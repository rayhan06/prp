package treeView;

public class TreeFactory 
{
	
	public enum TYPE
	{
		MINISTRIES_TO_OFFICES_INCLUDING_ORIGINS,
		MINISTRIES_TO_OFFICES_EXCLUDING_ORIGINS,
		MINISTRIES_TO_ORIGINS,
		MINISTRIES,
		MINISTRIES_TO_ORGANOGRAMS_EXCLUDING_ORIGINS,
		ORIGIN_UNITS_TO_ORIGIN_ORGANOGRAMS,
		UNITS_TO_ORGANOGRAMS,
		EDMS_DOC_TREE
    }


    public static TreeDTO getTreeDTO(TYPE type)
	{
		TreeDTO treeDTO = new TreeDTO();
		if(type == TYPE.MINISTRIES_TO_OFFICES_INCLUDING_ORIGINS)
		{
			treeDTO.parentChildMap = new String[]{"office_ministries", "office_layers", "office_origins", "offices"};
			treeDTO.showNameMap = new String[]{"Office Ministries", "Office Layers", "Office Origins", "Offices"};
			treeDTO.parentIDColumn = new String[]{"", "office_ministry_id", "office_layer_id", "office_origin_id"};
			treeDTO.englishNameColumn = new String[]{"name_eng", "layer_name_eng", "office_name_eng", "office_name_eng"};
			treeDTO.banglaNameColumn = new String[]{"name_bng", "layer_name_bng", "office_name_bng", "office_name_bng"};	
		}
		else if(type == TYPE.MINISTRIES_TO_OFFICES_EXCLUDING_ORIGINS)
		{
			treeDTO.parentChildMap = new String[]{"office_ministries", "office_layers", "offices"};
			treeDTO.showNameMap = new String[]{"Office Ministries", "Office Layers",  "Offices"};
			treeDTO.parentIDColumn = new String[]{"", "office_ministry_id", "office_layer_id"};
			treeDTO.englishNameColumn = new String[]{"name_eng", "layer_name_eng", "office_name_eng"};
			treeDTO.banglaNameColumn = new String[]{"name_bng", "layer_name_bng", "office_name_bng"};	
		}
		else if(type == TYPE.MINISTRIES_TO_ORIGINS)
		{
			treeDTO.parentChildMap = new String[]{"office_ministries", "office_layers", "office_origins"};
			treeDTO.showNameMap = new String[]{"Office Ministries", "Office Layers", "Office Origins"};
			treeDTO.parentIDColumn = new String[]{"", "office_ministry_id", "office_layer_id"};
			treeDTO.englishNameColumn = new String[]{"name_eng", "layer_name_eng", "office_name_eng"};
			treeDTO.banglaNameColumn = new String[]{"name_bng", "layer_name_bng", "office_name_bng"};	
		}
		else if(type == TYPE.MINISTRIES)
		{
			treeDTO.parentChildMap = new String[]{"office_ministries"};
			treeDTO.showNameMap = new String[]{"Office Ministries"};
			treeDTO.parentIDColumn = new String[]{""};
			treeDTO.englishNameColumn = new String[]{"name_eng"};
			treeDTO.banglaNameColumn = new String[]{"name_bng"};	
		}
		else if(type == TYPE.MINISTRIES_TO_ORGANOGRAMS_EXCLUDING_ORIGINS)
		{
			treeDTO.parentChildMap = new String[]{"office_ministries", "office_layers", "offices", "office_units", "office_unit_organograms"};
			treeDTO.showNameMap = new String[]{"Office Ministries", "Office Layers",  "Offices", "Office Units", "Office Unit Organograms"};
			treeDTO.parentIDColumn = new String[]{"", "office_ministry_id", "office_layer_id", "office_id", "office_unit_id"};
			treeDTO.englishNameColumn = new String[]{"name_eng", "layer_name_eng", "office_name_eng", "unit_name_eng", "designation_eng"};
			treeDTO.banglaNameColumn = new String[]{"name_bng", "layer_name_bng", "office_name_bng", "unit_name_bng", "designation_bng"};
			treeDTO.sameTableParentColumnName = new String[] {"", "", "", "parent_unit_id", ""}; //only for recursive tables
		}
		else if(type == TYPE.ORIGIN_UNITS_TO_ORIGIN_ORGANOGRAMS)
		{
			treeDTO.parentChildMap = new String[]{"office_origin_units", "office_origin_unit_organograms"};
			treeDTO.showNameMap = new String[]{"Office Origin Units", "Office Origin Unit Organograms"};
			treeDTO.parentIDColumn = new String[]{"office_origin_id", "office_origin_unit_id"};
			treeDTO.englishNameColumn = new String[]{"unit_name_eng", "designation_eng"};
			treeDTO.banglaNameColumn = new String[]{"unit_name_bng", "designation_bng"};
			treeDTO.sameTableParentColumnName = new String[] {"parent_unit_id", ""}; //only for recursive tables
		}
		else if( type == TYPE.EDMS_DOC_TREE ){

			treeDTO.parentChildMap = new String[]{ "office_units", "office_unit_organograms"};
			treeDTO.showNameMap = new String[]{ "Offices Units", "Office Unit Organograms"};
			treeDTO.parentIDColumn = new String[]{ "", "office_unit_id"};
			treeDTO.englishNameColumn = new String[]{ "unit_name_eng", "designation_eng"};
			treeDTO.banglaNameColumn = new String[]{ "unit_name_bng", "designation_bng"};

			treeDTO.sameTableParentColumnName = new String[]{  "parent_unit_id", ""}; //only for recursive tables
		}
		else if(type == TYPE.UNITS_TO_ORGANOGRAMS)
		{
			treeDTO.parentChildMap = new String[]{"office_units", "office_unit_organograms"};
			treeDTO.showNameMap = new String[]{"Office Units", "Office Unit Organograms"};
			treeDTO.parentIDColumn = new String[]{"office_id", "office_unit_id"};
			treeDTO.englishNameColumn = new String[]{"unit_name_eng", "designation_eng"};
			treeDTO.banglaNameColumn = new String[]{"unit_name_bng", "designation_bng"};
			treeDTO.sameTableParentColumnName = new String[] {"parent_unit_id", ""}; //only for recursive tables
		}
		
		return treeDTO;
	}

}
