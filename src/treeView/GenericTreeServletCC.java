package treeView;

import java.io.IOException;
import java.util.*;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import centre.Constants;
import inbox.InboxServlet;
import login.LoginDTO;
import permission.MenuConstants;
import role.PermissionRepository;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;

import pb.*;

/**
 * Servlet implementation class TreeServlet
 */
@WebServlet("/GenericTreeServletCC")
public class GenericTreeServletCC extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	 public static Logger logger = Logger.getLogger(GenericTreeServlet.class);
	
	public String actionType = "";
	public String servletType = "GenericTreeServlet";
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GenericTreeServletCC() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {		
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		try
		{
			actionType = request.getParameter("actionType");
			TreeDTO treeDTO = new TreeDTO();
			System.out.println("############################# In doget2 actionType = " + actionType);
			if(actionType.equals("getOffice"))
			{
				
				treeDTO = TreeFactory.getTreeDTO(TreeFactory.TYPE.MINISTRIES_TO_OFFICES_INCLUDING_ORIGINS);
				treeDTO.pageTitle = "Office Management";
				treeDTO.treeName = "OfficeDropDownChain";
				treeDTO.treeType = "select";
				treeDTO.treeBody = "officeOriginTreeBody.jsp";
				treeDTO.checkBoxType = 0;
				treeDTO.plusButtonType = 0;
			}

			else if(actionType.equals("getOriginUnit"))
			{
				treeDTO = TreeFactory.getTreeDTO(TreeFactory.TYPE.ORIGIN_UNITS_TO_ORIGIN_ORGANOGRAMS);
				treeDTO.pageTitle = "Office Units";
				treeDTO.treeName = "OfficeOriginUnitsTree";
				treeDTO.treeType = "tree";
				treeDTO.treeBody = "officeOriginTreeBody.jsp";
				treeDTO.checkBoxType = 2;				
				treeDTO.plusButtonType = 0;
				treeDTO.drawExtraLayer = true;
			}
			
			else if(actionType.equals("getUnit"))
			{
				treeDTO = TreeFactory.getTreeDTO(TreeFactory.TYPE.UNITS_TO_ORGANOGRAMS);
				treeDTO.pageTitle = "Office Units";
				treeDTO.treeName = "OfficeUnitsTree";
				treeDTO.treeType = "tree";
				treeDTO.treeBody = "officeOriginTreeBody.jsp";
				treeDTO.checkBoxType = 2;				
				treeDTO.plusButtonType = 0;
				treeDTO.drawExtraLayer = true;
			}
			else if(actionType.equals("getApprovalPath"))
			{
				treeDTO = TreeFactory.getTreeDTO(TreeFactory.TYPE.MINISTRIES_TO_OFFICES_INCLUDING_ORIGINS);
				treeDTO.pageTitle = "Approval Path";
				treeDTO.treeName = "OfficeDropDownChain";
				treeDTO.treeType = "select";
				treeDTO.treeBody = "approvalPathTreeBody.jsp";
				treeDTO.checkBoxType = 0;
				treeDTO.additionalParams = "&pathType=1";
				treeDTO.plusButtonType = 0;
			}
			else if(actionType.equals("getApprovalPathTemplate"))
			{
				treeDTO = TreeFactory.getTreeDTO(TreeFactory.TYPE.MINISTRIES_TO_ORIGINS);
				treeDTO.pageTitle = "Approval Path Template";
				treeDTO.treeName = "OfficeDropDownChain";
				treeDTO.treeType = "select";
				treeDTO.treeBody = "approvalPathTreeBody.jsp";
				treeDTO.checkBoxType = 0;
				treeDTO.plusButtonType = 0;
				treeDTO.additionalParams = "&pathType=2";
			}
			else if(actionType.equals("assignPathToOrganogram"))
			{
				treeDTO = TreeFactory.getTreeDTO(TreeFactory.TYPE.MINISTRIES_TO_ORGANOGRAMS_EXCLUDING_ORIGINS);
				treeDTO.pageTitle = "Assign Approval Path";
				treeDTO.treeName = "approvalPathTree";
				treeDTO.treeType = "tree";
				treeDTO.treeBody = "OrganogramPathTreeBody.jsp";				
				treeDTO.checkBoxType = 1;
				treeDTO.plusButtonType = 0;
			}
			else if(actionType.equals("assignPathToOffices"))
			{
				treeDTO = TreeFactory.getTreeDTO(TreeFactory.TYPE.MINISTRIES_TO_OFFICES_EXCLUDING_ORIGINS);
				treeDTO.pageTitle = "Assign Approval Path";
				treeDTO.treeName = "approvalPathTree";
				treeDTO.treeType = "tree";
				treeDTO.treeBody = "OrganogramPathTreeBody.jsp";
				treeDTO.checkBoxType = 1;
				treeDTO.plusButtonType = 0;
			}
			else if(actionType.equals("assignPathToMinistries"))
			{
				treeDTO = TreeFactory.getTreeDTO(TreeFactory.TYPE.MINISTRIES);
				treeDTO.pageTitle = "Assign Approval Path";
				treeDTO.treeName = "approvalPathTree";
				treeDTO.treeType = "tree";
				treeDTO.treeBody = "OrganogramPathTreeBody.jsp";
				treeDTO.checkBoxType = 1;
				treeDTO.plusButtonType = 0;
			}
			else if(actionType.equals("getUnitWithCheckBox"))
			{
				System.out.println("in getUnitWithCheckBox" );
				treeDTO = TreeFactory.getTreeDTO(TreeFactory.TYPE.UNITS_TO_ORGANOGRAMS);
				treeDTO.pageTitle = "Office Units";
				treeDTO.treeName = "OfficeUnitsTree";
				treeDTO.treeType = "tree";
				treeDTO.treeBody = "approvalPathTreeBody.jsp";
				treeDTO.checkBoxType = 1;				
				treeDTO.additionalParams = "&pathType=1";
				treeDTO.plusButtonType = 0;
			}
			else if(actionType.equals("getUnitOriginWithCheckBox"))
			{
				System.out.println("in getUnitOriginWithCheckBox" );
				treeDTO = TreeFactory.getTreeDTO(TreeFactory.TYPE.ORIGIN_UNITS_TO_ORIGIN_ORGANOGRAMS);
				treeDTO.pageTitle = "Office Units";
				treeDTO.treeName = "OfficeUnitsTree";
				treeDTO.treeType = "tree";
				treeDTO.treeBody = "approvalPathTreeBody.jsp";
				treeDTO.checkBoxType = 1;				
				treeDTO.additionalParams = "&pathType=2";
				treeDTO.plusButtonType = 0;
				treeDTO.drawExtraLayer = true;
			}
			else if(actionType.equals("getRole"))
			{
				treeDTO = TreeFactory.getTreeDTO(TreeFactory.TYPE.MINISTRIES_TO_OFFICES_INCLUDING_ORIGINS);
				treeDTO.pageTitle = "Role Management";
				treeDTO.treeName = "OfficeDropDownChain";
				treeDTO.treeType = "select";
				treeDTO.treeBody = "roleTreeBody.jsp";
				treeDTO.checkBoxType = 0;
				treeDTO.plusButtonType = 0;
			}
			else if(actionType.equals("getFlatUnits"))
			{
				System.out.println("in getFlatUnits" );
				treeDTO = TreeFactory.getTreeDTO(TreeFactory.TYPE.UNITS_TO_ORGANOGRAMS);
				treeDTO.pageTitle = "Office Units";
				treeDTO.treeName = "OfficeUnitsTree";
				treeDTO.treeType = "flat";
				treeDTO.treeBody = "roleTreeBody.jsp";
				treeDTO.checkBoxType = 1;
				treeDTO.plusButtonType = 0;

			}
			else if(actionType.equals("getOfficeRole"))
			{
				treeDTO = TreeFactory.getTreeDTO(TreeFactory.TYPE.MINISTRIES_TO_ORGANOGRAMS_EXCLUDING_ORIGINS);
				treeDTO.pageTitle = "Role Management";
				treeDTO.treeName = "OfficeDropDownChain";
				treeDTO.treeType = "tree";
				treeDTO.treeBody = "officeRoleTreeBody.jsp";				
				treeDTO.checkBoxType = 2;
				treeDTO.plusButtonType = 0;
			}
			else if(actionType.equals("assignApprovalPathTemplate"))
			{
				treeDTO = TreeFactory.getTreeDTO(TreeFactory.TYPE.MINISTRIES_TO_OFFICES_INCLUDING_ORIGINS);
				treeDTO.pageTitle = "Assign Approval Path to Office";
				treeDTO.treeName = "OfficeDropDownChain";
				treeDTO.treeType = "select";
				treeDTO.treeBody = "approvalAssignmentTreeBody.jsp";
				treeDTO.checkBoxType = 0;
				treeDTO.plusButtonType = 0;
			}
			else if (actionType.equals("assignPathToOrganogram")) {
				treeDTO.parentChildMap = new String[]{"office_ministries", "office_layers", "offices", "office_units", "office_unit_organograms"};
				treeDTO.showNameMap = new String[]{"Office Ministries", "Office Layers", "Offices", "Office Units", "Office Unit Organograms"};
				treeDTO.parentIDColumn = new String[]{"", "office_ministry_id", "office_layer_id", "office_id", "office_unit_id"};
				treeDTO.englishNameColumn = new String[]{"name_eng", "layer_name_eng", "office_name_eng", "unit_name_eng", "designation_eng"};
				treeDTO.banglaNameColumn = new String[]{"name_bng", "layer_name_bng", "office_name_bng", "unit_name_bng", "designation_bng"};
				treeDTO.pageTitle = "Assign Approval Path";
				treeDTO.treeName = "approvalPathTree";
				treeDTO.treeType = "tree";
				treeDTO.treeBody = "OrganogramPathTreeBody.jsp";
				treeDTO.sameTableParentColumnName = new String[]{"", "", "", "parent_unit_id", ""}; //only for recursive tables
				treeDTO.checkBoxType = 1;
				treeDTO.plusButtonType = 0;
				}
			
			
			
			
			//if(PermissionRepository.getInstance().checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.TREE_VIEW))
			//if(true)
			{
				String myLayer = request.getParameter("myLayer");
				if(myLayer != null && !myLayer.equalsIgnoreCase(""))
				{
					int layer = Integer.parseInt(request.getParameter("myLayer"));
					if(treeDTO.treeType.equalsIgnoreCase("tree") && 
							layer < treeDTO.parentChildMap.length && 
							treeDTO.sameTableParentColumnName != null &&							
							(!treeDTO.sameTableParentColumnName[layer].equals("") || !treeDTO.sameTableParentColumnName[layer].equals("")))
					{
						getRecursiveNodes(request, response, layer, treeDTO);
					}
					else
					{
						getNormalNodes(request, response, layer, treeDTO);
					}
				}
				else
				{					
					getTopNodes(request, response, treeDTO);	
				}
			}
					
			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			
			logger.debug("a1 : "+actionType + " : " + ex);
		}
	}
	
	private void setAttributes(int parentID, int layer, List<Integer> IDs, HttpServletRequest request, TreeDTO treeDTO)
	{

		request.setAttribute("nodeIDs", IDs);
		request.setAttribute("myLayer", layer);
		String request_layer_id_pairs = request.getParameter("layer_id_pairs");
		if(request_layer_id_pairs == null)
		{
			request_layer_id_pairs = "";
		}
		System.out.println("layer_id_pairs = " + request_layer_id_pairs);
		treeDTO.layer_id_pairs = request_layer_id_pairs;
		request.setAttribute("treeDTO", treeDTO);
		
	}
	
	private void setParametersAndForward(int parentID, String nodeJSP, HttpServletRequest request, HttpServletResponse response, TreeDTO treeDTO)
	{

		try 
		{
			String parentElementID = request.getParameter("parentElementID");
			String checkBoxChecked = request.getParameter("checkBoxChecked");
			request.getRequestDispatcher("treeView/" + nodeJSP 
					+ "?actionType=" + actionType 
					+ "&treeType=" + treeDTO.treeType 
					+ "&treeBody=" + treeDTO.treeBody 
					+ "&servletType=" + servletType
					+ "&pageTitle=" + treeDTO.pageTitle
					+ "&parentID=" + parentID
					+ "&checkBoxChecked=" + checkBoxChecked
					+ "&parentElementID=" + parentElementID
					+ treeDTO.additionalParams
					).forward(request, response);
		}
		catch (ServletException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		catch (IOException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	
	private String getNodeJSP(TreeDTO treeDTO)
	{
		if(treeDTO.treeType.equalsIgnoreCase("tree"))
		{
			return "treeNode.jsp";
		}
		else if(treeDTO.treeType.equalsIgnoreCase("select"))
		{
			return "dropDownNode.jsp";
			
		}
		else if(treeDTO.treeType.equalsIgnoreCase("flat"))
		{
			return "expandedTreeNode.jsp";
			
		}
		return "";
	}
	
	private void getTopNodes(HttpServletRequest request, HttpServletResponse response, TreeDTO treeDTO) throws ServletException, IOException 
	{
		System.out.println("METHOD: getTopNodes");
		try
		{
			List<Integer> IDs = GenericTree.getTopIDs(treeDTO.parentChildMap[0]);
			setAttributes(-1, 0, IDs, request, treeDTO);
			String nodeJsp = "tree.jsp";		
			setParametersAndForward(0, nodeJsp, request, response, treeDTO);			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}
	
	private void getRecursiveNodes(HttpServletRequest request, HttpServletResponse response, int layer, TreeDTO treeDTO)
    {
		System.out.println("METHOD: getRecursiveNodes");
        int id = Integer.parseInt(request.getParameter("id"));
        int parentTableID;
       
        try
        {
            parentTableID = Integer.parseInt(request.getParameter("parentID"));

            int iPrevLayer = -1;
            String prevLayer = request.getParameter("prevLayer");
            if (prevLayer != null && !prevLayer.equalsIgnoreCase(""))
            {
                iPrevLayer = Integer.parseInt(prevLayer);
            }
            System.out.println("layer = " + layer + " id = " + id + " treeDTO.parentChildMap.length = " + treeDTO.parentChildMap.length + " iPrevLayer = " + iPrevLayer);
            if (layer != 0 &&
                    (treeDTO.sameTableParentColumnName != null &&
                            (!treeDTO.sameTableParentColumnName[layer].equals("") || !treeDTO.sameTableParentColumnName[layer].equals("")))
                    && layer != iPrevLayer)  //If a recursive node suddenly starts at midpoint
            {
                parentTableID = id;
                id = 0;
                System.out.println("AAAAAAAAAAAAAAA after reshuffling layer = " + layer + " id = " + id + " parentTableID = " + parentTableID);
            }

            if (layer < treeDTO.parentChildMap.length)
            {
                List<Integer> UnitIDs = GenericTree.getRecurviveIDs(treeDTO.parentChildMap[layer], treeDTO.sameTableParentColumnName[layer], id, treeDTO.parentIDColumn[layer], parentTableID);

                List<Integer> IDs = new ArrayList<Integer>();
                List<Integer> recursiveNodeTypes = new ArrayList<Integer>();

                setAttributes(id, layer, IDs, request, treeDTO);

                if (id != 0 && layer + 1 < treeDTO.parentChildMap.length)
                {
                    List<Integer> ChildIDs = GenericTree.getChildIDs(treeDTO.parentChildMap[layer + 1], treeDTO.parentIDColumn[layer + 1], id);
                    if(ChildIDs == null)
                    {
                        ChildIDs = new ArrayList<>();

                    }
                    ChildIDs.add(-1);

                    if (ChildIDs != null)
                    {
                        System.out.println("ooooooooooooooooooo ChildIDs = " + ChildIDs.size());
                        for (int i = 0; i < ChildIDs.size(); i++) {
                            IDs.add(ChildIDs.get(i));
                            recursiveNodeTypes.add(2);
                        }
                    }
                }

                for (int i = 0; i < UnitIDs.size(); i++)
                {
                    IDs.add(UnitIDs.get(i));
                    recursiveNodeTypes.add(1);
                }
                request.setAttribute("isRecursive", 1);
                request.setAttribute("recursiveNodeTypes", recursiveNodeTypes);
                request.setAttribute("childName", treeDTO.parentChildMap[layer + 1]);
                request.setAttribute("childEnglishNameColumn", treeDTO.englishNameColumn[layer + 1]);
                request.setAttribute("childBanglaNameColumn", treeDTO.banglaNameColumn[layer + 1]);

				String nodeJsp = "treeNode.jsp";
                System.out.println("SSSSSSSSSSSS Setting parentTableID = " + parentTableID);
                setParametersAndForward(parentTableID, nodeJsp, request, response, treeDTO);

            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }
	
	private void getNormalNodes(HttpServletRequest request, HttpServletResponse response, int layer, TreeDTO treeDTO) throws ServletException, IOException 
	{
		
		System.out.println("METHOD: getNormalNodes");
		try
		{
			int id = Integer.parseInt(request.getParameter("id"));
			System.out.println("NNNNNNNNNNNNNN getNormalNodes layer = " + layer + " id = " + id);
			
			if(treeDTO.parentChildMap == null)
			{
				treeDTO.parentChildMap = new String[]{"office_ministries", "office_layers", "offices", "office_units", "office_unit_organograms"};
			}
			
			if(treeDTO.parentIDColumn == null)
			{
				treeDTO.parentIDColumn = new String[]{"", "office_ministry_id", "office_layer_id", "office_id", "office_unit_id"};
				
			}
			
				
				
			if(layer < treeDTO.parentChildMap.length)
			{				
				List<Integer> IDs = GenericTree.getChildIDs(treeDTO.parentChildMap[layer], treeDTO.parentIDColumn[layer], id);
				setAttributes(id, layer, IDs, request, treeDTO);
				String nodeJsp = getNodeJSP(treeDTO);
				treeDTO.treeType = "tree";
				System.out.println("Going to geoNode, treeDTO.treeType = " + treeDTO.treeType);				
				setParametersAndForward(id, nodeJsp, request, response, treeDTO);				
			
			}
			else
			{
				System.out.println("Maximum Layer Reached");
			}
			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}
	


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

}
