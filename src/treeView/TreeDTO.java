package treeView;

import java.util.Map;

public class TreeDTO
{
    public String[] parentChildMap = null;
    public String[] showNameMap = null;
    public String[] parentIDColumn = null;
    public String[] englishNameColumn = null;
    public String[] banglaNameColumn = null;
    public String[] sameTableParentColumnName = null;
    public String pageTitle = "";
    public String treeName = ""; //Must be unique in the same page. Must not contain spaces
    public String treeType = ""; //Must be "tree" or "select" or "flat"
    public String treeBody = ""; //Template File
    public boolean drawExtraLayer = false;
    public boolean[] hasExtraLayerPlusButton = null;
    public int checkBoxType = 0; //0 means no checkbox, 1 means checkboxes in leaves, 2 means checkboxes in all the layers.
    public int plusButtonType = 0;
    public String additionalParams = "";
    public String layer_id_pairs = "";
    public String filter = null;
    public boolean helpButton = false;
    public boolean showUpDownButton = false;

    public String actionType = "";
}
