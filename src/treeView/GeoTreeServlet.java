package treeView;

import java.io.IOException;
import java.util.*;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import centre.Constants;
import login.LoginDTO;
import permission.MenuConstants;
import role.PermissionRepository;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;

import pb.*;

/**
 * Servlet implementation class TreeServlet
 */
@WebServlet("/GeoTreeServlet")
public class GeoTreeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	String servletType = "GeoTreeServlet";
	String actionType = "";
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GeoTreeServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("In doget request = " + request);
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		try
		{
			actionType = request.getParameter("actionType");
			TreeDTO treeDTO = new TreeDTO();
			treeDTO.treeBody = "geoTreeBody.jsp";
			if(actionType.equals("getUnionView"))
			{
				treeDTO.parentChildMap = new String[]{"division", "district", "upazila", "union"};
				treeDTO.pageTitle = "à¦ªà§�à¦°à¦¶à¦¾à¦¸à¦¨à¦¿à¦• à¦¬à¦¿à¦­à¦¾à¦— à¦¥à§‡à¦•à§‡ à¦‡à¦‰à¦¨à¦¿à§Ÿà¦¨";
				treeDTO.treeName = "UnionView";				
			}
			
			treeDTO.englishNameColumn = new String[treeDTO.parentChildMap.length];
			treeDTO.banglaNameColumn = new String[treeDTO.parentChildMap.length];
			treeDTO.showNameMap = new String[treeDTO.parentChildMap.length];
			for(int i = 0; i < treeDTO.parentChildMap.length; i ++)
			{
				treeDTO.englishNameColumn[i] = treeDTO.parentChildMap[i] + "_name_eng";
				treeDTO.banglaNameColumn[i] = treeDTO.parentChildMap[i] + "_name_bng";
			}
			//if(PermissionRepository.getInstance().checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.TREE_VIEW))
			{
				String myLayer = request.getParameter("myLayer");
				if(myLayer != null && !myLayer.equalsIgnoreCase(""))
				{
					getNormalNodes(request, response, treeDTO);						
				}
				else
				{
					getTopNodes(request, response, treeDTO);	
				}
							
			}
					
			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}
	
	private void getTopNodes(HttpServletRequest request, HttpServletResponse response, TreeDTO treeDTO) throws ServletException, IOException {

		try
		{
			List<Integer> IDs = GeoTree.getTopIDs(treeDTO.parentChildMap[0]);
			request.setAttribute("nodeIDs", IDs);
			request.setAttribute("myLayer", 0);
			request.setAttribute("treeDTO", treeDTO);
			
			request.getRequestDispatcher("treeView/tree.jsp?" 
			+ "actionType=" + actionType 
			+ "&servletType=" + servletType 
			+ "&treeBody=" + treeDTO.treeBody
			+ "&pageTitle=" + treeDTO.pageTitle
			+ "&parentID=" + 0
			).forward(request, response);
			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}
	
	private void getNormalNodes(HttpServletRequest request, HttpServletResponse response, TreeDTO treeDTO) throws ServletException, IOException 
	{
		

		try
		{
			int layer = Integer.parseInt(request.getParameter("myLayer"));
			int id = Integer.parseInt(request.getParameter("id"));
			System.out.println("layer = " + layer + " id = " + id);
			if(layer < treeDTO.parentChildMap.length)
			{				
				List<Integer> IDs = GeoTree.getChildIDs(treeDTO.parentChildMap[layer], treeDTO.parentChildMap[layer - 1], id);
				request.setAttribute("nodeIDs", IDs);
				request.setAttribute("myLayer", layer);
				request.setAttribute("treeDTO", treeDTO);
				System.out.println("Going to geoNode");
				request.getRequestDispatcher("treeView/treeNode.jsp?" 
				+ "actionType=" + actionType 
				+ "&servletType=" + servletType 
				+ "&treeBody=" + treeDTO.treeBody
				+ "&pageTitle=" + treeDTO.pageTitle
				+ "&parentID=" + id
				).forward(request, response);
				
			}
			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}
	


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

}
