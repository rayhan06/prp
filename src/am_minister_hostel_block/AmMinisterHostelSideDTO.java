package am_minister_hostel_block;
import java.util.*; 
import util.*; 


public class AmMinisterHostelSideDTO extends CommonDTO
{

	public long amMinisterHostelBlockId = -1;
	public int ministerHostelSideCat = -1;
	public long insertedByUserId = -1;
	public long insertedByOrganogramId = -1;
	public long insertionDate = -1;
    public String lastModifierUser = "";
	
	public List<AmMinisterHostelSideDTO> amMinisterHostelSideDTOList = new ArrayList<>();
	
    @Override
	public String toString() {
            return "$AmMinisterHostelSideDTO[" +
            " iD = " + iD +
            " amMinisterHostelBlockId = " + amMinisterHostelBlockId +
            " ministerHostelSideCat = " + ministerHostelSideCat +
            " searchColumn = " + searchColumn +
            " insertedByUserId = " + insertedByUserId +
            " insertedByOrganogramId = " + insertedByOrganogramId +
            " insertionDate = " + insertionDate +
            " lastModifierUser = " + lastModifierUser +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}