package am_minister_hostel_block;

import com.google.gson.Gson;
import org.apache.log4j.Logger;
import repository.Repository;
import repository.RepositoryManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;


public class AmMinisterHostelSideRepository implements Repository {
	AmMinisterHostelSideDAO amMinisterHostelSideDAO = null;
	Gson gson = null;

	
	static Logger logger = Logger.getLogger(am_minister_hostel_block.AmMinisterHostelSideRepository.class);
	Map<Long, AmMinisterHostelSideDTO>mapOfAmMinisterHostelSideDTOToiD;
	Map<Long, List<AmMinisterHostelSideDTO>>mapOfAmMinisterHostelSideDTOToBlockId;

  
	private AmMinisterHostelSideRepository(){
		gson = new Gson();
		mapOfAmMinisterHostelSideDTOToiD = new ConcurrentHashMap<>();
		mapOfAmMinisterHostelSideDTOToBlockId = new ConcurrentHashMap<>();
		amMinisterHostelSideDAO = AmMinisterHostelSideDAO.getInstance();
		RepositoryManager.getInstance().addRepository(this);
	}

	private static class LazyLoader{
        final static AmMinisterHostelSideRepository INSTANCE = new AmMinisterHostelSideRepository();
    }

    public static AmMinisterHostelSideRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

	public void reload(boolean reloadAll){
		try {
			List<AmMinisterHostelSideDTO> amMinisterHostelSideDTOs = amMinisterHostelSideDAO.getAllDTOs(reloadAll);
			for(AmMinisterHostelSideDTO amMinisterHostelSideDTO : amMinisterHostelSideDTOs) {
				AmMinisterHostelSideDTO oldAmMinisterHostelSideDTO = getAmMinisterHostelSideDTOByIDWithoutClone(amMinisterHostelSideDTO.iD);
				if( oldAmMinisterHostelSideDTO != null ) {
					mapOfAmMinisterHostelSideDTOToiD.remove(oldAmMinisterHostelSideDTO.iD);

					if (mapOfAmMinisterHostelSideDTOToBlockId.get(oldAmMinisterHostelSideDTO.amMinisterHostelBlockId) != null) {
						mapOfAmMinisterHostelSideDTOToBlockId.get(oldAmMinisterHostelSideDTO.amMinisterHostelBlockId).remove(oldAmMinisterHostelSideDTO);
					}
					
				}
				if(amMinisterHostelSideDTO.isDeleted == 0) 
				{
					
					mapOfAmMinisterHostelSideDTOToiD.put(amMinisterHostelSideDTO.iD, amMinisterHostelSideDTO);

					List<AmMinisterHostelSideDTO> minister_hostel_blockDTOS = mapOfAmMinisterHostelSideDTOToBlockId.getOrDefault(amMinisterHostelSideDTO.amMinisterHostelBlockId, new ArrayList<>());
					minister_hostel_blockDTOS.add(amMinisterHostelSideDTO);
					mapOfAmMinisterHostelSideDTOToBlockId.put(amMinisterHostelSideDTO.amMinisterHostelBlockId, minister_hostel_blockDTOS);

				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<AmMinisterHostelSideDTO> getAmMinisterHostelSideList() {
		List <AmMinisterHostelSideDTO> amMinisterHostelSides = new ArrayList<AmMinisterHostelSideDTO>(this.mapOfAmMinisterHostelSideDTOToiD.values());
		return amMinisterHostelSides;
	}

	public AmMinisterHostelSideDTO clone(AmMinisterHostelSideDTO dto) {
		if (dto == null)return null;
		String raw = gson.toJson(dto);
		return gson.fromJson(raw, AmMinisterHostelSideDTO.class);
	}

	public List<AmMinisterHostelSideDTO> clone(List<AmMinisterHostelSideDTO> dtoList) {
		if (dtoList == null)return new ArrayList<>();
		return dtoList
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}
	
	public AmMinisterHostelSideDTO getAmMinisterHostelSideDTOByIDWithoutClone( long ID){
		return mapOfAmMinisterHostelSideDTOToiD.get(ID);
	}

	public AmMinisterHostelSideDTO getAmMinisterHostelSideDTOByID( long ID){
		return clone(mapOfAmMinisterHostelSideDTOToiD.get(ID));
	}

	public List<AmMinisterHostelSideDTO> getAmMinisterHostelSideDTOByBlockID( long ID){
		return clone(mapOfAmMinisterHostelSideDTOToBlockId.get(ID));
	}

	@Override
	public String getDtoJsonById(long ID) {
		return gson.toJson(getAmMinisterHostelSideDTOByID(ID));
	}
	
	@Override
	public String getTableName() {
		return "am_minister_hostel_side";
	}
}


