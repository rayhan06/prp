package am_minister_hostel_block;

import java.sql.*;
import java.util.*;
import java.util.stream.Collectors;

import am_minister_hostel_block.AmMinisterHostelSideDTO;
import common.CommonDAOService;
import common.ConnectionAndStatementUtil;
import org.apache.log4j.Logger;
import util.*;
import pb.*;

public class AmMinisterHostelSideDAO  implements CommonDAOService<AmMinisterHostelSideDTO>
{
	
	Logger logger = Logger.getLogger(getClass());
	
	private static String addQuery = "";
	private static String updateQuery = "";
	private String[] columnNames = null;

	private AmMinisterHostelSideDAO()
	{
		columnNames = new String[] 
		{
			"ID",
			"am_minister_hostel_block_id",
			"minister_hostel_side_cat",
			"search_column",
			"inserted_by_user_id",
			"inserted_by_organogram_id",
			"insertion_date",
			"last_modifier_user",
			"isDeleted",
			"lastModificationTime"
		};
		updateQuery = getUpdateQuery(columnNames);
        addQuery = getAddQuery(columnNames);
		
		searchMap.put("am_minister_hostel_block_id"," and (am_minister_hostel_block_id = ?)");
		searchMap.put("minister_hostel_side_cat"," and (minister_hostel_side_cat = ?)");
		searchMap.put("insertion_date_start"," and (insertion_date >= ?)");
		searchMap.put("insertion_date_end"," and (insertion_date <= ?)");
		searchMap.put("AnyField"," and (search_column like ?)");
	}

	private final Map<String,String> searchMap = new HashMap<>();

    private static class LazyLoader
    {
    	static final AmMinisterHostelSideDAO INSTANCE = new AmMinisterHostelSideDAO();
	}

	public static AmMinisterHostelSideDAO getInstance()
	{
    	return LazyLoader.INSTANCE;
	}
	
	public void setSearchColumn(AmMinisterHostelSideDTO amministerhostelsideDTO)
	{
		amministerhostelsideDTO.searchColumn = "";
		amministerhostelsideDTO.searchColumn += CommonDAO.getName(amministerhostelsideDTO.amMinisterHostelBlockId, "am_minister_hostel_block", "block_no", "id") ;
		amministerhostelsideDTO.searchColumn += CatDAO.getName("English", "minister_hostel_side", amministerhostelsideDTO.ministerHostelSideCat) + " " + CatDAO.getName("Bangla", "minister_hostel_side", amministerhostelsideDTO.ministerHostelSideCat) + " ";
	}
	
	@Override
	public void set(PreparedStatement ps, AmMinisterHostelSideDTO amministerhostelsideDTO, boolean isInsert) throws SQLException
	{
		int index = 0;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(amministerhostelsideDTO);
		if(isInsert)
		{
			ps.setObject(++index,amministerhostelsideDTO.iD);
		}
		ps.setObject(++index,amministerhostelsideDTO.amMinisterHostelBlockId);
		ps.setObject(++index,amministerhostelsideDTO.ministerHostelSideCat);
		ps.setObject(++index,amministerhostelsideDTO.searchColumn);
		ps.setObject(++index,amministerhostelsideDTO.insertedByUserId);
		ps.setObject(++index,amministerhostelsideDTO.insertedByOrganogramId);
		ps.setObject(++index,amministerhostelsideDTO.insertionDate);
		ps.setObject(++index,amministerhostelsideDTO.lastModifierUser);
		if(isInsert)
		{
			ps.setObject(++index,amministerhostelsideDTO.isDeleted);
		}
		ps.setObject(++index, lastModificationTime);
		if(!isInsert)
		{
			ps.setObject(++index,amministerhostelsideDTO.iD);
		}
	}
	
	@Override
	public AmMinisterHostelSideDTO buildObjectFromResultSet(ResultSet rs)
	{
		try
		{
			AmMinisterHostelSideDTO amministerhostelsideDTO = new AmMinisterHostelSideDTO();
			int i = 0;
			amministerhostelsideDTO.iD = rs.getLong(columnNames[i++]);
			amministerhostelsideDTO.amMinisterHostelBlockId = rs.getLong(columnNames[i++]);
			amministerhostelsideDTO.ministerHostelSideCat = rs.getInt(columnNames[i++]);
			amministerhostelsideDTO.searchColumn = rs.getString(columnNames[i++]);
			amministerhostelsideDTO.insertedByUserId = rs.getLong(columnNames[i++]);
			amministerhostelsideDTO.insertedByOrganogramId = rs.getLong(columnNames[i++]);
			amministerhostelsideDTO.insertionDate = rs.getLong(columnNames[i++]);
			amministerhostelsideDTO.lastModifierUser = rs.getString(columnNames[i++]);
			amministerhostelsideDTO.isDeleted = rs.getInt(columnNames[i++]);
			amministerhostelsideDTO.lastModificationTime = rs.getLong(columnNames[i++]);
			return amministerhostelsideDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
		
	public AmMinisterHostelSideDTO getDTOByID (long id)
	{
		return getDTOFromID(id);
	}

	@Override
	public String getTableName() {
		return "am_minister_hostel_side";
	}

	@Override
	public Map<String, String> getSearchMap() {
		return searchMap;
	}

	public long add(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((AmMinisterHostelSideDTO) commonDTO,addQuery,true);
	}

	public long update(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((AmMinisterHostelSideDTO) commonDTO,updateQuery,false);
	}
	
	@Override
	public String getLastModifierUser(){
		return "last_modifier_user";
    }



    long deleteSideChildrenByParent(long parentId, String parentColName, String sqlIds) throws Exception {
		long lastModificationTime = System.currentTimeMillis();
		StringBuilder sqlBuilder = new StringBuilder("UPDATE ")
				.append(getTableName())
				.append(" SET isDeleted=1,lastModificationTime=")
				.append(lastModificationTime)
				.append(" WHERE " + parentColName + " = ")
				.append(parentId)
				.append(sqlIds);
		ConnectionAndStatementUtil.getWriteConnectionAndStatement(model -> {
			String sql = sqlBuilder.toString();
			Connection connection = model.getConnection();
			Statement stmt = model.getStatement();
			try {
				logger.debug(sql);
				stmt.execute(sql);
				recordUpdateTime(model.getConnection(), getTableName(), lastModificationTime);
			} catch (SQLException ex) {
				logger.error(ex);
			}
		});
		return parentId;
	}


	public void deleteSideChildrenNotInList(String parentName, String childName, long parentID, List<Long> chilIds, String sqlIds) throws Exception {
		ConnectionAndStatementUtil.getWriteStatement(st -> {
			String sql = "update " + childName + " set isDeleted = 1, lastModificationTime = " + System.currentTimeMillis()
					+ " WHERE " + parentName + "_id=" + parentID;
			sql += sqlIds;
			if (chilIds != null && chilIds.size() > 0) {
				String ids = chilIds.stream()
						.map(String::valueOf)
						.collect(Collectors.joining(","));
				sql += " and id not in (" + ids + ")";
			}
			logger.debug("sql " + sql);
			try {
				st.execute(sql);
			} catch (SQLException ex) {
				logger.error(ex);
			}
		});
	}


}
	