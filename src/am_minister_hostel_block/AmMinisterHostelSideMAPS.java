package am_minister_hostel_block;
import java.util.*; 
import util.*;


public class AmMinisterHostelSideMAPS extends CommonMaps
{	
	public AmMinisterHostelSideMAPS(String tableName)
	{
		


		java_SQL_map.put("am_minister_hostel_block_id".toLowerCase(), "amMinisterHostelBlockId".toLowerCase());
		java_SQL_map.put("minister_hostel_side_cat".toLowerCase(), "ministerHostelSideCat".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Am Minister Hostel Block".toLowerCase(), "amMinisterHostelBlockId".toLowerCase());
		java_Text_map.put("Minister Hostel Side".toLowerCase(), "ministerHostelSideCat".toLowerCase());
		java_Text_map.put("Search Column".toLowerCase(), "searchColumn".toLowerCase());
		java_Text_map.put("Inserted By User Id".toLowerCase(), "insertedByUserId".toLowerCase());
		java_Text_map.put("Inserted By Organogram Id".toLowerCase(), "insertedByOrganogramId".toLowerCase());
		java_Text_map.put("Insertion Date".toLowerCase(), "insertionDate".toLowerCase());
		java_Text_map.put("Last Modifier User".toLowerCase(), "lastModifierUser".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}