package am_minister_hostel_block;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.sql.SQLException;
import common.CommonDAOService;
import org.apache.log4j.Logger;
import util.*;
import pb.*;

public class Am_minister_hostel_blockDAO  implements CommonDAOService<Am_minister_hostel_blockDTO>
{

	Logger logger = Logger.getLogger(getClass());

	private static String addQuery = "";
	private static String updateQuery = "";
	private String[] columnNames = null;

	private Am_minister_hostel_blockDAO()
	{
		columnNames = new String[]
		{
			"ID",
			"block_no",
			"search_column",
			"inserted_by_user_id",
			"inserted_by_organogram_id",
			"insertion_date",
			"last_modifier_user",
			"isDeleted",
			"lastModificationTime"
		};
		updateQuery = getUpdateQuery(columnNames);
        addQuery = getAddQuery(columnNames);

		searchMap.put("block_no"," and (block_no like ?)");
		searchMap.put("insertion_date_start"," and (insertion_date >= ?)");
		searchMap.put("insertion_date_end"," and (insertion_date <= ?)");
		searchMap.put("AnyField"," and (search_column like ?)");
	}

	private final Map<String,String> searchMap = new HashMap<>();

    private static class LazyLoader
    {
    	static final Am_minister_hostel_blockDAO INSTANCE = new Am_minister_hostel_blockDAO();
	}

	public static Am_minister_hostel_blockDAO getInstance()
	{
    	return LazyLoader.INSTANCE;
	}

	public void setSearchColumn(Am_minister_hostel_blockDTO am_minister_hostel_blockDTO)
	{
		am_minister_hostel_blockDTO.searchColumn = "";
		am_minister_hostel_blockDTO.searchColumn += am_minister_hostel_blockDTO.blockNo + " ";
	}

	@Override
	public void set(PreparedStatement ps, Am_minister_hostel_blockDTO am_minister_hostel_blockDTO, boolean isInsert) throws SQLException
	{
		int index = 0;
		long lastModificationTime = System.currentTimeMillis();
		setSearchColumn(am_minister_hostel_blockDTO);
		if(isInsert)
		{
			ps.setObject(++index,am_minister_hostel_blockDTO.iD);
		}
		ps.setObject(++index,am_minister_hostel_blockDTO.blockNo);
		ps.setObject(++index,am_minister_hostel_blockDTO.searchColumn);
		ps.setObject(++index,am_minister_hostel_blockDTO.insertedByUserId);
		ps.setObject(++index,am_minister_hostel_blockDTO.insertedByOrganogramId);
		ps.setObject(++index,am_minister_hostel_blockDTO.insertionDate);
		ps.setObject(++index,am_minister_hostel_blockDTO.lastModifierUser);
		if(isInsert)
		{
			ps.setObject(++index,am_minister_hostel_blockDTO.isDeleted);
		}
		ps.setObject(++index, lastModificationTime);
		if(!isInsert)
		{
			ps.setObject(++index,am_minister_hostel_blockDTO.iD);
		}
	}

	@Override
	public Am_minister_hostel_blockDTO buildObjectFromResultSet(ResultSet rs)
	{
		try
		{
			Am_minister_hostel_blockDTO am_minister_hostel_blockDTO = new Am_minister_hostel_blockDTO();
			int i = 0;
			am_minister_hostel_blockDTO.iD = rs.getLong(columnNames[i++]);
			am_minister_hostel_blockDTO.blockNo = rs.getString(columnNames[i++]);
			am_minister_hostel_blockDTO.searchColumn = rs.getString(columnNames[i++]);
			am_minister_hostel_blockDTO.insertedByUserId = rs.getLong(columnNames[i++]);
			am_minister_hostel_blockDTO.insertedByOrganogramId = rs.getLong(columnNames[i++]);
			am_minister_hostel_blockDTO.insertionDate = rs.getLong(columnNames[i++]);
			am_minister_hostel_blockDTO.lastModifierUser = rs.getString(columnNames[i++]);
			am_minister_hostel_blockDTO.isDeleted = rs.getInt(columnNames[i++]);
			am_minister_hostel_blockDTO.lastModificationTime = rs.getLong(columnNames[i++]);
			return am_minister_hostel_blockDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}

	public Am_minister_hostel_blockDTO getDTOByID (long id)
	{
		Am_minister_hostel_blockDTO am_minister_hostel_blockDTO = null;
		try
		{
			am_minister_hostel_blockDTO = getDTOFromID(id);
			if(am_minister_hostel_blockDTO != null)
			{
//				List<AmMinisterHostelSideDTO> amMinisterHostelSideDTOList = (List<AmMinisterHostelSideDTO>)AmMinisterHostelSideRepository.getInstance().getAmMinisterHostelSideDTOByBlockID(am_minister_hostel_blockDTO.iD);
				List<AmMinisterHostelSideDTO> amMinisterHostelSideDTOList = (List<AmMinisterHostelSideDTO>)AmMinisterHostelSideDAO.getInstance().getDTOsByParent("am_minister_hostel_block_id", am_minister_hostel_blockDTO.iD);
				am_minister_hostel_blockDTO.amMinisterHostelSideDTOList = amMinisterHostelSideDTOList;
			}
		}
		catch (Exception e)
		{
				// TODO Auto-generated catch block
				e.printStackTrace();
		}
		return am_minister_hostel_blockDTO;
	}

	@Override
	public String getTableName() {
		return "am_minister_hostel_block";
	}

	@Override
	public Map<String, String> getSearchMap() {
		return searchMap;
	}

	public long add(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Am_minister_hostel_blockDTO) commonDTO,addQuery,true);
	}

	public long update(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Am_minister_hostel_blockDTO) commonDTO,updateQuery,false);
	}

	@Override
	public String getLastModifierUser(){
		return "last_modifier_user";
    }
				
}
	