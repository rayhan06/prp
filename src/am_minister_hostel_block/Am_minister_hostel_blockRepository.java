package am_minister_hostel_block;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import com.google.gson.Gson;
import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class Am_minister_hostel_blockRepository implements Repository {
	Am_minister_hostel_blockDAO am_minister_hostel_blockDAO = null;
	Gson gson = null;


	static Logger logger = Logger.getLogger(Am_minister_hostel_blockRepository.class);
	Map<Long, Am_minister_hostel_blockDTO>mapOfAm_minister_hostel_blockDTOToiD;

  
	private Am_minister_hostel_blockRepository(){
		gson = new Gson();
		mapOfAm_minister_hostel_blockDTOToiD = new ConcurrentHashMap<>();
		am_minister_hostel_blockDAO = Am_minister_hostel_blockDAO.getInstance();
		RepositoryManager.getInstance().addRepository(this);
	}

	private static class LazyLoader{
        final static Am_minister_hostel_blockRepository INSTANCE = new Am_minister_hostel_blockRepository();
    }

    public static Am_minister_hostel_blockRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

	public void reload(boolean reloadAll){
		try {
			List<Am_minister_hostel_blockDTO> am_minister_hostel_blockDTOs = am_minister_hostel_blockDAO.getAllDTOs(reloadAll);
			for(Am_minister_hostel_blockDTO am_minister_hostel_blockDTO : am_minister_hostel_blockDTOs) {
				Am_minister_hostel_blockDTO oldAm_minister_hostel_blockDTO = getAm_minister_hostel_blockDTOByIDWithoutClone(am_minister_hostel_blockDTO.iD);
				if( oldAm_minister_hostel_blockDTO != null ) {
					mapOfAm_minister_hostel_blockDTOToiD.remove(oldAm_minister_hostel_blockDTO.iD);
				
					
				}
				if(am_minister_hostel_blockDTO.isDeleted == 0) 
				{
					
					mapOfAm_minister_hostel_blockDTOToiD.put(am_minister_hostel_blockDTO.iD, am_minister_hostel_blockDTO);
				
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Am_minister_hostel_blockDTO> getAm_minister_hostel_blockList() {
		List <Am_minister_hostel_blockDTO> am_minister_hostel_blocks = new ArrayList<Am_minister_hostel_blockDTO>(this.mapOfAm_minister_hostel_blockDTOToiD.values());
		return am_minister_hostel_blocks;
	}

	public Am_minister_hostel_blockDTO clone(Am_minister_hostel_blockDTO dto) {
		if (dto == null)return null;
		String raw = gson.toJson(dto);
		return gson.fromJson(raw, Am_minister_hostel_blockDTO.class);
	}

	public List<Am_minister_hostel_blockDTO> clone(List<Am_minister_hostel_blockDTO> dtoList) {
		if (dtoList == null)return new ArrayList<>();
		return dtoList
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}

	public Am_minister_hostel_blockDTO getAm_minister_hostel_blockDTOByIDWithoutClone( long ID){
		return mapOfAm_minister_hostel_blockDTOToiD.get(ID);
	}

	public Am_minister_hostel_blockDTO getAm_minister_hostel_blockDTOByID( long ID){
		return clone(mapOfAm_minister_hostel_blockDTOToiD.get(ID));
	}

	@Override
	public String getDtoJsonById(long ID) {
		return gson.toJson(getAm_minister_hostel_blockDTOByID(ID));
	}
	
	@Override
	public String getTableName() {
		return "am_minister_hostel_block";
	}
}


