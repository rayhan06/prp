package am_minister_hostel_block;
import java.util.*; 
import util.*; 


public class Am_minister_hostel_blockDTO extends CommonDTO
{

    public String blockNo = "";
	public long insertedByUserId = -1;
	public long insertedByOrganogramId = -1;
	public long insertionDate = -1;
    public String lastModifierUser = "";
	
	public List<AmMinisterHostelSideDTO> amMinisterHostelSideDTOList = new ArrayList<>();
	
    @Override
	public String toString() {
            return "$Am_minister_hostel_blockDTO[" +
            " iD = " + iD +
            " blockNo = " + blockNo +
            " searchColumn = " + searchColumn +
            " insertedByUserId = " + insertedByUserId +
            " insertedByOrganogramId = " + insertedByOrganogramId +
            " insertionDate = " + insertionDate +
            " lastModifierUser = " + lastModifierUser +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}