package am_minister_hostel_block;

import am_minister_hostel_unit.Am_minister_hostel_unitDAO;
import com.google.gson.Gson;
import common.BaseServlet;
import files.FilesDTO;
import language.LC;
import language.LM;
import login.LoginDTO;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import pb.*;
import permission.MenuConstants;
import role.PermissionRepository;
import role.RoleDTO;
import sessionmanager.SessionConstants;
import user.UserDTO;
import util.*;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;


/**
 * Servlet implementation class Am_minister_hostel_blockServlet
 */
@WebServlet("/Am_minister_hostel_blockServlet")
@MultipartConfig
public class Am_minister_hostel_blockServlet extends BaseServlet {
    private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Am_minister_hostel_blockServlet.class);
    String domainName = "am_minister_hostel_side";
    CatDAO catDAO = new CatDAO();

    @Override
    public String getTableName() {
        return Am_minister_hostel_blockDAO.getInstance().getTableName();
    }

    @Override
    public String getServletName() {
        return "Am_minister_hostel_blockServlet";
    }

    @Override
    public Am_minister_hostel_blockDAO getCommonDAOService() {
        return Am_minister_hostel_blockDAO.getInstance();
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[]{MenuConstants.AM_MINISTER_HOSTEL_BLOCK_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[]{MenuConstants.AM_MINISTER_HOSTEL_BLOCK_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[]{MenuConstants.AM_MINISTER_HOSTEL_BLOCK_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Am_minister_hostel_blockServlet.class;
    }

    AmMinisterHostelSideDAO amMinisterHostelSideDAO = AmMinisterHostelSideDAO.getInstance();
    private final Gson gson = new Gson();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        CommonLoginData commonLoginData = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get();
        init(request);
        try {
            switch (request.getParameter("actionType")) {
                case "getAddPage":
                    if (Utils.checkPermission(commonLoginData.userDTO, getAddPageMenuConstants()) && getAddPagePermission(request)) {
                        finalize(request);
                        request.getRequestDispatcher(commonPartOfDispatchURL() + "Edit.jsp").forward(request, response);
                        return;
                    }
                    break;
                case "getEditPage":
                    if (Utils.checkPermission(commonLoginData.userDTO, getEditPageMenuConstants()) && getEditPagePermission(request)) {
                        setDTOForJsp(request);
                        finalize(request);
                        request.getRequestDispatcher(commonPartOfDispatchURL() + "Edit.jsp?actionType=edit").forward(request, response);
                        return;
                    }
                    break;
                case "getBlockOptions":
                    getBlockOptions(request, response);
                    return;
                case "getSideOptionsByBlockId":
                    getSideOptionsByBlockId(request, response);
                    return;
                case "downloadDropzoneFile": {
                    long id = Long.parseLong(request.getParameter("id"));
                    FilesDTO filesDTO = (FilesDTO) filesDAO.getDTOByID(id);
                    Utils.ProcessFile(request, response, filesDTO.fileTitle, filesDTO.inputStream);
                    return;
                }
                case "DeleteFileFromDropZone": {
                    long id = Long.parseLong(request.getParameter("id"));
                    logger.debug("In delete file");
                    filesDAO.hardDeleteByID(id);
                    response.getWriter().write("Deleted");
                    return;
                }
                case "getURL":
                    String URL = request.getParameter("URL");
                    response.sendRedirect(URL);
                    return;
                case "search":
                    if (Utils.checkPermission(commonLoginData.userDTO, getSearchMenuConstants()) && getSearchPagePermission(request)) {
                        UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;
                        RoleDTO role = PermissionRepository.getRoleDTOByRoleID(userDTO.roleID);
//						boolean isAdmin = role.ID == SessionConstants.VEHICLE_ADMIN_ROLE || role.ID == SessionConstants.ADMIN_ROLE;
                        boolean isAdmin = role.ID == SessionConstants.ADMIN_ROLE || role.ID == SessionConstants.ASSET_ADMIN_ROLE;
                        if (!isAdmin) {
                            return;
                        }
                        search(request, response);
                        return;
                    }
                    break;
                case "view":
                    logger.debug("view requested");
                    if (Utils.checkPermission(commonLoginData.userDTO, getViewMenuConstants()) && getViewPagePermission(request)) {
                        setDTOForJsp(request);
                        finalize(request);
                        request.getRequestDispatcher(commonPartOfDispatchURL() + "View.jsp?ID=" + getId(request)).forward(request, response);
                        return;
                    }
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error(ex);
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }

    @Override
    public boolean getEditPermission(HttpServletRequest request) {
        return checkEditPermission(request);
    }

    @Override
    public boolean getAddPermission(HttpServletRequest request) {
        return checkEditPermission(request);
    }

    @Override
    public boolean getAddPagePermission(HttpServletRequest request) {
        return checkEditPermission(request);
    }

    @Override
    public boolean getEditPagePermission(HttpServletRequest request) {
        return checkEditPagePermission(request);
    }

    @Override
    public boolean getViewPagePermission(HttpServletRequest request) {
        return checkEditPagePermission(request);
    }

    private boolean checkEditPagePermission(HttpServletRequest request) {
        UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;
        RoleDTO role = PermissionRepository.getRoleDTOByRoleID(userDTO.roleID);
        boolean isAdmin = role.ID == SessionConstants.ADMIN_ROLE || role.ID == SessionConstants.ASSET_ADMIN_ROLE;
        return isAdmin;
//		if (isAdmin) {
//			return true;
//		}
//		Am_minister_hostel_blockDTO am_minister_hostel_blockDTO = getCommonDAOService().getDTOFromID(Long.parseLong(request.getParameter("ID")));
//		if (am_minister_hostel_blockDTO == null) {
//			return false;
//		}
//		return am_minister_hostel_blockDTO.insertedByOrganogramId == userDTO.organogramID;
    }

    private boolean checkEditPermission(HttpServletRequest request) {
        UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;
        RoleDTO role = PermissionRepository.getRoleDTOByRoleID(userDTO.roleID);
        boolean isAdmin = role.ID == SessionConstants.ADMIN_ROLE || role.ID == SessionConstants.ASSET_ADMIN_ROLE;
        return isAdmin;
//		if (isAdmin) {
//			return true;
//		}
//		Am_minister_hostel_blockDTO am_minister_hostel_blockDTO = getCommonDAOService().getDTOFromID(Long.parseLong(request.getParameter("iD")));
//		if (am_minister_hostel_blockDTO == null) {
//			return false;
//		}
//		return am_minister_hostel_blockDTO.insertedByOrganogramId == userDTO.organogramID;
    }

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        // TODO Auto-generated method stub

        request.setAttribute("failureMessage", "");
        System.out.println("%%%% addAm_minister_hostel_block");
        String language = userDTO.languageID == SessionConstants.ENGLISH ? "English" : "Bangla";
        Am_minister_hostel_blockDTO am_minister_hostel_blockDTO;
        SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");

        if (addFlag == true) {
            am_minister_hostel_blockDTO = new Am_minister_hostel_blockDTO();
        } else {
            am_minister_hostel_blockDTO = Am_minister_hostel_blockDAO.getInstance().getDTOFromID(Long.parseLong(request.getParameter("iD")));

            List<Long> childExists = null;
            childExists = AmMinisterHostelSideDAO.getInstance().getDistinctColumnIds("am_minister_hostel_block", "am_minister_hostel_unit", "am_minister_hostel_block_id", Arrays.asList(am_minister_hostel_blockDTO.iD));
            if (!childExists.isEmpty()) {
                String errMsg = language.equals("English") ?
                        "This cannot be edited, because it has at least one child" :
                        "দুঃখিত, এটি পরিবর্তন করা যাবে না, কারণ এর অন্তত একটি চাইল্ড বিদ্যমান";
                throw new Exception(errMsg);
            }
        }

        String Value = "";

        Value = request.getParameter("blockNo");

        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }
        System.out.println("blockNo = " + Value);
        if (Value != null) {
            am_minister_hostel_blockDTO.blockNo = (Value);
        } else {
            System.out.println("FieldName has a null Value, not updating" + " = " + Value);
        }

        Value = request.getParameter("searchColumn");

        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }
        System.out.println("searchColumn = " + Value);
        if (Value != null) {
            am_minister_hostel_blockDTO.searchColumn = (Value);
        } else {
            System.out.println("FieldName has a null Value, not updating" + " = " + Value);
        }

        if (addFlag) {
            am_minister_hostel_blockDTO.insertedByUserId = userDTO.ID;
        }


        if (addFlag) {
            am_minister_hostel_blockDTO.insertedByOrganogramId = userDTO.organogramID;
        }


        if (addFlag) {
            am_minister_hostel_blockDTO.insertionDate = TimeConverter.getToday();
        }


        am_minister_hostel_blockDTO.lastModifierUser = userDTO.userName;


        System.out.println("Done adding  addAm_minister_hostel_block dto = " + am_minister_hostel_blockDTO);

        List<AmMinisterHostelSideDTO> amMinisterHostelSideDTOList = createAmMinisterHostelSideDTOListByRequest(request, language);

        Utils.handleTransaction(() -> {
            if (addFlag == true) {
                Am_minister_hostel_blockDAO.getInstance().add(am_minister_hostel_blockDTO);
            } else {
                Am_minister_hostel_blockDAO.getInstance().update(am_minister_hostel_blockDTO);
            }


            if (addFlag == true) //add or validate
            {
                if (amMinisterHostelSideDTOList != null) {
                    for (AmMinisterHostelSideDTO amMinisterHostelSideDTO : amMinisterHostelSideDTOList) {
                        amMinisterHostelSideDTO.amMinisterHostelBlockId = am_minister_hostel_blockDTO.iD;
                        amMinisterHostelSideDAO.add(amMinisterHostelSideDTO);
                    }
                }

            } else {

                List<Long> childIDsInDatabaseForSide = amMinisterHostelSideDAO.getChilIds("am_minister_hostel_block", "am_minister_hostel_side", am_minister_hostel_blockDTO.iD);

                List<Long> childOfSideExists = Am_minister_hostel_unitDAO.getInstance().getDistinctColumnIds("am_minister_hostel_side", "am_minister_hostel_unit", "am_minister_hostel_side_id", childIDsInDatabaseForSide);
                String sqlIDs = "";
                if (childOfSideExists != null && !childOfSideExists.isEmpty()) {
                    String idStr = childOfSideExists.stream()
                            .distinct()
                            .map(String::valueOf)
                            .collect(Collectors.joining(","));
                    sqlIDs += " and ID not in (" + idStr + ") ";
                }

                List<Long> childIdsFromRequest = amMinisterHostelSideDAO.getChildIdsFromRequest(request, "amMinisterHostelSide");
                //delete the removed children
                amMinisterHostelSideDAO.deleteSideChildrenNotInList("am_minister_hostel_block", "am_minister_hostel_side", am_minister_hostel_blockDTO.iD, childIdsFromRequest, sqlIDs);
                List<Long> childIDsInDatabase = amMinisterHostelSideDAO.getChilIds("am_minister_hostel_block", "am_minister_hostel_side", am_minister_hostel_blockDTO.iD);


                if (childIdsFromRequest != null) {
                    for (int i = 0; i < childIdsFromRequest.size(); i++) {
                        Long childIDFromRequest = childIdsFromRequest.get(i);
                        if (childIDsInDatabase.contains(childIDFromRequest)) {
                            AmMinisterHostelSideDTO amMinisterHostelSideDTO = createAmMinisterHostelSideDTOByRequestAndIndex(request, false, i, language);
                            amMinisterHostelSideDTO.amMinisterHostelBlockId = am_minister_hostel_blockDTO.iD;
                            amMinisterHostelSideDAO.update(amMinisterHostelSideDTO);
                        } else {
                            AmMinisterHostelSideDTO amMinisterHostelSideDTO = createAmMinisterHostelSideDTOByRequestAndIndex(request, true, i, language);
                            amMinisterHostelSideDTO.amMinisterHostelBlockId = am_minister_hostel_blockDTO.iD;
                            amMinisterHostelSideDAO.add(amMinisterHostelSideDTO);
                        }
                    }
                } else {
                    amMinisterHostelSideDAO.deleteSideChildrenByParent(am_minister_hostel_blockDTO.iD, "am_minister_hostel_block_id", sqlIDs);
                }

            }
        });

        return am_minister_hostel_blockDTO;


    }


    public void deleteTBackup(HttpServletRequest request, UserDTO userDTO) throws Exception {
        String language = userDTO.languageID == SessionConstants.ENGLISH ? "English" : "Bangla";
        String[] IDsToDelete = request.getParameterValues("ID");
        if (IDsToDelete.length > 0) {
            List<Long> ids = Stream.of(IDsToDelete)
                    .map(Long::parseLong)
                    .collect(Collectors.toList());

            List<Long> childExists = AmMinisterHostelSideDAO.getInstance().getDistinctColumnIds("am_minister_hostel_block", "am_minister_hostel_side", "am_minister_hostel_block_id", ids);
            childExists
                    .forEach(childExistsId -> ids.remove(childExistsId));

            if (!ids.isEmpty()) {
                getCommonDAOService().deletePb(userDTO.employee_record_id, ids);
            }

            if (!childExists.isEmpty()) {
                List<Am_minister_hostel_blockDTO> childExistDTOs = Am_minister_hostel_blockDAO.getInstance().getDTOs(childExists);

                String existingNameSting = childExistDTOs.stream()
                        .distinct()
                        .map(dto -> Utils.getDigits(dto.blockNo, language))
                        .collect(Collectors.joining(","));

                String errMsg = language.equals("English") ? "Because of existing children, couldnot delete these: " : "চাইল্ড ডাটা থাকায় ডিলিট করা যায়নি: ";
                errMsg += existingNameSting;

                throw new Exception(errMsg);
            }
        }
    }


    @Override
    public void deleteT(HttpServletRequest request, UserDTO userDTO) {
        String[] IDsToDelete = request.getParameterValues("ID");
        if (IDsToDelete.length > 0) {
            List<Long> ids = Stream.of(IDsToDelete)
                    .map(Long::parseLong)
                    .collect(Collectors.toList());

            List<Long> childExists = null;
            try {
                childExists = AmMinisterHostelSideDAO.getInstance().getDistinctColumnIds("am_minister_hostel_block", "am_minister_hostel_side", "am_minister_hostel_block_id", ids);
            } catch (Exception e) {
                e.printStackTrace();
            }
            childExists
                    .forEach(childExistsId -> ids.remove(childExistsId));

            if (!ids.isEmpty()) {
                try {
                    Utils.handleTransaction(() -> {
                        getCommonDAOService().deletePb(userDTO.employee_record_id, ids);
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }


    private List<AmMinisterHostelSideDTO> createAmMinisterHostelSideDTOListByRequest(HttpServletRequest request, String language) throws Exception {
        List<AmMinisterHostelSideDTO> amMinisterHostelSideDTOList = new ArrayList<AmMinisterHostelSideDTO>();
        if (request.getParameterValues("amMinisterHostelSide.iD") != null) {
            int amMinisterHostelSideItemNo = request.getParameterValues("amMinisterHostelSide.iD").length;


            for (int index = 0; index < amMinisterHostelSideItemNo; index++) {
                AmMinisterHostelSideDTO amMinisterHostelSideDTO = createAmMinisterHostelSideDTOByRequestAndIndex(request, true, index, language);
                amMinisterHostelSideDTOList.add(amMinisterHostelSideDTO);
            }

            return amMinisterHostelSideDTOList;
        }
        return null;
    }

    private AmMinisterHostelSideDTO createAmMinisterHostelSideDTOByRequestAndIndex(HttpServletRequest request, boolean addFlag,
                                                                                   int index, String language) throws Exception {

        AmMinisterHostelSideDTO amMinisterHostelSideDTO;
        boolean isLanEng = language.equalsIgnoreCase("English");

        if (addFlag) {
            amMinisterHostelSideDTO = new AmMinisterHostelSideDTO();
        } else {
            amMinisterHostelSideDTO = amMinisterHostelSideDAO.getDTOByID(Long.parseLong(request.getParameterValues("amMinisterHostelSide.iD")[index]));
        }

        if (addFlag) {

            amMinisterHostelSideDTO.insertionDate = amMinisterHostelSideDTO.lastModificationTime = System.currentTimeMillis();
            //amMinisterHostelSideDTO.insertedByUserId = amMinisterHostelSideDTO.lastModifierUser = String.valueOf(userDTO.ID);
        }


        String Value = "";

        Value = request.getParameterValues("amMinisterHostelSide.ministerHostelSideCat")[index];


        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        } else {
            throw new Exception(LM.getText(LC.AM_MINISTER_HOSTEL_BLOCK_ADD_AM_MINISTER_HOSTEL_SIDE_MINISTERHOSTELSIDECAT, language) + " " + ErrorMessage.getInvalidMessage(language));
        }

        int ministerHostelSideCat = Integer.parseInt(Value);

        if (ministerHostelSideCat == -2) {

            String otherSideNameEn = "";
            String otherSideNameBn = "";

            Value = request.getParameterValues("amMinisterHostelSide.otherMinisterHostelSideEn")[index];
            if (Value == null || Value.equals("") || Value.equals("-1")) {
                throw new Exception(isLanEng ? "Please provide minister hostel side english name" : "অনুগ্রহপূর্বক মন্ত্রী হোস্টেল পাশের ইংরেজি নাম দিন");
            }
            boolean hasNonEnglish =
                    Value.chars()
                            .anyMatch(ch -> !(ch >= ' ' && ch <= '~'));
            if (!hasNonEnglish) {
                otherSideNameEn = (Value.trim());
            } else {
                throw new Exception(isLanEng ? "Please provide minister hostel side english name" : "অনুগ্রহপূর্বক মন্ত্রী হোস্টেল পাশের ইংরেজি নাম দিন");
            }


            Value = request.getParameterValues("amMinisterHostelSide.otherMinisterHostelSideBn")[index];

            if (Value == null || Value.equals("") || Value.equals("-1")) {
                throw new Exception(isLanEng ? "Please provide block bangla name" : "অনুগ্রহপূর্বক ব্লকের বাংলা নাম দিন");
            }

            boolean hasAllBangla = Value.chars().allMatch(Utils::isValidBanglaCharacter);
            if (!hasAllBangla) {
                throw new Exception(isLanEng ? "Please provide minister hostel side bangla name" : "অনুগ্রহপূর্বক মন্ত্রী হোস্টেল পাশের বাংলা নাম দিন");
            }

            otherSideNameBn = (Value.trim());

            List<CategoryLanguageModel> categoryLanguageModelList = catDAO.getCategoryLanguageModelListByDomainName(domainName);

            String finalOtherSideNameEn = otherSideNameEn;
            String finalOtherSideNameBn = otherSideNameBn;
            boolean isBlockAlreadyExist = categoryLanguageModelList.stream().
                    anyMatch(e -> e.englishText.equals(finalOtherSideNameEn) || e.banglaText.equals(finalOtherSideNameBn));

            if (isBlockAlreadyExist) throw new Exception(isLanEng ? "Other minister hostel side name already exist" :
                    "অন্যান্য মন্ত্রী হোস্টেল পাশের নাম ইতোমধ্যে যোগ করা হয়েছে");

            amMinisterHostelSideDTO.ministerHostelSideCat = CatDAO.addFromOthers(domainName, otherSideNameEn, otherSideNameBn);
        } else {
            amMinisterHostelSideDTO.ministerHostelSideCat = ministerHostelSideCat;
            CategoryLanguageModel categoryLanguageModel = CatRepository.getInstance().getCategoryLanguageModel
                    (domainName, amMinisterHostelSideDTO.ministerHostelSideCat);
            if (categoryLanguageModel == null) {
                UtilCharacter.throwException("মন্ত্রী হোস্টেল পাশ খুঁজে পাওয়া যায় নি", "Minister hostel side not found");
            }
        }


        return amMinisterHostelSideDTO;

    }

    private void getBlockOptions(HttpServletRequest request, HttpServletResponse response) {
        try {
            System.out.println("In getBlockOptions");
            LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
            String Language = LM.getText(LC.VM_REQUISITION_EDIT_LANGUAGE, loginDTO);
            List<Am_minister_hostel_blockDTO> am_minister_hostel_blockDTOS = Am_minister_hostel_blockRepository.getInstance().getAm_minister_hostel_blockList();
            PrintWriter out = response.getWriter();
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");

            long id = -1;
            String blockIdString = request.getParameter("ID");
            if (StringUtils.isValidString(blockIdString)) id = Long.parseLong(blockIdString);

            String tempString = "";
            String selected = "";
            tempString += Utils.buildSelectOption(HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language.equalsIgnoreCase("english"));

            for (Am_minister_hostel_blockDTO am_minister_hostel_blockDTO : am_minister_hostel_blockDTOS) {
                selected = "";
                if (am_minister_hostel_blockDTO.iD == id) selected = "selected";
                tempString += "<option " + selected + " value='" + am_minister_hostel_blockDTO.iD + "'>" + (am_minister_hostel_blockDTO.blockNo) + "</option>";
            }

            String encoded = this.gson.toJson(tempString);
//			System.out.println("json encoded data = " + encoded);
            out.print(encoded);
            out.flush();
        } catch (NumberFormatException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    private void getSideOptionsByBlockId(HttpServletRequest request, HttpServletResponse response) {
        try {
            System.out.println("In getSideOptionsByBlockId");
            LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
            String Language = LM.getText(LC.VM_REQUISITION_EDIT_LANGUAGE, loginDTO);
            List<AmMinisterHostelSideDTO> amMinisterHostelSideDTOS = AmMinisterHostelSideRepository.getInstance().getAmMinisterHostelSideDTOByBlockID(Long.parseLong(request.getParameter("blockId")));
            PrintWriter out = response.getWriter();
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");

            long id = -1;
            String blockIdString = request.getParameter("ID");
            if (StringUtils.isValidString(blockIdString)) id = Long.parseLong(blockIdString);

            String tempString = "";
            String selected = "";
            tempString += Utils.buildSelectOption(HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language.equalsIgnoreCase("english"));

            for (AmMinisterHostelSideDTO amMinisterHostelSideDTO : amMinisterHostelSideDTOS) {
                selected = "";
                if (amMinisterHostelSideDTO.iD == id) selected = "selected";
                String side = CatRepository.getName(Language, "am_minister_hostel_side", amMinisterHostelSideDTO.ministerHostelSideCat);
                tempString += "<option " + selected + " value='" + amMinisterHostelSideDTO.iD + "'>" + (side) + "</option>";
            }

            String encoded = this.gson.toJson(tempString);
//			System.out.println("json encoded data = " + encoded);
            out.print(encoded);
            out.flush();
        } catch (NumberFormatException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }
}

