package id_card_requisition;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;
import java.sql.SQLException;


import org.apache.log4j.Logger;

import dbm.*;
import javafx.util.Pair;
import repository.RepositoryManager;

import util.*;
import employee_offices.Employee_officesDTO;
import user.UserDTO;

public class Id_card_requisitionDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());
	
	public CommonMaps approvalMaps = new Id_card_requisitionApprovalMAPS("id_card_requisition");
	
	public Id_card_requisitionDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";

		commonMaps = new Id_card_requisitionMAPS(tableName);
	}
	
	public Id_card_requisitionDAO()
	{
		this("id_card_requisition");		
	}
	
	public void set(PreparedStatement ps, Id_card_requisitionDTO id_card_requisitionDTO, boolean isInsert) throws SQLException
	{
		int index = 1;
		long lastModificationTime = System.currentTimeMillis();	
		if(isInsert)
		{
			ps.setObject(index++,id_card_requisitionDTO.iD);
		}
		ps.setObject(index++,id_card_requisitionDTO.employeeRecordsId);
		ps.setObject(index++,id_card_requisitionDTO.jobCat);
		ps.setObject(index++,id_card_requisitionDTO.isLost);
		ps.setObject(index++,id_card_requisitionDTO.filesDropzone);
		ps.setObject(index++,id_card_requisitionDTO.insertedByUserId);
		ps.setObject(index++,id_card_requisitionDTO.insertionDate);
		ps.setObject(index++,id_card_requisitionDTO.modifiedBy);
		if(isInsert)
		{
			ps.setObject(index++, 0);
			ps.setObject(index++, lastModificationTime);
		}
	}
	
	public void get(Id_card_requisitionDTO id_card_requisitionDTO, ResultSet rs) throws SQLException
	{
		id_card_requisitionDTO.iD = rs.getLong("ID");
		id_card_requisitionDTO.employeeRecordsId = rs.getLong("employee_records_id");
		id_card_requisitionDTO.jobCat = rs.getInt("job_cat");
		id_card_requisitionDTO.isLost = rs.getBoolean("is_lost");
		id_card_requisitionDTO.filesDropzone = rs.getLong("files_dropzone");
		id_card_requisitionDTO.insertedByUserId = rs.getLong("inserted_by_user_id");
		id_card_requisitionDTO.insertionDate = rs.getLong("insertion_date");
		id_card_requisitionDTO.modifiedBy = rs.getString("modified_by");
		id_card_requisitionDTO.isDeleted = rs.getInt("isDeleted");
		id_card_requisitionDTO.lastModificationTime = rs.getLong("lastModificationTime");
	}
	public void getEmployeeOffices(Employee_officesDTO employee_officeDTO, ResultSet rs) throws SQLException
	{
		employee_officeDTO.officeUnitOrganogramId = rs.getLong("office_unit_organogram_id");
		employee_officeDTO.designation = rs.getString("designation");
	}
	
	
	public long add(CommonDTO commonDTO) throws Exception
	{
		
		Id_card_requisitionDTO id_card_requisitionDTO = (Id_card_requisitionDTO)commonDTO;
		
		Connection connection = null;
		PreparedStatement ps = null;

		long lastModificationTime = System.currentTimeMillis();	

		try{
			connection = DBMW.getInstance().getConnection();
			
			if(connection == null)
			{
				System.out.println("nullconn");
			}

			id_card_requisitionDTO.iD = DBMW.getInstance().getNextSequenceId(tableName);

			String sql = "INSERT INTO " + tableName;
			
			sql += " (";
			sql += "ID";
			sql += ", ";
			sql += "employee_records_id";
			sql += ", ";
			sql += "job_cat";
			sql += ", ";
			sql += "is_lost";
			sql += ", ";
			sql += "files_dropzone";
			sql += ", ";
			sql += "inserted_by_user_id";
			sql += ", ";
			sql += "insertion_date";
			sql += ", ";
			sql += "modified_by";
			sql += ", ";
			sql += "isDeleted";
			sql += ", ";
			sql += "lastModificationTime";
		
			sql += ")";
			
			
            sql += " VALUES(";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			
			sql += ")";
				
			ps = connection.prepareStatement(sql);
			set(ps, id_card_requisitionDTO, true);
			ps.execute();
			
			
			recordUpdateTime(connection, lastModificationTime);

		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if (ps != null) {
					ps.close();
				}
			} catch(Exception e){}
			try{
				if(connection != null)
				{
					DBMW.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		return id_card_requisitionDTO.iD;		
	}
	public Pair<String,String> getEmployeeOfficeNameByRecordsID(long recordsID) {
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		long officeUnitId = -1;
		long officeId = -1;
		String officeNameEng = "";
		String officeNameBng = "";
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + "employee_offices";
			
            sql += " WHERE employee_record_id=" + recordsID;
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			if(rs.next()){
				 officeUnitId = rs.getLong("office_unit_id");
			}
			
			String sql2 = "SELECT * ";

			sql2 += " FROM " + "office_units";
			
            sql2 += " WHERE id=" + officeUnitId;
            
            rs = stmt.executeQuery(sql2);
            
            if(rs.next()){
				 officeId = rs.getLong("office_id");
			}
            
            String sql3 = "SELECT * ";

			sql3 += " FROM " + "offices";
			
            sql3 += " WHERE id=" + officeId;
            
            rs = stmt.executeQuery(sql3);
            
            if(rs.next()){
                officeNameEng = rs.getString("office_name_eng");
                officeNameBng = rs.getString("office_name_bng");
			}
			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return new Pair(officeNameEng,officeNameBng);
	}
	public Pair<String,String> getDesignationByRecordsID(long recordsID) {
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Employee_officesDTO employee_officeDTO = null;
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + "employee_offices";
			
            sql += " WHERE employee_record_id=" + recordsID;
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			if(rs.next()){
				employee_officeDTO = new Employee_officesDTO();

				getEmployeeOffices(employee_officeDTO, rs);

			}			
			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return getDesignationByOfficeUnitOrganogramId(employee_officeDTO.officeUnitOrganogramId);
	}
	public Pair<String,String> getDesignationByOfficeUnitOrganogramId(long officeUnitOrganogramId) {
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		String designationEng = "";
		String designationBng = "";
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + "office_unit_organograms";
			
            sql += " WHERE id=" + officeUnitOrganogramId;
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			if(rs.next()){
				designationEng = rs.getString("designation_eng");
				designationBng = rs.getString("designation_bng");
			}			
			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return new Pair(designationEng,designationBng);
	}

	//need another getter for repository
	public CommonDTO getDTOByID (long ID) throws Exception
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Id_card_requisitionDTO id_card_requisitionDTO = null;
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
			
            sql += " WHERE ID=" + ID;
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			if(rs.next()){
				id_card_requisitionDTO = new Id_card_requisitionDTO();

				get(id_card_requisitionDTO, rs);

			}			
			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return id_card_requisitionDTO;
	}
	
	public long update(CommonDTO commonDTO) throws Exception
	{		
		Id_card_requisitionDTO id_card_requisitionDTO = (Id_card_requisitionDTO)commonDTO;
		
		Connection connection = null;
		PreparedStatement ps = null;

		long lastModificationTime = System.currentTimeMillis();	
		try{
			connection = DBMW.getInstance().getConnection();

			String sql = "UPDATE " + tableName;
			
			sql += " SET ";
			sql += "employee_records_id=?";
			sql += ", ";
			sql += "job_cat=?";
			sql += ", ";
			sql += "is_lost=?";
			sql += ", ";
			sql += "files_dropzone=?";
			sql += ", ";
			sql += "inserted_by_user_id=?";
			sql += ", ";
			sql += "insertion_date=?";
			sql += ", ";
			sql += "modified_by=?";
			sql += ", lastModificationTime = "	+ lastModificationTime + "";
            sql += " WHERE ID = " + id_card_requisitionDTO.iD;
				

			ps = connection.prepareStatement(sql);
			set(ps, id_card_requisitionDTO, false);
			ps.executeUpdate();
			

			recordUpdateTime(connection, lastModificationTime);

						
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if (ps != null) {
					ps.close();
				}
			} catch(Exception e){}
			try{
				if(connection != null)
				{
					DBMW.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		return id_card_requisitionDTO.iD;
	}
	
	
	public List<Id_card_requisitionDTO> getDTOs(Collection recordIDs){
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Id_card_requisitionDTO id_card_requisitionDTO = null;
		List<Id_card_requisitionDTO> id_card_requisitionDTOList = new ArrayList<>();
		if(recordIDs.isEmpty()){
			return id_card_requisitionDTOList;
		}
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
            
            sql += " WHERE ID IN ( ";

			for(int i = 0;i<recordIDs.size();i++){
				if(i!=0){
					sql+=",";
				}
				sql+=((ArrayList)recordIDs).get(i);
			}
			sql+=")  order by lastModificationTime desc";
			
			printSql(sql);
			
			logger.debug("sql " + sql);
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				id_card_requisitionDTO = new Id_card_requisitionDTO();
				get(id_card_requisitionDTO, rs);
				System.out.println("got this DTO: " + id_card_requisitionDTO);
				
				id_card_requisitionDTOList.add(id_card_requisitionDTO);

			}			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return id_card_requisitionDTOList;
	
	}
	
	

	
	
	
	//add repository
	public List<Id_card_requisitionDTO> getAllId_card_requisition (boolean isFirstReload)
    {
		List<Id_card_requisitionDTO> id_card_requisitionDTOList = new ArrayList<>();

		String sql = "SELECT * FROM id_card_requisition";
		sql += " WHERE ";
	

		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by id_card_requisition.lastModificationTime desc";
		printSql(sql);
		
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;

		
		try{
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();
			rs = stmt.executeQuery(sql);
			

			while(rs.next()){
				Id_card_requisitionDTO id_card_requisitionDTO = new Id_card_requisitionDTO();
				get(id_card_requisitionDTO, rs);
				
				id_card_requisitionDTOList.add(id_card_requisitionDTO);
			}			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}

		return id_card_requisitionDTOList;
    }

	
	public List<Id_card_requisitionDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO)
	{
		return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
	}

	
	public List<Id_card_requisitionDTO> getDTOs(Hashtable  p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
			String filter, boolean tableHasJobCat)
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		List<Id_card_requisitionDTO> id_card_requisitionDTOList = new ArrayList<>();

		try{
			
			String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat);
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				Id_card_requisitionDTO id_card_requisitionDTO = new Id_card_requisitionDTO();
				get(id_card_requisitionDTO, rs);
				
				id_card_requisitionDTOList.add(id_card_requisitionDTO);
			}					
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return id_card_requisitionDTOList;
	
	}
	public String getSqlWithSearchCriteriaForApprovalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
			UserDTO userDTO, String filter, boolean tableHasJobCat)
	{
		
		boolean viewAll = false;
		
		if(p_searchCriteria != null && p_searchCriteria.get("ViewAll") != null)
		{
			System.out.println("ViewAll = " + p_searchCriteria.get("ViewAll"));
			viewAll = true;
		}
		else
		{
			System.out.println("ViewAll is null ");
		}
		
		String sql = "SELECT ";
		if(category == GETIDS)
		{
			sql += " " + tableName + ".ID as ID ";
		}
		else if(category == GETCOUNT)
		{
			sql += " count( " + tableName + ".ID) as countID ";
		}
		else if(category == GETDTOS)
		{
			sql += " distinct " + tableName + ".* ";
		}
		sql += "FROM " + tableName + " ";

		
		sql += " join approval_execution_table as aet on ("
				+ tableName + ".id = aet.updated_row_id "
				+ ")";
		
		sql += " join approval_summary on ("
				+ "aet.previous_row_id = approval_summary.table_id and  approval_summary.table_name = '" + tableName + "'"
				+ ")";
		
		if(!viewAll)
		{
																	
			sql += " left join approval_path_details on ("
					+ "aet.approval_path_id = approval_path_details.approval_path_id "
					+ "and aet.approval_path_order = approval_path_details.approval_order "
					+ ")";
		}
									
		
	

		

		String AllFieldSql = "";
		
	
		
		if(p_searchCriteria != null)
		{
						
			
			Enumeration names = p_searchCriteria.keys();
			String str, value;
			
			
			
			AllFieldSql = "(";
			int i = 0;
			while(names.hasMoreElements())
			{				
				str = (String) names.nextElement();
				value = (String)p_searchCriteria.get(str);
		        System.out.println(str + ": " + value);
		        if(approvalMaps.java_allfield_type_map.get(str.toLowerCase()) != null &&  !approvalMaps.java_allfield_type_map.get(str.toLowerCase()).equalsIgnoreCase("")
		        		&& !str.equalsIgnoreCase("AnyField")
		        		&&
						( ( value != null && !value.equalsIgnoreCase("") ) || commonMaps.rangeMap.get( str.toLowerCase() ) != null ) )
		        {
					if(p_searchCriteria.get(str).equals("any"))
		        	{
		        		continue;
		        	}
					
		        	if( i > 0)
		        	{
		        		AllFieldSql+= " AND  ";
		        	}

		        	String dataType = approvalMaps.java_allfield_type_map.get(str.toLowerCase()); 
		        	
		        	String fromTable = tableName;
		        	if(approvalMaps.java_table_map.get(str) != null)
		    		{
		    			fromTable = approvalMaps.java_table_map.get(str);
		    		}
		    		
		        	if(str.equalsIgnoreCase("starting_date") || str.equalsIgnoreCase("ending_date"))
		        	{
		        		String string_date = (String) p_searchCriteria.get(str);
		        		long milliseconds = 0;

		        		SimpleDateFormat f = new SimpleDateFormat("dd-MM-yyyy");
		        		try {
		        		    Date d = f.parse(string_date);
		        		    milliseconds = d.getTime();
		        		} catch (Exception e) {
		        		    e.printStackTrace();
		        		}
		        		if(str.equalsIgnoreCase("starting_date"))
		        		{
		        			AllFieldSql += "approval_summary.date_of_initiation >= " +  milliseconds;
		        		}
		        		else
		        		{
		        			AllFieldSql += "approval_summary.date_of_initiation <= " +  milliseconds;
		        		}
		        	}
		        	else
		        	{
		        		if( dataType.equals("String") )
			        	{
			        		AllFieldSql += "" + fromTable + "." + str.toLowerCase() + " like '%" + p_searchCriteria.get(str) + "%'";
			        	}
			        	else if( dataType.equals("Integer") || dataType.equals("Long"))
			        	{
			        		AllFieldSql += "" + fromTable + "." + str.toLowerCase() + " = " + p_searchCriteria.get(str) ;
			        	}
		        	}
		        	
		        	
		        	
		        	
		        	i ++;
		        }
			}
			
			AllFieldSql += ")";
			System.out.println("AllFieldSql = " + AllFieldSql);
			
			
		}
		sql += " WHERE ";
		
		if(viewAll)
		{
			sql += " (" + tableName + ".isDeleted != " + CommonDTO.DELETED + " and " + tableName + ".isDeleted != " + CommonDTO.OUTDATED
					+ ")";
		}
		else
		{
			sql += " (" + tableName + ".isDeleted != " + CommonDTO.DELETED + " and " + tableName + ".isDeleted != " + CommonDTO.OUTDATED
				+ " and (approval_path_details.organogram_id = " + userDTO.organogramID 
						+ " or " +  userDTO.organogramID + " in ("
						+ " select organogram_id from approval_execution_table where previous_row_id = aet.previous_row_id"
						+ "))"
				+ ")";
		}
		
		
		if(!filter.equalsIgnoreCase(""))
		{
			sql += " and " + filter + " ";
		}

		if(!AllFieldSql.equals("()") && !AllFieldSql.equals(""))
		{			
			sql += " AND " + AllFieldSql;
		}
		
	
			
		sql += "  order by " + tableName + ".lastModificationTime desc ";

		printSql(sql);
		
		if(limit >= 0)
		{
			sql += " limit " + limit;
		}
		if(offset >= 0)
		{
			sql += " offset " + offset;
		}
		
		System.out.println("-------------- sql = " + sql);
		
		return sql;
	}
	
	public String getSqlWithSearchCriteriaForNormalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
			UserDTO userDTO, String filter, boolean tableHasJobCat)
    {
		boolean viewAll = false;
		
		if(p_searchCriteria != null && p_searchCriteria.get("ViewAll") != null)
		{
			System.out.println("ViewAll = " + p_searchCriteria.get("ViewAll"));
			viewAll = true;
		}
		else
		{
			System.out.println("ViewAll is null ");
		}
		
		String sql = "SELECT ";
		if(category == GETIDS)
		{
			sql += " " + tableName + ".ID as ID ";
		}
		else if(category == GETCOUNT)
		{
			sql += " count( " + tableName + ".ID) as countID ";
		}
		else if(category == GETDTOS)
		{
			sql += " distinct " + tableName + ".* ";
		}
		sql += "FROM " + tableName + " ";
		sql += joinSQL;
		

		
		String AnyfieldSql = "";
		String AllFieldSql = "";
		
		if(p_searchCriteria != null)
		{
						
			
			Enumeration names = p_searchCriteria.keys();
			String str, value;
			
			AnyfieldSql = "(";
			
			if(p_searchCriteria.get("AnyField")!= null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase(""))
			{
				int i = 0;
				Iterator it = (commonMaps).java_anyfield_search_map.entrySet().iterator();
				while(it.hasNext())
				{
					if( i > 0)
		        	{
						AnyfieldSql+= " OR  ";
		        	}
					Entry pair = (Entry)it.next();
					AnyfieldSql+= pair.getKey() + " like '%" + p_searchCriteria.get("AnyField").toString() + "%'";
					i ++;
				}						
			}
			AnyfieldSql += ")";
			System.out.println("AnyfieldSql = " + AnyfieldSql);
			
			AllFieldSql = "(";
			int i = 0;
			while(names.hasMoreElements())
			{				
				str = (String) names.nextElement();
				value = (String)p_searchCriteria.get(str);
		        System.out.println(str + ": " + value);
				if((commonMaps).java_allfield_type_map.get(str.toLowerCase()) != null &&  !(commonMaps).java_allfield_type_map.get(str.toLowerCase()).equalsIgnoreCase("")
						&& !str.equalsIgnoreCase("AnyField")
						&& value != null && !value.equalsIgnoreCase(""))
				{
					if(p_searchCriteria.get(str).equals("any"))
					{
						continue;
					}

					if( i > 0)
					{
						AllFieldSql+= " AND  ";
					}
					if((commonMaps).java_allfield_type_map.get(str.toLowerCase()).equals("String"))
					{
						AllFieldSql += "" + tableName + "." + str.toLowerCase() + " like '%" + p_searchCriteria.get(str) + "%'";
					}
					else
					{
						AllFieldSql += "" + tableName + "." + str.toLowerCase() + " = '" + p_searchCriteria.get(str) + "'";
					}
					i ++;
				}
			}
			
			AllFieldSql += ")";
			System.out.println("AllFieldSql = " + AllFieldSql);
			
			
		}
		
		
		sql += " WHERE ";
		
		sql += " (" + tableName + ".isDeleted = 0 ";			
		sql += ")";
		
		
		if(!filter.equalsIgnoreCase(""))
		{
			sql += " and " + filter + " ";
		}
		
		if(!AnyfieldSql.equals("()") && !AnyfieldSql.equals(""))
		{
			sql += " AND " + AnyfieldSql;
			
		}
		if(!AllFieldSql.equals("()") && !AllFieldSql.equals(""))
		{			
			sql += " AND " + AllFieldSql;
		}
		
	
			
		sql += " order by " + tableName + ".lastModificationTime desc ";

		printSql(sql);
		
		if(limit >= 0)
		{
			sql += " limit " + limit;
		}
		if(offset >= 0)
		{
			sql += " offset " + offset;
		}
		
		System.out.println("-------------- sql = " + sql);
		
		return sql;
    }
	
	
	public String getSqlWithSearchCriteria(Hashtable p_searchCriteria, int limit, int offset, int category,
			boolean isPermanentTable, UserDTO userDTO, String filter, boolean tableHasJobCat)
    {
		if(isPermanentTable)
		{
			return getSqlWithSearchCriteriaForNormalSearch(p_searchCriteria, limit, offset, category, userDTO, filter, tableHasJobCat);
		}
		else
		{
			return getSqlWithSearchCriteriaForApprovalSearch(p_searchCriteria, limit, offset, category, userDTO, filter, tableHasJobCat);
		}
				
    }
				
}
	