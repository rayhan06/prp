package id_card_requisition;

import java.io.IOException;
import java.io.*;
import java.text.SimpleDateFormat;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import org.apache.log4j.Logger;

import login.LoginDTO;
import permission.MenuConstants;
import role.PermissionRepository;


import sessionmanager.SessionConstants;

import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;
import util.CommonRequestHandler;
import util.RecordNavigationManager4;

import java.util.*;
import javax.servlet.http.*;
import java.util.UUID;





import geolocation.GeoLocationDAO2;
import javafx.util.Pair;

import java.util.StringTokenizer;

import com.google.gson.Gson;

import pb.*;
import pbReport.*;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;

import files.*;
import workflow.WorkflowController;
import approval_execution_table.*;
import employee_records.Employee_recordsDAO;
import employee_records.Employee_recordsDTO;
import pb_notifications.Pb_notificationsDAO;



/**
 * Servlet implementation class Id_card_requisitionServlet
 */
@WebServlet("/Id_card_requisitionServlet")
@MultipartConfig
public class Id_card_requisitionServlet extends HttpServlet 
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Id_card_requisitionServlet.class);

    String tableName = "id_card_requisition";

	Id_card_requisitionDAO id_card_requisitionDAO;
	CommonRequestHandler commonRequestHandler;
	FilesDAO filesDAO = new FilesDAO();
	Employee_recordsDAO recordsDAO = new Employee_recordsDAO();
    private final Gson gson = new Gson();
    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Id_card_requisitionServlet() 
	{
        super();
    	try
    	{
			id_card_requisitionDAO = new Id_card_requisitionDAO(tableName);
			commonRequestHandler = new CommonRequestHandler(id_card_requisitionDAO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }   

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		System.out.println("In doget request = " + request);
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		try
		{
			String actionType = request.getParameter("actionType");
			if(actionType.equals("getAddPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ID_CARD_REQUISITION_ADD))
				{
					long recordsID = WorkflowController.getEmployeeRecordIDFromOrganogramID(userDTO.organogramID);
					Employee_recordsDTO recordsDto = recordsDAO.getEmployee_recordsDTOByID(recordsID);
					request.setAttribute("employee_records_dto", recordsDto);
					Pair<String,String> designations = id_card_requisitionDAO.getDesignationByRecordsID(recordsID);
					request.setAttribute("employee_designation_en", designations.getKey());
					request.setAttribute("employee_designation_bn", designations.getValue());
					Pair<String,String> officeName = id_card_requisitionDAO.getEmployeeOfficeNameByRecordsID(recordsID);
					request.setAttribute("office_name_en", officeName.getKey());
					request.setAttribute("office_name_bn", officeName.getValue());
					commonRequestHandler.getAddPage(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getEditPage"))
			{
				long recordsID = WorkflowController.getEmployeeRecordIDFromOrganogramID(userDTO.organogramID);
				Employee_recordsDTO recordsDto = recordsDAO.getEmployee_recordsDTOByID(recordsID);
				request.setAttribute("employee_records_dto", recordsDto);
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ID_CARD_REQUISITION_UPDATE))
				{
					getId_card_requisition(request, response);					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}						
			}
			else if(actionType.equals("downloadDropzoneFile"))
			{
				long id = Long.parseLong(request.getParameter("id"));

				FilesDTO filesDTO = (FilesDTO) filesDAO.getDTOByID(id);
				
				Utils.ProcessFile(request, response, filesDTO.fileTitle, filesDTO.inputStream);
				
			}
			else if(actionType.equals("DeleteFileFromDropZone"))
			{
				long id = Long.parseLong(request.getParameter("id"));
			
				
				System.out.println("In delete file");
				filesDAO.hardDeleteByID(id);
				response.getWriter().write("Deleted");
							
			}
			else if(actionType.equals("getURL"))
			{
				String URL = request.getParameter("URL");
				System.out.println("URL = " + URL);
				response.sendRedirect(URL);			
			}			
			else if(actionType.equals("search"))
			{
				System.out.println("search requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ID_CARD_REQUISITION_SEARCH))
				{
					if(isPermanentTable)
					{
						String filter = request.getParameter("filter");
						System.out.println("filter = " + filter);
						if(filter!=null)
						{
							filter = ""; //shouldn't be directly used, rather manipulate it.
							searchId_card_requisition(request, response, isPermanentTable, filter);
						}
						else
						{
							searchId_card_requisition(request, response, isPermanentTable, "");
						}
					}					
					else
					{
						//searchId_card_requisition(request, response, tempTableName, isPermanentTable);
					}
				}			
			}
			else if(actionType.equals("getApprovalPage"))
			{
				System.out.println("Id_card_requisition getApprovalPage requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ID_CARD_REQUISITION_APPROVE))
				{
					searchId_card_requisition(request, response, false, "");
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}				
			}
			else if(actionType.equals("viewApprovalNotification"))
			{
				System.out.println("view requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.APPROVAL_TEST_SEARCH))
				{
					commonRequestHandler.viewApprovalNotification(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}				
			}
			else if(actionType.equals("view"))
			{
				System.out.println("view requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ID_CARD_REQUISITION_SEARCH))
				{
					commonRequestHandler.view(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else
			{
				request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}


	
	

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		// TODO Auto-generated method stub
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		System.out.println("doPost");
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		
		try
		{
			String actionType = request.getParameter("actionType");
			System.out.println("actionType = " + actionType);
			if(actionType.equals("add"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ID_CARD_REQUISITION_ADD))
				{
					System.out.println("going to  addId_card_requisition ");
					addId_card_requisition(request, response, true, userDTO, true);
				}
				else
				{
					System.out.println("Not going to  addId_card_requisition ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else if(actionType.equals("UploadFilesFromDropZone"))
			{
				String Column = request.getParameter("columnName");
				long ColumnID = Long.parseLong(request.getParameter("ColumnID"));
				String pageType = request.getParameter("pageType");
				
				System.out.println("In " + pageType);				
				Utils.UploadFilesFromDropZone(request, response, userDTO.ID, ColumnID, "Id_card_requisitionServlet");	
			}

			else if(actionType.equals("SendToApprovalPath"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ID_CARD_REQUISITION_APPROVE))
				{
					commonRequestHandler.sendToApprovalPath(request, response, userDTO);
				}
				else
				{
					System.out.println("Not going to SendToApprovalPath ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}
			else if(actionType.equals("approve"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ID_CARD_REQUISITION_ADD))
				{					
					commonRequestHandler.approve(request, response, true, userDTO, true);
				}
				else
				{
					System.out.println("Not going to  addId_card_requisition ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else if(actionType.equals("reject"))
			{
				System.out.println("trying to approve");
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ID_CARD_REQUISITION_APPROVE))
				{					
					commonRequestHandler.approve(request, response, true, userDTO, false);
				}
				else
				{
					System.out.println("Not going to  addId_card_requisition ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}				
			}
			else if(actionType.equals("terminate"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ID_CARD_REQUISITION_ADD))
				{
					commonRequestHandler.terminate(request, response, userDTO);
				}
				else
				{
					System.out.println("Not going to  addId_card_requisition ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}
			else if(actionType.equals("skipStep"))
			{
				
				System.out.println("skipStep");
				commonRequestHandler.skipStep(request, response, userDTO);									
			}
			else if(actionType.equals("getDTO"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ID_CARD_REQUISITION_ADD))
				{
					getDTO(request, response);					
				}
				else
				{
					System.out.println("Not going to  addId_card_requisition ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else if(actionType.equals("edit"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ID_CARD_REQUISITION_UPDATE))
				{					
					addId_card_requisition(request, response, false, userDTO, isPermanentTable);					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("delete"))
			{								
				deleteId_card_requisition(request, response, userDTO);				
			}	
			else if(actionType.equals("search"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ID_CARD_REQUISITION_SEARCH))
				{
					searchId_card_requisition(request, response, true, "");
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getGeo"))
			{
				System.out.println("going to geoloc ");
				request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
			}
			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}
	
	private void getDTO(HttpServletRequest request, HttpServletResponse response) 
	{
		try 
		{
			System.out.println("In getDTO");
			Id_card_requisitionDTO id_card_requisitionDTO = (Id_card_requisitionDTO)id_card_requisitionDAO.getDTOByID(Long.parseLong(request.getParameter("ID")));
			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			
			String encoded = this.gson.toJson(id_card_requisitionDTO);
			System.out.println("json encoded data = " + encoded);
			out.print(encoded);
			out.flush();
		}
		catch (NumberFormatException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	private void addId_card_requisition(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, boolean isPermanentTable) throws IOException 
	{
		// TODO Auto-generated method stub
		try 
		{
			request.setAttribute("failureMessage", "");
			System.out.println("%%%% addId_card_requisition");
			String path = getServletContext().getRealPath("/img2/");
			Id_card_requisitionDTO id_card_requisitionDTO;
			SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
						
			if(addFlag == true)
			{
				id_card_requisitionDTO = new Id_card_requisitionDTO();
			}
			else
			{
				id_card_requisitionDTO = (Id_card_requisitionDTO)id_card_requisitionDAO.getDTOByID(Long.parseLong(request.getParameter("iD")));
			}
			
			String Value = "";

			Value = request.getParameter("employeeRecordsId");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("employeeRecordsId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				id_card_requisitionDTO.employeeRecordsId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("jobCat");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("jobCat = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				id_card_requisitionDTO.jobCat = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("isLost");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("isLost = " + Value);
            id_card_requisitionDTO.isLost = Value != null && !Value.equalsIgnoreCase("");

			Value = request.getParameter("filesDropzone");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("filesDropzone = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				
				System.out.println("filesDropzone = " + Value);
				if(Value != null && !Value.equalsIgnoreCase(""))
				{
					id_card_requisitionDTO.filesDropzone = Long.parseLong(Value);
				}
				else
				{
					System.out.println("FieldName has a null Value, not updating" + " = " + Value);
				}
				
				if(addFlag == false)
				{
					String filesDropzoneFilesToDelete = request.getParameter("filesDropzoneFilesToDelete");
					String[] deleteArray = filesDropzoneFilesToDelete.split(",");
					for(int i = 0; i < deleteArray.length; i ++)
					{
						System.out.println("going to delete " + deleteArray[i]);
						if(i>0)
						{
							filesDAO.delete(Long.parseLong(deleteArray[i]));
						}
					}
				}
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			if(addFlag)
			{
				id_card_requisitionDTO.insertedByUserId = userDTO.ID;
			}


			if(addFlag)
			{
				Calendar c = Calendar.getInstance();
				c.set(Calendar.HOUR_OF_DAY, 0);
				c.set(Calendar.MINUTE, 0);
				c.set(Calendar.SECOND, 0);
				c.set(Calendar.MILLISECOND, 0);
				
				id_card_requisitionDTO.insertionDate = c.getTimeInMillis();
			}			


			Value = request.getParameter("modifiedBy");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("modifiedBy = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				id_card_requisitionDTO.modifiedBy = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			
			System.out.println("Done adding  addId_card_requisition dto = " + id_card_requisitionDTO);
			long returnedID = -1;
			
			if(isPermanentTable == false) //add new row for validation and make the old row outdated
			{
				id_card_requisitionDAO.setIsDeleted(id_card_requisitionDTO.iD, CommonDTO.OUTDATED);
				returnedID = id_card_requisitionDAO.add(id_card_requisitionDTO);
				id_card_requisitionDAO.setIsDeleted(returnedID, CommonDTO.WAITING_FOR_APPROVAL);
			}
			else if(addFlag == true)
			{
				returnedID = id_card_requisitionDAO.manageWriteOperations(id_card_requisitionDTO, SessionConstants.INSERT, -1, userDTO);
			}
			else
			{				
				returnedID = id_card_requisitionDAO.manageWriteOperations(id_card_requisitionDTO, SessionConstants.UPDATE, -1, userDTO);											
			}
			
			
			
			
			
			
			
			
			
			if(isPermanentTable)
			{
				String inPlaceSubmit = request.getParameter("inplacesubmit");
				
				if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
				{
					getId_card_requisition(request, response, returnedID);
				}
				else
				{
					response.sendRedirect("Id_card_requisitionServlet?actionType=search");
				}
			}
			else
			{
				commonRequestHandler.validate(id_card_requisitionDAO.getDTOByID(returnedID), request, response, userDTO);
			}
					
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	



	
	
	

	private void deleteId_card_requisition(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO) throws IOException 
	{				
		try 
		{
			String[] IDsToDelete = request.getParameterValues("ID");
			for(int i = 0; i < IDsToDelete.length; i ++)
			{
				long id = Long.parseLong(IDsToDelete[i]);
				System.out.println("------ DELETING " + IDsToDelete[i]);
				
				
				Id_card_requisitionDTO id_card_requisitionDTO = (Id_card_requisitionDTO)id_card_requisitionDAO.getDTOByID(id);
				id_card_requisitionDAO.manageWriteOperations(id_card_requisitionDTO, SessionConstants.DELETE, id, userDTO);
				response.sendRedirect("Id_card_requisitionServlet?actionType=search");
				
			}			
		}
		catch (Exception ex) 
		{
			ex.printStackTrace();
		}
		
	}

	private void getId_card_requisition(HttpServletRequest request, HttpServletResponse response, long id) throws ServletException, IOException
	{
		System.out.println("in getId_card_requisition");
		Id_card_requisitionDTO id_card_requisitionDTO = null;
		try 
		{
			id_card_requisitionDTO = (Id_card_requisitionDTO)id_card_requisitionDAO.getDTOByID(id);
			boolean isPermanentTable = true;
			if(request.getParameter("isPermanentTable") != null)
			{
				isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
			}
			request.setAttribute("ID", id_card_requisitionDTO.iD);
			request.setAttribute("id_card_requisitionDTO",id_card_requisitionDTO);
			request.setAttribute("id_card_requisitionDAO",id_card_requisitionDAO);
			
			String URL= "";
			
			String inPlaceEdit = request.getParameter("inplaceedit");
			String inPlaceSubmit = request.getParameter("inplacesubmit");
			String getBodyOnly = request.getParameter("getBodyOnly");
			
			if(inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase(""))
			{
				URL = "id_card_requisition/id_card_requisitionInPlaceEdit.jsp";	
				request.setAttribute("inplaceedit","");				
			}
			else if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
			{
				URL = "id_card_requisition/id_card_requisitionSearchRow.jsp";
				request.setAttribute("inplacesubmit","");					
			}
			else
			{
				if(getBodyOnly  != null && !getBodyOnly.equalsIgnoreCase(""))
				{
					URL = "id_card_requisition/id_card_requisitionEditBody.jsp?actionType=edit";
				}
				else
				{
					URL = "id_card_requisition/id_card_requisitionEdit.jsp?actionType=edit";
				}				
			}
			
			RequestDispatcher rd = request.getRequestDispatcher(URL);
			rd.forward(request, response);
		}
		catch (NumberFormatException e) 
		{
			e.printStackTrace();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	
	private void getId_card_requisition(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		getId_card_requisition(request, response, Long.parseLong(request.getParameter("ID")));	
	}
	
	private void searchId_card_requisition(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException
	{
		System.out.println("in  searchId_card_requisition 1");
		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String ajax = request.getParameter("ajax");
		boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);
		
        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
			SessionConstants.NAV_ID_CARD_REQUISITION,
			request,
			id_card_requisitionDAO,
			SessionConstants.VIEW_ID_CARD_REQUISITION,
			SessionConstants.SEARCH_ID_CARD_REQUISITION,
			tableName,
			isPermanent,
			userDTO,
			filter,
			true);
        try
        {
			System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        }
        catch(Exception e)
        {
			System.out.println("failed to dojob" + e);
        }

		request.setAttribute("id_card_requisitionDAO",id_card_requisitionDAO);
        RequestDispatcher rd;
        if(!isPermanent)
        {
        	if(hasAjax == false)
	        {
	        	System.out.println("Going to id_card_requisition/id_card_requisitionApproval.jsp");
	        	rd = request.getRequestDispatcher("id_card_requisition/id_card_requisitionApproval.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to id_card_requisition/id_card_requisitionApprovalForm.jsp");
	        	rd = request.getRequestDispatcher("id_card_requisition/id_card_requisitionApprovalForm.jsp");
	        }
        }
        else
        {
	        if(hasAjax == false)
	        {
	        	System.out.println("Going to id_card_requisition/id_card_requisitionSearch.jsp");
	        	rd = request.getRequestDispatcher("id_card_requisition/id_card_requisitionSearch.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to id_card_requisition/id_card_requisitionSearchForm.jsp");
	        	rd = request.getRequestDispatcher("id_card_requisition/id_card_requisitionSearchForm.jsp");
	        }
        }
		rd.forward(request, response);
	}
	
}

