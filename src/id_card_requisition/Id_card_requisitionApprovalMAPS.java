package id_card_requisition;
import util.*;

public class Id_card_requisitionApprovalMAPS extends CommonMaps
{	
	public Id_card_requisitionApprovalMAPS(String tableName)
	{
		
		java_allfield_type_map.put("employee_records_id".toLowerCase(), "Long");
		java_allfield_type_map.put("is_lost".toLowerCase(), "Boolean");

		java_allfield_type_map.put("job_cat", "Integer");
		java_allfield_type_map.put("approval_status_cat", "Integer");
		java_allfield_type_map.put("initiator", "Long");
		java_allfield_type_map.put("assigned_to", "Long");
		java_allfield_type_map.put("starting_date", "Long");
		java_allfield_type_map.put("ending_date", "Long");
		
		java_table_map.put("approval_status_cat", "approval_summary");
		java_table_map.put("initiator", "approval_summary");
		java_table_map.put("assigned_to", "approval_summary");
	}

}