package id_card_requisition;
import java.util.*; 
import util.*; 


public class Id_card_requisitionDTO extends CommonDTO
{

	public long employeeRecordsId = 0;
	public boolean isLost = false;
	public long filesDropzone = 0;
	public long insertedByUserId = 0;
	public long insertionDate = 0;
    public String modifiedBy = "";
	
	
    @Override
	public String toString() {
            return "$Id_card_requisitionDTO[" +
            " iD = " + iD +
            " employeeRecordsId = " + employeeRecordsId +
            " jobCat = " + jobCat +
            " isLost = " + isLost +
            " filesDropzone = " + filesDropzone +
            " insertedByUserId = " + insertedByUserId +
            " insertionDate = " + insertionDate +
            " modifiedBy = " + modifiedBy +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}