package id_card_requisition;
import java.util.*; 
import util.*;


public class Id_card_requisitionMAPS extends CommonMaps
{	
	public Id_card_requisitionMAPS(String tableName)
	{
		
		java_allfield_type_map.put("employee_records_id".toLowerCase(), "Long");
		java_allfield_type_map.put("is_lost".toLowerCase(), "Boolean");


		java_anyfield_search_map.put(tableName + ".employee_records_id".toLowerCase(), "Long");
		java_anyfield_search_map.put(tableName + ".is_lost".toLowerCase(), "Boolean");

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("employeeRecordsId".toLowerCase(), "employeeRecordsId".toLowerCase());
		java_DTO_map.put("jobCat".toLowerCase(), "jobCat".toLowerCase());
		java_DTO_map.put("isLost".toLowerCase(), "isLost".toLowerCase());
		java_DTO_map.put("filesDropzone".toLowerCase(), "filesDropzone".toLowerCase());
		java_DTO_map.put("insertedByUserId".toLowerCase(), "insertedByUserId".toLowerCase());
		java_DTO_map.put("insertionDate".toLowerCase(), "insertionDate".toLowerCase());
		java_DTO_map.put("modifiedBy".toLowerCase(), "modifiedBy".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("employee_records_id".toLowerCase(), "employeeRecordsId".toLowerCase());
		java_SQL_map.put("is_lost".toLowerCase(), "isLost".toLowerCase());
		java_SQL_map.put("files_dropzone".toLowerCase(), "filesDropzone".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Employee Records Id".toLowerCase(), "employeeRecordsId".toLowerCase());
		java_Text_map.put("Job Cat".toLowerCase(), "jobCat".toLowerCase());
		java_Text_map.put("Is Lost".toLowerCase(), "isLost".toLowerCase());
		java_Text_map.put("Files Dropzone".toLowerCase(), "filesDropzone".toLowerCase());
		java_Text_map.put("Inserted By User Id".toLowerCase(), "insertedByUserId".toLowerCase());
		java_Text_map.put("Insertion Date".toLowerCase(), "insertionDate".toLowerCase());
		java_Text_map.put("Modified By".toLowerCase(), "modifiedBy".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}