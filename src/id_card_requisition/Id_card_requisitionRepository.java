package id_card_requisition;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class Id_card_requisitionRepository implements Repository {
	Id_card_requisitionDAO id_card_requisitionDAO = null;
	
	public void setDAO(Id_card_requisitionDAO id_card_requisitionDAO)
	{
		this.id_card_requisitionDAO = id_card_requisitionDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Id_card_requisitionRepository.class);
	Map<Long, Id_card_requisitionDTO>mapOfId_card_requisitionDTOToiD;
	Map<Long, Set<Id_card_requisitionDTO> >mapOfId_card_requisitionDTOToemployeeRecordsId;
	Map<Integer, Set<Id_card_requisitionDTO> >mapOfId_card_requisitionDTOTojobCat;
	Map<Boolean, Set<Id_card_requisitionDTO> >mapOfId_card_requisitionDTOToisLost;
	Map<Long, Set<Id_card_requisitionDTO> >mapOfId_card_requisitionDTOTofilesDropzone;
	Map<Long, Set<Id_card_requisitionDTO> >mapOfId_card_requisitionDTOToinsertedByUserId;
	Map<Long, Set<Id_card_requisitionDTO> >mapOfId_card_requisitionDTOToinsertionDate;
	Map<String, Set<Id_card_requisitionDTO> >mapOfId_card_requisitionDTOTomodifiedBy;
	Map<Long, Set<Id_card_requisitionDTO> >mapOfId_card_requisitionDTOTolastModificationTime;


	static Id_card_requisitionRepository instance = null;  
	private Id_card_requisitionRepository(){
		mapOfId_card_requisitionDTOToiD = new ConcurrentHashMap<>();
		mapOfId_card_requisitionDTOToemployeeRecordsId = new ConcurrentHashMap<>();
		mapOfId_card_requisitionDTOTojobCat = new ConcurrentHashMap<>();
		mapOfId_card_requisitionDTOToisLost = new ConcurrentHashMap<>();
		mapOfId_card_requisitionDTOTofilesDropzone = new ConcurrentHashMap<>();
		mapOfId_card_requisitionDTOToinsertedByUserId = new ConcurrentHashMap<>();
		mapOfId_card_requisitionDTOToinsertionDate = new ConcurrentHashMap<>();
		mapOfId_card_requisitionDTOTomodifiedBy = new ConcurrentHashMap<>();
		mapOfId_card_requisitionDTOTolastModificationTime = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Id_card_requisitionRepository getInstance(){
		if (instance == null){
			instance = new Id_card_requisitionRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(id_card_requisitionDAO == null)
		{
			return;
		}
		try {
			List<Id_card_requisitionDTO> id_card_requisitionDTOs = id_card_requisitionDAO.getAllId_card_requisition(reloadAll);
			for(Id_card_requisitionDTO id_card_requisitionDTO : id_card_requisitionDTOs) {
				Id_card_requisitionDTO oldId_card_requisitionDTO = getId_card_requisitionDTOByID(id_card_requisitionDTO.iD);
				if( oldId_card_requisitionDTO != null ) {
					mapOfId_card_requisitionDTOToiD.remove(oldId_card_requisitionDTO.iD);
				
					if(mapOfId_card_requisitionDTOToemployeeRecordsId.containsKey(oldId_card_requisitionDTO.employeeRecordsId)) {
						mapOfId_card_requisitionDTOToemployeeRecordsId.get(oldId_card_requisitionDTO.employeeRecordsId).remove(oldId_card_requisitionDTO);
					}
					if(mapOfId_card_requisitionDTOToemployeeRecordsId.get(oldId_card_requisitionDTO.employeeRecordsId).isEmpty()) {
						mapOfId_card_requisitionDTOToemployeeRecordsId.remove(oldId_card_requisitionDTO.employeeRecordsId);
					}
					
					if(mapOfId_card_requisitionDTOTojobCat.containsKey(oldId_card_requisitionDTO.jobCat)) {
						mapOfId_card_requisitionDTOTojobCat.get(oldId_card_requisitionDTO.jobCat).remove(oldId_card_requisitionDTO);
					}
					if(mapOfId_card_requisitionDTOTojobCat.get(oldId_card_requisitionDTO.jobCat).isEmpty()) {
						mapOfId_card_requisitionDTOTojobCat.remove(oldId_card_requisitionDTO.jobCat);
					}
					
					if(mapOfId_card_requisitionDTOToisLost.containsKey(oldId_card_requisitionDTO.isLost)) {
						mapOfId_card_requisitionDTOToisLost.get(oldId_card_requisitionDTO.isLost).remove(oldId_card_requisitionDTO);
					}
					if(mapOfId_card_requisitionDTOToisLost.get(oldId_card_requisitionDTO.isLost).isEmpty()) {
						mapOfId_card_requisitionDTOToisLost.remove(oldId_card_requisitionDTO.isLost);
					}
					
					if(mapOfId_card_requisitionDTOTofilesDropzone.containsKey(oldId_card_requisitionDTO.filesDropzone)) {
						mapOfId_card_requisitionDTOTofilesDropzone.get(oldId_card_requisitionDTO.filesDropzone).remove(oldId_card_requisitionDTO);
					}
					if(mapOfId_card_requisitionDTOTofilesDropzone.get(oldId_card_requisitionDTO.filesDropzone).isEmpty()) {
						mapOfId_card_requisitionDTOTofilesDropzone.remove(oldId_card_requisitionDTO.filesDropzone);
					}
					
					if(mapOfId_card_requisitionDTOToinsertedByUserId.containsKey(oldId_card_requisitionDTO.insertedByUserId)) {
						mapOfId_card_requisitionDTOToinsertedByUserId.get(oldId_card_requisitionDTO.insertedByUserId).remove(oldId_card_requisitionDTO);
					}
					if(mapOfId_card_requisitionDTOToinsertedByUserId.get(oldId_card_requisitionDTO.insertedByUserId).isEmpty()) {
						mapOfId_card_requisitionDTOToinsertedByUserId.remove(oldId_card_requisitionDTO.insertedByUserId);
					}
					
					if(mapOfId_card_requisitionDTOToinsertionDate.containsKey(oldId_card_requisitionDTO.insertionDate)) {
						mapOfId_card_requisitionDTOToinsertionDate.get(oldId_card_requisitionDTO.insertionDate).remove(oldId_card_requisitionDTO);
					}
					if(mapOfId_card_requisitionDTOToinsertionDate.get(oldId_card_requisitionDTO.insertionDate).isEmpty()) {
						mapOfId_card_requisitionDTOToinsertionDate.remove(oldId_card_requisitionDTO.insertionDate);
					}
					
					if(mapOfId_card_requisitionDTOTomodifiedBy.containsKey(oldId_card_requisitionDTO.modifiedBy)) {
						mapOfId_card_requisitionDTOTomodifiedBy.get(oldId_card_requisitionDTO.modifiedBy).remove(oldId_card_requisitionDTO);
					}
					if(mapOfId_card_requisitionDTOTomodifiedBy.get(oldId_card_requisitionDTO.modifiedBy).isEmpty()) {
						mapOfId_card_requisitionDTOTomodifiedBy.remove(oldId_card_requisitionDTO.modifiedBy);
					}
					
					if(mapOfId_card_requisitionDTOTolastModificationTime.containsKey(oldId_card_requisitionDTO.lastModificationTime)) {
						mapOfId_card_requisitionDTOTolastModificationTime.get(oldId_card_requisitionDTO.lastModificationTime).remove(oldId_card_requisitionDTO);
					}
					if(mapOfId_card_requisitionDTOTolastModificationTime.get(oldId_card_requisitionDTO.lastModificationTime).isEmpty()) {
						mapOfId_card_requisitionDTOTolastModificationTime.remove(oldId_card_requisitionDTO.lastModificationTime);
					}
					
					
				}
				if(id_card_requisitionDTO.isDeleted == 0) 
				{
					
					mapOfId_card_requisitionDTOToiD.put(id_card_requisitionDTO.iD, id_card_requisitionDTO);
				
					if( ! mapOfId_card_requisitionDTOToemployeeRecordsId.containsKey(id_card_requisitionDTO.employeeRecordsId)) {
						mapOfId_card_requisitionDTOToemployeeRecordsId.put(id_card_requisitionDTO.employeeRecordsId, new HashSet<>());
					}
					mapOfId_card_requisitionDTOToemployeeRecordsId.get(id_card_requisitionDTO.employeeRecordsId).add(id_card_requisitionDTO);
					
					if( ! mapOfId_card_requisitionDTOTojobCat.containsKey(id_card_requisitionDTO.jobCat)) {
						mapOfId_card_requisitionDTOTojobCat.put(id_card_requisitionDTO.jobCat, new HashSet<>());
					}
					mapOfId_card_requisitionDTOTojobCat.get(id_card_requisitionDTO.jobCat).add(id_card_requisitionDTO);
					
					if( ! mapOfId_card_requisitionDTOToisLost.containsKey(id_card_requisitionDTO.isLost)) {
						mapOfId_card_requisitionDTOToisLost.put(id_card_requisitionDTO.isLost, new HashSet<>());
					}
					mapOfId_card_requisitionDTOToisLost.get(id_card_requisitionDTO.isLost).add(id_card_requisitionDTO);
					
					if( ! mapOfId_card_requisitionDTOTofilesDropzone.containsKey(id_card_requisitionDTO.filesDropzone)) {
						mapOfId_card_requisitionDTOTofilesDropzone.put(id_card_requisitionDTO.filesDropzone, new HashSet<>());
					}
					mapOfId_card_requisitionDTOTofilesDropzone.get(id_card_requisitionDTO.filesDropzone).add(id_card_requisitionDTO);
					
					if( ! mapOfId_card_requisitionDTOToinsertedByUserId.containsKey(id_card_requisitionDTO.insertedByUserId)) {
						mapOfId_card_requisitionDTOToinsertedByUserId.put(id_card_requisitionDTO.insertedByUserId, new HashSet<>());
					}
					mapOfId_card_requisitionDTOToinsertedByUserId.get(id_card_requisitionDTO.insertedByUserId).add(id_card_requisitionDTO);
					
					if( ! mapOfId_card_requisitionDTOToinsertionDate.containsKey(id_card_requisitionDTO.insertionDate)) {
						mapOfId_card_requisitionDTOToinsertionDate.put(id_card_requisitionDTO.insertionDate, new HashSet<>());
					}
					mapOfId_card_requisitionDTOToinsertionDate.get(id_card_requisitionDTO.insertionDate).add(id_card_requisitionDTO);
					
					if( ! mapOfId_card_requisitionDTOTomodifiedBy.containsKey(id_card_requisitionDTO.modifiedBy)) {
						mapOfId_card_requisitionDTOTomodifiedBy.put(id_card_requisitionDTO.modifiedBy, new HashSet<>());
					}
					mapOfId_card_requisitionDTOTomodifiedBy.get(id_card_requisitionDTO.modifiedBy).add(id_card_requisitionDTO);
					
					if( ! mapOfId_card_requisitionDTOTolastModificationTime.containsKey(id_card_requisitionDTO.lastModificationTime)) {
						mapOfId_card_requisitionDTOTolastModificationTime.put(id_card_requisitionDTO.lastModificationTime, new HashSet<>());
					}
					mapOfId_card_requisitionDTOTolastModificationTime.get(id_card_requisitionDTO.lastModificationTime).add(id_card_requisitionDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Id_card_requisitionDTO> getId_card_requisitionList() {
		List <Id_card_requisitionDTO> id_card_requisitions = new ArrayList<Id_card_requisitionDTO>(this.mapOfId_card_requisitionDTOToiD.values());
		return id_card_requisitions;
	}
	
	
	public Id_card_requisitionDTO getId_card_requisitionDTOByID( long ID){
		return mapOfId_card_requisitionDTOToiD.get(ID);
	}
	
	
	public List<Id_card_requisitionDTO> getId_card_requisitionDTOByemployee_records_id(long employee_records_id) {
		return new ArrayList<>( mapOfId_card_requisitionDTOToemployeeRecordsId.getOrDefault(employee_records_id,new HashSet<>()));
	}
	
	
	public List<Id_card_requisitionDTO> getId_card_requisitionDTOByjob_cat(int job_cat) {
		return new ArrayList<>( mapOfId_card_requisitionDTOTojobCat.getOrDefault(job_cat,new HashSet<>()));
	}
	
	
	public List<Id_card_requisitionDTO> getId_card_requisitionDTOByis_lost(boolean is_lost) {
		return new ArrayList<>( mapOfId_card_requisitionDTOToisLost.getOrDefault(is_lost,new HashSet<>()));
	}
	
	
	public List<Id_card_requisitionDTO> getId_card_requisitionDTOByfiles_dropzone(long files_dropzone) {
		return new ArrayList<>( mapOfId_card_requisitionDTOTofilesDropzone.getOrDefault(files_dropzone,new HashSet<>()));
	}
	
	
	public List<Id_card_requisitionDTO> getId_card_requisitionDTOByinserted_by_user_id(long inserted_by_user_id) {
		return new ArrayList<>( mapOfId_card_requisitionDTOToinsertedByUserId.getOrDefault(inserted_by_user_id,new HashSet<>()));
	}
	
	
	public List<Id_card_requisitionDTO> getId_card_requisitionDTOByinsertion_date(long insertion_date) {
		return new ArrayList<>( mapOfId_card_requisitionDTOToinsertionDate.getOrDefault(insertion_date,new HashSet<>()));
	}
	
	
	public List<Id_card_requisitionDTO> getId_card_requisitionDTOBymodified_by(String modified_by) {
		return new ArrayList<>( mapOfId_card_requisitionDTOTomodifiedBy.getOrDefault(modified_by,new HashSet<>()));
	}
	
	
	public List<Id_card_requisitionDTO> getId_card_requisitionDTOBylastModificationTime(long lastModificationTime) {
		return new ArrayList<>( mapOfId_card_requisitionDTOTolastModificationTime.getOrDefault(lastModificationTime,new HashSet<>()));
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "id_card_requisition";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


