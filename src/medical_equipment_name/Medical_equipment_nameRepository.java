package medical_equipment_name;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class Medical_equipment_nameRepository implements Repository {
	Medical_equipment_nameDAO medical_equipment_nameDAO = null;
	
	public void setDAO(Medical_equipment_nameDAO medical_equipment_nameDAO)
	{
		this.medical_equipment_nameDAO = medical_equipment_nameDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Medical_equipment_nameRepository.class);
	Map<Long, Medical_equipment_nameDTO>mapOfMedical_equipment_nameDTOToiD;


	static Medical_equipment_nameRepository instance = null;  
	private Medical_equipment_nameRepository(){
		mapOfMedical_equipment_nameDTOToiD = new ConcurrentHashMap<>();
	

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Medical_equipment_nameRepository getInstance(){
		if (instance == null){
			instance = new Medical_equipment_nameRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(medical_equipment_nameDAO == null)
		{
			medical_equipment_nameDAO = new Medical_equipment_nameDAO();
		}
		try {
			List<Medical_equipment_nameDTO> medical_equipment_nameDTOs = medical_equipment_nameDAO.getAllMedical_equipment_name(reloadAll);
			for(Medical_equipment_nameDTO medical_equipment_nameDTO : medical_equipment_nameDTOs) {
				Medical_equipment_nameDTO oldMedical_equipment_nameDTO = getMedical_equipment_nameDTOByID(medical_equipment_nameDTO.iD);
				if( oldMedical_equipment_nameDTO != null ) {
					mapOfMedical_equipment_nameDTOToiD.remove(oldMedical_equipment_nameDTO.iD);
				
					
					
					
				}
				if(medical_equipment_nameDTO.isDeleted == 0) 
				{
					
					mapOfMedical_equipment_nameDTOToiD.put(medical_equipment_nameDTO.iD, medical_equipment_nameDTO);
				
			
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Medical_equipment_nameDTO> getMedical_equipment_nameList() {
		List <Medical_equipment_nameDTO> medical_equipment_names = new ArrayList<Medical_equipment_nameDTO>(this.mapOfMedical_equipment_nameDTOToiD.values());
		return medical_equipment_names;
	}
	
	
	public Medical_equipment_nameDTO getMedical_equipment_nameDTOByID( long ID){
		return mapOfMedical_equipment_nameDTOToiD.get(ID);
	}
	
	
	

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "medical_equipment_name";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}

