package medical_equipment_name;
import java.util.*; 
import util.*; 


public class Medical_equipment_nameDTO extends CommonDTO
{

    public String nameEn = "";
    public String nameBn = "";
    public String quantityUnit = "";
	public int currentStock = 0;
	public boolean stockAlertRequired = false;
	public int stockAlertQuantity = 0;
	public long insertedByUserId = -1;
	public long insertedByOrganogramId = -1;
	public long insertionDate = -1;
	public int medicalEquipmentCat = -1;
	public int medicalDeptCat = -1;
	public double unitPrice = 0;
	
	public static final int XRAY_CAT = 0;
	
	
    @Override
	public String toString() {
            return "$Medical_equipment_nameDTO[" +
            " iD = " + iD +
            " nameEn = " + nameEn +
            " nameBn = " + nameBn +
            " quantityUnit = " + quantityUnit +
            " currentStock = " + currentStock +
            " stockAlertRequired = " + stockAlertRequired +
            " stockAlertQuantity = " + stockAlertQuantity +
            " searchColumn = " + searchColumn +
            " insertedByUserId = " + insertedByUserId +
            " insertedByOrganogramId = " + insertedByOrganogramId +
            " insertionDate = " + insertionDate +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}