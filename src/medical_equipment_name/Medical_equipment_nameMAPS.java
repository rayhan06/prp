package medical_equipment_name;
import java.util.*; 
import util.*;


public class Medical_equipment_nameMAPS extends CommonMaps
{	
	public Medical_equipment_nameMAPS(String tableName)
	{
		

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("nameEn".toLowerCase(), "nameEn".toLowerCase());
		java_DTO_map.put("nameBn".toLowerCase(), "nameBn".toLowerCase());
		java_DTO_map.put("quantityUnit".toLowerCase(), "quantityUnit".toLowerCase());
		java_DTO_map.put("currentStock".toLowerCase(), "currentStock".toLowerCase());
		java_DTO_map.put("stockAlertRequired".toLowerCase(), "stockAlertRequired".toLowerCase());
		java_DTO_map.put("stockAlertQuantity".toLowerCase(), "stockAlertQuantity".toLowerCase());
		java_DTO_map.put("searchColumn".toLowerCase(), "searchColumn".toLowerCase());
		java_DTO_map.put("insertedByUserId".toLowerCase(), "insertedByUserId".toLowerCase());
		java_DTO_map.put("insertedByOrganogramId".toLowerCase(), "insertedByOrganogramId".toLowerCase());
		java_DTO_map.put("insertionDate".toLowerCase(), "insertionDate".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("name_en".toLowerCase(), "nameEn".toLowerCase());
		java_SQL_map.put("name_bn".toLowerCase(), "nameBn".toLowerCase());
		java_SQL_map.put("quantity_unit".toLowerCase(), "quantityUnit".toLowerCase());
		java_SQL_map.put("current_stock".toLowerCase(), "currentStock".toLowerCase());
		java_SQL_map.put("stock_alert_required".toLowerCase(), "stockAlertRequired".toLowerCase());
		java_SQL_map.put("stock_alert_quantity".toLowerCase(), "stockAlertQuantity".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("English Name".toLowerCase(), "nameEn".toLowerCase());
		java_Text_map.put("Bangla Name".toLowerCase(), "nameBn".toLowerCase());
		java_Text_map.put("Quantity Unit".toLowerCase(), "quantityUnit".toLowerCase());
		java_Text_map.put("Current Stock".toLowerCase(), "currentStock".toLowerCase());
		java_Text_map.put("Stock Alert Required".toLowerCase(), "stockAlertRequired".toLowerCase());
		java_Text_map.put("Stock Alert Quantity".toLowerCase(), "stockAlertQuantity".toLowerCase());
		java_Text_map.put("Search Column".toLowerCase(), "searchColumn".toLowerCase());
		java_Text_map.put("Inserted By User Id".toLowerCase(), "insertedByUserId".toLowerCase());
		java_Text_map.put("Inserted By Organogram Id".toLowerCase(), "insertedByOrganogramId".toLowerCase());
		java_Text_map.put("Insertion Date".toLowerCase(), "insertionDate".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}