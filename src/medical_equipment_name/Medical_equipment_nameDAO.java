package medical_equipment_name;


import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.*;

import java.sql.SQLException;
import common.ConnectionAndStatementUtil;


import org.apache.log4j.Logger;


import repository.RepositoryManager;
import sessionmanager.SessionConstants;
import util.*;

import user.UserDTO;

public class Medical_equipment_nameDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());

	public Medical_equipment_nameDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";
		commonMaps = new Medical_equipment_nameMAPS(tableName);
		columnNames = new String[] 
		{
			"ID",
			"name_en",
			"name_bn",
			"quantity_unit",
			"current_stock",
			"stock_alert_required",
			"stock_alert_quantity",
			"medical_dept_cat",
			"medical_equipment_cat",
			"unit_price",
			"search_column",
			"inserted_by_user_id",
			"inserted_by_organogram_id",
			"insertion_date",
			"isDeleted",
			"lastModificationTime"
		};
	}
	
	public Medical_equipment_nameDAO()
	{
		this("medical_equipment_name");		
	}
	
	public void setSearchColumn(Medical_equipment_nameDTO medical_equipment_nameDTO)
	{
		medical_equipment_nameDTO.searchColumn = "";
		medical_equipment_nameDTO.searchColumn += medical_equipment_nameDTO.nameEn + " ";
		medical_equipment_nameDTO.searchColumn += medical_equipment_nameDTO.nameBn + " ";
		medical_equipment_nameDTO.searchColumn += medical_equipment_nameDTO.quantityUnit + " ";
	}
	
	public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException
	{
		Medical_equipment_nameDTO medical_equipment_nameDTO = (Medical_equipment_nameDTO)commonDTO;
		int index = 1;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(medical_equipment_nameDTO);
		if(isInsert)
		{
			ps.setObject(index++,medical_equipment_nameDTO.iD);
		}
		ps.setObject(index++,medical_equipment_nameDTO.nameEn);
		ps.setObject(index++,medical_equipment_nameDTO.nameBn);
		ps.setObject(index++,medical_equipment_nameDTO.quantityUnit);
		ps.setObject(index++,medical_equipment_nameDTO.currentStock);
		ps.setObject(index++,medical_equipment_nameDTO.stockAlertRequired);
		ps.setObject(index++,medical_equipment_nameDTO.stockAlertQuantity);
		ps.setObject(index++,medical_equipment_nameDTO.medicalDeptCat);
		ps.setObject(index++,medical_equipment_nameDTO.medicalEquipmentCat);
		ps.setObject(index++,medical_equipment_nameDTO.unitPrice);
		ps.setObject(index++,medical_equipment_nameDTO.searchColumn);
		ps.setObject(index++,medical_equipment_nameDTO.insertedByUserId);
		ps.setObject(index++,medical_equipment_nameDTO.insertedByOrganogramId);
		ps.setObject(index++,medical_equipment_nameDTO.insertionDate);
		if(isInsert)
		{
			ps.setObject(index++, 0);
			ps.setObject(index++, lastModificationTime);
		}
	}
	
	public Medical_equipment_nameDTO build(ResultSet rs)
	{
		try
		{
			Medical_equipment_nameDTO medical_equipment_nameDTO = new Medical_equipment_nameDTO();
			medical_equipment_nameDTO.iD = rs.getLong("ID");
			medical_equipment_nameDTO.nameEn = rs.getString("name_en");
			medical_equipment_nameDTO.nameBn = rs.getString("name_bn");
			medical_equipment_nameDTO.quantityUnit = rs.getString("quantity_unit");
			medical_equipment_nameDTO.currentStock = rs.getInt("current_stock");
			medical_equipment_nameDTO.stockAlertRequired = rs.getBoolean("stock_alert_required");
			medical_equipment_nameDTO.stockAlertQuantity = rs.getInt("stock_alert_quantity");
			medical_equipment_nameDTO.medicalDeptCat = rs.getInt("medical_dept_cat");
			medical_equipment_nameDTO.medicalEquipmentCat = rs.getInt("medical_equipment_cat");
			medical_equipment_nameDTO.unitPrice = rs.getDouble("unit_price");
			medical_equipment_nameDTO.searchColumn = rs.getString("search_column");
			medical_equipment_nameDTO.insertedByUserId = rs.getLong("inserted_by_user_id");
			medical_equipment_nameDTO.insertedByOrganogramId = rs.getLong("inserted_by_organogram_id");
			medical_equipment_nameDTO.insertionDate = rs.getLong("insertion_date");
			medical_equipment_nameDTO.isDeleted = rs.getInt("isDeleted");
			medical_equipment_nameDTO.lastModificationTime = rs.getLong("lastModificationTime");
			return medical_equipment_nameDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
	
	

	public Medical_equipment_nameDTO getDTOByID (long id)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID = " + id;		
		return ConnectionAndStatementUtil.getT(sql,this::build);
	}

	
	public List<Medical_equipment_nameDTO> getDTOs(Collection recordIDs)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID IN ( ";
		for(int i = 0;i<recordIDs.size();i++){
			if(i!=0){
				sql+=",";
			}
			sql+=((ArrayList)recordIDs).get(i);
		}
		sql+=")  order by lastModificationTime desc";
		printSql(sql);
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);		
	}
	
	public List<Medical_equipment_nameDTO> getDTOs(int medicalDeptCat, int medicalEquipmentCat)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE isDeleted = 0";
		if(medicalDeptCat != -1)
		{
			sql+= "  and medical_dept_cat = " + medicalDeptCat;
		}
		if(medicalEquipmentCat != -1)
		{
			sql+= "  and medical_equipment_cat = " + medicalEquipmentCat;
		}
		
		sql+= "  order by id asc";
		printSql(sql);
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);		
	}
	
	public List<Medical_equipment_nameDTO> getAllMedical_equipment_name (boolean isFirstReload)
    {
		String sql = "SELECT * FROM " + tableName + " WHERE ";
		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by " + tableName + ".lastModificationTime desc";
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);	
    }
	
	public List<Medical_equipment_nameDTO> getXrayPlates ()
    {
		String sql = "SELECT * FROM " + tableName + " WHERE ";
		
		sql+=" isDeleted =  0 and medical_equipment_cat = " + SessionConstants.XRAY_CAT;
		
		sql += " order by " + tableName + ".lastModificationTime desc";
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);	
    }

	
	public List<Medical_equipment_nameDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO)
	{
		return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
	}

	
	public List<Medical_equipment_nameDTO> getDTOs(Hashtable  p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
			String filter, boolean tableHasJobCat)
	{
		String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat);
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);			
	}
	public String getSqlWithSearchCriteriaForNormalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
			UserDTO userDTO, String filter, boolean tableHasJobCat)
    {
		boolean viewAll = false;
		
		if(p_searchCriteria != null && p_searchCriteria.get("ViewAll") != null)
		{
			System.out.println("ViewAll = " + p_searchCriteria.get("ViewAll"));
			viewAll = true;
		}
		else
		{
			System.out.println("ViewAll is null ");
		}
		
		String sql = "SELECT ";
		if(category == GETIDS)
		{
			sql += " " + tableName + ".ID as ID ";
		}
		else if(category == GETCOUNT)
		{
			sql += " count( " + tableName + ".ID) as countID ";
		}
		else if(category == GETDTOS)
		{
			sql += " distinct " + tableName + ".* ";
		}
		sql += "FROM " + tableName + " ";
		sql += joinSQL;
		

		
		String AnyfieldSql = "";
		String AllFieldSql = "";
		
		if(p_searchCriteria != null)
		{
						
			
			Enumeration names = p_searchCriteria.keys();
			String str, value;
			
			AnyfieldSql = "(";
			
			if(p_searchCriteria.get("AnyField")!= null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase(""))
			{
				AnyfieldSql+= tableName + ".search_column like '%" + p_searchCriteria.get("AnyField").toString() + "%'";			
			}
			AnyfieldSql += ")";
			System.out.println("AnyfieldSql = " + AnyfieldSql);
			
			AllFieldSql = "(";
			int i = 0;
			while(names.hasMoreElements())
			{				
				str = (String) names.nextElement();
				value = (String)p_searchCriteria.get(str);
		        System.out.println(str + ": " + value);
				if(value != null && !value.equalsIgnoreCase("") && (
						 str.equals("name_en")
						|| str.equals("name_bn")
						|| str.equals("quantity_unit")
						|| str.equals("insertion_date_start")
						|| str.equals("insertion_date_end")
				)
						
				)
				{
					if(p_searchCriteria.get(str).equals("any"))
					{
						continue;
					}

					if( i > 0)
					{
						AllFieldSql+= " AND  ";
					}
					
					 if(str.equals("name_en"))
					{
						AllFieldSql += "" + tableName + ".name_en like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("name_bn"))
					{
						AllFieldSql += "" + tableName + ".name_bn like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("quantity_unit"))
					{
						AllFieldSql += "" + tableName + ".quantity_unit like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("insertion_date_start"))
					{
						AllFieldSql += "" + tableName + ".insertion_date >= " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("insertion_date_end"))
					{
						AllFieldSql += "" + tableName + ".insertion_date <= " + p_searchCriteria.get(str);
						i ++;
					}
					
					
				}
			}
			
			AllFieldSql += ")";
			System.out.println("AllFieldSql = " + AllFieldSql);
			
			
		}
		
		
		sql += " WHERE ";
		
		sql += " (" + tableName + ".isDeleted = 0 ";			
		sql += ")";
		
		
		if(!filter.equalsIgnoreCase(""))
		{
			sql += " and " + filter + " ";
		}
		
		if(!AnyfieldSql.equals("()") && !AnyfieldSql.equals(""))
		{
			sql += " AND " + AnyfieldSql;
			
		}
		if(!AllFieldSql.equals("()") && !AllFieldSql.equals(""))
		{			
			sql += " AND " + AllFieldSql;
		}
		
	
			
		sql += " order by " + tableName + ".lastModificationTime desc ";

		printSql(sql);
		
		if(limit >= 0)
		{
			sql += " limit " + limit;
		}
		if(offset >= 0)
		{
			sql += " offset " + offset;
		}
		
		System.out.println("-------------- sql = " + sql);
		
		return sql;
    }
	
	public String getSqlWithSearchCriteria(Hashtable p_searchCriteria, int limit, int offset, int category,
			boolean isPermanentTable, UserDTO userDTO, String filter, boolean tableHasJobCat)
    {		
		return getSqlWithSearchCriteriaForNormalSearch(p_searchCriteria, limit, offset, category, userDTO, filter, tableHasJobCat);	
    }
				
}
	