package medical_equipment_name;

import java.io.IOException;
import java.io.*;
import java.text.SimpleDateFormat;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import org.apache.log4j.Logger;

import login.LoginDTO;
import permission.MenuConstants;
import role.PermissionRepository;


import sessionmanager.SessionConstants;

import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;
import util.CommonRequestHandler;
import util.RecordNavigationManager4;

import java.util.*;
import javax.servlet.http.*;
import java.util.UUID;





import geolocation.GeoLocationDAO2;
import language.LC;
import language.LM;

import java.util.StringTokenizer;

import com.google.gson.Gson;

import pb.*;
import pbReport.*;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;




/**
 * Servlet implementation class Medical_equipment_nameServlet
 */
@WebServlet("/Medical_equipment_nameServlet")
@MultipartConfig
public class Medical_equipment_nameServlet extends HttpServlet 
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Medical_equipment_nameServlet.class);

    String tableName = "medical_equipment_name";

	Medical_equipment_nameDAO medical_equipment_nameDAO;
	CommonRequestHandler commonRequestHandler;
    private final Gson gson = new Gson();
    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Medical_equipment_nameServlet() 
	{
        super();
    	try
    	{
			medical_equipment_nameDAO = new Medical_equipment_nameDAO(tableName);
			commonRequestHandler = new CommonRequestHandler(medical_equipment_nameDAO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }   

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		System.out.println("In doget request = " + request);
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		try
		{
			String actionType = request.getParameter("actionType");
			if(actionType.equals("getAddPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.MEDICAL_EQUIPMENT_NAME_ADD))
				{
					commonRequestHandler.getAddPage(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getEditPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.MEDICAL_EQUIPMENT_NAME_UPDATE))
				{
					getMedical_equipment_name(request, response);					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}						
			}
			else if(actionType.equals("getURL"))
			{
				String URL = request.getParameter("URL");
				System.out.println("URL = " + URL);
				response.sendRedirect(URL);			
			}	
			else if(actionType.equals("getEquipments"))
			{
				int medicalEquipmentCat = Integer.parseInt(request.getParameter("medicalEquipmentCat"));
				int medicalDeptCat = Integer.parseInt(request.getParameter("medicalDeptCat"));
				System.out.println("medicalEquipmentCat = " + medicalEquipmentCat + " medicalDeptCat = " + medicalDeptCat);
				List<Medical_equipment_nameDTO> medical_equipment_nameDTOs = medical_equipment_nameDAO.getDTOs(medicalDeptCat, medicalEquipmentCat);
				String options = "<option value = '-1'>" + LM.getText(LC.HM_SELECT, loginDTO) + "</option>";
				boolean isLangEng = LM.getLanguage(userDTO).equalsIgnoreCase("english");
				if(medical_equipment_nameDTOs != null)
				{
					for(Medical_equipment_nameDTO medical_equipment_nameDTO: medical_equipment_nameDTOs)
					{
						if(isLangEng)
						{
							options += "<option value = '" + medical_equipment_nameDTO.iD + "'>" + medical_equipment_nameDTO.nameEn + "</option>";
						}
						else
						{
							options += "<option value = '" + medical_equipment_nameDTO.iD + "'>" + medical_equipment_nameDTO.nameBn + "</option>";
						}
					}
				}
				response.getWriter().write(options);
			}
			else if(actionType.equals("search"))
			{
				System.out.println("search requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.MEDICAL_EQUIPMENT_NAME_SEARCH))
				{
					if(isPermanentTable)
					{
						String filter = request.getParameter("filter");
						System.out.println("filter = " + filter);
						if(filter!=null)
						{
							filter = ""; //shouldn't be directly used, rather manipulate it.
							searchMedical_equipment_name(request, response, isPermanentTable, filter);
						}
						else
						{
							searchMedical_equipment_name(request, response, isPermanentTable, "");
						}
					}					
					else
					{
						//searchMedical_equipment_name(request, response, tempTableName, isPermanentTable);
					}
				}			
			}
			else if(actionType.equals("view"))
			{
				System.out.println("view requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.MEDICAL_EQUIPMENT_NAME_SEARCH))
				{
					commonRequestHandler.view(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else
			{
				request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}


	
	

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		// TODO Auto-generated method stub
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		System.out.println("doPost");
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		
		try
		{
			String actionType = request.getParameter("actionType");
			System.out.println("actionType = " + actionType);
			if(actionType.equals("add"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.MEDICAL_EQUIPMENT_NAME_ADD))
				{
					System.out.println("going to  addMedical_equipment_name ");
					addMedical_equipment_name(request, response, true, userDTO, true);
				}
				else
				{
					System.out.println("Not going to  addMedical_equipment_name ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}

			else if(actionType.equals("getDTO"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.MEDICAL_EQUIPMENT_NAME_ADD))
				{
					getDTO(request, response);					
				}
				else
				{
					System.out.println("Not going to  addMedical_equipment_name ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else if(actionType.equals("edit"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.MEDICAL_EQUIPMENT_NAME_UPDATE))
				{					
					addMedical_equipment_name(request, response, false, userDTO, isPermanentTable);					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("delete"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.MEDICAL_EQUIPMENT_NAME_UPDATE))
				{
					commonRequestHandler.delete(request, response, userDTO);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}	
			else if(actionType.equals("search"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.MEDICAL_EQUIPMENT_NAME_SEARCH))
				{
					searchMedical_equipment_name(request, response, true, "");
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getGeo"))
			{
				System.out.println("going to geoloc ");
				request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
			}
			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}
	
	private void getDTO(HttpServletRequest request, HttpServletResponse response) 
	{
		try 
		{
			System.out.println("In getDTO");
			Medical_equipment_nameDTO medical_equipment_nameDTO = medical_equipment_nameDAO.getDTOByID(Long.parseLong(request.getParameter("ID")));
			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			
			String encoded = this.gson.toJson(medical_equipment_nameDTO);
			System.out.println("json encoded data = " + encoded);
			out.print(encoded);
			out.flush();
		}
		catch (NumberFormatException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	private void addMedical_equipment_name(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, boolean isPermanentTable) throws IOException 
	{
		// TODO Auto-generated method stub
		try 
		{
			request.setAttribute("failureMessage", "");
			System.out.println("%%%% addMedical_equipment_name");
			String path = getServletContext().getRealPath("/img2/");
			Medical_equipment_nameDTO medical_equipment_nameDTO;
			SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
						
			if(addFlag == true)
			{
				medical_equipment_nameDTO = new Medical_equipment_nameDTO();
			}
			else
			{
				medical_equipment_nameDTO = medical_equipment_nameDAO.getDTOByID(Long.parseLong(request.getParameter("iD")));
			}
			
			String Value = "";

			Value = request.getParameter("nameEn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("nameEn = " + Value);
			if(Value != null)
			{
				medical_equipment_nameDTO.nameEn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("nameBn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("nameBn = " + Value);
			if(Value != null)
			{
				medical_equipment_nameDTO.nameBn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("quantityUnit");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("quantityUnit = " + Value);
			if(Value != null)
			{
				medical_equipment_nameDTO.quantityUnit = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			
			
			Value = request.getParameter("medicalDeptCat");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("medicalDeptCat = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				medical_equipment_nameDTO.medicalDeptCat = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			
			Value = request.getParameter("medicalEquipmentCat");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("medicalEquipmentCat = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				medical_equipment_nameDTO.medicalEquipmentCat = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("stockAlertRequired");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("stockAlertRequired = " + Value);
            medical_equipment_nameDTO.stockAlertRequired = Value != null && !Value.equalsIgnoreCase("");

			Value = request.getParameter("stockAlertQuantity");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("stockAlertQuantity = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				medical_equipment_nameDTO.stockAlertQuantity = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("searchColumn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("searchColumn = " + Value);
			if(Value != null)
			{
				medical_equipment_nameDTO.searchColumn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			if(addFlag)
			{
				medical_equipment_nameDTO.insertedByUserId = userDTO.ID;
			}


			if(addFlag)
			{
				medical_equipment_nameDTO.insertedByOrganogramId = userDTO.organogramID;
			}


			if(addFlag)
			{
				Calendar c = Calendar.getInstance();
				c.set(Calendar.HOUR_OF_DAY, 0);
				c.set(Calendar.MINUTE, 0);
				c.set(Calendar.SECOND, 0);
				c.set(Calendar.MILLISECOND, 0);
				
				medical_equipment_nameDTO.insertionDate = c.getTimeInMillis();
			}			

			
			System.out.println("Done adding  addMedical_equipment_name dto = " + medical_equipment_nameDTO);
			long returnedID = -1;
			
			if(isPermanentTable == false) //add new row for validation and make the old row outdated
			{
				medical_equipment_nameDAO.setIsDeleted(medical_equipment_nameDTO.iD, CommonDTO.OUTDATED);
				returnedID = medical_equipment_nameDAO.add(medical_equipment_nameDTO);
				medical_equipment_nameDAO.setIsDeleted(returnedID, CommonDTO.WAITING_FOR_APPROVAL);
			}
			else if(addFlag == true)
			{
				returnedID = medical_equipment_nameDAO.manageWriteOperations(medical_equipment_nameDTO, SessionConstants.INSERT, -1, userDTO);
			}
			else
			{				
				returnedID = medical_equipment_nameDAO.manageWriteOperations(medical_equipment_nameDTO, SessionConstants.UPDATE, -1, userDTO);											
			}
			
			
			
			
			
			
			
			
			
			if(isPermanentTable)
			{
				String inPlaceSubmit = request.getParameter("inplacesubmit");
				
				if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
				{
					getMedical_equipment_name(request, response, returnedID);
				}
				else
				{
					response.sendRedirect("Medical_equipment_nameServlet?actionType=search");
				}
			}
			else
			{
				commonRequestHandler.validate(medical_equipment_nameDAO.getDTOByID(returnedID), request, response, userDTO);
			}
					
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	



	
	
	

	private void getMedical_equipment_name(HttpServletRequest request, HttpServletResponse response, long id) throws ServletException, IOException
	{
		System.out.println("in getMedical_equipment_name");
		Medical_equipment_nameDTO medical_equipment_nameDTO = null;
		try 
		{
			medical_equipment_nameDTO = medical_equipment_nameDAO.getDTOByID(id);
			request.setAttribute("ID", medical_equipment_nameDTO.iD);
			request.setAttribute("medical_equipment_nameDTO",medical_equipment_nameDTO);
			request.setAttribute("medical_equipment_nameDAO",medical_equipment_nameDAO);
			
			String URL= "";
			
			String inPlaceEdit = request.getParameter("inplaceedit");
			String inPlaceSubmit = request.getParameter("inplacesubmit");
			String getBodyOnly = request.getParameter("getBodyOnly");
			
			if(inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase(""))
			{
				URL = "medical_equipment_name/medical_equipment_nameInPlaceEdit.jsp";	
				request.setAttribute("inplaceedit","");				
			}
			else if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
			{
				URL = "medical_equipment_name/medical_equipment_nameSearchRow.jsp";
				request.setAttribute("inplacesubmit","");					
			}
			else
			{
				if(getBodyOnly  != null && !getBodyOnly.equalsIgnoreCase(""))
				{
					URL = "medical_equipment_name/medical_equipment_nameEditBody.jsp?actionType=edit";
				}
				else
				{
					URL = "medical_equipment_name/medical_equipment_nameEdit.jsp?actionType=edit";
				}				
			}
			
			RequestDispatcher rd = request.getRequestDispatcher(URL);
			rd.forward(request, response);
		}
		catch (NumberFormatException e) 
		{
			e.printStackTrace();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	
	private void getMedical_equipment_name(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		getMedical_equipment_name(request, response, Long.parseLong(request.getParameter("ID")));	
	}
	
	private void searchMedical_equipment_name(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException
	{
		System.out.println("in  searchMedical_equipment_name 1");
		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String ajax = request.getParameter("ajax");
		boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);
		
        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
			SessionConstants.NAV_MEDICAL_EQUIPMENT_NAME,
			request,
			medical_equipment_nameDAO,
			SessionConstants.VIEW_MEDICAL_EQUIPMENT_NAME,
			SessionConstants.SEARCH_MEDICAL_EQUIPMENT_NAME,
			tableName,
			isPermanent,
			userDTO,
			filter,
			true);
        try
        {
			System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        }
        catch(Exception e)
        {
			System.out.println("failed to dojob" + e);
        }

		request.setAttribute("medical_equipment_nameDAO",medical_equipment_nameDAO);
        RequestDispatcher rd;
        if(!isPermanent)
        {
        	if(hasAjax == false)
	        {
	        	System.out.println("Going to medical_equipment_name/medical_equipment_nameApproval.jsp");
	        	rd = request.getRequestDispatcher("medical_equipment_name/medical_equipment_nameApproval.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to medical_equipment_name/medical_equipment_nameApprovalForm.jsp");
	        	rd = request.getRequestDispatcher("medical_equipment_name/medical_equipment_nameApprovalForm.jsp");
	        }
        }
        else
        {
	        if(hasAjax == false)
	        {
	        	System.out.println("Going to medical_equipment_name/medical_equipment_nameSearch.jsp");
	        	rd = request.getRequestDispatcher("medical_equipment_name/medical_equipment_nameSearch.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to medical_equipment_name/medical_equipment_nameSearchForm.jsp");
	        	rd = request.getRequestDispatcher("medical_equipment_name/medical_equipment_nameSearchForm.jsp");
	        }
        }
		rd.forward(request, response);
	}
	
}

