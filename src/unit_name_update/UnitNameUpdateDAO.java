package unit_name_update;

import history.UnitNameChangesHistory;
import test_lib.RQueryBuilder;

public class UnitNameUpdateDAO {

    public UnitNameUpdateDAO() {
    }

    public String getUnitList(String officeId) {
        String sql = String.format("select id, unit_name_bng, unit_name_eng from office_units where office_id = %s and isDeleted = 0;", officeId);

        RQueryBuilder<UnitShortModel> rQueryBuilder = new RQueryBuilder<>();

        return rQueryBuilder.setSql(sql).of(UnitShortModel.class).buildJson();
    }

    public boolean updateUnitName(String id, String unitNameBn, String unitNameEn) {
        String sql = String.format("update office_units set unit_name_bng = '%s', unit_name_eng = '%s' where id = %s", unitNameBn, unitNameEn, id);

        UnitNameChangesHistory unitNameChangesHistory = new UnitNameChangesHistory();
        unitNameChangesHistory.saveHistory(Long.parseLong(id), unitNameBn, unitNameEn);

        RQueryBuilder<Boolean> rQueryBuilder = new RQueryBuilder<>();
        return rQueryBuilder.setSql(sql).buildUpdate();
    }

    public String getUnitModel(String id) {
        String sql = String.format("select id, unit_name_bng, unit_name_eng from office_units where id = %s;", id);

        RQueryBuilder<UnitShortModel> rQueryBuilder = new RQueryBuilder<>();

        return rQueryBuilder.setSql(sql).of(UnitShortModel.class).buildJson();
    }
}
