/*
package unit_name_update;


import employee_batches.Employee_batchesServlet;
import login.LoginDTO;
import org.apache.log4j.Logger;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/UnitNameUpdateServlet")
@MultipartConfig
public class UnitNameUpdateServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Employee_batchesServlet.class);

    public UnitNameUpdateServlet() {
        super();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            String actionType = request.getParameter("actionType");
            if (actionType.equals("getUnits")) {
                String officeId = request.getParameter("officeId");

                response.setContentType("application/json");
                PrintWriter out = response.getWriter();
                out.print(new UnitNameUpdateDAO().getUnitList(officeId));
                out.flush();
            } else if (actionType.equals("getUnitModel")) {
                String id = request.getParameter("id");

                response.setContentType("application/json");
                PrintWriter out = response.getWriter();
                out.print(new UnitNameUpdateDAO().getUnitModel(id));
                out.flush();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getInstance().getUserDtoByUserId(loginDTO.userID);
        System.out.println("doPost");

        try {
            String actionType = request.getParameter("actionType");
            System.out.println("actionType = " + actionType);
            if (actionType.equals("updateUnitName")) {
                String id = request.getParameter("id");
                String unitNameBn = request.getParameter("name_bn");
                String unitNameEn = request.getParameter("name_en");
                */
/*
                byte[] charset = unitNameBn.getBytes("ISO-8859-1");
                unitNameBn = new String(charset, "UTF-8");

                byte[] charset0 = unitNameEn.getBytes("ISO-8859-1");
                unitNameEn = new String(charset0, "UTF-8");
                *//*


                response.setContentType("application/json");
                PrintWriter out = response.getWriter();
                out.print(new UnitNameUpdateDAO().updateUnitName(id, unitNameBn, unitNameEn));
                out.flush();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
*/
