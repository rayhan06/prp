package office_organogram_transfer;

import office_ministries.Office_ministriesServlet;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;

@WebServlet("/OfficeOrganogramTransferServlet")
@MultipartConfig
public class OfficeOrganogramTransferServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Office_ministriesServlet.class);

    public OfficeOrganogramTransferServlet() {
        super();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            String actionType = request.getParameter("actionType");
            if (actionType.equals("getOfficeUnit")) {

                response.setContentType("application/json");
                int officeId = Integer.parseInt(request.getParameter("id"));
                PrintWriter out = response.getWriter();
                out.print(new OfficeOrganogramTransferDAO().getOfficeUnit(officeId));
                out.flush();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            String actionType = request.getParameter("actionType");
            if (actionType.equals("transfer")) {
                String[] parm = request.getParameterValues("id");
                String newOfficeUnit = request.getParameter("newOfficeUnit");
                System.out.println(Arrays.toString(parm));
                new OfficeOrganogramTransferDAO().updateTransferOfficeUnitOrganogram(parm, newOfficeUnit);

                response.setContentType("application/json");
                PrintWriter out = response.getWriter();
                out.print(true);
                out.flush();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
