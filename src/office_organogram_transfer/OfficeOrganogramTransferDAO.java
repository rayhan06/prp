package office_organogram_transfer;

import test_lib.RQueryBuilder;

public class OfficeOrganogramTransferDAO {

    public OfficeOrganogramTransferDAO() {
    }

    public String getOfficeUnit(int officeId) {
        String sql = String.format("select id, office_id, unit_name_bng, unit_name_eng from office_units where office_id = %d and isDeleted = 0", officeId);
        RQueryBuilder<OfficeUnitShortModel> rQueryBuilder = new RQueryBuilder<>();
        String data = rQueryBuilder.setSql(sql).of(OfficeUnitShortModel.class).buildJson();

        return data;
    }

    public void updateTransferOfficeUnitOrganogram(String[] parm, String newOfficeUnit) {
        for (int i = 0; i < parm.length; i += 2) {
            String sql = String.format("update office_unit_organograms set office_unit_id = %s where id = %s and office_unit_id = %s;", newOfficeUnit, parm[i + 1], parm[i]);

            RQueryBuilder<Boolean> builder = new RQueryBuilder<>();
            builder.setSql(sql).buildUpdate();
        }
    }
}
