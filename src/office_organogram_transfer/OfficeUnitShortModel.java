package office_organogram_transfer;

public class OfficeUnitShortModel {
    private int id;
    private int office_id;
    private String unit_name_bng;
    private String unit_name_eng;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getOffice_id() {
        return office_id;
    }

    public void setOffice_id(int office_id) {
        this.office_id = office_id;
    }

    public String getUnit_name_bng() {
        return unit_name_bng;
    }

    public void setUnit_name_bng(String unit_name_bng) {
        this.unit_name_bng = unit_name_bng;
    }

    public String getUnit_name_eng() {
        return unit_name_eng;
    }

    public void setUnit_name_eng(String unit_name_eng) {
        this.unit_name_eng = unit_name_eng;
    }
}
