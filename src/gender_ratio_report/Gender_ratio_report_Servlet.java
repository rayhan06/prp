package gender_ratio_report;


import com.google.gson.Gson;
import common.ApiResponse;
import common.RequestFailureException;
import language.LC;
import language.LM;
import login.LoginDTO;
import office_unit_organograms.OfficeUnitOrganogramsRepository;
import office_units.Office_unitsRepository;
import org.apache.log4j.Logger;
import pbReport.ColumnConvertor;
import pbReport.PBReportUtils;
import pbReport.ReportService;
import pbReport.ReportTemplate;
import permission.MenuConstants;
import role.PermissionRepository;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.ActionTypeConstant;
import util.JSPConstant;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;
import java.util.stream.Collectors;

@WebServlet("/Gender_ratio_report_Servlet")
public class Gender_ratio_report_Servlet extends HttpServlet {

    private static final long serialVersionUID = 1L;
    private static final Logger logger = Logger.getLogger(Gender_ratio_report_Servlet.class);
    ReportTemplate reportTemplate = new ReportTemplate();
    private static final Set<String> searchParam = new HashSet<>(Arrays.asList("officeUnitId", "officeUnitOrganogramId", "employmentCat", "gender"));
    private final Map<String, String[]> stringMap = new HashMap<>();

    public Gender_ratio_report_Servlet() {
        stringMap.put("officeUnitId", new String[]{"criteria", "eo", "office_unit_id", "IN", "AND", "String", "", "", "", "officeUnitIds"});
        stringMap.put("officeUnitOrganogramId", new String[]{"criteria", "eo", "office_unit_organogram_id", "=", "AND", "long", "", "", "", "officeUnitOrganogramId"});
        stringMap.put("office_id", new String[]{"criteria", "eo", "office_id", "=", "AND", "long", "", "", "2294", "office_id"});
        stringMap.put("isDeleted", new String[]{"criteria", "eo", "isDeleted", "=", "AND", "long", "", "", "0", "isDeleted"});
        stringMap.put("status", new String[]{"criteria", "eo", "status", "=", "AND", "long", "", "", "1", "status"});
        stringMap.put("isDeleted_1", new String[]{"criteria", "er", "isDeleted", "=", "AND", "long", "", "", "0", "isDeleted_1"});
        stringMap.put("employmentCat", new String[]{"criteria", "er", "employment_cat", "=", "AND", "int", "", "", "any", "employmentCat"});
        stringMap.put("gender", new String[]{"criteria", "er", "gender_cat", "=", "AND", "String", "", "", "any", "gender"});

    }

    String[][] Criteria;
//            {
//                    {"criteria", "eo", "office_unit_id", "=", "", "String", "", "", "any", "officeUnitId"},
//                    {"criteria", "eo", "office_unit_organogram_id", "=", "AND", "String", "", "", "any", "officeUnitOrganogramId"},
//                    {"criteria", "er", "employment_cat", "=", "AND", "int", "", "", "any", "employmentCat"},
//                    {"criteria", "er", "gender", "=", "AND", "String", "", "", "any", "gender"}
//            };

    String[][] Display =
            {
                    {"display", "eo", "employee_record_id", "employee_records_id", ""},
                    {"display", "eo", "office_unit_id", "office_unit", ""},
                    {"display", "eo", "office_unit_organogram_id", "organogram", ""},
                    {"display", "er", "employment_cat", "category", ""},
                    {"display", "er", "gender_cat", "category", ""}
            };

    String GroupBy = "";
    String OrderBY = " office_unit_id ";


    private final ReportService reportService = new ReportService();

    private String sql;


    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String language = "english";
		if(userDTO!= null && userDTO.languageID == SessionConstants.BANGLA)
		{
			language = "bangla";
		}
            String actionType = request.getParameter("actionType");
            ApiResponse apiResponse = null;

            sql = "employee_records er inner join employee_offices eo on eo.employee_record_id = er.id";

            Display[0][4] = LM.getText(LC.GENDER_RATIO_REPORT_SELECT_EMPLOYEERECORDID, loginDTO);
            Display[1][4] = LM.getText(LC.GENDER_RATIO_REPORT_SELECT_OFFICEUNITID, loginDTO);
            Display[2][4] = LM.getText(LC.GENDER_RATIO_REPORT_SELECT_OFFICEUNITORGANOGRAMID, loginDTO);
            Display[3][4] = LM.getText(LC.GENDER_RATIO_REPORT_SELECT_EMPLOYMENTCAT, loginDTO);
            Display[4][4] = LM.getText(LC.GENDER_RATIO_REPORT_SELECT_GENDER, loginDTO);
            Set<String> inputs = PBReportUtils.prepareInputSets(searchParam, request);
            inputs.add("office_id");
            inputs.add("isDeleted");
            inputs.add("status");
            inputs.add("isDeleted_1");
            Criteria = PBReportUtils.prepareCriteria(inputs, stringMap);
            if (inputs.contains("officeUnitId")) {
                List<Long> officeUnitIds = Office_unitsRepository.getInstance().getDescentsOfficeUnitId(Long.parseLong(request.getParameter("officeUnitId")));
                stringMap.get("officeUnitId")[8] = officeUnitIds.stream()
                        .distinct()
                        .map(String::valueOf)
                        .collect(Collectors.joining(","));
            }
            if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.GENDER_RATIO_REPORT)) {

                if (ActionTypeConstant.REPORT_PAGE.equals(actionType)) {

                    request.getRequestDispatcher("/gender_ratio_report/gender_ratio_report.jsp").forward(request, response);

                    return;

                } else if (ActionTypeConstant.REPORT_PAGE_SUMMARY.equals(actionType)) {

                    request.getRequestDispatcher(JSPConstant.PERFORMANCE_LOG_SUMMARY_REPORT).forward(request, response);

                    return;

                } else if (ActionTypeConstant.REPORT_COUNT.equals(actionType)) {
                    try {
                        int count = getTotalCount(request, response, language);
                        apiResponse = ApiResponse.makeSuccessResponse(count, "Success");
                    } catch (Exception ex) {
                        apiResponse = ApiResponse.makeErrorResponse(ex.getMessage());
                        if (ex instanceof RequestFailureException) {
                            throw (RequestFailureException) ex;
                        }
                        throw new RuntimeException(ex);
                    }
                } else if (ActionTypeConstant.REPORT_RESULT.equals(actionType)) {
                    try {
                        List list = getReport(request, response, language);
                        apiResponse = ApiResponse.makeSuccessResponse(list, "Success");
                    } catch (Exception ex) {
                        apiResponse = ApiResponse.makeErrorResponse(ex.getMessage());
                        if (ex instanceof RequestFailureException) {
                            throw (RequestFailureException) ex;
                        }
                        throw new RuntimeException(ex);
                    }
                } else if (ActionTypeConstant.REPORT_TEMPLATE.equals(actionType)) {

                    apiResponse = ApiResponse.makeSuccessResponse(reportTemplate, "Success");

                } else if (actionType.equals("getGeo")) {
                    System.out.println("going to geoloc ");
                    request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
                }


                PrintWriter writer = response.getWriter();
                writer.write(new Gson().toJson(apiResponse));
                writer.flush();
                writer.close();

            }
        }
    

//    protected void doPost(HttpServletRequest request, HttpServletResponse response)
//            throws ServletException, IOException {
//
//
//        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
//        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
//        String actionType = request.getParameter("actionType");
//        System.out.println("In ssservlet dopost, actiontype = " + actionType);
//
//        ApiResponse apiResponse = null;
//
//        if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.REPORT_PAGE)) {
//
//            if (ActionTypeConstant.REPORT_COUNT.equals(actionType)) {
//                try {
//                    int count = getTotalCount(request, response);
//                    apiResponse = ApiResponse.makeSuccessResponse(count, "Success");
//                } catch (Exception ex) {
//                    apiResponse = ApiResponse.makeErrorResponse(ex.getMessage());
//                    if (ex instanceof RequestFailureException) {
//                        throw (RequestFailureException) ex;
//                    }
//                    throw new RuntimeException(ex);
//                }
//            } else if (ActionTypeConstant.REPORT_RESULT.equals(actionType)) {
//                try {
//                    List list = getReport(request, response);
//                    apiResponse = ApiResponse.makeSuccessResponse(list, "Success");
//                } catch (Exception ex) {
//                    apiResponse = ApiResponse.makeErrorResponse(ex.getMessage());
//                    if (ex instanceof RequestFailureException) {
//                        throw (RequestFailureException) ex;
//                    }
//                    throw new RuntimeException(ex);
//                }
//            } else if (ActionTypeConstant.REPORT_TEMPLATE.equals(actionType)) {
//
//
//                reportTemplate = createReportTemplate(request);
//
//                apiResponse = ApiResponse.makeSuccessResponse(null, "Success");
//
//            } else if (actionType.equals("getGeo")) {
//                System.out.println("going to geoloc ");
//                request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
//            }
//
//            PrintWriter writer = response.getWriter();
//            writer.write(new Gson().toJson(apiResponse));
//            writer.flush();
//            writer.close();
//
//        } else {
//            request.getRequestDispatcher(JSPConstant.ERROR_GLOBAL).forward(request, response);
//        }
//    }
//
//    private ReportTemplate createReportTemplate(HttpServletRequest request) {
//
//        String[] orderByColumns = request.getParameterValues("orderByColumns");
//        List<String> displayColumns = new ArrayList<>();
//        List<String> criteriaColumns = new ArrayList<>();
//
//        for (String parameterName : request.getParameterMap().keySet()) {
//            if (parameterName.startsWith("display")) {
//                displayColumns.add(parameterName + "=" + request.getParameter(parameterName));
//            }
//            if (parameterName.startsWith("criteria")) {
//                criteriaColumns.add(parameterName);
//            }
//        }
//        String orderByColumnString = "";
//        String displayColumnString = "";
//        String criteriaColumnString = "";
//        if (orderByColumns != null) {
//            for (int i = 0; i < orderByColumns.length; i++) {
//                if (i != 0) {
//                    orderByColumnString += ",";
//                }
//                String orderByColumn = orderByColumns[i];
//                orderByColumnString += orderByColumn;
//            }
//        }
//        for (int i = 0; i < displayColumns.size(); i++) {
//            if (i != 0) {
//                displayColumnString += ",";
//            }
//            String displayColumn = displayColumns.get(i);
//            displayColumnString += displayColumn;
//        }
//        for (int i = 0; i < criteriaColumns.size(); i++) {
//            if (i != 0) {
//                criteriaColumnString += ",";
//            }
//            String criteraiColumn = criteriaColumns.get(i);
//            criteriaColumnString += criteraiColumn;
//        }
//
//        ReportTemplate reportTemplate = new ReportTemplate();
//
//        reportTemplate.reportDisplay = displayColumnString;
//        reportTemplate.reportCriteria = criteriaColumnString;
//        reportTemplate.reportOrder = orderByColumnString;
//
//        return reportTemplate;
//
//    }

    private int getTotalCount(HttpServletRequest request, HttpServletResponse response, String language) throws Exception {
        int count = reportService.getTotalCount(sql, request, Criteria, Display, GroupBy, OrderBY, language);
        return count;
    }

    private List<List<Object>> getReport(HttpServletRequest request, HttpServletResponse response, String language) throws Exception {

        Integer recordPerPage = Integer.parseInt(request.getParameter("RECORDS_PER_PAGE"));
        Integer pageNo = Integer.parseInt(request.getParameter("pageno"));
        System.out.println("recordPerPage = " + recordPerPage + " pageNo = " + pageNo);
        int offset = (pageNo - 1) * recordPerPage;
        return reportService.getResultSet(sql, request, recordPerPage, offset, Criteria, Display, GroupBy, OrderBY, language);
    }

}
