package public_users;

import common.ConnectionAndStatementUtil;
import common.RequestFailureException;
import dbm.DBMR;
import dbm.DBMW;
import login.LoginDTO;
import org.apache.log4j.Logger;
import repository.RepositoryManager;
import util.ConnectionUtil;
import util.HttpUtil;
import util.NavigationService;
import util.NavigationService4;

import java.sql.*;
import java.util.*;
/**
 * @author Kayesh Parvez
 *
 */
public class PublicUserDAO implements NavigationService {
	
	Logger logger = Logger.getLogger(getClass());

	public static final String publicUserTable = "public_users";
	public static final int ADMIN = 1;
	public static final int DEFAULT = 8005;



	public void updatePublicUserPassword(PublicUserDTO publicUserDTO) throws Exception{

		Connection connection = null;
		PreparedStatement ps = null;

		long currentTime = System.currentTimeMillis();

		try{
			connection = DBMW.getInstance().getConnection();
			PublicUserDTO existingPublicUserDTO = PublicUserRepository.getPublicUserDTOByUserName(publicUserDTO.userName);
			if(existingPublicUserDTO != null && existingPublicUserDTO.ID != publicUserDTO.ID && (existingPublicUserDTO.isDeleted != 1))
			{
				throw new RequestFailureException("Username already exists. Please use another username.");
			}

			String sql ="UPDATE " + publicUserTable + " SET password=?, lastModificationTime=? WHERE ID = ?";

			ps = connection.prepareStatement(sql);

			int index = 1;


			ps.setObject(index++,publicUserDTO.password);

			ps.setObject(index++,currentTime);


			ps.setObject(index++,publicUserDTO.ID);

			ps.executeUpdate();

			ps.close();

			PublicUserRepository.getInstance().reload(false);
//			String baseUrl = "http://localhost:8080/public_prp/";
			String baseUrl = "https://prp.parliament.gov.bd/recruitment/";
			String url = baseUrl + "UserServlet?actionType=updateRepo";
			HttpUtil.sendGet(url, null);
//			ConnectionUtil.updateVbSequencer("" + publicUserTable + "", currentTime, connection, ps);

		}catch(Exception ex){
			logger.fatal("",ex);
			throw ex;
		}finally{
			try{
				if (ps != null) {
					ps.close();
				}
			} catch(Exception e){}
			try{
				if(connection != null){
					DBMW.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}





	}

	private PublicUserDTO buildPublicUserDTOFromUsersTableRS(ResultSet rs) {
		try{
			PublicUserDTO publicUserDTO = new PublicUserDTO();
			publicUserDTO.ID = rs.getLong("ID");
			publicUserDTO.userName = rs.getString("userName");
			publicUserDTO.password = rs.getString("password");
			publicUserDTO.userType = rs.getInt("userType");
			publicUserDTO.roleID = rs.getInt("roleID");
			publicUserDTO.languageID = rs.getInt("languageID");
			publicUserDTO.isDeleted = rs.getInt("isDeleted");
			publicUserDTO.mailAddress = rs.getString("mailAddress");
			publicUserDTO.fullName = rs.getString("fullName");
			publicUserDTO.phoneNo = rs.getString("phoneNo");
			publicUserDTO.centreType = rs.getInt("centre_Type");
			return publicUserDTO;
		}catch (SQLException ex){
			logger.error(ex);
			return null;
		}
	}

	public PublicUserDTO getPublicUserDTOByColumnNameAndVal(String columnName, String columnVal){

		String sql = "SELECT ID,userName,password,userType,roleID, languageID, lastModificationTime,isDeleted,mailAddress,fullName,phoneNo,centre_Type FROM " + publicUserTable;
		//sql += " WHERE userName LIKE '"+userName+"' order by ID desc";
		sql += " WHERE "+columnName+" LIKE ? order by ID desc";

		return ConnectionAndStatementUtil.getT(sql, Collections.singletonList(columnVal), this::buildPublicUserDTOFromUsersTableRS);
	}
	
	
	public PublicUserDTO getPublicUserDTOByUsername(String userName){
		
		String sql = "SELECT ID,userName,password,userType,roleID, languageID, lastModificationTime,isDeleted,mailAddress,fullName,phoneNo,centre_Type FROM " + publicUserTable;
		//sql += " WHERE userName LIKE '"+userName+"' order by ID desc";
		sql += " WHERE userName LIKE ? order by ID desc";

		return ConnectionAndStatementUtil.getT(sql, Collections.singletonList(userName), this::buildPublicUserDTOFromUsersTableRS);

	}

	public PublicUserDTO getPublicUserDTOByMail(String mailAddress){
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		PublicUserDTO publicUserDTO = null;
		try{

			String sql = "SELECT ID,userName,password,userType,roleID,lastModificationTime,isDeleted,mailAddress,fullName,phoneNo,centre_Type FROM " + publicUserTable + " WHERE mailAddress LIKE '"+mailAddress+"'";

			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();

			if(rs.next()){
				publicUserDTO = new PublicUserDTO();
				publicUserDTO.ID = rs.getLong("ID");
				publicUserDTO.userName = rs.getString("userName");
				publicUserDTO.password = rs.getString("password");
				publicUserDTO.userType = rs.getInt("userType");
				publicUserDTO.roleID = rs.getInt("roleID");
				publicUserDTO.isDeleted = rs.getInt("isDeleted");
				publicUserDTO.mailAddress = rs.getString("mailAddress");
				publicUserDTO.fullName = rs.getString("fullName");
				publicUserDTO.phoneNo = rs.getString("phoneNo");
				publicUserDTO.centreType = rs.getInt("centre_Type");

			}



		}catch(Exception ex){
			logger.fatal("",ex);
		}finally{
			try{
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}

			try{
				if (connection != null){
					DBMR.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		return publicUserDTO;
	}



	public PublicUserDTO getPublicUserDTOByEmployeeRecordId(Long employeeRecordId){
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		PublicUserDTO publicUserDTO = null;
		try{

			String sql = "SELECT ID,userName,password,userType,roleID,lastModificationTime,isDeleted,mailAddress,fullName,phoneNo,centre_Type FROM " + publicUserTable + " WHERE employee_record_id = " + employeeRecordId;

			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			if(rs.next()){
				publicUserDTO = new PublicUserDTO();
				publicUserDTO.ID = rs.getLong("ID");
				publicUserDTO.userName = rs.getString("userName");
				publicUserDTO.password = rs.getString("password");
				publicUserDTO.userType = rs.getInt("userType");
				publicUserDTO.roleID = rs.getInt("roleID");
				publicUserDTO.isDeleted = rs.getInt("isDeleted");
				publicUserDTO.mailAddress = rs.getString("mailAddress");
				publicUserDTO.fullName = rs.getString("fullName");
				publicUserDTO.phoneNo = rs.getString("phoneNo");
				publicUserDTO.centreType = rs.getInt("centre_Type");

			}



		}catch(Exception ex){
			logger.fatal("",ex);
		}finally{
			try{
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}

			try{
				if (connection != null){
					DBMR.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		return publicUserDTO;
	}


	public PublicUserDTO build(ResultSet rs)
	{
		try
		{
			PublicUserDTO publicUserDTO = new PublicUserDTO();

			publicUserDTO.ID = rs.getLong(publicUserTable + ".ID");
			publicUserDTO.userName = rs.getString("userName");
			publicUserDTO.password = rs.getString("password");
			publicUserDTO.userType = rs.getInt("userType");
			publicUserDTO.roleID = rs.getInt("roleID");

			publicUserDTO.languageID = rs.getInt("languageID");
			publicUserDTO.isDeleted = rs.getInt("isDeleted");
			publicUserDTO.mailAddress = rs.getString("mailAddress");
			publicUserDTO.fullName = rs.getString("fullName");
			publicUserDTO.phoneNo = rs.getString("phoneNo");
			publicUserDTO.centreType = rs.getInt("centre_Type");
			publicUserDTO.employee_record_id = rs.getInt("employee_record_id");
			//publicUserDTO.officeID = rs.getLong("office_id");
			//publicUserDTO.organogramID = rs.getLong("office_unit_organogram_id");
			//publicUserDTO.unitID = rs.getLong("office_unit_id");

			publicUserDTO.otpSMS = rs.getBoolean("otpSMS");
			publicUserDTO.otpEmail = rs.getBoolean("otpEmail");
			publicUserDTO.otpPushNotification = rs.getBoolean("otpPushNotification");

			publicUserDTO.nid = rs.getString("nid");
			publicUserDTO.dateOfBirth = rs.getLong("date_of_birth");
			publicUserDTO.birthRegistrationNo  =   rs.getString("birth_reg_no");


			return publicUserDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
    
    public List getAllUsers(boolean isFirstReload)
    {
		String sql = "SELECT * FROM " + publicUserTable + " WHERE ";

		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		}


		logger.debug("sql " + sql);
		
		return ConnectionAndStatementUtil.getListOfT(sql, this::build);

    }


	@Override
	public Collection getIDs(LoginDTO loginDTO) throws Exception {
		return null;
	}

	@Override
	public Collection getIDsWithSearchCriteria(Hashtable searchCriteria, LoginDTO loginDTO) throws Exception {
		return null;
	}

	@Override
	public Collection getDTOs(Collection recordIDs, LoginDTO loginDTO) throws Exception {
		return null;
	}
}
