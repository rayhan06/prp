package public_users;

import login.LoginDTO;
import org.apache.log4j.Logger;
import repository.Repository;
import repository.RepositoryManager;
import user.UserNameEmailDTO;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;

/**
 * @author Kayesh Parvez
 *
 */
@SuppressWarnings("unchecked")
public class PublicUserRepository implements Repository {
	PublicUserDAO PublicUserDAO = new PublicUserDAO();

	static Logger logger = Logger.getLogger(PublicUserRepository.class);
	static Map<String, PublicUserDTO> mapOfPublicUserDTOToUserName;
	static Map<Long, PublicUserDTO> mapOfPublicUserDTOToUserID;
	static Map<Long, Set<PublicUserDTO>> mapOfPublicUserDTOsToRoleID;
	private static Map<Long, PublicUserDTO> publicUserDtoById;

	static Map<String, PublicUserDTO> mapOfPublicUserDTOToEmail;
	static Map<String, PublicUserDTO> mapOfPublicUserDTOToNid;
	static Map<String, PublicUserDTO> mapOfPublicUserDTOToBirthRegNo;

	static PublicUserRepository instance = null;



	private PublicUserRepository() {
		mapOfPublicUserDTOToUserName = new ConcurrentHashMap<>();
		mapOfPublicUserDTOToUserID = new ConcurrentHashMap<>();
		mapOfPublicUserDTOsToRoleID = new ConcurrentHashMap<>();
		publicUserDtoById = new ConcurrentHashMap<>();

		mapOfPublicUserDTOToEmail = new ConcurrentHashMap<>();
		mapOfPublicUserDTOToNid = new ConcurrentHashMap<>();
		mapOfPublicUserDTOToBirthRegNo = new ConcurrentHashMap<>();



		RepositoryManager.getInstance().addRepository(this);
	}

	private static class LazyLoader {
		static PublicUserRepository INSTANCE = new PublicUserRepository();
	}

	public synchronized static PublicUserRepository getInstance() {
		return LazyLoader.INSTANCE;
	}

	public void reload(boolean reloadAll) {
		try {
			List<PublicUserDTO> publicPublicUserDTOs = PublicUserDAO.getAllUsers(reloadAll);
			for (PublicUserDTO publicPublicUserDTO : publicPublicUserDTOs) {
				PublicUserDTO oldPublicUserDTO = getPublicUserDTOByUserID2(publicPublicUserDTO.ID);
				if (oldPublicUserDTO != null) {
					mapOfPublicUserDTOToUserID.remove(oldPublicUserDTO.ID);
					publicUserDtoById.remove(oldPublicUserDTO.employee_record_id);
					mapOfPublicUserDTOToUserName.remove(oldPublicUserDTO.userName);

					mapOfPublicUserDTOToEmail.remove(oldPublicUserDTO.mailAddress);
					mapOfPublicUserDTOToNid.remove(oldPublicUserDTO.nid);
					mapOfPublicUserDTOToBirthRegNo.remove(oldPublicUserDTO.birthRegistrationNo);

					if (mapOfPublicUserDTOsToRoleID.containsKey(oldPublicUserDTO.roleID)) {
						mapOfPublicUserDTOsToRoleID.get(oldPublicUserDTO.roleID).remove(oldPublicUserDTO);
					}
					if (mapOfPublicUserDTOsToRoleID.get(oldPublicUserDTO.roleID).isEmpty()) {
						mapOfPublicUserDTOsToRoleID.remove(oldPublicUserDTO.roleID);
					}
				}
				if (publicPublicUserDTO.isDeleted == 0) {
					mapOfPublicUserDTOToUserID.put(publicPublicUserDTO.ID, publicPublicUserDTO);
					publicUserDtoById.put(publicPublicUserDTO.employee_record_id,publicPublicUserDTO);
					mapOfPublicUserDTOToUserName.put(publicPublicUserDTO.userName, publicPublicUserDTO);

					mapOfPublicUserDTOToEmail.put(publicPublicUserDTO.mailAddress, publicPublicUserDTO);
					mapOfPublicUserDTOToNid.put(publicPublicUserDTO.nid, publicPublicUserDTO);
					mapOfPublicUserDTOToBirthRegNo.put(publicPublicUserDTO.birthRegistrationNo, publicPublicUserDTO);

					if (!mapOfPublicUserDTOsToRoleID.containsKey(publicPublicUserDTO.roleID)) {
						mapOfPublicUserDTOsToRoleID.put(publicPublicUserDTO.roleID, new HashSet<>());
					}
					mapOfPublicUserDTOsToRoleID.get(publicPublicUserDTO.roleID).add(publicPublicUserDTO);
				}
			}
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}

	public void updatePublicUserDTO(PublicUserDTO publicPublicUserDTO) {
		mapOfPublicUserDTOToUserID.put(publicPublicUserDTO.ID, publicPublicUserDTO);

		mapOfPublicUserDTOToUserName.put(publicPublicUserDTO.userName, publicPublicUserDTO);

		mapOfPublicUserDTOToEmail.put(publicPublicUserDTO.mailAddress, publicPublicUserDTO);
		mapOfPublicUserDTOToNid.put(publicPublicUserDTO.nid, publicPublicUserDTO);
		mapOfPublicUserDTOToBirthRegNo.put(publicPublicUserDTO.birthRegistrationNo, publicPublicUserDTO);

		if (!mapOfPublicUserDTOsToRoleID.containsKey(publicPublicUserDTO.roleID)) {
			mapOfPublicUserDTOsToRoleID.put(publicPublicUserDTO.roleID, new HashSet<>());
		}
		mapOfPublicUserDTOsToRoleID.get(publicPublicUserDTO.roleID).add(publicPublicUserDTO);
	}

	public static List<PublicUserDTO> getUserList() {
		List<PublicUserDTO> users = new ArrayList<PublicUserDTO>(mapOfPublicUserDTOToUserID.values());
		return users;
	}

	public static int getUserCount() {
		return mapOfPublicUserDTOToUserID.size();
	}


	public static PublicUserDTO getPublicUserDTOByUserID2(long userID) {
		try {
			return mapOfPublicUserDTOToUserID.get(userID);
		} catch (Exception e) {
			logger.error(e);
		}
		return null;
	}

	public static PublicUserDTO getPublicUserDTOByUserName(String userName) {
		PublicUserDTO tempUser = null;
		try {
			return mapOfPublicUserDTOToUserName.get(userName);
		} catch (Exception e) {
			logger.error(e);
		}
		return tempUser;
	}

	public static PublicUserDTO getPublicUserDTOByUserMail(String mailAddress) {
		PublicUserDTO tempUser = null;
		try {
			return mapOfPublicUserDTOToEmail.get(mailAddress);
		} catch (Exception e) {
			logger.error(e);
		}
		return tempUser;
	}

	public static PublicUserDTO getPublicUserDTOByUserNid(String Nid) {
		PublicUserDTO tempUser = null;
		try {
			return mapOfPublicUserDTOToNid.get(Nid);
		} catch (Exception e) {
			logger.error(e);
		}
		return tempUser;
	}

	public static PublicUserDTO getPublicUserDTOByUserBirthRegNo(String birthRegNo) {
		PublicUserDTO tempUser = null;
		try {
			return mapOfPublicUserDTOToBirthRegNo.get(birthRegNo);
		} catch (Exception e) {
			logger.error(e);
		}
		return tempUser;
	}

	public List<PublicUserDTO> getUsersByRoleID(long roleID) {
		return (List<PublicUserDTO>) mapOfPublicUserDTOsToRoleID.get(roleID);
	}

	@Override
	public String getTableName() {
		String tableName = "";
		try {
			tableName = public_users.PublicUserDAO.publicUserTable;
		} catch (Exception ex) {
			logger.error(ex);
		}
		return tableName;
	}

	public static void main(String[] args) throws Exception {
		PublicUserRepository.getInstance();
	}

	public static UserNameEmailDTO convertPublicUserDTOIntoNameEmailDTO(PublicUserDTO publicPublicUserDTO) {
		UserNameEmailDTO userNameEmailDTO = new UserNameEmailDTO();
		userNameEmailDTO.email = publicPublicUserDTO.userName == null ? "" : publicPublicUserDTO.userName;
		userNameEmailDTO.name = publicPublicUserDTO.fullName == null ? "" : publicPublicUserDTO.fullName;
		return userNameEmailDTO;
	}

	public PublicUserDTO getPublicUserDtoById(long employeeRecordId) {
		try {
			return mapOfPublicUserDTOToUserID.get(employeeRecordId);
		} catch (Exception e) {
			return new PublicUserDTO();
		}
	}

	public PublicUserDTO getExistingUser(Function<PublicUserDTO, String> colValueExtractor, String colVal){

		  return   getUserList().stream()
				   .filter(publicPublicUserDTO -> colValueExtractor.apply(publicPublicUserDTO).equals(colVal))
				   .findFirst()
				   .orElse(null);

	}

}
