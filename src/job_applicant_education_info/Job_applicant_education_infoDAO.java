package job_applicant_education_info;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;

import common.ConnectionAndStatementUtil;
import sessionmanager.SessionConstants;
import java.sql.SQLException;


import org.apache.log4j.Logger;

import dbm.*;

import repository.RepositoryManager;

import util.*;
import pb.*;
import user.UserDTO;
import vm_vehicle_office_assignment.Vm_vehicle_office_assignmentDTO;

public class Job_applicant_education_infoDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());
	
	
	public Job_applicant_education_infoDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";
//		commonMaps = new Job_applicant_education_infoMAPS(tableName);
	}

	@Override
	public Collection getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO) {
		return null;
	}

	public Job_applicant_education_infoDAO()
	{
		this("job_applicant_education_info");		
	}
	
	public void get(Job_applicant_education_infoDTO job_applicant_education_infoDTO, ResultSet rs) throws SQLException
	{
		job_applicant_education_infoDTO.iD = rs.getLong("ID");
		job_applicant_education_infoDTO.jobApplicantId = rs.getLong("job_applicant_id");
		job_applicant_education_infoDTO.jobId = rs.getLong("job_id");
		job_applicant_education_infoDTO.educationLevelType = rs.getLong("education_level_type");
		job_applicant_education_infoDTO.degreeExamType = rs.getLong("degree_exam_type");
		job_applicant_education_infoDTO.otherDegreeExam = rs.getString("other_degree_exam");
		job_applicant_education_infoDTO.major = rs.getString("major");
		job_applicant_education_infoDTO.resultExamType = rs.getLong("result_exam_type");
		job_applicant_education_infoDTO.marksPercentage = rs.getString("marks_percentage");
		job_applicant_education_infoDTO.gradePointCat = rs.getInt("grade_point_cat");
		job_applicant_education_infoDTO.cgpaNumber = rs.getDouble("cgpa_number");
		job_applicant_education_infoDTO.yearOfPassingNumber = rs.getInt("year_of_passing_number");
		job_applicant_education_infoDTO.boardType = rs.getLong("board_type");
		job_applicant_education_infoDTO.institutionName = rs.getString("institution_name");
		job_applicant_education_infoDTO.isForeign = rs.getBoolean("is_foreign");
		job_applicant_education_infoDTO.durationYears = rs.getInt("duration_years");
		job_applicant_education_infoDTO.achievement = rs.getString("achievement");
		job_applicant_education_infoDTO.filesDropzone = rs.getLong("files_dropzone");
		job_applicant_education_infoDTO.insertionDate = rs.getLong("insertion_date");
		job_applicant_education_infoDTO.insertedBy = rs.getString("inserted_by");
		job_applicant_education_infoDTO.modifiedBy = rs.getString("modified_by");
		job_applicant_education_infoDTO.isDeleted = rs.getInt("isDeleted");
		job_applicant_education_infoDTO.lastModificationTime = rs.getLong("lastModificationTime");
		job_applicant_education_infoDTO.institution_id = rs.getLong("institution_id");
	}

	public Job_applicant_education_infoDTO build(ResultSet rs)
	{
		try
		{
			Job_applicant_education_infoDTO job_applicant_education_infoDTO = new Job_applicant_education_infoDTO();
			job_applicant_education_infoDTO.iD = rs.getLong("ID");
			job_applicant_education_infoDTO.jobApplicantId = rs.getLong("job_applicant_id");
			job_applicant_education_infoDTO.jobId = rs.getLong("job_id");
			job_applicant_education_infoDTO.educationLevelType = rs.getLong("education_level_type");
			job_applicant_education_infoDTO.degreeExamType = rs.getLong("degree_exam_type");
			job_applicant_education_infoDTO.otherDegreeExam = rs.getString("other_degree_exam");
			job_applicant_education_infoDTO.major = rs.getString("major");
			job_applicant_education_infoDTO.resultExamType = rs.getLong("result_exam_type");
			job_applicant_education_infoDTO.marksPercentage = rs.getString("marks_percentage");
			job_applicant_education_infoDTO.gradePointCat = rs.getInt("grade_point_cat");
			job_applicant_education_infoDTO.cgpaNumber = rs.getDouble("cgpa_number");
			job_applicant_education_infoDTO.yearOfPassingNumber = rs.getInt("year_of_passing_number");
			job_applicant_education_infoDTO.boardType = rs.getLong("board_type");
			job_applicant_education_infoDTO.institutionName = rs.getString("institution_name");
			job_applicant_education_infoDTO.isForeign = rs.getBoolean("is_foreign");
			job_applicant_education_infoDTO.durationYears = rs.getInt("duration_years");
			job_applicant_education_infoDTO.achievement = rs.getString("achievement");
			job_applicant_education_infoDTO.filesDropzone = rs.getLong("files_dropzone");
			job_applicant_education_infoDTO.insertionDate = rs.getLong("insertion_date");
			job_applicant_education_infoDTO.insertedBy = rs.getString("inserted_by");
			job_applicant_education_infoDTO.modifiedBy = rs.getString("modified_by");
			job_applicant_education_infoDTO.isDeleted = rs.getInt("isDeleted");
			job_applicant_education_infoDTO.lastModificationTime = rs.getLong("lastModificationTime");
			job_applicant_education_infoDTO.institution_id = rs.getLong("institution_id");
			return job_applicant_education_infoDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
	

		
	

	//need another getter for repository
	public Job_applicant_education_infoDTO getDTOByID (long ID)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID = ? ";
		Job_applicant_education_infoDTO job_applicant_education_infoDTO =
				ConnectionAndStatementUtil.getT(sql, Arrays.asList(ID), this::build);
		return job_applicant_education_infoDTO;
	}
	
	
	public List<Job_applicant_education_infoDTO> getDTOs(Collection recordIDs){
		if(recordIDs.isEmpty()){
			return new ArrayList<>();
		}
		String sql = "SELECT * FROM " + tableName + " WHERE ID IN ( ";
		for(int i = 0;i<recordIDs.size();i++){
			if(i!=0){
				sql+=",";
			}
			sql+= " ? ";
		}
		sql+=")  order by lastModificationTime desc";
		printSql(sql);
		return ConnectionAndStatementUtil.getListOfT(sql, Arrays.asList(recordIDs.toArray()),this::build);
	
	}
	
	

	
	
	
	//add repository
	public List<Job_applicant_education_infoDTO> getAllJob_applicant_education_info (boolean isFirstReload)
    {
		List<Job_applicant_education_infoDTO> job_applicant_education_infoDTOList = new ArrayList<>();

		String sql = "SELECT * FROM job_applicant_education_info";
		sql += " WHERE ";
	

		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by job_applicant_education_info.lastModificationTime desc";
		printSql(sql);
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);
    }

	public List<Job_applicant_education_infoDTO> getDTOByJobApplicantID (long ID)
	{

		String sql = "SELECT * FROM job_applicant_education_info";

		sql += " WHERE job_applicant_id = ? ";

		sql+=" AND isDeleted =  0";

		sql += " order by job_applicant_education_info.lastModificationTime desc";
		printSql(sql);
		return ConnectionAndStatementUtil.getListOfT(sql, Arrays.asList(ID), this::build);
	}
				
}
	