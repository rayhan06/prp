package job_applicant_education_info;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class Job_applicant_education_infoRepository implements Repository {
	Job_applicant_education_infoDAO job_applicant_education_infoDAO = null;
	
	public void setDAO(Job_applicant_education_infoDAO job_applicant_education_infoDAO)
	{
		this.job_applicant_education_infoDAO = job_applicant_education_infoDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Job_applicant_education_infoRepository.class);
	Map<Long, Job_applicant_education_infoDTO>mapOfJob_applicant_education_infoDTOToiD;
	Map<Long, Set<Job_applicant_education_infoDTO> >mapOfJob_applicant_education_infoDTOTojobApplicantId;
	Map<Long, Set<Job_applicant_education_infoDTO> >mapOfJob_applicant_education_infoDTOToeducationLevelType;
	Map<Long, Set<Job_applicant_education_infoDTO> >mapOfJob_applicant_education_infoDTOTodegreeExamType;
	Map<String, Set<Job_applicant_education_infoDTO> >mapOfJob_applicant_education_infoDTOTootherDegreeExam;
	Map<String, Set<Job_applicant_education_infoDTO> >mapOfJob_applicant_education_infoDTOTomajor;
	Map<Long, Set<Job_applicant_education_infoDTO> >mapOfJob_applicant_education_infoDTOToresultExamType;
	Map<String, Set<Job_applicant_education_infoDTO> >mapOfJob_applicant_education_infoDTOTomarksPercentage;
	Map<Integer, Set<Job_applicant_education_infoDTO> >mapOfJob_applicant_education_infoDTOTogradePointCat;
	Map<Double, Set<Job_applicant_education_infoDTO> >mapOfJob_applicant_education_infoDTOTocgpaNumber;
	Map<Integer, Set<Job_applicant_education_infoDTO> >mapOfJob_applicant_education_infoDTOToyearOfPassingNumber;
	Map<Long, Set<Job_applicant_education_infoDTO> >mapOfJob_applicant_education_infoDTOToboardType;
	Map<String, Set<Job_applicant_education_infoDTO> >mapOfJob_applicant_education_infoDTOToinstitutionName;
	Map<Boolean, Set<Job_applicant_education_infoDTO> >mapOfJob_applicant_education_infoDTOToisForeign;
	Map<Integer, Set<Job_applicant_education_infoDTO> >mapOfJob_applicant_education_infoDTOTodurationYears;
	Map<String, Set<Job_applicant_education_infoDTO> >mapOfJob_applicant_education_infoDTOToachievement;
	Map<Long, Set<Job_applicant_education_infoDTO> >mapOfJob_applicant_education_infoDTOTofilesDropzone;
	Map<Long, Set<Job_applicant_education_infoDTO> >mapOfJob_applicant_education_infoDTOToinsertionDate;
	Map<String, Set<Job_applicant_education_infoDTO> >mapOfJob_applicant_education_infoDTOToinsertedBy;
	Map<String, Set<Job_applicant_education_infoDTO> >mapOfJob_applicant_education_infoDTOTomodifiedBy;
	Map<Long, Set<Job_applicant_education_infoDTO> >mapOfJob_applicant_education_infoDTOTolastModificationTime;


	static Job_applicant_education_infoRepository instance = null;  
	private Job_applicant_education_infoRepository(){
		mapOfJob_applicant_education_infoDTOToiD = new ConcurrentHashMap<>();
		mapOfJob_applicant_education_infoDTOTojobApplicantId = new ConcurrentHashMap<>();
		mapOfJob_applicant_education_infoDTOToeducationLevelType = new ConcurrentHashMap<>();
		mapOfJob_applicant_education_infoDTOTodegreeExamType = new ConcurrentHashMap<>();
		mapOfJob_applicant_education_infoDTOTootherDegreeExam = new ConcurrentHashMap<>();
		mapOfJob_applicant_education_infoDTOTomajor = new ConcurrentHashMap<>();
		mapOfJob_applicant_education_infoDTOToresultExamType = new ConcurrentHashMap<>();
		mapOfJob_applicant_education_infoDTOTomarksPercentage = new ConcurrentHashMap<>();
		mapOfJob_applicant_education_infoDTOTogradePointCat = new ConcurrentHashMap<>();
		mapOfJob_applicant_education_infoDTOTocgpaNumber = new ConcurrentHashMap<>();
		mapOfJob_applicant_education_infoDTOToyearOfPassingNumber = new ConcurrentHashMap<>();
		mapOfJob_applicant_education_infoDTOToboardType = new ConcurrentHashMap<>();
		mapOfJob_applicant_education_infoDTOToinstitutionName = new ConcurrentHashMap<>();
		mapOfJob_applicant_education_infoDTOToisForeign = new ConcurrentHashMap<>();
		mapOfJob_applicant_education_infoDTOTodurationYears = new ConcurrentHashMap<>();
		mapOfJob_applicant_education_infoDTOToachievement = new ConcurrentHashMap<>();
		mapOfJob_applicant_education_infoDTOTofilesDropzone = new ConcurrentHashMap<>();
		mapOfJob_applicant_education_infoDTOToinsertionDate = new ConcurrentHashMap<>();
		mapOfJob_applicant_education_infoDTOToinsertedBy = new ConcurrentHashMap<>();
		mapOfJob_applicant_education_infoDTOTomodifiedBy = new ConcurrentHashMap<>();
		mapOfJob_applicant_education_infoDTOTolastModificationTime = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Job_applicant_education_infoRepository getInstance(){
		if (instance == null){
			instance = new Job_applicant_education_infoRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(job_applicant_education_infoDAO == null)
		{
			return;
		}
		try {
			List<Job_applicant_education_infoDTO> job_applicant_education_infoDTOs = job_applicant_education_infoDAO.getAllJob_applicant_education_info(reloadAll);
			for(Job_applicant_education_infoDTO job_applicant_education_infoDTO : job_applicant_education_infoDTOs) {
				Job_applicant_education_infoDTO oldJob_applicant_education_infoDTO = getJob_applicant_education_infoDTOByID(job_applicant_education_infoDTO.iD);
				if( oldJob_applicant_education_infoDTO != null ) {
					mapOfJob_applicant_education_infoDTOToiD.remove(oldJob_applicant_education_infoDTO.iD);
				
					if(mapOfJob_applicant_education_infoDTOTojobApplicantId.containsKey(oldJob_applicant_education_infoDTO.jobApplicantId)) {
						mapOfJob_applicant_education_infoDTOTojobApplicantId.get(oldJob_applicant_education_infoDTO.jobApplicantId).remove(oldJob_applicant_education_infoDTO);
					}
					if(mapOfJob_applicant_education_infoDTOTojobApplicantId.get(oldJob_applicant_education_infoDTO.jobApplicantId).isEmpty()) {
						mapOfJob_applicant_education_infoDTOTojobApplicantId.remove(oldJob_applicant_education_infoDTO.jobApplicantId);
					}
					
					if(mapOfJob_applicant_education_infoDTOToeducationLevelType.containsKey(oldJob_applicant_education_infoDTO.educationLevelType)) {
						mapOfJob_applicant_education_infoDTOToeducationLevelType.get(oldJob_applicant_education_infoDTO.educationLevelType).remove(oldJob_applicant_education_infoDTO);
					}
					if(mapOfJob_applicant_education_infoDTOToeducationLevelType.get(oldJob_applicant_education_infoDTO.educationLevelType).isEmpty()) {
						mapOfJob_applicant_education_infoDTOToeducationLevelType.remove(oldJob_applicant_education_infoDTO.educationLevelType);
					}
					
					if(mapOfJob_applicant_education_infoDTOTodegreeExamType.containsKey(oldJob_applicant_education_infoDTO.degreeExamType)) {
						mapOfJob_applicant_education_infoDTOTodegreeExamType.get(oldJob_applicant_education_infoDTO.degreeExamType).remove(oldJob_applicant_education_infoDTO);
					}
					if(mapOfJob_applicant_education_infoDTOTodegreeExamType.get(oldJob_applicant_education_infoDTO.degreeExamType).isEmpty()) {
						mapOfJob_applicant_education_infoDTOTodegreeExamType.remove(oldJob_applicant_education_infoDTO.degreeExamType);
					}
					
					if(mapOfJob_applicant_education_infoDTOTootherDegreeExam.containsKey(oldJob_applicant_education_infoDTO.otherDegreeExam)) {
						mapOfJob_applicant_education_infoDTOTootherDegreeExam.get(oldJob_applicant_education_infoDTO.otherDegreeExam).remove(oldJob_applicant_education_infoDTO);
					}
					if(mapOfJob_applicant_education_infoDTOTootherDegreeExam.get(oldJob_applicant_education_infoDTO.otherDegreeExam).isEmpty()) {
						mapOfJob_applicant_education_infoDTOTootherDegreeExam.remove(oldJob_applicant_education_infoDTO.otherDegreeExam);
					}
					
					if(mapOfJob_applicant_education_infoDTOTomajor.containsKey(oldJob_applicant_education_infoDTO.major)) {
						mapOfJob_applicant_education_infoDTOTomajor.get(oldJob_applicant_education_infoDTO.major).remove(oldJob_applicant_education_infoDTO);
					}
					if(mapOfJob_applicant_education_infoDTOTomajor.get(oldJob_applicant_education_infoDTO.major).isEmpty()) {
						mapOfJob_applicant_education_infoDTOTomajor.remove(oldJob_applicant_education_infoDTO.major);
					}
					
					if(mapOfJob_applicant_education_infoDTOToresultExamType.containsKey(oldJob_applicant_education_infoDTO.resultExamType)) {
						mapOfJob_applicant_education_infoDTOToresultExamType.get(oldJob_applicant_education_infoDTO.resultExamType).remove(oldJob_applicant_education_infoDTO);
					}
					if(mapOfJob_applicant_education_infoDTOToresultExamType.get(oldJob_applicant_education_infoDTO.resultExamType).isEmpty()) {
						mapOfJob_applicant_education_infoDTOToresultExamType.remove(oldJob_applicant_education_infoDTO.resultExamType);
					}
					
					if(mapOfJob_applicant_education_infoDTOTomarksPercentage.containsKey(oldJob_applicant_education_infoDTO.marksPercentage)) {
						mapOfJob_applicant_education_infoDTOTomarksPercentage.get(oldJob_applicant_education_infoDTO.marksPercentage).remove(oldJob_applicant_education_infoDTO);
					}
					if(mapOfJob_applicant_education_infoDTOTomarksPercentage.get(oldJob_applicant_education_infoDTO.marksPercentage).isEmpty()) {
						mapOfJob_applicant_education_infoDTOTomarksPercentage.remove(oldJob_applicant_education_infoDTO.marksPercentage);
					}
					
					if(mapOfJob_applicant_education_infoDTOTogradePointCat.containsKey(oldJob_applicant_education_infoDTO.gradePointCat)) {
						mapOfJob_applicant_education_infoDTOTogradePointCat.get(oldJob_applicant_education_infoDTO.gradePointCat).remove(oldJob_applicant_education_infoDTO);
					}
					if(mapOfJob_applicant_education_infoDTOTogradePointCat.get(oldJob_applicant_education_infoDTO.gradePointCat).isEmpty()) {
						mapOfJob_applicant_education_infoDTOTogradePointCat.remove(oldJob_applicant_education_infoDTO.gradePointCat);
					}
					
					if(mapOfJob_applicant_education_infoDTOTocgpaNumber.containsKey(oldJob_applicant_education_infoDTO.cgpaNumber)) {
						mapOfJob_applicant_education_infoDTOTocgpaNumber.get(oldJob_applicant_education_infoDTO.cgpaNumber).remove(oldJob_applicant_education_infoDTO);
					}
					if(mapOfJob_applicant_education_infoDTOTocgpaNumber.get(oldJob_applicant_education_infoDTO.cgpaNumber).isEmpty()) {
						mapOfJob_applicant_education_infoDTOTocgpaNumber.remove(oldJob_applicant_education_infoDTO.cgpaNumber);
					}
					
					if(mapOfJob_applicant_education_infoDTOToyearOfPassingNumber.containsKey(oldJob_applicant_education_infoDTO.yearOfPassingNumber)) {
						mapOfJob_applicant_education_infoDTOToyearOfPassingNumber.get(oldJob_applicant_education_infoDTO.yearOfPassingNumber).remove(oldJob_applicant_education_infoDTO);
					}
					if(mapOfJob_applicant_education_infoDTOToyearOfPassingNumber.get(oldJob_applicant_education_infoDTO.yearOfPassingNumber).isEmpty()) {
						mapOfJob_applicant_education_infoDTOToyearOfPassingNumber.remove(oldJob_applicant_education_infoDTO.yearOfPassingNumber);
					}
					
					if(mapOfJob_applicant_education_infoDTOToboardType.containsKey(oldJob_applicant_education_infoDTO.boardType)) {
						mapOfJob_applicant_education_infoDTOToboardType.get(oldJob_applicant_education_infoDTO.boardType).remove(oldJob_applicant_education_infoDTO);
					}
					if(mapOfJob_applicant_education_infoDTOToboardType.get(oldJob_applicant_education_infoDTO.boardType).isEmpty()) {
						mapOfJob_applicant_education_infoDTOToboardType.remove(oldJob_applicant_education_infoDTO.boardType);
					}
					
					if(mapOfJob_applicant_education_infoDTOToinstitutionName.containsKey(oldJob_applicant_education_infoDTO.institutionName)) {
						mapOfJob_applicant_education_infoDTOToinstitutionName.get(oldJob_applicant_education_infoDTO.institutionName).remove(oldJob_applicant_education_infoDTO);
					}
					if(mapOfJob_applicant_education_infoDTOToinstitutionName.get(oldJob_applicant_education_infoDTO.institutionName).isEmpty()) {
						mapOfJob_applicant_education_infoDTOToinstitutionName.remove(oldJob_applicant_education_infoDTO.institutionName);
					}
					
					if(mapOfJob_applicant_education_infoDTOToisForeign.containsKey(oldJob_applicant_education_infoDTO.isForeign)) {
						mapOfJob_applicant_education_infoDTOToisForeign.get(oldJob_applicant_education_infoDTO.isForeign).remove(oldJob_applicant_education_infoDTO);
					}
					if(mapOfJob_applicant_education_infoDTOToisForeign.get(oldJob_applicant_education_infoDTO.isForeign).isEmpty()) {
						mapOfJob_applicant_education_infoDTOToisForeign.remove(oldJob_applicant_education_infoDTO.isForeign);
					}
					
					if(mapOfJob_applicant_education_infoDTOTodurationYears.containsKey(oldJob_applicant_education_infoDTO.durationYears)) {
						mapOfJob_applicant_education_infoDTOTodurationYears.get(oldJob_applicant_education_infoDTO.durationYears).remove(oldJob_applicant_education_infoDTO);
					}
					if(mapOfJob_applicant_education_infoDTOTodurationYears.get(oldJob_applicant_education_infoDTO.durationYears).isEmpty()) {
						mapOfJob_applicant_education_infoDTOTodurationYears.remove(oldJob_applicant_education_infoDTO.durationYears);
					}
					
					if(mapOfJob_applicant_education_infoDTOToachievement.containsKey(oldJob_applicant_education_infoDTO.achievement)) {
						mapOfJob_applicant_education_infoDTOToachievement.get(oldJob_applicant_education_infoDTO.achievement).remove(oldJob_applicant_education_infoDTO);
					}
					if(mapOfJob_applicant_education_infoDTOToachievement.get(oldJob_applicant_education_infoDTO.achievement).isEmpty()) {
						mapOfJob_applicant_education_infoDTOToachievement.remove(oldJob_applicant_education_infoDTO.achievement);
					}
					
					if(mapOfJob_applicant_education_infoDTOTofilesDropzone.containsKey(oldJob_applicant_education_infoDTO.filesDropzone)) {
						mapOfJob_applicant_education_infoDTOTofilesDropzone.get(oldJob_applicant_education_infoDTO.filesDropzone).remove(oldJob_applicant_education_infoDTO);
					}
					if(mapOfJob_applicant_education_infoDTOTofilesDropzone.get(oldJob_applicant_education_infoDTO.filesDropzone).isEmpty()) {
						mapOfJob_applicant_education_infoDTOTofilesDropzone.remove(oldJob_applicant_education_infoDTO.filesDropzone);
					}
					
					if(mapOfJob_applicant_education_infoDTOToinsertionDate.containsKey(oldJob_applicant_education_infoDTO.insertionDate)) {
						mapOfJob_applicant_education_infoDTOToinsertionDate.get(oldJob_applicant_education_infoDTO.insertionDate).remove(oldJob_applicant_education_infoDTO);
					}
					if(mapOfJob_applicant_education_infoDTOToinsertionDate.get(oldJob_applicant_education_infoDTO.insertionDate).isEmpty()) {
						mapOfJob_applicant_education_infoDTOToinsertionDate.remove(oldJob_applicant_education_infoDTO.insertionDate);
					}
					
					if(mapOfJob_applicant_education_infoDTOToinsertedBy.containsKey(oldJob_applicant_education_infoDTO.insertedBy)) {
						mapOfJob_applicant_education_infoDTOToinsertedBy.get(oldJob_applicant_education_infoDTO.insertedBy).remove(oldJob_applicant_education_infoDTO);
					}
					if(mapOfJob_applicant_education_infoDTOToinsertedBy.get(oldJob_applicant_education_infoDTO.insertedBy).isEmpty()) {
						mapOfJob_applicant_education_infoDTOToinsertedBy.remove(oldJob_applicant_education_infoDTO.insertedBy);
					}
					
					if(mapOfJob_applicant_education_infoDTOTomodifiedBy.containsKey(oldJob_applicant_education_infoDTO.modifiedBy)) {
						mapOfJob_applicant_education_infoDTOTomodifiedBy.get(oldJob_applicant_education_infoDTO.modifiedBy).remove(oldJob_applicant_education_infoDTO);
					}
					if(mapOfJob_applicant_education_infoDTOTomodifiedBy.get(oldJob_applicant_education_infoDTO.modifiedBy).isEmpty()) {
						mapOfJob_applicant_education_infoDTOTomodifiedBy.remove(oldJob_applicant_education_infoDTO.modifiedBy);
					}
					
					if(mapOfJob_applicant_education_infoDTOTolastModificationTime.containsKey(oldJob_applicant_education_infoDTO.lastModificationTime)) {
						mapOfJob_applicant_education_infoDTOTolastModificationTime.get(oldJob_applicant_education_infoDTO.lastModificationTime).remove(oldJob_applicant_education_infoDTO);
					}
					if(mapOfJob_applicant_education_infoDTOTolastModificationTime.get(oldJob_applicant_education_infoDTO.lastModificationTime).isEmpty()) {
						mapOfJob_applicant_education_infoDTOTolastModificationTime.remove(oldJob_applicant_education_infoDTO.lastModificationTime);
					}
					
					
				}
				if(job_applicant_education_infoDTO.isDeleted == 0) 
				{
					
					mapOfJob_applicant_education_infoDTOToiD.put(job_applicant_education_infoDTO.iD, job_applicant_education_infoDTO);
				
					if( ! mapOfJob_applicant_education_infoDTOTojobApplicantId.containsKey(job_applicant_education_infoDTO.jobApplicantId)) {
						mapOfJob_applicant_education_infoDTOTojobApplicantId.put(job_applicant_education_infoDTO.jobApplicantId, new HashSet<>());
					}
					mapOfJob_applicant_education_infoDTOTojobApplicantId.get(job_applicant_education_infoDTO.jobApplicantId).add(job_applicant_education_infoDTO);
					
					if( ! mapOfJob_applicant_education_infoDTOToeducationLevelType.containsKey(job_applicant_education_infoDTO.educationLevelType)) {
						mapOfJob_applicant_education_infoDTOToeducationLevelType.put(job_applicant_education_infoDTO.educationLevelType, new HashSet<>());
					}
					mapOfJob_applicant_education_infoDTOToeducationLevelType.get(job_applicant_education_infoDTO.educationLevelType).add(job_applicant_education_infoDTO);
					
					if( ! mapOfJob_applicant_education_infoDTOTodegreeExamType.containsKey(job_applicant_education_infoDTO.degreeExamType)) {
						mapOfJob_applicant_education_infoDTOTodegreeExamType.put(job_applicant_education_infoDTO.degreeExamType, new HashSet<>());
					}
					mapOfJob_applicant_education_infoDTOTodegreeExamType.get(job_applicant_education_infoDTO.degreeExamType).add(job_applicant_education_infoDTO);
					
					if( ! mapOfJob_applicant_education_infoDTOTootherDegreeExam.containsKey(job_applicant_education_infoDTO.otherDegreeExam)) {
						mapOfJob_applicant_education_infoDTOTootherDegreeExam.put(job_applicant_education_infoDTO.otherDegreeExam, new HashSet<>());
					}
					mapOfJob_applicant_education_infoDTOTootherDegreeExam.get(job_applicant_education_infoDTO.otherDegreeExam).add(job_applicant_education_infoDTO);
					
					if( ! mapOfJob_applicant_education_infoDTOTomajor.containsKey(job_applicant_education_infoDTO.major)) {
						mapOfJob_applicant_education_infoDTOTomajor.put(job_applicant_education_infoDTO.major, new HashSet<>());
					}
					mapOfJob_applicant_education_infoDTOTomajor.get(job_applicant_education_infoDTO.major).add(job_applicant_education_infoDTO);
					
					if( ! mapOfJob_applicant_education_infoDTOToresultExamType.containsKey(job_applicant_education_infoDTO.resultExamType)) {
						mapOfJob_applicant_education_infoDTOToresultExamType.put(job_applicant_education_infoDTO.resultExamType, new HashSet<>());
					}
					mapOfJob_applicant_education_infoDTOToresultExamType.get(job_applicant_education_infoDTO.resultExamType).add(job_applicant_education_infoDTO);
					
					if( ! mapOfJob_applicant_education_infoDTOTomarksPercentage.containsKey(job_applicant_education_infoDTO.marksPercentage)) {
						mapOfJob_applicant_education_infoDTOTomarksPercentage.put(job_applicant_education_infoDTO.marksPercentage, new HashSet<>());
					}
					mapOfJob_applicant_education_infoDTOTomarksPercentage.get(job_applicant_education_infoDTO.marksPercentage).add(job_applicant_education_infoDTO);
					
					if( ! mapOfJob_applicant_education_infoDTOTogradePointCat.containsKey(job_applicant_education_infoDTO.gradePointCat)) {
						mapOfJob_applicant_education_infoDTOTogradePointCat.put(job_applicant_education_infoDTO.gradePointCat, new HashSet<>());
					}
					mapOfJob_applicant_education_infoDTOTogradePointCat.get(job_applicant_education_infoDTO.gradePointCat).add(job_applicant_education_infoDTO);
					
					if( ! mapOfJob_applicant_education_infoDTOTocgpaNumber.containsKey(job_applicant_education_infoDTO.cgpaNumber)) {
						mapOfJob_applicant_education_infoDTOTocgpaNumber.put(job_applicant_education_infoDTO.cgpaNumber, new HashSet<>());
					}
					mapOfJob_applicant_education_infoDTOTocgpaNumber.get(job_applicant_education_infoDTO.cgpaNumber).add(job_applicant_education_infoDTO);
					
					if( ! mapOfJob_applicant_education_infoDTOToyearOfPassingNumber.containsKey(job_applicant_education_infoDTO.yearOfPassingNumber)) {
						mapOfJob_applicant_education_infoDTOToyearOfPassingNumber.put(job_applicant_education_infoDTO.yearOfPassingNumber, new HashSet<>());
					}
					mapOfJob_applicant_education_infoDTOToyearOfPassingNumber.get(job_applicant_education_infoDTO.yearOfPassingNumber).add(job_applicant_education_infoDTO);
					
					if( ! mapOfJob_applicant_education_infoDTOToboardType.containsKey(job_applicant_education_infoDTO.boardType)) {
						mapOfJob_applicant_education_infoDTOToboardType.put(job_applicant_education_infoDTO.boardType, new HashSet<>());
					}
					mapOfJob_applicant_education_infoDTOToboardType.get(job_applicant_education_infoDTO.boardType).add(job_applicant_education_infoDTO);
					
					if( ! mapOfJob_applicant_education_infoDTOToinstitutionName.containsKey(job_applicant_education_infoDTO.institutionName)) {
						mapOfJob_applicant_education_infoDTOToinstitutionName.put(job_applicant_education_infoDTO.institutionName, new HashSet<>());
					}
					mapOfJob_applicant_education_infoDTOToinstitutionName.get(job_applicant_education_infoDTO.institutionName).add(job_applicant_education_infoDTO);
					
					if( ! mapOfJob_applicant_education_infoDTOToisForeign.containsKey(job_applicant_education_infoDTO.isForeign)) {
						mapOfJob_applicant_education_infoDTOToisForeign.put(job_applicant_education_infoDTO.isForeign, new HashSet<>());
					}
					mapOfJob_applicant_education_infoDTOToisForeign.get(job_applicant_education_infoDTO.isForeign).add(job_applicant_education_infoDTO);
					
					if( ! mapOfJob_applicant_education_infoDTOTodurationYears.containsKey(job_applicant_education_infoDTO.durationYears)) {
						mapOfJob_applicant_education_infoDTOTodurationYears.put(job_applicant_education_infoDTO.durationYears, new HashSet<>());
					}
					mapOfJob_applicant_education_infoDTOTodurationYears.get(job_applicant_education_infoDTO.durationYears).add(job_applicant_education_infoDTO);
					
					if( ! mapOfJob_applicant_education_infoDTOToachievement.containsKey(job_applicant_education_infoDTO.achievement)) {
						mapOfJob_applicant_education_infoDTOToachievement.put(job_applicant_education_infoDTO.achievement, new HashSet<>());
					}
					mapOfJob_applicant_education_infoDTOToachievement.get(job_applicant_education_infoDTO.achievement).add(job_applicant_education_infoDTO);
					
					if( ! mapOfJob_applicant_education_infoDTOTofilesDropzone.containsKey(job_applicant_education_infoDTO.filesDropzone)) {
						mapOfJob_applicant_education_infoDTOTofilesDropzone.put(job_applicant_education_infoDTO.filesDropzone, new HashSet<>());
					}
					mapOfJob_applicant_education_infoDTOTofilesDropzone.get(job_applicant_education_infoDTO.filesDropzone).add(job_applicant_education_infoDTO);
					
					if( ! mapOfJob_applicant_education_infoDTOToinsertionDate.containsKey(job_applicant_education_infoDTO.insertionDate)) {
						mapOfJob_applicant_education_infoDTOToinsertionDate.put(job_applicant_education_infoDTO.insertionDate, new HashSet<>());
					}
					mapOfJob_applicant_education_infoDTOToinsertionDate.get(job_applicant_education_infoDTO.insertionDate).add(job_applicant_education_infoDTO);
					
					if( ! mapOfJob_applicant_education_infoDTOToinsertedBy.containsKey(job_applicant_education_infoDTO.insertedBy)) {
						mapOfJob_applicant_education_infoDTOToinsertedBy.put(job_applicant_education_infoDTO.insertedBy, new HashSet<>());
					}
					mapOfJob_applicant_education_infoDTOToinsertedBy.get(job_applicant_education_infoDTO.insertedBy).add(job_applicant_education_infoDTO);
					
					if( ! mapOfJob_applicant_education_infoDTOTomodifiedBy.containsKey(job_applicant_education_infoDTO.modifiedBy)) {
						mapOfJob_applicant_education_infoDTOTomodifiedBy.put(job_applicant_education_infoDTO.modifiedBy, new HashSet<>());
					}
					mapOfJob_applicant_education_infoDTOTomodifiedBy.get(job_applicant_education_infoDTO.modifiedBy).add(job_applicant_education_infoDTO);
					
					if( ! mapOfJob_applicant_education_infoDTOTolastModificationTime.containsKey(job_applicant_education_infoDTO.lastModificationTime)) {
						mapOfJob_applicant_education_infoDTOTolastModificationTime.put(job_applicant_education_infoDTO.lastModificationTime, new HashSet<>());
					}
					mapOfJob_applicant_education_infoDTOTolastModificationTime.get(job_applicant_education_infoDTO.lastModificationTime).add(job_applicant_education_infoDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Job_applicant_education_infoDTO> getJob_applicant_education_infoList() {
		List <Job_applicant_education_infoDTO> job_applicant_education_infos = new ArrayList<Job_applicant_education_infoDTO>(this.mapOfJob_applicant_education_infoDTOToiD.values());
		return job_applicant_education_infos;
	}
	
	
	public Job_applicant_education_infoDTO getJob_applicant_education_infoDTOByID( long ID){
		return mapOfJob_applicant_education_infoDTOToiD.get(ID);
	}
	
	
	public List<Job_applicant_education_infoDTO> getJob_applicant_education_infoDTOByjob_applicant_id(long job_applicant_id) {
		return new ArrayList<>( mapOfJob_applicant_education_infoDTOTojobApplicantId.getOrDefault(job_applicant_id,new HashSet<>()));
	}
	
	
	public List<Job_applicant_education_infoDTO> getJob_applicant_education_infoDTOByeducation_level_type(long education_level_type) {
		return new ArrayList<>( mapOfJob_applicant_education_infoDTOToeducationLevelType.getOrDefault(education_level_type,new HashSet<>()));
	}
	
	
	public List<Job_applicant_education_infoDTO> getJob_applicant_education_infoDTOBydegree_exam_type(long degree_exam_type) {
		return new ArrayList<>( mapOfJob_applicant_education_infoDTOTodegreeExamType.getOrDefault(degree_exam_type,new HashSet<>()));
	}
	
	
	public List<Job_applicant_education_infoDTO> getJob_applicant_education_infoDTOByother_degree_exam(String other_degree_exam) {
		return new ArrayList<>( mapOfJob_applicant_education_infoDTOTootherDegreeExam.getOrDefault(other_degree_exam,new HashSet<>()));
	}
	
	
	public List<Job_applicant_education_infoDTO> getJob_applicant_education_infoDTOBymajor(String major) {
		return new ArrayList<>( mapOfJob_applicant_education_infoDTOTomajor.getOrDefault(major,new HashSet<>()));
	}
	
	
	public List<Job_applicant_education_infoDTO> getJob_applicant_education_infoDTOByresult_exam_type(long result_exam_type) {
		return new ArrayList<>( mapOfJob_applicant_education_infoDTOToresultExamType.getOrDefault(result_exam_type,new HashSet<>()));
	}
	
	
	public List<Job_applicant_education_infoDTO> getJob_applicant_education_infoDTOBymarks_percentage(String marks_percentage) {
		return new ArrayList<>( mapOfJob_applicant_education_infoDTOTomarksPercentage.getOrDefault(marks_percentage,new HashSet<>()));
	}
	
	
	public List<Job_applicant_education_infoDTO> getJob_applicant_education_infoDTOBygrade_point_cat(int grade_point_cat) {
		return new ArrayList<>( mapOfJob_applicant_education_infoDTOTogradePointCat.getOrDefault(grade_point_cat,new HashSet<>()));
	}
	
	
	public List<Job_applicant_education_infoDTO> getJob_applicant_education_infoDTOBycgpa_number(double cgpa_number) {
		return new ArrayList<>( mapOfJob_applicant_education_infoDTOTocgpaNumber.getOrDefault(cgpa_number,new HashSet<>()));
	}
	
	
	public List<Job_applicant_education_infoDTO> getJob_applicant_education_infoDTOByyear_of_passing_number(int year_of_passing_number) {
		return new ArrayList<>( mapOfJob_applicant_education_infoDTOToyearOfPassingNumber.getOrDefault(year_of_passing_number,new HashSet<>()));
	}
	
	
	public List<Job_applicant_education_infoDTO> getJob_applicant_education_infoDTOByboard_type(long board_type) {
		return new ArrayList<>( mapOfJob_applicant_education_infoDTOToboardType.getOrDefault(board_type,new HashSet<>()));
	}
	
	
	public List<Job_applicant_education_infoDTO> getJob_applicant_education_infoDTOByinstitution_name(String institution_name) {
		return new ArrayList<>( mapOfJob_applicant_education_infoDTOToinstitutionName.getOrDefault(institution_name,new HashSet<>()));
	}
	
	
	public List<Job_applicant_education_infoDTO> getJob_applicant_education_infoDTOByis_foreign(boolean is_foreign) {
		return new ArrayList<>( mapOfJob_applicant_education_infoDTOToisForeign.getOrDefault(is_foreign,new HashSet<>()));
	}
	
	
	public List<Job_applicant_education_infoDTO> getJob_applicant_education_infoDTOByduration_years(int duration_years) {
		return new ArrayList<>( mapOfJob_applicant_education_infoDTOTodurationYears.getOrDefault(duration_years,new HashSet<>()));
	}
	
	
	public List<Job_applicant_education_infoDTO> getJob_applicant_education_infoDTOByachievement(String achievement) {
		return new ArrayList<>( mapOfJob_applicant_education_infoDTOToachievement.getOrDefault(achievement,new HashSet<>()));
	}
	
	
	public List<Job_applicant_education_infoDTO> getJob_applicant_education_infoDTOByfiles_dropzone(long files_dropzone) {
		return new ArrayList<>( mapOfJob_applicant_education_infoDTOTofilesDropzone.getOrDefault(files_dropzone,new HashSet<>()));
	}
	
	
	public List<Job_applicant_education_infoDTO> getJob_applicant_education_infoDTOByinsertion_date(long insertion_date) {
		return new ArrayList<>( mapOfJob_applicant_education_infoDTOToinsertionDate.getOrDefault(insertion_date,new HashSet<>()));
	}
	
	
	public List<Job_applicant_education_infoDTO> getJob_applicant_education_infoDTOByinserted_by(String inserted_by) {
		return new ArrayList<>( mapOfJob_applicant_education_infoDTOToinsertedBy.getOrDefault(inserted_by,new HashSet<>()));
	}
	
	
	public List<Job_applicant_education_infoDTO> getJob_applicant_education_infoDTOBymodified_by(String modified_by) {
		return new ArrayList<>( mapOfJob_applicant_education_infoDTOTomodifiedBy.getOrDefault(modified_by,new HashSet<>()));
	}
	
	
	public List<Job_applicant_education_infoDTO> getJob_applicant_education_infoDTOBylastModificationTime(long lastModificationTime) {
		return new ArrayList<>( mapOfJob_applicant_education_infoDTOTolastModificationTime.getOrDefault(lastModificationTime,new HashSet<>()));
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "job_applicant_education_info";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


