package job_applicant_education_info;

import files.FilesDTO;
import util.CommonDTO;

import java.util.ArrayList;
import java.util.List;


public class Job_applicant_education_infoSummaryDTO extends CommonDTO
{
	public long jobId = 0;
	public long jobApplicantId = 0;
	public long educationLevelType = 0;
	public long degreeExamType = 0;
	public String otherDegreeExam = "";
	public String major = "";
	public long resultExamType = 0;
	public String marksPercentage = "";
	public Integer gradePointCat;
	public Double cgpaNumber;
	public int yearOfPassingNumber = 0;
	public long boardType = 0;
	public String institutionName = "";
	public boolean isForeign = false;
	public int durationYears = 0;
	public String achievement = "";
	public long filesDropzone = 0;
	public long insertionDate = 0;
	public String insertedBy = "";
	public String modifiedBy = "";
	public String roll_no = "";
	public long institution_id = 0;

	public List<FilesDTO> filesDropzoneFiles = new ArrayList();

	@Override
	public String toString() {
            return "$Job_applicant_education_infoDTO[" +
            " iD = " + iD +
            " jobApplicantId = " + jobApplicantId +
            " educationLevelType = " + educationLevelType +
            " degreeExamType = " + degreeExamType +
            " otherDegreeExam = " + otherDegreeExam +
            " major = " + major +
            " resultExamType = " + resultExamType +
            " marksPercentage = " + marksPercentage +
            " gradePointCat = " + gradePointCat +
            " cgpaNumber = " + cgpaNumber +
            " yearOfPassingNumber = " + yearOfPassingNumber +
            " boardType = " + boardType +
            " institutionName = " + institutionName +
            " isForeign = " + isForeign +
            " durationYears = " + durationYears +
            " achievement = " + achievement +
            " filesDropzone = " + filesDropzone +
            " insertionDate = " + insertionDate +
            " insertedBy = " + insertedBy +
            " modifiedBy = " + modifiedBy +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}