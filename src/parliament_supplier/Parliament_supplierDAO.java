package parliament_supplier;


import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.sql.SQLException;
import common.ConnectionAndStatementUtil;
import org.apache.log4j.Logger;
import repository.RepositoryManager;
import util.*;
import user.UserDTO;

public class Parliament_supplierDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());

	public Parliament_supplierDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";
		commonMaps = new Parliament_supplierMAPS(tableName);
		columnNames = new String[] 
		{
			"ID",
			"name_en",
			"name_bn",
			"agreement_date",
			"files_dropzone",
			"trade_license_number",
			"trade_license_expiry_date",
			"supplier_code",
			"is_foreign",
			"supplier_bin",
			"supplier_address",
			"supplier_mobile_1",
			"supplier_mobile_2",
			"supplier_email",
			"remarks",
			"inserted_by_user_id",
			"inserted_by_organogram_id",
			"insertion_date",
			"search_column",
			"isDeleted",
			"lastModificationTime"
		};
	}
	
	public Parliament_supplierDAO()
	{
		this("parliament_supplier");		
	}
	
	public void setSearchColumn(Parliament_supplierDTO parliament_supplierDTO)
	{
		parliament_supplierDTO.searchColumn = "";
		parliament_supplierDTO.searchColumn += parliament_supplierDTO.nameEn + " ";
		parliament_supplierDTO.searchColumn += parliament_supplierDTO.nameBn + " ";
		
		
	}
	
	public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException
	{
		Parliament_supplierDTO parliament_supplierDTO = (Parliament_supplierDTO)commonDTO;
		int index = 1;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(parliament_supplierDTO);
		if(isInsert)
		{
			ps.setObject(index++,parliament_supplierDTO.iD);
		}
		ps.setObject(index++,parliament_supplierDTO.nameEn);
		ps.setObject(index++,parliament_supplierDTO.nameBn);
		ps.setObject(index++,parliament_supplierDTO.agreementDate);
		ps.setObject(index++,parliament_supplierDTO.filesDropzone);
		ps.setObject(index++,parliament_supplierDTO.tradeLicenseNumber);
		ps.setObject(index++,parliament_supplierDTO.tradeLicenseExpiryDate);
		ps.setObject(index++,parliament_supplierDTO.supplierCode);
		ps.setObject(index++,parliament_supplierDTO.isForeign);
		ps.setObject(index++,parliament_supplierDTO.supplierBin);
		ps.setObject(index++,parliament_supplierDTO.supplierAddress);
		ps.setObject(index++,parliament_supplierDTO.supplierMobile1);
		ps.setObject(index++,parliament_supplierDTO.supplierMobile2);
		ps.setObject(index++,parliament_supplierDTO.supplierEmail);
		ps.setObject(index++,parliament_supplierDTO.remarks);
		ps.setObject(index++,parliament_supplierDTO.insertedByUserId);
		ps.setObject(index++,parliament_supplierDTO.insertedByOrganogramId);
		ps.setObject(index++,parliament_supplierDTO.insertionDate);
		ps.setObject(index++,parliament_supplierDTO.searchColumn);
		if(isInsert)
		{
			ps.setObject(index++, 0);
			ps.setObject(index++, lastModificationTime);
		}
	}
	
	public Parliament_supplierDTO build(ResultSet rs)
	{
		try
		{
			Parliament_supplierDTO parliament_supplierDTO = new Parliament_supplierDTO();
			parliament_supplierDTO.iD = rs.getLong("ID");
			parliament_supplierDTO.nameEn = rs.getString("name_en");
			parliament_supplierDTO.nameBn = rs.getString("name_bn");
			parliament_supplierDTO.agreementDate = rs.getLong("agreement_date");
			parliament_supplierDTO.filesDropzone = rs.getLong("files_dropzone");
			parliament_supplierDTO.tradeLicenseNumber = rs.getString("trade_license_number");
			parliament_supplierDTO.tradeLicenseExpiryDate = rs.getLong("trade_license_expiry_date");
			parliament_supplierDTO.supplierCode = rs.getString("supplier_code");
			parliament_supplierDTO.isForeign = rs.getBoolean("is_foreign");
			parliament_supplierDTO.supplierBin = rs.getString("supplier_bin");
			parliament_supplierDTO.supplierAddress = rs.getString("supplier_address");
			parliament_supplierDTO.supplierMobile1 = rs.getString("supplier_mobile_1");
			parliament_supplierDTO.supplierMobile2 = rs.getString("supplier_mobile_2");
			parliament_supplierDTO.supplierEmail = rs.getString("supplier_email");
			parliament_supplierDTO.remarks = rs.getString("remarks");
			parliament_supplierDTO.insertedByUserId = rs.getLong("inserted_by_user_id");
			parliament_supplierDTO.insertedByOrganogramId = rs.getLong("inserted_by_organogram_id");
			parliament_supplierDTO.insertionDate = rs.getLong("insertion_date");
			parliament_supplierDTO.searchColumn = rs.getString("search_column");
			parliament_supplierDTO.isDeleted = rs.getInt("isDeleted");
			parliament_supplierDTO.lastModificationTime = rs.getLong("lastModificationTime");
			return parliament_supplierDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
	
	

	public Parliament_supplierDTO getDTOByID (long id)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID = " + id;
		Parliament_supplierDTO parliament_supplierDTO = ConnectionAndStatementUtil.getT(sql,this::build);
		return parliament_supplierDTO;
	}

	
	public List<Parliament_supplierDTO> getDTOs(Collection recordIDs)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID IN ( ";
		for(int i = 0;i<recordIDs.size();i++){
			if(i!=0){
				sql+=",";
			}
			sql+=((ArrayList)recordIDs).get(i);
		}
		sql+=")  order by lastModificationTime desc";
		printSql(sql);
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);		
	}
	
	public List<Parliament_supplierDTO> getAllParliament_supplier (boolean isFirstReload)
    {
		String sql = "SELECT * FROM " + tableName + " WHERE ";
		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by " + tableName + ".lastModificationTime desc";
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);	
    }

	
	public List<Parliament_supplierDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO)
	{
		return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
	}

	
	public List<Parliament_supplierDTO> getDTOs(Hashtable  p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
			String filter, boolean tableHasJobCat)
	{
		String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat);
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);			
	}
	public String getSqlWithSearchCriteriaForNormalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
			UserDTO userDTO, String filter, boolean tableHasJobCat)
    {
		boolean viewAll = false;
		
		if(p_searchCriteria != null && p_searchCriteria.get("ViewAll") != null)
		{
			System.out.println("ViewAll = " + p_searchCriteria.get("ViewAll"));
			viewAll = true;
		}
		else
		{
			System.out.println("ViewAll is null ");
		}
		
		String sql = "SELECT ";
		if(category == GETIDS)
		{
			sql += " " + tableName + ".ID as ID ";
		}
		else if(category == GETCOUNT)
		{
			sql += " count( " + tableName + ".ID) as countID ";
		}
		else if(category == GETDTOS)
		{
			sql += " distinct " + tableName + ".* ";
		}
		sql += "FROM " + tableName + " ";
		sql += joinSQL;
		

		
		String AnyfieldSql = "";
		String AllFieldSql = "";
		
		if(p_searchCriteria != null)
		{
						
			
			Enumeration names = p_searchCriteria.keys();
			String str, value;
			
			AnyfieldSql = "(";
			
			if(p_searchCriteria.get("AnyField")!= null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase(""))
			{
				AnyfieldSql+= tableName + ".search_column like '%" + p_searchCriteria.get("AnyField").toString() + "%'";			
			}
			AnyfieldSql += ")";
			System.out.println("AnyfieldSql = " + AnyfieldSql);
			
			AllFieldSql = "(";
			int i = 0;
			while(names.hasMoreElements())
			{				
				str = (String) names.nextElement();
				value = (String)p_searchCriteria.get(str);
		        System.out.println(str + ": " + value);
				if(value != null && !value.equalsIgnoreCase("") && (
						 str.equals("name_en")
						|| str.equals("name_bn")
						|| str.equals("agreement_date_start")
						|| str.equals("agreement_date_end")
					

				)
						
				)
				{
					if(p_searchCriteria.get(str).equals("any"))
					{
						continue;
					}

					if( i > 0)
					{
						AllFieldSql+= " AND  ";
					}
					
					 if(str.equals("name_en"))
					{
						AllFieldSql += "" + tableName + ".name_en like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("name_bn"))
					{
						AllFieldSql += "" + tableName + ".name_bn like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("agreement_date_start"))
					{
						AllFieldSql += "" + tableName + ".agreement_date >= " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("agreement_date_end"))
					{
						AllFieldSql += "" + tableName + ".agreement_date <= " + p_searchCriteria.get(str);
						i ++;
					}
					
					
					
				}
			}
			
			AllFieldSql += ")";
			System.out.println("AllFieldSql = " + AllFieldSql);
			
			
		}
		
		
		sql += " WHERE ";
		
		sql += " (" + tableName + ".isDeleted = 0 ";			
		sql += ")";
		
		
		if(!filter.equalsIgnoreCase(""))
		{
			sql += " and " + filter + " ";
		}
		
		if(!AnyfieldSql.equals("()") && !AnyfieldSql.equals(""))
		{
			sql += " AND " + AnyfieldSql;
			
		}
		if(!AllFieldSql.equals("()") && !AllFieldSql.equals(""))
		{			
			sql += " AND " + AllFieldSql;
		}
		
	
			
		sql += " order by " + tableName + ".lastModificationTime desc ";

		printSql(sql);
		
		if(limit >= 0)
		{
			sql += " limit " + limit;
		}
		if(offset >= 0)
		{
			sql += " offset " + offset;
		}
		
		System.out.println("-------------- sql = " + sql);
		
		return sql;
    }
	
	public String getSqlWithSearchCriteria(Hashtable p_searchCriteria, int limit, int offset, int category,
			boolean isPermanentTable, UserDTO userDTO, String filter, boolean tableHasJobCat)
    {		
		return getSqlWithSearchCriteriaForNormalSearch(p_searchCriteria, limit, offset, category, userDTO, filter, tableHasJobCat);	
    }
				
}
	