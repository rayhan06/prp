package parliament_supplier;
import java.util.*; 
import util.*; 


public class Parliament_supplierDTO extends CommonDTO
{

    public String nameEn = "";
    public String nameBn = "";
	public long agreementDate = System.currentTimeMillis();
	public long filesDropzone = -1;
    public String tradeLicenseNumber = "";
	public long tradeLicenseExpiryDate = System.currentTimeMillis();
    public String supplierCode = "";
	public boolean isForeign = false;
    public String supplierBin = "";
    public String supplierAddress = "";
    public String supplierMobile1 = "";
    public String supplierMobile2 = "";
    public String supplierEmail = "";
    public String remarks = "";
	public long insertedByUserId = -1;
	public long insertedByOrganogramId = -1;
	public long insertionDate = -1;
	
	
    @Override
	public String toString() {
            return "$Parliament_supplierDTO[" +
            " iD = " + iD +
            " nameEn = " + nameEn +
            " nameBn = " + nameBn +
            " agreementDate = " + agreementDate +
            " filesDropzone = " + filesDropzone +
            " tradeLicenseNumber = " + tradeLicenseNumber +
            " tradeLicenseExpiryDate = " + tradeLicenseExpiryDate +
            " supplierCode = " + supplierCode +
            " isForeign = " + isForeign +
            " supplierBin = " + supplierBin +
            " supplierAddress = " + supplierAddress +
            " supplierMobile1 = " + supplierMobile1 +
            " supplierMobile2 = " + supplierMobile2 +
            " supplierEmail = " + supplierEmail +
            " remarks = " + remarks +
            " insertedByUserId = " + insertedByUserId +
            " insertedByOrganogramId = " + insertedByOrganogramId +
            " insertionDate = " + insertionDate +
            " searchColumn = " + searchColumn +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}