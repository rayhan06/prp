package parliament_supplier;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class Parliament_supplierRepository implements Repository {
	Parliament_supplierDAO parliament_supplierDAO = null;
	
	public void setDAO(Parliament_supplierDAO parliament_supplierDAO)
	{
		this.parliament_supplierDAO = parliament_supplierDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Parliament_supplierRepository.class);
	Map<Long, Parliament_supplierDTO>mapOfParliament_supplierDTOToiD;
	Map<String, Set<Parliament_supplierDTO> >mapOfParliament_supplierDTOTonameEn;
	Map<String, Set<Parliament_supplierDTO> >mapOfParliament_supplierDTOTonameBn;
	Map<Long, Set<Parliament_supplierDTO> >mapOfParliament_supplierDTOToagreementDate;
	Map<Long, Set<Parliament_supplierDTO> >mapOfParliament_supplierDTOTofilesDropzone;
	Map<String, Set<Parliament_supplierDTO> >mapOfParliament_supplierDTOTotradeLicenseNumber;
	Map<Long, Set<Parliament_supplierDTO> >mapOfParliament_supplierDTOTotradeLicenseExpiryDate;
	Map<String, Set<Parliament_supplierDTO> >mapOfParliament_supplierDTOTosupplierCode;
	Map<Boolean, Set<Parliament_supplierDTO> >mapOfParliament_supplierDTOToisForeign;
	Map<String, Set<Parliament_supplierDTO> >mapOfParliament_supplierDTOTosupplierBin;
	Map<String, Set<Parliament_supplierDTO> >mapOfParliament_supplierDTOTosupplierAddress;
	Map<String, Set<Parliament_supplierDTO> >mapOfParliament_supplierDTOTosupplierMobile1;
	Map<String, Set<Parliament_supplierDTO> >mapOfParliament_supplierDTOTosupplierMobile2;
	Map<String, Set<Parliament_supplierDTO> >mapOfParliament_supplierDTOTosupplierEmail;
	Map<String, Set<Parliament_supplierDTO> >mapOfParliament_supplierDTOToremarks;
	Map<Long, Set<Parliament_supplierDTO> >mapOfParliament_supplierDTOToinsertedByUserId;
	Map<Long, Set<Parliament_supplierDTO> >mapOfParliament_supplierDTOToinsertedByOrganogramId;
	Map<Long, Set<Parliament_supplierDTO> >mapOfParliament_supplierDTOToinsertionDate;
	Map<String, Set<Parliament_supplierDTO> >mapOfParliament_supplierDTOTosearchColumn;
	Map<Long, Set<Parliament_supplierDTO> >mapOfParliament_supplierDTOTolastModificationTime;


	static Parliament_supplierRepository instance = null;  
	private Parliament_supplierRepository(){
		mapOfParliament_supplierDTOToiD = new ConcurrentHashMap<>();
		mapOfParliament_supplierDTOTonameEn = new ConcurrentHashMap<>();
		mapOfParliament_supplierDTOTonameBn = new ConcurrentHashMap<>();
		mapOfParliament_supplierDTOToagreementDate = new ConcurrentHashMap<>();
		mapOfParliament_supplierDTOTofilesDropzone = new ConcurrentHashMap<>();
		mapOfParliament_supplierDTOTotradeLicenseNumber = new ConcurrentHashMap<>();
		mapOfParliament_supplierDTOTotradeLicenseExpiryDate = new ConcurrentHashMap<>();
		mapOfParliament_supplierDTOTosupplierCode = new ConcurrentHashMap<>();
		mapOfParliament_supplierDTOToisForeign = new ConcurrentHashMap<>();
		mapOfParliament_supplierDTOTosupplierBin = new ConcurrentHashMap<>();
		mapOfParliament_supplierDTOTosupplierAddress = new ConcurrentHashMap<>();
		mapOfParliament_supplierDTOTosupplierMobile1 = new ConcurrentHashMap<>();
		mapOfParliament_supplierDTOTosupplierMobile2 = new ConcurrentHashMap<>();
		mapOfParliament_supplierDTOTosupplierEmail = new ConcurrentHashMap<>();
		mapOfParliament_supplierDTOToremarks = new ConcurrentHashMap<>();
		mapOfParliament_supplierDTOToinsertedByUserId = new ConcurrentHashMap<>();
		mapOfParliament_supplierDTOToinsertedByOrganogramId = new ConcurrentHashMap<>();
		mapOfParliament_supplierDTOToinsertionDate = new ConcurrentHashMap<>();
		mapOfParliament_supplierDTOTosearchColumn = new ConcurrentHashMap<>();
		mapOfParliament_supplierDTOTolastModificationTime = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Parliament_supplierRepository getInstance(){
		if (instance == null){
			instance = new Parliament_supplierRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(parliament_supplierDAO == null)
		{
			return;
		}
		try {
			List<Parliament_supplierDTO> parliament_supplierDTOs = parliament_supplierDAO.getAllParliament_supplier(reloadAll);
			for(Parliament_supplierDTO parliament_supplierDTO : parliament_supplierDTOs) {
				Parliament_supplierDTO oldParliament_supplierDTO = getParliament_supplierDTOByID(parliament_supplierDTO.iD);
				if( oldParliament_supplierDTO != null ) {
					mapOfParliament_supplierDTOToiD.remove(oldParliament_supplierDTO.iD);
				
					if(mapOfParliament_supplierDTOTonameEn.containsKey(oldParliament_supplierDTO.nameEn)) {
						mapOfParliament_supplierDTOTonameEn.get(oldParliament_supplierDTO.nameEn).remove(oldParliament_supplierDTO);
					}
					if(mapOfParliament_supplierDTOTonameEn.get(oldParliament_supplierDTO.nameEn).isEmpty()) {
						mapOfParliament_supplierDTOTonameEn.remove(oldParliament_supplierDTO.nameEn);
					}
					
					if(mapOfParliament_supplierDTOTonameBn.containsKey(oldParliament_supplierDTO.nameBn)) {
						mapOfParliament_supplierDTOTonameBn.get(oldParliament_supplierDTO.nameBn).remove(oldParliament_supplierDTO);
					}
					if(mapOfParliament_supplierDTOTonameBn.get(oldParliament_supplierDTO.nameBn).isEmpty()) {
						mapOfParliament_supplierDTOTonameBn.remove(oldParliament_supplierDTO.nameBn);
					}
					
					if(mapOfParliament_supplierDTOToagreementDate.containsKey(oldParliament_supplierDTO.agreementDate)) {
						mapOfParliament_supplierDTOToagreementDate.get(oldParliament_supplierDTO.agreementDate).remove(oldParliament_supplierDTO);
					}
					if(mapOfParliament_supplierDTOToagreementDate.get(oldParliament_supplierDTO.agreementDate).isEmpty()) {
						mapOfParliament_supplierDTOToagreementDate.remove(oldParliament_supplierDTO.agreementDate);
					}
					
					if(mapOfParliament_supplierDTOTofilesDropzone.containsKey(oldParliament_supplierDTO.filesDropzone)) {
						mapOfParliament_supplierDTOTofilesDropzone.get(oldParliament_supplierDTO.filesDropzone).remove(oldParliament_supplierDTO);
					}
					if(mapOfParliament_supplierDTOTofilesDropzone.get(oldParliament_supplierDTO.filesDropzone).isEmpty()) {
						mapOfParliament_supplierDTOTofilesDropzone.remove(oldParliament_supplierDTO.filesDropzone);
					}
					
					if(mapOfParliament_supplierDTOTotradeLicenseNumber.containsKey(oldParliament_supplierDTO.tradeLicenseNumber)) {
						mapOfParliament_supplierDTOTotradeLicenseNumber.get(oldParliament_supplierDTO.tradeLicenseNumber).remove(oldParliament_supplierDTO);
					}
					if(mapOfParliament_supplierDTOTotradeLicenseNumber.get(oldParliament_supplierDTO.tradeLicenseNumber).isEmpty()) {
						mapOfParliament_supplierDTOTotradeLicenseNumber.remove(oldParliament_supplierDTO.tradeLicenseNumber);
					}
					
					if(mapOfParliament_supplierDTOTotradeLicenseExpiryDate.containsKey(oldParliament_supplierDTO.tradeLicenseExpiryDate)) {
						mapOfParliament_supplierDTOTotradeLicenseExpiryDate.get(oldParliament_supplierDTO.tradeLicenseExpiryDate).remove(oldParliament_supplierDTO);
					}
					if(mapOfParliament_supplierDTOTotradeLicenseExpiryDate.get(oldParliament_supplierDTO.tradeLicenseExpiryDate).isEmpty()) {
						mapOfParliament_supplierDTOTotradeLicenseExpiryDate.remove(oldParliament_supplierDTO.tradeLicenseExpiryDate);
					}
					
					if(mapOfParliament_supplierDTOTosupplierCode.containsKey(oldParliament_supplierDTO.supplierCode)) {
						mapOfParliament_supplierDTOTosupplierCode.get(oldParliament_supplierDTO.supplierCode).remove(oldParliament_supplierDTO);
					}
					if(mapOfParliament_supplierDTOTosupplierCode.get(oldParliament_supplierDTO.supplierCode).isEmpty()) {
						mapOfParliament_supplierDTOTosupplierCode.remove(oldParliament_supplierDTO.supplierCode);
					}
					
					if(mapOfParliament_supplierDTOToisForeign.containsKey(oldParliament_supplierDTO.isForeign)) {
						mapOfParliament_supplierDTOToisForeign.get(oldParliament_supplierDTO.isForeign).remove(oldParliament_supplierDTO);
					}
					if(mapOfParliament_supplierDTOToisForeign.get(oldParliament_supplierDTO.isForeign).isEmpty()) {
						mapOfParliament_supplierDTOToisForeign.remove(oldParliament_supplierDTO.isForeign);
					}
					
					if(mapOfParliament_supplierDTOTosupplierBin.containsKey(oldParliament_supplierDTO.supplierBin)) {
						mapOfParliament_supplierDTOTosupplierBin.get(oldParliament_supplierDTO.supplierBin).remove(oldParliament_supplierDTO);
					}
					if(mapOfParliament_supplierDTOTosupplierBin.get(oldParliament_supplierDTO.supplierBin).isEmpty()) {
						mapOfParliament_supplierDTOTosupplierBin.remove(oldParliament_supplierDTO.supplierBin);
					}
					
					if(mapOfParliament_supplierDTOTosupplierAddress.containsKey(oldParliament_supplierDTO.supplierAddress)) {
						mapOfParliament_supplierDTOTosupplierAddress.get(oldParliament_supplierDTO.supplierAddress).remove(oldParliament_supplierDTO);
					}
					if(mapOfParliament_supplierDTOTosupplierAddress.get(oldParliament_supplierDTO.supplierAddress).isEmpty()) {
						mapOfParliament_supplierDTOTosupplierAddress.remove(oldParliament_supplierDTO.supplierAddress);
					}
					
					if(mapOfParliament_supplierDTOTosupplierMobile1.containsKey(oldParliament_supplierDTO.supplierMobile1)) {
						mapOfParliament_supplierDTOTosupplierMobile1.get(oldParliament_supplierDTO.supplierMobile1).remove(oldParliament_supplierDTO);
					}
					if(mapOfParliament_supplierDTOTosupplierMobile1.get(oldParliament_supplierDTO.supplierMobile1).isEmpty()) {
						mapOfParliament_supplierDTOTosupplierMobile1.remove(oldParliament_supplierDTO.supplierMobile1);
					}
					
					if(mapOfParliament_supplierDTOTosupplierMobile2.containsKey(oldParliament_supplierDTO.supplierMobile2)) {
						mapOfParliament_supplierDTOTosupplierMobile2.get(oldParliament_supplierDTO.supplierMobile2).remove(oldParliament_supplierDTO);
					}
					if(mapOfParliament_supplierDTOTosupplierMobile2.get(oldParliament_supplierDTO.supplierMobile2).isEmpty()) {
						mapOfParliament_supplierDTOTosupplierMobile2.remove(oldParliament_supplierDTO.supplierMobile2);
					}
					
					if(mapOfParliament_supplierDTOTosupplierEmail.containsKey(oldParliament_supplierDTO.supplierEmail)) {
						mapOfParliament_supplierDTOTosupplierEmail.get(oldParliament_supplierDTO.supplierEmail).remove(oldParliament_supplierDTO);
					}
					if(mapOfParliament_supplierDTOTosupplierEmail.get(oldParliament_supplierDTO.supplierEmail).isEmpty()) {
						mapOfParliament_supplierDTOTosupplierEmail.remove(oldParliament_supplierDTO.supplierEmail);
					}
					
					if(mapOfParliament_supplierDTOToremarks.containsKey(oldParliament_supplierDTO.remarks)) {
						mapOfParliament_supplierDTOToremarks.get(oldParliament_supplierDTO.remarks).remove(oldParliament_supplierDTO);
					}
					if(mapOfParliament_supplierDTOToremarks.get(oldParliament_supplierDTO.remarks).isEmpty()) {
						mapOfParliament_supplierDTOToremarks.remove(oldParliament_supplierDTO.remarks);
					}
					
					if(mapOfParliament_supplierDTOToinsertedByUserId.containsKey(oldParliament_supplierDTO.insertedByUserId)) {
						mapOfParliament_supplierDTOToinsertedByUserId.get(oldParliament_supplierDTO.insertedByUserId).remove(oldParliament_supplierDTO);
					}
					if(mapOfParliament_supplierDTOToinsertedByUserId.get(oldParliament_supplierDTO.insertedByUserId).isEmpty()) {
						mapOfParliament_supplierDTOToinsertedByUserId.remove(oldParliament_supplierDTO.insertedByUserId);
					}
					
					if(mapOfParliament_supplierDTOToinsertedByOrganogramId.containsKey(oldParliament_supplierDTO.insertedByOrganogramId)) {
						mapOfParliament_supplierDTOToinsertedByOrganogramId.get(oldParliament_supplierDTO.insertedByOrganogramId).remove(oldParliament_supplierDTO);
					}
					if(mapOfParliament_supplierDTOToinsertedByOrganogramId.get(oldParliament_supplierDTO.insertedByOrganogramId).isEmpty()) {
						mapOfParliament_supplierDTOToinsertedByOrganogramId.remove(oldParliament_supplierDTO.insertedByOrganogramId);
					}
					
					if(mapOfParliament_supplierDTOToinsertionDate.containsKey(oldParliament_supplierDTO.insertionDate)) {
						mapOfParliament_supplierDTOToinsertionDate.get(oldParliament_supplierDTO.insertionDate).remove(oldParliament_supplierDTO);
					}
					if(mapOfParliament_supplierDTOToinsertionDate.get(oldParliament_supplierDTO.insertionDate).isEmpty()) {
						mapOfParliament_supplierDTOToinsertionDate.remove(oldParliament_supplierDTO.insertionDate);
					}
					
					if(mapOfParliament_supplierDTOTosearchColumn.containsKey(oldParliament_supplierDTO.searchColumn)) {
						mapOfParliament_supplierDTOTosearchColumn.get(oldParliament_supplierDTO.searchColumn).remove(oldParliament_supplierDTO);
					}
					if(mapOfParliament_supplierDTOTosearchColumn.get(oldParliament_supplierDTO.searchColumn).isEmpty()) {
						mapOfParliament_supplierDTOTosearchColumn.remove(oldParliament_supplierDTO.searchColumn);
					}
					
					if(mapOfParliament_supplierDTOTolastModificationTime.containsKey(oldParliament_supplierDTO.lastModificationTime)) {
						mapOfParliament_supplierDTOTolastModificationTime.get(oldParliament_supplierDTO.lastModificationTime).remove(oldParliament_supplierDTO);
					}
					if(mapOfParliament_supplierDTOTolastModificationTime.get(oldParliament_supplierDTO.lastModificationTime).isEmpty()) {
						mapOfParliament_supplierDTOTolastModificationTime.remove(oldParliament_supplierDTO.lastModificationTime);
					}
					
					
				}
				if(parliament_supplierDTO.isDeleted == 0) 
				{
					
					mapOfParliament_supplierDTOToiD.put(parliament_supplierDTO.iD, parliament_supplierDTO);
				
					if( ! mapOfParliament_supplierDTOTonameEn.containsKey(parliament_supplierDTO.nameEn)) {
						mapOfParliament_supplierDTOTonameEn.put(parliament_supplierDTO.nameEn, new HashSet<>());
					}
					mapOfParliament_supplierDTOTonameEn.get(parliament_supplierDTO.nameEn).add(parliament_supplierDTO);
					
					if( ! mapOfParliament_supplierDTOTonameBn.containsKey(parliament_supplierDTO.nameBn)) {
						mapOfParliament_supplierDTOTonameBn.put(parliament_supplierDTO.nameBn, new HashSet<>());
					}
					mapOfParliament_supplierDTOTonameBn.get(parliament_supplierDTO.nameBn).add(parliament_supplierDTO);
					
					if( ! mapOfParliament_supplierDTOToagreementDate.containsKey(parliament_supplierDTO.agreementDate)) {
						mapOfParliament_supplierDTOToagreementDate.put(parliament_supplierDTO.agreementDate, new HashSet<>());
					}
					mapOfParliament_supplierDTOToagreementDate.get(parliament_supplierDTO.agreementDate).add(parliament_supplierDTO);
					
					if( ! mapOfParliament_supplierDTOTofilesDropzone.containsKey(parliament_supplierDTO.filesDropzone)) {
						mapOfParliament_supplierDTOTofilesDropzone.put(parliament_supplierDTO.filesDropzone, new HashSet<>());
					}
					mapOfParliament_supplierDTOTofilesDropzone.get(parliament_supplierDTO.filesDropzone).add(parliament_supplierDTO);
					
					if( ! mapOfParliament_supplierDTOTotradeLicenseNumber.containsKey(parliament_supplierDTO.tradeLicenseNumber)) {
						mapOfParliament_supplierDTOTotradeLicenseNumber.put(parliament_supplierDTO.tradeLicenseNumber, new HashSet<>());
					}
					mapOfParliament_supplierDTOTotradeLicenseNumber.get(parliament_supplierDTO.tradeLicenseNumber).add(parliament_supplierDTO);
					
					if( ! mapOfParliament_supplierDTOTotradeLicenseExpiryDate.containsKey(parliament_supplierDTO.tradeLicenseExpiryDate)) {
						mapOfParliament_supplierDTOTotradeLicenseExpiryDate.put(parliament_supplierDTO.tradeLicenseExpiryDate, new HashSet<>());
					}
					mapOfParliament_supplierDTOTotradeLicenseExpiryDate.get(parliament_supplierDTO.tradeLicenseExpiryDate).add(parliament_supplierDTO);
					
					if( ! mapOfParliament_supplierDTOTosupplierCode.containsKey(parliament_supplierDTO.supplierCode)) {
						mapOfParliament_supplierDTOTosupplierCode.put(parliament_supplierDTO.supplierCode, new HashSet<>());
					}
					mapOfParliament_supplierDTOTosupplierCode.get(parliament_supplierDTO.supplierCode).add(parliament_supplierDTO);
					
					if( ! mapOfParliament_supplierDTOToisForeign.containsKey(parliament_supplierDTO.isForeign)) {
						mapOfParliament_supplierDTOToisForeign.put(parliament_supplierDTO.isForeign, new HashSet<>());
					}
					mapOfParliament_supplierDTOToisForeign.get(parliament_supplierDTO.isForeign).add(parliament_supplierDTO);
					
					if( ! mapOfParliament_supplierDTOTosupplierBin.containsKey(parliament_supplierDTO.supplierBin)) {
						mapOfParliament_supplierDTOTosupplierBin.put(parliament_supplierDTO.supplierBin, new HashSet<>());
					}
					mapOfParliament_supplierDTOTosupplierBin.get(parliament_supplierDTO.supplierBin).add(parliament_supplierDTO);
					
					if( ! mapOfParliament_supplierDTOTosupplierAddress.containsKey(parliament_supplierDTO.supplierAddress)) {
						mapOfParliament_supplierDTOTosupplierAddress.put(parliament_supplierDTO.supplierAddress, new HashSet<>());
					}
					mapOfParliament_supplierDTOTosupplierAddress.get(parliament_supplierDTO.supplierAddress).add(parliament_supplierDTO);
					
					if( ! mapOfParliament_supplierDTOTosupplierMobile1.containsKey(parliament_supplierDTO.supplierMobile1)) {
						mapOfParliament_supplierDTOTosupplierMobile1.put(parliament_supplierDTO.supplierMobile1, new HashSet<>());
					}
					mapOfParliament_supplierDTOTosupplierMobile1.get(parliament_supplierDTO.supplierMobile1).add(parliament_supplierDTO);
					
					if( ! mapOfParliament_supplierDTOTosupplierMobile2.containsKey(parliament_supplierDTO.supplierMobile2)) {
						mapOfParliament_supplierDTOTosupplierMobile2.put(parliament_supplierDTO.supplierMobile2, new HashSet<>());
					}
					mapOfParliament_supplierDTOTosupplierMobile2.get(parliament_supplierDTO.supplierMobile2).add(parliament_supplierDTO);
					
					if( ! mapOfParliament_supplierDTOTosupplierEmail.containsKey(parliament_supplierDTO.supplierEmail)) {
						mapOfParliament_supplierDTOTosupplierEmail.put(parliament_supplierDTO.supplierEmail, new HashSet<>());
					}
					mapOfParliament_supplierDTOTosupplierEmail.get(parliament_supplierDTO.supplierEmail).add(parliament_supplierDTO);
					
					if( ! mapOfParliament_supplierDTOToremarks.containsKey(parliament_supplierDTO.remarks)) {
						mapOfParliament_supplierDTOToremarks.put(parliament_supplierDTO.remarks, new HashSet<>());
					}
					mapOfParliament_supplierDTOToremarks.get(parliament_supplierDTO.remarks).add(parliament_supplierDTO);
					
					if( ! mapOfParliament_supplierDTOToinsertedByUserId.containsKey(parliament_supplierDTO.insertedByUserId)) {
						mapOfParliament_supplierDTOToinsertedByUserId.put(parliament_supplierDTO.insertedByUserId, new HashSet<>());
					}
					mapOfParliament_supplierDTOToinsertedByUserId.get(parliament_supplierDTO.insertedByUserId).add(parliament_supplierDTO);
					
					if( ! mapOfParliament_supplierDTOToinsertedByOrganogramId.containsKey(parliament_supplierDTO.insertedByOrganogramId)) {
						mapOfParliament_supplierDTOToinsertedByOrganogramId.put(parliament_supplierDTO.insertedByOrganogramId, new HashSet<>());
					}
					mapOfParliament_supplierDTOToinsertedByOrganogramId.get(parliament_supplierDTO.insertedByOrganogramId).add(parliament_supplierDTO);
					
					if( ! mapOfParliament_supplierDTOToinsertionDate.containsKey(parliament_supplierDTO.insertionDate)) {
						mapOfParliament_supplierDTOToinsertionDate.put(parliament_supplierDTO.insertionDate, new HashSet<>());
					}
					mapOfParliament_supplierDTOToinsertionDate.get(parliament_supplierDTO.insertionDate).add(parliament_supplierDTO);
					
					if( ! mapOfParliament_supplierDTOTosearchColumn.containsKey(parliament_supplierDTO.searchColumn)) {
						mapOfParliament_supplierDTOTosearchColumn.put(parliament_supplierDTO.searchColumn, new HashSet<>());
					}
					mapOfParliament_supplierDTOTosearchColumn.get(parliament_supplierDTO.searchColumn).add(parliament_supplierDTO);
					
					if( ! mapOfParliament_supplierDTOTolastModificationTime.containsKey(parliament_supplierDTO.lastModificationTime)) {
						mapOfParliament_supplierDTOTolastModificationTime.put(parliament_supplierDTO.lastModificationTime, new HashSet<>());
					}
					mapOfParliament_supplierDTOTolastModificationTime.get(parliament_supplierDTO.lastModificationTime).add(parliament_supplierDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Parliament_supplierDTO> getParliament_supplierList() {
		List <Parliament_supplierDTO> parliament_suppliers = new ArrayList<Parliament_supplierDTO>(this.mapOfParliament_supplierDTOToiD.values());
		return parliament_suppliers;
	}
	
	
	public Parliament_supplierDTO getParliament_supplierDTOByID( long ID){
		return mapOfParliament_supplierDTOToiD.get(ID);
	}
	
	
	public List<Parliament_supplierDTO> getParliament_supplierDTOByname_en(String name_en) {
		return new ArrayList<>( mapOfParliament_supplierDTOTonameEn.getOrDefault(name_en,new HashSet<>()));
	}
	
	
	public List<Parliament_supplierDTO> getParliament_supplierDTOByname_bn(String name_bn) {
		return new ArrayList<>( mapOfParliament_supplierDTOTonameBn.getOrDefault(name_bn,new HashSet<>()));
	}
	
	
	public List<Parliament_supplierDTO> getParliament_supplierDTOByagreement_date(long agreement_date) {
		return new ArrayList<>( mapOfParliament_supplierDTOToagreementDate.getOrDefault(agreement_date,new HashSet<>()));
	}
	
	
	public List<Parliament_supplierDTO> getParliament_supplierDTOByfiles_dropzone(long files_dropzone) {
		return new ArrayList<>( mapOfParliament_supplierDTOTofilesDropzone.getOrDefault(files_dropzone,new HashSet<>()));
	}
	
	
	public List<Parliament_supplierDTO> getParliament_supplierDTOBytrade_license_number(String trade_license_number) {
		return new ArrayList<>( mapOfParliament_supplierDTOTotradeLicenseNumber.getOrDefault(trade_license_number,new HashSet<>()));
	}
	
	
	public List<Parliament_supplierDTO> getParliament_supplierDTOBytrade_license_expiry_date(long trade_license_expiry_date) {
		return new ArrayList<>( mapOfParliament_supplierDTOTotradeLicenseExpiryDate.getOrDefault(trade_license_expiry_date,new HashSet<>()));
	}
	
	
	public List<Parliament_supplierDTO> getParliament_supplierDTOBysupplier_code(String supplier_code) {
		return new ArrayList<>( mapOfParliament_supplierDTOTosupplierCode.getOrDefault(supplier_code,new HashSet<>()));
	}
	
	
	public List<Parliament_supplierDTO> getParliament_supplierDTOByis_foreign(boolean is_foreign) {
		return new ArrayList<>( mapOfParliament_supplierDTOToisForeign.getOrDefault(is_foreign,new HashSet<>()));
	}
	
	
	public List<Parliament_supplierDTO> getParliament_supplierDTOBysupplier_bin(String supplier_bin) {
		return new ArrayList<>( mapOfParliament_supplierDTOTosupplierBin.getOrDefault(supplier_bin,new HashSet<>()));
	}
	
	
	public List<Parliament_supplierDTO> getParliament_supplierDTOBysupplier_address(String supplier_address) {
		return new ArrayList<>( mapOfParliament_supplierDTOTosupplierAddress.getOrDefault(supplier_address,new HashSet<>()));
	}
	
	
	public List<Parliament_supplierDTO> getParliament_supplierDTOBysupplier_mobile_1(String supplier_mobile_1) {
		return new ArrayList<>( mapOfParliament_supplierDTOTosupplierMobile1.getOrDefault(supplier_mobile_1,new HashSet<>()));
	}
	
	
	public List<Parliament_supplierDTO> getParliament_supplierDTOBysupplier_mobile_2(String supplier_mobile_2) {
		return new ArrayList<>( mapOfParliament_supplierDTOTosupplierMobile2.getOrDefault(supplier_mobile_2,new HashSet<>()));
	}
	
	
	public List<Parliament_supplierDTO> getParliament_supplierDTOBysupplier_email(String supplier_email) {
		return new ArrayList<>( mapOfParliament_supplierDTOTosupplierEmail.getOrDefault(supplier_email,new HashSet<>()));
	}
	
	
	public List<Parliament_supplierDTO> getParliament_supplierDTOByremarks(String remarks) {
		return new ArrayList<>( mapOfParliament_supplierDTOToremarks.getOrDefault(remarks,new HashSet<>()));
	}
	
	
	public List<Parliament_supplierDTO> getParliament_supplierDTOByinserted_by_user_id(long inserted_by_user_id) {
		return new ArrayList<>( mapOfParliament_supplierDTOToinsertedByUserId.getOrDefault(inserted_by_user_id,new HashSet<>()));
	}
	
	
	public List<Parliament_supplierDTO> getParliament_supplierDTOByinserted_by_organogram_id(long inserted_by_organogram_id) {
		return new ArrayList<>( mapOfParliament_supplierDTOToinsertedByOrganogramId.getOrDefault(inserted_by_organogram_id,new HashSet<>()));
	}
	
	
	public List<Parliament_supplierDTO> getParliament_supplierDTOByinsertion_date(long insertion_date) {
		return new ArrayList<>( mapOfParliament_supplierDTOToinsertionDate.getOrDefault(insertion_date,new HashSet<>()));
	}
	
	
	public List<Parliament_supplierDTO> getParliament_supplierDTOBysearch_column(String search_column) {
		return new ArrayList<>( mapOfParliament_supplierDTOTosearchColumn.getOrDefault(search_column,new HashSet<>()));
	}
	
	
	public List<Parliament_supplierDTO> getParliament_supplierDTOBylastModificationTime(long lastModificationTime) {
		return new ArrayList<>( mapOfParliament_supplierDTOTolastModificationTime.getOrDefault(lastModificationTime,new HashSet<>()));
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "parliament_supplier";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


