package parliament_supplier;
import java.util.*; 
import util.*;


public class Parliament_supplierMAPS extends CommonMaps
{	
	public Parliament_supplierMAPS(String tableName)
	{
		

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("nameEn".toLowerCase(), "nameEn".toLowerCase());
		java_DTO_map.put("nameBn".toLowerCase(), "nameBn".toLowerCase());
		java_DTO_map.put("agreementDate".toLowerCase(), "agreementDate".toLowerCase());
		java_DTO_map.put("filesDropzone".toLowerCase(), "filesDropzone".toLowerCase());
		java_DTO_map.put("tradeLicenseNumber".toLowerCase(), "tradeLicenseNumber".toLowerCase());
		java_DTO_map.put("tradeLicenseExpiryDate".toLowerCase(), "tradeLicenseExpiryDate".toLowerCase());
		java_DTO_map.put("supplierCode".toLowerCase(), "supplierCode".toLowerCase());
		java_DTO_map.put("isForeign".toLowerCase(), "isForeign".toLowerCase());
		java_DTO_map.put("supplierBin".toLowerCase(), "supplierBin".toLowerCase());
		java_DTO_map.put("supplierAddress".toLowerCase(), "supplierAddress".toLowerCase());
		java_DTO_map.put("supplierMobile1".toLowerCase(), "supplierMobile1".toLowerCase());
		java_DTO_map.put("supplierMobile2".toLowerCase(), "supplierMobile2".toLowerCase());
		java_DTO_map.put("supplierEmail".toLowerCase(), "supplierEmail".toLowerCase());
		java_DTO_map.put("remarks".toLowerCase(), "remarks".toLowerCase());
		java_DTO_map.put("insertedByUserId".toLowerCase(), "insertedByUserId".toLowerCase());
		java_DTO_map.put("insertedByOrganogramId".toLowerCase(), "insertedByOrganogramId".toLowerCase());
		java_DTO_map.put("insertionDate".toLowerCase(), "insertionDate".toLowerCase());
		java_DTO_map.put("searchColumn".toLowerCase(), "searchColumn".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("name_en".toLowerCase(), "nameEn".toLowerCase());
		java_SQL_map.put("name_bn".toLowerCase(), "nameBn".toLowerCase());
		java_SQL_map.put("agreement_date".toLowerCase(), "agreementDate".toLowerCase());
		java_SQL_map.put("files_dropzone".toLowerCase(), "filesDropzone".toLowerCase());
		java_SQL_map.put("trade_license_number".toLowerCase(), "tradeLicenseNumber".toLowerCase());
		java_SQL_map.put("trade_license_expiry_date".toLowerCase(), "tradeLicenseExpiryDate".toLowerCase());
		java_SQL_map.put("supplier_code".toLowerCase(), "supplierCode".toLowerCase());
		java_SQL_map.put("is_foreign".toLowerCase(), "isForeign".toLowerCase());
		java_SQL_map.put("supplier_bin".toLowerCase(), "supplierBin".toLowerCase());
		java_SQL_map.put("supplier_address".toLowerCase(), "supplierAddress".toLowerCase());
		java_SQL_map.put("supplier_mobile_1".toLowerCase(), "supplierMobile1".toLowerCase());
		java_SQL_map.put("supplier_mobile_2".toLowerCase(), "supplierMobile2".toLowerCase());
		java_SQL_map.put("supplier_email".toLowerCase(), "supplierEmail".toLowerCase());
		java_SQL_map.put("remarks".toLowerCase(), "remarks".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("English Name".toLowerCase(), "nameEn".toLowerCase());
		java_Text_map.put("Bangla Name".toLowerCase(), "nameBn".toLowerCase());
		java_Text_map.put("Agreement Date".toLowerCase(), "agreementDate".toLowerCase());
		java_Text_map.put("Files Dropzone".toLowerCase(), "filesDropzone".toLowerCase());
		java_Text_map.put("Trade License Number".toLowerCase(), "tradeLicenseNumber".toLowerCase());
		java_Text_map.put("Trade License Expiry Date".toLowerCase(), "tradeLicenseExpiryDate".toLowerCase());
		java_Text_map.put("Supplier Code".toLowerCase(), "supplierCode".toLowerCase());
		java_Text_map.put("Is Foreign".toLowerCase(), "isForeign".toLowerCase());
		java_Text_map.put("Supplier Bin".toLowerCase(), "supplierBin".toLowerCase());
		java_Text_map.put("Supplier Address".toLowerCase(), "supplierAddress".toLowerCase());
		java_Text_map.put("Supplier Mobile 1".toLowerCase(), "supplierMobile1".toLowerCase());
		java_Text_map.put("Supplier Mobile 2".toLowerCase(), "supplierMobile2".toLowerCase());
		java_Text_map.put("Supplier Email".toLowerCase(), "supplierEmail".toLowerCase());
		java_Text_map.put("Remarks".toLowerCase(), "remarks".toLowerCase());
		java_Text_map.put("Inserted By User Id".toLowerCase(), "insertedByUserId".toLowerCase());
		java_Text_map.put("Inserted By Organogram Id".toLowerCase(), "insertedByOrganogramId".toLowerCase());
		java_Text_map.put("Insertion Date".toLowerCase(), "insertionDate".toLowerCase());
		java_Text_map.put("Search Column".toLowerCase(), "searchColumn".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}