package political_party;

import org.apache.log4j.Logger;
import pb.OptionDTO;
import pb.Utils;
import repository.Repository;
import repository.RepositoryManager;
import util.LockManager;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

@SuppressWarnings("Duplicates")
public class Political_partyRepository implements Repository {

	private final Political_partyDAO politicalPartyDAO = Political_partyDAO.getInstance();
	private static final Logger logger = Logger.getLogger(Political_partyRepository.class);
	private final Map<Long, Political_partyDTO>mapOfPolitical_partyDTOToiD;
	private List<Political_partyDTO> politicalPartyDTOList;

	private Political_partyRepository(){
		mapOfPolitical_partyDTOToiD = new ConcurrentHashMap<>();
		RepositoryManager.getInstance().addRepository(this);
	}

	private static class LazyLoader{
		static Political_partyRepository INSTANCE = new Political_partyRepository();
	}

	public static Political_partyRepository getInstance(){
		return LazyLoader.INSTANCE;
	}

	public void reload(boolean reloadAll){
		logger.debug("Political_partyRepository reload start reloadAll : "+reloadAll);
		politicalPartyDAO.getAllDTOs(reloadAll)
				.stream()
				.peek(this::removeDTOIfPresent)
				.filter(dto->dto.isDeleted == 0)
				.forEach(dto-> mapOfPolitical_partyDTOToiD.put(dto.iD,dto));
		politicalPartyDTOList = new ArrayList<>(mapOfPolitical_partyDTOToiD.values());
		logger.debug("Political_partyRepository reload end reloadAll : "+reloadAll);
	}

	private void removeDTOIfPresent(Political_partyDTO dto){
		if(dto!=null){
			mapOfPolitical_partyDTOToiD.remove(dto.iD);
		}
	}
	public List<Political_partyDTO> getPolitical_partyList() {
		return politicalPartyDTOList;
	}
	
	
	public Political_partyDTO getPolitical_partyDTOByID( long ID){
		if(mapOfPolitical_partyDTOToiD.get(ID) == null){
			synchronized (LockManager.getLock(ID+"PPR")){
				if(mapOfPolitical_partyDTOToiD.get(ID) == null){
					Political_partyDTO dto = politicalPartyDAO.getDTOFromID(ID);
					if(dto!=null){
						mapOfPolitical_partyDTOToiD.put(dto.iD,dto);
						if(dto.isDeleted == 0){
						    politicalPartyDTOList.add(dto);
                        }
					}
				}
			}
		}
		return mapOfPolitical_partyDTOToiD.get(ID);
	}
	
	@Override
	public String getTableName() {
		return "political_party";
	}

	public String buildOptions(String language, Long selectedId) {
		List<OptionDTO> optionDTOList = null;
		if (politicalPartyDTOList!=null && politicalPartyDTOList.size() > 0){
			optionDTOList = politicalPartyDTOList.stream()
					.map(dto->new OptionDTO(dto.nameEn,dto.nameBn,String.valueOf(dto.iD)))
					.collect(Collectors.toList());
		}
		return Utils.buildOptions(optionDTOList,language,selectedId == null ? null : String.valueOf(selectedId));
	}

	public String getText(long id,String language){
		return getText(id,"English".equalsIgnoreCase(language));
	}

	public String getText(long id,boolean isLangEng){
		Political_partyDTO dto = getPolitical_partyDTOByID(id);
		return dto==null?"":(isLangEng?dto.nameEn: dto.nameBn);
	}
}