package political_party;
import java.util.*;

import sessionmanager.SessionConstants;
import util.*; 


public class Political_partyDTO extends CommonDTO
{

    public int registrationNo = 0;
    public long registrationDate = SessionConstants.MIN_DATE;
    public String nameEn = "";
    public String nameBn = "";
    public String abbreviation = "";
    public int partyChairNameCat = 0;
    public int secondInCommandNameCat = 0;
    public String presidentName = "";
    public String gsName = "";
	public long foundationDate = SessionConstants.MIN_DATE;
    public String officeAddress = "";
    public String symbolNameEng = "";
    public String symbolNameBng = "";
	public byte[] electoralSymbolBlob = null;	
	public byte[] partyFlagBlob = null;
	public byte[] presidentProfilePicture = null;
    public byte[] gsProfilePicture = null;
	public String phoneNumber = "";
	public String mobileNumber = "";
	public String faxNumber = "";
    public String website = "";
    public String email = "";
	public long filesDropzone = 0;
	public long insertionDate = 0;
    public String insertedBy = "";
    public String modifiedBy = "";
	
	
    @Override
	public String toString() {
            return "$Political_partyDTO[" +
            " iD = " + iD +
            " nameEn = " + registrationNo +
            " nameEn = " + registrationDate +
            " nameEn = " + nameEn +
            " nameBn = " + nameBn +
            " abbreviation = " + abbreviation +
            " presidentName = " + presidentName +
            " gsName = " + gsName +
            " foundationDate = " + foundationDate +
            " officeAddress = " + officeAddress +
            " website = " + website +
            " email = " + email +
            " filesDropzone = " + filesDropzone +
            " insertionDate = " + insertionDate +
            " insertedBy = " + insertedBy +
            " modifiedBy = " + modifiedBy +
            " searchColumn = " + searchColumn +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}