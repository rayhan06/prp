package political_party;

import common.BaseServlet;
import common.CommonDAOService;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import pb.Utils;
import permission.MenuConstants;
import user.UserDTO;
import util.CommonDTO;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;


@WebServlet("/Political_partyServlet")
@MultipartConfig
public class Political_partyServlet extends BaseServlet {
    private static final long serialVersionUID = 1L;

    private final Political_partyDAO political_partyDAO = Political_partyDAO.getInstance();


    @Override
    public String getTableName() {
        return political_partyDAO.getTableName();
    }

    @Override
    public String getServletName() {
        return "Political_partyServlet";
    }

    @Override
    public CommonDAOService<? extends CommonDTO> getCommonDAOService() {
        return political_partyDAO;
    }

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        Political_partyDTO political_partyDTO;
        SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
        if (addFlag) {
            political_partyDTO = new Political_partyDTO();
            political_partyDTO.insertedBy = String.valueOf(userDTO.employee_record_id);
            political_partyDTO.insertionDate = new Date().getTime();
        } else {
            political_partyDTO = political_partyDAO.getDTOFromID(Long.parseLong(request.getParameter("iD")));
        }
        political_partyDTO.modifiedBy = String.valueOf(userDTO.ID);
        political_partyDTO.lastModificationTime = new Date().getTime();

        String Value;

        political_partyDTO.nameEn = (Jsoup.clean(request.getParameter("nameEn"), Whitelist.simpleText()));
        political_partyDTO.nameBn = (Jsoup.clean(request.getParameter("nameBn"), Whitelist.simpleText()));
        political_partyDTO.abbreviation = (Jsoup.clean(request.getParameter("abbreviation"), Whitelist.simpleText()));
        political_partyDTO.presidentName = (Jsoup.clean(request.getParameter("presidentName"), Whitelist.simpleText()));
        political_partyDTO.gsName = (Jsoup.clean(request.getParameter("gsName"), Whitelist.simpleText()));
        Date d = f.parse(Jsoup.clean(request.getParameter("foundationDate"), Whitelist.simpleText()));
        political_partyDTO.foundationDate = d.getTime();
        Value = request.getParameter("officeAddress");
        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }
        logger.debug("officeAddress = " + Value);
        if (Value != null && !Value.equalsIgnoreCase("")) {
            political_partyDTO.officeAddress = Value;
        }

        if (Utils.isValidInputFile(request.getPart("electoralSymbolBlob"))) {
            political_partyDTO.electoralSymbolBlob = Utils.uploadFileToByteAray(request.getPart("electoralSymbolBlob"));
        }

        if (Utils.isValidInputFile(request.getPart("partyFlagBlob"))) {
            political_partyDTO.partyFlagBlob = Utils.uploadFileToByteAray(request.getPart("partyFlagBlob"));
        }

        political_partyDTO.website = (Jsoup.clean(request.getParameter("website"), Whitelist.simpleText()));
        political_partyDTO.email = (Jsoup.clean(request.getParameter("email"), Whitelist.simpleText()));

        political_partyDTO.filesDropzone = Long.parseLong(request.getParameter("filesDropzone"));
        if (!addFlag) {
            String filesDropzoneFilesToDelete = request.getParameter("filesDropzoneFilesToDelete");
            String[] deleteArray = filesDropzoneFilesToDelete.split(",");
            for (int i = 0; i < deleteArray.length; i++) {
                System.out.println("going to delete " + deleteArray[i]);
                if (i > 0) {
                    filesDAO.delete(Long.parseLong(deleteArray[i]));
                }
            }
        }

        try {
            political_partyDTO.partyChairNameCat = Integer.parseInt(Jsoup.clean(request.getParameter("party_chair_name"), Whitelist.simpleText()));
        } catch (Exception ex) {
            logger.debug("This field is not required: Political_partyServlet!");
        }

        try {
            political_partyDTO.secondInCommandNameCat = Integer.parseInt(Jsoup.clean(request.getParameter("second_in_command_name"), Whitelist.simpleText()));
        } catch (Exception ex) {
            logger.debug("This field is not required: Political_partyServlet!");
        }

        try {
            political_partyDTO.symbolNameEng = (Jsoup.clean(request.getParameter("symbol_name_eng"), Whitelist.simpleText()));
        } catch (Exception ex) {
            logger.debug("This field is not required: Political_partyServlet!");
        }

        try {
            political_partyDTO.symbolNameBng = (Jsoup.clean(request.getParameter("symbol_name_bng"), Whitelist.simpleText()));
        } catch (Exception ex) {
            logger.debug("This field is not required: Political_partyServlet!");
        }

        try {
            political_partyDTO.registrationNo = Integer.parseInt(Jsoup.clean(request.getParameter("registration_no"), Whitelist.simpleText()));
        } catch (Exception ex) {
            logger.debug("This field is not required: Political_partyServlet!");
        }

        try {
            d = f.parse(Jsoup.clean(request.getParameter("registration_date"), Whitelist.simpleText()));
            political_partyDTO.registrationDate = d.getTime();
        } catch (Exception ex) {
            logger.debug("This field is not required: Political_partyServlet!");
        }

        try {
            political_partyDTO.phoneNumber = (Jsoup.clean(request.getParameter("phone_number"), Whitelist.simpleText()));
        } catch (Exception ex) {
            logger.debug("This field is not required: Political_partyServlet!");
        }

        try {
            political_partyDTO.mobileNumber = (Jsoup.clean(request.getParameter("mobile_number"), Whitelist.simpleText()));
        } catch (Exception ex) {
            logger.debug("This is not required field!");
        }

        if (Utils.isValidInputFile(request.getPart("president_profile_picture"))) {
            political_partyDTO.presidentProfilePicture = Utils.uploadFileToByteAray(request.getPart("president_profile_picture"));
        }

        if (Utils.isValidInputFile(request.getPart("gs_profile_picture"))) {
            political_partyDTO.gsProfilePicture = Utils.uploadFileToByteAray(request.getPart("gs_profile_picture"));
        }

        try {
            political_partyDTO.faxNumber = (Jsoup.clean(request.getParameter("fax"), Whitelist.simpleText()));
        } catch (Exception ex) {
            logger.debug("This is not required field!");
//            throw new Exception("Enter value in the required field!");
        }


        if(addFlag){
            political_partyDAO.add(political_partyDTO);
        }else{
            political_partyDAO.update(political_partyDTO);
        }
        return political_partyDTO;
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[]{MenuConstants.POLITICAL_PARTY_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[]{MenuConstants.POLITICAL_PARTY_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[]{MenuConstants.POLITICAL_PARTY_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Political_partyServlet.class;
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if("downloadBlob".equals(request.getParameter("actionType"))){
            long id = Long.parseLong(request.getParameter("id"));
            String name = request.getParameter("name");
            Political_partyDTO political_partyDTO = political_partyDAO.getDTOFromID(id);
            if (name.equalsIgnoreCase("electoralSymbolBlob")) {
                Utils.ProcessFile(request, response, "electoralSymbolBlob.jpg", political_partyDTO.electoralSymbolBlob);
            }
            if (name.equalsIgnoreCase("partyFlagBlob")) {
                Utils.ProcessFile(request, response, "partyFlagBlob.jpg", political_partyDTO.partyFlagBlob);
            }
            if (name.equalsIgnoreCase("president_profile_picture")) {
                Utils.ProcessFile(request, response, "president_profile_picture.jpg", political_partyDTO.presidentProfilePicture);
            }
            if (name.equalsIgnoreCase("gs_profile_picture")) {
                Utils.ProcessFile(request, response, "gs_profile_picture.jpg", political_partyDTO.gsProfilePicture);
            }
            return;
        }
        super.doGet(request,response);
    }
}