package political_party;

import common.CommonDAOService;
import org.apache.commons.compress.utils.IOUtils;
import org.apache.log4j.Logger;
import util.CommonDTO;

import java.io.IOException;
import java.io.InputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

@SuppressWarnings({"unused", "Duplicates"})
public class Political_partyDAO implements CommonDAOService<Political_partyDTO> {

    private static final Logger logger = Logger.getLogger(Political_partyDAO.class);

    private static final String addQuery = "INSERT INTO {tableName} (name_en,name_bn,abbreviation,president_name,gs_name,foundation_date,"
            .concat(" office_address,electoral_symbol_blob,party_flag_blob,website,email,files_dropzone,insertion_date,inserted_by,modified_by,")
            .concat(" search_column,lastModificationTime,party_chair_position_name_cat,second_in_command_position_name_cat,symbol_name_eng,")
            .concat(" symbol_name_bng,registration_no,registration_date,phone_number,mobile_number,fax,president_profile_picture,gs_profile_picture,")
            .concat(" isDeleted,ID) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

    private static final String updateQuery = "UPDATE {tableName} SET name_en=?,name_bn=?,abbreviation=?,president_name=?,gs_name=?," +
            " foundation_date=?,office_address=?,electoral_symbol_blob=?,party_flag_blob=?,website=?,email=?,files_dropzone=?," +
            " insertion_date=?,inserted_by=?,modified_by=?,search_column=?, lastModificationTime = ?,party_chair_position_name_cat=?,"+
            " second_in_command_position_name_cat=?,symbol_name_eng=?,symbol_name_bng=?,registration_no=?,registration_date=?,phone_number=?,"+
            " mobile_number=?,fax=?,president_profile_picture=?,gs_profile_picture=? WHERE ID = ?";

    private final Map<String,String> searchMap = new HashMap<>();

    private static Political_partyDAO INSTANCE = null;

    private Political_partyDAO() {
        searchMap.put("name_en"," and (name_en like ?)");
        searchMap.put("name_bn"," and (name_bn like ?)");
        searchMap.put("abbreviation"," and (abbreviation = ?)");
        searchMap.put("AnyField"," and (search_column like ?)");
    }

    public static Political_partyDAO getInstance(){
        if(INSTANCE == null){
            synchronized (Political_partyDAO.class){
                if(INSTANCE == null){
                    INSTANCE = new Political_partyDAO();
                }
            }
        }
        return INSTANCE;
    }

    public void setSearchColumn(Political_partyDTO political_partyDTO) {
        political_partyDTO.searchColumn = "";
        political_partyDTO.searchColumn += political_partyDTO.nameEn + " ";
        political_partyDTO.searchColumn += political_partyDTO.nameBn + " ";
        political_partyDTO.searchColumn += political_partyDTO.abbreviation + " ";
    }

    @Override
    public void set(PreparedStatement ps, Political_partyDTO political_partyDTO, boolean isInsert) throws SQLException {
        int index = 0;
        setSearchColumn(political_partyDTO);
        ps.setObject(++index, political_partyDTO.nameEn);
        ps.setObject(++index, political_partyDTO.nameBn);
        ps.setObject(++index, political_partyDTO.abbreviation);
        ps.setObject(++index, political_partyDTO.presidentName);
        ps.setObject(++index, political_partyDTO.gsName);
        ps.setObject(++index, political_partyDTO.foundationDate);
        ps.setObject(++index, political_partyDTO.officeAddress);
        ps.setBytes( ++index, political_partyDTO.electoralSymbolBlob);
        ps.setBytes( ++index, political_partyDTO.partyFlagBlob);
        ps.setObject(++index, political_partyDTO.website);
        ps.setObject(++index, political_partyDTO.email);
        ps.setObject(++index, political_partyDTO.filesDropzone);
        ps.setObject(++index, political_partyDTO.insertionDate);
        ps.setObject(++index, political_partyDTO.insertedBy);
        ps.setObject(++index, political_partyDTO.modifiedBy);
        ps.setObject(++index, political_partyDTO.searchColumn);
        ps.setObject(++index, System.currentTimeMillis());

        ps.setObject(++index, political_partyDTO.partyChairNameCat);
        ps.setObject(++index, political_partyDTO.secondInCommandNameCat);
        ps.setObject(++index, political_partyDTO.symbolNameEng);
        ps.setObject(++index, political_partyDTO.symbolNameBng);
        ps.setObject(++index, political_partyDTO.registrationNo);
        ps.setObject(++index, political_partyDTO.registrationDate);
        ps.setObject(++index, political_partyDTO.phoneNumber);
        ps.setObject(++index, political_partyDTO.mobileNumber);

        ps.setObject(++index, political_partyDTO.faxNumber);
        ps.setObject(++index, political_partyDTO.presidentProfilePicture);
        ps.setObject(++index, political_partyDTO.gsProfilePicture);

        if (isInsert) {
            ps.setObject(++index, 0);
        }
        ps.setObject(++index, political_partyDTO.iD);
    }

    @Override
    public Political_partyDTO buildObjectFromResultSet(ResultSet rs){
        try{
            Political_partyDTO political_partyDTO = new Political_partyDTO();
            political_partyDTO.iD = rs.getLong("ID");
            political_partyDTO.nameEn = rs.getString("name_en");
            political_partyDTO.nameBn = rs.getString("name_bn");
            political_partyDTO.abbreviation = rs.getString("abbreviation");
            political_partyDTO.presidentName = rs.getString("president_name");
            political_partyDTO.gsName = rs.getString("gs_name");
            political_partyDTO.foundationDate = rs.getLong("foundation_date");
            political_partyDTO.officeAddress = rs.getString("office_address");
            InputStream is = rs.getBinaryStream("electoral_symbol_blob");
            if (is != null) {
                try {
                    political_partyDTO.electoralSymbolBlob = IOUtils.toByteArray(is);
                } catch (IOException e) {
                    logger.error(e);
                }
            }
            is = rs.getBinaryStream("party_flag_blob");
            if (is != null) {
                try {
                    political_partyDTO.partyFlagBlob = IOUtils.toByteArray(is);
                } catch (IOException e) {
                    logger.error(e);
                }
            }
            political_partyDTO.website = rs.getString("website");
            political_partyDTO.email = rs.getString("email");
            political_partyDTO.filesDropzone = rs.getLong("files_dropzone");
            political_partyDTO.insertionDate = rs.getLong("insertion_date");
            political_partyDTO.insertedBy = rs.getString("inserted_by");
            political_partyDTO.modifiedBy = rs.getString("modified_by");
            political_partyDTO.searchColumn = rs.getString("search_column");
            political_partyDTO.isDeleted = rs.getInt("isDeleted");
            political_partyDTO.lastModificationTime = rs.getLong("lastModificationTime");

            political_partyDTO.partyChairNameCat = rs.getInt("party_chair_position_name_cat");
            political_partyDTO.secondInCommandNameCat = rs.getInt("second_in_command_position_name_cat");
            political_partyDTO.symbolNameEng = rs.getString("symbol_name_eng");
            political_partyDTO.symbolNameBng = rs.getString("symbol_name_bng");
            political_partyDTO.registrationNo = rs.getInt("registration_no");
            political_partyDTO.registrationDate = rs.getLong("registration_date");
            political_partyDTO.phoneNumber = rs.getString("phone_number");
            political_partyDTO.mobileNumber = rs.getString("mobile_number");

            is = rs.getBinaryStream("president_profile_picture");
            if (is != null) {
                try {
                    political_partyDTO.presidentProfilePicture = IOUtils.toByteArray(is);
                } catch (IOException e) {
                    logger.error(e);
                }
            }
            is = rs.getBinaryStream("gs_profile_picture");
            if (is != null) {
                try {
                    political_partyDTO.gsProfilePicture = IOUtils.toByteArray(is);
                } catch (IOException e) {
                    logger.error(e);
                }
            }
            political_partyDTO.faxNumber = rs.getString("fax");

            return political_partyDTO;
        }catch (SQLException ex){
            logger.error(ex);
            return null;
        }
    }

    @Override
    public String getTableName() {
        return "political_party";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }


    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Political_partyDTO) commonDTO,addQuery,true);
    }

    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Political_partyDTO) commonDTO,updateQuery,false);
    }
}
	