package approval_path_assign;

import common.BaseServlet;
import common.CommonDAOService;
import permission.MenuConstants;
import task_type_assign.Task_type_assignDAO;
import task_type_level.TaskTypeLevelDAO;
import user.UserDTO;
import util.CommonDTO;
import util.HttpRequestUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@WebServlet("/Approval_path_assignServlet")
@MultipartConfig
public class Approval_path_assignServlet extends BaseServlet {
    private static final long serialVersionUID = 1L;

    private final TaskTypeLevelDAO taskTypeLevelDAO = TaskTypeLevelDAO.getInstance();

    @Override
    public String getTableName() {
        return taskTypeLevelDAO.getTableName();
    }

    @Override
    public String getWhereClause(HttpServletRequest request) {
        long roleId = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO.roleID;
        return " AND (task_type_assign.role_ID like '"+roleId+"'" + " OR "+"task_type_assign.role_ID like '"+roleId+",%'"+
                " OR "+"task_type_assign.role_ID like '%,"+roleId+",%' " + "OR task_type_assign.role_ID like '%," +roleId+"')";
    }

    @Override
    public String getTableJoinClause(HttpServletRequest request) {
        return " INNER JOIN task_type_assign ON task_type_assign.task_type_ID = task_type_level.task_type_id ";
    }

    @Override
    public String getServletName() {
        return "Approval_path_assignServlet";
    }

    @Override
    public CommonDAOService<? extends CommonDTO> getCommonDAOService() {
        return taskTypeLevelDAO;
    }

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        return null;
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[0];
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[0];
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[]{MenuConstants.APPROVAL_PATH_ASSIGN};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Approval_path_assignServlet.class;
    }

    @Override
    public String defaultNonAjaxSearchPageURL() {
        return "approval_path_assign/approval_path_assignSearch.jsp";
    }

    @Override
    public String commonPartOfDispatchURL() {
        return "approval_path_assign/approval_path_assign";
    }
}