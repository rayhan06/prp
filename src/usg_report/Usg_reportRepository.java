package usg_report;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class Usg_reportRepository implements Repository {
	Usg_reportDAO usg_reportDAO = null;
	
	public void setDAO(Usg_reportDAO usg_reportDAO)
	{
		this.usg_reportDAO = usg_reportDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Usg_reportRepository.class);
	Map<Long, Usg_reportDTO>mapOfUsg_reportDTOToiD;
	Map<Long, Set<Usg_reportDTO> >mapOfUsg_reportDTOToprescriptionLabTestId;
	Map<Long, Set<Usg_reportDTO> >mapOfUsg_reportDTOToprescriptionDetailsId;
	Map<Long, Set<Usg_reportDTO> >mapOfUsg_reportDTOTocommonLabReportId;
	Map<Long, Set<Usg_reportDTO> >mapOfUsg_reportDTOToappointmentId;
	Map<String, Set<Usg_reportDTO> >mapOfUsg_reportDTOToname;
	Map<Long, Set<Usg_reportDTO> >mapOfUsg_reportDTOTodateOfBirth;
	Map<String, Set<Usg_reportDTO> >mapOfUsg_reportDTOToreferredByUserName;
	Map<Integer, Set<Usg_reportDTO> >mapOfUsg_reportDTOTogenderCat;
	Map<Long, Set<Usg_reportDTO> >mapOfUsg_reportDTOToultrasonoTypeType;
	Map<String, Set<Usg_reportDTO> >mapOfUsg_reportDTOTocomment;
	Map<Long, Set<Usg_reportDTO> >mapOfUsg_reportDTOTodoctorId;
	Map<Long, Set<Usg_reportDTO> >mapOfUsg_reportDTOToinsertedByUserId;
	Map<Long, Set<Usg_reportDTO> >mapOfUsg_reportDTOToinsertedByOrganogramId;
	Map<Long, Set<Usg_reportDTO> >mapOfUsg_reportDTOToinsertionDate;
	Map<String, Set<Usg_reportDTO> >mapOfUsg_reportDTOTosearchColumn;
	Map<Long, Set<Usg_reportDTO> >mapOfUsg_reportDTOTolastModificationTime;


	static Usg_reportRepository instance = null;  
	private Usg_reportRepository(){
		mapOfUsg_reportDTOToiD = new ConcurrentHashMap<>();
		mapOfUsg_reportDTOToprescriptionLabTestId = new ConcurrentHashMap<>();
		mapOfUsg_reportDTOToprescriptionDetailsId = new ConcurrentHashMap<>();
		mapOfUsg_reportDTOTocommonLabReportId = new ConcurrentHashMap<>();
		mapOfUsg_reportDTOToappointmentId = new ConcurrentHashMap<>();
		mapOfUsg_reportDTOToname = new ConcurrentHashMap<>();
		mapOfUsg_reportDTOTodateOfBirth = new ConcurrentHashMap<>();
		mapOfUsg_reportDTOToreferredByUserName = new ConcurrentHashMap<>();
		mapOfUsg_reportDTOTogenderCat = new ConcurrentHashMap<>();
		mapOfUsg_reportDTOToultrasonoTypeType = new ConcurrentHashMap<>();
		mapOfUsg_reportDTOTocomment = new ConcurrentHashMap<>();
		mapOfUsg_reportDTOTodoctorId = new ConcurrentHashMap<>();
		mapOfUsg_reportDTOToinsertedByUserId = new ConcurrentHashMap<>();
		mapOfUsg_reportDTOToinsertedByOrganogramId = new ConcurrentHashMap<>();
		mapOfUsg_reportDTOToinsertionDate = new ConcurrentHashMap<>();
		mapOfUsg_reportDTOTosearchColumn = new ConcurrentHashMap<>();
		mapOfUsg_reportDTOTolastModificationTime = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Usg_reportRepository getInstance(){
		if (instance == null){
			instance = new Usg_reportRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(usg_reportDAO == null)
		{
			return;
		}
		try {
			List<Usg_reportDTO> usg_reportDTOs = usg_reportDAO.getAllUsg_report(reloadAll);
			for(Usg_reportDTO usg_reportDTO : usg_reportDTOs) {
				Usg_reportDTO oldUsg_reportDTO = getUsg_reportDTOByID(usg_reportDTO.iD);
				if( oldUsg_reportDTO != null ) {
					mapOfUsg_reportDTOToiD.remove(oldUsg_reportDTO.iD);
				
					if(mapOfUsg_reportDTOToprescriptionLabTestId.containsKey(oldUsg_reportDTO.prescriptionLabTestId)) {
						mapOfUsg_reportDTOToprescriptionLabTestId.get(oldUsg_reportDTO.prescriptionLabTestId).remove(oldUsg_reportDTO);
					}
					if(mapOfUsg_reportDTOToprescriptionLabTestId.get(oldUsg_reportDTO.prescriptionLabTestId).isEmpty()) {
						mapOfUsg_reportDTOToprescriptionLabTestId.remove(oldUsg_reportDTO.prescriptionLabTestId);
					}
					
					if(mapOfUsg_reportDTOToprescriptionDetailsId.containsKey(oldUsg_reportDTO.prescriptionDetailsId)) {
						mapOfUsg_reportDTOToprescriptionDetailsId.get(oldUsg_reportDTO.prescriptionDetailsId).remove(oldUsg_reportDTO);
					}
					if(mapOfUsg_reportDTOToprescriptionDetailsId.get(oldUsg_reportDTO.prescriptionDetailsId).isEmpty()) {
						mapOfUsg_reportDTOToprescriptionDetailsId.remove(oldUsg_reportDTO.prescriptionDetailsId);
					}
					
					if(mapOfUsg_reportDTOTocommonLabReportId.containsKey(oldUsg_reportDTO.commonLabReportId)) {
						mapOfUsg_reportDTOTocommonLabReportId.get(oldUsg_reportDTO.commonLabReportId).remove(oldUsg_reportDTO);
					}
					if(mapOfUsg_reportDTOTocommonLabReportId.get(oldUsg_reportDTO.commonLabReportId).isEmpty()) {
						mapOfUsg_reportDTOTocommonLabReportId.remove(oldUsg_reportDTO.commonLabReportId);
					}
					
					if(mapOfUsg_reportDTOToappointmentId.containsKey(oldUsg_reportDTO.appointmentId)) {
						mapOfUsg_reportDTOToappointmentId.get(oldUsg_reportDTO.appointmentId).remove(oldUsg_reportDTO);
					}
					if(mapOfUsg_reportDTOToappointmentId.get(oldUsg_reportDTO.appointmentId).isEmpty()) {
						mapOfUsg_reportDTOToappointmentId.remove(oldUsg_reportDTO.appointmentId);
					}
					
					if(mapOfUsg_reportDTOToname.containsKey(oldUsg_reportDTO.name)) {
						mapOfUsg_reportDTOToname.get(oldUsg_reportDTO.name).remove(oldUsg_reportDTO);
					}
					if(mapOfUsg_reportDTOToname.get(oldUsg_reportDTO.name).isEmpty()) {
						mapOfUsg_reportDTOToname.remove(oldUsg_reportDTO.name);
					}
					
					if(mapOfUsg_reportDTOTodateOfBirth.containsKey(oldUsg_reportDTO.dateOfBirth)) {
						mapOfUsg_reportDTOTodateOfBirth.get(oldUsg_reportDTO.dateOfBirth).remove(oldUsg_reportDTO);
					}
					if(mapOfUsg_reportDTOTodateOfBirth.get(oldUsg_reportDTO.dateOfBirth).isEmpty()) {
						mapOfUsg_reportDTOTodateOfBirth.remove(oldUsg_reportDTO.dateOfBirth);
					}
					
					if(mapOfUsg_reportDTOToreferredByUserName.containsKey(oldUsg_reportDTO.referredByUserName)) {
						mapOfUsg_reportDTOToreferredByUserName.get(oldUsg_reportDTO.referredByUserName).remove(oldUsg_reportDTO);
					}
					if(mapOfUsg_reportDTOToreferredByUserName.get(oldUsg_reportDTO.referredByUserName).isEmpty()) {
						mapOfUsg_reportDTOToreferredByUserName.remove(oldUsg_reportDTO.referredByUserName);
					}
					
					if(mapOfUsg_reportDTOTogenderCat.containsKey(oldUsg_reportDTO.genderCat)) {
						mapOfUsg_reportDTOTogenderCat.get(oldUsg_reportDTO.genderCat).remove(oldUsg_reportDTO);
					}
					if(mapOfUsg_reportDTOTogenderCat.get(oldUsg_reportDTO.genderCat).isEmpty()) {
						mapOfUsg_reportDTOTogenderCat.remove(oldUsg_reportDTO.genderCat);
					}
					
					if(mapOfUsg_reportDTOToultrasonoTypeType.containsKey(oldUsg_reportDTO.ultrasonoTypeType)) {
						mapOfUsg_reportDTOToultrasonoTypeType.get(oldUsg_reportDTO.ultrasonoTypeType).remove(oldUsg_reportDTO);
					}
					if(mapOfUsg_reportDTOToultrasonoTypeType.get(oldUsg_reportDTO.ultrasonoTypeType).isEmpty()) {
						mapOfUsg_reportDTOToultrasonoTypeType.remove(oldUsg_reportDTO.ultrasonoTypeType);
					}
					
					if(mapOfUsg_reportDTOTocomment.containsKey(oldUsg_reportDTO.comment)) {
						mapOfUsg_reportDTOTocomment.get(oldUsg_reportDTO.comment).remove(oldUsg_reportDTO);
					}
					if(mapOfUsg_reportDTOTocomment.get(oldUsg_reportDTO.comment).isEmpty()) {
						mapOfUsg_reportDTOTocomment.remove(oldUsg_reportDTO.comment);
					}
					
					if(mapOfUsg_reportDTOTodoctorId.containsKey(oldUsg_reportDTO.doctorId)) {
						mapOfUsg_reportDTOTodoctorId.get(oldUsg_reportDTO.doctorId).remove(oldUsg_reportDTO);
					}
					if(mapOfUsg_reportDTOTodoctorId.get(oldUsg_reportDTO.doctorId).isEmpty()) {
						mapOfUsg_reportDTOTodoctorId.remove(oldUsg_reportDTO.doctorId);
					}
					
					if(mapOfUsg_reportDTOToinsertedByUserId.containsKey(oldUsg_reportDTO.insertedByUserId)) {
						mapOfUsg_reportDTOToinsertedByUserId.get(oldUsg_reportDTO.insertedByUserId).remove(oldUsg_reportDTO);
					}
					if(mapOfUsg_reportDTOToinsertedByUserId.get(oldUsg_reportDTO.insertedByUserId).isEmpty()) {
						mapOfUsg_reportDTOToinsertedByUserId.remove(oldUsg_reportDTO.insertedByUserId);
					}
					
					if(mapOfUsg_reportDTOToinsertedByOrganogramId.containsKey(oldUsg_reportDTO.insertedByOrganogramId)) {
						mapOfUsg_reportDTOToinsertedByOrganogramId.get(oldUsg_reportDTO.insertedByOrganogramId).remove(oldUsg_reportDTO);
					}
					if(mapOfUsg_reportDTOToinsertedByOrganogramId.get(oldUsg_reportDTO.insertedByOrganogramId).isEmpty()) {
						mapOfUsg_reportDTOToinsertedByOrganogramId.remove(oldUsg_reportDTO.insertedByOrganogramId);
					}
					
					if(mapOfUsg_reportDTOToinsertionDate.containsKey(oldUsg_reportDTO.insertionDate)) {
						mapOfUsg_reportDTOToinsertionDate.get(oldUsg_reportDTO.insertionDate).remove(oldUsg_reportDTO);
					}
					if(mapOfUsg_reportDTOToinsertionDate.get(oldUsg_reportDTO.insertionDate).isEmpty()) {
						mapOfUsg_reportDTOToinsertionDate.remove(oldUsg_reportDTO.insertionDate);
					}
					
					if(mapOfUsg_reportDTOTosearchColumn.containsKey(oldUsg_reportDTO.searchColumn)) {
						mapOfUsg_reportDTOTosearchColumn.get(oldUsg_reportDTO.searchColumn).remove(oldUsg_reportDTO);
					}
					if(mapOfUsg_reportDTOTosearchColumn.get(oldUsg_reportDTO.searchColumn).isEmpty()) {
						mapOfUsg_reportDTOTosearchColumn.remove(oldUsg_reportDTO.searchColumn);
					}
					
					if(mapOfUsg_reportDTOTolastModificationTime.containsKey(oldUsg_reportDTO.lastModificationTime)) {
						mapOfUsg_reportDTOTolastModificationTime.get(oldUsg_reportDTO.lastModificationTime).remove(oldUsg_reportDTO);
					}
					if(mapOfUsg_reportDTOTolastModificationTime.get(oldUsg_reportDTO.lastModificationTime).isEmpty()) {
						mapOfUsg_reportDTOTolastModificationTime.remove(oldUsg_reportDTO.lastModificationTime);
					}
					
					
				}
				if(usg_reportDTO.isDeleted == 0) 
				{
					
					mapOfUsg_reportDTOToiD.put(usg_reportDTO.iD, usg_reportDTO);
				
					if( ! mapOfUsg_reportDTOToprescriptionLabTestId.containsKey(usg_reportDTO.prescriptionLabTestId)) {
						mapOfUsg_reportDTOToprescriptionLabTestId.put(usg_reportDTO.prescriptionLabTestId, new HashSet<>());
					}
					mapOfUsg_reportDTOToprescriptionLabTestId.get(usg_reportDTO.prescriptionLabTestId).add(usg_reportDTO);
					
					if( ! mapOfUsg_reportDTOToprescriptionDetailsId.containsKey(usg_reportDTO.prescriptionDetailsId)) {
						mapOfUsg_reportDTOToprescriptionDetailsId.put(usg_reportDTO.prescriptionDetailsId, new HashSet<>());
					}
					mapOfUsg_reportDTOToprescriptionDetailsId.get(usg_reportDTO.prescriptionDetailsId).add(usg_reportDTO);
					
					if( ! mapOfUsg_reportDTOTocommonLabReportId.containsKey(usg_reportDTO.commonLabReportId)) {
						mapOfUsg_reportDTOTocommonLabReportId.put(usg_reportDTO.commonLabReportId, new HashSet<>());
					}
					mapOfUsg_reportDTOTocommonLabReportId.get(usg_reportDTO.commonLabReportId).add(usg_reportDTO);
					
					if( ! mapOfUsg_reportDTOToappointmentId.containsKey(usg_reportDTO.appointmentId)) {
						mapOfUsg_reportDTOToappointmentId.put(usg_reportDTO.appointmentId, new HashSet<>());
					}
					mapOfUsg_reportDTOToappointmentId.get(usg_reportDTO.appointmentId).add(usg_reportDTO);
					
					if( ! mapOfUsg_reportDTOToname.containsKey(usg_reportDTO.name)) {
						mapOfUsg_reportDTOToname.put(usg_reportDTO.name, new HashSet<>());
					}
					mapOfUsg_reportDTOToname.get(usg_reportDTO.name).add(usg_reportDTO);
					
					if( ! mapOfUsg_reportDTOTodateOfBirth.containsKey(usg_reportDTO.dateOfBirth)) {
						mapOfUsg_reportDTOTodateOfBirth.put(usg_reportDTO.dateOfBirth, new HashSet<>());
					}
					mapOfUsg_reportDTOTodateOfBirth.get(usg_reportDTO.dateOfBirth).add(usg_reportDTO);
					
					if( ! mapOfUsg_reportDTOToreferredByUserName.containsKey(usg_reportDTO.referredByUserName)) {
						mapOfUsg_reportDTOToreferredByUserName.put(usg_reportDTO.referredByUserName, new HashSet<>());
					}
					mapOfUsg_reportDTOToreferredByUserName.get(usg_reportDTO.referredByUserName).add(usg_reportDTO);
					
					if( ! mapOfUsg_reportDTOTogenderCat.containsKey(usg_reportDTO.genderCat)) {
						mapOfUsg_reportDTOTogenderCat.put(usg_reportDTO.genderCat, new HashSet<>());
					}
					mapOfUsg_reportDTOTogenderCat.get(usg_reportDTO.genderCat).add(usg_reportDTO);
					
					if( ! mapOfUsg_reportDTOToultrasonoTypeType.containsKey(usg_reportDTO.ultrasonoTypeType)) {
						mapOfUsg_reportDTOToultrasonoTypeType.put(usg_reportDTO.ultrasonoTypeType, new HashSet<>());
					}
					mapOfUsg_reportDTOToultrasonoTypeType.get(usg_reportDTO.ultrasonoTypeType).add(usg_reportDTO);
					
					if( ! mapOfUsg_reportDTOTocomment.containsKey(usg_reportDTO.comment)) {
						mapOfUsg_reportDTOTocomment.put(usg_reportDTO.comment, new HashSet<>());
					}
					mapOfUsg_reportDTOTocomment.get(usg_reportDTO.comment).add(usg_reportDTO);
					
					if( ! mapOfUsg_reportDTOTodoctorId.containsKey(usg_reportDTO.doctorId)) {
						mapOfUsg_reportDTOTodoctorId.put(usg_reportDTO.doctorId, new HashSet<>());
					}
					mapOfUsg_reportDTOTodoctorId.get(usg_reportDTO.doctorId).add(usg_reportDTO);
					
					if( ! mapOfUsg_reportDTOToinsertedByUserId.containsKey(usg_reportDTO.insertedByUserId)) {
						mapOfUsg_reportDTOToinsertedByUserId.put(usg_reportDTO.insertedByUserId, new HashSet<>());
					}
					mapOfUsg_reportDTOToinsertedByUserId.get(usg_reportDTO.insertedByUserId).add(usg_reportDTO);
					
					if( ! mapOfUsg_reportDTOToinsertedByOrganogramId.containsKey(usg_reportDTO.insertedByOrganogramId)) {
						mapOfUsg_reportDTOToinsertedByOrganogramId.put(usg_reportDTO.insertedByOrganogramId, new HashSet<>());
					}
					mapOfUsg_reportDTOToinsertedByOrganogramId.get(usg_reportDTO.insertedByOrganogramId).add(usg_reportDTO);
					
					if( ! mapOfUsg_reportDTOToinsertionDate.containsKey(usg_reportDTO.insertionDate)) {
						mapOfUsg_reportDTOToinsertionDate.put(usg_reportDTO.insertionDate, new HashSet<>());
					}
					mapOfUsg_reportDTOToinsertionDate.get(usg_reportDTO.insertionDate).add(usg_reportDTO);
					
					if( ! mapOfUsg_reportDTOTosearchColumn.containsKey(usg_reportDTO.searchColumn)) {
						mapOfUsg_reportDTOTosearchColumn.put(usg_reportDTO.searchColumn, new HashSet<>());
					}
					mapOfUsg_reportDTOTosearchColumn.get(usg_reportDTO.searchColumn).add(usg_reportDTO);
					
					if( ! mapOfUsg_reportDTOTolastModificationTime.containsKey(usg_reportDTO.lastModificationTime)) {
						mapOfUsg_reportDTOTolastModificationTime.put(usg_reportDTO.lastModificationTime, new HashSet<>());
					}
					mapOfUsg_reportDTOTolastModificationTime.get(usg_reportDTO.lastModificationTime).add(usg_reportDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Usg_reportDTO> getUsg_reportList() {
		List <Usg_reportDTO> usg_reports = new ArrayList<Usg_reportDTO>(this.mapOfUsg_reportDTOToiD.values());
		return usg_reports;
	}
	
	
	public Usg_reportDTO getUsg_reportDTOByID( long ID){
		return mapOfUsg_reportDTOToiD.get(ID);
	}
	
	
	public List<Usg_reportDTO> getUsg_reportDTOByprescription_lab_test_id(long prescription_lab_test_id) {
		return new ArrayList<>( mapOfUsg_reportDTOToprescriptionLabTestId.getOrDefault(prescription_lab_test_id,new HashSet<>()));
	}
	
	
	public List<Usg_reportDTO> getUsg_reportDTOByprescription_details_id(long prescription_details_id) {
		return new ArrayList<>( mapOfUsg_reportDTOToprescriptionDetailsId.getOrDefault(prescription_details_id,new HashSet<>()));
	}
	
	
	public List<Usg_reportDTO> getUsg_reportDTOBycommon_lab_report_id(long common_lab_report_id) {
		return new ArrayList<>( mapOfUsg_reportDTOTocommonLabReportId.getOrDefault(common_lab_report_id,new HashSet<>()));
	}
	
	
	public List<Usg_reportDTO> getUsg_reportDTOByappointment_id(long appointment_id) {
		return new ArrayList<>( mapOfUsg_reportDTOToappointmentId.getOrDefault(appointment_id,new HashSet<>()));
	}
	
	
	public List<Usg_reportDTO> getUsg_reportDTOByname(String name) {
		return new ArrayList<>( mapOfUsg_reportDTOToname.getOrDefault(name,new HashSet<>()));
	}
	
	
	public List<Usg_reportDTO> getUsg_reportDTOBydate_of_birth(long date_of_birth) {
		return new ArrayList<>( mapOfUsg_reportDTOTodateOfBirth.getOrDefault(date_of_birth,new HashSet<>()));
	}
	
	
	public List<Usg_reportDTO> getUsg_reportDTOByreferred_by_user_name(String referred_by_user_name) {
		return new ArrayList<>( mapOfUsg_reportDTOToreferredByUserName.getOrDefault(referred_by_user_name,new HashSet<>()));
	}
	
	
	public List<Usg_reportDTO> getUsg_reportDTOBygender_cat(int gender_cat) {
		return new ArrayList<>( mapOfUsg_reportDTOTogenderCat.getOrDefault(gender_cat,new HashSet<>()));
	}
	
	
	public List<Usg_reportDTO> getUsg_reportDTOByultrasono_type_type(long ultrasono_type_type) {
		return new ArrayList<>( mapOfUsg_reportDTOToultrasonoTypeType.getOrDefault(ultrasono_type_type,new HashSet<>()));
	}
	
	
	public List<Usg_reportDTO> getUsg_reportDTOBycomment(String comment) {
		return new ArrayList<>( mapOfUsg_reportDTOTocomment.getOrDefault(comment,new HashSet<>()));
	}
	
	
	public List<Usg_reportDTO> getUsg_reportDTOBydoctor_id(long doctor_id) {
		return new ArrayList<>( mapOfUsg_reportDTOTodoctorId.getOrDefault(doctor_id,new HashSet<>()));
	}
	
	
	public List<Usg_reportDTO> getUsg_reportDTOByinserted_by_user_id(long inserted_by_user_id) {
		return new ArrayList<>( mapOfUsg_reportDTOToinsertedByUserId.getOrDefault(inserted_by_user_id,new HashSet<>()));
	}
	
	
	public List<Usg_reportDTO> getUsg_reportDTOByinserted_by_organogram_id(long inserted_by_organogram_id) {
		return new ArrayList<>( mapOfUsg_reportDTOToinsertedByOrganogramId.getOrDefault(inserted_by_organogram_id,new HashSet<>()));
	}
	
	
	public List<Usg_reportDTO> getUsg_reportDTOByinsertion_date(long insertion_date) {
		return new ArrayList<>( mapOfUsg_reportDTOToinsertionDate.getOrDefault(insertion_date,new HashSet<>()));
	}
	
	
	public List<Usg_reportDTO> getUsg_reportDTOBysearch_column(String search_column) {
		return new ArrayList<>( mapOfUsg_reportDTOTosearchColumn.getOrDefault(search_column,new HashSet<>()));
	}
	
	
	public List<Usg_reportDTO> getUsg_reportDTOBylastModificationTime(long lastModificationTime) {
		return new ArrayList<>( mapOfUsg_reportDTOTolastModificationTime.getOrDefault(lastModificationTime,new HashSet<>()));
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "usg_report";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


