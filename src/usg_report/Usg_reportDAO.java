package usg_report;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;
import sessionmanager.SessionConstants;
import java.sql.SQLException;


import org.apache.log4j.Logger;

import dbm.*;

import repository.RepositoryManager;

import util.*;
import pb.*;
import user.UserDTO;

public class Usg_reportDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());
	

	
	public Usg_reportDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";
		commonMaps = new Usg_reportMAPS(tableName);
		columnNames = new String[] 
		{
			"ID",
			"prescription_lab_test_id",
			"prescription_details_id",
			"common_lab_report_id",
			"appointment_id",
			"name",
			"date_of_birth",
			"referred_by_user_name",
			"gender_cat",
			"ultrasono_type_type",
			"xray_type",
			"xray_subtype",
			"comment",
			"doctor_id",
			"inserted_by_user_id",
			"inserted_by_organogram_id",
			"insertion_date",
			"search_column",
			"doctor_recommendation",
			"title",
			"isDeleted",
			"lastModificationTime"
		};
	}
	
	public Usg_reportDAO()
	{
		this("usg_report");		
	}
	
	public void setSearchColumn(Usg_reportDTO usg_reportDTO)
	{
		usg_reportDTO.searchColumn = "";
		usg_reportDTO.searchColumn += usg_reportDTO.name + " ";
		usg_reportDTO.searchColumn += usg_reportDTO.referredByUserName + " ";
		usg_reportDTO.searchColumn += CatDAO.getName("English", "gender", usg_reportDTO.genderCat) + " " + CatDAO.getName("Bangla", "gender", usg_reportDTO.genderCat) + " ";
		usg_reportDTO.searchColumn += CommonDAO.getName("English", "ultrasono_type", usg_reportDTO.ultrasonoTypeType) + " " + CommonDAO.getName("Bangla", "ultrasono_type", usg_reportDTO.ultrasonoTypeType) + " ";
		
	}
	
	public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException
	{
		Usg_reportDTO usg_reportDTO = (Usg_reportDTO)commonDTO;
		int index = 1;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(usg_reportDTO);
		if(isInsert)
		{
			ps.setObject(index++,usg_reportDTO.iD);
		}
		ps.setObject(index++,usg_reportDTO.prescriptionLabTestId);
		ps.setObject(index++,usg_reportDTO.prescriptionDetailsId);
		ps.setObject(index++,usg_reportDTO.commonLabReportId);
		ps.setObject(index++,usg_reportDTO.appointmentId);
		ps.setObject(index++,usg_reportDTO.name);
		ps.setObject(index++,usg_reportDTO.dateOfBirth);
		ps.setObject(index++,usg_reportDTO.referredByUserName);
		ps.setObject(index++,usg_reportDTO.genderCat);
		ps.setObject(index++,usg_reportDTO.ultrasonoTypeType);
		ps.setObject(index++,usg_reportDTO.xRayType);
		ps.setObject(index++,usg_reportDTO.xRaySubType);
		ps.setObject(index++,usg_reportDTO.comment);
		ps.setObject(index++,usg_reportDTO.doctorId);
		ps.setObject(index++,usg_reportDTO.insertedByUserId);
		ps.setObject(index++,usg_reportDTO.insertedByOrganogramId);
		ps.setObject(index++,usg_reportDTO.insertionDate);
		ps.setObject(index++,usg_reportDTO.searchColumn);
		ps.setObject(index++,usg_reportDTO.doctorRecommendation);
		ps.setObject(index++,usg_reportDTO.title);
		if(isInsert)
		{
			ps.setObject(index++, 0);
			ps.setObject(index++, lastModificationTime);
		}
	}
	
	public void get(Usg_reportDTO usg_reportDTO, ResultSet rs) throws SQLException
	{
		usg_reportDTO.iD = rs.getLong("ID");
		usg_reportDTO.prescriptionLabTestId = rs.getLong("prescription_lab_test_id");
		usg_reportDTO.prescriptionDetailsId = rs.getLong("prescription_details_id");
		usg_reportDTO.commonLabReportId = rs.getLong("common_lab_report_id");
		usg_reportDTO.appointmentId = rs.getLong("appointment_id");
		usg_reportDTO.name = rs.getString("name");
		usg_reportDTO.dateOfBirth = rs.getLong("date_of_birth");
		usg_reportDTO.referredByUserName = rs.getString("referred_by_user_name");
		usg_reportDTO.genderCat = rs.getInt("gender_cat");
		usg_reportDTO.ultrasonoTypeType = rs.getLong("ultrasono_type_type");
		usg_reportDTO.xRayType = rs.getLong("xray_type");
		usg_reportDTO.xRaySubType = rs.getLong("xray_subtype");
		usg_reportDTO.comment = rs.getString("comment");
		usg_reportDTO.doctorId = rs.getLong("doctor_id");
		usg_reportDTO.insertedByUserId = rs.getLong("inserted_by_user_id");
		usg_reportDTO.insertedByOrganogramId = rs.getLong("inserted_by_organogram_id");
		usg_reportDTO.insertionDate = rs.getLong("insertion_date");
		usg_reportDTO.searchColumn = rs.getString("search_column");
		usg_reportDTO.doctorRecommendation = rs.getString("doctor_recommendation");
		usg_reportDTO.title = rs.getString("title");
		usg_reportDTO.isDeleted = rs.getInt("isDeleted");
		usg_reportDTO.lastModificationTime = rs.getLong("lastModificationTime");
	}
	
	
	
	public Usg_reportDTO getDTOByCommonLabReportId (long commonLabReportId)
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Usg_reportDTO usg_reportDTO = null;
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
			
            sql += " WHERE common_lab_report_id =" + commonLabReportId;
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			if(rs.next()){
				usg_reportDTO = new Usg_reportDTO();

				get(usg_reportDTO, rs);

			}			
			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return usg_reportDTO;
	}
		
	

	//need another getter for repository
	public Usg_reportDTO getDTOByID (long ID)
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Usg_reportDTO usg_reportDTO = null;
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
			
            sql += " WHERE ID=" + ID;
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			if(rs.next()){
				usg_reportDTO = new Usg_reportDTO();

				get(usg_reportDTO, rs);

			}			
			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return usg_reportDTO;
	}
	
	
	
	
	public List<Usg_reportDTO> getDTOs(Collection recordIDs){
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Usg_reportDTO usg_reportDTO = null;
		List<Usg_reportDTO> usg_reportDTOList = new ArrayList<>();
		if(recordIDs.isEmpty()){
			return usg_reportDTOList;
		}
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
            
            sql += " WHERE ID IN ( ";

			for(int i = 0;i<recordIDs.size();i++){
				if(i!=0){
					sql+=",";
				}
				sql+=((ArrayList)recordIDs).get(i);
			}
			sql+=")  order by lastModificationTime desc";
			
			printSql(sql);
			
			logger.debug("sql " + sql);
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				usg_reportDTO = new Usg_reportDTO();
				get(usg_reportDTO, rs);
				System.out.println("got this DTO: " + usg_reportDTO);
				
				usg_reportDTOList.add(usg_reportDTO);

			}			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return usg_reportDTOList;
	
	}
	
	

	
	
	
	//add repository
	public List<Usg_reportDTO> getAllUsg_report (boolean isFirstReload)
    {
		List<Usg_reportDTO> usg_reportDTOList = new ArrayList<>();

		String sql = "SELECT * FROM usg_report";
		sql += " WHERE ";
	

		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by usg_report.lastModificationTime desc";
		printSql(sql);
		
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;

		
		try{
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();
			rs = stmt.executeQuery(sql);
			

			while(rs.next()){
				Usg_reportDTO usg_reportDTO = new Usg_reportDTO();
				get(usg_reportDTO, rs);
				
				usg_reportDTOList.add(usg_reportDTO);
			}			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}

		return usg_reportDTOList;
    }

	
	public List<Usg_reportDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO)
	{
		return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
	}

	
	public List<Usg_reportDTO> getDTOs(Hashtable  p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
			String filter, boolean tableHasJobCat)
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		List<Usg_reportDTO> usg_reportDTOList = new ArrayList<>();

		try{
			
			String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat);
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				Usg_reportDTO usg_reportDTO = new Usg_reportDTO();
				get(usg_reportDTO, rs);
				
				usg_reportDTOList.add(usg_reportDTO);
			}					
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return usg_reportDTOList;
	
	}
	public String getSqlWithSearchCriteriaForNormalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
			UserDTO userDTO, String filter, boolean tableHasJobCat)
    {
		boolean viewAll = false;
		
		if(p_searchCriteria != null && p_searchCriteria.get("ViewAll") != null)
		{
			System.out.println("ViewAll = " + p_searchCriteria.get("ViewAll"));
			viewAll = true;
		}
		else
		{
			System.out.println("ViewAll is null ");
		}
		
		String sql = "SELECT ";
		if(category == GETIDS)
		{
			sql += " " + tableName + ".ID as ID ";
		}
		else if(category == GETCOUNT)
		{
			sql += " count( " + tableName + ".ID) as countID ";
		}
		else if(category == GETDTOS)
		{
			sql += " distinct " + tableName + ".* ";
		}
		sql += "FROM " + tableName + " ";
		sql += joinSQL;
		

		
		String AnyfieldSql = "";
		String AllFieldSql = "";
		
		if(p_searchCriteria != null)
		{
						
			
			Enumeration names = p_searchCriteria.keys();
			String str, value;
			
			AnyfieldSql = "(";
			
			if(p_searchCriteria.get("AnyField")!= null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase(""))
			{
				AnyfieldSql+= tableName + ".search_column like '%" + p_searchCriteria.get("AnyField").toString() + "%'";			
			}
			AnyfieldSql += ")";
			System.out.println("AnyfieldSql = " + AnyfieldSql);
			
			AllFieldSql = "(";
			int i = 0;
			while(names.hasMoreElements())
			{				
				str = (String) names.nextElement();
				value = (String)p_searchCriteria.get(str);
		        System.out.println(str + ": " + value);
				if(value != null && !value.equalsIgnoreCase("") && (
						 str.equals("name")
						|| str.equals("date_of_birth_start")
						|| str.equals("date_of_birth_end")
						|| str.equals("referred_by_user_name")
						|| str.equals("gender_cat")
						|| str.equals("ultrasono_type_type")
						|| str.equals("insertion_date_start")
						|| str.equals("insertion_date_end")
				)
						
				)
				{
					if(p_searchCriteria.get(str).equals("any"))
					{
						continue;
					}

					if( i > 0)
					{
						AllFieldSql+= " AND  ";
					}
					
					 if(str.equals("name"))
					{
						AllFieldSql += "" + tableName + ".name like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("date_of_birth_start"))
					{
						AllFieldSql += "" + tableName + ".date_of_birth >= " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("date_of_birth_end"))
					{
						AllFieldSql += "" + tableName + ".date_of_birth <= " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("referred_by_user_name"))
					{
						AllFieldSql += "" + tableName + ".referred_by_user_name like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("gender_cat"))
					{
						AllFieldSql += "" + tableName + ".gender_cat = " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("ultrasono_type_type"))
					{
						AllFieldSql += "" + tableName + ".ultrasono_type_type = " + p_searchCriteria.get(str);
						i ++;
					}					
					else if(str.equals("insertion_date_start"))
					{
						AllFieldSql += "" + tableName + ".insertion_date >= " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("insertion_date_end"))
					{
						AllFieldSql += "" + tableName + ".insertion_date <= " + p_searchCriteria.get(str);
						i ++;
					}
					
					
				}
			}
			
			AllFieldSql += ")";
			System.out.println("AllFieldSql = " + AllFieldSql);
			
			
		}
		
		
		sql += " WHERE ";
		
		sql += " (" + tableName + ".isDeleted = 0 ";			
		sql += ")";
		
		
		if(!filter.equalsIgnoreCase(""))
		{
			sql += " and " + filter + " ";
		}
		
		if(!AnyfieldSql.equals("()") && !AnyfieldSql.equals(""))
		{
			sql += " AND " + AnyfieldSql;
			
		}
		if(!AllFieldSql.equals("()") && !AllFieldSql.equals(""))
		{			
			sql += " AND " + AllFieldSql;
		}
		
	
			
		sql += " order by " + tableName + ".lastModificationTime desc ";

		printSql(sql);
		
		if(limit >= 0)
		{
			sql += " limit " + limit;
		}
		if(offset >= 0)
		{
			sql += " offset " + offset;
		}
		
		System.out.println("-------------- sql = " + sql);
		
		return sql;
    }
	
	public String getSqlWithSearchCriteria(Hashtable p_searchCriteria, int limit, int offset, int category,
			boolean isPermanentTable, UserDTO userDTO, String filter, boolean tableHasJobCat)
    {		
		return getSqlWithSearchCriteriaForNormalSearch(p_searchCriteria, limit, offset, category, userDTO, filter, tableHasJobCat);	
    }
				
}
	