package usg_report;

import java.io.IOException;
import java.io.*;
import java.text.SimpleDateFormat;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import org.apache.log4j.Logger;

import login.LoginDTO;
import permission.MenuConstants;
import role.PermissionRepository;


import sessionmanager.SessionConstants;
import ultrasono_type.Ultrasono_typeDAO;
import ultrasono_type.Ultrasono_typeDTO;
import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;
import util.CommonRequestHandler;
import util.RecordNavigationManager4;
import xray_report.Xray_reportDAO;
import xray_report.Xray_reportDTO;

import java.util.*;
import javax.servlet.http.*;
import java.util.UUID;





import geolocation.GeoLocationDAO2;
import java.util.StringTokenizer;

import com.google.gson.Gson;

import common_lab_report.CommonLabReportDetailsDTO;
import common_lab_report.Common_lab_reportDAO;
import common_lab_report.Common_lab_reportDTO;
import pb.*;
import pbReport.*;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;




/**
 * Servlet implementation class Usg_reportServlet
 */
@WebServlet("/Usg_reportServlet")
@MultipartConfig
public class Usg_reportServlet extends HttpServlet 
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Usg_reportServlet.class);

    String tableName = "usg_report";

	Usg_reportDAO usg_reportDAO;
	CommonRequestHandler commonRequestHandler;
    private final Gson gson = new Gson();
    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Usg_reportServlet() 
	{
        super();
    	try
    	{
			usg_reportDAO = new Usg_reportDAO(tableName);
			commonRequestHandler = new CommonRequestHandler(usg_reportDAO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }   

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		System.out.println("In doget request = " + request);
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		try
		{
			String actionType = request.getParameter("actionType");
			if(actionType.equals("getAddPage"))
			{
				if(userDTO.roleID == SessionConstants.RADIOLOGIST || userDTO.roleID == SessionConstants.ADMIN_ROLE)
				{
					commonRequestHandler.getAddPage(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getEditPage"))
			{
				if(userDTO.roleID == SessionConstants.RADIOLOGIST || userDTO.roleID == SessionConstants.ADMIN_ROLE)
				{
					getUsg_report(request, response);					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}						
			}
			else if(actionType.equals("getURL"))
			{
				String URL = request.getParameter("URL");
				System.out.println("URL = " + URL);
				response.sendRedirect(URL);			
			}			
			else if(actionType.equals("search"))
			{
				System.out.println("search requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.USG_REPORT_SEARCH))
				{
					if(isPermanentTable)
					{
						String filter = request.getParameter("filter");
						System.out.println("filter = " + filter);
						if(filter!=null)
						{
							filter = ""; //shouldn't be directly used, rather manipulate it.
							searchUsg_report(request, response, isPermanentTable, filter);
						}
						else
						{
							searchUsg_report(request, response, isPermanentTable, "");
						}
					}					
					else
					{
						//searchUsg_report(request, response, tempTableName, isPermanentTable);
					}
				}			
			}
			else if(actionType.equals("view"))
			{
				System.out.println("view requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.USG_REPORT_SEARCH))
				{
					commonRequestHandler.view(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else
			{
				request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}


	
	

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		// TODO Auto-generated method stub
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		System.out.println("doPost");
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		
		try
		{
			String actionType = request.getParameter("actionType");
			System.out.println("actionType = " + actionType);
			if(actionType.equals("add"))
			{
				
				if(userDTO.roleID == SessionConstants.RADIOLOGIST || userDTO.roleID == SessionConstants.ADMIN_ROLE)
				{
					System.out.println("going to  addUsg_report ");
					addUsg_report(request, response, true, userDTO, true);
				}
				else
				{
					System.out.println("Not going to  addUsg_report ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}			
			else if(actionType.equals("getDTO"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.USG_REPORT_ADD))
				{
					getDTO(request, response);					
				}
				else
				{
					System.out.println("Not going to  addUsg_report ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else if(actionType.equals("edit"))
			{
				
				if(userDTO.roleID == SessionConstants.RADIOLOGIST || userDTO.roleID == SessionConstants.ADMIN_ROLE)
				{					
					addUsg_report(request, response, false, userDTO, isPermanentTable);					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("delete"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.USG_REPORT_UPDATE))
				{
					commonRequestHandler.delete(request, response, userDTO);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}	
			else if(actionType.equals("search"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.USG_REPORT_SEARCH))
				{
					searchUsg_report(request, response, true, "");
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getGeo"))
			{
				System.out.println("going to geoloc ");
				request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
			}
			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}
	
	private void getDTO(HttpServletRequest request, HttpServletResponse response) 
	{
		try 
		{
			System.out.println("In getDTO");
			Usg_reportDTO usg_reportDTO = usg_reportDAO.getDTOByID(Long.parseLong(request.getParameter("ID")));
			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			
			String encoded = this.gson.toJson(usg_reportDTO);
			System.out.println("json encoded data = " + encoded);
			out.print(encoded);
			out.flush();
		}
		catch (NumberFormatException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	private void addUsg_report(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, boolean isPermanentTable) throws IOException 
	{
		// TODO Auto-generated method stub
		try 
		{
			request.setAttribute("failureMessage", "");
			System.out.println("%%%% addUsg_report");
			String path = getServletContext().getRealPath("/img2/");
			Usg_reportDTO usg_reportDTO;
			SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
						
			if(addFlag == true)
			{
				usg_reportDTO = new Usg_reportDTO();
			}
			else
			{
				usg_reportDTO = usg_reportDAO.getDTOByID(Long.parseLong(request.getParameter("iD")));
			}
			
			String Value = "";

			Value = request.getParameter("prescriptionLabTestId");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("prescriptionLabTestId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				usg_reportDTO.prescriptionLabTestId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("prescriptionDetailsId");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("prescriptionDetailsId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				usg_reportDTO.prescriptionDetailsId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			

			Value = request.getParameter("appointmentId");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("appointmentId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				usg_reportDTO.appointmentId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("name");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("name = " + Value);
			if(Value != null)
			{
				usg_reportDTO.name = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("dateOfBirth");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("dateOfBirth = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				try 
				{
					Date d = f.parse(Value);
					usg_reportDTO.dateOfBirth = d.getTime();
				}
				catch (Exception e) 
				{
					e.printStackTrace();
				}
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("referredByUserName");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("referredByUserName = " + Value);
			if(Value != null)
			{
				usg_reportDTO.referredByUserName = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("genderCat");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("genderCat = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				usg_reportDTO.genderCat = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("ultrasonoTypeType");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("ultrasonoTypeType = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				usg_reportDTO.ultrasonoTypeType = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			
			Value = request.getParameter("xRayType");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("xRayType = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				usg_reportDTO.xRayType = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			
			Value = request.getParameter("xRaySubType");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("xRaySubType = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				usg_reportDTO.xRaySubType = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("comment");

			
			System.out.println("comment = " + Value);
			if(Value != null)
			{
				usg_reportDTO.comment = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("doctorId");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("doctorId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				usg_reportDTO.doctorId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			if(addFlag)
			{
				usg_reportDTO.insertedByUserId = userDTO.ID;
			}


			if(addFlag)
			{
				usg_reportDTO.insertedByOrganogramId = userDTO.organogramID;
			}


			if(addFlag)
			{
				Calendar c = Calendar.getInstance();
				c.set(Calendar.HOUR_OF_DAY, 0);
				c.set(Calendar.MINUTE, 0);
				c.set(Calendar.SECOND, 0);
				c.set(Calendar.MILLISECOND, 0);
				
				usg_reportDTO.insertionDate = c.getTimeInMillis();
			}			


			
			if(usg_reportDTO.ultrasonoTypeType != -1) //usg
			{
				Ultrasono_typeDAO ultrasono_typeDAO = new Ultrasono_typeDAO();
				Ultrasono_typeDTO ultrasono_typeDTO = ultrasono_typeDAO.getDTOByID(usg_reportDTO.ultrasonoTypeType);
				if(ultrasono_typeDTO != null)
				{
					usg_reportDTO.title = ultrasono_typeDTO.heading;
				}
				
			}
			else if(usg_reportDTO.xRayType != -1) //xray
			{
				Xray_reportDAO xray_reportDAO = new Xray_reportDAO();
				Xray_reportDTO xray_reportDTO = xray_reportDAO.getDTOByID(usg_reportDTO.xRayType);
				if(xray_reportDTO != null)
				{
					usg_reportDTO.title = xray_reportDTO.comment;
				}
			}
			
			System.out.println("Done adding  addUsg_report dto = " + usg_reportDTO);
			long returnedID = -1;
			Common_lab_reportDAO common_lab_reportDAO = new Common_lab_reportDAO();
			Common_lab_reportDTO common_lab_reportDTO;
			if(addFlag)
			{
				common_lab_reportDTO = new Common_lab_reportDTO();
			}
			else
			{
				common_lab_reportDTO = common_lab_reportDAO.getDTOByID(usg_reportDTO.commonLabReportId);
			}
			
			if(common_lab_reportDTO != null)
			{
				common_lab_reportDTO.prescriptionLabTestId = usg_reportDTO.prescriptionLabTestId;
				if(usg_reportDTO.ultrasonoTypeType != -1)
				{
					common_lab_reportDTO.labTestId = SessionConstants.LAB_TEST_ULTRASONOGRAM;
				}
				else
				{
					common_lab_reportDTO.labTestId = SessionConstants.LAB_TEST_XRAY;
				}
				
				common_lab_reportDTO.name = usg_reportDTO.name;
				common_lab_reportDTO.sex = CatDAO.getName("English", "gender", usg_reportDTO.genderCat);
				common_lab_reportDTO.comment = usg_reportDTO.comment;
				common_lab_reportDTO.referredBy = usg_reportDTO.referredByUserName;
				
				
				common_lab_reportDTO.insertedByOrganogramId = usg_reportDTO.insertedByOrganogramId;
				common_lab_reportDTO.insertedByUserId = usg_reportDTO.insertedByUserId;
				common_lab_reportDTO.insertionDate = usg_reportDTO.insertionDate;
			}
			
			if(addFlag == true)
			{
				usg_reportDTO.commonLabReportId = common_lab_reportDAO.add(common_lab_reportDTO);
				returnedID = usg_reportDAO.manageWriteOperations(usg_reportDTO, SessionConstants.INSERT, -1, userDTO);
			}
			else
			{	
				common_lab_reportDAO.update(common_lab_reportDTO);
				returnedID = usg_reportDAO.manageWriteOperations(usg_reportDTO, SessionConstants.UPDATE, -1, userDTO);											
			}


			if(isPermanentTable)
			{
				String inPlaceSubmit = request.getParameter("inplacesubmit");
				
				if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
				{
					getUsg_report(request, response, returnedID);
				}
				else
				{
					response.sendRedirect("Common_lab_reportServlet?actionType=view&ID=" + usg_reportDTO.commonLabReportId);
				}
			}
			else
			{
				commonRequestHandler.validate(usg_reportDAO.getDTOByID(returnedID), request, response, userDTO);
			}
					
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	



	
	
	

	private void getUsg_report(HttpServletRequest request, HttpServletResponse response, long id) throws ServletException, IOException
	{
		System.out.println("in getUsg_report");
		Usg_reportDTO usg_reportDTO = null;
		try 
		{
			usg_reportDTO = usg_reportDAO.getDTOByID(id);
			request.setAttribute("ID", usg_reportDTO.iD);
			request.setAttribute("usg_reportDTO",usg_reportDTO);
			request.setAttribute("usg_reportDAO",usg_reportDAO);
			
			String URL= "";
			
			String inPlaceEdit = request.getParameter("inplaceedit");
			String inPlaceSubmit = request.getParameter("inplacesubmit");
			String getBodyOnly = request.getParameter("getBodyOnly");
			
			if(inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase(""))
			{
				URL = "usg_report/usg_reportInPlaceEdit.jsp";	
				request.setAttribute("inplaceedit","");				
			}
			else if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
			{
				URL = "usg_report/usg_reportSearchRow.jsp";
				request.setAttribute("inplacesubmit","");					
			}
			else
			{
				if(getBodyOnly  != null && !getBodyOnly.equalsIgnoreCase(""))
				{
					URL = "usg_report/usg_reportEditBody.jsp?actionType=edit";
				}
				else
				{
					URL = "usg_report/usg_reportEdit.jsp?actionType=edit";
				}				
			}
			
			RequestDispatcher rd = request.getRequestDispatcher(URL);
			rd.forward(request, response);
		}
		catch (NumberFormatException e) 
		{
			e.printStackTrace();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	
	private void getUsg_report(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		getUsg_report(request, response, Long.parseLong(request.getParameter("ID")));	
	}
	
	private void searchUsg_report(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException
	{
		System.out.println("in  searchUsg_report 1");
		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String ajax = request.getParameter("ajax");
		boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);
		
        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
			SessionConstants.NAV_USG_REPORT,
			request,
			usg_reportDAO,
			SessionConstants.VIEW_USG_REPORT,
			SessionConstants.SEARCH_USG_REPORT,
			tableName,
			isPermanent,
			userDTO,
			filter,
			true);
        try
        {
			System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        }
        catch(Exception e)
        {
			System.out.println("failed to dojob" + e);
        }

		request.setAttribute("usg_reportDAO",usg_reportDAO);
        RequestDispatcher rd;
        if(!isPermanent)
        {
        	if(hasAjax == false)
	        {
	        	System.out.println("Going to usg_report/usg_reportApproval.jsp");
	        	rd = request.getRequestDispatcher("usg_report/usg_reportApproval.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to usg_report/usg_reportApprovalForm.jsp");
	        	rd = request.getRequestDispatcher("usg_report/usg_reportApprovalForm.jsp");
	        }
        }
        else
        {
	        if(hasAjax == false)
	        {
	        	System.out.println("Going to usg_report/usg_reportSearch.jsp");
	        	rd = request.getRequestDispatcher("usg_report/usg_reportSearch.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to usg_report/usg_reportSearchForm.jsp");
	        	rd = request.getRequestDispatcher("usg_report/usg_reportSearchForm.jsp");
	        }
        }
		rd.forward(request, response);
	}
	
}

