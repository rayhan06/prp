package usg_report;
import java.util.*; 
import util.*; 


public class Usg_reportDTO extends CommonDTO
{

	public long prescriptionLabTestId = -1;
	public long prescriptionDetailsId = -1;
	public long commonLabReportId = -1;
	public long appointmentId = -1;
    public String name = "";
	public long dateOfBirth = System.currentTimeMillis();
    public String referredByUserName = "";
	public int genderCat = -1;
	public long ultrasonoTypeType = -1;
	public long xRayType = -1;
	public long xRaySubType = -1;
    public String comment = "";
	public long doctorId = -1;
	public long insertedByUserId = -1;
	public long insertedByOrganogramId = -1;
	public long insertionDate = -1;
    public String doctorRecommendation = "";
    public String title = "";
	
	
    @Override
	public String toString() {
            return "$Usg_reportDTO[" +
            " iD = " + iD +
            " prescriptionLabTestId = " + prescriptionLabTestId +
            " prescriptionDetailsId = " + prescriptionDetailsId +
            " commonLabReportId = " + commonLabReportId +
            " appointmentId = " + appointmentId +
            " name = " + name +
            " dateOfBirth = " + dateOfBirth +
            " referredByUserName = " + referredByUserName +
            " genderCat = " + genderCat +
            " ultrasonoTypeType = " + ultrasonoTypeType +
            " comment = " + comment +
            " doctorId = " + doctorId +
            " insertedByUserId = " + insertedByUserId +
            " insertedByOrganogramId = " + insertedByOrganogramId +
            " insertionDate = " + insertionDate +
            " searchColumn = " + searchColumn +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}