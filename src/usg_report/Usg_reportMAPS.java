package usg_report;
import java.util.*; 
import util.*;


public class Usg_reportMAPS extends CommonMaps
{	
	public Usg_reportMAPS(String tableName)
	{
		

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("prescriptionLabTestId".toLowerCase(), "prescriptionLabTestId".toLowerCase());
		java_DTO_map.put("prescriptionDetailsId".toLowerCase(), "prescriptionDetailsId".toLowerCase());
		java_DTO_map.put("commonLabReportId".toLowerCase(), "commonLabReportId".toLowerCase());
		java_DTO_map.put("appointmentId".toLowerCase(), "appointmentId".toLowerCase());
		java_DTO_map.put("name".toLowerCase(), "name".toLowerCase());
		java_DTO_map.put("dateOfBirth".toLowerCase(), "dateOfBirth".toLowerCase());
		java_DTO_map.put("referredByUserName".toLowerCase(), "referredByUserName".toLowerCase());
		java_DTO_map.put("genderCat".toLowerCase(), "genderCat".toLowerCase());
		java_DTO_map.put("ultrasonoTypeType".toLowerCase(), "ultrasonoTypeType".toLowerCase());
		java_DTO_map.put("comment".toLowerCase(), "comment".toLowerCase());
		java_DTO_map.put("doctorId".toLowerCase(), "doctorId".toLowerCase());
		java_DTO_map.put("insertedByUserId".toLowerCase(), "insertedByUserId".toLowerCase());
		java_DTO_map.put("insertedByOrganogramId".toLowerCase(), "insertedByOrganogramId".toLowerCase());
		java_DTO_map.put("insertionDate".toLowerCase(), "insertionDate".toLowerCase());
		java_DTO_map.put("searchColumn".toLowerCase(), "searchColumn".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("prescription_lab_test_id".toLowerCase(), "prescriptionLabTestId".toLowerCase());
		java_SQL_map.put("prescription_details_id".toLowerCase(), "prescriptionDetailsId".toLowerCase());
		java_SQL_map.put("common_lab_report_id".toLowerCase(), "commonLabReportId".toLowerCase());
		java_SQL_map.put("appointment_id".toLowerCase(), "appointmentId".toLowerCase());
		java_SQL_map.put("name".toLowerCase(), "name".toLowerCase());
		java_SQL_map.put("date_of_birth".toLowerCase(), "dateOfBirth".toLowerCase());
		java_SQL_map.put("referred_by_user_name".toLowerCase(), "referredByUserName".toLowerCase());
		java_SQL_map.put("gender_cat".toLowerCase(), "genderCat".toLowerCase());
		java_SQL_map.put("ultrasono_type_type".toLowerCase(), "ultrasonoTypeType".toLowerCase());
		java_SQL_map.put("comment".toLowerCase(), "comment".toLowerCase());
		java_SQL_map.put("doctor_id".toLowerCase(), "doctorId".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Prescription Lab Test Id".toLowerCase(), "prescriptionLabTestId".toLowerCase());
		java_Text_map.put("Prescription Details Id".toLowerCase(), "prescriptionDetailsId".toLowerCase());
		java_Text_map.put("Common Lab Report Id".toLowerCase(), "commonLabReportId".toLowerCase());
		java_Text_map.put("Appointment Id".toLowerCase(), "appointmentId".toLowerCase());
		java_Text_map.put("Name".toLowerCase(), "name".toLowerCase());
		java_Text_map.put("Date Of Birth".toLowerCase(), "dateOfBirth".toLowerCase());
		java_Text_map.put("Referred By User Name".toLowerCase(), "referredByUserName".toLowerCase());
		java_Text_map.put("Gender".toLowerCase(), "genderCat".toLowerCase());
		java_Text_map.put("Ultrasono Type".toLowerCase(), "ultrasonoTypeType".toLowerCase());
		java_Text_map.put("Comment".toLowerCase(), "comment".toLowerCase());
		java_Text_map.put("Doctor Id".toLowerCase(), "doctorId".toLowerCase());
		java_Text_map.put("Inserted By User Id".toLowerCase(), "insertedByUserId".toLowerCase());
		java_Text_map.put("Inserted By Organogram Id".toLowerCase(), "insertedByOrganogramId".toLowerCase());
		java_Text_map.put("Insertion Date".toLowerCase(), "insertionDate".toLowerCase());
		java_Text_map.put("Search Column".toLowerCase(), "searchColumn".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}