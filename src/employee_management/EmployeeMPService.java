package employee_management;

/*
 * @author Md. Erfan Hossain
 * @created 21/08/2021 - 2:14 PM
 * @project parliament
 */

import mail.EmailService;
import mail.SendEmailDTO;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import pb.Utils;
import sms.SmsService;
import sms_log.Sms_logDTO;
import util.HttpRequestUtils;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

@SuppressWarnings("Duplicates")
public interface EmployeeMPService {
    Logger loggerEmployeeMPService = Logger.getLogger(EmployeeMPService.class);

    String smsBody = "Dear %s,\n" + "Your user name is : %s\n" + "Password: %s";

    default void setEmployeeMPModel(EmployeeMPModel model, HttpServletRequest request, Boolean addFlag, boolean isLangEng) throws Exception {
        if (!addFlag) {
            throw new Exception("Update is not supported");
        }
        model.insertionDate = model.lastModificationTime = System.currentTimeMillis();
        model.insertedBy = model.modifiedBy = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO.employee_record_id;
        String dob = request.getParameter("dateOfBirth");//dd-MM-yyyy
        model.password =dob.substring(0, 2) + dob.substring(3, 5) + dob.substring(6);
        model.personalEml = Utils.doJsoupCleanOrReturnEmpty(request.getParameter("personalEml"));
    }

    default void sendSmsAndMailForNewUser(EmployeeMPModel model) {
        Sms_logDTO smsLogDTO = new Sms_logDTO();
        smsLogDTO.message = String.format(smsBody, model.nameEng, model.userName, model.password);
        smsLogDTO.insertionTime = Calendar.getInstance().getTimeInMillis();
        smsLogDTO.receiver = model.mobileNumber;
        try {
            SmsService.send(smsLogDTO.receiver, smsLogDTO.message);
        } catch (IOException e) {
            e.printStackTrace();
        }

        String email = model.personalEml == null ? null : model.personalEml.trim();
        if (Utils.isEmailValid(email)) {
            SendEmailDTO sendEmailDTO = new SendEmailDTO();
            sendEmailDTO.setTo(new String[]{email});
            sendEmailDTO.setText(smsLogDTO.message);
            sendEmailDTO.setSubject("New User Creation");
            sendEmailDTO.setFrom("edms@pbrlp.gov.bd");
            try {
                new EmailService().sendMail(sendEmailDTO);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else {
            loggerEmployeeMPService.debug("<<<<mail id is null or empty, mailId : " + model.personalEml);
        }
    }

}
