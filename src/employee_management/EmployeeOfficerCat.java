package employee_management;

public enum EmployeeOfficerCat {
    PARLIAMENT_SECRETARIAT(1),
    MEDICAL(2),
    VIP_OFFICE(3),
    FORCES_SAA(4),
    MP_OFFICE(5),
    PWD(6),
    AUDIT(7),
    PORJOTON(8),
    JOYEETA(9),
    MEMBERS_CLUB(10),
    OUTSOURCING(11),
    BANK(12),
    VENDOR(13),
    POST_OFFICE(14),
    OTHERS(99);

    private final int value;

    EmployeeOfficerCat(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
