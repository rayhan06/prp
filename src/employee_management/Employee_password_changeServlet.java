package employee_management;

import common.ApiResponse;
import employee_records.Employee_recordsDAO;
import employee_records.Employee_recordsDTO;
import login.LoginDTO;
import mail.EmailService;
import mail.SendEmailDTO;
import org.apache.log4j.Logger;
import pb.Utils;
import permission.MenuConstants;
import role.PermissionRepository;
import sessionmanager.SessionConstants;
import sms.SmsService;
import sms_log.Sms_logDTO;
import user.UserDTO;
import user.UserRepository;
import util.HttpRequestUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Calendar;

@WebServlet("/Employee_password_changeServlet")
@MultipartConfig
public class Employee_password_changeServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Employee_password_changeServlet.class);
    private static final String smsBodyForPasswordUpdate = "Dear %s,\n" + "Your user name is : %s\n" + "Updated Password is : %s";

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }
        try {
            String actionType = request.getParameter("actionType");
            if ("getAddPage".equals(actionType)) {
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID,
                        MenuConstants.CHANGE_PASSWORD)) {
                    request.getRequestDispatcher("employee_management/password_change.jsp").forward(request, response);
                    return;
                }
            }
        } catch (Exception ex) {
            logger.error(ex);
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }


    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        boolean isLangEng = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }
        try {
            String actionType = request.getParameter("actionType");
            switch (actionType) {
                case "ajax_changePassword":
                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID,
                            MenuConstants.CHANGE_PASSWORD)) {
                        logger.debug("going to  Change Password ");
                        String employeeRecordId = request.getParameter("employeeRecordId");
                        if (employeeRecordId == null)
                            throw new Exception(isLangEng ? "No User is Selected" : "কোন ইউজার বাছাই হয়নি");
                        Employee_recordsDTO employeeRecordsDTO = new Employee_recordsDAO().getEmployee_recordsDTOByID(Long.parseLong(employeeRecordId));
                        String password = request.getParameter("password");
                        String confirmPassword = request.getParameter("confirmPassword");
                        checkPassword(password, confirmPassword, isLangEng);
                        employeeRecordsDTO.password = password;
                        new Employee_recordsDAO().updateEmployee_records(employeeRecordsDTO, null);
                        ApiResponse.sendSuccessResponse(response, "Employee_managementServlet?actionType=search");
                        sendNotificationForUpdate(employeeRecordsDTO);
                        return;
                    }
                    break;
            }
        } catch (Exception ex) {
            logger.error(ex);
            ApiResponse.sendErrorResponse(response, ex.getMessage());
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }

    private void checkPassword(String password, String confirmPassword, boolean isLangEng) throws Exception {
        if (password == null || password.trim().length() == 0) {
            throw new Exception(isLangEng ? "Please enter password" : "পাসওয়ার্ড দিন");
        }
        if (confirmPassword == null || confirmPassword.trim().length() == 0) {
            throw new Exception(isLangEng ? "Please enter confirm password" : "পাসওয়ার্ড নিশ্চিত দিন");
        }
        if (!password.equals(confirmPassword)) {
            throw new Exception(isLangEng ? "Confirm password doesn't matched with password" : "নিশ্চিত পাসওয়ার্ড  পাসওয়ার্ডের সাথে মিলে নাই");
        }
    }

    private void sendNotificationForUpdate(Employee_recordsDTO employee_recordsDTO) {
        Utils.runIOTaskInASeparateThread(() -> () -> {
            Sms_logDTO smsLogDTO = new Sms_logDTO();
            smsLogDTO.insertionTime = Calendar.getInstance().getTimeInMillis();
            smsLogDTO.receiver = employee_recordsDTO.personalMobile;
            String mailSubject;
            smsLogDTO.message = String.format(smsBodyForPasswordUpdate, employee_recordsDTO.nameEng, employee_recordsDTO.employeeNumber, employee_recordsDTO.password);
            mailSubject = "Password is updated";
            try {
                SmsService.send(smsLogDTO.receiver, smsLogDTO.message);
            } catch (IOException e) {
                e.printStackTrace();
            }
            sendMail(employee_recordsDTO.personalEml, smsLogDTO.message, mailSubject);
        });
    }

    private void sendMail(String mailId, String message, String subject) {
        if (mailId != null && mailId.trim().length() > 0) {
            SendEmailDTO sendEmailDTO = new SendEmailDTO();
            sendEmailDTO.setTo(new String[]{mailId});
            sendEmailDTO.setText(message);
            sendEmailDTO.setSubject(subject);
            sendEmailDTO.setFrom("edms@pbrlp.gov.bd");
            try {
                new EmailService().sendMail(sendEmailDTO);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else {
            logger.debug("<<<<mail id is null or empty, mailId : " + mailId);
        }
    }

}
