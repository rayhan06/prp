package employee_management;

import annotation.CategoryValidation;
import sessionmanager.SessionConstants;


public class Employee_managementDTO extends EmployeeMPModel {
    @CategoryValidation(domainName = "emp_officer", engError = "Please select officer type", bngError = "অনুগ্রহ করে অফিসারের ধরন বাছাই করুন",
            properEngError = "Please select correct officer type", properBngError = "অনুগ্রহ করে সঠিক অফিসারের ধরন বাছাই করুন")
    public int empOfficerCat = -1;

    @CategoryValidation(domainName = "employee_class", engError = "Please select employee class type", bngError = "অনুগ্রহ করে কর্মকর্তার শ্রেনী বাছাই করুন",
            properEngError = "Please select correct employee class type", properBngError = "অনুগ্রহ করে সঠিক কর্মকর্তার শ্রেনী বাছাই করুন")
    public int employeeClassCat = -1;

    @CategoryValidation(domainName = "employment", engError = "Please select employment type", bngError = "অনুগ্রহ করে কর্মকর্তার ধরন বাছাই করুন",
            properEngError = "Please select correct employment type", properBngError = "অনুগ্রহ করে সঠিক কর্মকর্তার ধরন বাছাই করুন")
    public int employmentCat = -1;
    @CategoryValidation(domainName = "gender", engError = "Please select gender", bngError = "অনুগ্রহ করে লিঙ্গ বাছাই করুন",
            properEngError = "Please select correct gender", properBngError = "অনুগ্রহ করে সঠিক লিঙ্গ বাছাই করুন")
    public int genderCat = 0;
    public int gradeCat = 0;
    public long provisionPeriod = 0;
    public long provisionEndDate = SessionConstants.MIN_DATE;
    public long lprDate = SessionConstants.MIN_DATE;
    public long retirementDate = SessionConstants.MIN_DATE;
    public int jobQuota = -1;

    public long currentOffice = -1;
    public long otherOfficeDeptType = -1;
    public String currentDesignation = "";
    public int bloodGroup = 0;
    public int homeDistrict = 0;
    public byte[] photo = null;
    public String deactivationReason = "";


    public String alternativeMobileNo = "";

    @Override
    public String toString() {
        return "Employee_managementDTO{" +
                "nameEng='" + nameEng + '\'' +
                ", nameBng='" + nameBng + '\'' +
                ", mobileNumber='" + mobileNumber + '\'' +
                ", empOfficerCat=" + empOfficerCat +
                ", employeeClassCat=" + employeeClassCat +
                ", employmentCat=" + employmentCat +
                ", password='" + password + '\'' +
                ", insertionDate=" + insertionDate +
                ", insertedBy=" + insertedBy +
                ", modifiedBy=" + modifiedBy +
                ", userName='" + userName + '\'' +
                ", personalEml='" + personalEml + '\'' +
                ", joiningDate=" + joiningDate +
                ", dob=" + dob +
                ", provisionPeriod=" + provisionPeriod +
                ", provisionEndDate=" + provisionEndDate +
                '}';
    }
}