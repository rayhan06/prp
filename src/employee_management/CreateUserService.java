package employee_management;

/*
 * @author Md. Erfan Hossain
 * @created 21/08/2021 - 4:38 PM
 * @project parliament
 */

import common.ConnectionAndStatementUtil;
import common.VbSequencerService;
import dbm.DBMW;
import org.apache.log4j.Logger;
import util.PasswordUtil;

import java.sql.Connection;
import java.util.concurrent.atomic.AtomicReference;

@SuppressWarnings("Duplicates")
public interface CreateUserService extends VbSequencerService {
    Logger loggerCreateUserService = Logger.getLogger(CreateUserService.class);

    String addUserQuery = " INSERT INTO users (id, username, password, user_alias, employee_record_id, user_role_id, user_status, roleID, "
            .concat("languageID, userType, fullName, phoneNo,mailAddress, lastModificationTime, isDeleted) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");

    default void createUser(EmployeeMPModel dto, Connection connection) throws Exception{
        AtomicReference<Exception> exception = new AtomicReference<>();
        ConnectionAndStatementUtil.getWritePrepareStatement(ps -> {
            int k = 0;
            try {
                dto.userId = DBMW.getInstance().getNextSequenceId("users");
                ps.setLong(++k, dto.userId);//id
                ps.setString(++k, dto.userName);
                ps.setString(++k, PasswordUtil.getInstance().encrypt(dto.password));
                ps.setString(++k, dto.userName); //user_alias
                ps.setLong(++k, dto.iD);//employee_record_id
                ps.setInt(++k, 6);//user_role_id
                ps.setInt(++k, 1);//user_status
                ps.setInt(++k, 9601);//roleID
                ps.setInt(++k, 2);//languageID
                ps.setInt(++k, 2);//userType
                ps.setString(++k, dto.nameEng);
                ps.setString(++k, dto.mobileNumber);
                ps.setString(++k, dto.personalEml);
                ps.setLong(++k, dto.lastModificationTime);
                ps.setInt(++k, 0);//isDeleted
                loggerCreateUserService.debug(ps);
                ps.execute();
                recordUpdateTime(connection,"users",dto.lastModificationTime);
            } catch (Exception ex) {
                ex.printStackTrace();
                exception.set(ex);
            }
        }, connection, addUserQuery);
        if(exception.get()!=null){
            throw exception.get();
        }
    }
}