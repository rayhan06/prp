package employee_management;

/*
 * @author Md. Erfan Hossain
 * @created 21/08/2021 - 2:13 PM
 * @project parliament
 */

import annotation.DateValidation;
import annotation.EmailValidation;
import annotation.NotEmpty;
import annotation.ValidMobileNumber;
import election_constituency.Election_constituencyDTO;
import election_constituency.Election_constituencyRepository;
import election_details.Election_detailsDTO;
import election_details.Election_detailsRepository;
import election_wise_mp.Election_wise_mpDAO;
import sessionmanager.SessionConstants;
import user.UserDAO;
import util.CommonDTO;

@SuppressWarnings("unused")
public class EmployeeMPModel extends CommonDTO {
    @NotEmpty(engError = "Please enter name in english", bngError = "অনুগ্রহ করে ইংরেজিতে নাম লিখুন")
    public String nameEng = "";

    @NotEmpty(engError = "Please enter name in bangla", bngError = "অনুগ্রহ করে বাংলাতে নাম লিখুন")
    public String nameBng = "";

    @ValidMobileNumber(engError = "Please enter mobile number", bngError = "অনুগ্রহ করে মোবাইল নাম্বার লিখুন")
    public String mobileNumber = "";
    public String password = "";
    public long insertionDate = -1;
    public long insertedBy = -1;
    public long modifiedBy = -1;
    public String userName = "";
    @EmailValidation(isMandatory = false)
    public String personalEml = "";

    @DateValidation(engError = "Please enter joining date", bngError = "অনুগ্রহ করে যোগদান তারিখ দিন")
    public long joiningDate = SessionConstants.MIN_DATE;

    @DateValidation(engError = "Please enter date of birth", bngError = "অনুগ্রহ করে জন্ম তারিখ দিন", name = "dateOfBirth")
    public long dob = SessionConstants.MIN_DATE;

    @NotEmpty(engError = "Please enter nid number", bngError = "অনুগ্রহ করে জাতীয় পরিচয়পত্র নাম্বার লিখুন")
    public String nid;

    public boolean status = true;
    public int mpStatus = 0;
    public long userId = 0;



    public static String iDGenerationOfSecretary(int employmentType, int officerType, int employeeClassType) {
        return iDGenerationOfSecretary(generatePrefixOfSecretary(employmentType, officerType, employeeClassType));
    }

    public static String iDGenerationOfSecretary(String prefix) {
        String userName = UserDAO.getMaxUserNameByPrefix(prefix);
        int nextNumber = 1;
        if (userName != null) {
            userName = userName.substring(prefix.length());
            nextNumber = Integer.parseInt(userName) + 1;
        }
        return prefix.concat(String.format("%05d", nextNumber));
    }

    public static String generatePrefixOfSecretary(int employmentType, int officerType, int employeeClassType) {
        return String.valueOf(employeeClassType)
                .concat(String.valueOf(employmentType))
                .concat(String.format("%02d",officerType));
    }

    public static String generateUserNamePrefixForMP(long parliamentNumberId, long electionConstituencyId) {
        StringBuilder prefixBuilder = new StringBuilder("0");
        Election_detailsDTO electionDetailsDto = Election_detailsRepository.getInstance().getElectionDetailsDTOByID(parliamentNumberId);
        prefixBuilder.append(String.format("%02d", electionDetailsDto.parliamentNumber));
        Election_constituencyDTO electionConstituencyDto = Election_constituencyRepository.getInstance().getElection_constituencyDTOByID(electionConstituencyId);
        prefixBuilder.append(String.format("%04d", electionConstituencyDto.constituencyNumber));
        return prefixBuilder.toString();
    }

    public static String iDGenerationOfMP(long parliamentNumberId, long electionConstituencyId) {
        return iDGenerationOfMP(generateUserNamePrefixForMP(parliamentNumberId, electionConstituencyId));
    }

    public static String iDGenerationOfMP(String prefix) {
        String maxUsername = Election_wise_mpDAO.getMaxUserNameByPrefix(prefix);
        int nextNumber = 1;
        if (maxUsername != null) {
            nextNumber = Integer.parseInt(maxUsername.substring(prefix.length())) + 1;
        }
        return prefix.concat(String.format("%02d",nextNumber));
    }
}