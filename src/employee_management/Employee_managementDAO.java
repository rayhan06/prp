package employee_management;

import common.CommonDAOService;
import common.ConnectionAndStatementUtil;
import dbm.DBMW;
import org.apache.log4j.Logger;
import pb.CatRepository;
import pb.CategoryLanguageModel;
import pb.Utils;
import util.CommonDTO;
import util.RecordNavigator;
import util.StringUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

@SuppressWarnings("Duplicates")
public class Employee_managementDAO implements CommonDAOService<Employee_managementDTO>, CreateUserService {

    private static final Logger logger = Logger.getLogger(Employee_managementDAO.class);

    private static final String addQuery = "INSERT INTO employee_records (id,name_eng,name_bng,personal_mobile,emp_officer_cat," +
            "employee_class_cat,employment_cat,created_by,modified_by,created,lastModificationTime,isDeleted,personal_email,job_quota," +
            "joining_date,employee_number,is_mp,date_of_birth,provision_period,provision_end_date,search_column,lpr_date,retirement_date,nid,current_office,"
            + "other_office_department_type,current_designation,blood_group,home_district,gradeCat,gender_cat) " +
            "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

    private static final String getCountByNameAndDOB = "SELECT COUNT(*) FROM employee_records WHERE name_eng = '%s' AND date_of_birth = %d AND isDeleted = 0 AND is_mp = 2";

    private Employee_managementDAO() {
        searchMap.put("name_eng", " AND (name_eng like ?)");
        searchMap.put("name_bng", " AND (name_bng like ?)");
        searchMap.put("mobile_number", " AND ((personal_mobile = ?) OR (alternative_mobile = ?))");
        searchMap.put("emp_officer_cat", " AND (emp_officer_cat = ?)");
        searchMap.put("employee_class_cat", " AND (employee_class_cat = ?)");
        searchMap.put("employment_cat", " AND (employment_cat = ?)");
        searchMap.put("personal_email", " AND (personal_email = ?)");
        searchMap.put("userName", " AND (employee_number = ?)");
        searchMap.put("AnyField", " AND (search_column like ?)");
        searchMap.put("isMp", " AND (is_mp = ? )");
        searchMap.put("status", " AND (status = ? )");
        searchMap.put("name_eng1", " AND (employee_records.name_eng like ?)");
        searchMap.put("name_bng1", " AND (employee_records.name_bng like ?)");
        searchMap.put("mobile_number1", " AND (employee_records.personal_mobile = ?)");
        searchMap.put("userName1", " AND (employee_records.employee_number = ?)");
        searchMap.put("joiningDateStart", " AND (employee_records.joining_date >= ?)");
        searchMap.put("joiningDateEnd", " AND (employee_records.joining_date <= ?)");
        searchMap.put("AnyField1", " AND (employee_records.search_column like ?)");
    }

    private final Map<String, String> searchMap = new HashMap<>();

    private static class LazyLoader {
        static final Employee_managementDAO INSTANCE = new Employee_managementDAO();
    }

    public static Employee_managementDAO getInstance() {
        return LazyLoader.INSTANCE;
    }

    public void setSearchColumn(Employee_managementDTO employee_managementDTO) {
        employee_managementDTO.searchColumn = employee_managementDTO.nameEng + " ";
        employee_managementDTO.searchColumn += employee_managementDTO.nameBng + " ";
        employee_managementDTO.searchColumn += employee_managementDTO.mobileNumber + " " + StringUtils.convertToBanNumber(employee_managementDTO.mobileNumber) + " ";
        employee_managementDTO.searchColumn += employee_managementDTO.userName + " " + StringUtils.convertToBanNumber(employee_managementDTO.userName) + " ";
        CategoryLanguageModel model = CatRepository.getInstance().getCategoryLanguageModel("emp_officer", employee_managementDTO.empOfficerCat);
        if (model != null) {
            employee_managementDTO.searchColumn += model.englishText + " " + model.banglaText;
        }
        model = CatRepository.getInstance().getCategoryLanguageModel("employee_class", employee_managementDTO.employeeClassCat);
        if (model != null) {
            employee_managementDTO.searchColumn += model.englishText + " " + model.banglaText;
        }
        model = CatRepository.getInstance().getCategoryLanguageModel("employment", employee_managementDTO.employmentCat);
        if (model != null) {
            employee_managementDTO.searchColumn += model.englishText + " " + model.banglaText;
        }
    }

    @Override
    public void set(PreparedStatement ps, Employee_managementDTO employee_managementDTO, boolean isInsert) throws SQLException {
        int index = 0;
        setSearchColumn(employee_managementDTO);
        ps.setObject(++index, employee_managementDTO.iD);
        ps.setObject(++index, employee_managementDTO.nameEng);
        ps.setObject(++index, employee_managementDTO.nameBng);
        ps.setObject(++index, employee_managementDTO.mobileNumber);
        ps.setObject(++index, employee_managementDTO.empOfficerCat);
        ps.setObject(++index, employee_managementDTO.employeeClassCat);
        ps.setObject(++index, employee_managementDTO.employmentCat);
        ps.setObject(++index, employee_managementDTO.insertedBy);
        ps.setObject(++index, employee_managementDTO.modifiedBy);
        ps.setObject(++index, employee_managementDTO.insertionDate);
        ps.setObject(++index, System.currentTimeMillis());
        ps.setObject(++index, employee_managementDTO.isDeleted);
        ps.setObject(++index, employee_managementDTO.personalEml);
        ps.setObject(++index, employee_managementDTO.jobQuota);
        ps.setObject(++index, employee_managementDTO.joiningDate);
        ps.setObject(++index, employee_managementDTO.userName);
        ps.setObject(++index, 2);
        ps.setObject(++index, employee_managementDTO.dob);
        ps.setObject(++index, employee_managementDTO.provisionPeriod);
        ps.setObject(++index, employee_managementDTO.provisionEndDate);
        ps.setObject(++index, employee_managementDTO.searchColumn);
        ps.setObject(++index, employee_managementDTO.lprDate);
        ps.setObject(++index, employee_managementDTO.retirementDate);
        ps.setObject(++index, employee_managementDTO.nid);
        ps.setObject(++index, employee_managementDTO.currentOffice);
        ps.setObject(++index, employee_managementDTO.otherOfficeDeptType);
        ps.setObject(++index, employee_managementDTO.currentDesignation);
        ps.setObject(++index, employee_managementDTO.bloodGroup);
        ps.setObject(++index, employee_managementDTO.homeDistrict);
        ps.setObject(++index, employee_managementDTO.gradeCat);
        ps.setObject(++index, employee_managementDTO.genderCat);
    }

    @Override
    public Employee_managementDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Employee_managementDTO employee_managementDTO = new Employee_managementDTO();
            employee_managementDTO.iD = rs.getLong("id");
            employee_managementDTO.joiningDate = rs.getLong("joining_date");
            employee_managementDTO.nameEng = rs.getString("name_eng");
            employee_managementDTO.nameBng = rs.getString("name_bng");
            employee_managementDTO.mobileNumber = rs.getString("personal_mobile");
            employee_managementDTO.alternativeMobileNo = rs.getString("alternative_mobile");
            employee_managementDTO.userName = rs.getString("employee_number");
            employee_managementDTO.personalEml = rs.getString("personal_email");
            employee_managementDTO.jobQuota = rs.getInt("job_quota");
            employee_managementDTO.empOfficerCat = rs.getInt("emp_officer_cat");
            employee_managementDTO.employeeClassCat = rs.getInt("employee_class_cat");
            employee_managementDTO.employmentCat = rs.getInt("employment_cat");
            employee_managementDTO.otherOfficeDeptType = rs.getLong("other_office_department_type");
            employee_managementDTO.currentOffice = rs.getLong("current_office");
            employee_managementDTO.insertedBy = rs.getLong("created_by");
            employee_managementDTO.modifiedBy = rs.getLong("modified_by");
            employee_managementDTO.insertionDate = rs.getLong("created");
            employee_managementDTO.lastModificationTime = rs.getLong("lastModificationTime");
            employee_managementDTO.isDeleted = rs.getInt("isDeleted");
            employee_managementDTO.status = rs.getBoolean("status");
            employee_managementDTO.userName = rs.getString("employee_number");
            employee_managementDTO.nid = rs.getString("nid");
            employee_managementDTO.deactivationReason = rs.getString("deactivation_reason");
            if(employee_managementDTO.deactivationReason == null)
            {
            	employee_managementDTO.deactivationReason = "";
            }
            employee_managementDTO.photo = Utils.getByteArrayFromInputStream(rs.getBinaryStream("photo"));
            return employee_managementDTO;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    public Employee_managementDTO getDTOByID(long id) {
        return getDTOFromID(id);
    }

    @Override
    public String getTableName() {
        return "employee_records";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    public long add(CommonDTO commonDTO) throws Exception {
        AtomicReference<Exception> exception = new AtomicReference<>();
        ConnectionAndStatementUtil.getWriteConnectionAndPrepareStatement(model -> {
            Employee_managementDTO dto = (Employee_managementDTO) commonDTO;
            Connection connection = model.getConnection();
            try {
                connection.setAutoCommit(false);
                PreparedStatement ps = (PreparedStatement) model.getStatement();
                dto.iD = DBMW.getInstance().getNextSequenceId("employee_records");
                dto.lastModificationTime = System.currentTimeMillis() + 120000; // Add 2mins delay for reloading cache;
                set(ps, dto, true);
                logger.debug(ps);
                ps.execute();
                recordUpdateTime(connection, "employee_records", dto.lastModificationTime);
                createUser(dto, connection);
                connection.commit();
                connection.setAutoCommit(true);
            } catch (Exception ex) {
                loggerCommonDAOService.error("",ex);
                try {
                    connection.rollback();
                    connection.setAutoCommit(true);
                } catch (SQLException e) {
                    loggerCommonDAOService.error("",e);
                }
                exception.set(ex);
            }
        }, addQuery);
        if (exception.get() != null) {
            throw exception.get();
        }
        return commonDTO.iD;
    }

    public long update(CommonDTO commonDTO) throws Exception {
        throw new Exception("Update is not supported");
    }

    @Override
    public String getLastModifierUser() {
        return "modified_by";
    }

    public long getCountByNameAndDOB(String nameEng, long dob) {
        String sql = String.format(getCountByNameAndDOB, nameEng, dob);
        return ConnectionAndStatementUtil.getT(sql, rs -> {
            try {
                return rs.getLong(1);
            } catch (SQLException e) {
                loggerCommonDAOService.error("",e);
                return 0L;
            }
        }, 0L);
    }

    @Override
    public RecordNavigator getRecordNavigator(Map<String, String> params, String tableJoinClause, String whereClause, String sortingClause) {
        if (params.get("emp_photo") != null) {
            if (whereClause == null) {
                whereClause = "";
            }
            if (params.get("emp_photo").equals("1")) {
                whereClause += " AND (photo is not null) ";
            } else if (params.get("emp_photo").equals("0")) {
                whereClause += " AND (photo is null) ";
            }
        }
        return CommonDAOService.super.getRecordNavigator(params, tableJoinClause, whereClause, sortingClause);
    }

    private static final String deleteByIds = "UPDATE employee_records SET isDeleted = 1,lastModificationTime= %d WHERE id in (%s)";

    public void deleteByIds(List<Long> idList, long lastModificationTime) throws Exception {
        if(idList == null || idList.isEmpty()){
            return;
        }
        AtomicReference<Exception> ar = new AtomicReference<>();
        ConnectionAndStatementUtil.getWriteStatement(st -> {
            String ids = idList.stream().map(String::valueOf).collect(Collectors.joining(","));
            String sql = String.format(deleteByIds, lastModificationTime, ids);
            logger.debug(sql);
            try {
                st.execute(sql);
                recordUpdateTime(CommonDAOService.CONNECTION_THREAD_LOCAL.get(),getTableName(),lastModificationTime);
            } catch (SQLException e) {
                loggerCommonDAOService.error("",e);
                ar.set(e);
            }
        }, CommonDAOService.CONNECTION_THREAD_LOCAL.get());
        if (ar.get() != null) {
            throw ar.get();
        }
    }
}