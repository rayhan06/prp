package employee_management;

import common.ApiResponse;
import common.BaseServlet;
import common.CommonDAOService;
import employee_offices.EmployeeOfficeRepository;
import employee_offices.EmployeeOfficesDAO;
import employee_provision.Employee_provisionDAO;
import employee_provision.Employee_provisionDTO;
import employee_records.Employee_recordsRepository;
import employee_vaccination_info.Employee_vaccination_infoDAO;
import employee_vaccination_info.Employee_vaccination_infoDTO;
import geolocation.GeoLocationDTO;
import geolocation.GeoLocationRepository;
import language.LC;
import language.LM;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import pb.CatRepository;
import pb.Utils;
import pbReport.GenerateExcelReportService;
import permission.MenuConstants;
import sessionmanager.SessionConstants;
import user.UserDAO;
import user.UserDTO;
import user.UserRepository;
import util.*;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@SuppressWarnings({"Duplicates", "unchecked"})
@WebServlet("/Employee_managementServlet")
@MultipartConfig
public class Employee_managementServlet extends BaseServlet implements EmployeeMPService, GenerateExcelReportService {
    private static final long serialVersionUID = 1L;
    public static final int EMPLOYEE_JOB_DURATION = 59;
    public static final int FREEDOM_FIGHTER_EMPLOYEE_JOB_DURATION = 61;
    public static final int RETIREMENT_DURATION = 1;
    private final Employee_managementDAO employeeManagementDAO = Employee_managementDAO.getInstance();

    @Override
    public String getTableName() {
        return Employee_managementDAO.getInstance().getTableName();
    }

    @Override
    public String getServletName() {
        return "Employee_managementServlet";
    }

    @Override
    public Employee_managementDAO getCommonDAOService() {
        return employeeManagementDAO;
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[]{MenuConstants.EMPLOYEE_MANAGEMENT_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[]{MenuConstants.EMPLOYEE_MANAGEMENT_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[]{MenuConstants.EMPLOYEE_MANAGEMENT_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Employee_managementServlet.class;
    }

    public String commonPartOfDispatchURL() {
        return "employee_management/employee_management";
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        CommonLoginData commonLoginData = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get();
        try {
            switch (request.getParameter("actionType")) {
                case "search":
                    Map<String, String> map = new HashMap<>();
                    map.put("isMp", "2");
                    request.setAttribute(CommonDAOService.CRITERIA_MAP_KEY, map);
                    boolean toGetExcel = request.getParameter("isXL") != null
                                         && Utils.checkPermission(commonLoginData.userDTO, getSearchMenuConstants())
                                         && getSearchPagePermission(request);
                    if (toGetExcel) {
                        generateXL(request, response, commonLoginData.userDTO);
                        return;
                    }else{
                        super.doGet(request, response);
                    }
                    break;
                case "vacantOfficer":
                    vacantOfficer(request, response);
                    return;
                case "getPayScaleEditPage":
                    request.getRequestDispatcher("employee_management/payScaleEdit.jsp").forward(request, response);
                    return;
                case "getEmployeeSeniorityEditPage":
                    request.getRequestDispatcher("employee_management/employee_seniorityEdit.jsp").forward(request, response);
                    return;
                default:
                    super.doGet(request, response);
                    return;
            }
        } catch (Exception e) {
            logger.error(e);
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if ("delete".equals(request.getParameter("actionType"))) {
            if (Utils.checkPermission(HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO, getEditPageMenuConstants())) {
                String[] ids = request.getParameterValues("ID");
                List<Long> idList = Arrays.stream(ids)
                                          .peek(id -> logger.debug("###DELETING " + id))
                                          .map(Long::valueOf)
                                          .collect(Collectors.toList());
                try {
                    deleteByIds(idList);
                    ApiResponse.sendSuccessResponse(response, "option");
                } catch (Exception ex) {
                    logger.error("",ex);
                    ApiResponse.sendErrorResponse(response, ex.getMessage());
                }
            } else {
                ApiResponse.sendSuccessResponse(response, "permission issue");
            }
        } else {
            super.doPost(request, response);
        }
    }

    private void deleteByIds(List<Long> empIdList) throws Exception {
        if (empIdList == null || empIdList.isEmpty()) {
            return;
        }
        Utils.handleTransaction(() -> {
            long lastModificationTime = System.currentTimeMillis() + 180000; //3mins delay
            employeeManagementDAO.deleteByIds(empIdList, lastModificationTime);
            UserDAO userDAO = new UserDAO();
            List<Long> userIdList = userDAO.getIdsByEmployeeIds(empIdList);
            userDAO.deleteByIds(userIdList, lastModificationTime);
            List<Long> empOfcIdList = EmployeeOfficesDAO.getInstance().getIdsByEmployeeIds(empIdList);
            EmployeeOfficesDAO.getInstance().deleteByIds(empOfcIdList, lastModificationTime, lastModificationTime);
            updateCache(empIdList, userIdList, empOfcIdList);
        });
    }

    private void updateCache(List<Long> empIdList, List<Long> userIdList, List<Long> empOfcIdList) {
        Utils.runIOTaskInASeparateThread(() -> () -> {
            Employee_recordsRepository.getInstance().updateCacheByIds(empIdList);
            UserRepository.getInstance().updateCache(userIdList);
            EmployeeOfficeRepository.getInstance().updateCache(empOfcIdList);
        });
    }

    private void generateXL(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO) throws IOException {

        Map<String, String> params = HttpRequestUtils.buildRequestParams(request);
        if (request.getAttribute(CommonDAOService.CRITERIA_MAP_KEY) != null) {
            Map<String, String> extraCriteriaMap = (Map<String, String>) request.getAttribute(CommonDAOService.CRITERIA_MAP_KEY);
            params.putAll(extraCriteriaMap);
        }
        params.put(CommonDAOService.FETCH_ALL, "true");
        RecordNavigator recordNavigator = getCommonDAOService().getRecordNavigator(params, getTableJoinClause(request), getWhereClause(request), getSortClause(request));
        String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
        List<List<Object>> data = setData(recordNavigator, userDTO);

        String file_name = "employee_report_" + System.currentTimeMillis() + ".xlsx";
        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "attachment; filename=" + file_name);
        XSSFWorkbook wb = createXl(data, null, LM.getText(LC.EMPLOYEE_MANAGEMENT_ADD_EMPLOYEE_MANAGEMENT_ADD_FORMNAME, userDTO), Language, userDTO);
        wb.write(response.getOutputStream());
        wb.close();
    }

    List<List<Object>> setData(RecordNavigator recordNavigator, UserDTO userDTO) {
        String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
        boolean isLanguageEnglish = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
        List<List<Object>> data = new ArrayList<>();

        List<Object> headerList = new ArrayList<>();
        headerList.add(LM.getText(LC.HM_USER_NAME, userDTO));
        headerList.add(LM.getText(LC.EMPLOYEE_RECORDS_ADD_NAMEENG, userDTO));
        headerList.add(LM.getText(LC.EMPLOYEE_RECORDS_ADD_NAMEBNG, userDTO));
        headerList.add(LM.getText(LC.GLOBAL_MOBILE_NUMBER, userDTO));
        headerList.add(LM.getText(LC.EMPLOYEE_OFFICER_TYPE, userDTO));
        headerList.add(LM.getText(LC.EMPLOYEE_EMPLOYEE_CLASS, userDTO));
        headerList.add(LM.getText(LC.EMPLOYMENT_CATEGORY_EMPLOYMENTCATEGORY, userDTO));
        headerList.add(isLanguageEnglish ? "Photo" : "ছবি");
        data.add(headerList);

        for (Employee_managementDTO employee_managementDTO : (List<Employee_managementDTO>) recordNavigator.list) {
            List<Object> list = new ArrayList<>();
            list.add(employee_managementDTO.userName);
            list.add(employee_managementDTO.nameEng);
            list.add(employee_managementDTO.nameBng);
            list.add(employee_managementDTO.mobileNumber == null ? "" :
                     isLanguageEnglish ? employee_managementDTO.mobileNumber
                                       : StringUtils.convertToBanNumber(employee_managementDTO.mobileNumber));
            list.add(CatRepository.getInstance().getText(Language, "emp_officer", employee_managementDTO.empOfficerCat));
            list.add(CatRepository.getInstance().getText(Language, "employee_class", employee_managementDTO.employeeClassCat));
            list.add(CatRepository.getInstance().getText(Language, "employment", employee_managementDTO.employmentCat));
            list.add(employee_managementDTO.photo);
            data.add(list);
        }
        return data;
    }

    @Override
    public synchronized CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        boolean isLangEng = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
        Employee_managementDTO employee_managementDTO = new Employee_managementDTO();
        validateAndSet(employee_managementDTO, request, HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng);

        setEmployeeMPModel(employee_managementDTO, request, addFlag, isLangEng);
        if (employeeManagementDAO.getCountByNameAndDOB(employee_managementDTO.nameEng, employee_managementDTO.dob) > 0) {
            throw new Exception(isLangEng ? String.format("A duplicate employee is found by name %s and date of birth %s", employee_managementDTO.nameEng, request.getParameter("dateOfBirth"))
                                          : String.format("%s নামে কর্মকর্তা পাওয়া গেছে, যার জন্মতারিখও %s", employee_managementDTO.nameBng, StringUtils.convertToBanNumber(request.getParameter("dateOfBirth"))));
        }
        employee_managementDTO.userName = EmployeeMPModel.iDGenerationOfSecretary(employee_managementDTO.employmentCat, employee_managementDTO.empOfficerCat, employee_managementDTO.employeeClassCat);
        String value = Utils.doJsoupCleanOrReturnDefault(request.getParameter("provision_period"), "0");
        int homeDistrict = Utils.parseMandatoryInt(request.getParameter("homeDistrict"), 0, isLangEng ? "Please select home district" : "অনুগ্রহ করে স্থায়ী জেলা বাছাই করুন");
        GeoLocationDTO dto = GeoLocationRepository.getInstance().getById(homeDistrict);
        if (dto == null || dto.geoLevelID != 2) {
            throw new Exception(isLangEng ? "Please select correct home district" : "অনুগ্রহ করে সঠিক স্থায়ী জেলা বাছাই করুন");
        }
        employee_managementDTO.homeDistrict = homeDistrict;
        if (value != null && value.length() > 0) {
            try {
                employee_managementDTO.provisionPeriod = Long.parseLong(value);
            } catch (Exception ex) {
                throw new Exception(isLangEng ? "Provision period should be a number" : "প্রোভিশন সময়কাল অব্যশই সংখ্যা হবে");
            }
            if (employee_managementDTO.provisionPeriod > 0) {
                Calendar c = Calendar.getInstance();
                c.setTime(new Date(employee_managementDTO.joiningDate));
                c.add(Calendar.MONTH, (int) employee_managementDTO.provisionPeriod);
                c.add(Calendar.DATE, -1);
                employee_managementDTO.provisionEndDate = c.getTimeInMillis();
            } else {
                employee_managementDTO.provisionPeriod = 0;
            }
        } else {
            employee_managementDTO.provisionPeriod = 0;
        }

        employee_managementDTO.lprDate = calculateLPRDate(employee_managementDTO.dob, request.getParameter("freedomFighter"));
        employee_managementDTO.retirementDate = calculateRetirementDate(employee_managementDTO.lprDate);
        Employee_vaccination_infoDTO employee_vaccination_infoDTO = new Employee_vaccination_infoDTO();

        value = request.getParameter("isFullyVaccinated");
        if (value != null && !value.equalsIgnoreCase("")) {
            value = Jsoup.clean(value, Whitelist.simpleText());
        }
        employee_vaccination_infoDTO.isFullyVaccinated = value != null && !value.equalsIgnoreCase("");

        if (employee_managementDTO.empOfficerCat == EmployeeOfficerCat.OTHERS.getValue()) {
            value = request.getParameter("currentOfficeCat");
            if (value != null) {
                value = Jsoup.clean(value, Whitelist.simpleText());
                if (value == null || value.isEmpty()) {
                    throw new Exception(isLangEng ? "Current office not found" : "বর্তমান অফিস পাওয়া যায় নি");
                }
                employee_managementDTO.currentOffice = Integer.parseInt(value);
            } else {
                throw new Exception(isLangEng ? "Current office not found" : "বর্তমান অফিস পাওয়া যায় নি");
            }

            value = request.getParameter("otherOfficeDeptType");
            if (value != null) {
                value = Jsoup.clean(value, Whitelist.simpleText());
                if (value == null || value.isEmpty()) {
                    throw new Exception(isLangEng ? "Dept not found" : "পদ পাওয়া যায় নি");
                }
                employee_managementDTO.otherOfficeDeptType = Integer.parseInt(value);
            } else {
                throw new Exception(isLangEng ? "Dept not found" : "বিভাগ পাওয়া যায় নি");
            }

            value = request.getParameter("currentDesignation");
            if (value != null) {
                value = Jsoup.clean(value, Whitelist.simpleText());
                employee_managementDTO.currentDesignation = value;
            }
            value = request.getParameter("bloodGroup");
            if (value != null) {
                value = Jsoup.clean(value, Whitelist.simpleText());
                if (value != null && !value.isEmpty()) {
                    employee_managementDTO.bloodGroup = Integer.parseInt(value);
                }
            }

            value = request.getParameter("grade");
            if (value != null) {
                value = Jsoup.clean(request.getParameter("grade"), Whitelist.simpleText());
                if (value != null && !value.isEmpty()) {
                    employee_managementDTO.gradeCat = Integer.parseInt(value);
                }
            }
        }

        Employee_managementDAO.getInstance().add(employee_managementDTO);
        Utils.runIOTaskInSingleThread(() -> () -> sendSmsAndMailForNewUser(employee_managementDTO));
        CommonLoginData commonLoginData = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get();
        if (employee_managementDTO.provisionPeriod > 0) {
            Utils.runIOTaskInASeparateThread(() -> () -> {
                HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.set(commonLoginData);
                addProvision(employee_managementDTO, commonLoginData.userDTO);
                HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.remove();
            });
        }
        employee_vaccination_infoDTO.employeeRecordsId = employee_managementDTO.iD;
        Employee_vaccination_infoDAO.getInstance().add(employee_vaccination_infoDTO);
        if (employee_managementDTO.empOfficerCat == EmployeeOfficerCat.OTHERS.getValue()) {
            employee_vaccination_infoDTO.employeeRecordsId = employee_managementDTO.iD;
            Employee_vaccination_infoDAO.getInstance().add(employee_vaccination_infoDTO);
        }
        return employee_managementDTO;
    }

    private void addProvision(Employee_managementDTO employeeManagementDTO, UserDTO userDTO) {
        boolean provisionAddFlag = false;
        long curTime = System.currentTimeMillis();
        Employee_provisionDTO employee_provisionDTO;
        employee_provisionDTO = Employee_provisionDAO.getInstance().getDTOByEmployeeId(employeeManagementDTO.iD);
        if (employee_provisionDTO == null) {
            provisionAddFlag = true;
            employee_provisionDTO = new Employee_provisionDTO();
            employee_provisionDTO.insertionTime = curTime;
            employee_provisionDTO.insertedBy = userDTO.ID;
        }
        employee_provisionDTO.modifiedBy = userDTO.ID;
        employee_provisionDTO.lastModificationTime = curTime;
        employee_provisionDTO.employeeRecordsId = employeeManagementDTO.iD;
        employee_provisionDTO.startTime = employeeManagementDTO.joiningDate;
        employee_provisionDTO.endTime = employeeManagementDTO.provisionEndDate;
        employee_provisionDTO.duration = (int) employeeManagementDTO.provisionPeriod;
        try {
            if (provisionAddFlag) {
                Employee_provisionDAO.getInstance().add(employee_provisionDTO);
            } else
                Employee_provisionDAO.getInstance().update(employee_provisionDTO);
        } catch (Exception ex) {
            logger.error("",ex);
        }
    }

    @Override
    public void init(HttpServletRequest request) throws ServletException {
        request.setAttribute("navName", "../employee_management/employee_managementNav.jsp");
        request.setAttribute("formName", "../employee_management/employee_managementSearchForm.jsp");
        request.setAttribute("servletName", "Employee_managementServlet");
    }

    private long calculateLPRDate(long dateOfBirth, String freedomFighterInfo) {
        if (dateOfBirth == SessionConstants.MIN_DATE) {
            return SessionConstants.MIN_DATE;
        }

        int jobYear = EMPLOYEE_JOB_DURATION;
        if (freedomFighterInfo != null && (freedomFighterInfo.contains("1") || freedomFighterInfo.contains("2"))) {
            jobYear = FREEDOM_FIGHTER_EMPLOYEE_JOB_DURATION;
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date(dateOfBirth));
        calendar.add(Calendar.YEAR, jobYear);
        calendar.add(Calendar.DAY_OF_MONTH, -1);
        return calendar.getTime().getTime();
    }

    private long calculateRetirementDate(long lprDate) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date(lprDate));
        calendar.add(Calendar.YEAR, RETIREMENT_DURATION);
        return calendar.getTime().getTime();
    }

    void vacantOfficer(HttpServletRequest request, HttpServletResponse response) throws Exception {
        request.setAttribute("navName", "../employee_management/no_designation_employeeNav.jsp");
        request.setAttribute("formName", "../employee_management/no_designation_employeeSearchForm.jsp");
        request.setAttribute("servletName", "Employee_managementServlet");

        Map<String, String> params = HttpRequestUtils.buildRequestParams(request);

        String joinClause = " left join employee_offices eo on eo.employee_record_id = employee_records.id ";
        String whereClause = " AND (eo.id is null or   (select count(*) from employee_offices eo1  where eo1.employee_record_id = employee_records.id  and eo1.incharge_label = 'Routine responsibility' " +
                             "  and eo1.status = 1) = 0 and eo.status = 1 ) AND employee_records.is_mp=2 and employee_records.emp_officer_cat!=3 ";

        RecordNavigator recordNavigator = getCommonDAOService().getRecordNavigator(params, joinClause, whereClause, "");
        recordNavigator.m_tableName = "Unassigned_employee";
        request.setAttribute(RECORD_NAVIGATOR, recordNavigator);
        String ajax = request.getParameter("ajax");
        String url;
        if (ajax != null && ajax.equalsIgnoreCase("true")) {
            url = "employee_management/no_designation_employeeSearchForm.jsp";
        } else {
            url = defaultNonAjaxSearchPageURL();
        }
        logger.debug("url : " + url);
        request.getRequestDispatcher(url).forward(request, response);
    }
}