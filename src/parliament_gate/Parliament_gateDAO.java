package parliament_gate;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;
import sessionmanager.SessionConstants;
import java.sql.SQLException;


import org.apache.log4j.Logger;

import dbm.*;

import repository.RepositoryManager;

import util.*;
import pb.*;
import user.UserDTO;

public class Parliament_gateDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());
	

	
	public Parliament_gateDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";
		commonMaps = new Parliament_gateMAPS(tableName);
		columnNames = new String[] 
		{
			"id",
			"gate_number",
			"gate_pabx_ext_number",
			"name_eng",
			"name_bng",
			"location_eng",
			"location_bng",
				"car_entry",
				"public_entry",
				"only_vip_entry",
				"entry_permission",
			"insertion_date",
			"inserted_by",
			"modified_by",
			"isDeleted",
			"lastModificationTime"
		};
	}
	
	public Parliament_gateDAO()
	{
		this("parliament_gate");		
	}
	
	public void setSearchColumn(Parliament_gateDTO parliament_gateDTO)
	{
		parliament_gateDTO.searchColumn = "";
		parliament_gateDTO.searchColumn += parliament_gateDTO.gateNumber + " ";
		parliament_gateDTO.searchColumn += parliament_gateDTO.gatePabxExtNumber + " ";
		parliament_gateDTO.searchColumn += parliament_gateDTO.nameEng + " ";
		parliament_gateDTO.searchColumn += parliament_gateDTO.nameBng + " ";
		parliament_gateDTO.searchColumn += parliament_gateDTO.locationEng + " ";
		parliament_gateDTO.searchColumn += parliament_gateDTO.locationBng + " ";
		
	}
	
	public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException
	{
		Parliament_gateDTO parliament_gateDTO = (Parliament_gateDTO)commonDTO;
		int index = 1;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(parliament_gateDTO);
		if(isInsert)
		{
			ps.setObject(index++,parliament_gateDTO.iD);
		}
		ps.setObject(index++,parliament_gateDTO.gateNumber);
		ps.setObject(index++,parliament_gateDTO.gatePabxExtNumber);
		ps.setObject(index++,parliament_gateDTO.nameEng);
		ps.setObject(index++,parliament_gateDTO.nameBng);
		ps.setObject(index++,parliament_gateDTO.locationEng);
		ps.setObject(index++,parliament_gateDTO.locationBng);
		ps.setObject(index++,parliament_gateDTO.carEntry);
		ps.setObject(index++,parliament_gateDTO.publicEntry);
		ps.setObject(index++,parliament_gateDTO.onlyVipEntry);
		ps.setObject(index++,parliament_gateDTO.entryPermission);
		ps.setObject(index++,parliament_gateDTO.insertionDate);
		ps.setObject(index++,parliament_gateDTO.insertedBy);
		ps.setObject(index++,parliament_gateDTO.modifiedBy);
		if(isInsert)
		{
			ps.setObject(index++, 0);
			ps.setObject(index++, lastModificationTime);
		}
	}
	
	public void get(Parliament_gateDTO parliament_gateDTO, ResultSet rs) throws SQLException
	{
		parliament_gateDTO.iD = rs.getLong("id");
		parliament_gateDTO.gateNumber = rs.getString("gate_number");
		parliament_gateDTO.gatePabxExtNumber = rs.getString("gate_pabx_ext_number");
		parliament_gateDTO.nameEng = rs.getString("name_eng");
		parliament_gateDTO.nameBng = rs.getString("name_bng");
		parliament_gateDTO.locationEng = rs.getString("location_eng");
		parliament_gateDTO.locationBng = rs.getString("location_bng");
		parliament_gateDTO.carEntry = rs.getInt("car_entry");
		parliament_gateDTO.publicEntry = rs.getInt("public_entry");
		parliament_gateDTO.onlyVipEntry = rs.getInt("only_vip_entry");
		parliament_gateDTO.entryPermission = rs.getInt("entry_permission");
		parliament_gateDTO.insertionDate = rs.getLong("insertion_date");
		parliament_gateDTO.insertedBy = rs.getString("inserted_by");
		parliament_gateDTO.modifiedBy = rs.getString("modified_by");
		parliament_gateDTO.isDeleted = rs.getInt("isDeleted");
		parliament_gateDTO.lastModificationTime = rs.getLong("lastModificationTime");
	}

	//need another getter for repository
	public Parliament_gateDTO getDTOByID (long ID)
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Parliament_gateDTO parliament_gateDTO = null;
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
			
            sql += " WHERE id=" + ID;
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			if(rs.next()){
				parliament_gateDTO = new Parliament_gateDTO();

				get(parliament_gateDTO, rs);

			}			
			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return parliament_gateDTO;
	}
	
	
	
	
	public List<Parliament_gateDTO> getDTOs(Collection recordIDs){
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Parliament_gateDTO parliament_gateDTO = null;
		List<Parliament_gateDTO> parliament_gateDTOList = new ArrayList<>();
		if(recordIDs.isEmpty()){
			return parliament_gateDTOList;
		}
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
            
            sql += " WHERE id IN ( ";

			for(int i = 0;i<recordIDs.size();i++){
				if(i!=0){
					sql+=",";
				}
				sql+=((ArrayList)recordIDs).get(i);
			}
			sql+=")  order by lastModificationTime desc";
			
			printSql(sql);
			
			logger.debug("sql " + sql);
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				parliament_gateDTO = new Parliament_gateDTO();
				get(parliament_gateDTO, rs);
				System.out.println("got this DTO: " + parliament_gateDTO);
				
				parliament_gateDTOList.add(parliament_gateDTO);

			}			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return parliament_gateDTOList;
	
	}
	
	

	
	
	
	//add repository
	public List<Parliament_gateDTO> getAllParliament_gate (boolean isFirstReload)
    {
		List<Parliament_gateDTO> parliament_gateDTOList = new ArrayList<>();

		String sql = "SELECT * FROM parliament_gate";
		sql += " WHERE ";
	

		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by parliament_gate.lastModificationTime desc";
		printSql(sql);
		
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;

		
		try{
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();
			rs = stmt.executeQuery(sql);
			

			while(rs.next()){
				Parliament_gateDTO parliament_gateDTO = new Parliament_gateDTO();
				get(parliament_gateDTO, rs);
				
				parliament_gateDTOList.add(parliament_gateDTO);
			}			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}

		return parliament_gateDTOList;
    }

	
	public List<Parliament_gateDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO)
	{
		return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
	}

	
	public List<Parliament_gateDTO> getDTOs(Hashtable  p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
			String filter, boolean tableHasJobCat)
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		List<Parliament_gateDTO> parliament_gateDTOList = new ArrayList<>();

		try{
			
			String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat);
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				Parliament_gateDTO parliament_gateDTO = new Parliament_gateDTO();
				get(parliament_gateDTO, rs);
				
				parliament_gateDTOList.add(parliament_gateDTO);
			}					
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return parliament_gateDTOList;
	
	}
	public String getSqlWithSearchCriteriaForNormalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
			UserDTO userDTO, String filter, boolean tableHasJobCat)
    {
		boolean viewAll = false;
		
		if(p_searchCriteria != null && p_searchCriteria.get("ViewAll") != null)
		{
			System.out.println("ViewAll = " + p_searchCriteria.get("ViewAll"));
			viewAll = true;
		}
		else
		{
			System.out.println("ViewAll is null ");
		}
		
		String sql = "SELECT ";
		if(category == GETIDS)
		{
			sql += " " + tableName + ".ID as ID ";
		}
		else if(category == GETCOUNT)
		{
			sql += " count( " + tableName + ".ID) as countID ";
		}
		else if(category == GETDTOS)
		{
			sql += " distinct " + tableName + ".* ";
		}
		sql += "FROM " + tableName + " ";
		sql += joinSQL;
		

		
		String AnyfieldSql = "";
		String AllFieldSql = "";
		
		if(p_searchCriteria != null)
		{
						
			
			Enumeration names = p_searchCriteria.keys();
			String str, value;
			
			AnyfieldSql = "(";
			
			if(p_searchCriteria.get("AnyField")!= null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase(""))
			{
				AnyfieldSql+= tableName + ".search_column like '%" + p_searchCriteria.get("AnyField").toString() + "%'";			
			}
			AnyfieldSql += ")";
			System.out.println("AnyfieldSql = " + AnyfieldSql);
			
			AllFieldSql = "(";
			int i = 0;
			while(names.hasMoreElements())
			{				
				str = (String) names.nextElement();
				value = (String)p_searchCriteria.get(str);
		        System.out.println(str + ": " + value);
				if(value != null && !value.equalsIgnoreCase("") && (
						 str.equals("gate_number")
						|| str.equals("gate_pabx_ext_number")
						|| str.equals("name_eng")
						|| str.equals("name_bng")
						|| str.equals("location_eng")
						|| str.equals("location_bng")
				))
				{
					if(p_searchCriteria.get(str).equals("any"))
					{
						continue;
					}

					if( i > 0)
					{
						AllFieldSql+= " AND  ";
					}
					
					 if(str.equals("gate_number"))
					{
						AllFieldSql += "" + tableName + ".gate_number like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("gate_pabx_ext_number"))
					{
						AllFieldSql += "" + tableName + ".gate_pabx_ext_number like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("name_eng"))
					{
						AllFieldSql += "" + tableName + ".name_eng like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("name_bng"))
					{
						AllFieldSql += "" + tableName + ".name_bng like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("location_eng"))
					{
						AllFieldSql += "" + tableName + ".location_eng like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("location_bng"))
					{
						AllFieldSql += "" + tableName + ".location_bng like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("description"))
					{
						AllFieldSql += "" + tableName + ".description like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("insertion_date_start"))
					{
						AllFieldSql += "" + tableName + ".insertion_date >= " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("insertion_date_end"))
					{
						AllFieldSql += "" + tableName + ".insertion_date <= " + p_searchCriteria.get(str);
						i ++;
					}
					
					
				}
			}
			
			AllFieldSql += ")";
			System.out.println("AllFieldSql = " + AllFieldSql);
			
			
		}
		
		
		sql += " WHERE ";
		
		sql += " (" + tableName + ".isDeleted = 0 ";			
		sql += ")";
		
		
		if(!filter.equalsIgnoreCase(""))
		{
			sql += " and " + filter + " ";
		}
		
		if(!AnyfieldSql.equals("()") && !AnyfieldSql.equals(""))
		{
			sql += " AND " + AnyfieldSql;
			
		}
		if(!AllFieldSql.equals("()") && !AllFieldSql.equals(""))
		{			
			sql += " AND " + AllFieldSql;
		}
		
	
			
		sql += " order by " + tableName + ".lastModificationTime desc ";

		printSql(sql);
		
		if(limit >= 0)
		{
			sql += " limit " + limit;
		}
		if(offset >= 0)
		{
			sql += " offset " + offset;
		}
		
		System.out.println("-------------- sql = " + sql);
		
		return sql;
    }
	
	public String getSqlWithSearchCriteria(Hashtable p_searchCriteria, int limit, int offset, int category,
			boolean isPermanentTable, UserDTO userDTO, String filter, boolean tableHasJobCat)
    {		
		return getSqlWithSearchCriteriaForNormalSearch(p_searchCriteria, limit, offset, category, userDTO, filter, tableHasJobCat);	
    }
				
}
	