package parliament_gate;
import java.util.*; 
import util.*;


public class Parliament_gateMAPS extends CommonMaps
{	
	public Parliament_gateMAPS(String tableName)
	{
		

		java_DTO_map.put("id".toLowerCase(), "id".toLowerCase());
		java_DTO_map.put("gateNumber".toLowerCase(), "gateNumber".toLowerCase());
		java_DTO_map.put("gatePabxExtNumber".toLowerCase(), "gatePabxExtNumber".toLowerCase());
		java_DTO_map.put("nameEng".toLowerCase(), "nameEng".toLowerCase());
		java_DTO_map.put("nameBng".toLowerCase(), "nameBng".toLowerCase());
		java_DTO_map.put("locationEng".toLowerCase(), "locationEng".toLowerCase());
		java_DTO_map.put("locationBng".toLowerCase(), "locationBng".toLowerCase());
		java_DTO_map.put("description".toLowerCase(), "description".toLowerCase());
		java_DTO_map.put("imageDropzone".toLowerCase(), "imageDropzone".toLowerCase());
		java_DTO_map.put("insertionDate".toLowerCase(), "insertionDate".toLowerCase());
		java_DTO_map.put("insertedBy".toLowerCase(), "insertedBy".toLowerCase());
		java_DTO_map.put("modifiedBy".toLowerCase(), "modifiedBy".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("gate_number".toLowerCase(), "gateNumber".toLowerCase());
		java_SQL_map.put("gate_pabx_ext_number".toLowerCase(), "gatePabxExtNumber".toLowerCase());
		java_SQL_map.put("name_eng".toLowerCase(), "nameEng".toLowerCase());
		java_SQL_map.put("name_bng".toLowerCase(), "nameBng".toLowerCase());
		java_SQL_map.put("location_eng".toLowerCase(), "locationEng".toLowerCase());
		java_SQL_map.put("location_bng".toLowerCase(), "locationBng".toLowerCase());
		java_SQL_map.put("description".toLowerCase(), "description".toLowerCase());

		java_Text_map.put("Id".toLowerCase(), "id".toLowerCase());
		java_Text_map.put("Gate Number".toLowerCase(), "gateNumber".toLowerCase());
		java_Text_map.put("Gate Pabx Ext Number".toLowerCase(), "gatePabxExtNumber".toLowerCase());
		java_Text_map.put("Name Eng".toLowerCase(), "nameEng".toLowerCase());
		java_Text_map.put("Name Bng".toLowerCase(), "nameBng".toLowerCase());
		java_Text_map.put("Location Eng".toLowerCase(), "locationEng".toLowerCase());
		java_Text_map.put("Location Bng".toLowerCase(), "locationBng".toLowerCase());
		java_Text_map.put("Description".toLowerCase(), "description".toLowerCase());
		java_Text_map.put("Image Dropzone".toLowerCase(), "imageDropzone".toLowerCase());
		java_Text_map.put("Insertion Date".toLowerCase(), "insertionDate".toLowerCase());
		java_Text_map.put("Inserted By".toLowerCase(), "insertedBy".toLowerCase());
		java_Text_map.put("Modified By".toLowerCase(), "modifiedBy".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}