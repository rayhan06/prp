package parliament_gate;
import java.util.*; 
import util.*; 


public class Parliament_gateDTO extends CommonDTO
{

    public String gateNumber = "";
    public String gatePabxExtNumber = "";
    public String nameEng = "";
    public String nameBng = "";
    public String locationEng = "";
    public String locationBng = "";
    public int carEntry = 0;
    public int publicEntry = 0;
    public int onlyVipEntry = 0;
    public int entryPermission = 0;
	public long insertionDate = -1;
    public String insertedBy = "";
    public String modifiedBy = "";
	
	
    @Override
	public String toString() {
            return "$Parliament_gateDTO[" +
            " id = " + iD +
            " gateNumber = " + gateNumber +
            " gatePabxExtNumber = " + gatePabxExtNumber +
            " nameEng = " + nameEng +
            " nameBng = " + nameBng +
            " locationEng = " + locationEng +
            " locationBng = " + locationBng +
            " insertionDate = " + insertionDate +
            " insertedBy = " + insertedBy +
            " modifiedBy = " + modifiedBy +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}