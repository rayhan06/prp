package parliament_gate;

import java.io.IOException;
import java.io.*;
import java.text.SimpleDateFormat;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import org.apache.log4j.Logger;

import login.LoginDTO;
import permission.MenuConstants;
import role.PermissionRepository;


import sessionmanager.SessionConstants;

import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;
import util.CommonRequestHandler;
import util.RecordNavigationManager4;

import java.util.*;
import javax.servlet.http.*;
import java.util.UUID;





import geolocation.GeoLocationDAO2;
import java.util.StringTokenizer;

import com.google.gson.Gson;

import pb.*;
import pbReport.*;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;

import files.*;



/**
 * Servlet implementation class Parliament_gateServlet
 */
@WebServlet("/Parliament_gateServlet")
@MultipartConfig
public class Parliament_gateServlet extends HttpServlet
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Parliament_gateServlet.class);

    String tableName = "parliament_gate";

	Parliament_gateDAO parliament_gateDAO;
	CommonRequestHandler commonRequestHandler;
	FilesDAO filesDAO = new FilesDAO();
    private final Gson gson = new Gson();

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Parliament_gateServlet()
	{
        super();
    	try
    	{
			parliament_gateDAO = new Parliament_gateDAO(tableName);
			commonRequestHandler = new CommonRequestHandler(parliament_gateDAO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		System.out.println("In doget request = " + request);
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		try
		{
			String actionType = request.getParameter("actionType");
			if(actionType.equals("getAddPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PARLIAMENT_GATE_ADD))
				{
					commonRequestHandler.getAddPage(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getEditPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PARLIAMENT_GATE_UPDATE))
				{
					getParliament_gate(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("downloadDropzoneFile"))
			{
				commonRequestHandler.downloadDropzoneFile(request, response, filesDAO);
			}
			else if(actionType.equals("DeleteFileFromDropZone"))
			{
				commonRequestHandler.deleteFileFromDropZone(request, response, filesDAO);
			}
			else if(actionType.equals("getURL"))
			{
				String URL = request.getParameter("URL");
				System.out.println("URL = " + URL);
				response.sendRedirect(URL);
			}
			else if(actionType.equals("search"))
			{
				System.out.println("search requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PARLIAMENT_GATE_SEARCH))
				{
					if(isPermanentTable)
					{
						String filter = request.getParameter("filter");
						System.out.println("filter = " + filter);
						if(filter!=null)
						{
							filter = ""; //shouldn't be directly used, rather manipulate it.
							searchParliament_gate(request, response, isPermanentTable, filter);
						}
						else
						{
							searchParliament_gate(request, response, isPermanentTable, "");
						}
					}
					else
					{
						//searchParliament_gate(request, response, tempTableName, isPermanentTable);
					}
				}
			}
			else if(actionType.equals("view"))
			{
				System.out.println("view requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PARLIAMENT_GATE_SEARCH))
				{
					commonRequestHandler.view(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}
			else
			{
				request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}





	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		// TODO Auto-generated method stub
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		System.out.println("doPost");
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);

		try
		{
			String actionType = request.getParameter("actionType");
			System.out.println("actionType = " + actionType);
			if(actionType.equals("add"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PARLIAMENT_GATE_ADD))
				{
					System.out.println("going to  addParliament_gate ");
					addParliament_gate(request, response, true, userDTO, true);
				}
				else
				{
					System.out.println("Not going to  addParliament_gate ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}
			else if(actionType.equals("UploadFilesFromDropZone"))
			{
				commonRequestHandler.UploadFilesFromDropZone(request, response, userDTO);
			}

			else if(actionType.equals("getDTO"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PARLIAMENT_GATE_ADD))
				{
					getDTO(request, response);
				}
				else
				{
					System.out.println("Not going to  addParliament_gate ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}
			else if(actionType.equals("edit"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PARLIAMENT_GATE_UPDATE))
				{
					addParliament_gate(request, response, false, userDTO, isPermanentTable);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("delete"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PARLIAMENT_GATE_UPDATE))
				{
					commonRequestHandler.delete(request, response, userDTO);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("search"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PARLIAMENT_GATE_SEARCH))
				{
					searchParliament_gate(request, response, true, "");
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getGeo"))
			{
				System.out.println("going to geoloc ");
				request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
			}

		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}

	private void getDTO(HttpServletRequest request, HttpServletResponse response)
	{
		try
		{
			System.out.println("In getDTO");
			Parliament_gateDTO parliament_gateDTO = parliament_gateDAO.getDTOByID(Long.parseLong(request.getParameter("ID")));
			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");

			String encoded = this.gson.toJson(parliament_gateDTO);
			System.out.println("json encoded data = " + encoded);
			out.print(encoded);
			out.flush();
		}
		catch (NumberFormatException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void addParliament_gate(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, boolean isPermanentTable) throws IOException
	{
		// TODO Auto-generated method stub
		try
		{
			request.setAttribute("failureMessage", "");
			System.out.println("%%%% addParliament_gate");
			String path = getServletContext().getRealPath("/img2/");
			Parliament_gateDTO parliament_gateDTO;
			SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");

			if(addFlag == true)
			{
				parliament_gateDTO = new Parliament_gateDTO();
			}
			else
			{
				parliament_gateDTO = parliament_gateDAO.getDTOByID(Long.parseLong(request.getParameter("ID")));
			}

			String Value = "";

			Value = request.getParameter("gateNumber");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("gateNumber = " + Value);
			if(Value != null)
			{
				parliament_gateDTO.gateNumber = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("gatePabxExtNumber");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("gatePabxExtNumber = " + Value);
			if(Value != null)
			{
				parliament_gateDTO.gatePabxExtNumber = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("nameEng");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("nameEng = " + Value);
			if(Value != null)
			{
				parliament_gateDTO.nameEng = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("nameBng");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("nameBng = " + Value);
			if(Value != null)
			{
				parliament_gateDTO.nameBng = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("locationEng");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("locationEng = " + Value);
			if(Value != null)
			{
				parliament_gateDTO.locationEng = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("locationBng");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("locationBng = " + Value);
			if(Value != null)
			{
				parliament_gateDTO.locationBng = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

//			Value = request.getParameter("description");
//
//			if(Value != null)
//			{
//				Value = Jsoup.clean(Value,Whitelist.simpleText());
//			}
//			System.out.println("description = " + Value);
//			if(Value != null)
//			{
//				parliament_gateDTO.description = (Value);
//			}
//			else
//			{
//				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//			}


			Value = request.getParameter("car_entry");

			if (Value != null) {
				Value = Jsoup.clean(Value, Whitelist.simpleText());
			}
			System.out.println("car_entry = " + Value);
			if (Value != null && !Value.equalsIgnoreCase("")) {

				parliament_gateDTO.carEntry = 1;
			} else {
				parliament_gateDTO.carEntry = 0;
			}

			Value = request.getParameter("public_entry");

			if (Value != null) {
				Value = Jsoup.clean(Value, Whitelist.simpleText());
			}
			System.out.println("public_entry = " + Value);
			if (Value != null && !Value.equalsIgnoreCase("")) {

				parliament_gateDTO.publicEntry = 1;
			} else {
				parliament_gateDTO.publicEntry = 0;
			}

			Value = request.getParameter("only_vip_entry");

			if (Value != null) {
				Value = Jsoup.clean(Value, Whitelist.simpleText());
			}
			System.out.println("only_vip_entry = " + Value);
			if (Value != null && !Value.equalsIgnoreCase("")) {

				parliament_gateDTO.onlyVipEntry = 1;
			} else {
				parliament_gateDTO.onlyVipEntry = 0;
			}

			Value = request.getParameter("entry_permission");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("entryPermission = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{

				parliament_gateDTO.entryPermission = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			if(addFlag)
			{
				Calendar c = Calendar.getInstance();
				c.set(Calendar.HOUR_OF_DAY, 0);
				c.set(Calendar.MINUTE, 0);
				c.set(Calendar.SECOND, 0);
				c.set(Calendar.MILLISECOND, 0);

				parliament_gateDTO.insertionDate = c.getTimeInMillis();
			}


			Value = request.getParameter("insertedBy");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("insertedBy = " + Value);
			if(Value != null)
			{
				parliament_gateDTO.insertedBy = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("modifiedBy");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("modifiedBy = " + Value);
			if(Value != null)
			{
				parliament_gateDTO.modifiedBy = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			System.out.println("Done adding  addParliament_gate dto = " + parliament_gateDTO);
			long returnedID = -1;

			if(isPermanentTable == false) //add new row for validation and make the old row outdated
			{
				parliament_gateDAO.setIsDeleted(parliament_gateDTO.iD, CommonDTO.OUTDATED);
				returnedID = parliament_gateDAO.add(parliament_gateDTO);
				parliament_gateDAO.setIsDeleted(returnedID, CommonDTO.WAITING_FOR_APPROVAL);
			}
			else if(addFlag == true)
			{
				returnedID = parliament_gateDAO.manageWriteOperations(parliament_gateDTO, SessionConstants.INSERT, -1, userDTO);
			}
			else
			{
				returnedID = parliament_gateDAO.manageWriteOperations(parliament_gateDTO, SessionConstants.UPDATE, -1, userDTO);
			}









			if(isPermanentTable)
			{
				String inPlaceSubmit = request.getParameter("inplacesubmit");

				if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
				{
					getParliament_gate(request, response, returnedID);
				}
				else
				{
					response.sendRedirect("Parliament_gateServlet?actionType=search");
				}
			}
			else
			{
				commonRequestHandler.validate(parliament_gateDAO.getDTOByID(returnedID), request, response, userDTO);
			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}









	private void getParliament_gate(HttpServletRequest request, HttpServletResponse response, long id) throws ServletException, IOException
	{
		System.out.println("in getParliament_gate");
		Parliament_gateDTO parliament_gateDTO = null;
		try
		{
			parliament_gateDTO = parliament_gateDAO.getDTOByID(id);
			request.setAttribute("ID", parliament_gateDTO.iD);
			request.setAttribute("parliament_gateDTO",parliament_gateDTO);
			request.setAttribute("parliament_gateDAO",parliament_gateDAO);

			String URL= "";

			String inPlaceEdit = request.getParameter("inplaceedit");
			String inPlaceSubmit = request.getParameter("inplacesubmit");
			String getBodyOnly = request.getParameter("getBodyOnly");

			if(inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase(""))
			{
				URL = "parliament_gate/parliament_gateInPlaceEdit.jsp";
				request.setAttribute("inplaceedit","");
			}
			else if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
			{
				URL = "parliament_gate/parliament_gateSearchRow.jsp";
				request.setAttribute("inplacesubmit","");
			}
			else
			{
				if(getBodyOnly  != null && !getBodyOnly.equalsIgnoreCase(""))
				{
					URL = "parliament_gate/parliament_gateEditBody.jsp?actionType=edit";
				}
				else
				{
					URL = "parliament_gate/parliament_gateEdit.jsp?actionType=edit";
				}
			}

			RequestDispatcher rd = request.getRequestDispatcher(URL);
			rd.forward(request, response);
		}
		catch (NumberFormatException e)
		{
			e.printStackTrace();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}


	private void getParliament_gate(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		getParliament_gate(request, response, Long.parseLong(request.getParameter("ID")));
	}

	private void searchParliament_gate(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException
	{
		System.out.println("in  searchParliament_gate 1");
		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String ajax = request.getParameter("ajax");
		boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);

        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
			SessionConstants.NAV_PARLIAMENT_GATE,
			request,
			parliament_gateDAO,
			SessionConstants.VIEW_PARLIAMENT_GATE,
			SessionConstants.SEARCH_PARLIAMENT_GATE,
			tableName,
			isPermanent,
			userDTO,
			filter,
			true);
        try
        {
			System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        }
        catch(Exception e)
        {
			System.out.println("failed to dojob" + e);
        }

		request.setAttribute("parliament_gateDAO",parliament_gateDAO);
        RequestDispatcher rd;
        if(!isPermanent)
        {
        	if(hasAjax == false)
	        {
	        	System.out.println("Going to parliament_gate/parliament_gateApproval.jsp");
	        	rd = request.getRequestDispatcher("parliament_gate/parliament_gateApproval.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to parliament_gate/parliament_gateApprovalForm.jsp");
	        	rd = request.getRequestDispatcher("parliament_gate/parliament_gateApprovalForm.jsp");
	        }
        }
        else
        {
	        if(hasAjax == false)
	        {
	        	System.out.println("Going to parliament_gate/parliament_gateSearch.jsp");
	        	rd = request.getRequestDispatcher("parliament_gate/parliament_gateSearch.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to parliament_gate/parliament_gateSearchForm.jsp");
	        	rd = request.getRequestDispatcher("parliament_gate/parliament_gateSearchForm.jsp");
	        }
        }
		rd.forward(request, response);
	}

}

