package parliament_gate;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;

import pb.OptionDTO;
import pb.Utils;
import repository.Repository;
import repository.RepositoryManager;


public class Parliament_gateRepository implements Repository {
	Parliament_gateDAO parliament_gateDAO = new Parliament_gateDAO();
	
	public void setDAO(Parliament_gateDAO parliament_gateDAO)
	{
		this.parliament_gateDAO = parliament_gateDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Parliament_gateRepository.class);
	Map<Long, Parliament_gateDTO>mapOfParliament_gateDTOToid;
	Map<String, Set<Parliament_gateDTO> >mapOfParliament_gateDTOTogateNumber;
	Map<String, Set<Parliament_gateDTO> >mapOfParliament_gateDTOTogatePabxExtNumber;
	Map<String, Set<Parliament_gateDTO> >mapOfParliament_gateDTOTonameEng;
	Map<String, Set<Parliament_gateDTO> >mapOfParliament_gateDTOTonameBng;
	Map<String, Set<Parliament_gateDTO> >mapOfParliament_gateDTOTolocationEng;
	Map<String, Set<Parliament_gateDTO> >mapOfParliament_gateDTOTolocationBng;
	Map<String, Set<Parliament_gateDTO> >mapOfParliament_gateDTOTodescription;
	Map<Long, Set<Parliament_gateDTO> >mapOfParliament_gateDTOToimageDropzone;
	Map<Long, Set<Parliament_gateDTO> >mapOfParliament_gateDTOToinsertionDate;
	Map<String, Set<Parliament_gateDTO> >mapOfParliament_gateDTOToinsertedBy;
	Map<String, Set<Parliament_gateDTO> >mapOfParliament_gateDTOTomodifiedBy;
	Map<Long, Set<Parliament_gateDTO> >mapOfParliament_gateDTOTolastModificationTime;


	static Parliament_gateRepository instance = null;  
	private Parliament_gateRepository(){
		mapOfParliament_gateDTOToid = new ConcurrentHashMap<>();
		mapOfParliament_gateDTOTogateNumber = new ConcurrentHashMap<>();
		mapOfParliament_gateDTOTogatePabxExtNumber = new ConcurrentHashMap<>();
		mapOfParliament_gateDTOTonameEng = new ConcurrentHashMap<>();
		mapOfParliament_gateDTOTonameBng = new ConcurrentHashMap<>();
		mapOfParliament_gateDTOTolocationEng = new ConcurrentHashMap<>();
		mapOfParliament_gateDTOTolocationBng = new ConcurrentHashMap<>();
		mapOfParliament_gateDTOTodescription = new ConcurrentHashMap<>();
		mapOfParliament_gateDTOToimageDropzone = new ConcurrentHashMap<>();
		mapOfParliament_gateDTOToinsertionDate = new ConcurrentHashMap<>();
		mapOfParliament_gateDTOToinsertedBy = new ConcurrentHashMap<>();
		mapOfParliament_gateDTOTomodifiedBy = new ConcurrentHashMap<>();
		mapOfParliament_gateDTOTolastModificationTime = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Parliament_gateRepository getInstance(){
		if (instance == null){
			instance = new Parliament_gateRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(parliament_gateDAO == null)
		{
			return;
		}
		try {
			List<Parliament_gateDTO> parliament_gateDTOs = parliament_gateDAO.getAllParliament_gate(reloadAll);
			for(Parliament_gateDTO parliament_gateDTO : parliament_gateDTOs) {
				Parliament_gateDTO oldParliament_gateDTO = getParliament_gateDTOByid(parliament_gateDTO.iD);
				if( oldParliament_gateDTO != null ) {
					mapOfParliament_gateDTOToid.remove(oldParliament_gateDTO.iD);
				
					if(mapOfParliament_gateDTOTogateNumber.containsKey(oldParliament_gateDTO.gateNumber)) {
						mapOfParliament_gateDTOTogateNumber.get(oldParliament_gateDTO.gateNumber).remove(oldParliament_gateDTO);
					}
					if(mapOfParliament_gateDTOTogateNumber.get(oldParliament_gateDTO.gateNumber).isEmpty()) {
						mapOfParliament_gateDTOTogateNumber.remove(oldParliament_gateDTO.gateNumber);
					}
					
					if(mapOfParliament_gateDTOTogatePabxExtNumber.containsKey(oldParliament_gateDTO.gatePabxExtNumber)) {
						mapOfParliament_gateDTOTogatePabxExtNumber.get(oldParliament_gateDTO.gatePabxExtNumber).remove(oldParliament_gateDTO);
					}
					if(mapOfParliament_gateDTOTogatePabxExtNumber.get(oldParliament_gateDTO.gatePabxExtNumber).isEmpty()) {
						mapOfParliament_gateDTOTogatePabxExtNumber.remove(oldParliament_gateDTO.gatePabxExtNumber);
					}
					
					if(mapOfParliament_gateDTOTonameEng.containsKey(oldParliament_gateDTO.nameEng)) {
						mapOfParliament_gateDTOTonameEng.get(oldParliament_gateDTO.nameEng).remove(oldParliament_gateDTO);
					}
					if(mapOfParliament_gateDTOTonameEng.get(oldParliament_gateDTO.nameEng).isEmpty()) {
						mapOfParliament_gateDTOTonameEng.remove(oldParliament_gateDTO.nameEng);
					}
					
					if(mapOfParliament_gateDTOTonameBng.containsKey(oldParliament_gateDTO.nameBng)) {
						mapOfParliament_gateDTOTonameBng.get(oldParliament_gateDTO.nameBng).remove(oldParliament_gateDTO);
					}
					if(mapOfParliament_gateDTOTonameBng.get(oldParliament_gateDTO.nameBng).isEmpty()) {
						mapOfParliament_gateDTOTonameBng.remove(oldParliament_gateDTO.nameBng);
					}
					
					if(mapOfParliament_gateDTOTolocationEng.containsKey(oldParliament_gateDTO.locationEng)) {
						mapOfParliament_gateDTOTolocationEng.get(oldParliament_gateDTO.locationEng).remove(oldParliament_gateDTO);
					}
					if(mapOfParliament_gateDTOTolocationEng.get(oldParliament_gateDTO.locationEng).isEmpty()) {
						mapOfParliament_gateDTOTolocationEng.remove(oldParliament_gateDTO.locationEng);
					}
					
					if(mapOfParliament_gateDTOTolocationBng.containsKey(oldParliament_gateDTO.locationBng)) {
						mapOfParliament_gateDTOTolocationBng.get(oldParliament_gateDTO.locationBng).remove(oldParliament_gateDTO);
					}
					if(mapOfParliament_gateDTOTolocationBng.get(oldParliament_gateDTO.locationBng).isEmpty()) {
						mapOfParliament_gateDTOTolocationBng.remove(oldParliament_gateDTO.locationBng);
					}
					
					if(mapOfParliament_gateDTOToinsertionDate.containsKey(oldParliament_gateDTO.insertionDate)) {
						mapOfParliament_gateDTOToinsertionDate.get(oldParliament_gateDTO.insertionDate).remove(oldParliament_gateDTO);
					}
					if(mapOfParliament_gateDTOToinsertionDate.get(oldParliament_gateDTO.insertionDate).isEmpty()) {
						mapOfParliament_gateDTOToinsertionDate.remove(oldParliament_gateDTO.insertionDate);
					}
					
					if(mapOfParliament_gateDTOToinsertedBy.containsKey(oldParliament_gateDTO.insertedBy)) {
						mapOfParliament_gateDTOToinsertedBy.get(oldParliament_gateDTO.insertedBy).remove(oldParliament_gateDTO);
					}
					if(mapOfParliament_gateDTOToinsertedBy.get(oldParliament_gateDTO.insertedBy).isEmpty()) {
						mapOfParliament_gateDTOToinsertedBy.remove(oldParliament_gateDTO.insertedBy);
					}
					
					if(mapOfParliament_gateDTOTomodifiedBy.containsKey(oldParliament_gateDTO.modifiedBy)) {
						mapOfParliament_gateDTOTomodifiedBy.get(oldParliament_gateDTO.modifiedBy).remove(oldParliament_gateDTO);
					}
					if(mapOfParliament_gateDTOTomodifiedBy.get(oldParliament_gateDTO.modifiedBy).isEmpty()) {
						mapOfParliament_gateDTOTomodifiedBy.remove(oldParliament_gateDTO.modifiedBy);
					}
					
					if(mapOfParliament_gateDTOTolastModificationTime.containsKey(oldParliament_gateDTO.lastModificationTime)) {
						mapOfParliament_gateDTOTolastModificationTime.get(oldParliament_gateDTO.lastModificationTime).remove(oldParliament_gateDTO);
					}
					if(mapOfParliament_gateDTOTolastModificationTime.get(oldParliament_gateDTO.lastModificationTime).isEmpty()) {
						mapOfParliament_gateDTOTolastModificationTime.remove(oldParliament_gateDTO.lastModificationTime);
					}
					
					
				}
				if(parliament_gateDTO.isDeleted == 0) 
				{
					
					mapOfParliament_gateDTOToid.put(parliament_gateDTO.iD, parliament_gateDTO);
				
					if( ! mapOfParliament_gateDTOTogateNumber.containsKey(parliament_gateDTO.gateNumber)) {
						mapOfParliament_gateDTOTogateNumber.put(parliament_gateDTO.gateNumber, new HashSet<>());
					}
					mapOfParliament_gateDTOTogateNumber.get(parliament_gateDTO.gateNumber).add(parliament_gateDTO);
					
					if( ! mapOfParliament_gateDTOTogatePabxExtNumber.containsKey(parliament_gateDTO.gatePabxExtNumber)) {
						mapOfParliament_gateDTOTogatePabxExtNumber.put(parliament_gateDTO.gatePabxExtNumber, new HashSet<>());
					}
					mapOfParliament_gateDTOTogatePabxExtNumber.get(parliament_gateDTO.gatePabxExtNumber).add(parliament_gateDTO);
					
					if( ! mapOfParliament_gateDTOTonameEng.containsKey(parliament_gateDTO.nameEng)) {
						mapOfParliament_gateDTOTonameEng.put(parliament_gateDTO.nameEng, new HashSet<>());
					}
					mapOfParliament_gateDTOTonameEng.get(parliament_gateDTO.nameEng).add(parliament_gateDTO);
					
					if( ! mapOfParliament_gateDTOTonameBng.containsKey(parliament_gateDTO.nameBng)) {
						mapOfParliament_gateDTOTonameBng.put(parliament_gateDTO.nameBng, new HashSet<>());
					}
					mapOfParliament_gateDTOTonameBng.get(parliament_gateDTO.nameBng).add(parliament_gateDTO);
					
					if( ! mapOfParliament_gateDTOTolocationEng.containsKey(parliament_gateDTO.locationEng)) {
						mapOfParliament_gateDTOTolocationEng.put(parliament_gateDTO.locationEng, new HashSet<>());
					}
					mapOfParliament_gateDTOTolocationEng.get(parliament_gateDTO.locationEng).add(parliament_gateDTO);
					
					if( ! mapOfParliament_gateDTOTolocationBng.containsKey(parliament_gateDTO.locationBng)) {
						mapOfParliament_gateDTOTolocationBng.put(parliament_gateDTO.locationBng, new HashSet<>());
					}
					mapOfParliament_gateDTOTolocationBng.get(parliament_gateDTO.locationBng).add(parliament_gateDTO);
					
					if( ! mapOfParliament_gateDTOToinsertionDate.containsKey(parliament_gateDTO.insertionDate)) {
						mapOfParliament_gateDTOToinsertionDate.put(parliament_gateDTO.insertionDate, new HashSet<>());
					}
					mapOfParliament_gateDTOToinsertionDate.get(parliament_gateDTO.insertionDate).add(parliament_gateDTO);
					
					if( ! mapOfParliament_gateDTOToinsertedBy.containsKey(parliament_gateDTO.insertedBy)) {
						mapOfParliament_gateDTOToinsertedBy.put(parliament_gateDTO.insertedBy, new HashSet<>());
					}
					mapOfParliament_gateDTOToinsertedBy.get(parliament_gateDTO.insertedBy).add(parliament_gateDTO);
					
					if( ! mapOfParliament_gateDTOTomodifiedBy.containsKey(parliament_gateDTO.modifiedBy)) {
						mapOfParliament_gateDTOTomodifiedBy.put(parliament_gateDTO.modifiedBy, new HashSet<>());
					}
					mapOfParliament_gateDTOTomodifiedBy.get(parliament_gateDTO.modifiedBy).add(parliament_gateDTO);
					
					if( ! mapOfParliament_gateDTOTolastModificationTime.containsKey(parliament_gateDTO.lastModificationTime)) {
						mapOfParliament_gateDTOTolastModificationTime.put(parliament_gateDTO.lastModificationTime, new HashSet<>());
					}
					mapOfParliament_gateDTOTolastModificationTime.get(parliament_gateDTO.lastModificationTime).add(parliament_gateDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Parliament_gateDTO> getParliament_gateList() {
		List <Parliament_gateDTO> parliament_gates = new ArrayList<Parliament_gateDTO>(this.mapOfParliament_gateDTOToid.values());
		return parliament_gates;
	}
	
	
	public Parliament_gateDTO getParliament_gateDTOByid( long id){
		return mapOfParliament_gateDTOToid.get(id);
	}
	
	
	public List<Parliament_gateDTO> getParliament_gateDTOBygate_number(String gate_number) {
		return new ArrayList<>( mapOfParliament_gateDTOTogateNumber.getOrDefault(gate_number,new HashSet<>()));
	}
	
	
	public List<Parliament_gateDTO> getParliament_gateDTOBygate_pabx_ext_number(String gate_pabx_ext_number) {
		return new ArrayList<>( mapOfParliament_gateDTOTogatePabxExtNumber.getOrDefault(gate_pabx_ext_number,new HashSet<>()));
	}
	
	
	public List<Parliament_gateDTO> getParliament_gateDTOByname_eng(String name_eng) {
		return new ArrayList<>( mapOfParliament_gateDTOTonameEng.getOrDefault(name_eng,new HashSet<>()));
	}
	
	
	public List<Parliament_gateDTO> getParliament_gateDTOByname_bng(String name_bng) {
		return new ArrayList<>( mapOfParliament_gateDTOTonameBng.getOrDefault(name_bng,new HashSet<>()));
	}
	
	
	public List<Parliament_gateDTO> getParliament_gateDTOBylocation_eng(String location_eng) {
		return new ArrayList<>( mapOfParliament_gateDTOTolocationEng.getOrDefault(location_eng,new HashSet<>()));
	}
	
	
	public List<Parliament_gateDTO> getParliament_gateDTOBylocation_bng(String location_bng) {
		return new ArrayList<>( mapOfParliament_gateDTOTolocationBng.getOrDefault(location_bng,new HashSet<>()));
	}
	
	
	public List<Parliament_gateDTO> getParliament_gateDTOBydescription(String description) {
		return new ArrayList<>( mapOfParliament_gateDTOTodescription.getOrDefault(description,new HashSet<>()));
	}
	
	
	public List<Parliament_gateDTO> getParliament_gateDTOByimage_dropzone(long image_dropzone) {
		return new ArrayList<>( mapOfParliament_gateDTOToimageDropzone.getOrDefault(image_dropzone,new HashSet<>()));
	}
	
	
	public List<Parliament_gateDTO> getParliament_gateDTOByinsertion_date(long insertion_date) {
		return new ArrayList<>( mapOfParliament_gateDTOToinsertionDate.getOrDefault(insertion_date,new HashSet<>()));
	}
	
	
	public List<Parliament_gateDTO> getParliament_gateDTOByinserted_by(String inserted_by) {
		return new ArrayList<>( mapOfParliament_gateDTOToinsertedBy.getOrDefault(inserted_by,new HashSet<>()));
	}
	
	
	public List<Parliament_gateDTO> getParliament_gateDTOBymodified_by(String modified_by) {
		return new ArrayList<>( mapOfParliament_gateDTOTomodifiedBy.getOrDefault(modified_by,new HashSet<>()));
	}
	
	
	public List<Parliament_gateDTO> getParliament_gateDTOBylastModificationTime(long lastModificationTime) {
		return new ArrayList<>( mapOfParliament_gateDTOTolastModificationTime.getOrDefault(lastModificationTime,new HashSet<>()));
	}

	public String buildOptions(String language, long selectedId) {
		List<OptionDTO> optionDTOList = null;
		List<Parliament_gateDTO> parliament_gateDTOList = new Parliament_gateDAO().getAllParliament_gate(true);
		if (parliament_gateDTOList != null && parliament_gateDTOList.size() > 0) {
			optionDTOList = parliament_gateDTOList.stream()
					.map(dto -> new OptionDTO(dto.nameEng + " (" + dto.gateNumber + ")",
							dto.nameBng +" (" + englishToBanglaNumber(dto.gateNumber) + ")", String.valueOf(dto.iD)))
					.collect(Collectors.toList());
		}
		return Utils.buildOptions(optionDTOList, language, String.valueOf(selectedId));
	}


	private String englishToBanglaNumber(String englishNumber) {
		char[] en = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
		char[] bn = {'০', '১', '২', '৩', '৪', '৫', '৬', '৭', '৮', '৯'};
		if (englishNumber == null || englishNumber.isEmpty()) {
			return "";
		}
		String banglaNumber = "";
		for (int i = 0; i < englishNumber.length(); i++) {
			for (int j = 0; j < en.length; j++) {
				if (englishNumber.charAt(i) == en[j]) {
					banglaNumber += bn[j];
					break;
				}
			}
		}
		if (banglaNumber.isEmpty()) banglaNumber = "0";
		return banglaNumber;
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "parliament_gate";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


