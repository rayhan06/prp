package user;

import dbm.DBMR;
import org.apache.log4j.Logger;
import repository.Repository;
import repository.RepositoryManager;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public class LoginIPRepository implements Repository {
    private final Logger logger = Logger.getLogger(getClass());

    private static Map<Long, Set<LoginIPDTO>> loginIPDTOHashMap;
    private static LoginIPRepository instance = null;

    private LoginIPRepository() {
        loginIPDTOHashMap = new ConcurrentHashMap<>();
        RepositoryManager.getInstance().addRepository(this);
    }

    public static LoginIPRepository getInstance() {
        if (instance == null) {
            synchronized (LoginIPRepository.class) {
                if (instance == null) instance = new LoginIPRepository();
            }
        }
        return instance;
    }

    ArrayList<String> getLoginIPAddressByUserId(long userId) {
        ArrayList<String> ipList = new ArrayList<>();
        Set<LoginIPDTO> loginIPDTOS = loginIPDTOHashMap.get(userId);

        if (loginIPDTOS == null) return ipList;

        for (LoginIPDTO loginIPDTO : loginIPDTOS) {
            ipList.add(loginIPDTO.IPAddress);
        }
        return ipList;
    }

    @Override
    public synchronized void reload(boolean reloadAll) {
        logger.debug("LoginIPRepository loading start for reloadAll: "+reloadAll);
        ArrayList<LoginIPDTO> ipDtoArrayList = getLoginIPAddress(reloadAll);
        for (LoginIPDTO loginIPDTO : ipDtoArrayList) {
            Set<LoginIPDTO> list = loginIPDTOHashMap.computeIfAbsent(loginIPDTO.userId, k -> new HashSet<>());
            list.add(loginIPDTO);
        }
        logger.debug("LoginIPRepository loading start for reloadAll: "+reloadAll);
    }

    @Override
    public String getTableName() {
        return null;
    }

    private ArrayList<LoginIPDTO> getLoginIPAddress(boolean isFirstReload) {
        Connection connection;
        Statement stmt;
        ResultSet rs;

        ArrayList<LoginIPDTO> ipAddressList = new ArrayList<>();
        try {
            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();

            String sql = "select ID, userID, IP from login_ip where ";
            if (isFirstReload) {
                sql += " isDeleted =  0";
            }
            if (!isFirstReload) {
                sql += " lastModificationTime >= " + RepositoryManager.lastModifyTime;
            }

            rs = stmt.executeQuery(sql);

            while (rs.next()) {
                LoginIPDTO loginIPDTO = new LoginIPDTO();
                loginIPDTO.ID = rs.getLong("ID");
                loginIPDTO.userId = rs.getLong("userID");
                loginIPDTO.IPAddress = rs.getString("IP");

                ipAddressList.add(loginIPDTO);
            }
        } catch (Exception ex) {
            logger.error("getLoginIPAddress: " + ex.getMessage());
        }
        return ipAddressList;
    }
}
