package user;

import sessionmanager.SessionConstants;

import java.util.ArrayList;
import java.util.List;

public class UserDTO {
	public long ID;
	public String userName = "";
	public String password = "";
	public String repassword = "";
	public int userType = 1;
	public long roleID = 1;
	public int languageID = 1;
	public int isDeleted;
	public String mailAddress = "";
	public String fullName = "";
	public String phoneNo = "";
	public int centreType = 3;
	public String roleName = "";
	public String userTypeName = "";
	public List<String> loginIPs = new ArrayList<>();
	public boolean otpSMS;
	public boolean otpEmail;
	public boolean otpPushNotification;
	public long employee_record_id = -1;
	public long officeID = -1;
	public long employeeID = -1;
	public long organogramID = -1;
	public long unitID = -1;
	public String nid = "";
	public long dateOfBirth = -1;
	public String birthRegistrationNo = "";
	public long approvalPathID = -1;
	public long approvalOrder = -1;
	public long approvalRole = -1;
	public long maxApprovalOrder = -1;
	public String otp="";
	public long otpRequestTime= SessionConstants.MIN_DATE;
	public long otpExpireTime= SessionConstants.MIN_DATE;
	public long otpAttemptCount= -1;
	public boolean active = true;
	public long lastModificationTime=0;

	@Override
	public String toString() {
		return "UserDTO{" +
				"ID=" + ID +
				", userName='" + userName + '\'' +
				", password='" + password + '\'' +
				", userType=" + userType +
				", roleID=" + roleID +
				", languageID=" + languageID +
				", isDeleted=" + isDeleted +
				", mailAddress='" + mailAddress + '\'' +
				", fullName='" + fullName + '\'' +
				", phoneNo='" + phoneNo + '\'' +
				", centreType=" + centreType +
				", roleName='" + roleName + '\'' +
				", userTypeName='" + userTypeName + '\'' +
				", loginIPs=" + loginIPs +
				", otpSMS=" + otpSMS +
				", otpEmail=" + otpEmail +
				", otpPushNotification=" + otpPushNotification +
				", employee_record_id=" + employee_record_id +
				", officeID=" + officeID +
				", employeeID=" + employeeID +
				", organogramID=" + organogramID +
				", unitID=" + unitID +
				", nid='" + nid + '\'' +
				", dateOfBirth=" + dateOfBirth +
				", birthRegistrationNo='" + birthRegistrationNo + '\'' +
				", approvalPathID=" + approvalPathID +
				", approvalOrder=" + approvalOrder +
				", approvalRole=" + approvalRole +
				", maxApprovalOrder=" + maxApprovalOrder +
				", otp='" + otp + '\'' +
				", otpRequestTime=" + otpRequestTime +
				", otpExpireTime=" + otpExpireTime +
				", otpAttemptCount=" + otpAttemptCount +
				'}';
	}
}