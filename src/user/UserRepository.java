package user;

import login.LoginDTO;
import org.apache.log4j.Logger;
import pb.Utils;
import repository.Repository;
import repository.RepositoryManager;
import util.LockManager;

import java.util.*;
import java.util.concurrent.locks.ReentrantReadWriteLock;


/**
 * @author Kayesh Parvez
 */
@SuppressWarnings({"unused"})
public class UserRepository implements Repository {
    private static final UserDAO userDAO = new UserDAO();

    private static final Logger logger = Logger.getLogger(UserRepository.class);
    private static Map<String, UserDTO> mapOfUserDTOToUserName = new HashMap<>();
    private static Map<String, UserDTO> mapOfAllUserDTOToUserName = new HashMap<>();
    private static Map<Long, UserDTO> mapOfUserDTOToUserID = new HashMap<>();
    private static Map<Long, UserDTO> mapOfUserDTOToOrganogramID = new HashMap<>();
    private static Map<Long, UserDTO> userDtoByEmployeeRecordId = new HashMap<>();

    private static final ReentrantReadWriteLock lock = new ReentrantReadWriteLock();
    private static final ReentrantReadWriteLock.ReadLock readLock = lock.readLock();
    private static final ReentrantReadWriteLock.WriteLock writeLock = lock.writeLock();

    private static volatile UserDTO superAdminUserDTO = null;

    private UserRepository() {
        RepositoryManager.getInstance().addRepository(this);
    }

    private static class LazyLoader {
        static UserRepository INSTANCE = new UserRepository();
    }

    public synchronized static UserRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

    public void reload(boolean reloadAll) {
        logger.debug("UserRepository reload start,reloadAll: " + reloadAll);
        if (reloadAll) {
            init();
        }
        try {
            List<UserDTO> userDTOs = userDAO.getAllDefaultUsers(reloadAll);
            List<UserDTO> list = userDAO.getAllUsersFromUsersTable(reloadAll);
            if (userDTOs == null || userDTOs.size() == 0) {
                return;
            }
            cache(userDTOs,list);
        } catch (Exception e) {
            logger.debug("FATAL", e);
        }
        logger.debug("UserRepository reload end,reloadAll :" + reloadAll);
    }

    private void cache(List<UserDTO> defaultUsers,List<UserDTO> allUsers){
        try {
            writeLock.lock();
            defaultUsers.forEach(userDTO -> updateCache(mapOfUserDTOToUserID.get(userDTO.ID), userDTO));
            allUsers.forEach(UserRepository::updateUsersTableCache);
        } finally {
            writeLock.unlock();
        }
    }

    @Override
    public void reloadWithExactModificationTime(long time) {
        List<UserDTO> userDTOs = userDAO.getAllDefaultUsersForFixedLastModificationTime(time);
        List<UserDTO> list = userDAO.getAllUsersFromUsersTableForFixedLastModificationTime(time);
        cache(userDTOs,list);
    }

    private static void init() {
        mapOfUserDTOToUserName = new HashMap<>();
        mapOfAllUserDTOToUserName = new HashMap<>();
        mapOfUserDTOToUserID = new HashMap<>();
        mapOfUserDTOToOrganogramID = new HashMap<>();
        userDtoByEmployeeRecordId = new HashMap<>();
    }

    private static void updateCache(UserDTO oldUserDTO, UserDTO userDTO) {
        removeFromCache(oldUserDTO);
        if (userDTO.isDeleted == 0 && userDTO.active) {
            if (userDTO.lastModificationTime > System.currentTimeMillis()) {
                userDTO.lastModificationTime = System.currentTimeMillis();
            }
            mapOfUserDTOToUserID.put(userDTO.ID, userDTO);
            userDtoByEmployeeRecordId.put(userDTO.employee_record_id, userDTO);
            mapOfUserDTOToUserName.put(userDTO.userName, userDTO);
            mapOfUserDTOToOrganogramID.put(userDTO.organogramID, userDTO);
            mapOfAllUserDTOToUserName.put(userDTO.userName, userDTO);
        }
    }

    private static void updateUsersTableCache(UserDTO userDTO) {
        UserDTO oldUserDTO = mapOfUserDTOToUserID.get(userDTO.ID);
        if (oldUserDTO != null) {
            mapOfAllUserDTOToUserName.remove(oldUserDTO.userName);
        }
        if (userDTO.isDeleted == 0 && userDTO.active) {
            mapOfAllUserDTOToUserName.put(userDTO.userName, userDTO);
        }
    }


    private static void removeFromCache(UserDTO oldUserDTO) {
        if (oldUserDTO != null) {
            mapOfUserDTOToUserID.remove(oldUserDTO.ID);
            userDtoByEmployeeRecordId.remove(oldUserDTO.employee_record_id);
            mapOfUserDTOToUserName.remove(oldUserDTO.userName);
            mapOfUserDTOToOrganogramID.remove(oldUserDTO.organogramID);
            mapOfAllUserDTOToUserName.remove(oldUserDTO.userName);
        }
    }

    public static void updateCache(UserDTO userDTO) {
        if (userDTO != null) {
            try {
                writeLock.lock();
                updateCache(mapOfUserDTOToUserID.get(userDTO.ID), userDTO);
            } finally {
                writeLock.unlock();
            }
        }
    }

    public void updateCache(long id) {
        UserDTO userDTO = userDAO.getUserDTOByUserID(id);
        if (userDTO != null) {
            updateCache(userDTO);
        } else {
            userDTO = userDAO.getByIdDeletedOrNot(id);
            cacheUpdateIfUserHasNotAssignedInOffice(userDTO);
        }
    }

    public void updateUserDTO(UserDTO userDTO) {
        try {
            writeLock.lock();
            mapOfUserDTOToUserID.put(userDTO.ID, userDTO);
            mapOfUserDTOToUserName.put(userDTO.userName, userDTO);
        } finally {
            writeLock.unlock();
        }
    }

    public static List<UserDTO> getUserList() {
        try {
            readLock.lock();
            return new ArrayList<>(mapOfUserDTOToUserID.values());
        } finally {
            readLock.unlock();
        }
    }

    public static UserDTO getUserDTOByOrganogramID(long organogramID) {
        boolean updateCache = false;
        UserDTO userDTO;
        try {
            readLock.lock();
            userDTO = mapOfUserDTOToOrganogramID.get(organogramID);
            if (userDTO == null || !userDTO.active) {
                userDTO = userDAO.getUserDTOByOrganogramID(organogramID);
                updateCache = true;
            }
        } finally {
            readLock.unlock();
        }

        if (userDTO != null && updateCache) {
            updateCacheInSeparateThread(userDTO);
        }
        return userDTO;
    }
    
    public static UserDTO getUserDTOByOrganogramIDNoDB(long organogramID) {
        boolean updateCache = false;
        UserDTO userDTO;
        try {
            readLock.lock();
            userDTO = mapOfUserDTOToOrganogramID.get(organogramID);
            
        } finally {
            readLock.unlock();
        }

        return userDTO;
    }

    public static List<UserDTO> getUserDTOByOrganogramID(Set<Long> organogramIDList) {
        List<UserDTO> resultList = new ArrayList<>();
        List<UserDTO> userDTOList = null;
        try {
            readLock.lock();
            List<Long> idList = new ArrayList<>();
            organogramIDList.forEach(organogramID->{
                UserDTO userDTO = mapOfUserDTOToOrganogramID.get(organogramID);
                if (userDTO == null || !userDTO.active) {
                    idList.add(organogramID);
                }else{
                    resultList.add(userDTO);
                }
            });
            if(idList.size()>0){
                userDTOList = userDAO.getUserDTOByOrganogramIDList(idList);
                resultList.addAll(userDTOList);
            }
        } finally {
            readLock.unlock();
        }

        if (userDTOList!=null && userDTOList.size()>0) {
            updateCacheInSeparateThread(userDTOList);
        }
        return resultList;
    }

    private static void updateCacheInSeparateThread(UserDTO userDTO) {
        Thread thread = new Thread(() -> updateCache(userDTO));
        thread.setDaemon(true);
        thread.start();
    }

    private static void updateCacheInSeparateThread(List<UserDTO> userDTOList) {
        Thread thread = new Thread(() -> userDTOList.forEach(UserRepository::updateCache));
        thread.setDaemon(true);
        thread.start();
    }

    public static UserDTO getUserDTOByOrganogramIDAndUserId(long organogramID, long userId) {
        boolean updateCache = false;
        UserDTO userDTO;
        try {
            readLock.lock();
            userDTO = mapOfUserDTOToOrganogramID.get(organogramID);
            if (userDTO == null || !userDTO.active || userDTO.ID != userId) {
                userDTO = userDAO.getUserDTOByOrganogramID(organogramID, userId);
                updateCache = true;
            }
        } finally {
            readLock.unlock();
        }
        if (userDTO != null && updateCache) {
            updateCacheInSeparateThread(userDTO);
        }
        return userDTO;
    }

    public static UserDTO getUserDTOByUserID(long id) {
        boolean updateCache = false;
        UserDTO userDTO;
        try {
            readLock.lock();
            userDTO = mapOfUserDTOToUserID.get(id);
            if (userDTO == null) {
                userDTO = userDAO.getUserDTOByUserID(id);
                updateCache = true;
            }
        } finally {
            readLock.unlock();
        }
        if (userDTO != null && updateCache) {
            updateCacheInSeparateThread(userDTO);
        }
        return userDTO;
    }

    public static UserDTO getUserDTOByUserID(LoginDTO loginDTO) {
        UserDAO userDAO = new UserDAO();
        if (loginDTO.isOisf == 1) {
            try {
                return userDAO.getUserDTOByUserID(loginDTO.userID);
            } catch (Exception e) {
                logger.error(e);
            }

        } else {
            try {
                if (loginDTO.roleId != -1) {
                    return userDAO.getUserDTOByUserIdAndRoleId(loginDTO.userID, loginDTO.roleId, loginDTO.organogramId);
                } else {
                    if (loginDTO.userID == -1) {
                        if (superAdminUserDTO == null) {
                            synchronized (LockManager.getLock("SUPER_ADMIN")) {
                                if (superAdminUserDTO == null) {
                                    superAdminUserDTO = userDAO.getUserDTOByUserID(loginDTO.userID);
                                }
                            }
                        }
                        return superAdminUserDTO;
                    }
                    return userDAO.getUserDTOByUserID(loginDTO.userID);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public static UserDTO getUserDTOByUserName(String userName) {
        boolean updateCache = false;
        UserDTO userDTO;
        try {
            readLock.lock();
            userDTO = mapOfUserDTOToUserName.get(userName);
            if (userDTO == null) {
                userDTO = userDAO.getUserDTOByUsername(userName);
                updateCache = true;
            }
        } finally {
            readLock.unlock();
        }
        if (userDTO != null && updateCache) {
            updateCacheInSeparateThread(userDTO);
        }
        return userDTO;
    }

    public static UserDTO getUserDTOByUserNameOnlyFromRepo(String userName) {
        UserDTO userDTO;
        try {
            readLock.lock();
            userDTO = mapOfUserDTOToUserName.get(userName);
        } finally {
            readLock.unlock();
        }
        return userDTO;
    }

    public static UserDTO getUserDTOByUserNameFromUsersTable(String userName) {
        logger.debug("getUserDTOByUserNameFromUsersTable for userName = " + userName);
        UserDTO userDTO = mapOfAllUserDTOToUserName.get(userName);
        if (userDTO == null) {
            try {
                userDTO = new UserDAO().getUserDTOByUsernameFromOnlyUsers(userName);
                if (userDTO != null) {
                    mapOfAllUserDTOToUserName.put(userName, userDTO);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return userDTO;
    }

    @Override
    public String getTableName() {
        return "users";
    }

    public static UserNameEmailDTO convertUserDTOIntoNameEmailDTO(UserDTO userDTO) {
        UserNameEmailDTO userNameEmailDTO = new UserNameEmailDTO();
        userNameEmailDTO.email = userDTO.userName == null ? "" : userDTO.userName;
        userNameEmailDTO.name = userDTO.fullName == null ? "" : userDTO.fullName;
        return userNameEmailDTO;
    }

    public UserDTO getUserDtoByEmployeeRecordId(long employeeRecordId) {
        boolean updateCache = false;
        UserDTO userDTO;
        try {
            readLock.lock();
            userDTO = userDtoByEmployeeRecordId.get(employeeRecordId);
            if (userDTO == null) {
                userDTO = userDAO.getUserDTOByEmployeeRecordId(employeeRecordId);
                updateCache = true;
            }
        } finally {
            readLock.unlock();
        }
        if (userDTO != null && updateCache) {
            updateCacheInSeparateThread(userDTO);
        }
        return userDTO;
    }

    public UserDTO getUserDtoByEmployeeUserName(String userName) {
        try {
            return getUserDTOByUserName(userName);
        } catch (Exception e) {
            return new UserDTO();
        }
    }

    public static void cacheUpdateByUsername(String userName) {
        UserDTO userDTO = userDAO.getUserDTOByUsername(userName);
        if (userDTO != null) {
            updateCache(userDTO);
        } else {
            userDTO = userDAO.getByUserNameDeletedOrNot(userName);
            cacheUpdateIfUserHasNotAssignedInOffice(userDTO);
        }
    }

    private static void cacheUpdateIfUserHasNotAssignedInOffice(UserDTO userDTO) {
        if (userDTO != null) {
            try {
                writeLock.lock();
                mapOfAllUserDTOToUserName.remove(userDTO.userName);
                mapOfUserDTOToUserName.remove(userDTO.userName);
                mapOfUserDTOToUserID.remove(userDTO.ID);
                mapOfUserDTOToOrganogramID.remove(userDTO.organogramID);
                if (userDTO.isDeleted == 0) {
                    mapOfAllUserDTOToUserName.put(userDTO.userName, userDTO);
                }
            } finally {
                writeLock.unlock();
            }
        }
    }

    public void updateCache(List<Long> ids) {
        if (ids == null || ids.isEmpty()) {
            return;
        }
        List<UserDTO> list = userDAO.getOnlyUsersByIds(ids);
        if (list == null || list.isEmpty()) {
            return;
        }
        try {
            writeLock.lock();
            list.forEach(userDTO -> updateCache(mapOfUserDTOToUserID.get(userDTO.ID), userDTO));
            list.forEach(UserRepository::updateUsersTableCache);
        } finally {
            writeLock.unlock();
        }
    }
}