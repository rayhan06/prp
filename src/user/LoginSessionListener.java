package user;


import java.util.concurrent.ConcurrentHashMap;


import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.apache.log4j.Logger;

import login.LoginDTO;
import sessionmanager.SessionConstants;

@WebListener
public class LoginSessionListener implements HttpSessionListener{
	public static Logger logger = Logger.getLogger(LoginSessionListener.class);	
	private static final ConcurrentHashMap<Long, UserDTO> activeUsers = new ConcurrentHashMap<Long, UserDTO>();
	
	public LoginSessionListener()
	{
		logger.debug("LoginSessionListener created");
	}	
	public static void add(UserDTO userDTO)
	{
		if(!activeUsers.contains(userDTO.ID))
		{
			activeUsers.put(userDTO.ID,userDTO);
			logger.debug("******************* added to session " + userDTO.userName + " id = " + userDTO.ID);
		}
		if(activeUsers.getOrDefault(userDTO.ID, null) == null)
		{
			logger.debug("******************* Still not contains " + userDTO.userName + " id = " + userDTO.ID);
		}
	}
	
	public static void remove(UserDTO userDTO)
	{
		activeUsers.remove(userDTO.ID);
		logger.debug("******************* removed from session " + userDTO.userName );
	}

	@Override
	public void sessionCreated(HttpSessionEvent event) {
		logger.debug("******************* an active session is created");
		LoginDTO loginDTO = (LoginDTO) event.getSession().getAttribute(SessionConstants.USER_LOGIN);
		if(loginDTO != null)
		{
			UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
			if(userDTO != null)
			{
				add(userDTO);
			}	
		}
		
	}

	@Override
	public void sessionDestroyed(HttpSessionEvent event) {
		logger.debug("******************* an active session is destroyed");
		LoginDTO loginDTO = (LoginDTO) event.getSession().getAttribute(SessionConstants.USER_LOGIN);
		if(loginDTO != null)
		{
			UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
			remove(userDTO);
		}
	}
	
	public static boolean userActive(UserDTO userDTO)
	{
		boolean contains = activeUsers.getOrDefault(userDTO.ID, null) != null;
		logger.debug("******************* Session Contains " + userDTO.userName + " id = " + userDTO.ID + ": " + contains);
		return contains;
	}
}

