package user;

import common.CommonDAOService;
import common.ConnectionAndStatementUtil;
import common.RequestFailureException;
import dbm.DBMR;
import dbm.DBMW;
import login.LoginDTO;
import org.apache.log4j.Logger;
import pb.Utils;
import repository.RepositoryManager;
import util.NavigationService;
import util.PasswordUtil;
import util.StringUtils;

import java.sql.*;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

/**
 * @author Kayesh Parvez
 */

@SuppressWarnings("ALL")
public class UserDAO implements NavigationService {

    private static final Logger logger = Logger.getLogger(UserDAO.class);

    public static final String userTable = "users";
    public static final int ADMIN = 1;
    public static final int DEFAULT = 8005;

    private static final String sqlCountByNotDelete = "SELECT count(*) as cnt FROM users WHERE isDeleted <> 1";

    private static final String addUserQuery = "insert into users (ID,userName,password,userType,roleID,lastModificationTime,isDeleted,"
            .concat("mailAddress,fullName,phoneNo,centre_type,nid,birth_registration_no,date_of_birth) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

    private static final String updateUserQuery = "UPDATE users SET userName=?,password=?,userType=?,roleID=?,languageID=?," +
            " lastModificationTime=?,isDeleted=?,mailAddress=?,fullName=?,phoneNo=?,centre_Type=?,otpSMS=?,otpEmail=?," +
            " otpPushNotification=? WHERE ID = ?";

    private static final String updatePasswordQuery = "UPDATE users SET password= ?, lastModificationTime= ? WHERE ID = ?";

    private static final String updatePersonalInfo = "UPDATE users SET fullName= ?,date_of_birth=?, lastModificationTime= ? WHERE ID = ?";

    private static final String updateContactInfo = "UPDATE users SET phoneNo= ?,mailAddress=?, lastModificationTime= ? WHERE ID = ?";

    private static final String updateBCNInfo = "UPDATE users SET birth_reg_no= ?, lastModificationTime= ? WHERE ID = ?";

    private static final String updateNIDInfo = "UPDATE users SET nid= ?, lastModificationTime= ? WHERE ID = ?";

    private static final String updateUserNameInfo = "UPDATE users SET username= ?, lastModificationTime= ? WHERE ID = ?";

    private static final String updateOTPQuery = "UPDATE users SET otp= ?, otp_request_time=?, otp_expire_time=? , otp_attempt_count=? , lastModificationTime= ? WHERE ID = ?";

    private static final String updateUserTypeAndRoleQuery = "UPDATE users SET userType=?,roleID=?,lastModificationTime=? WHERE ID = ?";

    private static final String deleteUserQuery = "UPDATE users SET isDeleted=1,lastModificationTime=? WHERE ID = ?";

    private static final String getByUserName = "SELECT * FROM users WHERE userName = ? and isDeleted = 0";

    private static final String getByUserNameDeletedOrNot = "SELECT * FROM users WHERE userName = ?";

    private static final String getByIdDeletedOrNot = "SELECT * FROM users WHERE id = %d";

    private static final String reloadAllUsersFromUserTableForTrue = "SELECT * FROM users where users.isDeleted = 0 ";

    private static final String reloadAllUsersFromUserTableForFalse = "SELECT * FROM users where  users.lastModificationTime  >= %d";

    private static final String reloadAllUsersFromUserTableForFixedLastModificationTime = "SELECT * FROM users where lastModificationTime=%d";

    private static final String reloadAllDefaultUsersForTrue = "SELECT * FROM users INNER JOIN employee_offices "
            .concat(" ON employee_offices.employee_record_id = users.employee_record_id ")
            .concat(" WHERE employee_offices.isdefault = 1 AND users.isDeleted = 0 AND users.active = 1 AND ")
            .concat(" employee_offices.isDeleted = 0 AND employee_offices.status = 1 and employee_offices.is_default_role = 1");

    private static final String reloadAllDefaultUsersForFalse = "SELECT * FROM users INNER JOIN employee_offices "
            .concat(" ON employee_offices.employee_record_id = users.employee_record_id ")
            .concat(" WHERE employee_offices.isdefault = 1 AND users.active = 1  AND employee_offices.status = 1 AND employee_offices.is_default_role = 1 AND")
            .concat(" (users.lastModificationTime  >= %d OR employee_offices.lastModificationTime >= %d)");

    private static final String reloadAllDefaultUsersForFixedLastModificationTime = "SELECT * FROM users INNER JOIN employee_offices "
            .concat(" ON employee_offices.employee_record_id = users.employee_record_id ")
            .concat(" WHERE employee_offices.isdefault = 1 AND users.isDeleted = 0 AND users.active = 1 AND ")
            .concat(" employee_offices.isDeleted = 0 AND employee_offices.status = 1 and employee_offices.is_default_role = 1")
            .concat(" and (users.lastModificationTime = %d OR employee_offices.lastModificationTime = %d)");

    private static final String sqlQueryToGetIpsByUserIds = "SELECT ID, userID, IP FROM login_ip WHERE userID IN (?)";

    private static final String reloadAllUsersForTrue = "SELECT * FROM users INNER JOIN employee_offices "
            .concat(" ON employee_offices.employee_record_id = users.employee_record_id ")
            .concat(" WHERE users.isDeleted = 0 and employee_offices.isDeleted = 0 and employee_offices.status = 1  and employee_offices.is_default_role = 1");

    private static final String reloadAllUsersForFalse = "SELECT * FROM users INNER JOIN employee_offices "
            .concat(" ON employee_offices.employee_record_id = users.employee_record_id  ")
            .concat(" WHERE users.isDeleted = 0 and employee_offices.isDeleted = 0 and employee_offices.status = 1 and employee_offices.is_default_role = 1 and ")
            .concat(" (users.lastModificationTime  >= %d or employee_offices.lastModificationTime >= %d) ");

    private static final String sqlGetUsersByOfficeUnitOrganogramId = "SELECT * FROM users INNER JOIN employee_offices "
            .concat(" ON employee_offices.employee_record_id = users.employee_record_id ")
            .concat(" WHERE users.isDeleted = 0 and employee_offices.isDeleted = 0 and employee_offices.status = 1  and employee_offices.is_default_role = 1 ")
            .concat(" and office_unit_organogram_id = %d");

    private static final String sqlGetUsersByOfficeUnitOrganogramIdList = "SELECT * FROM users INNER JOIN employee_offices "
            .concat(" ON employee_offices.employee_record_id = users.employee_record_id ")
            .concat(" WHERE users.isDeleted = 0 and employee_offices.isDeleted = 0 and employee_offices.status = 1  and employee_offices.is_default_role = 1 ")
            .concat(" and office_unit_organogram_id IN (%s)");

    private static final String sqlGetUsersByOfficeUnitOrganogramIdAndUserId = "SELECT * FROM users INNER JOIN employee_offices "
            .concat(" ON employee_offices.employee_record_id = users.employee_record_id ")
            .concat(" WHERE users.isDeleted = 0 and employee_offices.isDeleted = 0 and employee_offices.status = 1  and employee_offices.is_default_role = 1 ")
            .concat(" and office_unit_organogram_id = %d and users.ID = %d");

    private static final String sqlGetEmployeeRecordIdById = "SELECT employee_record_id FROM users WHERE id = %d";

    private static final String sqlGetByIdAndOfficeUnitOrganogramId = "SELECT * FROM users INNER join employee_offices "
            .concat("  on employee_offices.employee_record_id = users.employee_record_id ")
            .concat(" WHERE users.isDeleted = 0 and employee_offices.isDeleted = 0 and ")
            .concat(" employee_offices.status = 1 and users.ID = %d and office_unit_organogram_id = %d  and employee_offices.is_default_role = 1 ");

    private static final String sqlGetById = "SELECT * FROM users INNER JOIN employee_offices "
            .concat(" on employee_offices.employee_record_id = users.employee_record_id ")
            .concat(" WHERE users.isDeleted = 0 AND employee_offices.isDeleted = 0 AND employee_offices.status = 1  and employee_offices.is_default_role = 1 ")
            .concat(" AND employee_offices.isDefault = 1 AND users.ID = %d");

    private static final String sqlAllId = "SELECT users.id FROM users INNER JOIN employee_offices "
            .concat(" on employee_offices.employee_record_id = users.employee_record_id ")
            .concat(" WHERE users.isDeleted = 0 AND employee_offices.isDeleted = 0 AND employee_offices.status = 1  and employee_offices.is_default_role = 1 ")
            .concat(" AND employee_offices.isDefault = 1 order by employee_offices.lastModificationTime desc");

    private static final String sqlRoleSearch = "SELECT distinct users.id FROM users INNER JOIN employee_offices "
            .concat(" on employee_offices.employee_record_id = users.employee_record_id ")
            .concat(" join employee_records on employee_records.id = users.employee_record_id ")
            .concat(" WHERE users.isDeleted = 0 and users.active = 1 AND employee_records.status = 1 "
                    + "AND employee_offices.isDeleted = 0 AND employee_offices.status = 1  and employee_offices.is_default_role = 1 ")
            .concat(" AND employee_offices.isDefault = 1 and employee_records.isDeleted = 0");

    private static final String sqlGetByUserName = "SELECT * FROM users INNER JOIN employee_offices "
            .concat(" ON employee_offices.employee_record_id = users.employee_record_id ")
            .concat(" WHERE users.isDeleted = 0 AND employee_offices.isDeleted = 0 AND employee_offices.status = 1  and employee_offices.is_default_role = 1 ")
            .concat(" AND users.userName = ?");

    private static final String sqlGetByUserNameFromOnlyUsers = "SELECT * FROM users "
            .concat(" WHERE users.isDeleted = 0 ")
            .concat(" AND users.userName = ?");

    private static final String sqlGetByMailAddress = "SELECT * FROM users INNER JOIN employee_offices"
            .concat("  ON employee_offices.employee_record_id = users.employee_record_id")
            .concat(" WHERE users.isDeleted = 0 and employee_offices.isDeleted = 0 and employee_offices.status = 1 and employee_offices.is_default_role = 1 and ")
            .concat(" users.mailAddress = ?");

    private static final String sqlGetByPhoneNo = "SELECT * FROM users INNER JOIN employee_offices"
            .concat("  ON employee_offices.employee_record_id = users.employee_record_id")
            .concat(" WHERE users.isDeleted = 0 and employee_offices.isDeleted = 0 and employee_offices.status = 1 and employee_offices.is_default_role = 1 and ")
            .concat(" users.phoneNo = ?");

    private static final String sqlGetByEmployeeRecordId = "SELECT * FROM users WHERE employee_record_id = %d and active = 1 and isDeleted = 0 ";

    private static final String sqlGetByEmployeeRecordIdActiveOrNot = "SELECT * FROM users WHERE employee_record_id = %d and isDeleted = 0 and username = %s ";

    private static final String sqlGetIdByEmployeeRecordId = "SELECT id FROM users WHERE employee_record_id = %d and active=1 and isDeleted=0   ";

    private static final String updateUserOisfQuery = "UPDATE user_organogram  SET user_type=?,role_id=?,languageID=?,designation=?, lastModificationTime=?,isDeleted=? WHERE organogram_id = ?";

    private static final String getDTOsByIdsNotSorted = "SELECT * FROM users INNER JOIN employee_offices "
            .concat(" ON employee_offices.employee_record_id = users.employee_record_id ")
            .concat(" WHERE employee_offices.isdefault = 1 AND  employee_offices.is_default_role = 1 and ")
            .concat(" users.isDeleted = 0 AND employee_offices.isDeleted = 0 AND employee_offices.status = 1 AND users.ID in (%s)");

    private static final String getMaxUserNameByPrefix = "select max(username) from users where username like '{prefix}%'";

    private static final String updateSuperAdminPassword = "UPDATE users SET password = '%s',lastModificationTime=%d WHERE username = 'superadmin'";

    public void updateSuperAdminPassword(String pw) {
        ConnectionAndStatementUtil.getWriteStatement(st -> {
            try {
                String encPw = PasswordUtil.getInstance().encrypt(pw);
                long lastModificationTime = System.currentTimeMillis() + 60000;
                String sql = String.format(updateSuperAdminPassword, encPw, lastModificationTime);
                logger.debug(sql);
                st.executeUpdate(sql);
                recordUpdateTime("users", lastModificationTime);
                UserRepository.cacheUpdateByUsername("superadmin");
            } catch (Exception e) {
                logger.error("",e);
            }
        });
    }

    public List<UserDTO> getDTOsNotSorted(List<Long> ids) {
        if (ids.isEmpty()) {
            return new ArrayList<>();
        }
        return ConnectionAndStatementUtil.getDTOListByNumbers(getDTOsByIdsNotSorted, ids, this::buildUserDTOFromRS);
    }

    public void addUser(UserDTO userDTO) throws Exception {
        UserDTO existingUserDTO = UserRepository.getUserDTOByUserName(userDTO.userName);
        if (existingUserDTO != null && existingUserDTO.isDeleted == 0) {
            throw new RequestFailureException("Username already exists. Please use another username.");
        }
        userDTO.ID = DBMW.getInstance().getNextSequenceId("users");
        userDTO.isDeleted = 0;
        ConnectionAndStatementUtil.getWriteConnectionAndPrepareStatement(model -> {
            PreparedStatement ps = (PreparedStatement) model.getStatement();
            try {
                long currentTime = System.currentTimeMillis();
                int index = 0;
                ps.setObject(++index, userDTO.ID);
                ps.setObject(++index, userDTO.userName);
                ps.setObject(++index, userDTO.password);
                ps.setObject(++index, userDTO.userType);
                ps.setObject(++index, userDTO.roleID);
                ps.setObject(++index, currentTime);
                ps.setObject(++index, userDTO.isDeleted);
                ps.setObject(++index, userDTO.mailAddress);
                ps.setObject(++index, userDTO.fullName);
                ps.setObject(++index, userDTO.phoneNo);
                ps.setObject(++index, userDTO.centreType);
                ps.setObject(++index, userDTO.nid);
                ps.setObject(++index, userDTO.birthRegistrationNo);
                ps.setObject(++index, userDTO.dateOfBirth);
                ps.execute();
                recordUpdateTime(model.getConnection(), "users", currentTime);
            } catch (SQLException ex) {
                logger.error(ex);
            }
        }, addUserQuery);
    }

    public void updatePersonalInfo(UserDTO userDTO, long lastModificationTime) {

        ConnectionAndStatementUtil.getWriteConnectionAndPrepareStatement(model -> {
            PreparedStatement ps = (PreparedStatement) model.getStatement();
            try {
                int index = 0;
                ps.setObject(++index, userDTO.fullName);
                ps.setObject(++index, userDTO.dateOfBirth);
                ps.setObject(++index, lastModificationTime);
                ps.setObject(++index, userDTO.ID);
                ps.executeUpdate();
                recordUpdateTime(model.getConnection(), "users", lastModificationTime);
            } catch (SQLException ex) {
                logger.error(ex);
            }
        }, updatePersonalInfo);
    }

    public void updateContactInfo(UserDTO userDTO, long lastModificationTime) {

        ConnectionAndStatementUtil.getWriteConnectionAndPrepareStatement(model -> {
            PreparedStatement ps = (PreparedStatement) model.getStatement();
            try {
                int index = 0;
                ps.setObject(++index, userDTO.phoneNo);
                ps.setObject(++index, userDTO.mailAddress);
                ps.setObject(++index, lastModificationTime);
                ps.setObject(++index, userDTO.ID);
                ps.executeUpdate();
                recordUpdateTime(model.getConnection(), "users", lastModificationTime);
            } catch (SQLException ex) {
                logger.error(ex);
            }
        }, updateContactInfo);
    }

    public void updateBCNInfo(UserDTO userDTO, long lastModificationTime) {

        ConnectionAndStatementUtil.getWriteConnectionAndPrepareStatement(model -> {
            PreparedStatement ps = (PreparedStatement) model.getStatement();
            try {
                int index = 0;
                ps.setObject(++index, userDTO.birthRegistrationNo);
                ps.setObject(++index, lastModificationTime);
                ps.setObject(++index, userDTO.ID);
                ps.executeUpdate();
                recordUpdateTime(model.getConnection(), "users", lastModificationTime);
            } catch (SQLException ex) {
                logger.error(ex);
            }
        }, updateBCNInfo);
    }

    private static final String activateSql = "UPDATE users SET active= ?, lastModificationTime= ? WHERE ID = ?";
    public void activate(UserDTO userDTO, long lastModificationTime) throws Exception {
        logger.debug("############### userDTO.active = " + userDTO.active);
        ConnectionAndStatementUtil.getWriteConnectionAndPrepareStatement(model -> {
            PreparedStatement ps = (PreparedStatement) model.getStatement();
            try {
                int index = 0;
                ps.setObject(++index, userDTO.active);
                ps.setObject(++index, lastModificationTime);
                ps.setObject(++index, userDTO.ID);
                logger.info(ps);
                ps.executeUpdate();
                recordUpdateTime(model.getConnection(), "users", lastModificationTime);
            } catch (SQLException ex) {
                logger.error(ex);
            }
        }, activateSql);
    }

    public void updateNIDInfo(UserDTO userDTO, long lastModificationTime) {

        ConnectionAndStatementUtil.getWriteConnectionAndPrepareStatement(model -> {
            PreparedStatement ps = (PreparedStatement) model.getStatement();
            try {
                int index = 0;
                ps.setObject(++index, userDTO.nid);
                ps.setObject(++index, lastModificationTime);
                ps.setObject(++index, userDTO.ID);
                ps.executeUpdate();
                recordUpdateTime(model.getConnection(), "users", lastModificationTime);
            } catch (SQLException ex) {
                logger.error(ex);
            }
        }, updateNIDInfo);
    }

    public void updateUsernameInfo(UserDTO userDTO, long lastModificationTime) {

        ConnectionAndStatementUtil.getWriteConnectionAndPrepareStatement(model -> {
            PreparedStatement ps = (PreparedStatement) model.getStatement();
            try {
                int index = 0;
                ps.setObject(++index, userDTO.userName);
                ps.setObject(++index, lastModificationTime);
                ps.setObject(++index, userDTO.ID);
                ps.executeUpdate();
                recordUpdateTime(model.getConnection(), "users", lastModificationTime);
            } catch (SQLException ex) {
                logger.error(ex);
            }
        }, updateUserNameInfo);
    }

    public void updateUserPassword(UserDTO userDTO) {
        UserDTO existingUserDTO = UserRepository.getInstance().getUserDTOByUserName(userDTO.userName);
        if (existingUserDTO != null && existingUserDTO.ID != userDTO.ID && (existingUserDTO.isDeleted != 1)) {
            throw new RequestFailureException("Username already exists. Please use another username.");
        }

        ConnectionAndStatementUtil.getWriteConnectionAndPrepareStatement(model -> {
            PreparedStatement ps = (PreparedStatement) model.getStatement();
            try {
                long currentTime = System.currentTimeMillis();
                int index = 0;
                ps.setObject(++index, userDTO.password);
                ps.setObject(++index, currentTime);
                ps.setObject(++index, userDTO.ID);
                ps.executeUpdate();
                recordUpdateTime(model.getConnection(), "users", currentTime);
            } catch (SQLException ex) {
                logger.error(ex);
            }
        }, updatePasswordQuery);
    }

    public void updateOTP(UserDTO userDTO) {
        ConnectionAndStatementUtil.getWriteConnectionAndPrepareStatement(model -> {
            PreparedStatement ps = (PreparedStatement) model.getStatement();
            try {
                long currentTime = System.currentTimeMillis();
                int index = 0;
                ps.setObject(++index, userDTO.otp);
                ps.setObject(++index, userDTO.otpRequestTime);
                ps.setObject(++index, userDTO.otpExpireTime);
                ps.setObject(++index, userDTO.otpAttemptCount);
                ps.setObject(++index, currentTime);
                ps.setObject(++index, userDTO.ID);
                ps.executeUpdate();
                recordUpdateTime(model.getConnection(), "users", currentTime);
            } catch (SQLException ex) {
                logger.error(ex);
            }
        }, updateOTPQuery);
    }

    public long getEmployeeRecordID(long ID) {
        String sql = String.format(sqlGetEmployeeRecordIdById, ID);
        Long value = ConnectionAndStatementUtil.getT(sql, rs -> {
            try {
                return rs.getLong("employee_record_id");
            } catch (SQLException ex) {
                logger.error(ex);
                return null;
            }
        }, null);
        return value == null ? -1 : value;
    }

    public UserDTO getUserDTOByOrganogramID(long ID) {
        String sql = String.format(sqlGetUsersByOfficeUnitOrganogramId, ID);
        return ConnectionAndStatementUtil.getT(sql, this::buildUserDTOFromRS, null);
    }

    public List<UserDTO> getUserDTOByOrganogramIDList(List<Long> list) {
        return ConnectionAndStatementUtil.getDTOListByNumbers(sqlGetUsersByOfficeUnitOrganogramIdList, list,this::buildUserDTOFromRS);
    }

    public UserDTO getUserDTOByOrganogramID(long organogramId, long userId) {
        String sql = String.format(sqlGetUsersByOfficeUnitOrganogramIdAndUserId, organogramId, userId);
        return ConnectionAndStatementUtil.getT(sql, this::buildUserDTOFromRS, null);
    }

    public void updateUserOisf(UserDTO userDTO) {
        UserDTO existingUserDTO = UserRepository.getInstance().getUserDTOByUserName(userDTO.userName);
        if (existingUserDTO != null && existingUserDTO.ID != userDTO.ID && existingUserDTO.isDeleted == 0) {
            throw new RequestFailureException("Username already exists. Please use another username.");
        }
        ConnectionAndStatementUtil.getWriteConnectionAndPrepareStatement(model -> {
            PreparedStatement ps = (PreparedStatement) model.getStatement();
            try {
                long currentTime = System.currentTimeMillis();
                int index = 0;
                ps.setObject(++index, userDTO.userType);
                ps.setObject(++index, userDTO.roleID);
                ps.setObject(++index, userDTO.languageID);
                ps.setObject(++index, userDTO.userTypeName);
                ps.setObject(++index, currentTime);
                ps.setObject(++index, userDTO.isDeleted);
                ps.setObject(++index, userDTO.organogramID);
                ps.executeUpdate();
                recordUpdateTime(model.getConnection(), "user_organogram", currentTime);
            } catch (SQLException ex) {
                logger.error(ex);
            }
        }, updateUserOisfQuery);
    }

    public UserDTO getUserDTOByUserIDandOrganogramId(long userId, long organogramId) {
        String sql = String.format(sqlGetByIdAndOfficeUnitOrganogramId, userId, organogramId);
        return ConnectionAndStatementUtil.getT(sql, this::buildUserDTOFromRS);
    }

    public UserDTO getUserDTOByUserID(long ID) {
        String sql = String.format(sqlGetById, ID);
        return ConnectionAndStatementUtil.getT(sql, this::buildUserDTOFromRS);
    }

    public UserDTO getByIdDeletedOrNot(long ID) {
        String sql = String.format(getByIdDeletedOrNot, ID);
        return ConnectionAndStatementUtil.getT(sql, this::buildUserDTOFromUsersTableRS);
    }

    public void updateUser(UserDTO userDTO) {
        UserDTO existingUserDTO = UserRepository.getUserDTOByUserName(userDTO.userName);
        if (existingUserDTO != null && existingUserDTO.ID != userDTO.ID && existingUserDTO.isDeleted == 0) {
            throw new RequestFailureException("Username already exists. Please use another username.");
        }
        ConnectionAndStatementUtil.getWriteConnectionAndPrepareStatement(model -> {
            PreparedStatement ps = (PreparedStatement) model.getStatement();
            try {
                long currentTime = System.currentTimeMillis();
                int index = 0;
                ps.setObject(++index, userDTO.userName);
                ps.setObject(++index, userDTO.password);
                ps.setObject(++index, userDTO.userType);
                ps.setObject(++index, userDTO.roleID);
                ps.setObject(++index, userDTO.languageID);
                ps.setObject(++index, currentTime);
                ps.setObject(++index, userDTO.isDeleted);
                ps.setObject(++index, userDTO.mailAddress);
                ps.setObject(++index, userDTO.fullName);
                ps.setObject(++index, userDTO.phoneNo);
                ps.setObject(++index, userDTO.centreType);
                ps.setObject(++index, userDTO.otpSMS);
                ps.setObject(++index, userDTO.otpEmail);
                ps.setObject(++index, userDTO.otpPushNotification);
                ps.setObject(++index, userDTO.ID);
                logger.debug(ps);
                ps.executeUpdate();
                recordUpdateTime(model.getConnection(), "users", currentTime);
            } catch (SQLException ex) {
                logger.error(ex);
            }
        }, updateUserQuery);
    }

    public void updateUserTypeAndRole(UserDTO userDTO, long lastModificationTime) throws Exception {
        UserDTO existingUserDTO = UserRepository.getInstance().getUserDTOByUserID(userDTO.ID);
        if (existingUserDTO == null || existingUserDTO.isDeleted != 0) {
            throw new RequestFailureException("No user found with ID - " + userDTO.ID);
        }

        System.out.println("updateUserTypeAndRole called");
        ConnectionAndStatementUtil.getWriteConnectionAndPrepareStatement(model -> {
            PreparedStatement ps = (PreparedStatement) model.getStatement();
            try {
                long currentTime = System.currentTimeMillis();
                int index = 0;
                ps.setObject(++index, userDTO.userType);
                ps.setObject(++index, userDTO.roleID);
                ps.setObject(++index, currentTime);
                ps.setObject(++index, userDTO.ID);
                recordUpdateTime(model.getConnection(), "users", currentTime);
            } catch (SQLException ex) {
                logger.error(ex);
            }
        }, updateUserTypeAndRoleQuery);

        setRole(userDTO, lastModificationTime);
    }

    public void setRole(UserDTO userDTO, long lastModificationTime) throws Exception {
        AtomicReference<Exception> ar = new AtomicReference<>();
        ConnectionAndStatementUtil.getWriteStatement(st -> {
            String sql = "Update employee_offices set organogram_role = " + userDTO.roleID + ", lastModificationTime=" + lastModificationTime
                    + " where office_unit_organogram_id = " + userDTO.organogramID + " and employee_record_id = " + userDTO.employee_record_id;
            logger.debug("sql : " + sql);
            try {
                st.executeUpdate(sql);
            } catch (SQLException e) {
                logger.error("",e);
                ar.set(e);
            }
        }, CommonDAOService.CONNECTION_THREAD_LOCAL.get());
        if (ar.get() != null) {
            throw ar.get();
        }
    }

    public void deleteUserByUserID(long ID) {
        ConnectionAndStatementUtil.getWriteConnectionAndPrepareStatement(model -> {
            PreparedStatement ps = (PreparedStatement) model.getStatement();
            try {
                long currentTime = System.currentTimeMillis();
                int index = 0;
                ps.setObject(++index, currentTime);
                ps.setObject(++index, ID);
                recordUpdateTime(model.getConnection(), "users", currentTime);
            } catch (SQLException ex) {
                logger.error(ex);
            }
        }, deleteUserQuery);
    }

    public List<UserDTO> getOtherUserDTOsByUserId(long userId, long organogramId) {
        String sql = "SELECT * FROM " + userTable
                + " join employee_offices on employee_offices.employee_record_id = " + userTable + ".employee_record_id"
                + " WHERE ";
        if (organogramId != -2) {
            sql += "office_unit_organogram_id !=" + organogramId + " and ";
        }

        sql += " " + userTable + ".isDeleted = 0 and employee_offices.isDeleted = 0 and employee_offices.status = 1 and "
                + userTable + ".ID = " + userId;
        return ConnectionAndStatementUtil.getListOfT(sql, this::buildUserDTOFromRS);
    }

    public List<UserDTO> getOtherUserDTOsByUserIdAndRoleId(long userId, long roleId, long organogramId) {
        String sql = "SELECT * FROM " + userTable
                + " join employee_offices on employee_offices.employee_record_id = " + userTable + ".employee_record_id"
                + " WHERE ";

        sql += " (organogram_role !=" + roleId + " or office_unit_organogram_id != " + organogramId + ") and ";


        sql += " " + userTable + ".isDeleted = 0 and employee_offices.isDeleted = 0 and employee_offices.status = 1 and "
                + userTable + ".ID = " + userId;
        sql += " and 1 in (select status from employee_offices eo where eo.employee_record_id = users.employee_record_id and eo.is_default_role = 1\r\n" +
                "    and eo.office_unit_organogram_id = employee_offices.office_unit_organogram_id)";
        return ConnectionAndStatementUtil.getListOfT(sql, this::buildUserDTOFromRS);
    }


    public List<UserDTO> getrUserDTOsByUserIdAndOrgId(long userId, long organogramId) {
        String sql = "SELECT * FROM " + userTable
                + " join employee_offices on employee_offices.employee_record_id = " + userTable + ".employee_record_id"
                + " WHERE ";
        sql += " (  office_unit_organogram_id = " + organogramId + ") and ";


        sql += " " + userTable + ".isDeleted = 0 and employee_offices.isDeleted = 0 and employee_offices.status = 1 and "
                + userTable + ".ID = " + userId;
        return ConnectionAndStatementUtil.getListOfT(sql, this::buildUserDTOFromRS);
    }

    public List<UserDTO> getNonDefaultUserDTOsByUserId(long userId) {
        String sql = "SELECT * FROM " + userTable
                + " join employee_offices on employee_offices.employee_record_id = " + userTable + ".employee_record_id"
                + " WHERE employee_offices.is_default_role = 0 and ";
        sql += " " + userTable + ".isDeleted = 0 and employee_offices.isDeleted = 0 and employee_offices.status = 1 and "
                + userTable + ".ID = " + userId;
        return ConnectionAndStatementUtil.getListOfT(sql, this::buildUserDTOFromRS);
    }

    public List<UserDTO> getNonDefaultUserDTOsByUserIdAndOrgId(long userId, long organogramId) {
        String sql = "SELECT * FROM " + userTable
                + " join employee_offices on employee_offices.employee_record_id = " + userTable + ".employee_record_id"
                + " WHERE employee_offices.is_default_role = 0 and ";
        sql += " (  office_unit_organogram_id = " + organogramId + ") and ";
        sql += " " + userTable + ".isDeleted = 0 and employee_offices.isDeleted = 0 and employee_offices.status = 1 and "
                + userTable + ".ID = " + userId;
        return ConnectionAndStatementUtil.getListOfT(sql, this::buildUserDTOFromRS);
    }

    public UserDTO getUserDTOByUserIdAndRoleId(long userId, long roleId, long organogramId) {
        String sql = "SELECT * FROM " + userTable
                + " join employee_offices on employee_offices.employee_record_id = " + userTable + ".employee_record_id"
                + " WHERE ";

        sql += "organogram_role =" + roleId + " and office_unit_organogram_id = " + organogramId + " and ";


        sql += " " + userTable + ".isDeleted = 0 and employee_offices.isDeleted = 0 and employee_offices.status = 1 and "
                + userTable + ".ID = " + userId;
        return ConnectionAndStatementUtil.getT(sql, this::buildUserDTOFromRS);
    }

    private void get(UserDTO userDTO, ResultSet rs) {
        try {
            userDTO.ID = rs.getLong(userTable + ".ID");
            userDTO.userName = rs.getString("userName");
            userDTO.password = rs.getString("password");
            userDTO.userType = rs.getInt("userType");
            userDTO.roleID = rs.getInt("organogram_role");

            userDTO.languageID = rs.getInt("languageID");
            userDTO.isDeleted = rs.getInt("isDeleted");
            userDTO.mailAddress = rs.getString("mailAddress");
            userDTO.fullName = rs.getString("fullName");
            userDTO.phoneNo = rs.getString("phoneNo");
            userDTO.centreType = rs.getInt("centre_Type");
            userDTO.employee_record_id = rs.getInt("employee_record_id");
            userDTO.officeID = rs.getLong("office_id");
            userDTO.organogramID = rs.getLong("office_unit_organogram_id");
            userDTO.unitID = rs.getLong("office_unit_id");

            userDTO.otpSMS = rs.getBoolean("otpSMS");
            userDTO.otpEmail = rs.getBoolean("otpEmail");
            userDTO.otpPushNotification = rs.getBoolean("otpPushNotification");

            userDTO.nid = rs.getString("nid");
            userDTO.dateOfBirth = rs.getLong("date_of_birth");
            userDTO.active = rs.getBoolean("active");
        } catch (SQLException e) {
            logger.error("",e);
        }
    }

    private UserDTO buildUserDTOFromRSForOnlyUsers(ResultSet rs) {
        try {
            UserDTO userDTO = new UserDTO();
            userDTO.ID = rs.getLong(userTable + ".ID");
            userDTO.userName = rs.getString("userName");
            userDTO.password = rs.getString("password");
            userDTO.userType = rs.getInt("userType");
            userDTO.roleID = -1;

            userDTO.languageID = rs.getInt("languageID");
            userDTO.isDeleted = rs.getInt("isDeleted");
            userDTO.mailAddress = rs.getString("mailAddress");
            userDTO.fullName = rs.getString("fullName");
            userDTO.phoneNo = rs.getString("phoneNo");
            userDTO.centreType = rs.getInt("centre_Type");
            userDTO.employee_record_id = rs.getInt("employee_record_id");


            userDTO.nid = rs.getString("nid");
            userDTO.dateOfBirth = rs.getLong("date_of_birth");
            userDTO.otp = rs.getString("otp");
            userDTO.otpRequestTime = rs.getLong("otp_request_time");
            userDTO.otpExpireTime = rs.getLong("otp_expire_time");
            userDTO.otpAttemptCount = rs.getInt("otp_attempt_count");
            userDTO.active = rs.getBoolean("active");
            return userDTO;
        } catch (SQLException e) {
            logger.error("",e);
            return null;
        }
    }

    private UserDTO buildUserDTOFromRS(ResultSet rs) {
        try {
            UserDTO userDTO = new UserDTO();
            userDTO.ID = rs.getLong(userTable + ".ID");
            userDTO.userName = rs.getString("userName");
            userDTO.password = rs.getString("password");
            userDTO.userType = rs.getInt("userType");
            userDTO.roleID = rs.getInt("organogram_role");
            userDTO.languageID = rs.getInt("languageID");
            userDTO.isDeleted = rs.getInt("isDeleted");
            userDTO.mailAddress = rs.getString("mailAddress");
            userDTO.fullName = rs.getString("fullName");
            userDTO.phoneNo = rs.getString("phoneNo");
            userDTO.centreType = rs.getInt("centre_Type");
            userDTO.employee_record_id = rs.getInt("employee_record_id");
            userDTO.otpSMS = rs.getBoolean("otpSMS");
            userDTO.otpEmail = rs.getBoolean("otpEmail");
            userDTO.otpPushNotification = rs.getBoolean("otpPushNotification");
            userDTO.nid = rs.getString("nid");
            userDTO.dateOfBirth = rs.getLong("date_of_birth");
            userDTO.otp = rs.getString("otp");
            userDTO.otpRequestTime = rs.getLong("otp_request_time");
            userDTO.otpExpireTime = rs.getLong("otp_expire_time");
            userDTO.otpAttemptCount = rs.getInt("otp_attempt_count");
            userDTO.active = rs.getBoolean("active");
            userDTO.officeID = rs.getLong("office_id");
            userDTO.organogramID = rs.getLong("office_unit_organogram_id");
            userDTO.unitID = rs.getLong("office_unit_id");
            return userDTO;
        } catch (SQLException e) {
            logger.error("",e);
            return null;
        }
    }

    private UserDTO buildUserDTOFromUsersTableRS(ResultSet rs) {
        try {
            UserDTO userDTO = new UserDTO();
            userDTO.ID = rs.getLong("id");
            userDTO.userName = rs.getString("userName");
            userDTO.password = rs.getString("password");
            userDTO.userType = rs.getInt("userType");
            userDTO.roleID = rs.getInt("roleID");

            userDTO.languageID = rs.getInt("languageID");
            userDTO.isDeleted = rs.getInt("isDeleted");
            userDTO.mailAddress = rs.getString("mailAddress");
            userDTO.fullName = rs.getString("fullName");
            userDTO.phoneNo = rs.getString("phoneNo");
            userDTO.centreType = rs.getInt("centre_Type");
            userDTO.employee_record_id = rs.getInt("employee_record_id");

            userDTO.otpSMS = rs.getBoolean("otpSMS");
            userDTO.otpEmail = rs.getBoolean("otpEmail");
            userDTO.otpPushNotification = rs.getBoolean("otpPushNotification");

            userDTO.nid = rs.getString("nid");
            userDTO.dateOfBirth = rs.getLong("date_of_birth");
            userDTO.active = rs.getBoolean("active");
            return userDTO;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    public List<UserDTO> getUserDTOsByUsername(String userName) {
        return ConnectionAndStatementUtil.getListOfT(sqlGetByUserName, Collections.singletonList(userName), this::buildUserDTOFromRS);
    }

    public UserDTO getUserDTOByUsername(String userName) {
        return ConnectionAndStatementUtil.getT(sqlGetByUserName, Collections.singletonList(userName), this::buildUserDTOFromRS, null);
    }

    public UserDTO getUserDTOByUsernameFromOnlyUsers(String userName) {
        return ConnectionAndStatementUtil.getT(sqlGetByUserNameFromOnlyUsers, Collections.singletonList(userName), this::buildUserDTOFromRSForOnlyUsers);
    }

    public UserDTO getUserDTOByMail(String mailAddress) {
        return ConnectionAndStatementUtil.getT(sqlGetByMailAddress, Collections.singletonList(mailAddress), this::buildUserDTOFromRS, null);
    }

    public UserDTO getUserDTOByPhoneNo(String phoneNo) {
        return ConnectionAndStatementUtil.getT(sqlGetByPhoneNo, Collections.singletonList(phoneNo), this::buildUserDTOFromRS, null);
    }

    public UserDTO getUserDTOByEmployeeRecordId(long employeeRecordId) {
        String sql = String.format(sqlGetByEmployeeRecordId, employeeRecordId);
        return ConnectionAndStatementUtil.getT(sql, this::buildUserDTOFromUsersTableRS, null);
    }

    public UserDTO getUserDTOByEmployeeRecordIdActiveOrNot(long employeeRecordId, String employeeNumber) {
        String sql = String.format(sqlGetByEmployeeRecordIdActiveOrNot, employeeRecordId, employeeNumber);
        return ConnectionAndStatementUtil.getT(sql, this::buildUserDTOFromUsersTableRS, null);
    }

    public Long getIdByEmployeeRecordId(long employeeRecordId) {
        String sql = String.format(sqlGetIdByEmployeeRecordId, employeeRecordId);
        return ConnectionAndStatementUtil.getT(sql, rs -> {
            try {
                return rs.getLong("id");
            } catch (SQLException ex) {
                logger.error(ex);
                return 0L;
            }
        }, null);
    }

    public boolean hasOrganogramId(List<UserDTO> userDTOList, long organogramId) {
        for (UserDTO userDTO : userDTOList) {
            if (userDTO.organogramID == organogramId) {
                return true;
            }

        }
        return false;
    }

    public List<UserDTO> getDTOs(Collection recordIDs, LoginDTO loginDTO) {
        Connection connection = null;
        ResultSet rs;
        Statement stmt = null;
        List<UserDTO> userDTOList = new ArrayList<>();
        if (recordIDs.isEmpty()) {
            return userDTOList;
        }
        try {


            StringBuilder sql = new StringBuilder("SELECT * FROM " + userTable
                    + " join employee_offices on employee_offices.employee_record_id = " + userTable + ".employee_record_id"
                    + " WHERE ");

            sql.append(" " + userTable + ".isDeleted = 0 and employee_offices.isDeleted = 0 and employee_offices.status = 1  and employee_offices.is_default_role = 1 ");


            sql.append(" and " + userTable + ".ID in (");
            for (int i = 0; i < recordIDs.size(); i++) {
                if (i != 0) {
                    sql.append(",");
                }
                sql.append(((ArrayList) recordIDs).get(i));
            }
            sql.append(")");

            sql.append(" order by employee_offices.lastModificationTime desc");


            logger.debug("sql " + sql);
            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();


            rs = stmt.executeQuery(sql.toString());

            while (rs.next()) {
                UserDTO userDTO = new UserDTO();
                get(userDTO, rs);
                if (!hasOrganogramId(userDTOList, userDTO.organogramID)) {
                    userDTOList.add(userDTO);
                }
            }

        } catch (Exception ex) {
            logger.fatal("", ex);
            throw new RuntimeException();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception ignored) {
            }

            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ignored) {
            }
        }
        return userDTOList;

    }

    public Collection getIDs(LoginDTO loginDTO) {
        Collection data = new ArrayList();
        Connection connection = null;
        Statement stmt = null;
        ResultSet resultSet = null;

        try {
            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();

            for (resultSet = stmt.executeQuery(sqlAllId); resultSet.next(); data.add(resultSet.getString("ID"))) ;

            resultSet.close();
        } catch (Exception e) {
            logger.fatal("", e);
            throw new RuntimeException(e);
        } finally {
            try {
                if (resultSet != null && !resultSet.isClosed()) {
                    resultSet.close();
                }
            } catch (Exception ignored) {

            }
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception ignored) {
            }
            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception e) {
                logger.fatal("DAO finally :" + e);
            }
        }
        return data;
    }

    public Collection getIDsWithSearchCriteria(Hashtable p_searchCriteria, LoginDTO loginDTO) {
        Hashtable<String, String> searchCriteria = (Hashtable<String, String>) p_searchCriteria;
        List<Long> idList = new ArrayList<>();
        Connection connection = null;
        PreparedStatement ps = null;
        if (searchCriteria.get("userName") != null) {
            searchCriteria.put("userName", StringUtils.convertToEngNumber(searchCriteria.get("userName")));
        }
        try {
            boolean conditionAdded = false;
            List<Object> objectList = new ArrayList<>();
            String sql = sqlRoleSearch;
            conditionAdded = true;

            String userNameInputFromUI = searchCriteria.get("userName");
            if (userNameInputFromUI != null && !userNameInputFromUI.trim().equals("")) {
                if (conditionAdded) {
                    sql += " AND  ";
                } else {
                    sql += " WHERE  ";
                }
                conditionAdded = true;
                sql += "userName LIKE ? ";
                objectList.add("%" + Utils.getDigits(userNameInputFromUI, "english") + "%");
            }

            String userTypeInputFromUI = searchCriteria.get("userType");
            if (userTypeInputFromUI != null && !userTypeInputFromUI.trim().equals("")) {
                if (conditionAdded) {
                    sql += " AND  ";
                } else {
                    sql += " WHERE  ";
                }
                conditionAdded = true;
                sql += "userType LIKE ? ";
                objectList.add("%" + userTypeInputFromUI + "%");
            }
            String fullName = searchCriteria.get("fullName");
            if (fullName != null && fullName.trim().length() > 0) {
                fullName = fullName.trim();
                if (conditionAdded) {
                    sql += " AND  ";
                } else {
                    sql += " WHERE  ";
                }
                conditionAdded = true;
                sql += "( employee_records.name_eng LIKE ? ";
                objectList.add("%" + fullName + "%");
                sql += " or employee_records.name_bng LIKE ? )";
                objectList.add("%" + fullName + "%");
            }

            String sRole = searchCriteria.get("roleToSearch");
            if (sRole != null && sRole.trim().length() > 0) {
                int role = Integer.parseInt(sRole.trim());
                if (role != -1) {
                    if (conditionAdded) {
                        sql += " AND  ";
                    } else {
                        sql += " WHERE  ";
                    }
                    sql += " ? in (select organogram_role from employee_offices where employee_record_id = employee_records.id and isDeleted = 0 and status = 1)";
                    objectList.add(role);
                }

            }

            //sql += " order by employee_offices.lastModificationTime desc ";


            connection = DBMR.getInstance().getConnection();
            ps = connection.prepareStatement(sql);
            logger.debug("initial ps:" + ps);
            for (int i = 0; i < objectList.size(); i++) {
                ps.setObject(i + 1, objectList.get(i));
            }
            logger.debug(ps);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                idList.add(rs.getLong(1));
            }


        } catch (Exception ex) {
            logger.fatal("", ex);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception ignored) {
            }
            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ignored) {
            }
        }
        return idList;


    }

    public List<UserDTO> getAllUsersByOrganogramID(long office_unit_organogram_id) {
        String sql = String.format(sqlGetUsersByOfficeUnitOrganogramId, office_unit_organogram_id);
        return ConnectionAndStatementUtil.getListOfT(sql, this::buildUserDTOFromRS);
    }

    public List<UserDTO> getAllUsers(boolean isFirstReload) {
        String sql;
        if (isFirstReload) {
            sql = reloadAllUsersForTrue;
        } else {
            sql = String.format(reloadAllUsersForFalse, RepositoryManager.lastModifyTime, RepositoryManager.lastModifyTime);
        }
        return getUserDTOs(sql);
    }

    public List<UserDTO> getAllUsers(boolean isFirstReload, long lastModifyTime) {
        String sql;
        if (isFirstReload) {
            sql = reloadAllUsersForTrue;
        } else {
            sql = String.format(reloadAllUsersForFalse, RepositoryManager.lastModifyTime, RepositoryManager.lastModifyTime);
        }
        return getUserDTOs(sql);
    }

    private LoginIPDTO buildLoginIPDTOFromRS(ResultSet rs) {
        try {
            LoginIPDTO dto = new LoginIPDTO();
            dto.ID = rs.getLong("ID");
            dto.userId = rs.getLong("userID");
            dto.IPAddress = rs.getString("IP");
            return dto;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    public List<UserDTO> getAllDefaultUsersForFixedLastModificationTime(long lastModificationTime) {
        String sql = String.format(reloadAllDefaultUsersForFixedLastModificationTime, lastModificationTime, lastModificationTime);
        return getUserDTOs(sql);
    }

    public List<UserDTO> getAllDefaultUsers(boolean isFirstReload) {
        String sql;
        if (isFirstReload) {
            sql = reloadAllDefaultUsersForTrue;
        } else {
            sql = String.format(reloadAllDefaultUsersForFalse, RepositoryManager.lastModifyTime, RepositoryManager.lastModifyTime);
        }
        return getUserDTOs(sql);
    }

    public List<UserDTO> getAllUsersFromUsersTableForFixedLastModificationTime(long lastModificationTime) {
        String sql = String.format(reloadAllUsersFromUserTableForFixedLastModificationTime, lastModificationTime);
        return ConnectionAndStatementUtil.getListOfT(sql, this::buildUserDTOFromRSForOnlyUsers);
    }

    public List<UserDTO> getAllUsersFromUsersTable(boolean isFirstReload) {
        String sql;
        if (isFirstReload) {
            sql = reloadAllUsersFromUserTableForTrue;
        } else {
            sql = String.format(reloadAllUsersFromUserTableForFalse, RepositoryManager.lastModifyTime);
        }
        return ConnectionAndStatementUtil.getListOfT(sql, this::buildUserDTOFromRSForOnlyUsers);
    }

    private List<UserDTO> getUserDTOs(String sql) {
        List<UserDTO> userDTOList = ConnectionAndStatementUtil.getListOfT(sql, this::buildUserDTOFromRS);

        String userDTOIds = userDTOList.stream()
                .map(userDTO -> userDTO.ID)
                .map(String::valueOf)
                .collect(Collectors.joining(","));
        if (userDTOIds.length() > 0) {
            List<LoginIPDTO> loginIPDTOS = ConnectionAndStatementUtil.getListOfT(sqlQueryToGetIpsByUserIds, Collections.singletonList(userDTOIds), this::buildLoginIPDTOFromRS);

            Map<Long, List<String>> mapLoginIPDTOByUserId = loginIPDTOS.stream()
                    .collect(Collectors.groupingBy(dto -> dto.userId
                            , Collectors.mapping(dto -> dto.IPAddress, Collectors.toList())));

            userDTOList.forEach(dto -> {
                List<String> loginIPs = mapLoginIPDTOByUserId.get(dto.ID);
                if (loginIPs != null) {
                    dto.loginIPs.addAll(loginIPs);
                }
            });
        }
        return userDTOList;
    }

    public Long getUsersCountByIsDeletedNotOne() {
        return (Long) ConnectionAndStatementUtil.getT(sqlCountByNotDelete, rs -> {
            try {
                return rs.getLong("cnt");
            } catch (SQLException ex) {
                logger.error(ex);
                return 0;
            }
        });
    }

    private static final String updateUsersLastModificationTime = "UPDATE users SET lastModificationTime = %d WHERE ID = %d";

    public void updateLastModificationTime(long id, long lastModificationTime, Connection connection) throws Exception {
        String sql = String.format(updateUsersLastModificationTime, lastModificationTime, id);
        executeUpdate(sql, lastModificationTime, connection);
    }

    private static final String updateLastModificationTimeByEmpId =
            "UPDATE users SET lastModificationTime = %d WHERE employee_record_id = %d AND isDeleted = 0 AND active = 1";

    public void updateLastModificationTimeByEmpId(long empId, long lastModificationTime, Connection connection) throws Exception {
        String sql = String.format(updateLastModificationTimeByEmpId, lastModificationTime, empId);
        executeUpdate(sql, lastModificationTime, connection);
    }

    private void executeUpdate(String sql, long lastModificationTime, Connection connection) throws Exception {
        Utils.wrapWithAtomicReference(ar ->
                ConnectionAndStatementUtil.getWriteStatement(st -> {
                    try {
                        logger.debug(sql);
                        st.executeUpdate(sql);
                        recordUpdateTime(connection, "users", lastModificationTime);
                    } catch (SQLException e) {
                        logger.error("",e);
                        ar.set(e);
                    }
                }, connection)
        );
    }

    private UserDTO buildUserDTOFromUsersRS(ResultSet rs) {
        try {
            UserDTO userDTO = new UserDTO();
            userDTO.ID = rs.getLong("id");
            userDTO.userName = rs.getString("username");
            userDTO.employee_record_id = rs.getLong("employee_record_id");
            userDTO.roleID = rs.getLong("roleID");
            userDTO.languageID = rs.getInt("languageID");
            userDTO.userType = rs.getInt("userType");
            userDTO.fullName = rs.getString("fullName");
            userDTO.isDeleted = rs.getInt("isDeleted");
            userDTO.password = rs.getString("password");
            return userDTO;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return null;
        }
    }

    public UserDTO getByUserName(String userName) {
        return ConnectionAndStatementUtil.getT(getByUserName, Collections.singletonList(userName), this::buildUserDTOFromUsersRS);
    }

    public UserDTO getByUserNameDeletedOrNot(String userName) {
        return ConnectionAndStatementUtil.getT(getByUserNameDeletedOrNot, Collections.singletonList(userName), this::buildUserDTOFromUsersRS);
    }


    public List<UserDTO> getALlUsersForAPI() {
        String sql = "select * from users where isDeleted=0";
        return ConnectionAndStatementUtil.getListOfT(sql, this::buildUserDTOFromRS);
    }

    public static String getMaxUserNameByPrefix(String prefix) {
        return ConnectionAndStatementUtil.getT(getMaxUserNameByPrefix.replace("{prefix}", prefix), rs -> {
            try {
                return rs.getString(1);
            } catch (SQLException ex) {
                logger.error("",ex);
                return null;
            }
        }, null);
    }

    private static final String getIdsByEmpIds = "SELECT id FROM users WHERE employee_record_id IN (%s) AND isDeleted = 0 AND active = 1";

    public List<Long> getIdsByEmployeeIds(List<Long> empIds) {
        if (empIds == null || empIds.isEmpty()) {
            return new ArrayList<>();
        }
        return ConnectionAndStatementUtil.getDTOListByNumbers(getIdsByEmpIds, empIds, rs -> {
            try {
                return rs.getLong(1);
            } catch (SQLException ex) {
                return null;
            }
        });
    }

    private static final String deleteByIds = "UPDATE users SET isDeleted = 1,active = 0,lastModificationTime = %d WHERE id in (%s)";

    public void deleteByIds(List<Long> idList, long lastModificationTime) throws Exception {
        if (idList == null || idList.isEmpty()) {
            return;
        }
        AtomicReference<Exception> ar = new AtomicReference<>();
        ConnectionAndStatementUtil.getWriteStatement(st -> {
            String ids = idList.stream().map(String::valueOf).collect(Collectors.joining(","));
            String sql = String.format(deleteByIds, lastModificationTime, ids);
            logger.debug(sql);
            try {
                st.execute(sql);
                recordUpdateTime(CommonDAOService.CONNECTION_THREAD_LOCAL.get(), "users", lastModificationTime);
            } catch (SQLException e) {
                logger.error("",e);
                ar.set(e);
            }
        }, CommonDAOService.CONNECTION_THREAD_LOCAL.get());
        if (ar.get() != null) {
            throw ar.get();
        }
    }

    private static final String getByIds = "SELECT * FROM users WHERE id in (%s)";

    public List<UserDTO> getOnlyUsersByIds(List<Long> idList) {
        return ConnectionAndStatementUtil.getDTOListByNumbers(getByIds, idList, this::buildUserDTOFromRSForOnlyUsers);
    }
}