package user;


import com.google.gson.Gson;
import common.RequestFailureException;
import doctor_time_slot.Doctor_time_slotDAO;
import doctor_time_slot.Doctor_time_slotDTO;
import employee_offices.EmployeeOfficeDTO;
import employee_offices.EmployeeOfficeRepository;
import employee_offices.EmployeeOfficesDAO;
import employee_records.Employee_recordsRepository;
import language.LC;
import language.LM;
import login.LoginDTO;
import login.LoginService;
import login.RememberMeOptionDAO;
import mail.EmailService;
import mail.SendEmailDTO;
import pb.Utils;

import org.apache.log4j.Logger;
import permission.MenuConstants;
import role.PermissionRepository;
import role.RoleDTO;
import sessionmanager.SessionConstants;
import sms.SmsService;
import util.*;
import workflow.WorkflowController;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;


/**
 * @author Kayesh Parvez
 */
@WebServlet("/UserServlet")
public class UserServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(UserServlet.class);
    public final Gson gson = new Gson();
    public static final int OTP_LENGTH = 6;
    public static final int OTP_TIMEOUT_MINUTE = 5;
    public static final int MIN_PASSWORD_LENGTH = 8;
    EmployeeOfficesDAO employeeOfficesDAO = new EmployeeOfficesDAO();

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        if (loginDTO == null) {

            response.sendRedirect("LogoutServlet");
        }
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);

        String actionType = request.getParameter(ActionTypeConstant.ACTION_TYPE);
        switch (actionType) {
            case ActionTypeConstant.USER_GET_ADD_PAGE:
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.USER_ADD)) {
                    getAddPage(request, response);
                } else {
                    request.getRequestDispatcher(JSPConstant.ERROR_GLOBAL).forward(request, response);
                }
                break;
            case ActionTypeConstant.USER_GET_EDIT_PAGE:
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.USER_VIEW)) {
                    getEditPage(request, response);
                } else {
                    request.getRequestDispatcher(JSPConstant.ERROR_GLOBAL).forward(request, response);
                }

                break;
            case "changeUser":
                long userId = Long.parseLong(request.getParameter("userId"));
                long roleId = Long.parseLong(request.getParameter("roleId"));
                long organogramId = Long.parseLong(request.getParameter("organogramId"));
                logger.debug("changeUser called, prevUserId = " + loginDTO.userID + " newUserId = " + userId);

                try {

                    logger.debug(" userName = " + userDTO.userName + " pw = " + userDTO.password);
                    //logger.debug("New userName = " + newUserDTO.userName + " pw = " + newUserDTO.password);
                    //if(newUserDTO.userName.equals(userDTO.userName) && newUserDTO.password.equals(userDTO.password))
                    {
                        LoginDTO newLoginDTO = new LoginDTO();
                        //request.getSession().removeAttribute(SessionConstants.USER_LOGIN);

                        newLoginDTO.isOisf = 0;

                        newLoginDTO.userID = userId;

                        newLoginDTO.loginSourceIP = loginDTO.loginSourceIP;

                        newLoginDTO.roleId = roleId;

                        newLoginDTO.organogramId = organogramId;
                        //request.getSession().setAttribute(SessionConstants.USER_LOGIN, newLoginDTO);

                        RememberMeOptionDAO.getInstance().removeRememberMeOptionByUserID(loginDTO.userID);
                        request.getSession().invalidate();

                        LoginService service = new LoginService();
                        service.processValidLogin(newLoginDTO, userDTO.userName, request);

                        loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
                        logger.debug("now user id = " + loginDTO.userID);

                        String homeUrl = "home/index.jsp";
                        logger.debug("#############################Getting index from userservlet##############################");
                        RequestDispatcher rd = request.getRequestDispatcher(homeUrl);
                        rd.forward(request, response);
                    }
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                break;
            case ActionTypeConstant.USER_SEARCH:
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.USER_SEARCH)) {
                    searchUser(request, response);
                } else {
                    request.getRequestDispatcher(JSPConstant.ERROR_GLOBAL).forward(request, response);
                }

                break;
            case ActionTypeConstant.USER_GET_CHANGE_PASSWORD:
                //if(PermissionRepository.getInstance().checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.USER_CHANGE_PASSWORD)){
                UserChangePassword(request, response);

                break;
        }

    }

    private void getAddPage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute(ServletConstant.ROLE_LIST, PermissionRepository.getAllRoles());//make Servlet constant
        RequestDispatcher requestDispatcher = request.getRequestDispatcher(JSPConstant.USER_EDIT);
        requestDispatcher.forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        String actionType = request.getParameter(ActionTypeConstant.ACTION_TYPE);
        logger.debug("actiontype found in userServlet: " + actionType);
        switch (actionType) {
            case "pinCodeRetrieve":
                pinCodeRetrieve(request, response);

                break;
            case "sendOTP":
            case "resendOTP":
                sendOTP(request, response);
                return;
            case "validatePasswordOTP":
                validatePasswordOTP(request, response);
                return;
        }
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);

        if (actionType.equals(ActionTypeConstant.USER_ADD)) {
            if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.USER_ADD)) {
                addUser(request, response);
            } else {
                request.getRequestDispatcher(JSPConstant.ERROR_GLOBAL).forward(request, response);
            }
        }
        if (actionType.equals("addFromRecruitmentRegistration")) {
//			addUserFromrecruitmentRegistration(request, response);

//			if(PermissionRepository.getInstance().checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.USER_ADD)){
//				addUserFromSecondLogin(request, response);
//			}else{
//				request.getRequestDispatcher(JSPConstant.ERROR_GLOBAL).forward(request, response);
//			}
        } else if (actionType.equals(ActionTypeConstant.USER_EDIT)) {

            if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.USER_UPDATE)) {
                //updateUser(request, response);
                updateUserTypeAndRole(request, response);
            } else {
                request.getRequestDispatcher(JSPConstant.ERROR_GLOBAL).forward(request, response);
            }
        }
        switch (actionType) {
            case ActionTypeConstant.USER_DELETE:
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.USER_DELETE)) {
                    deleteUser(request, response);
                } else {
                    request.getRequestDispatcher(JSPConstant.ERROR_GLOBAL).forward(request, response);
                }
                break;
            case ActionTypeConstant.USER_SEARCH:
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.USER_SEARCH)) {
                    searchUser(request, response);
                } else {
                    request.getRequestDispatcher(JSPConstant.ERROR_GLOBAL).forward(request, response);
                }
                break;
            case ActionTypeConstant.USER_CHANGE_PASSWORD:
                //if(PermissionRepository.getInstance().checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.USER_CHANGE_PASSWORD)){
                ChangePassUser(request, response);
                break;
        }

    }

    private void addUser(HttpServletRequest request, HttpServletResponse response) {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        try {
            UserDTO userDTO = new UserDTO();
            userDTO.userName = request.getParameter(ServletConstant.USERNAME);
            userDTO.password = request.getParameter(ServletConstant.PASSWORD);
            userDTO.userType = Integer.parseInt(request.getParameter(ServletConstant.USER_TYPE));
            userDTO.roleID = Integer.parseInt(request.getParameter(ServletConstant.ROLE_NAME));
            userDTO.mailAddress = request.getParameter(ServletConstant.MAIL_ADDRESS);
            userDTO.fullName = request.getParameter(ServletConstant.FULL_NAME);
            userDTO.phoneNo = request.getParameter(ServletConstant.PHONE_NO);
            userDTO.centreType = Integer.parseInt(request.getParameter("CentreType"));
            userDTO.nid = request.getParameter("nid");
            userDTO.birthRegistrationNo = request.getParameter("birth_registration_no");
            userDTO.dateOfBirth = Long.parseLong(request.getParameter("date_of_birth"));
            if (!isUserDTOValid(userDTO, request, response)) {
                request.getSession().setAttribute(ServletConstant.USER_DTO, userDTO);
                throw new RequestFailureException(LM.getText(LC.USER_ADD_ERROR_USER_ADD, loginDTO));
            }
            userDTO.password = PasswordUtil.getInstance().encrypt(userDTO.password);
            UserDAO userDAO = new UserDAO();
            userDAO.addUser(userDTO);
            request.getSession().setAttribute(ServletConstant.SUCCESSFUL_MSG, LC.USER_ADD_SUCCESS_USER_ADD);

            response.sendRedirect(JSPConstant.USER_SEARCH_SERVLET);
        } catch (Exception e) {
            logger.debug("", e);
            throw new RequestFailureException(LM.getText(LC.USER_ADD_ERROR_USER_ADD, loginDTO), e instanceof RequestFailureException ? null : e);
        }
    }

    //	private void addUserFromrecruitmentRegistration(HttpServletRequest request, HttpServletResponse response) throws IOException {
//		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
//		try {
//			UserDTO userDTO = new UserDTO();
//			userDTO.userName="880" + request.getParameter(ServletConstant.PHONE_NO);
//			userDTO.password= PasswordGenerator.getRandomNumberPassword(6);
//			String password = userDTO.password;
//			userDTO.userType=Integer.parseInt(request.getParameter(ServletConstant.USER_TYPE));
////			userDTO.userType=1;
//			userDTO.roleID=10007;
//			userDTO.mailAddress=request.getParameter(ServletConstant.MAIL_ADDRESS);
//			userDTO.fullName=request.getParameter(ServletConstant.FULL_NAME);
//			userDTO.phoneNo="880" + request.getParameter(ServletConstant.PHONE_NO);
//			userDTO.centreType=3;
//			userDTO.nid=request.getParameter("nid");
//			userDTO.birthRegistrationNo=request.getParameter("birth_registration_no");
//			userDTO.languageID=CommonConstant.Language_ID_Bangla;
//			String dateOfBirthString  = request.getParameter("date_of_birth");
//
//			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
//			long dateOfBirth = simpleDateFormat.parse(dateOfBirthString).getTime();
//
//			userDTO.dateOfBirth=dateOfBirth;
//			if(!isUserDTOValid(userDTO, request, response))
//			{
//				request.getSession().setAttribute(ServletConstant.USER_DTO, userDTO);
//				throw new RequestFailureException(LM.getText(LC.USER_ADD_ERROR_USER_ADD, loginDTO));
//			}
//			userDTO.password= PasswordUtil.getInstance().encrypt(userDTO.password);
////			userDTO.password= PasswordUtil.getInstance().encrypt("123456");
//			UserDAO userDAO = new UserDAO();
//
//			UserDTO existingUser = userDAO.getUserDTOByUsername(userDTO.phoneNo);
//			if (existingUser != null) {
//				PrintWriter out = response.getWriter();
//				out.write("Duplicate");
//				out.close();
//			}
//
//			else {
//
//				long userId = userDAO.addUser(userDTO);
//				request.getSession().setAttribute(ServletConstant.SUCCESSFUL_MSG, LC.USER_ADD_SUCCESS_USER_ADD);
//
//				Parliament_job_applicantDAO parliament_job_applicantDAO = new Parliament_job_applicantDAO();
//
//				Parliament_job_applicantDTO parliament_job_applicantDTO = new Parliament_job_applicantDTO();
//				parliament_job_applicantDTO.nameBn = userDTO.fullName;
//				parliament_job_applicantDTO.nameEn = request.getParameter("nameEn");
//				parliament_job_applicantDTO.dateOfBirth = userDTO.dateOfBirth;
//				parliament_job_applicantDTO.email = userDTO.mailAddress;
//				parliament_job_applicantDTO.contactNumber = userDTO.phoneNo;
//				parliament_job_applicantDTO.userId = userId;
//
//				long jobApplicationId = parliament_job_applicantDAO.add(parliament_job_applicantDTO);
//
//				Job_applicant_photo_signatureDAO job_applicant_photo_signatureDAO = new Job_applicant_photo_signatureDAO();
//
//				Job_applicant_photo_signatureDTO job_applicant_photo_signatureDTO = new Job_applicant_photo_signatureDTO();
//				job_applicant_photo_signatureDTO.jobApplicantId = jobApplicationId;
//				job_applicant_photo_signatureDTO.photoOrSignature = 1;
//				job_applicant_photo_signatureDTO.filesDropzone = DBMW.getInstance().getNextSequenceId("fileid");
//				job_applicant_photo_signatureDTO.insertionDate = new Date().getTime();
//				job_applicant_photo_signatureDTO.insertedByUserId = userDTO.ID;
//				job_applicant_photo_signatureDAO.add(job_applicant_photo_signatureDTO);
//
//				job_applicant_photo_signatureDTO = new Job_applicant_photo_signatureDTO();
//				job_applicant_photo_signatureDTO.jobApplicantId = jobApplicationId;
//				job_applicant_photo_signatureDTO.photoOrSignature = 2;
//				job_applicant_photo_signatureDTO.filesDropzone = DBMW.getInstance().getNextSequenceId("fileid");
//				job_applicant_photo_signatureDTO.insertionDate = new Date().getTime();
//				job_applicant_photo_signatureDTO.insertedByUserId = userDTO.ID;
//				job_applicant_photo_signatureDAO.add(job_applicant_photo_signatureDTO);
//
//			SmsService.send(parliament_job_applicantDTO.contactNumber, String.format("Dear " + parliament_job_applicantDTO.nameBn + ", " + "your PRP registration is successful. Please log in with Username: " + userDTO.phoneNo + " , and Password: " + password));
//
//				request.getSession().invalidate();
//				response.sendRedirect("RecruitmentLoginServlet");
//
//			}
//
//		} catch (Exception e) {
//			logger.debug("",e);
//			throw new RequestFailureException(LM.getText(LC.USER_ADD_ERROR_USER_ADD, loginDTO),e instanceof RequestFailureException?null:e);
//		}
//	}
    private void sendOTP(HttpServletRequest request, HttpServletResponse response) throws IOException {
        Map<String, Object> res = new HashMap<>();
        try {
            String method = request.getParameter("methodRadio");
            String phoneNo = "88" + UtilCharacter.convertNumberBnToEn(request.getParameter(ServletConstant.PHONE_NO));
            String email = request.getParameter("email");
            String OTP = PasswordGenerator.getRandomNumberPassword(OTP_LENGTH);
            UserDAO userDAO = new UserDAO();
            UserDTO userDTO = userDAO.getUserDTOByPhoneNo(phoneNo);
            if (userDTO == null) {
                userDTO = userDAO.getUserDTOByMail(email);
            }
            if (userDTO == null) {
                res.put("success", false);
                res.put("messageEn", "No user exists with this phone number/email");
                res.put("messageBn", "এই নাম্বার/ইমেইল দিয়ে কোনো ইউজার নেই");
            } else {
                userDTO.otp = OTP;
                userDTO.otpRequestTime = System.currentTimeMillis();
                userDTO.otpExpireTime = System.currentTimeMillis() + OTP_TIMEOUT_MINUTE * 60 * 1000;
                userDAO.updateOTP(userDTO);
                UserRepository.getInstance().updateUserDTO(userDTO);
                if (method.equalsIgnoreCase("mobile")) {
                    SmsService.send(userDTO.phoneNo, "Your OTP for resetting PRP password is " + OTP + ". The OTP is valid for " + OTP_TIMEOUT_MINUTE + " Minutes.");
                } else {
                    SendEmailDTO sendEmailDTO = new SendEmailDTO();
                    sendEmailDTO.setFrom("edms@pbrlp.gov.bd");
                    sendEmailDTO.to = new String[]{email}; //new String[]{"saifun@revesoft.com"};
                    sendEmailDTO.subject = "Password reset OTP";
                    sendEmailDTO.text = "Your OTP for resetting PRP password is " + OTP + ". The OTP is valid for " + OTP_TIMEOUT_MINUTE + " Minutes.";
                    EmailService receiver = new EmailService();
                    receiver.sendMail(sendEmailDTO);
                }

                res.put("success", true);
                res.put("messageEn", "OTP sent to mobile number");
                res.put("messageBn", "ওটিপি মোবাইলে প্রেরণ করা হয়ছে");
                res.put("TimeRemaining", (userDTO.otpExpireTime - System.currentTimeMillis()) / 1000);
            }

        } catch (Exception e) {
            res.put("success", false);
            res.put("messageEn", "Sending OTP Failed failed!");
            res.put("messageBn", "ওটিপি প্রেরণ ব্যর্থ হয়েছে!");
        }

        PrintWriter out = response.getWriter();
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        out.println(gson.toJson(res));
        out.close();
    }

    private void pinCodeRetrieve(HttpServletRequest request, HttpServletResponse response) throws IOException {
        try {
            UserDTO userDTO = new UserDTO();
            userDTO.phoneNo = "88" + UtilCharacter.convertNumberBnToEn(request.getParameter(ServletConstant.PHONE_NO));
            userDTO.password = PasswordGenerator.getRandomNumberPassword(6);
            String password = userDTO.password;

            userDTO.password = PasswordUtil.getInstance().encrypt(userDTO.password);

            UserDAO userDAO = new UserDAO();

            UserDTO existingUser = userDAO.getUserDTOByPhoneNo(userDTO.phoneNo);

            if (existingUser == null) {

                String tempString = "No user exists";

                PrintWriter out = response.getWriter();
                out.println(tempString);
                out.close();

            } else {
                existingUser.password = userDTO.password;

                userDAO.updateUserPassword(existingUser);
                UserRepository.getInstance().updateUserDTO(existingUser);
                request.getSession().setAttribute(ServletConstant.SUCCESSFUL_MSG, LC.USER_ADD_SUCCESS_USER_ADD);

                SmsService.send(existingUser.phoneNo, "Dear " + existingUser.fullName + ", " + "your PRP pincode is successfully changed. Please log in with Username: " + existingUser.userName + " , and Password: " + password);

                request.getSession().invalidate();
//				    response.sendRedirect("RecruitmentLoginServlet");

            }

        } catch (Exception e) {

            PrintWriter out = response.getWriter();
            out.println("Error");
            out.close();

//			logger.debug("",e);
//			throw new RequestFailureException(LM.getText(LC.USER_ADD_ERROR_USER_ADD, loginDTO),e instanceof RequestFailureException?null:e);
        }
    }

    private void deleteUser(HttpServletRequest request, HttpServletResponse response) {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        try {

            String[] IDsToDelete = request.getParameterValues(ServletConstant.ID);
            for (String s : IDsToDelete) {
                long id = Long.parseLong(s);
                logger.debug("###DELETING " + s);
                new UserDAO().deleteUserByUserID(id);
            }


            request.getSession().setAttribute(ServletConstant.SUCCESSFUL_MSG, LC.USER_SEARCH_SUCCESS_USER_DELETE);
            response.sendRedirect(JSPConstant.USER_SEARCH_SERVLET);

        } catch (Exception ex) {
            logger.debug(ex);
            throw new RequestFailureException(LM.getText(LC.USER_SEARCH_ERROR_USER_DELETE, loginDTO), ex instanceof RequestFailureException ? null : ex);
        }


    }

    private void ChangePassUser(HttpServletRequest request, HttpServletResponse response) {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        try {
//			long userID = Long.parseLong(request.getParameter(ServletConstant.ID));
//
//			LoginDTO loginDTO2 = new LoginDTO();
//
//			loginDTO2.isOisf = 0;
//
//			loginDTO2.userID = userID;
//
//			UserDTO userDTORepo = UserRepository.getInstance().getUserDTOByUserID(loginDTO2);
//			if(userDTORepo == null || userDTORepo.isDeleted)
//			{
//				throw new RequestFailureException(LM.getText(LC.USER_ADD_ERROR_USER_NOT_FOUND, loginDTO));
//			}
            UserDTO userDTO = new UserDTO();
            userDTO.ID = loginDTO.userID;

            userDTO.password = request.getParameter(ServletConstant.PASSWORD);
            userDTO.repassword = request.getParameter("repassword");
            String oldPassword = request.getParameter("oldPassword");


            if (!isUserDTOValidPassword(userDTO, request, response, oldPassword)) {
                request.getSession().setAttribute(ServletConstant.USER_DTO, userDTO);
                //throw new RequestFailureException(LM.getText(LC.USER_ADD_ERROR_USER_EDIT, loginDTO));
                response.sendRedirect("UserServlet?actionType=getChangePassword");
            } else {

                userDTO.password = PasswordUtil.getInstance().encrypt(userDTO.password);
                //			if(!userDTO.password.equals(ServletConstant.DEFAULT_PASSWORD))
                //			{
                //				userDTO.password = PasswordUtil.getInstance().encrypt(userDTO.password);
                //			}
                //			else
                //			{
                //				userDTO.password = userDTORepo.password;
                //			}
                UserDAO userDAO = new UserDAO();
                userDAO.updateUserPassword(userDTO);

                request.getSession().setAttribute(ServletConstant.SUCCESSFUL_MSG, LC.USER_ADD_SUCCESS_USER_EDIT);

                //response.sendRedirect(JSPConstant.USER_SEARCH_SERVLET);
                logger.debug("#############################Getting index from cpfilter##############################");
                RequestDispatcher requestDispatcher = request.getRequestDispatcher("/home/index.jsp");
                requestDispatcher.forward(request, response);
                //response.sendRedirect( "LogoutServlet" );
            }
        } catch (Exception e) {
            logger.fatal("", e);
            throw new RequestFailureException(LM.getText(LC.USER_ADD_ERROR_USER_EDIT, loginDTO), e instanceof RequestFailureException ? null : e);
        }

    }

    private void updateUser(HttpServletRequest request, HttpServletResponse response) {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        try {
            long userID = Long.parseLong(request.getParameter(ServletConstant.ID));

            LoginDTO loginDTO2 = new LoginDTO();

            loginDTO2.isOisf = 0;

            loginDTO2.userID = userID;

            UserDTO userDTORepo = UserRepository.getUserDTOByUserID(loginDTO2);
            if (userDTORepo == null || userDTORepo.isDeleted == 1) {
                throw new RequestFailureException(LM.getText(LC.USER_ADD_ERROR_USER_NOT_FOUND, loginDTO));
            }
            UserDTO userDTO = new UserDTO();
            userDTO.ID = userID;
            userDTO.userName = request.getParameter(ServletConstant.USERNAME);
            userDTO.password = request.getParameter(ServletConstant.PASSWORD);
            userDTO.userType = Integer.parseInt(request.getParameter(ServletConstant.USER_TYPE));
            userDTO.roleID = Integer.parseInt(request.getParameter(ServletConstant.ROLE_NAME));
            userDTO.languageID = Integer.parseInt(request.getParameter(ServletConstant.LANGUAGE_ID));
            userDTO.mailAddress = request.getParameter(ServletConstant.MAIL_ADDRESS);
            userDTO.fullName = request.getParameter(ServletConstant.FULL_NAME);
            userDTO.phoneNo = request.getParameter(ServletConstant.PHONE_NO);
            userDTO.centreType = Integer.parseInt(request.getParameter("CentreType"));

            userDTO.otpSMS = request.getParameter("otpSMS") != null;
            userDTO.otpEmail = request.getParameter("otpEmail") != null;
            userDTO.otpPushNotification = request.getParameter("otpPushNotification") != null;

            if (!isUserDTOValid(userDTO, request, response)) {
                request.getSession().setAttribute(ServletConstant.USER_DTO, userDTO);
                throw new RequestFailureException(LM.getText(LC.USER_ADD_ERROR_USER_EDIT, loginDTO));
            }
            if (!userDTO.password.equals(ServletConstant.DEFAULT_PASSWORD)) {
                userDTO.password = PasswordUtil.getInstance().encrypt(userDTO.password);
            } else {
                userDTO.password = userDTORepo.password;
            }
            UserDAO userDAO = new UserDAO();
            userDAO.updateUser(userDTO);

            request.getSession().setAttribute(ServletConstant.SUCCESSFUL_MSG, LC.USER_ADD_SUCCESS_USER_EDIT);

            response.sendRedirect(JSPConstant.USER_SEARCH_SERVLET);
        } catch (Exception e) {
            logger.fatal("", e);
            throw new RequestFailureException(LM.getText(LC.USER_ADD_ERROR_USER_EDIT, loginDTO), e instanceof RequestFailureException ? null : e);
        }

    }

    Doctor_time_slotDAO doctor_time_slotDAO = new Doctor_time_slotDAO();

    private void updateUserTypeAndRole(HttpServletRequest request, HttpServletResponse response) {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        try {
            long orgId = Long.parseLong(request.getParameter("orgId"));
            long lastModificationTime = System.currentTimeMillis() + 180000;//3mins delay

            logger.debug("orgId = " + orgId);
            UserDTO userDTO = UserRepository.getUserDTOByOrganogramID(orgId);

            if (userDTO == null) {
                throw new RequestFailureException(LM.getText(LC.USER_ADD_ERROR_USER_NOT_FOUND, loginDTO));
            }

            logger.debug("username and organogram = " + userDTO.userName + " " + userDTO.organogramID);
            userDTO.userType = Integer.parseInt(request.getParameter(ServletConstant.USER_TYPE));
            userDTO.roleID = Integer.parseInt(request.getParameter(ServletConstant.ROLE_NAME));


            Utils.handleTransaction(()->{
                if (userDTO.roleID == SessionConstants.DOCTOR_ROLE || userDTO.roleID == SessionConstants.PHYSIOTHERAPIST_ROLE) {
                    if (doctor_time_slotDAO.get1stDTOByDoctor(userDTO.organogramID) == null) {
                        Doctor_time_slotDTO doctor_time_slotDTO = new Doctor_time_slotDTO(userDTO);
                        doctor_time_slotDTO.lastModificationTime = lastModificationTime;
                        doctor_time_slotDAO.add(doctor_time_slotDTO);
                    }
                }

                UserDAO userDAO = new UserDAO();
                userDAO.setRole(userDTO,lastModificationTime);

                request.getSession().setAttribute(ServletConstant.SUCCESSFUL_MSG, LC.USER_ADD_SUCCESS_USER_EDIT);

                String[] moreRoles = request.getParameterValues("moreRole");
                List<Long> rolesFromParams;

                if (moreRoles != null) {
                    rolesFromParams = Stream.of(moreRoles)
                            .map(Long::parseLong)
                            .sorted()
                            .collect(Collectors.toList());
                } else {
                    rolesFromParams = new ArrayList<>();
                }

                employeeOfficesDAO.deleteNonDefaultsByOrg(orgId, lastModificationTime);
                logger.debug("########## userDTO.employee_record_id " + userDTO.employee_record_id);
                EmployeeOfficeDTO defDTO = employeeOfficesDAO.getDefaultByOfficeUnitOrganogramId(userDTO.organogramID);
                if (defDTO != null) {
                    for (long roleId : rolesFromParams) {
                        logger.debug("########## Adding " + roleId);
                        defDTO.organogramRole = (int) roleId;
                        defDTO.isDefaultRole = 0;
                        defDTO.lastModificationTime = lastModificationTime;
                        employeeOfficesDAO.add(defDTO);
                        if (roleId == SessionConstants.DOCTOR_ROLE || roleId == SessionConstants.PHYSIOTHERAPIST_ROLE) {
                            if (doctor_time_slotDAO.get1stDTOByDoctor(userDTO.organogramID) == null) {
                                Doctor_time_slotDTO doctor_time_slotDTO = new Doctor_time_slotDTO(userDTO);
                                doctor_time_slotDTO.lastModificationTime = lastModificationTime;
                                doctor_time_slotDAO.add(doctor_time_slotDTO);
                            }
                        }
                    }
                } else {
                    logger.debug("########## defDTO is null ");
                }

                boolean hasDR = userDAO.getrUserDTOsByUserIdAndOrgId(userDTO.ID, userDTO.organogramID)
                        .stream()
                        .anyMatch(dto -> dto.roleID == SessionConstants.DOCTOR_ROLE || dto.roleID == SessionConstants.PHYSIOTHERAPIST_ROLE);

                logger.debug("hasDR = " + hasDR);

                if (!hasDR) {
                    doctor_time_slotDAO.deleteChildrenByParent(userDTO.organogramID, "doctor_id", lastModificationTime);
                }

            });

            Utils.runIOTaskInASeparateThread(() -> () -> {
                logger.debug("going to updating cache all after role change");
                EmployeeOfficeRepository.getInstance().reloadWithExactModificationTime(lastModificationTime);
                UserRepository.getInstance().reloadWithExactModificationTime(lastModificationTime);
                logger.debug("cache updating is done");
            });


            response.sendRedirect(JSPConstant.USER_SEARCH_SERVLET);
        } catch (Exception e) {
            logger.fatal("", e);
            throw new RequestFailureException(LM.getText(LC.USER_ADD_ERROR_USER_EDIT, loginDTO), e instanceof RequestFailureException ? null : e);
        }

    }

    private void getEditPage(HttpServletRequest request, HttpServletResponse response) {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        try {
            UserDTO userDTO;
            long orgId = Long.parseLong(request.getParameter("orgId"));

            userDTO = UserRepository.getUserDTOByOrganogramID(orgId);
            RoleDTO roleDTO = PermissionRepository.getRoleDTOByRoleID(userDTO.roleID);
            if (roleDTO != null) {
                userDTO.roleName = PermissionRepository.getRoleDTOByRoleID(userDTO.roleID).roleName;
            }
            request.setAttribute(ServletConstant.USER_DTO, userDTO);
            request.setAttribute(ServletConstant.ROLE_LIST, PermissionRepository.getAllRoles());
            RequestDispatcher rd = request.getRequestDispatcher(JSPConstant.USER_EDIT);
            rd.forward(request, response);
        } catch (Exception ex) {
            logger.fatal("", ex);
            throw new RequestFailureException(LM.getText(LC.USER_SEARCH_ERROR_USER_GET, loginDTO), ex instanceof RequestFailureException ? null : ex);
        }

    }

    private void getUserDTOByUsername(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        try {
            String userName = "880" + request.getParameter("phoneNo");

            UserDAO userDAO = new UserDAO();
            UserDTO userDTO = userDAO.getUserDTOByUsername(userName);

            PrintWriter out = response.getWriter();
            if (userDTO == null) out.print("null");
            else {
                Gson gson = new Gson();
                String message = gson.toJson(userDTO);
                out.print(message);
            }
            out.close();

        } catch (Exception ex) {
            logger.fatal("", ex);
            throw new RequestFailureException(LM.getText(LC.USER_SEARCH_ERROR_USER_GET, loginDTO), ex instanceof RequestFailureException ? null : ex);
        }

    }

    private void searchUser(HttpServletRequest request, HttpServletResponse response) {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDAO userDAO = new UserDAO();
        RecordNavigationManager rnManager = new RecordNavigationManager(SessionConstants.NAV_USER, request, userDAO, SessionConstants.VIEW_USER, SessionConstants.SEARCH_USER);
        try {
            rnManager.doJob(loginDTO);
            RequestDispatcher rd = request.getRequestDispatcher(JSPConstant.USER_SEARCH);
            rd.forward(request, response);
        } catch (Exception e) {
            logger.fatal("", e);
            throw new RequestFailureException(LM.getText(LC.USER_SEARCH_ERROR_USER_SEARCH, loginDTO), e instanceof RequestFailureException ? null : e);
        }

    }

    private void UserChangePassword(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //request.setAttribute(ServletConstant.ROLE_LIST, PermissionRepository.getInstance().getAllRoles());//make Servlet constant
        RequestDispatcher requestDispatcher = request.getRequestDispatcher(JSPConstant.USER_CHANGE_PASSWORD);
        requestDispatcher.forward(request, response);
    }

    private boolean isUserDTOValid(UserDTO userDTO, HttpServletRequest request, HttpServletResponse response) {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        boolean valid = true;
        if (StringUtils.isBlank(userDTO.userName)) {
            request.getSession().setAttribute(ServletConstant.USERNAME, LM.getText(LC.USER_ADD_ERROR_USERNAME_EMPTY, loginDTO));
            valid = false;
        }
		/*if(StringUtils.isBlank(userDTO.password))
		{
			request.getSession().setAttribute(ServletConstant.PASSWORD, LM.getText(LC.USER_ADD_ERROR_PASSWORD_EMPTY, loginDTO));
			valid = false;
		}
		else if(userDTO.password.length() < 5)
		{
			request.getSession().setAttribute(ServletConstant.PASSWORD, LM.getText(LC.USER_ADD_ERROR_PASSWORD_LENGTH_SHORT, loginDTO));
			valid = false;
		}*/
        if (StringUtils.isBlank(userDTO.mailAddress)) {
            request.getSession().setAttribute(ServletConstant.MAIL_ADDRESS, LM.getText(LC.USER_ADD_ERROR_EMAIL_EMPTY, loginDTO));
            valid = false;
        }
        return valid;
    }

    private boolean isUserDTOValidPassword(UserDTO userDTO, HttpServletRequest request, HttpServletResponse response, String oldPassword) throws Exception {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTOOfLoggedInUser = UserRepository.getUserDTOByUserID(loginDTO);

        String loggedInUserPassword = userDTOOfLoggedInUser.password;
        String encryptedOldPassword = PasswordUtil.getInstance().encrypt(oldPassword);

        boolean valid = true;

        if (StringUtils.isBlank(userDTO.password)) {
            request.getSession().setAttribute(ServletConstant.PASSWORD, LM.getText(LC.USER_ADD_ERROR_PASSWORD_EMPTY, loginDTO));
            valid = false;
        } else if (userDTO.password.length() < 5) {
            request.getSession().setAttribute(ServletConstant.PASSWORD, LM.getText(LC.USER_ADD_ERROR_PASSWORD_LENGTH_SHORT, loginDTO));
            valid = false;
        } else if (!userDTO.password.equals(userDTO.repassword)) {
            request.getSession().setAttribute(ServletConstant.PASSWORD, "Retyped password does not match");
            valid = false;
        } else if (!loggedInUserPassword.equals(encryptedOldPassword)) {

            request.getSession().setAttribute(ServletConstant.PASSWORD, "Old Password does not match");
            valid = false;
        }

        return valid;
    }

    private String checkPassword(String password, String confirmPassword, boolean isLangEng) {
        if (password == null || password.trim().length() == 0) {
            return isLangEng ? "Please enter password" : "পাসওয়ার্ড দিন";
        }
        if (confirmPassword == null || confirmPassword.trim().length() == 0) {
            return isLangEng ? "Please enter confirm password" : "পাসওয়ার্ড নিশ্চিত দিন";
        }
        if (password.length() < 8 || confirmPassword.length() < 8) {
            return isLangEng ? "Please enter password with minimum 8 characters" : "ন্যূনতম ৮ অক্ষরের পাসওয়ার্ড দিন";
        }
        if (!password.equals(confirmPassword)) {
            return isLangEng ? "Confirm password doesn't matched with password" : "নিশ্চিত পাসওয়ার্ড  পাসওয়ার্ডের সাথে মিলে নাই";
        }
        return "";
    }

    private void updateForgetPassword(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO) throws IOException {
        Map<String, Object> res = new HashMap<>();
        try {
            String password = request.getParameter(ServletConstant.PASSWORD);
            String confirmPassword = request.getParameter("confirmPassword");
            String checkMessage = checkPassword(password, confirmPassword, false);
            if (checkMessage.length() == 0) {
                userDTO.password = PasswordUtil.getInstance().encrypt(password);

                UserDAO userDAO = new UserDAO();
                userDAO.updateUserPassword(userDTO);
                UserRepository.getInstance().updateUserDTO(userDTO);

                res.put("success", true);
                res.put("messageEn", "Password successfully changed.");
                res.put("messageBn", "পাসওয়ার্ড সফল ভাবে পরিবর্তিত হয়েছে।");
                res.put("redirectPath", "/LoginServlet");
            } else {
                res.put("success", false);
                res.put("messageEn", "Password change failed!");
                res.put("messageBn", "পাসওয়ার্ড পরিবর্তন ব্যর্থ হয়েছে!");
            }
        } catch (Exception e) {
            logger.error(e);
            res.put("success", false);
            res.put("messageEn", "Password change failed!");
            res.put("messageBn", "পাসওয়ার্ড পরিবর্তন ব্যর্থ হয়েছে!");
        }
        PrintWriter out = response.getWriter();
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        out.println(gson.toJson(res));
        out.close();
    }


    public void validatePasswordOTP(HttpServletRequest request, HttpServletResponse response) throws IOException {
        Map<String, Object> res = new HashMap<>();
        try {

            String OTP = request.getParameter("otp");
            String phoneNo = "88" + UtilCharacter.convertNumberBnToEn(request.getParameter(ServletConstant.PHONE_NO));
            String email = request.getParameter("email");
            UserDAO userDAO = new UserDAO();
            UserDTO userDTO = userDAO.getUserDTOByPhoneNo(phoneNo);
            if (userDTO == null) {
                userDTO = userDAO.getUserDTOByMail(email);
            }
            if (userDTO == null) {
                res.put("success", false);
                res.put("messageEn", "No user exists with this phone number/email");
                res.put("messageBn", "এই নাম্বার/ইমেইল দিয়ে কোনো ইউজার নেই");
            } else {
                if (OTP.equalsIgnoreCase(userDTO.otp)) {
                    updateForgetPassword(request, response, userDTO);
                } else {
                    res.put("success", false);
                    res.put("messageEn", "Wrong OPT! Please try again.");
                    res.put("messageBn", "ভুল ওটিপি! আবার চেষ্টা করুন");
                }
            }

        } catch (Exception e) {
            res.put("success", false);
            res.put("messageEn", "Password change failed!");
            res.put("messageBn", "পাসওয়ার্ড পরিবর্তন ব্যর্থ হয়েছে!");
        }
        PrintWriter out = response.getWriter();
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        out.println(gson.toJson(res));
        out.close();
    }
}
