package employee_acr;

import common.EmployeeCommonDAOService;
import employee_records.Employee_recordsDAO;
import employee_records.Employee_recordsDTO;
import org.apache.log4j.Logger;
import pb.CatRepository;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings({"unused", "Duplicates"})
public class Employee_acrDAO implements EmployeeCommonDAOService<Employee_acrDTO> {

    private static final Logger logger = Logger.getLogger(Employee_acrDAO.class);

    private static final String addQuery = "INSERT INTO {tableName} (employee_records_id, year, acr_from, acr_to, status,is_partial, remarks,office_unit_id,filesDropzone, " +
            "modified_by,search_column, lastModificationTime,inserted_by_user_id,insertion_date, isDeleted, ID) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

    private static final String updateQuery = "UPDATE {tableName} SET employee_records_id=?, year=?, acr_from=?, acr_to=?, status=?,is_partial=?, remarks=?,office_unit_id=?,filesDropzone=?, " +
            "modified_by=?,search_column=?, lastModificationTime=?  WHERE ID=?";

    private final Map<String, String> searchMap = new HashMap<>();

    private static Employee_acrDAO INSTANCE = null;

    private Employee_acrDAO() {
        searchMap.put("office_units_id", " and (office_unit_id IN ?)");
        searchMap.put("employee_records_id", " and (employee_records_id  = ?)");
        searchMap.put("acr_from_start", " and (acr_from >= ?)");
        searchMap.put("acr_from_end", " and (acr_from <= ?)");
        searchMap.put("acr_to_start", " and (acr_to >= ?)");
        searchMap.put("acr_to_end", " and (acr_to <= ?)");
        searchMap.put("status_cat", " and (status = ?)");
        searchMap.put("is_partial", " and (is_partial = ?)");
        searchMap.put("year", " and (year = ?)");
        searchMap.put("AnyField", " and (search_column like ?)");
        searchMap.put("employee_records_id_internal", " and (employee_records_id = ?)");
    }

    public static Employee_acrDAO getInstance() {
        if (INSTANCE == null) {
            synchronized (Employee_acrDAO.class) {
                if (INSTANCE == null) {
                    INSTANCE = new Employee_acrDAO();
                }
            }
        }
        return INSTANCE;
    }

    public void set(PreparedStatement ps, Employee_acrDTO employee_acrDTO, boolean isInsert) throws SQLException {
        int index = 0;
        setSearchColumn(employee_acrDTO);
        ps.setLong(++index, employee_acrDTO.employeeRecordsId);
        ps.setInt(++index, employee_acrDTO.year);
        ps.setLong(++index, employee_acrDTO.acrFrom);
        ps.setLong(++index, employee_acrDTO.acrTo);
        ps.setInt(++index, employee_acrDTO.status);
        ps.setBoolean(++index, employee_acrDTO.isPartial);
        ps.setString(++index, employee_acrDTO.remarks);
        ps.setLong(++index, employee_acrDTO.officeUnitId);
        ps.setLong(++index, employee_acrDTO.filesDropzone);
        ps.setString(++index, employee_acrDTO.modifiedBy);
        ps.setString(++index, employee_acrDTO.searchColumn);
        ps.setLong(++index, employee_acrDTO.lastModificationTime);
        if (isInsert) {
            ps.setLong(++index, employee_acrDTO.insertedByUserId);
            ps.setLong(++index, employee_acrDTO.insertionDate);
            ps.setInt(++index, 0 /* isDeleted */);
        }
        ps.setLong(++index, employee_acrDTO.iD);
    }

    public void setSearchColumn(Employee_acrDTO employee_acrDTO) {
        List<String> list = new ArrayList<>();
        try {
            Employee_recordsDTO employeeRecordsDTO = new Employee_recordsDAO().getEmployee_recordsDTOByID(employee_acrDTO.employeeRecordsId);
            if (employeeRecordsDTO != null) {
                if (employeeRecordsDTO.nameEng != null && employeeRecordsDTO.nameEng.trim().length() > 0) {
                    list.add(employeeRecordsDTO.nameEng.trim());
                }
                if (employeeRecordsDTO.nameBng != null && employeeRecordsDTO.nameBng.trim().length() > 0) {
                    list.add(employeeRecordsDTO.nameBng.trim());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        list.add(CatRepository.getInstance().getText("English", "status", employee_acrDTO.status));
        list.add(CatRepository.getInstance().getText("Bangla", "status", employee_acrDTO.status));
        employee_acrDTO.searchColumn = String.join(" ", list);
    }

    @Override
    public Employee_acrDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Employee_acrDTO employee_acrDTO = new Employee_acrDTO();
            employee_acrDTO.employeeRecordsId = rs.getLong("employee_records_id");
            employee_acrDTO.iD = rs.getLong("ID");
            employee_acrDTO.year = rs.getInt("year");
            employee_acrDTO.acrFrom = rs.getLong("acr_from");
            employee_acrDTO.acrTo = rs.getLong("acr_to");
            employee_acrDTO.status = rs.getInt("status");
            employee_acrDTO.isPartial = rs.getBoolean("is_partial");
            employee_acrDTO.remarks = rs.getString("remarks");
            employee_acrDTO.officeUnitId = rs.getLong("office_unit_id");
            employee_acrDTO.filesDropzone = rs.getLong("filesDropzone");
            employee_acrDTO.insertedByUserId = rs.getLong("inserted_by_user_id");
            employee_acrDTO.insertionDate = rs.getLong("insertion_date");
            employee_acrDTO.modifiedBy = rs.getString("modified_by");
            employee_acrDTO.searchColumn = rs.getString("search_column");
            employee_acrDTO.isDeleted = rs.getInt("isDeleted");
            employee_acrDTO.lastModificationTime = rs.getLong("lastModificationTime");
            return employee_acrDTO;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    @Override
    public String getTableName() {
        return "employee_acr";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Employee_acrDTO) commonDTO, addQuery, true);
    }

    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Employee_acrDTO) commonDTO, updateQuery, false);
    }
}
	