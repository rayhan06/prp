package employee_acr;

import sessionmanager.SessionConstants;
import util.CommonEmployeeDTO;


public class Employee_acrDTO extends CommonEmployeeDTO {
    public int year = 0;
    public long acrFrom = SessionConstants.MIN_DATE;
    public long acrTo = SessionConstants.MIN_DATE;
    public int status = 0;
    public boolean isPartial = false;
    public long officeUnitId = 0;
    public long filesDropzone = -1;
    public long insertedByUserId = 0;
    public long insertionDate = 0;
    public String modifiedBy = "";


    @Override
    public String toString() {
        return "Employee_acrDTO{" +
                "employeeRecordsId=" + employeeRecordsId +
                ", year=" + year +
                ", acrFrom=" + acrFrom +
                ", acrTo=" + acrTo +
                ", status=" + status +
                ", isPartial=" + isPartial +
                ", officeUnitId=" + officeUnitId +
                ", remarks='" + remarks + '\'' +
                ", filesDropzone=" + filesDropzone +
                ", insertedByUserId=" + insertedByUserId +
                ", insertionDate=" + insertionDate +
                ", modifiedBy='" + modifiedBy + '\'' +
                '}';
    }

}