package employee_acr;

import employee_offices.EmployeeOfficeDTO;
import employee_offices.EmployeeOfficeRepository;
import employee_records.Employee_recordsDTO;
import employee_records.Employee_recordsRepository;
import office_unit_organograms.OfficeUnitOrganograms;
import office_unit_organograms.OfficeUnitOrganogramsRepository;
import office_units.Office_unitsDTO;
import office_units.Office_unitsRepository;
import pb_notifications.Pb_notificationsDAO;
import util.StringUtils;

public class Employee_acrNotification {
    private final Pb_notificationsDAO pb_notificationsDAO;

    private Employee_acrNotification(){
        pb_notificationsDAO = Pb_notificationsDAO.getInstance();
    }

    private static class LazyLoader{
        static final Employee_acrNotification INSTANCE = new Employee_acrNotification();
    }

    public static Employee_acrNotification getInstance(){
        return LazyLoader.INSTANCE;
    }

    public void sendAcrSubmitNotification(Employee_acrDTO acrDTO){
        Employee_recordsDTO employee_recordsDTO = Employee_recordsRepository.getInstance().getById(acrDTO.employeeRecordsId);
        if(employee_recordsDTO == null) return;

        EmployeeOfficeDTO employeeOfficeDTO = EmployeeOfficeRepository.getInstance().getByEmployeeRecordIdIsDefault(acrDTO.employeeRecordsId);
        if(employeeOfficeDTO == null) return;

        long fromOrganongramId = employeeOfficeDTO.officeUnitOrganogramId;
        OfficeUnitOrganograms officeUnitOrganograms = OfficeUnitOrganogramsRepository.getInstance().getById(fromOrganongramId);
        if(officeUnitOrganograms == null) return;

        Office_unitsDTO officeUnitsDTO = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(employeeOfficeDTO.officeUnitId);
        if(officeUnitsDTO == null) return;

        long toOrganongramId = officeUnitOrganograms.superior_designation_id;
        String url = "Employee_acrServlet?actionType=view&ID=" + acrDTO.iD;

        String messageEn = employee_recordsDTO.nameEng.concat(",").concat(officeUnitOrganograms.designation_eng).concat(",")
                .concat(officeUnitsDTO.unitNameEng).concat(" has submitted his ").concat(acrDTO.isPartial ? "Partial " : "")
                .concat("ACR for ").concat(String.valueOf(acrDTO.year));

        String messageBn = employee_recordsDTO.nameBng.concat(",").concat(officeUnitOrganograms.designation_bng).concat(",")
                .concat(officeUnitsDTO.unitNameBng).concat(" তার ").concat(StringUtils.convertToBanNumber(acrDTO.year + " "))
                .concat("সালের ").concat(acrDTO.isPartial ? "আংশিক " : " ")
                .concat("এসিআর জমা দিয়েছেন");

        String notificationMessage = messageEn.concat("$").concat(messageBn);

        Thread thread = new Thread(()->{
            long currentTime = System.currentTimeMillis();
            pb_notificationsDAO.addPb_notifications(toOrganongramId, currentTime, url, notificationMessage);
        });
        thread.setDaemon(true);
        thread.start();
    }
}
