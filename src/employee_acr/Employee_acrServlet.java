package employee_acr;

import common.BaseServlet;
import common.CommonDAOService;
import common.EmployeeServletService;
import employee_offices.EmployeeOfficeDTO;
import employee_offices.EmployeeOfficeRepository;
import office_unit_organograms.OfficeUnitOrganogramsRepository;
import office_units.Office_unitsRepository;
import permission.MenuConstants;
import user.UserDTO;
import util.CommonDTO;
import util.HttpRequestUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.joining;


@WebServlet("/Employee_acrServlet")
@MultipartConfig
public class Employee_acrServlet extends BaseServlet implements EmployeeServletService {
    private static final long serialVersionUID = 1L;

    private static final int STATUS_OPEN = 1;

    private final Employee_acrDAO employee_acrDAO = Employee_acrDAO.getInstance();

    @Override
    public String getTableName() {
        return employee_acrDAO.getTableName();
    }

    @Override
    public String getServletName() {
        return "Employee_acrServlet";
    }

    @Override
    public Employee_acrDAO getCommonDAOService() {
        return employee_acrDAO;
    }

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        boolean isLangEng = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
        Employee_acrDTO employee_acrDTO;
        SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
        long currentTime = System.currentTimeMillis();
        if (addFlag) {
            employee_acrDTO = new Employee_acrDTO();
            employee_acrDTO.employeeRecordsId = Long.parseLong(request.getParameter("empId"));
            employee_acrDTO.insertionDate = currentTime;
            employee_acrDTO.insertedByUserId = userDTO.employee_record_id;
        } else {
            employee_acrDTO = employee_acrDAO.getDTOFromID(Long.parseLong(request.getParameter("iD")));
        }
        employee_acrDTO.modifiedBy = String.valueOf(userDTO.employee_record_id);
        employee_acrDTO.lastModificationTime = currentTime;
        employee_acrDTO.year = Integer.parseInt(request.getParameter("year"));
        Date d = f.parse(request.getParameter("acrFrom"));
        employee_acrDTO.acrFrom = d.getTime();
        d = f.parse(request.getParameter("acrTo"));
        employee_acrDTO.acrTo = d.getTime();
        if (employee_acrDTO.acrFrom > employee_acrDTO.acrTo)
            throw new Exception(
                    isLangEng ? "From date can not be greater than to date"
                            : "তারিখ হতে, তারিখ পর্যন্ত থেকে বড় হতে পারে না"
            );

        List<Employee_acrDTO> employee_acrDTOList = employee_acrDAO.getByEmployeeId(employee_acrDTO.employeeRecordsId);
        employee_acrDTOList = employee_acrDTOList.stream()
                .filter(e->e.iD!=employee_acrDTO.iD)
                .collect(Collectors.toList());
        for (Employee_acrDTO employeeAcrDTO : employee_acrDTOList) {
            if (employeeAcrDTO.acrFrom <= employee_acrDTO.acrFrom && employeeAcrDTO.acrTo >= employee_acrDTO.acrFrom) {
                throw new Exception(
                        isLangEng ? "No conflict between two ACRs"
                                : "দুইটি এসিআরের মধ্যে তারিখের কনফ্লিক্ট হতে পারে না"
                );
            }

            if (employeeAcrDTO.acrFrom <= employee_acrDTO.acrTo && employeeAcrDTO.acrTo >= employee_acrDTO.acrTo) {
                throw new Exception(
                        isLangEng ? "No conflict between two ACRs"
                                : "দুইটি এসিআরের মধ্যে তারিখের কনফ্লিক্ট হতে পারে না"
                );
            }
        }
        employee_acrDTO.status = STATUS_OPEN;

        String isPartial = request.getParameter("isPartial");
        employee_acrDTO.isPartial = "true".equalsIgnoreCase(isPartial);
        EmployeeOfficeDTO employeeOfficeDTO = EmployeeOfficeRepository.getInstance().getByEmployeeRecordIdIsDefault(employee_acrDTO.employeeRecordsId);
        if (employeeOfficeDTO != null) {
            employee_acrDTO.officeUnitId = employeeOfficeDTO.officeUnitId;
        }
        employee_acrDTO.remarks = (request.getParameter("remarks"));
        employee_acrDTO.filesDropzone = Long.parseLong(request.getParameter("filesDropzone"));

        if (addFlag) {
            employee_acrDAO.add(employee_acrDTO);
        } else {
            employee_acrDAO.update(employee_acrDTO);
        }

        Employee_acrNotification.getInstance().sendAcrSubmitNotification(employee_acrDTO);
        return employee_acrDTO;
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[0];
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[0];
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[0];
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Employee_acrServlet.class;
    }

    @Override
    public String getAjaxAddRedirectURL(HttpServletRequest request, CommonDTO commonDTO) {
        return getAddOrEditOrDeleteRedirectURL(request, 3, "acr");
    }

    @Override
    public String getAjaxEditRedirectURL(HttpServletRequest request, CommonDTO commonDTO) {
        return getAddOrEditOrDeleteRedirectURL(request, 3, "acr");
    }

    @Override
    public String getDeleteRedirectURL(HttpServletRequest request) {
        return getAddOrEditOrDeleteRedirectURL(request, 3, "acr");
    }

    @Override
    public boolean getAddPagePermission(HttpServletRequest request) {
        return checkAddPermission(request);
    }

    @Override
    public boolean getEditPagePermission(HttpServletRequest request) {
        return checkEditOrViewPagePermission(request);
    }

    @Override
    public boolean getViewPagePermission(HttpServletRequest request) {
        return checkEditOrViewPagePermission(request);
    }

    @Override
    public boolean getEditPermission(HttpServletRequest request) {
        return checkEditOrViewPagePermission(request);
    }

    @Override
    public boolean getAddPermission(HttpServletRequest request) {
        return checkAddPermission(request);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        search(request);
        Map<String, String> extraCriteriaMap;
        if (request.getAttribute(CommonDAOService.CRITERIA_MAP_KEY) == null) {
            extraCriteriaMap = new HashMap<>();
        } else {
            extraCriteriaMap = (Map<String, String>) request.getAttribute(CommonDAOService.CRITERIA_MAP_KEY);
        }
        String onlySelectedOfficeString = request.getParameter("onlySelectedOffice");
        if (onlySelectedOfficeString != null) {
            boolean onlySelectedOffice = Boolean.parseBoolean(onlySelectedOfficeString);
            String officeUnitIdString = request.getParameter("office_units_id");
            if (officeUnitIdString != null) {
                long officeUnitId = Long.parseLong(officeUnitIdString);
                if (!onlySelectedOffice) {
                    //Set<Long> officeUnitIds = OfficeUnitOrganogramsRepository.getInstance().getDescentsOfficeUnitId(officeUnitId);
                    String officeUnitIdsString = Office_unitsRepository.getInstance().getDescentsOfficeUnitId(officeUnitId)
                            .stream()
                            .distinct()
                            .map(String::valueOf)
                            .collect(joining(","));
                    extraCriteriaMap.put("office_units_id", officeUnitIdsString);
                } else {
                    extraCriteriaMap.put("office_units_id", String.valueOf(officeUnitId));
                }
            }
        }

        request.setAttribute(CommonDAOService.CRITERIA_MAP_KEY, extraCriteriaMap);

        super.doGet(request, response);
    }
}
