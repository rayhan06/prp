package employee_acr;

/*
 * @author Md. Erfan Hossain
 * @created 16/03/2021 - 9:55 AM
 * @project parliament
 */

import employee_records.EmpOfficeModel;
import employee_records.EmployeeFlatInfoDAO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class Employee_acrDonebyDAO implements EmployeeFlatInfoDAO<Employee_acrDonebyDTO> {

    private static final List<String> columnList;

    static {
        columnList = Collections.singletonList("employee_acr_id");
    }

    @Override
    public String getTableName() {
        return "employee_acr_doneby";
    }

    @Override
    public List<String> getExtraColumnListForAddSQLQueryClause() {
        return columnList;
    }

    @Override
    public List<String> getExtraColumnListUpdateSQLQueryClause() {
        return columnList;
    }

    @Override
    public void setExtraPreparedStatementParams(PreparedStatement ps, Employee_acrDonebyDTO dto, boolean isInsert) throws Exception {
        ps.setObject(1,dto.employee_acr_id);
    }

    @Override
    public Employee_acrDonebyDTO buildTFromResultSet(ResultSet rs) throws SQLException {
        Employee_acrDonebyDTO dto = new Employee_acrDonebyDTO();
        dto.employee_acr_id = rs.getLong("employee_acr_id");
        return dto;
    }

    public void add(List<EmpOfficeModel> list, long trainingCalendarId, String requestBy) throws Exception {
        add(list,trainingCalendarId,requestBy,System.currentTimeMillis());
    }

    private void add(List<EmpOfficeModel> list, long trainingCalendarId, String requestBy,long requestTime) throws Exception {
        for (EmpOfficeModel model : list) {
            addEmployeeFlatInfoDTO(buildTrainerDTO(model, trainingCalendarId, requestBy, requestTime));
        }
    }

    private Employee_acrDonebyDTO buildTrainerDTO(EmpOfficeModel model, long employeeAcrId, String requestBy, long requestTime) {
        Employee_acrDonebyDTO dto = new Employee_acrDonebyDTO();
        dto.employee_acr_id = employeeAcrId;
        dto.employeeRecordsId = model.employeeRecordId;
        dto.officeUnitId = model.officeUnitId;
        dto.organogramId = model.organogramId;
        dto.insertBy = requestBy;
        dto.insertionDate = requestTime;
        dto.modified_by = requestBy;
        dto.lastModificationTime = requestTime;
        return dto;
    }

    public void update(List<EmpOfficeModel> list, long employeeAcrId, String requestBy) throws Exception {
        long requestTime = System.currentTimeMillis();
        Map<Boolean, List<EmpOfficeModel>> booleanListMap = deleteDeletedInfoAndReturnAMap(list, String.valueOf(employeeAcrId), requestBy, requestTime);
        List<EmpOfficeModel> newlyAddingList = booleanListMap.get(false);
        if(newlyAddingList != null && newlyAddingList.size()>0){
            add(newlyAddingList,employeeAcrId,requestBy,requestTime);
        }
        // No updating required here.
    }

}
