package designations;

import com.google.gson.Gson;
import org.apache.log4j.Logger;
import pb.OptionDTO;
import pb.Utils;
import repository.Repository;
import repository.RepositoryManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;


public class DesignationsRepository implements Repository {

    private static final Logger logger = Logger.getLogger(DesignationsRepository.class);
    private final DesignationsDAO designationsDAO;
    private final Map<Long, DesignationsDTO> mapOfDesignationsDTOToiD;
    private final List<DesignationsDTO> dtoList;
    private final Gson gson;


    private DesignationsRepository() {
        designationsDAO = DesignationsDAO.getInstance();
        mapOfDesignationsDTOToiD = new ConcurrentHashMap<>();
        dtoList = new ArrayList<>();
        gson = new Gson();
        RepositoryManager.getInstance().addRepository(this);
    }

    private static class LazyLoader {
        final static DesignationsRepository INSTANCE = new DesignationsRepository();
    }

    public static DesignationsRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

    public void reload(boolean reloadAll) {
        try {
            List<DesignationsDTO> dtoList = designationsDAO.getAllDTOs(reloadAll);
            updateCache(dtoList);
        } catch (Exception e) {
            logger.debug("FATAL", e);
        }
    }

    private void updateCache(List<DesignationsDTO> list){
        list.stream()
                .peek(e->{
                    DesignationsDTO dto = mapOfDesignationsDTOToiD.get(e.iD);
                    if(dto!=null){
                        mapOfDesignationsDTOToiD.remove(e.iD);
                        dtoList.remove(dto);
                    }
                })
                .filter(e->e.isDeleted == 0)
                .forEach(e-> {
                    mapOfDesignationsDTOToiD.put(e.iD,e);
                    dtoList.add(e);
                });
    }

    @Override
    public void reloadWithExactModificationTime(long time) {
        List<DesignationsDTO> dtoList = designationsDAO.getAllDTOsExactLastModificationTime(time);
        updateCache(dtoList);
    }

    public DesignationsDTO clone(DesignationsDTO dto) {
        String raw = gson.toJson(dto);
        return gson.fromJson(raw, DesignationsDTO.class);
    }


    public List<DesignationsDTO> getDesignationsList() {
        return dtoList;
    }

    public DesignationsDTO getDesignationsDTOByiD(long iD) {
        return mapOfDesignationsDTOToiD.get(iD);
    }

    @Override
    public String getTableName() {
        return designationsDAO.getTableName();
    }

    public String buildOptions(String language, Long selectedId) {
        return buildOptions(language, selectedId, false, dtoList);
    }

    public String buildOptionsWithoutSelectOption(String language) {
        return buildOptions(language, null, true, dtoList);
    }

    public String buildOptions(String language, Long selectedId, boolean withoutSelectOption, List<DesignationsDTO> dtoList) {
        List<OptionDTO> optionDTOList = null;
        if (dtoList != null && dtoList.size() > 0) {
            optionDTOList = dtoList.stream()
                    .map(dto -> new OptionDTO(dto.nameEn, dto.nameBn, String.valueOf(dto.iD)))
                    .collect(Collectors.toList());
        }
        if (withoutSelectOption) {
            return Utils.buildOptionsWithoutSelectOption(optionDTOList, language);
        }
        return Utils.buildOptions(optionDTOList, language, selectedId == null ? null : String.valueOf(selectedId));
    }
}


