package designations;


import common.BaseServlet;
import common.CacheUpdateModel;
import common.CommonDAOService;
import employee_offices.EmployeeOfficesDAO;
import office_unit_organogram.Office_unit_organogramDAO;
import office_unit_organograms.OfficeUnitOrganogramsRepository;
import org.apache.log4j.Logger;
import pb.Utils;
import permission.MenuConstants;
import user.UserDTO;
import util.CommonDTO;
import util.HttpRequestUtils;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;

@WebServlet("/DesignationsServlet")
@MultipartConfig
public class DesignationsServlet extends BaseServlet {
    private static final long serialVersionUID = 1L;

    public static Logger logger = Logger.getLogger(DesignationsServlet.class);

    @Override
    public String getTableName() {
        return DesignationsDAO.getInstance().getTableName();
    }

    @Override
    public String getServletName() {
        return "DesignationsServlet";
    }

    @Override
    public DesignationsDAO getCommonDAOService() {
        return DesignationsDAO.getInstance();
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[]{MenuConstants.DESIGNATIONS_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[]{MenuConstants.DESIGNATIONS_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[]{MenuConstants.DESIGNATIONS_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return DesignationsServlet.class;
    }


    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        boolean isLangEng = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
        DesignationsDTO dto;
        if (addFlag) {
            dto = new DesignationsDTO();
            dto.insertedBy = userDTO.employee_record_id;
            dto.insertionDate = System.currentTimeMillis();
        } else {
            dto = DesignationsDAO.getInstance().getDTOFromID(Long.parseLong(request.getParameter("iD")));
            if (dto == null) {
                throw new Exception(isLangEng ? "Information is not found" : "তথ্য পাওয়া যায়নি");
            }
        }
        validateAndSet(dto, request, isLangEng);
        dto.modifiedBy = userDTO.employee_record_id;
        dto.lastModificationTime = System.currentTimeMillis() + 60000; //1min delay;

        CommonDAOService.CACHE_UPDATE_MODEL_THREAD_LOCAL.set(new CacheUpdateModel(dto.lastModificationTime));
        Utils.handleTransaction(() -> {
            if (addFlag) {
                DesignationsDAO.getInstance().add(dto);
            } else {
                DesignationsDAO.getInstance().update(dto);
                Office_unit_organogramDAO.getInstance().updateDesignationInfoByDesignationsDTO(dto);
                EmployeeOfficesDAO.getInstance().updateDesignationNameByDesignationsDTO(dto);
            }
        });
        Utils.executeCache();
        //DesignationsRepository.getInstance().reloadWithExactModificationTime(dto.lastModificationTime);
        //OfficeUnitOrganogramsRepository.getInstance().reloadWithExactModificationTime(dto.lastModificationTime);
        return dto;
    }

    @Override
    public void finalize(HttpServletRequest request) {
        if (request.getParameter("actionType").equals("delete")) {
            if (request.getAttribute(BaseServlet.DELETED_ID_LIST) != null &&
                    request.getAttribute(BaseServlet.DELETED_TIME) != null) {
                try{
                    long deletedTime = (long) request.getAttribute(BaseServlet.DELETED_TIME);
                    DesignationsRepository.getInstance().reloadWithExactModificationTime(deletedTime);
                }catch (Exception ex){
                    ex.printStackTrace();
                    logger.error(ex.getMessage());
                }
            }
        }
    }
}