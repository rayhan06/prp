package designations;

public enum DesignationsEnum {

    DEPUTY_SECRETARY(293L),
    ;

    private final long id;

    DesignationsEnum(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }
}
