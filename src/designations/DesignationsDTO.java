package designations;

import annotation.CategoryValidation;
import annotation.NotEmpty;
import util.CommonDTO;


public class DesignationsDTO extends CommonDTO {

    @NotEmpty(engError = "English name is not found",bngError = "ইংরেজি নামের তথ্য পাওয়া যায়নি")
    public String nameEn = "";
    @NotEmpty(engError = "Bangla name is not found",bngError = "বাংলা নামের তথ্য পাওয়া যায়নি")
    public String nameBn = "";
    @CategoryValidation(domainName = "job_grade", engError = "Please select grade", bngError = "গ্রেড বাছাই করুন",
            properEngError = "Please select correct grade", properBngError = "সঠিক গ্রেড বাছাই করুন")
    public int jobGradeCat = -1;
    public long insertionDate = -1;
    public long insertedBy = -1;
    public long modifiedBy = -1;


    @Override
    public String toString() {
        return "$DesignationsDTO[" +
                " iD = " + iD +
                " nameEn = " + nameEn +
                " nameBn = " + nameBn +
                " jobGradeCat = " + jobGradeCat +
                " insertionDate = " + insertionDate +
                " insertedBy = " + insertedBy +
                " modifiedBy = " + modifiedBy +
                " searchColumn = " + searchColumn +
                " isDeleted = " + isDeleted +
                " lastModificationTime = " + lastModificationTime +
                "]";
    }

}