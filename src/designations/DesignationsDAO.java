package designations;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.sql.SQLException;

import common.CommonDAOService;
import org.apache.log4j.Logger;
import util.*;
import pb.*;

public class DesignationsDAO implements CommonDAOService<DesignationsDTO> {

    private static final Logger logger = Logger.getLogger(DesignationsDAO.class);

    private static String addQuery = "";
    private static String updateQuery = "";
    private final String[] columnNames;

    private DesignationsDAO() {
        columnNames = new String[]
                {
                        "name_en",
                        "name_bn",
                        "job_grade_cat",
                        "insertion_date",
                        "inserted_by",
                        "modified_by",
                        "search_column",
                        "lastModificationTime",
                        "isDeleted",
                        "ID"
                };
        updateQuery = getUpdateQuery2(columnNames);
        addQuery = getAddQuery2(columnNames);

        searchMap.put("name_en", " and (name_en like ?)");
        searchMap.put("name_bn", " and (name_bn like ?)");
        searchMap.put("job_grade_cat", " and (job_grade_cat = ?)");

        searchMap.put("AnyField", " AND (name_en LIKE ? OR name_bn LIKE ?)");
    }

    private final Map<String, String> searchMap = new HashMap<>();

    private static class LazyLoader {
        static final DesignationsDAO INSTANCE = new DesignationsDAO();
    }

    public static DesignationsDAO getInstance() {
        return LazyLoader.INSTANCE;
    }

    public void setSearchColumn(DesignationsDTO designationsDTO) {
        designationsDTO.searchColumn = "";
        designationsDTO.searchColumn += designationsDTO.nameEn + " ";
        designationsDTO.searchColumn += designationsDTO.nameBn + " ";
        designationsDTO.searchColumn += CatDAO.getName("English", "job_grade", designationsDTO.jobGradeCat) + " " + CatDAO.getName("Bangla", "job_grade", designationsDTO.jobGradeCat) + " ";

    }

    @Override
    public void set(PreparedStatement ps, DesignationsDTO designationsDTO, boolean isInsert) throws SQLException {
        int index = 0;
        setSearchColumn(designationsDTO);
        ps.setObject(++index, designationsDTO.nameEn);
        ps.setObject(++index, designationsDTO.nameBn);
        ps.setObject(++index, designationsDTO.jobGradeCat);
        ps.setObject(++index, designationsDTO.insertionDate);
        ps.setObject(++index, designationsDTO.insertedBy);
        ps.setObject(++index, designationsDTO.modifiedBy);
        ps.setObject(++index, designationsDTO.searchColumn);
        ps.setObject(++index, designationsDTO.lastModificationTime);
        if (isInsert) {
            ps.setObject(++index, designationsDTO.isDeleted);
        }
        ps.setObject(++index, designationsDTO.iD);
    }

    @Override
    public DesignationsDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            DesignationsDTO designationsDTO = new DesignationsDTO();
            int i = -1;
            designationsDTO.nameEn = rs.getString(columnNames[++i]);
            designationsDTO.nameBn = rs.getString(columnNames[++i]);
            designationsDTO.jobGradeCat = rs.getInt(columnNames[++i]);
            designationsDTO.insertionDate = rs.getLong(columnNames[++i]);
            designationsDTO.insertedBy = rs.getLong(columnNames[++i]);
            designationsDTO.modifiedBy = rs.getLong(columnNames[++i]);
            designationsDTO.searchColumn = rs.getString(columnNames[++i]);
            designationsDTO.lastModificationTime = rs.getLong(columnNames[++i]);
            designationsDTO.isDeleted = rs.getInt(columnNames[++i]);
            designationsDTO.iD = rs.getLong(columnNames[++i]);
            return designationsDTO;
        } catch (SQLException ex) {
            ex.printStackTrace();
            logger.error(ex);
            return null;
        }
    }

    public DesignationsDTO getDTOByID(long id) {
        return getDTOFromID(id);
    }

    @Override
    public String getTableName() {
        return "designations";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((DesignationsDTO) commonDTO, addQuery, true);
    }

    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((DesignationsDTO) commonDTO, updateQuery, false);
    }

    @Override
    public String getLastModifierUser() {
        return "modified_by";
    }

}
	