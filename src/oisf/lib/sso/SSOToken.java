package oisf.lib.sso;

import oisf.SSOPropertyReader;

public class SSOToken {
    private final SSOPropertyReader ssoPropertyReader;

    public SSOToken() throws Exception{
        this.ssoPropertyReader = SSOPropertyReader.getInstance();
    }
    public long getExpiryTime(){
        long curUtc = System.currentTimeMillis();
        return curUtc + Long.parseLong(this.ssoPropertyReader.getEtIntervalms());
    }
}
