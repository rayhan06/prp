package oisf.lib.Interfaces;

import oisf.lib.UserInformationDTO;
import oisf.lib.sso.SSOResponseDTO;

import javax.servlet.http.HttpServletRequest;

public interface IUserInformationRequest {
    UserInformationDTO getUserInfoModel(HttpServletRequest request, SSOResponseDTO ssoResponseDTO);
}
