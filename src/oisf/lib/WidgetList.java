package oisf.lib;

import okhttp3.*;

import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import oisf.lib.Interfaces.IWidgetList;
import oisf.lib.controller.Widget;
import oisf.SSOPropertyReader;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

public class WidgetList implements IWidgetList {
    private final OkHttpClient okHttpClient = new OkHttpClient();
    
    Logger logger = Logger.getLogger(WidgetList.class);

    public WidgetList() {
    }

    @Override
    public JSONArray getWidgetList(HttpServletRequest request) {
        ApiAccessToken apiAccessToken = new ApiAccessToken();
        String accessToken = apiAccessToken.getApiAccessToken(request);
        String designation = Long.toString(this.getDesignation(request));
        
        

        return this.get(accessToken, designation);
    }

    private JSONArray get(String accessToken, String designation) {
        try {
            String widgetUrl = String.format(SSOPropertyReader.getInstance().getWidgetUrl(), designation);

            Request request0 = new Request.Builder()
                    .url(widgetUrl)
                    .addHeader("Content-type", "application/json")
                    .addHeader("Authorization", "Bearer " + accessToken)
                    .build();

            try (Response response = okHttpClient.newCall(request0).execute()) {
                if (response.isSuccessful()) {
                    ResponseBody responseBody = response.body();
                    if (responseBody != null) {
                        return this.parseData(responseBody.string());
                    }
                }
            } catch (IOException e) {
                logger.error("",e);
                return null;
            }
        } catch (Exception e) {
            logger.error("",e);
        }
        return null;
    }

    private long getDesignation(HttpServletRequest request) {
    	
    	
        Object tempLong =    request.getSession().getAttribute(LibConstants.SSO_DESIGNATION);
        
        if(tempLong != null)
        {
        	long tempReturn2 = (long)tempLong;
        	
        	return tempReturn2;
        }
        
        return -1;
    }

    private JSONArray parseData(String data) throws ParseException {
        return (JSONArray) new JSONParser().parse(data);
    }
}
