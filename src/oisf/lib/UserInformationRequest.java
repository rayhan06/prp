package oisf.lib;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

import org.apache.log4j.Logger;
import org.json.simple.parser.ParseException;

import login.LoginServlet;
import oisf.lib.Interfaces.IUserInformationRequest;
import oisf.SSOPropertyReader;
import oisf.lib.sso.SSOResponseDTO;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

// After SSO complete
public class UserInformationRequest implements IUserInformationRequest {
    private final OkHttpClient okHttpClient = new OkHttpClient();
    Logger logger = Logger.getLogger(UserInformationRequest.class);

    public UserInformationRequest() {
    }

    @Override
    public UserInformationDTO getUserInfoModel(HttpServletRequest request, SSOResponseDTO ssoResponseDTO) {
        ApiAccessToken apiAccessToken = new ApiAccessToken();
        String accessToken = apiAccessToken.getApiAccessToken(request);
        return this.get(ssoResponseDTO.getUsername(), accessToken);
    }
    
    public UserInformationDTO getUserInfoModelForAPI(HttpServletRequest request, String userName) {
        ApiAccessToken apiAccessToken = new ApiAccessToken();
        String accessToken = apiAccessToken.getApiAccessToken(request);
      
        return null;
    }
    
    
    // depricated
    public UserInformationDTO getUserDetailsModelForAPI(HttpServletRequest request, String userName) {
        ApiAccessToken apiAccessToken = new ApiAccessToken();
        String accessToken = apiAccessToken.getApiAccessToken(request);
        return this.getDetails(userName,accessToken);
    }

    private UserInformationDTO get(String userName, String accessToken) {
        try {
            String url = String.format(SSOPropertyReader.getInstance().getEmployeeDetails(), userName);

            Request request0 = new Request.Builder()
                    .url(url)
                    .addHeader("Content-type", "application/json")
                    .addHeader("Authorization", "Bearer " + accessToken)
                    .build();

            try (Response response = okHttpClient.newCall(request0).execute()) {
                if (response.isSuccessful()) {
                    ResponseBody responseBody = response.body();
                    if (responseBody != null) {
                        return this.parseData(responseBody.string());
                    }
                }
            } catch (IOException e) {
                logger.error("",e);
                return null;
            }
        } catch (Exception e) {
            logger.error("",e);
        }
        return null;
    }
    
    // depricated
    private UserInformationDTO getDetails(String userId, String token ) {
    	 
    	OkHttpClient okHttpClient0 = new OkHttpClient();
    	try {
            String widgetUrl = String.format(SSOPropertyReader.getInstance().getEmployeeDetails(), userId);
            
            Request request0 = new Request.Builder()
                    .url(widgetUrl)
                    .addHeader("Content-type", "application/json")
                    .addHeader("Authorization", "Bearer " + token)
                    .build();

            try (Response response = okHttpClient0.newCall(request0).execute()) {
                if (response.isSuccessful()) {
                    ResponseBody responseBody = response.body();
                    if (responseBody != null) {
                        return this.parseData(responseBody.string());
                    	
                    }
                } else {
                	
                }
            } catch (IOException e) {
                logger.error("",e);
               
                return null;
            }
        } catch (Exception e) {
        	
            logger.error("",e);
        }
    	
        return null;
    }
    
    private UserInformationDTO parseData(String data) throws ParseException {
        return UserInformationDTO.parseJson(data);
    }
}
