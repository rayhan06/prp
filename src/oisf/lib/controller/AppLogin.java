package oisf.lib.controller;

import oisf.lib.Interfaces.ISSOAppLogin;
import oisf.lib.SSOAppLogin;
import org.apache.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/applogin")
public class AppLogin extends HttpServlet {
private static final Logger logger = Logger.getLogger(AppLogin.class);
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        try {
            ISSOAppLogin issoAppLogin = new SSOAppLogin();
            String url = issoAppLogin.getLandingUrl(request);

            response.sendRedirect(url);
        } catch (Exception ex) {
            logger.error("",ex);
        }
    }
}
