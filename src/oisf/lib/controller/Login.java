package oisf.lib.controller;

import oisf.lib.Interfaces.ISSOLogin;
import oisf.lib.SSOLogin;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/login")
public class Login extends HttpServlet {
private static final Logger logger = Logger.getLogger(Login.class);
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        try {
            ISSOLogin issoLogin = new SSOLogin();
            response.sendRedirect(issoLogin.getRedirectUrl(request));
        } catch (Exception e) {
            logger.error("",e);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}
