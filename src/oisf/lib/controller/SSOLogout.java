package oisf.lib.controller;

import oisf.lib.sso.AppLogoutRequest;
import org.apache.log4j.Logger;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/ssologout")
public class SSOLogout extends HttpServlet {
    private static final Logger logger = Logger.getLogger(SSOLogout.class);
    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        try {
            request.getSession().invalidate();
            AppLogoutRequest appLogoutRequest = new AppLogoutRequest();
            response.sendRedirect(appLogoutRequest.buildLogoutRequest());
        } catch (Exception e) {
            logger.error("",e);
        }
    }
}
