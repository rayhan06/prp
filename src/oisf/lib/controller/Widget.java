package oisf.lib.controller;

import org.apache.log4j.Logger;
import org.json.simple.JSONArray;

import login.LoginServlet;
import oisf.lib.Interfaces.IWidgetList;
import oisf.lib.WidgetList;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/Widget")
public class Widget extends HttpServlet {
	
	Logger logger = Logger.getLogger(Widget.class);

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("application/json;charset=UTF-8");
        IWidgetList iWidgetList = new WidgetList();
        JSONArray widgets = iWidgetList.getWidgetList(request);
        
        //logger.debug("Served at widget: ************************************** " + widgets.toString());

        PrintWriter out = response.getWriter();
        out.print(widgets);
        out.flush();
    }
}
