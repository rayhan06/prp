package oisf.lib.controller;

import oisf.lib.Interfaces.ISSOLogout;
import oisf.lib.SSOLogout;
import org.apache.log4j.Logger;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/Logout")
public class Logout extends HttpServlet {
    private static final Logger logger = Logger.getLogger(Logout.class);
    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        ISSOLogout issoLogout = new SSOLogout();
        try {
            response.sendRedirect(issoLogout.getRedirectUrl(request));
        } catch (IOException e) {
            logger.error("",e);
        }
    }
}
