package oisf;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.Properties;

import config.GlobalConfigConstants;
import config.GlobalConfigurationRepository;

public class SSOPropertyReader {
    private static SSOPropertyReader ssoFileReader = null;
    private static String appName = "";
    private static String appId = "";
    private static String secret = "";
    private static String idpUrl = "";
    private static String idpPort = "";
    private static String appNameQS = "";
    private static String appLoginEndPoint = "";
    private static String iaLoginEndPoint = "";
    private static String etIntervalms = "";
    private static String redirectUri = "";
    private static String landingPageUri = "";
    private static String loginPageUri = "";
    private static String apiAuth = "";
    private static final String ssoPropertiesFileName = "sso.properties";
    private static String employeeDetails = "";
    private static String createTokenUrl = "";
    private static String widgetUrl = "";

    public SSOPropertyReader() throws Exception{
//        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
//        InputStream resource = classLoader.getResourceAsStream(ssoPropertiesFileName);
//
//        Path temp = Files.createTempFile("sso-", ".properties");
//        Files.copy(classLoader.getResourceAsStream("sso.properties"), temp, StandardCopyOption.REPLACE_EXISTING);
//        FileInputStream fileInput = new FileInputStream(temp.toFile());
//        Properties properties = new Properties();
//        properties.load(fileInput);
//        fileInput.close();
//        appId = properties.getProperty("appId");
//        appName = properties.getProperty("appName");
//        secret = properties.getProperty("sharedSecret");
//        idpUrl = properties.getProperty("idpurl");
//        appNameQS = properties.getProperty("appNameQS");
//        appLoginEndPoint = properties.getProperty("apploginendpoint");
//        iaLoginEndPoint = properties.getProperty("ialoginendpoint");
//        etIntervalms = properties.getProperty("etintervalms");
//        redirectUri = properties.getProperty("redirectUri");
//        landingPageUri = properties.getProperty("landingPageUri");
//        loginPageUri = properties.getProperty("loginpageuri");
//        idpPort = properties.getProperty("idpport");
//        apiAuth = properties.getProperty("apiAuth");
//        employeeDetails = properties.getProperty("employeeDetails");
//        createTokenUrl = properties.getProperty("createTokenUrl");
//        widgetUrl = properties.getProperty("widgetUrl");
//        
//        
//        
//
//        if(appId == null || appId.equals("")){
//            throw new Exception("App id is null or empty. Could not read app id from property file. Please set app id in properties file");
//        }
//
//        if(appName == null || appName.equals("")){
//            throw new Exception("App name is null or empty. Could not read app name from property file. Please set ap pname in properties file.");
//        }
//
//        if(secret == null || secret.equals("")){
//            throw new Exception("App id is null or empty. Could not read app id from property file. Please set app id in properties file");
//        }
//
//        if(idpUrl == null || idpUrl.equals("")){
//            throw new Exception("App id is null or empty. Could not read app id from property file. Please set app id in properties file");
//        }
//
//        if(appNameQS == null || appNameQS.equals("")){
//            throw new Exception("App id is null or empty. Could not read app id from property file. Please set app id in properties file");
//        }
//
//        if(appLoginEndPoint == null || appLoginEndPoint.equals("")){
//            throw new Exception("App login end point is null or empty. Could not read app id from property file. Please set app id in properties file");
//        }
//
//        if(iaLoginEndPoint == null || iaLoginEndPoint.equals("")){
//            throw new Exception("Inter app login end point is null or empty. Could not read dashboardLoginEndPoint from property file. Please set up dashboardLoginEndPoint in properties file");
//        }
//
//        if(this.etIntervalms == null || etIntervalms.equals("")){
//            throw new Exception("Expiry time interval is null or empty. Could not read etIntervalms from property file. Please set up etIntervalms in properties file");
//        }
//
//        if(loginPageUri == null || loginPageUri.equals("")){
//            throw new Exception("Login page uri is null or empty. Could not read loginpageuri from property file. Please set up loginpageuri in properties file");
//        }
    }

    private synchronized static void createSSOFileReader() throws Exception{
        if (ssoFileReader == null) {
            ssoFileReader = new SSOPropertyReader();
        }
    }

    public synchronized static SSOPropertyReader getInstance() throws Exception{
        if (ssoFileReader == null) {
            createSSOFileReader();
        }
        return ssoFileReader;
    }

    public  String getAppId(){
    	appId = GlobalConfigurationRepository.getGlobalConfigDTOByID(GlobalConfigConstants.OISF_APPID).value;
		
    	return appId;}
    public  String getAppName(){
    	appName = GlobalConfigurationRepository.getGlobalConfigDTOByID(GlobalConfigConstants.OISF_APPNAME).value;
    	return appName;}
    public  String getSecret(){
    	secret = GlobalConfigurationRepository.getGlobalConfigDTOByID(GlobalConfigConstants.OISF_SHARED_SECRET).value;
    	return secret;}
    public  String getIdpUrl(){
    	idpUrl =  GlobalConfigurationRepository.getGlobalConfigDTOByID(GlobalConfigConstants.OISF_IDPURL).value;
    	return idpUrl;}
    public  String getIdpPort(){
    	idpPort = GlobalConfigurationRepository.getGlobalConfigDTOByID(GlobalConfigConstants.OISF_IDPPORT).value;
    	return idpPort;}
    public  String getAppNameQS(){
    	appNameQS =  GlobalConfigurationRepository.getGlobalConfigDTOByID(GlobalConfigConstants.OISF_APP_NAME_QS).value;
    	return appNameQS;}
    public  String getAppLoginEndPoint(){
    	appLoginEndPoint = GlobalConfigurationRepository.getGlobalConfigDTOByID(GlobalConfigConstants.OISF_APP_LOGIN_END_POINT).value;
    	return appLoginEndPoint;}
    public  String getIALoginEndPoint(){
    	iaLoginEndPoint = GlobalConfigurationRepository.getGlobalConfigDTOByID(GlobalConfigConstants.OISF_IA_LOGIN_ENDPOINT).value;
    	return iaLoginEndPoint;}
    public  String getEtIntervalms(){
    	etIntervalms = GlobalConfigurationRepository.getGlobalConfigDTOByID(GlobalConfigConstants.OISF_ETINTERVALMS).value;
    	return etIntervalms;}
    public  String getRedirectUri(){
    	redirectUri = GlobalConfigurationRepository.getGlobalConfigDTOByID(GlobalConfigConstants.OISF_REDIRECT_URIi).value;
    	return redirectUri;}
    public  String getLandingPageUri(){
    	landingPageUri = GlobalConfigurationRepository.getGlobalConfigDTOByID(GlobalConfigConstants.OISF_LANDING_PAGE_URI).value;
    	return landingPageUri;}
    public String getLoginPageUri(){
    	loginPageUri = GlobalConfigurationRepository.getGlobalConfigDTOByID(GlobalConfigConstants.OISF_LOGIN_PAGE_URI).value;
    	return loginPageUri;}
    public String getApiAuth(){
    	apiAuth = GlobalConfigurationRepository.getGlobalConfigDTOByID(GlobalConfigConstants.OISF_API_AUTH).value;
    	return apiAuth;}
    public String getEmployeeDetails(){
    	employeeDetails = GlobalConfigurationRepository.getGlobalConfigDTOByID(GlobalConfigConstants.OISF_EMPLOYEE_DETAILS).value;
    	
    	return employeeDetails;}
    public String getCreateTokenUrl(){
    	createTokenUrl = GlobalConfigurationRepository.getGlobalConfigDTOByID(GlobalConfigConstants.OISF_CREATE_TOKEN_URL).value;
    	return createTokenUrl;}
    
    public String getWidgetUrl(){
    	widgetUrl = GlobalConfigurationRepository.getGlobalConfigDTOByID(GlobalConfigConstants.OISF_WIDGET_URL).value;
    	return widgetUrl;}
    
    
}
