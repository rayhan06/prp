package oisf;

import java.io.IOException;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import util.StringUtils;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import org.apache.log4j.Logger;
import org.json.JSONException;
//import org.json.JSONObject;
import org.json.simple.JSONObject;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import oisf.lib.LibConstants;
import oisf.lib.UserInformationDTO;
import oisf.lib.UserInformationRequest;
import oisf.lib.sso.SSOResponseDTO;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
//import common.StringUtils;
import sessionmanager.SessionConstants;
//import org.springframework.web.servlet.ModelAndView;
//import org.springframework.security.core.Authentication;

public class OisfDAO {
	
	
	public static final okhttp3.MediaType JSON = okhttp3.MediaType.parse("application/json; charset=utf-8");
	
	private RestTemplate restTemplate;
	
	private SSOPropertyReader m_SSOPropertyReader;
	
	Logger logger = Logger.getLogger(getClass());
	
	public OisfDAO()
	{
		try
		{
			 m_SSOPropertyReader = new SSOPropertyReader();
		}
		catch(Exception ex)
		{
			
		}
	}
	
	public void getToken()
	{
		
	}
	
//	 public String getBaseUrlWithPort() {
//	        return baseUrl + ":" + port;
//	    }
	
	 public Object getAuthenticationToken(){
		 
		 
		    restTemplate = new RestTemplate();
		    
		    SSOPropertyReader tempReader;
			try {
				
			
	        HttpHeaders headers = new HttpHeaders();
	        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
	        headers.add("Authorization", "Secret " + m_SSOPropertyReader.getSecret());

	        MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
	        map.add("grant_type", "client_credentials");
	        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);
	        Map<String, String> resultObject = null;
		        try {
		        	//String tempBaseUrl = m_SSOPropertyReader.getIdpUrl() + ":" + m_SSOPropertyReader.getIdpPort();
		            resultObject = restTemplate.postForObject(getBaseUrlWithPort() + "/token/create", request, Map.class);
		        } catch (HttpClientErrorException e){
		           // log.error(e.getMessage());
		            return null;
		        }
		        
		        return resultObject.get("token");
	        
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				logger.error("",e1);
			}
			
			return null;
	        
	    }
	 
	 public String getBaseUrlWithPort() {
				
		 String tempBaseUrl = m_SSOPropertyReader.getIdpUrl() + ":" + m_SSOPropertyReader.getIdpPort();
	     return tempBaseUrl;		 
		
	    }
	 
	 //nonce= "nonce"
	 
	 public Map<String, String> getLoginAuthorization(String authToken, String userName, String password, String nonce){
	        HttpHeaders headers = new HttpHeaders();
	        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
	        headers.add("Authorization", "Bearer " + authToken);

	        MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
	        map.add("uname", userName);
	        map.add("passwd", password);
	        map.add("nonce", nonce);

	        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);
	        Map<String, String> resultObject = restTemplate.postForObject(getBaseUrlWithPort() + "/idp/api/verify", request, Map.class);
	        return resultObject;
	    }
	 
	 
	
	public void processLogin(String inUserName, String inPassword)
	{
		//jsonVal = this.esbConnectorService.getLoginAuthorization(token.toString(), loginRequest.getUsername(), loginRequest.getPassword(), "nonce");
		 
		Map<String, String> jsonVal = null;
		
		 Object token = getAuthenticationToken();

         jsonVal = getLoginAuthorization(token.toString(), inUserName, inPassword, "nonce");

         //UserInformation userInformation = this.userDetailsService.getUserInformationByApi(jsonVal.get("username"));

		
	}
	
	
//	    public void redirectAfterLoginSuccess(HttpServletResponse response, Authentication authentication) throws IOException {
//	        if (authentication != null) {
//	            UserInformation userInformation = Utility.extractUserInformationFromAuthentication(authentication);
//	            if (userInformation.getUserType().equals(UserType.OISF_USER)) {
//	                 if (userInformation.getOisfUserType().equals(OISFUserType.HEAD_OF_OFFICE)) {
//	                    response.sendRedirect("/dashboard.do");
//	                } else {
//	                    response.sendRedirect("/viewGrievances.do");
//	                }
//	            } else if (userInformation.getUserType().equals(UserType.COMPLAINANT)) {
//	                response.sendRedirect("/viewGrievances.do");
//	            } else if (userInformation.getUserType().equals(UserType.SYSTEM_USER)) {
//	                response.sendRedirect("/viewOffice.do");
//	            }
//	        }
//	    }
	
	public String processLoginPage(HttpServletRequest request, String inLandingPageUrl )
	{
		
		 try {
             String landingPageUrl = inLandingPageUrl;//"http://119.148.4.20:8087/rootstocktest6/OisfServlet";//inLandingPageUrl;//request.getParameter("landing_page_uri");
             //log.info(landingPageUrl);
             AppLoginRequest appLoginRequest = new AppLoginRequest();
             appLoginRequest.setLandingPageUrl(landingPageUrl);
             
            // String tempLandingPageUri = "http://www.grs.gov.bd/login/success";

             String authnRequest = appLoginRequest.buildAuthnRequest(landingPageUrl);
             request.getSession().setAttribute("nonce", appLoginRequest.getNonce());
             //log.error("Applogin request nonce : " + appLoginRequest.getNonce() + "\nSession nonce: " + request.getSession().getAttribute("nonce"));
             return authnRequest;
         } catch (Exception e) {
			 logger.error("",e);
         }
		 
		 return null;
		
	}
	
	
	
	public SSOResponseDTO processLoginAPI(HttpServletRequest request, String userName, String password )
	{
		SSOResponseDTO tempReturn = null;
		
		
		
		 try {
			 
			 SSOPropertyReader ssoPropertyReader = SSOPropertyReader.getInstance();
			 
			 String apiAuth = ssoPropertyReader.getApiAuth();//"http://esb.beta.doptor.gov.bd:8280/idp/api/authenticate";
			 final OkHttpClient okHttpClient = new OkHttpClient();
			 
			 
			 try {

//				 RequestBody formBody = new FormBody.Builder()
//					        .add("username", userName)
//					        .add("password", password)
//					        .add("clientId", ssoPropertyReader.getAppId())
//					        .build();
				 
				 JSONObject jsonObject = new JSONObject();
				   try {
				       jsonObject.put("username", userName);
				       jsonObject.put("password", password);
				       jsonObject.put("clientId", ssoPropertyReader.getAppId());

				   } catch (Exception e) {
				       logger.error("",e);
				   }
				 
				
				 RequestBody body = RequestBody.create(JSON, jsonObject.toString());
				 
				 Request req = new Request.Builder()
					        .url(apiAuth)
					        .post(body)
					        .build();

		            try (Response response = okHttpClient.newCall(req).execute()) {
		                if (response.isSuccessful()) {
		                    ResponseBody responseBody = response.body();
		                    if (responseBody != null) {
		                        
		                        JSONObject tempObj = this.parseData(responseBody.string());
		                        		                        		                        
		                        Map tempData = ((Map)tempObj.get("data"));
		                        
		                        String tempToken2 = (String)tempData.get("token");
		                       
		                       // UserInformationDTO r = getOisfName(request, "100000015944");
		                        
	                        LoginResponse tempResponse = new LoginResponse();		                		
		               				               		
		               		 	tempReturn = tempResponse.parseResponse2(tempToken2);               		 	
	               			
		               		 	
		               		 	UserInformationDTO d = getUserDetailsAPI(request, tempReturn.getUsername());
	               		 	
		                    	
		                    	SSOResponseDTO e = new SSOResponseDTO();
		                    	e.setUsername(userName);
		                    	e.setOfficeUnitOrgId(d.getOfficeUnitOrganogramId());
		                    	e.setDesignation(d.getDesignation_bng());
		                    	e.setname_bn(d.getName_bng());
		                    	e.setOfficeNameBng(d.getOffice_name_bng());
		                    	
		               		
		               		
		               		return e;		               		

		                    }
		                }
		            } catch (IOException e) {
		            	
		            	
		                logger.error("",e);
		                return null;
		            }
		        } catch (Exception e) {
		        	
		        	
		            logger.error("",e);
		        }
			 
			 
			 
			 
         } catch (Exception e) {
             logger.error("",e);
         }
		 
		 return null;
		
	}
	
	
	private UserInformationDTO getUserDetailsAPI(HttpServletRequest request, String username) {
		
		
		UserInformationRequest a = new UserInformationRequest();
		UserInformationDTO d = a.getUserDetailsModelForAPI(request, username);
		
		return d;
	}
	
	public UserInformationDTO getOisfName(HttpServletRequest request, String userName)
	{
		UserInformationRequest tempObj = new UserInformationRequest();
		UserInformationDTO tempUser = tempObj.getUserInfoModelForAPI(request, userName);

		return tempUser;
	}
	
	public UserInformationDTO getOisfName(HttpServletRequest request, long inOrganogramID)
	{
		
		//todo -- finish
		
		
		UserInformationRequest tempObj = new UserInformationRequest();
		UserInformationDTO tempUser = new UserInformationDTO();//tempObj.getUserDetailsModelForAPI(request, userName);
		
		tempUser.setName_bng("Sailful Islam");
		
		tempUser.setDesignation_bng("DG");

		return tempUser;
	}
	
	
	
//	public UserInformationDTO getOisfName2(HttpServletRequest request, String userName)
//	{
//		UserInformationRequest a = new UserInformationRequest();
//		UserInformationDTO d = a.getUserInfoModelForAPI(request, userName);
//
//		return d;
//	}
	
	public SSOResponseDTO getRefreshToken_old(HttpServletRequest request)
	{
		SSOResponseDTO tempReturn = null;
		
		 try {
			 
			 SSOPropertyReader ssoPropertyReader = SSOPropertyReader.getInstance();
			 
			 String apiAuth = ssoPropertyReader.getApiAuth();//"http://esb.beta.doptor.gov.bd:8280/idp/api/authenticate";
			 final OkHttpClient okHttpClient = new OkHttpClient();
			 
			 
			 try {

//				 RequestBody formBody = new FormBody.Builder()
//					        .add("username", userName)
//					        .add("password", password)
//					        .add("clientId", ssoPropertyReader.getAppId())
//					        .build();
				 
				 JSONObject jsonObject = new JSONObject();
				   try {
				       jsonObject.put("username", "100000015944");
				       jsonObject.put("password", "02522016");
				       jsonObject.put("clientId", "tg45ALrfOnpUtXtm8QYK1hBJHDEp71Y4");

				   } catch (Exception e) {
				       logger.error("",e);
				   }
				 
				
				 RequestBody body = RequestBody.create(JSON, jsonObject.toString());
				 
				 Request req = new Request.Builder()
					        .url(apiAuth)
					        .post(body)
					        .build();

		            try (Response response = okHttpClient.newCall(req).execute()) {
		                if (response.isSuccessful()) {
		                    ResponseBody responseBody = response.body();
		                    if (responseBody != null) {
		                        
		                        JSONObject tempObj = this.parseData(responseBody.string());
		                        		                        		                        
		                        Map tempData = ((Map)tempObj.get("data")); 
		                        
		                        String tempToken2 = (String)tempData.get("token");
		                       
		                        
		                        LoginResponse tempResponse = new LoginResponse();		                		
		               				               		
		               		 tempReturn = tempResponse.parseResponse2(tempToken2 );
		               		
		               		return tempReturn;		               		

		                    }
		                }
		            } catch (IOException e) {
		                logger.error("",e);
		                return null;
		            }
		        } catch (Exception e) {
		            logger.error("",e);
		        }
			 
			 
			 
			 
         } catch (Exception e) {
             logger.error("",e);
         }
		 
		 return null;
		
	}
	
    private JSONObject parseData(String data) throws ParseException {
    	
    	
    	Object temp = new JSONParser().parse(data);
    	
    	JSONObject temp1 = (JSONObject) temp;
    	
    	
        return temp1;
    }
	
	public void processPassport()
	{
		
	}
	
	public void processBirth()
	{
		
	}
	
 //method for api call	
	 public Object getObjectFromESB(String URL, Class objectClass) {
		 
		 try
		 {
	        HttpHeaders headers = new HttpHeaders();
	        headers.add("Authorization", "Bearer " + SSOPropertyReader.getInstance().getSecret());//Constant.OISF_ACCESS_TOKEN);
	        HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
	        ResponseEntity<?> responseEntity = null;
	        try {
	            responseEntity = restTemplate.exchange(getBaseUrlWithPort() + URL, HttpMethod.GET, entity, objectClass);
	        }catch (HttpClientErrorException e){
	           // log.error(e.getMessage());
	            if(e.getStatusCode().value() == 401) {
	                Object token = getAuthenticationToken();
	                String auth = token== null ? null : token.toString();
	                int attemptCount = 0;
	                while (!StringUtils.isValidString(auth) && attemptCount < 10) {
	                    token = getAuthenticationToken();
	                    auth = token== null ? null : token.toString();
	                    attemptCount++;
	                }
	                if (attemptCount >= 10) {
	                  //  log.error("Could not get authentication token. Attempt count = " + attemptCount);
	                    return null;
	                }
	                SessionConstants.OISF_ACCESS_TOKEN = auth;
	                headers.clear();
	                headers.add("Authorization", "Bearer " + SessionConstants.OISF_ACCESS_TOKEN);
	                HttpEntity<String> newEntity = new HttpEntity<String>("parameters", headers);
	                attemptCount = 0;
	                while (attemptCount < 10){
	                    try {

	                        responseEntity = restTemplate.exchange(getBaseUrlWithPort() + URL, HttpMethod.GET, newEntity, objectClass);
	                    } catch (Exception exception) {
	                        // Output expected SocketTimeoutExceptions.
	                        //log.error(exception.getMessage());
	                        attemptCount++;
	                        continue;
	                    }
	                    break;
	                }
	            }
	        }
	      //  log.info(responseEntity.getStatusCode().getReasonPhrase());
	        return responseEntity.getBody();
	    }
		 
	 
		 catch(Exception ex)
		 {
			 return null;
		 }
	 }
	
	

}
