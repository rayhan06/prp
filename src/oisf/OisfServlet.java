package oisf;

import java.io.IOException;
import java.util.UUID;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import forgetPassword.VerificationConstants;
import login.LoginDTO;
import login.LoginService;
import login.LoginServlet;
import login.RememberMeOptionDAO;
import oisf.*;
import oisf.lib.LibConstants;
import oisf.lib.UserInformationDTO;
import oisf.lib.sso.SSOResponseDTO;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.ServletConstant;

/**
 * Servlet implementation class OisfServlet
 */
@WebServlet("/OisfServlet/applogin")
public class OisfServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	Logger logger = Logger.getLogger(LoginServlet.class);
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public OisfServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		String token = "";
		
		String tempNonce = "";
		try
		{
		LoginResponse tempResponse = new LoginResponse();
		
		 token = request.getParameter("token");
		 
		  tempNonce = (String)request.getSession().getAttribute("nonce");//, appLoginRequest.getNonce());
		
		SSOResponseDTO tempDto = tempResponse.parseResponse2(token,tempNonce );
		
		if(tempDto == null)
		{
			//response.getWriter().append("Served at 1: ");
			logger.debug("Served at 1: ");
		}
		else
		{
			//response.getWriter().append("Served at 2: ").append(tempDto.getDesignation());
			
			//logger.debug("Served at 2: ");
			request.setAttribute("ID", -1L);
			
			request.getSession().setAttribute(LibConstants.SSO_DESIGNATION,tempDto.getOfficeUnitOrgId());
			//request.getSession().setAttribute("name",tempDto.getUsername());
			request.getSession().setAttribute("designation",tempDto.getDesignation());
			request.getSession().setAttribute("officeName",tempDto.getOfficeNameBng());
			
			OisfDAO tempOisfDao = new OisfDAO();
			
			UserInformationDTO tempObj = tempOisfDao.getOisfName(request, tempDto.getUsername());
			
			request.getSession().setAttribute("name",tempObj.getName_bng());
			
			//logger.debug("Served at 2: ****************************** " + );
			
			
			
			//getName();
			
			
			//RequestDispatcher requestDispatcher = request.getRequestDispatcher("../home/welcome.jsp");
			//requestDispatcher.forward(request, response);
			
			
			
			
			
			//request.setAttribute(LibConstants.SSO_DESIGNATION,tempDto.getDesignation() );
			
			// get his dashboard page from db
			
			
			String loginUrl = "home/login.jsp";
			String homeUrl = "home/index.jsp";
			String otpUrl = "home/otpVerifier.jsp";

			LoginService service = new LoginService();
			
			UserDTO loginUserDTO = UserRepository.getUserDTOByOrganogramID(tempDto.getOfficeUnitOrgId());
			
			logger.debug("role2 :**************************** " + loginUserDTO.roleID + "    "+ tempDto.getOfficeUnitOrgId());
			if(service.needOTPCheck(loginUserDTO, request))
				{
				LoginDTO loginDTO = new LoginDTO();
				loginDTO.isOisf = 1;
				loginDTO.userID = tempDto.getOfficeUnitOrgId();
				
				String tempLoginIP = (String)request.getSession(true).getAttribute(VerificationConstants.LOGIN_IP);
				
				loginDTO.loginSourceIP = tempLoginIP;
				
				request.getSession().removeAttribute(SessionConstants.USER_LOGIN);
				
				//request.getSession().setAttribute(SessionConstants.USER_LOGIN, loginDTO);
				request.getSession().setAttribute(VerificationConstants.CACHE_LOGIN_USERNAME, tempDto.getUsername());
				response.sendRedirect(otpUrl);
				
				}
			else
			{	//valid login
				if(request.getParameter("stay_logged_in")!=null){
					Cookie cookie = new Cookie(ServletConstant.REMEMBER_ME_COOKIE_NAME, UUID.randomUUID().toString());
					cookie.setMaxAge(60 * 60 * 24 * 365 * 10);
					RememberMeOptionDAO.getInstance().insertRememberMeOption(tempDto.getOfficeUnitOrgId(), cookie.getValue(),1);
					response.addCookie(cookie);
				}
				
				
				String tempLoginIP = (String)request.getSession(true).getAttribute(VerificationConstants.LOGIN_IP);
				
				String tempUserName = (String) request.getSession(true).getAttribute(VerificationConstants.CACHE_LOGIN_USERNAME);
				
				LoginDTO tempLoginDto = new LoginDTO();
				tempLoginDto.userID = tempDto.getOfficeUnitOrgId();
				tempLoginDto.isOisf = 1;
				tempLoginDto.loginSourceIP = tempLoginIP;
				
				if(tempUserName == null)
				{
					tempUserName = tempDto.getUsername();
				}
				
				
				service.processValidLogin(tempLoginDto, tempUserName, request);
				String lastVisitedUrl = service.getLastRequestedUrl(request);
				if(lastVisitedUrl != null && !lastVisitedUrl.endsWith("LogoutServlet")) response.sendRedirect(lastVisitedUrl);
				else response.sendRedirect(homeUrl);
			}
			
			
			
			
			
			
			
			//RequestDispatcher requestDispatcher = request.getRequestDispatcher("../home/welcome.jsp");
			//RequestDispatcher requestDispatcher = request.getRequestDispatcher("../home/welcome.jsp");
			//requestDispatcher.forward(request, response);
			
		}
		
		
		}
		catch(Exception ex)
		{
			logger.debug("Served at 3: " + token + " : "+ tempNonce + ":" + ex);
		}
		
		
		
		
		
	}
	
	private void getName()
	{
		
		//request.getSession().setAttribute("name",tempDto.getUsername());
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
