package restriction_summary_report;


import java.io.IOException;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import language.LC;
import language.LM;

import login.LoginDTO;
import permission.MenuConstants;

import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;

import util.ReportRequestHandler;
import pbReport.*;






@WebServlet("/Restriction_summary_report_Servlet")
public class Restriction_summary_report_Servlet  extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	String[][] Criteria =
	{
		{"criteria","","employee_record_id","=","","int","","","any","employeeRecordId", LC.HM_EMPLOYEE_ID + ""},		
		{"criteria","","restriction_cat","=","AND","int","","","any","restrictionCat", LC.RESTRICTION_SUMMARY_REPORT_WHERE_RESTRICTIONCAT + ""},
		{"criteria","","restriction_added","=","AND","int","","","any","restrictionAdded",  ""},
		{"criteria","","inserted_by","=","AND","int","","","any","insertedBy", LC.RESTRICTION_SUMMARY_REPORT_WHERE_INSERTEDBY + ""},		
		{"criteria","","insertion_date",">=","AND","long","","",Long.MIN_VALUE + "","startDate", LC.HM_START_DATE + ""},		
		{"criteria","","insertion_date","<=","AND","long","","",Long.MAX_VALUE + "","endDate", LC.HM_END_DATE + ""}		
	};
	
	String[][] Display =
	{
		{"display","","employee_record_id","erIdToName",""},		
		{"display","","restriction_cat","cat",""},		
		{"display","","inserted_by","erIdToName",""},		
		{"display","","insertion_date","date",""},	
		{"display","","restriction_added","boolean",""},
		{"display","","restriction_reason","text",""},
		{"display","","employee_record_id","hidden",""}
	};
	
	String GroupBy = "";
	String OrderBY = "lastModificationTime desc";
	
	ReportRequestHandler reportRequestHandler;
	
	public Restriction_summary_report_Servlet(){

	}
	
	private final ReportService reportService = new ReportService();
	
	private String sql;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String language = "english";
		if(userDTO!= null && userDTO.languageID == SessionConstants.BANGLA)
		{
			language = "bangla";
		}
		boolean isLangEng = language.equalsIgnoreCase("english");
				
		String actionType = request.getParameter("actionType");

		System.out.println("In ssservlet doget, actiontype = " + actionType);
		
		sql = "restriction_summary";

		Display[0][4] = LM.getText(LC.HM_EMPLOYEE_ID, loginDTO);
		Display[1][4] = isLangEng?"Restriction Category":"রেস্ট্রিকশনের ধরন";
		Display[2][4] = isLangEng?"Action Taken By":"পদক্ষেপগ্রহীতা";
		Display[3][4] = LM.getText(LC.HM_DATE, loginDTO);
		Display[4][4] = isLangEng?"Is Restricted":"রেস্ট্রিকটেড আছেন";
		Display[5][4] = isLangEng?"Restriction Reason":"রেস্ট্রিকশনের কারণ";
		Display[6][4] = "";
		
		String reportName = isLangEng?"Restriction Status Report":"রেস্ট্রিকশনের অবস্থা রিপোর্ট";
		
		reportRequestHandler = new ReportRequestHandler(null,
				Criteria, Display, GroupBy, OrderBY, sql,
				reportService);

		
		reportRequestHandler.handleReportGet(request, response, userDTO, "restriction_summary_report",
				MenuConstants.RESTRICTION_SUMMARY_REPORT_DETAILS, language, reportName, "restriction_summary_report");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		doGet(request, response);
	}
}
