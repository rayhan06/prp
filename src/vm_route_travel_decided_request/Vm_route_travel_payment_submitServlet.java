package vm_route_travel_decided_request;

import com.google.gson.Gson;
import files.FilesDAO;
import login.LoginDTO;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import pb.Utils;
import permission.MenuConstants;
import role.PermissionRepository;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;
import util.CommonRequestHandler;
import util.RecordNavigationManager4;
import vm_fuel_request.InvalidDataException;
import vm_requisition.CommonApprovalStatus;
import vm_route_travel.Vm_route_travelDAO;
import vm_route_travel.Vm_route_travelDTO;
import vm_route_travel.Vm_route_travelRepository;
import vm_route_travel_approver.Vm_route_travel_approverDAO;
import vm_route_travel_approver.Vm_route_travel_approverDTO;
import vm_route_travel_approver.Vm_route_travel_approverRepository;
import vm_route_travel_payment.Vm_route_travel_paymentDAO;
import vm_route_travel_payment.Vm_route_travel_paymentDTO;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


/**
 * Servlet implementation class Vm_route_travel_payment_submitServlet
 */
@WebServlet("/Vm_route_travel_payment_submitServlet")
@MultipartConfig
public class Vm_route_travel_payment_submitServlet extends HttpServlet
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Vm_route_travel_payment_submitServlet.class);

    String tableName = "vm_route_travel";

	Vm_route_travelDAO vm_route_travelDAO;
	CommonRequestHandler commonRequestHandler;
	FilesDAO filesDAO = new FilesDAO();
    private final Gson gson = new Gson();

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Vm_route_travel_payment_submitServlet()
	{
        super();
    	try
    	{
			vm_route_travelDAO = new Vm_route_travelDAO(tableName);
			commonRequestHandler = new CommonRequestHandler(vm_route_travelDAO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		System.out.println("In doget request = " + request);
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		try
		{
			String actionType = request.getParameter("actionType");

			if (!actionType.equals("search")) {
				long approverOrgId = userDTO.organogramID;
				List<Vm_route_travel_approverDTO> approver = Vm_route_travel_approverRepository.getInstance().getVm_route_travel_approverDTOByapprover_org_id(approverOrgId);

				if (approver.isEmpty()) {
					throw new InvalidDataException("Invalid employee request to approve");
				}

//				if (request.getParameter("ID") != null) {
//					String pendingFilter = " (status = " + CommonApprovalStatus.SATISFIED.getValue() + " OR status = " + CommonApprovalStatus.WITHDRAWN.getValue() + ") AND ID = " + Long.parseLong(request.getParameter("ID")) + " ";
//					int pendingRequest = vm_route_travelDAO.getCount(null, 1, 0, true, userDTO, pendingFilter, false);
//					if (pendingRequest != 1) {
//						throw new card_info.InvalidDataException("No Decided approval is found for id : "+ request.getParameter("ID"));
//					}
//				}
			}

			if(actionType.equals("getAddPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_ROUTE_TRAVEL_ADD))
				{
					commonRequestHandler.getAddPage(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getEditPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_ROUTE_TRAVEL_UPDATE))
				{
					getVm_route_travel(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("downloadDropzoneFile"))
			{
				commonRequestHandler.downloadDropzoneFile(request, response, filesDAO);
			}
			else if(actionType.equals("DeleteFileFromDropZone"))
			{
				commonRequestHandler.deleteFileFromDropZone(request, response, filesDAO);
			}
			else if(actionType.equals("getURL"))
			{
				String URL = request.getParameter("URL");
				System.out.println("URL = " + URL);
				response.sendRedirect(URL);
			}
			else if(actionType.equals("search"))
			{
				System.out.println("search requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_ROUTE_TRAVEL_SEARCH))
				{
					if(isPermanentTable)
					{
						String filter = request.getParameter("filter");
						System.out.println("filter = " + filter);
						if(filter!=null)
						{
							filter = ""; //shouldn't be directly used, rather manipulate it.
							searchVm_route_travel(request, response, isPermanentTable, filter);
						}
						else
						{
							searchVm_route_travel(request, response, isPermanentTable, "");
						}
					}
					else
					{
						//searchVm_route_travel(request, response, tempTableName, isPermanentTable);
					}
				}
			}
//			else if(actionType.equals("view"))
//			{
//				System.out.println("view requested");
//				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_ROUTE_TRAVEL_SEARCH))
//				{
//					commonRequestHandler.view(request, response);
//				}
//				else
//				{
//					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
//				}
//
//			}
			else if(actionType.equals("view"))
			{
				System.out.println("view requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_ROUTE_TRAVEL_SEARCH))
				{
					searchVm_route_travelPayment(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}
			else
			{
				request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}





	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		// TODO Auto-generated method stub
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		System.out.println("doPost");
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);

		try
		{
			String actionType = request.getParameter("actionType");
			System.out.println("actionType = " + actionType);

			if (!actionType.equals("search")) {
				long approverOrgId = userDTO.organogramID;
				List<Vm_route_travel_approverDTO> approver = Vm_route_travel_approverRepository.getInstance().getVm_route_travel_approverDTOByapprover_org_id(approverOrgId);

				if (approver.isEmpty()) {
					throw new InvalidDataException("Invalid employee request to approve");
				}

//				if (request.getParameter("iD") != null) {
//					String pendingFilter = " (status = " + CommonApprovalStatus.SATISFIED.getValue() + " OR status = " + CommonApprovalStatus.WITHDRAWN.getValue() + ") AND ID = " + Long.parseLong(request.getParameter("iD")) + " ";
//					int pendingRequest = vm_route_travelDAO.getCount(null, 1, 0, true, userDTO, pendingFilter, false);
//					if (pendingRequest != 1) {
//						throw new card_info.InvalidDataException("No Decided approval is found for id : "+ request.getParameter("iD"));
//					}
//				}
			}

//			if(actionType.equals("add"))
//			{
//
//				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_ROUTE_TRAVEL_PAYMENT_ADD))
//				{
//					System.out.println("going to  addVm_route_travel_payment ");
//					addVm_route_travel_payment(request, response, true, userDTO, true);
//				}
//				else
//				{
//					System.out.println("Not going to  addVm_route_travel_payment ");
//					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
//				}
//
//			}
			if(actionType.equals("UploadFilesFromDropZone"))
			{
				commonRequestHandler.UploadFilesFromDropZone(request, response, userDTO);
			}

			else if(actionType.equals("getDTO"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_ROUTE_TRAVEL_ADD))
				{
					getDTO(request, response);
				}
				else
				{
					System.out.println("Not going to  addVm_route_travel ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}
			else if(actionType.equals("edit"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_ROUTE_TRAVEL_PAYMENT_UPDATE))
				{
					addVm_route_travel_payment(request, response, false, userDTO, isPermanentTable);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("delete"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_ROUTE_TRAVEL_UPDATE))
				{
					commonRequestHandler.delete(request, response, userDTO);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("search"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_ROUTE_TRAVEL_SEARCH))
				{
					searchVm_route_travel(request, response, true, "");
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getGeo"))
			{
				System.out.println("going to geoloc ");
				request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
			}

		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}

	private void getDTO(HttpServletRequest request, HttpServletResponse response)
	{
		try
		{
			System.out.println("In getDTO");
			Vm_route_travelDTO vm_route_travelDTO = Vm_route_travelRepository.getInstance().getVm_route_travelDTOByID(Long.parseLong(request.getParameter("ID")));
			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");

			String encoded = this.gson.toJson(vm_route_travelDTO);
			System.out.println("json encoded data = " + encoded);
			out.print(encoded);
			out.flush();
		}
		catch (NumberFormatException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void addVm_route_travel_payment(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, boolean isPermanentTable) throws IOException
	{
		// TODO Auto-generated method stub
		try
		{
			request.setAttribute("failureMessage", "");
			System.out.println("%%%% addVm_route_travel_payment");
			String path = getServletContext().getRealPath("/img2/");
			Vm_route_travel_paymentDAO vm_route_travel_paymentDAO = new Vm_route_travel_paymentDAO();
//			Vm_route_travel_paymentDTO vm_route_travel_paymentDTO;
			SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");

//			if(addFlag == true)
//			{
//				vm_route_travel_paymentDTO = new Vm_route_travel_paymentDTO();
//			}
//			else
//			{
//				vm_route_travel_paymentDTO = (Vm_route_travel_paymentDTO)vm_route_travel_paymentDAO.getInstance().getVm_route_travelDTOByID(Long.parseLong(request.getParameter("iD")));
//			}

			String Value = "";

			Value = request.getParameter("routeTravelId");

			long routeTravelId = -1;

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("routeTravelId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				routeTravelId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			String[] existingDataTracker = {request.getParameter("existingDataTracker")};

			String[] isPaids = request.getParameterValues("isPaid");

			long finalRouteTravelId = routeTravelId;
			Utils.handleTransactionForNavigationService4(()->{
				if (isPaids != null) {
					int isPaidLength = isPaids.length;

					for (int index=0;index < isPaidLength; index++) {

						String[] combined = isPaids[index].split("_");

						long fiscalYearId = Long.parseLong(combined[0]);
						int month = Integer.parseInt(combined[1]);

						Vm_route_travel_paymentDTO vm_route_travel_paymentDTO = new Vm_route_travel_paymentDTO();

						if (!existingDataTracker[0].contains(isPaids[index])) {
							vm_route_travel_paymentDTO.insertedByUserId = userDTO.ID;
							vm_route_travel_paymentDTO.insertedByOrganogramId = userDTO.organogramID;

							Calendar c = Calendar.getInstance();
							c.set(Calendar.HOUR_OF_DAY, 0);
							c.set(Calendar.MINUTE, 0);
							c.set(Calendar.SECOND, 0);
							c.set(Calendar.MILLISECOND, 0);

							vm_route_travel_paymentDTO.insertionDate = c.getTimeInMillis();

						}

						vm_route_travel_paymentDTO.routeTravelId = finalRouteTravelId;

						vm_route_travel_paymentDTO.fiscalYearId = fiscalYearId;

						vm_route_travel_paymentDTO.isPaid = true;

						vm_route_travel_paymentDTO.month = month;


						System.out.println("Done adding  addVm_route_travel_payment dto = " + vm_route_travel_paymentDTO);
						long returnedID = -1;

						if (!existingDataTracker[0].contains(isPaids[index]))
						{
							returnedID = vm_route_travel_paymentDAO.manageWriteOperations(vm_route_travel_paymentDTO, SessionConstants.INSERT, -1, userDTO);
						}
						else
						{
							returnedID = vm_route_travel_paymentDAO.manageWriteOperations(vm_route_travel_paymentDTO, SessionConstants.UPDATE, -1, userDTO);
						}


						existingDataTracker[0] = existingDataTracker[0].replaceAll(" " + isPaids[index] + " ", "");

					}
				}

				if (!existingDataTracker[0].trim().isEmpty()) {
					String[] isUnpaids = existingDataTracker[0].trim().split("\\s+");
					int isUnpaidLength = isUnpaids.length;

					for (int index=0;index < isUnpaidLength; index++) {

						String[] combined = isUnpaids[index].split("_");

						long fiscalYearId = Long.parseLong(combined[0]);
						int month = Integer.parseInt(combined[1]);

						vm_route_travel_paymentDAO.hardDelete(finalRouteTravelId, fiscalYearId, month);

					}
				}
			});


//			if(isPermanentTable == false) //add new row for validation and make the old row outdated
//			{
//				vm_route_travel_paymentDAO.setIsDeleted(vm_route_travel_paymentDTO.iD, CommonDTO.OUTDATED);
//				returnedID = vm_route_travel_paymentDAO.add(vm_route_travel_paymentDTO);
//				vm_route_travel_paymentDAO.setIsDeleted(returnedID, CommonDTO.WAITING_FOR_APPROVAL);
//			}
//			else if(addFlag == true)
//			{
//				returnedID = vm_route_travel_paymentDAO.manageWriteOperations(vm_route_travel_paymentDTO, SessionConstants.INSERT, -1, userDTO);
//			}
//			else
//			{
//				returnedID = vm_route_travel_paymentDAO.manageWriteOperations(vm_route_travel_paymentDTO, SessionConstants.UPDATE, -1, userDTO);
//			}









			if(isPermanentTable)
			{
				response.sendRedirect("Vm_route_travel_payment_submitServlet?actionType=search");
			}
			else
			{
//				commonRequestHandler.validate(vm_route_travel_paymentDAO.getInstance().getVm_route_travelDTOByID(returnedID), request, response, userDTO);
			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}









	private void getVm_route_travel(HttpServletRequest request, HttpServletResponse response, long id) throws ServletException, IOException
	{
		System.out.println("in getVm_route_travel");
		Vm_route_travelDTO vm_route_travelDTO = null;
		try
		{
			vm_route_travelDTO = Vm_route_travelRepository.getInstance().getVm_route_travelDTOByID(id);
			request.setAttribute("ID", vm_route_travelDTO.iD);
			request.setAttribute("vm_route_travelDTO",vm_route_travelDTO);
			request.setAttribute("vm_route_travelDAO",vm_route_travelDAO);

			String URL= "";

			String inPlaceEdit = request.getParameter("inplaceedit");
			String inPlaceSubmit = request.getParameter("inplacesubmit");
			String getBodyOnly = request.getParameter("getBodyOnly");

			if(inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase(""))
			{
				URL = "vm_route_travel_payment_submit/vm_route_travelInPlaceEdit.jsp";
				request.setAttribute("inplaceedit","");
			}
			else if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
			{
				URL = "vm_route_travel_payment_submit/vm_route_travelSearchRow.jsp";
				request.setAttribute("inplacesubmit","");
			}
			else
			{
				if(getBodyOnly  != null && !getBodyOnly.equalsIgnoreCase(""))
				{
					URL = "vm_route_travel_payment_submit/vm_route_travelEditBody.jsp?actionType=edit";
				}
				else
				{
					URL = "vm_route_travel_payment_submit/vm_route_travelEdit.jsp?actionType=edit";
				}
			}

			RequestDispatcher rd = request.getRequestDispatcher(URL);
			rd.forward(request, response);
		}
		catch (NumberFormatException e)
		{
			e.printStackTrace();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}


	private void getVm_route_travel(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		getVm_route_travel(request, response, Long.parseLong(request.getParameter("ID")));
	}

	private void searchVm_route_travel(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException
	{
		System.out.println("in  searchVm_route_travel 1");
		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String ajax = request.getParameter("ajax");
		boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);

		String status = request.getParameter("status");
		if(status == null || status.trim().isEmpty()) filter = " (status = " + CommonApprovalStatus.SATISFIED.getValue() + " OR status = " + CommonApprovalStatus.WITHDRAWN.getValue() + ") ";

		long approverOrgId = userDTO.organogramID;
		List<Vm_route_travel_approverDTO> approver = Vm_route_travel_approverRepository.getInstance().getVm_route_travel_approverDTOByapprover_org_id(approverOrgId);
		if (approver.isEmpty()) {
			if (filter != null && !filter.isEmpty()) filter += " AND ";
			filter += " FALSE ";
		}

        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
			SessionConstants.NAV_VM_ROUTE_TRAVEL,
			request,
			vm_route_travelDAO,
			SessionConstants.VIEW_VM_ROUTE_TRAVEL,
			SessionConstants.SEARCH_VM_ROUTE_TRAVEL,
			tableName,
			isPermanent,
			userDTO,
			filter,
			true);
        try
        {
			System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        }
        catch(Exception e)
        {
			System.out.println("failed to dojob" + e);
        }

		request.setAttribute("vm_route_travelDAO",vm_route_travelDAO);
        RequestDispatcher rd;
        if(!isPermanent)
        {
        	if(hasAjax == false)
	        {
	        	System.out.println("Going to vm_route_travel_payment_submit/vm_route_travelApproval.jsp");
	        	rd = request.getRequestDispatcher("vm_route_travel_payment_submit/vm_route_travelApproval.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to vm_route_travel_payment_submit/vm_route_travelApprovalForm.jsp");
	        	rd = request.getRequestDispatcher("vm_route_travel_payment_submit/vm_route_travelApprovalForm.jsp");
	        }
        }
        else
        {
	        if(hasAjax == false)
	        {
	        	System.out.println("Going to vm_route_travel_payment_submit/vm_route_travelSearch.jsp");
	        	rd = request.getRequestDispatcher("vm_route_travel_payment_submit/vm_route_travelSearch.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to vm_route_travel_payment_submit/vm_route_travelSearchForm.jsp");
	        	rd = request.getRequestDispatcher("vm_route_travel_payment_submit/vm_route_travelSearchForm.jsp");
	        }
        }
		rd.forward(request, response);
	}

	private void searchVm_route_travelPayment(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		System.out.println("in  searchVm_route_travel 1");
		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String ajax = request.getParameter("ajax");
		boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);

		request.setAttribute("requester_org_id",request.getParameter("ID"));
		request.setAttribute("vm_route_travelDAO",vm_route_travelDAO);
		RequestDispatcher rd;

		{
			if(hasAjax == false)
			{
				System.out.println("Going to vm_route_travel_payment_submit/vm_route_travelSearchPayment.jsp");
				rd = request.getRequestDispatcher("vm_route_travel_payment_submit/vm_route_travelSearchPayment.jsp");
			}
			else
			{
				System.out.println("Going to vm_route_travel_payment_submit/vm_route_travelSearchPaymentForm.jsp");
				rd = request.getRequestDispatcher("vm_route_travel_payment_submit/vm_route_travelSearchPaymentForm.jsp");
			}
		}
		rd.forward(request, response);
	}

}

