package purchase_history_report;


import java.io.IOException;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import language.LC;
import language.LM;

import login.LoginDTO;
import permission.MenuConstants;

import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;

import util.ReportRequestHandler;
import pbReport.*;






@WebServlet("/Purchase_history_report_Servlet")
public class Purchase_history_report_Servlet  extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	String[][] Criteria =
	{
		{"criteria","pi_purchase_parent","purchase_order_number","like","","String","","","%","purchaseOrderNumber", LC.PURCHASE_HISTORY_REPORT_WHERE_PURCHASEORDERNUMBER + ""},		
		{"criteria","pi_purchase_parent","fiscal_year_type","=","AND","int","","","any","fiscalYearType", LC.PURCHASE_HISTORY_REPORT_WHERE_FISCALYEARTYPE + ""},		
		{"criteria","pi_purchase","pi_vendor_auctioneer_details_type","=","AND","int","","","any","piVendorAuctioneerDetailsType", LC.PURCHASE_HISTORY_REPORT_WHERE_PIVENDORAUCTIONEERDETAILSTYPE + ""},		
		{"criteria","pi_purchase_parent","purchase_date",">=","AND","long","","",Long.MIN_VALUE + "","startDate", LC.HM_START_DATE + ""},		
		{"criteria","pi_purchase_parent","purchase_date","<=","AND","long","","",Long.MAX_VALUE + "","endDate", LC.HM_END_DATE + ""}		
	};
	
	String[][] Display =
	{
		{"display","pi_purchase_parent","purchase_order_number","text",""},		
		{"display","pi_purchase_parent","fiscal_year_type","type",""},		
		{"display","pi_purchase_parent","purchase_date","date",""},		
		{"display","pi_purchase","pi_vendor_auctioneer_details_type","type",""},		
		{"display","pi_purchase","procurement_package_id","id",""},		
		{"display","pi_purchase","procurement_goods_type_id","id",""},		
		{"display","pi_purchase","procurement_goods_id","id",""},		
		{"display","pi_purchase","amount","text",""},		
		{"display","pi_purchase","unit_price","text",""},		
		{"display","pi_purchase","total_bill","text",""},		
		{"display","pi_purchase","purchase_item_remarks","text",""}		
	};
	
	String GroupBy = "";
	String OrderBY = "";
	
	ReportRequestHandler reportRequestHandler;
	
	public Purchase_history_report_Servlet(){

	}
	
	private final ReportService reportService = new ReportService();
	
	private String sql;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String language = "english";
		if(userDTO!= null && userDTO.languageID == SessionConstants.BANGLA)
		{
			language = "bangla";
		}
		boolean isLangEng = language.equalsIgnoreCase("english");
				
		String actionType = request.getParameter("actionType");

		System.out.println("In ssservlet doget, actiontype = " + actionType);
		
		sql = "pi_purchase_parent JOIN pi_purchase ON pi_purchase.pi_purchase_parent_id = pi_purchase_parent.id";

		Display[0][4] = LM.getText(LC.PURCHASE_HISTORY_REPORT_SELECT_PURCHASEORDERNUMBER, loginDTO);
		Display[1][4] = LM.getText(LC.PURCHASE_HISTORY_REPORT_SELECT_FISCALYEARTYPE, loginDTO);
		Display[2][4] = LM.getText(LC.PURCHASE_HISTORY_REPORT_SELECT_PURCHASEDATE, loginDTO);
		Display[3][4] = LM.getText(LC.PURCHASE_HISTORY_REPORT_SELECT_PIVENDORAUCTIONEERDETAILSTYPE, loginDTO);
		Display[4][4] = LM.getText(LC.PURCHASE_HISTORY_REPORT_SELECT_PROCUREMENTPACKAGEID, loginDTO);
		Display[5][4] = LM.getText(LC.PURCHASE_HISTORY_REPORT_SELECT_PROCUREMENTGOODSTYPEID, loginDTO);
		Display[6][4] = LM.getText(LC.PURCHASE_HISTORY_REPORT_SELECT_PROCUREMENTGOODSID, loginDTO);
		Display[7][4] = LM.getText(LC.PURCHASE_HISTORY_REPORT_SELECT_AMOUNT, loginDTO);
		Display[8][4] = LM.getText(LC.PURCHASE_HISTORY_REPORT_SELECT_UNITPRICE, loginDTO);
		Display[9][4] = LM.getText(LC.PURCHASE_HISTORY_REPORT_SELECT_TOTALBILL, loginDTO);
		Display[10][4] = LM.getText(LC.PURCHASE_HISTORY_REPORT_SELECT_PURCHASEITEMREMARKS, loginDTO);

		
		String reportName = isLangEng?"Purchase History Report":"ক্রয়ের ইতিহাস রিপোর্ট";
		
		reportRequestHandler = new ReportRequestHandler(null,
				Criteria, Display, GroupBy, OrderBY, sql,
				reportService);

		
		reportRequestHandler.handleReportGet(request, response, userDTO, "purchase_history_report",
				MenuConstants.PURCHASE_HISTORY_REPORT_DETAILS, language, reportName, "purchase_history_report");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		doGet(request, response);
	}
}
