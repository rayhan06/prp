package pi_app_request;

import pb_notifications.Pb_notificationsDAO;
import util.UtilCharacter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PiAppNotification {
    private final Pb_notificationsDAO pb_notificationsDAO;

    private PiAppNotification() {
        pb_notificationsDAO = Pb_notificationsDAO.getInstance();
    }

    private static class LazyLoader {
        static final PiAppNotification INSTANCE = new PiAppNotification();
    }

    public static PiAppNotification getInstance() {
        return LazyLoader.INSTANCE;
    }

    public void sendNotificationAfterThirdApproval(Pi_app_requestDTO requestDTO) {
        String bnMsg = UtilCharacter.getFiscalYearDataById(requestDTO.fiscalYearId, "bangla")
                + " অর্থবছরের জন্যে "
                + UtilCharacter.getOfficeUnitDataById(requestDTO.officeUnitId, "bangla")
                + " এর বার্ষিক ক্রয় পরিকল্পনার অনুরোধ অনুমোদিত হয়েছে ";

        String enMsg = " Annual Purchase Plan for fiscal year "
                + UtilCharacter.getFiscalYearDataById(requestDTO.fiscalYearId, "english")
                + " of "
                + UtilCharacter.getOfficeUnitDataById(requestDTO.officeUnitId, "english")
                + " have been approved";

        String notificationMessage = enMsg + "$" + bnMsg;
        String url = "Pi_app_requestServlet?actionType=view&ID=" + requestDTO.iD;

        List<Long> organogramIds = new ArrayList<>();
        organogramIds.add(requestDTO.requesterOrgId);

        sendNotification(organogramIds, notificationMessage, url);
    }

    public void sendNotificationAfterSecondApproval(Pi_app_requestDTO requestDTO) {
        // send to 3rd approver
        String bnMsg = UtilCharacter.getFiscalYearDataById(requestDTO.fiscalYearId, "bangla")
                + " অর্থবছরের জন্যে "
                + UtilCharacter.getOfficeUnitDataById(requestDTO.officeUnitId, "bangla")
                + " বার্ষিক ক্রয় পরিকল্পনার অনুরোধ করেছেন ";

        String enMsg = UtilCharacter.getOfficeUnitDataById(requestDTO.officeUnitId, "english")
                + " have requested Annual Purchase Plan for fiscal year "
                + UtilCharacter.getFiscalYearDataById(requestDTO.fiscalYearId, "english");

        String notificationMessage = enMsg + "$" + bnMsg;
        String url = "Pi_app_requestServlet?actionType=addApprovalThree&ID=" + requestDTO.iD;

        sendNotification(Collections.singletonList(requestDTO.approverThreeOrgId), notificationMessage, url);

    }

    public void sendNotificationAfterFirstApproval(Pi_app_requestDTO requestDTO) {
        // send to 2nd approver
        String bnMsg = UtilCharacter.getFiscalYearDataById(requestDTO.fiscalYearId, "bangla")
                + " অর্থবছরের জন্যে "
                + UtilCharacter.getOfficeUnitDataById(requestDTO.officeUnitId, "bangla")
                + " বার্ষিক ক্রয় পরিকল্পনার অনুরোধ করেছেন ";

        String enMsg = UtilCharacter.getOfficeUnitDataById(requestDTO.officeUnitId, "english")
                + " have requested Annual Purchase Plan for fiscal year "
                + UtilCharacter.getFiscalYearDataById(requestDTO.fiscalYearId, "english");

        String notificationMessage = enMsg + "$" + bnMsg;
        String url = "Pi_app_requestServlet?actionType=addApprovalTwo&ID=" + requestDTO.iD;

        sendNotification(Collections.singletonList(requestDTO.approverTwoOrgId), notificationMessage, url);

    }

    public void sendNotificationAfterRequest(Pi_app_requestDTO requestDTO) {

        String bnMsg = UtilCharacter.getFiscalYearDataById(requestDTO.fiscalYearId, "bangla")
                + " অর্থবছরের জন্যে "
                + UtilCharacter.getOfficeUnitDataById(requestDTO.officeUnitId, "bangla")
                + " বার্ষিক ক্রয় পরিকল্পনার অনুরোধ করেছেন ";

        String enMsg = UtilCharacter.getOfficeUnitDataById(requestDTO.officeUnitId, "english")
                + " have requested Annual Purchase Plan for fiscal year "
                + UtilCharacter.getFiscalYearDataById(requestDTO.fiscalYearId, "english");

        String notificationMessage = enMsg + "$" + bnMsg;
        String url;
        List<Long> organogramIds = new ArrayList<>();
        if (requestDTO.skip_first_layer) {
            url = "Pi_app_requestServlet?actionType=addApprovalTwo&ID=" + requestDTO.iD;
            organogramIds.add(requestDTO.approverTwoOrgId);
        } else {
            url = "Pi_app_requestServlet?actionType=addApprovalOne&ID=" + requestDTO.iD;
            organogramIds.add(requestDTO.approverOneOrgId);
        }

        sendNotification(organogramIds, notificationMessage, url);
    }

    private void sendNotification(List<Long> organogramIds, String notificationMessage, String url) {
        if (organogramIds == null || organogramIds.isEmpty()) return;

        Thread thread = new Thread(() -> {
            long currentTime = System.currentTimeMillis();
            organogramIds.forEach(organogramId -> pb_notificationsDAO.addPb_notifications(organogramId, currentTime, url, notificationMessage, false));
        });
        thread.setDaemon(true);
        thread.start();
    }
}
