package pi_app_request;
import java.util.*; 
import util.*; 


public class Pi_app_requestDTO extends CommonDTO
{

    public boolean skip_first_layer = false;
    public boolean isDirectWingHead = false;
	public long officeUnitId = -1;
	public long fiscalYearId = -1;
	public int status = -1;
    public int requested_package_count = 0;
    public int approver_one_package_count = 0;
    public int approver_two_package_count = 0;
    public int approver_three_package_count = 0;
    public double requested_item_count = 0;
    public double approver_one_item_count = 0;
    public double approver_two_item_count = 0;
    public double approver_three_item_count = 0;
    public double requested_est_cost = 0;
    public double approver_one_est_cost = 0;
    public double approver_two_est_cost = 0;
    public double approver_three_est_cost = 0;
	public long requestedDate = 1;
	public long approveOneDate = 1;
	public long approveTwoDate = 1;
	public long approveThreeDate = 1;
	public long requesterOrgId = -1;
	public long requesterOfficeId = -1;
	public long requesterOfficeUnitId = -1;
//    public long requested_office_unit_id = -1;
	public long requesterEmpId = -1;
    public String requesterPhoneNum = "";
    public String requesterNameEn = "";
    public String requesterNameBn = "";
    public String requesterOfficeNameEn = "";
    public String requesterOfficeNameBn = "";
    public String requesterOfficeUnitNameEn = "";
    public String requesterOfficeUnitNameBn = "";
    public String requesterOfficeUnitOrgNameEn = "";
    public String requesterOfficeUnitOrgNameBn = "";
	public long approverOneOrgId = -1;
	public long approverOneOfficeId = -1;
	public long approverOneOfficeUnitId = -1;
	public long approverOneEmpId = -1;
    public String approverOnePhoneNum = "";
    public String approverOneNameEn = "";
    public String approverOneNameBn = "";
    public String approverOneOfficeNameEn = "";
    public String approverOneOfficeNameBn = "";
    public String approverOneOfficeUnitNameEn = "";
    public String approverOneOfficeUnitNameBn = "";
    public String approverOneOfficeUnitOrgNameEn = "";
    public String approverOneOfficeUnitOrgNameBn = "";
	public long approverTwoOrgId = -1;
	public long approverTwoOfficeId = -1;
	public long approverTwoOfficeUnitId = -1;
	public long approverTwoEmpId = -1;
    public String approverTwoPhoneNum = "";
    public String approverTwoNameEn = "";
    public String approverTwoNameBn = "";
    public String approverTwoOfficeNameEn = "";
    public String approverTwoOfficeNameBn = "";
    public String approverTwoOfficeUnitNameEn = "";
    public String approverTwoOfficeUnitNameBn = "";
    public String approverTwoOfficeUnitOrgNameEn = "";
    public String approverTwoOfficeUnitOrgNameBn = "";
	public long approverThreeOrgId = -1;
	public long approverThreeOfficeId = -1;
	public long approverThreeOfficeUnitId = -1;
	public long approverThreeEmpId = -1;
    public String approverThreePhoneNum = "";
    public String approverThreeNameEn = "";
    public String approverThreeNameBn = "";
    public String approverThreeOfficeNameEn = "";
    public String approverThreeOfficeNameBn = "";
    public String approverThreeOfficeUnitNameEn = "";
    public String approverThreeOfficeUnitNameBn = "";
    public String approverThreeOfficeUnitOrgNameEn = "";
    public String approverThreeOfficeUnitOrgNameBn = "";
	public long insertedByUserId = -1;
	public long insertedByOrganogramId = -1;
	public long modifiedBy = -1;
	public long insertionDate = -1;
	
	
    @Override
	public String toString() {
            return "$Pi_app_requestDTO[" +
            " iD = " + iD +
            " officeUnitId = " + officeUnitId +
            " fiscalYearId = " + fiscalYearId +
            " status = " + status +
            " requestedDate = " + requestedDate +
            " approveOneDate = " + approveOneDate +
            " approveTwoDate = " + approveTwoDate +
            " approveThreeDate = " + approveThreeDate +
            " requesterOrgId = " + requesterOrgId +
            " requesterOfficeId = " + requesterOfficeId +
            " requesterOfficeUnitId = " + requesterOfficeUnitId +
            " requesterEmpId = " + requesterEmpId +
            " requesterPhoneNum = " + requesterPhoneNum +
            " requesterNameEn = " + requesterNameEn +
            " requesterNameBn = " + requesterNameBn +
            " requesterOfficeNameEn = " + requesterOfficeNameEn +
            " requesterOfficeNameBn = " + requesterOfficeNameBn +
            " requesterOfficeUnitNameEn = " + requesterOfficeUnitNameEn +
            " requesterOfficeUnitNameBn = " + requesterOfficeUnitNameBn +
            " requesterOfficeUnitOrgNameEn = " + requesterOfficeUnitOrgNameEn +
            " requesterOfficeUnitOrgNameBn = " + requesterOfficeUnitOrgNameBn +
            " approverOneOrgId = " + approverOneOrgId +
            " approverOneOfficeId = " + approverOneOfficeId +
            " approverOneOfficeUnitId = " + approverOneOfficeUnitId +
            " approverOneEmpId = " + approverOneEmpId +
            " approverOnePhoneNum = " + approverOnePhoneNum +
            " approverOneNameEn = " + approverOneNameEn +
            " approverOneNameBn = " + approverOneNameBn +
            " approverOneOfficeNameEn = " + approverOneOfficeNameEn +
            " approverOneOfficeNameBn = " + approverOneOfficeNameBn +
            " approverOneOfficeUnitNameEn = " + approverOneOfficeUnitNameEn +
            " approverOneOfficeUnitNameBn = " + approverOneOfficeUnitNameBn +
            " approverOneOfficeUnitOrgNameEn = " + approverOneOfficeUnitOrgNameEn +
            " approverOneOfficeUnitOrgNameBn = " + approverOneOfficeUnitOrgNameBn +
            " approverTwoOrgId = " + approverTwoOrgId +
            " approverTwoOfficeId = " + approverTwoOfficeId +
            " approverTwoOfficeUnitId = " + approverTwoOfficeUnitId +
            " approverTwoEmpId = " + approverTwoEmpId +
            " approverTwoPhoneNum = " + approverTwoPhoneNum +
            " approverTwoNameEn = " + approverTwoNameEn +
            " approverTwoNameBn = " + approverTwoNameBn +
            " approverTwoOfficeNameEn = " + approverTwoOfficeNameEn +
            " approverTwoOfficeNameBn = " + approverTwoOfficeNameBn +
            " approverTwoOfficeUnitNameEn = " + approverTwoOfficeUnitNameEn +
            " approverTwoOfficeUnitNameBn = " + approverTwoOfficeUnitNameBn +
            " approverTwoOfficeUnitOrgNameEn = " + approverTwoOfficeUnitOrgNameEn +
            " approverTwoOfficeUnitOrgNameBn = " + approverTwoOfficeUnitOrgNameBn +
            " approverThreeOrgId = " + approverThreeOrgId +
            " approverThreeOfficeId = " + approverThreeOfficeId +
            " approverThreeOfficeUnitId = " + approverThreeOfficeUnitId +
            " approverThreeEmpId = " + approverThreeEmpId +
            " approverThreePhoneNum = " + approverThreePhoneNum +
            " approverThreeNameEn = " + approverThreeNameEn +
            " approverThreeNameBn = " + approverThreeNameBn +
            " approverThreeOfficeNameEn = " + approverThreeOfficeNameEn +
            " approverThreeOfficeNameBn = " + approverThreeOfficeNameBn +
            " approverThreeOfficeUnitNameEn = " + approverThreeOfficeUnitNameEn +
            " approverThreeOfficeUnitNameBn = " + approverThreeOfficeUnitNameBn +
            " approverThreeOfficeUnitOrgNameEn = " + approverThreeOfficeUnitOrgNameEn +
            " approverThreeOfficeUnitOrgNameBn = " + approverThreeOfficeUnitOrgNameBn +
            " insertedByUserId = " + insertedByUserId +
            " insertedByOrganogramId = " + insertedByOrganogramId +
            " modifiedBy = " + modifiedBy +
            " insertionDate = " + insertionDate +
            " searchColumn = " + searchColumn +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}