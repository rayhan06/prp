package pi_app_request;

import procurement_goods.Procurement_goodsDTO;

public class PiAppProductModel {
    public String iD = "";
    public String productName = "";
    public String supplierCompanyName = "";
    public String buyingDate = "";
    public String fiscalYearId = "";

    public String unitBuyingPrice = "";
    public String stock = "";

    public PiAppProductModel(){ }
    public PiAppProductModel(Procurement_goodsDTO subCodeDTO, String language){
        boolean isLanguageEnglish = "English".equalsIgnoreCase(language);
        iD = String.valueOf(subCodeDTO.iD);
        productName = isLanguageEnglish?subCodeDTO.nameEn:subCodeDTO.nameBn;
    }
    


    @Override
    public String toString() {
        return "PiProductModel{" +
                "iD='" + iD + '\'' +
                ", productName='" + productName + '\'' +
                ", supplierCompanyName='" + supplierCompanyName + '\'' +
                ", buyingDate='" + buyingDate + '\'' +
                ", fiscalYearId='" + fiscalYearId + '\'' +
                ", unitBuyingPrice='" + unitBuyingPrice + '\'' +
                ", stock='" + stock + '\'' +
                '}';
    }
}
