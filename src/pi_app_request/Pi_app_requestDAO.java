package pi_app_request;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.sql.SQLException;
import java.util.stream.Collectors;

import common.CommonDAOService;
import common.ConnectionAndStatementUtil;
import office_unit_organograms.OfficeUnitOrganograms;
import office_unit_organograms.OfficeUnitOrganogramsRepository;
import office_units.OfficeUnitTypeEnum;
import office_units.Office_unitsDTO;
import office_units.Office_unitsRepository;
import org.apache.log4j.Logger;
import pi_app_request_details.Pi_app_request_detailsDAO;
import pi_app_request_details.Pi_app_request_detailsDTO;
import pi_app_request_package_lot_item_list.Pi_app_request_package_lot_item_listDAO;
import pi_app_request_package_lot_item_list.Pi_app_request_package_lot_item_listDTO;
import pi_package_item_map.PiPackageItemMapChildRepository;
import procurement_goods.Procurement_goodsDTO;
import procurement_goods.Procurement_goodsRepository;
import util.*;
import vm_requisition.CommonApprovalStatus;

public class Pi_app_requestDAO implements CommonDAOService<Pi_app_requestDTO> {
    Logger logger = Logger.getLogger(getClass());

    private static String addQuery = "";
    private static String updateQuery = "";
    private String[] columnNames = null;

    private Pi_app_requestDAO() {
        columnNames = new String[]
                {
                        "ID",
                        "skip_first_layer",
                        "is_direct_wing_head",
                        "office_unit_id",
                        "fiscal_year_id",
                        "status",
                        "requested_package_count",
                        "approver_one_package_count",
                        "approver_two_package_count",
                        "approver_three_package_count",
                        "requested_item_count",
                        "approver_one_item_count",
                        "approver_two_item_count",
                        "approver_three_item_count",
                        "requested_est_cost",
                        "approver_one_est_cost",
                        "approver_two_est_cost",
                        "approver_three_est_cost",
                        "requested_date",
                        "approve_one_date",
                        "approve_two_date",
                        "approve_three_date",
                        "requester_org_id",
                        "requester_office_id",
                        "requester_office_unit_id",
//			"requested_office_unit_id",
                        "requester_emp_id",
                        "requester_phone_num",
                        "requester_name_en",
                        "requester_name_bn",
                        "requester_office_name_en",
                        "requester_office_name_bn",
                        "requester_office_unit_name_en",
                        "requester_office_unit_name_bn",
                        "requester_office_unit_org_name_en",
                        "requester_office_unit_org_name_bn",
                        "approver_one_org_id",
                        "approver_one_office_id",
                        "approver_one_office_unit_id",
                        "approver_one_emp_id",
                        "approver_one_phone_num",
                        "approver_one_name_en",
                        "approver_one_name_bn",
                        "approver_one_office_name_en",
                        "approver_one_office_name_bn",
                        "approver_one_office_unit_name_en",
                        "approver_one_office_unit_name_bn",
                        "approver_one_office_unit_org_name_en",
                        "approver_one_office_unit_org_name_bn",
                        "approver_two_org_id",
                        "approver_two_office_id",
                        "approver_two_office_unit_id",
                        "approver_two_emp_id",
                        "approver_two_phone_num",
                        "approver_two_name_en",
                        "approver_two_name_bn",
                        "approver_two_office_name_en",
                        "approver_two_office_name_bn",
                        "approver_two_office_unit_name_en",
                        "approver_two_office_unit_name_bn",
                        "approver_two_office_unit_org_name_en",
                        "approver_two_office_unit_org_name_bn",
                        "approver_three_org_id",
                        "approver_three_office_id",
                        "approver_three_office_unit_id",
                        "approver_three_emp_id",
                        "approver_three_phone_num",
                        "approver_three_name_en",
                        "approver_three_name_bn",
                        "approver_three_office_name_en",
                        "approver_three_office_name_bn",
                        "approver_three_office_unit_name_en",
                        "approver_three_office_unit_name_bn",
                        "approver_three_office_unit_org_name_en",
                        "approver_three_office_unit_org_name_bn",
                        "inserted_by_user_id",
                        "inserted_by_organogram_id",
                        "modified_by",
                        "insertion_date",
                        "search_column",
                        "isDeleted",
                        "lastModificationTime"
                };
        updateQuery = getUpdateQuery(columnNames);
        addQuery = getAddQuery(columnNames);

        searchMap.put("requested_date_start", " and (requested_date >= ?)");
        searchMap.put("requested_date_end", " and (requested_date <= ?)");
//		searchMap.put("approve_one_date_start"," and (approve_one_date >= ?)");
//		searchMap.put("approve_one_date_end"," and (approve_one_date <= ?)");
//		searchMap.put("approve_two_date_start"," and (approve_two_date >= ?)");
//		searchMap.put("approve_two_date_end"," and (approve_two_date <= ?)");
//		searchMap.put("approve_three_date_start"," and (approve_three_date >= ?)");
//		searchMap.put("approve_three_date_end"," and (approve_three_date <= ?)");
//		searchMap.put("requester_phone_num"," and (requester_phone_num like ?)");
//		searchMap.put("requester_name_en"," and (requester_name_en like ?)");
//		searchMap.put("requester_name_bn"," and (requester_name_bn like ?)");
//		searchMap.put("requester_office_name_en"," and (requester_office_name_en like ?)");
//		searchMap.put("requester_office_name_bn"," and (requester_office_name_bn like ?)");
//		searchMap.put("requester_office_unit_name_en"," and (requester_office_unit_name_en like ?)");
//		searchMap.put("requester_office_unit_name_bn"," and (requester_office_unit_name_bn like ?)");
//		searchMap.put("requester_office_unit_org_name_en"," and (requester_office_unit_org_name_en like ?)");
//		searchMap.put("requester_office_unit_org_name_bn"," and (requester_office_unit_org_name_bn like ?)");
//		searchMap.put("approver_one_phone_num"," and (approver_one_phone_num like ?)");
//		searchMap.put("approver_one_name_en"," and (approver_one_name_en like ?)");
//		searchMap.put("approver_one_name_bn"," and (approver_one_name_bn like ?)");
//		searchMap.put("approver_one_office_name_en"," and (approver_one_office_name_en like ?)");
//		searchMap.put("approver_one_office_name_bn"," and (approver_one_office_name_bn like ?)");
//		searchMap.put("approver_one_office_unit_name_en"," and (approver_one_office_unit_name_en like ?)");
//		searchMap.put("approver_one_office_unit_name_bn"," and (approver_one_office_unit_name_bn like ?)");
//		searchMap.put("approver_one_office_unit_org_name_en"," and (approver_one_office_unit_org_name_en like ?)");
//		searchMap.put("approver_one_office_unit_org_name_bn"," and (approver_one_office_unit_org_name_bn like ?)");
//		searchMap.put("approver_two_phone_num"," and (approver_two_phone_num like ?)");
//		searchMap.put("approver_two_name_en"," and (approver_two_name_en like ?)");
//		searchMap.put("approver_two_name_bn"," and (approver_two_name_bn like ?)");
//		searchMap.put("approver_two_office_name_en"," and (approver_two_office_name_en like ?)");
//		searchMap.put("approver_two_office_name_bn"," and (approver_two_office_name_bn like ?)");
//		searchMap.put("approver_two_office_unit_name_en"," and (approver_two_office_unit_name_en like ?)");
//		searchMap.put("approver_two_office_unit_name_bn"," and (approver_two_office_unit_name_bn like ?)");
//		searchMap.put("approver_two_office_unit_org_name_en"," and (approver_two_office_unit_org_name_en like ?)");
//		searchMap.put("approver_two_office_unit_org_name_bn"," and (approver_two_office_unit_org_name_bn like ?)");
//		searchMap.put("approver_three_phone_num"," and (approver_three_phone_num like ?)");
//		searchMap.put("approver_three_name_en"," and (approver_three_name_en like ?)");
//		searchMap.put("approver_three_name_bn"," and (approver_three_name_bn like ?)");
//		searchMap.put("approver_three_office_name_en"," and (approver_three_office_name_en like ?)");
//		searchMap.put("approver_three_office_name_bn"," and (approver_three_office_name_bn like ?)");
//		searchMap.put("approver_three_office_unit_name_en"," and (approver_three_office_unit_name_en like ?)");
//		searchMap.put("approver_three_office_unit_name_bn"," and (approver_three_office_unit_name_bn like ?)");
//		searchMap.put("approver_three_office_unit_org_name_en"," and (approver_three_office_unit_org_name_en like ?)");
//		searchMap.put("approver_three_office_unit_org_name_bn"," and (approver_three_office_unit_org_name_bn like ?)");
//		searchMap.put("modified_by"," and (modified_by like ?)");
//		searchMap.put("insertion_date_start"," and (insertion_date >= ?)");
//		searchMap.put("insertion_date_end"," and (insertion_date <= ?)");
        searchMap.put("AnyField", " and (search_column like ?)");
        searchMap.put("requesterId", " and (requester_org_id = ?)");
        searchMap.put("fiscalYearInModal", " and (fiscal_year_id = ?)");
        searchMap.put("reqOfficeUnitId", " and (office_unit_id = ?)");

    }

    private final Map<String, String> searchMap = new HashMap<>();

    private static class LazyLoader {
        static final Pi_app_requestDAO INSTANCE = new Pi_app_requestDAO();
    }

    public static Pi_app_requestDAO getInstance() {
        return LazyLoader.INSTANCE;
    }

    public void setSearchColumn(Pi_app_requestDTO pi_app_requestDTO) {
        pi_app_requestDTO.searchColumn = "";
        pi_app_requestDTO.searchColumn += pi_app_requestDTO.requesterPhoneNum + " ";
        pi_app_requestDTO.searchColumn += pi_app_requestDTO.requesterNameEn + " ";
        pi_app_requestDTO.searchColumn += pi_app_requestDTO.requesterNameBn + " ";
        pi_app_requestDTO.searchColumn += pi_app_requestDTO.requesterOfficeNameEn + " ";
        pi_app_requestDTO.searchColumn += pi_app_requestDTO.requesterOfficeNameBn + " ";
        pi_app_requestDTO.searchColumn += pi_app_requestDTO.requesterOfficeUnitNameEn + " ";
        pi_app_requestDTO.searchColumn += pi_app_requestDTO.requesterOfficeUnitNameBn + " ";
        pi_app_requestDTO.searchColumn += pi_app_requestDTO.requesterOfficeUnitOrgNameEn + " ";
        pi_app_requestDTO.searchColumn += pi_app_requestDTO.requesterOfficeUnitOrgNameBn + " ";
        pi_app_requestDTO.searchColumn += pi_app_requestDTO.approverOnePhoneNum + " ";
        pi_app_requestDTO.searchColumn += pi_app_requestDTO.approverOneNameEn + " ";
        pi_app_requestDTO.searchColumn += pi_app_requestDTO.approverOneNameBn + " ";
        pi_app_requestDTO.searchColumn += pi_app_requestDTO.approverOneOfficeNameEn + " ";
        pi_app_requestDTO.searchColumn += pi_app_requestDTO.approverOneOfficeNameBn + " ";
        pi_app_requestDTO.searchColumn += pi_app_requestDTO.approverOneOfficeUnitNameEn + " ";
        pi_app_requestDTO.searchColumn += pi_app_requestDTO.approverOneOfficeUnitNameBn + " ";
        pi_app_requestDTO.searchColumn += pi_app_requestDTO.approverOneOfficeUnitOrgNameEn + " ";
        pi_app_requestDTO.searchColumn += pi_app_requestDTO.approverOneOfficeUnitOrgNameBn + " ";
        pi_app_requestDTO.searchColumn += pi_app_requestDTO.approverTwoPhoneNum + " ";
        pi_app_requestDTO.searchColumn += pi_app_requestDTO.approverTwoNameEn + " ";
        pi_app_requestDTO.searchColumn += pi_app_requestDTO.approverTwoNameBn + " ";
        pi_app_requestDTO.searchColumn += pi_app_requestDTO.approverTwoOfficeNameEn + " ";
        pi_app_requestDTO.searchColumn += pi_app_requestDTO.approverTwoOfficeNameBn + " ";
        pi_app_requestDTO.searchColumn += pi_app_requestDTO.approverTwoOfficeUnitNameEn + " ";
        pi_app_requestDTO.searchColumn += pi_app_requestDTO.approverTwoOfficeUnitNameBn + " ";
        pi_app_requestDTO.searchColumn += pi_app_requestDTO.approverTwoOfficeUnitOrgNameEn + " ";
        pi_app_requestDTO.searchColumn += pi_app_requestDTO.approverTwoOfficeUnitOrgNameBn + " ";
        pi_app_requestDTO.searchColumn += pi_app_requestDTO.approverThreePhoneNum + " ";
        pi_app_requestDTO.searchColumn += pi_app_requestDTO.approverThreeNameEn + " ";
        pi_app_requestDTO.searchColumn += pi_app_requestDTO.approverThreeNameBn + " ";
        pi_app_requestDTO.searchColumn += pi_app_requestDTO.approverThreeOfficeNameEn + " ";
        pi_app_requestDTO.searchColumn += pi_app_requestDTO.approverThreeOfficeNameBn + " ";
        pi_app_requestDTO.searchColumn += pi_app_requestDTO.approverThreeOfficeUnitNameEn + " ";
        pi_app_requestDTO.searchColumn += pi_app_requestDTO.approverThreeOfficeUnitNameBn + " ";
        pi_app_requestDTO.searchColumn += pi_app_requestDTO.approverThreeOfficeUnitOrgNameEn + " ";
        pi_app_requestDTO.searchColumn += pi_app_requestDTO.approverThreeOfficeUnitOrgNameBn + " ";
        pi_app_requestDTO.searchColumn += pi_app_requestDTO.modifiedBy + " ";
    }

    @Override
    public void set(PreparedStatement ps, Pi_app_requestDTO pi_app_requestDTO, boolean isInsert) throws SQLException {
        int index = 0;
        long lastModificationTime = System.currentTimeMillis();
        setSearchColumn(pi_app_requestDTO);
        if (isInsert) {
            ps.setObject(++index, pi_app_requestDTO.iD);
        }
        ps.setObject(++index, pi_app_requestDTO.skip_first_layer);
        ps.setObject(++index, pi_app_requestDTO.isDirectWingHead);
        ps.setObject(++index, pi_app_requestDTO.officeUnitId);
        ps.setObject(++index, pi_app_requestDTO.fiscalYearId);
        ps.setObject(++index, pi_app_requestDTO.status);
        ps.setObject(++index, pi_app_requestDTO.requested_package_count);
        ps.setObject(++index, pi_app_requestDTO.approver_one_package_count);
        ps.setObject(++index, pi_app_requestDTO.approver_two_package_count);
        ps.setObject(++index, pi_app_requestDTO.approver_three_package_count);
        ps.setObject(++index, pi_app_requestDTO.requested_item_count);
        ps.setObject(++index, pi_app_requestDTO.approver_one_item_count);
        ps.setObject(++index, pi_app_requestDTO.approver_two_item_count);
        ps.setObject(++index, pi_app_requestDTO.approver_three_item_count);
        ps.setObject(++index, pi_app_requestDTO.requested_est_cost);
        ps.setObject(++index, pi_app_requestDTO.approver_one_est_cost);
        ps.setObject(++index, pi_app_requestDTO.approver_two_est_cost);
        ps.setObject(++index, pi_app_requestDTO.approver_three_est_cost);


        ps.setObject(++index, pi_app_requestDTO.requestedDate);
        ps.setObject(++index, pi_app_requestDTO.approveOneDate);
        ps.setObject(++index, pi_app_requestDTO.approveTwoDate);
        ps.setObject(++index, pi_app_requestDTO.approveThreeDate);
        ps.setObject(++index, pi_app_requestDTO.requesterOrgId);
        ps.setObject(++index, pi_app_requestDTO.requesterOfficeId);
        ps.setObject(++index, pi_app_requestDTO.requesterOfficeUnitId);
//		ps.setObject(++index,pi_app_requestDTO.requested_office_unit_id);
        ps.setObject(++index, pi_app_requestDTO.requesterEmpId);
        ps.setObject(++index, pi_app_requestDTO.requesterPhoneNum);
        ps.setObject(++index, pi_app_requestDTO.requesterNameEn);
        ps.setObject(++index, pi_app_requestDTO.requesterNameBn);
        ps.setObject(++index, pi_app_requestDTO.requesterOfficeNameEn);
        ps.setObject(++index, pi_app_requestDTO.requesterOfficeNameBn);
        ps.setObject(++index, pi_app_requestDTO.requesterOfficeUnitNameEn);
        ps.setObject(++index, pi_app_requestDTO.requesterOfficeUnitNameBn);
        ps.setObject(++index, pi_app_requestDTO.requesterOfficeUnitOrgNameEn);
        ps.setObject(++index, pi_app_requestDTO.requesterOfficeUnitOrgNameBn);
        ps.setObject(++index, pi_app_requestDTO.approverOneOrgId);
        ps.setObject(++index, pi_app_requestDTO.approverOneOfficeId);
        ps.setObject(++index, pi_app_requestDTO.approverOneOfficeUnitId);
        ps.setObject(++index, pi_app_requestDTO.approverOneEmpId);
        ps.setObject(++index, pi_app_requestDTO.approverOnePhoneNum);
        ps.setObject(++index, pi_app_requestDTO.approverOneNameEn);
        ps.setObject(++index, pi_app_requestDTO.approverOneNameBn);
        ps.setObject(++index, pi_app_requestDTO.approverOneOfficeNameEn);
        ps.setObject(++index, pi_app_requestDTO.approverOneOfficeNameBn);
        ps.setObject(++index, pi_app_requestDTO.approverOneOfficeUnitNameEn);
        ps.setObject(++index, pi_app_requestDTO.approverOneOfficeUnitNameBn);
        ps.setObject(++index, pi_app_requestDTO.approverOneOfficeUnitOrgNameEn);
        ps.setObject(++index, pi_app_requestDTO.approverOneOfficeUnitOrgNameBn);
        ps.setObject(++index, pi_app_requestDTO.approverTwoOrgId);
        ps.setObject(++index, pi_app_requestDTO.approverTwoOfficeId);
        ps.setObject(++index, pi_app_requestDTO.approverTwoOfficeUnitId);
        ps.setObject(++index, pi_app_requestDTO.approverTwoEmpId);
        ps.setObject(++index, pi_app_requestDTO.approverTwoPhoneNum);
        ps.setObject(++index, pi_app_requestDTO.approverTwoNameEn);
        ps.setObject(++index, pi_app_requestDTO.approverTwoNameBn);
        ps.setObject(++index, pi_app_requestDTO.approverTwoOfficeNameEn);
        ps.setObject(++index, pi_app_requestDTO.approverTwoOfficeNameBn);
        ps.setObject(++index, pi_app_requestDTO.approverTwoOfficeUnitNameEn);
        ps.setObject(++index, pi_app_requestDTO.approverTwoOfficeUnitNameBn);
        ps.setObject(++index, pi_app_requestDTO.approverTwoOfficeUnitOrgNameEn);
        ps.setObject(++index, pi_app_requestDTO.approverTwoOfficeUnitOrgNameBn);
        ps.setObject(++index, pi_app_requestDTO.approverThreeOrgId);
        ps.setObject(++index, pi_app_requestDTO.approverThreeOfficeId);
        ps.setObject(++index, pi_app_requestDTO.approverThreeOfficeUnitId);
        ps.setObject(++index, pi_app_requestDTO.approverThreeEmpId);
        ps.setObject(++index, pi_app_requestDTO.approverThreePhoneNum);
        ps.setObject(++index, pi_app_requestDTO.approverThreeNameEn);
        ps.setObject(++index, pi_app_requestDTO.approverThreeNameBn);
        ps.setObject(++index, pi_app_requestDTO.approverThreeOfficeNameEn);
        ps.setObject(++index, pi_app_requestDTO.approverThreeOfficeNameBn);
        ps.setObject(++index, pi_app_requestDTO.approverThreeOfficeUnitNameEn);
        ps.setObject(++index, pi_app_requestDTO.approverThreeOfficeUnitNameBn);
        ps.setObject(++index, pi_app_requestDTO.approverThreeOfficeUnitOrgNameEn);
        ps.setObject(++index, pi_app_requestDTO.approverThreeOfficeUnitOrgNameBn);
        ps.setObject(++index, pi_app_requestDTO.insertedByUserId);
        ps.setObject(++index, pi_app_requestDTO.insertedByOrganogramId);
        ps.setObject(++index, pi_app_requestDTO.modifiedBy);
        ps.setObject(++index, pi_app_requestDTO.insertionDate);
        ps.setObject(++index, pi_app_requestDTO.searchColumn);
        if (isInsert) {
            ps.setObject(++index, pi_app_requestDTO.isDeleted);
        }
        ps.setObject(++index, lastModificationTime);
        if (!isInsert) {
            ps.setObject(++index, pi_app_requestDTO.iD);
        }
    }

    @Override
    public Pi_app_requestDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Pi_app_requestDTO pi_app_requestDTO = new Pi_app_requestDTO();
            int i = 0;
            pi_app_requestDTO.iD = rs.getLong(columnNames[i++]);
            pi_app_requestDTO.skip_first_layer = rs.getBoolean(columnNames[i++]);
            pi_app_requestDTO.isDirectWingHead = rs.getBoolean(columnNames[i++]);
            pi_app_requestDTO.officeUnitId = rs.getLong(columnNames[i++]);
            pi_app_requestDTO.fiscalYearId = rs.getLong(columnNames[i++]);
            pi_app_requestDTO.status = rs.getInt(columnNames[i++]);

            pi_app_requestDTO.requested_package_count = rs.getInt(columnNames[i++]);
            pi_app_requestDTO.approver_one_package_count = rs.getInt(columnNames[i++]);
            pi_app_requestDTO.approver_two_package_count = rs.getInt(columnNames[i++]);
            pi_app_requestDTO.approver_three_package_count = rs.getInt(columnNames[i++]);

            pi_app_requestDTO.requested_item_count = rs.getInt(columnNames[i++]);
            pi_app_requestDTO.approver_one_item_count = rs.getInt(columnNames[i++]);
            pi_app_requestDTO.approver_two_item_count = rs.getInt(columnNames[i++]);
            pi_app_requestDTO.approver_three_item_count = rs.getInt(columnNames[i++]);

            pi_app_requestDTO.requested_est_cost = rs.getDouble(columnNames[i++]);
            pi_app_requestDTO.approver_one_est_cost = rs.getDouble(columnNames[i++]);
            pi_app_requestDTO.approver_two_est_cost = rs.getDouble(columnNames[i++]);
            pi_app_requestDTO.approver_three_est_cost = rs.getDouble(columnNames[i++]);

            pi_app_requestDTO.requestedDate = rs.getLong(columnNames[i++]);
            pi_app_requestDTO.approveOneDate = rs.getLong(columnNames[i++]);
            pi_app_requestDTO.approveTwoDate = rs.getLong(columnNames[i++]);
            pi_app_requestDTO.approveThreeDate = rs.getLong(columnNames[i++]);
            pi_app_requestDTO.requesterOrgId = rs.getLong(columnNames[i++]);
            pi_app_requestDTO.requesterOfficeId = rs.getLong(columnNames[i++]);
            pi_app_requestDTO.requesterOfficeUnitId = rs.getLong(columnNames[i++]);
//			pi_app_requestDTO.requested_office_unit_id = rs.getLong(columnNames[i++]);
            pi_app_requestDTO.requesterEmpId = rs.getLong(columnNames[i++]);
            pi_app_requestDTO.requesterPhoneNum = rs.getString(columnNames[i++]);
            pi_app_requestDTO.requesterNameEn = rs.getString(columnNames[i++]);
            pi_app_requestDTO.requesterNameBn = rs.getString(columnNames[i++]);
            pi_app_requestDTO.requesterOfficeNameEn = rs.getString(columnNames[i++]);
            pi_app_requestDTO.requesterOfficeNameBn = rs.getString(columnNames[i++]);
            pi_app_requestDTO.requesterOfficeUnitNameEn = rs.getString(columnNames[i++]);
            pi_app_requestDTO.requesterOfficeUnitNameBn = rs.getString(columnNames[i++]);
            pi_app_requestDTO.requesterOfficeUnitOrgNameEn = rs.getString(columnNames[i++]);
            pi_app_requestDTO.requesterOfficeUnitOrgNameBn = rs.getString(columnNames[i++]);
            pi_app_requestDTO.approverOneOrgId = rs.getLong(columnNames[i++]);
            pi_app_requestDTO.approverOneOfficeId = rs.getLong(columnNames[i++]);
            pi_app_requestDTO.approverOneOfficeUnitId = rs.getLong(columnNames[i++]);
            pi_app_requestDTO.approverOneEmpId = rs.getLong(columnNames[i++]);
            pi_app_requestDTO.approverOnePhoneNum = rs.getString(columnNames[i++]);
            pi_app_requestDTO.approverOneNameEn = rs.getString(columnNames[i++]);
            pi_app_requestDTO.approverOneNameBn = rs.getString(columnNames[i++]);
            pi_app_requestDTO.approverOneOfficeNameEn = rs.getString(columnNames[i++]);
            pi_app_requestDTO.approverOneOfficeNameBn = rs.getString(columnNames[i++]);
            pi_app_requestDTO.approverOneOfficeUnitNameEn = rs.getString(columnNames[i++]);
            pi_app_requestDTO.approverOneOfficeUnitNameBn = rs.getString(columnNames[i++]);
            pi_app_requestDTO.approverOneOfficeUnitOrgNameEn = rs.getString(columnNames[i++]);
            pi_app_requestDTO.approverOneOfficeUnitOrgNameBn = rs.getString(columnNames[i++]);
            pi_app_requestDTO.approverTwoOrgId = rs.getLong(columnNames[i++]);
            pi_app_requestDTO.approverTwoOfficeId = rs.getLong(columnNames[i++]);
            pi_app_requestDTO.approverTwoOfficeUnitId = rs.getLong(columnNames[i++]);
            pi_app_requestDTO.approverTwoEmpId = rs.getLong(columnNames[i++]);
            pi_app_requestDTO.approverTwoPhoneNum = rs.getString(columnNames[i++]);
            pi_app_requestDTO.approverTwoNameEn = rs.getString(columnNames[i++]);
            pi_app_requestDTO.approverTwoNameBn = rs.getString(columnNames[i++]);
            pi_app_requestDTO.approverTwoOfficeNameEn = rs.getString(columnNames[i++]);
            pi_app_requestDTO.approverTwoOfficeNameBn = rs.getString(columnNames[i++]);
            pi_app_requestDTO.approverTwoOfficeUnitNameEn = rs.getString(columnNames[i++]);
            pi_app_requestDTO.approverTwoOfficeUnitNameBn = rs.getString(columnNames[i++]);
            pi_app_requestDTO.approverTwoOfficeUnitOrgNameEn = rs.getString(columnNames[i++]);
            pi_app_requestDTO.approverTwoOfficeUnitOrgNameBn = rs.getString(columnNames[i++]);
            pi_app_requestDTO.approverThreeOrgId = rs.getLong(columnNames[i++]);
            pi_app_requestDTO.approverThreeOfficeId = rs.getLong(columnNames[i++]);
            pi_app_requestDTO.approverThreeOfficeUnitId = rs.getLong(columnNames[i++]);
            pi_app_requestDTO.approverThreeEmpId = rs.getLong(columnNames[i++]);
            pi_app_requestDTO.approverThreePhoneNum = rs.getString(columnNames[i++]);
            pi_app_requestDTO.approverThreeNameEn = rs.getString(columnNames[i++]);
            pi_app_requestDTO.approverThreeNameBn = rs.getString(columnNames[i++]);
            pi_app_requestDTO.approverThreeOfficeNameEn = rs.getString(columnNames[i++]);
            pi_app_requestDTO.approverThreeOfficeNameBn = rs.getString(columnNames[i++]);
            pi_app_requestDTO.approverThreeOfficeUnitNameEn = rs.getString(columnNames[i++]);
            pi_app_requestDTO.approverThreeOfficeUnitNameBn = rs.getString(columnNames[i++]);
            pi_app_requestDTO.approverThreeOfficeUnitOrgNameEn = rs.getString(columnNames[i++]);
            pi_app_requestDTO.approverThreeOfficeUnitOrgNameBn = rs.getString(columnNames[i++]);
            pi_app_requestDTO.insertedByUserId = rs.getLong(columnNames[i++]);
            pi_app_requestDTO.insertedByOrganogramId = rs.getLong(columnNames[i++]);
            pi_app_requestDTO.modifiedBy = rs.getLong(columnNames[i++]);
            pi_app_requestDTO.insertionDate = rs.getLong(columnNames[i++]);
            pi_app_requestDTO.searchColumn = rs.getString(columnNames[i++]);
            pi_app_requestDTO.isDeleted = rs.getInt(columnNames[i++]);
            pi_app_requestDTO.lastModificationTime = rs.getLong(columnNames[i++]);
            return pi_app_requestDTO;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    public Pi_app_requestDTO getDTOByID(long id) {
        return getDTOFromID(id);
    }

    @Override
    public String getTableName() {
        return "pi_app_request";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Pi_app_requestDTO) commonDTO, addQuery, true);
    }

    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Pi_app_requestDTO) commonDTO, updateQuery, false);
    }

    @Override
    public String getLastModifierUser() {
        return "modified_by";
    }

    public int getCountByUnitIdAndFiscalYearId(long unitId, long fiscalYearId, long iD) {
        String countQuery = "SELECT count(*) as countID FROM "
                + getTableName() + " where isDeleted = 0 and office_unit_id = ? and fiscal_year_id = ? and ID != ?";
        return ConnectionAndStatementUtil.getT(countQuery, Arrays.asList(unitId, fiscalYearId, iD), rs -> {
            try {
                return rs.getInt("countID");
            } catch (SQLException ex) {
                ex.printStackTrace();
                return 0;
            }
        }, 0);
    }

    public List<Pi_app_requestDTO> getByFiscalYearIdAndUnitId(Long fiscalYearId, long unitId) {
        String sql = "SELECT * FROM " + getTableName() +
                " WHERE isDeleted = 0 and office_unit_id = ? and fiscal_year_id = ? ";
        return ConnectionAndStatementUtil.getListOfT(sql, Arrays.asList(unitId, fiscalYearId), this::buildObjectFromResultSet);
    }

    public long getFirstApprovalOrgId(long currentUserOfficeUnitId) throws Exception {
        Office_unitsDTO nearestDivision = Office_unitsRepository.getInstance()
                .findClosestOfficeUnitOfAnOfficeType(currentUserOfficeUnitId, OfficeUnitTypeEnum.DIVISION);
        validateApprovalRelatedError(nearestDivision == null);

        OfficeUnitOrganograms firstApprover = OfficeUnitOrganogramsRepository.getInstance()
                .getOfficeUnitHeadWithActiveEmpStatus(nearestDivision.iD, true);
        validateApprovalRelatedError(firstApprover == null);

        return firstApprover.id;
//		return 262146L; // 43 Admin Division Head Organogram ID
    }

    public long getSecondApprovalOrgId(long currentUserOfficeUnitId) throws Exception {
        Office_unitsDTO nearestBranch = Office_unitsRepository.getInstance()
                .findClosestOfficeUnitOfAnOfficeType(currentUserOfficeUnitId, OfficeUnitTypeEnum.BRANCH);
        validateApprovalRelatedError(nearestBranch == null);

        OfficeUnitOrganograms secondApprover = OfficeUnitOrganogramsRepository.getInstance()
                .getOfficeUnitHeadWithActiveEmpStatus(nearestBranch.iD, true);
        validateApprovalRelatedError(secondApprover == null);

        return secondApprover.id;
//		return 262109L; // 42 Deputy Secretary Admin 1 Head Organogram ID
    }

    public long getThirdApprovalOrgId(long currentUserOfficeUnitId) throws Exception {
        Office_unitsDTO nearestWing = Office_unitsRepository.getInstance()
                .findClosestOfficeUnitOfAnOfficeType(currentUserOfficeUnitId, OfficeUnitTypeEnum.WINGS);
        validateApprovalRelatedError(nearestWing == null);

        OfficeUnitOrganograms thirdApprover = OfficeUnitOrganogramsRepository.getInstance()
                .getOfficeUnitHeadWithActiveEmpStatus(nearestWing.iD, true);
        validateApprovalRelatedError(thirdApprover == null);

        return thirdApprover.id;
//		return 262105L; // 41 Administrative Wing Head Organogram ID
    }

    private void validateApprovalRelatedError(boolean isError) throws Exception {
        if (isError) {
            UtilCharacter.throwException("অনুমোদন সংক্রান্ত ত্রুটি!", "Approval Related Error!");
        }
    }

    public List<Procurement_goodsDTO> getDTOSByFiscalYearIdAndOfcUnitIdAndPackageId(long fiscalYearId, long ofcUnitId, long packageId) {

        List<Procurement_goodsDTO> goodsDTOS = new ArrayList<>();

        List<Pi_app_requestDTO> requestDTOS = Pi_app_requestDAO.getInstance().
                getByFiscalYearIdAndUnitId(fiscalYearId, ofcUnitId);
        if (!requestDTOS.isEmpty()) {
            List<Long> piPackageItemMapChildDTOSItemIds = PiPackageItemMapChildRepository.getInstance().
                    getPiPackageItemMapChildDTOBypiPackageNewId(packageId).
                    stream().
                    map(dto -> dto.itemId).collect(Collectors.toList());
//			List<Pi_app_request_detailsDTO> detailsDTOS = Pi_app_request_detailsDAO.getInstance()
//					.getByAppId(requestDTOS.get(0).iD).stream().filter(i -> i.packageId == packageId).
//							collect(Collectors.toList());
            List<Pi_app_request_detailsDTO> detailsDTOS = Pi_app_request_detailsDAO.getInstance()
                    .getByAppId(requestDTOS.get(0).iD);
            List<Long> itemsIds = detailsDTOS.stream().map(i -> i.itemId).distinct().
                    collect(Collectors.toList());

            itemsIds = itemsIds.stream().filter(piPackageItemMapChildDTOSItemIds::contains).collect(Collectors.toList());

            goodsDTOS = Procurement_goodsRepository.getInstance().getDTOsByIds(itemsIds);
        }
        return goodsDTOS;

    }

    public List<Pi_app_requestDTO> getDataByFiscalYear(List<Long> fiscalYearIds, boolean flag) {
        StringBuilder sql = new StringBuilder("SELECT * FROM ")
                .append(getTableName())
                .append(" WHERE isDeleted = 0 and status = ?");
        List<Object> parameters = new ArrayList<>();
        parameters.add(CommonApprovalStatus.SATISFIED.getValue());
        if (!flag) {
            sql.append(" and fiscal_year_id IN ( ");
            for (int i = 0; i < fiscalYearIds.size(); i++) {
                if (i != 0) {
                    sql.append(",");
                }
                sql.append(" ? ");
                parameters.add(fiscalYearIds.get(i));
            }
            sql.append(" ) ");

        }

        return ConnectionAndStatementUtil.getListOfT(sql.toString(), parameters, this::buildObjectFromResultSet);
    }

    public List<Pi_app_request_detailsDTO> getItemByUnitIdAndFiscalYearId(long fiscalYearId, long unitId, long itemId) {
        List<Pi_app_request_detailsDTO> detailsDTOS = new ArrayList<>();


        List<Pi_app_requestDTO> requestDTOS = Pi_app_requestDAO.getInstance().
                getByFiscalYearIdAndUnitId(fiscalYearId, unitId);
        if (!requestDTOS.isEmpty()) {
            detailsDTOS = Pi_app_request_detailsDAO.getInstance()
                    .getByAppId(requestDTOS.get(0).iD).stream().filter(i -> i.itemId == itemId).
                    collect(Collectors.toList());
        }
        return detailsDTOS;
    }

    public List<Pi_app_request_package_lot_item_listDTO> getItemByFiscalYearIdAndPackageIdAndLotId(long fiscalYearId, long packageFinalId, long lotFinalId) {
        List<Pi_app_request_package_lot_item_listDTO> pi_app_request_package_lot_item_listDTOS = new ArrayList<>();


        List<Pi_app_requestDTO> requestDTOS = Pi_app_requestDAO.getInstance().
                getByFiscalYearId(fiscalYearId);
        if (requestDTOS.size() == 1) {
            pi_app_request_package_lot_item_listDTOS = Pi_app_request_package_lot_item_listDAO.getInstance()
                    .getByAppId(requestDTOS.get(0).iD).stream().filter(i -> i.packageFinalId == packageFinalId && i.lotFinalId == lotFinalId).
                    collect(Collectors.toList());
        }
        return pi_app_request_package_lot_item_listDTOS;
    }

    public List<Pi_app_request_package_lot_item_listDTO> getItemByAppIdFiscalYearIdAndPackageIdAndLotId(long appId, long fiscalYearId, long packageFinalId, long lotFinalId) {

        List<Pi_app_request_package_lot_item_listDTO> pi_app_request_package_lot_item_listDTOS = Pi_app_request_package_lot_item_listDAO.getInstance()
                .getByAppId(appId).stream().filter(i -> i.packageFinalId == packageFinalId && i.lotFinalId == lotFinalId &&
                        i.fiscalYearId == fiscalYearId).
                collect(Collectors.toList());

        return pi_app_request_package_lot_item_listDTOS;
    }

	/*public List<Pi_app_request_package_lot_item_listDTO> getItemByAppIdFiscalYearIdAndPackageIdAndLotIdAndItemId(long appId, long fiscalYearId, long packageFinalId, long lotFinalId)
	{

		List<Pi_app_request_package_lot_item_listDTO> pi_app_request_package_lot_item_listDTOS = Pi_app_request_package_lot_item_listDAO.getInstance()
				.getByAppId(appId).stream().filter(i -> i.packageFinalId == packageFinalId && i.lotFinalId == lotFinalId &&
						i.fiscalYearId == fiscalYearId).
						collect(Collectors.toList());

		return pi_app_request_package_lot_item_listDTOS;
	}*/

    public double getApprovedAmountOfItemByFiscalYearIdAndOfficeUnitId(long itemId, long fiscalId, long unitId) {
        double amount = 0;
        List<Pi_app_requestDTO> requestDTOS = getByFiscalYearIdAndUnitId(fiscalId, unitId)
                .stream()
                .filter(i -> i.status == CommonApprovalStatus.SATISFIED.getValue())
                .collect(Collectors.toList());
        if (!requestDTOS.isEmpty()) {
            List<Pi_app_request_detailsDTO> detailsDTOS = Pi_app_request_detailsDAO.getInstance()
                    .getApprovedItemByAppIdAndItemId(requestDTOS.get(0).iD, itemId);
            if (!detailsDTOS.isEmpty()) {
                amount = detailsDTOS.get(0).approveThreeQuantity;
            }
        }

        return amount;
    }

    public List<Pi_app_requestDTO> getByFiscalYearId(Long fiscalYearId) {
        String sql = "SELECT * FROM " + getTableName() +
                " WHERE isDeleted = 0 and  fiscal_year_id = ? ";
        return ConnectionAndStatementUtil.getListOfT(sql, Arrays.asList(fiscalYearId), this::buildObjectFromResultSet);
    }
}
	