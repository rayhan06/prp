package pi_app_request;

import com.google.gson.Gson;
import common.ApiResponse;
import common.BaseServlet;
import common.CommonDAOService;
import employee_records.Employee_recordsDTO;
import employee_records.Employee_recordsRepository;
import fiscal_year.Fiscal_yearRepository;
import office_unit_organograms.OfficeUnitOrganograms;
import office_unit_organograms.OfficeUnitOrganogramsRepository;
import office_units.OfficeUnitModel;
import office_units.Office_unitsDTO;
import office_units.Office_unitsRepository;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import pb.OptionDTO;
import pb.Utils;
import permission.MenuConstants;
import pi_app_request_details.Pi_app_request_detailsDAO;
import pi_app_request_details.Pi_app_request_detailsDTO;
import pi_app_request_package_lot_item_list.Pi_app_request_package_lot_item_listDAO;
import pi_app_request_package_lot_item_list.Pi_app_request_package_lot_item_listDTO;
import pi_package_item_map.PiPackageItemMapChildDAO;
import pi_package_item_map.PiPackageItemMapChildDTO;
import pi_package_vendor_items.Pi_package_vendor_itemsDAO;
import procurement_goods.Procurement_goodsDAO;
import procurement_package.Procurement_packageDTO;
import procurement_package.Procurement_packageRepository;
import sessionmanager.SessionConstants;
import user.UserDTO;
import util.*;
import vm_requisition.CommonApprovalStatus;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@WebServlet("/Pi_app_requestServlet")
@MultipartConfig
public class Pi_app_requestServlet extends BaseServlet {
    private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Pi_app_requestServlet.class);

    @Override
    public String getTableName() {
        return Pi_app_requestDAO.getInstance().getTableName();
    }

    @Override
    public String getServletName() {
        return "Pi_app_requestServlet";
    }

    @Override
    public Pi_app_requestDAO getCommonDAOService() {
        return Pi_app_requestDAO.getInstance();
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[]{MenuConstants.PI_APP_REQUEST_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[]{MenuConstants.PI_APP_REQUEST_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[]{MenuConstants.PI_APP_REQUEST_SEARCH};
    }

    public int[] getFirstApprovalMenu() {
        return new int[]{MenuConstants.PI_APP_APPROVAL_ONE};
    }

    public int[] getSecondApprovalMenu() {
        return new int[]{MenuConstants.PI_APP_APPROVAL_TWO};
    }

    public int[] getThirdApprovalMenu() {
        return new int[]{MenuConstants.PI_APP_APPROVAL_THREE};
    }

    public int[] getFinalSearchMenu() {
        return new int[]{MenuConstants.PI_APP_FINAL_SEARCH};
    }

    public int[] getReportMenu() {
        return new int[]{MenuConstants.PI_APP_FINAL_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Pi_app_requestServlet.class;
    }

    private final Gson gson = new Gson();


    public CommonDTO saveApproverOneData(HttpServletRequest request, UserDTO userDTO) throws Exception {
        logger.debug("saveApproverOneData");
        Pi_app_requestDTO pi_app_requestDTO = Pi_app_requestDAO.getInstance().getDTOFromID
                (Long.parseLong(request.getParameter("iD")));
        if (pi_app_requestDTO == null) {
            throwException("App request not found", "App request not found");
        }

        if (pi_app_requestDTO.status != CommonApprovalStatus.PENDING.getValue()) {
            throwException("Status is not Pending", "Status is not Pending");
        }

        if (userDTO.organogramID != pi_app_requestDTO.approverOneOrgId) {
            throwException("Not Approver", "Not Approver");
        }

        pi_app_requestDTO.lastModificationTime = System.currentTimeMillis();
        pi_app_requestDTO.modifiedBy = userDTO.ID;


        String Value = "";
        Value = request.getParameter("isDirectWingHead");
        pi_app_requestDTO.isDirectWingHead = StringUtils.isValidString(Value);

        // IF DIRECT FORWARD TO WING HEAD, SKIPS SECOND LAYER APPROVAL
        if (!pi_app_requestDTO.isDirectWingHead) {
            pi_app_requestDTO.status = CommonApprovalStatus.SECOND_APPROVER_PENDING.getValue();
            pi_app_requestDTO.approverTwoOrgId = getCommonDAOService().getSecondApprovalOrgId(userDTO.unitID);
        } else {
            pi_app_requestDTO.status = CommonApprovalStatus.THIRD_APPROVER_PENDING.getValue();
            pi_app_requestDTO.approverThreeOrgId = getCommonDAOService().getThirdApprovalOrgId(userDTO.unitID);
        }

        pi_app_requestDTO.approverOneEmpId = userDTO.employee_record_id;
        Employee_recordsDTO employee_recordsDTO = Employee_recordsRepository.getInstance().
                getById(userDTO.employee_record_id);

        if (employee_recordsDTO != null) {
            pi_app_requestDTO.approverOneNameEn = employee_recordsDTO.nameEng;
            pi_app_requestDTO.approverOneNameBn = employee_recordsDTO.nameBng;
        }

        OfficeUnitOrganograms org = OfficeUnitOrganogramsRepository.getInstance().getById(userDTO.organogramID);
        if (org != null) {
            pi_app_requestDTO.approverOneOfficeUnitOrgNameBn = org.designation_bng;
            pi_app_requestDTO.approverOneOfficeUnitOrgNameEn = org.designation_eng;
            pi_app_requestDTO.approverOneOfficeUnitId = org.office_unit_id;
            Office_unitsDTO unit = Office_unitsRepository.getInstance().
                    getOffice_unitsDTOByID(org.office_unit_id);
            if (unit != null) {
                pi_app_requestDTO.approverOneOfficeUnitNameEn = unit.unitNameEng;
                pi_app_requestDTO.approverOneOfficeUnitNameBn = unit.unitNameBng;
            }
        }


        int packageLotCount = request.getParameterValues("piAppDetails.appDetailsId").length;
        for (int index = 0; index < packageLotCount; index++) {
            Pi_app_request_detailsDTO detailsDTO;
            long currentChildId = Long.parseLong(request.getParameterValues("piAppDetails.appDetailsId")[index]);
            if (currentChildId == -1) {
                continue;
            } else {
                detailsDTO = Pi_app_request_detailsDAO.getInstance().getDTOByID(currentChildId);
                detailsDTO.isSelected = 2;
            }

            if (!pi_app_requestDTO.isDirectWingHead) {
                detailsDTO.status = CommonApprovalStatus.SECOND_APPROVER_PENDING.getValue();
            } else {
                detailsDTO.status = CommonApprovalStatus.THIRD_APPROVER_PENDING.getValue();
            }

            Value = request.getParameterValues("approvalAuthority")[index];
            Value = Jsoup.clean(Value, Whitelist.simpleText());
            detailsDTO.approveOneContractAppAuthId = Integer.parseInt(Value);

            Value = request.getParameterValues("sourceFund")[index];
            Value = Jsoup.clean(Value, Whitelist.simpleText());
            detailsDTO.approveOneSourceFundId = Integer.parseInt(Value);


            Value = request.getParameterValues("piAppDetails.procurementMethod")[index];
            if (StringUtils.isValidString(Value)) {
                detailsDTO.approveOneProcurementMethodId = Jsoup.clean(Value, Whitelist.simpleText());
            }

            setCommonFieldData(detailsDTO, request, index);

            if (!pi_app_requestDTO.isDirectWingHead) {
                detailsDTO.approveTwoContractAppAuthId = detailsDTO.approveOneContractAppAuthId;
                detailsDTO.approveTwoProcurementMethodId = detailsDTO.approveOneProcurementMethodId;
                detailsDTO.approveTwoSourceFundId = detailsDTO.approveOneSourceFundId;
                detailsDTO.app_two_condition = detailsDTO.app_one_condition;
                detailsDTO.app_two_isMaintenance = detailsDTO.app_one_isMaintenance;
            } else {
                detailsDTO.approveThreeContractAppAuthId = detailsDTO.approveOneContractAppAuthId;
                detailsDTO.approveThreeProcurementMethodId = detailsDTO.approveOneProcurementMethodId;
                detailsDTO.approveThreeSourceFundId = detailsDTO.approveOneSourceFundId;
                detailsDTO.app_three_condition = detailsDTO.app_one_condition;
                detailsDTO.app_three_isMaintenance = detailsDTO.app_one_isMaintenance;
            }

            // FORWARD ITEM LIST DATA FILTERED BY APPROVER 1 TO APPROVER 2 OR APPROVER 3
            List<Pi_app_request_package_lot_item_listDTO> childDTOS = (List<Pi_app_request_package_lot_item_listDTO>)
                    Pi_app_request_package_lot_item_listDAO.getInstance()
                            .getDTOsByParent("app_details_id", detailsDTO.iD);
            for (Pi_app_request_package_lot_item_listDTO model : childDTOS) {
                if (!pi_app_requestDTO.isDirectWingHead) {
                    model.approverTwoQuantity = model.approverOneQuantity;
                    model.approverTwoUnitPrice = model.approverOneUnitPrice;
                    model.approverTwoEstimatedTotalPrice = model.approverOneQuantity * model.approverOneUnitPrice;
                } else {
                    model.approverThreeQuantity = model.approverOneQuantity;
                    model.approverThreeUnitPrice = model.approverOneUnitPrice;
                    model.approverThreeEstimatedTotalPrice = model.approverOneQuantity * model.approverOneUnitPrice;
                }

                Pi_app_request_package_lot_item_listDAO.getInstance().update(model);
            }
            Pi_app_request_detailsDAO.getInstance().update(detailsDTO);
        }

        pi_app_requestDTO.approveOneDate = System.currentTimeMillis();

        Pi_app_requestDAO.getInstance().update(pi_app_requestDTO);
        if (!pi_app_requestDTO.isDirectWingHead) {
            PiAppNotification.getInstance().sendNotificationAfterFirstApproval(pi_app_requestDTO);
        } else {
            PiAppNotification.getInstance().sendNotificationAfterSecondApproval(pi_app_requestDTO);
        }
        return pi_app_requestDTO;
    }

    public CommonDTO saveApproverTwoData(HttpServletRequest request, UserDTO userDTO) throws Exception {
        logger.debug("saveApproverTwoData");
        Pi_app_requestDTO pi_app_requestDTO = Pi_app_requestDAO.getInstance().getDTOFromID
                (Long.parseLong(request.getParameter("iD")));
        if (pi_app_requestDTO == null) {
            throwException("App request not found", "App request not found");
        }

        if (pi_app_requestDTO.status != CommonApprovalStatus.SECOND_APPROVER_PENDING.getValue()) {
            throwException("Status is not Second Approver Pending", "Status is not Second Approver Pending");
        }

        if (userDTO.organogramID != pi_app_requestDTO.approverTwoOrgId) {
            throwException("Not Approver", "Not Approver");
        }

        pi_app_requestDTO.lastModificationTime = System.currentTimeMillis();
        pi_app_requestDTO.modifiedBy = userDTO.ID;

        pi_app_requestDTO.status = CommonApprovalStatus.THIRD_APPROVER_PENDING.getValue();
        pi_app_requestDTO.approverThreeOrgId = getCommonDAOService().getThirdApprovalOrgId(userDTO.unitID);

        pi_app_requestDTO.approverTwoEmpId = userDTO.employee_record_id;
        Employee_recordsDTO employee_recordsDTO = Employee_recordsRepository.getInstance().
                getById(userDTO.employee_record_id);

        if (employee_recordsDTO != null) {
            pi_app_requestDTO.approverTwoNameEn = employee_recordsDTO.nameEng;
            pi_app_requestDTO.approverTwoNameBn = employee_recordsDTO.nameBng;
        }

        OfficeUnitOrganograms org = OfficeUnitOrganogramsRepository.getInstance().getById(userDTO.organogramID);
        if (org != null) {
            pi_app_requestDTO.approverTwoOfficeUnitOrgNameBn = org.designation_bng;
            pi_app_requestDTO.approverTwoOfficeUnitOrgNameEn = org.designation_eng;
            pi_app_requestDTO.approverTwoOfficeUnitId = org.office_unit_id;
            Office_unitsDTO unit = Office_unitsRepository.getInstance().
                    getOffice_unitsDTOByID(org.office_unit_id);
            if (unit != null) {
                pi_app_requestDTO.approverTwoOfficeUnitNameEn = unit.unitNameEng;
                pi_app_requestDTO.approverTwoOfficeUnitNameBn = unit.unitNameBng;
            }
        }

        int packageLotCount = request.getParameterValues("piAppDetails.appDetailsId").length;
        String Value = "";
        for (int index = 0; index < packageLotCount; index++) {
            Pi_app_request_detailsDTO detailsDTO;
            long currentChildId = Long.parseLong(request.getParameterValues("piAppDetails.appDetailsId")[index]);
            if (currentChildId == -1) {
                continue;
            } else {
                detailsDTO = Pi_app_request_detailsDAO.getInstance().getDTOByID(currentChildId);
                detailsDTO.isSelected = 2;
            }
            detailsDTO = Pi_app_request_detailsDAO.getInstance().getDTOByID(currentChildId);

            detailsDTO.status = CommonApprovalStatus.THIRD_APPROVER_PENDING.getValue();

            Value = request.getParameterValues("approvalAuthority")[index];
            Value = Jsoup.clean(Value, Whitelist.simpleText());
            detailsDTO.approveTwoContractAppAuthId = Integer.parseInt(Value);

            Value = request.getParameterValues("sourceFund")[index];
            Value = Jsoup.clean(Value, Whitelist.simpleText());
            detailsDTO.approveTwoSourceFundId = Integer.parseInt(Value);

            Value = request.getParameterValues("procurementMethod")[index];
            if (StringUtils.isValidString(Value)) {
                detailsDTO.approveTwoProcurementMethodId = Jsoup.clean(Value, Whitelist.simpleText());
            }

            setCommonFieldData(detailsDTO, request, index);

            detailsDTO.approveThreeContractAppAuthId = detailsDTO.approveTwoContractAppAuthId;
            detailsDTO.approveThreeProcurementMethodId = detailsDTO.approveTwoProcurementMethodId;
            detailsDTO.approveThreeSourceFundId = detailsDTO.approveTwoSourceFundId;
            detailsDTO.app_three_condition = detailsDTO.app_two_condition;
            detailsDTO.app_three_isMaintenance = detailsDTO.app_two_isMaintenance;

            Pi_app_request_detailsDAO.getInstance().update(detailsDTO);

            // FORWARD ITEM LIST DATA FILTERED BY APPROVER 2 TO APPROVER 3
            List<Pi_app_request_package_lot_item_listDTO> childDTOS = (List<Pi_app_request_package_lot_item_listDTO>)
                    Pi_app_request_package_lot_item_listDAO.getInstance()
                            .getDTOsByParent("app_details_id", detailsDTO.iD);
            for (Pi_app_request_package_lot_item_listDTO model : childDTOS) {
                model.approverThreeQuantity = model.approverTwoQuantity;
                model.approverThreeUnitPrice = model.approverTwoUnitPrice;
                model.approverThreeEstimatedTotalPrice = model.approverTwoQuantity * model.approverTwoUnitPrice;

                Pi_app_request_package_lot_item_listDAO.getInstance().update(model);
            }
        }

        pi_app_requestDTO.approveTwoDate = System.currentTimeMillis();

        Pi_app_requestDAO.getInstance().update(pi_app_requestDTO);
        PiAppNotification.getInstance().sendNotificationAfterSecondApproval(pi_app_requestDTO);
        return pi_app_requestDTO;
    }

    public CommonDTO saveApproverThreeData(HttpServletRequest request, UserDTO userDTO) throws Exception {
        logger.debug("saveApproverThreeData");
        Pi_app_requestDTO pi_app_requestDTO = Pi_app_requestDAO.getInstance().getDTOFromID
                (Long.parseLong(request.getParameter("iD")));
        if (pi_app_requestDTO == null) {
            throwException("App request not found", "App request not found");
        }

        if (pi_app_requestDTO.status != CommonApprovalStatus.THIRD_APPROVER_PENDING.getValue()) {
            throwException("Status is not Third Approver Pending", "Status is not Third Approver Pending");
        }

        if (userDTO.organogramID != pi_app_requestDTO.approverThreeOrgId) {
            throwException("Not Approver", "Not Approver");
        }

        pi_app_requestDTO.lastModificationTime = System.currentTimeMillis();
        pi_app_requestDTO.modifiedBy = userDTO.ID;

        pi_app_requestDTO.status = CommonApprovalStatus.SATISFIED.getValue();

        pi_app_requestDTO.approverThreeEmpId = userDTO.employee_record_id;
        Employee_recordsDTO employee_recordsDTO = Employee_recordsRepository.getInstance().
                getById(userDTO.employee_record_id);

        if (employee_recordsDTO != null) {
            pi_app_requestDTO.approverThreeNameEn = employee_recordsDTO.nameEng;
            pi_app_requestDTO.approverThreeNameBn = employee_recordsDTO.nameBng;
        }

        OfficeUnitOrganograms org = OfficeUnitOrganogramsRepository.getInstance().getById(userDTO.organogramID);
        if (org != null) {
            pi_app_requestDTO.approverThreeOfficeUnitOrgNameBn = org.designation_bng;
            pi_app_requestDTO.approverThreeOfficeUnitOrgNameEn = org.designation_eng;
            pi_app_requestDTO.approverThreeOfficeUnitId = org.office_unit_id;
            Office_unitsDTO unit = Office_unitsRepository.getInstance().
                    getOffice_unitsDTOByID(org.office_unit_id);
            if (unit != null) {
                pi_app_requestDTO.approverThreeOfficeUnitNameEn = unit.unitNameEng;
                pi_app_requestDTO.approverThreeOfficeUnitNameBn = unit.unitNameBng;
            }
        }

        int packageLotCount = request.getParameterValues("piAppDetails.appDetailsId").length;
        String Value = "";
        for (int index = 0; index < packageLotCount; index++) {
            Pi_app_request_detailsDTO detailsDTO;
            long currentChildId = Long.parseLong(request.getParameterValues("piAppDetails.appDetailsId")[index]);
            if (currentChildId == -1) {
                continue;
            } else {
                detailsDTO = Pi_app_request_detailsDAO.getInstance().getDTOByID(currentChildId);
            }

            detailsDTO.status = CommonApprovalStatus.SATISFIED.getValue();

            Value = request.getParameterValues("approvalAuthority")[index];
            Value = Jsoup.clean(Value, Whitelist.simpleText());
            detailsDTO.approveThreeContractAppAuthId = Integer.parseInt(Value);

            Value = request.getParameterValues("sourceFund")[index];
            Value = Jsoup.clean(Value, Whitelist.simpleText());
            detailsDTO.approveThreeSourceFundId = Integer.parseInt(Value);

            Value = request.getParameterValues("procurementMethod")[index];
            if (StringUtils.isValidString(Value)) {
                detailsDTO.approveThreeProcurementMethodId = Jsoup.clean(Value, Whitelist.simpleText());
            }


            setCommonFieldData(detailsDTO, request, index);

            Pi_app_request_detailsDAO.getInstance().update(detailsDTO);
        }

        pi_app_requestDTO.approveThreeDate = System.currentTimeMillis();

        Pi_app_requestDAO.getInstance().update(pi_app_requestDTO);
        PiAppNotification.getInstance().sendNotificationAfterThirdApproval(pi_app_requestDTO);
        return pi_app_requestDTO;
    }

    public void setCommonFieldData(Pi_app_request_detailsDTO detailsDTO, HttpServletRequest request, int index) {
        String Value = "";
        Value = request.getParameterValues("piAppDetails.piPackageItemMapId")[index];
        if (isValueValid(Value)) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
            detailsDTO.piPackageItemMapId = Long.parseLong(Value);
        }
        logger.debug("piPackageItemMapId = " + Value);


        Value = request.getParameterValues("piAppDetails.serialNumber")[index];
        if (isValueValid(Value)) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
            detailsDTO.serialNum = Long.parseLong(Value);
        }
        logger.debug("serialNumber = " + Value);


        Value = request.getParameterValues("piAppDetails.packageFinalId")[index];
        if (isValueValid(Value)) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
            detailsDTO.packageFinalId = Long.parseLong(Value);
        }
        logger.debug("packageFinalId = " + Value);


        Value = request.getParameterValues("piAppDetails.lotFinalId")[index];
        if (isValueValid(Value)) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
            detailsDTO.lotFinalId = Long.parseLong(Value);
        }
        logger.debug("lotFinalId = " + Value);


        Value = request.getParameterValues("piAppDetails.packageUnitPrice")[index];
        if (isValueValid(Value)) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
            detailsDTO.packageUnitPrice = Value;
        }
        logger.debug("packageUnitPrice = " + Value);

        Value = request.getParameterValues("piAppDetails.timeCodeForProcess")[index];
        if (isValueValid(Value)) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
            detailsDTO.timeCodeForProcess = Value;
        }
        logger.debug("timeCodeForProcess = " + Value);


        Value = request.getParameterValues("piAppDetails.inviteAdvertiseTender")[index];
        if (isValueValid(Value)) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
            detailsDTO.inviteAdvertiseTender = Value;
        }
        logger.debug("inviteAdvertiseTender = " + Value);


        Value = request.getParameterValues("piAppDetails.tenderOpening")[index];
        if (isValueValid(Value)) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
            detailsDTO.tenderOpening = Value;
        }
        logger.debug("tenderOpening = " + Value);


        Value = request.getParameterValues("piAppDetails.tenderEvaluation")[index];
        if (isValueValid(Value)) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
            detailsDTO.tenderEvaluation = Value;
        }
        logger.debug("tenderEvaluation = " + Value);


        Value = request.getParameterValues("piAppDetails.approvalToAward")[index];
        if (isValueValid(Value)) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
            detailsDTO.approvalToAward = Value;
        }
        logger.debug("approvalToAward = " + Value);


        Value = request.getParameterValues("piAppDetails.notificationOfAward")[index];
        if (isValueValid(Value)) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
            detailsDTO.notificationOfAward = Value;
        }
        logger.debug("notificationOfAward = " + Value);


        Value = request.getParameterValues("piAppDetails.signingOfContract")[index];
        if (isValueValid(Value)) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
            detailsDTO.signingOfContract = Value;
        }
        logger.debug("signingOfContract = " + Value);


        Value = request.getParameterValues("piAppDetails.totalTimeToContractSignature")[index];
        if (isValueValid(Value)) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
            detailsDTO.totalTimeToContractSignature = Value;
        }
        logger.debug("totalTimeToContractSignature = " + Value);


        Value = request.getParameterValues("piAppDetails.timeForCompletionOfContract")[index];
        if (isValueValid(Value)) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
            detailsDTO.timeForCompletionOfContract = Value;
        }
        logger.debug("timeForCompletionOfContract = " + Value);


        Value = request.getParameterValues("piAppDetails.remark")[index];
        if (isValueValid(Value)) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
            detailsDTO.remark = Value;
        }
        logger.debug("remark = " + Value);
    }

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        String language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
        logger.debug("%%%% addPi_app_request");
        Pi_app_requestDTO pi_app_requestDTO;

        if (Boolean.TRUE.equals(addFlag)) {
            pi_app_requestDTO = new Pi_app_requestDTO();
            pi_app_requestDTO.insertionDate = pi_app_requestDTO.lastModificationTime = pi_app_requestDTO.requestedDate
                    = System.currentTimeMillis();
            pi_app_requestDTO.insertedByUserId = userDTO.ID;
            pi_app_requestDTO.insertedByOrganogramId = userDTO.organogramID;
        } else {
            pi_app_requestDTO = Pi_app_requestDAO.getInstance().getDTOFromID(Long.parseLong(request.getParameter("iD")));
            if (pi_app_requestDTO == null) {
                throwException("App request not found", "App request not found");
            }

            if (!(pi_app_requestDTO.status == CommonApprovalStatus.PENDING.getValue() ||
                    pi_app_requestDTO.status == CommonApprovalStatus.SATISFIED.getValue())) {
                throwException("Status is not Pending", "Status is not Pending");
            }

            pi_app_requestDTO.lastModificationTime = System.currentTimeMillis();
            pi_app_requestDTO.modifiedBy = userDTO.ID;
        }

        String value = "";

        value = request.getParameter("fiscalYearId");
        if (StringUtils.isValidString(value)) {
            pi_app_requestDTO.fiscalYearId = Long.parseLong(value);
            if (addFlag) {
                validateFiscalYear(pi_app_requestDTO.fiscalYearId);
            }
        } else {
            UtilCharacter.throwException("অর্থবছর নির্বাচন করুন!", "Select fiscal year!");
        }


        pi_app_requestDTO.officeUnitId = Long.parseLong(request.getParameter("officeUnitId"));

        OfficeUnitModel model = Office_unitsRepository.getInstance().getByOfficeUnitId(pi_app_requestDTO.officeUnitId);

        if (!model.isSecretaryOrUnderOffice) {
            throwException("শাখা সচিবের দপ্তরের অন্তর্গত নয় ", "Office Unit is not Under Secretary");
        }

        boolean skipFirstLayer = false;
        long firstApproverOrgId = -1;

//        if (model.isSecretaryOrJsOrDs) {
//            skipFirstLayer = true;
//            pi_app_requestDTO.status = CommonApprovalStatus.SECOND_APPROVER_PENDING.getValue();
//            pi_app_requestDTO.skip_first_layer = true;
//            pi_app_requestDTO.approverTwoOrgId = getCommonDAOService().getSecondApprovalOrgId();
//
//        } else {
//            firstApproverOrgId = model.superiorOrganogramId;
        firstApproverOrgId = getCommonDAOService().getFirstApprovalOrgId(userDTO.unitID);
        if (firstApproverOrgId == -1) {
            throwException("এই অফিস ইউনিটের জন্য সুপিরিয়র  বাছাই করা হয় নি", "Superior Not Selected for this Office Unit");
        }
        if (pi_app_requestDTO.status != CommonApprovalStatus.SATISFIED.getValue()) {
            pi_app_requestDTO.status = CommonApprovalStatus.PENDING.getValue();
        }
        pi_app_requestDTO.approverOneOrgId = firstApproverOrgId;
//        }

//		uniquenessValidation(pi_app_requestDTO.officeUnitId, pi_app_requestDTO.fiscalYearId, pi_app_requestDTO.iD);

        pi_app_requestDTO.requesterEmpId = userDTO.employee_record_id;
        Employee_recordsDTO employee_recordsDTO = Employee_recordsRepository.getInstance().
                getById(userDTO.employee_record_id);

        if (employee_recordsDTO != null) {
            pi_app_requestDTO.requesterNameEn = employee_recordsDTO.nameEng;
            pi_app_requestDTO.requesterNameBn = employee_recordsDTO.nameBng;
        }

        pi_app_requestDTO.requesterOrgId = userDTO.organogramID;
        OfficeUnitOrganograms org = OfficeUnitOrganogramsRepository.getInstance().getById(userDTO.organogramID);
        if (org != null) {
            pi_app_requestDTO.requesterOfficeUnitOrgNameBn = org.designation_bng;
            pi_app_requestDTO.requesterOfficeUnitOrgNameEn = org.designation_eng;
            pi_app_requestDTO.requesterOfficeUnitId = org.office_unit_id;
            Office_unitsDTO unit = Office_unitsRepository.getInstance().
                    getOffice_unitsDTOByID(org.office_unit_id);
            if (unit != null) {
                pi_app_requestDTO.requesterOfficeUnitNameEn = unit.unitNameEng;
                pi_app_requestDTO.requesterOfficeUnitNameBn = unit.unitNameBng;
            }
        }


        logger.debug("Done adding  addPi_app_request dto = " + pi_app_requestDTO);


        // childValidation
//		childValidation(request);

        long returnedID;


        if (Boolean.TRUE.equals(addFlag)) {
            returnedID = Pi_app_requestDAO.getInstance().add(pi_app_requestDTO);
        } else {
            // WHEN APPROVAL IS DONE
            if (pi_app_requestDTO.status == CommonApprovalStatus.SATISFIED.getValue()) {
                returnedID = pi_app_requestDTO.iD;
            } else {
                returnedID = Pi_app_requestDAO.getInstance().update(pi_app_requestDTO);
            }
        }
        addChild(request, returnedID, pi_app_requestDTO);
        PiAppNotification.getInstance().sendNotificationAfterRequest(pi_app_requestDTO);
        return pi_app_requestDTO;

    }

    private void addChild(HttpServletRequest request, long appId, Pi_app_requestDTO pi_app_requestDTO)
            throws Exception {
        int packageLotCount = request.getParameterValues("piAppDetails.serialNumber").length;
        String Value = "";
        boolean addFlag = false;
        for (int index = 0; index < packageLotCount; index++) {
            Pi_app_request_detailsDTO detailsDTO;

            Value = request.getParameterValues("piAppDetails.iD")[index];
            if (Value == null || Value.equals("") || Value.equals("-1")) {
                addFlag = true;
                detailsDTO = new Pi_app_request_detailsDTO();
                detailsDTO.status = CommonApprovalStatus.PENDING.getValue();
            } else {
                addFlag = false;
                detailsDTO = Pi_app_request_detailsDAO.getInstance().getDTOByID(Long.parseLong(Value));
                detailsDTO.iD = Long.parseLong(Value);
                if (detailsDTO.status == CommonApprovalStatus.SATISFIED.getValue()) {
                    continue;
                }
            }

            detailsDTO.app_id = appId;
            detailsDTO.fiscalYearId = pi_app_requestDTO.fiscalYearId;

            Value = request.getParameterValues("piAppDetails.piPackageItemMapId")[index];
            if (isValueValid(Value)) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
                detailsDTO.piPackageItemMapId = Long.parseLong(Value);
            }
            logger.debug("piPackageItemMapId = " + Value);


            Value = request.getParameterValues("piAppDetails.serialNumber")[index];
            if (isValueValid(Value)) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
                detailsDTO.serialNum = Long.parseLong(Value);
            }
            logger.debug("serialNumber = " + Value);


            Value = request.getParameterValues("piAppDetails.packageFinalId")[index];
            if (isValueValid(Value)) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
                detailsDTO.packageFinalId = Long.parseLong(Value);
            }
            logger.debug("packageFinalId = " + Value);


            Value = request.getParameterValues("piAppDetails.lotFinalId")[index];
            if (isValueValid(Value)) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
                detailsDTO.lotFinalId = Long.parseLong(Value);
            }
            logger.debug("lotFinalId = " + Value);


            Value = request.getParameterValues("piAppDetails.packageUnitPrice")[index];
            if (isValueValid(Value)) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
                detailsDTO.packageUnitPrice = Value;
            }
            logger.debug("packageUnitPrice = " + Value);


            Value = request.getParameterValues("piAppDetails.procurementMethod")[index];
            if (isValueValid(Value)) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
                detailsDTO.procurementMethodId = Value;
            }
            logger.debug("procurementMethodId = " + Value);


            Value = request.getParameterValues("piAppDetails.approvalAuthority")[index];
            if (isValueValid(Value)) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
                detailsDTO.contractAppAuthId = Long.parseLong(Value);
            }
            logger.debug("approvalAuthority = " + Value);


            Value = request.getParameterValues("piAppDetails.sourceFund")[index];
            if (isValueValid(Value)) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
                detailsDTO.sourceFundId = Long.parseLong(Value);
            }
            logger.debug("sourceFund = " + Value);


            Value = request.getParameterValues("piAppDetails.timeCodeForProcess")[index];
            if (isValueValid(Value)) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
                detailsDTO.timeCodeForProcess = Value;
            }
            logger.debug("timeCodeForProcess = " + Value);


            Value = request.getParameterValues("piAppDetails.inviteAdvertiseTender")[index];
            if (isValueValid(Value)) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
                detailsDTO.inviteAdvertiseTender = Value;
            }
            logger.debug("inviteAdvertiseTender = " + Value);


            Value = request.getParameterValues("piAppDetails.tenderOpening")[index];
            if (isValueValid(Value)) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
                detailsDTO.tenderOpening = Value;
            }
            logger.debug("tenderOpening = " + Value);


            Value = request.getParameterValues("piAppDetails.tenderEvaluation")[index];
            if (isValueValid(Value)) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
                detailsDTO.tenderEvaluation = Value;
            }
            logger.debug("tenderEvaluation = " + Value);


            Value = request.getParameterValues("piAppDetails.approvalToAward")[index];
            if (isValueValid(Value)) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
                detailsDTO.approvalToAward = Value;
            }
            logger.debug("approvalToAward = " + Value);


            Value = request.getParameterValues("piAppDetails.notificationOfAward")[index];
            if (isValueValid(Value)) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
                detailsDTO.notificationOfAward = Value;
            }
            logger.debug("notificationOfAward = " + Value);


            Value = request.getParameterValues("piAppDetails.signingOfContract")[index];
            if (isValueValid(Value)) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
                detailsDTO.signingOfContract = Value;
            }
            logger.debug("signingOfContract = " + Value);


            Value = request.getParameterValues("piAppDetails.totalTimeToContractSignature")[index];
            if (isValueValid(Value)) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
                detailsDTO.totalTimeToContractSignature = Value;
            }
            logger.debug("totalTimeToContractSignature = " + Value);


            Value = request.getParameterValues("piAppDetails.timeForCompletionOfContract")[index];
            if (isValueValid(Value)) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
                detailsDTO.timeForCompletionOfContract = Value;
            }
            logger.debug("timeForCompletionOfContract = " + Value);


            Value = request.getParameterValues("piAppDetails.remark")[index];
            if (isValueValid(Value)) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
                detailsDTO.remark = Value;
            }
            logger.debug("remark = " + Value);


            long appDetailsId;
            // ADD OR UPDATE PACKAGE/ PACKAGE LOT INSIDE APP
            if (addFlag) {
                appDetailsId = Pi_app_request_detailsDAO.getInstance().add(detailsDTO);
            } else {
                appDetailsId = detailsDTO.iD;
                // WHEN APPROVAL IS DONE, EXISTING DATA CAN NOT BE CHANGED
                if(pi_app_requestDTO.status != CommonApprovalStatus.SATISFIED.getValue()){
                    Pi_app_request_detailsDAO.getInstance().update(detailsDTO);
                }
            }

            // SAVE ITEM LIST OF APP PACKAGE LOT
            // THIS IS FOR ITEM DATA MANIPULATION OF APPROVER 1,2,3
            if (addFlag) {
                List<PiPackageItemMapChildDTO> childDTOS = (List<PiPackageItemMapChildDTO>) PiPackageItemMapChildDAO.getInstance()
                        .getDTOsByParent("pi_package_item_map_id", detailsDTO.piPackageItemMapId);
                Pi_app_request_package_lot_item_listDTO itemModel = new Pi_app_request_package_lot_item_listDTO();
                for (PiPackageItemMapChildDTO model : childDTOS) {
                    itemModel.appId = appId;
                    itemModel.appDetailsId = appDetailsId;
                    itemModel.piPackageItemMapId = model.piPackageItemMapId;
                    itemModel.serialNumber = model.serialNumber;
                    itemModel.fiscalYearId = pi_app_requestDTO.fiscalYearId;
                    itemModel.officeUnitId = model.officeUnitId;
                    itemModel.packageFinalId = model.piPackageFinalId;
                    itemModel.lotFinalId = model.piLotFinalId;
                    itemModel.itemGroupId = model.itemGroupId;
                    itemModel.itemTypeId = model.itemTypeId;
                    itemModel.itemId = model.itemId;

                    itemModel.requestedQuantity = model.itemQuantity;
                    itemModel.requestedUnitPrice = model.itemUnitPrice;
                    itemModel.requestedEstimatedTotalPrice = model.itemQuantity * model.itemUnitPrice;

                    if (pi_app_requestDTO.status == CommonApprovalStatus.SATISFIED.getValue()) {
                        itemModel.approverThreeQuantity = model.itemQuantity;
                        itemModel.approverThreeUnitPrice = model.itemUnitPrice;
                        itemModel.approverThreeEstimatedTotalPrice = model.itemQuantity * model.itemUnitPrice;
                    } else {
                        itemModel.approverOneQuantity = model.itemQuantity;
                        itemModel.approverOneUnitPrice = model.itemUnitPrice;
                        itemModel.approverOneEstimatedTotalPrice = model.itemQuantity * model.itemUnitPrice;
                    }

                    Pi_app_request_package_lot_item_listDAO.getInstance().add(itemModel);
                }
            }
        }
    }

    private void throwException(String bn, String en) throws Exception {
        String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
        throw new Exception(UtilCharacter.getDataByLanguage(Language, bn, en));
    }

    @Override
    protected void deleteT(HttpServletRequest request, UserDTO userDTO) {
        String[] IDsToDelete = request.getParameterValues("ID");
        if (IDsToDelete.length > 0) {
            Pi_app_requestDAO dao = Pi_app_requestDAO.getInstance();
            Pi_app_request_detailsDAO childDAO = Pi_app_request_detailsDAO.getInstance();
            Stream.of(IDsToDelete)
                    .map(Long::parseLong)
                    .filter(i -> {
                        Pi_app_requestDTO dto = dao.getDTOByID(i);
                        return dto != null && dto.status == CommonApprovalStatus.PENDING.getValue();
                    }).forEach(i -> {
                        List<Pi_app_request_detailsDTO> childDTOs = childDAO.getByAppId(i);
                        childDTOs.forEach(dto -> childDAO.delete(userDTO.organogramID, dto.iD));
                        dao.delete(userDTO.organogramID, i);
                    });
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String actionType = request.getParameter("actionType");
        CommonLoginData commonLoginData = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get();
        if ("getPackageByUnitIdAndFiscalYearId".equals(actionType)) {
            try {
                long fiscalYearId = Long.parseLong(request.getParameter("fiscalYearId"));
                long unitId = Long.parseLong(request.getParameter("unitId"));
                //long unitId = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO.unitID;
                String language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
                List<OptionDTO> optionDTOList = new ArrayList<>();

                List<Pi_app_requestDTO> requestDTOS = Pi_app_requestDAO.getInstance().
                        getByFiscalYearIdAndUnitId(fiscalYearId, unitId)
                        .stream()
                        .filter(i -> i.status == CommonApprovalStatus.SATISFIED.getValue())
                        .collect(Collectors.toList());
                if (!requestDTOS.isEmpty()) {
                    List<Pi_app_request_detailsDTO> detailsDTOS = Pi_app_request_detailsDAO.getInstance()
                            .getByAppId(requestDTOS.get(0).iD);
                    List<Long> packageIds = detailsDTOS.stream().map(i -> i.packageId).distinct().
                            collect(Collectors.toList());
                    List<Procurement_packageDTO> packageDTOS = Procurement_packageRepository.getInstance().
                            getDTOsByIds(packageIds);
                    optionDTOList = packageDTOS
                            .stream()
                            .map(e -> new OptionDTO(e.nameEn, e.nameBn, String.valueOf(e.iD)))
                            .collect(Collectors.toList());
                }


                String responseText = Utils.buildOptions(optionDTOList, language, "-1");
                ApiResponse.sendSuccessResponse(response, responseText);
            } catch (Exception ex) {
                logger.error(ex);
                ApiResponse.sendErrorResponse(response, ex.getMessage());
            }
            return;
        } else if ("getItemByFiscalYearIdAndPackageIdAndLotId".equals(actionType)) {
            try {
                long fiscalYearId = Long.parseLong(request.getParameter("fiscalYearId"));
                long packageId = Long.parseLong(request.getParameter("packageId"));
                String lotIdStr = request.getParameter("lotId");

                List<Pi_app_request_package_lot_item_listDTO> items;
                if (lotIdStr == null || lotIdStr.equals("")) {
                    lotIdStr = "-1";
                    items = (List<Pi_app_request_package_lot_item_listDTO>) Pi_app_request_package_lot_item_listDAO
                            .getInstance().getDTOsByParent("package_final_id", packageId);
                } else {
                    long lotId = Long.parseLong(lotIdStr);
                    items = (List<Pi_app_request_package_lot_item_listDTO>) Pi_app_request_package_lot_item_listDAO
                            .getInstance().getDTOsByParent("lot_final_id", lotId);
                }

                // FILTER ITEMS BY FISCAL YEAR ID
                items = items.stream().filter(item -> item.fiscalYearId == fiscalYearId).collect(Collectors.toList());

                // FILTER ITEMS OUT WHICH ARE GOT TENDER ALREADY
                String finalLotIdStr = lotIdStr;
                List<Long> itemsIncludedForTender = Pi_package_vendor_itemsDAO.getInstance().getAllDTOs()
                        .stream().filter(dto -> dto.packageId == packageId)
                        .filter(dto -> dto.fiscalYearId == fiscalYearId)
                        .filter(dto -> dto.lotId == Long.parseLong(finalLotIdStr))
                        .map(dto -> dto.productId)
                        .collect(Collectors.toList());
                items = items.stream().filter(item -> !itemsIncludedForTender.contains(item.itemId)).collect(Collectors.toList());

                HashSet<Long> itemsIds = new HashSet<>();
                for (Pi_app_request_package_lot_item_listDTO item : items) {
                    itemsIds.add(item.itemId);
                }
                Procurement_goodsDAO procurement_goodsDAO = Procurement_goodsDAO.getInstance();

                List<PiAppProductModel> modelList = itemsIds
                        .stream()
                        .map(i -> {
                            PiAppProductModel model = new PiAppProductModel();
                            model.iD = String.valueOf(i);
                            model.productName = procurement_goodsDAO.getCircularData
                                    (i, HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language);
                            return model;
                        })
                        .collect(Collectors.toList());

                response.setContentType("application/json");
                response.setCharacterEncoding("UTF-8");
                response.getWriter().println(gson.toJson(modelList));

            } catch (Exception ex) {
                logger.error(ex);
                ApiResponse.sendErrorResponse(response, ex.getMessage());
            }
            return;
        } else if ("getItemByUnitIdAndFiscalYearId".equals(actionType)) {
            try {
                long fiscalYearId = Long.parseLong(request.getParameter("fiscalYearId"));
                long unitId = Long.parseLong(request.getParameter("unitId"));
                long itemId = Long.parseLong(request.getParameter("itemId"));
                List<Pi_app_request_detailsDTO> detailsDTOS = new ArrayList<>();
                List<Pi_app_requestDTO> requestDTOS = Pi_app_requestDAO.getInstance().
                        getByFiscalYearIdAndUnitId(fiscalYearId, unitId)
                        .stream()
                        .filter(i -> i.status == CommonApprovalStatus.SATISFIED.getValue())
                        .collect(Collectors.toList());
                if (!requestDTOS.isEmpty()) {
                    detailsDTOS = Pi_app_request_detailsDAO.getInstance()
                            .getByAppId(requestDTOS.get(0).iD).stream().filter(i -> i.itemId == itemId).
                            collect(Collectors.toList());
                }
                String responseText = this.gson.toJson(detailsDTOS);
                ApiResponse.sendSuccessResponse(response, responseText);

            } catch (Exception ex) {
                logger.error(ex);
                ApiResponse.sendErrorResponse(response, ex.getMessage());
            }
            return;
        } else if ("getAppDetailsByUnitIdAndFiscalYearId".equals(actionType)) {
            try {
                long fiscalYearId = Long.parseLong(request.getParameter("fiscalYearId"));
                long unitId = Long.parseLong(request.getParameter("unitId"));
                String language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
                List<Pi_app_request_detailsDTO> detailsDTOS = new ArrayList<>();

                List<Pi_app_requestDTO> requestDTOS = Pi_app_requestDAO.getInstance().
                        getByFiscalYearIdAndUnitId(fiscalYearId, unitId)
                        .stream()
                        .filter(i -> i.status == CommonApprovalStatus.SATISFIED.getValue())
                        .collect(Collectors.toList());
                if (!requestDTOS.isEmpty()) {
                    Procurement_goodsDAO procurement_goodsDAO = Procurement_goodsDAO.getInstance();
                    detailsDTOS = Pi_app_request_detailsDAO.getInstance()
                            .getByAppId(requestDTOS.get(0).iD).stream().
                            peek(i -> {
                                i.itemName = procurement_goodsDAO.getCircularData(i.itemId, language);
                                Procurement_packageDTO packageDTO = Procurement_packageRepository.getInstance().
                                        getProcurement_packageDTOByID(i.packageId);
                                if (packageDTO != null) {
                                    i.packageName = UtilCharacter.getDataByLanguage
                                            (language, packageDTO.nameBn, packageDTO.nameEn);
                                }
                            }).collect(Collectors.toList());
                }
                String responseText = this.gson.toJson(detailsDTOS);
                ApiResponse.sendSuccessResponse(response, responseText);

            } catch (Exception ex) {
                logger.error(ex);
                ApiResponse.sendErrorResponse(response, ex.getMessage());
            }
            return;
        } else if ("search".equals(actionType)) {
            if (Utils.checkPermission(commonLoginData.userDTO, getSearchMenuConstants()) && getSearchPagePermission(request)) {
                try {
                    long roleId = commonLoginData.userDTO.roleID;
                    if (roleId != SessionConstants.INVENTORY_ADMIN_ROLE && roleId != SessionConstants.ADMIN_ROLE) {
                        Map<String, String> extraCriteriaMap;
                        if (request.getAttribute(CommonDAOService.CRITERIA_MAP_KEY) == null) {
                            extraCriteriaMap = new HashMap<>();
                        } else {
                            extraCriteriaMap = (Map<String, String>) request.getAttribute(CommonDAOService.CRITERIA_MAP_KEY);
                        }
                        extraCriteriaMap.put("requesterId",
                                String.valueOf(commonLoginData.userDTO.organogramID));
                        request.setAttribute(CommonDAOService.CRITERIA_MAP_KEY, extraCriteriaMap);
                    }

                    search(request, response);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return;
        } else if ("addApprovalOne".equals(actionType)) {
            if (Utils.checkPermission(commonLoginData.userDTO, getFirstApprovalMenu())) {
                try {
                    String url = "pi_app_request/pi_app_requestApproverOneEdit.jsp";
                    request.getRequestDispatcher(url).forward(request, response);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            return;
        } else if ("addApprovalTwo".equals(actionType)) {
            if (Utils.checkPermission(commonLoginData.userDTO, getSecondApprovalMenu())) {
                try {
                    String url = "pi_app_request/pi_app_requestApproverTwoEdit.jsp";
                    request.getRequestDispatcher(url).forward(request, response);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            return;
        } else if ("addApprovalThree".equals(actionType)) {
            if (Utils.checkPermission(commonLoginData.userDTO, getThirdApprovalMenu())) {
                try {
                    String url = "pi_app_request/pi_app_requestApproverThreeEdit.jsp";
                    request.getRequestDispatcher(url).forward(request, response);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            return;
        } else if ("finalApp".equals(actionType)) {
            if (Utils.checkPermission(commonLoginData.userDTO, getFinalSearchMenu())) {
                try {
                    String url = "pi_app_request/final_app.jsp";
                    request.getRequestDispatcher(url).forward(request, response);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            return;
        } else if ("appView".equals(actionType)) {
            if (Utils.checkPermission(commonLoginData.userDTO, getFinalSearchMenu())) {
                try {
                    String url = "pi_app_request/appView.jsp";
                    request.getRequestDispatcher(url).forward(request, response);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            return;
        } else if ("getFiscalYearData".equals(actionType)) {
            if (Utils.checkPermission(commonLoginData.userDTO, getFinalSearchMenu())) {
                try {
                    getFiscalYearData(request, response);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            return;
        } else if ("forApprovalOne".equals(actionType)) {
            if (Utils.checkPermission(commonLoginData.userDTO, getFirstApprovalMenu())) {
                try {
                    String whereClause = " and approver_one_org_id = " + HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO.organogramID;
                    Map<String, String> params = HttpRequestUtils.buildRequestParams(request);
                    RecordNavigator recordNavigator = getCommonDAOService().getRecordNavigator(params, null, whereClause, null);
                    request.setAttribute(RECORD_NAVIGATOR, recordNavigator);
                    String ajax = request.getParameter("ajax");
                    String url;
                    if (ajax != null && ajax.equalsIgnoreCase("true")) {
                        url = "pi_app_request/approverOneSearchForm.jsp";
                    } else {
                        url = "pi_app_request/approverOneSearch.jsp";
                    }
                    request.getRequestDispatcher(url).forward(request, response);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            return;
        } else if ("forApprovalTwo".equals(actionType)) {
            if (Utils.checkPermission(commonLoginData.userDTO, getSecondApprovalMenu())) {
                try {
                    String whereClause = " and approver_two_org_id = " + HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO.organogramID;
                    Map<String, String> params = HttpRequestUtils.buildRequestParams(request);
                    RecordNavigator recordNavigator = getCommonDAOService().getRecordNavigator(params, null, whereClause, null);
                    request.setAttribute(RECORD_NAVIGATOR, recordNavigator);
                    String ajax = request.getParameter("ajax");
                    String url;
                    if (ajax != null && ajax.equalsIgnoreCase("true")) {
                        url = "pi_app_request/approverTwoSearchForm.jsp";
                    } else {
                        url = "pi_app_request/approverTwoSearch.jsp";
                    }
                    request.getRequestDispatcher(url).forward(request, response);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            return;
        } else if ("forApprovalThree".equals(actionType)) {
            if (Utils.checkPermission(commonLoginData.userDTO, getSecondApprovalMenu())) {
                try {
                    String whereClause = " and approver_three_org_id = " + HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO.organogramID;
                    Map<String, String> params = HttpRequestUtils.buildRequestParams(request);
                    RecordNavigator recordNavigator = getCommonDAOService().getRecordNavigator(params, null, whereClause, null);
                    request.setAttribute(RECORD_NAVIGATOR, recordNavigator);
                    String ajax = request.getParameter("ajax");
                    String url;
                    if (ajax != null && ajax.equalsIgnoreCase("true")) {
                        url = "pi_app_request/approverThreeSearchForm.jsp";
                    } else {
                        url = "pi_app_request/approverThreeSearch.jsp";
                    }
                    request.getRequestDispatcher(url).forward(request, response);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            return;
        } else if ("getItemSpecificAppReport".equals(actionType)) {
            if (Utils.checkPermission(commonLoginData.userDTO, getSecondApprovalMenu())) {
                try {
                    String url = "pi_app_report/pi_app_report.jsp";
                    request.getRequestDispatcher(url).forward(request, response);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            return;
        } else if ("getItemSpecificFiscalYearData".equals(actionType)) {
            try {
                getItemSpecificFiscalYearData(request, response);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return;
        } else if ("isAvailableAppReport".equals(actionType)) {
            try {
                long fiscalYearId = Long.parseLong(request.getParameter("fiscalYearId"));
                long officeUnitId = commonLoginData.userDTO.unitID;
                Pi_app_requestDAO appRequestDAO = Pi_app_requestDAO.getInstance();
                Map<String, Object> res = new HashMap<>();
                List<Pi_app_requestDTO> piAppRequestDTOS = appRequestDAO
                        .getByFiscalYearIdAndUnitId(fiscalYearId, officeUnitId)
                        .stream()
                        .filter(i -> i.status == CommonApprovalStatus.SATISFIED.getValue())
                        .collect(Collectors.toList());
                if (piAppRequestDTOS.isEmpty()) {
                    res.put("appAvailable", false);
                    res.put("appMsg", UtilCharacter.getDataByLanguage(commonLoginData.language,
                            "নির্বাচিত অর্থবছরের জন্যে এই ইউনিট এর অনুমোদিত বার্ষিক ক্রয় পরিকল্পনা নেই ",
                            "There is no approved annual purchase plan for this unit for the selected fiscal year"));
                } else {
                    String itemInString = Jsoup.clean(request.getParameter("itemId"), Whitelist.simpleText());
                    if (itemInString.length() > 0) {
                        long itemId = Long.parseLong(itemInString);
                        List<Pi_app_request_detailsDTO> detailsDTOS = Pi_app_request_detailsDAO.getInstance()
                                .getApprovedItemByAppIdAndItemId(piAppRequestDTOS.get(0).iD, itemId);
                        if (detailsDTOS.isEmpty()) {
                            res.put("appAvailable", false);
                            res.put("appMsg", UtilCharacter.getDataByLanguage(commonLoginData.language,
                                    "নির্বাচিত আইটেমটির জন্যে ডাটা নেই",
                                    "There is no data for the selected item"));
                        } else {
                            res.put("appAvailable", true);
                        }
                    } else {
                        res.put("appAvailable", true);
                    }
                }

                response.setContentType("application/json");
                response.setCharacterEncoding("UTF-8");
                response.getWriter().println(new Gson().toJson(res));

            } catch (Exception e) {
                e.printStackTrace();
            }

            return;
        }

        super.doGet(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        CommonLoginData commonLoginData = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get();
        try {
            switch (request.getParameter("actionType")) {
                case "ajax_approver_one_add":
                    if (Utils.checkPermission(commonLoginData.userDTO, getFirstApprovalMenu())) {
                        try {
                            saveApproverOneData(request, commonLoginData.userDTO);
                            ApiResponse.sendSuccessResponse(response, getApproverOneRedirectURL());
                        } catch (Exception ex) {
                            ex.printStackTrace();
                            ApiResponse.sendErrorResponse(response, ex.getMessage());
                        }
                        return;
                    }
                    break;
                case "ajax_approver_two_add":
                    if (Utils.checkPermission(commonLoginData.userDTO, getSecondApprovalMenu())) {
                        try {
                            saveApproverTwoData(request, commonLoginData.userDTO);
                            ApiResponse.sendSuccessResponse(response, getApproverTwoRedirectURL());
                        } catch (Exception ex) {
                            logger.error(ex);
                            ApiResponse.sendErrorResponse(response, ex.getMessage());
                        }
                        return;
                    }
                    break;
                case "ajax_approver_three_add":
                    if (Utils.checkPermission(commonLoginData.userDTO, getThirdApprovalMenu())) {
                        try {
                            saveApproverThreeData(request, commonLoginData.userDTO);
                            ApiResponse.sendSuccessResponse(response, getApproverThreeRedirectURL());
                        } catch (Exception ex) {
                            logger.error(ex);
                            ApiResponse.sendErrorResponse(response, ex.getMessage());
                        }
                        return;
                    }
                    break;
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error(ex);
        }


        super.doPost(request, response);
    }

    public String getApproverOneRedirectURL() {
        return getServletName() + "?actionType=forApprovalOne";
    }

    public String getApproverTwoRedirectURL() {
        return getServletName() + "?actionType=forApprovalTwo";
    }

    public String getApproverThreeRedirectURL() {
        return getServletName() + "?actionType=forApprovalThree";
    }

    private void getFiscalYearData(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
        String fiscalYearIdsInString = request.getParameter("fiscalYearIds");
        List<String> fiscalYearIds = Arrays.asList(fiscalYearIdsInString.split(","));
        boolean allSelected = fiscalYearIds.contains("-100");
        List<Long> fiscalYearIdsInLong = fiscalYearIds.stream()
                .map(Long::parseLong)
                .collect(Collectors.toList());

        List<Pi_app_requestDTO> requestDTOS = Pi_app_requestDAO.getInstance().getDataByFiscalYear
                (fiscalYearIdsInLong, allSelected);

        List<Long> activeFiscalYearIds = requestDTOS.stream().map(i -> i.fiscalYearId).
                distinct().collect(Collectors.toList());
        List<DataByFiscalYearDTO> data = new ArrayList<>();

        Pi_app_request_detailsDAO childDAO = Pi_app_request_detailsDAO.getInstance();

        for (long current : activeFiscalYearIds) {
            DataByFiscalYearDTO dataDTO = new DataByFiscalYearDTO();
            List<Pi_app_requestDTO> currentReqDTOs = requestDTOS.stream().filter(i -> i.fiscalYearId == current).
                    collect(Collectors.toList());
            dataDTO.itemCount = currentReqDTOs.stream().map(i -> i.approver_three_item_count).
                    reduce(0.0, Double::sum);
            dataDTO.cost = currentReqDTOs.stream().map(i -> i.approver_three_est_cost).
                    reduce(0.0, Double::sum);
            dataDTO.fiscalYearId = current;
            dataDTO.fiscalYearName = Fiscal_yearRepository.getInstance().getText(current, Language);
            dataDTO.packageCount = (int) childDAO.getByAppIds(currentReqDTOs
                            .stream()
                            .map(i -> i.iD).
                            collect(Collectors.toList()))
                    .stream()
                    .filter(i -> childDAO.allowedValue().contains(i.isSelected))
                    .map(i -> i.packageId)
                    .distinct().count();
            data.add(dataDTO);

        }

        request.setAttribute("data", data);
        RequestDispatcher rd = request.getRequestDispatcher("pi_app_request/SearchForm.jsp");
        rd.forward(request, response);
    }

    private void getItemSpecificFiscalYearData(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        CommonLoginData commonLoginData = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get();
        long fiscalYearId = Long.parseLong(request.getParameter("fiscalYearId"));
        long officeUnitId = commonLoginData.userDTO.unitID;

        Pi_app_requestDAO appRequestDAO = Pi_app_requestDAO.getInstance();
        Pi_app_request_detailsDAO childDao = Pi_app_request_detailsDAO.getInstance();
        List<Pi_app_requestDTO> piAppRequestDTOS = appRequestDAO
                .getByFiscalYearIdAndUnitId(fiscalYearId, officeUnitId)
                .stream()
                .filter(i -> i.status == CommonApprovalStatus.SATISFIED.getValue())
                .collect(Collectors.toList());

        List<Pi_app_request_detailsDTO> detailsDTOS;
        String itemInString = Jsoup.clean(request.getParameter("itemId"), Whitelist.simpleText());
        if (itemInString.length() > 0) {
            long itemId = Long.parseLong(itemInString);
            detailsDTOS = childDao.getApprovedItemByAppIdAndItemId(piAppRequestDTOS.get(0).iD, itemId);
        } else {
            detailsDTOS = childDao.getByAppId(piAppRequestDTOS.get(0).iD)
                    .stream()
                    .filter(i -> childDao.allowedValue().contains(i.isSelected))
                    .collect(Collectors.toList());
        }

        request.setAttribute("data", detailsDTOS);
        request.setAttribute("fiscalYearId", fiscalYearId);
        request.setAttribute("officeUnitId", officeUnitId);
        RequestDispatcher rd = request.getRequestDispatcher("pi_app_report/FiscalYearDataByItem.jsp");
        rd.forward(request, response);
    }

    private boolean isValueValid(String value) {
        return value != null && !value.equals("");
    }

    private void validateFiscalYear(long fiscalYearId) throws Exception {
        List<Pi_app_requestDTO> list = (List<Pi_app_requestDTO>) Pi_app_requestDAO.getInstance()
                .getDTOsByParent("fiscal_year_id", fiscalYearId);

        // MORE THAN ONE APP IS NOT ALLOWED PER FISCAL YEAR
        if (!(list == null || list.isEmpty())) {
            UtilCharacter.throwException("এক অর্থবছরে একটার বেশি বার্ষিক ক্রয় পরিকল্পনা তৈরি করা যাবে না!",
                    "Can not create more than one Annual Purchase Plan per fiscal year!");
        }
    }
}

