package pi_app_request;

public class DataByFiscalYearDTO {
    public String fiscalYearName = "";
    public long fiscalYearId = -1;
    public int packageCount = 0;
    public double itemCount = 0;
    public double cost = 0;
}
