package vm_vehicle_driver_office_report;


import java.io.IOException;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import language.LC;
import language.LM;

import login.LoginDTO;
import permission.MenuConstants;

import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;

import util.ReportRequestHandler;
import pbReport.*;






@WebServlet("/Vm_vehicle_driver_office_report_Servlet")
public class Vm_vehicle_driver_office_report_Servlet  extends HttpServlet {
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	String[][] Criteria =
	{
		{"criteria","v1","vehicle_brand_cat","=","","int","","","any","vehicleBrandCat", LC.VM_VEHICLE_ADD_VEHICLEBRANDCAT + ""},
		{"criteria","v1","supplier_type","=","AND","int","","","any","supplierType", LC.VM_VEHICLE_DRIVER_OFFICE_REPORT_WHERE_SUPPLIERTYPE + ""},
		{"criteria","v1","vehicle_fuel_cat","=","AND","int","","","any","vehicleFuelCat", LC.VM_VEHICLE_DRIVER_OFFICE_REPORT_WHERE_VEHICLEFUELCAT + ""},
		{"criteria","v1","status","=","AND","String","","","any","status", LC.VM_VEHICLE_DRIVER_OFFICE_REPORT_WHERE_STATUS + ""},
		{"criteria","v1","purchase_date",">=","AND","long","","",Long.MIN_VALUE + "","startDate", LC.HM_START_DATE + ""},
		{"criteria","v1","purchase_date","<=","AND","long","","",Long.MAX_VALUE + "","endDate", LC.HM_END_DATE + ""},
		{"criteria","v1","isDeleted","=","AND","String","","","0","isDeleted", LC.VM_VEHICLE_DRIVER_OFFICE_REPORT_WHERE_ISDELETED + ""}
	};

	String[][] Display =
	{
		{"display","v1","vehicle_type_cat","cat",""},
		{"display","v2","office_id","office_unit",""},
		{"display","v1","reg_no","text",""},
		{"display","v1","status","vehicle_status",""},
		{"display","v1","purchase_date","date",""},
		{"display","v1","vehicle_brand_cat","cat",""},
		{"display","v1","model_no","text",""},
		{"display","v1","vehicle_color_cat","cat",""},
		{"display","v1","chasis_no","text",""},
		{"display","v1","engine_no","text",""},
		{"display","v1","manufacture_year","text",""},
		{"display","v1","number_of_seats","text",""},
		{"display","v1","vehicle_fuel_cat","cat",""},
		{"display","v1","supplier_type","vehicle_supplier_converter",""},
		{"display","v1","fitness_expiry_date","date",""},
		{"display","v1","tax_token_expiry_date","date",""},
		{"display","v3","driver_id","vehicle_driver_assignment_id_check",""}
	};

	String GroupBy = "";
	String OrderBY = "";

	ReportRequestHandler reportRequestHandler;

	public Vm_vehicle_driver_office_report_Servlet(){

	}

	private final ReportService reportService = new ReportService();

	private String sql;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{

		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String language = "english";
		if(userDTO!= null && userDTO.languageID == SessionConstants.BANGLA)
		{
			language = "bangla";
		}

		String actionType = request.getParameter("actionType");

		System.out.println("In ssservlet doget, actiontype = " + actionType);

		sql = "vm_vehicle v1 INNER JOIN vm_vehicle_office_assignment v2 ON v1.id = v2.vehicle_id INNER JOIN vm_vehicle_driver_assignment v3 ON v1.id = v3.vehicle_id";

		Display[0][4] = LM.getText(LC.VM_VEHICLE_DRIVER_OFFICE_REPORT_SELECT_VEHICLETYPECAT, loginDTO);
		Display[1][4] = LM.getText(LC.VM_VEHICLE_DRIVER_OFFICE_REPORT_SELECT_OFFICEID, loginDTO);
		Display[2][4] = LM.getText(LC.VM_VEHICLE_DRIVER_OFFICE_REPORT_SELECT_REGNO, loginDTO);
		Display[3][4] = LM.getText(LC.VM_VEHICLE_DRIVER_OFFICE_REPORT_SELECT_STATUS, loginDTO);
		Display[4][4] = LM.getText(LC.VM_VEHICLE_DRIVER_OFFICE_REPORT_SELECT_PURCHASEDATE, loginDTO);
		Display[5][4] = LM.getText(LC.VM_VEHICLE_DRIVER_OFFICE_REPORT_SELECT_VEHICLEBRANDCAT, loginDTO);
		Display[6][4] = LM.getText(LC.VM_VEHICLE_DRIVER_OFFICE_REPORT_SELECT_MODELNO, loginDTO);
		Display[7][4] = LM.getText(LC.VM_VEHICLE_DRIVER_OFFICE_REPORT_SELECT_VEHICLECOLORCAT, loginDTO);
		Display[8][4] = LM.getText(LC.VM_VEHICLE_DRIVER_OFFICE_REPORT_SELECT_CHASISNO, loginDTO);
		Display[9][4] = LM.getText(LC.VM_VEHICLE_DRIVER_OFFICE_REPORT_SELECT_ENGINENO, loginDTO);
		Display[10][4] = LM.getText(LC.VM_VEHICLE_DRIVER_OFFICE_REPORT_SELECT_MANUFACTUREYEAR, loginDTO);
		Display[11][4] = LM.getText(LC.VM_VEHICLE_DRIVER_OFFICE_REPORT_SELECT_NUMBEROFSEATS, loginDTO);
		Display[12][4] = LM.getText(LC.VM_VEHICLE_DRIVER_OFFICE_REPORT_SELECT_VEHICLEFUELCAT, loginDTO);
		Display[13][4] = LM.getText(LC.VM_VEHICLE_DRIVER_OFFICE_REPORT_SELECT_SUPPLIERTYPE, loginDTO);
		Display[14][4] = LM.getText(LC.VM_VEHICLE_DRIVER_OFFICE_REPORT_SELECT_FITNESSEXPIRYDATE, loginDTO);
		Display[15][4] = LM.getText(LC.VM_VEHICLE_DRIVER_OFFICE_REPORT_SELECT_TAXTOKENEXPIRYDATE, loginDTO);
		Display[16][4] = LM.getText(LC.VM_VEHICLE_DRIVER_OFFICE_REPORT_SELECT_DRIVERID, loginDTO);


		String reportName = LM.getText(LC.VM_VEHICLE_DRIVER_OFFICE_REPORT_OTHER_VM_VEHICLE_DRIVER_OFFICE_REPORT, loginDTO);

		reportRequestHandler = new ReportRequestHandler(null,
				Criteria, Display, GroupBy, OrderBY, sql,
				reportService);


		reportRequestHandler.handleReportGet(request, response, userDTO, "vm_vehicle_driver_office_report",
				MenuConstants.VM_VEHICLE_DRIVER_OFFICE_REPORT_DETAILS, language, reportName, "vm_vehicle_driver_office_report");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		doGet(request, response);
	}
}
