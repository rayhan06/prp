package medical_transaction;

import medical_equipment_name.Medical_equipment_nameDAO;
import medical_equipment_name.Medical_equipment_nameDTO;
import medical_inventory_lot.MedicalInventoryInDTO;
import medical_inventory_out.Medical_inventory_outDTO;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;

public class Medical_transactionDTO extends CommonDTO
{
	
	public long medicalItemCat = 0;
	public long drugInformationId = 0;
	public long medicalReagentNameId = 0;
	public long medicalEquipmentNameId = 0;
	
	public int quantity = 0;
	public double unitPrice = 0;
	public long transactionDate = 0;
    public String tableName = "";
	public long insertedByUserId = 0;
	public long insertedByOrganogramId = 0;
	public long insertionDate = 0;


	public int transactionType = MedicalInventoryInDTO.TR_RECEIVE_MEDICINE;
	public long organogramId = -1;
	public String userName = "";
	
	public long fromOrganogramId = -1;
	public String fromUserName = "";
	
	public long toOrganogramId = -1;
	public String toUserName = "";
	public long transactionTime = System.currentTimeMillis();
	public String patientName;
	
	public long prescriptionLabTestId = -1;
	public int medicalEquipmentCat = -1;
	
	public long inId = -1;
	public long outId = -1;
	
	public long prescriptionDetailsId = -1;
	public long ppmId = -1;
	

	public int medicalDeptCat = -1;
	
	public long employeeRecordId = -1;
	public long otherOfficeId = -1;
	
	public Medical_transactionDTO()
	{
		
	}
	
	public Medical_transactionDTO(Medical_inventory_outDTO medical_inventory_outDTO)
	{
		prescriptionDetailsId = medical_inventory_outDTO.prescriptionDetailsId;
		ppmId = medical_inventory_outDTO.ppmId;
		
		medicalItemCat = medical_inventory_outDTO.medicalItemCat;
		drugInformationId = medical_inventory_outDTO.drugInformationId;
		medicalReagentNameId = medical_inventory_outDTO.medicalReagentNameId;
		medicalEquipmentNameId = medical_inventory_outDTO.medicalEquipmentNameId;
		
		quantity = medical_inventory_outDTO.stockOutQuantity;
		unitPrice = medical_inventory_outDTO.unitPrice;
		transactionDate = medical_inventory_outDTO.transactionDate;
		tableName = "medical_inventory_out";
		
		insertedByUserId = medical_inventory_outDTO.insertedByUserId;
		insertedByOrganogramId = medical_inventory_outDTO.insertedByOrganogramId;
		insertionDate = medical_inventory_outDTO.insertionDate;
		
		transactionType = medical_inventory_outDTO.transactionType;
		organogramId = medical_inventory_outDTO.organogramId;
		userName = medical_inventory_outDTO.userName;
		
		fromOrganogramId = -1;
		fromUserName = "";
		
		toOrganogramId = medical_inventory_outDTO.organogramId;
		toUserName = medical_inventory_outDTO.userName;
		
		transactionTime = medical_inventory_outDTO.transactionTime;
		patientName = medical_inventory_outDTO.patientName;
		
		prescriptionLabTestId = medical_inventory_outDTO.prescriptionLabTestId;
		medicalEquipmentCat = medical_inventory_outDTO.medicalEquipmentCat;
		
		inId = medical_inventory_outDTO.medicalInventoryInId;
		outId = medical_inventory_outDTO.iD;
		
		medicalDeptCat = medical_inventory_outDTO.medicalDeptCat;
		
		employeeRecordId = medical_inventory_outDTO.employeeRecordId;
		otherOfficeId = medical_inventory_outDTO.otherOfficeId;
	}
	
	public Medical_transactionDTO(MedicalInventoryInDTO medicalInventoryInDTO)
	{
		medicalItemCat = medicalInventoryInDTO.medicalItemCat;
		drugInformationId = medicalInventoryInDTO.drugInformationType;
		medicalReagentNameId = medicalInventoryInDTO.medicalReagentNameType;
		medicalEquipmentNameId = medicalInventoryInDTO.medicalEquipmentNameType;
		
		quantity = -medicalInventoryInDTO.stockInQuantity;
		unitPrice = medicalInventoryInDTO.unitPrice;
		transactionDate = medicalInventoryInDTO.transactionDate;
		tableName = "medical_inventory_in";
		
		insertedByUserId = medicalInventoryInDTO.insertedByUserId;
		insertedByOrganogramId = medicalInventoryInDTO.insertedByOrganogramId;
		insertionDate = medicalInventoryInDTO.insertionDate;
		
		transactionType = medicalInventoryInDTO.transactionType;
		organogramId = medicalInventoryInDTO.organogramId;
		userName = medicalInventoryInDTO.userName;
		
		fromOrganogramId = medicalInventoryInDTO.organogramId;
		fromUserName = medicalInventoryInDTO.userName;
		
		UserDTO inserterDTO = UserRepository.getUserDTOByUserID(insertedByUserId);
		if(inserterDTO != null)
		{
			employeeRecordId = inserterDTO.employee_record_id;
		}
		
		
		
		
		toOrganogramId = -1;
		toUserName = "";
		
		transactionTime = medicalInventoryInDTO.transactionTime;
		patientName = "";
		
		prescriptionLabTestId = -1;
		medicalEquipmentCat = -1;
		
		if(medicalItemCat == SessionConstants.MEDICAL_ITEM_EQUIPMENT)
		{
			Medical_equipment_nameDAO medical_equipment_nameDAO = new Medical_equipment_nameDAO();
			Medical_equipment_nameDTO medical_equipment_nameDTO = medical_equipment_nameDAO.getDTOByID(medicalEquipmentNameId);
			if(medical_equipment_nameDTO != null)
			{
				unitPrice = medical_equipment_nameDTO.unitPrice;
				this.medicalEquipmentCat = medical_equipment_nameDTO.medicalEquipmentCat;
			}
		}
		
		inId = medicalInventoryInDTO.iD;
		outId = -1;
		
		medicalDeptCat = medicalInventoryInDTO.medicalDeptCat;
		
		employeeRecordId = medicalInventoryInDTO.employeeRecordId;
		otherOfficeId = medicalInventoryInDTO.otherOfficeId;
	}

}
