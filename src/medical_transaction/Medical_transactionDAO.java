package medical_transaction;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Hashtable;
import java.util.List;

import appointment.KeyCountDTO;
import appointment.YearMonthCount;
import common.ConnectionAndStatementUtil;
import medical_inventory_lot.MedicalInventoryInDTO;
import prescription_details.Prescription_detailsDTO;
import user.UserDTO;
import util.CommonDTO;
import util.NavigationService4;
import util.TimeConverter;

public class Medical_transactionDAO extends NavigationService4
{

	public Medical_transactionDAO(String tableName) {
		super(tableName);
		columnNames = new String[] 
		{
			"ID",
			"medical_item_cat",
			"drug_information_id",
			"medical_reagent_name_id",
			"medical_equipment_name_id",
			
			"quantity",
			"unit_price",
			"transaction_date",
			
			"inserted_by_user_id",
			"inserted_by_organogram_id",
			"insertion_date",
			
			"medical_inventory_in_id",
			"medical_inventory_out_id",
			
			"transaction_type",
			"user_name",
			"organogram_id",
			"transaction_time",
			"patient_name",
			
			"from_user_name",
			"from_organogram_id",
			
			"to_user_name",
			"to_organogram_id",
			
			
			"medical_dept_cat",
			"prescription_lab_test_id",
			"medical_equipment_cat",
			
			"table_name",
			
			"prescription_details_id",
			"ppm_id",
			
			"employee_record_id",
			"other_office_id",
			
			"isDeleted",
			"lastModificationTime"
		};
	}
	
	public Medical_transactionDAO()
	{
		this("medical_transaction");	
	}
	
	public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException
	{
		Medical_transactionDTO medical_transactionDTO = (Medical_transactionDTO)commonDTO;
		int index = 1;
		long lastModificationTime = System.currentTimeMillis();	

		if(isInsert)
		{
			ps.setObject(index++,medical_transactionDTO.iD);
		}
		ps.setObject(index++,medical_transactionDTO.medicalItemCat);
		ps.setObject(index++,medical_transactionDTO.drugInformationId);
		ps.setObject(index++,medical_transactionDTO.medicalReagentNameId);
		ps.setObject(index++,medical_transactionDTO.medicalEquipmentNameId);
		
		ps.setObject(index++,medical_transactionDTO.quantity);
		ps.setObject(index++,medical_transactionDTO.unitPrice);
		ps.setObject(index++,medical_transactionDTO.transactionDate);
		

		ps.setObject(index++,medical_transactionDTO.insertedByUserId);
		ps.setObject(index++,medical_transactionDTO.insertedByOrganogramId);
		ps.setObject(index++,medical_transactionDTO.insertionDate);

		ps.setObject(index++,medical_transactionDTO.inId);
		ps.setObject(index++,medical_transactionDTO.outId);

		ps.setObject(index++,medical_transactionDTO.transactionType);
		ps.setObject(index++,medical_transactionDTO.userName);
		ps.setObject(index++,medical_transactionDTO.organogramId);
		ps.setObject(index++,medical_transactionDTO.transactionTime);		
		ps.setObject(index++,medical_transactionDTO.patientName);
		
		ps.setObject(index++,medical_transactionDTO.fromUserName);
		ps.setObject(index++,medical_transactionDTO.fromOrganogramId);
		
		ps.setObject(index++,medical_transactionDTO.toUserName);
		ps.setObject(index++,medical_transactionDTO.toOrganogramId);
	
		ps.setObject(index++,medical_transactionDTO.medicalDeptCat);
		ps.setObject(index++,medical_transactionDTO.prescriptionLabTestId);
		ps.setObject(index++,medical_transactionDTO.medicalEquipmentCat);
		
		ps.setObject(index++,medical_transactionDTO.tableName);
		
		ps.setObject(index++,medical_transactionDTO.prescriptionDetailsId);
		ps.setObject(index++,medical_transactionDTO.ppmId);
		ps.setObject(index++,medical_transactionDTO.employeeRecordId);
		ps.setObject(index++,medical_transactionDTO.otherOfficeId);
		
		if(isInsert)
		{
			ps.setObject(index++, 0);
			ps.setObject(index++, lastModificationTime);
		}
	}
	
	public Medical_transactionDTO build(ResultSet rs)
	{
		try
		{
			Medical_transactionDTO medical_transactionDTO = new Medical_transactionDTO();
			medical_transactionDTO.iD = rs.getLong("ID");
			
			medical_transactionDTO.medicalItemCat = rs.getLong("medical_item_cat");
			medical_transactionDTO.drugInformationId = rs.getLong("drug_information_id");
			medical_transactionDTO.medicalReagentNameId = rs.getLong("medical_reagent_name_id");
			medical_transactionDTO.medicalEquipmentNameId = rs.getLong("medical_equipment_name_id");
			
			medical_transactionDTO.quantity = rs.getInt("quantity");
			medical_transactionDTO.unitPrice = rs.getDouble("unit_price");
			medical_transactionDTO.transactionDate = rs.getLong("transaction_date");
			
			medical_transactionDTO.insertedByUserId = rs.getLong("inserted_by_user_id");
			medical_transactionDTO.insertedByOrganogramId = rs.getLong("inserted_by_organogram_id");
			medical_transactionDTO.insertionDate = rs.getLong("insertion_date");
			
			medical_transactionDTO.inId = rs.getLong("medical_inventory_in_id");
			medical_transactionDTO.outId = rs.getLong("medical_inventory_out_id");

			medical_transactionDTO.transactionType = rs.getInt("transaction_type");
			medical_transactionDTO.userName = rs.getString("user_name");
			medical_transactionDTO.organogramId = rs.getLong("organogram_id");
			medical_transactionDTO.transactionTime = rs.getLong("transaction_time");
			medical_transactionDTO.patientName = rs.getString("patient_name");
			
			medical_transactionDTO.fromUserName = rs.getString("from_user_name");
			medical_transactionDTO.fromOrganogramId = rs.getLong("from_organogram_id");
			
			medical_transactionDTO.toUserName = rs.getString("to_user_name");
			medical_transactionDTO.toOrganogramId = rs.getLong("to_organogram_id");
			
			
			medical_transactionDTO.medicalDeptCat = rs.getInt("medical_dept_cat");
			medical_transactionDTO.prescriptionLabTestId = rs.getLong("prescription_lab_test_id");
			medical_transactionDTO.medicalEquipmentCat = rs.getInt("medical_equipment_cat");
			
			medical_transactionDTO.tableName = rs.getString("table_name");
			
			medical_transactionDTO.prescriptionDetailsId = rs.getLong("prescription_details_id");
			medical_transactionDTO.ppmId = rs.getLong("ppm_id");
			
			medical_transactionDTO.employeeRecordId = rs.getLong("employee_record_id");
			medical_transactionDTO.otherOfficeId = rs.getLong("other_office_id");
			
			medical_transactionDTO.isDeleted = rs.getInt("isDeleted");
			medical_transactionDTO.lastModificationTime = rs.getLong("lastModificationTime");
			return medical_transactionDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
	
	public KeyCountDTO getPatientsMedicines(String userName)
	{
		String sql = "SELECT count(distinct user_name), sum(quantity) FROM medical_transaction where inserted_by_user_id = \r\n" + 
				"(select id from users where username = '" + userName + "' and isDeleted = 0 limit 1) "
				+ "and transaction_type in (" + MedicalInventoryInDTO.TR_RECEIVE_MEDICINE + ", " + MedicalInventoryInDTO.TR_RETURN + ")";
		return ConnectionAndStatementUtil.getT(sql,this::getPatientsMedicines);			
	}
	
	public KeyCountDTO getPatientsMedicines()
	{
		String sql = "SELECT count(distinct prescription_details_id), sum(quantity) FROM medical_transaction where transaction_date >= " + TimeConverter.getToday() + " \r\n" 

				+ "and transaction_type in (" + MedicalInventoryInDTO.TR_RECEIVE_MEDICINE + ", " + MedicalInventoryInDTO.TR_RETURN + ")";
		return ConnectionAndStatementUtil.getT(sql,this::getPatientsMedicines);			
	}
	
	public KeyCountDTO getDeliveredPrescriptions()
	{
		String sql = "SELECT count(distinct prescription_details_id), sum(quantity) FROM medical_transaction where transaction_date >= " + TimeConverter.getToday() + " \r\n" 

				+ "and transaction_type = (" + MedicalInventoryInDTO.TR_RECEIVE_MEDICINE + ") and prescription_details_id != -1";
		return ConnectionAndStatementUtil.getT(sql,this::getPatientsMedicines);			
	}
	
	public KeyCountDTO getDeliveredPrescriptions(long startDate, long endDate)
	{
		String sql = "SELECT count(distinct prescription_details_id), sum(quantity), sum(quantity * unit_price) FROM medical_transaction "
				+ "where transaction_date >= " + startDate + " \r\n" 
				+ "and transaction_date <= " + endDate + " \r\n" 

				+ "and transaction_type = (" + MedicalInventoryInDTO.TR_RECEIVE_MEDICINE + ") and prescription_details_id != -1";
		return ConnectionAndStatementUtil.getT(sql,this::getPatientsMedicinesWithCost);			
	}
	public KeyCountDTO getPatientsMedicinesWithCost(ResultSet rs)
	{
		try
		{
			KeyCountDTO keyCountDTO = new KeyCountDTO();
			keyCountDTO.count = rs.getInt("count(distinct prescription_details_id)");
			keyCountDTO.count2 = rs.getInt("sum(quantity)");
			keyCountDTO.cost = rs.getDouble("sum(quantity * unit_price)");
			return keyCountDTO;
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
			return null;
		}
	}
	
	public KeyCountDTO getDeliveredMedicines()
	{
		String sql = "SELECT count(distinct prescription_details_id), sum(quantity) FROM medical_transaction where transaction_date >= " + TimeConverter.getToday() + " \r\n" 

				+ "and transaction_type in (" + MedicalInventoryInDTO.TR_RECEIVE_MEDICINE + ", " + MedicalInventoryInDTO.TR_RETURN + ")";
		return ConnectionAndStatementUtil.getT(sql,this::getPatientsMedicines);					
	}
	
	
	
	public List<KeyCountDTO> getLast7DayCountCost()
    {
		String sql = "SELECT  transaction_date, sum(quantity), sum(quantity * unit_price), " +
				"	count(distinct prescription_details_id) as presCount" +
				" FROM medical_transaction"
				+ " where transaction_date >=" + TimeConverter.getNthDay(-7) + " and isDeleted = 0 "
				+ "and transaction_type in (" + MedicalInventoryInDTO.TR_RECEIVE_MEDICINE + ", " + MedicalInventoryInDTO.TR_RETURN + ")";
		sql+= " group by transaction_date order by transaction_date asc";
		
		return ConnectionAndStatementUtil.getListOfT(sql,this::getCountCost);	
    }
	
	public KeyCountDTO getCostCount (long startDate, long endDate)
	{
		String sql = "SELECT sum(quantity), sum(quantity * unit_price) FROM medical_transaction where transaction_date >= " + startDate 
				+ " and transaction_date < " + endDate + " and isDeleted = 0 "
				+ "and transaction_type in (" + MedicalInventoryInDTO.TR_RECEIVE_MEDICINE + ", " + MedicalInventoryInDTO.TR_RETURN + ")";
        return ConnectionAndStatementUtil.getT(sql,this::getCountCostNoKey);
	}
	
	public KeyCountDTO getCountCost(ResultSet rs)
	{
		try
		{
			KeyCountDTO keyCountDTO = new KeyCountDTO();
			keyCountDTO.key = rs.getLong("transaction_date");
			keyCountDTO.count = rs.getInt("sum(quantity)");
			keyCountDTO.count2 = rs.getInt("presCount");
			keyCountDTO.cost = rs.getDouble("sum(quantity * unit_price)");
			return keyCountDTO;
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
			return null;
		}
	}
	
	
	public YearMonthCount getYmCountCost(ResultSet rs)
	{
		try
		{
			YearMonthCount ymCount = new YearMonthCount();
			String ym = rs.getString("ym");
			ymCount.year = Integer.parseInt(ym.split("-")[0]);
			ymCount.month = Integer.parseInt(ym.split("-")[1]);
			ymCount.count = rs.getInt("sum(quantity)");
			ymCount.count2 = rs.getInt("presCount");
			ymCount.cost = rs.getDouble("sum(quantity * unit_price)");
			
			//System.out.println("year = " + ymCount.year + " month = " + ymCount.month + " count = " + ymCount.count);

			return ymCount;
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
			return null;
		}
	}
	
	public List<YearMonthCount> getLast6MonthCostCount ()
    {
		String sql = "SELECT \r\n" + 
				"    DATE_FORMAT(FROM_UNIXTIME(`transaction_date` / 1000),\r\n" + 
				"            '%Y-%m') AS ym,\r\n" + 
				"    sum(quantity), sum(quantity * unit_price), \r\n" + 
				"	count(distinct prescription_details_id) as presCount " +
				"FROM\r\n" + 
				"    medical_transaction\r\n" + 
				"WHERE\r\n" + 
				"    transaction_date >= " + TimeConverter.get1stDayOfNthtMonth(-6) + " AND isDeleted = 0 \r\n" + 
				"and transaction_type in (" + MedicalInventoryInDTO.TR_RECEIVE_MEDICINE + ", " + MedicalInventoryInDTO.TR_RETURN + ") " +
				"GROUP BY ym\r\n" + 
				"ORDER BY ym ASC;";
		
		//System.out.println(sql);
		return ConnectionAndStatementUtil.getListOfT(sql,this::getYmCountCost);	
    }
	
	public KeyCountDTO getCountCostNoKey(ResultSet rs)
	{
		try
		{
			KeyCountDTO keyCountDTO = new KeyCountDTO();
			keyCountDTO.count = rs.getInt("sum(quantity)");
			keyCountDTO.cost = rs.getDouble("sum(quantity * unit_price)");
			return keyCountDTO;
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
			return null;
		}
	}
	
	public KeyCountDTO getQuantityCost(long startDate, long endDate)
	{
		String sql = "SELECT sum(quantity), sum(quantity * unit_price) FROM medical_transaction where transaction_date >= " + startDate + " and transaction_date <= " + endDate
				+ " and transaction_type in (" + MedicalInventoryInDTO.TR_RECEIVE_MEDICINE + ", " + MedicalInventoryInDTO.TR_RETURN + ") and medical_item_cat = 0";
		return ConnectionAndStatementUtil.getT(sql,this::getQuantityCost);			
	}
	
	public KeyCountDTO getQuantityCost(ResultSet rs)
	{
		try
		{
			KeyCountDTO keyCountDTO = new KeyCountDTO();
			keyCountDTO.count = rs.getInt("sum(quantity)");
			keyCountDTO.cost = rs.getDouble("sum(quantity * unit_price)");
			return keyCountDTO;
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
			return null;
		}
	}
	public KeyCountDTO getPatientsMedicines(ResultSet rs)
	{
		try
		{
			KeyCountDTO keyCountDTO = new KeyCountDTO();
			keyCountDTO.count = rs.getInt("count(distinct prescription_details_id)");
			keyCountDTO.count2 = rs.getInt("sum(quantity)");
			return keyCountDTO;
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
			return null;
		}
	}

}
