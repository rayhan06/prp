package fund_management_question;

import common.NameDao;

public class FundManagementQuestionDAO extends NameDao {
    private FundManagementQuestionDAO() {
        super("fund_management_question");
    }

    private static class LazyLoader{
        static final FundManagementQuestionDAO INSTANCE = new FundManagementQuestionDAO();
    }

    public static FundManagementQuestionDAO getInstance(){
        return LazyLoader.INSTANCE;
    }
}
