package fund_management_question;

import common.NameRepository;

public class FundManagementQuestionRepository extends NameRepository {

    private FundManagementQuestionRepository() {
        super(FundManagementQuestionDAO.getInstance(), FundManagementQuestionRepository.class);
    }

    private static class LazyLoader {
        static FundManagementQuestionRepository INSTANCE = new FundManagementQuestionRepository();
    }

    public static FundManagementQuestionRepository getInstance() {
        return LazyLoader.INSTANCE;
    }
}
