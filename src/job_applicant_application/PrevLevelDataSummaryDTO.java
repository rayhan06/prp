package job_applicant_application;

import java.util.List;

public class PrevLevelDataSummaryDTO
{
    public List<PrevLevelDataDTO> data;

    @Override
	public String toString() {
            return "PrevLevelDataSummaryDTO[" +
            " data = " + data +
            "]";
    }

}