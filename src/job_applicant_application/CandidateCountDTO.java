package job_applicant_application;

public class CandidateCountDTO {
    public int applied = 0;
    public int accepted = 0;
    public int declined = 0;
    public int quotaId = 0;

    @Override
    public String toString() {
        return "CandidateCountDTO[" +
                " applied = " + applied +
                " accepted = " + accepted +
                " declined = " + declined +
                " quotaId = " + quotaId +
                "]";
    }
}
