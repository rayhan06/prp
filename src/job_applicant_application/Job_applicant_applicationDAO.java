package job_applicant_application;

import common.ConnectionAndStatementUtil;
import common.NameDTO;
import education_level.Education_levelRepository;
import geolocation.GeoDistrictDTO;
import geolocation.GeoDistrictRepository;
import geolocation.GeoDivisionDTO;
import geolocation.GeoDivisionRepository;
import javafx.util.Pair;
import language.LC;
import language.LM;
import login.LoginDTO;
import org.apache.log4j.Logger;
import parliament_job_applicant.Parliament_job_applicantDTO;
import parliament_job_applicant.Parliament_job_applicantRepository;
import pb.CatRepository;
import pb.CategoryLanguageModel;
import recruitment_dashboard.PositionWiseItemDTO;
import recruitment_dashboard.PositionWiseStatusDTO;
import recruitment_job_specific_exam_type.JobSpecificExamTypeRepository;
import recruitment_job_specific_exam_type.RecruitmentJobSpecificExamTypeDTO;
import recruitment_seat_plan.RecruitmentSeatPlanChildDTO;
import repository.RepositoryManager;
import user.UserDTO;
import util.CommonDTO;
import util.NavigationService4;
import util.UtilCharacter;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

public class Job_applicant_applicationDAO extends NavigationService4 {

    Logger logger = Logger.getLogger(getClass());

    public static HashMap<Integer, String> viewMap = new HashMap() {{
        put("VIEWED", "দেখেছেন");
        put("NOT VIEWED", "দেখেন নি");
    }};
    List<String> viewList = Arrays.asList("VIEWED", "NOT VIEWED");

    public static HashMap<Integer, String> approveMap = new HashMap() {{
        put("REJECTED", "বাতিলকৃত");
        put("SHORT LISTED", "সংক্ষিপ্ত তালিকাভুক্ত");
        put("RUNNING", "চলমান");
    }};
    List<String> approveList = Arrays.asList("REJECTED", "SHORT LISTED", "RUNNING");

    public static Pair<String, String> getDecision(int isSelected, boolean isRejected) {
        Pair<String, String> pair = new Pair<>("", "");
        if (isSelected == 1) {
            pair = new Pair<>("SHORT LISTED", "সংক্ষিপ্ত তালিকাভুক্ত");
        } else if (isSelected == 2) {
            pair = new Pair<>("FINAL SELECTED", "চূড়ান্ত নির্বাচিত");
        } else if (isRejected) {
            pair = new Pair<>("REJECTED", "বাতিলকৃত");
        } else if (isSelected == 0 && !isRejected) {
            pair = new Pair<>("RUNNING", "চলমান");
        }

        return pair;
    }

    public static Pair<String, String> getPresent(int isPresent) {
        if (isPresent == 1) {
            return new Pair<>("Present", "উপস্থিত");
        } else if (isPresent == 2) {
            return new Pair<>("Absent", "অনুপস্থিত");
        } else {
            return new Pair<>("N/A", "প্রযোজ্য নহে");
        }
    }

    public static Pair<String, String> getMarks(double marks, double passMarks) {
        if (marks > -1) {
            String passStatus = "";
            if (passMarks > 0.0) {
                if (marks >= passMarks) {
                    passStatus = " (P) ";
                } else {
                    passStatus = " (F) ";
                }
            }
            String marksInString = Double.toString(marks);
            return new Pair<>(marksInString + passStatus, new UtilCharacter().convertNumberEnToBn(marksInString) + passStatus);
        } else {
            return new Pair<>("N/A", "প্রযোজ্য নহে");
        }
    }


    public Job_applicant_applicationDAO(String tableName) {
        super(tableName);
        joinSQL = "";
        commonMaps = new Job_applicant_applicationMAPS(tableName);
        columnNames = new String[]
                {
                        "ID",
                        "job_applicant_id",
                        "job_id",
                        "acceptance_status",
                        "insertion_date",
                        "inserted_by_user_id",
                        "modified_by",
                        "isDeleted",
                        "lastModificationTime",
                        "roll_number",
                        "quota_id",
                        "tracking_number",
                        "level",
                        "is_viewed",
                        "is_rejected",
                        "is_selected",
                        "decision_remarks",
                        "summary",
                        "is_present",
                        "marks",
                        "prev_level_data",
                        "applicant_name_bn",
                        "applicant_name_en",
                        "present_address_name_bn",
                        "present_address_name_en",
                        "permanent_address_name_bn",
                        "permanent_address_name_en",
                        "district_name_bn",
                        "district_name_en",
                        "applicant_dob_en",
                        "applicant_dob_bn",
                        "applicant_age_en",
                        "applicant_age_bn",
                        "applicant_nid",
                        "quota_en",
                        "quota_bn",
                        "annual_en",
                        "annual_bn",
                        "priviledge_en",
                        "priviledge_bn",
                        "edu_info_en",
                        "edu_info_bn",
                        "job_experience_en",
                        "job_experience_bn",
                        "photo_file_id",
                        "father_name",
                        "mother_name",
                        "highest_edu_level",
                        "district",
                        "division",
                        "age_in_years",
                        "qualification_in_years",
                        "university_ids",
                        "seat_plan_id",
                        "seat_plan_child_id",
                        "recruitment_exam_venue_id",
                        "recruitment_exam_venue_item_id" ,
                        "is_admit_downloaded"
                };
    }


    public Job_applicant_applicationDAO() {
        this("job_applicant_application");
    }

    public void setSearchColumn(Job_applicant_applicationDTO job_applicant_applicationDTO) {
        job_applicant_applicationDTO.searchColumn = "";
        job_applicant_applicationDTO.searchColumn += job_applicant_applicationDTO.acceptanceStatus + " ";
        job_applicant_applicationDTO.searchColumn += job_applicant_applicationDTO.modifiedBy + " ";
        job_applicant_applicationDTO.searchColumn += job_applicant_applicationDTO.trackingNumber + " ";

    }

    public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException {
        Job_applicant_applicationDTO job_applicant_applicationDTO = (Job_applicant_applicationDTO) commonDTO;

        int index = 1;
        long lastModificationTime = System.currentTimeMillis();
        setSearchColumn(job_applicant_applicationDTO);
        if (isInsert) {
            ps.setObject(index++, job_applicant_applicationDTO.iD);
        }
        ps.setObject(index++, job_applicant_applicationDTO.jobApplicantId);
        ps.setObject(index++, job_applicant_applicationDTO.jobId);
        ps.setObject(index++, job_applicant_applicationDTO.acceptanceStatus);
        ps.setObject(index++, job_applicant_applicationDTO.insertionDate);
        ps.setObject(index++, job_applicant_applicationDTO.insertedByUserId);
        ps.setObject(index++, job_applicant_applicationDTO.modifiedBy);
        if (isInsert) {
            ps.setObject(index++, 0);
            ps.setObject(index++, lastModificationTime);
        }

        ps.setObject(index++, job_applicant_applicationDTO.rollNumber);
        ps.setObject(index++, job_applicant_applicationDTO.quota_id);
        ps.setObject(index++, job_applicant_applicationDTO.trackingNumber);
        ps.setObject(index++, job_applicant_applicationDTO.level);
        ps.setObject(index++, job_applicant_applicationDTO.isViewed);
        ps.setObject(index++, job_applicant_applicationDTO.isRejected);
        ps.setObject(index++, job_applicant_applicationDTO.isSelected);
        ps.setObject(index++, job_applicant_applicationDTO.decision_remarks);
        ps.setObject(index++, job_applicant_applicationDTO.summary);

        ps.setObject(index++, job_applicant_applicationDTO.isPresent);
        ps.setObject(index++, job_applicant_applicationDTO.marks);
        ps.setObject(index++, job_applicant_applicationDTO.prev_level_data);


        ps.setObject(index++, job_applicant_applicationDTO.applicant_name_bn);
        ps.setObject(index++, job_applicant_applicationDTO.applicant_name_en);
        ps.setObject(index++, job_applicant_applicationDTO.present_address_name_bn);
        ps.setObject(index++, job_applicant_applicationDTO.present_address_name_en);
        ps.setObject(index++, job_applicant_applicationDTO.permanent_address_name_bn);
        ps.setObject(index++, job_applicant_applicationDTO.permanent_address_name_en);
        ps.setObject(index++, job_applicant_applicationDTO.district_name_bn);
        ps.setObject(index++, job_applicant_applicationDTO.district_name_en);
        ps.setObject(index++, job_applicant_applicationDTO.applicant_dob_en);
        ps.setObject(index++, job_applicant_applicationDTO.applicant_dob_bn);
        ps.setObject(index++, job_applicant_applicationDTO.applicant_age_en);
        ps.setObject(index++, job_applicant_applicationDTO.applicant_age_bn);
        ps.setObject(index++, job_applicant_applicationDTO.applicant_nid);
        ps.setObject(index++, job_applicant_applicationDTO.quota_en);
        ps.setObject(index++, job_applicant_applicationDTO.quota_bn);
        ps.setObject(index++, job_applicant_applicationDTO.annual_en);
        ps.setObject(index++, job_applicant_applicationDTO.annual_bn);
        ps.setObject(index++, job_applicant_applicationDTO.priviledge_en);
        ps.setObject(index++, job_applicant_applicationDTO.priviledge_bn);
        ps.setObject(index++, job_applicant_applicationDTO.edu_info_en);
        ps.setObject(index++, job_applicant_applicationDTO.edu_info_bn);
        ps.setObject(index++, job_applicant_applicationDTO.job_experience_en);
        ps.setObject(index++, job_applicant_applicationDTO.job_experience_bn);
        ps.setObject(index++, job_applicant_applicationDTO.photo_file_id);

        ps.setObject(index++, job_applicant_applicationDTO.father_name);
        ps.setObject(index++, job_applicant_applicationDTO.mother_name);
        ps.setObject(index++, job_applicant_applicationDTO.highest_edu_level);

        ps.setObject(index++, job_applicant_applicationDTO.district);
        ps.setObject(index++, job_applicant_applicationDTO.division);

        ps.setObject(index++, job_applicant_applicationDTO.age_in_years);
        ps.setObject(index++, job_applicant_applicationDTO.qualification_in_years);
        ps.setObject(index++, job_applicant_applicationDTO.university_ids);

        ps.setObject(index++, job_applicant_applicationDTO.seatPlanId);
        ps.setObject(index++, job_applicant_applicationDTO.seatPlanChildId);
        ps.setObject(index++, job_applicant_applicationDTO.recruitmentExamVenueId);
        ps.setObject(index++, job_applicant_applicationDTO.recruitmentExamVenueItemId);
        ps.setObject(index++, job_applicant_applicationDTO.isAdmitCardDownloaded);


    }

    public Job_applicant_applicationDTO buildForCount(ResultSet rs) {
        try {
            Job_applicant_applicationDTO jobApplicantApplicationDTO = new Job_applicant_applicationDTO();
            jobApplicantApplicationDTO.count = rs.getInt("count");
            return jobApplicantApplicationDTO;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    public Job_applicant_applicationDTO build(ResultSet rs) {
        try {
            Job_applicant_applicationDTO job_applicant_applicationDTO = new Job_applicant_applicationDTO();
            job_applicant_applicationDTO.iD = rs.getLong("ID");
            job_applicant_applicationDTO.jobApplicantId = rs.getLong("job_applicant_id");
            job_applicant_applicationDTO.jobId = rs.getLong("job_id");
            job_applicant_applicationDTO.acceptanceStatus = rs.getLong("acceptance_status");
            job_applicant_applicationDTO.insertionDate = rs.getLong("insertion_date");
            job_applicant_applicationDTO.insertedByUserId = rs.getLong("inserted_by_user_id");
            job_applicant_applicationDTO.modifiedBy = rs.getString("modified_by");
            job_applicant_applicationDTO.isDeleted = rs.getInt("isDeleted");
            job_applicant_applicationDTO.quota_id = rs.getInt("quota_id");
            job_applicant_applicationDTO.lastModificationTime = rs.getLong("lastModificationTime");
            job_applicant_applicationDTO.rollNumber = rs.getString("roll_number");
            job_applicant_applicationDTO.trackingNumber = rs.getString("tracking_number");
            job_applicant_applicationDTO.level = rs.getInt("level");
            job_applicant_applicationDTO.isViewed = rs.getBoolean("is_viewed");
            job_applicant_applicationDTO.isSelected = rs.getInt("is_selected");
            job_applicant_applicationDTO.isRejected = rs.getBoolean("is_rejected");
            job_applicant_applicationDTO.summary = rs.getString("summary");
            job_applicant_applicationDTO.decision_remarks = rs.getString("decision_remarks");

            job_applicant_applicationDTO.isPresent = rs.getInt("is_present");
            job_applicant_applicationDTO.marks = rs.getDouble("marks");
            job_applicant_applicationDTO.prev_level_data = rs.getString("prev_level_data");


            job_applicant_applicationDTO.applicant_name_bn = rs.getString("applicant_name_bn");
            job_applicant_applicationDTO.applicant_name_en = rs.getString("applicant_name_en");
            job_applicant_applicationDTO.present_address_name_bn = rs.getString("present_address_name_bn");
            job_applicant_applicationDTO.present_address_name_en = rs.getString("present_address_name_en");
            job_applicant_applicationDTO.permanent_address_name_bn = rs.getString("permanent_address_name_bn");
            job_applicant_applicationDTO.permanent_address_name_en = rs.getString("permanent_address_name_en");
            job_applicant_applicationDTO.district_name_bn = rs.getString("district_name_bn");
            job_applicant_applicationDTO.district_name_en = rs.getString("district_name_en");
            job_applicant_applicationDTO.applicant_dob_en = rs.getString("applicant_dob_en");
            job_applicant_applicationDTO.applicant_dob_bn = rs.getString("applicant_dob_bn");
            job_applicant_applicationDTO.applicant_age_en = rs.getString("applicant_age_en");
            job_applicant_applicationDTO.applicant_age_bn = rs.getString("applicant_age_bn");
            job_applicant_applicationDTO.applicant_nid = rs.getString("applicant_nid");
            job_applicant_applicationDTO.quota_en = rs.getString("quota_en");
            job_applicant_applicationDTO.quota_bn = rs.getString("quota_bn");
            job_applicant_applicationDTO.annual_en = rs.getString("annual_en");
            job_applicant_applicationDTO.annual_bn = rs.getString("annual_bn");
            job_applicant_applicationDTO.priviledge_en = rs.getString("priviledge_en");
            job_applicant_applicationDTO.priviledge_bn = rs.getString("priviledge_bn");
            job_applicant_applicationDTO.edu_info_en = rs.getString("edu_info_en");
            job_applicant_applicationDTO.edu_info_bn = rs.getString("edu_info_bn");
            job_applicant_applicationDTO.job_experience_en = rs.getString("job_experience_en");
            job_applicant_applicationDTO.job_experience_bn = rs.getString("job_experience_bn");
            job_applicant_applicationDTO.photo_file_id = rs.getLong("photo_file_id");

            job_applicant_applicationDTO.father_name = rs.getString("father_name");
            job_applicant_applicationDTO.mother_name = rs.getString("mother_name");
            job_applicant_applicationDTO.highest_edu_level = rs.getLong("highest_edu_level");

            job_applicant_applicationDTO.district = rs.getString("district");
            job_applicant_applicationDTO.division = rs.getString("division");

            job_applicant_applicationDTO.university_ids = rs.getString("university_ids");

            job_applicant_applicationDTO.seatPlanId = rs.getLong("seat_plan_id");
            job_applicant_applicationDTO.seatPlanChildId = rs.getLong("seat_plan_child_id");
            job_applicant_applicationDTO.recruitmentExamVenueId = rs.getLong("recruitment_exam_venue_id");
            job_applicant_applicationDTO.recruitmentExamVenueItemId = rs.getLong("recruitment_exam_venue_item_id");
            job_applicant_applicationDTO.isAdmitCardDownloaded = rs.getInt("is_admit_downloaded");

            job_applicant_applicationDTO.age_in_years = rs.getInt("age_in_years");
            job_applicant_applicationDTO.qualification_in_years = rs.getInt("qualification_in_years");
            return job_applicant_applicationDTO;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }


    public Job_applicant_applicationDTO buildWithoutSummary(ResultSet rs) {
        try {
            Job_applicant_applicationDTO job_applicant_applicationDTO = new Job_applicant_applicationDTO();
            job_applicant_applicationDTO.iD = rs.getLong("ID");
            job_applicant_applicationDTO.jobApplicantId = rs.getLong("job_applicant_id");
            job_applicant_applicationDTO.jobId = rs.getLong("job_id");
            job_applicant_applicationDTO.acceptanceStatus = rs.getLong("acceptance_status");
            job_applicant_applicationDTO.insertionDate = rs.getLong("insertion_date");
            job_applicant_applicationDTO.insertedByUserId = rs.getLong("inserted_by_user_id");
            job_applicant_applicationDTO.modifiedBy = rs.getString("modified_by");
            job_applicant_applicationDTO.isDeleted = rs.getInt("isDeleted");
            job_applicant_applicationDTO.quota_id = rs.getInt("quota_id");
            job_applicant_applicationDTO.lastModificationTime = rs.getLong("lastModificationTime");
            job_applicant_applicationDTO.rollNumber = rs.getString("roll_number");
            job_applicant_applicationDTO.trackingNumber = rs.getString("tracking_number");
            job_applicant_applicationDTO.level = rs.getInt("level");
            job_applicant_applicationDTO.isViewed = rs.getBoolean("is_viewed");
            job_applicant_applicationDTO.isSelected = rs.getInt("is_selected");
            job_applicant_applicationDTO.isRejected = rs.getBoolean("is_rejected");
//			job_applicant_applicationDTO.summary = rs.getString("summary");
            job_applicant_applicationDTO.decision_remarks = rs.getString("decision_remarks");

            job_applicant_applicationDTO.isPresent = rs.getInt("is_present");
            job_applicant_applicationDTO.marks = rs.getDouble("marks");
            job_applicant_applicationDTO.prev_level_data = rs.getString("prev_level_data");


            job_applicant_applicationDTO.applicant_name_bn = rs.getString("applicant_name_bn");
            job_applicant_applicationDTO.applicant_name_en = rs.getString("applicant_name_en");
            job_applicant_applicationDTO.present_address_name_bn = rs.getString("present_address_name_bn");
            job_applicant_applicationDTO.present_address_name_en = rs.getString("present_address_name_en");
            job_applicant_applicationDTO.permanent_address_name_bn = rs.getString("permanent_address_name_bn");
            job_applicant_applicationDTO.permanent_address_name_en = rs.getString("permanent_address_name_en");
            job_applicant_applicationDTO.district_name_bn = rs.getString("district_name_bn");
            job_applicant_applicationDTO.district_name_en = rs.getString("district_name_en");
            job_applicant_applicationDTO.applicant_dob_en = rs.getString("applicant_dob_en");
            job_applicant_applicationDTO.applicant_dob_bn = rs.getString("applicant_dob_bn");
            job_applicant_applicationDTO.applicant_age_en = rs.getString("applicant_age_en");
            job_applicant_applicationDTO.applicant_age_bn = rs.getString("applicant_age_bn");
            job_applicant_applicationDTO.applicant_nid = rs.getString("applicant_nid");
            job_applicant_applicationDTO.quota_en = rs.getString("quota_en");
            job_applicant_applicationDTO.quota_bn = rs.getString("quota_bn");
            job_applicant_applicationDTO.annual_en = rs.getString("annual_en");
            job_applicant_applicationDTO.annual_bn = rs.getString("annual_bn");
            job_applicant_applicationDTO.priviledge_en = rs.getString("priviledge_en");
            job_applicant_applicationDTO.priviledge_bn = rs.getString("priviledge_bn");
            job_applicant_applicationDTO.edu_info_en = rs.getString("edu_info_en");
            job_applicant_applicationDTO.edu_info_bn = rs.getString("edu_info_bn");
            job_applicant_applicationDTO.job_experience_en = rs.getString("job_experience_en");
            job_applicant_applicationDTO.job_experience_bn = rs.getString("job_experience_bn");
            job_applicant_applicationDTO.photo_file_id = rs.getLong("photo_file_id");

            job_applicant_applicationDTO.father_name = rs.getString("father_name");
            job_applicant_applicationDTO.mother_name = rs.getString("mother_name");
            job_applicant_applicationDTO.highest_edu_level = rs.getLong("highest_edu_level");

            job_applicant_applicationDTO.district = rs.getString("district");
            job_applicant_applicationDTO.division = rs.getString("division");

            job_applicant_applicationDTO.university_ids = rs.getString("university_ids");

            job_applicant_applicationDTO.seatPlanId = rs.getLong("seat_plan_id");
            job_applicant_applicationDTO.seatPlanChildId = rs.getLong("seat_plan_child_id");
            job_applicant_applicationDTO.recruitmentExamVenueId = rs.getLong("recruitment_exam_venue_id");
            job_applicant_applicationDTO.recruitmentExamVenueItemId = rs.getLong("recruitment_exam_venue_item_id");
            job_applicant_applicationDTO.isAdmitCardDownloaded = rs.getInt("is_admit_downloaded");

            job_applicant_applicationDTO.age_in_years = rs.getInt("age_in_years");
            job_applicant_applicationDTO.qualification_in_years = rs.getInt("qualification_in_years");
            return job_applicant_applicationDTO;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }


    //need another getter for repository
    public Job_applicant_applicationDTO getDTOByID(long ID) {
        String sql = "SELECT * FROM " + tableName + " WHERE ID = ? ";
        Job_applicant_applicationDTO jobApplicantApplicationDTO =
                ConnectionAndStatementUtil.getT(sql, Arrays.asList(ID), this::build);
        return jobApplicantApplicationDTO;
    }


    public List<Job_applicant_applicationDTO> getDTOs(Collection recordIDs) {
        if (recordIDs.isEmpty()) {
            return new ArrayList<>();
        }
        String sql = "SELECT * FROM " + tableName + " WHERE ID IN ( ";
        for (int i = 0; i < recordIDs.size(); i++) {
            if (i != 0) {
                sql += ",";
            }
            sql += " ? ";
        }
        sql += ")  order by lastModificationTime desc";
        printSql(sql);
        return ConnectionAndStatementUtil.getListOfT(sql, Arrays.asList(recordIDs.toArray()), this::build);

    }


    public List<Job_applicant_applicationDTO> getDTOsWithMaxLevelByApplicantId(long appId) {

        List<Job_applicant_applicationDTO> dtos = new ArrayList<>();

        List<Job_applicant_applicationDTO> applicantDTOs = Job_applicant_applicationRepository.getInstance().
                getJob_applicant_applicationDTOByjob_applicant_id(appId);

        Set<Long> jobIds = applicantDTOs.stream().map(i -> i.jobId).collect(Collectors.toSet());

        jobIds.forEach(i -> {
            List<Job_applicant_applicationDTO> jobSpecificDTOs =
                    applicantDTOs.stream().filter(j -> j.jobId == i).collect(Collectors.toList());
            int l = jobSpecificDTOs.stream().mapToInt(j -> j.level).max().getAsInt();
            Job_applicant_applicationDTO jobApplicantApplicationDTO = jobSpecificDTOs.stream().
                    filter(j -> j.level == l).findFirst().get();
            jobApplicantApplicationDTO.insertionDate = jobSpecificDTOs.stream().mapToLong(j -> j.insertionDate).min().getAsLong();
            dtos.add(jobApplicantApplicationDTO);
        });

        return dtos;


//		String sql = "SELECT job_id,  (SELECT min(insertion_date) FROM job_applicant_application jaa " +
//				"WHERE jaa.isDeleted = 0 and jaa.job_applicant_id = ? "  +
//				" and jaa.job_id = job_applicant_application.job_id) AS insertion_date, level, is_selected, is_rejected";
//
//		sql += " FROM " + tableName;
//
//		sql += " WHERE job_applicant_id =  ? " ;
//		sql += " AND isDeleted = 0 AND level = (SELECT MAX(level) FROM job_applicant_application jaa " +
//				" WHERE jaa.job_id = job_applicant_application.job_id and jaa.isDeleted = 0 and jaa.job_applicant_id = ?  )";
//
//
//		sql+="  order by lastModificationTime desc";
//		printSql(sql);
//		return ConnectionAndStatementUtil.getListOfT(sql, Arrays.asList(appId,appId,appId),this::buildWithMaxLevelByApplicantId);

    }


    public Job_applicant_applicationDTO buildWithMaxLevelByApplicantId(ResultSet rs) {
        try {
            Job_applicant_applicationDTO job_applicant_applicationDTO = new Job_applicant_applicationDTO();
            job_applicant_applicationDTO.jobId = rs.getLong("job_id");
            job_applicant_applicationDTO.insertionDate = rs.getLong("insertion_date");
            job_applicant_applicationDTO.level = rs.getInt("level");
            job_applicant_applicationDTO.isSelected = rs.getInt("is_selected");
            job_applicant_applicationDTO.isRejected = rs.getBoolean("is_rejected");
            return job_applicant_applicationDTO;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }


    //add repository
    public List<Job_applicant_applicationDTO> getAllJob_applicant_application(boolean isFirstReload) {

        String sql = "SELECT * FROM job_applicant_application";
        sql += " WHERE ";


        if (isFirstReload) {
            sql += " isDeleted =  0";
        }
        if (!isFirstReload) {
            sql += " lastModificationTime >= " + RepositoryManager.lastModifyTime;
        }
        sql += " order by job_applicant_application.lastModificationTime desc";
        printSql(sql);

        return ConnectionAndStatementUtil.getListOfT(sql, this::build);
    }

    public List<Job_applicant_applicationDTO> getDTOByJobApplicantID(long ID) {

        String sql = "SELECT * FROM job_applicant_application";

        sql += " WHERE job_applicant_id= ? ";

        sql += " AND isDeleted =  0";

        sql += " order by job_applicant_application.lastModificationTime desc";
        printSql(sql);

        return ConnectionAndStatementUtil.getListOfT(sql, Arrays.asList(ID), this::build);
    }


    public List<Job_applicant_applicationDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO) {
        return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
    }


    public List<Job_applicant_applicationDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
                                                      String filter, boolean tableHasJobCat) {
        String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat);
        printSql(sql);
        return ConnectionAndStatementUtil.getListOfT(sql, this::build);

    }

    public String getSqlWithSearchCriteriaForNormalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
                                                          UserDTO userDTO, String filter, boolean tableHasJobCat) {
        boolean viewAll = false;

        if (p_searchCriteria.get("ViewAll") != null) {
            System.out.println("ViewAll = " + p_searchCriteria.get("ViewAll"));
            viewAll = true;
        } else {
            System.out.println("ViewAll is null ");
        }

        String sql = "SELECT ";
        if (category == GETIDS) {
            sql += " " + tableName + ".ID as ID ";
        } else if (category == GETCOUNT) {
            sql += " count( " + tableName + ".ID) as countID ";
        } else if (category == GETDTOS) {
            sql += " distinct " + tableName + ".* ";
        }
        sql += "FROM " + tableName + " ";
        sql += joinSQL;


        String AnyfieldSql = "";
        String AllFieldSql = "";

        if (p_searchCriteria != null) {


            Enumeration names = p_searchCriteria.keys();
            String str, value;

            AnyfieldSql = "(";

            if (p_searchCriteria.get("AnyField") != null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase("")) {
                AnyfieldSql += tableName + ".search_column like '%" + p_searchCriteria.get("AnyField").toString() + "%'";
            }
            AnyfieldSql += ")";
            System.out.println("AnyfieldSql = " + AnyfieldSql);

            AllFieldSql = "(";
            int i = 0;
            while (names.hasMoreElements()) {
                str = (String) names.nextElement();
                value = (String) p_searchCriteria.get(str);
                System.out.println(str + ": " + value);
                if (value != null && !value.equalsIgnoreCase("") && (
                        str.equals("acceptance_status")
                                || str.equals("tracking_number")
                                || str.equals("insertion_date_start")
                                || str.equals("insertion_date_end")
                                || str.equals("modified_by")
                )

                ) {
                    if (p_searchCriteria.get(str).equals("any")) {
                        continue;
                    }

                    if (i > 0) {
                        AllFieldSql += " AND  ";
                    }

                    if (str.equals("acceptance_status")) {
                        AllFieldSql += "" + tableName + ".acceptance_status like '%" + p_searchCriteria.get(str) + "%'";
                        i++;
                    } else if (str.equals("insertion_date_start")) {
                        AllFieldSql += "" + tableName + ".insertion_date >= " + p_searchCriteria.get(str);
                        i++;
                    } else if (str.equals("insertion_date_end")) {
                        AllFieldSql += "" + tableName + ".insertion_date <= " + p_searchCriteria.get(str);
                        i++;
                    } else if (str.equals("modified_by")) {
                        AllFieldSql += "" + tableName + ".modified_by like '%" + p_searchCriteria.get(str) + "%'";
                        i++;
                    } else if (str.equals("tracking_number")) {
                        AllFieldSql += "" + tableName + ".tracking_number like '%" + p_searchCriteria.get(str) + "%'";
                        i++;
                    }


                }
            }

            AllFieldSql += ")";
            System.out.println("AllFieldSql = " + AllFieldSql);


        }


        sql += " WHERE ";

        sql += " (" + tableName + ".isDeleted = 0 ";
        sql += ")";


        if (!filter.equalsIgnoreCase("")) {
            sql += " and " + filter + " ";
        }

        if (!AnyfieldSql.equals("()") && !AnyfieldSql.equals("")) {
            sql += " AND " + AnyfieldSql;

        }
        if (!AllFieldSql.equals("()") && !AllFieldSql.equals("")) {
            sql += " AND " + AllFieldSql;
        }


        sql += " order by " + tableName + ".marks desc ";

        printSql(sql);

        if (limit >= 0) {
            sql += " limit " + limit;
        }
        if (offset >= 0) {
            sql += " offset " + offset;
        }

        System.out.println("-------------- sql = " + sql);

        return sql;
    }

    public String getSqlWithSearchCriteria(Hashtable p_searchCriteria, int limit, int offset, int category,
                                           boolean isPermanentTable, UserDTO userDTO, String filter, boolean tableHasJobCat) {
        return getSqlWithSearchCriteriaForNormalSearch(p_searchCriteria, limit, offset, category, userDTO, filter, tableHasJobCat);
    }


    public List<Job_applicant_applicationDTO> getDTOsByJobID(long ID) {

        String sql = "SELECT * FROM job_applicant_application";

        sql += " WHERE job_id= ? ";

        sql += " AND isDeleted =  0";

        sql += " order by job_applicant_application.lastModificationTime desc";
        printSql(sql);
        return ConnectionAndStatementUtil.getListOfT(sql, Arrays.asList(ID), this::build);
    }


    public List<Job_applicant_applicationDTO> getDTOsByJobIDAndLevel(long jobId, int level) {
        List<Job_applicant_applicationDTO> job_applicant_applicationDTOList = new ArrayList<>();

        String sql = "SELECT * FROM job_applicant_application";

        sql += " WHERE job_id= ? ";
        sql += " AND level = ? ";
        sql += " AND isDeleted =  0";

        sql += " order by job_applicant_application.lastModificationTime desc";
        printSql(sql);
        return ConnectionAndStatementUtil.getListOfT(sql, Arrays.asList(jobId, level), this::build);
    }


    public List<Job_applicant_applicationDTO> getDTOsByJobIDAndLevelAndApplicantId(long jobId, int level, long applicantId) {
        List<Job_applicant_applicationDTO> job_applicant_applicationDTOList = new ArrayList<>();

        String sql = "SELECT * FROM job_applicant_application";

        sql += " WHERE job_id= ? ";
        sql += " AND level = ? ";
        sql += " AND job_applicant_id = ? ";
        sql += " AND isDeleted =  0";

        sql += " order by job_applicant_application.lastModificationTime desc";
        printSql(sql);

        return ConnectionAndStatementUtil.getListOfT(sql, Arrays.asList(jobId, level, applicantId), this::build);
    }

    public String getViewList(String Language) {
        String sOptions = "";
        if (Language.equals("English")) {
            sOptions = "<option value = 'ALL'>All</option>";
        } else {
            sOptions = "<option value = 'ALL'>সকল</option>";
        }

        for (int i = 0; i < viewList.size(); i++) {
            if (Language.equals("English")) {
                sOptions += "<option value = '" + viewList.get(i) + "'>" + viewList.get(i) + "</option>";
            } else {
                sOptions += "<option value = '" + viewList.get(i) + "'>" + viewMap.get(viewList.get(i)) + "</option>";
            }
        }

        return sOptions;
    }


    public String getApproveList(String Language) {
        String sOptions = "";
        if (Language.equals("English")) {
            sOptions = "<option value = 'ALL'>All</option>";
        } else {
            sOptions = "<option value = 'ALL'>সকল</option>";
        }

        for (int i = 0; i < approveList.size(); i++) {
            if (Language.equals("English")) {
                sOptions += "<option value = '" + approveList.get(i) + "'>" + approveList.get(i) + "</option>";
            } else {
                sOptions += "<option value = '" + approveList.get(i) + "'>" + approveMap.get(approveList.get(i)) + "</option>";
            }
        }

        return sOptions;
    }

    public String getQuotaList(String Language) {
//		CatDAO catDAO = new CatDAO();
        List<CategoryLanguageModel> categoryLanguageModelList = CatRepository.getInstance().getCategoryLanguageModelList
                ("recruitment_quota").stream().filter(i -> i.isDeleted == 0 && i.categoryValue != 0).collect(Collectors.toList());
        String sOptions = "";
        if (Language.equals("English")) {
            sOptions = "<option value = '0'>All</option>";
        } else {
            sOptions = "<option value = '0'>সকল</option>";
        }

        for (int i = 0; i < categoryLanguageModelList.size(); i++) {
            if (Language.equals("English")) {
                sOptions += "<option value = '" + categoryLanguageModelList.get(i).categoryValue
                        + "'>" + categoryLanguageModelList.get(i).englishText + "</option>";
            } else {
                sOptions += "<option value = '" + categoryLanguageModelList.get(i).categoryValue + "'>" +
                        categoryLanguageModelList.get(i).banglaText + "</option>";
            }
        }
        return sOptions;
    }

    public int getAllCountByJobIdAndLevelId(long jobId, long levelId) {

        return (int) Job_applicant_applicationRepository.getInstance().getJob_applicant_applicationDTOByjob_id(jobId)
                .stream()
                .filter(i -> i.level == levelId)
                .count();

//		String sql  = " SELECT COUNT(*) AS count FROM job_applicant_application ";
//		sql += " WHERE ";
//		sql += " job_id = ? " ;
//		sql += " and level =  ? " ;
//		sql += " and isDeleted = 0 ";
//
//		printSql(sql);
//		int count = 0;
//
//		Job_applicant_applicationDTO dto = ConnectionAndStatementUtil.
//				getT(sql, Arrays.asList(jobId, levelId),this::buildForCount);
//		if(dto != null){
//			count = dto.count;
//		}
//		return count;


    }

    public int getViewAllCountByJobIdAndLevelId(long jobId, long levelId) {
        return (int) Job_applicant_applicationRepository.getInstance().getJob_applicant_applicationDTOByjob_id(jobId)
                .stream()
                .filter(i -> i.level == levelId && i.isViewed)
                .count();


//		String sql  = " SELECT COUNT(*) AS count FROM job_applicant_application ";
//		sql += " WHERE ";
//		sql += " job_id = ? " ;
//		sql += " and level =  ? " ;
//		sql += " and is_viewed =  1" ;
//		sql += " and isDeleted = 0 ";
//
//		printSql(sql);
//
//		int count = 0;
//
//		Job_applicant_applicationDTO dto = ConnectionAndStatementUtil.
//				getT(sql, Arrays.asList(jobId, levelId),this::buildForCount);
//		if(dto != null){
//			count = dto.count;
//		}
//		return count;
    }

    public int getRejectAllCountByJobIdAndLevelId(long jobId, long levelId) {
        return (int) Job_applicant_applicationRepository.getInstance().getJob_applicant_applicationDTOByjob_id(jobId)
                .stream()
                .filter(i -> i.level == levelId && i.isRejected)
                .count();
    }

    public int getAllAdmitCardByJobIdAndLevelId(long jobId, long levelId) {
//        return (int) Job_applicant_applicationRepository.getInstance().getJob_applicant_applicationDTOByjob_id(jobId)
//                .stream()
//                .filter(i -> i.level == levelId && i.isAdmitCardDownloaded == 1)
//                .count();

        return (int) getDTOsByJobID(jobId)
                .stream()
                .filter(i -> i.level == levelId && i.isAdmitCardDownloaded == 1)
                .count();
    }

    public int getSelectAllCountByJobIdAndLevelId(long jobId, long levelId) {

        return (int) Job_applicant_applicationRepository.getInstance().getJob_applicant_applicationDTOByjob_id(jobId)
                .stream()
                .filter(i -> i.level == levelId && (i.isSelected == 1 || i.isSelected == 2))
                .count();

//		String sql  = " SELECT COUNT(*) AS count FROM job_applicant_application ";
//		sql += " WHERE ";
//		sql += " job_id = ? " ;
//		sql += " and level =  ? " ;
//		sql += " and (is_selected =  1 or is_selected = 2 ) " ;
//		sql += " and isDeleted = 0 ";
//
//		printSql(sql);
//
//		int count = 0;
//
//		Job_applicant_applicationDTO dto = ConnectionAndStatementUtil.
//				getT(sql, Arrays.asList(jobId, levelId),this::buildForCount);
//		if(dto != null){
//			count = dto.count;
//		}
//		return count;
    }


    public Long buildForApplicantIdsByJobIDAndLevelZero(ResultSet rs) {
        try {
            return rs.getLong("job_applicant_id");
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    public List<Long> getApplicantIdsByJobIDAndLevelZero(long jobId) {

        return Job_applicant_applicationRepository.getInstance().getJob_applicant_applicationDTOByjob_id(jobId)
                .stream()
                .filter(i -> i.level == 0)
                .map(i -> i.jobApplicantId)
                .collect(Collectors.toList());

//		List<Long> ids = new ArrayList<>();
//		String sql = "SELECT job_applicant_id FROM job_applicant_application";
//
//		sql += " WHERE job_id = ? ";
//		sql+=" AND isDeleted =  0 AND level = 0";
//		sql += " order by job_applicant_application.lastModificationTime desc";
//		printSql(sql);
//
//		return ConnectionAndStatementUtil.getListOfT(sql, Arrays.asList(jobId),this::buildForApplicantIdsByJobIDAndLevelZero);
    }

    public PositionWiseStatusDTO getPositionWiseStatus(long jobId, String language) {
        PositionWiseStatusDTO positionWiseStatusDTO = new PositionWiseStatusDTO();
        positionWiseStatusDTO.data = new ArrayList<>();
        PositionWiseItemDTO positionWiseItemDTO = new PositionWiseItemDTO();
        positionWiseItemDTO.level = UtilCharacter.getDataByLanguage(language, "প্রাথমিক", "Primary");
        positionWiseItemDTO.total = getAllCountByJobIdAndLevelId(jobId, 0);
        positionWiseItemDTO.shortListed = getSelectAllCountByJobIdAndLevelId(jobId, 0);
        positionWiseItemDTO.rejected = getRejectAllCountByJobIdAndLevelId(jobId, 0);

        positionWiseStatusDTO.data.add(positionWiseItemDTO);

//		JobSpecificExamTypeDao jobSpecificExamTypeDao = new JobSpecificExamTypeDao();
        List<RecruitmentJobSpecificExamTypeDTO> jobSpecificExamTypeDTOS =
                JobSpecificExamTypeRepository.getInstance()
                        .getRecruitmentJobSpecificExamTypeDTOByjob_id(jobId);
//				jobSpecificExamTypeDao.getDTOsByJobId(jobId);

//		System.out.println("#######");
//		System.out.println(jobSpecificExamTypeDTOS.size());
//		System.out.println(JobSpecificExamTypeRepository.getInstance()
//				.getRecruitmentJobSpecificExamTypeDTOByjob_id(jobId));
//		System.out.println("#######");


        for (RecruitmentJobSpecificExamTypeDTO examTypeDTO : jobSpecificExamTypeDTOS) {
            positionWiseItemDTO = new PositionWiseItemDTO();
            positionWiseItemDTO.level = CatRepository.getName(language, "job_exam_type", examTypeDTO.jobExamTypeCat);
            positionWiseItemDTO.total = getAllCountByJobIdAndLevelId(jobId, examTypeDTO.order);
            positionWiseItemDTO.shortListed = getSelectAllCountByJobIdAndLevelId(jobId, examTypeDTO.order);
            positionWiseItemDTO.rejected = getRejectAllCountByJobIdAndLevelId(jobId, examTypeDTO.order);

            positionWiseStatusDTO.data.add(positionWiseItemDTO);
        }

//		System.out.println(positionWiseStatusDTO.data);
//
//		System.out.println("#######");

        return positionWiseStatusDTO;
    }

    public List<Job_applicant_applicationDTO> getIDDTOsByJobLevelID(int job_id, int level_id) {


        String sql = "SELECT * ";

        sql += " FROM " + tableName;

        sql += " WHERE job_id = ? ";

        sql += " AND level = ? ";

        sql += " AND isDeleted=0";

        printSql(sql);
        return ConnectionAndStatementUtil.getListOfT(sql, Arrays.asList(job_id, level_id), this::build);

    }

    public int JobLevelIdExistence(int job_id, int level_id) {
        return getIDDTOsByJobLevelID(job_id, level_id).size() > 0 ? 1 : 0;
    }


    public Boolean decisionNotStarted(long jobId) {
        List<Job_applicant_applicationDTO> dtos = Job_applicant_applicationRepository.getInstance().
                getJob_applicant_applicationDTOByjob_id(jobId).stream().filter(i -> i.level == 0)
                .collect(Collectors.toList());
//		getDTOsByFilter("level = 0 and job_id = " + jobId + " ");
        int size = dtos.size();
        int newSize = (int) dtos.stream().filter(i -> i.isSelected == 0 && !i.isRejected).count();
        return size > 0 && size == newSize;
    }


    public List<Job_applicant_applicationDTO> getDTOsByFilter(String filter) {
        String sql = "SELECT * ";

        sql += " FROM " + tableName;
        sql += " WHERE " + filter;
        sql += " AND isDeleted= 0";

        printSql(sql);
        return ConnectionAndStatementUtil.getListOfT(sql, this::buildWithoutSummary);
    }


    public Job_applicant_applicationDTO getDTOByJobIdAndRollNumber(long jobId, String rollNumber) {
        String sql = "SELECT * ";
        sql += " FROM " + tableName;
        sql += " WHERE job_id= ? ";
        sql += " AND roll_number like ? AND isDeleted = 0";
        printSql(sql);
        return ConnectionAndStatementUtil.getT(sql, Arrays.asList(jobId, rollNumber), this::build);
    }

    public Job_applicant_applicationDTO getDTOByJobIdAndLevelAndRollNumber(long jobId, long level, String rollNumber) {
        String sql = "SELECT * ";
        sql += " FROM " + tableName;
        sql += " WHERE job_id = ? ";
        sql += " AND level= ? ";
        sql += " AND roll_number like ? AND isDeleted = 0";

        printSql(sql);
        return ConnectionAndStatementUtil.getT(sql, Arrays.asList(jobId, level, rollNumber), this::build);
    }


    public CandidateCountDTO buildForApplicantCount(ResultSet rs) {
        try {
            CandidateCountDTO candidateCountDTO = new CandidateCountDTO();
            candidateCountDTO.applied = rs.getInt("applied");
            candidateCountDTO.accepted = rs.getInt("accepted");
            candidateCountDTO.declined = rs.getInt("discarded");
            return candidateCountDTO;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    public CandidateCountDTO getApplicantCount(long jobId) {
        List<Job_applicant_applicationDTO> dtos = Job_applicant_applicationRepository.getInstance().
                getJob_applicant_applicationDTOByjob_id(jobId);
        CandidateCountDTO candidateCountDTO = new CandidateCountDTO();
        candidateCountDTO.applied = (int) dtos.stream().filter(i -> i.level == 0).count();
        candidateCountDTO.accepted = (int) dtos.stream().filter(i -> i.isSelected == 2).count();
        candidateCountDTO.declined = (int) dtos.stream().filter(i -> i.isRejected).count();

        return candidateCountDTO;

//		String sql = "SELECT  ";
//		sql += " (SELECT COUNT(*) FROM job_applicant_application jaa WHERE jaa.job_id = ? AND jaa.level = 0 AND jaa.isDeleted = 0  ) AS applied, ";
//		sql += " (SELECT COUNT(*) FROM job_applicant_application jaa WHERE jaa.job_id = ? AND jaa.is_rejected = 1 AND jaa.isDeleted = 0 ) AS discarded, ";
//		sql += " (SELECT COUNT(*) FROM job_applicant_application jaa WHERE jaa.job_id = ? AND jaa.is_selected = 2 AND jaa.isDeleted = 0 ) AS accepted";
//
//		printSql(sql);
//		return ConnectionAndStatementUtil.getT(sql,Arrays.asList(jobId, jobId, jobId), this::buildForApplicantCount);
    }

    public long getTotalApplicationCount() {

        return Job_applicant_applicationRepository.getInstance().getJob_applicant_applicationList()
                .stream()
                .filter(i -> i.level == 0)
                .count();


//		String sql = "SELECT COUNT(*) AS count FROM job_applicant_application " +
//				" WHERE isDeleted = 0 AND LEVEL = 0  ";
//
//		printSql(sql);
//		int count = 0;
//
//		Job_applicant_applicationDTO dto = ConnectionAndStatementUtil.
//				getT(sql, this::buildForCount);
//		if(dto != null){
//			count = dto.count;
//		}
//		return count;

    }

    public CandidateCountDTO buildForQuotaCount(ResultSet rs) {
        try {
            CandidateCountDTO candidateCountDTO = new CandidateCountDTO();
            candidateCountDTO.quotaId = rs.getInt("quota_id");
            candidateCountDTO.applied = rs.getInt("count");
            return candidateCountDTO;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    public List<CandidateCountDTO> getQuotaCount(long jobId) {

        List<Job_applicant_applicationDTO> dtos = Job_applicant_applicationRepository.getInstance()
                .getJob_applicant_applicationDTOByjob_id(jobId)
                .stream().filter(i -> i.level == 0).collect(Collectors.toList());

        List<CategoryLanguageModel> categoryLanguageModelList = CatRepository.getInstance().getCategoryLanguageModelList
                ("recruitment_quota").stream().filter(i -> i.isDeleted == 0 && i.categoryValue != 0).collect(Collectors.toList());

        List<CandidateCountDTO> candidateCountDTOS = new ArrayList<>();

        for (CategoryLanguageModel m : categoryLanguageModelList) {
            CandidateCountDTO dto = new CandidateCountDTO();
            dto.quotaId = m.categoryValue;
            dto.applied = (int) dtos.stream().filter(i -> i.quota_id == m.categoryValue).count();
            candidateCountDTOS.add(dto);
        }

        return candidateCountDTOS;


//		String sql = "SELECT quota_id, COUNT(*) as count FROM job_applicant_application ";
//		sql += " WHERE  job_id = ? " ;
//		sql += " AND LEVEL = 0 AND isDeleted = 0 GROUP BY quota_id";
//		printSql(sql);
//		return ConnectionAndStatementUtil.getListOfT(sql, Arrays.asList(jobId),this::buildForQuotaCount);

    }


    public Map<String, Integer> getHighestEduLevelList(long jobId, String language) {

        List<Job_applicant_applicationDTO> dtos = Job_applicant_applicationRepository.getInstance()
                .getJob_applicant_applicationDTOByjob_id(jobId)
                .stream()
                .filter(i -> i.level == 0)
                .collect(Collectors.toList());
//				getDTOsByJobIDAndLevel(jobId, 0);

        if (dtos.size() == 0) {
            return new HashMap<>();
        }

        List<NameDTO> nameDTOS = Education_levelRepository.getInstance().getNameDTOList();
        Map<Long, NameDTO> nameDTOMap = nameDTOS.stream()
                .collect(Collectors.toMap(model -> model.iD, model -> model));
        boolean isLanguageBangla = "Bangla".equalsIgnoreCase(language);
        Map<String, Integer> levelMap = dtos.stream()
                .map(employee -> getHighestEduLevel(nameDTOMap.get(employee.highest_edu_level), isLanguageBangla))
                .collect(Collectors.groupingBy(genderText -> genderText, Collectors.collectingAndThen(Collectors.counting(), Long::intValue)));
        return new TreeMap<>(levelMap);

    }


    private String getHighestEduLevel(NameDTO model, boolean isLanguageBangla) {
        return model == null ? (isLanguageBangla ? "নেই" : "Not Available") : (model.nameBn);
    }

    private String getDivision(GeoDivisionDTO model, boolean isLanguageBangla) {
        return model == null ? (isLanguageBangla ? "নেই" : "Not Available") : (isLanguageBangla ? model.nameBan : model.nameEng);
    }

    private String getDistrict(GeoDistrictDTO model, boolean isLanguageBangla) {
        return model == null ? (isLanguageBangla ? "নেই" : "Not Available") : (isLanguageBangla ? model.nameBan : model.nameEng);
    }

    public Map<String, Integer> getDistrictCountList(long jobId, String language) {

        List<Job_applicant_applicationDTO> dtos = Job_applicant_applicationRepository.getInstance()
                .getJob_applicant_applicationDTOByjob_id(jobId)
                .stream()
                .filter(i -> i.level == 0)
                .collect(Collectors.toList());
//				getDTOsByJobIDAndLevel(jobId, 0);

        if (dtos.size() == 0) {
            return new HashMap<>();
        }

        List<GeoDistrictDTO> districtDTOS = GeoDistrictRepository.getInstance().getAllDistrict();
        Map<String, GeoDistrictDTO> nameDTOMap = districtDTOS.stream()
                .collect(Collectors.toMap(model -> String.valueOf(model.id), model -> model));
        boolean isLanguageBangla = "Bangla".equalsIgnoreCase(language);
        Map<String, Integer> levelMap = dtos.stream()
                .map(employee -> getDistrict(nameDTOMap.get(employee.district), isLanguageBangla))
                .collect(Collectors.groupingBy(genderText -> genderText, Collectors.collectingAndThen(Collectors.counting(), Long::intValue)));


        int highestCount = 10;

        if (levelMap.size() > highestCount) {
            List<String> r = Arrays.asList("Not Available", "নেই");
            List<Map.Entry<String, Integer>> list =
                    new LinkedList<>(levelMap.entrySet()).stream()
                            .filter(i -> !r.contains(i.getKey())).collect(Collectors.toList());
            Comparator c = Collections.reverseOrder((Comparator<Map.Entry<String, Integer>>)
                    (o1, o2) -> (o1.getValue()).compareTo(o2.getValue()));
            list.sort(c);


            TreeMap<String, Integer> temp = new TreeMap<>();
            for (int i = 0; i < highestCount && i < list.size(); i++) {
                temp.put(list.get(i).getKey(), list.get(i).getValue());
            }
            return temp;

        }


        return new TreeMap<>(levelMap);

    }


    public Map<String, Integer> getDivisionCountList(long jobId, String language) {

        List<Job_applicant_applicationDTO> dtos = Job_applicant_applicationRepository.getInstance()
                .getJob_applicant_applicationDTOByjob_id(jobId)
                .stream()
                .filter(i -> i.level == 0)
                .collect(Collectors.toList());
//				getDTOsByJobIDAndLevel(jobId, 0);

        if (dtos.size() == 0) {
            return new HashMap<>();
        }

        List<GeoDivisionDTO> divisionDTOS = GeoDivisionRepository.getInstance().getDivisionDTOList();
        Map<String, GeoDivisionDTO> nameDTOMap = divisionDTOS.stream()
                .collect(Collectors.toMap(model -> String.valueOf(model.id), model -> model));
        boolean isLanguageBangla = "Bangla".equalsIgnoreCase(language);
        Map<String, Integer> levelMap = dtos.stream()
                .map(employee -> getDivision(nameDTOMap.get(employee.division), isLanguageBangla))
                .collect(Collectors.groupingBy(genderText -> genderText, Collectors.collectingAndThen(Collectors.counting(), Long::intValue)));
        return new TreeMap<>(levelMap);

    }

    public Map<String, Integer> getAllStatusCountByJobId(long jobId, LoginDTO loginDTO) {

        CandidateCountDTO candidateCountDTO = getApplicantCount(jobId);
        Map<String, Integer> levelMap = new HashMap<>();
        levelMap.put(LM.getText(LC.CANDIDATE_LIST_APPLIED, loginDTO), candidateCountDTO.applied);
        levelMap.put(LM.getText(LC.CANDIDATE_LIST_ACCEPTED, loginDTO), candidateCountDTO.accepted);
        levelMap.put(LM.getText(LC.CANDIDATE_LIST_DISCARDED, loginDTO), candidateCountDTO.declined);
        return new TreeMap<>(levelMap);

    }

    public void generateRoll(long jobId) throws Exception {

//			Parliament_job_applicantDAO parliament_job_applicantDAO = new Parliament_job_applicantDAO();
        Job_applicant_applicationDAO job_applicant_applicationDAO = new Job_applicant_applicationDAO();

        List<Job_applicant_applicationDTO> job_applicant_applicationDTOS =
                Job_applicant_applicationRepository.getInstance().getJob_applicant_applicationDTOByjob_id(jobId);
//					job_applicant_applicationDAO.getDTOsByJobID(jobId);

        List<Long> applicantIds = new ArrayList<>();

        List<Integer> districtIds = new LinkedList<>();

        for (Job_applicant_applicationDTO job_applicant_applicationDTO : job_applicant_applicationDTOS) {
            applicantIds.add(job_applicant_applicationDTO.jobApplicantId);
        }

//			List<Parliament_job_applicantDTO> parliament_job_applicantDTOS = parliament_job_applicantDAO.getDTOs(applicantIds);

        List<Parliament_job_applicantDTO> parliament_job_applicantDTOS = new ArrayList<>();
        for (Long appID : applicantIds) {
            Parliament_job_applicantDTO dto = Parliament_job_applicantRepository.getInstance().
                    getParliament_job_applicantDTOByID(appID);
            if (dto != null) {
                parliament_job_applicantDTOS.add(dto);
            }

        }

        applicantIds = new LinkedList<>();

        for (Parliament_job_applicantDTO parliament_job_applicantDTO : parliament_job_applicantDTOS) {
            districtIds.add(Integer.parseInt(parliament_job_applicantDTO.homeDistrictName));
            applicantIds.add(parliament_job_applicantDTO.iD);
        }

        HashMap<Integer, LinkedList<Integer>> districtToApplicationId = new HashMap<>();

        HashMap<Integer, Integer> result = new HashMap<>();
        HashMap<Integer, Integer> resultCombined = new HashMap<>();
        HashMap<Integer, Integer> resultSorted = new HashMap<>();
        HashMap<Integer, Integer> applicationIdToDistrict = new HashMap<>();

        LinkedList<Integer> allowedSameDistrict = new LinkedList<>();

        int applicantSize = applicantIds.size();
        final int applicantSizeFinal = applicantSize;
        AtomicReference<Integer> majorDistrictAtmoic = new AtomicReference<>(-1);
        int majorDistrict = -1;
        int allowedNumberMajorDistrict = -1;
        int allowedNumberSameDistrict = -1;

        parliament_job_applicantDTOS
                .forEach(parliament_job_applicantDTO -> {
                    if (districtToApplicationId.containsKey(Integer.parseInt(parliament_job_applicantDTO.homeDistrictName))) {
                        LinkedList<Integer> applicantList = districtToApplicationId.get(Integer.parseInt(parliament_job_applicantDTO.homeDistrictName));
                        applicantList.add(((Number) (parliament_job_applicantDTO.iD)).intValue());
                        districtToApplicationId.put(Integer.parseInt(parliament_job_applicantDTO.homeDistrictName), applicantList);
                    } else {
                        districtToApplicationId.put(Integer.parseInt(parliament_job_applicantDTO.homeDistrictName), new LinkedList<Integer>(Arrays.asList(((Number) (parliament_job_applicantDTO.iD)).intValue())));
                    }
                });

        districtToApplicationId.forEach((k, v) -> {
            if (v.size() >= (applicantSizeFinal / 2)) {
                if (majorDistrictAtmoic.get() != -1) {
                    if (districtToApplicationId.get(majorDistrictAtmoic.get()).size() == v.size() && districtIds.size() == 3) {
                        majorDistrictAtmoic.set(-1);
                    } else if (districtToApplicationId.get(majorDistrictAtmoic.get()).size() < v.size()) {
                        majorDistrictAtmoic.set(k);
                    }
                } else majorDistrictAtmoic.set(k);
            }
        });

        if (majorDistrictAtmoic.get() != -1) {
            majorDistrict = majorDistrictAtmoic.get();
            allowedNumberMajorDistrict = districtToApplicationId.get(majorDistrict).size();
            int diff = 2 * (districtToApplicationId.get(majorDistrict).size()) - applicantSizeFinal;
            if (diff > 1) allowedNumberSameDistrict = (diff / 2) + 1;
            districtIds.remove((Integer) majorDistrict);
        }

        for (int j = 0; j < allowedNumberMajorDistrict; j++) {
            LinkedList<Integer> applications = districtToApplicationId.get(majorDistrict);

            int applicationRandom = (int) (Math.random() * (applications.size() - 1) + 0);

            int applicationId = applications.get(applicationRandom);
            applications.remove(applicationRandom);

            allowedSameDistrict.add(applicationId);
            districtToApplicationId.put(majorDistrict, applications);

            applicantSize--;
        }


        int districtSize = districtIds.size();
        int previousDistrict = -1;
        int currentDistrict = -1;
        int currentDistrictIndex = 0;
        int min = 0;
        int max = min + districtSize - 1;
        int rollNumber = 1;

        while (rollNumber <= applicantSize) {

            if (districtSize != 1) {

                currentDistrictIndex = (int) (Math.random() * (max - min) + min);
                currentDistrict = districtIds.get(currentDistrictIndex);

                districtIds.remove(currentDistrictIndex);
                if (previousDistrict != -1) districtIds.add(previousDistrict);
                previousDistrict = currentDistrict;

            } else {
                currentDistrict = districtIds.get(currentDistrictIndex);
            }

            LinkedList<Integer> applications = districtToApplicationId.get(currentDistrict);
            if (applications.isEmpty()) continue;

            int applicationRandom = (int) (Math.random() * ((applications.size() - 1) - min) + min);

            int applicationId = applications.get(applicationRandom);
            applications.remove(applicationRandom);
            districtToApplicationId.put(currentDistrict, applications);

            result.put(applicationId, rollNumber);
            applicationIdToDistrict.put(applicationId, currentDistrict);
            rollNumber++;

        }

        resultSorted = sortByValueInt(result);

        List<Job_applicant_applicationDTO> resultList = new LinkedList<>();


        for (Map.Entry<Integer, Integer> entry : resultSorted.entrySet()) {

            Job_applicant_applicationDTO job_applicant_applicationDTO = new Job_applicant_applicationDTO();
            job_applicant_applicationDTO.level = (entry.getKey());
            job_applicant_applicationDTO.isSelected = (entry.getValue());

            resultList.add(job_applicant_applicationDTO);
        }


        while (true && majorDistrict == -1) {

            boolean noSameDistrict = true;

            for (int ii = 0; ii < applicantSize; ii++) {

                HashSet<Integer> neighbours = new HashSet<>();

                Job_applicant_applicationDTO dto = resultList.get(ii);

                if (ii > 0) {
                    neighbours.add(applicationIdToDistrict.get(resultList.get(ii - 1).level));
                }
                if (ii < applicantSize - 1) {
                    neighbours.add(applicationIdToDistrict.get(resultList.get(ii + 1).level));
                }

                if (neighbours.contains(applicationIdToDistrict.get(dto.level))) {

                    for (int inner = 0; inner < applicantSize; inner++) {

                        boolean breakForLoop = false;

                        HashSet<Integer> neighboursInner = new HashSet<>();

                        Job_applicant_applicationDTO dtoInner = resultList.get(inner);

                        if (inner > 0) {
                            neighboursInner.add(applicationIdToDistrict.get(resultList.get(inner - 1).level));
                        }
                        if (inner < applicantSize - 1) {
                            neighboursInner.add(applicationIdToDistrict.get(resultList.get(inner + 1).level));
                        }

                        if (!(neighboursInner.contains(applicationIdToDistrict.get(dto.level))
                                || neighbours.contains(applicationIdToDistrict.get(dtoInner.level)))) {

                            int level = dto.level;
                            dto.level = dtoInner.level;
                            dtoInner.level = level;

                            resultList.set(ii, dto);
                            resultList.set(inner, dtoInner);

                            breakForLoop = true;

                        }

                        if (breakForLoop) break;

                    }
                    noSameDistrict = false;

                }

                if (noSameDistrict && ii > applicantSize - 3) break;


            }

            if (noSameDistrict) break;
        }

        int sameDistrictIndex = 0;
        int nonSameDistrictIndex = 0;
        int checkEvenOdd = 0;

        for (int i = 1; i < applicantSizeFinal + 1; i++) {

            if (i % 2 == checkEvenOdd) {

                if (resultList.size() >= nonSameDistrictIndex + 1) {
                    resultCombined.put(resultList.get(nonSameDistrictIndex).level, i);
                    nonSameDistrictIndex++;
                } else if (allowedSameDistrict.size() >= sameDistrictIndex + 1) {
                    resultCombined.put(allowedSameDistrict.get(sameDistrictIndex), i);
                    sameDistrictIndex++;

                    if (allowedNumberSameDistrict > 0 && allowedSameDistrict.size() > sameDistrictIndex) {
                        resultCombined.put(allowedSameDistrict.get(sameDistrictIndex), i + 1);
                        sameDistrictIndex++;
                        allowedNumberSameDistrict--;
                        i++;
                        checkEvenOdd = 1 - checkEvenOdd;
                    }
                }
            } else {

                if (allowedSameDistrict.size() >= sameDistrictIndex + 1) {
                    resultCombined.put(allowedSameDistrict.get(sameDistrictIndex), i);
                    sameDistrictIndex++;

                    if (allowedNumberSameDistrict > 0 && allowedSameDistrict.size() > sameDistrictIndex) {
                        resultCombined.put(allowedSameDistrict.get(sameDistrictIndex), i + 1);
                        sameDistrictIndex++;
                        allowedNumberSameDistrict--;
                        i++;
                        checkEvenOdd = 1 - checkEvenOdd;
                    }
                } else if (resultList.size() >= nonSameDistrictIndex + 1) {
                    resultCombined.put(resultList.get(nonSameDistrictIndex).level, i);
                    nonSameDistrictIndex++;
                }
            }

        }

        for (Job_applicant_applicationDTO job_applicant_applicationDTO : job_applicant_applicationDTOS) {
            job_applicant_applicationDTO.rollNumber = String.valueOf(10000 + resultCombined.get(((Number) job_applicant_applicationDTO.jobApplicantId).intValue()));
            update(job_applicant_applicationDTO);
        }

    }

    public static HashMap<Integer, Integer> sortByValueInt(HashMap<Integer, Integer> hm) {
        // Create a list from elements of HashMap
        List<Map.Entry<Integer, Integer>> list =
                new LinkedList<Map.Entry<Integer, Integer>>(hm.entrySet());

        // Sort the list
        Collections.sort(list, new Comparator<Map.Entry<Integer, Integer>>() {
            public int compare(Map.Entry<Integer, Integer> o1,
                               Map.Entry<Integer, Integer> o2) {
                return (o1.getValue()).compareTo(o2.getValue());
            }
        });

        // put data from sorted list to hashmap
        HashMap<Integer, Integer> temp = new LinkedHashMap<Integer, Integer>();
        for (Map.Entry<Integer, Integer> aa : list) {
            temp.put(aa.getKey(), aa.getValue());
        }
        return temp;
    }

    public String JobApplicantDistrict(String Language, int id, List<GeoDistrictDTO> getAllDistricts) {

        String districtName = "No district found";
        String districtNameBn = "জেলা অনুপস্থিত";
        GeoDistrictDTO district = getAllDistricts.stream()
                .filter(d -> d.id == id)
                .findFirst()
                .orElse(null);

        if (district != null) {
            districtName = Language.equalsIgnoreCase("English") ? district.nameEng : district.nameBan;
        } else {
            districtName = Language.equalsIgnoreCase("English") ? districtName : districtNameBn;
        }
        return districtName;

    }

    public List<Job_applicant_applicationDTO> getDTOsForGeneratingRoll(long jobId) {
        String sql = "SELECT * FROM job_applicant_application ";
        sql += " WHERE job_id = ? ";
        sql += " AND level = 0 AND is_selected = 0 ";
        sql += " AND isDeleted =  0 ";
        sql += " order by job_applicant_application.lastModificationTime desc";
        printSql(sql);
        return ConnectionAndStatementUtil.getListOfT(sql, Collections.singletonList(jobId), this::buildForGeneratingRoll);
    }

    public Job_applicant_applicationDTO buildForGeneratingRoll(ResultSet rs) {
        try {
            Job_applicant_applicationDTO job_applicant_applicationDTO = new Job_applicant_applicationDTO();
            job_applicant_applicationDTO.iD = rs.getLong("ID");
            job_applicant_applicationDTO.district = rs.getString("district");
            return job_applicant_applicationDTO;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    public void generateRollByPriorityQueue(long jobId) {
        try {
            Job_applicant_applicationDAO job_applicant_applicationDAO = new Job_applicant_applicationDAO();
            List<Job_applicant_applicationDTO> job_applicant_applicationDTOS = job_applicant_applicationDAO.
                    getDTOsForGeneratingRoll(jobId);

            job_applicant_applicationDTOS.forEach(i -> {
                if (i.district == null || i.district.trim().length() == 0) {
                    i.district = "0";
                }
            });

            Map<String, List<Job_applicant_applicationDTO>> applicantByDistrict = job_applicant_applicationDTOS.stream()
                    .collect(Collectors.groupingBy(i -> i.district));
            List<Integer> appCount = applicantByDistrict.values().stream().map(List::size).collect(Collectors.toList());
            int maxCount = appCount.stream().mapToInt(i -> i).max().getAsInt();
            int sum = appCount.stream().mapToInt(i -> i).sum() - maxCount;
            if (maxCount > sum) {
                return;
            }

            Queue<RollGenerateDTO> rollGenerateDTOS = new PriorityQueue<>();
            int uId = 1;
            for (Map.Entry<String, List<Job_applicant_applicationDTO>> entry : applicantByDistrict.entrySet()) {
                RollGenerateDTO rollGenerateDTO = new RollGenerateDTO();
                rollGenerateDTO.iD = uId++;
                rollGenerateDTO.applicantIds = entry.getValue().stream()
                        .map(i -> i.iD).collect(Collectors.toCollection(LinkedList::new));
                rollGenerateDTO.count = rollGenerateDTO.applicantIds.size();
                rollGenerateDTOS.add(rollGenerateDTO);
            }

            int lastDistrictId = -1;
            long rollNumber = jobId * (int) (Math.pow(10, ((maxCount + sum) + "").length())) + 1;

            while (!rollGenerateDTOS.isEmpty()) {
                RollGenerateDTO rollGenerateDTO = rollGenerateDTOS.poll();
                if (rollGenerateDTO.iD != lastDistrictId) {
                    rollGenerateDTO.count = rollGenerateDTO.count - 1;
                    lastDistrictId = rollGenerateDTO.iD;
                    updateRollNumber(rollGenerateDTO.applicantIds.poll(), rollNumber++);
                    if (rollGenerateDTO.count > 0) {
                        rollGenerateDTOS.add(rollGenerateDTO);
                    }
                } else {
                    RollGenerateDTO secondDTO = rollGenerateDTOS.poll();
                    secondDTO.count = secondDTO.count - 1;
                    lastDistrictId = secondDTO.iD;
                    updateRollNumber(secondDTO.applicantIds.poll(), rollNumber++);
                    if (secondDTO.count > 0) {
                        rollGenerateDTOS.add(secondDTO);
                    }
                    rollGenerateDTOS.add(rollGenerateDTO);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateRollNumber(long iD, long rollNumber) {
        ConnectionAndStatementUtil.getWriteStatement(st -> {
            String sql = "UPDATE " + tableName;
            sql += " SET  roll_number = '" + rollNumber;
            sql += "' , lastModificationTime = " + System.currentTimeMillis();
            sql += " WHERE ID = " + iD;
            logger.debug(sql);
            try {
                return st.executeUpdate(sql);
            } catch (SQLException ex) {
                logger.error(ex);
                return 0;
            }
        });
    }

    public void updateSeatPlan(RecruitmentSeatPlanChildDTO recruitmentSeatPlanChildDTO) throws Exception {
        Long jobId = recruitmentSeatPlanChildDTO.recruitmentJobDescriptionId;
        Long level = recruitmentSeatPlanChildDTO.recruitmentJobSpecificExamTypeId;
        Long startRoll = Long.parseLong(recruitmentSeatPlanChildDTO.startRoll);
        Long endRoll = Long.parseLong(recruitmentSeatPlanChildDTO.endRoll);
        List<Job_applicant_applicationDTO> job_applicant_applicationDTOS = Job_applicant_applicationRepository.getInstance()
                .getJob_applicant_applicationDTOByJobIdAndLevel(jobId, level);
        job_applicant_applicationDTOS = job_applicant_applicationDTOS.stream()
                .filter(e -> !e.isRejected).collect(Collectors.toList());
        Long currentTime = System.currentTimeMillis();

        for (Job_applicant_applicationDTO job_applicant_applicationDTO : job_applicant_applicationDTOS) {
            if (Long.parseLong(job_applicant_applicationDTO.rollNumber) >= startRoll && Long.parseLong(job_applicant_applicationDTO.rollNumber) <= endRoll) {
                job_applicant_applicationDTO.seatPlanId = recruitmentSeatPlanChildDTO.recruitmentSeatPlanId;
                job_applicant_applicationDTO.seatPlanChildId = recruitmentSeatPlanChildDTO.iD;
                job_applicant_applicationDTO.recruitmentExamVenueId = recruitmentSeatPlanChildDTO.recruitmentExamVenueId;
                job_applicant_applicationDTO.recruitmentExamVenueItemId = recruitmentSeatPlanChildDTO.recruitmentExamVenueItemId;
                job_applicant_applicationDTO.lastModificationTime = currentTime;
                update(job_applicant_applicationDTO);
            }
        }

    }
}
	