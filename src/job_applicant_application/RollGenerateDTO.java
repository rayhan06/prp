package job_applicant_application;

import java.util.Queue;

public class RollGenerateDTO implements Comparable<RollGenerateDTO> {
    public int iD = -1;
    public Queue<Long> applicantIds;
    public int count = 0;

    @Override
    public int compareTo(RollGenerateDTO o1) {
        return o1.count > this.count ? 1 : -1;
    }
}
