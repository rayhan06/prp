package job_applicant_application;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import com.google.gson.Gson;
import org.apache.log4j.Logger;

import recruitment_bulk_mark_entry.Recruitment_bulk_mark_entryDTO;
import repository.Repository;
import repository.RepositoryManager;


public class Job_applicant_applicationRepository implements Repository {
	Job_applicant_applicationDAO job_applicant_applicationDAO = null;
	Gson gson;
	
	public void setDAO(Job_applicant_applicationDAO job_applicant_applicationDAO)
	{
		this.job_applicant_applicationDAO = job_applicant_applicationDAO;
		gson = new Gson();
	}
	
	
	static Logger logger = Logger.getLogger(Job_applicant_applicationRepository.class);
	Map<Long, Job_applicant_applicationDTO>mapOfJob_applicant_applicationDTOToiD;
	Map<Long, Set<Job_applicant_applicationDTO> >mapOfJob_applicant_applicationDTOTojobApplicantId;
	Map<Long, Set<Job_applicant_applicationDTO> >mapOfJob_applicant_applicationDTOTojobId;
//	Map<Long, Set<Job_applicant_applicationDTO> >mapOfJob_applicant_applicationDTOToacceptanceStatus;
//	Map<Long, Set<Job_applicant_applicationDTO> >mapOfJob_applicant_applicationDTOToinsertionDate;
//	Map<Long, Set<Job_applicant_applicationDTO> >mapOfJob_applicant_applicationDTOToinsertedByUserId;
//	Map<String, Set<Job_applicant_applicationDTO> >mapOfJob_applicant_applicationDTOTomodifiedBy;
//	Map<Long, Set<Job_applicant_applicationDTO> >mapOfJob_applicant_applicationDTOTolastModificationTime;
//	Map<String, Set<Job_applicant_applicationDTO> >mapOfJob_applicant_applicationDTOTorollNumber;


	static Job_applicant_applicationRepository instance = null;  
	private Job_applicant_applicationRepository(){
		mapOfJob_applicant_applicationDTOToiD = new ConcurrentHashMap<>();
		mapOfJob_applicant_applicationDTOTojobApplicantId = new ConcurrentHashMap<>();
		mapOfJob_applicant_applicationDTOTojobId = new ConcurrentHashMap<>();
//		mapOfJob_applicant_applicationDTOToacceptanceStatus = new ConcurrentHashMap<>();
//		mapOfJob_applicant_applicationDTOToinsertionDate = new ConcurrentHashMap<>();
//		mapOfJob_applicant_applicationDTOToinsertedByUserId = new ConcurrentHashMap<>();
//		mapOfJob_applicant_applicationDTOTomodifiedBy = new ConcurrentHashMap<>();
//		mapOfJob_applicant_applicationDTOTolastModificationTime = new ConcurrentHashMap<>();
//		mapOfJob_applicant_applicationDTOTorollNumber = new ConcurrentHashMap<>();

		setDAO(new Job_applicant_applicationDAO());
		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Job_applicant_applicationRepository getInstance(){
		if (instance == null){
			instance = new Job_applicant_applicationRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(job_applicant_applicationDAO == null)
		{
			return;
		}
		try {
			List<Job_applicant_applicationDTO> job_applicant_applicationDTOs = job_applicant_applicationDAO.getAllJob_applicant_application(reloadAll);
			for(Job_applicant_applicationDTO job_applicant_applicationDTO : job_applicant_applicationDTOs) {
				Job_applicant_applicationDTO oldJob_applicant_applicationDTO = getJob_applicant_applicationDTOByIDWithoutClone(job_applicant_applicationDTO.iD);
				if( oldJob_applicant_applicationDTO != null ) {
					mapOfJob_applicant_applicationDTOToiD.remove(oldJob_applicant_applicationDTO.iD);
				
					if(mapOfJob_applicant_applicationDTOTojobApplicantId.containsKey(oldJob_applicant_applicationDTO.jobApplicantId)) {
						mapOfJob_applicant_applicationDTOTojobApplicantId.get(oldJob_applicant_applicationDTO.jobApplicantId).remove(oldJob_applicant_applicationDTO);
					}
					if(mapOfJob_applicant_applicationDTOTojobApplicantId.get(oldJob_applicant_applicationDTO.jobApplicantId).isEmpty()) {
						mapOfJob_applicant_applicationDTOTojobApplicantId.remove(oldJob_applicant_applicationDTO.jobApplicantId);
					}
					
					if(mapOfJob_applicant_applicationDTOTojobId.containsKey(oldJob_applicant_applicationDTO.jobId)) {
						mapOfJob_applicant_applicationDTOTojobId.get(oldJob_applicant_applicationDTO.jobId).remove(oldJob_applicant_applicationDTO);
					}
					if(mapOfJob_applicant_applicationDTOTojobId.get(oldJob_applicant_applicationDTO.jobId).isEmpty()) {
						mapOfJob_applicant_applicationDTOTojobId.remove(oldJob_applicant_applicationDTO.jobId);
					}
					
//					if(mapOfJob_applicant_applicationDTOToacceptanceStatus.containsKey(oldJob_applicant_applicationDTO.acceptanceStatus)) {
//						mapOfJob_applicant_applicationDTOToacceptanceStatus.get(oldJob_applicant_applicationDTO.acceptanceStatus).remove(oldJob_applicant_applicationDTO);
//					}
//					if(mapOfJob_applicant_applicationDTOToacceptanceStatus.get(oldJob_applicant_applicationDTO.acceptanceStatus).isEmpty()) {
//						mapOfJob_applicant_applicationDTOToacceptanceStatus.remove(oldJob_applicant_applicationDTO.acceptanceStatus);
//					}
//
//					if(mapOfJob_applicant_applicationDTOToinsertionDate.containsKey(oldJob_applicant_applicationDTO.insertionDate)) {
//						mapOfJob_applicant_applicationDTOToinsertionDate.get(oldJob_applicant_applicationDTO.insertionDate).remove(oldJob_applicant_applicationDTO);
//					}
//					if(mapOfJob_applicant_applicationDTOToinsertionDate.get(oldJob_applicant_applicationDTO.insertionDate).isEmpty()) {
//						mapOfJob_applicant_applicationDTOToinsertionDate.remove(oldJob_applicant_applicationDTO.insertionDate);
//					}
//
//					if(mapOfJob_applicant_applicationDTOToinsertedByUserId.containsKey(oldJob_applicant_applicationDTO.insertedByUserId)) {
//						mapOfJob_applicant_applicationDTOToinsertedByUserId.get(oldJob_applicant_applicationDTO.insertedByUserId).remove(oldJob_applicant_applicationDTO);
//					}
//					if(mapOfJob_applicant_applicationDTOToinsertedByUserId.get(oldJob_applicant_applicationDTO.insertedByUserId).isEmpty()) {
//						mapOfJob_applicant_applicationDTOToinsertedByUserId.remove(oldJob_applicant_applicationDTO.insertedByUserId);
//					}
//
//					if(mapOfJob_applicant_applicationDTOTomodifiedBy.containsKey(oldJob_applicant_applicationDTO.modifiedBy)) {
//						mapOfJob_applicant_applicationDTOTomodifiedBy.get(oldJob_applicant_applicationDTO.modifiedBy).remove(oldJob_applicant_applicationDTO);
//					}
//					if(mapOfJob_applicant_applicationDTOTomodifiedBy.get(oldJob_applicant_applicationDTO.modifiedBy).isEmpty()) {
//						mapOfJob_applicant_applicationDTOTomodifiedBy.remove(oldJob_applicant_applicationDTO.modifiedBy);
//					}
//
//					if(mapOfJob_applicant_applicationDTOTolastModificationTime.containsKey(oldJob_applicant_applicationDTO.lastModificationTime)) {
//						mapOfJob_applicant_applicationDTOTolastModificationTime.get(oldJob_applicant_applicationDTO.lastModificationTime).remove(oldJob_applicant_applicationDTO);
//					}
//					if(mapOfJob_applicant_applicationDTOTolastModificationTime.get(oldJob_applicant_applicationDTO.lastModificationTime).isEmpty()) {
//						mapOfJob_applicant_applicationDTOTolastModificationTime.remove(oldJob_applicant_applicationDTO.lastModificationTime);
//					}
//
//					if(mapOfJob_applicant_applicationDTOTorollNumber.containsKey(oldJob_applicant_applicationDTO.rollNumber)) {
//						mapOfJob_applicant_applicationDTOTorollNumber.get(oldJob_applicant_applicationDTO.rollNumber).remove(oldJob_applicant_applicationDTO);
//					}
//					if(mapOfJob_applicant_applicationDTOTorollNumber.get(oldJob_applicant_applicationDTO.rollNumber).isEmpty()) {
//						mapOfJob_applicant_applicationDTOTorollNumber.remove(oldJob_applicant_applicationDTO.rollNumber);
//					}
					
					
				}
				if(job_applicant_applicationDTO.isDeleted == 0) 
				{
					
					mapOfJob_applicant_applicationDTOToiD.put(job_applicant_applicationDTO.iD, job_applicant_applicationDTO);
				
					if( ! mapOfJob_applicant_applicationDTOTojobApplicantId.containsKey(job_applicant_applicationDTO.jobApplicantId)) {
						mapOfJob_applicant_applicationDTOTojobApplicantId.put(job_applicant_applicationDTO.jobApplicantId, new HashSet<>());
					}
					mapOfJob_applicant_applicationDTOTojobApplicantId.get(job_applicant_applicationDTO.jobApplicantId).add(job_applicant_applicationDTO);
					
					if( ! mapOfJob_applicant_applicationDTOTojobId.containsKey(job_applicant_applicationDTO.jobId)) {
						mapOfJob_applicant_applicationDTOTojobId.put(job_applicant_applicationDTO.jobId, new HashSet<>());
					}
					mapOfJob_applicant_applicationDTOTojobId.get(job_applicant_applicationDTO.jobId).add(job_applicant_applicationDTO);
					
//					if( ! mapOfJob_applicant_applicationDTOToacceptanceStatus.containsKey(job_applicant_applicationDTO.acceptanceStatus)) {
//						mapOfJob_applicant_applicationDTOToacceptanceStatus.put(job_applicant_applicationDTO.acceptanceStatus, new HashSet<>());
//					}
//					mapOfJob_applicant_applicationDTOToacceptanceStatus.get(job_applicant_applicationDTO.acceptanceStatus).add(job_applicant_applicationDTO);
//
//					if( ! mapOfJob_applicant_applicationDTOToinsertionDate.containsKey(job_applicant_applicationDTO.insertionDate)) {
//						mapOfJob_applicant_applicationDTOToinsertionDate.put(job_applicant_applicationDTO.insertionDate, new HashSet<>());
//					}
//					mapOfJob_applicant_applicationDTOToinsertionDate.get(job_applicant_applicationDTO.insertionDate).add(job_applicant_applicationDTO);
//
//					if( ! mapOfJob_applicant_applicationDTOToinsertedByUserId.containsKey(job_applicant_applicationDTO.insertedByUserId)) {
//						mapOfJob_applicant_applicationDTOToinsertedByUserId.put(job_applicant_applicationDTO.insertedByUserId, new HashSet<>());
//					}
//					mapOfJob_applicant_applicationDTOToinsertedByUserId.get(job_applicant_applicationDTO.insertedByUserId).add(job_applicant_applicationDTO);
//
//					if( ! mapOfJob_applicant_applicationDTOTomodifiedBy.containsKey(job_applicant_applicationDTO.modifiedBy)) {
//						mapOfJob_applicant_applicationDTOTomodifiedBy.put(job_applicant_applicationDTO.modifiedBy, new HashSet<>());
//					}
//					mapOfJob_applicant_applicationDTOTomodifiedBy.get(job_applicant_applicationDTO.modifiedBy).add(job_applicant_applicationDTO);
//
//					if( ! mapOfJob_applicant_applicationDTOTolastModificationTime.containsKey(job_applicant_applicationDTO.lastModificationTime)) {
//						mapOfJob_applicant_applicationDTOTolastModificationTime.put(job_applicant_applicationDTO.lastModificationTime, new HashSet<>());
//					}
//					mapOfJob_applicant_applicationDTOTolastModificationTime.get(job_applicant_applicationDTO.lastModificationTime).add(job_applicant_applicationDTO);
//
//					if( ! mapOfJob_applicant_applicationDTOTorollNumber.containsKey(job_applicant_applicationDTO.rollNumber)) {
//						mapOfJob_applicant_applicationDTOTorollNumber.put(job_applicant_applicationDTO.rollNumber, new HashSet<>());
//					}
//					mapOfJob_applicant_applicationDTOTorollNumber.get(job_applicant_applicationDTO.rollNumber).add(job_applicant_applicationDTO);
//
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}

	public Job_applicant_applicationDTO clone(Job_applicant_applicationDTO dto) {
		String raw = gson.toJson(dto);
		return gson.fromJson(raw, Job_applicant_applicationDTO.class);
	}

	public List<Job_applicant_applicationDTO> clone(List<Job_applicant_applicationDTO> dtoList) {
		return dtoList
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}

	public Job_applicant_applicationDTO getJob_applicant_applicationDTOByIDWithoutClone( long ID){
		return mapOfJob_applicant_applicationDTOToiD.get(ID);
	}
	
	public List<Job_applicant_applicationDTO> getJob_applicant_applicationList() {
		List <Job_applicant_applicationDTO> job_applicant_applications = new ArrayList<Job_applicant_applicationDTO>(this.mapOfJob_applicant_applicationDTOToiD.values());
		return job_applicant_applications;
	}
	
	
	public Job_applicant_applicationDTO getJob_applicant_applicationDTOByID( long ID){
		return clone(mapOfJob_applicant_applicationDTOToiD.get(ID));
	}
	
	
	public List<Job_applicant_applicationDTO> getJob_applicant_applicationDTOByjob_applicant_id(long job_applicant_id) {
		return new ArrayList<>( mapOfJob_applicant_applicationDTOTojobApplicantId.getOrDefault(job_applicant_id,new HashSet<>()));
	}
	
	
	public List<Job_applicant_applicationDTO> getJob_applicant_applicationDTOByjob_id(long job_id) {
		return new ArrayList<>( mapOfJob_applicant_applicationDTOTojobId.getOrDefault(job_id,new HashSet<>()));
	}

	public List<Job_applicant_applicationDTO> getJob_applicant_applicationDTOByJobIdAndLevelAndRollNumber(long job_id, long level,String rollNum) {
		List<Job_applicant_applicationDTO> job_applicant_applicationDTOS = (new ArrayList<>( mapOfJob_applicant_applicationDTOTojobId.getOrDefault(job_id,new HashSet<>())));
		return job_applicant_applicationDTOS
				.stream()
				.filter(job_applicant_applicationDTO -> job_applicant_applicationDTO.level == level)
				.filter(job_applicant_applicationDTO -> job_applicant_applicationDTO.rollNumber.equals(rollNum))
				.map(job_applicant_applicationDTO -> clone(job_applicant_applicationDTO))
				.collect(Collectors.toList());
	}

	public List<Job_applicant_applicationDTO> getJob_applicant_applicationDTOByJobIdAndLevel(long job_id, long level) {
		List<Job_applicant_applicationDTO> job_applicant_applicationDTOS = (new ArrayList<>( mapOfJob_applicant_applicationDTOTojobId.getOrDefault(job_id,new HashSet<>())));
		return job_applicant_applicationDTOS
				.stream()
				.filter(job_applicant_applicationDTO -> job_applicant_applicationDTO.level == level)
				.map(job_applicant_applicationDTO -> clone(job_applicant_applicationDTO))
				.collect(Collectors.toList());
	}
	
	
//	public List<Job_applicant_applicationDTO> getJob_applicant_applicationDTOByacceptance_status(long acceptance_status) {
//		return new ArrayList<>( mapOfJob_applicant_applicationDTOToacceptanceStatus.getOrDefault(acceptance_status,new HashSet<>()));
//	}
//
//
//	public List<Job_applicant_applicationDTO> getJob_applicant_applicationDTOByinsertion_date(long insertion_date) {
//		return new ArrayList<>( mapOfJob_applicant_applicationDTOToinsertionDate.getOrDefault(insertion_date,new HashSet<>()));
//	}
//
//
//	public List<Job_applicant_applicationDTO> getJob_applicant_applicationDTOByinserted_by_user_id(long inserted_by_user_id) {
//		return new ArrayList<>( mapOfJob_applicant_applicationDTOToinsertedByUserId.getOrDefault(inserted_by_user_id,new HashSet<>()));
//	}
//
//
//	public List<Job_applicant_applicationDTO> getJob_applicant_applicationDTOBymodified_by(String modified_by) {
//		return new ArrayList<>( mapOfJob_applicant_applicationDTOTomodifiedBy.getOrDefault(modified_by,new HashSet<>()));
//	}
//
//
//	public List<Job_applicant_applicationDTO> getJob_applicant_applicationDTOBylastModificationTime(long lastModificationTime) {
//		return new ArrayList<>( mapOfJob_applicant_applicationDTOTolastModificationTime.getOrDefault(lastModificationTime,new HashSet<>()));
//	}
//
//
//	public List<Job_applicant_applicationDTO> getJob_applicant_applicationDTOByroll_number(String roll_number) {
//		return new ArrayList<>( mapOfJob_applicant_applicationDTOTorollNumber.getOrDefault(roll_number,new HashSet<>()));
//	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "job_applicant_application";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


