package job_applicant_application;
import java.util.*; 
import util.*; 


public class Job_applicant_applicationDTO extends CommonDTO
{

	public long jobApplicantId = 0;
	public long jobId = 0;
	public long acceptanceStatus = 0;
	public long insertionDate = 0;
	public long insertedByUserId = 0;
    public String modifiedBy = "";
    public String rollNumber = "";
    public String summary = "";
    public String trackingNumber = "";
    public int level = 0;
    public boolean isViewed = false;
    public boolean isRejected = false;
    public int isSelected = 0;
    public String decision_remarks = "";

    public int isPresent = 0 ;
    public int count = 0;

    public int quota_id = 0 ;
    public double marks = -1;
    public String prev_level_data = "";

    public String applicant_name_bn = "";
    public String applicant_name_en = "";
    public String present_address_name_bn = "";
    public String present_address_name_en = "";
    public String permanent_address_name_bn = "";
    public String permanent_address_name_en = "";
    public String district_name_bn = "";
    public String district_name_en = "";
    public String applicant_dob_en = "";
    public String applicant_dob_bn = "";
    public String applicant_age_en = "";
    public String applicant_age_bn = "";
    public String applicant_nid = "";
    public String quota_en = "";
    public String quota_bn = "";
    public String annual_en = "";
    public String annual_bn = "";
    public String priviledge_en = "";
    public String priviledge_bn = "";
    public String edu_info_en = "";
    public String edu_info_bn = "";
    public String job_experience_en = "";
    public String job_experience_bn = "";
    public long photo_file_id = 0;
    public String father_name = "";
    public String mother_name = "";
    public long highest_edu_level = 0;

    public String district = "";
    public String division = "";

    public int age_in_years = 0;
    public int qualification_in_years = 0;
    public String university_ids = "";

    public long seatPlanId ;
    public long seatPlanChildId ;
    public long recruitmentExamVenueId ;
    public long recruitmentExamVenueItemId ;

    public int isAdmitCardDownloaded = 0;

	
    @Override
	public String toString() {
            return "$Job_applicant_applicationDTO[" +
            " iD = " + iD +
            " jobApplicantId = " + jobApplicantId +
            " jobId = " + jobId +
            " acceptanceStatus = " + acceptanceStatus +
            " insertionDate = " + insertionDate +
            " insertedByUserId = " + insertedByUserId +
            " modifiedBy = " + modifiedBy +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            " rollNumber = " + rollNumber +
            " summary = " + summary +
            " isPresent = " + isPresent +
            " marks = " + marks +
            " prev_level_data = " + prev_level_data +
            " district  = " + district  +
            " division  = " + division  +
            "]";
    }

}