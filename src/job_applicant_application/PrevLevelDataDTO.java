package job_applicant_application;

public class PrevLevelDataDTO
{

    public int level = 0;
    public String decision_remarks = "";
    public int isPresent = 0;
    public double marks = -1;

	
    @Override
	public String toString() {
            return "PrevLevelDataDTO[" +
            " level = " + level +
            " decision_remarks = " + decision_remarks +
            " isPresent = " + isPresent +
            " marks = " + marks +
            "]";
    }

}