package job_applicant_application;

import files.FilesDTO;
import job_applicant_certification.Job_applicant_certificationDTO;
import job_applicant_certification.Job_applicant_certificationSummaryDTO;
import job_applicant_documents.Job_applicant_documentsDTO;
import job_applicant_documents.Job_applicant_documentsSummaryDTO;
import job_applicant_education_info.Job_applicant_education_infoDTO;
import job_applicant_education_info.Job_applicant_education_infoSummaryDTO;
import job_applicant_photo_signature.Job_applicant_photo_signatureDTO;
import job_applicant_photo_signature.Job_applicant_photo_signatureSummaryDTO;
import job_applicant_professional_info.Job_applicant_professional_infoDTO;
import job_applicant_professional_info.Job_applicant_professional_infoSummaryDTO;
import job_applicant_qualifications.Job_applicant_qualificationSummaryDTO;
import job_applicant_reference.Job_applicant_referenceDTO;
import job_applicant_reference.Job_applicant_referenceSummaryDTO;
import parliament_job_applicant.Parliament_job_applicantDTO;
import util.CommonDTO;

import java.util.List;


public class SummaryDTO
{

	public Parliament_job_applicantDTO parliament_job_applicantDTO;
	public List<Job_applicant_certificationSummaryDTO> job_applicant_certificationSummaryDTOList;
	public List<Job_applicant_photo_signatureSummaryDTO> job_applicant_photo_signatureSummaryDTOList;
	public List<Job_applicant_education_infoSummaryDTO> job_applicant_education_infoSummaryDTOList;
	public List<Job_applicant_professional_infoSummaryDTO> job_applicant_professional_infoSummaryDTOList;
	public List<Job_applicant_documentsSummaryDTO> job_applicant_documentsSummaryDTOList;
	public List<Job_applicant_referenceSummaryDTO> job_applicant_referenceSummaryDTOList;
	public List<Job_applicant_qualificationSummaryDTO> job_applicant_qualificationSummaryDTOList;
	public long totalExperience = 0;
	public String applicant_name_bn = "";
	public String applicant_name_en = "";
	public String present_address_name_bn = "";
	public String present_address_name_en = "";
	public String permanent_address_name_bn = "";
	public String permanent_address_name_en = "";
	public String district_name_bn = "";
	public String district_name_en = "";
	public String applicant_dob_en = "";
	public String applicant_dob_bn = "";
	public String applicant_age_en = "";
	public String applicant_age_bn = "";
	public String applicant_nid = "";
	public String quota_en = "";
	public String quota_bn = "";
	public String annual_en = "";
	public String annual_bn = "";
	public String priviledge_en = "";
	public String priviledge_bn = "";
	public String edu_info_en = "";
	public String edu_info_bn = "";
	public String job_experience_en = "";
	public String job_experience_bn = "";
	public String chalan = "";
	public String photo_file_id = "";

}