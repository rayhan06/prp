package job_applicant_application;

import recruitment_exam_venue.RecruitmentExamVenueItemDTO;
import recruitment_seat_plan.RecruitmentSeatPlanChildDTO;

public class JobApplicantApplicationModel {

    public String startRoll = "";
    public String endRoll = "";




    public JobApplicantApplicationModel(){ }
    public JobApplicantApplicationModel(Job_applicant_applicationDTO dto, String language){
        boolean isLanguageEnglish = "English".equalsIgnoreCase(language);
    }

    public JobApplicantApplicationModel(String sRoll, String eRoll, String language){
        boolean isLanguageEnglish = "English".equalsIgnoreCase(language);
        String sRoll1 = sRoll.equals("-1")? isLanguageEnglish?"No roll found":"কোনো রোল নম্বর পাওয়া যায়নি":sRoll;
        String eRoll1 = eRoll.equals("-1")? isLanguageEnglish?"No roll found":"কোনো রোল নম্বর পাওয়া যায়নি":eRoll;
        startRoll = sRoll1;
        endRoll = eRoll1;

    }


    @Override
    public String toString() {
        return "JobApplicantApplicationModel{" +
                ", startRoll='" + startRoll + '\'' +
                ", endRoll='" + endRoll + '\'' +
                '}';
    }
}
