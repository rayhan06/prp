package job_applicant_application;

import com.google.gson.Gson;
import files.FilesDAO;
import files.FilesDTO;
import job_applicant_certification.Job_applicant_certificationDAO;
import job_applicant_certification.Job_applicant_certificationDTO;
import job_applicant_certification.Job_applicant_certificationSummaryDTO;
import job_applicant_documents.Job_applicant_documentsDAO;
import job_applicant_documents.Job_applicant_documentsDTO;
import job_applicant_documents.Job_applicant_documentsSummaryDTO;
import job_applicant_education_info.Job_applicant_education_infoDAO;
import job_applicant_education_info.Job_applicant_education_infoDTO;
import job_applicant_education_info.Job_applicant_education_infoSummaryDTO;
import job_applicant_photo_signature.Job_applicant_photo_signatureDAO;
import job_applicant_photo_signature.Job_applicant_photo_signatureDTO;
import job_applicant_photo_signature.Job_applicant_photo_signatureSummaryDTO;
import job_applicant_professional_info.Job_applicant_professional_infoDAO;
import job_applicant_professional_info.Job_applicant_professional_infoDTO;
import job_applicant_professional_info.Job_applicant_professional_infoSummaryDTO;
import job_applicant_reference.Job_applicant_referenceDAO;
import job_applicant_reference.Job_applicant_referenceDTO;
import job_applicant_reference.Job_applicant_referenceSummaryDTO;
import login.LoginDTO;
import mail.EmailService;
import mail.SendEmailDTO;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import parliament_job_applicant.Parliament_job_applicantDAO;
import parliament_job_applicant.Parliament_job_applicantDTO;
import parliament_job_applicant.Parliament_job_applicantRepository;
import pb.Utils;
import permission.MenuConstants;
import recruitment_exam_venue.RecruitmentExamVenueItemModel;
import recruitment_job_description.Recruitment_job_descriptionDAO;
import recruitment_job_description.Recruitment_job_descriptionDTO;
import recruitment_job_description.Recruitment_job_descriptionRepository;
import recruitment_job_specific_exam_type.JobSpecificExamTypeDao;
import role.PermissionRepository;
import sessionmanager.SessionConstants;
import sms.SmsService;
import user.UserDTO;
import user.UserRepository;
import util.*;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.stream.Collectors;


/**
 * Servlet implementation class Job_applicant_applicationServlet
 */
@WebServlet("/Job_applicant_applicationServlet")
@MultipartConfig
public class Job_applicant_applicationServlet extends HttpServlet
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Job_applicant_applicationServlet.class);

    String tableName = "job_applicant_application";

	Job_applicant_applicationDAO job_applicant_applicationDAO;
	CommonRequestHandler commonRequestHandler;
    private final Gson gson = new Gson();
//	String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Job_applicant_applicationServlet()
	{
        super();
    	try
    	{
			job_applicant_applicationDAO = new Job_applicant_applicationDAO(tableName);
			commonRequestHandler = new CommonRequestHandler(job_applicant_applicationDAO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		System.out.println("In doget request = " + request);
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		try
		{
			String actionType = request.getParameter("actionType");
			if(actionType.equals("getAddPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.JOB_APPLICANT_APPLICATION_ADD))
				{
					commonRequestHandler.getAddPage(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getEditPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.JOB_APPLICANT_APPLICATION_UPDATE))
				{
					getJob_applicant_application(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getSummaryPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.JOB_APPLICANT_APPLICATION_UPDATE))
				{
					getJob_applicant_applicationSummary(request, response, Long.parseLong(request.getParameter("ID")));
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getURL"))
			{
				String URL = request.getParameter("URL");
				System.out.println("URL = " + URL);
				response.sendRedirect(URL);
			}


			else if(actionType.equals("validateShortListCountForBulkDecision")){
				long jobId = Long.parseLong(request.getParameter("jobId"));
				int level = Integer.parseInt(request.getParameter("level"));
				int shortListCount = Integer.parseInt(request.getParameter("shortListCount"));
				int nextLevel = new JobSpecificExamTypeDao().
						getNextLevelFromJobIdAndCurrentLevel(jobId, level);
				boolean flag = true;
				if(nextLevel > 0){
				} else {
					Recruitment_job_descriptionDTO jobDescriptionDTO = Recruitment_job_descriptionRepository.getInstance().
							getRecruitment_job_descriptionDTOByID(jobId);
					CandidateCountDTO candidateCountDTO = job_applicant_applicationDAO.getApplicantCount(jobId);
					//System.out.println("here sev short1: "+candidateCountDTO.accepted + shortListCount);
					if(candidateCountDTO.accepted + shortListCount > jobDescriptionDTO.numberOfVacancy){
						//System.out.println("here sev short2: "+candidateCountDTO.accepted + shortListCount);
						flag = false;
					}
				}

				PrintWriter out = response.getWriter();
				out.print(flag);
				out.flush();
			}


			else if(actionType.equals("validationForShortList")){
				long jobAppAppId = Long.parseLong(request.getParameter("jobAppAppId"));
				Job_applicant_applicationDTO dto = Job_applicant_applicationRepository.getInstance().
						getJob_applicant_applicationDTOByID(jobAppAppId);
//				job_applicant_applicationDAO.getDTOByID(jobAppAppId);
				int nextLevel = new JobSpecificExamTypeDao().
						getNextLevelFromJobIdAndCurrentLevel(dto.jobId, dto.level);
				boolean flag = true;
				if(nextLevel > 0){
				} else {
					Recruitment_job_descriptionDTO jobDescriptionDTO = Recruitment_job_descriptionRepository.getInstance().
							getRecruitment_job_descriptionDTOByID(dto.jobId);
					CandidateCountDTO candidateCountDTO = job_applicant_applicationDAO.getApplicantCount(dto.jobId);
					if(candidateCountDTO.accepted >= jobDescriptionDTO.numberOfVacancy){
						flag = false;
					}
				}

				PrintWriter out = response.getWriter();
				out.print(flag);
				out.flush();


			}
			else if(actionType.equals("search"))
			{
				System.out.println("search requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.JOB_APPLICANT_APPLICATION_SEARCH))
				{
					if(isPermanentTable)
					{
						String filter = request.getParameter("filter");
						System.out.println("filter = " + filter);
						if(filter!=null)
						{
							filter = ""; //shouldn't be directly used, rather manipulate it.
							searchJob_applicant_application(request, response, isPermanentTable, filter);
						}
						else
						{
							searchJob_applicant_application(request, response, isPermanentTable, "");
						}
					}
					else
					{
						//searchJob_applicant_application(request, response, tempTableName, isPermanentTable);
					}
				}
			}
			else if(actionType.equals("view"))
			{
				System.out.println("view requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.JOB_APPLICANT_APPLICATION_SEARCH))
				{
					commonRequestHandler.view(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}

			else
			{
				request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}





	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		// TODO Auto-generated method stub
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		System.out.println("doPost");
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);

		try
		{
			String actionType = request.getParameter("actionType");
			System.out.println("actionType = " + actionType);
			if(actionType.equals("add"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.JOB_APPLICANT_APPLICATION_ADD))
				{
					System.out.println("going to  addJob_applicant_application ");
					addJob_applicant_application(request, response, true, userDTO, true);
				}
				else
				{
					System.out.println("Not going to  addJob_applicant_application ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}

			else if(actionType.equals("generateRoll"))
			{
				generateRoll(request);
			}

			else if(actionType.equals("makeShortlist")){

				Utils.handleTransactionForNavigationService4(()->{
					Long Id = Long.parseLong(request.getParameter("id"));
					String remarks = request.getParameter("remarks");
					Job_applicant_applicationDTO dto = Job_applicant_applicationRepository.
							getInstance().getJob_applicant_applicationDTOByID(Id);
//						job_applicant_applicationDAO.getDTOByID(Id);
					// check last step or not
					int nextLevel = new JobSpecificExamTypeDao().
							getNextLevelFromJobIdAndCurrentLevel(dto.jobId, dto.level);
					if(nextLevel > 0){
						// next level data add
						deleteNextLevelData(dto);
						duplicateEntryWithNextLevel(dto, nextLevel, userDTO);
						dto.isSelected = 1;
					} else {
						dto.isSelected = 2;
					}

					dto.decision_remarks = remarks;
					dto.isRejected = false;
					job_applicant_applicationDAO.update(dto);

					// send msg
					Parliament_job_applicantDTO jobApplicantDTO = Parliament_job_applicantRepository.getInstance()
							.getParliament_job_applicantDTOByID(dto.jobApplicantId);
//						new Parliament_job_applicantDAO().
//						getDTOByID(dto.jobApplicantId);
					Recruitment_job_descriptionDTO jobDescriptionDTO =
							Recruitment_job_descriptionRepository.getInstance().getRecruitment_job_descriptionDTOByID(dto.jobId);
					String msg = "";
					if(dto.isSelected == 1){
						msg = "You have been shortlisted for next level of " + jobDescriptionDTO.jobTitleEn + " post.";
					} else if(dto.isSelected == 2){
						msg = "You have been final-shortlisted for the post of " + jobDescriptionDTO.jobTitleEn + ".";
					}
					SmsService.send(jobApplicantDTO.contactNumber,
							String.format("Dear %s, " + msg, jobApplicantDTO.nameEn ));
					sendMail(jobApplicantDTO.email, String.format("Dear %s, " + msg, jobApplicantDTO.nameEn ),
							"Parliament Recruitment");
				});



			}

			else if(actionType.equals("makeRejected")){
				Long Id = Long.parseLong(request.getParameter("id"));
				String remarks = request.getParameter("remarks");
				Job_applicant_applicationDTO dto = Job_applicant_applicationRepository.
						getInstance().getJob_applicant_applicationDTOByID(Id);
//				job_applicant_applicationDAO.getDTOByID(Id);
				dto.decision_remarks = remarks;
				dto.isRejected = true;
				dto.isSelected = 0;
				job_applicant_applicationDAO.update(dto);

				// next steps data delete
				deleteNextLevelData(dto);
			}

			else if(actionType.equals("makeViewed")){
				Long Id = Long.parseLong(request.getParameter("id"));
				Job_applicant_applicationDTO dto = Job_applicant_applicationRepository.
						getInstance().getJob_applicant_applicationDTOByID(Id);
//				job_applicant_applicationDAO.getDTOByID(Id);
				dto.isViewed = true;
				job_applicant_applicationDAO.update(dto);
			}

			else if(actionType.equals("getDTO"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.JOB_APPLICANT_APPLICATION_ADD))
				{
					getDTO(request, response);
				}
				else
				{
					System.out.println("Not going to  addJob_applicant_application ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}
			else if(actionType.equals("edit"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.JOB_APPLICANT_APPLICATION_UPDATE))
				{
					addJob_applicant_application(request, response, false, userDTO, isPermanentTable);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("delete"))
			{
				deleteJob_applicant_application(request, response, userDTO);
			}
			else if(actionType.equals("search"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.JOB_APPLICANT_APPLICATION_SEARCH))
				{
					searchJob_applicant_application(request, response, true, "");
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getGeo"))
			{
				System.out.println("going to geoloc ");
				request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
			}

		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}

	public void deleteNextLevelData(Job_applicant_applicationDTO dto) throws Exception {
		List<Integer> nextLevels = new JobSpecificExamTypeDao().
				getNextLevelsFromJobIdAndCurrentLevel(dto.jobId, dto.level);

		for(int level: nextLevels){
			List<Job_applicant_applicationDTO> nextLevelDTOs = job_applicant_applicationDAO.
					getDTOsByJobIDAndLevelAndApplicantId(dto.jobId, level, dto.jobApplicantId);
			for(Job_applicant_applicationDTO nextDTO: nextLevelDTOs){
				nextDTO.isDeleted = 1;
				job_applicant_applicationDAO.delete(nextDTO.iD);
			}
		}

	}

	private long getCurrentTimeInMilliSeconds(){
		Calendar c = Calendar.getInstance();
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
		return c.getTimeInMillis();
	}

	public void duplicateEntryWithNextLevel(Job_applicant_applicationDTO dto, int level, UserDTO userDTO) throws Exception {
		Job_applicant_applicationDTO newDTO = new Job_applicant_applicationDTO();
		newDTO.jobApplicantId = dto.jobApplicantId;
		newDTO.jobId = dto.jobId;
		newDTO.level = level;
		newDTO.summary = dto.summary;
		newDTO.insertionDate = getCurrentTimeInMilliSeconds();
		newDTO.insertedByUserId = userDTO.ID;
		newDTO.rollNumber = dto.rollNumber;
		newDTO.quota_id = dto.quota_id;
		newDTO.qualification_in_years = dto.qualification_in_years;
		newDTO.age_in_years = dto.age_in_years;
		newDTO.university_ids = dto.university_ids;


		newDTO.applicant_name_bn = dto.applicant_name_bn;
		newDTO.applicant_name_en = dto.applicant_name_en;
		newDTO.present_address_name_bn = dto.present_address_name_bn;
		newDTO.present_address_name_en = dto.present_address_name_en;
		newDTO.permanent_address_name_bn = dto.permanent_address_name_bn;
		newDTO.permanent_address_name_en = dto.permanent_address_name_en;
		newDTO.district_name_bn = dto.district_name_bn;
		newDTO.district_name_en = dto.district_name_en;
		newDTO.applicant_dob_bn = dto.applicant_dob_bn;
		newDTO.applicant_dob_en = dto.applicant_dob_en;
		newDTO.applicant_age_en = dto.applicant_age_en;
		newDTO.applicant_age_bn = dto.applicant_age_bn;
		newDTO.quota_bn = dto.quota_bn;
		newDTO.quota_en = dto.quota_en;
		newDTO.annual_bn = dto.annual_bn;
		newDTO.annual_en = dto.annual_en;
		newDTO.priviledge_bn = dto.priviledge_bn;
		newDTO.priviledge_en = dto.priviledge_en;
		newDTO.applicant_nid = dto.applicant_nid;
		newDTO.edu_info_bn = dto.edu_info_bn;
		newDTO.edu_info_en = dto.edu_info_en;
		newDTO.job_experience_bn = dto.job_experience_bn;
		newDTO.job_experience_en = dto.job_experience_en;
		newDTO.photo_file_id = dto.photo_file_id;

		newDTO.father_name = dto.father_name;
		newDTO.mother_name = dto.mother_name;
		newDTO.highest_edu_level = dto.highest_edu_level;



		PrevLevelDataSummaryDTO newPrevLevelData = new PrevLevelDataSummaryDTO();
		newPrevLevelData.data = new ArrayList<>();

		PrevLevelDataSummaryDTO convertedObject = new Gson().fromJson(dto.prev_level_data,
				PrevLevelDataSummaryDTO.class);
		if(convertedObject!= null && convertedObject.data != null && convertedObject.data.size() > 0){
			newPrevLevelData.data.addAll(convertedObject.data);
		}

		PrevLevelDataDTO prevLevelDataDTO = new PrevLevelDataDTO();
		prevLevelDataDTO.decision_remarks = dto.decision_remarks;
		prevLevelDataDTO.level = dto.level;
		prevLevelDataDTO.isPresent = dto.isPresent;
		prevLevelDataDTO.marks = dto.marks;
		newPrevLevelData.data.add(prevLevelDataDTO);

		Gson g = new Gson();
		newDTO.prev_level_data = g.toJson(newPrevLevelData);


		job_applicant_applicationDAO.manageWriteOperations(newDTO, SessionConstants.INSERT, -1, userDTO);
	}

	private void getDTO(HttpServletRequest request, HttpServletResponse response)
	{
		try
		{
			System.out.println("In getDTO");
			Job_applicant_applicationDTO job_applicant_applicationDTO = job_applicant_applicationDAO.getDTOByID(Long.parseLong(request.getParameter("ID")));
			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");

			String encoded = this.gson.toJson(job_applicant_applicationDTO);
			System.out.println("json encoded data = " + encoded);
			out.print(encoded);
			out.flush();
		}
		catch (NumberFormatException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void addJob_applicant_application(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, boolean isPermanentTable) throws IOException
	{
		// TODO Auto-generated method stub
		try
		{
			request.setAttribute("failureMessage", "");
			System.out.println("%%%% addJob_applicant_application");
			String path = getServletContext().getRealPath("/img2/");
			Job_applicant_applicationDTO job_applicant_applicationDTO;
			SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");

			if(addFlag == true)
			{
				job_applicant_applicationDTO = new Job_applicant_applicationDTO();
			}
			else
			{
				job_applicant_applicationDTO = job_applicant_applicationDAO.getDTOByID(Long.parseLong(request.getParameter("iD")));
			}

			String Value = "";

			Value = request.getParameter("jobApplicantId");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("jobApplicantId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{

				job_applicant_applicationDTO.jobApplicantId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("jobId");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("jobId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{

				job_applicant_applicationDTO.jobId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("acceptanceStatus");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("acceptanceStatus = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{

				job_applicant_applicationDTO.acceptanceStatus = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			if(addFlag)
			{
				Calendar c = Calendar.getInstance();
				c.set(Calendar.HOUR_OF_DAY, 0);
				c.set(Calendar.MINUTE, 0);
				c.set(Calendar.SECOND, 0);
				c.set(Calendar.MILLISECOND, 0);

				job_applicant_applicationDTO.insertionDate = c.getTimeInMillis();
			}


			if(addFlag)
			{
				job_applicant_applicationDTO.insertedByUserId = userDTO.ID;
			}


			Value = request.getParameter("modifiedBy");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("modifiedBy = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{

				job_applicant_applicationDTO.modifiedBy = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("rollNumber");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("rollNumber = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{

				job_applicant_applicationDTO.rollNumber = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			String trackingNumber = PasswordGenerator.getRandomNumberPassword(6);
			job_applicant_applicationDTO.trackingNumber = (trackingNumber);

			Parliament_job_applicantDAO parliament_job_applicantDAO = new Parliament_job_applicantDAO();
			Job_applicant_certificationDAO job_applicant_certificationDAO = new Job_applicant_certificationDAO();
			Job_applicant_photo_signatureDAO job_applicant_photo_signatureDAO = new Job_applicant_photo_signatureDAO();
			Job_applicant_education_infoDAO job_applicant_education_infoDAO = new Job_applicant_education_infoDAO();
			Job_applicant_professional_infoDAO job_applicant_professional_infoDAO = new Job_applicant_professional_infoDAO();
			Job_applicant_documentsDAO job_applicant_documentsDAO = new Job_applicant_documentsDAO();
			Job_applicant_referenceDAO job_applicant_referenceDAO = new Job_applicant_referenceDAO();

			Parliament_job_applicantDTO parliament_job_applicantDTO = parliament_job_applicantDAO.getDTOByID(job_applicant_applicationDTO.jobApplicantId);
			List<Job_applicant_certificationDTO> job_applicant_certificationDTOList = job_applicant_certificationDAO.getDTOByJobApplicantID(job_applicant_applicationDTO.jobApplicantId);
			List<Job_applicant_photo_signatureDTO> job_applicant_photo_signatureDTOList = job_applicant_photo_signatureDAO.getDTOByJobApplicantID(job_applicant_applicationDTO.jobApplicantId);
			List<Job_applicant_education_infoDTO> job_applicant_education_infoDTOList = job_applicant_education_infoDAO.getDTOByJobApplicantID(job_applicant_applicationDTO.jobApplicantId);
			List<Job_applicant_professional_infoDTO> job_applicant_professional_infoDTOList = job_applicant_professional_infoDAO.getDTOByJobApplicantID(job_applicant_applicationDTO.jobApplicantId);
			List<Job_applicant_documentsDTO> job_applicant_documentsDTOList = job_applicant_documentsDAO.getDTOByJobApplicantID(job_applicant_applicationDTO.jobApplicantId);
			List<Job_applicant_referenceDTO> job_applicant_referenceDTOList = job_applicant_referenceDAO.getDTOByJobApplicantID(job_applicant_applicationDTO.jobApplicantId);

			List<Job_applicant_certificationSummaryDTO> job_applicant_certificationSummaryDTOList = new ArrayList();
			List<Job_applicant_photo_signatureSummaryDTO> job_applicant_photo_signatureSummaryDTOList = new ArrayList();
			List<Job_applicant_education_infoSummaryDTO> job_applicant_education_infoSummaryDTOList = new ArrayList();
			List<Job_applicant_professional_infoSummaryDTO> job_applicant_professional_infoSummaryDTOList = new ArrayList();
			List<Job_applicant_documentsSummaryDTO> job_applicant_documentsSummaryDTOList = new ArrayList();
			List<Job_applicant_referenceSummaryDTO> job_applicant_referenceSummaryDTOList = new ArrayList();

			Gson g = new Gson();
			FilesDAO filesDAO = new FilesDAO();

			for (Job_applicant_certificationDTO dto: job_applicant_certificationDTOList) {
				Job_applicant_certificationSummaryDTO summaryDto = new Job_applicant_certificationSummaryDTO();
				String json = g.toJson(dto);
				summaryDto = g.fromJson(json, Job_applicant_certificationSummaryDTO.class);

				long fileDropZone = dto.filesDropzone;
				List<FilesDTO> filesDTOS = filesDAO.getDTOsByFileID(fileDropZone);
//				summaryDto.filesDropzoneFiles = filesDTOS;

				job_applicant_certificationSummaryDTOList.add(summaryDto);
			}

			for (Job_applicant_photo_signatureDTO dto: job_applicant_photo_signatureDTOList) {
				Job_applicant_photo_signatureSummaryDTO summaryDto = new Job_applicant_photo_signatureSummaryDTO();
				String json = g.toJson(dto);
				summaryDto = g.fromJson(json, Job_applicant_photo_signatureSummaryDTO.class);

				long fileDropZone = dto.filesDropzone;
				List<FilesDTO> filesDTOS = filesDAO.getDTOsByFileID(fileDropZone);
//				summaryDto.filesDropzoneFiles = filesDTOS;

				job_applicant_photo_signatureSummaryDTOList.add(summaryDto);
			}

			for (Job_applicant_education_infoDTO dto: job_applicant_education_infoDTOList) {
				Job_applicant_education_infoSummaryDTO summaryDto = new Job_applicant_education_infoSummaryDTO();
				String json = g.toJson(dto);
				summaryDto = g.fromJson(json, Job_applicant_education_infoSummaryDTO.class);

				long fileDropZone = dto.filesDropzone;
				List<FilesDTO> filesDTOS = filesDAO.getDTOsByFileID(fileDropZone);
//				summaryDto.filesDropzoneFiles = filesDTOS;

				job_applicant_education_infoSummaryDTOList.add(summaryDto);
			}

			for (Job_applicant_professional_infoDTO dto: job_applicant_professional_infoDTOList) {
				Job_applicant_professional_infoSummaryDTO summaryDto = new Job_applicant_professional_infoSummaryDTO();
				String json = g.toJson(dto);
				summaryDto = g.fromJson(json, Job_applicant_professional_infoSummaryDTO.class);
				job_applicant_professional_infoSummaryDTOList.add(summaryDto);
			}

			for (Job_applicant_documentsDTO dto: job_applicant_documentsDTOList) {
				Job_applicant_documentsSummaryDTO summaryDto = new Job_applicant_documentsSummaryDTO();
				String json = g.toJson(dto);
				summaryDto = g.fromJson(json, Job_applicant_documentsSummaryDTO.class);

				long fileDropZone = dto.filesDropzone;
				List<FilesDTO> filesDTOS = filesDAO.getDTOsByFileID(fileDropZone);
//				summaryDto.filesDropzoneFiles = filesDTOS;

				job_applicant_documentsSummaryDTOList.add(summaryDto);
			}

			for (Job_applicant_referenceDTO dto: job_applicant_referenceDTOList) {
				Job_applicant_referenceSummaryDTO summaryDto = new Job_applicant_referenceSummaryDTO();
				String json = g.toJson(dto);
				summaryDto = g.fromJson(json, Job_applicant_referenceSummaryDTO.class);
				job_applicant_referenceSummaryDTOList.add(summaryDto);
			}

			SummaryDTO summaryDTO = new SummaryDTO();

			summaryDTO.parliament_job_applicantDTO = parliament_job_applicantDTO;
			summaryDTO.job_applicant_certificationSummaryDTOList = job_applicant_certificationSummaryDTOList;
			summaryDTO.job_applicant_photo_signatureSummaryDTOList = job_applicant_photo_signatureSummaryDTOList;
			summaryDTO.job_applicant_education_infoSummaryDTOList = job_applicant_education_infoSummaryDTOList;
			summaryDTO.job_applicant_professional_infoSummaryDTOList = job_applicant_professional_infoSummaryDTOList;
			summaryDTO.job_applicant_documentsSummaryDTOList = job_applicant_documentsSummaryDTOList;
			summaryDTO.job_applicant_referenceSummaryDTOList = job_applicant_referenceSummaryDTOList;

			String summary = g.toJson(summaryDTO);

			job_applicant_applicationDTO.summary = summary;

			System.out.println("Done adding  addJob_applicant_application dto = " + job_applicant_applicationDTO);
			long returnedID = -1;

			if(isPermanentTable == false) //add new row for validation and make the old row outdated
			{
				job_applicant_applicationDAO.setIsDeleted(job_applicant_applicationDTO.iD, CommonDTO.OUTDATED);
				returnedID = job_applicant_applicationDAO.add(job_applicant_applicationDTO);
				job_applicant_applicationDAO.setIsDeleted(returnedID, CommonDTO.WAITING_FOR_APPROVAL);
			}
			else if(addFlag == true)
			{
				returnedID = job_applicant_applicationDAO.manageWriteOperations(job_applicant_applicationDTO, SessionConstants.INSERT, -1, userDTO);
			}
			else
			{
				returnedID = job_applicant_applicationDAO.manageWriteOperations(job_applicant_applicationDTO, SessionConstants.UPDATE, -1, userDTO);
			}

			SmsService.send(parliament_job_applicantDTO.contactNumber, String.format("Dear %s, Your tracking number has been set, tracking number is " + trackingNumber, parliament_job_applicantDTO.nameBn ));


			response.sendRedirect(JSPConstant.PUBLIC_SEARCH_SERVLET);




//			if(isPermanentTable)
//			{
//				String inPlaceSubmit = (String)request.getParameter("inplacesubmit");
//
//				if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
//				{
//					getJob_applicant_application(request, response, returnedID);
//				}
//				else
//				{
//					response.sendRedirect("Job_applicant_applicationServlet?actionType=search");
//				}
//			}
//			else
//			{
//				commonRequestHandler.validate(job_applicant_applicationDAO.getDTOByID(returnedID), request, response, userDTO);
//			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}









	private void deleteJob_applicant_application(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO) throws IOException
	{
		try
		{
			String[] IDsToDelete = request.getParameterValues("ID");
			for(int i = 0; i < IDsToDelete.length; i ++)
			{
				long id = Long.parseLong(IDsToDelete[i]);
				System.out.println("------ DELETING " + IDsToDelete[i]);


				Job_applicant_applicationDTO job_applicant_applicationDTO = job_applicant_applicationDAO.getDTOByID(id);
				job_applicant_applicationDAO.manageWriteOperations(job_applicant_applicationDTO, SessionConstants.DELETE, id, userDTO);
				response.sendRedirect("Job_applicant_applicationServlet?actionType=search");

			}
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}

	}

	private void getJob_applicant_application(HttpServletRequest request, HttpServletResponse response, long id) throws ServletException, IOException
	{
		System.out.println("in getJob_applicant_application");
		Job_applicant_applicationDTO job_applicant_applicationDTO = null;
		try
		{
			job_applicant_applicationDTO = job_applicant_applicationDAO.getDTOByID(id);
			request.setAttribute("ID", job_applicant_applicationDTO.iD);
			request.setAttribute("job_applicant_applicationDTO",job_applicant_applicationDTO);
			request.setAttribute("job_applicant_applicationDAO",job_applicant_applicationDAO);

			String URL= "";

			String inPlaceEdit = request.getParameter("inplaceedit");
			String inPlaceSubmit = request.getParameter("inplacesubmit");
			String getBodyOnly = request.getParameter("getBodyOnly");

			if(inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase(""))
			{
				URL = "job_applicant_application/job_applicant_applicationInPlaceEdit.jsp";
				request.setAttribute("inplaceedit","");
			}
			else if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
			{
				URL = "job_applicant_application/job_applicant_applicationSearchRow.jsp";
				request.setAttribute("inplacesubmit","");
			}
			else
			{
				if(getBodyOnly  != null && !getBodyOnly.equalsIgnoreCase(""))
				{
					URL = "job_applicant_application/job_applicant_applicationEditBody.jsp?actionType=edit";
				}
				else
				{
					URL = "job_applicant_application/job_applicant_applicationEdit.jsp?actionType=edit";
				}
			}

			RequestDispatcher rd = request.getRequestDispatcher(URL);
			rd.forward(request, response);
		}
		catch (NumberFormatException e)
		{
			e.printStackTrace();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	private void getJob_applicant_applicationSummary(HttpServletRequest request, HttpServletResponse response, long id) throws ServletException, IOException
	{
		System.out.println("in getJob_applicant_application");
		Job_applicant_applicationDTO job_applicant_applicationDTO = null;
		try
		{
			job_applicant_applicationDTO = Job_applicant_applicationRepository.getInstance()
					.getJob_applicant_applicationDTOByID(id);
//					(Job_applicant_applicationDTO)job_applicant_applicationDAO.getDTOByID(id);
			request.setAttribute("ID", job_applicant_applicationDTO.jobApplicantId);
			request.setAttribute("job_applicant_applicationDTO",job_applicant_applicationDTO);
			request.setAttribute("job_applicant_applicationDAO",job_applicant_applicationDAO);

			String URL= "";

			String inPlaceEdit = request.getParameter("inplaceedit");
			String inPlaceSubmit = request.getParameter("inplacesubmit");
			String getBodyOnly = request.getParameter("getBodyOnly");

			if(inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase(""))
			{
				URL = "job_applicant_application/job_applicant_applicationInPlaceEdit.jsp";
				request.setAttribute("inplaceedit","");
			}
			else if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
			{
				URL = "job_applicant_application/job_applicant_applicationSearchRow.jsp";
				request.setAttribute("inplacesubmit","");
			}
			else
			{
				if(getBodyOnly  != null && !getBodyOnly.equalsIgnoreCase(""))
				{
					URL = "job_applicant_application/job_applicant_applicationSummaryBody.jsp?actionType=edit";
				}
				else
				{
					URL = "job_applicant_application/job_applicant_applicationSummary.jsp?actionType=edit";
				}
			}

			RequestDispatcher rd = request.getRequestDispatcher(URL);
			rd.forward(request, response);
		}
		catch (NumberFormatException e)
		{
			e.printStackTrace();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	private void getJob_applicant_application(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		getJob_applicant_application(request, response, Long.parseLong(request.getParameter("ID")));
	}

	private void searchJob_applicant_application(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException
	{
		System.out.println("in  searchJob_applicant_application 1");
		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String ajax = request.getParameter("ajax");
		boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);

        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
			SessionConstants.NAV_JOB_APPLICANT_APPLICATION,
			request,
			job_applicant_applicationDAO,
			SessionConstants.VIEW_JOB_APPLICANT_APPLICATION,
			SessionConstants.SEARCH_JOB_APPLICANT_APPLICATION,
			tableName,
			isPermanent,
			userDTO,
			filter,
			true);
        try
        {
			System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        }
        catch(Exception e)
        {
			System.out.println("failed to dojob" + e);
        }

		request.setAttribute("job_applicant_applicationDAO",job_applicant_applicationDAO);
        RequestDispatcher rd;
        if(!isPermanent)
        {
        	if(hasAjax == false)
	        {
	        	System.out.println("Going to job_applicant_application/job_applicant_applicationApproval.jsp");
	        	rd = request.getRequestDispatcher("job_applicant_application/job_applicant_applicationApproval.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to job_applicant_application/job_applicant_applicationApprovalForm.jsp");
	        	rd = request.getRequestDispatcher("job_applicant_application/job_applicant_applicationApprovalForm.jsp");
	        }
        }
        else
        {
	        if(hasAjax == false)
	        {
	        	System.out.println("Going to job_applicant_application/job_applicant_applicationSearch.jsp");
	        	rd = request.getRequestDispatcher("job_applicant_application/job_applicant_applicationSearch.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to job_applicant_application/job_applicant_applicationSearchForm.jsp");
	        	rd = request.getRequestDispatcher("job_applicant_application/job_applicant_applicationSearchForm.jsp");
	        }
        }
		rd.forward(request, response);
	}

	private void generateRoll(HttpServletRequest request) throws Exception {
		long jobId = Long.parseLong(request.getParameter("jobId"));
		job_applicant_applicationDAO.generateRollByPriorityQueue(jobId);
		Recruitment_job_descriptionDAO jobDescriptionDAO = new Recruitment_job_descriptionDAO();
		Recruitment_job_descriptionDTO jobDescriptionDTO = Recruitment_job_descriptionRepository.getInstance()
				.getRecruitment_job_descriptionDTOByID(jobId);
		jobDescriptionDTO.roll_created = true;
		jobDescriptionDTO.lastModificationTime = System.currentTimeMillis();
		jobDescriptionDAO.update(jobDescriptionDTO);
		Job_applicant_applicationRepository.getInstance().reload(false);
	}

	public void sendMail(String mailId,String message,String subject){
		if(mailId!=null && mailId.trim().length()>0){
			SendEmailDTO sendEmailDTO = new SendEmailDTO();
			sendEmailDTO.setTo(new String[]{mailId});
			sendEmailDTO.setText(message);
			sendEmailDTO.setSubject(subject);
			sendEmailDTO.setFrom("edms@pbrlp.gov.bd");
			try{
				new EmailService().sendMail(sendEmailDTO);
			}catch (Exception ex){
				ex.printStackTrace();
			}
		}else{
			logger.debug("<<<<mail id is null or empty, mailId : "+mailId);
		}
	}

}

