package job_applicant_application;
import java.util.*; 
import util.*;


public class Job_applicant_applicationMAPS extends CommonMaps
{	
	public Job_applicant_applicationMAPS(String tableName)
	{
		

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("jobApplicantId".toLowerCase(), "jobApplicantId".toLowerCase());
		java_DTO_map.put("jobId".toLowerCase(), "jobId".toLowerCase());
		java_DTO_map.put("acceptanceStatus".toLowerCase(), "acceptanceStatus".toLowerCase());
		java_DTO_map.put("insertionDate".toLowerCase(), "insertionDate".toLowerCase());
		java_DTO_map.put("insertedByUserId".toLowerCase(), "insertedByUserId".toLowerCase());
		java_DTO_map.put("modifiedBy".toLowerCase(), "modifiedBy".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
		java_DTO_map.put("rollNumber".toLowerCase(), "rollNumber".toLowerCase());

		java_SQL_map.put("job_applicant_id".toLowerCase(), "jobApplicantId".toLowerCase());
		java_SQL_map.put("job_id".toLowerCase(), "jobId".toLowerCase());
		java_SQL_map.put("acceptance_status".toLowerCase(), "acceptanceStatus".toLowerCase());
		java_SQL_map.put("insertion_date".toLowerCase(), "insertionDate".toLowerCase());
		java_SQL_map.put("inserted_by_user_id".toLowerCase(), "insertedByUserId".toLowerCase());
		java_SQL_map.put("modified_by".toLowerCase(), "modifiedBy".toLowerCase());
		java_SQL_map.put("roll_number".toLowerCase(), "rollNumber".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Job Applicant Id".toLowerCase(), "jobApplicantId".toLowerCase());
		java_Text_map.put("Job Id".toLowerCase(), "jobId".toLowerCase());
		java_Text_map.put("Acceptance Status".toLowerCase(), "acceptanceStatus".toLowerCase());
		java_Text_map.put("Insertion Date".toLowerCase(), "insertionDate".toLowerCase());
		java_Text_map.put("Inserted By User Id".toLowerCase(), "insertedByUserId".toLowerCase());
		java_Text_map.put("Modified By".toLowerCase(), "modifiedBy".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
		java_Text_map.put("Roll".toLowerCase(), "rollNumber".toLowerCase());
		java_Text_map.put("Reason".toLowerCase(), "decision_remarks".toLowerCase());
			
	}

}