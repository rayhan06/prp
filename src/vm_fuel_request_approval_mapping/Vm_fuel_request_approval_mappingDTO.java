package vm_fuel_request_approval_mapping;

import util.CommonDTO;


public class Vm_fuel_request_approval_mappingDTO extends CommonDTO {
    public long id = 0;
    public long vm_fuel_requestId = 0;
    public long vm_fuel_requestApprovalId = 0;
    public int sequence = 0;
    public long insertedBy = 0;

    @Override
    public String toString() {
        return "Vm_fuel_request_approval_mappingDTO{" +
                "id=" + id +
                ", vm_fuel_requestApprovalId=" + vm_fuel_requestApprovalId +
                ", sequence=" + sequence +
                ", insertedBy=" + insertedBy +
                ", insertionTime=" + insertionTime +
                ", modifiedBy=" + modifiedBy +
                ", vm_fuel_requestApprovalStatusCat=" + vm_fuel_requestApprovalId +
                ", approverEmployeeRecordsId=" + approverEmployeeRecordsId +
                ", taskTypeId=" + taskTypeId +
                ", iD=" + iD +
                ", lastModificationTime=" + lastModificationTime +
                '}';
    }

    public long insertionTime = 0;
    public long modifiedBy = 0;
    public int vm_fuel_requestApprovalStatusCat = 0;
    public long approverEmployeeRecordsId = 0;
    public long taskTypeId = 0;

}