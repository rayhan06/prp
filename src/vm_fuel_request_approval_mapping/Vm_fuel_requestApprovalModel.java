package vm_fuel_request_approval_mapping;

import card_info.*;
import geolocation.GeoLocationDAO2;
import org.apache.commons.codec.binary.Base64;
import pb.CatRepository;
import sessionmanager.SessionConstants;
import util.StringUtils;
import vm_fuel_request.Vm_fuel_requestApprovalDTO;
import vm_fuel_request.Vm_fuel_requestDTO;

public class Vm_fuel_requestApprovalModel {
    public long cardInfoId = -1;
    public String approvedBy = "";
    public String approvedOn = "";
    public Vm_fuel_request_approval_mappingDTO cardApprovalMappingDTO;


    public Vm_fuel_requestApprovalModel() {
    }

    public Vm_fuel_requestApprovalModel(Vm_fuel_requestDTO card_infoDTO,Vm_fuel_requestApprovalDTO cardApprovalDTO, String Language, Vm_fuel_request_approval_mappingDTO cardApprovalMappingDTO) {
        boolean isLanguageEnglish = Language.equalsIgnoreCase("English");
        cardInfoId = card_infoDTO.iD;
        this.cardApprovalMappingDTO = cardApprovalMappingDTO;
        if (cardApprovalDTO != null) {
            approvedBy = cardApprovalDTO.nameEng;
            approvedOn = StringUtils.getFormattedDate(Language, cardApprovalDTO.insertionTime);
        }
    }

    @Override
    public String toString() {
        return "Vm_fuel_requestApprovalModel{" +
                "cardInfoId=" + cardInfoId +
                ", approvedBy='" + approvedBy + '\'' +
                ", approvedOn='" + approvedOn + '\'' +
                ", cardApprovalMappingDTO=" + cardApprovalMappingDTO +
                '}';
    }
}
