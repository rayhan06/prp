package vm_fuel_request_approval_mapping;

import card_info.*;
import com.google.gson.Gson;
import common.RoleEnum;
import employee_assign.EmployeeSearchModalUtil;
import employee_assign.EmployeeSearchModel;
import employee_records.Employee_recordsDTO;
import employee_records.Employee_recordsRepository;
import login.LoginDTO;
import office_units.Office_unitsRepository;
import offices.OfficesDTO;
import offices.OfficesRepository;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import pb.Utils;
import permission.MenuConstants;
import role.PermissionRepository;
import sessionmanager.SessionConstants;
import task_type.TaskTypeEnum;
import user.UserDTO;
import user.UserRepository;
import util.RecordNavigationManager4;
import util.StringUtils;
import vm_fuel_request.*;
import vm_requisition.CommonApprovalStatus;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;


@WebServlet("/Vm_fuel_request_approval_mappingServlet")
@MultipartConfig
public class Vm_fuel_request_approval_mappingServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private static final Logger logger = Logger.getLogger(Vm_fuel_request_approval_mappingServlet.class);

    private static final String tableName = "vm_fuel_request_approval_mapping";

    private final Vm_fuel_request_approval_mappingDAO card_approval_mappingDAO;

    public Vm_fuel_request_approval_mappingServlet() {
        super();
        card_approval_mappingDAO = new Vm_fuel_request_approval_mappingDAO(tableName);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }
        try {
            String actionType = request.getParameter("actionType");
            logger.debug("actionType : "+actionType);
            switch (actionType) {
                case "getApprovalPage":
                    long cardInfoId = Long.parseLong(request.getParameter("cardInfoId"));
                    getVm_fuel_requestApprovalModel(cardInfoId, request, userDTO, -1);
                    request.getRequestDispatcher("vm_fuel_request_approval_mapping/vm_fuel_request_approval_mappingEdit.jsp").forward(request, response);
                    return;
                case "getPaymentPage":
                    cardInfoId = Long.parseLong(request.getParameter("cardInfoId"));
                    getVm_fuel_requestApprovalModel(cardInfoId, request, userDTO, CommonApprovalStatus.SATISFIED.getValue());
                    request.getRequestDispatcher("vm_fuel_request_approval_mapping/vm_fuel_requestPaymentEdit.jsp").forward(request, response);
                    return;
                case "search":
                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.CARD_APPROVAL_MAPPING_SEARCH)) {

//                        if (userDTO.roleID == RoleEnum.EMPLOYEE_OF_PARLIAMENT.getRoleId()) {
                        searchVm_fuel_request_approval_mapping(request, response, String.valueOf(userDTO.employee_record_id), loginDTO, userDTO);
//                        } else {
//                            searchVm_fuel_request_approval_mapping(request, response, "", loginDTO, userDTO);
//                        }
                        return;
                    }
                    break;
                case "approvedVm_fuel_request":
                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.CARD_APPROVAL_MAPPING_ADD)) {
                        Approve(request, userDTO, true,null,null);
                        response.sendRedirect("Vm_fuel_request_approval_mappingServlet?actionType=search");
                        return;
                    }
                    break;
                case "rejectVm_fuel_request":
                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.CARD_APPROVAL_MAPPING_ADD)) {
                        Approve(request, userDTO, false,null,null);
                        response.sendRedirect("Vm_fuel_request_approval_mappingServlet?actionType=search");
                        return;
                    }
                    break;
            }
        } catch (Exception ex) {
            logger.debug(ex);
            ex.printStackTrace();
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }

        try {
            String actionType = request.getParameter("actionType");
            switch (actionType) {
                case "approvedVm_fuel_request":
//                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.CARD_APPROVAL_MAPPING_ADD)) {
                    if (true) {
                        SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
//                        Date from = f.parse(Jsoup.clean(request.getParameter("validationStartDate"), Whitelist.simpleText()));
//                        Date to = f.parse(Jsoup.clean(request.getParameter("validationEndDate"), Whitelist.simpleText()));
                        Approve(request, userDTO, true,null,null);
                        response.sendRedirect("Vm_fuel_request_approval_mappingServlet?actionType=search");
                        return;
                    }
                    break;
                case "paidVm_fuel_request":
//                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.CARD_APPROVAL_MAPPING_ADD)) {
                    if (true) {
                        SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
//                        Date from = f.parse(Jsoup.clean(request.getParameter("validationStartDate"), Whitelist.simpleText()));
//                        Date to = f.parse(Jsoup.clean(request.getParameter("validationEndDate"), Whitelist.simpleText()));
                        Pay(request, response, userDTO, true,null,null);
//                        response.sendRedirect("Vm_fuel_request_approval_mappingServlet?actionType=search");
                        return;
                    }
                    break;
                case "rejectVm_fuel_request":
//                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.CARD_APPROVAL_MAPPING_ADD)) {
                    if (true) {
                        Approve(request, userDTO, false,null,null);
                        response.sendRedirect("Vm_fuel_request_approval_mappingServlet?actionType=search");
                        return;
                    }
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error(ex);
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }

    private void getVm_fuel_requestApprovalModel(long cardInfoId, HttpServletRequest request, UserDTO userDTO, int status) throws Exception {

        long requesterEmployeeRecordId = userDTO.employee_record_id;
        TaskTypeEnum taskTypeEnum = TaskTypeEnum.VM_FUEL_REQUEST;

        CreateVm_fuel_requestApprovalModel model = new CreateVm_fuel_requestApprovalModel.CreateVm_fuel_requestApprovalModelBuilder()
                .setTaskTypeId(taskTypeEnum.getValue())
                .setVm_fuel_requestId(cardInfoId)
                .setRequesterEmployeeRecordId(requesterEmployeeRecordId)
                .build();

        new Vm_fuel_request_approval_mappingDAO().checkValidation(model, status);

        Vm_fuel_requestDAO vm_fuel_requestDAO = new Vm_fuel_requestDAO();
        Vm_fuel_requestDTO vm_fuel_requestDTO = Vm_fuel_requestRepository.getInstance().getVm_fuel_requestDTOByID(cardInfoId);
        request.setAttribute("ID", vm_fuel_requestDTO.iD);
        request.setAttribute("vm_fuel_requestDTO",vm_fuel_requestDTO);
        request.setAttribute("vm_fuel_requestDAO",vm_fuel_requestDAO);
    }

    private void Approve(HttpServletRequest request, UserDTO userDTO, boolean isAccepted,Date validFrom,Date validTo) throws Exception {
        Utils.handleTransactionForNavigationService4(()->{
            long requesterEmployeeRecordId = userDTO.employee_record_id;
            long cardInfoId = Long.parseLong(request.getParameter("cardInfoId"));
            Vm_fuel_requestDTO card_infoDTO = Vm_fuel_requestRepository.getInstance().getVm_fuel_requestDTOByID(cardInfoId);
            Vm_fuel_requestDAO cardInfoDAO = new Vm_fuel_requestDAO();
//        cardInfoDAO.update(card_infoDTO);

            TaskTypeEnum taskTypeEnum = TaskTypeEnum.VM_FUEL_REQUEST;
//        if (card_infoDTO. == Vm_fuel_requestCategoryEnum.PERMANENT.getValue()) {
//            if (employeeRecordsDTO.employeeClass == 3 || employeeRecordsDTO.employeeClass == 4) {
//                taskTypeEnum = TaskTypeEnum.PERMANENT_CARD_FOR_3RD_OR_4TH_CLASS_EMPLOYEE;
//            } else {
//                taskTypeEnum = TaskTypeEnum.PERMANENT_CARD_FOR_1ST_OR_2ND_CLASS_EMPLOYEE;
//            }
//        } else {
//            taskTypeEnum = TaskTypeEnum.TEMPORARY_CARD_FOR_EMPLOYEE;
//        }
            CreateVm_fuel_requestApprovalModel model = new CreateVm_fuel_requestApprovalModel.CreateVm_fuel_requestApprovalModelBuilder()
                    .setTaskTypeId(taskTypeEnum.getValue())
                    .setVm_fuel_requestId(card_infoDTO.iD)
                    .setRequesterEmployeeRecordId(requesterEmployeeRecordId)
                    .build();

            new Vm_fuel_request_approval_mappingDAO().checkValidation(model, -1);

            if (isAccepted) {
                Vm_fuel_requestApprovalResponse response = new Vm_fuel_request_approval_mappingDAO().movedToNextLevelOfApproval(model);
                if(!response.hasNextApproval){
                    card_infoDTO.status = Vm_fuel_requestStatusEnum.APPROVED.getValue();
                    long vendorId = Long.parseLong(request.getParameter("vendorId"));
                    card_infoDTO.vendorId = vendorId;

                    EmployeeSearchModel employeeSearchModel = new Gson().fromJson(EmployeeSearchModalUtil.getEmployeeSearchModelJson(userDTO.employee_record_id, userDTO.unitID, userDTO.organogramID), EmployeeSearchModel.class);

                    card_infoDTO.approverEmpId = employeeSearchModel.employeeRecordId;
                    card_infoDTO.approverOfficeUnitId = employeeSearchModel.officeUnitId;
                    card_infoDTO.approverOrgId = employeeSearchModel.organogramId;

                    card_infoDTO.approverNameEn = employeeSearchModel.employeeNameEn;
                    card_infoDTO.approverNameBn = employeeSearchModel.employeeNameBn;
                    card_infoDTO.approverOfficeUnitNameEn = employeeSearchModel.officeUnitNameEn;
                    card_infoDTO.approverOfficeUnitNameBn = employeeSearchModel.officeUnitNameBn;
                    card_infoDTO.approverOfficeUnitOrgNameEn = employeeSearchModel.organogramNameEn;
                    card_infoDTO.approverOfficeUnitOrgNameBn = employeeSearchModel.organogramNameBn;

                    card_infoDTO.approverPhoneNum = employeeSearchModel.phoneNumber;

                    OfficesDTO approverOffice = OfficesRepository.getInstance().getOfficesDTOByID(card_infoDTO.approverOfficeId);

                    if (approverOffice != null) {
                        card_infoDTO.approverOfficeId = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(employeeSearchModel.officeUnitId).officeId;

                        card_infoDTO.approverOfficeNameEn = approverOffice.officeNameEng;
                        card_infoDTO.approverOfficeNameBn = approverOffice.officeNameBng;
                    }

                    cardInfoDAO.update(card_infoDTO);
                }
            } else {
                new Vm_fuel_request_approval_mappingDAO().rejectPendingApproval(model);
                card_infoDTO.status = Vm_fuel_requestStatusEnum.REJECTED.getValue();
                cardInfoDAO.update(card_infoDTO);
            }
        });
    }

    private void Pay(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO, boolean isAccepted,Date validFrom,Date validTo) throws Exception {
        Utils.handleTransactionForNavigationService4(()->{
            long requesterEmployeeRecordId = userDTO.employee_record_id;

            TaskTypeEnum taskTypeEnum = TaskTypeEnum.VM_FUEL_REQUEST;

            String paymentRemarks = request.getParameter("paymentRemarks");
            String sarokNumber = request.getParameter("sarokNumber");

            if (StringUtils.isValidString(sarokNumber)) {

                long filesDropzone = Long.parseLong(request.getParameter("filesDropzone"));
                Vm_fuel_requestDAO cardInfoDAO = new Vm_fuel_requestDAO();
                String[] isPaid = (request.getParameterValues("isPaid"));

                if (isPaid != null) {
                    int isPaidLength = isPaid.length;

                    for (int i=0; i< isPaidLength; i++) {

                        long cardInfoId = Long.parseLong(isPaid[i]);

                        CreateVm_fuel_requestApprovalModel approvalModel = new CreateVm_fuel_requestApprovalModel.CreateVm_fuel_requestApprovalModelBuilder()
                                .setTaskTypeId(taskTypeEnum.getValue())
                                .setVm_fuel_requestId(cardInfoId)
                                .setRequesterEmployeeRecordId(requesterEmployeeRecordId)
                                .build();

                        new Vm_fuel_request_approval_mappingDAO().checkValidation(approvalModel, CommonApprovalStatus.SATISFIED.getValue());

                        Vm_fuel_requestDTO card_infoDTO = Vm_fuel_requestRepository.getInstance().getVm_fuel_requestDTOByID(cardInfoId);
                        card_infoDTO.status = Vm_fuel_requestStatusEnum.PAID.getValue();

                        card_infoDTO.paymentRemarks = paymentRemarks;
                        card_infoDTO.sarokNumber = sarokNumber;
                        card_infoDTO.filesDropzone = filesDropzone;

                        EmployeeSearchModel model = new Gson().fromJson(EmployeeSearchModalUtil.getEmployeeSearchModelJson(userDTO.employee_record_id, userDTO.unitID, userDTO.organogramID), EmployeeSearchModel.class);

                        card_infoDTO.paymentReceiverEmpId = model.employeeRecordId;
                        card_infoDTO.paymentReceiverOfficeUnitId = model.officeUnitId;
                        card_infoDTO.paymentReceiverOrgId = model.organogramId;

                        card_infoDTO.paymentReceiverNameEn = model.employeeNameEn;
                        card_infoDTO.paymentReceiverNameBn = model.employeeNameBn;
                        card_infoDTO.paymentReceiverOfficeUnitNameEn = model.officeUnitNameEn;
                        card_infoDTO.paymentReceiverOfficeUnitNameBn = model.officeUnitNameBn;
                        card_infoDTO.paymentReceiverOfficeUnitOrgNameEn = model.organogramNameEn;
                        card_infoDTO.paymentReceiverOfficeUnitOrgNameBn = model.organogramNameBn;

                        card_infoDTO.paymentReceiverPhoneNum = model.phoneNumber;

                        OfficesDTO paymentReceiverOffice = OfficesRepository.getInstance().getOfficesDTOByID(card_infoDTO.paymentReceiverOfficeId);

                        if (paymentReceiverOffice != null) {
                            card_infoDTO.paymentReceiverOfficeId = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(model.officeUnitId).officeId;

                            card_infoDTO.paymentReceiverOfficeNameEn = paymentReceiverOffice.officeNameEng;
                            card_infoDTO.paymentReceiverOfficeNameBn = paymentReceiverOffice.officeNameBng;
                        }


                        cardInfoDAO.update(card_infoDTO);

                        CreateVm_fuel_requestApprovalModel approvalModelForPayment = new CreateVm_fuel_requestApprovalModel.CreateVm_fuel_requestApprovalModelBuilder()
                                .setTaskTypeId(taskTypeEnum.getValue())
                                .setVm_fuel_requestId(card_infoDTO.iD)
                                .setRequesterEmployeeRecordId(card_infoDTO.requesterEmpId)
                                .build();

                        new Vm_fuel_request_approval_mappingDAO().paySatisfiedApproval(approvalModelForPayment);

                    }
                }

                PrintWriter out = response.getWriter();
                out.println(new Gson().toJson("Success"));
                out.close();
            }
            else {
                PrintWriter out = response.getWriter();
                out.println(new Gson().toJson("Invalid Input"));
                out.close();
            }
        });

    }

    private void searchVm_fuel_request_approval_mapping(HttpServletRequest request, HttpServletResponse response, String filter,
                                             LoginDTO loginDTO, UserDTO userDTO) throws Exception {
        String ajax = request.getParameter("ajax");
        boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");

        logger.debug("ajax = " + ajax + " hasajax = " + hasAjax);

        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
                SessionConstants.NAV_CARD_APPROVAL_MAPPING,
                request,
                card_approval_mappingDAO,
                SessionConstants.VIEW_CARD_APPROVAL_MAPPING,
                SessionConstants.SEARCH_CARD_APPROVAL_MAPPING,
                tableName,
                true,
                userDTO,
                filter,
                true);

        rnManager.doJob(loginDTO);


        request.setAttribute("vm_fuel_request_approval_mappingDAO", card_approval_mappingDAO);

        if (!hasAjax) {
            request.getRequestDispatcher("vm_fuel_request_approval_mapping/vm_fuel_request_approval_mappingSearch.jsp").forward(request, response);
        } else {
            request.getRequestDispatcher("vm_fuel_request_approval_mapping/vm_fuel_request_approval_mappingSearchForm.jsp").forward(request, response);
        }
    }
}