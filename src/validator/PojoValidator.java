package validator;

import java.lang.annotation.Annotation;

public interface PojoValidator {
	void validate(Object fieldValue, Annotation annotation);
}
