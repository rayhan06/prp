package am_house_allocation_approval_mapping;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import am_house.Am_houseDAO;
import am_house.Am_houseDTO;
import am_house.Am_houseRepository;
import am_house_allocation.Am_house_allocationDAO;
import am_house_allocation.Am_house_allocationDTO;
import am_house_allocation_request.AmHouseAllocationRequestStatus;
import am_house_allocation_request.Am_house_allocation_requestDAO;
import am_house_allocation_request.Am_house_allocation_requestDTO;

import am_office_assignment.Am_office_assignmentDAO;
import am_office_assignment.Am_office_assignmentDTO;
import am_parliament_building_room.Am_parliament_building_roomDTO;
import am_parliament_building_room.Am_parliament_building_roomRepository;
import am_reliable_family_member.Am_reliable_family_memberDAO;
import card_info.CardApprovalResponse;
import common.CommonDAOService;
import employee_assign.EmployeeSearchIds;
import employee_assign.EmployeeSearchModalUtil;
import employee_assign.EmployeeSearchModel;

import employee_offices.EmployeeOfficeRepository;
import office_units.Office_unitsRepository;
import offices.OfficesDTO;
import offices.OfficesRepository;
import org.apache.log4j.Logger;

import login.LoginDTO;
import permission.MenuConstants;


import sessionmanager.SessionConstants;
import task_type.TaskTypeEnum;
import user.UserDTO;
import user.UserRepository;
import util.*;

import javax.servlet.http.*;
import java.util.*;

import common.ApiResponse;
import common.BaseServlet;
import com.google.gson.Gson;
import pb.*;

import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import vm_requisition.CommonApprovalStatus;


/**
 * Servlet implementation class Am_house_allocation_approval_mappingServlet
 */
@WebServlet("/Am_house_allocation_approval_mappingServlet")
@MultipartConfig
public class Am_house_allocation_approval_mappingServlet extends BaseServlet {
    private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Am_house_allocation_approval_mappingServlet.class);

    @Override
    public String getTableName() {
        return Am_house_allocation_approval_mappingDAO.getInstance().getTableName();
    }

    @Override
    public String getServletName() {
        return "Am_house_allocation_approval_mappingServlet";
    }

    @Override
    public Am_house_allocation_approval_mappingDAO getCommonDAOService() {
        return Am_house_allocation_approval_mappingDAO.getInstance();
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[]{MenuConstants.AM_HOUSE_ALLOCATION_APPROVAL_MAPPING_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[]{MenuConstants.AM_HOUSE_ALLOCATION_APPROVAL_MAPPING_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[]{MenuConstants.AM_HOUSE_ALLOCATION_APPROVAL_MAPPING_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Am_house_allocation_approval_mappingServlet.class;
    }

    private final Gson gson = new Gson();


    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        return null;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }

        try {
            String actionType = request.getParameter("actionType");
            switch (actionType) {
                case "search":
                    if (Utils.checkPermission(String.valueOf(userDTO.employee_record_id), userDTO, MenuConstants.AM_HOUSE_ALLOCATION_APPROVAL_MAPPING_SEARCH)) {
                        Map<String, String> extraCriteriaMap = new HashMap<>();
                        extraCriteriaMap.put("approverEmployeeRecordsId", String.valueOf(userDTO.employee_record_id));
                        request.setAttribute(CommonDAOService.CRITERIA_MAP_KEY, extraCriteriaMap);
                        search(request, response);
                        return;
                    }
                    break;
                case "getApprovalPage":
                    System.out.println("here read house app");
                    long amHouseAllocationRequestId = Long.parseLong(request.getParameter("amHouseAllocationRequestId"));
                    setApprovalPageData(amHouseAllocationRequestId, request, userDTO);
                    request.getRequestDispatcher("am_house_allocation_approval_mapping/am_house_allocation_approval_mappingView.jsp").forward(request, response);  //TODO AM HOUSE ALLOCATION CHANGE THIS
                    //request.getRequestDispatcher(commonPartOfDispatchURL() + "View.jsp").forward(request,response);
                    return;

                case "bulkHouseAllocation":

                    request.getRequestDispatcher("am_house_allocation_approval_mapping/am_house_bulk_allocationEdit.jsp").
                            forward(request, response);
                    return;
                default:
                    super.doGet(request, response);
                    return;
            }
        } catch (Exception ex) {
            logger.error(ex);
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }

        try {
            String actionType = request.getParameter("actionType");
            switch (actionType) {

                case "withdrawAllocation":
                    if (Utils.checkPermission(String.valueOf(userDTO.employee_record_id), userDTO, MenuConstants.AM_HOUSE_ALLOCATION_APPROVAL_MAPPING_SEARCH)) {
                        withdraw(request, userDTO);
                        response.sendRedirect("Am_house_allocation_approval_mappingServlet?actionType=search");
                        return;
                    }
                    break;

                case "rejectApplication":
                    if (Utils.checkPermission(String.valueOf(userDTO.employee_record_id), userDTO, MenuConstants.AM_HOUSE_ALLOCATION_APPROVAL_MAPPING_SEARCH)) {
                        approve(request, userDTO, false);
                        response.sendRedirect("Am_house_allocation_approval_mappingServlet?actionType=search");
                        return;
                    }
                    break;

                case "rejectApplicationSearchRow":
                    if (Utils.checkPermission(String.valueOf(userDTO.employee_record_id), userDTO, MenuConstants.AM_HOUSE_ALLOCATION_APPROVAL_MAPPING_SEARCH)) {
                        approve(request, userDTO, false);
                        try {
                            ApiResponse.sendSuccessResponse(response, "Am_house_allocation_approval_mappingServlet?actionType=search");
                        } catch (Exception ex) {
                            logger.error(ex);
                            ApiResponse.sendErrorResponse(response, ex.getMessage());
                        }
                        return;
                    }
                    break;

                case "approveApplication":
                    if (Utils.checkPermission(String.valueOf(userDTO.employee_record_id), userDTO, MenuConstants.AM_HOUSE_ALLOCATION_APPROVAL_MAPPING_SEARCH)) {
                        approve(request, userDTO, true);
                        try {
                            ApiResponse.sendSuccessResponse(response, "Am_house_allocationServlet?actionType=search");
                            //ApiResponse.makeSuccessResponse("Am_house_allocationServlet?actionType=search");
                        } catch (Exception ex) {
                            logger.error(ex);
                            ApiResponse.sendErrorResponse(response, ex.getMessage());
                        }
                        //response.sendRedirect("Am_house_allocation_approval_mappingServlet?actionType=search");
                        return;
                    }
                case "bulk_house_allocation":

                    if (Utils.checkPermission(String.valueOf(userDTO.employee_record_id), userDTO, MenuConstants.AM_HOUSE_ALLOCATION_APPROVAL_MAPPING_UPDATE)) {
                        Long am_house_allocation_requestId = bulkHouseAllocationRequestSubmit(request, userDTO,true);
                        if(am_house_allocation_requestId != -1){
                            bulkHouseAllocationSubmit(request, userDTO,am_house_allocation_requestId,true);
                        }
                        try {
                            ApiResponse.sendSuccessResponse(response, "Am_house_allocationServlet?actionType=search");
                            //ApiResponse.makeSuccessResponse("Am_house_allocationServlet?actionType=search");
                        } catch (Exception ex) {
                            logger.error(ex);
                            ApiResponse.sendErrorResponse(response, ex.getMessage());
                        }
                        //response.sendRedirect("Am_house_allocation_approval_mappingServlet?actionType=search");
                        return;
                    }

                    break;
            }
        } catch (Exception ex) {
            logger.error(ex);
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }

    private void bulkHouseAllocationSubmit(HttpServletRequest request, UserDTO userDTO, Long am_house_allocation_requestId, boolean isInsert) throws Exception {

        Am_house_allocationDTO am_house_allocationDTO;
        if(isInsert){
            am_house_allocationDTO = new Am_house_allocationDTO();
            am_house_allocationDTO.insertionDate = am_house_allocationDTO.lastModificationTime
                    = System.currentTimeMillis();
            am_house_allocationDTO.insertedByUserId = userDTO.ID;
            am_house_allocationDTO.insertedByOrganogramId = userDTO.organogramID;
        }
        else{
            am_house_allocationDTO = Am_house_allocationDAO.getInstance().getDTOFromID(Long.parseLong(request.getParameter("iD")));
            if (am_house_allocationDTO == null) {
                UtilCharacter.throwException("বাসা বরাদ্দ খুঁজে পাওয়া যায় নি  ", "House Allocation not found");
            }
            am_house_allocationDTO.lastModificationTime = System.currentTimeMillis();
            am_house_allocationDTO.modifiedBy = userDTO.ID;
        }



        Am_house_allocation_requestDTO am_house_allocation_requestDTO =  Am_house_allocation_requestDAO.getInstance().getDTOFromID(am_house_allocation_requestId);

        am_house_allocationDTO.requestId = am_house_allocation_requestDTO.iD;

        am_house_allocationDTO.requesterEmpId = am_house_allocation_requestDTO.requesterEmpId;
        am_house_allocationDTO.requesterOfficeId = am_house_allocation_requestDTO.requesterOfficeId;
        am_house_allocationDTO.requesterOfficeUnitId = am_house_allocation_requestDTO.requesterOfficeUnitId;
        am_house_allocationDTO.requesterOrgId = am_house_allocation_requestDTO.requesterOrgId;
        am_house_allocationDTO.requesterNameBn = am_house_allocation_requestDTO.requesterNameBn != null ? am_house_allocation_requestDTO.requesterNameBn: "";
        am_house_allocationDTO.requesterNameEn = am_house_allocation_requestDTO.requesterNameEn != null ? am_house_allocation_requestDTO.requesterNameEn : "";
        am_house_allocationDTO.requesterOfficeNameBn = am_house_allocation_requestDTO.requesterOfficeNameBn;
        am_house_allocationDTO.requesterOfficeNameEn = am_house_allocation_requestDTO.requesterOfficeNameEn;

        am_house_allocationDTO.requesterOfficeUnitNameEn = am_house_allocation_requestDTO.requesterOfficeUnitNameEn;
        am_house_allocationDTO.requesterOfficeUnitNameBn = am_house_allocation_requestDTO.requesterOfficeUnitNameBn;
        am_house_allocationDTO.requesterOfficeUnitOrgNameBn = am_house_allocation_requestDTO.requesterOfficeUnitOrgNameBn != null ? am_house_allocation_requestDTO.requesterOfficeUnitOrgNameBn : "";
        am_house_allocationDTO.requesterOfficeUnitOrgNameEn = am_house_allocation_requestDTO.requesterOfficeUnitOrgNameEn != null ? am_house_allocation_requestDTO.requesterOfficeUnitOrgNameEn : "";
        am_house_allocationDTO.requesterPhoneNum = am_house_allocation_requestDTO.requesterPhoneNum != null ? am_house_allocation_requestDTO.requesterPhoneNum : "";
        am_house_allocationDTO.status = CommonApprovalStatus.SATISFIED.getValue();

        am_house_allocationDTO.houseId = Long.parseLong(request.getParameter("amHouseId"));

        am_house_allocationDTO.amHouseOldNewCat = Integer.parseInt(request.getParameter("amHouseOldNewCat"));
        CategoryLanguageModel categoryLanguageModel = CatRepository.getInstance().getCategoryLanguageModel
                ("am_house_old_new", am_house_allocationDTO.amHouseOldNewCat);
        if (categoryLanguageModel == null) {
            UtilCharacter.throwException("নতুন বাসা/পুরাতন বাসা খুঁজে পাওয়া যায় নি", "House old/new is invalid");
        }

        am_house_allocationDTO.amHouseLocationCat = Integer.parseInt(request.getParameter("amHouseLocationCat"));
        categoryLanguageModel = CatRepository.getInstance().getCategoryLanguageModel
                ("am_house_location", am_house_allocationDTO.amHouseLocationCat);
        if (categoryLanguageModel == null) {
            UtilCharacter.throwException("বাসার অবস্থান খুঁজে পাওয়া যায় নি ", "House location is invalid");
        }

//        am_house_allocationDTO.amHouseAllocationQuota = Integer.parseInt(request.getParameter("amHouseAllocationQuota"));
//        categoryLanguageModel = CatRepository.getInstance().getCategoryLanguageModel
//                ("asset_house_allocation_quota", am_house_allocationDTO.amHouseAllocationQuota);
//        if (categoryLanguageModel == null) {
//            UtilCharacter.throwException("অনুগ্রহ করে বাসা বরাদ্দের কোটা প্রদান করুন", "Please provide quota for housing allocation");
//        }

        am_house_allocationDTO.amHouseClassCat = Integer.parseInt(request.getParameter("amHouseClassCat"));
        categoryLanguageModel = CatRepository.getInstance().getCategoryLanguageModel
                ("am_house_class", am_house_allocationDTO.amHouseClassCat);
        if (categoryLanguageModel == null) {
            UtilCharacter.throwException("বাসার শ্রেণী খুঁজে পাওয়া যায় নি ", "House class is invalid");
        }

        String houseSubClassCat = request.getParameter("amHouseClassSubCat");
        if(houseSubClassCat != null && !houseSubClassCat.equals("")){
            am_house_allocationDTO.amHouseClassSubCat = Integer.parseInt(houseSubClassCat);
        }

        am_house_allocationDTO.allocationDate = System.currentTimeMillis();

        if(isInsert){
             Long dtoId = Am_house_allocationDAO.getInstance().add(am_house_allocationDTO);

        }
        Am_houseDTO houseDTO = Am_houseRepository.getInstance().getAm_houseDTOByID(am_house_allocationDTO.houseId);
        houseDTO.status = CommonApprovalStatus.USED.getValue();
        houseDTO.lastModificationTime = am_house_allocationDTO.insertionDate;
        Am_houseDAO.getInstance().update(houseDTO);

    }

    private void setApprovalPageData(long amHouseAllocationRequestId, HttpServletRequest request, UserDTO userDTO) throws Exception {
        Am_house_allocation_requestDTO am_house_allocation_requestDTO = Am_house_allocation_requestDAO.getInstance().getDTOByID(amHouseAllocationRequestId);
        if (am_house_allocation_requestDTO == null)
            throw new Exception("No application for house allocation is not found with id = " + amHouseAllocationRequestId);
        request.setAttribute("am_house_allocation_requestDTO", am_house_allocation_requestDTO);

        Am_house_allocation_approval_mappingDTO approverAmHouseAllocationRequestDTO =
                Am_house_allocation_approval_mappingDAO.getInstance().getByAmHouseAllocationRequestAndApproverRecordsId(amHouseAllocationRequestId, userDTO.employee_record_id);

        if (approverAmHouseAllocationRequestDTO == null)
            throw new Exception("User Not allowed to approve  for house with  id = " + amHouseAllocationRequestId);
        request.setAttribute("approverAmHouseAllocationRequestDTO", approverAmHouseAllocationRequestDTO);
    }

    private Long bulkHouseAllocationRequestSubmit(HttpServletRequest request,UserDTO userDTO,boolean isInsert) throws Exception {

        Am_house_allocation_requestDTO am_house_allocation_requestDTO;
        if(isInsert){
            am_house_allocation_requestDTO = new Am_house_allocation_requestDTO();
            am_house_allocation_requestDTO.insertedByUserId = userDTO.ID;
            am_house_allocation_requestDTO.insertedByOrganogramId = userDTO.organogramID;
            am_house_allocation_requestDTO.insertionDate = TimeConverter.getToday();
        }
        else {
            am_house_allocation_requestDTO = Am_house_allocation_requestDAO.getInstance().getDTOFromID(Long.parseLong(request.getParameter("iD")));
        }

        am_house_allocation_requestDTO.lastModifierUser = userDTO.userName;
        am_house_allocation_requestDTO.status = CommonApprovalStatus.SATISFIED.getValue();

        String Value = request.getParameter("employeeRecordId");

        if(Value != null && !Value.equalsIgnoreCase(""))
        {
            Value = Jsoup.clean(Value,Whitelist.simpleText());
        }

        EmployeeSearchIds defaultOfficeIds = EmployeeSearchModalUtil.getDefaultOfficeIds(Long.parseLong(Value));
        EmployeeSearchModel model = EmployeeSearchModalUtil.getEmployeeSearchModel(defaultOfficeIds);
        am_house_allocation_requestDTO.requesterEmpId = model.employeeRecordId;
        am_house_allocation_requestDTO.requesterOfficeUnitId = model.officeUnitId;
        am_house_allocation_requestDTO.requesterOrgId = model.organogramId;

        am_house_allocation_requestDTO.requesterNameEn = model.employeeNameEn;
        am_house_allocation_requestDTO.requesterNameBn = model.employeeNameBn;
        am_house_allocation_requestDTO.requesterOfficeUnitNameEn = model.officeUnitNameEn;
        am_house_allocation_requestDTO.requesterOfficeUnitNameBn = model.officeUnitNameBn;
        am_house_allocation_requestDTO.requesterOfficeUnitOrgNameEn = model.organogramNameEn;
        am_house_allocation_requestDTO.requesterOfficeUnitOrgNameBn = model.organogramNameBn;

        am_house_allocation_requestDTO.requesterPhoneNum = model.phoneNumber;

        OfficesDTO requesterOffice = OfficesRepository.getInstance().getOfficesDTOByID(am_house_allocation_requestDTO.requesterOfficeId);

        if (requesterOffice != null) {
            am_house_allocation_requestDTO.requesterOfficeId = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(model.officeUnitId).officeId;

            am_house_allocation_requestDTO.requesterOfficeNameEn = requesterOffice.officeNameEng;
            am_house_allocation_requestDTO.requesterOfficeNameBn = requesterOffice.officeNameBng;
        }

        Value = request.getParameter("amHouseOldNewCat");

        if(Value != null && !Value.equalsIgnoreCase(""))
        {
            Value = Jsoup.clean(Value,Whitelist.simpleText());
        }
        System.out.println("amHouseOldNewCat = " + Value);
        if(Value != null && !Value.equalsIgnoreCase(""))
        {
            am_house_allocation_requestDTO.amHouseOldNewCat = Integer.parseInt(Value);
        }
        else
        {
            System.out.println("FieldName has a null Value, not updating" + " = " + Value);
        }

        Value = request.getParameter("amHouseLocationCat");

        if(Value != null && !Value.equalsIgnoreCase(""))
        {
            Value = Jsoup.clean(Value,Whitelist.simpleText());
        }
        System.out.println("amHouseLocationCat = " + Value);
        if(Value != null && !Value.equalsIgnoreCase(""))
        {
            am_house_allocation_requestDTO.amHouseLocationCat = Integer.parseInt(Value);
        }
        else
        {
            System.out.println("FieldName has a null Value, not updating" + " = " + Value);
        }

        Value = request.getParameter("amHouseClassCat");

        if(Value != null && !Value.equalsIgnoreCase(""))
        {
            Value = Jsoup.clean(Value,Whitelist.simpleText());
        }
        System.out.println("amHouseClassCat = " + Value);
        if(Value != null && !Value.equalsIgnoreCase(""))
        {
            am_house_allocation_requestDTO.amHouseClassCat = Integer.parseInt(Value);
        }
        else
        {
            System.out.println("FieldName has a null Value, not updating" + " = " + Value);
        }

        Value = request.getParameter("amHouseId");

        if(Value != null && !Value.equalsIgnoreCase(""))
        {
            Value = Jsoup.clean(Value,Whitelist.simpleText());
        }
        System.out.println("amHouseId = " + Value);
        if(Value != null && !Value.equalsIgnoreCase(""))
        {
            am_house_allocation_requestDTO.amHouseId = Long.parseLong(Value);
        }
        else
        {
            System.out.println("FieldName has a null Value, not updating" + " = " + Value);
        }

        Long am_house_allocation_requestDTOId =  -1L;
        if(isInsert)
        {
            boolean isWaitingForApproval = false;
            am_house_allocation_requestDTOId = Am_house_allocation_requestDAO.getInstance().add(am_house_allocation_requestDTO);
            Utils.handleTransaction(()->{

                AmHouseAllocationApprovalModel model1 =
                        new AmHouseAllocationApprovalModel.AmHouseAllocationApprovalModelBuilder()
                                .setAmHouseAllocationRequestDTO(am_house_allocation_requestDTO)
                                .setRequesterEmployeeRecordId(am_house_allocation_requestDTO.requesterEmpId)
                                .setTaskTypeId(TaskTypeEnum.AM_HOUSE_ASSIGNMENT.getValue())
                                .build();
                CardApprovalResponse cardApprovalResponse = Am_house_allocation_approval_mappingDAO.getInstance().createAmOfficeApproval(model1,isWaitingForApproval);

            });


        }


        return am_house_allocation_requestDTOId;

    }

    private void approve(HttpServletRequest request, UserDTO userDTO, boolean isAccepted) throws Exception {
        long requesterEmployeeRecordId = userDTO.employee_record_id;
        long amHouseAllocationRequestId = Long.parseLong(request.getParameter("amHouseAllocationRequestId"));
        Am_house_allocation_requestDTO amHouseAllocationRequestDTO = Am_house_allocation_requestDAO.getInstance().getDTOByID(amHouseAllocationRequestId);

        AmHouseAllocationApprovalModel model =
                new AmHouseAllocationApprovalModel.AmHouseAllocationApprovalModelBuilder()
                        .setAmHouseAllocationRequestDTO(amHouseAllocationRequestDTO)
                        .setRequesterEmployeeRecordId(userDTO.employee_record_id)
                        .setTaskTypeId(TaskTypeEnum.AM_HOUSE_ASSIGNMENT.getValue())
                        .build();

        EmployeeSearchModel employeeSearchModel = (gson.fromJson
                (EmployeeSearchModalUtil.getEmployeeSearchModelJson
                                (userDTO.employee_record_id,
                                        userDTO.unitID,
                                        userDTO.organogramID),
                        EmployeeSearchModel.class)
        );

        boolean sendNotificationToUser = false;
        if (isAccepted) {
            boolean isWaitingForApproval = true;
            CardApprovalResponse approvalResponse = Am_house_allocation_approval_mappingDAO.getInstance().movedToNextLevelOfApproval(model,isWaitingForApproval);
            if (!approvalResponse.hasNextApproval) {

                amHouseAllocationRequestDTO.approverEmpId = employeeSearchModel.employeeRecordId;
                amHouseAllocationRequestDTO.approverOfficeUnitId = employeeSearchModel.officeUnitId;
                amHouseAllocationRequestDTO.approverOrgId = employeeSearchModel.organogramId;

                amHouseAllocationRequestDTO.approverNameEn = employeeSearchModel.employeeNameEn;
                amHouseAllocationRequestDTO.approverNameBn = employeeSearchModel.employeeNameBn;
                amHouseAllocationRequestDTO.approverOfficeUnitNameEn = employeeSearchModel.officeUnitNameEn;
                amHouseAllocationRequestDTO.approverOfficeUnitNameBn = employeeSearchModel.officeUnitNameBn;
                amHouseAllocationRequestDTO.approverOfficeUnitOrgNameEn = employeeSearchModel.organogramNameEn;
                amHouseAllocationRequestDTO.approverOfficeUnitOrgNameBn = employeeSearchModel.organogramNameBn;

                amHouseAllocationRequestDTO.approverPhoneNum = employeeSearchModel.phoneNumber;

                OfficesDTO approverOffice = OfficesRepository.getInstance().getOfficesDTOByID(amHouseAllocationRequestDTO.approverOfficeId);

                if (approverOffice != null) {
                    amHouseAllocationRequestDTO.approverOfficeId = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(employeeSearchModel.officeUnitId).officeId;

                    amHouseAllocationRequestDTO.approverOfficeNameEn = approverOffice.officeNameEng;
                    amHouseAllocationRequestDTO.approverOfficeNameBn = approverOffice.officeNameBng;
                }

                amHouseAllocationRequestDTO.status = AmHouseAllocationRequestStatus.APPROVED.getValue();
                amHouseAllocationRequestDTO.lastModificationTime = System.currentTimeMillis();
                amHouseAllocationRequestDTO.lastModifierUser = String.valueOf(requesterEmployeeRecordId);
                Am_house_allocation_requestDAO.getInstance().update(amHouseAllocationRequestDTO);
                sendNotificationToUser = true;

                
                Am_house_allocation_approval_mappingDAO.getInstance()
                        .withdrawByRequesterOrgId(
                                amHouseAllocationRequestDTO.requesterOrgId,
                                amHouseAllocationRequestDTO.iD,
                                userDTO,
                                "Another house allocation is approved"
                        );

            } else {
                AmHouseAllocationNotification.getInstance().sendWaitingForApprovalNotification(
                        approvalResponse.organogramIds, amHouseAllocationRequestDTO
                );
            }
        } else {
            String rejectReason = request.getParameter("reject_reason");
            if (rejectReason != null) {
                rejectReason = Jsoup.clean(rejectReason, Whitelist.simpleText());
                rejectReason = rejectReason.replace("'", " ");
                rejectReason = rejectReason.replace("\"", " ");
            } else {
                rejectReason = "";
            }
            Am_house_allocation_approval_mappingDAO.getInstance().rejectPendingApproval(model, rejectReason);

            amHouseAllocationRequestDTO.approverEmpId = employeeSearchModel.employeeRecordId;
            amHouseAllocationRequestDTO.approverOfficeUnitId = employeeSearchModel.officeUnitId;
            amHouseAllocationRequestDTO.approverOrgId = employeeSearchModel.organogramId;

            amHouseAllocationRequestDTO.approverNameEn = employeeSearchModel.employeeNameEn;
            amHouseAllocationRequestDTO.approverNameBn = employeeSearchModel.employeeNameBn;
            amHouseAllocationRequestDTO.approverOfficeUnitNameEn = employeeSearchModel.officeUnitNameEn;
            amHouseAllocationRequestDTO.approverOfficeUnitNameBn = employeeSearchModel.officeUnitNameBn;
            amHouseAllocationRequestDTO.approverOfficeUnitOrgNameEn = employeeSearchModel.organogramNameEn;
            amHouseAllocationRequestDTO.approverOfficeUnitOrgNameBn = employeeSearchModel.organogramNameBn;

            amHouseAllocationRequestDTO.approverPhoneNum = employeeSearchModel.phoneNumber;

            OfficesDTO approverOffice = OfficesRepository.getInstance().getOfficesDTOByID(amHouseAllocationRequestDTO.approverOfficeId);

            if (approverOffice != null) {
                amHouseAllocationRequestDTO.approverOfficeId = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(employeeSearchModel.officeUnitId).officeId;

                amHouseAllocationRequestDTO.approverOfficeNameEn = approverOffice.officeNameEng;
                amHouseAllocationRequestDTO.approverOfficeNameBn = approverOffice.officeNameBng;
            }
            amHouseAllocationRequestDTO.status = AmHouseAllocationRequestStatus.REJECTED.getValue();
            amHouseAllocationRequestDTO.rejectionReason = rejectReason;
            Am_house_allocation_requestDAO.getInstance().update(amHouseAllocationRequestDTO);
            sendNotificationToUser = true;
        }

        if (sendNotificationToUser) {
            AmHouseAllocationNotification.getInstance().sendApproveOrRejectNotification(
                    Collections.singletonList(amHouseAllocationRequestDTO.requesterOrgId), amHouseAllocationRequestDTO, isAccepted
            );
        }
    }

    private void withdraw(HttpServletRequest request, UserDTO userDTO) throws Exception {

        long amHouseAllocationRequestId = Long.parseLong(request.getParameter("amHouseAllocationRequestId"));
        String withdrawalReason = request.getParameter("withdrawal_reason");

        Am_house_allocation_requestDTO amHouseAllocationRequestDTO = Am_house_allocation_requestDAO.getInstance().getDTOByID(amHouseAllocationRequestId);

        Am_house_allocation_approval_mappingDAO.getInstance().withdraw(amHouseAllocationRequestDTO, userDTO, withdrawalReason);
    }


}

