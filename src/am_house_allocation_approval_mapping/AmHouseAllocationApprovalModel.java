package am_house_allocation_approval_mapping;
import am_house_allocation_request.Am_house_allocation_requestDTO;

public class AmHouseAllocationApprovalModel {
    private final long taskTypeId;
    private final long requesterEmployeeRecordId;
    private final Am_house_allocation_requestDTO amHouseAllocationRequestDTO;

    private AmHouseAllocationApprovalModel(AmHouseAllocationApprovalModelBuilder builder) {
        taskTypeId = builder.taskTypeId;
        requesterEmployeeRecordId = builder.requesterEmployeeRecordId;
        amHouseAllocationRequestDTO = builder.amHouseAllocationRequestDTO;
    }

    public long getTaskTypeId() {
        return taskTypeId;
    }

    public long getRequesterEmployeeRecordId() {
        return requesterEmployeeRecordId;
    }

    public Am_house_allocation_requestDTO getAmHouseAllocationRequestDTO() {
        return amHouseAllocationRequestDTO;
    }

    public static class AmHouseAllocationApprovalModelBuilder {
        private long taskTypeId;
        private long requesterEmployeeRecordId;
        private Am_house_allocation_requestDTO amHouseAllocationRequestDTO;

        public AmHouseAllocationApprovalModel build() {
            return new AmHouseAllocationApprovalModel(this);
        }

        public AmHouseAllocationApprovalModelBuilder setTaskTypeId(long taskTypeId) {
            this.taskTypeId = taskTypeId;
            return this;
        }

        public AmHouseAllocationApprovalModelBuilder setRequesterEmployeeRecordId(long requesterEmployeeRecordId) {
            this.requesterEmployeeRecordId = requesterEmployeeRecordId;
            return this;
        }

        public AmHouseAllocationApprovalModelBuilder setAmHouseAllocationRequestDTO(Am_house_allocation_requestDTO amHouseAllocationRequestDTO) {
            this.amHouseAllocationRequestDTO = amHouseAllocationRequestDTO;
            return this;
        }
    }
}
