package am_house_allocation_approval_mapping;
import java.util.*; 
import util.*; 


public class Am_house_allocation_approval_mappingDTO extends CommonDTO
{

	public long amHouseAllocationRequestId = -1;
	public long cardApprovalId = -1;
	public int sequence = -1;
	public int amHouseAllocationStatusCat = -1;
	public long taskTypeId = -1;
	public long employeeRecordsId = -1;
	public long approverEmployeeRecordsId = -1;
    public String rejectionReason = "";
	public long insertionDate = -1;
    public String insertedBy = "";
    public String modifiedBy = "";
	
	
    @Override
	public String toString() {
            return "$Am_house_allocation_approval_mappingDTO[" +
            " iD = " + iD +
            " amHouseAllocationRequestId = " + amHouseAllocationRequestId +
            " cardApprovalId = " + cardApprovalId +
            " sequence = " + sequence +
            " amHouseAllocationStatusCat = " + amHouseAllocationStatusCat +
            " taskTypeId = " + taskTypeId +
            " employeeRecordsId = " + employeeRecordsId +
            " approverEmployeeRecordsId = " + approverEmployeeRecordsId +
            " rejectionReason = " + rejectionReason +
            " insertionDate = " + insertionDate +
            " insertedBy = " + insertedBy +
            " modifiedBy = " + modifiedBy +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            " searchColumn = " + searchColumn +
            "]";
    }

}