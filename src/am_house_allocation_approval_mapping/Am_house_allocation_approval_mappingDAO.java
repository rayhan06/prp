package am_house_allocation_approval_mapping;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.sql.SQLException;
import java.util.stream.Collectors;

import am_house.Am_houseDAO;
import am_house.Am_houseDTO;
import am_house.Am_houseRepository;
import am_house_allocation.Am_house_allocationDAO;
import am_house_allocation.Am_house_allocationDTO;
import am_house_allocation_request.AmHouseAllocationRequestStatus;
import am_house_allocation_request.Am_house_allocation_requestDAO;
import am_house_allocation_request.Am_house_allocation_requestDTO;
import am_office_approval_mapping.AmOfficeApprovalModel;
import am_office_approval_mapping.Am_office_approval_mappingDAO;
import am_office_approval_mapping.Am_office_approval_mappingDTO;
import am_office_assignment_request.AmOfficeAssignmentRequestStatus;
import card_info.*;
import com.google.gson.Gson;
import common.CommonDAOService;
import common.ConnectionAndStatementUtil;
import employee_assign.EmployeeSearchModalUtil;
import employee_assign.EmployeeSearchModel;
import employee_offices.EmployeeOfficeDTO;
import employee_offices.EmployeeOfficeRepository;
import office_units.Office_unitsRepository;
import offices.OfficesDTO;
import offices.OfficesRepository;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import task_type.TaskTypeEnum;
import task_type_approval_path.TaskTypeApprovalPathDTO;
import task_type_approval_path.TaskTypeApprovalPathRepository;
import user.UserDTO;
import util.*;
import pb.*;
import vm_requisition.CommonApprovalStatus;

import javax.servlet.http.HttpServletRequest;

public class Am_house_allocation_approval_mappingDAO  implements CommonDAOService<Am_house_allocation_approval_mappingDTO>
{
	
	Logger logger = Logger.getLogger(getClass());

	private static final String getByAmHouseAllocationRequestId =
			"SELECT * FROM am_house_allocation_approval_mapping WHERE am_house_allocation_request_id = %d AND isDeleted = 0 ORDER BY sequence DESC";

	private static final String getByAmHouseAllocationRequestAndApproverRecordsId =
			"SELECT * FROM am_house_allocation_approval_mapping WHERE am_house_allocation_request_id = %d AND approver_employee_records_id = %d";

	private static final String getByAmHouseAllocationRequestIdAndStatus =
			"SELECT * FROM am_house_allocation_approval_mapping WHERE am_house_allocation_request_id = %d AND am_house_allocation_status_cat = %d AND isDeleted = 0";

	private static final String updateStatus =
			"UPDATE am_house_allocation_approval_mapping SET am_house_allocation_status_cat = %d,isDeleted = %d,modified_by = %d,lastModificationTime = %d WHERE ID IN (%s)";

	private static final String updateStatusByAmHouseAllocationRequestId =
			"UPDATE am_house_allocation_approval_mapping SET am_house_allocation_status_cat = ?, rejection_reason = ?,isDeleted =?," +
					"modified_by = ?,lastModificationTime = ? WHERE am_house_allocation_status_cat = ? AND am_house_allocation_request_id = ?";


	
	private static String addQuery = "";
	private static String updateQuery = "";
	private String[] columnNames = null;
	private Gson gson = null;

	public Am_house_allocation_approval_mappingDAO()
	{
		columnNames = new String[] 
		{
			"ID",
			"am_house_allocation_request_id",
			"card_approval_id",
			"sequence",
			"am_house_allocation_status_cat",
			"task_type_id",
			"employee_records_id",
			"approver_employee_records_id",
			"rejection_reason",
			"insertion_date",
			"inserted_by",
			"modified_by",
			"search_column",
			"isDeleted",
			"lastModificationTime"
		};
		updateQuery = getUpdateQuery(columnNames);
        addQuery = getAddQuery(columnNames);

		searchMap.put("approverEmployeeRecordsId", " AND (approver_employee_records_id = ?)");
		searchMap.put("employeeRecordsId", " AND (employee_records_id = ?)");
		
		searchMap.put("am_house_allocation_status_cat"," and (am_house_allocation_status_cat = ?)");
		searchMap.put("insertion_date_start"," and (insertion_date >= ?)");
		searchMap.put("insertion_date_end"," and (insertion_date <= ?)");
		gson = new Gson();
	}

	private final Map<String,String> searchMap = new HashMap<>();

    private static class LazyLoader
    {
    	static final Am_house_allocation_approval_mappingDAO INSTANCE = new Am_house_allocation_approval_mappingDAO();
	}

	public static Am_house_allocation_approval_mappingDAO getInstance()
	{
    	return LazyLoader.INSTANCE;
	}


	
	public void setSearchColumn(Am_house_allocation_approval_mappingDTO am_house_allocation_approval_mappingDTO)
	{
		am_house_allocation_approval_mappingDTO.searchColumn = "";
		am_house_allocation_approval_mappingDTO.searchColumn += CatDAO.getName("English", "am_house_allocation_status", am_house_allocation_approval_mappingDTO.amHouseAllocationStatusCat) + " " + CatDAO.getName("Bangla", "am_house_allocation_status", am_house_allocation_approval_mappingDTO.amHouseAllocationStatusCat) + " ";
	}
	
	@Override
	public void set(PreparedStatement ps, Am_house_allocation_approval_mappingDTO am_house_allocation_approval_mappingDTO, boolean isInsert) throws SQLException
	{
		int index = 0;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(am_house_allocation_approval_mappingDTO);
		if(isInsert)
		{
			ps.setObject(++index,am_house_allocation_approval_mappingDTO.iD);
		}
		ps.setObject(++index,am_house_allocation_approval_mappingDTO.amHouseAllocationRequestId);
		ps.setObject(++index,am_house_allocation_approval_mappingDTO.cardApprovalId);
		ps.setObject(++index,am_house_allocation_approval_mappingDTO.sequence);
		ps.setObject(++index,am_house_allocation_approval_mappingDTO.amHouseAllocationStatusCat);
		ps.setObject(++index,am_house_allocation_approval_mappingDTO.taskTypeId);
		ps.setObject(++index,am_house_allocation_approval_mappingDTO.employeeRecordsId);
		ps.setObject(++index,am_house_allocation_approval_mappingDTO.approverEmployeeRecordsId);
		ps.setObject(++index,am_house_allocation_approval_mappingDTO.rejectionReason);
		ps.setObject(++index,am_house_allocation_approval_mappingDTO.insertionDate);
		ps.setObject(++index,am_house_allocation_approval_mappingDTO.insertedBy);
		ps.setObject(++index,am_house_allocation_approval_mappingDTO.modifiedBy);
		ps.setObject(++index,am_house_allocation_approval_mappingDTO.searchColumn);
		if(isInsert)
		{
			ps.setObject(++index,am_house_allocation_approval_mappingDTO.isDeleted);
		}
		ps.setObject(++index, lastModificationTime);
		if(!isInsert)
		{
			ps.setObject(++index,am_house_allocation_approval_mappingDTO.iD);
		}
	}
	
	@Override
	public Am_house_allocation_approval_mappingDTO buildObjectFromResultSet(ResultSet rs)
	{
		try
		{
			Am_house_allocation_approval_mappingDTO am_house_allocation_approval_mappingDTO = new Am_house_allocation_approval_mappingDTO();
			int i = 0;
			am_house_allocation_approval_mappingDTO.iD = rs.getLong(columnNames[i++]);
			am_house_allocation_approval_mappingDTO.amHouseAllocationRequestId = rs.getLong(columnNames[i++]);
			am_house_allocation_approval_mappingDTO.cardApprovalId = rs.getLong(columnNames[i++]);
			am_house_allocation_approval_mappingDTO.sequence = rs.getInt(columnNames[i++]);
			am_house_allocation_approval_mappingDTO.amHouseAllocationStatusCat = rs.getInt(columnNames[i++]);
			am_house_allocation_approval_mappingDTO.taskTypeId = rs.getLong(columnNames[i++]);
			am_house_allocation_approval_mappingDTO.employeeRecordsId = rs.getLong(columnNames[i++]);
			am_house_allocation_approval_mappingDTO.approverEmployeeRecordsId = rs.getLong(columnNames[i++]);
			am_house_allocation_approval_mappingDTO.rejectionReason = rs.getString(columnNames[i++]);
			am_house_allocation_approval_mappingDTO.insertionDate = rs.getLong(columnNames[i++]);
			am_house_allocation_approval_mappingDTO.insertedBy = rs.getString(columnNames[i++]);
			am_house_allocation_approval_mappingDTO.modifiedBy = rs.getString(columnNames[i++]);
			am_house_allocation_approval_mappingDTO.searchColumn = rs.getString(columnNames[i++]);
			am_house_allocation_approval_mappingDTO.isDeleted = rs.getInt(columnNames[i++]);
			am_house_allocation_approval_mappingDTO.lastModificationTime = rs.getLong(columnNames[i++]);

			return am_house_allocation_approval_mappingDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
		
	public Am_house_allocation_approval_mappingDTO getDTOByID (long id)
	{
		return getDTOFromID(id);
	}

	@Override
	public String getTableName() {
		return "am_house_allocation_approval_mapping";
	}

	@Override
	public Map<String, String> getSearchMap() {
		return searchMap;
	}

	public long add(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Am_house_allocation_approval_mappingDTO) commonDTO,addQuery,true);
	}

	public long update(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Am_house_allocation_approval_mappingDTO) commonDTO,updateQuery,false);
	}
	
	@Override
	public String getLastModifierUser(){
		return "modified_by";
    }

	public CardApprovalResponse createAmOfficeApproval(AmHouseAllocationApprovalModel model,boolean isWaitingForApproval) throws Exception {
		String sql = String.format(getByAmHouseAllocationRequestId, model.getAmHouseAllocationRequestDTO().iD);
		Am_house_allocation_approval_mappingDTO approvalMappingDTO = ConnectionAndStatementUtil.getT(sql, this::buildObjectFromResultSet);
		if (approvalMappingDTO != null) {
			throw new DuplicateCardInfoException("Approval is already created for " + model.getAmHouseAllocationRequestDTO().iD);
		}
		return movedToNextLevel(model, 1,isWaitingForApproval);
	}

	private CardApprovalResponse movedToNextLevel(AmHouseAllocationApprovalModel model, int nextLevel,boolean isWaitingForApproval) {
		CardApprovalResponse response = new CardApprovalResponse();
		List<TaskTypeApprovalPathDTO> nextApprovalPath = TaskTypeApprovalPathRepository.getInstance()
				.getByTaskTypeIdAndLevel(model.getTaskTypeId(), nextLevel);

		if (nextApprovalPath == null || nextApprovalPath.size() == 0) {
			response.hasNextApproval = false;
			response.organogramIds = null;
		} else {
			List<Long> organogramIds = addToApprovalAndGetAddedOrganogramIds(nextApprovalPath, model, nextLevel,isWaitingForApproval);
			response.hasNextApproval = true;
			response.organogramIds = organogramIds;
		}
		return response;
	}

	private List<Long> addToApprovalAndGetAddedOrganogramIds(List<TaskTypeApprovalPathDTO> nextApprovalPath, AmHouseAllocationApprovalModel model, int level,
															 boolean isWaitingForApproval) {
		List<Long> addedToApproval = new ArrayList<>();
		nextApprovalPath.forEach(approver -> {
			EmployeeOfficeDTO approverOfficeDTO = EmployeeOfficeRepository.getInstance().getByOfficeUnitOrganogramId(approver.officeUnitOrganogramId);
			if (approverOfficeDTO != null) {
				Am_house_allocation_approval_mappingDTO dto = buildDTO(
						approverOfficeDTO.employeeRecordId, model, level,
						0, isWaitingForApproval?AmHouseAllocationRequestStatus.WAITING_FOR_APPROVAL:AmHouseAllocationRequestStatus.APPROVED
				);
				if (dto != null) {
					try {
						add(dto);
						addedToApproval.add(approverOfficeDTO.officeUnitOrganogramId);
					} catch (Exception e) {
						logger.error(e);
					}
				}
			}
		});
		return addedToApproval;
	}

	private Am_house_allocation_approval_mappingDTO buildDTO(long approverEmployeeRecordId, AmHouseAllocationApprovalModel model, int level,
												   int isDeleted, AmHouseAllocationRequestStatus amHouseAllocationRequestStatus) {
		Am_house_allocation_approval_mappingDTO dto = new Am_house_allocation_approval_mappingDTO();
		CardApprovalDTO cardApprovalDTO = CardApprovalRepository.getInstance()
				.getByEmployeeRecordId(approverEmployeeRecordId, model.getRequesterEmployeeRecordId());
		if (cardApprovalDTO == null) {
			return null;
		}

		dto.amHouseAllocationRequestId = model.getAmHouseAllocationRequestDTO().iD;
		dto.amHouseAllocationStatusCat = amHouseAllocationRequestStatus.getValue();
		dto.cardApprovalId = cardApprovalDTO.iD;
		dto.sequence = level;
		dto.taskTypeId = model.getTaskTypeId();
		dto.insertedBy = dto.modifiedBy = String.valueOf(model.getRequesterEmployeeRecordId());
		dto.insertionDate = dto.lastModificationTime = System.currentTimeMillis();
		dto.isDeleted = isDeleted;
		dto.employeeRecordsId = model.getAmHouseAllocationRequestDTO().requesterEmpId;
		dto.approverEmployeeRecordsId = cardApprovalDTO.employeeRecordId;

		return dto;
	}

	public Am_house_allocation_approval_mappingDTO getByAmHouseAllocationRequestAndApproverRecordsId(long amHouseAllocationRequestId, long approverEmployeeId) {
		String sql = String.format(getByAmHouseAllocationRequestAndApproverRecordsId, amHouseAllocationRequestId, approverEmployeeId);
		return ConnectionAndStatementUtil.getT(sql, this::buildObjectFromResultSet);
	}

	private static class ValidApprovalInfo {
		List<Am_house_allocation_approval_mappingDTO> dtoList;
		int currentLevel;
		Am_house_allocation_approval_mappingDTO approvalMappingDTO;

		public ValidApprovalInfo(List<Am_house_allocation_approval_mappingDTO> dtoList, int currentLevel, Am_house_allocation_approval_mappingDTO approvalMappingDTO) {
			this.dtoList = dtoList;
			this.currentLevel = currentLevel;
			this.approvalMappingDTO = approvalMappingDTO;
		}
	}

	public ValidApprovalInfo validateInput(AmHouseAllocationApprovalModel model, int currentStatus) throws InvalidDataException, AlreadyApprovedException {
		String sql = String.format(
				getByAmHouseAllocationRequestIdAndStatus,
				model.getAmHouseAllocationRequestDTO().iD,
				currentStatus
		);

		List<Am_house_allocation_approval_mappingDTO> dtoList = ConnectionAndStatementUtil.getListOfT(sql, this::buildObjectFromResultSet);
		if (dtoList == null || dtoList.size() == 0) {
			throw new InvalidDataException("No Pending approval is found for requestId : " + model.getAmHouseAllocationRequestDTO().iD);
		}

		Am_house_allocation_approval_mappingDTO requesterApprovalDTO =
				dtoList.stream()
						.filter(dto -> dto.approverEmployeeRecordsId == model.getRequesterEmployeeRecordId())
						.findAny()
						.orElse(null);

		if (requesterApprovalDTO == null) {
			sql = String.format(getByAmHouseAllocationRequestAndApproverRecordsId, model.getAmHouseAllocationRequestDTO().iD, model.getRequesterEmployeeRecordId());
			requesterApprovalDTO = ConnectionAndStatementUtil.getT(sql, this::buildObjectFromResultSet);
			if (requesterApprovalDTO != null) {
				throw new AlreadyApprovedException();
			}
			throw new InvalidDataException("Invalid employee request to approve");
		}

		Set<Integer> sequenceValueSet = dtoList.stream()
				.map(e -> e.sequence)
				.collect(Collectors.toSet());
		if (sequenceValueSet.size() > 1) {
			throw new InvalidDataException(
					"Multiple sequence value is found for Pending approval of requestID : "
							+ model.getAmHouseAllocationRequestDTO().iD
			);
		}
		return new ValidApprovalInfo(dtoList, (Integer) sequenceValueSet.toArray()[0], requesterApprovalDTO);

	}

	public CardApprovalResponse movedToNextLevelOfApproval(AmHouseAllocationApprovalModel model,boolean isWaitingForApproval) throws InvalidDataException, AlreadyApprovedException {
		synchronized (LockManager.getLock(model.getAmHouseAllocationRequestDTO().iD + "AMHTNLA")) {
//			ValidApprovalInfo validApprovalInfo = validateInput(model,AmHouseAllocationRequestStatus.WAITING_FOR_APPROVAL.getValue());
			ValidApprovalInfo validApprovalInfo = validateInput(model,CommonApprovalStatus.REQUISITION_APPROVED_BY_FIRST_APPROVER.getValue());

			String idsToUpdate =
					validApprovalInfo.dtoList.stream()
							.map(e -> String.valueOf(e.iD))
							.collect(Collectors.joining(","));

			String sql = String.format(
					updateStatus, AmHouseAllocationRequestStatus.APPROVED.getValue(),
					0, model.getRequesterEmployeeRecordId(), System.currentTimeMillis(), idsToUpdate
			);

			boolean res = (Boolean) ConnectionAndStatementUtil.getWriteStatement(st -> {
				try {
					st.executeUpdate(sql);
					return true;
				} catch (SQLException ex) {
					logger.error(ex);
					return false;
				}
			});
			if (!res) {
				throw new InvalidDataException("Exception occurred during updating approval status");
			}
			return movedToNextLevel(model, validApprovalInfo.currentLevel + 1,isWaitingForApproval);
		}
	}

	public boolean rejectPendingApproval(AmHouseAllocationApprovalModel model, String rejectReason) throws InvalidDataException, AlreadyApprovedException {
		synchronized (LockManager.getLock(model.getAmHouseAllocationRequestDTO().iD + "AMDHRejectPA")) {
			validateInput(model,AmHouseAllocationRequestStatus.APPROVED_BY_FIRST_APPROVER.getValue());
			return (Boolean) ConnectionAndStatementUtil.getWritePrepareStatement(ps -> {
				try {
					int index = 0;

					ps.setInt(++index, AmHouseAllocationRequestStatus.REJECTED.getValue());
					ps.setString(++index, rejectReason);
					ps.setInt(++index, 0);
					ps.setLong(++index, model.getRequesterEmployeeRecordId());
					ps.setLong(++index, System.currentTimeMillis());
					ps.setInt(++index, AmHouseAllocationRequestStatus.APPROVED_BY_FIRST_APPROVER.getValue());
					ps.setObject(++index, model.getAmHouseAllocationRequestDTO().iD);

					logger.info(ps);
					ps.executeUpdate();
					return true;
				} catch (SQLException ex) {
					logger.error(ex);
					return false;
				}
			}, updateStatusByAmHouseAllocationRequestId);
		}
	}

	public boolean withdrawApprovedApproval(AmHouseAllocationApprovalModel model, String withdrawReason) throws InvalidDataException, AlreadyApprovedException {
		synchronized (LockManager.getLock(model.getAmHouseAllocationRequestDTO().iD + "AMHWithdrawPA")) {
			validateInput(model,AmHouseAllocationRequestStatus.APPROVED.getValue());
			return (Boolean) ConnectionAndStatementUtil.getWritePrepareStatement(ps -> {
				try {
					int index = 0;

					ps.setInt(++index, AmOfficeAssignmentRequestStatus.WITHDRAWN.getValue());
					ps.setString(++index, withdrawReason);
					ps.setInt(++index, 0);
					ps.setLong(++index, model.getRequesterEmployeeRecordId());
					ps.setLong(++index, System.currentTimeMillis());
					ps.setInt(++index, AmOfficeAssignmentRequestStatus.APPROVED.getValue());
					ps.setObject(++index, model.getAmHouseAllocationRequestDTO().iD);

					logger.info(ps);
					ps.executeUpdate();
					return true;
				} catch (SQLException ex) {
					logger.error(ex);
					return false;
				}
			}, updateStatusByAmHouseAllocationRequestId);
		}
	}

	public void withdrawByRequesterOrgId(Long requesterOrgId, Long amHouseAllocationRequestIdExcept, UserDTO userDTO, String withdrawalReason) throws Exception {

		List<Am_house_allocation_requestDTO> existing = Am_house_allocation_requestDAO.getInstance()
				.getExistingRequestByOrgId(requesterOrgId);

		for (Am_house_allocation_requestDTO amHouseAllocationRequestDTO : existing) {
			if (amHouseAllocationRequestDTO.iD == amHouseAllocationRequestIdExcept) continue;
			withdraw(amHouseAllocationRequestDTO, userDTO, withdrawalReason);
		}

	}

	public void withdraw(Am_house_allocation_requestDTO amHouseAllocationRequestDTO, UserDTO userDTO, String withdrawalReason) throws Exception {

		AmHouseAllocationApprovalModel model =
				new AmHouseAllocationApprovalModel.AmHouseAllocationApprovalModelBuilder()
						.setAmHouseAllocationRequestDTO(amHouseAllocationRequestDTO)
						.setRequesterEmployeeRecordId(userDTO.employee_record_id)
						.setTaskTypeId(TaskTypeEnum.AM_HOUSE_ASSIGNMENT.getValue())
						.build();

		EmployeeSearchModel employeeSearchModel = (gson.fromJson
				(EmployeeSearchModalUtil.getEmployeeSearchModelJson
								(userDTO.employee_record_id,
										userDTO.unitID,
										userDTO.organogramID),
						EmployeeSearchModel.class)
		);

		boolean sendNotificationToUser = false;
		long currentTime = System.currentTimeMillis();


		Am_house_allocationDTO am_house_allocationDTO = Am_house_allocationDAO.getInstance().getByAmHouseAllocationRequestId(amHouseAllocationRequestDTO.iD);

		Am_houseDTO houseDTO = Am_houseRepository.getInstance().getAm_houseDTOByID(am_house_allocationDTO.houseId);
		houseDTO.status = CommonApprovalStatus.AVAILABLE.getValue();
		houseDTO.lastModificationTime = currentTime;
		Am_houseDAO.getInstance().update(houseDTO);

		if (withdrawalReason != null) {
			withdrawalReason = Jsoup.clean(withdrawalReason, Whitelist.simpleText());
			withdrawalReason = withdrawalReason.replace("'", " ");
			withdrawalReason = withdrawalReason.replace("\"", " ");
		} else {
			withdrawalReason = "";
		}
		Am_house_allocation_approval_mappingDAO.getInstance().withdrawApprovedApproval(model, withdrawalReason);


		amHouseAllocationRequestDTO.withdrawerEmpId = employeeSearchModel.employeeRecordId;
		amHouseAllocationRequestDTO.withdrawerOfficeUnitId = employeeSearchModel.officeUnitId;
		amHouseAllocationRequestDTO.withdrawerOrgId = employeeSearchModel.organogramId;

		amHouseAllocationRequestDTO.withdrawerNameEn = employeeSearchModel.employeeNameEn;
		amHouseAllocationRequestDTO.withdrawerNameBn = employeeSearchModel.employeeNameBn;
		amHouseAllocationRequestDTO.withdrawerOfficeUnitNameEn = employeeSearchModel.officeUnitNameEn;
		amHouseAllocationRequestDTO.withdrawerOfficeUnitNameBn = employeeSearchModel.officeUnitNameBn;
		amHouseAllocationRequestDTO.withdrawerOfficeUnitOrgNameEn = employeeSearchModel.organogramNameEn;
		amHouseAllocationRequestDTO.withdrawerOfficeUnitOrgNameBn = employeeSearchModel.organogramNameBn;

		amHouseAllocationRequestDTO.withdrawerPhoneNum = employeeSearchModel.phoneNumber;

		OfficesDTO withdrawerOffice = OfficesRepository.getInstance().getOfficesDTOByID(amHouseAllocationRequestDTO.withdrawerOfficeId);

		if (withdrawerOffice != null) {
			amHouseAllocationRequestDTO.withdrawerOfficeId = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(employeeSearchModel.officeUnitId).officeId;

			amHouseAllocationRequestDTO.withdrawerOfficeNameEn = withdrawerOffice.officeNameEng;
			amHouseAllocationRequestDTO.withdrawerOfficeNameBn = withdrawerOffice.officeNameBng;
		}


		amHouseAllocationRequestDTO.status = AmHouseAllocationRequestStatus.WITHDRAWN.getValue();
		amHouseAllocationRequestDTO.withdrawalReason = withdrawalReason;
		amHouseAllocationRequestDTO.withdrawalDate = currentTime;
		Am_house_allocation_requestDAO.getInstance().update(amHouseAllocationRequestDTO);


		am_house_allocationDTO.status = CommonApprovalStatus.WITHDRAWN.getValue();
		am_house_allocationDTO.withdrawn_date = currentTime;
		am_house_allocationDTO.lastModificationTime = currentTime;
		am_house_allocationDTO.modifiedBy = userDTO.ID;
		Am_house_allocationDAO.getInstance().update(am_house_allocationDTO);
		sendNotificationToUser = true;


		if (sendNotificationToUser) {
			AmHouseAllocationNotification.getInstance().sendWithdrawalNotification(
					Collections.singletonList(amHouseAllocationRequestDTO.requesterOrgId), amHouseAllocationRequestDTO
			);
		}
	}

	public void approve(HttpServletRequest request, UserDTO userDTO,Long amHouseAllocationRequestId, boolean isAccepted) throws Exception {
		long requesterEmployeeRecordId = userDTO.employee_record_id;
		//long amHouseAllocationRequestId = Long.parseLong(request.getParameter("amHouseAllocationRequestId"));
		Am_house_allocation_requestDTO amHouseAllocationRequestDTO = Am_house_allocation_requestDAO.getInstance().getDTOByID(amHouseAllocationRequestId);

		AmHouseAllocationApprovalModel model =
				new AmHouseAllocationApprovalModel.AmHouseAllocationApprovalModelBuilder()
						.setAmHouseAllocationRequestDTO(amHouseAllocationRequestDTO)
						.setRequesterEmployeeRecordId(userDTO.employee_record_id)
						.setTaskTypeId(TaskTypeEnum.AM_HOUSE_ASSIGNMENT.getValue())
						.build();

		EmployeeSearchModel employeeSearchModel = (gson.fromJson
				(EmployeeSearchModalUtil.getEmployeeSearchModelJson
								(userDTO.employee_record_id,
										userDTO.unitID,
										userDTO.organogramID),
						EmployeeSearchModel.class)
		);

		boolean sendNotificationToUser = false;
		if (isAccepted) {
			boolean isWaitingForApproval = true;
			CardApprovalResponse approvalResponse = Am_house_allocation_approval_mappingDAO.getInstance().movedToNextLevelOfApproval(model,isWaitingForApproval);
			if (!approvalResponse.hasNextApproval) {

				amHouseAllocationRequestDTO.approverEmpId = employeeSearchModel.employeeRecordId;
				amHouseAllocationRequestDTO.approverOfficeUnitId = employeeSearchModel.officeUnitId;
				amHouseAllocationRequestDTO.approverOrgId = employeeSearchModel.organogramId;

				amHouseAllocationRequestDTO.approverNameEn = employeeSearchModel.employeeNameEn;
				amHouseAllocationRequestDTO.approverNameBn = employeeSearchModel.employeeNameBn;
				amHouseAllocationRequestDTO.approverOfficeUnitNameEn = employeeSearchModel.officeUnitNameEn;
				amHouseAllocationRequestDTO.approverOfficeUnitNameBn = employeeSearchModel.officeUnitNameBn;
				amHouseAllocationRequestDTO.approverOfficeUnitOrgNameEn = employeeSearchModel.organogramNameEn;
				amHouseAllocationRequestDTO.approverOfficeUnitOrgNameBn = employeeSearchModel.organogramNameBn;

				amHouseAllocationRequestDTO.approverPhoneNum = employeeSearchModel.phoneNumber;

				OfficesDTO approverOffice = OfficesRepository.getInstance().getOfficesDTOByID(amHouseAllocationRequestDTO.approverOfficeId);

				if (approverOffice != null) {
					amHouseAllocationRequestDTO.approverOfficeId = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(employeeSearchModel.officeUnitId).officeId;

					amHouseAllocationRequestDTO.approverOfficeNameEn = approverOffice.officeNameEng;
					amHouseAllocationRequestDTO.approverOfficeNameBn = approverOffice.officeNameBng;
				}

				amHouseAllocationRequestDTO.status = AmHouseAllocationRequestStatus.APPROVED.getValue();
				amHouseAllocationRequestDTO.lastModificationTime = System.currentTimeMillis();
				amHouseAllocationRequestDTO.lastModifierUser = String.valueOf(requesterEmployeeRecordId);
				Am_house_allocation_requestDAO.getInstance().update(amHouseAllocationRequestDTO);
				sendNotificationToUser = true;


				Am_house_allocation_approval_mappingDAO.getInstance()
						.withdrawByRequesterOrgId(
								amHouseAllocationRequestDTO.requesterOrgId,
								amHouseAllocationRequestDTO.iD,
								userDTO,
								"Another house allocation is approved"
						);

			} else {
				AmHouseAllocationNotification.getInstance().sendWaitingForApprovalNotification(
						approvalResponse.organogramIds, amHouseAllocationRequestDTO
				);
			}
		} else {
			String rejectReason = request.getParameter("reject_reason");
			if (rejectReason != null) {
				rejectReason = Jsoup.clean(rejectReason, Whitelist.simpleText());
				rejectReason = rejectReason.replace("'", " ");
				rejectReason = rejectReason.replace("\"", " ");
			} else {
				rejectReason = "";
			}
			Am_house_allocation_approval_mappingDAO.getInstance().rejectPendingApproval(model, rejectReason);

			amHouseAllocationRequestDTO.approverEmpId = employeeSearchModel.employeeRecordId;
			amHouseAllocationRequestDTO.approverOfficeUnitId = employeeSearchModel.officeUnitId;
			amHouseAllocationRequestDTO.approverOrgId = employeeSearchModel.organogramId;

			amHouseAllocationRequestDTO.approverNameEn = employeeSearchModel.employeeNameEn;
			amHouseAllocationRequestDTO.approverNameBn = employeeSearchModel.employeeNameBn;
			amHouseAllocationRequestDTO.approverOfficeUnitNameEn = employeeSearchModel.officeUnitNameEn;
			amHouseAllocationRequestDTO.approverOfficeUnitNameBn = employeeSearchModel.officeUnitNameBn;
			amHouseAllocationRequestDTO.approverOfficeUnitOrgNameEn = employeeSearchModel.organogramNameEn;
			amHouseAllocationRequestDTO.approverOfficeUnitOrgNameBn = employeeSearchModel.organogramNameBn;

			amHouseAllocationRequestDTO.approverPhoneNum = employeeSearchModel.phoneNumber;

			OfficesDTO approverOffice = OfficesRepository.getInstance().getOfficesDTOByID(amHouseAllocationRequestDTO.approverOfficeId);

			if (approverOffice != null) {
				amHouseAllocationRequestDTO.approverOfficeId = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(employeeSearchModel.officeUnitId).officeId;

				amHouseAllocationRequestDTO.approverOfficeNameEn = approverOffice.officeNameEng;
				amHouseAllocationRequestDTO.approverOfficeNameBn = approverOffice.officeNameBng;
			}
			amHouseAllocationRequestDTO.status = AmHouseAllocationRequestStatus.REJECTED.getValue();
			amHouseAllocationRequestDTO.rejectionReason = rejectReason;
			Am_house_allocation_requestDAO.getInstance().update(amHouseAllocationRequestDTO);
			sendNotificationToUser = true;
		}

		if (sendNotificationToUser) {
			AmHouseAllocationNotification.getInstance().sendApproveOrRejectNotification(
					Collections.singletonList(amHouseAllocationRequestDTO.requesterOrgId), amHouseAllocationRequestDTO, isAccepted
			);
		}
	}

}
	