package am_house_allocation_approval_mapping;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class Am_house_allocation_approval_mappingRepository implements Repository {
	Am_house_allocation_approval_mappingDAO am_house_allocation_approval_mappingDAO = null;
	
	static Logger logger = Logger.getLogger(Am_house_allocation_approval_mappingRepository.class);
	Map<Long, Am_house_allocation_approval_mappingDTO>mapOfAm_house_allocation_approval_mappingDTOToiD;

  
	private Am_house_allocation_approval_mappingRepository(){
		am_house_allocation_approval_mappingDAO = Am_house_allocation_approval_mappingDAO.getInstance();
		mapOfAm_house_allocation_approval_mappingDTOToiD = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	private static class LazyLoader{
        final static Am_house_allocation_approval_mappingRepository INSTANCE = new Am_house_allocation_approval_mappingRepository();
    }

    public static Am_house_allocation_approval_mappingRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

	public void reload(boolean reloadAll){
		try {
			List<Am_house_allocation_approval_mappingDTO> am_house_allocation_approval_mappingDTOs = am_house_allocation_approval_mappingDAO.getAllDTOs(reloadAll);
			for(Am_house_allocation_approval_mappingDTO am_house_allocation_approval_mappingDTO : am_house_allocation_approval_mappingDTOs) {
				Am_house_allocation_approval_mappingDTO oldAm_house_allocation_approval_mappingDTO = getAm_house_allocation_approval_mappingDTOByID(am_house_allocation_approval_mappingDTO.iD);
				if( oldAm_house_allocation_approval_mappingDTO != null ) {
					mapOfAm_house_allocation_approval_mappingDTOToiD.remove(oldAm_house_allocation_approval_mappingDTO.iD);
				
					
				}
				if(am_house_allocation_approval_mappingDTO.isDeleted == 0) 
				{
					
					mapOfAm_house_allocation_approval_mappingDTOToiD.put(am_house_allocation_approval_mappingDTO.iD, am_house_allocation_approval_mappingDTO);
				
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Am_house_allocation_approval_mappingDTO> getAm_house_allocation_approval_mappingList() {
		List <Am_house_allocation_approval_mappingDTO> am_house_allocation_approval_mappings = new ArrayList<Am_house_allocation_approval_mappingDTO>(this.mapOfAm_house_allocation_approval_mappingDTOToiD.values());
		return am_house_allocation_approval_mappings;
	}
	
	
	public Am_house_allocation_approval_mappingDTO getAm_house_allocation_approval_mappingDTOByID( long ID){
		return mapOfAm_house_allocation_approval_mappingDTOToiD.get(ID);
	}

	
	@Override
	public String getTableName() {
		return "am_house_allocation_approval_mapping";
	}
}


