package am_house_allocation_approval_mapping;

import am_house_allocation_request.Am_house_allocation_requestDTO;
import pb_notifications.Pb_notificationsDAO;

import java.util.List;

public class AmHouseAllocationNotification {
    private final Pb_notificationsDAO pb_notificationsDAO;

    private AmHouseAllocationNotification(){
        pb_notificationsDAO = Pb_notificationsDAO.getInstance();
    }

    private static class LazyLoader {
        static final AmHouseAllocationNotification INSTANCE = new AmHouseAllocationNotification();
    }

    public static AmHouseAllocationNotification getInstance(){
        return LazyLoader.INSTANCE;
    }

    public void sendWaitingForFirstApprovalNotification(List<Long> organogramIds, Am_house_allocation_requestDTO amHouseAllocationRequestDTO){
        String textEn = amHouseAllocationRequestDTO.requesterNameEn + "(" + amHouseAllocationRequestDTO.requesterOfficeUnitOrgNameEn + ","
                + amHouseAllocationRequestDTO.requesterOfficeNameEn + ")'s house allocation's application is waiting for your approval";

        String textBn = amHouseAllocationRequestDTO.requesterNameBn + "(" + amHouseAllocationRequestDTO.requesterOfficeUnitOrgNameBn + ","
                + amHouseAllocationRequestDTO.requesterOfficeNameBn + ") এর বাসা বরাদ্দের আবেদন আপনার আনুমোদনের জন্য অপেক্ষমান";

        String notificationMessage = textEn + "$" + textBn;

        String url = "Am_house_allocation_requestServlet?actionType=getApprovalPage&amHouseAllocationRequestId=" + amHouseAllocationRequestDTO.iD;
        sendNotification(organogramIds, notificationMessage, url);
    }

    public void sendWaitingForApprovalNotification(List<Long> organogramIds, Am_house_allocation_requestDTO amHouseAllocationRequestDTO){
        String textEn = amHouseAllocationRequestDTO.requesterNameEn + "(" + amHouseAllocationRequestDTO.requesterOfficeUnitOrgNameEn + ","
                + amHouseAllocationRequestDTO.requesterOfficeNameEn + ")'s house allocation's application is waiting for your approval";

        String textBn = amHouseAllocationRequestDTO.requesterNameBn + "(" + amHouseAllocationRequestDTO.requesterOfficeUnitOrgNameBn + ","
                + amHouseAllocationRequestDTO.requesterOfficeNameBn + ") এর বাসা বরাদ্দের আবেদন আপনার আনুমোদনের জন্য অপেক্ষমান";

        String notificationMessage = textEn + "$" + textBn;

        String url = "Am_house_allocation_approval_mappingServlet?actionType=getApprovalPage&amHouseAllocationRequestId=" + amHouseAllocationRequestDTO.iD;
        sendNotification(organogramIds, notificationMessage, url);
    }

    public void sendApproveOrRejectNotification(List<Long> organogramIds, Am_house_allocation_requestDTO am_house_allocation_requestDTO, boolean isApproved){
        String textEn = "Your application for house allocation has been " + (isApproved ? "accepted" : "rejected");

        String textBn = "আপনার বাসা বরাদ্দের আবেদন " + (isApproved ? "গৃহীত " : " প্রত্যাখ্যাত ") + " হয়েছে";

        String notificationMessage = textEn + "$" + textBn;

        String url = "Am_house_allocation_requestServlet?actionType=view&ID=" + am_house_allocation_requestDTO.iD;

        sendNotification(organogramIds,notificationMessage,url);
    }

    public void sendWithdrawalNotification(List<Long> organogramIds, Am_house_allocation_requestDTO am_house_allocation_requestDTO){
        String textEn = "The house assigned to you has been canceled";

        String textBn = "আপনার জন্য বরাদ্দকৃত বাসা বাতিল করা হয়েছে";

        String notificationMessage = textEn + "$" + textBn;

        String url = "Am_house_allocation_requestServlet?actionType=view&ID=" + am_house_allocation_requestDTO.iD;

        sendNotification(organogramIds,notificationMessage,url);
    }

    private void sendNotification(List<Long> organogramIds,String notificationMessage,String url){
        if(organogramIds == null || organogramIds.isEmpty()) return;

        Thread thread = new Thread(()->{
            long currentTime = System.currentTimeMillis();
            organogramIds.forEach(organogramId -> pb_notificationsDAO.addPb_notifications(organogramId, currentTime, url, notificationMessage));
        });
        thread.setDaemon(true);
        thread.start();
    }

}
