package forgetPassword;

import common.VbSequencerService;
import dbm.DBMR;
import dbm.DBMW;
import org.apache.log4j.Logger;
import pb.Utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Alam
 */
public class VerificationDAO implements VbSequencerService {
	
	static Logger logger = Logger.getLogger(VerificationDAO.class.getName());

	/**
	 * This method add a new row to token table
	 * @author Alam
	 * @param
	 * @throws Exception 
	 */
	public void add(VerificationDTO verificationDTO) throws Exception
	{
	
	
        //MineDTO mineDTO = (MineDTO)commonDTO;
		
		Connection connection = null;
		PreparedStatement ps = null;

		long lastModificationTime = System.currentTimeMillis();	

		try{
			connection = DBMW.getInstance().getConnection();
			
			if(connection == null)
			{
				System.out.println("nullconn");
			}
			
			String tableName = "verification";

			verificationDTO.id = DBMW.getInstance().getNextSequenceId(tableName);

			String sql = "INSERT INTO " + tableName;
			
			sql += " (";
			sql += "id";
			sql += ", ";
			sql += "token";
			sql += ", ";
			sql += "authPurpose";
			sql += ", ";
			sql += "username";
			sql += ", ";
			sql += "timestamp";
			sql += ", ";
			sql += "expirationTime";
			
			
			sql += ")";
			
			
            sql += " VALUES(";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			
		
			sql += ")";
				
			//printSql(sql);

			ps = connection.prepareStatement(sql);
			
			
			

			int index = 1;

			ps.setObject(index++,verificationDTO.id);
			ps.setObject(index++,verificationDTO.token);
			ps.setObject(index++,verificationDTO.authPurpose);
			ps.setObject(index++,verificationDTO.username);
			ps.setObject(index++,verificationDTO.timestamp);
			ps.setObject(index++,verificationDTO.expirationTime);
			
			
			System.out.println(ps);
			ps.execute();
			
			
			recordUpdateTime(connection, tableName, lastModificationTime);

		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if (ps != null) {
					ps.close();
				}
			} catch(Exception e){}
			try{
				if(connection != null){
					DBMW.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		//return verificationDTO.id;		
	
	}

	/**
	 * This method returns a which mathces given username from token table
	 * @author Alam
	 * @param username
	 * @return ForgetPassword an ForgetPassword object. Null if not found
	 * @throws Exception 
	 */
	public static VerificationDTO getByUsername(String username) throws Exception {
		if(!Utils.isValidUserName(username))
		{
			return null;
		}
		
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		VerificationDTO verificationDTO = null;
		try{
			
			String sql = "SELECT * ";

			sql += " FROM verification" ;
			
            sql += " WHERE username LIKE '" + username.trim() + "'";
			
			//printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			if(rs.next()){
				verificationDTO = new VerificationDTO();

				verificationDTO.id = rs.getLong("id");
				verificationDTO.token = rs.getString("token");
				verificationDTO.authPurpose = rs.getInt("authPurpose");
				verificationDTO.username = rs.getString("username");
				verificationDTO.timestamp = rs.getLong("timestamp");
				verificationDTO.expirationTime = rs.getLong("expirationTime");
			

			}			
			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DBMR.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
		return verificationDTO;
	}

	public VerificationDTO getVerificationDTOByTokenID(long tokenID) throws Exception
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		VerificationDTO verificationDTO = null;
		try{
			
			String sql = "SELECT * ";

			sql += " FROM verification" ;
			
            sql += " WHERE id = " + tokenID ;
			
			//printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			if(rs.next()){
				verificationDTO = new VerificationDTO();

				verificationDTO.id = rs.getLong("id");
				verificationDTO.token = rs.getString("token");
				verificationDTO.authPurpose = rs.getInt("authPurpose");
				verificationDTO.username = rs.getString("username");
				verificationDTO.timestamp = rs.getLong("timestamp");
				verificationDTO.expirationTime = rs.getLong("expirationTime");
			

			}			
			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DBMR.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
		return verificationDTO;
	}

	public VerificationDTO getVerificationDTOByToken(String token) throws Exception
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		VerificationDTO verificationDTO = null;
		try{
			
			String sql = "SELECT * ";

			sql += " FROM verification" ;
			
            sql += " WHERE token LIKE '" + token.trim() + "'";
			
			//printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			if(rs.next()){
				verificationDTO = new VerificationDTO();

				verificationDTO.id = rs.getLong("id");
				verificationDTO.token = rs.getString("token");
				verificationDTO.authPurpose = rs.getInt("authPurpose");
				verificationDTO.username = rs.getString("username");
				verificationDTO.timestamp = rs.getLong("timestamp");
				verificationDTO.expirationTime = rs.getLong("expirationTime");
			

			}			
			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DBMR.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
		return verificationDTO;		
	}
	
	public List<VerificationDTO> getVerificationDTOsBeforeCurrentTime(String username,int authType) throws Exception
	{
		if(!Utils.isValidUserName(username))
		{
			return null;
		}
		//String[] columnNames = new String[] {getColumnName(VerificationDTO.class, "username"), getColumnName(VerificationDTO.class, "authPurpose")};
		List<VerificationDTO> verificationDTOs = new ArrayList();//ModifiedSqlGenerator.getObjectFullyPopulatedByString(VerificationDTO.class, new String[] {username, ""+authType}, columnNames, " and " + getColumnName(VerificationDTO.class, "expirationTime") + " < " + System.currentTimeMillis());
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		//VerificationDTO verificationDTO = null;
		try{
			
			long lastModificationTime = System.currentTimeMillis();	
			
			String sql = "SELECT * ";

			sql += " FROM verification" ;
			
            sql += " WHERE username LIKE '" + username + "' and authPurpose = " + authType + " and expirationTime >  " + lastModificationTime ;
			
			//printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				VerificationDTO verificationDTO = new VerificationDTO();

				verificationDTO.id = rs.getLong("id");
				verificationDTO.token = rs.getString("token");
				verificationDTO.authPurpose = rs.getInt("authPurpose");
				verificationDTO.username = rs.getString("username");
				verificationDTO.timestamp = rs.getLong("timestamp");
				verificationDTO.expirationTime = rs.getLong("expirationTime");
				
				verificationDTOs.add(verificationDTO);
			

			}			
			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DBMR.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
		return verificationDTOs;	
	}
	
	public List<VerificationDTO> getVerificationDTOsAfterCurrentTime(String username,int authType) throws Exception
	{
		if(!Utils.isValidUserName(username))
		{
			return null;
		}
		List<VerificationDTO> verificationDTOs = new ArrayList();//ModifiedSqlGenerator.getObjectFullyPopulatedByString(VerificationDTO.class, new String[] {username, ""+authType}, columnNames, " and " + getColumnName(VerificationDTO.class, "expirationTime") + " < " + System.currentTimeMillis());
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		//VerificationDTO verificationDTO = null;
		try{
			
			long lastModificationTime = System.currentTimeMillis();	
			
			String sql = "SELECT * ";

			sql += " FROM verification" ;
			
            sql += " WHERE username LIKE '" + username + "' and authPurpose = " + authType + " and expirationTime <  " + lastModificationTime ;
			
			//printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				VerificationDTO verificationDTO = new VerificationDTO();

				verificationDTO.id = rs.getLong("id");
				verificationDTO.token = rs.getString("token");
				verificationDTO.authPurpose = rs.getInt("authPurpose");
				verificationDTO.username = rs.getString("username");
				verificationDTO.timestamp = rs.getLong("timestamp");
				verificationDTO.expirationTime = rs.getLong("expirationTime");
				
				verificationDTOs.add(verificationDTO);
			

			}			
			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DBMR.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
		return verificationDTOs;		
	}
	
	public void updateVerificationDTO(VerificationDTO verificationDTO) throws Exception
	{
	//MineDTO mineDTO = (MineDTO)commonDTO;
		
		Connection connection = null;
		PreparedStatement ps = null;

		long lastModificationTime = System.currentTimeMillis();	
		try{
			String tableName = "verification";
			connection = DBMW.getInstance().getConnection();

			String sql = "UPDATE " + tableName;
			
			sql += " SET ";
			sql += "token=?";
			sql += ", ";
			sql += "authPurpose=?";
			sql += ", ";
			sql += "username=?";
			sql += ", ";
			sql += "timestamp=?";
			sql += ", ";
			sql += "expirationTime=?";
		
            sql += " WHERE id = " + verificationDTO.id;
				
			//printSql(sql);

			ps = connection.prepareStatement(sql);
			
			

			int index = 1;
			
			ps.setObject(index++,verificationDTO.token);
			ps.setObject(index++,verificationDTO.authPurpose);
			ps.setObject(index++,verificationDTO.username);
			ps.setObject(index++,verificationDTO.timestamp);
			ps.setObject(index++,verificationDTO.expirationTime);
			
			System.out.println(ps);
			ps.executeUpdate();
			


			
			recordUpdateTime(connection, tableName, lastModificationTime);

						
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if (ps != null) {
					ps.close();
				}
			} catch(Exception e){}
			try{
				if(connection != null){
					DBMW.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		
	}
	
	public void deleteVerificationDTO(long id) throws Exception
	{
		Connection connection = null;
		PreparedStatement ps = null;

		long lastModificationTime = System.currentTimeMillis();	
		try{
			String tableName = "verification";
			connection = DBMW.getInstance().getConnection();

			String sql = "delete from " + tableName;
			
		
		
            sql += " WHERE id = " + id;
				
			//printSql(sql);

			ps = connection.prepareStatement(sql);
			
			

		
			
			System.out.println(ps);
			ps.executeUpdate();
			


			
			recordUpdateTime(connection, tableName,  lastModificationTime);

						
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if (ps != null) {
					ps.close();
				}
			} catch(Exception e){}
			try{
				if(connection != null){
					DBMW.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
	}
	
	public void deleteVerificationDTO(String username, int authPurpose) throws Exception
	{
		if(!Utils.isValidUserName(username))
		{
			return;
		}
		Connection connection = null;
		PreparedStatement ps = null;

		long lastModificationTime = System.currentTimeMillis();	
		try{
			String tableName = "verification";
			connection = DBMW.getInstance().getConnection();

			String sql = "delete from " + tableName;
			
		
		
            sql += " WHERE username LIKE '" + username + "' and authPurpose = " + authPurpose;
				
			//printSql(sql);

			ps = connection.prepareStatement(sql);
			
			

		
			
			System.out.println(ps);
			ps.executeUpdate();
			


			
			recordUpdateTime(connection, tableName, lastModificationTime);

						
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if (ps != null) {
					ps.close();
				}
			} catch(Exception e){}
			try{
				if(connection != null){
					DBMW.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
	}


}
