package am_house_type_sub_category;
import java.util.*; 
import util.*; 


public class Am_house_type_sub_categoryDTO extends CommonDTO
{

	public int amHouseCatVal = -1;
    public String amHouseCatDomainName = "";
    public String amHouseCatNameEn = "";
    public String amHouseCatNameBn = "";
	public int amHouseCatOrdering = -1;
	public int amHouseSubCatVal = -1;
    public String amHouseSubCatNameEn = "";
    public String amHouseSubCatNameBn = "";
	public int amHouseSubCatOrdering = -1;
	public long modifiedBy = -1;
	public long insertedBy = -1;
	public long insertionTime = -1;
	
	
    @Override
	public String toString() {
            return "$Am_house_type_sub_categoryDTO[" +
            " iD = " + iD +
            " amHouseCatVal = " + amHouseCatVal +
            " amHouseCatDomainName = " + amHouseCatDomainName +
            " amHouseCatNameEn = " + amHouseCatNameEn +
            " amHouseCatNameBn = " + amHouseCatNameBn +
            " amHouseCatOrdering = " + amHouseCatOrdering +
            " amHouseSubCatVal = " + amHouseSubCatVal +
            " amHouseSubCatNameEn = " + amHouseSubCatNameEn +
            " amHouseSubCatNameBn = " + amHouseSubCatNameBn +
            " amHouseSubCatOrdering = " + amHouseSubCatOrdering +
            " searchColumn = " + searchColumn +
            " modifiedBy = " + modifiedBy +
            " lastModificationTime = " + lastModificationTime +
            " insertedBy = " + insertedBy +
            " insertionTime = " + insertionTime +
            " isDeleted = " + isDeleted +
            "]";
    }

}