package am_house_type_sub_category;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.sql.SQLException;
import common.CommonDAOService;
import org.apache.log4j.Logger;
import util.*;
import pb.*;

public class Am_house_type_sub_categoryDAO  implements CommonDAOService<Am_house_type_sub_categoryDTO>
{

	Logger logger = Logger.getLogger(getClass());

	private static String addQuery = "";
	private static String updateQuery = "";
	private String[] columnNames = null;

	private Am_house_type_sub_categoryDAO()
	{
		columnNames = new String[]
		{
			"ID",
			"am_house_cat_val",
			"am_house_cat_domain_name",
			"am_house_cat_name_en",
			"am_house_cat_name_bn",
			"am_house_cat_ordering",
			"am_house_sub_cat_val",
			"am_house_sub_cat_name_en",
			"am_house_sub_cat_name_bn",
			"am_house_sub_cat_ordering",
			"search_column",
			"modified_by",
			"inserted_by",
			"insertion_time",
			"isDeleted",
			"lastModificationTime"
		};
		updateQuery = getUpdateQuery(columnNames);
        addQuery = getAddQuery(columnNames);

		searchMap.put("am_house_cat_domain_name"," and (am_house_cat_domain_name like ?)");
		searchMap.put("am_house_cat_name_en"," and (am_house_cat_name_en like ?)");
		searchMap.put("am_house_cat_name_bn"," and (am_house_cat_name_bn like ?)");
		searchMap.put("am_house_sub_cat_name_en"," and (am_house_sub_cat_name_en like ?)");
		searchMap.put("am_house_sub_cat_name_bn"," and (am_house_sub_cat_name_bn like ?)");
		searchMap.put("modified_by"," and (modified_by like ?)");
		searchMap.put("inserted_by"," and (inserted_by like ?)");
		searchMap.put("AnyField"," and (search_column like ?)");
	}

	private final Map<String,String> searchMap = new HashMap<>();

    private static class LazyLoader
    {
    	static final Am_house_type_sub_categoryDAO INSTANCE = new Am_house_type_sub_categoryDAO();
	}

	public static Am_house_type_sub_categoryDAO getInstance()
	{
    	return LazyLoader.INSTANCE;
	}

	public void setSearchColumn(Am_house_type_sub_categoryDTO am_house_type_sub_categoryDTO)
	{
		am_house_type_sub_categoryDTO.searchColumn = "";
		am_house_type_sub_categoryDTO.searchColumn += am_house_type_sub_categoryDTO.amHouseCatDomainName + " ";
		am_house_type_sub_categoryDTO.searchColumn += am_house_type_sub_categoryDTO.amHouseCatNameEn + " ";
		am_house_type_sub_categoryDTO.searchColumn += am_house_type_sub_categoryDTO.amHouseCatNameBn + " ";
		am_house_type_sub_categoryDTO.searchColumn += am_house_type_sub_categoryDTO.amHouseSubCatNameEn + " ";
		am_house_type_sub_categoryDTO.searchColumn += am_house_type_sub_categoryDTO.amHouseSubCatNameBn + " ";
		am_house_type_sub_categoryDTO.searchColumn += am_house_type_sub_categoryDTO.modifiedBy + " ";
		am_house_type_sub_categoryDTO.searchColumn += am_house_type_sub_categoryDTO.insertedBy + " ";
	}

	@Override
	public void set(PreparedStatement ps, Am_house_type_sub_categoryDTO am_house_type_sub_categoryDTO, boolean isInsert) throws SQLException
	{
		int index = 0;
		long lastModificationTime = System.currentTimeMillis();
		setSearchColumn(am_house_type_sub_categoryDTO);
		if(isInsert)
		{
			ps.setObject(++index,am_house_type_sub_categoryDTO.iD);
		}
		ps.setObject(++index,am_house_type_sub_categoryDTO.amHouseCatVal);
		ps.setObject(++index,am_house_type_sub_categoryDTO.amHouseCatDomainName);
		ps.setObject(++index,am_house_type_sub_categoryDTO.amHouseCatNameEn);
		ps.setObject(++index,am_house_type_sub_categoryDTO.amHouseCatNameBn);
		ps.setObject(++index,am_house_type_sub_categoryDTO.amHouseCatOrdering);
		ps.setObject(++index,am_house_type_sub_categoryDTO.amHouseSubCatVal);
		ps.setObject(++index,am_house_type_sub_categoryDTO.amHouseSubCatNameEn);
		ps.setObject(++index,am_house_type_sub_categoryDTO.amHouseSubCatNameBn);
		ps.setObject(++index,am_house_type_sub_categoryDTO.amHouseSubCatOrdering);
		ps.setObject(++index,am_house_type_sub_categoryDTO.searchColumn);
		ps.setObject(++index,am_house_type_sub_categoryDTO.modifiedBy);
		ps.setObject(++index,am_house_type_sub_categoryDTO.insertedBy);
		ps.setObject(++index,am_house_type_sub_categoryDTO.insertionTime);
		if(isInsert)
		{
			ps.setObject(++index,am_house_type_sub_categoryDTO.isDeleted);
		}
		ps.setObject(++index, lastModificationTime);
		if(!isInsert)
		{
			ps.setObject(++index,am_house_type_sub_categoryDTO.iD);
		}
	}

	@Override
	public Am_house_type_sub_categoryDTO buildObjectFromResultSet(ResultSet rs)
	{
		try
		{
			Am_house_type_sub_categoryDTO am_house_type_sub_categoryDTO = new Am_house_type_sub_categoryDTO();
			int i = 0;
			am_house_type_sub_categoryDTO.iD = rs.getLong(columnNames[i++]);
			am_house_type_sub_categoryDTO.amHouseCatVal = rs.getInt(columnNames[i++]);
			am_house_type_sub_categoryDTO.amHouseCatDomainName = rs.getString(columnNames[i++]);
			am_house_type_sub_categoryDTO.amHouseCatNameEn = rs.getString(columnNames[i++]);
			am_house_type_sub_categoryDTO.amHouseCatNameBn = rs.getString(columnNames[i++]);
			am_house_type_sub_categoryDTO.amHouseCatOrdering = rs.getInt(columnNames[i++]);
			am_house_type_sub_categoryDTO.amHouseSubCatVal = rs.getInt(columnNames[i++]);
			am_house_type_sub_categoryDTO.amHouseSubCatNameEn = rs.getString(columnNames[i++]);
			am_house_type_sub_categoryDTO.amHouseSubCatNameBn = rs.getString(columnNames[i++]);
			am_house_type_sub_categoryDTO.amHouseSubCatOrdering = rs.getInt(columnNames[i++]);
			am_house_type_sub_categoryDTO.searchColumn = rs.getString(columnNames[i++]);
			am_house_type_sub_categoryDTO.modifiedBy = rs.getLong(columnNames[i++]);
			am_house_type_sub_categoryDTO.insertedBy = rs.getLong(columnNames[i++]);
			am_house_type_sub_categoryDTO.insertionTime = rs.getLong(columnNames[i++]);
			am_house_type_sub_categoryDTO.isDeleted = rs.getInt(columnNames[i++]);
			am_house_type_sub_categoryDTO.lastModificationTime = rs.getLong(columnNames[i++]);
			return am_house_type_sub_categoryDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}

	public Am_house_type_sub_categoryDTO getDTOByID (long id)
	{
		return getDTOFromID(id);
	}

	@Override
	public String getTableName() {
		return "am_house_type_sub_category";
	}

	@Override
	public Map<String, String> getSearchMap() {
		return searchMap;
	}

	public long add(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Am_house_type_sub_categoryDTO) commonDTO,addQuery,true);
	}

	public long update(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Am_house_type_sub_categoryDTO) commonDTO,updateQuery,false);
	}

	@Override
	public String getLastModifierUser(){
		return "modified_by";
    }

}
