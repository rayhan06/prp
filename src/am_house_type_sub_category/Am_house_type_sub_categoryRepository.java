package am_house_type_sub_category;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import am_house.Am_houseDTO;
import org.apache.log4j.Logger;
import com.google.gson.Gson;

import pb.CatDTO;
import repository.Repository;
import repository.RepositoryManager;


public class Am_house_type_sub_categoryRepository implements Repository {
    Am_house_type_sub_categoryDAO am_house_type_sub_categoryDAO = null;

    static Logger logger = Logger.getLogger(Am_house_type_sub_categoryRepository.class);
    Map<Long, Am_house_type_sub_categoryDTO> mapOfAm_house_type_sub_categoryDTOToiD;
    Map<Integer, Set<Am_house_type_sub_categoryDTO>> mapOfAm_house_type_sub_categoryDTOToamHouseCatVal;
    Map<String, Set<Am_house_type_sub_categoryDTO>> mapOfAm_house_type_sub_categoryDTOToamHouseCatDomainName;
    Map<String, Set<Am_house_type_sub_categoryDTO>> mapOfAm_house_type_sub_categoryDTOToamHouseCatNameEn;
    Map<String, Set<Am_house_type_sub_categoryDTO>> mapOfAm_house_type_sub_categoryDTOToamHouseCatNameBn;
    Map<Integer, Set<Am_house_type_sub_categoryDTO>> mapOfAm_house_type_sub_categoryDTOToamHouseCatOrdering;
    Map<Integer, Set<Am_house_type_sub_categoryDTO>> mapOfAm_house_type_sub_categoryDTOToamHouseSubCatVal;
    Map<String, Set<Am_house_type_sub_categoryDTO>> mapOfAm_house_type_sub_categoryDTOToamHouseSubCatNameEn;
    Map<String, Set<Am_house_type_sub_categoryDTO>> mapOfAm_house_type_sub_categoryDTOToamHouseSubCatNameBn;
    Map<Integer, Set<Am_house_type_sub_categoryDTO>> mapOfAm_house_type_sub_categoryDTOToamHouseSubCatOrdering;
    Gson gson;


    private Am_house_type_sub_categoryRepository() {
        am_house_type_sub_categoryDAO = Am_house_type_sub_categoryDAO.getInstance();
        mapOfAm_house_type_sub_categoryDTOToiD = new ConcurrentHashMap<>();
        mapOfAm_house_type_sub_categoryDTOToamHouseCatVal = new ConcurrentHashMap<>();
        mapOfAm_house_type_sub_categoryDTOToamHouseCatDomainName = new ConcurrentHashMap<>();
        mapOfAm_house_type_sub_categoryDTOToamHouseCatNameEn = new ConcurrentHashMap<>();
        mapOfAm_house_type_sub_categoryDTOToamHouseCatNameBn = new ConcurrentHashMap<>();
        mapOfAm_house_type_sub_categoryDTOToamHouseCatOrdering = new ConcurrentHashMap<>();
        mapOfAm_house_type_sub_categoryDTOToamHouseSubCatVal = new ConcurrentHashMap<>();
        mapOfAm_house_type_sub_categoryDTOToamHouseSubCatNameEn = new ConcurrentHashMap<>();
        mapOfAm_house_type_sub_categoryDTOToamHouseSubCatNameBn = new ConcurrentHashMap<>();
        mapOfAm_house_type_sub_categoryDTOToamHouseSubCatOrdering = new ConcurrentHashMap<>();
        gson = new Gson();
        RepositoryManager.getInstance().addRepository(this);
    }

    private static class LazyLoader {
        final static Am_house_type_sub_categoryRepository INSTANCE = new Am_house_type_sub_categoryRepository();
    }

    public static Am_house_type_sub_categoryRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

    public void reload(boolean reloadAll) {
        try {
            List<Am_house_type_sub_categoryDTO> am_house_type_sub_categoryDTOs = am_house_type_sub_categoryDAO.getAllDTOs(reloadAll);
            for (Am_house_type_sub_categoryDTO am_house_type_sub_categoryDTO : am_house_type_sub_categoryDTOs) {
                Am_house_type_sub_categoryDTO oldAm_house_type_sub_categoryDTO = getAm_house_type_sub_categoryDTOByiD(am_house_type_sub_categoryDTO.iD);
                if (oldAm_house_type_sub_categoryDTO != null) {
                    mapOfAm_house_type_sub_categoryDTOToiD.remove(oldAm_house_type_sub_categoryDTO.iD);

                    if (mapOfAm_house_type_sub_categoryDTOToamHouseCatVal.containsKey(oldAm_house_type_sub_categoryDTO.amHouseCatVal)) {
                        mapOfAm_house_type_sub_categoryDTOToamHouseCatVal.get(oldAm_house_type_sub_categoryDTO.amHouseCatVal).remove(oldAm_house_type_sub_categoryDTO);
                    }
                    if (mapOfAm_house_type_sub_categoryDTOToamHouseCatVal.get(oldAm_house_type_sub_categoryDTO.amHouseCatVal).isEmpty()) {
                        mapOfAm_house_type_sub_categoryDTOToamHouseCatVal.remove(oldAm_house_type_sub_categoryDTO.amHouseCatVal);
                    }

                    if (mapOfAm_house_type_sub_categoryDTOToamHouseCatDomainName.containsKey(oldAm_house_type_sub_categoryDTO.amHouseCatDomainName)) {
                        mapOfAm_house_type_sub_categoryDTOToamHouseCatDomainName.get(oldAm_house_type_sub_categoryDTO.amHouseCatDomainName).remove(oldAm_house_type_sub_categoryDTO);
                    }
                    if (mapOfAm_house_type_sub_categoryDTOToamHouseCatDomainName.get(oldAm_house_type_sub_categoryDTO.amHouseCatDomainName).isEmpty()) {
                        mapOfAm_house_type_sub_categoryDTOToamHouseCatDomainName.remove(oldAm_house_type_sub_categoryDTO.amHouseCatDomainName);
                    }

                    if (mapOfAm_house_type_sub_categoryDTOToamHouseCatNameEn.containsKey(oldAm_house_type_sub_categoryDTO.amHouseCatNameEn)) {
                        mapOfAm_house_type_sub_categoryDTOToamHouseCatNameEn.get(oldAm_house_type_sub_categoryDTO.amHouseCatNameEn).remove(oldAm_house_type_sub_categoryDTO);
                    }
                    if (mapOfAm_house_type_sub_categoryDTOToamHouseCatNameEn.get(oldAm_house_type_sub_categoryDTO.amHouseCatNameEn).isEmpty()) {
                        mapOfAm_house_type_sub_categoryDTOToamHouseCatNameEn.remove(oldAm_house_type_sub_categoryDTO.amHouseCatNameEn);
                    }

                    if (mapOfAm_house_type_sub_categoryDTOToamHouseCatNameBn.containsKey(oldAm_house_type_sub_categoryDTO.amHouseCatNameBn)) {
                        mapOfAm_house_type_sub_categoryDTOToamHouseCatNameBn.get(oldAm_house_type_sub_categoryDTO.amHouseCatNameBn).remove(oldAm_house_type_sub_categoryDTO);
                    }
                    if (mapOfAm_house_type_sub_categoryDTOToamHouseCatNameBn.get(oldAm_house_type_sub_categoryDTO.amHouseCatNameBn).isEmpty()) {
                        mapOfAm_house_type_sub_categoryDTOToamHouseCatNameBn.remove(oldAm_house_type_sub_categoryDTO.amHouseCatNameBn);
                    }

                    if (mapOfAm_house_type_sub_categoryDTOToamHouseCatOrdering.containsKey(oldAm_house_type_sub_categoryDTO.amHouseCatOrdering)) {
                        mapOfAm_house_type_sub_categoryDTOToamHouseCatOrdering.get(oldAm_house_type_sub_categoryDTO.amHouseCatOrdering).remove(oldAm_house_type_sub_categoryDTO);
                    }
                    if (mapOfAm_house_type_sub_categoryDTOToamHouseCatOrdering.get(oldAm_house_type_sub_categoryDTO.amHouseCatOrdering).isEmpty()) {
                        mapOfAm_house_type_sub_categoryDTOToamHouseCatOrdering.remove(oldAm_house_type_sub_categoryDTO.amHouseCatOrdering);
                    }

                    if (mapOfAm_house_type_sub_categoryDTOToamHouseSubCatVal.containsKey(oldAm_house_type_sub_categoryDTO.amHouseSubCatVal)) {
                        mapOfAm_house_type_sub_categoryDTOToamHouseSubCatVal.get(oldAm_house_type_sub_categoryDTO.amHouseSubCatVal).remove(oldAm_house_type_sub_categoryDTO);
                    }
                    if (mapOfAm_house_type_sub_categoryDTOToamHouseSubCatVal.get(oldAm_house_type_sub_categoryDTO.amHouseSubCatVal).isEmpty()) {
                        mapOfAm_house_type_sub_categoryDTOToamHouseSubCatVal.remove(oldAm_house_type_sub_categoryDTO.amHouseSubCatVal);
                    }

                    if (mapOfAm_house_type_sub_categoryDTOToamHouseSubCatNameEn.containsKey(oldAm_house_type_sub_categoryDTO.amHouseSubCatNameEn)) {
                        mapOfAm_house_type_sub_categoryDTOToamHouseSubCatNameEn.get(oldAm_house_type_sub_categoryDTO.amHouseSubCatNameEn).remove(oldAm_house_type_sub_categoryDTO);
                    }
                    if (mapOfAm_house_type_sub_categoryDTOToamHouseSubCatNameEn.get(oldAm_house_type_sub_categoryDTO.amHouseSubCatNameEn).isEmpty()) {
                        mapOfAm_house_type_sub_categoryDTOToamHouseSubCatNameEn.remove(oldAm_house_type_sub_categoryDTO.amHouseSubCatNameEn);
                    }

                    if (mapOfAm_house_type_sub_categoryDTOToamHouseSubCatNameBn.containsKey(oldAm_house_type_sub_categoryDTO.amHouseSubCatNameBn)) {
                        mapOfAm_house_type_sub_categoryDTOToamHouseSubCatNameBn.get(oldAm_house_type_sub_categoryDTO.amHouseSubCatNameBn).remove(oldAm_house_type_sub_categoryDTO);
                    }
                    if (mapOfAm_house_type_sub_categoryDTOToamHouseSubCatNameBn.get(oldAm_house_type_sub_categoryDTO.amHouseSubCatNameBn).isEmpty()) {
                        mapOfAm_house_type_sub_categoryDTOToamHouseSubCatNameBn.remove(oldAm_house_type_sub_categoryDTO.amHouseSubCatNameBn);
                    }

                    if (mapOfAm_house_type_sub_categoryDTOToamHouseSubCatOrdering.containsKey(oldAm_house_type_sub_categoryDTO.amHouseSubCatOrdering)) {
                        mapOfAm_house_type_sub_categoryDTOToamHouseSubCatOrdering.get(oldAm_house_type_sub_categoryDTO.amHouseSubCatOrdering).remove(oldAm_house_type_sub_categoryDTO);
                    }
                    if (mapOfAm_house_type_sub_categoryDTOToamHouseSubCatOrdering.get(oldAm_house_type_sub_categoryDTO.amHouseSubCatOrdering).isEmpty()) {
                        mapOfAm_house_type_sub_categoryDTOToamHouseSubCatOrdering.remove(oldAm_house_type_sub_categoryDTO.amHouseSubCatOrdering);
                    }


                }
                if (am_house_type_sub_categoryDTO.isDeleted == 0) {

                    mapOfAm_house_type_sub_categoryDTOToiD.put(am_house_type_sub_categoryDTO.iD, am_house_type_sub_categoryDTO);

                    if (!mapOfAm_house_type_sub_categoryDTOToamHouseCatVal.containsKey(am_house_type_sub_categoryDTO.amHouseCatVal)) {
                        mapOfAm_house_type_sub_categoryDTOToamHouseCatVal.put(am_house_type_sub_categoryDTO.amHouseCatVal, new HashSet<>());
                    }
                    mapOfAm_house_type_sub_categoryDTOToamHouseCatVal.get(am_house_type_sub_categoryDTO.amHouseCatVal).add(am_house_type_sub_categoryDTO);

                    if (!mapOfAm_house_type_sub_categoryDTOToamHouseCatDomainName.containsKey(am_house_type_sub_categoryDTO.amHouseCatDomainName)) {
                        mapOfAm_house_type_sub_categoryDTOToamHouseCatDomainName.put(am_house_type_sub_categoryDTO.amHouseCatDomainName, new HashSet<>());
                    }
                    mapOfAm_house_type_sub_categoryDTOToamHouseCatDomainName.get(am_house_type_sub_categoryDTO.amHouseCatDomainName).add(am_house_type_sub_categoryDTO);

                    if (!mapOfAm_house_type_sub_categoryDTOToamHouseCatNameEn.containsKey(am_house_type_sub_categoryDTO.amHouseCatNameEn)) {
                        mapOfAm_house_type_sub_categoryDTOToamHouseCatNameEn.put(am_house_type_sub_categoryDTO.amHouseCatNameEn, new HashSet<>());
                    }
                    mapOfAm_house_type_sub_categoryDTOToamHouseCatNameEn.get(am_house_type_sub_categoryDTO.amHouseCatNameEn).add(am_house_type_sub_categoryDTO);

                    if (!mapOfAm_house_type_sub_categoryDTOToamHouseCatNameBn.containsKey(am_house_type_sub_categoryDTO.amHouseCatNameBn)) {
                        mapOfAm_house_type_sub_categoryDTOToamHouseCatNameBn.put(am_house_type_sub_categoryDTO.amHouseCatNameBn, new HashSet<>());
                    }
                    mapOfAm_house_type_sub_categoryDTOToamHouseCatNameBn.get(am_house_type_sub_categoryDTO.amHouseCatNameBn).add(am_house_type_sub_categoryDTO);

                    if (!mapOfAm_house_type_sub_categoryDTOToamHouseCatOrdering.containsKey(am_house_type_sub_categoryDTO.amHouseCatOrdering)) {
                        mapOfAm_house_type_sub_categoryDTOToamHouseCatOrdering.put(am_house_type_sub_categoryDTO.amHouseCatOrdering, new HashSet<>());
                    }
                    mapOfAm_house_type_sub_categoryDTOToamHouseCatOrdering.get(am_house_type_sub_categoryDTO.amHouseCatOrdering).add(am_house_type_sub_categoryDTO);

                    if (!mapOfAm_house_type_sub_categoryDTOToamHouseSubCatVal.containsKey(am_house_type_sub_categoryDTO.amHouseSubCatVal)) {
                        mapOfAm_house_type_sub_categoryDTOToamHouseSubCatVal.put(am_house_type_sub_categoryDTO.amHouseSubCatVal, new HashSet<>());
                    }
                    mapOfAm_house_type_sub_categoryDTOToamHouseSubCatVal.get(am_house_type_sub_categoryDTO.amHouseSubCatVal).add(am_house_type_sub_categoryDTO);

                    if (!mapOfAm_house_type_sub_categoryDTOToamHouseSubCatNameEn.containsKey(am_house_type_sub_categoryDTO.amHouseSubCatNameEn)) {
                        mapOfAm_house_type_sub_categoryDTOToamHouseSubCatNameEn.put(am_house_type_sub_categoryDTO.amHouseSubCatNameEn, new HashSet<>());
                    }
                    mapOfAm_house_type_sub_categoryDTOToamHouseSubCatNameEn.get(am_house_type_sub_categoryDTO.amHouseSubCatNameEn).add(am_house_type_sub_categoryDTO);

                    if (!mapOfAm_house_type_sub_categoryDTOToamHouseSubCatNameBn.containsKey(am_house_type_sub_categoryDTO.amHouseSubCatNameBn)) {
                        mapOfAm_house_type_sub_categoryDTOToamHouseSubCatNameBn.put(am_house_type_sub_categoryDTO.amHouseSubCatNameBn, new HashSet<>());
                    }
                    mapOfAm_house_type_sub_categoryDTOToamHouseSubCatNameBn.get(am_house_type_sub_categoryDTO.amHouseSubCatNameBn).add(am_house_type_sub_categoryDTO);

                    if (!mapOfAm_house_type_sub_categoryDTOToamHouseSubCatOrdering.containsKey(am_house_type_sub_categoryDTO.amHouseSubCatOrdering)) {
                        mapOfAm_house_type_sub_categoryDTOToamHouseSubCatOrdering.put(am_house_type_sub_categoryDTO.amHouseSubCatOrdering, new HashSet<>());
                    }
                    mapOfAm_house_type_sub_categoryDTOToamHouseSubCatOrdering.get(am_house_type_sub_categoryDTO.amHouseSubCatOrdering).add(am_house_type_sub_categoryDTO);

                }
            }

        } catch (Exception e) {
            logger.debug("FATAL", e);
        }
    }

    public Am_house_type_sub_categoryDTO clone(Am_house_type_sub_categoryDTO dto) {
        String raw = gson.toJson(dto);
        return gson.fromJson(raw, Am_house_type_sub_categoryDTO.class);
    }


    public List<Am_house_type_sub_categoryDTO> getAm_house_type_sub_categoryList() {
        List<Am_house_type_sub_categoryDTO> am_house_type_sub_categorys = new ArrayList<Am_house_type_sub_categoryDTO>(this.mapOfAm_house_type_sub_categoryDTOToiD.values());
        return am_house_type_sub_categorys;
    }

    public String buildOptionsByHouseClass(String language, int houseClass, int defaultValue) {
        List<Am_house_type_sub_categoryDTO> houseSubClasses = getAm_house_type_sub_categoryDTOByamHouseCatVal(houseClass);
        StringBuilder sOptions = new StringBuilder();
        boolean isLangEnglish = language.equalsIgnoreCase("English");
        if (defaultValue == -1) {
            sOptions.append(isLangEnglish ? "<option value = ''>Select</option>" : "<option value = ''>বাছাই করুন</option>");
        }

        for (Am_house_type_sub_categoryDTO dto : houseSubClasses) {
            StringBuilder sOption = new StringBuilder();
            sOption.append("<option value = '").append(dto.iD).append("'");
            if (defaultValue != CatDTO.CATDEFAULTNOTREQUIRED && dto.iD == defaultValue) {
                sOption.append(" selected ");
            }
            sOption.append(">").append(isLangEnglish ? dto.amHouseSubCatNameEn : dto.amHouseSubCatNameBn).append("</option>");
            sOptions.append(sOption);
        }

        return sOptions.toString();
    }

    public List<Am_house_type_sub_categoryDTO> copyAm_house_type_sub_categoryList() {
        List<Am_house_type_sub_categoryDTO> am_house_type_sub_categorys = getAm_house_type_sub_categoryList();
        return am_house_type_sub_categorys
                .stream()
                .map(dto -> clone(dto))
                .collect(Collectors.toList());
    }


    public Am_house_type_sub_categoryDTO getAm_house_type_sub_categoryDTOByiD(long iD) {
        return mapOfAm_house_type_sub_categoryDTOToiD.get(iD);
    }

    public Am_house_type_sub_categoryDTO copyAm_house_type_sub_categoryDTOByiD(long iD) {
        return clone(mapOfAm_house_type_sub_categoryDTOToiD.get(iD));
    }


    public List<Am_house_type_sub_categoryDTO> getAm_house_type_sub_categoryDTOByamHouseCatVal(int amHouseCatVal) {
        return new ArrayList<>(mapOfAm_house_type_sub_categoryDTOToamHouseCatVal.getOrDefault(amHouseCatVal, new HashSet<>()));
    }

    public List<Am_house_type_sub_categoryDTO> copyAm_house_type_sub_categoryDTOByamHouseCatVal(int amHouseCatVal) {
        List<Am_house_type_sub_categoryDTO> am_house_type_sub_categorys = getAm_house_type_sub_categoryDTOByamHouseCatVal(amHouseCatVal);
        return am_house_type_sub_categorys
                .stream()
                .map(dto -> clone(dto))
                .collect(Collectors.toList());
    }


    public List<Am_house_type_sub_categoryDTO> getAm_house_type_sub_categoryDTOByamHouseCatDomainName(String amHouseCatDomainName) {
        return new ArrayList<>(mapOfAm_house_type_sub_categoryDTOToamHouseCatDomainName.getOrDefault(amHouseCatDomainName, new HashSet<>()));
    }

    public List<Am_house_type_sub_categoryDTO> copyAm_house_type_sub_categoryDTOByamHouseCatDomainName(String amHouseCatDomainName) {
        List<Am_house_type_sub_categoryDTO> am_house_type_sub_categorys = getAm_house_type_sub_categoryDTOByamHouseCatDomainName(amHouseCatDomainName);
        return am_house_type_sub_categorys
                .stream()
                .map(dto -> clone(dto))
                .collect(Collectors.toList());
    }


    public List<Am_house_type_sub_categoryDTO> getAm_house_type_sub_categoryDTOByamHouseCatNameEn(String amHouseCatNameEn) {
        return new ArrayList<>(mapOfAm_house_type_sub_categoryDTOToamHouseCatNameEn.getOrDefault(amHouseCatNameEn, new HashSet<>()));
    }

    public List<Am_house_type_sub_categoryDTO> copyAm_house_type_sub_categoryDTOByamHouseCatNameEn(String amHouseCatNameEn) {
        List<Am_house_type_sub_categoryDTO> am_house_type_sub_categorys = getAm_house_type_sub_categoryDTOByamHouseCatNameEn(amHouseCatNameEn);
        return am_house_type_sub_categorys
                .stream()
                .map(dto -> clone(dto))
                .collect(Collectors.toList());
    }


    public List<Am_house_type_sub_categoryDTO> getAm_house_type_sub_categoryDTOByamHouseCatNameBn(String amHouseCatNameBn) {
        return new ArrayList<>(mapOfAm_house_type_sub_categoryDTOToamHouseCatNameBn.getOrDefault(amHouseCatNameBn, new HashSet<>()));
    }

    public List<Am_house_type_sub_categoryDTO> copyAm_house_type_sub_categoryDTOByamHouseCatNameBn(String amHouseCatNameBn) {
        List<Am_house_type_sub_categoryDTO> am_house_type_sub_categorys = getAm_house_type_sub_categoryDTOByamHouseCatNameBn(amHouseCatNameBn);
        return am_house_type_sub_categorys
                .stream()
                .map(dto -> clone(dto))
                .collect(Collectors.toList());
    }


    public List<Am_house_type_sub_categoryDTO> getAm_house_type_sub_categoryDTOByamHouseCatOrdering(int amHouseCatOrdering) {
        return new ArrayList<>(mapOfAm_house_type_sub_categoryDTOToamHouseCatOrdering.getOrDefault(amHouseCatOrdering, new HashSet<>()));
    }

    public List<Am_house_type_sub_categoryDTO> copyAm_house_type_sub_categoryDTOByamHouseCatOrdering(int amHouseCatOrdering) {
        List<Am_house_type_sub_categoryDTO> am_house_type_sub_categorys = getAm_house_type_sub_categoryDTOByamHouseCatOrdering(amHouseCatOrdering);
        return am_house_type_sub_categorys
                .stream()
                .map(dto -> clone(dto))
                .collect(Collectors.toList());
    }


    public List<Am_house_type_sub_categoryDTO> getAm_house_type_sub_categoryDTOByamHouseSubCatVal(int amHouseSubCatVal) {
        return new ArrayList<>(mapOfAm_house_type_sub_categoryDTOToamHouseSubCatVal.getOrDefault(amHouseSubCatVal, new HashSet<>()));
    }

    public List<Am_house_type_sub_categoryDTO> copyAm_house_type_sub_categoryDTOByamHouseSubCatVal(int amHouseSubCatVal) {
        List<Am_house_type_sub_categoryDTO> am_house_type_sub_categorys = getAm_house_type_sub_categoryDTOByamHouseSubCatVal(amHouseSubCatVal);
        return am_house_type_sub_categorys
                .stream()
                .map(dto -> clone(dto))
                .collect(Collectors.toList());
    }


    public List<Am_house_type_sub_categoryDTO> getAm_house_type_sub_categoryDTOByamHouseSubCatNameEn(String amHouseSubCatNameEn) {
        return new ArrayList<>(mapOfAm_house_type_sub_categoryDTOToamHouseSubCatNameEn.getOrDefault(amHouseSubCatNameEn, new HashSet<>()));
    }

    public List<Am_house_type_sub_categoryDTO> copyAm_house_type_sub_categoryDTOByamHouseSubCatNameEn(String amHouseSubCatNameEn) {
        List<Am_house_type_sub_categoryDTO> am_house_type_sub_categorys = getAm_house_type_sub_categoryDTOByamHouseSubCatNameEn(amHouseSubCatNameEn);
        return am_house_type_sub_categorys
                .stream()
                .map(dto -> clone(dto))
                .collect(Collectors.toList());
    }


    public List<Am_house_type_sub_categoryDTO> getAm_house_type_sub_categoryDTOByamHouseSubCatNameBn(String amHouseSubCatNameBn) {
        return new ArrayList<>(mapOfAm_house_type_sub_categoryDTOToamHouseSubCatNameBn.getOrDefault(amHouseSubCatNameBn, new HashSet<>()));
    }

    public List<Am_house_type_sub_categoryDTO> copyAm_house_type_sub_categoryDTOByamHouseSubCatNameBn(String amHouseSubCatNameBn) {
        List<Am_house_type_sub_categoryDTO> am_house_type_sub_categorys = getAm_house_type_sub_categoryDTOByamHouseSubCatNameBn(amHouseSubCatNameBn);
        return am_house_type_sub_categorys
                .stream()
                .map(dto -> clone(dto))
                .collect(Collectors.toList());
    }


    public List<Am_house_type_sub_categoryDTO> getAm_house_type_sub_categoryDTOByamHouseSubCatOrdering(int amHouseSubCatOrdering) {
        return new ArrayList<>(mapOfAm_house_type_sub_categoryDTOToamHouseSubCatOrdering.getOrDefault(amHouseSubCatOrdering, new HashSet<>()));
    }

    public List<Am_house_type_sub_categoryDTO> copyAm_house_type_sub_categoryDTOByamHouseSubCatOrdering(int amHouseSubCatOrdering) {
        List<Am_house_type_sub_categoryDTO> am_house_type_sub_categorys = getAm_house_type_sub_categoryDTOByamHouseSubCatOrdering(amHouseSubCatOrdering);
        return am_house_type_sub_categorys
                .stream()
                .map(dto -> clone(dto))
                .collect(Collectors.toList());
    }


    @Override
    public String getTableName() {
        return am_house_type_sub_categoryDAO.getTableName();
    }
}


