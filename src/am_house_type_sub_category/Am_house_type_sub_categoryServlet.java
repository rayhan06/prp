package am_house_type_sub_category;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import am_house.Am_houseRepository;
import common.ApiResponse;
import login.LoginDTO;
import org.apache.log4j.Logger;

import pb.CatDTO;
import pb.CatRepository;
import pb.Utils;
import permission.MenuConstants;

import recruitment_test_name.RecruitmentTestNameModel;
import recruitment_test_name.Recruitment_test_nameDAO;
import recruitment_test_name.Recruitment_test_nameEnum;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.*;

import javax.servlet.http.*;

import common.BaseServlet;
import com.google.gson.Gson;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;

@WebServlet("/Am_house_type_sub_categoryServlet")
@MultipartConfig
public class Am_house_type_sub_categoryServlet extends BaseServlet {
    private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Am_house_type_sub_categoryServlet.class);

    @Override
    public String getTableName() {
        return Am_house_type_sub_categoryDAO.getInstance().getTableName();
    }

    @Override
    public String getServletName() {
        return "Am_house_type_sub_categoryServlet";
    }

    @Override
    public Am_house_type_sub_categoryDAO getCommonDAOService() {
        return Am_house_type_sub_categoryDAO.getInstance();
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[]{MenuConstants.AM_HOUSE_TYPE_SUB_CATEGORY_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[]{MenuConstants.AM_HOUSE_TYPE_SUB_CATEGORY_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[]{MenuConstants.AM_HOUSE_TYPE_SUB_CATEGORY_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Am_house_type_sub_categoryServlet.class;
    }

    private final Gson gson = new Gson();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String actionType = request.getParameter("actionType");
        if(actionType.equals("getHouseSubClass")){
            String language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
            int houseClass = Integer.parseInt(request.getParameter("houseClass"));
            String houseSubClassOptions = Am_house_type_sub_categoryRepository.getInstance()
                    .buildOptionsByHouseClass(language, houseClass, -1);
            ApiResponse.sendSuccessResponse(response, houseSubClassOptions);
            return;
        }
        super.doGet(request,response);
    }

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {

        request.setAttribute("failureMessage", "");
        logger.debug("%%%% addAm_house_type_sub_category");
        String language = userDTO.languageID == SessionConstants.ENGLISH ? "English" : "Bangla";
        boolean isLanEng = language == "English";
        Am_house_type_sub_categoryDTO am_house_type_sub_categoryDTO;

        if (addFlag) {
            am_house_type_sub_categoryDTO = new Am_house_type_sub_categoryDTO();
            am_house_type_sub_categoryDTO.insertionTime = am_house_type_sub_categoryDTO.lastModificationTime
                    = System.currentTimeMillis();
            am_house_type_sub_categoryDTO.insertedBy = am_house_type_sub_categoryDTO.modifiedBy = userDTO.ID;
        } else {
            am_house_type_sub_categoryDTO = Am_house_type_sub_categoryDAO.getInstance()
                    .getDTOFromID(Long.parseLong(request.getParameter("iD")));
            am_house_type_sub_categoryDTO.lastModificationTime = System.currentTimeMillis();
            am_house_type_sub_categoryDTO.modifiedBy = userDTO.ID;
        }

        String Value = "";

        Value = request.getParameter("amHouseCatList");

        if (Value == null || Value.equals("-1") || Value.equals("")) {
            throw new Exception(isLanEng ? "Please select house category" : "অনুগ্রহপূর্বক বাসার শ্রেণী সিলেক্ট করুন");
        } else {
            am_house_type_sub_categoryDTO.amHouseCatVal = Integer.parseInt(Value);
            am_house_type_sub_categoryDTO.amHouseCatDomainName = "am_house_class";
            CatDTO amHouseCatDTO = CatRepository.getDTO("am_house_class", am_house_type_sub_categoryDTO.amHouseCatVal);
            am_house_type_sub_categoryDTO.amHouseCatNameEn = amHouseCatDTO.nameEn;
            am_house_type_sub_categoryDTO.amHouseCatNameBn = amHouseCatDTO.nameBn;
            am_house_type_sub_categoryDTO.amHouseCatOrdering = amHouseCatDTO.ordering;
        }

        Value = request.getParameter("amHouseSubCatNameEn");

        if (Value == null || Value.equals("") || Value.equals("-1")) {
            throw new Exception(isLanEng ? "Please provide house sub category english name" : "অনুগ্রহপূর্বক বাসার উপশ্রেণীর ইংরেজি নাম দিন");
        }
        boolean hasNonEnglish = Value.chars().anyMatch(ch -> !(ch >= ' ' && ch <= '~'));
        if (hasNonEnglish) {
            throw new Exception(isLanEng ? "Please provide recruitment test english name" : "অনুগ্রহপূর্বক নিয়োগ পরীক্ষার ইংরেজি নাম দিন");
        }
        am_house_type_sub_categoryDTO.amHouseSubCatNameEn = (Value);


        Value = request.getParameter("amHouseSubCatNameBn").trim();

        if (Value.equals("") || Value.equals("-1")) {
            throw new Exception(isLanEng ? "Please provide house sub category bangla name" : "অনুগ্রহপূর্বক বাসার উপশ্রেণীর বাংলা নাম দিন");
        }
        boolean hasAllBangla = Value.chars().allMatch(Utils::isValidBanglaCharacter);
        if (!hasAllBangla) {
            throw new Exception(isLanEng ? "Please provide house sub category bangla name" : "অনুগ্রহপূর্বক বাসার উপশ্রেণীর বাংলা নাম দিন");
        }
        am_house_type_sub_categoryDTO.amHouseSubCatNameBn = (Value);

        if (addFlag) {
            Am_house_type_sub_categoryDAO.getInstance().add(am_house_type_sub_categoryDTO);
        } else {
            Am_house_type_sub_categoryDAO.getInstance().update(am_house_type_sub_categoryDTO);
        }

        return am_house_type_sub_categoryDTO;
    }
}

