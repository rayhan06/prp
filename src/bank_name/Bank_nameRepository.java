package bank_name;

import common.NameRepository;


public class Bank_nameRepository extends NameRepository {

	private Bank_nameRepository(){
		super(Bank_nameDAO.getInstance(),Bank_nameRepository.class);
	}

	private static class Bank_nameRepositoryLoader{
		static Bank_nameRepository INSTANCE = new Bank_nameRepository();
	}

	public synchronized static Bank_nameRepository getInstance(){
		return Bank_nameRepositoryLoader.INSTANCE;
	}
}


