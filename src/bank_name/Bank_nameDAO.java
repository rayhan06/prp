package bank_name;

import common.NameDao;

public class Bank_nameDAO extends NameDao {

    private Bank_nameDAO() {
        super("bank_name");
    }

    private static class LazyLoader{
        static final Bank_nameDAO INSTANCE = new Bank_nameDAO();
    }

    public static Bank_nameDAO getInstance(){
        return LazyLoader.INSTANCE;
    }
}