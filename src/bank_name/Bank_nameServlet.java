package bank_name;

import common.BaseServlet;
import common.NameDao;
import common.NameInterface;
import common.NameRepository;
import language.LC;
import permission.MenuConstants;
import sessionmanager.SessionConstants;
import user.UserDTO;
import util.CommonDTO;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;

@WebServlet("/Bank_nameServlet")
@MultipartConfig
public class Bank_nameServlet extends BaseServlet implements NameInterface {
    private final Bank_nameDAO bankNameDAO = Bank_nameDAO.getInstance();

    public String commonPartOfDispatchURL(){
        return  "common/name";
    }

    @Override
    public String getTableName() {
        return bankNameDAO.getTableName();
    }

    @Override
    public String getServletName() {
        return "Bank_nameServlet";
    }

    @Override
    public NameDao getCommonDAOService() {
        return bankNameDAO;
    }

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        return addT(request,addFlag,userDTO,bankNameDAO);
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[] {MenuConstants.BANK_NAME_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[] {MenuConstants.BANK_NAME_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[] {MenuConstants.BANK_NAME_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Bank_nameServlet.class;
    }

    @Override
    public int getSearchTitleValue() {
        return LC.BANK_NAME_SEARCH_BANK_NAME_SEARCH_FORMNAME;
    }

    @Override
    public String get_p_navigatorName() {
        return SessionConstants.NAV_BANK_NAME;
    }

    @Override
    public String get_p_dtoCollectionName() {
        return SessionConstants.VIEW_BANK_NAME;
    }

    @Override
    public NameRepository getNameRepository() {
        return Bank_nameRepository.getInstance();
    }

    @Override
    public int getAddTitleValue() {
        return LC.BANK_NAME_ADD_BANK_NAME_ADD_FORMNAME;
    }

    @Override
    public int getEditTitleValue() {
        return LC.BANK_NAME_EDIT_BANK_NAME_EDIT_FORMNAME;
    }

    @Override
    public void init(HttpServletRequest request) throws ServletException {
        setCommonAttributes(request,getServletName(),getCommonDAOService());
    }
}