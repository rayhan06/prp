package offices;

import java.io.IOException;
import java.io.*;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import org.apache.log4j.Logger;

import login.LoginDTO;
import permission.MenuConstants;
import role.PermissionRepository;


import sessionmanager.SessionConstants;

import user.UserDTO;
import user.UserRepository;

import util.RecordNavigationManager2;

import java.util.*;
import javax.servlet.http.*;
import java.util.UUID;

import offices.Constants;


import geolocation.GeoLocationDAO2;
import java.util.StringTokenizer;

import pb.*;
import pbReport.*;

/**
 * Servlet implementation class OfficesServlet
 */
@WebServlet("/OfficesServlet")
@MultipartConfig
public class OfficesServlet extends HttpServlet 
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(OfficesServlet.class);
    /**
     * @see HttpServlet#HttpServlet()
     */
    public OfficesServlet() 
	{
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		System.out.println("In doget request = " + request);
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		try
		{
			String actionType = request.getParameter("actionType");
			if(actionType.equals("getAddPage"))
			{
				//if(PermissionRepository.getInstance().checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.OFFICES_ADD))
				{
					getAddPage(request, response);
				}
				
			}
			else if(actionType.equals("getEditPage"))
			{
				//if(PermissionRepository.getInstance().checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.OFFICES_UPDATE))
				{
					getOffices(request, response);
				}
							
			}
			else if(actionType.equals("getURL"))
			{
				String URL = request.getParameter("URL");
				System.out.println("URL = " + URL);
				response.sendRedirect(URL);			
			}
			else if(actionType.equals("search"))
			{
				System.out.println("search requested");
				//if(PermissionRepository.getInstance().checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.OFFICES_SEARCH))
				{
					searchOffices(request, response);
				}
							
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}

	private void getAddPage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		request.setAttribute("ID", -1L);
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("offices/officesEdit.jsp");
		requestDispatcher.forward(request, response);
	}
	private String getFileName(final Part part) 
	{
	    final String partHeader = part.getHeader("content-disposition");
	    System.out.println("Part Header = {0}" +  partHeader);
	    for (String content : part.getHeader("content-disposition").split(";")) {
	        if (content.trim().startsWith("filename")) {
	            return content.substring(
	                    content.indexOf('=') + 1).trim().replace("\"", "");
	        }
	    }
	    return null;
	}

	
	

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		// TODO Auto-generated method stub
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		System.out.println("doPost");
		
		try
		{
			String actionType = request.getParameter("actionType");
			System.out.println("actionType = " + actionType);
			if(actionType.equals("add"))
			{
				
				//if(PermissionRepository.getInstance().checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.OFFICES_ADD))
				{
					System.out.println("going to  addOffices ");
					addOffices(request, response, true);
				}
				
				
			}
			else if(actionType.equals("edit"))
			{
				
				//if(PermissionRepository.getInstance().checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.OFFICES_UPDATE))
				{
					addOffices(request, response, false);
				}
				
			}
			else if(actionType.equals("delete"))
			{
				deleteOffices(request, response);
			}
			else if(actionType.equals("search"))
			{
				//if(PermissionRepository.getInstance().checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.OFFICES_SEARCH))
				{
					searchOffices(request, response);
				}
				
			}
			else if(actionType.equals("getGeo"))
			{
				System.out.println("going to geoloc ");
				request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
			}
			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}
	private void addOffices(HttpServletRequest request, HttpServletResponse response, Boolean addFlag) throws IOException 
	{
		// TODO Auto-generated method stub
		try 
		{
			request.setAttribute("failureMessage", "");
			System.out.println("%%%% addOffices");
			String path = getServletContext().getRealPath("/img2/");
			OfficesDAO officesDAO = new OfficesDAO();
			OfficesDTO officesDTO;
			String FileNamePrefix;
			if(addFlag == true)
			{
				officesDTO = new OfficesDTO();
				FileNamePrefix = UUID.randomUUID().toString().substring(0, 10);
			}
			else
			{
				officesDTO = officesDAO.getOfficesDTOByID(Long.parseLong(request.getParameter("identity")));
				FileNamePrefix = request.getParameter("identity");
			}
			
			String Value = "";
			Value = request.getParameter("officeMinistryId");
			System.out.println("officeMinistryId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				officesDTO.officeMinistryId = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null value, not updating" + " = " + Value);
			}
			Value = request.getParameter("officeLayerId");
			System.out.println("officeLayerId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				officesDTO.officeLayerId = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null value, not updating" + " = " + Value);
			}
			Value = request.getParameter("customLayerId");
			System.out.println("customLayerId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				officesDTO.customLayerId = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null value, not updating" + " = " + Value);
			}
			Value = request.getParameter("officeOriginId");
			System.out.println("officeOriginId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				officesDTO.officeOriginId = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null value, not updating" + " = " + Value);
			}
			Value = request.getParameter("officeNameEng");
			System.out.println("officeNameEng = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				officesDTO.officeNameEng = (Value);
			}
			else
			{
				System.out.println("FieldName has a null value, not updating" + " = " + Value);
			}
			Value = request.getParameter("officeNameBng");
			System.out.println("officeNameBng = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				officesDTO.officeNameBng = (Value);
			}
			else
			{
				System.out.println("FieldName has a null value, not updating" + " = " + Value);
			}
			Value = request.getParameter("geoDivisionId");
			System.out.println("geoDivisionId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				officesDTO.geoDivisionId = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null value, not updating" + " = " + Value);
			}
			Value = request.getParameter("geoDistrictId");
			System.out.println("geoDistrictId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				officesDTO.geoDistrictId = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null value, not updating" + " = " + Value);
			}
			Value = request.getParameter("geoUpazilaId");
			System.out.println("geoUpazilaId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				officesDTO.geoUpazilaId = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null value, not updating" + " = " + Value);
			}
			Value = request.getParameter("geoUnionId");
			System.out.println("geoUnionId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				officesDTO.geoUnionId = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null value, not updating" + " = " + Value);
			}
			Value = request.getParameter("geoCityCorporationId");
			System.out.println("geoCityCorporationId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				officesDTO.geoCityCorporationId = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null value, not updating" + " = " + Value);
			}
			Value = request.getParameter("geoCityCorporationWardId");
			System.out.println("geoCityCorporationWardId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				officesDTO.geoCityCorporationWardId = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null value, not updating" + " = " + Value);
			}
			Value = request.getParameter("geoMunicipalityId");
			System.out.println("geoMunicipalityId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				officesDTO.geoMunicipalityId = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null value, not updating" + " = " + Value);
			}
			Value = request.getParameter("geoMunicipalityWardId");
			System.out.println("geoMunicipalityWardId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				officesDTO.geoMunicipalityWardId = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null value, not updating" + " = " + Value);
			}
			Value = request.getParameter("officeCode");
			System.out.println("officeCode = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				officesDTO.officeCode = (Value);
			}
			else
			{
				System.out.println("FieldName has a null value, not updating" + " = " + Value);
			}
			Value = request.getParameter("officeAddress");
			System.out.println("officeAddress = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				{
					StringTokenizer tok3=new StringTokenizer(Value, ":");
					int i = 0;
					String addressDetails = "", address_id = "";
					while(tok3.hasMoreElements())
					{
						if(i == 0)
						{
							address_id = tok3.nextElement() + "";
						}
						else if(i == 1)
						{
							addressDetails = tok3.nextElement() + "";
						}
						i ++;
					}
					officesDTO.officeAddress = address_id + ":" + GeoLocationDAO2.getLocationText(Integer.parseInt(address_id))
					+ ":" + addressDetails;
				}
				
			}
			else
			{
				System.out.println("FieldName has a null value, not updating" + " = " + Value);
			}
			Value = request.getParameter("digitalNothiCode");
			System.out.println("digitalNothiCode = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				officesDTO.digitalNothiCode = (Value);
			}
			else
			{
				System.out.println("FieldName has a null value, not updating" + " = " + Value);
			}
			Value = request.getParameter("referenceCode");
			System.out.println("referenceCode = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				officesDTO.referenceCode = (Value);
			}
			else
			{
				System.out.println("FieldName has a null value, not updating" + " = " + Value);
			}
			Value = request.getParameter("officePhone");
			System.out.println("officePhone = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				officesDTO.officePhone = (Value);
			}
			else
			{
				System.out.println("FieldName has a null value, not updating" + " = " + Value);
			}
			Value = request.getParameter("officeMobile");
			System.out.println("officeMobile = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				officesDTO.officeMobile = (Value);
			}
			else
			{
				System.out.println("FieldName has a null value, not updating" + " = " + Value);
			}
			Value = request.getParameter("officeFax");
			System.out.println("officeFax = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				officesDTO.officeFax = (Value);
			}
			else
			{
				System.out.println("FieldName has a null value, not updating" + " = " + Value);
			}
			Value = request.getParameter("officeEmail");
			System.out.println("officeEmail = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				officesDTO.officeEmail = (Value);
			}
			else
			{
				System.out.println("FieldName has a null value, not updating" + " = " + Value);
			}
			Value = request.getParameter("officeWeb");
			System.out.println("officeWeb = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				officesDTO.officeWeb = (Value);
			}
			else
			{
				System.out.println("FieldName has a null value, not updating" + " = " + Value);
			}
			Value = request.getParameter("parentOfficeId");
			System.out.println("parentOfficeId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				officesDTO.parentOfficeId = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null value, not updating" + " = " + Value);
			}
			Value = request.getParameter("status");
			System.out.println("status = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				officesDTO.status = Boolean.parseBoolean(Value);
			}
			else
			{
				System.out.println("FieldName has a null value, not updating" + " = " + Value);
			}
			Value = request.getParameter("createdBy");
			System.out.println("createdBy = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				officesDTO.createdBy = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null value, not updating" + " = " + Value);
			}
			Value = request.getParameter("modifiedBy");
			System.out.println("modifiedBy = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				officesDTO.modifiedBy = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null value, not updating" + " = " + Value);
			}
			Value = request.getParameter("created");
			System.out.println("created = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				officesDTO.created = (Value);
			}
			else
			{
				System.out.println("FieldName has a null value, not updating" + " = " + Value);
			}
			Value = request.getParameter("modified");
			System.out.println("modified = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				officesDTO.modified = (Value);
			}
			else
			{
				System.out.println("FieldName has a null value, not updating" + " = " + Value);
			}
			Value = request.getParameter("geoThanaId");
			System.out.println("geoThanaId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				officesDTO.geoThanaId = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null value, not updating" + " = " + Value);
			}
			Value = request.getParameter("geoPostofficeId");
			System.out.println("geoPostofficeId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				officesDTO.geoPostofficeId = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null value, not updating" + " = " + Value);
			}
			Value = request.getParameter("geoCountryId");
			System.out.println("geoCountryId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				officesDTO.geoCountryId = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null value, not updating" + " = " + Value);
			}
			Value = request.getParameter("approvalPathType");
			System.out.println("approvalPathType = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				officesDTO.approvalPathType = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null value, not updating" + " = " + Value);
			}
			
			System.out.println("Done adding  addOffices dto = " + officesDTO);
			
			if(addFlag == true)
			{
				officesDAO.addOffices(officesDTO);
			}
			else
			{
				officesDAO.updateOffices(officesDTO);
				
			}
			
			
			
			
			
			
			
			
			String inPlaceSubmit = request.getParameter("inplacesubmit");
			
			if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
			{
				getOffices(request, response);
			}
			else
			{
				response.sendRedirect("OfficesServlet?actionType=search");
			}
					
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	



	
	
	

	private void deleteOffices(HttpServletRequest request, HttpServletResponse response) throws IOException 
	{				
		try 
		{
			String[] IDsToDelete = request.getParameterValues("ID");
			for(int i = 0; i < IDsToDelete.length; i ++)
			{
				long id = Long.parseLong(IDsToDelete[i]);
				System.out.println("###DELETING " + IDsToDelete[i]);				
				new OfficesDAO().deleteOfficesByID(id);
			}			
		}
		catch (Exception ex) 
		{
			logger.debug(ex);
		}
		response.sendRedirect("OfficesServlet?actionType=search");
	}

	private void getOffices(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		System.out.println("in getOffices");
		OfficesDTO officesDTO = null;
		try 
		{
			officesDTO = new OfficesDAO().getOfficesDTOByID(Long.parseLong(request.getParameter("ID")));
			request.setAttribute("ID", officesDTO.iD);
			request.setAttribute("officesDTO",officesDTO);
			
			String URL= "";
			
			String inPlaceEdit = request.getParameter("inplaceedit");
			String inPlaceSubmit = request.getParameter("inplacesubmit");
			String getBodyOnly = request.getParameter("getBodyOnly");
			
			if(inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase(""))
			{
				URL = "offices/officesInPlaceEdit.jsp";	
				request.setAttribute("inplaceedit","");				
			}
			else if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
			{
				URL = "offices/officesSearchRow.jsp";
				request.setAttribute("inplacesubmit","");					
			}
			else
			{
				if(getBodyOnly  != null && !getBodyOnly.equalsIgnoreCase(""))
				{
					URL = "offices/officesEditBody.jsp?actionType=edit";
				}
				else
				{
					URL = "offices/officesEdit.jsp?actionType=edit";
				}				
			}
			
			RequestDispatcher rd = request.getRequestDispatcher(URL);
			rd.forward(request, response);
		}
		catch (NumberFormatException e) 
		{
			e.printStackTrace();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	private void searchOffices(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		System.out.println("in  searchOffices 1");
        OfficesDAO officesDAO = new OfficesDAO();
		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
		String ajax = request.getParameter("ajax");
		boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);
		
        RecordNavigationManager2 rnManager = new RecordNavigationManager2(
			SessionConstants.NAV_OFFICES,
			request,
			officesDAO,
			SessionConstants.VIEW_OFFICES,
			SessionConstants.SEARCH_OFFICES,
			"offices");
        try
        {
			System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        }
        catch(Exception e)
        {
			System.out.println("failed to dojob" + e);
        }

        RequestDispatcher rd;
        if(hasAjax == false)
        {
        	System.out.println("Going to offices/officesSearch.jsp");
        	rd = request.getRequestDispatcher("offices/officesSearch.jsp");
        }
        else
        {
        	System.out.println("Going to offices/officesSearchForm.jsp");
        	rd = request.getRequestDispatcher("offices/officesSearchForm.jsp");
        }
		rd.forward(request, response);
	}
	
}

