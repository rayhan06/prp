package offices;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;


import org.apache.log4j.Logger;

import dbm.*;
import login.LoginDTO;
import repository.RepositoryManager;

import util.NavigationService2;


public class OfficesDAO  extends NavigationService2
{
	
	Logger logger = Logger.getLogger(getClass());
	
	
	public long addOffices(OfficesDTO officesDTO) throws Exception{
		
		Connection connection = null;
		PreparedStatement ps = null;

		long lastModificationTime = System.currentTimeMillis();	

		try{
			connection = DBMW.getInstance().getConnection();
			
			if(connection == null)
			{
				System.out.println("nullconn");
			}

			officesDTO.iD = DBMW.getInstance().getNextSequenceId("offices");

			String sql = "INSERT INTO offices";
			
			sql += " (";
			sql += "ID";
			sql += ", ";
			sql += "office_ministry_id";
			sql += ", ";
			sql += "office_layer_id";
			sql += ", ";
			sql += "custom_layer_id";
			sql += ", ";
			sql += "office_origin_id";
			sql += ", ";
			sql += "office_name_eng";
			sql += ", ";
			sql += "office_name_bng";
			sql += ", ";
			sql += "geo_division_id";
			sql += ", ";
			sql += "geo_district_id";
			sql += ", ";
			sql += "geo_upazila_id";
			sql += ", ";
			sql += "geo_union_id";
			sql += ", ";
			sql += "geo_city_corporation_id";
			sql += ", ";
			sql += "geo_city_corporation_ward_id";
			sql += ", ";
			sql += "geo_municipality_id";
			sql += ", ";
			sql += "geo_municipality_ward_id";
			sql += ", ";
			sql += "office_code";
			sql += ", ";
			sql += "office_address";
			sql += ", ";
			sql += "digital_nothi_code";
			sql += ", ";
			sql += "reference_code";
			sql += ", ";
			sql += "office_phone";
			sql += ", ";
			sql += "office_mobile";
			sql += ", ";
			sql += "office_fax";
			sql += ", ";
			sql += "office_email";
			sql += ", ";
			sql += "office_web";
			sql += ", ";
			sql += "parent_office_id";
			sql += ", ";
			sql += "status";
			sql += ", ";
			sql += "created_by";
			sql += ", ";
			sql += "modified_by";
			sql += ", ";
			sql += "created";
			sql += ", ";
			sql += "modified";
			sql += ", ";
			sql += "geo_thana_id";
			sql += ", ";
			sql += "geo_postoffice_id";
			sql += ", ";
			sql += "geo_country_id";
			sql += ", ";
			sql += "approval_path_type";
			sql += ", ";
			sql += "isDeleted";
			sql += ", ";
			sql += "lastModificationTime";
			sql += ")";
			
			
            sql += " VALUES(";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ")";
				
			printSql(sql);

			ps = connection.prepareStatement(sql);
			
			
			

			int index = 1;

			ps.setObject(index++,officesDTO.iD);
			ps.setObject(index++,officesDTO.officeMinistryId);
			ps.setObject(index++,officesDTO.officeLayerId);
			ps.setObject(index++,officesDTO.customLayerId);
			ps.setObject(index++,officesDTO.officeOriginId);
			ps.setObject(index++,officesDTO.officeNameEng);
			ps.setObject(index++,officesDTO.officeNameBng);
			ps.setObject(index++,officesDTO.geoDivisionId);
			ps.setObject(index++,officesDTO.geoDistrictId);
			ps.setObject(index++,officesDTO.geoUpazilaId);
			ps.setObject(index++,officesDTO.geoUnionId);
			ps.setObject(index++,officesDTO.geoCityCorporationId);
			ps.setObject(index++,officesDTO.geoCityCorporationWardId);
			ps.setObject(index++,officesDTO.geoMunicipalityId);
			ps.setObject(index++,officesDTO.geoMunicipalityWardId);
			ps.setObject(index++,officesDTO.officeCode);
			ps.setObject(index++,officesDTO.officeAddress);
			ps.setObject(index++,officesDTO.digitalNothiCode);
			ps.setObject(index++,officesDTO.referenceCode);
			ps.setObject(index++,officesDTO.officePhone);
			ps.setObject(index++,officesDTO.officeMobile);
			ps.setObject(index++,officesDTO.officeFax);
			ps.setObject(index++,officesDTO.officeEmail);
			ps.setObject(index++,officesDTO.officeWeb);
			ps.setObject(index++,officesDTO.parentOfficeId);
			ps.setObject(index++,officesDTO.status);
			ps.setObject(index++,officesDTO.createdBy);
			ps.setObject(index++,officesDTO.modifiedBy);
			ps.setObject(index++,officesDTO.created);
			ps.setObject(index++,officesDTO.modified);
			ps.setObject(index++,officesDTO.geoThanaId);
			ps.setObject(index++,officesDTO.geoPostofficeId);
			ps.setObject(index++,officesDTO.geoCountryId);
			ps.setObject(index++,officesDTO.approvalPathType);
			ps.setObject(index++,officesDTO.isDeleted);
			ps.setObject(index++, lastModificationTime);
			
			System.out.println(ps);
			ps.execute();
			
			
			recordUpdateTime(connection, lastModificationTime);

		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if (ps != null) {
					ps.close();
				}
			} catch(Exception e){}
			try{
				if(connection != null){
					DBMW.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		return officesDTO.iD;		
	}
	
	
	//need another getter for repository
	public OfficesDTO getOfficesDTOByID (long ID) throws Exception{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		OfficesDTO officesDTO = null;
		try{
			
			String sql = "SELECT * ";

			sql += " FROM offices";
			
            sql += " WHERE ID=" + ID;
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			if(rs.next()){
				officesDTO = new OfficesDTO();

				officesDTO.iD = rs.getLong("ID");
				officesDTO.officeMinistryId = rs.getInt("office_ministry_id");
				officesDTO.officeLayerId = rs.getInt("office_layer_id");
				officesDTO.customLayerId = rs.getInt("custom_layer_id");
				officesDTO.officeOriginId = rs.getInt("office_origin_id");
				officesDTO.officeNameEng = rs.getString("office_name_eng");
				officesDTO.officeNameBng = rs.getString("office_name_bng");
				officesDTO.geoDivisionId = rs.getInt("geo_division_id");
				officesDTO.geoDistrictId = rs.getInt("geo_district_id");
				officesDTO.geoUpazilaId = rs.getInt("geo_upazila_id");
				officesDTO.geoUnionId = rs.getInt("geo_union_id");
				officesDTO.geoCityCorporationId = rs.getInt("geo_city_corporation_id");
				officesDTO.geoCityCorporationWardId = rs.getInt("geo_city_corporation_ward_id");
				officesDTO.geoMunicipalityId = rs.getInt("geo_municipality_id");
				officesDTO.geoMunicipalityWardId = rs.getInt("geo_municipality_ward_id");
				officesDTO.officeCode = rs.getString("office_code");
				officesDTO.officeAddress = rs.getString("office_address");
				officesDTO.digitalNothiCode = rs.getString("digital_nothi_code");
				officesDTO.referenceCode = rs.getString("reference_code");
				officesDTO.officePhone = rs.getString("office_phone");
				officesDTO.officeMobile = rs.getString("office_mobile");
				officesDTO.officeFax = rs.getString("office_fax");
				officesDTO.officeEmail = rs.getString("office_email");
				officesDTO.officeWeb = rs.getString("office_web");
				officesDTO.parentOfficeId = rs.getInt("parent_office_id");
				officesDTO.status = rs.getBoolean("status");
				officesDTO.createdBy = rs.getInt("created_by");
				officesDTO.modifiedBy = rs.getInt("modified_by");
				officesDTO.created = rs.getString("created");
				officesDTO.modified = rs.getString("modified");
				officesDTO.geoThanaId = rs.getInt("geo_thana_id");
				officesDTO.geoPostofficeId = rs.getInt("geo_postoffice_id");
				officesDTO.geoCountryId = rs.getInt("geo_country_id");
				officesDTO.approvalPathType = rs.getInt("approval_path_type");
				officesDTO.isDeleted = rs.getInt("isDeleted");
				officesDTO.lastModificationTime = rs.getLong("lastModificationTime");

			}			
			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DBMR.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
		return officesDTO;
	}

	public OfficesDTO getOfficesDTOByOfficeOriginId (Integer ID) throws Exception{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		OfficesDTO officesDTO = null;
		try{

			String sql = "SELECT * ";

			sql += " FROM offices";

			sql += " WHERE office_origin_id=" + ID;

			printSql(sql);

			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			if(rs.next()){
				officesDTO = new OfficesDTO();

				officesDTO.iD = rs.getLong("ID");
				officesDTO.officeMinistryId = rs.getInt("office_ministry_id");
				officesDTO.officeLayerId = rs.getInt("office_layer_id");
				officesDTO.customLayerId = rs.getInt("custom_layer_id");
				officesDTO.officeOriginId = rs.getInt("office_origin_id");
				officesDTO.officeNameEng = rs.getString("office_name_eng");
				officesDTO.officeNameBng = rs.getString("office_name_bng");
				officesDTO.geoDivisionId = rs.getInt("geo_division_id");
				officesDTO.geoDistrictId = rs.getInt("geo_district_id");
				officesDTO.geoUpazilaId = rs.getInt("geo_upazila_id");
				officesDTO.geoUnionId = rs.getInt("geo_union_id");
				officesDTO.geoCityCorporationId = rs.getInt("geo_city_corporation_id");
				officesDTO.geoCityCorporationWardId = rs.getInt("geo_city_corporation_ward_id");
				officesDTO.geoMunicipalityId = rs.getInt("geo_municipality_id");
				officesDTO.geoMunicipalityWardId = rs.getInt("geo_municipality_ward_id");
				officesDTO.officeCode = rs.getString("office_code");
				officesDTO.officeAddress = rs.getString("office_address");
				officesDTO.digitalNothiCode = rs.getString("digital_nothi_code");
				officesDTO.referenceCode = rs.getString("reference_code");
				officesDTO.officePhone = rs.getString("office_phone");
				officesDTO.officeMobile = rs.getString("office_mobile");
				officesDTO.officeFax = rs.getString("office_fax");
				officesDTO.officeEmail = rs.getString("office_email");
				officesDTO.officeWeb = rs.getString("office_web");
				officesDTO.parentOfficeId = rs.getInt("parent_office_id");
				officesDTO.status = rs.getBoolean("status");
				officesDTO.createdBy = rs.getInt("created_by");
				officesDTO.modifiedBy = rs.getInt("modified_by");
				officesDTO.created = rs.getString("created");
				officesDTO.modified = rs.getString("modified");
				officesDTO.geoThanaId = rs.getInt("geo_thana_id");
				officesDTO.geoPostofficeId = rs.getInt("geo_postoffice_id");
				officesDTO.geoCountryId = rs.getInt("geo_country_id");
				officesDTO.isDeleted = rs.getInt("isDeleted");
				officesDTO.lastModificationTime = rs.getLong("lastModificationTime");

			}


		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}

			try{
				if (connection != null){
					DBMR.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		return officesDTO;
	}


	public void updateOffice_approvalPathByID(long ID, int approval_path_type) throws Exception{
		
		Connection connection = null;
		PreparedStatement ps = null;

		long lastModificationTime = System.currentTimeMillis();	
		try{
			connection = DBMW.getInstance().getConnection();

			String sql = "UPDATE offices";
			
			sql += " SET ";
			sql += "approval_path_type=? , lastModificationTime=" + lastModificationTime;
			sql += " WHERE id  =  " + ID;

				
			printSql(sql);

			ps = connection.prepareStatement(sql);
			
			

			int index = 1;
			
			ps.setObject(index++,approval_path_type);
			System.out.println(ps);
			ps.executeUpdate();
			


			
			recordUpdateTime(connection, lastModificationTime);

						
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if (ps != null) {
					ps.close();
				}
			} catch(Exception e){}
			try{
				if(connection != null){
					DBMW.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
	}
	public long updateOffices(OfficesDTO officesDTO) throws Exception{
		
		Connection connection = null;
		PreparedStatement ps = null;

		long lastModificationTime = System.currentTimeMillis();	
		try{
			connection = DBMW.getInstance().getConnection();

			String sql = "UPDATE offices";
			
			sql += " SET ";
			sql += "office_ministry_id=?";
			sql += ", ";
			sql += "office_layer_id=?";
			sql += ", ";
			sql += "custom_layer_id=?";
			sql += ", ";
			sql += "office_origin_id=?";
			sql += ", ";
			sql += "office_name_eng=?";
			sql += ", ";
			sql += "office_name_bng=?";
			sql += ", ";
			sql += "geo_division_id=?";
			sql += ", ";
			sql += "geo_district_id=?";
			sql += ", ";
			sql += "geo_upazila_id=?";
			sql += ", ";
			sql += "geo_union_id=?";
			sql += ", ";
			sql += "geo_city_corporation_id=?";
			sql += ", ";
			sql += "geo_city_corporation_ward_id=?";
			sql += ", ";
			sql += "geo_municipality_id=?";
			sql += ", ";
			sql += "geo_municipality_ward_id=?";
			sql += ", ";
			sql += "office_code=?";
			sql += ", ";
			sql += "office_address=?";
			sql += ", ";
			sql += "digital_nothi_code=?";
			sql += ", ";
			sql += "reference_code=?";
			sql += ", ";
			sql += "office_phone=?";
			sql += ", ";
			sql += "office_mobile=?";
			sql += ", ";
			sql += "office_fax=?";
			sql += ", ";
			sql += "office_email=?";
			sql += ", ";
			sql += "office_web=?";
			sql += ", ";
			sql += "parent_office_id=?";
			sql += ", ";
			sql += "status=?";
			sql += ", ";
			sql += "created_by=?";
			sql += ", ";
			sql += "modified_by=?";
			sql += ", ";
			sql += "created=?";
			sql += ", ";
			sql += "modified=?";
			sql += ", ";
			sql += "geo_thana_id=?";
			sql += ", ";
			sql += "geo_postoffice_id=?";
			sql += ", ";
			sql += "geo_country_id=?";
			sql += ", ";
			sql += "approval_path_type=?";
			sql += ", lastModificationTime = "	+ lastModificationTime + "";
            sql += " WHERE ID = " + officesDTO.iD;
				
			printSql(sql);

			ps = connection.prepareStatement(sql);
			
			

			int index = 1;
			
			ps.setObject(index++,officesDTO.officeMinistryId);
			ps.setObject(index++,officesDTO.officeLayerId);
			ps.setObject(index++,officesDTO.customLayerId);
			ps.setObject(index++,officesDTO.officeOriginId);
			ps.setObject(index++,officesDTO.officeNameEng);
			ps.setObject(index++,officesDTO.officeNameBng);
			ps.setObject(index++,officesDTO.geoDivisionId);
			ps.setObject(index++,officesDTO.geoDistrictId);
			ps.setObject(index++,officesDTO.geoUpazilaId);
			ps.setObject(index++,officesDTO.geoUnionId);
			ps.setObject(index++,officesDTO.geoCityCorporationId);
			ps.setObject(index++,officesDTO.geoCityCorporationWardId);
			ps.setObject(index++,officesDTO.geoMunicipalityId);
			ps.setObject(index++,officesDTO.geoMunicipalityWardId);
			ps.setObject(index++,officesDTO.officeCode);
			ps.setObject(index++,officesDTO.officeAddress);
			ps.setObject(index++,officesDTO.digitalNothiCode);
			ps.setObject(index++,officesDTO.referenceCode);
			ps.setObject(index++,officesDTO.officePhone);
			ps.setObject(index++,officesDTO.officeMobile);
			ps.setObject(index++,officesDTO.officeFax);
			ps.setObject(index++,officesDTO.officeEmail);
			ps.setObject(index++,officesDTO.officeWeb);
			ps.setObject(index++,officesDTO.parentOfficeId);
			ps.setObject(index++,officesDTO.status);
			ps.setObject(index++,officesDTO.createdBy);
			ps.setObject(index++,officesDTO.modifiedBy);
			ps.setObject(index++,officesDTO.created);
			ps.setObject(index++,officesDTO.modified);
			ps.setObject(index++,officesDTO.geoThanaId);
			ps.setObject(index++,officesDTO.geoPostofficeId);
			ps.setObject(index++,officesDTO.geoCountryId);
			ps.setObject(index++,officesDTO.approvalPathType);
			System.out.println(ps);
			ps.executeUpdate();
			


			
			recordUpdateTime(connection, lastModificationTime);

						
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if (ps != null) {
					ps.close();
				}
			} catch(Exception e){}
			try{
				if(connection != null){
					DBMW.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		return officesDTO.iD;
	}
	
	public void deleteOfficesByID(long ID) throws Exception{
		Connection connection = null;
		Statement stmt = null;
		PreparedStatement ps = null;
		
		long lastModificationTime = System.currentTimeMillis();	
		try{
			String sql = "UPDATE offices";
			
			sql += " SET isDeleted=1,lastModificationTime="+ lastModificationTime +" WHERE ID = " + ID;
			
			printSql(sql);

			connection = DBMW.getInstance().getConnection();
			stmt  = connection.createStatement();
			stmt.execute(sql);
			

			
			recordUpdateTime(connection, lastModificationTime);

			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DBMW.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
	}

	
	
	public List<OfficesDTO> getDTOs(Collection recordIDs){
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		OfficesDTO officesDTO = null;
		List<OfficesDTO> officesDTOList = new ArrayList<>();
		if(recordIDs.isEmpty()){
			return officesDTOList;
		}
		try{
			
			String sql = "SELECT * ";

			sql += " FROM offices";
            
            sql += " WHERE ID IN ( ";

			for(int i = 0;i<recordIDs.size();i++){
				if(i!=0){
					sql+=",";
				}
				sql+=((ArrayList)recordIDs).get(i);
			}
			sql+=")  order by lastModificationTime desc";
			
			printSql(sql);
			
			logger.debug("sql " + sql);
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				officesDTO = new OfficesDTO();
				officesDTO.iD = rs.getLong("ID");
				officesDTO.officeMinistryId = rs.getInt("office_ministry_id");
				officesDTO.officeLayerId = rs.getInt("office_layer_id");
				officesDTO.customLayerId = rs.getInt("custom_layer_id");
				officesDTO.officeOriginId = rs.getInt("office_origin_id");
				officesDTO.officeNameEng = rs.getString("office_name_eng");
				officesDTO.officeNameBng = rs.getString("office_name_bng");
				officesDTO.geoDivisionId = rs.getInt("geo_division_id");
				officesDTO.geoDistrictId = rs.getInt("geo_district_id");
				officesDTO.geoUpazilaId = rs.getInt("geo_upazila_id");
				officesDTO.geoUnionId = rs.getInt("geo_union_id");
				officesDTO.geoCityCorporationId = rs.getInt("geo_city_corporation_id");
				officesDTO.geoCityCorporationWardId = rs.getInt("geo_city_corporation_ward_id");
				officesDTO.geoMunicipalityId = rs.getInt("geo_municipality_id");
				officesDTO.geoMunicipalityWardId = rs.getInt("geo_municipality_ward_id");
				officesDTO.officeCode = rs.getString("office_code");
				officesDTO.officeAddress = rs.getString("office_address");
				officesDTO.digitalNothiCode = rs.getString("digital_nothi_code");
				officesDTO.referenceCode = rs.getString("reference_code");
				officesDTO.officePhone = rs.getString("office_phone");
				officesDTO.officeMobile = rs.getString("office_mobile");
				officesDTO.officeFax = rs.getString("office_fax");
				officesDTO.officeEmail = rs.getString("office_email");
				officesDTO.officeWeb = rs.getString("office_web");
				officesDTO.parentOfficeId = rs.getInt("parent_office_id");
				officesDTO.status = rs.getBoolean("status");
				officesDTO.createdBy = rs.getInt("created_by");
				officesDTO.modifiedBy = rs.getInt("modified_by");
				officesDTO.created = rs.getString("created");
				officesDTO.modified = rs.getString("modified");
				officesDTO.geoThanaId = rs.getInt("geo_thana_id");
				officesDTO.geoPostofficeId = rs.getInt("geo_postoffice_id");
				officesDTO.geoCountryId = rs.getInt("geo_country_id");
				officesDTO.approvalPathType = rs.getInt("approval_path_type");
				officesDTO.isDeleted = rs.getInt("isDeleted");
				officesDTO.lastModificationTime = rs.getLong("lastModificationTime");
				System.out.println("got this DTO: " + officesDTO);
				
				officesDTOList.add(officesDTO);

			}			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DBMR.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
		return officesDTOList;
	
	}
	
	

	
	
	
	//add repository
	public List<OfficesDTO> getAllOffices (boolean isFirstReload)
    {
		List<OfficesDTO> officesDTOList = new ArrayList<>();

		String sql = "SELECT * FROM offices";
		sql += " WHERE ";
	

		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by offices.lastModificationTime desc";
		printSql(sql);
		
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;

		
		try{
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();
			rs = stmt.executeQuery(sql);
			

			while(rs.next()){
				OfficesDTO officesDTO = new OfficesDTO();
				officesDTO.iD = rs.getLong("ID");
				officesDTO.officeMinistryId = rs.getInt("office_ministry_id");
				officesDTO.officeLayerId = rs.getInt("office_layer_id");
				officesDTO.customLayerId = rs.getInt("custom_layer_id");
				officesDTO.officeOriginId = rs.getInt("office_origin_id");
				officesDTO.officeNameEng = rs.getString("office_name_eng");
				officesDTO.officeNameBng = rs.getString("office_name_bng");
				officesDTO.geoDivisionId = rs.getInt("geo_division_id");
				officesDTO.geoDistrictId = rs.getInt("geo_district_id");
				officesDTO.geoUpazilaId = rs.getInt("geo_upazila_id");
				officesDTO.geoUnionId = rs.getInt("geo_union_id");
				officesDTO.geoCityCorporationId = rs.getInt("geo_city_corporation_id");
				officesDTO.geoCityCorporationWardId = rs.getInt("geo_city_corporation_ward_id");
				officesDTO.geoMunicipalityId = rs.getInt("geo_municipality_id");
				officesDTO.geoMunicipalityWardId = rs.getInt("geo_municipality_ward_id");
				officesDTO.officeCode = rs.getString("office_code");
				officesDTO.officeAddress = rs.getString("office_address");
				officesDTO.digitalNothiCode = rs.getString("digital_nothi_code");
				officesDTO.referenceCode = rs.getString("reference_code");
				officesDTO.officePhone = rs.getString("office_phone");
				officesDTO.officeMobile = rs.getString("office_mobile");
				officesDTO.officeFax = rs.getString("office_fax");
				officesDTO.officeEmail = rs.getString("office_email");
				officesDTO.officeWeb = rs.getString("office_web");
				officesDTO.parentOfficeId = rs.getInt("parent_office_id");
				officesDTO.status = rs.getBoolean("status");
				officesDTO.createdBy = rs.getInt("created_by");
				officesDTO.modifiedBy = rs.getInt("modified_by");
				officesDTO.created = rs.getString("created");
				officesDTO.modified = rs.getString("modified");
				officesDTO.geoThanaId = rs.getInt("geo_thana_id");
				officesDTO.geoPostofficeId = rs.getInt("geo_postoffice_id");
				officesDTO.geoCountryId = rs.getInt("geo_country_id");
				officesDTO.isDeleted = rs.getInt("isDeleted");
				officesDTO.lastModificationTime = rs.getLong("lastModificationTime");
				
				officesDTOList.add(officesDTO);
			}			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			try{ 
				if (connection != null){ 
					DBMR.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}

		return officesDTOList;
    }
	
	public List<OfficesDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset)
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		List<OfficesDTO> officesDTOList = new ArrayList<>();

		try{
			
			String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS);
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				OfficesDTO officesDTO = new OfficesDTO();
				officesDTO.iD = rs.getLong("ID");
				officesDTO.officeMinistryId = rs.getInt("office_ministry_id");
				officesDTO.officeLayerId = rs.getInt("office_layer_id");
				officesDTO.customLayerId = rs.getInt("custom_layer_id");
				officesDTO.officeOriginId = rs.getInt("office_origin_id");
				officesDTO.officeNameEng = rs.getString("office_name_eng");
				officesDTO.officeNameBng = rs.getString("office_name_bng");
				officesDTO.geoDivisionId = rs.getInt("geo_division_id");
				officesDTO.geoDistrictId = rs.getInt("geo_district_id");
				officesDTO.geoUpazilaId = rs.getInt("geo_upazila_id");
				officesDTO.geoUnionId = rs.getInt("geo_union_id");
				officesDTO.geoCityCorporationId = rs.getInt("geo_city_corporation_id");
				officesDTO.geoCityCorporationWardId = rs.getInt("geo_city_corporation_ward_id");
				officesDTO.geoMunicipalityId = rs.getInt("geo_municipality_id");
				officesDTO.geoMunicipalityWardId = rs.getInt("geo_municipality_ward_id");
				officesDTO.officeCode = rs.getString("office_code");
				officesDTO.officeAddress = rs.getString("office_address");
				officesDTO.digitalNothiCode = rs.getString("digital_nothi_code");
				officesDTO.referenceCode = rs.getString("reference_code");
				officesDTO.officePhone = rs.getString("office_phone");
				officesDTO.officeMobile = rs.getString("office_mobile");
				officesDTO.officeFax = rs.getString("office_fax");
				officesDTO.officeEmail = rs.getString("office_email");
				officesDTO.officeWeb = rs.getString("office_web");
				officesDTO.parentOfficeId = rs.getInt("parent_office_id");
				officesDTO.status = rs.getBoolean("status");
				officesDTO.createdBy = rs.getInt("created_by");
				officesDTO.modifiedBy = rs.getInt("modified_by");
				officesDTO.created = rs.getString("created");
				officesDTO.modified = rs.getString("modified");
				officesDTO.geoThanaId = rs.getInt("geo_thana_id");
				officesDTO.geoPostofficeId = rs.getInt("geo_postoffice_id");
				officesDTO.geoCountryId = rs.getInt("geo_country_id");
				officesDTO.approvalPathType = rs.getInt("approval_path_type");
				officesDTO.isDeleted = rs.getInt("isDeleted");
				officesDTO.lastModificationTime = rs.getLong("lastModificationTime");
				
				officesDTOList.add(officesDTO);
			}					
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DBMR.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
		return officesDTOList;
	
	}
	public Collection getIDs() 
    {
        Collection data = new ArrayList();
        Connection connection=null;
    	Statement stmt=null;
    	ResultSet resultSet = null;
    	
        String sql = getSqlWithSearchCriteria(null, -1, -1, GETIDS);
		
		printSql(sql);
		
        try
        {
	        connection = DBMR.getInstance().getConnection();
	        stmt = connection.createStatement();
	        
	        for(resultSet = stmt.executeQuery(sql); resultSet.next(); data.add(resultSet.getString("ID")));
	
	        resultSet.close();
        }
        catch (Exception e)
        {
	    	logger.fatal("DAO " + e, e);
        }
	    finally
        {
	    	try
            {
          	  if(resultSet!= null && !resultSet.isClosed())
          	  {
          		  resultSet.close();
          	  }
            }
            catch(Exception ex)
            {
          	  
            }
          try{if (stmt != null){stmt.close();}}catch (Exception e){}
          try{if (connection != null){DBMR.getInstance().freeConnection(connection);}}
          catch (Exception e){logger.fatal("DAO finally :" + e);}
        }
        return data;
    }
	
	public String getSqlWithSearchCriteria(Hashtable p_searchCriteria, int limit, int offset, int category)
    {
		String sql = "SELECT ";
		if(category == GETIDS)
		{
			sql += "distinct offices.ID as ID ";
		}
		else if(category == GETCOUNT)
		{
			sql += " count(offices.ID) as countID ";
		}
		else if(category == GETDTOS)
		{
			sql += "  offices.* ";
		}
		sql += "FROM offices ";
		
		String AnyfieldSql = "";
		String AllFieldSql = "";
		
		if(p_searchCriteria != null)
		{
						
			
			Enumeration names = p_searchCriteria.keys();
			String str, value;
			
			AnyfieldSql = "(";
			
			if(p_searchCriteria.get("AnyField")!= null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase(""))
			{
				int i = 0;
				Iterator it = OfficesMAPS.GetInstance().java_anyfield_search_map.entrySet().iterator();
				while(it.hasNext())
				{
					if( i > 0)
		        	{
						AnyfieldSql+= " OR  ";
		        	}
					Map.Entry pair = (Map.Entry)it.next();
					AnyfieldSql+= pair.getKey() + " like '%" + p_searchCriteria.get("AnyField").toString() + "%'";
					i ++;
				}						
			}
			AnyfieldSql += ")";
			System.out.println("AnyfieldSql = " + AnyfieldSql);
			
			AllFieldSql = "(";
			int i = 0;
			while(names.hasMoreElements())
			{				
				str = (String) names.nextElement();
				value = (String)p_searchCriteria.get(str);
		        System.out.println(str + ": " + value);
		        if(OfficesMAPS.GetInstance().java_allfield_type_map.get(str.toLowerCase()) != null &&  !OfficesMAPS.GetInstance().java_allfield_type_map.get(str.toLowerCase()).equalsIgnoreCase("")
		        		&& !str.equalsIgnoreCase("AnyField")
		        		&& value != null && !value.equalsIgnoreCase(""))
		        {
					if(p_searchCriteria.get(str).equals("any"))
		        	{
		        		continue;
		        	}
					
		        	if( i > 0)
		        	{
		        		AllFieldSql+= " AND  ";
		        	}
		        	if(OfficesMAPS.GetInstance().java_allfield_type_map.get(str.toLowerCase()).equals("String")) 
		        	{
		        		AllFieldSql += "offices." + str.toLowerCase() + " like '%" + p_searchCriteria.get(str) + "%'";
		        	}
		        	else
		        	{
		        		AllFieldSql += "offices." + str.toLowerCase() + " = '" + p_searchCriteria.get(str) + "'";
		        	}
		        	i ++;
		        }
			}
			
			AllFieldSql += ")";
			System.out.println("AllFieldSql = " + AllFieldSql);
			
			
		}
		
		if(!AnyfieldSql.equals("()") && !AnyfieldSql.equals(""))
		{
			sql += " join approval_path on offices.approval_path_type = approval_path.ID ";
			
		}
		sql += " WHERE ";
		sql += " offices.isDeleted = 0 ";
		
		if(!AnyfieldSql.equals("()") && !AnyfieldSql.equals(""))
		{
			sql += " AND " + AnyfieldSql;
			
		}
		if(!AllFieldSql.equals("()") && !AllFieldSql.equals(""))
		{			
			sql += " AND " + AllFieldSql;
		}

			
		sql += " order by offices.lastModificationTime desc ";

		printSql(sql);
		
		if(limit >= 0)
		{
			sql += " limit " + limit;
		}
		if(offset >= 0)
		{
			sql += " offset " + offset;
		}
		
		//System.out.println("-------------- sql = " + sql);
		
		return sql;
    }			
}
	