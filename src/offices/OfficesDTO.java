package offices;
import java.util.*; 


public class OfficesDTO {

	public long iD = 0;
	public int officeMinistryId = 0;
	public int officeLayerId = 0;
	public int customLayerId = 0;
	public int officeOriginId = 0;
    public String officeNameEng = "";
    public String officeNameBng = "";
	public int geoDivisionId = 0;
	public int geoDistrictId = 0;
	public int geoUpazilaId = 0;
	public int geoUnionId = 0;
	public int geoCityCorporationId = 0;
	public int geoCityCorporationWardId = 0;
	public int geoMunicipalityId = 0;
	public int geoMunicipalityWardId = 0;
    public String officeCode = "";
    public String officeAddress = "";
    public String digitalNothiCode = "";
    public String referenceCode = "";
    public String officePhone = "";
    public String officeMobile = "";
    public String officeFax = "";
    public String officeEmail = "";
    public String officeWeb = "";
	public int parentOfficeId = 0;
	public boolean status = false;
	public int createdBy = 0;
	public int modifiedBy = 0;
    public String created = "";
    public String modified = "";
	public int geoThanaId = 0;
	public int geoPostofficeId = 0;
	public int geoCountryId = 0;
	public int approvalPathType = 0;
	public int isDeleted = 0;
	public long lastModificationTime = 0;
	
	
    @Override
	public String toString() {
            return "$OfficesDTO[" +
            " iD = " + iD +
            " officeMinistryId = " + officeMinistryId +
            " officeLayerId = " + officeLayerId +
            " customLayerId = " + customLayerId +
            " officeOriginId = " + officeOriginId +
            " officeNameEng = " + officeNameEng +
            " officeNameBng = " + officeNameBng +
            " geoDivisionId = " + geoDivisionId +
            " geoDistrictId = " + geoDistrictId +
            " geoUpazilaId = " + geoUpazilaId +
            " geoUnionId = " + geoUnionId +
            " geoCityCorporationId = " + geoCityCorporationId +
            " geoCityCorporationWardId = " + geoCityCorporationWardId +
            " geoMunicipalityId = " + geoMunicipalityId +
            " geoMunicipalityWardId = " + geoMunicipalityWardId +
            " officeCode = " + officeCode +
            " officeAddress = " + officeAddress +
            " digitalNothiCode = " + digitalNothiCode +
            " referenceCode = " + referenceCode +
            " officePhone = " + officePhone +
            " officeMobile = " + officeMobile +
            " officeFax = " + officeFax +
            " officeEmail = " + officeEmail +
            " officeWeb = " + officeWeb +
            " parentOfficeId = " + parentOfficeId +
            " status = " + status +
            " createdBy = " + createdBy +
            " modifiedBy = " + modifiedBy +
            " created = " + created +
            " modified = " + modified +
            " geoThanaId = " + geoThanaId +
            " geoPostofficeId = " + geoPostofficeId +
            " geoCountryId = " + geoCountryId +
            " approvalPathType = " + approvalPathType +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}