package offices;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class OfficesRepository implements Repository {
	OfficesDAO officesDAO = new OfficesDAO();
	
	
	private static final Logger logger = Logger.getLogger(OfficesRepository.class);
	Map<Long, OfficesDTO>mapOfOfficesDTOToiD;
	Map<Integer, Set<OfficesDTO> >mapOfOfficesDTOToofficeMinistryId;
	Map<Integer, Set<OfficesDTO> >mapOfOfficesDTOToofficeLayerId;
	Map<Integer, Set<OfficesDTO> >mapOfOfficesDTOTocustomLayerId;
	Map<Integer, Set<OfficesDTO> >mapOfOfficesDTOToofficeOriginId;
	Map<String, Set<OfficesDTO> >mapOfOfficesDTOToofficeNameEng;
	Map<String, Set<OfficesDTO> >mapOfOfficesDTOToofficeNameBng;
	Map<Integer, Set<OfficesDTO> >mapOfOfficesDTOTogeoDivisionId;
	Map<Integer, Set<OfficesDTO> >mapOfOfficesDTOTogeoDistrictId;
	Map<Integer, Set<OfficesDTO> >mapOfOfficesDTOTogeoUpazilaId;
	Map<Integer, Set<OfficesDTO> >mapOfOfficesDTOTogeoUnionId;
	Map<Integer, Set<OfficesDTO> >mapOfOfficesDTOTogeoCityCorporationId;
	Map<Integer, Set<OfficesDTO> >mapOfOfficesDTOTogeoCityCorporationWardId;
	Map<Integer, Set<OfficesDTO> >mapOfOfficesDTOTogeoMunicipalityId;
	Map<Integer, Set<OfficesDTO> >mapOfOfficesDTOTogeoMunicipalityWardId;
	Map<String, Set<OfficesDTO> >mapOfOfficesDTOToofficeCode;
	Map<String, Set<OfficesDTO> >mapOfOfficesDTOToofficeAddress;
	Map<String, Set<OfficesDTO> >mapOfOfficesDTOTodigitalNothiCode;
	Map<String, Set<OfficesDTO> >mapOfOfficesDTOToreferenceCode;
	Map<String, Set<OfficesDTO> >mapOfOfficesDTOToofficePhone;
	Map<String, Set<OfficesDTO> >mapOfOfficesDTOToofficeMobile;
	Map<String, Set<OfficesDTO> >mapOfOfficesDTOToofficeFax;
	Map<String, Set<OfficesDTO> >mapOfOfficesDTOToofficeEmail;
	Map<String, Set<OfficesDTO> >mapOfOfficesDTOToofficeWeb;
	Map<Integer, Set<OfficesDTO> >mapOfOfficesDTOToparentOfficeId;
	Map<Boolean, Set<OfficesDTO> >mapOfOfficesDTOTostatus;
	Map<Integer, Set<OfficesDTO> >mapOfOfficesDTOTocreatedBy;
	Map<Integer, Set<OfficesDTO> >mapOfOfficesDTOTomodifiedBy;
	Map<String, Set<OfficesDTO> >mapOfOfficesDTOTocreated;
	Map<String, Set<OfficesDTO> >mapOfOfficesDTOTomodified;
	Map<Integer, Set<OfficesDTO> >mapOfOfficesDTOTogeoThanaId;
	Map<Integer, Set<OfficesDTO> >mapOfOfficesDTOTogeoPostofficeId;
	Map<Integer, Set<OfficesDTO> >mapOfOfficesDTOTogeoCountryId;
	Map<Integer, Set<OfficesDTO> >mapOfOfficesDTOToapprovalPathType;
	Map<Long, Set<OfficesDTO> >mapOfOfficesDTOTolastModificationTime;


	static OfficesRepository instance = null;  
	private OfficesRepository(){
		mapOfOfficesDTOToiD = new ConcurrentHashMap<>();
		mapOfOfficesDTOToofficeMinistryId = new ConcurrentHashMap<>();
		mapOfOfficesDTOToofficeLayerId = new ConcurrentHashMap<>();
		mapOfOfficesDTOTocustomLayerId = new ConcurrentHashMap<>();
		mapOfOfficesDTOToofficeOriginId = new ConcurrentHashMap<>();
		mapOfOfficesDTOToofficeNameEng = new ConcurrentHashMap<>();
		mapOfOfficesDTOToofficeNameBng = new ConcurrentHashMap<>();
		mapOfOfficesDTOTogeoDivisionId = new ConcurrentHashMap<>();
		mapOfOfficesDTOTogeoDistrictId = new ConcurrentHashMap<>();
		mapOfOfficesDTOTogeoUpazilaId = new ConcurrentHashMap<>();
		mapOfOfficesDTOTogeoUnionId = new ConcurrentHashMap<>();
		mapOfOfficesDTOTogeoCityCorporationId = new ConcurrentHashMap<>();
		mapOfOfficesDTOTogeoCityCorporationWardId = new ConcurrentHashMap<>();
		mapOfOfficesDTOTogeoMunicipalityId = new ConcurrentHashMap<>();
		mapOfOfficesDTOTogeoMunicipalityWardId = new ConcurrentHashMap<>();
		mapOfOfficesDTOToofficeCode = new ConcurrentHashMap<>();
		mapOfOfficesDTOToofficeAddress = new ConcurrentHashMap<>();
		mapOfOfficesDTOTodigitalNothiCode = new ConcurrentHashMap<>();
		mapOfOfficesDTOToreferenceCode = new ConcurrentHashMap<>();
		mapOfOfficesDTOToofficePhone = new ConcurrentHashMap<>();
		mapOfOfficesDTOToofficeMobile = new ConcurrentHashMap<>();
		mapOfOfficesDTOToofficeFax = new ConcurrentHashMap<>();
		mapOfOfficesDTOToofficeEmail = new ConcurrentHashMap<>();
		mapOfOfficesDTOToofficeWeb = new ConcurrentHashMap<>();
		mapOfOfficesDTOToparentOfficeId = new ConcurrentHashMap<>();
		mapOfOfficesDTOTostatus = new ConcurrentHashMap<>();
		mapOfOfficesDTOTocreatedBy = new ConcurrentHashMap<>();
		mapOfOfficesDTOTomodifiedBy = new ConcurrentHashMap<>();
		mapOfOfficesDTOTocreated = new ConcurrentHashMap<>();
		mapOfOfficesDTOTomodified = new ConcurrentHashMap<>();
		mapOfOfficesDTOTogeoThanaId = new ConcurrentHashMap<>();
		mapOfOfficesDTOTogeoPostofficeId = new ConcurrentHashMap<>();
		mapOfOfficesDTOTogeoCountryId = new ConcurrentHashMap<>();
		mapOfOfficesDTOToapprovalPathType = new ConcurrentHashMap<>();
		mapOfOfficesDTOTolastModificationTime = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static OfficesRepository getInstance(){
		return LazyLoader.INSTANCE;
	}

	private static class LazyLoader{
		static OfficesRepository INSTANCE = new OfficesRepository();
	}

	public void reload(boolean reloadAll){
		logger.debug("OfficesRepository loading start for reloadAll: "+reloadAll);
		try {
			List<OfficesDTO> officesDTOs = officesDAO.getAllOffices(reloadAll);
			for(OfficesDTO officesDTO : officesDTOs) {
				OfficesDTO oldOfficesDTO = getOfficesDTOByID(officesDTO.iD);
				if( oldOfficesDTO != null ) {
					mapOfOfficesDTOToiD.remove(oldOfficesDTO.iD);
				
					if(mapOfOfficesDTOToofficeMinistryId.containsKey(oldOfficesDTO.officeMinistryId)) {
						mapOfOfficesDTOToofficeMinistryId.get(oldOfficesDTO.officeMinistryId).remove(oldOfficesDTO);
					}
					if(mapOfOfficesDTOToofficeMinistryId.get(oldOfficesDTO.officeMinistryId).isEmpty()) {
						mapOfOfficesDTOToofficeMinistryId.remove(oldOfficesDTO.officeMinistryId);
					}
					
					if(mapOfOfficesDTOToofficeLayerId.containsKey(oldOfficesDTO.officeLayerId)) {
						mapOfOfficesDTOToofficeLayerId.get(oldOfficesDTO.officeLayerId).remove(oldOfficesDTO);
					}
					if(mapOfOfficesDTOToofficeLayerId.get(oldOfficesDTO.officeLayerId).isEmpty()) {
						mapOfOfficesDTOToofficeLayerId.remove(oldOfficesDTO.officeLayerId);
					}
					
					if(mapOfOfficesDTOTocustomLayerId.containsKey(oldOfficesDTO.customLayerId)) {
						mapOfOfficesDTOTocustomLayerId.get(oldOfficesDTO.customLayerId).remove(oldOfficesDTO);
					}
					if(mapOfOfficesDTOTocustomLayerId.get(oldOfficesDTO.customLayerId).isEmpty()) {
						mapOfOfficesDTOTocustomLayerId.remove(oldOfficesDTO.customLayerId);
					}
					
					if(mapOfOfficesDTOToofficeOriginId.containsKey(oldOfficesDTO.officeOriginId)) {
						mapOfOfficesDTOToofficeOriginId.get(oldOfficesDTO.officeOriginId).remove(oldOfficesDTO);
					}
					if(mapOfOfficesDTOToofficeOriginId.get(oldOfficesDTO.officeOriginId).isEmpty()) {
						mapOfOfficesDTOToofficeOriginId.remove(oldOfficesDTO.officeOriginId);
					}
					
					if(mapOfOfficesDTOToofficeNameEng.containsKey(oldOfficesDTO.officeNameEng)) {
						mapOfOfficesDTOToofficeNameEng.get(oldOfficesDTO.officeNameEng).remove(oldOfficesDTO);
					}
					if(mapOfOfficesDTOToofficeNameEng.get(oldOfficesDTO.officeNameEng).isEmpty()) {
						mapOfOfficesDTOToofficeNameEng.remove(oldOfficesDTO.officeNameEng);
					}
					
					if(mapOfOfficesDTOToofficeNameBng.containsKey(oldOfficesDTO.officeNameBng)) {
						mapOfOfficesDTOToofficeNameBng.get(oldOfficesDTO.officeNameBng).remove(oldOfficesDTO);
					}
					if(mapOfOfficesDTOToofficeNameBng.get(oldOfficesDTO.officeNameBng).isEmpty()) {
						mapOfOfficesDTOToofficeNameBng.remove(oldOfficesDTO.officeNameBng);
					}
					
					if(mapOfOfficesDTOTogeoDivisionId.containsKey(oldOfficesDTO.geoDivisionId)) {
						mapOfOfficesDTOTogeoDivisionId.get(oldOfficesDTO.geoDivisionId).remove(oldOfficesDTO);
					}
					if(mapOfOfficesDTOTogeoDivisionId.get(oldOfficesDTO.geoDivisionId).isEmpty()) {
						mapOfOfficesDTOTogeoDivisionId.remove(oldOfficesDTO.geoDivisionId);
					}
					
					if(mapOfOfficesDTOTogeoDistrictId.containsKey(oldOfficesDTO.geoDistrictId)) {
						mapOfOfficesDTOTogeoDistrictId.get(oldOfficesDTO.geoDistrictId).remove(oldOfficesDTO);
					}
					if(mapOfOfficesDTOTogeoDistrictId.get(oldOfficesDTO.geoDistrictId).isEmpty()) {
						mapOfOfficesDTOTogeoDistrictId.remove(oldOfficesDTO.geoDistrictId);
					}
					
					if(mapOfOfficesDTOTogeoUpazilaId.containsKey(oldOfficesDTO.geoUpazilaId)) {
						mapOfOfficesDTOTogeoUpazilaId.get(oldOfficesDTO.geoUpazilaId).remove(oldOfficesDTO);
					}
					if(mapOfOfficesDTOTogeoUpazilaId.get(oldOfficesDTO.geoUpazilaId).isEmpty()) {
						mapOfOfficesDTOTogeoUpazilaId.remove(oldOfficesDTO.geoUpazilaId);
					}
					
					if(mapOfOfficesDTOTogeoUnionId.containsKey(oldOfficesDTO.geoUnionId)) {
						mapOfOfficesDTOTogeoUnionId.get(oldOfficesDTO.geoUnionId).remove(oldOfficesDTO);
					}
					if(mapOfOfficesDTOTogeoUnionId.get(oldOfficesDTO.geoUnionId).isEmpty()) {
						mapOfOfficesDTOTogeoUnionId.remove(oldOfficesDTO.geoUnionId);
					}
					
					if(mapOfOfficesDTOTogeoCityCorporationId.containsKey(oldOfficesDTO.geoCityCorporationId)) {
						mapOfOfficesDTOTogeoCityCorporationId.get(oldOfficesDTO.geoCityCorporationId).remove(oldOfficesDTO);
					}
					if(mapOfOfficesDTOTogeoCityCorporationId.get(oldOfficesDTO.geoCityCorporationId).isEmpty()) {
						mapOfOfficesDTOTogeoCityCorporationId.remove(oldOfficesDTO.geoCityCorporationId);
					}
					
					if(mapOfOfficesDTOTogeoCityCorporationWardId.containsKey(oldOfficesDTO.geoCityCorporationWardId)) {
						mapOfOfficesDTOTogeoCityCorporationWardId.get(oldOfficesDTO.geoCityCorporationWardId).remove(oldOfficesDTO);
					}
					if(mapOfOfficesDTOTogeoCityCorporationWardId.get(oldOfficesDTO.geoCityCorporationWardId).isEmpty()) {
						mapOfOfficesDTOTogeoCityCorporationWardId.remove(oldOfficesDTO.geoCityCorporationWardId);
					}
					
					if(mapOfOfficesDTOTogeoMunicipalityId.containsKey(oldOfficesDTO.geoMunicipalityId)) {
						mapOfOfficesDTOTogeoMunicipalityId.get(oldOfficesDTO.geoMunicipalityId).remove(oldOfficesDTO);
					}
					if(mapOfOfficesDTOTogeoMunicipalityId.get(oldOfficesDTO.geoMunicipalityId).isEmpty()) {
						mapOfOfficesDTOTogeoMunicipalityId.remove(oldOfficesDTO.geoMunicipalityId);
					}
					
					if(mapOfOfficesDTOTogeoMunicipalityWardId.containsKey(oldOfficesDTO.geoMunicipalityWardId)) {
						mapOfOfficesDTOTogeoMunicipalityWardId.get(oldOfficesDTO.geoMunicipalityWardId).remove(oldOfficesDTO);
					}
					if(mapOfOfficesDTOTogeoMunicipalityWardId.get(oldOfficesDTO.geoMunicipalityWardId).isEmpty()) {
						mapOfOfficesDTOTogeoMunicipalityWardId.remove(oldOfficesDTO.geoMunicipalityWardId);
					}
					
					if(mapOfOfficesDTOToofficeCode.containsKey(oldOfficesDTO.officeCode)) {
						mapOfOfficesDTOToofficeCode.get(oldOfficesDTO.officeCode).remove(oldOfficesDTO);
					}
					if(mapOfOfficesDTOToofficeCode.get(oldOfficesDTO.officeCode).isEmpty()) {
						mapOfOfficesDTOToofficeCode.remove(oldOfficesDTO.officeCode);
					}
					
					if(mapOfOfficesDTOToofficeAddress.containsKey(oldOfficesDTO.officeAddress)) {
						mapOfOfficesDTOToofficeAddress.get(oldOfficesDTO.officeAddress).remove(oldOfficesDTO);
					}
					if(mapOfOfficesDTOToofficeAddress.get(oldOfficesDTO.officeAddress).isEmpty()) {
						mapOfOfficesDTOToofficeAddress.remove(oldOfficesDTO.officeAddress);
					}
					
					if(mapOfOfficesDTOTodigitalNothiCode.containsKey(oldOfficesDTO.digitalNothiCode)) {
						mapOfOfficesDTOTodigitalNothiCode.get(oldOfficesDTO.digitalNothiCode).remove(oldOfficesDTO);
					}
					if(mapOfOfficesDTOTodigitalNothiCode.get(oldOfficesDTO.digitalNothiCode).isEmpty()) {
						mapOfOfficesDTOTodigitalNothiCode.remove(oldOfficesDTO.digitalNothiCode);
					}
					
					if(mapOfOfficesDTOToreferenceCode.containsKey(oldOfficesDTO.referenceCode)) {
						mapOfOfficesDTOToreferenceCode.get(oldOfficesDTO.referenceCode).remove(oldOfficesDTO);
					}
					if(mapOfOfficesDTOToreferenceCode.get(oldOfficesDTO.referenceCode).isEmpty()) {
						mapOfOfficesDTOToreferenceCode.remove(oldOfficesDTO.referenceCode);
					}
					
					if(mapOfOfficesDTOToofficePhone.containsKey(oldOfficesDTO.officePhone)) {
						mapOfOfficesDTOToofficePhone.get(oldOfficesDTO.officePhone).remove(oldOfficesDTO);
					}
					if(mapOfOfficesDTOToofficePhone.get(oldOfficesDTO.officePhone).isEmpty()) {
						mapOfOfficesDTOToofficePhone.remove(oldOfficesDTO.officePhone);
					}
					
					if(mapOfOfficesDTOToofficeMobile.containsKey(oldOfficesDTO.officeMobile)) {
						mapOfOfficesDTOToofficeMobile.get(oldOfficesDTO.officeMobile).remove(oldOfficesDTO);
					}
					if(mapOfOfficesDTOToofficeMobile.get(oldOfficesDTO.officeMobile).isEmpty()) {
						mapOfOfficesDTOToofficeMobile.remove(oldOfficesDTO.officeMobile);
					}
					
					if(mapOfOfficesDTOToofficeFax.containsKey(oldOfficesDTO.officeFax)) {
						mapOfOfficesDTOToofficeFax.get(oldOfficesDTO.officeFax).remove(oldOfficesDTO);
					}
					if(mapOfOfficesDTOToofficeFax.get(oldOfficesDTO.officeFax).isEmpty()) {
						mapOfOfficesDTOToofficeFax.remove(oldOfficesDTO.officeFax);
					}
					
					if(mapOfOfficesDTOToofficeEmail.containsKey(oldOfficesDTO.officeEmail)) {
						mapOfOfficesDTOToofficeEmail.get(oldOfficesDTO.officeEmail).remove(oldOfficesDTO);
					}
					if(mapOfOfficesDTOToofficeEmail.get(oldOfficesDTO.officeEmail).isEmpty()) {
						mapOfOfficesDTOToofficeEmail.remove(oldOfficesDTO.officeEmail);
					}
					
					if(mapOfOfficesDTOToofficeWeb.containsKey(oldOfficesDTO.officeWeb)) {
						mapOfOfficesDTOToofficeWeb.get(oldOfficesDTO.officeWeb).remove(oldOfficesDTO);
					}
					if(mapOfOfficesDTOToofficeWeb.get(oldOfficesDTO.officeWeb).isEmpty()) {
						mapOfOfficesDTOToofficeWeb.remove(oldOfficesDTO.officeWeb);
					}
					
					if(mapOfOfficesDTOToparentOfficeId.containsKey(oldOfficesDTO.parentOfficeId)) {
						mapOfOfficesDTOToparentOfficeId.get(oldOfficesDTO.parentOfficeId).remove(oldOfficesDTO);
					}
					if(mapOfOfficesDTOToparentOfficeId.get(oldOfficesDTO.parentOfficeId).isEmpty()) {
						mapOfOfficesDTOToparentOfficeId.remove(oldOfficesDTO.parentOfficeId);
					}
					
					if(mapOfOfficesDTOTostatus.containsKey(oldOfficesDTO.status)) {
						mapOfOfficesDTOTostatus.get(oldOfficesDTO.status).remove(oldOfficesDTO);
					}
					if(mapOfOfficesDTOTostatus.get(oldOfficesDTO.status).isEmpty()) {
						mapOfOfficesDTOTostatus.remove(oldOfficesDTO.status);
					}
					
					if(mapOfOfficesDTOTocreatedBy.containsKey(oldOfficesDTO.createdBy)) {
						mapOfOfficesDTOTocreatedBy.get(oldOfficesDTO.createdBy).remove(oldOfficesDTO);
					}
					if(mapOfOfficesDTOTocreatedBy.get(oldOfficesDTO.createdBy).isEmpty()) {
						mapOfOfficesDTOTocreatedBy.remove(oldOfficesDTO.createdBy);
					}
					
					if(mapOfOfficesDTOTomodifiedBy.containsKey(oldOfficesDTO.modifiedBy)) {
						mapOfOfficesDTOTomodifiedBy.get(oldOfficesDTO.modifiedBy).remove(oldOfficesDTO);
					}
					if(mapOfOfficesDTOTomodifiedBy.get(oldOfficesDTO.modifiedBy).isEmpty()) {
						mapOfOfficesDTOTomodifiedBy.remove(oldOfficesDTO.modifiedBy);
					}
					
					if(mapOfOfficesDTOTocreated.containsKey(oldOfficesDTO.created)) {
						mapOfOfficesDTOTocreated.get(oldOfficesDTO.created).remove(oldOfficesDTO);
					}
					if(mapOfOfficesDTOTocreated.get(oldOfficesDTO.created).isEmpty()) {
						mapOfOfficesDTOTocreated.remove(oldOfficesDTO.created);
					}
					
					if(mapOfOfficesDTOTomodified.containsKey(oldOfficesDTO.modified)) {
						mapOfOfficesDTOTomodified.get(oldOfficesDTO.modified).remove(oldOfficesDTO);
					}
					if(mapOfOfficesDTOTomodified.get(oldOfficesDTO.modified).isEmpty()) {
						mapOfOfficesDTOTomodified.remove(oldOfficesDTO.modified);
					}
					
					if(mapOfOfficesDTOTogeoThanaId.containsKey(oldOfficesDTO.geoThanaId)) {
						mapOfOfficesDTOTogeoThanaId.get(oldOfficesDTO.geoThanaId).remove(oldOfficesDTO);
					}
					if(mapOfOfficesDTOTogeoThanaId.get(oldOfficesDTO.geoThanaId).isEmpty()) {
						mapOfOfficesDTOTogeoThanaId.remove(oldOfficesDTO.geoThanaId);
					}
					
					if(mapOfOfficesDTOTogeoPostofficeId.containsKey(oldOfficesDTO.geoPostofficeId)) {
						mapOfOfficesDTOTogeoPostofficeId.get(oldOfficesDTO.geoPostofficeId).remove(oldOfficesDTO);
					}
					if(mapOfOfficesDTOTogeoPostofficeId.get(oldOfficesDTO.geoPostofficeId).isEmpty()) {
						mapOfOfficesDTOTogeoPostofficeId.remove(oldOfficesDTO.geoPostofficeId);
					}
					
					if(mapOfOfficesDTOTogeoCountryId.containsKey(oldOfficesDTO.geoCountryId)) {
						mapOfOfficesDTOTogeoCountryId.get(oldOfficesDTO.geoCountryId).remove(oldOfficesDTO);
					}
					if(mapOfOfficesDTOTogeoCountryId.get(oldOfficesDTO.geoCountryId).isEmpty()) {
						mapOfOfficesDTOTogeoCountryId.remove(oldOfficesDTO.geoCountryId);
					}
					
					if(mapOfOfficesDTOToapprovalPathType.containsKey(oldOfficesDTO.approvalPathType)) {
						mapOfOfficesDTOToapprovalPathType.get(oldOfficesDTO.approvalPathType).remove(oldOfficesDTO);
					}
					if(mapOfOfficesDTOToapprovalPathType.get(oldOfficesDTO.approvalPathType).isEmpty()) {
						mapOfOfficesDTOToapprovalPathType.remove(oldOfficesDTO.approvalPathType);
					}
					
					if(mapOfOfficesDTOTolastModificationTime.containsKey(oldOfficesDTO.lastModificationTime)) {
						mapOfOfficesDTOTolastModificationTime.get(oldOfficesDTO.lastModificationTime).remove(oldOfficesDTO);
					}
					if(mapOfOfficesDTOTolastModificationTime.get(oldOfficesDTO.lastModificationTime).isEmpty()) {
						mapOfOfficesDTOTolastModificationTime.remove(oldOfficesDTO.lastModificationTime);
					}
					
					
				}
				if(officesDTO.isDeleted == 0) 
				{
					
					mapOfOfficesDTOToiD.put(officesDTO.iD, officesDTO);
				
					if( ! mapOfOfficesDTOToofficeMinistryId.containsKey(officesDTO.officeMinistryId)) {
						mapOfOfficesDTOToofficeMinistryId.put(officesDTO.officeMinistryId, new HashSet<>());
					}
					mapOfOfficesDTOToofficeMinistryId.get(officesDTO.officeMinistryId).add(officesDTO);
					
					if( ! mapOfOfficesDTOToofficeLayerId.containsKey(officesDTO.officeLayerId)) {
						mapOfOfficesDTOToofficeLayerId.put(officesDTO.officeLayerId, new HashSet<>());
					}
					mapOfOfficesDTOToofficeLayerId.get(officesDTO.officeLayerId).add(officesDTO);
					
					if( ! mapOfOfficesDTOTocustomLayerId.containsKey(officesDTO.customLayerId)) {
						mapOfOfficesDTOTocustomLayerId.put(officesDTO.customLayerId, new HashSet<>());
					}
					mapOfOfficesDTOTocustomLayerId.get(officesDTO.customLayerId).add(officesDTO);
					
					if( ! mapOfOfficesDTOToofficeOriginId.containsKey(officesDTO.officeOriginId)) {
						mapOfOfficesDTOToofficeOriginId.put(officesDTO.officeOriginId, new HashSet<>());
					}
					mapOfOfficesDTOToofficeOriginId.get(officesDTO.officeOriginId).add(officesDTO);
					
					if( ! mapOfOfficesDTOToofficeNameEng.containsKey(officesDTO.officeNameEng)) {
						mapOfOfficesDTOToofficeNameEng.put(officesDTO.officeNameEng, new HashSet<>());
					}
					mapOfOfficesDTOToofficeNameEng.get(officesDTO.officeNameEng).add(officesDTO);
					
					if( ! mapOfOfficesDTOToofficeNameBng.containsKey(officesDTO.officeNameBng)) {
						mapOfOfficesDTOToofficeNameBng.put(officesDTO.officeNameBng, new HashSet<>());
					}
					mapOfOfficesDTOToofficeNameBng.get(officesDTO.officeNameBng).add(officesDTO);
					
					if( ! mapOfOfficesDTOTogeoDivisionId.containsKey(officesDTO.geoDivisionId)) {
						mapOfOfficesDTOTogeoDivisionId.put(officesDTO.geoDivisionId, new HashSet<>());
					}
					mapOfOfficesDTOTogeoDivisionId.get(officesDTO.geoDivisionId).add(officesDTO);
					
					if( ! mapOfOfficesDTOTogeoDistrictId.containsKey(officesDTO.geoDistrictId)) {
						mapOfOfficesDTOTogeoDistrictId.put(officesDTO.geoDistrictId, new HashSet<>());
					}
					mapOfOfficesDTOTogeoDistrictId.get(officesDTO.geoDistrictId).add(officesDTO);
					
					if( ! mapOfOfficesDTOTogeoUpazilaId.containsKey(officesDTO.geoUpazilaId)) {
						mapOfOfficesDTOTogeoUpazilaId.put(officesDTO.geoUpazilaId, new HashSet<>());
					}
					mapOfOfficesDTOTogeoUpazilaId.get(officesDTO.geoUpazilaId).add(officesDTO);
					
					if( ! mapOfOfficesDTOTogeoUnionId.containsKey(officesDTO.geoUnionId)) {
						mapOfOfficesDTOTogeoUnionId.put(officesDTO.geoUnionId, new HashSet<>());
					}
					mapOfOfficesDTOTogeoUnionId.get(officesDTO.geoUnionId).add(officesDTO);
					
					if( ! mapOfOfficesDTOTogeoCityCorporationId.containsKey(officesDTO.geoCityCorporationId)) {
						mapOfOfficesDTOTogeoCityCorporationId.put(officesDTO.geoCityCorporationId, new HashSet<>());
					}
					mapOfOfficesDTOTogeoCityCorporationId.get(officesDTO.geoCityCorporationId).add(officesDTO);
					
					if( ! mapOfOfficesDTOTogeoCityCorporationWardId.containsKey(officesDTO.geoCityCorporationWardId)) {
						mapOfOfficesDTOTogeoCityCorporationWardId.put(officesDTO.geoCityCorporationWardId, new HashSet<>());
					}
					mapOfOfficesDTOTogeoCityCorporationWardId.get(officesDTO.geoCityCorporationWardId).add(officesDTO);
					
					if( ! mapOfOfficesDTOTogeoMunicipalityId.containsKey(officesDTO.geoMunicipalityId)) {
						mapOfOfficesDTOTogeoMunicipalityId.put(officesDTO.geoMunicipalityId, new HashSet<>());
					}
					mapOfOfficesDTOTogeoMunicipalityId.get(officesDTO.geoMunicipalityId).add(officesDTO);
					
					if( ! mapOfOfficesDTOTogeoMunicipalityWardId.containsKey(officesDTO.geoMunicipalityWardId)) {
						mapOfOfficesDTOTogeoMunicipalityWardId.put(officesDTO.geoMunicipalityWardId, new HashSet<>());
					}
					mapOfOfficesDTOTogeoMunicipalityWardId.get(officesDTO.geoMunicipalityWardId).add(officesDTO);
					
					if( ! mapOfOfficesDTOToofficeCode.containsKey(officesDTO.officeCode)) {
						mapOfOfficesDTOToofficeCode.put(officesDTO.officeCode, new HashSet<>());
					}
					mapOfOfficesDTOToofficeCode.get(officesDTO.officeCode).add(officesDTO);
					
					if( ! mapOfOfficesDTOToofficeAddress.containsKey(officesDTO.officeAddress)) {
						mapOfOfficesDTOToofficeAddress.put(officesDTO.officeAddress, new HashSet<>());
					}
					mapOfOfficesDTOToofficeAddress.get(officesDTO.officeAddress).add(officesDTO);
					
					if( ! mapOfOfficesDTOTodigitalNothiCode.containsKey(officesDTO.digitalNothiCode)) {
						mapOfOfficesDTOTodigitalNothiCode.put(officesDTO.digitalNothiCode, new HashSet<>());
					}
					mapOfOfficesDTOTodigitalNothiCode.get(officesDTO.digitalNothiCode).add(officesDTO);

					if(officesDTO.referenceCode!=null){
						if( ! mapOfOfficesDTOToreferenceCode.containsKey(officesDTO.referenceCode)) {
							mapOfOfficesDTOToreferenceCode.put(officesDTO.referenceCode, new HashSet<>());
						}
						mapOfOfficesDTOToreferenceCode.get(officesDTO.referenceCode).add(officesDTO);
					}

					if( ! mapOfOfficesDTOToofficePhone.containsKey(officesDTO.officePhone)) {
						mapOfOfficesDTOToofficePhone.put(officesDTO.officePhone, new HashSet<>());
					}
					mapOfOfficesDTOToofficePhone.get(officesDTO.officePhone).add(officesDTO);
					
					if( ! mapOfOfficesDTOToofficeMobile.containsKey(officesDTO.officeMobile)) {
						mapOfOfficesDTOToofficeMobile.put(officesDTO.officeMobile, new HashSet<>());
					}
					mapOfOfficesDTOToofficeMobile.get(officesDTO.officeMobile).add(officesDTO);
					
					if( ! mapOfOfficesDTOToofficeFax.containsKey(officesDTO.officeFax)) {
						mapOfOfficesDTOToofficeFax.put(officesDTO.officeFax, new HashSet<>());
					}
					mapOfOfficesDTOToofficeFax.get(officesDTO.officeFax).add(officesDTO);
					
					if( ! mapOfOfficesDTOToofficeEmail.containsKey(officesDTO.officeEmail)) {
						mapOfOfficesDTOToofficeEmail.put(officesDTO.officeEmail, new HashSet<>());
					}
					mapOfOfficesDTOToofficeEmail.get(officesDTO.officeEmail).add(officesDTO);
					
					if( ! mapOfOfficesDTOToofficeWeb.containsKey(officesDTO.officeWeb)) {
						mapOfOfficesDTOToofficeWeb.put(officesDTO.officeWeb, new HashSet<>());
					}
					mapOfOfficesDTOToofficeWeb.get(officesDTO.officeWeb).add(officesDTO);
					
					if( ! mapOfOfficesDTOToparentOfficeId.containsKey(officesDTO.parentOfficeId)) {
						mapOfOfficesDTOToparentOfficeId.put(officesDTO.parentOfficeId, new HashSet<>());
					}
					mapOfOfficesDTOToparentOfficeId.get(officesDTO.parentOfficeId).add(officesDTO);
					
					if( ! mapOfOfficesDTOTostatus.containsKey(officesDTO.status)) {
						mapOfOfficesDTOTostatus.put(officesDTO.status, new HashSet<>());
					}
					mapOfOfficesDTOTostatus.get(officesDTO.status).add(officesDTO);
					
					if( ! mapOfOfficesDTOTocreatedBy.containsKey(officesDTO.createdBy)) {
						mapOfOfficesDTOTocreatedBy.put(officesDTO.createdBy, new HashSet<>());
					}
					mapOfOfficesDTOTocreatedBy.get(officesDTO.createdBy).add(officesDTO);
					
					if( ! mapOfOfficesDTOTomodifiedBy.containsKey(officesDTO.modifiedBy)) {
						mapOfOfficesDTOTomodifiedBy.put(officesDTO.modifiedBy, new HashSet<>());
					}
					mapOfOfficesDTOTomodifiedBy.get(officesDTO.modifiedBy).add(officesDTO);
					
					if( ! mapOfOfficesDTOTocreated.containsKey(officesDTO.created)) {
						mapOfOfficesDTOTocreated.put(officesDTO.created, new HashSet<>());
					}
					mapOfOfficesDTOTocreated.get(officesDTO.created).add(officesDTO);
					
					if( ! mapOfOfficesDTOTomodified.containsKey(officesDTO.modified)) {
						mapOfOfficesDTOTomodified.put(officesDTO.modified, new HashSet<>());
					}
					mapOfOfficesDTOTomodified.get(officesDTO.modified).add(officesDTO);
					
					if( ! mapOfOfficesDTOTogeoThanaId.containsKey(officesDTO.geoThanaId)) {
						mapOfOfficesDTOTogeoThanaId.put(officesDTO.geoThanaId, new HashSet<>());
					}
					mapOfOfficesDTOTogeoThanaId.get(officesDTO.geoThanaId).add(officesDTO);
					
					if( ! mapOfOfficesDTOTogeoPostofficeId.containsKey(officesDTO.geoPostofficeId)) {
						mapOfOfficesDTOTogeoPostofficeId.put(officesDTO.geoPostofficeId, new HashSet<>());
					}
					mapOfOfficesDTOTogeoPostofficeId.get(officesDTO.geoPostofficeId).add(officesDTO);
					
					if( ! mapOfOfficesDTOTogeoCountryId.containsKey(officesDTO.geoCountryId)) {
						mapOfOfficesDTOTogeoCountryId.put(officesDTO.geoCountryId, new HashSet<>());
					}
					mapOfOfficesDTOTogeoCountryId.get(officesDTO.geoCountryId).add(officesDTO);
					
					if( ! mapOfOfficesDTOToapprovalPathType.containsKey(officesDTO.approvalPathType)) {
						mapOfOfficesDTOToapprovalPathType.put(officesDTO.approvalPathType, new HashSet<>());
					}
					mapOfOfficesDTOToapprovalPathType.get(officesDTO.approvalPathType).add(officesDTO);
					
					if( ! mapOfOfficesDTOTolastModificationTime.containsKey(officesDTO.lastModificationTime)) {
						mapOfOfficesDTOTolastModificationTime.put(officesDTO.lastModificationTime, new HashSet<>());
					}
					mapOfOfficesDTOTolastModificationTime.get(officesDTO.lastModificationTime).add(officesDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
		logger.debug("OfficesRepository loading end for reloadAll: "+reloadAll);
	}
	
	public List<OfficesDTO> getOfficesList() {
		List <OfficesDTO> officess = new ArrayList<OfficesDTO>(this.mapOfOfficesDTOToiD.values());
		return officess;
	}
	
	
	public OfficesDTO getOfficesDTOByID( long ID){
		return mapOfOfficesDTOToiD.get(ID);
	}
	
	
	public List<OfficesDTO> getOfficesDTOByoffice_ministry_id(int office_ministry_id) {
		return new ArrayList<>( mapOfOfficesDTOToofficeMinistryId.getOrDefault(office_ministry_id,new HashSet<>()));
	}
	
	
	public List<OfficesDTO> getOfficesDTOByoffice_layer_id(int office_layer_id) {
		return new ArrayList<>( mapOfOfficesDTOToofficeLayerId.getOrDefault(office_layer_id,new HashSet<>()));
	}
	
	
	public List<OfficesDTO> getOfficesDTOBycustom_layer_id(int custom_layer_id) {
		return new ArrayList<>( mapOfOfficesDTOTocustomLayerId.getOrDefault(custom_layer_id,new HashSet<>()));
	}
	
	
	public List<OfficesDTO> getOfficesDTOByoffice_origin_id(int office_origin_id) {
		return new ArrayList<>( mapOfOfficesDTOToofficeOriginId.getOrDefault(office_origin_id,new HashSet<>()));
	}
	
	
	public List<OfficesDTO> getOfficesDTOByoffice_name_eng(String office_name_eng) {
		return new ArrayList<>( mapOfOfficesDTOToofficeNameEng.getOrDefault(office_name_eng,new HashSet<>()));
	}
	
	
	public List<OfficesDTO> getOfficesDTOByoffice_name_bng(String office_name_bng) {
		return new ArrayList<>( mapOfOfficesDTOToofficeNameBng.getOrDefault(office_name_bng,new HashSet<>()));
	}
	
	
	public List<OfficesDTO> getOfficesDTOBygeo_division_id(int geo_division_id) {
		return new ArrayList<>( mapOfOfficesDTOTogeoDivisionId.getOrDefault(geo_division_id,new HashSet<>()));
	}
	
	
	public List<OfficesDTO> getOfficesDTOBygeo_district_id(int geo_district_id) {
		return new ArrayList<>( mapOfOfficesDTOTogeoDistrictId.getOrDefault(geo_district_id,new HashSet<>()));
	}
	
	
	public List<OfficesDTO> getOfficesDTOBygeo_upazila_id(int geo_upazila_id) {
		return new ArrayList<>( mapOfOfficesDTOTogeoUpazilaId.getOrDefault(geo_upazila_id,new HashSet<>()));
	}
	
	
	public List<OfficesDTO> getOfficesDTOBygeo_union_id(int geo_union_id) {
		return new ArrayList<>( mapOfOfficesDTOTogeoUnionId.getOrDefault(geo_union_id,new HashSet<>()));
	}
	
	
	public List<OfficesDTO> getOfficesDTOBygeo_city_corporation_id(int geo_city_corporation_id) {
		return new ArrayList<>( mapOfOfficesDTOTogeoCityCorporationId.getOrDefault(geo_city_corporation_id,new HashSet<>()));
	}
	
	
	public List<OfficesDTO> getOfficesDTOBygeo_city_corporation_ward_id(int geo_city_corporation_ward_id) {
		return new ArrayList<>( mapOfOfficesDTOTogeoCityCorporationWardId.getOrDefault(geo_city_corporation_ward_id,new HashSet<>()));
	}
	
	
	public List<OfficesDTO> getOfficesDTOBygeo_municipality_id(int geo_municipality_id) {
		return new ArrayList<>( mapOfOfficesDTOTogeoMunicipalityId.getOrDefault(geo_municipality_id,new HashSet<>()));
	}
	
	
	public List<OfficesDTO> getOfficesDTOBygeo_municipality_ward_id(int geo_municipality_ward_id) {
		return new ArrayList<>( mapOfOfficesDTOTogeoMunicipalityWardId.getOrDefault(geo_municipality_ward_id,new HashSet<>()));
	}
	
	
	public List<OfficesDTO> getOfficesDTOByoffice_code(String office_code) {
		return new ArrayList<>( mapOfOfficesDTOToofficeCode.getOrDefault(office_code,new HashSet<>()));
	}
	
	
	public List<OfficesDTO> getOfficesDTOByoffice_address(String office_address) {
		return new ArrayList<>( mapOfOfficesDTOToofficeAddress.getOrDefault(office_address,new HashSet<>()));
	}
	
	
	public List<OfficesDTO> getOfficesDTOBydigital_nothi_code(String digital_nothi_code) {
		return new ArrayList<>( mapOfOfficesDTOTodigitalNothiCode.getOrDefault(digital_nothi_code,new HashSet<>()));
	}
	
	
	public List<OfficesDTO> getOfficesDTOByreference_code(String reference_code) {
		return new ArrayList<>( mapOfOfficesDTOToreferenceCode.getOrDefault(reference_code,new HashSet<>()));
	}
	
	
	public List<OfficesDTO> getOfficesDTOByoffice_phone(String office_phone) {
		return new ArrayList<>( mapOfOfficesDTOToofficePhone.getOrDefault(office_phone,new HashSet<>()));
	}
	
	
	public List<OfficesDTO> getOfficesDTOByoffice_mobile(String office_mobile) {
		return new ArrayList<>( mapOfOfficesDTOToofficeMobile.getOrDefault(office_mobile,new HashSet<>()));
	}
	
	
	public List<OfficesDTO> getOfficesDTOByoffice_fax(String office_fax) {
		return new ArrayList<>( mapOfOfficesDTOToofficeFax.getOrDefault(office_fax,new HashSet<>()));
	}
	
	
	public List<OfficesDTO> getOfficesDTOByoffice_email(String office_email) {
		return new ArrayList<>( mapOfOfficesDTOToofficeEmail.getOrDefault(office_email,new HashSet<>()));
	}
	
	
	public List<OfficesDTO> getOfficesDTOByoffice_web(String office_web) {
		return new ArrayList<>( mapOfOfficesDTOToofficeWeb.getOrDefault(office_web,new HashSet<>()));
	}
	
	
	public List<OfficesDTO> getOfficesDTOByparent_office_id(int parent_office_id) {
		return new ArrayList<>( mapOfOfficesDTOToparentOfficeId.getOrDefault(parent_office_id,new HashSet<>()));
	}
	
	
	public List<OfficesDTO> getOfficesDTOBystatus(boolean status) {
		return new ArrayList<>( mapOfOfficesDTOTostatus.getOrDefault(status,new HashSet<>()));
	}
	
	
	public List<OfficesDTO> getOfficesDTOBycreated_by(int created_by) {
		return new ArrayList<>( mapOfOfficesDTOTocreatedBy.getOrDefault(created_by,new HashSet<>()));
	}
	
	
	public List<OfficesDTO> getOfficesDTOBymodified_by(int modified_by) {
		return new ArrayList<>( mapOfOfficesDTOTomodifiedBy.getOrDefault(modified_by,new HashSet<>()));
	}
	
	
	public List<OfficesDTO> getOfficesDTOBycreated(String created) {
		return new ArrayList<>( mapOfOfficesDTOTocreated.getOrDefault(created,new HashSet<>()));
	}
	
	
	public List<OfficesDTO> getOfficesDTOBymodified(String modified) {
		return new ArrayList<>( mapOfOfficesDTOTomodified.getOrDefault(modified,new HashSet<>()));
	}
	
	
	public List<OfficesDTO> getOfficesDTOBygeo_thana_id(int geo_thana_id) {
		return new ArrayList<>( mapOfOfficesDTOTogeoThanaId.getOrDefault(geo_thana_id,new HashSet<>()));
	}
	
	
	public List<OfficesDTO> getOfficesDTOBygeo_postoffice_id(int geo_postoffice_id) {
		return new ArrayList<>( mapOfOfficesDTOTogeoPostofficeId.getOrDefault(geo_postoffice_id,new HashSet<>()));
	}
	
	
	public List<OfficesDTO> getOfficesDTOBygeo_country_id(int geo_country_id) {
		return new ArrayList<>( mapOfOfficesDTOTogeoCountryId.getOrDefault(geo_country_id,new HashSet<>()));
	}
	
	
	public List<OfficesDTO> getOfficesDTOByapproval_path_type(int approval_path_type) {
		return new ArrayList<>( mapOfOfficesDTOToapprovalPathType.getOrDefault(approval_path_type,new HashSet<>()));
	}
	
	
	public List<OfficesDTO> getOfficesDTOBylastModificationTime(long lastModificationTime) {
		return new ArrayList<>( mapOfOfficesDTOTolastModificationTime.getOrDefault(lastModificationTime,new HashSet<>()));
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "offices";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


