package offices;
import java.util.*; 


public class OfficesMAPS 
{

	public HashMap<String, String> java_allfield_type_map = new HashMap<String, String>();
	public HashMap<String, String> java_anyfield_search_map = new HashMap<String, String>();
	public HashMap<String, String> java_DTO_map = new HashMap<String, String>();
	public HashMap<String, String> java_SQL_map = new HashMap<String, String>();
	public HashMap<String, String> java_Text_map = new HashMap<String, String>();
	
	private static OfficesMAPS self = null;
	
	private OfficesMAPS()
	{
		
		java_allfield_type_map.put("office_ministry_id".toLowerCase(), "Integer");
		java_allfield_type_map.put("office_layer_id".toLowerCase(), "Integer");
		java_allfield_type_map.put("custom_layer_id".toLowerCase(), "Integer");
		java_allfield_type_map.put("office_origin_id".toLowerCase(), "Integer");
		java_allfield_type_map.put("office_name_eng".toLowerCase(), "String");
		java_allfield_type_map.put("office_name_bng".toLowerCase(), "String");
		java_allfield_type_map.put("geo_division_id".toLowerCase(), "Integer");
		java_allfield_type_map.put("geo_district_id".toLowerCase(), "Integer");
		java_allfield_type_map.put("geo_upazila_id".toLowerCase(), "Integer");
		java_allfield_type_map.put("geo_union_id".toLowerCase(), "Integer");
		java_allfield_type_map.put("geo_city_corporation_id".toLowerCase(), "Integer");
		java_allfield_type_map.put("geo_city_corporation_ward_id".toLowerCase(), "Integer");
		java_allfield_type_map.put("geo_municipality_id".toLowerCase(), "Integer");
		java_allfield_type_map.put("geo_municipality_ward_id".toLowerCase(), "Integer");
		java_allfield_type_map.put("office_code".toLowerCase(), "String");
		java_allfield_type_map.put("office_address".toLowerCase(), "String");
		java_allfield_type_map.put("digital_nothi_code".toLowerCase(), "String");
		java_allfield_type_map.put("reference_code".toLowerCase(), "String");
		java_allfield_type_map.put("office_phone".toLowerCase(), "String");
		java_allfield_type_map.put("office_mobile".toLowerCase(), "String");
		java_allfield_type_map.put("office_fax".toLowerCase(), "String");
		java_allfield_type_map.put("office_email".toLowerCase(), "String");
		java_allfield_type_map.put("office_web".toLowerCase(), "String");
		java_allfield_type_map.put("parent_office_id".toLowerCase(), "Integer");
		java_allfield_type_map.put("status".toLowerCase(), "Boolean");
		java_allfield_type_map.put("created_by".toLowerCase(), "Integer");
		java_allfield_type_map.put("modified_by".toLowerCase(), "Integer");
		java_allfield_type_map.put("created".toLowerCase(), "String");
		java_allfield_type_map.put("modified".toLowerCase(), "String");
		java_allfield_type_map.put("geo_thana_id".toLowerCase(), "Integer");
		java_allfield_type_map.put("geo_postoffice_id".toLowerCase(), "Integer");
		java_allfield_type_map.put("geo_country_id".toLowerCase(), "Integer");
		java_allfield_type_map.put("approval_path_type".toLowerCase(), "Integer");

		java_anyfield_search_map.put("offices.office_ministry_id".toLowerCase(), "Integer");
		java_anyfield_search_map.put("offices.office_layer_id".toLowerCase(), "Integer");
		java_anyfield_search_map.put("offices.custom_layer_id".toLowerCase(), "Integer");
		java_anyfield_search_map.put("offices.office_origin_id".toLowerCase(), "Integer");
		java_anyfield_search_map.put("offices.office_name_eng".toLowerCase(), "String");
		java_anyfield_search_map.put("offices.office_name_bng".toLowerCase(), "String");
		java_anyfield_search_map.put("offices.geo_division_id".toLowerCase(), "Integer");
		java_anyfield_search_map.put("offices.geo_district_id".toLowerCase(), "Integer");
		java_anyfield_search_map.put("offices.geo_upazila_id".toLowerCase(), "Integer");
		java_anyfield_search_map.put("offices.geo_union_id".toLowerCase(), "Integer");
		java_anyfield_search_map.put("offices.geo_city_corporation_id".toLowerCase(), "Integer");
		java_anyfield_search_map.put("offices.geo_city_corporation_ward_id".toLowerCase(), "Integer");
		java_anyfield_search_map.put("offices.geo_municipality_id".toLowerCase(), "Integer");
		java_anyfield_search_map.put("offices.geo_municipality_ward_id".toLowerCase(), "Integer");
		java_anyfield_search_map.put("offices.office_code".toLowerCase(), "String");
		java_anyfield_search_map.put("offices.office_address".toLowerCase(), "String");
		java_anyfield_search_map.put("offices.digital_nothi_code".toLowerCase(), "String");
		java_anyfield_search_map.put("offices.reference_code".toLowerCase(), "String");
		java_anyfield_search_map.put("offices.office_phone".toLowerCase(), "String");
		java_anyfield_search_map.put("offices.office_mobile".toLowerCase(), "String");
		java_anyfield_search_map.put("offices.office_fax".toLowerCase(), "String");
		java_anyfield_search_map.put("offices.office_email".toLowerCase(), "String");
		java_anyfield_search_map.put("offices.office_web".toLowerCase(), "String");
		java_anyfield_search_map.put("offices.parent_office_id".toLowerCase(), "Integer");
		java_anyfield_search_map.put("offices.status".toLowerCase(), "Boolean");
		java_anyfield_search_map.put("offices.created_by".toLowerCase(), "Integer");
		java_anyfield_search_map.put("offices.modified_by".toLowerCase(), "Integer");
		java_anyfield_search_map.put("offices.created".toLowerCase(), "String");
		java_anyfield_search_map.put("offices.modified".toLowerCase(), "String");
		java_anyfield_search_map.put("offices.geo_thana_id".toLowerCase(), "Integer");
		java_anyfield_search_map.put("offices.geo_postoffice_id".toLowerCase(), "Integer");
		java_anyfield_search_map.put("offices.geo_country_id".toLowerCase(), "Integer");
		java_anyfield_search_map.put("approval_path.name_en".toLowerCase(), "String");
		java_anyfield_search_map.put("approval_path.name_bn".toLowerCase(), "String");

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("officeMinistryId".toLowerCase(), "officeMinistryId".toLowerCase());
		java_DTO_map.put("officeLayerId".toLowerCase(), "officeLayerId".toLowerCase());
		java_DTO_map.put("customLayerId".toLowerCase(), "customLayerId".toLowerCase());
		java_DTO_map.put("officeOriginId".toLowerCase(), "officeOriginId".toLowerCase());
		java_DTO_map.put("officeNameEng".toLowerCase(), "officeNameEng".toLowerCase());
		java_DTO_map.put("officeNameBng".toLowerCase(), "officeNameBng".toLowerCase());
		java_DTO_map.put("geoDivisionId".toLowerCase(), "geoDivisionId".toLowerCase());
		java_DTO_map.put("geoDistrictId".toLowerCase(), "geoDistrictId".toLowerCase());
		java_DTO_map.put("geoUpazilaId".toLowerCase(), "geoUpazilaId".toLowerCase());
		java_DTO_map.put("geoUnionId".toLowerCase(), "geoUnionId".toLowerCase());
		java_DTO_map.put("geoCityCorporationId".toLowerCase(), "geoCityCorporationId".toLowerCase());
		java_DTO_map.put("geoCityCorporationWardId".toLowerCase(), "geoCityCorporationWardId".toLowerCase());
		java_DTO_map.put("geoMunicipalityId".toLowerCase(), "geoMunicipalityId".toLowerCase());
		java_DTO_map.put("geoMunicipalityWardId".toLowerCase(), "geoMunicipalityWardId".toLowerCase());
		java_DTO_map.put("officeCode".toLowerCase(), "officeCode".toLowerCase());
		java_DTO_map.put("officeAddress".toLowerCase(), "officeAddress".toLowerCase());
		java_DTO_map.put("digitalNothiCode".toLowerCase(), "digitalNothiCode".toLowerCase());
		java_DTO_map.put("referenceCode".toLowerCase(), "referenceCode".toLowerCase());
		java_DTO_map.put("officePhone".toLowerCase(), "officePhone".toLowerCase());
		java_DTO_map.put("officeMobile".toLowerCase(), "officeMobile".toLowerCase());
		java_DTO_map.put("officeFax".toLowerCase(), "officeFax".toLowerCase());
		java_DTO_map.put("officeEmail".toLowerCase(), "officeEmail".toLowerCase());
		java_DTO_map.put("officeWeb".toLowerCase(), "officeWeb".toLowerCase());
		java_DTO_map.put("parentOfficeId".toLowerCase(), "parentOfficeId".toLowerCase());
		java_DTO_map.put("status".toLowerCase(), "status".toLowerCase());
		java_DTO_map.put("createdBy".toLowerCase(), "createdBy".toLowerCase());
		java_DTO_map.put("modifiedBy".toLowerCase(), "modifiedBy".toLowerCase());
		java_DTO_map.put("created".toLowerCase(), "created".toLowerCase());
		java_DTO_map.put("modified".toLowerCase(), "modified".toLowerCase());
		java_DTO_map.put("geoThanaId".toLowerCase(), "geoThanaId".toLowerCase());
		java_DTO_map.put("geoPostofficeId".toLowerCase(), "geoPostofficeId".toLowerCase());
		java_DTO_map.put("geoCountryId".toLowerCase(), "geoCountryId".toLowerCase());
		java_DTO_map.put("approvalPathType".toLowerCase(), "approvalPathType".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("office_ministry_id".toLowerCase(), "officeMinistryId".toLowerCase());
		java_SQL_map.put("office_layer_id".toLowerCase(), "officeLayerId".toLowerCase());
		java_SQL_map.put("custom_layer_id".toLowerCase(), "customLayerId".toLowerCase());
		java_SQL_map.put("office_origin_id".toLowerCase(), "officeOriginId".toLowerCase());
		java_SQL_map.put("office_name_eng".toLowerCase(), "officeNameEng".toLowerCase());
		java_SQL_map.put("office_name_bng".toLowerCase(), "officeNameBng".toLowerCase());
		java_SQL_map.put("geo_division_id".toLowerCase(), "geoDivisionId".toLowerCase());
		java_SQL_map.put("geo_district_id".toLowerCase(), "geoDistrictId".toLowerCase());
		java_SQL_map.put("geo_upazila_id".toLowerCase(), "geoUpazilaId".toLowerCase());
		java_SQL_map.put("geo_union_id".toLowerCase(), "geoUnionId".toLowerCase());
		java_SQL_map.put("geo_city_corporation_id".toLowerCase(), "geoCityCorporationId".toLowerCase());
		java_SQL_map.put("geo_city_corporation_ward_id".toLowerCase(), "geoCityCorporationWardId".toLowerCase());
		java_SQL_map.put("geo_municipality_id".toLowerCase(), "geoMunicipalityId".toLowerCase());
		java_SQL_map.put("geo_municipality_ward_id".toLowerCase(), "geoMunicipalityWardId".toLowerCase());
		java_SQL_map.put("office_code".toLowerCase(), "officeCode".toLowerCase());
		java_SQL_map.put("office_address".toLowerCase(), "officeAddress".toLowerCase());
		java_SQL_map.put("digital_nothi_code".toLowerCase(), "digitalNothiCode".toLowerCase());
		java_SQL_map.put("reference_code".toLowerCase(), "referenceCode".toLowerCase());
		java_SQL_map.put("office_phone".toLowerCase(), "officePhone".toLowerCase());
		java_SQL_map.put("office_mobile".toLowerCase(), "officeMobile".toLowerCase());
		java_SQL_map.put("office_fax".toLowerCase(), "officeFax".toLowerCase());
		java_SQL_map.put("office_email".toLowerCase(), "officeEmail".toLowerCase());
		java_SQL_map.put("office_web".toLowerCase(), "officeWeb".toLowerCase());
		java_SQL_map.put("parent_office_id".toLowerCase(), "parentOfficeId".toLowerCase());
		java_SQL_map.put("status".toLowerCase(), "status".toLowerCase());
		java_SQL_map.put("created_by".toLowerCase(), "createdBy".toLowerCase());
		java_SQL_map.put("modified_by".toLowerCase(), "modifiedBy".toLowerCase());
		java_SQL_map.put("created".toLowerCase(), "created".toLowerCase());
		java_SQL_map.put("modified".toLowerCase(), "modified".toLowerCase());
		java_SQL_map.put("geo_thana_id".toLowerCase(), "geoThanaId".toLowerCase());
		java_SQL_map.put("geo_postoffice_id".toLowerCase(), "geoPostofficeId".toLowerCase());
		java_SQL_map.put("geo_country_id".toLowerCase(), "geoCountryId".toLowerCase());
		java_SQL_map.put("approval_path_type".toLowerCase(), "approvalPathType".toLowerCase());
		java_SQL_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Office Ministry Id".toLowerCase(), "officeMinistryId".toLowerCase());
		java_Text_map.put("Office Layer Id".toLowerCase(), "officeLayerId".toLowerCase());
		java_Text_map.put("Custom Layer Id".toLowerCase(), "customLayerId".toLowerCase());
		java_Text_map.put("Office Origin Id".toLowerCase(), "officeOriginId".toLowerCase());
		java_Text_map.put("Office Name Eng".toLowerCase(), "officeNameEng".toLowerCase());
		java_Text_map.put("Office Name Bng".toLowerCase(), "officeNameBng".toLowerCase());
		java_Text_map.put("Geo Division Id".toLowerCase(), "geoDivisionId".toLowerCase());
		java_Text_map.put("Geo District Id".toLowerCase(), "geoDistrictId".toLowerCase());
		java_Text_map.put("Geo Upazila Id".toLowerCase(), "geoUpazilaId".toLowerCase());
		java_Text_map.put("Geo Union Id".toLowerCase(), "geoUnionId".toLowerCase());
		java_Text_map.put("Geo City Corporation Id".toLowerCase(), "geoCityCorporationId".toLowerCase());
		java_Text_map.put("Geo City Corporation Ward Id".toLowerCase(), "geoCityCorporationWardId".toLowerCase());
		java_Text_map.put("Geo Municipality Id".toLowerCase(), "geoMunicipalityId".toLowerCase());
		java_Text_map.put("Geo Municipality Ward Id".toLowerCase(), "geoMunicipalityWardId".toLowerCase());
		java_Text_map.put("Office Code".toLowerCase(), "officeCode".toLowerCase());
		java_Text_map.put("Office Address".toLowerCase(), "officeAddress".toLowerCase());
		java_Text_map.put("Digital Nothi Code".toLowerCase(), "digitalNothiCode".toLowerCase());
		java_Text_map.put("Reference Code".toLowerCase(), "referenceCode".toLowerCase());
		java_Text_map.put("Office Phone".toLowerCase(), "officePhone".toLowerCase());
		java_Text_map.put("Office Mobile".toLowerCase(), "officeMobile".toLowerCase());
		java_Text_map.put("Office Fax".toLowerCase(), "officeFax".toLowerCase());
		java_Text_map.put("Office Email".toLowerCase(), "officeEmail".toLowerCase());
		java_Text_map.put("Office Web".toLowerCase(), "officeWeb".toLowerCase());
		java_Text_map.put("Parent Office Id".toLowerCase(), "parentOfficeId".toLowerCase());
		java_Text_map.put("Status".toLowerCase(), "status".toLowerCase());
		java_Text_map.put("Created By".toLowerCase(), "createdBy".toLowerCase());
		java_Text_map.put("Modified By".toLowerCase(), "modifiedBy".toLowerCase());
		java_Text_map.put("Created".toLowerCase(), "created".toLowerCase());
		java_Text_map.put("Modified".toLowerCase(), "modified".toLowerCase());
		java_Text_map.put("Geo Thana Id".toLowerCase(), "geoThanaId".toLowerCase());
		java_Text_map.put("Geo Postoffice Id".toLowerCase(), "geoPostofficeId".toLowerCase());
		java_Text_map.put("Geo Country Id".toLowerCase(), "geoCountryId".toLowerCase());
		java_Text_map.put("Approval Path".toLowerCase(), "approvalPathType".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}
	
	public static OfficesMAPS GetInstance()
	{
		if(self == null)
		{
			self = new OfficesMAPS ();
		}
		return self;
	}
	

}