package employee_info_report;


import language.LC;
import language.LM;
import login.LoginDTO;
import org.apache.log4j.Logger;
import pbReport.PBReportUtils;
import pbReport.ReportCommonService;
import permission.MenuConstants;
import role.PermissionRepository;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.HttpRequestUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;


@SuppressWarnings({"Duplicates", "unused"})
@WebServlet("/Employee_info_report_Servlet")
public class Employee_info_report_Servlet extends HttpServlet implements ReportCommonService {

    private static final Logger logger = Logger.getLogger(Employee_info_report_Servlet.class);
    private static final long serialVersionUID = 1L;

    private static final Set<String> searchParam = new HashSet<>(Arrays.asList(
            "age_end", "age_start", "bloodGroup", "religion", "maritalStatus", "joiningDateStart", "joiningDateEnd", "incharge_label",
            "parliamentJoiningDateStart", "parliamentJoiningDateEnd", "lprDateStart", "lprDateEnd", "empOfficerCat",
            "employmentCat", "employeeClassCat", "genderCat", "officeUnitOrganogramId", "home_district",
            "designationName", "officeUnitIds", "organogram_role", "jobGradeTypeCat", "nameEng", "nameBng" , "official_email", "personal_email"
    ));

    private final Map<String, String[]> stringMap = new HashMap<>();

    private static final String sql = " employee_offices eo inner join employee_records er on eo.employee_record_id = er.id";

    private String[][] Criteria;

    private String[][] Display;

    public Employee_info_report_Servlet() {
        stringMap.put("officeUnitIds", new String[]{"criteria", "eo", "office_unit_id", "IN", "AND", "String", "", "", ""
                , "officeUnitIdList", "officeUnitIds", String.valueOf(LC.EMPLOYEE_INFO_REPORT_WHERE_OFFICEUNITID), "office_units", null, "true", "1"});
        stringMap.put("officeUnitOrganogramId", new String[]{"criteria", "eo", "office_unit_organogram_id", "=", "AND", "long", "", "", "any",
                "officeUnitOrganogramId", "officeUnitOrganogramId", String.valueOf(LC.EMPLOYEE_OFFICE_REPORT_WHERE_OFFICEUNITORGANOGRAMID), "office_unit_organogram", null, "true", "2"});
        stringMap.put("bloodGroup", new String[]{"criteria", "er", "blood_group", "=", "AND", "String", "", "", "any",
                "bloodGroup", "bloodGroup", String.valueOf(LC.EMPLOYEE_INFO_REPORT_WHERE_BLOODGROUP), "category", "blood_group", "true", "3"});
        stringMap.put("religion", new String[]{"criteria", "er", "religion", "=", "AND", "long", "", "", "any",
                "religion", "religion", String.valueOf(LC.EMPLOYEE_INFO_REPORT_WHERE_RELIGION), "religion", null, "true", "4"});
        stringMap.put("genderCat", new String[]{"criteria", "er", "gender_cat", "=", "AND", "long", "", "", "any"
                , "genderCat", "genderCat", String.valueOf(LC.EMPLOYEE_INFO_REPORT_WHERE_GENDERCAT), "category", "gender", "true", "5"});
        stringMap.put("maritalStatus", new String[]{"criteria", "er", "marital_status", "=", "AND", "String", "", "", "any"
                , "maritalStatus", "maritalStatus", String.valueOf(LC.EMPLOYEE_INFO_REPORT_WHERE_MARITALSTATUS), "category", "marital_status", "true", "6"});
        stringMap.put("employeeClassCat", new String[]{"criteria", "er", "employee_class_cat", "=", "AND", "long", "", "", "any",
                "employeeClassCat", "employeeClassCat", String.valueOf(LC.EMPLOYEE_EMPLOYEE_CLASS), "category", "employee_class", "true", "7"});
        stringMap.put("employmentCat", new String[]{"criteria", "er", "employment_cat", "=", "AND", "long", "", "", "any"
                , "employmentCat", "employmentCat", String.valueOf(LC.EMPLOYMENT_CATEGORY_EMPLOYMENTCATEGORY), "category", "employment", "true", "8"});
        stringMap.put("empOfficerCat", new String[]{"criteria", "er", "emp_officer_cat", "=", "AND", "long", "", "", "any",
                "empOfficerCat", "empOfficerCat", String.valueOf(LC.EMPLOYEE_OFFICER_TYPE), "category", "emp_officer", "true", "9"});
        stringMap.put("joiningDateStart", new String[]{"criteria", "eo", "joining_date", ">=", "AND", "long", "", "", Long.MIN_VALUE + "",
                "joiningDateStart", "joiningDateStart", String.valueOf(LC.EMPLOYEE_INFO_REPORT_WHERE_JOININGDATE), "date", null, "true", "10"});
        stringMap.put("joiningDateEnd", new String[]{"criteria", "eo", "joining_date", "<=", "AND", "long", "", "", Long.MAX_VALUE + "",
                "joiningDateEnd", "joiningDateEnd", String.valueOf(LC.EMPLOYEE_INFO_REPORT_WHERE_JOININGDATE_10), "date", null, "true", "11"});
        stringMap.put("parliamentJoiningDateStart", new String[]{"criteria", "er", "joining_date", ">=", "AND", "long", "", "", Long.MIN_VALUE + "",
                "parliamentJoiningDateStart", "parliamentJoiningDateStart", String.valueOf(LC.EMPLOYEE_INFO_REPORT_WHERE_PARLIAMENT_JOINING_DATE_START), "date", null, "true", "12"});
        stringMap.put("parliamentJoiningDateEnd", new String[]{"criteria", "er", "joining_date", "<=", "AND", "long", "", "", Long.MAX_VALUE + "",
                "parliamentJoiningDateEnd", "parliamentJoiningDateEnd", String.valueOf(LC.EMPLOYEE_INFO_REPORT_WHERE_PARLIAMENT_JOINING_DATE_END), "date", null, "true", "13"});
        stringMap.put("lprDateStart", new String[]{"criteria", "er", "lpr_date", ">=", "AND", "long", "", "", Long.MIN_VALUE + "",
                "lprDateStart", "lprDateStart", String.valueOf(LC.EMPLOYEE_INFO_REPORT_WHERE_LPRDATE), "date", null, "true", "14"});
        stringMap.put("lprDateEnd", new String[]{"criteria", "er", "lpr_date", "<=", "AND", "long", "", "", Long.MAX_VALUE + "",
                "lprDateEnd", "lprDateEnd", String.valueOf(LC.EMPLOYEE_INFO_REPORT_WHERE_LPRDATE_12), "date", null, "true", "15"});
        stringMap.put("age_start", new String[]{"criteria", "er", "date_of_birth", "<=", "AND", "long", "", "", ""
                , "ageStart", "age_start", String.valueOf(LC.HR_REPORT_AGE_RANGE_START), "number", null, "true", "16"});
        stringMap.put("age_end", new String[]{"criteria", "er", "date_of_birth", ">=", "AND", "long", "", "", ""
                , "ageEnd", "age_end", String.valueOf(LC.HR_REPORT_AGE_RANGE_TO), "number", null, "true", "17"});
        stringMap.put("home_district", new String[]{"criteria", "er", "home_district", "=", "AND", "long", "", "", "", "home_district"
                , "home_district", String.valueOf(LC.EMPLOYEE_RECORDS_ADD_HOME_DISTRICT), "location", null, "true", "18"});
        stringMap.put("designationName", new String[]{"criteria", "eo", "designation_en", "IN", "AND", "String", "", "", "", "designation_name"
                , "designationName", String.valueOf(LC.EMPLOYEE_OFFICE_REPORT_WHERE_OFFICEUNITORGANOGRAMID), "text", null, "true", "19"});
        stringMap.put("organogram_role", new String[]{"criteria", "eo", "organogram_role", "IN", "AND", "long", "", "", "any",
                "organogram_role", "organogram_role", String.valueOf(LC.USER_ADD_ROLE), "number", null, "true", "20"});
        stringMap.put("jobGradeTypeCat", new String[]{"criteria", "eo", "job_grade_type_cat", "=", "AND", "int", "", "", "any", "jobGradeTypeCat",
                "jobGradeTypeCat", String.valueOf(LC.DESIGNATION_STATUS_REPORT_WHERE_JOBGRADETYPECAT), "category", "job_grade_type", "true", "21"});
        stringMap.put("nameEng", new String[]{"criteria", "er", "name_eng", "Like", "AND", "String", "", "", "any", "nameEng",
                "nameEng", String.valueOf(LC.EMPLOYEE_RECORDS_ADD_NAMEENG), "text", null, "true", "22"});
        stringMap.put("nameBng", new String[]{"criteria", "er", "name_bng", "Like", "AND", "String", "", "", "any", "nameBng",
                "nameBng", String.valueOf(LC.EMPLOYEE_RECORDS_ADD_NAMEBNG), "text", null, "true", "23"});
        stringMap.put("official_email", new String[]{"criteria", "er", "official_email", "Like", "AND", "String", "", "", "any", "official_email",
                "official_email", String.valueOf(LC.GLOBAL_EMAIL), "basic", null, "true", "24"});
        stringMap.put("personal_email", new String[]{"criteria", "er", "personal_email", "Like", "AND", "String", "", "", "any", "personal_email",
                "personal_email", String.valueOf(LC.GLOBAL_EMAIL), "basic", null, "true", "25"});
        
        stringMap.put("lprDateEndDefault", new String[]{"criteria", "er", "lpr_date", ">", "AND", "long", "", "", SessionConstants.MIN_DATE + "", "lprDateEndDefault", null, null, null, null, null, null});
        stringMap.put("incharge_label", new String[]{"criteria", "eo", "incharge_label", "IN", "AND", "String", "", "", "any", "incharge_label", null, null, null, null, null, null});
        stringMap.put("status", new String[]{"criteria", "eo", "status", "=", "AND", "long", "", "", "1", "status", null, null, null, null, null, null});
        stringMap.put("office_id", new String[]{"criteria", "eo", "office_id", "=", "AND", "long", "", "", "2294", "office_id", null, null, null, null, null, null});
        stringMap.put("isDeleted", new String[]{"criteria", "eo", "isDeleted", "=", "AND", "long", "", "", "0", "isDeleted", null, null, null, null, null, null});
        stringMap.put("is_mp", new String[]{"criteria", "er", "is_mp", "=", "AND", "int", "", "", "2", "is_mp", null, null, null, null, null, null});
        stringMap.put("is_default_role", new String[]{"criteria", "eo", "is_default_role", "=", "AND", "int", "", "", "1", "is_default_role", null, null, null, null, null, null});
        stringMap.put("isDeleted_1", new String[]{"criteria", "er", "isDeleted", "=", "AND", "long", "", "", "0", "isDeleted", null, null, null, null, null, null});
        stringMap.put("joiningDateEndDefault", new String[]{"criteria", "eo", "joining_date", ">", "AND", "long", "", "", SessionConstants.MIN_DATE + "", "joiningDateEndDefault", null, null, null, null, null, null});
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);

        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }
        boolean isLangEng = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
        Set<String> inputs = PBReportUtils.prepareInputSets(searchParam, request);
        if (inputs.contains("joiningDateEnd")) {
            inputs.add("joiningDateEndDefault");
        }
        if (inputs.contains("lprDateEnd")) {
            inputs.add("lprDateEndDefault");
        }
        inputs.add("office_id");
        inputs.add("isDeleted_1");
        inputs.add("isDeleted");
        inputs.add("status");
//        inputs.add("incharge_label");
        inputs.add("is_mp");
        inputs.add("is_default_role");

        Display = new String[][]{
                {"display", "eo", "employee_record_id", "employee_records_id_en", LM.getText(LC.GLOBAL_EMPLOYEE_NAME_ENG, loginDTO)},
                {"display", "eo", "employee_record_id", "employee_records_id_bn", LM.getText(LC.GLOBAL_EMPLOYEE_NAME_BNG, loginDTO)},
                {"display", "eo", "employee_record_id", "user_name", LM.getText(LC.EMPLOYEE_INFO_REPORT_SELECT_USER_ID, loginDTO)},
                {"display", "er", "personal_mobile", "number", LM.getText(LC.EMPLOYEE_INFO_REPORT_SELECT_MOBILE_NUMBER, loginDTO)},
                {"display", "er", "personal_email", "plain", isLangEng?"Personal Email":"ব্যাক্তিগত ইমেইল"},
                {"display", "er", "official_email", "basic", isLangEng?"Official Email":"অফিসিয়াল ইমেইল"},
                {"display", "er", "date_of_birth", "date", LM.getText(LC.EMPLOYEE_RECORDS_ADD_DATEOFBIRTH, loginDTO)},
                {"display", "er", "employee_class_cat", "category", LM.getText(LC.EMPLOYEE_INFO_REPORT_SELECT_EMPLOYEECLASSCAT, loginDTO)},
                {"display", "er", "emp_officer_cat", "category", LM.getText(LC.EMPLOYEE_INFO_REPORT_SELECT_EMPOFFICERCAT, loginDTO)},
                {"display", "eo", "job_grade_type_cat", "category", LM.getText(LC.DESIGNATION_STATUS_REPORT_SELECT_JOBGRADETYPECAT, loginDTO)},
                {"display", "eo", "office_unit_id", "office_unit", LM.getText(LC.RETIREMENT_DETAILS_REPORT_SELECT_OFFICEUNITID, loginDTO)},
                {"display", "eo", "office_unit_organogram_id", "office_unit_organogram", LM.getText(LC.RETIREMENT_DETAILS_REPORT_SELECT_OFFICEUNITORGANOGRAMID, loginDTO)},
                {"display", "eo", "joining_date", "date", LM.getText(LC.EMPLOYEE_INFO_REPORT_SELECT_JOININGDATE, loginDTO)},
                {"display", "er", "date_of_birth", "complete_age", LM.getText(LC.PHYSIOTHERAPY_PLAN_ADD_AGE, loginDTO)},
                {"display", "er", "joining_date", "date", LM.getText(LC.EMPLOYEE_INFO_REPORT_SELECT_PARLIAMENT_JOINING_DATE, loginDTO)},
                {"display", "er", "lpr_date", "date", LM.getText(LC.EMPLOYEE_INFO_REPORT_SELECT_LPRDATE, loginDTO)},
                {"display", "eo", "last_office_date", "date", isLangEng ? "Last office date" : "অফিসের শেষ তারিখ"},
                {"display", "er", "nid", "number", LM.getText(LC.EMPLOYEE_INFO_REPORT_SELECT_NID, loginDTO)},
                {"display", "er", "home_district", "location", LM.getText(LC.EMPLOYEE_RECORDS_ADD_HOME_DISTRICT, loginDTO)},
                {"display", "er", "blood_group", "categorywithoutCat", LM.getText(LC.EMPLOYEE_INFO_REPORT_SELECT_BLOODGROUP, loginDTO)},
                {"display", "er", "religion", "religion", LM.getText(LC.EMPLOYEE_INFO_REPORT_SELECT_RELIGION, loginDTO)},
                {"display", "er", "gender_cat", "category", LM.getText(LC.EMPLOYEE_INFO_REPORT_SELECT_GENDERCAT, loginDTO)},
                {"display", "er", "marital_status", "categorywithoutCat", LM.getText(LC.EMPLOYEE_INFO_REPORT_SELECT_MARITALSTATUS, loginDTO)},
                {"display", "er", "employment_cat", "category", LM.getText(LC.EMPLOYEE_INFO_REPORT_SELECT_EMPLOYMENTCAT, loginDTO)},
                {"display", "eo", "organogram_role", "role", LM.getText(LC.USER_ADD_ROLE, loginDTO)},
                {"display", "eo", "incharge_label", "incharge_label", isLangEng ? "In charge Level" : "ইনচার্জ লেভেল"},
                {"display", "eo", "employee_record_id", "id_to_present_address", LM.getText(LC.EMPLOYEE_RECORDS_ADD_PRESENTADDRESS, loginDTO)},
                {"display", "eo", "employee_record_id", "id_to_permanent_address", LM.getText(LC.EMPLOYEE_RECORDS_ADD_PERMANENTADDRESS, loginDTO)},
               
                {"display", "er", "photo", "file", LM.getText(LC.HM_PHOTO, loginDTO)},
                {"display", "er", "signature", "file", isLangEng ? "Signature" : "সাক্ষর"}
        };
        
        logger.debug("showPic = " + request.getParameter("showPic"));
		if(request.getParameter("showPic") == null || request.getParameter("showPic").equals("false") )
			if(request.getParameter("showPic") == null || request.getParameter("showPic").equals("false"))
			{
				Display[28][2] = "id";
				Display[28][3] = "none";
				
				Display[29][2] = "id";
				Display[29][3] = "none";
			}
			else
			{
				Display[28][2] = "photo";
				Display[28][3] = "file";
				
				Display[29][2] = "photo";
				Display[29][3] = "file";
			}
        Criteria = PBReportUtils.prepareCriteria(inputs, stringMap);

        if (inputs.contains("officeUnitIds") || inputs.contains("age_start")
                || inputs.contains("age_end") || inputs.contains("designationName") || inputs.contains("jobGradeTypeCat")) {
            for (String[] arr : Criteria) {
                switch (arr[9]) {
                    case "officeUnitIdList":
                        arr[8] = getOfficeIdsFromOfficeUnitIds(request).stream()
                                .map(String::valueOf)
                                .collect(Collectors.joining(","));
                        break;
                    case "designation_name":
                        arr[8] = Arrays.stream(request.getParameter("designationName").split(","))
                                .filter(e -> e.contains(":"))
                                .map(e -> e.split(":")[1])
                                .map(String::trim)
                                .filter(e -> e.length() > 0)
                                .map(e -> "'" + e + "'")
                                .collect(Collectors.joining(","));
                        logger.debug("designation values " + arr[8]);
                        break;
                    case "ageStart":
                        long ageStart = Long.parseLong(request.getParameter("age_start"));
                        arr[8] = String.valueOf(calculateLongDateValue(ageStart));
                        break;
                    case "ageEnd":
                        long ageEnd = Long.parseLong(request.getParameter("age_end"));
                        arr[8] = String.valueOf(calculateLongDateValue(ageEnd));
                        break;
                    case "jobGradeTypeCat":
                        int jobGradeCat = Integer.parseInt(request.getParameter("jobGradeTypeCat"));
                        String sign = "=";
                        int jobGradeEqual = Integer.parseInt(request.getParameter("jobGradeEqual"));
                        int jobGradeLess = Integer.parseInt(request.getParameter("jobGradeLess"));
                        int jobGradeMore = Integer.parseInt(request.getParameter("jobGradeMore"));
                        if (jobGradeEqual == 0 && jobGradeLess == 0 && jobGradeMore == 0)
                            break;
                        if (jobGradeEqual == 0)
                            sign = "";
                        if (jobGradeLess == 1) {
                            if (jobGradeCat <= 20) {
                                sign = ">" + sign;
                            }
                        }
                        if (jobGradeMore == 1) {
                            if (jobGradeCat <= 20) {
                                sign = "<" + sign;
                            }
                        }
                        arr[3] = sign;
                        break;
                }
            }
        }

        request.setAttribute("dontUseOutDatedModal", true);
        if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.EMPLOYEE_INFO_REPORT_DETAILS)) {
            reportGenerate(request, response);
        }

    }

    private long calculateLongDateValue(long ageYears) {
        LocalDate ld = LocalDate.now();
        ld = ld.minusYears(ageYears);
        return Date.from(ld.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant()).getTime();
    }

    @Override
    public String[][] getCriteria() {
        return Criteria;
    }

    @Override
    public String[][] getDisplay() {
        return Display;
    }

    @Override
    public String getSQL() {
        return sql;
    }

    @Override
    public int getLCForReportName() {
        return LC.EMPLOYEE_INFO_REPORT_OTHER_EMPLOYEE_INFO_REPORT;
    }

    @Override
    public String getFileName() {
        return "employee_info_report";
    }

    @Override
    public String getTableName() {
        return "employee_info_report";
    }

}
