package budget_submission_info;

import budget.BudgetUtils;
import budget.SubmissionStatusEnum;
import org.apache.log4j.Logger;
import repository.Repository;
import repository.RepositoryManager;
import util.LockManager;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class Budget_submission_infoRepository implements Repository {
    private static final Logger logger = Logger.getLogger(Budget_submission_infoRepository.class);
    private final Budget_submission_infoDAO budget_submission_infoDAO;
    private final Map<Long, Budget_submission_infoDTO> mapById;
    private List<Budget_submission_infoDTO> budget_submission_infoDTOList;

    private Budget_submission_infoRepository() {
        budget_submission_infoDAO = Budget_submission_infoDAO.getInstance();
        mapById = new ConcurrentHashMap<>();
        RepositoryManager.getInstance().addRepository(this);
    }

    private static class Budget_submission_infoRepositoryLazyLoader {
        final static Budget_submission_infoRepository INSTANCE = new Budget_submission_infoRepository();
    }

    public static Budget_submission_infoRepository getInstance() {
        return Budget_submission_infoRepository.Budget_submission_infoRepositoryLazyLoader.INSTANCE;
    }

    public void reload(boolean reloadAll) {
        logger.debug("Budget Submission Info Repository loading start for reload: " + reloadAll);
        List<Budget_submission_infoDTO> budget_submission_infoDTOS = budget_submission_infoDAO.getAllBudgetOperation(reloadAll);
        if (budget_submission_infoDTOS != null && budget_submission_infoDTOS.size() > 0) {
            budget_submission_infoDTOS.stream()
                                      .peek(this::removeIfPresent)
                                      .filter(dto -> dto.isDeleted == 0)
                                      .forEach(dto -> mapById.put(dto.iD, dto));
            budget_submission_infoDTOList = new ArrayList<>(mapById.values());
            budget_submission_infoDTOList.sort(Comparator.comparing(o -> o.iD));
        }
        logger.debug("Budget Submission Info Repository loading end for reload: " + reloadAll);
    }

    @Override
    public String getTableName() {
        return "budget_submission_info";
    }

    public Budget_submission_infoDTO getById(long id) {
        if (mapById.get(id) == null) {
            synchronized (LockManager.getLock(id + "BSIBI")) {
                if (mapById.get(id) == null) {
                    Budget_submission_infoDTO dto = budget_submission_infoDAO.getDTOByID(id);
                    if (dto != null) {
                        mapById.put(id, dto);
                    }
                }
            }
        }
        return mapById.get(id);
    }

    private void removeIfPresent(Budget_submission_infoDTO dto) {
        if (dto == null) return;
        if (mapById.get(dto.iD) != null) {
            mapById.remove(dto.iD);
        }
    }

    public List<Budget_submission_infoDTO> getBudget_operationDTOList() {
        return budget_submission_infoDTOList;
    }

    public boolean hasAllOfficeSubmitted(Set<Long> officeIds, long budgetSelectionInfoId, int budgetTypeCat) {
        if (budget_submission_infoDTOList == null) return false;

        long count =
                budget_submission_infoDTOList
                        .stream()
                        .filter(submissionInfoDTO -> submissionInfoDTO.budgetOfficeId != BudgetUtils.FINANCE_SECTION_OFFICE_ID)
                        .filter(submissionInfoDTO -> submissionInfoDTO.budgetSelectionInfoId == budgetSelectionInfoId
                                                     && submissionInfoDTO.budgetTypeCat == budgetTypeCat)
                        .filter(submissionInfoDTO -> officeIds.contains(submissionInfoDTO.budgetOfficeId))
                        .filter(submissionInfoDTO -> submissionInfoDTO.isSubmit == SubmissionStatusEnum.SUBMITTED.getValue())
                        .count();
        return count == officeIds.size();
    }

    public boolean hasOfficeSubmitted(long budgetSelectionInfoId, long budgetOfficeId, int budgetTypeCat) {
        if (budget_submission_infoDTOList == null) return false;
        return budget_submission_infoDTOList
                .stream()
                .filter(submissionInfoDTO -> submissionInfoDTO.budgetSelectionInfoId == budgetSelectionInfoId)
                .filter(submissionInfoDTO -> submissionInfoDTO.budgetTypeCat == budgetTypeCat)
                .anyMatch(submissionInfoDTO -> submissionInfoDTO.budgetOfficeId == budgetOfficeId);
    }
}