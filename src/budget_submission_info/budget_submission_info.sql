create table budget_submission_info
(
    ID bigint(20) not null primary key,
    budget_Id   BIGINT(20),
    budget_type_cat INT(11),
    budget_selection_info_id   BIGINT(20),
    budget_office_id   BIGINT(20),
    is_submit TINYINT(4),
    is_expenditure_submit TINYINT(4),
    isDeleted            INT(11)      DEFAULT 0,
    inserted_by          BIGINT(20)   DEFAULT 0,
    insertion_time       BIGINT(20)   DEFAULT -62135791200000,
    modified_by          BIGINT(20)   DEFAULT 0,
    lastModificationTime BIGINT(20)   DEFAULT -62135791200000
) ENGINE = MyISAM
  DEFAULT CHARSET = utf8
  COLLATE = utf8_unicode_ci;