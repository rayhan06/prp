package budget_submission_info;

import budget.SubmissionStatusEnum;
import util.CommonDTO;

public class Budget_submission_infoDTO extends CommonDTO {
    public int budgetTypeCat;
    public long budgetSelectionInfoId;
    public long budgetOfficeId;
    public int isSubmit = SubmissionStatusEnum.NOT_APPLICABLE.getValue();
    public int isExpenditureSubmit = SubmissionStatusEnum.NOT_APPLICABLE.getValue();
    public int isPreparationSubmit=SubmissionStatusEnum.NOT_APPLICABLE.getValue();
    public long insertedBy = 0;
    public long insertionTime = 0;
    public long modifiedBy = 0;

    @Override
    public String toString() {
        return "budget_submission_infoDTO{" +
                ", budgetTypeCat=" + budgetTypeCat +
                ", budgetSelectionInfoId=" + budgetSelectionInfoId +
                ", budgetOfficeId=" + budgetOfficeId +
                ", isSubmit=" + isSubmit +
                ", isExpenditureSubmit=" + isExpenditureSubmit +
                ", insertedBy=" + insertedBy +
                ", insertionTime=" + insertionTime +
                ", modifiedBy=" + modifiedBy +
                '}';
    }
}
