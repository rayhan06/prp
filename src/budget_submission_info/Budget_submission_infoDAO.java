package budget_submission_info;

import budget.BudgetTypeEnum;
import budget.BudgetUtils;
import budget.SubmissionStatusEnum;
import budget_selection_info.BudgetSelectionInfoRepository;
import common.CommonDAOService;
import org.apache.log4j.Logger;
import util.CommonDTO;
import util.LockManager;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

public class Budget_submission_infoDAO implements CommonDAOService<Budget_submission_infoDTO> {
    private static final Logger logger = Logger.getLogger(Budget_submission_infoDAO.class);
    private static final String addQuery = "INSERT INTO {tableName} (budget_type_cat,budget_selection_info_id,"
            .concat("budget_office_id,is_submit,is_expenditure_submit,is_preparation_submit,")
            .concat("modified_by,lastModificationTime,insertion_time,inserted_by,isDeleted,ID) VALUES(?,?,?,?,?,?,?,?,?,?,?,?)");

    private static final String updateQuery = "UPDATE {tableName} SET budget_type_cat=?,budget_selection_info_id=?,"
            .concat("budget_office_id=?,is_submit=?,is_expenditure_submit=?,is_preparation_submit=?,")
            .concat("modified_by=?,lastModificationTime=? WHERE ID=?");

    private static final String getByBudgetTypeAndBudgetOfficeId = "SELECT * FROM %s WHERE budget_selection_info_id=%d AND budget_type_cat=%d AND "
            .concat("budget_office_id=%d AND isDeleted=0");

    private static Budget_submission_infoDAO INSTANCE = null;

    public static Budget_submission_infoDAO getInstance() {
        if (INSTANCE == null) {
            synchronized (Budget_submission_infoDAO.class) {
                if (INSTANCE == null) {
                    INSTANCE = new Budget_submission_infoDAO();
                }
            }
        }
        return INSTANCE;
    }

    @Override
    public void set(PreparedStatement ps, Budget_submission_infoDTO budget_submission_infoDTO, boolean isInsert) throws SQLException {
        int index = 0;
        ps.setInt(++index, budget_submission_infoDTO.budgetTypeCat);
        ps.setLong(++index, budget_submission_infoDTO.budgetSelectionInfoId);
        ps.setLong(++index, budget_submission_infoDTO.budgetOfficeId);
        ps.setInt(++index, budget_submission_infoDTO.isSubmit);
        ps.setInt(++index, budget_submission_infoDTO.isExpenditureSubmit);
        ps.setInt(++index, budget_submission_infoDTO.isPreparationSubmit);

        ps.setLong(++index, budget_submission_infoDTO.modifiedBy);
        ps.setLong(++index, budget_submission_infoDTO.lastModificationTime);
        if (isInsert) {
            ps.setLong(++index, budget_submission_infoDTO.insertionTime);
            ps.setLong(++index, budget_submission_infoDTO.insertedBy);
            ps.setInt(++index, 0 /* isDeleted */);
        }
        ps.setObject(++index, budget_submission_infoDTO.iD);
    }

    @Override
    public Budget_submission_infoDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Budget_submission_infoDTO dto = new Budget_submission_infoDTO();
            dto.iD = rs.getLong("ID");
            dto.budgetTypeCat = rs.getInt("budget_type_cat");
            dto.budgetSelectionInfoId = rs.getLong("budget_selection_info_id");
            dto.budgetOfficeId = rs.getLong("budget_office_id");
            dto.isSubmit = rs.getInt("is_submit");
            dto.isExpenditureSubmit = rs.getInt("is_expenditure_submit");
            dto.isPreparationSubmit = rs.getInt("is_preparation_submit");
            dto.insertionTime = rs.getLong("insertion_time");
            dto.insertedBy = rs.getLong("inserted_by");
            dto.modifiedBy = rs.getLong("modified_by");
            dto.isDeleted = rs.getInt("isDeleted");
            dto.lastModificationTime = rs.getLong("lastModificationTime");
            return dto;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    @Override
    public String getTableName() {
        return "budget_submission_info";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return null;
    }

    @Override
    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Budget_submission_infoDTO) commonDTO, updateQuery, false);
    }

    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Budget_submission_infoDTO) commonDTO, addQuery, true);
    }

    public Budget_submission_infoDTO getDTOByID(long ID) {
        return getDTOFromID(ID);
    }


    public List<Budget_submission_infoDTO> getAllBudgetOperation(boolean isFirstReload) {
        return getAllDTOs(isFirstReload);
    }

    public Budget_submission_infoDTO getByBudgetTypeAndBudgetOffice(long budgetSelectionInfoId, int budgetTypeCat, long budgetOfficeId) {
        List<Budget_submission_infoDTO> budget_submission_infoDTOS = getDTOs(String.format(getByBudgetTypeAndBudgetOfficeId, getTableName(), budgetSelectionInfoId, budgetTypeCat, budgetOfficeId));
        if (budget_submission_infoDTOS == null || budget_submission_infoDTOS.size() == 0)
            return null;
        return budget_submission_infoDTOS.get(0);
    }

    public Budget_submission_infoDTO getOrCreateAndGetDTO(long budgetSelectionInfoId, int budgetTypeCat, long budgetOfficeId, long requesterId) throws Exception {
        Budget_submission_infoDTO submissionInfoDTO = getByBudgetTypeAndBudgetOffice(budgetSelectionInfoId, budgetTypeCat, budgetOfficeId);
        if (submissionInfoDTO == null) {
            synchronized (LockManager.getLock("BudgetSubmissionInfo" + budgetSelectionInfoId + budgetTypeCat + budgetOfficeId)) {
                submissionInfoDTO = getByBudgetTypeAndBudgetOffice(budgetSelectionInfoId, budgetTypeCat, budgetOfficeId);
                if (submissionInfoDTO == null) {
                    long currentTime = System.currentTimeMillis();
                    submissionInfoDTO = new Budget_submission_infoDTO();

                    submissionInfoDTO.budgetSelectionInfoId = budgetSelectionInfoId;
                    submissionInfoDTO.budgetTypeCat = budgetTypeCat;
                    submissionInfoDTO.budgetOfficeId = budgetOfficeId;
                    submissionInfoDTO.insertedBy = requesterId;
                    submissionInfoDTO.modifiedBy = requesterId;
                    submissionInfoDTO.insertionTime = currentTime;
                    submissionInfoDTO.lastModificationTime = currentTime;
                    submissionInfoDTO.isSubmit = SubmissionStatusEnum.NOT_SUBMITTED.getValue();

                    if (budgetOfficeId != BudgetUtils.FINANCE_SECTION_OFFICE_ID) {
                        submissionInfoDTO.isExpenditureSubmit = SubmissionStatusEnum.NOT_APPLICABLE.getValue();
                        submissionInfoDTO.isPreparationSubmit = SubmissionStatusEnum.NOT_APPLICABLE.getValue();
                    } else {
                        submissionInfoDTO.isExpenditureSubmit = SubmissionStatusEnum.NOT_SUBMITTED.getValue();
                        submissionInfoDTO.isPreparationSubmit = SubmissionStatusEnum.NOT_SUBMITTED.getValue();
                    }

                    add(submissionInfoDTO);
                }
            }
        }
        return submissionInfoDTO;
    }

    public void submitBudget(long budgetSelectionInfoId, long budgetOfficeId, int budgetTypeCat, long requesterId) throws Exception {
        boolean toAdd = false;

        Budget_submission_infoDTO budget_submission_infoDTO = getByBudgetTypeAndBudgetOffice(budgetSelectionInfoId, budgetTypeCat, budgetOfficeId);
        if (budget_submission_infoDTO == null) {
            budget_submission_infoDTO = new Budget_submission_infoDTO();
            toAdd = true;
        }
        if (budget_submission_infoDTO.isSubmit == SubmissionStatusEnum.SUBMITTED.getValue())
            throw new Exception("Office Already Submitted Budget.");

        budget_submission_infoDTO.budgetTypeCat = budgetTypeCat;
        budget_submission_infoDTO.budgetSelectionInfoId = budgetSelectionInfoId;
        budget_submission_infoDTO.budgetOfficeId = budgetOfficeId;
        budget_submission_infoDTO.isSubmit = SubmissionStatusEnum.SUBMITTED.getValue();
        if (budgetOfficeId != BudgetUtils.FINANCE_SECTION_OFFICE_ID) {
            budget_submission_infoDTO.isExpenditureSubmit = SubmissionStatusEnum.NOT_APPLICABLE.getValue();
            budget_submission_infoDTO.isPreparationSubmit = SubmissionStatusEnum.NOT_APPLICABLE.getValue();
        } else {
            budget_submission_infoDTO.isExpenditureSubmit = SubmissionStatusEnum.NOT_SUBMITTED.getValue();
            budget_submission_infoDTO.isPreparationSubmit = SubmissionStatusEnum.NOT_SUBMITTED.getValue();
        }
        budget_submission_infoDTO.modifiedBy = requesterId;
        budget_submission_infoDTO.lastModificationTime = System.currentTimeMillis();

        if (toAdd) {
            budget_submission_infoDTO.insertedBy = requesterId;
            budget_submission_infoDTO.insertionTime = System.currentTimeMillis();
            add(budget_submission_infoDTO);
        } else {
            update(budget_submission_infoDTO);
        }
    }

    public boolean isAbleToSubmitExpenditure(long budgetSelectionInfoId, long budgetOfficeId, int budgetTypeCat) {
        if (budgetOfficeId != BudgetUtils.FINANCE_SECTION_OFFICE_ID) return false;

        Budget_submission_infoDTO submissionInfoDTO = getByBudgetTypeAndBudgetOffice(budgetSelectionInfoId, budgetTypeCat, budgetOfficeId);
        if (submissionInfoDTO == null) return false;
        String economicYear = BudgetSelectionInfoRepository.getInstance().getDTOByID(budgetSelectionInfoId).economicYear;
        long currentTime = System.currentTimeMillis();
        boolean timeValidity;
        if (budgetTypeCat == BudgetTypeEnum.BUDGET.getValue()) {
            timeValidity = BudgetUtils.isExpenditureAvailableByYear(economicYear, currentTime);
        } else {
            timeValidity = BudgetUtils.isRevisedExpenditureAvailableByYear(economicYear, currentTime);
        }
        return submissionInfoDTO.isSubmit == SubmissionStatusEnum.SUBMITTED.getValue()
                && submissionInfoDTO.isExpenditureSubmit != SubmissionStatusEnum.SUBMITTED.getValue() && timeValidity;
    }

    public void submitExpenditure(long budgetSelectionInfoId, long budgetOfficeId, int budgetTypeCat, long requesterId) throws Exception {
        if (budgetOfficeId != BudgetUtils.FINANCE_SECTION_OFFICE_ID)
            throw new Exception("Only Finance Department can submit expenditure");

        Budget_submission_infoDTO submissionInfoDTO = getOrCreateAndGetDTO(budgetSelectionInfoId, budgetTypeCat, budgetOfficeId, requesterId);
        boolean alreadySubmitted = submissionInfoDTO.isExpenditureSubmit == SubmissionStatusEnum.SUBMITTED.getValue();
        if (alreadySubmitted) throw new Exception("Expenditure already submitted");

        submissionInfoDTO.isExpenditureSubmit = SubmissionStatusEnum.SUBMITTED.getValue();
        submissionInfoDTO.modifiedBy = requesterId;
        submissionInfoDTO.lastModificationTime = System.currentTimeMillis();
        update(submissionInfoDTO);
    }
}
