package business_card_approval_mapping;

import business_card_info.Business_card_infoDAO;
import business_card_info.Business_card_infoDTO;
import card_info.*;
import common.ConnectionAndStatementUtil;
import common.EmployeeCommonDAOService;
import employee_offices.EmployeeOfficeDTO;
import employee_offices.EmployeeOfficeRepository;
import office_unit_organograms.OfficeUnitOrganograms;
import office_unit_organograms.OfficeUnitOrganogramsRepository;
import org.apache.log4j.Logger;
import task_type_approval_path.TaskTypeApprovalPathDTO;
import task_type_approval_path.TaskTypeApprovalPathRepository;
import util.CommonDTO;
import util.LockManager;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;

@SuppressWarnings("ALL")
public class Business_card_approval_mappingDAO implements EmployeeCommonDAOService<Business_card_approval_mappingDTO> {
    private static final Logger logger = Logger.getLogger(Business_card_approval_mappingDAO.class);
    private static final String addSqlQuery = "INSERT INTO {tableName} (business_card_status_cat,card_approval_status_cat,sequence,modified_by,lastModificationTime,search_column,"
            .concat("business_card_info_id,card_approval_id,inserted_by,insertion_time,task_type_id,")
            .concat("employee_records_id,approver_employee_records_id,isDeleted,id) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
    private static final String updateSqlQuery = "UPDATE {tableName} SET business_card_status_cat = ?,card_approval_status_cat = ?,sequence = ?,modified_by = ?,lastModificationTime = ?,search_column=?,"
            .concat("isDeleted = ? WHERE id = ?");
    private static final String getByCardInfoId = "SELECT * FROM business_card_approval_mapping WHERE business_card_info_id = %d AND isDeleted = 0 ORDER BY sequence DESC";
    private static final String getByCardInfoIdAndApproverEmployeeRecordsId = "SELECT * FROM business_card_approval_mapping WHERE business_card_info_id = %d AND approver_employee_records_id = %d";
    private static final String getByCardInfoIdAndStatus = "SELECT * FROM business_card_approval_mapping WHERE business_card_info_id = %d AND card_approval_status_cat = %d AND isDeleted = 0";
    private static final String updateStatus = "UPDATE business_card_approval_mapping SET card_approval_status_cat = %d,isDeleted = %d,modified_by = %d,lastModificationTime = %d WHERE id IN (%s)";
    private static final String updateStatusByCardInfoId = "UPDATE business_card_approval_mapping SET card_approval_status_cat = %d,isDeleted = %d,"
            .concat("modified_by = %d,lastModificationTime = %d WHERE card_approval_status_cat = %d AND business_card_info_id = %d");
    private static final String updateCardStatusByBusinessCardInfoId = "UPDATE business_card_approval_mapping SET business_card_status_cat = %d WHERE business_card_info_id = %d";
    private static final Map<String, String> searchMap = new HashMap<>();


    private static final String tableName = "business_card_approval_mapping";

    private static class LazyLoader {
        static final Business_card_approval_mappingDAO INSTANCE = new Business_card_approval_mappingDAO();
    }

    public static Business_card_approval_mappingDAO getInstance() {
        return Business_card_approval_mappingDAO.LazyLoader.INSTANCE;
    }


    private Business_card_approval_mappingDAO() {
        searchMap.put("business_card_status_cat", "and (business_card_status_cat = ?)");
        searchMap.put("card_approval_status_cat", "and (card_approval_status_cat = ?)");
        searchMap.put("employee_records_id_internal", "and (approver_employee_records_id = ?)");
        searchMap.put("AnyField", " and (search_column like ?)");
    }

    public void setSearchColumn(Business_card_approval_mappingDTO card_approval_mappingDTO) {
        Business_card_infoDTO card_infoDTO = Business_card_infoDAO.getInstance().getDTOFromID(card_approval_mappingDTO.businessCardInfoId);
        CardEmployeeInfoDTO employeeInfoDTO = CardEmployeeInfoRepository.getInstance().getById(card_infoDTO.cardEmployeeInfoId);
        card_approval_mappingDTO.searchColumn = employeeInfoDTO.nameEn.toLowerCase();
        if (!card_approval_mappingDTO.searchColumn.contains(employeeInfoDTO.nameBn.toLowerCase())) {
            card_approval_mappingDTO.searchColumn += " " + employeeInfoDTO.nameBn.toLowerCase();
        }
        CardEmployeeOfficeInfoDTO officeInfoDTO = CardEmployeeOfficeInfoRepository.getInstance().getById(card_infoDTO.cardEmployeeOfficeInfoId);
        if (!card_approval_mappingDTO.searchColumn.contains(officeInfoDTO.officeUnitEng.toLowerCase())) {
            card_approval_mappingDTO.searchColumn += " " + officeInfoDTO.officeUnitEng.toLowerCase();
        }
        if (!card_approval_mappingDTO.searchColumn.contains(officeInfoDTO.officeUnitBng.toLowerCase())) {
            card_approval_mappingDTO.searchColumn += " " + officeInfoDTO.officeUnitBng.toLowerCase();
        }
        if (!card_approval_mappingDTO.searchColumn.contains(officeInfoDTO.organogramEng.toLowerCase())) {
            card_approval_mappingDTO.searchColumn += " " + officeInfoDTO.organogramEng.toLowerCase();
        }
        if (!card_approval_mappingDTO.searchColumn.contains(officeInfoDTO.organogramBng.toLowerCase())) {
            card_approval_mappingDTO.searchColumn += " " + officeInfoDTO.organogramBng.toLowerCase();
        }
    }

    @Override
    public void set(PreparedStatement ps, Business_card_approval_mappingDTO dto, boolean isInsert) throws SQLException {
        int index = 0;
        setSearchColumn(dto);
        ps.setInt(++index, dto.cardStatus);
        ps.setInt(++index, dto.cardApprovalStatusCat);
        ps.setInt(++index, dto.sequence);
        ps.setLong(++index, dto.modifiedBy);
        ps.setLong(++index, dto.lastModificationTime);
        ps.setObject(++index, dto.searchColumn);
        if (isInsert) {
            ps.setLong(++index, dto.businessCardInfoId);
            ps.setLong(++index, dto.cardApprovalId);
            ps.setLong(++index, dto.insertedBy);
            ps.setLong(++index, dto.insertionTime);
            ps.setLong(++index, dto.taskTypeId);
            ps.setLong(++index, dto.employeeRecordsId);
            ps.setLong(++index, dto.approverEmployeeRecordsId);
        }
        ps.setInt(++index, dto.isDeleted);
        ps.setLong(++index, dto.iD);
    }


    @Override
    public Business_card_approval_mappingDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Business_card_approval_mappingDTO dto = new Business_card_approval_mappingDTO();
            dto.iD = rs.getLong("id");
            dto.businessCardInfoId = rs.getLong("business_card_info_id");
            dto.cardApprovalId = rs.getLong("card_approval_id");
            dto.cardApprovalStatusCat = rs.getInt("card_approval_status_cat");
            dto.sequence = rs.getInt("sequence");
            dto.taskTypeId = rs.getLong("task_type_id");
            dto.insertedBy = rs.getLong("inserted_by");
            dto.insertionTime = rs.getLong("insertion_time");
            dto.modifiedBy = rs.getLong("modified_by");
            dto.lastModificationTime = rs.getLong("lastModificationTime");
            dto.isDeleted = rs.getInt("isDeleted");
            dto.searchColumn = rs.getString("search_column");
            dto.employeeRecordsId = rs.getInt("employee_records_id");
            dto.approverEmployeeRecordsId = rs.getInt("approver_employee_records_id");
            dto.cardStatus = rs.getInt("business_card_status_cat");
            return dto;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    @Override
    public String getTableName() {
        return tableName;
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    @Override
    public long add(CommonDTO dto) throws Exception {
        return executeAddOrUpdateQuery((Business_card_approval_mappingDTO) dto, addSqlQuery, true);
    }

    @Override
    public long update(CommonDTO dto) throws Exception {
        return executeAddOrUpdateQuery((Business_card_approval_mappingDTO) dto, updateSqlQuery, false);
    }

    public Business_card_approval_mappingDTO getDTOByID(long ID) {
        return getDTOFromID(ID);
    }

    public CardApprovalResponse createCardApproval(CreateBusinessCardApprovalModel model) throws Exception {
        String sql = String.format(getByCardInfoId, model.getCardInfoDTO().iD);
        Business_card_approval_mappingDTO dto = ConnectionAndStatementUtil.getT(sql, this::buildObjectFromResultSet);
        if (dto != null) {
            throw new DuplicateCardInfoException("Approval is already created for " + model.getCardInfoDTO().iD);
        }
        CardApprovalResponse cardApprovalResponse = new CardApprovalResponse();
        EmployeeOfficeDTO employeeOfficeDTO = EmployeeOfficeRepository.getInstance().getByEmployeeRecordIdIsDefault(model.getRequesterEmployeeRecordId());
        OfficeUnitOrganograms officeUnitOrganograms = OfficeUnitOrganogramsRepository.getInstance().getById(employeeOfficeDTO.officeUnitOrganogramId);
        TaskTypeApprovalPathDTO taskTypeApprovalPathDTO = new TaskTypeApprovalPathDTO();
        taskTypeApprovalPathDTO.officeUnitOrganogramId = officeUnitOrganograms.superior_designation_id;
        if (taskTypeApprovalPathDTO.officeUnitOrganogramId == 0) {
            logger.debug("taskTypeApprovalPathDTO.officeUnitOrganogramId = 0; For that card approval direct moved to next level");
            return movedToNextLevel(model, 2);
        } else {
            cardApprovalResponse.hasNextApproval = true;
            cardApprovalResponse.organogramIds = add(Collections.singletonList(taskTypeApprovalPathDTO), model, 1);
        }
        return cardApprovalResponse;
    }

    private Business_card_approval_mappingDTO buildDTO(long approverEmployeeRecordId, CreateBusinessCardApprovalModel model, int level,
                                                       int isDeleted, ApprovalStatus approvalStatus) {
        Business_card_approval_mappingDTO cardApprovalMappingDTO = new Business_card_approval_mappingDTO();
        CardApprovalDTO cardApprovalDTO = CardApprovalRepository.getInstance().getByEmployeeRecordId(approverEmployeeRecordId, model.getRequesterEmployeeRecordId());
        if (cardApprovalDTO == null) {
            return null;
        }
        cardApprovalMappingDTO.businessCardInfoId = model.getCardInfoDTO().iD;
        cardApprovalMappingDTO.cardApprovalId = cardApprovalDTO.iD;
        cardApprovalMappingDTO.cardApprovalStatusCat = approvalStatus.getValue();
        cardApprovalMappingDTO.sequence = level;
        cardApprovalMappingDTO.taskTypeId = model.getTaskTypeId();
        cardApprovalMappingDTO.insertedBy = cardApprovalMappingDTO.modifiedBy = model.getRequesterEmployeeRecordId();
        cardApprovalMappingDTO.insertionTime = cardApprovalMappingDTO.lastModificationTime = System.currentTimeMillis();
        cardApprovalMappingDTO.isDeleted = isDeleted;
        cardApprovalMappingDTO.employeeRecordsId = model.getCardInfoDTO().employeeRecordsId;
        cardApprovalMappingDTO.approverEmployeeRecordsId = cardApprovalDTO.employeeRecordId;
        return cardApprovalMappingDTO;
    }

    private List<Long> add(List<TaskTypeApprovalPathDTO> nextApprovalPath, CreateBusinessCardApprovalModel model, int level) {
        long currentTime = System.currentTimeMillis();
        List<Long> addedToApproval = new ArrayList<>();
        nextApprovalPath.forEach(approver -> {
            EmployeeOfficeDTO approverOfficeDTO = EmployeeOfficeRepository.getInstance().getByOfficeUnitOrganogramId(approver.officeUnitOrganogramId);
            if (approverOfficeDTO != null) {
                Business_card_approval_mappingDTO dto = buildDTO(approverOfficeDTO.employeeRecordId, model, level, 0, ApprovalStatus.PENDING);
                if (dto != null) {
                    try {
                        add(dto);
                        addedToApproval.add(approverOfficeDTO.officeUnitOrganogramId);
                    } catch (Exception e) {
                        logger.error(e);
                    }
                }
            }
        });
        return addedToApproval;
    }

    public CardApprovalResponse movedToNextLevelOfApproval(CreateBusinessCardApprovalModel model) throws InvalidDataException, AlreadyApprovedException {
        synchronized (LockManager.getLock(model.getCardInfoDTO().iD + "bcamd")) {
            ValidateResponse validateResponse = approveOfCurrentLevel(model);
            return movedToNextLevel(model, validateResponse.currentLevel + 1);
        }
    }

    public ValidateResponse approveOfCurrentLevel(CreateBusinessCardApprovalModel model) throws InvalidDataException, AlreadyApprovedException {
        synchronized (LockManager.getLock(model.getCardInfoDTO().iD + "bcamd")) {
            ValidateResponse validateResponse = validate(model);
            String ids = validateResponse.dtoList.stream()
                    .map(e -> String.valueOf(e.iD))
                    .collect(Collectors.joining(","));
            String sql2 = String.format(updateStatus, ApprovalStatus.SATISFIED.getValue(), 0, model.getRequesterEmployeeRecordId(), System.currentTimeMillis(), ids);

            boolean res = (Boolean) ConnectionAndStatementUtil.getWriteStatement(st -> {
                try {
                    st.executeUpdate(sql2);
                    return true;
                } catch (SQLException ex) {
                    logger.error(ex);
                    return false;
                }
            });
            if (!res) {
                throw new InvalidDataException("Exception occurred during updating approval status");
            }
            return validateResponse;
        }
    }

    private CardApprovalResponse movedToNextLevel(CreateBusinessCardApprovalModel model, int nextLevel) {
        CardApprovalResponse response = new CardApprovalResponse();
        List<TaskTypeApprovalPathDTO> nextApprovalPath = TaskTypeApprovalPathRepository.getInstance().getByTaskTypeIdAndLevel(model.getTaskTypeId(), nextLevel);
        if (nextApprovalPath == null || nextApprovalPath.size() == 0) {
            response.hasNextApproval = false;
            response.organogramIds = null;
        } else {
            List<Long> organogramIds = add(nextApprovalPath, model, nextLevel);
            response.hasNextApproval = true;
            response.organogramIds = organogramIds;
            response.nextLevel = nextLevel;
        }
        return response;
    }

    public boolean rejectPendingApproval(CreateBusinessCardApprovalModel model, String rejectReason) throws InvalidDataException, AlreadyApprovedException {
        synchronized (LockManager.getLock(model.getCardInfoDTO().iD + "bcamd")) {
            validate(model);
            String sql = String.format(updateStatusByCardInfoId, ApprovalStatus.DISSATISFIED.getValue(), 0, model.getRequesterEmployeeRecordId(), System.currentTimeMillis(),
                    ApprovalStatus.PENDING.getValue(), model.getCardInfoDTO().iD);
            return (Boolean) ConnectionAndStatementUtil.getWriteStatement(st -> {
                try {
                    st.executeUpdate(sql);
                    return true;
                } catch (SQLException ex) {
                    logger.error(ex);
                    return false;
                }
            });
        }
    }

    private ValidateResponse validate(CreateBusinessCardApprovalModel model) throws InvalidDataException, AlreadyApprovedException {
        String sql = String.format(getByCardInfoIdAndStatus, model.getCardInfoDTO().iD, ApprovalStatus.PENDING.getValue());
        List<Business_card_approval_mappingDTO> dtoList = ConnectionAndStatementUtil.getListOfT(sql, this::buildObjectFromResultSet);
        if (dtoList == null || dtoList.size() == 0) {
            throw new InvalidDataException("No Pending approval is found for cardInfoId : " + model.getCardInfoDTO().iD);
        }
        Business_card_approval_mappingDTO validRequester = dtoList.stream()
                .filter(dto -> dto.approverEmployeeRecordsId == model.getRequesterEmployeeRecordId())
                .findAny()
                .orElse(null);

        if (validRequester == null) {
            sql = String.format(getByCardInfoIdAndApproverEmployeeRecordsId, model.getCardInfoDTO().iD, model.getRequesterEmployeeRecordId());
            validRequester = ConnectionAndStatementUtil.getT(sql, this::buildObjectFromResultSet);
            if (validRequester != null) {
                throw new AlreadyApprovedException();
            }
            throw new InvalidDataException("Invalid employee request to approve");
        }
        Set<Integer> sequenceValueSet = dtoList.stream()
                .map(e -> e.sequence)
                .collect(Collectors.toSet());
        if (sequenceValueSet.size() > 1) {
            throw new InvalidDataException("Multiple sequence value is found for Pending approval of cardInfoId : " + model.getCardInfoDTO().iD);
        }
        return new ValidateResponse(dtoList, (Integer) sequenceValueSet.toArray()[0], validRequester);
    }

    private static class ValidateResponse {
        List<Business_card_approval_mappingDTO> dtoList;
        int currentLevel;
        Business_card_approval_mappingDTO cardApprovalMappingDTO;

        public ValidateResponse(List<Business_card_approval_mappingDTO> dtoList, int currentLevel, Business_card_approval_mappingDTO cardApprovalMappingDTO) {
            this.dtoList = dtoList;
            this.currentLevel = currentLevel;
            this.cardApprovalMappingDTO = cardApprovalMappingDTO;
        }
    }

    public Business_card_approval_mappingDTO getByCardInfoIdAndApproverEmployeeRecordId(long cardInfoId, long approverEmployeeId) {
        String sql = String.format(getByCardInfoIdAndApproverEmployeeRecordsId, cardInfoId, approverEmployeeId);
        return ConnectionAndStatementUtil.getT(sql, this::buildObjectFromResultSet);
    }

    public List<Business_card_approval_mappingDTO> getAllApprovalDTOByCardInfoId(long cardInfoId) {
        String sql = String.format(getByCardInfoId, cardInfoId);
        List<Business_card_approval_mappingDTO> list = ConnectionAndStatementUtil.getListOfT(sql, this::buildObjectFromResultSet);
        return list.stream()
                .filter(dto -> dto.cardApprovalStatusCat == ApprovalStatus.PENDING.getValue() || dto.approverEmployeeRecordsId == dto.modifiedBy)
                .collect(Collectors.toList());
    }

    public List<Business_card_approval_mappingDTO> getAllApprovalDTOByCardInfoIdNotPending(long cardInfoId) {
        String sql = String.format(getByCardInfoId, cardInfoId);
        List<Business_card_approval_mappingDTO> list = ConnectionAndStatementUtil.getListOfT(sql, this::buildObjectFromResultSet);
        return list.stream()
                .filter(dto -> dto.cardApprovalStatusCat != ApprovalStatus.PENDING.getValue() && dto.approverEmployeeRecordsId == dto.modifiedBy)
                .collect(Collectors.toList());
    }

    public void updateCardStatusByBusinessCardInfoId(int status, long businessCardInfoId) {
        String sql = String.format(updateCardStatusByBusinessCardInfoId, status, businessCardInfoId);
        ConnectionAndStatementUtil.getWriteStatement(st -> {
            logger.debug(sql);
            try {
                st.executeUpdate(sql);
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        });
    }
}