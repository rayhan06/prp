package business_card_approval_mapping;

import util.CommonDTO;
import util.CommonEmployeeDTO;

public class Business_card_approval_mappingDTO extends CommonEmployeeDTO {
    public long businessCardInfoId = 0;
    public long cardApprovalId = 0;
    public int sequence = 0;
    public long insertedBy = 0;
    public long insertionTime = 0;
    public long modifiedBy = 0;
    public int cardApprovalStatusCat = 0;
    public long approverEmployeeRecordsId = 0;
    public long taskTypeId = 0;
    public int cardStatus;

    @Override
    public String toString() {
        return "Business_card_approval_mappingDTO{" +
                "cardInfoId=" + businessCardInfoId +
                ", cardApprovalId=" + cardApprovalId +
                ", sequence=" + sequence +
                ", insertedBy=" + insertedBy +
                ", insertionTime=" + insertionTime +
                ", modifiedBy=" + modifiedBy +
                ", cardApprovalStatusCat=" + cardApprovalStatusCat +
                ", employeeRecordsId=" + employeeRecordsId +
                ", approverEmployeeRecordsId=" + approverEmployeeRecordsId +
                ", taskTypeId=" + taskTypeId +
                '}';
    }
}
