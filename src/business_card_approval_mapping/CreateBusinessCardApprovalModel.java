package business_card_approval_mapping;

import business_card_info.Business_card_infoDTO;

public class CreateBusinessCardApprovalModel {
    private final long taskTypeId;
    private final Business_card_infoDTO cardInfoDTO;
    private final long requesterEmployeeRecordId;

    private CreateBusinessCardApprovalModel(CreateBusinessCardApprovalModel.CreateBusinessCardApprovalModelBuilder builder) {
        taskTypeId = builder.taskTypeId;
        cardInfoDTO = builder.cardInfoDTO;
        requesterEmployeeRecordId = builder.requesterEmployeeRecordId;
    }

    public long getTaskTypeId() {
        return taskTypeId;
    }

    public Business_card_infoDTO getCardInfoDTO() {
        return cardInfoDTO;
    }

    public long getRequesterEmployeeRecordId() {
        return requesterEmployeeRecordId;
    }

    public static class CreateBusinessCardApprovalModelBuilder {
        private long taskTypeId;
        private Business_card_infoDTO cardInfoDTO;
        private long requesterEmployeeRecordId;

        public CreateBusinessCardApprovalModel.CreateBusinessCardApprovalModelBuilder setTaskTypeId(long taskTypeId) {
            this.taskTypeId = taskTypeId;
            return this;
        }

        public CreateBusinessCardApprovalModel.CreateBusinessCardApprovalModelBuilder setCardInfoDTO(Business_card_infoDTO cardInfoDTO) {
            this.cardInfoDTO = cardInfoDTO;
            return this;
        }

        public CreateBusinessCardApprovalModel.CreateBusinessCardApprovalModelBuilder setRequesterEmployeeRecordId(long requesterEmployeeRecordId) {
            this.requesterEmployeeRecordId = requesterEmployeeRecordId;
            return this;
        }

        public CreateBusinessCardApprovalModel build() {
            return new CreateBusinessCardApprovalModel(this);
        }
    }
}
