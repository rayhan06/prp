package business_card_approval_mapping;

import business_card_info.BusinessCardStatusEnum;
import business_card_info.Business_card_infoDAO;
import business_card_info.Business_card_infoDTO;
import card_info.CardApprovalResponse;
import common.BaseServlet;
import common.CommonDAOService;
import common.EmployeeServletService;
import employee_offices.EmployeeOfficeDTO;
import employee_offices.EmployeeOfficeRepository;
import login.LoginDTO;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import permission.MenuConstants;
import role.PermissionRepository;
import sessionmanager.SessionConstants;
import task_type.TaskTypeEnum;
import task_type_approval_path.TaskTypeApprovalPathDTO;
import task_type_approval_path.TaskTypeApprovalPathRepository;
import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings("Duplicates")
@WebServlet("/Business_card_approval_mappingServlet")
@MultipartConfig
public class BusinessCardApprovalMappingServlet extends BaseServlet implements EmployeeServletService {
    private static final long serialVersionUID = 1L;
    private static final Logger logger = Logger.getLogger(BusinessCardApprovalMappingServlet.class);

    private static final String tableName = "business_card_approval_mapping";

    private final Business_card_approval_mappingDAO businessCardApprovalMappingDAO = Business_card_approval_mappingDAO.getInstance();


    @Override
    public String getTableName() {
        return tableName;
    }

    @Override
    public String getServletName() {
        return "Business_card_approval_mappingServlet";
    }

    @Override
    public Business_card_approval_mappingDAO getCommonDAOService() {
        return businessCardApprovalMappingDAO;
    }

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        return null;
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[0];
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[0];
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[0];
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return BusinessCardApprovalMappingServlet.class;
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }
        try {
            String actionType = request.getParameter("actionType");
            switch (actionType) {
                case "getApprovalPage":
                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.BUSINESS_CARD_APPROVAL_MAPPING_SEARCH)) {
                        request.getRequestDispatcher("business_card_approval_mapping/business_card_approval_mappingEdit.jsp").forward(request, response);
                        return;
                    }

                case "approvedCard":
                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.BUSINESS_CARD_APPROVAL_MAPPING_SEARCH)) {
                        approve(request, userDTO, true);
                        response.sendRedirect("Business_card_approval_mappingServlet?actionType=search");
                        return;
                    }
                    break;
                case "rejectCard":
                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.BUSINESS_CARD_APPROVAL_MAPPING_SEARCH)) {
                        approve(request, userDTO, false);
                        response.sendRedirect("Business_card_approval_mappingServlet?actionType=search");
                        return;
                    }
                    break;
                case "search": {
                    Map<String, String> extraCriteriaMap = new HashMap<>();
                    extraCriteriaMap.put("employee_records_id_internal", String.valueOf(userDTO.employee_record_id));
                    request.setAttribute(CommonDAOService.CRITERIA_MAP_KEY, extraCriteriaMap);
                    super.doGet(request, response);
                    return;
                }
                default:
                    super.doGet(request, response);
                    return;
            }
        } catch (Exception ex) {
            logger.debug(ex);
            ex.printStackTrace();
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }

        try {
            String actionType = request.getParameter("actionType");
            switch (actionType) {
                case "approved_card":
                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.BUSINESS_CARD_APPROVAL_MAPPING_SEARCH)) {
                        approve(request, userDTO, true);
                        response.sendRedirect("Business_card_approval_mappingServlet?actionType=search");
                        return;
                    }
                case "approvedCard":
                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.BUSINESS_CARD_APPROVAL_MAPPING_SEARCH)) {
                        approve(request, userDTO, true);
                        response.sendRedirect("Business_card_approval_mappingServlet?actionType=search");
                        return;
                    }
                    break;
                case "rejectCard":
                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.BUSINESS_CARD_APPROVAL_MAPPING_SEARCH)) {
                        approve(request, userDTO, false);
                        response.sendRedirect("Business_card_approval_mappingServlet?actionType=search");
                        return;
                    }
                    break;
                default:
                    super.doPost(request, response);
                    return;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error(ex);
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }

    private void approve(HttpServletRequest request, UserDTO userDTO, boolean isAccepted) throws Exception {

        long requesterEmployeeRecordId = userDTO.employee_record_id;
        long cardInfoId = Long.parseLong(request.getParameter("cardInfoId"));
        Business_card_infoDAO businessCardInfoDAO = Business_card_infoDAO.getInstance();
        Business_card_infoDTO card_infoDTO = businessCardInfoDAO.getDTOFromID(cardInfoId);
        businessCardInfoDAO.update(card_infoDTO);

        CreateBusinessCardApprovalModel model = new CreateBusinessCardApprovalModel.CreateBusinessCardApprovalModelBuilder()
                .setTaskTypeId(TaskTypeEnum.BUSINESS_CARD.getValue())
                .setCardInfoDTO(card_infoDTO)
                .setRequesterEmployeeRecordId(requesterEmployeeRecordId)
                .build();
        boolean sendNotificationToUser = false;

        if (isAccepted) {
            CardApprovalResponse response = businessCardApprovalMappingDAO.movedToNextLevelOfApproval(model);
            if (!response.hasNextApproval) {
                card_infoDTO.cardStatusCat = BusinessCardStatusEnum.APPROVED.getValue();
                businessCardInfoDAO.update(card_infoDTO);
                businessCardApprovalMappingDAO.updateCardStatusByBusinessCardInfoId(BusinessCardStatusEnum.APPROVED.getValue(), cardInfoId);
                sendNotificationToUser = true;
            } else {
                List<TaskTypeApprovalPathDTO> nextApprovalPath = TaskTypeApprovalPathRepository.getInstance().getByTaskTypeIdAndLevel(model.getTaskTypeId(), response.nextLevel);
                if (nextApprovalPath == null || nextApprovalPath.size() == 0) {
                    card_infoDTO.cardStatusCat = BusinessCardStatusEnum.APPROVED.getValue();
                    card_infoDTO.modifiedBy = userDTO.employee_record_id;
                    card_infoDTO.lastModificationTime = System.currentTimeMillis();
                    businessCardInfoDAO.update(card_infoDTO);
                    businessCardApprovalMappingDAO.updateCardStatusByBusinessCardInfoId(BusinessCardStatusEnum.APPROVED.getValue(), cardInfoId);
                    sendNotificationToUser = true;
                    BusinessCardApprovalNotification.getInstance().sendApproveNotificationToLastApprover(response.organogramIds, card_infoDTO);
                } else {
                    BusinessCardApprovalNotification.getInstance().sendPendingNotification(response.organogramIds, card_infoDTO);
                }
            }
        } else {
            String rejectReason = request.getParameter("reject_reason");
            if (rejectReason != null) {
                rejectReason = Jsoup.clean(rejectReason, Whitelist.simpleText());
                rejectReason = rejectReason.replace("'", " ");
                rejectReason = rejectReason.replace("\"", " ");
            } else {
                rejectReason = "";
            }
            businessCardApprovalMappingDAO.rejectPendingApproval(model, rejectReason);
            card_infoDTO.cardStatusCat = BusinessCardStatusEnum.REJECTED.getValue();
            card_infoDTO.comment = rejectReason;
            businessCardInfoDAO.update(card_infoDTO);
            businessCardApprovalMappingDAO.updateCardStatusByBusinessCardInfoId(BusinessCardStatusEnum.REJECTED.getValue(), cardInfoId);
            sendNotificationToUser = true;
        }
        if (sendNotificationToUser) {
            EmployeeOfficeDTO employeeOfficeDTO = EmployeeOfficeRepository.getInstance().getByEmployeeRecordIdIsDefault(card_infoDTO.insertedBy);
            if (employeeOfficeDTO != null && employeeOfficeDTO.officeUnitOrganogramId != null) {
                BusinessCardApprovalNotification.getInstance().sendApproveOrRejectNotification(Collections.singletonList(employeeOfficeDTO.officeUnitOrganogramId), card_infoDTO, isAccepted);
            }
        }
    }

}
