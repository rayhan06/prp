package business_card_approval_mapping;

/*
 * @author Md. Erfan Hossain
 * @created 11/05/2021 - 3:26 PM
 * @project parliament
 */

import business_card_info.Business_card_infoDTO;
import card_info.CardEmployeeInfoDTO;
import card_info.CardEmployeeInfoRepository;
import card_info.CardEmployeeOfficeInfoDTO;
import card_info.CardEmployeeOfficeInfoRepository;
import pb_notifications.Pb_notificationsDAO;
import util.StringUtils;

import java.util.List;

public class BusinessCardApprovalNotification {
    private final Pb_notificationsDAO pb_notificationsDAO;

    private BusinessCardApprovalNotification(){
        pb_notificationsDAO = Pb_notificationsDAO.getInstance();
    }

    private static class BusinessCardApprovalNotificationLazyLoader{
        static final BusinessCardApprovalNotification INSTANCE = new BusinessCardApprovalNotification();
    }

    public static BusinessCardApprovalNotification getInstance(){
        return BusinessCardApprovalNotificationLazyLoader.INSTANCE;
    }

    public void sendPendingNotification(List<Long> organogramIds, Business_card_infoDTO cardInfoDTO){
        String cardEngText = "Business";
        String cardBngText = "বিজনেস";
        CardEmployeeInfoDTO cardEmployeeInfoDTO = CardEmployeeInfoRepository.getInstance().getById(cardInfoDTO.cardEmployeeInfoId);
        CardEmployeeOfficeInfoDTO cardEmployeeOfficeInfoDTO = CardEmployeeOfficeInfoRepository.getInstance().getById(cardInfoDTO.cardEmployeeOfficeInfoId);
        String empEngText = cardEmployeeInfoDTO.nameEn + "("+cardEmployeeOfficeInfoDTO.organogramEng+","+cardEmployeeOfficeInfoDTO.officeUnitEng+")s";
        String empBngText = cardEmployeeInfoDTO.nameBn + "("+cardEmployeeOfficeInfoDTO.organogramBng+","+cardEmployeeOfficeInfoDTO.officeUnitBng+") এর";
        String notificationMessage = empEngText+" "+cardEngText+" "+"card-"+cardInfoDTO.iD+" "+"is waiting for your approval.$"
                +empBngText+" "+cardBngText+" "+"কার্ড-"+ StringUtils.convertToBanNumber(String.valueOf(cardInfoDTO.iD))+" "+ "অনুমোদনের অপেক্ষায় আছে।";
        String url = "Business_card_approval_mappingServlet?actionType=getApprovalPage&cardInfoId="+cardInfoDTO.iD;
        sendNotification(organogramIds,notificationMessage,url);
    }

    public void sendApproveOrRejectNotification(List<Long> organogramIds, Business_card_infoDTO cardInfoDTO, boolean isApproved){
        String cardEngText = isApproved ? "You requested business card-"+cardInfoDTO.iD+" has been approved."
                : "You requested business card-"+cardInfoDTO.iD+" has been rejected.";
        String cardBngText = isApproved ? "আপনার অনুরোধ করা বিজনেস কার্ড-"+StringUtils.convertToBanNumber(String.valueOf(cardInfoDTO.iD))+" অনুমোদিত হয়েছে।"
                : "আপনার অনুরোধ করা বিজনেস কার্ড-"+StringUtils.convertToBanNumber(String.valueOf(cardInfoDTO.iD))+" প্রত্যাখ্যান করা হয়েছে।";
        String notificationMessage = cardEngText +"$"+cardBngText;
        String url = "Business_card_infoServlet?actionType=view&ID="+cardInfoDTO.iD;
        sendNotification(organogramIds,notificationMessage,url);
    }

    private void sendNotification(List<Long> organogramIds,String notificationMessage,String url){
        if(organogramIds == null || organogramIds.size() == 0){
            return;
        }
        Thread thread = new Thread(()->{
            long currentTime = System.currentTimeMillis();
            organogramIds.forEach(id-> pb_notificationsDAO.addPb_notifications(id, currentTime, url, notificationMessage,false));
        });
        thread.setDaemon(true);
        thread.start();
    }

    public void sendApproveNotificationToLastApprover(List<Long> organogramIds, Business_card_infoDTO cardInfoDTO){
        String cardEngText = "Approved Business";
        String cardBngText = "অনুমোদিত বিজনেস";
        CardEmployeeInfoDTO cardEmployeeInfoDTO = CardEmployeeInfoRepository.getInstance().getById(cardInfoDTO.cardEmployeeInfoId);
        CardEmployeeOfficeInfoDTO cardEmployeeOfficeInfoDTO = CardEmployeeOfficeInfoRepository.getInstance().getById(cardInfoDTO.cardEmployeeOfficeInfoId);
        String empEngText = cardEmployeeInfoDTO.nameEn + "("+cardEmployeeOfficeInfoDTO.organogramEng+","+cardEmployeeOfficeInfoDTO.officeUnitEng+")s";
        String empBngText = cardEmployeeInfoDTO.nameBn + "("+cardEmployeeOfficeInfoDTO.organogramBng+","+cardEmployeeOfficeInfoDTO.officeUnitBng+") এর";
        String notificationMessage = empEngText+" "+cardEngText+" "+"card-"+cardInfoDTO.iD+" "+"is waiting for next activity.$"
                +empBngText+" "+cardBngText+" "+"কার্ড-"+ StringUtils.convertToBanNumber(String.valueOf(cardInfoDTO.iD))+" "+ "পরবর্তী কার্যক্রমের অপেক্ষায় আছে।";
        String url = "Business_card_approval_mappingServlet?actionType=getApprovalPage&cardInfoId="+cardInfoDTO.iD;
        sendNotification(organogramIds,notificationMessage,url);
    }
}