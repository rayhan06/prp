package vm_requisition_approver;

import java.io.IOException;
import java.io.*;
import java.text.SimpleDateFormat;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import employee_assign.EmployeeSearchModel;
import office_units.Office_unitsRepository;
import offices.OfficesDTO;
import offices.OfficesRepository;
import org.apache.log4j.Logger;

import login.LoginDTO;
import permission.MenuConstants;
import role.PermissionRepository;


import sessionmanager.SessionConstants;

import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;
import util.CommonRequestHandler;
import util.RecordNavigationManager4;

import java.util.*;
import javax.servlet.http.*;
import java.util.UUID;





import geolocation.GeoLocationDAO2;
import java.util.StringTokenizer;

import com.google.gson.Gson;

import pb.*;
import pbReport.*;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;




/**
 * Servlet implementation class Vm_requisition_approverServlet
 */
@WebServlet("/Vm_requisition_approverServlet")
@MultipartConfig
public class Vm_requisition_approverServlet extends HttpServlet
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Vm_requisition_approverServlet.class);

    String tableName = "vm_requisition_approver";

	Vm_requisition_approverDAO vm_requisition_approverDAO;
	CommonRequestHandler commonRequestHandler;
    private final Gson gson = new Gson();

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Vm_requisition_approverServlet()
	{
        super();
    	try
    	{
			vm_requisition_approverDAO = new Vm_requisition_approverDAO(tableName);
			commonRequestHandler = new CommonRequestHandler(vm_requisition_approverDAO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		System.out.println("In doget request = " + request);
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		try
		{
			String actionType = request.getParameter("actionType");
			if(actionType.equals("getAddPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_REQUISITION_APPROVER_ADD))
				{
					commonRequestHandler.getAddPage(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getEditPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_REQUISITION_APPROVER_UPDATE))
				{
					getVm_requisition_approver(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getURL"))
			{
				String URL = request.getParameter("URL");
				System.out.println("URL = " + URL);
				response.sendRedirect(URL);
			}
			else if(actionType.equals("search"))
			{
				System.out.println("search requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_REQUISITION_APPROVER_SEARCH))
				{
					if(isPermanentTable)
					{
						String filter = request.getParameter("filter");
						System.out.println("filter = " + filter);
						if(filter!=null)
						{
							filter = ""; //shouldn't be directly used, rather manipulate it.
							searchVm_requisition_approver(request, response, isPermanentTable, filter);
						}
						else
						{
							searchVm_requisition_approver(request, response, isPermanentTable, "");
						}
					}
					else
					{
						//searchVm_requisition_approver(request, response, tempTableName, isPermanentTable);
					}
				}
			}
			else if(actionType.equals("view"))
			{
				System.out.println("view requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_REQUISITION_APPROVER_SEARCH))
				{
					commonRequestHandler.view(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}
			else
			{
				request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}





	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		// TODO Auto-generated method stub
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		System.out.println("doPost");
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);

		try
		{
			String actionType = request.getParameter("actionType");
			System.out.println("actionType = " + actionType);
			if(actionType.equals("add"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_REQUISITION_APPROVER_ADD))
				{
					System.out.println("going to  addVm_requisition_approver ");
					addVm_requisition_approver(request, response, true, userDTO, true);
				}
				else
				{
					System.out.println("Not going to  addVm_requisition_approver ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}

			else if(actionType.equals("getDTO"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_REQUISITION_APPROVER_ADD))
				{
					getDTO(request, response);
				}
				else
				{
					System.out.println("Not going to  addVm_requisition_approver ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}
			else if(actionType.equals("edit"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_REQUISITION_APPROVER_UPDATE))
				{
					addVm_requisition_approver(request, response, false, userDTO, isPermanentTable);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("delete"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_REQUISITION_APPROVER_UPDATE))
				{
					commonRequestHandler.delete(request, response, userDTO);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("search"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_REQUISITION_APPROVER_SEARCH))
				{
					searchVm_requisition_approver(request, response, true, "");
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getGeo"))
			{
				System.out.println("going to geoloc ");
				request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
			}

		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}

	private void getDTO(HttpServletRequest request, HttpServletResponse response)
	{
		try
		{
			System.out.println("In getDTO");
			Vm_requisition_approverDTO vm_requisition_approverDTO = Vm_requisition_approverRepository.getInstance().getVm_requisition_approverDTOByID(Long.parseLong(request.getParameter("ID")));
			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");

			String encoded = this.gson.toJson(vm_requisition_approverDTO);
			System.out.println("json encoded data = " + encoded);
			out.print(encoded);
			out.flush();
		}
		catch (NumberFormatException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void addVm_requisition_approver(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, boolean isPermanentTable) throws IOException
	{
		// TODO Auto-generated method stub
		try
		{
			request.setAttribute("failureMessage", "");
			System.out.println("%%%% addVm_requisition_approver");
			String path = getServletContext().getRealPath("/img2/");
			final Vm_requisition_approverDTO[] vm_requisition_approverDTO = new Vm_requisition_approverDTO[1];
			SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");

			if(addFlag == true)
			{
				vm_requisition_approverDTO[0] = new Vm_requisition_approverDTO();
			}
			else
			{
				vm_requisition_approverDTO[0] = Vm_requisition_approverRepository.getInstance().getVm_requisition_approverDTOByID(Long.parseLong(request.getParameter("iD")));
			}

			String Value = "";

			if(addFlag)
			{
				vm_requisition_approverDTO[0].insertedByUserId = userDTO.ID;
			}


			if(addFlag)
			{
				vm_requisition_approverDTO[0].insertedByOrganogramId = userDTO.organogramID;
			}


			if(addFlag)
			{
				Calendar c = Calendar.getInstance();
				c.set(Calendar.HOUR_OF_DAY, 0);
				c.set(Calendar.MINUTE, 0);
				c.set(Calendar.SECOND, 0);
				c.set(Calendar.MILLISECOND, 0);

				vm_requisition_approverDTO[0].insertionDate = c.getTimeInMillis();
			}


			Value = request.getParameter("searchColumn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("searchColumn = " + Value);
			if(Value != null)
			{
				vm_requisition_approverDTO[0].searchColumn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("approverOrgId");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("approverOrgId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				vm_requisition_approverDTO[0].approverOrgId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("approverOfficeId");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("approverOfficeId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				vm_requisition_approverDTO[0].approverOfficeId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("approverOfficeUnitId");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("approverOfficeUnitId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				vm_requisition_approverDTO[0].approverOfficeUnitId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			

			Value = request.getParameter("approverPhoneNum");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("approverPhoneNum = " + Value);
			if(Value != null)
			{
				vm_requisition_approverDTO[0].approverPhoneNum = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("approverNameEn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("approverNameEn = " + Value);
			if(Value != null)
			{
				vm_requisition_approverDTO[0].approverNameEn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("approverNameBn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("approverNameBn = " + Value);
			if(Value != null)
			{
				vm_requisition_approverDTO[0].approverNameBn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("approverOfficeNameEn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("approverOfficeNameEn = " + Value);
			if(Value != null)
			{
				vm_requisition_approverDTO[0].approverOfficeNameEn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("approverOfficeNameBn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("approverOfficeNameBn = " + Value);
			if(Value != null)
			{
				vm_requisition_approverDTO[0].approverOfficeNameBn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("approverOfficeUnitNameEn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("approverOfficeUnitNameEn = " + Value);
			if(Value != null)
			{
				vm_requisition_approverDTO[0].approverOfficeUnitNameEn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("approverOfficeUnitNameBn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("approverOfficeUnitNameBn = " + Value);
			if(Value != null)
			{
				vm_requisition_approverDTO[0].approverOfficeUnitNameBn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("approverOfficeUnitOrgNameEn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("approverOfficeUnitOrgNameEn = " + Value);
			if(Value != null)
			{
				vm_requisition_approverDTO[0].approverOfficeUnitOrgNameEn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("approverOfficeUnitOrgNameBn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("approverOfficeUnitOrgNameBn = " + Value);
			if(Value != null)
			{
				vm_requisition_approverDTO[0].approverOfficeUnitOrgNameBn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}


			vm_requisition_approverDAO.deleteAll();

			long[] returnedID = {-1};

			Value = request.getParameter("approverEmpId");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("approverEmpId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				EmployeeSearchModel[] models = gson.fromJson(Value, EmployeeSearchModel[].class);

				Utils.handleTransactionForNavigationService4(()->{
					for(EmployeeSearchModel model: models){

						vm_requisition_approverDTO[0] = new Vm_requisition_approverDTO();
						vm_requisition_approverDTO[0].approverEmpId = model.employeeRecordId;
						vm_requisition_approverDTO[0].approverOfficeUnitId = model.officeUnitId;
						vm_requisition_approverDTO[0].approverOrgId = model.organogramId;

						vm_requisition_approverDTO[0].approverNameEn = model.employeeNameEn;
						vm_requisition_approverDTO[0].approverNameBn = model.employeeNameBn;
						vm_requisition_approverDTO[0].approverOfficeUnitNameEn = model.officeUnitNameEn;
						vm_requisition_approverDTO[0].approverOfficeUnitNameBn = model.officeUnitNameBn;
						vm_requisition_approverDTO[0].approverOfficeUnitOrgNameEn = model.organogramNameEn;
						vm_requisition_approverDTO[0].approverOfficeUnitOrgNameBn = model.organogramNameBn;

						vm_requisition_approverDTO[0].approverPhoneNum = model.phoneNumber;

						OfficesDTO approverOffice = OfficesRepository.getInstance().getOfficesDTOByID(vm_requisition_approverDTO[0].approverOfficeId);
						vm_requisition_approverDTO[0].approverOfficeId = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(model.officeUnitId).officeId;

						if (approverOffice != null) {
							vm_requisition_approverDTO[0].approverOfficeNameEn = approverOffice.officeNameEng;
							vm_requisition_approverDTO[0].approverOfficeNameBn = approverOffice.officeNameBng;
						}

						String filter = " approver_emp_id = '" + vm_requisition_approverDTO[0].approverEmpId
								+ "' AND approver_org_id = '" + vm_requisition_approverDTO[0].approverOrgId + "' ";
						long existingCount = vm_requisition_approverDAO.getCount(filter, userDTO);

						if (existingCount > 0) continue;

						if(isPermanentTable == false) //add new row for validation and make the old row outdated
						{
							vm_requisition_approverDAO.setIsDeleted(vm_requisition_approverDTO[0].iD, CommonDTO.OUTDATED);
							returnedID[0] = vm_requisition_approverDAO.add(vm_requisition_approverDTO[0]);
							vm_requisition_approverDAO.setIsDeleted(returnedID[0], CommonDTO.WAITING_FOR_APPROVAL);
						}
						else if(addFlag == true)
						{
							returnedID[0] = vm_requisition_approverDAO.manageWriteOperations(vm_requisition_approverDTO[0], SessionConstants.INSERT, -1, userDTO);
						}
						else
						{
							returnedID[0] = vm_requisition_approverDAO.manageWriteOperations(vm_requisition_approverDTO[0], SessionConstants.UPDATE, -1, userDTO);
						}

					}
				});

			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			
			
			System.out.println("Done adding  addVm_requisition_approver dto = " + vm_requisition_approverDTO[0]);









			if(isPermanentTable)
			{
				String inPlaceSubmit = request.getParameter("inplacesubmit");

				if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
				{
					getVm_requisition_approver(request, response, returnedID[0]);
				}
				else
				{
					response.sendRedirect("Vm_requisition_approverServlet?actionType=getAddPage");
				}
			}
			else
			{
				commonRequestHandler.validate(Vm_requisition_approverRepository.getInstance().getVm_requisition_approverDTOByID(returnedID[0]), request, response, userDTO);
			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}









	private void getVm_requisition_approver(HttpServletRequest request, HttpServletResponse response, long id) throws ServletException, IOException
	{
		System.out.println("in getVm_requisition_approver");
		Vm_requisition_approverDTO vm_requisition_approverDTO = null;
		try
		{
			vm_requisition_approverDTO = Vm_requisition_approverRepository.getInstance().getVm_requisition_approverDTOByID(id);
			request.setAttribute("ID", vm_requisition_approverDTO.iD);
			request.setAttribute("vm_requisition_approverDTO",vm_requisition_approverDTO);
			request.setAttribute("vm_requisition_approverDAO",vm_requisition_approverDAO);

			String URL= "";

			String inPlaceEdit = request.getParameter("inplaceedit");
			String inPlaceSubmit = request.getParameter("inplacesubmit");
			String getBodyOnly = request.getParameter("getBodyOnly");

			if(inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase(""))
			{
				URL = "vm_requisition_approver/vm_requisition_approverInPlaceEdit.jsp";
				request.setAttribute("inplaceedit","");
			}
			else if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
			{
				URL = "vm_requisition_approver/vm_requisition_approverSearchRow.jsp";
				request.setAttribute("inplacesubmit","");
			}
			else
			{
				if(getBodyOnly  != null && !getBodyOnly.equalsIgnoreCase(""))
				{
					URL = "vm_requisition_approver/vm_requisition_approverEditBody.jsp?actionType=edit";
				}
				else
				{
					URL = "vm_requisition_approver/vm_requisition_approverEdit.jsp?actionType=edit";
				}
			}

			RequestDispatcher rd = request.getRequestDispatcher(URL);
			rd.forward(request, response);
		}
		catch (NumberFormatException e)
		{
			e.printStackTrace();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}


	private void getVm_requisition_approver(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		getVm_requisition_approver(request, response, Long.parseLong(request.getParameter("ID")));
	}

	private void searchVm_requisition_approver(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException
	{
		System.out.println("in  searchVm_requisition_approver 1");
		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String ajax = request.getParameter("ajax");
		boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);

        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
			SessionConstants.NAV_VM_REQUISITION_APPROVER,
			request,
			vm_requisition_approverDAO,
			SessionConstants.VIEW_VM_REQUISITION_APPROVER,
			SessionConstants.SEARCH_VM_REQUISITION_APPROVER,
			tableName,
			isPermanent,
			userDTO,
			filter,
			true);
        try
        {
			System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        }
        catch(Exception e)
        {
			System.out.println("failed to dojob" + e);
        }

		request.setAttribute("vm_requisition_approverDAO",vm_requisition_approverDAO);
        RequestDispatcher rd;
        if(!isPermanent)
        {
        	if(hasAjax == false)
	        {
	        	System.out.println("Going to vm_requisition_approver/vm_requisition_approverApproval.jsp");
	        	rd = request.getRequestDispatcher("vm_requisition_approver/vm_requisition_approverApproval.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to vm_requisition_approver/vm_requisition_approverApprovalForm.jsp");
	        	rd = request.getRequestDispatcher("vm_requisition_approver/vm_requisition_approverApprovalForm.jsp");
	        }
        }
        else
        {
	        if(hasAjax == false)
	        {
	        	System.out.println("Going to vm_requisition_approver/vm_requisition_approverSearch.jsp");
	        	rd = request.getRequestDispatcher("vm_requisition_approver/vm_requisition_approverSearch.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to vm_requisition_approver/vm_requisition_approverSearchForm.jsp");
	        	rd = request.getRequestDispatcher("vm_requisition_approver/vm_requisition_approverSearchForm.jsp");
	        }
        }
		rd.forward(request, response);
	}

}

