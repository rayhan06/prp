package vm_requisition_approver;
import java.util.*; 
import util.*;


public class Vm_requisition_approverMAPS extends CommonMaps
{	
	public Vm_requisition_approverMAPS(String tableName)
	{
		

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("insertedByUserId".toLowerCase(), "insertedByUserId".toLowerCase());
		java_DTO_map.put("insertedByOrganogramId".toLowerCase(), "insertedByOrganogramId".toLowerCase());
		java_DTO_map.put("insertionDate".toLowerCase(), "insertionDate".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
		java_DTO_map.put("searchColumn".toLowerCase(), "searchColumn".toLowerCase());
		java_DTO_map.put("approverOrgId".toLowerCase(), "approverOrgId".toLowerCase());
		java_DTO_map.put("approverOfficeId".toLowerCase(), "approverOfficeId".toLowerCase());
		java_DTO_map.put("approverOfficeUnitId".toLowerCase(), "approverOfficeUnitId".toLowerCase());
		java_DTO_map.put("approverEmpId".toLowerCase(), "approverEmpId".toLowerCase());
		java_DTO_map.put("approverPhoneNum".toLowerCase(), "approverPhoneNum".toLowerCase());
		java_DTO_map.put("approverNameEn".toLowerCase(), "approverNameEn".toLowerCase());
		java_DTO_map.put("approverNameBn".toLowerCase(), "approverNameBn".toLowerCase());
		java_DTO_map.put("approverOfficeNameEn".toLowerCase(), "approverOfficeNameEn".toLowerCase());
		java_DTO_map.put("approverOfficeNameBn".toLowerCase(), "approverOfficeNameBn".toLowerCase());
		java_DTO_map.put("approverOfficeUnitNameEn".toLowerCase(), "approverOfficeUnitNameEn".toLowerCase());
		java_DTO_map.put("approverOfficeUnitNameBn".toLowerCase(), "approverOfficeUnitNameBn".toLowerCase());
		java_DTO_map.put("approverOfficeUnitOrgNameEn".toLowerCase(), "approverOfficeUnitOrgNameEn".toLowerCase());
		java_DTO_map.put("approverOfficeUnitOrgNameBn".toLowerCase(), "approverOfficeUnitOrgNameBn".toLowerCase());

		java_SQL_map.put("approver_org_id".toLowerCase(), "approverOrgId".toLowerCase());
		java_SQL_map.put("approver_office_id".toLowerCase(), "approverOfficeId".toLowerCase());
		java_SQL_map.put("approver_office_unit_id".toLowerCase(), "approverOfficeUnitId".toLowerCase());
		java_SQL_map.put("approver_emp_id".toLowerCase(), "approverEmpId".toLowerCase());
		java_SQL_map.put("approver_phone_num".toLowerCase(), "approverPhoneNum".toLowerCase());
		java_SQL_map.put("approver_name_en".toLowerCase(), "approverNameEn".toLowerCase());
		java_SQL_map.put("approver_name_bn".toLowerCase(), "approverNameBn".toLowerCase());
		java_SQL_map.put("approver_office_name_en".toLowerCase(), "approverOfficeNameEn".toLowerCase());
		java_SQL_map.put("approver_office_name_bn".toLowerCase(), "approverOfficeNameBn".toLowerCase());
		java_SQL_map.put("approver_office_unit_name_en".toLowerCase(), "approverOfficeUnitNameEn".toLowerCase());
		java_SQL_map.put("approver_office_unit_name_bn".toLowerCase(), "approverOfficeUnitNameBn".toLowerCase());
		java_SQL_map.put("approver_office_unit_org_name_en".toLowerCase(), "approverOfficeUnitOrgNameEn".toLowerCase());
		java_SQL_map.put("approver_office_unit_org_name_bn".toLowerCase(), "approverOfficeUnitOrgNameBn".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Inserted By User Id".toLowerCase(), "insertedByUserId".toLowerCase());
		java_Text_map.put("Inserted By Organogram Id".toLowerCase(), "insertedByOrganogramId".toLowerCase());
		java_Text_map.put("Insertion Date".toLowerCase(), "insertionDate".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
		java_Text_map.put("Search Column".toLowerCase(), "searchColumn".toLowerCase());
		java_Text_map.put("Approver Org Id".toLowerCase(), "approverOrgId".toLowerCase());
		java_Text_map.put("Approver Office Id".toLowerCase(), "approverOfficeId".toLowerCase());
		java_Text_map.put("Approver Office Unit Id".toLowerCase(), "approverOfficeUnitId".toLowerCase());
		java_Text_map.put("Approver Emp Id".toLowerCase(), "approverEmpId".toLowerCase());
		java_Text_map.put("Approver Phone Num".toLowerCase(), "approverPhoneNum".toLowerCase());
		java_Text_map.put("Approver Name En".toLowerCase(), "approverNameEn".toLowerCase());
		java_Text_map.put("Approver Name Bn".toLowerCase(), "approverNameBn".toLowerCase());
		java_Text_map.put("Approver Office Name En".toLowerCase(), "approverOfficeNameEn".toLowerCase());
		java_Text_map.put("Approver Office Name Bn".toLowerCase(), "approverOfficeNameBn".toLowerCase());
		java_Text_map.put("Approver Office Unit Name En".toLowerCase(), "approverOfficeUnitNameEn".toLowerCase());
		java_Text_map.put("Approver Office Unit Name Bn".toLowerCase(), "approverOfficeUnitNameBn".toLowerCase());
		java_Text_map.put("Approver Office Unit Org Name En".toLowerCase(), "approverOfficeUnitOrgNameEn".toLowerCase());
		java_Text_map.put("Approver Office Unit Org Name Bn".toLowerCase(), "approverOfficeUnitOrgNameBn".toLowerCase());
			
	}

}