package vm_requisition_approver;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import com.google.gson.Gson;
import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class Vm_requisition_approverRepository implements Repository {
	Vm_requisition_approverDAO vm_requisition_approverDAO = null;
	Gson gson;

	public void setDAO(Vm_requisition_approverDAO vm_requisition_approverDAO)
	{
		this.vm_requisition_approverDAO = vm_requisition_approverDAO;
		gson = new Gson();
	}
	
	
	static Logger logger = Logger.getLogger(Vm_requisition_approverRepository.class);
	Map<Long, Vm_requisition_approverDTO>mapOfVm_requisition_approverDTOToiD;
//	Map<Long, Set<Vm_requisition_approverDTO> >mapOfVm_requisition_approverDTOToinsertedByUserId;
//	Map<Long, Set<Vm_requisition_approverDTO> >mapOfVm_requisition_approverDTOToinsertedByOrganogramId;
//	Map<Long, Set<Vm_requisition_approverDTO> >mapOfVm_requisition_approverDTOToinsertionDate;
//	Map<Long, Set<Vm_requisition_approverDTO> >mapOfVm_requisition_approverDTOTolastModificationTime;
//	Map<String, Set<Vm_requisition_approverDTO> >mapOfVm_requisition_approverDTOTosearchColumn;
	Map<Long, Set<Vm_requisition_approverDTO> >mapOfVm_requisition_approverDTOToapproverOrgId;
//	Map<Long, Set<Vm_requisition_approverDTO> >mapOfVm_requisition_approverDTOToapproverOfficeId;
//	Map<Long, Set<Vm_requisition_approverDTO> >mapOfVm_requisition_approverDTOToapproverOfficeUnitId;
//	Map<Long, Set<Vm_requisition_approverDTO> >mapOfVm_requisition_approverDTOToapproverEmpId;
//	Map<String, Set<Vm_requisition_approverDTO> >mapOfVm_requisition_approverDTOToapproverPhoneNum;
//	Map<String, Set<Vm_requisition_approverDTO> >mapOfVm_requisition_approverDTOToapproverNameEn;
//	Map<String, Set<Vm_requisition_approverDTO> >mapOfVm_requisition_approverDTOToapproverNameBn;
//	Map<String, Set<Vm_requisition_approverDTO> >mapOfVm_requisition_approverDTOToapproverOfficeNameEn;
//	Map<String, Set<Vm_requisition_approverDTO> >mapOfVm_requisition_approverDTOToapproverOfficeNameBn;
//	Map<String, Set<Vm_requisition_approverDTO> >mapOfVm_requisition_approverDTOToapproverOfficeUnitNameEn;
//	Map<String, Set<Vm_requisition_approverDTO> >mapOfVm_requisition_approverDTOToapproverOfficeUnitNameBn;
//	Map<String, Set<Vm_requisition_approverDTO> >mapOfVm_requisition_approverDTOToapproverOfficeUnitOrgNameEn;
//	Map<String, Set<Vm_requisition_approverDTO> >mapOfVm_requisition_approverDTOToapproverOfficeUnitOrgNameBn;


	static Vm_requisition_approverRepository instance = null;  
	private Vm_requisition_approverRepository(){
		mapOfVm_requisition_approverDTOToiD = new ConcurrentHashMap<>();
//		mapOfVm_requisition_approverDTOToinsertedByUserId = new ConcurrentHashMap<>();
//		mapOfVm_requisition_approverDTOToinsertedByOrganogramId = new ConcurrentHashMap<>();
//		mapOfVm_requisition_approverDTOToinsertionDate = new ConcurrentHashMap<>();
//		mapOfVm_requisition_approverDTOTolastModificationTime = new ConcurrentHashMap<>();
//		mapOfVm_requisition_approverDTOTosearchColumn = new ConcurrentHashMap<>();
		mapOfVm_requisition_approverDTOToapproverOrgId = new ConcurrentHashMap<>();
//		mapOfVm_requisition_approverDTOToapproverOfficeId = new ConcurrentHashMap<>();
//		mapOfVm_requisition_approverDTOToapproverOfficeUnitId = new ConcurrentHashMap<>();
//		mapOfVm_requisition_approverDTOToapproverEmpId = new ConcurrentHashMap<>();
//		mapOfVm_requisition_approverDTOToapproverPhoneNum = new ConcurrentHashMap<>();
//		mapOfVm_requisition_approverDTOToapproverNameEn = new ConcurrentHashMap<>();
//		mapOfVm_requisition_approverDTOToapproverNameBn = new ConcurrentHashMap<>();
//		mapOfVm_requisition_approverDTOToapproverOfficeNameEn = new ConcurrentHashMap<>();
//		mapOfVm_requisition_approverDTOToapproverOfficeNameBn = new ConcurrentHashMap<>();
//		mapOfVm_requisition_approverDTOToapproverOfficeUnitNameEn = new ConcurrentHashMap<>();
//		mapOfVm_requisition_approverDTOToapproverOfficeUnitNameBn = new ConcurrentHashMap<>();
//		mapOfVm_requisition_approverDTOToapproverOfficeUnitOrgNameEn = new ConcurrentHashMap<>();
//		mapOfVm_requisition_approverDTOToapproverOfficeUnitOrgNameBn = new ConcurrentHashMap<>();

		setDAO(new Vm_requisition_approverDAO());

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Vm_requisition_approverRepository getInstance(){
		if (instance == null){
			instance = new Vm_requisition_approverRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(vm_requisition_approverDAO == null)
		{
			return;
		}
		try {
			List<Vm_requisition_approverDTO> vm_requisition_approverDTOs = vm_requisition_approverDAO.getAllVm_requisition_approver(reloadAll);
			for(Vm_requisition_approverDTO vm_requisition_approverDTO : vm_requisition_approverDTOs) {
				Vm_requisition_approverDTO oldVm_requisition_approverDTO = getVm_requisition_approverDTOByIDWithoutClone(vm_requisition_approverDTO.iD);
				if( oldVm_requisition_approverDTO != null ) {
					mapOfVm_requisition_approverDTOToiD.remove(oldVm_requisition_approverDTO.iD);
				
//					if(mapOfVm_requisition_approverDTOToinsertedByUserId.containsKey(oldVm_requisition_approverDTO.insertedByUserId)) {
//						mapOfVm_requisition_approverDTOToinsertedByUserId.get(oldVm_requisition_approverDTO.insertedByUserId).remove(oldVm_requisition_approverDTO);
//					}
//					if(mapOfVm_requisition_approverDTOToinsertedByUserId.get(oldVm_requisition_approverDTO.insertedByUserId).isEmpty()) {
//						mapOfVm_requisition_approverDTOToinsertedByUserId.remove(oldVm_requisition_approverDTO.insertedByUserId);
//					}
//
//					if(mapOfVm_requisition_approverDTOToinsertedByOrganogramId.containsKey(oldVm_requisition_approverDTO.insertedByOrganogramId)) {
//						mapOfVm_requisition_approverDTOToinsertedByOrganogramId.get(oldVm_requisition_approverDTO.insertedByOrganogramId).remove(oldVm_requisition_approverDTO);
//					}
//					if(mapOfVm_requisition_approverDTOToinsertedByOrganogramId.get(oldVm_requisition_approverDTO.insertedByOrganogramId).isEmpty()) {
//						mapOfVm_requisition_approverDTOToinsertedByOrganogramId.remove(oldVm_requisition_approverDTO.insertedByOrganogramId);
//					}
//
//					if(mapOfVm_requisition_approverDTOToinsertionDate.containsKey(oldVm_requisition_approverDTO.insertionDate)) {
//						mapOfVm_requisition_approverDTOToinsertionDate.get(oldVm_requisition_approverDTO.insertionDate).remove(oldVm_requisition_approverDTO);
//					}
//					if(mapOfVm_requisition_approverDTOToinsertionDate.get(oldVm_requisition_approverDTO.insertionDate).isEmpty()) {
//						mapOfVm_requisition_approverDTOToinsertionDate.remove(oldVm_requisition_approverDTO.insertionDate);
//					}
//
//					if(mapOfVm_requisition_approverDTOTolastModificationTime.containsKey(oldVm_requisition_approverDTO.lastModificationTime)) {
//						mapOfVm_requisition_approverDTOTolastModificationTime.get(oldVm_requisition_approverDTO.lastModificationTime).remove(oldVm_requisition_approverDTO);
//					}
//					if(mapOfVm_requisition_approverDTOTolastModificationTime.get(oldVm_requisition_approverDTO.lastModificationTime).isEmpty()) {
//						mapOfVm_requisition_approverDTOTolastModificationTime.remove(oldVm_requisition_approverDTO.lastModificationTime);
//					}
//
//					if(mapOfVm_requisition_approverDTOTosearchColumn.containsKey(oldVm_requisition_approverDTO.searchColumn)) {
//						mapOfVm_requisition_approverDTOTosearchColumn.get(oldVm_requisition_approverDTO.searchColumn).remove(oldVm_requisition_approverDTO);
//					}
//					if(mapOfVm_requisition_approverDTOTosearchColumn.get(oldVm_requisition_approverDTO.searchColumn).isEmpty()) {
//						mapOfVm_requisition_approverDTOTosearchColumn.remove(oldVm_requisition_approverDTO.searchColumn);
//					}
					
					if(mapOfVm_requisition_approverDTOToapproverOrgId.containsKey(oldVm_requisition_approverDTO.approverOrgId)) {
						mapOfVm_requisition_approverDTOToapproverOrgId.get(oldVm_requisition_approverDTO.approverOrgId).remove(oldVm_requisition_approverDTO);
					}
					if(mapOfVm_requisition_approverDTOToapproverOrgId.get(oldVm_requisition_approverDTO.approverOrgId).isEmpty()) {
						mapOfVm_requisition_approverDTOToapproverOrgId.remove(oldVm_requisition_approverDTO.approverOrgId);
					}
					
//					if(mapOfVm_requisition_approverDTOToapproverOfficeId.containsKey(oldVm_requisition_approverDTO.approverOfficeId)) {
//						mapOfVm_requisition_approverDTOToapproverOfficeId.get(oldVm_requisition_approverDTO.approverOfficeId).remove(oldVm_requisition_approverDTO);
//					}
//					if(mapOfVm_requisition_approverDTOToapproverOfficeId.get(oldVm_requisition_approverDTO.approverOfficeId).isEmpty()) {
//						mapOfVm_requisition_approverDTOToapproverOfficeId.remove(oldVm_requisition_approverDTO.approverOfficeId);
//					}
//
//					if(mapOfVm_requisition_approverDTOToapproverOfficeUnitId.containsKey(oldVm_requisition_approverDTO.approverOfficeUnitId)) {
//						mapOfVm_requisition_approverDTOToapproverOfficeUnitId.get(oldVm_requisition_approverDTO.approverOfficeUnitId).remove(oldVm_requisition_approverDTO);
//					}
//					if(mapOfVm_requisition_approverDTOToapproverOfficeUnitId.get(oldVm_requisition_approverDTO.approverOfficeUnitId).isEmpty()) {
//						mapOfVm_requisition_approverDTOToapproverOfficeUnitId.remove(oldVm_requisition_approverDTO.approverOfficeUnitId);
//					}
//
//					if(mapOfVm_requisition_approverDTOToapproverEmpId.containsKey(oldVm_requisition_approverDTO.approverEmpId)) {
//						mapOfVm_requisition_approverDTOToapproverEmpId.get(oldVm_requisition_approverDTO.approverEmpId).remove(oldVm_requisition_approverDTO);
//					}
//					if(mapOfVm_requisition_approverDTOToapproverEmpId.get(oldVm_requisition_approverDTO.approverEmpId).isEmpty()) {
//						mapOfVm_requisition_approverDTOToapproverEmpId.remove(oldVm_requisition_approverDTO.approverEmpId);
//					}
//
//					if(mapOfVm_requisition_approverDTOToapproverPhoneNum.containsKey(oldVm_requisition_approverDTO.approverPhoneNum)) {
//						mapOfVm_requisition_approverDTOToapproverPhoneNum.get(oldVm_requisition_approverDTO.approverPhoneNum).remove(oldVm_requisition_approverDTO);
//					}
//					if(mapOfVm_requisition_approverDTOToapproverPhoneNum.get(oldVm_requisition_approverDTO.approverPhoneNum).isEmpty()) {
//						mapOfVm_requisition_approverDTOToapproverPhoneNum.remove(oldVm_requisition_approverDTO.approverPhoneNum);
//					}
//
//					if(mapOfVm_requisition_approverDTOToapproverNameEn.containsKey(oldVm_requisition_approverDTO.approverNameEn)) {
//						mapOfVm_requisition_approverDTOToapproverNameEn.get(oldVm_requisition_approverDTO.approverNameEn).remove(oldVm_requisition_approverDTO);
//					}
//					if(mapOfVm_requisition_approverDTOToapproverNameEn.get(oldVm_requisition_approverDTO.approverNameEn).isEmpty()) {
//						mapOfVm_requisition_approverDTOToapproverNameEn.remove(oldVm_requisition_approverDTO.approverNameEn);
//					}
//
//					if(mapOfVm_requisition_approverDTOToapproverNameBn.containsKey(oldVm_requisition_approverDTO.approverNameBn)) {
//						mapOfVm_requisition_approverDTOToapproverNameBn.get(oldVm_requisition_approverDTO.approverNameBn).remove(oldVm_requisition_approverDTO);
//					}
//					if(mapOfVm_requisition_approverDTOToapproverNameBn.get(oldVm_requisition_approverDTO.approverNameBn).isEmpty()) {
//						mapOfVm_requisition_approverDTOToapproverNameBn.remove(oldVm_requisition_approverDTO.approverNameBn);
//					}
//
//					if(mapOfVm_requisition_approverDTOToapproverOfficeNameEn.containsKey(oldVm_requisition_approverDTO.approverOfficeNameEn)) {
//						mapOfVm_requisition_approverDTOToapproverOfficeNameEn.get(oldVm_requisition_approverDTO.approverOfficeNameEn).remove(oldVm_requisition_approverDTO);
//					}
//					if(mapOfVm_requisition_approverDTOToapproverOfficeNameEn.get(oldVm_requisition_approverDTO.approverOfficeNameEn).isEmpty()) {
//						mapOfVm_requisition_approverDTOToapproverOfficeNameEn.remove(oldVm_requisition_approverDTO.approverOfficeNameEn);
//					}
//
//					if(mapOfVm_requisition_approverDTOToapproverOfficeNameBn.containsKey(oldVm_requisition_approverDTO.approverOfficeNameBn)) {
//						mapOfVm_requisition_approverDTOToapproverOfficeNameBn.get(oldVm_requisition_approverDTO.approverOfficeNameBn).remove(oldVm_requisition_approverDTO);
//					}
//					if(mapOfVm_requisition_approverDTOToapproverOfficeNameBn.get(oldVm_requisition_approverDTO.approverOfficeNameBn).isEmpty()) {
//						mapOfVm_requisition_approverDTOToapproverOfficeNameBn.remove(oldVm_requisition_approverDTO.approverOfficeNameBn);
//					}
//
//					if(mapOfVm_requisition_approverDTOToapproverOfficeUnitNameEn.containsKey(oldVm_requisition_approverDTO.approverOfficeUnitNameEn)) {
//						mapOfVm_requisition_approverDTOToapproverOfficeUnitNameEn.get(oldVm_requisition_approverDTO.approverOfficeUnitNameEn).remove(oldVm_requisition_approverDTO);
//					}
//					if(mapOfVm_requisition_approverDTOToapproverOfficeUnitNameEn.get(oldVm_requisition_approverDTO.approverOfficeUnitNameEn).isEmpty()) {
//						mapOfVm_requisition_approverDTOToapproverOfficeUnitNameEn.remove(oldVm_requisition_approverDTO.approverOfficeUnitNameEn);
//					}
//
//					if(mapOfVm_requisition_approverDTOToapproverOfficeUnitNameBn.containsKey(oldVm_requisition_approverDTO.approverOfficeUnitNameBn)) {
//						mapOfVm_requisition_approverDTOToapproverOfficeUnitNameBn.get(oldVm_requisition_approverDTO.approverOfficeUnitNameBn).remove(oldVm_requisition_approverDTO);
//					}
//					if(mapOfVm_requisition_approverDTOToapproverOfficeUnitNameBn.get(oldVm_requisition_approverDTO.approverOfficeUnitNameBn).isEmpty()) {
//						mapOfVm_requisition_approverDTOToapproverOfficeUnitNameBn.remove(oldVm_requisition_approverDTO.approverOfficeUnitNameBn);
//					}
//
//					if(mapOfVm_requisition_approverDTOToapproverOfficeUnitOrgNameEn.containsKey(oldVm_requisition_approverDTO.approverOfficeUnitOrgNameEn)) {
//						mapOfVm_requisition_approverDTOToapproverOfficeUnitOrgNameEn.get(oldVm_requisition_approverDTO.approverOfficeUnitOrgNameEn).remove(oldVm_requisition_approverDTO);
//					}
//					if(mapOfVm_requisition_approverDTOToapproverOfficeUnitOrgNameEn.get(oldVm_requisition_approverDTO.approverOfficeUnitOrgNameEn).isEmpty()) {
//						mapOfVm_requisition_approverDTOToapproverOfficeUnitOrgNameEn.remove(oldVm_requisition_approverDTO.approverOfficeUnitOrgNameEn);
//					}
//
//					if(mapOfVm_requisition_approverDTOToapproverOfficeUnitOrgNameBn.containsKey(oldVm_requisition_approverDTO.approverOfficeUnitOrgNameBn)) {
//						mapOfVm_requisition_approverDTOToapproverOfficeUnitOrgNameBn.get(oldVm_requisition_approverDTO.approverOfficeUnitOrgNameBn).remove(oldVm_requisition_approverDTO);
//					}
//					if(mapOfVm_requisition_approverDTOToapproverOfficeUnitOrgNameBn.get(oldVm_requisition_approverDTO.approverOfficeUnitOrgNameBn).isEmpty()) {
//						mapOfVm_requisition_approverDTOToapproverOfficeUnitOrgNameBn.remove(oldVm_requisition_approverDTO.approverOfficeUnitOrgNameBn);
//					}
					
					
				}
				if(vm_requisition_approverDTO.isDeleted == 0) 
				{
					
					mapOfVm_requisition_approverDTOToiD.put(vm_requisition_approverDTO.iD, vm_requisition_approverDTO);
				
//					if( ! mapOfVm_requisition_approverDTOToinsertedByUserId.containsKey(vm_requisition_approverDTO.insertedByUserId)) {
//						mapOfVm_requisition_approverDTOToinsertedByUserId.put(vm_requisition_approverDTO.insertedByUserId, new HashSet<>());
//					}
//					mapOfVm_requisition_approverDTOToinsertedByUserId.get(vm_requisition_approverDTO.insertedByUserId).add(vm_requisition_approverDTO);
//
//					if( ! mapOfVm_requisition_approverDTOToinsertedByOrganogramId.containsKey(vm_requisition_approverDTO.insertedByOrganogramId)) {
//						mapOfVm_requisition_approverDTOToinsertedByOrganogramId.put(vm_requisition_approverDTO.insertedByOrganogramId, new HashSet<>());
//					}
//					mapOfVm_requisition_approverDTOToinsertedByOrganogramId.get(vm_requisition_approverDTO.insertedByOrganogramId).add(vm_requisition_approverDTO);
//
//					if( ! mapOfVm_requisition_approverDTOToinsertionDate.containsKey(vm_requisition_approverDTO.insertionDate)) {
//						mapOfVm_requisition_approverDTOToinsertionDate.put(vm_requisition_approverDTO.insertionDate, new HashSet<>());
//					}
//					mapOfVm_requisition_approverDTOToinsertionDate.get(vm_requisition_approverDTO.insertionDate).add(vm_requisition_approverDTO);
//
//					if( ! mapOfVm_requisition_approverDTOTolastModificationTime.containsKey(vm_requisition_approverDTO.lastModificationTime)) {
//						mapOfVm_requisition_approverDTOTolastModificationTime.put(vm_requisition_approverDTO.lastModificationTime, new HashSet<>());
//					}
//					mapOfVm_requisition_approverDTOTolastModificationTime.get(vm_requisition_approverDTO.lastModificationTime).add(vm_requisition_approverDTO);
//
//					if( ! mapOfVm_requisition_approverDTOTosearchColumn.containsKey(vm_requisition_approverDTO.searchColumn)) {
//						mapOfVm_requisition_approverDTOTosearchColumn.put(vm_requisition_approverDTO.searchColumn, new HashSet<>());
//					}
//					mapOfVm_requisition_approverDTOTosearchColumn.get(vm_requisition_approverDTO.searchColumn).add(vm_requisition_approverDTO);
//
					if( ! mapOfVm_requisition_approverDTOToapproverOrgId.containsKey(vm_requisition_approverDTO.approverOrgId)) {
						mapOfVm_requisition_approverDTOToapproverOrgId.put(vm_requisition_approverDTO.approverOrgId, new HashSet<>());
					}
					mapOfVm_requisition_approverDTOToapproverOrgId.get(vm_requisition_approverDTO.approverOrgId).add(vm_requisition_approverDTO);
					
//					if( ! mapOfVm_requisition_approverDTOToapproverOfficeId.containsKey(vm_requisition_approverDTO.approverOfficeId)) {
//						mapOfVm_requisition_approverDTOToapproverOfficeId.put(vm_requisition_approverDTO.approverOfficeId, new HashSet<>());
//					}
//					mapOfVm_requisition_approverDTOToapproverOfficeId.get(vm_requisition_approverDTO.approverOfficeId).add(vm_requisition_approverDTO);
//
//					if( ! mapOfVm_requisition_approverDTOToapproverOfficeUnitId.containsKey(vm_requisition_approverDTO.approverOfficeUnitId)) {
//						mapOfVm_requisition_approverDTOToapproverOfficeUnitId.put(vm_requisition_approverDTO.approverOfficeUnitId, new HashSet<>());
//					}
//					mapOfVm_requisition_approverDTOToapproverOfficeUnitId.get(vm_requisition_approverDTO.approverOfficeUnitId).add(vm_requisition_approverDTO);
//
//					if( ! mapOfVm_requisition_approverDTOToapproverEmpId.containsKey(vm_requisition_approverDTO.approverEmpId)) {
//						mapOfVm_requisition_approverDTOToapproverEmpId.put(vm_requisition_approverDTO.approverEmpId, new HashSet<>());
//					}
//					mapOfVm_requisition_approverDTOToapproverEmpId.get(vm_requisition_approverDTO.approverEmpId).add(vm_requisition_approverDTO);
//
//					if( ! mapOfVm_requisition_approverDTOToapproverPhoneNum.containsKey(vm_requisition_approverDTO.approverPhoneNum)) {
//						mapOfVm_requisition_approverDTOToapproverPhoneNum.put(vm_requisition_approverDTO.approverPhoneNum, new HashSet<>());
//					}
//					mapOfVm_requisition_approverDTOToapproverPhoneNum.get(vm_requisition_approverDTO.approverPhoneNum).add(vm_requisition_approverDTO);
//
//					if( ! mapOfVm_requisition_approverDTOToapproverNameEn.containsKey(vm_requisition_approverDTO.approverNameEn)) {
//						mapOfVm_requisition_approverDTOToapproverNameEn.put(vm_requisition_approverDTO.approverNameEn, new HashSet<>());
//					}
//					mapOfVm_requisition_approverDTOToapproverNameEn.get(vm_requisition_approverDTO.approverNameEn).add(vm_requisition_approverDTO);
//
//					if( ! mapOfVm_requisition_approverDTOToapproverNameBn.containsKey(vm_requisition_approverDTO.approverNameBn)) {
//						mapOfVm_requisition_approverDTOToapproverNameBn.put(vm_requisition_approverDTO.approverNameBn, new HashSet<>());
//					}
//					mapOfVm_requisition_approverDTOToapproverNameBn.get(vm_requisition_approverDTO.approverNameBn).add(vm_requisition_approverDTO);
//
//					if( ! mapOfVm_requisition_approverDTOToapproverOfficeNameEn.containsKey(vm_requisition_approverDTO.approverOfficeNameEn)) {
//						mapOfVm_requisition_approverDTOToapproverOfficeNameEn.put(vm_requisition_approverDTO.approverOfficeNameEn, new HashSet<>());
//					}
//					mapOfVm_requisition_approverDTOToapproverOfficeNameEn.get(vm_requisition_approverDTO.approverOfficeNameEn).add(vm_requisition_approverDTO);
//
//					if( ! mapOfVm_requisition_approverDTOToapproverOfficeNameBn.containsKey(vm_requisition_approverDTO.approverOfficeNameBn)) {
//						mapOfVm_requisition_approverDTOToapproverOfficeNameBn.put(vm_requisition_approverDTO.approverOfficeNameBn, new HashSet<>());
//					}
//					mapOfVm_requisition_approverDTOToapproverOfficeNameBn.get(vm_requisition_approverDTO.approverOfficeNameBn).add(vm_requisition_approverDTO);
//
//					if( ! mapOfVm_requisition_approverDTOToapproverOfficeUnitNameEn.containsKey(vm_requisition_approverDTO.approverOfficeUnitNameEn)) {
//						mapOfVm_requisition_approverDTOToapproverOfficeUnitNameEn.put(vm_requisition_approverDTO.approverOfficeUnitNameEn, new HashSet<>());
//					}
//					mapOfVm_requisition_approverDTOToapproverOfficeUnitNameEn.get(vm_requisition_approverDTO.approverOfficeUnitNameEn).add(vm_requisition_approverDTO);
//
//					if( ! mapOfVm_requisition_approverDTOToapproverOfficeUnitNameBn.containsKey(vm_requisition_approverDTO.approverOfficeUnitNameBn)) {
//						mapOfVm_requisition_approverDTOToapproverOfficeUnitNameBn.put(vm_requisition_approverDTO.approverOfficeUnitNameBn, new HashSet<>());
//					}
//					mapOfVm_requisition_approverDTOToapproverOfficeUnitNameBn.get(vm_requisition_approverDTO.approverOfficeUnitNameBn).add(vm_requisition_approverDTO);
//
//					if( ! mapOfVm_requisition_approverDTOToapproverOfficeUnitOrgNameEn.containsKey(vm_requisition_approverDTO.approverOfficeUnitOrgNameEn)) {
//						mapOfVm_requisition_approverDTOToapproverOfficeUnitOrgNameEn.put(vm_requisition_approverDTO.approverOfficeUnitOrgNameEn, new HashSet<>());
//					}
//					mapOfVm_requisition_approverDTOToapproverOfficeUnitOrgNameEn.get(vm_requisition_approverDTO.approverOfficeUnitOrgNameEn).add(vm_requisition_approverDTO);
//
//					if( ! mapOfVm_requisition_approverDTOToapproverOfficeUnitOrgNameBn.containsKey(vm_requisition_approverDTO.approverOfficeUnitOrgNameBn)) {
//						mapOfVm_requisition_approverDTOToapproverOfficeUnitOrgNameBn.put(vm_requisition_approverDTO.approverOfficeUnitOrgNameBn, new HashSet<>());
//					}
//					mapOfVm_requisition_approverDTOToapproverOfficeUnitOrgNameBn.get(vm_requisition_approverDTO.approverOfficeUnitOrgNameBn).add(vm_requisition_approverDTO);
//
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}

	public Vm_requisition_approverDTO clone(Vm_requisition_approverDTO dto) {
		String raw = gson.toJson(dto);
		return gson.fromJson(raw, Vm_requisition_approverDTO.class);
	}

	public List<Vm_requisition_approverDTO> clone(List<Vm_requisition_approverDTO> dtoList) {
		return dtoList
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}

	public Vm_requisition_approverDTO getVm_requisition_approverDTOByIDWithoutClone( long ID){
		return mapOfVm_requisition_approverDTOToiD.get(ID);
	}
	
	public List<Vm_requisition_approverDTO> getVm_requisition_approverList() {
		List <Vm_requisition_approverDTO> vm_requisition_approvers = new ArrayList<Vm_requisition_approverDTO>(this.mapOfVm_requisition_approverDTOToiD.values());
		return vm_requisition_approvers;
	}
	
	
	public Vm_requisition_approverDTO getVm_requisition_approverDTOByID( long ID){
		return clone(mapOfVm_requisition_approverDTOToiD.get(ID));
	}
	
	
//	public List<Vm_requisition_approverDTO> getVm_requisition_approverDTOByinserted_by_user_id(long inserted_by_user_id) {
//		return new ArrayList<>( mapOfVm_requisition_approverDTOToinsertedByUserId.getOrDefault(inserted_by_user_id,new HashSet<>()));
//	}
//
//
//	public List<Vm_requisition_approverDTO> getVm_requisition_approverDTOByinserted_by_organogram_id(long inserted_by_organogram_id) {
//		return new ArrayList<>( mapOfVm_requisition_approverDTOToinsertedByOrganogramId.getOrDefault(inserted_by_organogram_id,new HashSet<>()));
//	}
//
//
//	public List<Vm_requisition_approverDTO> getVm_requisition_approverDTOByinsertion_date(long insertion_date) {
//		return new ArrayList<>( mapOfVm_requisition_approverDTOToinsertionDate.getOrDefault(insertion_date,new HashSet<>()));
//	}
//
//
//	public List<Vm_requisition_approverDTO> getVm_requisition_approverDTOBylastModificationTime(long lastModificationTime) {
//		return new ArrayList<>( mapOfVm_requisition_approverDTOTolastModificationTime.getOrDefault(lastModificationTime,new HashSet<>()));
//	}
//
//
//	public List<Vm_requisition_approverDTO> getVm_requisition_approverDTOBysearch_column(String search_column) {
//		return new ArrayList<>( mapOfVm_requisition_approverDTOTosearchColumn.getOrDefault(search_column,new HashSet<>()));
//	}
	
	
	public List<Vm_requisition_approverDTO> getVm_requisition_approverDTOByapprover_org_id(long approver_org_id) {
		return clone(new ArrayList<>( mapOfVm_requisition_approverDTOToapproverOrgId.getOrDefault(approver_org_id,new HashSet<>())));
	}
	
	
//	public List<Vm_requisition_approverDTO> getVm_requisition_approverDTOByapprover_office_id(long approver_office_id) {
//		return new ArrayList<>( mapOfVm_requisition_approverDTOToapproverOfficeId.getOrDefault(approver_office_id,new HashSet<>()));
//	}
//
//
//	public List<Vm_requisition_approverDTO> getVm_requisition_approverDTOByapprover_office_unit_id(long approver_office_unit_id) {
//		return new ArrayList<>( mapOfVm_requisition_approverDTOToapproverOfficeUnitId.getOrDefault(approver_office_unit_id,new HashSet<>()));
//	}
//
//
//	public List<Vm_requisition_approverDTO> getVm_requisition_approverDTOByapprover_emp_id(long approver_emp_id) {
//		return new ArrayList<>( mapOfVm_requisition_approverDTOToapproverEmpId.getOrDefault(approver_emp_id,new HashSet<>()));
//	}
//
//
//	public List<Vm_requisition_approverDTO> getVm_requisition_approverDTOByapprover_phone_num(String approver_phone_num) {
//		return new ArrayList<>( mapOfVm_requisition_approverDTOToapproverPhoneNum.getOrDefault(approver_phone_num,new HashSet<>()));
//	}
//
//
//	public List<Vm_requisition_approverDTO> getVm_requisition_approverDTOByapprover_name_en(String approver_name_en) {
//		return new ArrayList<>( mapOfVm_requisition_approverDTOToapproverNameEn.getOrDefault(approver_name_en,new HashSet<>()));
//	}
//
//
//	public List<Vm_requisition_approverDTO> getVm_requisition_approverDTOByapprover_name_bn(String approver_name_bn) {
//		return new ArrayList<>( mapOfVm_requisition_approverDTOToapproverNameBn.getOrDefault(approver_name_bn,new HashSet<>()));
//	}
//
//
//	public List<Vm_requisition_approverDTO> getVm_requisition_approverDTOByapprover_office_name_en(String approver_office_name_en) {
//		return new ArrayList<>( mapOfVm_requisition_approverDTOToapproverOfficeNameEn.getOrDefault(approver_office_name_en,new HashSet<>()));
//	}
//
//
//	public List<Vm_requisition_approverDTO> getVm_requisition_approverDTOByapprover_office_name_bn(String approver_office_name_bn) {
//		return new ArrayList<>( mapOfVm_requisition_approverDTOToapproverOfficeNameBn.getOrDefault(approver_office_name_bn,new HashSet<>()));
//	}
//
//
//	public List<Vm_requisition_approverDTO> getVm_requisition_approverDTOByapprover_office_unit_name_en(String approver_office_unit_name_en) {
//		return new ArrayList<>( mapOfVm_requisition_approverDTOToapproverOfficeUnitNameEn.getOrDefault(approver_office_unit_name_en,new HashSet<>()));
//	}
//
//
//	public List<Vm_requisition_approverDTO> getVm_requisition_approverDTOByapprover_office_unit_name_bn(String approver_office_unit_name_bn) {
//		return new ArrayList<>( mapOfVm_requisition_approverDTOToapproverOfficeUnitNameBn.getOrDefault(approver_office_unit_name_bn,new HashSet<>()));
//	}
//
//
//	public List<Vm_requisition_approverDTO> getVm_requisition_approverDTOByapprover_office_unit_org_name_en(String approver_office_unit_org_name_en) {
//		return new ArrayList<>( mapOfVm_requisition_approverDTOToapproverOfficeUnitOrgNameEn.getOrDefault(approver_office_unit_org_name_en,new HashSet<>()));
//	}
//
//
//	public List<Vm_requisition_approverDTO> getVm_requisition_approverDTOByapprover_office_unit_org_name_bn(String approver_office_unit_org_name_bn) {
//		return new ArrayList<>( mapOfVm_requisition_approverDTOToapproverOfficeUnitOrgNameBn.getOrDefault(approver_office_unit_org_name_bn,new HashSet<>()));
//	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "vm_requisition_approver";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


