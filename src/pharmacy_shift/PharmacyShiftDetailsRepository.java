package pharmacy_shift;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import com.google.gson.Gson;

import repository.Repository;
import repository.RepositoryManager;
import util.TimeConverter;


public class PharmacyShiftDetailsRepository implements Repository {
	PharmacyShiftDetailsDAO pharmacyshiftdetailsDAO = null;
	
	static Logger logger = Logger.getLogger(PharmacyShiftDetailsRepository.class);
	Map<Long, PharmacyShiftDetailsDTO>mapOfPharmacyShiftDetailsDTOToiD;
	Map<Long, Set<PharmacyShiftDetailsDTO> >mapOfPharmacyShiftDetailsDTOTopharmacyShiftId;
	Map<Integer, PharmacyShiftDetailsDTO >mapOfPharmacyShiftDetailsDTOToDayCat;
	Gson gson;

  
	private PharmacyShiftDetailsRepository(){
		pharmacyshiftdetailsDAO = PharmacyShiftDetailsDAO.getInstance();
		mapOfPharmacyShiftDetailsDTOToiD = new ConcurrentHashMap<>();
		mapOfPharmacyShiftDetailsDTOTopharmacyShiftId = new ConcurrentHashMap<>();
		mapOfPharmacyShiftDetailsDTOToDayCat = new ConcurrentHashMap<>();
		gson = new Gson();
		RepositoryManager.getInstance().addRepository(this);
	}

	private static class LazyLoader{
        final static PharmacyShiftDetailsRepository INSTANCE = new PharmacyShiftDetailsRepository();
    }

    public static PharmacyShiftDetailsRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

	public void reload(boolean reloadAll){
		try {
			List<PharmacyShiftDetailsDTO> pharmacyshiftdetailsDTOs = pharmacyshiftdetailsDAO.getAllDTOs(reloadAll);
			for(PharmacyShiftDetailsDTO pharmacyshiftdetailsDTO : pharmacyshiftdetailsDTOs) {
				PharmacyShiftDetailsDTO oldPharmacyShiftDetailsDTO = getPharmacyShiftDetailsDTOByiD(pharmacyshiftdetailsDTO.iD);
				if( oldPharmacyShiftDetailsDTO != null ) {
					mapOfPharmacyShiftDetailsDTOToiD.remove(oldPharmacyShiftDetailsDTO.iD);
					mapOfPharmacyShiftDetailsDTOToDayCat.remove(oldPharmacyShiftDetailsDTO.dayCat);
				
					if(mapOfPharmacyShiftDetailsDTOTopharmacyShiftId.containsKey(oldPharmacyShiftDetailsDTO.pharmacyShiftId)) {
						mapOfPharmacyShiftDetailsDTOTopharmacyShiftId.get(oldPharmacyShiftDetailsDTO.pharmacyShiftId).remove(oldPharmacyShiftDetailsDTO);
					}
					if(mapOfPharmacyShiftDetailsDTOTopharmacyShiftId.get(oldPharmacyShiftDetailsDTO.pharmacyShiftId).isEmpty()) {
						mapOfPharmacyShiftDetailsDTOTopharmacyShiftId.remove(oldPharmacyShiftDetailsDTO.pharmacyShiftId);
					}
					
					
				}
				if(pharmacyshiftdetailsDTO.isDeleted == 0) 
				{
					
					mapOfPharmacyShiftDetailsDTOToiD.put(pharmacyshiftdetailsDTO.iD, pharmacyshiftdetailsDTO);
					mapOfPharmacyShiftDetailsDTOToDayCat.put(pharmacyshiftdetailsDTO.dayCat, pharmacyshiftdetailsDTO);
				
					if( ! mapOfPharmacyShiftDetailsDTOTopharmacyShiftId.containsKey(pharmacyshiftdetailsDTO.pharmacyShiftId)) {
						mapOfPharmacyShiftDetailsDTOTopharmacyShiftId.put(pharmacyshiftdetailsDTO.pharmacyShiftId, new HashSet<>());
					}
					mapOfPharmacyShiftDetailsDTOTopharmacyShiftId.get(pharmacyshiftdetailsDTO.pharmacyShiftId).add(pharmacyshiftdetailsDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public PharmacyShiftDetailsDTO clone(PharmacyShiftDetailsDTO dto) {
		String raw = gson.toJson(dto);
		return gson.fromJson(raw, PharmacyShiftDetailsDTO.class);
	}
	
	
	public List<PharmacyShiftDetailsDTO> getPharmacyShiftDetailsList() {
		List <PharmacyShiftDetailsDTO> pharmacyshiftdetailss = new ArrayList<PharmacyShiftDetailsDTO>(this.mapOfPharmacyShiftDetailsDTOToiD.values());
		return pharmacyshiftdetailss;
	}
	
	public List<PharmacyShiftDetailsDTO> copyPharmacyShiftDetailsList() {
		List <PharmacyShiftDetailsDTO> pharmacyshiftdetailss = getPharmacyShiftDetailsList();
		return pharmacyshiftdetailss
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}
	
	public boolean isPharmacyOpenNow()
	{
		Calendar cal = Calendar.getInstance();
        int day = cal.get(Calendar.DAY_OF_WEEK) % 7;
        PharmacyShiftDetailsDTO pharmacyShiftDetailsDTO= getPharmacyShiftDetailsDTOByDayCat(day);
        if(pharmacyShiftDetailsDTO == null || !pharmacyShiftDetailsDTO.isOpen)
        {
        	return false;
        }
        if(pharmacyShiftDetailsDTO.startTime.isEmpty() || pharmacyShiftDetailsDTO.endTime.isEmpty())
        {
        	return false;
        }
        long now = cal.getTimeInMillis();
        TimeConverter.clearTimes(cal);
        
        String [] start = pharmacyShiftDetailsDTO.startTime.split(":");
        cal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(start[0]));
        cal.set(Calendar.MINUTE, Integer.parseInt(start[1]));
        long startMillis = cal.getTimeInMillis();
        
        String [] end = pharmacyShiftDetailsDTO.endTime.split(":");
        cal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(end[0]));
        cal.set(Calendar.MINUTE, Integer.parseInt(end[1]));
        long endMillis = cal.getTimeInMillis();

        return startMillis <= now && now <= endMillis;
        
	}
	
	public PharmacyShiftDetailsDTO getPharmacyShiftDetailsDTOByDayCat( int dayCat){
		return mapOfPharmacyShiftDetailsDTOToDayCat.getOrDefault(dayCat, null);
	}
	
	public PharmacyShiftDetailsDTO getPharmacyShiftDetailsDTOByiD( long iD){
		return mapOfPharmacyShiftDetailsDTOToiD.get(iD);
	}
	
	public PharmacyShiftDetailsDTO copyPharmacyShiftDetailsDTOByiD( long iD){
		return clone(mapOfPharmacyShiftDetailsDTOToiD.get(iD));
	}
	
	
	public List<PharmacyShiftDetailsDTO> getPharmacyShiftDetailsDTOBypharmacyShiftId(long pharmacyShiftId) {
		return new ArrayList<>( mapOfPharmacyShiftDetailsDTOTopharmacyShiftId.getOrDefault(pharmacyShiftId,new HashSet<>()));
	}
	
	public List<PharmacyShiftDetailsDTO> copyPharmacyShiftDetailsDTOBypharmacyShiftId(long pharmacyShiftId)
	{
		List <PharmacyShiftDetailsDTO> pharmacyshiftdetailss = getPharmacyShiftDetailsDTOBypharmacyShiftId(pharmacyShiftId);
		return pharmacyshiftdetailss
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}

	
	@Override
	public String getTableName() {
		return pharmacyshiftdetailsDAO.getTableName();
	}
}


