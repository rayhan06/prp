package pharmacy_shift;


import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import org.apache.log4j.Logger;


import permission.MenuConstants;

import user.UserDTO;
import util.*;
import javax.servlet.http.*;
import java.util.*;

import common.ApiResponse;
import common.BaseServlet;
import pb.Utils;

import com.google.gson.Gson;

import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;




/**
 * Servlet implementation class Pharmacy_shiftServlet
 */
@WebServlet("/Pharmacy_shiftServlet")
@MultipartConfig
public class Pharmacy_shiftServlet extends BaseServlet 
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Pharmacy_shiftServlet.class);

    @Override
    public String getTableName() {
        return Pharmacy_shiftDAO.getInstance().getTableName();
    }

    @Override
    public String getServletName() {
        return "Pharmacy_shiftServlet";
    }

    @Override
    public Pharmacy_shiftDAO getCommonDAOService() {
        return Pharmacy_shiftDAO.getInstance();
    }
    
    @Override
    public int[] getAddPageMenuConstants() {
        return new int[] {MenuConstants.PHARMACY_SHIFT_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[] {MenuConstants.PHARMACY_SHIFT_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[] {MenuConstants.PHARMACY_SHIFT_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Pharmacy_shiftServlet.class;
    }
	PharmacyShiftDetailsDAO pharmacyShiftDetailsDAO = PharmacyShiftDetailsDAO.getInstance();
    private final Gson gson = new Gson();
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        CommonLoginData commonLoginData = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get();
        init(request);
        String actionType = request.getParameter("actionType");
		if ("ajax_edit".equals(actionType)) {
			 if (Utils.checkPermission(commonLoginData.userDTO, getEditPageMenuConstants())
                     && getEditPermission(request)) {
                 try {
                     CommonDTO commonDTO = addT(request, false, commonLoginData.userDTO);
                     finalize(request);
                     ApiResponse.sendSuccessResponse(response, "Pharmacy_shiftServlet?actionType=view&ID=" + commonDTO.iD);
                 } catch (Exception ex) {
                     ex.printStackTrace();
                     logger.error(ex);
                     ApiResponse.sendErrorResponse(response, ex.getMessage());
                 }
             } else {
                 ApiResponse.sendErrorResponse(response, commonLoginData.isLangEng ? "You have no permission to do this task" : "আপনার এই ক্রিয়া সম্পাদনের অনুমতি নেই");
             }
            return;
		}
		
		else
		{
			super.doPost(request,response);
		}
    }
 	

	@Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception 
	{
		addFlag = false;
		
		request.setAttribute("failureMessage", "");
		System.out.println("%%%% addPharmacy_shift");
		String language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
		Pharmacy_shiftDTO pharmacy_shiftDTO;
		
					
		if(addFlag)
		{
			pharmacy_shiftDTO = new Pharmacy_shiftDTO();
		}
		else
		{
			pharmacy_shiftDTO = Pharmacy_shiftDAO.getInstance().getDTOFromID(Long.parseLong(request.getParameter("iD")));
		}
		
		
		System.out.println("Done2 adding  addPharmacy_shift dto = " + pharmacy_shiftDTO);

		if(addFlag == true)
		{
			Pharmacy_shiftDAO.getInstance().add(pharmacy_shiftDTO);
		}
		else
		{				
			Pharmacy_shiftDAO.getInstance().update(pharmacy_shiftDTO);										
		}
		
		List<PharmacyShiftDetailsDTO> pharmacyShiftDetailsDTOList = createPharmacyShiftDetailsDTOListByRequest(request, language, pharmacy_shiftDTO, userDTO);
		pharmacyShiftDetailsDAO.deleteChildrenByParent(pharmacy_shiftDTO.iD, "pharmacy_shift_id");

		
		if(pharmacyShiftDetailsDTOList != null)
		{				
			for(PharmacyShiftDetailsDTO pharmacyShiftDetailsDTO: pharmacyShiftDetailsDTOList)
			{
				pharmacyShiftDetailsDAO.add(pharmacyShiftDetailsDTO);
			}
		}
	
		PharmacyShiftDetailsRepository.getInstance().reload(true);	
		return pharmacy_shiftDTO;

	}
	private List<PharmacyShiftDetailsDTO> createPharmacyShiftDetailsDTOListByRequest(HttpServletRequest request, String language, Pharmacy_shiftDTO pharmacy_shiftDTO, UserDTO userDTO) throws Exception{ 
		List<PharmacyShiftDetailsDTO> pharmacyShiftDetailsDTOList = new ArrayList<PharmacyShiftDetailsDTO>();
		if(request.getParameterValues("pharmacyShiftDetails.iD") != null) 
		{
			int pharmacyShiftDetailsItemNo = request.getParameterValues("pharmacyShiftDetails.iD").length;
			
			
			for(int index=0;index<pharmacyShiftDetailsItemNo;index++){
				PharmacyShiftDetailsDTO pharmacyShiftDetailsDTO = createPharmacyShiftDetailsDTOByRequestAndIndex(request,true,index, language, pharmacy_shiftDTO, userDTO);
				if(pharmacyShiftDetailsDTO != null)
				{
					pharmacyShiftDetailsDTOList.add(pharmacyShiftDetailsDTO);
				}
				
			}
			
			return pharmacyShiftDetailsDTOList;
		}
		return null;
	}
	
	private PharmacyShiftDetailsDTO createPharmacyShiftDetailsDTOByRequestAndIndex(HttpServletRequest request, boolean addFlag, int index, String language, Pharmacy_shiftDTO pharmacy_shiftDTO, UserDTO userDTO) throws Exception{
	
		PharmacyShiftDetailsDTO pharmacyShiftDetailsDTO;
		if(addFlag == true )
		{
			pharmacyShiftDetailsDTO = new PharmacyShiftDetailsDTO();
		}
		else
		{
			pharmacyShiftDetailsDTO = pharmacyShiftDetailsDAO.getDTOByID(Long.parseLong(request.getParameterValues("pharmacyShiftDetails.iD")[index]));
		}
		
		
		pharmacyShiftDetailsDTO.pharmacyShiftId = pharmacy_shiftDTO.iD; 

		String Value = "";

		Value = request.getParameterValues("pharmacyShiftDetails.isOpen")[index];

		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("isOpen = " + Value);
		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			pharmacyShiftDetailsDTO.isOpen = Boolean.parseBoolean(Value);
		}
		

		Value = request.getParameterValues("pharmacyShiftDetails.dayCat")[index];

		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("dayCat = " + Value);
		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			pharmacyShiftDetailsDTO.dayCat = Integer.parseInt(Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}
		
		if(!pharmacyShiftDetailsDTO.isOpen)
		{
			pharmacyShiftDetailsDTO.startTime = "";
			pharmacyShiftDetailsDTO.endTime = "";
			System.out.println("Not open");
			return pharmacyShiftDetailsDTO;
			
		}
		
		System.out.println("open");

		Value = request.getParameterValues("pharmacyShiftDetails.startTime")[index];

		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("startTime = " + Value);
		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			pharmacyShiftDetailsDTO.startTime = TimeFormat.getIn24HourFormat(Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}

		Value = request.getParameterValues("pharmacyShiftDetails.endTime")[index];

		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("endTime = " + Value);
		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			pharmacyShiftDetailsDTO.endTime = TimeFormat.getIn24HourFormat(Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}
		
		if(pharmacyShiftDetailsDTO.isOpen)
		{
			if(pharmacyShiftDetailsDTO.endTime.equalsIgnoreCase("") || pharmacyShiftDetailsDTO.startTime.equalsIgnoreCase(""))
			{
				return null;
			}
			
			int endHour = Integer.parseInt(pharmacyShiftDetailsDTO.endTime.split(":")[0]);
			int endMinute = Integer.parseInt(pharmacyShiftDetailsDTO.endTime.split(":")[1]);
			int startHour = Integer.parseInt(pharmacyShiftDetailsDTO.startTime.split(":")[0]);
			int startMinute = Integer.parseInt(pharmacyShiftDetailsDTO.startTime.split(":")[1]);
			
			if(endHour * 60 + endMinute < startHour * 60 + startMinute)
			{
				return null;
			}
		}
		
		
		return pharmacyShiftDetailsDTO;
	
	}	
}

