package pharmacy_shift;
import java.util.*; 
import util.*; 


public class PharmacyShiftDetailsDTO extends CommonDTO
{

	public long pharmacyShiftId = -1;
	public boolean isOpen = false;
	public int dayCat = -1;
	public String startTime = "";
	public String endTime = "";
	

	
	public List<PharmacyShiftDetailsDTO> pharmacyShiftDetailsDTOList = new ArrayList<>();
	
    @Override
	public String toString() {
            return "$PharmacyShiftDetailsDTO[" +
            " iD = " + iD +
            " pharmacyShiftId = " + pharmacyShiftId +
            " isOpen = " + isOpen +
            " dayCat = " + dayCat +
            " startTime = " + startTime +
            " endTime = " + endTime +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}