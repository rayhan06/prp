package pharmacy_shift;
import java.util.*; 
import util.*;


public class PharmacyShiftDetailsMAPS extends CommonMaps
{	
	public PharmacyShiftDetailsMAPS(String tableName)
	{
		


		java_SQL_map.put("pharmacy_shift_id".toLowerCase(), "pharmacyShiftId".toLowerCase());
		java_SQL_map.put("is_open".toLowerCase(), "isOpen".toLowerCase());
		java_SQL_map.put("day_cat".toLowerCase(), "dayCat".toLowerCase());
		java_SQL_map.put("start_time".toLowerCase(), "startTime".toLowerCase());
		java_SQL_map.put("end_time".toLowerCase(), "endTime".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Pharmacy Shift Id".toLowerCase(), "pharmacyShiftId".toLowerCase());
		java_Text_map.put("Is Open".toLowerCase(), "isOpen".toLowerCase());
		java_Text_map.put("Day".toLowerCase(), "dayCat".toLowerCase());
		java_Text_map.put("Start Time".toLowerCase(), "startTime".toLowerCase());
		java_Text_map.put("End Time".toLowerCase(), "endTime".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}