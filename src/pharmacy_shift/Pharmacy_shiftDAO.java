package pharmacy_shift;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.sql.SQLException;
import common.CommonDAOService;
import org.apache.log4j.Logger;
import util.*;
import pb.*;

public class Pharmacy_shiftDAO  implements CommonDAOService<Pharmacy_shiftDTO>
{
	
	Logger logger = Logger.getLogger(getClass());
	
	private static String addQuery = "";
	private static String updateQuery = "";
	private String[] columnNames = null;

	private Pharmacy_shiftDAO()
	{
		columnNames = new String[] 
		{
			"ID",
			"isDeleted",
			"lastModificationTime"
		};
		updateQuery = getUpdateQuery(columnNames);
        addQuery = getAddQuery(columnNames);
		
		searchMap.put("AnyField"," and (search_column like ?)");
	}

	private final Map<String,String> searchMap = new HashMap<>();

    private static class LazyLoader
    {
    	static final Pharmacy_shiftDAO INSTANCE = new Pharmacy_shiftDAO();
	}

	public static Pharmacy_shiftDAO getInstance()
	{
    	return LazyLoader.INSTANCE;
	}
	
	public void setSearchColumn(Pharmacy_shiftDTO pharmacy_shiftDTO)
	{
		pharmacy_shiftDTO.searchColumn = "";
	}
	
	@Override
	public void set(PreparedStatement ps, Pharmacy_shiftDTO pharmacy_shiftDTO, boolean isInsert) throws SQLException
	{
		int index = 0;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(pharmacy_shiftDTO);
		if(isInsert)
		{
			ps.setObject(++index,pharmacy_shiftDTO.iD);
		}
		if(isInsert)
		{
			ps.setObject(++index,pharmacy_shiftDTO.isDeleted);
		}
		ps.setObject(++index, lastModificationTime);
		if(!isInsert)
		{
			ps.setObject(++index,pharmacy_shiftDTO.iD);
		}
	}
	
	@Override
	public Pharmacy_shiftDTO buildObjectFromResultSet(ResultSet rs)
	{
		try
		{
			Pharmacy_shiftDTO pharmacy_shiftDTO = new Pharmacy_shiftDTO();
			int i = 0;
			pharmacy_shiftDTO.iD = rs.getLong(columnNames[i++]);
			pharmacy_shiftDTO.isDeleted = rs.getInt(columnNames[i++]);
			pharmacy_shiftDTO.lastModificationTime = rs.getLong(columnNames[i++]);
			return pharmacy_shiftDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
		
	public Pharmacy_shiftDTO getDTOByID (long id)
	{
		Pharmacy_shiftDTO pharmacy_shiftDTO = null;
		try 
		{
			pharmacy_shiftDTO = getDTOFromID(id);
			if(pharmacy_shiftDTO != null)
			{
				PharmacyShiftDetailsDAO pharmacyShiftDetailsDAO = PharmacyShiftDetailsDAO.getInstance();				
				List<PharmacyShiftDetailsDTO> pharmacyShiftDetailsDTOList = (List<PharmacyShiftDetailsDTO>)pharmacyShiftDetailsDAO.getDTOsByParent("pharmacy_shift_id", pharmacy_shiftDTO.iD);
				pharmacy_shiftDTO.pharmacyShiftDetailsDTOList = pharmacyShiftDetailsDTOList;
			}
		}
		catch (Exception e) 
		{
				// TODO Auto-generated catch block
				e.printStackTrace();
		}
		return pharmacy_shiftDTO;
	}

	@Override
	public String getTableName() {
		return "pharmacy_shift";
	}

	@Override
	public Map<String, String> getSearchMap() {
		return searchMap;
	}

	public long add(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Pharmacy_shiftDTO) commonDTO,addQuery,true);
	}

	public long update(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Pharmacy_shiftDTO) commonDTO,updateQuery,false);
	}
	
	@Override
	public String getLastModifierUser(){
		return "";
    }
				
}
	