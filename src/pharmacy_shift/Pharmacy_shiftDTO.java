package pharmacy_shift;
import java.util.*; 
import util.*; 


public class Pharmacy_shiftDTO extends CommonDTO
{
	public Pharmacy_shiftDTO()
	{
		iD = 1;
	}
	
	public List<PharmacyShiftDetailsDTO> pharmacyShiftDetailsDTOList = new ArrayList<>();
	
    @Override
	public String toString() {
            return "$Pharmacy_shiftDTO[" +
            " iD = " + iD +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}