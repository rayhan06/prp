package pharmacy_shift;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.sql.SQLException;
import common.CommonDAOService;
import common.ConnectionAndStatementUtil;

import org.apache.log4j.Logger;
import util.*;
import pb.*;
import shift_slot.Shift_slotDTO;

public class PharmacyShiftDetailsDAO  implements CommonDAOService<PharmacyShiftDetailsDTO>
{
	
	Logger logger = Logger.getLogger(getClass());
	
	private static String addQuery = "";
	private static String updateQuery = "";
	private String[] columnNames = null;

	private PharmacyShiftDetailsDAO()
	{
		columnNames = new String[] 
		{
			"ID",
			"pharmacy_shift_id",
			"is_open",
			"day_cat",
			"start_time",
			"end_time",
			"isDeleted",
			"lastModificationTime"
		};
		updateQuery = getUpdateQuery(columnNames);
        addQuery = getAddQuery(columnNames);
		
		searchMap.put("day_cat"," and (day_cat = ?)");
		searchMap.put("AnyField"," and (search_column like ?)");
	}

	private final Map<String,String> searchMap = new HashMap<>();

    private static class LazyLoader
    {
    	static final PharmacyShiftDetailsDAO INSTANCE = new PharmacyShiftDetailsDAO();
	}

	public static PharmacyShiftDetailsDAO getInstance()
	{
    	return LazyLoader.INSTANCE;
	}
	
	public void setSearchColumn(PharmacyShiftDetailsDTO pharmacyshiftdetailsDTO)
	{
		pharmacyshiftdetailsDTO.searchColumn = "";
		pharmacyshiftdetailsDTO.searchColumn += CatDAO.getName("English", "day", pharmacyshiftdetailsDTO.dayCat) + " " + CatDAO.getName("Bangla", "day", pharmacyshiftdetailsDTO.dayCat) + " ";
	}
	
	@Override
	public void set(PreparedStatement ps, PharmacyShiftDetailsDTO pharmacyshiftdetailsDTO, boolean isInsert) throws SQLException
	{
		int index = 0;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(pharmacyshiftdetailsDTO);
		if(isInsert)
		{
			ps.setObject(++index,pharmacyshiftdetailsDTO.iD);
		}
		ps.setObject(++index,pharmacyshiftdetailsDTO.pharmacyShiftId);
		ps.setObject(++index,pharmacyshiftdetailsDTO.isOpen);
		ps.setObject(++index,pharmacyshiftdetailsDTO.dayCat);
		ps.setObject(++index,pharmacyshiftdetailsDTO.startTime);
		ps.setObject(++index,pharmacyshiftdetailsDTO.endTime);
		if(isInsert)
		{
			ps.setObject(++index,pharmacyshiftdetailsDTO.isDeleted);
		}
		ps.setObject(++index, lastModificationTime);
		if(!isInsert)
		{
			ps.setObject(++index,pharmacyshiftdetailsDTO.iD);
		}
	}
	
	@Override
	public PharmacyShiftDetailsDTO buildObjectFromResultSet(ResultSet rs)
	{
		try
		{
			PharmacyShiftDetailsDTO pharmacyshiftdetailsDTO = new PharmacyShiftDetailsDTO();
			int i = 0;
			pharmacyshiftdetailsDTO.iD = rs.getLong(columnNames[i++]);
			pharmacyshiftdetailsDTO.pharmacyShiftId = rs.getLong(columnNames[i++]);
			pharmacyshiftdetailsDTO.isOpen = rs.getBoolean(columnNames[i++]);
			pharmacyshiftdetailsDTO.dayCat = rs.getInt(columnNames[i++]);
			pharmacyshiftdetailsDTO.startTime = rs.getString(columnNames[i++]);
			pharmacyshiftdetailsDTO.endTime = rs.getString(columnNames[i++]);
			pharmacyshiftdetailsDTO.isDeleted = rs.getInt(columnNames[i++]);
			pharmacyshiftdetailsDTO.lastModificationTime = rs.getLong(columnNames[i++]);
			return pharmacyshiftdetailsDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
		
	public PharmacyShiftDetailsDTO getDTOByID (long id)
	{
		return getDTOFromID(id);
	}
	
	public PharmacyShiftDetailsDTO getDTOByDay (int day)
	{
		String sql = "select * from pharmacy_shift_details where isDeleted = 0 and day_cat = " + day + " limit 1 ";
		return ConnectionAndStatementUtil.getT(sql,this::buildObjectFromResultSet);	
	}

	@Override
	public String getTableName() {
		return "pharmacy_shift_details";
	}

	@Override
	public Map<String, String> getSearchMap() {
		return searchMap;
	}

	public long add(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((PharmacyShiftDetailsDTO) commonDTO,addQuery,true);
	}

	public long update(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((PharmacyShiftDetailsDTO) commonDTO,updateQuery,false);
	}
	
	@Override
	public String getLastModifierUser(){
		return "";
    }
				
}
	