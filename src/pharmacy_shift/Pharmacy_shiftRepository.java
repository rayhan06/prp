package pharmacy_shift;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import com.google.gson.Gson;

import repository.Repository;
import repository.RepositoryManager;


public class Pharmacy_shiftRepository implements Repository {
	Pharmacy_shiftDAO pharmacy_shiftDAO = null;
	
	static Logger logger = Logger.getLogger(Pharmacy_shiftRepository.class);
	Map<Long, Pharmacy_shiftDTO>mapOfPharmacy_shiftDTOToiD;
	Gson gson;

  
	private Pharmacy_shiftRepository(){
		pharmacy_shiftDAO = Pharmacy_shiftDAO.getInstance();
		mapOfPharmacy_shiftDTOToiD = new ConcurrentHashMap<>();
		gson = new Gson();
		RepositoryManager.getInstance().addRepository(this);
	}

	private static class LazyLoader{
        final static Pharmacy_shiftRepository INSTANCE = new Pharmacy_shiftRepository();
    }

    public static Pharmacy_shiftRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

	public void reload(boolean reloadAll){
		try {
			List<Pharmacy_shiftDTO> pharmacy_shiftDTOs = pharmacy_shiftDAO.getAllDTOs(reloadAll);
			for(Pharmacy_shiftDTO pharmacy_shiftDTO : pharmacy_shiftDTOs) {
				Pharmacy_shiftDTO oldPharmacy_shiftDTO = getPharmacy_shiftDTOByiD(pharmacy_shiftDTO.iD);
				if( oldPharmacy_shiftDTO != null ) {
					mapOfPharmacy_shiftDTOToiD.remove(oldPharmacy_shiftDTO.iD);
				
					
				}
				if(pharmacy_shiftDTO.isDeleted == 0) 
				{
					
					mapOfPharmacy_shiftDTOToiD.put(pharmacy_shiftDTO.iD, pharmacy_shiftDTO);
				
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public Pharmacy_shiftDTO clone(Pharmacy_shiftDTO dto) {
		String raw = gson.toJson(dto);
		return gson.fromJson(raw, Pharmacy_shiftDTO.class);
	}
	
	
	public List<Pharmacy_shiftDTO> getPharmacy_shiftList() {
		List <Pharmacy_shiftDTO> pharmacy_shifts = new ArrayList<Pharmacy_shiftDTO>(this.mapOfPharmacy_shiftDTOToiD.values());
		return pharmacy_shifts;
	}
	
	public List<Pharmacy_shiftDTO> copyPharmacy_shiftList() {
		List <Pharmacy_shiftDTO> pharmacy_shifts = getPharmacy_shiftList();
		return pharmacy_shifts
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}
	
	
	
	public Pharmacy_shiftDTO getPharmacy_shiftDTOByiD( long iD){
		return mapOfPharmacy_shiftDTOToiD.get(iD);
	}
	
	public Pharmacy_shiftDTO copyPharmacy_shiftDTOByiD( long iD){
		return clone(mapOfPharmacy_shiftDTOToiD.get(iD));
	}

	
	@Override
	public String getTableName() {
		return pharmacy_shiftDAO.getTableName();
	}
}


