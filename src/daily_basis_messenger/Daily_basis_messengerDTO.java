package daily_basis_messenger;

import sessionmanager.SessionConstants;
import util.CommonDTO;


public class Daily_basis_messengerDTO extends CommonDTO {
    public String name = "";
    public String fatherName = "";
    public long dailyRate = 0;
    public String mobileNo = "";
    public String bankAccountNo = "";
    public long modifiedBy = -1;
    public long insertedBy = -1;
    public long insertionTime = SessionConstants.MIN_DATE;

    @Override
    public String toString() {
        return "$Daily_basis_messengerDTO[" +
               " iD = " + iD +
               " name = " + name +
               " fatherName = " + fatherName +
               " dailyRate = " + dailyRate +
               " mobileNo = " + mobileNo +
               " bankAccountNo = " + bankAccountNo +
               " searchColumn = " + searchColumn +
               " modifiedBy = " + modifiedBy +
               " lastModificationTime = " + lastModificationTime +
               " insertedBy = " + insertedBy +
               " insertionTime = " + insertionTime +
               " isDeleted = " + isDeleted +
               "]";
    }
}