package daily_basis_messenger;

import common.BaseServlet;
import org.apache.log4j.Logger;
import pb.Utils;
import permission.MenuConstants;
import user.UserDTO;
import util.CommonDTO;
import util.HttpRequestUtils;
import util.StringUtils;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;


@WebServlet("/Daily_basis_messengerServlet")
@MultipartConfig
public class Daily_basis_messengerServlet extends BaseServlet {
    private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Daily_basis_messengerServlet.class);


    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        boolean isEnglish = "english".equalsIgnoreCase(HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language);
        Daily_basis_messengerDTO dailyBasisMessengerDTO;
        long currentTime = System.currentTimeMillis();
        if (addFlag) {
            dailyBasisMessengerDTO = new Daily_basis_messengerDTO();
            dailyBasisMessengerDTO.insertedBy = userDTO.ID;
            dailyBasisMessengerDTO.insertionTime = currentTime;
        } else {
            dailyBasisMessengerDTO = Daily_basis_messengerDAO.getInstance().getDTOFromID(Long.parseLong(request.getParameter("iD")));
        }
        dailyBasisMessengerDTO.modifiedBy = userDTO.ID;
        dailyBasisMessengerDTO.lastModificationTime = currentTime;

        dailyBasisMessengerDTO.name = Utils.cleanAndGetMandatoryString(
                request.getParameter("name"),
                isEnglish ? "Write Name" : "নাম লিখুন"
        );
        dailyBasisMessengerDTO.dailyRate = Utils.parseMandatoryLong(
                request.getParameter("dailyRate"),
                isEnglish ? "White Daily Rate" : "দৈনিক হার লিখুন"
        );
        dailyBasisMessengerDTO.fatherName = Utils.cleanAndGetOptionalString(request.getParameter("fatherName"));
        dailyBasisMessengerDTO.mobileNo = Utils.parseOptionalMobileNumber(
                request.getParameter("mobileNo"),
                isEnglish ? "Write correct mobile number" : "সঠিক মোবাইল নম্বর লিখুন"
        );
        dailyBasisMessengerDTO.bankAccountNo = StringUtils.convertToEngNumber(
                Utils.cleanAndGetOptionalString(request.getParameter("bankAccountNo"))
        );

        logger.debug(dailyBasisMessengerDTO);
        if (addFlag) {
            Daily_basis_messengerDAO.getInstance().add(dailyBasisMessengerDTO);
        } else {
            Daily_basis_messengerDAO.getInstance().update(dailyBasisMessengerDTO);
        }
        return dailyBasisMessengerDTO;
    }

    @Override
    public String getTableName() {
        return Daily_basis_messengerDAO.getInstance().getTableName();
    }

    @Override
    public String getServletName() {
        return "Daily_basis_messengerServlet";
    }

    @Override
    public Daily_basis_messengerDAO getCommonDAOService() {
        return Daily_basis_messengerDAO.getInstance();
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[]{MenuConstants.DAILY_BASIS_MESSENGER_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[]{MenuConstants.DAILY_BASIS_MESSENGER_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[]{MenuConstants.DAILY_BASIS_MESSENGER_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Daily_basis_messengerServlet.class;
    }
}