package daily_basis_messenger;

import common.CommonDAOService;
import org.apache.log4j.Logger;
import util.CommonDTO;
import util.StringUtils;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Daily_basis_messengerDAO implements CommonDAOService<Daily_basis_messengerDTO> {
    private static final Logger logger = Logger.getLogger(Daily_basis_messengerDAO.class);

    private static final String addQuery =
            "INSERT INTO {tableName} (name,father_name,daily_rate,mobile_no,bank_account_no,search_column,"
                    .concat("modified_by,lastModificationTime,inserted_by,insertion_time,isDeleted,ID)")
                    .concat("VALUES (?,?,?,?,?,?,?,?,?,?,?,?)");

    private static final String updateQuery =
            "UPDATE {tableName} SET name=?,father_name=?,daily_rate=?,mobile_no=?,bank_account_no=?,search_column=?,"
                    .concat("modified_by=?,lastModificationTime=? WHERE ID=?");

    private static final String getAllDTOs =
            "SELECT * FROM daily_basis_messenger WHERE isDeleted=0";

    private Daily_basis_messengerDAO() {
        searchMap.put("name", " and (name like ?)");
        searchMap.put("mobile_no", " and (mobile_no like ?)");
        searchMap.put("AnyField", " and (search_column like ?)");
    }

    private final Map<String, String> searchMap = new HashMap<>();

    private static class LazyLoader {
        static final Daily_basis_messengerDAO INSTANCE = new Daily_basis_messengerDAO();
    }

    public static Daily_basis_messengerDAO getInstance() {
        return LazyLoader.INSTANCE;
    }

    public void setSearchColumn(Daily_basis_messengerDTO dto) {
        dto.searchColumn = dto.name + " "
                + dto.mobileNo + " " + StringUtils.convertToBanNumber(dto.mobileNo);
    }

    @Override
    public void set(PreparedStatement ps, Daily_basis_messengerDTO dto, boolean isInsert) throws SQLException {
        setSearchColumn(dto);
        int index = 0;
        ps.setString(++index, dto.name);
        ps.setString(++index, dto.fatherName);
        ps.setLong(++index, dto.dailyRate);
        ps.setString(++index, dto.mobileNo);
        ps.setString(++index, dto.bankAccountNo);
        ps.setString(++index, dto.searchColumn);
        ps.setLong(++index, dto.modifiedBy);
        ps.setLong(++index, dto.lastModificationTime);
        if (isInsert) {
            ps.setLong(++index, dto.insertedBy);
            ps.setLong(++index, dto.insertionTime);
            ps.setObject(++index, 0 /* isDeleted */);
        }
        ps.setLong(++index, dto.iD);
    }

    @Override
    public Daily_basis_messengerDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Daily_basis_messengerDTO dto = new Daily_basis_messengerDTO();
            dto.iD = rs.getLong("ID");
            dto.name = rs.getString("name");
            dto.fatherName = rs.getString("father_name");
            dto.dailyRate = rs.getLong("daily_rate");
            dto.mobileNo = rs.getString("mobile_no");
            dto.bankAccountNo = rs.getString("bank_account_no");
            dto.searchColumn = rs.getString("search_column");
            dto.modifiedBy = rs.getLong("modified_by");
            dto.insertedBy = rs.getLong("inserted_by");
            dto.insertionTime = rs.getLong("insertion_time");
            dto.isDeleted = rs.getInt("isDeleted");
            dto.lastModificationTime = rs.getLong("lastModificationTime");
            return dto;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    public Daily_basis_messengerDTO getDTOByID(long id) {
        return getDTOFromID(id);
    }

    @Override
    public String getTableName() {
        return "daily_basis_messenger";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    @Override
    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Daily_basis_messengerDTO) commonDTO, addQuery, true);
    }

    @Override
    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Daily_basis_messengerDTO) commonDTO, updateQuery, false);
    }

    public List<Daily_basis_messengerDTO> getAllActiveMessengers() {
        return getDTOs(getAllDTOs);
    }
}