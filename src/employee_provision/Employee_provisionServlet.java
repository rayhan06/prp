package employee_provision;

import common.BaseServlet;
import employee_records.Employee_recordsDAO;
import employee_records.Employee_recordsDTO;
import employee_records.Employee_recordsRepository;
import files.FilesDAO;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import permission.MenuConstants;
import user.UserDTO;
import util.CommonConstant;
import util.CommonDTO;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import java.util.Date;


@WebServlet("/Employee_provisionServlet")
@MultipartConfig
public class Employee_provisionServlet extends BaseServlet {
    public static final Logger logger = Logger.getLogger(Employee_provisionServlet.class);

    public static final long MONTH_IN_MILISECONDS = 30 * 24 * 60 * 60 * 1000L;

    FilesDAO filesDAO = new FilesDAO();
    private final Employee_provisionDAO employee_provisionDAO = Employee_provisionDAO.getInstance();

    @Override
    public String getTableName() {
        return "employee_provision";
    }

    @Override
    public String getServletName() {
        return "Employee_provisionServlet";
    }

    @Override
    public Employee_provisionDAO getCommonDAOService() {
        return employee_provisionDAO;
    }

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        boolean isLangEng = userDTO.languageID == CommonConstant.Language_ID_English;
        request.setAttribute("failureMessage", "");
        Employee_provisionDTO employee_provisionDTO;

        if (addFlag) {
            employee_provisionDTO = new Employee_provisionDTO();
            employee_provisionDTO.employeeRecordsId = Long.parseLong(request.getParameter("employeeRecordsId"));
            employee_provisionDTO.insertedBy = userDTO.employee_record_id;
            employee_provisionDTO.insertionTime = new Date().getTime();
        } else {
            employee_provisionDTO = employee_provisionDAO.getDTOByID(Long.parseLong(request.getParameter("iD")));
        }
        employee_provisionDTO.modifiedBy = userDTO.employee_record_id;
        employee_provisionDTO.lastModificationTime = new Date().getTime();
        employee_provisionDTO.employeeRecordsId = Long.parseLong(request.getParameter("employeeRecordsId"));
        employee_provisionDTO.startTime = Long.parseLong(request.getParameter("startTime"));
        employee_provisionDTO.duration = Integer.parseInt(request.getParameter("duration"));
        employee_provisionDTO.endTime = employee_provisionDTO.startTime + employee_provisionDTO.duration * MONTH_IN_MILISECONDS;

        if (employee_provisionDTO.startTime > employee_provisionDTO.endTime)
            throw new Exception(
                    isLangEng ? "From date can not be greater than to date"
                            : "তারিখ হতে, তারিখ পর্যন্ত থেকে বড় হতে পারে না"
            );

        Employee_recordsDTO employeeRecordsDTO = Employee_recordsRepository.getInstance().getById(employee_provisionDTO.employeeRecordsId);
        employeeRecordsDTO.provisionEndDate = employee_provisionDTO.endTime;
        employeeRecordsDTO.provisionPeriod = employee_provisionDTO.duration;
        new Employee_recordsDAO().updateEmployee_records(employeeRecordsDTO, null);

        long currentTime = new Date().getTime();

        employee_provisionDTO.isDone = currentTime > employee_provisionDTO.endTime;
        String Value = request.getParameter("assessmentReportFile");

        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }
        if (Value != null && !Value.equalsIgnoreCase("")) {


            if (!Value.equalsIgnoreCase("")) {
                employee_provisionDTO.assessmentReportFile = Long.parseLong(Value);
            }

            if (!addFlag) {
                String filesDropzoneFilesToDelete = request.getParameter("filesDropzoneFilesToDelete");
                String[] deleteArray = filesDropzoneFilesToDelete.split(",");
                for (int i = 0; i < deleteArray.length; i++) {
                    if (i > 0) {
                        filesDAO.delete(Long.parseLong(deleteArray[i]));
                    }
                }
            }
        }
        System.out.println("Done adding  addEmployee_provision dto = " + employee_provisionDTO);

        if (addFlag) {
            employee_provisionDAO.add(employee_provisionDTO);
        } else {
            employee_provisionDAO.update(employee_provisionDTO);
        }
        return employee_provisionDTO;
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[]{MenuConstants.EMPLOYEE_PROVISION_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[]{MenuConstants.EMPLOYEE_PROVISION_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[]{MenuConstants.EMPLOYEE_PROVISION_SEARCH};
    }


    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Employee_provisionServlet.class;
    }


}

