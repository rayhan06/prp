package employee_provision;
import sessionmanager.SessionConstants;
import util.*;


public class Employee_provisionDTO extends CommonDTO
{

	public long employeeRecordsId = -1;
	public long startTime = SessionConstants.MIN_DATE;
	public long endTime = SessionConstants.MIN_DATE;
	public int duration = 0;
	public boolean isDone = false;
	public long assessmentReportFile = 0;
	public long insertedBy = -1;
	public long insertionTime = -1;
	public long modifiedBy = -1;
	
	
    @Override
	public String toString() {
            return "$Employee_provisionDTO[" +
            " iD = " + iD +
            " employeeRecordsId = " + employeeRecordsId +
            " startTime = " + startTime +
            " endTime = " + endTime +
            " duration = " + duration +
            " isDone = " + isDone +
            " assesmentReportFile = " + assessmentReportFile +
            " insertedBy = " + insertedBy +
            " insertionTime = " + insertionTime +
            " isDeleted = " + isDeleted +
            " modifiedBy = " + modifiedBy +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}