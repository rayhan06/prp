package employee_provision;

import common.EmployeeCommonDAOService;
import org.apache.log4j.Logger;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings({"unused", "Duplicates"})
public class Employee_provisionDAO implements EmployeeCommonDAOService<Employee_provisionDTO> {

    private static final Logger logger = Logger.getLogger(Employee_provisionDAO.class);


    private static final String addQuery = "INSERT INTO {tableName} (employee_records_id, start_time,end_time, duration, is_Done, assessment_report_file, inserted_by, insertion_time, " +
            "modified_by, lastModificationTime, isDeleted, ID) " +
            "VALUES(?,?,?,?,?,?,?,?,?,?,?,?)";

    private static final String updateQuery = "UPDATE {tableName} SET employee_records_id=?, start_time=?, end_time=?, duration=?,is_Done=?, assessment_report_file=?, " +
            "inserted_by=?, insertion_time=?, modified_by=?,lastModificationTime=? WHERE ID = ?";
    private static final Map<String, String> searchMap = new HashMap<>();

    private static final String getByEmployeeId = "SELECT * FROM %s WHERE employee_records_id=%d AND isDeleted=0";

    private Employee_provisionDAO() {
        searchMap.put("startTime", " and (start_time >= ?)");
        searchMap.put("endTime", " and (end_time <= ?)");
        searchMap.put("AnyField", " and (search_column like ?)");
    }

    private static class LazyLoader {
        static final Employee_provisionDAO INSTANCE = new Employee_provisionDAO();
    }

    public static Employee_provisionDAO getInstance() {
        return Employee_provisionDAO.LazyLoader.INSTANCE;
    }

    public void setSearchColumn(Employee_provisionDTO employee_provisionDTO) {
        employee_provisionDTO.searchColumn = "";
        employee_provisionDTO.searchColumn += employee_provisionDTO.insertedBy + " ";
    }


    @Override
    public void set(PreparedStatement ps, Employee_provisionDTO employee_provisionDTO, boolean isInsert) throws SQLException {
        int index = 0;
        setSearchColumn(employee_provisionDTO);
        ps.setObject(++index, employee_provisionDTO.employeeRecordsId);
        ps.setObject(++index, employee_provisionDTO.startTime);
        ps.setObject(++index, employee_provisionDTO.endTime);
        ps.setObject(++index, employee_provisionDTO.duration);
        ps.setObject(++index, employee_provisionDTO.isDone);
        ps.setObject(++index, employee_provisionDTO.assessmentReportFile);
        ps.setObject(++index, employee_provisionDTO.insertedBy);
        ps.setObject(++index, employee_provisionDTO.insertionTime);
        ps.setObject(++index, employee_provisionDTO.modifiedBy);
        ps.setObject(++index, System.currentTimeMillis());

        if (isInsert) {
            ps.setObject(++index, 0);
        }
        ps.setObject(++index, employee_provisionDTO.iD);


    }

    @Override
    public Employee_provisionDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Employee_provisionDTO employee_provisionDTO = new Employee_provisionDTO();
            employee_provisionDTO.iD = rs.getLong("ID");
            employee_provisionDTO.employeeRecordsId = rs.getLong("employee_records_id");
            employee_provisionDTO.startTime = rs.getLong("start_time");
            employee_provisionDTO.endTime = rs.getLong("end_time");
            employee_provisionDTO.duration = rs.getInt("duration");
            employee_provisionDTO.isDone = rs.getBoolean("is_Done");
            employee_provisionDTO.assessmentReportFile = rs.getLong("assessment_report_file");
            employee_provisionDTO.insertedBy = rs.getLong("inserted_by");
            employee_provisionDTO.insertionTime = rs.getLong("insertion_time");
            employee_provisionDTO.isDeleted = rs.getInt("isDeleted");
            employee_provisionDTO.modifiedBy = rs.getLong("modified_by");
            employee_provisionDTO.lastModificationTime = rs.getLong("lastModificationTime");
            return employee_provisionDTO;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    @Override
    public String getTableName() {
        return "employee_provision";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Employee_provisionDTO) commonDTO, addQuery, true);
    }


    public Employee_provisionDTO getDTOByID(long ID) {
        return getDTOFromID(ID);

    }

    public Employee_provisionDTO getDTOByEmployeeId(long employeeId) {
        List<Employee_provisionDTO> dtoList = getDTOs(String.format(getByEmployeeId, "employee_provision", employeeId));
        if (dtoList == null || dtoList.size() == 0)
            return null;
        return dtoList.get(0);
    }

    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Employee_provisionDTO) commonDTO, updateQuery, false);
    }

    public List<Employee_provisionDTO> getDTOsByEmployeeId(long employeeId) {
        return getByEmployeeId(employeeId);
    }


    public List<Employee_provisionDTO> getAllEmployee_provision(boolean isFirstReload) {
        return getAllDTOs(isFirstReload);
    }

}
	