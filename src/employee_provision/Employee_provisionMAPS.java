package employee_provision;

import util.CommonMaps;


public class Employee_provisionMAPS extends CommonMaps {
    public Employee_provisionMAPS(String tableName) {


        java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
        java_DTO_map.put("employeeRecordsId".toLowerCase(), "employeeRecordsId".toLowerCase());
        java_DTO_map.put("startTime".toLowerCase(), "startTime".toLowerCase());
        java_DTO_map.put("endTime".toLowerCase(), "endTime".toLowerCase());
        java_DTO_map.put("duration".toLowerCase(), "duration".toLowerCase());
        java_DTO_map.put("isDone".toLowerCase(), "isDone".toLowerCase());
        java_DTO_map.put("assesmentReportFile".toLowerCase(), "assesmentReportFile".toLowerCase());
        java_DTO_map.put("insertedBy".toLowerCase(), "insertedBy".toLowerCase());
        java_DTO_map.put("insertionTime".toLowerCase(), "insertionTime".toLowerCase());
        java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
        java_DTO_map.put("modifiedBy".toLowerCase(), "modifiedBy".toLowerCase());
        java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

        java_SQL_map.put("start_time".toLowerCase(), "startTime".toLowerCase());
        java_SQL_map.put("end_time".toLowerCase(), "endTime".toLowerCase());
        java_SQL_map.put("duration".toLowerCase(), "duration".toLowerCase());
        java_SQL_map.put("is_Done".toLowerCase(), "isDone".toLowerCase());

        java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
        java_Text_map.put("Employee Records Id".toLowerCase(), "employeeRecordsId".toLowerCase());
        java_Text_map.put("Start Time".toLowerCase(), "startTime".toLowerCase());
        java_Text_map.put("End Time".toLowerCase(), "endTime".toLowerCase());
        java_Text_map.put("Duration".toLowerCase(), "duration".toLowerCase());
        java_Text_map.put("Is Done".toLowerCase(), "isDone".toLowerCase());
        java_Text_map.put("Assesment Report File".toLowerCase(), "assesmentReportFile".toLowerCase());
        java_Text_map.put("Inserted By".toLowerCase(), "insertedBy".toLowerCase());
        java_Text_map.put("Insertion Time".toLowerCase(), "insertionTime".toLowerCase());
        java_Text_map.put("Is Deleted".toLowerCase(), "isDeleted".toLowerCase());
        java_Text_map.put("Modified By".toLowerCase(), "modifiedBy".toLowerCase());
        java_Text_map.put("Last Modification Time".toLowerCase(), "lastModificationTime".toLowerCase());

    }

}