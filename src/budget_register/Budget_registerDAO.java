package budget_register;

import common.CommonDAOService;
import common.ConnectionAndStatementUtil;
import org.apache.log4j.Logger;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings({"Duplicates"})
public class Budget_registerDAO implements CommonDAOService<Budget_registerDTO> {
    private static final Logger logger = Logger.getLogger(Budget_registerDAO.class);

    private static final String addSqlQuery =
            "INSERT INTO {tableName} (supplier_institution_id,recipient_name, issue_number, issue_date, description, bill_amount, upto_last_bill_total, total_cost, remaining_budget,"
                    .concat("budget_selection_info_id, budget_office_id, budget_mapping_id, economic_sub_code_id,")
                    .concat("allocated_budget,metadata_class_name,metadata_json_string,modified_by,lastModificationTime,inserted_by,insertion_time, isDeleted, ID) ")
                    .concat("VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"); // fields 8 + 4 + 10

    private static final String updateSqlQuery =
            "UPDATE {tableName} SET supplier_institution_id=?,recipient_name=?, issue_number=?, issue_date=?, description=?, bill_amount=?, upto_last_bill_total=?,"
                    .concat("total_cost=?,remaining_budget=?,budget_selection_info_id=?, budget_office_id=?, budget_mapping_id=?,")
                    .concat("economic_sub_code_id=?,allocated_budget=?,metadata_class_name=?,metadata_json_string=?,modified_by=?, lastModificationTime=? WHERE ID=?");

    private static final Map<String, String> searchMap = new HashMap<>();

    private Budget_registerDAO() {
        searchMap.put("budget_selection_info_id", " AND (budget_selection_info_id = ?)");
        searchMap.put("budget_mapping_id", " AND (budget_mapping_id = ?)");
        searchMap.put("budget_office_id", " AND (budget_office_id = ?)");
        searchMap.put("economic_sub_code_id", " AND (economic_sub_code_id = ?)");
        searchMap.put("issue_number", " AND (issue_number LIKE ?)");
        searchMap.put("voucher_number", " AND (ID LIKE ?)");
        searchMap.put("date_from", " AND (insertion_time >= ?)");
        searchMap.put("date_to", " AND (insertion_time <= ?)");
    }

    private static class LazyLoader {
        static final Budget_registerDAO INSTANCE = new Budget_registerDAO();
    }

    public static Budget_registerDAO getInstance() {
        return Budget_registerDAO.LazyLoader.INSTANCE;
    }

    @Override
    public void set(PreparedStatement ps, Budget_registerDTO dto, boolean isInsert) throws SQLException {
        dto.performBillAndBudgetCalculations();
        int index = 0;
        ps.setLong(++index, dto.supplierInstitutionId);
        ps.setString(++index, dto.recipientName);
        ps.setString(++index, dto.issueNumber);
        ps.setLong(++index, dto.issueDate);
        ps.setString(++index, dto.description);
        ps.setLong(++index, dto.billAmount);
        ps.setLong(++index, dto.uptoLastBillTotal);
        ps.setLong(++index, dto.totalCost);
        ps.setLong(++index, dto.remainingBudget);
        ps.setLong(++index, dto.budgetSelectionInfoId);
        ps.setLong(++index, dto.budgetOfficeId);
        ps.setLong(++index, dto.budgetMappingId);
        ps.setLong(++index, dto.economicSubCodeId);
        ps.setLong(++index, dto.allocatedBudget);
        if(dto.budgetRegisterMetadata != null) {
            ps.setString(++index, dto.budgetRegisterMetadata.className);
            ps.setString(++index, dto.budgetRegisterMetadata.jsonString);
        } else {
            ps.setString(++index, null);
            ps.setString(++index, null);
        }
        ps.setLong(++index, dto.modifiedBy);
        ps.setLong(++index, dto.lastModificationTime);
        if (isInsert) {
            ps.setLong(++index, dto.insertedBy);
            ps.setLong(++index, dto.insertionTime);
            ps.setInt(++index, 0 /* isDeleted */);
        }
        ps.setLong(++index, dto.iD);
    }

    @Override
    public Budget_registerDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Budget_registerDTO dto = new Budget_registerDTO();
            dto.iD = rs.getLong("ID");
            dto.supplierInstitutionId = rs.getLong("supplier_institution_id");
            dto.recipientName = rs.getString("recipient_name");
            dto.issueNumber = rs.getString("issue_number");
            dto.issueDate = rs.getLong("issue_date");
            dto.description = rs.getString("description");
            dto.billAmount = rs.getLong("bill_amount");
            dto.uptoLastBillTotal = rs.getLong("upto_last_bill_total");
            dto.totalCost = rs.getLong("total_cost");
            dto.remainingBudget = rs.getLong("remaining_budget");
            dto.budgetSelectionInfoId = rs.getLong("budget_selection_info_id");
            dto.budgetOfficeId = rs.getLong("budget_office_id");
            dto.budgetMappingId = rs.getLong("budget_mapping_id");
            dto.economicSubCodeId = rs.getLong("economic_sub_code_id");
            dto.allocatedBudget = rs.getLong("allocated_budget");
            dto.budgetRegisterMetadata = new Budget_registerMetadata(
                    rs.getString("metadata_class_name"),
                    rs.getString("metadata_json_string")
            );
            dto.modifiedBy = rs.getLong("modified_by");
            dto.lastModificationTime = rs.getLong("lastModificationTime");
            dto.insertedBy = rs.getLong("inserted_by");
            dto.insertionTime = rs.getLong("insertion_time");
            dto.isDeleted = rs.getInt("isDeleted");
            return dto;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    @Override
    public String getTableName() {
        return "budget_register";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    @Override
    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Budget_registerDTO) commonDTO, addSqlQuery, true);
    }

    @Override
    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Budget_registerDTO) commonDTO, updateSqlQuery, false);
    }

    public Budget_registerModel getModelById(long id) {
        Budget_registerDTO budgetRegisterDTO = getDTOFromID(id);
        return budgetRegisterDTO == null ? null : new Budget_registerModel(budgetRegisterDTO);
    }

    private static final String getByEconomicYearAndSector =
            "SELECT * FROM budget_register WHERE budget_selection_info_id=%d AND budget_mapping_id=%d AND "
                    .concat("economic_sub_code_id=%d AND isDeleted=0");

    public List<Budget_registerDTO> getDTOsByEconomicYearAndSector(long budgetSelectionInfoId, long budgetMappingId, long economicSubCodeId) {
        return ConnectionAndStatementUtil.getListOfT(
                String.format(getByEconomicYearAndSector, budgetSelectionInfoId, budgetMappingId, economicSubCodeId),
                this::buildObjectFromResultSet
        );
    }

    private static final String getBySupplierInstitution =
            "SELECT * FROM budget_register WHERE budget_selection_info_id=%d AND supplier_institution_id=%d AND isDeleted=0";

    public List<Budget_registerDTO> getDTOsByEconomicYearAndSupplierInstitution(long budgetSelectionInfoId, long supplierInstitutionId) {
        return ConnectionAndStatementUtil.getListOfT(
                String.format(getBySupplierInstitution, budgetSelectionInfoId, supplierInstitutionId),
                this::buildObjectFromResultSet
        );
    }
}