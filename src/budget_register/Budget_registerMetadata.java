package budget_register;

import com.google.gson.Gson;
import pb.Utils;

public class Budget_registerMetadata {
    public String className;
    public String jsonString;
    public Object object;

    private static final Gson gson = new Gson();

    public Budget_registerMetadata(Object object) {
        this.className = object.getClass().getName();
        this.jsonString = gson.toJson(object);
        this.object = object;
    }

    public Budget_registerMetadata(String className, String jsonString) {
        this.className = className;
        this.jsonString = jsonString;
        this.object = null;
        setObject();
    }

    private void setObject() {
        if (jsonString == null || className == null) {
            return;
        }
        object = gson.fromJson(jsonString, Utils.getClassByClassName(className));
    }
}
