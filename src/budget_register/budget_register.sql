CREATE TABLE budget_register
(
    ID                       BIGINT(20) PRIMARY KEY, -- voucher number ভাউচার নম্বর
    recipient_name           VARCHAR(2048),          -- Recipient Name প্রাপকের নাম
    issue_number             VARCHAR(50),            -- No নং
    issue_date               BIGINT(20),             -- Date তাং
    description              VARCHAR(4096),
    bill_amount              BIGINT(20),             -- Bill Amount বিলের টাকার পরিমাণ
    total_cost               BIGINT(20),             -- Total Cost মোট খরচ
    remaining_budget         BIGINT(20),             -- Remaining অবশিষ্ট
    budget_selection_info_id BIGINT(20),             -- Economic Year অর্থবছর
    budget_office_id         BIGINT(20),             -- Office কার্যালয়
    budget_mapping_id        BIGINT(20),             -- Budget Operation Code বাজেট অপারেশন কোড
    economic_sub_code_id     BIGINT(20),             -- Economic Code অর্থনৈতিক কোড
    allocated_budget         BIGINT(20),             -- বাজেট বরাদ্দ
    modified_by              BIGINT(20),
    lastModificationTime     BIGINT(20) DEFAULT -62135791200000,
    inserted_by              BIGINT(20),
    insertion_time           BIGINT(20) DEFAULT -62135791200000,
    isDeleted                INT(11)    DEFAULT 0
) ENGINE = MyISAM
  DEFAULT CHARSET = utf8
  COLLATE = utf8_unicode_ci;

DELETE FROM vbSequencer WHERE table_name = 'budget_register';
INSERT INTO vbSequencer (table_name, next_id, table_LastModificationTime) VALUES ('budget_register', 1, 0);