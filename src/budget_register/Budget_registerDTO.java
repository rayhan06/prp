package budget_register;

import budget.BudgetAmountModel;
import budget.BudgetDAO;
import sessionmanager.SessionConstants;
import util.CommonDTO;

import java.util.List;

public class Budget_registerDTO extends CommonDTO {
    public long supplierInstitutionId = -1;
    public String recipientName = "";
    public String issueNumber = "";
    public long issueDate = SessionConstants.MIN_DATE;
    public String description = "";
    public long billAmount = 0;
    public long uptoLastBillTotal = 0;
    public long totalCost = 0;
    public long remainingBudget = 0;
    public long budgetSelectionInfoId = -1;
    public long budgetOfficeId = -1;
    public long budgetMappingId = -1;
    public long economicSubCodeId = -1;
    public long allocatedBudget = 0;
    public long modifiedBy = -1;
    public long insertedBy = -1;
    public long insertionTime = SessionConstants.MIN_DATE;
    public Budget_registerMetadata budgetRegisterMetadata;

    public void performBillAndBudgetCalculations() {
        List<Budget_registerDTO> billsOfEconomicYearAndSector =
                Budget_registerDAO
                        .getInstance()
                        .getDTOsByEconomicYearAndSector(
                                budgetSelectionInfoId,
                                budgetMappingId,
                                economicSubCodeId
                        );
        uptoLastBillTotal =
                billsOfEconomicYearAndSector
                        .stream()
                        .filter(budgetRegisterDTO -> budgetRegisterDTO.iD != this.iD)
                        .mapToLong(budgetRegisterDTO -> budgetRegisterDTO.billAmount)
                        .sum();
        BudgetAmountModel budgetAmountModel =
                BudgetDAO.getInstance().getAllocatedBudget(
                        issueDate,
                        budgetSelectionInfoId,
                        budgetMappingId,
                        economicSubCodeId
                );
        allocatedBudget = budgetAmountModel.allocatedBudget;
        totalCost = budgetAmountModel.initialExpenditureAmount + uptoLastBillTotal + billAmount;
        remainingBudget = Math.max(0, allocatedBudget - totalCost);
    }

    public long calculateSupplierTotalBill() {
        return Budget_registerDAO.getInstance()
                                 .getDTOsByEconomicYearAndSupplierInstitution(budgetSelectionInfoId, supplierInstitutionId)
                                 .stream()
                                 .mapToLong(budgetRegisterDTO -> budgetRegisterDTO.billAmount)
                                 .sum();
    }

    @Override
    public String toString() {
        return "Budget_registerDTO{" +
               "recipientName='" + recipientName + '\'' +
               ", issueNumber='" + issueNumber + '\'' +
               ", issueDate=" + issueDate +
               ", description='" + description + '\'' +
               ", billAmount=" + billAmount +
               ", totalCost=" + totalCost +
               ", remainingBudget=" + remainingBudget +
               ", budgetSelectionInfoId=" + budgetSelectionInfoId +
               ", budgetOfficeId=" + budgetOfficeId +
               ", budgetMappingId=" + budgetMappingId +
               ", economicSubCodeId=" + economicSubCodeId +
               ", allocatedBudget=" + allocatedBudget +
               ", iD=" + iD +
               ", lastModificationTime=" + lastModificationTime +
               '}';
    }
}
