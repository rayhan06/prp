package budget_register;

import sessionmanager.SessionConstants;

public class Budget_registerModel {
    public String issueNumber = "";
    public long issueDate = SessionConstants.MIN_DATE;

    public Budget_registerModel(Budget_registerDTO dto) {
        issueNumber = dto.issueNumber;
        issueDate = dto.issueDate;
    }
}
