package budget_register;

import common.BaseServlet;
import login.LoginDTO;
import org.apache.log4j.Logger;
import permission.MenuConstants;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/Budget_registerServlet")
@MultipartConfig
@SuppressWarnings({"Duplicates"})
public class Budget_registerServlet extends BaseServlet {
    private static final long serialVersionUID = 1L;
    private static final Logger logger = Logger.getLogger(Budget_registerServlet.class);

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }
        String actionType = request.getParameter("actionType");
        // only handles search from user
        if ("search".equals(actionType)) {
            super.doGet(request, response);
            return;
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // no POST request support from user end
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        return null;
    }

    @Override
    public String getTableName() {
        return Budget_registerDAO.getInstance().getTableName();
    }

    @Override
    public String getServletName() {
        return "Budget_registerServlet";
    }

    @Override
    public Budget_registerDAO getCommonDAOService() {
        return Budget_registerDAO.getInstance();
    }

    @Override
    public int[] getAddPageMenuConstants() {
        // no add support from user
        return new int[]{};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        // no edit support from user
        return new int[]{};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[]{MenuConstants.BUDGET_REGISTER_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Budget_registerServlet.class;
    }
}