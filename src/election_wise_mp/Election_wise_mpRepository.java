package election_wise_mp;

import election_constituency.Election_constituencyDTO;
import election_constituency.Election_constituencyRepository;
import election_details.Election_detailsDTO;
import election_details.Election_detailsRepository;
import employee_records.Employee_recordsDTO;
import employee_records.Employee_recordsRepository;
import employee_records.MpStatusEnum;
import org.apache.log4j.Logger;
import pb.OptionDTO;
import pb.Utils;
import political_party.Political_partyRepository;
import repository.Repository;
import repository.RepositoryManager;
import util.CommonDTO;
import util.LockManager;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.stream.Collectors;

@SuppressWarnings({"unused", "Duplicates"})
public class Election_wise_mpRepository implements Repository {
    private final Election_wise_mpDAO election_wise_mpDAO = Election_wise_mpDAO.getInstance();

    private static final Logger logger = Logger.getLogger(Election_wise_mpRepository.class);

    private final Map<Long, Election_wise_mpDTO> mapById;
    private final Map<Long, List<Election_wise_mpDTO>> mapByElectionDetailsId;
    private final Map<Long, List<Election_wise_mpDTO>> mapByEmployeeId;
    private static final ReentrantReadWriteLock lock = new ReentrantReadWriteLock();
    private static final ReentrantReadWriteLock.ReadLock readLock = lock.readLock();
    private static final ReentrantReadWriteLock.WriteLock writeLock = lock.writeLock();


    private Election_wise_mpRepository() {
        mapById = new HashMap<>();
        mapByElectionDetailsId = new HashMap<>();
        mapByEmployeeId = new HashMap<>();
        RepositoryManager.getInstance().addRepository(this);
    }

    private static class LazyLoader {
        static final Election_wise_mpRepository INSTANCE = new Election_wise_mpRepository();
    }

    public static Election_wise_mpRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

    public synchronized void reload(boolean reloadAll) {

        logger.debug("Loading start for Election_wise_mpRepository, reloadAll : " + reloadAll);
        List<Election_wise_mpDTO> dtoList = election_wise_mpDAO.getAllDTOs(reloadAll);
        updateCacheByList(dtoList);
        logger.debug("Loading end for Election_wise_mpRepository, reloadAll : " + reloadAll);

    }

    private void updateCacheByList(List<Election_wise_mpDTO> dtoList) {
        if (dtoList == null || dtoList.isEmpty()) {
            return;
        }
        try {
            writeLock.lock();
            dtoList.forEach(this::updateCache);
        } finally {
            writeLock.unlock();
        }
    }

    private void addDTO(Election_wise_mpDTO dto) {
        if (dto != null) {
            mapById.put(dto.iD, dto);
            List<Election_wise_mpDTO> electionWiseList = mapByElectionDetailsId.getOrDefault(dto.electionDetailsId, new ArrayList<>());
            electionWiseList.add(dto);
            mapByElectionDetailsId.put(dto.electionDetailsId, electionWiseList);
            List<Election_wise_mpDTO> list = mapByEmployeeId.getOrDefault(dto.employeeRecordsId, new ArrayList<>());
            list.add(dto);
            mapByEmployeeId.put(dto.employeeRecordsId, list);
        }
    }

    public List<Election_wise_mpDTO> getAll() {
        return new ArrayList<>(this.mapById.values());
    }

    public Election_wise_mpDTO getDTOByID(long ID) {
        Election_wise_mpDTO dto;
        boolean hasInCache = true;
        try {
            readLock.lock();
            dto = mapById.get(ID);
            if (dto == null) {
                synchronized (LockManager.getLock(ID + "EWMP")) {
                    dto = mapById.get(ID);
                    if (dto == null) {
                        hasInCache = false;
                        dto = election_wise_mpDAO.getDTOFromID(ID);
                    }
                }
            }
        } finally {
            readLock.unlock();
        }
        if (dto != null && !hasInCache) {
            updateCacheInSeparateThread(dto);
        }
        return dto;
    }

    public List<Election_wise_mpDTO> getElection_wise_mpDTOByElectionDetailsId(long election_details_id) {
        List<Election_wise_mpDTO> list;
        try {
            readLock.lock();
            list = mapByElectionDetailsId.get(election_details_id);
        } finally {
            readLock.unlock();
        }
        return list;
    }

    public ElectionWiseMpModel getLatestElection_wise_mpDTO(long empId) {
        try {
            readLock.lock();
            List<Election_wise_mpDTO> list = mapByEmployeeId.get(empId);
            if (list == null || list.size() == 0) {
                return null;
            }
            Election_wise_mpDTO dto = list.get(0);
            ElectionWiseMpModel model = new ElectionWiseMpModel();
            model.electionDetailsDTO = Election_detailsRepository.getInstance().getElectionDetailsDTOByID(dto.electionDetailsId);
            model.electionConstituencyDTO = Election_constituencyRepository.getInstance().getElection_constituencyDTOByID(dto.electionConstituencyId);
            model.politicalPartyDTO = Political_partyRepository.getInstance().getPolitical_partyDTOByID(dto.politicalPartyId);
            model.electionWiseMpDTO = dto;
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            model.startDate = sdf.format(new Date(dto.startDate));
            model.endDate = sdf.format(new Date(dto.endDate));
            return model;
        } finally {
            readLock.unlock();
        }
    }

    public Election_wise_mpDTO getByElectionAndConstituency(long electionId, long constituencyId) {
        try {
            readLock.lock();
            return mapByElectionDetailsId.entrySet()
                                         .stream().filter(entry -> entry.getKey() == electionId)
                                         .flatMap(entry -> entry.getValue().stream())
                                         .filter(dto -> dto.electionConstituencyId == constituencyId)
                                         .findAny()
                                         .orElse(null);
        } finally {
            readLock.unlock();
        }
    }

    public Election_wise_mpDTO getActiveMPByElectionAndConstituency(long electionId, long constituencyId) {
        try {
            readLock.lock();
            return mapByElectionDetailsId.entrySet()
                                         .stream().filter(entry -> entry.getKey() == electionId)
                                         .flatMap(entry -> entry.getValue().stream())
                                         .filter(dto -> dto.electionConstituencyId == constituencyId && dto.mpStatus == MpStatusEnum.ACTIVE.getValue())
                                         .findAny()
                                         .orElse(null);
        } finally {
            readLock.unlock();
        }
    }

    public Election_wise_mpDTO getByElectionAndRecordsId(long electionId, long employeeRecordsId) {
        return mapByElectionDetailsId.entrySet()
                                     .stream().filter(entry -> entry.getKey() == electionId)
                                     .flatMap(entry -> entry.getValue().stream())
                                     .filter(dto -> dto.employeeRecordsId == employeeRecordsId)
                                     .findAny()
                                     .orElse(null);
    }

    public Election_wise_mpDTO getByElectionAndConstituency(String employeeNumber) {
        int parliament = Integer.parseInt(employeeNumber.substring(1, 3));
        int constituency = Integer.parseInt(employeeNumber.substring(3, 6));
        Election_detailsDTO electionDetailsDTO = Election_detailsRepository.getInstance().getElection_detailsDTOByParliamentNumber(parliament);
        Election_constituencyDTO constituencyDTO = Election_constituencyRepository.getInstance().getElection_constituencyDTOByConstituencyNumber(electionDetailsDTO.iD, constituency);
        return getByElectionAndConstituency(electionDetailsDTO.iD, constituencyDTO.iD);
    }

    public Employee_recordsDTO getMpRecordsDTOByElectionAndConstituency(long electionDetailsId, long electionConstituencyId) {
        Election_wise_mpDTO electionWiseMpDTO = getByElectionAndConstituency(electionDetailsId, electionConstituencyId);
        return electionWiseMpDTO == null ? null
                                         : Employee_recordsRepository.getInstance().getById(electionWiseMpDTO.employeeRecordsId);
    }

    @Override
    public String getTableName() {
        return "election_wise_mp";
    }

    public Election_wise_mpDTO getByUserName(String userName) {
        Election_wise_mpDTO dto;
        boolean hasInCache = true;
        try {
            readLock.lock();
            dto = mapById.values()
                         .stream()
                         .filter(e -> e.userName.equals(userName))
                         .findAny()
                         .orElse(null);
            if (dto == null) {
                hasInCache = false;
                dto = election_wise_mpDAO.getByUserName(userName);
            }
        } finally {
            readLock.unlock();
        }
        if (dto != null && !hasInCache) {
            updateCacheInSeparateThread(dto);
        }
        return dto;
    }

    private void updateCacheInSeparateThread(Election_wise_mpDTO newDTO) {
        Thread thread = new Thread(() -> updateCacheInSameThread(newDTO));
        thread.setDaemon(true);
        thread.start();
    }

    private void updateCacheInSameThread(Election_wise_mpDTO newDTO) {
        try {
            writeLock.lock();
            updateCache(newDTO);
        } finally {
            writeLock.unlock();
        }
    }

    @Override
    public void updateCache(CommonDTO dto) {
        if (dto != null) {
            updateCacheInSameThread((Election_wise_mpDTO) dto);
        }
    }

    private void updateCache(Election_wise_mpDTO newDTO) {
        if (newDTO != null) {
            Election_wise_mpDTO oldDTO = mapById.get(newDTO.iD);
            if (oldDTO != null) {
                if (oldDTO.lastModificationTime > newDTO.lastModificationTime) {
                    return;
                }
                mapById.remove(newDTO.iD);
                if (mapByElectionDetailsId.get(newDTO.electionDetailsId) != null) {
                    mapByElectionDetailsId.get(newDTO.electionDetailsId).remove(oldDTO);
                }
                if (mapByEmployeeId.get(newDTO.employeeRecordsId) != null) {
                    mapByEmployeeId.get(newDTO.employeeRecordsId).remove(oldDTO);
                }
            }
            if (newDTO.isDeleted == 0) {
                if (newDTO.lastModificationTime > System.currentTimeMillis()) {
                    newDTO.lastModificationTime = System.currentTimeMillis();
                }
                mapById.put(newDTO.iD, newDTO);
                List<Election_wise_mpDTO> l1 = mapByElectionDetailsId.getOrDefault(newDTO.electionDetailsId, new ArrayList<>());
                l1.add(newDTO);
                mapByElectionDetailsId.put(newDTO.electionDetailsId, l1);
                List<Election_wise_mpDTO> l2 = mapByEmployeeId.getOrDefault(newDTO.employeeRecordsId, new ArrayList<>());
                l2.add(newDTO);
                mapByEmployeeId.put(newDTO.employeeRecordsId, l2);
            }
        }
    }

    @Override
    public void updateCache(long id) {
        Election_wise_mpDTO electionWiseMpDTO = election_wise_mpDAO.getDTOFromIdDeletedOrNot(id);
        updateCacheInSeparateThread(electionWiseMpDTO);
    }

    public void updateCache(List<Long> idList) {
        if (idList == null || idList.isEmpty()) {
            return;
        }
        List<Election_wise_mpDTO> list = election_wise_mpDAO.getDTOs(idList);
        updateCacheByList(list);
    }

    public String buildOptionsOfActiveMps(String language, String commaSeperatedSelectedIds) {
        List<OptionDTO> activeMpsOptionDTOs =
                mapById.values()
                       .stream()
                       .filter(election_wise_mpDTO -> election_wise_mpDTO.mpStatus == MpStatusEnum.ACTIVE.getValue())
                       .map(Election_wise_mpDTO::getOptionDTO)
                       .filter(Objects::nonNull)
                       .sorted(OptionDTO.defaultOrderingComparator)
                       .collect(Collectors.toList());
        return Utils.buildOptionsMultipleSelection(activeMpsOptionDTOs, language, commaSeperatedSelectedIds);
    }
}