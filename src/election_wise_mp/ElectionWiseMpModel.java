package election_wise_mp;

import election_constituency.Election_constituencyDTO;
import election_details.Election_detailsDTO;
import political_party.Political_partyDTO;

public class ElectionWiseMpModel {
    public Election_detailsDTO electionDetailsDTO;
    public Election_constituencyDTO electionConstituencyDTO;
    public Political_partyDTO politicalPartyDTO;
    public Election_wise_mpDTO electionWiseMpDTO;
    public String startDate;
    public String endDate;
}
