package election_wise_mp;

public class ElectionWiseMpModelForApi {
    public int parliamentNumber = 0;
    public int constituencyNumber = 0;
    public String constituencyNameEng = "";
    public String constituencyNameBng = "";
    public String division = "";
    public String district = "";
    public String thana = "";
    public String boundaryDetails = "";
    long politicalParty = 0;
    public String electionDate = "";
    public String startDate = "";
    public String endDate = "";
    public String status = "";

    @Override
    public String toString() {
        return "ElectionWiseMpModelForApi{" +
                "parliamentNumber=" + parliamentNumber +
                "constituencyNumber=" + constituencyNumber +
                "division=" + division +
                "district=" + district +
                "thana=" + thana +
                ", politicalParty=" + politicalParty + '\'' +
                '}';
    }
}
