package election_wise_mp;

import election_constituency.Election_constituencyDTO;
import election_constituency.Election_constituencyRepository;
import employee_records.Employee_recordsDTO;
import employee_records.Employee_recordsRepository;
import pb.OptionDTO;
import sessionmanager.SessionConstants;
import util.CommonEmployeeDTO;
import util.StringUtils;

import java.util.Arrays;


public class Election_wise_mpDTO extends CommonEmployeeDTO {

    public long electionDetailsId = 0;
    public long electionConstituencyId = 0;
    public int constituencyNumber = 0;
    public long politicalPartyId = 0;
    public long startDate = SessionConstants.MIN_DATE;
    public long endDate = SessionConstants.MIN_DATE;
    public long insertionDate = 0;
    public String insertedBy = "";
    public String modifiedBy = "";
    public String userName = "";
    public String mpRemarks = "";
    public String mpOrganization = "";
    public int mpStatus = 0;
    public long mpStatusChangeDate = SessionConstants.MIN_DATE;
    public String professionOfMP = "";
    public String addressOfMP = "";
    public byte[] alternateImageOfMP = null;
    public String presentAddress;
    public String permanentAddress;
    public boolean isAdd;
    public long dateOfBirth = SessionConstants.MIN_DATE;
    public String mpHobbies = "";
    public long mpGazetteDate = SessionConstants.MIN_DATE;
    public byte[] mpGazetteDocument = null;
    public String parliamentaryDesignation = "";
    public long oathDate = SessionConstants.MIN_DATE;
    public long electionDate = SessionConstants.MIN_DATE;

    public OptionDTO getOptionDTO() {
        Election_wise_mpInfoModel infoModel = getInfoModel();
        if(infoModel == null) {
            return null;
        }
        OptionDTO optionDTO = new OptionDTO();
        optionDTO.value = String.format("%d", iD);
        optionDTO.englishText = String.format("%s, %s", infoModel.bangladeshNumberEn, infoModel.mpNameEn);
        optionDTO.banglaText = String.format("%s, %s", infoModel.bangladeshNumberBn, infoModel.mpNameBn);
        optionDTO.orderingString = String.format("%05d", infoModel.constituencyNumber);
        return optionDTO;
    }

    public Election_wise_mpInfoModel getInfoModel() {
        Election_constituencyDTO constituencyDTO = Election_constituencyRepository.getInstance().getElection_constituencyDTOByID(electionConstituencyId);
        if (constituencyDTO == null) {
            return null;
        }
        Employee_recordsDTO employeeRecordsDTO = Employee_recordsRepository.getInstance().getById(employeeRecordsId);
        if (employeeRecordsDTO == null) {
            return null;
        }
        Election_wise_mpInfoModel infoModel = new Election_wise_mpInfoModel();
        infoModel.electionWiseMpId = iD;
        infoModel.constituencyNumber = constituencyDTO.constituencyNumber;
        infoModel.bangladeshNumberEn = String.format(
                "%d | %s",
                infoModel.constituencyNumber,
                constituencyDTO.constituencyNameEn
        );
        infoModel.bangladeshNumberBn = String.format(
                "%s | %s",
                StringUtils.convertToBanNumber(String.format("%d", infoModel.constituencyNumber)),
                constituencyDTO.constituencyNameBn
        );
        infoModel.mpNameEn = employeeRecordsDTO.nameEng;
        infoModel.mpNameBn = employeeRecordsDTO.nameBng;
        return infoModel;
    }

    @Override
    public String toString() {
        return "Election_wise_mpDTO{" +
               "electionDetailsId=" + electionDetailsId +
               ", electionConstituencyId=" + electionConstituencyId +
               ", constituencyNumber=" + constituencyNumber +
               ", politicalPartyId=" + politicalPartyId +
               ", startDate=" + startDate +
               ", endDate=" + endDate +
               ", insertionDate=" + insertionDate +
               ", insertedBy='" + insertedBy + '\'' +
               ", modifiedBy='" + modifiedBy + '\'' +
               ", userName='" + userName + '\'' +
               ", mpRemarks='" + mpRemarks + '\'' +
               ", mpOrganization='" + mpOrganization + '\'' +
               ", mpStatus=" + mpStatus +
               ", mpStatusChangeDate=" + mpStatusChangeDate +
               ", professionOfMP='" + professionOfMP + '\'' +
               ", addressOfMP='" + addressOfMP + '\'' +
               ", alternateImageOfMP=" + Arrays.toString(alternateImageOfMP) +
               ", presentAddress='" + presentAddress + '\'' +
               ", permanentAddress='" + permanentAddress + '\'' +
               ", isAdd=" + isAdd +
               ", dateOfBirth=" + dateOfBirth +
               ", mpHobbies='" + mpHobbies + '\'' +
               ", mpGazetteDate=" + mpGazetteDate +
               ", mpGazetteDocument=" + Arrays.toString(mpGazetteDocument) +
               ", parliamentaryDesignation='" + parliamentaryDesignation + '\'' +
               ", oathDate=" + oathDate +
               ", electionDate=" + electionDate +
               '}';
    }
}