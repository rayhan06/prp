package election_wise_mp;

public class Election_wise_mpInfoModel {
    public long electionWiseMpId = -1;
    public int constituencyNumber = 0;
    public String bangladeshNumberEn = "";
    public String bangladeshNumberBn = "";
    public String mpNameEn = "";
    public String mpNameBn = "";

    public static Election_wise_mpInfoModel getFromElectionWiseMpId(long electionWiseMpId) {
        Election_wise_mpDTO electionWiseMpDTO = Election_wise_mpRepository.getInstance().getDTOByID(electionWiseMpId);
        return electionWiseMpDTO == null ? null : electionWiseMpDTO.getInfoModel();
    }
}
