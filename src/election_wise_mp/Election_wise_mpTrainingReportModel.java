package election_wise_mp;

import sessionmanager.SessionConstants;

public class Election_wise_mpTrainingReportModel {
    public long electionWiseMpId = -1;
    public long employeeRecordsId = -1;
    public String trainingNameEn = "";
    public String trainingNameBn = "";
    public long country = 0;
    public long startDate = SessionConstants.MIN_DATE;
    public long endDate = SessionConstants.MIN_DATE;
}
