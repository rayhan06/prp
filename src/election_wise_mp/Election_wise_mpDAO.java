package election_wise_mp;

import common.CommonDAOService;
import common.ConnectionAndStatementUtil;
import common.EmployeeCommonDAOService;
import election_constituency.Election_constituencyDTO;
import election_constituency.Election_constituencyRepository;
import employee_records.Employee_recordsDTO;
import employee_records.Employee_recordsRepository;
import employee_records.MpStatusEnum;
import org.apache.log4j.Logger;
import pb.Utils;
import political_party.Political_partyDTO;
import political_party.Political_partyRepository;
import util.CommonDTO;
import util.StringUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

@SuppressWarnings({"unused", "Duplicates"})
public class Election_wise_mpDAO implements EmployeeCommonDAOService<Election_wise_mpDTO> {

    private static final Logger logger = Logger.getLogger(Election_wise_mpDAO.class);
    private static final Map<String, String> searchMap = new HashMap<>();
    /*private static final String [] columns = new String[]{
            "election_details_id",
            "election_constituency_id",
            "constituency_number",
            "employee_records_id",
            "political_party_id",
            "start_date",
            "end_date",
            "insertion_date",
            "inserted_by",
            "modified_by",
            "search_column",
            "lastModificationTime",
            "mp_remarks",
            "mp_status_cat",
            "change_mp_status_date",
            "mp_profession",
            "mp_parliament_address",
            "mp_alt_image_blob",
            "present_address",
            "permanent_address",
            "username",
            "date_of_birth",
            "mp_hobbies",
            "mp_gazette_date",
            "mp_gazette_doc",
            "parliamentary_designation",
            "oath_date",
            "election_date",
            "isDeleted",
            "ID"
    };*/

    private static final String addSqlQuery = "INSERT INTO {tableName} (election_details_id,election_constituency_id,constituency_number,employee_records_id,"
            .concat("political_party_id, start_date,end_date,insertion_date,inserted_by,modified_by,search_column,lastModificationTime,")
            .concat("mp_remarks,mp_organization,mp_status_cat,change_mp_status_date,mp_profession,mp_parliament_address,mp_alt_image_blob,")
            .concat("present_address,permanent_address,username,date_of_birth,mp_hobbies,mp_gazette_date,mp_gazette_doc,parliamentary_designation,oath_date,election_date,isDeleted,ID)")
            .concat(" VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

    private static final String updateSqlQuery = "UPDATE {tableName} SET election_details_id=?,election_constituency_id=?,constituency_number=?,employee_records_id=?,political_party_id=?,"
            .concat("start_date=?,end_date=?,insertion_date=?,inserted_by=?,modified_by=?,search_column=?, lastModificationTime =?,")
            .concat("mp_remarks=?,mp_organization=?,mp_status_cat=?,change_mp_status_date=?,mp_profession=?,mp_parliament_address=?,mp_alt_image_blob=?,")
            .concat("present_address=?,permanent_address=?,username=?,date_of_birth=?,mp_hobbies=?,mp_gazette_date=?,mp_gazette_doc=?,parliamentary_designation=?,oath_date=?,election_date=? WHERE ID =?");

    private static final String updatePersonalInfo = "UPDATE election_wise_mp SET date_of_birth=?,search_column=?,lastModificationTime=?  WHERE ID =?";
    private static final String updateAddressInfo = "UPDATE election_wise_mp SET permanent_address=?,present_address=?,lastModificationTime=?  WHERE ID =?";

    private static final String getElectionInfoById = ("select parliament_number, name_en, name_bn, election_details.election_date,")
            .concat(" boundary_details, election_constituency.constituency_number, constituency_name_en, constituency_name_bn,")
            .concat(" geo_divisions_type,geo_districts_type, thana, start_date, end_date, political_party_id")
            .concat(" from election_wise_mp inner join election_constituency on election_wise_mp.election_constituency_id =")
            .concat(" election_constituency.ID inner join political_party on election_wise_mp.political_party_id = political_party.ID inner join")
            .concat(" election_details on election_wise_mp.election_details_id = election_details.ID where election_wise_mp.employee_records_id = ? order by start_date DESC");

    private static final String getByUserName = "SELECT * FROM election_wise_mp WHERE username = %s AND isDeleted = 0 ";

    private static final String getActiveMPByElectionDetailsIdAndElectionConstituencyId =
            "SELECT * FROM election_wise_mp WHERE election_details_id = %d AND election_constituency_id = %d AND isDeleted = 0 AND mp_status_cat = " + MpStatusEnum.ACTIVE.getValue();

    private static final String getMaxUserNameByPrefix = "select max(username) from election_wise_mp where username like '{prefix}%'";


    private static final String tableName = "election_wise_mp";

    private static class LazyLoader {
        static final Election_wise_mpDAO INSTANCE = new Election_wise_mpDAO();
    }

    public static Election_wise_mpDAO getInstance() {
        return Election_wise_mpDAO.LazyLoader.INSTANCE;
    }


    private Election_wise_mpDAO() {
        searchMap.put("election_details_id", "and (election_details_id= ?)");
        searchMap.put("election_constituency_id", "and (election_constituency_id= ?)");
        searchMap.put("political_party_id", "and (political_party_id= ?)");
        searchMap.put("AnyField", " and (search_column like ?)");
    }

    public void setSearchColumn(Election_wise_mpDTO election_wise_mpDTO) {
        StringBuilder column = new StringBuilder();
        Election_constituencyDTO constituencyDTO = Election_constituencyRepository.getInstance().getElection_constituencyDTOByID(election_wise_mpDTO.electionConstituencyId);
        column.append(constituencyDTO.constituencyNameEn).append(" ").append(constituencyDTO.constituencyNameBn).append(" ");
        column.append(constituencyDTO.constituencyNumber).append(" ")
              .append(StringUtils.convertToBanNumber(String.valueOf(constituencyDTO.constituencyNumber))).append(" ");
        Employee_recordsDTO recordDTO = Employee_recordsRepository.getInstance().getById(election_wise_mpDTO.employeeRecordsId);
        if (recordDTO != null) {
            column.append(recordDTO.nameEng).append(" ").append(recordDTO.nameBng).append(" ");
        }
        Political_partyDTO politicalPartyDTO = Political_partyRepository.getInstance().getPolitical_partyDTOByID(election_wise_mpDTO.politicalPartyId);
        column.append(politicalPartyDTO.nameEn).append(" ").append(politicalPartyDTO.nameBn).append(" ");
        election_wise_mpDTO.searchColumn = column.toString();
    }

    @Override
    public void set(PreparedStatement ps, Election_wise_mpDTO election_wise_mpDTO, boolean isInsert) throws SQLException {
        setSearchColumn(election_wise_mpDTO);
        int index = 0;
        if (election_wise_mpDTO.lastModificationTime == 0) {
            election_wise_mpDTO.lastModificationTime = System.currentTimeMillis();
        }
        ps.setObject(++index, election_wise_mpDTO.electionDetailsId);
        ps.setObject(++index, election_wise_mpDTO.electionConstituencyId);
        ps.setObject(++index, election_wise_mpDTO.constituencyNumber);
        ps.setObject(++index, election_wise_mpDTO.employeeRecordsId);
        ps.setObject(++index, election_wise_mpDTO.politicalPartyId);
        ps.setObject(++index, election_wise_mpDTO.startDate);
        ps.setObject(++index, election_wise_mpDTO.endDate);
        ps.setObject(++index, election_wise_mpDTO.insertionDate);
        ps.setObject(++index, election_wise_mpDTO.insertedBy);
        ps.setObject(++index, election_wise_mpDTO.modifiedBy);
        ps.setObject(++index, election_wise_mpDTO.searchColumn);
        ps.setObject(++index, election_wise_mpDTO.lastModificationTime);
        ps.setObject(++index, election_wise_mpDTO.mpRemarks);
        ps.setObject(++index, election_wise_mpDTO.mpOrganization);
        ps.setObject(++index, election_wise_mpDTO.mpStatus);
        ps.setObject(++index, election_wise_mpDTO.mpStatusChangeDate);
        ps.setObject(++index, election_wise_mpDTO.professionOfMP);
        ps.setObject(++index, election_wise_mpDTO.addressOfMP);
        ps.setObject(++index, election_wise_mpDTO.alternateImageOfMP);
        ps.setObject(++index, election_wise_mpDTO.presentAddress);
        ps.setObject(++index, election_wise_mpDTO.permanentAddress);
        ps.setObject(++index, election_wise_mpDTO.userName);
        ps.setObject(++index, election_wise_mpDTO.dateOfBirth);
        ps.setObject(++index, election_wise_mpDTO.mpHobbies);
        ps.setObject(++index, election_wise_mpDTO.mpGazetteDate);
        ps.setObject(++index, election_wise_mpDTO.mpGazetteDocument);
        ps.setObject(++index, election_wise_mpDTO.parliamentaryDesignation);
        ps.setObject(++index, election_wise_mpDTO.oathDate);
        ps.setObject(++index, election_wise_mpDTO.electionDate);
        if (isInsert) {
            ps.setObject(++index, 0);
        }
        ps.setObject(++index, election_wise_mpDTO.iD);
    }

    public Election_wise_mpDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Election_wise_mpDTO election_wise_mpDTO = new Election_wise_mpDTO();
            election_wise_mpDTO.iD = rs.getLong("ID");
            election_wise_mpDTO.electionDetailsId = rs.getLong("election_details_id");
            election_wise_mpDTO.electionConstituencyId = rs.getLong("election_constituency_id");
            election_wise_mpDTO.constituencyNumber = rs.getInt("constituency_number");
            election_wise_mpDTO.employeeRecordsId = rs.getLong("employee_records_id");
            election_wise_mpDTO.politicalPartyId = rs.getLong("political_party_id");
            election_wise_mpDTO.startDate = rs.getLong("start_date");
            election_wise_mpDTO.endDate = rs.getLong("end_date");
            election_wise_mpDTO.insertionDate = rs.getLong("insertion_date");
            election_wise_mpDTO.insertedBy = rs.getString("inserted_by");
            election_wise_mpDTO.modifiedBy = rs.getString("modified_by");
            election_wise_mpDTO.searchColumn = rs.getString("search_column");
            election_wise_mpDTO.isDeleted = rs.getInt("isDeleted");
            election_wise_mpDTO.lastModificationTime = rs.getLong("lastModificationTime");
            election_wise_mpDTO.mpRemarks = rs.getString("mp_remarks");
            election_wise_mpDTO.mpOrganization = rs.getString("mp_organization");
            election_wise_mpDTO.mpStatus = rs.getInt("mp_status_cat");
            election_wise_mpDTO.mpStatusChangeDate = rs.getLong("change_mp_status_date");
            election_wise_mpDTO.professionOfMP = rs.getString("mp_profession");
            election_wise_mpDTO.addressOfMP = rs.getString("mp_parliament_address");
            election_wise_mpDTO.alternateImageOfMP = Utils.getByteArrayFromInputStream(rs.getBinaryStream("mp_alt_image_blob"));
            election_wise_mpDTO.presentAddress = rs.getString("present_address");
            election_wise_mpDTO.permanentAddress = rs.getString("permanent_address");
            election_wise_mpDTO.userName = rs.getString("username");
            election_wise_mpDTO.dateOfBirth = rs.getLong("date_of_birth");
            election_wise_mpDTO.mpHobbies = rs.getString("mp_hobbies");
            election_wise_mpDTO.mpGazetteDate = rs.getLong("mp_gazette_date");
            election_wise_mpDTO.mpGazetteDocument = rs.getBytes("mp_gazette_doc");
            election_wise_mpDTO.parliamentaryDesignation = rs.getString("parliamentary_designation");
            election_wise_mpDTO.oathDate = rs.getLong("oath_date");
            election_wise_mpDTO.electionDate = rs.getLong("election_date");
            return election_wise_mpDTO;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    @Override
    public String getTableName() {
        return "election_wise_mp";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }


    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Election_wise_mpDTO) commonDTO, addSqlQuery, true);
    }

    public long add(CommonDTO commonDTO, Connection connection) throws Exception {
        return executeAddOrUpdateQuery((Election_wise_mpDTO) commonDTO, addSqlQuery, true, connection);
    }

    public Election_wise_mpDTO getDTOByID(long ID) {
        return getDTOFromID(ID);
    }

    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Election_wise_mpDTO) commonDTO, updateSqlQuery, false);
    }

    public long update(CommonDTO commonDTO, Connection connection) throws Exception {
        return executeAddOrUpdateQuery((Election_wise_mpDTO) commonDTO, updateSqlQuery, false, connection);
    }


    private ElectionWiseMpModelForApi buildElectionWiseMpModelForApiFromResultSet(ResultSet rs) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        List<String> codeOfThanas;
        try {
            ElectionWiseMpModelForApi model = new ElectionWiseMpModelForApi();
            model.parliamentNumber = rs.getInt("parliament_number");
            model.constituencyNumber = rs.getInt("constituency_number");
            model.constituencyNameEng = rs.getString("constituency_name_en");
            model.constituencyNameBng = rs.getString("constituency_name_bn");

            try {
//                model.division = GeoDivisionRepository.getInstance().getById(rs.getLong("geo_divisions_type")).nameEng;
//                model.district = GeoDistrictRepository.getInstance().getById(rs.getLong("geo_districts_type")).nameEng;
//                codeOfThanas = Arrays.asList(rs.getString("thana").split(","));
//                model.thana = codeOfThanas.stream().map(id -> GeoThanaRepository.getInstance().getById(Long.parseLong(id.trim())).nameEng).collect(Collectors.joining(", "));
                model.division = String.valueOf(rs.getLong("geo_divisions_type"));
                model.district = String.valueOf(rs.getLong("geo_districts_type"));
                model.thana = rs.getString("thana");
            } catch (NullPointerException npe) {
                logger.debug(npe);
            }

            model.boundaryDetails = rs.getString("boundary_details");
            model.politicalParty = rs.getLong("political_party_id");
            model.electionDate = dateFormat.format(new Date(rs.getLong("election_date")));
            model.startDate = dateFormat.format(new Date(rs.getLong("start_date")));
            model.endDate = dateFormat.format(new Date(rs.getLong("end_date")));

            model.status = rs.getLong("end_date") >= System.currentTimeMillis() ? "active" : "expired";

            return model;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    public List<ElectionWiseMpModelForApi> getAllElectionInfoByEmpId(long id) {
        return ConnectionAndStatementUtil.getListOfT(getElectionInfoById, ps -> {
            try {
                ps.setObject(1, id);
            } catch (SQLException ex) {
                logger.error(ex);
            }
        }, this::buildElectionWiseMpModelForApiFromResultSet);
    }

    private static final String electionBreakDown = "UPDATE election_wise_mp SET mp_status_cat=?,change_mp_status_date=?,lastModificationTime=?," +
                                                    " modified_by =?,end_date=? WHERE mp_status_cat = 1 AND election_details_id = ?";


    public void setElectionBreakDown(long electionDetailsId, long breakDownTime, long modifiedBy) {
        ConnectionAndStatementUtil.getWriteConnectionAndPrepareStatement(model -> {
            PreparedStatement ps = (PreparedStatement) model.getStatement();
            long currentTime = System.currentTimeMillis();
            try {
                ps.setObject(1, MpStatusEnum.COMPLETED.getValue());
                ps.setObject(2, breakDownTime);
                ps.setObject(3, currentTime);
                ps.setObject(4, modifiedBy);
                ps.setObject(5, breakDownTime);
                ps.setObject(6, electionDetailsId);
                logger.debug(ps);
                ps.executeUpdate();
                recordUpdateTime(model.getConnection(), getTableName(), currentTime);
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }, electionBreakDown);
    }


    public void updatePersonalInfoMP(Election_wise_mpDTO electionWiseMpDTO) {
        ConnectionAndStatementUtil.getWriteConnectionAndPrepareStatement(model -> {
            PreparedStatement ps = (PreparedStatement) model.getStatement();
            setSearchColumn(electionWiseMpDTO);
            try {
                int index = 0;
                ps.setObject(++index, electionWiseMpDTO.dateOfBirth);
                ps.setObject(++index, electionWiseMpDTO.searchColumn);
                ps.setObject(++index, electionWiseMpDTO.lastModificationTime);
                ps.setObject(++index, electionWiseMpDTO.iD);
                ps.executeUpdate();
                recordUpdateTime(model.getConnection(), "election_wise_mp", electionWiseMpDTO.lastModificationTime);
            } catch (SQLException ex) {
                logger.error(ex);
            }
        }, updatePersonalInfo);
    }

    public void updateAddressInfoMP(Election_wise_mpDTO electionWiseMpDTO) {
        ConnectionAndStatementUtil.getWriteConnectionAndPrepareStatement(model -> {
            PreparedStatement ps = (PreparedStatement) model.getStatement();
            try {
                int index = 0;
                ps.setObject(++index, electionWiseMpDTO.permanentAddress);
                ps.setObject(++index, electionWiseMpDTO.presentAddress);
                ps.setObject(++index, electionWiseMpDTO.lastModificationTime);
                ps.setObject(++index, electionWiseMpDTO.iD);
                ps.executeUpdate();
                recordUpdateTime(model.getConnection(), "election_wise_mp", electionWiseMpDTO.lastModificationTime);
            } catch (SQLException ex) {
                logger.error(ex);
            }
        }, updateAddressInfo);
    }

    public void updateEmploymentInfoMP(Election_wise_mpDTO electionWiseMpDTO) {
        ConnectionAndStatementUtil.getWriteConnectionAndPrepareStatement(model -> {
            PreparedStatement ps = (PreparedStatement) model.getStatement();
            try {
                int index = 0;
                ps.setObject(++index, electionWiseMpDTO.permanentAddress);
                ps.setObject(++index, electionWiseMpDTO.presentAddress);
                ps.setObject(++index, electionWiseMpDTO.lastModificationTime);
                ps.setObject(++index, electionWiseMpDTO.iD);
                ps.executeUpdate();
                recordUpdateTime(model.getConnection(), "election_wise_mp", electionWiseMpDTO.lastModificationTime);
            } catch (SQLException ex) {
                logger.error(ex);
            }
        }, updateAddressInfo);
    }

    public Election_wise_mpDTO getByUserName(String userName) {
        return ConnectionAndStatementUtil.getT(String.format(getByUserName, userName), this::buildObjectFromResultSet);
    }

    public Election_wise_mpDTO getActiveMPByElectionDetailsIdAndElectionConstituencyId(long electionDetailsId, long electionConstituencyId) {
        String sql = String.format(getActiveMPByElectionDetailsIdAndElectionConstituencyId, electionDetailsId, electionConstituencyId);
        return ConnectionAndStatementUtil.getT(sql, this::buildObjectFromResultSet);
    }

    public static String getMaxUserNameByPrefix(String prefix) {
        return ConnectionAndStatementUtil.getT(getMaxUserNameByPrefix.replace("{prefix}", prefix), rs -> {
            try {
                return rs.getString(1);
            } catch (SQLException ex) {
                ex.printStackTrace();
                return null;
            }
        }, null);
    }

    private static final String getIdsByEmpIds = "SELECT id FROM election_wise_mp WHERE employee_records_id IN (%s) AND isDeleted = 0 ";

    public List<Long> getIdsByEmployeeIds(List<Long> empIds) {
        if (empIds == null || empIds.isEmpty()) {
            return new ArrayList<>();
        }
        return ConnectionAndStatementUtil.getDTOListByNumbers(getIdsByEmpIds, empIds, rs -> {
            try {
                return rs.getLong(1);
            } catch (SQLException ex) {
                return null;
            }
        });
    }

    private static final String deleteByIds = "UPDATE election_wise_mp SET isDeleted = 1,lastModificationTime=%d WHERE id in (%s)";

    public void deleteByIds(List<Long> idList, long lastModificationTime) throws Exception {
        if (idList == null || idList.isEmpty()) {
            return;
        }
        AtomicReference<Exception> ar = new AtomicReference<>();
        ConnectionAndStatementUtil.getWriteStatement(st -> {
            String ids = idList.stream().map(String::valueOf).collect(Collectors.joining(","));
            String sql = String.format(deleteByIds, lastModificationTime, ids);
            logger.debug(sql);
            try {
                st.execute(sql);
                recordUpdateTime(CommonDAOService.CONNECTION_THREAD_LOCAL.get(), getTableName(), lastModificationTime);
            } catch (SQLException e) {
                e.printStackTrace();
                ar.set(e);
            }
        }, CommonDAOService.CONNECTION_THREAD_LOCAL.get());
        if (ar.get() != null) {
            throw ar.get();
        }
    }


    private Election_wise_mpTrainingReportModel buildTrainingReportModelFromResultSet(ResultSet rs) {
        try {
            Election_wise_mpTrainingReportModel reportModel = new Election_wise_mpTrainingReportModel();
            reportModel.electionWiseMpId = rs.getLong("ewm.id");
            reportModel.employeeRecordsId = rs.getLong("ewm.employee_records_id");
            reportModel.trainingNameEn = rs.getString("tc.name_en");
            reportModel.trainingNameBn = rs.getString("tc.name_bn");
            reportModel.country = rs.getLong("tc.country");
            reportModel.startDate = rs.getLong("tc.start_date");
            reportModel.endDate = rs.getLong("tc.end_date");
            return reportModel;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    private static final String getTrainingReportModelSql =
            "select ewm.id, " +
            "       ewm.employee_records_id, " +
            "       tc.name_en," +
            "       tc.name_bn," +
            "       tc.country, " +
            "       tc.start_date, " +
            "       tc.end_date " +
            "from election_wise_mp ewm " +
            "  join training_calendar_details tcd on ewm.employee_records_id = tcd.employee_records_id " +
            "  join training_calender tc on tcd.training_calendar_id = tc.id " +
            "where ewm.isDeleted = 0 " +
            "  and tcd.isDeleted = 0 " +
            "  and tc.isDeleted = 0 " +
            "  and ewm.id in (%s)";
    
    private static final String getForeignTrainingReportModelSql =
            "SELECT \r\n" + 
            "    tcd.employee_records_id,\r\n" + 
            "    tc.name_en,\r\n" + 
            "    tc.name_bn,\r\n" + 
            "    tc.country,\r\n" + 
            "    tc.start_date,\r\n" + 
            "    tc.end_date\r\n" + 
            "FROM\r\n" + 
            "\r\n" + 
            "    training_calendar_details tcd \r\n" + 
            "        JOIN\r\n" + 
            "    training_calender tc ON tcd.training_calendar_id = tc.id\r\n" + 
            "WHERE\r\n" + 
            "    tcd.isDeleted = 0\r\n" + 
            "        AND tc.isDeleted = 0\r\n" + 
            "        AND tc.country != 0\r\n" + 
            "        AND tcd.employee_records_id IN (%s)";

    public List<Election_wise_mpTrainingReportModel> getTrainingReportModels(List<Long> electionWiseMpIds) {
        if (electionWiseMpIds == null || electionWiseMpIds.isEmpty()) {
            return Collections.emptyList();
        }
        String electionWiseMpIdsStr =
                electionWiseMpIds.stream()
                                 .map(String::valueOf)
                                 .collect(Collectors.joining(","));
        return ConnectionAndStatementUtil.getListOfT(
                String.format(getTrainingReportModelSql, electionWiseMpIdsStr),
                this::buildTrainingReportModelFromResultSet
        );
    }
    
    public List<Election_wise_mpTrainingReportModel> getForeignTrainingReportModels(List<Long> electionWiseMpIds) {
        if (electionWiseMpIds == null || electionWiseMpIds.isEmpty()) {
            return Collections.emptyList();
        }
        String electionWiseMpIdsStr =
                electionWiseMpIds.stream()
                                 .map(String::valueOf)
                                 .collect(Collectors.joining(","));
        return ConnectionAndStatementUtil.getListOfT(
                String.format(getForeignTrainingReportModelSql, electionWiseMpIdsStr),
                this::buildTrainingReportModelFromResultSetForAll
        );
    }
    
    private Election_wise_mpTrainingReportModel buildTrainingReportModelFromResultSetForAll(ResultSet rs) {
        try {
            Election_wise_mpTrainingReportModel reportModel = new Election_wise_mpTrainingReportModel();

            reportModel.employeeRecordsId = rs.getLong("tcd.employee_records_id");
            reportModel.trainingNameEn = rs.getString("tc.name_en");
            reportModel.trainingNameBn = rs.getString("tc.name_bn");
            reportModel.country = rs.getLong("tc.country");
            reportModel.startDate = rs.getLong("tc.start_date");
            reportModel.endDate = rs.getLong("tc.end_date");
            return reportModel;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }
}