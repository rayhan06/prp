package election_wise_mp;

import common.ApiResponse;
import common.BaseServlet;
import common.EmployeeServletService;
import election_details.Election_detailsDTO;
import election_details.Election_detailsRepository;
import employee_management.Employee_managementDAO;
import employee_offices.EmployeeOfficeRepository;
import employee_offices.EmployeeOfficesDAO;
import employee_records.Employee_recordsRepository;
import login.LoginDTO;
import org.apache.log4j.Logger;
import pb.Utils;
import permission.MenuConstants;
import sessionmanager.SessionConstants;
import user.UserDAO;
import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;
import util.HttpRequestUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@WebServlet("/Election_wise_mpServlet")
@MultipartConfig
public class Election_wise_mpServlet extends BaseServlet implements EmployeeServletService {
    private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Election_wise_mpServlet.class);

    String tableName = "election_wise_mp";

    Election_wise_mpDAO election_wise_mpDAO = Election_wise_mpDAO.getInstance();


    @Override
    public String getTableName() {
        return tableName;
    }

    @Override
    public String getServletName() {
        return "Election_wise_mpServlet";
    }

    @Override
    public Election_wise_mpDAO getCommonDAOService() {
        return election_wise_mpDAO;
    }

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        request.setAttribute("failureMessage", "");

        Election_wise_mpDTO election_wise_mpDTO;
        SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");

        if (addFlag) {
            election_wise_mpDTO = new Election_wise_mpDTO();
            election_wise_mpDTO.insertedBy = String.valueOf(userDTO.employee_record_id);
            election_wise_mpDTO.insertionDate = new Date().getTime();
        } else {
            election_wise_mpDTO = election_wise_mpDAO.getDTOByID(Long.parseLong(request.getParameter("iD")));
        }
        election_wise_mpDTO.modifiedBy = String.valueOf(userDTO.ID);
        election_wise_mpDTO.lastModificationTime = new Date().getTime();


        election_wise_mpDTO.electionDetailsId = Long.parseLong(request.getParameter("electionDetailsId"));
        election_wise_mpDTO.electionConstituencyId = Long.parseLong(request.getParameter("electionConstituencyId"));
        election_wise_mpDTO.employeeRecordsId = Long.parseLong(request.getParameter("employeeRecordsId"));
        election_wise_mpDTO.politicalPartyId = Long.parseLong(request.getParameter("politicalPartyId"));
        Date d = f.parse(request.getParameter("startDate"));
        election_wise_mpDTO.startDate = d.getTime();
        d = f.parse(request.getParameter("endDate"));
        election_wise_mpDTO.endDate = d.getTime();
        election_wise_mpDTO.searchColumn = (request.getParameter("searchColumn"));
        System.out.println("Done adding  addElection_wise_mp dto = " + election_wise_mpDTO);
        if (addFlag) {
            election_wise_mpDAO.add(election_wise_mpDTO);
        } else {
            election_wise_mpDAO.update(election_wise_mpDTO);
        }


//        String inPlaceSubmit = request.getParameter("inplacesubmit");
//
//        if (inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase("")) {
//            getElection_wise_mp(request, response, returnedID);
//        } else {
//            response.sendRedirect("Election_wise_mpServlet?actionType=search");
//        }
        return election_wise_mpDTO;

    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[0];
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[0];
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[0];
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Election_wise_mpServlet.class;
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }
        try {
            String actionType = request.getParameter("actionType");
            switch (actionType) {
                case "getAddPage":
                    if (Utils.checkPermission(userDTO, MenuConstants.ELECTION_WISE_MP_ADD)) {
                        List<Election_detailsDTO> electionDetailsDtos = Election_detailsRepository.getInstance().getElectionDetailsList();
                        request.setAttribute("electionDetailsDtos", electionDetailsDtos);
                        super.doGet(request, response);
                        return;
                    }
                    break;
                case "getEditPage":
                    if (Utils.checkPermission(userDTO, MenuConstants.ELECTION_WISE_MP_UPDATE)) {
                        List<Election_detailsDTO> electionDetailsDtos = Election_detailsRepository.getInstance().getElectionDetailsList();
                        request.setAttribute("electionDetailsDtos", electionDetailsDtos);
                        super.doGet(request, response);
                        return;
                    }
                    break;
                case "getTrainingReportPage":
                    if (Utils.checkPermission(userDTO, MenuConstants.MP_CONFERENCE_REPORT)) {
                        request.getRequestDispatcher("election_wise_mp/election_wise_mpTrainingReport.jsp").forward(request, response);
                        return;
                    }
                    break;
                case "ajax_getTrainingReport":
                    if (Utils.checkPermission(userDTO, MenuConstants.MP_CONFERENCE_REPORT)) {
                        getTrainingReport(request, response);
                        return;
                    }
                    break;
                default:
                    super.doGet(request, response);
                    return;
            }
        } catch (Exception ex) {
            logger.debug(ex);
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }

    private void getTrainingReport(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<Long> electionWiseMpIds = new ArrayList<>();
        String electionWiseMpIdsStr = request.getParameter("electionWiseMpIds");
        if (electionWiseMpIdsStr != null) {
            electionWiseMpIds =
                    Arrays.stream(electionWiseMpIdsStr.split(","))
                          .map(idStr -> Utils.parseOptionalLong(idStr, null, null))
                          .filter(Objects::nonNull)
                          .collect(Collectors.toList());
        }
        List<Election_wise_mpInfoModel> mpInfoModels =
                electionWiseMpIds.stream()
                                 .map(Election_wise_mpInfoModel::getFromElectionWiseMpId)
                                 .filter(Objects::nonNull)
                                 .sorted(Comparator.comparingInt(mpInfoModel -> mpInfoModel.constituencyNumber))
                                 .collect(Collectors.toList());
        List<Election_wise_mpTrainingReportModel> mpTrainingReportModels =
                Election_wise_mpDAO.getInstance().getTrainingReportModels(electionWiseMpIds);
        request.setAttribute("mpInfoModels", mpInfoModels);
        request.setAttribute("mpTrainingReportModels", mpTrainingReportModels);
        request.getRequestDispatcher("election_wise_mp/election_wise_mpTrainingReportTable.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if ("delete".equals(request.getParameter("actionType"))) {
            if (Utils.checkPermission(HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO, getEditPageMenuConstants())) {
                String[] ids = request.getParameterValues("ID");
                List<Long> idList = Arrays.stream(ids)
                                          .peek(id -> logger.debug("###DELETING " + id))
                                          .map(Long::valueOf)
                                          .collect(Collectors.toList());
                try {
                    deleteByIds(idList);
                    ApiResponse.sendSuccessResponse(response, "option");
                } catch (Exception ex) {
                    ex.printStackTrace();
                    ApiResponse.sendErrorResponse(response, ex.getMessage());
                }
            } else {
                ApiResponse.sendSuccessResponse(response, "permission issue");
            }
        } else {
            super.doPost(request, response);
        }
    }

    private void deleteByIds(List<Long> empIdList) throws Exception {
        if (empIdList == null || empIdList.isEmpty()) {
            return;
        }
        Utils.handleTransaction(() -> {
            long lastModificationTime = System.currentTimeMillis() + 180000; //3mins delay
            Employee_managementDAO.getInstance().deleteByIds(empIdList, lastModificationTime);
            List<Long> electionWiseMpIdList = election_wise_mpDAO.getIdsByEmployeeIds(empIdList);
            election_wise_mpDAO.deleteByIds(electionWiseMpIdList, lastModificationTime);
            UserDAO userDAO = new UserDAO();
            List<Long> userIdList = userDAO.getIdsByEmployeeIds(empIdList);
            userDAO.deleteByIds(userIdList, lastModificationTime);
            List<Long> empOfcIdList = EmployeeOfficesDAO.getInstance().getIdsByEmployeeIds(empIdList);
            EmployeeOfficesDAO.getInstance().deleteByIds(empOfcIdList, lastModificationTime, lastModificationTime);
            updateCache(empIdList, userIdList, empOfcIdList, electionWiseMpIdList);
        });
    }

    private void updateCache(List<Long> empIdList, List<Long> userIdList, List<Long> empOfcIdList, List<Long> electionWiseMpIdList) {
        Utils.runIOTaskInASeparateThread(() -> () -> {
            Employee_recordsRepository.getInstance().updateCacheByIds(empIdList);
            UserRepository.getInstance().updateCache(userIdList);
            EmployeeOfficeRepository.getInstance().updateCache(empOfcIdList);
            Election_wise_mpRepository.getInstance().updateCache(electionWiseMpIdList);
        });
    }


}

