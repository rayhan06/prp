package yearly_procurement_report;


import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import common.ApiResponse;
import common.RequestFailureException;
import language.LC;
import language.LM;

import java.util.*;
import login.LoginDTO;
import pb.CatDAO;
import pb.CommonDAO;
import pb.Utils;
import permission.MenuConstants;
import procurement_goods.Procurement_goodsDTO;
import procurement_package.ProcurementGoodsTypeRepository;
import procurement_package.Procurement_packageRepository;
import role.PermissionRepository;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.ActionTypeConstant;
import util.JSPConstant;
import pbReport.*;






@WebServlet("/Yearly_procurement_report_Servlet")
public class Yearly_procurement_report_Servlet  extends HttpServlet {
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	ReportTemplate reportTemplate = new ReportTemplate();

	String[][] Criteria =
	{
		{"criteria","","report_name","=","","String","","","any","reportName"},
		{"criteria","","procurement_year","=","AND","String","","","any","procurementYear"},
		{"criteria","","insertion_date",">=","AND","long","","",Long.MIN_VALUE + "","startDate"},
		{"criteria","","insertion_date","<=","AND","long","","",Long.MAX_VALUE + "","endDate"}
	};

	String[][] Display =
	{
		{"display","","ID","text",""},
		{"display","","procurement_package_id","text",""},
		{"display","","name_bn","text",""},
		{"display","","quantity","text",""},
		{"display","","unit_price","text",""},
		{"display","","est_cost","text",""},
		{"display","","method_and_type_cat","text",""},
		{"display","","approving_authority","text",""},
		{"display","","source_of_fund_cat","text",""},
		{"display","","time_code","text",""},
		{"display","","tender","text",""},
		{"display","","tender_opening","text",""},
		{"display","","tender_evaluation","text",""},
		{"display","","approval_toward","text",""},
		{"display","","award_notification","text",""},
		{"display","","sigining_of_contract","text",""},
		{"display","","contract_signature_time","text",""},
		{"display","","contract_completion_time","text",""},
		{"display","","procurement_goods_type_id","text",""},
	};

	String GroupBy = "";
	String OrderBY = " procurement_goods.procurement_package_id, procurement_goods.procurement_goods_type_id ";

	public Yearly_procurement_report_Servlet(){

	}

	private final ReportService reportService = new ReportService();

	private String sql;



	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{

		long l1stDayOfMonth = DateUtils.get1stDayOfMonth();
		long l1stDayOfJuly = DateUtils.get1stDayOfJuly();
		System.out.println("1st day of month " + l1stDayOfMonth);
		System.out.println("1st day of july " + l1stDayOfJuly);

		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String language = "english";
		if(userDTO.languageID == SessionConstants.BANGLA)
		{
			language = "bangla";
		}
		
		String Language = LM.getText(LC.PROCUREMENT_GOODS_EDIT_LANGUAGE, loginDTO);

		String actionType = request.getParameter("actionType");
		ApiResponse apiResponse = null;
		System.out.println("In ssservlet doget, actiontype = " + actionType);

		sql = "procurement_goods";

//		Display[0][4] = LM.getText(LC.VISITOR_PASS_REQUEST_SERIAL_NUMBER, loginDTO);
//		Display[1][4] = LM.getText(LC.YEARLY_PROCUREMENT_REPORT_SELECT_NAMEEN, loginDTO);
//		Display[2][4] = LM.getText(LC.YEARLY_PROCUREMENT_REPORT_SELECT_NAMEBN, loginDTO);
//		Display[3][4] = LM.getText(LC.YEARLY_PROCUREMENT_REPORT_SELECT_PROCUREMENTYEAR, loginDTO);
//		Display[4][4] = LM.getText(LC.YEARLY_PROCUREMENT_REPORT_SELECT_REPORTNAME, loginDTO);
//		Display[5][4] = LM.getText(LC.PROCUREMENT_GOODS_ADD_PACKAGE, loginDTO);
//		Display[6][4] = LM.getText(LC.YEARLY_PROCUREMENT_REPORT_SELECT_PROCUREMENTGOODSTYPEID, loginDTO);


		Display[0][4] = LM.getText(LC.VISITOR_PASS_REQUEST_SERIAL_NUMBER, loginDTO);
		Display[1][4] = LM.getText(LC.PROCUREMENT_GOODS_ADD_PACKAGE, loginDTO);
		Display[2][4] = LM.getText(LC.YEARLY_PROCUREMENT_REPORT_SELECT_NAMEBN, loginDTO);
		Display[3][4] = LM.getText(LC.YEARLY_PROCUREMENT_REPORT_SELECT_QUANTITY, loginDTO);
		Display[4][4] = LM.getText(LC.YEARLY_PROCUREMENT_REPORT_SELECT_UNITPRICE, loginDTO);
		Display[5][4] = LM.getText(LC.YEARLY_PROCUREMENT_REPORT_SELECT_ESTCOST, loginDTO);
		Display[6][4] = LM.getText(LC.YEARLY_PROCUREMENT_REPORT_SELECT_METHODANDTYPECAT, loginDTO);
		Display[7][4] = LM.getText(LC.YEARLY_PROCUREMENT_REPORT_SELECT_APPROVINGAUTHORITY, loginDTO);
		Display[8][4] = LM.getText(LC.YEARLY_PROCUREMENT_REPORT_SELECT_SOURCEOFFUNDCAT, loginDTO);
		Display[9][4] = LM.getText(LC.YEARLY_PROCUREMENT_REPORT_SELECT_TIMECODE, loginDTO);
		Display[10][4] = LM.getText(LC.YEARLY_PROCUREMENT_REPORT_SELECT_TENDER, loginDTO);
		Display[11][4] = LM.getText(LC.YEARLY_PROCUREMENT_REPORT_SELECT_TENDEROPENING, loginDTO);
		Display[12][4] = LM.getText(LC.YEARLY_PROCUREMENT_REPORT_SELECT_TENDEREVALUATION, loginDTO);
		Display[13][4] = LM.getText(LC.YEARLY_PROCUREMENT_REPORT_SELECT_APPROVALTOWARD, loginDTO);
		Display[14][4] = LM.getText(LC.YEARLY_PROCUREMENT_REPORT_SELECT_AWARDNOTIFICATION, loginDTO);
		Display[15][4] = LM.getText(LC.YEARLY_PROCUREMENT_REPORT_SELECT_SIGININGOFCONTRACT, loginDTO);
		Display[16][4] = LM.getText(LC.YEARLY_PROCUREMENT_REPORT_SELECT_CONTRACTSIGNATURETIME, loginDTO);
		Display[17][4] = LM.getText(LC.YEARLY_PROCUREMENT_REPORT_SELECT_CONTRACTCOMPLETIONTIME, loginDTO);
		Display[18][4] = "";


//		if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.YEARLY_PROCUREMENT_REPORT_DETAILS))
		if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PROCUREMENT_GOODS_SEARCH))
		{

			if(ActionTypeConstant.REPORT_PAGE.equals(actionType)){

				request.getRequestDispatcher("/yearly_procurement_report/yearly_procurement_report.jsp").forward(request, response);

				return;

			}else if(ActionTypeConstant.REPORT_PAGE_SUMMARY.equals(actionType)){

				request.getRequestDispatcher(JSPConstant.PERFORMANCE_LOG_SUMMARY_REPORT).forward(request, response);

				return;

			}else if(ActionTypeConstant.REPORT_COUNT.equals(actionType)){
				try{
					int count = getTotalCount(request, response, language);
					apiResponse = ApiResponse.makeSuccessResponse(count,"Success");
				}catch(Exception ex){
					apiResponse = ApiResponse.makeErrorResponse(ex.getMessage());
					if(ex instanceof RequestFailureException){
						throw (RequestFailureException)ex;
					}
					throw new RuntimeException(ex);
				}
			}else if(ActionTypeConstant.REPORT_RESULT.equals(actionType)){
				try{
//					Gson gson = new Gson();
//					ObjectMapper mapper = new ObjectMapper();
//					JsonNode node = mapper.createObjectNode();
//
//					List list  = getReport(request, response);
//					((ObjectNode)node).putArray("rows").addAll(list);
//
//					Object toValue = mapper.treeToValue(node, Object.class);

					List<List<Object>> list  = getReport(request, response, language);
					Yearly_procurement_reportDTO dto = new Yearly_procurement_reportDTO();
					dto.resultRows = list;

					dto.tbodyHtml = processResultRows(list, Language);

					list.get(0).add(2, LM.getText(LC.MEETING_MINUTES_ADDING_ADD_MEETING_MINUTES_ITEMNO, loginDTO));

					for (int i=0; i< list.size(); i++) {

						list.get(i).remove(list.get(i).size()-1);

					}

					apiResponse = ApiResponse.makeSuccessResponse(dto,"Success");
				}catch(Exception ex){
					apiResponse = ApiResponse.makeErrorResponse(ex.getMessage());
					if(ex instanceof RequestFailureException){
						throw (RequestFailureException)ex;
					}
					throw new RuntimeException(ex);
				}
			}else if(ActionTypeConstant.REPORT_TEMPLATE.equals(actionType)){

				apiResponse = ApiResponse.makeSuccessResponse(reportTemplate,"Success");

			}
			else if(actionType.equals("getGeo"))
			{
				System.out.println("going to geoloc ");
				request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
			}


			PrintWriter writer = response.getWriter();
			writer.write(new Gson().toJson(apiResponse));
			writer.flush();
			writer.close();

		}
//		else{
//			request.getRequestDispatcher(JSPConstant.ERROR_GLOBAL).forward(request, response);
//		}



	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {




		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String language = "english";
		if(userDTO.languageID == SessionConstants.BANGLA)
		{
			language = "bangla";
		}
		String actionType = request.getParameter("actionType");
		System.out.println("In ssservlet dopost, actiontype = " + actionType);

		String Language = LM.getText(LC.PROCUREMENT_GOODS_EDIT_LANGUAGE, loginDTO);

		ApiResponse apiResponse = null;

		if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.REPORT_PAGE)){

			if(ActionTypeConstant.REPORT_COUNT.equals(actionType)){
				try{
					int count = getTotalCount(request, response, language);
					apiResponse = ApiResponse.makeSuccessResponse(count,"Success");
				}catch(Exception ex){
					apiResponse = ApiResponse.makeErrorResponse(ex.getMessage());
					if(ex instanceof RequestFailureException){
						throw (RequestFailureException)ex;
					}
					throw new RuntimeException(ex);
				}
			}else if(ActionTypeConstant.REPORT_RESULT.equals(actionType)){
				try{
					List<List<Object>> list  = getReport(request, response, language);
					Yearly_procurement_reportDTO dto = new Yearly_procurement_reportDTO();
					dto.resultRows = list;

					dto.tbodyHtml = processResultRows(list, Language);

					list.get(0).add(2, LM.getText(LC.MEETING_MINUTES_ADDING_ADD_MEETING_MINUTES_ITEMNO, loginDTO));

					for (int i=0; i< list.size(); i++) {

						list.get(i).remove(list.get(i).size()-1);

					}

					apiResponse = ApiResponse.makeSuccessResponse(dto,"Success");
				}catch(Exception ex){
					apiResponse = ApiResponse.makeErrorResponse(ex.getMessage());
					if(ex instanceof RequestFailureException){
						throw (RequestFailureException)ex;
					}
					throw new RuntimeException(ex);
				}
			}else if(ActionTypeConstant.REPORT_TEMPLATE.equals(actionType)){


				reportTemplate = createReportTemplate(request);

				apiResponse = ApiResponse.makeSuccessResponse(null,"Success");

			}
			else if(actionType.equals("getGeo"))
			{
				System.out.println("going to geoloc ");
				request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
			}

			PrintWriter writer = response.getWriter();
			writer.write(new Gson().toJson(apiResponse));
			writer.flush();
			writer.close();

		}else{
			request.getRequestDispatcher(JSPConstant.ERROR_GLOBAL).forward(request, response);
		}
	}

	private ReportTemplate createReportTemplate(HttpServletRequest request) {

		String[] orderByColumns = request.getParameterValues("orderByColumns");
		List<String> displayColumns = new ArrayList<>();
		List<String> criteriaColumns = new ArrayList<>();

		for(String parameterName:request.getParameterMap().keySet()){
			if(parameterName.startsWith("display")){
				displayColumns.add(parameterName+"="+request.getParameter(parameterName));
			}
			if(parameterName.startsWith("criteria")){
				criteriaColumns.add(parameterName);
			}
		}
		String orderByColumnString = "";
		String displayColumnString = "";
		String criteriaColumnString = "";
		if(orderByColumns!=null){
			for(int i=0;i<orderByColumns.length;i++){
				if(i!=0){
					orderByColumnString+=",";
				}
				String orderByColumn = orderByColumns[i];
				orderByColumnString+=orderByColumn;
			}
		}
		for(int i=0;i<displayColumns.size();i++){
			if(i!=0){
				displayColumnString+=",";
			}
			String displayColumn = displayColumns.get(i);
			displayColumnString+=displayColumn;
		}
		for(int i=0;i<criteriaColumns.size();i++){
			if(i!=0){
				criteriaColumnString+=",";
			}
			String criteraiColumn = criteriaColumns.get(i);
			criteriaColumnString += criteraiColumn;
		}

		ReportTemplate reportTemplate = new ReportTemplate();

		reportTemplate.reportDisplay = displayColumnString;
		reportTemplate.reportCriteria = criteriaColumnString;
		reportTemplate.reportOrder = orderByColumnString;

		return reportTemplate;

	}

	private int getTotalCount(HttpServletRequest request, HttpServletResponse response, String language) throws Exception{
		int count = reportService.getTotalCount(sql, request, Criteria, Display, GroupBy, OrderBY, language);
		return count;
	}

	private List<List<Object>> getReport(HttpServletRequest request, HttpServletResponse response, String language) throws Exception{

		Integer recordPerPage = Integer.parseInt(request.getParameter("RECORDS_PER_PAGE"));
		Integer pageNo = Integer.parseInt(request.getParameter("pageno"));
		System.out.println("recordPerPage = " + recordPerPage + " pageNo = " + pageNo);
		int offset = (pageNo-1)*recordPerPage;
		return reportService.getResultSet(sql, request, recordPerPage,offset, Criteria, Display, GroupBy, OrderBY, language);
	}

	private String processResultRows(List<List<Object>> list, String Language) {

		String html  = "";
		String value  = "";

		HashMap<Long, Integer> packageIdToCount = new HashMap<>();
		HashMap<Long, HashSet<Long>> packageIdToGoodsType = new HashMap<>();

		List<Procurement_goodsDTO> data = new ArrayList<>();

		for (int i=1; i< list.size(); i++) {

			Procurement_goodsDTO dto = processProcurementGoodsDTO(list.get(i));
			data.add(dto);

		}

		int size = data.size();

		data
				.forEach(dto -> {
					if (packageIdToCount.containsKey(dto.procurementPackageId)) {
						int count = packageIdToCount.get(dto.procurementPackageId);
						count++;
						packageIdToCount.put(dto.procurementPackageId, count);
					}
					else {
						packageIdToCount.put(dto.procurementPackageId, 1);
					}

					HashSet<Long> goodsType;
					if (packageIdToGoodsType.containsKey(dto.procurementPackageId)) {
						goodsType = packageIdToGoodsType.get(dto.procurementPackageId);
					}
					else {
						goodsType = new HashSet<>();
					}
					goodsType.add(dto.procurementGoodsTypeId);
					packageIdToGoodsType.put(dto.procurementPackageId, goodsType);
				});

		long previousProcurementPackageId = -1;
		long previousProcurementGoodsTypeId = -1;

		int packageNum = 0;
		int itemNum = 0;
		int rowNumByPackage = 0;

		int totalQuantity = 0;
		double totalUnitPrice = 0;
		double totalEstCost = 0;

		for (int i = 0; i < size; i++) {
			Procurement_goodsDTO procurement_goodsDTO = data.get(i);

			int rowSpan = (packageIdToCount.get(procurement_goodsDTO.procurementPackageId) + packageIdToGoodsType.get(procurement_goodsDTO.procurementPackageId).size()) + 1;

			boolean newPackage = procurement_goodsDTO.procurementPackageId != previousProcurementPackageId;
			if (newPackage) {
				previousProcurementPackageId = procurement_goodsDTO.procurementPackageId;
				packageNum++;
				rowNumByPackage = 0;
			}

			boolean newGoodsType = procurement_goodsDTO.procurementGoodsTypeId != previousProcurementGoodsTypeId;
			if (newGoodsType) {
				previousProcurementGoodsTypeId = procurement_goodsDTO.procurementGoodsTypeId;
			}
			
			if (newGoodsType) 
			{

				itemNum = 1;

				html += "<tr>";

				if (newPackage) {
					html += "<td class=\"text-center-report\" rowspan=\"" + rowSpan + "\"><b>" + Utils.getDigits(packageNum, Language) + "</b></td>";
					html += "<td class=\"text-center-report\" id = '" + i + "_procurementPackageId' rowspan=\"" + rowSpan + "\"><b>";

					value = Procurement_packageRepository.getInstance().getOptions(Language, procurement_goodsDTO.procurementPackageId) + "";
					
					html += Utils.getDigits(value, Language);
					
					html += "</b></td>";
				}
				else {
					
					html += "<td class=\"text-center-report\" style=\"display: none\"></td>\n" +
							"\t\t\t\t\t<td class=\"text-center-report\" style=\"display: none\"></td>";
					
				}
				
				html += "<td class=\"text-center-report boldType\" colspan=\"5\"><b>";
				value = ProcurementGoodsTypeRepository.getInstance().getOptions(Language, procurement_goodsDTO.procurementGoodsTypeId) + "";
				
				html += Utils.getDigits(value, Language);
				html += "</b>\n" +
						"\t</td>\n" +
						"\t<td class=\"text-center-report\"></td>\n" +
						"\t<td class=\"text-center-report\"></td>\n" +
						"\t<td class=\"text-center-report\"></td>\n" +
						"\t<td class=\"text-center-report\"></td>\n" +
						"\t<td class=\"text-center-report\"></td>\n" +
						"\t<td class=\"text-center-report\"></td>\n" +
						"\t<td class=\"text-center-report\"></td>\n" +
						"\t<td class=\"text-center-report\"></td>\n" +
						"\t<td class=\"text-center-report\"></td>\n" +
						"\t<td class=\"text-center-report\"></td>\n" +
						"\t<td class=\"text-center-report\"></td>\n" +
						"\t<td class=\"text-center-report\"></td>\n" +
						"\t<td class=\"text-center-report\" style=\"display: none\"></td>\n" +
						"\t<td class=\"text-center-report\" style=\"display: none\"></td>\n" +
						"\t<td class=\"text-center-report\" style=\"display: none\"></td>\n" +
						"\t<td class=\"text-center-report\" style=\"display: none\"></td>";

				html += "</tr>";
				rowNumByPackage++;

			}
			
			html += "<tr id = 'tr_" + i + "'>";

			html += "<td class=\"text-center-report\" style=\"display: none\"></td>\n" +
					"\t\t\t\t\t<td class=\"text-center-report\" style=\"display: none\"></td>";

			totalQuantity += procurement_goodsDTO.quantity;
			totalUnitPrice += procurement_goodsDTO.unitPrice;
			totalEstCost += procurement_goodsDTO.estCost;

			String unitPrice = String.format("%.1f", procurement_goodsDTO.unitPrice);
			String estCost = String.format("%.1f", procurement_goodsDTO.estCost);
			String methodAndType = CatDAO.getName(Language, "method_and_type", procurement_goodsDTO.methodAndTypeCat);
			String sourceOfFund = CatDAO.getName(Language, "source_of_fund", procurement_goodsDTO.sourceOfFundCat);

			
			html += "<td class=\"text-center-report\">\n" +
					"\t\t\t\t\t\t\t\t\t\t\t" + Utils.getDigits(itemNum++, Language) + "\n" +
					"\t\t\t\t\n" +
					"\t\t\t\n" +
					"\t\t\t\t\t\t\t\t\t\t\t</td>\n" +
					"\n" +
					"\t\t\t\t\t\t\t\t\t\t\t<td class=\"text-center-report\" id = '" + i + "_nameBn'>\n" +
					"\t\t\t\t\t\t\t\t\t\t\t" + Utils.getDigits(procurement_goodsDTO.nameBn, Language) + "\n" +
					"\n" +
					"\n" +
					"\t\t\t\t\t\t\t\t\t\t\t</td>\n" +
					"\n" +
					"                                            <td class=\"text-center-report\" id = '" + i + "_quantity' style=\"text-align: right\">\n" +
					"                                                " + Utils.getDigits((int)procurement_goodsDTO.quantity, Language) + "\n" +
					"\n" +
					"\n" +
					"                                            </td>\n" +
					"\n" +
					"\t\t\t\t\t\t\t\t\t\t\t<td class=\"text-center-report\" id = '" + i + "_unitPrice' style=\"text-align: right\">\n" +
					"\t\t\t\t\t\t\t\t\t\t\t" + Utils.getDigits(unitPrice, Language) + "\n" +
					"\t\t\t\t\n" +
					"\t\t\t\n" +
					"\t\t\t\t\t\t\t\t\t\t\t</td>\n" +
					"\t\t\n" +
					"\t\t\t\t\t\t\t\t\t\t\t<td class=\"text-center-report\" id = '" + i + "_estCost' style=\"text-align: right\">\n" +
					"\t\t\t\t\t\t\t\t\t\t\t" + Utils.getDigits(estCost, Language) + "\n" +
					"\t\t\t\t\n" +
					"\t\t\t\n" +
					"\t\t\t\t\t\t\t\t\t\t\t</td>\n" +
					"\t\t\n" +
					"\t\t\t\t\t\t\t\t\t\t\t<td class=\"text-center-report\" id = '" + i + "_methodAndTypeCat'>\n" +
					"\t\t\t\t\t\t\t\t\t\t\t" + Utils.getDigits(methodAndType, Language) + "\n" +
					"\t\t\t\t\n" +
					"\t\t\t\n" +
					"\t\t\t\t\t\t\t\t\t\t\t</td>\n" +
					"\t\t\n" +
					"\t\t\t\t\t\t\t\t\t\t\t<td class=\"text-center-report\" id = '" + i + "_approvingAuthority'>\n" +
					"\t\t\t\t\t\t\t\t\t\t\t" + Utils.getDigits(procurement_goodsDTO.approvingAuthority, Language) + "\n" +
					"\t\t\t\t\n" +
					"\t\t\t\n" +
					"\t\t\t\t\t\t\t\t\t\t\t</td>\n" +
					"\t\t\n" +
					"\t\t\t\t\t\t\t\t\t\t\t<td class=\"text-center-report\" id = '" + i + "_sourceOfFundCat'>\n" +
					"\t\t\t\t\t\t\t\t\t\t\t" + Utils.getDigits(sourceOfFund, Language) + "\n" +
					"\t\t\t\t\n" +
					"\t\t\t\n" +
					"\t\t\t\t\t\t\t\t\t\t\t</td>\n" +
					"\t\t\n" +
					"\t\t\t\t\t\t\t\t\t\t\t<td class=\"text-center-report\" id = '" + i + "_timeCode'>\n" +
					"\t\t\t\t\t\t\t\t\t\t\t" + Utils.getDigits(procurement_goodsDTO.timeCode, Language) + "\n" +
					"\t\t\t\t\n" +
					"\t\t\t\n" +
					"\t\t\t\t\t\t\t\t\t\t\t</td>\n" +
					"\t\t\n" +
					"\t\t\t\t\t\t\t\t\t\t\t<td class=\"text-center-report\" id = '" + i + "_tender'>\n" +
					"\t\t\t\t\t\t\t\t\t\t\t" + Utils.getDigits(procurement_goodsDTO.tender, Language) + "\n" +
					"\t\t\t\t\n" +
					"\t\t\t\n" +
					"\t\t\t\t\t\t\t\t\t\t\t</td>\n" +
					"\t\t\n" +
					"\t\t\t\t\t\t\t\t\t\t\t<td class=\"text-center-report\" id = '" + i + "_tenderOpening'>\n" +
					"\t\t\t\t\t\t\t\t\t\t\t" + Utils.getDigits(procurement_goodsDTO.tenderOpening, Language) + "\n" +
					"\t\t\t\t\n" +
					"\t\t\t\n" +
					"\t\t\t\t\t\t\t\t\t\t\t</td>\n" +
					"\t\t\n" +
					"\t\t\t\t\t\t\t\t\t\t\t<td class=\"text-center-report\" id = '" + i + "_tenderEvaluation'>\n" +
					"\t\t\t\t\t\t\t\t\t\t\t" + Utils.getDigits(procurement_goodsDTO.tenderEvaluation, Language) + "\n" +
					"\t\t\t\t\n" +
					"\t\t\t\n" +
					"\t\t\t\t\t\t\t\t\t\t\t</td>\n" +
					"\t\t\n" +
					"\t\t\t\t\t\t\t\t\t\t\t<td class=\"text-center-report\" id = '" + i + "_approvalToward'>\n" +
					"\t\t\t\t\t\t\t\t\t\t\t" + Utils.getDigits(procurement_goodsDTO.approvalToward, Language) + "\n" +
					"\t\t\t\t\n" +
					"\t\t\t\n" +
					"\t\t\t\t\t\t\t\t\t\t\t</td>\n" +
					"\t\t\n" +
					"\t\t\t\t\t\t\t\t\t\t\t<td class=\"text-center-report\" id = '" + i + "_awardNotification'>\n" +
					"\t\t\t\t\t\t\t\t\t\t\t" + Utils.getDigits(procurement_goodsDTO.awardNotification, Language) + "\n" +
					"\t\t\t\t\n" +
					"\t\t\t\n" +
					"\t\t\t\t\t\t\t\t\t\t\t</td>\n" +
					"\t\t\n" +
					"\t\t\t\t\t\t\t\t\t\t\t<td class=\"text-center-report\" id = '" + i + "_siginingOfContract'>\n" +
					"\t\t\t\t\t\t\t\t\t\t\t" + Utils.getDigits(procurement_goodsDTO.siginingOfContract, Language) + "\n" +
					"\t\t\t\t\n" +
					"\t\t\t\n" +
					"\t\t\t\t\t\t\t\t\t\t\t</td>\n" +
					"\t\t\n" +
					"\t\t\t\t\t\t\t\t\t\t\t<td class=\"text-center-report\" id = '" + i + "_contractSignatureTime'>\n" +
					"\t\t\t\t\t\t\t\t\t\t\t" + Utils.getDigits(procurement_goodsDTO.contractSignatureTime, Language) + "\n" +
					"\t\t\t\t\n" +
					"\t\t\t\n" +
					"\t\t\t\t\t\t\t\t\t\t\t</td>\n" +
					"\t\t\n" +
					"\t\t\t\t\t\t\t\t\t\t\t<td class=\"text-center-report\" id = '" + i + "_contractCompletionTime'>\n" +
					"\t\t\t\t\t\t\t\t\t\t\t" + Utils.getDigits(procurement_goodsDTO.contractCompletionTime, Language) + "\n" +
					"\t\t\t\t\n" +
					"\t\t\t\n" +
					"\t\t\t\t\t\t\t\t\t\t\t</td>";
			
			
			html += "</tr>";
			rowNumByPackage++;


			if (rowNumByPackage == rowSpan-1) {
				html += "<tr>";

				html += "<td class=\"text-center-report\" style=\"display: none\"></td>\n" +
						"\t\t\t\t\t<td class=\"text-center-report\" style=\"display: none\"></td>" +
						"\t\t\t\t\t<td class=\"text-center-report\" style=\"display: none\"></td>";

				String columnName = Language.equals("English") ? "languageTextEnglish" : "languageTextBangla";
				html += "<td class=\"text-center-report\" colspan=\"2\" style=\"text-align: right\"><b>" + CommonDAO.getName(LC.YEARLY_PROCUREMENT_REPORT_OTHER_TOTAL, "language_text", columnName, "ID") + "</b></td>\n";


				html +=	"\t<td class=\"text-center-report\" style=\"text-align: right\"><b>" + Utils.getDigits(totalQuantity, Language) + "</b></td>\n";
				html +=	"\t<td class=\"text-center-report\" style=\"text-align: right\"><b>" + Utils.getDigits(totalUnitPrice, Language) + "</b></td>\n";
				html +=	"\t<td class=\"text-center-report\" style=\"text-align: right\"><b>" + Utils.getDigits(totalEstCost, Language) + "</b></td>\n";


				html +=	"\t<td class=\"text-center-report\"></td>\n" +
						"\t<td class=\"text-center-report\"></td>\n" +
						"\t<td class=\"text-center-report\"></td>\n" +
						"\t<td class=\"text-center-report\"></td>\n" +
						"\t<td class=\"text-center-report\"></td>\n" +
						"\t<td class=\"text-center-report\"></td>\n" +
						"\t<td class=\"text-center-report\"></td>\n" +
						"\t<td class=\"text-center-report\"></td>\n" +
						"\t<td class=\"text-center-report\"></td>\n" +
						"\t<td class=\"text-center-report\"></td>\n" +
						"\t<td class=\"text-center-report\"></td>\n" +
						"\t<td class=\"text-center-report\"></td>\n" +
						"\t<td class=\"text-center-report\" style=\"display: none\"></td>\n" +
						"\t<td class=\"text-center-report\" style=\"display: none\"></td>\n" +
						"\t<td class=\"text-center-report\" style=\"display: none\"></td>\n" +
						"\t<td class=\"text-center-report\" style=\"display: none\"></td>";

				html += "</tr>";

				totalQuantity = 0;
				totalUnitPrice = 0;
				totalEstCost = 0;
			}

		}

		System.out.println("printing done");
			

		return html;
	}

	private Procurement_goodsDTO processProcurementGoodsDTO(List<Object> list) {

		Procurement_goodsDTO dto = new Procurement_goodsDTO();

		int i=0;

		dto.iD = Long.parseLong(String.valueOf(list.get(i++)));
		dto.procurementPackageId = Long.parseLong(String.valueOf(list.get(i++)));
		dto.nameBn = (String) list.get(i++);
		dto.quantity = Long.parseLong(String.valueOf(list.get(i++)));
		dto.unitPrice = processDouble((String) list.get(i++));
		dto.estCost = processDouble((String) list.get(i++));
		dto.methodAndTypeCat = Integer.parseInt(String.valueOf(list.get(i++)));
		dto.approvingAuthority = (String) list.get(i++);
		dto.sourceOfFundCat = Integer.parseInt(String.valueOf(list.get(i++)));
		dto.timeCode = (String) list.get(i++);
		dto.tender = (String) list.get(i++);
		dto.tenderOpening = (String) list.get(i++);
		dto.tenderEvaluation = (String) list.get(i++);
		dto.approvalToward = (String) list.get(i++);
		dto.awardNotification = (String) list.get(i++);
		dto.siginingOfContract = (String) list.get(i++);
		dto.contractSignatureTime = Long.parseLong(String.valueOf(list.get(i++)));
		dto.contractCompletionTime = Long.parseLong(String.valueOf(list.get(i++)));
		dto.procurementGoodsTypeId = Long.parseLong(String.valueOf(list.get(i++)));

		return dto;
	}

	public double processDouble (String value) {
		if (value.contains(".")) {
			String intPart = value.substring(0, value.indexOf('.'));
			String decimalPart = value.substring(value.indexOf('.') + 1);
			int divider = 1;
			for (int i=0; i<decimalPart.length(); i++) {
				divider *= 10;
			}
			double decimal = Integer.parseInt(decimalPart) * 1.0 / divider;
			return Integer.parseInt(intPart) + decimal;
		}
		else {
			return Integer.parseInt(value) * 1.0;
		}

	}

}
