package economic_operation_mapping;

import util.CommonDTO;


public class Economic_operation_mappingDTO extends CommonDTO {
    public long economicGroupType = 0;
    public long budgetOperationId = 0;
    public long insertedBy = 0;
    public long insertionTime = 0;
    public long modifiedBy = 0;

    @Override
    public String toString() {
        return "Economic_operation_mappingDTO{" +
                "economicGroup=" + economicGroupType +
                ", budgetOperationId=" + budgetOperationId +
                ", insertedBy=" + insertedBy +
                ", insertionTime=" + insertionTime +
                ", modifiedBy=" + modifiedBy +
                '}';
    }

}
