package economic_operation_mapping;

import common.EmployeeCommonDAOService;
import org.apache.log4j.Logger;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings({"unused", "Duplicates"})
public class Economic_operation_mappingDAO implements EmployeeCommonDAOService<Economic_operation_mappingDTO> {
    private static final Logger logger = Logger.getLogger(Economic_operation_mappingDAO.class);

    private static final String addQuery = "INSERT INTO {tableName} (economic_group_type,budget_operation_id,"
            .concat("modified_by,lastModificationTime,insertion_time,inserted_by,isDeleted) VALUES(?,?,?,?,?,?,?)");
    private static final String getByBudgetOperationId = "SELECT * FROM economic_operation_mapping WHERE budget_operation_id=%d AND isDeleted=0";

    private static Economic_operation_mappingDAO INSTANCE = null;
    private static final Map<String, String> searchMap = new HashMap<>();

    public static Economic_operation_mappingDAO getInstance() {
        if (INSTANCE == null) {
            synchronized (Economic_operation_mappingDAO.class) {
                if (INSTANCE == null) {
                    INSTANCE = new Economic_operation_mappingDAO();
                }
            }
        }
        return INSTANCE;
    }

    private Economic_operation_mappingDAO() {

    }

    @Override
    public void set(PreparedStatement ps, Economic_operation_mappingDTO economic_operation_mappingDTO, boolean isInsert) throws SQLException {
        int index = 0;
        ps.setObject(++index, economic_operation_mappingDTO.economicGroupType);
        ps.setObject(++index, economic_operation_mappingDTO.budgetOperationId);
        ps.setObject(++index, economic_operation_mappingDTO.modifiedBy);
        ps.setObject(++index, economic_operation_mappingDTO.lastModificationTime);
        if (isInsert) {
            ps.setObject(++index, economic_operation_mappingDTO.insertionTime);
            ps.setObject(++index, economic_operation_mappingDTO.insertedBy);
            ps.setObject(++index, 0);
        }
    }

    @Override
    public Economic_operation_mappingDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Economic_operation_mappingDTO dto = new Economic_operation_mappingDTO();
            dto.economicGroupType = rs.getLong("economic_group_type");
            dto.budgetOperationId = rs.getLong("budget_operation_id");
            dto.insertionTime = rs.getLong("insertion_time");
            dto.insertedBy = rs.getLong("inserted_by");
            dto.modifiedBy = rs.getLong("modified_by");
            dto.isDeleted = rs.getInt("isDeleted");
            dto.lastModificationTime = rs.getLong("lastModificationTime");
            return dto;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    @Override
    public String getTableName() {
        return "economic_operation_mapping";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Economic_operation_mappingDTO) commonDTO, addQuery, true);
    }

    @Override
    public long update(CommonDTO commonDTO) throws Exception {
        return 0;
    }

    public Economic_operation_mappingDTO getDTOByID(long ID) {
        return getDTOFromID(ID);
    }

    public List<Economic_operation_mappingDTO> getDtosByBudgetOperationId(long budgetOperationId) {
        return getDTOs(String.format(getByBudgetOperationId, budgetOperationId));
    }

    public List<Economic_operation_mappingDTO> getAllEconomicOperationMapping(boolean isFirstReload) {
        return getAllDTOs(isFirstReload);
    }


}