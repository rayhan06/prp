create table economic_operation_mapping
(
    economic_group_type  bigint(20)           not null,
    budget_operation_id  bigint(20)           not null,
    inserted_by          bigint(20) default 0 null,
    insertion_time       bigint(20) default 0 null,
    isDeleted            int(4)     default 0 null,
    modified_by          bigint(20) default 0 null,
    lastModificationTime bigint(20) default 0 null,
    constraint economic_operation_mapping_pk
        unique (economic_group_type, budget_operation_id)
)
    engine = MyISAM
    collate = utf8_unicode_ci;