package economic_operation_mapping;

import common.NameDTO;
import economic_group.EconomicGroupRepository;
import org.apache.log4j.Logger;
import pb.OptionDTO;
import pb.Utils;

import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class Economic_operation_mappingRepository {
    private static final Logger logger = Logger.getLogger(Economic_operation_mappingRepository.class);
    private final Economic_operation_mappingDAO economicOperationMappingDAO;
    private Map<Long, List<Economic_operation_mappingDTO>> mapByBudgetOperationId;
    private List<Economic_operation_mappingDTO> economicOperationMappingDTOList;

    private Economic_operation_mappingRepository() {
        economicOperationMappingDAO = Economic_operation_mappingDAO.getInstance();
        reload();
    }

    private static class EconomicOperationMappingLazyLoader {
        final static Economic_operation_mappingRepository INSTANCE = new Economic_operation_mappingRepository();
    }

    public static Economic_operation_mappingRepository getInstance() {
        return Economic_operation_mappingRepository.EconomicOperationMappingLazyLoader.INSTANCE;
    }

    private void reload() {
        logger.debug("Economic Operation Mapping Repository loading start for reload: ");
        List<Economic_operation_mappingDTO> list = economicOperationMappingDAO.getAllEconomicOperationMapping(true);
        if (list != null && list.size() > 0) {
            economicOperationMappingDTOList = list.stream()
                                                  .filter(dto -> dto.isDeleted == 0)
                                                  .collect(Collectors.collectingAndThen(Collectors.toList(), ls -> {
                                                      ls.sort(Comparator.comparing(o -> o.budgetOperationId));
                                                      return ls;
                                                  }));
            mapByBudgetOperationId = economicOperationMappingDTOList.stream()
                                                                    .collect(Collectors.collectingAndThen(Collectors.groupingBy(e -> e.budgetOperationId), map -> {
                                                                        map.values().forEach(ls -> ls.sort(Comparator.comparing(o -> o.economicGroupType)));
                                                                        return map;
                                                                    }));
        } else {
            economicOperationMappingDTOList = new ArrayList<>();
            mapByBudgetOperationId = new HashMap<>();
        }
        logger.debug("Economic Operation Mapping Repository loading end for reload: ");
    }

    public List<Economic_operation_mappingDTO> getEconomic_operation_mappingDTOList() {
        return economicOperationMappingDTOList;
    }

    public String buildEconomicGroupOption(String language, long budgetOperationId, Long selectedId, Set<Long> specificIds) {
        List<OptionDTO> optionDTOList =
                mapByBudgetOperationId.get(budgetOperationId)
                                      .stream()
                                      .map(dto -> EconomicGroupRepository.getInstance().getDTOByID(dto.economicGroupType))
                                      .filter(nameDTO -> specificIds.contains(nameDTO.iD))
                                      .map(EconomicGroupRepository::buildOptionDTOWithCode)
                                      .collect(Collectors.toList());
        return Utils.buildOptions(optionDTOList, language, selectedId == null ? null : String.valueOf(selectedId));
    }

    public String buildEconomicGroupOption(String language, long budgetOperationId, Long selectedId) {
        List<OptionDTO> optionDTOList = mapByBudgetOperationId.get(budgetOperationId)
                                                              .stream()
                                                              .map(dto -> EconomicGroupRepository.getInstance().getDTOByID(dto.economicGroupType))
                                                              .map(EconomicGroupRepository::buildOptionDTOWithCode)
                                                              .collect(Collectors.toList());
        return Utils.buildOptions(optionDTOList, language, selectedId == null ? null : String.valueOf(selectedId));
    }
}