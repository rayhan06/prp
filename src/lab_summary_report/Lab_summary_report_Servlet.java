package lab_summary_report;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import language.LC;
import language.LM;
import login.LoginDTO;
import pbReport.ReportService;
import permission.MenuConstants;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.ReportRequestHandler;

@WebServlet("/Lab_summary_report_Servlet")
public class Lab_summary_report_Servlet extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	String[][] Criteria =
	{		
		{"criteria","","inserted_by_er_id","=","","String","","","any","userName",LC.HM_EMPLOYEE_ID + "", "userNameToEmployeeRecordId"},
		{"criteria","","insertion_date",">=","AND","long","","",1 + "","startDate", LC.HM_START_DATE + ""},		
		{"criteria","","insertion_date","<=","AND","long","","",Long.MAX_VALUE + "","endDate", LC.HM_END_DATE + ""},
		{"criteria","","isDeleted","=","AND","int","","", "0","none",  ""},
	};
	
	String[][] Display =
	{
		{"display","","inserted_by_er_id","erIdToName",""},	
		{"display","","count(*)","int",""}
	};
	
	String GroupBy = " inserted_by_er_id ";
	String OrderBY = "  ";
	
	ReportRequestHandler reportRequestHandler;
	
	public Lab_summary_report_Servlet(){

	}
	
	private final ReportService reportService = new ReportService();
	
	private String sql;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String language = "english";
		if(userDTO!= null && userDTO.languageID == SessionConstants.BANGLA)
		{
			language = "bangla";
		}
		boolean isLangEng = language.equalsIgnoreCase("english");
				
		String actionType = request.getParameter("actionType");

		System.out.println("In ssservlet doget, actiontype = " + actionType);
		
		sql = " common_lab_report ";

		Display[0][4] = isLangEng?"Lab Technologist":"ল্যাব টেকনোলজিস্ট";
		Display[1][4] = isLangEng?"Test Count":"টেস্টের সংখ্যা";

		
		String reportName = isLangEng?"Lab Summary Report":"ল্যাব সারসংক্ষেপ রিপোর্ট";
		
		reportRequestHandler = new ReportRequestHandler(null,
				Criteria, Display, GroupBy, OrderBY, sql,
				reportService);
		
		reportRequestHandler.clientSideDataFormatting = new ArrayList<Pair<Integer, Integer>>();
		reportRequestHandler.clientSideDataFormatting.add(new ImmutablePair<Integer, Integer>(1, ReportRequestHandler.RIGHT_ALIGN_INT));

		
		reportRequestHandler.handleReportGet(request, response, userDTO, "lab_summary_report",
				MenuConstants.LSR, language, reportName, "lab_summary_report");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		doGet(request, response);
	}
}
