package inbox_tab_action;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;


import org.apache.log4j.Logger;

import dbm.*;

import repository.RepositoryManager;

import util.*;
import pb.*;
import approval_module_map.*;

public class Inbox_tab_actionDAO  extends NavigationService3
{
	
	Logger logger = Logger.getLogger(getClass());

	
	public Inbox_tab_actionDAO(String tableName, String tempTableName, Approval_module_mapDTO approval_module_mapDTO)
	{
		super(tableName, tempTableName, approval_module_mapDTO);		
	}
	
	public Inbox_tab_actionDAO()
	{
		super("inbox_tab_action", null, null);		
	}
	
	
	
	public long add(CommonDTO commonDTO, String tableName, TempTableDTO tempTableDTO) throws Exception
	{
		
		Inbox_tab_actionDTO inbox_tab_actionDTO = (Inbox_tab_actionDTO)commonDTO;
		
		Connection connection = null;
		PreparedStatement ps = null;

		long lastModificationTime = System.currentTimeMillis();	

		try{
			connection = DBMW.getInstance().getConnection();
			
			if(connection == null)
			{
				System.out.println("nullconn");
			}

			inbox_tab_actionDTO.iD = DBMW.getInstance().getNextSequenceId(tableName);

			String sql = "INSERT INTO " + tableName;
			
			sql += " (";
			sql += "ID";
			sql += ", ";
			sql += "inbox_tab_id";
			sql += ", ";
			sql += "icon";
			sql += ", ";
			sql += "description_language_id";
			sql += ", ";
			sql += "tooltip_text";
			sql += ", ";
			sql += "action_link";
			sql += ", ";
			sql += "isDeleted";
			sql += ", ";
			sql += "lastModificationTime";
			if(tempTableDTO!=null)
			{
				sql += ", permanent_table_id, operation_type, approval_path_type, approval_order";
			}
			sql += ")";
			
			
            sql += " VALUES(";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			if(tempTableDTO!=null)
			{
				sql += ", " + tempTableDTO.permanent_table_id + ", " + tempTableDTO.operation_type + ", " + tempTableDTO.approval_path_type + ", " + tempTableDTO.approval_order;
			}
			sql += ")";
				

			ps = connection.prepareStatement(sql);
			
			
			

			int index = 1;

			ps.setObject(index++,inbox_tab_actionDTO.iD);
			ps.setObject(index++,inbox_tab_actionDTO.inboxTabId);
			ps.setObject(index++,inbox_tab_actionDTO.icon);
			ps.setObject(index++,inbox_tab_actionDTO.descriptionLanguageId);
			ps.setObject(index++,inbox_tab_actionDTO.tooltipText);
			ps.setObject(index++,inbox_tab_actionDTO.actionLink);
			ps.setObject(index++,inbox_tab_actionDTO.isDeleted);
			ps.setObject(index++, lastModificationTime);
			
			System.out.println(ps);
			ps.execute();
			
			
			recordUpdateTime(connection, lastModificationTime, tableName);

		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if (ps != null) {
					ps.close();
				}
			} catch(Exception e){}
			try{
				if(connection != null)
				{
					DBMW.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		return inbox_tab_actionDTO.iD;		
	}
		
	
	public CommonDTO getDTOByTabID (long ID) throws Exception
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Inbox_tab_actionDTO inbox_tab_actionDTO = null;
		try{
			
			String sql = "SELECT * ";

			sql += " FROM inbox_tab_action" ;
			
            sql += " WHERE ID=" + ID;
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			if(rs.next()){
				inbox_tab_actionDTO = new Inbox_tab_actionDTO();

				inbox_tab_actionDTO.iD = rs.getLong("ID");
				inbox_tab_actionDTO.inboxTabId = rs.getLong("inbox_tab_id");
				inbox_tab_actionDTO.icon = rs.getString("icon");
				inbox_tab_actionDTO.descriptionLanguageId = rs.getLong("description_language_id");
				inbox_tab_actionDTO.tooltipText = rs.getLong("tooltip_text");
				inbox_tab_actionDTO.actionLink = rs.getString("action_link");
				inbox_tab_actionDTO.isDeleted = rs.getInt("isDeleted");
				inbox_tab_actionDTO.lastModificationTime = rs.getLong("lastModificationTime");

			}			
			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return inbox_tab_actionDTO;
	}
	

	//need another getter for repository
	public CommonDTO getDTOByID (long ID, String tableName) throws Exception
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Inbox_tab_actionDTO inbox_tab_actionDTO = null;
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
			
            sql += " WHERE ID=" + ID;
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			if(rs.next()){
				inbox_tab_actionDTO = new Inbox_tab_actionDTO();

				inbox_tab_actionDTO.iD = rs.getLong("ID");
				inbox_tab_actionDTO.inboxTabId = rs.getLong("inbox_tab_id");
				inbox_tab_actionDTO.icon = rs.getString("icon");
				inbox_tab_actionDTO.descriptionLanguageId = rs.getLong("description_language_id");
				inbox_tab_actionDTO.tooltipText = rs.getLong("tooltip_text");
				inbox_tab_actionDTO.actionLink = rs.getString("action_link");
				inbox_tab_actionDTO.isDeleted = rs.getInt("isDeleted");
				inbox_tab_actionDTO.lastModificationTime = rs.getLong("lastModificationTime");

			}			
			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return inbox_tab_actionDTO;
	}
	
	public long update(CommonDTO commonDTO, String tableName) throws Exception
	{		
		Inbox_tab_actionDTO inbox_tab_actionDTO = (Inbox_tab_actionDTO)commonDTO;
		
		Connection connection = null;
		PreparedStatement ps = null;

		long lastModificationTime = System.currentTimeMillis();	
		try{
			connection = DBMW.getInstance().getConnection();

			String sql = "UPDATE " + tableName;
			
			sql += " SET ";
			sql += "inbox_tab_id=?";
			sql += ", ";
			sql += "icon=?";
			sql += ", ";
			sql += "description_language_id=?";
			sql += ", ";
			sql += "tooltip_text=?";
			sql += ", ";
			sql += "action_link=?";
			sql += ", lastModificationTime = "	+ lastModificationTime + "";
            sql += " WHERE ID = " + inbox_tab_actionDTO.iD;
				

			ps = connection.prepareStatement(sql);
			
			

			int index = 1;
			
			ps.setObject(index++,inbox_tab_actionDTO.inboxTabId);
			ps.setObject(index++,inbox_tab_actionDTO.icon);
			ps.setObject(index++,inbox_tab_actionDTO.descriptionLanguageId);
			ps.setObject(index++,inbox_tab_actionDTO.tooltipText);
			ps.setObject(index++,inbox_tab_actionDTO.actionLink);
			System.out.println(ps);
			ps.executeUpdate();
			


			
			recordUpdateTime(connection,  lastModificationTime, tableName);

						
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if (ps != null) {
					ps.close();
				}
			} catch(Exception e){}
			try{
				if(connection != null)
				{
					DBMW.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		return inbox_tab_actionDTO.iD;
	}
	
	public List<Inbox_tab_actionDTO> getDTOs(Collection recordIDs)
	{
		return getDTOs(recordIDs, tableName);
	}
	
	public List<Inbox_tab_actionDTO> getDTOs(Collection recordIDs, String tableName){
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Inbox_tab_actionDTO inbox_tab_actionDTO = null;
		List<Inbox_tab_actionDTO> inbox_tab_actionDTOList = new ArrayList<>();
		if(recordIDs.isEmpty()){
			return inbox_tab_actionDTOList;
		}
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
            
            sql += " WHERE ID IN ( ";

			for(int i = 0;i<recordIDs.size();i++){
				if(i!=0){
					sql+=",";
				}
				sql+=((ArrayList)recordIDs).get(i);
			}
			sql+=")  order by lastModificationTime desc";
			
			printSql(sql);
			
			logger.debug("sql " + sql);
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				inbox_tab_actionDTO = new Inbox_tab_actionDTO();
				inbox_tab_actionDTO.iD = rs.getLong("ID");
				inbox_tab_actionDTO.inboxTabId = rs.getLong("inbox_tab_id");
				inbox_tab_actionDTO.icon = rs.getString("icon");
				inbox_tab_actionDTO.descriptionLanguageId = rs.getLong("description_language_id");
				inbox_tab_actionDTO.tooltipText = rs.getLong("tooltip_text");
				inbox_tab_actionDTO.actionLink = rs.getString("action_link");
				inbox_tab_actionDTO.isDeleted = rs.getInt("isDeleted");
				inbox_tab_actionDTO.lastModificationTime = rs.getLong("lastModificationTime");
				System.out.println("got this DTO: " + inbox_tab_actionDTO);
				
				inbox_tab_actionDTOList.add(inbox_tab_actionDTO);

			}			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return inbox_tab_actionDTOList;
	
	}
	
	

	
	
	
	//add repository
	public List<Inbox_tab_actionDTO> getAllInbox_tab_action (boolean isFirstReload)
    {
		List<Inbox_tab_actionDTO> inbox_tab_actionDTOList = new ArrayList<>();

		String sql = "SELECT * FROM inbox_tab_action";
		sql += " WHERE ";
	

		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by inbox_tab_action.lastModificationTime desc";
		printSql(sql);
		
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;

		
		try{
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();
			rs = stmt.executeQuery(sql);
			

			while(rs.next()){
				Inbox_tab_actionDTO inbox_tab_actionDTO = new Inbox_tab_actionDTO();
				inbox_tab_actionDTO.iD = rs.getLong("ID");
				inbox_tab_actionDTO.inboxTabId = rs.getLong("inbox_tab_id");
				inbox_tab_actionDTO.icon = rs.getString("icon");
				inbox_tab_actionDTO.descriptionLanguageId = rs.getLong("description_language_id");
				inbox_tab_actionDTO.tooltipText = rs.getLong("tooltip_text");
				inbox_tab_actionDTO.actionLink = rs.getString("action_link");
				inbox_tab_actionDTO.isDeleted = rs.getInt("isDeleted");
				inbox_tab_actionDTO.lastModificationTime = rs.getLong("lastModificationTime");
				
				inbox_tab_actionDTOList.add(inbox_tab_actionDTO);
			}			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}

		return inbox_tab_actionDTOList;
    }
	
	
	public List<Inbox_tab_actionDTO> getAllInbox_tab_actionByTabID (long inTabID)
    {
		List<Inbox_tab_actionDTO> inbox_tab_actionDTOList = new ArrayList<>();

		String sql = "SELECT * FROM inbox_tab_action";
		sql += " WHERE inbox_tab_id = " + inTabID ;
	

		sql +="  and isDeleted =  0";
		sql += " order by inbox_tab_action.lastModificationTime desc";
		printSql(sql);
		
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;

		
		try{
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();
			rs = stmt.executeQuery(sql);
			

			while(rs.next()){
				Inbox_tab_actionDTO inbox_tab_actionDTO = new Inbox_tab_actionDTO();
				inbox_tab_actionDTO.iD = rs.getLong("ID");
				inbox_tab_actionDTO.inboxTabId = rs.getLong("inbox_tab_id");
				inbox_tab_actionDTO.icon = rs.getString("icon");
				inbox_tab_actionDTO.descriptionLanguageId = rs.getLong("description_language_id");
				inbox_tab_actionDTO.tooltipText = rs.getLong("tooltip_text");
				inbox_tab_actionDTO.actionLink = rs.getString("action_link");
				inbox_tab_actionDTO.isDeleted = rs.getInt("isDeleted");
				inbox_tab_actionDTO.lastModificationTime = rs.getLong("lastModificationTime");
				
				inbox_tab_actionDTOList.add(inbox_tab_actionDTO);
			}			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}

		return inbox_tab_actionDTOList;
    }
	
	public List<Inbox_tab_actionDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset)
	{
		return getDTOs(p_searchCriteria, limit, offset, tableName, true, 0);
	}
	
	public List<Inbox_tab_actionDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, String tableName, boolean isPermanentTable, long userApprovalPathType)
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		List<Inbox_tab_actionDTO> inbox_tab_actionDTOList = new ArrayList<>();

		try{
			
			String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, tableName, isPermanentTable, userApprovalPathType);
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				Inbox_tab_actionDTO inbox_tab_actionDTO = new Inbox_tab_actionDTO();
				inbox_tab_actionDTO.iD = rs.getLong("ID");
				inbox_tab_actionDTO.inboxTabId = rs.getLong("inbox_tab_id");
				inbox_tab_actionDTO.icon = rs.getString("icon");
				inbox_tab_actionDTO.descriptionLanguageId = rs.getLong("description_language_id");
				inbox_tab_actionDTO.tooltipText = rs.getLong("tooltip_text");
				inbox_tab_actionDTO.actionLink = rs.getString("action_link");
				inbox_tab_actionDTO.isDeleted = rs.getInt("isDeleted");
				inbox_tab_actionDTO.lastModificationTime = rs.getLong("lastModificationTime");
				
				inbox_tab_actionDTOList.add(inbox_tab_actionDTO);
			}					
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return inbox_tab_actionDTOList;
	
	}

		
	
	public String getSqlWithSearchCriteria(Hashtable p_searchCriteria, int limit, int offset, int category, String tableName, boolean isPermanentTable, long userApprovalPathType)
    {
		Inbox_tab_actionMAPS maps = new Inbox_tab_actionMAPS(tableName);
		String joinSQL = "";
		return getSqlWithSearchCriteria(p_searchCriteria, limit, offset, category, tableName, isPermanentTable, userApprovalPathType, maps, joinSQL);
    }			
}
	