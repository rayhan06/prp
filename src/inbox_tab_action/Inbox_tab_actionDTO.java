package inbox_tab_action;
import java.util.*; 
import util.*; 


public class Inbox_tab_actionDTO extends CommonDTO
{

	public long inboxTabId = 0;
    public String icon = "";
	public long descriptionLanguageId = 0;
    public long tooltipText = 0;
    public String actionLink = "";
	
	
    @Override
	public String toString() {
            return "$Inbox_tab_actionDTO[" +
            " iD = " + iD +
            " inboxTabId = " + inboxTabId +
            " icon = " + icon +
            " descriptionLanguageId = " + descriptionLanguageId +
            " tooltipText = " + tooltipText +
            " actionLink = " + actionLink +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}