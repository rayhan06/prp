package inbox_tab_action;
import java.util.*; 
import util.*;


public class Inbox_tab_actionMAPS extends CommonMaps
{	
	public Inbox_tab_actionMAPS(String tableName)
	{
		
		java_allfield_type_map.put("inbox_tab_id".toLowerCase(), "Long");
		java_allfield_type_map.put("icon".toLowerCase(), "String");
		java_allfield_type_map.put("description_language_id".toLowerCase(), "Long");
		java_allfield_type_map.put("tooltip_text".toLowerCase(), "String");
		java_allfield_type_map.put("action_link".toLowerCase(), "String");

		java_anyfield_search_map.put(tableName + ".inbox_tab_id".toLowerCase(), "Long");
		java_anyfield_search_map.put(tableName + ".icon".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".description_language_id".toLowerCase(), "Long");
		java_anyfield_search_map.put(tableName + ".tooltip_text".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".action_link".toLowerCase(), "String");

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("inboxTabId".toLowerCase(), "inboxTabId".toLowerCase());
		java_DTO_map.put("icon".toLowerCase(), "icon".toLowerCase());
		java_DTO_map.put("descriptionLanguageId".toLowerCase(), "descriptionLanguageId".toLowerCase());
		java_DTO_map.put("tooltipText".toLowerCase(), "tooltipText".toLowerCase());
		java_DTO_map.put("actionLink".toLowerCase(), "actionLink".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("inbox_tab_id".toLowerCase(), "inboxTabId".toLowerCase());
		java_SQL_map.put("icon".toLowerCase(), "icon".toLowerCase());
		java_SQL_map.put("description_language_id".toLowerCase(), "descriptionLanguageId".toLowerCase());
		java_SQL_map.put("tooltip_text".toLowerCase(), "tooltipText".toLowerCase());
		java_SQL_map.put("action_link".toLowerCase(), "actionLink".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Inbox Tab Id".toLowerCase(), "inboxTabId".toLowerCase());
		java_Text_map.put("Icon".toLowerCase(), "icon".toLowerCase());
		java_Text_map.put("Description Language Id".toLowerCase(), "descriptionLanguageId".toLowerCase());
		java_Text_map.put("Tooltip Text".toLowerCase(), "tooltipText".toLowerCase());
		java_Text_map.put("Action Link".toLowerCase(), "actionLink".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}