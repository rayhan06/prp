package inbox_tab_action;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class Inbox_tab_actionRepository implements Repository {
	Inbox_tab_actionDAO inbox_tab_actionDAO = null;
	
	public void setDAO(Inbox_tab_actionDAO inbox_tab_actionDAO)
	{
		this.inbox_tab_actionDAO = inbox_tab_actionDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Inbox_tab_actionRepository.class);
	Map<Long, Inbox_tab_actionDTO>mapOfInbox_tab_actionDTOToiD;
	Map<Long, Set<Inbox_tab_actionDTO> >mapOfInbox_tab_actionDTOToinboxTabId;
	Map<String, Set<Inbox_tab_actionDTO> >mapOfInbox_tab_actionDTOToicon;
	Map<Long, Set<Inbox_tab_actionDTO> >mapOfInbox_tab_actionDTOTodescriptionLanguageId;
	Map<Long, Set<Inbox_tab_actionDTO> >mapOfInbox_tab_actionDTOTotooltipText;
	Map<String, Set<Inbox_tab_actionDTO> >mapOfInbox_tab_actionDTOToactionLink;
	Map<Long, Set<Inbox_tab_actionDTO> >mapOfInbox_tab_actionDTOTolastModificationTime;


	static Inbox_tab_actionRepository instance = null;  
	private Inbox_tab_actionRepository(){
		mapOfInbox_tab_actionDTOToiD = new ConcurrentHashMap<>();
		mapOfInbox_tab_actionDTOToinboxTabId = new ConcurrentHashMap<>();
		mapOfInbox_tab_actionDTOToicon = new ConcurrentHashMap<>();
		mapOfInbox_tab_actionDTOTodescriptionLanguageId = new ConcurrentHashMap<>();
		mapOfInbox_tab_actionDTOTotooltipText = new ConcurrentHashMap<>();
		mapOfInbox_tab_actionDTOToactionLink = new ConcurrentHashMap<>();
		mapOfInbox_tab_actionDTOTolastModificationTime = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Inbox_tab_actionRepository getInstance(){
		if (instance == null){
			instance = new Inbox_tab_actionRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(inbox_tab_actionDAO == null)
		{
			return;
		}
		try {
			List<Inbox_tab_actionDTO> inbox_tab_actionDTOs = inbox_tab_actionDAO.getAllInbox_tab_action(reloadAll);
			for(Inbox_tab_actionDTO inbox_tab_actionDTO : inbox_tab_actionDTOs) {
				Inbox_tab_actionDTO oldInbox_tab_actionDTO = getInbox_tab_actionDTOByID(inbox_tab_actionDTO.iD);
				if( oldInbox_tab_actionDTO != null ) {
					mapOfInbox_tab_actionDTOToiD.remove(oldInbox_tab_actionDTO.iD);
				
					if(mapOfInbox_tab_actionDTOToinboxTabId.containsKey(oldInbox_tab_actionDTO.inboxTabId)) {
						mapOfInbox_tab_actionDTOToinboxTabId.get(oldInbox_tab_actionDTO.inboxTabId).remove(oldInbox_tab_actionDTO);
					}
					if(mapOfInbox_tab_actionDTOToinboxTabId.get(oldInbox_tab_actionDTO.inboxTabId).isEmpty()) {
						mapOfInbox_tab_actionDTOToinboxTabId.remove(oldInbox_tab_actionDTO.inboxTabId);
					}
					
					if(mapOfInbox_tab_actionDTOToicon.containsKey(oldInbox_tab_actionDTO.icon)) {
						mapOfInbox_tab_actionDTOToicon.get(oldInbox_tab_actionDTO.icon).remove(oldInbox_tab_actionDTO);
					}
					if(mapOfInbox_tab_actionDTOToicon.get(oldInbox_tab_actionDTO.icon).isEmpty()) {
						mapOfInbox_tab_actionDTOToicon.remove(oldInbox_tab_actionDTO.icon);
					}
					
					if(mapOfInbox_tab_actionDTOTodescriptionLanguageId.containsKey(oldInbox_tab_actionDTO.descriptionLanguageId)) {
						mapOfInbox_tab_actionDTOTodescriptionLanguageId.get(oldInbox_tab_actionDTO.descriptionLanguageId).remove(oldInbox_tab_actionDTO);
					}
					if(mapOfInbox_tab_actionDTOTodescriptionLanguageId.get(oldInbox_tab_actionDTO.descriptionLanguageId).isEmpty()) {
						mapOfInbox_tab_actionDTOTodescriptionLanguageId.remove(oldInbox_tab_actionDTO.descriptionLanguageId);
					}
					
					if(mapOfInbox_tab_actionDTOTotooltipText.containsKey(oldInbox_tab_actionDTO.tooltipText)) {
						mapOfInbox_tab_actionDTOTotooltipText.get(oldInbox_tab_actionDTO.tooltipText).remove(oldInbox_tab_actionDTO);
					}
					if(mapOfInbox_tab_actionDTOTotooltipText.get(oldInbox_tab_actionDTO.tooltipText).isEmpty()) {
						mapOfInbox_tab_actionDTOTotooltipText.remove(oldInbox_tab_actionDTO.tooltipText);
					}
					
					if(mapOfInbox_tab_actionDTOToactionLink.containsKey(oldInbox_tab_actionDTO.actionLink)) {
						mapOfInbox_tab_actionDTOToactionLink.get(oldInbox_tab_actionDTO.actionLink).remove(oldInbox_tab_actionDTO);
					}
					if(mapOfInbox_tab_actionDTOToactionLink.get(oldInbox_tab_actionDTO.actionLink).isEmpty()) {
						mapOfInbox_tab_actionDTOToactionLink.remove(oldInbox_tab_actionDTO.actionLink);
					}
					
					if(mapOfInbox_tab_actionDTOTolastModificationTime.containsKey(oldInbox_tab_actionDTO.lastModificationTime)) {
						mapOfInbox_tab_actionDTOTolastModificationTime.get(oldInbox_tab_actionDTO.lastModificationTime).remove(oldInbox_tab_actionDTO);
					}
					if(mapOfInbox_tab_actionDTOTolastModificationTime.get(oldInbox_tab_actionDTO.lastModificationTime).isEmpty()) {
						mapOfInbox_tab_actionDTOTolastModificationTime.remove(oldInbox_tab_actionDTO.lastModificationTime);
					}
					
					
				}
				if(inbox_tab_actionDTO.isDeleted == 0) 
				{
					
					mapOfInbox_tab_actionDTOToiD.put(inbox_tab_actionDTO.iD, inbox_tab_actionDTO);
				
					if( ! mapOfInbox_tab_actionDTOToinboxTabId.containsKey(inbox_tab_actionDTO.inboxTabId)) {
						mapOfInbox_tab_actionDTOToinboxTabId.put(inbox_tab_actionDTO.inboxTabId, new HashSet<>());
					}
					mapOfInbox_tab_actionDTOToinboxTabId.get(inbox_tab_actionDTO.inboxTabId).add(inbox_tab_actionDTO);
					
					if( ! mapOfInbox_tab_actionDTOToicon.containsKey(inbox_tab_actionDTO.icon)) {
						mapOfInbox_tab_actionDTOToicon.put(inbox_tab_actionDTO.icon, new HashSet<>());
					}
					mapOfInbox_tab_actionDTOToicon.get(inbox_tab_actionDTO.icon).add(inbox_tab_actionDTO);
					
					if( ! mapOfInbox_tab_actionDTOTodescriptionLanguageId.containsKey(inbox_tab_actionDTO.descriptionLanguageId)) {
						mapOfInbox_tab_actionDTOTodescriptionLanguageId.put(inbox_tab_actionDTO.descriptionLanguageId, new HashSet<>());
					}
					mapOfInbox_tab_actionDTOTodescriptionLanguageId.get(inbox_tab_actionDTO.descriptionLanguageId).add(inbox_tab_actionDTO);
					
					if( ! mapOfInbox_tab_actionDTOTotooltipText.containsKey(inbox_tab_actionDTO.tooltipText)) {
						mapOfInbox_tab_actionDTOTotooltipText.put(inbox_tab_actionDTO.tooltipText, new HashSet<>());
					}
					mapOfInbox_tab_actionDTOTotooltipText.get(inbox_tab_actionDTO.tooltipText).add(inbox_tab_actionDTO);
					
					if( ! mapOfInbox_tab_actionDTOToactionLink.containsKey(inbox_tab_actionDTO.actionLink)) {
						mapOfInbox_tab_actionDTOToactionLink.put(inbox_tab_actionDTO.actionLink, new HashSet<>());
					}
					mapOfInbox_tab_actionDTOToactionLink.get(inbox_tab_actionDTO.actionLink).add(inbox_tab_actionDTO);
					
					if( ! mapOfInbox_tab_actionDTOTolastModificationTime.containsKey(inbox_tab_actionDTO.lastModificationTime)) {
						mapOfInbox_tab_actionDTOTolastModificationTime.put(inbox_tab_actionDTO.lastModificationTime, new HashSet<>());
					}
					mapOfInbox_tab_actionDTOTolastModificationTime.get(inbox_tab_actionDTO.lastModificationTime).add(inbox_tab_actionDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Inbox_tab_actionDTO> getInbox_tab_actionList() {
		List <Inbox_tab_actionDTO> inbox_tab_actions = new ArrayList<Inbox_tab_actionDTO>(this.mapOfInbox_tab_actionDTOToiD.values());
		return inbox_tab_actions;
	}
	
	
	public Inbox_tab_actionDTO getInbox_tab_actionDTOByID( long ID){
		return mapOfInbox_tab_actionDTOToiD.get(ID);
	}
	
	
	public List<Inbox_tab_actionDTO> getInbox_tab_actionDTOByinbox_tab_id(long inbox_tab_id) {
		return new ArrayList<>( mapOfInbox_tab_actionDTOToinboxTabId.getOrDefault(inbox_tab_id,new HashSet<>()));
	}
	
	
	public List<Inbox_tab_actionDTO> getInbox_tab_actionDTOByicon(String icon) {
		return new ArrayList<>( mapOfInbox_tab_actionDTOToicon.getOrDefault(icon,new HashSet<>()));
	}
	
	
	public List<Inbox_tab_actionDTO> getInbox_tab_actionDTOBydescription_language_id(long description_language_id) {
		return new ArrayList<>( mapOfInbox_tab_actionDTOTodescriptionLanguageId.getOrDefault(description_language_id,new HashSet<>()));
	}
	
	
	public List<Inbox_tab_actionDTO> getInbox_tab_actionDTOBytooltip_text(String tooltip_text) {
		return new ArrayList<>( mapOfInbox_tab_actionDTOTotooltipText.getOrDefault(tooltip_text,new HashSet<>()));
	}
	
	
	public List<Inbox_tab_actionDTO> getInbox_tab_actionDTOByaction_link(String action_link) {
		return new ArrayList<>( mapOfInbox_tab_actionDTOToactionLink.getOrDefault(action_link,new HashSet<>()));
	}
	
	
	public List<Inbox_tab_actionDTO> getInbox_tab_actionDTOBylastModificationTime(long lastModificationTime) {
		return new ArrayList<>( mapOfInbox_tab_actionDTOTolastModificationTime.getOrDefault(lastModificationTime,new HashSet<>()));
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "inbox_tab_action";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


