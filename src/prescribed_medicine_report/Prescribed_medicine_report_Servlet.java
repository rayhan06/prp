package prescribed_medicine_report;


import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import language.LC;
import language.LM;

import login.LoginDTO;
import permission.MenuConstants;

import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;

import util.ReportRequestHandler;
import pbReport.*;






@WebServlet("/Prescribed_medicine_report_Servlet")
public class Prescribed_medicine_report_Servlet  extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	String[][] Criteria =
	{
		{"criteria","appointment","visit_date",">=","","long","","",Long.MIN_VALUE + "","startDate", LC.HM_START_DATE + ""},		
		{"criteria","appointment","visit_date","<=","AND","long","","",Long.MAX_VALUE + "","endDate", LC.HM_END_DATE + ""},		
		{"criteria","","dr_employee_record_id","=","AND","String","","","any","dr_user_name", LC.HM_DOCTOR + "", "userNameToEmployeeRecordId"},		
		{"criteria","","drug_information_type","=","AND","int","","","any","drugInformationType", LC.HM_DRUGS + ""}		
	};
	
	String[][] Display =
	{
		{"display", "", "appointment.dr_employee_record_id", "erIdToName", ""},		
		{"display","","CONCAT(drug_information.name_en, ', ', strength)","basic",""},		
		{"display","","SUM(quantity)","int",""},		
		{"display","","SUM(quantity * drug_information.unit_price)","double",""}		
	};
	
	String GroupBy = "appointment.dr_employee_record_id, drug_information_type";
	String OrderBY = " SUM(quantity * drug_information.unit_price) desc ";
	
	ReportRequestHandler reportRequestHandler;
	
	public Prescribed_medicine_report_Servlet(){

	}
	
	private final ReportService reportService = new ReportService();
	
	private String sql;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String language = "english";
		if(userDTO!= null && userDTO.languageID == SessionConstants.BANGLA)
		{
			language = "bangla";
		}
				
		String actionType = request.getParameter("actionType");

		System.out.println("In ssservlet doget, actiontype = " + actionType);
		
		sql = "patient_prescription_medicine2 "
				+ "JOIN prescription_details ON patient_prescription_medicine2.prescription_details_id = prescription_details.ID "
				+ "JOIN appointment ON prescription_details.appointment_id = appointment.ID "
				+ "JOIN drug_information ON drug_information.id = drug_information_type";

		Display[0][4] = LM.getText(LC.HM_DOCTOR, loginDTO);
		Display[1][4] = LM.getText(LC.HM_DRUGS, loginDTO);
		Display[2][4] = LM.getText(LC.HM_COUNT, loginDTO);
		Display[3][4] = LM.getText(LC.HM_COST, loginDTO);

		
		String reportName = LM.getText(LC.PRESCRIBED_MEDICINE_REPORT_OTHER_PRESCRIBED_MEDICINE_REPORT, loginDTO);
		
		reportRequestHandler = new ReportRequestHandler(null,
				Criteria, Display, GroupBy, OrderBY, sql,
				reportService);
		
		reportRequestHandler.clientSideDataFormatting = new ArrayList<Pair<Integer, Integer>>();
		reportRequestHandler.clientSideDataFormatting.add(new ImmutablePair<Integer, Integer>(2, ReportRequestHandler.RIGHT_ALIGN_INT));
		reportRequestHandler.clientSideDataFormatting.add(new ImmutablePair<Integer, Integer>(3, ReportRequestHandler.RIGHT_ALIGN_FLOAT));

		
		reportRequestHandler.handleReportGet(request, response, userDTO, "prescribed_medicine_report",
				MenuConstants.PRESCRIBED_MEDICINE_REPORT_DETAILS, language, reportName, "prescribed_medicine_report");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		doGet(request, response);
	}
}
