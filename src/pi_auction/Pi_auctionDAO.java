package pi_auction;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.sql.SQLException;

import common.CommonDAOService;
import common.ConnectionAndStatementUtil;
import org.apache.log4j.Logger;
import pi_package_vendor.Pi_package_vendorDTO;
import user.UserDTO;
import util.*;
import pb.*;

public class Pi_auctionDAO implements CommonDAOService<Pi_auctionDTO> {

    Logger logger = Logger.getLogger(getClass());

    private static String addQuery = "";
    private static String updateQuery = "";
    private String[] columnNames = null;

    private static final String UpdateAuctionStatus =
            "UPDATE pi_auction SET auction_status = %d,modified_by = %d,lastModificationTime = %d " +
                    " WHERE ID = %d AND isDeleted = %d ";

    private Pi_auctionDAO() {
        columnNames = new String[]
                {
                        "ID",
                        "auction_package_name",
                        "requester_org_id",
                        "requester_office_id",
                        "requester_office_unit_id",
                        "requester_emp_id",
                        "requester_phone_num",
                        "requester_name_en",
                        "requester_name_bn",
                        "requester_office_name_en",
                        "requester_office_name_bn",
                        "requester_office_unit_name_en",
                        "requester_office_unit_name_bn",
                        "requester_office_unit_org_name_en",
                        "requester_office_unit_org_name_bn",
                        "requested_date",
                        "auction_status",
                        "item_count",
                        "total_selling_price",
                        "total_buying_price",
                        "insertion_date",
                        "inserted_by",
                        "modified_by",
                        "search_column",
                        "office_unit_id",
                        "isDeleted",
                        "lastModificationTime"
                };
        updateQuery = getUpdateQuery(columnNames);
        addQuery = getAddQuery(columnNames);

        searchMap.put("auctionStatus", " AND (auction_status = ?)");
        searchMap.put("officeUnitId", " AND (office_unit_id = ?)");
        searchMap.put("auctionPackageName", " AND (auction_package_name like ?)");
        searchMap.put("itemCount", " AND (item_count = ?)");
    }

    private final Map<String, String> searchMap = new HashMap<>();

    private static class LazyLoader {
        static final Pi_auctionDAO INSTANCE = new Pi_auctionDAO();
    }

    public static Pi_auctionDAO getInstance() {
        return LazyLoader.INSTANCE;
    }

    public void setSearchColumn(Pi_auctionDTO pi_auctionDTO) {
        pi_auctionDTO.searchColumn = "";
        pi_auctionDTO.searchColumn += pi_auctionDTO.auctionPackageName + " ";
        pi_auctionDTO.searchColumn += pi_auctionDTO.itemCount + " ";
        pi_auctionDTO.searchColumn += pi_auctionDTO.totalBuyingPrice + " ";
        pi_auctionDTO.searchColumn += pi_auctionDTO.totalSellingPrice + " ";
    }

    @Override
    public void set(PreparedStatement ps, Pi_auctionDTO pi_auctionDTO, boolean isInsert) throws SQLException {
        int index = 0;
        long lastModificationTime = System.currentTimeMillis();
        setSearchColumn(pi_auctionDTO);
        if (isInsert) {
            ps.setObject(++index, pi_auctionDTO.iD);
        }
        ps.setObject(++index, pi_auctionDTO.auctionPackageName);
        ps.setObject(++index, pi_auctionDTO.requesterOrgId);
        ps.setObject(++index, pi_auctionDTO.requesterOfficeId);
        ps.setObject(++index, pi_auctionDTO.requesterOfficeUnitId);
        ps.setObject(++index, pi_auctionDTO.requesterEmpId);
        ps.setObject(++index, pi_auctionDTO.requesterPhoneNum);
        ps.setObject(++index, pi_auctionDTO.requesterNameEn);
        ps.setObject(++index, pi_auctionDTO.requesterNameBn);
        ps.setObject(++index, pi_auctionDTO.requesterOfficeNameEn);
        ps.setObject(++index, pi_auctionDTO.requesterOfficeNameBn);
        ps.setObject(++index, pi_auctionDTO.requesterOfficeUnitNameEn);
        ps.setObject(++index, pi_auctionDTO.requesterOfficeUnitNameBn);
        ps.setObject(++index, pi_auctionDTO.requesterOfficeUnitOrgNameEn);
        ps.setObject(++index, pi_auctionDTO.requesterOfficeUnitOrgNameBn);
        ps.setObject(++index, pi_auctionDTO.requestedDate);
        ps.setObject(++index, pi_auctionDTO.auctionStatus);
        ps.setObject(++index, pi_auctionDTO.itemCount);
        ps.setObject(++index, pi_auctionDTO.totalSellingPrice);
        ps.setObject(++index, pi_auctionDTO.totalBuyingPrice);
        ps.setObject(++index, pi_auctionDTO.insertionDate);
        ps.setObject(++index, pi_auctionDTO.insertedBy);
        ps.setObject(++index, pi_auctionDTO.modifiedBy);
        ps.setObject(++index, pi_auctionDTO.searchColumn);
        ps.setObject(++index, pi_auctionDTO.officeUnitId);
        if (isInsert) {
            ps.setObject(++index, pi_auctionDTO.isDeleted);
        }
        ps.setObject(++index, lastModificationTime);
        if (!isInsert) {
            ps.setObject(++index, pi_auctionDTO.iD);
        }
    }

    @Override
    public Pi_auctionDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Pi_auctionDTO pi_auctionDTO = new Pi_auctionDTO();
            int i = 0;
            pi_auctionDTO.iD = rs.getLong(columnNames[i++]);
            pi_auctionDTO.auctionPackageName = rs.getString(columnNames[i++]);
            pi_auctionDTO.requesterOrgId = rs.getLong(columnNames[i++]);
            pi_auctionDTO.requesterOfficeId = rs.getLong(columnNames[i++]);
            pi_auctionDTO.requesterOfficeUnitId = rs.getLong(columnNames[i++]);
            pi_auctionDTO.requesterEmpId = rs.getLong(columnNames[i++]);
            pi_auctionDTO.requesterPhoneNum = rs.getString(columnNames[i++]);
            pi_auctionDTO.requesterNameEn = rs.getString(columnNames[i++]);
            pi_auctionDTO.requesterNameBn = rs.getString(columnNames[i++]);
            pi_auctionDTO.requesterOfficeNameEn = rs.getString(columnNames[i++]);
            pi_auctionDTO.requesterOfficeNameBn = rs.getString(columnNames[i++]);
            pi_auctionDTO.requesterOfficeUnitNameEn = rs.getString(columnNames[i++]);
            pi_auctionDTO.requesterOfficeUnitNameBn = rs.getString(columnNames[i++]);
            pi_auctionDTO.requesterOfficeUnitOrgNameEn = rs.getString(columnNames[i++]);
            pi_auctionDTO.requesterOfficeUnitOrgNameBn = rs.getString(columnNames[i++]);
            pi_auctionDTO.requestedDate = rs.getLong(columnNames[i++]);
            pi_auctionDTO.auctionStatus = rs.getInt(columnNames[i++]);
            pi_auctionDTO.itemCount = rs.getDouble(columnNames[i++]);
            pi_auctionDTO.totalSellingPrice = rs.getDouble(columnNames[i++]);
            pi_auctionDTO.totalBuyingPrice = rs.getDouble(columnNames[i++]);
            pi_auctionDTO.insertionDate = rs.getLong(columnNames[i++]);
            pi_auctionDTO.insertedBy = rs.getString(columnNames[i++]);
            pi_auctionDTO.modifiedBy = rs.getString(columnNames[i++]);
            pi_auctionDTO.searchColumn = rs.getString(columnNames[i++]);
            pi_auctionDTO.officeUnitId = rs.getLong(columnNames[i++]);
            pi_auctionDTO.isDeleted = rs.getInt(columnNames[i++]);
            pi_auctionDTO.lastModificationTime = rs.getLong(columnNames[i++]);

            return pi_auctionDTO;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    public Pi_auctionDTO getDTOByID(long id) {
        return getDTOFromID(id);
    }

    @Override
    public String getTableName() {
        return "pi_auction";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Pi_auctionDTO) commonDTO, addQuery, true);
    }

    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Pi_auctionDTO) commonDTO, updateQuery, false);
    }

    @Override
    public String getLastModifierUser() {
        return "modified_by";
    }

    public void UpdateAuctionStatus(long auction_status, long id, UserDTO userDTO) {

        String sql = String.format(
                UpdateAuctionStatus, auction_status,
                userDTO.employee_record_id, System.currentTimeMillis(), id
                , 0
        );

        boolean res = (Boolean) ConnectionAndStatementUtil.getWriteStatement(st -> {
            try {
                st.executeUpdate(sql);
                return true;
            } catch (SQLException ex) {
                logger.error(ex);
                return false;
            }
        });
    }

}
	