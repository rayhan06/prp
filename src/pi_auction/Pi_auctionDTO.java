package pi_auction;
import java.util.*; 
import util.*; 


public class Pi_auctionDTO extends CommonDTO
{

    public String auctionPackageName = "";
	public long requesterOrgId = -1;
	public long requesterOfficeId = -1;
	public long requesterOfficeUnitId = -1;
	public long requesterEmpId = -1;
    public String requesterPhoneNum = "";
    public String requesterNameEn = "";
    public String requesterNameBn = "";
    public String requesterOfficeNameEn = "";
    public String requesterOfficeNameBn = "";
    public String requesterOfficeUnitNameEn = "";
    public String requesterOfficeUnitNameBn = "";
    public String requesterOfficeUnitOrgNameEn = "";
    public String requesterOfficeUnitOrgNameBn = "";
	public long requestedDate = System.currentTimeMillis();
	public int auctionStatus = -1;
	public double itemCount = -1;
	public double totalSellingPrice = -1;
	public double totalBuyingPrice = -1;
	public long insertionDate = -1;
    public String insertedBy = "";
    public String modifiedBy = "";
    public long officeUnitId = -1;
	
	
    @Override
	public String toString() {
            return "$Pi_auctionDTO[" +
            " iD = " + iD +
            " auctionPackageName = " + auctionPackageName +
            " requesterOrgId = " + requesterOrgId +
            " requesterOfficeId = " + requesterOfficeId +
            " requesterOfficeUnitId = " + requesterOfficeUnitId +
            " requesterEmpId = " + requesterEmpId +
            " requesterPhoneNum = " + requesterPhoneNum +
            " requesterNameEn = " + requesterNameEn +
            " requesterNameBn = " + requesterNameBn +
            " requesterOfficeNameEn = " + requesterOfficeNameEn +
            " requesterOfficeNameBn = " + requesterOfficeNameBn +
            " requesterOfficeUnitNameEn = " + requesterOfficeUnitNameEn +
            " requesterOfficeUnitNameBn = " + requesterOfficeUnitNameBn +
            " requesterOfficeUnitOrgNameEn = " + requesterOfficeUnitOrgNameEn +
            " requesterOfficeUnitOrgNameBn = " + requesterOfficeUnitOrgNameBn +
            " requestedDate = " + requestedDate +
            " auctionStatus = " + auctionStatus +
            " itemCount = " + itemCount +
            " totalSellingPrice = " + totalSellingPrice +
            " totalBuyingPrice = " + totalBuyingPrice +
            " insertionDate = " + insertionDate +
            " insertedBy = " + insertedBy +
            " modifiedBy = " + modifiedBy +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            " searchColumn = " + searchColumn +
            " officeUnitId = " + officeUnitId +
            "]";
    }

}