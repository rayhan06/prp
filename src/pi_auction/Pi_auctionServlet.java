package pi_auction;

import com.google.gson.Gson;
import common.ApiResponse;
import common.BaseServlet;
import common.CommonDAOService;
import employee_assign.EmployeeSearchModalUtil;
import employee_assign.EmployeeSearchModel;
import login.LoginDTO;
import office_units.Office_unitsRepository;
import offices.OfficesDTO;
import offices.OfficesRepository;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import pb.Utils;
import permission.MenuConstants;
import pi_auction_items.Pi_auction_itemsDAO;
import pi_auction_items.Pi_auction_itemsDTO;
import pi_unique_item.PiStageEnum;
import pi_unique_item.PiUniqueItemAssignmentDAO;
import procurement_goods.Procurement_goodsDAO;
import role.PermissionRepository;
import role.RoleDTO;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;
import util.HttpRequestUtils;
import vm_requisition.CommonApprovalStatus;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

import static permission.MenuConstants.*;


/**
 * Servlet implementation class Pi_auctionServlet
 */
@WebServlet("/Pi_auctionServlet")
@MultipartConfig
public class Pi_auctionServlet extends BaseServlet {
    private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Pi_auctionServlet.class);

    @Override
    public String getTableName() {
        return Pi_auctionDAO.getInstance().getTableName();
    }

    @Override
    public String getServletName() {
        return "Pi_auctionServlet";
    }

    @Override
    public Pi_auctionDAO getCommonDAOService() {
        return Pi_auctionDAO.getInstance();
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[]{MenuConstants.PI_AUCTION_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[]{MenuConstants.PI_AUCTION_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[]{MenuConstants.PI_AUCTION_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Pi_auctionServlet.class;
    }

    private final Gson gson = new Gson();


    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {

        String language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
        boolean isLangEng = language.equalsIgnoreCase("English");
        Pi_auctionDTO pi_auctionDTO;
        SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
        Procurement_goodsDAO procurement_goodsDAO = Procurement_goodsDAO.getInstance();

        long totalSold = 0;
        double totalUnitSellingPrice = 0;
        double totalUnitBuyingPrice = 0;


        if (addFlag) {
            pi_auctionDTO = new Pi_auctionDTO();
            pi_auctionDTO.insertionDate = pi_auctionDTO.lastModificationTime = System.currentTimeMillis();
            pi_auctionDTO.insertedBy = pi_auctionDTO.modifiedBy = String.valueOf(userDTO.ID);
        } else {
            pi_auctionDTO = Pi_auctionDAO.getInstance().getDTOFromID(Long.parseLong(request.getParameter("iD")));
            if (pi_auctionDTO == null) {
                throw new Exception(isLangEng ? "Auction information is not found" : "নিলামের তথ্য প্রদান করুন");
            }
            pi_auctionDTO.lastModificationTime = System.currentTimeMillis();
            pi_auctionDTO.modifiedBy = String.valueOf(userDTO.ID);
        }

        String Value = "";

        Value = request.getParameter("auctionPackageName");
        if (Value == null || Value.equals("") || Value.equals("-1")) {
            throw new Exception(isLangEng ? "Please provide auction package name" : "অনুগ্রহপূর্বক নিলামের প্যাকেজ প্রদান করুন");
        }
        pi_auctionDTO.auctionPackageName = Jsoup.clean(Value, Whitelist.simpleText());

        List<Pi_auction_itemsDTO> pi_auction_itemsDTOS = new ArrayList<>();
        String[] paramValues = request.getParameterValues("piPurchaseItemsId");
        long officeUnitId = -1;

        for (int i = 0; i < paramValues.length; i++) {
            Value = request.getParameterValues("stock")[i];
            if (Value == null || Value.equals("") || Value.equals("-1") || Value.contains("-") || Value.isEmpty()) {
                throw new Exception(isLangEng ? "Please provide valid stock" : "অনুগ্রহপূর্বক বৈধ মজুদ প্রদান করুন");
            }
            long remainingStock = Long.parseLong(Value);

            Value = request.getParameterValues("sellingUnit")[i];
            if (Value == null || Value.equals("") || Value.equals("-1") || Value.contains("-") || Value.isEmpty()) {
                throw new Exception(isLangEng ? "Please provide valid selling unit" : "অনুগ্রহপূর্বক বৈধ বিক্রয় সংখ্যা প্রদান করুন");
            }
            if (addFlag) {
                long givenSellingUnit = Long.parseLong(Value);
                if (givenSellingUnit > remainingStock) {
                    throw new Exception(isLangEng ? "Please provide selling unit less or equal to stock" : "অনুগ্রহপূর্বক মজুদ থেকে কম অথবা মজুদের সমান বিক্রয় সংখ্যা প্রদান করুন");
                } else if (givenSellingUnit <= 0) {
                    throw new Exception(isLangEng ? "Please provide valid selling unit" : "অনুগ্রহপূর্বক বৈধ বিক্রয় সংখ্যা প্রদান করুন");
                }
            }
        }

        for (int i = 0; i < paramValues.length; i++) {
            Pi_auction_itemsDTO pi_auction_itemsDTO;
            if (addFlag) {
                pi_auction_itemsDTO = new Pi_auction_itemsDTO();
                pi_auction_itemsDTO.insertionDate = pi_auction_itemsDTO.lastModificationTime = System.currentTimeMillis();
                pi_auction_itemsDTO.insertedBy = pi_auction_itemsDTO.modifiedBy = String.valueOf(userDTO.ID);


            } else {
                pi_auction_itemsDTO = Pi_auction_itemsDAO.getInstance().getDTOFromID(Long.parseLong(request.getParameterValues("piAuctionItemId")[i]));
                if (pi_auction_itemsDTO == null) {
                    throw new Exception(isLangEng ? "Product item information is not found" : "পণ্যের তথ্য খুঁজে পাওয়া যায়নি");
                }
                pi_auction_itemsDTO.lastModificationTime = System.currentTimeMillis();
                pi_auction_itemsDTO.modifiedBy = String.valueOf(userDTO.ID);
            }

            Value = "";

            Value = request.getParameterValues("piPurchaseItemsId")[i];
            if (Value == null || Value.equals("") || Value.equals("-1")) {
                throw new Exception(isLangEng ? "Please provide product" : "অনুগ্রহপূর্বক পণ্য প্রদান করুন");
            }
            pi_auction_itemsDTO.piPurchaseItemsId = Long.parseLong(Value);

            Value = request.getParameterValues("itemId")[i];
            if (Value == null || Value.equals("") || Value.equals("-1")) {
                throw new Exception(isLangEng ? "Please provide item" : "অনুগ্রহপূর্বক আইটেম করুন");
            }
            pi_auction_itemsDTO.itemId = Long.parseLong(Value);

            Value = request.getParameterValues("productName")[i];
            if (Value == null || Value.equals("") || Value.equals("-1")) {
                throw new Exception(isLangEng ? "Please provide product name" : "অনুগ্রহপূর্বক পণ্যের নাম প্রদান করুন");
            }
            pi_auction_itemsDTO.productName = (Value);

            Value = request.getParameterValues("supplierCompanyName")[i];
            if (Value == null || Value.equals("") || Value.equals("-1")) {
                throw new Exception(isLangEng ? "Please provide supplier company name" : "অনুগ্রহপূর্বক সরবরাহকারী প্রতিষ্ঠানের নাম প্রদান করুন");
            }
            pi_auction_itemsDTO.supplierCompanyName = (Value);


            Value = request.getParameterValues("buyingDate")[i];
            if (Value == null || Value.equals("") || Value.equals("-1")) {
                throw new Exception(isLangEng ? "Please provide buying date" : "অনুগ্রহপূর্বক সরবরাহকারী প্রতিষ্ঠানের নাম প্রদান করুন");
            }
            pi_auction_itemsDTO.buyingDate = Long.parseLong(Value);

            Value = request.getParameterValues("fiscalYearId")[i];
            if (Value == null || Value.equals("") || Value.equals("-1")) {
                throw new Exception(isLangEng ? "Please provide fiscal year" : "অনুগ্রহপূর্বক অর্থবছর প্রদান করুন");
            }
            pi_auction_itemsDTO.fiscalYearId = Long.parseLong(Value);

            Value = request.getParameterValues("officeUnitId")[i];
            if (Value == null || Value.equals("") || Value.equals("-1")) {
                throw new Exception(isLangEng ? "Please provide office" : "অনুগ্রহপূর্বক অফিস প্রদান করুন");
            }
            pi_auction_itemsDTO.officeUnitId = Long.parseLong((Value));
            officeUnitId = pi_auction_itemsDTO.officeUnitId;

            Value = request.getParameterValues("unitBuyingPrice")[i];
            if (Value == null || Value.equals("") || Value.equals("-1")) {
                throw new Exception(isLangEng ? "Please provide unit buying price" : "অনুগ্রহপূর্বক বিক্রয়মূল্য প্রদান করুন");
            }
            pi_auction_itemsDTO.unitBuyingPrice = Double.parseDouble(Value);

            Value = request.getParameterValues("sellingUnit")[i];
            if (Value == null || Value.equals("") || Value.equals("-1")) {
                throw new Exception(isLangEng ? "Please provide selling unit" : "অনুগ্রহপূর্বক বিক্রয় সংখ্যা প্রদান করুন");
            }
            long prevSellingUnit = pi_auction_itemsDTO.sellingUnit;
            pi_auction_itemsDTO.sellingUnit = Long.parseLong(Value);
            if(pi_auction_itemsDTO.sellingUnit < 0) {
                pi_auction_itemsDTO.sellingUnit = 0;
            }


            Value = request.getParameterValues("stock")[i];
            if (Value == null || Value.equals("") || Value.equals("-1")) {
                throw new Exception(isLangEng ? "Please provide stock" : "অনুগ্রহপূর্বক মজুদ প্রদান করুন");
            }
            if (addFlag) {
                int initialStockInt = PiUniqueItemAssignmentDAO.getInstance()
                        .getItemCountInStockByItemIdAndPurchaseId(pi_auction_itemsDTO.itemId, pi_auction_itemsDTO.piPurchaseItemsId);
                if(pi_auction_itemsDTO.sellingUnit> initialStockInt){
                    pi_auction_itemsDTO.sellingUnit = initialStockInt;
                }
                pi_auction_itemsDTO.stock = initialStockInt - pi_auction_itemsDTO.sellingUnit;
            }


            Value = request.getParameterValues("unitSellingPrice")[i];
            if (Value == null || Value.equals("") || Value.equals("-1") || Value.contains("-")) {
                throw new Exception(isLangEng ? "Please provide unit selling price" : "অনুগ্রহপূর্বক একক বিক্রয় সংখ্যা প্রদান করুন");
            }
            pi_auction_itemsDTO.unitSellingPrice = Double.parseDouble(Value);

            totalSold += pi_auction_itemsDTO.sellingUnit;
            totalUnitBuyingPrice += (pi_auction_itemsDTO.unitBuyingPrice * pi_auction_itemsDTO.sellingUnit);
            totalUnitSellingPrice += (pi_auction_itemsDTO.unitSellingPrice * pi_auction_itemsDTO.sellingUnit);

            if (addFlag) {
                PiUniqueItemAssignmentDAO.getInstance().UpdateStageAfterAuction(PiStageEnum.AUCTIONED.getValue(),
                        pi_auction_itemsDTO.piPurchaseItemsId, pi_auction_itemsDTO.officeUnitId,
                        pi_auction_itemsDTO.sellingUnit, pi_auction_itemsDTO.lastModificationTime, PiStageEnum.IN_STOCK.getValue(), pi_auction_itemsDTO.itemId);
            }
            if (!addFlag) {
                long limit = 0;
                if (prevSellingUnit > pi_auction_itemsDTO.sellingUnit) {
                    limit = prevSellingUnit - pi_auction_itemsDTO.sellingUnit;
                    PiUniqueItemAssignmentDAO.getInstance().UpdateStageAfterAuction(PiStageEnum.IN_STOCK.getValue(),
                            pi_auction_itemsDTO.piPurchaseItemsId, pi_auction_itemsDTO.officeUnitId,
                            limit, pi_auction_itemsDTO.lastModificationTime, PiStageEnum.AUCTIONED.getValue(), pi_auction_itemsDTO.itemId);

                } else if (prevSellingUnit < pi_auction_itemsDTO.sellingUnit) {
                    limit = pi_auction_itemsDTO.sellingUnit - prevSellingUnit;
                    PiUniqueItemAssignmentDAO.getInstance().UpdateStageAfterAuction(PiStageEnum.AUCTIONED.getValue(),
                            pi_auction_itemsDTO.piPurchaseItemsId, pi_auction_itemsDTO.officeUnitId,
                            limit, pi_auction_itemsDTO.lastModificationTime, PiStageEnum.IN_STOCK.getValue(), pi_auction_itemsDTO.itemId);
                }

            }


            pi_auction_itemsDTOS.add(pi_auction_itemsDTO);
        }

        if (addFlag) {
            EmployeeSearchModel model = (gson.fromJson
                    (EmployeeSearchModalUtil.getEmployeeSearchModelJson
                                    (userDTO.employee_record_id,
                                            userDTO.unitID,
                                            userDTO.organogramID),
                            EmployeeSearchModel.class)
            );


            pi_auctionDTO.requesterEmpId = model.employeeRecordId;
            pi_auctionDTO.requesterOfficeUnitId = model.officeUnitId;
            pi_auctionDTO.requesterOrgId = model.organogramId;

            pi_auctionDTO.requesterNameEn = model.employeeNameEn;
            pi_auctionDTO.requesterNameBn = model.employeeNameBn;
            pi_auctionDTO.requesterOfficeUnitNameEn = model.officeUnitNameEn;
            pi_auctionDTO.requesterOfficeUnitNameBn = model.officeUnitNameBn;
            pi_auctionDTO.requesterOfficeUnitOrgNameEn = model.organogramNameEn;
            pi_auctionDTO.requesterOfficeUnitOrgNameBn = model.organogramNameBn;

            pi_auctionDTO.requesterPhoneNum = model.phoneNumber;

            OfficesDTO requesterOffice = OfficesRepository.getInstance().getOfficesDTOByID(pi_auctionDTO.requesterOfficeId);

            if (requesterOffice != null) {
                pi_auctionDTO.requesterOfficeId = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(model.officeUnitId).officeId;

                pi_auctionDTO.requesterOfficeNameEn = requesterOffice.officeNameEng;
                pi_auctionDTO.requesterOfficeNameBn = requesterOffice.officeNameBng;
            }
        }

        if (addFlag) {
            pi_auctionDTO.auctionStatus = CommonApprovalStatus.PENDING.getValue();
            pi_auctionDTO.officeUnitId = officeUnitId;
        }

        pi_auctionDTO.itemCount = totalSold;
        pi_auctionDTO.totalBuyingPrice = totalUnitBuyingPrice;
        pi_auctionDTO.totalSellingPrice = totalUnitSellingPrice;

        logger.debug("Done adding  addPi_auction dto = " + pi_auctionDTO);
        long returnedID = -1;


        if (addFlag == true) {
            returnedID = Pi_auctionDAO.getInstance().add(pi_auctionDTO);
        } else {
            returnedID = Pi_auctionDAO.getInstance().update(pi_auctionDTO);
        }

        for (Pi_auction_itemsDTO pi_auction_itemsDTO : pi_auction_itemsDTOS) {
            if (addFlag) {
                pi_auction_itemsDTO.piAuctionId = returnedID;
                Pi_auction_itemsDAO.getInstance().add(pi_auction_itemsDTO);
            } else {
                Pi_auction_itemsDAO.getInstance().update(pi_auction_itemsDTO);
            }
        }


        return pi_auctionDTO;


    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }

        try {
            String actionType = request.getParameter("actionType");
            switch (actionType) {
                case "createNewList":
                    if (Utils.checkPermission(String.valueOf(userDTO.employee_record_id), userDTO, MenuConstants.PI_AUCTION_ADD)) {
                        request.getRequestDispatcher("pi_auction/pi_auctionCreateNewListEdit.jsp").forward(request, response);
                    }
                    return;
                case "search":
                    if (Utils.checkPermission(String.valueOf(userDTO.employee_record_id), userDTO, MenuConstants.PI_AUCTION_SEARCH)) {
                        request.setAttribute("menuIDPath", new ArrayList<>(Arrays.asList(INVENTORY_AND_PURCHASE_MANAGEMENT,
                                PI_AUCTION_MANAGEMENT,
                                PI_AUCTION)));
                        RoleDTO role = PermissionRepository.getRoleDTOByRoleID(userDTO.roleID);
                        boolean isAdmin = role.ID == SessionConstants.INVENTORY_ADMIN_ROLE || role.ID == SessionConstants.ADMIN_ROLE;
                        Map<String, String> extraCriteriaMap = new HashMap<>();
                        if (isAdmin) {
                            extraCriteriaMap.put("auctionStatus", String.valueOf(CommonApprovalStatus.PENDING.getValue()));
                            request.setAttribute(CommonDAOService.CRITERIA_MAP_KEY, extraCriteriaMap);
                            search(request, response);
                            return;
                        } else {
                            extraCriteriaMap.put("auctionStatus", String.valueOf(CommonApprovalStatus.PENDING.getValue()));
                            extraCriteriaMap.put("officeUnitId", String.valueOf(userDTO.unitID));
                            request.setAttribute(CommonDAOService.CRITERIA_MAP_KEY, extraCriteriaMap);
                            search(request, response);
                            return;
                        }
                    }
                    break;
                default:
                    super.doGet(request, response);
                    return;
            }
        } catch (Exception ex) {
            logger.error(ex);
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);


    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }

        try {
            String actionType = request.getParameter("actionType");
            switch (actionType) {

                case "rejectApplicationSearchRow":
                    if (Utils.checkPermission(String.valueOf(userDTO.employee_record_id), userDTO, MenuConstants.PI_AUCTION_SEARCH)) {
                        try {
                            long piAuctionId = Long.parseLong(request.getParameter("piAuctionId"));
                            long officeUnitId = Long.parseLong(request.getParameter("officeUnitId"));
                            Pi_auctionDTO pi_auctionDTO = Pi_auctionDAO.getInstance().getDTOByID(piAuctionId);
                            List<Pi_auction_itemsDTO> pi_auction_itemsDTOs = Pi_auction_itemsDAO.getInstance().getPiAuctionItemsByAuctionId(piAuctionId, officeUnitId);
                            for (Pi_auction_itemsDTO pi_auction_itemsDTO : pi_auction_itemsDTOs) {
                                PiUniqueItemAssignmentDAO.getInstance().UpdateStageAfterAuction(PiStageEnum.IN_STOCK.getValue(),
                                        pi_auction_itemsDTO.piPurchaseItemsId, pi_auction_itemsDTO.officeUnitId,
                                        pi_auction_itemsDTO.sellingUnit, -1, PiStageEnum.AUCTIONED.getValue(), pi_auction_itemsDTO.itemId);
                            }

                            Pi_auctionDAO.getInstance().UpdateAuctionStatus(CommonApprovalStatus.CANCELLED_AUCTION.getValue(), pi_auctionDTO.iD,
                                    userDTO);

                            ApiResponse.sendSuccessResponse(response, "Pi_auctionServlet?actionType=search");
                        } catch (Exception ex) {
                            logger.error(ex);
                            ApiResponse.sendErrorResponse(response, ex.getMessage());
                        }
                        return;
                    }
                    break;
                default:
                    super.doPost(request, response);
                    return;


            }
        } catch (Exception ex) {
            logger.error(ex);
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }
}

