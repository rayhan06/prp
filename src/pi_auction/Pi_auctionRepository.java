package pi_auction;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import com.google.gson.Gson;

import repository.Repository;
import repository.RepositoryManager;


public class Pi_auctionRepository implements Repository {
	Pi_auctionDAO pi_auctionDAO = null;
	
	static Logger logger = Logger.getLogger(Pi_auctionRepository.class);
	Map<Long, Pi_auctionDTO>mapOfPi_auctionDTOToiD;
	Gson gson;

  
	private Pi_auctionRepository(){
		pi_auctionDAO = Pi_auctionDAO.getInstance();
		mapOfPi_auctionDTOToiD = new ConcurrentHashMap<>();
		gson = new Gson();
		RepositoryManager.getInstance().addRepository(this);
	}

	private static class LazyLoader{
        final static Pi_auctionRepository INSTANCE = new Pi_auctionRepository();
    }

    public static Pi_auctionRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

	public void reload(boolean reloadAll){
		try {
			List<Pi_auctionDTO> pi_auctionDTOs = pi_auctionDAO.getAllDTOs(reloadAll);
			for(Pi_auctionDTO pi_auctionDTO : pi_auctionDTOs) {
				Pi_auctionDTO oldPi_auctionDTO = getPi_auctionDTOByiD(pi_auctionDTO.iD);
				if( oldPi_auctionDTO != null ) {
					mapOfPi_auctionDTOToiD.remove(oldPi_auctionDTO.iD);
				
					
				}
				if(pi_auctionDTO.isDeleted == 0) 
				{
					
					mapOfPi_auctionDTOToiD.put(pi_auctionDTO.iD, pi_auctionDTO);
				
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public Pi_auctionDTO clone(Pi_auctionDTO dto) {
		String raw = gson.toJson(dto);
		return gson.fromJson(raw, Pi_auctionDTO.class);
	}
	
	
	public List<Pi_auctionDTO> getPi_auctionList() {
		List <Pi_auctionDTO> pi_auctions = new ArrayList<Pi_auctionDTO>(this.mapOfPi_auctionDTOToiD.values());
		return pi_auctions;
	}
	
	public List<Pi_auctionDTO> copyPi_auctionList() {
		List <Pi_auctionDTO> pi_auctions = getPi_auctionList();
		return pi_auctions
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}
	
	
	
	public Pi_auctionDTO getPi_auctionDTOByiD( long iD){
		return mapOfPi_auctionDTOToiD.get(iD);
	}
	
	public Pi_auctionDTO copyPi_auctionDTOByiD( long iD){
		return clone(mapOfPi_auctionDTOToiD.get(iD));
	}

	
	@Override
	public String getTableName() {
		return pi_auctionDAO.getTableName();
	}
}


