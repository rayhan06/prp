package roomwise_ticket_count_report;


import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import language.LC;
import language.LM;

import login.LoginDTO;
import permission.MenuConstants;

import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;

import util.ReportRequestHandler;
import pbReport.*;






@WebServlet("/Roomwise_ticket_count_report_Servlet")
public class Roomwise_ticket_count_report_Servlet  extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	String[][] Criteria =
	{
		{"criteria","","room_no_cat","!=","","int","","","-1","none",  ""},		
		{"criteria","","room_no_cat","=","AND","int","","","any","roomNoCat_1", LC.ROOMWISE_TICKET_COUNT_REPORT_WHERE_ROOMNOCAT_1 + ""},		
		{"criteria","","insertion_date",">=","AND","long","","",Long.MIN_VALUE + "","startDate", LC.HM_START_DATE + ""},		
		{"criteria","","insertion_date","<=","AND","long","","",Long.MAX_VALUE + "","endDate", LC.HM_END_DATE + ""}		
	};
	
	String[][] Display =
	{
		{"display","","room_no_cat","cat",""},		
		{"display","","ticket_issues_type","type",""},		
		{"display","","COUNT(id)","text",""}		
	};
	
	String GroupBy = "room_no_cat, ticket_issues_type";
	String OrderBY = "";
	
	ReportRequestHandler reportRequestHandler;
	
	public Roomwise_ticket_count_report_Servlet(){

	}
	
	private final ReportService reportService = new ReportService();
	
	private String sql;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String language = "english";
		if(userDTO!= null && userDTO.languageID == SessionConstants.BANGLA)
		{
			language = "bangla";
		}
		boolean isLangEng = language.equalsIgnoreCase("english");
				
		String actionType = request.getParameter("actionType");

		System.out.println("In ssservlet doget, actiontype = " + actionType);
		
		sql = "support_ticket";

		Display[0][4] = isLangEng ? "Room" : "কক্ষ";
        Display[1][4] = isLangEng ? "Ticket Type" : "টিকেটের ধরণ";
		Display[2][4] = LM.getText(LC.HM_COUNT, loginDTO);

		
		String reportName = LM.getText(LC.ROOMWISE_TICKET_COUNT_REPORT_OTHER_ROOMWISE_TICKET_COUNT_REPORT, loginDTO);
		
		reportRequestHandler = new ReportRequestHandler(null,
				Criteria, Display, GroupBy, OrderBY, sql,
				reportService);
		
		
		reportRequestHandler = new ReportRequestHandler(null,
				Criteria, Display, GroupBy, OrderBY, sql,
				reportService);
		
		reportRequestHandler.clientSideDataFormatting = new ArrayList<Pair<Integer, Integer>>();
		reportRequestHandler.clientSideDataFormatting.add(new ImmutablePair<Integer, Integer>(1, ReportRequestHandler.RIGHT_ALIGN_INT));
		reportRequestHandler.clientSideDataFormatting.add(new ImmutablePair<Integer, Integer>(2, ReportRequestHandler.RIGHT_ALIGN_INT));


		
		reportRequestHandler.handleReportGet(request, response, userDTO, "roomwise_ticket_count_report",
				MenuConstants.ROOMWISE_TICKET_COUNT_REPORT_DETAILS, language, reportName, "roomwise_ticket_count_report");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		doGet(request, response);
	}
}
