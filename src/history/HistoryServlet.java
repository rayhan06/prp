/*
package history;

import login.LoginDTO;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/HistoryServlet")
@MultipartConfig
public class HistoryServlet extends HttpServlet {

    public HistoryServlet() {
        super();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("In doget request = " + request);
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getInstance().getUserDtoByUserId(loginDTO.userID);
        String actionType = request.getParameter("actionType");

        if (actionType.equals("getUnitNameChangesHistory")) {
            response.setContentType("application/json");

            String unit_id = request.getParameter("unit_id");
            PrintWriter out = response.getWriter();
            out.print(new UnitNameChangesHistory().getHistoryByUnitId(Integer.parseInt(unit_id)));
            out.flush();
        }
    }
}
*/
