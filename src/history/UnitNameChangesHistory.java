package history;

import test_lib.RInsertQueryBuilder;
import test_lib.RQueryBuilder;

public class UnitNameChangesHistory {
    public UnitNameChangesHistory() {
    }

    public boolean saveHistory(long id, String name_bng, String name_eng) {
        OfficeUnitModel a = getModelById(id);
        if (!name_bng.equals(a.getUnit_name_bng()) || !name_eng.equals(a.getUnit_name_eng())) {
            unit_name_change_histories s = new unit_name_change_histories();
            s.setUnit_id(id);
            s.setName_bng(name_bng);
            s.setName_eng(name_eng);
            s.setIsDeleted(false);
            s.setLastModificationTime(System.currentTimeMillis());

            RInsertQueryBuilder<unit_name_change_histories> insertQueryBuilder = new RInsertQueryBuilder<>();
            boolean data = insertQueryBuilder.of(unit_name_change_histories.class).model(s).buildInsert();
            return data;
        }
        return false;
    }

    private OfficeUnitModel getModelById(long id) {
        String sql = String.format("select id, unit_name_bng, unit_name_eng from office_units where id = %d", id);
        RQueryBuilder<OfficeUnitModel> rQueryBuilder = new RQueryBuilder<>();
        OfficeUnitModel data = rQueryBuilder.setSql(sql).of(OfficeUnitModel.class).buildRaw().get(0);

        return data;
    }

    public String getHistoryByUnitId(int unit_id) {
        String sql = String.format("select * from unit_name_change_histories where unit_id = %d", unit_id);
        RQueryBuilder<unit_name_change_histories> rQueryBuilder = new RQueryBuilder<>();
        String data = rQueryBuilder.setSql(sql).of(unit_name_change_histories.class).buildJson();

        return data;
    }

    public static class OfficeUnitModel {
        private int id;
        private String unit_name_bng;
        private String unit_name_eng;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getUnit_name_bng() {
            return unit_name_bng;
        }

        public void setUnit_name_bng(String unit_name_bng) {
            this.unit_name_bng = unit_name_bng;
        }

        public String getUnit_name_eng() {
            return unit_name_eng;
        }

        public void setUnit_name_eng(String unit_name_eng) {
            this.unit_name_eng = unit_name_eng;
        }
    }

    public static class unit_name_change_histories {
        private long unit_id;
        private String name_eng;
        private String name_bng;
        private long changes_date;
        private long changes_by;
        private long lastModificationTime;
        private boolean isDeleted;

        public long getUnit_id() {
            return unit_id;
        }

        public void setUnit_id(long unit_id) {
            this.unit_id = unit_id;
        }

        public String getName_eng() {
            return name_eng;
        }

        public void setName_eng(String name_eng) {
            this.name_eng = name_eng;
        }

        public String getName_bng() {
            return name_bng;
        }

        public void setName_bng(String name_bng) {
            this.name_bng = name_bng;
        }

        public long getChanges_date() {
            return changes_date;
        }

        public void setChanges_date(long changes_date) {
            this.changes_date = changes_date;
        }

        public long getChanges_by() {
            return changes_by;
        }

        public void setChanges_by(long changes_by) {
            this.changes_by = changes_by;
        }

        public long getLastModificationTime() {
            return lastModificationTime;
        }

        public void setLastModificationTime(long lastModificationTime) {
            this.lastModificationTime = lastModificationTime;
        }

        public boolean getIsDeleted() {
            return isDeleted;
        }

        public void setIsDeleted(boolean deleted) {
            isDeleted = deleted;
        }
    }
}
