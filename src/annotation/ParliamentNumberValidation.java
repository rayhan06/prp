package annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/*
 * @author Md. Erfan Hossain
 * @created 22/09/2021 - 5:48 AM
 * @project parliament
 */

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface ParliamentNumberValidation {
    String engError() default "Please select parliament number";
    String bngError() default "সংসদ নম্বর বাছাই করুন";
    String properEngError() default "Please select correct parliament number";
    String properBngError() default "সঠিক সংসদ নম্বর বাছাই করুন";
    boolean isMandatory() default true;
    String name() default "";
    long value() default 0;
    boolean isRunningParliamentMandatory() default true;
}