package annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/*
 * @author Md. Erfan Hossain
 * @created 21/09/2021 - 12:53 PM
 * @project parliament
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface CategoryValidation {
    String domainName();
    String engError() default "";
    String bngError() default "";
    String properEngError() default "";
    String properBngError() default "";
    String name() default "";
    boolean mandatory() default true;
    int value() default 0;
}
