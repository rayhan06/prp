package annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/*
 * @author Md. Erfan Hossain
 * @created 21/09/2021 - 12:33 PM
 * @project parliament
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface ValidMobileNumber {
    String engError() default "Please enter valid mobile number";
    String bngError() default "অনুগ্রহ করে সঠিক মোবাইল নাম্বার লিখুন";
    boolean isMandatory() default true;
    String name() default "";
}
