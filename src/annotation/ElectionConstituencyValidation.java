package annotation;

/*
 * @author Md. Erfan Hossain
 * @created 22/09/2021 - 5:51 AM
 * @project parliament
 */

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface ElectionConstituencyValidation {
    String engError() default "Please select constituency";
    String bngError() default "নির্বাচনী এলাকা বাছাই করুন";
    String properEngError() default "Please select correct constituency";
    String properBngError() default "সঠিক নির্বাচনী এলাকা বাছাই করুন";
    boolean isMandatory() default true;
    String name() default "";
    long value() default 0;
}