package annotation;

import sessionmanager.SessionConstants;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/*
 * @author Md. Erfan Hossain
 * @created 21/09/2021 - 12:49 PM
 * @project parliament
 */

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface DateValidation {
    String engError() default "";
    String bngError() default "";
    boolean isEmptyValid() default false;
    String name() default "";
    long value() default SessionConstants.MIN_DATE;
    boolean mandatory() default true;
    String pattern() default "dd/MM/yyyy";
}