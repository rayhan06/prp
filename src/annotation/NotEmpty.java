package annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/*
 * @author Md. Erfan Hossain
 * @created 21/09/2021 - 12:29 PM
 * @project parliament
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface NotEmpty {
    String engError() default "";
    String bngError() default "";
    int minLength() default 1;
    int maxLength() default Integer.MAX_VALUE;
    String name() default "";
}
