package annotation;

/*
 * @author Md. Erfan Hossain
 * @created 22/09/2021 - 5:52 AM
 * @project parliament
 */

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface PoliticalPartyAnnotation {
    String engError() default "Please select political party";
    String bngError() default "অনুগ্রহ করে রাজনৈতিক দল বাছাই করুন";
    String properEngError() default "Please select correct political party";
    String properBngError() default "অনুগ্রহ করে সঠিক রাজনৈতিক দল বাছাই করুন";
    boolean isMandatory() default true;
    String name() default "";
    long value() default 0;
}