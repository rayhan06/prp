package annotation;

/*
 * @author Md. Erfan Hossain
 * @created 21/09/2021 - 7:43 PM
 * @project parliament
 */

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface EmailValidation {
    String engError() default "Please enter email address";
    String bngError() default "অনুগ্রহ করে ইমেইল লিখুন";
    String properEngError() default "Please enter valid email address";
    String properBngError() default "অনুগ্রহ করে বৈধ ইমেইল লিখুন";
    boolean isMandatory() default true;
    String name() default "";
}