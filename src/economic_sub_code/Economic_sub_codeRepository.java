package economic_sub_code;

import com.google.gson.Gson;
import org.apache.log4j.Logger;
import pb.OptionDTO;
import pb.Utils;
import repository.Repository;
import repository.RepositoryManager;
import util.LockManager;
import util.StringUtils;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public class Economic_sub_codeRepository implements Repository {
    private static final Logger logger = Logger.getLogger(Economic_sub_codeRepository.class);

    private final Economic_sub_codeDAO dao;
    private final Map<Long, Economic_sub_codeDTO> mapById;
    private final Map<Long, List<Economic_sub_codeDTO>> mapByEconomicCodeId;

    private Economic_sub_codeRepository() {
        dao = Economic_sub_codeDAO.getInstance();
        mapById = new ConcurrentHashMap<>();
        mapByEconomicCodeId = new ConcurrentHashMap<>();
        RepositoryManager.getInstance().addRepository(this);
    }

    private static class LazyLoader {
        static final Economic_sub_codeRepository INSTANCE = new Economic_sub_codeRepository();
    }

    public static Economic_sub_codeRepository getInstance() {
        return Economic_sub_codeRepository.LazyLoader.INSTANCE;
    }

    @Override
    public void reload(boolean reloadAll) {
        logger.debug("Economic_sub_codeRepository reload start for, reloadAll : " + reloadAll);
        List<Economic_sub_codeDTO> dtoList = dao.getAllEconomic_sub_code(reloadAll);
        if (dtoList != null && dtoList.size() > 0) {
            dtoList.stream()
                   .peek(this::removeIfPresent)
                   .filter(dto -> dto.isDeleted == 0)
                   .forEach(dto -> {
                       mapById.put(dto.iD, dto);
                       putInEconomicCodeMap(dto);
                   });
            mapByEconomicCodeId.values().forEach(ls -> ls.sort(Comparator.comparing(o -> o.code)));
        }
        logger.debug("Economic_sub_codeRepository reload end for, reloadAll : " + reloadAll);
    }

    private void putInEconomicCodeMap(Economic_sub_codeDTO dto) {
        List<Economic_sub_codeDTO> dtoListByEconomic = mapByEconomicCodeId.getOrDefault(
                dto.economicCodeId, new ArrayList<>()
        );
        dtoListByEconomic.add(dto);
        mapByEconomicCodeId.put(dto.economicCodeId, dtoListByEconomic);
    }

    private void removeIfPresent(Economic_sub_codeDTO dto) {
        if (dto == null) return;
        if (mapById.get(dto.iD) != null) {
            mapById.remove(dto.iD);
        }
        List<Economic_sub_codeDTO> dtoList = mapByEconomicCodeId.get(dto.economicCodeId);
        if (dtoList != null) {
            dtoList.removeIf(d -> d.iD == dto.iD);
        }
    }

    public List<Economic_sub_codeDTO> getAllDTOs() {
        return new ArrayList<>(mapById.values());
    }

    public Economic_sub_codeDTO getDTOByID(long id) {
        if (mapById.get(id) == null) {
            synchronized (LockManager.getLock(id + "ESCI")) {
                if (mapById.get(id) == null) {
                    Economic_sub_codeDTO dto = dao.getDTOByID(id);
                    if (dto != null) {
                        mapById.put(id, dto);
                        putInEconomicCodeMap(dto);
                    }
                }
            }
        }
        return mapById.get(id);
    }

    public String getCode(long id, String language) {
        Economic_sub_codeDTO dto = getDTOByID(id);
        if (dto == null) return "";
        return StringUtils.convertBanglaIfLanguageIsBangla(language, dto.code);
    }

    public List<Economic_sub_codeDTO> getDTOsByEconomicCodeId(long economicCodeId) {
        if (mapByEconomicCodeId.get(economicCodeId) == null) {
            synchronized (LockManager.getLock(economicCodeId + "ESCECI")) {
                if (mapByEconomicCodeId.get(economicCodeId) == null) {
                    List<Economic_sub_codeDTO> dtoList = dao.getDTOsByEconomicCodeId(economicCodeId);
                    if (dtoList != null && dtoList.size() > 0) {
                        mapByEconomicCodeId.put(economicCodeId, dtoList);
                    }
                }
            }
        }
        return mapByEconomicCodeId.get(economicCodeId);
    }

    public String getModelsJSON(long economicCodeId, String language) {
        List<EconomicSubCodeModel> modelList = getDTOsByEconomicCodeId(economicCodeId)
                .stream()
                .map(dto -> new EconomicSubCodeModel(dto, language))
                .collect(Collectors.toList());
        return new Gson().toJson(modelList);
    }

    public String buildOption(String language, long economicCodeId, Long selectedId) {
        return buildOptionFromDTOs(getDTOsByEconomicCodeId(economicCodeId), language, selectedId);
    }

    public String buildOptionWithAll(String language, Long selectedId) {
        return buildOptionFromDTOs(mapById.values(), language, selectedId);
    }

    public String buildOptionFromDTOs(Collection<Economic_sub_codeDTO> dtoList, String language, Long selectedId) {
        List<OptionDTO> optionDTOList = dtoList.stream()
                                               .sorted(Comparator.comparing(economic_sub_codeDTO -> economic_sub_codeDTO.code))
                                               .map(Economic_sub_codeDTO::getOptionDTO)
                                               .collect(Collectors.toList());
        return Utils.buildOptions(optionDTOList, language, selectedId == null ? null : String.valueOf(selectedId));
    }

    @Override
    public String getTableName() {
        return "economic_sub_code";
    }

    public String getText(String language, long id) {
        Economic_sub_codeDTO subCodeDTO = getDTOByID(id);
        if (subCodeDTO == null) return "";
        OptionDTO optionDTO = subCodeDTO.getOptionDTO();
        return "english".equalsIgnoreCase(language) ? optionDTO.englishText : optionDTO.banglaText;
    }
}