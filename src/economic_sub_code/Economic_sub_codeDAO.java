package economic_sub_code;

import common.CommonDAOService;
import common.ConnectionAndStatementUtil;
import org.apache.log4j.Logger;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings({"unused"})
public class Economic_sub_codeDAO implements CommonDAOService<Economic_sub_codeDTO> {
    private static final Logger logger = Logger.getLogger(Economic_sub_codeDAO.class);

    private static final String addQuery = "INSERT INTO {tableName} (economic_code_id, code, description_en,description_bn,"
            .concat("modified_by,lastModificationTime,inserted_by,insertion_time,isDeleted,ID) ")
            .concat("VALUES (?,?,?,?,?,?,?,?,?,?)");

    private static final String updateQuery = "UPDATE {tableName} SET economic_code_id = ?, code = ?, description_en = ?, "
            .concat("description_bn = ?, modified_by = ?, lastModificationTime = ? WHERE ID = ?");

    private static final String getByEconomicCodeId = "SELECT * FROM economic_sub_code WHERE economic_code_id = %d "
            .concat("AND isDeleted = 0 ORDER BY lastModificationTime;");

    private static final String getGetByEconomicCodeIdAndCodeId = "SELECT * FROM economic_sub_code WHERE economic_code_id = ? and code = ?";

    private final Map<String, String> searchMap = new HashMap<>();

    private static Economic_sub_codeDAO INSTANCE = null;

    public static Economic_sub_codeDAO getInstance() {
        if (INSTANCE == null) {
            synchronized (Economic_sub_codeDAO.class) {
                if (INSTANCE == null) {
                    INSTANCE = new Economic_sub_codeDAO();
                }
            }
        }
        return INSTANCE;
    }

    private Economic_sub_codeDAO() {

        searchMap.put("economic_code_id", " and (economic_code_id = ?)");
        searchMap.put("code", " and (code like ?)");
        searchMap.put("description_en", " and (description_en like ?)");
        searchMap.put("description_bn", " and (description_bn like ?)");
    }

    public void setSearchColumn(Economic_sub_codeDTO economic_sub_codeDTO) {
        economic_sub_codeDTO.searchColumn = "";
        economic_sub_codeDTO.searchColumn += economic_sub_codeDTO.economicCodeId + " ";
        economic_sub_codeDTO.searchColumn += economic_sub_codeDTO.code + " ";
        economic_sub_codeDTO.searchColumn += economic_sub_codeDTO.descriptionEn + " ";
        economic_sub_codeDTO.searchColumn += economic_sub_codeDTO.descriptionBn + " ";
    }

    @Override
    public void set(PreparedStatement ps, Economic_sub_codeDTO economic_sub_codeDTO, boolean isInsert) throws SQLException {
        int index = 0;
        ps.setLong(++index, economic_sub_codeDTO.economicCodeId);
        ps.setString(++index, economic_sub_codeDTO.code);
        ps.setString(++index, economic_sub_codeDTO.descriptionEn);
        ps.setString(++index, economic_sub_codeDTO.descriptionBn);
        ps.setLong(++index, economic_sub_codeDTO.modifiedBy);
        ps.setLong(++index, economic_sub_codeDTO.lastModificationTime);
        if (isInsert) {
            ps.setLong(++index, economic_sub_codeDTO.insertedBy);
            ps.setLong(++index, economic_sub_codeDTO.insertionTime);
            ps.setInt(++index, 0 /* isDeleted */);
        }
        ps.setLong(++index, economic_sub_codeDTO.iD);
    }

    @Override
    public Economic_sub_codeDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Economic_sub_codeDTO economic_sub_codeDTO = new Economic_sub_codeDTO();

            economic_sub_codeDTO.iD = rs.getLong("ID");
            economic_sub_codeDTO.economicCodeId = rs.getLong("economic_code_id");
            economic_sub_codeDTO.code = rs.getString("code");
            economic_sub_codeDTO.descriptionEn = rs.getString("description_en");
            economic_sub_codeDTO.descriptionBn = rs.getString("description_bn");
            economic_sub_codeDTO.isDeleted = rs.getInt("isDeleted");
            economic_sub_codeDTO.insertedBy = rs.getLong("inserted_by");
            economic_sub_codeDTO.insertionTime = rs.getLong("insertion_time");
            economic_sub_codeDTO.modifiedBy = rs.getLong("modified_by");
            economic_sub_codeDTO.lastModificationTime = rs.getLong("lastModificationTime");

            return economic_sub_codeDTO;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    @Override
    public String getTableName() {
        return "economic_sub_code";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    @Override
    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Economic_sub_codeDTO) commonDTO, addQuery, true);
    }

    @Override
    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Economic_sub_codeDTO) commonDTO, updateQuery, false);
    }

    public List<Economic_sub_codeDTO> getAllEconomic_sub_code(boolean isFirstReload) {
        return getAllDTOs(isFirstReload);
    }

    public Economic_sub_codeDTO getDTOByID(long ID) {
        return getDTOFromID(ID);
    }

    public List<Economic_sub_codeDTO> getDTOsByEconomicCodeId(long economicCodeId) {
        return getDTOs(String.format(getByEconomicCodeId, economicCodeId));
    }

    public Economic_sub_codeDTO getGetByEconomicCodeIdAndCodeId(long economicCodeId, String code) {
        return ConnectionAndStatementUtil.getT(getGetByEconomicCodeIdAndCodeId, Arrays.asList(economicCodeId, code), this::buildObjectFromResultSet);
    }
}
	