package economic_sub_code;

import pb.OptionDTO;
import sessionmanager.SessionConstants;
import util.CommonDTO;
import util.StringUtils;

public class Economic_sub_codeDTO extends CommonDTO {

    public long economicCodeId = 0;
    public String code = "";
    public String descriptionEn = "";
    public String descriptionBn = "";
    public long insertedBy = 0;
    public long insertionTime = SessionConstants.MIN_DATE;
    public long modifiedBy = 0;

    public OptionDTO getOptionDTO() {
        String textEn = code.concat(" - ").concat(descriptionEn);
        String textBn = StringUtils.convertToBanNumber(code).concat(" - ").concat(descriptionBn);
        return new OptionDTO(textEn, textBn, String.valueOf(iD));
    }

    @Override
    public String toString() {
        return "$Economic_sub_codeDTO[" +
                " iD = " + iD +
                " economicCodeId = " + economicCodeId +
                " code = " + code +
                " descriptionEn = " + descriptionEn +
                " descriptionBn = " + descriptionBn +
                " isDeleted = " + isDeleted +
                " insertedBy = " + insertedBy +
                " insertionTime = " + insertionTime +
                " modifiedBy = " + modifiedBy +
                " lastModificationTime = " + lastModificationTime +
                "]";
    }
}