CREATE TABLE economic_sub_code
(
    ID                   BIGINT(20) PRIMARY KEY,
    economic_code_id     BIGINT(20)   DEFAULT 0,
    code                 VARCHAR(30) DEFAULT NULL,
    description_en       VARCHAR(255) DEFAULT NULL,
    description_bn       VARCHAR(255) DEFAULT NULL,
    isDeleted            INT(11)      DEFAULT 0,
    inserted_by          BIGINT(20)   DEFAULT 0,
    insertion_time       BIGINT(20)   DEFAULT -62135791200000,
    modified_by          BIGINT(20)   DEFAULT 0,
    lastModificationTime BIGINT(20)   DEFAULT -62135791200000
) ENGINE = MyISAM
  DEFAULT CHARSET = utf8
  COLLATE = utf8_unicode_ci;