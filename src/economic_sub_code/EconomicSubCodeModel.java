package economic_sub_code;

import budget.BudgetDTO;
import common.NameDTO;
import economic_code.Economic_codeDTO;
import economic_code.Economic_codeRepository;
import economic_group.EconomicGroupRepository;
import util.StringUtils;

public class EconomicSubCodeModel {
    public String id = "";
    public String economicCodeId = "";
    public String economicGroupId = "";
    public String name = "";
    public String code = ""; // 7 digit economic code - converted for language
    // these code needed for js logic
    public String economicGroup = ""; // 2 digit economic code
    public String economicGroupName = "";
    public String economicCode = ""; // 4 digit economic code
    public String economicCodeName = "";
    public String economicSubCode = ""; // 7 digit economic code
    public String budgetId="";
    public EconomicSubCodeModel(){ }
    public EconomicSubCodeModel(Economic_sub_codeDTO subCodeDTO, String language){
        boolean isLanguageEnglish = "English".equalsIgnoreCase(language);
        id = String.valueOf(subCodeDTO.iD);
        name = isLanguageEnglish? subCodeDTO.descriptionEn : subCodeDTO.descriptionBn;
        code = isLanguageEnglish? subCodeDTO.code : StringUtils.convertToBanNumber(subCodeDTO.code);
        economicSubCode = subCodeDTO.code;

        Economic_codeDTO economicCodeDTO = Economic_codeRepository.getInstance().getById(subCodeDTO.economicCodeId);
        economicCodeId = String.valueOf(economicCodeDTO.iD);
        economicCode = economicCodeDTO.code;
        economicCodeName = isLanguageEnglish? economicCodeDTO.descriptionEn : economicCodeDTO.descriptionBn;

        NameDTO economicGroupDTO = EconomicGroupRepository.getInstance().getDTOByID(economicCodeDTO.economicGroupType);
        economicGroup = economicGroupId = String.valueOf(economicGroupDTO.iD);
        economicGroupName = isLanguageEnglish ? economicGroupDTO.nameEn : economicGroupDTO.nameBn;
    }
    public EconomicSubCodeModel(BudgetDTO budgetDTO, String language){
        Economic_sub_codeDTO subCodeDTO=Economic_sub_codeRepository.getInstance().getDTOByID(budgetDTO.economicSubCodeId);
        budgetId=String.valueOf(budgetDTO.iD);
        boolean isLanguageEnglish = "English".equalsIgnoreCase(language);
        id = String.valueOf(subCodeDTO.iD);
        name = isLanguageEnglish? subCodeDTO.descriptionEn : subCodeDTO.descriptionBn;
        code = isLanguageEnglish? subCodeDTO.code : StringUtils.convertToBanNumber(subCodeDTO.code);
        economicSubCode = subCodeDTO.code;

        Economic_codeDTO economicCodeDTO = Economic_codeRepository.getInstance().getById(subCodeDTO.economicCodeId);
        economicCodeId = String.valueOf(economicCodeDTO.iD);
        economicCode = economicCodeDTO.code;
        economicCodeName = isLanguageEnglish? economicCodeDTO.descriptionEn : economicCodeDTO.descriptionBn;

        NameDTO economicGroupDTO = EconomicGroupRepository.getInstance().getDTOByID(economicCodeDTO.economicGroupType);
        economicGroup = economicGroupId = String.valueOf(economicGroupDTO.iD);
        economicGroupName = isLanguageEnglish ? economicGroupDTO.nameEn : economicGroupDTO.nameBn;
    }

    @Override
    public String toString() {
        return "EconomicSubCodeModel{" +
                "id='" + id + '\'' +
                ", economicCodeId='" + economicCodeId + '\'' +
                ", economicGroupId='" + economicGroupId + '\'' +
                ", name='" + name + '\'' +
                ", code='" + code + '\'' +
                ", economicGroup='" + economicGroup + '\'' +
                ", economicGroupName='" + economicGroupName + '\'' +
                ", economicCode='" + economicCode + '\'' +
                ", economicCodeName='" + economicCodeName + '\'' +
                ", economicSubCode='" + economicSubCode + '\'' +
                '}';
    }
}
