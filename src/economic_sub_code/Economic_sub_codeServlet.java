package economic_sub_code;

import common.ApiResponse;
import common.BaseServlet;
import common.CommonDAOService;
import economic_code.Economic_codeDTO;
import economic_code.Economic_codeRepository;
import login.LoginDTO;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import permission.MenuConstants;
import role.PermissionRepository;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.CommonConstant;
import util.CommonDTO;
import util.StringUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/Economic_sub_codeServlet")
@MultipartConfig
public class Economic_sub_codeServlet extends BaseServlet {
    private static final long serialVersionUID = 1L;
    private static final Logger logger = Logger.getLogger(Economic_sub_codeServlet.class);
    private final Economic_sub_codeDAO economic_sub_codeDAO = Economic_sub_codeDAO.getInstance();


    @Override
    public String getTableName() {
        return "economic_sub_code";
    }

    @Override
    public String getServletName() {
        return "Economic_sub_codeServlet";
    }

    @Override
    public CommonDAOService<? extends CommonDTO> getCommonDAOService() {
        return economic_sub_codeDAO;
    }

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        boolean isLangEng = userDTO.languageID == CommonConstant.Language_ID_English;
        long economicCodeId = Long.parseLong(Jsoup.clean(request.getParameter("economicCodeId"), Whitelist.simpleText()));
        String code = Jsoup.clean(request.getParameter("code"), Whitelist.simpleText()).trim();
        Economic_sub_codeDTO economic_sub_codeDTO = economic_sub_codeDAO.getGetByEconomicCodeIdAndCodeId(economicCodeId, code);
        if (economic_sub_codeDTO != null) {
            Economic_codeDTO economicCodeDTO = Economic_codeRepository.getInstance().getById(economicCodeId);
            throw new Exception(isLangEng ? "Code " + code + " is already exist under " + economicCodeDTO.code + "-" + economicCodeDTO.descriptionEn + " economic code" :
                    "ইতিমধ্যে " + StringUtils.convertToBanNumber(economicCodeDTO.code) + "-" + economicCodeDTO.descriptionBn + " ইকোনোমিক কোডের অধীনে " + code + " কোড রয়েছে।");
        }
        economic_sub_codeDTO = new Economic_sub_codeDTO();
        economic_sub_codeDTO.economicCodeId = economicCodeId;
        economic_sub_codeDTO.code = code;
        economic_sub_codeDTO.insertedBy = economic_sub_codeDTO.modifiedBy = userDTO.ID;
        economic_sub_codeDTO.lastModificationTime = economic_sub_codeDTO.insertionTime = System.currentTimeMillis();
        economic_sub_codeDTO.descriptionEn = Jsoup.clean(request.getParameter("descriptionEn"), Whitelist.simpleText());
        economic_sub_codeDTO.descriptionBn = Jsoup.clean(request.getParameter("descriptionBn"), Whitelist.simpleText());
        economic_sub_codeDAO.add(economic_sub_codeDTO);
        return economic_sub_codeDTO;
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[0];
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[0];
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[0];
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Economic_sub_codeServlet.class;
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }

        try {
            String actionType = request.getParameter("actionType");
            if ("add".equals(actionType)) {
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ECONOMIC_SUB_CODE_ADD)) {
                    ApiResponse apiResponse;
                    try {
                        addT(request, true, userDTO);
                        apiResponse = ApiResponse.makeSuccessResponse("Economic_sub_codeServlet?actionType=search");
                    } catch (Exception ex) {
                        ex.printStackTrace();
                        apiResponse = ApiResponse.makeErrorResponse(ex.getMessage());
                    }
                    PrintWriter pw = response.getWriter();
                    pw.write(apiResponse.getJSONString());
                    pw.flush();
                    pw.close();
                    return;
                }
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            logger.debug(ex);
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }

}

