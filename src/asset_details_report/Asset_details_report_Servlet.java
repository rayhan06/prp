package asset_details_report;


import java.io.IOException;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import language.LC;
import language.LM;

import login.LoginDTO;
import permission.MenuConstants;

import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;

import util.ReportRequestHandler;
import pbReport.*;






@WebServlet("/Asset_details_report_Servlet")
public class Asset_details_report_Servlet  extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	String[][] Criteria =
	{
		{"criteria","asset_model","asset_category_type","=","","int","","","any","assetCategoryType", LC.ASSET_DETAILS_REPORT_WHERE_ASSETCATEGORYTYPE + ""},		
		{"criteria","","model","like","AND","String","","","%","model", LC.HM_MODEL + ""},		
		{"criteria","","assignment_status","=","AND","int","","","any","assignmentStatus", LC.HM_STATUS + ""},		
		{"criteria","asset_assignee","sl","like","AND","String","","","%","sl", LC.HM_SL + ""},		
		{"criteria","","assigned_organogram_id","=","AND","String","","","any","assignedOrganogramId", LC.HM_EMPLOYEE_ID + ""},				
		{"criteria","","assignment_date",">=","AND","long","","",Long.MIN_VALUE + "","startDate", LC.HM_START_DATE + ""},		
		{"criteria","","assignment_date","<=","AND","long","","",Long.MAX_VALUE + "","endDate", LC.HM_END_DATE + ""}		
	};
	
	String[][] Display =
	{
		{"display","asset_model","asset_category_type","type",""},		
		{"display","","model","basic",""},		
		{"display","asset_assignee","sl","text",""},		
		{"display","","assignment_status","assignment_status",""},		
		{"display","","assigned_organogram_id","organogram_to_name",""},			
		{"display","","assigned_organogram_id","organogram_to_office_type",""},		
		{"display","","assigned_organogram_id","organogram_to_wing",""},				
		{"display","","receiving_date","dateExceptZero",""},		
		{"display","","assignment_date","dateExceptZero",""}		
	};
	
	String GroupBy = "";
	String OrderBY = "";
	
	ReportRequestHandler reportRequestHandler;
	
	public Asset_details_report_Servlet(){

	}
	
	private final ReportService reportService = new ReportService();
	
	private String sql;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String language = "english";
		if(userDTO!= null && userDTO.languageID == SessionConstants.BANGLA)
		{
			language = "bangla";
		}
				
		String actionType = request.getParameter("actionType");

		System.out.println("In ssservlet doget, actiontype = " + actionType);
		
		sql = "asset_assignee JOIN asset_model ON asset_model.id = asset_assignee.asset_model_id";

		Display[0][4] = LM.getText(LC.ASSET_DETAILS_REPORT_SELECT_ASSETCATEGORYTYPE, loginDTO);
		Display[1][4] = LM.getText(LC.HM_MODEL, loginDTO);
		Display[2][4] = LM.getText(LC.HM_SL, loginDTO);
		Display[3][4] = LM.getText(LC.HM_STATUS, loginDTO);
		Display[4][4] = language.equalsIgnoreCase("english")?"Employee/Room":"কর্মকর্তা/কর্মচারী/কক্ষ";
		Display[5][4] = LM.getText(LC.HM_OFFICE_TYPE, loginDTO);
		Display[6][4] = LM.getText(LC.HM_WING, loginDTO);
		Display[7][4] = LM.getText(LC.ASSET_MODEL_ADD_RECEIVINGDATE, loginDTO);
		Display[8][4] = LM.getText(LC.HM_ASSIGNMENT_DATE, loginDTO);

		
		String reportName = LM.getText(LC.ASSET_DETAILS_REPORT_OTHER_ASSET_DETAILS_REPORT, loginDTO);
		
		reportRequestHandler = new ReportRequestHandler(null,
				Criteria, Display, GroupBy, OrderBY, sql,
				reportService);

		
		reportRequestHandler.handleReportGet(request, response, userDTO, "asset_details_report",
				MenuConstants.ASSET_DETAILS_REPORT_DETAILS, language, reportName, "asset_details_report");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		doGet(request, response);
	}
}
