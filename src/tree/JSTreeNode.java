package tree;

import java.util.HashMap;
import java.util.Map;

public class JSTreeNode {

    public String id;

    public String parent;

    public String text;

    public String icon;

    public JSTreeNodeState state;

    public Boolean children;

    public Map<String,String> li_attr = new HashMap<>();

    public Map<String,String> a_attr = new HashMap<>();
}
