package tree;

public enum JSTreePlugin {

    CHANGED( "changed" ),
    CHECKBOX( "checkbox" ),
    MASSLOAD( "massload" ),
    SEARCH( "search" ),
    STATE( "state" ),
    WHOLEROW( "wholerow" );

    private final String value;

    JSTreePlugin(String val ){

        this.value = val;
    }
}
