package tree;

public enum JSTreeConfig {

    ADD_PLUGIN,
    REMOVE_PLUGIN,
    TREE_ID,
    LANGUAGE,
    AJAX_SEARCH,
    SHOW_CHECKBOX_AT_LEAF_ONLY
}
