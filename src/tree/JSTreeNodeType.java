package tree;

public enum JSTreeNodeType {

    OFFICE_UNIT,
    ORGANOGRAM,
    EMPLOYEE
}
