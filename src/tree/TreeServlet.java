package tree;

import com.google.gson.Gson;
import employee_offices.EmployeeOfficeDTO;
import employee_offices.EmployeeOfficeRepository;
import employee_records.Employee_recordsDAO;
import employee_records.Employee_recordsDTO;
import employee_records.Employee_recordsRepository;
import office_unit_organograms.OfficeUnitOrganograms;
import office_unit_organograms.OfficeUnitOrganogramsRepository;
import office_units.Office_unitsDTO;
import office_units.Office_unitsRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

@WebServlet( "/treeServlet" )
public class TreeServlet extends HttpServlet {

    private static final Gson gson = new Gson();

    private final String OFFICE_UNIT_PREFIX = "OFFICE_UNIT_";
    private final String ORGANOGRAM_PREFIX = "ORG_";
    private final String EMPLOYEE_PREFIX = "EMP_";

    private final Employee_recordsDAO employeeRecordsDAO = new Employee_recordsDAO();

    protected void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {

        String actionType = request.getParameter( "actionType" );
        actionType = actionType == null?"":actionType;

        switch ( actionType ){

            case "getNodes":

                response.setContentType( "application/json" );

                String idStr = request.getParameter( "id" );
                //String id = idStr == null || idStr.equals( "#" )? "0": idStr;
                String nodeType = request.getParameter( "nodeType" ) == null? JSTreeNodeType.OFFICE_UNIT.toString(): request.getParameter( "nodeType" );
                String lang = request.getParameter( "lang" ) == null? "bn": request.getParameter( "lang" );
                Boolean showCheckBoxAtLeaf = request.getParameter("showCheckboxAtLeaf") != null && Boolean.parseBoolean(request.getParameter("showCheckboxAtLeaf"));

                List<JSTreeNode> jsTreeNodes = new ArrayList<>();

                switch ( nodeType ){

                    case "OFFICE_UNIT":
                        buildTreeForOfficeUnit( getIdBasedOnNodeType( idStr, JSTreeNodeType.OFFICE_UNIT ), lang, showCheckBoxAtLeaf, jsTreeNodes );
                        break;
                    case "ORGANOGRAM":
                        buildTreeForOrganogram( getIdBasedOnNodeType( idStr, JSTreeNodeType.ORGANOGRAM ), lang, showCheckBoxAtLeaf, jsTreeNodes );
                        break;
                }

                response.getWriter().write( gson.toJson( jsTreeNodes ) );
                break;
            case "search":
                Set<String> resultNodes = searchTree( request.getParameter( "str" ) );
                response.setContentType( "application/json" );
                response.getWriter().write( gson.toJson( resultNodes ) );
                break;
        }
    }

    private Set<String> searchTree( String searchText ) {

        Set<String> nodeIds = new HashSet<>();
        List<Long> empIds = employeeRecordsDAO.getIdsByName( searchText );

        for( Long id: empIds ){

            if( id != null ) {

                Employee_recordsDTO employee_recordsDTO = Employee_recordsRepository.getInstance().getById( id );

                if ( employee_recordsDTO != null ) {

                    List<EmployeeOfficeDTO> employee_officesDTOList = EmployeeOfficeRepository.getInstance().getByEmployeeRecordId( employee_recordsDTO.iD );

                    for( EmployeeOfficeDTO employeeOfficesDTO: employee_officesDTOList ) {

                        OfficeUnitOrganograms office_unit_organogram = OfficeUnitOrganogramsRepository.getInstance().getById(employeeOfficesDTO.officeUnitOrganogramId);

                        if( office_unit_organogram != null ){

                            nodeIds.add( ORGANOGRAM_PREFIX + office_unit_organogram.id );

                            addParentOfficeUnitsToNodeList( nodeIds, employeeOfficesDTO.officeUnitId );
                        }
                    }
                }
            }
        }

        return nodeIds;
    }

    private void addParentOfficeUnitsToNodeList( Set<String> nodeIds, Long officeUnitId ) {

        Office_unitsDTO office_unitsDTO = Office_unitsRepository.getInstance().getOffice_unitsDTOByID( officeUnitId );

        if( office_unitsDTO != null ){

            nodeIds.add( OFFICE_UNIT_PREFIX + office_unitsDTO.iD );
            addParentOfficeUnitsToNodeList( nodeIds, office_unitsDTO.parentUnitId );
        }
    }

    private Long getIdBasedOnNodeType(String idStr, JSTreeNodeType officeUnit) {

        if( idStr.equals( "#") )
            return 0L;

        if( officeUnit == JSTreeNodeType.OFFICE_UNIT && idStr.startsWith( OFFICE_UNIT_PREFIX ) ){
            return Long.parseLong( idStr.substring( OFFICE_UNIT_PREFIX.length() ) );
        }
        else if( officeUnit == JSTreeNodeType.ORGANOGRAM && idStr.startsWith( ORGANOGRAM_PREFIX ) ){

            return Long.parseLong( idStr.substring( ORGANOGRAM_PREFIX.length() ) );
        }

        return 0L;
    }

    private void buildTreeForOfficeUnit(Long id, String lang, Boolean showCheckBoxAtLeaf, List<JSTreeNode> jsTreeNodes) {

        List<Office_unitsDTO> office_unitsDTOList = Office_unitsRepository.getInstance().getChildList( id );
        if( office_unitsDTOList != null && office_unitsDTOList.size() > 0 ){

            buildJstreeNodeFromOfficeUnitDTO( office_unitsDTOList, jsTreeNodes, showCheckBoxAtLeaf, id, lang );
        }

        List<OfficeUnitOrganograms> officeUnitOrganogramsDTOS = OfficeUnitOrganogramsRepository.getInstance().getByOfficeUnitId( id );
        if( officeUnitOrganogramsDTOS != null ){

            buildJstreeNodeFromOfficeUnitOrganogramsDTO( officeUnitOrganogramsDTOS, jsTreeNodes, showCheckBoxAtLeaf, id, lang );
        }
    }

    private void buildJstreeNodeFromOfficeUnitDTO(List<Office_unitsDTO> office_unitsDTOList, List<JSTreeNode> jsTreeNodes, Boolean showCheckBoxAtLeaf, Long id, String lang){

        for( Office_unitsDTO office_unitsDTO: office_unitsDTOList ){
            jsTreeNodes.add( createNodeForOfficeUnit( office_unitsDTO, lang, id, showCheckBoxAtLeaf ) );
        }
    }

    private JSTreeNode createNodeForOfficeUnit( Office_unitsDTO office_unitsDTO, String lang, Long parentId, Boolean showCheckBoxAtLeaf ){

        JSTreeNode jsTreeNode = new JSTreeNode();

        jsTreeNode.text = lang.equals( "Bangla" )? "<b>" + office_unitsDTO.unitNameBng + "</b>" : "<b>" + office_unitsDTO.unitNameEng + "</b>";
        jsTreeNode.id = OFFICE_UNIT_PREFIX + office_unitsDTO.iD;
        jsTreeNode.parent = parentId == 0? "#": OFFICE_UNIT_PREFIX + parentId;
        jsTreeNode.children = true;
        jsTreeNode.icon = "flaticon-home-1 kt-font-info"; //"/parliament/assets/images/jstree_office_unit.png";

        Map<String,String> attr = new HashMap<>();
        attr.put( "nodeType", JSTreeNodeType.OFFICE_UNIT.toString() );
        if( showCheckBoxAtLeaf ){
            attr.put( "class", "no_checkbox" );
        }
        jsTreeNode.a_attr = attr;

        return jsTreeNode;
    }

    private void buildJstreeNodeFromOfficeUnitOrganogramsDTO(List<OfficeUnitOrganograms> officeUnitOrganogramsDTOS, List<JSTreeNode> jsTreeNodes, Boolean showCheckBoxAtLeaf, Long id, String lang) {

        for( OfficeUnitOrganograms office_unit_organogram: officeUnitOrganogramsDTOS ){
            jsTreeNodes.add( createNodeForOrganogram( office_unit_organogram, lang, id, showCheckBoxAtLeaf ) );
        }
    }

    private JSTreeNode createNodeForOrganogram( OfficeUnitOrganograms office_unit_organogram, String lang, Long parentId, Boolean showCheckBoxAtLeaf ){

        JSTreeNode jsTreeNode = new JSTreeNode();

        jsTreeNode.text = lang.equals( "Bangla" )? office_unit_organogram.designation_bng: office_unit_organogram.designation_eng;
        jsTreeNode.id = ORGANOGRAM_PREFIX + office_unit_organogram.id;
        jsTreeNode.parent = parentId == 0? "#": OFFICE_UNIT_PREFIX + parentId;
        jsTreeNode.children = true;
        jsTreeNode.icon = "flaticon2-group kt-font-warning";

        Map<String,String> attr = new HashMap<>();
        attr.put( "nodeType", JSTreeNodeType.ORGANOGRAM.toString() );
        if( showCheckBoxAtLeaf ){
            attr.put( "class", "no_checkbox" );
        }
        jsTreeNode.a_attr = attr;

        return jsTreeNode;
    }

    private void buildTreeForOrganogram( Long id, String lang, Boolean showCheckBoxAtLeaf, List<JSTreeNode> jsTreeNodes ) {

        EmployeeOfficeDTO employeeOfficeDTO = EmployeeOfficeRepository.getInstance().getByOfficeUnitOrganogramId( id );

        if( employeeOfficeDTO != null ) {

            Employee_recordsDTO employeeRecordDTO = Employee_recordsRepository.getInstance().getById( employeeOfficeDTO.employeeRecordId );

            JSTreeNode employeeNode = createNodeForEmployee(employeeRecordDTO, lang, id);
            if (employeeNode != null) {

                jsTreeNodes.add(employeeNode);
            }
        }
    }

    private JSTreeNode createNodeForEmployee( Employee_recordsDTO employeeRecordDTO, String lang, Long parentId ){

        JSTreeNode jsTreeNode = new JSTreeNode();

        if( employeeRecordDTO != null ) {
            jsTreeNode.text = lang.equals("Bangla") ? employeeRecordDTO.nameBng : employeeRecordDTO.nameEng;

            jsTreeNode.id = EMPLOYEE_PREFIX + employeeRecordDTO.iD;
            jsTreeNode.parent = parentId == 0 ? "#" : ORGANOGRAM_PREFIX + parentId;
            jsTreeNode.children = false;
            jsTreeNode.icon = "flaticon2-user-1  kt-font-success";

            Map<String, String> attr = new HashMap<>();
            attr.put("nodeType", JSTreeNodeType.EMPLOYEE.toString());
            jsTreeNode.a_attr = attr;

            return jsTreeNode;
        }
        return null;
    }
}
