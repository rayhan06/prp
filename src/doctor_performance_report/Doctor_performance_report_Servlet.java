package doctor_performance_report;


import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import language.LC;
import language.LM;


import login.LoginDTO;
import permission.MenuConstants;

import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.ReportRequestHandler;
import pbReport.*;


@WebServlet("/Doctor_performance_report_Servlet")
public class Doctor_performance_report_Servlet extends HttpServlet {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    ReportTemplate reportTemplate = new ReportTemplate();
    String[][] Criteria =
            {
                    {"criteria", "appointment", "dr_employee_record_id", "=",  "", "String", "", "","any", "dr_user_name", LC.HM_DOCTOR + "", "userNameToEmployeeRecordId"},
                    {"criteria", "appointment", "visit_date", ">=", "AND", "long", "", "", Long.MIN_VALUE + "", "startDate", LC.HM_START_DATE + ""},
                    {"criteria", "appointment", "visit_date", "<=", "AND", "long", "", "", Long.MAX_VALUE + "", "endDate", LC.HM_END_DATE + ""},
                    {"criteria", "appointment", "isCancelled", "=", "AND", "long", "", "",  "0", "",  ""},
                    {"criteria", "appointment", "available_time_slot", "!=", "AND", "long", "", "",  "0", "",  ""},
                    {"criteria", "appointment", "speciality_type", "!=", "AND", "long", "", "",  "3", "",  ""}
            };

    String[][] Display =
            {
            		{"display", "", "appointment.dr_employee_record_id", "erIdToName", ""},
            		{"display", "", "appointment.doctor_id", "dr_dept", ""},                   
                    {"display", "", "count(appointment.id)", "int", ""},
                    {"display", "", "SUM(CASE  WHEN prescription_id >= 0 THEN 1 ELSE 0  END)", "int", ""},	 
                    {"display", "", "appointment.visit_date", "date", ""},
                    {"display", "", "appointment.doctor_id", "invisible", ""}
            };

    String GroupBy = "appointment.dr_employee_record_id, appointment.visit_date";
    String OrderBY = "appointment.visit_date DESC";

    public Doctor_performance_report_Servlet() {

    }

    private final ReportService reportService = new ReportService();

    private String sql;
    ReportRequestHandler reportRequestHandler;


    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    	LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String language = "english";
		if(userDTO!= null && userDTO.languageID == SessionConstants.BANGLA)
		{
			language = "bangla";
		}
				
		String actionType = request.getParameter("actionType");

		System.out.println("In ssservlet doget, actiontype = " + actionType);

        sql = " appointment ";


        Display[0][4] = LM.getText(LC.DOCTOR_PERFORMANCE_REPORT_SELECT_DOCTOR, loginDTO);
        Display[1][4] = LM.getText(LC.HM_DEPARTMENT, loginDTO);
        Display[2][4] = LM.getText(LC.HM_APPOINTMENTS, loginDTO);
        Display[3][4] = LM.getText(LC.DOCTOR_PERFORMANCE_REPORT_SELECT_PRESCRITIONS, loginDTO);
        Display[4][4] = LM.getText(LC.DOCTOR_PERFORMANCE_REPORT_SELECT_VISITDATE, loginDTO);
        Display[5][4] = "";
        
        String reportName = LM.getText(LC.DOCTOR_PERFORMANCE_REPORT_OTHER_DOCTOR_PERFORMANCE_REPORT, loginDTO);
		
		reportRequestHandler = new ReportRequestHandler(null,
				Criteria, Display, GroupBy, OrderBY, sql,
				reportService);
		
		reportRequestHandler.clientSideDataFormatting = new ArrayList<Pair<Integer, Integer>>();
		reportRequestHandler.clientSideDataFormatting.add(new ImmutablePair<Integer, Integer>(2, ReportRequestHandler.RIGHT_ALIGN_INT));
		reportRequestHandler.clientSideDataFormatting.add(new ImmutablePair<Integer, Integer>(3, ReportRequestHandler.RIGHT_ALIGN_INT));

		
		reportRequestHandler.handleReportGet(request, response, userDTO, "doctor_performance_report",
				MenuConstants.DOCTOR_ACTIVITIES_REPORT, language, reportName, "doctor_performance_report");


        
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    	doGet(request, response);
    }
 
}
