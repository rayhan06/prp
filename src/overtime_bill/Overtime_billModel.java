package overtime_bill;

import overtime_allowance.Overtime_allowanceModel;

import java.util.List;

public class Overtime_billModel {
    public Overtime_billDTO overtimeBillDTO;
    public List<Overtime_allowanceModel> allowanceModels;
}
