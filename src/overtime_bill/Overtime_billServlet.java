package overtime_bill;

import allowance_employee_info.AllowanceEmployeeInfoDTO;
import allowance_employee_info.AllowanceEmployeeInfoRepository;
import bill_approval_history.BillApprovalResponse;
import bill_approval_history.BillApprovalStatus;
import bill_approval_history.Bill_approval_historyDAO;
import bill_approval_history.Bill_approval_historyDTO;
import budget_office.Budget_officeRepository;
import common.BaseServlet;
import common.CommonDAOService;
import common.CustomException;
import common.RoleEnum;
import employee_leave_details.Employee_leave_detailsDAO;
import employee_leave_details.Employee_leave_detailsDTO;
import login.LoginDTO;
import office_unit_organograms.OfficeUnitOrganograms;
import office_unit_organograms.OfficeUnitOrganogramsRepository;
import office_units.OfficeUnitTypeEnum;
import office_units.Office_unitsDTO;
import office_units.Office_unitsRepository;
import org.apache.log4j.Logger;
import ot_bill_submission_config.OT_bill_submission_configDAO;
import ot_bill_submission_config.OT_bill_submission_configDTO;
import ot_bill_type_config.OT_bill_type_configDTO;
import ot_bill_type_config.OT_bill_type_configRepository;
import ot_employee_type.OT_employee_typeRepository;
import ot_type_calendar.OT_type_CalendarServlet;
import overtime_allowance.Overtime_allowanceDAO;
import overtime_allowance.Overtime_allowanceDTO;
import overtime_allowance.Overtime_allowanceEmployeeIdModel;
import overtime_allowance.Overtime_allowanceModel;
import pb.Utils;
import permission.MenuConstants;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;
import util.HttpRequestUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.util.Comparator.comparingLong;

@SuppressWarnings({"Duplicates", "unchecked"})
@WebServlet("/Overtime_billServlet")
@MultipartConfig
public class Overtime_billServlet extends BaseServlet {

    private static final Logger logger = Logger.getLogger(Overtime_billServlet.class);
    public static final Set<Integer> ONE_LAYER_OFFICE_TYPES = new HashSet<>(Arrays.asList(
            OfficeUnitTypeEnum.SPEAKER.getValue(),
            OfficeUnitTypeEnum.SECRETARY.getValue(),
            OfficeUnitTypeEnum.WINGS.getValue(),
            OfficeUnitTypeEnum.BRANCH.getValue(),
            OfficeUnitTypeEnum.DIVISION.getValue(),
            OfficeUnitTypeEnum.UNIT.getValue(),
            OfficeUnitTypeEnum.NATIONAL_PARLIAMENT.getValue()
    ));


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }

        String actionType = request.getParameter("actionType");
        try {
            switch (actionType) {
                case "getAddPage":
                    if (Utils.checkPermission(userDTO, MenuConstants.OVERTIME_BILL_ADD)) {
                        request.getRequestDispatcher("overtime_bill/overtime_billEdit.jsp").forward(request, response);
                        return;
                    }
                    break;
                case "getEditPage":
                    if (Utils.checkPermission(userDTO, MenuConstants.OVERTIME_BILL_ADD)) {
                        Long id = Utils.parseOptionalLong(request.getParameter("ID"), null, null);
                        String language = userDTO.languageID == SessionConstants.ENGLISH ? "English" : "Bangla";

                        Overtime_billModel overtimeBillModel = getOvertimeBillModelFromDB(id, language);
                        request.setAttribute("overtimeBillDTO", overtimeBillModel.overtimeBillDTO);
                        Overtime_billTableData overtimeBillTableData = new Overtime_billTableData();
                        overtimeBillTableData.sortedBillDates = overtimeBillModel.overtimeBillDTO.getSortedBillDates();
                        overtimeBillTableData.allowanceModels = overtimeBillModel.allowanceModels;
                        try {
                            overtimeBillModel.overtimeBillDTO.checkEditabilityOrThrowException(userDTO, language);
                            overtimeBillTableData.isEditable = true;
                        } catch (Exception ex) {
                            logger.error(ex);
                            request.setAttribute("notEditableCause", ex.getMessage());
                            overtimeBillTableData.isEditable = false;
                        }
                        request.setAttribute("overtimeBillTableData", overtimeBillTableData);
                        request.getRequestDispatcher("overtime_bill/overtime_billEdit.jsp").forward(request, response);
                        return;
                    }
                    break;
                case "ajax_getOvertimeBillData":
                    if (Utils.checkPermission(userDTO, MenuConstants.OVERTIME_BILL_ADD)) {
                        UserInput userInput = new UserInput();
                        userInput.otBillSubmissionConfigId = Utils.parseOptionalLong(request.getParameter("otBillSubmissionConfigId"), Long.MIN_VALUE, null);
                        userInput.officeUnitsId = Utils.parseOptionalLong(request.getParameter("officeUnitsId"), Long.MIN_VALUE, null);
                        userInput.otEmployeeTypeId = Utils.parseOptionalLong(request.getParameter("otEmployeeTypeId"), Long.MIN_VALUE, null);
                        userInput.designationPrefix = OT_employee_typeRepository.getInstance().getDesignationPrefixId(userInput.otEmployeeTypeId);
                        userInput.language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
                        userInput.isLangEn = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
                        request.setAttribute("overtimeBillTableData", getOvertimeBillTableData(userInput));
                        request.getRequestDispatcher("overtime_bill/overtime_billTable.jsp").forward(request, response);
                        return;
                    }
                    break;
                case "search": {
                    Map<String, String> extraCriteriaMap;
                    extraCriteriaMap = (Map<String, String>) request.getAttribute(CommonDAOService.CRITERIA_MAP_KEY);
                    if (extraCriteriaMap == null) extraCriteriaMap = new HashMap<>();
                    extraCriteriaMap.put("isInPreview", "0");
                    if (!isAllowedToSeeAllOfficeBill(userDTO)) {
                        Office_unitsDTO usersAllowedOfficeUnitDTO = getOfficeUnitFromUser(userDTO);
                        long usersAllowedOfficeUnitId = usersAllowedOfficeUnitDTO == null ? -1 : usersAllowedOfficeUnitDTO.iD;
                        String restrictedViewWhereClause = String.format(
                                Overtime_billDAO.restrictedViewWhereClause,
                                usersAllowedOfficeUnitId,
                                userDTO.organogramID
                        );
                        request.setAttribute("Overtime_billServlet.restrictedViewWhereClause", restrictedViewWhereClause);
                    }
                    String isInPreview = request.getParameter("isInPreview");
                    if (isInPreview == null) {
                        request.setAttribute(CommonDAOService.CRITERIA_MAP_KEY, extraCriteriaMap);
                    }
                    super.doGet(request, response);
                    return;
                }
                default:
                    super.doGet(request, response);
                    return;
            }
        } catch (Exception ex) {
            logger.error(ex);
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }

    @Override
    public String getWhereClause(HttpServletRequest request) {
        return (String) request.getAttribute("Overtime_billServlet.restrictedViewWhereClause");
    }

    @Override
    public String getSortClause(HttpServletRequest request) {
        return " approval_status ASC, lastModificationTime DESC ";
    }

    public static Office_unitsDTO getOfficeUnitFromUser(UserDTO userDTO) {
        long organogramId = userDTO.organogramID;
        OfficeUnitOrganograms organogramDTO = OfficeUnitOrganogramsRepository.getInstance().getById(organogramId);
        if (organogramDTO == null) return null;
        Office_unitsDTO current = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(organogramDTO.office_unit_id);
        while (current != null) {
            if (isValidOfficeType(current.officeOrder)) {
                return current;
            }
            current = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(current.parentUnitId);
        }
        return null;
    }

    public static boolean isAllowedToSeeAllOfficeBill(UserDTO userDTO) {
        if (userDTO == null) return false;
        return userDTO.roleID == RoleEnum.ADMIN.getRoleId()
               || userDTO.roleID == RoleEnum.HR_ADMIN.getRoleId()
               || userDTO.roleID == RoleEnum.DATA_ENTRY.getRoleId();
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }

        String actionType = request.getParameter("actionType");
        try {
            if ("ajax_approve".equals(actionType)) {
                Map<String, Object> res = new HashMap<>();
                try {
                    approve(request, userDTO);
                    res.put("success", true);
                    res.put("message", "");
                } catch (Exception ex) {
                    logger.error(ex);
                    res.put("success", false);
                    res.put("message", ex.getMessage());
                }
                response.setContentType("application/json");
                response.setCharacterEncoding("UTF-8");
                response.getWriter().print(gson.toJson(res));
            } else if ("ajax_submit".equals(actionType)) {
                Map<String, Object> res = submitOvertimeBill(request, userDTO);
                response.setContentType("application/json");
                response.setCharacterEncoding("UTF-8");
                response.getWriter().print(gson.toJson(res));
            } else if ("ajax_deleteBill".equals(actionType)) {
                Map<String, Object> res = deleteBill(request, userDTO);
                response.setContentType("application/json");
                response.setCharacterEncoding("UTF-8");
                response.getWriter().print(gson.toJson(res));
            } else {
                super.doPost(request, response);
            }
            return;
        } catch (Exception ex) {
            logger.error(ex);
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }

    public void deleteBillDtoOrThrow(String idStr, UserDTO userDTO, boolean isLangEn) throws Exception {
        long id = Utils.parseMandatoryLong(idStr, "invalid id=" + idStr);
        Overtime_billDTO billDTO = Overtime_billDAO.getInstance().getDTOFromID(id);
        if (billDTO == null) {
            throw new CustomException(isLangEn ? "Already deleted!" : "ইতোমধ্যে ডিলিট করা হয়েছে!");
        }
        if (!billDTO.isDeletableByUser(userDTO)) {
            throw new CustomException(isLangEn ? "You are not allowed to delete" : "আপনার ডিলিট করার অনুমতি নেই");
        }
        if (billDTO.hasFinancePreparedBill()) {
            throw new CustomException(
                    isLangEn ? "Unable to delete because Finance has already prepared this bill!"
                             : "ডিলিট করতে ব্যর্থ হয়েছে কারণ অর্থ শাখা বিল প্রস্তুত করে ফেলেছে"
            );
        }
        boolean isDeleted = Overtime_billDAO.getInstance().deleteBillById(id, userDTO.ID, System.currentTimeMillis());
        if (!isDeleted) {
            throw new RuntimeException("failed to delete overtime_billDTO.id=" + id);
        }
    }

    private Map<String, Object> deleteBill(HttpServletRequest request, UserDTO userDTO) {
        boolean isLangEn = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
        Map<String, Object> res = new HashMap<>();
        res.put("success", true);
        res.put("message", isLangEn ? "Deleted successfully" : "সফলভাবে ডিলিট করা হয়েছে");

        try {
            deleteBillDtoOrThrow(request.getParameter("ID"), userDTO, isLangEn);
        } catch (CustomException customException) {
            logger.error(customException);
            res.put("success", false);
            res.put("message", customException.getMessage());
        } catch (Exception e) {
            logger.error(e);
            res.put("success", false);
            res.put("message", isLangEn ? "Failed to delete" : "ডিলিট করতে ব্যর্থ হয়েছে");
        }
        return res;
    }

    private Map<String, Object> submitOvertimeBill(HttpServletRequest request, UserDTO userDTO) {
        String language = userDTO.languageID == SessionConstants.ENGLISH ? "English" : "Bangla";
        boolean isLangEn = language.equalsIgnoreCase("English");
        Long id = Utils.parseOptionalLong(request.getParameter("id"), null, null);
        Map<String, Object> res = new HashMap<>();
        res.put("success", false);
        res.put("message", getServletName() + "?actionType=search");

        if (id == null) {
            res.put("message", isLangEn ? "Invalid id" : "ভুল আইডি");
            return res;
        }
        Overtime_billDTO overtimeBillDTO = Overtime_billDAO.getInstance().getDTOFromID(id);
        if (overtimeBillDTO == null) {
            res.put("message", (isLangEn ? "No record found with id=" : "কোন তথ্য পাওয়া যায়নি যার আইডি=") + id);
            return res;
        }
        try {
            Utils.handleTransaction(() -> {
                overtimeBillDTO.checkEditabilityOrThrowException(userDTO, language);
                overtimeBillDTO.setPreparedInfo(userDTO, language);
                Overtime_billDAO.getInstance().update(overtimeBillDTO);
                List<Bill_approval_historyDTO> historyOfCurrentLevel = Bill_approval_historyDAO.getInstance().findByLevel(
                        overtimeBillDTO.getTableName(), overtimeBillDTO.iD, overtimeBillDTO.currentApprovalLevel
                );
                Optional<Bill_approval_historyDTO> usersHistoryOptional = Bill_approval_historyDTO.findUsersHistory(historyOfCurrentLevel, userDTO);
                List<Bill_approval_historyDTO> nextApprovalHistoryDTOs;
                if (usersHistoryOptional.isPresent()) {
                    nextApprovalHistoryDTOs = approveAndGetNextLayerApprovers(overtimeBillDTO, userDTO);
                } else {
                    nextApprovalHistoryDTOs = historyOfCurrentLevel;
                }
                Overtime_billNotification.getInstance().send(overtimeBillDTO, nextApprovalHistoryDTOs);
            });
        } catch (CustomException e) {
            logger.error(e);
            res.put("message", e.getMessage());
            return res;
        } catch (Exception e) {
            logger.error(e);
            res.put("message", isLangEn ? "Failed to submit" : "সাবমিট করতে ব্যর্থ হয়েছে");
            return res;
        }
        res.put("success", true);
        return res;
    }

    private void approve(HttpServletRequest request, UserDTO userDTO) throws Exception {
        Long id = Utils.parseOptionalLong(request.getParameter("id"), null, null);
        String language = userDTO.languageID == SessionConstants.ENGLISH ? "English" : "Bangla";

        Overtime_billDTO overtimeBillDTO = Overtime_billDAO.getInstance().getDTOFromID(id);
        // not done approval history editability check, because it is done inside Bill_approval_historyDAO.getInstance().approve()
        overtimeBillDTO.checkEditabilityOrThrowException(userDTO, language, false);
        List<Bill_approval_historyDTO> nextApprovalHistoryDTOs = approveAndGetNextLayerApprovers(overtimeBillDTO, userDTO);
        Overtime_billNotification.getInstance().send(overtimeBillDTO, nextApprovalHistoryDTOs);
    }

    private List<Bill_approval_historyDTO> approveAndGetNextLayerApprovers(Overtime_billDTO overtimeBillDTO, UserDTO userDTO) throws Exception {
        BillApprovalResponse approvalResponse =
                Bill_approval_historyDAO.getInstance()
                                        .approveWitAlreadyAddedApprover(overtimeBillDTO, userDTO);
        if (!approvalResponse.hasNextApproval) {
            overtimeBillDTO.approvalStatus = BillApprovalStatus.APPROVED.getValue();
        }
        overtimeBillDTO.currentApprovalLevel = approvalResponse.nextLevel;
        overtimeBillDTO.modifiedBy = userDTO.ID;
        overtimeBillDTO.lastModificationTime = System.currentTimeMillis();
        Overtime_billDAO.getInstance().update(overtimeBillDTO);
        return approvalResponse.nextApprovalHistoryDTOs;
    }

    public Overtime_billModel getOvertimeBillModelFromDB(Long id, String language) {
        if (id == null) throw new IllegalArgumentException("ID can not be null");

        Overtime_billDTO overtimeBillDTO = Overtime_billDAO.getInstance().getDTOFromID(id);
        if (overtimeBillDTO == null) throw new NoSuchElementException("No Overtime_billDTO fond with ID=" + id);

        List<Overtime_allowanceDTO> overtimeAllowanceDTOs = Overtime_allowanceDAO.getInstance().findByOvertimeBillId(overtimeBillDTO.iD);
        if (overtimeAllowanceDTOs == null || overtimeAllowanceDTOs.isEmpty())
            throw new NoSuchElementException("No Overtime_allowanceDTO list fond with overtimeBillId=" + id);
        if (overtimeBillDTO.isInPreviewStage) {
            overtimeAllowanceDTOs.sort(Overtime_allowanceDTO.compareOfficeUnitThenOrganogramOrderValue);
        }
        Overtime_billModel overtimeBillModel = new Overtime_billModel();
        overtimeBillModel.overtimeBillDTO = overtimeBillDTO;
        overtimeBillModel.allowanceModels =
                overtimeAllowanceDTOs.stream()
                                     .map(allowanceDTO -> new Overtime_allowanceModel(allowanceDTO, language))
                                     .collect(Collectors.toList());
        return overtimeBillModel;
    }


    private Set<Long> getAllowedOfficeUnitIds(long officeUnitId) {
        Office_unitsDTO officeUnitsDTO = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(officeUnitId);
        if (officeUnitsDTO == null) return new HashSet<>();
        if (ONE_LAYER_OFFICE_TYPES.contains(officeUnitsDTO.officeOrder)) {
            return new HashSet<>(Collections.singletonList(officeUnitId));
        }
        return new HashSet<>();
    }

    private boolean isDateFallsInLeave(long date, List<Employee_leave_detailsDTO> leavesSortedByStartDate) {
        return leavesSortedByStartDate
                .stream()
                .anyMatch(leaveDetailsDTO -> (leaveDetailsDTO.leaveStartDate <= date) && (date <= leaveDetailsDTO.leaveEndDate));
    }

    private Overtime_allowanceModel getOvertimeAllowanceModel(AllowanceEmployeeInfoDTO employeeInfoDTO, UserInput userInput) {
        employeeInfoDTO.designationPrefix = userInput.designationPrefix;
        Overtime_allowanceModel model = new Overtime_allowanceModel(employeeInfoDTO, userInput.language);

        List<Employee_leave_detailsDTO> approvedLeavesSortedByStartDate =
                Employee_leave_detailsDAO.getInstance().findApprovedLeavesByEmployeeInRange(
                        employeeInfoDTO.employeeRecordId,
                        userInput.startDateInclusive,
                        userInput.endDateExclusive
                );
        approvedLeavesSortedByStartDate.sort(comparingLong(leaveDetailsDTO -> leaveDetailsDTO.leaveStartDate));

        if (userInput.overtimeInfoList != null) {
            Function<OvertimeInfo, String> getOvertimeTypeForEmployee = overtimeInfo -> {
                if (isDateFallsInLeave(overtimeInfo.date, approvedLeavesSortedByStartDate)) {
                    return OvertimeType.BLANK.getValue();
                }
                if (employeeInfoDTO.isEmployeeActiveInThisTime(overtimeInfo.date)) {
                    return overtimeInfo.overtimeType.getValue();
                }
                return OvertimeType.BLANK.getValue();
            };
            model.overtimeTypes =
                    userInput.overtimeInfoList
                            .stream()
                            .map(getOvertimeTypeForEmployee)
                            .collect(Collectors.toList());
        }
        model.calculateAndSetKaKhaDays();
        return model.isAllOtDaysBlank() ? null : model;
    }

    private Overtime_billTableData getOvertimeBillTableData(UserInput userInput) {
        Overtime_billTableData overtimeBillTableData = new Overtime_billTableData();

        Set<Long> officeUnitIds = getAllowedOfficeUnitIds(userInput.officeUnitsId);
        if (officeUnitIds.isEmpty()) {
            overtimeBillTableData.errorMessage = userInput.isLangEn ? "This Office is not allowed to submit bill" : "এই কার্যালয়ের বিল জমা দেয়ার অনুমতি নেই";
            return overtimeBillTableData;
        }
        // IMPORTANT 16-11-2022 Temporary Ban on OT-Food bill Submission for MP Offices
        Office_unitsDTO officeUnitsDTO = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(userInput.officeUnitsId);
        if (officeUnitsDTO.parentUnitId == 9) {
            overtimeBillTableData.errorMessage =
                    userInput.isLangEn
                    ? "It is requested not to file through PRP software until further instructions regarding the overtime allowance of Personal Assistants of Hon'ble Members of Parliament"
                    : "মাননীয় সংসদ সদস্যগণের ব্যক্তিগত সহকারী এর অধিকাল ভাতা বিল পরবর্তী নির্দেশনা না দেয়া পর্যন্ত পি আর পি সফটওয়্যার এর মাধ্যমে দাখিল না করার জন্য অনুরোধ করা হলো";
            return overtimeBillTableData;
        }

        OT_bill_submission_configDTO submissionConfigDTO = OT_bill_submission_configDAO.getInstance().getDTOFromID(userInput.otBillSubmissionConfigId);
        if (submissionConfigDTO == null) {
            overtimeBillTableData.errorMessage = userInput.isLangEn ? "Select correct bill date" : "সঠিক বিলের তারিখ বাছাই করুন";
            return overtimeBillTableData;
        }
        userInput.setStartEndDate(submissionConfigDTO);

        List<Overtime_allowanceEmployeeIdModel> addedEmployeeIdModel = Overtime_allowanceDAO.getInstance().getAddedEmployeeIdModel(
                userInput.officeUnitsId, userInput.otEmployeeTypeId, userInput.otBillSubmissionConfigId
        );
        Set<Long> excludedEmployeeRecordIds =
                addedEmployeeIdModel
                        .stream()
                        .filter(employeeIdModel -> !employeeIdModel.isInPreviewStage)
                        .map(employeeIdModel -> employeeIdModel.employeeRecordsId)
                        .collect(Collectors.toSet());
        List<AllowanceEmployeeInfoDTO> employeeInfoDTOs =
                AllowanceEmployeeInfoRepository.getInstance().buildDTOsForOvertimeBill(
                        officeUnitIds,
                        OT_employee_typeRepository.getInstance().getById(userInput.otEmployeeTypeId),
                        excludedEmployeeRecordIds,
                        userInput.startDateInclusive,
                        userInput.endDateExclusive
                );
        userInput.overtimeInfoList = OT_type_CalendarServlet.getOtTypeWithDateListOfTimeRange(
                userInput.startDateInclusive, userInput.endDateExclusive
        );
        Map<Long, Overtime_allowanceEmployeeIdModel> allowanceEmployeeIdModelByEmployeeId =
                addedEmployeeIdModel.stream()
                                    .collect(Collectors.toMap(
                                            employeeIdModel -> employeeIdModel.employeeRecordsId,
                                            Function.identity(),
                                            (e1, e2) -> e1
                                    ));
        overtimeBillTableData.allowanceModels = new ArrayList<>(
                employeeInfoDTOs
                        .stream()
                        .sequential()
                        .map(employeeInfoDTO -> getOvertimeAllowanceModel(employeeInfoDTO, userInput))
                        .filter(Objects::nonNull)
                        .collect(Collectors.groupingBy(
                                model -> model.employeeRecordsId,
                                LinkedHashMap::new,
                                Collectors.collectingAndThen(
                                        Collectors.toList(),
                                        Overtime_allowanceModel::mergeSameEmployeeIdModels
                                )
                        )).values()
        );
        Set<Long> addedEmployeeRecordIds = new HashSet<>();
        for (int i = 0; i < overtimeBillTableData.allowanceModels.size(); ++i) {
            Overtime_allowanceModel model = overtimeBillTableData.allowanceModels.get(i);
            model.serialNo = i + 1;
            long employeeRecordsId = Long.parseLong(model.employeeRecordsId);
            addedEmployeeRecordIds.add(employeeRecordsId);
            model.overtimeAllowanceId =
                    Optional.ofNullable(allowanceEmployeeIdModelByEmployeeId.get(employeeRecordsId))
                            .map(employeeIdModel -> employeeIdModel.overtimeAllowanceId)
                            .orElse(-1L);
        }
        overtimeBillTableData.isEditable = true;
        overtimeBillTableData.sortedBillDates = userInput.overtimeInfoList.stream()
                                                                          .map(overtimeInfo -> overtimeInfo.date)
                                                                          .collect(Collectors.toList());
        overtimeBillTableData.deletedOvertimeAllowanceIds =
                addedEmployeeIdModel.stream()
                                    .filter(idModel -> idModel.isInPreviewStage)
                                    .filter(idModel -> !addedEmployeeRecordIds.contains(idModel.employeeRecordsId))
                                    .map(idModel -> idModel.overtimeAllowanceId)
                                    .collect(Collectors.toList());
        return overtimeBillTableData;
    }

    private static class UserInput {
        public String addEditRequestSource;
        public Boolean isInPreviewStage;
        String language;
        Boolean isLangEn;
        Long otBillSubmissionConfigId;
        Long modifierId, modifiedTime, budgetOfficeId, employeeRecordsId;
        Long overtimeBillId;
        Long officeUnitsId;
        Long otEmployeeTypeId;
        String designationPrefix;
        Overtime_allowanceModel overtimeAllowanceModel;
        List<Overtime_allowanceModel> allowanceModelListFromRequest;
        List<Long> deletedOvertimeAllowanceIds;
        Long startDateInclusive, endDateExclusive;
        List<OvertimeInfo> overtimeInfoList;
        Long kaDailyRate, khaDailyRate;
        Integer revenueStampDeduction;

        public void setStartEndDate(OT_bill_submission_configDTO submissionConfigDTO) {
            startDateInclusive = submissionConfigDTO.billStartDate;
            endDateExclusive = submissionConfigDTO.billEndDate + 1;
        }
    }

    public Overtime_allowanceDTO getOvertimeAllowance(UserInput userInput, Overtime_allowanceDTO alreadyAddedAllowanceDTO) {
        Overtime_allowanceDTO overtimeAllowanceDTO = alreadyAddedAllowanceDTO;
        if (overtimeAllowanceDTO == null) {
            overtimeAllowanceDTO = new Overtime_allowanceDTO();
            overtimeAllowanceDTO.insertedBy = userInput.modifierId;
            overtimeAllowanceDTO.insertionDate = userInput.modifiedTime;
        }
        overtimeAllowanceDTO.modifiedBy = userInput.modifierId;
        overtimeAllowanceDTO.lastModificationTime = userInput.modifiedTime;
        if (userInput.isInPreviewStage) {
            overtimeAllowanceDTO.officeUnitId = userInput.officeUnitsId;
            overtimeAllowanceDTO.budgetOfficeId = userInput.budgetOfficeId;
            AllowanceEmployeeInfoDTO employeeInfoDTO =
                    AllowanceEmployeeInfoRepository.getInstance()
                                                   .getByEmployeeRecordId(userInput.employeeRecordsId);
            if (employeeInfoDTO == null) {
                return null;
            }
            overtimeAllowanceDTO.allowanceEmployeeInfoId = employeeInfoDTO.iD;
            overtimeAllowanceDTO.designationPrefix = userInput.designationPrefix;
            overtimeAllowanceDTO.officeUnitId = employeeInfoDTO.officeUnitId;
            overtimeAllowanceDTO.organogramId = employeeInfoDTO.organogramId;
            overtimeAllowanceDTO.employeeRecordsId = employeeInfoDTO.employeeRecordId;
            overtimeAllowanceDTO.kaDailyRate = userInput.kaDailyRate;
            overtimeAllowanceDTO.khaDailyRate = userInput.khaDailyRate;
            overtimeAllowanceDTO.revenueStampDeduction = userInput.revenueStampDeduction;
        }
        overtimeAllowanceDTO.serialNo = userInput.overtimeAllowanceModel.serialNo;
        overtimeAllowanceDTO.setOvertimeTypesFromStringValues(userInput.overtimeAllowanceModel.overtimeTypes);
        overtimeAllowanceDTO.kaDays = Long.parseLong(userInput.overtimeAllowanceModel.kaDays);
        overtimeAllowanceDTO.khaDays = Long.parseLong(userInput.overtimeAllowanceModel.khaDays);
        overtimeAllowanceDTO.calculateAndSetTotalAmount();
        return overtimeAllowanceDTO;
    }

    private Overtime_billDTO getNewOvertimeBillDTO(UserInput userInput) {
        Overtime_billDTO overtimeBillDTO = new Overtime_billDTO();
        overtimeBillDTO.insertedBy = userInput.modifierId;
        overtimeBillDTO.insertionTime = userInput.modifiedTime;
        overtimeBillDTO.officeUnitId = userInput.officeUnitsId;
        overtimeBillDTO.budgetOfficeId = userInput.budgetOfficeId;
        overtimeBillDTO.otEmployeeTypeId = userInput.otEmployeeTypeId;
        overtimeBillDTO.employeeCount = 0L;
        overtimeBillDTO.billAmount = 0L;
        return overtimeBillDTO;
    }

    public static boolean isValidOfficeType(int officeOrder) {
        return ONE_LAYER_OFFICE_TYPES.contains(officeOrder);
    }

    @Override
    public String getAjaxAddRedirectURL(HttpServletRequest request, CommonDTO commonDTO) {
        boolean isSourcePreview = "preview".equals(request.getParameter("source"));
        return getServletName()
               + (isSourcePreview ? "?actionType=view&ID=" + commonDTO.iD : "?actionType=search");
    }

    @Override
    public String getAjaxEditRedirectURL(HttpServletRequest request, CommonDTO commonDTO) {
        return getAjaxAddRedirectURL(request, commonDTO);
    }

    private UserInput parseUserInput(HttpServletRequest request, UserDTO userDTO) throws Exception {
        UserInput userInput = new UserInput();
        userInput.isLangEn = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
        userInput.language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;

        userInput.otBillSubmissionConfigId = Utils.parseMandatoryLong(
                request.getParameter("otBillSubmissionConfigId"),
                userInput.isLangEn ? "Select Bill Date" : "বিলের তারিখ বাছাই করুন"
        );
        userInput.officeUnitsId = Utils.parseMandatoryLong(
                request.getParameter("officeUnitsId"),
                userInput.isLangEn ? "Select Office" : "দপ্তর বাছাই করুন"
        );
        userInput.addEditRequestSource = request.getParameter("source");
        Office_unitsDTO officeUnitsDTO = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(userInput.officeUnitsId);
        if (officeUnitsDTO == null) {
            throw new Exception(userInput.isLangEn ? "Select Correct Office" : "সঠিক দপ্তর বাছাই করুন");
        }
        if (!isValidOfficeType(officeUnitsDTO.officeOrder)) {
            throw new Exception(
                    userInput.isLangEn
                    ? String.format("%s is not allowed to submit Overtime Bill", officeUnitsDTO.unitNameEng)
                    : String.format("%s-এর অতিরিক্ত খাটুনি ভাতার বিল জমা দেবার অনুমতি নেই", officeUnitsDTO.unitNameEng)
            );
        }
        userInput.budgetOfficeId = Budget_officeRepository.getInstance().getBudgetOfficeIdByOfficeUnitId(userInput.officeUnitsId);
        userInput.otEmployeeTypeId = Utils.parseMandatoryLong(
                request.getParameter("otEmployeeTypeId"),
                userInput.isLangEn ? "Select Employee Type" : "কর্মকর্তা/কর্মচারীর ধরণ বাছাই করুন"
        );
        userInput.designationPrefix = OT_employee_typeRepository.getInstance().getDesignationPrefixId(userInput.otEmployeeTypeId);
        userInput.modifiedTime = System.currentTimeMillis();
        userInput.modifierId = userDTO.ID;
        try {
            userInput.deletedOvertimeAllowanceIds = Arrays.asList(gson.fromJson(
                    request.getParameter("deletedOvertimeAllowanceIds"),
                    Long[].class
            ));
            userInput.allowanceModelListFromRequest =
                    Arrays.stream(gson.fromJson(
                                  request.getParameter("overtimeAllowanceModels"),
                                  Overtime_allowanceModel[].class
                          ))
                          .filter(allowanceModel -> !userInput.deletedOvertimeAllowanceIds.contains(allowanceModel.overtimeAllowanceId))
                          .collect(Collectors.toList());
        } catch (Exception ex) {
            logger.error(ex);
            throw new Exception(userInput.isLangEn ? "Wrong Employee Info" : "কর্মকর্তা/কর্মচারীর তথ্য ভুল দেয়া হয়েছে");
        }
        return userInput;
    }

    private Overtime_billDTO saveOvertimeBillDTO(Overtime_billDTO overtimeBillDTO, UserInput userInput, UserDTO userDTO) throws Exception {
        if (overtimeBillDTO == null) {
            throw new CustomException(userInput.isLangEn ? "No record found to edit" : "এডিট করার মত কোন তথ্য পাওয়া যায়নি");
        }
        if (overtimeBillDTO.isInPreviewStage) {
            overtimeBillDTO.isInPreviewStage = "preview".equals(userInput.addEditRequestSource);
        }
        userInput.isInPreviewStage = overtimeBillDTO.isInPreviewStage;
        Optional<Bill_approval_historyDTO> optionalApprovalHistoryOfUser = overtimeBillDTO.checkEditabilityOrThrowException(userDTO, userInput.language);

        Map<Long, Overtime_allowanceDTO> allowanceDTOById = new HashMap<>();
        if (overtimeBillDTO.iD > 0) {
            allowanceDTOById =
                    Overtime_allowanceDAO.getInstance()
                                         .findByOvertimeBillId(overtimeBillDTO.iD)
                                         .stream()
                                         .collect(Collectors.toMap(allowanceDTO -> allowanceDTO.iD, allowanceDTO -> allowanceDTO));
        }
        Map<Long, Overtime_allowanceDTO> allowanceDTOByEmployeeRecordId =
                allowanceDTOById.values()
                                .stream()
                                .collect(Collectors.toMap(allowanceDTO -> allowanceDTO.employeeRecordsId, allowanceDTO -> allowanceDTO, (e1, e2) -> e1));
        List<Overtime_allowanceDTO> allowanceDTOs = new ArrayList<>();
        for (Overtime_allowanceModel overtimeAllowanceModel : userInput.allowanceModelListFromRequest) {
            userInput.employeeRecordsId = Long.parseLong(overtimeAllowanceModel.employeeRecordsId);
            userInput.overtimeAllowanceModel = overtimeAllowanceModel;
            userInput.overtimeAllowanceModel.calculateAndSetKaKhaDays();
            Overtime_allowanceDTO alreadyAddedAllowanceDTO;
            if (overtimeAllowanceModel.overtimeAllowanceId < 0) {
                alreadyAddedAllowanceDTO = allowanceDTOByEmployeeRecordId.get(userInput.employeeRecordsId);
            } else {
                alreadyAddedAllowanceDTO = allowanceDTOById.get(overtimeAllowanceModel.overtimeAllowanceId);
            }
            Overtime_allowanceDTO allowanceDTO = getOvertimeAllowance(userInput, alreadyAddedAllowanceDTO);
            if (allowanceDTO != null) {
                allowanceDTOs.add(allowanceDTO);
            }
        }
        if (allowanceDTOs.isEmpty()) {
            throw new Exception(userInput.isLangEn ? "No valid employee data found to add" : "যোগ করার জন্য কোন সঠিক কর্মকর্তা/কর্মচারীর পাওয়া যায়নি");
        }
        overtimeBillDTO.setSummaryEmployeeInfo(allowanceDTOs);
        Utils.handleTransaction(() -> {
            if (overtimeBillDTO.iD < 0) {
                Overtime_billDAO.getInstance().add(overtimeBillDTO);
            } else {
                Overtime_billDAO.getInstance().update(overtimeBillDTO);
            }
            Overtime_billDAO.getInstance().insertApproverIntoBillHistory(overtimeBillDTO);
            overtimeBillDTO.billAmount = 0L;
            overtimeBillDTO.employeeCount = 0L;
            // not done with lambda to throw exception above
            for (Overtime_allowanceDTO allowanceDTO : allowanceDTOs) {
                allowanceDTO.overtimeBillId = overtimeBillDTO.iD;
                saveAllowanceAndUpdateBillDTO(allowanceDTO, userInput, overtimeBillDTO);
            }
            boolean hasDeletedAllowance = userInput.deletedOvertimeAllowanceIds != null
                                          && !userInput.deletedOvertimeAllowanceIds.isEmpty();
            if (hasDeletedAllowance) {
                Overtime_allowanceDAO.getInstance().delete(
                        userInput.modifierId,
                        userInput.deletedOvertimeAllowanceIds,
                        userInput.modifiedTime
                );
            }
            Overtime_billDAO.getInstance().update(overtimeBillDTO);
            optionalApprovalHistoryOfUser.ifPresent(
                    approvalHistoryDTO ->
                            Overtime_billNotification
                                    .getInstance()
                                    .sendModificationNotification(overtimeBillDTO, approvalHistoryDTO)
            );
        });
        return overtimeBillDTO;
    }

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        UserInput userInput = parseUserInput(request, userDTO);
        Overtime_billDTO overtimeBillDTO;
        if (addFlag) {
            overtimeBillDTO = Overtime_billDAO.getInstance().findBillDtoForOffice(
                    userInput.otBillSubmissionConfigId, userInput.officeUnitsId, userInput.otEmployeeTypeId, true
            );
            if (overtimeBillDTO == null) {
                overtimeBillDTO = getNewOvertimeBillDTO(userInput);
                userInput.allowanceModelListFromRequest =
                        userInput.allowanceModelListFromRequest
                                .stream()
                                .filter(allowanceModel -> allowanceModel.overtimeAllowanceId < 0)
                                .collect(Collectors.toList());
            }
            OT_bill_submission_configDTO submissionConfigDTO =
                    OT_bill_submission_configDAO.getInstance().getDTOFromID(userInput.otBillSubmissionConfigId);
            if (submissionConfigDTO == null) {
                throw new CustomException(
                        userInput.isLangEn ? "Overtime Allowance Economic Code configuration missing"
                                           : "অধিকাল ভাতার অর্থনৈতিক কোডের কনফিগারেশন পাওয়া যায়নি"
                );
            }
            boolean isAllowedToPrepareBill = submissionConfigDTO.isAllowedToSubmitByTime(System.currentTimeMillis());
            if (!isAllowedToPrepareBill) {
                throw new CustomException(
                        userInput.isLangEn ? "Not Allowed to prepare this bill date now"
                                           : "এখন এই বিলের তারিখের বিল প্রস্তুত করার অনুমতি নেই"
                );
            }
            if(!submissionConfigDTO.isOfficeUnitIdAllowed(overtimeBillDTO.officeUnitId)) {
                throw new CustomException(
                        userInput.isLangEn ? "Office not allowed to submit bill"
                                           : "অফিসের বিল জমা দেওয়ার অনুমতি নেই"
                );
            }
            overtimeBillDTO.setDataFromSubmissionConfig(submissionConfigDTO);
            userInput.kaDailyRate = overtimeBillDTO.kaDailyRate;
            userInput.khaDailyRate = overtimeBillDTO.khaDailyRate;
            userInput.revenueStampDeduction = overtimeBillDTO.revenueStampDeduction;

            OT_bill_type_configDTO otBillTypeConfigDTO =
                    OT_bill_type_configRepository
                            .getInstance()
                            .findByBudgetOfficeAndEmployeeType(userInput.budgetOfficeId, userInput.otEmployeeTypeId);
            if (otBillTypeConfigDTO == null) {
                throw new CustomException(
                        userInput.isLangEn ? "Overtime Allowance Economic Code configuration missing"
                                           : "অধিকাল ভাতার অর্থনৈতিক কোডের কনফিগারেশন পাওয়া যায়নি"
                );
            }
            overtimeBillDTO.taskTypeId = otBillTypeConfigDTO.taskTypeId;
            overtimeBillDTO.budgetMappingId = otBillTypeConfigDTO.budgetMappingId;
            overtimeBillDTO.economicSubCodeId = otBillTypeConfigDTO.economicSubCodeId;
        } else {
            userInput.overtimeBillId = Utils.parseMandatoryLong(
                    request.getParameter("id"),
                    userInput.isLangEn ? "Invalid id to edit" : "এডিট করার জন্য ভুল আইডি দেয়া হয়েছে"
            );
            overtimeBillDTO = Overtime_billDAO.getInstance().getDTOFromID(userInput.overtimeBillId);
            userInput.kaDailyRate = overtimeBillDTO.kaDailyRate;
            userInput.khaDailyRate = overtimeBillDTO.khaDailyRate;
            userInput.revenueStampDeduction = overtimeBillDTO.revenueStampDeduction;
            userInput.allowanceModelListFromRequest =
                    userInput.allowanceModelListFromRequest
                            .stream()
                            .filter(allowanceModel -> allowanceModel.overtimeAllowanceId > 0)
                            .collect(Collectors.toList());
        }
        return saveOvertimeBillDTO(overtimeBillDTO, userInput, userDTO);
    }

    private void saveAllowanceAndUpdateBillDTO(Overtime_allowanceDTO allowanceDTO,
                                               UserInput userInput,
                                               Overtime_billDTO outOvertimeBillDTO) throws Exception {
        if (allowanceDTO.iD < 0) {
            Overtime_allowanceDAO.getInstance().add(allowanceDTO);
        } else {
            Overtime_allowanceDAO.getInstance().update(allowanceDTO);
        }
        outOvertimeBillDTO.billAmount += allowanceDTO.totalAmount;
        ++outOvertimeBillDTO.employeeCount;
        outOvertimeBillDTO.lastModificationTime = userInput.modifiedTime;
        outOvertimeBillDTO.modifiedBy = userInput.modifierId;
    }

    @Override
    public String getTableName() {
        return Overtime_billDAO.getInstance().getTableName();
    }

    @Override
    public String getServletName() {
        return "Overtime_billServlet";
    }

    @Override
    public Overtime_billDAO getCommonDAOService() {
        return Overtime_billDAO.getInstance();
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[]{MenuConstants.OVERTIME_BILL_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[]{MenuConstants.OVERTIME_BILL_ADD};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[]{MenuConstants.OVERTIME_BILL_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Overtime_billServlet.class;
    }
}
