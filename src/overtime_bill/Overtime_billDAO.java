package overtime_bill;

import bill_approval_history.BillApprovalStatus;
import bill_approval_history.Bill_approval_historyDAO;
import bill_approval_history.Bill_approval_historyDTO;
import bill_approval_mapping.Bill_approval_mappingDAO;
import bill_approval_mapping.Bill_approval_mappingDTO;
import bill_register.Bill_registerDAO;
import budget_register.Budget_registerDAO;
import common.CommonDAOService;
import common.ConnectionAndStatementUtil;
import designations.DesignationsEnum;
import office_unit_organograms.OfficeUnitOrganogramsRepository;
import office_units.Office_unitsDTO;
import office_units.Office_unitsRepository;
import org.apache.log4j.Logger;
import overtime_allowance.Overtime_allowanceDAO;
import overtime_allowance.Overtime_allowanceDTO;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.regex.Pattern;

import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;
import static overtime_bill.Overtime_billStatus.PENDING;

@SuppressWarnings({"Duplicates"})
public class Overtime_billDAO implements CommonDAOService<Overtime_billDTO> {
    private static final Logger logger = Logger.getLogger(Overtime_billDAO.class);

    private static final String addSqlQuery =
            "INSERT INTO {tableName} (ot_bill_submission_config_id,bill_start_date,bill_end_date,overtime_bill_type_cat,is_in_preview_stage,office_unit_id,ot_employee_type_id,budget_office_id,budget_mapping_id,economic_sub_code_id,"
                    .concat("prepared_org_id,prepared_name_bn,prepared_org_name_bn,prepared_office_name_bn,prepared_signature,prepared_time,")
                    .concat("finance_bill_status,finance_serial_number,budget_register_id,bill_register_id,overtime_bill_finance_id,employee_count,summary_emp_name_bn,summary_emp_designation_bn,ordinance_text,")
                    .concat("ka_daily_rate,kha_daily_rate,revenue_stamp_deduction,bill_amount,current_approval_level,")
                    .concat("approval_status,task_type_id,modified_by,lastModificationTime,inserted_by,insertion_time,isDeleted,ID) ")
                    .concat("VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

    private static final String updateSqlQuery =
            "UPDATE {tableName} SET ot_bill_submission_config_id=?,bill_start_date=?,bill_end_date=?,overtime_bill_type_cat=?,is_in_preview_stage=?,office_unit_id=?,ot_employee_type_id=?,budget_office_id=?,budget_mapping_id=?,economic_sub_code_id=?,"
                    .concat("prepared_org_id=?,prepared_name_bn=?,prepared_org_name_bn=?,prepared_office_name_bn=?,prepared_signature=?,prepared_time=?,")
                    .concat("finance_bill_status=?,finance_serial_number=?,budget_register_id=?,bill_register_id=?,overtime_bill_finance_id=?,employee_count=?,summary_emp_name_bn=?,summary_emp_designation_bn=?,ordinance_text=?,")
                    .concat("ka_daily_rate=?,kha_daily_rate=?,revenue_stamp_deduction=?,bill_amount=?,current_approval_level=?,")
                    .concat("approval_status=?,task_type_id=?,modified_by=?,lastModificationTime=? WHERE ID=?");

    private static final Map<String, String> searchMap = new HashMap<>();

    public static final String restrictedViewWhereClause =
            " AND (office_unit_id=%d OR overtime_bill.id IN (SELECT approvable_dto_id FROM bill_approval_history "
                    .concat("WHERE table_name='overtime_bill' AND organogram_id=%d AND isDeleted=0)) ");

    private Overtime_billDAO() {
        searchMap.put("monthYearFrom", " AND (bill_end_date >= ?) ");
        searchMap.put("monthYearTo", " AND (bill_start_date <= ?) ");
        searchMap.put("overtimeBillTypeCat", " AND (overtime_bill_type_cat = ?) ");
        searchMap.put("office_unit_id", " AND (office_unit_id = ?) ");
        searchMap.put("otEmployeeTypeId", " AND (ot_employee_type_id = ?) ");
        searchMap.put("financeSerialNumber", " AND (finance_serial_number = ?) ");
        searchMap.put("isInPreview", " AND (is_in_preview_stage = ?) ");
    }

    private static class LazyLoader {
        static final Overtime_billDAO INSTANCE = new Overtime_billDAO();
    }

    public static Overtime_billDAO getInstance() {
        return LazyLoader.INSTANCE;
    }

    @Override
    public void set(PreparedStatement ps, Overtime_billDTO dto, boolean isInsert) throws SQLException {
        int index = 0;
        ps.setLong(++index, dto.otBillSubmissionConfigId);
        ps.setLong(++index, dto.billStartDate);
        ps.setLong(++index, dto.billEndDate);
        ps.setInt(++index, dto.overtimeBillTypeCat);
        ps.setBoolean(++index, dto.isInPreviewStage);
        ps.setLong(++index, dto.officeUnitId);
        ps.setLong(++index, dto.otEmployeeTypeId);
        ps.setLong(++index, dto.budgetOfficeId);
        ps.setLong(++index, dto.budgetMappingId);
        ps.setLong(++index, dto.economicSubCodeId);
        ps.setLong(++index, dto.preparedOrgId);
        ps.setString(++index, dto.preparedNameBn);
        ps.setString(++index, dto.preparedOrgNameBn);
        ps.setString(++index, dto.preparedOfficeNameBn);
        ps.setBytes(++index, dto.preparedSignature);
        ps.setLong(++index, dto.preparedTime);
        ps.setInt(++index, dto.financeBillStatus);
        ps.setInt(++index, dto.financeSerialNumber);
        ps.setLong(++index, dto.budgetRegisterId);
        ps.setLong(++index, dto.billRegisterId);
        ps.setLong(++index, dto.overtimeBillFinanceId);
        ps.setLong(++index, dto.employeeCount);
        ps.setString(++index, dto.summaryEmployeeNameBn);
        ps.setString(++index, dto.summaryEmployeeDesignationBn);
        ps.setString(++index, dto.ordinanceText);
        ps.setLong(++index, dto.kaDailyRate);
        ps.setLong(++index, dto.khaDailyRate);
        ps.setInt(++index, dto.revenueStampDeduction);
        ps.setLong(++index, dto.billAmount);
        ps.setInt(++index, dto.currentApprovalLevel);
        ps.setInt(++index, dto.approvalStatus);
        ps.setLong(++index, dto.taskTypeId);
        ps.setLong(++index, dto.modifiedBy);
        ps.setLong(++index, dto.lastModificationTime);
        if (isInsert) {
            ps.setLong(++index, dto.insertedBy);
            ps.setLong(++index, dto.insertionTime);
            ps.setInt(++index, 0 /* isDeleted */);
        }
        ps.setLong(++index, dto.iD);
    }

    @Override
    public Overtime_billDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Overtime_billDTO dto = new Overtime_billDTO();
            dto.iD = rs.getLong("ID");
            dto.otBillSubmissionConfigId = rs.getLong("ot_bill_submission_config_id");
            dto.billStartDate = rs.getLong("bill_start_date");
            dto.billEndDate = rs.getLong("bill_end_date");
            dto.overtimeBillTypeCat = rs.getInt("overtime_bill_type_cat");
            dto.isInPreviewStage = rs.getBoolean("is_in_preview_stage");
            dto.officeUnitId = rs.getLong("office_unit_id");
            dto.otEmployeeTypeId = rs.getLong("ot_employee_type_id");
            dto.budgetOfficeId = rs.getLong("budget_office_id");
            dto.budgetMappingId = rs.getLong("budget_mapping_id");
            dto.economicSubCodeId = rs.getLong("economic_sub_code_id");
            dto.preparedOrgId = rs.getLong("prepared_org_id");
            dto.preparedNameBn = rs.getString("prepared_name_bn");
            dto.preparedOrgNameBn = rs.getString("prepared_org_name_bn");
            dto.preparedOfficeNameBn = rs.getString("prepared_office_name_bn");
            dto.preparedSignature = rs.getBytes("prepared_signature");
            dto.preparedTime = rs.getLong("prepared_time");
            dto.financeBillStatus = rs.getInt("finance_bill_status");
            dto.financeSerialNumber = rs.getInt("finance_serial_number");
            dto.budgetRegisterId = rs.getLong("budget_register_id");
            dto.billRegisterId = rs.getLong("bill_register_id");
            dto.overtimeBillFinanceId = rs.getLong("overtime_bill_finance_id");
            dto.employeeCount = rs.getLong("employee_count");
            dto.summaryEmployeeNameBn = rs.getString("summary_emp_name_bn");
            dto.summaryEmployeeDesignationBn = rs.getString("summary_emp_designation_bn");
            dto.ordinanceText = rs.getString("ordinance_text");
            dto.kaDailyRate = rs.getLong("ka_daily_rate");
            dto.khaDailyRate = rs.getLong("kha_daily_rate");
            dto.revenueStampDeduction = rs.getInt("revenue_stamp_deduction");
            dto.billAmount = rs.getLong("bill_amount");
            dto.currentApprovalLevel = rs.getInt("current_approval_level");
            dto.approvalStatus = rs.getInt("approval_status");
            dto.taskTypeId = rs.getLong("task_type_id");
            dto.modifiedBy = rs.getLong("modified_by");
            dto.lastModificationTime = rs.getLong("lastModificationTime");
            dto.insertedBy = rs.getLong("inserted_by");
            dto.insertionTime = rs.getLong("insertion_time");
            dto.isDeleted = rs.getInt("isDeleted");
            return dto;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    @Override
    public String getTableName() {
        return "overtime_bill";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    @Override
    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Overtime_billDTO) commonDTO, addSqlQuery, true);
    }

    @Override
    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Overtime_billDTO) commonDTO, updateSqlQuery, false);
    }

    private static final String findNotSubmittedBillSql =
            "select * " +
            "from overtime_bill " +
            "where ot_bill_submission_config_id = %d " +
            "  and office_unit_id = %d " +
            "  and ot_employee_type_id = %d " +
            "  and is_in_preview_stage = %d and isDeleted = 0";

    public Overtime_billDTO findBillDtoForOffice(long otBillSubmissionConfigId, long officeUnitsId, long otEmployeeTypeId, boolean isInPreview) {
        int isInPreviewStage = isInPreview ? 1 : 0;
        return ConnectionAndStatementUtil.getT(
                String.format(findNotSubmittedBillSql, otBillSubmissionConfigId, officeUnitsId, otEmployeeTypeId, isInPreviewStage),
                this::buildObjectFromResultSet
        );
    }

    private static final String getByBudgetMappingEconomicSubcodeSql =
            "SELECT * FROM overtime_bill WHERE budget_mapping_id=%d AND economic_sub_code_id=%d AND ot_bill_submission_config_id=%d AND isDeleted=0";

    public List<Overtime_billDTO> getByBudgetMappingEconomicSubcode(long budgetMappingId, long economicSubCodeId, long otBillSubmissionConfigId) {
        return ConnectionAndStatementUtil.getListOfT(
                String.format(getByBudgetMappingEconomicSubcodeSql, budgetMappingId, economicSubCodeId, otBillSubmissionConfigId),
                this::buildObjectFromResultSet
        );
    }

    private static final String getBySubmissionIdBudgetOfficeIdEmpType =
            "SELECT * FROM overtime_bill WHERE budget_office_id=%d AND ot_employee_type_id=%d AND ot_bill_submission_config_id=%d AND isDeleted=0";

    public List<Overtime_billDTO> getByBudgetOfficeEmployeeTypeConfigId(long budgetOfficeId, long otEmployeeTypeId, long otBillSubmissionConfigId) {
        return ConnectionAndStatementUtil.getListOfT(
                String.format(getBySubmissionIdBudgetOfficeIdEmpType, budgetOfficeId, otEmployeeTypeId, otBillSubmissionConfigId),
                this::buildObjectFromResultSet
        );
    }

    private static final String findByOvertimeBillFinanceIdSql = "SELECT * FROM overtime_bill WHERE overtime_bill_finance_id=%d AND isDeleted=0";

    public List<Overtime_billDTO> findByOvertimeBillFinanceId(long overtimeBillFinanceId) {
        return ConnectionAndStatementUtil.getListOfT(
                String.format(findByOvertimeBillFinanceIdSql, overtimeBillFinanceId),
                this::buildObjectFromResultSet
        );
    }

    public List<Overtime_billDTO> findOfficeApprovedBills(long budgetOfficeId, long otEmployeeTypeId, long otBillSubmissionConfigId) {
        return getByBudgetOfficeEmployeeTypeConfigId(budgetOfficeId, otEmployeeTypeId, otBillSubmissionConfigId)
                .stream()
                .filter(Overtime_billDTO::isApproved)
                .collect(toList());
    }

    public List<Overtime_billDTO> findOfficeApprovedBillsByBudgetMappingEconomicSubcode(long budgetMappingId, long economicSubCodeId, long otBillSubmissionConfigId) {
        return getByBudgetMappingEconomicSubcode(budgetMappingId, economicSubCodeId, otBillSubmissionConfigId)
                .stream()
                .filter(Overtime_billDTO::isApproved)
                .collect(toList());
    }

    public List<Overtime_billDTO> findFinanceBillNotPreparedDtoListByBudgetMappingEconomicSubcode(long budgetMappingId, long economicSubCodeId, long otBillSubmissionConfigId) {
        return findFinanceBillNotPreparedDtoListByBudgetMappingEconomicSubcode(
                findOfficeApprovedBillsByBudgetMappingEconomicSubcode(
                        budgetMappingId, economicSubCodeId, otBillSubmissionConfigId
                )
        );
    }

    public List<Overtime_billDTO> findFinanceBillNotPreparedDtoListByBudgetMappingEconomicSubcode(List<Overtime_billDTO> overtimeBillDTOList) {
        if (overtimeBillDTOList == null) {
            return Collections.emptyList();
        }
        return overtimeBillDTOList
                .stream()
                .filter(overtimeBillDTO -> !overtimeBillDTO.hasFinancePreparedBill())
                .sorted(Overtime_billDTO.compareOfficeUnitId)
                .collect(toList());
    }

    public List<Overtime_billDTO> findFinanceBillPreparedDtoList(List<Overtime_billDTO> overtimeBillDTOList) {
        if (overtimeBillDTOList == null) {
            return Collections.emptyList();
        }
        return overtimeBillDTOList
                .stream()
                .filter(Overtime_billDTO::hasFinancePreparedBill)
                .sorted(Overtime_billDTO.compareOfficeUnitId)
                .collect(toList());
    }

    private static final String findBySubmissionConfigIdSql =
            "SELECT * FROM overtime_bill WHERE ot_bill_submission_config_id in (%s) AND isDeleted=0";

    public List<Long> getBudgetRegisterIdBySubmissionConfigId(List<Long> otBillSubmissionConfigIds) {
        if (otBillSubmissionConfigIds == null || otBillSubmissionConfigIds.isEmpty()) {
            return Collections.emptyList();
        }
        String getDtoListSql = String.format(
                findBySubmissionConfigIdSql,
                otBillSubmissionConfigIds
                        .stream()
                        .map(Objects::toString)
                        .collect(joining(","))
        );
        return ConnectionAndStatementUtil
                .getListOfT(getDtoListSql, this::buildObjectFromResultSet)
                .stream()
                .filter(Overtime_billDTO::hasFinancePreparedBill)
                .map(overtimeBillDTO -> overtimeBillDTO.budgetRegisterId)
                .distinct()
                .collect(toList());
    }

    private boolean isApproverOfDesignation(List<Bill_approval_historyDTO> approvalHistoryDTOs, DesignationsEnum designationsEnum) {
        return approvalHistoryDTOs
                .stream()
                .map(approvalHistoryDTO -> approvalHistoryDTO.organogramId)
                .map(organogramId -> OfficeUnitOrganogramsRepository.getInstance().getById(organogramId))
                .filter(Objects::nonNull)
                .allMatch(officeUnitOrganograms -> officeUnitOrganograms.isDesignation(designationsEnum));
    }

    public void insertApproverIntoBillHistory(Overtime_billDTO overtimeBillDTO) throws Exception {
        if (!overtimeBillDTO.isInPreviewStage) {
            return;
        }
        Bill_approval_historyDAO approvalHistoryDAO = Bill_approval_historyDAO.getInstance();
        approvalHistoryDAO.deleteAllLevelHistory(
                overtimeBillDTO, overtimeBillDTO.modifiedBy, overtimeBillDTO.lastModificationTime
        );
        List<Bill_approval_mappingDTO> billApprovalMappings =
                Bill_approval_mappingDAO.getInstance().findAllLevelsByTaskType(overtimeBillDTO.taskTypeId);

        List<Bill_approval_historyDTO> approvalHistoryDTOs = approvalHistoryDAO.getDTOsFromMappingDTOs(billApprovalMappings, overtimeBillDTO);

        Office_unitsDTO billOfficeUnitDto = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(overtimeBillDTO.officeUnitId);
        // শাখার নিচে উপসচিব হলে সরাসরি অর্থ শাখায় যাবে
        if (billOfficeUnitDto.hasBranchInAncestorNotSelf()) {
            approvalHistoryDTOs = approvalHistoryDAO.getDTOsWithCircuitBreaker(
                    approvalHistoryDTOs,
                    approvalHistoryDTOsOfALevel -> isApproverOfDesignation(approvalHistoryDTOsOfALevel, DesignationsEnum.DEPUTY_SECRETARY),
                    approvalHistoryDTOsOfALevel -> false // don't take next levels after circuit breaker
            );
        }
        approvalHistoryDAO.addAll(approvalHistoryDTOs);

        overtimeBillDTO.currentApprovalLevel = 0;
        overtimeBillDTO.approvalStatus = BillApprovalStatus.PENDING.getValue();
    }


    public boolean deleteBillById(long id, long modifiedBy, long modificationTime) {
        boolean isDeleted = delete(modifiedBy, id, modificationTime);
        if (!isDeleted) {
            return false;
        }
        Overtime_allowanceDAO.getInstance().deleteByOvertimeBillId(id, modifiedBy, modificationTime);
        return true;
    }

    private static class IdFromOvertimeBill {
        long budgetRegisterId;
        long billRegisterId;

        static IdFromOvertimeBill buildFromResultSet(ResultSet rs) {
            try {
                IdFromOvertimeBill idFromOvertimeBill = new IdFromOvertimeBill();
                idFromOvertimeBill.budgetRegisterId = rs.getLong("budget_register_id");
                idFromOvertimeBill.billRegisterId = rs.getLong("bill_register_id");
                return idFromOvertimeBill;
            } catch (Exception e) {
                logger.error(e);
            }
            return null;
        }
    }

    private static final String getIdFromOvertimeBillByFinanceIdSql =
            "select budget_register_id, " +
            "       bill_register_id " +
            "from overtime_bill " +
            "where overtime_bill_finance_id = %d " +
            "  and isDeleted = 0";

    private List<IdFromOvertimeBill> getIdFromOvertimeBillByFinanceId(long overtimeBillFinanceId) {
        return ConnectionAndStatementUtil.getListOfT(
                String.format(getIdFromOvertimeBillByFinanceIdSql, overtimeBillFinanceId),
                IdFromOvertimeBill::buildFromResultSet
        );
    }

    private static final String cascadeFinanceBillDeletedSql =
            "update overtime_bill " +
            "set modified_by = %d," +
            "    lastModificationTime = %d," +
            "    finance_bill_status = %d," +
            "    finance_serial_number = -1," +
            "    budget_register_id = -1," +
            "    bill_register_id = -1," +
            "    overtime_bill_finance_id = -1 " +
            "where overtime_bill_finance_id = %d";

    public void cascadeFinanceBillDeleted(long overtimeBillFinanceId, long modifiedBy, long modificationTime) {
        String sql = String.format(
                cascadeFinanceBillDeletedSql,
                modifiedBy, modificationTime,
                PENDING.getValue(),
                overtimeBillFinanceId
        );
        logger.debug(sql);
        ConnectionAndStatementUtil.getWriteConnectionAndStatement(model -> {
            try {
                model.getStatement().executeUpdate(sql);
                recordUpdateTime(model.getConnection(), getTableName(), modificationTime);
                return true;
            } catch (SQLException ex) {
                ex.printStackTrace();
                return false;
            }
        });
    }

    public void financeBillDeleted(long overtimeBillFinanceId, long modifiedBy, long modificationTime) {
        List<IdFromOvertimeBill> idFromOvertimeBillByFinanceId = getIdFromOvertimeBillByFinanceId(overtimeBillFinanceId);

        cascadeFinanceBillDeleted(overtimeBillFinanceId, modifiedBy, modificationTime);

        Budget_registerDAO.getInstance().delete(
                modifiedBy,
                idFromOvertimeBillByFinanceId
                        .stream()
                        .map(idFromOvertimeBill -> idFromOvertimeBill.budgetRegisterId)
                        .collect(toList()),
                modificationTime
        );

        Bill_registerDAO.getInstance().delete(
                modifiedBy,
                idFromOvertimeBillByFinanceId
                        .stream()
                        .map(idFromOvertimeBill -> idFromOvertimeBill.billRegisterId)
                        .collect(toList()),
                modificationTime
        );
    }

    public BillStatusResponse getStatusForEmployeeDashboard(
            long otBillSubmissionConfigId, long officeUnitsId,
            long otEmployeeTypeId, long employeeRecordId, boolean isLangEn
    ) {
        BillStatusResponse response = new BillStatusResponse();
        Overtime_billDTO billDto = findBillDtoForOffice(otBillSubmissionConfigId, officeUnitsId, otEmployeeTypeId, false);
        if (billDto == null) {
            response.setStatusAndItsMessage(BillStatusView.NOT_SUBMITTED, isLangEn);
            return response;
        }
        Overtime_allowanceDTO employeeAllowanceId =
                Overtime_allowanceDAO.getInstance().findByOvertimeBillIdAndEmployeeId(billDto.iD, employeeRecordId);
        if (employeeAllowanceId == null) {
            response.setStatusAndItsMessage(BillStatusView.SUBMITTED_WITHOUT_EMPLOYEE, isLangEn);
            return response;
        }
        if (billDto.hasFinancePreparedBill()) {
            response.setStatusAndItsMessage(BillStatusView.FINANCE_PREPARED, isLangEn);
            return response;
        }
        if (billDto.isApproved()) {
            response.setStatusAndItsMessage(BillStatusView.SENT_TO_FINANCE, isLangEn);
            return response;
        }
        response.setStatusAndItsMessage(BillStatusView.WAITING_FOR_APPROVAL, isLangEn);
        response.message =
                Bill_approval_historyDAO
                        .getInstance()
                        .findByLevel(billDto.getTableName(), billDto.getId(), billDto.getLevel())
                        .stream()
                        .findFirst()
                        .map(dto -> isLangEn ? " at " + dto.officeNameEn : dto.officeNameBn + "-এ ")
                        .map(officeName -> String.format(response.message, officeName))
                        .orElse(response.message.replaceAll(Pattern.quote("%s"), ""));
        return response;
    }
}
