package overtime_bill;

import bangladehi_number_format_util.BangladeshiNumberFormatter;
import office_units.Office_unitsRepository;
import util.StringUtils;

public class Overtime_billSummaryModel {
    public static final String BILL_DESCRIPTION_BN = "%s\n%sসহ %s জন";
    public static final String BILL_DESCRIPTION_EN = "Including %s\n%s, %s employees";

    public String overtimeBillId;
    public int financeSerialNumber;
    public String billDescription;
    public String officeName;
    public String billAmount;
    public long billAmountLong;

    public Overtime_billSummaryModel(Overtime_billDTO overtimeBillDTO, String language) {
        boolean isLangEng = "english".equalsIgnoreCase(language);
        String empCount = StringUtils.convertBanglaIfLanguageIsBangla(language, String.format("%d", overtimeBillDTO.employeeCount));
        overtimeBillId = String.format("%d", overtimeBillDTO.iD);
        financeSerialNumber = overtimeBillDTO.financeSerialNumber;
        billDescription = String.format(
                isLangEng ? BILL_DESCRIPTION_EN : BILL_DESCRIPTION_BN,
                overtimeBillDTO.summaryEmployeeNameBn,
                overtimeBillDTO.summaryEmployeeDesignationBn,
                empCount
        );
        officeName = Office_unitsRepository.getInstance().geText(language, overtimeBillDTO.officeUnitId);
        billAmountLong = overtimeBillDTO.billAmount;
        billAmount = BangladeshiNumberFormatter.getFormattedNumber(
                StringUtils.convertBanglaIfLanguageIsBangla(
                        language,
                        String.format("%d", overtimeBillDTO.billAmount)
                )
        );
    }

    public String getBillDescriptionHtml() {
        return billDescription.replaceAll("\n", "<br>");
    }
}
