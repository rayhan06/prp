package overtime_bill;

public class OvertimeInfo {
    public Long date;
    public OvertimeType overtimeType;

    public OvertimeInfo() {
    }

    public OvertimeInfo(Long date, OvertimeType overtimeType) {
        this.date = date;
        this.overtimeType = overtimeType;
    }
}
