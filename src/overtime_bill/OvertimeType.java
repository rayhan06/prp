package overtime_bill;

import allowance_configure.AllowanceCatEnum;
import pb.OptionDTO;
import pb.Utils;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public enum OvertimeType {
    KA(AllowanceCatEnum.OVERTIME_A_ALLOWANCE.getValue(), "ক"),
    KHA(AllowanceCatEnum.OVERTIME_B_ALLOWANCE.getValue(), "খ"),
    NONE(0, "-"),
    KA_NONE(-1, "-"),
    KHA_NONE(-2, "-"),
    BLANK(-3, "-"),
    ;
    private final String value;
    private final int intValue;
    private final OptionDTO optionDTO;
    private final String name;

    private static final Map<String, OvertimeType> mapByValue;

    static {
        mapByValue = Arrays.stream(values())
                           .collect(Collectors.toMap(
                                   e -> e.value,
                                   e -> e,
                                   (e1, e2) -> e1
                           ));
    }

    OvertimeType(int intValue, String name) {
        this.intValue = intValue;
        this.value = String.format("%d", this.intValue);
        this.name = name;
        this.optionDTO = new OptionDTO(this.name, this.name, this.value);
    }

    public String getValue() {
        return value;
    }

    public int getIntValue() {
        return intValue;
    }

    public String getName() {
        return name;
    }

    public static OvertimeType getByIntValue(Integer intValue) {
        if (intValue == null) {
            return null;
        }
        return getByValue(String.format("%d", intValue));
    }

    public static OvertimeType getByValue(String value) {
        if (value == null) {
            return null;
        }
        return mapByValue.getOrDefault(value, OvertimeType.BLANK);
    }

    public static String getNameByValue(String value) {
        OvertimeType overtimeType = getByValue(value);
        return overtimeType == null ? "null" : overtimeType.getName();
    }

    public static String buildOptionByValue(String value, String language) {
        return buildOption(getByValue(value), language);
    }

    public static String buildOption(OvertimeType overtimeType, String language) {
        List<OptionDTO> optionDTOList;
        switch (overtimeType) {
            case KA:
            case KA_NONE:
                optionDTOList = Arrays.asList(KA_NONE.optionDTO, KA.optionDTO);
                break;
            case KHA:
            case KHA_NONE:
                optionDTOList = Arrays.asList(KHA_NONE.optionDTO, KHA.optionDTO);
                break;
            case NONE:
                optionDTOList = Arrays.asList(NONE.optionDTO, KA.optionDTO, KHA.optionDTO);
                break;
            default:
                optionDTOList = null;
        }
        return optionDTOList == null ? "" : Utils.buildOptionsWithoutSelectWithSelectId(optionDTOList, language, overtimeType.value);
    }

    private static boolean isNonNoneValue(OvertimeType overtimeType) {
        if (overtimeType == null) {
            return false;
        }
        return overtimeType == KA || overtimeType == KHA;
    }

    public static boolean isNonNoneValue(String value) {
        return isNonNoneValue(getByValue(value));
    }

    public static boolean isNonNoneValue(Integer intValue) {
        return isNonNoneValue(getByIntValue(intValue));
    }
}
