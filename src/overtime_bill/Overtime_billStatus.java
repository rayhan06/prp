package overtime_bill;

public enum Overtime_billStatus {
    PENDING(0),
    APPROVED(1),
    NOT_APPLICABLE(-1),
    WAITING_FOR_LAYER_1(2),
    FINANCE_BILL_PREPARED(3),
    ;

    private final int value;

    Overtime_billStatus(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
