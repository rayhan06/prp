package overtime_bill;

public class BillStatusResponse {
    public String billDescription;
    public BillStatusView status;
    public String message;

    public void setStatusAndItsMessage(BillStatusView status, boolean isLangEn) {
        this.status = status;
        this.message = isLangEn ? status.messageEn : status.messageBn;
    }
}
