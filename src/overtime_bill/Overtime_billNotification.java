package overtime_bill;

import bill_approval_history.Bill_approval_historyDAO;
import bill_approval_history.Bill_approval_historyDTO;
import office_units.Office_unitsDTO;
import office_units.Office_unitsRepository;
import org.apache.log4j.Logger;
import pb_notifications.Pb_notificationsDAO;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@SuppressWarnings({"Duplicates"})
public class Overtime_billNotification {
    private final Logger logger = Logger.getLogger(Overtime_billNotification.class);

    private final Pb_notificationsDAO pb_notificationsDAO;

    private Overtime_billNotification() {
        pb_notificationsDAO = Pb_notificationsDAO.getInstance();
    }

    private static class LazyLoader {
        static final Overtime_billNotification INSTANCE = new Overtime_billNotification();
    }

    public static Overtime_billNotification getInstance() {
        return LazyLoader.INSTANCE;
    }

    public void send(Overtime_billDTO overtimeBillDTO, List<Bill_approval_historyDTO> nextApprovalHistoryDTOs) {
        if (nextApprovalHistoryDTOs == null || nextApprovalHistoryDTOs.isEmpty()) {
            return;
        }
        Office_unitsDTO officeUnitsDTO = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(overtimeBillDTO.officeUnitId);
        String officeNameEn = "", officeNameBn = "";
        if (officeUnitsDTO != null) {
            officeNameEn = officeUnitsDTO.unitNameEng;
            officeNameBn = officeUnitsDTO.unitNameBng;
        }
        String monthNameEn = overtimeBillDTO.getBillDateTypeText(true);
        String monthNameBn = overtimeBillDTO.getBillDateTypeText(false);

        String textEn = monthNameEn + " month's Overtime Bill of " + officeNameEn + " is waiting for your approval";
        String textBn = monthNameBn + " মাসের, " + officeNameBn + "-এর অধিকাল ভাতার বিল আপনার আনুমোদনের জন্য অপেক্ষমান";

        String notificationMessage = textEn + "$" + textBn;
        String url = "Overtime_billServlet?actionType=view&ID=" + overtimeBillDTO.iD;

        sendNotification(
                nextApprovalHistoryDTOs.stream()
                                       .map(approvalHistoryDTO -> approvalHistoryDTO.organogramId)
                                       .collect(Collectors.toList()),
                notificationMessage,
                url
        );
    }

    public void sendModificationNotification(Overtime_billDTO overtimeBillDTO,
                                             Bill_approval_historyDTO approvalHistoryOfUser) {
        if (overtimeBillDTO.isInPreviewStage) {
            logger.debug(String.format(
                    "Overtime_billDTO[id=%d] - modification notification NOT SENT - cause isInPreviewStage",
                    overtimeBillDTO.iD
            ));
            return;
        }
        List<Long> organogramIdsForNotification =
                Bill_approval_historyDAO
                        .getInstance()
                        .findAllApprovedHistory(overtimeBillDTO.getTableName(), overtimeBillDTO.getId())
                        .stream()
                        .map(approvalHistoryDTO -> approvalHistoryDTO.organogramId)
                        .filter(organogramId -> !Objects.equals(organogramId, approvalHistoryOfUser.organogramId))
                        .collect(Collectors.toList());
        if(overtimeBillDTO.preparedOrgId > 0) {
            organogramIdsForNotification.add(overtimeBillDTO.preparedOrgId);
        }
        if (organogramIdsForNotification.isEmpty()) {
            logger.debug(String.format(
                    "Overtime_billDTO[id=%d] - modification notification NOT SENT - cause organogramIdsForNotification isEmpty",
                    overtimeBillDTO.iD
            ));
            return;
        }
        Office_unitsDTO officeUnitsDTO = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(overtimeBillDTO.officeUnitId);
        String officeNameEn = "", officeNameBn = "";
        if (officeUnitsDTO != null) {
            officeNameEn = officeUnitsDTO.unitNameEng;
            officeNameBn = officeUnitsDTO.unitNameBng;
        }
        String monthNameEn = overtimeBillDTO.getBillDateTypeText(true);
        String monthNameBn = overtimeBillDTO.getBillDateTypeText(false);

        String textEn = monthNameEn + " month's Overtime Bill of " + officeNameEn + " is modified by "
                        + approvalHistoryOfUser.nameEn + "," + approvalHistoryOfUser.orgNameEn;
        String textBn = monthNameBn + " মাসের, " + officeNameBn + "-এর অধিকাল ভাতার বিল "
                        + approvalHistoryOfUser.nameBn + "," + approvalHistoryOfUser.orgNameBn
                        + " পরিবর্তন করেছেন";
        String notificationMessage = textEn + "$" + textBn;
        String url = "Overtime_billServlet?actionType=view&ID=" + overtimeBillDTO.iD;
        sendNotification(organogramIdsForNotification, notificationMessage, url);
    }

    private void sendNotification(List<Long> organogramIds, String notificationMessage, String url) {
        if (organogramIds == null || organogramIds.size() == 0) return;

        Thread thread = new Thread(() -> {
            long currentTime = System.currentTimeMillis();
            organogramIds.forEach(organogramId -> pb_notificationsDAO.addPb_notifications(organogramId, currentTime, url, notificationMessage));
        });
        thread.setDaemon(true);
        thread.start();
    }
}
