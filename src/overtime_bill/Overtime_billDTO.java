package overtime_bill;

import allowance_employee_info.AllowanceEmployeeInfoDTO;
import allowance_employee_info.AllowanceEmployeeInfoRepository;
import bill_approval_history.ApprovableDTO;
import bill_approval_history.BillApprovalStatus;
import bill_approval_history.Bill_approval_historyDAO;
import bill_approval_history.Bill_approval_historyDTO;
import common.CustomException;
import employee_offices.EmployeeOfficeDTO;
import employee_offices.EmployeeOfficeRepository;
import employee_records.Employee_recordsDTO;
import employee_records.Employee_recordsRepository;
import jdk.nashorn.internal.runtime.options.Option;
import office_unit_organograms.OfficeUnitOrganograms;
import office_unit_organograms.OfficeUnitOrganogramsRepository;
import office_units.Office_unitsDTO;
import office_units.Office_unitsRepository;
import ot_bill_submission_config.OT_bill_submission_configDTO;
import overtime_allowance.Overtime_allowanceDTO;
import pb.CatRepository;
import pbReport.DateUtils;
import sessionmanager.SessionConstants;
import user.UserDTO;
import util.CommonDTO;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

import static overtime_bill.Overtime_billStatus.FINANCE_BILL_PREPARED;
import static overtime_bill.Overtime_billStatus.PENDING;

@SuppressWarnings({"Duplicates"})
public class Overtime_billDTO extends CommonDTO implements ApprovableDTO {
    public long otBillSubmissionConfigId = -1;
    public long billStartDate = SessionConstants.MIN_DATE;
    public long billEndDate = SessionConstants.MIN_DATE;
    public int overtimeBillTypeCat = 1;

    public long officeUnitId = -1;
    public long otEmployeeTypeId = -1;
    public long budgetOfficeId = -1;
    public long budgetMappingId = -1;
    public long economicSubCodeId = -1;

    public boolean isInPreviewStage = true;

    public long preparedOrgId;
    public String preparedNameBn;
    public String preparedOrgNameBn;
    public String preparedOfficeNameBn;
    public byte[] preparedSignature;
    public long preparedTime;

    public int financeBillStatus = PENDING.getValue();
    public int financeSerialNumber = -1;

    public long budgetRegisterId = -1;
    public long billRegisterId = -1;
    public long overtimeBillFinanceId = -1;
    public long employeeCount = 0;
    public long billAmount = 0;

    public String summaryEmployeeNameBn = "";
    public String summaryEmployeeDesignationBn = "";
    public String ordinanceText = "";
    public long kaDailyRate = 0;
    public long khaDailyRate = 0;
    public int revenueStampDeduction = 0;

    public int currentApprovalLevel;
    public int approvalStatus;
    public Long taskTypeId;

    public long modifiedBy = -1;
    public long insertedBy = -1;
    public long insertionTime = -1;

    public static Comparator<Overtime_billDTO> compareOfficeUnitId
            = Comparator.comparing(overtimeBillDTO -> overtimeBillDTO.officeUnitId);

    public String getBillDateTypeText(boolean isLangEn) {
        return String.format("%s (%s)", getBillDateRangeText(isLangEn), getBillTypeText(isLangEn));
    }

    public String getBillDateRangeText(boolean isLangEn) {
        return DateUtils.getDateRangeString(
                billStartDate,
                billEndDate,
                isLangEn
        );
    }

    public String getBillTypeText(boolean isLangEn) {
        return CatRepository.getInstance().getText(
                isLangEn,
                SessionConstants.OT_BILL_TYPE_CAT_DOMAIN_NAME,
                overtimeBillTypeCat
        );
    }

    public List<Long> getSortedBillDates() {
        return getSortedBillDates(this.billStartDate, this.billEndDate);
    }

    public static List<Long> getSortedBillDates(long billStartDate, long billEndDate) {
        List<Long> sortedBillDates = new ArrayList<>();
        boolean isBillDateRangeInvalid = billStartDate == SessionConstants.MIN_DATE
                                         || billEndDate == SessionConstants.MIN_DATE
                                         || billStartDate > billEndDate;
        if (isBillDateRangeInvalid) {
            return sortedBillDates;
        }
        long current = billStartDate;
        while (current <= billEndDate) {
            sortedBillDates.add(current);
            current += DateUtils.ONE_DAY_IN_MILLIS;
        }
        return sortedBillDates;
    }

    public boolean isApproved() {
        if (isInPreviewStage) {
            return false;
        }
        return approvalStatus == BillApprovalStatus.APPROVED.getValue();
    }

    public boolean hasFinancePreparedBill() {
        return financeBillStatus == FINANCE_BILL_PREPARED.getValue();
    }

    public boolean isEditable(UserDTO userDTO, String language) {
        try {
            checkEditabilityOrThrowException(userDTO, language);
            return true;
        } catch (IllegalAccessException ex) {
            return false;
        }
    }

    public boolean isDeletableByUser(UserDTO userDTO) {
        if (isInPreviewStage) {
            return true;
        }
        return Overtime_billServlet.isAllowedToSeeAllOfficeBill(userDTO);
    }

    public Optional<Bill_approval_historyDTO> checkEditabilityOrThrowException(UserDTO userDTO, String language, boolean doApprovalHistoryCheck) throws IllegalAccessException {
        boolean isLangEng = "english".equalsIgnoreCase(language);
        if (isApproved()) {
            throw new IllegalAccessException(
                    isLangEng ? "Approval has been completed. Not Editable Now."
                              : "অনুমোদন সম্পন্ন হয়েছে এখন পরিবর্তনযোগ্য নয়"
            );
        }
        if (hasFinancePreparedBill()) {
            throw new IllegalAccessException(
                    isLangEng ? "Finance already prepared bill. Can not edit!"
                              : "অর্থশাখা বিল প্রস্তুত করে ফেলেছে। এখন এডিটযোগ্য নয়!"
            );
        }

        if (isInPreviewStage) {
            return Optional.empty();
        }

        EmployeeOfficeDTO employeeOfficeDTO = EmployeeOfficeRepository.getInstance().getByEmployeeRecordIdIsDefault(userDTO.employee_record_id);
        if (employeeOfficeDTO == null) {
            throw new IllegalAccessException(
                    isLangEng ? "You are not allowed to edit"
                              : "আপনার এডিটের অনুমতি নেই"
            );
        }
        if (doApprovalHistoryCheck) {
            Optional<Bill_approval_historyDTO> optionalApprovalHistory =
                    Bill_approval_historyDAO
                            .getInstance()
                            .findByLevel(getTableName(), iD, currentApprovalLevel)
                            .stream()
                            .filter(approvalHistoryDTO -> approvalHistoryDTO.isThisUsersHistory(userDTO))
                            .findAny();
            if(optionalApprovalHistory.isPresent()) {
                return optionalApprovalHistory;
            }
            throw new IllegalAccessException(isLangEng ? "You are not allowed to approve" : "আপনার অনুমোদন করার অনুমতি নেই");
        }
        return Optional.empty();
    }

    public Optional<Bill_approval_historyDTO> checkEditabilityOrThrowException(UserDTO userDTO, String language) throws IllegalAccessException {
        return checkEditabilityOrThrowException(userDTO, language, true);
    }

    public void setPreparedInfo(UserDTO userDTO, String language)  {
        boolean isLangEng = "english".equalsIgnoreCase(language);
        if (isApproved()) {
            throw new CustomException(
                    isLangEng ? "Approval has been completed. Can Not Prepare Bill Now."
                              : "অনুমোদন সম্পন্ন হয়েছে এখন বিল প্রস্তুতযোগ্য নয়"
            );
        }
        if (hasFinancePreparedBill()) {
            throw new CustomException(
                    isLangEng ? "Finance already prepared bill. Can Not Prepare Bill Now!"
                              : "অর্থশাখা বিল প্রস্তুত করে ফেলেছে। এখন বিল প্রস্তুতযোগ্য নয়!"
            );
        }

        if (!isInPreviewStage) {
            throw new CustomException(
                    isLangEng ? "Bill has already been prepared."
                              : "বিল ইতো মধ্যে প্রস্তুত করা হয়ে গেছে!"
            );
        }

        isInPreviewStage = false;
        preparedOrgId = -1;
        preparedNameBn = "(এডমিন)";
        preparedOfficeNameBn = "(এডমিন)";
        preparedOrgNameBn = "(এডমিন)";
        preparedSignature = null;
        preparedTime = lastModificationTime = System.currentTimeMillis();
        modifiedBy = userDTO.ID;

        EmployeeOfficeDTO employeeOfficeDTO = EmployeeOfficeRepository.getInstance().getByEmployeeRecordIdIsDefault(userDTO.employee_record_id);
        if (employeeOfficeDTO == null) return;

        Employee_recordsDTO employeeRecords = Employee_recordsRepository.getInstance().getById(employeeOfficeDTO.employeeRecordId);
        if (employeeRecords != null) {
            if(employeeRecords.employeeClass == 4) {
                throw new CustomException(
                        isLangEng ? "4th Class Employee can not submit bill"
                                  : "৪র্থ শ্রেণির কর্মচারী বিল জমা দিতে পারবেন না"
                );
            }
            preparedNameBn = employeeRecords.nameBng;
            preparedSignature = employeeRecords.signature;
            if(preparedSignature == null || preparedSignature.length == 0) {
                throw new CustomException(
                        isLangEng ? "Please upload your signature!"
                                  : "আপনার স্বাক্ষর আপলোড করুন!"
                );
            }
        }
        Office_unitsDTO officeUnits = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(employeeOfficeDTO.officeUnitId);
        if (officeUnits != null) {
            preparedOfficeNameBn = officeUnits.unitNameBng;
        }
        OfficeUnitOrganograms officeUnitOrganograms = OfficeUnitOrganogramsRepository.getInstance().getById(employeeOfficeDTO.officeUnitOrganogramId);
        if (officeUnitOrganograms != null) {
            preparedOrgNameBn = officeUnitOrganograms.designation_bng;
            preparedOrgId = officeUnitOrganograms.id;
        }
    }

    public void setDataFromSubmissionConfig(OT_bill_submission_configDTO submissionConfigDTO) {
        otBillSubmissionConfigId = submissionConfigDTO.iD;
        billStartDate = submissionConfigDTO.billStartDate;
        billEndDate = submissionConfigDTO.billEndDate;
        overtimeBillTypeCat = submissionConfigDTO.overtimeBillTypeCat;
        kaDailyRate = submissionConfigDTO.kaDailyRate;
        khaDailyRate = submissionConfigDTO.khaDailyRate;
        revenueStampDeduction = submissionConfigDTO.revenueStampDeduction;
        ordinanceText = submissionConfigDTO.ordinanceText;
    }

    public void setSummaryEmployeeInfo(List<Overtime_allowanceDTO> allowanceDTOs) {
        if (allowanceDTOs == null || allowanceDTOs.isEmpty()) {
            return;
        }
        Overtime_allowanceDTO firstEmployeeData = allowanceDTOs.stream()
                                                               .min(Overtime_allowanceDTO.orderingComparator)
                                                               .get();
        AllowanceEmployeeInfoDTO allowanceEmployeeInfoDTO =
                AllowanceEmployeeInfoRepository.getInstance().getById(firstEmployeeData.allowanceEmployeeInfoId);
        if (allowanceEmployeeInfoDTO != null) {
            summaryEmployeeNameBn = allowanceEmployeeInfoDTO.nameBn;
            summaryEmployeeDesignationBn = allowanceEmployeeInfoDTO.organogramNameBn;
        }
    }

    @Override
    public String toString() {
        return "Overtime_billDTO{" +
               "iD=" + iD +
               ", otBillSubmissionConfigId=" + otBillSubmissionConfigId +
               ", billStartDate=" + billStartDate +
               ", billEndDate=" + billEndDate +
               ", overtimeBillTypeCat=" + overtimeBillTypeCat +
               ", officeUnitId=" + officeUnitId +
               ", otEmployeeTypeId=" + otEmployeeTypeId +
               ", budgetOfficeId=" + budgetOfficeId +
               ", isInPreviewStage=" + isInPreviewStage +
               ", financeBillStatus=" + financeBillStatus +
               ", budgetRegisterId=" + budgetRegisterId +
               ", billRegisterId=" + billRegisterId +
               ", employeeCount=" + employeeCount +
               ", billAmount=" + billAmount +
               ", kaDailyRate=" + kaDailyRate +
               ", khaDailyRate=" + khaDailyRate +
               ", revenueStampDeduction=" + revenueStampDeduction +
               ", currentApprovalLevel=" + currentApprovalLevel +
               ", approvalStatus=" + approvalStatus +
               '}';
    }

    @Override
    public String getTableName() {
        return Overtime_billDAO.getInstance().getTableName();
    }

    @Override
    public Long getId() {
        return iD;
    }

    @Override
    public Integer getLevel() {
        return currentApprovalLevel;
    }

    @Override
    public Long getOfficeUnitId() {
        return officeUnitId;
    }

    @Override
    public Long getTaskTypeId() {
        return taskTypeId;
    }
}