package overtime_bill;

public enum BillStatusView {
    NOT_SUBMITTED("Bill not yet submitted", "বিল এখনো জমা দেয়া হয়নি"),
    SUBMITTED_WITHOUT_EMPLOYEE("Bill submitted without you", "আপনাকে ছাড়া বিল জমা দেওয়া হয়েছে"),
    FINANCE_PREPARED("Finance has prepared bill", "অর্থ বিল প্রস্তুত করেছে"),
    SENT_TO_FINANCE("Sent to Finance", "অর্থশাখায় পাঠানো হয়েছে"),
    WAITING_FOR_APPROVAL("Waiting for approval%s", "%sঅনুমোদনের অপেক্ষায়");

    public final String messageEn, messageBn;

    BillStatusView(String messageEn, String messageBn) {
        this.messageEn = messageEn;
        this.messageBn = messageBn;
    }
}
