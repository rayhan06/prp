create table overtime_bill
(
    ID                         bigint primary key,
    month_year                 bigint        default -62135791200000,
    is_in_preview_stage        tinyint       default 0,
    office_unit_id             bigint,
    ot_employee_type_id        bigint,
    budget_office_id           bigint,
    finance_bill_status        int           default 0,
    budget_register_id         bigint,
    bill_register_id           bigint,
    employee_count             bigint,
    summary_emp_name_bn        varchar(1024) default '',
    summary_emp_designation_bn varchar(1024) default '',
    bill_amount                bigint,
    prepared_org_id            bigint        default -1,
    prepared_name_bn           varchar(1024) default '',
    prepared_org_name_bn       varchar(1024) default '',
    prepared_office_name_bn    varchar(1024) default '',
    prepared_signature         longblob,
    prepared_time              bigint        default -62135791200000,
    ordinance_text             varchar(4096) default '',
    ka_daily_rate              bigint,
    kha_daily_rate             bigint,
    revenue_stamp_deduction    int           default 0,
    current_approval_level     int,
    approval_status            int,
    task_type_id               bigint,

    modified_by                bigint,
    lastModificationTime       bigint        default -62135791200000,
    inserted_by                bigint,
    insertion_time             bigint        default -62135791200000,
    isDeleted                  int           default 0
)
    ENGINE = MyISAM
    DEFAULT CHARSET = utf8
    COLLATE = utf8_unicode_ci;

INSERT INTO vb_sequencer (table_name, next_id, table_LastModificationTime)
VALUES ('overtime_bill', 1, 0);