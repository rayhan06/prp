package overtime_bill;

import overtime_allowance.Overtime_allowanceModel;

import java.util.ArrayList;
import java.util.List;

public class Overtime_billTableData {
    public String errorMessage = null;
    public boolean isEditable = false;
    public List<Long> sortedBillDates = new ArrayList<>();
    public List<Overtime_allowanceModel> allowanceModels = new ArrayList<>();
    public List<Long> deletedOvertimeAllowanceIds = new ArrayList<>();
}
