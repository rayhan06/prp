package ultrasono_type;
import java.util.*; 
import util.*; 


public class Ultrasono_typeDTO extends CommonDTO
{

	public int genderCat = -1;
    public String nameEn = "";
    public String nameBn = "";
    public String heading = "";
    public String details = "";
	
	
    @Override
	public String toString() {
            return "$Ultrasono_typeDTO[" +
            " iD = " + iD +
            " genderCat = " + genderCat +
            " nameEn = " + nameEn +
            " nameBn = " + nameBn +
            " heading = " + heading +
            " details = " + details +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}