package ultrasono_type;
import java.util.*; 
import util.*;


public class Ultrasono_typeMAPS extends CommonMaps
{	
	public Ultrasono_typeMAPS(String tableName)
	{
		

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("genderCat".toLowerCase(), "genderCat".toLowerCase());
		java_DTO_map.put("nameEn".toLowerCase(), "nameEn".toLowerCase());
		java_DTO_map.put("nameBn".toLowerCase(), "nameBn".toLowerCase());
		java_DTO_map.put("heading".toLowerCase(), "heading".toLowerCase());
		java_DTO_map.put("details".toLowerCase(), "details".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("gender_cat".toLowerCase(), "genderCat".toLowerCase());
		java_SQL_map.put("name_en".toLowerCase(), "nameEn".toLowerCase());
		java_SQL_map.put("name_bn".toLowerCase(), "nameBn".toLowerCase());
		java_SQL_map.put("heading".toLowerCase(), "heading".toLowerCase());
		java_SQL_map.put("details".toLowerCase(), "details".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Gender".toLowerCase(), "genderCat".toLowerCase());
		java_Text_map.put("English Name".toLowerCase(), "nameEn".toLowerCase());
		java_Text_map.put("Bangla Name".toLowerCase(), "nameBn".toLowerCase());
		java_Text_map.put("Heading".toLowerCase(), "heading".toLowerCase());
		java_Text_map.put("Details".toLowerCase(), "details".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}