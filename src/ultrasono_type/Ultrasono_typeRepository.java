package ultrasono_type;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class Ultrasono_typeRepository implements Repository {
	Ultrasono_typeDAO ultrasono_typeDAO = null;
	
	public void setDAO(Ultrasono_typeDAO ultrasono_typeDAO)
	{
		this.ultrasono_typeDAO = ultrasono_typeDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Ultrasono_typeRepository.class);
	Map<Long, Ultrasono_typeDTO>mapOfUltrasono_typeDTOToiD;
	Map<Integer, Set<Ultrasono_typeDTO> >mapOfUltrasono_typeDTOTogenderCat;
	Map<String, Set<Ultrasono_typeDTO> >mapOfUltrasono_typeDTOTonameEn;
	Map<String, Set<Ultrasono_typeDTO> >mapOfUltrasono_typeDTOTonameBn;
	Map<String, Set<Ultrasono_typeDTO> >mapOfUltrasono_typeDTOToheading;
	Map<String, Set<Ultrasono_typeDTO> >mapOfUltrasono_typeDTOTodetails;
	Map<Long, Set<Ultrasono_typeDTO> >mapOfUltrasono_typeDTOTolastModificationTime;


	static Ultrasono_typeRepository instance = null;  
	private Ultrasono_typeRepository(){
		mapOfUltrasono_typeDTOToiD = new ConcurrentHashMap<>();
		mapOfUltrasono_typeDTOTogenderCat = new ConcurrentHashMap<>();
		mapOfUltrasono_typeDTOTonameEn = new ConcurrentHashMap<>();
		mapOfUltrasono_typeDTOTonameBn = new ConcurrentHashMap<>();
		mapOfUltrasono_typeDTOToheading = new ConcurrentHashMap<>();
		mapOfUltrasono_typeDTOTodetails = new ConcurrentHashMap<>();
		mapOfUltrasono_typeDTOTolastModificationTime = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Ultrasono_typeRepository getInstance(){
		if (instance == null){
			instance = new Ultrasono_typeRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(ultrasono_typeDAO == null)
		{
			return;
		}
		try {
			List<Ultrasono_typeDTO> ultrasono_typeDTOs = ultrasono_typeDAO.getAllUltrasono_type(reloadAll);
			for(Ultrasono_typeDTO ultrasono_typeDTO : ultrasono_typeDTOs) {
				Ultrasono_typeDTO oldUltrasono_typeDTO = getUltrasono_typeDTOByID(ultrasono_typeDTO.iD);
				if( oldUltrasono_typeDTO != null ) {
					mapOfUltrasono_typeDTOToiD.remove(oldUltrasono_typeDTO.iD);
				
					if(mapOfUltrasono_typeDTOTogenderCat.containsKey(oldUltrasono_typeDTO.genderCat)) {
						mapOfUltrasono_typeDTOTogenderCat.get(oldUltrasono_typeDTO.genderCat).remove(oldUltrasono_typeDTO);
					}
					if(mapOfUltrasono_typeDTOTogenderCat.get(oldUltrasono_typeDTO.genderCat).isEmpty()) {
						mapOfUltrasono_typeDTOTogenderCat.remove(oldUltrasono_typeDTO.genderCat);
					}
					
					if(mapOfUltrasono_typeDTOTonameEn.containsKey(oldUltrasono_typeDTO.nameEn)) {
						mapOfUltrasono_typeDTOTonameEn.get(oldUltrasono_typeDTO.nameEn).remove(oldUltrasono_typeDTO);
					}
					if(mapOfUltrasono_typeDTOTonameEn.get(oldUltrasono_typeDTO.nameEn).isEmpty()) {
						mapOfUltrasono_typeDTOTonameEn.remove(oldUltrasono_typeDTO.nameEn);
					}
					
					if(mapOfUltrasono_typeDTOTonameBn.containsKey(oldUltrasono_typeDTO.nameBn)) {
						mapOfUltrasono_typeDTOTonameBn.get(oldUltrasono_typeDTO.nameBn).remove(oldUltrasono_typeDTO);
					}
					if(mapOfUltrasono_typeDTOTonameBn.get(oldUltrasono_typeDTO.nameBn).isEmpty()) {
						mapOfUltrasono_typeDTOTonameBn.remove(oldUltrasono_typeDTO.nameBn);
					}
					
					if(mapOfUltrasono_typeDTOToheading.containsKey(oldUltrasono_typeDTO.heading)) {
						mapOfUltrasono_typeDTOToheading.get(oldUltrasono_typeDTO.heading).remove(oldUltrasono_typeDTO);
					}
					if(mapOfUltrasono_typeDTOToheading.get(oldUltrasono_typeDTO.heading).isEmpty()) {
						mapOfUltrasono_typeDTOToheading.remove(oldUltrasono_typeDTO.heading);
					}
					
					if(mapOfUltrasono_typeDTOTodetails.containsKey(oldUltrasono_typeDTO.details)) {
						mapOfUltrasono_typeDTOTodetails.get(oldUltrasono_typeDTO.details).remove(oldUltrasono_typeDTO);
					}
					if(mapOfUltrasono_typeDTOTodetails.get(oldUltrasono_typeDTO.details).isEmpty()) {
						mapOfUltrasono_typeDTOTodetails.remove(oldUltrasono_typeDTO.details);
					}
					
					if(mapOfUltrasono_typeDTOTolastModificationTime.containsKey(oldUltrasono_typeDTO.lastModificationTime)) {
						mapOfUltrasono_typeDTOTolastModificationTime.get(oldUltrasono_typeDTO.lastModificationTime).remove(oldUltrasono_typeDTO);
					}
					if(mapOfUltrasono_typeDTOTolastModificationTime.get(oldUltrasono_typeDTO.lastModificationTime).isEmpty()) {
						mapOfUltrasono_typeDTOTolastModificationTime.remove(oldUltrasono_typeDTO.lastModificationTime);
					}
					
					
				}
				if(ultrasono_typeDTO.isDeleted == 0) 
				{
					
					mapOfUltrasono_typeDTOToiD.put(ultrasono_typeDTO.iD, ultrasono_typeDTO);
				
					if( ! mapOfUltrasono_typeDTOTogenderCat.containsKey(ultrasono_typeDTO.genderCat)) {
						mapOfUltrasono_typeDTOTogenderCat.put(ultrasono_typeDTO.genderCat, new HashSet<>());
					}
					mapOfUltrasono_typeDTOTogenderCat.get(ultrasono_typeDTO.genderCat).add(ultrasono_typeDTO);
					
					if( ! mapOfUltrasono_typeDTOTonameEn.containsKey(ultrasono_typeDTO.nameEn)) {
						mapOfUltrasono_typeDTOTonameEn.put(ultrasono_typeDTO.nameEn, new HashSet<>());
					}
					mapOfUltrasono_typeDTOTonameEn.get(ultrasono_typeDTO.nameEn).add(ultrasono_typeDTO);
					
					if( ! mapOfUltrasono_typeDTOTonameBn.containsKey(ultrasono_typeDTO.nameBn)) {
						mapOfUltrasono_typeDTOTonameBn.put(ultrasono_typeDTO.nameBn, new HashSet<>());
					}
					mapOfUltrasono_typeDTOTonameBn.get(ultrasono_typeDTO.nameBn).add(ultrasono_typeDTO);
					
					if( ! mapOfUltrasono_typeDTOToheading.containsKey(ultrasono_typeDTO.heading)) {
						mapOfUltrasono_typeDTOToheading.put(ultrasono_typeDTO.heading, new HashSet<>());
					}
					mapOfUltrasono_typeDTOToheading.get(ultrasono_typeDTO.heading).add(ultrasono_typeDTO);
					
					if( ! mapOfUltrasono_typeDTOTodetails.containsKey(ultrasono_typeDTO.details)) {
						mapOfUltrasono_typeDTOTodetails.put(ultrasono_typeDTO.details, new HashSet<>());
					}
					mapOfUltrasono_typeDTOTodetails.get(ultrasono_typeDTO.details).add(ultrasono_typeDTO);
					
					if( ! mapOfUltrasono_typeDTOTolastModificationTime.containsKey(ultrasono_typeDTO.lastModificationTime)) {
						mapOfUltrasono_typeDTOTolastModificationTime.put(ultrasono_typeDTO.lastModificationTime, new HashSet<>());
					}
					mapOfUltrasono_typeDTOTolastModificationTime.get(ultrasono_typeDTO.lastModificationTime).add(ultrasono_typeDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Ultrasono_typeDTO> getUltrasono_typeList() {
		List <Ultrasono_typeDTO> ultrasono_types = new ArrayList<Ultrasono_typeDTO>(this.mapOfUltrasono_typeDTOToiD.values());
		return ultrasono_types;
	}
	
	
	public Ultrasono_typeDTO getUltrasono_typeDTOByID( long ID){
		return mapOfUltrasono_typeDTOToiD.get(ID);
	}
	
	
	public List<Ultrasono_typeDTO> getUltrasono_typeDTOBygender_cat(int gender_cat) {
		return new ArrayList<>( mapOfUltrasono_typeDTOTogenderCat.getOrDefault(gender_cat,new HashSet<>()));
	}
	
	
	public List<Ultrasono_typeDTO> getUltrasono_typeDTOByname_en(String name_en) {
		return new ArrayList<>( mapOfUltrasono_typeDTOTonameEn.getOrDefault(name_en,new HashSet<>()));
	}
	
	
	public List<Ultrasono_typeDTO> getUltrasono_typeDTOByname_bn(String name_bn) {
		return new ArrayList<>( mapOfUltrasono_typeDTOTonameBn.getOrDefault(name_bn,new HashSet<>()));
	}
	
	
	public List<Ultrasono_typeDTO> getUltrasono_typeDTOByheading(String heading) {
		return new ArrayList<>( mapOfUltrasono_typeDTOToheading.getOrDefault(heading,new HashSet<>()));
	}
	
	
	public List<Ultrasono_typeDTO> getUltrasono_typeDTOBydetails(String details) {
		return new ArrayList<>( mapOfUltrasono_typeDTOTodetails.getOrDefault(details,new HashSet<>()));
	}
	
	
	public List<Ultrasono_typeDTO> getUltrasono_typeDTOBylastModificationTime(long lastModificationTime) {
		return new ArrayList<>( mapOfUltrasono_typeDTOTolastModificationTime.getOrDefault(lastModificationTime,new HashSet<>()));
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "ultrasono_type";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


