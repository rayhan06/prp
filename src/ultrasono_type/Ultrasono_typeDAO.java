package ultrasono_type;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;
import sessionmanager.SessionConstants;
import java.sql.SQLException;


import org.apache.log4j.Logger;

import dbm.*;

import repository.RepositoryManager;

import util.*;
import pb.*;
import user.UserDTO;

public class Ultrasono_typeDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());
	

	
	public Ultrasono_typeDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";
		commonMaps = new Ultrasono_typeMAPS(tableName);
		columnNames = new String[] 
		{
			"ID",
			"gender_cat",
			"name_en",
			"name_bn",
			"heading",
			"details",
			"isDeleted",
			"lastModificationTime"
		};
	}
	
	public Ultrasono_typeDAO()
	{
		this("ultrasono_type");		
	}
	
	public void setSearchColumn(Ultrasono_typeDTO ultrasono_typeDTO)
	{
		ultrasono_typeDTO.searchColumn = "";
		ultrasono_typeDTO.searchColumn += CatDAO.getName("English", "gender", ultrasono_typeDTO.genderCat) + " " + CatDAO.getName("Bangla", "gender", ultrasono_typeDTO.genderCat) + " ";
		ultrasono_typeDTO.searchColumn += ultrasono_typeDTO.nameEn + " ";
		ultrasono_typeDTO.searchColumn += ultrasono_typeDTO.nameBn + " ";
		ultrasono_typeDTO.searchColumn += ultrasono_typeDTO.heading + " ";
		ultrasono_typeDTO.searchColumn += ultrasono_typeDTO.details + " ";
		
	}
	
	public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException
	{
		Ultrasono_typeDTO ultrasono_typeDTO = (Ultrasono_typeDTO)commonDTO;
		int index = 1;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(ultrasono_typeDTO);
		if(isInsert)
		{
			ps.setObject(index++,ultrasono_typeDTO.iD);
		}
		ps.setObject(index++,ultrasono_typeDTO.genderCat);
		ps.setObject(index++,ultrasono_typeDTO.nameEn);
		ps.setObject(index++,ultrasono_typeDTO.nameBn);
		ps.setObject(index++,ultrasono_typeDTO.heading);
		ps.setObject(index++,ultrasono_typeDTO.details);
		if(isInsert)
		{
			ps.setObject(index++, 0);
			ps.setObject(index++, lastModificationTime);
		}
	}
	
	public void get(Ultrasono_typeDTO ultrasono_typeDTO, ResultSet rs) throws SQLException
	{
		ultrasono_typeDTO.iD = rs.getLong("ID");
		ultrasono_typeDTO.genderCat = rs.getInt("gender_cat");
		ultrasono_typeDTO.nameEn = rs.getString("name_en");
		ultrasono_typeDTO.nameBn = rs.getString("name_bn");
		ultrasono_typeDTO.heading = rs.getString("heading");
		ultrasono_typeDTO.details = rs.getString("details");
		ultrasono_typeDTO.isDeleted = rs.getInt("isDeleted");
		ultrasono_typeDTO.lastModificationTime = rs.getLong("lastModificationTime");
	}
	
	
	
	
		
	

	//need another getter for repository
	public Ultrasono_typeDTO getDTOByID (long ID)
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Ultrasono_typeDTO ultrasono_typeDTO = null;
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
			
            sql += " WHERE ID=" + ID;
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			if(rs.next()){
				ultrasono_typeDTO = new Ultrasono_typeDTO();

				get(ultrasono_typeDTO, rs);

			}			
			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return ultrasono_typeDTO;
	}
	
	public List<Ultrasono_typeDTO> getDTOsByGenderCat(int genderCat){
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Ultrasono_typeDTO ultrasono_typeDTO = null;
		List<Ultrasono_typeDTO> ultrasono_typeDTOList = new ArrayList<>();
		
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
            
            sql += " WHERE isDeleted = 0 and gender_cat = " + genderCat;

			
			printSql(sql);
			
			logger.debug("sql " + sql);
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				ultrasono_typeDTO = new Ultrasono_typeDTO();
				get(ultrasono_typeDTO, rs);
				System.out.println("got this DTO: " + ultrasono_typeDTO);
				
				ultrasono_typeDTOList.add(ultrasono_typeDTO);

			}			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return ultrasono_typeDTOList;
	
	}
	
	
	
	
	public List<Ultrasono_typeDTO> getDTOs(Collection recordIDs){
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Ultrasono_typeDTO ultrasono_typeDTO = null;
		List<Ultrasono_typeDTO> ultrasono_typeDTOList = new ArrayList<>();
		if(recordIDs.isEmpty()){
			return ultrasono_typeDTOList;
		}
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
            
            sql += " WHERE ID IN ( ";

			for(int i = 0;i<recordIDs.size();i++){
				if(i!=0){
					sql+=",";
				}
				sql+=((ArrayList)recordIDs).get(i);
			}
			sql+=")  order by lastModificationTime desc";
			
			printSql(sql);
			
			logger.debug("sql " + sql);
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				ultrasono_typeDTO = new Ultrasono_typeDTO();
				get(ultrasono_typeDTO, rs);
				System.out.println("got this DTO: " + ultrasono_typeDTO);
				
				ultrasono_typeDTOList.add(ultrasono_typeDTO);

			}			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return ultrasono_typeDTOList;
	
	}
	
	

	
	
	
	//add repository
	public List<Ultrasono_typeDTO> getAllUltrasono_type (boolean isFirstReload)
    {
		List<Ultrasono_typeDTO> ultrasono_typeDTOList = new ArrayList<>();

		String sql = "SELECT * FROM ultrasono_type";
		sql += " WHERE ";
	

		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by ultrasono_type.lastModificationTime desc";
		printSql(sql);
		
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;

		
		try{
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();
			rs = stmt.executeQuery(sql);
			

			while(rs.next()){
				Ultrasono_typeDTO ultrasono_typeDTO = new Ultrasono_typeDTO();
				get(ultrasono_typeDTO, rs);
				
				ultrasono_typeDTOList.add(ultrasono_typeDTO);
			}			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}

		return ultrasono_typeDTOList;
    }

	
	public List<Ultrasono_typeDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO)
	{
		return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
	}

	
	public List<Ultrasono_typeDTO> getDTOs(Hashtable  p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
			String filter, boolean tableHasJobCat)
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		List<Ultrasono_typeDTO> ultrasono_typeDTOList = new ArrayList<>();

		try{
			
			String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat);
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				Ultrasono_typeDTO ultrasono_typeDTO = new Ultrasono_typeDTO();
				get(ultrasono_typeDTO, rs);
				
				ultrasono_typeDTOList.add(ultrasono_typeDTO);
			}					
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return ultrasono_typeDTOList;
	
	}
	public String getSqlWithSearchCriteriaForNormalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
			UserDTO userDTO, String filter, boolean tableHasJobCat)
    {
		boolean viewAll = false;
		
		if(p_searchCriteria != null && p_searchCriteria.get("ViewAll") != null)
		{
			System.out.println("ViewAll = " + p_searchCriteria.get("ViewAll"));
			viewAll = true;
		}
		else
		{
			System.out.println("ViewAll is null ");
		}
		
		String sql = "SELECT ";
		if(category == GETIDS)
		{
			sql += " " + tableName + ".ID as ID ";
		}
		else if(category == GETCOUNT)
		{
			sql += " count( " + tableName + ".ID) as countID ";
		}
		else if(category == GETDTOS)
		{
			sql += " distinct " + tableName + ".* ";
		}
		sql += "FROM " + tableName + " ";
		sql += joinSQL;
		

		
		String AnyfieldSql = "";
		String AllFieldSql = "";
		
		if(p_searchCriteria != null)
		{
						
			
			Enumeration names = p_searchCriteria.keys();
			String str, value;
			
			AnyfieldSql = "(";
			
			if(p_searchCriteria.get("AnyField")!= null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase(""))
			{
				AnyfieldSql+= tableName + ".name_en like '%" + p_searchCriteria.get("AnyField").toString() + "%'";
				AnyfieldSql+= " or " + tableName + ".heading like '%" + p_searchCriteria.get("AnyField").toString() + "%'";
			}
			AnyfieldSql += ")";
			System.out.println("AnyfieldSql = " + AnyfieldSql);
			
			AllFieldSql = "(";
			int i = 0;
			while(names.hasMoreElements())
			{				
				str = (String) names.nextElement();
				value = (String)p_searchCriteria.get(str);
		        System.out.println(str + ": " + value);
				if(value != null && !value.equalsIgnoreCase("") && (
						 str.equals("gender_cat")
						|| str.equals("name_en")
						|| str.equals("name_bn")
						|| str.equals("heading")
						|| str.equals("details")
				)
						
				)
				{
					if(p_searchCriteria.get(str).equals("any"))
					{
						continue;
					}

					if( i > 0)
					{
						AllFieldSql+= " AND  ";
					}
					
					 if(str.equals("gender_cat"))
					{
						AllFieldSql += "" + tableName + ".gender_cat = " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("name_en"))
					{
						AllFieldSql += "" + tableName + ".name_en like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("name_bn"))
					{
						AllFieldSql += "" + tableName + ".name_bn like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("heading"))
					{
						AllFieldSql += "" + tableName + ".heading like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("details"))
					{
						AllFieldSql += "" + tableName + ".details like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					
					
				}
			}
			
			AllFieldSql += ")";
			System.out.println("AllFieldSql = " + AllFieldSql);
			
			
		}
		
		
		sql += " WHERE ";
		
		sql += " (" + tableName + ".isDeleted = 0 ";			
		sql += ")";
		
		
		if(!filter.equalsIgnoreCase(""))
		{
			sql += " and " + filter + " ";
		}
		
		if(!AnyfieldSql.equals("()") && !AnyfieldSql.equals(""))
		{
			sql += " AND " + AnyfieldSql;
			
		}
		if(!AllFieldSql.equals("()") && !AllFieldSql.equals(""))
		{			
			sql += " AND " + AllFieldSql;
		}
		
	
			
		sql += " order by " + tableName + ".lastModificationTime desc ";

		printSql(sql);
		
		if(limit >= 0)
		{
			sql += " limit " + limit;
		}
		if(offset >= 0)
		{
			sql += " offset " + offset;
		}
		
		System.out.println("-------------- sql = " + sql);
		
		return sql;
    }
	
	public String getSqlWithSearchCriteria(Hashtable p_searchCriteria, int limit, int offset, int category,
			boolean isPermanentTable, UserDTO userDTO, String filter, boolean tableHasJobCat)
    {		
		return getSqlWithSearchCriteriaForNormalSearch(p_searchCriteria, limit, offset, category, userDTO, filter, tableHasJobCat);	
    }
				
}
	