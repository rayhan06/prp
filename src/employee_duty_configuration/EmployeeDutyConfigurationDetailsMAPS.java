package employee_duty_configuration;
import java.util.*; 
import util.*;


public class EmployeeDutyConfigurationDetailsMAPS extends CommonMaps
{	
	public EmployeeDutyConfigurationDetailsMAPS(String tableName)
	{
		


		java_SQL_map.put("employee_duty_configuration_id".toLowerCase(), "employeeDutyConfigurationId".toLowerCase());
		java_SQL_map.put("duty_on".toLowerCase(), "dutyOn".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Employee Duty Configuration Id".toLowerCase(), "employeeDutyConfigurationId".toLowerCase());
		java_Text_map.put("Duty On".toLowerCase(), "dutyOn".toLowerCase());
		java_Text_map.put("Search Column".toLowerCase(), "searchColumn".toLowerCase());
		java_Text_map.put("Inserted By Organogram Id".toLowerCase(), "insertedByOrganogramId".toLowerCase());
		java_Text_map.put("Last Modifier User".toLowerCase(), "lastModifierUser".toLowerCase());
		java_Text_map.put("Insertion Date".toLowerCase(), "insertionDate".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}