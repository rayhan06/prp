package employee_duty_configuration;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.sql.SQLException;
import common.CommonDAOService;
import common.ConnectionAndStatementUtil;

import org.apache.log4j.Logger;
import util.*;
import pb.*;

public class Employee_duty_configurationDAO  implements CommonDAOService<Employee_duty_configurationDTO>
{
	
	Logger logger = Logger.getLogger(getClass());
	
	private static String addQuery = "";
	private static String updateQuery = "";
	private String[] columnNames = null;

	private Employee_duty_configurationDAO()
	{
		columnNames = new String[] 
		{
			"ID",
			"is_office",
			"employee_id",
			"office_id",
			"duty_cat",
			"is_continuation",
			"start_date",
			"end_date",
			"employees",
			"search_column",
			"inserted_by_organogram_id",
			"last_modifier_user",
			"insertion_date",
			"isDeleted",
			"lastModificationTime"
		};
		updateQuery = getUpdateQuery(columnNames);
        addQuery = getAddQuery(columnNames);
		
		searchMap.put("duty_cat"," and (duty_cat = ?)");
		searchMap.put("start_date_start"," and (start_date >= ?)");
		searchMap.put("start_date_end"," and (start_date <= ?)");
		searchMap.put("end_date_start"," and (end_date >= ?)");
		searchMap.put("end_date_end"," and (end_date <= ?)");
		searchMap.put("insertion_date_start"," and (insertion_date >= ?)");
		searchMap.put("insertion_date_end"," and (insertion_date <= ?)");
		searchMap.put("AnyField"," and (search_column like ?)");
	}

	private final Map<String,String> searchMap = new HashMap<>();

    private static class LazyLoader
    {
    	static final Employee_duty_configurationDAO INSTANCE = new Employee_duty_configurationDAO();
	}

	public static Employee_duty_configurationDAO getInstance()
	{
    	return LazyLoader.INSTANCE;
	}
	
	public void setSearchColumn(Employee_duty_configurationDTO employee_duty_configurationDTO)
	{
		employee_duty_configurationDTO.searchColumn = "";
		employee_duty_configurationDTO.searchColumn += CatDAO.getName("English", "duty", employee_duty_configurationDTO.dutyCat) + " " + CatDAO.getName("Bangla", "duty", employee_duty_configurationDTO.dutyCat) + " ";
	}
	
	@Override
	public void set(PreparedStatement ps, Employee_duty_configurationDTO employee_duty_configurationDTO, boolean isInsert) throws SQLException
	{
		int index = 0;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(employee_duty_configurationDTO);
		if(isInsert)
		{
			ps.setObject(++index,employee_duty_configurationDTO.iD);
		}
		ps.setObject(++index,employee_duty_configurationDTO.isOffice);
		ps.setObject(++index,employee_duty_configurationDTO.employeeId);
		ps.setObject(++index,employee_duty_configurationDTO.officeId);
		ps.setObject(++index,employee_duty_configurationDTO.dutyCat);
		ps.setObject(++index,employee_duty_configurationDTO.isContinuation);
		ps.setObject(++index,employee_duty_configurationDTO.startDate);
		ps.setObject(++index,employee_duty_configurationDTO.endDate);
		ps.setObject(++index,employee_duty_configurationDTO.employees);
		ps.setObject(++index,employee_duty_configurationDTO.searchColumn);
		ps.setObject(++index,employee_duty_configurationDTO.insertedByOrganogramId);
		ps.setObject(++index,employee_duty_configurationDTO.lastModifierUser);
		ps.setObject(++index,employee_duty_configurationDTO.insertionDate);
		if(isInsert)
		{
			ps.setObject(++index,employee_duty_configurationDTO.isDeleted);
		}
		ps.setObject(++index, lastModificationTime);
		if(!isInsert)
		{
			ps.setObject(++index,employee_duty_configurationDTO.iD);
		}
	}
	
	@Override
	public Employee_duty_configurationDTO buildObjectFromResultSet(ResultSet rs)
	{
		try
		{
			Employee_duty_configurationDTO employee_duty_configurationDTO = new Employee_duty_configurationDTO();
			int i = 0;
			employee_duty_configurationDTO.iD = rs.getLong(columnNames[i++]);
			employee_duty_configurationDTO.isOffice = rs.getBoolean(columnNames[i++]);
			employee_duty_configurationDTO.employeeId = rs.getLong(columnNames[i++]);
			employee_duty_configurationDTO.officeId = rs.getLong(columnNames[i++]);
			employee_duty_configurationDTO.dutyCat = rs.getInt(columnNames[i++]);
			employee_duty_configurationDTO.isContinuation = rs.getBoolean(columnNames[i++]);
			employee_duty_configurationDTO.startDate = rs.getLong(columnNames[i++]);
			employee_duty_configurationDTO.endDate = rs.getLong(columnNames[i++]);
			employee_duty_configurationDTO.employees = rs.getString(columnNames[i++]);
			
			employee_duty_configurationDTO.searchColumn = rs.getString(columnNames[i++]);
			employee_duty_configurationDTO.insertedByOrganogramId = rs.getLong(columnNames[i++]);
			employee_duty_configurationDTO.lastModifierUser = rs.getString(columnNames[i++]);
			employee_duty_configurationDTO.insertionDate = rs.getLong(columnNames[i++]);
			employee_duty_configurationDTO.isDeleted = rs.getInt(columnNames[i++]);
			employee_duty_configurationDTO.lastModificationTime = rs.getLong(columnNames[i++]);
			return employee_duty_configurationDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
		
	public Employee_duty_configurationDTO getDTOByID (long id)
	{
		Employee_duty_configurationDTO employee_duty_configurationDTO = null;
		try 
		{
			employee_duty_configurationDTO = getDTOFromID(id);
			if(employee_duty_configurationDTO != null)
			{
				EmployeeDutyConfigurationDetailsDAO employeeDutyConfigurationDetailsDAO = EmployeeDutyConfigurationDetailsDAO.getInstance();				
				List<EmployeeDutyConfigurationDetailsDTO> employeeDutyConfigurationDetailsDTOList = (List<EmployeeDutyConfigurationDetailsDTO>)employeeDutyConfigurationDetailsDAO.getDTOsByParent("employee_duty_configuration_id", employee_duty_configurationDTO.iD);
				employee_duty_configurationDTO.employeeDutyConfigurationDetailsDTOList = employeeDutyConfigurationDetailsDTOList;
			}
		}
		catch (Exception e) 
		{
				// TODO Auto-generated catch block
				e.printStackTrace();
		}
		return employee_duty_configurationDTO;
	}
	
	public Employee_duty_configurationDTO getByEmployeeOrOffice (boolean isOffice, long employeeId, long officeId)
	{
		
		String sql = "select * from " + getTableName() + " where isDeleted = 0 and is_office = " + isOffice
				+ " and employee_id = " + employeeId
				+ " and office_id = " + officeId;
		
		return ConnectionAndStatementUtil.getT(sql,this::buildObjectFromResultSet, null);		
	}

	@Override
	public String getTableName() {
		return "employee_duty_configuration";
	}

	@Override
	public Map<String, String> getSearchMap() {
		return searchMap;
	}

	public long add(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Employee_duty_configurationDTO) commonDTO,addQuery,true);
	}

	public long update(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Employee_duty_configurationDTO) commonDTO,updateQuery,false);
	}
	
	@Override
	public String getLastModifierUser(){
		return "last_modifier_user";
    }
				
}
	