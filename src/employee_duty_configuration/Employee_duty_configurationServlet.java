package employee_duty_configuration;

import java.io.*;
import java.text.SimpleDateFormat;



import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import org.apache.log4j.Logger;

import office_unit_organograms.OfficeUnitOrganograms;
import office_unit_organograms.OfficeUnitOrganogramsRepository;
import permission.MenuConstants;

import user.UserDTO;
import util.*;
import javax.servlet.http.*;
import java.util.*;



import holiday.HolidayDAO;
import holiday.HolidayDTO;
import language.LC;
import language.LM;
import common.ApiResponse;
import common.BaseServlet;
import com.google.gson.Gson;
import pb.*;

import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;




/**
 * Servlet implementation class Employee_duty_configurationServlet
 */
@WebServlet("/Employee_duty_configurationServlet")
@MultipartConfig
public class Employee_duty_configurationServlet extends BaseServlet 
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Employee_duty_configurationServlet.class);
    HolidayDAO holidayDAO = new HolidayDAO();
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        CommonLoginData commonLoginData = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get();
        init(request);
        String actionType = request.getParameter("actionType");
		if ("getByEmpOrOffice".equals(actionType)) {
			try {
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
				boolean isOffice = Boolean.parseBoolean(request.getParameter("isOffice"));
				long id  = Long.parseLong(request.getParameter("id"));
				long employeeId = -1;
				long officeId = -1;
				if(isOffice)
				{
					officeId = id;
				}
				else
				{
					employeeId = id;
				}
				long startDate = Long.parseLong(request.getParameter("startDate"));
				long endDate = Long.parseLong(request.getParameter("endDate"));
								
				DutyDates dutyDates = new DutyDates();
				List<HolidayDTO> holidays = holidayDAO.findByTimeRange(startDate, endDate);
				
				if(holidays != null)
				{
					for(HolidayDTO holiday: holidays)
					{
						dutyDates.holidays.add(simpleDateFormat.format(new Date(holiday.currentDay)));
					}
				}
				
				
				List<EmployeeDutyConfigurationDetailsDTO> dutyDTOs = EmployeeDutyConfigurationDetailsDAO.getInstance().getAll(isOffice, officeId, employeeId, startDate, endDate );
				
				if(dutyDTOs != null)
				{
					for(EmployeeDutyConfigurationDetailsDTO dutyDTO: dutyDTOs)
					{
						dutyDates.workingdays.add(simpleDateFormat.format(new Date(dutyDTO.dutyOn)));
					}
				}
				
				Calendar cal = Calendar.getInstance();
				cal.setTimeInMillis(startDate);
				TimeConverter.clearTimes(cal);
				
				while(cal.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY && cal.getTimeInMillis() <= endDate)
				{
					cal.add(Calendar.DAY_OF_YEAR, 1);
				}
				
				while(cal.getTimeInMillis() <= endDate)
				{
					dutyDates.saturdays.add(simpleDateFormat.format(new Date(cal.getTimeInMillis())));
					cal.add(Calendar.DAY_OF_YEAR, 7);
				}
				
				cal.setTimeInMillis(startDate);
				TimeConverter.clearTimes(cal);
				
				while(cal.get(Calendar.DAY_OF_WEEK) != Calendar.FRIDAY && cal.getTimeInMillis() <= endDate)
				{
					cal.add(Calendar.DAY_OF_YEAR, 1);
				}
				
				while(cal.getTimeInMillis() <= endDate)
				{
					dutyDates.fridays.add(simpleDateFormat.format(new Date(cal.getTimeInMillis())));
					cal.add(Calendar.DAY_OF_YEAR, 7);
				}
				
				cal.setTimeInMillis(startDate);
				TimeConverter.clearTimes(cal);

				while(cal.getTimeInMillis() <= endDate)
				{
					dutyDates.everyday.add(simpleDateFormat.format(new Date(cal.getTimeInMillis())));
					cal.add(Calendar.DAY_OF_YEAR, 1);
				}
				
				PrintWriter out = response.getWriter();
				response.setContentType("application/json");
				response.setCharacterEncoding("UTF-8");
				
				String encoded = this.gson.toJson(dutyDates);
				System.out.println("json encoded data = " + encoded);
				out.print(encoded);
				out.flush();
				
			} catch (Exception ex) {
				logger.error(ex);
				ApiResponse.sendErrorResponse(response, ex.getMessage());
			}
			return;
		}	
		else
		{
			super.doGet(request,response);
		}
    }

    @Override
    public String getTableName() {
        return Employee_duty_configurationDAO.getInstance().getTableName();
    }

    @Override
    public String getServletName() {
        return "Employee_duty_configurationServlet";
    }

    @Override
    public Employee_duty_configurationDAO getCommonDAOService() {
        return Employee_duty_configurationDAO.getInstance();
    }
    
    @Override
    public int[] getAddPageMenuConstants() {
        return new int[] {MenuConstants.EMPLOYEE_DUTY_CONFIGURATION_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[] {MenuConstants.EMPLOYEE_DUTY_CONFIGURATION_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[] {MenuConstants.EMPLOYEE_DUTY_CONFIGURATION_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Employee_duty_configurationServlet.class;
    }
	EmployeeDutyConfigurationDetailsDAO employeeDutyConfigurationDetailsDAO = EmployeeDutyConfigurationDetailsDAO.getInstance();
    private final Gson gson = new Gson();
 	

	@Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception 
	{
		
		request.setAttribute("failureMessage", "");
		System.out.println("%%%% addEmployee_duty_configuration");
		String language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
		Employee_duty_configurationDTO employee_duty_configurationDTO;
		SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
					
		if(addFlag)
		{
			employee_duty_configurationDTO = new Employee_duty_configurationDTO();
		}
		else
		{
			employee_duty_configurationDTO = Employee_duty_configurationDAO.getInstance().getDTOFromID(Long.parseLong(request.getParameter("iD")));
		}
		
		String Value = "";

		

		Value = request.getParameter("employeeId");

		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("employeeId = " + Value);
		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			employee_duty_configurationDTO.employeeId = Long.parseLong(Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}

		Value = request.getParameter("officeId");

		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("officeId = " + Value);
		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			employee_duty_configurationDTO.officeId = Long.parseLong(Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}

        employee_duty_configurationDTO.isOffice = employee_duty_configurationDTO.employeeId < 0;


		Value = request.getParameter("startDate");

		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("startDate = " + Value);
		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			try 
			{
				employee_duty_configurationDTO.startDate = Long.parseLong(Value);
			}
			catch (Exception e) 
			{
				e.printStackTrace();
				throw new Exception(LM.getText(LC.EMPLOYEE_DUTY_CONFIGURATION_ADD_STARTDATE, language) + " " + ErrorMessage.getInvalidMessage(language));
			}
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}

		Value = request.getParameter("endDate");

		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("endDate = " + Value);
		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			try 
			{
				employee_duty_configurationDTO.endDate = Long.parseLong(Value);
			}
			catch (Exception e) 
			{
				e.printStackTrace();
				throw new Exception(LM.getText(LC.EMPLOYEE_DUTY_CONFIGURATION_ADD_ENDDATE, language) + " " + ErrorMessage.getInvalidMessage(language));
			}
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}

		if(addFlag)
		{
			employee_duty_configurationDTO.insertedByOrganogramId = userDTO.organogramID;
		}


		employee_duty_configurationDTO.lastModifierUser = userDTO.userName;


		if(addFlag)
		{				
			employee_duty_configurationDTO.insertionDate = TimeConverter.getToday();
		}			

		System.out.println("Done adding  addEmployee_duty_configuration dto = " + employee_duty_configurationDTO);
		
		List<OfficeUnitOrganograms> orgs = null;
		if(employee_duty_configurationDTO.isOffice)
		{
			orgs = OfficeUnitOrganogramsRepository.getInstance().getByOfficeUnitId(employee_duty_configurationDTO.officeId);
			employee_duty_configurationDTO.employees = "";
			if(orgs != null)
			{
				for(OfficeUnitOrganograms org: orgs)
				{
					employee_duty_configurationDTO.employees += org.id + ", ";
				}
			}
		}

		if(addFlag == true)
		{
			Employee_duty_configurationDAO.getInstance().add(employee_duty_configurationDTO);
		}
		else
		{				
			Employee_duty_configurationDAO.getInstance().update(employee_duty_configurationDTO);										
		}
		
		if(employee_duty_configurationDTO.isOffice)
		{
			EmployeeDutyConfigurationDetailsDAO.getInstance().deleteAllByOffice(employee_duty_configurationDTO.officeId, employee_duty_configurationDTO.startDate, employee_duty_configurationDTO.endDate);
		}
		else
		{
			EmployeeDutyConfigurationDetailsDAO.getInstance().deleteAllByEmployee(employee_duty_configurationDTO.employeeId, employee_duty_configurationDTO.startDate, employee_duty_configurationDTO.endDate);
		}
		
		String[] dates = request.getParameterValues("dutyOn");
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
		if(dates != null)
		{
			for(String sDate: dates)
			{
				long dutyOn = simpleDateFormat.parse(sDate).getTime();
				EmployeeDutyConfigurationDetailsDTO employeeDutyConfigurationDetailsDTO = new EmployeeDutyConfigurationDetailsDTO(employee_duty_configurationDTO, dutyOn, sDate);
				EmployeeDutyConfigurationDetailsDAO.getInstance().add(employeeDutyConfigurationDetailsDTO);
				
				logger.debug("$$$$$$$$$$$$$$$$$ employee_duty_configurationDTO.isOffice: " + employee_duty_configurationDTO.isOffice);
				if(employee_duty_configurationDTO.isOffice)
				{
					if(orgs != null)
					{
						for(OfficeUnitOrganograms org: orgs)
						{
							logger.debug("$$$$$$$$$$$$$$$$$ org found for office: " + org.id);
							EmployeeDutyConfigurationDetailsDTO empDTO = new EmployeeDutyConfigurationDetailsDTO(employee_duty_configurationDTO, dutyOn, org.id, sDate);
							EmployeeDutyConfigurationDetailsDAO.getInstance().add(empDTO);
						}
					}
					
				}
			}
		}
		
		
		return employee_duty_configurationDTO;

	}
	
	
	
}

