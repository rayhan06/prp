package employee_duty_configuration;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import com.google.gson.Gson;

import repository.Repository;
import repository.RepositoryManager;


public class EmployeeDutyConfigurationDetailsRepository implements Repository {
	EmployeeDutyConfigurationDetailsDAO employeedutyconfigurationdetailsDAO = null;
	
	static Logger logger = Logger.getLogger(EmployeeDutyConfigurationDetailsRepository.class);
	Map<Long, EmployeeDutyConfigurationDetailsDTO>mapOfEmployeeDutyConfigurationDetailsDTOToiD;
	Map<Long, Set<EmployeeDutyConfigurationDetailsDTO> >mapOfEmployeeDutyConfigurationDetailsDTOToemployeeDutyConfigurationId;
	Gson gson;

  
	private EmployeeDutyConfigurationDetailsRepository(){
		employeedutyconfigurationdetailsDAO = EmployeeDutyConfigurationDetailsDAO.getInstance();
		mapOfEmployeeDutyConfigurationDetailsDTOToiD = new ConcurrentHashMap<>();
		mapOfEmployeeDutyConfigurationDetailsDTOToemployeeDutyConfigurationId = new ConcurrentHashMap<>();
		gson = new Gson();
		RepositoryManager.getInstance().addRepository(this);
	}

	private static class LazyLoader{
        final static EmployeeDutyConfigurationDetailsRepository INSTANCE = new EmployeeDutyConfigurationDetailsRepository();
    }

    public static EmployeeDutyConfigurationDetailsRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

	public void reload(boolean reloadAll){
		try {
			List<EmployeeDutyConfigurationDetailsDTO> employeedutyconfigurationdetailsDTOs = employeedutyconfigurationdetailsDAO.getAllDTOs(reloadAll);
			for(EmployeeDutyConfigurationDetailsDTO employeedutyconfigurationdetailsDTO : employeedutyconfigurationdetailsDTOs) {
				EmployeeDutyConfigurationDetailsDTO oldEmployeeDutyConfigurationDetailsDTO = getEmployeeDutyConfigurationDetailsDTOByiD(employeedutyconfigurationdetailsDTO.iD);
				if( oldEmployeeDutyConfigurationDetailsDTO != null ) {
					mapOfEmployeeDutyConfigurationDetailsDTOToiD.remove(oldEmployeeDutyConfigurationDetailsDTO.iD);
				
					if(mapOfEmployeeDutyConfigurationDetailsDTOToemployeeDutyConfigurationId.containsKey(oldEmployeeDutyConfigurationDetailsDTO.employeeDutyConfigurationId)) {
						mapOfEmployeeDutyConfigurationDetailsDTOToemployeeDutyConfigurationId.get(oldEmployeeDutyConfigurationDetailsDTO.employeeDutyConfigurationId).remove(oldEmployeeDutyConfigurationDetailsDTO);
					}
					if(mapOfEmployeeDutyConfigurationDetailsDTOToemployeeDutyConfigurationId.get(oldEmployeeDutyConfigurationDetailsDTO.employeeDutyConfigurationId).isEmpty()) {
						mapOfEmployeeDutyConfigurationDetailsDTOToemployeeDutyConfigurationId.remove(oldEmployeeDutyConfigurationDetailsDTO.employeeDutyConfigurationId);
					}
					
					
				}
				if(employeedutyconfigurationdetailsDTO.isDeleted == 0) 
				{
					
					mapOfEmployeeDutyConfigurationDetailsDTOToiD.put(employeedutyconfigurationdetailsDTO.iD, employeedutyconfigurationdetailsDTO);
				
					if( ! mapOfEmployeeDutyConfigurationDetailsDTOToemployeeDutyConfigurationId.containsKey(employeedutyconfigurationdetailsDTO.employeeDutyConfigurationId)) {
						mapOfEmployeeDutyConfigurationDetailsDTOToemployeeDutyConfigurationId.put(employeedutyconfigurationdetailsDTO.employeeDutyConfigurationId, new HashSet<>());
					}
					mapOfEmployeeDutyConfigurationDetailsDTOToemployeeDutyConfigurationId.get(employeedutyconfigurationdetailsDTO.employeeDutyConfigurationId).add(employeedutyconfigurationdetailsDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public EmployeeDutyConfigurationDetailsDTO clone(EmployeeDutyConfigurationDetailsDTO dto) {
		String raw = gson.toJson(dto);
		return gson.fromJson(raw, EmployeeDutyConfigurationDetailsDTO.class);
	}
	
	
	public List<EmployeeDutyConfigurationDetailsDTO> getEmployeeDutyConfigurationDetailsList() {
		List <EmployeeDutyConfigurationDetailsDTO> employeedutyconfigurationdetailss = new ArrayList<EmployeeDutyConfigurationDetailsDTO>(this.mapOfEmployeeDutyConfigurationDetailsDTOToiD.values());
		return employeedutyconfigurationdetailss;
	}
	
	public List<EmployeeDutyConfigurationDetailsDTO> copyEmployeeDutyConfigurationDetailsList() {
		List <EmployeeDutyConfigurationDetailsDTO> employeedutyconfigurationdetailss = getEmployeeDutyConfigurationDetailsList();
		return employeedutyconfigurationdetailss
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}
	
	
	
	public EmployeeDutyConfigurationDetailsDTO getEmployeeDutyConfigurationDetailsDTOByiD( long iD){
		return mapOfEmployeeDutyConfigurationDetailsDTOToiD.get(iD);
	}
	
	public EmployeeDutyConfigurationDetailsDTO copyEmployeeDutyConfigurationDetailsDTOByiD( long iD){
		return clone(mapOfEmployeeDutyConfigurationDetailsDTOToiD.get(iD));
	}
	
	
	public List<EmployeeDutyConfigurationDetailsDTO> getEmployeeDutyConfigurationDetailsDTOByemployeeDutyConfigurationId(long employeeDutyConfigurationId) {
		return new ArrayList<>( mapOfEmployeeDutyConfigurationDetailsDTOToemployeeDutyConfigurationId.getOrDefault(employeeDutyConfigurationId,new HashSet<>()));
	}
	
	public List<EmployeeDutyConfigurationDetailsDTO> copyEmployeeDutyConfigurationDetailsDTOByemployeeDutyConfigurationId(long employeeDutyConfigurationId)
	{
		List <EmployeeDutyConfigurationDetailsDTO> employeedutyconfigurationdetailss = getEmployeeDutyConfigurationDetailsDTOByemployeeDutyConfigurationId(employeeDutyConfigurationId);
		return employeedutyconfigurationdetailss
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}

	
	@Override
	public String getTableName() {
		return employeedutyconfigurationdetailsDAO.getTableName();
	}
}


