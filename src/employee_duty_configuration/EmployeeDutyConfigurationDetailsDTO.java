package employee_duty_configuration;
import java.util.*; 
import util.*; 


public class EmployeeDutyConfigurationDetailsDTO extends CommonDTO
{

	public long employeeDutyConfigurationId = -1;
	public long dutyOn = -1;
	public long insertedByOrganogramId = -1;
    public String lastModifierUser = "";
	public long insertionDate = -1;
	public String dateStr = "";
	
	public boolean isOffice = false;
	public long employeeId = -1;
	public long officeId = -1;

	
	public List<EmployeeDutyConfigurationDetailsDTO> employeeDutyConfigurationDetailsDTOList = new ArrayList<>();
	
	public EmployeeDutyConfigurationDetailsDTO()
	{
		
	}
	
	public EmployeeDutyConfigurationDetailsDTO(Employee_duty_configurationDTO dto, long dutyOn, String dateStr)
	{
		employeeDutyConfigurationId = dto.iD;
		this.dutyOn = dutyOn;
		isOffice = dto.isOffice;
		employeeId = dto.employeeId;
		officeId = dto.officeId;
		insertedByOrganogramId = dto.insertedByOrganogramId;
		lastModifierUser = dto.lastModifierUser;
		insertionDate = dto.insertionDate;
		this.dateStr = dateStr;
	}
	
	public EmployeeDutyConfigurationDetailsDTO(Employee_duty_configurationDTO dto, long dutyOn, long employeeId, String dateStr)
	{
		employeeDutyConfigurationId = dto.iD;
		this.dutyOn = dutyOn;
		isOffice = false;
		this.employeeId = employeeId;
		officeId = dto.officeId;
		insertedByOrganogramId = dto.insertedByOrganogramId;
		lastModifierUser = dto.lastModifierUser;
		insertionDate = dto.insertionDate;
		this.dateStr = dateStr;
	}
	
    @Override
	public String toString() {
            return "$EmployeeDutyConfigurationDetailsDTO[" +
            " iD = " + iD +
            " employeeDutyConfigurationId = " + employeeDutyConfigurationId +
            " dutyOn = " + dutyOn +
            " searchColumn = " + searchColumn +
            " insertedByOrganogramId = " + insertedByOrganogramId +
            " lastModifierUser = " + lastModifierUser +
            " insertionDate = " + insertionDate +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}