package employee_duty_configuration;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.sql.SQLException;
import java.sql.Statement;

import common.CommonDAOService;
import common.ConnectionAndStatementUtil;

import org.apache.log4j.Logger;
import util.*;
import pb.*;

public class EmployeeDutyConfigurationDetailsDAO  implements CommonDAOService<EmployeeDutyConfigurationDetailsDTO>
{
	
	Logger logger = Logger.getLogger(getClass());
	
	private static String addQuery = "";
	private static String updateQuery = "";
	private String[] columnNames = null;

	private EmployeeDutyConfigurationDetailsDAO()
	{
		columnNames = new String[] 
		{
			"ID",
			"employee_duty_configuration_id",
			"is_office",
			"employee_id",
			"office_id",
			"duty_on",
			"search_column",
			"inserted_by_organogram_id",
			"last_modifier_user",
			"insertion_date",
			"date_str",
			"isDeleted",
			"lastModificationTime"
		};
		updateQuery = getUpdateQuery(columnNames);
        addQuery = getAddQuery(columnNames);
		
		searchMap.put("duty_on"," and (duty_on like ?)");
		searchMap.put("insertion_date_start"," and (insertion_date >= ?)");
		searchMap.put("insertion_date_end"," and (insertion_date <= ?)");
		searchMap.put("AnyField"," and (search_column like ?)");
	}

	private final Map<String,String> searchMap = new HashMap<>();

    private static class LazyLoader
    {
    	static final EmployeeDutyConfigurationDetailsDAO INSTANCE = new EmployeeDutyConfigurationDetailsDAO();
	}

	public static EmployeeDutyConfigurationDetailsDAO getInstance()
	{
    	return LazyLoader.INSTANCE;
	}
	
	public void setSearchColumn(EmployeeDutyConfigurationDetailsDTO employeedutyconfigurationdetailsDTO)
	{
		employeedutyconfigurationdetailsDTO.searchColumn = "";
		employeedutyconfigurationdetailsDTO.searchColumn += employeedutyconfigurationdetailsDTO.dutyOn + " ";
	}
	
	@Override
	public void set(PreparedStatement ps, EmployeeDutyConfigurationDetailsDTO employeedutyconfigurationdetailsDTO, boolean isInsert) throws SQLException
	{
		int index = 0;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(employeedutyconfigurationdetailsDTO);
		if(isInsert)
		{
			ps.setObject(++index,employeedutyconfigurationdetailsDTO.iD);
		}
		ps.setObject(++index,employeedutyconfigurationdetailsDTO.employeeDutyConfigurationId);
		ps.setObject(++index,employeedutyconfigurationdetailsDTO.isOffice);
		ps.setObject(++index,employeedutyconfigurationdetailsDTO.employeeId);
		ps.setObject(++index,employeedutyconfigurationdetailsDTO.officeId);
		ps.setObject(++index,employeedutyconfigurationdetailsDTO.dutyOn);
		ps.setObject(++index,employeedutyconfigurationdetailsDTO.searchColumn);
		ps.setObject(++index,employeedutyconfigurationdetailsDTO.insertedByOrganogramId);
		ps.setObject(++index,employeedutyconfigurationdetailsDTO.lastModifierUser);
		ps.setObject(++index,employeedutyconfigurationdetailsDTO.insertionDate);
		ps.setObject(++index,employeedutyconfigurationdetailsDTO.dateStr);
		if(isInsert)
		{
			ps.setObject(++index,employeedutyconfigurationdetailsDTO.isDeleted);
		}
		ps.setObject(++index, lastModificationTime);
		if(!isInsert)
		{
			ps.setObject(++index,employeedutyconfigurationdetailsDTO.iD);
		}
	}
	
	@Override
	public EmployeeDutyConfigurationDetailsDTO buildObjectFromResultSet(ResultSet rs)
	{
		try
		{
			EmployeeDutyConfigurationDetailsDTO employeedutyconfigurationdetailsDTO = new EmployeeDutyConfigurationDetailsDTO();
			int i = 0;
			employeedutyconfigurationdetailsDTO.iD = rs.getLong(columnNames[i++]);
			employeedutyconfigurationdetailsDTO.employeeDutyConfigurationId = rs.getLong(columnNames[i++]);
			employeedutyconfigurationdetailsDTO.isOffice = rs.getBoolean(columnNames[i++]);
			employeedutyconfigurationdetailsDTO.employeeId = rs.getLong(columnNames[i++]);
			employeedutyconfigurationdetailsDTO.officeId = rs.getLong(columnNames[i++]);
			employeedutyconfigurationdetailsDTO.dutyOn = rs.getLong(columnNames[i++]);
			employeedutyconfigurationdetailsDTO.searchColumn = rs.getString(columnNames[i++]);
			employeedutyconfigurationdetailsDTO.insertedByOrganogramId = rs.getLong(columnNames[i++]);
			employeedutyconfigurationdetailsDTO.lastModifierUser = rs.getString(columnNames[i++]);
			employeedutyconfigurationdetailsDTO.insertionDate = rs.getLong(columnNames[i++]);
			employeedutyconfigurationdetailsDTO.dateStr = rs.getString(columnNames[i++]);
			employeedutyconfigurationdetailsDTO.isDeleted = rs.getInt(columnNames[i++]);
			employeedutyconfigurationdetailsDTO.lastModificationTime = rs.getLong(columnNames[i++]);
			return employeedutyconfigurationdetailsDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
	
	public List<EmployeeDutyConfigurationDetailsDTO> getAll (boolean isOffice, long officeId, long employeeId, long startDate, long endDate)
	{
		
		String sql = "select * from " + getTableName() + " where isDeleted = 0 and is_office = " + isOffice
				+ " and employee_id = " + employeeId
				+ " and office_id = " + officeId
				+ " and duty_on >= " + startDate
				+ " and duty_on <= " + endDate
				+ " order by duty_on asc";
		
		return ConnectionAndStatementUtil.getListOfT(sql,this::buildObjectFromResultSet);		
	}
	
	public boolean hasOfficeOn(List<EmployeeDutyConfigurationDetailsDTO> dtos, long date)
	{
		for(EmployeeDutyConfigurationDetailsDTO dto: dtos)
		{
			if(dto.dutyOn == date)
			{
				return true;
			}
		}
		return false;
	}
	
	
	 public void deleteAllByOffice(long officeId,long startDate, long endDate) throws Exception {
        long lastModificationTime = System.currentTimeMillis();
     	        
        String sql = "update " + getTableName() + " set isDeleted = 1, lastModificationTime = " + lastModificationTime
        		+ " where office_id = " + officeId
        		+ " and duty_on >= " + startDate
				+ " and duty_on <= " + endDate;
        ConnectionAndStatementUtil.getWriteConnectionAndStatement(model -> {
            
            Connection connection = model.getConnection();
            Statement stmt = model.getStatement();
            try {
                logger.debug(sql);
                stmt.execute(sql);
                recordUpdateTime(connection, getTableName());
            } catch (SQLException ex) {
                logger.error(ex);
            }
        });
	}
	 
	 public void deleteAllByEmployee(long employeeId,long startDate, long endDate) throws Exception {
        long lastModificationTime = System.currentTimeMillis();
     	        
        String sql = "update " + getTableName() + " set isDeleted = 1, lastModificationTime = " + lastModificationTime
        		+ " where employee_id = " + employeeId
        		+ " and duty_on >= " + startDate
				+ " and duty_on <= " + endDate;
        ConnectionAndStatementUtil.getWriteConnectionAndStatement(model -> {
            
            Connection connection = model.getConnection();
            Statement stmt = model.getStatement();
            try {
                logger.debug(sql);
                stmt.execute(sql);
                recordUpdateTime(connection, getTableName());
            } catch (SQLException ex) {
                logger.error(ex);
            }
        });
	}
		
	public EmployeeDutyConfigurationDetailsDTO getDTOByID (long id)
	{
		return getDTOFromID(id);
	}

	@Override
	public String getTableName() {
		return "employee_duty_configuration_details";
	}

	@Override
	public Map<String, String> getSearchMap() {
		return searchMap;
	}

	public long add(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((EmployeeDutyConfigurationDetailsDTO) commonDTO,addQuery,true);
	}

	public long update(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((EmployeeDutyConfigurationDetailsDTO) commonDTO,updateQuery,false);
	}
	
	@Override
	public String getLastModifierUser(){
		return "last_modifier_user";
    }
				
}
	