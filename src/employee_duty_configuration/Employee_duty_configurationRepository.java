package employee_duty_configuration;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import com.google.gson.Gson;

import repository.Repository;
import repository.RepositoryManager;


public class Employee_duty_configurationRepository implements Repository {
	Employee_duty_configurationDAO employee_duty_configurationDAO = null;
	
	static Logger logger = Logger.getLogger(Employee_duty_configurationRepository.class);
	Map<Long, Employee_duty_configurationDTO>mapOfEmployee_duty_configurationDTOToiD;
	Gson gson;

  
	private Employee_duty_configurationRepository(){
		employee_duty_configurationDAO = Employee_duty_configurationDAO.getInstance();
		mapOfEmployee_duty_configurationDTOToiD = new ConcurrentHashMap<>();
		gson = new Gson();
		RepositoryManager.getInstance().addRepository(this);
	}

	private static class LazyLoader{
        final static Employee_duty_configurationRepository INSTANCE = new Employee_duty_configurationRepository();
    }

    public static Employee_duty_configurationRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

	public void reload(boolean reloadAll){
		try {
			List<Employee_duty_configurationDTO> employee_duty_configurationDTOs = employee_duty_configurationDAO.getAllDTOs(reloadAll);
			for(Employee_duty_configurationDTO employee_duty_configurationDTO : employee_duty_configurationDTOs) {
				Employee_duty_configurationDTO oldEmployee_duty_configurationDTO = getEmployee_duty_configurationDTOByiD(employee_duty_configurationDTO.iD);
				if( oldEmployee_duty_configurationDTO != null ) {
					mapOfEmployee_duty_configurationDTOToiD.remove(oldEmployee_duty_configurationDTO.iD);
				
					
				}
				if(employee_duty_configurationDTO.isDeleted == 0) 
				{
					
					mapOfEmployee_duty_configurationDTOToiD.put(employee_duty_configurationDTO.iD, employee_duty_configurationDTO);
				
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public Employee_duty_configurationDTO clone(Employee_duty_configurationDTO dto) {
		String raw = gson.toJson(dto);
		return gson.fromJson(raw, Employee_duty_configurationDTO.class);
	}
	
	
	public List<Employee_duty_configurationDTO> getEmployee_duty_configurationList() {
		List <Employee_duty_configurationDTO> employee_duty_configurations = new ArrayList<Employee_duty_configurationDTO>(this.mapOfEmployee_duty_configurationDTOToiD.values());
		return employee_duty_configurations;
	}
	
	public List<Employee_duty_configurationDTO> copyEmployee_duty_configurationList() {
		List <Employee_duty_configurationDTO> employee_duty_configurations = getEmployee_duty_configurationList();
		return employee_duty_configurations
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}
	
	
	
	public Employee_duty_configurationDTO getEmployee_duty_configurationDTOByiD( long iD){
		return mapOfEmployee_duty_configurationDTOToiD.get(iD);
	}
	
	public Employee_duty_configurationDTO copyEmployee_duty_configurationDTOByiD( long iD){
		return clone(mapOfEmployee_duty_configurationDTOToiD.get(iD));
	}

	
	@Override
	public String getTableName() {
		return employee_duty_configurationDAO.getTableName();
	}
}


