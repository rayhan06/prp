package employee_duty_configuration;
import java.util.*; 
import util.*; 


public class Employee_duty_configurationDTO extends CommonDTO
{

	public boolean isOffice = false;
	public long employeeId = -1;
	public long officeId = -1;
	public int dutyCat = -1;
	public boolean isContinuation = false;
	public long startDate = System.currentTimeMillis();
	public long endDate = System.currentTimeMillis();
	public long insertedByOrganogramId = -1;
    public String lastModifierUser = "";
	public long insertionDate = -1;
	
	public String employees = "";
	
	public List<EmployeeDutyConfigurationDetailsDTO> employeeDutyConfigurationDetailsDTOList = new ArrayList<>();
	
    @Override
	public String toString() {
            return "$Employee_duty_configurationDTO[" +
            " iD = " + iD +
            " isOffice = " + isOffice +
            " employeeId = " + employeeId +
            " officeId = " + officeId +
            " dutyCat = " + dutyCat +
            " isContinuation = " + isContinuation +
            " startDate = " + startDate +
            " endDate = " + endDate +
            " searchColumn = " + searchColumn +
            " insertedByOrganogramId = " + insertedByOrganogramId +
            " lastModifierUser = " + lastModifierUser +
            " insertionDate = " + insertionDate +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}