package approval_module_map;
import java.util.*; 


public class Approval_module_mapDTO {

	public long iD = 0;
    public String tableName = "";
	public boolean hasAddApproval = false;
	public boolean hasEditApproval = false;
	public boolean hasDeleteApproval = false;
	public int isDeleted = 0;
	public long lastModificationTime = 0;
	
	
    @Override
	public String toString() {
            return "$Approval_module_mapDTO[" +
            " iD = " + iD +
            " tableName = " + tableName +
            " hasAddApproval = " + hasAddApproval +
            " hasEditApproval = " + hasEditApproval +
            " hasDeleteApproval = " + hasDeleteApproval +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}