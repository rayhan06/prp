package approval_module_map;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;


import org.apache.log4j.Logger;

import dbm.*;
import login.LoginDTO;
import repository.RepositoryManager;

import util.NavigationService2;


public class Approval_module_mapDAO  extends NavigationService2
{
	
	Logger logger = Logger.getLogger(getClass());
	
	
	public long addApproval_module_map(Approval_module_mapDTO approval_module_mapDTO) throws Exception{
		
		Connection connection = null;
		PreparedStatement ps = null;

		long lastModificationTime = System.currentTimeMillis();	

		try{
			connection = DBMW.getInstance().getConnection();
			
			if(connection == null)
			{
				System.out.println("nullconn");
			}

			approval_module_mapDTO.iD = DBMW.getInstance().getNextSequenceId("approval_module_map");

			String sql = "INSERT INTO approval_module_map";
			
			sql += " (";
			sql += "ID";
			sql += ", ";
			sql += "table_name";
			sql += ", ";
			sql += "has_add_approval";
			sql += ", ";
			sql += "has_edit_approval";
			sql += ", ";
			sql += "has_delete_approval";
			sql += ", ";
			sql += "isDeleted";
			sql += ", ";
			sql += "lastModificationTime";
			sql += ")";
			
			
            sql += " VALUES(";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ")";
				
			printSql(sql);

			ps = connection.prepareStatement(sql);
			
			
			

			int index = 1;

			ps.setObject(index++,approval_module_mapDTO.iD);
			ps.setObject(index++,approval_module_mapDTO.tableName);
			ps.setObject(index++,approval_module_mapDTO.hasAddApproval);
			ps.setObject(index++,approval_module_mapDTO.hasEditApproval);
			ps.setObject(index++,approval_module_mapDTO.hasDeleteApproval);
			ps.setObject(index++,approval_module_mapDTO.isDeleted);
			ps.setObject(index++, lastModificationTime);
			
			System.out.println(ps);
			ps.execute();
			
			
			recordUpdateTime(connection, lastModificationTime);

		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if (ps != null) {
					ps.close();
				}
			} catch(Exception e){}
			try{
				if(connection != null){
					DBMW.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		return approval_module_mapDTO.iD;		
	}
	
	
	//need another getter for repository
	public Approval_module_mapDTO getApproval_module_mapDTOByID (long ID) throws Exception{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Approval_module_mapDTO approval_module_mapDTO = null;
		try{
			
			String sql = "SELECT * ";

			sql += " FROM approval_module_map";
			
            sql += " WHERE ID=" + ID;
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			if(rs.next()){
				approval_module_mapDTO = new Approval_module_mapDTO();

				approval_module_mapDTO.iD = rs.getLong("ID");
				approval_module_mapDTO.tableName = rs.getString("table_name");
				approval_module_mapDTO.hasAddApproval = rs.getBoolean("has_add_approval");
				approval_module_mapDTO.hasEditApproval = rs.getBoolean("has_edit_approval");
				approval_module_mapDTO.hasDeleteApproval = rs.getBoolean("has_delete_approval");
				approval_module_mapDTO.isDeleted = rs.getInt("isDeleted");
				approval_module_mapDTO.lastModificationTime = rs.getLong("lastModificationTime");

			}			
			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DBMR.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
		return approval_module_mapDTO;
	}
	
	
	public Approval_module_mapDTO getApproval_module_mapDTOByTableName(String table_name) throws Exception{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Approval_module_mapDTO approval_module_mapDTO = new Approval_module_mapDTO();
		try{
			
			String sql = "SELECT * ";

			sql += " FROM approval_module_map";
			
            sql += " WHERE table_name ='" + table_name + "'";
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			if(rs.next()){
				approval_module_mapDTO = new Approval_module_mapDTO();

				approval_module_mapDTO.iD = rs.getLong("ID");
				approval_module_mapDTO.tableName = rs.getString("table_name");
				approval_module_mapDTO.hasAddApproval = rs.getBoolean("has_add_approval");
				approval_module_mapDTO.hasEditApproval = rs.getBoolean("has_edit_approval");
				approval_module_mapDTO.hasDeleteApproval = rs.getBoolean("has_delete_approval");
				approval_module_mapDTO.isDeleted = rs.getInt("isDeleted");
				approval_module_mapDTO.lastModificationTime = rs.getLong("lastModificationTime");

			}			
			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DBMR.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
		return approval_module_mapDTO;
	}
	
	public long updateApproval_module_map(Approval_module_mapDTO approval_module_mapDTO) throws Exception{
		
		Connection connection = null;
		PreparedStatement ps = null;

		long lastModificationTime = System.currentTimeMillis();	
		try{
			connection = DBMW.getInstance().getConnection();

			String sql = "UPDATE approval_module_map";
			
			sql += " SET ";
			sql += "table_name=?";
			sql += ", ";
			sql += "has_add_approval=?";
			sql += ", ";
			sql += "has_edit_approval=?";
			sql += ", ";
			sql += "has_delete_approval=?";
			sql += ", lastModificationTime = "	+ lastModificationTime + "";
            sql += " WHERE ID = " + approval_module_mapDTO.iD;
				
			printSql(sql);

			ps = connection.prepareStatement(sql);
			
			

			int index = 1;
			
			ps.setObject(index++,approval_module_mapDTO.tableName);
			ps.setObject(index++,approval_module_mapDTO.hasAddApproval);
			ps.setObject(index++,approval_module_mapDTO.hasEditApproval);
			ps.setObject(index++,approval_module_mapDTO.hasDeleteApproval);
			System.out.println(ps);
			ps.executeUpdate();
			


			
			recordUpdateTime(connection, lastModificationTime);

						
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if (ps != null) {
					ps.close();
				}
			} catch(Exception e){}
			try{
				if(connection != null){
					DBMW.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		return approval_module_mapDTO.iD;
	}
	
	public void deleteApproval_module_mapByID(long ID) throws Exception{
		Connection connection = null;
		Statement stmt = null;
		PreparedStatement ps = null;
		
		long lastModificationTime = System.currentTimeMillis();	
		try{
			String sql = "UPDATE approval_module_map";
			
			sql += " SET isDeleted=1,lastModificationTime="+ lastModificationTime +" WHERE ID = " + ID;
			
			printSql(sql);

			connection = DBMW.getInstance().getConnection();
			stmt  = connection.createStatement();
			stmt.execute(sql);
			

			
			recordUpdateTime(connection, lastModificationTime);

			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DBMW.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
	}

	
	
	public List<Approval_module_mapDTO> getDTOs(Collection recordIDs){
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Approval_module_mapDTO approval_module_mapDTO = null;
		List<Approval_module_mapDTO> approval_module_mapDTOList = new ArrayList<>();
		if(recordIDs.isEmpty()){
			return approval_module_mapDTOList;
		}
		try{
			
			String sql = "SELECT * ";

			sql += " FROM approval_module_map";
            
            sql += " WHERE ID IN ( ";

			for(int i = 0;i<recordIDs.size();i++){
				if(i!=0){
					sql+=",";
				}
				sql+=((ArrayList)recordIDs).get(i);
			}
			sql+=")  order by lastModificationTime desc";
			
			printSql(sql);
			
			logger.debug("sql " + sql);
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				approval_module_mapDTO = new Approval_module_mapDTO();
				approval_module_mapDTO.iD = rs.getLong("ID");
				approval_module_mapDTO.tableName = rs.getString("table_name");
				approval_module_mapDTO.hasAddApproval = rs.getBoolean("has_add_approval");
				approval_module_mapDTO.hasEditApproval = rs.getBoolean("has_edit_approval");
				approval_module_mapDTO.hasDeleteApproval = rs.getBoolean("has_delete_approval");
				approval_module_mapDTO.isDeleted = rs.getInt("isDeleted");
				approval_module_mapDTO.lastModificationTime = rs.getLong("lastModificationTime");
				System.out.println("got this DTO: " + approval_module_mapDTO);
				
				approval_module_mapDTOList.add(approval_module_mapDTO);

			}			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DBMR.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
		return approval_module_mapDTOList;
	
	}
	
	

	
	
	
	//add repository
	public List<Approval_module_mapDTO> getAllApproval_module_map (boolean isFirstReload)
    {
		List<Approval_module_mapDTO> approval_module_mapDTOList = new ArrayList<>();

		String sql = "SELECT * FROM approval_module_map";
		sql += " WHERE ";
	

		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by approval_module_map.lastModificationTime desc";
		printSql(sql);
		
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;

		
		try{
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();
			rs = stmt.executeQuery(sql);
			

			while(rs.next()){
				Approval_module_mapDTO approval_module_mapDTO = new Approval_module_mapDTO();
				approval_module_mapDTO.iD = rs.getLong("ID");
				approval_module_mapDTO.tableName = rs.getString("table_name");
				approval_module_mapDTO.hasAddApproval = rs.getBoolean("has_add_approval");
				approval_module_mapDTO.hasEditApproval = rs.getBoolean("has_edit_approval");
				approval_module_mapDTO.hasDeleteApproval = rs.getBoolean("has_delete_approval");
				approval_module_mapDTO.isDeleted = rs.getInt("isDeleted");
				approval_module_mapDTO.lastModificationTime = rs.getLong("lastModificationTime");
				
				approval_module_mapDTOList.add(approval_module_mapDTO);
			}			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			try{ 
				if (connection != null){ 
					DBMR.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}

		return approval_module_mapDTOList;
    }
	
	public List<Approval_module_mapDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset)
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		List<Approval_module_mapDTO> approval_module_mapDTOList = new ArrayList<>();

		try{
			
			String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS);
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				Approval_module_mapDTO approval_module_mapDTO = new Approval_module_mapDTO();
				approval_module_mapDTO.iD = rs.getLong("ID");
				approval_module_mapDTO.tableName = rs.getString("table_name");
				approval_module_mapDTO.hasAddApproval = rs.getBoolean("has_add_approval");
				approval_module_mapDTO.hasEditApproval = rs.getBoolean("has_edit_approval");
				approval_module_mapDTO.hasDeleteApproval = rs.getBoolean("has_delete_approval");
				approval_module_mapDTO.isDeleted = rs.getInt("isDeleted");
				approval_module_mapDTO.lastModificationTime = rs.getLong("lastModificationTime");
				
				approval_module_mapDTOList.add(approval_module_mapDTO);
			}					
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DBMR.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
		return approval_module_mapDTOList;
	
	}
	public Collection getIDs() 
    {
        Collection data = new ArrayList();
        Connection connection=null;
    	Statement stmt=null;
    	ResultSet resultSet = null;
    	
        String sql = getSqlWithSearchCriteria(null, -1, -1, GETIDS);
		
		printSql(sql);
		
        try
        {
	        connection = DBMR.getInstance().getConnection();
	        stmt = connection.createStatement();
	        
	        for(resultSet = stmt.executeQuery(sql); resultSet.next(); data.add(resultSet.getString("ID")));
	
	        resultSet.close();
        }
        catch (Exception e)
        {
	    	logger.fatal("DAO " + e, e);
        }
	    finally
        {
	    	try
            {
          	  if(resultSet!= null && !resultSet.isClosed())
          	  {
          		  resultSet.close();
          	  }
            }
            catch(Exception ex)
            {
          	  
            }
          try{if (stmt != null){stmt.close();}}catch (Exception e){}
          try{if (connection != null){DBMR.getInstance().freeConnection(connection);}}
          catch (Exception e){logger.fatal("DAO finally :" + e);}
        }
        return data;
    }
	
	public String getSqlWithSearchCriteria(Hashtable p_searchCriteria, int limit, int offset, int category)
    {
		String sql = "SELECT ";
		if(category == GETIDS)
		{
			sql += "distinct approval_module_map.ID as ID ";
		}
		else if(category == GETCOUNT)
		{
			sql += " count(approval_module_map.ID) as countID ";
		}
		else if(category == GETDTOS)
		{
			sql += "  approval_module_map.* ";
		}
		sql += "FROM approval_module_map ";
		
		String AnyfieldSql = "";
		String AllFieldSql = "";
		
		if(p_searchCriteria != null)
		{
						
			
			Enumeration names = p_searchCriteria.keys();
			String str, value;
			
			AnyfieldSql = "(";
			
			if(p_searchCriteria.get("AnyField")!= null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase(""))
			{
				int i = 0;
				Iterator it = Approval_module_mapMAPS.GetInstance().java_anyfield_search_map.entrySet().iterator();
				while(it.hasNext())
				{
					if( i > 0)
		        	{
						AnyfieldSql+= " OR  ";
		        	}
					Map.Entry pair = (Map.Entry)it.next();
					AnyfieldSql+= pair.getKey() + " like '%" + p_searchCriteria.get("AnyField").toString() + "%'";
					i ++;
				}						
			}
			AnyfieldSql += ")";
			System.out.println("AnyfieldSql = " + AnyfieldSql);
			
			AllFieldSql = "(";
			int i = 0;
			while(names.hasMoreElements())
			{				
				str = (String) names.nextElement();
				value = (String)p_searchCriteria.get(str);
		        System.out.println(str + ": " + value);
		        if(Approval_module_mapMAPS.GetInstance().java_allfield_type_map.get(str.toLowerCase()) != null &&  !Approval_module_mapMAPS.GetInstance().java_allfield_type_map.get(str.toLowerCase()).equalsIgnoreCase("")
		        		&& !str.equalsIgnoreCase("AnyField")
		        		&& value != null && !value.equalsIgnoreCase(""))
		        {
					if(p_searchCriteria.get(str).equals("any"))
		        	{
		        		continue;
		        	}
					
		        	if( i > 0)
		        	{
		        		AllFieldSql+= " AND  ";
		        	}
		        	if(Approval_module_mapMAPS.GetInstance().java_allfield_type_map.get(str.toLowerCase()).equals("String")) 
		        	{
		        		AllFieldSql += "approval_module_map." + str.toLowerCase() + " like '%" + p_searchCriteria.get(str) + "%'";
		        	}
		        	else
		        	{
		        		AllFieldSql += "approval_module_map." + str.toLowerCase() + " = '" + p_searchCriteria.get(str) + "'";
		        	}
		        	i ++;
		        }
			}
			
			AllFieldSql += ")";
			System.out.println("AllFieldSql = " + AllFieldSql);
			
			
		}
		
		if(!AnyfieldSql.equals("()") && !AnyfieldSql.equals(""))
		{
			
		}
		sql += " WHERE ";
		sql += " approval_module_map.isDeleted = 0 ";
		
		if(!AnyfieldSql.equals("()") && !AnyfieldSql.equals(""))
		{
			sql += " AND " + AnyfieldSql;
			
		}
		if(!AllFieldSql.equals("()") && !AllFieldSql.equals(""))
		{			
			sql += " AND " + AllFieldSql;
		}

			
		sql += " order by approval_module_map.lastModificationTime desc ";

		printSql(sql);
		
		if(limit >= 0)
		{
			sql += " limit " + limit;
		}
		if(offset >= 0)
		{
			sql += " offset " + offset;
		}
		
		//System.out.println("-------------- sql = " + sql);
		
		return sql;
    }			
}
	