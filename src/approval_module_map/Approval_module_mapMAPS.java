package approval_module_map;
import java.util.*; 


public class Approval_module_mapMAPS 
{

	public HashMap<String, String> java_allfield_type_map = new HashMap<String, String>();
	public HashMap<String, String> java_anyfield_search_map = new HashMap<String, String>();
	public HashMap<String, String> java_DTO_map = new HashMap<String, String>();
	public HashMap<String, String> java_SQL_map = new HashMap<String, String>();
	public HashMap<String, String> java_Text_map = new HashMap<String, String>();
	
	private static Approval_module_mapMAPS self = null;
	
	private Approval_module_mapMAPS()
	{
		
		java_allfield_type_map.put("table_name".toLowerCase(), "String");
		java_allfield_type_map.put("has_add_approval".toLowerCase(), "Boolean");
		java_allfield_type_map.put("has_edit_approval".toLowerCase(), "Boolean");
		java_allfield_type_map.put("has_delete_approval".toLowerCase(), "Boolean");

		java_anyfield_search_map.put("approval_module_map.table_name".toLowerCase(), "String");
		java_anyfield_search_map.put("approval_module_map.has_add_approval".toLowerCase(), "Boolean");
		java_anyfield_search_map.put("approval_module_map.has_edit_approval".toLowerCase(), "Boolean");
		java_anyfield_search_map.put("approval_module_map.has_delete_approval".toLowerCase(), "Boolean");

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("tableName".toLowerCase(), "tableName".toLowerCase());
		java_DTO_map.put("hasAddApproval".toLowerCase(), "hasAddApproval".toLowerCase());
		java_DTO_map.put("hasEditApproval".toLowerCase(), "hasEditApproval".toLowerCase());
		java_DTO_map.put("hasDeleteApproval".toLowerCase(), "hasDeleteApproval".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("table_name".toLowerCase(), "tableName".toLowerCase());
		java_SQL_map.put("has_add_approval".toLowerCase(), "hasAddApproval".toLowerCase());
		java_SQL_map.put("has_edit_approval".toLowerCase(), "hasEditApproval".toLowerCase());
		java_SQL_map.put("has_delete_approval".toLowerCase(), "hasDeleteApproval".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Table Name".toLowerCase(), "tableName".toLowerCase());
		java_Text_map.put("Has Add Approval".toLowerCase(), "hasAddApproval".toLowerCase());
		java_Text_map.put("Has Edit Approval".toLowerCase(), "hasEditApproval".toLowerCase());
		java_Text_map.put("Has Delete Approval".toLowerCase(), "hasDeleteApproval".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}
	
	public static Approval_module_mapMAPS GetInstance()
	{
		if(self == null)
		{
			self = new Approval_module_mapMAPS ();
		}
		return self;
	}
	

}