package approval_module_map;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class Approval_module_mapRepository implements Repository {
	Approval_module_mapDAO approval_module_mapDAO = new Approval_module_mapDAO();
	
	
	static Logger logger = Logger.getLogger(Approval_module_mapRepository.class);
	Map<Long, Approval_module_mapDTO>mapOfApproval_module_mapDTOToiD;
	Map<String, Set<Approval_module_mapDTO> >mapOfApproval_module_mapDTOTotableName;
	Map<Boolean, Set<Approval_module_mapDTO> >mapOfApproval_module_mapDTOTohasAddApproval;
	Map<Boolean, Set<Approval_module_mapDTO> >mapOfApproval_module_mapDTOTohasEditApproval;
	Map<Boolean, Set<Approval_module_mapDTO> >mapOfApproval_module_mapDTOTohasDeleteApproval;
	Map<Long, Set<Approval_module_mapDTO> >mapOfApproval_module_mapDTOTolastModificationTime;


	static Approval_module_mapRepository instance = null;  
	private Approval_module_mapRepository(){
		mapOfApproval_module_mapDTOToiD = new ConcurrentHashMap<>();
		mapOfApproval_module_mapDTOTotableName = new ConcurrentHashMap<>();
		mapOfApproval_module_mapDTOTohasAddApproval = new ConcurrentHashMap<>();
		mapOfApproval_module_mapDTOTohasEditApproval = new ConcurrentHashMap<>();
		mapOfApproval_module_mapDTOTohasDeleteApproval = new ConcurrentHashMap<>();
		mapOfApproval_module_mapDTOTolastModificationTime = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Approval_module_mapRepository getInstance(){
		if (instance == null){
			instance = new Approval_module_mapRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		try {
			List<Approval_module_mapDTO> approval_module_mapDTOs = approval_module_mapDAO.getAllApproval_module_map(reloadAll);
			for(Approval_module_mapDTO approval_module_mapDTO : approval_module_mapDTOs) {
				Approval_module_mapDTO oldApproval_module_mapDTO = getApproval_module_mapDTOByID(approval_module_mapDTO.iD);
				if( oldApproval_module_mapDTO != null ) {
					mapOfApproval_module_mapDTOToiD.remove(oldApproval_module_mapDTO.iD);
				
					if(mapOfApproval_module_mapDTOTotableName.containsKey(oldApproval_module_mapDTO.tableName)) {
						mapOfApproval_module_mapDTOTotableName.get(oldApproval_module_mapDTO.tableName).remove(oldApproval_module_mapDTO);
					}
					if(mapOfApproval_module_mapDTOTotableName.get(oldApproval_module_mapDTO.tableName).isEmpty()) {
						mapOfApproval_module_mapDTOTotableName.remove(oldApproval_module_mapDTO.tableName);
					}
					
					if(mapOfApproval_module_mapDTOTohasAddApproval.containsKey(oldApproval_module_mapDTO.hasAddApproval)) {
						mapOfApproval_module_mapDTOTohasAddApproval.get(oldApproval_module_mapDTO.hasAddApproval).remove(oldApproval_module_mapDTO);
					}
					if(mapOfApproval_module_mapDTOTohasAddApproval.get(oldApproval_module_mapDTO.hasAddApproval).isEmpty()) {
						mapOfApproval_module_mapDTOTohasAddApproval.remove(oldApproval_module_mapDTO.hasAddApproval);
					}
					
					if(mapOfApproval_module_mapDTOTohasEditApproval.containsKey(oldApproval_module_mapDTO.hasEditApproval)) {
						mapOfApproval_module_mapDTOTohasEditApproval.get(oldApproval_module_mapDTO.hasEditApproval).remove(oldApproval_module_mapDTO);
					}
					if(mapOfApproval_module_mapDTOTohasEditApproval.get(oldApproval_module_mapDTO.hasEditApproval).isEmpty()) {
						mapOfApproval_module_mapDTOTohasEditApproval.remove(oldApproval_module_mapDTO.hasEditApproval);
					}
					
					if(mapOfApproval_module_mapDTOTohasDeleteApproval.containsKey(oldApproval_module_mapDTO.hasDeleteApproval)) {
						mapOfApproval_module_mapDTOTohasDeleteApproval.get(oldApproval_module_mapDTO.hasDeleteApproval).remove(oldApproval_module_mapDTO);
					}
					if(mapOfApproval_module_mapDTOTohasDeleteApproval.get(oldApproval_module_mapDTO.hasDeleteApproval).isEmpty()) {
						mapOfApproval_module_mapDTOTohasDeleteApproval.remove(oldApproval_module_mapDTO.hasDeleteApproval);
					}
					
					if(mapOfApproval_module_mapDTOTolastModificationTime.containsKey(oldApproval_module_mapDTO.lastModificationTime)) {
						mapOfApproval_module_mapDTOTolastModificationTime.get(oldApproval_module_mapDTO.lastModificationTime).remove(oldApproval_module_mapDTO);
					}
					if(mapOfApproval_module_mapDTOTolastModificationTime.get(oldApproval_module_mapDTO.lastModificationTime).isEmpty()) {
						mapOfApproval_module_mapDTOTolastModificationTime.remove(oldApproval_module_mapDTO.lastModificationTime);
					}
					
					
				}
				if(approval_module_mapDTO.isDeleted == 0) 
				{
					
					mapOfApproval_module_mapDTOToiD.put(approval_module_mapDTO.iD, approval_module_mapDTO);
				
					if( ! mapOfApproval_module_mapDTOTotableName.containsKey(approval_module_mapDTO.tableName)) {
						mapOfApproval_module_mapDTOTotableName.put(approval_module_mapDTO.tableName, new HashSet<>());
					}
					mapOfApproval_module_mapDTOTotableName.get(approval_module_mapDTO.tableName).add(approval_module_mapDTO);
					
					if( ! mapOfApproval_module_mapDTOTohasAddApproval.containsKey(approval_module_mapDTO.hasAddApproval)) {
						mapOfApproval_module_mapDTOTohasAddApproval.put(approval_module_mapDTO.hasAddApproval, new HashSet<>());
					}
					mapOfApproval_module_mapDTOTohasAddApproval.get(approval_module_mapDTO.hasAddApproval).add(approval_module_mapDTO);
					
					if( ! mapOfApproval_module_mapDTOTohasEditApproval.containsKey(approval_module_mapDTO.hasEditApproval)) {
						mapOfApproval_module_mapDTOTohasEditApproval.put(approval_module_mapDTO.hasEditApproval, new HashSet<>());
					}
					mapOfApproval_module_mapDTOTohasEditApproval.get(approval_module_mapDTO.hasEditApproval).add(approval_module_mapDTO);
					
					if( ! mapOfApproval_module_mapDTOTohasDeleteApproval.containsKey(approval_module_mapDTO.hasDeleteApproval)) {
						mapOfApproval_module_mapDTOTohasDeleteApproval.put(approval_module_mapDTO.hasDeleteApproval, new HashSet<>());
					}
					mapOfApproval_module_mapDTOTohasDeleteApproval.get(approval_module_mapDTO.hasDeleteApproval).add(approval_module_mapDTO);
					
					if( ! mapOfApproval_module_mapDTOTolastModificationTime.containsKey(approval_module_mapDTO.lastModificationTime)) {
						mapOfApproval_module_mapDTOTolastModificationTime.put(approval_module_mapDTO.lastModificationTime, new HashSet<>());
					}
					mapOfApproval_module_mapDTOTolastModificationTime.get(approval_module_mapDTO.lastModificationTime).add(approval_module_mapDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Approval_module_mapDTO> getApproval_module_mapList() {
		List <Approval_module_mapDTO> approval_module_maps = new ArrayList<Approval_module_mapDTO>(this.mapOfApproval_module_mapDTOToiD.values());
		return approval_module_maps;
	}
	
	
	public Approval_module_mapDTO getApproval_module_mapDTOByID( long ID){
		return mapOfApproval_module_mapDTOToiD.get(ID);
	}
	
	
	public List<Approval_module_mapDTO> getApproval_module_mapDTOBytable_name(String table_name) {
		return new ArrayList<>( mapOfApproval_module_mapDTOTotableName.getOrDefault(table_name,new HashSet<>()));
	}
	
	
	public List<Approval_module_mapDTO> getApproval_module_mapDTOByhas_add_approval(boolean has_add_approval) {
		return new ArrayList<>( mapOfApproval_module_mapDTOTohasAddApproval.getOrDefault(has_add_approval,new HashSet<>()));
	}
	
	
	public List<Approval_module_mapDTO> getApproval_module_mapDTOByhas_edit_approval(boolean has_edit_approval) {
		return new ArrayList<>( mapOfApproval_module_mapDTOTohasEditApproval.getOrDefault(has_edit_approval,new HashSet<>()));
	}
	
	
	public List<Approval_module_mapDTO> getApproval_module_mapDTOByhas_delete_approval(boolean has_delete_approval) {
		return new ArrayList<>( mapOfApproval_module_mapDTOTohasDeleteApproval.getOrDefault(has_delete_approval,new HashSet<>()));
	}
	
	
	public List<Approval_module_mapDTO> getApproval_module_mapDTOBylastModificationTime(long lastModificationTime) {
		return new ArrayList<>( mapOfApproval_module_mapDTOTolastModificationTime.getOrDefault(lastModificationTime,new HashSet<>()));
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "approval_module_map";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


