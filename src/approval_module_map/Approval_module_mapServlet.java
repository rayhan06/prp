package approval_module_map;

import java.io.IOException;
import java.io.*;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import org.apache.log4j.Logger;

import login.LoginDTO;
import permission.MenuConstants;
import role.PermissionRepository;


import sessionmanager.SessionConstants;

import user.UserDTO;
import user.UserRepository;

import util.RecordNavigationManager2;

import java.util.*;
import javax.servlet.http.*;
import java.util.UUID;

import approval_module_map.Constants;



import pb.*;
import pbReport.*;

/**
 * Servlet implementation class Approval_module_mapServlet
 */
@WebServlet("/Approval_module_mapServlet")
@MultipartConfig
public class Approval_module_mapServlet extends HttpServlet 
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Approval_module_mapServlet.class);
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Approval_module_mapServlet() 
	{
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		System.out.println("In doget request = " + request);
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		
		
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		try
		{
			String actionType = request.getParameter("actionType");
			if(actionType.equals("getAddPage"))
			{
				////if(PermissionRepository.getInstance().checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.APPROVAL_MODULE_MAP_ADD))
				{
					getAddPage(request, response);
				}
				
			}
			else if(actionType.equals("getEditPage"))
			{
				//if(PermissionRepository.getInstance().checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.APPROVAL_MODULE_MAP_UPDATE))
				{
					getApproval_module_map(request, response);
				}
							
			}
			else if(actionType.equals("getURL"))
			{
				String URL = request.getParameter("URL");
				System.out.println("URL = " + URL);
				response.sendRedirect(URL);			
			}
			else if(actionType.equals("search"))
			{
				System.out.println("search requested");
				//if(PermissionRepository.getInstance().checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.APPROVAL_MODULE_MAP_SEARCH))
				{
					searchApproval_module_map(request, response);
				}
								
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}

	private void getAddPage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		request.setAttribute("ID", -1L);
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("approval_module_map/approval_module_mapEdit.jsp");
		requestDispatcher.forward(request, response);
	}
	private String getFileName(final Part part) 
	{
	    final String partHeader = part.getHeader("content-disposition");
	    System.out.println("Part Header = {0}" +  partHeader);
	    for (String content : part.getHeader("content-disposition").split(";")) {
	        if (content.trim().startsWith("filename")) {
	            return content.substring(
	                    content.indexOf('=') + 1).trim().replace("\"", "");
	        }
	    }
	    return null;
	}

	
	

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		// TODO Auto-generated method stub
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		
		
		System.out.println("doPost");
		
		try
		{
			String actionType = request.getParameter("actionType");
			System.out.println("actionType = " + actionType);
			if(actionType.equals("add"))
			{
				
				//if(PermissionRepository.getInstance().checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.APPROVAL_MODULE_MAP_ADD))
				{
					System.out.println("going to  addApproval_module_map ");
					addApproval_module_map(request, response, true);
				}
			
				
			}
			else if(actionType.equals("edit"))
			{
				
				//if(PermissionRepository.getInstance().checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.APPROVAL_MODULE_MAP_UPDATE))
				{
					addApproval_module_map(request, response, false);
				}
				
			}
			else if(actionType.equals("delete"))
			{
				deleteApproval_module_map(request, response);
			}
			else if(actionType.equals("search"))
			{
				//if(PermissionRepository.getInstance().checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.APPROVAL_MODULE_MAP_SEARCH))
				{
					searchApproval_module_map(request, response);
				}
				
			}
			else if(actionType.equals("getGeo"))
			{
				System.out.println("going to geoloc ");
				request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
			}
			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}
	private void addApproval_module_map(HttpServletRequest request, HttpServletResponse response, Boolean addFlag) throws IOException 
	{
		// TODO Auto-generated method stub
		try 
		{
			request.setAttribute("failureMessage", "");
			System.out.println("%%%% addApproval_module_map");
			String path = getServletContext().getRealPath("/img2/");
			Approval_module_mapDAO approval_module_mapDAO = new Approval_module_mapDAO();
			Approval_module_mapDTO approval_module_mapDTO;
			String FileNamePrefix;
			if(addFlag == true)
			{
				approval_module_mapDTO = new Approval_module_mapDTO();
				FileNamePrefix = UUID.randomUUID().toString().substring(0, 10);
			}
			else
			{
				approval_module_mapDTO = approval_module_mapDAO.getApproval_module_mapDTOByID(Long.parseLong(request.getParameter("identity")));
				FileNamePrefix = request.getParameter("identity");
			}
			
			String Value = "";
			Value = request.getParameter("tableName");
			System.out.println("tableName = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				approval_module_mapDTO.tableName = (Value);
			}
			else
			{
				System.out.println("FieldName has a null value, not updating" + " = " + Value);
			}
			Value = request.getParameter("hasAddApproval");
			System.out.println("hasAddApproval = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				approval_module_mapDTO.hasAddApproval = Boolean.parseBoolean(Value);
			}
			else
			{
				System.out.println("FieldName has a null value, not updating" + " = " + Value);
			}
			Value = request.getParameter("hasEditApproval");
			System.out.println("hasEditApproval = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				approval_module_mapDTO.hasEditApproval = Boolean.parseBoolean(Value);
			}
			else
			{
				System.out.println("FieldName has a null value, not updating" + " = " + Value);
			}
			Value = request.getParameter("hasDeleteApproval");
			System.out.println("hasDeleteApproval = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				approval_module_mapDTO.hasDeleteApproval = Boolean.parseBoolean(Value);
			}
			else
			{
				System.out.println("FieldName has a null value, not updating" + " = " + Value);
			}
			
			System.out.println("Done adding  addApproval_module_map dto = " + approval_module_mapDTO);
			
			if(addFlag == true)
			{
				approval_module_mapDAO.addApproval_module_map(approval_module_mapDTO);
			}
			else
			{
				approval_module_mapDAO.updateApproval_module_map(approval_module_mapDTO);
				
			}
			
			
			
			
			
			
			
			
			String inPlaceSubmit = request.getParameter("inplacesubmit");
			
			if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
			{
				getApproval_module_map(request, response);
			}
			else
			{
				response.sendRedirect("Approval_module_mapServlet?actionType=search");
			}
					
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	



	
	
	

	private void deleteApproval_module_map(HttpServletRequest request, HttpServletResponse response) throws IOException 
	{				
		try 
		{
			String[] IDsToDelete = request.getParameterValues("ID");
			for(int i = 0; i < IDsToDelete.length; i ++)
			{
				long id = Long.parseLong(IDsToDelete[i]);
				System.out.println("###DELETING " + IDsToDelete[i]);				
				new Approval_module_mapDAO().deleteApproval_module_mapByID(id);
			}			
		}
		catch (Exception ex) 
		{
			logger.debug(ex);
		}
		response.sendRedirect("Approval_module_mapServlet?actionType=search");
	}

	private void getApproval_module_map(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		System.out.println("in getApproval_module_map");
		Approval_module_mapDTO approval_module_mapDTO = null;
		try 
		{
			approval_module_mapDTO = new Approval_module_mapDAO().getApproval_module_mapDTOByID(Long.parseLong(request.getParameter("ID")));
			request.setAttribute("ID", approval_module_mapDTO.iD);
			request.setAttribute("approval_module_mapDTO",approval_module_mapDTO);
			
			String URL= "";
			
			String inPlaceEdit = request.getParameter("inplaceedit");
			String inPlaceSubmit = request.getParameter("inplacesubmit");
			String getBodyOnly = request.getParameter("getBodyOnly");
			
			if(inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase(""))
			{
				URL = "approval_module_map/approval_module_mapInPlaceEdit.jsp";	
				request.setAttribute("inplaceedit","");				
			}
			else if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
			{
				URL = "approval_module_map/approval_module_mapSearchRow.jsp";
				request.setAttribute("inplacesubmit","");					
			}
			else
			{
				if(getBodyOnly  != null && !getBodyOnly.equalsIgnoreCase(""))
				{
					URL = "approval_module_map/approval_module_mapEditBody.jsp?actionType=edit";
				}
				else
				{
					URL = "approval_module_map/approval_module_mapEdit.jsp?actionType=edit";
				}				
			}
			
			RequestDispatcher rd = request.getRequestDispatcher(URL);
			rd.forward(request, response);
		}
		catch (NumberFormatException e) 
		{
			e.printStackTrace();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	private void searchApproval_module_map(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		System.out.println("in  searchApproval_module_map 1");
        Approval_module_mapDAO approval_module_mapDAO = new Approval_module_mapDAO();
		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
		String ajax = request.getParameter("ajax");
		boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);
		
        RecordNavigationManager2 rnManager = new RecordNavigationManager2(
			SessionConstants.NAV_APPROVAL_MODULE_MAP,
			request,
			approval_module_mapDAO,
			SessionConstants.VIEW_APPROVAL_MODULE_MAP,
			SessionConstants.SEARCH_APPROVAL_MODULE_MAP,
			"approval_module_map");
        try
        {
			System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        }
        catch(Exception e)
        {
			System.out.println("failed to dojob" + e);
        }

        RequestDispatcher rd;
        if(hasAjax == false)
        {
        	System.out.println("Going to approval_module_map/approval_module_mapSearch.jsp");
        	rd = request.getRequestDispatcher("approval_module_map/approval_module_mapSearch.jsp");
        }
        else
        {
        	System.out.println("Going to approval_module_map/approval_module_mapSearchForm.jsp");
        	rd = request.getRequestDispatcher("approval_module_map/approval_module_mapSearchForm.jsp");
        }
		rd.forward(request, response);
	}
	
}

