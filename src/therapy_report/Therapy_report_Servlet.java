package therapy_report;


import java.io.IOException;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



import language.LC;
import language.LM;

import login.LoginDTO;
import permission.MenuConstants;

import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;

import util.ReportRequestHandler;
import pbReport.*;






@WebServlet("/Therapy_report_Servlet")
public class Therapy_report_Servlet  extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	String[][] Criteria =
	{
		{"criteria","","employee_record_id","=","","String","","","any","userName",LC.PHARMACY_REPORT_WHERE_EMPLOYEEID + "", "userNameToEmployeeRecordId"},
		{"criteria","","actual_therapy_date",">=","AND","long","","",Long.MIN_VALUE + "","startDate", LC.HM_START_DATE + ""},		
		{"criteria","","actual_therapy_date","<=","AND","long","","",Long.MAX_VALUE + "","endDate", LC.HM_END_DATE + ""},
		{"criteria","","therapy_treatment_cat","=","AND","int","","","any","therapyCat", LC.THERAPY_REPORT_WHERE_THERAPYCAT + ""},
		{"criteria","","therapist_organogram_id","=","AND","int","","","any","nurse_organogram_id",LC.HM_NURSE + ""},
	};
	
	String[][] Display =
	{
		{"display","","actual_therapy_date","date",""},
		{"display","","therapist_organogram_id","organogram_to_name",""},	
		{"display","","employee_record_id","erIdToName",""},		
		{"display","","therapy_treatment_cat","cat",""}
	};
	
	String GroupBy = " therapy_treatment_cat, actual_therapy_date, employee_record_id, therapist_organogram_id ";
	String OrderBY = "actual_therapy_date DESC";
	
	ReportRequestHandler reportRequestHandler;
	
	public Therapy_report_Servlet(){

	}	
	private final ReportService reportService = new ReportService();
	
	private String sql;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String language = "english";
		if(userDTO!= null && userDTO.languageID == SessionConstants.BANGLA)
		{
			language = "bangla";
		}
				
		String actionType = request.getParameter("actionType");

		System.out.println("In ssservlet doget, actiontype = " + actionType);
		
		sql = "physiotherapy_schedule join users on username = therapy_recipient_user_name";

		Display[0][4] = LM.getText(LC.THERAPY_REPORT_SELECT_ACTUALTHERAPYDATE, loginDTO);
		Display[1][4] = language.equalsIgnoreCase("english")?"Therapist":"থেরাপিদাদাতা";
		Display[2][4] = LM.getText(LC.HM_EMPLOYEE_ID, loginDTO);
		Display[3][4] = LM.getText(LC.THERAPY_REPORT_SELECT_THERAPYCAT, loginDTO);


		
		String reportName = LM.getText(LC.THERAPY_REPORT_OTHER_THERAPY_REPORT, loginDTO);
		
		reportRequestHandler = new ReportRequestHandler(null,
				Criteria, Display, GroupBy, OrderBY, sql,
				reportService);

		
		reportRequestHandler.handleReportGet(request, response, userDTO, "therapy_report",
				MenuConstants.THERAPY_REPORT_DETAILS, language, reportName, "therapy_report");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		doGet(request, response);
	}
}
