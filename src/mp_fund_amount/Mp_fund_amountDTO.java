package mp_fund_amount;

import util.CommonDTO;


public class Mp_fund_amountDTO extends CommonDTO {
    public long electionDetailsId = -1;
    public long electionConstituencyId = -1;
    public int economicYearStart = -1;
    public long totalAmount = 0;
    public long amountLeft = -1;
    public long insertedBy = -1;
    public long insertionTime = -1;
    public long modifiedBy = -1;

    @Override
    public String toString() {
        return "$Mp_fund_amountDTO[" +
                " iD = " + iD +
                " electionDetailsId = " + electionDetailsId +
                " electionConstituencyId = " + electionConstituencyId +
                " economicYearStart = " + economicYearStart +
                " totalAmount = " + totalAmount +
                " amountLeft = " + amountLeft +
                " insertedBy = " + insertedBy +
                " insertionTime = " + insertionTime +
                " isDeleted = " + isDeleted +
                " modifiedBy = " + modifiedBy +
                " lastModificationTime = " + lastModificationTime +
                "]";
    }
}