package mp_fund_amount;

import common.BaseServlet;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import permission.MenuConstants;
import user.UserDTO;
import util.CommonDTO;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;


@WebServlet("/Mp_fund_amountServlet")
@MultipartConfig
public class Mp_fund_amountServlet extends BaseServlet {
    private static final long serialVersionUID = 1L;
    public static final long fixedTotalMpFundAmountId = 1L;

    private CommonDTO addFixedTotalFundAmount(HttpServletRequest request, long userId) throws Exception {
        boolean addFlag = false;
        Mp_fund_amountDTO mpFundAmountDTO = Mp_fund_amountDAO.getInstance().getDTOFromID(fixedTotalMpFundAmountId);
        long currentTime = System.currentTimeMillis();
        if (mpFundAmountDTO == null) {
            addFlag = true;
            mpFundAmountDTO = new Mp_fund_amountDTO();
            mpFundAmountDTO.iD = fixedTotalMpFundAmountId;
            mpFundAmountDTO.insertedBy = userId;
            mpFundAmountDTO.insertionTime = currentTime;
        }
        mpFundAmountDTO.modifiedBy = userId;
        mpFundAmountDTO.lastModificationTime = currentTime;
        mpFundAmountDTO.totalAmount = Long.parseLong(Jsoup.clean(
                request.getParameter("totalAmount"), Whitelist.simpleText()
        ));
        if (addFlag) {
            Mp_fund_amountDAO.getInstance().addWithSetId(mpFundAmountDTO);
        } else {
            Mp_fund_amountDAO.getInstance().update(mpFundAmountDTO);
        }
        return mpFundAmountDTO;
    }

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        return addFixedTotalFundAmount(request, userDTO.ID);
    }

    public static long getFixedTotalFundAmount() {
        Mp_fund_amountDTO mpFundAmountDTO = Mp_fund_amountDAO.getInstance().getDTOFromID(fixedTotalMpFundAmountId);
        return mpFundAmountDTO == null? 0 : mpFundAmountDTO.totalAmount;
    }

    @Override
    public String getTableName() {
        return Mp_fund_amountDAO.getInstance().getTableName();
    }

    @Override
    public String getServletName() {
        return "Mp_fund_amountServlet";
    }

    @Override
    public Mp_fund_amountDAO getCommonDAOService() {
        return Mp_fund_amountDAO.getInstance();
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[]{MenuConstants.MP_FUND_AMOUNT_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[]{MenuConstants.MP_FUND_AMOUNT_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[]{MenuConstants.MP_FUND_AMOUNT_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Mp_fund_amountServlet.class;
    }
}