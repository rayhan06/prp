package mp_fund_amount;

import audit.log.AuditLogDAO;
import audit.log.LogEvent;
import common.CommonDAOService;
import common.ConnectionAndStatementUtil;
import org.apache.log4j.Logger;
import sessionmanager.SessionConstants;
import util.CommonDTO;
import util.HttpRequestUtils;
import util.LockManager;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@SuppressWarnings({"Duplicates"})
public class Mp_fund_amountDAO implements CommonDAOService<Mp_fund_amountDTO> {
    private static final Logger logger = Logger.getLogger(Mp_fund_amountDAO.class);

    private static final String addQuery =
            "INSERT INTO {tableName} (election_details_id,election_constituency_id,economic_year_start,total_amount,"
                    .concat("amount_left,modified_by,lastModificationTime,inserted_by,insertion_time,isDeleted,ID) ")
                    .concat("VALUES (?,?,?,?,?,?,?,?,?,?,?)");

    private static final String updateQuery =
            "UPDATE {tableName} SET election_details_id=?,election_constituency_id=?,economic_year_start=?,"
                    .concat("total_amount=?,amount_left=?,modified_by=?,lastModificationTime=? WHERE ID=?");

    private static final String getAmountOfMp =
            "SELECT * FROM mp_fund_amount WHERE election_details_id = ? AND election_constituency_id = ? AND "
                    .concat("economic_year_start = ? AND isDeleted = 0");

    private Mp_fund_amountDAO() {
        searchMap.put("election_details_id", " and (election_details_id = ?)");
        searchMap.put("election_constituency_id", " and (election_constituency_id = ?)");
    }

    private final Map<String, String> searchMap = new HashMap<>();

    private static class LazyLoader {
        static final Mp_fund_amountDAO INSTANCE = new Mp_fund_amountDAO();
    }

    public static Mp_fund_amountDAO getInstance() {
        return LazyLoader.INSTANCE;
    }

    @Override
    public void set(PreparedStatement ps, Mp_fund_amountDTO dto, boolean isInsert) throws SQLException {
        int index = 0;
        ps.setLong(++index, dto.electionDetailsId);
        ps.setLong(++index, dto.electionConstituencyId);
        ps.setInt(++index, dto.economicYearStart);
        ps.setLong(++index, dto.totalAmount);
        ps.setLong(++index, dto.amountLeft);
        ps.setLong(++index, dto.modifiedBy);
        ps.setLong(++index, dto.lastModificationTime);
        if (isInsert) {
            ps.setLong(++index, dto.insertedBy);
            ps.setLong(++index, dto.insertionTime);
            ps.setInt(++index, 0 /* isDeleted */);
        }
        ps.setLong(++index, dto.iD);
    }

    @Override
    public Mp_fund_amountDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Mp_fund_amountDTO dto = new Mp_fund_amountDTO();
            dto.iD = rs.getLong("ID");
            dto.electionDetailsId = rs.getLong("election_details_id");
            dto.electionConstituencyId = rs.getLong("election_constituency_id");
            dto.economicYearStart = rs.getInt("economic_year_start");
            dto.totalAmount = rs.getLong("total_amount");
            dto.amountLeft = rs.getLong("amount_left");
            dto.insertedBy = rs.getLong("inserted_by");
            dto.insertionTime = rs.getLong("insertion_time");
            dto.isDeleted = rs.getInt("isDeleted");
            dto.modifiedBy = rs.getLong("modified_by");
            dto.lastModificationTime = rs.getLong("lastModificationTime");
            return dto;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    public Mp_fund_amountDTO getDTOByID(long id) {
        return getDTOFromID(id);
    }

    @Override
    public String getTableName() {
        return "mp_fund_amount";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    @Override
    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Mp_fund_amountDTO) commonDTO, addQuery, true);
    }

    @SuppressWarnings("UnusedReturnValue")
    public long addWithSetId(CommonDTO commonDTO) throws Exception {
        String sql = addQuery.replace("{tableName}", getTableName());
        return (long) ConnectionAndStatementUtil.getWriteConnectionAndPrepareStatement(obj -> {
            PreparedStatement ps = (PreparedStatement) obj.getStatement();
            long returnId = -1L;
            try {
                set(ps, (Mp_fund_amountDTO) commonDTO, true);
                logger.debug(ps);
                ps.execute();
                returnId = commonDTO.iD;
                AuditLogDAO.add(
                        null, (Mp_fund_amountDTO) commonDTO, LogEvent.ADD, getTableName(),
                        HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().ipAddress,
                        HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO.ID,
                        HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO.organogramID
                );
                recordUpdateTime(obj.getConnection(), getTableName());
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return returnId;
        }, sql);
    }

    @Override
    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Mp_fund_amountDTO) commonDTO, updateQuery, false);
    }

    public Mp_fund_amountDTO getDtoOfMp(long electionDetailsId, long electionConstituencyId, int economicStartYear) {
        return ConnectionAndStatementUtil.getT(
                getAmountOfMp,
                Arrays.asList(electionDetailsId, electionConstituencyId, economicStartYear),
                this::buildObjectFromResultSet
        );
    }

    private Mp_fund_amountDTO createDTO(long electionDetailsId, long electionConstituencyId,
                                        int economicStartYear, long requestedBy, long requestTime) {
        Mp_fund_amountDTO mpFundAmountDTO = new Mp_fund_amountDTO();
        mpFundAmountDTO.insertedBy = mpFundAmountDTO.modifiedBy = requestedBy;
        mpFundAmountDTO.insertionTime = mpFundAmountDTO.lastModificationTime = requestTime;
        mpFundAmountDTO.electionDetailsId = electionDetailsId;
        mpFundAmountDTO.electionConstituencyId = electionConstituencyId;
        mpFundAmountDTO.economicYearStart = economicStartYear;
        mpFundAmountDTO.totalAmount = mpFundAmountDTO.amountLeft = Mp_fund_amountServlet.getFixedTotalFundAmount();
        return mpFundAmountDTO;
    }

    public Mp_fund_amountDTO getOrCreateDtoOfMp(long electionDetailsId, long electionConstituencyId,
                                                int economicStartYear, long requestedBy, long requestTime) throws Exception {
        Mp_fund_amountDTO mpFundAmountDTO = getDtoOfMp(electionDetailsId, electionConstituencyId, economicStartYear);
        if (mpFundAmountDTO == null) {
            String lockId = "Mp_fund_amountDTO-" + electionDetailsId + "-" + electionConstituencyId + "-" + economicStartYear;
            synchronized (LockManager.getLock(lockId)) {
                mpFundAmountDTO = getDtoOfMp(electionDetailsId, electionConstituencyId, economicStartYear);
                if (mpFundAmountDTO == null) {
                    mpFundAmountDTO = createDTO(
                            electionDetailsId, electionConstituencyId,
                            economicStartYear, requestedBy, requestTime
                    );
                    mpFundAmountDTO.iD = add(mpFundAmountDTO);
                }
            }
        }
        return mpFundAmountDTO;
    }

    public Mp_fund_amountDTO prepareMpFundDTO(long electionDetailsId, long electionConstituencyId, int economicStartYear) throws Exception {
        Mp_fund_amountDTO mpFundAmountDTO = getDtoOfMp(electionDetailsId, electionConstituencyId, economicStartYear);
        if (mpFundAmountDTO == null) {
            mpFundAmountDTO = createDTO(
                    electionDetailsId, electionConstituencyId,
                    economicStartYear, -1, SessionConstants.MIN_DATE
            );
        }
        return mpFundAmountDTO;
    }

    public void allocateFund(long mpFundAmountId, long applicationTotalAmount, long requesterId, long requestTime) throws Exception {
        String lockId = "Mp_fund_amountDTO-" + mpFundAmountId;
        synchronized (LockManager.getLock(lockId)) {
            Mp_fund_amountDTO fundAmountDTO = getDTOByID(mpFundAmountId);
            if (applicationTotalAmount > fundAmountDTO.amountLeft)
                throw new Exception("Not enough fund to allocate for this bill!");
            fundAmountDTO.amountLeft -= applicationTotalAmount;
            fundAmountDTO.modifiedBy = requesterId;
            fundAmountDTO.lastModificationTime = requestTime;
            update(fundAmountDTO);
        }
    }
}
	