package db.tables;

public class cadre_changes_history {
    private long id;
    private long employee_record_id;
    private String prev_username;
    private long prev_is_cadre;
    private long prev_employee_cadre_id;
    private long prev_employee_batch_id;
    private String prev_identity_no;
    private String prev_appointment_memo_no;
    private long prev_joining_date;
    private long modified_by;
    private boolean isDeleted;
    private long lastModificationTime;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getEmployee_record_id() {
        return employee_record_id;
    }

    public void setEmployee_record_id(long employee_record_id) {
        this.employee_record_id = employee_record_id;
    }

    public String getPrev_username() {
        return prev_username;
    }

    public void setPrev_username(String prev_username) {
        this.prev_username = prev_username;
    }

    public long getPrev_is_cadre() {
        return prev_is_cadre;
    }

    public void setPrev_is_cadre(long prev_is_cadre) {
        this.prev_is_cadre = prev_is_cadre;
    }

    public long getPrev_employee_cadre_id() {
        return prev_employee_cadre_id;
    }

    public void setPrev_employee_cadre_id(long prev_employee_cadre_id) {
        this.prev_employee_cadre_id = prev_employee_cadre_id;
    }

    public long getPrev_employee_batch_id() {
        return prev_employee_batch_id;
    }

    public void setPrev_employee_batch_id(long prev_employee_batch_id) {
        this.prev_employee_batch_id = prev_employee_batch_id;
    }

    public String getPrev_identity_no() {
        return prev_identity_no;
    }

    public void setPrev_identity_no(String prev_identity_no) {
        this.prev_identity_no = prev_identity_no;
    }

    public String getPrev_appointment_memo_no() {
        return prev_appointment_memo_no;
    }

    public void setPrev_appointment_memo_no(String prev_appointment_memo_no) {
        this.prev_appointment_memo_no = prev_appointment_memo_no;
    }

    public long getModified_by() {
        return modified_by;
    }

    public void setModified_by(long modified_by) {
        this.modified_by = modified_by;
    }

    public long getPrev_joining_date() {
        return prev_joining_date;
    }

    public void setPrev_joining_date(long prev_joining_date) {
        this.prev_joining_date = prev_joining_date;
    }

    public boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(boolean deleted) {
        isDeleted = deleted;
    }

    public long getLastModificationTime() {
        return lastModificationTime;
    }

    public void setLastModificationTime(long lastModificationTime) {
        this.lastModificationTime = lastModificationTime;
    }
}
