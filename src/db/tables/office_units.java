package db.tables;

public class office_units {
    private long id;
    private int office_ministry_id;
    private int office_layer_id;
    private int office_id;
    private long office_origin_unit_id;
    private String unit_name_bng;
    private String unit_name_eng;
    private String office_unit_category;
    private long parent_unit_id;
    private int parent_origin_unit_id;
    private String unit_nothi_code;
    private int unit_level;
    private int sarok_no_start;
    private String email;
    private String phone;
    private String fax;
    private int active_status;
    private int created_by;
    private int modified_by;
    private long created;
    private long modified;
    private int status;
    private int isDeleted;
    private long lastModificationTime;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getOffice_ministry_id() {
        return office_ministry_id;
    }

    public void setOffice_ministry_id(int office_ministry_id) {
        this.office_ministry_id = office_ministry_id;
    }

    public int getOffice_layer_id() {
        return office_layer_id;
    }

    public void setOffice_layer_id(int office_layer_id) {
        this.office_layer_id = office_layer_id;
    }

    public int getOffice_id() {
        return office_id;
    }

    public void setOffice_id(int office_id) {
        this.office_id = office_id;
    }

    public long getOffice_origin_unit_id() {
        return office_origin_unit_id;
    }

    public void setOffice_origin_unit_id(long office_origin_unit_id) {
        this.office_origin_unit_id = office_origin_unit_id;
    }

    public String getUnit_name_bng() {
        return unit_name_bng;
    }

    public void setUnit_name_bng(String unit_name_bng) {
        this.unit_name_bng = unit_name_bng;
    }

    public String getUnit_name_eng() {
        return unit_name_eng;
    }

    public void setUnit_name_eng(String unit_name_eng) {
        this.unit_name_eng = unit_name_eng;
    }

    public String getOffice_unit_category() {
        return office_unit_category;
    }

    public void setOffice_unit_category(String office_unit_category) {
        this.office_unit_category = office_unit_category;
    }

    public long getParent_unit_id() {
        return parent_unit_id;
    }

    public void setParent_unit_id(long parent_unit_id) {
        this.parent_unit_id = parent_unit_id;
    }

    public int getParent_origin_unit_id() {
        return parent_origin_unit_id;
    }

    public void setParent_origin_unit_id(int parent_origin_unit_id) {
        this.parent_origin_unit_id = parent_origin_unit_id;
    }

    public String getUnit_nothi_code() {
        return unit_nothi_code;
    }

    public void setUnit_nothi_code(String unit_nothi_code) {
        this.unit_nothi_code = unit_nothi_code;
    }

    public int getUnit_level() {
        return unit_level;
    }

    public void setUnit_level(int unit_level) {
        this.unit_level = unit_level;
    }

    public int getSarok_no_start() {
        return sarok_no_start;
    }

    public void setSarok_no_start(int sarok_no_start) {
        this.sarok_no_start = sarok_no_start;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public int getActive_status() {
        return active_status;
    }

    public void setActive_status(int active_status) {
        this.active_status = active_status;
    }

    public int getCreated_by() {
        return created_by;
    }

    public void setCreated_by(int created_by) {
        this.created_by = created_by;
    }

    public int getModified_by() {
        return modified_by;
    }

    public void setModified_by(int modified_by) {
        this.modified_by = modified_by;
    }

    public long getCreated() {
        return created;
    }

    public void setCreated(long created) {
        this.created = created;
    }

    public long getModified() {
        return modified;
    }

    public void setModified(long modified) {
        this.modified = modified;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(int isDeleted) {
        this.isDeleted = isDeleted;
    }

    public long getLastModificationTime() {
        return lastModificationTime;
    }

    public void setLastModificationTime(long lastModificationTime) {
        this.lastModificationTime = lastModificationTime;
    }
}
