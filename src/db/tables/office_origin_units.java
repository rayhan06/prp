package db.tables;

public class office_origin_units {
    private long id;
    private int office_ministry_id;
    private int office_layer_id;
    private int office_origin_id;
    private String unit_name_bng;
    private String unit_name_eng;
    private String office_unit_category;
    private int parent_unit_id;
    private int unit_level;
    private int status;
    private int created_by;
    private int modified_by;
    private long created;
    private long modified;
    private int isDeleted;
    private long lastModificationTime;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getOffice_ministry_id() {
        return office_ministry_id;
    }

    public void setOffice_ministry_id(int office_ministry_id) {
        this.office_ministry_id = office_ministry_id;
    }

    public int getOffice_layer_id() {
        return office_layer_id;
    }

    public void setOffice_layer_id(int office_layer_id) {
        this.office_layer_id = office_layer_id;
    }

    public int getOffice_origin_id() {
        return office_origin_id;
    }

    public void setOffice_origin_id(int office_origin_id) {
        this.office_origin_id = office_origin_id;
    }

    public String getUnit_name_bng() {
        return unit_name_bng;
    }

    public void setUnit_name_bng(String unit_name_bng) {
        this.unit_name_bng = unit_name_bng;
    }

    public String getUnit_name_eng() {
        return unit_name_eng;
    }

    public void setUnit_name_eng(String unit_name_eng) {
        this.unit_name_eng = unit_name_eng;
    }

    public String getOffice_unit_category() {
        return office_unit_category;
    }

    public void setOffice_unit_category(String office_unit_category) {
        this.office_unit_category = office_unit_category;
    }

    public int getParent_unit_id() {
        return parent_unit_id;
    }

    public void setParent_unit_id(int parent_unit_id) {
        this.parent_unit_id = parent_unit_id;
    }

    public int getUnit_level() {
        return unit_level;
    }

    public void setUnit_level(int unit_level) {
        this.unit_level = unit_level;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getCreated_by() {
        return created_by;
    }

    public void setCreated_by(int created_by) {
        this.created_by = created_by;
    }

    public int getModified_by() {
        return modified_by;
    }

    public void setModified_by(int modified_by) {
        this.modified_by = modified_by;
    }

    public long getCreated() {
        return created;
    }

    public void setCreated(long created) {
        this.created = created;
    }

    public long getModified() {
        return modified;
    }

    public void setModified(long modified) {
        this.modified = modified;
    }

    public int getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(int isDeleted) {
        this.isDeleted = isDeleted;
    }

    public long getLastModificationTime() {
        return lastModificationTime;
    }

    public void setLastModificationTime(long lastModificationTime) {
        this.lastModificationTime = lastModificationTime;
    }
}
