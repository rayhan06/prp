package metricsolver2d;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.sql.SQLException;
import common.CommonDAOService;


import org.apache.log4j.Logger;
import util.*;
import pb.*;

public class Metricsolver2dDAO  implements CommonDAOService<Metricsolver2dDTO>
{
	
	Logger logger = Logger.getLogger(getClass());
	
	private static String addQuery = "";
	private static String updateQuery = "";
	private String[] columnNames = null;
	public static final int DIM = Metricsolver2dServlet.DIM;

	private Metricsolver2dDAO()
	{
		columnNames = new String[] 
		{
			"ID",
			"gtt",
			"gxx",
			"gtt_num",
			"gxx_num",
			"dt_gtt",
			"dx_gtt",
			"dt_gxx",
			"dx_gxx",
			"dt_gtt_num",
			"dx_gtt_num",
			"dt_gxx_num",
			"dx_gxx_num",
			"x",
			"t",
			"diff_metrics",
			"inv_metrics",
			"gammas",
			"riemanns",
			"s_diff_metric",
			"s_gamma",
			"s_inv_metric",
			
			"pretty_gamma",
			"diff_gamma",
			"diff_gamma_num",
			
			"non_zero_riemann_count",
			
			"search_column",
			"isDeleted",
			"lastModificationTime"
		};
		updateQuery = getUpdateQuery(columnNames);
        addQuery = getAddQuery(columnNames);
		
		searchMap.put("gtt"," and (gtt like ?)");
		searchMap.put("gxx"," and (gxx like ?)");
		searchMap.put("dt_gtt"," and (dt_gtt like ?)");
		searchMap.put("dx_gtt"," and (dx_gtt like ?)");
		searchMap.put("dt_gxx"," and (dt_gxx like ?)");
		searchMap.put("dx_gxx"," and (dx_gxx like ?)");
		searchMap.put("diff_metrics"," and (diff_metrics like ?)");
		searchMap.put("inv_metrics"," and (inv_metrics like ?)");
		searchMap.put("gammas"," and (gammas like ?)");
		searchMap.put("riemanns"," and (riemanns like ?)");
		searchMap.put("s_diff_metric"," and (s_diff_metric like ?)");
		searchMap.put("s_gamma"," and (s_gamma like ?)");
		searchMap.put("s_inv_metric"," and (s_inv_metric like ?)");
		searchMap.put("AnyField"," and (search_column like ?)");
	}

	private final Map<String,String> searchMap = new HashMap<>();

    private static class LazyLoader
    {
    	static final Metricsolver2dDAO INSTANCE = new Metricsolver2dDAO();
	}

	public static Metricsolver2dDAO getInstance()
	{
    	return LazyLoader.INSTANCE;
	}
	
	public void setSearchColumn(Metricsolver2dDTO metricsolver2dDTO)
	{
		metricsolver2dDTO.searchColumn = "";
		metricsolver2dDTO.searchColumn += metricsolver2dDTO.gtt + " ";
		metricsolver2dDTO.searchColumn += metricsolver2dDTO.gxx + " ";
		metricsolver2dDTO.searchColumn += metricsolver2dDTO.dtGtt + " ";
		metricsolver2dDTO.searchColumn += metricsolver2dDTO.dxGtt + " ";
		metricsolver2dDTO.searchColumn += metricsolver2dDTO.dtGxx + " ";
		metricsolver2dDTO.searchColumn += metricsolver2dDTO.dxGxx + " ";
		metricsolver2dDTO.searchColumn += metricsolver2dDTO.diffMetrics + " ";
		metricsolver2dDTO.searchColumn += metricsolver2dDTO.invMetrics + " ";
		metricsolver2dDTO.searchColumn += metricsolver2dDTO.gammas + " ";
		metricsolver2dDTO.searchColumn += metricsolver2dDTO.riemanns + " ";
		metricsolver2dDTO.searchColumn += metricsolver2dDTO.sDiffMetric + " ";
		metricsolver2dDTO.searchColumn += metricsolver2dDTO.sGamma + " ";
		metricsolver2dDTO.searchColumn += metricsolver2dDTO.sInvMetric + " ";
	}
	
	@Override
	public void set(PreparedStatement ps, Metricsolver2dDTO metricsolver2dDTO, boolean isInsert) throws SQLException
	{
		int index = 0;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(metricsolver2dDTO);
		if(isInsert)
		{
			ps.setObject(++index,metricsolver2dDTO.iD);
		}
		ps.setObject(++index,metricsolver2dDTO.gtt);
		ps.setObject(++index,metricsolver2dDTO.gxx);
		ps.setObject(++index,metricsolver2dDTO.gttNum);
		ps.setObject(++index,metricsolver2dDTO.gxxNum);
		ps.setObject(++index,metricsolver2dDTO.dtGtt);
		ps.setObject(++index,metricsolver2dDTO.dxGtt);
		ps.setObject(++index,metricsolver2dDTO.dtGxx);
		ps.setObject(++index,metricsolver2dDTO.dxGxx);
		ps.setObject(++index,metricsolver2dDTO.dtGttNum);
		ps.setObject(++index,metricsolver2dDTO.dxGttNum);
		ps.setObject(++index,metricsolver2dDTO.dtGxxNum);
		ps.setObject(++index,metricsolver2dDTO.dxGxxNum);
		ps.setObject(++index,metricsolver2dDTO.x);
		ps.setObject(++index,metricsolver2dDTO.t);
		ps.setObject(++index,metricsolver2dDTO.diffMetrics);
		ps.setObject(++index,metricsolver2dDTO.invMetrics);
		ps.setObject(++index,metricsolver2dDTO.gammas);
		ps.setObject(++index,metricsolver2dDTO.riemanns);
		ps.setObject(++index,metricsolver2dDTO.sDiffMetric);
		ps.setObject(++index,metricsolver2dDTO.sGamma);
		ps.setObject(++index,metricsolver2dDTO.sInvMetric);
		
		ps.setObject(++index,metricsolver2dDTO.prettyGamma);
		ps.setObject(++index,metricsolver2dDTO.diffGamma);
		ps.setObject(++index,metricsolver2dDTO.diffGammaNum);
		
		ps.setObject(++index,metricsolver2dDTO.nonZeroRiemannCount);
		
		ps.setObject(++index,metricsolver2dDTO.searchColumn);
		if(isInsert)
		{
			ps.setObject(++index,metricsolver2dDTO.isDeleted);
		}
		ps.setObject(++index, lastModificationTime);
		if(!isInsert)
		{
			ps.setObject(++index,metricsolver2dDTO.iD);
		}
	}
	
	public String getCute(String str)
	{
		return(addSup(remove1(str)));
	}
	
	public String getInv(String str)
	{
		if(str.equals(""))
		{
			return str;
		}
		else if(str.startsWith("1/"))
		{
			return str.substring(2, str.length() - 1);
		}
		else
		{
			return ("1/" + str);
		}
	}
	
	public String remove1(String str)
	{
		if(str.equals("1"))
		{
			return "";
		}
		if(str.equals("-1"))
		{
			return "-";
		}
		return str;
	}
	
	public String addSup(String str)
	{
		int index = str.indexOf('^');
		if(index >= 0)
		{
			String toReturn =  str.substring(0, index) + "<sup>" + str.charAt(index + 1) + "</sup>";
			if(str.length() > index + 2)
			{
				toReturn += str.substring(index + 2, str.length() - 1);
			}
			return toReturn;
		}
		else
		{
			return str;
		}
	
	}
	public String getEquation(Metricsolver2dDTO metricsolver2dDTO)
	{
		String str = addSup(remove1(metricsolver2dDTO.gtt)) + "dt<sup>2</sup> + " + addSup(remove1(metricsolver2dDTO.gxx)) + "dx<sup>2</sup>";
		return str;
	}
	
	public String getTX(int i)
	{
		return i == 0? "t":"x";
	}
	
	public String getRiemann(String riemann)
	{
		if(riemann == null)
		{
			return "";
		}
		String[] sRiemann = riemann.split(", ");
		if(sRiemann == null || sRiemann.length < DIM * DIM * DIM * DIM)
		{
			return "";
		}
		String table = "<table class=\"table table-bordered table-striped\">";
		for(int i = 0; i < DIM; i ++)
		{
			for(int j = 0; j < DIM; j ++)
			{
				table += "<tr>";
				for(int k = 0; k < DIM; k ++)
				{
					for(int l = 0; l < DIM; l ++)
					{
						table += "<td>R<sup>" + getTX(l) + "</sup>";
						table += "<sub>" + getTX(i) + "</sub>";
						table += "<sub>" + getTX(j) + "</sub>";
						table += "<sub>" + getTX(k) + "</sub>";
						table += "</td>";
						
						table += "<td>";
						table += sRiemann[i * DIM * DIM * DIM + j *  DIM * DIM + k * DIM + l];
						table += "</td>";
					}
				}
				table += "</tr>";
			}
		}
		table += "</table>";
		return table;
	}
	
	public String getGamma(String data)
	{
		if(data == null)
		{
			return "";
		}
		String []sData = data.split(", ");
		if(sData == null || sData.length < DIM * DIM * DIM)
		{
			return "";
		}
		String table = "<table class=\"table table-bordered table-striped\">";
		int count = 0;
		for(int i = 0; i < DIM; i ++)
		{
			table += "<tr><td><b>e<sub>" + (i == 0? "t":"x") + "</sub></b></td>";
			for(int j = 0; j < DIM * DIM; j ++)
			{
				table += "<td>𝛤<i>";
				if(count % 2 == 0)
				{
					table += "<sup>t</sup>";
				}
				else
				{
					table += "<sup>x</sup>";
				}
				table += "<sub>";
				if(count <= 3)
				{
					table += "t";
				}
				else
				{
					table += "x";
				}
				if(count % 4 < 2)
				{
					table += "t";
				}
				else
				{
					table += "x";
				}
				table += "</sub>";
				table += "</i></td>";
				table += "<td>" 
						+ sData[i * DIM * DIM + j] 
						+ "</td>";
				count ++;
			}
			table += "</tr>";
		}
		table += "</table>";
		return table;
	}
	
	public String getGammaEditable(String sGamma, String sDiffGamma, String diffGammaNum)
	{
		if(sGamma == null)
		{
			return "";
		}
		String []sGammaArray = sGamma.split(", ");
		if(sGammaArray == null || sGammaArray.length < DIM * DIM * DIM)
		{
			return "";
		}
		String []sDiffGammaArray = null;
		if(sDiffGamma != null && !sDiffGamma.equals(""))
		{
			sDiffGammaArray = sDiffGamma.split(", ");
		}
		String []diffGammaNumArray = null;
		if(diffGammaNum != null && !diffGammaNum.equals(""))
		{
			diffGammaNumArray = diffGammaNum.split(", ");
		}
		
		String table = "<table class=\"table table-bordered table-striped\">";
		int count = 0;
		for(int i = 0; i < DIM; i ++)
		{
			table += "<tr><td><b>e<sub>" + (i == 0? "t":"x") + "</sub></b></td>";
			for(int j = 0; j < DIM * DIM; j ++)
			{
				table += "<td>𝛤<i>";
				if(count % 2 == 0)
				{
					table += "<sup>t</sup>";
				}
				else
				{
					table += "<sup>x</sup>";
				}
				table += "<sub>";
				if(count <= 3)
				{
					table += "t";
				}
				else
				{
					table += "x";
				}
				if(count % 4 < 2)
				{
					table += "t";
				}
				else
				{
					table += "x";
				}
				table += "</sub>";
				table += "</i></td>";
				table += "<td>"; 
				if(sGammaArray[i * DIM * DIM + j].equals("0"))
				{
					table += "0"; 
					table += "<input type = 'hidden' value = '0' name = 'prettyGamma' />"; 
					table += "<input type = 'hidden' value = '0' name = 'diffGamma' />"; 
					table += "<input type = 'hidden' value = '0' name = 'diffGamma' />";
					table += "<input type = 'hidden' value = '0' name = 'diffNumGamma' />"; 
					table += "<input type = 'hidden' value = '0' name = 'diffNumGamma' />"; 
				}
				else
				{
					String val = sGammaArray[i * DIM * DIM + j].replaceAll("<sup>", "^").replaceAll("</sup>", "");
					table += "<table>";
					table += "<tr><td colspan = '4'>";
					table += "<input type = 'text' class = 'form-control'  name = 'prettyGamma' value = '" + val + "' />"; 
					table += "</td></tr>";
					
					
					table += "<tr><td>";
					table += "∂<sub>t</sub>"; 
					table += "</td>";
					table += "<td>";
					if(sDiffGammaArray == null)
					{
						val = "0";
					}
					else
					{
						val = sDiffGammaArray[DIM * (i * DIM * DIM + j)].replaceAll("<sup>", "^").replaceAll("</sup>", "");
					}
					table += "<input type = 'text' class = 'form-control' style = 'width:100px'  name = 'diffGamma' value = '" + val + "' />"; 
					table += "</td>";
					
					
					table += "<td>";
					table += "∂<sub>x</sub>";
					table += "</td>";
					table += "<td>";
					if(sDiffGammaArray == null)
					{
						val = "0";
					}
					else
					{
						val = sDiffGammaArray[DIM * (i * DIM * DIM + j) + 1].replaceAll("<sup>", "^").replaceAll("</sup>", "");
					}
					table += "<input type = 'text' class = 'form-control' style = 'width:100px' name = 'diffGamma' value = '" + val + "' />"; 
					table += "</td></tr>";
					
					table += "<tr><td>";
					table += "∂<sub>t</sub> (num)"; 
					table += "</td>";
					table += "<td>";
					if(diffGammaNumArray == null)
					{
						val = "0";
					}
					else
					{
						val = diffGammaNumArray[DIM * (i * DIM * DIM + j)].replaceAll("<sup>", "^").replaceAll("</sup>", "");
					}
					table += "<input type = 'text' class = 'form-control' style = 'width:100px' name = 'diffNumGamma' value = '" + val + "' />"; 
					table += "</td>";
					
					
					table += "<td>";
					table += "∂<sub>x</sub> (num)";
					table += "</td>";
					table += "<td>";
					if(diffGammaNumArray == null)
					{
						val = "0";
					}
					else
					{
						val = diffGammaNumArray[DIM * (i * DIM * DIM + j) + 1].replaceAll("<sup>", "^").replaceAll("</sup>", "");
					}
					table += "<input type = 'text' class = 'form-control' style = 'width:100px' name = 'diffNumGamma' value = '" + val + "' />"; 
					table += "</td></tr>";
					
					
					table += "</table>";
				}
				
				table += "</td>";
				count ++;
			}
			table += "</tr>";
		}
		table += "</table>";
		return table;
	}
	
	
	
	public String getTable2D(String data)
	{
		if(data == null)
		{
			return "";
		}
		String []sData = data.split(", ");
		if(sData == null || sData.length < DIM * DIM)
		{
			return "";
		}
		String table = "<table class=\"table table-bordered table-striped\">";
		table += "<tr><td></td><td><b>t</b></td><td><b>x</b></td></tr>";
		for(int i = 0; i < DIM; i ++)
		{
			table += "<tr><td><b>" + (i == 0? "t":"x") + "</b></td>";
			for(int j = 0; j < DIM; j ++)
			{
				table += "<td>" + sData[i * DIM + j] + "</td>";
			}
			table += "</tr>";
		}
		table += "</table>";
		return table;
	}
	
	@Override
	public Metricsolver2dDTO buildObjectFromResultSet(ResultSet rs)
	{
		try
		{
			Metricsolver2dDTO metricsolver2dDTO = new Metricsolver2dDTO();
			int i = 0;
			metricsolver2dDTO.iD = rs.getLong(columnNames[i++]);
			metricsolver2dDTO.gtt = rs.getString(columnNames[i++]);
			metricsolver2dDTO.gxx = rs.getString(columnNames[i++]);
			metricsolver2dDTO.gttNum = rs.getDouble(columnNames[i++]);
			metricsolver2dDTO.gxxNum = rs.getDouble(columnNames[i++]);
			metricsolver2dDTO.dtGtt = rs.getString(columnNames[i++]);
			metricsolver2dDTO.dxGtt = rs.getString(columnNames[i++]);
			metricsolver2dDTO.dtGxx = rs.getString(columnNames[i++]);
			metricsolver2dDTO.dxGxx = rs.getString(columnNames[i++]);
			metricsolver2dDTO.dtGttNum = rs.getDouble(columnNames[i++]);
			metricsolver2dDTO.dxGttNum = rs.getDouble(columnNames[i++]);
			metricsolver2dDTO.dtGxxNum = rs.getDouble(columnNames[i++]);
			metricsolver2dDTO.dxGxxNum = rs.getDouble(columnNames[i++]);
			metricsolver2dDTO.x = rs.getDouble(columnNames[i++]);
			metricsolver2dDTO.t = rs.getDouble(columnNames[i++]);
			metricsolver2dDTO.diffMetrics = rs.getString(columnNames[i++]);
			metricsolver2dDTO.invMetrics = rs.getString(columnNames[i++]);
			metricsolver2dDTO.gammas = rs.getString(columnNames[i++]);
			metricsolver2dDTO.riemanns = rs.getString(columnNames[i++]);
			metricsolver2dDTO.sDiffMetric = rs.getString(columnNames[i++]);
			metricsolver2dDTO.sGamma = rs.getString(columnNames[i++]);
			metricsolver2dDTO.sInvMetric = rs.getString(columnNames[i++]);
			
			metricsolver2dDTO.prettyGamma = rs.getString(columnNames[i++]);
			metricsolver2dDTO.diffGamma = rs.getString(columnNames[i++]);
			metricsolver2dDTO.diffGammaNum = rs.getString(columnNames[i++]);
			
			metricsolver2dDTO.nonZeroRiemannCount = rs.getInt(columnNames[i++]);
			
			metricsolver2dDTO.searchColumn = rs.getString(columnNames[i++]);
			metricsolver2dDTO.isDeleted = rs.getInt(columnNames[i++]);
			metricsolver2dDTO.lastModificationTime = rs.getLong(columnNames[i++]);
			return metricsolver2dDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
		
	public Metricsolver2dDTO getDTOByID (long id)
	{
		return getDTOFromID(id);
	}

	@Override
	public String getTableName() {
		return "metricsolver2d";
	}

	@Override
	public Map<String, String> getSearchMap() {
		return searchMap;
	}

	public long add(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Metricsolver2dDTO) commonDTO,addQuery,true);
	}

	public long update(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Metricsolver2dDTO) commonDTO,updateQuery,false);
	}
	
	@Override
	public String getLastModifierUser(){
		return "";
    }
				
}
	