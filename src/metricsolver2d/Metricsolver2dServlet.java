package metricsolver2d;


import java.io.IOException;
import java.text.SimpleDateFormat;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import org.apache.log4j.Logger;


import permission.MenuConstants;

import user.UserDTO;
import util.*;
import javax.servlet.http.*;

import common.ApiResponse;
import common.BaseServlet;

import com.google.gson.Gson;

import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;



/**
 * Servlet implementation class Metricsolver2dServlet
 */
@WebServlet("/Metricsolver2dServlet")
@MultipartConfig
public class Metricsolver2dServlet extends BaseServlet 
{
	private static final long serialVersionUID = 1L;
	public static final int DIM = 2;
	public static final double THRESHOLD = 0.0000001;
    public static Logger logger = Logger.getLogger(Metricsolver2dServlet.class);

    @Override
    public String getTableName() {
        return Metricsolver2dDAO.getInstance().getTableName();
    }

    @Override
    public String getServletName() {
        return "Metricsolver2dServlet";
    }

    @Override
    public Metricsolver2dDAO getCommonDAOService() {
        return Metricsolver2dDAO.getInstance();
    }
    
    @Override
    public int[] getAddPageMenuConstants() {
        return new int[] {MenuConstants.METRICSOLVER2D_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[] {MenuConstants.METRICSOLVER2D_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[] {MenuConstants.METRICSOLVER2D_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Metricsolver2dServlet.class;
    }
    private final Gson gson = new Gson();
 	
    public String commaSeparate(String param, HttpServletRequest request)
    {
    	String str = "";
    	String [] array = request.getParameterValues(param);
    	if(array != null)
    	{
    		for(String ar: array)
			{
				str += Metricsolver2dDAO.getInstance().addSup(ar) + ", ";
			}
    	}
    	return str;
    }
    public double getRiemann(int sau, int mu, int nu, int rho, double [][][] gammaNum, double [][][][] diffGammaNum)
    {
    	double riemann = diffGammaNum[nu][sau][rho][mu] - diffGammaNum[mu][sau][rho][nu];
    	for(int lambda = 0; lambda < DIM; lambda ++)
    	{
    		riemann += gammaNum[mu][lambda][rho] * gammaNum[nu][sau][lambda];
    	}
    	for(int lambda = 0; lambda < DIM; lambda ++)
    	{
    		riemann -= gammaNum[nu][lambda][rho] * gammaNum[mu][sau][lambda];
    	}
    	if(Math.abs(riemann) <= THRESHOLD)
    	{
    		riemann = 0;
    	}
    	return riemann;
    			
    }
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        CommonLoginData commonLoginData = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get();
        init(request);
        String actionType = request.getParameter("actionType");
		if ("prettyfyGamma".equals(actionType)) {
			try {
				long id = Long.parseLong(request.getParameter("iD"));
				Metricsolver2dDTO metricsolver2dDTO = Metricsolver2dDAO.getInstance().getDTOByID(id);
				metricsolver2dDTO.prettyGamma = commaSeparate("prettyGamma", request);
				metricsolver2dDTO.diffGamma = commaSeparate("diffGamma", request);
				metricsolver2dDTO.diffGammaNum = commaSeparate("diffNumGamma", request);
				
				//String [][][] prettyGammas = new String [DIM][DIM][DIM];
				//String [][][] diffGamma = new String [DIM][DIM][DIM][DIM};
				double [][][] numGammas = new double
						[DIM][DIM][DIM];
				double [][][][] diffGammaNums = new double
						[DIM][DIM][DIM][DIM];
				double [][][][] riemann = new double
						[DIM][DIM][DIM][DIM];
				
				String [] sNumGammas = metricsolver2dDTO.gammas.split(", ");
				for(int i = 0; i < DIM; i ++)
				{
					for(int j = 0; j < DIM; j ++)
					{
						for(int k = 0; k < DIM; k ++)
						{
							numGammas[i][j][k] = Double.parseDouble(sNumGammas[i * DIM * DIM  + j *  DIM + k ]);
							
						}
					}
				}
				
				String [] sDiffGammaNums = request.getParameterValues("diffNumGamma");
				for(int i = 0; i < DIM; i ++)
				{
					for(int j = 0; j < DIM; j ++)
					{
						for(int k = 0; k < DIM; k ++)
						{
							for(int l = 0; l < DIM; l ++)
							{
								diffGammaNums[i][j][k][l] = Double.parseDouble(sDiffGammaNums[i * DIM * DIM * DIM + j *  DIM * DIM + k * DIM + l]);
							}
						}
					}
				}
				
				metricsolver2dDTO.riemanns = "";
				metricsolver2dDTO.nonZeroRiemannCount = 0;
				for(int i = 0; i < DIM; i ++)
				{
					for(int j = 0; j < DIM; j ++)
					{
						for(int k = 0; k < DIM; k ++)
						{
							for(int l = 0; l < DIM; l ++)
							{
								riemann[i][j][k][l] = getRiemann(i, j, k, l, numGammas, diffGammaNums);
								metricsolver2dDTO.riemanns += riemann[i][j][k][l] + ", ";
								if(Math.abs(riemann[i][j][k][l]) > THRESHOLD)
								{
									metricsolver2dDTO.nonZeroRiemannCount ++;
								}
							}
						}
					}
				}
				
				
				Metricsolver2dDAO.getInstance().update(metricsolver2dDTO);
				request.getRequestDispatcher(commonPartOfDispatchURL() + "View.jsp?ID=" + id).forward(request,response);
			} catch (Exception ex) {
				logger.error(ex);
				ApiResponse.sendErrorResponse(response, ex.getMessage());
			}
			return;
		}
		
		else
		{
			super.doPost(request,response);
		}
    }

	@Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception 
	{
		
		request.setAttribute("failureMessage", "");
		System.out.println("%%%% addMetricsolver2d");
		String language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
		Metricsolver2dDTO metricsolver2dDTO;
		SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
					
		if(addFlag)
		{
			metricsolver2dDTO = new Metricsolver2dDTO();
		}
		else
		{
			metricsolver2dDTO = Metricsolver2dDAO.getInstance().getDTOFromID(Long.parseLong(request.getParameter("iD")));
		}
		
		String Value = "";

		Value = request.getParameter("gtt");

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("gtt = " + Value);
		if(Value != null)
		{
			metricsolver2dDTO.gtt = (Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}

		Value = request.getParameter("gxx");

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("gxx = " + Value);
		if(Value != null)
		{
			metricsolver2dDTO.gxx = (Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}

		Value = request.getParameter("gttNum");

		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("gttNum = " + Value);
		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			metricsolver2dDTO.gttNum = Double.parseDouble(Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}

		Value = request.getParameter("gxxNum");

		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("gxxNum = " + Value);
		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			metricsolver2dDTO.gxxNum = Double.parseDouble(Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}

		Value = request.getParameter("dtGtt");

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("dtGtt = " + Value);
		if(Value != null)
		{
			metricsolver2dDTO.dtGtt = (Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}

		Value = request.getParameter("dxGtt");

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("dxGtt = " + Value);
		if(Value != null)
		{
			metricsolver2dDTO.dxGtt = (Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}

		Value = request.getParameter("dtGxx");

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("dtGxx = " + Value);
		if(Value != null)
		{
			metricsolver2dDTO.dtGxx = (Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}

		Value = request.getParameter("dxGxx");

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("dxGxx = " + Value);
		if(Value != null)
		{
			metricsolver2dDTO.dxGxx = (Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}

		Value = request.getParameter("dtGttNum");

		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("dtGttNum = " + Value);
		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			metricsolver2dDTO.dtGttNum = Double.parseDouble(Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}

		Value = request.getParameter("dxGttNum");

		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("dxGttNum = " + Value);
		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			metricsolver2dDTO.dxGttNum = Double.parseDouble(Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}

		Value = request.getParameter("dtGxxNum");

		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("dtGxxNum = " + Value);
		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			metricsolver2dDTO.dtGxxNum = Double.parseDouble(Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}

		Value = request.getParameter("dxGxxNum");

		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("dxGxxNum = " + Value);
		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			metricsolver2dDTO.dxGxxNum = Double.parseDouble(Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}

		Value = request.getParameter("x");

		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("x = " + Value);
		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			metricsolver2dDTO.x = Double.parseDouble(Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}

		Value = request.getParameter("t");

		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("t = " + Value);
		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			metricsolver2dDTO.t = Double.parseDouble(Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}
		
		processMetric(metricsolver2dDTO);

		
		System.out.println("Done adding  addMetricsolver2d dto = " + metricsolver2dDTO);

		if(addFlag == true)
		{
			Metricsolver2dDAO.getInstance().add(metricsolver2dDTO);
		}
		else
		{				
			Metricsolver2dDAO.getInstance().update(metricsolver2dDTO);										
		}
		
		

		return metricsolver2dDTO;

	}
	
	public double getGamma(int c, int b, int d, double[][] metrics, double[][] invMetrics, double[][][] diffMetrics)
	{
		int a = d;
		double gamma_cbd = 0.5 * invMetrics[d][a] * (diffMetrics[a][b][c] + diffMetrics[c][a][b] - diffMetrics[b][c][a]);
		return gamma_cbd;
	}
	
	public String getGammaStr(int c, int b, int d, String[][] sMetrics, String[][] sInvMetrics, String[][][] sDiffMetrics, double numericGamma)
	{
		if(Math.abs(numericGamma) <= THRESHOLD)
		{
			return "0";
		}
		int a = d;
		String internal = "";
		if(!sDiffMetrics[a][b][c].equals("0"))
		{
			internal += "(" + sDiffMetrics[a][b][c] + ")";
		}
		if(!sDiffMetrics[c][a][b].equals("0"))
		{
			if(!internal.equalsIgnoreCase(""))
			{
				internal += " + ";
			}
			internal += "(" + sDiffMetrics[c][a][b] + ")";
		}
		if(!sDiffMetrics[b][c][a].equals("0"))
		{
			
			internal += " - ";
			
			internal += "(" + sDiffMetrics[b][c][a] + ")";
		}
		String sGamma = "1/2" ;
		if(!sInvMetrics[d][a].equals(""))
		{
			sGamma += " × (" + sInvMetrics[d][a] + ")";
		}
		
		if(!internal.equals(""))
		{
			sGamma += " × (" + internal + ")";
		}
		return sGamma;
	}

	private void processMetric(Metricsolver2dDTO metricsolver2dDTO) {
		Metricsolver2dDAO metricsolver2dDAO = Metricsolver2dDAO.getInstance();
		double[][] metrics = {{metricsolver2dDTO.gttNum, 0},{0, metricsolver2dDTO.gxxNum}};
		double[][] invMetrics = {{1/metricsolver2dDTO.gttNum, 0},{0, 1/metricsolver2dDTO.gxxNum}};
		double[][][] diffMetrics = {
										{
											{metricsolver2dDTO.dtGttNum, metricsolver2dDTO.dxGttNum},
											{0, 0}
										},
										{
											{0, 0},
											{metricsolver2dDTO.dtGxxNum, metricsolver2dDTO.dxGxxNum},
											
										}
									};
		
		String[][] sMetrics = {{metricsolver2dDAO.getCute(metricsolver2dDTO.gtt), "0"},{"0", metricsolver2dDAO.getCute(metricsolver2dDTO.gxx)}};
		String[][] sInvMetrics = {{metricsolver2dDAO.getInv(sMetrics[0][0]), "0"},{"0", metricsolver2dDAO.getInv(sMetrics[1][1])}};
		String[][][] sDiffMetrics = {
				{
					{metricsolver2dDAO.getCute(metricsolver2dDTO.dtGtt), metricsolver2dDAO.getCute(metricsolver2dDTO.dxGtt)},
					{"0", "0"}
				},
				{
					{"0", "0"},
					{metricsolver2dDAO.getCute(metricsolver2dDTO.dtGxx), metricsolver2dDAO.getCute(metricsolver2dDTO.dxGxx)},
					
				}
			};
		double[][][] gamma = new double[DIM][DIM][DIM];
		String [][][] sGamma = new String[DIM][DIM][DIM];
		metricsolver2dDTO.gammas =  "";
		metricsolver2dDTO.sGamma = "";
		
		for(int i = 0; i < DIM; i ++)
		{
			for(int j = 0; j < DIM; j ++)
			{
				for(int k = 0; k < DIM; k ++)
				{
					gamma[i][j][k] = getGamma(i, j, k, metrics, invMetrics, diffMetrics);
					if(gamma[i][j][k] <= 0 && gamma[i][j][k]> -THRESHOLD)
					{
						gamma[i][j][k] = 0;
					}
					metricsolver2dDTO.gammas += String. format("%.2f", gamma[i][j][k])  + ", ";
					
					sGamma[i][j][k] = getGammaStr(i, j, k, sMetrics, sInvMetrics, sDiffMetrics, gamma[i][j][k]);
					metricsolver2dDTO.sGamma += sGamma[i][j][k] + ", ";
				}
				
			}
		}
		
		metricsolver2dDTO.prettyGamma = metricsolver2dDTO.sGamma;
		
	}
}

