package metricsolver2d;
import java.util.*; 
import util.*; 


public class Metricsolver2dDTO extends CommonDTO
{

    public String gtt = "";
    public String gxx = "";
	public double gttNum = -1;
	public double gxxNum = 1;
    public String dtGtt = "0";
    public String dxGtt = "0";
    public String dtGxx = "0";
    public String dxGxx = "0";
	public double dtGttNum = 0;
	public double dxGttNum = 0;
	public double dtGxxNum = 0;
	public double dxGxxNum = 0;
	public double x = 5;
	public double t = 7;
    public String diffMetrics = "";
    public String invMetrics = "";
    public String gammas = "";
    public String riemanns = "";
    public String sDiffMetric = "";
    public String sGamma = "";
    public String sInvMetric = "";
    
    public String prettyGamma = "";
    public String diffGamma = "";
    public String diffGammaNum = "";
    public int nonZeroRiemannCount = -1;
	
	
    @Override
	public String toString() {
            return "$Metricsolver2dDTO[" +
            " iD = " + iD +
            " gtt = " + gtt +
            " gxx = " + gxx +
            " gttNum = " + gttNum +
            " gxxNum = " + gxxNum +
            " dtGtt = " + dtGtt +
            " dxGtt = " + dxGtt +
            " dtGxx = " + dtGxx +
            " dxGxx = " + dxGxx +
            " dtGttNum = " + dtGttNum +
            " dxGttNum = " + dxGttNum +
            " dtGxxNum = " + dtGxxNum +
            " dxGxxNum = " + dxGxxNum +
            " x = " + x +
            " t = " + t +
            " diffMetrics = " + diffMetrics +
            " invMetrics = " + invMetrics +
            " gammas = " + gammas +
            " riemanns = " + riemanns +
            " sDiffMetric = " + sDiffMetric +
            " sGamma = " + sGamma +
            " sInvMetric = " + sInvMetric +
            " searchColumn = " + searchColumn +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}