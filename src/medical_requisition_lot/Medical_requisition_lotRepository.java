package medical_requisition_lot;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class Medical_requisition_lotRepository implements Repository {
	Medical_requisition_lotDAO medical_requisition_lotDAO = null;
	
	public void setDAO(Medical_requisition_lotDAO medical_requisition_lotDAO)
	{
		this.medical_requisition_lotDAO = medical_requisition_lotDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Medical_requisition_lotRepository.class);
	Map<Long, Medical_requisition_lotDTO>mapOfMedical_requisition_lotDTOToiD;
	Map<Long, Set<Medical_requisition_lotDTO> >mapOfMedical_requisition_lotDTOTorequisitionToOrganogramId;
	Map<String, Set<Medical_requisition_lotDTO> >mapOfMedical_requisition_lotDTOTosubject;
	Map<String, Set<Medical_requisition_lotDTO> >mapOfMedical_requisition_lotDTOTodescription;
	Map<Long, Set<Medical_requisition_lotDTO> >mapOfMedical_requisition_lotDTOToinsertedByUserId;
	Map<Long, Set<Medical_requisition_lotDTO> >mapOfMedical_requisition_lotDTOToinsertedByOrganogramId;
	Map<Integer, Set<Medical_requisition_lotDTO> >mapOfMedical_requisition_lotDTOTojobCat;
	Map<Long, Set<Medical_requisition_lotDTO> >mapOfMedical_requisition_lotDTOToinsertionDate;
	Map<String, Set<Medical_requisition_lotDTO> >mapOfMedical_requisition_lotDTOTosearchColumn;
	Map<Long, Set<Medical_requisition_lotDTO> >mapOfMedical_requisition_lotDTOTolastModificationTime;


	static Medical_requisition_lotRepository instance = null;  
	private Medical_requisition_lotRepository(){
		mapOfMedical_requisition_lotDTOToiD = new ConcurrentHashMap<>();
		mapOfMedical_requisition_lotDTOTorequisitionToOrganogramId = new ConcurrentHashMap<>();
		mapOfMedical_requisition_lotDTOTosubject = new ConcurrentHashMap<>();
		mapOfMedical_requisition_lotDTOTodescription = new ConcurrentHashMap<>();
		mapOfMedical_requisition_lotDTOToinsertedByUserId = new ConcurrentHashMap<>();
		mapOfMedical_requisition_lotDTOToinsertedByOrganogramId = new ConcurrentHashMap<>();
		mapOfMedical_requisition_lotDTOTojobCat = new ConcurrentHashMap<>();
		mapOfMedical_requisition_lotDTOToinsertionDate = new ConcurrentHashMap<>();
		mapOfMedical_requisition_lotDTOTosearchColumn = new ConcurrentHashMap<>();
		mapOfMedical_requisition_lotDTOTolastModificationTime = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Medical_requisition_lotRepository getInstance(){
		if (instance == null){
			instance = new Medical_requisition_lotRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(medical_requisition_lotDAO == null)
		{
			return;
		}
		try {
			List<Medical_requisition_lotDTO> medical_requisition_lotDTOs = medical_requisition_lotDAO.getAllMedical_requisition_lot(reloadAll);
			for(Medical_requisition_lotDTO medical_requisition_lotDTO : medical_requisition_lotDTOs) {
				Medical_requisition_lotDTO oldMedical_requisition_lotDTO = getMedical_requisition_lotDTOByID(medical_requisition_lotDTO.iD);
				if( oldMedical_requisition_lotDTO != null ) {
					mapOfMedical_requisition_lotDTOToiD.remove(oldMedical_requisition_lotDTO.iD);
				
					if(mapOfMedical_requisition_lotDTOTorequisitionToOrganogramId.containsKey(oldMedical_requisition_lotDTO.requisitionToOrganogramId)) {
						mapOfMedical_requisition_lotDTOTorequisitionToOrganogramId.get(oldMedical_requisition_lotDTO.requisitionToOrganogramId).remove(oldMedical_requisition_lotDTO);
					}
					if(mapOfMedical_requisition_lotDTOTorequisitionToOrganogramId.get(oldMedical_requisition_lotDTO.requisitionToOrganogramId).isEmpty()) {
						mapOfMedical_requisition_lotDTOTorequisitionToOrganogramId.remove(oldMedical_requisition_lotDTO.requisitionToOrganogramId);
					}
					
					if(mapOfMedical_requisition_lotDTOTosubject.containsKey(oldMedical_requisition_lotDTO.subject)) {
						mapOfMedical_requisition_lotDTOTosubject.get(oldMedical_requisition_lotDTO.subject).remove(oldMedical_requisition_lotDTO);
					}
					if(mapOfMedical_requisition_lotDTOTosubject.get(oldMedical_requisition_lotDTO.subject).isEmpty()) {
						mapOfMedical_requisition_lotDTOTosubject.remove(oldMedical_requisition_lotDTO.subject);
					}
					
					if(mapOfMedical_requisition_lotDTOTodescription.containsKey(oldMedical_requisition_lotDTO.description)) {
						mapOfMedical_requisition_lotDTOTodescription.get(oldMedical_requisition_lotDTO.description).remove(oldMedical_requisition_lotDTO);
					}
					if(mapOfMedical_requisition_lotDTOTodescription.get(oldMedical_requisition_lotDTO.description).isEmpty()) {
						mapOfMedical_requisition_lotDTOTodescription.remove(oldMedical_requisition_lotDTO.description);
					}
					
					if(mapOfMedical_requisition_lotDTOToinsertedByUserId.containsKey(oldMedical_requisition_lotDTO.insertedByUserId)) {
						mapOfMedical_requisition_lotDTOToinsertedByUserId.get(oldMedical_requisition_lotDTO.insertedByUserId).remove(oldMedical_requisition_lotDTO);
					}
					if(mapOfMedical_requisition_lotDTOToinsertedByUserId.get(oldMedical_requisition_lotDTO.insertedByUserId).isEmpty()) {
						mapOfMedical_requisition_lotDTOToinsertedByUserId.remove(oldMedical_requisition_lotDTO.insertedByUserId);
					}
					
					if(mapOfMedical_requisition_lotDTOToinsertedByOrganogramId.containsKey(oldMedical_requisition_lotDTO.insertedByOrganogramId)) {
						mapOfMedical_requisition_lotDTOToinsertedByOrganogramId.get(oldMedical_requisition_lotDTO.insertedByOrganogramId).remove(oldMedical_requisition_lotDTO);
					}
					if(mapOfMedical_requisition_lotDTOToinsertedByOrganogramId.get(oldMedical_requisition_lotDTO.insertedByOrganogramId).isEmpty()) {
						mapOfMedical_requisition_lotDTOToinsertedByOrganogramId.remove(oldMedical_requisition_lotDTO.insertedByOrganogramId);
					}
					
					if(mapOfMedical_requisition_lotDTOTojobCat.containsKey(oldMedical_requisition_lotDTO.jobCat)) {
						mapOfMedical_requisition_lotDTOTojobCat.get(oldMedical_requisition_lotDTO.jobCat).remove(oldMedical_requisition_lotDTO);
					}
					if(mapOfMedical_requisition_lotDTOTojobCat.get(oldMedical_requisition_lotDTO.jobCat).isEmpty()) {
						mapOfMedical_requisition_lotDTOTojobCat.remove(oldMedical_requisition_lotDTO.jobCat);
					}
					
					if(mapOfMedical_requisition_lotDTOToinsertionDate.containsKey(oldMedical_requisition_lotDTO.insertionDate)) {
						mapOfMedical_requisition_lotDTOToinsertionDate.get(oldMedical_requisition_lotDTO.insertionDate).remove(oldMedical_requisition_lotDTO);
					}
					if(mapOfMedical_requisition_lotDTOToinsertionDate.get(oldMedical_requisition_lotDTO.insertionDate).isEmpty()) {
						mapOfMedical_requisition_lotDTOToinsertionDate.remove(oldMedical_requisition_lotDTO.insertionDate);
					}
					
					if(mapOfMedical_requisition_lotDTOTosearchColumn.containsKey(oldMedical_requisition_lotDTO.searchColumn)) {
						mapOfMedical_requisition_lotDTOTosearchColumn.get(oldMedical_requisition_lotDTO.searchColumn).remove(oldMedical_requisition_lotDTO);
					}
					if(mapOfMedical_requisition_lotDTOTosearchColumn.get(oldMedical_requisition_lotDTO.searchColumn).isEmpty()) {
						mapOfMedical_requisition_lotDTOTosearchColumn.remove(oldMedical_requisition_lotDTO.searchColumn);
					}
					
					if(mapOfMedical_requisition_lotDTOTolastModificationTime.containsKey(oldMedical_requisition_lotDTO.lastModificationTime)) {
						mapOfMedical_requisition_lotDTOTolastModificationTime.get(oldMedical_requisition_lotDTO.lastModificationTime).remove(oldMedical_requisition_lotDTO);
					}
					if(mapOfMedical_requisition_lotDTOTolastModificationTime.get(oldMedical_requisition_lotDTO.lastModificationTime).isEmpty()) {
						mapOfMedical_requisition_lotDTOTolastModificationTime.remove(oldMedical_requisition_lotDTO.lastModificationTime);
					}
					
					
				}
				if(medical_requisition_lotDTO.isDeleted == 0) 
				{
					
					mapOfMedical_requisition_lotDTOToiD.put(medical_requisition_lotDTO.iD, medical_requisition_lotDTO);
				
					if( ! mapOfMedical_requisition_lotDTOTorequisitionToOrganogramId.containsKey(medical_requisition_lotDTO.requisitionToOrganogramId)) {
						mapOfMedical_requisition_lotDTOTorequisitionToOrganogramId.put(medical_requisition_lotDTO.requisitionToOrganogramId, new HashSet<>());
					}
					mapOfMedical_requisition_lotDTOTorequisitionToOrganogramId.get(medical_requisition_lotDTO.requisitionToOrganogramId).add(medical_requisition_lotDTO);
					
					if( ! mapOfMedical_requisition_lotDTOTosubject.containsKey(medical_requisition_lotDTO.subject)) {
						mapOfMedical_requisition_lotDTOTosubject.put(medical_requisition_lotDTO.subject, new HashSet<>());
					}
					mapOfMedical_requisition_lotDTOTosubject.get(medical_requisition_lotDTO.subject).add(medical_requisition_lotDTO);
					
					if( ! mapOfMedical_requisition_lotDTOTodescription.containsKey(medical_requisition_lotDTO.description)) {
						mapOfMedical_requisition_lotDTOTodescription.put(medical_requisition_lotDTO.description, new HashSet<>());
					}
					mapOfMedical_requisition_lotDTOTodescription.get(medical_requisition_lotDTO.description).add(medical_requisition_lotDTO);
					
					if( ! mapOfMedical_requisition_lotDTOToinsertedByUserId.containsKey(medical_requisition_lotDTO.insertedByUserId)) {
						mapOfMedical_requisition_lotDTOToinsertedByUserId.put(medical_requisition_lotDTO.insertedByUserId, new HashSet<>());
					}
					mapOfMedical_requisition_lotDTOToinsertedByUserId.get(medical_requisition_lotDTO.insertedByUserId).add(medical_requisition_lotDTO);
					
					if( ! mapOfMedical_requisition_lotDTOToinsertedByOrganogramId.containsKey(medical_requisition_lotDTO.insertedByOrganogramId)) {
						mapOfMedical_requisition_lotDTOToinsertedByOrganogramId.put(medical_requisition_lotDTO.insertedByOrganogramId, new HashSet<>());
					}
					mapOfMedical_requisition_lotDTOToinsertedByOrganogramId.get(medical_requisition_lotDTO.insertedByOrganogramId).add(medical_requisition_lotDTO);
					
					if( ! mapOfMedical_requisition_lotDTOTojobCat.containsKey(medical_requisition_lotDTO.jobCat)) {
						mapOfMedical_requisition_lotDTOTojobCat.put(medical_requisition_lotDTO.jobCat, new HashSet<>());
					}
					mapOfMedical_requisition_lotDTOTojobCat.get(medical_requisition_lotDTO.jobCat).add(medical_requisition_lotDTO);
					
					if( ! mapOfMedical_requisition_lotDTOToinsertionDate.containsKey(medical_requisition_lotDTO.insertionDate)) {
						mapOfMedical_requisition_lotDTOToinsertionDate.put(medical_requisition_lotDTO.insertionDate, new HashSet<>());
					}
					mapOfMedical_requisition_lotDTOToinsertionDate.get(medical_requisition_lotDTO.insertionDate).add(medical_requisition_lotDTO);
					
					if( ! mapOfMedical_requisition_lotDTOTosearchColumn.containsKey(medical_requisition_lotDTO.searchColumn)) {
						mapOfMedical_requisition_lotDTOTosearchColumn.put(medical_requisition_lotDTO.searchColumn, new HashSet<>());
					}
					mapOfMedical_requisition_lotDTOTosearchColumn.get(medical_requisition_lotDTO.searchColumn).add(medical_requisition_lotDTO);
					
					if( ! mapOfMedical_requisition_lotDTOTolastModificationTime.containsKey(medical_requisition_lotDTO.lastModificationTime)) {
						mapOfMedical_requisition_lotDTOTolastModificationTime.put(medical_requisition_lotDTO.lastModificationTime, new HashSet<>());
					}
					mapOfMedical_requisition_lotDTOTolastModificationTime.get(medical_requisition_lotDTO.lastModificationTime).add(medical_requisition_lotDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Medical_requisition_lotDTO> getMedical_requisition_lotList() {
		List <Medical_requisition_lotDTO> medical_requisition_lots = new ArrayList<Medical_requisition_lotDTO>(this.mapOfMedical_requisition_lotDTOToiD.values());
		return medical_requisition_lots;
	}
	
	
	public Medical_requisition_lotDTO getMedical_requisition_lotDTOByID( long ID){
		return mapOfMedical_requisition_lotDTOToiD.get(ID);
	}
	
	
	public List<Medical_requisition_lotDTO> getMedical_requisition_lotDTOByrequisition_to_organogram_id(long requisition_to_organogram_id) {
		return new ArrayList<>( mapOfMedical_requisition_lotDTOTorequisitionToOrganogramId.getOrDefault(requisition_to_organogram_id,new HashSet<>()));
	}
	
	
	public List<Medical_requisition_lotDTO> getMedical_requisition_lotDTOBysubject(String subject) {
		return new ArrayList<>( mapOfMedical_requisition_lotDTOTosubject.getOrDefault(subject,new HashSet<>()));
	}
	
	
	public List<Medical_requisition_lotDTO> getMedical_requisition_lotDTOBydescription(String description) {
		return new ArrayList<>( mapOfMedical_requisition_lotDTOTodescription.getOrDefault(description,new HashSet<>()));
	}
	
	
	public List<Medical_requisition_lotDTO> getMedical_requisition_lotDTOByinserted_by_user_id(long inserted_by_user_id) {
		return new ArrayList<>( mapOfMedical_requisition_lotDTOToinsertedByUserId.getOrDefault(inserted_by_user_id,new HashSet<>()));
	}
	
	
	public List<Medical_requisition_lotDTO> getMedical_requisition_lotDTOByinserted_by_organogram_id(long inserted_by_organogram_id) {
		return new ArrayList<>( mapOfMedical_requisition_lotDTOToinsertedByOrganogramId.getOrDefault(inserted_by_organogram_id,new HashSet<>()));
	}
	
	
	public List<Medical_requisition_lotDTO> getMedical_requisition_lotDTOByjob_cat(int job_cat) {
		return new ArrayList<>( mapOfMedical_requisition_lotDTOTojobCat.getOrDefault(job_cat,new HashSet<>()));
	}
	
	
	public List<Medical_requisition_lotDTO> getMedical_requisition_lotDTOByinsertion_date(long insertion_date) {
		return new ArrayList<>( mapOfMedical_requisition_lotDTOToinsertionDate.getOrDefault(insertion_date,new HashSet<>()));
	}
	
	
	public List<Medical_requisition_lotDTO> getMedical_requisition_lotDTOBysearch_column(String search_column) {
		return new ArrayList<>( mapOfMedical_requisition_lotDTOTosearchColumn.getOrDefault(search_column,new HashSet<>()));
	}
	
	
	public List<Medical_requisition_lotDTO> getMedical_requisition_lotDTOBylastModificationTime(long lastModificationTime) {
		return new ArrayList<>( mapOfMedical_requisition_lotDTOTolastModificationTime.getOrDefault(lastModificationTime,new HashSet<>()));
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "medical_requisition_lot";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


