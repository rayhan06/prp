package medical_requisition_lot;
import java.util.*; 
import util.*; 


public class MedicalRequisitionItemDTO extends CommonDTO
{

	public long medicalRequisitionLotId = -1;
	public long drugInformationType = -1;
	public int totalCurrentStock = -1;
    public String expiryDateList = "";
    public String currentStockList = "";
	public int quantity = 1;
    public String remarks = "";
	
	public List<MedicalRequisitionItemDTO> medicalRequisitionItemDTOList = new ArrayList<>();
	
    @Override
	public String toString() {
            return "$MedicalRequisitionItemDTO[" +
            " iD = " + iD +
            " medicalRequisitionLotId = " + medicalRequisitionLotId +
            " drugInformationType = " + drugInformationType +
            " totalCurrentStock = " + totalCurrentStock +
            " expiryDateList = " + expiryDateList +
            " currentStockList = " + currentStockList +
            " quantity = " + quantity +
            " remarks = " + remarks +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}