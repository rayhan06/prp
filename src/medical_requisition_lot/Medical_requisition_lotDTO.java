package medical_requisition_lot;
import java.util.*; 
import util.*; 


public class Medical_requisition_lotDTO extends CommonDTO
{

	public long requisitionToOrganogramId = -1;
    public String subject = "";
    public String description = "";
	public long insertedByUserId = -1;
	public long insertedByOrganogramId = -1;
	public long insertionDate = -1;
	
	public List<MedicalRequisitionItemDTO> medicalRequisitionItemDTOList = new ArrayList<>();
	
    @Override
	public String toString() {
            return "$Medical_requisition_lotDTO[" +
            " iD = " + iD +
            " requisitionToOrganogramId = " + requisitionToOrganogramId +
            " subject = " + subject +
            " description = " + description +
            " insertedByUserId = " + insertedByUserId +
            " insertedByOrganogramId = " + insertedByOrganogramId +
            " jobCat = " + jobCat +
            " insertionDate = " + insertionDate +
            " searchColumn = " + searchColumn +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}