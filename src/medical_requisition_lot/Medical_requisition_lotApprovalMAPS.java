package medical_requisition_lot;
import util.*;

public class Medical_requisition_lotApprovalMAPS extends CommonMaps
{	
	public Medical_requisition_lotApprovalMAPS(String tableName)
	{
		
		java_allfield_type_map.put("requisition_to_organogram_id".toLowerCase(), "Long");
		java_allfield_type_map.put("subject".toLowerCase(), "String");
		java_allfield_type_map.put("description".toLowerCase(), "String");
		java_allfield_type_map.put("inserted_by_user_id".toLowerCase(), "Long");
		java_allfield_type_map.put("inserted_by_organogram_id".toLowerCase(), "Long");
		java_allfield_type_map.put("insertion_date".toLowerCase(), "Long");

		java_allfield_type_map.put("job_cat", "Integer");
		java_allfield_type_map.put("approval_status_cat", "Integer");
		java_allfield_type_map.put("initiator", "Long");
		java_allfield_type_map.put("assigned_to", "Long");
		java_allfield_type_map.put("starting_date", "Long");
		java_allfield_type_map.put("ending_date", "Long");
		
		java_table_map.put("approval_status_cat", "approval_summary");
		java_table_map.put("initiator", "approval_summary");
		java_table_map.put("assigned_to", "approval_summary");
	}

}