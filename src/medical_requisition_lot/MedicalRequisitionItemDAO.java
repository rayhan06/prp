package medical_requisition_lot;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;
import sessionmanager.SessionConstants;
import java.sql.SQLException;
import common.ConnectionAndStatementUtil;


import org.apache.log4j.Logger;

import dbm.*;

import repository.RepositoryManager;

import util.*;
import pb.*;
import user.UserDTO;

public class MedicalRequisitionItemDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());

	public MedicalRequisitionItemDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";
		commonMaps = new MedicalRequisitionItemMAPS(tableName);
		columnNames = new String[] 
		{
			"ID",
			"medical_requisition_lot_id",
			"drug_information_type",
			"total_current_stock",
			"expiry_date_list",
			"current_stock_list",
			"quantity",
			"remarks",
			"isDeleted",
			"lastModificationTime"
		};
	}
	
	public MedicalRequisitionItemDAO()
	{
		this("medical_requisition_item");		
	}
	
	public void setSearchColumn(MedicalRequisitionItemDTO medicalrequisitionitemDTO)
	{
		medicalrequisitionitemDTO.searchColumn = "";
		medicalrequisitionitemDTO.searchColumn += CommonDAO.getName("English", "drug_information", medicalrequisitionitemDTO.drugInformationType) + " " + CommonDAO.getName("Bangla", "drug_information", medicalrequisitionitemDTO.drugInformationType) + " ";
		medicalrequisitionitemDTO.searchColumn += medicalrequisitionitemDTO.expiryDateList + " ";
		medicalrequisitionitemDTO.searchColumn += medicalrequisitionitemDTO.currentStockList + " ";
		medicalrequisitionitemDTO.searchColumn += medicalrequisitionitemDTO.remarks + " ";
	}
	
	public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException
	{
		MedicalRequisitionItemDTO medicalrequisitionitemDTO = (MedicalRequisitionItemDTO)commonDTO;
		int index = 1;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(medicalrequisitionitemDTO);
		if(isInsert)
		{
			ps.setObject(index++,medicalrequisitionitemDTO.iD);
		}
		ps.setObject(index++,medicalrequisitionitemDTO.medicalRequisitionLotId);
		ps.setObject(index++,medicalrequisitionitemDTO.drugInformationType);
		ps.setObject(index++,medicalrequisitionitemDTO.totalCurrentStock);
		ps.setObject(index++,medicalrequisitionitemDTO.expiryDateList);
		ps.setObject(index++,medicalrequisitionitemDTO.currentStockList);
		ps.setObject(index++,medicalrequisitionitemDTO.quantity);
		ps.setObject(index++,medicalrequisitionitemDTO.remarks);
		if(isInsert)
		{
			ps.setObject(index++, 0);
			ps.setObject(index++, lastModificationTime);
		}
	}
	
	public MedicalRequisitionItemDTO build(ResultSet rs)
	{
		try
		{
			MedicalRequisitionItemDTO medicalrequisitionitemDTO = new MedicalRequisitionItemDTO();
			medicalrequisitionitemDTO.iD = rs.getLong("ID");
			medicalrequisitionitemDTO.medicalRequisitionLotId = rs.getLong("medical_requisition_lot_id");
			medicalrequisitionitemDTO.drugInformationType = rs.getLong("drug_information_type");
			medicalrequisitionitemDTO.totalCurrentStock = rs.getInt("total_current_stock");
			medicalrequisitionitemDTO.expiryDateList = rs.getString("expiry_date_list");
			medicalrequisitionitemDTO.currentStockList = rs.getString("current_stock_list");
			medicalrequisitionitemDTO.quantity = rs.getInt("quantity");
			medicalrequisitionitemDTO.remarks = rs.getString("remarks");
			medicalrequisitionitemDTO.isDeleted = rs.getInt("isDeleted");
			medicalrequisitionitemDTO.lastModificationTime = rs.getLong("lastModificationTime");
			return medicalrequisitionitemDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
	
	
	public List<MedicalRequisitionItemDTO> getMedicalRequisitionItemDTOListByMedicalRequisitionLotID(long medicalRequisitionLotID) throws Exception
	{
		String sql = "SELECT * FROM medical_requisition_item where isDeleted=0 and medical_requisition_lot_id="+medicalRequisitionLotID+" order by medical_requisition_item.lastModificationTime";
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);		
	}

	public MedicalRequisitionItemDTO getDTOByID (long id)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID = " + id;
		MedicalRequisitionItemDTO medicalrequisitionitemDTO = ConnectionAndStatementUtil.getT(sql,this::build);
		return medicalrequisitionitemDTO;
	}

	
	public List<MedicalRequisitionItemDTO> getDTOs(Collection recordIDs)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID IN ( ";
		for(int i = 0;i<recordIDs.size();i++){
			if(i!=0){
				sql+=",";
			}
			sql+=((ArrayList)recordIDs).get(i);
		}
		sql+=")  order by lastModificationTime desc";
		printSql(sql);
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);		
	}
	
	public List<MedicalRequisitionItemDTO> getAllMedicalRequisitionItem (boolean isFirstReload)
    {
		String sql = "SELECT * FROM " + tableName + " WHERE ";
		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by " + tableName + ".lastModificationTime desc";
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);	
    }

	
	public List<MedicalRequisitionItemDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO)
	{
		return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
	}

	
	public List<MedicalRequisitionItemDTO> getDTOs(Hashtable  p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
			String filter, boolean tableHasJobCat)
	{
		String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat);
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);			
	}
				
}
	