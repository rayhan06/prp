package medical_requisition_lot;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;
import sessionmanager.SessionConstants;
import java.sql.SQLException;
import common.ConnectionAndStatementUtil;


import org.apache.log4j.Logger;

import dbm.*;

import repository.RepositoryManager;

import util.*;
import pb.*;
import user.UserDTO;

public class Medical_requisition_lotDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());

	public Medical_requisition_lotDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";
		commonMaps = new Medical_requisition_lotMAPS(tableName);
		approvalMaps = new Medical_requisition_lotApprovalMAPS("medical_requisition_lot");
		columnNames = new String[] 
		{
			"ID",
			"requisition_to_organogram_id",
			"subject",
			"description",
			"inserted_by_user_id",
			"inserted_by_organogram_id",
			"job_cat",
			"insertion_date",
			"search_column",
			"isDeleted",
			"lastModificationTime"
		};
	}
	
	public Medical_requisition_lotDAO()
	{
		this("medical_requisition_lot");		
	}
	
	public void setSearchColumn(Medical_requisition_lotDTO medical_requisition_lotDTO)
	{
		medical_requisition_lotDTO.searchColumn = "";
		medical_requisition_lotDTO.searchColumn += medical_requisition_lotDTO.subject + " ";
				
	}
	
	public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException
	{
		Medical_requisition_lotDTO medical_requisition_lotDTO = (Medical_requisition_lotDTO)commonDTO;
		int index = 1;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(medical_requisition_lotDTO);
		if(isInsert)
		{
			ps.setObject(index++,medical_requisition_lotDTO.iD);
		}
		ps.setObject(index++,medical_requisition_lotDTO.requisitionToOrganogramId);
		ps.setObject(index++,medical_requisition_lotDTO.subject);
		ps.setObject(index++,medical_requisition_lotDTO.description);
		ps.setObject(index++,medical_requisition_lotDTO.insertedByUserId);
		ps.setObject(index++,medical_requisition_lotDTO.insertedByOrganogramId);
		ps.setObject(index++,medical_requisition_lotDTO.jobCat);
		ps.setObject(index++,medical_requisition_lotDTO.insertionDate);
		ps.setObject(index++,medical_requisition_lotDTO.searchColumn);
		if(isInsert)
		{
			ps.setObject(index++, 0);
			ps.setObject(index++, lastModificationTime);
		}
	}
	
	public Medical_requisition_lotDTO build(ResultSet rs)
	{
		try
		{
			Medical_requisition_lotDTO medical_requisition_lotDTO = new Medical_requisition_lotDTO();
			medical_requisition_lotDTO.iD = rs.getLong("ID");
			medical_requisition_lotDTO.requisitionToOrganogramId = rs.getLong("requisition_to_organogram_id");
			medical_requisition_lotDTO.subject = rs.getString("subject");
			medical_requisition_lotDTO.description = rs.getString("description");
			medical_requisition_lotDTO.insertedByUserId = rs.getLong("inserted_by_user_id");
			medical_requisition_lotDTO.insertedByOrganogramId = rs.getLong("inserted_by_organogram_id");
			medical_requisition_lotDTO.jobCat = rs.getInt("job_cat");
			medical_requisition_lotDTO.insertionDate = rs.getLong("insertion_date");
			medical_requisition_lotDTO.searchColumn = rs.getString("search_column");
			medical_requisition_lotDTO.isDeleted = rs.getInt("isDeleted");
			medical_requisition_lotDTO.lastModificationTime = rs.getLong("lastModificationTime");
			return medical_requisition_lotDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
	
	

	public Medical_requisition_lotDTO getDTOByID (long id)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID = " + id;
		Medical_requisition_lotDTO medical_requisition_lotDTO = ConnectionAndStatementUtil.getT(sql,this::build);
		try {
			MedicalRequisitionItemDAO medicalRequisitionItemDAO = new MedicalRequisitionItemDAO("medical_requisition_item");			
			List<MedicalRequisitionItemDTO> medicalRequisitionItemDTOList = medicalRequisitionItemDAO.getMedicalRequisitionItemDTOListByMedicalRequisitionLotID(medical_requisition_lotDTO.iD);
			medical_requisition_lotDTO.medicalRequisitionItemDTOList = medicalRequisitionItemDTOList;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}			
		return medical_requisition_lotDTO;
	}

	
	public List<Medical_requisition_lotDTO> getDTOs(Collection recordIDs)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID IN ( ";
		for(int i = 0;i<recordIDs.size();i++){
			if(i!=0){
				sql+=",";
			}
			sql+=((ArrayList)recordIDs).get(i);
		}
		sql+=")  order by lastModificationTime desc";
		printSql(sql);
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);		
	}
	
	public List<Medical_requisition_lotDTO> getAllMedical_requisition_lot (boolean isFirstReload)
    {
		String sql = "SELECT * FROM " + tableName + " WHERE ";
		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by " + tableName + ".lastModificationTime desc";
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);	
    }

	
	public List<Medical_requisition_lotDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO)
	{
		return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
	}

	
	public List<Medical_requisition_lotDTO> getDTOs(Hashtable  p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
			String filter, boolean tableHasJobCat)
	{
		String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat);
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);			
	}
	public String getSqlWithSearchCriteriaForNormalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
			UserDTO userDTO, String filter, boolean tableHasJobCat)
    {
		boolean viewAll = false;
		
		if(p_searchCriteria != null && p_searchCriteria.get("ViewAll") != null)
		{
			System.out.println("ViewAll = " + p_searchCriteria.get("ViewAll"));
			viewAll = true;
		}
		else
		{
			System.out.println("ViewAll is null ");
		}
		
		String sql = "SELECT ";
		if(category == GETIDS)
		{
			sql += " " + tableName + ".ID as ID ";
		}
		else if(category == GETCOUNT)
		{
			sql += " count( " + tableName + ".ID) as countID ";
		}
		else if(category == GETDTOS)
		{
			sql += " distinct " + tableName + ".* ";
		}
		sql += "FROM " + tableName + " ";
		sql += joinSQL;
		

		
		String AnyfieldSql = "";
		String AllFieldSql = "";
		
		if(p_searchCriteria != null)
		{
						
			
			Enumeration names = p_searchCriteria.keys();
			String str, value;
			
			AnyfieldSql = "(";
			
			if(p_searchCriteria.get("AnyField")!= null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase(""))
			{
				AnyfieldSql+= tableName + ".search_column like '%" + p_searchCriteria.get("AnyField").toString() + "%'";			
			}
			AnyfieldSql += ")";
			System.out.println("AnyfieldSql = " + AnyfieldSql);
			
			AllFieldSql = "(";
			int i = 0;
			while(names.hasMoreElements())
			{				
				str = (String) names.nextElement();
				value = (String)p_searchCriteria.get(str);
		        System.out.println(str + ": " + value);
				if(value != null && !value.equalsIgnoreCase("") && (
						 str.equals("subject")
						|| str.equals("insertion_date_start")
						|| str.equals("insertion_date_end")
				)
						
				)
				{
					if(p_searchCriteria.get(str).equals("any"))
					{
						continue;
					}

					if( i > 0)
					{
						AllFieldSql+= " AND  ";
					}
					
					 if(str.equals("subject"))
					{
						AllFieldSql += "" + tableName + ".subject like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("insertion_date_start"))
					{
						AllFieldSql += "" + tableName + ".insertion_date >= " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("insertion_date_end"))
					{
						AllFieldSql += "" + tableName + ".insertion_date <= " + p_searchCriteria.get(str);
						i ++;
					}
					
					
				}
			}
			
			AllFieldSql += ")";
			System.out.println("AllFieldSql = " + AllFieldSql);
			
			
		}
		
		
		sql += " WHERE ";
		
		sql += " (" + tableName + ".isDeleted = 0 ";			
		sql += ")";
		
		
		if(!filter.equalsIgnoreCase(""))
		{
			sql += " and " + filter + " ";
		}
		
		if(!AnyfieldSql.equals("()") && !AnyfieldSql.equals(""))
		{
			sql += " AND " + AnyfieldSql;
			
		}
		if(!AllFieldSql.equals("()") && !AllFieldSql.equals(""))
		{			
			sql += " AND " + AllFieldSql;
		}
		
	
			
		sql += " order by " + tableName + ".lastModificationTime desc ";

		printSql(sql);
		
		if(limit >= 0)
		{
			sql += " limit " + limit;
		}
		if(offset >= 0)
		{
			sql += " offset " + offset;
		}
		
		System.out.println("-------------- sql = " + sql);
		
		return sql;
    }
	
	public String getSqlWithSearchCriteria(Hashtable p_searchCriteria, int limit, int offset, int category,
			boolean isPermanentTable, UserDTO userDTO, String filter, boolean tableHasJobCat)
    {
		if(isPermanentTable)
		{
			return getSqlWithSearchCriteriaForNormalSearch(p_searchCriteria, limit, offset, category, userDTO, filter, tableHasJobCat);
		}
		else
		{
			return getSqlWithSearchCriteriaForApprovalSearch(p_searchCriteria, limit, offset, category, userDTO, filter, tableHasJobCat);
		}
				
    }
				
}
	