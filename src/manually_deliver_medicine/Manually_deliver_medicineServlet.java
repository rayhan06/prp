package manually_deliver_medicine;


import java.text.SimpleDateFormat;



import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import org.apache.log4j.Logger;


import permission.MenuConstants;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.*;
import workflow.WorkflowController;

import javax.servlet.http.*;
import java.util.*;

import common.BaseServlet;
import drug_information.Drug_informationDAO;
import drug_information.Drug_informationDTO;

import medical_inventory_lot.MedicalInventoryInDAO;
import medical_inventory_lot.MedicalInventoryInDTO;
import medical_inventory_out.Medical_inventory_outDAO;
import medical_inventory_out.Medical_inventory_outDTO;
import pb.ErrorMessage;

import com.google.gson.Gson;

import borrow_medicine.PersonalStockDAO;
import borrow_medicine.PersonalStockDTO;

import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;




/**
 * Servlet implementation class Manually_deliver_medicineServlet
 */
@WebServlet("/Manually_deliver_medicineServlet")
@MultipartConfig
public class Manually_deliver_medicineServlet extends BaseServlet 
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Manually_deliver_medicineServlet.class);

    @Override
    public String getTableName() {
        return Manually_deliver_medicineDAO.getInstance().getTableName();
    }

    @Override
    public String getServletName() {
        return "Manually_deliver_medicineServlet";
    }

    @Override
    public Manually_deliver_medicineDAO getCommonDAOService() {
        return Manually_deliver_medicineDAO.getInstance();
    }
    
    @Override
    public int[] getAddPageMenuConstants() {
        return new int[] {MenuConstants.MANUALLY_DELIVER_MEDICINE_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[] {MenuConstants.MANUALLY_DELIVER_MEDICINE_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[] {MenuConstants.MANUALLY_DELIVER_MEDICINE_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Manually_deliver_medicineServlet.class;
    }
	MedicineDetailsDAO medicineDetailsDAO = MedicineDetailsDAO.getInstance();
    private final Gson gson = new Gson();
 	

	@Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception 
	{
		
		request.setAttribute("failureMessage", "");
		System.out.println("%%%% addManually_deliver_medicine");
		String language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
		Manually_deliver_medicineDTO manually_deliver_medicineDTO;
		SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
					
		if(addFlag)
		{
			manually_deliver_medicineDTO = new Manually_deliver_medicineDTO();
		}
		else
		{
			manually_deliver_medicineDTO = Manually_deliver_medicineDAO.getInstance().getDTOFromID(Long.parseLong(request.getParameter("iD")));
		}
		
		String Value = "";

		manually_deliver_medicineDTO.deliveryDate = TimeConverter.getToday();

		Value = request.getParameter("employeeType");

		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("employeeType = " + Value);
		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			manually_deliver_medicineDTO.employeeType = Long.parseLong(Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}

		Value = request.getParameter("employeeUserName");

		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("employeeUserName = " + Value);
		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			manually_deliver_medicineDTO.employeeUserName = (Value);	
			UserDTO employeeUserDTO = UserRepository.getUserDTOByUserName(manually_deliver_medicineDTO.employeeUserName);
			if(employeeUserDTO != null)
			{
				manually_deliver_medicineDTO.employeeOrganogramId = employeeUserDTO.organogramID;
			}
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}

		
		Value = request.getParameter("patientName");

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("patientName = " + Value);
		if(Value != null)
		{
			manually_deliver_medicineDTO.patientName = (Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}

		Value = request.getParameter("phoneNumber");

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("phoneNumber = " + Value);
		if(Value != null)
		{
			manually_deliver_medicineDTO.phoneNumber = (Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}

		Value = request.getParameter("doctorOrganogramId");

		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("doctorOrganogramId = " + Value);
		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			manually_deliver_medicineDTO.doctorOrganogramId = Long.parseLong(Value);
			if(manually_deliver_medicineDTO.doctorOrganogramId == -1)
			{
				 throw new Exception(ErrorMessage.getGenericInvalidMessage(language));

			}
			manually_deliver_medicineDTO.doctorUserName = WorkflowController.getUserNameFromOrganogramId(manually_deliver_medicineDTO.doctorOrganogramId);
			manually_deliver_medicineDTO.doctorName = WorkflowController.getNameFromOrganogramId(manually_deliver_medicineDTO.doctorOrganogramId, language);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}

		

		if(addFlag)
		{
			manually_deliver_medicineDTO.insertedByUserId = userDTO.ID;
		}


		if(addFlag)
		{
			manually_deliver_medicineDTO.insertedByOrganogramId = userDTO.organogramID;
		}


		if(addFlag)
		{				
			manually_deliver_medicineDTO.insertionDate = TimeConverter.getToday();
		}			


		manually_deliver_medicineDTO.lastModifierUser = userDTO.userName;

		System.out.println("Done adding  addManually_deliver_medicine dto = " + manually_deliver_medicineDTO);

		if(addFlag == true)
		{
			Manually_deliver_medicineDAO.getInstance().add(manually_deliver_medicineDTO);
		}
		else
		{				
			Manually_deliver_medicineDAO.getInstance().update(manually_deliver_medicineDTO);										
		}
		
		List<MedicineDetailsDTO> medicineDetailsDTOList = createMedicineDetailsDTOListByRequest(request, language, manually_deliver_medicineDTO, userDTO);
		Medical_inventory_outDAO medical_inventory_outDAO = new Medical_inventory_outDAO();
		MedicalInventoryInDAO medicalInventoryInDAO = new MedicalInventoryInDAO();
		Drug_informationDAO drug_informationDAO = new Drug_informationDAO();

		if(addFlag == true) //add or validate
		{
			if(medicineDetailsDTOList != null)
			{				
				for(MedicineDetailsDTO medicineDetailsDTO: medicineDetailsDTOList)
				{
					medicineDetailsDAO.add(medicineDetailsDTO);
					Drug_informationDTO drug_informationDTO = drug_informationDAO.getDTOByID(medicineDetailsDTO.drugInformationType);
					if(drug_informationDTO != null)
					{
						int quantity = medicineDetailsDTO.quantity;
						if(drug_informationDTO.availableStock - quantity <= 0)
						{
							quantity = drug_informationDTO.availableStock;
						}
						
						UserDTO patientDTO = UserRepository.getUserDTOByOrganogramID(manually_deliver_medicineDTO.employeeOrganogramId);
						
						Medical_inventory_outDTO medical_inventory_outDTO = new Medical_inventory_outDTO(
								drug_informationDTO,
								quantity, userDTO, MedicalInventoryInDTO.TR_RECEIVE_MEDICINE,
								patientDTO, SessionConstants.MEDICAL_ITEM_DRUG,
								manually_deliver_medicineDTO.patientName,
								manually_deliver_medicineDTO.phoneNumber, -1, -1);				
						medical_inventory_outDAO.add(medical_inventory_outDTO);
						
						PersonalStockDTO personalStockDTO = PersonalStockDAO.getInstance().getDTOByUserNameAndDrugId(manually_deliver_medicineDTO.employeeUserName,
								drug_informationDTO.iD, SessionConstants.MEDICAL_ITEM_DRUG);
						if(personalStockDTO == null)
						{
							personalStockDTO = new PersonalStockDTO(manually_deliver_medicineDTO, medicineDetailsDTO, drug_informationDTO);
							PersonalStockDAO.getInstance().add(personalStockDTO);
						}
						else
						{
							personalStockDTO.quantity += medicineDetailsDTO.quantity;
							PersonalStockDAO.getInstance().update(personalStockDTO);
						}
						
					}
				
				}
			}
		
		}			
		else
		{
			List<Long> childIdsFromRequest = medicineDetailsDAO.getChildIdsFromRequest(request, "medicineDetails");
			//delete the removed children
			medicineDetailsDAO.deleteChildrenNotInList("manually_deliver_medicine", "medicine_details", manually_deliver_medicineDTO.iD, childIdsFromRequest);
			List<Long> childIDsInDatabase = medicineDetailsDAO.getChilIds("manually_deliver_medicine", "medicine_details", manually_deliver_medicineDTO.iD);
			
			
			if(childIdsFromRequest != null)
			{
				for(int i = 0; i < childIdsFromRequest.size(); i ++)
				{
					Long childIDFromRequest = childIdsFromRequest.get(i);
					if(childIDsInDatabase.contains(childIDFromRequest))
					{
						MedicineDetailsDTO medicineDetailsDTO =  createMedicineDetailsDTOByRequestAndIndex(request, false, i, language, manually_deliver_medicineDTO, userDTO);
						medicineDetailsDAO.update(medicineDetailsDTO);
					}
					else
					{
						MedicineDetailsDTO medicineDetailsDTO =  createMedicineDetailsDTOByRequestAndIndex(request, true, i, language, manually_deliver_medicineDTO, userDTO);
						medicineDetailsDAO.add(medicineDetailsDTO);
					}
				}
			}
			else
			{
				medicineDetailsDAO.deleteChildrenByParent(manually_deliver_medicineDTO.iD, "manually_deliver_medicine_id");
			}
			
		}					
		return manually_deliver_medicineDTO;

	}
	private List<MedicineDetailsDTO> createMedicineDetailsDTOListByRequest(HttpServletRequest request, String language, Manually_deliver_medicineDTO manually_deliver_medicineDTO, UserDTO userDTO) throws Exception{ 
		List<MedicineDetailsDTO> medicineDetailsDTOList = new ArrayList<MedicineDetailsDTO>();
		if(request.getParameterValues("medicineDetails.iD") != null) 
		{
			int medicineDetailsItemNo = request.getParameterValues("medicineDetails.iD").length;
			
			
			for(int index=0;index<medicineDetailsItemNo;index++){
				MedicineDetailsDTO medicineDetailsDTO = createMedicineDetailsDTOByRequestAndIndex(request,true,index, language, manually_deliver_medicineDTO, userDTO);
				if(medicineDetailsDTO != null)
				{
					medicineDetailsDTOList.add(medicineDetailsDTO);
				}
				
			}
			
			return medicineDetailsDTOList;
		}
		return null;
	}
	
	private MedicineDetailsDTO createMedicineDetailsDTOByRequestAndIndex(HttpServletRequest request, boolean addFlag, int index, String language, Manually_deliver_medicineDTO manually_deliver_medicineDTO, UserDTO userDTO) throws Exception{
	
		MedicineDetailsDTO medicineDetailsDTO;
		if(addFlag == true )
		{
			medicineDetailsDTO = new MedicineDetailsDTO();
		}
		else
		{
			medicineDetailsDTO = medicineDetailsDAO.getDTOByID(Long.parseLong(request.getParameterValues("medicineDetails.iD")[index]));
		}
		String path = getServletContext().getRealPath("/img2/");
		SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
		
		medicineDetailsDTO.manuallyDeliverMedicineId = manually_deliver_medicineDTO.iD; 

		String Value = "";

		Value = request.getParameterValues("medicineDetails.drugInformationType")[index];

		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("drugInformationType = " + Value);
		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			medicineDetailsDTO.drugInformationType = Long.parseLong(Value);
			if(medicineDetailsDTO.drugInformationType == -1)
			{
				return null;
			}
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}

		Value = request.getParameterValues("medicineDetails.quantity")[index];

		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("quantity = " + Value);
		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			medicineDetailsDTO.quantity = Integer.parseInt(Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}
		return medicineDetailsDTO;
	
	}	
}

