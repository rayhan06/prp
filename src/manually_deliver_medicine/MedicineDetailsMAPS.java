package manually_deliver_medicine;
import java.util.*; 
import util.*;


public class MedicineDetailsMAPS extends CommonMaps
{	
	public MedicineDetailsMAPS(String tableName)
	{
		


		java_SQL_map.put("manually_deliver_medicine_id".toLowerCase(), "manuallyDeliverMedicineId".toLowerCase());
		java_SQL_map.put("drug_information_type".toLowerCase(), "drugInformationType".toLowerCase());
		java_SQL_map.put("quantity".toLowerCase(), "quantity".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Manually Deliver Medicine Id".toLowerCase(), "manuallyDeliverMedicineId".toLowerCase());
		java_Text_map.put("Drug Information".toLowerCase(), "drugInformationType".toLowerCase());
		java_Text_map.put("Quantity".toLowerCase(), "quantity".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
		java_Text_map.put("Search Column".toLowerCase(), "searchColumn".toLowerCase());
			
	}

}