package manually_deliver_medicine;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.sql.SQLException;
import common.CommonDAOService;
import org.apache.log4j.Logger;
import util.*;
import pb.*;

public class MedicineDetailsDAO  implements CommonDAOService<MedicineDetailsDTO>
{
	
	Logger logger = Logger.getLogger(getClass());
	
	private static String addQuery = "";
	private static String updateQuery = "";
	private String[] columnNames = null;

	private MedicineDetailsDAO()
	{
		columnNames = new String[] 
		{
			"ID",
			"manually_deliver_medicine_id",
			"drug_information_type",
			"quantity",
			"search_column",
			"isDeleted",
			
			"lastModificationTime"
		};
		updateQuery = getUpdateQuery(columnNames);
        addQuery = getAddQuery(columnNames);
		
		searchMap.put("drug_information_type"," and (drug_information_type = ?)");
		searchMap.put("AnyField"," and (search_column like ?)");
	}

	private final Map<String,String> searchMap = new HashMap<>();

    private static class LazyLoader
    {
    	static final MedicineDetailsDAO INSTANCE = new MedicineDetailsDAO();
	}

	public static MedicineDetailsDAO getInstance()
	{
    	return LazyLoader.INSTANCE;
	}
	
	public void setSearchColumn(MedicineDetailsDTO medicinedetailsDTO)
	{
		medicinedetailsDTO.searchColumn = "";
		medicinedetailsDTO.searchColumn += CommonDAO.getName("English", "drug_information", medicinedetailsDTO.drugInformationType) + " " + CommonDAO.getName("Bangla", "drug_information", medicinedetailsDTO.drugInformationType) + " ";
	}
	
	@Override
	public void set(PreparedStatement ps, MedicineDetailsDTO medicinedetailsDTO, boolean isInsert) throws SQLException
	{
		int index = 0;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(medicinedetailsDTO);
		if(isInsert)
		{
			ps.setObject(++index,medicinedetailsDTO.iD);
		}
		ps.setObject(++index,medicinedetailsDTO.manuallyDeliverMedicineId);
		ps.setObject(++index,medicinedetailsDTO.drugInformationType);
		ps.setObject(++index,medicinedetailsDTO.quantity);
		ps.setObject(++index,medicinedetailsDTO.searchColumn);
		if(isInsert)
		{
			ps.setObject(++index,medicinedetailsDTO.isDeleted);
		}
		ps.setObject(++index, lastModificationTime);
	
	}
	
	@Override
	public MedicineDetailsDTO buildObjectFromResultSet(ResultSet rs)
	{
		try
		{
			MedicineDetailsDTO medicinedetailsDTO = new MedicineDetailsDTO();
			int i = 0;
			medicinedetailsDTO.iD = rs.getLong(columnNames[i++]);
			medicinedetailsDTO.manuallyDeliverMedicineId = rs.getLong(columnNames[i++]);
			medicinedetailsDTO.drugInformationType = rs.getLong(columnNames[i++]);
			medicinedetailsDTO.quantity = rs.getInt(columnNames[i++]);
			medicinedetailsDTO.searchColumn = rs.getString(columnNames[i++]);
			medicinedetailsDTO.isDeleted = rs.getInt(columnNames[i++]);
			medicinedetailsDTO.lastModificationTime = rs.getLong(columnNames[i++]);
			
			return medicinedetailsDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
		
	public MedicineDetailsDTO getDTOByID (long id)
	{
		return getDTOFromID(id);
	}

	@Override
	public String getTableName() {
		return "medicine_details";
	}

	@Override
	public Map<String, String> getSearchMap() {
		return searchMap;
	}

	public long add(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((MedicineDetailsDTO) commonDTO,addQuery,true);
	}

	public long update(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((MedicineDetailsDTO) commonDTO,updateQuery,false);
	}
	
	@Override
	public String getLastModifierUser(){
		return "";
    }
				
}
	