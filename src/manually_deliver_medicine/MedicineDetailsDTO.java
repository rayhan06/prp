package manually_deliver_medicine;
import java.util.*; 
import util.*; 


public class MedicineDetailsDTO extends CommonDTO
{

	public long manuallyDeliverMedicineId = -1;
	public long drugInformationType = -1;
	public int quantity = -1;
	
	public List<MedicineDetailsDTO> medicineDetailsDTOList = new ArrayList<>();
	
    @Override
	public String toString() {
            return "$MedicineDetailsDTO[" +
            " iD = " + iD +
            " manuallyDeliverMedicineId = " + manuallyDeliverMedicineId +
            " drugInformationType = " + drugInformationType +
            " quantity = " + quantity +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            " searchColumn = " + searchColumn +
            "]";
    }

}