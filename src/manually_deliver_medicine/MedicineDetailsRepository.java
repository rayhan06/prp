package manually_deliver_medicine;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import com.google.gson.Gson;

import repository.Repository;
import repository.RepositoryManager;


public class MedicineDetailsRepository implements Repository {
	MedicineDetailsDAO medicinedetailsDAO = null;
	
	static Logger logger = Logger.getLogger(MedicineDetailsRepository.class);
	Map<Long, MedicineDetailsDTO>mapOfMedicineDetailsDTOToiD;
	Map<Long, Set<MedicineDetailsDTO> >mapOfMedicineDetailsDTOTomanuallyDeliverMedicineId;
	Gson gson;

  
	private MedicineDetailsRepository(){
		medicinedetailsDAO = MedicineDetailsDAO.getInstance();
		mapOfMedicineDetailsDTOToiD = new ConcurrentHashMap<>();
		mapOfMedicineDetailsDTOTomanuallyDeliverMedicineId = new ConcurrentHashMap<>();
		gson = new Gson();
		RepositoryManager.getInstance().addRepository(this);
	}

	private static class LazyLoader{
        final static MedicineDetailsRepository INSTANCE = new MedicineDetailsRepository();
    }

    public static MedicineDetailsRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

	public void reload(boolean reloadAll){
		try {
			List<MedicineDetailsDTO> medicinedetailsDTOs = medicinedetailsDAO.getAllDTOs(reloadAll);
			for(MedicineDetailsDTO medicinedetailsDTO : medicinedetailsDTOs) {
				MedicineDetailsDTO oldMedicineDetailsDTO = getMedicineDetailsDTOByiD(medicinedetailsDTO.iD);
				if( oldMedicineDetailsDTO != null ) {
					mapOfMedicineDetailsDTOToiD.remove(oldMedicineDetailsDTO.iD);
				
					if(mapOfMedicineDetailsDTOTomanuallyDeliverMedicineId.containsKey(oldMedicineDetailsDTO.manuallyDeliverMedicineId)) {
						mapOfMedicineDetailsDTOTomanuallyDeliverMedicineId.get(oldMedicineDetailsDTO.manuallyDeliverMedicineId).remove(oldMedicineDetailsDTO);
					}
					if(mapOfMedicineDetailsDTOTomanuallyDeliverMedicineId.get(oldMedicineDetailsDTO.manuallyDeliverMedicineId).isEmpty()) {
						mapOfMedicineDetailsDTOTomanuallyDeliverMedicineId.remove(oldMedicineDetailsDTO.manuallyDeliverMedicineId);
					}
					
					
				}
				if(medicinedetailsDTO.isDeleted == 0) 
				{
					
					mapOfMedicineDetailsDTOToiD.put(medicinedetailsDTO.iD, medicinedetailsDTO);
				
					if( ! mapOfMedicineDetailsDTOTomanuallyDeliverMedicineId.containsKey(medicinedetailsDTO.manuallyDeliverMedicineId)) {
						mapOfMedicineDetailsDTOTomanuallyDeliverMedicineId.put(medicinedetailsDTO.manuallyDeliverMedicineId, new HashSet<>());
					}
					mapOfMedicineDetailsDTOTomanuallyDeliverMedicineId.get(medicinedetailsDTO.manuallyDeliverMedicineId).add(medicinedetailsDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public MedicineDetailsDTO clone(MedicineDetailsDTO dto) {
		String raw = gson.toJson(dto);
		return gson.fromJson(raw, MedicineDetailsDTO.class);
	}
	
	
	public List<MedicineDetailsDTO> getMedicineDetailsList() {
		List <MedicineDetailsDTO> medicinedetailss = new ArrayList<MedicineDetailsDTO>(this.mapOfMedicineDetailsDTOToiD.values());
		return medicinedetailss;
	}
	
	public List<MedicineDetailsDTO> copyMedicineDetailsList() {
		List <MedicineDetailsDTO> medicinedetailss = getMedicineDetailsList();
		return medicinedetailss
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}
	
	
	
	public MedicineDetailsDTO getMedicineDetailsDTOByiD( long iD){
		return mapOfMedicineDetailsDTOToiD.get(iD);
	}
	
	public MedicineDetailsDTO copyMedicineDetailsDTOByiD( long iD){
		return clone(mapOfMedicineDetailsDTOToiD.get(iD));
	}
	
	
	public List<MedicineDetailsDTO> getMedicineDetailsDTOBymanuallyDeliverMedicineId(long manuallyDeliverMedicineId) {
		return new ArrayList<>( mapOfMedicineDetailsDTOTomanuallyDeliverMedicineId.getOrDefault(manuallyDeliverMedicineId,new HashSet<>()));
	}
	
	public List<MedicineDetailsDTO> copyMedicineDetailsDTOBymanuallyDeliverMedicineId(long manuallyDeliverMedicineId)
	{
		List <MedicineDetailsDTO> medicinedetailss = getMedicineDetailsDTOBymanuallyDeliverMedicineId(manuallyDeliverMedicineId);
		return medicinedetailss
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}

	
	@Override
	public String getTableName() {
		return medicinedetailsDAO.getTableName();
	}
}


