package manually_deliver_medicine;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import com.google.gson.Gson;

import repository.Repository;
import repository.RepositoryManager;


public class Manually_deliver_medicineRepository implements Repository {
	Manually_deliver_medicineDAO manually_deliver_medicineDAO = null;
	
	static Logger logger = Logger.getLogger(Manually_deliver_medicineRepository.class);
	Map<Long, Manually_deliver_medicineDTO>mapOfManually_deliver_medicineDTOToiD;
	Gson gson;

  
	private Manually_deliver_medicineRepository(){
		manually_deliver_medicineDAO = Manually_deliver_medicineDAO.getInstance();
		mapOfManually_deliver_medicineDTOToiD = new ConcurrentHashMap<>();
		gson = new Gson();
		RepositoryManager.getInstance().addRepository(this);
	}

	private static class LazyLoader{
        final static Manually_deliver_medicineRepository INSTANCE = new Manually_deliver_medicineRepository();
    }

    public static Manually_deliver_medicineRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

	public void reload(boolean reloadAll){
		try {
			List<Manually_deliver_medicineDTO> manually_deliver_medicineDTOs = manually_deliver_medicineDAO.getAllDTOs(reloadAll);
			for(Manually_deliver_medicineDTO manually_deliver_medicineDTO : manually_deliver_medicineDTOs) {
				Manually_deliver_medicineDTO oldManually_deliver_medicineDTO = getManually_deliver_medicineDTOByiD(manually_deliver_medicineDTO.iD);
				if( oldManually_deliver_medicineDTO != null ) {
					mapOfManually_deliver_medicineDTOToiD.remove(oldManually_deliver_medicineDTO.iD);
				
					
				}
				if(manually_deliver_medicineDTO.isDeleted == 0) 
				{
					
					mapOfManually_deliver_medicineDTOToiD.put(manually_deliver_medicineDTO.iD, manually_deliver_medicineDTO);
				
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public Manually_deliver_medicineDTO clone(Manually_deliver_medicineDTO dto) {
		String raw = gson.toJson(dto);
		return gson.fromJson(raw, Manually_deliver_medicineDTO.class);
	}
	
	
	public List<Manually_deliver_medicineDTO> getManually_deliver_medicineList() {
		List <Manually_deliver_medicineDTO> manually_deliver_medicines = new ArrayList<Manually_deliver_medicineDTO>(this.mapOfManually_deliver_medicineDTOToiD.values());
		return manually_deliver_medicines;
	}
	
	public List<Manually_deliver_medicineDTO> copyManually_deliver_medicineList() {
		List <Manually_deliver_medicineDTO> manually_deliver_medicines = getManually_deliver_medicineList();
		return manually_deliver_medicines
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}
	
	
	
	public Manually_deliver_medicineDTO getManually_deliver_medicineDTOByiD( long iD){
		return mapOfManually_deliver_medicineDTOToiD.get(iD);
	}
	
	public Manually_deliver_medicineDTO copyManually_deliver_medicineDTOByiD( long iD){
		return clone(mapOfManually_deliver_medicineDTOToiD.get(iD));
	}

	
	@Override
	public String getTableName() {
		return manually_deliver_medicineDAO.getTableName();
	}
}


