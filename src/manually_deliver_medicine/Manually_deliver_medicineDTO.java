package manually_deliver_medicine;
import java.util.*; 
import util.*; 


public class Manually_deliver_medicineDTO extends CommonDTO
{

	public long deliveryDate = System.currentTimeMillis();
	public long employeeType = -1;
	public long employeeOrganogramId = -1;
    public String employeeUserName = "";
    public String patientName = "";
    public String phoneNumber = "";
	public long doctorOrganogramId = -1;
    public String doctorUserName = "";
    public String doctorName = "";
	public long insertedByUserId = -1;
	public long insertedByOrganogramId = -1;
	public long insertionDate = -1;
    public String lastModifierUser = "";
    
    public static final int EMPLOYEE_VIP = 0;
    public static final int EMPLOYEE_PWD = 1;
    public static final int EMPLOYEE_SECURITY = 2;
    public static final int EMPLOYEE_EMERGENCY = 3;
    public static final int EMPLOYEE_PS = 4;
	
	public List<MedicineDetailsDTO> medicineDetailsDTOList = new ArrayList<>();
	
    @Override
	public String toString() {
            return "$Manually_deliver_medicineDTO[" +
            " iD = " + iD +
            " deliveryDate = " + deliveryDate +
            " employeeType = " + employeeType +
            " employeeOrganogramId = " + employeeOrganogramId +
            " employeeUserName = " + employeeUserName +
            " patientName = " + patientName +
            " phoneNumber = " + phoneNumber +
            " doctorOrganogramId = " + doctorOrganogramId +
            " doctorUserName = " + doctorUserName +
            " doctorName = " + doctorName +
            " insertedByUserId = " + insertedByUserId +
            " insertedByOrganogramId = " + insertedByOrganogramId +
            " insertionDate = " + insertionDate +
            " lastModifierUser = " + lastModifierUser +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            " searchColumn = " + searchColumn +
            "]";
    }

}