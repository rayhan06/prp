package manually_deliver_medicine;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.sql.SQLException;
import common.CommonDAOService;
import org.apache.log4j.Logger;
import util.*;
import pb.*;

public class Manually_deliver_medicineDAO  implements CommonDAOService<Manually_deliver_medicineDTO>
{
	
	Logger logger = Logger.getLogger(getClass());
	
	private static String addQuery = "";
	private static String updateQuery = "";
	private String[] columnNames = null;

	private Manually_deliver_medicineDAO()
	{
		columnNames = new String[] 
		{
			"ID",
			"delivery_date",
			"employee_type",
			"employee_organogram_id",
			"employee_user_name",
			"patient_name",
			"phone_number",
			"doctor_organogram_id",
			"doctor_user_name",
			"doctor_name",
			"inserted_by_user_id",
			"inserted_by_organogram_id",
			"insertion_date",
			"last_modifier_user",
			"search_column",
			"isDeleted",
			
			"lastModificationTime"
		};
		updateQuery = getUpdateQuery(columnNames);
        addQuery = getAddQuery(columnNames);
		
		searchMap.put("delivery_date_start"," and (delivery_date >= ?)");
		searchMap.put("delivery_date_end"," and (delivery_date <= ?)");
		searchMap.put("employee_type"," and (employee_type = ?)");
		searchMap.put("employee_user_name"," and (employee_user_name like ?)");
		searchMap.put("patient_name"," and (patient_name like ?)");
		searchMap.put("doctor_user_name"," and (doctor_user_name like ?)");
		searchMap.put("doctor_name"," and (doctor_name like ?)");
		searchMap.put("insertion_date_start"," and (insertion_date >= ?)");
		searchMap.put("insertion_date_end"," and (insertion_date <= ?)");
		searchMap.put("AnyField"," and (search_column like ?)");
	}

	private final Map<String,String> searchMap = new HashMap<>();

    private static class LazyLoader
    {
    	static final Manually_deliver_medicineDAO INSTANCE = new Manually_deliver_medicineDAO();
	}

	public static Manually_deliver_medicineDAO getInstance()
	{
    	return LazyLoader.INSTANCE;
	}
	
	public void setSearchColumn(Manually_deliver_medicineDTO manually_deliver_medicineDTO)
	{
		manually_deliver_medicineDTO.searchColumn = "";
		manually_deliver_medicineDTO.searchColumn += CatDAO.getName("English", "employee", manually_deliver_medicineDTO.employeeType) + " " + CatDAO.getName("Bangla", "employee", manually_deliver_medicineDTO.employeeType) + " ";
		manually_deliver_medicineDTO.searchColumn += manually_deliver_medicineDTO.employeeUserName + " ";
		manually_deliver_medicineDTO.searchColumn += manually_deliver_medicineDTO.patientName + " ";
		manually_deliver_medicineDTO.searchColumn += manually_deliver_medicineDTO.doctorUserName + " ";
		manually_deliver_medicineDTO.searchColumn += manually_deliver_medicineDTO.doctorName + " ";
	}
	
	@Override
	public void set(PreparedStatement ps, Manually_deliver_medicineDTO manually_deliver_medicineDTO, boolean isInsert) throws SQLException
	{
		int index = 0;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(manually_deliver_medicineDTO);
		if(isInsert)
		{
			ps.setObject(++index,manually_deliver_medicineDTO.iD);
		}
		ps.setObject(++index,manually_deliver_medicineDTO.deliveryDate);
		ps.setObject(++index,manually_deliver_medicineDTO.employeeType);
		ps.setObject(++index,manually_deliver_medicineDTO.employeeOrganogramId);
		ps.setObject(++index,manually_deliver_medicineDTO.employeeUserName);
		ps.setObject(++index,manually_deliver_medicineDTO.patientName);
		ps.setObject(++index,manually_deliver_medicineDTO.phoneNumber);
		ps.setObject(++index,manually_deliver_medicineDTO.doctorOrganogramId);
		ps.setObject(++index,manually_deliver_medicineDTO.doctorUserName);
		ps.setObject(++index,manually_deliver_medicineDTO.doctorName);
		ps.setObject(++index,manually_deliver_medicineDTO.insertedByUserId);
		ps.setObject(++index,manually_deliver_medicineDTO.insertedByOrganogramId);
		ps.setObject(++index,manually_deliver_medicineDTO.insertionDate);
		ps.setObject(++index,manually_deliver_medicineDTO.lastModifierUser);
		ps.setObject(++index,manually_deliver_medicineDTO.searchColumn);
		if(isInsert)
		{
			ps.setObject(++index,manually_deliver_medicineDTO.isDeleted);
		}
		ps.setObject(++index, lastModificationTime);
		
	}
	
	@Override
	public Manually_deliver_medicineDTO buildObjectFromResultSet(ResultSet rs)
	{
		try
		{
			Manually_deliver_medicineDTO manually_deliver_medicineDTO = new Manually_deliver_medicineDTO();
			int i = 0;
			manually_deliver_medicineDTO.iD = rs.getLong(columnNames[i++]);
			manually_deliver_medicineDTO.deliveryDate = rs.getLong(columnNames[i++]);
			manually_deliver_medicineDTO.employeeType = rs.getLong(columnNames[i++]);
			manually_deliver_medicineDTO.employeeOrganogramId = rs.getLong(columnNames[i++]);
			manually_deliver_medicineDTO.employeeUserName = rs.getString(columnNames[i++]);
			manually_deliver_medicineDTO.patientName = rs.getString(columnNames[i++]);
			manually_deliver_medicineDTO.phoneNumber = rs.getString(columnNames[i++]);
			manually_deliver_medicineDTO.doctorOrganogramId = rs.getLong(columnNames[i++]);
			manually_deliver_medicineDTO.doctorUserName = rs.getString(columnNames[i++]);
			manually_deliver_medicineDTO.doctorName = rs.getString(columnNames[i++]);
			manually_deliver_medicineDTO.insertedByUserId = rs.getLong(columnNames[i++]);
			manually_deliver_medicineDTO.insertedByOrganogramId = rs.getLong(columnNames[i++]);
			manually_deliver_medicineDTO.insertionDate = rs.getLong(columnNames[i++]);
			manually_deliver_medicineDTO.lastModifierUser = rs.getString(columnNames[i++]);
			manually_deliver_medicineDTO.searchColumn = rs.getString(columnNames[i++]);
			manually_deliver_medicineDTO.isDeleted = rs.getInt(columnNames[i++]);
			manually_deliver_medicineDTO.lastModificationTime = rs.getLong(columnNames[i++]);
			
			return manually_deliver_medicineDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
		
	public Manually_deliver_medicineDTO getDTOByID (long id)
	{
		Manually_deliver_medicineDTO manually_deliver_medicineDTO = null;
		try 
		{
			manually_deliver_medicineDTO = getDTOFromID(id);
			if(manually_deliver_medicineDTO != null)
			{
				MedicineDetailsDAO medicineDetailsDAO = MedicineDetailsDAO.getInstance();				
				List<MedicineDetailsDTO> medicineDetailsDTOList = (List<MedicineDetailsDTO>)medicineDetailsDAO.getDTOsByParent("manually_deliver_medicine_id", manually_deliver_medicineDTO.iD);
				manually_deliver_medicineDTO.medicineDetailsDTOList = medicineDetailsDTOList;
			}
		}
		catch (Exception e) 
		{
				// TODO Auto-generated catch block
				e.printStackTrace();
		}
		return manually_deliver_medicineDTO;
	}

	@Override
	public String getTableName() {
		return "manually_deliver_medicine";
	}

	@Override
	public Map<String, String> getSearchMap() {
		return searchMap;
	}

	public long add(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Manually_deliver_medicineDTO) commonDTO,addQuery,true);
	}

	public long update(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Manually_deliver_medicineDTO) commonDTO,updateQuery,false);
	}
	
	@Override
	public String getLastModifierUser(){
		return "last_modifier_user";
    }
				
}
	