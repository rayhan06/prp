package files_mapping;
import java.util.*; 
import util.*;


public class Files_mappingMAPS extends CommonMaps
{	
	public Files_mappingMAPS(String tableName)
	{
		
		java_allfield_type_map.put("file_id".toLowerCase(), "Long");
		java_allfield_type_map.put("table_name".toLowerCase(), "String");
		java_allfield_type_map.put("table_id".toLowerCase(), "Integer");

		java_anyfield_search_map.put(tableName + ".file_id".toLowerCase(), "Long");
		java_anyfield_search_map.put(tableName + ".table_name".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".table_id".toLowerCase(), "Integer");

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("fileId".toLowerCase(), "fileId".toLowerCase());
		java_DTO_map.put("tableName".toLowerCase(), "tableName".toLowerCase());
		java_DTO_map.put("tableId".toLowerCase(), "tableId".toLowerCase());

		java_SQL_map.put("file_id".toLowerCase(), "fileId".toLowerCase());
		java_SQL_map.put("table_name".toLowerCase(), "tableName".toLowerCase());
		java_SQL_map.put("table_id".toLowerCase(), "tableId".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("File Id".toLowerCase(), "fileId".toLowerCase());
		java_Text_map.put("Table Name".toLowerCase(), "tableName".toLowerCase());
		java_Text_map.put("Table Id".toLowerCase(), "tableId".toLowerCase());
			
	}

}