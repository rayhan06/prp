package files_mapping;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;


import org.apache.log4j.Logger;

import dbm.*;

import repository.RepositoryManager;

import util.*;
import pb.*;
import approval_module_map.*;

public class Files_mappingDAO  extends NavigationService3
{
	
	Logger logger = Logger.getLogger(getClass());

	
	public Files_mappingDAO(String tableName, String tempTableName, Approval_module_mapDTO approval_module_mapDTO)
	{
		super(tableName, tempTableName, approval_module_mapDTO);		
	}
	
	
	
	public long add(CommonDTO commonDTO, String tableName, TempTableDTO tempTableDTO) throws Exception
	{
		
		Files_mappingDTO files_mappingDTO = (Files_mappingDTO)commonDTO;
		
		Connection connection = null;
		PreparedStatement ps = null;

		long lastModificationTime = System.currentTimeMillis();	

		try{
			connection = DBMW.getInstance().getConnection();
			
			if(connection == null)
			{
				System.out.println("nullconn");
			}

			files_mappingDTO.iD = DBMW.getInstance().getNextSequenceId(tableName);

			String sql = "INSERT INTO " + tableName;
			
			sql += " (";
			sql += "ID";
			sql += ", ";
			sql += "file_id";
			sql += ", ";
			sql += "table_name";
			sql += ", ";
			sql += "table_id";
			if(tempTableDTO!=null)
			{
				sql += ", permanent_table_id, operation_type, approval_path_type, approval_order";
			}
			sql += ")";
			
			
            sql += " VALUES(";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			if(tempTableDTO!=null)
			{
				sql += ", " + tempTableDTO.permanent_table_id + ", " + tempTableDTO.operation_type + ", " + tempTableDTO.approval_path_type + ", " + tempTableDTO.approval_order;
			}
			sql += ")";
				
			printSql(sql);

			ps = connection.prepareStatement(sql);
			
			
			

			int index = 1;

			ps.setObject(index++,files_mappingDTO.iD);
			ps.setObject(index++,files_mappingDTO.fileId);
			ps.setObject(index++,files_mappingDTO.tableName);
			ps.setObject(index++,files_mappingDTO.tableId);
			ps.setObject(index++, lastModificationTime);
			
			System.out.println(ps);
			ps.execute();
			
			
			recordUpdateTime(connection, lastModificationTime, tableName);

		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if (ps != null) {
					ps.close();
				}
			} catch(Exception e){}
			try{
				if(connection != null){
					DBMW.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		return files_mappingDTO.iD;		
	}
		
	

	//need another getter for repository
	public CommonDTO getDTOByID (long ID, String tableName) throws Exception
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Files_mappingDTO files_mappingDTO = null;
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
			
            sql += " WHERE ID=" + ID;
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			if(rs.next()){
				files_mappingDTO = new Files_mappingDTO();

				files_mappingDTO.iD = rs.getLong("ID");
				files_mappingDTO.fileId = rs.getLong("file_id");
				files_mappingDTO.tableName = rs.getString("table_name");
				files_mappingDTO.tableId = rs.getInt("table_id");

			}			
			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DBMR.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
		return files_mappingDTO;
	}
	
	public long update(CommonDTO commonDTO, String tableName) throws Exception
	{		
		Files_mappingDTO files_mappingDTO = (Files_mappingDTO)commonDTO;
		
		Connection connection = null;
		PreparedStatement ps = null;

		long lastModificationTime = System.currentTimeMillis();	
		try{
			connection = DBMW.getInstance().getConnection();

			String sql = "UPDATE " + tableName;
			
			sql += " SET ";
			sql += "file_id=?";
			sql += ", ";
			sql += "table_name=?";
			sql += ", ";
			sql += "table_id=?";
			sql += ", lastModificationTime = "	+ lastModificationTime + "";
            sql += " WHERE ID = " + files_mappingDTO.iD;
				
			printSql(sql);

			ps = connection.prepareStatement(sql);
			
			

			int index = 1;
			
			ps.setObject(index++,files_mappingDTO.fileId);
			ps.setObject(index++,files_mappingDTO.tableName);
			ps.setObject(index++,files_mappingDTO.tableId);
			System.out.println(ps);
			ps.executeUpdate();
			


			
			recordUpdateTime(connection, lastModificationTime, tableName);

						
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if (ps != null) {
					ps.close();
				}
			} catch(Exception e){}
			try{
				if(connection != null){
					DBMW.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		return files_mappingDTO.iD;
	}
	
	public List<Files_mappingDTO> getDTOs(Collection recordIDs)
	{
		return getDTOs(recordIDs, tableName);
	}
	
	public List<Files_mappingDTO> getDTOs(Collection recordIDs, String tableName){
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Files_mappingDTO files_mappingDTO = null;
		List<Files_mappingDTO> files_mappingDTOList = new ArrayList<>();
		if(recordIDs.isEmpty()){
			return files_mappingDTOList;
		}
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
            
            sql += " WHERE ID IN ( ";

			for(int i = 0;i<recordIDs.size();i++){
				if(i!=0){
					sql+=",";
				}
				sql+=((ArrayList)recordIDs).get(i);
			}
			sql+=")  order by lastModificationTime desc";
			
			printSql(sql);
			
			logger.debug("sql " + sql);
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				files_mappingDTO = new Files_mappingDTO();
				files_mappingDTO.iD = rs.getLong("ID");
				files_mappingDTO.fileId = rs.getLong("file_id");
				files_mappingDTO.tableName = rs.getString("table_name");
				files_mappingDTO.tableId = rs.getInt("table_id");
				System.out.println("got this DTO: " + files_mappingDTO);
				
				files_mappingDTOList.add(files_mappingDTO);

			}			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DBMR.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
		return files_mappingDTOList;
	
	}
	
	

	
	
	
	//add repository
	public List<Files_mappingDTO> getAllFiles_mapping (boolean isFirstReload)
    {
		List<Files_mappingDTO> files_mappingDTOList = new ArrayList<>();

		String sql = "SELECT * FROM files_mapping";
		sql += " WHERE ";
	

		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by files_mapping.lastModificationTime desc";
		printSql(sql);
		
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;

		
		try{
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();
			rs = stmt.executeQuery(sql);
			

			while(rs.next()){
				Files_mappingDTO files_mappingDTO = new Files_mappingDTO();
				files_mappingDTO.iD = rs.getLong("ID");
				files_mappingDTO.fileId = rs.getLong("file_id");
				files_mappingDTO.tableName = rs.getString("table_name");
				files_mappingDTO.tableId = rs.getInt("table_id");
				
				files_mappingDTOList.add(files_mappingDTO);
			}			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			try{ 
				if (connection != null){ 
					DBMR.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}

		return files_mappingDTOList;
    }
	
	public List<Files_mappingDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset)
	{
		return getDTOs(p_searchCriteria, limit, offset, tableName, true, 0);
	}
	
	public List<Files_mappingDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, String tableName, boolean isPermanentTable, long userApprovalPathType)
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		List<Files_mappingDTO> files_mappingDTOList = new ArrayList<>();

		try{
			
			String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, tableName, isPermanentTable, userApprovalPathType);
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				Files_mappingDTO files_mappingDTO = new Files_mappingDTO();
				files_mappingDTO.iD = rs.getLong("ID");
				files_mappingDTO.fileId = rs.getLong("file_id");
				files_mappingDTO.tableName = rs.getString("table_name");
				files_mappingDTO.tableId = rs.getInt("table_id");
				
				files_mappingDTOList.add(files_mappingDTO);
			}					
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DBMR.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
		return files_mappingDTOList;
	
	}

		
	
	public String getSqlWithSearchCriteria(Hashtable p_searchCriteria, int limit, int offset, int category, String tableName, boolean isPermanentTable, long userApprovalPathType)
    {
		Files_mappingMAPS maps = new Files_mappingMAPS(tableName);
		String joinSQL = "";
		return getSqlWithSearchCriteria(p_searchCriteria, limit, offset, category, tableName, isPermanentTable, userApprovalPathType, maps, joinSQL);
    }			
}
	