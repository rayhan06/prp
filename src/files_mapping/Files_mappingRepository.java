package files_mapping;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class Files_mappingRepository implements Repository {
	Files_mappingDAO files_mappingDAO = null;
	
	public void setDAO(Files_mappingDAO files_mappingDAO)
	{
		this.files_mappingDAO = files_mappingDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Files_mappingRepository.class);
	Map<Long, Files_mappingDTO>mapOfFiles_mappingDTOToiD;
	Map<Long, Set<Files_mappingDTO> >mapOfFiles_mappingDTOTofileId;
	Map<String, Set<Files_mappingDTO> >mapOfFiles_mappingDTOTotableName;
	Map<Integer, Set<Files_mappingDTO> >mapOfFiles_mappingDTOTotableId;


	static Files_mappingRepository instance = null;  
	private Files_mappingRepository(){
		mapOfFiles_mappingDTOToiD = new ConcurrentHashMap<>();
		mapOfFiles_mappingDTOTofileId = new ConcurrentHashMap<>();
		mapOfFiles_mappingDTOTotableName = new ConcurrentHashMap<>();
		mapOfFiles_mappingDTOTotableId = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Files_mappingRepository getInstance(){
		if (instance == null){
			instance = new Files_mappingRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(files_mappingDAO == null)
		{
			return;
		}
		try {
			List<Files_mappingDTO> files_mappingDTOs = files_mappingDAO.getAllFiles_mapping(reloadAll);
			for(Files_mappingDTO files_mappingDTO : files_mappingDTOs) {
				Files_mappingDTO oldFiles_mappingDTO = getFiles_mappingDTOByID(files_mappingDTO.iD);
				if( oldFiles_mappingDTO != null ) {
					mapOfFiles_mappingDTOToiD.remove(oldFiles_mappingDTO.iD);
				
					if(mapOfFiles_mappingDTOTofileId.containsKey(oldFiles_mappingDTO.fileId)) {
						mapOfFiles_mappingDTOTofileId.get(oldFiles_mappingDTO.fileId).remove(oldFiles_mappingDTO);
					}
					if(mapOfFiles_mappingDTOTofileId.get(oldFiles_mappingDTO.fileId).isEmpty()) {
						mapOfFiles_mappingDTOTofileId.remove(oldFiles_mappingDTO.fileId);
					}
					
					if(mapOfFiles_mappingDTOTotableName.containsKey(oldFiles_mappingDTO.tableName)) {
						mapOfFiles_mappingDTOTotableName.get(oldFiles_mappingDTO.tableName).remove(oldFiles_mappingDTO);
					}
					if(mapOfFiles_mappingDTOTotableName.get(oldFiles_mappingDTO.tableName).isEmpty()) {
						mapOfFiles_mappingDTOTotableName.remove(oldFiles_mappingDTO.tableName);
					}
					
					if(mapOfFiles_mappingDTOTotableId.containsKey(oldFiles_mappingDTO.tableId)) {
						mapOfFiles_mappingDTOTotableId.get(oldFiles_mappingDTO.tableId).remove(oldFiles_mappingDTO);
					}
					if(mapOfFiles_mappingDTOTotableId.get(oldFiles_mappingDTO.tableId).isEmpty()) {
						mapOfFiles_mappingDTOTotableId.remove(oldFiles_mappingDTO.tableId);
					}
					
					
				}
				if(files_mappingDTO.isDeleted == 0) 
				{
					
					mapOfFiles_mappingDTOToiD.put(files_mappingDTO.iD, files_mappingDTO);
				
					if( ! mapOfFiles_mappingDTOTofileId.containsKey(files_mappingDTO.fileId)) {
						mapOfFiles_mappingDTOTofileId.put(files_mappingDTO.fileId, new HashSet<>());
					}
					mapOfFiles_mappingDTOTofileId.get(files_mappingDTO.fileId).add(files_mappingDTO);
					
					if( ! mapOfFiles_mappingDTOTotableName.containsKey(files_mappingDTO.tableName)) {
						mapOfFiles_mappingDTOTotableName.put(files_mappingDTO.tableName, new HashSet<>());
					}
					mapOfFiles_mappingDTOTotableName.get(files_mappingDTO.tableName).add(files_mappingDTO);
					
					if( ! mapOfFiles_mappingDTOTotableId.containsKey(files_mappingDTO.tableId)) {
						mapOfFiles_mappingDTOTotableId.put(files_mappingDTO.tableId, new HashSet<>());
					}
					mapOfFiles_mappingDTOTotableId.get(files_mappingDTO.tableId).add(files_mappingDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Files_mappingDTO> getFiles_mappingList() {
		List <Files_mappingDTO> files_mappings = new ArrayList<Files_mappingDTO>(this.mapOfFiles_mappingDTOToiD.values());
		return files_mappings;
	}
	
	
	public Files_mappingDTO getFiles_mappingDTOByID( long ID){
		return mapOfFiles_mappingDTOToiD.get(ID);
	}
	
	
	public List<Files_mappingDTO> getFiles_mappingDTOByfile_id(long file_id) {
		return new ArrayList<>( mapOfFiles_mappingDTOTofileId.getOrDefault(file_id,new HashSet<>()));
	}
	
	
	public List<Files_mappingDTO> getFiles_mappingDTOBytable_name(String table_name) {
		return new ArrayList<>( mapOfFiles_mappingDTOTotableName.getOrDefault(table_name,new HashSet<>()));
	}
	
	
	public List<Files_mappingDTO> getFiles_mappingDTOBytable_id(int table_id) {
		return new ArrayList<>( mapOfFiles_mappingDTOTotableId.getOrDefault(table_id,new HashSet<>()));
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "files_mapping";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


