DELETE FROM language_text WHERE menuID = 157101;
DELETE FROM language_text WHERE menuID = 157102;
DELETE FROM language_text WHERE menuID = 157103;
DELETE FROM language_text WHERE languageConstantPrefix = 'FILES_MAPPING_ADD';
DELETE FROM language_text WHERE languageConstantPrefix = 'FILES_MAPPING_EDIT';
DELETE FROM language_text WHERE languageConstantPrefix = 'FILES_MAPPING_SEARCH';
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('60400','157101','ID','????','FILES_MAPPING_ADD','ID');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('60401','157101','File Id','???? ????','FILES_MAPPING_ADD','FILEID');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('60402','157101','Table Name','????? ???','FILES_MAPPING_ADD','TABLENAME');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('60403','157101','Table Id','????? ????','FILES_MAPPING_ADD','TABLEID');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('60404','157102','ID','????','FILES_MAPPING_EDIT','ID');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('60405','157102','File Id','???? ????','FILES_MAPPING_EDIT','FILEID');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('60406','157102','Table Name','????? ???','FILES_MAPPING_EDIT','TABLENAME');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('60407','157102','Table Id','????? ????','FILES_MAPPING_EDIT','TABLEID');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('60408','157103','ID','????','FILES_MAPPING_SEARCH','ID');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('60409','157103','File Id','???? ????','FILES_MAPPING_SEARCH','FILEID');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('60410','157103','Table Name','????? ???','FILES_MAPPING_SEARCH','TABLENAME');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('60411','157103','Table Id','????? ????','FILES_MAPPING_SEARCH','TABLEID');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('60412','157101','FILES MAPPING ADD','???? ????? ?? ??? ????','FILES_MAPPING_ADD','FILES_MAPPING_ADD_FORMNAME');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('60413','157101','ADD','??? ????','FILES_MAPPING_ADD','FILES_MAPPING_ADD_BUTTON');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('60414','157101','SUBMIT','?????? ????','FILES_MAPPING_ADD','FILES_MAPPING_SUBMIT_BUTTON');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('60415','157101','CANCEL','????? ????','FILES_MAPPING_ADD','FILES_MAPPING_CANCEL_BUTTON');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('60416','157102','FILES MAPPING EDIT','???? ????? ?? ???????? ????','FILES_MAPPING_EDIT','FILES_MAPPING_EDIT_FORMNAME');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('60417','157102','SUBMIT','?????? ????','FILES_MAPPING_EDIT','FILES_MAPPING_SUBMIT_BUTTON');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('60418','157102','CANCEL','????? ????','FILES_MAPPING_EDIT','FILES_MAPPING_CANCEL_BUTTON');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('60419','157102','English','Bangla','FILES_MAPPING_EDIT','LANGUAGE');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('60420','157103','FILES MAPPING SEARCH','???? ????? ?? ??????','FILES_MAPPING_SEARCH','FILES_MAPPING_SEARCH_FORMNAME');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('60421','157103','SEARCH','??????','FILES_MAPPING_SEARCH','FILES_MAPPING_SEARCH_BUTTON');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('60422','157103','DELETE','????? ????','FILES_MAPPING_SEARCH','FILES_MAPPING_DELETE_BUTTON');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('60423','157103','EDIT','???????? ????','FILES_MAPPING_SEARCH','FILES_MAPPING_EDIT_BUTTON');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('60424','157103','CANCEL','????? ????','FILES_MAPPING_SEARCH','FILES_MAPPING_CANCEL_BUTTON');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('60425','157103','FILES MAPPING','???? ????? ??','FILES_MAPPING_SEARCH','ANYFIELD');
