package files_mapping;
import java.util.*; 
import util.*; 


public class Files_mappingDTO extends CommonDTO
{

	public long fileId = 0;
    public String tableName = "";
	public int tableId = 0;
	
	
    @Override
	public String toString() {
            return "$Files_mappingDTO[" +
            " iD = " + iD +
            " fileId = " + fileId +
            " tableName = " + tableName +
            " tableId = " + tableId +
            "]";
    }

}