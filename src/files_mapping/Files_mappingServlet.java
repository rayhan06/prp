//package files_mapping;
//
//import java.io.IOException;
//import java.io.*;
//
//
//import javax.servlet.RequestDispatcher;
//import javax.servlet.ServletException;
//import javax.servlet.annotation.MultipartConfig;
//import javax.servlet.annotation.WebServlet;
//
//
//import org.apache.log4j.Logger;
//
//import login.LoginDTO;
//import permission.MenuConstants;
//import role.PermissionRepository;
//
//
//import sessionmanager.SessionConstants;
//
//import user.UserDTO;
//import user.UserRepository;
//
//import util.RecordNavigationManager3;
//
//import java.util.*;
//import javax.servlet.http.*;
//import java.util.UUID;
//
//import files_mapping.Constants;
//import approval_module_map.*;
//
//
//import com.google.gson.Gson;
//
//import pb.*;
//import pbReport.*;
//import org.jsoup.Jsoup;
//import org.jsoup.safety.Whitelist;
//
///**
// * Servlet implementation class Files_mappingServlet
// */
//@WebServlet("/Files_mappingServlet")
//@MultipartConfig
//public class Files_mappingServlet extends HttpServlet 
//{
//	private static final long serialVersionUID = 1L;
//    public static Logger logger = Logger.getLogger(Files_mappingServlet.class);
//	Approval_module_mapDAO approval_module_mapDAO = new Approval_module_mapDAO();
//    Approval_module_mapDTO approval_module_mapDTO;
//    String tableName = "files_mapping";
//    String tempTableName = "files_mapping_temp";
//	Files_mappingDAO files_mappingDAO;
//    private Gson gson = new Gson();
//    
//    /**
//     * @see HttpServlet#HttpServlet()
//     */
//    public Files_mappingServlet() 
//	{
//        super();
//    	try
//    	{
//			approval_module_mapDTO = approval_module_mapDAO.getApproval_module_mapDTOByTableName("files_mapping");
//			files_mappingDAO = new Files_mappingDAO(tableName, tempTableName, approval_module_mapDTO);
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//    }   
//
//	/**
//	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
//	 */
//	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
//	{
//		System.out.println("In doget request = " + request);
//		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
//		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
//		
//
//		boolean isPermanentTable = true;
//		if(request.getParameter("isPermanentTable") != null)
//		{
//			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
//		}
//		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
//		try
//		{
//			String actionType = request.getParameter("actionType");
//			if(actionType.equals("getAddPage"))
//			{
//				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.FILES_MAPPING_ADD))
//				{
//					getAddPage(request, response);
//				}
//				else
//				{
//					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
//				}
//			}
//			else if(actionType.equals("getEditPage"))
//			{
//				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.FILES_MAPPING_UPDATE))
//				{
//					if(isPermanentTable)
//					{
//						getFiles_mapping(request, response, tableName);
//					}
//					else
//					{
//						getFiles_mapping(request, response, tempTableName);
//					}
//				}
//				else
//				{
//					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
//				}						
//			}
//			else if(actionType.equals("getURL"))
//			{
//				String URL = request.getParameter("URL");
//				System.out.println("URL = " + URL);
//				response.sendRedirect(URL);			
//			}
//			else if(actionType.equals("search"))
//			{
//				System.out.println("search requested");
//				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.FILES_MAPPING_SEARCH))
//				{
//					
//					if(isPermanentTable)
//					{
//						searchFiles_mapping(request, response, tableName, isPermanentTable);
//					}
//					else
//					{
//						searchFiles_mapping(request, response, tempTableName, isPermanentTable);
//					}
//				}			
//			}
//			else if(actionType.equals("getApprovalPage"))
//			{
//				System.out.println("getApprovalPage requested");
//				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.FILES_MAPPING_SEARCH))
//				{
//					searchFiles_mapping(request, response, tempTableName, false);
//				}
//				else
//				{
//					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
//				}				
//			}
//			else
//			{
//				request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
//			}
//		}
//		catch(Exception ex)
//		{
//			ex.printStackTrace();
//			logger.debug(ex);
//		}
//	}
//
//	private void getAddPage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
//	{
//		request.setAttribute("ID", -1L);
//		RequestDispatcher requestDispatcher = request.getRequestDispatcher("files_mapping/files_mappingEdit.jsp");
//		requestDispatcher.forward(request, response);
//	}
//	private String getFileName(final Part part) 
//	{
//	    final String partHeader = part.getHeader("content-disposition");
//	    System.out.println("Part Header = {0}" +  partHeader);
//	    for (String content : part.getHeader("content-disposition").split(";")) {
//	        if (content.trim().startsWith("filename")) {
//	            return content.substring(
//	                    content.indexOf('=') + 1).trim().replace("\"", "");
//	        }
//	    }
//	    return null;
//	}
//
//	
//	
//
//	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
//	{
//		// TODO Auto-generated method stub
//		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
//		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
//		
//
//		System.out.println("doPost");
//		boolean isPermanentTable = true;
//		if(request.getParameter("isPermanentTable") != null)
//		{
//			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
//		}
//		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
//		
//		try
//		{
//			String actionType = request.getParameter("actionType");
//			System.out.println("actionType = " + actionType);
//			if(actionType.equals("add"))
//			{
//				
//				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.FILES_MAPPING_ADD))
//				{
//					System.out.println("going to  addFiles_mapping ");
//					addFiles_mapping(request, response, true, userDTO, tableName, true);
//				}
//				else
//				{
//					System.out.println("Not going to  addFiles_mapping ");
//					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
//				}
//				
//			}
//			if(actionType.equals("approve"))
//			{
//				
//				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.FILES_MAPPING_ADD))
//				{					
//					approveFiles_mapping(request, response, true, userDTO);
//				}
//				else
//				{
//					System.out.println("Not going to  addFiles_mapping ");
//					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
//				}
//				
//			}
//			if(actionType.equals("getDTO"))
//			{
//				
//				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.FILES_MAPPING_ADD))
//				{
//					if(isPermanentTable)
//					{
//						getDTO(request, response, tableName);
//					}
//					else
//					{
//						getDTO(request, response, tempTableName);
//					}
//				}
//				else
//				{
//					System.out.println("Not going to  addFiles_mapping ");
//					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
//				}
//				
//			}
//			else if(actionType.equals("edit"))
//			{
//				
//				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.FILES_MAPPING_UPDATE))
//				{
//					if(isPermanentTable)
//					{
//						addFiles_mapping(request, response, false, userDTO, tableName, isPermanentTable);
//					}
//					else
//					{
//						addFiles_mapping(request, response, false, userDTO, tempTableName, isPermanentTable);
//					}
//				}
//				else
//				{
//					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
//				}
//			}
//			else if(actionType.equals("delete"))
//			{								
//				deleteFiles_mapping(request, response, userDTO, isPermanentTable);				
//			}	
//			else if(actionType.equals("search"))
//			{
//				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.FILES_MAPPING_SEARCH))
//				{
//					searchFiles_mapping(request, response, tableName, true);
//				}
//				else
//				{
//					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
//				}
//			}
//			else if(actionType.equals("getGeo"))
//			{
//				System.out.println("going to geoloc ");
//				request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
//			}
//			
//		}
//		catch(Exception ex)
//		{
//			ex.printStackTrace();
//			logger.debug(ex);
//		}
//	}
//	
//	private void getDTO(HttpServletRequest request, HttpServletResponse response, String tableName) 
//	{
//		try 
//		{
//			System.out.println("In getDTO");
//			Files_mappingDTO files_mappingDTO = (Files_mappingDTO)files_mappingDAO.getDTOByID(Long.parseLong(request.getParameter("ID")), tableName);
//			PrintWriter out = response.getWriter();
//			response.setContentType("application/json");
//			response.setCharacterEncoding("UTF-8");
//			
//			String encoded = this.gson.toJson(files_mappingDTO);
//			System.out.println("json encoded data = " + encoded);
//			out.print(encoded);
//			out.flush();
//		}
//		catch (NumberFormatException e) 
//		{
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		
//	}
//	private void approveFiles_mapping(HttpServletRequest request, HttpServletResponse response, boolean b, UserDTO userDTO) 
//	{
//		try
//		{
//			long id = Long.parseLong(request.getParameter("idToApprove"));
//			Files_mappingDTO files_mappingDTO = (Files_mappingDTO)files_mappingDAO.getDTOByID(id, tempTableName);
//			files_mappingDAO.manageWriteOperations(files_mappingDTO, SessionConstants.APPROVE, id, userDTO);
//		}
//		catch (Exception e) 
//		{
//			e.printStackTrace();
//		}
//		
//	}
//	private void addFiles_mapping(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, String tableName, boolean isPermanentTable) throws IOException 
//	{
//		// TODO Auto-generated method stub
//		try 
//		{
//			request.setAttribute("failureMessage", "");
//			System.out.println("%%%% addFiles_mapping");
//			String path = getServletContext().getRealPath("/img2/");
//			Files_mappingDTO files_mappingDTO;
//			String FileNamePrefix;
//			if(addFlag == true)
//			{
//				files_mappingDTO = new Files_mappingDTO();
//				FileNamePrefix = UUID.randomUUID().toString().substring(0, 10);
//			}
//			else
//			{
//				files_mappingDTO = (Files_mappingDTO)files_mappingDAO.getDTOByID(Long.parseLong(request.getParameter("identity")), tableName);
//				FileNamePrefix = request.getParameter("identity");
//			}
//			
//			String Value = "";
//			Value = request.getParameter("fileId");
//			if(Value != null)
//			{
//				Value = Jsoup.clean(Value,Whitelist.simpleText());
//			}
//			System.out.println("fileId = " + Value);
//			if(Value != null && !Value.equalsIgnoreCase(""))
//			{
//				files_mappingDTO.fileId = Long.parseLong(Value);
//			}
//			else
//			{
//				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//			}
//			Value = request.getParameter("tableName");
//			if(Value != null)
//			{
//				Value = Jsoup.clean(Value,Whitelist.simpleText());
//			}
//			System.out.println("tableName = " + Value);
//			if(Value != null && !Value.equalsIgnoreCase(""))
//			{
//				files_mappingDTO.tableName = (Value);
//			}
//			else
//			{
//				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//			}
//			Value = request.getParameter("tableId");
//			if(Value != null)
//			{
//				Value = Jsoup.clean(Value,Whitelist.simpleText());
//			}
//			System.out.println("tableId = " + Value);
//			if(Value != null && !Value.equalsIgnoreCase(""))
//			{
//				files_mappingDTO.tableId = Integer.parseInt(Value);
//			}
//			else
//			{
//				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//			}
//			
//			System.out.println("Done adding  addFiles_mapping dto = " + files_mappingDTO);
//			
//			if(addFlag == true)
//			{
//				files_mappingDAO.manageWriteOperations(files_mappingDTO, SessionConstants.INSERT, -1, userDTO);
//			}
//			else
//			{
//				if(isPermanentTable)
//				{
//					files_mappingDAO.manageWriteOperations(files_mappingDTO, SessionConstants.UPDATE, -1, userDTO);
//				}
//				else
//				{
//					files_mappingDAO.manageWriteOperations(files_mappingDTO, SessionConstants.VALIDATE, -1, userDTO);
//				}
//				
//			}
//			
//			
//			
//			
//			
//			
//			
//			
//			String inPlaceSubmit = (String)request.getParameter("inplacesubmit");
//			
//			if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
//			{
//				getFiles_mapping(request, response, tableName);
//			}
//			else
//			{
//				response.sendRedirect("Files_mappingServlet?actionType=search");
//			}
//					
//		}
//		catch (Exception e) 
//		{
//			e.printStackTrace();
//		}
//	}
//	
//	
//
//
//
//	
//	
//	
//
//	private void deleteFiles_mapping(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO, boolean deleteOrReject) throws IOException 
//	{				
//		try 
//		{
//			String[] IDsToDelete = request.getParameterValues("ID");
//			for(int i = 0; i < IDsToDelete.length; i ++)
//			{
//				long id = Long.parseLong(IDsToDelete[i]);
//				System.out.println("------ DELETING " + IDsToDelete[i]);
//				
//				if(deleteOrReject)
//				{
//					Files_mappingDTO files_mappingDTO = (Files_mappingDTO)files_mappingDAO.getDTOByID(id);
//					files_mappingDAO.manageWriteOperations(files_mappingDTO, SessionConstants.DELETE, id, userDTO);
//					response.sendRedirect("Files_mappingServlet?actionType=search");
//				}
//				else
//				{
//					Files_mappingDTO files_mappingDTO = (Files_mappingDTO)files_mappingDAO.getDTOByID(id, tempTableName);
//					files_mappingDAO.manageWriteOperations(files_mappingDTO, SessionConstants.REJECT, id, userDTO);
//					response.sendRedirect("Files_mappingServlet?actionType=getApprovalPage");
//				}
//			}			
//		}
//		catch (Exception ex) 
//		{
//			ex.printStackTrace();
//		}
//		
//	}
//
//	private void getFiles_mapping(HttpServletRequest request, HttpServletResponse response, String tableName) throws ServletException, IOException
//	{
//		System.out.println("in getFiles_mapping");
//		Files_mappingDTO files_mappingDTO = null;
//		try 
//		{
//			files_mappingDTO = (Files_mappingDTO)files_mappingDAO.getDTOByID(Long.parseLong(request.getParameter("ID")), tableName);
//			request.setAttribute("ID", files_mappingDTO.iD);
//			request.setAttribute("files_mappingDTO",files_mappingDTO);
//			request.setAttribute("files_mappingDAO",files_mappingDAO);
//			
//			String URL= "";
//			
//			String inPlaceEdit = (String)request.getParameter("inplaceedit");
//			String inPlaceSubmit = (String)request.getParameter("inplacesubmit");
//			String getBodyOnly = (String)request.getParameter("getBodyOnly");
//			
//			if(inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase(""))
//			{
//				URL = "files_mapping/files_mappingInPlaceEdit.jsp";	
//				request.setAttribute("inplaceedit","");				
//			}
//			else if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
//			{
//				URL = "files_mapping/files_mappingSearchRow.jsp";
//				request.setAttribute("inplacesubmit","");					
//			}
//			else
//			{
//				if(getBodyOnly  != null && !getBodyOnly.equalsIgnoreCase(""))
//				{
//					URL = "files_mapping/files_mappingEditBody.jsp?actionType=edit";
//				}
//				else
//				{
//					URL = "files_mapping/files_mappingEdit.jsp?actionType=edit";
//				}				
//			}
//			
//			RequestDispatcher rd = request.getRequestDispatcher(URL);
//			rd.forward(request, response);
//		}
//		catch (NumberFormatException e) 
//		{
//			e.printStackTrace();
//		}
//		catch (Exception e) 
//		{
//			e.printStackTrace();
//		}
//	}
//	
//	private void searchFiles_mapping(HttpServletRequest request, HttpServletResponse response, String tableName, boolean isPermanent) throws ServletException, IOException
//	{
//		System.out.println("in  searchFiles_mapping 1");
//		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
//		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
//		
//		
//		String ajax = (String)request.getParameter("ajax");
//		boolean hasAjax = false;
//		if(ajax != null && !ajax.equalsIgnoreCase(""))
//		{
//			hasAjax = true;
//		}
//		System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);
//		
//        RecordNavigationManager3 rnManager = new RecordNavigationManager3(
//			SessionConstants.NAV_FILES_MAPPING,
//			request,
//			files_mappingDAO,
//			SessionConstants.VIEW_FILES_MAPPING,
//			SessionConstants.SEARCH_FILES_MAPPING,
//			tableName,
//			isPermanent,
//			userDTO.approvalPathID);
//        try
//        {
//			System.out.println("trying to dojob");
//            rnManager.doJob(loginDTO);
//        }
//        catch(Exception e)
//        {
//			System.out.println("failed to dojob" + e);
//        }
//
//		request.setAttribute("files_mappingDAO",files_mappingDAO);
//        RequestDispatcher rd;
//        if(hasAjax == false)
//        {
//        	System.out.println("Going to files_mapping/files_mappingSearch.jsp");
//        	rd = request.getRequestDispatcher("files_mapping/files_mappingSearch.jsp");
//        }
//        else
//        {
//        	System.out.println("Going to files_mapping/files_mappingSearchForm.jsp");
//        	rd = request.getRequestDispatcher("files_mapping/files_mappingSearchForm.jsp");
//        }
//		rd.forward(request, response);
//	}
//	
//}
//
