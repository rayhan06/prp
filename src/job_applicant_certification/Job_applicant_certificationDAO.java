package job_applicant_certification;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;

import common.ConnectionAndStatementUtil;
import sessionmanager.SessionConstants;
import java.sql.SQLException;


import org.apache.log4j.Logger;

import dbm.*;

import repository.RepositoryManager;

import util.*;
import pb.*;
import user.UserDTO;
import vm_vehicle_office_assignment.Vm_vehicle_office_assignmentDTO;

public class Job_applicant_certificationDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());
	
	
	public Job_applicant_certificationDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";
//		commonMaps = new Job_applicant_certificationMAPS(tableName);
	}

	@Override
	public Collection getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO) {
		return null;
	}

	public Job_applicant_certificationDAO()
	{
		this("job_applicant_certification");		
	}
	
//	public void get(Job_applicant_certificationDTO job_applicant_certificationDTO, ResultSet rs) throws SQLException
//	{
//
//	}

	public Job_applicant_certificationDTO build(ResultSet rs)
	{
		try
		{
			Job_applicant_certificationDTO job_applicant_certificationDTO = new Job_applicant_certificationDTO();
			job_applicant_certificationDTO.iD = rs.getLong("ID");
			job_applicant_certificationDTO.jobApplicantId = rs.getLong("job_applicant_id");
			job_applicant_certificationDTO.jobId = rs.getLong("job_id");
			job_applicant_certificationDTO.certificationName = rs.getString("certification_name");
			job_applicant_certificationDTO.issuingOrganization = rs.getString("issuing_organization");
			job_applicant_certificationDTO.issuingInstituteAddress = rs.getString("issuing_institute_address");
			job_applicant_certificationDTO.validFrom = rs.getString("valid_from");
			job_applicant_certificationDTO.hasExpiryCat = rs.getInt("has_expiry_cat");
			job_applicant_certificationDTO.validUpto = rs.getString("valid_upto");
			job_applicant_certificationDTO.credentialId = rs.getString("credential_id");
			job_applicant_certificationDTO.filesDropzone = rs.getLong("files_dropzone");
			job_applicant_certificationDTO.insertedByUserId = rs.getLong("inserted_by_user_id");
			job_applicant_certificationDTO.insertionDate = rs.getLong("insertion_date");
			job_applicant_certificationDTO.modifiedBy = rs.getString("modified_by");
			job_applicant_certificationDTO.isDeleted = rs.getInt("isDeleted");
			job_applicant_certificationDTO.lastModificationTime = rs.getLong("lastModificationTime");
			return job_applicant_certificationDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}

		
	

	//need another getter for repository
	public Job_applicant_certificationDTO getDTOByID (long ID)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID = ? ";
		Job_applicant_certificationDTO job_applicant_certificationDTO =
				ConnectionAndStatementUtil.getT(sql, Arrays.asList(ID), this::build);
		return job_applicant_certificationDTO;
	}
	
	
	public List<Job_applicant_certificationDTO> getDTOs(Collection recordIDs){
		if(recordIDs.isEmpty()){
			return new ArrayList<>();
		}
		String sql = "SELECT * FROM " + tableName + " WHERE ID IN ( ";
		for(int i = 0;i<recordIDs.size();i++){
			if(i!=0){
				sql+=",";
			}
			sql+= " ? ";
		}
		sql+=")  order by lastModificationTime desc";
		printSql(sql);
		return ConnectionAndStatementUtil.getListOfT(sql, Arrays.asList(recordIDs.toArray()),this::build);
	
	}
	
	//add repository
	public List<Job_applicant_certificationDTO> getAllJob_applicant_certification (boolean isFirstReload)
    {

		String sql = "SELECT * FROM job_applicant_certification";
		sql += " WHERE ";
	

		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by job_applicant_certification.lastModificationTime desc";
		printSql(sql);

		return ConnectionAndStatementUtil.getListOfT(sql,this::build);
    }

	public List<Job_applicant_certificationDTO> getDTOByJobApplicantID (long ID)
	{

		String sql = "SELECT * FROM job_applicant_certification";

		sql += " WHERE job_applicant_id= ? ";

		sql+=" AND isDeleted =  0";

		sql += " order by job_applicant_certification.lastModificationTime desc";
		printSql(sql);
		return ConnectionAndStatementUtil.getListOfT(sql, Arrays.asList(ID),this::build);


	}

				
}
	