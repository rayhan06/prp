package job_applicant_certification;

import files.FilesDTO;
import util.CommonDTO;

import java.util.ArrayList;
import java.util.List;


public class Job_applicant_certificationSummaryDTO extends CommonDTO
{
    public long jobId = 0;
	public long jobApplicantId = 0;
    public String certificationName = "";
    public String issuingOrganization = "";
    public String issuingInstituteAddress = "";
    public String validFrom = "";
	public int hasExpiryCat = 0;
    public String validUpto = "";
    public String credentialId = "";
	public long filesDropzone = 0;
	public long insertedByUserId = 0;
	public long insertionDate = 0;
    public String modifiedBy = "";

    public List<FilesDTO> filesDropzoneFiles = new ArrayList();
	
	
    @Override
	public String toString() {
            return "$Job_applicant_certificationDTO[" +
            " iD = " + iD +
            " jobApplicantId = " + jobApplicantId +
            " certificationName = " + certificationName +
            " issuingOrganization = " + issuingOrganization +
            " issuingInstituteAddress = " + issuingInstituteAddress +
            " validFrom = " + validFrom +
            " hasExpiryCat = " + hasExpiryCat +
            " validUpto = " + validUpto +
            " credentialId = " + credentialId +
            " filesDropzone = " + filesDropzone +
            " insertedByUserId = " + insertedByUserId +
            " insertionDate = " + insertionDate +
            " modifiedBy = " + modifiedBy +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}