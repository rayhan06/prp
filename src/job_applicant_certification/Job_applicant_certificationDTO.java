package job_applicant_certification;
import java.util.*; 
import util.*; 


public class Job_applicant_certificationDTO extends CommonDTO
{
    public long jobId = 0;
	public long jobApplicantId = 0;
    public String certificationName = "";
    public String issuingOrganization = "";
    public String issuingInstituteAddress = "";
    public String validFrom = "";
	public int hasExpiryCat = 0;
    public String validUpto = "";
    public String credentialId = "";
	public long filesDropzone = -1;
	public long insertedByUserId = 0;
	public long insertionDate = 0;
    public String modifiedBy = "";
	
	
    @Override
	public String toString() {
            return "$Job_applicant_certificationDTO[" +
            " iD = " + iD +
            " jobApplicantId = " + jobApplicantId +
            " certificationName = " + certificationName +
            " issuingOrganization = " + issuingOrganization +
            " issuingInstituteAddress = " + issuingInstituteAddress +
            " validFrom = " + validFrom +
            " hasExpiryCat = " + hasExpiryCat +
            " validUpto = " + validUpto +
            " credentialId = " + credentialId +
            " filesDropzone = " + filesDropzone +
            " insertedByUserId = " + insertedByUserId +
            " insertionDate = " + insertionDate +
            " modifiedBy = " + modifiedBy +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}