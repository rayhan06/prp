package job_applicant_certification;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class Job_applicant_certificationRepository implements Repository {
	Job_applicant_certificationDAO job_applicant_certificationDAO = null;
	
	public void setDAO(Job_applicant_certificationDAO job_applicant_certificationDAO)
	{
		this.job_applicant_certificationDAO = job_applicant_certificationDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Job_applicant_certificationRepository.class);
	Map<Long, Job_applicant_certificationDTO>mapOfJob_applicant_certificationDTOToiD;
	Map<Long, Set<Job_applicant_certificationDTO> >mapOfJob_applicant_certificationDTOTojobApplicantId;
	Map<String, Set<Job_applicant_certificationDTO> >mapOfJob_applicant_certificationDTOTocertificationName;
	Map<String, Set<Job_applicant_certificationDTO> >mapOfJob_applicant_certificationDTOToissuingOrganization;
	Map<String, Set<Job_applicant_certificationDTO> >mapOfJob_applicant_certificationDTOToissuingInstituteAddress;
	Map<String, Set<Job_applicant_certificationDTO> >mapOfJob_applicant_certificationDTOTovalidFrom;
	Map<Integer, Set<Job_applicant_certificationDTO> >mapOfJob_applicant_certificationDTOTohasExpiryCat;
	Map<String, Set<Job_applicant_certificationDTO> >mapOfJob_applicant_certificationDTOTovalidUpto;
	Map<String, Set<Job_applicant_certificationDTO> >mapOfJob_applicant_certificationDTOTocredentialId;
	Map<Long, Set<Job_applicant_certificationDTO> >mapOfJob_applicant_certificationDTOTofilesDropzone;
	Map<Long, Set<Job_applicant_certificationDTO> >mapOfJob_applicant_certificationDTOToinsertedByUserId;
	Map<Long, Set<Job_applicant_certificationDTO> >mapOfJob_applicant_certificationDTOToinsertionDate;
	Map<String, Set<Job_applicant_certificationDTO> >mapOfJob_applicant_certificationDTOTomodifiedBy;
	Map<Long, Set<Job_applicant_certificationDTO> >mapOfJob_applicant_certificationDTOTolastModificationTime;


	static Job_applicant_certificationRepository instance = null;  
	private Job_applicant_certificationRepository(){
		mapOfJob_applicant_certificationDTOToiD = new ConcurrentHashMap<>();
		mapOfJob_applicant_certificationDTOTojobApplicantId = new ConcurrentHashMap<>();
		mapOfJob_applicant_certificationDTOTocertificationName = new ConcurrentHashMap<>();
		mapOfJob_applicant_certificationDTOToissuingOrganization = new ConcurrentHashMap<>();
		mapOfJob_applicant_certificationDTOToissuingInstituteAddress = new ConcurrentHashMap<>();
		mapOfJob_applicant_certificationDTOTovalidFrom = new ConcurrentHashMap<>();
		mapOfJob_applicant_certificationDTOTohasExpiryCat = new ConcurrentHashMap<>();
		mapOfJob_applicant_certificationDTOTovalidUpto = new ConcurrentHashMap<>();
		mapOfJob_applicant_certificationDTOTocredentialId = new ConcurrentHashMap<>();
		mapOfJob_applicant_certificationDTOTofilesDropzone = new ConcurrentHashMap<>();
		mapOfJob_applicant_certificationDTOToinsertedByUserId = new ConcurrentHashMap<>();
		mapOfJob_applicant_certificationDTOToinsertionDate = new ConcurrentHashMap<>();
		mapOfJob_applicant_certificationDTOTomodifiedBy = new ConcurrentHashMap<>();
		mapOfJob_applicant_certificationDTOTolastModificationTime = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Job_applicant_certificationRepository getInstance(){
		if (instance == null){
			instance = new Job_applicant_certificationRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(job_applicant_certificationDAO == null)
		{
			return;
		}
		try {
			List<Job_applicant_certificationDTO> job_applicant_certificationDTOs = job_applicant_certificationDAO.getAllJob_applicant_certification(reloadAll);
			for(Job_applicant_certificationDTO job_applicant_certificationDTO : job_applicant_certificationDTOs) {
				Job_applicant_certificationDTO oldJob_applicant_certificationDTO = getJob_applicant_certificationDTOByID(job_applicant_certificationDTO.iD);
				if( oldJob_applicant_certificationDTO != null ) {
					mapOfJob_applicant_certificationDTOToiD.remove(oldJob_applicant_certificationDTO.iD);
				
					if(mapOfJob_applicant_certificationDTOTojobApplicantId.containsKey(oldJob_applicant_certificationDTO.jobApplicantId)) {
						mapOfJob_applicant_certificationDTOTojobApplicantId.get(oldJob_applicant_certificationDTO.jobApplicantId).remove(oldJob_applicant_certificationDTO);
					}
					if(mapOfJob_applicant_certificationDTOTojobApplicantId.get(oldJob_applicant_certificationDTO.jobApplicantId).isEmpty()) {
						mapOfJob_applicant_certificationDTOTojobApplicantId.remove(oldJob_applicant_certificationDTO.jobApplicantId);
					}
					
					if(mapOfJob_applicant_certificationDTOTocertificationName.containsKey(oldJob_applicant_certificationDTO.certificationName)) {
						mapOfJob_applicant_certificationDTOTocertificationName.get(oldJob_applicant_certificationDTO.certificationName).remove(oldJob_applicant_certificationDTO);
					}
					if(mapOfJob_applicant_certificationDTOTocertificationName.get(oldJob_applicant_certificationDTO.certificationName).isEmpty()) {
						mapOfJob_applicant_certificationDTOTocertificationName.remove(oldJob_applicant_certificationDTO.certificationName);
					}
					
					if(mapOfJob_applicant_certificationDTOToissuingOrganization.containsKey(oldJob_applicant_certificationDTO.issuingOrganization)) {
						mapOfJob_applicant_certificationDTOToissuingOrganization.get(oldJob_applicant_certificationDTO.issuingOrganization).remove(oldJob_applicant_certificationDTO);
					}
					if(mapOfJob_applicant_certificationDTOToissuingOrganization.get(oldJob_applicant_certificationDTO.issuingOrganization).isEmpty()) {
						mapOfJob_applicant_certificationDTOToissuingOrganization.remove(oldJob_applicant_certificationDTO.issuingOrganization);
					}
					
					if(mapOfJob_applicant_certificationDTOToissuingInstituteAddress.containsKey(oldJob_applicant_certificationDTO.issuingInstituteAddress)) {
						mapOfJob_applicant_certificationDTOToissuingInstituteAddress.get(oldJob_applicant_certificationDTO.issuingInstituteAddress).remove(oldJob_applicant_certificationDTO);
					}
					if(mapOfJob_applicant_certificationDTOToissuingInstituteAddress.get(oldJob_applicant_certificationDTO.issuingInstituteAddress).isEmpty()) {
						mapOfJob_applicant_certificationDTOToissuingInstituteAddress.remove(oldJob_applicant_certificationDTO.issuingInstituteAddress);
					}
					
					if(mapOfJob_applicant_certificationDTOTovalidFrom.containsKey(oldJob_applicant_certificationDTO.validFrom)) {
						mapOfJob_applicant_certificationDTOTovalidFrom.get(oldJob_applicant_certificationDTO.validFrom).remove(oldJob_applicant_certificationDTO);
					}
					if(mapOfJob_applicant_certificationDTOTovalidFrom.get(oldJob_applicant_certificationDTO.validFrom).isEmpty()) {
						mapOfJob_applicant_certificationDTOTovalidFrom.remove(oldJob_applicant_certificationDTO.validFrom);
					}
					
					if(mapOfJob_applicant_certificationDTOTohasExpiryCat.containsKey(oldJob_applicant_certificationDTO.hasExpiryCat)) {
						mapOfJob_applicant_certificationDTOTohasExpiryCat.get(oldJob_applicant_certificationDTO.hasExpiryCat).remove(oldJob_applicant_certificationDTO);
					}
					if(mapOfJob_applicant_certificationDTOTohasExpiryCat.get(oldJob_applicant_certificationDTO.hasExpiryCat).isEmpty()) {
						mapOfJob_applicant_certificationDTOTohasExpiryCat.remove(oldJob_applicant_certificationDTO.hasExpiryCat);
					}
					
					if(mapOfJob_applicant_certificationDTOTovalidUpto.containsKey(oldJob_applicant_certificationDTO.validUpto)) {
						mapOfJob_applicant_certificationDTOTovalidUpto.get(oldJob_applicant_certificationDTO.validUpto).remove(oldJob_applicant_certificationDTO);
					}
					if(mapOfJob_applicant_certificationDTOTovalidUpto.get(oldJob_applicant_certificationDTO.validUpto).isEmpty()) {
						mapOfJob_applicant_certificationDTOTovalidUpto.remove(oldJob_applicant_certificationDTO.validUpto);
					}
					
					if(mapOfJob_applicant_certificationDTOTocredentialId.containsKey(oldJob_applicant_certificationDTO.credentialId)) {
						mapOfJob_applicant_certificationDTOTocredentialId.get(oldJob_applicant_certificationDTO.credentialId).remove(oldJob_applicant_certificationDTO);
					}
					if(mapOfJob_applicant_certificationDTOTocredentialId.get(oldJob_applicant_certificationDTO.credentialId).isEmpty()) {
						mapOfJob_applicant_certificationDTOTocredentialId.remove(oldJob_applicant_certificationDTO.credentialId);
					}
					
					if(mapOfJob_applicant_certificationDTOTofilesDropzone.containsKey(oldJob_applicant_certificationDTO.filesDropzone)) {
						mapOfJob_applicant_certificationDTOTofilesDropzone.get(oldJob_applicant_certificationDTO.filesDropzone).remove(oldJob_applicant_certificationDTO);
					}
					if(mapOfJob_applicant_certificationDTOTofilesDropzone.get(oldJob_applicant_certificationDTO.filesDropzone).isEmpty()) {
						mapOfJob_applicant_certificationDTOTofilesDropzone.remove(oldJob_applicant_certificationDTO.filesDropzone);
					}
					
					if(mapOfJob_applicant_certificationDTOToinsertedByUserId.containsKey(oldJob_applicant_certificationDTO.insertedByUserId)) {
						mapOfJob_applicant_certificationDTOToinsertedByUserId.get(oldJob_applicant_certificationDTO.insertedByUserId).remove(oldJob_applicant_certificationDTO);
					}
					if(mapOfJob_applicant_certificationDTOToinsertedByUserId.get(oldJob_applicant_certificationDTO.insertedByUserId).isEmpty()) {
						mapOfJob_applicant_certificationDTOToinsertedByUserId.remove(oldJob_applicant_certificationDTO.insertedByUserId);
					}
					
					if(mapOfJob_applicant_certificationDTOToinsertionDate.containsKey(oldJob_applicant_certificationDTO.insertionDate)) {
						mapOfJob_applicant_certificationDTOToinsertionDate.get(oldJob_applicant_certificationDTO.insertionDate).remove(oldJob_applicant_certificationDTO);
					}
					if(mapOfJob_applicant_certificationDTOToinsertionDate.get(oldJob_applicant_certificationDTO.insertionDate).isEmpty()) {
						mapOfJob_applicant_certificationDTOToinsertionDate.remove(oldJob_applicant_certificationDTO.insertionDate);
					}
					
					if(mapOfJob_applicant_certificationDTOTomodifiedBy.containsKey(oldJob_applicant_certificationDTO.modifiedBy)) {
						mapOfJob_applicant_certificationDTOTomodifiedBy.get(oldJob_applicant_certificationDTO.modifiedBy).remove(oldJob_applicant_certificationDTO);
					}
					if(mapOfJob_applicant_certificationDTOTomodifiedBy.get(oldJob_applicant_certificationDTO.modifiedBy).isEmpty()) {
						mapOfJob_applicant_certificationDTOTomodifiedBy.remove(oldJob_applicant_certificationDTO.modifiedBy);
					}
					
					if(mapOfJob_applicant_certificationDTOTolastModificationTime.containsKey(oldJob_applicant_certificationDTO.lastModificationTime)) {
						mapOfJob_applicant_certificationDTOTolastModificationTime.get(oldJob_applicant_certificationDTO.lastModificationTime).remove(oldJob_applicant_certificationDTO);
					}
					if(mapOfJob_applicant_certificationDTOTolastModificationTime.get(oldJob_applicant_certificationDTO.lastModificationTime).isEmpty()) {
						mapOfJob_applicant_certificationDTOTolastModificationTime.remove(oldJob_applicant_certificationDTO.lastModificationTime);
					}
					
					
				}
				if(job_applicant_certificationDTO.isDeleted == 0) 
				{
					
					mapOfJob_applicant_certificationDTOToiD.put(job_applicant_certificationDTO.iD, job_applicant_certificationDTO);
				
					if( ! mapOfJob_applicant_certificationDTOTojobApplicantId.containsKey(job_applicant_certificationDTO.jobApplicantId)) {
						mapOfJob_applicant_certificationDTOTojobApplicantId.put(job_applicant_certificationDTO.jobApplicantId, new HashSet<>());
					}
					mapOfJob_applicant_certificationDTOTojobApplicantId.get(job_applicant_certificationDTO.jobApplicantId).add(job_applicant_certificationDTO);
					
					if( ! mapOfJob_applicant_certificationDTOTocertificationName.containsKey(job_applicant_certificationDTO.certificationName)) {
						mapOfJob_applicant_certificationDTOTocertificationName.put(job_applicant_certificationDTO.certificationName, new HashSet<>());
					}
					mapOfJob_applicant_certificationDTOTocertificationName.get(job_applicant_certificationDTO.certificationName).add(job_applicant_certificationDTO);
					
					if( ! mapOfJob_applicant_certificationDTOToissuingOrganization.containsKey(job_applicant_certificationDTO.issuingOrganization)) {
						mapOfJob_applicant_certificationDTOToissuingOrganization.put(job_applicant_certificationDTO.issuingOrganization, new HashSet<>());
					}
					mapOfJob_applicant_certificationDTOToissuingOrganization.get(job_applicant_certificationDTO.issuingOrganization).add(job_applicant_certificationDTO);
					
					if( ! mapOfJob_applicant_certificationDTOToissuingInstituteAddress.containsKey(job_applicant_certificationDTO.issuingInstituteAddress)) {
						mapOfJob_applicant_certificationDTOToissuingInstituteAddress.put(job_applicant_certificationDTO.issuingInstituteAddress, new HashSet<>());
					}
					mapOfJob_applicant_certificationDTOToissuingInstituteAddress.get(job_applicant_certificationDTO.issuingInstituteAddress).add(job_applicant_certificationDTO);
					
					if( ! mapOfJob_applicant_certificationDTOTovalidFrom.containsKey(job_applicant_certificationDTO.validFrom)) {
						mapOfJob_applicant_certificationDTOTovalidFrom.put(job_applicant_certificationDTO.validFrom, new HashSet<>());
					}
					mapOfJob_applicant_certificationDTOTovalidFrom.get(job_applicant_certificationDTO.validFrom).add(job_applicant_certificationDTO);
					
					if( ! mapOfJob_applicant_certificationDTOTohasExpiryCat.containsKey(job_applicant_certificationDTO.hasExpiryCat)) {
						mapOfJob_applicant_certificationDTOTohasExpiryCat.put(job_applicant_certificationDTO.hasExpiryCat, new HashSet<>());
					}
					mapOfJob_applicant_certificationDTOTohasExpiryCat.get(job_applicant_certificationDTO.hasExpiryCat).add(job_applicant_certificationDTO);
					
					if( ! mapOfJob_applicant_certificationDTOTovalidUpto.containsKey(job_applicant_certificationDTO.validUpto)) {
						mapOfJob_applicant_certificationDTOTovalidUpto.put(job_applicant_certificationDTO.validUpto, new HashSet<>());
					}
					mapOfJob_applicant_certificationDTOTovalidUpto.get(job_applicant_certificationDTO.validUpto).add(job_applicant_certificationDTO);
					
					if( ! mapOfJob_applicant_certificationDTOTocredentialId.containsKey(job_applicant_certificationDTO.credentialId)) {
						mapOfJob_applicant_certificationDTOTocredentialId.put(job_applicant_certificationDTO.credentialId, new HashSet<>());
					}
					mapOfJob_applicant_certificationDTOTocredentialId.get(job_applicant_certificationDTO.credentialId).add(job_applicant_certificationDTO);
					
					if( ! mapOfJob_applicant_certificationDTOTofilesDropzone.containsKey(job_applicant_certificationDTO.filesDropzone)) {
						mapOfJob_applicant_certificationDTOTofilesDropzone.put(job_applicant_certificationDTO.filesDropzone, new HashSet<>());
					}
					mapOfJob_applicant_certificationDTOTofilesDropzone.get(job_applicant_certificationDTO.filesDropzone).add(job_applicant_certificationDTO);
					
					if( ! mapOfJob_applicant_certificationDTOToinsertedByUserId.containsKey(job_applicant_certificationDTO.insertedByUserId)) {
						mapOfJob_applicant_certificationDTOToinsertedByUserId.put(job_applicant_certificationDTO.insertedByUserId, new HashSet<>());
					}
					mapOfJob_applicant_certificationDTOToinsertedByUserId.get(job_applicant_certificationDTO.insertedByUserId).add(job_applicant_certificationDTO);
					
					if( ! mapOfJob_applicant_certificationDTOToinsertionDate.containsKey(job_applicant_certificationDTO.insertionDate)) {
						mapOfJob_applicant_certificationDTOToinsertionDate.put(job_applicant_certificationDTO.insertionDate, new HashSet<>());
					}
					mapOfJob_applicant_certificationDTOToinsertionDate.get(job_applicant_certificationDTO.insertionDate).add(job_applicant_certificationDTO);
					
					if( ! mapOfJob_applicant_certificationDTOTomodifiedBy.containsKey(job_applicant_certificationDTO.modifiedBy)) {
						mapOfJob_applicant_certificationDTOTomodifiedBy.put(job_applicant_certificationDTO.modifiedBy, new HashSet<>());
					}
					mapOfJob_applicant_certificationDTOTomodifiedBy.get(job_applicant_certificationDTO.modifiedBy).add(job_applicant_certificationDTO);
					
					if( ! mapOfJob_applicant_certificationDTOTolastModificationTime.containsKey(job_applicant_certificationDTO.lastModificationTime)) {
						mapOfJob_applicant_certificationDTOTolastModificationTime.put(job_applicant_certificationDTO.lastModificationTime, new HashSet<>());
					}
					mapOfJob_applicant_certificationDTOTolastModificationTime.get(job_applicant_certificationDTO.lastModificationTime).add(job_applicant_certificationDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Job_applicant_certificationDTO> getJob_applicant_certificationList() {
		List <Job_applicant_certificationDTO> job_applicant_certifications = new ArrayList<Job_applicant_certificationDTO>(this.mapOfJob_applicant_certificationDTOToiD.values());
		return job_applicant_certifications;
	}
	
	
	public Job_applicant_certificationDTO getJob_applicant_certificationDTOByID( long ID){
		return mapOfJob_applicant_certificationDTOToiD.get(ID);
	}
	
	
	public List<Job_applicant_certificationDTO> getJob_applicant_certificationDTOByjob_applicant_id(long job_applicant_id) {
		return new ArrayList<>( mapOfJob_applicant_certificationDTOTojobApplicantId.getOrDefault(job_applicant_id,new HashSet<>()));
	}
	
	
	public List<Job_applicant_certificationDTO> getJob_applicant_certificationDTOBycertification_name(String certification_name) {
		return new ArrayList<>( mapOfJob_applicant_certificationDTOTocertificationName.getOrDefault(certification_name,new HashSet<>()));
	}
	
	
	public List<Job_applicant_certificationDTO> getJob_applicant_certificationDTOByissuing_organization(String issuing_organization) {
		return new ArrayList<>( mapOfJob_applicant_certificationDTOToissuingOrganization.getOrDefault(issuing_organization,new HashSet<>()));
	}
	
	
	public List<Job_applicant_certificationDTO> getJob_applicant_certificationDTOByissuing_institute_address(String issuing_institute_address) {
		return new ArrayList<>( mapOfJob_applicant_certificationDTOToissuingInstituteAddress.getOrDefault(issuing_institute_address,new HashSet<>()));
	}
	
	
	public List<Job_applicant_certificationDTO> getJob_applicant_certificationDTOByvalid_from(String valid_from) {
		return new ArrayList<>( mapOfJob_applicant_certificationDTOTovalidFrom.getOrDefault(valid_from,new HashSet<>()));
	}
	
	
	public List<Job_applicant_certificationDTO> getJob_applicant_certificationDTOByhas_expiry_cat(int has_expiry_cat) {
		return new ArrayList<>( mapOfJob_applicant_certificationDTOTohasExpiryCat.getOrDefault(has_expiry_cat,new HashSet<>()));
	}
	
	
	public List<Job_applicant_certificationDTO> getJob_applicant_certificationDTOByvalid_upto(String valid_upto) {
		return new ArrayList<>( mapOfJob_applicant_certificationDTOTovalidUpto.getOrDefault(valid_upto,new HashSet<>()));
	}
	
	
	public List<Job_applicant_certificationDTO> getJob_applicant_certificationDTOBycredential_id(String credential_id) {
		return new ArrayList<>( mapOfJob_applicant_certificationDTOTocredentialId.getOrDefault(credential_id,new HashSet<>()));
	}
	
	
	public List<Job_applicant_certificationDTO> getJob_applicant_certificationDTOByfiles_dropzone(long files_dropzone) {
		return new ArrayList<>( mapOfJob_applicant_certificationDTOTofilesDropzone.getOrDefault(files_dropzone,new HashSet<>()));
	}
	
	
	public List<Job_applicant_certificationDTO> getJob_applicant_certificationDTOByinserted_by_user_id(long inserted_by_user_id) {
		return new ArrayList<>( mapOfJob_applicant_certificationDTOToinsertedByUserId.getOrDefault(inserted_by_user_id,new HashSet<>()));
	}
	
	
	public List<Job_applicant_certificationDTO> getJob_applicant_certificationDTOByinsertion_date(long insertion_date) {
		return new ArrayList<>( mapOfJob_applicant_certificationDTOToinsertionDate.getOrDefault(insertion_date,new HashSet<>()));
	}
	
	
	public List<Job_applicant_certificationDTO> getJob_applicant_certificationDTOBymodified_by(String modified_by) {
		return new ArrayList<>( mapOfJob_applicant_certificationDTOTomodifiedBy.getOrDefault(modified_by,new HashSet<>()));
	}
	
	
	public List<Job_applicant_certificationDTO> getJob_applicant_certificationDTOBylastModificationTime(long lastModificationTime) {
		return new ArrayList<>( mapOfJob_applicant_certificationDTOTolastModificationTime.getOrDefault(lastModificationTime,new HashSet<>()));
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "job_applicant_certification";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


