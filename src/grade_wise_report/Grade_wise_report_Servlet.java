package grade_wise_report;


import language.LC;
import login.LoginDTO;
import pbReport.PBReportUtils;
import pbReport.ReportCommonService;
import pbReport.ReportService;
import permission.MenuConstants;
import role.PermissionRepository;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.CommonConstant;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;


@WebServlet("/Grade_wise_report_Servlet")
public class Grade_wise_report_Servlet extends HttpServlet implements ReportCommonService {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private final Map<String, String[]> stringMap = new HashMap<>();

    private static final String sql = "employee_records er\n" +
            "left join employee_offices eo on eo.employee_record_id=er.id ";

    private static final Set<String> searchParam = new HashSet<>(Arrays.asList(

            "officeUnitIds", "gradeCat"
    ));

    private String[][] Criteria;
    private String[][] Display;


    public Grade_wise_report_Servlet() {
        stringMap.put("officeUnitIds", new String[]{"criteria", "eo", "office_unit_id", "IN", "AND", "String", "", "", "", "officeUnitIdList"
                , "officeUnitIds", String.valueOf(LC.EMPLOYEE_OFFICE_REPORT_WHERE_OFFICEUNITID), "office_units", null, "true", "1"});
        stringMap.put("gradeCat", new String[]{"criteria", "er", "gradeCat", "=", "AND", "int", "", "", "any",
                "gradeCat", "gradeCat", String.valueOf(LC.EMPLOYEE_RECORDS_ADD_GENDER), "category", "job_grade", "true", "2"});
        stringMap.put("isDeleted_1", new String[]{"criteria", "er", "isDeleted", "=", "AND", "long", "", "", "0", "isDeleted", null, null, null, null, null, null});
        stringMap.put("isDeleted", new String[]{"criteria", "eo", "isDeleted", "=", "AND", "long", "", "", "0", "isDeleted", null, null, null, null, null, null});
    }


    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);

        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }
        boolean isLangEng = userDTO.languageID == CommonConstant.Language_ID_English;
        Set<String> inputs = PBReportUtils.prepareInputSets(searchParam, request);
        inputs.add("isDeleted_1");
        inputs.add("isDeleted");
        Display = new String[][]{
                {"display", "er", "gradeCat", "grade", isLangEng ? "Grade" : "গ্রেড"},
                {"display", "", "count(*) ", "text", isLangEng ? "Total" : "সর্বমোট"},
                {"display", "", " sum(IF(TIMESTAMPDIFF(YEAR, FROM_UNIXTIME(date_of_birth * .001), CURDATE()) <= 34, 1, 0)) ", "text", isLangEng ? "Upto 34 Years" : "৩৪ বছর পর্যন্ত"},
                {"display", "", " sum(IF(TIMESTAMPDIFF(YEAR, FROM_UNIXTIME(date_of_birth * .001), CURDATE()) <= 34, 0, 1)) ", "text", isLangEng ? "Above 34 Years" : "৩৪ বছরের বেশি"},
                {"display", "", " SUM(IF(er.gender_cat = 1, 1, 0))   ", "text", isLangEng ? "Male" : "পুরুষ"},
                {"display", "", " SUM(IF(er.gender_cat = 2, 1, 0))   ", "text", isLangEng ? "Female" : "মহিলা"},
                {"display", "", "SUM(IF(er.religion = 2, 1, 0))  ", "text", isLangEng ? "Muslim" : "মুসলিম"},
                {"display", "", "SUM(IF(er.religion = 3, 1, 0))  ", "text", isLangEng ? "Hindu" : "হিন্দু"},
                {"display", "", "SUM(IF(er.religion = 4, 1, 0))   ", "text", isLangEng ? "Christianity" : "খ্রিস্টান"},
                {"display", "", "SUM(IF(er.religion = 5, 1, 0))  ", "text", isLangEng ? "Buddhisn" : "বৌদ্ধ"},
                {"display", "", "SUM(IF(er.religion = 6, 1, 0)) ", "text", isLangEng ? "Atheism" : "নাস্তিকতা"},
                {"display", "", "SUM(IF(er.religion = 7, 1, 0))    ", "text", isLangEng ? "Other" : "অন্যান্য"},
                {"display", "", "SUM(IF(er.job_quota = 1, 1, 0))    ", "text", isLangEng ? "Minorities" : "সংখ্যালঘু"},
                {"display", "", "SUM(IF(er.job_quota = 5, 1, 0))   ", "text", isLangEng ? "Disabilities" : "প্রতিবন্ধী"}
        };
        Criteria = PBReportUtils.prepareCriteria(inputs, stringMap);

        if (inputs.contains("officeUnitIds")) {
            for (String[] arr : Criteria) {
                if ("officeUnitIdList".equals(arr[9])) {
                    arr[8] = getOfficeIdsFromOfficeUnitIds(request).stream()
                            .map(String::valueOf)
                            .collect(Collectors.joining(","));
                }
            }
        }
        request.setAttribute("dontUseOutDatedModal", true);
        if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.GRADE_WISE_REPORT_DETAILS)) {
            reportGenerate(request, response);
        }
    }


    @Override
    public String[][] getCriteria() {
        return Criteria;
    }

    @Override
    public String[][] getDisplay() {
        return Display;
    }

    @Override
    public String getSQL() {
        return sql;
    }

    @Override
    public int getLCForReportName() {
        return LC.GRADE_WISE_REPORT_OTHER_GRADE_WISE_REPORT;
    }

    @Override
    public String getFileName() {
        return "grade_wise_report";
    }

    @Override
    public String getTableName() {
        return "grade_wise_report";
    }

    @Override
    public String getGroupBy() {
        return "er.gradeCat";
    }

    @Override
    public Integer getTotalEntryCount(String tableJoinSql, HttpServletRequest request, String[][] criteria, String GroupBy) throws Exception {
        if (request.getAttribute("TotalListSize") != null) {
            return Integer.parseInt(request.getAttribute("TotalListSize").toString());
        }
        return ReportService.getTotalCount2(getSQL(), request, getCriteria(), getGroupBy());
    }
}
