package bill_approval_history;

import bill_approval_mapping.Bill_approval_mappingDAO;
import bill_approval_mapping.Bill_approval_mappingDTO;
import card_info.AlreadyApprovedException;
import common.CommonDAOService;
import common.ConnectionAndStatementUtil;
import common.CustomException;
import employee_offices.EmployeeOfficeDTO;
import employee_offices.EmployeeOfficeRepository;
import employee_records.Employee_recordsDTO;
import employee_records.Employee_recordsRepository;
import food_bill.Food_billDTO;
import org.apache.log4j.Logger;
import pb.Utils;
import user.UserDTO;
import util.CommonDTO;
import util.HttpRequestUtils;
import util.LockManager;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static bill_approval_history.BillApprovalStatus.APPROVED;
import static bill_approval_history.BillApprovalStatus.APPROVED_BY_OTHER;
import static bill_approval_history.Bill_approval_historyDTO.findUsersHistory;
import static java.util.stream.Collectors.*;

@SuppressWarnings({"Duplicates"})
public class Bill_approval_historyDAO implements CommonDAOService<Bill_approval_historyDTO> {
    private static final Logger logger = Logger.getLogger(Bill_approval_historyDAO.class);

    private static final String addSqlQuery =
            "INSERT INTO {tableName} (table_name,approvable_dto_id,level,approval_status,approval_time,name_en,name_bn,"
                    .concat("office_unit_id,office_name_en,office_name_bn,organogram_id,org_name_en,org_name_bn,signature,")
                    .concat("modified_by,lastModificationTime,inserted_by,insertion_time,isDeleted,ID) ")
                    .concat("VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

    private static final String updateSqlQuery =
            "UPDATE {tableName} SET table_name=?,approvable_dto_id=?,level=?,approval_status=?,approval_time=?,name_en=?,name_bn=?,"
                    .concat("office_unit_id=?,office_name_en=?,office_name_bn=?,organogram_id=?,org_name_en=?,org_name_bn=?,signature=?,")
                    .concat("modified_by=?,lastModificationTime=? WHERE ID=?");

    private static final Map<String, String> searchMap = new HashMap<>();

    private Bill_approval_historyDAO() {

    }

    private static class LazyLoader {
        static final Bill_approval_historyDAO INSTANCE = new Bill_approval_historyDAO();
    }

    public static Bill_approval_historyDAO getInstance() {
        return LazyLoader.INSTANCE;
    }

    @Override
    public String getTableName() {
        return "bill_approval_history";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    @Override
    public void set(PreparedStatement ps, Bill_approval_historyDTO dto, boolean isInsert) throws SQLException {
        int index = 0;
        ps.setString(++index, dto.tableName);
        ps.setLong(++index, dto.approvableDtoId);
        ps.setInt(++index, dto.level);
        ps.setInt(++index, dto.billApprovalStatus.getValue());
        ps.setLong(++index, dto.approvalTime);
        ps.setString(++index, dto.nameEn);
        ps.setString(++index, dto.nameBn);
        ps.setLong(++index, dto.officeUnitId);
        ps.setString(++index, dto.officeNameEn);
        ps.setString(++index, dto.officeNameBn);
        ps.setLong(++index, dto.organogramId);
        ps.setString(++index, dto.orgNameEn);
        ps.setString(++index, dto.orgNameBn);
        ps.setBytes(++index, dto.signature);
        ps.setLong(++index, dto.modifiedBy);
        ps.setLong(++index, dto.lastModificationTime);
        if (isInsert) {
            ps.setLong(++index, dto.insertedBy);
            ps.setLong(++index, dto.insertionTime);
            ps.setInt(++index, 0 /* isDeleted */);
        }
        ps.setLong(++index, dto.iD);
    }

    @Override
    public Bill_approval_historyDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Bill_approval_historyDTO dto = new Bill_approval_historyDTO();
            dto.iD = rs.getLong("ID");
            dto.tableName = rs.getString("table_name");
            dto.approvableDtoId = rs.getLong("approvable_dto_id");
            dto.level = rs.getInt("level");
            dto.billApprovalStatus = BillApprovalStatus.getFromValue(rs.getInt("approval_status"));
            dto.approvalTime = rs.getLong("approval_time");
            dto.nameEn = rs.getString("name_en");
            dto.nameBn = rs.getString("name_bn");
            dto.officeUnitId = rs.getLong("office_unit_id");
            dto.officeNameEn = rs.getString("office_name_en");
            dto.officeNameBn = rs.getString("office_name_bn");
            dto.organogramId = rs.getLong("organogram_id");
            dto.orgNameEn = rs.getString("org_name_en");
            dto.orgNameBn = rs.getString("org_name_bn");
            dto.signature = rs.getBytes("signature");
            dto.modifiedBy = rs.getLong("modified_by");
            dto.lastModificationTime = rs.getLong("lastModificationTime");
            dto.insertedBy = rs.getLong("inserted_by");
            dto.insertionTime = rs.getLong("insertion_time");
            dto.isDeleted = rs.getInt("isDeleted");
            return dto;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    @Override
    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Bill_approval_historyDTO) commonDTO, addSqlQuery, true);
    }

    public void addAll(List<Bill_approval_historyDTO> approvalHistoryDTOs) throws Exception {
        Utils.handleTransaction(() -> {
            // IMPORTANT not done with stream to throw exception above
            for (Bill_approval_historyDTO approvalHistoryDTO : approvalHistoryDTOs) {
                add(approvalHistoryDTO);
            }
        });
    }

    @Override
    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Bill_approval_historyDTO) commonDTO, updateSqlQuery, false);
    }

    private static final String findAllHistoryOfApprovableDtoSql =
            "SELECT * FROM bill_approval_history WHERE table_name='%s' AND approvable_dto_id=%d AND isDeleted=0";

    public List<Bill_approval_historyDTO> findAllLevelHistory(String tableName, Long approvableDtoId) {
        return ConnectionAndStatementUtil.getListOfT(
                String.format(findAllHistoryOfApprovableDtoSql, tableName, approvableDtoId),
                this::buildObjectFromResultSet
        );
    }

    private static final String deleteAllLevelHistorySql =
            "UPDATE bill_approval_history SET isDeleted=1,modified_by=%d,lastModificationTime=%d " +
            "WHERE table_name='%s' AND approvable_dto_id=%d AND isDeleted=0";

    public void deleteAllLevelHistory(ApprovableDTO approvableDTO, long modifiedBy, long modificationTime) {
        String sql = String.format(
                deleteAllLevelHistorySql,
                modifiedBy, modificationTime,
                approvableDTO.getTableName(),
                approvableDTO.getId()
        );
        logger.debug(sql);
        ConnectionAndStatementUtil.getWriteConnectionAndStatement(model -> {
            try {
                model.getStatement().executeUpdate(sql);
                recordUpdateTime(model.getConnection(), getTableName(), modificationTime);
                return true;
            } catch (SQLException ex) {
                ex.printStackTrace();
                return false;
            }
        });
    }

    private static final String findAllApprovedHistoryDtoSql =
            "SELECT * FROM bill_approval_history WHERE table_name='%s' AND approvable_dto_id=%d AND approval_status=%d AND isDeleted=0";

    public List<Bill_approval_historyDTO> findAllApprovedHistory(String tableName, Long approvableDtoId) {
        return ConnectionAndStatementUtil.getListOfT(
                String.format(findAllApprovedHistoryDtoSql, tableName, approvableDtoId, APPROVED.getValue()),
                this::buildObjectFromResultSet
        );
    }

    private static final String findByLevelSql =
            "SELECT * FROM bill_approval_history WHERE table_name='%s' AND approvable_dto_id=%d AND level=%d AND isDeleted=0";

    public List<Bill_approval_historyDTO> findByLevel(String tableName, Long approvableDtoId, Integer level) {
        return ConnectionAndStatementUtil.getListOfT(
                String.format(findByLevelSql, tableName, approvableDtoId, level),
                this::buildObjectFromResultSet
        );
    }

    private Object getLockOnApprovableDTO(ApprovableDTO approvableDTO, String method) {
        return LockManager.getLock(String.format(
                "bill_approval_history-%s-%d-%s",
                approvableDTO.getTableName(), approvableDTO.getId(), method
        ));
    }

    private List<Bill_approval_historyDTO> addAndGetAddedDTOsOfALevel(Integer level,
                                                                      List<Bill_approval_mappingDTO> billApprovalMappingDTOs,
                                                                      ApprovableDTO approvableDTO) throws Exception {
        synchronized (getLockOnApprovableDTO(approvableDTO, "addAndGetAddedDTOsOfALevel")) {
            List<Bill_approval_historyDTO> insertedHistoryList = findByLevel(
                    approvableDTO.getTableName(),
                    approvableDTO.getId(),
                    level
            );
            if (!insertedHistoryList.isEmpty()) {
                return insertedHistoryList;
            }
            List<Bill_approval_historyDTO> historyList = new ArrayList<>();
            Utils.handleTransaction(() -> {
                // IMPORTANT not done with stream to throw exception above
                for (Bill_approval_mappingDTO approvalMappingDTO : billApprovalMappingDTOs) {
                    historyList.addAll(addAndGetAddedDTOs(approvalMappingDTO, approvableDTO));
                }
            });
            return historyList;
        }
    }

    private List<Bill_approval_historyDTO> getDTOsOfALevel(Integer level,
                                                           List<Bill_approval_mappingDTO> billApprovalMappingDTOs,
                                                           ApprovableDTO approvableDTO) {
        synchronized (getLockOnApprovableDTO(approvableDTO, "getDTOsOfALevel")) {
            List<Bill_approval_historyDTO> insertedHistoryList = findByLevel(
                    approvableDTO.getTableName(),
                    approvableDTO.getId(),
                    level
            );
            if (!insertedHistoryList.isEmpty()) {
                return insertedHistoryList;
            }
            return billApprovalMappingDTOs
                    .stream()
                    .flatMap(approvalMappingDTO -> Bill_approval_historyDTO.newDtoListFrom(approvalMappingDTO, approvableDTO).stream())
                    .collect(Collectors.toList());
        }
    }

    private List<Bill_approval_historyDTO> addAndGetAddedDTOs(Bill_approval_mappingDTO billApprovalMappingDTO,
                                                              ApprovableDTO approvableDTO) throws Exception {
        List<Bill_approval_historyDTO> historyList = Bill_approval_historyDTO.newDtoListFrom(billApprovalMappingDTO, approvableDTO);
        // IMPORTANT not done with stream to throw exception above
        for (Bill_approval_historyDTO history : historyList) {
            add(history);
        }
        return historyList;
    }

    public List<Bill_approval_historyDTO> addAndGetAddedDTOs(List<Bill_approval_mappingDTO> billApprovalMappingDTOs,
                                                             ApprovableDTO approvableDTO) throws Exception {
        Map<Integer, List<Bill_approval_mappingDTO>> billApprovalMappingDTOsByLevel =
                billApprovalMappingDTOs.stream()
                                       .collect(Collectors.groupingBy(approvalMappingDTO -> approvalMappingDTO.level));

        List<Bill_approval_historyDTO> historyList = new ArrayList<>();
        Utils.handleTransaction(() -> {
            // IMPORTANT not done with stream to throw exception above
            for (Map.Entry<Integer, List<Bill_approval_mappingDTO>> levelMappingDTOs : billApprovalMappingDTOsByLevel.entrySet()) {
                List<Bill_approval_historyDTO> approvalHistoryDTOs = addAndGetAddedDTOsOfALevel(
                        levelMappingDTOs.getKey(),
                        levelMappingDTOs.getValue(),
                        approvableDTO
                );
                historyList.addAll(approvalHistoryDTOs);
            }
        });
        return historyList;
    }

    public List<Bill_approval_historyDTO> getDTOsFromMappingDTOs(List<Bill_approval_mappingDTO> billApprovalMappingDTOs,
                                                                 ApprovableDTO approvableDTO) throws Exception {
        Map<Integer, List<Bill_approval_mappingDTO>> billApprovalMappingDTOsByLevel =
                billApprovalMappingDTOs.stream()
                                       .collect(Collectors.groupingBy(approvalMappingDTO -> approvalMappingDTO.level));
        return billApprovalMappingDTOsByLevel
                .entrySet()
                .stream()
                .flatMap(levelMappingDTOs -> getDTOsOfALevel(levelMappingDTOs.getKey(), levelMappingDTOs.getValue(), approvableDTO).stream())
                .collect(Collectors.toList());
    }

    public List<Bill_approval_historyDTO> getDTOsWithCircuitBreaker(
            List<Bill_approval_historyDTO> approvalHistoryDTOs,
            Predicate<List<Bill_approval_historyDTO>> circuitBreaker,
            Predicate<List<Bill_approval_historyDTO>> takeConditionAfterCircuitBreaker
    ) {
        TreeMap<Integer, List<Bill_approval_historyDTO>> approvalHistoryBySortedLevel =
                approvalHistoryDTOs.stream()
                                   .collect(Collectors.groupingBy(
                                           approvalHistory -> approvalHistory.level,
                                           TreeMap::new,
                                           Collectors.toList()
                                   ));
        List<Bill_approval_historyDTO> modifiedApprovalHistoryList = new ArrayList<>();
        boolean circuitBroken = false;
        for (Map.Entry<Integer, List<Bill_approval_historyDTO>> levelApprovalHistoryListEntry : approvalHistoryBySortedLevel.entrySet()) {
            List<Bill_approval_historyDTO> approvalHistoryOfLevel = levelApprovalHistoryListEntry.getValue();
            if (!circuitBroken) {
                modifiedApprovalHistoryList.addAll(approvalHistoryOfLevel);
                circuitBroken = circuitBreaker.test(approvalHistoryOfLevel);
            } else {
                if (takeConditionAfterCircuitBreaker.test(approvalHistoryOfLevel)) {
                    modifiedApprovalHistoryList.addAll(approvalHistoryOfLevel);
                }
            }
        }
        return modifiedApprovalHistoryList;
    }

    @FunctionalInterface
    private interface MoveToLevelMethod {
        void moveToLevel(ApprovableDTO approvableDTO, Integer level, BillApprovalResponse outApprovalResponse) throws Exception;
    }

    private void moveToLevelWithAddNextLevelApprover(ApprovableDTO approvableDTO,
                                                     Integer level,
                                                     BillApprovalResponse outApprovalResponse) throws Exception {
        List<Bill_approval_mappingDTO> approvalMappings =
                Bill_approval_mappingDAO.getInstance()
                                        .findByTaskTypeAndLevel(approvableDTO.getTaskTypeId(), level);
        List<Bill_approval_historyDTO> approvalHistoryDTOs = addAndGetAddedDTOs(approvalMappings, approvableDTO);
        outApprovalResponse.hasNextApproval = !(approvalHistoryDTOs == null || approvalHistoryDTOs.isEmpty());
        outApprovalResponse.nextLevel = level;
        outApprovalResponse.nextApprovalHistoryDTOs = outApprovalResponse.hasNextApproval ? approvalHistoryDTOs : null;
    }

    private void moveToLevelWitAlreadyAddedApprover(ApprovableDTO approvableDTO,
                                                    Integer level,
                                                    BillApprovalResponse outApprovalResponse) {
        List<Bill_approval_historyDTO> approvalHistoryDTOs = findByLevel(
                approvableDTO.getTableName(), approvableDTO.getId(), level
        );
        outApprovalResponse.hasNextApproval = !(approvalHistoryDTOs == null || approvalHistoryDTOs.isEmpty());
        outApprovalResponse.nextLevel = level;
        outApprovalResponse.nextApprovalHistoryDTOs = outApprovalResponse.hasNextApproval ? approvalHistoryDTOs : null;
    }

    private void checkApprovalHistorySignature(Bill_approval_historyDTO approvalHistoryDTO, boolean isLangEng) {
        if (approvalHistoryDTO.hasValidSignature()) {
            return;
        }
        final CustomException notAllowedToApproveException = new CustomException(
                isLangEng ? "You are not allowed to approve at this level" : "এই স্তরে আপনার অনুমোদন করার অনুমতি নেই"
        );
        EmployeeOfficeDTO employeeOfficeDTO = EmployeeOfficeRepository.getInstance().getByOfficeUnitOrganogramId(approvalHistoryDTO.organogramId);
        if (employeeOfficeDTO == null) {
            throw notAllowedToApproveException;
        }
        Employee_recordsDTO employeeRecords = Employee_recordsRepository.getInstance().getById(employeeOfficeDTO.employeeRecordId);
        if (employeeRecords == null) {
            throw notAllowedToApproveException;
        }
        approvalHistoryDTO.nameEn = employeeRecords.nameEng;
        approvalHistoryDTO.nameBn = employeeRecords.nameBng;
        approvalHistoryDTO.signature = employeeRecords.signature;
        if (!approvalHistoryDTO.hasValidSignature()) {
            throw new CustomException(
                    isLangEng ? "Please upload your signature!"
                              : "আপনার স্বাক্ষর আপলোড করুন!"
            );
        }
    }

    public BillApprovalResponse approveWitAlreadyAddedApprover(
            ApprovableDTO approvableDTO,
            UserDTO userDTO
    ) throws Exception {
        return approve(approvableDTO, userDTO, this::moveToLevelWitAlreadyAddedApprover);
    }

    @SuppressWarnings({"unused"})
    public BillApprovalResponse approveWitAddNextLevelApprover(
            ApprovableDTO approvableDTO,
            UserDTO userDTO
    ) throws Exception {
        return approve(approvableDTO, userDTO, this::moveToLevelWithAddNextLevelApprover);
    }

    private BillApprovalResponse approve(
            ApprovableDTO approvableDTO,
            UserDTO userDTO,
            MoveToLevelMethod moveToLevelMethod
    ) throws Exception {
        synchronized (getLockOnApprovableDTO(approvableDTO, "approve")) {
            List<Bill_approval_historyDTO> historyOfLevel = findByLevel(
                    approvableDTO.getTableName(), approvableDTO.getId(), approvableDTO.getLevel()
            );
            boolean isLangEng = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
            boolean isAlreadyApproved = historyOfLevel.stream().anyMatch(Bill_approval_historyDTO::isApprovalDone);
            if (isAlreadyApproved) {
                throw new AlreadyApprovedException(isLangEng ? "Already Approved" : "ইতোমধ্যে অনুমোদিত");
            }
            Bill_approval_historyDTO usersApprovalHistory =
                    findUsersHistory(historyOfLevel, userDTO).orElseThrow(() -> new CustomException(
                            isLangEng ? "You are not allowed to approve at this level"
                                      : "এই স্তরে আপনার অনুমোদন করার অনুমতি নেই"
                    ));
            checkApprovalHistorySignature(usersApprovalHistory, isLangEng);
            long modificationTime = System.currentTimeMillis();
            BillApprovalResponse approvalResponse = new BillApprovalResponse();
            Utils.handleTransaction(() -> {
                for (Bill_approval_historyDTO approvalHistoryDTO : historyOfLevel) {
                    if (approvalHistoryDTO.iD != usersApprovalHistory.iD) {
                        approvalHistoryDTO.billApprovalStatus = APPROVED_BY_OTHER;
                    } else {
                        approvalHistoryDTO.billApprovalStatus = APPROVED;
                    }
                    approvalHistoryDTO.modifiedBy = userDTO.ID;
                    approvalHistoryDTO.lastModificationTime = approvalHistoryDTO.approvalTime = modificationTime;
                    update(approvalHistoryDTO);
                }
                moveToLevelMethod.moveToLevel(approvableDTO, approvableDTO.getLevel() + 1, approvalResponse);
            });
            return approvalResponse;
        }
    }

    private Bill_approval_historyDTO flattenApprovalHistoryListForUser(List<Bill_approval_historyDTO> approvalHistoryDTOs,
                                                                       UserDTO userDTO) {
        if (approvalHistoryDTOs == null || approvalHistoryDTOs.isEmpty()) {
            return null;
        }
        if (approvalHistoryDTOs.size() == 1) {
            return approvalHistoryDTOs.get(0);
        }
        Bill_approval_historyDTO userHistory = null;
        for (Bill_approval_historyDTO approvalHistoryDTO : approvalHistoryDTOs) {
            if (approvalHistoryDTO.isApprovalStatus(BillApprovalStatus.APPROVED)) {
                return approvalHistoryDTO;
            }
            if (approvalHistoryDTO.isThisUsersHistory(userDTO)) {
                userHistory = approvalHistoryDTO;
            }
        }
        return userHistory;
    }


    public TreeMap<Integer, Bill_approval_historyDTO> getApprovalHistoryBySortedLevel(ApprovableDTO approvableDTO, UserDTO userDTO) {
        return Bill_approval_historyDAO
                .getInstance()
                .findAllLevelHistory(approvableDTO.getTableName(), approvableDTO.getId())
                .stream()
                .collect(groupingBy(
                        approvalHistoryDTO -> approvalHistoryDTO.level,
                        TreeMap::new,
                        collectingAndThen(
                                toList(),
                                list -> flattenApprovalHistoryListForUser(list, userDTO)
                        )
                ));
    }
}
