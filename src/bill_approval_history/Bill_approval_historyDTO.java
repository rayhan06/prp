package bill_approval_history;

import bill_approval_mapping.Bill_approval_mappingDTO;
import employee_offices.EmployeeOfficeDTO;
import employee_offices.EmployeeOfficeRepository;
import employee_records.Employee_recordsDTO;
import employee_records.Employee_recordsRepository;
import office_unit_organograms.OfficeUnitOrganograms;
import office_unit_organograms.OfficeUnitOrganogramsRepository;
import office_units.Office_unitsDTO;
import office_units.Office_unitsRepository;
import sessionmanager.SessionConstants;
import user.UserDTO;
import util.CommonDTO;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

public class Bill_approval_historyDTO extends CommonDTO {
    public String tableName;
    public Long approvableDtoId;
    public Integer level;
    public BillApprovalStatus billApprovalStatus;
    public Long approvalTime;
    public String nameEn = "";
    public String nameBn = "";
    public Long officeUnitId;
    public String officeNameEn = "";
    public String officeNameBn = "";
    public Long organogramId;
    public String orgNameEn = "";
    public String orgNameBn = "";
    public byte[] signature;

    public long modifiedBy = -1;
    public long insertedBy = -1;
    public long insertionTime = SessionConstants.MIN_DATE;

    public boolean isApprovalDone() {
        return billApprovalStatus != BillApprovalStatus.PENDING;
    }

    public boolean isApprovalStatus(BillApprovalStatus billApprovalStatus) {
        return this.billApprovalStatus == billApprovalStatus;
    }

    public boolean isThisUsersHistory(UserDTO userDTO) {
        if (userDTO == null) {
            return false;
        }
        return Objects.equals(organogramId, userDTO.organogramID);
    }

    public static boolean isThisUsersHistory(Bill_approval_historyDTO approvalHistoryDTO, UserDTO userDTO) {
        return approvalHistoryDTO != null && approvalHistoryDTO.isThisUsersHistory(userDTO);
    }

    public static Bill_approval_historyDTO newDtoFrom(Bill_approval_mappingDTO billApprovalMappingDTO,
                                                      ApprovableDTO approvableDTO,
                                                      EmployeeOfficeDTO employeeOfficeDTO) {
        Bill_approval_historyDTO approvalHistoryDTO = new Bill_approval_historyDTO();

        approvalHistoryDTO.tableName = approvableDTO.getTableName();
        approvalHistoryDTO.approvableDtoId = approvableDTO.getId();
        approvalHistoryDTO.level = billApprovalMappingDTO.level;
        approvalHistoryDTO.billApprovalStatus = BillApprovalStatus.PENDING;
        approvalHistoryDTO.approvalTime = SessionConstants.MIN_DATE;

        Employee_recordsDTO employeeRecords = Employee_recordsRepository.getInstance().getById(employeeOfficeDTO.employeeRecordId);
        if (employeeRecords != null) {
            approvalHistoryDTO.nameEn = employeeRecords.nameEng;
            approvalHistoryDTO.nameBn = employeeRecords.nameBng;
            approvalHistoryDTO.signature = employeeRecords.signature;
        }
        Office_unitsDTO officeUnits = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(employeeOfficeDTO.officeUnitId);
        if (officeUnits != null) {
            approvalHistoryDTO.officeUnitId = officeUnits.iD;
            approvalHistoryDTO.officeNameEn = officeUnits.unitNameEng;
            approvalHistoryDTO.officeNameBn = officeUnits.unitNameBng;
        }
        OfficeUnitOrganograms officeUnitOrganograms = OfficeUnitOrganogramsRepository.getInstance().getById(employeeOfficeDTO.officeUnitOrganogramId);
        if (officeUnitOrganograms != null) {
            approvalHistoryDTO.organogramId = officeUnitOrganograms.id;
            approvalHistoryDTO.orgNameEn = officeUnitOrganograms.designation_eng;
            approvalHistoryDTO.orgNameBn = officeUnitOrganograms.designation_bng;
        }
        return approvalHistoryDTO;
    }

    public static List<Bill_approval_historyDTO> newDtoListFrom(Bill_approval_mappingDTO billApprovalMappingDTO,
                                                                ApprovableDTO approvableDTO) {
        return billApprovalMappingDTO
                .getOrganogramIdList(approvableDTO)
                .stream()
                .map(organogramId -> EmployeeOfficeRepository.getInstance().getByOfficeUnitOrganogramId(organogramId))
                .filter(Objects::nonNull)
                .map(employeeOfficeDTO -> newDtoFrom(billApprovalMappingDTO, approvableDTO, employeeOfficeDTO))
                .collect(Collectors.toList());
    }

    public static Optional<Bill_approval_historyDTO> findUsersHistory(List<Bill_approval_historyDTO> approvalHistoryDTOs,
                                                                      UserDTO userDTO) {
        if (approvalHistoryDTOs == null || approvalHistoryDTOs.isEmpty()) {
            return Optional.empty();
        }
        return approvalHistoryDTOs.stream()
                                  .filter(approvalHistoryDTO -> approvalHistoryDTO.isThisUsersHistory(userDTO))
                                  .findAny();
    }

    public boolean hasValidSignature() {
        return signature != null && signature.length != 0;
    }
}
