package bill_approval_history;

public interface ApprovableDTO {
    String getTableName();

    Long getId();

    Integer getLevel();

    Long getOfficeUnitId();

    Long getTaskTypeId();
}
