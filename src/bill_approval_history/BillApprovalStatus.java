package bill_approval_history;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

public enum BillApprovalStatus {
    PENDING(1, "Waiting for Approval", "অনুমোদনের অপেক্ষায়", "#22ccc1"),
    APPROVED(2, "Approved", "অনুমোদিত", "green"),
    APPROVED_BY_OTHER(3, "", "", ""),
    ;

    private static final Map<Integer, BillApprovalStatus> mapByValue;

    static {
        mapByValue = Arrays.stream(values())
                           .collect(Collectors.toMap(e -> e.value, e -> e));
    }

    public static BillApprovalStatus getFromValue(Integer value) {
        if (value == null) {
            return null;
        }
        return mapByValue.getOrDefault(value, null);
    }

    private final int value;
    private final String nameEn, nameBn, color;

    BillApprovalStatus(int value, String nameEn, String nameBn, String color) {
        this.value = value;
        this.nameEn = nameEn;
        this.nameBn = nameBn;
        this.color = color;
    }

    public int getValue() {
        return value;
    }

    public String getColor() {
        return color;
    }

    public String getName(boolean isLangEn) {
        return isLangEn ? nameEn : nameBn;
    }
}
