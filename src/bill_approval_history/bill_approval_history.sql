CREATE TABLE bill_approval_history
(
    ID                   BIGINT PRIMARY KEY,

    table_name           VARCHAR(128),
    approvable_dto_id    BIGINT,
    level                INT,
    approval_status      INT,
    approval_time        BIGINT,
    name_en              VARCHAR(2048),
    name_bn              VARCHAR(2048),
    office_unit_id       BIGINT,
    office_name_en       VARCHAR(2048),
    office_name_bn       VARCHAR(2048),
    organogram_id        BIGINT,
    org_name_en          VARCHAR(2048),
    org_name_bn          VARCHAR(2048),
    signature            LONGBLOB,

    modified_by          BIGINT,
    lastModificationTime BIGINT DEFAULT -62135791200000,
    inserted_by          BIGINT,
    insertion_time       BIGINT DEFAULT -62135791200000,
    isDeleted            INT    DEFAULT 0
) ENGINE = MyISAM
  DEFAULT CHARSET = utf8
  COLLATE = utf8_unicode_ci;

insert into vb_sequencer (table_name, next_id, table_LastModificationTime) value ('bill_approval_history', 1, 0);