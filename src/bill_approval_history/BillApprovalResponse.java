package bill_approval_history;

import java.util.List;

public class BillApprovalResponse {
    public Boolean hasNextApproval;
    public Integer nextLevel;
    public List<Bill_approval_historyDTO> nextApprovalHistoryDTOs;
}
