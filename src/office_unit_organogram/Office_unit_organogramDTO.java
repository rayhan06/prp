package office_unit_organogram;

import office_unit_organograms.SameDesignationGroup;
import util.CommonDTO;

import java.util.Comparator;

public class Office_unit_organogramDTO extends CommonDTO {


	public long id;
    public long officeUnitId;
    public long superiorUnitId = -1;
    public long superiorDesignationId = -1;
    public String designationEng = "";
    public String designationBng = "";
    public String briefDescriptionEng = "";
    public String briefDescriptionBng = "";
    public int jobGradeTypeCat = -1;
    public String orgTree = "";
    public int layer1 = 0;
    public int layer2 = 0;
    public boolean isVacant = false;
    public long assignedEmployeeRecordId = -1;
    public long insertionDate = -1;
    public long insertedBy = -1;
    public long modifiedBy = -1;
    public int orderValue = 99999;
    public int status = 1;
    public long permanentOrganogramId = -1;
    public long substituteOrganogramId = -1;
    public long designationsId = -1;
    
    public boolean isVirtual = false;

    public Office_unit_organogramDTO()
    {
    	
    }
    
    public Office_unit_organogramDTO(Office_unit_organogramDTO mainOrganogram)
    {
    	officeUnitId = mainOrganogram.officeUnitId;
    	superiorUnitId = mainOrganogram.superiorUnitId;
    	superiorDesignationId = mainOrganogram.superiorDesignationId;
    	designationEng = mainOrganogram.designationEng;
    	designationBng = mainOrganogram.designationBng;
    	briefDescriptionEng = mainOrganogram.briefDescriptionEng;
    	briefDescriptionBng = mainOrganogram.briefDescriptionBng;
    	jobGradeTypeCat = mainOrganogram.jobCat;
    	orgTree = mainOrganogram.orgTree;
    	layer1 = mainOrganogram.layer1;
    	layer2 = mainOrganogram.layer2;
    	isVacant = mainOrganogram.isVacant;
    	assignedEmployeeRecordId = mainOrganogram.assignedEmployeeRecordId;
    	insertionDate = mainOrganogram.insertionDate;
    	insertedBy = mainOrganogram.insertedBy;
    	modifiedBy= mainOrganogram.modifiedBy;
    	orderValue = mainOrganogram.orderValue;
    	status = mainOrganogram.status;
    	permanentOrganogramId = mainOrganogram.iD;
    }
    
 
    
    
    @Override
    public String toString() {
        return "Office_unit_organogramDTO{" +
                "id=" + id +
                ", officeUnitId=" + officeUnitId +
                ", superiorUnitId=" + superiorUnitId +
                ", superiorDesignationId=" + superiorDesignationId +
                ", designationEng='" + designationEng + '\'' +
                ", designationBng='" + designationBng + '\'' +
                ", briefDescriptionEng='" + briefDescriptionEng + '\'' +
                ", briefDescriptionBng='" + briefDescriptionBng + '\'' +
                ", jobGradeTypeCat=" + jobGradeTypeCat +
                ", orgTree='" + orgTree + '\'' +
                ", layer1=" + layer1 +
                ", layer2=" + layer2 +
                ", isVacant=" + isVacant +
                ", insertionDate=" + insertionDate +
                ", insertedBy=" + insertedBy +
                ", modifiedBy=" + modifiedBy +
                '}';
    }

    public static Comparator<Office_unit_organogramDTO> groupSameDesignations =
            Comparator.comparing((Office_unit_organogramDTO dto) -> SameDesignationGroup.getDesignationKey(dto.designationEng))
                    .thenComparing(dto -> dto.layer1)
                    .thenComparing(dto -> dto.layer2)
                    .thenComparing(dto -> dto.officeUnitId)
                    .thenComparingLong(dto -> dto.iD);
}