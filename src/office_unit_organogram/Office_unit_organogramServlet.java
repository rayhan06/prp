package office_unit_organogram;

import com.google.gson.Gson;
import common.ApiResponse;
import common.BaseServlet;
import common.CacheUpdateModel;
import common.CommonDAOService;
import designations.DesignationsDTO;
import designations.DesignationsRepository;
import employee_offices.EmployeeOfficeDTO;
import employee_offices.EmployeeOfficeRepository;
import employee_records.Employee_recordsDTO;
import employee_records.Employee_recordsRepository;
import grade_wise_pay_scale.Grade_wise_pay_scaleRepository;
import language.LC;
import language.LM;
import login.LoginDTO;
import office_unit_organograms.*;
import office_units.Office_unitsDAO;
import office_units.Office_unitsDTO;
import office_units.Office_unitsRepository;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import pb.CatRepository;
import pb.CommonDAO;
import pb.Utils;
import pbReport.GenerateExcelReportService;
import permission.MenuConstants;
import role.PermissionRepository;
import sessionmanager.SessionConstants;
import test_lib.util.Pair;
import user.UserDTO;
import user.UserRepository;
import util.*;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;


@WebServlet("/Office_unit_organogramServlet")
@MultipartConfig
@SuppressWarnings({"Duplicates", "unchecked"})
public class Office_unit_organogramServlet extends BaseServlet implements GenerateExcelReportService {
    private static final long serialVersionUID = 1L;

    private final Office_unit_organogramDAO officeUnitOrganogramDAO = Office_unit_organogramDAO.getInstance();

    private final OfficeUnitOrganogramsRepository repository = OfficeUnitOrganogramsRepository.getInstance();

    @Override
    public String getTableName() {
        return officeUnitOrganogramDAO.getTableName();
    }

    @Override
    public String getServletName() {
        return "Office_unit_organogramServlet";
    }

    @Override
    public Office_unit_organogramDAO getCommonDAOService() {
        return officeUnitOrganogramDAO;
    }

    @Override
    public String commonPartOfDispatchURL() {
        return "office_unit_organogram/office_unit_organogram";
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[]{MenuConstants.OFFICE_UNIT_ORGANOGRAM_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[]{MenuConstants.OFFICE_UNIT_ORGANOGRAM_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[]{MenuConstants.OFFICE_UNIT_ORGANOGRAM_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Office_unit_organogramServlet.class;
    }

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        boolean isLangEng = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;

        Office_unit_organogramDTO office_unit_organogramDTO;
        long currentTime = System.currentTimeMillis() + 180000; //3mins delay
        if (addFlag) {
            office_unit_organogramDTO = new Office_unit_organogramDTO();
            office_unit_organogramDTO.insertionDate = currentTime;
            office_unit_organogramDTO.insertedBy = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO.employee_record_id;
        } else {
            office_unit_organogramDTO = officeUnitOrganogramDAO.getDTOFromID(Long.parseLong(request.getParameter("iD")));
            long officeId = Long.parseLong(request.getParameter("officeId"));
            if (office_unit_organogramDTO.officeUnitId != officeId) {
                if (!office_unit_organogramDTO.isVacant) {
                    throw new Exception(isLangEng ? "You can't change office because employees are already assigned in this office"
                                                  : "দপ্তর পরিবর্তন করতে পারবেন না কারন ইতিমধ্যে এই দপ্তরে কর্মকর্তা নিযুক্ত করা আছে");
                }
            }
        }
        office_unit_organogramDTO.lastModificationTime = currentTime;
        office_unit_organogramDTO.modifiedBy = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO.employee_record_id;

        office_unit_organogramDTO.officeUnitId = Long.parseLong(request.getParameter("officeId"));
        Office_unitsDTO officeUnitsDTO = Office_unitsDAO.getInstance().getDTOFromID(office_unit_organogramDTO.officeUnitId);

        if (officeUnitsDTO == null) {
            throw new Exception(isLangEng ? "Office is not found" : "দপ্তর পাওয়া যায়নি");
        }

        if (officeUnitsDTO.isAbstractOffice) {
            throw new Exception(isLangEng ? "Can't to create designation in abstract office" : "কাল্পনিক দপ্তরের পদবী তৈরি করতে পারবেন না");
        }

        office_unit_organogramDTO.superiorUnitId = officeUnitsDTO.parentUnitId;

        office_unit_organogramDTO.superiorDesignationId = Long.parseLong(request.getParameter("superiorDesignationId"));
        if (OfficeUnitOrganogramsRepository.getInstance().getById(office_unit_organogramDTO.superiorDesignationId) == null) {
            throw new Exception(isLangEng ? "Please select superior designation" : "অনুগ্রহ করে ঊর্ধ্বতন পদবী বাছাই করুন");
        }

        office_unit_organogramDTO.isVirtual = request.getParameter("isVirtual") != null &&
                                              request.getParameter("isVirtual").equalsIgnoreCase("true");

        if (request.getParameter("designationsId") == null) {
            throw new Exception(isLangEng ? "Please select a designation" : "অনুগ্রহ করে পদবী বাছাই করুন");
        }
        office_unit_organogramDTO.designationsId = Long.parseLong(request.getParameter("designationsId"));
        if (office_unit_organogramDTO.designationsId == -1) {
            throw new Exception(isLangEng ? "Please select a designation" : "অনুগ্রহ করে পদবী বাছাই করুন");
        }

        DesignationsDTO designationsDTO = DesignationsRepository.getInstance().getDesignationsDTOByiD(office_unit_organogramDTO.designationsId);
        if (designationsDTO == null) {
            throw new Exception(isLangEng ? "Please select a valid designation" : "অনুগ্রহ করে বৈধ পদবী বাছাই করুন");
        }
        office_unit_organogramDTO.designationBng = designationsDTO.nameBn;
        office_unit_organogramDTO.designationEng = designationsDTO.nameEn;
        office_unit_organogramDTO.jobGradeTypeCat = designationsDTO.jobGradeCat;


        if (CatRepository.getInstance().getCategoryLanguageModel("job_grade_type", office_unit_organogramDTO.jobGradeTypeCat) == null) {
            throw new Exception(isLangEng ? "Please select grade" : "অনুগ্রহ করে গ্রেড বাছাই করুন");
        }
        office_unit_organogramDTO.briefDescriptionEng = Utils.doJsoupCleanOrReturnEmpty(request.getParameter("briefDescriptionEng"));
        office_unit_organogramDTO.briefDescriptionBng = Utils.doJsoupCleanOrReturnEmpty(request.getParameter("briefDescriptionBng"));
        office_unit_organogramDTO.layer1 = Utils.parseOptionalInt(
                request.getParameter("layer1"),
                0,
                isLangEng ? "Enter valid 1st Layer Order!" : "সঠিক ১ম স্তরের ক্রম দিন!"
        );
        office_unit_organogramDTO.layer2 = Utils.parseOptionalInt(
                request.getParameter("layer2"),
                0,
                isLangEng ? "Enter valid 2nd Layer Order!" : "সঠিক ২য় স্তরের ক্রম দিন!"
        );
        String substituteOrganogramId = request.getParameter("substituteOrganogramId");
        if (substituteOrganogramId != null) {
            substituteOrganogramId = substituteOrganogramId.trim();
        }
        if (substituteOrganogramId != null && substituteOrganogramId.length() > 0) {
            office_unit_organogramDTO.substituteOrganogramId = Long.parseLong(request.getParameter("substituteOrganogramId"));
        } else {
            office_unit_organogramDTO.substituteOrganogramId = -1;
        }
        CommonDAOService.CACHE_UPDATE_MODEL_THREAD_LOCAL.set(new CacheUpdateModel(office_unit_organogramDTO.lastModificationTime));
        Utils.handleTransaction(() -> {
            if (addFlag) {
                office_unit_organogramDTO.isVacant = true;
                officeUnitOrganogramDAO.add(office_unit_organogramDTO);
            } else {
                officeUnitOrganogramDAO.update(office_unit_organogramDTO);
            }
            OfficeUnitOrganogramsRepository.getInstance().reload(false);
        });
        Utils.executeCache();
        return office_unit_organogramDTO;
    }

    @Override
    public void init(HttpServletRequest request) throws ServletException {
        request.setAttribute("navName", "../office_unit_organogram/office_unit_organogramNav.jsp");
        request.setAttribute("formName", "../office_unit_organogram/office_unit_organogramSearchForm.jsp");
        request.setAttribute("servletName", "Office_unit_organogramServlet");
        if (request.getParameter("actionType").equals("delete")) {
            List<Long> ids = getDeleteIdList(request);
            List<Office_unit_organogramDTO> list = Office_unit_organogramDAO.getInstance().getDTOs(ids);
            boolean alreadyAssign = list.stream()
                                        .anyMatch(e -> e.isDeleted == 0 && e.status == 1 && !e.isVacant);
            if (alreadyAssign) {
                throw new ServletException(HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng
                                           ? "Employee(s) is already assigned in requested designation"
                                           : "অনুরোধকৃত পদবীতে ইতিমধ্যে কর্মকর্তা নিযুক্ত আছেন");
            }
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }
        String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
        try {
            String actionType = request.getParameter("actionType");
            logger.debug("actionType = " + actionType);
            if ("setOrdering".equals(actionType)) {
                if (Utils.checkPermission(String.valueOf(userDTO.employee_record_id), userDTO, MenuConstants.OFFICE_UNIT_ORGANOGRAM_SET_ORDERING)) {
                    request.getRequestDispatcher("office_unit_organogram/office_unit_irganogram_order_page.jsp").forward(request, response);
                    return;
                }
            } else if ("ajax_getOrderingRows".equals(actionType)) {
                Long officeUnitId = Utils.parseOptionalLong(request.getParameter("officeUnitId"), null, null);
                String designationName = Utils.cleanAndGetOptionalString(request.getParameter("designationName"));
                request.setAttribute(
                        "organogramDTOs",
                        Office_unit_organogramDAO.getInstance().getAllActiveDTOs(officeUnitId, designationName)
                );
                request.getRequestDispatcher("office_unit_organogram/setOrderingTableRows.jsp").forward(request, response);
            } else if ("revert".equals(actionType)) {
                if (Utils.checkPermission(String.valueOf(userDTO.employee_record_id), userDTO, MenuConstants.OFFICE_UNIT_ORGANOGRAM_UPDATE)) {
                    Utils.handleTransaction(() -> revert(request, response));
                    return;
                }
            } else if ("xl".equals(actionType)) {
                if (Utils.checkPermission(String.valueOf(userDTO.employee_record_id), userDTO, MenuConstants.OFFICE_UNIT_ORGANOGRAM_UPDATE)) {
                    getXl(request, response, userDTO, loginDTO);
                    return;
                }
            } else if ("getCPCEditPage".equals(actionType)) {
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.CPC)) {
                    logger.debug("going to cpc");
                    request.getRequestDispatcher("office_unit_organogram/cpc.jsp").forward(request, response);
                    return;
                }
                logger.debug("not going to cpc");
            } else if ("getOrganograms".equals(actionType)) {
                long officeUnitOrganogramId = Long.parseLong(request.getParameter("officeUnitOrganogramId"));
                OfficeUnitOrganograms officeUnitOrganograms = OfficeUnitOrganogramsRepository.getInstance().getById(officeUnitOrganogramId);


                Map<String, Object> modelMap = new HashMap<>();
                modelMap.put("id", officeUnitOrganograms.id);
                modelMap.put("gradeTypeCat", officeUnitOrganograms.job_grade_type_cat);
                modelMap.put("gradeTypeCatText", CatRepository.getInstance().getText(Language, "job_grade", officeUnitOrganograms.job_grade_type_cat));
                modelMap.put("option", Grade_wise_pay_scaleRepository.getInstance().buildOptions(Language, officeUnitOrganograms.job_grade_type_cat, null));

                String data = new Gson().toJson(modelMap);
                response.setContentType("application/json");
                PrintWriter out = response.getWriter();
                out.print(data);
                out.flush();
                return;
            } else if ("getOrganogramOptionsAll".equals(actionType)) {

                String options = CommonDAO.getOptions(Language, "designations", -1);
                response.getWriter().write(options);

                return;
            } else if ("ajax_office_unit_organogram".equals(actionType)) {
                request.getRequestDispatcher("office_unit_organogram/office_unit_irganogram_order_body.jsp").forward(request, response);
                return;
            } else if ("ajax_getOrderingExcel".equals(actionType)) {
                getOrderingExcel(request, response, loginDTO, userDTO);
                return;
            } else if ("ajax_head".equals(actionType)) {
                long id = Long.parseLong(request.getParameter("id"));
                Map<String, Object> modelMap = new LinkedHashMap<>();
                OfficeUnitOrganograms organograms = repository.getOfficeUnitHeadWithActiveEmpStatusAndConsiderSubstitute(id);
                if (organograms != null) {
                    modelMap.put("id", organograms.id);
                    modelMap.put("designationEng", organograms.designation_eng);
                    modelMap.put("designationBng", organograms.designation_bng);
                    Office_unitsDTO officeUnitsDTO = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(organograms.office_unit_id);
                    if (officeUnitsDTO != null) {
                        EmployeeOfficeDTO employeeOfficeDTO = EmployeeOfficeRepository.getInstance().getByOfficeUnitOrganogramId(organograms.id);
                        if (employeeOfficeDTO != null) {
                            modelMap.put("officeEng", employeeOfficeDTO.unitNameEn);
                            modelMap.put("officeBng", employeeOfficeDTO.unitNameBn);
                            Employee_recordsDTO employeeRecordsDTO = Employee_recordsRepository.getInstance().getById(employeeOfficeDTO.employeeRecordId);
                            if (employeeRecordsDTO != null) {
                                modelMap.put("employeeNameEng", employeeRecordsDTO.nameEng);
                                modelMap.put("employeeNameBng", employeeRecordsDTO.nameBng);
                            }
                        }
                    }
                }
                String data = new Gson().toJson(modelMap.size() > 0 ? modelMap : "{}");
                response.setContentType("application/json");
                PrintWriter out = response.getWriter();
                out.print(data);
                out.flush();
                return;
            } else if ("ajax_headList".equals(actionType)) {
                long id = Long.parseLong(request.getParameter("id"));
                String considerVacancy = request.getParameter("considerVacancy");
                response.setContentType("application/json");
                PrintWriter out = response.getWriter();
                out.print(repository.getOfficeUnitHeadList(id, considerVacancy != null && considerVacancy.equals("true"), true));
                out.flush();
                return;
            } else {
                super.doGet(request, response);
            }
            return;
        } catch (Exception ex) {
            logger.error(ex);
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }

    private static final int organogramIdIndex = 0;
    private static final int orderIndex = 3;

    private void getOrderingExcel(HttpServletRequest request, HttpServletResponse response, LoginDTO loginDTO, UserDTO userDTO) throws IOException {
        long officeUnitId = Long.parseLong(request.getParameter("officeUnitId"));
        boolean isLangEng = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
        String excelHeader = isLangEng ? "Designation Ordering" : "পদবী ক্রম";
        List<List<Object>> excelRows = new ArrayList<>();
        excelRows.add(Arrays.asList(
                "ID",
                isLangEng ? "Designation" : "পদবী",
                LM.getText(LC.GLOBAL_EMPLOYEE_MP, loginDTO),
                isLangEng ? "Order" : "ক্রম"
        ));
        List<Office_unit_organogramDTO> organogramDTOs = Office_unit_organogramDAO.getInstance().getByOfficeUnitId(officeUnitId);
        for (Office_unit_organogramDTO organogramDTO : organogramDTOs) {
            if (organogramDTO == null) {
                continue;
            }
            List<Object> row = new ArrayList<>();
            row.add(organogramDTO.iD);
            row.add(isLangEng ? organogramDTO.designationEng : organogramDTO.designationBng);
            Employee_recordsDTO employeeRecordsDTO = Employee_recordsRepository.getInstance().getDTOByOrganogramId(organogramDTO.iD);

            String employeeInfo = "";

            if (employeeRecordsDTO != null) {
                if (isLangEng) {
                    employeeInfo = employeeRecordsDTO.nameEng + ", \n"
                                   + employeeRecordsDTO.employeeNumber + ", \n"
                                   + employeeRecordsDTO.personalMobile;
                } else {
                    employeeInfo = employeeRecordsDTO.nameBng + ", \n"
                                   + StringUtils.convertToBanNumber(employeeRecordsDTO.employeeNumber) + ", \n"
                                   + StringUtils.convertToBanNumber(employeeRecordsDTO.personalMobile);
                }
            }
            row.add(employeeInfo);
            row.add(organogramDTO.orderValue);
            excelRows.add(row);
        }
        XSSFWorkbook workbook = createXl(excelRows, null, excelHeader, "ignored", userDTO);
        response.setContentType("application/vnd.ms-excel");
        String fileName = "Designation Ordering - "
                          + Office_unitsRepository.getInstance().geText("english", officeUnitId)
                          + ".xlsx";
        response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
        workbook.write(response.getOutputStream());
        workbook.close();
    }


    private void getXl(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO, LoginDTO loginDTO) {
        Map<String, String> params = HttpRequestUtils.buildRequestParams(request);
        if (request.getAttribute(CommonDAOService.CRITERIA_MAP_KEY) != null) {
            Map<String, String> extraCriteriaMap = (Map<String, String>) request.getAttribute(CommonDAOService.CRITERIA_MAP_KEY);
            params.putAll(extraCriteriaMap);
        }
        RecordNavigator recordNavigator = getCommonDAOService().getRecordNavigatorLimitless(params, getTableJoinClause(request), getWhereClause(request), getSortClause(request));
        List<Office_unit_organogramDTO> office_unit_organogramDTOs = (List<Office_unit_organogramDTO>) recordNavigator.list;
        String Language = LM.getLanguage(userDTO);

        getXl(office_unit_organogramDTOs, response, Language, loginDTO);

    }

    private void getXl(List<Office_unit_organogramDTO> office_unit_organogramDTOs, HttpServletResponse response,
                       String Language, LoginDTO loginDTO) {
        XSSFWorkbook wb = new XSSFWorkbook();
        boolean isLangEng = Language.equalsIgnoreCase("english");

        List<String> headers = new ArrayList<>();
        if (isLangEng) {
            headers.add("Office");
            headers.add("Superior Designation");

            headers.add("Designation (English)");
            headers.add("Designation (Bangla)");

            headers.add("Brief Description");
            headers.add("1st Layer Order");
            headers.add("2nd Layer Order");
            headers.add("Status");
            headers.add("Virtual Post");

        } else {
            headers.add("দপ্তর");
            headers.add("ঊর্ধ্বতন পদবী");

            headers.add("পদবী (ইংরেজী)");
            headers.add("পদবী (বাংলা)");

            headers.add("সংক্ষিপ্ত বিবরণ");
            headers.add("১ম স্তরের ক্রম");
            headers.add("২য় স্তরের ক্রম");
            headers.add("অবস্থা");
            headers.add("ভার্চুয়াল পদ");
        }


        String tabName = "Designations";
        CommonRequestHandler commonRequestHandler = new CommonRequestHandler();
        Sheet sheet;
        sheet = wb.createSheet(tabName);
        Row headerRow = sheet.createRow(0);
        try {
            int cellIndex = 0;
            for (String header : headers) {
                sheet.setColumnWidth(cellIndex, 25 * 256);
                Cell cell = headerRow.createCell(cellIndex);
                cell.setCellValue(header);
                cell.setCellStyle(commonRequestHandler.getHeaderStyle(wb));

                cellIndex++;
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.getMessage());
        }

        int i = 1;
        CellStyle wrapStyle = wb.createCellStyle();
        wrapStyle.setWrapText(true);
        wrapStyle.setVerticalAlignment(VerticalAlignment.TOP);
        for (Office_unit_organogramDTO office_unit_organogramDTO : office_unit_organogramDTOs) {
            Row row = sheet.createRow(i);

            int cellIndex = 0;

            Cell cell = row.createCell(cellIndex);
            cell.setCellStyle(wrapStyle);
            cell.setCellValue(Office_unitsRepository.getInstance().geText(Language, office_unit_organogramDTO.officeUnitId));
            cellIndex++;

            cell = row.createCell(cellIndex);
            cell.setCellStyle(wrapStyle);
            cell.setCellValue(OfficeUnitOrganogramsRepository.getInstance().getDesignation(Language, office_unit_organogramDTO.superiorDesignationId));
            cellIndex++;

            cell = row.createCell(cellIndex);
            cell.setCellStyle(wrapStyle);
            cell.setCellValue(office_unit_organogramDTO.designationEng);
            cellIndex++;


            cell = row.createCell(cellIndex);
            cell.setCellStyle(wrapStyle);
            cell.setCellValue(office_unit_organogramDTO.designationBng);
            cellIndex++;

            cell = row.createCell(cellIndex);
            cell.setCellStyle(wrapStyle);
            cell.setCellValue(isLangEng ? office_unit_organogramDTO.briefDescriptionEng : office_unit_organogramDTO.briefDescriptionBng);
            cellIndex++;

            cell = row.createCell(cellIndex);
            cell.setCellStyle(wrapStyle);
            cell.setCellValue(StringUtils.convertBanglaIfLanguageIsBangla(Language, String.valueOf(office_unit_organogramDTO.layer1)));
            cellIndex++;

            cell = row.createCell(cellIndex);
            cell.setCellStyle(wrapStyle);
            cell.setCellValue(StringUtils.convertBanglaIfLanguageIsBangla(Language, String.valueOf(office_unit_organogramDTO.layer2)));
            cellIndex++;


            cell = row.createCell(cellIndex);
            cell.setCellStyle(wrapStyle);
            cell.setCellValue(office_unit_organogramDTO.status == 1 ? LM.getText(LC.GLOBAL_STATUS_ACTIVE, loginDTO) : LM.getText(LC.GLOBAL_STATUS_INACTIVE, loginDTO));
            cellIndex++;


            cell = row.createCell(cellIndex);
            cell.setCellStyle(wrapStyle);
            cell.setCellValue(Utils.getYesNo(office_unit_organogramDTO.isVirtual, Language));
            cellIndex++;
            i++;
        }

        try {
            String fileName = "Designations.xlsx";
            response.setContentType("application/vnd.ms-excel");
            response.setHeader("Content-Disposition", "attachment; filename=" + fileName);
            System.out.println("Writing xl");
            wb.write(response.getOutputStream());
            wb.close();
        } catch (IOException e) {
            e.printStackTrace();
            logger.error(e.getMessage());
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }

        try {
            String actionType = request.getParameter("actionType");
            if ("ajax_setOrdering".equals(actionType)) {
                if (Utils.checkPermission(String.valueOf(userDTO.employee_record_id), userDTO, MenuConstants.OFFICE_UNIT_ORGANOGRAM_SET_ORDERING)) {
                    addSetOrdering(request, response, userDTO);
                    return;
                }
            } else if ("ajax_saveOrderFromExcel".equals(actionType)) {
                if (Utils.checkPermission(String.valueOf(userDTO.employee_record_id), userDTO, MenuConstants.OFFICE_UNIT_ORGANOGRAM_SET_ORDERING)) {
                    ApiResponse apiResponse;
                    boolean isLangEng = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
                    try {
                        saveOrderFromExcel(request);
                        apiResponse = ApiResponse.makeSuccessResponse(isLangEng ? "Successfully saved" : "সফলভাবে সেভ হয়েছে");
                    } catch (Exception ex) {
                        logger.error(ex.getMessage());
                        apiResponse = ApiResponse.makeErrorResponse(isLangEng ? "Error with excel" : "এক্সেলে ভুল রয়েছে");
                    }
                    response.getWriter().write(apiResponse.getJSONString());
                    response.getWriter().flush();
                    response.getWriter().close();
                    return;
                }
            } else if ("ajax_save_order".equals(actionType)) {
                if (Utils.checkPermission(String.valueOf(userDTO.employee_record_id), userDTO, MenuConstants.OFFICE_UNIT_ORGANOGRAM_SET_ORDERING)) {
                    setOrder(request, response);
                    return;
                }
            } else if ("createTemp".equals(actionType)) {
                if (Utils.checkPermission(String.valueOf(userDTO.employee_record_id), userDTO, MenuConstants.OFFICE_UNIT_ORGANOGRAM_UPDATE)) {
                    Utils.handleTransaction(() -> createTemp(request, response));
                    return;
                }
            } else if ("ajax_config".equals(actionType)) {
                if (Utils.checkPermission(String.valueOf(userDTO.employee_record_id), userDTO, MenuConstants.OFFICE_UNIT_ORGANOGRAM_UPDATE)) {
                    config(request, response, userDTO);
                    return;
                }
            } else {
                super.doPost(request, response);
            }
            return;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }


    private void config(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO) throws IOException {
        long committeeId = Long.parseLong(request.getParameter("committeeId"));
        long selectedOrg = Long.parseLong(request.getParameter("selectedOrg"));

        OfficeUnitOrganograms head = OfficeUnitOrganogramsRepository.getInstance().getCommiteeHead(committeeId);
        if (head == null) {
            response.getWriter().write("Failure");
            return;
        }

        BooleanWrapper doAdd = BooleanWrapper.of(false);

        Committee_post_mapDTO committee_post_mapDTO = Committee_post_mapRepository.getInstance().copyByCommittee(committeeId);

        try {
            if (committee_post_mapDTO == null) {
                committee_post_mapDTO = new Committee_post_mapDTO();
                committee_post_mapDTO.committeeId = committeeId;
                doAdd.setValue(true);
            }

            committee_post_mapDTO.postId = head.id;
            committee_post_mapDTO.replacementPostId = selectedOrg;
            committee_post_mapDTO.modifiedBy = userDTO.ID;

            long lastModificationTime = System.currentTimeMillis() + 180000; //3mins delay

            committee_post_mapDTO.lastModificationTime = lastModificationTime;

            Committee_post_mapDTO dto = committee_post_mapDTO;

            CommonDAOService.CACHE_UPDATE_MODEL_THREAD_LOCAL.set(new CacheUpdateModel(lastModificationTime));
            Utils.handleTransaction(() -> {
                if (doAdd.getValue()) {
                    Committee_post_mapDAO.getInstance().add(dto);
                } else {
                    Committee_post_mapDAO.getInstance().update(dto);
                }
                Office_unit_organogramDAO.getInstance().assignHead(dto.postId, dto.replacementPostId, userDTO, committeeId, lastModificationTime, -1);
            });
            Utils.executeCache();
            response.getWriter().write("Success");
        } catch (Exception e) {
            response.getWriter().write("Failure");
            e.printStackTrace();
        }
    }

    private void revert(HttpServletRequest request, HttpServletResponse response) throws Exception {
        // TODO Auto-generated method stub
        long id = Long.parseLong(request.getParameter("ID"));
        Office_unit_organogramDTO office_unit_organogramDTO = Office_unit_organogramDAO.getInstance().getDTOByID(id);
        if (office_unit_organogramDTO != null) {
            Office_unit_organogramDTO permanenttDTO = Office_unit_organogramDAO.getInstance().getDTOByID(office_unit_organogramDTO.permanentOrganogramId);
            permanenttDTO.status = 1;
            Office_unit_organogramDAO.getInstance().update(permanenttDTO);

            office_unit_organogramDTO.status = 0;
            Office_unit_organogramDAO.getInstance().update(office_unit_organogramDTO);

            String orgPrefixToSet = permanenttDTO.orgTree;
            String orgPrefixToMatch = office_unit_organogramDTO.orgTree;


            if (orgPrefixToSet.endsWith("$")) {
                orgPrefixToSet = orgPrefixToSet.substring(0, orgPrefixToSet.length() - 1) + "|";
            }
            Office_unit_organogramDAO.getInstance().updateChildrenOrgTree(permanenttDTO.iD, orgPrefixToMatch, orgPrefixToSet, office_unit_organogramDTO.iD);
        }

        response.sendRedirect("Office_unit_organogramServlet?actionType=search");
    }

    private void createTemp(HttpServletRequest request, HttpServletResponse response) throws Exception {
        // TODO Auto-generated method stub
        long id = Long.parseLong(request.getParameter("ID"));
        Office_unit_organogramDTO office_unit_organogramDTO = Office_unit_organogramDAO.getInstance().getDTOByID(id);
        if (office_unit_organogramDTO != null) {
            Office_unit_organogramDTO tempOrgDTO = new Office_unit_organogramDTO(office_unit_organogramDTO);
            tempOrgDTO.designationEng = Jsoup.clean(request.getParameter("designationEng"), Whitelist.simpleText());
            tempOrgDTO.designationBng = Jsoup.clean(request.getParameter("designationBng"), Whitelist.simpleText());
            if (tempOrgDTO.designationEng == null || tempOrgDTO.designationEng.equalsIgnoreCase("") ||
                tempOrgDTO.designationBng == null || tempOrgDTO.designationBng.equalsIgnoreCase("")) {
                logger.debug("returning after finding null");
                response.sendRedirect("Office_unit_organogramServlet?actionType=search");
                return;
            }
            logger.debug("office_unit_organogramDTO = " + office_unit_organogramDTO.iD + " tempOrgDTO.designationEng = " + tempOrgDTO.designationEng);
            Office_unit_organogramDAO.getInstance().add(tempOrgDTO);
            System.out.println("added " + tempOrgDTO.iD);


            office_unit_organogramDTO.status = 0;
            Office_unit_organogramDAO.getInstance().update(office_unit_organogramDTO);

            String orgPrefixToSet = tempOrgDTO.orgTree;
            String orgPrefixToMatch = office_unit_organogramDTO.orgTree;


            if (orgPrefixToSet.endsWith("$")) {
                orgPrefixToSet = orgPrefixToSet.substring(0, orgPrefixToSet.length() - 1) + "|";
            }
            Office_unit_organogramDAO.getInstance().updateChildrenOrgTree(tempOrgDTO.iD, orgPrefixToMatch, orgPrefixToSet, office_unit_organogramDTO.iD);
        }

        response.sendRedirect("Office_unit_organogramServlet?actionType=search");

    }

    private void setOrder(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String orders = request.getParameter("orders");
        long officeUnitId = Long.parseLong(request.getParameter("officeUnitId"));
        List<Long> organogramIdList = Stream.of(orders.split(","))
                                            .map(Long::parseLong)
                                            .collect(Collectors.toList());
        logger.debug("officeUnitId : " + officeUnitId);
        logger.debug("orders : " + orders);
        ApiResponse apiResponse;
        try {
            officeUnitOrganogramDAO.updateOrder(organogramIdList);
            boolean isLangEng = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
            apiResponse = ApiResponse.makeSuccessResponse(isLangEng ? "Successfully saved" : "সফলভাবে সেভ হয়েছে");
        } catch (Exception ex) {
            apiResponse = ApiResponse.makeErrorResponse(ex.getMessage());
        }
        response.getWriter().write(apiResponse.getJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    }

    private void saveOrderFromExcel(HttpServletRequest request) throws Exception {
        Part orderingExcel = request.getPart("orderingExcel");
        XSSFWorkbook workbook = new XSSFWorkbook(orderingExcel.getInputStream());
        XSSFSheet sheet = workbook.getSheetAt(0);
        List<Pair<Long, Integer>> organogramIdOrder = new ArrayList<>();
        final DataFormatter formatter = new DataFormatter();
        for (int i = 2; i < sheet.getPhysicalNumberOfRows(); i++) {
            final Row row = sheet.getRow(i);
            long organogramId = Long.parseLong(formatter.formatCellValue(row.getCell(organogramIdIndex)));
            int order = Integer.parseInt(formatter.formatCellValue(row.getCell(orderIndex)));
            organogramIdOrder.add(new Pair<>(organogramId, order));
        }
        List<Long> orderedOrganogramIds =
                organogramIdOrder.stream()
                                 .sorted(Comparator.comparingInt(Pair::getValue))
                                 .map(Pair::getKey)
                                 .collect(Collectors.toList());
        officeUnitOrganogramDAO.updateOrder(orderedOrganogramIds);
    }

    private void addSetOrdering(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO) throws Exception {
        boolean isLangEng = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
        Long officeUnitId = Utils.parseOptionalLong(request.getParameter("officeUnitId"), null, null);
        String designationName = Utils.cleanAndGetOptionalString(request.getParameter("designationName"));

        List<Office_unit_organogramDTO> organogramDTOs = Office_unit_organogramDAO.getInstance()
                                                                                  .getAllActiveDTOs(officeUnitId, designationName);
        int layer1, layer2;
        long currentTime = System.currentTimeMillis();
        for (Office_unit_organogramDTO organogramDTO : organogramDTOs) {
            layer1 = Utils.parseOptionalInt(
                    request.getParameter("layer1_" + organogramDTO.iD),
                    0,
                    isLangEng ? "Enter valid number!" : "সঠিক সংখ্যা দিন"
            );
            layer2 = Utils.parseOptionalInt(
                    request.getParameter("layer2_" + organogramDTO.iD),
                    0,
                    isLangEng ? "Enter valid number!" : "সঠিক সংখ্যা দিন"
            );
            boolean layerChanged = (organogramDTO.layer1 != layer1) || (organogramDTO.layer2 != layer2);
            if (layerChanged) {
                Office_unit_organogramDAO.getInstance().updateLayers(
                        organogramDTO.iD, layer1, layer2, userDTO.ID, currentTime
                );
            }
        }
        ApiResponse apiResponse;
        apiResponse = ApiResponse.makeSuccessResponse(isLangEng ? "Successfully saved" : "সফল ভাবে সেভ করা হয়েছে");
        response.getWriter().write(apiResponse.getJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    }
}