package office_unit_organogram;

import appointment.KeyCountDTO;
import asset_model.AssetAssigneeDAO;
import audit.log.AuditLogDAO;
import audit.log.LogEvent;
import common.CommonDAOService;
import common.ConnectionAndStatementUtil;
import common.ConnectionType;
import dbm.DBMW;
import designations.DesignationsDTO;
import employee_assign.EmployeeAssignDAO;
import employee_office_report.InChargeLevelEnum;
import employee_offices.EmployeeOfficeDTO;
import employee_offices.EmployeeOfficeRepository;
import employee_offices.EmployeeOfficesDAO;
import office_unit_organograms.OfficeUnitOrganograms;
import office_unit_organograms.OfficeUnitOrganogramsRepository;
import office_unit_organograms.SameDesignationGroup;
import office_units.Office_unitsDTO;
import office_units.Office_unitsRepository;
import org.apache.log4j.Logger;
import pb.CatRepository;
import pb.CategoryLanguageModel;
import pb.Utils;
import sessionmanager.SessionConstants;
import user.UserDTO;
import util.CommonDTO;
import util.HttpRequestUtils;
import util.StringUtils;
import workflow.WingModel;
import workflow.WorkflowController;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@SuppressWarnings({"Duplicates"})
public class Office_unit_organogramDAO implements CommonDAOService<Office_unit_organogramDTO> {
    private final Logger logger = Logger.getLogger(Office_unit_organogramDAO.class);
    private final String addQuery;
    private final String updateQuery;
    private final String[] columnNames;
    private final static String updateOrgTree = "UPDATE office_unit_organograms SET org_tree = '%s',lastModificationTime = %d,modified_by = %d WHERE ID = %d";
    private final static String getAllActiveDTOs = "SELECT * FROM office_unit_organograms WHERE isDeleted = 0";
    private static final String getByOfficeUnitId = "SELECT * FROM office_unit_organograms WHERE office_unit_id = %d AND isDeleted = 0 ORDER BY order_value";
    private static final String getByDesignationsId = "SELECT * FROM office_unit_organograms WHERE designations_id = %d AND isDeleted = 0 ORDER BY order_value";
    private static final String updateOrder = "UPDATE office_unit_organograms SET order_value = %d,lastModificationTime = %d WHERE id = %d ";
    private static final String updateOrgTreeByOrgTreePrefix = "UPDATE office_unit_organograms SET org_tree= REPLACE(org_tree, ?, ?) , lastModificationTime= ?  where org_tree LIKE ?";
    private static final String updateOrgTreeByOrgTreePrefix2 = "UPDATE office_unit_organograms SET org_tree= REPLACE(org_tree, '%s', '%s') , lastModificationTime= %d  where org_tree LIKE '{prefix}' AND isDeleted = 0";
    private static final String updateOrgTreeByAndParentId = "UPDATE office_unit_organograms SET org_tree= '%s',superior_designation_id = %d, lastModificationTime= %d  where id = %d AND isDeleted = 0";
    private static final String updateSuperiorUnitIdByOfficeUnitId = "UPDATE office_unit_organograms SET superior_unit_id = %d, lastModificationTime= %d  where office_unit_id = %d AND isDeleted = 0";
    private static final String countByOrgTreePrefix = "SELECT COUNT(*) FROM office_unit_organograms WHERE org_tree LIKE '{prefix}' AND STATUS = 1 AND isDeleted = 0";
    private static final String updateDesignationInfoByDesignationId = "UPDATE office_unit_organograms SET designation_eng = '%s',designation_bng = '%s',job_grade_type_cat = %d, lastModificationTime = %d WHERE designations_id = %d AND isDeleted = 0";

    public void updateDesignationInfoByDesignationsDTO(DesignationsDTO dto) throws Exception {
        String sql = String.format(updateDesignationInfoByDesignationId, dto.nameEn, dto.nameBn, dto.jobGradeCat, dto.lastModificationTime, dto.iD);
        Utils.wrapWithAtomicReference(ar ->
                ConnectionAndStatementUtil.getWriteStatement(st -> {
                    try {
                        st.executeUpdate(sql);
                        recordUpdateTime(CommonDAOService.CONNECTION_THREAD_LOCAL.get(), getTableName(), dto.lastModificationTime);
                        Utils.addToRepoList(getTableName());
                    } catch (SQLException e) {
                        logger.error(e.getMessage());
                        ar.set(e);
                        e.printStackTrace();
                    }
                }, CommonDAOService.CONNECTION_THREAD_LOCAL.get())
        );
    }

    private Office_unit_organogramDAO() {
        columnNames = new String[]
                {
                        "office_unit_id",
                        "superior_unit_id",
                        "superior_designation_id",
                        "designation_eng",
                        "designation_bng",
                        "brief_description_eng",
                        "brief_description_bng",
                        "job_grade_type_cat",
                        "org_tree",
                        "layer_1",
                        "layer_2",
                        "isVacant",
                        "assignedEmployeeRecordId",
                        "created",
                        "created_by",
                        "modified_by",
                        "search_column",
                        "order_value",
                        "status",
                        "permanent_organogram_id",
                        "lastModificationTime",
                        "substitute_organogram_id",
                        "designations_id",
                        "is_virtual",
                        "isDeleted",
                        "ID",
                };
        updateQuery = getUpdateQuery2(columnNames);
        addQuery = getAddQuery2(columnNames);

        searchMap.put("designation_eng", " and (designation_eng like ?)");
        searchMap.put("designation_bng", " and (designation_bng like ?)");
        searchMap.put("job_grade_type_cat", " and (job_grade_type_cat = ?)");
        searchMap.put("officeUnitId", " and (office_unit_id = ?)");
        searchMap.put("AnyField", " and (search_column like ?)");
    }

    private final Map<String, String> searchMap = new HashMap<>();

    private static class LazyLoader {
        static final Office_unit_organogramDAO INSTANCE = new Office_unit_organogramDAO();
    }

    public static Office_unit_organogramDAO getInstance() {
        return LazyLoader.INSTANCE;
    }

    public void setSearchColumn(Office_unit_organogramDTO office_unit_organogramDTO) {
        office_unit_organogramDTO.searchColumn = office_unit_organogramDTO.designationEng + " " + office_unit_organogramDTO.designationBng + " ";
        CategoryLanguageModel model = CatRepository.getInstance().getCategoryLanguageModel("job_grade_type", office_unit_organogramDTO.jobGradeTypeCat);
        if (model != null) {
            office_unit_organogramDTO.searchColumn += model.englishText + " " + model.banglaText;
        }
    }

    @Override
    public void set(PreparedStatement ps, Office_unit_organogramDTO office_unit_organogramDTO, boolean isInsert) throws SQLException {
        int index = 0;
        if (office_unit_organogramDTO.lastModificationTime == 0) {
            office_unit_organogramDTO.lastModificationTime = System.currentTimeMillis();
        }
        setSearchColumn(office_unit_organogramDTO);
        ps.setObject(++index, office_unit_organogramDTO.officeUnitId);
        ps.setObject(++index, office_unit_organogramDTO.superiorUnitId);
        ps.setObject(++index, office_unit_organogramDTO.superiorDesignationId);
        ps.setObject(++index, office_unit_organogramDTO.designationEng);
        ps.setObject(++index, office_unit_organogramDTO.designationBng);
        ps.setObject(++index, office_unit_organogramDTO.briefDescriptionEng);
        ps.setObject(++index, office_unit_organogramDTO.briefDescriptionBng);
        ps.setObject(++index, office_unit_organogramDTO.jobGradeTypeCat);
        ps.setObject(++index, office_unit_organogramDTO.orgTree);
        ps.setObject(++index, office_unit_organogramDTO.layer1);
        ps.setObject(++index, office_unit_organogramDTO.layer2);
        ps.setObject(++index, office_unit_organogramDTO.isVacant);
        ps.setObject(++index, office_unit_organogramDTO.assignedEmployeeRecordId);
        ps.setObject(++index, office_unit_organogramDTO.insertionDate);
        ps.setObject(++index, office_unit_organogramDTO.insertedBy);
        ps.setObject(++index, office_unit_organogramDTO.modifiedBy);
        ps.setObject(++index, office_unit_organogramDTO.searchColumn);
        ps.setObject(++index, office_unit_organogramDTO.orderValue);
        ps.setObject(++index, office_unit_organogramDTO.status);
        ps.setObject(++index, office_unit_organogramDTO.permanentOrganogramId);
        ps.setObject(++index, office_unit_organogramDTO.lastModificationTime);
        ps.setObject(++index, office_unit_organogramDTO.substituteOrganogramId);
        ps.setObject(++index, office_unit_organogramDTO.designationsId);
        ps.setObject(++index, office_unit_organogramDTO.isVirtual);
        if (isInsert) {
            ps.setObject(++index, 0);
        }
        ps.setObject(++index, office_unit_organogramDTO.iD);
    }

    @Override
    public Office_unit_organogramDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Office_unit_organogramDTO office_unit_organogramDTO = new Office_unit_organogramDTO();
            int i = 0;
            office_unit_organogramDTO.officeUnitId = rs.getLong(columnNames[i++]);
            office_unit_organogramDTO.superiorUnitId = rs.getLong(columnNames[i++]);
            office_unit_organogramDTO.superiorDesignationId = rs.getLong(columnNames[i++]);
            office_unit_organogramDTO.designationEng = rs.getString(columnNames[i++]);
            office_unit_organogramDTO.designationBng = rs.getString(columnNames[i++]);
            office_unit_organogramDTO.briefDescriptionEng = rs.getString(columnNames[i++]);
            office_unit_organogramDTO.briefDescriptionBng = rs.getString(columnNames[i++]);
            office_unit_organogramDTO.jobGradeTypeCat = rs.getInt(columnNames[i++]);
            office_unit_organogramDTO.orgTree = rs.getString(columnNames[i++]);
            office_unit_organogramDTO.layer1 = rs.getInt(columnNames[i++]);
            office_unit_organogramDTO.layer2 = rs.getInt(columnNames[i++]);
            office_unit_organogramDTO.isVacant = rs.getBoolean(columnNames[i++]);
            office_unit_organogramDTO.assignedEmployeeRecordId = rs.getLong(columnNames[i++]);
            office_unit_organogramDTO.insertionDate = rs.getLong(columnNames[i++]);
            office_unit_organogramDTO.insertedBy = rs.getLong(columnNames[i++]);
            office_unit_organogramDTO.modifiedBy = rs.getLong(columnNames[i++]);
            office_unit_organogramDTO.searchColumn = rs.getString(columnNames[i++]);
            office_unit_organogramDTO.orderValue = rs.getInt(columnNames[i++]);
            office_unit_organogramDTO.status = rs.getInt(columnNames[i++]);
            office_unit_organogramDTO.permanentOrganogramId = rs.getInt(columnNames[i++]);
            office_unit_organogramDTO.lastModificationTime = rs.getLong(columnNames[i++]);
            office_unit_organogramDTO.substituteOrganogramId = rs.getLong(columnNames[i++]);
            office_unit_organogramDTO.designationsId = rs.getLong(columnNames[i++]);
            office_unit_organogramDTO.isVirtual = rs.getBoolean(columnNames[i++]);
            office_unit_organogramDTO.isDeleted = rs.getInt(columnNames[i++]);
            office_unit_organogramDTO.iD = rs.getLong(columnNames[i]);
            office_unit_organogramDTO.id = office_unit_organogramDTO.iD;
            return office_unit_organogramDTO;
        } catch (SQLException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public Office_unit_organogramDTO getDTOByID(long id) {
        return getDTOFromID(id);
    }

    @Override
    public String getTableName() {
        return "office_unit_organograms";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    public long add(CommonDTO commonDTO) throws Exception {
        return add((Office_unit_organogramDTO) commonDTO);
    }

    public long update(CommonDTO commonDTO) throws Exception {
        return update((Office_unit_organogramDTO) commonDTO);
    }

    public void assignHead(long headOrgId, long idToAssign, UserDTO userDTO,
                           long committeeId, long lastModificationTime, long erIdGiven) throws Exception {
        logger.debug("assignHead called with headOrgId = " + headOrgId + " idToAssign = " + idToAssign + " committeeId = " + committeeId);

        EmployeeOfficeDTO existingHeadDTO = EmployeeOfficeRepository.getInstance().getByOfficeUnitOrganogramId(headOrgId);

        if (existingHeadDTO != null && existingHeadDTO.officeUnitOrganogramId != -1) {
            logger.debug("Existing head found: " + existingHeadDTO.officeUnitOrganogramId);

            new EmployeeAssignDAO().addLastOfficeDateByOrganogramId(existingHeadDTO.officeUnitOrganogramId, lastModificationTime, lastModificationTime);

            Office_unit_organogramDAO.getInstance().updateVacancy(
                    existingHeadDTO.officeUnitOrganogramId, 0, true, userDTO.ID, lastModificationTime
            );
        }

        if (existingHeadDTO == null) {
            existingHeadDTO = new EmployeeOfficeDTO();
        }

        long employeeRecordId;
        if (erIdGiven != -1) {
            employeeRecordId = erIdGiven;
        } else {
            employeeRecordId = WorkflowController.getEmployeeRecordIDFromOrganogramIdDBOnly(idToAssign);
        }

        logger.debug("employeeRecordId: " + employeeRecordId);
        if (employeeRecordId != -1) {
            new EmployeeAssignDAO().addEmployeeIntoNewOffice(employeeRecordId,
                    committeeId, headOrgId, SessionConstants.COMMITTEE_SECRETARY, 0,
                    0, InChargeLevelEnum.ADDITIONAL_RESPONSIBILITY.getValue(),
                    0, lastModificationTime, -1, lastModificationTime, userDTO.ID, existingHeadDTO.salaryGradeType);

            Office_unit_organogramDAO.getInstance().updateVacancy(
                    headOrgId, employeeRecordId, false, userDTO.ID, lastModificationTime
            );
            new AssetAssigneeDAO().assignEmployeeToPost(headOrgId, employeeRecordId);
        }

    }

    public void updateChildrenOrgTree(long superiorId, String prevOrgPrefix, String orgPrefixToSet, long origSuperiorId) throws Exception {
        long lastModificationTime = System.currentTimeMillis();
        StringBuilder sqlBuilder = new StringBuilder("update office_unit_organograms set lastModificationTime = " + lastModificationTime + ", "
                + " superior_unit_id = " + superiorId + ","
                + " org_tree = concat('" + orgPrefixToSet + "', id, '$')  "
                + " where org_tree like '" + prevOrgPrefix + "%' "
                + " and id not in (" + superiorId + ", " + origSuperiorId + ")"
        );
        ConnectionAndStatementUtil.getWriteStatement(st -> {
            String sql = sqlBuilder.toString();
            try {
                logger.debug(sql);
                st.execute(sql);
                recordUpdateTime(CommonDAOService.CONNECTION_THREAD_LOCAL.get(), getTableName());
            } catch (SQLException ex) {
                logger.error(ex);
            }
        }, CommonDAOService.CONNECTION_THREAD_LOCAL.get());
    }

    private long add(Office_unit_organogramDTO dto) throws Exception {
        if (dto == null) {
            return -1;
        }
        String sql = addQuery.replace("{tableName}", getTableName());
        AtomicReference<Exception> atomicReference = new AtomicReference<>();
        long id = (Long) ConnectionAndStatementUtil.getWritePrepareStatement(ps -> {
            long lastModificationTime = System.currentTimeMillis() + 180000; // 3min delay
            try {
                dto.iD = DBMW.getInstance().getNextSequenceId(getTableName());
                logger.debug("add 1");
                OfficeUnitOrganograms superiorOrganogram = OfficeUnitOrganogramsRepository.getInstance().getById(dto.superiorDesignationId);
                if (superiorOrganogram == null) {
                    return -1;
                }
                logger.debug("add 2");
                if (superiorOrganogram.orgTree.endsWith("$")) {
                    superiorOrganogram.orgTree = superiorOrganogram.orgTree.substring(0, superiorOrganogram.orgTree.length() - 1) + "|";
                    ConnectionAndStatementUtil.getWriteStatement(st -> {
                        String updateSuperiorOrganogramSQL = String.format(updateOrgTree, superiorOrganogram.orgTree, lastModificationTime, dto.modifiedBy, dto.superiorDesignationId);
                        try {
                            st.executeUpdate(updateSuperiorOrganogramSQL);
                        } catch (SQLException e) {
                            e.printStackTrace();
                            atomicReference.set(e);
                        }
                    }, CommonDAOService.CONNECTION_THREAD_LOCAL.get());
                }
                logger.debug("add 3");
                if (atomicReference.get() == null) {
                    dto.orgTree = superiorOrganogram.orgTree + dto.iD + "$";
                    set(ps, dto, true);
                    dto.lastModificationTime = lastModificationTime;
                    logger.debug(ps);
                    ps.execute();
                    recordUpdateTime(CommonDAOService.CONNECTION_THREAD_LOCAL.get(), getTableName(), lastModificationTime);
                    AuditLogDAO.add(null, dto, LogEvent.ADD, getTableName(), HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().ipAddress,
                            HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO.ID, HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO.organogramID);
                }
            } catch (Exception e) {
                e.printStackTrace();
                atomicReference.set(e);
            }
            return dto.iD;
        }, CommonDAOService.CONNECTION_THREAD_LOCAL.get(), sql);
        if (atomicReference.get() != null) {
            throw atomicReference.get();
        }
        return id;
    }

    private long update(Office_unit_organogramDTO dto) throws Exception {
        Office_unit_organogramDTO oldDTO = getDTOFromID(dto.iD);
        if (oldDTO == null) {
            throw new Exception("Entity is not found");
        }
        OfficeUnitOrganograms superiorOrganogram = OfficeUnitOrganogramsRepository.getInstance().getById(dto.superiorDesignationId);
        if (superiorOrganogram == null) {
            throw new Exception("Superior is not found");
        }
        String sql = updateQuery.replace("{tableName}", getTableName());
        if (dto.superiorDesignationId == oldDTO.superiorDesignationId) {
            Utils.wrapWithAtomicReference(ar ->
                    ConnectionAndStatementUtil.getWritePrepareStatement(ps -> {
                        try {
                            set(ps, dto, false);
                            logger.debug(ps);
                            ps.executeUpdate();
                            Utils.addToRepoList(getTableName());
                            recordUpdateTime(CommonDAOService.CONNECTION_THREAD_LOCAL.get(), getTableName(), dto.lastModificationTime);
                            AuditLogDAO.add(oldDTO, dto, LogEvent.UPDATE, getTableName(), HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().ipAddress,
                                    HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO.ID, HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO.organogramID);
                        } catch (SQLException e) {
                            e.printStackTrace();
                            ar.set(e);
                        }
                    }, CommonDAOService.CONNECTION_THREAD_LOCAL.get(), sql)
            );
        } else {
            OfficeUnitOrganograms oldSuperiorOrganogram = OfficeUnitOrganogramsRepository.getInstance().getById(oldDTO.superiorDesignationId);
            if (oldSuperiorOrganogram == null) {
                throw new Exception("Old superior is not found");
            }
            boolean hasChild = oldDTO.orgTree.charAt(oldDTO.orgTree.length() - 1) == '|';
            boolean superiorHasChild = superiorOrganogram.orgTree.charAt(superiorOrganogram.orgTree.length() - 1) == '|';
            if (!superiorHasChild) {
                superiorOrganogram.orgTree = superiorOrganogram.orgTree.substring(0, superiorOrganogram.orgTree.length() - 1) + "|";
            }
            dto.orgTree = superiorOrganogram.orgTree + dto.iD + (hasChild ? "|" : "$");

            long count = Utils.wrapWithAtomicReferenceAndReturnR(ar ->
                    ConnectionAndStatementUtil.getT(countByOrgTreePrefix.replace("{prefix}", oldSuperiorOrganogram.orgTree + "%"),
                            rs -> {
                                try {
                                    return rs.getLong(1);
                                } catch (SQLException e) {
                                    e.printStackTrace();
                                    ar.set(e);
                                    return 0L;
                                }
                            })
            );
            logger.debug("Old superior's child count : " + count);

            if (count <= 1) {
                throw new Exception("ORG tree for old superior is inconsistent");
            }

            Utils.wrapWithAtomicReference(ar ->
                    ConnectionAndStatementUtil.getWritePrepareStatement(ps -> {
                        try {
                            set(ps, dto, false);
                            ps.executeUpdate();

                            ConnectionAndStatementUtil.getWriteStatement(st -> {
                                if (hasChild) {
                                    String updateChild = String.format(updateOrgTreeByOrgTreePrefix2, oldDTO.orgTree, dto.orgTree, dto.lastModificationTime)
                                            .replace("{prefix}", oldDTO.orgTree + "%");
                                    logger.debug(updateChild);
                                    try {
                                        st.executeUpdate(updateChild);
                                    } catch (SQLException e) {
                                        e.printStackTrace();
                                        ar.set(e);
                                    }
                                }

                                if (!superiorHasChild && ar.get() == null) {
                                    String updateParent = String.format(updateOrgTree, superiorOrganogram.orgTree, dto.lastModificationTime, dto.modifiedBy, dto.superiorDesignationId);
                                    logger.debug(updateParent);
                                    try {
                                        st.executeUpdate(updateParent);
                                    } catch (SQLException e) {
                                        e.printStackTrace();
                                        ar.set(e);
                                    }
                                }

                                if (count == 2) {
                                    String updateOldParent = String.format(updateOrgTree,
                                            oldSuperiorOrganogram.orgTree.substring(0, oldSuperiorOrganogram.orgTree.length() - 1) + "$",
                                            dto.lastModificationTime, dto.modifiedBy, oldDTO.superiorDesignationId);
                                    logger.debug(updateOldParent);
                                    try {
                                        st.executeUpdate(updateOldParent);
                                    } catch (SQLException e) {
                                        e.printStackTrace();
                                        ar.set(e);
                                    }
                                }
                                Utils.addToRepoList(getTableName());
                            }, CommonDAOService.CONNECTION_THREAD_LOCAL.get());

                        } catch (SQLException e) {
                            e.printStackTrace();
                            ar.set(e);
                        }
                    }, CommonDAOService.CONNECTION_THREAD_LOCAL.get(), sql)
            );

        }

        boolean ofcChange = dto.officeUnitId != oldDTO.officeUnitId;

        boolean designationChange = !dto.designationEng.equals(oldDTO.designationEng) ||
                !dto.designationBng.equals(oldDTO.designationBng);

        if (designationChange) {
            EmployeeOfficesDAO.getInstance().updateDesignationByOfcUnitOrganogram(dto, ofcChange);
        } else if (ofcChange) {
            EmployeeOfficesDAO.getInstance().updateOfficeByOfcUnitOrganogramIdAndOfficeUnitId(dto);
        }
        return dto.iD;
    }

    public List<KeyCountDTO> getUnitWiseCount() {
        String sql = "SELECT \r\n" +
                "    office_unit_id, COUNT(id),\r\n" +
                "SUM(CASE\r\n" +
                "        WHEN office_unit_organograms.isVacant = 1 THEN 1\r\n" +
                "        ELSE 0\r\n" +
                "    END) vacantCount," +
                "SUM(CASE\r\n" +
                "        WHEN office_unit_organograms.isVacant = 0 THEN 1\r\n" +
                "        ELSE 0\r\n" +
                "    END) assignedCount\r\n" +
                "FROM\r\n" +
                "    office_unit_organograms\r\n" +
                "WHERE\r\n" +
                "    isDeleted = 0 \r\n" +
                "GROUP BY office_unit_id;";

        return ConnectionAndStatementUtil.getListOfT(sql, this::getUnitWiseCount);
    }

    public KeyCountDTO getUnitWiseCount(ResultSet rs) {
        try {
            KeyCountDTO keyCountDTO = new KeyCountDTO();
            keyCountDTO.key = rs.getLong("office_unit_id");
            keyCountDTO.count = rs.getInt("count(id)");
            keyCountDTO.count2 = rs.getInt("vacantCount");
            keyCountDTO.count3 = rs.getInt("assignedCount");

            return keyCountDTO;
        } catch (SQLException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public Map<Long, KeyCountDTO> getWingWiseCount() {
        List<KeyCountDTO> unitWiseCount = getUnitWiseCount();
        Map<Long, KeyCountDTO> wingWiseCount = new HashMap<>();
        for (KeyCountDTO keyCountDTO : unitWiseCount) {
            WingModel wing = WorkflowController.getWingFromOfficeUnitId(keyCountDTO.key);
            KeyCountDTO wingKey = wingWiseCount.get(wing.id);
            if (wingKey == null) {
                if (wing.nameEn.toLowerCase().contains("wing") && wing.id != SessionConstants.MP_OFFICE) {
                    wingKey = new KeyCountDTO();
                    wingKey.key = wing.id;
                    wingKey.count = 0;
                    wingKey.count2 = 0;
                    wingKey.count3 = 0;
                    wingKey.nameEn = wing.nameEn;
                    wingKey.nameBn = wing.nameBn;
                    wingWiseCount.put(wing.id, wingKey);
                }
            }
            if (wingKey != null) {
                wingKey.count += keyCountDTO.count;
                wingKey.count2 += keyCountDTO.count2;
                wingKey.count3 += keyCountDTO.count3;
            }
        }

        logger.debug("Fetching wing wise Count");
        for (Map.Entry<Long, KeyCountDTO> entry : wingWiseCount.entrySet()) {
            logger.debug(entry.getKey() + ": " + entry.getValue().nameEn + " " + entry.getValue().count);
        }
        return wingWiseCount;
    }

    @Override
    public long executeAddOrUpdateQuery(Office_unit_organogramDTO t, String sql, boolean isInsert) throws Exception {
        Office_unit_organogramDTO oldT = null;
        if (isInsert) {
            t.iD = DBMW.getInstance().getNextSequenceId(getTableName());
        } else {
            oldT = getDTOFromID(t.iD);
        }
        sql = sql.replace("{tableName}", getTableName());
        Office_unit_organogramDTO finalOldT = oldT;
        OfficeUnitOrganograms superiorOrganogram = OfficeUnitOrganogramsRepository.getInstance().getById(t.superiorDesignationId);
        if (superiorOrganogram == null) {
            return -1;
        }
        if (superiorOrganogram.orgTree.endsWith("$")) {
            t.orgTree = superiorOrganogram.orgTree.substring(0, superiorOrganogram.orgTree.length() - 1) + "|" + t.iD + "$";
        } else {
            t.orgTree = superiorOrganogram.orgTree + t.iD + "$";
        }
        return (long) ConnectionAndStatementUtil.getWriteConnectionAndPrepareStatement(obj -> {
            PreparedStatement ps = (PreparedStatement) obj.getStatement();
            long returnId = -1L;
            long lastModificationTime = System.currentTimeMillis();
            try {
                set(ps, t, isInsert);
                t.lastModificationTime = lastModificationTime;
                logger.debug(ps);
                if (isInsert) {
                    ps.execute();
                } else {
                    ps.executeUpdate();
                }
                returnId = t.iD;
                recordUpdateTime(obj.getConnection(), getTableName(), lastModificationTime);
                AuditLogDAO.add(finalOldT, t, isInsert ? LogEvent.ADD : LogEvent.UPDATE, getTableName(), HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().ipAddress,
                        HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO.ID, HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO.organogramID);
                if (superiorOrganogram.orgTree.endsWith("$")) {
                    logger.debug("Before update parent orgTree : " + superiorOrganogram.orgTree);
                    String orgTreeParent = superiorOrganogram.orgTree.substring(0, superiorOrganogram.orgTree.length() - 1) + "|";
                    Office_unit_organogramDTO oldParent = getDTOFromID(superiorOrganogram.id);
                    long lastModificationTime2 = System.currentTimeMillis();
                    ConnectionAndStatementUtil.getWriteStatement(st -> {
                        String sql2 = String.format(updateOrgTree, orgTreeParent, lastModificationTime2, t.modifiedBy, superiorOrganogram.id);
                        logger.debug(sql2);
                        try {
                            st.executeUpdate(sql2);
                        } catch (SQLException ex) {
                            logger.error(ex);
                        }
                    });
                    recordUpdateTime(obj.getConnection(), getTableName(), lastModificationTime2);
                    Office_unit_organogramDTO newParent = getDTOFromID(superiorOrganogram.id);
                    AuditLogDAO.add(newParent, oldParent, LogEvent.UPDATE, getTableName(), HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().ipAddress,
                            HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO.ID, HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO.organogramID);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return returnId;
        }, sql);
    }

    @Override
    public String getLastModifierUser() {
        return "modified_by";
    }

    public List<Office_unit_organogramDTO> getAllActiveDTOs() {
        return getDTOs(getAllActiveDTOs, this::buildObjectFromResultSet);
    }

    public List<Office_unit_organogramDTO> getAllActiveDTOs(Predicate<Office_unit_organogramDTO> filterFunction) {
        return getAllActiveDTOs().stream()
                .filter(filterFunction)
                .collect(Collectors.toList());
    }

    public List<Office_unit_organogramDTO> getAllActiveDTOs(Long officeUnitId, String designationName) {
        Predicate<Office_unit_organogramDTO> filterFunction = dto -> {
            boolean isValid = true;
            if (officeUnitId != null && officeUnitId > 0) {
                isValid = (dto.officeUnitId == officeUnitId);
            }
            if (designationName != null && !designationName.isEmpty()) {
                isValid &= SameDesignationGroup.getDesignationKey(dto.designationEng).equals(designationName);
            }
            return isValid;
        };
        return getAllActiveDTOs(filterFunction);
    }

    private static final String updateLayersSQL =
            "UPDATE office_unit_organograms SET layer_1 = %d, layer_2 = %d, lastModificationTime = %d,"
                    .concat("modified_by = %d WHERE ID = %d");

    public void updateLayers(long id, int layer1, int layer2, long modifiedBy, long modificationTime) {
        String sql = String.format(updateLayersSQL, layer1, layer2, modificationTime, modifiedBy, id);
        ConnectionAndStatementUtil.getWriteConnectionAndStatement(model -> {
            try {
                model.getStatement().executeUpdate(sql);
                recordUpdateTime(model.getConnection(), getTableName(), modificationTime);
                return true;
            } catch (SQLException ex) {
                ex.printStackTrace();
                return false;
            }
        });
    }


    private static final String updateVacancySQL =
            "UPDATE office_unit_organograms SET isVacant=%d, assignedEmployeeRecordId=%d, lastModificationTime = %d,"
                    .concat("modified_by = %d WHERE ID = %d");

    public void updateVacancy(long id, long employeeRecordId, boolean isVacant, long modifiedBy, long modificationTime) throws Exception {
        String sql = String.format(updateVacancySQL, (isVacant ? 1 : 0), employeeRecordId, modificationTime, modifiedBy, id);
        updateVacancy(sql, modificationTime);
    }

    private void updateVacancy(String sql, long modificationTime) throws Exception {
        if (CommonDAOService.CONNECTION_THREAD_LOCAL.get() != null) {
            execute(sql, modificationTime);
            Utils.addToRepoList(getTableName());
        } else {
            Utils.handleTransaction(() -> {
                execute(sql, modificationTime);
                Utils.runExactTimeRepoMethod(getTableName(), modificationTime);
            });
        }
    }

    private void execute(String sql, long lastModificationTime) throws Exception {
        Utils.wrapWithAtomicReference(ar ->
                ConnectionAndStatementUtil.getWriteStatement(st -> {
                    try {
                        logger.debug(sql);
                        st.executeUpdate(sql);
                        recordUpdateTime(CommonDAOService.CONNECTION_THREAD_LOCAL.get(), getTableName(), lastModificationTime);
                    } catch (SQLException ex) {
                        ex.printStackTrace();
                        ar.set(ex);
                    }
                }, CommonDAOService.CONNECTION_THREAD_LOCAL.get())
        );
    }

    private static final String updateVacancySQLByList =
            "UPDATE office_unit_organograms SET isVacant=%d, assignedEmployeeRecordId=%d, lastModificationTime = %d,"
                    .concat("modified_by = %d WHERE ID IN (%s)");

    public void updateVacancyList(List<Long> list, long employeeRecordId, boolean isVacant, long modifiedBy, long modificationTime) throws Exception {
        Optional<String> ids = StringUtils.getCommaSeparatedStringFromLongList(list);
        if (ids.isPresent()) {
            String sql = String.format(updateVacancySQLByList, (isVacant ? 1 : 0), employeeRecordId, modificationTime, modifiedBy, ids.get());
            updateVacancy(sql, modificationTime);
        }
    }

    private static final String updateVacancyAllSQL =
            "UPDATE office_unit_organograms SET isVacant=1, lastModificationTime = %d "
                    .concat(" WHERE assignedEmployeeRecordId = %d");

    public void updateVacancyALl(long employeeRecordId) {
        long modificationTime = System.currentTimeMillis();
        String sql = String.format(updateVacancyAllSQL,modificationTime, employeeRecordId);
        logger.info(sql);
        ConnectionAndStatementUtil.getWriteConnectionAndStatement(model -> {
            try {
                model.getStatement().executeUpdate(sql);
                recordUpdateTime(model.getConnection(), getTableName(), modificationTime);
                return true;
            } catch (SQLException ex) {
                logger.error(ex);
                return false;
            }
        });
    }

    public List<Office_unit_organogramDTO> getDistinctNames(boolean isLangEng) {
        String sql = "SELECT \r\n" +
                "    designation_bng, designation_eng\r\n" +
                "FROM\r\n" +
                "    office_unit_organograms\r\n" +
                "WHERE\r\n" +
                "    designation_bng != '' AND isDeleted = 0\r\n" +
                "GROUP BY designation_bng ";
        if (isLangEng) {
            sql += " order by designation_eng asc";
        } else {
            sql += " order by designation_bng asc";
        }
        return ConnectionAndStatementUtil.getListOfT(sql, this::getNames);
    }


    public Office_unit_organogramDTO getNames(ResultSet rs) {
        try {
            Office_unit_organogramDTO office_unit_organogramDTO = new Office_unit_organogramDTO();

            office_unit_organogramDTO.designationEng = rs.getString("designation_eng");
            office_unit_organogramDTO.designationBng = rs.getString("designation_bng");

            return office_unit_organogramDTO;
        } catch (SQLException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public List<Office_unit_organogramDTO> getByOfficeUnitId(long id) {
        return ConnectionAndStatementUtil.getListOfT(String.format(getByOfficeUnitId, id), this::buildObjectFromResultSet);
    }

    public void updateOrder(List<Long> idList) throws Exception {
        Connection connection = ConnectionAndStatementUtil.getConnection(ConnectionType.WRITE);
        AtomicReference<Exception> exceptionAtomicReference = new AtomicReference<>();
        long lastModificationTime = System.currentTimeMillis() + 300000;//5minutes delay for cache updating
        ConnectionAndStatementUtil.getWriteStatement(st -> {
            String sql;
            try {
                connection.setAutoCommit(false);
                for (int i = 1; i <= idList.size(); i++) {
                    sql = String.format(updateOrder, i, lastModificationTime, idList.get(i - 1));
                    st.executeUpdate(sql);
                    logger.debug(sql);
                }
                connection.commit();
                connection.setAutoCommit(true);
            } catch (Exception e) {
                e.printStackTrace();
                exceptionAtomicReference.set(e);
                try {
                    connection.rollback();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }, connection);
        if (connection != null) {
            connection.close();
        }
        if (exceptionAtomicReference.get() != null) {
            throw exceptionAtomicReference.get();
        }
        recordUpdateTime(getTableName(), lastModificationTime);
        Utils.runIOTaskInASeparateThread(() -> () -> OfficeUnitOrganogramsRepository.getInstance().updatingCacheByIdList(idList));
    }

    public void updateOrgTreeByChangingParent(long officeUnitId, long newParentNode, long lastModificationTime) throws Exception {
        boolean isLangEng = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
        Office_unitsDTO newParentOfficeDTO = Office_unitsRepository.getInstance().getConcreteParentIncludingSelf(newParentNode);
        if (newParentOfficeDTO == null) {
            logger.error(String.format("A concrete parent office is not found, id : %d", newParentNode));
            throw new Exception(isLangEng ? "A concrete parent office is not found" : "প্রধান দপ্তর পাওয়া যায়নি");
        }
        OfficeUnitOrganograms newOfcHeadOrganograms = OfficeUnitOrganogramsRepository.getInstance()
                .getOfficeUnitHeadWithActiveEmpStatusAndConsiderSubstitute(newParentOfficeDTO.iD);

        if (newOfcHeadOrganograms == null) {
            logger.error(String.format("Head of office is not found, id : %d", newParentOfficeDTO.iD));
            throw new Exception(isLangEng ? "Head of office is not found" : "দপ্তর প্রধান পাওয়া যায়নি");
        }

        Office_unitsDTO officeUnitsDTO = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(officeUnitId);
        if (officeUnitsDTO == null) {
            logger.error(String.format("Office is not found, id : %d", officeUnitId));
            throw new Exception(isLangEng ? "office is not found" : "দপ্তর পাওয়া যায়নি");
        }

        List<Office_unitsDTO> movedOfficeList;
        if (officeUnitsDTO.isAbstractOffice) {
            movedOfficeList = Office_unitsRepository.getInstance().getConcreteChildOffices(officeUnitsDTO);
        } else {
            movedOfficeList = Collections.singletonList(officeUnitsDTO);
        }

        List<OfficeUnitOrganograms> organogramsList = movedOfficeList
                .stream()
                .flatMap(e -> OfficeUnitOrganogramsRepository.getInstance()
                        .getOfficeUnitHeadList(e.iD, false, false).stream())
                .collect(Collectors.toList());

        if (organogramsList.size() == 0) {
            logger.debug("No Office unit organogram is found to update org_tree");
            return;
        }

        Utils.wrapWithAtomicReference(ar ->
                ConnectionAndStatementUtil.getWriteStatement(st -> {
                    for (OfficeUnitOrganograms organograms : organogramsList) {
                        String org_tree = newOfcHeadOrganograms.orgTree.substring(0, newOfcHeadOrganograms.orgTree.length() - 1) + "|" + organograms.id + organograms.orgTree.substring(organograms.orgTree.length() - 1);
                        try {
                            String sql = String.format(updateOrgTreeByAndParentId, org_tree, newOfcHeadOrganograms.id, lastModificationTime, organograms.id);
                            logger.debug(sql);
                            int x = st.executeUpdate(sql);
                            logger.debug("Updated count : " + x);
                            if (org_tree.charAt(org_tree.length() - 1) == '|') {
                                sql = String.format(updateOrgTreeByOrgTreePrefix2, organograms.orgTree, org_tree, lastModificationTime);
                                sql = sql.replace("{prefix}", organograms.orgTree + "%");
                                logger.debug(sql);
                                x = st.executeUpdate(sql);
                                logger.debug("Updated count : " + x);
                            }
                            sql = String.format(updateSuperiorUnitIdByOfficeUnitId, newParentNode, lastModificationTime, officeUnitId);
                            logger.debug(sql);
                            x = st.executeUpdate(sql);
                            logger.debug("Updated count : " + x);
                            Utils.addToRepoList(getTableName());
                            recordUpdateTime(CommonDAOService.CONNECTION_THREAD_LOCAL.get(), getTableName(), lastModificationTime);
                        } catch (SQLException e) {
                            e.printStackTrace();
                            ar.set(e);
                            break;
                        }
                    }
                }, CommonDAOService.CONNECTION_THREAD_LOCAL.get())
        );
    }
}