package admit_card;
import java.util.*; 
import util.*; 


public class Admit_cardDTO extends CommonDTO
{

	public long recruitmentJobDescriptionType = 0;
    public String placeOfExam = "";
	public long dateOfExam = 0;
    public String timeOfExam = "10:00 AM";
	public String endTimeOfExam = "10:00 AM";
	public long rollNoFrom = 0;
	public long rollNoTo = 0;
	public long employeeRecordsId = 0;
	public long post_id = 0;
	public long unit_id = 0;
	public long insertedByUserId = 0;
	public long insertionDate = 0;
    public String modifiedBy = "";
	public int level = 0;
	public boolean range = false;
	public String signatory_name = "";
	public String signatory_name_bn = "";
	public String signatory_office_unit_name = "";
	public String signatory_office_unit_name_bn = "";
	public String signatory_post_name = "";
	public String signatory_post_name_bn = "";
	public String rules = "";
	public long recruitmentTestName;
	
	
    @Override
	public String toString() {
            return "$Admit_cardDTO[" +
            " iD = " + iD +
            " recruitmentJobDescriptionType = " + recruitmentJobDescriptionType +
            " placeOfExam = " + placeOfExam +
            " dateOfExam = " + dateOfExam +
            " timeOfExam = " + timeOfExam +
            " rollNoFrom = " + rollNoFrom +
            " rollNoTo = " + rollNoTo +
            " employeeRecordsId = " + employeeRecordsId +
            " insertedByUserId = " + insertedByUserId +
            " insertionDate = " + insertionDate +
            " modifiedBy = " + modifiedBy +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
			" range = " + range +
            "]";
    }

}