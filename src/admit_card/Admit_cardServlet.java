package admit_card;

import am_other_office_unit.Am_other_office_unitDTO;
import am_other_office_unit.Am_other_office_unitRepository;
import com.google.gson.Gson;
import common.ApiResponse;
import employee_assign.EmployeeSearchModel;
import employee_records.Employee_recordsDTO;
import employee_records.Employee_recordsRepository;
import files.FilesDAO;
import job_applicant_application.Job_applicant_applicationDAO;
import job_applicant_application.Job_applicant_applicationDTO;
import job_applicant_application.Job_applicant_applicationRepository;
import job_applicant_photo_signature.Job_applicant_photo_signatureDAO;
import login.LoginDTO;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import parliament_job_applicant.Parliament_job_applicantDTO;
import parliament_job_applicant.Parliament_job_applicantRepository;
import pb.CatRepository;
import pb.OptionDTO;
import permission.MenuConstants;
import recruitment_job_description.Recruitment_job_descriptionDAO;
import recruitment_job_description.Recruitment_job_descriptionDTO;
import recruitment_job_description.Recruitment_job_descriptionRepository;
import recruitment_job_specific_exam_type.JobSpecificExamTypeRepository;
import recruitment_job_specific_exam_type.RecruitmentJobSpecificExamTypeDTO;
import role.PermissionRepository;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;
import util.CommonRequestHandler;
import util.RecordNavigationManager4;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;




/**
 * Servlet implementation class Admit_cardServlet
 */
@WebServlet("/Admit_cardServlet")
@MultipartConfig
public class Admit_cardServlet extends HttpServlet
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Admit_cardServlet.class);

    String tableName = "admit_card";

	Admit_cardDAO admit_cardDAO;
	CommonRequestHandler commonRequestHandler;
    private final Gson gson = new Gson();

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Admit_cardServlet()
	{
        super();
    	try
    	{
			admit_cardDAO = new Admit_cardDAO(tableName);
			commonRequestHandler = new CommonRequestHandler(admit_cardDAO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		System.out.println("In doget request = " + request);
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		try
		{
			String actionType = request.getParameter("actionType");
			if(actionType.equals("getAddPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ADMIT_CARD_ADD))
				{
					commonRequestHandler.getAddPage(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getEditPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ADMIT_CARD_ADD))
				{
					getAdmit_card(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getURL"))
			{
				String URL = request.getParameter("URL");
				System.out.println("URL = " + URL);
				response.sendRedirect(URL);
			}
			else if(actionType.equals("search"))
			{
				System.out.println("search requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ADMIT_CARD_SEARCH))
				{
					if(isPermanentTable)
					{
						String filter = request.getParameter("filter");
						System.out.println("filter = " + filter);
						if(filter!=null)
						{
							filter = ""; //shouldn't be directly used, rather manipulate it.
							searchAdmit_card(request, response, isPermanentTable, filter);
						}
						else
						{
							searchAdmit_card(request, response, isPermanentTable, "");
						}
					}
					else
					{
						//searchAdmit_card(request, response, tempTableName, isPermanentTable);
					}
				}
			}
			else if (actionType.equals("getPrintPage")) {
				if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ADMIT_CARD_ADD)) {
					printAdmit_card(request, response);
				} else {
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}

			else if (actionType.equals("getAttendancePage")) {
				if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ADMIT_CARD_ADD)) {
					printAttendance(request, response);
				} else {
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}

			else if (actionType.equals("getAttendancePageForSpecificExam")) {
				if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ADMIT_CARD_ADD)) {
					printAttendanceForSpecificExam(request, response);
				} else {
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}

			else if (actionType.equals("checkPrintEligibility")) {
				checkPrintEligibility(request, response);
			}

			else if (actionType.equals("checkPrintEligibilityForSpecificExam")) {
				checkPrintEligibilityForSpecificExam(request, response);
			}

			else if(actionType.equals("prevDataLoad")){
				long jobId = Long.parseLong(request.getParameter("jobId"));
//				List<Long> rollNumbers = new Job_applicant_applicationDAO();.getDTOsByJobIDAndLevel(jobId, 0)
				List<Long> rollNumbers = Job_applicant_applicationRepository.getInstance()
						.getJob_applicant_applicationDTOByjob_id(jobId)
								.stream()
								.filter(i -> i.isSelected == 1 && i.level == 0)
								.map(i -> Long.parseLong(i.rollNumber))
								.collect(Collectors.toList());
				long min = 0;
				long max = 0;

				if(rollNumbers.size() > 0){
					min = rollNumbers.stream().mapToLong(v -> v).min().orElseThrow(NoSuchElementException::new);
					max = rollNumbers.stream().mapToLong(v -> v).max().orElseThrow(NoSuchElementException::new);
				}

				StringBuilder string = new StringBuilder().append(" From ").append(min).append(" To ").append(max);
				PrintWriter out = response.getWriter();
				out.print(string);
				out.flush();
			}

			else if(actionType.equals("admitCardExists")){
				long jobId = Long.parseLong(request.getParameter("jobId"));
				List<Admit_cardDTO> dtos = Admit_cardRepository.getInstance().getAdmit_cardDTOByjob_id(jobId);
//						admit_cardDAO.getAllAdmit_cardByJobID(jobId);

				boolean flag = false;
				long currentTime = System.currentTimeMillis();
				if(dtos.size() > 0){
					flag = true;
					for(Admit_cardDTO admit_cardDTO: dtos){
						if(admit_cardDTO.dateOfExam > currentTime){
							flag = false;
							break;
						}
					}
				}

				PrintWriter out = response.getWriter();
				out.print(flag);
				out.flush();
			}

			else if(actionType.equals("getAllOtherOffices")){

				OptionDTO optionDTO;
				Recruitment_job_descriptionDAO recruitment_job_descriptionDAO = new Recruitment_job_descriptionDAO();
				List<Recruitment_job_descriptionDTO> recruitment_job_descriptionDTOS = recruitment_job_descriptionDAO.
						getAllRecruitment_job_descriptionForCreatingAdmitCard();

				List<OptionDTO> OptionDTOList = new ArrayList<>();

				for (Recruitment_job_descriptionDTO dto : recruitment_job_descriptionDTOS) {

					optionDTO = new OptionDTO(dto.jobTitleEn, dto.jobTitleBn, dto.iD + "");
					OptionDTOList.add(optionDTO);
				}
				response.getWriter().write(gson.toJson(OptionDTOList));
				response.getWriter().flush();
				response.getWriter().close();
			}


			else if(actionType.equals("view"))
			{
				System.out.println("view requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ADMIT_CARD_SEARCH))
				{
					commonRequestHandler.view(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}
			else
			{
				request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}





	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		// TODO Auto-generated method stub
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		System.out.println("doPost");
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);

		try
		{
			String actionType = request.getParameter("actionType");
			System.out.println("actionType = " + actionType);
			if(actionType.equals("add"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ADMIT_CARD_ADD))
				{
					System.out.println("going to  addAdmit_card ");
					addAdmit_card(request, response, true, userDTO, true);
				}
				else
				{
					System.out.println("Not going to  addAdmit_card ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}

			else if(actionType.equals("getDTO"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ADMIT_CARD_ADD))
				{
					getDTO(request, response);
				}
				else
				{
					System.out.println("Not going to  addAdmit_card ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}
			else if(actionType.equals("edit"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ADMIT_CARD_ADD))
				{
					addAdmit_card(request, response, false, userDTO, isPermanentTable);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("delete"))
			{
				deleteAdmit_card(request, response, userDTO);
			}
			else if(actionType.equals("search"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ADMIT_CARD_SEARCH))
				{
					searchAdmit_card(request, response, true, "");
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getGeo"))
			{
				System.out.println("going to geoloc ");
				request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
			}

		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}

	private void getDTO(HttpServletRequest request, HttpServletResponse response)
	{
		try
		{
			System.out.println("In getDTO");
			Admit_cardDTO admit_cardDTO = Admit_cardRepository.getInstance().getAdmit_cardDTOByID
					(Long.parseLong(request.getParameter("ID")));
//					(Admit_cardDTO)admit_cardDAO.getDTOByID(Long.parseLong(request.getParameter("ID")));
			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");

			String encoded = this.gson.toJson(admit_cardDTO);
			System.out.println("json encoded data = " + encoded);
			out.print(encoded);
			out.flush();
		}
		catch (NumberFormatException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void addAdmit_card(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, boolean isPermanentTable) throws IOException
	{
		// TODO Auto-generated method stub
		ApiResponse apiResponse;
		try
		{
			request.setAttribute("failureMessage", "");
			System.out.println("%%%% addAdmit_card");
			String path = getServletContext().getRealPath("/img2/");
			Admit_cardDTO admit_cardDTO;
			SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");

			if(addFlag == true)
			{
				admit_cardDTO = new Admit_cardDTO();
			}
			else
			{
				admit_cardDTO = Admit_cardRepository.getInstance().getAdmit_cardDTOByID
						(Long.parseLong(request.getParameter("iD")));
//						(Admit_cardDTO)admit_cardDAO.getDTOByID(Long.parseLong(request.getParameter("iD")));
			}

			String Value = "";

			Value = request.getParameter("recruitmentJobDescriptionType");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("recruitmentJobDescriptionType = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{

				admit_cardDTO.recruitmentJobDescriptionType = Long.parseLong(Value);
				if(admit_cardDTO.recruitmentJobDescriptionType <= 0){
					throw new Exception(" Invalid job");
				}
			}
			else
			{
				throw new Exception(" Invalid job");
			}


			Value = request.getParameter("recruitmentTestNameId");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("recruitmentTestNameId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{

				admit_cardDTO.recruitmentTestName = Long.parseLong(Value);
				if(admit_cardDTO.recruitmentTestName <= 0){
					throw new Exception(" Invalid Test");
				}
			}
			else
			{
				throw new Exception(" Invalid job");
			}


			Value = request.getParameter("placeOfExam");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("placeOfExam = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{

				admit_cardDTO.placeOfExam = (Value);
			}
			else
			{
				throw new Exception(" Invalid place Of Exam");
			}


			Value = request.getParameter("rules");


			System.out.println("rules = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{

				admit_cardDTO.rules = (Value);
			}
			else
			{
				System.out.println("rules has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("dateOfExam");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("dateOfExam = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{

					Date d = f.parse(Value);
					admit_cardDTO.dateOfExam = d.getTime();

			}
			else
			{
				throw new Exception(" Invalid date of exam");			}

			Value = request.getParameter("timeOfExam");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("timeOfExam = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{

				admit_cardDTO.timeOfExam = (Value);
			}
			else
			{
				throw new Exception(" Invalid time Of Exam");
			}

			Value = request.getParameter("endTimeOfExam");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("endTimeOfExam = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{

				admit_cardDTO.endTimeOfExam = (Value);
			}
			else
			{
				throw new Exception(" Invalid end time Of Exam");
			}


			Value = request.getParameter("range");

            admit_cardDTO.range = Value != null;

			Value = request.getParameter("rollNoFrom");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("rollNoFrom = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{

				admit_cardDTO.rollNoFrom = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("level");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("level = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{

				admit_cardDTO.level = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("level has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("rollNoTo");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("rollNoTo = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{

				admit_cardDTO.rollNoTo = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("employeeRecordsId");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("employeeRecordsId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{

//				admit_cardDTO.employeeRecordsId = Long.parseLong(Value);
				List<EmployeeSearchModel> models = Arrays.asList(gson.fromJson(Value, EmployeeSearchModel[].class));
				if(models.size() > 0){
					admit_cardDTO.employeeRecordsId = models.get(0).employeeRecordId;
					admit_cardDTO.unit_id = models.get(0).officeUnitId;
					admit_cardDTO.post_id = models.get(0).organogramId;
					admit_cardDTO.signatory_name = models.get(0).employeeNameEn;
					admit_cardDTO.signatory_name_bn = models.get(0).employeeNameBn;
					admit_cardDTO.signatory_post_name = models.get(0).organogramNameEn;
					admit_cardDTO.signatory_post_name_bn = models.get(0).organogramNameBn;
					admit_cardDTO.signatory_office_unit_name = models.get(0).officeUnitNameEn;
					admit_cardDTO.signatory_office_unit_name_bn = models.get(0).officeUnitNameBn;
				}
			}
			else
			{
				throw new Exception(" Invalid signatory person");
			}

			if(addFlag)
			{
				admit_cardDTO.insertedByUserId = userDTO.ID;
			}


			if(addFlag)
			{
				Calendar c = Calendar.getInstance();
				c.set(Calendar.HOUR_OF_DAY, 0);
				c.set(Calendar.MINUTE, 0);
				c.set(Calendar.SECOND, 0);
				c.set(Calendar.MILLISECOND, 0);

				admit_cardDTO.insertionDate = c.getTimeInMillis();
			}


			Value = request.getParameter("modifiedBy");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("modifiedBy = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{

				admit_cardDTO.modifiedBy = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			System.out.println("Done adding  addAdmit_card dto = " + admit_cardDTO);
			long returnedID = -1;

			if(isPermanentTable == false) //add new row for validation and make the old row outdated
			{
				admit_cardDAO.setIsDeleted(admit_cardDTO.iD, CommonDTO.OUTDATED);
				returnedID = admit_cardDAO.add(admit_cardDTO);
				admit_cardDAO.setIsDeleted(returnedID, CommonDTO.WAITING_FOR_APPROVAL);
			}
			else if(addFlag == true)
			{
				returnedID = admit_cardDAO.manageWriteOperations(admit_cardDTO, SessionConstants.INSERT, -1, userDTO);
			}
			else
			{
				returnedID = admit_cardDAO.manageWriteOperations(admit_cardDTO, SessionConstants.UPDATE, -1, userDTO);
			}




			apiResponse = ApiResponse.makeSuccessResponse("Admit_cardServlet?actionType=search");


		}
		catch (Exception e)
		{
			e.printStackTrace();
			apiResponse = ApiResponse.makeErrorResponse(e.getMessage());
		}

		PrintWriter pw = response.getWriter();
		pw.write(apiResponse.getJSONString());
		pw.flush();
		pw.close();
	}









	private void deleteAdmit_card(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO) throws IOException
	{
		try
		{
			String[] IDsToDelete = request.getParameterValues("ID");
			for(int i = 0; i < IDsToDelete.length; i ++)
			{
				long id = Long.parseLong(IDsToDelete[i]);
				System.out.println("------ DELETING " + IDsToDelete[i]);


				Admit_cardDTO admit_cardDTO = Admit_cardRepository.getInstance().getAdmit_cardDTOByID(id);
//						(Admit_cardDTO)admit_cardDAO.getDTOByID(id);
				admit_cardDAO.manageWriteOperations(admit_cardDTO, SessionConstants.DELETE, id, userDTO);
				response.sendRedirect("Admit_cardServlet?actionType=search");

			}
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}

	}

	private void getAdmit_card(HttpServletRequest request, HttpServletResponse response, long id) throws ServletException, IOException
	{
		System.out.println("in getAdmit_card");
		Admit_cardDTO admit_cardDTO = null;
		try
		{
			admit_cardDTO = Admit_cardRepository.getInstance().getAdmit_cardDTOByID(id);
//					(Admit_cardDTO)admit_cardDAO.getDTOByID(id);
			request.setAttribute("ID", admit_cardDTO.iD);
			request.setAttribute("admit_cardDTO",admit_cardDTO);
			request.setAttribute("admit_cardDAO",admit_cardDAO);

			String URL= "";

			String inPlaceEdit = request.getParameter("inplaceedit");
			String inPlaceSubmit = request.getParameter("inplacesubmit");
			String getBodyOnly = request.getParameter("getBodyOnly");

			if(inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase(""))
			{
				URL = "admit_card/admit_cardInPlaceEdit.jsp";
				request.setAttribute("inplaceedit","");
			}
			else if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
			{
				URL = "admit_card/admit_cardSearchRow.jsp";
				request.setAttribute("inplacesubmit","");
			}
			else
			{
				if(getBodyOnly  != null && !getBodyOnly.equalsIgnoreCase(""))
				{
					URL = "admit_card/admit_cardEditBody.jsp?actionType=edit";
				}
				else
				{
					URL = "admit_card/admit_cardEdit.jsp?actionType=edit";
				}
			}

			RequestDispatcher rd = request.getRequestDispatcher(URL);
			rd.forward(request, response);
		}
		catch (NumberFormatException e)
		{
			e.printStackTrace();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}


	private void getAdmit_card(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		getAdmit_card(request, response, Long.parseLong(request.getParameter("ID")));
	}

	private void searchAdmit_card(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException
	{
		System.out.println("in  searchAdmit_card 1");
		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String ajax = request.getParameter("ajax");
		boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);

        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
			SessionConstants.NAV_ADMIT_CARD,
			request,
			admit_cardDAO,
			SessionConstants.VIEW_ADMIT_CARD,
			SessionConstants.SEARCH_ADMIT_CARD,
			tableName,
			isPermanent,
			userDTO,
			filter,
			true);

		//System.out.println("SessionConstants.NAV_ADMIT_CARD: "+SessionConstants.NAV_ADMIT_CARD+" ,tableName: "+tableName+" ,isPermanent: "+isPermanent+" filter: "+filter);
        try
        {
			System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        }
        catch(Exception e)
        {
			System.out.println("failed to dojob" + e);
        }

		request.setAttribute("admit_cardDAO",admit_cardDAO);
        RequestDispatcher rd;
        if(!isPermanent)
        {
        	if(hasAjax == false)
	        {
	        	System.out.println("Going to admit_card/admit_cardApproval.jsp");
	        	rd = request.getRequestDispatcher("admit_card/admit_cardApproval.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to admit_card/admit_cardApprovalForm.jsp");
	        	rd = request.getRequestDispatcher("admit_card/admit_cardApprovalForm.jsp");
	        }
        }
        else
        {
	        if(hasAjax == false)
	        {
	        	System.out.println("Going to admit_card/admit_cardSearch.jsp");
	        	rd = request.getRequestDispatcher("admit_card/admit_cardSearch.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to admit_card/admit_cardSearchForm.jsp");
	        	rd = request.getRequestDispatcher("admit_card/admit_cardSearchForm.jsp");
	        }
        }
		rd.forward(request, response);
		}

	private void checkPrintEligibility(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			Admit_cardDTO admit_cardDTO = Admit_cardRepository.getInstance().getAdmit_cardDTOByID(Long.parseLong(request.getParameter("ID")));
			Job_applicant_applicationDAO job_applicant_applicationDAO = new Job_applicant_applicationDAO();

			long admitLevel = admit_cardDTO.level;
			List<Job_applicant_applicationDTO> job_applicant_applicationDTOS =
					Job_applicant_applicationRepository.getInstance()
							.getJob_applicant_applicationDTOByjob_id(admit_cardDTO.recruitmentJobDescriptionType)
							.stream()
							.filter(i -> i.level == admitLevel && i.isSelected == 1)
							.collect(Collectors.toList());

			if(admit_cardDTO.range){
				long rollNoFrom = admit_cardDTO.rollNoFrom;
				long rollNoTo = admit_cardDTO.rollNoTo;
				job_applicant_applicationDTOS = job_applicant_applicationDTOS.stream().
						filter(i -> Long.parseLong(i.rollNumber) >= rollNoFrom
								&& Long.parseLong(i.rollNumber) <= rollNoTo ).collect(Collectors.toList());
			}

			Boolean flag = false;
			if(job_applicant_applicationDTOS.size() > 0){
				flag = true;
			}

			PrintWriter out = response.getWriter();
			out.print(flag);
			out.flush();
		}

	private void checkPrintEligibilityForSpecificExam(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Admit_cardDTO admit_cardDTO = Admit_cardRepository.getInstance().getAdmit_cardDTOByID(Long.parseLong(request.getParameter("ID")));
		Long curLevel = Long.parseLong(request.getParameter("examOrder"));
		Job_applicant_applicationDAO job_applicant_applicationDAO = new Job_applicant_applicationDAO();

		long admitLevel = admit_cardDTO.level;
//		List<Job_applicant_applicationDTO> job_applicant_applicationDTOS =
//				Job_applicant_applicationRepository.getInstance()
//						.getJob_applicant_applicationDTOByjob_id(admit_cardDTO.recruitmentJobDescriptionType)
//						.stream()
//						.filter(i -> i.level == admitLevel && i.isSelected == 1)
//						.collect(Collectors.toList());

		List<Job_applicant_applicationDTO> job_applicant_applicationDTOS =
				Job_applicant_applicationRepository.getInstance()
						.getJob_applicant_applicationDTOByjob_id(admit_cardDTO.recruitmentJobDescriptionType)
						.stream()
						.filter(i -> i.level == curLevel && !i.isRejected)
						.collect(Collectors.toList());

		if(admit_cardDTO.range){
			long rollNoFrom = admit_cardDTO.rollNoFrom;
			long rollNoTo = admit_cardDTO.rollNoTo;
			job_applicant_applicationDTOS = job_applicant_applicationDTOS.stream().
					filter(i -> Long.parseLong(i.rollNumber) >= rollNoFrom
							&& Long.parseLong(i.rollNumber) <= rollNoTo ).collect(Collectors.toList());
		}

		Boolean flag = false;
		if(job_applicant_applicationDTOS.size() > 0){
			flag = true;
		}

		PrintWriter out = response.getWriter();
		out.print(flag);
		out.flush();
	}

	private void printAdmit_card(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

			Admit_cardDTO admit_cardDTO = null;
			try {
				admit_cardDTO = Admit_cardRepository.getInstance().
						getAdmit_cardDTOByID(Long.parseLong(request.getParameter("ID")));
				request.setAttribute("ID", admit_cardDTO.iD);
				request.setAttribute("admit_cardDTO", admit_cardDTO);
				request.setAttribute("admit_cardDAO", admit_cardDAO);

				Job_applicant_photo_signatureDAO photo_signatureDAO = new Job_applicant_photo_signatureDAO();
				request.setAttribute("photo_signatureDAO", photo_signatureDAO);

				FilesDAO filesDAO = new FilesDAO();
				request.setAttribute("filesDAO", filesDAO);


//				Job_applicant_applicationDAO job_applicant_applicationDAO = new Job_applicant_applicationDAO();
//				Parliament_job_applicantDAO parliament_job_applicantDAO = new Parliament_job_applicantDAO();


				Recruitment_job_descriptionDTO jobDescriptionDTO =
						Recruitment_job_descriptionRepository.getInstance().
								getRecruitment_job_descriptionDTOByID(admit_cardDTO.recruitmentJobDescriptionType);
				request.setAttribute("jobDescriptionDTO", jobDescriptionDTO);


				Employee_recordsDTO employee_recordsDTO = Employee_recordsRepository.getInstance().
						getById(admit_cardDTO.employeeRecordsId);
				request.setAttribute("employee_recordsDTO", employee_recordsDTO);

				request.setAttribute("designation", "");

				long admitLevel = admit_cardDTO.level;

//				List<Job_applicant_applicationDTO> job_applicant_applicationDTOS =
//						Job_applicant_applicationRepository.getInstance()
//								.getJob_applicant_applicationDTOByjob_id(admit_cardDTO.recruitmentJobDescriptionType)
//								.stream()
//								.filter(i -> i.level == admitLevel && i.isSelected == 1)
//								.collect(Collectors.toList());



				/*admit card generation only for written start*/

				List<RecruitmentJobSpecificExamTypeDTO> jobSpecificExamTypeDTOs = JobSpecificExamTypeRepository.getInstance()
						.getRecruitmentJobSpecificExamTypeDTOByjob_id(jobDescriptionDTO.iD);

				for(RecruitmentJobSpecificExamTypeDTO jobSpecificExamTypeDTO : jobSpecificExamTypeDTOs){
					String jobExamType = CatRepository.getName("Bangla", "job_exam_type", jobSpecificExamTypeDTO.jobExamTypeCat);
					if(jobExamType.equals("লিখিত")){
						 admitLevel = jobSpecificExamTypeDTO.order;
						 break;
					}
				}

				long finalAdmitLevel = admitLevel;
				List<Job_applicant_applicationDTO> job_applicant_applicationDTOS =
						Job_applicant_applicationRepository.getInstance()
								.getJob_applicant_applicationDTOByjob_id(admit_cardDTO.recruitmentJobDescriptionType)
								.stream()
								.filter(i -> i.level == finalAdmitLevel && !i.isRejected)
								.collect(Collectors.toList());
				
				/*admit card generation only for written end*/

				if(admit_cardDTO.range){
					long rollNoFrom = admit_cardDTO.rollNoFrom;
					long rollNoTo = admit_cardDTO.rollNoTo;
					job_applicant_applicationDTOS = job_applicant_applicationDTOS.stream().
							filter(i -> Long.parseLong(i.rollNumber) >= rollNoFrom
									&& Long.parseLong(i.rollNumber) <= rollNoTo ).collect(Collectors.toList());
				}

				request.setAttribute("job_applicant_applicationDTOS", job_applicant_applicationDTOS);

				List<Long> jobApplicantIds = job_applicant_applicationDTOS.stream().map(i -> i.jobApplicantId)
						.collect(Collectors.toList());


				List<Parliament_job_applicantDTO> parliament_job_applicantDTOS = new ArrayList<>();
				for(Long appID: jobApplicantIds){
					Parliament_job_applicantDTO dto = Parliament_job_applicantRepository.getInstance().
							getParliament_job_applicantDTOByID(appID);
					if(dto != null){
						parliament_job_applicantDTOS.add(dto);
					}

				}

//				List<Parliament_job_applicantDTO> parliament_job_applicantDTOS = parliament_job_applicantDAO.getDTOs(jobApplicantIds);
				Map<Long, Parliament_job_applicantDTO> job_applicantDTOMap = new HashMap<>(
						parliament_job_applicantDTOS.stream().collect(Collectors.toMap(s ->  s.iD, s -> s))) ;


				request.setAttribute("job_applicantDTOMap", job_applicantDTOMap);
				String URL = "admit_card/admit_cardPrintForm.jsp";

				RequestDispatcher rd = request.getRequestDispatcher(URL);
				rd.forward(request, response);
			} catch (NumberFormatException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}

		}

	private void printAttendance(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		Admit_cardDTO admit_cardDTO = null;
		try {
			admit_cardDTO = Admit_cardRepository.getInstance().
					getAdmit_cardDTOByID(Long.parseLong(request.getParameter("ID")));
			request.setAttribute("ID", admit_cardDTO.iD);
			request.setAttribute("admit_cardDTO", admit_cardDTO);
			request.setAttribute("admit_cardDAO", admit_cardDAO);

			Job_applicant_photo_signatureDAO photo_signatureDAO = new Job_applicant_photo_signatureDAO();
			request.setAttribute("photo_signatureDAO", photo_signatureDAO);

			FilesDAO filesDAO = new FilesDAO();
			request.setAttribute("filesDAO", filesDAO);

			Recruitment_job_descriptionDTO jobDescriptionDTO =
					Recruitment_job_descriptionRepository.getInstance().
							getRecruitment_job_descriptionDTOByID(admit_cardDTO.recruitmentJobDescriptionType);
			request.setAttribute("jobDescriptionDTO", jobDescriptionDTO);

			long admitLevel = admit_cardDTO.level;

			List<Job_applicant_applicationDTO> job_applicant_applicationDTOS =
					Job_applicant_applicationRepository.getInstance()
							.getJob_applicant_applicationDTOByjob_id(admit_cardDTO.recruitmentJobDescriptionType)
							.stream()
							.filter(i -> i.level == admitLevel && i.isSelected == 1)
							.collect(Collectors.toList());

			/*attendance page for all type of exam start*/

//			List<RecruitmentJobSpecificExamTypeDTO> jobSpecificExamTypeDTOs = JobSpecificExamTypeRepository.getInstance()
//					.getRecruitmentJobSpecificExamTypeDTOByjob_id(jobDescriptionDTO.iD);
//
//			for(RecruitmentJobSpecificExamTypeDTO jobSpecificExamTypeDTO : jobSpecificExamTypeDTOs){
//				String jobExamType = CatRepository.getInstance().getName("Bangla", "job_exam_type", jobSpecificExamTypeDTO.jobExamTypeCat);
//				if(jobExamType.equals("লিখিত")){
//					admitLevel = jobSpecificExamTypeDTO.order;
//					break;
//				}
//			}
//
//			long finalAdmitLevel = admitLevel;
//			List<Job_applicant_applicationDTO> job_applicant_applicationDTOS =
//					Job_applicant_applicationRepository.getInstance()
//							.getJob_applicant_applicationDTOByjob_id(admit_cardDTO.recruitmentJobDescriptionType)
//							.stream()
//							.filter(i -> i.level == finalAdmitLevel && !i.isRejected)
//							.collect(Collectors.toList());

			/*attendance page for all type of exam end*/

			if(admit_cardDTO.range){
				long rollNoFrom = admit_cardDTO.rollNoFrom;
				long rollNoTo = admit_cardDTO.rollNoTo;
				job_applicant_applicationDTOS = job_applicant_applicationDTOS.stream().
						filter(i -> Long.parseLong(i.rollNumber) >= rollNoFrom
								&& Long.parseLong(i.rollNumber) <= rollNoTo ).collect(Collectors.toList());
			}

			request.setAttribute("job_applicant_applicationDTOS", job_applicant_applicationDTOS);
			String URL = "admit_card/attendancePrintForm.jsp";

			RequestDispatcher rd = request.getRequestDispatcher(URL);
			rd.forward(request, response);
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void printAttendanceForSpecificExam(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		Admit_cardDTO admit_cardDTO = null;
		try {
			admit_cardDTO = Admit_cardRepository.getInstance().
					getAdmit_cardDTOByID(Long.parseLong(request.getParameter("ID")));

			Long curLevel =  Long.parseLong(request.getParameter("examOrder"));
			request.setAttribute("ID", admit_cardDTO.iD);
			request.setAttribute("admit_cardDTO", admit_cardDTO);
			request.setAttribute("admit_cardDAO", admit_cardDAO);

			Job_applicant_photo_signatureDAO photo_signatureDAO = new Job_applicant_photo_signatureDAO();
			request.setAttribute("photo_signatureDAO", photo_signatureDAO);

			FilesDAO filesDAO = new FilesDAO();
			request.setAttribute("filesDAO", filesDAO);

			Recruitment_job_descriptionDTO jobDescriptionDTO =
					Recruitment_job_descriptionRepository.getInstance().
							getRecruitment_job_descriptionDTOByID(admit_cardDTO.recruitmentJobDescriptionType);
			request.setAttribute("jobDescriptionDTO", jobDescriptionDTO);

			long admitLevel = admit_cardDTO.level;

//			List<Job_applicant_applicationDTO> job_applicant_applicationDTOS =
//					Job_applicant_applicationRepository.getInstance()
//							.getJob_applicant_applicationDTOByjob_id(admit_cardDTO.recruitmentJobDescriptionType)
//							.stream()
//							.filter(i -> i.level == admitLevel && i.isSelected == 1)
//							.collect(Collectors.toList());

			/*attendance page for all type of exam start*/

			List<Job_applicant_applicationDTO> job_applicant_applicationDTOS =
					Job_applicant_applicationRepository.getInstance()
							.getJob_applicant_applicationDTOByjob_id(admit_cardDTO.recruitmentJobDescriptionType)
							.stream()
							.filter(i -> i.level == curLevel && !i.isRejected)
							.collect(Collectors.toList());

			/*attendance page for all type of exam end*/

			if(admit_cardDTO.range){
				long rollNoFrom = admit_cardDTO.rollNoFrom;
				long rollNoTo = admit_cardDTO.rollNoTo;
				job_applicant_applicationDTOS = job_applicant_applicationDTOS.stream().
						filter(i -> Long.parseLong(i.rollNumber) >= rollNoFrom
								&& Long.parseLong(i.rollNumber) <= rollNoTo ).collect(Collectors.toList());
			}

			request.setAttribute("job_applicant_applicationDTOS", job_applicant_applicationDTOS);
			String URL = "admit_card/attendancePrintForm.jsp";

			RequestDispatcher rd = request.getRequestDispatcher(URL);
			rd.forward(request, response);
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}


}

