package admit_card;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import com.google.gson.Gson;
import job_applicant_application.Job_applicant_applicationDTO;
import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;
import vm_route.VmRouteStoppageDTO;


public class Admit_cardRepository implements Repository {
	Admit_cardDAO admit_cardDAO = null;
	Gson gson = new Gson();
	
	public void setDAO(Admit_cardDAO admit_cardDAO)
	{
		this.admit_cardDAO = admit_cardDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Admit_cardRepository.class);
	Map<Long, Admit_cardDTO>mapOfAdmit_cardDTOToiD;
	Map<Long, Set<Admit_cardDTO> >mapOfAdmit_cardDTOTojobId;


	static Admit_cardRepository instance = null;  
	private Admit_cardRepository(){
		mapOfAdmit_cardDTOToiD = new ConcurrentHashMap<>();
		mapOfAdmit_cardDTOTojobId = new ConcurrentHashMap<>();
		setDAO(new Admit_cardDAO());

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Admit_cardRepository getInstance(){
		if (instance == null){
			instance = new Admit_cardRepository();
		}
		return instance;
	}

	public Admit_cardDTO clone(Admit_cardDTO dto) {
		String raw = gson.toJson(dto);
		return gson.fromJson(raw, Admit_cardDTO.class);
	}

	public List<Admit_cardDTO> clone(List<Admit_cardDTO> dtoList) {
		return dtoList
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}

	public void reload(boolean reloadAll){
		if(admit_cardDAO == null)
		{
			return;
		}
		try {
			List<Admit_cardDTO> admit_cardDTOs = admit_cardDAO.getAllAdmit_card(reloadAll);
			for(Admit_cardDTO admit_cardDTO : admit_cardDTOs) {
				Admit_cardDTO oldAdmit_cardDTO = getAdmit_cardDTOByIDWithoutClone(admit_cardDTO.iD);
				if( oldAdmit_cardDTO != null ) {
					mapOfAdmit_cardDTOToiD.remove(oldAdmit_cardDTO.iD);

					if(mapOfAdmit_cardDTOTojobId.containsKey(oldAdmit_cardDTO.recruitmentJobDescriptionType)) {
						mapOfAdmit_cardDTOTojobId.get(oldAdmit_cardDTO.recruitmentJobDescriptionType).remove(oldAdmit_cardDTO);
					}
					if(mapOfAdmit_cardDTOTojobId.get(oldAdmit_cardDTO.recruitmentJobDescriptionType).isEmpty()) {
						mapOfAdmit_cardDTOTojobId.remove(oldAdmit_cardDTO.recruitmentJobDescriptionType);
					}
					
					
				}
				if(admit_cardDTO.isDeleted == 0) 
				{
					
					mapOfAdmit_cardDTOToiD.put(admit_cardDTO.iD, admit_cardDTO);

					if( ! mapOfAdmit_cardDTOTojobId.containsKey(admit_cardDTO.recruitmentJobDescriptionType)) {
						mapOfAdmit_cardDTOTojobId.put(admit_cardDTO.recruitmentJobDescriptionType, new HashSet<>());
					}
					mapOfAdmit_cardDTOTojobId.get(admit_cardDTO.recruitmentJobDescriptionType).add(admit_cardDTO);

				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Admit_cardDTO> getAdmit_cardList() {
		List <Admit_cardDTO> admit_cards = new ArrayList<Admit_cardDTO>(this.mapOfAdmit_cardDTOToiD.values());
		return clone(admit_cards);
	}

	public Admit_cardDTO getAdmit_cardDTOByIDWithoutClone( long ID){
		return mapOfAdmit_cardDTOToiD.get(ID);
	}
	
	public Admit_cardDTO getAdmit_cardDTOByID( long ID){
		return clone(mapOfAdmit_cardDTOToiD.get(ID));
	}

	public List<Admit_cardDTO> getAdmit_cardDTOByjob_id(long job_id) {
		return clone(new ArrayList<>( mapOfAdmit_cardDTOTojobId.getOrDefault(job_id,new HashSet<>())));
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "admit_card";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


