package admit_card;
import java.util.*; 
import util.*;


public class Admit_cardMAPS extends CommonMaps
{	
	public Admit_cardMAPS(String tableName)
	{
		

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("recruitmentJobDescriptionType".toLowerCase(), "recruitmentJobDescriptionType".toLowerCase());
		java_DTO_map.put("placeOfExam".toLowerCase(), "placeOfExam".toLowerCase());
		java_DTO_map.put("dateOfExam".toLowerCase(), "dateOfExam".toLowerCase());
		java_DTO_map.put("timeOfExam".toLowerCase(), "timeOfExam".toLowerCase());
		java_DTO_map.put("rollNoFrom".toLowerCase(), "rollNoFrom".toLowerCase());
		java_DTO_map.put("rollNoTo".toLowerCase(), "rollNoTo".toLowerCase());
		java_DTO_map.put("employeeRecordsId".toLowerCase(), "employeeRecordsId".toLowerCase());
		java_DTO_map.put("insertedByUserId".toLowerCase(), "insertedByUserId".toLowerCase());
		java_DTO_map.put("insertionDate".toLowerCase(), "insertionDate".toLowerCase());
		java_DTO_map.put("modifiedBy".toLowerCase(), "modifiedBy".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("recruitment_job_description_type".toLowerCase(), "recruitmentJobDescriptionType".toLowerCase());
		java_SQL_map.put("place_of_exam".toLowerCase(), "placeOfExam".toLowerCase());
		java_SQL_map.put("date_of_exam".toLowerCase(), "dateOfExam".toLowerCase());
		java_SQL_map.put("time_of_exam".toLowerCase(), "timeOfExam".toLowerCase());
		java_SQL_map.put("roll_no_from".toLowerCase(), "rollNoFrom".toLowerCase());
		java_SQL_map.put("roll_no_to".toLowerCase(), "rollNoTo".toLowerCase());
		java_SQL_map.put("employee_records_id".toLowerCase(), "employeeRecordsId".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Recruitment Job Description".toLowerCase(), "recruitmentJobDescriptionType".toLowerCase());
		java_Text_map.put("Place Of Exam".toLowerCase(), "placeOfExam".toLowerCase());
		java_Text_map.put("Date Of Exam".toLowerCase(), "dateOfExam".toLowerCase());
		java_Text_map.put("Time Of Exam".toLowerCase(), "timeOfExam".toLowerCase());
		java_Text_map.put("Roll No From".toLowerCase(), "rollNoFrom".toLowerCase());
		java_Text_map.put("Roll No To".toLowerCase(), "rollNoTo".toLowerCase());
		java_Text_map.put("Employee Records Id".toLowerCase(), "employeeRecordsId".toLowerCase());
		java_Text_map.put("Inserted By User Id".toLowerCase(), "insertedByUserId".toLowerCase());
		java_Text_map.put("Insertion Date".toLowerCase(), "insertionDate".toLowerCase());
		java_Text_map.put("Modified By".toLowerCase(), "modifiedBy".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}