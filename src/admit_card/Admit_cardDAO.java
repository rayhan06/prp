package admit_card;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;

import common.ConnectionAndStatementUtil;
import recruitment_job_description.Recruitment_job_descriptionDAO;
import recruitment_job_description.Recruitment_job_descriptionDTO;
import recruitment_job_description.Recruitment_job_descriptionRepository;
import sessionmanager.SessionConstants;
import java.sql.SQLException;


import org.apache.log4j.Logger;

import dbm.*;

import repository.RepositoryManager;

import util.*;
import pb.*;
import user.UserDTO;
import vm_vehicle_office_assignment.Vm_vehicle_office_assignmentDTO;

public class Admit_cardDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());

	private static final char[] banglaDigits = {'০', '১', '২', '৩', '৪', '৫', '৬', '৭', '৮', '৯'};
	private static final Map<String, String> amPm;
	static {
		amPm = new HashMap<String, String>() {{
			put("AM", "পুর্বাহ্ন");
			put("PM", "অপরাহ্ন");
		}};
	}
	
	
	public Admit_cardDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";
		commonMaps = new Admit_cardMAPS(tableName);
		columnNames = new String[]
		{
			"ID",
			"recruitment_job_description_type",
			"place_of_exam",
			"date_of_exam",
			"time_of_exam",
			"end_time_of_exam",
			"roll_no_from",
			"roll_no_to",
			"employee_records_id",
			"inserted_by_user_id",
			"insertion_date",
			"modified_by",
			"search_column",
			"level",
			"range_active",
			"signatory_name",
			"signatory_name_bn",
			"signatory_office_unit_name",
			"signatory_office_unit_name_bn",
			"signatory_post_name",
			"signatory_post_name_bn",
			"rules",
			"unit_id",
			"post_id",
			"recruitment_test_name",
			"isDeleted",
			"lastModificationTime"
		};
	}


	
	public Admit_cardDAO()
	{
		this("admit_card");		
	}
	
	public void setSearchColumn(Admit_cardDTO admit_cardDTO)
	{
		admit_cardDTO.searchColumn = "";
		Recruitment_job_descriptionDAO recruitment_job_descriptionDAO =
				new Recruitment_job_descriptionDAO();
		Recruitment_job_descriptionDTO jobDescriptionDTO =
				Recruitment_job_descriptionRepository.getInstance().getRecruitment_job_descriptionDTOByID(admit_cardDTO.recruitmentJobDescriptionType);
		if(jobDescriptionDTO != null){
			admit_cardDTO.searchColumn += jobDescriptionDTO.jobTitleBn + " " + jobDescriptionDTO.jobTitleEn + " ";
		}
//		admit_cardDTO.searchColumn += CommonDAO.getName("English", "recruitment_job_description", admit_cardDTO.recruitmentJobDescriptionType) + " " + CommonDAO.getName("Bangla", "recruitment_job_description", admit_cardDTO.recruitmentJobDescriptionType) + " ";
		admit_cardDTO.searchColumn += admit_cardDTO.placeOfExam + " ";
		
	}
	
	public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException
	{
		Admit_cardDTO admit_cardDTO = (Admit_cardDTO)commonDTO;

		int index = 1;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(admit_cardDTO);
		if(isInsert)
		{
			ps.setObject(index++,admit_cardDTO.iD);
		}
		ps.setObject(index++,admit_cardDTO.recruitmentJobDescriptionType);
		ps.setObject(index++,admit_cardDTO.placeOfExam);
		ps.setObject(index++,admit_cardDTO.dateOfExam);
		ps.setObject(index++,admit_cardDTO.timeOfExam);
		ps.setObject(index++,admit_cardDTO.endTimeOfExam);
		ps.setObject(index++,admit_cardDTO.rollNoFrom);
		ps.setObject(index++,admit_cardDTO.rollNoTo);
		ps.setObject(index++,admit_cardDTO.employeeRecordsId);
		ps.setObject(index++,admit_cardDTO.insertedByUserId);
		ps.setObject(index++,admit_cardDTO.insertionDate);
		ps.setObject(index++,admit_cardDTO.modifiedBy);
		ps.setObject(index++,admit_cardDTO.searchColumn);
		ps.setObject(index++,admit_cardDTO.level);
		ps.setObject(index++,admit_cardDTO.range);
		ps.setObject(index++,admit_cardDTO.signatory_name);
		ps.setObject(index++,admit_cardDTO.signatory_name_bn);
		ps.setObject(index++,admit_cardDTO.signatory_office_unit_name);
		ps.setObject(index++,admit_cardDTO.signatory_office_unit_name_bn);
		ps.setObject(index++,admit_cardDTO.signatory_post_name);
		ps.setObject(index++,admit_cardDTO.signatory_post_name_bn);
		ps.setObject(index++,admit_cardDTO.rules);
		ps.setObject(index++,admit_cardDTO.unit_id);
		ps.setObject(index++,admit_cardDTO.post_id);
		ps.setObject(index++,admit_cardDTO.recruitmentTestName);
		if(isInsert)
		{
			ps.setObject(index++, 0);
			ps.setObject(index++, lastModificationTime);
		}
	}

	public Admit_cardDTO build(ResultSet rs)
	{
		try
		{
			Admit_cardDTO admit_cardDTO = new Admit_cardDTO();
			admit_cardDTO.iD = rs.getLong("ID");
			admit_cardDTO.recruitmentJobDescriptionType = rs.getLong("recruitment_job_description_type");
			admit_cardDTO.placeOfExam = rs.getString("place_of_exam");
			admit_cardDTO.dateOfExam = rs.getLong("date_of_exam");
			admit_cardDTO.timeOfExam = rs.getString("time_of_exam");
			admit_cardDTO.endTimeOfExam = rs.getString("end_time_of_exam");
			admit_cardDTO.rollNoFrom = rs.getLong("roll_no_from");
			admit_cardDTO.rollNoTo = rs.getLong("roll_no_to");
			admit_cardDTO.employeeRecordsId = rs.getLong("employee_records_id");
			admit_cardDTO.insertedByUserId = rs.getLong("inserted_by_user_id");
			admit_cardDTO.insertionDate = rs.getLong("insertion_date");
			admit_cardDTO.modifiedBy = rs.getString("modified_by");
			admit_cardDTO.isDeleted = rs.getInt("isDeleted");
			admit_cardDTO.lastModificationTime = rs.getLong("lastModificationTime");
			admit_cardDTO.level = rs.getInt("level");
			admit_cardDTO.range = rs.getBoolean("range_active");
			admit_cardDTO.signatory_name = rs.getString("signatory_name");
			admit_cardDTO.signatory_name_bn = rs.getString("signatory_name_bn");
			admit_cardDTO.signatory_office_unit_name = rs.getString("signatory_office_unit_name");
			admit_cardDTO.signatory_office_unit_name_bn = rs.getString("signatory_office_unit_name_bn");
			admit_cardDTO.signatory_post_name = rs.getString("signatory_post_name");
			admit_cardDTO.signatory_post_name_bn = rs.getString("signatory_post_name_bn");
			admit_cardDTO.rules = rs.getString("rules");
			admit_cardDTO.unit_id = rs.getLong("unit_id");
			admit_cardDTO.post_id = rs.getLong("post_id");
			admit_cardDTO.recruitmentTestName = rs.getLong("recruitment_test_name");
			return admit_cardDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
	

		
	

	//need another getter for repository
	public Admit_cardDTO getDTOByID (long ID)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID = ? ";
		return 	ConnectionAndStatementUtil.getT(sql, Arrays.asList(ID), this::build);
	}
	
	
	public List<Admit_cardDTO> getDTOs(Collection recordIDs){
		if(recordIDs.isEmpty()){
			return new ArrayList<>();
		}
		String sql = "SELECT * FROM " + tableName + " WHERE ID IN ( ";
		for(int i = 0;i<recordIDs.size();i++){
			if(i!=0){
				sql+=",";
			}
			sql+= " ? ";
		}
		sql+=")  order by lastModificationTime desc";
		printSql(sql);
		return ConnectionAndStatementUtil.getListOfT(sql, Arrays.asList(recordIDs.toArray()),this::build);
	
	}
	
	

	
	
	
	//add repository
	public List<Admit_cardDTO> getAllAdmit_card (boolean isFirstReload)
    {

		String sql = "SELECT * FROM admit_card";
		sql += " WHERE ";
	

		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by admit_card.lastModificationTime desc";
		printSql(sql);
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);
    }

	public List<Admit_cardDTO> getAllAdmit_cardByJobID (long jobId)
	{

		String sql = "SELECT * FROM admit_card";
		sql += " WHERE ";
		sql+=" isDeleted =  0 and recruitment_job_description_type = ? ";


		sql += " order by admit_card.lastModificationTime desc";
		printSql(sql);
		return ConnectionAndStatementUtil.getListOfT(sql, Arrays.asList(jobId), this::build);
	}


	public String getSqlWithSearchCriteriaForNormalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
			UserDTO userDTO, String filter, boolean tableHasJobCat, List<Object> objectList)
    {
		boolean viewAll = false;
		
		if(p_searchCriteria.get("ViewAll") != null)
		{
			System.out.println("ViewAll = " + p_searchCriteria.get("ViewAll"));
			viewAll = true;
		}
		else
		{
			System.out.println("ViewAll is null ");
		}
		
		String sql = "SELECT ";
		if(category == GETIDS)
		{
			sql += " " + tableName + ".ID as ID ";
		}
		else if(category == GETCOUNT)
		{
			sql += " count( " + tableName + ".ID) as countID ";
		}
		else if(category == GETDTOS)
		{
			sql += " distinct " + tableName + ".* ";
		}
		sql += "FROM " + tableName + " ";
		sql += joinSQL;
		

		
		String AnyfieldSql = "";
		String AllFieldSql = "";
		
		if(p_searchCriteria != null)
		{
						
			
			Enumeration names = p_searchCriteria.keys();
			String str, value;
			
			AnyfieldSql = "(";
			
			if(p_searchCriteria.get("AnyField")!= null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase(""))
			{
				AnyfieldSql+= tableName + ".search_column like ? ";
				objectList.add("%" + p_searchCriteria.get("AnyField").toString() + "%");			}
			AnyfieldSql += ")";
			System.out.println("AnyfieldSql = " + AnyfieldSql);
			
			AllFieldSql = "(";
			int i = 0;
			while(names.hasMoreElements())
			{				
				str = (String) names.nextElement();
				value = (String)p_searchCriteria.get(str);
		        System.out.println(str + ": " + value);
				if(value != null && !value.equalsIgnoreCase("") && (
						 str.equals("recruitment_job_description_type")
						|| str.equals("place_of_exam")
//						|| str.equals("date_of_exam_start")
//						|| str.equals("date_of_exam_end")
//						|| str.equals("insertion_date_start")
//						|| str.equals("insertion_date_end")
				)
						
				)
				{
					if(p_searchCriteria.get(str).equals("any"))
					{
						continue;
					}

					if( i > 0)
					{
						AllFieldSql+= " AND  ";
					}
					
					 if(str.equals("recruitment_job_description_type"))
					{
						AllFieldSql += "" + tableName + ".recruitment_job_description_type = ? ";
						objectList.add(p_searchCriteria.get(str));
						i ++;
					}
					else if(str.equals("place_of_exam"))
					{
						AllFieldSql += "" + tableName + ".place_of_exam like ? ";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
//					else if(str.equals("date_of_exam_start"))
//					{
//						AllFieldSql += "" + tableName + ".date_of_exam >= " + p_searchCriteria.get(str);
//						i ++;
//					}
//					else if(str.equals("date_of_exam_end"))
//					{
//						AllFieldSql += "" + tableName + ".date_of_exam <= " + p_searchCriteria.get(str);
//						i ++;
//					}
//					else if(str.equals("insertion_date_start"))
//					{
//						AllFieldSql += "" + tableName + ".insertion_date >= " + p_searchCriteria.get(str);
//						i ++;
//					}
//					else if(str.equals("insertion_date_end"))
//					{
//						AllFieldSql += "" + tableName + ".insertion_date <= " + p_searchCriteria.get(str);
//						i ++;
//					}
					
					
				}
			}
			
			AllFieldSql += ")";
			System.out.println("AllFieldSql = " + AllFieldSql);
			
			
		}
		
		
		sql += " WHERE ";
		
		sql += " (" + tableName + ".isDeleted = 0 ";			
		sql += ")";
		
		
		if(!filter.equalsIgnoreCase(""))
		{
			sql += " and " + filter + " ";
		}
		
		if(!AnyfieldSql.equals("()") && !AnyfieldSql.equals(""))
		{
			sql += " AND " + AnyfieldSql;
			
		}
		if(!AllFieldSql.equals("()") && !AllFieldSql.equals(""))
		{			
			sql += " AND " + AllFieldSql;
		}
		
	
			
		sql += " order by " + tableName + ".lastModificationTime desc ";

		printSql(sql);
		
		if(limit >= 0)
		{
			sql += " limit " + limit;
		}
		if(offset >= 0)
		{
			sql += " offset " + offset;
		}
		
		System.out.println("-------------- sql = " + sql);
		
		return sql;
    }
	




	public static final String getDateBanglaFromEnglish(String number) {
		if (number == null)
			return "";
		StringBuilder builder = new StringBuilder();
		try {
			for (int i = 0; i < number.length(); i++) {
				if (Character.isDigit(number.charAt(i))) {
					if (((int) (number.charAt(i)) - 48) <= 9) {
						builder.append(banglaDigits[(int) (number.charAt(i)) - 48]);
					} else {
						builder.append(number.charAt(i));
					}
				} else {
					builder.append(number.charAt(i));
				}
			}
		} catch (Exception e) {
			return "";
		}
		String dateBangla = builder.toString();
		if (dateBangla.contains("AM")) {
			dateBangla = dateBangla.replace("AM", amPm.get("AM"));
		} else if (dateBangla.contains("PM")) {
			dateBangla = dateBangla.replace("PM", amPm.get("PM"));
		}
		return dateBangla;
	}
				
}
	