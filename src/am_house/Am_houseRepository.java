package am_house;

import am_parliament_building_block.Am_parliament_building_blockDTO;
import com.google.gson.Gson;
import org.apache.log4j.Logger;
import pb.CatDTO;
import pb.CatRepository;
import pb.CategoryLanguageModel;
import repository.Repository;
import repository.RepositoryManager;
import vm_fuel_vendor.Vm_fuel_vendorDTO;
import vm_requisition.CommonApprovalStatus;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;


public class Am_houseRepository implements Repository {
	Am_houseDAO am_houseDAO;
	Gson gson = new Gson();
	
	public void setDAO(Am_houseDAO am_houseDAO)
	{
		this.am_houseDAO = am_houseDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Am_houseRepository.class);
	Map<Long, Am_houseDTO>mapOfAm_houseDTOToiD;
	Map<Integer, Set<Am_houseDTO>>mapOfAm_houseDTOToClassId;

  
	private Am_houseRepository(){
		am_houseDAO = Am_houseDAO.getInstance();
		mapOfAm_houseDTOToiD = new ConcurrentHashMap<>();
		mapOfAm_houseDTOToClassId = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	private static class LazyLoader{
        static final Am_houseRepository INSTANCE = new Am_houseRepository();
    }

    public static Am_houseRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

	public void reload(boolean reloadAll){
		try {
			List<Am_houseDTO> am_houseDTOs = am_houseDAO.getAllDTOs(reloadAll);
			for(Am_houseDTO am_houseDTO : am_houseDTOs) {
				Am_houseDTO oldAm_houseDTO = getAm_houseDTOByIDWithoutClone(am_houseDTO.iD);
				if( oldAm_houseDTO != null ) {
					mapOfAm_houseDTOToiD.remove(oldAm_houseDTO.iD);

					if(mapOfAm_houseDTOToClassId.containsKey(oldAm_houseDTO.amHouseClassCat)) {
						mapOfAm_houseDTOToClassId.get(oldAm_houseDTO.amHouseClassCat).
								remove(oldAm_houseDTO);
					}
					if(mapOfAm_houseDTOToClassId.get(oldAm_houseDTO.amHouseClassCat).isEmpty()) {
						mapOfAm_houseDTOToClassId.remove(oldAm_houseDTO.amHouseClassCat);
					}
				
					
				}
				if(am_houseDTO.isDeleted == 0) 
				{
					
					mapOfAm_houseDTOToiD.put(am_houseDTO.iD, am_houseDTO);

					if( ! mapOfAm_houseDTOToClassId.containsKey(am_houseDTO.amHouseClassCat)) {
						mapOfAm_houseDTOToClassId.put(am_houseDTO.amHouseClassCat, new HashSet<>());
					}
					mapOfAm_houseDTOToClassId.get(am_houseDTO.amHouseClassCat).add(am_houseDTO);
				
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Am_houseDTO> getAm_houseList() {
		return clone(new ArrayList<>(this.mapOfAm_houseDTOToiD.values()));

	}
	
	
	public Am_houseDTO getAm_houseDTOByIDWithoutClone( long ID){
		return mapOfAm_houseDTOToiD.get(ID);
	}

	public Am_houseDTO getAm_houseDTOByID( long ID){
		return clone(mapOfAm_houseDTOToiD.get(ID));
	}

	
	@Override
	public String getTableName() {
		return "am_house";
	}

	public Am_houseDTO clone(Am_houseDTO dto) {
		String raw = gson.toJson(dto);
		return gson.fromJson(raw, Am_houseDTO.class);
	}

	public List<Am_houseDTO> clone(List<Am_houseDTO> dtoList) {
		return dtoList
				.stream()
				.map(this::clone)
				.collect(Collectors.toList());
	}

	public List<Am_houseDTO> getAm_houseDTOByClassId(int classId) {
		return clone(new ArrayList<>( mapOfAm_houseDTOToClassId.getOrDefault(classId,new HashSet<>())));
	}

	public String getActiveHouse(int newCat, int locCat, int classCat, long defaultValue, String language){
		List<Am_houseDTO> dtos = getAm_houseDTOByClassId(classCat).
				stream()
				.filter(i -> i.amHouseOldNewCat == newCat && i.amHouseLocationCat == locCat)
				.filter(i -> i.status == CommonApprovalStatus.AVAILABLE.getValue())
				.collect(Collectors.toList());

		StringBuilder sOptions =new StringBuilder();
		if (defaultValue == -1)
		{
			if (language.equalsIgnoreCase("English"))
			{
				sOptions.append("<option value = ''>Select</option>");
			}
			else
			{
				sOptions.append("<option value = ''>বাছাই করুন</option>");
			}
		}

		for(Am_houseDTO dto: dtos){
			StringBuilder sOption =new StringBuilder();
			sOption.append("<option value = '").append( dto.iD).append("'");
			if(defaultValue != CatDTO.CATDEFAULTNOTREQUIRED && dto.iD == defaultValue)
			{
				sOption.append(" selected ");
			}
			sOption.append(">").append(dto.houseNumber).append("</option>");
			sOptions.append(sOption);
		}

		return sOptions.toString();
	}

	public String getText(long id,String language){
		Am_houseDTO dto = getAm_houseDTOByID(id);
		return dto==null?"": dto.houseNumber;
	}
}


