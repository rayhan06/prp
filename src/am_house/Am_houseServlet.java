package am_house;

import common.ApiResponse;
import common.BaseServlet;
import common.CommonDAOService;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import pb.CatRepository;
import pb.CategoryLanguageModel;
import pb.Utils;
import permission.MenuConstants;
import user.UserDTO;
import util.CommonDTO;
import util.CommonLoginData;
import util.HttpRequestUtils;
import util.UtilCharacter;
import vm_requisition.CommonApprovalStatus;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;


/**
 * Servlet implementation class Am_houseServlet
 */
@WebServlet("/Am_houseServlet")
@MultipartConfig
public class Am_houseServlet extends BaseServlet
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Am_houseServlet.class);

	@Override
	public String getTableName() {
		return Am_houseDAO.getInstance().getTableName();
	}

	@Override
	public String getServletName() {
		return "Am_houseServlet";
	}

	@Override
	public CommonDAOService<? extends CommonDTO> getCommonDAOService() {
		return Am_houseDAO.getInstance();
	}

	@Override
	public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {

		Am_houseDTO am_houseDTO;

		if(Boolean.TRUE.equals(addFlag))
		{
			am_houseDTO = new Am_houseDTO();
			am_houseDTO.insertionDate = am_houseDTO.lastModificationTime
					= System.currentTimeMillis();
			am_houseDTO.insertedByUserId = userDTO.ID;
			am_houseDTO.insertedByOrganogramId = userDTO.organogramID;
			am_houseDTO.status = CommonApprovalStatus.AVAILABLE.getValue();
		}
		else
		{
			am_houseDTO = Am_houseRepository.getInstance().getAm_houseDTOByID
					(Long.parseLong(request.getParameter("iD")));
			if (am_houseDTO == null) {
				UtilCharacter.throwException("বাসা খুঁজে পাওয়া যায় নি ", "House not found");
			}

			if(!Am_houseDAO.getInstance().isNotUsed(am_houseDTO.iD)){
//				UtilCharacter.throwException("ইতোমধ্যে ব্যবহৃত, পরিবর্তন সম্ভব নয় ", "Can't Update, Already Used.");
				am_houseDTO.amHouseClassCat = Integer.parseInt(request.getParameter("amHouseClassCat"));
				CategoryLanguageModel categoryLanguageModel  = CatRepository.getInstance().getCategoryLanguageModel
						("am_house_class", am_houseDTO.amHouseClassCat);
				if (categoryLanguageModel == null) {
					UtilCharacter.throwException("বাসার শ্রেণী খুঁজে পাওয়া যায় নি ", "House class is invalid");
				}

				String houseSubClassCat = request.getParameter("amHouseClassSubCat");
				if(houseSubClassCat != null && !houseSubClassCat.equals("")){
					am_houseDTO.amHouseClassSubCat = Integer.parseInt(houseSubClassCat);
				}
				getCommonDAOService().update(am_houseDTO);
				return am_houseDTO;
			}


			am_houseDTO.lastModificationTime = System.currentTimeMillis();
			am_houseDTO.modifiedBy = userDTO.ID;
		}

		am_houseDTO.amHouseOldNewCat = Integer.parseInt(request.getParameter("amHouseOldNewCat"));
		CategoryLanguageModel categoryLanguageModel = CatRepository.getInstance().getCategoryLanguageModel
				("am_house_old_new", am_houseDTO.amHouseOldNewCat);
		if (categoryLanguageModel == null) {
			UtilCharacter.throwException("নতুন বাসা/পুরাতন বাসা খুঁজে পাওয়া যায় নি", "House old/new is invalid");
		}

		am_houseDTO.amHouseLocationCat = Integer.parseInt(request.getParameter("amHouseLocationCat"));
		categoryLanguageModel = CatRepository.getInstance().getCategoryLanguageModel
				("am_house_location", am_houseDTO.amHouseLocationCat);
		if (categoryLanguageModel == null) {
			UtilCharacter.throwException("বাসার অবস্থান খুঁজে পাওয়া যায় নি ", "House location is invalid");
		}

		am_houseDTO.amHouseClassCat = Integer.parseInt(request.getParameter("amHouseClassCat"));
		categoryLanguageModel = CatRepository.getInstance().getCategoryLanguageModel
				("am_house_class", am_houseDTO.amHouseClassCat);
		if (categoryLanguageModel == null) {
			UtilCharacter.throwException("বাসার শ্রেণী খুঁজে পাওয়া যায় নি ", "House class is invalid");
		}

		String houseSubClassCat = request.getParameter("amHouseClassSubCat");
		if(houseSubClassCat != null && !houseSubClassCat.equals("")){
			am_houseDTO.amHouseClassSubCat = Integer.parseInt(houseSubClassCat);
		}

		am_houseDTO.houseNumber = Jsoup.clean(request.getParameter("houseNumber"),Whitelist.simpleText());
		if (am_houseDTO.houseNumber.isEmpty()) {
			UtilCharacter.throwException("বাসার নম্বর প্রয়োজনীয়", "House Number required");
		}

		if(!uniquenessCheck(am_houseDTO.houseNumber, addFlag, am_houseDTO.iD)){
			UtilCharacter.throwException("বাসার নম্বর ইউনিক নয় ", "House Number not unique");
		}

		if (Boolean.TRUE.equals(addFlag)) {
			getCommonDAOService().add(am_houseDTO);
		} else {
			getCommonDAOService().update(am_houseDTO);
		}

		return am_houseDTO;


	}

	@Override
	public int[] getAddPageMenuConstants() {
		return new int[]{MenuConstants.AM_HOUSE_ADD};
	}

	@Override
	public int[] getEditPageMenuConstants() {
		return new int[]{MenuConstants.AM_HOUSE_UPDATE};
	}

	@Override
	public int[] getSearchMenuConstants() {
		return new int[]{MenuConstants.AM_HOUSE_SEARCH};
	}

	@Override
	public Class<? extends HttpServlet> getClazz() {
		return Am_houseServlet.class;
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String actionType = request.getParameter("actionType");
		if ("getActiveHouse".equals(actionType)) {
			try {
				int newCat = Integer.parseInt(request.getParameter("newCat"));
				int locCat = Integer.parseInt(request.getParameter("locCat"));
				int classCat = Integer.parseInt(request.getParameter("classCat"));
				long defaultValue = Long.parseLong(request.getParameter("defaultValue"));
				String language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
				String responseText = Am_houseRepository.getInstance().
						getActiveHouse(newCat, locCat, classCat, defaultValue, language);
				ApiResponse.sendSuccessResponse(response, responseText);
			} catch (Exception ex) {
				logger.error(ex);
				ApiResponse.sendErrorResponse(response, ex.getMessage());
			}
			return;
		}
		super.doGet(request,response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String actionType = request.getParameter("actionType");
		CommonLoginData commonLoginData = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get();

		if ("setNotAvailable".equals(actionType) &&Utils.checkPermission(commonLoginData.userDTO, getEditPageMenuConstants())
				&& getEditPermission(request)) {
			try {
				Am_houseDTO am_houseDTO = Am_houseRepository.getInstance().getAm_houseDTOByID
						(Long.parseLong(request.getParameter("houseId")));
				if (am_houseDTO == null) {
					UtilCharacter.throwException("বাসা খুঁজে পাওয়া যায় নি ", "House not found");
				}

				if(am_houseDTO.status != CommonApprovalStatus.AVAILABLE.getValue()){
					UtilCharacter.throwException("অবস্থা ব্যবহারযোগ্য হতে হবে ", "Status Have to be Available");
				}

				am_houseDTO.not_available_reason = Jsoup.clean(request.getParameter("remarks"),
						Whitelist.simpleText());
				am_houseDTO.status = CommonApprovalStatus.NOT_AVAILABLE.getValue();
				am_houseDTO.lastModificationTime = System.currentTimeMillis();
				am_houseDTO.modifiedBy = commonLoginData.userDTO.ID;
				Am_houseDAO.getInstance().update(am_houseDTO);

				String redirectUrl = "Am_houseServlet?actionType=view&ID=" + am_houseDTO.iD;
				ApiResponse.sendSuccessResponse(response, redirectUrl);


			} catch (Exception ex) {
				logger.error(ex);
				ApiResponse.sendErrorResponse(response, ex.getMessage());
			}
			return;
		}

		super.doPost(request,response);
	}

	@Override
	protected void deleteT(HttpServletRequest request, UserDTO userDTO) {
		String[] IDsToDelete = request.getParameterValues("ID");
		if(IDsToDelete.length>0){
			List<Long> ids = Stream.of(IDsToDelete)
					.map(Long::parseLong)
					.filter(i -> Am_houseDAO.getInstance().isNotUsed(i))
					.collect(Collectors.toList());

			if(!ids.isEmpty()){
				getCommonDAOService().deletePb(userDTO.employee_record_id, ids);
			}


		}
	}

	private boolean uniquenessCheck(String houseNo, boolean addFlag, Long ID){
		boolean flag = false;
		int count = Am_houseDAO.getInstance().getCountOfHouseNo(houseNo);
		if (addFlag) {
			if (count == 0) {
				flag = true;
			}
		} else {
			Am_houseDTO houseDTO = Am_houseRepository.getInstance().getAm_houseDTOByID(ID);
			if(houseDTO != null){
				Boolean sameValueSaveLogic = houseDTO.houseNumber.equals(houseNo) && count == 1;
				Boolean differentValueSaveLogic = !houseDTO.houseNumber.equals(houseNo) && count == 0;
				if (sameValueSaveLogic || differentValueSaveLogic) {
					flag = true;
				}
			}
		}

		return flag;
	}


}

