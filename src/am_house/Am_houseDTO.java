package am_house;
import util.*; 


public class Am_houseDTO extends CommonDTO
{

	public int amHouseOldNewCat = -1;
	public int amHouseLocationCat = -1;
	public int amHouseClassCat = -1;
	public int amHouseClassSubCat = -1;
    public String houseNumber = "";
	public long insertedByUserId = -1;
	public long insertedByOrganogramId = -1;
	public long modifiedBy = -1;
	public long insertionDate = -1;
	public int status = -1;
	public String not_available_reason = "";
	
	
    @Override
	public String toString() {
            return "$Am_houseDTO[" +
            " iD = " + iD +
            " amHouseOldNewCat = " + amHouseOldNewCat +
            " amHouseLocationCat = " + amHouseLocationCat +
            " amHouseClassCat = " + amHouseClassCat +
            " amHouseClassSubCat = " + amHouseClassSubCat +
            " houseNumber = " + houseNumber +
            " insertedByUserId = " + insertedByUserId +
            " insertedByOrganogramId = " + insertedByOrganogramId +
            " modifiedBy = " + modifiedBy +
            " insertionDate = " + insertionDate +
            " searchColumn = " + searchColumn +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}