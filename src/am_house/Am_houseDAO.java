package am_house;

import am_house_allocation.Am_house_allocationDAO;
import am_house_allocation_request.Am_house_allocation_requestDAO;
import common.CommonDAOService;
import org.apache.log4j.Logger;
import pb.CatRepository;
import pb.CategoryLanguageModel;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class Am_houseDAO  implements CommonDAOService<Am_houseDTO>
{

	private static final Logger logger = Logger.getLogger(Am_houseDAO.class);
	private final Map<String,String> searchMap = new HashMap<>();

	private static final String addQuery = "INSERT INTO {tableName} (inserted_by_user_id,inserted_by_organogram_id," +
			"insertion_date, modified_by, search_column, am_house_old_new_cat, am_house_location_cat, am_house_class_cat," +
			"am_house_class_sub_cat, house_number, not_available_reason, status,  lastModificationTime, isDeleted,ID) VALUES (?,?,?,?,?,?,?,?, ?, ?, ?, ?, ?, ?, ?)";

	private static final String updateQuery = "UPDATE {tableName} SET inserted_by_user_id = ?,inserted_by_organogram_id=?," +
			" insertion_date=?, modified_by = ?, search_column=?,am_house_old_new_cat=?, am_house_location_cat = ?," +
			" am_house_class_cat = ?, am_house_class_sub_cat = ?, house_number = ?, not_available_reason = ?, status = ?, lastModificationTime=? WHERE ID = ?";

	private static class LazyLoader{
		static final Am_houseDAO INSTANCE = new Am_houseDAO();
	}

	public static Am_houseDAO getInstance(){
		return Am_houseDAO.LazyLoader.INSTANCE;
	}

	private Am_houseDAO() {
		searchMap.put("am_house_old_new_cat"," and (am_house_old_new_cat = ?)");
		searchMap.put("am_house_location_cat"," and (am_house_location_cat = ?)");
		searchMap.put("am_house_class_cat"," and (am_house_class_cat = ?)");
		searchMap.put("am_house_class_sub_cat"," and (am_house_class_sub_cat = ?)");
		searchMap.put("house_number"," and ( house_number like ? )");
		searchMap.put("insertion_date_start"," and (insertion_date>= ?)");
		searchMap.put("insertion_date_end"," and (insertion_date <= ?)");
		searchMap.put("AnyField"," and (search_column like ?)");
	}

	@Override
	public void set(PreparedStatement ps, Am_houseDTO am_houseDTO, boolean isInsert) throws SQLException {
		setSearchColumn(am_houseDTO);
		int index = 0;
		ps.setObject(++index, am_houseDTO.insertedByUserId);
		ps.setObject(++index, am_houseDTO.insertedByOrganogramId);
		ps.setObject(++index, am_houseDTO.insertionDate);
		ps.setObject(++index, am_houseDTO.modifiedBy);
		ps.setObject(++index, am_houseDTO.searchColumn);
		ps.setObject(++index, am_houseDTO.amHouseOldNewCat);
		ps.setObject(++index, am_houseDTO.amHouseLocationCat);
		ps.setObject(++index, am_houseDTO.amHouseClassCat);
		ps.setObject(++index, am_houseDTO.amHouseClassSubCat);
		ps.setObject(++index, am_houseDTO.houseNumber);
		ps.setObject(++index, am_houseDTO.not_available_reason);
		ps.setObject(++index, am_houseDTO.status);
		ps.setObject(++index, am_houseDTO.lastModificationTime);
		if (isInsert) {
			ps.setObject(++index, 0);
		}
		ps.setObject(++index, am_houseDTO.iD);
	}
	


	@Override
	public Am_houseDTO buildObjectFromResultSet(ResultSet rs) {

		try {
			Am_houseDTO am_houseDTO = new Am_houseDTO();
			am_houseDTO.iD = rs.getLong("ID");
			am_houseDTO.amHouseOldNewCat = rs.getInt("am_house_old_new_cat");
			am_houseDTO.amHouseLocationCat = rs.getInt("am_house_location_cat");
			am_houseDTO.amHouseClassCat = rs.getInt("am_house_class_cat");
			am_houseDTO.amHouseClassSubCat = rs.getInt("am_house_class_sub_cat");
			am_houseDTO.houseNumber = rs.getString("house_number");
			am_houseDTO.not_available_reason = rs.getString("not_available_reason");
			am_houseDTO.status = rs.getInt("status");
			am_houseDTO.insertedByUserId = rs.getLong("inserted_by_user_id");
			am_houseDTO.insertedByOrganogramId = rs.getLong("inserted_by_organogram_id");
			am_houseDTO.modifiedBy = rs.getLong("modified_by");
			am_houseDTO.insertionDate = rs.getLong("insertion_date");
			am_houseDTO.searchColumn = rs.getString("search_column");
			am_houseDTO.isDeleted = rs.getInt("isDeleted");
			am_houseDTO.lastModificationTime = rs.getLong("lastModificationTime");
			return am_houseDTO;
		} catch (SQLException ex) {
			logger.error(ex);
			return null;
		}
	}

	@Override
	public String getTableName() {
		return "am_house";
	}

	@Override
	public Map<String, String> getSearchMap() {
		return searchMap;
	}

	@Override
	public long add(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Am_houseDTO) commonDTO,addQuery,true);
	}

	@Override
	public long update(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Am_houseDTO) commonDTO,updateQuery,false);
	}

	public void setSearchColumn(Am_houseDTO am_houseDTO)
	{
		am_houseDTO.searchColumn = "";
		CategoryLanguageModel model = CatRepository.getInstance().getCategoryLanguageModel
				("am_house_old_new",am_houseDTO.amHouseOldNewCat);
		if(model != null){
			am_houseDTO.searchColumn += model.englishText+" "+model.banglaText+" ";
		}

		model = CatRepository.getInstance().getCategoryLanguageModel
				("am_house_location",am_houseDTO.amHouseLocationCat);
		if(model != null){
			am_houseDTO.searchColumn += model.englishText+" "+model.banglaText+" ";
		}

		model = CatRepository.getInstance().getCategoryLanguageModel
				("am_house_class",am_houseDTO.amHouseClassCat);
		if(model != null){
			am_houseDTO.searchColumn += model.englishText+" "+model.banglaText+" ";
		}

		am_houseDTO.searchColumn += am_houseDTO.houseNumber + " ";
	}

	public boolean isNotUsed(Long iD){
		if(Am_house_allocationDAO.getInstance().getCountByHouseId(iD) > 0){
			return false;
		}

        return Am_house_allocation_requestDAO.getInstance().getCountByHouseId(iD) <= 0;
    }

	public int getCountOfHouseNo (String houseNo) {
		return (int) Am_houseRepository.getInstance().getAm_houseList().
				stream().filter(i -> i.houseNumber.equals(houseNo)).count();
	}
}
	