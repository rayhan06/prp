package asset_clearance_status;
import java.util.*;

import asset_clearance_letter.Asset_clearance_letterDTO;
import asset_model.AssetAssigneeDTO;
import pb.CommonDAO;
import support_ticket.Support_ticketDTO;
import user.UserDTO;
import util.*; 


public class Asset_clearance_statusDTO extends CommonDTO
{

	public long supportTicketId = -1;
	public long assetClearanceLetterId = -1;
    public long assetAssigneeId = -1;
	public int assetClearanceStatusCat = -1;
	public long employeeRecordId = -1;
	public long organogramId = -1;
	public long insertedByUserId = -1;
	public long insertedByOrganogramId = -1;
	public long insertionDate = -1;
	public long clearedByUserId = -1;
	public long clearingDate = 0;
	public String clearingRemarks = "";
	public String assetName = "";
	
	public Asset_clearance_statusDTO()
	{
		
	}
	
	public Asset_clearance_statusDTO(Asset_clearance_letterDTO asset_clearance_letterDTO, Support_ticketDTO support_ticketDTO, UserDTO userDTO, AssetAssigneeDTO assetAssigneeDTO)
	{
		supportTicketId = support_ticketDTO.iD;
		assetClearanceLetterId = asset_clearance_letterDTO.iD;
		employeeRecordId = asset_clearance_letterDTO.employeeRecordId;
		organogramId = asset_clearance_letterDTO.organogramId;
		assetClearanceStatusCat = Asset_clearance_letterDTO.NA;
		assetAssigneeId = assetAssigneeDTO.iD;
		assetName = CommonDAO.getName("english", "asset_category", assetAssigneeDTO.assetCategoryType) + ": " + assetAssigneeDTO.modelName;		
		insertedByUserId = userDTO.ID;
		insertedByOrganogramId = userDTO.organogramID;
		insertionDate = System.currentTimeMillis();
	}
	
	
	
	
	
    @Override
	public String toString() {
            return "$Asset_clearance_statusDTO[" +
            " iD = " + iD +
            " supportTicketId = " + supportTicketId +
            " assetClearanceLetterId = " + assetClearanceLetterId +
            " assetAssigneeId = " + assetAssigneeId +
            " assetClearanceStatusCat = " + assetClearanceStatusCat +
            " employeeRecordId = " + employeeRecordId +
            " organogramId = " + organogramId +
            " insertedByUserId = " + insertedByUserId +
            " insertedByOrganogramId = " + insertedByOrganogramId +
            " insertionDate = " + insertionDate +
            " clearedByUserId = " + clearedByUserId +
            " clearingDate = " + clearingDate +
            " clearingRemarks = " + clearingRemarks +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}