package asset_clearance_status;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.sql.SQLException;
import common.CommonDAOService;
import common.ConnectionAndStatementUtil;

import org.apache.log4j.Logger;

import asset_model.AssetAssigneeDTO;
import util.*;
import pb.*;

public class Asset_clearance_statusDAO  implements CommonDAOService<Asset_clearance_statusDTO>
{
	
	Logger logger = Logger.getLogger(getClass());
	
	private static String addQuery = "";
	private static String updateQuery = "";
	private String[] columnNames = null;

	private Asset_clearance_statusDAO()
	{
		columnNames = new String[] 
		{
			"ID",
			"support_ticket_id",
			"asset_clearance_letter_id",
			"asset_assignee_id",
			"asset_clearance_status_cat",
			"employee_record_id",
			"organogram_id",
			"inserted_by_user_id",
			"inserted_by_organogram_id",
			"insertion_date",
			"cleared_by_user_id",
			"clearing_date",
			"clearing_remarks",
			"asset_name",
			"isDeleted",
			"lastModificationTime"
		};
		updateQuery = getUpdateQuery(columnNames);
        addQuery = getAddQuery(columnNames);
		
		searchMap.put("asset_clearance_status_cat"," and (asset_clearance_status_cat = ?)");
		searchMap.put("insertion_date_start"," and (insertion_date >= ?)");
		searchMap.put("insertion_date_end"," and (insertion_date <= ?)");
		searchMap.put("clearing_date_start"," and (clearing_date >= ?)");
		searchMap.put("clearing_date_end"," and (clearing_date <= ?)");
		searchMap.put("clearing_remarks"," and (clearing_remarks like ?)");
		searchMap.put("AnyField"," and (search_column like ?)");
	}

	private final Map<String,String> searchMap = new HashMap<>();

    private static class LazyLoader
    {
    	static final Asset_clearance_statusDAO INSTANCE = new Asset_clearance_statusDAO();
	}

	public static Asset_clearance_statusDAO getInstance()
	{
    	return LazyLoader.INSTANCE;
	}
	
	public void setSearchColumn(Asset_clearance_statusDTO asset_clearance_statusDTO)
	{
		asset_clearance_statusDTO.searchColumn = "";
		asset_clearance_statusDTO.searchColumn += CatDAO.getName("English", "asset_clearance_status", asset_clearance_statusDTO.assetClearanceStatusCat) + " " + CatDAO.getName("Bangla", "asset_clearance_status", asset_clearance_statusDTO.assetClearanceStatusCat) + " ";
		asset_clearance_statusDTO.searchColumn += asset_clearance_statusDTO.clearingRemarks + " ";
	}
	
	@Override
	public void set(PreparedStatement ps, Asset_clearance_statusDTO asset_clearance_statusDTO, boolean isInsert) throws SQLException
	{
		int index = 0;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(asset_clearance_statusDTO);
		if(isInsert)
		{
			ps.setObject(++index,asset_clearance_statusDTO.iD);
		}
		ps.setObject(++index,asset_clearance_statusDTO.supportTicketId);
		ps.setObject(++index,asset_clearance_statusDTO.assetClearanceLetterId);
		ps.setObject(++index,asset_clearance_statusDTO.assetAssigneeId);
		ps.setObject(++index,asset_clearance_statusDTO.assetClearanceStatusCat);
		ps.setObject(++index,asset_clearance_statusDTO.employeeRecordId);
		ps.setObject(++index,asset_clearance_statusDTO.organogramId);
		ps.setObject(++index,asset_clearance_statusDTO.insertedByUserId);
		ps.setObject(++index,asset_clearance_statusDTO.insertedByOrganogramId);
		ps.setObject(++index,asset_clearance_statusDTO.insertionDate);
		ps.setObject(++index,asset_clearance_statusDTO.clearedByUserId);
		ps.setObject(++index,asset_clearance_statusDTO.clearingDate);
		ps.setObject(++index,asset_clearance_statusDTO.clearingRemarks);
		ps.setObject(++index,asset_clearance_statusDTO.assetName);
		if(isInsert)
		{
			ps.setObject(++index,asset_clearance_statusDTO.isDeleted);
		}
		ps.setObject(++index, lastModificationTime);
		if(!isInsert)
		{
			ps.setObject(++index,asset_clearance_statusDTO.iD);
		}
	}
	
	@Override
	public Asset_clearance_statusDTO buildObjectFromResultSet(ResultSet rs)
	{
		try
		{
			Asset_clearance_statusDTO asset_clearance_statusDTO = new Asset_clearance_statusDTO();
			int i = 0;
			asset_clearance_statusDTO.iD = rs.getLong(columnNames[i++]);
			asset_clearance_statusDTO.supportTicketId = rs.getLong(columnNames[i++]);
			asset_clearance_statusDTO.assetClearanceLetterId = rs.getLong(columnNames[i++]);
			asset_clearance_statusDTO.assetAssigneeId = rs.getLong(columnNames[i++]);
			asset_clearance_statusDTO.assetClearanceStatusCat = rs.getInt(columnNames[i++]);
			asset_clearance_statusDTO.employeeRecordId = rs.getLong(columnNames[i++]);
			asset_clearance_statusDTO.organogramId = rs.getLong(columnNames[i++]);
			asset_clearance_statusDTO.insertedByUserId = rs.getLong(columnNames[i++]);
			asset_clearance_statusDTO.insertedByOrganogramId = rs.getLong(columnNames[i++]);
			asset_clearance_statusDTO.insertionDate = rs.getLong(columnNames[i++]);
			asset_clearance_statusDTO.clearedByUserId = rs.getLong(columnNames[i++]);
			asset_clearance_statusDTO.clearingDate = rs.getLong(columnNames[i++]);
			asset_clearance_statusDTO.clearingRemarks = rs.getString(columnNames[i++]);
			asset_clearance_statusDTO.assetName = rs.getString(columnNames[i++]);
			asset_clearance_statusDTO.isDeleted = rs.getInt(columnNames[i++]);
			asset_clearance_statusDTO.lastModificationTime = rs.getLong(columnNames[i++]);
			return asset_clearance_statusDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
		
	public Asset_clearance_statusDTO getDTOByID (long id)
	{
		return getDTOFromID(id);
	}

	@Override
	public String getTableName() {
		return "asset_clearance_status";
	}

	@Override
	public Map<String, String> getSearchMap() {
		return searchMap;
	}

	public long add(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Asset_clearance_statusDTO) commonDTO,addQuery,true);
	}

	public long update(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Asset_clearance_statusDTO) commonDTO,updateQuery,false);
	}
	
	@Override
	public String getLastModifierUser(){
		return "";
    }
	
	public List<Asset_clearance_statusDTO> getByTicketId(long ticketId) 
	{
        String sql = "select * from " + getTableName() + " WHERE support_ticket_id =" + ticketId
        		+ " and isDeleted = 0 order by asset_name asc"; //hardwares come first
        return ConnectionAndStatementUtil.getListOfT(sql,this::buildObjectFromResultSet);
    }
	
	public List<Asset_clearance_statusDTO> getByClearanceId(long clearanceId) 
	{
        String sql = "select * from " + getTableName() + " WHERE asset_clearance_letter_id =" + clearanceId
        		+ " and isDeleted = 0 order by asset_name asc"; //hardwares come first
        return ConnectionAndStatementUtil.getListOfT(sql,this::buildObjectFromResultSet);
    }
				
}
	