package asset_license;

import java.io.IOException;
import java.io.*;
import java.text.SimpleDateFormat;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import asset_license_key.Asset_license_keyDAO;
import asset_license_key.Asset_license_keyDTO;
import asset_license_key.Asset_license_keyRepository;
import employee_license_key.Employee_license_keyDAO;
import employee_license_key.Employee_license_keyDTO;
import org.apache.log4j.Logger;

import login.LoginDTO;
import permission.MenuConstants;
import role.PermissionRepository;


import sessionmanager.SessionConstants;

import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;
import util.CommonRequestHandler;
import util.RecordNavigationManager4;

import java.util.*;
import javax.servlet.http.*;
import java.util.UUID;





import geolocation.GeoLocationDAO2;
import language.LC;
import language.LM;

import java.util.StringTokenizer;

import com.google.gson.Gson;

import pb.*;
import pbReport.*;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;




/**
 * Servlet implementation class Asset_licenseServlet
 */
@WebServlet("/Asset_licenseServlet")
@MultipartConfig
public class Asset_licenseServlet extends HttpServlet 
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Asset_licenseServlet.class);

    String tableName = "asset_license";

	Asset_licenseDAO asset_licenseDAO;
	CommonRequestHandler commonRequestHandler;
    private final Gson gson = new Gson();
    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Asset_licenseServlet() 
	{
        super();
    	try
    	{
			asset_licenseDAO = new Asset_licenseDAO(tableName);
			commonRequestHandler = new CommonRequestHandler(asset_licenseDAO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }   

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		System.out.println("In doget request = " + request);
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		try
		{
			String actionType = request.getParameter("actionType");
			if(actionType.equals("getAddPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ASSET_LICENSE_ADD))
				{
					commonRequestHandler.getAddPage(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getEditPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ASSET_LICENSE_UPDATE))
				{
					getAsset_license(request, response);					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}						
			}
			else if(actionType.equals("getURL"))
			{
				String URL = request.getParameter("URL");
				System.out.println("URL = " + URL);
				response.sendRedirect(URL);			
			}			
			else if(actionType.equals("search"))
			{
				System.out.println("search requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ASSET_LICENSE_SEARCH))
				{
					if(isPermanentTable)
					{
						String filter = request.getParameter("filter");
						System.out.println("filter = " + filter);
						if(filter!=null)
						{
							filter = ""; //shouldn't be directly used, rather manipulate it.
							searchAsset_license(request, response, isPermanentTable, filter);
						}
						else
						{
							searchAsset_license(request, response, isPermanentTable, "");
						}
					}					
					else
					{
						//searchAsset_license(request, response, tempTableName, isPermanentTable);
					}
				}			
			}
			else if(actionType.equals("view"))
			{
				System.out.println("view requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ASSET_LICENSE_SEARCH))
				{
					commonRequestHandler.view(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else if(actionType.equals("assignLicense")) {
				assignLicense(request,response);
			}
			else if(actionType.equals("revokeLicense")) {
				revokeLicense(request,response);
			}
			else
			{
				request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}

	private void assignLicense(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String ID = request.getParameter("ID");
        Asset_licenseDTO asset_licenseDTO = Asset_licenseRepository.getInstance().getAsset_licenseDTOByID(Long.parseLong(ID));
       // List<Asset_license_keyDTO> keyDtos = Asset_license_keyRepository.getInstance().getAsset_license_keyDTOByasset_license_id(Long.parseLong(ID));
        request.setAttribute("asset_licenseDTO",asset_licenseDTO);
		request.setAttribute("assignOrRevoke",1);
		RequestDispatcher rd;
		rd = request.getRequestDispatcher("asset_license/assign_license.jsp");
		rd.forward(request, response);
	}
	private void revokeLicense(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String ID = request.getParameter("ID");
		Asset_licenseDTO asset_licenseDTO = Asset_licenseRepository.getInstance().getAsset_licenseDTOByID(Long.parseLong(ID));
		// List<Asset_license_keyDTO> keyDtos = Asset_license_keyRepository.getInstance().getAsset_license_keyDTOByasset_license_id(Long.parseLong(ID));
		request.setAttribute("asset_licenseDTO",asset_licenseDTO);
		request.setAttribute("assignOrRevoke",2);
		RequestDispatcher rd;
		rd = request.getRequestDispatcher("asset_license/assign_license.jsp");
		rd.forward(request, response);
	}

	
	

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		// TODO Auto-generated method stub
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		System.out.println("doPost");
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		
		try
		{
			String actionType = request.getParameter("actionType");
			System.out.println("actionType = " + actionType);
			if(actionType.equals("add"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ASSET_LICENSE_ADD))
				{
					System.out.println("going to  addAsset_license ");
					addAsset_license(request, response, true, userDTO, true);
				}
				else
				{
					System.out.println("Not going to  addAsset_license ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			
			else if(actionType.equals("getSoftware"))
			{
				long id = Long.parseLong(request.getParameter("id"));
				int cat = Integer.parseInt(request.getParameter("cat"));
				
				String options = CommonDAO.getOptionsWithWhere("English", "software_subtype", id, 
						" (software_cat = " + cat + ")"
						);

				
				options += "<option value='-1'>" + LM.getText(LC.HM_NONE) + "</option>";
				response.getWriter().write(options);
			}

			else if(actionType.equals("getDTO"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ASSET_LICENSE_ADD))
				{
					getDTO(request, response);					
				}
				else
				{
					System.out.println("Not going to  addAsset_license ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else if(actionType.equals("edit"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ASSET_LICENSE_UPDATE))
				{					
					addAsset_license(request, response, false, userDTO, isPermanentTable);					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("delete"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ASSET_LICENSE_UPDATE))
				{
					commonRequestHandler.delete(request, response, userDTO);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}	
			else if(actionType.equals("search"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ASSET_LICENSE_SEARCH))
				{
					searchAsset_license(request, response, true, "");
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("assignLicenseKey")) {
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ASSET_LICENSE_SEARCH))
				{
					addEmployeeLicenseKey(request,response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("revokeLicenseKey")) {
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ASSET_LICENSE_SEARCH))
				{
					revokeEmployeeLicenseKey(request,response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}
			else if(actionType.equals("getGeo"))
			{
				System.out.println("going to geoloc ");
				request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
			}
			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}

	private void addEmployeeLicenseKey(HttpServletRequest request, HttpServletResponse response) throws Exception {
		String value = "";

		value = request.getParameter("employeeRecordsId");
		long employeeRecordsId = 0;
		if(value != null)
		{
			value = Jsoup.clean(value,Whitelist.simpleText());
			employeeRecordsId = Long.parseLong(value);
		}

		value = request.getParameter("licenseKeyId");
		long licenseKeyId = 0;
		if(value != null)
		{
			value = Jsoup.clean(value,Whitelist.simpleText());
			licenseKeyId = Long.parseLong(value);
		}

		value = request.getParameter("remarks");
		if(value != null)
		{
			value = Jsoup.clean(value,Whitelist.simpleText());
		}

		addEmployeeLicenseKey(licenseKeyId, employeeRecordsId, value, 1);

		Asset_license_keyDTO keyDto = Asset_license_keyRepository.getInstance().getAsset_license_keyDTOByID(licenseKeyId);
		keyDto.isUsed = true;
		keyDto.employeeRecordsId = employeeRecordsId;
		new Asset_license_keyDAO().update(keyDto);

		response.sendRedirect("Asset_licenseServlet?actionType=search");
	}

	private void revokeEmployeeLicenseKey(HttpServletRequest request, HttpServletResponse response) throws Exception {
		String value = "";

		value = request.getParameter("employeeRecordsId");
		long employeeRecordsId = 0;
		if(value != null)
		{
			value = Jsoup.clean(value,Whitelist.simpleText());
			employeeRecordsId = Long.parseLong(value);
		}


		value = request.getParameter("remarks");
		String remarks = "";
		if(value != null)
		{
			value = Jsoup.clean(value,Whitelist.simpleText());
			remarks = value;
		}

		value = request.getParameter("assetLicenseId");
		long assetLicenseId = -1;
		if(value != null)
		{
			value = Jsoup.clean(value,Whitelist.simpleText());
			assetLicenseId = Long.parseLong(value);
		}
		Asset_licenseDTO asset_licenseDTO = Asset_licenseRepository.getInstance().getAsset_licenseDTOByID(assetLicenseId);

		List<Asset_license_keyDTO> licenseKeyDTOS = Asset_license_keyRepository.getInstance().getAsset_license_keyDTOByasset_license_id(assetLicenseId);
		for(Asset_license_keyDTO dto: licenseKeyDTOS) {
			if(dto.isUsed==true && dto.employeeRecordsId==employeeRecordsId) {
				if(asset_licenseDTO.isReassignable) {
					dto.isUsed = false;
				}
				dto.employeeRecordsId = -1;
				new Asset_license_keyDAO().update(dto);
				addEmployeeLicenseKey(dto.iD, employeeRecordsId, remarks, 2);
			}
		}

		response.sendRedirect("Asset_licenseServlet?actionType=search");
	}

	private void addEmployeeLicenseKey(long assetLicenseKeyId, long employeeRecordsId, String remarks, int assignOrRevoke) throws Exception {
		Employee_license_keyDTO employee_license_keyDTO = new Employee_license_keyDTO();
		employee_license_keyDTO.insertionDate = System.currentTimeMillis();
		employee_license_keyDTO.assetLicenseKeyId = assetLicenseKeyId;
		employee_license_keyDTO.employeeRecordsId = employeeRecordsId;
		employee_license_keyDTO.remarks = remarks;
		employee_license_keyDTO.assignOrRevoke = assignOrRevoke;
		new Employee_license_keyDAO().add(employee_license_keyDTO);
	}
	
	private void getDTO(HttpServletRequest request, HttpServletResponse response) 
	{
		try 
		{
			System.out.println("In getDTO");
			Asset_licenseDTO asset_licenseDTO = asset_licenseDAO.getDTOByID(Long.parseLong(request.getParameter("ID")));
			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			
			String encoded = this.gson.toJson(asset_licenseDTO);
			System.out.println("json encoded data = " + encoded);
			out.print(encoded);
			out.flush();
		}
		catch (NumberFormatException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	private void addAsset_license(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, boolean isPermanentTable) throws IOException 
	{
		// TODO Auto-generated method stub
		try 
		{
			request.setAttribute("failureMessage", "");
			System.out.println("%%%% addAsset_license");
			String path = getServletContext().getRealPath("/img2/");
			Asset_licenseDTO asset_licenseDTO;
			SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
						
			if(addFlag == true)
			{
				asset_licenseDTO = new Asset_licenseDTO();
			}
			else
			{
				asset_licenseDTO = asset_licenseDAO.getDTOByID(Long.parseLong(request.getParameter("iD")));
			}
			
			String Value = "";

			Value = request.getParameter("iD");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("iD = " + Value);
			if(Value != null)
			{
				
				asset_licenseDTO.iD = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			
			Value = request.getParameter("softwareCat");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("softwareCat = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				asset_licenseDTO.softwareCat = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			
			Value = request.getParameter("softwareSt");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("softwareSt = " + Value);
			if(Value != null)
			{
				asset_licenseDTO.softwareSt = Long.parseLong(Value);
				asset_licenseDTO.softwareName = CommonDAO.getName("English", "software_subtype", asset_licenseDTO.softwareSt);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}


			Value = request.getParameter("licenseStatus");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("licenseStatus = " + Value);
			if(Value != null)
			{

				asset_licenseDTO.status = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}



			Value = request.getParameter("numberOfLicense");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("numberOfLicense = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				asset_licenseDTO.numberOfLicense = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("licenseKey");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}

            List<String> keys = parseAndGetKey(Value);

			Value = request.getParameter("assetManufacturerId");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("assetManufacturerId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				asset_licenseDTO.assetManufacturerId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("licensedToName");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("licensedToName = " + Value);
			if(Value != null)
			{
				asset_licenseDTO.licensedToName = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("licensedToEmail");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("licensedToEmail = " + Value);
			if(Value != null)
			{
				asset_licenseDTO.licensedToEmail = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("isReassignable");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("isReassignable = " + Value);
            asset_licenseDTO.isReassignable = Value != null && !Value.equalsIgnoreCase("");

			Value = request.getParameter("expirationDate");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("expirationDate = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				try 
				{
					Date d = f.parse(Value);
					asset_licenseDTO.expirationDate = d.getTime();
				}
				catch (Exception e) 
				{
					e.printStackTrace();
				}
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("terminationDate");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("terminationDate = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				try 
				{
					Date d = f.parse(Value);
					asset_licenseDTO.terminationDate = d.getTime();
				}
				catch (Exception e) 
				{
					e.printStackTrace();
				}
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("purchaseDate");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("purchaseDate = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				try 
				{
					Date d = f.parse(Value);
					asset_licenseDTO.purchaseDate = d.getTime();
				}
				catch (Exception e) 
				{
					e.printStackTrace();
				}
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("assetSupplierId");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("assetSupplierId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				asset_licenseDTO.assetSupplierId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("orderNumber");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("orderNumber = " + Value);
			if(Value != null)
			{
				asset_licenseDTO.orderNumber = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("purchaseCost");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("purchaseCost = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				asset_licenseDTO.purchaseCost = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("isDepreciationApplicable");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("isDepreciationApplicable = " + Value);
            asset_licenseDTO.isDepreciationApplicable = Value != null && !Value.equalsIgnoreCase("");

			Value = request.getParameter("description");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("description = " + Value);
			if(Value != null)
			{
				asset_licenseDTO.description = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			if(addFlag)
			{
				asset_licenseDTO.insertedByUserId = userDTO.ID;
			}


			if(addFlag)
			{
				asset_licenseDTO.insertedByOrganogramId = userDTO.organogramID;
			}


			if(addFlag)
			{
				Calendar c = Calendar.getInstance();
				c.set(Calendar.HOUR_OF_DAY, 0);
				c.set(Calendar.MINUTE, 0);
				c.set(Calendar.SECOND, 0);
				c.set(Calendar.MILLISECOND, 0);
				
				asset_licenseDTO.insertionDate = c.getTimeInMillis();
			}			


			asset_licenseDTO.lastModifierUser = userDTO.userName;


			Value = request.getParameter("searchColumn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("searchColumn = " + Value);
			if(Value != null)
			{
				asset_licenseDTO.searchColumn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("status");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("status = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				asset_licenseDTO.status = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			
			System.out.println("Done adding  addAsset_license dto = " + asset_licenseDTO);
			long returnedID = -1;
			
			if(isPermanentTable == false) //add new row for validation and make the old row outdated
			{
				asset_licenseDAO.setIsDeleted(asset_licenseDTO.iD, CommonDTO.OUTDATED);
				returnedID = asset_licenseDAO.add(asset_licenseDTO);
				asset_licenseDAO.setIsDeleted(returnedID, CommonDTO.WAITING_FOR_APPROVAL);
			}
			else if(addFlag == true)
			{
				returnedID = asset_licenseDAO.manageWriteOperations(asset_licenseDTO, SessionConstants.INSERT, -1, userDTO);
			}
			else
			{				
				returnedID = asset_licenseDAO.manageWriteOperations(asset_licenseDTO, SessionConstants.UPDATE, -1, userDTO);											
			}
			
			
			if(returnedID!=-1) {
				addLicenseKey(keys, returnedID);
			}
			
			
			
			
			
			
			if(isPermanentTable)
			{
				String inPlaceSubmit = request.getParameter("inplacesubmit");
				
				if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
				{
					getAsset_license(request, response, returnedID);
				}
				else
				{
					response.sendRedirect("Asset_licenseServlet?actionType=search");
				}
			}
			else
			{
				commonRequestHandler.validate(asset_licenseDAO.getDTOByID(returnedID), request, response, userDTO);
			}
					
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}

	private void addLicenseKey(List<String> keys, long assetLicenseId) throws Exception {
		Asset_license_keyDAO dao = new Asset_license_keyDAO();
		for(String key: keys) {
			Asset_license_keyDTO dto = new Asset_license_keyDTO();
			dto.assetLicenseId = assetLicenseId;
			dto.productKey = key;
			dao.add(dto);
		}
		Asset_license_keyRepository.getInstance().reload(false);

	}
	
	
    private List<String> parseAndGetKey(String keyString) {
		if(keyString==null || keyString.isEmpty()) return  new ArrayList<>();
		int len = keyString.length();
		int i = 0;
		List<String> keys = new ArrayList<>();
		StringBuilder key = new StringBuilder();
		while(i<len) {
			while(i<len && keyString.charAt(i) != ' ' && keyString.charAt(i) != '\n') {
				key.append(keyString.charAt(i));
				i++;
			}
			i++;
			if(key.toString().length()>0)
			    keys.add(key.toString());
			key.setLength(0);
		}
		return keys;
	}


	
	
	

	private void getAsset_license(HttpServletRequest request, HttpServletResponse response, long id) throws ServletException, IOException
	{
		System.out.println("in getAsset_license");
		Asset_licenseDTO asset_licenseDTO = null;
		try 
		{
			asset_licenseDTO = asset_licenseDAO.getDTOByID(id);
			request.setAttribute("ID", asset_licenseDTO.iD);
			request.setAttribute("asset_licenseDTO",asset_licenseDTO);
			request.setAttribute("asset_licenseDAO",asset_licenseDAO);
			
			String URL= "";
			
			String inPlaceEdit = request.getParameter("inplaceedit");
			String inPlaceSubmit = request.getParameter("inplacesubmit");
			String getBodyOnly = request.getParameter("getBodyOnly");
			
			if(inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase(""))
			{
				URL = "asset_license/asset_licenseInPlaceEdit.jsp";	
				request.setAttribute("inplaceedit","");				
			}
			else if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
			{
				URL = "asset_license/asset_licenseSearchRow.jsp";
				request.setAttribute("inplacesubmit","");					
			}
			else
			{
				if(getBodyOnly  != null && !getBodyOnly.equalsIgnoreCase(""))
				{
					URL = "asset_license/asset_licenseEditBody.jsp?actionType=edit";
				}
				else
				{
					URL = "asset_license/asset_licenseEdit.jsp?actionType=edit";
				}				
			}
			
			RequestDispatcher rd = request.getRequestDispatcher(URL);
			rd.forward(request, response);
		}
		catch (NumberFormatException e) 
		{
			e.printStackTrace();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	
	private void getAsset_license(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		getAsset_license(request, response, Long.parseLong(request.getParameter("ID")));	
	}
	
	private void searchAsset_license(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException
	{
		System.out.println("in  searchAsset_license 1");
		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String ajax = request.getParameter("ajax");
		boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);
		
        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
			SessionConstants.NAV_ASSET_LICENSE,
			request,
			asset_licenseDAO,
			SessionConstants.VIEW_ASSET_LICENSE,
			SessionConstants.SEARCH_ASSET_LICENSE,
			tableName,
			isPermanent,
			userDTO,
			filter,
			true);
        try
        {
			System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        }
        catch(Exception e)
        {
			System.out.println("failed to dojob" + e);
        }

		request.setAttribute("asset_licenseDAO",asset_licenseDAO);
        RequestDispatcher rd;
        if(!isPermanent)
        {
        	if(hasAjax == false)
	        {
	        	System.out.println("Going to asset_license/asset_licenseApproval.jsp");
	        	rd = request.getRequestDispatcher("asset_license/asset_licenseApproval.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to asset_license/asset_licenseApprovalForm.jsp");
	        	rd = request.getRequestDispatcher("asset_license/asset_licenseApprovalForm.jsp");
	        }
        }
        else
        {
	        if(hasAjax == false)
	        {
	        	System.out.println("Going to asset_license/asset_licenseSearch.jsp");
	        	rd = request.getRequestDispatcher("asset_license/asset_licenseSearch.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to asset_license/asset_licenseSearchForm.jsp");
	        	rd = request.getRequestDispatcher("asset_license/asset_licenseSearchForm.jsp");
	        }
        }
		rd.forward(request, response);
	}
	
}

