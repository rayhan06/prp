package asset_license;

import asset_license_key.Asset_license_keyDTO;
import asset_license_key.Asset_license_keyRepository;
import common.ConnectionAndStatementUtil;
import employee_records.Employee_recordsDAO;
import org.apache.log4j.Logger;
import pb.CatDAO;
import pb.OptionDTO;
import pb.Utils;
import repository.RepositoryManager;
import user.UserDTO;
import util.CommonDTO;
import util.NavigationService4;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;

public class Asset_licenseDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());

	public Asset_licenseDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";
		commonMaps = new Asset_licenseMAPS(tableName);
		columnNames = new String[] 
		{
			"ID",
			"software_name",
			"software_cat",
			"number_of_license",
			"asset_manufacturer_id",
			"licensed_to_name",
			"licensed_to_email",
			"is_reassignable",
			"expiration_date",
			"termination_date",
			"purchase_date",
			"asset_supplier_id",
			"order_number",
			"purchase_cost",
			"is_depreciation_applicable",
			"description",
			"inserted_by_user_id",
			"inserted_by_organogram_id",
			"insertion_date",
			"last_modifier_user",
			"search_column",
			"status",
			"software_subtype_type",
			"isDeleted",
			"lastModificationTime"
		};
	}
	
	public Asset_licenseDAO()
	{
		this("asset_license");		
	}
	
	public void setSearchColumn(Asset_licenseDTO asset_licenseDTO)
	{
		asset_licenseDTO.searchColumn = "";
		asset_licenseDTO.searchColumn += asset_licenseDTO.softwareName + " ";
		asset_licenseDTO.searchColumn += CatDAO.getName("English", "software", asset_licenseDTO.softwareCat) + " " + CatDAO.getName("Bangla", "software", asset_licenseDTO.softwareCat) + " ";
		asset_licenseDTO.searchColumn += asset_licenseDTO.licensedToName + " ";
		asset_licenseDTO.searchColumn += asset_licenseDTO.licensedToEmail + " ";
		asset_licenseDTO.searchColumn += asset_licenseDTO.orderNumber + " ";
	}
	
	public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException
	{
		Asset_licenseDTO asset_licenseDTO = (Asset_licenseDTO)commonDTO;
		int index = 1;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(asset_licenseDTO);
		if(isInsert)
		{
			ps.setObject(index++,asset_licenseDTO.iD);
		}
		ps.setObject(index++,asset_licenseDTO.softwareName);
		ps.setObject(index++,asset_licenseDTO.softwareCat);
		ps.setObject(index++,asset_licenseDTO.numberOfLicense);
		ps.setObject(index++,asset_licenseDTO.assetManufacturerId);
		ps.setObject(index++,asset_licenseDTO.licensedToName);
		ps.setObject(index++,asset_licenseDTO.licensedToEmail);
		ps.setObject(index++,asset_licenseDTO.isReassignable);
		ps.setObject(index++,asset_licenseDTO.expirationDate);
		ps.setObject(index++,asset_licenseDTO.terminationDate);
		ps.setObject(index++,asset_licenseDTO.purchaseDate);
		ps.setObject(index++,asset_licenseDTO.assetSupplierId);
		ps.setObject(index++,asset_licenseDTO.orderNumber);
		ps.setObject(index++,asset_licenseDTO.purchaseCost);
		ps.setObject(index++,asset_licenseDTO.isDepreciationApplicable);
		ps.setObject(index++,asset_licenseDTO.description);
		ps.setObject(index++,asset_licenseDTO.insertedByUserId);
		ps.setObject(index++,asset_licenseDTO.insertedByOrganogramId);
		ps.setObject(index++,asset_licenseDTO.insertionDate);
		ps.setObject(index++,asset_licenseDTO.lastModifierUser);
		ps.setObject(index++,asset_licenseDTO.searchColumn);
		ps.setObject(index++,asset_licenseDTO.status);
		ps.setObject(index++,asset_licenseDTO.softwareSt);
		if(isInsert)
		{
			ps.setObject(index++, 0);
			ps.setObject(index++, lastModificationTime);
		}
	}
	
	public Asset_licenseDTO build(ResultSet rs)
	{
		try
		{
			Asset_licenseDTO asset_licenseDTO = new Asset_licenseDTO();
			asset_licenseDTO.iD = rs.getLong("ID");
			asset_licenseDTO.softwareName = rs.getString("software_name");
			asset_licenseDTO.softwareCat = rs.getInt("software_cat");
			asset_licenseDTO.numberOfLicense = rs.getInt("number_of_license");
			asset_licenseDTO.assetManufacturerId = rs.getLong("asset_manufacturer_id");
			asset_licenseDTO.licensedToName = rs.getString("licensed_to_name");
			asset_licenseDTO.licensedToEmail = rs.getString("licensed_to_email");
			asset_licenseDTO.isReassignable = rs.getBoolean("is_reassignable");
			asset_licenseDTO.expirationDate = rs.getLong("expiration_date");
			asset_licenseDTO.terminationDate = rs.getLong("termination_date");
			asset_licenseDTO.purchaseDate = rs.getLong("purchase_date");
			asset_licenseDTO.assetSupplierId = rs.getLong("asset_supplier_id");
			asset_licenseDTO.orderNumber = rs.getString("order_number");
			asset_licenseDTO.purchaseCost = rs.getInt("purchase_cost");
			asset_licenseDTO.isDepreciationApplicable = rs.getBoolean("is_depreciation_applicable");
			asset_licenseDTO.description = rs.getString("description");
			asset_licenseDTO.insertedByUserId = rs.getLong("inserted_by_user_id");
			asset_licenseDTO.insertedByOrganogramId = rs.getLong("inserted_by_organogram_id");
			asset_licenseDTO.insertionDate = rs.getLong("insertion_date");
			asset_licenseDTO.lastModifierUser = rs.getString("last_modifier_user");
			asset_licenseDTO.searchColumn = rs.getString("search_column");
			asset_licenseDTO.status = rs.getInt("status");
			asset_licenseDTO.softwareSt = rs.getInt("software_subtype_type");
			asset_licenseDTO.isDeleted = rs.getInt("isDeleted");
			asset_licenseDTO.lastModificationTime = rs.getLong("lastModificationTime");
			return asset_licenseDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
	
	

	public Asset_licenseDTO getDTOByID (long id)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID = " + id;
		Asset_licenseDTO asset_licenseDTO = ConnectionAndStatementUtil.getT(sql,this::build);
		return asset_licenseDTO;
	}

	
	public List<Asset_licenseDTO> getDTOs(Collection recordIDs)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID IN ( ";
		for(int i = 0;i<recordIDs.size();i++){
			if(i!=0){
				sql+=",";
			}
			sql+=((ArrayList)recordIDs).get(i);
		}
		sql+=")  order by lastModificationTime desc";
		printSql(sql);
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);		
	}
	
	public List<Asset_licenseDTO> getAllAsset_license (boolean isFirstReload)
    {
		String sql = "SELECT * FROM " + tableName + " WHERE ";
		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by " + tableName + ".lastModificationTime desc";
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);	
    }

	
	public List<Asset_licenseDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO)
	{
		return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
	}

	
	public List<Asset_licenseDTO> getDTOs(Hashtable  p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
			String filter, boolean tableHasJobCat)
	{
		String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat);
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);			
	}
	public String getSqlWithSearchCriteriaForNormalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
			UserDTO userDTO, String filter, boolean tableHasJobCat)
    {
		boolean viewAll = false;
		
		if(p_searchCriteria != null && p_searchCriteria.get("ViewAll") != null)
		{
			System.out.println("ViewAll = " + p_searchCriteria.get("ViewAll"));
			viewAll = true;
		}
		else
		{
			System.out.println("ViewAll is null ");
		}
		
		String sql = "SELECT ";
		if(category == GETIDS)
		{
			sql += " " + tableName + ".ID as ID ";
		}
		else if(category == GETCOUNT)
		{
			sql += " count( " + tableName + ".ID) as countID ";
		}
		else if(category == GETDTOS)
		{
			sql += " distinct " + tableName + ".* ";
		}
		sql += "FROM " + tableName + " ";
		sql += joinSQL;
		

		
		String AnyfieldSql = "";
		String AllFieldSql = "";
		
		if(p_searchCriteria != null)
		{
						
			
			Enumeration names = p_searchCriteria.keys();
			String str, value;
			
			AnyfieldSql = "(";
			
			if(p_searchCriteria.get("AnyField")!= null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase(""))
			{
				AnyfieldSql+= tableName + ".search_column like '%" + p_searchCriteria.get("AnyField").toString() + "%'";			
			}
			AnyfieldSql += ")";
			System.out.println("AnyfieldSql = " + AnyfieldSql);
			
			AllFieldSql = "(";
			int i = 0;
			while(names.hasMoreElements())
			{				
				str = (String) names.nextElement();
				value = (String)p_searchCriteria.get(str);
		        System.out.println(str + ": " + value);
				if(value != null && !value.equalsIgnoreCase("") && (
						 str.equals("software_name")
						|| str.equals("software_cat")
						|| str.equals("licensed_to_name")
						|| str.equals("licensed_to_email")
						|| str.equals("expiration_date_start")
						|| str.equals("expiration_date_end")
						|| str.equals("termination_date_start")
						|| str.equals("termination_date_end")
						|| str.equals("purchase_date_start")
						|| str.equals("purchase_date_end")
						|| str.equals("order_number")
						|| str.equals("description")
						|| str.equals("insertion_date_start")
						|| str.equals("insertion_date_end")
				)
						
				)
				{
					if(p_searchCriteria.get(str).equals("any"))
					{
						continue;
					}

					if( i > 0)
					{
						AllFieldSql+= " AND  ";
					}
					
					 if(str.equals("software_name"))
					{
						AllFieldSql += "" + tableName + ".software_name like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("software_cat"))
					{
						AllFieldSql += "" + tableName + ".software_cat = " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("licensed_to_name"))
					{
						AllFieldSql += "" + tableName + ".licensed_to_name like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("licensed_to_email"))
					{
						AllFieldSql += "" + tableName + ".licensed_to_email like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("expiration_date_start"))
					{
						AllFieldSql += "" + tableName + ".expiration_date >= " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("expiration_date_end"))
					{
						AllFieldSql += "" + tableName + ".expiration_date <= " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("termination_date_start"))
					{
						AllFieldSql += "" + tableName + ".termination_date >= " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("termination_date_end"))
					{
						AllFieldSql += "" + tableName + ".termination_date <= " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("purchase_date_start"))
					{
						AllFieldSql += "" + tableName + ".purchase_date >= " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("purchase_date_end"))
					{
						AllFieldSql += "" + tableName + ".purchase_date <= " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("order_number"))
					{
						AllFieldSql += "" + tableName + ".order_number like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("description"))
					{
						AllFieldSql += "" + tableName + ".description like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("insertion_date_start"))
					{
						AllFieldSql += "" + tableName + ".insertion_date >= " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("insertion_date_end"))
					{
						AllFieldSql += "" + tableName + ".insertion_date <= " + p_searchCriteria.get(str);
						i ++;
					}
					
					
				}
			}
			
			AllFieldSql += ")";
			System.out.println("AllFieldSql = " + AllFieldSql);
			
			
		}
		
		
		sql += " WHERE ";
		
		sql += " (" + tableName + ".isDeleted = 0 ";			
		sql += ")";
		
		
		if(!filter.equalsIgnoreCase(""))
		{
			sql += " and " + filter + " ";
		}
		
		if(!AnyfieldSql.equals("()") && !AnyfieldSql.equals(""))
		{
			sql += " AND " + AnyfieldSql;
			
		}
		if(!AllFieldSql.equals("()") && !AllFieldSql.equals(""))
		{			
			sql += " AND " + AllFieldSql;
		}
		
	
			
		sql += " order by " + tableName + ".lastModificationTime desc ";

		printSql(sql);
		
		if(limit >= 0)
		{
			sql += " limit " + limit;
		}
		if(offset >= 0)
		{
			sql += " offset " + offset;
		}
		
		System.out.println("-------------- sql = " + sql);
		
		return sql;
    }
	
	public String getSqlWithSearchCriteria(Hashtable p_searchCriteria, int limit, int offset, int category,
			boolean isPermanentTable, UserDTO userDTO, String filter, boolean tableHasJobCat)
    {		
		return getSqlWithSearchCriteriaForNormalSearch(p_searchCriteria, limit, offset, category, userDTO, filter, tableHasJobCat);	
    }
				
    public String buildOptionsAssignedEmployee(long assetLicenseId, String language) {
    	String smLanguage = language.toLowerCase();
    	if(!smLanguage.equalsIgnoreCase("bangla") && !smLanguage.equalsIgnoreCase("english"))
    	{
    		language = "bangla";
    	}
		List<Asset_license_keyDTO> keyDtos = Asset_license_keyRepository.getInstance().getAsset_license_keyDTOByasset_license_id(assetLicenseId);
		List<OptionDTO> optionDTOList = null;
		optionDTOList = keyDtos.stream()
				.filter(dto -> dto.isUsed)
				.filter(dto -> dto.employeeRecordsId!=-1)
				.map(dto -> new OptionDTO(Employee_recordsDAO.getEmployeeName(dto.employeeRecordsId,"English")+ " | " + dto.productKey, Employee_recordsDAO.getEmployeeName(dto.employeeRecordsId, "Bangla")+ " | " + dto.productKey, String.valueOf(dto.employeeRecordsId)))
				.collect(Collectors.toList());
		return Utils.buildOptions(optionDTOList,language, null);
	}

}
	