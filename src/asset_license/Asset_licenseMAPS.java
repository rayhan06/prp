package asset_license;
import java.util.*; 
import util.*;


public class Asset_licenseMAPS extends CommonMaps
{	
	public Asset_licenseMAPS(String tableName)
	{
		

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("softwareName".toLowerCase(), "softwareName".toLowerCase());
		java_DTO_map.put("softwareCat".toLowerCase(), "softwareCat".toLowerCase());
		java_DTO_map.put("numberOfLicense".toLowerCase(), "numberOfLicense".toLowerCase());
		java_DTO_map.put("assetManufacturerId".toLowerCase(), "assetManufacturerId".toLowerCase());
		java_DTO_map.put("licensedToName".toLowerCase(), "licensedToName".toLowerCase());
		java_DTO_map.put("licensedToEmail".toLowerCase(), "licensedToEmail".toLowerCase());
		java_DTO_map.put("isReassignable".toLowerCase(), "isReassignable".toLowerCase());
		java_DTO_map.put("expirationDate".toLowerCase(), "expirationDate".toLowerCase());
		java_DTO_map.put("terminationDate".toLowerCase(), "terminationDate".toLowerCase());
		java_DTO_map.put("purchaseDate".toLowerCase(), "purchaseDate".toLowerCase());
		java_DTO_map.put("assetSupplierId".toLowerCase(), "assetSupplierId".toLowerCase());
		java_DTO_map.put("orderNumber".toLowerCase(), "orderNumber".toLowerCase());
		java_DTO_map.put("purchaseCost".toLowerCase(), "purchaseCost".toLowerCase());
		java_DTO_map.put("isDepreciationApplicable".toLowerCase(), "isDepreciationApplicable".toLowerCase());
		java_DTO_map.put("description".toLowerCase(), "description".toLowerCase());
		java_DTO_map.put("insertedByUserId".toLowerCase(), "insertedByUserId".toLowerCase());
		java_DTO_map.put("insertedByOrganogramId".toLowerCase(), "insertedByOrganogramId".toLowerCase());
		java_DTO_map.put("insertionDate".toLowerCase(), "insertionDate".toLowerCase());
		java_DTO_map.put("lastModifierUser".toLowerCase(), "lastModifierUser".toLowerCase());
		java_DTO_map.put("searchColumn".toLowerCase(), "searchColumn".toLowerCase());
		java_DTO_map.put("status".toLowerCase(), "status".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("software_name".toLowerCase(), "softwareName".toLowerCase());
		java_SQL_map.put("software_cat".toLowerCase(), "softwareCat".toLowerCase());
		java_SQL_map.put("number_of_license".toLowerCase(), "numberOfLicense".toLowerCase());
		java_SQL_map.put("asset_manufacturer_id".toLowerCase(), "assetManufacturerId".toLowerCase());
		java_SQL_map.put("licensed_to_name".toLowerCase(), "licensedToName".toLowerCase());
		java_SQL_map.put("licensed_to_email".toLowerCase(), "licensedToEmail".toLowerCase());
		java_SQL_map.put("is_reassignable".toLowerCase(), "isReassignable".toLowerCase());
		java_SQL_map.put("expiration_date".toLowerCase(), "expirationDate".toLowerCase());
		java_SQL_map.put("termination_date".toLowerCase(), "terminationDate".toLowerCase());
		java_SQL_map.put("purchase_date".toLowerCase(), "purchaseDate".toLowerCase());
		java_SQL_map.put("asset_supplier_id".toLowerCase(), "assetSupplierId".toLowerCase());
		java_SQL_map.put("order_number".toLowerCase(), "orderNumber".toLowerCase());
		java_SQL_map.put("purchase_cost".toLowerCase(), "purchaseCost".toLowerCase());
		java_SQL_map.put("is_depreciation_applicable".toLowerCase(), "isDepreciationApplicable".toLowerCase());
		java_SQL_map.put("description".toLowerCase(), "description".toLowerCase());
		java_SQL_map.put("status".toLowerCase(), "status".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Software Name".toLowerCase(), "softwareName".toLowerCase());
		java_Text_map.put("Software".toLowerCase(), "softwareCat".toLowerCase());
		java_Text_map.put("Number Of License".toLowerCase(), "numberOfLicense".toLowerCase());
		java_Text_map.put("Asset Manufacturer Id".toLowerCase(), "assetManufacturerId".toLowerCase());
		java_Text_map.put("Licensed To Name".toLowerCase(), "licensedToName".toLowerCase());
		java_Text_map.put("Licensed To Email".toLowerCase(), "licensedToEmail".toLowerCase());
		java_Text_map.put("Is Reassignable".toLowerCase(), "isReassignable".toLowerCase());
		java_Text_map.put("Expiration Date".toLowerCase(), "expirationDate".toLowerCase());
		java_Text_map.put("Termination Date".toLowerCase(), "terminationDate".toLowerCase());
		java_Text_map.put("Purchase Date".toLowerCase(), "purchaseDate".toLowerCase());
		java_Text_map.put("Asset Supplier Id".toLowerCase(), "assetSupplierId".toLowerCase());
		java_Text_map.put("Order Number".toLowerCase(), "orderNumber".toLowerCase());
		java_Text_map.put("Purchase Cost".toLowerCase(), "purchaseCost".toLowerCase());
		java_Text_map.put("Is Depreciation Applicable".toLowerCase(), "isDepreciationApplicable".toLowerCase());
		java_Text_map.put("Description".toLowerCase(), "description".toLowerCase());
		java_Text_map.put("Inserted By User Id".toLowerCase(), "insertedByUserId".toLowerCase());
		java_Text_map.put("Inserted By Organogram Id".toLowerCase(), "insertedByOrganogramId".toLowerCase());
		java_Text_map.put("Insertion Date".toLowerCase(), "insertionDate".toLowerCase());
		java_Text_map.put("Last Modifier User".toLowerCase(), "lastModifierUser".toLowerCase());
		java_Text_map.put("Search Column".toLowerCase(), "searchColumn".toLowerCase());
		java_Text_map.put("Status".toLowerCase(), "status".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}