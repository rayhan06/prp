package asset_license;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class Asset_licenseRepository implements Repository {
	Asset_licenseDAO asset_licenseDAO = null;
	
	public void setDAO(Asset_licenseDAO asset_licenseDAO)
	{
		this.asset_licenseDAO = asset_licenseDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Asset_licenseRepository.class);
	Map<Long, Asset_licenseDTO>mapOfAsset_licenseDTOToiD;
	Map<String, Set<Asset_licenseDTO> >mapOfAsset_licenseDTOTosoftwareName;
	Map<Integer, Set<Asset_licenseDTO> >mapOfAsset_licenseDTOTosoftwareCat;
	Map<Integer, Set<Asset_licenseDTO> >mapOfAsset_licenseDTOTonumberOfLicense;
	Map<Long, Set<Asset_licenseDTO> >mapOfAsset_licenseDTOToassetManufacturerId;
	Map<String, Set<Asset_licenseDTO> >mapOfAsset_licenseDTOTolicensedToName;
	Map<String, Set<Asset_licenseDTO> >mapOfAsset_licenseDTOTolicensedToEmail;
	Map<Boolean, Set<Asset_licenseDTO> >mapOfAsset_licenseDTOToisReassignable;
	Map<Long, Set<Asset_licenseDTO> >mapOfAsset_licenseDTOToexpirationDate;
	Map<Long, Set<Asset_licenseDTO> >mapOfAsset_licenseDTOToterminationDate;
	Map<Long, Set<Asset_licenseDTO> >mapOfAsset_licenseDTOTopurchaseDate;
	Map<Long, Set<Asset_licenseDTO> >mapOfAsset_licenseDTOToassetSupplierId;
	Map<String, Set<Asset_licenseDTO> >mapOfAsset_licenseDTOToorderNumber;
	Map<Integer, Set<Asset_licenseDTO> >mapOfAsset_licenseDTOTopurchaseCost;
	Map<Boolean, Set<Asset_licenseDTO> >mapOfAsset_licenseDTOToisDepreciationApplicable;
	Map<String, Set<Asset_licenseDTO> >mapOfAsset_licenseDTOTodescription;
	Map<Long, Set<Asset_licenseDTO> >mapOfAsset_licenseDTOToinsertedByUserId;
	Map<Long, Set<Asset_licenseDTO> >mapOfAsset_licenseDTOToinsertedByOrganogramId;
	Map<Long, Set<Asset_licenseDTO> >mapOfAsset_licenseDTOToinsertionDate;
	Map<String, Set<Asset_licenseDTO> >mapOfAsset_licenseDTOTolastModifierUser;
	Map<String, Set<Asset_licenseDTO> >mapOfAsset_licenseDTOTosearchColumn;
	Map<Integer, Set<Asset_licenseDTO> >mapOfAsset_licenseDTOTostatus;
	Map<Long, Set<Asset_licenseDTO> >mapOfAsset_licenseDTOTolastModificationTime;


	static Asset_licenseRepository instance = null;  
	private Asset_licenseRepository(){
		mapOfAsset_licenseDTOToiD = new ConcurrentHashMap<>();
		mapOfAsset_licenseDTOTosoftwareName = new ConcurrentHashMap<>();
		mapOfAsset_licenseDTOTosoftwareCat = new ConcurrentHashMap<>();
		mapOfAsset_licenseDTOTonumberOfLicense = new ConcurrentHashMap<>();
		mapOfAsset_licenseDTOToassetManufacturerId = new ConcurrentHashMap<>();
		mapOfAsset_licenseDTOTolicensedToName = new ConcurrentHashMap<>();
		mapOfAsset_licenseDTOTolicensedToEmail = new ConcurrentHashMap<>();
		mapOfAsset_licenseDTOToisReassignable = new ConcurrentHashMap<>();
		mapOfAsset_licenseDTOToexpirationDate = new ConcurrentHashMap<>();
		mapOfAsset_licenseDTOToterminationDate = new ConcurrentHashMap<>();
		mapOfAsset_licenseDTOTopurchaseDate = new ConcurrentHashMap<>();
		mapOfAsset_licenseDTOToassetSupplierId = new ConcurrentHashMap<>();
		mapOfAsset_licenseDTOToorderNumber = new ConcurrentHashMap<>();
		mapOfAsset_licenseDTOTopurchaseCost = new ConcurrentHashMap<>();
		mapOfAsset_licenseDTOToisDepreciationApplicable = new ConcurrentHashMap<>();
		mapOfAsset_licenseDTOTodescription = new ConcurrentHashMap<>();
		mapOfAsset_licenseDTOToinsertedByUserId = new ConcurrentHashMap<>();
		mapOfAsset_licenseDTOToinsertedByOrganogramId = new ConcurrentHashMap<>();
		mapOfAsset_licenseDTOToinsertionDate = new ConcurrentHashMap<>();
		mapOfAsset_licenseDTOTolastModifierUser = new ConcurrentHashMap<>();
		mapOfAsset_licenseDTOTosearchColumn = new ConcurrentHashMap<>();
		mapOfAsset_licenseDTOTostatus = new ConcurrentHashMap<>();
		mapOfAsset_licenseDTOTolastModificationTime = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Asset_licenseRepository getInstance(){
		if (instance == null){
			instance = new Asset_licenseRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(asset_licenseDAO == null)
		{
			return;
		}
		try {
			List<Asset_licenseDTO> asset_licenseDTOs = asset_licenseDAO.getAllAsset_license(reloadAll);
			for(Asset_licenseDTO asset_licenseDTO : asset_licenseDTOs) {
				Asset_licenseDTO oldAsset_licenseDTO = getAsset_licenseDTOByID(asset_licenseDTO.iD);
				if( oldAsset_licenseDTO != null ) {
					mapOfAsset_licenseDTOToiD.remove(oldAsset_licenseDTO.iD);
				
					if(mapOfAsset_licenseDTOTosoftwareName.containsKey(oldAsset_licenseDTO.softwareName)) {
						mapOfAsset_licenseDTOTosoftwareName.get(oldAsset_licenseDTO.softwareName).remove(oldAsset_licenseDTO);
					}
					if(mapOfAsset_licenseDTOTosoftwareName.get(oldAsset_licenseDTO.softwareName).isEmpty()) {
						mapOfAsset_licenseDTOTosoftwareName.remove(oldAsset_licenseDTO.softwareName);
					}
					
					if(mapOfAsset_licenseDTOTosoftwareCat.containsKey(oldAsset_licenseDTO.softwareCat)) {
						mapOfAsset_licenseDTOTosoftwareCat.get(oldAsset_licenseDTO.softwareCat).remove(oldAsset_licenseDTO);
					}
					if(mapOfAsset_licenseDTOTosoftwareCat.get(oldAsset_licenseDTO.softwareCat).isEmpty()) {
						mapOfAsset_licenseDTOTosoftwareCat.remove(oldAsset_licenseDTO.softwareCat);
					}
					
					if(mapOfAsset_licenseDTOTonumberOfLicense.containsKey(oldAsset_licenseDTO.numberOfLicense)) {
						mapOfAsset_licenseDTOTonumberOfLicense.get(oldAsset_licenseDTO.numberOfLicense).remove(oldAsset_licenseDTO);
					}
					if(mapOfAsset_licenseDTOTonumberOfLicense.get(oldAsset_licenseDTO.numberOfLicense).isEmpty()) {
						mapOfAsset_licenseDTOTonumberOfLicense.remove(oldAsset_licenseDTO.numberOfLicense);
					}
					
					if(mapOfAsset_licenseDTOToassetManufacturerId.containsKey(oldAsset_licenseDTO.assetManufacturerId)) {
						mapOfAsset_licenseDTOToassetManufacturerId.get(oldAsset_licenseDTO.assetManufacturerId).remove(oldAsset_licenseDTO);
					}
					if(mapOfAsset_licenseDTOToassetManufacturerId.get(oldAsset_licenseDTO.assetManufacturerId).isEmpty()) {
						mapOfAsset_licenseDTOToassetManufacturerId.remove(oldAsset_licenseDTO.assetManufacturerId);
					}
					
					if(mapOfAsset_licenseDTOTolicensedToName.containsKey(oldAsset_licenseDTO.licensedToName)) {
						mapOfAsset_licenseDTOTolicensedToName.get(oldAsset_licenseDTO.licensedToName).remove(oldAsset_licenseDTO);
					}
					if(mapOfAsset_licenseDTOTolicensedToName.get(oldAsset_licenseDTO.licensedToName).isEmpty()) {
						mapOfAsset_licenseDTOTolicensedToName.remove(oldAsset_licenseDTO.licensedToName);
					}
					
					if(mapOfAsset_licenseDTOTolicensedToEmail.containsKey(oldAsset_licenseDTO.licensedToEmail)) {
						mapOfAsset_licenseDTOTolicensedToEmail.get(oldAsset_licenseDTO.licensedToEmail).remove(oldAsset_licenseDTO);
					}
					if(mapOfAsset_licenseDTOTolicensedToEmail.get(oldAsset_licenseDTO.licensedToEmail).isEmpty()) {
						mapOfAsset_licenseDTOTolicensedToEmail.remove(oldAsset_licenseDTO.licensedToEmail);
					}
					
					if(mapOfAsset_licenseDTOToisReassignable.containsKey(oldAsset_licenseDTO.isReassignable)) {
						mapOfAsset_licenseDTOToisReassignable.get(oldAsset_licenseDTO.isReassignable).remove(oldAsset_licenseDTO);
					}
					if(mapOfAsset_licenseDTOToisReassignable.get(oldAsset_licenseDTO.isReassignable).isEmpty()) {
						mapOfAsset_licenseDTOToisReassignable.remove(oldAsset_licenseDTO.isReassignable);
					}
					
					if(mapOfAsset_licenseDTOToexpirationDate.containsKey(oldAsset_licenseDTO.expirationDate)) {
						mapOfAsset_licenseDTOToexpirationDate.get(oldAsset_licenseDTO.expirationDate).remove(oldAsset_licenseDTO);
					}
					if(mapOfAsset_licenseDTOToexpirationDate.get(oldAsset_licenseDTO.expirationDate).isEmpty()) {
						mapOfAsset_licenseDTOToexpirationDate.remove(oldAsset_licenseDTO.expirationDate);
					}
					
					if(mapOfAsset_licenseDTOToterminationDate.containsKey(oldAsset_licenseDTO.terminationDate)) {
						mapOfAsset_licenseDTOToterminationDate.get(oldAsset_licenseDTO.terminationDate).remove(oldAsset_licenseDTO);
					}
					if(mapOfAsset_licenseDTOToterminationDate.get(oldAsset_licenseDTO.terminationDate).isEmpty()) {
						mapOfAsset_licenseDTOToterminationDate.remove(oldAsset_licenseDTO.terminationDate);
					}
					
					if(mapOfAsset_licenseDTOTopurchaseDate.containsKey(oldAsset_licenseDTO.purchaseDate)) {
						mapOfAsset_licenseDTOTopurchaseDate.get(oldAsset_licenseDTO.purchaseDate).remove(oldAsset_licenseDTO);
					}
					if(mapOfAsset_licenseDTOTopurchaseDate.get(oldAsset_licenseDTO.purchaseDate).isEmpty()) {
						mapOfAsset_licenseDTOTopurchaseDate.remove(oldAsset_licenseDTO.purchaseDate);
					}
					
					if(mapOfAsset_licenseDTOToassetSupplierId.containsKey(oldAsset_licenseDTO.assetSupplierId)) {
						mapOfAsset_licenseDTOToassetSupplierId.get(oldAsset_licenseDTO.assetSupplierId).remove(oldAsset_licenseDTO);
					}
					if(mapOfAsset_licenseDTOToassetSupplierId.get(oldAsset_licenseDTO.assetSupplierId).isEmpty()) {
						mapOfAsset_licenseDTOToassetSupplierId.remove(oldAsset_licenseDTO.assetSupplierId);
					}
					
					if(mapOfAsset_licenseDTOToorderNumber.containsKey(oldAsset_licenseDTO.orderNumber)) {
						mapOfAsset_licenseDTOToorderNumber.get(oldAsset_licenseDTO.orderNumber).remove(oldAsset_licenseDTO);
					}
					if(mapOfAsset_licenseDTOToorderNumber.get(oldAsset_licenseDTO.orderNumber).isEmpty()) {
						mapOfAsset_licenseDTOToorderNumber.remove(oldAsset_licenseDTO.orderNumber);
					}
					
					if(mapOfAsset_licenseDTOTopurchaseCost.containsKey(oldAsset_licenseDTO.purchaseCost)) {
						mapOfAsset_licenseDTOTopurchaseCost.get(oldAsset_licenseDTO.purchaseCost).remove(oldAsset_licenseDTO);
					}
					if(mapOfAsset_licenseDTOTopurchaseCost.get(oldAsset_licenseDTO.purchaseCost).isEmpty()) {
						mapOfAsset_licenseDTOTopurchaseCost.remove(oldAsset_licenseDTO.purchaseCost);
					}
					
					if(mapOfAsset_licenseDTOToisDepreciationApplicable.containsKey(oldAsset_licenseDTO.isDepreciationApplicable)) {
						mapOfAsset_licenseDTOToisDepreciationApplicable.get(oldAsset_licenseDTO.isDepreciationApplicable).remove(oldAsset_licenseDTO);
					}
					if(mapOfAsset_licenseDTOToisDepreciationApplicable.get(oldAsset_licenseDTO.isDepreciationApplicable).isEmpty()) {
						mapOfAsset_licenseDTOToisDepreciationApplicable.remove(oldAsset_licenseDTO.isDepreciationApplicable);
					}
					
					if(mapOfAsset_licenseDTOTodescription.containsKey(oldAsset_licenseDTO.description)) {
						mapOfAsset_licenseDTOTodescription.get(oldAsset_licenseDTO.description).remove(oldAsset_licenseDTO);
					}
					if(mapOfAsset_licenseDTOTodescription.get(oldAsset_licenseDTO.description).isEmpty()) {
						mapOfAsset_licenseDTOTodescription.remove(oldAsset_licenseDTO.description);
					}
					
					if(mapOfAsset_licenseDTOToinsertedByUserId.containsKey(oldAsset_licenseDTO.insertedByUserId)) {
						mapOfAsset_licenseDTOToinsertedByUserId.get(oldAsset_licenseDTO.insertedByUserId).remove(oldAsset_licenseDTO);
					}
					if(mapOfAsset_licenseDTOToinsertedByUserId.get(oldAsset_licenseDTO.insertedByUserId).isEmpty()) {
						mapOfAsset_licenseDTOToinsertedByUserId.remove(oldAsset_licenseDTO.insertedByUserId);
					}
					
					if(mapOfAsset_licenseDTOToinsertedByOrganogramId.containsKey(oldAsset_licenseDTO.insertedByOrganogramId)) {
						mapOfAsset_licenseDTOToinsertedByOrganogramId.get(oldAsset_licenseDTO.insertedByOrganogramId).remove(oldAsset_licenseDTO);
					}
					if(mapOfAsset_licenseDTOToinsertedByOrganogramId.get(oldAsset_licenseDTO.insertedByOrganogramId).isEmpty()) {
						mapOfAsset_licenseDTOToinsertedByOrganogramId.remove(oldAsset_licenseDTO.insertedByOrganogramId);
					}
					
					if(mapOfAsset_licenseDTOToinsertionDate.containsKey(oldAsset_licenseDTO.insertionDate)) {
						mapOfAsset_licenseDTOToinsertionDate.get(oldAsset_licenseDTO.insertionDate).remove(oldAsset_licenseDTO);
					}
					if(mapOfAsset_licenseDTOToinsertionDate.get(oldAsset_licenseDTO.insertionDate).isEmpty()) {
						mapOfAsset_licenseDTOToinsertionDate.remove(oldAsset_licenseDTO.insertionDate);
					}
					
					if(mapOfAsset_licenseDTOTolastModifierUser.containsKey(oldAsset_licenseDTO.lastModifierUser)) {
						mapOfAsset_licenseDTOTolastModifierUser.get(oldAsset_licenseDTO.lastModifierUser).remove(oldAsset_licenseDTO);
					}
					if(mapOfAsset_licenseDTOTolastModifierUser.get(oldAsset_licenseDTO.lastModifierUser).isEmpty()) {
						mapOfAsset_licenseDTOTolastModifierUser.remove(oldAsset_licenseDTO.lastModifierUser);
					}
					
					if(mapOfAsset_licenseDTOTosearchColumn.containsKey(oldAsset_licenseDTO.searchColumn)) {
						mapOfAsset_licenseDTOTosearchColumn.get(oldAsset_licenseDTO.searchColumn).remove(oldAsset_licenseDTO);
					}
					if(mapOfAsset_licenseDTOTosearchColumn.get(oldAsset_licenseDTO.searchColumn).isEmpty()) {
						mapOfAsset_licenseDTOTosearchColumn.remove(oldAsset_licenseDTO.searchColumn);
					}
					
					if(mapOfAsset_licenseDTOTostatus.containsKey(oldAsset_licenseDTO.status)) {
						mapOfAsset_licenseDTOTostatus.get(oldAsset_licenseDTO.status).remove(oldAsset_licenseDTO);
					}
					if(mapOfAsset_licenseDTOTostatus.get(oldAsset_licenseDTO.status).isEmpty()) {
						mapOfAsset_licenseDTOTostatus.remove(oldAsset_licenseDTO.status);
					}
					
					if(mapOfAsset_licenseDTOTolastModificationTime.containsKey(oldAsset_licenseDTO.lastModificationTime)) {
						mapOfAsset_licenseDTOTolastModificationTime.get(oldAsset_licenseDTO.lastModificationTime).remove(oldAsset_licenseDTO);
					}
					if(mapOfAsset_licenseDTOTolastModificationTime.get(oldAsset_licenseDTO.lastModificationTime).isEmpty()) {
						mapOfAsset_licenseDTOTolastModificationTime.remove(oldAsset_licenseDTO.lastModificationTime);
					}
					
					
				}
				if(asset_licenseDTO.isDeleted == 0) 
				{
					
					mapOfAsset_licenseDTOToiD.put(asset_licenseDTO.iD, asset_licenseDTO);
				
					if( ! mapOfAsset_licenseDTOTosoftwareName.containsKey(asset_licenseDTO.softwareName)) {
						mapOfAsset_licenseDTOTosoftwareName.put(asset_licenseDTO.softwareName, new HashSet<>());
					}
					mapOfAsset_licenseDTOTosoftwareName.get(asset_licenseDTO.softwareName).add(asset_licenseDTO);
					
					if( ! mapOfAsset_licenseDTOTosoftwareCat.containsKey(asset_licenseDTO.softwareCat)) {
						mapOfAsset_licenseDTOTosoftwareCat.put(asset_licenseDTO.softwareCat, new HashSet<>());
					}
					mapOfAsset_licenseDTOTosoftwareCat.get(asset_licenseDTO.softwareCat).add(asset_licenseDTO);
					
					if( ! mapOfAsset_licenseDTOTonumberOfLicense.containsKey(asset_licenseDTO.numberOfLicense)) {
						mapOfAsset_licenseDTOTonumberOfLicense.put(asset_licenseDTO.numberOfLicense, new HashSet<>());
					}
					mapOfAsset_licenseDTOTonumberOfLicense.get(asset_licenseDTO.numberOfLicense).add(asset_licenseDTO);
					
					if( ! mapOfAsset_licenseDTOToassetManufacturerId.containsKey(asset_licenseDTO.assetManufacturerId)) {
						mapOfAsset_licenseDTOToassetManufacturerId.put(asset_licenseDTO.assetManufacturerId, new HashSet<>());
					}
					mapOfAsset_licenseDTOToassetManufacturerId.get(asset_licenseDTO.assetManufacturerId).add(asset_licenseDTO);
					
					if( ! mapOfAsset_licenseDTOTolicensedToName.containsKey(asset_licenseDTO.licensedToName)) {
						mapOfAsset_licenseDTOTolicensedToName.put(asset_licenseDTO.licensedToName, new HashSet<>());
					}
					mapOfAsset_licenseDTOTolicensedToName.get(asset_licenseDTO.licensedToName).add(asset_licenseDTO);
					
					if( ! mapOfAsset_licenseDTOTolicensedToEmail.containsKey(asset_licenseDTO.licensedToEmail)) {
						mapOfAsset_licenseDTOTolicensedToEmail.put(asset_licenseDTO.licensedToEmail, new HashSet<>());
					}
					mapOfAsset_licenseDTOTolicensedToEmail.get(asset_licenseDTO.licensedToEmail).add(asset_licenseDTO);
					
					if( ! mapOfAsset_licenseDTOToisReassignable.containsKey(asset_licenseDTO.isReassignable)) {
						mapOfAsset_licenseDTOToisReassignable.put(asset_licenseDTO.isReassignable, new HashSet<>());
					}
					mapOfAsset_licenseDTOToisReassignable.get(asset_licenseDTO.isReassignable).add(asset_licenseDTO);
					
					if( ! mapOfAsset_licenseDTOToexpirationDate.containsKey(asset_licenseDTO.expirationDate)) {
						mapOfAsset_licenseDTOToexpirationDate.put(asset_licenseDTO.expirationDate, new HashSet<>());
					}
					mapOfAsset_licenseDTOToexpirationDate.get(asset_licenseDTO.expirationDate).add(asset_licenseDTO);
					
					if( ! mapOfAsset_licenseDTOToterminationDate.containsKey(asset_licenseDTO.terminationDate)) {
						mapOfAsset_licenseDTOToterminationDate.put(asset_licenseDTO.terminationDate, new HashSet<>());
					}
					mapOfAsset_licenseDTOToterminationDate.get(asset_licenseDTO.terminationDate).add(asset_licenseDTO);
					
					if( ! mapOfAsset_licenseDTOTopurchaseDate.containsKey(asset_licenseDTO.purchaseDate)) {
						mapOfAsset_licenseDTOTopurchaseDate.put(asset_licenseDTO.purchaseDate, new HashSet<>());
					}
					mapOfAsset_licenseDTOTopurchaseDate.get(asset_licenseDTO.purchaseDate).add(asset_licenseDTO);
					
					if( ! mapOfAsset_licenseDTOToassetSupplierId.containsKey(asset_licenseDTO.assetSupplierId)) {
						mapOfAsset_licenseDTOToassetSupplierId.put(asset_licenseDTO.assetSupplierId, new HashSet<>());
					}
					mapOfAsset_licenseDTOToassetSupplierId.get(asset_licenseDTO.assetSupplierId).add(asset_licenseDTO);
					
					if( ! mapOfAsset_licenseDTOToorderNumber.containsKey(asset_licenseDTO.orderNumber)) {
						mapOfAsset_licenseDTOToorderNumber.put(asset_licenseDTO.orderNumber, new HashSet<>());
					}
					mapOfAsset_licenseDTOToorderNumber.get(asset_licenseDTO.orderNumber).add(asset_licenseDTO);
					
					if( ! mapOfAsset_licenseDTOTopurchaseCost.containsKey(asset_licenseDTO.purchaseCost)) {
						mapOfAsset_licenseDTOTopurchaseCost.put(asset_licenseDTO.purchaseCost, new HashSet<>());
					}
					mapOfAsset_licenseDTOTopurchaseCost.get(asset_licenseDTO.purchaseCost).add(asset_licenseDTO);
					
					if( ! mapOfAsset_licenseDTOToisDepreciationApplicable.containsKey(asset_licenseDTO.isDepreciationApplicable)) {
						mapOfAsset_licenseDTOToisDepreciationApplicable.put(asset_licenseDTO.isDepreciationApplicable, new HashSet<>());
					}
					mapOfAsset_licenseDTOToisDepreciationApplicable.get(asset_licenseDTO.isDepreciationApplicable).add(asset_licenseDTO);
					
					if( ! mapOfAsset_licenseDTOTodescription.containsKey(asset_licenseDTO.description)) {
						mapOfAsset_licenseDTOTodescription.put(asset_licenseDTO.description, new HashSet<>());
					}
					mapOfAsset_licenseDTOTodescription.get(asset_licenseDTO.description).add(asset_licenseDTO);
					
					if( ! mapOfAsset_licenseDTOToinsertedByUserId.containsKey(asset_licenseDTO.insertedByUserId)) {
						mapOfAsset_licenseDTOToinsertedByUserId.put(asset_licenseDTO.insertedByUserId, new HashSet<>());
					}
					mapOfAsset_licenseDTOToinsertedByUserId.get(asset_licenseDTO.insertedByUserId).add(asset_licenseDTO);
					
					if( ! mapOfAsset_licenseDTOToinsertedByOrganogramId.containsKey(asset_licenseDTO.insertedByOrganogramId)) {
						mapOfAsset_licenseDTOToinsertedByOrganogramId.put(asset_licenseDTO.insertedByOrganogramId, new HashSet<>());
					}
					mapOfAsset_licenseDTOToinsertedByOrganogramId.get(asset_licenseDTO.insertedByOrganogramId).add(asset_licenseDTO);
					
					if( ! mapOfAsset_licenseDTOToinsertionDate.containsKey(asset_licenseDTO.insertionDate)) {
						mapOfAsset_licenseDTOToinsertionDate.put(asset_licenseDTO.insertionDate, new HashSet<>());
					}
					mapOfAsset_licenseDTOToinsertionDate.get(asset_licenseDTO.insertionDate).add(asset_licenseDTO);
					
					if( ! mapOfAsset_licenseDTOTolastModifierUser.containsKey(asset_licenseDTO.lastModifierUser)) {
						mapOfAsset_licenseDTOTolastModifierUser.put(asset_licenseDTO.lastModifierUser, new HashSet<>());
					}
					mapOfAsset_licenseDTOTolastModifierUser.get(asset_licenseDTO.lastModifierUser).add(asset_licenseDTO);
					
					if( ! mapOfAsset_licenseDTOTosearchColumn.containsKey(asset_licenseDTO.searchColumn)) {
						mapOfAsset_licenseDTOTosearchColumn.put(asset_licenseDTO.searchColumn, new HashSet<>());
					}
					mapOfAsset_licenseDTOTosearchColumn.get(asset_licenseDTO.searchColumn).add(asset_licenseDTO);
					
					if( ! mapOfAsset_licenseDTOTostatus.containsKey(asset_licenseDTO.status)) {
						mapOfAsset_licenseDTOTostatus.put(asset_licenseDTO.status, new HashSet<>());
					}
					mapOfAsset_licenseDTOTostatus.get(asset_licenseDTO.status).add(asset_licenseDTO);
					
					if( ! mapOfAsset_licenseDTOTolastModificationTime.containsKey(asset_licenseDTO.lastModificationTime)) {
						mapOfAsset_licenseDTOTolastModificationTime.put(asset_licenseDTO.lastModificationTime, new HashSet<>());
					}
					mapOfAsset_licenseDTOTolastModificationTime.get(asset_licenseDTO.lastModificationTime).add(asset_licenseDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Asset_licenseDTO> getAsset_licenseList() {
		List <Asset_licenseDTO> asset_licenses = new ArrayList<Asset_licenseDTO>(this.mapOfAsset_licenseDTOToiD.values());
		return asset_licenses;
	}
	
	
	public Asset_licenseDTO getAsset_licenseDTOByID( long ID){
		return mapOfAsset_licenseDTOToiD.get(ID);
	}
	
	
	public List<Asset_licenseDTO> getAsset_licenseDTOBysoftware_name(String software_name) {
		return new ArrayList<>( mapOfAsset_licenseDTOTosoftwareName.getOrDefault(software_name,new HashSet<>()));
	}
	
	
	public List<Asset_licenseDTO> getAsset_licenseDTOBysoftware_cat(int software_cat) {
		return new ArrayList<>( mapOfAsset_licenseDTOTosoftwareCat.getOrDefault(software_cat,new HashSet<>()));
	}
	
	
	public List<Asset_licenseDTO> getAsset_licenseDTOBynumber_of_license(int number_of_license) {
		return new ArrayList<>( mapOfAsset_licenseDTOTonumberOfLicense.getOrDefault(number_of_license,new HashSet<>()));
	}
	
	
	public List<Asset_licenseDTO> getAsset_licenseDTOByasset_manufacturer_id(long asset_manufacturer_id) {
		return new ArrayList<>( mapOfAsset_licenseDTOToassetManufacturerId.getOrDefault(asset_manufacturer_id,new HashSet<>()));
	}
	
	
	public List<Asset_licenseDTO> getAsset_licenseDTOBylicensed_to_name(String licensed_to_name) {
		return new ArrayList<>( mapOfAsset_licenseDTOTolicensedToName.getOrDefault(licensed_to_name,new HashSet<>()));
	}
	
	
	public List<Asset_licenseDTO> getAsset_licenseDTOBylicensed_to_email(String licensed_to_email) {
		return new ArrayList<>( mapOfAsset_licenseDTOTolicensedToEmail.getOrDefault(licensed_to_email,new HashSet<>()));
	}
	
	
	public List<Asset_licenseDTO> getAsset_licenseDTOByis_reassignable(boolean is_reassignable) {
		return new ArrayList<>( mapOfAsset_licenseDTOToisReassignable.getOrDefault(is_reassignable,new HashSet<>()));
	}
	
	
	public List<Asset_licenseDTO> getAsset_licenseDTOByexpiration_date(long expiration_date) {
		return new ArrayList<>( mapOfAsset_licenseDTOToexpirationDate.getOrDefault(expiration_date,new HashSet<>()));
	}
	
	
	public List<Asset_licenseDTO> getAsset_licenseDTOBytermination_date(long termination_date) {
		return new ArrayList<>( mapOfAsset_licenseDTOToterminationDate.getOrDefault(termination_date,new HashSet<>()));
	}
	
	
	public List<Asset_licenseDTO> getAsset_licenseDTOBypurchase_date(long purchase_date) {
		return new ArrayList<>( mapOfAsset_licenseDTOTopurchaseDate.getOrDefault(purchase_date,new HashSet<>()));
	}
	
	
	public List<Asset_licenseDTO> getAsset_licenseDTOByasset_supplier_id(long asset_supplier_id) {
		return new ArrayList<>( mapOfAsset_licenseDTOToassetSupplierId.getOrDefault(asset_supplier_id,new HashSet<>()));
	}
	
	
	public List<Asset_licenseDTO> getAsset_licenseDTOByorder_number(String order_number) {
		return new ArrayList<>( mapOfAsset_licenseDTOToorderNumber.getOrDefault(order_number,new HashSet<>()));
	}
	
	
	public List<Asset_licenseDTO> getAsset_licenseDTOBypurchase_cost(int purchase_cost) {
		return new ArrayList<>( mapOfAsset_licenseDTOTopurchaseCost.getOrDefault(purchase_cost,new HashSet<>()));
	}
	
	
	public List<Asset_licenseDTO> getAsset_licenseDTOByis_depreciation_applicable(boolean is_depreciation_applicable) {
		return new ArrayList<>( mapOfAsset_licenseDTOToisDepreciationApplicable.getOrDefault(is_depreciation_applicable,new HashSet<>()));
	}
	
	
	public List<Asset_licenseDTO> getAsset_licenseDTOBydescription(String description) {
		return new ArrayList<>( mapOfAsset_licenseDTOTodescription.getOrDefault(description,new HashSet<>()));
	}
	
	
	public List<Asset_licenseDTO> getAsset_licenseDTOByinserted_by_user_id(long inserted_by_user_id) {
		return new ArrayList<>( mapOfAsset_licenseDTOToinsertedByUserId.getOrDefault(inserted_by_user_id,new HashSet<>()));
	}
	
	
	public List<Asset_licenseDTO> getAsset_licenseDTOByinserted_by_organogram_id(long inserted_by_organogram_id) {
		return new ArrayList<>( mapOfAsset_licenseDTOToinsertedByOrganogramId.getOrDefault(inserted_by_organogram_id,new HashSet<>()));
	}
	
	
	public List<Asset_licenseDTO> getAsset_licenseDTOByinsertion_date(long insertion_date) {
		return new ArrayList<>( mapOfAsset_licenseDTOToinsertionDate.getOrDefault(insertion_date,new HashSet<>()));
	}
	
	
	public List<Asset_licenseDTO> getAsset_licenseDTOBylast_modifier_user(String last_modifier_user) {
		return new ArrayList<>( mapOfAsset_licenseDTOTolastModifierUser.getOrDefault(last_modifier_user,new HashSet<>()));
	}
	
	
	public List<Asset_licenseDTO> getAsset_licenseDTOBysearch_column(String search_column) {
		return new ArrayList<>( mapOfAsset_licenseDTOTosearchColumn.getOrDefault(search_column,new HashSet<>()));
	}
	
	
	public List<Asset_licenseDTO> getAsset_licenseDTOBystatus(int status) {
		return new ArrayList<>( mapOfAsset_licenseDTOTostatus.getOrDefault(status,new HashSet<>()));
	}
	
	
	public List<Asset_licenseDTO> getAsset_licenseDTOBylastModificationTime(long lastModificationTime) {
		return new ArrayList<>( mapOfAsset_licenseDTOTolastModificationTime.getOrDefault(lastModificationTime,new HashSet<>()));
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "asset_license";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


