package asset_license;
import java.util.*; 
import util.*; 


public class Asset_licenseDTO extends CommonDTO
{

    public String softwareName = "";
	public int softwareCat = -1;
	public int numberOfLicense = -1;
	public long assetManufacturerId = -1;
    public String licensedToName = "";
    public String licensedToEmail = "";
	public boolean isReassignable = false;
	public long expirationDate = System.currentTimeMillis();
	public long terminationDate = System.currentTimeMillis();
	public long purchaseDate = System.currentTimeMillis();
	public long assetSupplierId = -1;
    public String orderNumber = "";
	public int purchaseCost = -1;
	public boolean isDepreciationApplicable = false;
    public String description = "";
	public long insertedByUserId = -1;
	public long insertedByOrganogramId = -1;
	public long insertionDate = -1;
    public String lastModifierUser = "";
	public int status = -1;
	public long softwareSt = -1;
	
	
    @Override
	public String toString() {
            return "$Asset_licenseDTO[" +
            " iD = " + iD +
            " softwareName = " + softwareName +
            " softwareCat = " + softwareCat +
            " numberOfLicense = " + numberOfLicense +
            " assetManufacturerId = " + assetManufacturerId +
            " licensedToName = " + licensedToName +
            " licensedToEmail = " + licensedToEmail +
            " isReassignable = " + isReassignable +
            " expirationDate = " + expirationDate +
            " terminationDate = " + terminationDate +
            " purchaseDate = " + purchaseDate +
            " assetSupplierId = " + assetSupplierId +
            " orderNumber = " + orderNumber +
            " purchaseCost = " + purchaseCost +
            " isDepreciationApplicable = " + isDepreciationApplicable +
            " description = " + description +
            " insertedByUserId = " + insertedByUserId +
            " insertedByOrganogramId = " + insertedByOrganogramId +
            " insertionDate = " + insertionDate +
            " lastModifierUser = " + lastModifierUser +
            " searchColumn = " + searchColumn +
            " status = " + status +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}