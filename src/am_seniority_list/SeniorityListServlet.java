package am_seniority_list;

import am_house_allocation_request.Am_house_allocation_requestDAO;
import am_house_allocation_request.Am_house_allocation_requestDTO;
import common.ApiResponse;
import employee_pay_scale.Employee_pay_scaleRepository;
import employee_records.Employee_recordsRepository;
import grade_wise_pay_scale.Grade_wise_pay_scaleRepository;
import org.apache.log4j.Logger;
import permission.MenuConstants;
import role.PermissionRepository;
import user.UserDTO;
import util.HttpRequestUtils;
import util.UtilCharacter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@WebServlet("/SeniorityListServlet")
@MultipartConfig
public class SeniorityListServlet extends HttpServlet
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(SeniorityListServlet.class);


    public SeniorityListServlet()
	{
        super();
    }

    void getList(HttpServletRequest request, HttpServletResponse response) throws Exception {

		List<Am_house_allocation_requestDTO> dtos = Am_house_allocation_requestDAO.getInstance()
				.getPendingRequest();
		request.setAttribute("dtos",dtos);

		RequestDispatcher rd;
		rd = request.getRequestDispatcher( "am_seniority_list/seniority_list.jsp");
		rd.forward(request, response);

	}


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		logger.debug("get list requested = " + request);
		UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;
		try
		{
			String actionType = request.getParameter("actionType");
			if(actionType.equals("getList"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.AM_HOUSE_ALLOCATION_SENIORITY_SELECTION))
				{
					getList(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}

			else if("verifyList".equalsIgnoreCase(actionType)){
				try {
					String selectedIds = request.getParameter("selectedIds");
					String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
					Set<Long> ids = Arrays.stream(selectedIds.split(","))
							.map(String::trim)
							.map(Long::parseLong)
							.collect(Collectors.toSet());
					if(ids.size() < 2){
						throw new Exception(UtilCharacter.getDataByLanguage(Language, "নূন্যতম ২ টি নির্বাচন করুন ",
								"Please Select Minimum 2 Items" ));
					}

					long classSize = Am_house_allocation_requestDAO.getInstance().getDTOs(new ArrayList<>(ids))
							.stream()
							.map(i -> i.amHouseClassCat)
							.collect(Collectors.toSet())
							.size();
					if(classSize !=1){
						throw new Exception(UtilCharacter.getDataByLanguage(Language, "শুধুমাত্র ১টি বাসার শ্রেণী হতে হবে",
								"Have to be Only 1 House Class" ));
					}

					ApiResponse.sendSuccessResponse(response, "");
				} catch (Exception ex) {
					logger.error(ex);
					ApiResponse.sendErrorResponse(response, ex.getMessage());
				}

			}

			else if("goToSeniorityPage".equalsIgnoreCase(actionType)){
				String selectedIds = request.getParameter("selectedIds");
				//Grade_wise_pay_scaleRepository.getInstance().getText("English", employeeOfficeDTO.gradeTypeLevel);
				Set<Long> ids = Arrays.stream(selectedIds.split(","))
						.map(String::trim)
						.map(Long::parseLong)
						.collect(Collectors.toSet());

				List<Am_house_allocation_requestDTO> dtos =
						Am_house_allocation_requestDAO.getInstance().getDTOs(new ArrayList<>(ids));
				Employee_recordsRepository employee_recordsRepository = Employee_recordsRepository.getInstance();

				dtos.forEach(i -> i.govtJobDays = employee_recordsRepository.
						getGovtOfficeDaysByEmployeeRecordId(i.requesterEmpId));

				dtos.forEach(i -> i.employeePayScaleTypeEn = Grade_wise_pay_scaleRepository.getInstance().getText("English", i.employeePayScaleId));
				dtos.forEach(i -> i.salary = Grade_wise_pay_scaleRepository.getInstance().getPayScale(i.employeePayScaleId));

				Comparator<Am_house_allocation_requestDTO> comparator = (h1, h2) -> {
					int currentPayScale = Long.compare(h1.salary, h2.salary);
					if(currentPayScale != 0){
						return currentPayScale;
					}
					else{
						int govtJobDaysComparator = Long.compare(h1.govtJobDays, h2.govtJobDays);
						if(govtJobDaysComparator != 0){
							return govtJobDaysComparator;
						} else {
							int dobComparator = Long.compare(h1.dateOfBirth, h2.dateOfBirth);
							if(dobComparator != 0){
								return dobComparator;
							}else {
								return h1.requesterNameEn.compareToIgnoreCase(h2.requesterNameEn);
							}
						}
					}

				};


				dtos.sort(comparator.reversed());
				request.setAttribute("dtos",dtos);

				RequestDispatcher rd;
				rd = request.getRequestDispatcher( "am_seniority_list/seniority_list_data.jsp");
				rd.forward(request, response);

			}
			else
			{
				request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}




    @Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{

	}

}

