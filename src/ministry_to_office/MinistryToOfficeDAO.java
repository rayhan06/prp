package ministry_to_office;

import test_lib.RQueryBuilder;

public class MinistryToOfficeDAO {

    public String getMinistry() {
        String sql = "select id, name_eng, name_bng from office_ministries where isDeleted = 0;";
        RQueryBuilder<MinistryToOfficeModel.MinistryModel> rQueryBuilder = new RQueryBuilder<>();
        String data = rQueryBuilder.setSql(sql).of(MinistryToOfficeModel.MinistryModel.class).buildJson();

        return data;
    }

    public String getLayer(String ministry) {
        String sql = String.format("select id, layer_name_eng, layer_name_bng from office_layers where office_ministry_id = %s and isDeleted = 0;", ministry);

        RQueryBuilder<MinistryToOfficeModel.LayerModel> rQueryBuilder = new RQueryBuilder<>();
        String data = rQueryBuilder.setSql(sql).of(MinistryToOfficeModel.LayerModel.class).buildJson();

        return data;
    }

    public String getOrigin(String layer) {
        String sql = String.format("select id, office_name_eng, office_name_bng from office_origins where office_layer_id = %s and isDeleted = 0;", layer);

        RQueryBuilder<MinistryToOfficeModel.OfficeOriginModel> rQueryBuilder = new RQueryBuilder<>();
        String data = rQueryBuilder.setSql(sql).of(MinistryToOfficeModel.OfficeOriginModel.class).buildJson();

        return data;
    }

    public String getOffice(String origin) {
        String sql = String.format("select id, office_name_eng, office_name_bng from offices where office_origin_id = %s and isDeleted = 0;", origin);

        RQueryBuilder<MinistryToOfficeModel.OfficeModel> rQueryBuilder = new RQueryBuilder<>();
        String data = rQueryBuilder.setSql(sql).of(MinistryToOfficeModel.OfficeModel.class).buildJson();

        return data;
    }

    public String getUnit(String office) {
        String sql = String.format("select id, unit_name_bng, unit_name_eng from office_units where office_id = %s and isDeleted = 0;", office);

        RQueryBuilder<MinistryToOfficeModel.OfficeUnitModel> rQueryBuilder = new RQueryBuilder<>();
        String data = rQueryBuilder.setSql(sql).of(MinistryToOfficeModel.OfficeUnitModel.class).buildJson();

        return data;
    }

    public String getOrganogram(String unit) {
        String sql = String.format("select id, designation_eng, designation_bng from office_unit_organograms where office_unit_id = %s and isDeleted = 0;", unit);

        RQueryBuilder<MinistryToOfficeModel.OrganogramModel> rQueryBuilder = new RQueryBuilder<>();
        String data = rQueryBuilder.setSql(sql).of(MinistryToOfficeModel.OrganogramModel.class).buildJson();

        return data;
    }

    public String getEmployee(String organogramId) {
        String sql = String.format("select \n" +
                "employee_records.id,\n" +
                "employee_records.name_eng,\n" +
                "employee_records.name_bng\n" +
                "from employee_records, employee_offices \n" +
                "where employee_offices.office_unit_organogram_id = %s \n" +
                "and employee_records.id = employee_offices.employee_record_id\n" +
                "and employee_records.isDeleted = 0\n" +
                "and employee_offices.isDeleted = 0\n" +
                "limit 1;", organogramId);

        RQueryBuilder<MinistryToOfficeModel.EmployeeModel> rQueryBuilder = new RQueryBuilder<>();
        String data = rQueryBuilder.setSql(sql).of(MinistryToOfficeModel.EmployeeModel.class).buildJson();

        return data;
    }
}
