package ministry_to_office;

public class MinistryToOfficeModel {

    public static class MinistryModel {
        private int id;
        private String name_eng;
        private String name_bng;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName_eng() {
            return name_eng;
        }

        public void setName_eng(String name_eng) {
            this.name_eng = name_eng;
        }

        public String getName_bng() {
            return name_bng;
        }

        public void setName_bng(String name_bng) {
            this.name_bng = name_bng;
        }
    }

    public static class LayerModel {
        private int id;
        private String layer_name_eng;
        private String layer_name_bng;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getLayer_name_eng() {
            return layer_name_eng;
        }

        public void setLayer_name_eng(String layer_name_eng) {
            this.layer_name_eng = layer_name_eng;
        }

        public String getLayer_name_bng() {
            return layer_name_bng;
        }

        public void setLayer_name_bng(String layer_name_bng) {
            this.layer_name_bng = layer_name_bng;
        }
    }

    public static class OfficeOriginModel {
        private int id;
        private String office_name_eng;
        private String office_name_bng;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getOffice_name_eng() {
            return office_name_eng;
        }

        public void setOffice_name_eng(String office_name_eng) {
            this.office_name_eng = office_name_eng;
        }

        public String getOffice_name_bng() {
            return office_name_bng;
        }

        public void setOffice_name_bng(String office_name_bng) {
            this.office_name_bng = office_name_bng;
        }
    }

    public static class OfficeModel {
        private int id;
        private String office_name_eng;
        private String office_name_bng;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getOffice_name_eng() {
            return office_name_eng;
        }

        public void setOffice_name_eng(String office_name_eng) {
            this.office_name_eng = office_name_eng;
        }

        public String getOffice_name_bng() {
            return office_name_bng;
        }

        public void setOffice_name_bng(String office_name_bng) {
            this.office_name_bng = office_name_bng;
        }
    }

    public static class OfficeUnitModel {
        private int id;
        private String unit_name_eng;
        private String unit_name_bng;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getUnit_name_eng() {
            return unit_name_eng;
        }

        public void setUnit_name_eng(String unit_name_eng) {
            this.unit_name_eng = unit_name_eng;
        }

        public String getUnit_name_bng() {
            return unit_name_bng;
        }

        public void setUnit_name_bng(String unit_name_bng) {
            this.unit_name_bng = unit_name_bng;
        }
    }

    public static class OrganogramModel {
        private int id;
        private String designation_eng;
        private String designation_bng;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getDesignation_eng() {
            return designation_eng;
        }

        public void setDesignation_eng(String designation_eng) {
            this.designation_eng = designation_eng;
        }

        public String getDesignation_bng() {
            return designation_bng;
        }

        public void setDesignation_bng(String designation_bng) {
            this.designation_bng = designation_bng;
        }
    }

    public static class EmployeeModel {
        private int id;
        private String name_eng;
        private String name_bng;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName_eng() {
            return name_eng;
        }

        public void setName_eng(String name_eng) {
            this.name_eng = name_eng;
        }

        public String getName_bng() {
            return name_bng;
        }

        public void setName_bng(String name_bng) {
            this.name_bng = name_bng;
        }
    }
}
