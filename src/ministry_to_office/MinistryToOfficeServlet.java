/*
package ministry_to_office;

import login.LoginDTO;
import office_layers.Office_layersServlet;
import org.apache.log4j.Logger;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/MinistryToOfficeServlet")
@MultipartConfig
public class MinistryToOfficeServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Office_layersServlet.class);

    public MinistryToOfficeServlet() {
        super();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getInstance().getUserDtoByUserId(loginDTO.userID);
        try {
            String actionType = request.getParameter("actionType");
            switch (actionType) {
                case "getMinistry":
                    response.setContentType("application/json");
                    PrintWriter out0 = response.getWriter();
                    out0.print(new MinistryToOfficeDAO().getMinistry());
                    out0.flush();
                    break;
                case "getLayer":
                    String ministry_id = request.getParameter("ministry_id");
                    response.setContentType("application/json");
                    PrintWriter out1 = response.getWriter();
                    out1.print(new MinistryToOfficeDAO().getLayer(ministry_id));
                    out1.flush();
                    break;
                case "getOrigin":
                    String layer_id = request.getParameter("layer_id");
                    response.setContentType("application/json");
                    PrintWriter out2 = response.getWriter();
                    out2.print(new MinistryToOfficeDAO().getOrigin(layer_id));
                    out2.flush();
                    break;
                case "getOffice":
                    String origin_id = request.getParameter("origin_id");
                    response.setContentType("application/json");
                    PrintWriter out3 = response.getWriter();
                    out3.print(new MinistryToOfficeDAO().getOffice(origin_id));
                    out3.flush();
                    break;
                case "getUnit":
                    String office_id = request.getParameter("office_id");
                    response.setContentType("application/json");
                    PrintWriter out4 = response.getWriter();
                    out4.print(new MinistryToOfficeDAO().getUnit(office_id));
                    out4.flush();
                    break;
                case "getOrganogram":
                    String unit_id = request.getParameter("unit_id");
                    response.setContentType("application/json");
                    PrintWriter out5 = response.getWriter();
                    out5.print(new MinistryToOfficeDAO().getOrganogram(unit_id));
                    out5.flush();
                    break;
                case "getOEmployee":
                    String organogram_id = request.getParameter("organogram_id");
                    response.setContentType("application/json");
                    PrintWriter out6 = response.getWriter();
                    out6.print(new MinistryToOfficeDAO().getEmployee(organogram_id));
                    out6.flush();
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
*/
