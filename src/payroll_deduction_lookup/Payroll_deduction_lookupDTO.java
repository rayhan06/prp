package payroll_deduction_lookup;

import util.CommonDTO;

public class Payroll_deduction_lookupDTO extends CommonDTO {
    public long organogramId = -1;
    public String organogramKey = "";
    public int employmentCat = -1;
    public long taxDeductionConfigId = -1;
    public long amount = 0;
    public long insertedBy = -1;
    public long insertionTime = -1;
    public long modifiedBy = -1;

    public Payroll_deduction_lookupDTO() {
    }

    public Payroll_deduction_lookupDTO(long taxDeductionConfigId) {
        this.taxDeductionConfigId = taxDeductionConfigId;
    }

    @Override
    public String toString() {
        return "$Payroll_deduction_lookupDTO[" +
                " iD = " + iD +
                " organogramId = " + organogramId +
                " organogramKey = " + organogramKey +
                " employmentCat = " + employmentCat +
                " taxDeductionConfigId = " + taxDeductionConfigId +
                " amount = " + amount +
                " insertedBy = " + insertedBy +
                " insertionTime = " + insertionTime +
                " isDeleted = " + isDeleted +
                " modifiedBy = " + modifiedBy +
                " lastModificationTime = " + lastModificationTime +
                "]";
    }
}