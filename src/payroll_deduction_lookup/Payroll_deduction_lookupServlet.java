package payroll_deduction_lookup;

import com.google.gson.Gson;
import common.BaseServlet;
import login.LoginDTO;
import office_unit_organograms.SameDesignationGroup;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import permission.MenuConstants;
import sessionmanager.SessionConstants;
import tax_deduction_configuration.Tax_deduction_configurationRepository;
import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


@WebServlet("/Payroll_deduction_lookupServlet")
@MultipartConfig
@SuppressWarnings({"Duplicates"})
public class Payroll_deduction_lookupServlet extends BaseServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }

        try {
            String actionType = request.getParameter("actionType");
            if ("ajax_getDeductionLookupModels".equals(actionType)) {
                long organogramId = Long.parseLong(Jsoup.clean(
                        request.getParameter("organogramId"), Whitelist.simpleText()
                ));
                String organogramKey = new SameDesignationGroup(organogramId).organogramKey;

                int employmentCat = Integer.parseInt(Jsoup.clean(
                        request.getParameter("employmentCat"), Whitelist.simpleText())
                );

                List<PayrollDeductionLookupModel> res = getLookupModels(employmentCat, organogramKey, userDTO);
                response.setContentType("application/json");
                response.setCharacterEncoding("UTF-8");
                response.getWriter().println(new Gson().toJson(res));
                return;
            }
            super.doGet(request, response);
            return;
        } catch (Exception ex) {
            logger.error(ex);
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }

    public List<PayrollDeductionLookupModel> getLookupModels(int employmentCat, String organogramKey, UserDTO userDTO) {
        Set<Long> latestConfigIds =
                Tax_deduction_configurationRepository.getInstance()
                                                     .getActiveDTOs(employmentCat)
                                                     .stream()
                                                     .map(configDto -> configDto.iD)
                                                     .collect(Collectors.toSet());

        String language = userDTO.languageID == SessionConstants.ENGLISH ? "English" : "Bangla";
        return Payroll_deduction_lookupDAO.getInstance()
                                          .getLatestLookupDTOs(latestConfigIds, employmentCat, organogramKey, userDTO.ID, System.currentTimeMillis())
                                          .stream()
                                          .map(dto -> new PayrollDeductionLookupModel(dto, language))
                                          .collect(Collectors.toList());
    }

    public void addLookupDTO(UserInput userInput, boolean addFlag) throws Exception {
        Payroll_deduction_lookupDTO deductionLookupDTO;
        if (addFlag) {
            deductionLookupDTO = new Payroll_deduction_lookupDTO();
            deductionLookupDTO.insertedBy = userInput.modifierId;
            deductionLookupDTO.insertionTime = userInput.lastModificationTime;
        } else {
            deductionLookupDTO = Payroll_deduction_lookupDAO.getInstance().getDTOFromID(userInput.lookupDTOId);
        }
        deductionLookupDTO.modifiedBy = userInput.modifierId;
        deductionLookupDTO.lastModificationTime = userInput.lastModificationTime;

        deductionLookupDTO.organogramId = userInput.organogramId;
        deductionLookupDTO.organogramKey = new SameDesignationGroup(deductionLookupDTO.organogramId).organogramKey;

        deductionLookupDTO.employmentCat = userInput.employmentCat;
        deductionLookupDTO.taxDeductionConfigId = userInput.configId;
        deductionLookupDTO.amount = userInput.amount;

        if (addFlag) {
            Payroll_deduction_lookupDAO.getInstance().add(deductionLookupDTO);
        } else {
            Payroll_deduction_lookupDAO.getInstance().update(deductionLookupDTO);
        }
    }

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        if (!addFlag)
            throw new UnsupportedOperationException("Payroll_deduction_lookupServlet does not support Edit on user side");

        UserInput userInput = new UserInput();
        userInput.modifierId = userDTO.ID;
        userInput.lastModificationTime = System.currentTimeMillis();
        userInput.organogramId = Long.parseLong(Jsoup.clean(
                request.getParameter("organogramId"), Whitelist.simpleText()
        ));
        userInput.employmentCat = Integer.parseInt(Jsoup.clean(
                request.getParameter("employmentCat"), Whitelist.simpleText())
        );
        String[] lookupDTOIds = request.getParameterValues("lookupDTOId");
        String[] configIds = request.getParameterValues("configId");
        String[] amounts = request.getParameterValues("amount");
        for (int i = 0; i < configIds.length; ++i) {
            userInput.configId = Long.parseLong(configIds[i]);
            if (userInput.configId < 0) continue;
            userInput.lookupDTOId = Long.parseLong(lookupDTOIds[i]);
            userInput.amount = Long.parseLong(amounts[i]);
            userInput.lookupDTOId = Long.parseLong(lookupDTOIds[i]);
            addFlag = (userInput.lookupDTOId < 0);
            addLookupDTO(userInput, addFlag);
        }
        return null;
    }

    @Override
    public String getTableName() {
        return Payroll_deduction_lookupDAO.getInstance().getTableName();
    }

    @Override
    public String getServletName() {
        return "Payroll_deduction_lookupServlet";
    }

    @Override
    public Payroll_deduction_lookupDAO getCommonDAOService() {
        return Payroll_deduction_lookupDAO.getInstance();
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[]{MenuConstants.PAYROLL_DEDUCTION_LOOKUP_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[]{MenuConstants.PAYROLL_DEDUCTION_LOOKUP_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[]{MenuConstants.PAYROLL_DEDUCTION_LOOKUP_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Payroll_deduction_lookupServlet.class;
    }

    public static class UserInput {
        public long lookupDTOId;
        public long organogramId;
        public  long lastModificationTime;
        public long modifierId;
        public long configId;
        public long amount;
        public int employmentCat;
    }
}

