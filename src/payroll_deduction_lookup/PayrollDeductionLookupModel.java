package payroll_deduction_lookup;

import tax_deduction_configuration.Tax_deduction_configurationRepository;

public class PayrollDeductionLookupModel {
    public long lookupDTOId;
    public long payrollDeductionConfigId;
    public long amount;
    public String allowanceName;

    public PayrollDeductionLookupModel(Payroll_deduction_lookupDTO dto, String language) {
        lookupDTOId = dto.iD;
        payrollDeductionConfigId = dto.taxDeductionConfigId;
        amount = Math.max(0, dto.amount);
        allowanceName = Tax_deduction_configurationRepository.getInstance().getDeductionText(
                payrollDeductionConfigId, language
        );
    }
}
