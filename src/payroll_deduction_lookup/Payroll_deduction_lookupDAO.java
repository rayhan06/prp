package payroll_deduction_lookup;

import common.CommonDAOService;
import org.apache.log4j.Logger;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;

@SuppressWarnings({"Duplicates"})
public class Payroll_deduction_lookupDAO implements CommonDAOService<Payroll_deduction_lookupDTO> {
    private static final Logger logger = Logger.getLogger(Payroll_deduction_lookupDAO.class);

    private static final String addQuery =
            "INSERT INTO {tableName} (organogram_id,organogram_key,employment_cat,tax_deduction_config_id,amount,"
                    .concat("modified_by,lastModificationTime,inserted_by,insertion_time,isDeleted,ID) VALUES(?,?,?,?,?,?,?,?,?,?,?)");

    private static final String updateQuery =
            "UPDATE {tableName} SET organogram_id=?,organogram_key=?,employment_cat=?,tax_deduction_config_id=?,amount=?,"
                    .concat("modified_by=?,lastModificationTime=? WHERE ID=?");

    private static final String getByEmploymentOrganogram =
            "SELECT * FROM payroll_deduction_lookup WHERE employment_cat=? AND organogram_key=? AND isDeleted=0";

    private final Map<String, String> searchMap = new HashMap<>();

    private Payroll_deduction_lookupDAO() {
        searchMap.put("organogram_key", " and (organogram_key = ?)");
    }

    private static class LazyLoader {
        static final Payroll_deduction_lookupDAO INSTANCE = new Payroll_deduction_lookupDAO();
    }

    public static Payroll_deduction_lookupDAO getInstance() {
        return LazyLoader.INSTANCE;
    }

    @Override
    public void set(PreparedStatement ps, Payroll_deduction_lookupDTO dto, boolean isInsert) throws SQLException {
        int index = 0;
        ps.setLong(++index, dto.organogramId);
        ps.setString(++index, dto.organogramKey);
        ps.setInt(++index, dto.employmentCat);
        ps.setLong(++index, dto.taxDeductionConfigId);
        ps.setLong(++index, dto.amount);
        ps.setLong(++index, dto.modifiedBy);
        ps.setLong(++index, dto.lastModificationTime);
        if (isInsert) {
            ps.setLong(++index, dto.insertedBy);
            ps.setLong(++index, dto.insertionTime);
            ps.setInt(++index, 0 /* isDeleted */);
        }
        ps.setLong(++index, dto.iD);
    }

    @Override
    public Payroll_deduction_lookupDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Payroll_deduction_lookupDTO dto = new Payroll_deduction_lookupDTO();
            dto.iD = rs.getLong("ID");
            dto.organogramId = rs.getLong("organogram_id");
            dto.organogramKey = rs.getString("organogram_key");
            dto.employmentCat = rs.getInt("employment_cat");
            dto.taxDeductionConfigId = rs.getLong("tax_deduction_config_id");
            dto.amount = rs.getLong("amount");
            dto.insertedBy = rs.getLong("inserted_by");
            dto.insertionTime = rs.getLong("insertion_time");
            dto.isDeleted = rs.getInt("isDeleted");
            dto.modifiedBy = rs.getLong("modified_by");
            dto.lastModificationTime = rs.getLong("lastModificationTime");
            return dto;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    @Override
    public String getTableName() {
        return "payroll_deduction_lookup";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    @Override
    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Payroll_deduction_lookupDTO) commonDTO, addQuery, true);
    }

    @Override
    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Payroll_deduction_lookupDTO) commonDTO, updateQuery, false);
    }

    public List<Payroll_deduction_lookupDTO> getLatestLookupDTOs(Set<Long> latestConfigIds, int employmentCat, String organogramKey,
                                                                 long requestBy, long requestTime) {
        List<Payroll_deduction_lookupDTO> currentDTOs = getDTOs(
                getByEmploymentOrganogram,
                Arrays.asList(employmentCat, organogramKey)
        );
        if (currentDTOs == null) currentDTOs = new ArrayList<>();

        Map<Boolean, List<Payroll_deduction_lookupDTO>> presentInCurrent =
                currentDTOs.stream()
                           .collect(Collectors.partitioningBy(dto -> latestConfigIds.contains(dto.taxDeductionConfigId)));

        deleteMissingConfigIds(requestBy, requestTime, presentInCurrent.get(false));

        List<Payroll_deduction_lookupDTO> latestLookupDTOs = presentInCurrent.getOrDefault(true, new ArrayList<>());
        latestLookupDTOs.addAll(getNewAdditionDTOs(latestConfigIds, currentDTOs));

        return latestLookupDTOs;
    }

    private List<Payroll_deduction_lookupDTO> getNewAdditionDTOs(Set<Long> latestConfigIds,
                                                                 List<Payroll_deduction_lookupDTO> currentDTOs) {
        Set<Long> currentConfigIds = currentDTOs.stream()
                                                .map(configDTO -> configDTO.taxDeductionConfigId)
                                                .collect(Collectors.toSet());
        return latestConfigIds.stream()
                              .filter(configId -> !currentConfigIds.contains(configId))
                              .map(Payroll_deduction_lookupDTO::new)
                              .collect(Collectors.toList());
    }

    private void deleteMissingConfigIds(long requestBy, long requestTime, List<Payroll_deduction_lookupDTO> missingFromCurrent) {
        if (missingFromCurrent == null || missingFromCurrent.isEmpty()) return;
        delete(
                requestBy,
                missingFromCurrent.stream()
                                  .map(dto -> dto.iD)
                                  .collect(Collectors.toList()),
                requestTime
        );
    }
}