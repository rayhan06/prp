package payroll_deduction_lookup;

import org.apache.log4j.Logger;
import repository.Repository;
import repository.RepositoryManager;
import util.LockManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public class Payroll_deduction_lookupRepository implements Repository {
    private static final Logger logger = Logger.getLogger(Payroll_deduction_lookupRepository.class);

    private final Payroll_deduction_lookupDAO dao;
    private final Map<Long, Payroll_deduction_lookupDTO> mapById;
    private final Map<String, List<Payroll_deduction_lookupDTO>> mapByOrganogramKey;

    private Payroll_deduction_lookupRepository() {
        dao = Payroll_deduction_lookupDAO.getInstance();
        mapById = new ConcurrentHashMap<>();
        mapByOrganogramKey = new ConcurrentHashMap<>();
        RepositoryManager.getInstance().addRepository(this);
    }

    private static class LazyLoader {
        final static Payroll_deduction_lookupRepository INSTANCE = new Payroll_deduction_lookupRepository();
    }

    public static Payroll_deduction_lookupRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

    public void reload(boolean reloadAll) {
        logger.debug("Payroll_deduction_lookupRepository reload start for, reloadAll : " + reloadAll);
        List<Payroll_deduction_lookupDTO> dtoList = dao.getAllDTOs(reloadAll);
        if (dtoList != null && dtoList.size() > 0) {
            dtoList.stream()
                    .peek(this::removeIfPresent)
                    .filter(dto -> dto.isDeleted == 0)
                    .forEach(this::storeInCache);
        }
        logger.debug("Payroll_deduction_lookupRepository reload end for, reloadAll : " + reloadAll);
    }

    private void removeIfPresent(Payroll_deduction_lookupDTO dto) {
        if (dto == null) return;

        if (mapById.get(dto.iD) != null) {
            mapById.remove(dto.iD);
        }
        List<Payroll_deduction_lookupDTO> dtoList = mapByOrganogramKey.get(dto.organogramKey);
        if (dtoList != null) {
            dtoList.removeIf(d -> d.iD == dto.iD);
        }
    }

    private void storeInCache(Payroll_deduction_lookupDTO dto) {
        if (dto == null) return;

        mapById.put(dto.iD, dto);

        List<Payroll_deduction_lookupDTO> dtoListByOrganogramKey = mapByOrganogramKey.getOrDefault(
                dto.organogramKey, new ArrayList<>()
        );
        dtoListByOrganogramKey.add(dto);
        mapByOrganogramKey.put(dto.organogramKey, dtoListByOrganogramKey);
    }

    public List<Payroll_deduction_lookupDTO> getPayroll_deduction_lookupList() {
        return new ArrayList<>(this.mapById.values());
    }

    public Payroll_deduction_lookupDTO getDTOById(long id) {
        if (mapById.get(id) == null) {
            synchronized (LockManager.getLock(id + "PDLRGDBI")) {
                if (mapById.get(id) == null) {
                    storeInCache(dao.getDTOFromID(id));
                }
            }
        }
        return mapById.get(id);
    }

    public List<Payroll_deduction_lookupDTO> getDTOByOrganogramKey(int employmentCat, String organogramKey) {
        return mapByOrganogramKey.getOrDefault(organogramKey.trim(), new ArrayList<>())
                .stream()
                .filter(deductionLookupDTO -> deductionLookupDTO.employmentCat == employmentCat)
                .collect(Collectors.toList());
    }

    @Override
    public String getTableName() {
        return dao.getTableName();
    }


}