package jasper;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.PageSize;
import com.lowagie.text.pdf.PdfWriter;
import dbm.DBMR;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.HtmlExporter;
import net.sf.jasperreports.engine.export.JRGraphics2DExporter;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.ooxml.JRDocxExporter;
import net.sf.jasperreports.export.*;
import org.apache.commons.lang3.StringUtils;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

public class JasperUtil {

    private final String jasperFileSource;
    private final Map<String,Object> params;
    private Collection<?> data;
    private JasperPrint jasperPrint;

    public JasperUtil( String jasperFileSource, Map<String, Object> params ) throws JRException, IllegalArgumentException, ClassNotFoundException, SQLException, InstantiationException, IllegalAccessException {

        if( StringUtils.isBlank( jasperFileSource ) ) {
            throw new IllegalArgumentException( "Jasper file location can't be empty" );
        }

        this.jasperFileSource = jasperFileSource;
        this.params = params;

        fillWithJdbcConnection();
    }

    public JasperUtil( String jasperFileSource, Collection<?> data, Map<String, Object> params ) throws JRException, IllegalArgumentException, ClassNotFoundException, SQLException, InstantiationException, IllegalAccessException {

        if( StringUtils.isBlank( jasperFileSource ) ) {
            throw new IllegalArgumentException( "Jasper file location can't be empty" );
        }

        this.jasperFileSource = jasperFileSource;
        this.params = params;
        this.data = data;

        fillWithBean();
    }

    private void fillWithBean() throws JRException {

        JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource( data, false );
        jasperPrint = JasperFillManager.fillReport( this.getClass().getClassLoader().getResourceAsStream( jasperFileSource ), params, jrBeanCollectionDataSource );
    }

    private void fillWithJdbcConnection() throws JRException, ClassNotFoundException, SQLException, InstantiationException, IllegalAccessException {

        Connection connection = DBMR.getInstance().getConnection();
        jasperPrint = JasperFillManager.fillReport( this.getClass().getClassLoader().getResourceAsStream( jasperFileSource ), params, connection );
    }

    public void writePdfToStream( OutputStream os ) throws JRException {

        JRPdfExporter exporter = new JRPdfExporter();
        SimplePdfExporterConfiguration configuration = new SimplePdfExporterConfiguration();

        exporter.setExporterInput( new SimpleExporterInput( jasperPrint ) );
        exporter.setConfiguration( configuration );
        exporter.setExporterOutput( new SimpleOutputStreamExporterOutput( os ) );

        exporter.exportReport();
    }

    public void writeDocxToStream( OutputStream os ) throws JRException {

        JRDocxExporter exporter = new JRDocxExporter();
        SimpleDocxExporterConfiguration configuration = new SimpleDocxExporterConfiguration();

        exporter.setExporterInput( new SimpleExporterInput( jasperPrint) );
        exporter.setConfiguration( configuration );
        exporter.setExporterOutput( new SimpleOutputStreamExporterOutput( os) ) ;

        exporter.exportReport();
    }

    public void writeHtmlToStream( OutputStream os ) throws JRException {

        HtmlExporter exporter = new HtmlExporter();

        SimpleHtmlExporterConfiguration configuration = new SimpleHtmlExporterConfiguration();

        exporter.setExporterInput( new SimpleExporterInput( jasperPrint) );
        exporter.setConfiguration( configuration );
        exporter.setExporterOutput( new SimpleHtmlExporterOutput( os ) ) ;

        exporter.exportReport();
    }

    public void writeImagePdfToStream( OutputStream os ) throws IOException, DocumentException, JRException {

        Document document = new Document( PageSize.A4, 0,0,0,0 );
        PdfWriter pdfWriter = PdfWriter.getInstance( document, os );

        List<Image> pageImageList = getPagesAsImage();

        document.open();
        for( Image pageImage: pageImageList ){

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write( (BufferedImage)pageImage, "png", baos );
            com.lowagie.text.Image iTextImage = com.lowagie.text.Image.getInstance( baos.toByteArray() );
            document.add( iTextImage );
        }
        document.close();

        pdfWriter.flush();
        pdfWriter.close();
    }

    public void writeImageToStream( OutputStream os, Integer page ) throws JRException, IOException {

        List<Image> pageImageList = getPagesAsImage();
        BufferedImage pageImage = (BufferedImage) pageImageList.get( page - 1 );
        ImageIO.write( pageImage, "jpg", os );
    }

    public List<Image> getPagesAsImage() throws JRException {

        Integer pages = jasperPrint.getPages().size();
        List<Image> pageImages = new ArrayList<>();

        for( int i = 0; i< pages; i++ ){

            /*Image curPageImg = JasperPrintManager.printPageToImage( jasperPrint, i, 1f );
            pageImages.add( curPageImg );*/

            JRGraphics2DExporter exporter = new JRGraphics2DExporter();
            SimpleGraphics2DExporterConfiguration configuration = new SimpleGraphics2DExporterConfiguration();

            BufferedImage pageImage = new BufferedImage( jasperPrint.getPageWidth() + 1, jasperPrint.getPageHeight() + 1, BufferedImage.TYPE_INT_RGB );

            SimpleGraphics2DExporterOutput exporterOutput = new SimpleGraphics2DExporterOutput();
            exporterOutput.setGraphics2D((Graphics2D) pageImage.getGraphics());

            exporter.setExporterInput( new SimpleExporterInput( jasperPrint ) );
            exporter.setConfiguration( configuration );
            exporter.setExporterOutput( exporterOutput );
            exporter.exportReport();
            
            pageImages.add( pageImage );
        }

        return pageImages;
    }
}
