package supplier_institution;

import util.CommonDTO;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import static util.UtilCharacter.getDataByLanguage;


public class Supplier_institutionDTO extends CommonDTO {
    public String nameEn = "";
    public String nameBn = "";
    public String addressEn = "";
    public String addressBn = "";
    public String mobileNo = "";
    public String nidNo = "";
    public String tinNo = "";
    public long fileDropzone = -1;
    public long modifiedBy = -1;
    public long insertedBy = -1;
    public long insertionTime = -1;

    public String getIssuedToText(String language) {
        return Stream.of(getDataByLanguage(language, nameBn, nameEn), getDataByLanguage(language, addressBn, addressEn))
                     .filter(s -> !s.isEmpty())
                     .collect(Collectors.joining(", "));
    }

    @Override
    public String toString() {
        return "$Supplier_institutionDTO[" +
               " iD = " + iD +
               " nameEn = " + nameEn +
               " nameBn = " + nameBn +
               " addressEn = " + addressEn +
               " addressBn = " + addressBn +
               " mobileNo = " + mobileNo +
               " nidNo = " + nidNo +
               " tinNo = " + tinNo +
               " modifiedBy = " + modifiedBy +
               " lastModificationTime = " + lastModificationTime +
               " insertedBy = " + insertedBy +
               " insertionTime = " + insertionTime +
               " isDeleted = " + isDeleted +
               "]";
    }
}