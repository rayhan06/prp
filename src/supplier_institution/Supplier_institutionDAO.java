package supplier_institution;

import common.CommonDAOService;
import org.apache.log4j.Logger;
import util.CommonDTO;
import util.StringUtils;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

@SuppressWarnings({"Duplicates"})
public class Supplier_institutionDAO implements CommonDAOService<Supplier_institutionDTO> {
    private static final Logger logger = Logger.getLogger(Supplier_institutionDAO.class);

    private static final String addQuery =
            "INSERT INTO {tableName} (name_en,name_bn,address_en,address_bn,mobile_no,nid_no,tin_no,file_dropzone,"
                    .concat("search_column,modified_by,lastModificationTime,inserted_by,insertion_time,isDeleted,ID) ")
                    .concat("VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

    private static final String updateQuery =
            "UPDATE {tableName} SET name_en=?,name_bn=?,address_en=?,address_bn=?,mobile_no=?,nid_no=?,tin_no=?,"
                    .concat("file_dropzone=?,search_column=?,modified_by=?,lastModificationTime=? WHERE ID=?");

    private Supplier_institutionDAO() {
        searchMap.put("name_en", " and (name_en like ?)");
        searchMap.put("name_bn", " and (name_bn like ?)");
        searchMap.put("mobile_no", " and (mobile_no like ?)");
        searchMap.put("tin_no", " and (tin_no like ?)");
        searchMap.put("AnyField", " and (search_column like ?)");
    }

    private final Map<String, String> searchMap = new HashMap<>();

    private static class LazyLoader {
        static final Supplier_institutionDAO INSTANCE = new Supplier_institutionDAO();
    }

    public static Supplier_institutionDAO getInstance() {
        return LazyLoader.INSTANCE;
    }

    public void setSearchColumn(Supplier_institutionDTO dto) {
        dto.searchColumn = dto.nameEn + " " + dto.nameBn + " "
                           + dto.mobileNo + " " + StringUtils.convertToBanNumber(dto.mobileNo);
    }

    @Override
    public void set(PreparedStatement ps, Supplier_institutionDTO dto, boolean isInsert) throws SQLException {
        setSearchColumn(dto);
        int index = 0;
        ps.setString(++index, dto.nameEn);
        ps.setString(++index, dto.nameBn);
        ps.setString(++index, dto.addressEn);
        ps.setString(++index, dto.addressBn);
        ps.setString(++index, dto.mobileNo);
        ps.setString(++index, dto.nidNo);
        ps.setString(++index, dto.tinNo);
        ps.setLong(++index, dto.fileDropzone);
        ps.setString(++index, dto.searchColumn);
        ps.setLong(++index, dto.modifiedBy);
        ps.setLong(++index, dto.lastModificationTime);
        if (isInsert) {
            ps.setLong(++index, dto.insertedBy);
            ps.setLong(++index, dto.insertionTime);
            ps.setInt(++index, 0 /* isDeleted */);
        }
        ps.setLong(++index, dto.iD);
    }

    @Override
    public Supplier_institutionDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Supplier_institutionDTO dto = new Supplier_institutionDTO();
            dto.iD = rs.getLong("ID");
            dto.nameEn = rs.getString("name_en");
            dto.nameBn = rs.getString("name_bn");
            dto.addressEn = rs.getString("address_en");
            dto.addressBn = rs.getString("address_bn");
            dto.mobileNo = rs.getString("mobile_no");
            dto.nidNo = rs.getString("nid_no");
            dto.tinNo = rs.getString("tin_no");
            dto.fileDropzone = rs.getLong("file_dropzone");
            dto.searchColumn = rs.getString("search_column");
            dto.modifiedBy = rs.getLong("modified_by");
            dto.lastModificationTime = rs.getLong("lastModificationTime");
            dto.insertedBy = rs.getLong("inserted_by");
            dto.insertionTime = rs.getLong("insertion_time");
            dto.isDeleted = rs.getInt("isDeleted");
            return dto;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    @Override
    public String getTableName() {
        return "supplier_institution";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    @Override
    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Supplier_institutionDTO) commonDTO, addQuery, true);
    }

    @Override
    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Supplier_institutionDTO) commonDTO, updateQuery, false);
    }
}