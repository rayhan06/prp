CREATE TABLE supplier_institution
(
    ID                   BIGINT(20) PRIMARY KEY,
    name_en              VARCHAR(512),
    name_bn              VARCHAR(1024),
    address_en           VARCHAR(512),
    address_bn           VARCHAR(1024),
    mobile_no            VARCHAR(32),
    nid_no               VARCHAR(32),
    tin_no               VARCHAR(64),
    file_dropzone        BIGINT(20),
    modified_by          BIGINT(20),
    lastModificationTime BIGINT(20) DEFAULT -62135791200000,
    inserted_by          BIGINT(20),
    insertion_time       BIGINT(20) DEFAULT -62135791200000,
    isDeleted            INT(11) DEFAULT 0,
    search_column        VARCHAR(4096)
) ENGINE = MyISAM
  DEFAULT CHARSET = utf8
  COLLATE = utf8_unicode_ci;