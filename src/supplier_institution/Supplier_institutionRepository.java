package supplier_institution;

import org.apache.log4j.Logger;
import pb.OptionDTO;
import pb.Utils;
import repository.Repository;
import repository.RepositoryManager;
import util.LockManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;


public class Supplier_institutionRepository implements Repository {
    private static final Logger logger = Logger.getLogger(Supplier_institutionRepository.class);
    private final Supplier_institutionDAO supplierInstitutionDAO;
    private final Map<Long, Supplier_institutionDTO> mapById;

    private Supplier_institutionRepository() {
        supplierInstitutionDAO = Supplier_institutionDAO.getInstance();
        mapById = new ConcurrentHashMap<>();
        RepositoryManager.getInstance().addRepository(this);
    }

    private static class LazyLoader {
        final static Supplier_institutionRepository INSTANCE = new Supplier_institutionRepository();
    }

    public static Supplier_institutionRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

    public void reload(boolean reloadAll) {
        logger.debug("Supplier_institutionRepository reload start, reloadAll = " + reloadAll);
        List<Supplier_institutionDTO> dtoList = supplierInstitutionDAO.getAllDTOs(reloadAll);
        if (dtoList.size() > 0) {
            dtoList.stream()
                   .peek(this::removeIfPresent)
                   .filter(dot -> dot.isDeleted == 0)
                   .forEach(dto -> mapById.put(dto.iD, dto));
        }
        logger.debug("Supplier_institutionRepository reload end, reloadAll = " + reloadAll);
    }

    private void removeIfPresent(Supplier_institutionDTO newDTO) {
        if (newDTO == null) return;
        if (mapById.get(newDTO.iD) != null) {
            mapById.remove(newDTO.iD);
        }
    }


    public List<Supplier_institutionDTO> getAllDTOs() {
        return new ArrayList<>(this.mapById.values());
    }


    public Supplier_institutionDTO getDTOById(long id) {
        if (mapById.get(id) == null) {
            synchronized (LockManager.getLock("SIRGDBI" + id)) {
                if (mapById.get(id) == null) {
                    Supplier_institutionDTO taskTypeDTO = supplierInstitutionDAO.getDTOFromID(id);
                    if (taskTypeDTO != null) {
                        mapById.put(taskTypeDTO.iD, taskTypeDTO);
                    }
                }
            }
        }
        return mapById.get(id);
    }

    @Override
    public String getTableName() {
        return supplierInstitutionDAO.getTableName();
    }

    public String buildOptions(String language, Long selectedId) {
        List<OptionDTO> optionDTOList =
                getAllDTOs().stream()
                            .map(dto -> new OptionDTO(dto.nameEn, dto.nameBn, String.valueOf(dto.iD)))
                            .collect(Collectors.toList());
        return Utils.buildOptions(optionDTOList, language, selectedId == null ? null : String.valueOf(selectedId));
    }

    public String getIssuedToText(long id, String language) {
        Supplier_institutionDTO supplierInstitutionDTO = getDTOById(id);
        return supplierInstitutionDTO == null ? "" : supplierInstitutionDTO.getIssuedToText(language);
    }
}