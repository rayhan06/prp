package supplier_institution;

import common.BaseServlet;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import pb.Utils;
import permission.MenuConstants;
import user.UserDTO;
import util.CommonDTO;
import util.HttpRequestUtils;
import util.StringUtils;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;


@WebServlet("/Supplier_institutionServlet")
@MultipartConfig
@SuppressWarnings({"Duplicates"})
public class Supplier_institutionServlet extends BaseServlet {
    private static final long serialVersionUID = 1L;
    private static final Logger logger = Logger.getLogger(Supplier_institutionServlet.class);

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        long currentTime = System.currentTimeMillis();
        Supplier_institutionDTO supplierInstitutionDTO;
        if (addFlag) {
            supplierInstitutionDTO = new Supplier_institutionDTO();
            supplierInstitutionDTO.insertedBy = userDTO.ID;
            supplierInstitutionDTO.insertionTime = currentTime;
        } else {
            supplierInstitutionDTO = Supplier_institutionDAO.getInstance().getDTOFromID(Long.parseLong(request.getParameter("iD")));
        }
        supplierInstitutionDTO.modifiedBy = userDTO.ID;
        supplierInstitutionDTO.lastModificationTime = currentTime;

        boolean isLanguageEnglish = "english".equalsIgnoreCase(HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language);

        supplierInstitutionDTO.nameEn = Utils.cleanAndGetMandatoryString(
                request.getParameter("nameEn"),
                isLanguageEnglish ? "Enter Supplier's Name in English" : "সরবরাহকারীর ইংরেজি নাম দিন"
        );
        supplierInstitutionDTO.nameBn = Utils.cleanAndGetMandatoryString(
                request.getParameter("nameBn"),
                isLanguageEnglish ? "Enter Supplier's Name in Bangla" : "সরবরাহকারীর বাংলা নাম দিন"
        );
        supplierInstitutionDTO.addressEn = Utils.cleanAndGetOptionalString(request.getParameter("addressEn"));
        supplierInstitutionDTO.addressBn = Utils.cleanAndGetOptionalString(request.getParameter("addressBn"));
        supplierInstitutionDTO.mobileNo = Utils.parseOptionalMobileNumber(
                request.getParameter("mobileNo"),
                isLanguageEnglish ? "Write correct mobile number" : "সঠিক মোবাইল নম্বর লিখুন"
        );
        supplierInstitutionDTO.nidNo = Utils.cleanAndGetOptionalString(request.getParameter("nidNo"));
        supplierInstitutionDTO.tinNo = Utils.cleanAndGetOptionalString(request.getParameter("tinNo"));

        String Value;
        Value = request.getParameter("fileDropzone");
        if (Value != null && !Value.equalsIgnoreCase("")) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }
        if (Value != null && !Value.equalsIgnoreCase("")) {
            supplierInstitutionDTO.fileDropzone = Long.parseLong(Value);
            if (!addFlag) {
                String fileDropzoneFilesToDelete = request.getParameter("fileDropzoneFilesToDelete");
                String[] deleteArray = fileDropzoneFilesToDelete.split(",");
                for (int i = 0; i < deleteArray.length; i++) {
                    System.out.println("going to delete " + deleteArray[i]);
                    if (i > 0) {
                        filesDAO.delete(Long.parseLong(deleteArray[i]));
                    }
                }
            }
        }
        logger.debug(supplierInstitutionDTO);

        if (addFlag) {
            Supplier_institutionDAO.getInstance().add(supplierInstitutionDTO);
        } else {
            Supplier_institutionDAO.getInstance().update(supplierInstitutionDTO);
        }
        return supplierInstitutionDTO;
    }

    @Override
    public String getTableName() {
        return Supplier_institutionDAO.getInstance().getTableName();
    }

    @Override
    public String getServletName() {
        return "Supplier_institutionServlet";
    }

    @Override
    public Supplier_institutionDAO getCommonDAOService() {
        return Supplier_institutionDAO.getInstance();
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[]{MenuConstants.SUPPLIER_INSTITUTION_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[]{MenuConstants.SUPPLIER_INSTITUTION_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[]{MenuConstants.SUPPLIER_INSTITUTION_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Supplier_institutionServlet.class;
    }
}