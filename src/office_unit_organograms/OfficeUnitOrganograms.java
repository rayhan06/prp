package office_unit_organograms;

/*
 * @author Md. Erfan Hossain
 * @created 24/04/2021 - 3:40 PM
 * @project parliament
 */

import designations.DesignationsEnum;

public class OfficeUnitOrganograms {
    public long id;
    public int office_id;
    public long office_unit_id;
    public int superior_unit_id;
    public long superior_designation_id;
    public long ref_origin_unit_org_id;
    public int ref_sup_origin_unit_desig_id;
    public int ref_sup_origin_unit_id;
    public String designation_eng = "";
    public String designation_bng = "";
    public String short_name_eng;
    public String short_name_bng;
    public int designation_level;
    public String briefDescriptionEng;
    public String briefDescriptionBng;
    public String designation_description;
    public boolean status;
    public boolean is_admin;
    public int created_by;
    public int modified_by;
    public long created;
    public long modified;
    public String role_type;
    public boolean isDeleted;
    public long lastModificationTime;
    public int approval_path_type;
    public int job_grade_type_cat;
    public String orgTree;
    public int layer1 = 0;
    public int layer2 = 0;
    public boolean isVacant = true;
    public long designationsId = -1;
    public boolean isVirtual = false;
    public int orderValue = 99999;
    public long substituteOrganogramId = -1;
    public long assignedEmployeeRecordId = -1;

    public boolean isDesignation(DesignationsEnum designationsEnum) {
        return designationsId == designationsEnum.getId();
    }

    @Override
    public String toString() {
        return "OfficeUnitOrganograms{" +
               "id=" + id +
               ", office_unit_id=" + office_unit_id +
               ", superior_unit_id=" + superior_unit_id +
               ", superior_designation_id=" + superior_designation_id +
               ", designation_eng='" + designation_eng + '\'' +
               ", designation_bng='" + designation_bng + '\'' +
               ", short_name_eng='" + short_name_eng + '\'' +
               ", short_name_bng='" + short_name_bng + '\'' +
               ", designation_description='" + designation_description + '\'' +
               ", status=" + status +
               ", role_type='" + role_type + '\'' +
               ", job_grade_type_cat=" + job_grade_type_cat +
               ", orgTree='" + orgTree + '\'' +
               ", isVacant=" + isVacant +
               ", substituteOrganogramId=" + substituteOrganogramId +
               '}';
    }
}
