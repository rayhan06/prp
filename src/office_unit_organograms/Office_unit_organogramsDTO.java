package office_unit_organograms;
import java.util.*; 


public class Office_unit_organogramsDTO {

	public long iD = 0;
	public String  roleType = "";
	public int approvalPathType = 0;
	public int isDeleted = 0;
	public long lastModificationTime = 0;
	
	
    @Override
	public String toString() {
            return "$Office_unit_organogramsDTO[" +
            " iD = " + iD +
            " roleType = " + roleType +
            " approvalPathType = " + approvalPathType +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}