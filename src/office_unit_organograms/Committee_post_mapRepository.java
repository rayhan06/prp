package office_unit_organograms;

import com.google.gson.Gson;
import org.apache.log4j.Logger;
import repository.Repository;
import repository.RepositoryManager;
import util.LockManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.stream.Collectors;

@SuppressWarnings({"unused"})
public class Committee_post_mapRepository implements Repository {
    private static final Logger logger = Logger.getLogger(Committee_post_mapRepository.class);
    private static final Committee_post_mapDAO committee_post_mapDAO = Committee_post_mapDAO.getInstance();

    private static final Map<Long, Committee_post_mapDTO> mapById = new ConcurrentHashMap<>();
    private static final Map<Long, Committee_post_mapDTO> mapByCommittee = new ConcurrentHashMap<>();
    private static List<Committee_post_mapDTO> allList = new ArrayList<>();

    private static final ReentrantReadWriteLock lock = new ReentrantReadWriteLock();
    private static final ReentrantReadWriteLock.ReadLock readLock = lock.readLock();
    private static final ReentrantReadWriteLock.WriteLock writeLock = lock.writeLock();

    private static final Gson gson = new Gson();


    private Committee_post_mapRepository() {
        RepositoryManager.getInstance().addRepository(this);
    }

    private static class LazyLoader {
        final static Committee_post_mapRepository INSTANCE = new Committee_post_mapRepository();
    }

    public static Committee_post_mapRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

    public void reload(boolean reloadAll) {
        logger.debug("reload Committee_post_mapRepository for reloadAll : "+reloadAll);
        List<Committee_post_mapDTO> dtoList = committee_post_mapDAO.getAllDTOs(reloadAll);
        doCaching(dtoList);
        logger.debug("Committee_post_mapRepository, reload is completed for reloadAll : "+reloadAll);
    }

    @Override
    public void reloadWithExactModificationTime(long time) {
        List<Committee_post_mapDTO> dtoList = committee_post_mapDAO.getAllDTOsExactLastModificationTime(time);
        doCaching(dtoList);
    }

    private void doCaching(List<Committee_post_mapDTO> list) {
        writeLock.lock();
        try {
            list.stream()
                    .peek(this::removeIfFound)
                    .forEach(this::updateCache);
            allList = new ArrayList<>(mapById.values());
        } finally {
            writeLock.unlock();
        }
    }

    private void updateCache(Committee_post_mapDTO dto) {
        if (dto != null && dto.isDeleted == 0) {
            mapById.put(dto.iD, dto);
            mapByCommittee.put(dto.committeeId, dto);
            allList.add(dto);
        }
    }

    private void removeIfFound(Committee_post_mapDTO dto) {
        if (dto != null) {
            Committee_post_mapDTO oldDTO = mapById.get(dto.iD);
            if (oldDTO != null) {
                mapById.remove(dto.iD);
                mapByCommittee.remove(dto.committeeId);
                allList.remove(oldDTO);
            }
        }
    }

    public Committee_post_mapDTO clone(Committee_post_mapDTO dto) {
        if (dto == null) {
            return null;
        }
        String raw = gson.toJson(dto);
        return gson.fromJson(raw, Committee_post_mapDTO.class);
    }


    public List<Committee_post_mapDTO> getCommittee_post_mapList() {
        readLock.lock();
        try {
            return allList;
        } finally {
            readLock.unlock();
        }
    }

    public List<Committee_post_mapDTO> getByReplacementPost(long replacementPostId) {
        readLock.lock();
        try {
            return allList.stream()
                    .filter(e -> e.replacementPostId == replacementPostId)
                    .collect(Collectors.toList());
        } finally {
            readLock.unlock();
        }
    }

    public List<Committee_post_mapDTO> getByPost(long postId) {
        readLock.lock();
        try {
            return allList.stream()
                    .filter(e -> e.postId == postId)
                    .collect(Collectors.toList());
        } finally {
            readLock.unlock();
        }
    }

    public List<Committee_post_mapDTO> copyCommittee_post_mapList() {
        readLock.lock();
        try {
            return allList
                    .stream()
                    .map(this::clone)
                    .collect(Collectors.toList());
        } finally {
            readLock.unlock();
        }

    }

    public Committee_post_mapDTO getByID(long id) {
        return getFromMap(id, mapById);
    }

    public Committee_post_mapDTO copyById(long id) {
        return clone(getByID(id));
    }

    public Committee_post_mapDTO getByCommittee(long committeeId) {
        return getFromMap(committeeId, mapByCommittee);
    }

    public Committee_post_mapDTO copyByCommittee(long committeeId) {
        return clone(getByCommittee(committeeId));
    }

    private Committee_post_mapDTO getFromMap(long requestId, Map<Long, Committee_post_mapDTO> map) {
        readLock.lock();
        try {
            if (map.get(requestId) == null) {
                synchronized (LockManager.getLock("COMMITTEE_MAP_" + requestId)) {
                    if (map.get(requestId) == null) {
                        Committee_post_mapDTO dto = committee_post_mapDAO.getDTOFromID(requestId);
                        if (dto != null) {
                            readLock.unlock();
                            writeLock.lock();
                            try {
                                updateCache(dto);
                                readLock.lock();
                            } finally {
                                writeLock.unlock();
                            }
                        }
                    }
                }
            }
            return map.get(requestId);
        } finally {
            readLock.unlock();
        }
    }

    @Override
    public String getTableName() {
        return committee_post_mapDAO.getTableName();
    }
}