package office_unit_organograms;

import common.CommonDAOService;
import org.apache.log4j.Logger;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;


public class Committee_post_mapDAO implements CommonDAOService<Committee_post_mapDTO> {

    Logger logger = Logger.getLogger(getClass());

    private static String addQuery = "";
    private static String updateQuery = "";

    private Committee_post_mapDAO() {
        String[] columnNames = new String[]
                {
                        "committee_id",
                        "post_id",
                        "replacement_post_id",
                        "modified_by",
                        "lastModificationTime",
                        "isDeleted",
                        "ID"
                };
        updateQuery = getUpdateQuery2(columnNames);
        addQuery = getAddQuery2(columnNames);

    }

    private static class LazyLoader {
        static final Committee_post_mapDAO INSTANCE = new Committee_post_mapDAO();
    }

    public static Committee_post_mapDAO getInstance() {
        return LazyLoader.INSTANCE;
    }

    @Override
    public void set(PreparedStatement ps, Committee_post_mapDTO committee_post_mapDTO, boolean isInsert) throws SQLException {
        int index = 0;
        long lastModificationTime = committee_post_mapDTO.lastModificationTime == 0 ? System.currentTimeMillis() : committee_post_mapDTO.lastModificationTime;
        ps.setObject(++index, committee_post_mapDTO.committeeId);
        ps.setObject(++index, committee_post_mapDTO.postId);
        ps.setObject(++index, committee_post_mapDTO.replacementPostId);
        ps.setObject(++index, committee_post_mapDTO.modifiedBy);
        ps.setObject(++index, lastModificationTime);
        if (isInsert) {
            ps.setObject(++index, committee_post_mapDTO.isDeleted);
        }
        ps.setObject(++index, committee_post_mapDTO.iD);

    }

    @Override
    public Committee_post_mapDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Committee_post_mapDTO committee_post_mapDTO = new Committee_post_mapDTO();
            committee_post_mapDTO.committeeId = rs.getLong("committee_id");
            committee_post_mapDTO.postId = rs.getLong("post_id");
            committee_post_mapDTO.replacementPostId = rs.getLong("replacement_post_id");
            committee_post_mapDTO.modifiedBy = rs.getLong("modified_by");
            committee_post_mapDTO.lastModificationTime = rs.getLong("lastModificationTime");
            committee_post_mapDTO.isDeleted = rs.getInt("isDeleted");
            committee_post_mapDTO.iD = rs.getLong("ID");
            return committee_post_mapDTO;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    @Override
    public String getTableName() {
        return "committee_post_map";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return null;
    }

    @Override
    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Committee_post_mapDTO) commonDTO, addQuery, true);
    }

    @Override
    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Committee_post_mapDTO) commonDTO, updateQuery, false);
    }
}