package office_unit_organograms;

import employee_offices.EmployeeOfficeDTO;
import employee_offices.EmployeeOfficeRepository;
import office_units.Office_unitsDTO;
import office_units.Office_unitsRepository;
import office_units.Organization;
import org.apache.log4j.Logger;
import pb.OptionDTO;
import pb.Utils;
import repository.Repository;
import repository.RepositoryManager;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import workflow.WorkflowController;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@SuppressWarnings({"unused"})
public class OfficeUnitOrganogramsRepository implements Repository {
    private final Office_unit_organogramsDAO office_unit_organogramsDAO = new Office_unit_organogramsDAO();

    private static final Logger logger = Logger.getLogger(OfficeUnitOrganogramsRepository.class);
    private final Map<Long, OfficeUnitOrganograms> mapById;
    private Map<Long, List<OfficeUnitOrganograms>> mapByOfficeUnitId;
    private Map<Long, List<OfficeUnitOrganograms>> mapBySuperiorDesignationId;
    private List<OfficeUnitOrganograms> allOfficeUnitOrganograms;

    public static class OrganogramEmployeeInfo {
        public OfficeUnitOrganograms officeUnitOrganograms;
    }

    private OfficeUnitOrganogramsRepository() {
        mapById = new ConcurrentHashMap<>();
        mapByOfficeUnitId = new ConcurrentHashMap<>();
        mapBySuperiorDesignationId = new ConcurrentHashMap<>();
        allOfficeUnitOrganograms = new ArrayList<>();
        RepositoryManager.getInstance().addRepository(this);
    }

    private static class LazyLoader {
        final static OfficeUnitOrganogramsRepository INSTANCE = new OfficeUnitOrganogramsRepository();
    }

    public static OfficeUnitOrganogramsRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

    public synchronized void reload(boolean reloadAll) {
        logger.debug("OfficeUnitOrganogramsRepository loading start for reloadAll: " + reloadAll);
        List<OfficeUnitOrganograms> dtoList = office_unit_organogramsDAO.getAllOffice_unit_organograms(reloadAll);
        updateCache(dtoList);
        logger.debug("OfficeUnitOrganogramsRepository loading end for reloadAll: " + reloadAll);
    }

    @Override
    public void reloadWithExactModificationTime(long time) {
        List<OfficeUnitOrganograms> dtoList = office_unit_organogramsDAO.getAllDTOsExactLastModificationTime(time);
        updateCache(dtoList);
    }

    private void updateCache(List<OfficeUnitOrganograms> dtoList) {
        if (dtoList != null && dtoList.size() > 0) {
            dtoList.stream()
                    .peek(this::removeIfPresent)
                    .filter(dto -> !dto.isDeleted)
                    .forEach(dto -> mapById.put(dto.id, dto));
            mapByOfficeUnitId = mapById.entrySet()
                    .parallelStream()
                    .map(Map.Entry::getValue)
                    .collect(Collectors.groupingBy(e -> e.office_unit_id));
            mapBySuperiorDesignationId = mapById.entrySet()
                    .parallelStream()
                    .map(Map.Entry::getValue)
                    .collect(Collectors.groupingBy(e -> e.superior_designation_id));
            allOfficeUnitOrganograms = new ArrayList<>(mapById.values());
            allOfficeUnitOrganograms.sort(Comparator.comparing(e -> e.id));
        }
    }

    private void removeIfPresent(OfficeUnitOrganograms dto) {
        if (mapById.get(dto.id) != null) {
            mapById.remove(dto.id);
        }
    }

    @Override
    public String getTableName() {
        return "office_unit_organograms";
    }

    public void addNewDto(OfficeUnitOrganograms dto) {
        OfficeUnitOrganograms old = mapById.get(dto.id);
        if (old != null) {
            mapById.remove(old.id);
        }
        if (!dto.isDeleted) {
            mapById.put(dto.id, dto);
        }
    }

    public void removeDto(long id) {
        OfficeUnitOrganograms old = mapById.get(id);
        if (old != null) {
            mapById.remove(old.id);
        }
    }

    public OfficeUnitOrganograms getById(long id) {
        if (mapById.get(id) == null) {
            synchronized (this) {
                if (mapById.get(id) == null) {
                    OfficeUnitOrganograms organograms = office_unit_organogramsDAO.getOffice_unit_organogramsDTOByID(id);
                    if (organograms != null) {
                        mapById.put(id, organograms);
                        List<OfficeUnitOrganograms> list = mapByOfficeUnitId.getOrDefault(organograms.office_unit_id, new ArrayList<>());
                        list.add(organograms);
                        mapByOfficeUnitId.put(organograms.office_unit_id, list);

                        list = mapByOfficeUnitId.getOrDefault(organograms.office_unit_id, new ArrayList<>());
                        list.add(organograms);
                        mapByOfficeUnitId.put(organograms.office_unit_id, list);

                        list = mapBySuperiorDesignationId.getOrDefault(organograms.superior_designation_id, new ArrayList<>());
                        list.add(organograms);
                        mapBySuperiorDesignationId.put(organograms.superior_designation_id, list);
                    }
                }
            }
        }
        //System.out.println("Returning OfficeUnitOrganograms called by " + id);
        return mapById.get(id);
    }

    public List<OfficeUnitOrganograms> getOffice_unit_organogramsList() {
        return allOfficeUnitOrganograms;
    }

    public List<OfficeUnitOrganograms> getByOfficeUnitId(long officeUintId) {
        return mapByOfficeUnitId.get(officeUintId);
    }

    public String buildOptionsForDesignation(String language, Long selectedId, Long officeUnitId) {
        List<OfficeUnitOrganograms> organogramList = mapByOfficeUnitId.get(officeUnitId);
        List<OptionDTO> optionDTOList = null;
        if (organogramList != null && organogramList.size() > 0) {
            optionDTOList = organogramList.stream()
                    .map(model -> new OptionDTO(model.designation_eng, model.designation_bng, String.valueOf(model.id)))
                    .collect(Collectors.toList());
        }
        return Utils.buildOptions(optionDTOList, language, selectedId == null ? null : String.valueOf(selectedId));
    }

    public String buildOptionsForDesignationWithParentDesignation(String language, Long selectedId, long officeUnitId) {
        List<OptionDTO> optionDTOList = null;

        Office_unitsDTO officeUnitsDTO = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(officeUnitId);
        if (officeUnitsDTO != null) {
            List<OfficeUnitOrganograms> organogramList = new ArrayList<>();
            if (!officeUnitsDTO.isAbstractOffice && mapByOfficeUnitId.get(officeUnitId) != null) {
                organogramList = mapByOfficeUnitId.get(officeUnitId);
            }

            Office_unitsDTO parentOfficeUnitsDTO = Office_unitsRepository.getInstance().getParentOffice(officeUnitsDTO);
            if (parentOfficeUnitsDTO != null) {
                List<OfficeUnitOrganograms> parentOrgList = mapByOfficeUnitId.get(parentOfficeUnitsDTO.iD);
                if (parentOrgList != null) {
                    organogramList.addAll(parentOrgList);
                }
            }

            boolean isLangEng = "English".equalsIgnoreCase(language);

            if (organogramList != null && organogramList.size() > 0) {
                optionDTOList = organogramList.stream()
                        .map(model -> new OptionDTO(model.designation_eng, model.designation_bng, String.valueOf(model.id), isLangEng ? model.briefDescriptionEng : model.briefDescriptionBng))
                        .collect(Collectors.toList());
            }
        }

        return Utils.buildOptions(optionDTOList, language, selectedId == null ? null : String.valueOf(selectedId));
    }

    public String buildOptionsForDesignationWithoutSelectOption(String language) {
        List<OptionDTO> optionDTOList = null;
        if (allOfficeUnitOrganograms != null && allOfficeUnitOrganograms.size() > 0) {
            optionDTOList = allOfficeUnitOrganograms.stream()
                    .map(model -> new OptionDTO(model.designation_eng, model.designation_bng, String.valueOf(model.id)))
                    .collect(Collectors.toList());
        }
        return Utils.buildOptionsWithoutSelectOption(optionDTOList, language);
    }

    public String getDesignation(String language, long id) {
        OfficeUnitOrganograms organograms = getById(id);
        return organograms == null ? "" : (language.equalsIgnoreCase("English") ?
                (organograms.designation_eng == null ? "" : organograms.designation_eng)
                : (organograms.designation_bng == null ? "" : organograms.designation_bng));
    }

    public String getDesignationWithOffice(String language, long id, String formatStr) {
        OfficeUnitOrganograms organograms = getById(id);
        if (organograms == null) return "";
        String fullDesignation = "";
        boolean isEng = language.equalsIgnoreCase("English");
        Office_unitsDTO officeUnitsDTO = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(organograms.office_unit_id);
        if (officeUnitsDTO != null) {
            return String.format(
                    formatStr,
                    isEng ? organograms.designation_eng : organograms.designation_bng,
                    isEng ? officeUnitsDTO.unitNameEng : officeUnitsDTO.unitNameBng
            );
        }
        return isEng ? organograms.designation_eng : organograms.designation_bng;
    }

    public String getDesignationWithOffice(String language, long id) {
        return getDesignationWithOffice(language, id, "%s (%s)");
    }

    public String getDesignation(String language, String id) {
        try {
            OfficeUnitOrganograms organograms = getById(Long.parseLong(id.trim()));
            return organograms == null ? "" : (language.equalsIgnoreCase("English") ?
                    (organograms.designation_eng == null ? "" : organograms.designation_eng)
                    : (organograms.designation_bng == null ? "" : organograms.designation_bng));
        } catch (NumberFormatException ex) {
            logger.error(ex);
            return "";
        }
    }

    public String buildOptions(String language, long officeUnitId, long parentOfficeUnitId) {
        List<OptionDTO> optionDTOList = null;
        List<OfficeUnitOrganograms> dtoList = mapByOfficeUnitId.entrySet()
                .stream()
                .filter(e -> e.getKey() == officeUnitId || e.getKey() == parentOfficeUnitId)
                .flatMap(e -> e.getValue().stream())
                .collect(Collectors.toList());
        if (dtoList.size() > 0) {
            optionDTOList = dtoList.stream()
                    .map(dto -> new OptionDTO(dto.designation_eng, dto.designation_bng, String.valueOf(dto.id)))
                    .collect(Collectors.toList());
        }
        return Utils.buildOptions(optionDTOList, language, null);
    }

    public Set<Long> getDescentsOrganogram(long organogramId) {
        OfficeUnitOrganograms officeUnitOrganograms = getById(organogramId);
        if (officeUnitOrganograms == null) {
            return new HashSet<>();
        }
        return allOfficeUnitOrganograms.stream()
                .filter(e -> e.orgTree.startsWith(officeUnitOrganograms.orgTree))
                .map(e -> e.id)
                .collect(Collectors.toSet());
    }

    public Set<Long> getAllDescentsOrganogramIds(long officeUnitId) {
        return Office_unitsRepository.getInstance().getDescentsOfficeUnitId(officeUnitId)
                .stream()
                .map(mapByOfficeUnitId::get)
                .filter(Objects::nonNull)
                .flatMap(Collection::stream)
                .map(e -> e.id)
                .collect(Collectors.toSet());
    }

    public Set<Long> getAllDescentsOrganogramIdsInclusive(long officeUnitId) {
        Set<Long> orgs = Office_unitsRepository.getInstance().getDescentsOfficeUnitId(officeUnitId)
                .stream()
                .map(mapByOfficeUnitId::get)
                .filter(Objects::nonNull)
                .flatMap(Collection::stream)
                .map(e -> e.id)
                .collect(Collectors.toSet());

        List<OfficeUnitOrganograms> officeUnitOrganogramList = getByOfficeUnitId(officeUnitId);
        if (officeUnitOrganogramList != null) {
            for (OfficeUnitOrganograms officeUnitOrganogram : officeUnitOrganogramList) {
                orgs.add(officeUnitOrganogram.id);
            }
        }

        return orgs;
    }
    
    public Set<Long> getAllDescentsOrganogramIdsWithClassInclusive(long officeUnitId, int employeeClass) {
        Set<Long> orgs = getAllDescentsOrganogramIdsInclusive(officeUnitId);
        Set<Long> orgsToReturn = new HashSet<Long>();
        for(Long org: orgs)
        {
        	String username = WorkflowController.getUserNameFromOrganogramId(org);
        	if(username.startsWith(employeeClass + ""))
        	{
        		orgsToReturn.add(org);
        	}
        }

        return orgsToReturn;
    }

    public List<String> getAllDescentsUserNamesInclusive(long officeUnitId) {
        Set<Long> orgs = Office_unitsRepository.getInstance().getDescentsOfficeUnitId(officeUnitId)
                .stream()
                .map(mapByOfficeUnitId::get)
                .filter(Objects::nonNull)
                .flatMap(Collection::stream)
                .map(e -> e.id)
                .collect(Collectors.toSet());


        List<OfficeUnitOrganograms> officeUnitOrganogramList = getByOfficeUnitId(officeUnitId);
        if (officeUnitOrganogramList != null) {
            officeUnitOrganogramList.forEach(officeUnitOrganogram->orgs.add(officeUnitOrganogram.id));
        }

        return UserRepository.getUserDTOByOrganogramID(orgs)
                .stream()
                .filter(Objects::nonNull)
                .map(e->e.userName)
                .distinct()
                .collect(Collectors.toList());

        /*List<String> userNames = new ArrayList<String>();
        for (long org : orgs) {
            UserDTO userDTO = UserRepository.getUserDTOByOrganogramID(org);
            if (userDTO != null && !userNames.contains(userDTO.userName)) {
                userNames.add(userDTO.userName);
            }
        }

        return userNames;*/
    }

    public Set<OfficeUnitOrganograms> getAllDescentsOrganograms(long officeUnitId) {
        return Office_unitsRepository.getInstance().getDescentsOfficeUnitId(officeUnitId)
                .stream()
                .map(mapByOfficeUnitId::get)
                .filter(Objects::nonNull)
                .flatMap(Collection::stream)
                .collect(Collectors.toSet());
    }

    public OfficeUnitOrganograms getImmediateSuperior(long id) {
        OfficeUnitOrganograms organograms = getById(id);
        return organograms == null ? null :
                (organograms.superior_designation_id == 0 ? organograms : getById(organograms.superior_designation_id));
    }


    public OfficeUnitOrganograms getImmediateSuperiorWithActiveEmpStatusAndConsiderSubstitute(long id) {
        return getOfficeUnitHeadWithActiveEmpStatus(id, true);
    }

    public OfficeUnitOrganograms getImmediateSuperiorToOrganogramIdWithActiveEmpStatusAndConsiderSubstitute(long organogramId) {
        OfficeUnitOrganograms officeUnitOrganograms = getById(organogramId);
        OfficeUnitOrganograms headOfficeUnitOrganograms = getOfficeUnitHeadWithActiveEmpStatus(officeUnitOrganograms.office_unit_id, true);
        if (headOfficeUnitOrganograms != null && headOfficeUnitOrganograms.id == organogramId) {
            Office_unitsDTO parentOfficeUnitsDTO = Office_unitsRepository.getInstance().getParentOffice(officeUnitOrganograms.office_unit_id);
            return getOfficeUnitHeadWithActiveEmpStatus(parentOfficeUnitsDTO.iD, true);
        }
        return headOfficeUnitOrganograms;
    }

    public OfficeUnitOrganograms getOfficeUnitHeadWithActiveEmpStatusAndNotConsiderSubstitute(long officeUnitId) {
        return getOfficeUnitHeadWithActiveEmpStatus(officeUnitId, false);
    }

    public OfficeUnitOrganograms getOfficeUnitHeadWithActiveEmpStatusAndConsiderSubstitute(long officeUnitId) {
        return getOfficeUnitHeadWithActiveEmpStatus(officeUnitId, true);
    }

    public OfficeUnitOrganograms getOfficeUnitHeadWithActiveEmpStatus(long officeUnitId,
                                                                      boolean considerSubstitute) {
        return getOfficeUnitHeadWithActiveEmpStatus(officeUnitId, true, considerSubstitute);
    }

    public OfficeUnitOrganograms getOfficeUnitHeadWithActiveEmpStatus(long officeUnitId,
                                                                      boolean considerVacancy,
                                                                      boolean considerSubstitute) {

        List<OfficeUnitOrganograms> list = getOfficeUnitHeadList(officeUnitId, considerVacancy, considerSubstitute);
        return list.isEmpty() ? null : list.get(0);
    }

    public List<OfficeUnitOrganograms> getOfficeUnitHeadListWithActiveEmpStatusAndConsiderSubstitute(long officeUnitId) {
        return getOfficeUnitHeadList(officeUnitId, true, true);
    }

    public List<OfficeUnitOrganograms> getOfficeUnitHeadListWithActiveEmpStatusAndNotConsiderSubstitute(long officeUnitId) {
        return getOfficeUnitHeadList(officeUnitId, true, false);
    }

    public List<OfficeUnitOrganograms> getOfficeUnitHeadList(long officeUnitId, boolean considerVacancy, boolean considerSubstitute) {
        Office_unitsDTO officeUnitsDTO = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(officeUnitId);
        if (officeUnitsDTO == null) {
            logger.debug("Office Unit DTO is not found for officeUnitId : " + officeUnitId);
            return new ArrayList<>();
        }
        if (officeUnitsDTO.isAbstractOffice) {
            logger.debug(String.format("Requested office is abstract office, id : %d, office name : %s", officeUnitId, officeUnitsDTO.unitNameEng));
            return new ArrayList<>();
        }
        Office_unitsDTO parentOffice_unitsDTO = Office_unitsRepository.getInstance().getParentOffice(officeUnitId);
        if (parentOffice_unitsDTO == null) {
            logger.debug("Data inconsistent for parent office of office_unit_id : " + officeUnitId);
            return new ArrayList<>();
        }
        List<OfficeUnitOrganograms> parentOfficeOrganogramList;
        parentOfficeOrganogramList = mapByOfficeUnitId.get(parentOffice_unitsDTO.iD);
        if (parentOfficeOrganogramList == null || parentOfficeOrganogramList.size() == 0) {
            logger.debug("Organogram is empty for office unit id : " + parentOffice_unitsDTO.iD);
            return new ArrayList<>();
        }
        Set<Long> parentOrgIdList = parentOfficeOrganogramList.stream()
                .map(e -> e.id)
                .collect(Collectors.toSet());

        if (parentOrgIdList.size() == 0) {
            logger.debug("No parent organogram id is found in office unit id : " + parentOffice_unitsDTO.iD);
            return new ArrayList<>();
        }

        List<OfficeUnitOrganograms> list = mapByOfficeUnitId.get(officeUnitId);

        if (list == null || list.size() == 0) return new ArrayList<>();

        return list.stream()
                .filter(e -> parentOrgIdList.contains(e.superior_designation_id) || e.superior_designation_id == 0) // SPEAKER IS THE ROOT EMPLOYEE OF THE ORGANOGRAM TREE
                .collect(Collectors.groupingBy(
                        e -> {
                            String orgTree = e.orgTree.substring(0, e.orgTree.length() - 1); // IGNORE LAST CHAR
                            return orgTree.chars().filter(ch -> ch == '|').count();
                        }))
                .entrySet()
                .stream()
                .min(Map.Entry.comparingByKey())
                .map(e -> e.getValue()
                        .stream()
                        .filter(e1 -> predicateForOfficeUnitOrganogram(considerVacancy, considerSubstitute, e1))
                        .map(e1 -> mapOfficeUnitOrganograms(considerVacancy, e1))
                        .collect(Collectors.toList()))
                .orElse(new ArrayList<>());
    }

    private OfficeUnitOrganograms mapOfficeUnitOrganograms(boolean considerVacancy, OfficeUnitOrganograms e1) {
        if (!considerVacancy || !e1.isVacant) {
            return e1;
        }
        return mapById.get(e1.substituteOrganogramId);
    }

    private boolean predicateForOfficeUnitOrganogram(boolean considerVacancy, boolean considerSubstitute, OfficeUnitOrganograms e1) {
        if (!considerVacancy || !e1.isVacant) {
            return true;
        }
        if (considerSubstitute && e1.substituteOrganogramId > 0) {
            OfficeUnitOrganograms substituteOrganogram = mapById.get(e1.substituteOrganogramId);
            return substituteOrganogram != null && !substituteOrganogram.isVacant;
        }
        return false;
    }

    public EmployeeOfficeDTO getOfficeUnitHeadOfficesDTO(long officeUnitId) {
        OfficeUnitOrganograms organograms = getOfficeUnitHeadWithActiveEmpStatus(officeUnitId, true);
        return organograms == null ? null : EmployeeOfficeRepository.getInstance().getByOfficeUnitOrganogramId(organograms.id);
    }

    public List<OfficeUnitOrganograms> getListOfHeadOrganogram(long officeUnitId) {
        List<OfficeUnitOrganograms> organogramsByOfficeUnitId = mapByOfficeUnitId.get(officeUnitId);
        if (organogramsByOfficeUnitId == null || organogramsByOfficeUnitId.size() == 0) return new ArrayList<>();
        if (organogramsByOfficeUnitId.size() == 1) {
            if (organogramsByOfficeUnitId.get(0).status && !organogramsByOfficeUnitId.get(0).isDeleted) {
                return organogramsByOfficeUnitId;
            }
            return new ArrayList<>();
        }
        return organogramsByOfficeUnitId
                .parallelStream()
                .filter(e -> e.orgTree.endsWith("|") && e.status && !e.isDeleted)
                .collect(Collectors.toList());
    }

    public OfficeUnitOrganograms getCommiteeHead(long officeUnitId) {
        List<OfficeUnitOrganograms> organogramsByOfficeUnitId = mapByOfficeUnitId.get(officeUnitId);
        if (organogramsByOfficeUnitId == null || organogramsByOfficeUnitId.size() == 0) return null;

        List<OfficeUnitOrganograms> filtered = organogramsByOfficeUnitId
                .parallelStream()
                .filter(e -> e.designation_bng.equals(SessionConstants.COMMITTEE_SECRETARY) && e.status && !e.isDeleted)
                .collect(Collectors.toList());
        if (filtered.size() == 0) {
            return null;
        }
        return filtered.get(0);
    }


    public List<OfficeUnitOrganograms> getListOfFirstLayerHeadOrganogram(long officeUnitId) {
        List<OfficeUnitOrganograms> headList = getListOfHeadOrganogram(officeUnitId);
        if (headList.size() <= 1) {
            return headList;
        }

        return headList.stream()
                .collect(Collectors.groupingBy(e -> Arrays.asList(e.orgTree.split(Pattern.quote("|"))).size()))
                .entrySet()
                .stream()
                .min(Comparator.comparingInt(Map.Entry::getKey))
                .map(Map.Entry::getValue)
                .orElse(new ArrayList<>());
    }

    public List<OfficeUnitOrganograms> getImmediateInferiorOfficeUnitHeadWithActiveEmpStatusByUnitHead(Long officeUnitHeadOrganogram) {
        OfficeUnitOrganograms officeUnitOrganograms = getById(officeUnitHeadOrganogram);
        if (officeUnitOrganograms == null) {
            logger.error("officeUnitOrganograms is null for officeUnitHeadOrganogram : " + officeUnitHeadOrganogram);
            return new ArrayList<>();
        }
        List<Office_unitsDTO> childDTOs = Office_unitsRepository.getInstance().getChildList(officeUnitOrganograms.office_unit_id);
        return childDTOs.stream()
                .map(e -> getOfficeUnitHeadWithActiveEmpStatusAndNotConsiderSubstitute(e.iD))
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    public String getDesignationNameById(long iD, String language) {
        OfficeUnitOrganograms organogram = getById(iD);
        return organogram == null ? "" : ("English".equalsIgnoreCase(language) ? organogram.designation_eng : organogram.designation_bng);
    }

    public Office_unitsDTO getOfficeType(long organogramId) {
        OfficeUnitOrganograms organogramDTO = getById(organogramId);
        if (organogramDTO == null) return null;
        return Office_unitsRepository.getInstance().getOfficeType(organogramDTO.office_unit_id);
    }

    public Organization getOrganizationByOrganogramId(long organogramId) {
        OfficeUnitOrganograms organograms = getById(organogramId);
        if (organograms == null) {
            return Organization.OTHER;
        }
        return Office_unitsRepository.getInstance().getOrganizationByOfficeUnitId(organograms.office_unit_id);
    }

    public int getOrderValueById(long id) {
        OfficeUnitOrganograms organogram = getById(id);
        return organogram == null ? Integer.MAX_VALUE : organogram.orderValue;
    }

    public void updatingCacheByIdList(List<Long> idList) {
        List<OfficeUnitOrganograms> list = office_unit_organogramsDAO.getDTOs(idList);
        updateCache(list);
    }

    public List<OfficeUnitOrganograms> getByDesignationId(long designationId) {
        return allOfficeUnitOrganograms.parallelStream()
                .filter(e -> e.designationsId == designationId)
                .collect(Collectors.toList());
    }
}