package office_unit_organograms;

/*
 * @author Md. Erfan Hossain
 * @created 23/03/2021 - 12:08 PM
 * @project parliament
 */

import common.ConnectionAndStatementUtil;

import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class OrgTreeColumnUpdate {
    private static final String getOrgTree = "select org_tree from office_unit_organograms where id = %d";
    private static final String getBySuperiorDesignationId = "SELECT id FROM office_unit_organograms WHERE superior_designation_id = %d";
    private static final String updateOrgTreeQuery = "update office_unit_organograms set org_tree = '%s'  where superior_designation_id = %d";
    private static final String updateOrgTreeQueryById = "update office_unit_organograms set org_tree = '%s'  where id = %d";
    private static final String rootUpdateQuery = "UPDATE office_unit_organograms SET org_tree = '0|129333|' WHERE superior_designation_id = 0";
    public static void main(String[] args) {

        ConnectionAndStatementUtil.getWriteStatement(st->{
            try {
                st.executeUpdate(rootUpdateQuery);
            } catch (SQLException exception) {
                exception.printStackTrace();
            }
        });

        Queue<Integer> parentList = new LinkedList<>();
        parentList.add(129333);
        int updateCount = 0;
        while(!parentList.isEmpty()){
            int parent = parentList.poll();
            List<Long> childList = getBySuperiorDesignationId(parent);
            if(childList!=null && childList.size()>0){
                String parentOrgTree = getOrgTreeById(parent);
                for(long organogramId : childList){
                    int id = Long.valueOf(organogramId).intValue();
                    updateOrgTree(parentOrgTree+organogramId+"|",id);
                    parentList.add(id);
                    updateCount++;
                }
            }else{
                String parentOrgTree = getOrgTreeById(parent);
                if(parentOrgTree!=null){
                    parentOrgTree = parentOrgTree.concat("$");
                    parentOrgTree = parentOrgTree.replace("|$","$");
                    updateOrgTree(parentOrgTree,parent);
                    updateCount++;
                }
            }
            System.out.println("updateCount : "+updateCount+"  parentList Size : "+parentList.size());
        }
    }

    private static void updateOrgTree(String newOrgTree,int parent){
        String sql = String.format(updateOrgTreeQueryById,newOrgTree,parent);
        ConnectionAndStatementUtil.getWriteStatement(st->{
            try {
                st.executeUpdate(sql);
                System.out.println("sql : "+sql);
            } catch (SQLException exception) {
                exception.printStackTrace();
            }
        });
    }

    private static String getOrgTreeById(int id){
        String sql = String.format(getOrgTree,id);
        return ConnectionAndStatementUtil.getT(sql,rs->{
            try {
                return rs.getString("org_tree");
            } catch (SQLException exception) {
                exception.printStackTrace();
                return null;
            }
        });
    }

    private static List<Long> getBySuperiorDesignationId(int id){
        String sql = String.format(getBySuperiorDesignationId,id);
        return ConnectionAndStatementUtil.getListOfT(sql,rs->{
            try {
                return rs.getLong("id");
            } catch (SQLException exception) {
                exception.printStackTrace();
                return null;
            }
        });
    }
}
