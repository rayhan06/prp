package office_unit_organograms;
import java.util.*; 


public class Office_unit_organogramsMAPS 
{

	public HashMap<String, String> java_allfield_type_map = new HashMap<String, String>();
	public HashMap<String, String> java_anyfield_search_map = new HashMap<String, String>();
	public HashMap<String, String> java_DTO_map = new HashMap<String, String>();
	public HashMap<String, String> java_SQL_map = new HashMap<String, String>();
	public HashMap<String, String> java_Text_map = new HashMap<String, String>();
	
	private static Office_unit_organogramsMAPS self = null;
	
	private Office_unit_organogramsMAPS()
	{
		
		java_allfield_type_map.put("role_type".toLowerCase(), "Integer");
		java_allfield_type_map.put("approval_path_type".toLowerCase(), "Integer");

		java_anyfield_search_map.put("role.roleName".toLowerCase(), "String");
		java_anyfield_search_map.put("role.roleName".toLowerCase(), "String");
		java_anyfield_search_map.put("approval_path.name_en".toLowerCase(), "String");
		java_anyfield_search_map.put("approval_path.name_bn".toLowerCase(), "String");

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("roleType".toLowerCase(), "roleType".toLowerCase());
		java_DTO_map.put("approvalPathType".toLowerCase(), "approvalPathType".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("role_type".toLowerCase(), "roleType".toLowerCase());
		java_SQL_map.put("approval_path_type".toLowerCase(), "approvalPathType".toLowerCase());
		java_SQL_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Role".toLowerCase(), "roleType".toLowerCase());
		java_Text_map.put("Approval Path".toLowerCase(), "approvalPathType".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}
	
	public static Office_unit_organogramsMAPS GetInstance()
	{
		if(self == null)
		{
			self = new Office_unit_organogramsMAPS ();
		}
		return self;
	}
	

}