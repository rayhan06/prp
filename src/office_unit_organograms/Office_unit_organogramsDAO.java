package office_unit_organograms;


import common.ConnectionAndStatementUtil;
import org.apache.log4j.Logger;
import repository.RepositoryManager;
import util.NavigationService2;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

@SuppressWarnings({"rawtypes", "unused", "Duplicates"})
public class Office_unit_organogramsDAO extends NavigationService2 {

    private static final Logger logger = Logger.getLogger(Office_unit_organogramsDAO.class);

    private static final String getByIds = " SELECT * FROM office_unit_organograms where ID IN (%s) order by id";
    private static final String getById = "SELECT * FROM office_unit_organograms WHERE ID = ?";
    private static final String getAllTrue = "SELECT * FROM office_unit_organograms WHERE isDeleted =  0 order by lastModificationTime desc";
    private static final String getAllFalse = "SELECT * FROM office_unit_organograms WHERE lastModificationTime >= %d order by lastModificationTime desc";
    private static final String getAllIdByOrgTree = "SELECT id FROM office_unit_organograms WHERE org_tree LIKE '{org_tree|}%' or '{org_tree$}'";
    private static final String getAllIdByForSameOfficeId = "SELECT id FROM office_unit_organograms WHERE org_tree = '{org_tree$}' and office_unit_id = %d";
    private static final String updateByOfficeUnitId = "UPDATE office_unit_organograms SET role_type= %s, lastModificationTime= %d  WHERE office_unit_id = %d";
    private static final String updateByMinistryId = "UPDATE office_unit_organograms SET role_type= %s , lastModificationTime= %d WHERE office_unit_id in" +
            " (select office_units.id from office_units where office_id in (SELECT office_id FROM office_layers WHERE id in"
            + "(SELECT id FROM office_layers where office_ministry_id = %d)))";
    private static final String updateByLayerID = "UPDATE office_unit_organograms SET role_type= %s , lastModificationTime= %d WHERE office_unit_id in" +
            "(select office_units.id from office_units where office_id in (SELECT office_id FROM office_layers WHERE id = %d))";
    private static final String updateByOfficeId = "UPDATE office_unit_organograms SET role_type= %s , lastModificationTime= %d WHERE office_unit_id in" +
            " (select office_units.id from office_units where office_id = %d) ";
    private static final String updateById = "UPDATE office_unit_organograms SET role_type= %s , lastModificationTime= %d  WHERE id  =  %d";
    private static final String updateByapprovalPathByID = "UPDATE office_unit_organograms SET approval_path_type=%d , lastModificationTime=%d WHERE id  = %d";
    private static final String deleteById = "UPDATE office_unit_organograms SET isDeleted=1,lastModificationTime= %d WHERE ID =  %d";
    private static final String getAllDTOsExactLastModificationTime = "SELECT * FROM office_unit_organograms WHERE lastModificationTime = %d";

    public OfficeUnitOrganograms getOffice_unit_organogramsDTOByID(long ID) {
        return ConnectionAndStatementUtil.getT(getById, ps -> {
            try {
                ps.setLong(1, ID);
            } catch (SQLException ex) {
                logger.error(ex);
            }
        }, this::buildDTO);
    }

    private OfficeUnitOrganograms buildDTO(ResultSet rs) {
        try {
            OfficeUnitOrganograms dto = new OfficeUnitOrganograms();
            dto.id = rs.getLong("id");
            dto.office_id = rs.getInt("office_id");
            dto.office_unit_id = rs.getInt("office_unit_id");
            dto.superior_unit_id = rs.getInt("superior_unit_id");
            dto.superior_designation_id = rs.getLong("superior_designation_id");
            dto.ref_origin_unit_org_id = rs.getInt("ref_origin_unit_org_id");
            dto.ref_sup_origin_unit_desig_id = rs.getInt("ref_sup_origin_unit_desig_id");
            dto.ref_sup_origin_unit_id = rs.getInt("ref_sup_origin_unit_id");
            dto.designation_eng = rs.getString("designation_eng");
            dto.designation_bng = rs.getString("designation_bng");
            dto.short_name_eng = rs.getString("short_name_eng");
            dto.short_name_bng = rs.getString("short_name_bng");
            dto.designation_level = rs.getInt("designation_level");
            dto.briefDescriptionEng = rs.getString("brief_description_eng");
            dto.briefDescriptionBng = rs.getString("brief_description_bng");
            dto.designation_description = rs.getString("designation_description");
            dto.status = rs.getBoolean("status");
            dto.is_admin = rs.getBoolean("is_admin");
            dto.role_type = rs.getString("role_type");
            dto.layer1 = rs.getInt("layer_1");
            dto.layer2 = rs.getInt("layer_2");
            dto.isVacant = rs.getBoolean("isVacant");
            dto.assignedEmployeeRecordId = rs.getLong("assignedEmployeeRecordId");
            dto.isDeleted = rs.getBoolean("isDeleted");
            dto.lastModificationTime = rs.getLong("lastModificationTime");
            dto.approval_path_type = rs.getInt("approval_path_type");
            dto.job_grade_type_cat = rs.getInt("job_grade_type_cat");
            dto.orgTree = rs.getString("org_tree");
            dto.orderValue = rs.getInt("order_value");
            dto.designationsId = rs.getLong("designations_id");
            dto.isVirtual = rs.getBoolean("is_virtual");
            dto.substituteOrganogramId = rs.getLong("substitute_organogram_id");
            return dto;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    public void updateOffice_unit_organogramsByUnitID(long unitID, String role) {
        update(updateByOfficeUnitId, role, unitID);
    }

    public void updateOffice_unit_organogramsByMinistryID(long ministryID, String role) {
        update(updateByMinistryId, role, ministryID);
    }

    public void updateOffice_unit_organogramsByLayerID(long layerID, String role) {
        update(updateByLayerID, role, layerID);
    }

    public void updateOffice_unit_organogramsByOfficeID(long officeID, String role) {
        update(updateByOfficeId, role, officeID);
    }

    public void updateOffice_unit_organogramsByID(long ID, String role) {
        update(updateById, role, ID);
    }

    public void update(String sql, String role, long longValue) {
        long lastModificationTime = System.currentTimeMillis();
        String updateQuery = String.format(sql, role, lastModificationTime, longValue);
        update(updateQuery, lastModificationTime);
    }

    public void updateOffice_unit_organograms_approvalPathByID(long ID, int approval_path_type) {
        long lastModificationTime = System.currentTimeMillis();
        String updateQuery = String.format(updateByapprovalPathByID, approval_path_type, lastModificationTime, ID);
        update(updateQuery, lastModificationTime);
    }

    public void deleteOffice_unit_organogramsByID(long ID) {
        long lastModificationTime = System.currentTimeMillis();
        String updateQuery = String.format(deleteById, lastModificationTime, ID);
        update(updateQuery, lastModificationTime);
    }

    private void update(String sql, long lastModificationTime) {
        ConnectionAndStatementUtil.getWriteConnectionAndStatement(model -> {
            Connection connection = model.getConnection();
            logger.debug(sql);
            try {
                model.getStatement().executeUpdate(sql);
                recordUpdateTime(connection, lastModificationTime);
            } catch (SQLException ex) {
                logger.error(ex);
            }
        });
    }

    public List<OfficeUnitOrganograms> getDTOs(List<Long> recordIDs) {
        return ConnectionAndStatementUtil.getDTOListByNumbers(getByIds, recordIDs, this::buildDTO);
    }


    public List<OfficeUnitOrganograms> getAllOffice_unit_organograms(boolean isFirstReload) {
        if (isFirstReload) {
            return ConnectionAndStatementUtil.getListOfT(getAllTrue, this::buildDTO);
        } else {
            String sql = String.format(getAllFalse, RepositoryManager.lastModifyTime);
            return ConnectionAndStatementUtil.getListOfT(sql, this::buildDTO);
        }
    }

    public List<OfficeUnitOrganograms> getAllDTOsExactLastModificationTime(long time) {
        return ConnectionAndStatementUtil.getListOfT(String.format(getAllDTOsExactLastModificationTime, time),
                this::buildDTO);
    }

    public List<OfficeUnitOrganograms> getDTOs(Hashtable p_searchCriteria, int limit, int offset) {
        String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS);
        return ConnectionAndStatementUtil.getListOfT(sql, this::buildDTO);
    }

    public Collection getIDs() {
        String sql = getSqlWithSearchCriteria(null, -1, -1, GETIDS);
        return ConnectionAndStatementUtil.getListOfT(sql, rs -> {
            try {
                return rs.getString("ID");
            } catch (SQLException ex) {
                logger.error(ex);
                return null;
            }
        });
    }

    public String getSqlWithSearchCriteria(Hashtable p_searchCriteria, int limit, int offset, int category) {
        String sql = "SELECT ";
        if (category == GETIDS) {
            sql += "distinct office_unit_organograms.ID as ID ";
        } else if (category == GETCOUNT) {
            sql += " count(office_unit_organograms.ID) as countID ";
        } else if (category == GETDTOS) {
            sql += "  office_unit_organograms.* ";
        }
        sql += "FROM office_unit_organograms ";

        StringBuilder AnyfieldSql = new StringBuilder();
        StringBuilder AllFieldSql = new StringBuilder();

        if (p_searchCriteria != null) {


            Enumeration names = p_searchCriteria.keys();
            String str, value;

            AnyfieldSql = new StringBuilder("(");

            if (p_searchCriteria.get("AnyField") != null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase("")) {
                int i = 0;
                for (Map.Entry<String, String> stringStringEntry : Office_unit_organogramsMAPS.GetInstance().java_anyfield_search_map.entrySet()) {
                    if (i > 0) {
                        AnyfieldSql.append(" OR  ");
                    }
                    AnyfieldSql.append(((Map.Entry) stringStringEntry).getKey()).append(" like '%").append(p_searchCriteria.get("AnyField").toString()).append("%'");
                    i++;
                }
            }
            AnyfieldSql.append(")");
            System.out.println("AnyfieldSql = " + AnyfieldSql);

            AllFieldSql = new StringBuilder("(");
            int i = 0;
            while (names.hasMoreElements()) {
                str = (String) names.nextElement();
                value = (String) p_searchCriteria.get(str);
                System.out.println(str + ": " + value);
                if (Office_unit_organogramsMAPS.GetInstance().java_allfield_type_map.get(str.toLowerCase()) != null && !Office_unit_organogramsMAPS.GetInstance().java_allfield_type_map.get(str.toLowerCase()).equalsIgnoreCase("")
                        && !str.equalsIgnoreCase("AnyField")
                        && value != null && !value.equalsIgnoreCase("")) {
                    if (p_searchCriteria.get(str).equals("any")) {
                        continue;
                    }

                    if (i > 0) {
                        AllFieldSql.append(" AND  ");
                    }
                    if (Office_unit_organogramsMAPS.GetInstance().java_allfield_type_map.get(str.toLowerCase()).equals("String")) {
                        AllFieldSql.append("office_unit_organograms.").append(str.toLowerCase()).append(" like '%").append(p_searchCriteria.get(str)).append("%'");
                    } else {
                        AllFieldSql.append("office_unit_organograms.").append(str.toLowerCase()).append(" = '").append(p_searchCriteria.get(str)).append("'");
                    }
                    i++;
                }
            }

            AllFieldSql.append(")");
        }
        boolean anyFieldSqlFlag = !AnyfieldSql.toString().equals("()") && !AnyfieldSql.toString().equals("");
        if (anyFieldSqlFlag) {
            sql += " join role on office_unit_organograms.role_type = role.ID ";
            sql += " join approval_path on office_unit_organograms.approval_path_type = approval_path.ID ";

        }
        sql += " WHERE ";
        sql += " office_unit_organograms.isDeleted = false ";

        if (anyFieldSqlFlag) {
            sql += " AND " + AnyfieldSql;

        }
        if (!AllFieldSql.toString().equals("()") && !AllFieldSql.toString().equals("")) {
            sql += " AND " + AllFieldSql;
        }


        sql += " order by office_unit_organograms.lastModificationTime desc ";

        printSql(sql);

        if (limit >= 0) {
            sql += " limit " + limit;
        }
        if (offset >= 0) {
            sql += " offset " + offset;
        }
        return sql;
    }

    public List<Long> getAllIdByOrgTree(String orgTree) {
        String sql = getAllIdByOrgTree.replace("{org_tree|}", orgTree);
        String dollarEnd = orgTree.substring(0, orgTree.length() - 1) + "$";
        sql = sql.replace("{org_tree|}", dollarEnd);
        return ConnectionAndStatementUtil.getListOfT(sql, rs -> {
            try {
                return rs.getLong("id");
            } catch (SQLException ex) {
                ex.printStackTrace();
                logger.error(ex);
                return null;
            }
        });
    }

    public List<Long> getAllIdForSameOfficeId(String orgTree, long officeUnitId) {
        String dollarEnd = orgTree.substring(0, orgTree.length() - 1) + "$";
        String sql = getAllIdByForSameOfficeId.replace("{org_tree$}", dollarEnd);
        sql = String.format(sql, officeUnitId);
        return ConnectionAndStatementUtil.getListOfT(sql, rs -> {
            try {
                return rs.getLong("id");
            } catch (SQLException ex) {
                ex.printStackTrace();
                logger.error(ex);
                return null;
            }
        });
    }
}