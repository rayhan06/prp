package office_unit_organograms;

import util.CommonDTO;

public class Committee_post_mapDTO extends CommonDTO{
	
	public long committeeId = -1;
	public long postId = -1;
	public long replacementPostId = -1;
	public long modifiedBy = -1;
}
