package office_unit_organograms;

import pb.OptionDTO;

import java.util.Objects;

public class SameDesignationGroup {
    public final String organogramKey;
    public final long organogramId;
    public final String nameEn;
    public final String nameBn;

    public static String getDesignationKey(String designationNameEn) {
        return designationNameEn.toLowerCase().trim();
    }

    public SameDesignationGroup(long organogramId) {
        this.organogramId = organogramId;
        OfficeUnitOrganograms organogramDTO = OfficeUnitOrganogramsRepository.getInstance().getById(organogramId);
        this.nameEn = organogramDTO.designation_eng.trim();
        this.nameBn = organogramDTO.designation_bng;
        this.organogramKey = SameDesignationGroup.getDesignationKey(organogramDTO.designation_eng);
    }

    public SameDesignationGroup(OfficeUnitOrganograms organogramDTO) {
        this.organogramId = organogramDTO.id;
        this.nameEn = organogramDTO.designation_eng.trim();
        this.nameBn = organogramDTO.designation_bng;
        this.organogramKey = SameDesignationGroup.getDesignationKey(organogramDTO.designation_eng);
    }

    public OptionDTO getOptionDTO() {
        return new OptionDTO(nameEn, nameBn, String.valueOf(organogramId));
    }

    public OptionDTO getOptionDTOofOrganogramKey() {
        return new OptionDTO(nameEn, nameBn, organogramKey);
    }

    public OptionDTO getOptionDTOofOrganogramKeyAll() {
        return new OptionDTO(nameEn, nameBn, organogramId + ":" + organogramKey);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SameDesignationGroup that = (SameDesignationGroup) o;
        return Objects.equals(organogramKey, that.organogramKey);
    }

    @Override
    public int hashCode() {
        return Objects.hash(organogramKey);
    }
}
