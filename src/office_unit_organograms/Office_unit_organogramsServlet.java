package office_unit_organograms;

import com.google.gson.Gson;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@WebServlet("/office_unit_organograms_Servlet")
public class Office_unit_organogramsServlet extends HttpServlet {
    private static final Logger logger = Logger.getLogger(Office_unit_organogramsServlet.class);

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String actionType = request.getParameter("actionType");
        logger.debug("actionType : "+actionType);
        String data = "";
        if(actionType!=null){
            if ("office_unit_organograms".equals(actionType)) {
                String officeUnitId = request.getParameter("office_unit_id");
                logger.debug("office_unit_id :"+officeUnitId);
                List<OfficeUnitOrganograms> modelList = OfficeUnitOrganogramsRepository.getInstance().getByOfficeUnitId(Long.parseLong(officeUnitId));
                List<Map<String, Object>> result = modelList.stream()
                        .map(e -> {
                            Map<String, Object> modelMap = new HashMap<>();
                            modelMap.put("id", e.id);
                            modelMap.put("designation_bng", e.designation_bng);
                            modelMap.put("designation_eng", e.designation_eng);
                            return modelMap;
                        })
                        .collect(Collectors.toList());
                data = new Gson().toJson(result);
            }
        }
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        out.print(data);
        out.flush();
    }
}