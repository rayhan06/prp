package nothi_history;


import java.text.SimpleDateFormat;



import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import org.apache.log4j.Logger;

import permission.MenuConstants;

import user.UserDTO;
import util.*;


import javax.servlet.http.*;
import java.util.*;

import language.LC;
import language.LM;

import common.BaseServlet;
import com.google.gson.Gson;
import pb.*;
import nothi.*;

import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;




/**
 * Servlet implementation class Nothi_historyServlet
 */
@WebServlet("/Nothi_historyServlet")
@MultipartConfig
public class Nothi_historyServlet extends BaseServlet 
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Nothi_historyServlet.class);

    @Override
    public String getTableName() {
        return Nothi_historyDAO.getInstance().getTableName();
    }

    @Override
    public String getServletName() {
        return "Nothi_historyServlet";
    }

    @Override
    public Nothi_historyDAO getCommonDAOService() {
        return Nothi_historyDAO.getInstance();
    }
    
    @Override
    public int[] getAddPageMenuConstants() {
        return new int[] {MenuConstants.NOTHI_HISTORY_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[] {MenuConstants.NOTHI_HISTORY_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[] {MenuConstants.NOTHI_HISTORY_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Nothi_historyServlet.class;
    }
    private final Gson gson = new Gson();
 	
    NothiDTO nothiDTO = null;
	@Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception 
	{
		
		request.setAttribute("failureMessage", "");
		System.out.println("%%%% addNothi_history");
		String language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
		Nothi_historyDTO nothi_historyDTO;
		
		SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
		boolean isLangEng = language.equalsIgnoreCase("english");
					
		if(addFlag)
		{
			nothi_historyDTO = new Nothi_historyDTO();
		}
		else
		{
			nothi_historyDTO = Nothi_historyDAO.getInstance().getDTOFromID(Long.parseLong(request.getParameter("iD")));
		}
		
		String Value = "";

		Value = request.getParameter("nothiType");

		
		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("nothiType = " + Value);
		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			nothi_historyDTO.nothiType = Long.parseLong(Value);
			nothiDTO = NothiDAO.getInstance().getDTOByID(nothi_historyDTO.nothiType);
			if(nothiDTO == null)
			{
				throw new Exception(isLangEng?"Invalid nothi number":"নথি নম্বররটি অগ্রহণযোগ্য");
			}
			nothi_historyDTO.set(nothiDTO);
			
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}


		Value = request.getParameter("toOfficeId");

		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("toOfficeId = " + Value);
		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			nothi_historyDTO.toOfficeId = Long.parseLong(Value);
			if(nothi_historyDTO.toOfficeId == -1)
			{
				throw new Exception(isLangEng?"Invalid Office":"অফিস ইনপুট দিন");
			}
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}


		if(addFlag)
		{
			nothi_historyDTO.insertedByUserId = userDTO.ID;	
			nothi_historyDTO.insertedByErId = userDTO.employee_record_id;
			nothi_historyDTO.insertedByOrganogramId = userDTO.organogramID;					
			nothi_historyDTO.insertionDate = TimeConverter.getToday();
			
		}			


		nothi_historyDTO.lastModifierUser = userDTO.userName;


		Value = request.getParameter("transferDate");

		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("transferDate = " + Value);
		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			try 
			{
				Date d = f.parse(Value);
				nothi_historyDTO.transferDate = d.getTime();
			}
			catch (Exception e) 
			{
				e.printStackTrace();
				throw new Exception(LM.getText(LC.NOTHI_HISTORY_ADD_TRANSFERDATE, language) + " " + ErrorMessage.getInvalidMessage(language));
			}
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}
		System.out.println("Done adding  addNothi_history dto = " + nothi_historyDTO);

		if(addFlag == true)
		{
			nothiDTO.assignedOfficeId = nothi_historyDTO.toOfficeId;
			Utils.handleTransaction(() -> {
				Nothi_historyDAO.getInstance().add(nothi_historyDTO);
				NothiDAO.getInstance().update(nothiDTO);
			});
		}
		
		
		

		return nothi_historyDTO;

	}
}

