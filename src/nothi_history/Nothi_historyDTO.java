package nothi_history;
import nothi.NothiDTO;
import util.*; 


public class Nothi_historyDTO extends CommonDTO
{

	public long nothiType = -1;
	public long nothiConfigurationId = -1;
    public String nothiNumber = "";
    public String description = "";
	public long fromOfficeId = -1;
	public long toOfficeId = -1;
	public long insertedByErId = -1;
	public long insertedByUserId = -1;
	public long insertedByOrganogramId = -1;
	public long insertionDate = -1;
    public String lastModifierUser = "";
	public long transferDate = System.currentTimeMillis();
	
	
	public void set(NothiDTO nothiDTO)
	{
		nothiType = nothiDTO.iD;
		nothiConfigurationId= nothiDTO.nothiConfigurationType;
		nothiNumber = nothiDTO.nothiNumber;
		description = nothiDTO.description;
		fromOfficeId = nothiDTO.assignedOfficeId;
	}
	
	public void setNumber(NothiDTO nothiDTO)
	{
		nothiNumber = nothiDTO.nothiNumber;
		description = nothiDTO.description;
	}
	
	public void setInit(NothiDTO nothiDTO)
	{
		nothiType = nothiDTO.iD;
		nothiConfigurationId= nothiDTO.nothiConfigurationType;
		nothiNumber = nothiDTO.nothiNumber;
		description = nothiDTO.description;
		toOfficeId = nothiDTO.assignedOfficeId;
		insertedByUserId = nothiDTO.insertedByUserId;
		insertedByOrganogramId = nothiDTO.insertedByOrganogramId;
		insertionDate = nothiDTO.insertionDate;
		transferDate = nothiDTO.nothiDate;
	}
	
	
    @Override
	public String toString() {
            return "$Nothi_historyDTO[" +
            " iD = " + iD +
            " nothiType = " + nothiType +
            " nothiConfigurationId = " + nothiConfigurationId +
            " nothiNumber = " + nothiNumber +
            " description = " + description +
            " searchColumn = " + searchColumn +
            " fromOfficeId = " + fromOfficeId +
            " toOfficeId = " + toOfficeId +
            " insertedByErId = " + insertedByErId +
            " insertedByUserId = " + insertedByUserId +
            " insertedByOrganogramId = " + insertedByOrganogramId +
            " insertionDate = " + insertionDate +
            " lastModifierUser = " + lastModifierUser +
            " transferDate = " + transferDate +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}