package nothi_history;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.sql.SQLException;
import common.CommonDAOService;
import common.ConnectionAndStatementUtil;
import nothi.NothiDTO;

import org.apache.log4j.Logger;
import util.*;
import pb.*;

public class Nothi_historyDAO  implements CommonDAOService<Nothi_historyDTO>
{
	
	Logger logger = Logger.getLogger(getClass());
	
	private static String addQuery = "";
	private static String updateQuery = "";
	private String[] columnNames = null;

	private Nothi_historyDAO()
	{
		columnNames = new String[] 
		{
			"ID",
			"nothi_type",
			"nothi_configuration_id",
			"nothi_number",
			"description",
			"search_column",
			"from_office_id",
			"to_office_id",
			"inserted_by_er_id",
			"inserted_by_user_id",
			"inserted_by_organogram_id",
			"insertion_date",
			"last_modifier_user",
			"transfer_date",
			"isDeleted",
			"lastModificationTime"
		};
		updateQuery = getUpdateQuery(columnNames);
        addQuery = getAddQuery(columnNames);
		
		searchMap.put("nothi_type"," and (nothi_type = ?)");
		
		searchMap.put("description"," and (description like ?)");
		
		searchMap.put("transfer_date_start"," and (transfer_date >= ?)");
		searchMap.put("transfer_date_end"," and (transfer_date <= ?)");
		searchMap.put("AnyField"," and (search_column like ?)");
	}

	private final Map<String,String> searchMap = new HashMap<>();

    private static class LazyLoader
    {
    	static final Nothi_historyDAO INSTANCE = new Nothi_historyDAO();
	}

	public static Nothi_historyDAO getInstance()
	{
    	return LazyLoader.INSTANCE;
	}
	
	public void setSearchColumn(Nothi_historyDTO nothi_historyDTO)
	{
		nothi_historyDTO.searchColumn = "";
		nothi_historyDTO.searchColumn += CommonDAO.getName("English", "nothi", nothi_historyDTO.nothiType) + " " + CommonDAO.getName("Bangla", "nothi", nothi_historyDTO.nothiType) + " ";
		nothi_historyDTO.searchColumn += nothi_historyDTO.nothiNumber + " ";
		nothi_historyDTO.searchColumn += nothi_historyDTO.description + " ";
	}
	
	@Override
	public void set(PreparedStatement ps, Nothi_historyDTO nothi_historyDTO, boolean isInsert) throws SQLException
	{
		int index = 0;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(nothi_historyDTO);
		if(isInsert)
		{
			ps.setObject(++index,nothi_historyDTO.iD);
		}
		ps.setObject(++index,nothi_historyDTO.nothiType);
		ps.setObject(++index,nothi_historyDTO.nothiConfigurationId);
		ps.setObject(++index,nothi_historyDTO.nothiNumber);
		ps.setObject(++index,nothi_historyDTO.description);
		ps.setObject(++index,nothi_historyDTO.searchColumn);
		ps.setObject(++index,nothi_historyDTO.fromOfficeId);
		ps.setObject(++index,nothi_historyDTO.toOfficeId);
		ps.setObject(++index,nothi_historyDTO.insertedByErId);
		ps.setObject(++index,nothi_historyDTO.insertedByUserId);
		ps.setObject(++index,nothi_historyDTO.insertedByOrganogramId);
		ps.setObject(++index,nothi_historyDTO.insertionDate);
		ps.setObject(++index,nothi_historyDTO.lastModifierUser);
		ps.setObject(++index,nothi_historyDTO.transferDate);
		if(isInsert)
		{
			ps.setObject(++index,nothi_historyDTO.isDeleted);
		}
		ps.setObject(++index, lastModificationTime);
		if(!isInsert)
		{
			ps.setObject(++index,nothi_historyDTO.iD);
		}
	}
	
	@Override
	public Nothi_historyDTO buildObjectFromResultSet(ResultSet rs)
	{
		try
		{
			Nothi_historyDTO nothi_historyDTO = new Nothi_historyDTO();
			int i = 0;
			nothi_historyDTO.iD = rs.getLong(columnNames[i++]);
			nothi_historyDTO.nothiType = rs.getLong(columnNames[i++]);
			nothi_historyDTO.nothiConfigurationId = rs.getLong(columnNames[i++]);
			nothi_historyDTO.nothiNumber = rs.getString(columnNames[i++]);
			nothi_historyDTO.description = rs.getString(columnNames[i++]);
			nothi_historyDTO.searchColumn = rs.getString(columnNames[i++]);
			nothi_historyDTO.fromOfficeId = rs.getLong(columnNames[i++]);
			nothi_historyDTO.toOfficeId = rs.getLong(columnNames[i++]);
			nothi_historyDTO.insertedByErId = rs.getLong(columnNames[i++]);
			nothi_historyDTO.insertedByUserId = rs.getLong(columnNames[i++]);
			nothi_historyDTO.insertedByOrganogramId = rs.getLong(columnNames[i++]);
			nothi_historyDTO.insertionDate = rs.getLong(columnNames[i++]);
			nothi_historyDTO.lastModifierUser = rs.getString(columnNames[i++]);
			nothi_historyDTO.transferDate = rs.getLong(columnNames[i++]);
			nothi_historyDTO.isDeleted = rs.getInt(columnNames[i++]);
			nothi_historyDTO.lastModificationTime = rs.getLong(columnNames[i++]);
			return nothi_historyDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
		
	public Nothi_historyDTO getDTOByID (long id)
	{
		return getDTOFromID(id);
	}

	@Override
	public String getTableName() {
		return "nothi_history";
	}

	@Override
	public Map<String, String> getSearchMap() {
		return searchMap;
	}

	public long add(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Nothi_historyDTO) commonDTO,addQuery,true);
	}

	public long update(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Nothi_historyDTO) commonDTO,updateQuery,false);
	}
	
	@Override
	public String getLastModifierUser(){
		return "last_modifier_user";
    }
	
	public List<Nothi_historyDTO> getAllByNothiId (long nothiId)
    {
		String sql = "SELECT * FROM " + getTableName() + " WHERE isDeleted = 0  " 
				+ " and nothi_type =  " + nothiId + " order by id desc";

		return ConnectionAndStatementUtil.getListOfT(sql,this::buildObjectFromResultSet);	
    }
	
	public List<Nothi_historyDTO> getAllByNothiConfigId (long nothiConfigId)
    {
		String sql = "SELECT * FROM " + getTableName() + " WHERE isDeleted = 0  " 
				+ " and nothi_configuration_id =  " + nothiConfigId + " order by id desc";

		return ConnectionAndStatementUtil.getListOfT(sql,this::buildObjectFromResultSet);	
    }
				
}
	