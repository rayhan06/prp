package nothi_history;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import com.google.gson.Gson;

import repository.Repository;
import repository.RepositoryManager;


public class Nothi_historyRepository implements Repository {
	Nothi_historyDAO nothi_historyDAO = null;
	
	static Logger logger = Logger.getLogger(Nothi_historyRepository.class);
	Map<Long, Nothi_historyDTO>mapOfNothi_historyDTOToiD;
	Gson gson;

  
	private Nothi_historyRepository(){
		nothi_historyDAO = Nothi_historyDAO.getInstance();
		mapOfNothi_historyDTOToiD = new ConcurrentHashMap<>();
		gson = new Gson();
		RepositoryManager.getInstance().addRepository(this);
	}

	private static class LazyLoader{
        final static Nothi_historyRepository INSTANCE = new Nothi_historyRepository();
    }

    public static Nothi_historyRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

	public void reload(boolean reloadAll){
		try {
			List<Nothi_historyDTO> nothi_historyDTOs = nothi_historyDAO.getAllDTOs(reloadAll);
			if (reloadAll) {
				mapOfNothi_historyDTOToiD = new ConcurrentHashMap<>();
																																																                nothi_historyDTOs = nothi_historyDAO.getAllDTOs(reloadAll);
            } else {
                nothi_historyDTOs = nothi_historyDAO.getAllDTOsByLastModificationTimeGreaterThan(System.currentTimeMillis() - 60 * 1000);
            }
            reloadCache(nothi_historyDTOs, reloadAll);
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	@Override
    public void reloadWithExactModificationTime(long time) {
        List<Nothi_historyDTO> nothi_historyDTOs = nothi_historyDAO.getAllDTOsExactLastModificationTime(time);
        reloadCache(nothi_historyDTOs, false);
    }
	
	public Nothi_historyDTO clone(Nothi_historyDTO dto) {
		String raw = gson.toJson(dto);
		return gson.fromJson(raw, Nothi_historyDTO.class);
	}
	
	private void reloadCache(List<Nothi_historyDTO> nothi_historyDTOs, boolean reloadAll) {
        logger.debug("nothi_historyDTOssize = " + nothi_historyDTOs.size());

        for(Nothi_historyDTO nothi_historyDTO : nothi_historyDTOs) 
		{
			Nothi_historyDTO oldNothi_historyDTO = getNothi_historyDTOByiD(nothi_historyDTO.iD);
			if( oldNothi_historyDTO != null ) {
				mapOfNothi_historyDTOToiD.remove(oldNothi_historyDTO.iD);
				
			}
			if(nothi_historyDTO.isDeleted == 0) 
			{
					
				mapOfNothi_historyDTOToiD.put(nothi_historyDTO.iD, nothi_historyDTO);
				
			}
		}
        
    }
	
	
	public List<Nothi_historyDTO> getNothi_historyList() {
		List <Nothi_historyDTO> nothi_historys = new ArrayList<Nothi_historyDTO>(this.mapOfNothi_historyDTOToiD.values());
		return nothi_historys;
	}
	
	public List<Nothi_historyDTO> copyNothi_historyList() {
		List <Nothi_historyDTO> nothi_historys = getNothi_historyList();
		return nothi_historys
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}
	
	
	
	public Nothi_historyDTO getNothi_historyDTOByiD( long iD){
		return mapOfNothi_historyDTOToiD.get(iD);
	}
	
	public Nothi_historyDTO copyNothi_historyDTOByiD( long iD){
		return clone(mapOfNothi_historyDTOToiD.get(iD));
	}

	
	@Override
	public String getTableName() {
		return nothi_historyDAO.getTableName();
	}
}


