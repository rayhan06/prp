package appointment;
import java.util.*; 
import util.*;


public class AppointmentMAPS extends CommonMaps
{	
	public AppointmentMAPS(String tableName)
	{
		

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("visitDate".toLowerCase(), "visitDate".toLowerCase());
		java_DTO_map.put("specialityType".toLowerCase(), "specialityType".toLowerCase());
		java_DTO_map.put("doctorId".toLowerCase(), "doctorId".toLowerCase());
		java_DTO_map.put("shiftCat".toLowerCase(), "shiftCat".toLowerCase());
		java_DTO_map.put("availableTimeSlot".toLowerCase(), "availableTimeSlot".toLowerCase());
		java_DTO_map.put("remarks".toLowerCase(), "remarks".toLowerCase());
		java_DTO_map.put("patientId".toLowerCase(), "patientId".toLowerCase());
		java_DTO_map.put("patientName".toLowerCase(), "patientName".toLowerCase());
		java_DTO_map.put("phoneNumber".toLowerCase(), "phoneNumber".toLowerCase());
		java_DTO_map.put("dateOfBirth".toLowerCase(), "dateOfBirth".toLowerCase());
		java_DTO_map.put("age".toLowerCase(), "age".toLowerCase());
		java_DTO_map.put("genderCat".toLowerCase(), "genderCat".toLowerCase());
		java_DTO_map.put("whoIsThePatientCat".toLowerCase(), "whoIsThePatientCat".toLowerCase());
		java_DTO_map.put("isChargeNeeded".toLowerCase(), "isChargeNeeded".toLowerCase());
		java_DTO_map.put("doctorName".toLowerCase(), "doctorName".toLowerCase());
		java_DTO_map.put("deptName".toLowerCase(), "deptName".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("visit_date".toLowerCase(), "visitDate".toLowerCase());
		java_SQL_map.put("speciality_type".toLowerCase(), "specialityType".toLowerCase());
		java_SQL_map.put("doctor_id".toLowerCase(), "doctorId".toLowerCase());
		java_SQL_map.put("shift_cat".toLowerCase(), "shiftCat".toLowerCase());
		java_SQL_map.put("available_time_slot".toLowerCase(), "availableTimeSlot".toLowerCase());
		java_SQL_map.put("remarks".toLowerCase(), "remarks".toLowerCase());
		java_SQL_map.put("patient_id".toLowerCase(), "patientId".toLowerCase());
		java_SQL_map.put("patient_name".toLowerCase(), "patientName".toLowerCase());
		java_SQL_map.put("phone_number".toLowerCase(), "phoneNumber".toLowerCase());
		java_SQL_map.put("date_of_birth".toLowerCase(), "dateOfBirth".toLowerCase());
		java_SQL_map.put("age".toLowerCase(), "age".toLowerCase());
		java_SQL_map.put("gender_cat".toLowerCase(), "genderCat".toLowerCase());
		java_SQL_map.put("who_is_the_patient_cat".toLowerCase(), "whoIsThePatientCat".toLowerCase());
		java_SQL_map.put("is_charge_needed".toLowerCase(), "isChargeNeeded".toLowerCase());
		java_SQL_map.put("doctor_name".toLowerCase(), "doctorName".toLowerCase());
		java_SQL_map.put("dept_name".toLowerCase(), "deptName".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Visit Date".toLowerCase(), "visitDate".toLowerCase());
		java_Text_map.put("Speciality".toLowerCase(), "specialityType".toLowerCase());
		java_Text_map.put("Doctor Id".toLowerCase(), "doctorId".toLowerCase());
		java_Text_map.put("Shift".toLowerCase(), "shiftCat".toLowerCase());
		java_Text_map.put("Available Time Slot".toLowerCase(), "availableTimeSlot".toLowerCase());
		java_Text_map.put("Remarks".toLowerCase(), "remarks".toLowerCase());
		java_Text_map.put("Patient Id".toLowerCase(), "patientId".toLowerCase());
		java_Text_map.put("Patient Name".toLowerCase(), "patientName".toLowerCase());
		java_Text_map.put("Phone Number".toLowerCase(), "phoneNumber".toLowerCase());
		java_Text_map.put("Date Of Birth".toLowerCase(), "dateOfBirth".toLowerCase());
		java_Text_map.put("Age".toLowerCase(), "age".toLowerCase());
		java_Text_map.put("Gender".toLowerCase(), "genderCat".toLowerCase());
		java_Text_map.put("Who Is The Patient".toLowerCase(), "whoIsThePatientCat".toLowerCase());
		java_Text_map.put("Is Charge Needed".toLowerCase(), "isChargeNeeded".toLowerCase());
		java_Text_map.put("Doctor Name".toLowerCase(), "doctorName".toLowerCase());
		java_Text_map.put("Dept Name".toLowerCase(), "deptName".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}