package appointment;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class AppointmentRepository implements Repository {
	AppointmentDAO appointmentDAO = null;
	
	public void setDAO(AppointmentDAO appointmentDAO)
	{
		this.appointmentDAO = appointmentDAO;
	}
	
	
	static Logger logger = Logger.getLogger(AppointmentRepository.class);
	Map<Long, AppointmentDTO>mapOfAppointmentDTOToiD;
	Map<Long, Set<AppointmentDTO> >mapOfAppointmentDTOTovisitDate;
	Map<Long, Set<AppointmentDTO> >mapOfAppointmentDTOTospecialityType;
	Map<Long, Set<AppointmentDTO> >mapOfAppointmentDTOTodoctorId;
	Map<Integer, Set<AppointmentDTO> >mapOfAppointmentDTOToshiftCat;
	Map<Long, Set<AppointmentDTO> >mapOfAppointmentDTOToavailableTimeSlot;
	Map<String, Set<AppointmentDTO> >mapOfAppointmentDTOToremarks;
	Map<Long, Set<AppointmentDTO> >mapOfAppointmentDTOTopatientId;
	Map<String, Set<AppointmentDTO> >mapOfAppointmentDTOTopatientName;
	Map<String, Set<AppointmentDTO> >mapOfAppointmentDTOTophoneNumber;
	Map<Long, Set<AppointmentDTO> >mapOfAppointmentDTOTodateOfBirth;
	Map<Long, Set<AppointmentDTO> >mapOfAppointmentDTOToage;
	Map<Integer, Set<AppointmentDTO> >mapOfAppointmentDTOTogenderCat;
	Map<Integer, Set<AppointmentDTO> >mapOfAppointmentDTOTowhoIsThePatientCat;
	Map<Boolean, Set<AppointmentDTO> >mapOfAppointmentDTOToisChargeNeeded;
	Map<Long, Set<AppointmentDTO> >mapOfAppointmentDTOTolastModificationTime;


	static AppointmentRepository instance = null;  
	private AppointmentRepository(){
		mapOfAppointmentDTOToiD = new ConcurrentHashMap<>();
		mapOfAppointmentDTOTovisitDate = new ConcurrentHashMap<>();
		mapOfAppointmentDTOTospecialityType = new ConcurrentHashMap<>();
		mapOfAppointmentDTOTodoctorId = new ConcurrentHashMap<>();
		mapOfAppointmentDTOToshiftCat = new ConcurrentHashMap<>();
		mapOfAppointmentDTOToavailableTimeSlot = new ConcurrentHashMap<>();
		mapOfAppointmentDTOToremarks = new ConcurrentHashMap<>();
		mapOfAppointmentDTOTopatientId = new ConcurrentHashMap<>();
		mapOfAppointmentDTOTopatientName = new ConcurrentHashMap<>();
		mapOfAppointmentDTOTophoneNumber = new ConcurrentHashMap<>();
		mapOfAppointmentDTOTodateOfBirth = new ConcurrentHashMap<>();
		mapOfAppointmentDTOToage = new ConcurrentHashMap<>();
		mapOfAppointmentDTOTogenderCat = new ConcurrentHashMap<>();
		mapOfAppointmentDTOTowhoIsThePatientCat = new ConcurrentHashMap<>();
		mapOfAppointmentDTOToisChargeNeeded = new ConcurrentHashMap<>();
		mapOfAppointmentDTOTolastModificationTime = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static AppointmentRepository getInstance(){
		if (instance == null){
			instance = new AppointmentRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(appointmentDAO == null)
		{
			return;
		}
		try {
			List<AppointmentDTO> appointmentDTOs = appointmentDAO.getAllAppointment(reloadAll);
			for(AppointmentDTO appointmentDTO : appointmentDTOs) {
				AppointmentDTO oldAppointmentDTO = getAppointmentDTOByID(appointmentDTO.iD);
				if( oldAppointmentDTO != null ) {
					mapOfAppointmentDTOToiD.remove(oldAppointmentDTO.iD);
				
					if(mapOfAppointmentDTOTovisitDate.containsKey(oldAppointmentDTO.visitDate)) {
						mapOfAppointmentDTOTovisitDate.get(oldAppointmentDTO.visitDate).remove(oldAppointmentDTO);
					}
					if(mapOfAppointmentDTOTovisitDate.get(oldAppointmentDTO.visitDate).isEmpty()) {
						mapOfAppointmentDTOTovisitDate.remove(oldAppointmentDTO.visitDate);
					}
					
					if(mapOfAppointmentDTOTospecialityType.containsKey(oldAppointmentDTO.specialityType)) {
						mapOfAppointmentDTOTospecialityType.get(oldAppointmentDTO.specialityType).remove(oldAppointmentDTO);
					}
					if(mapOfAppointmentDTOTospecialityType.get(oldAppointmentDTO.specialityType).isEmpty()) {
						mapOfAppointmentDTOTospecialityType.remove(oldAppointmentDTO.specialityType);
					}
					
					if(mapOfAppointmentDTOTodoctorId.containsKey(oldAppointmentDTO.doctorId)) {
						mapOfAppointmentDTOTodoctorId.get(oldAppointmentDTO.doctorId).remove(oldAppointmentDTO);
					}
					if(mapOfAppointmentDTOTodoctorId.get(oldAppointmentDTO.doctorId).isEmpty()) {
						mapOfAppointmentDTOTodoctorId.remove(oldAppointmentDTO.doctorId);
					}
					
					if(mapOfAppointmentDTOToshiftCat.containsKey(oldAppointmentDTO.shiftCat)) {
						mapOfAppointmentDTOToshiftCat.get(oldAppointmentDTO.shiftCat).remove(oldAppointmentDTO);
					}
					if(mapOfAppointmentDTOToshiftCat.get(oldAppointmentDTO.shiftCat).isEmpty()) {
						mapOfAppointmentDTOToshiftCat.remove(oldAppointmentDTO.shiftCat);
					}
					
					if(mapOfAppointmentDTOToavailableTimeSlot.containsKey(oldAppointmentDTO.availableTimeSlot)) {
						mapOfAppointmentDTOToavailableTimeSlot.get(oldAppointmentDTO.availableTimeSlot).remove(oldAppointmentDTO);
					}
					if(mapOfAppointmentDTOToavailableTimeSlot.get(oldAppointmentDTO.availableTimeSlot).isEmpty()) {
						mapOfAppointmentDTOToavailableTimeSlot.remove(oldAppointmentDTO.availableTimeSlot);
					}
					
					if(mapOfAppointmentDTOToremarks.containsKey(oldAppointmentDTO.remarks)) {
						mapOfAppointmentDTOToremarks.get(oldAppointmentDTO.remarks).remove(oldAppointmentDTO);
					}
					if(mapOfAppointmentDTOToremarks.get(oldAppointmentDTO.remarks).isEmpty()) {
						mapOfAppointmentDTOToremarks.remove(oldAppointmentDTO.remarks);
					}
					
					if(mapOfAppointmentDTOTopatientId.containsKey(oldAppointmentDTO.patientId)) {
						mapOfAppointmentDTOTopatientId.get(oldAppointmentDTO.patientId).remove(oldAppointmentDTO);
					}
					if(mapOfAppointmentDTOTopatientId.get(oldAppointmentDTO.patientId).isEmpty()) {
						mapOfAppointmentDTOTopatientId.remove(oldAppointmentDTO.patientId);
					}
					
					if(mapOfAppointmentDTOTopatientName.containsKey(oldAppointmentDTO.patientName)) {
						mapOfAppointmentDTOTopatientName.get(oldAppointmentDTO.patientName).remove(oldAppointmentDTO);
					}
					if(mapOfAppointmentDTOTopatientName.get(oldAppointmentDTO.patientName).isEmpty()) {
						mapOfAppointmentDTOTopatientName.remove(oldAppointmentDTO.patientName);
					}
					
					if(mapOfAppointmentDTOTophoneNumber.containsKey(oldAppointmentDTO.phoneNumber)) {
						mapOfAppointmentDTOTophoneNumber.get(oldAppointmentDTO.phoneNumber).remove(oldAppointmentDTO);
					}
					if(mapOfAppointmentDTOTophoneNumber.get(oldAppointmentDTO.phoneNumber).isEmpty()) {
						mapOfAppointmentDTOTophoneNumber.remove(oldAppointmentDTO.phoneNumber);
					}
					
					if(mapOfAppointmentDTOTodateOfBirth.containsKey(oldAppointmentDTO.dateOfBirth)) {
						mapOfAppointmentDTOTodateOfBirth.get(oldAppointmentDTO.dateOfBirth).remove(oldAppointmentDTO);
					}
					if(mapOfAppointmentDTOTodateOfBirth.get(oldAppointmentDTO.dateOfBirth).isEmpty()) {
						mapOfAppointmentDTOTodateOfBirth.remove(oldAppointmentDTO.dateOfBirth);
					}
					
					if(mapOfAppointmentDTOToage.containsKey(oldAppointmentDTO.age)) {
						mapOfAppointmentDTOToage.get(oldAppointmentDTO.age).remove(oldAppointmentDTO);
					}
					if(mapOfAppointmentDTOToage.get(oldAppointmentDTO.age).isEmpty()) {
						mapOfAppointmentDTOToage.remove(oldAppointmentDTO.age);
					}
					
					if(mapOfAppointmentDTOTogenderCat.containsKey(oldAppointmentDTO.genderCat)) {
						mapOfAppointmentDTOTogenderCat.get(oldAppointmentDTO.genderCat).remove(oldAppointmentDTO);
					}
					if(mapOfAppointmentDTOTogenderCat.get(oldAppointmentDTO.genderCat).isEmpty()) {
						mapOfAppointmentDTOTogenderCat.remove(oldAppointmentDTO.genderCat);
					}
					
					if(mapOfAppointmentDTOTowhoIsThePatientCat.containsKey(oldAppointmentDTO.whoIsThePatientCat)) {
						mapOfAppointmentDTOTowhoIsThePatientCat.get(oldAppointmentDTO.whoIsThePatientCat).remove(oldAppointmentDTO);
					}
					if(mapOfAppointmentDTOTowhoIsThePatientCat.get(oldAppointmentDTO.whoIsThePatientCat).isEmpty()) {
						mapOfAppointmentDTOTowhoIsThePatientCat.remove(oldAppointmentDTO.whoIsThePatientCat);
					}
					
					if(mapOfAppointmentDTOToisChargeNeeded.containsKey(oldAppointmentDTO.isChargeNeeded)) {
						mapOfAppointmentDTOToisChargeNeeded.get(oldAppointmentDTO.isChargeNeeded).remove(oldAppointmentDTO);
					}
					if(mapOfAppointmentDTOToisChargeNeeded.get(oldAppointmentDTO.isChargeNeeded).isEmpty()) {
						mapOfAppointmentDTOToisChargeNeeded.remove(oldAppointmentDTO.isChargeNeeded);
					}
					
					if(mapOfAppointmentDTOTolastModificationTime.containsKey(oldAppointmentDTO.lastModificationTime)) {
						mapOfAppointmentDTOTolastModificationTime.get(oldAppointmentDTO.lastModificationTime).remove(oldAppointmentDTO);
					}
					if(mapOfAppointmentDTOTolastModificationTime.get(oldAppointmentDTO.lastModificationTime).isEmpty()) {
						mapOfAppointmentDTOTolastModificationTime.remove(oldAppointmentDTO.lastModificationTime);
					}
					
					
				}
				if(appointmentDTO.isDeleted == 0) 
				{
					
					mapOfAppointmentDTOToiD.put(appointmentDTO.iD, appointmentDTO);
				
					if( ! mapOfAppointmentDTOTovisitDate.containsKey(appointmentDTO.visitDate)) {
						mapOfAppointmentDTOTovisitDate.put(appointmentDTO.visitDate, new HashSet<>());
					}
					mapOfAppointmentDTOTovisitDate.get(appointmentDTO.visitDate).add(appointmentDTO);
					
					if( ! mapOfAppointmentDTOTospecialityType.containsKey(appointmentDTO.specialityType)) {
						mapOfAppointmentDTOTospecialityType.put(appointmentDTO.specialityType, new HashSet<>());
					}
					mapOfAppointmentDTOTospecialityType.get(appointmentDTO.specialityType).add(appointmentDTO);
					
					if( ! mapOfAppointmentDTOTodoctorId.containsKey(appointmentDTO.doctorId)) {
						mapOfAppointmentDTOTodoctorId.put(appointmentDTO.doctorId, new HashSet<>());
					}
					mapOfAppointmentDTOTodoctorId.get(appointmentDTO.doctorId).add(appointmentDTO);
					
					if( ! mapOfAppointmentDTOToshiftCat.containsKey(appointmentDTO.shiftCat)) {
						mapOfAppointmentDTOToshiftCat.put(appointmentDTO.shiftCat, new HashSet<>());
					}
					mapOfAppointmentDTOToshiftCat.get(appointmentDTO.shiftCat).add(appointmentDTO);
					
					if( ! mapOfAppointmentDTOToavailableTimeSlot.containsKey(appointmentDTO.availableTimeSlot)) {
						mapOfAppointmentDTOToavailableTimeSlot.put(appointmentDTO.availableTimeSlot, new HashSet<>());
					}
					mapOfAppointmentDTOToavailableTimeSlot.get(appointmentDTO.availableTimeSlot).add(appointmentDTO);
					
					if( ! mapOfAppointmentDTOToremarks.containsKey(appointmentDTO.remarks)) {
						mapOfAppointmentDTOToremarks.put(appointmentDTO.remarks, new HashSet<>());
					}
					mapOfAppointmentDTOToremarks.get(appointmentDTO.remarks).add(appointmentDTO);
					
					if( ! mapOfAppointmentDTOTopatientId.containsKey(appointmentDTO.patientId)) {
						mapOfAppointmentDTOTopatientId.put(appointmentDTO.patientId, new HashSet<>());
					}
					mapOfAppointmentDTOTopatientId.get(appointmentDTO.patientId).add(appointmentDTO);
					
					if( ! mapOfAppointmentDTOTopatientName.containsKey(appointmentDTO.patientName)) {
						mapOfAppointmentDTOTopatientName.put(appointmentDTO.patientName, new HashSet<>());
					}
					mapOfAppointmentDTOTopatientName.get(appointmentDTO.patientName).add(appointmentDTO);
					
					if( ! mapOfAppointmentDTOTophoneNumber.containsKey(appointmentDTO.phoneNumber)) {
						mapOfAppointmentDTOTophoneNumber.put(appointmentDTO.phoneNumber, new HashSet<>());
					}
					mapOfAppointmentDTOTophoneNumber.get(appointmentDTO.phoneNumber).add(appointmentDTO);
					
					if( ! mapOfAppointmentDTOTodateOfBirth.containsKey(appointmentDTO.dateOfBirth)) {
						mapOfAppointmentDTOTodateOfBirth.put(appointmentDTO.dateOfBirth, new HashSet<>());
					}
					mapOfAppointmentDTOTodateOfBirth.get(appointmentDTO.dateOfBirth).add(appointmentDTO);
					
					if( ! mapOfAppointmentDTOToage.containsKey(appointmentDTO.age)) {
						mapOfAppointmentDTOToage.put(appointmentDTO.age, new HashSet<>());
					}
					mapOfAppointmentDTOToage.get(appointmentDTO.age).add(appointmentDTO);
					
					if( ! mapOfAppointmentDTOTogenderCat.containsKey(appointmentDTO.genderCat)) {
						mapOfAppointmentDTOTogenderCat.put(appointmentDTO.genderCat, new HashSet<>());
					}
					mapOfAppointmentDTOTogenderCat.get(appointmentDTO.genderCat).add(appointmentDTO);
					
					if( ! mapOfAppointmentDTOTowhoIsThePatientCat.containsKey(appointmentDTO.whoIsThePatientCat)) {
						mapOfAppointmentDTOTowhoIsThePatientCat.put(appointmentDTO.whoIsThePatientCat, new HashSet<>());
					}
					mapOfAppointmentDTOTowhoIsThePatientCat.get(appointmentDTO.whoIsThePatientCat).add(appointmentDTO);
					
					if( ! mapOfAppointmentDTOToisChargeNeeded.containsKey(appointmentDTO.isChargeNeeded)) {
						mapOfAppointmentDTOToisChargeNeeded.put(appointmentDTO.isChargeNeeded, new HashSet<>());
					}
					mapOfAppointmentDTOToisChargeNeeded.get(appointmentDTO.isChargeNeeded).add(appointmentDTO);
					
					if( ! mapOfAppointmentDTOTolastModificationTime.containsKey(appointmentDTO.lastModificationTime)) {
						mapOfAppointmentDTOTolastModificationTime.put(appointmentDTO.lastModificationTime, new HashSet<>());
					}
					mapOfAppointmentDTOTolastModificationTime.get(appointmentDTO.lastModificationTime).add(appointmentDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<AppointmentDTO> getAppointmentList() {
		List <AppointmentDTO> appointments = new ArrayList<AppointmentDTO>(this.mapOfAppointmentDTOToiD.values());
		return appointments;
	}
	
	
	public AppointmentDTO getAppointmentDTOByID( long ID){
		return mapOfAppointmentDTOToiD.get(ID);
	}
	
	
	public List<AppointmentDTO> getAppointmentDTOByvisit_date(long visit_date) {
		return new ArrayList<>( mapOfAppointmentDTOTovisitDate.getOrDefault(visit_date,new HashSet<>()));
	}
	
	
	public List<AppointmentDTO> getAppointmentDTOByspeciality_type(long speciality_type) {
		return new ArrayList<>( mapOfAppointmentDTOTospecialityType.getOrDefault(speciality_type,new HashSet<>()));
	}
	
	
	public List<AppointmentDTO> getAppointmentDTOBydoctor_id(long doctor_id) {
		return new ArrayList<>( mapOfAppointmentDTOTodoctorId.getOrDefault(doctor_id,new HashSet<>()));
	}
	
	
	public List<AppointmentDTO> getAppointmentDTOByshift_cat(int shift_cat) {
		return new ArrayList<>( mapOfAppointmentDTOToshiftCat.getOrDefault(shift_cat,new HashSet<>()));
	}
	
	
	public List<AppointmentDTO> getAppointmentDTOByavailable_time_slot(long available_time_slot) {
		return new ArrayList<>( mapOfAppointmentDTOToavailableTimeSlot.getOrDefault(available_time_slot,new HashSet<>()));
	}
	
	
	public List<AppointmentDTO> getAppointmentDTOByremarks(String remarks) {
		return new ArrayList<>( mapOfAppointmentDTOToremarks.getOrDefault(remarks,new HashSet<>()));
	}
	
	
	public List<AppointmentDTO> getAppointmentDTOBypatient_id(long patient_id) {
		return new ArrayList<>( mapOfAppointmentDTOTopatientId.getOrDefault(patient_id,new HashSet<>()));
	}
	
	
	public List<AppointmentDTO> getAppointmentDTOBypatient_name(String patient_name) {
		return new ArrayList<>( mapOfAppointmentDTOTopatientName.getOrDefault(patient_name,new HashSet<>()));
	}
	
	
	public List<AppointmentDTO> getAppointmentDTOByphone_number(String phone_number) {
		return new ArrayList<>( mapOfAppointmentDTOTophoneNumber.getOrDefault(phone_number,new HashSet<>()));
	}
	
	
	public List<AppointmentDTO> getAppointmentDTOBydate_of_birth(long date_of_birth) {
		return new ArrayList<>( mapOfAppointmentDTOTodateOfBirth.getOrDefault(date_of_birth,new HashSet<>()));
	}
	
	
	public List<AppointmentDTO> getAppointmentDTOByage(long age) {
		return new ArrayList<>( mapOfAppointmentDTOToage.getOrDefault(age,new HashSet<>()));
	}
	
	
	public List<AppointmentDTO> getAppointmentDTOBygender_cat(int gender_cat) {
		return new ArrayList<>( mapOfAppointmentDTOTogenderCat.getOrDefault(gender_cat,new HashSet<>()));
	}
	
	
	public List<AppointmentDTO> getAppointmentDTOBywho_is_the_patient_cat(int who_is_the_patient_cat) {
		return new ArrayList<>( mapOfAppointmentDTOTowhoIsThePatientCat.getOrDefault(who_is_the_patient_cat,new HashSet<>()));
	}
	
	
	public List<AppointmentDTO> getAppointmentDTOByis_charge_needed(boolean is_charge_needed) {
		return new ArrayList<>( mapOfAppointmentDTOToisChargeNeeded.getOrDefault(is_charge_needed,new HashSet<>()));
	}
	
	
	public List<AppointmentDTO> getAppointmentDTOBylastModificationTime(long lastModificationTime) {
		return new ArrayList<>( mapOfAppointmentDTOTolastModificationTime.getOrDefault(lastModificationTime,new HashSet<>()));
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "appointment";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


