package appointment;

import java.io.IOException;
import java.io.*;
import java.text.DateFormat;

import java.text.ParseException;
import java.text.SimpleDateFormat;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import org.apache.log4j.Logger;

import login.LoginDTO;
import other_office.Other_officeRepository;
import permission.MenuConstants;

import prescription_details.Prescription_detailsDAO;
import prescription_details.Prescription_detailsDTO;
import role.PermissionRepository;

import sessionmanager.SessionConstants;
import user.UserDAO;
import user.UserDTO;
import user.UserRepository;
import util.CommonRequestHandler;
import util.RecordNavigationManager4;
import util.TimeConverter;
import util.TimeFormat;
import workflow.WorkflowController;
import java.util.*;
import javax.servlet.http.*;
import doctor_time_slot.Doctor_time_slotDAO;
import doctor_time_slot.Doctor_time_slotDTO;

import employee_offices.EmployeeOfficeDTO;
import employee_offices.EmployeeOfficeRepository;
import employee_records.Employee_recordsDTO;
import employee_records.Employee_recordsRepository;
import employee_records.EmploymentStatusEnum;
import family.FamilyDTO;
import family.FamilyMemberDAO;
import family.FamilyMemberDTO;
import language.LC;
import language.LM;
import com.google.gson.Gson;

import common.NameDTO;
import pb.*;
import pb_notifications.Pb_notificationsDAO;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;




/**
 * Servlet implementation class AppointmentServlet
 */
@WebServlet("/AppointmentServlet")
@MultipartConfig
public class AppointmentServlet extends HttpServlet 
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(AppointmentServlet.class);
    private static final UserDAO userDAO = new UserDAO();

    String tableName = "appointment";

	AppointmentDAO appointmentDAO;
	Prescription_detailsDAO prescription_detailsDAO = new Prescription_detailsDAO();
	CommonRequestHandler commonRequestHandler;
    private final Gson gson = new Gson();
    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AppointmentServlet() 
	{
        super();
    	try
    	{
			appointmentDAO = new AppointmentDAO(tableName);
			commonRequestHandler = new CommonRequestHandler(appointmentDAO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    public static int SELF = 1;
    public static int NONE = -1;
    public static int PRESELECTED = 2;
    
    private void getFormattedAddPage(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO, LoginDTO loginDTO, int addDoctor) throws Exception
    {
    	AppointmentDTO appointmentDTO = new AppointmentDTO();
    	if(addDoctor == SELF)
    	{
    		appointmentDTO.doctorId = userDTO.organogramID;
    		System.out.println("Self doctor , appointmentDTO.doctorId = " + appointmentDTO.doctorId);
    	}
    	else if (addDoctor == NONE)
    	{
    		appointmentDTO.doctorId = NONE;
    	}
    	else if (addDoctor == PRESELECTED)
    	{
    		appointmentDTO.doctorId = Long.parseLong(request.getParameter("referredTo"));
    	}
		
    	System.out.println("appointmentDTO.doctorId = " + appointmentDTO.doctorId);

		appointmentDTO.specialityType = WorkflowController.getUserNameAndDetailsFromOrganogramId(appointmentDTO.doctorId, "english").dept;
		appointmentDTO.employeeUserName = Utils.getDigitEnglishFromBangla(request.getParameter("userName"));
		
		System.out.println("appointmentDTO.employeeUserName = " + appointmentDTO.employeeUserName);
		UserDTO employeeUserDTO = UserRepository.getUserDTOByUserName(appointmentDTO.employeeUserName);
		if(employeeUserDTO != null)
		{
			appointmentDTO.patientId = employeeUserDTO.organogramID;
		}
		else
		{
			System.out.println("No user found for username = " + appointmentDTO.employeeUserName);
		}
		

		appointmentDTO.whoIsThePatientCat = Integer.parseInt(request.getParameter("whoIsThePatientCat"));
		
		int type = appointmentDTO.whoIsThePatientCat;
		FamilyMemberDTO familyMemberDTO = null;
		FamilyMemberDAO familyMemberDAO = new FamilyMemberDAO();

		if(type  >= 0) //relative
		{
			familyMemberDTO = familyMemberDAO.getEFEDTOById(type);
		}
		else if(type  == FamilyDTO.OWN)
		{
			familyMemberDTO = WorkflowController.getFamilyMemberDTOFromOrganogramId(appointmentDTO.patientId);
		}
		
		if(familyMemberDTO == null)
		{
			System.out.println("New dto");
			familyMemberDTO = new FamilyMemberDTO();
		}
		
		System.out.println(" fmdto = " + familyMemberDTO);
		
		String Language = LM.getText(LC.APPOINTMENT_EDIT_LANGUAGE, loginDTO);
		if(Language.equalsIgnoreCase("english"))
		{
			appointmentDTO.patientName = familyMemberDTO.nameEn;
		}
		else
		{
			appointmentDTO.patientName = familyMemberDTO.nameBn;
		}
		
		appointmentDTO.phoneNumber = familyMemberDTO.mobile;
		appointmentDTO.genderCat = familyMemberDTO.genderCat;
		
		
		request.setAttribute("ID", appointmentDTO.iD);
		request.setAttribute("appointmentDTO",appointmentDTO);
		request.setAttribute("appointmentDAO",appointmentDAO);
		String URL = "appointment/appointmentEdit.jsp?actionType=getAddPage";
		RequestDispatcher rd = request.getRequestDispatcher(URL);
		rd.forward(request, response);
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		System.out.println("In doget request = " + request);
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		boolean isPermanentTable = true;
		String language = LM.getLanguage(loginDTO);
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		try
		{
			String actionType = request.getParameter("actionType");
			if(actionType.equals("getAddPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.APPOINTMENT_ADD))
				{
					commonRequestHandler.getAddPage(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getEditPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.APPOINTMENT_UPDATE))
				{
					getAppointment(request, response);					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}						
			}
			else if(actionType.equals("getLast"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.APPOINTMENT_UPDATE))
				{
					long erId = WorkflowController.getEmployeeRecordsIdFromUserName(request.getParameter("userName"));
					String Language = request.getParameter("Language");
					List<AppointmentDTO> appointmentDTOs = appointmentDAO.getLastN(erId, 3);
					
					String rows = "";
					if(appointmentDTOs != null)
					{
						SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
						SimpleDateFormat hourFormat = new SimpleDateFormat("hh:mm aa");
	
						for(AppointmentDTO appointmentDTO: appointmentDTOs)
						{
							logger.debug("appointmentDTO.visitDate = " + appointmentDTO.visitDate);
							rows += "<tr>";
							
							rows += "<td>";
							rows += Utils.getDigits(f.format(new Date(appointmentDTO.visitDate)), Language);
							rows += "</td>";
							
							rows += "<td>";
							rows += Utils.getDigits(hourFormat.format(new Date(appointmentDTO.availableTimeSlot)), Language);
							rows += "</td>";
							
							rows += "<td>";
							rows += WorkflowController.getNameFromEmployeeRecordID(appointmentDTO.drEmployeeRecordId, Language);
							rows += "</td>";
							
							rows += "</tr>";
						}
					}
					
					response.getWriter().write(rows);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}						
			}
			else if(actionType.equals("cancel"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.APPOINTMENT_UPDATE))
				{
					cancel(request, response,userDTO);					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}						
			}
			else if(actionType.equals("reschedule"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.APPOINTMENT_UPDATE))
				{
					reschedule(request, response,userDTO);					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}						
			}
			else if(actionType.equals("getFormattedAddPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.APPOINTMENT_ADD))
				{
					getFormattedAddPage(request, response, userDTO, loginDTO, SELF);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
									
			}
			else if(actionType.equals("getFormattedAddPageForNurse"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.APPOINTMENT_ADD))
				{
					getFormattedAddPage(request, response, userDTO, loginDTO, NONE);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
									
			}
			else if(actionType.equals("getPrescriptionlessAppointmentsForDoctor"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.APPOINTMENT_SEARCH))
				{
					String userName = Utils.getDigitEnglishFromBangla(request.getParameter("userName"));
					if(!Utils.isValidUserName(userName))
					{
						return;
					}
					int whoIsThePatientCat = Integer.parseInt(request.getParameter("whoIsThePatientCat"));
					String filter = " ((appointment.employee_record_id = " + WorkflowController.getEmployeeRecordsIdFromUserName(userName) 
							+ " and appointment.who_is_the_patient_cat =" + whoIsThePatientCat
							+ " and appointment.iscancelled = 0 and dr_user_name = '" + userDTO.userName + "'"
							+ " and appointment.id not in (select appointment_id from prescription_details where prescription_details.isDeleted = 0))";
					System.out.println("filter = " + filter);
					
					searchAppointment(request, response, isPermanentTable, filter);
					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getPrescriptionFullAppointmentsForDoctor"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.APPOINTMENT_SEARCH))
				{
					String userName = Utils.getDigitEnglishFromBangla(request.getParameter("userName"));
					if(!Utils.isValidUserName(userName))
					{
						return;
					}
					int whoIsThePatientCat = Integer.parseInt(request.getParameter("whoIsThePatientCat"));
					String filter = " (appointment.employee_record_id = " + WorkflowController.getEmployeeRecordsIdFromUserName(userName) 
							+ " and appointment.who_is_the_patient_cat =" + whoIsThePatientCat
							+ " and appointment.iscancelled = 0 and dr_user_name = '" + userDTO.userName + "'"
							+ " and appointment.id in (select appointment_id from prescription_details where prescription_details.isDeleted = 0))";
					System.out.println("filter = " + filter);
					
					searchAppointment(request, response, isPermanentTable, filter);
					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getReceptionsAppointments"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.APPOINTMENT_SEARCH))
				{
					long startDate = Long.parseLong(request.getParameter("startDate"));
					long endDate = Long.parseLong(request.getParameter("endDate"));
					long erId = Long.parseLong(request.getParameter("erId"));

					System.out.println("getReceptionsAppointments startDate = " + startDate + " endDate = " 
					+ endDate + " inserted_by_erid = " + erId);
					String filter = " inserted_by_erid = " + erId + " and insertion_date >= " + startDate + " and insertion_date <= " + endDate;
					System.out.println("filter = " + filter);					
					searchAppointment(request, response, isPermanentTable, filter);
					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getReceptionsAppointmentsToday"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.APPOINTMENT_SEARCH))
				{
					String filter = " inserted_by_user_name = '" + userDTO.userName + "' and insertion_date >= " + TimeConverter.getToday();
					System.out.println("filter = " + filter);					
					searchAppointment(request, response, isPermanentTable, filter);
					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getReceptionsAppointmentsThisMonth"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.APPOINTMENT_SEARCH))
				{
					String filter = " inserted_by_user_name = '" + userDTO.userName + "' and insertion_date >= " + TimeConverter.get1stDayOfMonth();
					System.out.println("filter = " + filter);					
					searchAppointment(request, response, isPermanentTable, filter);
					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getFormattedAddPagePreselected"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.APPOINTMENT_ADD))
				{
					getFormattedAddPage(request, response, userDTO, loginDTO, PRESELECTED);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
									
			}
			else if(actionType.equals("getAvailableShifts"))
			{
				System.out.println("################ getAvailableShifts called");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.APPOINTMENT_ADD) 
						|| PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.APPOINTMENT_UPDATE))
				{
					SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
					Date d = f.parse(request.getParameter("date"));
                    long lDate = d.getTime();
                    long doctorId = Long.parseLong(request.getParameter("doctor"));
                    Doctor_time_slotDAO doctor_time_slotDAO = new Doctor_time_slotDAO();
                    
                    int week = TimeFormat.getNthWeek(lDate);
                    System.out.println("Found week = " + week);
                    int shiftCat = -1;
                    if(request.getParameter("shiftCat") != null)
                    {
                    	shiftCat= Integer.parseInt(request.getParameter("shiftCat"));
                    }
                    Calendar cal = Calendar.getInstance();
                    cal.setTimeInMillis(lDate);
                    int day = cal.get(Calendar.DAY_OF_WEEK) % 7;
                    long now = System.currentTimeMillis();
                    System.out.println("get day = " + day);
                    List<Doctor_time_slotDTO> doctor_time_slotDTOs = doctor_time_slotDAO.getSlotDTOsByDrDay(doctorId, day, week);
                    String options = "";
                    if(doctor_time_slotDTOs.isEmpty())
                    {
                    	if(language.equalsIgnoreCase("english"))
                    	{
                    		options = "<option value=''> No shift is available </option>";
                    	}
                    	else
                    	{
                    		options = "<option value=''> শিফট  নেই </option>";
                    		
                    	}
                    }
                    else
                    {
                    	options = "<option value=''> " + LM.getText(LC.HM_SELECT, loginDTO) + "</option>";
                    	int count = 0;
                    	for(Doctor_time_slotDTO doctor_time_slotDTO: doctor_time_slotDTOs)
                    	{
                    		String [] splittedEndTIme = doctor_time_slotDTO.endTime.split(":");
                    		int endHour = Integer.parseInt(splittedEndTIme[0]);
                    		int endMinute = Integer.parseInt(splittedEndTIme[1]);
                    		cal.set(Calendar.HOUR_OF_DAY, endHour);
                    		cal.set(Calendar.MINUTE, endMinute);
                    		if(cal.getTimeInMillis() < now)
                    		{
                    			continue;
                    		}
                    		options += "<option value = '" + doctor_time_slotDTO.shiftCat + "' " + (shiftCat == doctor_time_slotDTO.shiftCat? "selected":"") + " >";
                    		
                    		options += Utils.getDigits(TimeFormat.getInAmPmFormat(doctor_time_slotDTO.startTime), language) + "-" 
                    		+ Utils.getDigits(TimeFormat.getInAmPmFormat(doctor_time_slotDTO.endTime), language);
                    		options += "</options>";
                    		count ++;
                    	}
                    	if(count == 0)
                    	{
                    		if(language.equalsIgnoreCase("english"))
                        	{
                        		options = "<option value=''> No shift is available </option>";
                        	}
                        	else
                        	{
                        		options = "<option value=''> শিফট  নেই </option>";
                        		
                        	}
                    	}
                    }
                    response.getWriter().write(options);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}	
			}
			else if(actionType.equals("getSlot"))
			{
				System.out.println("################ getSlot called");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.APPOINTMENT_ADD))
				{
					SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
					Date d = f.parse(request.getParameter("date"));
                    long lDate = d.getTime();
                    long doctorId = Long.parseLong(request.getParameter("doctor"));
                    long shift= -1;
                    if(!request.getParameter("shift").equalsIgnoreCase(""))
                    {
                    	shift = Long.parseLong(request.getParameter("shift"));
                    }
                    
                    Doctor_time_slotDAO doctor_time_slotDAO = new Doctor_time_slotDAO();
                    Calendar cal = Calendar.getInstance();
                    cal.setTimeInMillis(lDate);
                    int day = cal.get(Calendar.DAY_OF_WEEK) % 7;
                    System.out.println("get day = " + day);
                    Doctor_time_slotDTO doctor_time_slotDTO = doctor_time_slotDAO.getSlotDTOByDoctorShiftDay(doctorId, shift, day);

                    
                    if(doctor_time_slotDTO != null)
                    {
                    	
                    	int startHour = Integer.parseInt(doctor_time_slotDTO.startTime.split(":")[0]);
                    	int startMinute = Integer.parseInt(doctor_time_slotDTO.startTime.split(":")[1]);
                    	
                    	int endHour = Integer.parseInt(doctor_time_slotDTO.endTime.split(":")[0]);
                    	int endMinute = Integer.parseInt(doctor_time_slotDTO.endTime.split(":")[1]);
                    	
                    	long slotSize= doctor_time_slotDTO.durationPerPatient;
                    	
                        Calendar startCalendar = Calendar.getInstance();
                        startCalendar.setTime(d);
                        startCalendar.add(Calendar.HOUR_OF_DAY, startHour);
                        startCalendar.add(Calendar.MINUTE, startMinute);
                        
                        Calendar endCalendar = Calendar.getInstance();
                        endCalendar.setTime(d);
                        endCalendar.add(Calendar.HOUR_OF_DAY, endHour);
                        endCalendar.add(Calendar.MINUTE, endMinute);
                        
                        
                        String options = "<option value = ''>" + LM.getText(LC.HM_SELECT, loginDTO) + "</option>";

                        DateFormat  hourMinuteFormat = new SimpleDateFormat("hh:mm a");
                        int count = 0;
                        List<AppointmentDTO> slottedAppointments = appointmentDAO.getAppointmentsInSlot(doctorId, lDate, startCalendar.getTimeInMillis(), endCalendar.getTimeInMillis());
                        while(startCalendar.getTimeInMillis() < endCalendar.getTimeInMillis())
                        {
                        	long slot = startCalendar.getTimeInMillis();
                        	long now = System.currentTimeMillis();
                        	if(slot > now && !appointmentDAO.hasDTOInDateAndSlot(doctorId, lDate, slot, slottedAppointments))
                        	{
                        		String text = hourMinuteFormat.format(new Date(startCalendar.getTimeInMillis()));
                        		text = Utils.getDigits(text, language);

                        		options += "<option value = '" + slot + "'>" 
                        				+ text
                        				+ "</option>";
                        		count ++;
                        	}
                        	startCalendar.add(Calendar.MINUTE, (int) slotSize);
                        }
                        if(count == 0)
                        {
                        	if(language.equalsIgnoreCase("english"))
                        	{
                        		options = "<option value=''> No slot is available </option>";
                        	}
                        	else
                        	{
                        		options = "<option value=''> স্লট নেই </option>";
                        	}
                        }
                        
                        response.getWriter().write(options);
                    }
                    else
                    {
                    	String options = "";

                    	if(language.equalsIgnoreCase("english"))
                    	{
                    		options = "<option value=''> No slot is available </option>";
                    	}
                    	else
                    	{
                    		options = "<option value=''> স্লট নেই </option>";
                    	}

                    	response.getWriter().write(options);
                    }

				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}						
			}
			else if(actionType.equals("getURL"))
			{
				String URL = request.getParameter("URL");
				System.out.println("URL = " + URL);
				response.sendRedirect(URL);			
			}
			else if(actionType.equals("getMyAppointments"))
			{
				System.out.println("search requested");
				//if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.APPOINTMENT_SEARCH))
				{
					if(isPermanentTable)
					{
						String filter = "patient_id = " + userDTO.organogramID;
						searchAppointment(request, response, isPermanentTable, filter);
						
					}					
					else
					{
						//searchAppointment(request, response, tempTableName, isPermanentTable);
					}
				}
			}
			else if(actionType.equals("search"))
			{
				System.out.println("search requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.APPOINTMENT_SEARCH))
				{
					if(isPermanentTable)
					{
						String filter = request.getParameter("filter");
						System.out.println("filter = " + filter);
						if(filter!=null)
						{
							if(filter.equalsIgnoreCase("getUpcomingAppointments"))
							{
								filter = getUpcomingFilter(userDTO);
							}
							else
							{
								filter = getUpcomingFilterIfDoctor(userDTO);
							}
							searchAppointment(request, response, isPermanentTable, filter);
						}
						else
						{
							filter = getUpcomingFilterIfDoctor(userDTO);
							searchAppointment(request, response, isPermanentTable, filter);
						}
					}					
					else
					{
						//searchAppointment(request, response, tempTableName, isPermanentTable);
					}
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("view"))
			{
				System.out.println("view requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.APPOINTMENT_SEARCH))
				{
					commonRequestHandler.view(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else
			{
				request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}
	
	private String getUpcomingFilter(UserDTO userDTO)
	{
		String filter = "";
		
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		filter = "(dr_user_name = '" + userDTO.userName + "' or employee_record_id = " + WorkflowController.getEmployeeRecordsIdFromUserName(userDTO.userName) + ")"
				+ " and visit_date >= " + cal.getTimeInMillis() 
		+ " and appointment.id not in (select appointment_id from prescription_details)";
		
		return filter;
	}
	
	private String getUpcomingFilterIfDoctor(UserDTO userDTO)
	{
		String filter = "";
		if(userDTO.roleID == SessionConstants.DOCTOR_ROLE || userDTO.roleID == SessionConstants.PHYSIOTHERAPIST_ROLE)
		{
			filter = getUpcomingFilter(userDTO);
		}
		else if(userDTO.roleID == SessionConstants.ADMIN_ROLE 
				|| userDTO.roleID == SessionConstants.MEDICAL_ADMIN_ROLE 
				|| userDTO.roleID == SessionConstants.NURSE_ROLE
				|| userDTO.roleID == SessionConstants.MEDICAL_RECEPTIONIST_ROLE) 
		{
			filter = ""; //shouldn't be directly used, rather manipulate it.
		}
		else
		{
			filter = " employee_record_id = " + WorkflowController.getEmployeeRecordsIdFromUserName(userDTO.userName);
		}
		return filter;
	}
	
	private String getDefaultFilter(UserDTO userDTO)
	{
		String filter = "";//Def
		if(userDTO.roleID == SessionConstants.DOCTOR_ROLE || userDTO.roleID == SessionConstants.PHYSIOTHERAPIST_ROLE)
		{
			filter = " (dr_user_name = '" + userDTO.userName + "' or  employee_record_id = " + WorkflowController.getEmployeeRecordsIdFromUserName(userDTO.userName) + ")";
		}
		else if(userDTO.roleID == SessionConstants.ADMIN_ROLE 
				|| userDTO.roleID == SessionConstants.MEDICAL_ADMIN_ROLE 
				|| userDTO.roleID == SessionConstants.NURSE_ROLE
				|| userDTO.roleID == SessionConstants.MEDICAL_RECEPTIONIST_ROLE) 
		{
			filter = ""; //shouldn't be directly used, rather manipulate it.
		}
		else
		{
			filter = " employee_record_id = " + WorkflowController.getEmployeeRecordsIdFromUserName(userDTO.userName) ;
		}
		return filter;
	}


	
	

	private void cancel(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO) {
		// TODO Auto-generated method stub
		long id = Long.parseLong(request.getParameter("id"));
		String remarks = Jsoup.clean(request.getParameter("remarks"),Whitelist.simpleText());
		
		AppointmentDTO appointmentDTO = appointmentDAO.getDTOByID(id);
		if(!appointmentDAO.canChangeMyAppointments(userDTO, appointmentDTO))
		{
			return;
		}
		appointmentDTO.cancellerOrganogram = userDTO.organogramID;
		appointmentDTO.cancellerUserName = userDTO.userName;
		appointmentDTO.cancellingRemarks = remarks;
		appointmentDTO.cancellingDate = new Date().getTime();
		appointmentDTO.isCancelled = true;
		
		try {
			appointmentDAO.update(appointmentDTO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	}
	
	private void reschedule(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO) {
		// TODO Auto-generated method stub
		long id = Long.parseLong(request.getParameter("id"));
		AppointmentDTO appointmentDTO = appointmentDAO.getDTOByID(id);
		if(!appointmentDAO.canChangeMyAppointments(userDTO, appointmentDTO))
		{
			return;
		}
		appointmentDTO.shiftCat = Integer.parseInt(request.getParameter("shift"));
		SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
		Date d;
		try {
			d = f.parse(request.getParameter("date"));
			
			Calendar c = Calendar.getInstance();
			c.setTimeInMillis(d.getTime());
			c.set(Calendar.HOUR_OF_DAY, 0);
			c.set(Calendar.MINUTE, 0);
			c.set(Calendar.SECOND, 0);
			c.set(Calendar.MILLISECOND, 0);
			appointmentDTO.visitDate = c.getTimeInMillis();
			if(appointmentDTO.visitDate < TimeConverter.getToday())
			{
				return;
			}
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		appointmentDTO.availableTimeSlot = Long.parseLong(request.getParameter("slot"));
		try {
			if(appointmentDAO.hasDTOInDateAndSlot(appointmentDTO.doctorId, appointmentDTO.visitDate, appointmentDTO.availableTimeSlot))
			{
				return;
			}
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		try {
			appointmentDAO.update(appointmentDTO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		SimpleDateFormat hourFormat = new SimpleDateFormat("hh:mm aa");
		Pb_notificationsDAO pb_notificationsDAO = Pb_notificationsDAO.getInstance();
		String formatted_visitDate = f.format(new Date(appointmentDTO.visitDate));
		String rescTime = hourFormat.format(new Date(appointmentDTO.availableTimeSlot));
		
		String englishText = "Your appointment is rescheduled to " 
				+ formatted_visitDate 
				+ " "
				+ rescTime;
		
		String banglaText =  "আপনার পরিবর্তিত অ্যাপয়েন্টমেন্টের সময় "
				+ Utils.getDigits(formatted_visitDate, "Bangla") + " "
				+ Utils.getDigits(rescTime, "Bangla");
		
		
		String URL =  "AppointmentServlet?actionType=view&ID=" + appointmentDTO.iD;
		UserDTO patientDTO = UserRepository.getUserDTOByUserName(appointmentDTO.employeeUserName);

		pb_notificationsDAO.addPb_notificationsAndSendMailSMS(patientDTO.organogramID, System.currentTimeMillis(),
				URL, englishText, banglaText, "Appointment Rescheduled", appointmentDTO.phoneNumber);
		
	
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		// TODO Auto-generated method stub
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		System.out.println("doPost");
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		
		try
		{
			String actionType = request.getParameter("actionType");
			System.out.println("actionType = " + actionType);
			if(actionType.equals("add"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.APPOINTMENT_ADD))
				{
					System.out.println("going to  addAppointment ");
					addAppointment(request, response, true, userDTO, true);
				}
				else
				{
					System.out.println("Not going to  addAppointment ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}

			else if(actionType.equals("getDTO"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.APPOINTMENT_ADD))
				{
					getDTO(request, response);					
				}
				else
				{
					System.out.println("Not going to  addAppointment ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else if(actionType.equals("edit"))
			{
				
				/*if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.APPOINTMENT_UPDATE))
				{					
					addAppointment(request, response, false, userDTO, isPermanentTable);					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}*/
			}
			else if(actionType.equals("delete"))
			{								
				//deleteAppointment(request, response, userDTO);				
			}	
			else if(actionType.equals("search"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.APPOINTMENT_SEARCH))
				{
					searchAppointment(request, response, true, getDefaultFilter(userDTO));
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			
			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}
	
	private void getDTO(HttpServletRequest request, HttpServletResponse response) 
	{
		try 
		{
			System.out.println("In getDTO");
			AppointmentDTO appointmentDTO = appointmentDAO.getDTOByID(Long.parseLong(request.getParameter("ID")));
			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			
			String encoded = this.gson.toJson(appointmentDTO);
			System.out.println("json encoded data = " + encoded);
			out.print(encoded);
			out.flush();
		}
		catch (NumberFormatException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	private void addAppointment(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, boolean isPermanentTable) throws IOException 
	{
		// TODO Auto-generated method stub
		try 
		{
			long startTime = System.currentTimeMillis();
			request.setAttribute("failureMessage", "");
			System.out.println("%%%% addAppointment");
			String path = getServletContext().getRealPath("/img2/");
			AppointmentDTO appointmentDTO;
			SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
						
			if(addFlag == true)
			{
				appointmentDTO = new AppointmentDTO();
			}
			else
			{
				appointmentDTO = appointmentDAO.getDTOByID(Long.parseLong(request.getParameter("iD")));
			}
			
			String Value = "";

			Value = request.getParameter("visitDate");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("visitDate = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				try 
				{
					Date d = f.parse(Value);

					Calendar c = Calendar.getInstance();
					c.setTimeInMillis(d.getTime());
					c.set(Calendar.HOUR_OF_DAY, 0);
					c.set(Calendar.MINUTE, 0);
					c.set(Calendar.SECOND, 0);
					c.set(Calendar.MILLISECOND, 0);
					
					appointmentDTO.visitDate = c.getTimeInMillis();
					System.out.println("Appointment visit Date set = " + appointmentDTO.visitDate);
					
					if(appointmentDTO.visitDate < TimeConverter.getToday()) //Old appointments are not taken
					{
						System.out.println("Appointment failed: Invalid date");
						request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
						return;
					}
				}
				catch (Exception e) 
				{
					e.printStackTrace();
				}
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			long spent = System.currentTimeMillis() - startTime;
			System.out.println("Spent 1 = " + spent);

			Value = request.getParameter("specialityType");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("specialityType = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				appointmentDTO.specialityType = Long.parseLong(Value);
				appointmentDTO.deptName = CatDAO.getName("english", "department", appointmentDTO.specialityType);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("doctorId");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("doctorId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				appointmentDTO.doctorId = Long.parseLong(Value);
				if(!appointmentDAO.isAvailableDoctorOrPhysiotherapist(appointmentDTO.doctorId))
				{
					System.out.println("Appointment failed: dr not valid");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
					return;
				}
				appointmentDTO.doctorName = WorkflowController.getNameFromOrganogramId(appointmentDTO.doctorId, "english");
				UserDTO drDTO = UserRepository.getUserDTOByOrganogramID(appointmentDTO.doctorId);
				appointmentDTO.drUserName = drDTO.userName;
				appointmentDTO.drEmployeeRecordId = drDTO.employee_record_id;
				
				EmployeeOfficeDTO drOfficeDTO = EmployeeOfficeRepository.getInstance().getByOfficeUnitOrganogramId(appointmentDTO.doctorId);
				if(drOfficeDTO != null && drOfficeDTO.medicalDeptCat == AppointmentDTO.DENTISTRY_DEPT)
				{
					appointmentDTO.isDental = true;
				}
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			spent = System.currentTimeMillis() - startTime;
			System.out.println("Spent 2 = " + spent);

			Value = request.getParameter("shiftCat");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("shiftCat = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				appointmentDTO.shiftCat = Integer.parseInt(Value); //invalid shift won't harm
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("availableTimeSlot");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("availableTimeSlot = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{				
				appointmentDTO.availableTimeSlot = Long.parseLong(Value);
				if(appointmentDAO.hasDTOInDateAndSlot(appointmentDTO.doctorId, appointmentDTO.visitDate, appointmentDTO.availableTimeSlot))
				{
					System.out.println("Appointment failed: Invalid slot");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
					return;
				}
				Doctor_time_slotDAO doctor_time_slotDAO = new Doctor_time_slotDAO();
				Doctor_time_slotDTO doctor_time_slotDTO = doctor_time_slotDAO.getSlotDTOByDoctorShift(appointmentDTO.doctorId, appointmentDTO.shiftCat);
				if(doctor_time_slotDTO != null)
				{
					appointmentDTO.qualifications = doctor_time_slotDTO.qualifications;
				}
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			spent = System.currentTimeMillis() - startTime;
			System.out.println("Spent 3 = " + spent);

			Value = request.getParameter("remarks");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("remarks = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				appointmentDTO.remarks = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = Utils.getDigitEnglishFromBangla(request.getParameter("userName"));

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("userName = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				appointmentDTO.employeeUserName = Value;
				Employee_recordsDTO erDTO = Employee_recordsRepository.getInstance().getByUserNameOrPhone(appointmentDTO.employeeUserName, "");
				if(erDTO != null)
				{
					if(erDTO.employmentStatus != EmploymentStatusEnum.ACTIVE.getValue()
							&& (erDTO.employmentStatus != EmploymentStatusEnum.LPR.getValue()))
					{
						System.out.println("Appointment failed: Inactive, not in lpr. erDTO.employmentStatus = " + erDTO.employmentStatus);
						request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
						return;
					}
				}
				UserDTO employeeUserDTO = userDAO.getUserDTOByUsernameFromOnlyUsers(appointmentDTO.employeeUserName);
				if(employeeUserDTO != null)
				{
					appointmentDTO.erId = erDTO.iD;
					UserDTO dtoFromReo = UserRepository.getUserDTOByUserName(appointmentDTO.employeeUserName);
					if(dtoFromReo != null)
					{
						appointmentDTO.patientId = dtoFromReo.organogramID;
					}
					System.out.println("Inside employee detected " + appointmentDTO.patientId);
					erDTO = Employee_recordsRepository.getInstance().getById(employeeUserDTO.employee_record_id);
					if(erDTO != null)
					{
						appointmentDTO.alternatePhone = erDTO.alternativeMobile;
					}
				}
				else
				{
					appointmentDTO.patientId = -1;
					System.out.println("Bad employee detected " + appointmentDTO.patientId);
				}
				
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			
			if(!appointmentDAO.canCreateThisAppointment(userDTO, appointmentDTO))
			{
				System.out.println("Appointment failed: You cannot create appointment for others");
				request.getRequestDispatcher("common/error_page.jsp").forward(request, response); //not an employee
				return;
			}
			spent = System.currentTimeMillis() - startTime;
			System.out.println("Spent 4 = " + spent);

			Value = request.getParameter("patientName");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("patientName = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				appointmentDTO.patientName = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			
			Value = request.getParameter("othersOfficeId");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("othersOfficeId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				appointmentDTO.othersOfficeId = Long.parseLong(Value);
				if(appointmentDTO.othersOfficeId != -1)
				{
					NameDTO otherOfficeDTO = Other_officeRepository.getInstance().getDTOByID(appointmentDTO.othersOfficeId);
					if(otherOfficeDTO != null)
					{
						appointmentDTO.othersOfficeBn = otherOfficeDTO.nameBn;
						appointmentDTO.othersOfficeEn = otherOfficeDTO.nameEn;
					}
				}
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			
			Value = request.getParameter("othersDesignationAndId");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("othersDesignationAndId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				appointmentDTO.othersDesignationAndId = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			
			Value = request.getParameter("bearerName");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("bearerName = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				appointmentDTO.bearerName = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			
			Value = request.getParameter("bearerPhone");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("bearerPhone = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				appointmentDTO.bearerPhone = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			
			Value = request.getParameter("bearerUserName");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("bearerUserName = " + Value);
			if(Value != null)
			{
				
				appointmentDTO.bearerUserName = (Value);
				if(!appointmentDTO.bearerUserName.equalsIgnoreCase(""))
				{
					UserDTO bearerDTO = userDAO.getUserDTOByUsernameFromOnlyUsers(appointmentDTO.bearerUserName);
					if(bearerDTO == null)
					{
						System.out.println("Invalid bearer username = " + Value);
						appointmentDTO.bearerUserName = "";
					}
				}
				
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("phoneNumber");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("phoneNumber = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				appointmentDTO.phoneNumber = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("dateOfBirth");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("dateOfBirth = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				try 
				{
					Date d = f.parse(Value);
					appointmentDTO.dateOfBirth = d.getTime();
					Date now = new Date();
                    long diffInMillies = Math.abs(now.getTime() - d.getTime());
                    appointmentDTO.age =   (diffInMillies 
                                    / (1000l * 60 * 60 * 24 * 365)); 
				}
				catch (Exception e) 
				{
					e.printStackTrace();
				}
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}



			Value = request.getParameter("genderCat");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("genderCat = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				appointmentDTO.genderCat = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("whoIsThePatientCat");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("whoIsThePatientCat = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				appointmentDTO.whoIsThePatientCat = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			
			Value = request.getParameter("extraRelation");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("extraRelation = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				appointmentDTO.extraRelation = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("isChargeNeeded");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("isChargeNeeded = " + Value);
            appointmentDTO.isChargeNeeded = Value != null && !Value.equalsIgnoreCase("");
			if(addFlag)
			{
				appointmentDTO.sl = appointmentDTO.employeeUserName.substring(appointmentDTO.employeeUserName.length() - 4, appointmentDTO.employeeUserName.length() - 1) + "-";
				SimpleDateFormat sf = new SimpleDateFormat("yy-MM-dd-HH-mm-ss");
				String now = sf.format(new Date());
				appointmentDTO.sl += now;
				appointmentDTO.sl = appointmentDTO.sl.replaceAll("-", "");
			}
			
			System.out.println("Done adding  addAppointment dto = " + appointmentDTO);
			long returnedID = -1;
			spent = System.currentTimeMillis() - startTime;
			System.out.println("Spent 5 = " + spent);
			
			
			if(addFlag == true)
			{
				appointmentDTO.insertedByUserName = userDTO.userName;
				appointmentDTO.insertedByErId = userDTO.employee_record_id;
				appointmentDTO.insertedByOrganogramId = userDTO.organogramID;
				appointmentDTO.insertedByRoleId = userDTO.roleID;
				appointmentDTO.insertionDate = TimeConverter.getToday();
				
				returnedID = appointmentDAO.add(appointmentDTO);
				appointmentDAO.sendNoti(appointmentDTO);
			}
			spent = System.currentTimeMillis() - startTime;
			System.out.println("Spent 5.1 = " + spent);
			
			
			Value = request.getParameter("fromPrescription");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("fromPrescription = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				long fromPrescription = Long.parseLong(Value);
				
				Prescription_detailsDTO prescription_detailsDTO = prescription_detailsDAO.getDTOByID(fromPrescription);
				prescription_detailsDTO.referredAppointmentId = returnedID;
				prescription_detailsDAO.update(prescription_detailsDTO);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			spent = System.currentTimeMillis() - startTime;
			System.out.println("Spent 6 = " + spent);
			
			
			response.sendRedirect("AppointmentServlet?actionType=view&ID=" + returnedID);

					
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	



	
	
	

	private void deleteAppointment(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO) throws IOException 
	{				
		try 
		{
			String[] IDsToDelete = request.getParameterValues("ID");
			for(int i = 0; i < IDsToDelete.length; i ++)
			{
				long id = Long.parseLong(IDsToDelete[i]);
				System.out.println("------ DELETING " + IDsToDelete[i]);
				
				
				AppointmentDTO appointmentDTO = appointmentDAO.getDTOByID(id);
				appointmentDAO.manageWriteOperations(appointmentDTO, SessionConstants.DELETE, id, userDTO);
				response.sendRedirect("AppointmentServlet?actionType=search");
				
			}			
		}
		catch (Exception ex) 
		{
			ex.printStackTrace();
		}
		
	}

	private void getAppointment(HttpServletRequest request, HttpServletResponse response, long id) throws ServletException, IOException
	{
		System.out.println("in getAppointment");
		AppointmentDTO appointmentDTO = null;
		try 
		{
			appointmentDTO = appointmentDAO.getDTOByID(id);
			request.setAttribute("ID", appointmentDTO.iD);
			request.setAttribute("appointmentDTO",appointmentDTO);
			request.setAttribute("appointmentDAO",appointmentDAO);
			
			String URL= "";
			
			String inPlaceEdit = request.getParameter("inplaceedit");
			String inPlaceSubmit = request.getParameter("inplacesubmit");
			String getBodyOnly = request.getParameter("getBodyOnly");
			
			if(inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase(""))
			{
				URL = "appointment/appointmentInPlaceEdit.jsp";	
				request.setAttribute("inplaceedit","");				
			}
			else if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
			{
				URL = "appointment/appointmentSearchRow.jsp";
				request.setAttribute("inplacesubmit","");					
			}
			else
			{
				if(getBodyOnly  != null && !getBodyOnly.equalsIgnoreCase(""))
				{
					URL = "appointment/appointmentEditBody.jsp?actionType=edit";
				}
				else
				{
					URL = "appointment/appointmentEdit.jsp?actionType=edit";
				}				
			}
			
			RequestDispatcher rd = request.getRequestDispatcher(URL);
			rd.forward(request, response);
		}
		catch (NumberFormatException e) 
		{
			e.printStackTrace();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	
	private void getAppointment(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		getAppointment(request, response, Long.parseLong(request.getParameter("ID")));	
	}
	
	private void searchAppointment(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException
	{
		System.out.println("in  searchAppointment 1");
		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String ajax = request.getParameter("ajax");
		boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);
		
        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
			SessionConstants.NAV_APPOINTMENT,
			request,
			appointmentDAO,
			SessionConstants.VIEW_APPOINTMENT,
			SessionConstants.SEARCH_APPOINTMENT,
			tableName,
			isPermanent,
			userDTO,
			filter,
			true);
        try
        {
			System.out.println("trying to dojob");
			
			System.out.println("rn 20");
			rnManager.doJob(loginDTO, "20");
		
        }
        catch(Exception e)
        {
			System.out.println("failed to dojob" + e);
        }
        

		request.setAttribute("appointmentDAO",appointmentDAO);
        RequestDispatcher rd;
        if(!isPermanent)
        {
        	if(hasAjax == false)
	        {
	        	System.out.println("Going to appointment/appointmentApproval.jsp");
	        	rd = request.getRequestDispatcher("appointment/appointmentApproval.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to appointment/appointmentApprovalForm.jsp");
	        	rd = request.getRequestDispatcher("appointment/appointmentApprovalForm.jsp");
	        }
        }
        else
        {
	        if(hasAjax == false)
	        {
	        	System.out.println("Going to appointment/appointmentSearch.jsp");
	        	rd = request.getRequestDispatcher("appointment/appointmentSearch.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to appointment/appointmentSearchForm.jsp");
	        	rd = request.getRequestDispatcher("appointment/appointmentSearchForm.jsp");
	        }
        }
		rd.forward(request, response);
	}
	
}

