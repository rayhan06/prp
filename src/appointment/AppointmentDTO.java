package appointment;

import family.FamilyDTO;
import pb.CatDTO;
import pbReport.TimeConvertor;
import util.*; 


public class AppointmentDTO extends CommonDTO
{

	public long visitDate = 0;
	public long specialityType = 0;
	public long doctorId = 0;
	public int shiftCat = 0;
	public long availableTimeSlot = 0;
    public String remarks = "";
	public long patientId = 0;
	public String employeeUserName = "";
    public String patientName = "";
    public String phoneNumber = "880";
	public long dateOfBirth = 0;
	public long age = 0;
	public int genderCat = CatDTO.CATDEFAULT;
	public int whoIsThePatientCat =FamilyDTO.OWN;
	public boolean isChargeNeeded = false;
    public String doctorName = "";
    public String deptName = "";
    public boolean isCancelled = false;
    public String cancellerUserName = "";
	public long cancellerOrganogram = -1;
	public long cancellingDate = -1;
	public String cancellingRemarks = "";
	public String sl = "";
	
	public int toDaysSerial = -1;
	public String drUserName = "";
	
	public String qualifications = "";
	
	public String othersOfficeBn = "";
	public String othersOfficeEn = "";
	public long othersOfficeId = -1;
	public String othersDesignationAndId = "";
	
	public String alternatePhone = "";
	
	
	public String bearerPhone = "";
	public String bearerName = "";
	public String bearerUserName = "";
	
	public String insertedByUserName ="";
	public long insertedByOrganogramId = -1;
	public long insertedByRoleId = -1;
	public long insertedByErId = -1;
	public long insertionDate = TimeConverter.getToday();
	
	public String extraRelation = "";
	
	public long erId = -1;
	public long prescriptionId = -1;
	public long drEmployeeRecordId = -1;
	
	public boolean isDental = false;
	
	public static final int DENTISTRY_DEPT = 16;
	
    @Override
	public String toString() {
            return "$AppointmentDTO[" +
            " iD = " + iD +
            " visitDate = " + visitDate +
            " specialityType = " + specialityType +
            " doctorId = " + doctorId +
            " shiftCat = " + shiftCat +
            " availableTimeSlot = " + availableTimeSlot +
            " remarks = " + remarks +
            " patientId = " + patientId +
            " patientName = " + patientName +
            " phoneNumber = " + phoneNumber +
            " dateOfBirth = " + dateOfBirth +
            " age = " + age +
            " genderCat = " + genderCat +
            " whoIsThePatientCat = " + whoIsThePatientCat +
            " isChargeNeeded = " + isChargeNeeded +
            " doctorName = " + doctorName +
            " deptName = " + deptName +
            " searchColumn = " + searchColumn +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }
    

}