package appointment;


import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.text.SimpleDateFormat;
import java.util.*;

import sessionmanager.SessionConstants;
import java.sql.SQLException;
import common.ConnectionAndStatementUtil;


import org.apache.log4j.Logger;


import doctor_time_slot.Doctor_time_slotDAO;
import doctor_time_slot.Doctor_time_slotDTO;
import employee_family_info.Employee_family_infoDAO;
import employee_family_info.Employee_family_infoDTO;

import family.FamilyDTO;
import repository.RepositoryManager;

import util.*;
import workflow.WorkflowController;
import pb.*;
import pb_notifications.Pb_notificationsDAO;
import user.UserDTO;
import user.UserRepository;

public class AppointmentDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());
	Doctor_time_slotDAO doctor_time_slotDAO = new Doctor_time_slotDAO();
	Employee_family_infoDAO employee_family_infoDAO = Employee_family_infoDAO.getInstance();
	
	
	public AppointmentDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";
		commonMaps = new AppointmentMAPS(tableName);
		columnNames = new String[] 
		{
			"ID",
			"visit_date",
			"speciality_type",
			"doctor_id",
			"shift_cat",
			"available_time_slot",
			"remarks",
			"patient_id",
			"patient_name",
			"phone_number",
			"date_of_birth",
			"age",
			"gender_cat",
			"who_is_the_patient_cat",
			"is_charge_needed",
			"doctor_name",
			"dept_name",
			"search_column",
			"employee_user_name",
			"isCancelled",
			"canceller_user_name",
			"canceller_organogram",
			"calcelling_remarks",
			"cancelling_date",
			"sl",
			"dr_user_name",
			"qualifications",
			"others_office",
			"others_office_en",
			"others_office_id",
			"others_designation_and_id",
			"alternate_phone",
			
			"extra_relation",
			
			"bearer_name",
			"bearer_phone",
			"bearer_user_name",
			
			"inserted_by_user_name",
			"inserted_by_organogram_id",
			"inserted_by_role_id",
			"insertion_date",
			
			"employee_record_id",
			"prescription_id",
			
			"dr_employee_record_id",
			"is_dental",
			
			"inserted_by_erid",
			
			"isDeleted",
			"lastModificationTime"
		};
	}
	
	public AppointmentDAO()
	{
		this("appointment");		
	}
	
	public void setSearchColumn(AppointmentDTO appointmentDTO)
	{
		appointmentDTO.searchColumn = "";
		appointmentDTO.searchColumn += appointmentDTO.patientName + " ";
		appointmentDTO.searchColumn += appointmentDTO.doctorName + " ";
		appointmentDTO.searchColumn += appointmentDTO.employeeUserName + " ";
		appointmentDTO.searchColumn += Utils.getDigitBanglaFromEnglish(appointmentDTO.employeeUserName) + " ";
		appointmentDTO.searchColumn += appointmentDTO.phoneNumber + " ";
		appointmentDTO.searchColumn += appointmentDTO.alternatePhone + " ";
		appointmentDTO.searchColumn += Utils.getDigitBanglaFromEnglish(appointmentDTO.phoneNumber) + " ";
		appointmentDTO.searchColumn += appointmentDTO.sl + " ";
		appointmentDTO.searchColumn += Utils.getDigitBanglaFromEnglish(appointmentDTO.sl) + " ";
	}
	
	public String getRelation(AppointmentDTO appointmentDTO, boolean isLangEng)
	{
		String value =  "";
    	if(appointmentDTO.whoIsThePatientCat == FamilyDTO.OTHER)
    	{
    		value += appointmentDTO.extraRelation;
    	}
    	else if(appointmentDTO.whoIsThePatientCat > 0)
    	{
    		Employee_family_infoDTO employee_family_infoDTO = employee_family_infoDAO.getDTOFromID(appointmentDTO.whoIsThePatientCat);
    		if(employee_family_infoDTO!= null)
    		{
    			value += CommonDAO.getName(isLangEng?"english":"bangla", "relation", employee_family_infoDTO.relationType);
    		}
    	}
    	else
    	{
    		if(-appointmentDTO.whoIsThePatientCat < FamilyDTO.catEn.length)
    		{
    			if(isLangEng)
        		{
        			value += FamilyDTO.catEn[-appointmentDTO.whoIsThePatientCat];
        		}
        		else
        		{
        			value += FamilyDTO.catBn[-appointmentDTO.whoIsThePatientCat];
        		}       
    		}    		
    	}
    	return value;
	}
	
	public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException
	{
		AppointmentDTO appointmentDTO = (AppointmentDTO)commonDTO;
		int index = 1;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(appointmentDTO);
		if(isInsert)
		{
			ps.setObject(index++,appointmentDTO.iD);
		}
		ps.setObject(index++,appointmentDTO.visitDate);
		ps.setObject(index++,appointmentDTO.specialityType);
		ps.setObject(index++,appointmentDTO.doctorId);
		ps.setObject(index++,appointmentDTO.shiftCat);
		ps.setObject(index++,appointmentDTO.availableTimeSlot);
		ps.setObject(index++,appointmentDTO.remarks);
		ps.setObject(index++,appointmentDTO.patientId);
		ps.setObject(index++,appointmentDTO.patientName);
		ps.setObject(index++,appointmentDTO.phoneNumber);
		ps.setObject(index++,appointmentDTO.dateOfBirth);
		ps.setObject(index++,appointmentDTO.age);
		ps.setObject(index++,appointmentDTO.genderCat);
		ps.setObject(index++,appointmentDTO.whoIsThePatientCat);
		ps.setObject(index++,appointmentDTO.isChargeNeeded);
		ps.setObject(index++,appointmentDTO.doctorName);
		ps.setObject(index++,appointmentDTO.deptName);
		ps.setObject(index++,appointmentDTO.searchColumn);
		ps.setObject(index++,appointmentDTO.employeeUserName);
		ps.setObject(index++,appointmentDTO.isCancelled);
		ps.setObject(index++,appointmentDTO.cancellerUserName);
		ps.setObject(index++,appointmentDTO.cancellerOrganogram);
		ps.setObject(index++,appointmentDTO.cancellingRemarks);
		ps.setObject(index++,appointmentDTO.cancellingDate);
		ps.setObject(index++,appointmentDTO.sl);
		ps.setObject(index++,appointmentDTO.drUserName);
		ps.setObject(index++,appointmentDTO.qualifications);
		ps.setObject(index++,appointmentDTO.othersOfficeBn);
		ps.setObject(index++,appointmentDTO.othersOfficeEn);
		ps.setObject(index++,appointmentDTO.othersOfficeId);
		ps.setObject(index++,appointmentDTO.othersDesignationAndId);
		ps.setObject(index++,appointmentDTO.alternatePhone);
		
		ps.setObject(index++,appointmentDTO.extraRelation);
		
		ps.setObject(index++,appointmentDTO.bearerName);
		ps.setObject(index++,appointmentDTO.bearerPhone);
		ps.setObject(index++,appointmentDTO.bearerUserName);
		
		
		ps.setObject(index++,appointmentDTO.insertedByUserName);
		ps.setObject(index++,appointmentDTO.insertedByOrganogramId);
		ps.setObject(index++,appointmentDTO.insertedByRoleId);
		ps.setObject(index++,appointmentDTO.insertionDate);
		
		ps.setObject(index++,appointmentDTO.erId);
		ps.setObject(index++,appointmentDTO.prescriptionId);
		
		ps.setObject(index++,appointmentDTO.drEmployeeRecordId);
		ps.setObject(index++,appointmentDTO.isDental);
		
		ps.setObject(index++,appointmentDTO.insertedByErId);
		if(isInsert)
		{
			ps.setObject(index++, 0);
			ps.setObject(index++, lastModificationTime);
		}
	}
	
	public AppointmentDTO build(ResultSet rs)
	{
		try
		{
			AppointmentDTO appointmentDTO = new AppointmentDTO();
			appointmentDTO.iD = rs.getLong("ID");
			appointmentDTO.visitDate = rs.getLong("visit_date");
			appointmentDTO.specialityType = rs.getLong("speciality_type");
			appointmentDTO.doctorId = rs.getLong("doctor_id");
			appointmentDTO.shiftCat = rs.getInt("shift_cat");
			appointmentDTO.availableTimeSlot = rs.getLong("available_time_slot");
			appointmentDTO.remarks = rs.getString("remarks");
			appointmentDTO.patientId = rs.getLong("patient_id");
			appointmentDTO.patientName = rs.getString("patient_name");
			appointmentDTO.phoneNumber = rs.getString("phone_number");
			appointmentDTO.dateOfBirth = rs.getLong("date_of_birth");
			appointmentDTO.age = rs.getLong("age");
			appointmentDTO.genderCat = rs.getInt("gender_cat");
			appointmentDTO.whoIsThePatientCat = rs.getInt("who_is_the_patient_cat");
			appointmentDTO.isChargeNeeded = rs.getBoolean("is_charge_needed");
			appointmentDTO.doctorName = rs.getString("doctor_name");
			appointmentDTO.deptName = rs.getString("dept_name");
			appointmentDTO.searchColumn = rs.getString("search_column");
			appointmentDTO.employeeUserName = rs.getString("employee_user_name");
			appointmentDTO.isCancelled = rs.getBoolean("isCancelled");
			appointmentDTO.cancellerUserName = rs.getString("canceller_user_name");
			appointmentDTO.cancellerOrganogram = rs.getLong("canceller_organogram");
			appointmentDTO.cancellingRemarks = rs.getString("calcelling_remarks");
			appointmentDTO.cancellingDate = rs.getLong("cancelling_date");
			appointmentDTO.sl = rs.getString("sl");
			appointmentDTO.drUserName = rs.getString("dr_user_name");
			appointmentDTO.qualifications = rs.getString("qualifications");
			
			appointmentDTO.othersOfficeBn = rs.getString("others_office");
			appointmentDTO.othersOfficeEn = rs.getString("others_office_en");
			appointmentDTO.othersOfficeId = rs.getLong("others_office_id");
			appointmentDTO.othersDesignationAndId = rs.getString("others_designation_and_id");
			appointmentDTO.alternatePhone = rs.getString("alternate_phone");
			
			appointmentDTO.extraRelation = rs.getString("extra_relation");
			if(appointmentDTO.extraRelation == null)
			{
				appointmentDTO.extraRelation = "";
			}
			
			appointmentDTO.bearerName = rs.getString("bearer_name");
			appointmentDTO.bearerPhone = rs.getString("bearer_phone");
			appointmentDTO.bearerUserName = rs.getString("bearer_user_name");
			
			appointmentDTO.insertedByUserName = rs.getString("inserted_by_user_name");
			appointmentDTO.insertedByOrganogramId = rs.getLong("inserted_by_organogram_id");
			appointmentDTO.insertedByRoleId = rs.getLong("inserted_by_role_id");
			appointmentDTO.insertionDate = rs.getLong("insertion_date");
			
			appointmentDTO.erId = rs.getLong("employee_record_id");
			appointmentDTO.prescriptionId = rs.getLong("prescription_id");
			
			appointmentDTO.drEmployeeRecordId = rs.getLong("dr_employee_record_id");
			appointmentDTO.isDental = rs.getBoolean("is_dental");
			
			appointmentDTO.insertedByErId = rs.getLong("inserted_by_erid");
			
			appointmentDTO.isDeleted = rs.getInt("isDeleted");
			appointmentDTO.lastModificationTime = rs.getLong("lastModificationTime");
			return appointmentDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}

	public int shift = -1;
	
	public List<KeyCountDTO> getDrAppointmentCount()
    {
		String sql = "select dr_user_name, count(id) from appointment where isDeleted = 0 group by dr_user_name order by count(id) desc;";

		return ConnectionAndStatementUtil.getListOfT(sql,this::getDrWiseCount);	
    }
	
	public KeyCountDTO getDrWiseCount(ResultSet rs)
	{
		try
		{
			KeyCountDTO keyCountDTO = new KeyCountDTO();
			keyCountDTO.key = rs.getLong("dr_user_name");
			keyCountDTO.count = rs.getInt("count(id)");

			return keyCountDTO;
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
			return null;
		}
	}
	
	public List<KeyCountDTO> getDrAppointmentPrescriptionCountToday()
    {
		String sql = "SELECT \r\n" + 
				"    dr_user_name,\r\n" + 
				"    COUNT(id) AS appointments,\r\n" + 
				"    SUM(CASE  WHEN prescription_id >= 0 THEN 1 ELSE 0  END) AS prescriptions\r\n" + 
				"FROM\r\n" + 
				"    appointment where appointment.isCancelled = 0 \r\n" + 
				"            and visit_date >= " + TimeConverter.getToday() + " \r\n" + 
				"GROUP BY dr_user_name";

		return ConnectionAndStatementUtil.getListOfT(sql,this::getDrWiseAPCount);	
    }
	
	public List<KeyCountDTO> getDrAppointmentPrescriptionCount()
    {
		String sql = "SELECT \r\n" + 
				"    dr_user_name,\r\n" + 
				"    COUNT(id) AS appointments,\r\n" + 
				"    SUM(CASE  WHEN prescription_id >= 0 THEN 1 ELSE 0  END) AS prescriptions\r\n" + 
				"FROM\r\n" + 
				"    appointment where appointment.isCancelled = 0 \r\n" + 
				"GROUP BY dr_user_name";

		return ConnectionAndStatementUtil.getListOfT(sql,this::getDrWiseAPCount);	
    }
	
	public KeyCountDTO getDrWiseAPCount(ResultSet rs)
	{
		try
		{
			KeyCountDTO keyCountDTO = new KeyCountDTO();
			keyCountDTO.keyStr = rs.getString("dr_user_name");
			keyCountDTO.count = rs.getInt("appointments");
			keyCountDTO.count2 = rs.getInt("prescriptions");

			return keyCountDTO;
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
			return null;
		}
	}
	
	
	public List<KeyCountDTO> getReceptionistAppointmentCount()
    {
		String sql = "select inserted_by_user_name, count(id) from appointment where isDeleted = 0 and inserted_by_role_id = " + SessionConstants.MEDICAL_RECEPTIONIST_ROLE;		
		sql += " group by inserted_by_user_name order by count(id) desc;";

		return ConnectionAndStatementUtil.getListOfT(sql,this::getReceptionistWiseCount);	
    }
	
	public List<KeyCountDTO> getReceptionistAppointmentCountToday()
    {
		String sql = "select inserted_by_user_name, count(id) from appointment "
				+ "where isDeleted = 0 and inserted_by_role_id = " + SessionConstants.MEDICAL_RECEPTIONIST_ROLE
				+ " and insertion_date >= " + TimeConverter.getToday();		
		sql += " group by inserted_by_user_name order by count(id) desc;";

		return ConnectionAndStatementUtil.getListOfT(sql,this::getReceptionistWiseCount);	
    }
	
	public KeyCountDTO getReceptionistWiseCount(ResultSet rs)
	{
		try
		{
			KeyCountDTO keyCountDTO = new KeyCountDTO();
			keyCountDTO.keyStr = rs.getString("inserted_by_user_name");
			keyCountDTO.count = rs.getInt("count(id)");
			UserDTO recDTO = UserRepository.getUserDTOByUserName(keyCountDTO.keyStr);
			if(recDTO != null)
			{
				keyCountDTO.key = recDTO.organogramID;
			}

			return keyCountDTO;
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
			return null;
		}
	}
	
	public boolean setDateShiftSlot(AppointmentDTO appointmentDTO) throws Exception
	{
		Calendar cal = Calendar.getInstance();
		long doctorId = appointmentDTO.doctorId;
		int daysChecked = 0;
		int maxDays = 10;
		long now = System.currentTimeMillis();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
		SimpleDateFormat hourFormat = new SimpleDateFormat("hh:mm aa");
		while(true)
		{
			TimeConverter.clearTimes(cal);
			long lDate = cal.getTimeInMillis();
			int day = cal.get(Calendar.DAY_OF_WEEK) % 7;
			int week = TimeFormat.getNthWeek(lDate);
			List<Doctor_time_slotDTO> doctor_time_slotDTOs = doctor_time_slotDAO.getSlotDTOsByDrDay(doctorId, day, week);
			if(doctor_time_slotDTOs.isEmpty())
			{
				System.out.println("###sloting " + simpleDateFormat.format(cal.getTime()) + " has no slots");
				daysChecked ++;
				cal.add(Calendar.DAY_OF_YEAR, 1);
				continue;
			}
			System.out.println("###sloting " + simpleDateFormat.format(cal.getTime()) + " has " + doctor_time_slotDTOs.size() + " slots");
			
			Calendar cal6PM = Calendar.getInstance();
        	cal6PM.setTime(cal.getTime());
            TimeConverter.clearTimes(cal6PM);
            cal6PM.add(Calendar.HOUR_OF_DAY, 18);
            
            Calendar cal9AM = Calendar.getInstance();
            cal9AM.setTime(cal.getTime());
            TimeConverter.clearTimes(cal9AM);
            cal9AM.add(Calendar.HOUR_OF_DAY, 9);
			
			for(Doctor_time_slotDTO doctor_time_slotDTO: doctor_time_slotDTOs)
			{
				int startHour = Integer.parseInt(doctor_time_slotDTO.startTime.split(":")[0]);
            	int startMinute = Integer.parseInt(doctor_time_slotDTO.startTime.split(":")[1]);
            	
            	int endHour = Integer.parseInt(doctor_time_slotDTO.endTime.split(":")[0]);
            	int endMinute = Integer.parseInt(doctor_time_slotDTO.endTime.split(":")[1]);
            	
            	long slotSize= doctor_time_slotDTO.durationPerPatient;
            	
                Calendar startCalendar = Calendar.getInstance();
                startCalendar.setTime(cal.getTime());
                TimeConverter.clearTimes(startCalendar);
                startCalendar.add(Calendar.HOUR_OF_DAY, startHour);
                startCalendar.add(Calendar.MINUTE, startMinute);
                
                Calendar endCalendar = Calendar.getInstance();
                TimeConverter.clearTimes(endCalendar);
                endCalendar.setTime(cal.getTime());
                endCalendar.add(Calendar.HOUR_OF_DAY, endHour);
                endCalendar.add(Calendar.MINUTE, endMinute);
                
                while(startCalendar.getTimeInMillis() < endCalendar.getTimeInMillis())
                {
                	long slot = startCalendar.getTimeInMillis();
                	System.out.println("###sloting trying " + hourFormat.format(new Date(slot)));
                    
                	if(slot > now 
                			&& slot <= cal6PM.getTimeInMillis() 
                			&& slot >= cal9AM.getTimeInMillis() 
                			&& !hasDTOInDateAndSlot(doctorId, lDate, slot))
                	{
                		System.out.println("###sloting success " + hourFormat.format(new Date(slot)));
                		appointmentDTO.availableTimeSlot = startCalendar.getTimeInMillis();
                		TimeConverter.clearTimes(startCalendar);
                		appointmentDTO.visitDate = startCalendar.getTimeInMillis();
                		appointmentDTO.shiftCat = doctor_time_slotDTO.shiftCat;
                		return true;
                	}
                	startCalendar.add(Calendar.MINUTE, (int) slotSize);
                }

			}			
			daysChecked ++;
			cal.add(Calendar.DAY_OF_YEAR, 1);
			if(daysChecked >= maxDays)
			{
				return false;
			}
		}
	}
	
	public long getNextAvailableSlot(long doctorId) throws Exception
	{
		Doctor_time_slotDAO doctor_time_slotDAO = new Doctor_time_slotDAO();
		long slot = -1;
		for(shift = 0; shift<=1; shift ++)
		{
			Doctor_time_slotDTO doctor_time_slotDTO = doctor_time_slotDAO.getSlotDTOByDoctorShift(doctorId, shift);
			if(doctor_time_slotDTO == null)
			{
				continue;
			}
			else
			{
				int startHour = Integer.parseInt(doctor_time_slotDTO.startTime.split(":")[0]);
            	int startMinute = Integer.parseInt(doctor_time_slotDTO.startTime.split(":")[1]);
            	
            	Calendar startCalendar = Calendar.getInstance();
            	Calendar now = Calendar.getInstance();
            	Calendar today12AM = Calendar.getInstance();
            	today12AM.set(Calendar.HOUR_OF_DAY, 0);
            	today12AM.set(Calendar.MINUTE, 0);
            	today12AM.set(Calendar.SECOND, 0);
            	today12AM.set(Calendar.MILLISECOND, 0);
            	
            	
            	
            	int endHour = Integer.parseInt(doctor_time_slotDTO.endTime.split(":")[0]);
            	int endMinute = Integer.parseInt(doctor_time_slotDTO.endTime.split(":")[1]);
            	
            	long slotSize= doctor_time_slotDTO.durationPerPatient;
            	
            	startCalendar = Calendar.getInstance();

            	startCalendar.set(Calendar.HOUR_OF_DAY, startHour);
            	startCalendar.set(Calendar.MINUTE, startMinute);
            	
            	long scheduledStartTime = startCalendar.getTimeInMillis();
            	
            	if(now.getTimeInMillis() > startCalendar.getTimeInMillis())
            	{
            		startCalendar.setTimeInMillis(now.getTimeInMillis());
            		startCalendar.set(Calendar.SECOND, 0);
            		startCalendar.set(Calendar.MILLISECOND, 0);
            		
            		long timePassedInMinutes = (startCalendar.getTimeInMillis() - scheduledStartTime)/(1000 * 60);
            		long nextSlotMinutes = slotSize - timePassedInMinutes % slotSize;
            		startCalendar.add(Calendar.MINUTE, (int)nextSlotMinutes);
            	}
                
                
                
                Calendar endCalendar = Calendar.getInstance();

                endCalendar.set(Calendar.HOUR_OF_DAY, endHour);
                endCalendar.set(Calendar.MINUTE, endMinute);
                
                while(startCalendar.getTimeInMillis() < endCalendar.getTimeInMillis())
                {
                	slot = startCalendar.getTimeInMillis();
                	if(!hasDTOInDateAndSlot(doctorId, today12AM.getTimeInMillis(), slot))
                	{
                		return slot;
                	}
                	startCalendar.add(Calendar.MINUTE, (int) slotSize);
                }
			}
		}
		return slot;
	}

	public boolean isDoctor(long drId)
	{
		UserDTO drDTO = UserRepository.getUserDTOByOrganogramID(drId);
        return drDTO.roleID == SessionConstants.DOCTOR_ROLE;
    }
	
	public boolean isPhysiotherapist(long drId)
	{
		UserDTO drDTO = UserRepository.getUserDTOByOrganogramID(drId);
        return drDTO.roleID == SessionConstants.PHYSIOTHERAPIST_ROLE;
    }
	
	public boolean isAvailableDoctorOrPhysiotherapist(long drId)
	{
		//if(isDoctor(drId) || isPhysiotherapist(drId))
		{
            return doctor_time_slotDAO.isAvailable(drId);
		}
    }
	
	public boolean isDoctorOrPhysiotherapist(long drId)
	{
        return isDoctor(drId) || isPhysiotherapist(drId);
    }

	public void createAppointmentNoti(AppointmentDTO appointmentDTO)
	{

		Pb_notificationsDAO pb_notificationsDAO = Pb_notificationsDAO.getInstance();

		try {
			String englishText = "Please create an appointment" 
					+ " with " + WorkflowController.getNameFromOrganogramId(appointmentDTO.doctorId, "English");
			String banglaText =  WorkflowController.getNameFromOrganogramId(appointmentDTO.doctorId, "Bangla") + " এর সাথে "
					+ "অনুগ্রহ করে অ্যাপয়েন্টমেন্ট করুন ";


            String URL =  "AppointmentServlet?actionType=view&ID=" + appointmentDTO.iD;
			UserDTO patientDTO = UserRepository.getUserDTOByUserName(appointmentDTO.employeeUserName);

			if(patientDTO != null)
			{
				pb_notificationsDAO.addPb_notificationsAndSendMailSMS(patientDTO.organogramID, System.currentTimeMillis(), URL, 
						englishText, banglaText, "Create Appointment", appointmentDTO.phoneNumber);
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	public void sendNoti(AppointmentDTO appointmentDTO)
	{
		SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat hourFormat = new SimpleDateFormat("hh:mm aa");
		Pb_notificationsDAO pb_notificationsDAO = Pb_notificationsDAO.getInstance();
		String formatted_visitDate = f.format(new Date(appointmentDTO.visitDate));
		String rescTime = hourFormat.format(new Date(appointmentDTO.availableTimeSlot));

		UserDTO drDTO = UserRepository.getUserDTOByOrganogramID(appointmentDTO.doctorId);
		try {
			String englishMessage = "Your patient has an appointment on " 
					+ formatted_visitDate;
			if(drDTO.roleID != SessionConstants.PHYSIOTHERAPIST_ROLE)
			{
				englishMessage+= " " + rescTime; 
			}
			englishMessage+=  " with " + WorkflowController.getNameFromOrganogramId(appointmentDTO.doctorId, "English");
			
			String banglaMessage =  WorkflowController.getNameFromOrganogramId(appointmentDTO.doctorId, "Bangla") + " এর সাথে "
					+ "আপনার রোগীর অ্যাপয়েন্টমেন্টের সময় ";
			if(drDTO.roleID != SessionConstants.PHYSIOTHERAPIST_ROLE)
			{
				banglaMessage+= Utils.getDigits(formatted_visitDate, "Bangla") + " ";
				banglaMessage+=  Utils.getDigits(rescTime, "Bangla");
			}
		
			
			String URL =  "AppointmentServlet?actionType=view&ID=" + appointmentDTO.iD;
			UserDTO patientDTO = UserRepository.getUserDTOByUserName(appointmentDTO.employeeUserName);

			if(patientDTO != null)
			{
				pb_notificationsDAO.addPb_notificationsAndSendMailSMS(patientDTO.organogramID, System.currentTimeMillis(), URL, 
						englishMessage, banglaMessage, "Appointment", appointmentDTO.phoneNumber);
			}
			else
			{
				logger.debug("Not sending noti to " + appointmentDTO.employeeUserName);
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	public long getId(ResultSet rs)
	{
		try {
			return rs.getLong("id");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return -1L;
		}
	
	}
	
	public long getMostRecentAppointmentIdTillToday (String userName, long patientCat)
	{
		
		String sql = "select id from appointment where isDeleted = 0 "
				+ " and employee_record_id = " + WorkflowController.getEmployeeRecordsIdFromUserName(userName)
				+ " and who_is_the_patient_cat = " + patientCat
				+ " and visit_date <= " + TimeConverter.getToday()
				+ " order by visit_date desc limit 1";
		
		long id = ConnectionAndStatementUtil.getT(sql,this::getId, -1L);
		return id;		
	}
	
	public long getMostRecentAppointmentIdTillToday (long doctorId, String userName, long patientCat)
	{
		
		String sql = "select id from appointment where isDeleted = 0 and doctor_id = " + doctorId
				+ " and employee_record_id = " + WorkflowController.getEmployeeRecordsIdFromUserName(userName)
				+ " and who_is_the_patient_cat = " + patientCat
				+ " and visit_date <= " + TimeConverter.getToday()
				+ " order by visit_date desc limit 1";
		
		long id = ConnectionAndStatementUtil.getT(sql,this::getId, -1L);
		return id;		
	}
	
	public int getSerial(ResultSet rs)
	{
		try {
			return rs.getInt("COUNT(id)") + 1;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return 0;
		}
		
	}
	
	
	public int getDaysSerial (AppointmentDTO appointmentDTO)
	{
		String sql = "SELECT \r\n" + 
				"    COUNT(id)\r\n" + 
				"FROM\r\n" + 
				"    appointment\r\n" + 
				"WHERE\r\n" + 
				"    visit_date = " + appointmentDTO.visitDate + "\r\n" + 
				"        AND doctor_id = " + appointmentDTO.doctorId + "\r\n" + 
				"        AND available_time_slot < " + appointmentDTO.availableTimeSlot + "\r\n" + 
				"        and iscancelled = 0\r\n" + 
				"        and isdeleted = 0;";
		int serial = ConnectionAndStatementUtil.getT(sql,this::getSerial, 0);
		return serial;
	}
	
	

	public AppointmentDTO getDTOByID (long id)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID = " + id;
		AppointmentDTO appointmentDTO = ConnectionAndStatementUtil.getT(sql,this::build);
		return appointmentDTO;
	}
	
	public boolean hasDTO(ResultSet rs)
	{
		try {
            return rs.getInt("count(id)") > 0;
        } catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}

	public boolean hasDTOInDateAndSlot (long drId, long date, long slot) throws Exception
	{
		if(isPhysiotherapist(drId))
		{
			return false; //physiotherapists don't have slots
		}
		String sql = "SELECT count(id) ";

		sql += " FROM " + tableName;
		
        sql += " WHERE isDeleted = 0 and isCancelled = 0 "
        		+ "and visit_date =" + date + " and available_time_slot = " + slot
        		+ " and doctor_id = " + drId;
        return ConnectionAndStatementUtil.getT(sql,this::hasDTO);
		
	}
	
	public boolean hasDTOInDateAndSlot (long drId, long date, long slot, List<AppointmentDTO> slottedAppointments) throws Exception
	{
		if(isPhysiotherapist(drId) || slottedAppointments == null || slottedAppointments.isEmpty())
		{
			return false; //physiotherapists don't have slots
		}
		for(AppointmentDTO appointmentDTO: slottedAppointments)
		{
			if(appointmentDTO.availableTimeSlot == slot)
			{
				return true;
			}
		}
		return false;
		
	}
	
	
	public List<AppointmentDTO> getAppointmentsInSlot (long drId, long date, long startTime, long endTime) throws Exception
	{
		if(isPhysiotherapist(drId))
		{
			return null; //physiotherapists don't have slots
		}
		String sql = "SELECT * ";

		sql += " FROM " + tableName;
		
        sql += " WHERE isDeleted = 0 and isCancelled = 0 "
        		+ "and visit_date =" + date + " and available_time_slot >= " + startTime 
        		+ " and available_time_slot <= " + endTime
        		+ " and doctor_id = " + drId;
        return ConnectionAndStatementUtil.getListOfT(sql,this::build);
		
	}

	public CommonDTO getDTOByDateAndSlot (long date, long slot) throws Exception
	{
		String sql = "SELECT * FROM " + tableName + " WHERE visit_date =" + date + " and available_time_slot = " + slot ;
        AppointmentDTO appointmentDTO = ConnectionAndStatementUtil.getT(sql,this::build);
		return appointmentDTO;
	}

	
	public List<AppointmentDTO> getDTOs(Collection recordIDs)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID IN ( ";
		for(int i = 0;i<recordIDs.size();i++){
			if(i!=0){
				sql+=",";
			}
			sql+=((ArrayList)recordIDs).get(i);
		}
		sql+=")  order by lastModificationTime desc";
		printSql(sql);
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);		
	}
	
	public List<AppointmentDTO> getAllAppointment (boolean isFirstReload)
    {
		String sql = "SELECT * FROM " + tableName + " WHERE ";
		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by " + tableName + ".lastModificationTime desc";
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);	
    }
	
	public List<AppointmentDTO> getLastN (long erId, int n)
    {
		String sql = "SELECT * FROM " + tableName + " WHERE isCancelled = 0 and employee_record_id = " + erId;
		
		sql += " order by " + tableName + ".visit_date desc, available_time_slot desc limit " + n;
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);	
    }
	
	public KeyCountDTO getCount(ResultSet rs)
	{
		try
		{
			KeyCountDTO keyCountDTO = new KeyCountDTO();
			keyCountDTO.key = rs.getLong("visit_date");
			keyCountDTO.count = rs.getInt("count(id)");

			return keyCountDTO;
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
			return null;
		}
	}
	
	public List<KeyCountDTO> getLast7DayCount (String drUserName)
    {
		String sql = "SELECT visit_date, count(id) FROM appointment where visit_date >=" + TimeConverter.getNthDay(-7) + " and isDeleted = 0";
		if(!drUserName.equalsIgnoreCase(""))
		{
			sql+= " and dr_user_name = '" + drUserName + "'";
		}
		sql+= " group by visit_date order by visit_date asc";
		
		return ConnectionAndStatementUtil.getListOfT(sql,this::getCount);	
    }
	
	public List<KeyCountDTO> getLast7DayCountForReceptionist (String userName)
    {
		String sql = "SELECT insertion_date, count(id) FROM appointment where insertion_date >=" + TimeConverter.getNthDay(-7) + " and isDeleted = 0";
		if(!userName.equalsIgnoreCase(""))
		{
			sql+= " and inserted_by_user_name = '" + userName + "'";
		}
		sql+= " group by insertion_date order by visit_date asc";
		
		return ConnectionAndStatementUtil.getListOfT(sql,this::getCountForReceptionist);	
    }
	
	public List<YearMonthCount> getLast6MonthCountForReceptionist (String userName)
    {
		String sql = "SELECT \r\n" + 
				"    DATE_FORMAT(FROM_UNIXTIME(`insertion_date` / 1000),\r\n" + 
				"            '%Y-%m') AS ym,\r\n" + 
				"    COUNT(id)\r\n" + 
				"FROM\r\n" + 
				"    appointment\r\n" + 
				"WHERE\r\n" + 
				"    insertion_date >= " + TimeConverter.get1stDayOfNthtMonth(-6) + " AND isDeleted = 0 and inserted_by_user_name = '" + userName + "'\r\n" + 
				"GROUP BY ym\r\n";
		
		//System.out.println(sql);
		return ConnectionAndStatementUtil.getListOfT(sql,this::getYmCount);	
    }
	
	public YearMonthCount getYmCount(ResultSet rs)
	{
		try
		{
			YearMonthCount ymCount = new YearMonthCount();
			String ym = rs.getString("ym");
			ymCount.year = Integer.parseInt(ym.split("-")[0]);
			ymCount.month = Integer.parseInt(ym.split("-")[1]);
			ymCount.count = rs.getInt("count(id)");
			
			//System.out.println("year = " + ymCount.year + " month = " + ymCount.month + " count = " + ymCount.count);

			return ymCount;
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
			return null;
		}
	}
	
	public KeyCountDTO getCountForReceptionist(ResultSet rs)
	{
		try
		{
			KeyCountDTO keyCountDTO = new KeyCountDTO();
			keyCountDTO.key = rs.getLong("insertion_date");
			keyCountDTO.count = rs.getInt("count(id)");

			return keyCountDTO;
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
			return null;
		}
	}

	
	public List<AppointmentDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO)
	{
		return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
	}

	
	public List<AppointmentDTO> getDTOs(Hashtable  p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
			String filter, boolean tableHasJobCat)
	{
		String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat);
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);			
	}
	public boolean canSeeAllAppointments(UserDTO userDTO)
	{
        return userDTO.roleID == SessionConstants.DOCTOR_ROLE
                || userDTO.roleID == SessionConstants.PHYSIOTHERAPIST_ROLE
                || userDTO.roleID == SessionConstants.NURSE_ROLE
                || userDTO.roleID == SessionConstants.MEDICAL_RECEPTIONIST_ROLE
                || userDTO.roleID == SessionConstants.MEDICAL_ADMIN_ROLE
                || userDTO.roleID == SessionConstants.ADMIN_ROLE

                || userDTO.roleID == SessionConstants.PHARMACY_PERSON
                || userDTO.roleID == SessionConstants.INVENTORY_MANGER_ROLE;
	}
	
	public boolean canCreateThisAppointment(UserDTO userDTO, AppointmentDTO appointmentDTO)
	{
		System.out.println("userDTO.organogramID = " + userDTO.organogramID + " appointmentDTO.patientId = " + appointmentDTO.patientId);
		if(userDTO.roleID == SessionConstants.NURSE_ROLE
				|| userDTO.roleID == SessionConstants.DOCTOR_ROLE
				|| userDTO.roleID == SessionConstants.MEDICAL_RECEPTIONIST_ROLE
				|| userDTO.roleID == SessionConstants.MEDICAL_ADMIN_ROLE
				|| userDTO.roleID == SessionConstants.ADMIN_ROLE)
		{
			return true;
		}
		else return userDTO.organogramID == appointmentDTO.doctorId || userDTO.organogramID == appointmentDTO.patientId;
	}
	
	public boolean canChangeMyAppointments(UserDTO userDTO, AppointmentDTO appointmentDTO)
	{
        return (userDTO.roleID == SessionConstants.DOCTOR_ROLE && appointmentDTO.doctorId == userDTO.organogramID)
                || (userDTO.roleID == SessionConstants.PHYSIOTHERAPIST_ROLE && appointmentDTO.doctorId == userDTO.organogramID)
                || userDTO.roleID == SessionConstants.NURSE_ROLE
                || userDTO.roleID == SessionConstants.MEDICAL_RECEPTIONIST_ROLE
                || userDTO.roleID == SessionConstants.MEDICAL_ADMIN_ROLE
                || userDTO.roleID == SessionConstants.ADMIN_ROLE;
	}
	
	public boolean canChangeAnyAppointments(UserDTO userDTO)
	{
        return (userDTO.roleID == SessionConstants.DOCTOR_ROLE)
                || (userDTO.roleID == SessionConstants.PHYSIOTHERAPIST_ROLE)
                || userDTO.roleID == SessionConstants.NURSE_ROLE
                || userDTO.roleID == SessionConstants.MEDICAL_ADMIN_ROLE
                || userDTO.roleID == SessionConstants.MEDICAL_RECEPTIONIST_ROLE
                || userDTO.roleID == SessionConstants.ADMIN_ROLE;
	}
	
	public boolean canAddPatientMeasurement(UserDTO userDTO, long doctorId)
	{
        return userDTO.roleID == SessionConstants.ADMIN_ROLE
                || userDTO.roleID == SessionConstants.MEDICAL_ADMIN_ROLE
                || userDTO.roleID == SessionConstants.NURSE_ROLE
                || userDTO.organogramID == doctorId;
	}
	public String getSqlWithSearchCriteriaForNormalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
			UserDTO userDTO, String filter, boolean tableHasJobCat)
    {
		boolean viewAll = false;
		
		if(p_searchCriteria != null && p_searchCriteria.get("ViewAll") != null)
		{
			System.out.println("ViewAll = " + p_searchCriteria.get("ViewAll"));
			viewAll = true;
		}
		else
		{
			System.out.println("ViewAll is null ");
		}
		
		String sql = "SELECT ";
		if(category == GETIDS)
		{
			sql += " " + tableName + ".ID as ID ";
		}
		else if(category == GETCOUNT)
		{
			sql += " count( " + tableName + ".ID) as countID ";
		}
		else if(category == GETDTOS)
		{
			sql += " distinct " + tableName + ".* ";
		}
		sql += "FROM " + tableName + " ";
		sql += joinSQL;
		

		
		String AnyfieldSql = "";
		String AllFieldSql = "";
		
		if(p_searchCriteria != null)
		{
						
			
			Enumeration names = p_searchCriteria.keys();
			String str, value;
			
			AnyfieldSql = "(";
			
			if(p_searchCriteria.get("AnyField")!= null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase(""))
			{
				AnyfieldSql+= "(" + tableName + ".search_column like '%" + p_searchCriteria.get("AnyField").toString() + "%'"
						 + "or employee_record_id = " 
						+ WorkflowController.getEmployeeRecordsIdFromUserName(Utils.getDigitEnglishFromBangla(p_searchCriteria.get("AnyField").toString())) + ")";			
			}
			AnyfieldSql += ")";
			System.out.println("AnyfieldSql = " + AnyfieldSql);
			
			AllFieldSql = "(";
			int i = 0;
			while(names.hasMoreElements())
			{				
				str = (String) names.nextElement();
				value = (String)p_searchCriteria.get(str);
		        System.out.println(str + ": " + value);
				if(value != null && !value.equalsIgnoreCase("") && (
						 str.equals("visit_date_start")
						|| str.equals("visit_date_end")
						|| str.equals("doctor_id")
						|| str.equals("patient_name")
						|| str.equals("phone_number")
						|| str.equals("dept_name")
						|| str.equals("employee_user_name")
						|| str.equals("todays_appointments")
						|| str.equals("sl")
				)
						
				)
				{
					if(p_searchCriteria.get(str).equals("any"))
					{
						continue;
					}

					if( i > 0)
					{
						AllFieldSql+= " AND  ";
					}
					
					 if(str.equals("visit_date_start"))
					{
						AllFieldSql += "" + tableName + ".visit_date >= " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("visit_date_end"))
					{
						AllFieldSql += "" + tableName + ".visit_date <= " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("doctor_id"))
					{
						AllFieldSql += "" + tableName + ".doctor_id = " + p_searchCriteria.get(str);
						i ++;
					}
					
					else if(str.equals("patient_name"))
					{
						AllFieldSql += "" + tableName + ".patient_name like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("employee_user_name"))
					{
						AllFieldSql += "" + tableName + ".employee_record_id = " + WorkflowController.getEmployeeRecordsIdFromUserName((String)p_searchCriteria.get(str)) ;
						i ++;
					}
					else if(str.equals("phone_number"))
					{
						AllFieldSql += " (" + tableName + ".phone_number = '" + p_searchCriteria.get(str) + "' ";
						AllFieldSql += " or " + tableName + ".alternate_phone = '" + p_searchCriteria.get(str) + "') ";
						i ++;
					}
					else if(str.equals("dept_name"))
					{
						AllFieldSql += "" + tableName + ".dept_name like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("todays_appointments"))
					{
						AllFieldSql += "" + tableName + ".visit_date =" + TimeConverter.getToday() ;
						i ++;
					}
					else if(str.equals("sl"))
					{
						AllFieldSql += "" + tableName + ".sl = '" + p_searchCriteria.get(str) + "'" ;
						i ++;
					}
					
					
				}
			}
			
			AllFieldSql += ")";
			System.out.println("AllFieldSql = " + AllFieldSql);
			
			
		}
		
		
		sql += " WHERE ";
		
		sql += " (" + tableName + ".isDeleted = 0 ";			
		sql += ")";
		
		
		if(!filter.equalsIgnoreCase(""))
		{
			sql += " and " + filter + " ";
		}
		
		if(!canSeeAllAppointments(userDTO))
		{
			sql += " and employee_user_name = '" + userDTO.userName + "' ";
		}
		
		if(!AnyfieldSql.equals("()") && !AnyfieldSql.equals(""))
		{
			sql += " AND " + AnyfieldSql;
			
		}
		if(!AllFieldSql.equals("()") && !AllFieldSql.equals(""))
		{			
			sql += " AND " + AllFieldSql;
		}
		
	
			
		sql += " order by " + tableName + ".visit_date desc, available_time_slot asc ";

		printSql(sql);
		
		if(limit >= 0)
		{
			sql += " limit " + limit;
		}
		if(offset >= 0)
		{
			sql += " offset " + offset;
		}
		
		System.out.println("-------------- sql = " + sql);
		
		return sql;
    }
	
	public String getSqlWithSearchCriteria(Hashtable p_searchCriteria, int limit, int offset, int category,
			boolean isPermanentTable, UserDTO userDTO, String filter, boolean tableHasJobCat)
    {		
		return getSqlWithSearchCriteriaForNormalSearch(p_searchCriteria, limit, offset, category, userDTO, filter, tableHasJobCat);	
    }
				
}
	