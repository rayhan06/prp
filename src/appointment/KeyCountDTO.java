package appointment;

public class KeyCountDTO {
	
	public long key = 0;
	public long key2 = 0;
	public String keyStr = "";
	public int count = 0;
	public int count2 = 0;
	public int count3 = 0;
	public double cost = 0;
	public String nameEn = "";
	public String nameBn = "";
	public boolean exists = false;
	
	public KeyCountDTO()
	{
		
	}
	
	public KeyCountDTO(long key, String nameEn, String nameBn)
	{
		this.key = key;
		this.nameEn = nameEn;
		this.nameBn = nameBn;
	}
	
	public KeyCountDTO(long key, String keyStr, int count)
	{
		this.key = key;
		this.keyStr = keyStr;
		this.count = count;
	}
	
	public KeyCountDTO(long key, String nameEn, String nameBn, int count, double cost)
	{
		this.key = key;
		this.nameEn = nameEn;
		this.nameBn = nameBn;
		this.count = count;
		this.cost = cost;
	}
	
	public KeyCountDTO(String nameEn, String nameBn, double cost)
	{
		this.nameEn = nameEn;
		this.nameBn = nameBn;
		this.cost = cost;
	}
	
	public KeyCountDTO(String keyStr, double cost)
	{
		this.keyStr = keyStr;
		this.cost = cost;
	}
	
	public KeyCountDTO(long key, double cost)
	{
		this.key = key;
		this.cost = cost;
	}
}
