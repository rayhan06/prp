package vm_requisition_report;


import java.io.IOException;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import language.LC;
import language.LM;

import login.LoginDTO;
import permission.MenuConstants;

import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;

import util.ReportRequestHandler;
import pbReport.*;






@WebServlet("/Vm_requisition_report_Servlet")
public class Vm_requisition_report_Servlet  extends HttpServlet {
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	String[][] Criteria =
	{
		{"criteria","tr","start_date",">=","","long","","",Long.MIN_VALUE + "","startDate", LC.HM_START_DATE + ""},
		//{"criteria","tr","start_date","<=","AND","long","","",Long.MAX_VALUE + "","startDate", LC.HM_END_DATE + ""},
		//{"criteria","tr","end_date",">=","AND","long","","",Long.MIN_VALUE + "","endDate", LC.HM_END_DATE + ""},
		{"criteria","tr","end_date","<=","AND","long","","",Long.MAX_VALUE + "","endDate", LC.HM_END_DATE + ""},
		{"criteria","tr","requester_emp_id","=","AND","String","","","any","requesterEmpId", LC.VM_REQUISITION_REPORT_WHERE_REQUESTEREMPID + ""},
		//{"criteria","tr","requester_name_bn","=","AND","String","","","any","requesterEmpIdBangla", LC.VM_REQUISITION_REPORT_SELECT_REQUESTERNAMEBN + ""},
		{"criteria","tr","given_vehicle_type","=","AND","int","","","any","vehicleTypeCat", LC.VM_REQUISITION_REPORT_WHERE_VEHICLETYPECAT + ""},
		{"criteria","tr","vehicle_requisition_purpose_cat","=","AND","int","","","any","vehicleRequisitionPurposeCat", LC.VM_REQUISITION_REPORT_WHERE_VEHICLEREQUISITIONPURPOSECAT + ""},
		{"criteria","tr","status","=","AND","String","","","any","status", LC.VM_REQUISITION_REPORT_WHERE_STATUS + ""},
		{"criteria","tr","isDeleted","=","AND","String","","","0","isDeleted", LC.VM_REQUISITION_REPORT_WHERE_ISDELETED + ""}
	};

	String[][] Display =
	{
		{"display","tr","requester_name_en","text",""},
		{"display","tr","requester_name_bn","text",""},
		{"display","tr","start_date","date",""},
		{"display","tr","end_date","date",""},
		{"display","tr","given_vehicle_type","given_vehicle_type_check",""},
		{"display","tr","given_vehicle_id","reg_no",""},
		{"display","tr","vehicle_requisition_purpose_cat","cat",""},
		{"display","tr","status","vm_requisition_status_check",""}
	};

	String GroupBy = "";
	String OrderBY = "";

	ReportRequestHandler reportRequestHandler;

	public Vm_requisition_report_Servlet(){

	}

	private final ReportService reportService = new ReportService();

	private String sql;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{

		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String language = "english";
		if(userDTO!= null && userDTO.languageID == SessionConstants.BANGLA)
		{
			language = "bangla";
		}

		String actionType = request.getParameter("actionType");

		System.out.println("In ssservlet doget, actiontype = " + actionType);

		sql = "vm_requisition tr";

		Display[0][4] = LM.getText(LC.VM_REQUISITION_REPORT_SELECT_REQUESTERNAMEEN, loginDTO);
		Display[1][4] = LM.getText(LC.VM_REQUISITION_REPORT_SELECT_REQUESTERNAMEBN, loginDTO);
		Display[2][4] = LM.getText(LC.VM_REQUISITION_REPORT_SELECT_STARTDATE, loginDTO);
		Display[3][4] = LM.getText(LC.VM_REQUISITION_REPORT_SELECT_ENDDATE, loginDTO);
		Display[4][4] = LM.getText(LC.VM_REQUISITION_REPORT_SELECT_GIVENVEHICLETYPE, loginDTO);
		Display[5][4] = LM.getText(LC.VM_REQUISITION_REPORT_SELECT_GIVENVEHICLEID, loginDTO);
		Display[6][4] = LM.getText(LC.VM_REQUISITION_REPORT_SELECT_VEHICLEREQUISITIONPURPOSECAT, loginDTO);
		Display[7][4] = LM.getText(LC.VM_REQUISITION_REPORT_SELECT_STATUS, loginDTO);


		String reportName = LM.getText(LC.VM_REQUISITION_REPORT_OTHER_VM_REQUISITION_REPORT, loginDTO);

		reportRequestHandler = new ReportRequestHandler(null,
				Criteria, Display, GroupBy, OrderBY, sql,
				reportService);


		reportRequestHandler.handleReportGet(request, response, userDTO, "vm_requisition_report",
				MenuConstants.VM_REQUISITION_REPORT_DETAILS, language, reportName, "vm_requisition_report");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		doGet(request, response);
	}
}
