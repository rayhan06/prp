/*
package office_rest;

import login.LoginDTO;
import org.apache.log4j.Logger;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLDecoder;

@WebServlet("/OfficeDuplicationServlet")
@MultipartConfig
public class OfficeDuplicationServlet extends HttpServlet {

    public static Logger logger = Logger.getLogger(OfficeDuplicationServlet.class);

    public OfficeDuplicationServlet() {
        super();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("In doget request = " + request);
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getInstance().getUserDtoByUserId(loginDTO.userID);
        try {
            String actionType = request.getParameter("actionType");
            if (actionType.equals("checkDuplication")) {

                response.setContentType("application/json");
                String[] parameters = URLDecoder.decode(request.getQueryString(), "UTF-8").split("&");

                PrintWriter out = response.getWriter();
                boolean isDuplicate = new OfficeDuplication().checkDuplicationValue(parameters);
                out.print(isDuplicate);
                out.flush();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
*/
