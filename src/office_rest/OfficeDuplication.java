package office_rest;

import office_layers.Office_layersDTO;
import office_layers.Office_layersRepository;
import office_ministries.Office_ministriesDTO;
import office_ministries.Office_ministriesRepository;
import office_origin_unit_organograms.Office_origin_unit_organogramsDTO;
import office_origin_unit_organograms.Office_origin_unit_organogramsRepository;
import office_origin_units.Office_origin_unitsDTO;
import office_origin_units.Office_origin_unitsRepository;
import office_origins.Office_originsDTO;
import office_origins.Office_originsRepository;
import office_types.Office_typesDTO;
import office_types.Office_typesRepository;
import office_units.Office_unitsDTO;
import office_units.Office_unitsRepository;
import offices.OfficesDTO;
import offices.OfficesRepository;

import java.lang.reflect.Field;
import java.util.List;

public class OfficeDuplication {

    public boolean checkDuplicationValue(String[] parameters) {
        String id = null, table = null, parent = null, parent_id = null, key = null, value = null;

        for (String parameter : parameters) {
            String[] nameValue = parameter.split("=");

            if (nameValue[0].equals("iD")) id = nameValue[1];
            else if (nameValue[0].equals("table")) table = nameValue[1];
            else if (nameValue[0].equals("parent")) parent = nameValue[1];
            else if (nameValue[0].equals("parent_id")) parent_id = nameValue[1];
            else {
                key = nameValue[0];
                value = nameValue[1];
            }
        }

        if (("office_types").equals(table)) {
            List<Office_typesDTO> dataList = Office_typesRepository.getInstance().getOffice_typesList();
            return checkDuplicationCommon(id, table, parent, parent_id, key, value, Office_typesDTO.class, dataList);
        } else if (("office_ministries").equals(table)) {
            List<Office_ministriesDTO> dataList = Office_ministriesRepository.getInstance().getOffice_ministriesList();
            return checkDuplicationCommon(id, table, parent, parent_id, key, value, Office_ministriesDTO.class, dataList);
        } else if (("office_layers").equals(table)) {
            List<Office_layersDTO> dataList = Office_layersRepository.getInstance().getOffice_layersList();
            return checkDuplicationCommon(id, table, parent, parent_id, key, value, Office_layersDTO.class, dataList);
        } else if (("office_origins").equals(table)) {
            List<Office_originsDTO> dataList = Office_originsRepository.getInstance().getOffice_originsList();
            return checkDuplicationCommon(id, table, parent, parent_id, key, value, Office_originsDTO.class, dataList);
        } else if (("office_origin_units").equals(table)) {
            List<Office_origin_unitsDTO> dataList = Office_origin_unitsRepository.getInstance().getOffice_origin_unitsList();
            return checkDuplicationCommon(id, table, parent, parent_id, key, value, Office_origin_unitsDTO.class, dataList);
        } else if (("office_origin_unit_organograms").equals(table)) {
            List<Office_origin_unit_organogramsDTO> dataList = Office_origin_unit_organogramsRepository.getInstance().getOffice_origin_unit_organogramsList();
            return checkDuplicationCommon(id, table, parent, parent_id, key, value, Office_origin_unit_organogramsDTO.class, dataList);
        } else if (("offices").equals(table)) {
            List<OfficesDTO> dataList = OfficesRepository.getInstance().getOfficesList();
            return checkDuplicationCommon(id, table, parent, parent_id, key, value, OfficesDTO.class, dataList);
        } else if (("office_units").equals(table)) {
            List<Office_unitsDTO> dataList = Office_unitsRepository.getInstance().getOffice_unitsList();
            return checkDuplicationCommon(id, table, parent, parent_id, key, value, Office_unitsDTO.class, dataList);
        }
        return false;
    }

    private boolean checkDuplicationCommon(String id, String table, String parent, String parent_id, String key, String value, Class<?> clazz, List<?> dataList) {

        for (Object s : dataList) {
            Field field;
            Object val = null;
            try {
                field = clazz.getField("iD");
                val = field.get(s);
            } catch (NoSuchFieldException | IllegalAccessException ignored) {
            }

            if (id != null && id.equals(String.valueOf(val))) continue;
            if (parent != null && parent_id != null) {
                try {
                    field = clazz.getField(parent);
                    val = field.get(s);
                    if (parent_id.equals(String.valueOf(val))) {
                        if (check(clazz, s, key, value)) return true;
                    }
                } catch (NoSuchFieldException | IllegalAccessException ignored) {
                }
            } else {
                if (check(clazz, s, key, value)) return true;
            }
        }
        return false;
    }

    private boolean check(Class<?> clazz, Object s, String key, String value) {
        try {
            Field field = clazz.getField(key);
            Object val = field.get(s);
            System.out.println(value + ' ' + val);
            System.out.println(value.equals(val));

            return value.equals(String.valueOf(val));
        } catch (NoSuchFieldException | IllegalAccessException ignored) {
        }
        return false;
    }
}
