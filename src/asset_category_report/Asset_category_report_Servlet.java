package asset_category_report;


import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import asset_model.AssetAssigneeDAO;
import asset_model.AssetAssigneeDTO;
import language.LC;
import language.LM;

import login.LoginDTO;
import permission.MenuConstants;

import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;

import util.ReportRequestHandler;
import pbReport.*;






@WebServlet("/Asset_category_report_Servlet")
public class Asset_category_report_Servlet  extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	String[][] Criteria =
	{
		{"criteria","","asset_category_type","=","","int","","","any","assetCategoryType", LC.ASSET_CATEGORY_REPORT_SELECT_ASSETCATEGORYTYPE + ""}	
	};
	
	String[][] Display =
	{	
		{"display","","asset_category_type","type",""},		
		{"display","","assigned_qunatity + remaining_quantity + condemned","int",""},		
		{"display","","assigned_qunatity","int",""},		
		{"display","","remaining_quantity","int",""},
		{"display","","condemned","int",""}
	};
	
	String GroupBy = "";
	String OrderBY = "";
	
	ReportRequestHandler reportRequestHandler;
	
	public Asset_category_report_Servlet(){

	}
	
	private final ReportService reportService = new ReportService();
	
	private String sql;
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String language = "english";
		if(userDTO!= null && userDTO.languageID == SessionConstants.BANGLA)
		{
			language = "bangla";
		}
				
		String actionType = request.getParameter("actionType");

		System.out.println("In ssservlet doget, actiontype = " + actionType);
		
		sql = "(SELECT \r\n" + 
				"    asset_model.asset_category_type as asset_category_type,\r\n" + 
				"    SUM(CASE\r\n" + 
				"        WHEN assignment_status = " + AssetAssigneeDTO.ASSIGNED + " THEN 1\r\n" + 
				"        ELSE 0\r\n" + 
				"    END) AS assigned_qunatity,\r\n" + 
				"    SUM(CASE\r\n" + 
				"        WHEN assignment_status = " + AssetAssigneeDTO.NOT_ASSIGNED + " THEN 1\r\n" + 
				"        ELSE 0\r\n" + 
				"    END) AS remaining_quantity,\r\n" + 
				"    SUM(CASE\r\n" + 
				"        WHEN assignment_status = " + AssetAssigneeDTO.CONDEMNED + " THEN 1\r\n" + 
				"        ELSE 0\r\n" + 
				"    END) AS condemned\r\n" + 
				"FROM\r\n" + 
				"    asset_model left join asset_assignee on asset_model.id = asset_assignee.asset_model_id\r\n" + 
				"GROUP BY asset_category_type) AS temp";

		Display[0][4] = LM.getText(LC.ASSET_CATEGORY_REPORT_SELECT_ASSETCATEGORYTYPE, loginDTO);
		Display[1][4] = LM.getText(LC.ASSET_CATEGORY_REPORT_SELECT_TOTALQUANTITY, loginDTO);
		Display[2][4] = LM.getText(LC.HM_ASSIGNED_QUANTITY, loginDTO);
		Display[3][4] = LM.getText(LC.ASSET_CATEGORY_REPORT_SELECT_REMAININGQUANTITY, loginDTO);
		Display[4][4] = language.equalsIgnoreCase("english")?"Condemned":"কনডেমকৃত";
		
		String reportName = LM.getText(LC.ASSET_CATEGORY_REPORT_OTHER_ASSET_CATEGORY_REPORT, loginDTO);
		
		reportRequestHandler = new ReportRequestHandler(null,
				Criteria, Display, GroupBy, OrderBY, sql,
				reportService);
		
		reportRequestHandler.clientSideDataFormatting = new ArrayList<Pair<Integer, Integer>>();
		reportRequestHandler.clientSideDataFormatting.add(new ImmutablePair<Integer, Integer>(1, ReportRequestHandler.RIGHT_ALIGN_INT));
		reportRequestHandler.clientSideDataFormatting.add(new ImmutablePair<Integer, Integer>(2, ReportRequestHandler.RIGHT_ALIGN_INT));
		reportRequestHandler.clientSideDataFormatting.add(new ImmutablePair<Integer, Integer>(3, ReportRequestHandler.RIGHT_ALIGN_INT));
		reportRequestHandler.clientSideDataFormatting.add(new ImmutablePair<Integer, Integer>(4, ReportRequestHandler.RIGHT_ALIGN_INT));

		
		reportRequestHandler.handleReportGet(request, response, userDTO, "asset_category_report",
				MenuConstants.ASSET_CATEGORY_REPORT_DETAILS, language, reportName, "asset_category_report");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		doGet(request, response);
	}
}
