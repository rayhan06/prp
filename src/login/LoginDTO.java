package login;

public class LoginDTO {
	//only different fields will be present + userID. We shall use UserID to get other user info to avoid duplicacy
	public long userID = -1;
	public String loginSourceIP;
	public int isOisf = 0; // 1 if oisf user
	public long organogramId = -2; // 1 if oisf user
	public String errorMsg =null;
	public long roleId = -1;

	@Override
	public String toString() {
		return "LoginDTO [" + ", userID=" + userID 
				+ ", loginSourceIP=" + loginSourceIP
				+"]";
	}
}
