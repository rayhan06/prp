package login;

//import nl.basjes.parse.useragent.UserAgent;
//import nl.basjes.parse.useragent.UserAgentAnalyzer;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.util.HashMap;
//import javax.servlet.http.HttpServletRequest;
//import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

public class DeviceInfoUtil {
    //static UserAgentAnalyzer uaa;
    /*static
    {
        uaa = UserAgentAnalyzer
                .newBuilder()
                .hideMatcherLoadStats()
                //.withCache(10)
                .build();
//             .withField("DeviceName")
//            .withField("AgentNameVersionMajor")
//            .withField("OperatingSystemNameVersionMajor")
    }*/
    private static final String[] IP_HEADER_CANDIDATES = {
            "X-Forwarded-For",
            "Proxy-Client-IP",
            "WL-Proxy-Client-IP",
            "HTTP_X_FORWARDED_FOR",
            "HTTP_X_FORWARDED",
            "HTTP_X_CLUSTER_CLIENT_IP",
            "HTTP_CLIENT_IP",
            "HTTP_FORWARDED_FOR",
            "HTTP_FORWARDED",
            "HTTP_VIA",
            "REMOTE_ADDR"
    };

//    public static InetAddress getClientIpAddr(HttpServletRequest request) {
//        String ip = request.getHeader("X-Forwarded-For");
//        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
//            ip = request.getHeader("Proxy-Client-IP");
//        }
//        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
//            ip = request.getHeader("WL-Proxy-Client-IP");
//        }
//        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
//            ip = request.getHeader("HTTP_CLIENT_IP");
//        }
//        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
//            ip = request.getHeader("HTTP_X_FORWARDED_FOR");
//        }
//        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
//            ip = request.getRemoteAddr();
//        }
//        try {
//            return InetAddress.getByName(ip);
//        } catch (UnknownHostException e) {
//            return null;
//        }
//    }

    public static String getClientIp(HttpServletRequest request) {
    	/*
        String ip = request.getHeader("X-Forwarded-For");
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("WL-Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("HTTP_CLIENT_IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("HTTP_X_FORWARDED_FOR");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getRemoteAddr();
        }
        return ip;*/
    	return "";
    }



    public static String getClientIpAddressIfServletRequestExist() {
    	return "127.0.0.1";

        /*if (RequestContextHolder.getRequestAttributes() == null) {
            return "0.0.0.0";
        }

        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        for (String header: IP_HEADER_CANDIDATES) {
            String ipList = request.getHeader(header);
            if (ipList != null && ipList.length() != 0 && !"unknown".equalsIgnoreCase(ipList)) {

                System.out.println("IP Address (getClientIpAddressIfServletRequestExist())--> "+ipList);
                return ipList.split(",")[0];
            }
        }

        return request.getRemoteAddr();*/
    }

    public Map<String,String> getInfo(HttpServletRequest request)
    {

        /*String userAgent = request.getHeader("User-Agent");
        System.out.println(userAgent);
        UserAgent agent = uaa.parse(userAgent);
        Map<String,String> deviceInfo = new HashMap<>();
        for (String fieldName: agent.getAvailableFieldNamesSorted()) {
            System.out.println(fieldName + " = " + agent.getValue(fieldName));
            deviceInfo.put(fieldName,agent.getValue(fieldName));

        }

        System.out.println("IP Address (request.getRemoteAddr())--> "+request.getRemoteAddr());
        deviceInfo.put("ip",getClientIpAddressIfServletRequestExist());
        return deviceInfo;*/
    	return new HashMap<String,String>();
    }
}

