package login;

import common.ConnectionAndStatementUtil;
import org.apache.log4j.Logger;

import java.sql.SQLException;
import java.util.List;

public class IgnoreLoginDAO {
	
	private static final String sqlGetQuery = "SELECT * FROM ignorelogin where isDeleted = 0";

	private static final Logger logger = Logger.getLogger(IgnoreLoginDAO.class);

	private final List<String> listOfIgnoreLoginURL;

	private IgnoreLoginDAO(){
		listOfIgnoreLoginURL = ConnectionAndStatementUtil.getListOfT(sqlGetQuery,rs->{
			try{
				return rs.getString("ignore_url");
			}catch (SQLException ex){
				logger.error(ex);
				ex.printStackTrace();
				return null;
			}
		});
	}

	private static class IgnoreLoginDAOLazyLoader{
		static final IgnoreLoginDAO INSTANCE = new IgnoreLoginDAO();
	}

	public static IgnoreLoginDAO getInstance(){
		return IgnoreLoginDAOLazyLoader.INSTANCE;
	}

	public List<String> getIgnoreLoginList(){
		return listOfIgnoreLoginURL;
	}
}