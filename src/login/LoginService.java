package login;

import common.CommonActionStatusDTO;
import config.GlobalConfigConstants;
import config.GlobalConfigurationRepository;
import employee_office_report.InChargeLevelEnum;
import employee_offices.EmployeeOfficeDTO;
import employee_offices.EmployeeOfficeRepository;
import employee_records.Employee_recordsDTO;
import employee_records.Employee_recordsRepository;
import forgetPassword.VerificationConstants;
import forgetPassword.VerificationDTO;
import forgetPassword.VerificationService;
import ipRestriction.IPRestrictionDTO;
import ipRestriction.IPRestrictionService;
import nl.captcha.Captcha;
import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import pb.Utils;
import pb_notifications.Pb_notificationsDAO;
import sessionmanager.SessionConstants;
import user.LoginSessionListener;
import user.UserDTO;
import user.UserRepository;
import util.CurrentTimeFactory;
import util.OTPResource;
import util.PasswordUtil;
import util.ServiceDAOFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.List;

public class LoginService {
    Logger logger = Logger.getLogger(getClass());
    IPRestrictionService ipRestrictionService = ServiceDAOFactory.getService(IPRestrictionService.class);

    VerificationService verificationService = ServiceDAOFactory.getService(VerificationService.class);

    public LoginDTO validateLogin(String username, String password, LoginDTO loginDTO) throws Exception {
        if (!Utils.isValidUserName(username)) {
            return null;
        }
        UserDTO userDTO = UserRepository.getUserDTOByUserName(username);
        if (userDTO == null || !userDTO.active) {
            return null;
        }
        String encryptedPass = PasswordUtil.getInstance().encrypt(password);
        logger.debug("encryptedPass " + encryptedPass + " udto.getPassword() " + userDTO.password);

        if (!userDTO.password.equals(encryptedPass)) {
            return null;
        }

        loginDTO.userID = userDTO.ID;
        LoginDTO loginDTO2 = new LoginDTO();
        loginDTO2.loginSourceIP = loginDTO.loginSourceIP;
        loginDTO2.userID = loginDTO.userID;
        loginDTO2.isOisf = 0;
        return loginDTO2;
    }

    public LoginDTO validateLogin2(String username, String password, LoginDTO loginDTO) throws Exception {
        LoginDTO loginDTO2 = new LoginDTO();

        if (!Utils.isValidUserName(username)) {
            loginDTO2.errorMsg ="ইউজারনেম ইনভেলিড ।";
            return loginDTO2;
        }

        UserDTO userDTO = UserRepository.getUserDTOByUserNameFromUsersTable(username);
        if (userDTO == null) {
            loginDTO2.errorMsg ="ইউজারনেম ভুল ।";
            return loginDTO2;
        }

        loginDTO.userID = userDTO.ID;

        if(!userDTO.active){
            loginDTO2.errorMsg ="ইউজার সক্রিয় নয় ।";
            return loginDTO2;
        }

        String encryptedPass = PasswordUtil.getInstance().encrypt(password);
        logger.debug("encryptedPass " + encryptedPass + " udto.getPassword() " + userDTO.password);

        if (!userDTO.password.equals(encryptedPass)) {
            loginDTO2.errorMsg ="পাসওয়ার্ড ভুল ।";
            return loginDTO2;
        }
        loginDTO.userID = userDTO.ID;
        loginDTO2.loginSourceIP = loginDTO.loginSourceIP;
        loginDTO2.userID = loginDTO.userID;
        loginDTO2.isOisf = 0;

        if(username.equals("superadmin")){
            return loginDTO2;
        }

        List<EmployeeOfficeDTO> list = EmployeeOfficeRepository.getInstance().getByEmployeeRecordId(userDTO.employee_record_id);

        if(list == null || list.size() == 0){
            loginDTO2.errorMsg ="আপনি কোন অফিসে নিযুক্ত নন ।";
            return loginDTO2;
        }

        EmployeeOfficeDTO primaryOffice = list.stream()
                .filter(e->e.inchargeLabel.equals(InChargeLevelEnum.ROUTINE_RESPONSIBILITY.getValue()))
                .findAny()
                .orElse(null);

        if(primaryOffice == null){
            loginDTO2.errorMsg ="আপনি কোন রুটিন দায়িত্বে নিযুক্ত নন ।";
            return loginDTO2;
        }
        return loginDTO2;
    }

    public boolean capchaMatched(HttpServletRequest request) throws UnsupportedEncodingException {
        if (Integer.parseInt(GlobalConfigurationRepository.getGlobalConfigDTOByID(GlobalConfigConstants.CAPTCHA_IN_LOGIN_ENABLED).value) == 1) {
            Captcha captcha = (Captcha) request.getSession().getAttribute(Captcha.NAME);
            request.setCharacterEncoding("UTF-8");
            String answer = request.getParameter("answer");
            logger.debug("captcha " + captcha.getAnswer() + " answer " + answer);

            if (!captcha.isCorrect(answer)) {
                new CommonActionStatusDTO().setErrorMessage("Captcha doesn't match", false, request);
                return false;
            }
        }
        return true;
    }

    public String getLastRequestedUrl(HttpServletRequest request) {
        Object lastRequestedUrlObj = request.getSession(true).getAttribute("lastRequestedURL");
        String lastRequestedURL = null;
        if (lastRequestedUrlObj != null) {
            lastRequestedURL = lastRequestedUrlObj.toString();
            if (lastRequestedURL.contains("ForgetPassword.do") || lastRequestedURL.contains("terms-and-conditions.jsp") || lastRequestedURL.contains("otpVerifier.jsp") || lastRequestedURL.contains("verify-otp.do")) {
                lastRequestedURL = null;
            }
        }
        return lastRequestedURL;
    }

    public boolean needOTPCheck(UserDTO loginUserDTO, HttpServletRequest request) throws Exception {
        if (!getOTPApplicable()) return false;

        request.getSession(true).setAttribute(VerificationConstants.CACHE_OBJECT, loginUserDTO);
        request.getSession(true).setAttribute(VerificationConstants.CACHE_OTP_VERIFICATION_URL, "VerificationServlet?actionType=verify-otp");
        request.getSession(true).setAttribute(VerificationConstants.CACHE_FORWARD, "home/index.jsp");

        OTPResource otpResource = verificationService.getOTPResourceByUserDTO(loginUserDTO);
        VerificationDTO verificationDTO = new VerificationDTO();
        verificationDTO.setUsername(loginUserDTO.userName);
        verificationDTO.setExpirationTime(CurrentTimeFactory.getCurrentTime() + VerificationConstants.DELAY_TIME_IN_MILLIS);
        verificationDTO.setAuthPurpose(VerificationConstants.VERIFICATION_TYPE_FORGET_PASSWORD);

        verificationService.sendOTP(loginUserDTO);

        request.getSession(true).setAttribute(VerificationConstants.CACHE_PHONE_NUMBER, otpResource.getPhoneNumber());
        request.getSession(true).setAttribute(VerificationConstants.CACHE_EMAIL, otpResource.getEmail());
        new CommonActionStatusDTO().setSuccessMessage(verificationService.getOTPSendMessage(otpResource, loginUserDTO), false, request);

        return true;
    }

    public boolean getOTPApplicable() {
        return Integer.parseInt(GlobalConfigurationRepository.getGlobalConfigDTOByID(GlobalConfigConstants.OTP_IN_LOGIN_ENABLED_FOR_ADMIN).value) == 1;
    }

    public String processInvalidLogin(String ip, String username, HttpServletRequest request) throws Exception {
        logger.error("Invalid Login");
        addIPHit(ip, username);
        IPRestrictionDTO ipRestrictionDTO = ipRestrictionService.getIPRestrictionDTOByIPAndUser(ip, username);

        new CommonActionStatusDTO().setErrorMessage("ভুল ইউজারনেম অথবা পাসওয়ার্ড।" + getIPRestrictionMessage(ipRestrictionDTO), false, request);
        return "actionError";
    }

    public String processInvalidLogin(String ip, String username,String errorMsg, HttpServletRequest request) throws Exception {
        logger.error("Invalid Login");
        addIPHit(ip, username);
        //IPRestrictionDTO ipRestrictionDTO = ipRestrictionService.getIPRestrictionDTOByIPAndUser(ip, username);

        new CommonActionStatusDTO().setErrorMessage(errorMsg , false, request);
        return "actionError";
    }
    
    public String getHost(HttpServletRequest request)
    {
    	StringBuffer url = request.getRequestURL();
    	String uri = request.getRequestURI();
    	int idx = (((uri != null) && (uri.length() > 0)) ? url.indexOf(uri) : url.length());
    	String host = url.substring(0, idx); //base url
    	idx = host.indexOf("://");
    	if(idx > 0) {
    	  host = host.substring(idx); //remove scheme if present
    	}
    	if(host.startsWith("://"))
    	{
    		host = "https" + host;
    	}
    	return host;
    }

    public void processValidLogin(LoginDTO loginDTO, String username, HttpServletRequest request) throws Exception {
        logger.debug("Processing valid Login " + loginDTO);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null || !userDTO.active) {
            throw new Exception("UserDTO is null");
        }
        LoginSessionListener.add(userDTO);
        Pb_notificationsDAO.urlPrefix = getHost(request);
        ipRestrictionService.deleteIPRestrictionDTOByIPAndUser(loginDTO.loginSourceIP, username);
        HttpSession session = request.getSession(true);
        session.setAttribute(SessionConstants.USER_LOGIN, loginDTO);
        Employee_recordsDTO employee_recordsDTO;
        try {
            employee_recordsDTO = Employee_recordsRepository.getInstance().getById(userDTO.employee_record_id);
            if (employee_recordsDTO == null || employee_recordsDTO.photo == null || employee_recordsDTO.photo.length == 0) {
                employee_recordsDTO = Employee_recordsRepository.getInstance().getById(1L);
            }
            if (employee_recordsDTO != null) {
                byte[] encodeBase64Photo = Base64.encodeBase64(employee_recordsDTO.photo);
                if (encodeBase64Photo != null) {
                    session.setAttribute(SessionConstants.USER_PP, encodeBase64Photo);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void addIPHit(String ip, String username) throws Exception {
        IPRestrictionDTO ipRestrictionDTO = new IPRestrictionDTO();
        ipRestrictionDTO.IP = ip;
        ipRestrictionDTO.username = username;
        ipRestrictionService.updateOrInsert(ipRestrictionDTO);
    }

    public boolean hasRestriction(LoginDTO loginDTO, String username, HttpServletRequest request) throws Exception {
        IPRestrictionDTO ipRestrictionDTO = ipRestrictionService.getIPRestrictionDTOByIPAndUser(loginDTO.loginSourceIP, username);
        if (ipRestrictionDTO != null) {
            if (ipRestrictionDTO.nextHitTimeAfter > System.currentTimeMillis()) {
                addIPHit(loginDTO.loginSourceIP, username);
                new CommonActionStatusDTO().setErrorMessage(getIPRestrictionMessage(ipRestrictionDTO), false, request);
                return true;
            }
        }
        return false;
    }

    public String getBlockMessage(IPRestrictionDTO ipRestrictionDTO) {
        return "অস্বাভাবিক রিকোয়েস্টের জন্য এই আইপিটি ব্লক করা হয়েছেঃ " + ipRestrictionDTO.IP + ". অনুগ্রহ করে সিস্টেম অ্যাডমিনের সাথে যোগাযোগ করুন";
    }

    public String getDelayMessage(IPRestrictionDTO ipRestrictionDTO) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm:ss a");
        return "এই আইপি থেকে অস্বাভাবিক রিকোয়েস্ট আসছে: " + ipRestrictionDTO.IP + ". অনুগ্রহ পূর্বক " + Utils.getDigitBanglaFromEnglish(simpleDateFormat.format(ipRestrictionDTO.nextHitTimeAfter) + " পর্যন্ত অপেক্ষা করুন।");
    }

    public String getIPRestrictionMessage(IPRestrictionDTO ipRestrictionDTO) {
        int hitCountToDelay = Integer.parseInt(GlobalConfigurationRepository.getGlobalConfigDTOByID(GlobalConfigConstants.HIT_COUNT_TO_START_DELAY_LOGIN).value);
        if (ipRestrictionDTO.nextHitTimeAfter == Long.MAX_VALUE) {
            return getBlockMessage(ipRestrictionDTO);
        } else if (ipRestrictionDTO.hitCount >= hitCountToDelay) {
            return getDelayMessage(ipRestrictionDTO);
        }
        return " আপনি " + Utils.getDigitBanglaFromEnglish(ipRestrictionDTO.hitCount + "") + " বার সিস্টেমে লগিন করতে ব্যর্থ হয়েছেন।";
    }
}