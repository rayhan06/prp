package login;

import common.ConnectionAndStatementUtil;

import java.sql.SQLException;
import java.util.Collections;

public class RememberMeOptionDAO {

    private static final String addSQLQuery = "insert into remember_me_option(userID,cookieValue, isOisf) VALUES(?,?,?)";

    private static final String selectByCookieValue = "select userID, isOisf from remember_me_option where cookieValue = ?";

    private static final String deleteByUserId = "delete from remember_me_option where userID = ?";

    private RememberMeOptionDAO(){}

	private static RememberMeOptionDAO INSTANCE = null;

    public static RememberMeOptionDAO getInstance(){
    	if(INSTANCE == null){
    		synchronized (RememberMeOptionDAO.class){
    			if(INSTANCE == null){
					INSTANCE = new RememberMeOptionDAO();
				}
			}
		}
    	return INSTANCE;
	}

    public void insertRememberMeOption(long userID, String cookieValue, int isOisf) {
        ConnectionAndStatementUtil.getWritePrepareStatement(ps -> {
            try {
                int index = 0;
                ps.setObject(++index, userID);
                ps.setObject(++index, cookieValue);
                ps.setObject(++index, isOisf);
                ps.execute();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }, addSQLQuery);
    }

    public LoginDTO getUserIDByCookieValue(String cookieValue) {
        return ConnectionAndStatementUtil.getT(selectByCookieValue, Collections.singletonList(cookieValue), rs -> {
            try {
                LoginDTO tempLoginDto = new LoginDTO();
                tempLoginDto.userID = rs.getLong("userID");
                tempLoginDto.isOisf = rs.getInt("isOisf");
                return tempLoginDto;
            } catch (SQLException ex) {
                ex.printStackTrace();
                return null;
            }
        });
    }

    public void removeRememberMeOptionByUserID(long userID) {
        ConnectionAndStatementUtil.getWritePrepareStatement(ps -> {
            try {
                ps.setObject(1, userID);
                ps.executeUpdate();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }, deleteByUserId);
    }
}