package login;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import sessionmanager.SessionConstants;
import user.LoginSessionListener;
import user.UserDTO;
import user.UserRepository;

@WebServlet("/LogoutServlet")
public class LogoutServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = Logger.getLogger(LogoutServlet.class);

    public LogoutServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		logger.debug("trying to remove userdto from session " + userDTO.userName);
		if(userDTO != null)
		{
			LoginSessionListener.remove(userDTO);
		}
		RememberMeOptionDAO.getInstance().removeRememberMeOptionByUserID(loginDTO.userID);
		request.getSession().invalidate();
		response.sendRedirect( request.getSession().getServletContext().getContextPath() + "/" );
	}
}
