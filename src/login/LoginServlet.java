package login;

import config.GlobalConfigConstants;
import config.GlobalConfigurationRepository;
import employee_offices.EmployeeOfficeDTO;
import employee_offices.EmployeeOfficeRepository;
import employee_records.Employee_recordsDTO;
import employee_records.Employee_recordsRepository;
import forgetPassword.VerificationConstants;
import login_log.Login_logDAO;
import login_log.Login_logDTO;
import oisf.OisfDAO;
import oisf.lib.LibConstants;
import oisf.lib.sso.SSOResponseDTO;
import org.apache.log4j.Logger;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.RootstockUtils;
import util.ServletConstant;
import util.StringUtils;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author Kayesh Parvez
 */
@SuppressWarnings("Duplicates")
@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {

    private static final Logger logger = Logger.getLogger(LoginServlet.class);
    private static final long serialVersionUID = 1L;

    public LoginServlet() {
        super();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);

        boolean isDefaultLoginEnabled = (Integer.parseInt(GlobalConfigurationRepository.getGlobalConfigDTOByID(GlobalConfigConstants.ENABLE_DEFAULT_LOGIN).value) == 1);

        if (!isDefaultLoginEnabled) {
            if (loginDTO == null) {
                String tempNonce = RootstockUtils.generateRandomHexToken(10);
                request.setAttribute("nonce", tempNonce);
                request.getSession().setAttribute("nonce", tempNonce);
                request.getRequestDispatcher("/home/login.jsp").forward(request, response);
            } else {
                response.sendRedirect(" ");
            }
        } else {

            long defaultUserID = Long.parseLong(GlobalConfigurationRepository.getGlobalConfigDTOByID(GlobalConfigConstants.DEFAULT_USER_ID).value);
            if (defaultUserID == loginDTO.userID) {
                String tempNonce = RootstockUtils.generateRandomHexToken(10);
                request.setAttribute("nonce", tempNonce);
                request.getSession().setAttribute("nonce", tempNonce);
                logger.debug("nonce 2 : " + tempNonce);
                request.getRequestDispatcher("/home/login.jsp").forward(request, response);
            } else {
                response.sendRedirect(request.getContextPath());
            }
        }
    }

    void addLoginLog(Login_logDTO login_logDTO) {
        ExecutorService executorService = Executors.newSingleThreadExecutor((r) -> {
            Thread thread = new Thread(r);
            thread.setDaemon(true);
            return thread;
        });

        executorService.execute(
                () -> {
                    try {
                        Login_logDAO.getInstance().add(login_logDTO);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
        );
        executorService.shutdown();

    }

    private void invalidLogin(HttpServletRequest request, HttpServletResponse response, LoginService service, String loginIP, String loginUrl, String username,String errorMsg) {
        try {
            service.processInvalidLogin(loginIP, username == null ? "" : username,errorMsg, request);
            request.getRequestDispatcher(loginUrl).forward(request, response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        Login_logDTO login_logDTO = new Login_logDTO();

        LoginDTO loginDTOTemp = new LoginDTO();
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        username = StringUtils.convertToEngNumber(username);


        String loginIP = request.getRemoteAddr();
        loginDTOTemp.loginSourceIP = loginIP;

        login_logDTO.userId = username;
        login_logDTO.IPAddress = loginIP;
        login_logDTO.userInformation = request.getHeader("User-Agent");
        login_logDTO.isSuccessful = false;
        login_logDTO.insertionDate = System.currentTimeMillis();
        login_logDTO.lastModificationTime = System.currentTimeMillis();

        Map<String, String> deviceData = new DeviceInfoUtil().getInfo(request);
        if (deviceData.get("OperatingSystemNameVersion") != null)
            login_logDTO.operatingSystem = deviceData.get("OperatingSystemNameVersion");
        if (deviceData.get("AgentNameVersion") != null)
            login_logDTO.browserName = deviceData.get("AgentNameVersion");
        if (deviceData.get("DeviceName") != null)
            login_logDTO.deviceName = deviceData.get("DeviceName");

        LoginService service = new LoginService();

        String loginUrl = "home/login.jsp";
        String homeUrl = "home/index.jsp";
        String otpUrl = "home/otpVerifier.jsp";

        try {
            if (username == null || username.length() == 0 || password == null || password.length() == 0) {
                login_logDTO.errorMessage = "ইউজারনেম প্রদান করুন ।";
                invalidLogin(request, response, service, loginIP, loginUrl, username,login_logDTO.errorMessage);
            }
            if (username == null || username.length() == 0) {
                login_logDTO.errorMessage = "পাসওয়ার্ড প্রদান করুন ।";
                invalidLogin(request, response, service, loginIP, loginUrl, username,login_logDTO.errorMessage);
            }
            if (username != null && username.length() > 10) {
                login_logDTO.errorMessage = "ইউজারনেম ভুল ।";
                invalidLogin(request, response, service, loginIP, loginUrl, username,login_logDTO.errorMessage);
            }
            if (username != null && !username.equalsIgnoreCase("superadmin")) {
                if (!StringUtils.isEngNumber(username)) {
                    login_logDTO.errorMessage = "ইউজারনেম ভুল ।";
                    invalidLogin(request, response, service, loginIP, loginUrl, username,login_logDTO.errorMessage);
                }
            }


            request.getSession(true).setAttribute(VerificationConstants.LOGIN_IP, loginIP);
            request.getSession(true).setAttribute(VerificationConstants.CACHE_LOGIN_DTO, loginDTOTemp);
            request.getSession(true).setAttribute(VerificationConstants.CACHE_LOGIN_USERNAME, username);
            request.getSession(true).setAttribute(VerificationConstants.LOGIN_URL, loginUrl);

            if (!service.capchaMatched(request)) {
                login_logDTO.errorMessage = "CAPCHA Mismatched";
                //addLoginLog(login_logDTO);
                if (Integer.parseInt(GlobalConfigurationRepository.getGlobalConfigDTOByID(GlobalConfigConstants.OISF_LOGIN_METHOD).value) == 1) {
                    OisfDAO tempDao = new OisfDAO();
                    String tempRedirect = tempDao.processLoginPage(request, "");
                    response.sendRedirect(tempRedirect);
                    return;
                } else {
                    request.getRequestDispatcher(loginUrl).forward(request, response);
                    return;
                }
            }
            if (service.hasRestriction(loginDTOTemp, username, request)) {
                login_logDTO.errorMessage = "Restriction on the user";
                //addLoginLog(login_logDTO);
                if (Integer.parseInt(GlobalConfigurationRepository.getGlobalConfigDTOByID(GlobalConfigConstants.OISF_LOGIN_METHOD).value) == 1) {
                    OisfDAO tempDao = new OisfDAO();
                    String tempRedirect = tempDao.processLoginPage(request, "");
                    response.sendRedirect(tempRedirect);
                    return;
                } else {
                    request.getRequestDispatcher(loginUrl).forward(request, response);
                    return;
                }
            }


            if (Integer.parseInt(GlobalConfigurationRepository.getGlobalConfigDTOByID(GlobalConfigConstants.OISF_LOGIN_METHOD).value) == 1) {
                LoginDTO loginDTO = service.validateLogin(username, password, loginDTOTemp);
                if (loginDTO == null) {
                    if (login_logDTO.errorMessage == null || login_logDTO.errorMessage.length() == 0) {
                        login_logDTO.errorMessage = "Invalid";
                        //addLoginLog(login_logDTO);
                    }

                    OisfDAO tempDao = new OisfDAO();
                    String tempRedirect = tempDao.processLoginPage(request, "");
                    response.sendRedirect(tempRedirect);
                } else {
                    UserDTO loginUserDTO = UserRepository.getUserDTOByUserID(loginDTO);
                    if (service.needOTPCheck(loginUserDTO, request)) {
                        request.getSession().removeAttribute(SessionConstants.USER_LOGIN);
                        request.getSession().setAttribute(VerificationConstants.CACHE_LOGIN_USERNAME, username);
                        login_logDTO.errorMessage = "OTP check needed";
                        //addLoginLog(login_logDTO);
                        request.getRequestDispatcher(otpUrl).forward(request, response);
                    } else {    //valid login
                        if (request.getParameter("stay_logged_in") != null) {
                            Cookie cookie = new Cookie(ServletConstant.REMEMBER_ME_COOKIE_NAME, UUID.randomUUID().toString());
                            cookie.setMaxAge(60 * 60 * 24 * 365 * 10);
                            RememberMeOptionDAO.getInstance().insertRememberMeOption(loginDTO.userID, cookie.getValue(), 0);
                            response.addCookie(cookie);
                        }

                        service.processValidLogin(loginDTO, username, request);
                        String lastVisitedUrl = service.getLastRequestedUrl(request);
                        logger.debug("Redirecting to: " + lastVisitedUrl);

                        sendLoginLogData(loginUserDTO, login_logDTO);


                        if (lastVisitedUrl != null && !lastVisitedUrl.endsWith("LogoutServlet") 
                        		&& !lastVisitedUrl.endsWith("Entry_pageServlet?actionType=pinCodeRetrieve"))
                            response.sendRedirect(lastVisitedUrl);
                        else {
                            RequestDispatcher rd = request.getRequestDispatcher(homeUrl);
                            rd.forward(request, response);
                        }
                    }
                }


            } else if (Integer.parseInt(GlobalConfigurationRepository.getGlobalConfigDTOByID(GlobalConfigConstants.OISF_LOGIN_METHOD).value) == 2) {
                LoginDTO loginDTO2 = service.validateLogin(username, password, loginDTOTemp);

                if (loginDTO2 == null) {
                    OisfDAO tempDao = new OisfDAO();

                    SSOResponseDTO tempOisfDto = tempDao.processLoginAPI(request, username, password);

                    if (tempOisfDto == null) {
                        String tempIp = (String) request.getSession(true).getAttribute(VerificationConstants.LOGIN_IP);
                        String tempUsername = (String) request.getSession(true).getAttribute(VerificationConstants.CACHE_LOGIN_USERNAME);
                        String tempUrl = (String) request.getSession(true).getAttribute(VerificationConstants.LOGIN_URL);
                        logger.debug("login #################" + tempIp + ":" + tempUsername);
                        service.processInvalidLogin(tempIp, tempUsername, request);
                        login_logDTO.errorMessage = "Invalid";
                        //addLoginLog(login_logDTO);
                        response.sendRedirect(tempUrl);
                    } else {
                        LoginDTO loginDTO = new LoginDTO();

                        loginDTO.isOisf = 1;

                        loginDTO.userID = tempOisfDto.getOfficeUnitOrgId();

                        UserDTO loginUserDTO = UserRepository.getUserDTOByOrganogramID(loginDTO.userID);
                        loginUserDTO.userTypeName = tempOisfDto.getDesignation();

                        String tempIP = (String) request.getSession(true).getAttribute(VerificationConstants.LOGIN_IP);
                        request.getSession(true).setAttribute(SessionConstants.USER_LOGIN, loginDTO);

                        String tempUsername = (String) request.getSession(true).getAttribute(VerificationConstants.CACHE_LOGIN_USERNAME);

                        if (service.needOTPCheck(loginUserDTO, request)) {
                            request.getSession().removeAttribute(SessionConstants.USER_LOGIN);
                            request.getSession().setAttribute(VerificationConstants.CACHE_LOGIN_USERNAME, username);
                            login_logDTO.errorMessage = "OTP check needed";
                            //addLoginLog(login_logDTO);
                            request.getRequestDispatcher(otpUrl).forward(request, response);
                        } else {    //valid login
                            if (request.getParameter("stay_logged_in") != null) {
                                Cookie cookie = new Cookie(ServletConstant.REMEMBER_ME_COOKIE_NAME, UUID.randomUUID().toString());
                                cookie.setMaxAge(60 * 60 * 24 * 365 * 10);
                                RememberMeOptionDAO.getInstance().insertRememberMeOption(loginDTO.userID, cookie.getValue(), 0);
                                response.addCookie(cookie);
                            }
                            loginDTO.loginSourceIP = tempIP;
                            service.processValidLogin(loginDTO, tempUsername, request);
                            request.getSession().setAttribute(LibConstants.SSO_DESIGNATION, tempOisfDto.getOfficeUnitOrgId());
                            request.getSession().setAttribute("name", tempOisfDto.getname_bn());
                            request.getSession().setAttribute("designation", tempOisfDto.getDesignation());
                            request.getSession().setAttribute("officeName", tempOisfDto.getOfficeNameBng());
                            sendLoginLogData(loginUserDTO, login_logDTO);
                            String lastVisitedUrl = service.getLastRequestedUrl(request);
                            if (lastVisitedUrl != null && !lastVisitedUrl.endsWith("LogoutServlet"))
                                response.sendRedirect(lastVisitedUrl);
                            else {
                                RequestDispatcher rd = request.getRequestDispatcher(homeUrl);
                                rd.forward(request, response);
                            }
                        }
                    }
                } else {
                    UserDTO loginUserDTO = UserRepository.getUserDTOByUserID(loginDTO2);
                    if (service.needOTPCheck(loginUserDTO, request)) {
                        request.getSession().removeAttribute(SessionConstants.USER_LOGIN);
                        request.getSession().setAttribute(VerificationConstants.CACHE_LOGIN_USERNAME, username);
                        request.getRequestDispatcher(otpUrl).forward(request, response);

                    } else {    //valid login
                        if (request.getParameter("stay_logged_in") != null) {
                            Cookie cookie = new Cookie(ServletConstant.REMEMBER_ME_COOKIE_NAME, UUID.randomUUID().toString());
                            cookie.setMaxAge(60 * 60 * 24 * 365 * 10);
                            RememberMeOptionDAO.getInstance().insertRememberMeOption(loginDTO2.userID, cookie.getValue(), 0);
                            response.addCookie(cookie);
                        }

                        service.processValidLogin(loginDTO2, username, request);
                        String lastVisitedUrl = service.getLastRequestedUrl(request);
                        if (lastVisitedUrl != null && !lastVisitedUrl.endsWith("LogoutServlet") && !lastVisitedUrl.endsWith("Entry_pageServlet?actionType=pinCodeRetrieve"))
                            response.sendRedirect(lastVisitedUrl);
                        else {
                            RequestDispatcher rd = request.getRequestDispatcher(homeUrl);
                            rd.forward(request, response);
                        }
                    }
                }
            } else {
                LoginDTO loginDTO = service.validateLogin2(username, password, loginDTOTemp);
                if (loginDTO.errorMsg != null) {
                    service.processInvalidLogin(loginIP, username,loginDTO.errorMsg, request);
                    if (login_logDTO.errorMessage == null || login_logDTO.errorMessage.length() == 0) {
                        login_logDTO.errorMessage = "Invalid";
                        //addLoginLog(login_logDTO);
                    }

                    request.getRequestDispatcher(loginUrl).forward(request, response);
                } else {
                    UserDTO loginUserDTO = UserRepository.getUserDTOByUserID(loginDTO);
                    if (service.needOTPCheck(loginUserDTO, request)) {
                        request.getSession().removeAttribute(SessionConstants.USER_LOGIN);
                        request.getSession().setAttribute(VerificationConstants.CACHE_LOGIN_USERNAME, username);
                        login_logDTO.errorMessage = "OTP check needed";
                        //addLoginLog(login_logDTO);
                        request.getRequestDispatcher(otpUrl).forward(request, response);
                    } else {    //valid login
                        if (request.getParameter("stay_logged_in") != null) {
                            Cookie cookie = new Cookie(ServletConstant.REMEMBER_ME_COOKIE_NAME, UUID.randomUUID().toString());
                            cookie.setMaxAge(60 * 60 * 24 * 365 * 10);
                            RememberMeOptionDAO.getInstance().insertRememberMeOption(loginDTO.userID, cookie.getValue(), 0);
                            response.addCookie(cookie);
                        }
                        service.processValidLogin(loginDTO, username, request);
                        sendLoginLogData(loginUserDTO, login_logDTO);
                        String lastVisitedUrl = service.getLastRequestedUrl(request);
                        if (lastVisitedUrl != null && !lastVisitedUrl.endsWith("LogoutServlet") && !lastVisitedUrl.endsWith("Entry_pageServlet?actionType=pinCodeRetrieve"))
                            response.sendRedirect(lastVisitedUrl);
                        else {
                            RequestDispatcher rd = request.getRequestDispatcher(homeUrl);
                            rd.forward(request, response);
                        }
                    }
                }
            }

        } catch (Exception e) {
            logger.fatal("Exception during login", e);
            if (login_logDTO.errorMessage == null || login_logDTO.errorMessage.length() == 0) {
                login_logDTO.errorMessage = "Exception during login";
            }
            //addLoginLog(login_logDTO);
            request.getRequestDispatcher(loginUrl).forward(request, response);
        }
    }

    void sendLoginLogData(UserDTO loginUserDTO, Login_logDTO login_logDTO) {
        if (loginUserDTO != null) {
            login_logDTO.employeeRecordsId = loginUserDTO.employee_record_id;
            Employee_recordsDTO employeeRecordsDTO = Employee_recordsRepository.getInstance().getById(loginUserDTO.employee_record_id);
            if (employeeRecordsDTO != null) {
                login_logDTO.nameEn = employeeRecordsDTO.nameEng;
                login_logDTO.nameBn = employeeRecordsDTO.nameBng;
                EmployeeOfficeDTO employeeOfficeDTO = EmployeeOfficeRepository.getInstance().getByEmployeeRecordIdIsDefault(employeeRecordsDTO.iD);
                if (employeeOfficeDTO != null) {
                    login_logDTO.employeeOfficeId = employeeOfficeDTO.id;
                }
            }
            login_logDTO.isSuccessful = true;
            login_logDTO.errorMessage = "Valid";
            //addLoginLog(login_logDTO);
        }

    }
}