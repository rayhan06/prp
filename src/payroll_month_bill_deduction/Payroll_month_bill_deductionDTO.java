package payroll_month_bill_deduction;

import sessionmanager.SessionConstants;
import util.CommonDTO;

import java.util.Objects;

public class Payroll_month_bill_deductionDTO extends CommonDTO {
    public long payrollMonthBillId = 0;
    public long taxDeductionConfigurationId = 0;
    public int amount = 0;
    public long insertedBy = -1;
    public long insertionTime = SessionConstants.MIN_DATE;
    public long modifiedBy = -1;
    public String dataField="";

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Payroll_month_bill_deductionDTO)) return false;
        Payroll_month_bill_deductionDTO that = (Payroll_month_bill_deductionDTO) o;
        return taxDeductionConfigurationId == that.taxDeductionConfigurationId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(taxDeductionConfigurationId);
    }

    @Override
    public String toString() {
        return "Payroll_month_bill_deductionDTO{" +
                "payrollMonthBillId=" + payrollMonthBillId +
                ", taxDeductionConfigurationId=" + taxDeductionConfigurationId +
                ", amount=" + amount +
                ", insertedBy=" + insertedBy +
                ", insertionTime=" + insertionTime +
                ", modifiedBy=" + modifiedBy +
                '}';
    }
}
