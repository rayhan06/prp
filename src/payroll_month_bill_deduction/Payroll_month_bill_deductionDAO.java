package payroll_month_bill_deduction;

import common.CommonDAOService;
import org.apache.log4j.Logger;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.*;

@SuppressWarnings({"Duplicates"})
public class Payroll_month_bill_deductionDAO implements CommonDAOService<Payroll_month_bill_deductionDTO> {
    private static final Logger logger = Logger.getLogger(Payroll_month_bill_deductionDAO.class);

    private static final String addQuery =
            "INSERT INTO {tableName} (payroll_month_bill_id,tax_deduction_configuration_id,amount,data_field,modified_by,lastModificationTime,"
                    .concat("inserted_by,insertion_time,isDeleted,ID) VALUES (?,?,?,?,?,?,?,?,?,?)");

    private static final String updateQuery =
            "UPDATE {tableName} SET payroll_month_bill_id=?,tax_deduction_configuration_id=?,amount=?,data_field=?,"
                    .concat("modified_by=?,lastModificationTime=? WHERE ID=?");
    
    private static final String getByMonthBillIdQuery =
            "SELECT * From payroll_month_bill_deduction where payroll_month_bill_id = %d AND isDeleted=0;";

    private static final String getByPayrollIds = "SELECT * FROM payroll_month_bill_deduction WHERE payroll_month_bill_id IN (%s) AND isDeleted=0";

    private static final Map<String, String> searchMap = new HashMap<>();

    private Payroll_month_bill_deductionDAO() {
    }

    private static class LazyLoader {
        static final Payroll_month_bill_deductionDAO INSTANCE = new Payroll_month_bill_deductionDAO();
    }

    public static Payroll_month_bill_deductionDAO getInstance() {
        return Payroll_month_bill_deductionDAO.LazyLoader.INSTANCE;
    }

    @Override
    public void set(PreparedStatement ps, Payroll_month_bill_deductionDTO payroll_month_bill_deductionDTO, boolean isInsert) throws SQLException {
        int index = 0;
        ps.setLong(++index, payroll_month_bill_deductionDTO.payrollMonthBillId);
        ps.setLong(++index, payroll_month_bill_deductionDTO.taxDeductionConfigurationId);
        ps.setInt(++index, payroll_month_bill_deductionDTO.amount);
        ps.setString(++index, payroll_month_bill_deductionDTO.dataField);
        ps.setLong(++index, payroll_month_bill_deductionDTO.modifiedBy);
        ps.setLong(++index, payroll_month_bill_deductionDTO.lastModificationTime);
        if (isInsert) {
            ps.setLong(++index, payroll_month_bill_deductionDTO.insertedBy);
            ps.setLong(++index, payroll_month_bill_deductionDTO.insertionTime);
            ps.setInt(++index, 0);
        }
        ps.setLong(++index, payroll_month_bill_deductionDTO.iD);
    }

    @Override
    public Payroll_month_bill_deductionDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Payroll_month_bill_deductionDTO dto = new Payroll_month_bill_deductionDTO();
            dto.iD = rs.getLong("ID");
            dto.payrollMonthBillId = rs.getInt("payroll_month_bill_id");
            dto.taxDeductionConfigurationId = rs.getInt("tax_deduction_configuration_id");
            dto.amount = rs.getInt("amount");
            dto.dataField = rs.getString("data_field");
            dto.insertedBy = rs.getLong("inserted_by");
            dto.insertionTime = rs.getLong("insertion_time");
            dto.isDeleted = rs.getInt("isDeleted");
            dto.modifiedBy = rs.getLong("modified_by");
            dto.lastModificationTime = rs.getLong("lastModificationTime");
            return dto;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    @Override
    public String getTableName() {
        return "payroll_month_bill_deduction";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    @Override
    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Payroll_month_bill_deductionDTO) commonDTO, addQuery, true);
    }

    @Override
    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Payroll_month_bill_deductionDTO) commonDTO, updateQuery, false);
    }

    public List<Payroll_month_bill_deductionDTO> getByByMonthBillId(long monthBillId) {
        return getDTOs(String.format(getByMonthBillIdQuery, monthBillId));
    }

    public List<Payroll_month_bill_deductionDTO> getBypayrollMonthBillIds(List<Long> payrollMonthBillIds) {
        String monthBillIds = payrollMonthBillIds.stream()
                                                 .map(String::valueOf)
                                                 .collect(joining(","));
        return getDTOs(String.format(getByPayrollIds, monthBillIds));
    }

    public Map<Long, Integer> getSumOfDeduction(List<Payroll_month_bill_deductionDTO> dtos) {
        return dtos.stream()
                   .collect(groupingBy(dto -> dto.taxDeductionConfigurationId, summingInt(dto -> dto.amount)));
    }
}