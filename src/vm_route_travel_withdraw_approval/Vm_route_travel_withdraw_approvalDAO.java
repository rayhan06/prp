package vm_route_travel_withdraw_approval;

import employee_records.EmployeeCommonDTOService;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

//			"ID",
//			"employee_records_id",
//			"name_bn",
//			"name_en",
//			"user_name",
//			"office_unit_id",
//			"office_unit_eng",
//			"office_unit_bng",
//			"organogram_id",
//			"organogram_eng",
//			"organogram_bng",
//			"search_column",
//			"inserted_by_user_id",
//			"inserted_by_organogram_id",
//			"insertion_date",
//			"isDeleted",
//			"lastModificationTime"
public class Vm_route_travel_withdraw_approvalDAO implements EmployeeCommonDTOService<Vm_route_travel_withdraw_approvalDTO> {
	private static final String addSqlQuery = "INSERT INTO {tableName} (employee_records_id,name_bn,name_en," +
			"office_unit_id,office_unit_eng,office_unit_bng,organogram_id,organogram_eng,organogram_bng," +
			"lastModificationTime,inserted_by_user_id,insertion_date,isDeleted,id) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	private static final String deleteSqlQuery = "UPDATE vm_route_travel_withdraw_approval SET isDeleted = 1, lastModificationTime = %d WHERE id = %d";



	@Override
	public void set(PreparedStatement ps, Vm_route_travel_withdraw_approvalDTO dto, boolean isInsert) throws SQLException {
		int index = 0;
		ps.setLong(++index,dto.employeeRecordId);
		ps.setString(++index,dto.nameEn);
		ps.setString(++index,dto.nameBn);
		ps.setLong(++index,dto.officeUnitId);
		ps.setString(++index,dto.officeUnitEng);
		ps.setString(++index,dto.officeUnitBng);
		ps.setLong(++index,dto.organogramId);
		ps.setString(++index,dto.organogramEng);
		ps.setString(++index,dto.organogramBng);

		ps.setLong(++index,dto.lastModificationTime);
		if(isInsert){
			ps.setLong(++index,dto.insertedByUserId);
			ps.setLong(++index,dto.insertionDate);
			ps.setInt(++index,0);
		}
		ps.setLong(++index,dto.iD);
	}

	@Override
	public Vm_route_travel_withdraw_approvalDTO buildObjectFromResultSet(ResultSet rs) {
		try{
			Vm_route_travel_withdraw_approvalDTO dto = new Vm_route_travel_withdraw_approvalDTO();
			dto.iD = rs.getLong("id");
			dto.employeeRecordId = rs.getLong("employee_records_id");
			dto.nameEn = rs.getString("name_en");
			dto.nameBn = rs.getString("name_bn");
			dto.officeUnitId = rs.getLong("office_unit_id");
			dto.officeUnitEng = rs.getString("office_unit_eng");
			dto.officeUnitBng = rs.getString("office_unit_bng");
			dto.organogramId = rs.getLong("organogram_id");
			dto.organogramEng = rs.getString("organogram_eng");
			dto.organogramBng = rs.getString("organogram_bng");
			dto.insertionDate = rs.getLong("insertion_date");
			dto.insertedByUserId = rs.getLong("inserted_by_user_id");
			//dto.modifiedBy = rs.getLong("modified_by");
			dto.isDeleted = rs.getInt("isDeleted");
			dto.lastModificationTime = rs.getLong("lastModificationTime");
			return dto;
		}catch (SQLException ex){
			return null;
		}
	}

	@Override
	public String getTableName() {
		return "vm_route_travel_withdraw_approval";
	}

	public long add(Vm_route_travel_withdraw_approvalDTO dto) throws Exception {
		System.out.println("here in createApprovalDTOForEmployeeRecordId 5 addSqlQuery: "+addSqlQuery);
		return executeAddOrUpdateQuery(dto, addSqlQuery, true);
	}
	public Vm_route_travel_withdraw_approvalDTO getDTOByID(long ID) {
		return getDTOFromID(ID);
	}

}

//public class Vm_route_travel_withdraw_approvalDAO  extends NavigationService4
//{
//
//	Logger logger = Logger.getLogger(getClass());
//
//	public Vm_route_travel_withdraw_approvalDAO(String tableName)
//	{
//		super(tableName);
//		joinSQL = "";
//		commonMaps = new Vm_route_travel_withdraw_approvalMAPS(tableName);
//		columnNames = new String[]
//		{
//			"ID",
//			"employee_records_id",
//			"name_bn",
//			"name_en",
//			"user_name",
//			"office_unit_id",
//			"office_unit_eng",
//			"office_unit_bng",
//			"organogram_id",
//			"organogram_eng",
//			"organogram_bng",
//			"search_column",
//			"inserted_by_user_id",
//			"inserted_by_organogram_id",
//			"insertion_date",
//			"isDeleted",
//			"lastModificationTime"
//		};
//	}
//
//	public Vm_route_travel_withdraw_approvalDAO()
//	{
//		this("vm_route_travel_withdraw_approval");
//	}
//
//	public void setSearchColumn(Vm_route_travel_withdraw_approvalDTO vm_route_travel_withdraw_approvalDTO)
//	{
//		vm_route_travel_withdraw_approvalDTO.searchColumn = "";
//	}
//
//	public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException
//	{
//		Vm_route_travel_withdraw_approvalDTO vm_route_travel_withdraw_approvalDTO = (Vm_route_travel_withdraw_approvalDTO)commonDTO;
//		int index = 1;
//		long lastModificationTime = System.currentTimeMillis();
//		setSearchColumn(vm_route_travel_withdraw_approvalDTO);
//		if(isInsert)
//		{
//			ps.setObject(index++,vm_route_travel_withdraw_approvalDTO.iD);
//		}
//		ps.setObject(index++,vm_route_travel_withdraw_approvalDTO.employeeRecordId);
//		ps.setObject(index++,vm_route_travel_withdraw_approvalDTO.nameBn);
//		ps.setObject(index++,vm_route_travel_withdraw_approvalDTO.nameEn);
//		ps.setObject(index++,vm_route_travel_withdraw_approvalDTO.userName);
//		ps.setObject(index++,vm_route_travel_withdraw_approvalDTO.officeUnitId);
//		ps.setObject(index++,vm_route_travel_withdraw_approvalDTO.officeUnitEng);
//		ps.setObject(index++,vm_route_travel_withdraw_approvalDTO.officeUnitBng);
//		ps.setObject(index++,vm_route_travel_withdraw_approvalDTO.organogramId);
//		ps.setObject(index++,vm_route_travel_withdraw_approvalDTO.organogramEng);
//		ps.setObject(index++,vm_route_travel_withdraw_approvalDTO.organogramBng);
//		ps.setObject(index++,vm_route_travel_withdraw_approvalDTO.searchColumn);
//		ps.setObject(index++,vm_route_travel_withdraw_approvalDTO.insertedByUserId);
//		ps.setObject(index++,vm_route_travel_withdraw_approvalDTO.insertedByOrganogramId);
//		ps.setObject(index++,vm_route_travel_withdraw_approvalDTO.insertionDate);
//		if(isInsert)
//		{
//			ps.setObject(index++, 0);
//			ps.setObject(index++, lastModificationTime);
//		}
//	}
//
//	public Vm_route_travel_withdraw_approvalDTO build(ResultSet rs)
//	{
//		try
//		{
//			Vm_route_travel_withdraw_approvalDTO vm_route_travel_withdraw_approvalDTO = new Vm_route_travel_withdraw_approvalDTO();
//			vm_route_travel_withdraw_approvalDTO.iD = rs.getLong("ID");
//			vm_route_travel_withdraw_approvalDTO.employeeRecordId = rs.getLong("employee_records_id");
//			vm_route_travel_withdraw_approvalDTO.nameBn = rs.getString("name_bn");
//			vm_route_travel_withdraw_approvalDTO.nameEn = rs.getString("name_en");
//			vm_route_travel_withdraw_approvalDTO.userName = rs.getString("user_name");
//			vm_route_travel_withdraw_approvalDTO.officeUnitId = rs.getLong("office_unit_id");
//			vm_route_travel_withdraw_approvalDTO.officeUnitEng = rs.getString("office_unit_eng");
//			vm_route_travel_withdraw_approvalDTO.officeUnitBng = rs.getString("office_unit_bng");
//			vm_route_travel_withdraw_approvalDTO.organogramId = rs.getLong("organogram_id");
//			vm_route_travel_withdraw_approvalDTO.organogramEng = rs.getString("organogram_eng");
//			vm_route_travel_withdraw_approvalDTO.organogramBng = rs.getString("organogram_bng");
//			vm_route_travel_withdraw_approvalDTO.searchColumn = rs.getString("search_column");
//			vm_route_travel_withdraw_approvalDTO.insertedByUserId = rs.getLong("inserted_by_user_id");
//			vm_route_travel_withdraw_approvalDTO.insertedByOrganogramId = rs.getLong("inserted_by_organogram_id");
//			vm_route_travel_withdraw_approvalDTO.insertionDate = rs.getLong("insertion_date");
//			vm_route_travel_withdraw_approvalDTO.isDeleted = rs.getInt("isDeleted");
//			vm_route_travel_withdraw_approvalDTO.lastModificationTime = rs.getLong("lastModificationTime");
//			return vm_route_travel_withdraw_approvalDTO;
//		}
//		catch (SQLException ex)
//		{
//			logger.error(ex);
//			return null;
//		}
//	}
//
//
//
//	public Vm_route_travel_withdraw_approvalDTO getDTOByID (long id)
//	{
//		String sql = "SELECT * FROM " + tableName + " WHERE ID = " + id;
//		Vm_route_travel_withdraw_approvalDTO vm_route_travel_withdraw_approvalDTO = ConnectionAndStatementUtil.getT(sql,this::build);
//		return vm_route_travel_withdraw_approvalDTO;
//	}
//
//
//	public List<Vm_route_travel_withdraw_approvalDTO> getDTOs(Collection recordIDs)
//	{
//		String sql = "SELECT * FROM " + tableName + " WHERE ID IN ( ";
//		for(int i = 0;i<recordIDs.size();i++){
//			if(i!=0){
//				sql+=",";
//			}
//			sql+=((ArrayList)recordIDs).get(i);
//		}
//		sql+=")  order by lastModificationTime desc";
//		printSql(sql);
//		return ConnectionAndStatementUtil.getListOfT(sql,this::build);
//	}
//
//	public List<Vm_route_travel_withdraw_approvalDTO> getAllVm_route_travel_withdraw_approval (boolean isFirstReload)
//    {
//		String sql = "SELECT * FROM " + tableName + " WHERE ";
//		if(isFirstReload){
//			sql+=" isDeleted =  0";
//		}
//		if(!isFirstReload){
//			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;
//		}
//		sql += " order by " + tableName + ".lastModificationTime desc";
//		return ConnectionAndStatementUtil.getListOfT(sql,this::build);
//    }
//
//
//	public List<Vm_route_travel_withdraw_approvalDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO)
//	{
//		return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
//	}
//
//
//	public List<Vm_route_travel_withdraw_approvalDTO> getDTOs(Hashtable  p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
//			String filter, boolean tableHasJobCat)
//	{
//		String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat);
//		return ConnectionAndStatementUtil.getListOfT(sql,this::build);
//	}
//	public String getSqlWithSearchCriteriaForNormalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
//			UserDTO userDTO, String filter, boolean tableHasJobCat)
//    {
//		boolean viewAll = false;
//
//		if(p_searchCriteria != null && p_searchCriteria.get("ViewAll") != null)
//		{
//			System.out.println("ViewAll = " + p_searchCriteria.get("ViewAll"));
//			viewAll = true;
//		}
//		else
//		{
//			System.out.println("ViewAll is null ");
//		}
//
//		String sql = "SELECT ";
//		if(category == GETIDS)
//		{
//			sql += " " + tableName + ".ID as ID ";
//		}
//		else if(category == GETCOUNT)
//		{
//			sql += " count( " + tableName + ".ID) as countID ";
//		}
//		else if(category == GETDTOS)
//		{
//			sql += " distinct " + tableName + ".* ";
//		}
//		sql += "FROM " + tableName + " ";
//		sql += joinSQL;
//
//
//
//		String AnyfieldSql = "";
//		String AllFieldSql = "";
//
//		if(p_searchCriteria != null)
//		{
//
//
//			Enumeration names = p_searchCriteria.keys();
//			String str, value;
//
//			AnyfieldSql = "(";
//
//			if(p_searchCriteria.get("AnyField")!= null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase(""))
//			{
//				AnyfieldSql+= tableName + ".search_column like '%" + p_searchCriteria.get("AnyField").toString() + "%'";
//			}
//			AnyfieldSql += ")";
//			System.out.println("AnyfieldSql = " + AnyfieldSql);
//
//			AllFieldSql = "(";
//			int i = 0;
//			while(names.hasMoreElements())
//			{
//				str = (String) names.nextElement();
//				value = (String)p_searchCriteria.get(str);
//		        System.out.println(str + ": " + value);
//				if(value != null && !value.equalsIgnoreCase("") && (
//						 str.equals("insertion_date_start")
//						|| str.equals("insertion_date_end")
//				)
//
//				)
//				{
//					if(p_searchCriteria.get(str).equals("any"))
//					{
//						continue;
//					}
//
//					if( i > 0)
//					{
//						AllFieldSql+= " AND  ";
//					}
//
//					 if(str.equals("insertion_date_start"))
//					{
//						AllFieldSql += "" + tableName + ".insertion_date >= " + p_searchCriteria.get(str);
//						i ++;
//					}
//					else if(str.equals("insertion_date_end"))
//					{
//						AllFieldSql += "" + tableName + ".insertion_date <= " + p_searchCriteria.get(str);
//						i ++;
//					}
//
//
//				}
//			}
//
//			AllFieldSql += ")";
//			System.out.println("AllFieldSql = " + AllFieldSql);
//
//
//		}
//
//
//		sql += " WHERE ";
//
//		sql += " (" + tableName + ".isDeleted = 0 ";
//		sql += ")";
//
//
//		if(!filter.equalsIgnoreCase(""))
//		{
//			sql += " and " + filter + " ";
//		}
//
//		if(!AnyfieldSql.equals("()") && !AnyfieldSql.equals(""))
//		{
//			sql += " AND " + AnyfieldSql;
//
//		}
//		if(!AllFieldSql.equals("()") && !AllFieldSql.equals(""))
//		{
//			sql += " AND " + AllFieldSql;
//		}
//
//
//
//		sql += " order by " + tableName + ".lastModificationTime desc ";
//
//		printSql(sql);
//
//		if(limit >= 0)
//		{
//			sql += " limit " + limit;
//		}
//		if(offset >= 0)
//		{
//			sql += " offset " + offset;
//		}
//
//		System.out.println("-------------- sql = " + sql);
//
//		return sql;
//    }
//
//	public String getSqlWithSearchCriteria(Hashtable p_searchCriteria, int limit, int offset, int category,
//			boolean isPermanentTable, UserDTO userDTO, String filter, boolean tableHasJobCat)
//    {
//		return getSqlWithSearchCriteriaForNormalSearch(p_searchCriteria, limit, offset, category, userDTO, filter, tableHasJobCat);
//    }
//
//}
	