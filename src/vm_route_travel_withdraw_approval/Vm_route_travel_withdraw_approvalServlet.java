//package vm_route_travel_withdraw_approval;
//
//import java.io.IOException;
//import java.io.*;
//import java.text.SimpleDateFormat;
//
//
//import javax.servlet.RequestDispatcher;
//import javax.servlet.ServletException;
//import javax.servlet.annotation.MultipartConfig;
//import javax.servlet.annotation.WebServlet;
//
//
//import org.apache.log4j.Logger;
//
//import login.LoginDTO;
//import permission.MenuConstants;
//import role.PermissionRepository;
//
//
//import sessionmanager.SessionConstants;
//
//import user.UserDTO;
//import user.UserRepository;
//import util.CommonDTO;
//import util.CommonRequestHandler;
//import util.RecordNavigationManager4;
//
//import java.util.*;
//import javax.servlet.http.*;
//import java.util.UUID;
//
//
//
//
//
//import geolocation.GeoLocationDAO2;
//import java.util.StringTokenizer;
//
//import com.google.gson.Gson;
//
//import pb.*;
//import pbReport.*;
//import org.jsoup.Jsoup;
//import org.jsoup.safety.Whitelist;
//
//
//
//
///**
// * Servlet implementation class Vm_route_travel_withdraw_approvalServlet
// */
//@WebServlet("/Vm_route_travel_withdraw_approvalServlet")
//@MultipartConfig
//public class Vm_route_travel_withdraw_approvalServlet extends HttpServlet
//{
//	private static final long serialVersionUID = 1L;
//    public static Logger logger = Logger.getLogger(Vm_route_travel_withdraw_approvalServlet.class);
//
//    String tableName = "vm_route_travel_withdraw_approval";
//
//	Vm_route_travel_withdraw_approvalDAO vm_route_travel_withdraw_approvalDAO;
//	CommonRequestHandler commonRequestHandler;
//    private Gson gson = new Gson();
//
//    /**
//     * @see HttpServlet#HttpServlet()
//     */
//    public Vm_route_travel_withdraw_approvalServlet()
//	{
//        super();
//    	try
//    	{
//			vm_route_travel_withdraw_approvalDAO = new Vm_route_travel_withdraw_approvalDAO(tableName);
//			commonRequestHandler = new CommonRequestHandler(vm_route_travel_withdraw_approvalDAO);
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//    }
//
//	/**
//	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
//	 */
//	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
//	{
//		System.out.println("In doget request = " + request);
//		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
//		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
//		boolean isPermanentTable = true;
//		if(request.getParameter("isPermanentTable") != null)
//		{
//			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
//		}
////		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
////		try
////		{
////			String actionType = request.getParameter("actionType");
////			if(actionType.equals("getAddPage"))
////			{
////				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_ROUTE_TRAVEL_WITHDRAW_APPROVAL_ADD))
////				{
////					commonRequestHandler.getAddPage(request, response);
////				}
////				else
////				{
////					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
////				}
////			}
////			else if(actionType.equals("getEditPage"))
////			{
////				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_ROUTE_TRAVEL_WITHDRAW_APPROVAL_UPDATE))
////				{
////					getVm_route_travel_withdraw_approval(request, response);
////				}
////				else
////				{
////					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
////				}
////			}
////			else if(actionType.equals("getURL"))
////			{
////				String URL = request.getParameter("URL");
////				System.out.println("URL = " + URL);
////				response.sendRedirect(URL);
////			}
////			else if(actionType.equals("search"))
////			{
////				System.out.println("search requested");
////				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_ROUTE_TRAVEL_WITHDRAW_APPROVAL_SEARCH))
////				{
////					if(isPermanentTable)
////					{
////						String filter = request.getParameter("filter");
////						System.out.println("filter = " + filter);
////						if(filter!=null)
////						{
////							filter = ""; //shouldn't be directly used, rather manipulate it.
////							searchVm_route_travel_withdraw_approval(request, response, isPermanentTable, filter);
////						}
////						else
////						{
////							searchVm_route_travel_withdraw_approval(request, response, isPermanentTable, "");
////						}
////					}
////					else
////					{
////						//searchVm_route_travel_withdraw_approval(request, response, tempTableName, isPermanentTable);
////					}
////				}
////			}
////			else if(actionType.equals("view"))
////			{
////				System.out.println("view requested");
////				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_ROUTE_TRAVEL_WITHDRAW_APPROVAL_SEARCH))
////				{
////					commonRequestHandler.view(request, response);
////				}
////				else
////				{
////					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
////				}
////
////			}
////			else
////			{
////				request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
////			}
////		}
////		catch(Exception ex)
////		{
////			ex.printStackTrace();
////			logger.debug(ex);
////		}
//	}
//
//
//
//
//
//	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
//	{
//		// TODO Auto-generated method stub
//		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
//		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
//		System.out.println("doPost");
//		boolean isPermanentTable = true;
//		if(request.getParameter("isPermanentTable") != null)
//		{
//			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
//		}
////		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
////
////		try
////		{
////			String actionType = request.getParameter("actionType");
////			System.out.println("actionType = " + actionType);
////			if(actionType.equals("add"))
////			{
////
////				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_ROUTE_TRAVEL_WITHDRAW_APPROVAL_ADD))
////				{
////					System.out.println("going to  addVm_route_travel_withdraw_approval ");
////					addVm_route_travel_withdraw_approval(request, response, true, userDTO, true);
////				}
////				else
////				{
////					System.out.println("Not going to  addVm_route_travel_withdraw_approval ");
////					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
////				}
////
////			}
////
////			else if(actionType.equals("getDTO"))
////			{
////
////				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_ROUTE_TRAVEL_WITHDRAW_APPROVAL_ADD))
////				{
////					getDTO(request, response);
////				}
////				else
////				{
////					System.out.println("Not going to  addVm_route_travel_withdraw_approval ");
////					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
////				}
////
////			}
////			else if(actionType.equals("edit"))
////			{
////
////				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_ROUTE_TRAVEL_WITHDRAW_APPROVAL_UPDATE))
////				{
////					addVm_route_travel_withdraw_approval(request, response, false, userDTO, isPermanentTable);
////				}
////				else
////				{
////					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
////				}
////			}
////			else if(actionType.equals("delete"))
////			{
////				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_ROUTE_TRAVEL_WITHDRAW_APPROVAL_UPDATE))
////				{
////					commonRequestHandler.delete(request, response, userDTO);
////				}
////				else
////				{
////					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
////				}
////			}
////			else if(actionType.equals("search"))
////			{
////				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_ROUTE_TRAVEL_WITHDRAW_APPROVAL_SEARCH))
////				{
////					searchVm_route_travel_withdraw_approval(request, response, true, "");
////				}
////				else
////				{
////					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
////				}
////			}
////			else if(actionType.equals("getGeo"))
////			{
////				System.out.println("going to geoloc ");
////				request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
////			}
////
////		}
////		catch(Exception ex)
////		{
////			ex.printStackTrace();
////			logger.debug(ex);
////		}
//	}
//
//	private void getDTO(HttpServletRequest request, HttpServletResponse response)
//	{
//		try
//		{
//			System.out.println("In getDTO");
//			Vm_route_travel_withdraw_approvalDTO vm_route_travel_withdraw_approvalDTO = (Vm_route_travel_withdraw_approvalDTO)vm_route_travel_withdraw_approvalDAO.getDTOByID(Long.parseLong(request.getParameter("ID")));
//			PrintWriter out = response.getWriter();
//			response.setContentType("application/json");
//			response.setCharacterEncoding("UTF-8");
//
//			String encoded = this.gson.toJson(vm_route_travel_withdraw_approvalDTO);
//			System.out.println("json encoded data = " + encoded);
//			out.print(encoded);
//			out.flush();
//		}
//		catch (NumberFormatException e)
//		{
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//
//	}
//
//	private void addVm_route_travel_withdraw_approval(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, boolean isPermanentTable) throws IOException
//	{
//		// TODO Auto-generated method stub
//		try
//		{
//			request.setAttribute("failureMessage", "");
//			System.out.println("%%%% addVm_route_travel_withdraw_approval");
//			String path = getServletContext().getRealPath("/img2/");
//			Vm_route_travel_withdraw_approvalDTO vm_route_travel_withdraw_approvalDTO;
//			SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
//
//			if(addFlag == true)
//			{
//				vm_route_travel_withdraw_approvalDTO = new Vm_route_travel_withdraw_approvalDTO();
//			}
//			else
//			{
//				vm_route_travel_withdraw_approvalDTO = (Vm_route_travel_withdraw_approvalDTO)vm_route_travel_withdraw_approvalDAO.getDTOByID(Long.parseLong(request.getParameter("iD")));
//			}
//
//			String Value = "";
//
//			Value = request.getParameter("employeeRecordId");
//
//			if(Value != null && !Value.equalsIgnoreCase(""))
//			{
//				Value = Jsoup.clean(Value,Whitelist.simpleText());
//			}
//			System.out.println("employeeRecordId = " + Value);
//			if(Value != null && !Value.equalsIgnoreCase(""))
//			{
//				vm_route_travel_withdraw_approvalDTO.employeeRecordId = Long.parseLong(Value);
//			}
//			else
//			{
//				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//			}
//
//			Value = request.getParameter("nameBn");
//
//			if(Value != null)
//			{
//				Value = Jsoup.clean(Value,Whitelist.simpleText());
//			}
//			System.out.println("nameBn = " + Value);
//			if(Value != null)
//			{
//				vm_route_travel_withdraw_approvalDTO.nameBn = (Value);
//			}
//			else
//			{
//				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//			}
//
//			Value = request.getParameter("nameEn");
//
//			if(Value != null)
//			{
//				Value = Jsoup.clean(Value,Whitelist.simpleText());
//			}
//			System.out.println("nameEn = " + Value);
//			if(Value != null)
//			{
//				vm_route_travel_withdraw_approvalDTO.nameEn = (Value);
//			}
//			else
//			{
//				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//			}
//
//			Value = request.getParameter("userName");
//
//			if(Value != null)
//			{
//				Value = Jsoup.clean(Value,Whitelist.simpleText());
//			}
//			System.out.println("userName = " + Value);
//			if(Value != null)
//			{
//				vm_route_travel_withdraw_approvalDTO.userName = (Value);
//			}
//			else
//			{
//				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//			}
//
//			Value = request.getParameter("officeUnitId");
//
//			if(Value != null && !Value.equalsIgnoreCase(""))
//			{
//				Value = Jsoup.clean(Value,Whitelist.simpleText());
//			}
//			System.out.println("officeUnitId = " + Value);
//			if(Value != null && !Value.equalsIgnoreCase(""))
//			{
//				vm_route_travel_withdraw_approvalDTO.officeUnitId = Long.parseLong(Value);
//			}
//			else
//			{
//				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//			}
//
//			Value = request.getParameter("officeUnitEng");
//
//			if(Value != null)
//			{
//				Value = Jsoup.clean(Value,Whitelist.simpleText());
//			}
//			System.out.println("officeUnitEng = " + Value);
//			if(Value != null)
//			{
//				vm_route_travel_withdraw_approvalDTO.officeUnitEng = (Value);
//			}
//			else
//			{
//				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//			}
//
//			Value = request.getParameter("officeUnitBng");
//
//			if(Value != null)
//			{
//				Value = Jsoup.clean(Value,Whitelist.simpleText());
//			}
//			System.out.println("officeUnitBng = " + Value);
//			if(Value != null)
//			{
//				vm_route_travel_withdraw_approvalDTO.officeUnitBng = (Value);
//			}
//			else
//			{
//				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//			}
//
//			Value = request.getParameter("organogramId");
//
//			if(Value != null && !Value.equalsIgnoreCase(""))
//			{
//				Value = Jsoup.clean(Value,Whitelist.simpleText());
//			}
//			System.out.println("organogramId = " + Value);
//			if(Value != null && !Value.equalsIgnoreCase(""))
//			{
//				vm_route_travel_withdraw_approvalDTO.organogramId = Long.parseLong(Value);
//			}
//			else
//			{
//				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//			}
//
//			Value = request.getParameter("organogramEng");
//
//			if(Value != null)
//			{
//				Value = Jsoup.clean(Value,Whitelist.simpleText());
//			}
//			System.out.println("organogramEng = " + Value);
//			if(Value != null)
//			{
//				vm_route_travel_withdraw_approvalDTO.organogramEng = (Value);
//			}
//			else
//			{
//				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//			}
//
//			Value = request.getParameter("organogramBng");
//
//			if(Value != null)
//			{
//				Value = Jsoup.clean(Value,Whitelist.simpleText());
//			}
//			System.out.println("organogramBng = " + Value);
//			if(Value != null)
//			{
//				vm_route_travel_withdraw_approvalDTO.organogramBng = (Value);
//			}
//			else
//			{
//				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//			}
//
//			Value = request.getParameter("searchColumn");
//
//			if(Value != null)
//			{
//				Value = Jsoup.clean(Value,Whitelist.simpleText());
//			}
//			System.out.println("searchColumn = " + Value);
//			if(Value != null)
//			{
//				vm_route_travel_withdraw_approvalDTO.searchColumn = (Value);
//			}
//			else
//			{
//				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//			}
//
//			if(addFlag)
//			{
//				vm_route_travel_withdraw_approvalDTO.insertedByUserId = userDTO.ID;
//			}
//
//
//			if(addFlag)
//			{
//				vm_route_travel_withdraw_approvalDTO.insertedByOrganogramId = userDTO.organogramID;
//			}
//
//
//			if(addFlag)
//			{
//				Calendar c = Calendar.getInstance();
//				c.set(Calendar.HOUR_OF_DAY, 0);
//				c.set(Calendar.MINUTE, 0);
//				c.set(Calendar.SECOND, 0);
//				c.set(Calendar.MILLISECOND, 0);
//
//				vm_route_travel_withdraw_approvalDTO.insertionDate = c.getTimeInMillis();
//			}
//
//
//			System.out.println("Done adding  addVm_route_travel_withdraw_approval dto = " + vm_route_travel_withdraw_approvalDTO);
//			long returnedID = -1;
//
//			if(isPermanentTable == false) //add new row for validation and make the old row outdated
//			{
//				vm_route_travel_withdraw_approvalDAO.setIsDeleted(vm_route_travel_withdraw_approvalDTO.iD, CommonDTO.OUTDATED);
//				returnedID = vm_route_travel_withdraw_approvalDAO.add(vm_route_travel_withdraw_approvalDTO);
//				vm_route_travel_withdraw_approvalDAO.setIsDeleted(returnedID, CommonDTO.WAITING_FOR_APPROVAL);
//			}
//			else if(addFlag == true)
//			{
//				returnedID = vm_route_travel_withdraw_approvalDAO.manageWriteOperations(vm_route_travel_withdraw_approvalDTO, SessionConstants.INSERT, -1, userDTO);
//			}
//			else
//			{
//				returnedID = vm_route_travel_withdraw_approvalDAO.manageWriteOperations(vm_route_travel_withdraw_approvalDTO, SessionConstants.UPDATE, -1, userDTO);
//			}
//
//
//
//
//
//
//
//
//
//			if(isPermanentTable)
//			{
//				String inPlaceSubmit = (String)request.getParameter("inplacesubmit");
//
//				if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
//				{
//					getVm_route_travel_withdraw_approval(request, response, returnedID);
//				}
//				else
//				{
//					response.sendRedirect("Vm_route_travel_withdraw_approvalServlet?actionType=search");
//				}
//			}
//			else
//			{
//				commonRequestHandler.validate(vm_route_travel_withdraw_approvalDAO.getDTOByID(returnedID), request, response, userDTO);
//			}
//
//		}
//		catch (Exception e)
//		{
//			e.printStackTrace();
//		}
//	}
//
//
//
//
//
//
//
//
//
//	private void getVm_route_travel_withdraw_approval(HttpServletRequest request, HttpServletResponse response, long id) throws ServletException, IOException
//	{
//		System.out.println("in getVm_route_travel_withdraw_approval");
//		Vm_route_travel_withdraw_approvalDTO vm_route_travel_withdraw_approvalDTO = null;
//		try
//		{
//			vm_route_travel_withdraw_approvalDTO = (Vm_route_travel_withdraw_approvalDTO)vm_route_travel_withdraw_approvalDAO.getDTOByID(id);
//			request.setAttribute("ID", vm_route_travel_withdraw_approvalDTO.iD);
//			request.setAttribute("vm_route_travel_withdraw_approvalDTO",vm_route_travel_withdraw_approvalDTO);
//			request.setAttribute("vm_route_travel_withdraw_approvalDAO",vm_route_travel_withdraw_approvalDAO);
//
//			String URL= "";
//
//			String inPlaceEdit = (String)request.getParameter("inplaceedit");
//			String inPlaceSubmit = (String)request.getParameter("inplacesubmit");
//			String getBodyOnly = (String)request.getParameter("getBodyOnly");
//
//			if(inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase(""))
//			{
//				URL = "vm_route_travel_withdraw_approval/vm_route_travel_withdraw_approvalInPlaceEdit.jsp";
//				request.setAttribute("inplaceedit","");
//			}
//			else if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
//			{
//				URL = "vm_route_travel_withdraw_approval/vm_route_travel_withdraw_approvalSearchRow.jsp";
//				request.setAttribute("inplacesubmit","");
//			}
//			else
//			{
//				if(getBodyOnly  != null && !getBodyOnly.equalsIgnoreCase(""))
//				{
//					URL = "vm_route_travel_withdraw_approval/vm_route_travel_withdraw_approvalEditBody.jsp?actionType=edit";
//				}
//				else
//				{
//					URL = "vm_route_travel_withdraw_approval/vm_route_travel_withdraw_approvalEdit.jsp?actionType=edit";
//				}
//			}
//
//			RequestDispatcher rd = request.getRequestDispatcher(URL);
//			rd.forward(request, response);
//		}
//		catch (NumberFormatException e)
//		{
//			e.printStackTrace();
//		}
//		catch (Exception e)
//		{
//			e.printStackTrace();
//		}
//	}
//
//
//	private void getVm_route_travel_withdraw_approval(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
//	{
//		getVm_route_travel_withdraw_approval(request, response, Long.parseLong(request.getParameter("ID")));
//	}
//
////	private void searchVm_route_travel_withdraw_approval(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException
////	{
////		System.out.println("in  searchVm_route_travel_withdraw_approval 1");
////		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
////		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
////		String ajax = (String)request.getParameter("ajax");
////		boolean hasAjax = false;
////		if(ajax != null && !ajax.equalsIgnoreCase(""))
////		{
////			hasAjax = true;
////		}
////		System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);
////
////        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
////			SessionConstants.NAV_VM_ROUTE_TRAVEL_WITHDRAW_APPROVAL,
////			request,
////			vm_route_travel_withdraw_approvalDAO,
////			SessionConstants.VIEW_VM_ROUTE_TRAVEL_WITHDRAW_APPROVAL,
////			SessionConstants.SEARCH_VM_ROUTE_TRAVEL_WITHDRAW_APPROVAL,
////			tableName,
////			isPermanent,
////			userDTO,
////			filter,
////			true);
////        try
////        {
////			System.out.println("trying to dojob");
////            rnManager.doJob(loginDTO);
////        }
////        catch(Exception e)
////        {
////			System.out.println("failed to dojob" + e);
////        }
////
////		request.setAttribute("vm_route_travel_withdraw_approvalDAO",vm_route_travel_withdraw_approvalDAO);
////        RequestDispatcher rd;
////        if(!isPermanent)
////        {
////        	if(hasAjax == false)
////	        {
////	        	System.out.println("Going to vm_route_travel_withdraw_approval/vm_route_travel_withdraw_approvalApproval.jsp");
////	        	rd = request.getRequestDispatcher("vm_route_travel_withdraw_approval/vm_route_travel_withdraw_approvalApproval.jsp");
////	        }
////	        else
////	        {
////	        	System.out.println("Going to vm_route_travel_withdraw_approval/vm_route_travel_withdraw_approvalApprovalForm.jsp");
////	        	rd = request.getRequestDispatcher("vm_route_travel_withdraw_approval/vm_route_travel_withdraw_approvalApprovalForm.jsp");
////	        }
////        }
////        else
////        {
////	        if(hasAjax == false)
////	        {
////	        	System.out.println("Going to vm_route_travel_withdraw_approval/vm_route_travel_withdraw_approvalSearch.jsp");
////	        	rd = request.getRequestDispatcher("vm_route_travel_withdraw_approval/vm_route_travel_withdraw_approvalSearch.jsp");
////	        }
////	        else
////	        {
////	        	System.out.println("Going to vm_route_travel_withdraw_approval/vm_route_travel_withdraw_approvalSearchForm.jsp");
////	        	rd = request.getRequestDispatcher("vm_route_travel_withdraw_approval/vm_route_travel_withdraw_approvalSearchForm.jsp");
////	        }
////        }
////		rd.forward(request, response);
////	}
//
//}
//
