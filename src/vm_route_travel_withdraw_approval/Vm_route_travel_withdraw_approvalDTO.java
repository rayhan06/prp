package vm_route_travel_withdraw_approval;
import java.util.*; 
import util.*; 


public class Vm_route_travel_withdraw_approvalDTO extends CommonDTO
{

	public long employeeRecordId = -1;
    public String nameBn = "";
    public String nameEn = "";
    public String userName = "";
	public long officeUnitId = -1;
    public String officeUnitEng = "";
    public String officeUnitBng = "";
	public long organogramId = -1;
    public String organogramEng = "";
    public String organogramBng = "";
	public long insertedByUserId = -1;
	public long insertedByOrganogramId = -1;
	public long insertionDate = -1;
	
	
    @Override
	public String toString() {
            return "$Vm_route_travel_withdraw_approvalDTO[" +
            " iD = " + iD +
            " employeeRecordId = " + employeeRecordId +
            " nameBn = " + nameBn +
            " nameEn = " + nameEn +
            " userName = " + userName +
            " officeUnitId = " + officeUnitId +
            " officeUnitEng = " + officeUnitEng +
            " officeUnitBng = " + officeUnitBng +
            " organogramId = " + organogramId +
            " organogramEng = " + organogramEng +
            " organogramBng = " + organogramBng +
            " searchColumn = " + searchColumn +
            " insertedByUserId = " + insertedByUserId +
            " insertedByOrganogramId = " + insertedByOrganogramId +
            " insertionDate = " + insertionDate +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}