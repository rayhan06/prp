package vm_route_travel_withdraw_approval;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import employee_offices.EmployeeOfficeDTO;
import employee_offices.EmployeeOfficeRepository;
import employee_records.Employee_recordsDTO;
import employee_records.Employee_recordsRepository;
import office_unit_organograms.OfficeUnitOrganograms;
import office_unit_organograms.OfficeUnitOrganogramsRepository;
import office_units.Office_unitsDTO;
import office_units.Office_unitsRepository;
import org.apache.log4j.Logger;






public class Vm_route_travel_withdraw_approvalRepository {
	private static final Logger logger = Logger.getLogger(Vm_route_travel_withdraw_approvalRepository.class);
	private final Vm_route_travel_withdraw_approvalDAO cardApprovalDAO;
	private final Map<Long, Vm_route_travel_withdraw_approvalDTO> mapById;
	private final Map<Long, Object> mapByIdLockObject;
	private final Map<Long, Vm_route_travel_withdraw_approvalDTO> mapByEmployeeRecordId;
	private final Map<Long, Object> mapByEmployeeRecordIdLockObject;

	private Vm_route_travel_withdraw_approvalRepository() {
		cardApprovalDAO = new Vm_route_travel_withdraw_approvalDAO();
		mapById = new ConcurrentHashMap<>();
		mapByIdLockObject = new ConcurrentHashMap<>();
		mapByEmployeeRecordId = new ConcurrentHashMap<>();
		mapByEmployeeRecordIdLockObject = new ConcurrentHashMap<>();
		reload();
	}

	private static class Vm_route_travel_withdraw_approvalRepositoryLazyLoader {
		final static Vm_route_travel_withdraw_approvalRepository INSTANCE = new Vm_route_travel_withdraw_approvalRepository();
	}

	public static Vm_route_travel_withdraw_approvalRepository getInstance() {
		return Vm_route_travel_withdraw_approvalRepository.Vm_route_travel_withdraw_approvalRepositoryLazyLoader.INSTANCE;
	}

	private void reload() {
		List<Vm_route_travel_withdraw_approvalDTO> list = cardApprovalDAO.getAllDTOs(true);
		list.forEach(dto -> {
			mapById.put(dto.iD, dto);
			mapByEmployeeRecordId.put(dto.employeeRecordId, dto);
		});
	}

	public Vm_route_travel_withdraw_approvalDTO getByEmployeeRecordId(long employeeRecordId) {
		return getByEmployeeRecordId(employeeRecordId, 0);
	}

	/*
        NullPointerException can be occurred in synchronized (mapByIdLockObject.get(id))
        because for mapByIdLockObject.remove(id).
    */
	public Vm_route_travel_withdraw_approvalDTO getById(long id) {
		if (mapById.get(id) == null) {
			mapByIdLockObject.putIfAbsent(id, new Object());
			try {
				synchronized (mapByIdLockObject.get(id)) {
					if (mapById.get(id) == null) {
						Vm_route_travel_withdraw_approvalDTO dto = cardApprovalDAO.getDTOFromIdDeletedOrNot(id);
						if (dto != null) {
							mapById.put(dto.iD, dto);
							if(dto.isDeleted == 0){
								mapByEmployeeRecordId.put(dto.employeeRecordId, dto);
							}
						}
					}
				}
			} catch (NullPointerException ex) {
				logger.error(ex);
			}
			if (mapById.get(id) != null) {
				mapByIdLockObject.remove(id);
			}
		}
		return mapById.get(id);
	}

    /*
        NullPointerException can be occurred in synchronized (mapByIdLockObject.get(id))
        because for mapByIdLockObject.remove(id).
    */

	public Vm_route_travel_withdraw_approvalDTO getByEmployeeRecordId(long employeeRecordId, long requesterId) {
		mapByEmployeeRecordIdLockObject.putIfAbsent(employeeRecordId, new Object());
		try {
			synchronized (mapByEmployeeRecordIdLockObject.get(employeeRecordId)) {
				if (mapByEmployeeRecordId.get(employeeRecordId) == null) {
					List<Vm_route_travel_withdraw_approvalDTO> dtoList = cardApprovalDAO.getByEmployeeId(employeeRecordId);
					if (dtoList != null && dtoList.size() > 0) {
						Vm_route_travel_withdraw_approvalDTO dto = dtoList.get(0);
						mapByEmployeeRecordId.put(dto.employeeRecordId, dto);
						mapById.put(dto.iD, dto);
					}
				}
				if (mapByEmployeeRecordId.get(employeeRecordId) == null) {
					createApprovalDTOForEmployeeRecordId(employeeRecordId, requesterId);
				} else {
					Vm_route_travel_withdraw_approvalDTO dto = mapByEmployeeRecordId.get(employeeRecordId);
					if (!isMatch(dto)) {
						boolean deleteResult = cardApprovalDAO.deleteById(dto.iD, requesterId);
						if (deleteResult) {
							createApprovalDTOForEmployeeRecordId(employeeRecordId, requesterId);
							mapById.remove(dto.iD);
						}
					}
				}
			}
		} catch (NullPointerException ex) {
			logger.error(ex);
		}
		if (mapByEmployeeRecordId.get(employeeRecordId) != null) {
			mapByEmployeeRecordIdLockObject.remove(employeeRecordId);
		}
		return mapByEmployeeRecordId.get(employeeRecordId);
	}

	private void createApprovalDTOForEmployeeRecordId(long employeeRecordId, long requesterId) {
		System.out.println("here in createApprovalDTOForEmployeeRecordId 1");

		Employee_recordsDTO employeeRecordsDTO = Employee_recordsRepository.getInstance().getById(employeeRecordId);
		if (employeeRecordsDTO == null) {
			System.out.println("here in createApprovalDTOForEmployeeRecordId 2");
			return;
		}
		EmployeeOfficeDTO employeeOfficeDTO = EmployeeOfficeRepository.getInstance().getByEmployeeRecordIdIsDefault(employeeRecordId);
		if (employeeOfficeDTO == null) {
			System.out.println("here in createApprovalDTOForEmployeeRecordId 3");
			return;
		}
		System.out.println("here in createApprovalDTOForEmployeeRecordId 4");
		Office_unitsDTO officeUnitsDTO = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(employeeOfficeDTO.officeUnitId);
		OfficeUnitOrganograms officeUnitOrganograms = OfficeUnitOrganogramsRepository.getInstance().getById(employeeOfficeDTO.officeUnitOrganogramId);
		Vm_route_travel_withdraw_approvalDTO dto = new Vm_route_travel_withdraw_approvalDTO();
		dto.employeeRecordId = employeeRecordId;
		dto.nameEn = employeeRecordsDTO.nameEng;
		dto.nameBn = employeeRecordsDTO.nameBng;
		dto.userName = employeeRecordsDTO.employeeNumber;
		dto.officeUnitId = employeeOfficeDTO.officeUnitId;
		dto.officeUnitEng = officeUnitsDTO.unitNameEng;
		dto.officeUnitBng = officeUnitsDTO.unitNameBng;
		dto.organogramId = employeeOfficeDTO.officeUnitOrganogramId;
		dto.organogramEng = officeUnitOrganograms.designation_eng;
		dto.organogramBng = officeUnitOrganograms.designation_bng;
		dto.insertedByUserId = requesterId;
		dto.insertionDate = System.currentTimeMillis();
		//dto.modifiedBy = requesterId;
		dto.lastModificationTime = System.currentTimeMillis();
		try {
			cardApprovalDAO.add(dto);
			mapById.put(dto.iD, dto);
			mapByEmployeeRecordId.put(dto.employeeRecordId, dto);
		} catch (Exception e) {
			logger.error(e);
		}
	}

	private boolean isMatch(Vm_route_travel_withdraw_approvalDTO dto) {
		Employee_recordsDTO employeeRecordsDTO = Employee_recordsRepository.getInstance().getById(dto.employeeRecordId);
		EmployeeOfficeDTO employeeOfficeDTO = EmployeeOfficeRepository.getInstance().getByEmployeeRecordIdIsDefault(dto.employeeRecordId);
		Office_unitsDTO officeUnitsDTO = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(employeeOfficeDTO.officeUnitId);
		OfficeUnitOrganograms officeUnitOrganograms = OfficeUnitOrganogramsRepository.getInstance().getById(employeeOfficeDTO.officeUnitOrganogramId);
//		return employeeRecordsDTO.nameBng.equals(dto.nameBng)
//				&& employeeRecordsDTO.nameEng.equals(dto.nameEng)
//				&& officeUnitsDTO.iD == dto.officeUnitId
//				&& officeUnitsDTO.unitNameEng.equals(dto.officeUnitEng)
//				&& officeUnitsDTO.unitNameBng.equals(dto.officeUnitBng)
//				&& officeUnitOrganograms.id == dto.organogramId
//				&& officeUnitOrganograms.designation_eng.equals(dto.organogramEng)
//				&& officeUnitOrganograms.designation_bng.equals(dto.organogramBng);

		return  officeUnitsDTO.iD == dto.officeUnitId
				&& officeUnitsDTO.unitNameEng.equals(dto.officeUnitEng)
				&& officeUnitsDTO.unitNameBng.equals(dto.officeUnitBng)
				&& officeUnitOrganograms.id == dto.organogramId
				&& officeUnitOrganograms.designation_eng.equals(dto.organogramEng)
				&& officeUnitOrganograms.designation_bng.equals(dto.organogramBng);
	}

	public List<Vm_route_travel_withdraw_approvalDTO> getByIds(List<Long> ids){
		List<Vm_route_travel_withdraw_approvalDTO> list = new ArrayList<>();
		List<Long> notFoundInCacheList = ids.stream()
				.peek(id->addToListIfFoundInCache(id,list))
				.filter(id->mapById.get(id) == null)
				.collect(Collectors.toList());
		if(notFoundInCacheList.size()>0){
			List<Vm_route_travel_withdraw_approvalDTO> list1 = cardApprovalDAO.getDTOs(notFoundInCacheList);
			if(list1 !=null && list1.size()>0){
				list.addAll(list1);
			}
		}
		return list;
	}
	private void addToListIfFoundInCache(long id,List<Vm_route_travel_withdraw_approvalDTO> list){
		Vm_route_travel_withdraw_approvalDTO dto = mapById.get(id);
		if(dto != null){
			list.add(dto);
		}
	}
}


