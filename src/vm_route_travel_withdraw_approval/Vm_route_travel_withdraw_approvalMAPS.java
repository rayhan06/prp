package vm_route_travel_withdraw_approval;
import java.util.*; 
import util.*;


public class Vm_route_travel_withdraw_approvalMAPS extends CommonMaps
{	
	public Vm_route_travel_withdraw_approvalMAPS(String tableName)
	{
		

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("employeeRecordId".toLowerCase(), "employeeRecordId".toLowerCase());
		java_DTO_map.put("nameBn".toLowerCase(), "nameBn".toLowerCase());
		java_DTO_map.put("nameEn".toLowerCase(), "nameEn".toLowerCase());
		java_DTO_map.put("userName".toLowerCase(), "userName".toLowerCase());
		java_DTO_map.put("officeUnitId".toLowerCase(), "officeUnitId".toLowerCase());
		java_DTO_map.put("officeUnitEng".toLowerCase(), "officeUnitEng".toLowerCase());
		java_DTO_map.put("officeUnitBng".toLowerCase(), "officeUnitBng".toLowerCase());
		java_DTO_map.put("organogramId".toLowerCase(), "organogramId".toLowerCase());
		java_DTO_map.put("organogramEng".toLowerCase(), "organogramEng".toLowerCase());
		java_DTO_map.put("organogramBng".toLowerCase(), "organogramBng".toLowerCase());
		java_DTO_map.put("searchColumn".toLowerCase(), "searchColumn".toLowerCase());
		java_DTO_map.put("insertedByUserId".toLowerCase(), "insertedByUserId".toLowerCase());
		java_DTO_map.put("insertedByOrganogramId".toLowerCase(), "insertedByOrganogramId".toLowerCase());
		java_DTO_map.put("insertionDate".toLowerCase(), "insertionDate".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());


		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Employee Record Id".toLowerCase(), "employeeRecordId".toLowerCase());
		java_Text_map.put("Bangla Name".toLowerCase(), "nameBn".toLowerCase());
		java_Text_map.put("English Name".toLowerCase(), "nameEn".toLowerCase());
		java_Text_map.put("User Name".toLowerCase(), "userName".toLowerCase());
		java_Text_map.put("Office Unit Id".toLowerCase(), "officeUnitId".toLowerCase());
		java_Text_map.put("Office Unit Eng".toLowerCase(), "officeUnitEng".toLowerCase());
		java_Text_map.put("Office Unit Bng".toLowerCase(), "officeUnitBng".toLowerCase());
		java_Text_map.put("Organogram Id".toLowerCase(), "organogramId".toLowerCase());
		java_Text_map.put("Organogram Eng".toLowerCase(), "organogramEng".toLowerCase());
		java_Text_map.put("Organogram Bng".toLowerCase(), "organogramBng".toLowerCase());
		java_Text_map.put("Search Column".toLowerCase(), "searchColumn".toLowerCase());
		java_Text_map.put("Inserted By User Id".toLowerCase(), "insertedByUserId".toLowerCase());
		java_Text_map.put("Inserted By Organogram Id".toLowerCase(), "insertedByOrganogramId".toLowerCase());
		java_Text_map.put("Insertion Date".toLowerCase(), "insertionDate".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}