package budget_submission_finance;

import budget.*;
import budget_selection_info.BudgetSelectionInfoDAO;
import budget_selection_info.BudgetSelectionInfoDTO;
import budget_selection_info.BudgetSelectionInfoRepository;
import budget_submission_info.Budget_submission_infoDAO;
import budget_submission_info.Budget_submission_infoDTO;
import budget_submission_info.Budget_submission_infoRepository;
import com.google.gson.Gson;
import common.BaseServlet;
import common.CommonDAOService;
import login.LoginDTO;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import permission.MenuConstants;
import role.PermissionRepository;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.CommonConstant;
import util.CommonDTO;
import util.StringUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;
import java.util.stream.Collectors;

@WebServlet("/BudgetSubmissionFinanceServlet")
@MultipartConfig
public class BudgetSubmissionFinanceServlet extends BaseServlet {
    private static final long serialVersionUID = 1L;
    public static final Logger logger = Logger.getLogger(BudgetSubmissionFinanceServlet.class);
    private static final String tableName = "budget";
    private final BudgetDAO budgetDAO = BudgetDAO.getInstance();
    private final Gson gson = new Gson();


    @Override
    public String getTableName() {
        return tableName;
    }

    @Override
    public String getServletName() {
        return "BudgetSubmissionFinanceServlet";
    }

    @Override
    public CommonDAOService<? extends CommonDTO> getCommonDAOService() {
        return null;
    }

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        return null;
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[0];
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[0];
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[0];
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return BudgetSubmissionFinanceServlet.class;
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (hasPermission(userDTO)) {
            try {
                String language = userDTO.languageID == CommonConstant.Language_ID_English ? "English" : "Bangla";
                String actionType = request.getParameter("actionType");
                switch (actionType) {
                    case "getAddPage":
                        request.getRequestDispatcher("budget_submission_finance/budget_submission_finance.jsp").forward(request, response);
                        return;
                    case "getPreviewPage":
                        setPreviewPageBudgetData(request, userDTO);
                        request.getRequestDispatcher("budget_submission_finance/budget_submission_finance_preview.jsp").forward(request, response);
                        return;
                    case "getBudgetModels":
                        sendBudgetModels(request, response, language);
                        return;
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                logger.error(ex);
            }
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);

        if (hasPermission(userDTO)) {
            try {
                String actionType = request.getParameter("actionType");

                switch (actionType) {
                    case "insertBudgetForFinance": {
                        boolean saveSuccess = saveBudgetForFinance(userDTO, request);
                        String resJson = "{\"success\": true}";
                        if (!saveSuccess)
                            resJson = "{\"success\": false}";
                        PrintWriter out = response.getWriter();
                        response.setContentType("application/json");
                        response.setCharacterEncoding("UTF-8");
                        out.print(resJson);
                        out.flush();
                        return;
                    }
                    case "submitBudgetForFinance": {
                        submitBudgetForFinance(userDTO);
                        String resJson = "{\"success\": true}";
                        PrintWriter out = response.getWriter();
                        response.setContentType("application/json");
                        response.setCharacterEncoding("UTF-8");
                        out.print(resJson);
                        out.flush();
                        return;
                    }
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                logger.error(ex);
            }
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }

    private boolean hasPermission(UserDTO userDTO) {
        return userDTO != null && PermissionRepository.checkPermissionByRoleIDAndMenuID(
                userDTO.roleID, MenuConstants.BUDGET_SUBMISSION_FINANCE
        );
    }

    private void sendBudgetModels(HttpServletRequest request, HttpServletResponse response, String language) throws Exception {
        BudgetInfo budgetInfo = BudgetUtils.getBudgetInfoForBudgetSubmission(System.currentTimeMillis());
        BudgetSelectionInfoDTO budgetSelectionInfoDTO = BudgetSelectionInfoRepository.getInstance().getDTOByEconomicYear(budgetInfo.economicYear);
        boolean isCodeSelectionDone = budgetSelectionInfoDTO != null && budgetSelectionInfoDTO.isSubmitted;
        if (!isCodeSelectionDone) throw new Exception("Budget Code Selection Not Done Yet!");

        // mandatory param
        long budgetSelectionInfoId = budgetSelectionInfoDTO.iD;
        long budgetOfficeId = Long.parseLong(request.getParameter("budgetOfficeId").trim());
        long budgetOperationId = Long.parseLong(request.getParameter("budgetOperationId").trim());

        // optional param
        Long economicGroupId = StringUtils.parseNullableLong(request.getParameter("economicGroupId"));
        Long economicCodeId = StringUtils.parseNullableLong(request.getParameter("economicCodeId"));

        List<BudgetModel> subCodeModelList = new ArrayList<>();
        boolean hasOfficeSubmittedBudget = Budget_submission_infoRepository.getInstance().hasOfficeSubmitted(
                budgetSelectionInfoId, budgetOfficeId, budgetInfo.budgetTypeEnum.getValue()
        );
        if (hasOfficeSubmittedBudget) {
            List<BudgetDTO> budgetDTOList = budgetDAO.getByBudgetOfficeAndOperationAndCodeAndSelectionIds(
                    budgetOfficeId,
                    budgetOperationId,
                    economicGroupId,
                    economicCodeId,
                    BudgetUtils.getSelectionIds(budgetInfo.beginYear, 1)
            );

            Map<Long, List<BudgetDTO>> mapBySelectionInfoId = budgetDTOList.stream()
                                                                           .collect(Collectors.groupingBy(e -> e.budgetSelectionInfoId));

            List<Map<Long, AmountModel>> previousAmountsGroupedBySubCode =
                    BudgetUtils.getPreviousAmountsGroupedBySubCode(budgetInfo.beginYear, 1, mapBySelectionInfoId);

            subCodeModelList =
                    mapBySelectionInfoId.get(budgetSelectionInfoId).stream()
                                        .map(dto -> new BudgetModel(dto, language))
                                        .sorted(Comparator.comparing(a -> a.economicSubCode))
                                        .peek(dto -> dto.setPreviousAmounts(previousAmountsGroupedBySubCode))
                                        .collect(Collectors.toList());
        }

        Map<String, Object> responseObject = new HashMap<>();
        responseObject.put("hasOfficeSubmittedBudget", hasOfficeSubmittedBudget);
        responseObject.put("subCodeModels", subCodeModelList);

        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().println(gson.toJson(responseObject));
        response.getWriter().flush();
    }

    private void submitBudgetForFinance(UserDTO userDTO) throws Exception {
        BudgetInfo budgetInfo = BudgetUtils.getBudgetInfoForBudgetSubmission(System.currentTimeMillis());
        BudgetSelectionInfoDTO budgetSelectionInfoDTO = BudgetSelectionInfoDAO.getInstance().getDTOByEconomicYear(budgetInfo.economicYear);

        boolean isCodeSelectionDone = budgetSelectionInfoDTO != null && budgetSelectionInfoDTO.isSubmitted;
        if (!isCodeSelectionDone)
            throw new Exception("Budget Code Selection has not yet been completed for economic year ".concat(budgetInfo.economicYear));

        Budget_submission_infoDAO.getInstance().submitBudget(
                budgetSelectionInfoDTO.iD,
                BudgetUtils.FINANCE_SECTION_OFFICE_ID,
                budgetInfo.budgetTypeEnum.getValue(),
                userDTO.ID
        );
    }

    private boolean saveBudgetForFinance(UserDTO userDTO, HttpServletRequest request) throws Exception {
        BudgetInfo budgetInfo = BudgetUtils.getBudgetInfoForBudgetSubmission(System.currentTimeMillis());
        BudgetSelectionInfoDTO budgetSelectionInfoDTO = BudgetSelectionInfoDAO.getInstance().getDTOByEconomicYear(budgetInfo.economicYear);
        boolean isCodeSelectionDone = budgetSelectionInfoDTO != null && budgetSelectionInfoDTO.isSubmitted;
        if (!isCodeSelectionDone) {
            logger.error("Budget Code Selection has not yet been completed for economic year ".concat(budgetInfo.economicYear));
            return false;
        }

        int budgetTypeCat = budgetInfo.budgetTypeEnum.getValue();
        Budget_submission_infoDTO budget_submission_infoDTO = Budget_submission_infoDAO.getInstance()
                .getOrCreateAndGetDTO(
                        budgetSelectionInfoDTO.iD,
                        budgetTypeCat,
                        BudgetUtils.FINANCE_SECTION_OFFICE_ID,
                        userDTO.ID
                );

        boolean isBudgetSubmitted = budget_submission_infoDTO != null
                                    && budget_submission_infoDTO.isSubmit == SubmissionStatusEnum.SUBMITTED.getValue();
        if (isBudgetSubmitted) {
            logger.error("Office has already submitted budget");
            return false;
        }

        String Value = request.getParameter("budgetModels");
        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }

        BudgetModel[] budgetModels = gson.fromJson(Value, BudgetModel[].class);
        for (BudgetModel budgetModel : budgetModels) {
            updateBudget(budgetModel, budgetTypeCat, userDTO.ID, System.currentTimeMillis());
        }
        return true;
    }

    private void updateBudget(BudgetModel model, int budgetTypeCat, long requestBy, long requestTime) {
        if (budgetTypeCat == BudgetTypeEnum.BUDGET.getValue()) {
            budgetDAO.updateFinalAmountById(model, requestBy, requestTime);
        } else if (budgetTypeCat == BudgetTypeEnum.REVISED_BUDGET.getValue()) {
            budgetDAO.updateRevisedFinalAmountById(model, requestBy, requestTime);
        }
    }

    private void setPreviewPageBudgetData(HttpServletRequest request, UserDTO userDTO) {
        boolean isLangEng = userDTO.languageID == CommonConstant.Language_ID_English;
        BudgetSelectionInfoDAO budgetSelectionInfoDAO = BudgetSelectionInfoDAO.getInstance();

        BudgetInfo thisYearBudgetInfo = BudgetUtils.getBudgetInfoForBudgetSubmission(System.currentTimeMillis());
        BudgetSelectionInfoDTO thisYearSelectionDTO = budgetSelectionInfoDAO.getDTOByEconomicYear(thisYearBudgetInfo.economicYear);
        boolean isCodeSelectionDone = thisYearSelectionDTO != null && thisYearSelectionDTO.isSubmitted;
        if (!isCodeSelectionDone) {
            String errorMessage = isLangEng
                                  ? "Budget code selection process has not yet been completed for this Economic Year "
                                  : "এই অর্থ বছরের বাজেটের কোড বাছাইকরণ প্রক্রিয়া এখনো সম্পন্ন হয়নি";
            request.setAttribute("errorMessage", errorMessage);
            return;
        }

        request.setAttribute("thisYearBudgetInfo", thisYearBudgetInfo);
        request.setAttribute("thisYearSelectionDTO", thisYearSelectionDTO);

        BudgetSelectionInfoDTO prevYearSelectionDTO = budgetSelectionInfoDAO.getDTOByEconomicYear(
                BudgetInfo.getEconomicYear(thisYearBudgetInfo.beginYear - 1)
        );
        long thisYearId = thisYearSelectionDTO.iD;
        Long prevYearId = prevYearSelectionDTO == null ? null : prevYearSelectionDTO.iD;

        Map<Long, List<BudgetDTO>> budgetDTOsByYear = budgetDAO.getDTOsBySelectionInfoIds(Arrays.asList(thisYearId, prevYearId))
                                                               .stream()
                                                               .collect(Collectors.groupingBy(dto -> dto.budgetSelectionInfoId));

        List<BudgetDTO> thisYearBudgetDTOs = budgetDTOsByYear.get(thisYearId);
        request.setAttribute("thisYearBudgetDTOs", thisYearBudgetDTOs);

        List<BudgetDTO> prevYearBudgetDTOs = budgetDTOsByYear.get(prevYearId);
        Map<Long, List<BudgetDTO>> prevYearBudgetDTOsByMappingId =
                prevYearBudgetDTOs == null ? new HashMap<>()
                                           : prevYearBudgetDTOs.stream()
                                                               .collect(Collectors.groupingBy(dto -> dto.budgetMappingId));
        request.setAttribute("prevYearBudgetDTOsByMappingId", prevYearBudgetDTOsByMappingId);
    }

    public static boolean hasAllOfficeSubmitted(long budgetSelectionInfoId, int budgetTypeCat) {
        Set<Long> officeIds = BudgetDAO.getInstance().getAllOffices(budgetSelectionInfoId);
        officeIds.remove(BudgetUtils.FINANCE_SECTION_OFFICE_ID);
        return Budget_submission_infoRepository.getInstance().hasAllOfficeSubmitted(
                officeIds,
                budgetSelectionInfoId,
                budgetTypeCat
        );
    }
}

