package vm_maintenance_vehicle_item_mapping;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;
import sessionmanager.SessionConstants;
import java.sql.SQLException;
import common.ConnectionAndStatementUtil;


import org.apache.log4j.Logger;

import dbm.*;

import repository.RepositoryManager;

import util.*;
import pb.*;
import user.UserDTO;

public class Vm_maintenance_vehicle_item_mappingDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());

	public Vm_maintenance_vehicle_item_mappingDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";
		commonMaps = new Vm_maintenance_vehicle_item_mappingMAPS(tableName);
		columnNames = new String[] 
		{
			"ID",
			"vm_id",
			"parts_id",
			"date",
			"inserted_by_user_id",
			"inserted_by_organogram_id",
			"insertion_date",
			"isDeleted",
			"lastModificationTime"
		};
	}
	
	public Vm_maintenance_vehicle_item_mappingDAO()
	{
		this("vm_maintenance_vehicle_item_mapping");		
	}
	
	public void setSearchColumn(Vm_maintenance_vehicle_item_mappingDTO vm_maintenance_vehicle_item_mappingDTO)
	{
		vm_maintenance_vehicle_item_mappingDTO.searchColumn = "";
		vm_maintenance_vehicle_item_mappingDTO.searchColumn += vm_maintenance_vehicle_item_mappingDTO.date + " ";
	}
	
	public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException
	{
		Vm_maintenance_vehicle_item_mappingDTO vm_maintenance_vehicle_item_mappingDTO = (Vm_maintenance_vehicle_item_mappingDTO)commonDTO;
		int index = 1;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(vm_maintenance_vehicle_item_mappingDTO);
		if(isInsert)
		{
			ps.setObject(index++,vm_maintenance_vehicle_item_mappingDTO.iD);
		}
		ps.setObject(index++,vm_maintenance_vehicle_item_mappingDTO.vmId);
		ps.setObject(index++,vm_maintenance_vehicle_item_mappingDTO.partsId);
		ps.setObject(index++,vm_maintenance_vehicle_item_mappingDTO.date);
		ps.setObject(index++,vm_maintenance_vehicle_item_mappingDTO.insertedByUserId);
		ps.setObject(index++,vm_maintenance_vehicle_item_mappingDTO.insertedByOrganogramId);
		ps.setObject(index++,vm_maintenance_vehicle_item_mappingDTO.insertionDate);
		if(isInsert)
		{
			ps.setObject(index++, 0);
			ps.setObject(index++, lastModificationTime);
		}
	}
	
	public Vm_maintenance_vehicle_item_mappingDTO build(ResultSet rs)
	{
		try
		{
			Vm_maintenance_vehicle_item_mappingDTO vm_maintenance_vehicle_item_mappingDTO = new Vm_maintenance_vehicle_item_mappingDTO();
			vm_maintenance_vehicle_item_mappingDTO.iD = rs.getLong("ID");
			vm_maintenance_vehicle_item_mappingDTO.vmId = rs.getLong("vm_id");
			vm_maintenance_vehicle_item_mappingDTO.partsId = rs.getLong("parts_id");
			vm_maintenance_vehicle_item_mappingDTO.date = rs.getLong("date");
			vm_maintenance_vehicle_item_mappingDTO.insertedByUserId = rs.getLong("inserted_by_user_id");
			vm_maintenance_vehicle_item_mappingDTO.insertedByOrganogramId = rs.getLong("inserted_by_organogram_id");
			vm_maintenance_vehicle_item_mappingDTO.insertionDate = rs.getLong("insertion_date");
			vm_maintenance_vehicle_item_mappingDTO.isDeleted = rs.getInt("isDeleted");
			vm_maintenance_vehicle_item_mappingDTO.lastModificationTime = rs.getLong("lastModificationTime");
			return vm_maintenance_vehicle_item_mappingDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
	
	

	public Vm_maintenance_vehicle_item_mappingDTO getDTOByID (long id)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID = ? ";
		Vm_maintenance_vehicle_item_mappingDTO vm_maintenance_vehicle_item_mappingDTO = ConnectionAndStatementUtil.getT(sql, Arrays.asList(id), this::build);
		return vm_maintenance_vehicle_item_mappingDTO;
	}

	
	public List<Vm_maintenance_vehicle_item_mappingDTO> getDTOs(Collection recordIDs)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID IN ( ";
		for(int i = 0;i<recordIDs.size();i++){
			if(i!=0){
				sql+=",";
			}
			sql+= " ? ";
		}
		sql+=")  order by lastModificationTime desc";
		printSql(sql);
		return ConnectionAndStatementUtil.getListOfT(sql, Arrays.asList(recordIDs.toArray()),this::build);
	}
	
	public List<Vm_maintenance_vehicle_item_mappingDTO> getAllVm_maintenance_vehicle_item_mapping (boolean isFirstReload)
    {
		String sql = "SELECT * FROM " + tableName + " WHERE ";
		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by " + tableName + ".lastModificationTime desc";
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);	
    }

	public List<Vm_maintenance_vehicle_item_mappingDTO> getAllVm_maintenance_vehicle_item_mappingByVehicleIdAndPartsId
			(long vehicleId, long partsId)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ";
		sql+=" isDeleted =  0 and vm_id = ?  and parts_id = ? ";
		sql += " order by " + tableName + ".lastModificationTime desc";
		return ConnectionAndStatementUtil.getListOfT(sql, Arrays.asList(vehicleId, partsId), this::build);
	}

	public String getDate (long vehicleId, long partsId)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ";
		sql+=" isDeleted =  0 and vm_id = ? and parts_id = ? ";
		sql += " order by " + tableName + ".lastModificationTime desc";

		Vm_maintenance_vehicle_item_mappingDTO vm_maintenance_vehicle_item_mappingDTO =
				ConnectionAndStatementUtil.getT(sql,Arrays.asList(vehicleId, partsId),this::build);
		String value = "";
		if(vm_maintenance_vehicle_item_mappingDTO != null){
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
			value = simpleDateFormat.format(new Date(Long.parseLong
					(vm_maintenance_vehicle_item_mappingDTO.date + "")));
		}
		return value;
	}

	
	public List<Vm_maintenance_vehicle_item_mappingDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO)
	{
		return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
	}

	
	public List<Vm_maintenance_vehicle_item_mappingDTO> getDTOs(Hashtable  p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
			String filter, boolean tableHasJobCat)
	{
		String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat);
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);			
	}
	public String getSqlWithSearchCriteriaForNormalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
			UserDTO userDTO, String filter, boolean tableHasJobCat)
    {
		boolean viewAll = false;
		
		if(p_searchCriteria != null && p_searchCriteria.get("ViewAll") != null)
		{
			System.out.println("ViewAll = " + p_searchCriteria.get("ViewAll"));
			viewAll = true;
		}
		else
		{
			System.out.println("ViewAll is null ");
		}
		
		String sql = "SELECT ";
		if(category == GETIDS)
		{
			sql += " " + tableName + ".ID as ID ";
		}
		else if(category == GETCOUNT)
		{
			sql += " count( " + tableName + ".ID) as countID ";
		}
		else if(category == GETDTOS)
		{
			sql += " distinct " + tableName + ".* ";
		}
		sql += "FROM " + tableName + " ";
		sql += joinSQL;
		

		
		String AnyfieldSql = "";
		String AllFieldSql = "";
		
		if(p_searchCriteria != null)
		{
						
			
			Enumeration names = p_searchCriteria.keys();
			String str, value;
			
			AnyfieldSql = "(";
			
			if(p_searchCriteria.get("AnyField")!= null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase(""))
			{
				AnyfieldSql+= tableName + ".search_column like '%" + p_searchCriteria.get("AnyField").toString() + "%'";			
			}
			AnyfieldSql += ")";
			System.out.println("AnyfieldSql = " + AnyfieldSql);
			
			AllFieldSql = "(";
			int i = 0;
			while(names.hasMoreElements())
			{				
				str = (String) names.nextElement();
				value = (String)p_searchCriteria.get(str);
		        System.out.println(str + ": " + value);
				if(value != null && !value.equalsIgnoreCase("") && (
						 str.equals("date")
						|| str.equals("insertion_date_start")
						|| str.equals("insertion_date_end")
				)
						
				)
				{
					if(p_searchCriteria.get(str).equals("any"))
					{
						continue;
					}

					if( i > 0)
					{
						AllFieldSql+= " AND  ";
					}
					
					 if(str.equals("date"))
					{
						AllFieldSql += "" + tableName + ".date like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("insertion_date_start"))
					{
						AllFieldSql += "" + tableName + ".insertion_date >= " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("insertion_date_end"))
					{
						AllFieldSql += "" + tableName + ".insertion_date <= " + p_searchCriteria.get(str);
						i ++;
					}
					
					
				}
			}
			
			AllFieldSql += ")";
			System.out.println("AllFieldSql = " + AllFieldSql);
			
			
		}
		
		
		sql += " WHERE ";
		
		sql += " (" + tableName + ".isDeleted = 0 ";			
		sql += ")";
		
		
		if(!filter.equalsIgnoreCase(""))
		{
			sql += " and " + filter + " ";
		}
		
		if(!AnyfieldSql.equals("()") && !AnyfieldSql.equals(""))
		{
			sql += " AND " + AnyfieldSql;
			
		}
		if(!AllFieldSql.equals("()") && !AllFieldSql.equals(""))
		{			
			sql += " AND " + AllFieldSql;
		}
		
	
			
		sql += " order by " + tableName + ".lastModificationTime desc ";

		printSql(sql);
		
		if(limit >= 0)
		{
			sql += " limit " + limit;
		}
		if(offset >= 0)
		{
			sql += " offset " + offset;
		}
		
		System.out.println("-------------- sql = " + sql);
		
		return sql;
    }
	
	public String getSqlWithSearchCriteria(Hashtable p_searchCriteria, int limit, int offset, int category,
			boolean isPermanentTable, UserDTO userDTO, String filter, boolean tableHasJobCat)
    {		
		return getSqlWithSearchCriteriaForNormalSearch(p_searchCriteria, limit, offset, category, userDTO, filter, tableHasJobCat);	
    }
				
}
	