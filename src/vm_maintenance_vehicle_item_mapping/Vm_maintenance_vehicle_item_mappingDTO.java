package vm_maintenance_vehicle_item_mapping;
import java.util.*; 
import util.*; 


public class Vm_maintenance_vehicle_item_mappingDTO extends CommonDTO
{

	public long vmId = -1;
	public long partsId = -1;
	public long date = -1;
	public long insertedByUserId = -1;
	public long insertedByOrganogramId = -1;
	public long insertionDate = -1;
	
	
    @Override
	public String toString() {
            return "$Vm_maintenance_vehicle_item_mappingDTO[" +
            " iD = " + iD +
            " vmId = " + vmId +
            " partsId = " + partsId +
            " date = " + date +
            " insertedByUserId = " + insertedByUserId +
            " insertedByOrganogramId = " + insertedByOrganogramId +
            " insertionDate = " + insertionDate +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}