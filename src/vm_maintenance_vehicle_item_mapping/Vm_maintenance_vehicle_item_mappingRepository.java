package vm_maintenance_vehicle_item_mapping;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class Vm_maintenance_vehicle_item_mappingRepository implements Repository {
	Vm_maintenance_vehicle_item_mappingDAO vm_maintenance_vehicle_item_mappingDAO =
			new Vm_maintenance_vehicle_item_mappingDAO();
	
	public void setDAO(Vm_maintenance_vehicle_item_mappingDAO vm_maintenance_vehicle_item_mappingDAO)
	{
		this.vm_maintenance_vehicle_item_mappingDAO = vm_maintenance_vehicle_item_mappingDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Vm_maintenance_vehicle_item_mappingRepository.class);
	Map<Long, Vm_maintenance_vehicle_item_mappingDTO>mapOfVm_maintenance_vehicle_item_mappingDTOToiD;


	static Vm_maintenance_vehicle_item_mappingRepository instance = null;  
	private Vm_maintenance_vehicle_item_mappingRepository(){
		mapOfVm_maintenance_vehicle_item_mappingDTOToiD = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Vm_maintenance_vehicle_item_mappingRepository getInstance(){
		if (instance == null){
			instance = new Vm_maintenance_vehicle_item_mappingRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(vm_maintenance_vehicle_item_mappingDAO == null)
		{
			return;
		}
		try {
			List<Vm_maintenance_vehicle_item_mappingDTO> vm_maintenance_vehicle_item_mappingDTOs = vm_maintenance_vehicle_item_mappingDAO.getAllVm_maintenance_vehicle_item_mapping(reloadAll);
			for(Vm_maintenance_vehicle_item_mappingDTO vm_maintenance_vehicle_item_mappingDTO : vm_maintenance_vehicle_item_mappingDTOs) {
				Vm_maintenance_vehicle_item_mappingDTO oldVm_maintenance_vehicle_item_mappingDTO = getVm_maintenance_vehicle_item_mappingDTOByID(vm_maintenance_vehicle_item_mappingDTO.iD);
				if( oldVm_maintenance_vehicle_item_mappingDTO != null ) {
					mapOfVm_maintenance_vehicle_item_mappingDTOToiD.remove(oldVm_maintenance_vehicle_item_mappingDTO.iD);

					
					
				}
				if(vm_maintenance_vehicle_item_mappingDTO.isDeleted == 0) 
				{
					
					mapOfVm_maintenance_vehicle_item_mappingDTOToiD.put(vm_maintenance_vehicle_item_mappingDTO.iD, vm_maintenance_vehicle_item_mappingDTO);

				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Vm_maintenance_vehicle_item_mappingDTO> getVm_maintenance_vehicle_item_mappingList() {
		List <Vm_maintenance_vehicle_item_mappingDTO> vm_maintenance_vehicle_item_mappings = new ArrayList<Vm_maintenance_vehicle_item_mappingDTO>(this.mapOfVm_maintenance_vehicle_item_mappingDTOToiD.values());
		return vm_maintenance_vehicle_item_mappings;
	}
	
	
	public Vm_maintenance_vehicle_item_mappingDTO getVm_maintenance_vehicle_item_mappingDTOByID( long ID){
		return mapOfVm_maintenance_vehicle_item_mappingDTOToiD.get(ID);
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "vm_maintenance_vehicle_item_mapping";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


