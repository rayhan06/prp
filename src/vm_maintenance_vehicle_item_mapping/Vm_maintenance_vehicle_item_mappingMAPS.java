package vm_maintenance_vehicle_item_mapping;
import java.util.*; 
import util.*;


public class Vm_maintenance_vehicle_item_mappingMAPS extends CommonMaps
{	
	public Vm_maintenance_vehicle_item_mappingMAPS(String tableName)
	{
		

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("vmId".toLowerCase(), "vmId".toLowerCase());
		java_DTO_map.put("partsId".toLowerCase(), "partsId".toLowerCase());
		java_DTO_map.put("date".toLowerCase(), "date".toLowerCase());
		java_DTO_map.put("insertedByUserId".toLowerCase(), "insertedByUserId".toLowerCase());
		java_DTO_map.put("insertedByOrganogramId".toLowerCase(), "insertedByOrganogramId".toLowerCase());
		java_DTO_map.put("insertionDate".toLowerCase(), "insertionDate".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("vm_id".toLowerCase(), "vmId".toLowerCase());
		java_SQL_map.put("parts_id".toLowerCase(), "partsId".toLowerCase());
		java_SQL_map.put("date".toLowerCase(), "date".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Vm Id".toLowerCase(), "vmId".toLowerCase());
		java_Text_map.put("Parts Id".toLowerCase(), "partsId".toLowerCase());
		java_Text_map.put("Date".toLowerCase(), "date".toLowerCase());
		java_Text_map.put("Inserted By User Id".toLowerCase(), "insertedByUserId".toLowerCase());
		java_Text_map.put("Inserted By Organogram Id".toLowerCase(), "insertedByOrganogramId".toLowerCase());
		java_Text_map.put("Insertion Date".toLowerCase(), "insertionDate".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}