package vm_maintenance_vehicle_item_mapping;

import java.io.IOException;
import java.io.*;
import java.text.SimpleDateFormat;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import org.apache.log4j.Logger;

import login.LoginDTO;
import permission.MenuConstants;
import role.PermissionRepository;


import sessionmanager.SessionConstants;

import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;
import util.CommonRequestHandler;
import util.RecordNavigationManager4;

import java.util.*;
import javax.servlet.http.*;
import java.util.UUID;





import geolocation.GeoLocationDAO2;
import java.util.StringTokenizer;

import com.google.gson.Gson;

import pb.*;
import pbReport.*;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;




/**
 * Servlet implementation class Vm_maintenance_vehicle_item_mappingServlet
 */
@WebServlet("/Vm_maintenance_vehicle_item_mappingServlet")
@MultipartConfig
public class Vm_maintenance_vehicle_item_mappingServlet extends HttpServlet
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Vm_maintenance_vehicle_item_mappingServlet.class);

    String tableName = "vm_maintenance_vehicle_item_mapping";

	Vm_maintenance_vehicle_item_mappingDAO vm_maintenance_vehicle_item_mappingDAO;
	CommonRequestHandler commonRequestHandler;
    private final Gson gson = new Gson();

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Vm_maintenance_vehicle_item_mappingServlet()
	{
        super();
    	try
    	{
			vm_maintenance_vehicle_item_mappingDAO = new Vm_maintenance_vehicle_item_mappingDAO(tableName);
			commonRequestHandler = new CommonRequestHandler(vm_maintenance_vehicle_item_mappingDAO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		System.out.println("In doget request = " + request);
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		try
		{
			String actionType = request.getParameter("actionType");
			if(actionType.equals("getAddPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_MAINTENANCE_VEHICLE_ITEM_MAPPING_ADD))
				{
					commonRequestHandler.getAddPage(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getEditPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_MAINTENANCE_VEHICLE_ITEM_MAPPING_UPDATE))
				{
					getVm_maintenance_vehicle_item_mapping(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getURL"))
			{
				String URL = request.getParameter("URL");
				System.out.println("URL = " + URL);
				response.sendRedirect(URL);
			}
			else if(actionType.equals("search"))
			{
				System.out.println("search requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_MAINTENANCE_VEHICLE_ITEM_MAPPING_SEARCH))
				{
					if(isPermanentTable)
					{
						String filter = request.getParameter("filter");
						System.out.println("filter = " + filter);
						if(filter!=null)
						{
							filter = ""; //shouldn't be directly used, rather manipulate it.
							searchVm_maintenance_vehicle_item_mapping(request, response, isPermanentTable, filter);
						}
						else
						{
							searchVm_maintenance_vehicle_item_mapping(request, response, isPermanentTable, "");
						}
					}
					else
					{
						//searchVm_maintenance_vehicle_item_mapping(request, response, tempTableName, isPermanentTable);
					}
				}
			}
			else if(actionType.equals("view"))
			{
				System.out.println("view requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_MAINTENANCE_VEHICLE_ITEM_MAPPING_SEARCH))
				{
					commonRequestHandler.view(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}
			else
			{
				request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}





	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		// TODO Auto-generated method stub
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		System.out.println("doPost");
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);

		try
		{
			String actionType = request.getParameter("actionType");
			System.out.println("actionType = " + actionType);
			if(actionType.equals("add"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_MAINTENANCE_VEHICLE_ITEM_MAPPING_ADD))
				{
					System.out.println("going to  addVm_maintenance_vehicle_item_mapping ");
					addVm_maintenance_vehicle_item_mapping(request, response, true, userDTO, true);
				}
				else
				{
					System.out.println("Not going to  addVm_maintenance_vehicle_item_mapping ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}

			else if(actionType.equals("getDTO"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_MAINTENANCE_VEHICLE_ITEM_MAPPING_ADD))
				{
					getDTO(request, response);
				}
				else
				{
					System.out.println("Not going to  addVm_maintenance_vehicle_item_mapping ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}
			else if(actionType.equals("edit"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_MAINTENANCE_VEHICLE_ITEM_MAPPING_UPDATE))
				{
					addVm_maintenance_vehicle_item_mapping(request, response, false, userDTO, isPermanentTable);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("delete"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_MAINTENANCE_VEHICLE_ITEM_MAPPING_UPDATE))
				{
					commonRequestHandler.delete(request, response, userDTO);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("search"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_MAINTENANCE_VEHICLE_ITEM_MAPPING_SEARCH))
				{
					searchVm_maintenance_vehicle_item_mapping(request, response, true, "");
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getGeo"))
			{
				System.out.println("going to geoloc ");
				request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
			}

		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}

	private void getDTO(HttpServletRequest request, HttpServletResponse response)
	{
		try
		{
			System.out.println("In getDTO");
			Vm_maintenance_vehicle_item_mappingDTO vm_maintenance_vehicle_item_mappingDTO = vm_maintenance_vehicle_item_mappingDAO.getDTOByID(Long.parseLong(request.getParameter("ID")));
			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");

			String encoded = this.gson.toJson(vm_maintenance_vehicle_item_mappingDTO);
			System.out.println("json encoded data = " + encoded);
			out.print(encoded);
			out.flush();
		}
		catch (NumberFormatException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void addVm_maintenance_vehicle_item_mapping(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, boolean isPermanentTable) throws IOException
	{
		// TODO Auto-generated method stub
		try
		{
			request.setAttribute("failureMessage", "");
			System.out.println("%%%% addVm_maintenance_vehicle_item_mapping");
			String path = getServletContext().getRealPath("/img2/");
			Vm_maintenance_vehicle_item_mappingDTO vm_maintenance_vehicle_item_mappingDTO;
			SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");

			if(addFlag == true)
			{
				vm_maintenance_vehicle_item_mappingDTO = new Vm_maintenance_vehicle_item_mappingDTO();
			}
			else
			{
				vm_maintenance_vehicle_item_mappingDTO = vm_maintenance_vehicle_item_mappingDAO.getDTOByID(Long.parseLong(request.getParameter("iD")));
			}

			String Value = "";

			Value = request.getParameter("vmId");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("vmId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				vm_maintenance_vehicle_item_mappingDTO.vmId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("partsId");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("partsId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				vm_maintenance_vehicle_item_mappingDTO.partsId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("date");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("date = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				vm_maintenance_vehicle_item_mappingDTO.date = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			if(addFlag)
			{
				vm_maintenance_vehicle_item_mappingDTO.insertedByUserId = userDTO.ID;
			}


			if(addFlag)
			{
				vm_maintenance_vehicle_item_mappingDTO.insertedByOrganogramId = userDTO.organogramID;
			}


			if(addFlag)
			{
				Calendar c = Calendar.getInstance();
				c.set(Calendar.HOUR_OF_DAY, 0);
				c.set(Calendar.MINUTE, 0);
				c.set(Calendar.SECOND, 0);
				c.set(Calendar.MILLISECOND, 0);

				vm_maintenance_vehicle_item_mappingDTO.insertionDate = c.getTimeInMillis();
			}


			System.out.println("Done adding  addVm_maintenance_vehicle_item_mapping dto = " + vm_maintenance_vehicle_item_mappingDTO);
			long returnedID = -1;

			if(isPermanentTable == false) //add new row for validation and make the old row outdated
			{
				vm_maintenance_vehicle_item_mappingDAO.setIsDeleted(vm_maintenance_vehicle_item_mappingDTO.iD, CommonDTO.OUTDATED);
				returnedID = vm_maintenance_vehicle_item_mappingDAO.add(vm_maintenance_vehicle_item_mappingDTO);
				vm_maintenance_vehicle_item_mappingDAO.setIsDeleted(returnedID, CommonDTO.WAITING_FOR_APPROVAL);
			}
			else if(addFlag == true)
			{
				returnedID = vm_maintenance_vehicle_item_mappingDAO.manageWriteOperations(vm_maintenance_vehicle_item_mappingDTO, SessionConstants.INSERT, -1, userDTO);
			}
			else
			{
				returnedID = vm_maintenance_vehicle_item_mappingDAO.manageWriteOperations(vm_maintenance_vehicle_item_mappingDTO, SessionConstants.UPDATE, -1, userDTO);
			}









			if(isPermanentTable)
			{
				String inPlaceSubmit = request.getParameter("inplacesubmit");

				if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
				{
					getVm_maintenance_vehicle_item_mapping(request, response, returnedID);
				}
				else
				{
					response.sendRedirect("Vm_maintenance_vehicle_item_mappingServlet?actionType=search");
				}
			}
			else
			{
				commonRequestHandler.validate(vm_maintenance_vehicle_item_mappingDAO.getDTOByID(returnedID), request, response, userDTO);
			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}









	private void getVm_maintenance_vehicle_item_mapping(HttpServletRequest request, HttpServletResponse response, long id) throws ServletException, IOException
	{
		System.out.println("in getVm_maintenance_vehicle_item_mapping");
		Vm_maintenance_vehicle_item_mappingDTO vm_maintenance_vehicle_item_mappingDTO = null;
		try
		{
			vm_maintenance_vehicle_item_mappingDTO = vm_maintenance_vehicle_item_mappingDAO.getDTOByID(id);
			request.setAttribute("ID", vm_maintenance_vehicle_item_mappingDTO.iD);
			request.setAttribute("vm_maintenance_vehicle_item_mappingDTO",vm_maintenance_vehicle_item_mappingDTO);
			request.setAttribute("vm_maintenance_vehicle_item_mappingDAO",vm_maintenance_vehicle_item_mappingDAO);

			String URL= "";

			String inPlaceEdit = request.getParameter("inplaceedit");
			String inPlaceSubmit = request.getParameter("inplacesubmit");
			String getBodyOnly = request.getParameter("getBodyOnly");

			if(inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase(""))
			{
				URL = "vm_maintenance_vehicle_item_mapping/vm_maintenance_vehicle_item_mappingInPlaceEdit.jsp";
				request.setAttribute("inplaceedit","");
			}
			else if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
			{
				URL = "vm_maintenance_vehicle_item_mapping/vm_maintenance_vehicle_item_mappingSearchRow.jsp";
				request.setAttribute("inplacesubmit","");
			}
			else
			{
				if(getBodyOnly  != null && !getBodyOnly.equalsIgnoreCase(""))
				{
					URL = "vm_maintenance_vehicle_item_mapping/vm_maintenance_vehicle_item_mappingEditBody.jsp?actionType=edit";
				}
				else
				{
					URL = "vm_maintenance_vehicle_item_mapping/vm_maintenance_vehicle_item_mappingEdit.jsp?actionType=edit";
				}
			}

			RequestDispatcher rd = request.getRequestDispatcher(URL);
			rd.forward(request, response);
		}
		catch (NumberFormatException e)
		{
			e.printStackTrace();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}


	private void getVm_maintenance_vehicle_item_mapping(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		getVm_maintenance_vehicle_item_mapping(request, response, Long.parseLong(request.getParameter("ID")));
	}

	private void searchVm_maintenance_vehicle_item_mapping(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException
	{
		System.out.println("in  searchVm_maintenance_vehicle_item_mapping 1");
		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String ajax = request.getParameter("ajax");
		boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);

        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
			SessionConstants.NAV_VM_MAINTENANCE_VEHICLE_ITEM_MAPPING,
			request,
			vm_maintenance_vehicle_item_mappingDAO,
			SessionConstants.VIEW_VM_MAINTENANCE_VEHICLE_ITEM_MAPPING,
			SessionConstants.SEARCH_VM_MAINTENANCE_VEHICLE_ITEM_MAPPING,
			tableName,
			isPermanent,
			userDTO,
			filter,
			true);
        try
        {
			System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        }
        catch(Exception e)
        {
			System.out.println("failed to dojob" + e);
        }

		request.setAttribute("vm_maintenance_vehicle_item_mappingDAO",vm_maintenance_vehicle_item_mappingDAO);
        RequestDispatcher rd;
        if(!isPermanent)
        {
        	if(hasAjax == false)
	        {
	        	System.out.println("Going to vm_maintenance_vehicle_item_mapping/vm_maintenance_vehicle_item_mappingApproval.jsp");
	        	rd = request.getRequestDispatcher("vm_maintenance_vehicle_item_mapping/vm_maintenance_vehicle_item_mappingApproval.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to vm_maintenance_vehicle_item_mapping/vm_maintenance_vehicle_item_mappingApprovalForm.jsp");
	        	rd = request.getRequestDispatcher("vm_maintenance_vehicle_item_mapping/vm_maintenance_vehicle_item_mappingApprovalForm.jsp");
	        }
        }
        else
        {
	        if(hasAjax == false)
	        {
	        	System.out.println("Going to vm_maintenance_vehicle_item_mapping/vm_maintenance_vehicle_item_mappingSearch.jsp");
	        	rd = request.getRequestDispatcher("vm_maintenance_vehicle_item_mapping/vm_maintenance_vehicle_item_mappingSearch.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to vm_maintenance_vehicle_item_mapping/vm_maintenance_vehicle_item_mappingSearchForm.jsp");
	        	rd = request.getRequestDispatcher("vm_maintenance_vehicle_item_mapping/vm_maintenance_vehicle_item_mappingSearchForm.jsp");
	        }
        }
		rd.forward(request, response);
	}

}

