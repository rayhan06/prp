package vm_employee_assigned_vehicle;

import java.io.IOException;
import java.io.*;
import java.text.SimpleDateFormat;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import employee_assign.EmployeeSearchIds;
import employee_assign.EmployeeSearchModalUtil;
import employee_assign.EmployeeSearchModel;
import office_units.Office_unitsRepository;
import offices.OfficesDTO;
import offices.OfficesRepository;
import org.apache.log4j.Logger;

import login.LoginDTO;
import permission.MenuConstants;
import role.PermissionRepository;

import sessionmanager.SessionConstants;
import user.UserDTO;
import util.*;

import javax.servlet.http.*;
import java.util.*;


import geolocation.GeoLocationDAO2;
import language.LC;
import language.LM;
import common.ApiResponse;
import common.BaseServlet;
import com.google.gson.Gson;
import pb.*;
import pbReport.*;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;

import files.*;


/**
 * Servlet implementation class Vm_employee_assigned_vehicleServlet
 */
@WebServlet("/Vm_employee_assigned_vehicleServlet")
@MultipartConfig
public class Vm_employee_assigned_vehicleServlet extends BaseServlet {
    private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Vm_employee_assigned_vehicleServlet.class);

    @Override
    public String getTableName() {
        return Vm_employee_assigned_vehicleDAO.getInstance().getTableName();
    }

    @Override
    public String getServletName() {
        return "Vm_employee_assigned_vehicleServlet";
    }

    @Override
    public Vm_employee_assigned_vehicleDAO getCommonDAOService() {
        return Vm_employee_assigned_vehicleDAO.getInstance();
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[]{MenuConstants.VM_EMPLOYEE_ASSIGNED_VEHICLE_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[]{MenuConstants.VM_EMPLOYEE_ASSIGNED_VEHICLE_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[]{MenuConstants.VM_EMPLOYEE_ASSIGNED_VEHICLE_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Vm_employee_assigned_vehicleServlet.class;
    }

    FilesDAO filesDAO = new FilesDAO();
    private final Gson gson = new Gson();


    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {

        request.setAttribute("failureMessage", "");
        System.out.println("%%%% addVm_employee_assigned_vehicle");
        String language = userDTO.languageID == SessionConstants.ENGLISH ? "English" : "Bangla";
        Vm_employee_assigned_vehicleDTO vm_employee_assigned_vehicleDTO;
        SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
        String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
        boolean isLanEng = Language.equalsIgnoreCase("English");

        if (addFlag == true) {
            vm_employee_assigned_vehicleDTO = new Vm_employee_assigned_vehicleDTO();
            vm_employee_assigned_vehicleDTO.insertionDate = vm_employee_assigned_vehicleDTO.lastModificationTime = System.currentTimeMillis();
            vm_employee_assigned_vehicleDTO.insertedBy = vm_employee_assigned_vehicleDTO.modifiedBy = String.valueOf(userDTO.ID);
        } else {
            vm_employee_assigned_vehicleDTO = Vm_employee_assigned_vehicleDAO.getInstance().getDTOFromID(Long.parseLong(request.getParameter("iD")));
            if (vm_employee_assigned_vehicleDTO == null) {
                throw new Exception(isLanEng ? "Employee vehicle information is not found" : "তথ্য খুঁজে পাওয়া যায়নি");
            }
            vm_employee_assigned_vehicleDTO.lastModificationTime = System.currentTimeMillis();
            vm_employee_assigned_vehicleDTO.modifiedBy = String.valueOf(userDTO.ID);
        }

        String Value = "";


        Value = request.getParameter("employeeRecordId");

        if (Value != null && !Value.equalsIgnoreCase("")) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        } else {
            throw new Exception(isLanEng ? "Please provide employee" : "অনুগ্রহপূর্বক কর্মকর্তা বাছাই করুন");
        }
        EmployeeSearchIds defaultOfficeIds = EmployeeSearchModalUtil.getDefaultOfficeIds(Long.parseLong(Value));
        EmployeeSearchModel model = EmployeeSearchModalUtil.getEmployeeSearchModel(defaultOfficeIds);
        vm_employee_assigned_vehicleDTO.employeeEmpId = model.employeeRecordId;
        vm_employee_assigned_vehicleDTO.employeeOfficeUnitId = model.officeUnitId;
        vm_employee_assigned_vehicleDTO.employeeOrgId = model.organogramId;

        vm_employee_assigned_vehicleDTO.employeeNameEn = model.employeeNameEn;
        vm_employee_assigned_vehicleDTO.employeeNameBn = model.employeeNameBn;
        vm_employee_assigned_vehicleDTO.employeeOfficeUnitNameEn = model.officeUnitNameEn;
        vm_employee_assigned_vehicleDTO.employeeOfficeUnitNameBn = model.officeUnitNameBn;
        vm_employee_assigned_vehicleDTO.employeeOfficeUnitOrgNameEn = model.organogramNameEn;
        vm_employee_assigned_vehicleDTO.employeeOfficeUnitOrgNameBn = model.organogramNameBn;

        vm_employee_assigned_vehicleDTO.employeePhoneNum = model.phoneNumber;

        OfficesDTO requesterOffice = OfficesRepository.getInstance().getOfficesDTOByID(vm_employee_assigned_vehicleDTO.employeeOfficeId);

        if (requesterOffice != null) {
            vm_employee_assigned_vehicleDTO.employeeOfficeId = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(model.officeUnitId).officeId;

            vm_employee_assigned_vehicleDTO.employeeOfficeNameEn = requesterOffice.officeNameEng;
            vm_employee_assigned_vehicleDTO.employeeOfficeNameBn = requesterOffice.officeNameBng;
        }

        Value = request.getParameter("assignmentDate");

        if (Value != null && !Value.equalsIgnoreCase("")) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }

        if (Value != null && !Value.equalsIgnoreCase("")) {
            try {
                Date d = f.parse(Value);
                vm_employee_assigned_vehicleDTO.assignmentDate = d.getTime();
            } catch (Exception e) {
                e.printStackTrace();
                throw new Exception(LM.getText(LC.VM_EMPLOYEE_ASSIGNED_VEHICLE_ADD_ASSIGNMENTDATE, language) + " " + ErrorMessage.getInvalidMessage(language));
            }
        } else {
            throw new Exception(LM.getText(LC.VM_EMPLOYEE_ASSIGNED_VEHICLE_ADD_ASSIGNMENTDATE, language) + " " + ErrorMessage.getInvalidMessage(language));
        }

        Value = request.getParameter("vehicleDescription");

        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }
        System.out.println("vehicleDescription = " + Value);
        if (Value != null) {
            vm_employee_assigned_vehicleDTO.vehicleDescription = (Value);
        } else {
            //throw new Exception(isLanEng?"Please provide vehicle description":"অনুগ্রহপূর্বক যানবাহনের  বিবরণ প্রদান করুন");
        }

        Value = request.getParameter("filesDropzone");

        if (Value != null && !Value.equalsIgnoreCase("")) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }
        if (Value != null && !Value.equalsIgnoreCase("")) {


            vm_employee_assigned_vehicleDTO.filesDropzone = Long.parseLong(Value);


            if (addFlag == false) {
                String filesDropzoneFilesToDelete = request.getParameter("filesDropzoneFilesToDelete");
                String[] deleteArray = filesDropzoneFilesToDelete.split(",");
                for (int i = 0; i < deleteArray.length; i++) {
                    System.out.println("going to delete " + deleteArray[i]);
                    if (i > 0) {
                        filesDAO.delete(Long.parseLong(deleteArray[i]));
                    }
                }
            }
        } else {

        }

        if (addFlag == true) {
            Vm_employee_assigned_vehicleDAO.getInstance().add(vm_employee_assigned_vehicleDTO);
        } else {
            Vm_employee_assigned_vehicleDAO.getInstance().update(vm_employee_assigned_vehicleDTO);
        }

        return vm_employee_assigned_vehicleDTO;


    }
}

