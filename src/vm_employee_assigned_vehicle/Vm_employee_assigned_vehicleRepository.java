package vm_employee_assigned_vehicle;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import com.google.gson.Gson;

import recruitment_seat_plan.RecruitmentSeatPlanChildDTO;
import repository.Repository;
import repository.RepositoryManager;


public class Vm_employee_assigned_vehicleRepository implements Repository {
	Vm_employee_assigned_vehicleDAO vm_employee_assigned_vehicleDAO = null;
	
	static Logger logger = Logger.getLogger(Vm_employee_assigned_vehicleRepository.class);
	Map<Long, Vm_employee_assigned_vehicleDTO>mapOfVm_employee_assigned_vehicleDTOToiD;
	Map<Long, Set<Vm_employee_assigned_vehicleDTO> >mapOfVm_employee_assigned_vehicleDTOToEmployeeRecordId;
	Gson gson;

  
	private Vm_employee_assigned_vehicleRepository(){
		vm_employee_assigned_vehicleDAO = Vm_employee_assigned_vehicleDAO.getInstance();
		mapOfVm_employee_assigned_vehicleDTOToiD = new ConcurrentHashMap<>();
		mapOfVm_employee_assigned_vehicleDTOToEmployeeRecordId = new ConcurrentHashMap<>();
		gson = new Gson();
		RepositoryManager.getInstance().addRepository(this);
	}

	private static class LazyLoader{
        final static Vm_employee_assigned_vehicleRepository INSTANCE = new Vm_employee_assigned_vehicleRepository();
    }

    public static Vm_employee_assigned_vehicleRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

	public void reload(boolean reloadAll){
		try {
			List<Vm_employee_assigned_vehicleDTO> vm_employee_assigned_vehicleDTOs = vm_employee_assigned_vehicleDAO.getAllDTOs(reloadAll);
			for(Vm_employee_assigned_vehicleDTO vm_employee_assigned_vehicleDTO : vm_employee_assigned_vehicleDTOs) {
				Vm_employee_assigned_vehicleDTO oldVm_employee_assigned_vehicleDTO = getVm_employee_assigned_vehicleDTOByiD(vm_employee_assigned_vehicleDTO.iD);
				if( oldVm_employee_assigned_vehicleDTO != null ) {
					mapOfVm_employee_assigned_vehicleDTOToiD.remove(oldVm_employee_assigned_vehicleDTO.iD);

					if(mapOfVm_employee_assigned_vehicleDTOToEmployeeRecordId.containsKey(oldVm_employee_assigned_vehicleDTO.employeeEmpId)) {
						mapOfVm_employee_assigned_vehicleDTOToEmployeeRecordId.get(oldVm_employee_assigned_vehicleDTO.employeeEmpId).remove(oldVm_employee_assigned_vehicleDTO);
					}
					if(mapOfVm_employee_assigned_vehicleDTOToEmployeeRecordId.get(oldVm_employee_assigned_vehicleDTO.employeeEmpId).isEmpty()) {
						mapOfVm_employee_assigned_vehicleDTOToEmployeeRecordId.remove(oldVm_employee_assigned_vehicleDTO.employeeEmpId);
					}
				
					
				}
				if(vm_employee_assigned_vehicleDTO.isDeleted == 0) 
				{
					
					mapOfVm_employee_assigned_vehicleDTOToiD.put(vm_employee_assigned_vehicleDTO.iD, vm_employee_assigned_vehicleDTO);

					if( ! mapOfVm_employee_assigned_vehicleDTOToEmployeeRecordId.containsKey(vm_employee_assigned_vehicleDTO.employeeEmpId)) {
						mapOfVm_employee_assigned_vehicleDTOToEmployeeRecordId.put(vm_employee_assigned_vehicleDTO.employeeEmpId, new HashSet<>());
					}
					mapOfVm_employee_assigned_vehicleDTOToEmployeeRecordId.get(vm_employee_assigned_vehicleDTO.employeeEmpId).add(vm_employee_assigned_vehicleDTO);
				
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}

	public List<Vm_employee_assigned_vehicleDTO> getVmEmployeeAssignedVehicleByEmployeeRecordsId(long employeeEmpId) {
		return new ArrayList<>( mapOfVm_employee_assigned_vehicleDTOToEmployeeRecordId.getOrDefault(employeeEmpId,new HashSet<>()));
	}

	public List<Vm_employee_assigned_vehicleDTO> copyVmEmployeeAssignedVehicleByEmployeeRecordsId(long recruitmentTestNameId)
	{
		List <Vm_employee_assigned_vehicleDTO> vmEmployeeAssignedVehicleByEmployeeRecordsId = getVmEmployeeAssignedVehicleByEmployeeRecordsId(recruitmentTestNameId);
		return vmEmployeeAssignedVehicleByEmployeeRecordsId
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}

	public Vm_employee_assigned_vehicleDTO clone(Vm_employee_assigned_vehicleDTO dto) {
		String raw = gson.toJson(dto);
		return gson.fromJson(raw, Vm_employee_assigned_vehicleDTO.class);
	}
	
	
	public List<Vm_employee_assigned_vehicleDTO> getVm_employee_assigned_vehicleList() {
		List <Vm_employee_assigned_vehicleDTO> vm_employee_assigned_vehicles = new ArrayList<Vm_employee_assigned_vehicleDTO>(this.mapOfVm_employee_assigned_vehicleDTOToiD.values());
		return vm_employee_assigned_vehicles;
	}
	
	public List<Vm_employee_assigned_vehicleDTO> copyVm_employee_assigned_vehicleList() {
		List <Vm_employee_assigned_vehicleDTO> vm_employee_assigned_vehicles = getVm_employee_assigned_vehicleList();
		return vm_employee_assigned_vehicles
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}
	
	
	
	public Vm_employee_assigned_vehicleDTO getVm_employee_assigned_vehicleDTOByiD( long iD){
		return mapOfVm_employee_assigned_vehicleDTOToiD.get(iD);
	}
	
	public Vm_employee_assigned_vehicleDTO copyVm_employee_assigned_vehicleDTOByiD( long iD){
		return clone(mapOfVm_employee_assigned_vehicleDTOToiD.get(iD));
	}

	
	@Override
	public String getTableName() {
		return vm_employee_assigned_vehicleDAO.getTableName();
	}
}


