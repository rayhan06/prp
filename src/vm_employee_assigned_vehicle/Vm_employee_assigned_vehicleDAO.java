package vm_employee_assigned_vehicle;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.sql.SQLException;
import common.CommonDAOService;
import org.apache.log4j.Logger;
import util.*;
import pb.*;

public class Vm_employee_assigned_vehicleDAO  implements CommonDAOService<Vm_employee_assigned_vehicleDTO>
{
	
	Logger logger = Logger.getLogger(getClass());
	
	private static String addQuery = "";
	private static String updateQuery = "";
	private String[] columnNames = null;

	private Vm_employee_assigned_vehicleDAO()
	{
		columnNames = new String[] 
		{
			"ID",
			"employee_org_id",
			"employee_office_id",
			"employee_office_unit_id",
			"employee_emp_id",
			"employee_phone_num",
			"employee_name_en",
			"employee_name_bn",
			"employee_office_name_en",
			"employee_office_name_bn",
			"employee_office_unit_name_en",
			"employee_office_unit_name_bn",
			"employee_office_unit_org_name_en",
			"employee_office_unit_org_name_bn",
			"assignment_date",
			"vehicle_description",
			"files_dropzone",
			"sarok_number",
			"search_column",
			"inserted_by",
			"modified_by",
			"insertion_date",
			"isDeleted",
			"lastModificationTime"
		};
		updateQuery = getUpdateQuery(columnNames);
        addQuery = getAddQuery(columnNames);
		
		searchMap.put("assignment_date_start"," and (assignment_date >= ?)");
		searchMap.put("assignment_date_end"," and (assignment_date <= ?)");
		searchMap.put("insertion_date_start"," and (insertion_date >= ?)");
		searchMap.put("insertion_date_end"," and (insertion_date <= ?)");
		searchMap.put("AnyField"," and (search_column like ?)");
	}

	private final Map<String,String> searchMap = new HashMap<>();

    private static class LazyLoader
    {
    	static final Vm_employee_assigned_vehicleDAO INSTANCE = new Vm_employee_assigned_vehicleDAO();
	}

	public static Vm_employee_assigned_vehicleDAO getInstance()
	{
    	return LazyLoader.INSTANCE;
	}
	
	public void setSearchColumn(Vm_employee_assigned_vehicleDTO vm_employee_assigned_vehicleDTO)
	{
		vm_employee_assigned_vehicleDTO.searchColumn = "";
	}
	
	@Override
	public void set(PreparedStatement ps, Vm_employee_assigned_vehicleDTO vm_employee_assigned_vehicleDTO, boolean isInsert) throws SQLException
	{
		int index = 0;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(vm_employee_assigned_vehicleDTO);
		if(isInsert)
		{
			ps.setObject(++index,vm_employee_assigned_vehicleDTO.iD);
		}
		ps.setObject(++index,vm_employee_assigned_vehicleDTO.employeeOrgId);
		ps.setObject(++index,vm_employee_assigned_vehicleDTO.employeeOfficeId);
		ps.setObject(++index,vm_employee_assigned_vehicleDTO.employeeOfficeUnitId);
		ps.setObject(++index,vm_employee_assigned_vehicleDTO.employeeEmpId);
		ps.setObject(++index,vm_employee_assigned_vehicleDTO.employeePhoneNum);
		ps.setObject(++index,vm_employee_assigned_vehicleDTO.employeeNameEn);
		ps.setObject(++index,vm_employee_assigned_vehicleDTO.employeeNameBn);
		ps.setObject(++index,vm_employee_assigned_vehicleDTO.employeeOfficeNameEn);
		ps.setObject(++index,vm_employee_assigned_vehicleDTO.employeeOfficeNameBn);
		ps.setObject(++index,vm_employee_assigned_vehicleDTO.employeeOfficeUnitNameEn);
		ps.setObject(++index,vm_employee_assigned_vehicleDTO.employeeOfficeUnitNameBn);
		ps.setObject(++index,vm_employee_assigned_vehicleDTO.employeeOfficeUnitOrgNameEn);
		ps.setObject(++index,vm_employee_assigned_vehicleDTO.employeeOfficeUnitOrgNameBn);
		ps.setObject(++index,vm_employee_assigned_vehicleDTO.assignmentDate);
		ps.setObject(++index,vm_employee_assigned_vehicleDTO.vehicleDescription);
		ps.setObject(++index,vm_employee_assigned_vehicleDTO.filesDropzone);
		ps.setObject(++index,vm_employee_assigned_vehicleDTO.sarokNumber);
		ps.setObject(++index,vm_employee_assigned_vehicleDTO.searchColumn);
		ps.setObject(++index,vm_employee_assigned_vehicleDTO.insertedBy);
		ps.setObject(++index,vm_employee_assigned_vehicleDTO.modifiedBy);
		ps.setObject(++index,vm_employee_assigned_vehicleDTO.insertionDate);
		if(isInsert)
		{
			ps.setObject(++index,vm_employee_assigned_vehicleDTO.isDeleted);
		}
		ps.setObject(++index, lastModificationTime);
		if(!isInsert)
		{
			ps.setObject(++index,vm_employee_assigned_vehicleDTO.iD);
		}
	}
	
	@Override
	public Vm_employee_assigned_vehicleDTO buildObjectFromResultSet(ResultSet rs)
	{
		try
		{
			Vm_employee_assigned_vehicleDTO vm_employee_assigned_vehicleDTO = new Vm_employee_assigned_vehicleDTO();
			int i = 0;
			vm_employee_assigned_vehicleDTO.iD = rs.getLong(columnNames[i++]);
			vm_employee_assigned_vehicleDTO.employeeOrgId = rs.getLong(columnNames[i++]);
			vm_employee_assigned_vehicleDTO.employeeOfficeId = rs.getLong(columnNames[i++]);
			vm_employee_assigned_vehicleDTO.employeeOfficeUnitId = rs.getLong(columnNames[i++]);
			vm_employee_assigned_vehicleDTO.employeeEmpId = rs.getLong(columnNames[i++]);
			vm_employee_assigned_vehicleDTO.employeePhoneNum = rs.getString(columnNames[i++]);
			vm_employee_assigned_vehicleDTO.employeeNameEn = rs.getString(columnNames[i++]);
			vm_employee_assigned_vehicleDTO.employeeNameBn = rs.getString(columnNames[i++]);
			vm_employee_assigned_vehicleDTO.employeeOfficeNameEn = rs.getString(columnNames[i++]);
			vm_employee_assigned_vehicleDTO.employeeOfficeNameBn = rs.getString(columnNames[i++]);
			vm_employee_assigned_vehicleDTO.employeeOfficeUnitNameEn = rs.getString(columnNames[i++]);
			vm_employee_assigned_vehicleDTO.employeeOfficeUnitNameBn = rs.getString(columnNames[i++]);
			vm_employee_assigned_vehicleDTO.employeeOfficeUnitOrgNameEn = rs.getString(columnNames[i++]);
			vm_employee_assigned_vehicleDTO.employeeOfficeUnitOrgNameBn = rs.getString(columnNames[i++]);
			vm_employee_assigned_vehicleDTO.assignmentDate = rs.getLong(columnNames[i++]);
			vm_employee_assigned_vehicleDTO.vehicleDescription = rs.getString(columnNames[i++]);
			vm_employee_assigned_vehicleDTO.filesDropzone = rs.getLong(columnNames[i++]);
			vm_employee_assigned_vehicleDTO.sarokNumber = rs.getString(columnNames[i++]);
			vm_employee_assigned_vehicleDTO.searchColumn = rs.getString(columnNames[i++]);
			vm_employee_assigned_vehicleDTO.insertedBy = rs.getString(columnNames[i++]);
			vm_employee_assigned_vehicleDTO.modifiedBy = rs.getString(columnNames[i++]);
			vm_employee_assigned_vehicleDTO.insertionDate = rs.getLong(columnNames[i++]);
			vm_employee_assigned_vehicleDTO.isDeleted = rs.getInt(columnNames[i++]);
			vm_employee_assigned_vehicleDTO.lastModificationTime = rs.getLong(columnNames[i++]);
			return vm_employee_assigned_vehicleDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
		
	public Vm_employee_assigned_vehicleDTO getDTOByID (long id)
	{
		return getDTOFromID(id);
	}

	@Override
	public String getTableName() {
		return "vm_employee_assigned_vehicle";
	}

	@Override
	public Map<String, String> getSearchMap() {
		return searchMap;
	}

	public long add(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Vm_employee_assigned_vehicleDTO) commonDTO,addQuery,true);
	}

	public long update(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Vm_employee_assigned_vehicleDTO) commonDTO,updateQuery,false);
	}
	
	@Override
	public String getLastModifierUser(){
		return "modified_by";
    }
				
}
	