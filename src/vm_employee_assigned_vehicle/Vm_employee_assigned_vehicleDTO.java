package vm_employee_assigned_vehicle;
import java.util.*; 
import util.*; 


public class Vm_employee_assigned_vehicleDTO extends CommonDTO
{

	public long employeeOrgId = -1;
	public long employeeOfficeId = -1;
	public long employeeOfficeUnitId = -1;
	public long employeeEmpId = -1;
    public String employeePhoneNum = "";
    public String employeeNameEn = "";
    public String employeeNameBn = "";
    public String employeeOfficeNameEn = "";
    public String employeeOfficeNameBn = "";
    public String employeeOfficeUnitNameEn = "";
    public String employeeOfficeUnitNameBn = "";
    public String employeeOfficeUnitOrgNameEn = "";
    public String employeeOfficeUnitOrgNameBn = "";
	public long assignmentDate = System.currentTimeMillis();
    public String vehicleDescription = "";
	public long filesDropzone = -1;
    public String sarokNumber = "";
    public String insertedBy = "";
    public String modifiedBy = "";
	public long insertionDate = -1;
	
	
    @Override
	public String toString() {
            return "$Vm_employee_assigned_vehicleDTO[" +
            " iD = " + iD +
            " employeeOrgId = " + employeeOrgId +
            " employeeOfficeId = " + employeeOfficeId +
            " employeeOfficeUnitId = " + employeeOfficeUnitId +
            " employeeEmpId = " + employeeEmpId +
            " employeePhoneNum = " + employeePhoneNum +
            " employeeNameEn = " + employeeNameEn +
            " employeeNameBn = " + employeeNameBn +
            " employeeOfficeNameEn = " + employeeOfficeNameEn +
            " employeeOfficeNameBn = " + employeeOfficeNameBn +
            " employeeOfficeUnitNameEn = " + employeeOfficeUnitNameEn +
            " employeeOfficeUnitNameBn = " + employeeOfficeUnitNameBn +
            " employeeOfficeUnitOrgNameEn = " + employeeOfficeUnitOrgNameEn +
            " employeeOfficeUnitOrgNameBn = " + employeeOfficeUnitOrgNameBn +
            " assignmentDate = " + assignmentDate +
            " vehicleDescription = " + vehicleDescription +
            " filesDropzone = " + filesDropzone +
            " sarokNumber = " + sarokNumber +
            " searchColumn = " + searchColumn +
            " insertedBy = " + insertedBy +
            " modifiedBy = " + modifiedBy +
            " insertionDate = " + insertionDate +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}