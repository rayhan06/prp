package audit.log;

import common.CommonDTOService;
import util.CommonDTO;

public interface AuditLogCommonDTOService<T extends CommonDTO> extends CommonDTOService<T>{
}
