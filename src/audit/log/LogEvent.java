package audit.log;

public interface LogEvent {
    Integer ADD = 1;
    Integer UPDATE = 2;
    Integer DELETE = 3;
}
