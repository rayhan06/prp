package audit.log;

import util.CommonDTO;

public class AuditLogDTO extends CommonDTO {

    private final Integer logEvent;
    private final Integer moduleId;
    private final String tableName;
    private final Long tableId;
    private final String affectedColumn;
    private final String prevValue;
    private final String newValue;
    private final Long userId;
    private final Long userOrganogramId;
    private final Long insertionTime;
    private final String ipAddress;
    private final String description;


    public Integer getLogEvent() {
        return logEvent;
    }

    public Integer getModuleId() {
        return moduleId;
    }

    public String getTableName() {
        return tableName;
    }

    public Long getTableId() {
        return tableId;
    }

    public String getAffectedColumn() {
        return affectedColumn;
    }

    public String getPrevValue() {
        return prevValue;
    }

    public String getNewValue() {
        return newValue;
    }

    public Long getUserId() {
        return userId;
    }

    public Long getUserOrganogramId() {
        return userOrganogramId;
    }

    public Long getInsertionTime() {
        return insertionTime;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public String getDescription() {
        return description;
    }

    private AuditLogDTO(AuditLogBuilder builder){
        logEvent = builder.logEvent;
        moduleId = builder.moduleId;
        tableName = builder.tableName;
        tableId = builder.tableId;
        affectedColumn = builder.affectedColumn;
        prevValue = builder.prevValue;
        newValue = builder.newValue;
        userId = builder.userId;
        userOrganogramId = builder.userOrganogramId;
        insertionTime = builder.insertionTime;
        ipAddress = builder.ipAddress;
        description = builder.description;
    }
    
    public static class AuditLogBuilder{
        private Integer logEvent;
        private Integer moduleId;
        private String tableName;
        private Long tableId;
        private String affectedColumn;
        private String prevValue;
        private String newValue;
        private Long userId;
        private Long userOrganogramId;
        private Long insertionTime;
        private String ipAddress;
        private String description;

        public AuditLogBuilder setLogEvent(Integer logEvent) {
            this.logEvent = logEvent;
            return this;
        }

        public AuditLogBuilder setModuleId(Integer moduleId) {
            this.moduleId = moduleId;
            return this;
        }

        public AuditLogBuilder setTableName(String tableName) {
            this.tableName = tableName;
            return this;
        }

        public AuditLogBuilder setTableId(Long tableId) {
            this.tableId = tableId;
            return this;
        }

        public AuditLogBuilder setAffectedColumn(String affectedColumn) {
            this.affectedColumn = affectedColumn;
            return this;
        }

        public AuditLogBuilder setPrevValue(String prevValue) {
            this.prevValue = prevValue;
            return this;
        }

        public AuditLogBuilder setNewValue(String newValue) {
            this.newValue = newValue;
            return this;
        }

        public AuditLogBuilder setUserId(Long userId) {
            this.userId = userId;
            return this;
        }

        public AuditLogBuilder setUserOrganogramId(Long userOrganogramId) {
            this.userOrganogramId = userOrganogramId;
            return this;
        }

        public AuditLogBuilder setInsertionTime(Long insertionTime) {
            this.insertionTime = insertionTime;
            return this;
        }

        public AuditLogBuilder setIpAddress(String ipAddress) {
            this.ipAddress = ipAddress;
            return this;
        }

        public AuditLogBuilder setDescription(String description) {
            this.description = description;
            return this;
        }

        public AuditLogDTO build(){
            return new AuditLogDTO(this);
        }
    }
}