package audit.log;

import com.google.gson.Gson;
import common.ConnectionAndStatementUtil;
import org.apache.log4j.Logger;
import pb.Utils;
import util.CommonDTO;
import util.HttpRequestUtils;

import java.sql.SQLException;

public class AuditLogDAO {
    private static final Logger logger = Logger.getLogger(AuditLogDAO.class);
    private static final Gson gsonObj = new Gson();

    private static final String addQuery = "INSERT INTO rs_audit_log ( log_event, table_name, table_id, affected_column, prev_value, new_value, " +
            "user_id, user_organogram_id, insertion_time, ip_address, description) " +
            "VALUES(?,?,?,?,?,?,?,?,?,?,?)";

    public static void add (AuditLogDTO logDTO){
        ConnectionAndStatementUtil.getLogWritePrepareStatement(ps->{
            int index = 0;
            try {
                ps.setObject(++index, logDTO.getLogEvent());
                ps.setObject(++index, logDTO.getTableName());
                ps.setObject(++index, logDTO.getTableId());
                ps.setObject(++index, logDTO.getAffectedColumn());
                ps.setObject(++index, logDTO.getPrevValue());
                ps.setObject(++index, logDTO.getNewValue());
                ps.setObject(++index, logDTO.getUserId());
                ps.setObject(++index, logDTO.getUserOrganogramId());
                ps.setObject(++index, logDTO.getInsertionTime());
                ps.setObject(++index, logDTO.getIpAddress());
                ps.setObject(++index, logDTO.getDescription());
                ps.execute();
            }catch (SQLException ex){
                logger.error("",ex);
            }
        },addQuery);
    }

    public static <T extends CommonDTO>void add(T oldT,T newT,Integer logEvent,String tableName,String ipAddress,long userId,long oraganogramId){
        Utils.runIOTaskInSingleThread(()->()->
        add(new AuditLogDTO.AuditLogBuilder()
                .setLogEvent(logEvent)
                .setInsertionTime(System.currentTimeMillis())
                .setIpAddress(ipAddress)
                .setNewValue(gsonObj.toJson(newT))
                .setPrevValue(oldT == null ? null : gsonObj.toJson(oldT))
                .setTableName(tableName)
                .setTableId(newT.iD)
                .setUserId(userId)
                .setUserOrganogramId(oraganogramId)
                .build()));
    }
}