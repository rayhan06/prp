/*
 * Created on Oct 27, 2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package sessionmanager;

import language.LC;

import java.util.HashMap;
import java.util.Map;

@SuppressWarnings({"unused"})
public class SessionConstants {
    public static final String OT_BILL_TYPE_CAT_DOMAIN_NAME = "overtime_bill_type";
    public static final String FOOD_BILL_TYPE_CAT_DOMAIN_NAME = "food_bill_type";
    public static final long PARLIAMENT_OFFICE = 2294;
    public static final String COMMITTEE_SECRETARY = "কমিটি সচিব";

    public static final int REPORT_ROWS_PER_PAGE = 1000;
    public static final long CHAIRMAN_COMMITTEE = 8;

    public static final int OTHER_TICKET_ISSUES = 300;
    public static final String EDMS_DOC_SELECTED_FOR_SHARE = "edms_doc_selected_for_share";
    public static String OISF_ACCESS_TOKEN = "";
    
    public static final long OTHER_OFFICE_PARLIAMENT = 801;

    public final static int INBOX_TAB_INBOX = 1;

    public final static int INBOX_TAB_SEND = 2;

    public final static int INBOX_MESSAGE_TYPE = 1;

    public final static int OCCURRENCE_HOUR = 1;
    public final static int OCCURRENCE_DAY = 2;

    public static final long SECRETARY_UNIT = 10;
    public static final long MEDICAL_UNIT = 55;

    public static final long OTHER_OFFICE_PWD = 1;
    public static final long OTHER_OFFICE_ANSAR = 302;
    public static final long OTHER_OFFICE_POLICE = 303;

    public static final int REPORT_OPERATOR_POS = 3;
    public static final int REPORT_VALUE_POS = 8;


    public final static int DEFAULT_INBOX_ORGANOGRAM_ID = 71577; // dg, bmd

    public final static int DEFAULT_INBOX_STATUS = 22; // new application

    public final static long BROADCASTING_AND_IT_WING = 56;

    public final static int CITIZEN_ROLE = 9301;

    public static final long LAB_HEAD_ORGANOGRAM_ID = 263618; // may change later
    public static final long MEDICAL_DIRECTOR_ORGANOGRAM_ID = 262115; // may change later
    public static final String LAB_HEAD_USER_NAME = "1110015";


    public final static int DOCTOR_ROLE = 10001;
    public final static int MEDICAL_RECEPTIONIST_ROLE = 11703;
    public final static int PHYSIOTHERAPIST_ROLE = 10301;
    public final static int NURSE_ROLE = 10002;
    public final static int INVENTORY_MANGER_ROLE = 10005;
    public static final int MEDICAL_ADMIN_ROLE = 10502;
    public static final int LAB_TECHNITIAN_ROLE = 10004;
    public static final int XRAY_GUY = 11802;
    public static final int RADIOLOGIST = 11803;
    public static final int PATHOLOGY_HEAD = 11804;

    public static final int PHARMACY_PERSON = 10006;
    public static final int DENTAL_TECHNOLOGIST_ROLE = 12000;

    public static final int GATE_PASS_RECV_ROLE = 11202;
    public static final int GATE_SECURITY_ROLE = 11203;
    public static final int SERJEANT_AT_ARMS_ROLE = 11805;

    public static final int PBS_ROLE = 11204;
    public static final int VISITOR_APROVER = 12107;
    
    public static final int HR_1_ROLE = 12206;
    public static final int HR_2_ROLE = 12207;
    public static final int HR_3_ROLE = 12208;

    public static final long MAINTENANCE1_OFFICE = 65;
    public static final long MAINTENANCE2_OFFICE = 68;
    public static final long DATABASE_OFFICE = 67;

    public static final long MP_OFFICE = 9;

    public static final long ROOM_OFFSET = 1000000;
    public static final long OFFICE_OFFSET = 2000000;

    public static final int PLATE_1 = 1400;
    public static final int PLATE_2 = 1401;
    public static final int PLATE_3 = 1402;

    public static final int XRAY_CAT = 0;

    public static final long MS_IN_A_DAY = 86400000;


    public static final String OUTSIDER_PATIENT = "599900001";
    public static final long OUTSIDER_PATIENT_ERID = 3214105;


    public final static int ADMIN_ROLE = 1;

    public static final int TICKET_ADMIN_ROLE = 10501;
    public static final int SUPPORT_ENGINEER_ROLE = 11702;
    public static final int IT_EMPLOYEE_ROLE = 11801;


    public static final int ATTENDANCE_ADMIN_ROLE = 10901;
    public static final int RECRUITMENT_ADMIN_ROLE = 10801;
    public static final int GATE_MANAGEMENT_ADMIN_ROLE = 11101;
    public static final int PARLIAMENT_EMPLOYEE_ROLE = 10401;
    public static final int VEHICLE_ADMIN_ROLE = 11001;

    public static final int ASSET_ADMIN_ROLE = 11401;
    public static final int STORE_KEEPER = 11501;
    public static final int INVENTORY_ADMIN_ROLE = 11601;

    public final static int AMBULANCE_SERVICE = 0;
    public final static int HOME_SERVICE = 1;

    public final static int ASSET_TYPE_ASSET = 0;
    public final static int ASSET_TYPE_ACCESSORY = 100;
    public final static int ASSET_TYPE_CONSUMABLE = 101;
    public final static int ASSET_TYPE_COMPONENT = 102;
    public final static int ASSET_TYPE_LICENCE = 103;

    public final static int ASSET_CAT_TABLET = 1;
    public final static int ASSET_CAT_LAPTOP = 100;
    public final static int ASSET_CAT_DESKTOP = 0;
    public final static int ASSET_CAT_MONITOR = 201;
    public final static int ASSET_CAT_HDD = 208;
    public final static int ASSET_CAT_PRINTER = 1000;
    public final static int ASSET_CAT_SCANNER = 1001;
    public final static int ASSET_CAT_ROUTER = 1002;

    public final static int ASSET_CAT_GRAPHICS_SOFT = 210;
    public final static int ASSET_CAT_OFFICE_SOFT = 211;
    public final static int ASSET_CAT_ANTIVIRUS_SOFT = 300;
    public final static int ASSET_CAT_OS_SOFT = 301;

    public final static int ASSET_CAT_KEYBOARD = 204;
    public final static int ASSET_CAT_MOUSE = 205;

    public final static int ASSET_STATUS_READY_TO_DEPLOY = 1;
    public final static int ASSET_STATUS_ASSIGNED = 2;
    public final static int ASSET_STATUS_IN_MAINTENACE = 3;
    public final static int ASSET_STATUS_REVOKED = 4;
    public final static int ASSET_STATUS_LOST = 5;
    public final static int ASSET_STATUS_ARCHIVED = 6;


    public final static int LAB_TEST_XRAY = 400;
    public final static int LAB_TEST_ULTRASONOGRAM = 401;
    public final static int LAB_TEST_OTHER = 1400;
    public final static int LAB_TEST_URINE = 300;
    public final static int LAB_TEST_HEMATO = 100;
    public final static int LAB_TEST_SERO = 101;
    public final static int LAB_TEST_BIO= 201;
    public final static long LAB_TEST_LIST_BG= 1707;
    public final static long LAB_TEST_LIST_ABO= 1708;
    public final static long LAB_TEST_LIST_RH= 1709;

    public static final int MEDICAL_ITEM_DRUG = 0;
    public static final int MEDICAL_ITEM_REAGENT = 1;
    public static final int MEDICAL_ITEM_EQUIPMENT = 2;

    public final static int NOTIFICATION_DESTINATION_USER = 0;
    public final static int NOTIFICATION_DESTINATION_ROLE = 1;
    public final static int NOTIFICATION_DESTINATION_ORGANOGRAM = 3;
    public final static int NOTIFICATION_DESTINATION_ORGANOGRAM_AND_ROLE = 4;

    public final static int DRUG_TABLET = 0;
    public final static int DRUG_SYRUP = 1;
    public final static int DRUG_SYRINGE = 2;

    public final static int CITIZEN_USER_TYPE = 3;

    public final static int CITIZEN_LANGUAGE_ID = 1;

    public final static int INBOX_TYPE_SEND = 16; // send, fix it
    public final static int INBOX_TYPE_CC = 17;
    public final static int INBOX_TYPE_FORWARD = 18;

    public final static int APPROVE = 5;
    public final static int DELETE = 3;
    public final static int INSERT = 1;
    public final static int REJECT = 7;
    public final static int UPDATE = 2;
    public final static int VALIDATE = 6;
    public final static int INITIATE = 9;
    public final static int TERMINATE = 10;
    public final static int SKIP_STEP = 11;

    public final static int VIEW_ORIGINAL = 8;

    public final static int VALIDATOR = 1;

    public final static long OFFICE_ID = 11599;
    public final static long OFFICE_ORIGIN_ID = 9581;

    public final static int DEFAULT_JOB_CAT = -1;

    public final static long MIN_DATE = -62135791200000L;

    public final static long OTHER_ID_VALUE = 2147483647;

    public static final int BANGLA = 2;
    public static final int ENGLISH = 1;

    public static final int INITIAL_FETCH_NOTIFICATION_COUNT = 30;
    public static final int NEXT_FETCH_NOTIFICATION_COUNT = 30;

    public final static String[] operation_names = {"", "INSERT", "UPDATE", "DELETE", "SEARCH", "APPROVED", "VALIDATED",
            "REJECTED", "VIEW_ORIGINAL", "INITIATED", "TERMINATED", "SKIPPED STEP"};

    public final static String[] DOCTYPES = {"Drawing", "Letter", "QSDR", "NCN", "HSE", "NCR Closing", "Meeting Request", "EDMS Documents", "Meeting Minutes", "NOC"};

    public static Map<String, String> tableDocTypeMap = new HashMap<String, String>() {{
        put("ncn_draft", "NCN");
        put("qsdr_documents", "QSDR");
        put("edms_documents", "EDMS Documents");
        put("doc_draft", "Letter");
        put("hse_enformance_notice", "HSE");
        put("drawing_documents", "Drawing");
        put("ncr_closing", "NCR Closing");
        put("meeting_minutes_adding", "Meeting Minutes");
        put("meeting_request_details", "Meeting Request");
        put("noc_info", "NOC");
    }};


    // navigation bar related
    public final static String NAVIGATION_BAR_FIRST = "first";
    public final static String NAVIGATION_BAR_NEXT = "next";
    public final static String NAVIGATION_BAR_PREVIOUS = "previous";
    public final static String NAVIGATION_BAR_LAST = "last";
    public final static String NAVIGATION_BAR_CURRENT = "current";
    // form check related
    public final static String NAVIGATION_LINK = "id";
    public final static String GO_CHECK_FIELD = "go";
    public final static String SEARCH_CHECK_FIELD = "search";
    public final static String HTML_SEARCH_CHECK_FIELD = "htmlsearch";
    // search quiry related
    public final static String ADVANCED_SEARCH = "AdvancedSearch";
    // record navigation initial parameter
    public final static int CRRENT_PAGE_NO = 1;
    public final static int TOTAL_PAGES = 10;
    public final static int PAGE_SIZE = 50000;
    public final static int NUMBER_OF_RECORDS = 100;
    public final static int FIRST_PAGE = 1;
    public static final String RECORDS_PER_PAGE = "RECORDS_PER_PAGE";

    public static final String REPORT_METADATA = "REPORT_METADATA";

    public final static int USER_ROLE_TYPE_BASED_ON_USER = 1;
    public final static int USER_ROLE_TYPE_BASED_ON_ORGANOGRAM_SINGLE = 2;
    public final static int USER_ROLE_TYPE_BASED_ON_ORGANOGRAM_MULTI = 3;

    public final static int INBOX_TYPE_APPROVAL_PATH = 2000;

    public final static int CURRENT_USER_ROLE_TYPE = USER_ROLE_TYPE_BASED_ON_ORGANOGRAM_MULTI;


    public final static String RECRUITMENT_USER = "RECRUITMENT_USER";

    /*
     *
     *
     * Failure Message
     */
    // Error Message
    public final static String FAILURE_MESSAGE = "failuremessage";
    /*
     * User Login
     */
    public final static String USER_LOGIN = "user_login";
    public final static String USER_NOTIFICATION = "user_notification";
    public final static String USER_NOTIFICATION_COUNT = "user_notification_count";
    public final static String USER_PP = "user_pp";
    public final static String SIDE_BAR = "side_bar";

    public final static String PREFERRED_LANGUAGE = "preferrer_language";

    /*
     * Role
     */
    public final static String NAV_ROLE = "navrole";
    public final static String VIEW_ROLE = "viewrole";
    public final static String[][] SEARCH_ROLE = {{"" + LC.ROLE_SEARCH_ROLE_NAME, "roleName"},
            {"" + LC.ROLE_SEARCH_DESCRIPTION, "description"}};

    public final static String NAV_USER = "navuser";
    public final static String VIEW_USER = "viewuser";
    public final static String[][] SEARCH_USER = {{"" + LC.USER_SEARCH_USER_ID, "userName"},
            {"" + LC.USER_SEARCH_FULL_NAME, "fullName"}, {"" + LC.ROLE_SEARCH_ROLE_NAME, "roleToSearch"}};

    public final static String VIEW_LANGUAGE = "viewlanguage";
    public final static String NAV_LANGUAGE = "navlanguage";
    public final static String[][] SEARCH_LANGUAGE = {{".//language//menu.jsp", "menuID"},
            {"English Text", "languageTextEnglish"}, {"Bangla Text", "languageTextBangla"},
            {"Constant Prefix", "languageConstantPrefix"}, {"Constant", "languageConstant"}, {"ID", "ID"}

    };

    public final static String VIEW_LANGUAGE_GROUP = "viewlanguageGroup";
    public final static String NAV_LANGUAGE_GROUP = "navlanguageGroup";
    public final static String[][] SEARCH_LANGUAGE_GROUP = {{"Name", "name"}, {"Url", "url"}};

    public final static String NAV_THEME = "navTHEME";
    public final static String VIEW_THEME = "viewTHEME";
    public static final String[][] SEARCH_THEME = {{"" + LC.THEME_SEARCH_THEMENAME, "theme_name"},
            {"" + LC.THEME_SEARCH_DIRECTORY, "directory"}, {"" + LC.THEME_SEARCH_ISAPPLIED, "isApplied"},
            {"" + LC.THEME_SEARCH_ANYFIELD, "AnyField"}};

    public final static String NAV_APPROVAL_MODULE_MAP = "navAPPROVAL_MODULE_MAP";
    public final static String VIEW_APPROVAL_MODULE_MAP = "viewAPPROVAL_MODULE_MAP";
    public static final String[][] SEARCH_APPROVAL_MODULE_MAP = {
            {"" + LC.APPROVAL_MODULE_MAP_SEARCH_TABLENAME, "table_name"},
            {"" + LC.APPROVAL_MODULE_MAP_SEARCH_HASADDAPPROVAL, "has_add_approval"},
            {"" + LC.APPROVAL_MODULE_MAP_SEARCH_HASEDITAPPROVAL, "has_edit_approval"},
            {"" + LC.APPROVAL_MODULE_MAP_SEARCH_HASDELETEAPPROVAL, "has_delete_approval"},
            {"" + LC.APPROVAL_MODULE_MAP_SEARCH_ANYFIELD, "AnyField"}};

    public final static String NAV_APPROVAL_ROLE = "navAPPROVAL_ROLE";
    public final static String VIEW_APPROVAL_ROLE = "viewAPPROVAL_ROLE";
    public static final String[][] SEARCH_APPROVAL_ROLE = {{"" + LC.APPROVAL_ROLE_SEARCH_NAMEEN, "name_en"},
            {"" + LC.APPROVAL_ROLE_SEARCH_NAMEBN, "name_bn"},
            {"" + LC.APPROVAL_ROLE_SEARCH_DESCRIPTION, "description"},
            {"" + LC.APPROVAL_ROLE_SEARCH_ANYFIELD, "AnyField"}};

    public final static String NAV_GRS_TOP_LAYER = "navGRS_TOP_LAYER";
    public final static String VIEW_GRS_TOP_LAYER = "viewGRS_TOP_LAYER";
    public static final String[][] SEARCH_GRS_TOP_LAYER = {{"" + LC.GRS_TOP_LAYER_SEARCH_NAMEEN, "name_en"},
            {"" + LC.GRS_TOP_LAYER_SEARCH_NAMEBN, "name_bn"},
            {"" + LC.GRS_TOP_LAYER_SEARCH_LAYERNUMBER, "layer_number"},
            {"" + LC.GRS_TOP_LAYER_SEARCH_DESCRIPTION, "description"},
            {"" + LC.GRS_TOP_LAYER_SEARCH_ANYFIELD, "AnyField"}};

//	public final static String NAV_NO_APTEST = "navNO_APTEST";
//	public final static String VIEW_NO_APTEST = "viewNO_APTEST";
//	public static final String[][] SEARCH_NO_APTEST = {
//		{ ""+LC.NO_APTEST_SEARCH_NAMEEN, "name_en" },
//		{ ""+LC.NO_APTEST_SEARCH_NAMEBN, "name_bn" },
//		{ ""+LC.NO_APTEST_SEARCH_CENTRETYPE, "centre_type" },
//		{ ""+LC.NO_APTEST_SEARCH_DESCRIPTION, "description" },
//		{ ""+LC.NO_APTEST_SEARCH_ANYFIELD , "AnyField" }
//	};

    public final static String NAV_OFFICE_MINISTRIES = "navOFFICE_MINISTRIES";
    public final static String VIEW_OFFICE_MINISTRIES = "viewOFFICE_MINISTRIES";
    public static final String[][] SEARCH_OFFICE_MINISTRIES = {
            {"" + LC.OFFICE_MINISTRIES_SEARCH_OFFICETYPE, "office_type"},
            {"" + LC.OFFICE_MINISTRIES_SEARCH_NAMEENG, "name_eng"},
            {"" + LC.OFFICE_MINISTRIES_SEARCH_NAMEBNG, "name_bng"},
            {"" + LC.OFFICE_MINISTRIES_SEARCH_NAMEENGSHORT, "name_eng_short"},
            {"" + LC.OFFICE_MINISTRIES_SEARCH_REFERENCECODE, "reference_code"},
            {"" + LC.OFFICE_MINISTRIES_SEARCH_STATUS, "status"},
            {"" + LC.OFFICE_MINISTRIES_SEARCH_CREATEDBY, "created_by"},
            {"" + LC.OFFICE_MINISTRIES_SEARCH_MODIFIEDBY, "modified_by"},
            {"" + LC.OFFICE_MINISTRIES_SEARCH_CREATED, "created"},
            {"" + LC.OFFICE_MINISTRIES_SEARCH_MODIFIED, "modified"},
            {"" + LC.OFFICE_MINISTRIES_SEARCH_APPROVALPATHTYPE, "approval_path_type"},
            {"" + LC.OFFICE_MINISTRIES_SEARCH_ANYFIELD, "AnyField"}};

    public final static String NAV_OFFICE_UNIT_ORGANOGRAMS = "navOFFICE_UNIT_ORGANOGRAMS";
    public final static String VIEW_OFFICE_UNIT_ORGANOGRAMS = "viewOFFICE_UNIT_ORGANOGRAMS";
    public static final String[][] SEARCH_OFFICE_UNIT_ORGANOGRAMS = {
            {"" + LC.OFFICE_UNIT_ORGANOGRAMS_SEARCH_ROLETYPE, "role_type"},
            {"" + LC.OFFICE_UNIT_ORGANOGRAMS_SEARCH_ANYFIELD, "AnyField"}};

    public final static String NAV_OFFICES = "navOFFICES";
    public final static String VIEW_OFFICES = "viewOFFICES";
    public static final String[][] SEARCH_OFFICES = {{"" + LC.OFFICES_SEARCH_OFFICEMINISTRYID, "office_ministry_id"},
            {"" + LC.OFFICES_SEARCH_OFFICELAYERID, "office_layer_id"},
            {"" + LC.OFFICES_SEARCH_CUSTOMLAYERID, "custom_layer_id"},
            {"" + LC.OFFICES_SEARCH_OFFICEORIGINID, "office_origin_id"},
            {"" + LC.OFFICES_SEARCH_OFFICENAMEENG, "office_name_eng"},
            {"" + LC.OFFICES_SEARCH_OFFICENAMEBNG, "office_name_bng"},
            {"" + LC.OFFICES_SEARCH_GEODIVISIONID, "geo_division_id"},
            {"" + LC.OFFICES_SEARCH_GEODISTRICTID, "geo_district_id"},
            {"" + LC.OFFICES_SEARCH_GEOUPAZILAID, "geo_upazila_id"},
            {"" + LC.OFFICES_SEARCH_GEOUNIONID, "geo_union_id"},
            {"" + LC.OFFICES_SEARCH_GEOCITYCORPORATIONID, "geo_city_corporation_id"},
            {"" + LC.OFFICES_SEARCH_GEOCITYCORPORATIONWARDID, "geo_city_corporation_ward_id"},
            {"" + LC.OFFICES_SEARCH_GEOMUNICIPALITYID, "geo_municipality_id"},
            {"" + LC.OFFICES_SEARCH_GEOMUNICIPALITYWARDID, "geo_municipality_ward_id"},
            {"" + LC.OFFICES_SEARCH_OFFICECODE, "office_code"},
            {"" + LC.OFFICES_SEARCH_OFFICEADDRESS, "office_address"},
            {"" + LC.OFFICES_SEARCH_DIGITALNOTHICODE, "digital_nothi_code"},
            {"" + LC.OFFICES_SEARCH_REFERENCECODE, "reference_code"},
            {"" + LC.OFFICES_SEARCH_OFFICEPHONE, "office_phone"},
            {"" + LC.OFFICES_SEARCH_OFFICEMOBILE, "office_mobile"},
            {"" + LC.OFFICES_SEARCH_OFFICEFAX, "office_fax"}, {"" + LC.OFFICES_SEARCH_OFFICEEMAIL, "office_email"},
            {"" + LC.OFFICES_SEARCH_OFFICEWEB, "office_web"},
            {"" + LC.OFFICES_SEARCH_PARENTOFFICEID, "parent_office_id"}, {"" + LC.OFFICES_SEARCH_STATUS, "status"},
            {"" + LC.OFFICES_SEARCH_CREATEDBY, "created_by"}, {"" + LC.OFFICES_SEARCH_MODIFIEDBY, "modified_by"},
            {"" + LC.OFFICES_SEARCH_CREATED, "created"}, {"" + LC.OFFICES_SEARCH_MODIFIED, "modified"},
            {"" + LC.OFFICES_SEARCH_GEOTHANAID, "geo_thana_id"},
            {"" + LC.OFFICES_SEARCH_GEOPOSTOFFICEID, "geo_postoffice_id"},
            {"" + LC.OFFICES_SEARCH_GEOCOUNTRYID, "geo_country_id"},
            {"" + LC.OFFICES_SEARCH_APPROVALPATHTYPE, "approval_path_type"},
            {"" + LC.OFFICES_SEARCH_ANYFIELD, "AnyField"}};

//	public final static String NAV_ORGANOGRAM_ROLE_MAPPING = "navORGANOGRAM_ROLE_MAPPING";
//	public final static String VIEW_ORGANOGRAM_ROLE_MAPPING = "viewORGANOGRAM_ROLE_MAPPING";
//	public static final String[][] SEARCH_ORGANOGRAM_ROLE_MAPPING = {
//		{ ""+LC.ORGANOGRAM_ROLE_MAPPING_SEARCH_ORGANOGRAMID, "organogram_id" },
//		{ ""+LC.ORGANOGRAM_ROLE_MAPPING_SEARCH_ROLEID, "role_id" },
//		{ ""+LC.ORGANOGRAM_ROLE_MAPPING_SEARCH_ANYFIELD , "AnyField" }
//	};

    public final static String NAV_PB_NOTIFICATIONS = "navPB_NOTIFICATIONS";
    public final static String VIEW_PB_NOTIFICATIONS = "viewPB_NOTIFICATIONS";
    public static final String[][] SEARCH_PB_NOTIFICATIONS = {{"" + LC.PB_NOTIFICATIONS_SEARCH_ISSEEN, "is_seen"},
            {"" + LC.PB_NOTIFICATIONS_SEARCH_ISHIDDEN, "is_hidden"},
            {"" + LC.PB_NOTIFICATIONS_SEARCH_SOURCE, "source"},
            {"" + LC.PB_NOTIFICATIONS_SEARCH_DESTINATION, "destination"},
            {"" + LC.PB_NOTIFICATIONS_SEARCH_FROMID, "from_id"}, {"" + LC.PB_NOTIFICATIONS_SEARCH_TOID, "to_id"},
            {"" + LC.PB_NOTIFICATIONS_SEARCH_TEXT, "text"}, {"" + LC.PB_NOTIFICATIONS_SEARCH_URL, "url"},
            {"" + LC.PB_NOTIFICATIONS_SEARCH_ENTRYDATE, "entry_date"},
            {"" + LC.PB_NOTIFICATIONS_SEARCH_SEENDATE, "seen_date"},
            {"" + LC.PB_NOTIFICATIONS_SEARCH_SHOWINGDATE, "showing_date"},
            {"" + LC.PB_NOTIFICATIONS_SEARCH_SENDALARM, "send_alarm"},
            {"" + LC.PB_NOTIFICATIONS_SEARCH_SENDSMS, "send_sms"},
            {"" + LC.PB_NOTIFICATIONS_SEARCH_SENDMAIL, "send_mail"},
            {"" + LC.PB_NOTIFICATIONS_SEARCH_SENDPUSH, "send_push"},
            {"" + LC.PB_NOTIFICATIONS_SEARCH_MAILSENT, "mail_sent"},
            {"" + LC.PB_NOTIFICATIONS_SEARCH_SMSSENT, "sms_sent"},
            {"" + LC.PB_NOTIFICATIONS_SEARCH_PUSHSENT, "push_sent"},
            {"" + LC.PB_NOTIFICATIONS_SEARCH_ANYFIELD, "AnyField"}};

    public final static String NAV_UNADVANCED_SEARCH = "navUNADVANCED_SEARCH";
    public final static String VIEW_UNADVANCED_SEARCH = "viewUNADVANCED_SEARCH";
    public static final String[][] SEARCH_UNADVANCED_SEARCH = {{"" + LC.UNADVANCED_SEARCH_SEARCH_NAMEEN, "name_en"},
            {"" + LC.UNADVANCED_SEARCH_SEARCH_NAMEBN, "name_bn"},
            {"" + LC.UNADVANCED_SEARCH_SEARCH_DESCRIPTION, "description"},
            {"" + LC.UNADVANCED_SEARCH_SEARCH_ANYFIELD, "AnyField"}};

    public final static String NAV_CENTRE = "navCENTRE";
    public final static String VIEW_CENTRE = "viewCENTRE";
    public static final String[][] SEARCH_CENTRE = {{"" + LC.CENTRE_SEARCH_NAMEEN, "name_en"},
            {"" + LC.CENTRE_SEARCH_NAMEBN, "name_bn"}, {"" + LC.CENTRE_SEARCH_ADDRESS, "address"},
            {"" + LC.CENTRE_SEARCH_CONTACTPERSON, "contact_person"},
            {"" + LC.CENTRE_SEARCH_PHONENUMBER, "phone_number"}, {"" + LC.CENTRE_SEARCH_EMAIL, "email"},
            {"" + LC.CENTRE_SEARCH_ANYFIELD, "AnyField"}};

    public final static String NAV_PROJECT_TRACKER = "navPROJECT_TRACKER";
    public final static String VIEW_PROJECT_TRACKER = "viewPROJECT_TRACKER";
    public static final String[][] SEARCH_PROJECT_TRACKER = {
            {"" + LC.PROJECT_TRACKER_SEARCH_PROJECTTRACKEREVENTSTYPE, "project_tracker_events_type"},
            {"" + LC.PROJECT_TRACKER_SEARCH_PROJECTTRACKEREVENTSTEXT, "project_tracker_events_text"},
            {"" + LC.PROJECT_TRACKER_SEARCH_PROJECTTRACKERSUBJECTSTYPE, "project_tracker_subjects_type"},
            {"" + LC.PROJECT_TRACKER_SEARCH_PROJECTTRACKERSUBJECTSTEXT, "project_tracker_subjects_text"},
            {"" + LC.PROJECT_TRACKER_SEARCH_EVENTLOCATION, "event_location"},
            {"" + LC.PROJECT_TRACKER_SEARCH_SUMMARY, "summary"},
            {"" + LC.PROJECT_TRACKER_SEARCH_ANYFIELD, "AnyField"}};

//	public final static String NAV_FILES = "navFILES";
//	public final static String VIEW_FILES = "viewFILES";
//	public static final String[][] SEARCH_FILES = {
//		{ ""+LC.FILES_SEARCH_FILETYPE, "file_type" },
//		{ ""+LC.FILES_SEARCH_FILEDATA, "file_data" },
//		{ ""+LC.FILES_SEARCH_FILETAG, "file_tag" },
//		{ ""+LC.FILES_SEARCH_FILETITLE, "file_title" },
//		{ ""+LC.FILES_SEARCH_CREATEDAT, "created_at" },
//		{ ""+LC.FILES_SEARCH_ANYFIELD , "AnyField" }
//	};

//	public final static String NAV_FILES_MAPPING = "navFILES_MAPPING";
//	public final static String VIEW_FILES_MAPPING = "viewFILES_MAPPING";
//	public static final String[][] SEARCH_FILES_MAPPING = {
//		{ ""+LC.FILES_MAPPING_SEARCH_FILEID, "file_id" },
//		{ ""+LC.FILES_MAPPING_SEARCH_TABLENAME, "table_name" },
//		{ ""+LC.FILES_MAPPING_SEARCH_ANYFIELD , "AnyField" }
//	};

//	public final static String NAV_INBOX = "navINBOX";
//	public final static String VIEW_INBOX = "viewINBOX";
//	public static final String[][] SEARCH_INBOX = {
//		{ ""+LC.INBOX_SEARCH_SUBMISSIONDATE, "submission_date" },
//		{ ""+LC.INBOX_SEARCH_MESSAGETYPE, "message_type" },
//		{ ""+LC.INBOX_SEARCH_CURRENTSTATUS, "current_status" },
//		{ ""+LC.INBOX_SEARCH_SUBJECT, "subject" },
//		{ ""+LC.INBOX_SEARCH_DETAILS, "details" },
//		{ ""+LC.INBOX_SEARCH_TRACKINGNUMBER, "tracking_number" },
//		{ ""+LC.INBOX_SEARCH_INBOXORIGINATORID, "inbox_originator_id" },
//		{ ""+LC.INBOX_SEARCH_USERID, "user_id" },
//		{ ""+LC.INBOX_SEARCH_ORGANOGRAMID, "organogram_id" },
//		{ ""+LC.INBOX_SEARCH_TASKDECISION, "task_decision" },
//		{ ""+LC.INBOX_SEARCH_TASKCOMMENTS, "task_comments" },
//		{ ""+LC.INBOX_SEARCH_CREATEDAT, "created_at" },
//		{ ""+LC.INBOX_SEARCH_MODIFIEDAT, "modified_at" },
//		{ ""+LC.INBOX_SEARCH_CREATEDBY, "created_by" },
//		{ ""+LC.INBOX_SEARCH_MODIFIEDBY, "modified_by" },
//		{ ""+LC.INBOX_SEARCH_STATUS, "status" },
//		{ ""+LC.INBOX_SEARCH_ANYFIELD , "AnyField" }
//	};

//
//	public final static String NAV_INBOX_ACTIONS = "navINBOX_ACTIONS";
//	public final static String VIEW_INBOX_ACTIONS = "viewINBOX_ACTIONS";
//	public static final String[][] SEARCH_INBOX_ACTIONS = {
//		{ ""+LC.INBOX_ACTIONS_SEARCH_ACTIONEN, "action_en" },
//		{ ""+LC.INBOX_ACTIONS_SEARCH_ACTIONBN, "action_bn" },
//		{ ""+LC.INBOX_ACTIONS_SEARCH_LINK, "link" },
//		{ ""+LC.INBOX_ACTIONS_SEARCH_ICONLINK, "icon_link" },
//		{ ""+LC.INBOX_ACTIONS_SEARCH_STATUS, "status" },
//		{ ""+LC.INBOX_ACTIONS_SEARCH_ANYFIELD , "AnyField" }
//	};

//	public final static String NAV_INBOX_ACTION_ROLES = "navINBOX_ACTION_ROLES";
//	public final static String VIEW_INBOX_ACTION_ROLES = "viewINBOX_ACTION_ROLES";
//	public static final String[][] SEARCH_INBOX_ACTION_ROLES = {
//		{ ""+LC.INBOX_ACTION_ROLES_SEARCH_INBOXSTATUSID, "inbox_status_id" },
//		{ ""+LC.INBOX_ACTION_ROLES_SEARCH_ROLEID, "role_id" },
//		{ ""+LC.INBOX_ACTION_ROLES_SEARCH_ACTIONID, "action_id" },
//		{ ""+LC.INBOX_ACTION_ROLES_SEARCH_ANYFIELD , "AnyField" }
//	};

//	public final static String NAV_INBOX_MOVEMENTS = "navINBOX_MOVEMENTS";
//	public final static String VIEW_INBOX_MOVEMENTS = "viewINBOX_MOVEMENTS";
//	public static final String[][] SEARCH_INBOX_MOVEMENTS = {
//		{ ""+LC.INBOX_MOVEMENTS_SEARCH_INBOXID, "inbox_id" },
//		{ ""+LC.INBOX_MOVEMENTS_SEARCH_NOTE, "note" },
//		{ ""+LC.INBOX_MOVEMENTS_SEARCH_ACTION, "action" },
//		{ ""+LC.INBOX_MOVEMENTS_SEARCH_TOEMPLOYEEUSERID, "to_employee_user_id" },
//		{ ""+LC.INBOX_MOVEMENTS_SEARCH_FROMEMPLOYEEUSERID, "from_employee_user_id" },
//		{ ""+LC.INBOX_MOVEMENTS_SEARCH_TOORGANOGRAMID, "to_organogram_id" },
//		{ ""+LC.INBOX_MOVEMENTS_SEARCH_FROMORGANOGRAMID, "from_organogram_id" },
//		{ ""+LC.INBOX_MOVEMENTS_SEARCH_ISCURRENT, "is_current" },
//		{ ""+LC.INBOX_MOVEMENTS_SEARCH_ISCC, "is_cc" },
//		{ ""+LC.INBOX_MOVEMENTS_SEARCH_CREATEDAT, "created_at" },
//		{ ""+LC.INBOX_MOVEMENTS_SEARCH_MODIFIEDAT, "modified_at" },
//		{ ""+LC.INBOX_MOVEMENTS_SEARCH_CREATEDBY, "created_by" },
//		{ ""+LC.INBOX_MOVEMENTS_SEARCH_MODIFIEDBY, "modified_by" },
//		{ ""+LC.INBOX_MOVEMENTS_SEARCH_DEADLINEDATE, "deadline_date" },
//		{ ""+LC.INBOX_MOVEMENTS_SEARCH_CURRENTSTATUS, "current_status" },
//		{ ""+LC.INBOX_MOVEMENTS_SEARCH_ISSEEN, "is_seen" },
//		{ ""+LC.INBOX_MOVEMENTS_SEARCH_ASSIGNEDROLE, "assigned_role" },
//		{ ""+LC.INBOX_MOVEMENTS_SEARCH_ANYFIELD , "AnyField" }
//	};
//

//	public final static String NAV_INBOX_NOTIFICATION_TEMPLATE = "navINBOX_NOTIFICATION_TEMPLATE";
//	public final static String VIEW_INBOX_NOTIFICATION_TEMPLATE = "viewINBOX_NOTIFICATION_TEMPLATE";
//	public static final String[][] SEARCH_INBOX_NOTIFICATION_TEMPLATE = {
//		{ ""+LC.INBOX_NOTIFICATION_TEMPLATE_SEARCH_INBOXACTIONROLEID, "inbox_action_role_id" },
//		{ ""+LC.INBOX_NOTIFICATION_TEMPLATE_SEARCH_TEMPLATENAME, "template_name" },
//		{ ""+LC.INBOX_NOTIFICATION_TEMPLATE_SEARCH_SUBJECTBN, "subject_bn" },
//		{ ""+LC.INBOX_NOTIFICATION_TEMPLATE_SEARCH_SUBJECTEN, "subject_en" },
//		{ ""+LC.INBOX_NOTIFICATION_TEMPLATE_SEARCH_BODYEN, "body_en" },
//		{ ""+LC.INBOX_NOTIFICATION_TEMPLATE_SEARCH_BODYBN, "body_bn" },
//		{ ""+LC.INBOX_NOTIFICATION_TEMPLATE_SEARCH_STATUS, "status" },
//		{ ""+LC.INBOX_NOTIFICATION_TEMPLATE_SEARCH_INBOXNOTIFICATIONTYPE, "inbox_notification_type" },
//		{ ""+LC.INBOX_NOTIFICATION_TEMPLATE_SEARCH_ANYFIELD , "AnyField" }
//	};

//	public final static String NAV_INBOX_NOTIFICATION_TYPE = "navINBOX_NOTIFICATION_TYPE";
//	public final static String VIEW_INBOX_NOTIFICATION_TYPE = "viewINBOX_NOTIFICATION_TYPE";
//	public static final String[][] SEARCH_INBOX_NOTIFICATION_TYPE = {
//		{ ""+LC.INBOX_NOTIFICATION_TYPE_SEARCH_DESCRIPTION, "description" },
//		{ ""+LC.INBOX_NOTIFICATION_TYPE_SEARCH_ANYFIELD , "AnyField" }
//	};

//	public final static String NAV_INBOX_ORIGINATOR = "navINBOX_ORIGINATOR";
//	public final static String VIEW_INBOX_ORIGINATOR = "viewINBOX_ORIGINATOR";
//	public static final String[][] SEARCH_INBOX_ORIGINATOR = {
//		{ ""+LC.INBOX_ORIGINATOR_SEARCH_BSADID, "bsad_id" },
//		{ ""+LC.INBOX_ORIGINATOR_SEARCH_ANYFIELD , "AnyField" }
//	};

//	public final static String NAV_INBOX_ROLE_ORGANOGRAM = "navINBOX_ROLE_ORGANOGRAM";
//	public final static String VIEW_INBOX_ROLE_ORGANOGRAM = "viewINBOX_ROLE_ORGANOGRAM";
//	public static final String[][] SEARCH_INBOX_ROLE_ORGANOGRAM = {
//		{ ""+LC.INBOX_ROLE_ORGANOGRAM_SEARCH_ROLEID, "role_id" },
//		{ ""+LC.INBOX_ROLE_ORGANOGRAM_SEARCH_ORGANOGRAMID, "organogram_id" },
//		{ ""+LC.INBOX_ROLE_ORGANOGRAM_SEARCH_ANYFIELD , "AnyField" }
//	};

//	public final static String NAV_INBOX_STATUS = "navINBOX_STATUS";
//	public final static String VIEW_INBOX_STATUS = "viewINBOX_STATUS";
//	public static final String[][] SEARCH_INBOX_STATUS = {
//		{ ""+LC.INBOX_STATUS_SEARCH_STATUSNAMEEN, "status_name_en" },
//		{ ""+LC.INBOX_STATUS_SEARCH_STATUSNAMEBN, "status_name_bn" },
//		{ ""+LC.INBOX_STATUS_SEARCH_CREATEDBY, "created_by" },
//		{ ""+LC.INBOX_STATUS_SEARCH_MODIFIEDBY, "modified_by" },
//		{ ""+LC.INBOX_STATUS_SEARCH_CREATEDAT, "created_at" },
//		{ ""+LC.INBOX_STATUS_SEARCH_MODIFIEDAT, "modified_at" },
//		{ ""+LC.INBOX_STATUS_SEARCH_STATUS, "status" },
//		{ ""+LC.INBOX_STATUS_SEARCH_ANYFIELD , "AnyField" }
//	};

//	public final static String NAV_INBOX_TAB = "navINBOX_TAB";
//	public final static String VIEW_INBOX_TAB = "viewINBOX_TAB";
//	public static final String[][] SEARCH_INBOX_TAB = {
//		{ ""+LC.INBOX_TAB_SEARCH_NAMEEN, "name_en" },
//		{ ""+LC.INBOX_TAB_SEARCH_NAMEBN, "name_bn" },
//		{ ""+LC.INBOX_TAB_SEARCH_QUERY, "query" },
//		{ ""+LC.INBOX_TAB_SEARCH_STATUS, "status" },
//		{ ""+LC.INBOX_TAB_SEARCH_ACTIONCOLUMN, "action_column" },
//		{ ""+LC.INBOX_TAB_SEARCH_ANYFIELD , "AnyField" }
//	};

//	public final static String NAV_INBOX_TAB_COLUMN = "navINBOX_TAB_COLUMN";
//	public final static String VIEW_INBOX_TAB_COLUMN = "viewINBOX_TAB_COLUMN";
//	public static final String[][] SEARCH_INBOX_TAB_COLUMN = {
//		{ ""+LC.INBOX_TAB_COLUMN_SEARCH_INBOXTABID, "inbox_tab_id" },
//		{ ""+LC.INBOX_TAB_COLUMN_SEARCH_INBOXCOLUMNNAMEEN, "inbox_column_name_en" },
//		{ ""+LC.INBOX_TAB_COLUMN_SEARCH_INBOXCOLUMNNAMEBN, "inbox_column_name_bn" },
//		{ ""+LC.INBOX_TAB_COLUMN_SEARCH_ISSORTED, "isSorted" },
//		{ ""+LC.INBOX_TAB_COLUMN_SEARCH_VISIBILITY, "visibility" },
//		{ ""+LC.INBOX_TAB_COLUMN_SEARCH_HASHYPERLINK, "hasHyperlink" },
//		{ ""+LC.INBOX_TAB_COLUMN_SEARCH_ANYFIELD , "AnyField" }
//	};

//	public final static String NAV_MESSAGE_TYPE = "navMESSAGE_TYPE";
//	public final static String VIEW_MESSAGE_TYPE = "viewMESSAGE_TYPE";
//	public static final String[][] SEARCH_MESSAGE_TYPE = {
//		{ ""+LC.MESSAGE_TYPE_SEARCH_DESCRIPTION, "description" },
//		{ ""+LC.MESSAGE_TYPE_SEARCH_ANYFIELD , "AnyField" }
//	};

//	public final static String NAV_USER_ORGANOGRAM = "navUSER_ORGANOGRAM";
//	public final static String VIEW_USER_ORGANOGRAM = "viewUSER_ORGANOGRAM";
//	public static final String[][] SEARCH_USER_ORGANOGRAM = {
//		{ ""+LC.USER_ORGANOGRAM_SEARCH_USERID, "user_id" },
//		{ ""+LC.USER_ORGANOGRAM_SEARCH_ORGANOGRAMID, "organogram_id" },
//		{ ""+LC.USER_ORGANOGRAM_SEARCH_STATUS, "status" },
//		{ ""+LC.USER_ORGANOGRAM_SEARCH_ANYFIELD , "AnyField" }
//	};

//	public final static String NAV_USER_ORGANOGRAM_HISTORY = "navUSER_ORGANOGRAM_HISTORY";
//	public final static String VIEW_USER_ORGANOGRAM_HISTORY = "viewUSER_ORGANOGRAM_HISTORY";
//	public static final String[][] SEARCH_USER_ORGANOGRAM_HISTORY = {
//		{ ""+LC.USER_ORGANOGRAM_HISTORY_SEARCH_USERORGANOGRAMID, "user_organogram_id" },
//		{ ""+LC.USER_ORGANOGRAM_HISTORY_SEARCH_USERNAME, "username" },
//		{ ""+LC.USER_ORGANOGRAM_HISTORY_SEARCH_ACCESSDATE, "access_date" },
//		{ ""+LC.USER_ORGANOGRAM_HISTORY_SEARCH_ANYFIELD , "AnyField" }
//	};

    public final static String NAV_USER_ORGANOGRAM = "navUSER_ORGANOGRAM";
    public final static String VIEW_USER_ORGANOGRAM = "viewUSER_ORGANOGRAM";
    public static final String[][] SEARCH_USER_ORGANOGRAM = {{"" + LC.USER_ORGANOGRAM_SEARCH_USERID, "user_id"},
            {"" + LC.USER_ORGANOGRAM_SEARCH_ORGANOGRAMID, "organogram_id"},
            {"" + LC.USER_ORGANOGRAM_SEARCH_STATUS, "status"},
            {"" + LC.USER_ORGANOGRAM_SEARCH_ANYFIELD, "AnyField"}};

//	public final static String NAV_USER_ORGANOGRAM_HISTORY = "navUSER_ORGANOGRAM_HISTORY";
//	public final static String VIEW_USER_ORGANOGRAM_HISTORY = "viewUSER_ORGANOGRAM_HISTORY";
//	public static final String[][] SEARCH_USER_ORGANOGRAM_HISTORY = {
//		{ ""+LC.USER_ORGANOGRAM_HISTORY_SEARCH_USERORGANOGRAMID, "user_organogram_id" },
//		{ ""+LC.USER_ORGANOGRAM_HISTORY_SEARCH_USERNAME, "username" },
//		{ ""+LC.USER_ORGANOGRAM_HISTORY_SEARCH_ACCESSDATE, "access_date" },
//		{ ""+LC.USER_ORGANOGRAM_HISTORY_SEARCH_ANYFIELD , "AnyField" }
//	};

    public final static String NAV_USER_ORGANOGRAM_HISTORY = "navUSER_ORGANOGRAM_HISTORY";
    public final static String VIEW_USER_ORGANOGRAM_HISTORY = "viewUSER_ORGANOGRAM_HISTORY";
    public static final String[][] SEARCH_USER_ORGANOGRAM_HISTORY = {
            {"" + LC.USER_ORGANOGRAM_HISTORY_SEARCH_USERORGANOGRAMID, "user_organogram_id"},
            {"" + LC.USER_ORGANOGRAM_HISTORY_SEARCH_USERNAME, "username"},
            {"" + LC.USER_ORGANOGRAM_HISTORY_SEARCH_ACCESSDATE, "access_date"},
            {"" + LC.USER_ORGANOGRAM_HISTORY_SEARCH_ANYFIELD, "AnyField"}};

//	public final static String NAV_INBOX_TAB = "navINBOX_TAB";
//	public final static String VIEW_INBOX_TAB = "viewINBOX_TAB";
//	public static final String[][] SEARCH_INBOX_TAB = {
//		//{ ""+LC.INBOX_TAB_SEARCH_NAMEEN, "name_en" },
//		//{ ""+LC.INBOX_TAB_SEARCH_NAMEBN, "name_bn" },
//		{ ""+LC.INBOX_TAB_SEARCH_QUERYTAB, "query_tab" },
//		{ ""+LC.INBOX_TAB_SEARCH_TABTABLEHEADER, "tab_table_header" },
//		{ ""+LC.INBOX_TAB_SEARCH_DETAILSLINK, "details_link" },
//		{ ""+LC.INBOX_TAB_SEARCH_QUERYDETAILS, "query_details" },
//		{ ""+LC.INBOX_TAB_SEARCH_STATUS, "status" },
//		{ ""+LC.INBOX_TAB_SEARCH_ACTIONCOLUMN, "action_column" },
//		{ ""+LC.INBOX_TAB_SEARCH_ANYFIELD , "AnyField" }
//	};

    public final static String NAV_BUG_TRACKER = "navBUG_TRACKER";
    public final static String VIEW_BUG_TRACKER = "viewBUG_TRACKER";
    public static final String[][] SEARCH_BUG_TRACKER = {{"" + LC.BUG_TRACKER_SEARCH_NAMEEN, "name_en"},
            {"" + LC.BUG_TRACKER_SEARCH_NAMEBN, "name_bn"}, {"" + LC.BUG_TRACKER_SEARCH_DESCRIPTION, "description"},
            {"" + LC.BUG_TRACKER_SEARCH_CREATORNAME, "creator_name"},
            {"" + LC.BUG_TRACKER_SEARCH_ASSIGNEENAME, "assignee_name"},
            {"" + LC.BUG_TRACKER_SEARCH_DATEOFCREATION, "date_of_creation"},
            {"" + LC.BUG_TRACKER_SEARCH_DATEOFSOLUTION, "date_of_solution"},
            {"" + LC.BUG_TRACKER_SEARCH_BUGSTATUSTYPE, "bug_status_type"},
            {"" + LC.BUG_TRACKER_SEARCH_ANYFIELD, "AnyField"}};

//	public final static String NAV_BUG_TRACKER_COMMENT_DETAILS = "navBUG_TRACKER_COMMENT_DETAILS";
//	public final static String VIEW_BUG_TRACKER_COMMENT_DETAILS = "viewBUG_TRACKER_COMMENT_DETAILS";
//	public static final String[][] SEARCH_BUG_TRACKER_COMMENT_DETAILS = {
//		{ ""+LC.BUG_TRACKER_COMMENT_DETAILS_SEARCH_BUGTRACKERID, "bug_tracker_id" },
//		{ ""+LC.BUG_TRACKER_COMMENT_DETAILS_SEARCH_DESCRIPTION, "description" },
//		{ ""+LC.BUG_TRACKER_COMMENT_DETAILS_SEARCH_CREATORNAME, "creator_name" },
//		{ ""+LC.BUG_TRACKER_COMMENT_DETAILS_SEARCH_DATEOFCOMMENTING, "date_of_commenting" },
//		{ ""+LC.BUG_TRACKER_COMMENT_DETAILS_SEARCH_ANYFIELD , "AnyField" }
//	};
//
//
//
//
//	public final static String NAV_BUG_STATUS = "navBUG_STATUS";
//	public final static String VIEW_BUG_STATUS = "viewBUG_STATUS";
//	public static final String[][] SEARCH_BUG_STATUS = {
//		{ ""+LC.BUG_STATUS_SEARCH_NAMEEN, "name_en" },
//		{ ""+LC.BUG_STATUS_SEARCH_NAMEBN, "name_bn" },
//		{ ""+LC.BUG_STATUS_SEARCH_ANYFIELD , "AnyField" }
//	};

    public final static String NAV_APPLICANT_PROFILE = "navAPPLICANT_PROFILE";
    public final static String VIEW_APPLICANT_PROFILE = "viewAPPLICANT_PROFILE";
    public static final String[][] SEARCH_APPLICANT_PROFILE = {
            {"" + LC.APPLICANT_PROFILE_SEARCH_APPLICANTTYPE, "applicant_type"},
            {"" + LC.APPLICANT_PROFILE_SEARCH_SERVICEID, "service_id"},
            {"" + LC.APPLICANT_PROFILE_SEARCH_SERVICETYPE, "service_type"},
            {"" + LC.APPLICANT_PROFILE_SEARCH_CHIEFSHAREHOLDERINFO, "chief_shareholder_info"},
            {"" + LC.APPLICANT_PROFILE_SEARCH_NAME, "name"},
            {"" + LC.APPLICANT_PROFILE_SEARCH_FATHERNAME, "father_name"},
            {"" + LC.APPLICANT_PROFILE_SEARCH_HUSBANDNAME, "husband_name"},
            {"" + LC.APPLICANT_PROFILE_SEARCH_ADDRESSPRESENT, "address_present"},
            {"" + LC.APPLICANT_PROFILE_SEARCH_ADDRESSPERMANENT, "address_permanent"},
            {"" + LC.APPLICANT_PROFILE_SEARCH_NAMEBN, "name_bn"},
            {"" + LC.APPLICANT_PROFILE_SEARCH_FATHERNAMEBN, "father_name_bn"},
            {"" + LC.APPLICANT_PROFILE_SEARCH_HUSBANDNAMEBN, "husband_name_bn"},
            {"" + LC.APPLICANT_PROFILE_SEARCH_ADDRESSPRESENTBN, "address_present_bn"},
            {"" + LC.APPLICANT_PROFILE_SEARCH_ADDRESSPERMANENTBN, "address_permanent_bn"},
            {"" + LC.APPLICANT_PROFILE_SEARCH_NATIONALITY, "nationality"},
            {"" + LC.APPLICANT_PROFILE_SEARCH_AGE, "age"}, {"" + LC.APPLICANT_PROFILE_SEARCH_GENDER, "gender"},
            {"" + LC.APPLICANT_PROFILE_SEARCH_NID, "nid"}, {"" + LC.APPLICANT_PROFILE_SEARCH_CAPITAL, "capital"},
            {"" + LC.APPLICANT_PROFILE_SEARCH_PREVIOUS, "previous"},
            {"" + LC.APPLICANT_PROFILE_SEARCH_BANKSOLVENCY, "bank_solvency"},
            {"" + LC.APPLICANT_PROFILE_SEARCH_PICTURE, "picture"},
            {"" + LC.APPLICANT_PROFILE_SEARCH_ANYFIELD, "AnyField"}};

//	public final static String NAV_APPLICANT_TYPE = "navAPPLICANT_TYPE";
//	public final static String VIEW_APPLICANT_TYPE = "viewAPPLICANT_TYPE";
//	public static final String[][] SEARCH_APPLICANT_TYPE = {
//		{ ""+LC.APPLICANT_TYPE_SEARCH_DESCRIPTION, "description" },
//		{ ""+LC.APPLICANT_TYPE_SEARCH_DESCRIPTIONBANGLA, "description_bangla" },
//		{ ""+LC.APPLICANT_TYPE_SEARCH_ANYFIELD , "AnyField" }
//	};

//	public final static String NAV_APPLICANT_TYPE = "navAPPLICANT_TYPE";
//	public final static String VIEW_APPLICANT_TYPE = "viewAPPLICANT_TYPE";
//	public static final String[][] SEARCH_APPLICANT_TYPE = {
//		{ ""+LC.APPLICANT_TYPE_SEARCH_DESCRIPTION, "description" },
//		{ ""+LC.APPLICANT_TYPE_SEARCH_DESCRIPTIONBANGLA, "description_bangla" },
//		{ ""+LC.APPLICANT_TYPE_SEARCH_ANYFIELD , "AnyField" }
//	};
//
//
//
//
//	public final static String NAV_APPLICATION_TYPE = "navAPPLICATION_TYPE";
//	public final static String VIEW_APPLICATION_TYPE = "viewAPPLICATION_TYPE";
//	public static final String[][] SEARCH_APPLICATION_TYPE = {
//		{ ""+LC.APPLICATION_TYPE_SEARCH_DESRIPTION, "desription" },
//		{ ""+LC.APPLICATION_TYPE_SEARCH_DESCRIPTIONBANGLA, "description_bangla" },
//		{ ""+LC.APPLICATION_TYPE_SEARCH_ANYFIELD , "AnyField" }
//	};

//	public final static String NAV_COMMITTEE = "navCOMMITTEE";
//	public final static String VIEW_COMMITTEE = "viewCOMMITTEE";
//	public static final String[][] SEARCH_COMMITTEE = {
//		{ ""+LC.COMMITTEE_SEARCH_SERVICEID, "service_id" },
//		{ ""+LC.COMMITTEE_SEARCH_SERVICENUMBER, "service_number" },
//		{ ""+LC.COMMITTEE_SEARCH_COMMITTEENUMBER, "committee_number" },
//		{ ""+LC.COMMITTEE_SEARCH_CREATEDATE, "create_date" },
//		{ ""+LC.COMMITTEE_SEARCH_CREATORID, "creator_id" },
//		{ ""+LC.COMMITTEE_SEARCH_CREATORNUMBER, "creator_number" },
//		{ ""+LC.COMMITTEE_SEARCH_NAME, "name" },
//		{ ""+LC.COMMITTEE_SEARCH_ANYFIELD , "AnyField" }
//	};

    public final static String NAV_COMMITTEE = "navCOMMITTEE";
    public final static String VIEW_COMMITTEE = "viewCOMMITTEE";
    public static final String[][] SEARCH_COMMITTEE = {{"" + LC.COMMITTEE_SEARCH_SERVICEID, "service_id"},
            {"" + LC.COMMITTEE_SEARCH_SERVICETYPES, "service_types"},
            {"" + LC.COMMITTEE_SEARCH_COMMITTEETYPES, "committee_types"},
            {"" + LC.COMMITTEE_SEARCH_CREATEDATE, "create_date"},
            {"" + LC.COMMITTEE_SEARCH_CREATORID, "creator_id"},
            {"" + LC.COMMITTEE_SEARCH_CREATORTYPES, "creator_types"}, {"" + LC.COMMITTEE_SEARCH_NAME, "name"},
            {"" + LC.COMMITTEE_SEARCH_ANYFIELD, "AnyField"}};

    public final static String NAV_COMMITTEE_MEMBER = "navCOMMITTEE_MEMBER";
    public final static String VIEW_COMMITTEE_MEMBER = "viewCOMMITTEE_MEMBER";
    public static final String[][] SEARCH_COMMITTEE_MEMBER = {{"" + LC.COMMITTEE_MEMBER_SEARCH_NAME, "name"},
            {"" + LC.COMMITTEE_MEMBER_SEARCH_OFFICE, "office"},
            {"" + LC.COMMITTEE_MEMBER_SEARCH_DEPARTMENT, "department"},
            {"" + LC.COMMITTEE_MEMBER_SEARCH_DESIGNATION, "designation"},
            {"" + LC.COMMITTEE_MEMBER_SEARCH_COMMITTEEID, "committee_id"},
            {"" + LC.COMMITTEE_MEMBER_SEARCH_PHONE, "phone"}, {"" + LC.COMMITTEE_MEMBER_SEARCH_EMAIL, "email"},
            {"" + LC.COMMITTEE_MEMBER_SEARCH_STATUS, "status"},
            {"" + LC.COMMITTEE_MEMBER_SEARCH_COMMENTS, "comments"},
            {"" + LC.COMMITTEE_MEMBER_SEARCH_ANYFIELD, "AnyField"}};

//	public final static String NAV_COMMITTEE_SCHEDULE = "navCOMMITTEE_SCHEDULE";
//	public final static String VIEW_COMMITTEE_SCHEDULE = "viewCOMMITTEE_SCHEDULE";
//	public static final String[][] SEARCH_COMMITTEE_SCHEDULE = {
//		{ ""+LC.COMMITTEE_SCHEDULE_SEARCH_COMMITTEEID, "committee_id" },
//		{ ""+LC.COMMITTEE_SCHEDULE_SEARCH_MEETINGDATE, "meeting_date" },
//		{ ""+LC.COMMITTEE_SCHEDULE_SEARCH_DESCRIPTION, "description" },
//		{ ""+LC.COMMITTEE_SCHEDULE_SEARCH_LOCATION, "location" },
//		{ ""+LC.COMMITTEE_SCHEDULE_SEARCH_SCHEDULETYPE, "schedule_type" },
//		{ ""+LC.COMMITTEE_SCHEDULE_SEARCH_ANYFIELD , "AnyField" }
//	};

    public final static String NAV_COMMITTEE_SCHEDULE = "navCOMMITTEE_SCHEDULE";
    public final static String VIEW_COMMITTEE_SCHEDULE = "viewCOMMITTEE_SCHEDULE";
    public static final String[][] SEARCH_COMMITTEE_SCHEDULE = {
            {"" + LC.COMMITTEE_SCHEDULE_SEARCH_COMMITTEEID, "committee_id"},
            {"" + LC.COMMITTEE_SCHEDULE_SEARCH_MEETINGDATE, "meeting_date"},
            {"" + LC.COMMITTEE_SCHEDULE_SEARCH_DESCRIPTION, "description"},
            {"" + LC.COMMITTEE_SCHEDULE_SEARCH_LOCATION, "location"},
            {"" + LC.COMMITTEE_SCHEDULE_SEARCH_SCHEDULETYPES, "schedule_types"},
            {"" + LC.COMMITTEE_SCHEDULE_SEARCH_ANYFIELD, "AnyField"}};

    public final static String NAV_REQUIRED_DOCS = "navREQUIRED_DOCS";
    public final static String VIEW_REQUIRED_DOCS = "viewREQUIRED_DOCS";
    public static final String[][] SEARCH_REQUIRED_DOCS = {
            {"" + LC.REQUIRED_DOCS_SEARCH_SERVICETYPES, "service_types"},
            {"" + LC.REQUIRED_DOCS_SEARCH_NAMEEN, "name_en"}, {"" + LC.REQUIRED_DOCS_SEARCH_NAMEBN, "name_bn"},
            {"" + LC.REQUIRED_DOCS_SEARCH_ANYFIELD, "AnyField"}};

    public final static String NAV_INSPECTION_REPORT = "navINSPECTION_REPORT";
    public final static String VIEW_INSPECTION_REPORT = "viewINSPECTION_REPORT";
    public static final String[][] SEARCH_INSPECTION_REPORT = {
            {"" + LC.INSPECTION_REPORT_SEARCH_COMMITTEEID, "committee_id"},
            {"" + LC.INSPECTION_REPORT_SEARCH_SUBMISSIONDATE, "submission_date"},
            {"" + LC.INSPECTION_REPORT_SEARCH_REPORTDETAILS, "report_details"},
            {"" + LC.INSPECTION_REPORT_SEARCH_LOCATIONMAPDATA, "location_map_data"},
            {"" + LC.INSPECTION_REPORT_SEARCH_ANYFIELD, "AnyField"}};

    public final static String NAV_PAYMENT = "navPAYMENT";
    public final static String VIEW_PAYMENT = "viewPAYMENT";
    public static final String[][] SEARCH_PAYMENT = {{"" + LC.PAYMENT_SEARCH_SERVICESEEKERID, "service_seeker_id"},
            {"" + LC.PAYMENT_SEARCH_PAYMENTTYPES, "payment_types"}, {"" + LC.PAYMENT_SEARCH_AMOUNT, "amount"},
            {"" + LC.PAYMENT_SEARCH_PAYMENTDATE, "payment_date"},
            {"" + LC.PAYMENT_SEARCH_PAYMENTMETHOD, "payment_method"},
            {"" + LC.PAYMENT_SEARCH_TRANSACTIONIDENTIFICATIONNUMBER, "transaction_identification_number"},
            {"" + LC.PAYMENT_SEARCH_ECHALANIDENTIFICATIONNUMBER, "e_chalan_identification_number"},
            {"" + LC.PAYMENT_SEARCH_SERVICETYPES, "service_types"}, {"" + LC.PAYMENT_SEARCH_ANYFIELD, "AnyField"}};

    public final static String NAV_PAYMENT_NOTIFICATION = "navPAYMENT_NOTIFICATION";
    public final static String VIEW_PAYMENT_NOTIFICATION = "viewPAYMENT_NOTIFICATION";
    public static final String[][] SEARCH_PAYMENT_NOTIFICATION = {
            {"" + LC.PAYMENT_NOTIFICATION_SEARCH_LICENSEID, "license_id"},
            {"" + LC.PAYMENT_NOTIFICATION_SEARCH_MESSAGE, "message"},
            {"" + LC.PAYMENT_NOTIFICATION_SEARCH_SENDDATE, "send_date"},
            {"" + LC.PAYMENT_NOTIFICATION_SEARCH_PAYMENTTYPES, "payment_types"},
            {"" + LC.PAYMENT_NOTIFICATION_SEARCH_ANYFIELD, "AnyField"}};

    public final static String NAV_PAYMENT_SCHEDULE = "navPAYMENT_SCHEDULE";
    public final static String VIEW_PAYMENT_SCHEDULE = "viewPAYMENT_SCHEDULE";
    public static final String[][] SEARCH_PAYMENT_SCHEDULE = {
            {"" + LC.PAYMENT_SCHEDULE_SEARCH_LICENSEID, "license_id"},
            {"" + LC.PAYMENT_SCHEDULE_SEARCH_PAYMENTTYPES, "payment_types"},
            {"" + LC.PAYMENT_SCHEDULE_SEARCH_AMOUNT, "amount"},
            {"" + LC.PAYMENT_SCHEDULE_SEARCH_PAYMENTLASTDATE, "payment_last_date"},
            {"" + LC.PAYMENT_SCHEDULE_SEARCH_ANYFIELD, "AnyField"}};

    public final static String NAV_PAYMENT_TYPE = "navPAYMENT_TYPE";
    public final static String VIEW_PAYMENT_TYPE = "viewPAYMENT_TYPE";
    public static final String[][] SEARCH_PAYMENT_TYPE = {{"" + LC.PAYMENT_TYPE_SEARCH_NAMEEN, "name_en"},
            {"" + LC.PAYMENT_TYPE_SEARCH_NAMEBN, "name_bn"}, {"" + LC.PAYMENT_TYPE_SEARCH_ANYFIELD, "AnyField"}};

    public final static String NAV_TEMPLATES = "navTEMPLATES";
    public final static String VIEW_TEMPLATES = "viewTEMPLATES";
    public static final String[][] SEARCH_TEMPLATES = {{"" + LC.TEMPLATES_SEARCH_DESCRIPTION, "description"},
            {"" + LC.TEMPLATES_SEARCH_DATA, "data"}, {"" + LC.TEMPLATES_SEARCH_CREATEDATE, "create_date"},
            {"" + LC.TEMPLATES_SEARCH_UPDATEDATE, "update_date"}, {"" + LC.TEMPLATES_SEARCH_ANYFIELD, "AnyField"}};

//	public final static String NAV_INBOX_NOTIFICATION_TEMPLATE = "navINBOX_NOTIFICATION_TEMPLATE";
//	public final static String VIEW_INBOX_NOTIFICATION_TEMPLATE = "viewINBOX_NOTIFICATION_TEMPLATE";
//	public static final String[][] SEARCH_INBOX_NOTIFICATION_TEMPLATE = {
//		{ ""+LC.INBOX_NOTIFICATION_TEMPLATE_SEARCH_INBOXACTIONROLEID, "inbox_action_role_id" },
//		{ ""+LC.INBOX_NOTIFICATION_TEMPLATE_SEARCH_TEMPLATENAME, "template_name" },
//		{ ""+LC.INBOX_NOTIFICATION_TEMPLATE_SEARCH_SUBJECTBN, "subject_bn" },
//		{ ""+LC.INBOX_NOTIFICATION_TEMPLATE_SEARCH_SUBJECTEN, "subject_en" },
//		{ ""+LC.INBOX_NOTIFICATION_TEMPLATE_SEARCH_BODYEN, "body_en" },
//		{ ""+LC.INBOX_NOTIFICATION_TEMPLATE_SEARCH_BODYBN, "body_bn" },
//		{ ""+LC.INBOX_NOTIFICATION_TEMPLATE_SEARCH_STATUS, "status" },
//		{ ""+LC.INBOX_NOTIFICATION_TEMPLATE_SEARCH_INBOXNOTIFICATIONTYPES, "inbox_notification_types" },
//		{ ""+LC.INBOX_NOTIFICATION_TEMPLATE_SEARCH_ANYFIELD , "AnyField" }
//	};

    public final static String NAV_NEWS = "navNEWS";
    public final static String VIEW_NEWS = "viewNEWS";
    public static final String[][] SEARCH_NEWS = {{"" + LC.NEWS_SEARCH_NAMEEN, "name_en"},
            {"" + LC.NEWS_SEARCH_NAMEBN, "name_bn"}, {"" + LC.NEWS_SEARCH_PUBLISHDATE, "publish_date"},
            {"" + LC.NEWS_SEARCH_ANYFIELD, "AnyField"}};

    public final static String NAV_TENDER_OPENING = "navTENDER_OPENING";
    public final static String VIEW_TENDER_OPENING = "viewTENDER_OPENING";
    public static final String[][] SEARCH_TENDER_OPENING = {
            {"" + LC.TENDER_OPENING_SEARCH_TENDEROPENINGDATE, "tender_opening_date"},
            {"" + LC.TENDER_OPENING_SEARCH_USERID, "user_id"}, {"" + LC.TENDER_OPENING_SEARCH_ISOISF, "is_oisf"},
            {"" + LC.TENDER_OPENING_SEARCH_COMMENTS, "comments"},
            {"" + LC.TENDER_OPENING_SEARCH_ANYFIELD, "AnyField"}};

    public final static String NAV_TENDER_PUBLICATION = "navTENDER_PUBLICATION";
    public final static String VIEW_TENDER_PUBLICATION = "viewTENDER_PUBLICATION";
    public static final String[][] SEARCH_TENDER_PUBLICATION = {
            {"" + LC.TENDER_PUBLICATION_SEARCH_PUBLISHDATE, "publish_date"},
            {"" + LC.TENDER_PUBLICATION_SEARCH_MEDIA, "media"},
            {"" + LC.TENDER_PUBLICATION_SEARCH_DESCRIPTION, "description"},
            {"" + LC.TENDER_PUBLICATION_SEARCH_SERVICEID, "service_id"},
            {"" + LC.TENDER_PUBLICATION_SEARCH_SERVICETYPES, "service_types"},
            {"" + LC.TENDER_PUBLICATION_SEARCH_ANYFIELD, "AnyField"}};

    public final static String NAV_TENDER_REQUEST_LETTER = "navTENDER_REQUEST_LETTER";
    public final static String VIEW_TENDER_REQUEST_LETTER = "viewTENDER_REQUEST_LETTER";
    public static final String[][] SEARCH_TENDER_REQUEST_LETTER = {
            {"" + LC.TENDER_REQUEST_LETTER_SEARCH_DESCRIPTION, "description"},
            {"" + LC.TENDER_REQUEST_LETTER_SEARCH_REQUESTDATE, "request_date"},
            {"" + LC.TENDER_REQUEST_LETTER_SEARCH_SERVICEID, "service_id"},
            {"" + LC.TENDER_REQUEST_LETTER_SEARCH_SERVICETYPES, "service_types"},
            {"" + LC.TENDER_REQUEST_LETTER_SEARCH_REQUESTSENDERID, "request_sender_id"},
            {"" + LC.TENDER_REQUEST_LETTER_SEARCH_REQUESTSENDERTYPES, "request_sender_types"},
            {"" + LC.TENDER_REQUEST_LETTER_SEARCH_COMMITTEEID, "committee_id"},
            {"" + LC.TENDER_REQUEST_LETTER_SEARCH_ANYFIELD, "AnyField"}};

    public final static String NAV_COMMITTEE_FORMATION_REQUEST = "navCOMMITTEE_FORMATION_REQUEST";
    public final static String VIEW_COMMITTEE_FORMATION_REQUEST = "viewCOMMITTEE_FORMATION_REQUEST";
    public static final String[][] SEARCH_COMMITTEE_FORMATION_REQUEST = {
            {"" + LC.COMMITTEE_FORMATION_REQUEST_SEARCH_USERID, "user_id"},
            {"" + LC.COMMITTEE_FORMATION_REQUEST_SEARCH_ISOISF, "is_oisf"},
            {"" + LC.COMMITTEE_FORMATION_REQUEST_SEARCH_REQUESTDATE, "request_date"},
            {"" + LC.COMMITTEE_FORMATION_REQUEST_SEARCH_SERVICEID, "service_id"},
            {"" + LC.COMMITTEE_FORMATION_REQUEST_SEARCH_SERVICETYPES, "service_types"},
            {"" + LC.COMMITTEE_FORMATION_REQUEST_SEARCH_DESCRIPTION, "description"},
            {"" + LC.COMMITTEE_FORMATION_REQUEST_SEARCH_ANYFIELD, "AnyField"}};

    public final static String NAV_EXPLORATION = "navEXPLORATION";
    public final static String VIEW_EXPLORATION = "viewEXPLORATION";
    public static final String[][] SEARCH_EXPLORATION = {{"" + LC.EXPLORATION_SEARCH_CAPITAL, "capital"},
            {"" + LC.EXPLORATION_SEARCH_MINERALTYPES, "mineral_types"},
            {"" + LC.EXPLORATION_SEARCH_ADDRESS, "address"},
            {"" + LC.EXPLORATION_SEARCH_GEOLOCATION, "geolocation"}, {"" + LC.EXPLORATION_SEARCH_STATUS, "status"},
            {"" + LC.EXPLORATION_SEARCH_EXPLOREDATE, "explore_date"},
            {"" + LC.EXPLORATION_SEARCH_ANYFIELD, "AnyField"}};

    public final static String NAV_MINE = "navMINE";
    public final static String VIEW_MINE = "viewMINE";
    public static final String[][] SEARCH_MINE = {{"" + LC.MINE_SEARCH_NAMEEN, "name_en"},
            {"" + LC.MINE_SEARCH_NAMEBN, "name_bn"}, {"" + LC.MINE_SEARCH_ADDRESSEN, "address_en"},
            {"" + LC.MINE_SEARCH_ADDRESSBN, "address_bn"}, {"" + LC.MINE_SEARCH_MINETYPES, "mine_types"},
            {"" + LC.MINE_SEARCH_QUANTITY, "quantity"}, {"" + LC.MINE_SEARCH_ANYFIELD, "AnyField"}};

    public final static String NAV_MINERAL_TYPE = "navMINERAL_TYPE";
    public final static String VIEW_MINERAL_TYPE = "viewMINERAL_TYPE";
    public static final String[][] SEARCH_MINERAL_TYPE = {{"" + LC.MINERAL_TYPE_SEARCH_NAMEEN, "name_en"},
            {"" + LC.MINERAL_TYPE_SEARCH_NAMEBN, "name_bn"}, {"" + LC.MINERAL_TYPE_SEARCH_ANYFIELD, "AnyField"}};

    public final static String NAV_MINE_TYPE = "navMINE_TYPE";
    public final static String VIEW_MINE_TYPE = "viewMINE_TYPE";
    public static final String[][] SEARCH_MINE_TYPE = {{"" + LC.MINE_TYPE_SEARCH_NAMEEN, "name_en"},
            {"" + LC.MINE_TYPE_SEARCH_NAMEBN, "name_bn"}, {"" + LC.MINE_TYPE_SEARCH_ANYFIELD, "AnyField"}};

    public final static String NAV_QUARRY = "navQUARRY";
    public final static String VIEW_QUARRY = "viewQUARRY";
    public static final String[][] SEARCH_QUARRY = {{"" + LC.QUARRY_SEARCH_NAMEEN, "name_en"},
            {"" + LC.QUARRY_SEARCH_NAMEBN, "name_bn"}, {"" + LC.QUARRY_SEARCH_MINERALTYPES, "mineral_types"},
            {"" + LC.QUARRY_SEARCH_OWNERSHIP, "ownership"}, {"" + LC.QUARRY_SEARCH_VALUE, "value"},
            {"" + LC.QUARRY_SEARCH_ADDRESSEN, "address_en"}, {"" + LC.QUARRY_SEARCH_ADDRESSBN, "address_bn"},
            {"" + LC.QUARRY_SEARCH_QUANTITY, "quantity"}, {"" + LC.QUARRY_SEARCH_GEOLOCATION, "geolocation"},
            {"" + LC.QUARRY_SEARCH_LEASESTATUS, "lease_status"},
            {"" + LC.QUARRY_SEARCH_DISCOVERDATE, "discover_date"}, {"" + LC.QUARRY_SEARCH_ANYFIELD, "AnyField"}};

    public final static String NAV_COMPLAINTS = "navCOMPLAINTS";
    public final static String VIEW_COMPLAINTS = "viewCOMPLAINTS";
    public static final String[][] SEARCH_COMPLAINTS = {
            {"" + LC.COMPLAINTS_SEARCH_SERVICESEEKERID, "service_seeker_id"},
            {"" + LC.COMPLAINTS_SEARCH_SERVICETYPES, "service_types"},
            {"" + LC.COMPLAINTS_SEARCH_DESCRIPTION, "description"},
            {"" + LC.COMPLAINTS_SEARCH_SUBMISSIONDATE, "submission_date"},
            {"" + LC.COMPLAINTS_SEARCH_ANYFIELD, "AnyField"}};

    public final static String NAV_LESSEE = "navLESSEE";
    public final static String VIEW_LESSEE = "viewLESSEE";
    public static final String[][] SEARCH_LESSEE = {{"" + LC.LESSEE_SEARCH_APPLICATIONTYPES, "application_types"},
            {"" + LC.LESSEE_SEARCH_APPLICANTID, "applicant_id"}, {"" + LC.LESSEE_SEARCH_ISSUEDATE, "issue_date"},
            {"" + LC.LESSEE_SEARCH_EXPIREDATE, "expire_date"}, {"" + LC.LESSEE_SEARCH_COMMENTS, "comments"},
            {"" + LC.LESSEE_SEARCH_ANYFIELD, "AnyField"}};

    public final static String NAV_LICENSEE = "navLICENSEE";
    public final static String VIEW_LICENSEE = "viewLICENSEE";
    public static final String[][] SEARCH_LICENSEE = {{"" + LC.LICENSEE_SEARCH_APPLICATIONID, "application_id"},
            {"" + LC.LICENSEE_SEARCH_APPLICATIONTYPES, "application_types"},
            {"" + LC.LICENSEE_SEARCH_ISSUEDATE, "issue_date"}, {"" + LC.LICENSEE_SEARCH_EXPIREDATE, "expire_date"},
            {"" + LC.LICENSEE_SEARCH_COMMENTS, "comments"}, {"" + LC.LICENSEE_SEARCH_ANYFIELD, "AnyField"}};

//	public final static String NAV_DEMARCATION_REPORT = "navDEMARCATION_REPORT";
//	public final static String VIEW_DEMARCATION_REPORT = "viewDEMARCATION_REPORT";
//	public static final String[][] SEARCH_DEMARCATION_REPORT = {
//		{ ""+LC.DEMARCATION_REPORT_SEARCH_COMMITTEEID, "committee_id" },
//		{ ""+LC.DEMARCATION_REPORT_SEARCH_TEMPLATE, "template" },
//		{ ""+LC.DEMARCATION_REPORT_SEARCH_REPORTDATE, "report_date" },
//		{ ""+LC.DEMARCATION_REPORT_SEARCH_ANYFIELD , "AnyField" }
//	};

    public final static String NAV_DEMARCATION_REPORT = "navDEMARCATION_REPORT";
    public final static String VIEW_DEMARCATION_REPORT = "viewDEMARCATION_REPORT";
    public static final String[][] SEARCH_DEMARCATION_REPORT = {
            {"" + LC.DEMARCATION_REPORT_SEARCH_COMMITTEEID, "committee_id"},
            {"" + LC.DEMARCATION_REPORT_SEARCH_TEMPLATE, "template"},
            {"" + LC.DEMARCATION_REPORT_SEARCH_REPORTDATE, "report_date"},
            {"" + LC.DEMARCATION_REPORT_SEARCH_ANYFIELD, "AnyField"}};

    public final static String NAV_APPLICATION_EVALUATION = "navAPPLICATION_EVALUATION";
    public final static String VIEW_APPLICATION_EVALUATION = "viewAPPLICATION_EVALUATION";
    public static final String[][] SEARCH_APPLICATION_EVALUATION = {
            {"" + LC.APPLICATION_EVALUATION_SEARCH_APPLICANTID, "applicant_id"},
            {"" + LC.APPLICATION_EVALUATION_SEARCH_COMMITTEEID, "committee_id"},
            {"" + LC.APPLICATION_EVALUATION_SEARCH_STATUS, "status"},
            {"" + LC.APPLICATION_EVALUATION_SEARCH_COMMENTS, "comments"},
            {"" + LC.APPLICATION_EVALUATION_SEARCH_ANYFIELD, "AnyField"}};

    public final static String NAV_APPLICATION_SUBMISSION = "navAPPLICATION_SUBMISSION";
    public final static String VIEW_APPLICATION_SUBMISSION = "viewAPPLICATION_SUBMISSION";
    public static final String[][] SEARCH_APPLICATION_SUBMISSION = {
            {"" + LC.APPLICATION_SUBMISSION_SEARCH_APPLICANTID, "applicant_id"},
            {"" + LC.APPLICATION_SUBMISSION_SEARCH_SUBMISSIONDATE, "submission_date"},
            {"" + LC.APPLICATION_SUBMISSION_SEARCH_ANYFIELD, "AnyField"}};

    public final static String NAV_APPLICANT_PROFILE_TRANSIENT = "navAPPLICANT_PROFILE_TRANSIENT";
    public final static String VIEW_APPLICANT_PROFILE_TRANSIENT = "viewAPPLICANT_PROFILE_TRANSIENT";
    public static final String[][] SEARCH_APPLICANT_PROFILE_TRANSIENT = {
            {"" + LC.APPLICANT_PROFILE_TRANSIENT_SEARCH_APPLICANTID, "applicant_id"},
            {"" + LC.APPLICANT_PROFILE_TRANSIENT_SEARCH_ANYFIELD, "AnyField"}};

    public final static String NAV_COMPANY_PROFILE = "navCOMPANY_PROFILE";
    public final static String VIEW_COMPANY_PROFILE = "viewCOMPANY_PROFILE";
    public static final String[][] SEARCH_COMPANY_PROFILE = {
            {"" + LC.COMPANY_PROFILE_SEARCH_APPLICANTPROFILEID, "applicant_profile_id"},
            {"" + LC.COMPANY_PROFILE_SEARCH_DIRECTORNAME, "director_name"},
            {"" + LC.COMPANY_PROFILE_SEARCH_DIRECTORNATIONALITY, "director_nationality"},
            {"" + LC.COMPANY_PROFILE_SEARCH_DIRECTORAGE, "director_age"},
            {"" + LC.COMPANY_PROFILE_SEARCH_COMPANYADDRESS, "company_address"},
            {"" + LC.COMPANY_PROFILE_SEARCH_BUSINESSTYPES, "business_types"},
            {"" + LC.COMPANY_PROFILE_SEARCH_TECHNICALEXPERT, "technical_expert"},
            {"" + LC.COMPANY_PROFILE_SEARCH_ANYFIELD, "AnyField"}};

    public final static String NAV_COMPANY_REPORT = "navCOMPANY_REPORT";
    public final static String VIEW_COMPANY_REPORT = "viewCOMPANY_REPORT";
    public static final String[][] SEARCH_COMPANY_REPORT = {
            {"" + LC.COMPANY_REPORT_SEARCH_COMPANYPROFILEID, "company_profile_id"},
            {"" + LC.COMPANY_REPORT_SEARCH_REPORTDATA, "report_data"},
            {"" + LC.COMPANY_REPORT_SEARCH_ANYFIELD, "AnyField"}};

    public final static String NAV_DG_PERMIT_LEASE = "navDG_PERMIT_LEASE";
    public final static String VIEW_DG_PERMIT_LEASE = "viewDG_PERMIT_LEASE";
    public static final String[][] SEARCH_DG_PERMIT_LEASE = {
            {"" + LC.DG_PERMIT_LEASE_SEARCH_PERMITDATE, "permit_date"},
            {"" + LC.DG_PERMIT_LEASE_SEARCH_SERVICEID, "service_id"},
            {"" + LC.DG_PERMIT_LEASE_SEARCH_SERVICETYPES, "service_types"},
            {"" + LC.DG_PERMIT_LEASE_SEARCH_APPLICANTID, "applicant_id"},
            {"" + LC.DG_PERMIT_LEASE_SEARCH_LEASESTARTDATE, "lease_start_date"},
            {"" + LC.DG_PERMIT_LEASE_SEARCH_LEASEEXPIREDATE, "lease_expire_date"},
            {"" + LC.DG_PERMIT_LEASE_SEARCH_GRANTTEMPLATE, "grant_template"},
            {"" + LC.DG_PERMIT_LEASE_SEARCH_ANYFIELD, "AnyField"}};

    public final static String NAV_DG_SIGN_AGREEMENT = "navDG_SIGN_AGREEMENT";
    public final static String VIEW_DG_SIGN_AGREEMENT = "viewDG_SIGN_AGREEMENT";
    public static final String[][] SEARCH_DG_SIGN_AGREEMENT = {
            {"" + LC.DG_SIGN_AGREEMENT_SEARCH_SIGNDATE, "sign_date"},
            {"" + LC.DG_SIGN_AGREEMENT_SEARCH_SERVICEID, "service_id"},
            {"" + LC.DG_SIGN_AGREEMENT_SEARCH_SERVICETYPES, "service_types"},
            {"" + LC.DG_SIGN_AGREEMENT_SEARCH_APPLICANTID, "applicant_id"},
            {"" + LC.DG_SIGN_AGREEMENT_SEARCH_OFFICERID, "officer_id"},
            {"" + LC.DG_SIGN_AGREEMENT_SEARCH_OFFICERTYPES, "officer_types"},
            {"" + LC.DG_SIGN_AGREEMENT_SEARCH_AGREEMENTTEMPLATE, "agreement_template"},
            {"" + LC.DG_SIGN_AGREEMENT_SEARCH_ANYFIELD, "AnyField"}};

    public final static String NAV_ELIGIBILITY_CRITERIA = "navELIGIBILITY_CRITERIA";
    public final static String VIEW_ELIGIBILITY_CRITERIA = "viewELIGIBILITY_CRITERIA";
    public static final String[][] SEARCH_ELIGIBILITY_CRITERIA = {
            {"" + LC.ELIGIBILITY_CRITERIA_SEARCH_DESCRIPTION, "description"},
            {"" + LC.ELIGIBILITY_CRITERIA_SEARCH_SERVICEID, "service_id"},
            {"" + LC.ELIGIBILITY_CRITERIA_SEARCH_SERVICETYPES, "service_types"},
            {"" + LC.ELIGIBILITY_CRITERIA_SEARCH_ANYFIELD, "AnyField"}};

    public final static String NAV_EXPIRY_INFO = "navEXPIRY_INFO";
    public final static String VIEW_EXPIRY_INFO = "viewEXPIRY_INFO";
    public static final String[][] SEARCH_EXPIRY_INFO = {
            {"" + LC.EXPIRY_INFO_SEARCH_LESSEELICENSEEID, "lessee_licensee_id"},
            {"" + LC.EXPIRY_INFO_SEARCH_LESSEELICENSEETYPES, "lessee_licensee_types"},
            {"" + LC.EXPIRY_INFO_SEARCH_RENEWALFLAG, "renewal_flag"},
            {"" + LC.EXPIRY_INFO_SEARCH_ISSUEDATE, "issue_date"},
            {"" + LC.EXPIRY_INFO_SEARCH_EXPIREDATE, "expire_date"},
            {"" + LC.EXPIRY_INFO_SEARCH_ANYFIELD, "AnyField"}};

    public final static String NAV_PORTAL_FEEDBACK = "navPORTAL_FEEDBACK";
    public final static String VIEW_PORTAL_FEEDBACK = "viewPORTAL_FEEDBACK";
    public static final String[][] SEARCH_PORTAL_FEEDBACK = {{"" + LC.PORTAL_FEEDBACK_SEARCH_FORM, "form"},
            {"" + LC.PORTAL_FEEDBACK_SEARCH_SUBJECT, "subject"},
            {"" + LC.PORTAL_FEEDBACK_SEARCH_MESSAGE, "message"},
            {"" + LC.PORTAL_FEEDBACK_SEARCH_ACTIONTAKEN, "action_taken"},
            {"" + LC.PORTAL_FEEDBACK_SEARCH_FORWARDEDTO, "forwarded_to"},
            {"" + LC.PORTAL_FEEDBACK_SEARCH_STATUS, "status"},
            {"" + LC.PORTAL_FEEDBACK_SEARCH_ANYFIELD, "AnyField"}};

//	public final static String NAV_REGISTER_LEASE_BMD = "navREGISTER_LEASE_BMD";
//	public final static String VIEW_REGISTER_LEASE_BMD = "viewREGISTER_LEASE_BMD";
//	public static final String[][] SEARCH_REGISTER_LEASE_BMD = {
//		{ ""+LC.REGISTER_LEASE_BMD_SEARCH_SERIALNUMBER, "serial_number" },
//		{ ""+LC.REGISTER_LEASE_BMD_SEARCH_APPLICANTNAME, "applicant_name" },
//		{ ""+LC.REGISTER_LEASE_BMD_SEARCH_PRESENTADDRESS, "present_address" },
//		{ ""+LC.REGISTER_LEASE_BMD_SEARCH_PERMANENTADDRESS, "permanent_address" },
//		{ ""+LC.REGISTER_LEASE_BMD_SEARCH_LEASEGRANTDATE, "lease_grant_date" },
//		{ ""+LC.REGISTER_LEASE_BMD_SEARCH_LEASEPERIOD, "lease_period" },
//		{ ""+LC.REGISTER_LEASE_BMD_SEARCH_MINETYPES, "mine_types" },
//		{ ""+LC.REGISTER_LEASE_BMD_SEARCH_LOCATIONINFO, "location_info" },
//		{ ""+LC.REGISTER_LEASE_BMD_SEARCH_TOTALAREA, "total_area" },
//		{ ""+LC.REGISTER_LEASE_BMD_SEARCH_ANNUALFEE, "annual_fee" },
//		{ ""+LC.REGISTER_LEASE_BMD_SEARCH_ENTITYRECRITMENTDETAILS, "entity_recritment_details" },
//		{ ""+LC.REGISTER_LEASE_BMD_SEARCH_APPOINTMENTDATE, "appointment_date" },
//		{ ""+LC.REGISTER_LEASE_BMD_SEARCH_EXPIREDATE, "expire_date" },
//		{ ""+LC.REGISTER_LEASE_BMD_SEARCH_COMMENTS, "comments" },
//		{ ""+LC.REGISTER_LEASE_BMD_SEARCH_SECURITYMONEY, "security_money" },
//		{ ""+LC.REGISTER_LEASE_BMD_SEARCH_SECURITYMONEYSTATUS, "security_money_status" },
//		{ ""+LC.REGISTER_LEASE_BMD_SEARCH_RENEWALDATE, "renewal_date" },
//		{ ""+LC.REGISTER_LEASE_BMD_SEARCH_ANYFIELD , "AnyField" }
//	};

    public final static String NAV_REGISTER_LEASE_BMD = "navREGISTER_LEASE_BMD";
    public final static String VIEW_REGISTER_LEASE_BMD = "viewREGISTER_LEASE_BMD";
    public static final String[][] SEARCH_REGISTER_LEASE_BMD = {
            {"" + LC.REGISTER_LEASE_BMD_SEARCH_SERIALNUMBER, "serial_number"},
            {"" + LC.REGISTER_LEASE_BMD_SEARCH_APPLICANTNAME, "applicant_name"},
            {"" + LC.REGISTER_LEASE_BMD_SEARCH_PRESENTADDRESS, "present_address"},
            {"" + LC.REGISTER_LEASE_BMD_SEARCH_PERMANENTADDRESS, "permanent_address"},
            {"" + LC.REGISTER_LEASE_BMD_SEARCH_LEASEGRANTDATE, "lease_grant_date"},
            {"" + LC.REGISTER_LEASE_BMD_SEARCH_LEASEPERIOD, "lease_period"},
            {"" + LC.REGISTER_LEASE_BMD_SEARCH_MINETYPES, "mine_types"},
            {"" + LC.REGISTER_LEASE_BMD_SEARCH_LOCATIONINFO, "location_info"},
            {"" + LC.REGISTER_LEASE_BMD_SEARCH_TOTALAREA, "total_area"},
            {"" + LC.REGISTER_LEASE_BMD_SEARCH_ANNUALFEE, "annual_fee"},
            {"" + LC.REGISTER_LEASE_BMD_SEARCH_ENTITYRECRITMENTDETAILS, "entity_recritment_details"},
            {"" + LC.REGISTER_LEASE_BMD_SEARCH_APPOINTMENTDATE, "appointment_date"},
            {"" + LC.REGISTER_LEASE_BMD_SEARCH_EXPIREDATE, "expire_date"},
            {"" + LC.REGISTER_LEASE_BMD_SEARCH_COMMENTS, "comments"},
            {"" + LC.REGISTER_LEASE_BMD_SEARCH_SECURITYMONEY, "security_money"},
            {"" + LC.REGISTER_LEASE_BMD_SEARCH_SECURITYMONEYSTATUS, "security_money_status"},
            {"" + LC.REGISTER_LEASE_BMD_SEARCH_RENEWALDATE, "renewal_date"},
            {"" + LC.REGISTER_LEASE_BMD_SEARCH_CONVERSIONDATE, "conversion_date"},
            {"" + LC.REGISTER_LEASE_BMD_SEARCH_ANYFIELD, "AnyField"}};

    public final static String NAV_REGISTER_LICENSE_BMD = "navREGISTER_LICENSE_BMD";
    public final static String VIEW_REGISTER_LICENSE_BMD = "viewREGISTER_LICENSE_BMD";
    public static final String[][] SEARCH_REGISTER_LICENSE_BMD = {
            {"" + LC.REGISTER_LICENSE_BMD_SEARCH_SERIALNUMBER, "serial_number"},
            {"" + LC.REGISTER_LICENSE_BMD_SEARCH_APPLICANTNAME, "applicant_name"},
            {"" + LC.REGISTER_LICENSE_BMD_SEARCH_PRESENTADDRESS, "present_address"},
            {"" + LC.REGISTER_LICENSE_BMD_SEARCH_PERMANENTADDRESS, "permanent_address"},
            {"" + LC.REGISTER_LICENSE_BMD_SEARCH_LICENSEGRANTDATE, "license_grant_date"},
            {"" + LC.REGISTER_LICENSE_BMD_SEARCH_LICENSEPERIOD, "license_period"},
            {"" + LC.REGISTER_LICENSE_BMD_SEARCH_MINETYPES, "mine_types"},
            {"" + LC.REGISTER_LICENSE_BMD_SEARCH_LOCATIONINFO, "location_info"},
            {"" + LC.REGISTER_LICENSE_BMD_SEARCH_TOTALAREA, "total_area"},
            {"" + LC.REGISTER_LICENSE_BMD_SEARCH_ANNUALFEE, "annual_fee"},
            {"" + LC.REGISTER_LICENSE_BMD_SEARCH_ENTITYRECRITMENTDETAILS, "entity_recritment_details"},
            {"" + LC.REGISTER_LICENSE_BMD_SEARCH_APPOINTMENTDATE, "appointment_date"},
            {"" + LC.REGISTER_LICENSE_BMD_SEARCH_EXPIREDATE, "expire_date"},
            {"" + LC.REGISTER_LICENSE_BMD_SEARCH_COMMENTS, "comments"},
            {"" + LC.REGISTER_LICENSE_BMD_SEARCH_SECURITYMONEY, "security_money"},
            {"" + LC.REGISTER_LICENSE_BMD_SEARCH_SECURITYMONEYSTATUS, "security_money_status"},
            {"" + LC.REGISTER_LICENSE_BMD_SEARCH_RENEWALDATE, "renewal_date"},
            {"" + LC.REGISTER_LICENSE_BMD_SEARCH_ANYFIELD, "AnyField"}};

    public final static String NAV_VALUE_FIXATION_REPORT = "navVALUE_FIXATION_REPORT";
    public final static String VIEW_VALUE_FIXATION_REPORT = "viewVALUE_FIXATION_REPORT";
    public static final String[][] SEARCH_VALUE_FIXATION_REPORT = {
            {"" + LC.VALUE_FIXATION_REPORT_SEARCH_DESCRIPTION, "description"},
            {"" + LC.VALUE_FIXATION_REPORT_SEARCH_SERVICEID, "service_id"},
            {"" + LC.VALUE_FIXATION_REPORT_SEARCH_SERVICETYPES, "service_types"},
            {"" + LC.VALUE_FIXATION_REPORT_SEARCH_PRICE, "price"},
            {"" + LC.VALUE_FIXATION_REPORT_SEARCH_QUANTITY, "quantity"},
            {"" + LC.VALUE_FIXATION_REPORT_SEARCH_ANYFIELD, "AnyField"}};

//	public final static String NAV_INBOX_ACTIONS = "navINBOX_ACTIONS";
//	public final static String VIEW_INBOX_ACTIONS = "viewINBOX_ACTIONS";
//	public static final String[][] SEARCH_INBOX_ACTIONS = {
//		{ ""+LC.INBOX_ACTIONS_SEARCH_ACTIONLANGUAGEID, "action_language_id" },
//		{ ""+LC.INBOX_ACTIONS_SEARCH_LINK, "link" },
//		{ ""+LC.INBOX_ACTIONS_SEARCH_ICONLINK, "icon_link" },
//		{ ""+LC.INBOX_ACTIONS_SEARCH_STATUS, "status" },
//		{ ""+LC.INBOX_ACTIONS_SEARCH_ANYFIELD , "AnyField" }
//	};

//	public final static String NAV_INBOX_ACTION_ROLES = "navINBOX_ACTION_ROLES";
//	public final static String VIEW_INBOX_ACTION_ROLES = "viewINBOX_ACTION_ROLES";
//	public static final String[][] SEARCH_INBOX_ACTION_ROLES = {
//		{ ""+LC.INBOX_ACTION_ROLES_SEARCH_INBOXSTATUSID, "inbox_status_id" },
//		{ ""+LC.INBOX_ACTION_ROLES_SEARCH_ROLEID, "role_id" },
//		{ ""+LC.INBOX_ACTION_ROLES_SEARCH_ACTIONID, "action_id" },
//		{ ""+LC.INBOX_ACTION_ROLES_SEARCH_ANYFIELD , "AnyField" }
//	};

//	public final static String NAV_INBOX_NOTIFICATION_TEMPLATE = "navINBOX_NOTIFICATION_TEMPLATE";
//	public final static String VIEW_INBOX_NOTIFICATION_TEMPLATE = "viewINBOX_NOTIFICATION_TEMPLATE";
//	public static final String[][] SEARCH_INBOX_NOTIFICATION_TEMPLATE = {
//		{ ""+LC.INBOX_NOTIFICATION_TEMPLATE_SEARCH_INBOXACTIONROLEID, "inbox_action_role_id" },
//		{ ""+LC.INBOX_NOTIFICATION_TEMPLATE_SEARCH_TEMPLATENAME, "template_name" },
//		{ ""+LC.INBOX_NOTIFICATION_TEMPLATE_SEARCH_SUBJECTLANGUAGEID, "subject_language_id" },
//		{ ""+LC.INBOX_NOTIFICATION_TEMPLATE_SEARCH_BODYEN, "body_en" },
//		{ ""+LC.INBOX_NOTIFICATION_TEMPLATE_SEARCH_BODYBN, "body_bn" },
//		{ ""+LC.INBOX_NOTIFICATION_TEMPLATE_SEARCH_STATUS, "status" },
//		{ ""+LC.INBOX_NOTIFICATION_TEMPLATE_SEARCH_INBOXNOTIFICATIONTYPES, "inbox_notification_types" },
//		{ ""+LC.INBOX_NOTIFICATION_TEMPLATE_SEARCH_ANYFIELD , "AnyField" }
//	};

//	public final static String NAV_INBOX_NOTIFICATION_TYPE = "navINBOX_NOTIFICATION_TYPE";
//	public final static String VIEW_INBOX_NOTIFICATION_TYPE = "viewINBOX_NOTIFICATION_TYPE";
//	public static final String[][] SEARCH_INBOX_NOTIFICATION_TYPE = {
//		{ ""+LC.INBOX_NOTIFICATION_TYPE_SEARCH_DESCRIPTIONLANGUAGEID, "description_language_id" },
//		{ ""+LC.INBOX_NOTIFICATION_TYPE_SEARCH_ANYFIELD , "AnyField" }
//	};

//	public final static String NAV_INBOX_ORIGINATOR = "navINBOX_ORIGINATOR";
//	public final static String VIEW_INBOX_ORIGINATOR = "viewINBOX_ORIGINATOR";
//	public static final String[][] SEARCH_INBOX_ORIGINATOR = {
//		{ ""+LC.INBOX_ORIGINATOR_SEARCH_BSADID, "bsad_id" },
//		{ ""+LC.INBOX_ORIGINATOR_SEARCH_ANYFIELD , "AnyField" }
//	};

//	public final static String NAV_INBOX_ROLE_ORGANOGRAM = "navINBOX_ROLE_ORGANOGRAM";
//	public final static String VIEW_INBOX_ROLE_ORGANOGRAM = "viewINBOX_ROLE_ORGANOGRAM";
//	public static final String[][] SEARCH_INBOX_ROLE_ORGANOGRAM = {
//		{ ""+LC.INBOX_ROLE_ORGANOGRAM_SEARCH_ROLEID, "role_id" },
//		{ ""+LC.INBOX_ROLE_ORGANOGRAM_SEARCH_ORGANOGRAMID, "organogram_id" },
//		{ ""+LC.INBOX_ROLE_ORGANOGRAM_SEARCH_ANYFIELD , "AnyField" }
//	};

//	public final static String NAV_INBOX_STATUS = "navINBOX_STATUS";
//	public final static String VIEW_INBOX_STATUS = "viewINBOX_STATUS";
//	public static final String[][] SEARCH_INBOX_STATUS = {
//		{ ""+LC.INBOX_STATUS_SEARCH_STATUSLANGUAGEID, "status_language_id" },
//		{ ""+LC.INBOX_STATUS_SEARCH_ANYFIELD , "AnyField" }
//	};

//	public final static String NAV_INBOX_TAB = "navINBOX_TAB";
//	public final static String VIEW_INBOX_TAB = "viewINBOX_TAB";
//	public static final String[][] SEARCH_INBOX_TAB = {
//		{ ""+LC.INBOX_TAB_SEARCH_NAMELANGUAGEID, "name_language_id" },
//		{ ""+LC.INBOX_TAB_SEARCH_QUERYTAB, "query_tab" },
//		{ ""+LC.INBOX_TAB_SEARCH_TABTABLEHEADER, "tab_table_header" },
//		{ ""+LC.INBOX_TAB_SEARCH_DETAILSLINK, "details_link" },
//		{ ""+LC.INBOX_TAB_SEARCH_QUERYDETAILS, "query_details" },
//		{ ""+LC.INBOX_TAB_SEARCH_STATUS, "status" },
//		{ ""+LC.INBOX_TAB_SEARCH_ACTIONCOLUMN, "action_column" },
//		{ ""+LC.INBOX_TAB_SEARCH_ANYFIELD , "AnyField" }
//	};

//	public final static String NAV_INBOX_TAB_COLUMN = "navINBOX_TAB_COLUMN";
//	public final static String VIEW_INBOX_TAB_COLUMN = "viewINBOX_TAB_COLUMN";
//	public static final String[][] SEARCH_INBOX_TAB_COLUMN = {
//		{ ""+LC.INBOX_TAB_COLUMN_SEARCH_INBOXTABID, "inbox_tab_id" },
//		{ ""+LC.INBOX_TAB_COLUMN_SEARCH_ISSORTED, "isSorted" },
//		{ ""+LC.INBOX_TAB_COLUMN_SEARCH_VISIBILITY, "visibility" },
//		{ ""+LC.INBOX_TAB_COLUMN_SEARCH_HASHYPERLINK, "hasHyperlink" },
//		{ ""+LC.INBOX_TAB_COLUMN_SEARCH_ANYFIELD , "AnyField" }
//	};

//	public final static String NAV_CATEGORY = "navCATEGORY";
//	public final static String VIEW_CATEGORY = "viewCATEGORY";
//	public static final String[][] SEARCH_CATEGORY = {
//		{ ""+LC.CATEGORY_SEARCH_DOMAINNAME, "domain_name" },
//		{ ""+LC.CATEGORY_SEARCH_LANGUAGEID, "language_id" },
//		{ ""+LC.CATEGORY_SEARCH_ANYFIELD , "AnyField" }
//	};

//	public final static String NAV_USER_DETAILS = "navUSER_DETAILS";
//	public final static String VIEW_USER_DETAILS = "viewUSER_DETAILS";
//	public static final String[][] SEARCH_USER_DETAILS = {
//		{ ""+LC.USER_DETAILS_SEARCH_PRESENTADDRESS, "present_address" },
//		{ ""+LC.USER_DETAILS_SEARCH_PERMANENTADDRESS, "permanent_address" },
//		{ ""+LC.USER_DETAILS_SEARCH_ANYFIELD , "AnyField" }
//	};

    public final static String NAV_USER_DETAILS = "navUSER_DETAILS";
    public final static String VIEW_USER_DETAILS = "viewUSER_DETAILS";
    public static final String[][] SEARCH_USER_DETAILS = {
            {"" + LC.USER_DETAILS_SEARCH_PRESENTADDRESS, "present_address"},
            {"" + LC.USER_DETAILS_SEARCH_PERMANENTADDRESS, "permanent_address"},
            {"" + LC.USER_DETAILS_SEARCH_ANYFIELD, "AnyField"}};

    public final static String NAV_INBOX = "navINBOX";
    public final static String VIEW_INBOX = "viewINBOX";
    public static final String[][] SEARCH_INBOX = {};

    public final static String NAV_ROOTSTOCK_FILES = "navROOTSTOCK_FILES";
    public final static String VIEW_ROOTSTOCK_FILES = "viewROOTSTOCK_FILES";
    public static final String[][] SEARCH_ROOTSTOCK_FILES = {{"" + LC.ROOTSTOCK_FILES_SEARCH_FILEID, "file_id"},
            {"" + LC.ROOTSTOCK_FILES_SEARCH_FILETYPES, "file_types"},
            {"" + LC.ROOTSTOCK_FILES_SEARCH_FILEBLOB, "file_blob"},
            {"" + LC.ROOTSTOCK_FILES_SEARCH_FILETAG, "file_tag"},
            {"" + LC.ROOTSTOCK_FILES_SEARCH_FILETITLE, "file_title"},
            {"" + LC.ROOTSTOCK_FILES_SEARCH_USERID, "user_id"}, {"" + LC.ROOTSTOCK_FILES_SEARCH_SOURCE, "source"},
            {"" + LC.ROOTSTOCK_FILES_SEARCH_CREATEDAT, "created_at"},
            {"" + LC.ROOTSTOCK_FILES_SEARCH_ANYFIELD, "AnyField"}};

//	public final static String NAV_CATEGORY = "navCATEGORY";
//	public final static String VIEW_CATEGORY = "viewCATEGORY";
//	public static final String[][] SEARCH_CATEGORY = {
//		{ ""+LC.CATEGORY_SEARCH_DOMAINNAME, "domain_name" },
//		{ ""+LC.CATEGORY_SEARCH_LANGUAGEID, "language_id" },
//		{ ""+LC.CATEGORY_SEARCH_VALUE, "value" },
//		{ ""+LC.CATEGORY_SEARCH_ANYFIELD , "AnyField" }
//	};

    public final static String NAV_CATEGORY = "navCATEGORY";
    public final static String VIEW_CATEGORY = "viewCATEGORY";
    public static final String[][] SEARCH_CATEGORY = {{"" + LC.CATEGORY_SEARCH_DOMAINNAME, "domain_name"},
            {"" + LC.CATEGORY_SEARCH_LANGUAGEID, "language_id"}, {"" + LC.CATEGORY_SEARCH_VALUE, "value"},
            {"" + LC.CATEGORY_SEARCH_ANYFIELD, "AnyField"}};

    public final static String NAV_FILES = "navFILES";
    public final static String VIEW_FILES = "viewFILES";
    public static final String[][] SEARCH_FILES = {{"" + LC.FILES_SEARCH_FILEID, "file_id"},
            {"" + LC.FILES_SEARCH_FILETYPES, "file_types"}, {"" + LC.FILES_SEARCH_FILETAG, "file_tag"},
            {"" + LC.FILES_SEARCH_FILETITLE, "file_title"}, {"" + LC.FILES_SEARCH_USERID, "user_id"},
            {"" + LC.FILES_SEARCH_SOURCE, "source"}, {"" + LC.FILES_SEARCH_CREATEDAT, "created_at"},
            {"" + LC.FILES_SEARCH_ANYFIELD, "AnyField"}};

    public final static String NAV_APPROVAL_PATH_DETAILS = "navAPPROVAL_PATH_DETAILS";
    public final static String VIEW_APPROVAL_PATH_DETAILS = "viewAPPROVAL_PATH_DETAILS";
    public static final String[][] SEARCH_APPROVAL_PATH_DETAILS = {
            {"" + LC.APPROVAL_PATH_DETAILS_SEARCH_APPROVALPATHID, "approval_path_id"},
            {"" + LC.APPROVAL_PATH_DETAILS_SEARCH_APPROVALROLECAT, "approval_role_cat"},
            {"" + LC.APPROVAL_PATH_DETAILS_SEARCH_APPROVALORDER, "approval_order"},
            {"" + LC.APPROVAL_PATH_DETAILS_SEARCH_ORGANOGRAMID, "organogram_id"},
            {"" + LC.APPROVAL_PATH_DETAILS_SEARCH_ANYFIELD, "AnyField"}};


    public final static String NAV_T1_JOBLESS = "navT1_JOBLESS";
    public final static String VIEW_T1_JOBLESS = "viewT1_JOBLESS";
    public static final String[][] SEARCH_T1_JOBLESS = {{"" + LC.T1_JOBLESS_SEARCH_DESCRIPTION, "description"},
            {"" + LC.T1_JOBLESS_SEARCH_SUPERPOSITION, "superposition"},
            {"" + LC.T1_JOBLESS_SEARCH_ADDRESS, "address"},
            {"" + LC.T1_JOBLESS_SEARCH_DATEOFBIRTH, "date_of_birth"},
            {"" + LC.T1_JOBLESS_SEARCH_ANYFIELD, "AnyField"}};

    public final static String NAV_T2_JOB = "navT2_JOB";
    public final static String VIEW_T2_JOB = "viewT2_JOB";
    public static final String[][] SEARCH_T2_JOB = {{"" + LC.T2_JOB_SEARCH_DESCRIPTION, "description"},
            {"" + LC.T2_JOB_SEARCH_SUPERPOSITION, "superposition"}, {"" + LC.T2_JOB_SEARCH_ADDRESS, "address"},
            {"" + LC.T2_JOB_SEARCH_DATEOFBIRTH, "date_of_birth"}, {"" + LC.T2_JOB_SEARCH_JOBCAT, "job_cat"},
            {"" + LC.T2_JOB_SEARCH_ANYFIELD, "AnyField"}};

    public final static String NAV_FILE_INDEX_TYPE = "navFILE_INDEX_TYPE";
    public final static String VIEW_FILE_INDEX_TYPE = "viewFILE_INDEX_TYPE";
    public static final String[][] SEARCH_FILE_INDEX_TYPE = {{"" + LC.FILE_INDEX_TYPE_SEARCH_NAMEEN, "name_en"},
            {"" + LC.FILE_INDEX_TYPE_SEARCH_NAMEBN, "name_bn"},
            {"" + LC.FILE_INDEX_TYPE_SEARCH_ANYFIELD, "AnyField"}};

    public final static String NAV_EDTEST = "navEDTEST";
    public final static String VIEW_EDTEST = "viewEDTEST";
    public static final String[][] SEARCH_EDTEST = {{"" + LC.EDTEST_SEARCH_NAMEEN, "name_en"},
            {"" + LC.EDTEST_SEARCH_NAMEBN, "name_bn"}, {"" + LC.EDTEST_SEARCH_ANYFIELD, "AnyField"}};

    public final static String NAV_PBRLP_CONTRACTOR = "navPBRLP_CONTRACTOR";
    public final static String VIEW_PBRLP_CONTRACTOR = "viewPBRLP_CONTRACTOR";
    public static final String[][] SEARCH_PBRLP_CONTRACTOR = {{"" + LC.PBRLP_CONTRACTOR_SEARCH_NAMEEN, "name_en"},
            {"" + LC.PBRLP_CONTRACTOR_SEARCH_NAMEBN, "name_bn"},
            {"" + LC.PBRLP_CONTRACTOR_SEARCH_ADDRESS, "address"},
            {"" + LC.PBRLP_CONTRACTOR_SEARCH_DESCRIPTION, "description"},
            {"" + LC.PBRLP_CONTRACTOR_SEARCH_CONTRACTNO, "contract_no"},
            {"" + LC.PBRLP_CONTRACTOR_SEARCH_ANYFIELD, "AnyField"}};

    public final static String NAV_DROPZONE_TEST = "navDROPZONE_TEST";
    public final static String VIEW_DROPZONE_TEST = "viewDROPZONE_TEST";
    public static final String[][] SEARCH_DROPZONE_TEST = {{"" + LC.DROPZONE_TEST_SEARCH_DESCRIPTION, "description"},
            {"" + LC.DROPZONE_TEST_SEARCH_ANYFIELD, "AnyField"}};

    public final static String NAV_EDMS_DOCUMENTS = "navEDMS_DOCUMENTS";
    public final static String VIEW_EDMS_DOCUMENTS = "viewEDMS_DOCUMENTS";
    public static final String[][] SEARCH_EDMS_DOCUMENTS = {
            {"" + LC.EDMS_DOCUMENTS_SEARCH_DOCUMENTPROCESSCAT, "document_process_cat"},
            {"" + LC.EDMS_DOCUMENTS_SEARCH_DOCUMENTNO, "document_no"},
            {"" + LC.EDMS_DOCUMENTS_SEARCH_PBRLPCONTRACTORTYPE, "pbrlp_contractor_type"},
            {"" + LC.EDMS_DOCUMENTS_SEARCH_FILEINDEXTYPETYPE, "file_index_type_type"},
            {"" + LC.EDMS_DOCUMENTS_SEARCH_FILEINDEXSUBTYPETYPE, "file_index_subtype_type"},
            {"" + LC.EDMS_DOCUMENTS_SEARCH_OFFICESTYPE, "offices_type"},
            {"" + LC.EDMS_DOCUMENTS_SEARCH_DOCUMENTTYPECAT, "document_type_cat"},
            {"" + LC.EDMS_DOCUMENTS_SEARCH_DOCUMENTDATE, "document_date"},
            {"" + LC.EDMS_DOCUMENTS_SEARCH_DOCUMENTRECEIVEDATE, "document_receive_date"},
            {"" + LC.EDMS_DOCUMENTS_SEARCH_REFERENCENO, "reference_no"},
            {"" + LC.EDMS_DOCUMENTS_SEARCH_DOCUMENTDATE, "document_date_from"},
            {"" + LC.EDMS_DOCUMENTS_SEARCH_DOCUMENTDATE, "document_date_to"},
            {"" + LC.EDMS_DOCUMENTS_SEARCH_ANYFIELD, "AnyField"}};

    public final static String NAV_APPROVAL_EXECUTION_TABLE = "navAPPROVAL_EXECUTION_TABLE";
    public final static String VIEW_APPROVAL_EXECUTION_TABLE = "viewAPPROVAL_EXECUTION_TABLE";
    public static final String[][] SEARCH_APPROVAL_EXECUTION_TABLE = {
            {"" + LC.APPROVAL_EXECUTION_TABLE_SEARCH_TABLENAME, "table_name"},
            {"" + LC.APPROVAL_EXECUTION_TABLE_SEARCH_PREVIOUSROWID, "previous_row_id"},
            {"" + LC.APPROVAL_EXECUTION_TABLE_SEARCH_UPDATEDROWID, "updated_row_id"},
            {"" + LC.APPROVAL_EXECUTION_TABLE_SEARCH_OPERATION, "operation"},
            {"" + LC.APPROVAL_EXECUTION_TABLE_SEARCH_ACTION, "action"},
            {"" + LC.APPROVAL_EXECUTION_TABLE_SEARCH_APPROVALSTATUSCAT, "approval_status_cat"},
            {"" + LC.APPROVAL_EXECUTION_TABLE_SEARCH_USERID, "user_id"},
            {"" + LC.APPROVAL_EXECUTION_TABLE_SEARCH_USERIP, "user_ip"},
            {"" + LC.APPROVAL_EXECUTION_TABLE_SEARCH_APPROVALPATHID, "approval_path_id"},
            {"" + LC.APPROVAL_EXECUTION_TABLE_SEARCH_APPROVALPATHORDER, "approval_path_order"},
            {"" + LC.APPROVAL_EXECUTION_TABLE_SEARCH_REMARKS, "remarks"},
            {"" + LC.APPROVAL_EXECUTION_TABLE_SEARCH_ANYFIELD, "AnyField"}};

    public final static String NAV_APPROVAL_NOTIFICATION_RECIPIENTS = "navAPPROVAL_NOTIFICATION_RECIPIENTS";
    public final static String VIEW_APPROVAL_NOTIFICATION_RECIPIENTS = "viewAPPROVAL_NOTIFICATION_RECIPIENTS";
    public static final String[][] SEARCH_APPROVAL_NOTIFICATION_RECIPIENTS = {
            {"" + LC.APPROVAL_NOTIFICATION_RECIPIENTS_SEARCH_APPROVALPATHID, "approval_path_id"},
            {"" + LC.APPROVAL_NOTIFICATION_RECIPIENTS_SEARCH_ORGANOGRAMID, "organogram_id"},
            {"" + LC.APPROVAL_NOTIFICATION_RECIPIENTS_SEARCH_ANYFIELD, "AnyField"}};


    public final static String NAV_APPROVAL_SUMMARY = "navAPPROVAL_SUMMARY";
    public final static String VIEW_APPROVAL_SUMMARY = "viewAPPROVAL_SUMMARY";
    public static final String[][] SEARCH_APPROVAL_SUMMARY = {
            {"" + LC.APPROVAL_SUMMARY_SEARCH_TABLENAME, "table_name"},
            {"" + LC.APPROVAL_SUMMARY_SEARCH_TABLEID, "table_id"},
            {"" + LC.APPROVAL_SUMMARY_SEARCH_INITIATOR, "initiator"},
            {"" + LC.APPROVAL_SUMMARY_SEARCH_APPROVALSTATUSCAT, "approval_status_cat"},
            {"" + LC.APPROVAL_SUMMARY_SEARCH_DATEOFINITIATION, "date_of_initiation"},
            {"" + LC.APPROVAL_SUMMARY_SEARCH_DATEOFRESOLUTION, "date_of_resolution"},
            {"" + LC.APPROVAL_SUMMARY_SEARCH_RESOLVER, "resolver"},
            {"" + LC.APPROVAL_SUMMARY_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_HOLIDAYS = "navHOLIDAYS";
    public final static String VIEW_HOLIDAYS = "viewHOLIDAYS";
    public static final String[][] SEARCH_HOLIDAYS = {
            {"" + LC.HOLIDAYS_SEARCH_HOLIDAYDATE, "holiday_date"},
            {"" + LC.HOLIDAYS_SEARCH_YEAR, "year"},
            {"" + LC.HOLIDAYS_SEARCH_DESCRIPTION, "description"},
            {"" + LC.HOLIDAYS_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_DOC_DRAFT = "navDOC_DRAFT";
    public final static String VIEW_DOC_DRAFT = "viewDOC_DRAFT";
    public static final String[][] SEARCH_DOC_DRAFT = {
            {"" + LC.DOC_DRAFT_SEARCH_REFERENCENO, "reference_no"},
            {"" + LC.DOC_DRAFT_SEARCH_DOCUMENTDATE, "document_date"},
            {"" + LC.DOC_DRAFT_SEARCH_REFERENCETEXT, "reference_text"},
            {"" + LC.DOC_DRAFT_SEARCH_DOCUMENTSUBJECT, "document_subject"},
            {"" + LC.DOC_DRAFT_SEARCH_FILEINDEXTYPETYPE, "file_index_type_type"},
            {"" + LC.DOC_DRAFT_SEARCH_FILEINDEXSUBTYPETYPE, "file_index_subtype_type"},
            {"" + LC.DOC_DRAFT_SEARCH_REPLYREQUIRED, "reply_required"},
            {"" + LC.DOC_DRAFT_SEARCH_DOCDISTGROUPTYPE, "doc_dist_group_type"},
            {"" + LC.DOC_DRAFT_SEARCH_USERID, "user_id"},
            {"" + LC.DOC_DRAFT_SEARCH_DOCUMENTSTATUSCAT, "document_status_cat"},
            {"" + LC.DOC_DRAFT_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_APPROVAL_TEST = "navAPPROVAL_TEST";
    public final static String VIEW_APPROVAL_TEST = "viewAPPROVAL_TEST";
    public static final String[][] SEARCH_APPROVAL_TEST = {
            {"" + LC.APPROVAL_TEST_SEARCH_DESCRIPTION, "description"},
            {"" + LC.APPROVAL_TEST_SEARCH_JOBCAT, "job_cat"},
            {"" + LC.APPROVAL_TEST_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_EMPLOYEE_RECORDS = "navEMPLOYEE_RECORDS";
    public final static String VIEW_EMPLOYEE_RECORDS = "viewEMPLOYEE_RECORDS";
    public static final String[][] SEARCH_EMPLOYEE_RECORDS = {
            {"" + LC.EMPLOYEE_RECORDS_SEARCH_NAMEENG, "name_eng"},
            {"" + LC.EMPLOYEE_RECORDS_SEARCH_NAMEBNG, "name_bng"},
            {"" + LC.EMPLOYEE_RECORDS_SEARCH_NID, "nid"},
            {"" + LC.EMPLOYEE_RECORDS_SEARCH_PERSONALEMAIL, "personal_email"},
            {"" + LC.EMPLOYEE_RECORDS_SEARCH_PERSONALMOBILE, "personal_mobile"},
            {"" + LC.EMPLOYEE_RECORDS_SEARCH_ALTERNATIVEMOBILE, "alternative_mobile"},
            {"" + LC.EMPLOYEE_RECORDS_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_EMPLOYEE_PENSION = "navEMPLOYEE_PENSION";
    public final static String VIEW_EMPLOYEE_PENSION = "viewEMPLOYEE_PENSION";
    public static final String[][] SEARCH_EMPLOYEE_PENSION = {
            {"" + LC.EMPLOYEE_RECORDS_SEARCH_NAMEENG, "name_eng"},
            {"" + LC.EMPLOYEE_RECORDS_SEARCH_NAMEBNG, "name_bng"},
            {"" + LC.EMPLOYEE_RECORDS_SEARCH_NID, "nid"},
            {"" + LC.EMPLOYEE_RECORDS_SEARCH_PERSONALEMAIL, "personal_email"},
            {"" + LC.EMPLOYEE_RECORDS_SEARCH_PERSONALMOBILE, "personal_mobile"},
            {"" + LC.EMPLOYEE_RECORDS_SEARCH_ALTERNATIVEMOBILE, "alternative_mobile"},
            {"" + LC.EMPLOYEE_RECORDS_SEARCH_ANYFIELD, "AnyField"}
    };


    //search quiry related


    public final static String NAV_GEO_UNIONS = "navGEO_UNIONS";
    public final static String VIEW_GEO_UNIONS = "viewGEO_UNIONS";
    public static final String[][] SEARCH_GEO_UNIONS = {
            {"" + LC.GEO_UNIONS_SEARCH_GEODIVISIONID, "geo_division_id"},
            {"" + LC.GEO_UNIONS_SEARCH_GEODISTRICTID, "geo_district_id"},
            {"" + LC.GEO_UNIONS_SEARCH_GEOUPAZILAID, "geo_upazila_id"},
            {"" + LC.GEO_UNIONS_SEARCH_UNIONNAMEENG, "union_name_eng"},
            {"" + LC.GEO_UNIONS_SEARCH_UNIONNAMEBNG, "union_name_bng"},
            {"" + LC.GEO_UNIONS_SEARCH_BBSCODE, "bbs_code"},
            {"" + LC.GEO_UNIONS_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAVIGATION_BAR_PAGE_NUMBER = "pageNumber";


    public final static int APPROVER = 0;
    public final static int UNKNOWN_ROLE = -1;


    public final static String NAV_PUPPIES = "navPUPPIES";
    public final static String VIEW_PUPPIES = "viewPUPPIES";
    public static final String[][] SEARCH_PUPPIES = {
            {"Name", "Name"},
            {"Age", "Age"},
            {"Address", "Address"},
            {"Gender", "Gender"},
            {"Color", "Color"},
            {"IsWellBehaved", "IsWellBehaved"},
            {"IsDeleted", "isDeleted"}
    };


    public final static String NAV_CITIZEN = "navCITIZEN";
    public final static String VIEW_CITIZEN = "viewCITIZEN";
    public static final String[][] SEARCH_CITIZEN = {
            {"" + LC.CITIZEN_SEARCH_NID, "nid"},
            {"" + LC.CITIZEN_SEARCH_NAME, "name"},
            {"" + LC.CITIZEN_SEARCH_PHONENUMBER, "phone_number"},
            {"" + LC.CITIZEN_SEARCH_MAILID, "mail_id"},
            {"" + LC.CITIZEN_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_REPORT = "navREPORT";
    public final static String VIEW_REPORT = "viewREPORT";
    public static final String[][] SEARCH_REPORT = {
            {"" + LC.REPORT_SEARCH_NAME, "name"},
            {"" + LC.REPORT_SEARCH_NID, "nid"},
            {"" + LC.REPORT_SEARCH_PHONENUMBER, "phone_number"},
            {"" + LC.REPORT_SEARCH_MAILID, "mail_id"},
            {"" + LC.REPORT_SEARCH_TYPEID, "type_id"},
            {"" + LC.REPORT_SEARCH_MODELNAME, "model_name"},
            {"" + LC.REPORT_SEARCH_COLOR, "color"},
            {"" + LC.REPORT_SEARCH_ENGINENUMBER, "engine_number"},
            {"" + LC.REPORT_SEARCH_ENGINETYPE, "engine_type"},
            {"" + LC.REPORT_SEARCH_CHASSISNUMBER, "chassis_number"},
            {"" + LC.REPORT_SEARCH_ENGINECC, "engine_cc"},
            {"" + LC.REPORT_SEARCH_REGISTRATIONNUMBER, "registration_number"},
            {"" + LC.REPORT_SEARCH_MANUFACTURER, "manufacturer"},
            {"" + LC.REPORT_SEARCH_MANUFACTURINGYEAR, "manufacturing_year"},
            {"" + LC.REPORT_SEARCH_MOREDETAILS, "more_details"},
            {"" + LC.REPORT_SEARCH_REPORTINGDATE, "reporting_date"},
            {"" + LC.REPORT_SEARCH_REPORTERID, "reporter_id"},
            {"" + LC.REPORT_SEARCH_VEHICLEID, "vehicle_id"},
            {"" + LC.REPORT_SEARCH_LOSTDATE, "lost_date"},
            {"" + LC.REPORT_SEARCH_FOUNDDATE, "found_date"},
            {"" + LC.REPORT_SEARCH_BLOG, "blog"},
            {"" + LC.REPORT_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_STATUS = "navSTATUS";
    public final static String VIEW_STATUS = "viewSTATUS";
    public static final String[][] SEARCH_STATUS = {
            {"" + LC.STATUS_SEARCH_NAMEEN, "name_en"},
            {"" + LC.STATUS_SEARCH_NAMEBN, "name_bn"},
            {"" + LC.STATUS_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_COLOR = "navCOLOR";
    public final static String VIEW_COLOR = "viewCOLOR";
    public static final String[][] SEARCH_COLOR = {
            {"" + LC.COLOR_SEARCH_NAMEEN, "name_en"},
            {"" + LC.COLOR_SEARCH_NAMEBN, "name_bn"},
            {"" + LC.COLOR_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_STICK = "navSTICK";
    public final static String VIEW_STICK = "viewSTICK";
    public static final String[][] SEARCH_STICK = {
            {"" + LC.STICK_SEARCH_NAMEEN, "name_en"},
            {"" + LC.STICK_SEARCH_NAMEBN, "name_bn"},
            {"" + LC.STICK_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_APPROVAL_STATUS = "navAPPROVAL_STATUS";
    public final static String VIEW_APPROVAL_STATUS = "viewAPPROVAL_STATUS";
    public static final String[][] SEARCH_APPROVAL_STATUS = {
            {"" + LC.APPROVAL_STATUS_SEARCH_NAMEEN, "name_en"},
            {"" + LC.APPROVAL_STATUS_SEARCH_NAMEBN, "name_bn"},
            {"" + LC.APPROVAL_STATUS_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_EMPLOYEE_HISTORY = "navEMPLOYEE_HISTORY";
    public final static String VIEW_EMPLOYEE_HISTORY = "viewEMPLOYEE_HISTORY";
    public static final String[][] SEARCH_EMPLOYEE_HISTORY = {
            {"" + LC.EMPLOYEE_HISTORY_SEARCH_USERNAME, "username"},
            {"" + LC.EMPLOYEE_HISTORY_SEARCH_NAMEENG, "name_eng"},
            {"" + LC.EMPLOYEE_HISTORY_SEARCH_NAMEBNG, "name_bng"},
            {"" + LC.EMPLOYEE_HISTORY_SEARCH_PERSONALEMAIL, "personal_email"},
            {"" + LC.EMPLOYEE_HISTORY_SEARCH_PERSONALMOBILE, "personal_mobile"},
            {"" + LC.EMPLOYEE_HISTORY_SEARCH_DESIGNATION, "designation"},
            {"" + LC.EMPLOYEE_HISTORY_SEARCH_UNITNAMEENG, "unit_name_eng"},
            {"" + LC.EMPLOYEE_HISTORY_SEARCH_UNITNAMEBNG, "unit_name_bng"},
            {"" + LC.EMPLOYEE_HISTORY_SEARCH_OFFICENAMEENG, "office_name_eng"},
            {"" + LC.EMPLOYEE_HISTORY_SEARCH_OFFICENAMEBNG, "office_name_bng"},
            {"" + LC.EMPLOYEE_HISTORY_SEARCH_JOININGDATE, "joining_date"},
            {"" + LC.EMPLOYEE_HISTORY_SEARCH_LASTOFFICEDATE, "last_office_date"},
            {"" + LC.EMPLOYEE_HISTORY_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_PROTIKOLPO_LOG = "navPROTIKOLPO_LOG";
    public final static String VIEW_PROTIKOLPO_LOG = "viewPROTIKOLPO_LOG";
    public static final String[][] SEARCH_PROTIKOLPO_LOG = {
            {"" + LC.PROTIKOLPO_LOG_SEARCH_PROTIKOLPOSTARTDATE, "protikolpo_start_date"},
            {"" + LC.PROTIKOLPO_LOG_SEARCH_PROTIKOLPOENDDATE, "protikolpo_end_date"},
            {"" + LC.PROTIKOLPO_LOG_SEARCH_PROTIKOLPOENDEDBY, "protikolpo_ended_by"},
            {"" + LC.PROTIKOLPO_LOG_SEARCH_EMPLOYEEOFFICEIDFROMNAME, "employee_office_id_from_name"},
            {"" + LC.PROTIKOLPO_LOG_SEARCH_EMPLOYEEOFFICEIDTONAME, "employee_office_id_to_name"},
            {"" + LC.PROTIKOLPO_LOG_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_LEAVE = "navLEAVE";
    public final static String VIEW_LEAVE = "viewLEAVE";
    public static final String[][] SEARCH_LEAVE = {
            {"" + LC.LEAVE_SEARCH_EMPLOYEEINLEAVETYPE, "employee_in_leave_type"},
            {"" + LC.LEAVE_SEARCH_EMPLOYEEINLEAVEORGANOGRAMID, "employee_in_leave_organogram_id"},
            {"" + LC.LEAVE_SEARCH_EMPLOYEEINLEAVENAME, "employee_in_leave_name"},
            {"" + LC.LEAVE_SEARCH_EMPLOYEEINLEAVEDESIGNATION, "employee_in_leave_designation"},
            {"" + LC.LEAVE_SEARCH_EMPLOYEEINLEAVEOFFICEUNITID, "employee_in_leave_office_unit_id"},
            {"" + LC.LEAVE_SEARCH_EMPLOYEEINLEAVEOFFICEUNITNAME, "employee_in_leave_office_unit_name"},
            {"" + LC.LEAVE_SEARCH_EMPLOYEEINCHARGETYPE, "employee_in_charge_type"},
            {"" + LC.LEAVE_SEARCH_EMPLOYEEINCHARGEORGANOGRAMID, "employee_in_charge_organogram_id"},
            {"" + LC.LEAVE_SEARCH_EMPLOYEEINCHARGENAME, "employee_in_charge_name"},
            {"" + LC.LEAVE_SEARCH_EMPLOYEEINCHARGEDESIGNATION, "employee_in_charge_designation"},
            {"" + LC.LEAVE_SEARCH_EMPLOYEEINCHARGEOFFICEUNITID, "employee_in_charge_office_unit_id"},
            {"" + LC.LEAVE_SEARCH_EMPLOYEEINCHARGEOFFICEUNITNAME, "employee_in_charge_office_unit_name"},
            {"" + LC.LEAVE_SEARCH_EMPLOYEEINCHARGEPOLICYROLESTYPE, "employee_in_charge_policy_roles_type"},
            {"" + LC.LEAVE_SEARCH_LEAVESTARTDATE, "leave_start_date"},
            {"" + LC.LEAVE_SEARCH_LEAVEENDDATE, "leave_end_date"},
            {"" + LC.LEAVE_SEARCH_LEAVEENTRYDATE, "leave_entry_date"},
            {"" + LC.LEAVE_SEARCH_LEAVESTATUSCAT, "leave_status_cat"},
            {"" + LC.LEAVE_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_SELECT_DEMO = "navSELECT_DEMO";
    public final static String VIEW_SELECT_DEMO = "viewSELECT_DEMO";
    public static final String[][] SEARCH_SELECT_DEMO = {
            {"" + LC.SELECT_DEMO_SEARCH_FILEINDEXTYPETYPE, "file_index_type_type"},
            {"" + LC.SELECT_DEMO_SEARCH_FILEINDEXSUBTYPETYPE, "file_index_subtype_type"},
            {"" + LC.SELECT_DEMO_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_QSDR_TYPE = "navQSDR_TYPE";
    public final static String VIEW_QSDR_TYPE = "viewQSDR_TYPE";
    public static final String[][] SEARCH_QSDR_TYPE = {
            {"" + LC.QSDR_TYPE_SEARCH_NAMEEN, "name_en"},
            {"" + LC.QSDR_TYPE_SEARCH_NAMEBN, "name_bn"},
            {"" + LC.QSDR_TYPE_SEARCH_DESCRIPTION, "description"},
            {"" + LC.QSDR_TYPE_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_EDMS_STRUCTURE = "navEDMS_STRUCTURE";
    public final static String VIEW_EDMS_STRUCTURE = "viewEDMS_STRUCTURE";
    public static final String[][] SEARCH_EDMS_STRUCTURE = {
            {"" + LC.EDMS_STRUCTURE_SEARCH_NAMEEN, "name_en"},
            {"" + LC.EDMS_STRUCTURE_SEARCH_NAMEBN, "name_bn"},
            {"" + LC.EDMS_STRUCTURE_SEARCH_DESCRIPTION, "description"},
            {"" + LC.EDMS_STRUCTURE_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_EDMS_DISCIPLINE = "navEDMS_DISCIPLINE";
    public final static String VIEW_EDMS_DISCIPLINE = "viewEDMS_DISCIPLINE";
    public static final String[][] SEARCH_EDMS_DISCIPLINE = {
            {"" + LC.EDMS_DISCIPLINE_SEARCH_NAMEEN, "name_en"},
            {"" + LC.EDMS_DISCIPLINE_SEARCH_NAMEBN, "name_bn"},
            {"" + LC.EDMS_DISCIPLINE_SEARCH_DESCRIPTION, "description"},
            {"" + LC.EDMS_DISCIPLINE_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_EDMS_OFFICE_SECTION = "navEDMS_OFFICE_SECTION";
    public final static String VIEW_EDMS_OFFICE_SECTION = "viewEDMS_OFFICE_SECTION";
    public static final String[][] SEARCH_EDMS_OFFICE_SECTION = {
            {"" + LC.EDMS_OFFICE_SECTION_SEARCH_NAMEEN, "name_en"},
            {"" + LC.EDMS_OFFICE_SECTION_SEARCH_NAMEBN, "name_bn"},
            {"" + LC.EDMS_OFFICE_SECTION_SEARCH_DESCRIPTION, "description"},
            {"" + LC.EDMS_OFFICE_SECTION_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_QSDR_DOCUMENTS = "navQSDR_DOCUMENTS";
    public final static String VIEW_QSDR_DOCUMENTS = "viewQSDR_DOCUMENTS";
    public static final String[][] SEARCH_QSDR_DOCUMENTS = {
            {"" + LC.QSDR_DOCUMENTS_SEARCH_EDMSOFFICESECTIONTYPE, "edms_office_section_type"},
            {"" + LC.QSDR_DOCUMENTS_SEARCH_DOCUMENTWORKDESCRIPTION, "document_work_description"},
            {"" + LC.QSDR_DOCUMENTS_SEARCH_DIVISION, "division"},
            {"" + LC.QSDR_DOCUMENTS_SEARCH_UNIT, "unit"},
            {"" + LC.QSDR_DOCUMENTS_SEARCH_REFERENCENO, "reference_no"},
            {"" + LC.QSDR_DOCUMENTS_SEARCH_REFERENCENOPART1, "reference_no_part1"},
            {"" + LC.QSDR_DOCUMENTS_SEARCH_REFERENCENOPART2, "reference_no_part2"},
            {"" + LC.QSDR_DOCUMENTS_SEARCH_REFERENCENOPART3, "reference_no_part3"},
            {"" + LC.QSDR_DOCUMENTS_SEARCH_REFERENCENOPART4, "reference_no_part4"},
            {"" + LC.QSDR_DOCUMENTS_SEARCH_REFERENCENOPART5, "reference_no_part5"},
            {"" + LC.QSDR_DOCUMENTS_SEARCH_REFERENCENOPART6, "reference_no_part6"},
            {"" + LC.QSDR_DOCUMENTS_SEARCH_DOCUMENTSUBJECT, "document_subject"},
            {"" + LC.QSDR_DOCUMENTS_SEARCH_DOCUMENTDATE, "document_date"},
            {"" + LC.QSDR_DOCUMENTS_SEARCH_DESCRIPTION, "description"},
            {"" + LC.QSDR_DOCUMENTS_SEARCH_EAREPRESENTATIVENAME, "EA_representative_name"},
            {"" + LC.QSDR_DOCUMENTS_SEARCH_SUBMISSIONTIME, "submission_time"},
            {"" + LC.QSDR_DOCUMENTS_SEARCH_CONTRACTORSSITEENGINEERNAME, "contractors_site_engineer_name"},
            {"" + LC.QSDR_DOCUMENTS_SEARCH_EDMSDISCIPLINETYPE, "edms_discipline_type"},
            {"" + LC.QSDR_DOCUMENTS_SEARCH_EDMSSTRUCTURETYPE, "edms_structure_type"},
            {"" + LC.QSDR_DOCUMENTS_SEARCH_QSDRTYPETYPE, "qsdr_type_type"},
            {"" + LC.QSDR_DOCUMENTS_SEARCH_ISINSPECTION, "is_inspection"},
            {"" + LC.QSDR_DOCUMENTS_SEARCH_ISPLANNEDSURVEILLANCE, "is_planned_surveillance"},
            {"" + LC.QSDR_DOCUMENTS_SEARCH_ISUNPLANNEDSURVEILLANCE, "is_unplanned_surveillance"},
            {"" + LC.QSDR_DOCUMENTS_SEARCH_DOCDISTGROUPTYPE, "doc_dist_group_type"},
            {"" + LC.QSDR_DOCUMENTS_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_NCN_DRAFT = "navNCN_DRAFT";
    public final static String VIEW_NCN_DRAFT = "viewNCN_DRAFT";
    public static final String[][] SEARCH_NCN_DRAFT = {
            {"" + LC.NCN_DRAFT_SEARCH_REFERENCENO, "referenceNo"},
            {"" + LC.NCN_DRAFT_SEARCH_ISSUEDTOOFCTYPECAT, "issued_to_ofc_type_cat"},
            {"" + LC.NCN_DRAFT_SEARCH_ISSUEDTOOFCNAME, "issued_to_ofc_name"},
            {"" + LC.NCN_DRAFT_SEARCH_ISSUEDTOPROCESSCAT, "issued_to_process_cat"},
            {"" + LC.NCN_DRAFT_SEARCH_DESCRIPTIONNONCONFORMANCE, "description_non_conformance"},
            {"" + LC.NCN_DRAFT_SEARCH_REFERENCEDOCUMENTS, "reference_documents"},
            {"" + LC.NCN_DRAFT_SEARCH_EVIDENCEATTACHED, "evidence_attached"},
            {"" + LC.NCN_DRAFT_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_HSE_ENFORMANCE_NOTICE = "navHSE_ENFORMANCE_NOTICE";
    public final static String VIEW_HSE_ENFORMANCE_NOTICE = "viewHSE_ENFORMANCE_NOTICE";
    public static final String[][] SEARCH_HSE_ENFORMANCE_NOTICE = {
            {"" + LC.HSE_ENFORMANCE_NOTICE_SEARCH_EDMSOFFICESECTIONTYPE, "edms_office_section_type"},
            {"" + LC.HSE_ENFORMANCE_NOTICE_SEARCH_DIVISION, "division"},
            {"" + LC.HSE_ENFORMANCE_NOTICE_SEARCH_UNIT, "unit"},
            {"" + LC.HSE_ENFORMANCE_NOTICE_SEARCH_REFERENCENO, "reference_no"},
            {"" + LC.HSE_ENFORMANCE_NOTICE_SEARCH_REFERENCENOPART1, "reference_no_part1"},
            {"" + LC.HSE_ENFORMANCE_NOTICE_SEARCH_REFERENCENOPART2, "reference_no_part2"},
            {"" + LC.HSE_ENFORMANCE_NOTICE_SEARCH_REFERENCENOPART3, "reference_no_part3"},
            {"" + LC.HSE_ENFORMANCE_NOTICE_SEARCH_REFERENCENOPART4, "reference_no_part4"},
            {"" + LC.HSE_ENFORMANCE_NOTICE_SEARCH_REFERENCENOPART5, "reference_no_part5"},
            {"" + LC.HSE_ENFORMANCE_NOTICE_SEARCH_REFERENCENOPART6, "reference_no_part6"},
            {"" + LC.HSE_ENFORMANCE_NOTICE_SEARCH_NOTICETYPE, "notice_type"},
            {"" + LC.HSE_ENFORMANCE_NOTICE_SEARCH_ISPPE, "is_ppe"},
            {"" + LC.HSE_ENFORMANCE_NOTICE_SEARCH_ISENVIRONMENTAL, "is_environmental"},
            {"" + LC.HSE_ENFORMANCE_NOTICE_SEARCH_ISHEALTHORHYGIENE, "is_health_or_hygiene"},
            {"" + LC.HSE_ENFORMANCE_NOTICE_SEARCH_SAFETYTYPE, "safety_type"},
            {"" + LC.HSE_ENFORMANCE_NOTICE_SEARCH_LOCATION, "location"},
            {"" + LC.HSE_ENFORMANCE_NOTICE_SEARCH_NOTICEDATE, "notice_date"},
            {"" + LC.HSE_ENFORMANCE_NOTICE_SEARCH_NOTICETIME, "notice_time"},
            {"" + LC.HSE_ENFORMANCE_NOTICE_SEARCH_NOTICEDESRIPTION, "notice_desription"},
            {"" + LC.HSE_ENFORMANCE_NOTICE_SEARCH_TIMESCALETYPE, "timescale_type"},
            {"" + LC.HSE_ENFORMANCE_NOTICE_SEARCH_ISSUERNAMETYPE, "issuer_name_type"},
            {"" + LC.HSE_ENFORMANCE_NOTICE_SEARCH_ISSUERDESIGNATION, "issuer_designation"},
            {"" + LC.HSE_ENFORMANCE_NOTICE_SEARCH_ISSUERSIGNATURE, "issuer_signature"},
            {"" + LC.HSE_ENFORMANCE_NOTICE_SEARCH_ISSUEDATE, "issue_date"},
            {"" + LC.HSE_ENFORMANCE_NOTICE_SEARCH_RECEIVERNAME, "receiver_name"},
            {"" + LC.HSE_ENFORMANCE_NOTICE_SEARCH_RECEIVERSIGNATURE, "receiver_signature"},
            {"" + LC.HSE_ENFORMANCE_NOTICE_SEARCH_RECEIVINGDATE, "receiving_date"},
            {"" + LC.HSE_ENFORMANCE_NOTICE_SEARCH_CLOSUREISSUERNAMETYPE, "closure_issuer_name_type"},
            {"" + LC.HSE_ENFORMANCE_NOTICE_SEARCH_CLOSUREISSUERDESIGNATION, "closure_issuer_designation"},
            {"" + LC.HSE_ENFORMANCE_NOTICE_SEARCH_CLOSUREISSUERSIGNATURE, "closure_issuer_signature"},
            {"" + LC.HSE_ENFORMANCE_NOTICE_SEARCH_CLOSUREISSUEDATE, "closure_issue_date"},
            {"" + LC.HSE_ENFORMANCE_NOTICE_SEARCH_DOCDISTGROUPTYPE, "doc_dist_group_type"},
            {"" + LC.HSE_ENFORMANCE_NOTICE_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_SMS_LOG = "navSMS_LOG";
    public final static String VIEW_SMS_LOG = "viewSMS_LOG";
    public static final String[][] SEARCH_SMS_LOG = {
            {"" + LC.SMS_LOG_SEARCH_SENDER, "sender"},
            {"" + LC.SMS_LOG_SEARCH_RECEIVER, "receiver"},
            {"" + LC.SMS_LOG_ADD_SMSRECEIVERTYPECAT, "sms_receiver_type_cat"},
            {"" + LC.SMS_LOG_SEARCH_MESSAGE, "message"},
            {"" + LC.SMS_LOG_EDIT_SMSSTATUSCAT, "sms_status_cat"},
            {"" + LC.SMS_LOG_SEARCH_INSERTIONTIME, "insertionTime"},
            {"" + LC.SMS_LOG_SEARCH_FETCHINGTIME, "fetchingTime"},
            {"" + LC.SMS_LOG_SEARCH_SENDINGTIME, "sendingTime"},
            {"" + LC.SMS_LOG_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_NOC_INFO = "navNOC_INFO";
    public final static String VIEW_NOC_INFO = "viewNOC_INFO";
    public static final String[][] SEARCH_NOC_INFO = {
            {"" + LC.NOC_INFO_SEARCH_SUBMISSIONNO, "submission_no"},
            {"" + LC.NOC_INFO_SEARCH_CREATIONDATE, "creation_date"},
            {"" + LC.NOC_INFO_SEARCH_CONTENT, "content"},
            {"" + LC.NOC_INFO_SEARCH_TITLE, "title"},
            {"" + LC.NOC_INFO_SEARCH_FROMNAME, "fromName"},
            {"" + LC.NOC_INFO_SEARCH_FROMORGID, "fromOrgId"},
            {"" + LC.NOC_INFO_SEARCH_FROMUSERID, "fromUserId"},
            {"" + LC.NOC_INFO_SEARCH_FROMADDRESS, "fromAddress"},
            {"" + LC.NOC_INFO_SEARCH_SENDEROFFICEUNITID, "sender_office_unit_id"},
            {"" + LC.NOC_INFO_SEARCH_SENDEROFFICEUNITNAME, "sender_office_unit_name"},
            {"" + LC.NOC_INFO_SEARCH_DOCUMENTSIGNATUREDATE, "document_signature_date"},
            {"" + LC.NOC_INFO_SEARCH_ADDEDBYUSERID, "added_by_user_id"},
            {"" + LC.NOC_INFO_SEARCH_ADDEDBYUSERNAME, "added_by_username"},
            {"" + LC.NOC_INFO_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_LVM = "navLVM";
    public final static String VIEW_LVM = "viewLVM";
    public static final String[][] SEARCH_LVM = {
            {"" + LC.LVM_SEARCH_EMPLOYEEINLEAVETYPE, "employee_in_leave_type"},
            {"" + LC.LVM_SEARCH_EMPLOYEEINLEAVEORGANOGRAMID, "employee_in_leave_organogram_id"},
            {"" + LC.LVM_SEARCH_EMPLOYEEINLEAVENAME, "employee_in_leave_name"},
            {"" + LC.LVM_SEARCH_EMPLOYEEINLEAVEDESIGNATION, "employee_in_leave_designation"},
            {"" + LC.LVM_SEARCH_EMPLOYEEINLEAVEOFFICEUNITID, "employee_in_leave_office_unit_id"},
            {"" + LC.LVM_SEARCH_EMPLOYEEINLEAVEOFFICEUNITNAME, "employee_in_leave_office_unit_name"},
            {"" + LC.LVM_SEARCH_EMPLOYEEINCHARGETYPE, "employee_in_charge_type"},
            {"" + LC.LVM_SEARCH_EMPLOYEEINCHARGEORGANOGRAMID, "employee_in_charge_organogram_id"},
            {"" + LC.LVM_SEARCH_EMPLOYEEINCHARGENAME, "employee_in_charge_name"},
            {"" + LC.LVM_SEARCH_EMPLOYEEINCHARGEDESIGNATION, "employee_in_charge_designation"},
            {"" + LC.LVM_SEARCH_EMPLOYEEINCHARGEOFFICEUNITID, "employee_in_charge_office_unit_id"},
            {"" + LC.LVM_SEARCH_EMPLOYEEINCHARGEOFFICEUNITNAME, "employee_in_charge_office_unit_name"},
            {"" + LC.LVM_SEARCH_EMPLOYEEINCHARGEPOLICYROLESTYPE, "employee_in_charge_policy_roles_type"},
            {"" + LC.LVM_SEARCH_LEAVESTARTDATE, "leave_start_date"},
            {"" + LC.LVM_SEARCH_LEAVEENDDATE, "leave_end_date"},
            {"" + LC.LVM_SEARCH_LEAVEENTRYDATE, "leave_entry_date"},
            {"" + LC.LVM_SEARCH_LEAVESTATUSCAT, "leave_status_cat"},
            {"" + LC.LVM_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_REPORT_HEADING = "navREPORT_HEADING";
    public final static String VIEW_REPORT_HEADING = "viewREPORT_HEADING";
    public static final String[][] SEARCH_REPORT_HEADING = {
            {"" + LC.REPORT_HEADING_SEARCH_NAMEEN, "name_en"},
            {"" + LC.REPORT_HEADING_SEARCH_NAMEBN, "name_bn"},
            {"" + LC.REPORT_HEADING_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_REPORT_SUB_HEADING_TOPICS = "navREPORT_SUB_HEADING_TOPICS";
    public final static String VIEW_REPORT_SUB_HEADING_TOPICS = "viewREPORT_SUB_HEADING_TOPICS";
    public static final String[][] SEARCH_REPORT_SUB_HEADING_TOPICS = {
            {"" + LC.REPORT_SUB_HEADING_TOPICS_SEARCH_REPORTHEADINGTYPE, "report_heading_type"},
            {"" + LC.REPORT_SUB_HEADING_TOPICS_SEARCH_REPORTSUBHEADINGTYPE, "report_sub_heading_type"},
            {"" + LC.REPORT_SUB_HEADING_TOPICS_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_WEEKLY_REPORT_DATA = "navWEEKLY_REPORT_DATA";
    public final static String VIEW_WEEKLY_REPORT_DATA = "viewWEEKLY_REPORT_DATA";
    public static final String[][] SEARCH_WEEKLY_REPORT_DATA = {
            {"" + LC.WEEKLY_REPORT_DATA_SEARCH_SECTIONCAT, "section_cat"},
            {"" + LC.WEEKLY_REPORT_DATA_SEARCH_YEARNUMBER, "year_number"},
            {"" + LC.WEEKLY_REPORT_DATA_SEARCH_MONTHNUMBER, "month_number"},
            {"" + LC.WEEKLY_REPORT_DATA_SEARCH_WEEKNUMBER, "week_number"},
            {"" + LC.WEEKLY_REPORT_DATA_SEARCH_WEEKSTARTDATE, "week_start_date"},
            {"" + LC.WEEKLY_REPORT_DATA_SEARCH_WEEKENDDATE, "week_end_date"},
            {"" + LC.WEEKLY_REPORT_DATA_SEARCH_FROMCHAINAGEKM, "from_chainage_km"},
            {"" + LC.WEEKLY_REPORT_DATA_SEARCH_FROMCHAINAGEMETER, "from_chainage_meter"},
            {"" + LC.WEEKLY_REPORT_DATA_SEARCH_TOCHAINAGEKM, "to_chainage_km"},
            {"" + LC.WEEKLY_REPORT_DATA_SEARCH_TOCHAINAGEMETER, "to_chainage_meter"},
            {"" + LC.WEEKLY_REPORT_DATA_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_EMAIL_LOG = "navEMAIL_LOG";
    public final static String VIEW_EMAIL_LOG = "viewEMAIL_LOG";
    public static final String[][] SEARCH_EMAIL_LOG = {
            {"" + LC.EMAIL_LOG_SEARCH_FROMADDR, "from_addr"},
            {"" + LC.EMAIL_LOG_SEARCH_TOADDR, "to_addr"},
            {"" + LC.EMAIL_LOG_SEARCH_CC, "cc"},
            {"" + LC.EMAIL_LOG_SEARCH_SUBJECT, "subject"},
            {"" + LC.EMAIL_LOG_SEARCH_EMAILBODY, "email_body"},
            {"" + LC.EMAIL_LOG_SEARCH_ISDRAFT, "is_draft"},
            {"" + LC.EMAIL_LOG_SEARCH_SENDINGTIME, "sending_time"},
            {"" + LC.EMAIL_LOG_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_DOC_DIST_GROUP = "navDOC_DIST_GROUP";
    public final static String VIEW_DOC_DIST_GROUP = "viewDOC_DIST_GROUP";
    public static final String[][] SEARCH_DOC_DIST_GROUP = {
            {"" + LC.DOC_DIST_GROUP_SEARCH_NAMEEN, "name_en"},
            {"" + LC.DOC_DIST_GROUP_SEARCH_NAMEBN, "name_bn"},
            {"" + LC.DOC_DIST_GROUP_SEARCH_CREATEDBY, "created_by"},
            {"" + LC.DOC_DIST_GROUP_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_OFFICE_ORIGIN_UNIT_ORGANOGRAMS = "navOFFICE_ORIGIN_UNIT_ORGANOGRAMS";
    public final static String VIEW_OFFICE_ORIGIN_UNIT_ORGANOGRAMS = "viewOFFICE_ORIGIN_UNIT_ORGANOGRAMS";
    public static final String[][] SEARCH_OFFICE_ORIGIN_UNIT_ORGANOGRAMS = {
            {"" + LC.OFFICE_ORIGIN_UNIT_ORGANOGRAMS_SEARCH_OFFICEORIGINUNITID, "office_origin_unit_id"},
            {"" + LC.OFFICE_ORIGIN_UNIT_ORGANOGRAMS_SEARCH_SUPERIORUNITID, "superior_unit_id"},
            {"" + LC.OFFICE_ORIGIN_UNIT_ORGANOGRAMS_SEARCH_SUPERIORDESIGNATIONID, "superior_designation_id"},
            {"" + LC.OFFICE_ORIGIN_UNIT_ORGANOGRAMS_SEARCH_DESIGNATIONENG, "designation_eng"},
            {"" + LC.OFFICE_ORIGIN_UNIT_ORGANOGRAMS_SEARCH_DESIGNATIONBNG, "designation_bng"},
            {"" + LC.OFFICE_ORIGIN_UNIT_ORGANOGRAMS_SEARCH_DESIGNATIONLEVEL, "designation_level"},
            {"" + LC.OFFICE_ORIGIN_UNIT_ORGANOGRAMS_SEARCH_DESIGNATIONSEQUENCE, "designation_sequence"},
            {"" + LC.OFFICE_ORIGIN_UNIT_ORGANOGRAMS_SEARCH_STATUS, "status"},
            {"" + LC.OFFICE_ORIGIN_UNIT_ORGANOGRAMS_SEARCH_ANYFIELD, "AnyField"}
    };

    public final static String NAV_HOLIDAY = "navHOLIDAY";
    public final static String VIEW_HOLIDAY = "viewHOLIDAY";
    public static final String[][] SEARCH_HOLIDAY = {
            {"" + LC.HOLIDAY_SEARCH_CURRENTDAY, "current_day"},
            {"" + LC.HOLIDAY_SEARCH_ISHOLIDAY, "is_holiday"},
            {"" + LC.HOLIDAY_SEARCH_DESCRIPTION, "description"},
            {"" + LC.HOLIDAY_SEARCH_HOLIDAYCAT, "holiday_cat"},
            {"" + LC.HOLIDAY_SEARCH_OFFICESTARTTIME, "office_start_time"},
            {"" + LC.HOLIDAY_SEARCH_OFFICEENDTIME, "office_end_time"},
            {"" + LC.HOLIDAY_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_ADDRESS_BOOK = "navADDRESS_BOOK";
    public final static String VIEW_ADDRESS_BOOK = "viewADDRESS_BOOK";
    public static final String[][] SEARCH_ADDRESS_BOOK = {
            {"" + LC.ADDRESS_BOOK_SEARCH_NAMEEN, "name_en"},
            {"" + LC.ADDRESS_BOOK_SEARCH_NAMEBN, "name_bn"},
            {"" + LC.ADDRESS_BOOK_SEARCH_CREATEDBY, "created_by"},
            {"" + LC.ADDRESS_BOOK_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_MEETING_REQUEST_DETAILS = "navMEETING_REQUEST_DETAILS";
    public final static String VIEW_MEETING_REQUEST_DETAILS = "viewMEETING_REQUEST_DETAILS";
    public static final String[][] SEARCH_MEETING_REQUEST_DETAILS = {
            {"" + LC.MEETING_REQUEST_DETAILS_SEARCH_MEETINGDATE, "meeting_date"},
            {"" + LC.MEETING_REQUEST_DETAILS_SEARCH_MEETINGSTARTTIME, "meeting_start_time"},
            {"" + LC.MEETING_REQUEST_DETAILS_SEARCH_MEETINGENDTIME, "meeting_end_time"},
            {"" + LC.MEETING_REQUEST_DETAILS_SEARCH_MEETINGSUBJECT, "meeting_subject"},
            {"" + LC.MEETING_REQUEST_DETAILS_SEARCH_MEETINGPLACE, "meeting_place"},
            {"" + LC.MEETING_REQUEST_DETAILS_SEARCH_MEETINGAGENDA, "meeting_agenda"},
            {"" + LC.MEETING_REQUEST_DETAILS_SEARCH_RESPONSIBLEMOMPERSONID, "responsible_mom_person_id"},
            {"" + LC.MEETING_REQUEST_DETAILS_SEARCH_USERID, "user_id"},
            {"" + LC.MEETING_REQUEST_DETAILS_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_MEETING_MINUTES_ADDING = "navMEETING_MINUTES_ADDING";
    public final static String VIEW_MEETING_MINUTES_ADDING = "viewMEETING_MINUTES_ADDING";
    public static final String[][] SEARCH_MEETING_MINUTES_ADDING = {
            {"" + LC.MEETING_MINUTES_ADDING_SEARCH_MEETINGREQUESTDETAILSID, "meeting_request_details_id"},
            {"" + LC.MEETING_MINUTES_ADDING_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_DRAWING = "navDRAWING";
    public final static String VIEW_DRAWING = "viewDRAWING";
    public static final String[][] SEARCH_DRAWING = {
            {"" + LC.DRAWING_SEARCH_NAMEEN, "name_en"},
            {"" + LC.DRAWING_SEARCH_NAMEBN, "name_bn"},
            {"" + LC.DRAWING_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_DRAWING_DOCUMENTS = "navDRAWING_DOCUMENTS";
    public final static String VIEW_DRAWING_DOCUMENTS = "viewDRAWING_DOCUMENTS";
    public static final String[][] SEARCH_DRAWING_DOCUMENTS = {
            {"" + LC.DRAWING_DOCUMENTS_SEARCH_CREATORORGANOGRAMID, "creator_organogram_id"},
            {"" + LC.DRAWING_DOCUMENTS_SEARCH_SUBMISSIONDATE, "submission_date"},
            {"" + LC.DRAWING_DOCUMENTS_SEARCH_RECEIVEDATE, "receive_date"},
            {"" + LC.DRAWING_DOCUMENTS_SEARCH_SUBJECT, "subject"},
            {"" + LC.DRAWING_DOCUMENTS_SEARCH_FROMCHAINAGEKM, "from_chainage_km"},
            {"" + LC.DRAWING_DOCUMENTS_SEARCH_FROMCHAINAGEMETER, "from_chainage_meter"},
            {"" + LC.DRAWING_DOCUMENTS_SEARCH_FROMCHAINAGEMM, "from_chainage_mm"},
            {"" + LC.DRAWING_DOCUMENTS_SEARCH_TOCHAINAGEKM, "to_chainage_km"},
            {"" + LC.DRAWING_DOCUMENTS_SEARCH_TOCHAINAGEMETER, "to_chainage_meter"},
            {"" + LC.DRAWING_DOCUMENTS_SEARCH_TOCHAINAGEMM, "to_chainage_mm"},
            {"" + LC.DRAWING_DOCUMENTS_SEARCH_LETTERREFERENCETO, "letter_reference_to"},
            {"" + LC.DRAWING_DOCUMENTS_SEARCH_LETTERREFERENCEAUTHOR, "letter_reference_author"},
            {"" + LC.DRAWING_DOCUMENTS_SEARCH_LETTERREFERENCEDEPT, "letter_reference_dept"},
            {"" + LC.DRAWING_DOCUMENTS_SEARCH_LETTERREFERENCEYEAR, "letter_reference_year"},
            {"" + LC.DRAWING_DOCUMENTS_SEARCH_LETTERREFERENCENO, "letter_reference_no"},
            {"" + LC.DRAWING_DOCUMENTS_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_EDMS_OFFICE_DIVISION = "navEDMS_OFFICE_DIVISION";
    public final static String VIEW_EDMS_OFFICE_DIVISION = "viewEDMS_OFFICE_DIVISION";
    public static final String[][] SEARCH_EDMS_OFFICE_DIVISION = {
            {"" + LC.EDMS_OFFICE_DIVISION_SEARCH_NAMEEN, "name_en"},
            {"" + LC.EDMS_OFFICE_DIVISION_SEARCH_NAMEBN, "name_bn"},
            {"" + LC.EDMS_OFFICE_DIVISION_SEARCH_DESCRIPTION, "description"},
            {"" + LC.EDMS_OFFICE_DIVISION_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_EDMS_OFFICE_UNIT = "navEDMS_OFFICE_UNIT";
    public final static String VIEW_EDMS_OFFICE_UNIT = "viewEDMS_OFFICE_UNIT";
    public static final String[][] SEARCH_EDMS_OFFICE_UNIT = {
            {"" + LC.EDMS_OFFICE_UNIT_SEARCH_NAMEEN, "name_en"},
            {"" + LC.EDMS_OFFICE_UNIT_SEARCH_NAMEBN, "name_bn"},
            {"" + LC.EDMS_OFFICE_UNIT_SEARCH_DESCRIPTION, "description"},
            {"" + LC.EDMS_OFFICE_UNIT_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_EDMS_ISSUER = "navEDMS_ISSUER";
    public final static String VIEW_EDMS_ISSUER = "viewEDMS_ISSUER";
    public static final String[][] SEARCH_EDMS_ISSUER = {
            {"" + LC.EDMS_ISSUER_SEARCH_NAMEEN, "name_en"},
            {"" + LC.EDMS_ISSUER_SEARCH_NAMEBN, "name_bn"},
            {"" + LC.EDMS_ISSUER_SEARCH_DESCRIPTION, "description"},
            {"" + LC.EDMS_ISSUER_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_LETTER_RECIPIENT_DETAIL = "navLETTER_RECIPIENT_DETAIL";
    public final static String VIEW_LETTER_RECIPIENT_DETAIL = "viewLETTER_RECIPIENT_DETAIL";
    public static final String[][] SEARCH_LETTER_RECIPIENT_DETAIL = {
            {"" + LC.LETTER_RECIPIENT_DETAIL_SEARCH_RECIPIENTALIAS, "recipient_alias"},
            {"" + LC.LETTER_RECIPIENT_DETAIL_SEARCH_RECIPIENTNAME, "recipient_name"},
            {"" + LC.LETTER_RECIPIENT_DETAIL_SEARCH_RECIPIENTDESIGNATION, "recipient_designation"},
            {"" + LC.LETTER_RECIPIENT_DETAIL_SEARCH_PROJECTNAME, "project_name"},
            {"" + LC.LETTER_RECIPIENT_DETAIL_SEARCH_COMPANYNAME, "company_name"},
            {"" + LC.LETTER_RECIPIENT_DETAIL_SEARCH_OFFICEUNIT, "office_unit"},
            {"" + LC.LETTER_RECIPIENT_DETAIL_SEARCH_REFERENCETEXT, "reference_text"},
            {"" + LC.LETTER_RECIPIENT_DETAIL_SEARCH_DETAILADDRESS, "detail_address"},
            {"" + LC.LETTER_RECIPIENT_DETAIL_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_MEDICINE_GENERIC_NAME = "navMEDICINE_GENERIC_NAME";
    public final static String VIEW_MEDICINE_GENERIC_NAME = "viewMEDICINE_GENERIC_NAME";
    public static final String[][] SEARCH_MEDICINE_GENERIC_NAME = {
            {"" + LC.MEDICINE_GENERIC_NAME_SEARCH_NAMEEN, "name_en"},
            {"" + LC.MEDICINE_GENERIC_NAME_SEARCH_NAMEBN, "name_bn"},
            {"" + LC.MEDICINE_GENERIC_NAME_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_PHARMA_COMPANY_NAME = "navPHARMA_COMPANY_NAME";
    public final static String VIEW_PHARMA_COMPANY_NAME = "viewPHARMA_COMPANY_NAME";
    public static final String[][] SEARCH_PHARMA_COMPANY_NAME = {
            {"" + LC.PHARMA_COMPANY_NAME_SEARCH_NAMEEN, "name_en"},
            {"" + LC.PHARMA_COMPANY_NAME_SEARCH_NAMEBN, "name_bn"},
            {"" + LC.PHARMA_COMPANY_NAME_SEARCH_ADDRESS, "address"},
            {"" + LC.PHARMA_COMPANY_NAME_SEARCH_CONTACT, "contact"},
            {"" + LC.PHARMA_COMPANY_NAME_SEARCH_EMAIL, "email"},
            {"" + LC.PHARMA_COMPANY_NAME_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_LAB_TEST = "navLAB_TEST";
    public final static String VIEW_LAB_TEST = "viewLAB_TEST";
    public static final String[][] SEARCH_LAB_TEST = {
            {"" + LC.LAB_TEST_SEARCH_NAMEEN, "name_en"},
            {"" + LC.LAB_TEST_SEARCH_NAMEBN, "name_bn"},
            {"" + LC.LAB_TEST_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_DOCTOR_TIME_SLOT = "navDOCTOR_TIME_SLOT";
    public final static String VIEW_DOCTOR_TIME_SLOT = "viewDOCTOR_TIME_SLOT";
    public static final String[][] SEARCH_DOCTOR_TIME_SLOT = {
            {"" + LC.DOCTOR_TIME_SLOT_SEARCH_DEPTCAT, "dept_cat"},
            {"" + LC.DOCTOR_TIME_SLOT_SEARCH_DOCTORID, "doctor_id"},
            {"" + LC.DOCTOR_TIME_SLOT_SEARCH_SHIFTCAT, "shift_cat"},
            {"" + LC.DOCTOR_TIME_SLOT_SEARCH_STARTTIME, "start_time"},
            {"" + LC.DOCTOR_TIME_SLOT_SEARCH_ENDTIME, "end_time"},
            {"" + LC.DOCTOR_TIME_SLOT_SEARCH_DURATIONPERPATIENT, "duration_per_patient"},
            {"" + LC.DOCTOR_TIME_SLOT_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_FAMILY = "navFAMILY";
    public final static String VIEW_FAMILY = "viewFAMILY";
    public static final String[][] SEARCH_FAMILY = {
            {"" + LC.FAMILY_SEARCH_ORGANOGRAMID, "organogram_id"},
            {"" + LC.FAMILY_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_PRESCRIPTION_DETAILS = "navPRESCRIPTION_DETAILS";
    public final static String VIEW_PRESCRIPTION_DETAILS = "viewPRESCRIPTION_DETAILS";
    public static final String[][] SEARCH_PRESCRIPTION_DETAILS = {
            {"" + LC.PRESCRIPTION_DETAILS_SEARCH_APPOINTMENTID, "appointment_id"},
            {"" + LC.PRESCRIPTION_DETAILS_SEARCH_NAME, "name"},
            {"" + LC.PRESCRIPTION_DETAILS_SEARCH_DATEOFBIRTH, "date_of_birth"},
            {"" + LC.PRESCRIPTION_DETAILS_SEARCH_AGE, "age"},
            {"" + LC.PRESCRIPTION_DETAILS_SEARCH_HEIGHT, "height"},
            {"" + LC.PRESCRIPTION_DETAILS_SEARCH_WEIGHT, "weight"},
            {"" + LC.PRESCRIPTION_DETAILS_SEARCH_BLOODPRESSURE, "blood_pressure"},
            {"" + LC.PRESCRIPTION_DETAILS_SEARCH_DIAGNOSIS, "diagnosis"},
            {"" + LC.PRESCRIPTION_DETAILS_SEARCH_CHIEFCOMPLAINTS, "chief_complaints"},
            {"" + LC.PRESCRIPTION_DETAILS_SEARCH_CLINICALFEATURES, "clinical_features"},
            {"" + LC.PRESCRIPTION_DETAILS_SEARCH_ADVICE, "advice"},
            {"" + LC.PRESCRIPTION_DETAILS_SEARCH_REMARKS, "remarks"},
            {"" + LC.PRESCRIPTION_DETAILS_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_DRUG_INFORMATION = "navDRUG_INFORMATION";
    public final static String VIEW_DRUG_INFORMATION = "viewDRUG_INFORMATION";
    public static final String[][] SEARCH_DRUG_INFORMATION = {
            {"" + LC.DRUG_INFORMATION_SEARCH_NAMEEN, "name_en"},
            {"" + LC.DRUG_INFORMATION_SEARCH_NAMEBN, "name_bn"},
            {"" + LC.DRUG_INFORMATION_SEARCH_MEDICINEGENERICNAMETYPE, "medicine_generic_name_type"},
            {"" + LC.DRUG_INFORMATION_SEARCH_PHARMACOMPANYNAMETYPE, "pharma_company_name_type"},
            {"" + LC.DRUG_INFORMATION_SEARCH_DRUGFORMCAT, "drug_form_cat"},
            {"" + LC.DRUG_INFORMATION_SEARCH_STRENGTH, "strength"},
            {"" + LC.DRUG_INFORMATION_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_MEDICINE_INVENTORY2 = "navMEDICINE_INVENTORY2";
    public final static String VIEW_MEDICINE_INVENTORY2 = "viewMEDICINE_INVENTORY2";
    public static final String[][] SEARCH_MEDICINE_INVENTORY2 = {
            {"" + LC.MEDICINE_INVENTORY2_SEARCH_MEDICINEGENERICNAMETYPE, "medicine_generic_name_type"},
            {"" + LC.MEDICINE_INVENTORY2_SEARCH_PHARMACOMPANYNAMETYPE, "pharma_company_name_type"},
            {"" + LC.MEDICINE_INVENTORY2_SEARCH_DRUGINFORMATIONTYPE, "drug_information_type"},
            {"" + LC.MEDICINE_INVENTORY2_SEARCH_PRICE, "price"},
            {"" + LC.MEDICINE_INVENTORY2_SEARCH_RELATEDPOID, "related_po_id"},
            {"" + LC.MEDICINE_INVENTORY2_SEARCH_SCANCODE, "scan_code"},
            {"" + LC.MEDICINE_INVENTORY2_SEARCH_PURCHASEDATE, "purchase_date"},
            {"" + LC.MEDICINE_INVENTORY2_SEARCH_REMARKS, "remarks"},
            {"" + LC.MEDICINE_INVENTORY2_SEARCH_INSERTEDBYUSERID, "inserted_by_user_id"},
            {"" + LC.MEDICINE_INVENTORY2_SEARCH_INSERTEDBYORGANOGRAMID, "inserted_by_organogram_id"},
            {"" + LC.MEDICINE_INVENTORY2_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_ROLE_ACTIONS = "navROLE_ACTIONS";
    public final static String VIEW_ROLE_ACTIONS = "viewROLE_ACTIONS";
    public static final String[][] SEARCH_ROLE_ACTIONS = {
            {"" + LC.ROLE_ACTIONS_SEARCH_ROLETYPE, "role_type"},
            {"" + LC.ROLE_ACTIONS_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_ENTRY_PAGE = "navENTRY_PAGE";
    public final static String VIEW_ENTRY_PAGE = "viewENTRY_PAGE";
    public static final String[][] SEARCH_ENTRY_PAGE = {
            {"" + LC.ENTRY_PAGE_SEARCH_ORGANOGRAMID, "organogram_id"},
            {"" + LC.ENTRY_PAGE_SEARCH_ACTIONDESCRIPTIONTYPE, "action_description_type"},
            {"" + LC.ENTRY_PAGE_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_TEST_BABA = "navTEST_BABA";
    public final static String VIEW_TEST_BABA = "viewTEST_BABA";
    public static final String[][] SEARCH_TEST_BABA = {
            {"" + LC.TEST_BABA_SEARCH_ROLEACTIONSID, "role_actions_id"},
            {"" + LC.TEST_BABA_SEARCH_NAMEEN, "name_en"},
            {"" + LC.TEST_BABA_SEARCH_NAMEBN, "name_bn"},
            {"" + LC.TEST_BABA_SEARCH_DATEOFBIRTH, "date_of_birth"},
            {"" + LC.TEST_BABA_SEARCH_TIMEOFBIRTH, "time_of_birth"},
            {"" + LC.TEST_BABA_SEARCH_INSERTEDBYUSERID, "inserted_by_user_id"},
            {"" + LC.TEST_BABA_SEARCH_INSERTEDBYORGANOGRAMID, "inserted_by_organogram_id"},
            {"" + LC.TEST_BABA_SEARCH_INSERTIONDATE, "insertion_date"},
            {"" + LC.TEST_BABA_SEARCH_ANYFIELD, "AnyField"}
    };

    public final static String NAV_PATIENT_MEASUREMENT = "navPATIENT_MEASUREMENT";
    public final static String VIEW_PATIENT_MEASUREMENT = "viewPATIENT_MEASUREMENT";
    public static final String[][] SEARCH_PATIENT_MEASUREMENT = {
            {"" + LC.PATIENT_MEASUREMENT_SEARCH_APPOINTMENTID, "appointment_id"},
            {"" + LC.PATIENT_MEASUREMENT_SEARCH_NAME, "name"},
            {"" + LC.PATIENT_MEASUREMENT_SEARCH_DATEOFBIRTH, "date_of_birth"},
            {"" + LC.PATIENT_MEASUREMENT_SEARCH_AGE, "age"},
            {"" + LC.PATIENT_MEASUREMENT_SEARCH_HEIGHT, "height"},
            {"" + LC.PATIENT_MEASUREMENT_SEARCH_WEIGHT, "weight"},
            {"" + LC.PATIENT_MEASUREMENT_SEARCH_ANYFIELD, "AnyField"}
    };

    public final static String NAV_EMPLOYEE_FAMILY_INFO = "navEMPLOYEE_FAMILY_INFO";
    public final static String VIEW_EMPLOYEE_FAMILY_INFO = "viewEMPLOYEE_FAMILY_INFO";
    public static final String[][] SEARCH_EMPLOYEE_FAMILY_INFO = {
            {"" + LC.EMPLOYEE_FAMILY_INFO_SEARCH_FIRSTNAMEEN, "first_name_en"},
            {"" + LC.EMPLOYEE_FAMILY_INFO_SEARCH_LASTNAMEEN, "last_name_en"},
            {"" + LC.EMPLOYEE_FAMILY_INFO_SEARCH_RELATIONTYPE, "relation_type"},
            {"" + LC.EMPLOYEE_FAMILY_INFO_SEARCH_NATIONALIDNO, "national_id_no"},
            {"" + LC.EMPLOYEE_FAMILY_INFO_SEARCH_MOBILENUMBER1, "mobile_number_1"},
            {"" + LC.EMPLOYEE_FAMILY_INFO_SEARCH_ANYFIELD, "AnyField"}
    };

    public final static String NAV_RELATION = "navRELATION";
    public final static String VIEW_RELATION = "viewRELATION";
    public static final String[][] SEARCH_RELATION = {
            {"" + LC.RELATION_SEARCH_NAMEEN, "name_en"},
            {"" + LC.RELATION_SEARCH_NAMEBN, "name_bn"},
            {"" + LC.RELATION_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_OCCUPATION = "navOCCUPATION";
    public final static String VIEW_OCCUPATION = "viewOCCUPATION";
    public static final String[][] SEARCH_OCCUPATION = {
            {"" + LC.OCCUPATION_SEARCH_NAMEEN, "name_en"},
            {"" + LC.OCCUPATION_SEARCH_NAMEBN, "name_bn"},
            {"" + LC.OCCUPATION_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_NATIONALITY = "navNATIONALITY";
    public final static String VIEW_NATIONALITY = "viewNATIONALITY";
    public static final String[][] SEARCH_NATIONALITY = {
            {"" + LC.NATIONALITY_SEARCH_NAMEEN, "name_en"},
            {"" + LC.NATIONALITY_SEARCH_NAMEBN, "name_bn"},
            {"" + LC.NATIONALITY_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_PROFESSION = "navPROFESSION";
    public final static String VIEW_PROFESSION = "viewPROFESSION";
    public static final String[][] SEARCH_PROFESSION = {
            {"" + LC.PROFESSION_SEARCH_NAMEEN, "name_en"},
            {"" + LC.PROFESSION_SEARCH_NAMEBN, "name_bn"},
            {"" + LC.PROFESSION_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_PARLIAMENT_SUPPLIER = "navPARLIAMENT_SUPPLIER";
    public final static String VIEW_PARLIAMENT_SUPPLIER = "viewPARLIAMENT_SUPPLIER";
    public static final String[][] SEARCH_PARLIAMENT_SUPPLIER = {
            {"" + LC.PARLIAMENT_SUPPLIER_SEARCH_NAMEEN, "name_en"},
            {"" + LC.PARLIAMENT_SUPPLIER_SEARCH_NAMEBN, "name_bn"},
            {"" + LC.PARLIAMENT_SUPPLIER_SEARCH_AGREEMENTDATE, "agreement_date"},
            {"" + LC.PARLIAMENT_SUPPLIER_SEARCH_TRADELICENSENUMBER, "trade_license_number"},
            {"" + LC.PARLIAMENT_SUPPLIER_SEARCH_TRADELICENSEEXPIRYDATE, "trade_license_expiry_date"},
            {"" + LC.PARLIAMENT_SUPPLIER_SEARCH_SUPPLIERCODE, "supplier_code"},
            {"" + LC.PARLIAMENT_SUPPLIER_SEARCH_ISFOREIGN, "is_foreign"},
            {"" + LC.PARLIAMENT_SUPPLIER_SEARCH_SUPPLIERBIN, "supplier_bin"},
            {"" + LC.PARLIAMENT_SUPPLIER_SEARCH_SUPPLIERADDRESS, "supplier_address"},
            {"" + LC.PARLIAMENT_SUPPLIER_SEARCH_SUPPLIERMOBILE1, "supplier_mobile_1"},
            {"" + LC.PARLIAMENT_SUPPLIER_SEARCH_SUPPLIERMOBILE2, "supplier_mobile_2"},
            {"" + LC.PARLIAMENT_SUPPLIER_SEARCH_SUPPLIEREMAIL, "supplier_email"},
            {"" + LC.PARLIAMENT_SUPPLIER_SEARCH_REMARKS, "remarks"},
            {"" + LC.PARLIAMENT_SUPPLIER_SEARCH_INSERTEDBYUSERID, "inserted_by_user_id"},
            {"" + LC.PARLIAMENT_SUPPLIER_SEARCH_INSERTEDBYORGANOGRAMID, "inserted_by_organogram_id"},
            {"" + LC.PARLIAMENT_SUPPLIER_SEARCH_INSERTIONDATE, "insertion_date"},
            {"" + LC.PARLIAMENT_SUPPLIER_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_EDUCATION_LEVEL = "navEDUCATION_LEVEL";
    public final static String VIEW_EDUCATION_LEVEL = "viewEDUCATION_LEVEL";
    public static final String[][] SEARCH_EDUCATION_LEVEL = {
            {"" + LC.EDUCATION_LEVEL_SEARCH_NAMEBN, "name_bn"},
            {"" + LC.EDUCATION_LEVEL_SEARCH_NAMEEN, "name_en"},
            {"" + LC.EDUCATION_LEVEL_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_RESULT_EXAM = "navRESULT_EXAM";
    public final static String VIEW_RESULT_EXAM = "viewRESULT_EXAM";
    public static final String[][] SEARCH_RESULT_EXAM = {
            {"" + LC.RESULT_EXAM_SEARCH_NAMEBN, "name_bn"},
            {"" + LC.RESULT_EXAM_SEARCH_NAMEEN, "name_en"},
            {"" + LC.RESULT_EXAM_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_BOARD = "navBOARD";
    public final static String VIEW_BOARD = "viewBOARD";
    public static final String[][] SEARCH_BOARD = {
            {"" + LC.BOARD_SEARCH_NAMEBN, "name_bn"},
            {"" + LC.BOARD_SEARCH_NAMEEN, "name_en"},
            {"" + LC.BOARD_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_EMPLOYEE_EDUCATION_INFO = "navEMPLOYEE_EDUCATION_INFO";
    public final static String VIEW_EMPLOYEE_EDUCATION_INFO = "viewEMPLOYEE_EDUCATION_INFO";
    public static final String[][] SEARCH_EMPLOYEE_EDUCATION_INFO = {
            {"" + LC.EMPLOYEE_EDUCATION_INFO_SEARCH_EDUCATIONLEVELTYPE, "education_level_type"},
            {"" + LC.EMPLOYEE_EDUCATION_INFO_SEARCH_DEGREEEXAMTYPE, "degree_exam_type"},
            {"" + LC.EMPLOYEE_EDUCATION_INFO_SEARCH_RESULTEXAMTYPE, "result_exam_type"},
            {"" + LC.EMPLOYEE_EDUCATION_INFO_SEARCH_BOARDTYPE, "board_type"},
//		{ ""+LC.EMPLOYEE_EDUCATION_INFO_SEARCH_EMPLOYEE_RECORD_ID, "employee_records_id" },
            {"" + LC.EMPLOYEE_EDUCATION_INFO_SEARCH_ANYFIELD, "AnyField"}
    };

    public final static String NAV_MEDICAL_EQUIPMENT_NAME = "navMEDICAL_EQUIPMENT_NAME";
    public final static String VIEW_MEDICAL_EQUIPMENT_NAME = "viewMEDICAL_EQUIPMENT_NAME";
    public static final String[][] SEARCH_MEDICAL_EQUIPMENT_NAME = {
            {"" + LC.MEDICAL_EQUIPMENT_NAME_SEARCH_NAMEEN, "name_en"},
            {"" + LC.MEDICAL_EQUIPMENT_NAME_SEARCH_NAMEBN, "name_bn"},
            {"" + LC.MEDICAL_EQUIPMENT_NAME_SEARCH_QUANTITYUNIT, "quantity_unit"},
            {"" + LC.MEDICAL_EQUIPMENT_NAME_SEARCH_CURRENTSTOCK, "current_stock"},
            {"" + LC.MEDICAL_EQUIPMENT_NAME_SEARCH_STOCKALERTREQUIRED, "stock_alert_required"},
            {"" + LC.MEDICAL_EQUIPMENT_NAME_SEARCH_STOCKALERTQUANTITY, "stock_alert_quantity"},
            {"" + LC.MEDICAL_EQUIPMENT_NAME_SEARCH_INSERTEDBYUSERID, "inserted_by_user_id"},
            {"" + LC.MEDICAL_EQUIPMENT_NAME_SEARCH_INSERTEDBYORGANOGRAMID, "inserted_by_organogram_id"},
            {"" + LC.MEDICAL_EQUIPMENT_NAME_SEARCH_INSERTIONDATE, "insertion_date"},
            {"" + LC.MEDICAL_EQUIPMENT_NAME_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_MEDICAL_INVENTORY_LOT = "navMEDICAL_INVENTORY_LOT";
    public final static String VIEW_MEDICAL_INVENTORY_LOT = "viewMEDICAL_INVENTORY_LOT";
    public static final String[][] SEARCH_MEDICAL_INVENTORY_LOT = {
            {"" + LC.MEDICAL_INVENTORY_LOT_SEARCH_PARLIAMENTSUPPLIERTYPE, "parliament_supplier_type"},
            {"" + LC.MEDICAL_INVENTORY_LOT_SEARCH_RELATEDPOID, "related_po_id"},
            {"" + LC.MEDICAL_INVENTORY_LOT_SEARCH_STOCKLOTNUMBER, "stock_lot_number"},
            {"" + LC.MEDICAL_INVENTORY_LOT_SEARCH_REMARKS, "remarks"},
            {"" + LC.MEDICAL_INVENTORY_LOT_SEARCH_INSERTEDBYUSERID, "inserted_by_user_id"},
            {"" + LC.MEDICAL_INVENTORY_LOT_SEARCH_INSERTEDBYORGANOGRAMID, "inserted_by_organogram_id"},
            {"" + LC.MEDICAL_INVENTORY_LOT_SEARCH_INSERTIONDATE, "insertion_date"},
            {"" + LC.MEDICAL_INVENTORY_LOT_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_MEDICAL_INVENTORY_OUT = "navMEDICAL_INVENTORY_OUT";
    public final static String VIEW_MEDICAL_INVENTORY_OUT = "viewMEDICAL_INVENTORY_OUT";
    public static final String[][] SEARCH_MEDICAL_INVENTORY_OUT = {
            {"" + LC.MEDICAL_INVENTORY_OUT_SEARCH_MEDICALITEMCAT, "medical_item_cat"},
            {"" + LC.MEDICAL_INVENTORY_OUT_SEARCH_DRUGINFORMATIONID, "drug_information_id"},
            {"" + LC.MEDICAL_INVENTORY_OUT_SEARCH_MEDICALREAGENTNAMEID, "medical_reagent_name_id"},
            {"" + LC.MEDICAL_INVENTORY_OUT_SEARCH_MEDICALEQUIPMENTNAMEID, "medical_equipment_name_id"},
            {"" + LC.MEDICAL_INVENTORY_OUT_SEARCH_STOCKOUTQUANTITY, "stock_out_quantity"},
            {"" + LC.MEDICAL_INVENTORY_OUT_SEARCH_UNITPRICE, "unit_price"},
            {"" + LC.MEDICAL_INVENTORY_OUT_SEARCH_TRANSACTIONDATE, "transaction_date"},
            {"" + LC.MEDICAL_INVENTORY_OUT_SEARCH_REMARKS, "remarks"},
            {"" + LC.MEDICAL_INVENTORY_OUT_SEARCH_INSERTEDBYUSERID, "inserted_by_user_id"},
            {"" + LC.MEDICAL_INVENTORY_OUT_SEARCH_INSERTEDBYORGANOGRAMID, "inserted_by_organogram_id"},
            {"" + LC.MEDICAL_INVENTORY_OUT_SEARCH_INSERTIONDATE, "insertion_date"},
            {"" + LC.MEDICAL_INVENTORY_OUT_SEARCH_EXPIRYDATE, "expiry_date"},
            {"" + LC.MEDICAL_INVENTORY_OUT_SEARCH_MEDICALINVENTORYINID, "medical_inventory_in_id"},
            {"" + LC.MEDICAL_INVENTORY_OUT_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_MEDICAL_REQUISITION_LOT = "navMEDICAL_REQUISITION_LOT";
    public final static String VIEW_MEDICAL_REQUISITION_LOT = "viewMEDICAL_REQUISITION_LOT";
    public static final String[][] SEARCH_MEDICAL_REQUISITION_LOT = {
            {"" + LC.MEDICAL_REQUISITION_LOT_SEARCH_REQUISITIONTOORGANOGRAMID, "requisition_to_organogram_id"},
            {"" + LC.MEDICAL_REQUISITION_LOT_SEARCH_SUBJECT, "subject"},
            {"" + LC.MEDICAL_REQUISITION_LOT_SEARCH_DESCRIPTION, "description"},
            {"" + LC.MEDICAL_REQUISITION_LOT_SEARCH_INSERTEDBYUSERID, "inserted_by_user_id"},
            {"" + LC.MEDICAL_REQUISITION_LOT_SEARCH_INSERTEDBYORGANOGRAMID, "inserted_by_organogram_id"},
            {"" + LC.MEDICAL_REQUISITION_LOT_SEARCH_INSERTIONDATE, "insertion_date"},
            {"" + LC.MEDICAL_REQUISITION_LOT_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_EMPLOYEE_HONORS_AWARDS = "navEMPLOYEE_HONORS_AWARDS";
    public final static String VIEW_EMPLOYEE_HONORS_AWARDS = "viewEMPLOYEE_HONORS_AWARDS";
    public static final String[][] SEARCH_EMPLOYEE_HONORS_AWARDS = {
            {"" + LC.EMPLOYEE_HONORS_AWARDS_SEARCH_AWARDTYPECAT, "award_type_cat"},
            {"" + LC.EMPLOYEE_HONORS_AWARDS_SEARCH_TITLESOFAWARD, "titles_of_award"},
            {"" + LC.EMPLOYEE_HONORS_AWARDS_SEARCH_AWARDINGINSTITUTION, "awarding_institution"},
            {"" + LC.EMPLOYEE_HONORS_AWARDS_SEARCH_AWARDDATE, "award_date"},
            {"" + LC.EMPLOYEE_HONORS_AWARDS_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_EMPLOYEE_LANGUAGE_PROFICIENCY = "navEMPLOYEE_LANGUAGE_PROFICIENCY";
    public final static String VIEW_EMPLOYEE_LANGUAGE_PROFICIENCY = "viewEMPLOYEE_LANGUAGE_PROFICIENCY";
    public static final String[][] SEARCH_EMPLOYEE_LANGUAGE_PROFICIENCY = {
            {"" + LC.EMPLOYEE_LANGUAGE_PROFICIENCY_SEARCH_EMPLOYEERECORDSID, "employee_records_id"},
            {"" + LC.EMPLOYEE_LANGUAGE_PROFICIENCY_SEARCH_LANGUAGECAT, "language_cat"},
            {"" + LC.EMPLOYEE_LANGUAGE_PROFICIENCY_SEARCH_READSKILLCAT, "read_skill_cat"},
            {"" + LC.EMPLOYEE_LANGUAGE_PROFICIENCY_SEARCH_WRITESKILLCAT, "write_skill_cat"},
            {"" + LC.EMPLOYEE_LANGUAGE_PROFICIENCY_SEARCH_SPEAKSKILLCAT, "speak_skill_cat"},
            {"" + LC.EMPLOYEE_LANGUAGE_PROFICIENCY_SEARCH_INSERTIONDATE, "insertion_date"},
            {"" + LC.EMPLOYEE_LANGUAGE_PROFICIENCY_SEARCH_INSERTEDBY, "inserted_by"},
            {"" + LC.EMPLOYEE_LANGUAGE_PROFICIENCY_SEARCH_MODIFIEDBY, "modified_by"},
            {"" + LC.EMPLOYEE_LANGUAGE_PROFICIENCY_SEARCH_ANYFIELD, "AnyField"}
    };
    public final static String NAV_EMPLOYEE_PUBLICATION = "navEMPLOYEE_PUBLICATION";
    public final static String VIEW_EMPLOYEE_PUBLICATION = "viewEMPLOYEE_PUBLICATION";
    public static final String[][] SEARCH_EMPLOYEE_PUBLICATION = {
            {"" + LC.EMPLOYEE_PUBLICATION_SEARCH_EMPLOYEERECORDSID, "employee_records_id"},
            {"" + LC.EMPLOYEE_PUBLICATION_SEARCH_PUBLICATIONCAT, "publication_cat"},
            {"" + LC.EMPLOYEE_PUBLICATION_SEARCH_CONFERENCECAT, "conference_cat"},
            {"" + LC.EMPLOYEE_PUBLICATION_SEARCH_GEOCOUNTRIESTYPE, "geo_countries_type"},
            {"" + LC.EMPLOYEE_PUBLICATION_SEARCH_PUBLICATIONDATE, "publication_date"},
            {"" + LC.EMPLOYEE_PUBLICATION_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_OTHER_OFFICE = "navOTHER_OFFICE";
    public final static String VIEW_OTHER_OFFICE = "viewOTHER_OFFICE";
    public static final String[][] SEARCH_OTHER_OFFICE = {
            {"" + LC.OTHER_OFFICE_SEARCH_NAMEEN, "name_en"},
            {"" + LC.OTHER_OFFICE_SEARCH_NAMEBN, "name_bn"},
            {"" + LC.OTHER_OFFICE_SEARCH_ANYFIELD, "AnyField"}
    };

    public final static String NAV_BANK_NAME = "navBANK_NAME";
    public final static String VIEW_BANK_NAME = "viewBANK_NAME";
    public static final String[][] SEARCH_BANK_NAME = {
            {"" + LC.BANK_NAME_SEARCH_NAMEEN, "name_en"},
            {"" + LC.BANK_NAME_SEARCH_NAMEBN, "name_bn"},
            {"" + LC.BANK_NAME_SEARCH_ANYFIELD, "AnyField"}
    };

    public final static String NAV_DESIGNATIONS = "navDESIGNATIONS";
    public final static String VIEW_DESIGNATIONS = "viewDESIGNATIONS";


    public final static String NAV_EMPLOYEE_WORK_AREA = "navEMPLOYEE_WORK_AREA";
    public final static String VIEW_EMPLOYEE_WORK_AREA = "viewEMPLOYEE_WORK_AREA";
    public static final String[][] SEARCH_EMPLOYEE_WORK_AREA = {
            {"" + LC.EMPLOYEE_WORK_AREA_SEARCH_NAMEEN, "name_en"},
            {"" + LC.EMPLOYEE_WORK_AREA_SEARCH_NAMEBN, "name_bn"},
            {"" + LC.EMPLOYEE_WORK_AREA_SEARCH_DESCRIPTION, "description"},
            {"" + LC.EMPLOYEE_WORK_AREA_SEARCH_ANYFIELD, "AnyField"}
    };

    public final static String NAV_INCIDENT = "navINCIDENT";
    public final static String VIEW_INCIDENT = "viewINCIDENT";
    public static final String[][] SEARCH_INCIDENT = {
            {"" + LC.INCIDENT_SEARCH_NAMEBN, "name_bn"},
            {"" + LC.INCIDENT_SEARCH_NAMEEN, "name_en"},
            {"" + LC.INCIDENT_SEARCH_ANYFIELD, "AnyField"}

    };


    public final static String NAV_EMPLOYEE_BANK_INFORMATION = "navEMPLOYEE_BANK_INFORMATION";
    public final static String VIEW_EMPLOYEE_BANK_INFORMATION = "viewEMPLOYEE_BANK_INFORMATION";
    public static final String[][] SEARCH_EMPLOYEE_BANK_INFORMATION = {
            {"" + LC.EMPLOYEE_BANK_INFORMATION_SEARCH_EMPLOYEERECORDSID, "employee_records_id"},
            {"" + LC.EMPLOYEE_BANK_INFORMATION_SEARCH_BANKNAMETYPE, "bank_name_type"},
            {"" + LC.EMPLOYEE_BANK_INFORMATION_SEARCH_BRANCHNAME, "branch_name"},
            {"" + LC.EMPLOYEE_BANK_INFORMATION_SEARCH_ACCOUNTCAT, "account_cat"},
            {"" + LC.EMPLOYEE_BANK_INFORMATION_SEARCH_BANKACCOUNTNUMBER, "bank_account_number"},
            {"" + LC.EMPLOYEE_BANK_INFORMATION_SEARCH_ANYFIELD, "AnyField"}
    };

    public final static String NAV_EMPLOYEE_EMPLOYMENT = "navEMPLOYEE_EMPLOYMENT";
    public final static String VIEW_EMPLOYEE_EMPLOYMENT = "viewEMPLOYEE_EMPLOYMENT";
    public static final String[][] SEARCH_EMPLOYEE_EMPLOYMENT = {
            {"" + LC.EMPLOYEE_EMPLOYMENT_SEARCH_NAMEEN, "name_en"},
            {"" + LC.EMPLOYEE_EMPLOYMENT_SEARCH_NAMEBN, "name_bn"},
            {"" + LC.EMPLOYEE_EMPLOYMENT_SEARCH_DESCRIPTION, "description"},
            {"" + LC.EMPLOYEE_EMPLOYMENT_SEARCH_ANYFIELD, "AnyField"}
    };
    public final static String NAV_DISCIPLINARY_LOG = "navDISCIPLINARY_LOG";
    public final static String VIEW_DISCIPLINARY_LOG = "viewDISCIPLINARY_LOG";
    public static final String[][] SEARCH_DISCIPLINARY_LOG = {
            {"" + LC.DISCIPLINARY_LOG_SEARCH_COMPLAINTSOURCECAT, "complaint_source_cat"},
            {"" + LC.DISCIPLINARY_LOG_SEARCH_REPORTEDDATE, "reported_date"},
            {"" + LC.DISCIPLINARY_LOG_SEARCH_INCIDENTTYPE, "incident_type"},
            {"" + LC.DISCIPLINARY_LOG_SEARCH_COMPLAINTSTATUSCAT, "complaint_status_cat"},
            {"" + LC.DISCIPLINARY_LOG_SEARCH_COMPLAINTRESOLVEDDATE, "complaint_resolved_date"},
            {"" + LC.DISCIPLINARY_LOG_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_EMPLOYEE_SERVICE_HISTORY = "navEMPLOYEE_SERVICE_HISTORY";
    public final static String VIEW_EMPLOYEE_SERVICE_HISTORY = "viewEMPLOYEE_SERVICE_HISTORY";
    public static final String[][] SEARCH_EMPLOYEE_SERVICE_HISTORY = {
            {"" + LC.EMPLOYEE_SERVICE_HISTORY_SEARCH_DESIGNATION, "designation"},
            {"" + LC.EMPLOYEE_SERVICE_HISTORY_SEARCH_DEPARTMENT, "department"},
            {"" + LC.EMPLOYEE_SERVICE_HISTORY_SEARCH_ISGOVTJOB, "is_govt_job"},
            {"" + LC.EMPLOYEE_SERVICE_HISTORY_SEARCH_NAMEOFEMPLOYEE, "name_of_employee"},
            {"" + LC.EMPLOYEE_SERVICE_HISTORY_SEARCH_EMPLOYEEEMPLOYMENTTYPE, "employee_employment_type"},
            {"" + LC.EMPLOYEE_SERVICE_HISTORY_SEARCH_SERVINGFROM, "serving_from"},
            {"" + LC.EMPLOYEE_SERVICE_HISTORY_SEARCH_SERVINGTO, "serving_to"},
            {"" + LC.EMPLOYEE_SERVICE_HISTORY_SEARCH_ISCURRENTLYWORKING, "is_currently_working"},
            {"" + LC.EMPLOYEE_SERVICE_HISTORY_SEARCH_JOBRESPONSIBILITY, "job_responsibility"},
            {"" + LC.EMPLOYEE_SERVICE_HISTORY_SEARCH_EMPLOYEEWORKAREATYPE, "employee_work_area_type"},
            {"" + LC.EMPLOYEE_SERVICE_HISTORY_SEARCH_SERVICETYPE, "service_type"},
            {"" + LC.EMPLOYEE_SERVICE_HISTORY_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_EMPLOYEE_NOMINEE = "navEMPLOYEE_NOMINEE";
    public final static String VIEW_EMPLOYEE_NOMINEE = "viewEMPLOYEE_NOMINEE";
    public static final String[][] SEARCH_EMPLOYEE_NOMINEE = {
            {"" + LC.EMPLOYEE_NOMINEE_SEARCH_EMPLOYEERECORDSID, "employee_records_id"},
            {"" + LC.EMPLOYEE_NOMINEE_SEARCH_EMPLOYEEFAMILYINFOID, "employee_family_info_id"},
            {"" + LC.EMPLOYEE_NOMINEE_SEARCH_NOMINEECAT, "nominee_cat"},
            {"" + LC.EMPLOYEE_NOMINEE_SEARCH_PERCENTAGE, "percentage"},
            {"" + LC.EMPLOYEE_NOMINEE_SEARCH_ANYFIELD, "AnyField"}
    };
    public final static String NAV_COMPLAIN_ACTION = "navCOMPLAIN_ACTION";
    public final static String VIEW_COMPLAIN_ACTION = "viewCOMPLAIN_ACTION";
    public static final String[][] SEARCH_COMPLAIN_ACTION = {
            {"" + LC.COMPLAIN_ACTION_SEARCH_NAMEBN, "name_bn"},
            {"" + LC.COMPLAIN_ACTION_SEARCH_NAMEEN, "name_en"},
            {"" + LC.COMPLAIN_ACTION_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_DISCIPLINARY_DETAILS = "navDISCIPLINARY_DETAILS";
    public final static String VIEW_DISCIPLINARY_DETAILS = "viewDISCIPLINARY_DETAILS";
    public static final String[][] SEARCH_DISCIPLINARY_DETAILS = {
            {"" + LC.DISCIPLINARY_DETAILS_SEARCH_DISCIPLINARYLOGID, "disciplinary_log_id"},
            {"" + LC.DISCIPLINARY_DETAILS_SEARCH_EMPLOYEERECORDSID, "employee_records_id"},
            {"" + LC.DISCIPLINARY_DETAILS_SEARCH_SUSPENSIONDAYS, "suspension_days"},
            {"" + LC.DISCIPLINARY_DETAILS_SEARCH_COMPLAINACTIONTYPE, "complain_action_type"},
            {"" + LC.DISCIPLINARY_DETAILS_SEARCH_COMPLIANOTHER, "complian_other"},
            {"" + LC.DISCIPLINARY_DETAILS_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_TIME_PERMISSION_REQUEST = "navTIME_PERMISSION_REQUEST";
    public final static String VIEW_TIME_PERMISSION_REQUEST = "viewTIME_PERMISSION_REQUEST";
    public static final String[][] SEARCH_TIME_PERMISSION_REQUEST = {
            {"" + LC.TIME_PERMISSION_REQUEST_SEARCH_EMPLOYEERECORDSID, "employee_records_id"},
            {"" + LC.TIME_PERMISSION_REQUEST_SEARCH_TIMEREQUESTCAT, "time_request_cat"},
            {"" + LC.TIME_PERMISSION_REQUEST_SEARCH_REASONTYPECAT, "reason_type_cat"},
            {"" + LC.TIME_PERMISSION_REQUEST_SEARCH_ISAPPROVED, "is_approved"},
            {"" + LC.TIME_PERMISSION_REQUEST_SEARCH_APPROVALDATE, "approval_date"},
            {"" + LC.TIME_PERMISSION_REQUEST_SEARCH_INSERTEDBY, "inserted_by"},
            {"" + LC.TIME_PERMISSION_REQUEST_SEARCH_MODIFIEDBY, "modified_by"},
            {"" + LC.TIME_PERMISSION_REQUEST_SEARCH_ANYFIELD, "AnyField"}
    };

    public final static String NAV_EMPLOYEE_LEAVE_DETAILS = "navEMPLOYEE_LEAVE_DETAILS";
    public final static String VIEW_EMPLOYEE_LEAVE_DETAILS = "viewEMPLOYEE_LEAVE_DETAILS";
    public static final String[][] SEARCH_EMPLOYEE_LEAVE_DETAILS = {
            {"" + LC.EMPLOYEE_LEAVE_DETAILS_SEARCH_EMPLOYEERECORDSID, "employee_records_id"},
            {"" + LC.EMPLOYEE_LEAVE_DETAILS_SEARCH_LEAVETYPECAT, "leave_type_cat"},
            {"" + LC.EMPLOYEE_LEAVE_DETAILS_SEARCH_LEAVESTARTDATE, "leave_start_date"},
            {"" + LC.EMPLOYEE_LEAVE_DETAILS_SEARCH_LEAVEENDDATE, "leave_end_date"},
            {"" + LC.EMPLOYEE_LEAVE_DETAILS_SEARCH_ANYFIELD, "AnyField"}

    };


    public final static String NAV_EMPLOYEE_OFFICE_HOUR = "navEMPLOYEE_OFFICE_HOUR";
    public final static String VIEW_EMPLOYEE_OFFICE_HOUR = "viewEMPLOYEE_OFFICE_HOUR";
    public static final String[][] SEARCH_EMPLOYEE_OFFICE_HOUR = {
            {"" + LC.EMPLOYEE_OFFICE_HOUR_SEARCH_OFFICEHOURCAT, "office_hour_cat"},
            {"" + LC.EMPLOYEE_OFFICE_HOUR_SEARCH_STARTTIME, "start_time"},
            {"" + LC.EMPLOYEE_OFFICE_HOUR_SEARCH_ENDTIME, "end_time"},
            {"" + LC.EMPLOYEE_OFFICE_HOUR_SEARCH_STARTDATE, "start_date"},
            {"" + LC.EMPLOYEE_OFFICE_HOUR_SEARCH_ENDDATE, "end_date"},
            {"" + LC.EMPLOYEE_OFFICE_HOUR_SEARCH_ISACTIVE, "is_active"},
            {"" + LC.EMPLOYEE_OFFICE_HOUR_SEARCH_INSERTEDBY, "inserted_by"},
            {"" + LC.EMPLOYEE_OFFICE_HOUR_SEARCH_MODIFIEDBY, "modified_by"},
            {"" + LC.EMPLOYEE_OFFICE_HOUR_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_COMMON_LAB_REPORT = "navCOMMON_LAB_REPORT";
    public final static String VIEW_COMMON_LAB_REPORT = "viewCOMMON_LAB_REPORT";
    public static final String[][] SEARCH_COMMON_LAB_REPORT = {
            {"" + LC.COMMON_LAB_REPORT_SEARCH_PRESCRIPTIONLABTESTID, "prescription_lab_test_id"},
            {"" + LC.COMMON_LAB_REPORT_SEARCH_LABTESTID, "lab_test_id"},
            {"" + LC.COMMON_LAB_REPORT_SEARCH_NAME, "name"},
            {"" + LC.COMMON_LAB_REPORT_SEARCH_AGE, "age"},
            {"" + LC.COMMON_LAB_REPORT_SEARCH_REFERREDBY, "referred_by"},
            {"" + LC.COMMON_LAB_REPORT_SEARCH_SPECIMEN, "specimen"},
            {"" + LC.COMMON_LAB_REPORT_SEARCH_INSERTEDBYUSERID, "inserted_by_user_id"},
            {"" + LC.COMMON_LAB_REPORT_SEARCH_INSERTEDBYORGANOGRAMID, "inserted_by_organogram_id"},
            {"" + LC.COMMON_LAB_REPORT_SEARCH_INSERTIONDATE, "insertion_date"},
            {"" + LC.COMMON_LAB_REPORT_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_URINE_EXAMINATION_REPORT = "navURINE_EXAMINATION_REPORT";
    public final static String VIEW_URINE_EXAMINATION_REPORT = "viewURINE_EXAMINATION_REPORT";
    public static final String[][] SEARCH_URINE_EXAMINATION_REPORT = {
            {"" + LC.URINE_EXAMINATION_REPORT_SEARCH_COMMONLABREPORTID, "common_lab_report_id"},
            {"" + LC.URINE_EXAMINATION_REPORT_SEARCH_QUANTITY, "quantity"},
            {"" + LC.URINE_EXAMINATION_REPORT_SEARCH_COLOR, "color"},
            {"" + LC.URINE_EXAMINATION_REPORT_SEARCH_REACTION, "reaction"},
            {"" + LC.URINE_EXAMINATION_REPORT_SEARCH_GRAVITY, "gravity"},
            {"" + LC.URINE_EXAMINATION_REPORT_SEARCH_ALBUMIN, "albumin"},
            {"" + LC.URINE_EXAMINATION_REPORT_SEARCH_SUGAR, "sugar"},
            {"" + LC.URINE_EXAMINATION_REPORT_SEARCH_BILESALTS, "bile_salts"},
            {"" + LC.URINE_EXAMINATION_REPORT_SEARCH_PUSSCELLS, "puss_cells"},
            {"" + LC.URINE_EXAMINATION_REPORT_SEARCH_EPCELLS, "ep_cells"},
            {"" + LC.URINE_EXAMINATION_REPORT_SEARCH_RBC, "rbc"},
            {"" + LC.URINE_EXAMINATION_REPORT_SEARCH_RBCCAST, "rbc_cast"},
            {"" + LC.URINE_EXAMINATION_REPORT_SEARCH_CRYSTALS, "crystals"},
            {"" + LC.URINE_EXAMINATION_REPORT_SEARCH_OTHERTHINGS, "other_things"},
            {"" + LC.URINE_EXAMINATION_REPORT_SEARCH_INSERTEDBYUSERID, "inserted_by_user_id"},
            {"" + LC.URINE_EXAMINATION_REPORT_SEARCH_INSERTEDBYORGANOGRAMID, "inserted_by_organogram_id"},
            {"" + LC.URINE_EXAMINATION_REPORT_SEARCH_INSERTIONDATE, "insertion_date"},
            {"" + LC.URINE_EXAMINATION_REPORT_SEARCH_ANYFIELD, "AnyField"}
    };
    public final static String NAV_LEAVE_TYPE_ENTITLEMENT = "navLEAVE_TYPE_ENTITLEMENT";
    public final static String VIEW_LEAVE_TYPE_ENTITLEMENT = "viewLEAVE_TYPE_ENTITLEMENT";
    public static final String[][] SEARCH_LEAVE_TYPE_ENTITLEMENT = {
            {"" + LC.LEAVE_TYPE_ENTITLEMENT_SEARCH_LEAVETYPECAT, "leave_type_cat"},
            {"" + LC.LEAVE_TYPE_ENTITLEMENT_SEARCH_NUMBEROFDAYS, "number_of_days"},
            {"" + LC.LEAVE_TYPE_ENTITLEMENT_SEARCH_FREQUENCY, "frequency"},
            {"" + LC.LEAVE_TYPE_ENTITLEMENT_SEARCH_MINIMUMJOBDURATION, "minimum_job_duration"},
            {"" + LC.LEAVE_TYPE_ENTITLEMENT_SEARCH_STARTDATE, "start_date"},
            {"" + LC.LEAVE_TYPE_ENTITLEMENT_SEARCH_ENDDATE, "end_date"},
            {"" + LC.LEAVE_TYPE_ENTITLEMENT_SEARCH_ANYFIELD, "AnyField"}
    };

    public static String SUCCESS_MESSAGE = "success_message";
    public static String ERROR_MESSAGE = "error_message";


    public final static String NAV_EMP_TRAVEL_DETAILS = "navEMP_TRAVEL_DETAILS";
    public final static String VIEW_EMP_TRAVEL_DETAILS = "viewEMP_TRAVEL_DETAILS";
    public static final String[][] SEARCH_EMP_TRAVEL_DETAILS = {
            {"" + LC.EMP_TRAVEL_DETAILS_SEARCH_TRAVELTYPECAT, "travel_type_cat"},
            {"" + LC.EMP_TRAVEL_DETAILS_SEARCH_MODELOFTRAVELCAT, "model_of_travel_cat"},
            {"" + LC.EMP_TRAVEL_DETAILS_SEARCH_TRAVELPURPOSECAT, "travel_purpose_cat"},
            {"" + LC.EMP_TRAVEL_DETAILS_SEARCH_TRAVELDESTINATION, "travel_destination"},
            {"" + LC.EMP_TRAVEL_DETAILS_SEARCH_TRANSPORTFACILITYCAT, "transport_facility_cat"},
            {"" + LC.EMP_TRAVEL_DETAILS_SEARCH_DEPARTUREDATE, "departure_date"},
            {"" + LC.EMP_TRAVEL_DETAILS_SEARCH_RETURNDATE, "return_date"},
            {"" + LC.EMP_TRAVEL_DETAILS_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_TRAINING_CALENDER = "navTRAINING_CALENDER";
    public final static String VIEW_TRAINING_CALENDER = "viewTRAINING_CALENDER";
    public static final String[][] SEARCH_TRAINING_CALENDER = {
            {"" + LC.TRAINING_CALENDER_SEARCH_NAMEEN, "name_en"},
            {"" + LC.TRAINING_CALENDER_SEARCH_NAMEBN, "name_bn"},
            {"" + LC.TRAINING_CALENDER_SEARCH_TRAININGTYPECAT, "training_type_cat"},
            {"" + LC.TRAINING_CALENDER_SEARCH_INSTITUTENAME, "institute_name"},
            {"" + LC.TRAINING_CALENDER_SEARCH_STARTDATE, "start_date"},
            {"" + LC.TRAINING_CALENDER_SEARCH_ENDDATE, "end_date"},
            {"" + LC.TRAINING_CALENDER_SEARCH_STARTTIME, "start_time"},
            {"" + LC.TRAINING_CALENDER_SEARCH_ENDTIME, "end_time"},
            {"" + LC.TRAINING_CALENDER_SEARCH_ARRANGEDBY, "arranged_by"},
            {"" + LC.TRAINING_CALENDER_SEARCH_TRAININGMODECAT, "training_mode_cat"},
            {"" + LC.TRAINING_CALENDER_SEARCH_ISFOREIGNTRAINING, "is_foreign_training"},
            {"" + LC.TRAINING_CALENDER_SEARCH_VENUE, "venue"},
            {"" + LC.TRAINING_CALENDER_SEARCH_MAXNUMBEROFPARTICIPANTS, "max_number_of_participants"},
            {"" + LC.TRAINING_CALENDER_SEARCH_ISENROLLMENTAPPLICALE, "is_enrollment_applicale"},
            {"" + LC.TRAINING_CALENDER_SEARCH_LASTENROLLMENTDATE, "last_enrollment_date"},
            {"" + LC.TRAINING_CALENDER_SEARCH_TARGETGROUPTYPE, "target_group_type"},
            {"" + LC.TRAINING_CALENDER_SEARCH_TRAININGOBJECTIVE, "training_objective"},
            {"" + LC.TRAINING_CALENDER_SEARCH_TRAININGPREREQUISITE, "training_prerequisite"},
            {"" + LC.TRAINING_CALENDER_SEARCH_ISBONDAPPLICABLE, "is_bond_applicable"},
            {"" + LC.TRAINING_CALENDER_SEARCH_YEAROFBOND, "year_of_bond"},
            {"" + LC.TRAINING_CALENDER_SEARCH_INSERTIONDATE, "insertion_date"},
            {"" + LC.TRAINING_CALENDER_SEARCH_INSERTEDBYUSERID, "inserted_by_user_id"},
            {"" + LC.TRAINING_CALENDER_SEARCH_MODIFIEDBYID, "modified_by_id"},
            {"" + LC.TRAINING_CALENDER_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_EMP_TRAINING_DETAILS = "navEMP_TRAINING_DETAILS";
    public final static String VIEW_EMP_TRAINING_DETAILS = "viewEMP_TRAINING_DETAILS";
    public static final String[][] SEARCH_EMP_TRAINING_DETAILS = {
            {"" + LC.EMP_TRAINING_DETAILS_SEARCH_EMPLOYEERECORDSID, "employee_records_id"},
            {"" + LC.EMP_TRAINING_DETAILS_SEARCH_TRAININGCALENDARTYPE, "training_calendar_type"},
            {"" + LC.EMP_TRAINING_DETAILS_SEARCH_GRADE, "grade"},
            {"" + LC.EMP_TRAINING_DETAILS_SEARCH_POSITION, "position"},
            {"" + LC.EMP_TRAINING_DETAILS_SEARCH_POSITION, "office_units_id"},
            {"" + LC.EMP_TRAINING_DETAILS_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_EMPLOYEE_CERTIFICATION = "navEMPLOYEE_CERTIFICATION";
    public final static String VIEW_EMPLOYEE_CERTIFICATION = "viewEMPLOYEE_CERTIFICATION";
    public static final String[][] SEARCH_EMPLOYEE_CERTIFICATION = {
            {"" + LC.EMPLOYEE_CERTIFICATION_SEARCH_CERTIFICATIONNAME, "certification_name"},
            {"" + LC.EMPLOYEE_CERTIFICATION_SEARCH_ISSUINGORGANIZATION, "issuing_organization"},
            {"" + LC.EMPLOYEE_CERTIFICATION_SEARCH_ISSUINGINSTITUTEADDRESS, "issuing_institute_address"},
            {"" + LC.EMPLOYEE_CERTIFICATION_SEARCH_VALIDFROM, "valid_from"},
            {"" + LC.EMPLOYEE_CERTIFICATION_SEARCH_VALIDUPTO, "valid_upto"},
            {"" + LC.EMPLOYEE_CERTIFICATION_SEARCH_CREDENTIALID, "credential_id"},
            {"" + LC.EMPLOYEE_CERTIFICATION_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_ID_CARD_REQUISITION = "navID_CARD_REQUISITION";
    public final static String VIEW_ID_CARD_REQUISITION = "viewID_CARD_REQUISITION";
    public static final String[][] SEARCH_ID_CARD_REQUISITION = {
            {"" + LC.ID_CARD_REQUISITION_SEARCH_EMPLOYEERECORDSID, "employee_records_id"},
            {"" + LC.ID_CARD_REQUISITION_SEARCH_JOBCAT, "job_cat"},
            {"" + LC.ID_CARD_REQUISITION_SEARCH_ISLOST, "is_lost"},
            {"" + LC.ID_CARD_REQUISITION_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_MEDICAL_REAGENT_NAME = "navMEDICAL_REAGENT_NAME";
    public final static String VIEW_MEDICAL_REAGENT_NAME = "viewMEDICAL_REAGENT_NAME";
    public static final String[][] SEARCH_MEDICAL_REAGENT_NAME = {
            {"" + LC.MEDICAL_REAGENT_NAME_SEARCH_NAMEEN, "name_en"},
            {"" + LC.MEDICAL_REAGENT_NAME_SEARCH_NAMEBN, "name_bn"},
            {"" + LC.MEDICAL_REAGENT_NAME_SEARCH_CURRENTSTOCK, "current_stock"},
            {"" + LC.MEDICAL_REAGENT_NAME_SEARCH_STOCKALERTREQUIRED, "stock_alert_required"},
            {"" + LC.MEDICAL_REAGENT_NAME_SEARCH_STOCKALERTQUANTITY, "stock_alert_quantity"},
            {"" + LC.MEDICAL_REAGENT_NAME_SEARCH_INSERTEDBYUSERID, "inserted_by_user_id"},
            {"" + LC.MEDICAL_REAGENT_NAME_SEARCH_INSERTEDBYORGANOGRAMID, "inserted_by_organogram_id"},
            {"" + LC.MEDICAL_REAGENT_NAME_SEARCH_INSERTIONDATE, "insertion_date"},
            {"" + LC.MEDICAL_REAGENT_NAME_SEARCH_DEPARTMENTCAT, "department_cat"},
            {"" + LC.MEDICAL_REAGENT_NAME_SEARCH_QUANTITYUNITCAT, "quantity_unit_cat"},
            {"" + LC.MEDICAL_REAGENT_NAME_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_REAGENT_REQUISITION_LOT = "navREAGENT_REQUISITION_LOT";
    public final static String VIEW_REAGENT_REQUISITION_LOT = "viewREAGENT_REQUISITION_LOT";
    public static final String[][] SEARCH_REAGENT_REQUISITION_LOT = {
            {"" + LC.REAGENT_REQUISITION_LOT_SEARCH_REQUISITIONTOORGANOGRAMID, "requisition_to_organogram_id"},
            {"" + LC.REAGENT_REQUISITION_LOT_SEARCH_SUBJECT, "subject"},
            {"" + LC.REAGENT_REQUISITION_LOT_SEARCH_DESCRIPTION, "description"},
            {"" + LC.REAGENT_REQUISITION_LOT_SEARCH_INSERTEDBYUSERID, "inserted_by_user_id"},
            {"" + LC.REAGENT_REQUISITION_LOT_SEARCH_INSERTEDBYORGANOGRAMID, "inserted_by_organogram_id"},
            {"" + LC.REAGENT_REQUISITION_LOT_SEARCH_INSERTIONDATE, "insertion_date"},
            {"" + LC.REAGENT_REQUISITION_LOT_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_VISITOR_PASS_REQUEST = "navVISITOR_PASS_REQUEST";
    public final static String VIEW_VISITOR_PASS_REQUEST = "viewVISITOR_PASS_REQUEST";
    public static final String[][] SEARCH_VISITOR_PASS_REQUEST = {
            {"" + LC.VISITOR_PASS_REQUEST_SEARCH_VISITDATE, "visit_date"},
            {"" + LC.VISITOR_PASS_REQUEST_SEARCH_REQUESTDATE, "request_date"},
            {"" + LC.VISITOR_PASS_REQUEST_SEARCH_ISKNOWN, "is_known"},
            {"" + LC.VISITOR_PASS_REQUEST_SEARCH_REFERENCEEMPLOYEERECORDSID, "reference_employee_records_id"},
            {"" + LC.VISITOR_PASS_REQUEST_SEARCH_ISAPPROVED, "is_approved"},
            {"" + LC.VISITOR_PASS_REQUEST_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_TICKET_FORWARD_HISTORY = "navTICKET_FORWARD_HISTORY";
    public final static String VIEW_TICKET_FORWARD_HISTORY = "viewTICKET_FORWARD_HISTORY";
    public static final String[][] SEARCH_TICKET_FORWARD_HISTORY = {
            {"" + LC.TICKET_FORWARD_HISTORY_SEARCH_SUPPORTTICKETID, "support_ticket_id"},
            {"" + LC.TICKET_FORWARD_HISTORY_SEARCH_FORWARDFROMUSERID, "forward_from_user_id"},
            {"" + LC.TICKET_FORWARD_HISTORY_SEARCH_FORWARDTOUSERID, "forward_to_user_id"},
            {"" + LC.TICKET_FORWARD_HISTORY_SEARCH_FORWARDDATE, "forward_date"},
            {"" + LC.TICKET_FORWARD_HISTORY_SEARCH_ANYFIELD, "AnyField"}
    };
    public final static String NAV_APPOINTMENT = "navAPPOINTMENT";
    public final static String VIEW_APPOINTMENT = "viewAPPOINTMENT";
    public static final String[][] SEARCH_APPOINTMENT = {
            {"" + LC.APPOINTMENT_SEARCH_VISITDATE, "visit_date"},
            {"" + LC.APPOINTMENT_SEARCH_SPECIALITYTYPE, "speciality_type"},
            {"" + LC.APPOINTMENT_SEARCH_DOCTORID, "doctor_id"},
            {"" + LC.APPOINTMENT_SEARCH_SHIFTCAT, "shift_cat"},
            {"" + LC.APPOINTMENT_SEARCH_AVAILABLETIMESLOT, "available_time_slot"},
            {"" + LC.APPOINTMENT_SEARCH_REMARKS, "remarks"},
            {"" + LC.APPOINTMENT_SEARCH_PATIENTID, "patient_id"},
            {"" + LC.APPOINTMENT_SEARCH_PATIENTNAME, "patient_name"},
            {"" + LC.APPOINTMENT_SEARCH_PHONENUMBER, "phone_number"},
            {"" + LC.APPOINTMENT_SEARCH_DATEOFBIRTH, "date_of_birth"},
            {"" + LC.APPOINTMENT_SEARCH_AGE, "age"},
            {"" + LC.APPOINTMENT_SEARCH_GENDERCAT, "gender_cat"},
            {"" + LC.APPOINTMENT_SEARCH_WHOISTHEPATIENTCAT, "who_is_the_patient_cat"},
            {"" + LC.APPOINTMENT_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_RECRUITMENT_JOB_REQUIRED_FILES = "navRECRUITMENT_JOB_REQUIRED_FILES";
    public final static String VIEW_RECRUITMENT_JOB_REQUIRED_FILES = "viewRECRUITMENT_JOB_REQUIRED_FILES";
    public static final String[][] SEARCH_RECRUITMENT_JOB_REQUIRED_FILES = {
            {"" + LC.RECRUITMENT_JOB_REQUIRED_FILES_SEARCH_NAMEBN, "name_bn"},
            {"" + LC.RECRUITMENT_JOB_REQUIRED_FILES_SEARCH_NAMEEN, "name_en"},
            {"" + LC.RECRUITMENT_JOB_REQUIRED_FILES_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_RECRUITMENT_JOB_REQ_TRAINS_AND_CERTS = "navRECRUITMENT_JOB_REQ_TRAINS_AND_CERTS";
    public final static String VIEW_RECRUITMENT_JOB_REQ_TRAINS_AND_CERTS = "viewRECRUITMENT_JOB_REQ_TRAINS_AND_CERTS";
    public static final String[][] SEARCH_RECRUITMENT_JOB_REQ_TRAINS_AND_CERTS = {
            {"" + LC.RECRUITMENT_JOB_REQ_TRAINS_AND_CERTS_SEARCH_NAMEBN, "name_bn"},
            {"" + LC.RECRUITMENT_JOB_REQ_TRAINS_AND_CERTS_SEARCH_NAMEEN, "name_en"},
            {"" + LC.RECRUITMENT_JOB_REQ_TRAINS_AND_CERTS_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_RECRUITMENT_JOB_DESCRIPTION = "navRECRUITMENT_JOB_DESCRIPTION";
    public final static String VIEW_RECRUITMENT_JOB_DESCRIPTION = "viewRECRUITMENT_JOB_DESCRIPTION";
    public static final String[][] SEARCH_RECRUITMENT_JOB_DESCRIPTION = {
            {"" + LC.RECRUITMENT_JOB_DESCRIPTION_SEARCH_JOBTITLEEN, "job_title_en"},
            {"" + LC.RECRUITMENT_JOB_DESCRIPTION_SEARCH_JOBTITLEBN, "job_title_bn"},
            {"" + LC.RECRUITMENT_JOB_DESCRIPTION_SEARCH_JOBGRADECAT, "job_grade_cat"},
            {"" + LC.RECRUITMENT_JOB_DESCRIPTION_SEARCH_EMPLOYMENTSTATUSCAT, "employment_status_cat"},
            {"" + LC.RECRUITMENT_JOB_DESCRIPTION_SEARCH_EMPLOYEEPAYSCALETYPE, "employee_pay_scale_type"},
            {"" + LC.RECRUITMENT_JOB_DESCRIPTION_SEARCH_JOBPURPOSE, "job_purpose"},
            {"" + LC.RECRUITMENT_JOB_DESCRIPTION_SEARCH_JOBRESPONSIBILITIES, "job_responsibilities"},
            {"" + LC.RECRUITMENT_JOB_DESCRIPTION_SEARCH_MINIMUMACADEMICQUALIFICATION, "minimum_academic_qualification"},
            {"" + LC.RECRUITMENT_JOB_DESCRIPTION_SEARCH_MINIMUMEXPERIENCEREQUIRED, "minimum_experience_required"},
            {"" + LC.RECRUITMENT_JOB_DESCRIPTION_SEARCH_REQUIREDCERTIFICATIONANDTRANING, "required_certification_and_traning"},
            {"" + LC.RECRUITMENT_JOB_DESCRIPTION_SEARCH_PERSONALCHARACTERISTICS, "personal_characteristics"},
            {"" + LC.RECRUITMENT_JOB_DESCRIPTION_SEARCH_REQUIREDCOMPETENCE, "required_competence"},
            {"" + LC.RECRUITMENT_JOB_DESCRIPTION_SEARCH_EDUCATIONLEVELTYPE, "education_level_type"},
            {"" + LC.RECRUITMENT_JOB_DESCRIPTION_SEARCH_MINEXPERIENCE, "min_experience"},
            {"" + LC.RECRUITMENT_JOB_DESCRIPTION_SEARCH_MAXAGEONBOUNDARYDATEREGULAR, "max_age_on_boundary_date_regular"},
            {"" + LC.RECRUITMENT_JOB_DESCRIPTION_SEARCH_MAXAGEONBOUNDARYDATEFF, "max_age_on_boundary_date_ff"},
            {"" + LC.RECRUITMENT_JOB_DESCRIPTION_SEARCH_JOBLOCATION, "job_location"},
            {"" + LC.RECRUITMENT_JOB_DESCRIPTION_SEARCH_NUMBEROFVACANCY, "number_of_vacancy"},
            {"" + LC.RECRUITMENT_JOB_DESCRIPTION_SEARCH_LASTAPPLICATIONDATE, "last_application_date"},
            {"" + LC.RECRUITMENT_JOB_DESCRIPTION_SEARCH_LASTAGECALCULATIONDATE, "last_age_calculation_date"},
            {"" + LC.RECRUITMENT_JOB_DESCRIPTION_SEARCH_ONLINEJOBPOSTINGDATE, "online_job_posting_date"},
            {"" + LC.RECRUITMENT_JOB_DESCRIPTION_SEARCH_ANYFIELD, "AnyField"}
    };

    public final static String NAV_EMPLOYEE_PAY_SCALE = "navEMPLOYEE_PAY_SCALE";
    public final static String VIEW_EMPLOYEE_PAY_SCALE = "viewEMPLOYEE_PAY_SCALE";
    public static final String[][] SEARCH_EMPLOYEE_PAY_SCALE = {
            {"" + LC.EMPLOYEE_PAY_SCALE_SEARCH_JOBGRADECAT, "job_grade_cat"},
            {"" + LC.EMPLOYEE_PAY_SCALE_SEARCH_PAYSCALESHORT, "pay_scale_short"},
            {"" + LC.EMPLOYEE_PAY_SCALE_SEARCH_PAYSCALEBRIEF, "pay_scale_brief"},
            {"" + LC.EMPLOYEE_PAY_SCALE_SEARCH_NATIONALPAYSCALECAT, "national_pay_scale_cat"},
            {"" + LC.EMPLOYEE_PAY_SCALE_SEARCH_ISACTIVE, "is_active"},
            {"" + LC.EMPLOYEE_PAY_SCALE_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_PARLIAMENT_JOB_APPLICANT = "navPARLIAMENT_JOB_APPLICANT";
    public final static String VIEW_PARLIAMENT_JOB_APPLICANT = "viewPARLIAMENT_JOB_APPLICANT";
    public static final String[][] SEARCH_PARLIAMENT_JOB_APPLICANT = {
            {"" + LC.PARLIAMENT_JOB_APPLICANT_SEARCH_NAMEBN, "name_bn"},
            {"" + LC.PARLIAMENT_JOB_APPLICANT_SEARCH_FATHERNAME, "father_name"},
            {"" + LC.PARLIAMENT_JOB_APPLICANT_SEARCH_MOTHERNAME, "mother_name"},
            {"" + LC.PARLIAMENT_JOB_APPLICANT_SEARCH_MARITALSTATUSCAT, "marital_status_cat"},
            {"" + LC.PARLIAMENT_JOB_APPLICANT_SEARCH_SPOUSENAME, "spouse_name"},
            {"" + LC.PARLIAMENT_JOB_APPLICANT_SEARCH_DATEOFBIRTH, "date_of_birth"},
            {"" + LC.PARLIAMENT_JOB_APPLICANT_SEARCH_GENDERCAT, "gender_cat"},
            {"" + LC.PARLIAMENT_JOB_APPLICANT_SEARCH_EMPLOYMENTSTATUSCAT, "employment_status_cat"},
            {"" + LC.PARLIAMENT_JOB_APPLICANT_SEARCH_ETHNICMINORITYCAT, "ethnic_minority_cat"},
            {"" + LC.PARLIAMENT_JOB_APPLICANT_SEARCH_FREEDOMFIGHTERCAT, "freedom_fighter_cat"},
            {"" + LC.PARLIAMENT_JOB_APPLICANT_SEARCH_NATIONALITYCAT, "nationality_cat"},
            {"" + LC.PARLIAMENT_JOB_APPLICANT_SEARCH_DISABILITYCAT, "disability_cat"},
            {"" + LC.PARLIAMENT_JOB_APPLICANT_SEARCH_SPECIALQUOTACAT, "special_quota_cat"},
            {"" + LC.PARLIAMENT_JOB_APPLICANT_SEARCH_HEIGHT, "height"},
            {"" + LC.PARLIAMENT_JOB_APPLICANT_SEARCH_WEIGHT, "weight"},
            {"" + LC.PARLIAMENT_JOB_APPLICANT_SEARCH_CHEST, "chest"},
            {"" + LC.PARLIAMENT_JOB_APPLICANT_SEARCH_IDENTIFICATIONTYPECAT, "identification_type_cat"},
            {"" + LC.PARLIAMENT_JOB_APPLICANT_SEARCH_IDENTIFICATIONNO, "identification_no"},
            {"" + LC.PARLIAMENT_JOB_APPLICANT_SEARCH_PERMANENTADDRESS, "permanent_address"},
            {"" + LC.PARLIAMENT_JOB_APPLICANT_SEARCH_PRESENTADDRESS, "present_address"},
            {"" + LC.PARLIAMENT_JOB_APPLICANT_SEARCH_HOMEDISTRICTNAME, "home_district_name"},
            {"" + LC.PARLIAMENT_JOB_APPLICANT_SEARCH_CONTACTNUMBER, "contact_number"},
            {"" + LC.PARLIAMENT_JOB_APPLICANT_SEARCH_EMAIL, "email"},
            {"" + LC.PARLIAMENT_JOB_APPLICANT_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_JOB_APPLICANT_EDUCATION_INFO = "navJOB_APPLICANT_EDUCATION_INFO";
    public final static String VIEW_JOB_APPLICANT_EDUCATION_INFO = "viewJOB_APPLICANT_EDUCATION_INFO";
    public static final String[][] SEARCH_JOB_APPLICANT_EDUCATION_INFO = {
            {"" + LC.JOB_APPLICANT_EDUCATION_INFO_SEARCH_EMPLOYEERECORDSID, "employee_records_id"},
            {"" + LC.JOB_APPLICANT_EDUCATION_INFO_SEARCH_EDUCATIONLEVELTYPE, "education_level_type"},
            {"" + LC.JOB_APPLICANT_EDUCATION_INFO_SEARCH_DEGREEEXAMTYPE, "degree_exam_type"},
            {"" + LC.JOB_APPLICANT_EDUCATION_INFO_SEARCH_OTHERDEGREEEXAM, "other_degree_exam"},
            {"" + LC.JOB_APPLICANT_EDUCATION_INFO_SEARCH_MAJOR, "major"},
            {"" + LC.JOB_APPLICANT_EDUCATION_INFO_SEARCH_RESULTEXAMTYPE, "result_exam_type"},
            {"" + LC.JOB_APPLICANT_EDUCATION_INFO_SEARCH_MARKSPERCENTAGE, "marks_percentage"},
            {"" + LC.JOB_APPLICANT_EDUCATION_INFO_SEARCH_GRADEPOINTCAT, "grade_point_cat"},
            {"" + LC.JOB_APPLICANT_EDUCATION_INFO_SEARCH_CGPANUMBER, "cgpa_number"},
            {"" + LC.JOB_APPLICANT_EDUCATION_INFO_SEARCH_YEAROFPASSINGNUMBER, "year_of_passing_number"},
            {"" + LC.JOB_APPLICANT_EDUCATION_INFO_SEARCH_BOARDTYPE, "board_type"},
            {"" + LC.JOB_APPLICANT_EDUCATION_INFO_SEARCH_INSTITUTIONNAME, "institution_name"},
            {"" + LC.JOB_APPLICANT_EDUCATION_INFO_SEARCH_ISFOREIGN, "is_foreign"},
            {"" + LC.JOB_APPLICANT_EDUCATION_INFO_SEARCH_DURATIONYEARS, "duration_years"},
            {"" + LC.JOB_APPLICANT_EDUCATION_INFO_SEARCH_ACHIEVEMENT, "achievement"},
            {"" + LC.JOB_APPLICANT_EDUCATION_INFO_SEARCH_INSERTIONDATE, "insertion_date"},
            {"" + LC.JOB_APPLICANT_EDUCATION_INFO_SEARCH_INSERTEDBY, "inserted_by"},
            {"" + LC.JOB_APPLICANT_EDUCATION_INFO_SEARCH_MODIFIEDBY, "modified_by"},
            {"" + LC.JOB_APPLICANT_EDUCATION_INFO_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_JOB_APPLICANT_PROFESSIONAL_INFO = "navJOB_APPLICANT_PROFESSIONAL_INFO";
    public final static String VIEW_JOB_APPLICANT_PROFESSIONAL_INFO = "viewJOB_APPLICANT_PROFESSIONAL_INFO";
    public static final String[][] SEARCH_JOB_APPLICANT_PROFESSIONAL_INFO = {
            {"" + LC.JOB_APPLICANT_PROFESSIONAL_INFO_SEARCH_EMPLOYEERECORDSID, "employee_records_id"},
            {"" + LC.JOB_APPLICANT_PROFESSIONAL_INFO_SEARCH_GOVTSERVICESTARTDATE, "govt_service_start_date"},
            {"" + LC.JOB_APPLICANT_PROFESSIONAL_INFO_SEARCH_GAZETTEDDATE, "gazetted_date"},
            {"" + LC.JOB_APPLICANT_PROFESSIONAL_INFO_SEARCH_ENCARDMENTDATE, "encardment_date"},
            {"" + LC.JOB_APPLICANT_PROFESSIONAL_INFO_SEARCH_EMPLOYEECADREID, "employee_cadre_id"},
            {"" + LC.JOB_APPLICANT_PROFESSIONAL_INFO_SEARCH_EMPLOYEEBATCHESID, "employee_batches_id"},
            {"" + LC.JOB_APPLICANT_PROFESSIONAL_INFO_SEARCH_DESIGNATION, "designation"},
            {"" + LC.JOB_APPLICANT_PROFESSIONAL_INFO_SEARCH_DEPARTMENT, "department"},
            {"" + LC.JOB_APPLICANT_PROFESSIONAL_INFO_SEARCH_LPRDATE, "lpr_date"},
            {"" + LC.JOB_APPLICANT_PROFESSIONAL_INFO_SEARCH_ISGOVTJOB, "is_govt_job"},
            {"" + LC.JOB_APPLICANT_PROFESSIONAL_INFO_SEARCH_NAMEOFEMPLOYEE, "name_of_employee"},
            {"" + LC.JOB_APPLICANT_PROFESSIONAL_INFO_SEARCH_EMPLOYEEADDRESS, "employee_address"},
            {"" + LC.JOB_APPLICANT_PROFESSIONAL_INFO_SEARCH_EMPLOYEEEMPLOYMENTTYPE, "employee_employment_type"},
            {"" + LC.JOB_APPLICANT_PROFESSIONAL_INFO_SEARCH_SERVINGFROM, "serving_from"},
            {"" + LC.JOB_APPLICANT_PROFESSIONAL_INFO_SEARCH_SERVINGTO, "serving_to"},
            {"" + LC.JOB_APPLICANT_PROFESSIONAL_INFO_SEARCH_ISCURRENTLYWORKING, "is_currently_working"},
            {"" + LC.JOB_APPLICANT_PROFESSIONAL_INFO_SEARCH_JOBRESPONSIBILITY, "job_responsibility"},
            {"" + LC.JOB_APPLICANT_PROFESSIONAL_INFO_SEARCH_EMPLOYEEWORKAREATYPE, "employee_work_area_type"},
            {"" + LC.JOB_APPLICANT_PROFESSIONAL_INFO_SEARCH_KEYACHEIVEMENT, "key_acheivement"},
            {"" + LC.JOB_APPLICANT_PROFESSIONAL_INFO_SEARCH_TOTALEXPERIENCE, "total_experience"},
            {"" + LC.JOB_APPLICANT_PROFESSIONAL_INFO_SEARCH_SERVICETYPE, "service_type"},
            {"" + LC.JOB_APPLICANT_PROFESSIONAL_INFO_SEARCH_SUPERVISORNAME, "supervisor_name"},
            {"" + LC.JOB_APPLICANT_PROFESSIONAL_INFO_SEARCH_SUPERVISORMOBILE, "supervisor_mobile"},
            {"" + LC.JOB_APPLICANT_PROFESSIONAL_INFO_SEARCH_SUPERVISOREMAIL, "supervisor_email"},
            {"" + LC.JOB_APPLICANT_PROFESSIONAL_INFO_SEARCH_FILESDROPZONE, "filesDropzone"},
            {"" + LC.JOB_APPLICANT_PROFESSIONAL_INFO_SEARCH_INSERTIONDATE, "insertion_date"},
            {"" + LC.JOB_APPLICANT_PROFESSIONAL_INFO_SEARCH_INSERTEDBY, "inserted_by"},
            {"" + LC.JOB_APPLICANT_PROFESSIONAL_INFO_SEARCH_MODIFIEDBY, "modified_by"},
            {"" + LC.JOB_APPLICANT_PROFESSIONAL_INFO_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_JOB_APPLICANT_CERTIFICATION = "navJOB_APPLICANT_CERTIFICATION";
    public final static String VIEW_JOB_APPLICANT_CERTIFICATION = "viewJOB_APPLICANT_CERTIFICATION";
    public static final String[][] SEARCH_JOB_APPLICANT_CERTIFICATION = {
            {"" + LC.JOB_APPLICANT_CERTIFICATION_SEARCH_EMPLOYEERECORDSID, "employee_records_id"},
            {"" + LC.JOB_APPLICANT_CERTIFICATION_SEARCH_CERTIFICATIONNAME, "certification_name"},
            {"" + LC.JOB_APPLICANT_CERTIFICATION_SEARCH_ISSUINGORGANIZATION, "issuing_organization"},
            {"" + LC.JOB_APPLICANT_CERTIFICATION_SEARCH_ISSUINGINSTITUTEADDRESS, "issuing_institute_address"},
            {"" + LC.JOB_APPLICANT_CERTIFICATION_SEARCH_VALIDFROM, "valid_from"},
            {"" + LC.JOB_APPLICANT_CERTIFICATION_SEARCH_HASEXPIRYCAT, "has_expiry_cat"},
            {"" + LC.JOB_APPLICANT_CERTIFICATION_SEARCH_VALIDUPTO, "valid_upto"},
            {"" + LC.JOB_APPLICANT_CERTIFICATION_SEARCH_CREDENTIALID, "credential_id"},
            {"" + LC.JOB_APPLICANT_CERTIFICATION_SEARCH_INSERTEDBYUSERID, "inserted_by_user_id"},
            {"" + LC.JOB_APPLICANT_CERTIFICATION_SEARCH_INSERTIONDATE, "insertion_date"},
            {"" + LC.JOB_APPLICANT_CERTIFICATION_SEARCH_MODIFIEDBY, "modified_by"},
            {"" + LC.JOB_APPLICANT_CERTIFICATION_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_JOB_APPLICANT_REFERENCE = "navJOB_APPLICANT_REFERENCE";
    public final static String VIEW_JOB_APPLICANT_REFERENCE = "viewJOB_APPLICANT_REFERENCE";
    public static final String[][] SEARCH_JOB_APPLICANT_REFERENCE = {
            {"" + LC.JOB_APPLICANT_REFERENCE_SEARCH_EMPLOYEERECORDSID, "employee_records_id"},
            {"" + LC.JOB_APPLICANT_REFERENCE_SEARCH_NAMEENG, "name_eng"},
            {"" + LC.JOB_APPLICANT_REFERENCE_SEARCH_NAMEBNG, "name_bng"},
            {"" + LC.JOB_APPLICANT_REFERENCE_SEARCH_DESIGNATION, "designation"},
            {"" + LC.JOB_APPLICANT_REFERENCE_SEARCH_DEPARTMENT, "department"},
            {"" + LC.JOB_APPLICANT_REFERENCE_SEARCH_MAILADDRESS, "mailAddress"},
            {"" + LC.JOB_APPLICANT_REFERENCE_SEARCH_RELATIONSHIPCAT, "relationship_cat"},
            {"" + LC.JOB_APPLICANT_REFERENCE_SEARCH_PHONENO, "phoneNo"},
            {"" + LC.JOB_APPLICANT_REFERENCE_SEARCH_ALTERNATEPHONENO, "alternatePhoneNo"},
            {"" + LC.JOB_APPLICANT_REFERENCE_SEARCH_REFADDRESS, "ref_address"},
            {"" + LC.JOB_APPLICANT_REFERENCE_SEARCH_INSERTEDBYUSERID, "inserted_by_user_id"},
            {"" + LC.JOB_APPLICANT_REFERENCE_SEARCH_INSERTIONDATE, "insertion_date"},
            {"" + LC.JOB_APPLICANT_REFERENCE_SEARCH_MODIFIEDBY, "modified_by"},
            {"" + LC.JOB_APPLICANT_REFERENCE_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_JOB_APPLICANT_PHOTO_SIGNATURE = "navJOB_APPLICANT_PHOTO_SIGNATURE";
    public final static String VIEW_JOB_APPLICANT_PHOTO_SIGNATURE = "viewJOB_APPLICANT_PHOTO_SIGNATURE";
    public static final String[][] SEARCH_JOB_APPLICANT_PHOTO_SIGNATURE = {
            {"" + LC.JOB_APPLICANT_PHOTO_SIGNATURE_SEARCH_JOBAPPLICANTID, "job_applicant_id"},
            {"" + LC.JOB_APPLICANT_PHOTO_SIGNATURE_SEARCH_PHOTOORSIGNATURE, "photo_or_signature"},
            {"" + LC.JOB_APPLICANT_PHOTO_SIGNATURE_SEARCH_FILESDROPZONE, "filesDropzone"},
            {"" + LC.JOB_APPLICANT_PHOTO_SIGNATURE_SEARCH_INSERTIONDATE, "insertion_date"},
            {"" + LC.JOB_APPLICANT_PHOTO_SIGNATURE_SEARCH_INSERTEDBYUSERID, "inserted_by_user_id"},
            {"" + LC.JOB_APPLICANT_PHOTO_SIGNATURE_SEARCH_MODIFIEDBY, "modified_by"},
            {"" + LC.JOB_APPLICANT_PHOTO_SIGNATURE_SEARCH_ANYFIELD, "AnyField"}
    };

    public final static String NAV_ELECTION_DETAILS = "navELECTION_DETAILS";
    public final static String VIEW_ELECTION_DETAILS = "viewELECTION_DETAILS";
    public static final String[][] SEARCH_ELECTION_DETAILS = {
            {"" + LC.ELECTION_DETAILS_SEARCH_PARLIAMENTNUMBER, "parliament_number"},
            {"" + LC.ELECTION_DETAILS_SEARCH_ELECTIONDATE, "election_date"},
            {"" + LC.ELECTION_DETAILS_SEARCH_ANYFIELD, "AnyField"}

    };


    public final static String NAV_JOB_APPLICANT_APPLICATION = "navJOB_APPLICANT_APPLICATION";
    public final static String VIEW_JOB_APPLICANT_APPLICATION = "viewJOB_APPLICANT_APPLICATION";
    public static final String[][] SEARCH_JOB_APPLICANT_APPLICATION = {
            {"" + LC.JOB_APPLICANT_APPLICATION_SEARCH_JOBAPPLICANTID, "job_applicant_id"},
            {"" + LC.JOB_APPLICANT_APPLICATION_SEARCH_JOBID, "job_id"},
            {"" + LC.JOB_APPLICANT_APPLICATION_SEARCH_ACCEPTANCESTATUS, "acceptance_status"},
            {"" + LC.JOB_APPLICANT_APPLICATION_SEARCH_INSERTIONDATE, "insertion_date"},
            {"" + LC.JOB_APPLICANT_APPLICATION_SEARCH_INSERTEDBYUSERID, "inserted_by_user_id"},
            {"" + LC.JOB_APPLICANT_APPLICATION_SEARCH_MODIFIEDBY, "modified_by"},
            {"" + LC.JOB_APPLICANT_APPLICATION_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_ELECTION_CONSTITUENCY = "navELECTION_CONSTITUENCY";
    public final static String VIEW_ELECTION_CONSTITUENCY = "viewELECTION_CONSTITUENCY";
    public static final String[][] SEARCH_ELECTION_CONSTITUENCY = {
            {"" + LC.ELECTION_CONSTITUENCY_SEARCH_ELECTIONDETAILSTYPE, "election_details_type"},
            {"" + LC.ELECTION_CONSTITUENCY_SEARCH_GEODIVISIONTYPE, "geo_division_type"},
            {"" + LC.ELECTION_CONSTITUENCY_SEARCH_GEODISTRICTTYPE, "geo_district_type"},
            {"" + LC.ELECTION_CONSTITUENCY_SEARCH_GEOTHANATYPE, "geo_thana_type"},
            {"" + LC.ELECTION_CONSTITUENCY_SEARCH_CONSTITUENCYNUMBER, "constituency_number"},
            {"" + LC.ELECTION_CONSTITUENCY_SEARCH_CONSTITUENCYNAMEEN, "constituency_name_en"},
            {"" + LC.ELECTION_CONSTITUENCY_SEARCH_ANYFIELD, "AnyField"}
    };
    public final static String NAV_ELECTION_WISE_MP = "navELECTION_WISE_MP";
    public final static String VIEW_ELECTION_WISE_MP = "viewELECTION_WISE_MP";
    public static final String[][] SEARCH_ELECTION_WISE_MP = {
            //{ ""+LC.ELECTION_WISE_MP_SEARCH_ELECTIONDETAILSID, "election_details_id" },
            {"" + LC.ELECTION_WISE_MP_SEARCH_ELECTIONCONSTITUENCYID, "election_constituency_id"},
            {"" + LC.ELECTION_WISE_MP_SEARCH_EMPLOYEERECORDSID, "employee_records_id"},
            {"" + LC.ELECTION_WISE_MP_SEARCH_POLITICALPARTYID, "political_party_id"},
            {"" + LC.ELECTION_WISE_MP_SEARCH_STARTDATE, "start_date"},
            {"" + LC.ELECTION_WISE_MP_SEARCH_ENDDATE, "end_date"},
            {"" + LC.ELECTION_WISE_MP_SEARCH_INSERTIONDATE, "insertion_date"},
            {"" + LC.ELECTION_WISE_MP_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_POLITICAL_PARTY = "navPOLITICAL_PARTY";
    public final static String VIEW_POLITICAL_PARTY = "viewPOLITICAL_PARTY";
    public static final String[][] SEARCH_POLITICAL_PARTY = {
            {"" + LC.POLITICAL_PARTY_SEARCH_NAMEEN, "name_en"},
            {"" + LC.POLITICAL_PARTY_SEARCH_NAMEBN, "name_bn"},
            {"" + LC.POLITICAL_PARTY_SEARCH_ABBREVIATION, "abbreviation"},
            {"" + LC.POLITICAL_PARTY_SEARCH_PRESIDENTNAME, "president_name"},
            {"" + LC.POLITICAL_PARTY_SEARCH_GSNAME, "gs_name"},
            {"" + LC.POLITICAL_PARTY_SEARCH_ANYFIELD, "AnyField"}
    };

    public final static String NAV_EMPLOYEE_POSTING = "navEMPLOYEE_POSTING";
    public final static String VIEW_EMPLOYEE_POSTING = "viewEMPLOYEE_POSTING";
    public static final String[][] SEARCH_EMPLOYEE_POSTING = {
            {"" + LC.EMPLOYEE_POSTING_SEARCH_LOCALORFOREIGN, "local_or_foreign"},
            {"" + LC.EMPLOYEE_POSTING_SEARCH_POST, "post"},
            {"" + LC.EMPLOYEE_POSTING_SEARCH_DESIGNATION, "designation"},
            {"" + LC.EMPLOYEE_POSTING_SEARCH_COUNTRY, "country"},
            {"" + LC.EMPLOYEE_POSTING_SEARCH_PLACE, "place"},
            {"" + LC.EMPLOYEE_POSTING_SEARCH_ORGANIZATION, "organization"},
            {"" + LC.EMPLOYEE_POSTING_SEARCH_POSTINGFROM, "posting_from"},
            {"" + LC.EMPLOYEE_POSTING_SEARCH_POSTINGTO, "posting_to"},
            {"" + LC.EMPLOYEE_POSTING_SEARCH_PAYSCALE, "pay_scale"},
            {"" + LC.EMPLOYEE_POSTING_SEARCH_ANYFIELD, "AnyField"}
    };
    public final static String NAV_PROMOTION_HISTORY = "navPROMOTION_HISTORY";
    public final static String VIEW_PROMOTION_HISTORY = "viewPROMOTION_HISTORY";
    public static final String[][] SEARCH_PROMOTION_HISTORY = {
            {"" + LC.PROMOTION_HISTORY_SEARCH_EMPLOYEERECORDSID, "employee_records_id"},
            {"" + LC.PROMOTION_HISTORY_SEARCH_RANKCAT, "rank_cat"},
            {"" + LC.PROMOTION_HISTORY_SEARCH_PROMOTIONDATE, "promotion_date"},
            {"" + LC.PROMOTION_HISTORY_SEARCH_GODATE, "g_o_date"},
            {"" + LC.PROMOTION_HISTORY_SEARCH_JOININGDATE, "joining_date"},
            {"" + LC.PROMOTION_HISTORY_SEARCH_PROMOTIONNATURECAT, "promotion_nature_cat"},
            {"" + LC.PROMOTION_HISTORY_SEARCH_EMPLOYEEPAYSCALETYPE, "employee_pay_scale_type"},
            {"" + LC.PROMOTION_HISTORY_SEARCH_PREVIOUSRANKTYPE, "previous_rank_type"},
            {"" + LC.PROMOTION_HISTORY_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_EMPLOYEE_ACR = "navEMPLOYEE_ACR";
    public final static String VIEW_EMPLOYEE_ACR = "viewEMPLOYEE_ACR";
    public static final String[][] SEARCH_EMPLOYEE_ACR = {
            {"" + LC.EMPLOYEE_ACR_SEARCH_YEAR, "year"},
            {"" + LC.EMPLOYEE_ACR_SEARCH_ACRFROM, "acr_from"},
            {"" + LC.EMPLOYEE_ACR_SEARCH_ACRTO, "acr_to"},
            {"" + LC.EMPLOYEE_ACR_SEARCH_STATUS, "status"},
            {"" + LC.EMPLOYEE_ACR_SEARCH_REMARKS, "remarks"},
            {"" + LC.EMPLOYEE_ACR_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_ADMIT_CARD = "navADMIT_CARD";
    public final static String VIEW_ADMIT_CARD = "viewADMIT_CARD";
    public static final String[][] SEARCH_ADMIT_CARD = {
            {"" + LC.ADMIT_CARD_SEARCH_RECRUITMENTJOBDESCRIPTIONTYPE, "recruitment_job_description_type"},
            {"" + LC.ADMIT_CARD_SEARCH_PLACEOFEXAM, "place_of_exam"},
            {"" + LC.ADMIT_CARD_SEARCH_DATEOFEXAM, "date_of_exam"},
            {"" + LC.ADMIT_CARD_SEARCH_TIMEOFEXAM, "time_of_exam"},
            {"" + LC.ADMIT_CARD_SEARCH_ROLLNOFROM, "roll_no_from"},
            {"" + LC.ADMIT_CARD_SEARCH_ROLLNOTO, "roll_no_to"},
            {"" + LC.ADMIT_CARD_SEARCH_EMPLOYEERECORDSID, "employee_records_id"},
            {"" + LC.ADMIT_CARD_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_FAST_SEARCH_TEST = "navFAST_SEARCH_TEST";
    public final static String VIEW_FAST_SEARCH_TEST = "viewFAST_SEARCH_TEST";
    public static final String[][] SEARCH_FAST_SEARCH_TEST = {
            {"" + LC.FAST_SEARCH_TEST_SEARCH_RELIGIONCAT, "religion_cat"},
            {"" + LC.FAST_SEARCH_TEST_SEARCH_MEDICALEQUIPMENTNAMETYPE, "medical_equipment_name_type"},
            {"" + LC.FAST_SEARCH_TEST_SEARCH_NAMEEN, "name_en"},
            {"" + LC.FAST_SEARCH_TEST_SEARCH_NAMEBN, "name_bn"},
            {"" + LC.FAST_SEARCH_TEST_SEARCH_INSERTEDBYUSERID, "inserted_by_user_id"},
            {"" + LC.FAST_SEARCH_TEST_SEARCH_INSERTEDBYORGANOGRAMID, "inserted_by_organogram_id"},
            {"" + LC.FAST_SEARCH_TEST_SEARCH_HOMEADDRESS, "home_address"},
            {"" + LC.FAST_SEARCH_TEST_SEARCH_INSERTIONDATE, "insertion_date"},
            {"" + LC.FAST_SEARCH_TEST_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_TICKET_ISSUES = "navTICKET_ISSUES";
    public final static String VIEW_TICKET_ISSUES = "viewTICKET_ISSUES";
    public static final String[][] SEARCH_TICKET_ISSUES = {
            {"" + LC.TICKET_ISSUES_SEARCH_NAMEEN, "name_en"},
            {"" + LC.TICKET_ISSUES_SEARCH_NAMEBN, "name_bn"},
            {"" + LC.TICKET_ISSUES_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_SUPPORT_TICKET = "navSUPPORT_TICKET";
    public final static String VIEW_SUPPORT_TICKET = "viewSUPPORT_TICKET";
    public static final String[][] SEARCH_SUPPORT_TICKET = {
            {"" + LC.SUPPORT_TICKET_SEARCH_DEPARTMENTDATETIMEID, "department_datetime_id"},
            {"" + LC.SUPPORT_TICKET_SEARCH_TICKETISSUESTYPE, "ticket_issues_type"},
            {"" + LC.SUPPORT_TICKET_SEARCH_TICKETISSUESSUBTYPETYPE, "ticket_issues_subtype_type"},
            {"" + LC.SUPPORT_TICKET_SEARCH_DESCRIPTION, "description"},
            {"" + LC.SUPPORT_TICKET_SEARCH_CONFIGURATION, "configuration"},
            {"" + LC.SUPPORT_TICKET_SEARCH_TICKETSTATUSCAT, "ticket_status_cat"},
            {"" + LC.SUPPORT_TICKET_SEARCH_DUEDATE, "due_date"},
            {"" + LC.SUPPORT_TICKET_SEARCH_CURRENTASSIGNEDUSERID, "current_assigned_user_id"},
            {"" + LC.SUPPORT_TICKET_SEARCH_PRIORITYCAT, "priority_cat"},
            {"" + LC.SUPPORT_TICKET_SEARCH_INSERTEDBYUSERID, "inserted_by_user_id"},
            {"" + LC.SUPPORT_TICKET_SEARCH_INSERTEDBYORGANOGRAMID, "inserted_by_organogram_id"},
            {"" + LC.SUPPORT_TICKET_SEARCH_INSERTIONDATE, "insertion_date"},
            {"" + LC.SUPPORT_TICKET_SEARCH_MODIFIEDBY, "modified_by"},
            {"" + LC.SUPPORT_TICKET_SEARCH_CLOSINGCOMMENTS, "closing_comments"},
            {"" + LC.SUPPORT_TICKET_SEARCH_CLOSINGDATE, "closing_date"},
            {"" + LC.SUPPORT_TICKET_SEARCH_CLOSEDBYID, "closed_by_id"},
            {"" + LC.SUPPORT_TICKET_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_ATTENDANCE_DEVICE = "navATTENDANCE_DEVICE";
    public final static String VIEW_ATTENDANCE_DEVICE = "viewATTENDANCE_DEVICE";
    public static final String[][] SEARCH_ATTENDANCE_DEVICE = {
            {"" + LC.ATTENDANCE_DEVICE_SEARCH_DEVICENAMEEN, "device_name_en"},
            {"" + LC.ATTENDANCE_DEVICE_SEARCH_DEVICENAMEBN, "device_name_bn"},
            {"" + LC.ATTENDANCE_DEVICE_SEARCH_DEVICECAT, "device_cat"},
            {"" + LC.ATTENDANCE_DEVICE_SEARCH_DESCRIPTION, "description"},
            {"" + LC.ATTENDANCE_DEVICE_SEARCH_SYNCHRONIZATIONPROCESSCAT, "synchronization_process_cat"},
            {"" + LC.ATTENDANCE_DEVICE_SEARCH_SYNCHRONIZATIONDURATION, "synchronization_duration"},
            {"" + LC.ATTENDANCE_DEVICE_SEARCH_DEVICEIPADDRESS, "device_ip_address"},
            {"" + LC.ATTENDANCE_DEVICE_SEARCH_FTPIPADDRESS, "ftp_ip_address"},
            {"" + LC.ATTENDANCE_DEVICE_SEARCH_FTPUSERNAME, "ftp_username"},
            {"" + LC.ATTENDANCE_DEVICE_SEARCH_FTPPASSWORD, "ftp_password"},
            {"" + LC.ATTENDANCE_DEVICE_SEARCH_ANYFIELD, "AnyField"}
    };

    public final static String NAV_OFFICE_SHIFT = "navOFFICE_SHIFT";
    public final static String VIEW_OFFICE_SHIFT = "viewOFFICE_SHIFT";
    public static final String[][] SEARCH_OFFICE_SHIFT = {
            {"" + LC.OFFICE_SHIFT_SEARCH_OFFICEUNITID, "office_unit_id"},
            {"" + LC.OFFICE_SHIFT_SEARCH_NAMEEN, "name_en"},
            {"" + LC.OFFICE_SHIFT_SEARCH_NAMEBN, "name_bn"},
            {"" + LC.OFFICE_SHIFT_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_OFFICE_SHIFT_DETAILS = "navOFFICE_SHIFT_DETAILS";
    public final static String VIEW_OFFICE_SHIFT_DETAILS = "viewOFFICE_SHIFT_DETAILS";
    public static final String[][] SEARCH_OFFICE_SHIFT_DETAILS = {
            {"" + LC.OFFICE_SHIFT_DETAILS_SEARCH_OFFICESHIFTID, "office_shift_id"},
            {"" + LC.OFFICE_SHIFT_DETAILS_SEARCH_INTIME, "in_time"},
            {"" + LC.OFFICE_SHIFT_DETAILS_SEARCH_OUTTIME, "out_time"},
            {"" + LC.OFFICE_SHIFT_DETAILS_SEARCH_GRACEINPERIODMINS, "grace_in_period_mins"},
            {"" + LC.OFFICE_SHIFT_DETAILS_SEARCH_GRACEOUTPERIODMINS, "grace_out_period_mins"},
            {"" + LC.OFFICE_SHIFT_DETAILS_SEARCH_STARTDATE, "start_date"},
            {"" + LC.OFFICE_SHIFT_DETAILS_SEARCH_ENDDATE, "end_date"},
            {"" + LC.OFFICE_SHIFT_DETAILS_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_EMPLOYEE_SHIFT = "navEMPLOYEE_SHIFT";
    public final static String VIEW_EMPLOYEE_SHIFT = "viewEMPLOYEE_SHIFT";
    public static final String[][] SEARCH_EMPLOYEE_SHIFT = {
            {"" + LC.EMPLOYEE_SHIFT_SEARCH_EMPLOYEERECORDSID, "employee_records_id"},
            {"" + LC.EMPLOYEE_SHIFT_SEARCH_EMPLOYEEOFFICESID, "employee_offices_id"},
            {"" + LC.EMPLOYEE_SHIFT_SEARCH_OFFICESHIFTDETAILSID, "office_shift_details_id"},
            {"" + LC.EMPLOYEE_SHIFT_SEARCH_STARTDATE, "start_date"},
            {"" + LC.EMPLOYEE_SHIFT_SEARCH_ENDDATE, "end_date"},
            {"" + LC.EMPLOYEE_SHIFT_SEARCH_ANYFIELD, "AnyField"}
    };

    public final static String NAV_EMPLOYEE_ATTENDANCE_DEVICE = "navEMPLOYEE_ATTENDANCE_DEVICE";
    public final static String VIEW_EMPLOYEE_ATTENDANCE_DEVICE = "viewEMPLOYEE_ATTENDANCE_DEVICE";
    public static final String[][] SEARCH_EMPLOYEE_ATTENDANCE_DEVICE = {
            {"" + LC.EMPLOYEE_ATTENDANCE_DEVICE_SEARCH_EMPLOYEERECORDSID, "employee_records_id"},
            {"" + LC.EMPLOYEE_ATTENDANCE_DEVICE_SEARCH_ATTENDANCEDEVICEID, "attendance_device_id"},
            {"" + LC.EMPLOYEE_ATTENDANCE_DEVICE_SEARCH_CARDNUMBER, "card_number"},
            {"" + LC.EMPLOYEE_ATTENDANCE_DEVICE_SEARCH_ANYFIELD, "AnyField"}
    };

    public final static String NAV_EMPLOYEE_ATTENDANCE = "navEMPLOYEE_ATTENDANCE";
    public final static String VIEW_EMPLOYEE_ATTENDANCE = "viewEMPLOYEE_ATTENDANCE";
    public static final String[][] SEARCH_EMPLOYEE_ATTENDANCE = {
            {"" + LC.EMPLOYEE_ATTENDANCE_SEARCH_EMPLOYEERECORDSTYPE, "employee_records_type"},
            {"" + LC.EMPLOYEE_ATTENDANCE_SEARCH_ATTENDANCEDEVICETYPE, "attendance_device_type"},
            {"" + LC.EMPLOYEE_ATTENDANCE_SEARCH_EMPLOYEEATTENDANCEDEVICETYPE, "employee_attendance_device_type"},
            {"" + LC.EMPLOYEE_ATTENDANCE_SEARCH_INTIMESTAMP, "in_timestamp"},
            {"" + LC.EMPLOYEE_ATTENDANCE_SEARCH_OUTTIMESTAMP, "out_timestamp"},
            {"" + LC.EMPLOYEE_ATTENDANCE_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_EMPLOYEE_GRADE = "navEMPLOYEE_GRADE";
    public final static String VIEW_EMPLOYEE_GRADE = "viewEMPLOYEE_GRADE";
//	public static final String[][] SEARCH_EMPLOYEE_GRADE = {
//		{ ""+LC.EMPLOYEE_GRADE_SEARCH_ROLL, "roll" },
//		{ ""+LC.EMPLOYEE_GRADE_SEARCH_NUMBER, "number" },
//		{ ""+LC.EMPLOYEE_GRADE_SEARCH_GRADE, "grade" },
//		{ ""+LC.EMPLOYEE_GRADE_SEARCH_ANYFIELD , "AnyField" }
//	};


//	public final static String NAV_TESTING_EXCEL = "navTESTING_EXCEL";
//	public final static String VIEW_TESTING_EXCEL = "viewTESTING_EXCEL";
//	public static final String[][] SEARCH_TESTING_EXCEL = {
//		{ ""+LC.TESTING_EXCEL_SEARCH_JOBID, "job_id" },
//		{ ""+LC.TESTING_EXCEL_SEARCH_LEVELID, "level_id" },
//		{ ""+LC.TESTING_EXCEL_SEARCH_ROLL, "roll" },
//		{ ""+LC.TESTING_EXCEL_SEARCH_NUMBER, "number" },
//		{ ""+LC.TESTING_EXCEL_SEARCH_GRADE, "grade" },
//		{ ""+LC.TESTING_EXCEL_SEARCH_INSERTIONDATE, "insertion_date" },
//		{ ""+LC.TESTING_EXCEL_SEARCH_INSERTEDBY, "inserted_by" },
//		{ ""+LC.TESTING_EXCEL_SEARCH_MODIFIEDBY, "modified_by" },
//		{ ""+LC.TESTING_EXCEL_SEARCH_ANYFIELD , "AnyField" }
//	};


    public final static String NAV_ATTENDANCE_STATUS = "navATTENDANCE_STATUS";
    public final static String VIEW_ATTENDANCE_STATUS = "viewATTENDANCE_STATUS";
//	public static final String[][] SEARCH_ATTENDANCE_STATUS = {
//		{ ""+LC.ATTENDANCE_STATUS_SEARCH_JOBID, "job_id" },
//		{ ""+LC.ATTENDANCE_STATUS_SEARCH_LEVELID, "level_id" },
//		{ ""+LC.ATTENDANCE_STATUS_SEARCH_ANYFIELD , "AnyField" }
//	};

//	public static Map<Integer, String> AttendanceStatusMap  = new HashMap<Integer, String>() {{
//		put(1,"Present");
//		put(0,"Absent");
//
//	}};

//	public static Map<Integer, String> AttendanceStatusMap  = new HashMap<Integer, String>() {{
//		put(0,"Present");
//		put(1,"Absent");
//
//	}};


//	public final static String NAV_EMPLOYEE_GRADE = "navEMPLOYEE_GRADE";
//	public final static String VIEW_EMPLOYEE_GRADE = "viewEMPLOYEE_GRADE";
//	public static final String[][] SEARCH_EMPLOYEE_GRADE = {
//		{ ""+LC.EMPLOYEE_GRADE_SEARCH_ROLL, "roll" },
//		{ ""+LC.EMPLOYEE_GRADE_SEARCH_NUMBER, "number" },
//		{ ""+LC.EMPLOYEE_GRADE_SEARCH_GRADE, "grade" },
//		{ ""+LC.EMPLOYEE_GRADE_SEARCH_ANYFIELD , "AnyField" }
//	};


//	public final static String NAV_TESTING_EXCEL = "navTESTING_EXCEL";
//	public final static String VIEW_TESTING_EXCEL = "viewTESTING_EXCEL";
//	public static final String[][] SEARCH_TESTING_EXCEL = {
//		{ ""+LC.TESTING_EXCEL_SEARCH_JOBID, "job_id" },
//		{ ""+LC.TESTING_EXCEL_SEARCH_LEVELID, "level_id" },
//		{ ""+LC.TESTING_EXCEL_SEARCH_ROLL, "roll" },
//		{ ""+LC.TESTING_EXCEL_SEARCH_NUMBER, "number" },
//		{ ""+LC.TESTING_EXCEL_SEARCH_GRADE, "grade" },
//		{ ""+LC.TESTING_EXCEL_SEARCH_INSERTIONDATE, "insertion_date" },
//		{ ""+LC.TESTING_EXCEL_SEARCH_INSERTEDBY, "inserted_by" },
//		{ ""+LC.TESTING_EXCEL_SEARCH_MODIFIEDBY, "modified_by" },
//		{ ""+LC.TESTING_EXCEL_SEARCH_ANYFIELD , "AnyField" }
//	};


//	public final static String NAV_ATTENDANCE_STATUS = "navATTENDANCE_STATUS";
//	public final static String VIEW_ATTENDANCE_STATUS = "viewATTENDANCE_STATUS";
//	public static final String[][] SEARCH_ATTENDANCE_STATUS = {
//		{ ""+LC.ATTENDANCE_STATUS_SEARCH_JOBID, "job_id" },
//		{ ""+LC.ATTENDANCE_STATUS_SEARCH_LEVELID, "level_id" },
//		{ ""+LC.ATTENDANCE_STATUS_SEARCH_ANYFIELD , "AnyField" }
//	};

//	public static Map<Integer, String> AttendanceStatusMap  = new HashMap<Integer, String>() {{
//		put(1,"Present");
//		put(0,"Absent");
//
//	}};

    public static Map<Integer, String> AttendanceStatusMap = new HashMap<Integer, String>() {{
        put(1, "Present");
        put(2, "Absent");

    }};

    public static Map<Integer, String> PassFailStatusMap = new HashMap<Integer, String>() {{
        put(0, "Fail");
        put(1, "Pass");

    }};

    public static Map<Integer, String> PetrolStatusMap = new HashMap<Integer, String>() {{
        put(1, "Yes");
        put(2, "No");

    }};

    public static Map<Integer, String> FuelStatusMap = new HashMap<Integer, String>() {{
        put(1, "Active");
        put(2, "Inactive");

    }};


    public final static String NAV_RECRUITMENT_BULK_MARK_ENTRY = "navRECRUITMENT_BULK_MARK_ENTRY";
    public final static String VIEW_RECRUITMENT_BULK_MARK_ENTRY = "viewRECRUITMENT_BULK_MARK_ENTRY";
    public static final String[][] SEARCH_RECRUITMENT_BULK_MARK_ENTRY = {
            {"" + LC.RECRUITMENT_BULK_MARK_ENTRY_SEARCH_ROLL, "roll"},
            {"" + LC.RECRUITMENT_BULK_MARK_ENTRY_SEARCH_MARKS, "marks"},
            {"" + LC.RECRUITMENT_BULK_MARK_ENTRY_SEARCH_GRADE, "grade"},
            {"" + LC.RECRUITMENT_BULK_MARK_ENTRY_SEARCH_ANYFIELD, "AnyField"}
    };
//
//
//
//
//	public final static String NAV_RECRUITMENT_EXCEL_MARK_ENTRY = "navRECRUITMENT_EXCEL_MARK_ENTRY";
//	public final static String VIEW_RECRUITMENT_EXCEL_MARK_ENTRY = "viewRECRUITMENT_EXCEL_MARK_ENTRY";
//	public static final String[][] SEARCH_RECRUITMENT_EXCEL_MARK_ENTRY = {
//		{ ""+LC.RECRUITMENT_EXCEL_MARK_ENTRY_SEARCH_EMPLOYEERECORDSID, "employee_records_id" },
//		{ ""+LC.RECRUITMENT_EXCEL_MARK_ENTRY_SEARCH_EMPLOYEEOFFICESID, "employee_offices_id" },
//		{ ""+LC.RECRUITMENT_EXCEL_MARK_ENTRY_SEARCH_OFFICESHIFTDETAILSID, "office_shift_details_id" },
//		{ ""+LC.RECRUITMENT_EXCEL_MARK_ENTRY_SEARCH_OVERTIMEAPPLICABLE, "overtime_applicable" },
//		{ ""+LC.RECRUITMENT_EXCEL_MARK_ENTRY_SEARCH_SELFATTENDANCEENABLED, "self_attendance_enabled" },
//		{ ""+LC.RECRUITMENT_EXCEL_MARK_ENTRY_SEARCH_STARTDATE, "start_date" },
//		{ ""+LC.RECRUITMENT_EXCEL_MARK_ENTRY_SEARCH_ENDDATE, "end_date" },
//		{ ""+LC.RECRUITMENT_EXCEL_MARK_ENTRY_SEARCH_ISACTIVE, "is_active" },
//		{ ""+LC.RECRUITMENT_EXCEL_MARK_ENTRY_SEARCH_INSERTIONDATE, "insertion_date" },
//		{ ""+LC.RECRUITMENT_EXCEL_MARK_ENTRY_SEARCH_INSERTEDBY, "inserted_by" },
//		{ ""+LC.RECRUITMENT_EXCEL_MARK_ENTRY_SEARCH_MODIFIEDBY, "modified_by" },
//		{ ""+LC.RECRUITMENT_EXCEL_MARK_ENTRY_SEARCH_ANYFIELD , "AnyField" }
//	};


//	public final static String NAV_RECRUITMENT_EXCEL_MARK_ENTRY = "navRECRUITMENT_EXCEL_MARK_ENTRY";
//	public final static String VIEW_RECRUITMENT_EXCEL_MARK_ENTRY = "viewRECRUITMENT_EXCEL_MARK_ENTRY";
//	public static final String[][] SEARCH_RECRUITMENT_EXCEL_MARK_ENTRY = {
//		{ ""+LC.RECRUITMENT_EXCEL_MARK_ENTRY_SEARCH_EMPLOYEERECORDSID, "employee_records_id" },
//		{ ""+LC.RECRUITMENT_EXCEL_MARK_ENTRY_SEARCH_EMPLOYEEOFFICESID, "employee_offices_id" },
//		{ ""+LC.RECRUITMENT_EXCEL_MARK_ENTRY_SEARCH_OFFICESHIFTDETAILSID, "office_shift_details_id" },
//		{ ""+LC.RECRUITMENT_EXCEL_MARK_ENTRY_SEARCH_OVERTIMEAPPLICABLE, "overtime_applicable" },
//		{ ""+LC.RECRUITMENT_EXCEL_MARK_ENTRY_SEARCH_SELFATTENDANCEENABLED, "self_attendance_enabled" },
//		{ ""+LC.RECRUITMENT_EXCEL_MARK_ENTRY_SEARCH_STARTDATE, "start_date" },
//		{ ""+LC.RECRUITMENT_EXCEL_MARK_ENTRY_SEARCH_ENDDATE, "end_date" },
//		{ ""+LC.RECRUITMENT_EXCEL_MARK_ENTRY_SEARCH_ISACTIVE, "is_active" },
//		{ ""+LC.RECRUITMENT_EXCEL_MARK_ENTRY_SEARCH_INSERTIONDATE, "insertion_date" },
//		{ ""+LC.RECRUITMENT_EXCEL_MARK_ENTRY_SEARCH_INSERTEDBY, "inserted_by" },
//		{ ""+LC.RECRUITMENT_EXCEL_MARK_ENTRY_SEARCH_MODIFIEDBY, "modified_by" },
//		{ ""+LC.RECRUITMENT_EXCEL_MARK_ENTRY_SEARCH_ANYFIELD , "AnyField" }
//	};


//	public final static String NAV_RECRUITMENT_BULK_MARK_ENTRY = "navRECRUITMENT_BULK_MARK_ENTRY";
//	public final static String VIEW_RECRUITMENT_BULK_MARK_ENTRY = "viewRECRUITMENT_BULK_MARK_ENTRY";
//	public static final String[][] SEARCH_RECRUITMENT_BULK_MARK_ENTRY = {
//		{ ""+LC.RECRUITMENT_BULK_MARK_ENTRY_SEARCH_JOBID, "job_id" },
//		{ ""+LC.RECRUITMENT_BULK_MARK_ENTRY_SEARCH_LEVELID, "level_id" },
//		{ ""+LC.RECRUITMENT_BULK_MARK_ENTRY_SEARCH_ROLL, "roll" },
//		{ ""+LC.RECRUITMENT_BULK_MARK_ENTRY_SEARCH_MARKS, "marks" },
//		{ ""+LC.RECRUITMENT_BULK_MARK_ENTRY_SEARCH_GRADE, "grade" },
//		{ ""+LC.RECRUITMENT_BULK_MARK_ENTRY_SEARCH_INSERTIONDATE, "insertion_date" },
//		{ ""+LC.RECRUITMENT_BULK_MARK_ENTRY_SEARCH_INSERTEDBY, "inserted_by" },
//		{ ""+LC.RECRUITMENT_BULK_MARK_ENTRY_SEARCH_MODIFIEDBY, "modified_by" },
//		{ ""+LC.RECRUITMENT_BULK_MARK_ENTRY_SEARCH_ANYFIELD , "AnyField" }
//	};
//
//
//
//
//	public final static String NAV_RECRUITMENT_BULK_MARK_ENTRY = "navRECRUITMENT_BULK_MARK_ENTRY";
//	public final static String VIEW_RECRUITMENT_BULK_MARK_ENTRY = "viewRECRUITMENT_BULK_MARK_ENTRY";
//	public static final String[][] SEARCH_RECRUITMENT_BULK_MARK_ENTRY = {
//		{ ""+LC.RECRUITMENT_BULK_MARK_ENTRY_SEARCH_ROLL, "roll" },
//		{ ""+LC.RECRUITMENT_BULK_MARK_ENTRY_SEARCH_MARKS, "marks" },
//		{ ""+LC.RECRUITMENT_BULK_MARK_ENTRY_SEARCH_GRADE, "grade" },
//		{ ""+LC.RECRUITMENT_BULK_MARK_ENTRY_SEARCH_ANYFIELD , "AnyField" }
//	};


    public final static String NAV_RECRUITMENT_BULK_ATTENDANCE = "navRECRUITMENT_BULK_ATTENDANCE";
    public final static String VIEW_RECRUITMENT_BULK_ATTENDANCE = "viewRECRUITMENT_BULK_ATTENDANCE";
    public static final String[][] SEARCH_RECRUITMENT_BULK_ATTENDANCE = {
            {"" + LC.RECRUITMENT_BULK_ATTENDANCE_SEARCH_JOBID, "job_id"},
            {"" + LC.RECRUITMENT_BULK_ATTENDANCE_SEARCH_LEVELID, "level_id"},
            {"" + LC.RECRUITMENT_BULK_ATTENDANCE_SEARCH_ROLL, "roll"},
            {"" + LC.RECRUITMENT_BULK_ATTENDANCE_SEARCH_STATUS, "status"},
            {"" + LC.RECRUITMENT_BULK_ATTENDANCE_SEARCH_ANYFIELD, "AnyField"}};
    public final static String NAV_ATTENDANCE_ALERT_DETAILS = "navATTENDANCE_ALERT_DETAILS";
    public final static String VIEW_ATTENDANCE_ALERT_DETAILS = "viewATTENDANCE_ALERT_DETAILS";
    public static final String[][] SEARCH_ATTENDANCE_ALERT_DETAILS = {
            {"" + LC.ATTENDANCE_ALERT_DETAILS_SEARCH_EMPLOYEERECORDSID, "employee_records_id"},
            {"" + LC.ATTENDANCE_ALERT_DETAILS_SEARCH_SUPERVISORORGANOGRAMID, "supervisor_organogram_id"},
            {"" + LC.ATTENDANCE_ALERT_DETAILS_SEARCH_SUPERVISORNAME, "supervisor_name"},
            {"" + LC.ATTENDANCE_ALERT_DETAILS_SEARCH_SUPERVISORDESIGNATION, "supervisor_designation"},
            {"" + LC.ATTENDANCE_ALERT_DETAILS_SEARCH_ALERTSENDTIME, "alert_send_time"},
            {"" + LC.ATTENDANCE_ALERT_DETAILS_SEARCH_ANYFIELD, "AnyField"}
    };
    public static Map<Integer, String> excelUploadChoice = new HashMap<Integer, String>() {{
        put(1, "YES");
        put(2, "NO");

    }};


    public final static String NAV_DISCIPLINARY_ACTION = "navDISCIPLINARY_ACTION";
    public final static String VIEW_DISCIPLINARY_ACTION = "viewDISCIPLINARY_ACTION";
    public static final String[][] SEARCH_DISCIPLINARY_ACTION = {
            {"" + LC.DISCIPLINARY_ACTION_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_FEEDBACK_QUESTION = "navFEEDBACK_QUESTION";
    public final static String VIEW_FEEDBACK_QUESTION = "viewFEEDBACK_QUESTION";
    public static final String[][] SEARCH_FEEDBACK_QUESTION = {
            {"" + LC.FEEDBACK_QUESTION_SEARCH_QUESTIONEN, "QUESTION_EN"},
            {"" + LC.FEEDBACK_QUESTION_SEARCH_QUESTIONBN, "QUESTION_BN"},
            {"" + LC.FEEDBACK_QUESTION_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_UNIVERSITY_INFO = "navUNIVERSITY_INFO";
    public final static String VIEW_UNIVERSITY_INFO = "viewUNIVERSITY_INFO";
    public static final String[][] SEARCH_UNIVERSITY_INFO = {
            {"" + LC.UNIVERSITY_INFO_SEARCH_UNIVERSITYNAMEEN, "university_name_en"},
            {"" + LC.UNIVERSITY_INFO_SEARCH_UNIVERSITYNAMEBN, "university_name_bn"},
            {"" + LC.UNIVERSITY_INFO_SEARCH_ANYFIELD, "AnyField"}
    };

    public final static String NAV_ASSET_TYPE = "navASSET_TYPE";
    public final static String VIEW_ASSET_TYPE = "viewASSET_TYPE";
    public static final String[][] SEARCH_ASSET_TYPE = {
            {"" + LC.ASSET_TYPE_SEARCH_NAMEEN, "name_en"},
            {"" + LC.ASSET_TYPE_SEARCH_NAMEBN, "name_bn"},
            {"" + LC.ASSET_TYPE_SEARCH_INSERTEDBYUSERID, "inserted_by_user_id"},
            {"" + LC.ASSET_TYPE_SEARCH_INSERTEDBYORGANOGRAMID, "inserted_by_organogram_id"},
            {"" + LC.ASSET_TYPE_SEARCH_INSERTIONDATE, "insertion_date"},
            {"" + LC.ASSET_TYPE_SEARCH_LASTMODIFIERUSER, "last_modifier_user"},
            {"" + LC.ASSET_TYPE_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_ASSET_MANUFACTURER = "navASSET_MANUFACTURER";
    public final static String VIEW_ASSET_MANUFACTURER = "viewASSET_MANUFACTURER";
    public static final String[][] SEARCH_ASSET_MANUFACTURER = {
            {"" + LC.ASSET_MANUFACTURER_SEARCH_NAMEEN, "name_en"},
            {"" + LC.ASSET_MANUFACTURER_SEARCH_NAMEBN, "name_bn"},
            {"" + LC.ASSET_MANUFACTURER_SEARCH_COMPANYWEBSITE, "company_website"},
            {"" + LC.ASSET_MANUFACTURER_SEARCH_SUPPORTWEBSITE, "support_website"},
            {"" + LC.ASSET_MANUFACTURER_SEARCH_MANUFACTURERCONTACTNAME, "manufacturer_contact_name"},
            {"" + LC.ASSET_MANUFACTURER_SEARCH_SUPPORTCONTACT1, "support_contact_1"},
            {"" + LC.ASSET_MANUFACTURER_SEARCH_SUPPORTCONTACT2, "support_contact_2"},
            {"" + LC.ASSET_MANUFACTURER_SEARCH_SUPPORTCONTACT3, "support_contact_3"},
            {"" + LC.ASSET_MANUFACTURER_SEARCH_SUPPORTEMAIL, "support_email"},
            {"" + LC.ASSET_MANUFACTURER_SEARCH_COMPANYADDRESS, "company_address"},
            {"" + LC.ASSET_MANUFACTURER_SEARCH_INSERTEDBYUSERID, "inserted_by_user_id"},
            {"" + LC.ASSET_MANUFACTURER_SEARCH_INSERTEDBYORGANOGRAMID, "inserted_by_organogram_id"},
            {"" + LC.ASSET_MANUFACTURER_SEARCH_INSERTIONDATE, "insertion_date"},
            {"" + LC.ASSET_MANUFACTURER_SEARCH_LASTMODIFIERUSER, "last_modifier_user"},
            {"" + LC.ASSET_MANUFACTURER_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_ASSET_SUPPLIER = "navASSET_SUPPLIER";
    public final static String VIEW_ASSET_SUPPLIER = "viewASSET_SUPPLIER";
    public static final String[][] SEARCH_ASSET_SUPPLIER = {
            {"" + LC.ASSET_SUPPLIER_SEARCH_NAMEEN, "name_en"},
            {"" + LC.ASSET_SUPPLIER_SEARCH_NAMEBN, "name_bn"},
            {"" + LC.ASSET_SUPPLIER_SEARCH_COMPANYWEBSITE, "company_website"},
            {"" + LC.ASSET_SUPPLIER_SEARCH_WEBSITE, "website"},
            {"" + LC.ASSET_SUPPLIER_SEARCH_CONTACTNAME, "contact_name"},
            {"" + LC.ASSET_SUPPLIER_SEARCH_CONTACT1, "contact_1"},
            {"" + LC.ASSET_SUPPLIER_SEARCH_CONTACT2, "contact_2"},
            {"" + LC.ASSET_SUPPLIER_SEARCH_EMAIL, "email"},
            {"" + LC.ASSET_SUPPLIER_SEARCH_COMPANYADDRESS, "company_address"},
            {"" + LC.ASSET_SUPPLIER_SEARCH_INSERTEDBYUSERID, "inserted_by_user_id"},
            {"" + LC.ASSET_SUPPLIER_SEARCH_INSERTEDBYORGANOGRAMID, "inserted_by_organogram_id"},
            {"" + LC.ASSET_SUPPLIER_SEARCH_INSERTIONDATE, "insertion_date"},
            {"" + LC.ASSET_SUPPLIER_SEARCH_LASTMODIFIERUSER, "last_modifier_user"},
            {"" + LC.ASSET_SUPPLIER_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_TRAINING_CALENDAR_DETAILS = "navTRAINING_CALENDAR_DETAILS";
    public final static String VIEW_TRAINING_CALENDAR_DETAILS = "viewTRAINING_CALENDAR_DETAILS";
    public static final String[][] SEARCH_TRAINING_CALENDAR_DETAILS = {
            {"" + LC.TRAINING_CALENDAR_DETAILS_SEARCH_TRAININGCALENDARID, "training_calendar_id"},
            {"" + LC.TRAINING_CALENDAR_DETAILS_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_ASSET_LICENSE = "navASSET_LICENSE";
    public final static String VIEW_ASSET_LICENSE = "viewASSET_LICENSE";
    public static final String[][] SEARCH_ASSET_LICENSE = {
            {"" + LC.ASSET_LICENSE_SEARCH_SOFTWARENAME, "software_name"},
            {"" + LC.ASSET_LICENSE_SEARCH_SOFTWARECAT, "software_cat"},
            {"" + LC.ASSET_LICENSE_SEARCH_NUMBEROFLICENSE, "number_of_license"},
            {"" + LC.ASSET_LICENSE_SEARCH_ASSETMANUFACTURERID, "asset_manufacturer_id"},
            {"" + LC.ASSET_LICENSE_SEARCH_LICENSEDTONAME, "licensed_to_name"},
            {"" + LC.ASSET_LICENSE_SEARCH_LICENSEDTOEMAIL, "licensed_to_email"},
            {"" + LC.ASSET_LICENSE_SEARCH_ISREASSIGNABLE, "is_reassignable"},
            {"" + LC.ASSET_LICENSE_SEARCH_EXPIRATIONDATE, "expiration_date"},
            {"" + LC.ASSET_LICENSE_SEARCH_TERMINATIONDATE, "termination_date"},
            {"" + LC.ASSET_LICENSE_SEARCH_PURCHASEDATE, "purchase_date"},
            {"" + LC.ASSET_LICENSE_SEARCH_ASSETSUPPLIERID, "asset_supplier_id"},
            {"" + LC.ASSET_LICENSE_SEARCH_ORDERNUMBER, "order_number"},
            {"" + LC.ASSET_LICENSE_SEARCH_PURCHASECOST, "purchase_cost"},
            {"" + LC.ASSET_LICENSE_SEARCH_ISDEPRECIATIONAPPLICABLE, "is_depreciation_applicable"},
            {"" + LC.ASSET_LICENSE_SEARCH_DESCRIPTION, "description"},
            {"" + LC.ASSET_LICENSE_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_ASSET_LICENSE_KEY = "navASSET_LICENSE_KEY";
    public final static String VIEW_ASSET_LICENSE_KEY = "viewASSET_LICENSE_KEY";
    public static final String[][] SEARCH_ASSET_LICENSE_KEY = {
            {"" + LC.ASSET_LICENSE_KEY_SEARCH_ASSETLICENSEID, "asset_license_id"},
            {"" + LC.ASSET_LICENSE_KEY_SEARCH_PRODUCTKEY, "product_key"},
            {"" + LC.ASSET_LICENSE_KEY_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_EMPLOYEE_LICENSE_KEY = "navEMPLOYEE_LICENSE_KEY";
    public final static String VIEW_EMPLOYEE_LICENSE_KEY = "viewEMPLOYEE_LICENSE_KEY";
    public static final String[][] SEARCH_EMPLOYEE_LICENSE_KEY = {
            {"" + LC.EMPLOYEE_LICENSE_KEY_SEARCH_ASSETLICENSEKEYID, "asset_license_key_id"},
            {"" + LC.EMPLOYEE_LICENSE_KEY_SEARCH_EMPLOYEERECORDSID, "employee_records_id"},
            {"" + LC.EMPLOYEE_LICENSE_KEY_SEARCH_REMARKS, "remarks"},
            {"" + LC.EMPLOYEE_LICENSE_KEY_SEARCH_INSERTIONDATE, "insertion_date"},
            {"" + LC.EMPLOYEE_LICENSE_KEY_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_PHYSIOTHERAPY_PLAN = "navPHYSIOTHERAPY_PLAN";
    public final static String VIEW_PHYSIOTHERAPY_PLAN = "viewPHYSIOTHERAPY_PLAN";
    public static final String[][] SEARCH_PHYSIOTHERAPY_PLAN = {
            {"" + LC.PHYSIOTHERAPY_PLAN_SEARCH_APPOINTMENTID, "appointment_id"},
            {"" + LC.PHYSIOTHERAPY_PLAN_SEARCH_NAME, "name"},
            {"" + LC.PHYSIOTHERAPY_PLAN_SEARCH_DATEOFBIRTH, "date_of_birth"},
            {"" + LC.PHYSIOTHERAPY_PLAN_SEARCH_AGE, "age"},
            {"" + LC.PHYSIOTHERAPY_PLAN_SEARCH_HEIGHT, "height"},
            {"" + LC.PHYSIOTHERAPY_PLAN_SEARCH_WEIGHT, "weight"},
            {"" + LC.PHYSIOTHERAPY_PLAN_SEARCH_REMARKS, "remarks"},
            {"" + LC.PHYSIOTHERAPY_PLAN_SEARCH_INSERTEDBYUSERID, "inserted_by_user_id"},
            {"" + LC.PHYSIOTHERAPY_PLAN_SEARCH_INSERTEDBYORGANOGRAMID, "inserted_by_organogram_id"},
            {"" + LC.PHYSIOTHERAPY_PLAN_SEARCH_INSERTIONDATE, "insertion_date"},
            {"" + LC.PHYSIOTHERAPY_PLAN_SEARCH_LASTMODIFIERUSER, "last_modifier_user"},
            {"" + LC.PHYSIOTHERAPY_PLAN_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_ULTRASONO_TYPE = "navULTRASONO_TYPE";
    public final static String VIEW_ULTRASONO_TYPE = "viewULTRASONO_TYPE";
    public static final String[][] SEARCH_ULTRASONO_TYPE = {
            {"" + LC.ULTRASONO_TYPE_SEARCH_GENDERCAT, "gender_cat"},
            {"" + LC.ULTRASONO_TYPE_SEARCH_NAMEEN, "name_en"},
            {"" + LC.ULTRASONO_TYPE_SEARCH_NAMEBN, "name_bn"},
            {"" + LC.ULTRASONO_TYPE_SEARCH_HEADING, "heading"},
            {"" + LC.ULTRASONO_TYPE_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_USG_REPORT = "navUSG_REPORT";
    public final static String VIEW_USG_REPORT = "viewUSG_REPORT";
    public static final String[][] SEARCH_USG_REPORT = {
            {"" + LC.USG_REPORT_SEARCH_PRESCRIPTIONLABTESTID, "prescription_lab_test_id"},
            {"" + LC.USG_REPORT_SEARCH_PRESCRIPTIONDETAILSID, "prescription_details_id"},
            {"" + LC.USG_REPORT_SEARCH_COMMONLABREPORTID, "common_lab_report_id"},
            {"" + LC.USG_REPORT_SEARCH_APPOINTMENTID, "appointment_id"},
            {"" + LC.USG_REPORT_SEARCH_NAME, "name"},
            {"" + LC.USG_REPORT_SEARCH_DATEOFBIRTH, "date_of_birth"},
            {"" + LC.USG_REPORT_SEARCH_REFERREDBYUSERNAME, "referred_by_user_name"},
            {"" + LC.USG_REPORT_SEARCH_GENDERCAT, "gender_cat"},
            {"" + LC.USG_REPORT_SEARCH_ULTRASONOTYPETYPE, "ultrasono_type_type"},
            {"" + LC.USG_REPORT_SEARCH_COMMENT, "comment"},
            {"" + LC.USG_REPORT_SEARCH_DOCTORID, "doctor_id"},
            {"" + LC.USG_REPORT_SEARCH_INSERTEDBYUSERID, "inserted_by_user_id"},
            {"" + LC.USG_REPORT_SEARCH_INSERTEDBYORGANOGRAMID, "inserted_by_organogram_id"},
            {"" + LC.USG_REPORT_SEARCH_INSERTIONDATE, "insertion_date"},
            {"" + LC.USG_REPORT_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_XRAY_REPORT = "navXRAY_REPORT";
    public final static String VIEW_XRAY_REPORT = "viewXRAY_REPORT";
    public static final String[][] SEARCH_XRAY_REPORT = {
            {"" + LC.XRAY_REPORT_SEARCH_NAMEEN, "name_en"},
            {"" + LC.XRAY_REPORT_SEARCH_NAMEBN, "name_bn"},
            {"" + LC.XRAY_REPORT_SEARCH_COMMENT, "comment"},
            {"" + LC.XRAY_REPORT_SEARCH_INSERTEDBYUSERID, "inserted_by_user_id"},
            {"" + LC.XRAY_REPORT_SEARCH_INSERTEDBYORGANOGRAMID, "inserted_by_organogram_id"},
            {"" + LC.XRAY_REPORT_SEARCH_INSERTIONDATE, "insertion_date"},
            {"" + LC.XRAY_REPORT_SEARCH_LASTMODIFIERUSER, "last_modifier_user"},
            {"" + LC.XRAY_REPORT_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_MEDICAL_EMERGENCY_REQUEST = "navMEDICAL_EMERGENCY_REQUEST";
    public final static String VIEW_MEDICAL_EMERGENCY_REQUEST = "viewMEDICAL_EMERGENCY_REQUEST";
    public static final String[][] SEARCH_MEDICAL_EMERGENCY_REQUEST = {
            {"" + LC.MEDICAL_EMERGENCY_REQUEST_SEARCH_EMPLOYEEID, "employee_id"},
            {"" + LC.MEDICAL_EMERGENCY_REQUEST_SEARCH_EMPLOYEEUSERID, "employee_user_id"},
            {"" + LC.MEDICAL_EMERGENCY_REQUEST_SEARCH_PATIENTNAME, "patient_name"},
            {"" + LC.MEDICAL_EMERGENCY_REQUEST_SEARCH_PHONENUMBER, "phone_number"},
            {"" + LC.MEDICAL_EMERGENCY_REQUEST_SEARCH_DATEOFBIRTH, "date_of_birth"},
            {"" + LC.MEDICAL_EMERGENCY_REQUEST_SEARCH_GENDERCAT, "gender_cat"},
            {"" + LC.MEDICAL_EMERGENCY_REQUEST_SEARCH_WHOISTHEPATIENTCAT, "who_is_the_patient_cat"},
            {"" + LC.MEDICAL_EMERGENCY_REQUEST_SEARCH_SERVICECAT, "service_cat"},
            {"" + LC.MEDICAL_EMERGENCY_REQUEST_SEARCH_FROMADDRESS, "from_address"},
            {"" + LC.MEDICAL_EMERGENCY_REQUEST_SEARCH_TOADDRESS, "to_address"},
            {"" + LC.MEDICAL_EMERGENCY_REQUEST_SEARCH_SERVICEDATE, "service_date"},
            {"" + LC.MEDICAL_EMERGENCY_REQUEST_SEARCH_FROMTIME, "from_time"},
            {"" + LC.MEDICAL_EMERGENCY_REQUEST_SEARCH_TOTIME, "to_time"},
            {"" + LC.MEDICAL_EMERGENCY_REQUEST_SEARCH_SERVICEPERSONCAT, "service_person_cat"},
            {"" + LC.MEDICAL_EMERGENCY_REQUEST_SEARCH_SERVICEPERSONORGANOGRAMID, "service_person_organogram_id"},
            {"" + LC.MEDICAL_EMERGENCY_REQUEST_SEARCH_SERVICEPERSONUSERID, "service_person_user_id"},
            {"" + LC.MEDICAL_EMERGENCY_REQUEST_SEARCH_INSTRUCTIONS, "instructions"},
            {"" + LC.MEDICAL_EMERGENCY_REQUEST_SEARCH_INSERTEDBYUSERID, "inserted_by_user_id"},
            {"" + LC.MEDICAL_EMERGENCY_REQUEST_SEARCH_INSERTEDBYORGANOGRAMID, "inserted_by_organogram_id"},
            {"" + LC.MEDICAL_EMERGENCY_REQUEST_SEARCH_INSERTIONDATE, "insertion_date"},
            {"" + LC.MEDICAL_EMERGENCY_REQUEST_SEARCH_LASTMODIFIERUSER, "last_modifier_user"},
            {"" + LC.MEDICAL_EMERGENCY_REQUEST_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_FORWARD_TICKET = "navFORWARD_TICKET";
    public final static String VIEW_FORWARD_TICKET = "viewFORWARD_TICKET";
    public static final String[][] SEARCH_FORWARD_TICKET = {
            {"" + LC.FORWARD_TICKET_SEARCH_TICKETSTATUSCAT, "ticket_status_cat"},
            {"" + LC.FORWARD_TICKET_SEARCH_REMARKS, "remarks"},
            {"" + LC.FORWARD_TICKET_SEARCH_DUEDATE, "due_date"},
            {"" + LC.FORWARD_TICKET_SEARCH_CONFIGURATION, "configuration"},
            {"" + LC.FORWARD_TICKET_SEARCH_MAINRECEIVER, "main_receiver"},
            {"" + LC.FORWARD_TICKET_SEARCH_INSERTEDBYUSERID, "inserted_by_user_id"},
            {"" + LC.FORWARD_TICKET_SEARCH_INSERTEDBYORGANOGRAMID, "inserted_by_organogram_id"},
            {"" + LC.FORWARD_TICKET_SEARCH_INSERTIONDATE, "insertion_date"},
            {"" + LC.FORWARD_TICKET_SEARCH_MODIFIEDBY, "modified_by"},
            {"" + LC.FORWARD_TICKET_SEARCH_ANYFIELD, "AnyField"}
    };

    public final static String NAV_EMPLOYEE_ASSET_HISTORY = "navEMPLOYEE_ASSET_HISTORY";
    public final static String VIEW_EMPLOYEE_ASSET_HISTORY = "viewEMPLOYEE_ASSET_HISTORY";
    public static final String[][] SEARCH_EMPLOYEE_ASSET_HISTORY = {
            {"" + LC.EMPLOYEE_ASSET_HISTORY_SEARCH_ASSETLISTID, "asset_list_id"},
            {"" + LC.EMPLOYEE_ASSET_HISTORY_SEARCH_EMPLOYEERECORDSID, "employee_records_id"},
            {"" + LC.EMPLOYEE_ASSET_HISTORY_SEARCH_REMARKS, "remarks"},
            {"" + LC.EMPLOYEE_ASSET_HISTORY_SEARCH_INSERTIONDATE, "insertion_date"},
            {"" + LC.EMPLOYEE_ASSET_HISTORY_SEARCH_INSERTEDBY, "inserted_by"},
            {"" + LC.EMPLOYEE_ASSET_HISTORY_SEARCH_MODIFIEDBY, "modified_by"},
            {"" + LC.EMPLOYEE_ASSET_HISTORY_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_APPOINTMENT_LETTER = "navAPPOINTMENT_LETTER";
    public final static String VIEW_APPOINTMENT_LETTER = "viewAPPOINTMENT_LETTER";
    public static final String[][] SEARCH_APPOINTMENT_LETTER = {
            {"" + LC.APPOINTMENT_LETTER_SEARCH_RECRUITMENTJOBDESCRIPTIONID, "recruitment_job_description_id"},
            {"" + LC.APPOINTMENT_LETTER_SEARCH_ORDERINGEMPLOYEERECORDID, "ordering_employee_record_id"},
            {"" + LC.APPOINTMENT_LETTER_SEARCH_ORDERINGUNITID, "ordering_unit_id"},
            {"" + LC.APPOINTMENT_LETTER_SEARCH_ORDERINGPOSTID, "ordering_post_id"},
            {"" + LC.APPOINTMENT_LETTER_SEARCH_CODE, "code"},
            {"" + LC.APPOINTMENT_LETTER_SEARCH_ORDERINGEMPLOYEERECORDNAME, "ordering_employee_record_name"},
            {"" + LC.APPOINTMENT_LETTER_SEARCH_ORDERINGEMPLOYEERECORDNAMEBN, "ordering_employee_record_name_bn"},
            {"" + LC.APPOINTMENT_LETTER_SEARCH_ORDERINGUNITNAME, "ordering_unit_name"},
            {"" + LC.APPOINTMENT_LETTER_SEARCH_ORDERINGUNITNAMEBN, "ordering_unit_name_bn"},
            {"" + LC.APPOINTMENT_LETTER_SEARCH_ORDERINGPOSTNAME, "ordering_post_name"},
            {"" + LC.APPOINTMENT_LETTER_SEARCH_ORDERINGPOSTNAMEBN, "ordering_post_name_bn"},
            {"" + LC.APPOINTMENT_LETTER_SEARCH_RULES, "rules"},
            {"" + LC.APPOINTMENT_LETTER_SEARCH_INSERTIONDATE, "insertion_date"},
            {"" + LC.APPOINTMENT_LETTER_SEARCH_INSERTEDBY, "inserted_by"},
            {"" + LC.APPOINTMENT_LETTER_SEARCH_MODIFIEDBY, "modified_by"},
            {"" + LC.APPOINTMENT_LETTER_SEARCH_ANYFIELD, "AnyField"}
    };

    public final static String NAV_EMPLOYEE_ABSENT_APPROVED_HISTORY = "navEMPLOYEE_ABSENT_APPROVED_HISTORY";
    public final static String VIEW_EMPLOYEE_ABSENT_APPROVED_HISTORY = "viewEMPLOYEE_ABSENT_APPROVED_HISTORY";
    public static final String[][] SEARCH_EMPLOYEE_ABSENT_APPROVED_HISTORY = {
            {"" + LC.EMPLOYEE_ABSENT_APPROVED_HISTORY_SEARCH_EMPLOYEERECORDSID, "employee_records_id"},
            {"" + LC.EMPLOYEE_ABSENT_APPROVED_HISTORY_SEARCH_APPROVEDBYEMPRECORDSID, "approved_by_emp_records_id"},
            {"" + LC.EMPLOYEE_ABSENT_APPROVED_HISTORY_SEARCH_ABSENTDATE, "absent_date"},
            {"" + LC.EMPLOYEE_ABSENT_APPROVED_HISTORY_SEARCH_APPROVALDATE, "approval_date"},
            {"" + LC.EMPLOYEE_ABSENT_APPROVED_HISTORY_SEARCH_COMMENTS, "comments"},
            {"" + LC.EMPLOYEE_ABSENT_APPROVED_HISTORY_SEARCH_INSERTIONDATE, "insertion_date"},
            {"" + LC.EMPLOYEE_ABSENT_APPROVED_HISTORY_SEARCH_INSERTEDBY, "inserted_by"},
            {"" + LC.EMPLOYEE_ABSENT_APPROVED_HISTORY_SEARCH_MODIFIEDBY, "modified_by"},
            {"" + LC.EMPLOYEE_ABSENT_APPROVED_HISTORY_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_GATE_PASS = "navGATE_PASS";
    public final static String VIEW_GATE_PASS = "viewGATE_PASS";
    public static final String[][] SEARCH_GATE_PASS = {
            {"" + LC.GATE_PASS_SEARCH_VISITINGDATE, "visiting_date"},
            {"" + LC.GATE_PASS_SEARCH_GATEPASSTYPE, "gate_pass_type"},
            {"" + LC.GATE_PASS_SEARCH_VALIDITYFROM, "validity_from"},
            {"" + LC.GATE_PASS_SEARCH_VALIDITYTO, "validity_to"}
    };

    public final static String NAV_PARLIAMENT_GATE = "navPARLIAMENT_GATE";
    public final static String VIEW_PARLIAMENT_GATE = "viewPARLIAMENT_GATE";
    public static final String[][] SEARCH_PARLIAMENT_GATE = {
            {"" + LC.PARLIAMENT_GATE_SEARCH_GATENUMBER, "gate_number"},
            {"" + LC.PARLIAMENT_GATE_SEARCH_GATEPABXEXTNUMBER, "gate_pabx_ext_number"},
            {"" + LC.PARLIAMENT_GATE_SEARCH_NAMEENG, "name_eng"},
            {"" + LC.PARLIAMENT_GATE_SEARCH_NAMEBNG, "name_bng"},
            {"" + LC.PARLIAMENT_GATE_SEARCH_LOCATIONENG, "location_eng"},
            {"" + LC.PARLIAMENT_GATE_SEARCH_LOCATIONBNG, "location_bng"},
            {"" + LC.PARLIAMENT_GATE_SEARCH_DESCRIPTION, "description"},
            {"" + LC.PARLIAMENT_GATE_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_GATE_PASS_VISITOR = "navGATE_PASS_VISITOR";
    public final static String VIEW_GATE_PASS_VISITOR = "viewGATE_PASS_VISITOR";
    public static final String[][] SEARCH_GATE_PASS_VISITOR = {
            {"" + LC.GATE_PASS_VISITOR_SEARCH_NAME, "name"},
            {"" + LC.GATE_PASS_VISITOR_SEARCH_CREDENTIALNO, "credential_no"},
            {"" + LC.GATE_PASS_VISITOR_SEARCH_MOBILENUMBER, "mobile_number"},
            {"" + LC.GATE_PASS_VISITOR_SEARCH_EMAIL, "email"},
            {"" + LC.GATE_PASS_VISITOR_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_GATE_PASS_TYPE = "navGATE_PASS_TYPE";
    public final static String VIEW_GATE_PASS_TYPE = "viewGATE_PASS_TYPE";
    public static final String[][] SEARCH_GATE_PASS_TYPE = {
            {"" + LC.GATE_PASS_TYPE_SEARCH_NAMEENG, "name_eng"},
            {"" + LC.GATE_PASS_TYPE_SEARCH_NAMEBNG, "name_bng"},
            {"" + LC.GATE_PASS_TYPE_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_GATE_PASS_SUB_TYPE = "navGATE_PASS_SUB_TYPE";
    public final static String VIEW_GATE_PASS_SUB_TYPE = "viewGATE_PASS_SUB_TYPE";
    public static final String[][] SEARCH_GATE_PASS_SUB_TYPE = {
            {"" + LC.GATE_PASS_SUB_TYPE_SEARCH_GATEPASSTYPEID, "gate_pass_type_id"},
            {"" + LC.GATE_PASS_SUB_TYPE_SEARCH_NAMEENG, "name_eng"},
            {"" + LC.GATE_PASS_SUB_TYPE_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_GATE_PASS_AFFILIATED_PERSON = "navGATE_PASS_AFFILIATED_PERSON";
    public final static String VIEW_GATE_PASS_AFFILIATED_PERSON = "viewGATE_PASS_AFFILIATED_PERSON";
    public static final String[][] SEARCH_GATE_PASS_AFFILIATED_PERSON = {
            {"" + LC.GATE_PASS_AFFILIATED_PERSON_SEARCH_CREDENTIALNO, "credential_no"},
            {"" + LC.GATE_PASS_AFFILIATED_PERSON_SEARCH_MOBILENUMBER, "mobile_number"},
            {"" + LC.GATE_PASS_AFFILIATED_PERSON_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_PARLIAMENT_ITEM = "navPARLIAMENT_ITEM";
    public final static String VIEW_PARLIAMENT_ITEM = "viewPARLIAMENT_ITEM";
    public static final String[][] SEARCH_PARLIAMENT_ITEM = {
            {"" + LC.PARLIAMENT_ITEM_SEARCH_NAMEENG, "name_eng"},
            {"" + LC.PARLIAMENT_ITEM_SEARCH_NAMEBNG, "name_bng"},
            {"" + LC.PARLIAMENT_ITEM_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_GATE_PASS_ITEM = "navGATE_PASS_ITEM";
    public final static String VIEW_GATE_PASS_ITEM = "viewGATE_PASS_ITEM";
    public static final String[][] SEARCH_GATE_PASS_ITEM = {
            {"" + LC.GATE_PASS_ITEM_SEARCH_GATEPASSID, "gate_pass_id"},
            {"" + LC.GATE_PASS_ITEM_SEARCH_PARLIAMENTITEMID, "parliament_item_id"},
            {"" + LC.GATE_PASS_ITEM_SEARCH_ANYFIELD, "AnyField"}
    };

    public final static String NAV_PARLIAMENT_SESSION = "navPARLIAMENT_SESSION";
    public final static String VIEW_PARLIAMENT_SESSION = "viewPARLIAMENT_SESSION";
    public static final String[][] SEARCH_PARLIAMENT_SESSION = {
            {"" + LC.PARLIAMENT_SESSION_SEARCH_ELECTIONDETAILSID, "election_details_id"},
            {"" + LC.PARLIAMENT_SESSION_SEARCH_SESSIONNUMBER, "session_number"},
            {"" + LC.PARLIAMENT_SESSION_SEARCH_STARTDATE, "start_date"},
            {"" + LC.PARLIAMENT_SESSION_SEARCH_ENDDATE, "end_date"},
            {"" + LC.PARLIAMENT_SESSION_SEARCH_ANYFIELD, "AnyField"}
    };

    public final static String NAV_EVENT_CALENDAR = "navEVENT_CALENDAR";
    public final static String VIEW_EVENT_CALENDAR = "viewEVENT_CALENDAR";
    public static final String[][] SEARCH_EVENT_CALENDAR = {
            {"" + LC.EVENT_CALENDAR_SEARCH_NAMEEN, "name_en"},
            {"" + LC.EVENT_CALENDAR_SEARCH_NAMEBN, "name_bn"},
            {"" + LC.EVENT_CALENDAR_SEARCH_EVENTCAT, "event_cat"},
            {"" + LC.EVENT_CALENDAR_SEARCH_EVENTLOCATION, "event_location"},
            {"" + LC.EVENT_CALENDAR_SEARCH_EVENTDATE, "event_date"},
            {"" + LC.EVENT_CALENDAR_SEARCH_EVENTSTARTTIME, "event_start_time"},
            {"" + LC.EVENT_CALENDAR_SEARCH_EVENTENDTIME, "event_end_time"},
            {"" + LC.EVENT_CALENDAR_SEARCH_EVENTDESCRIPTION, "event_description"},
            {"" + LC.EVENT_CALENDAR_SEARCH_ISRECURRING, "is_recurring"},
            {"" + LC.EVENT_CALENDAR_SEARCH_INSERTEDBYUSERID, "inserted_by_user_id"},
            {"" + LC.EVENT_CALENDAR_SEARCH_INSERTEDBYORGANOGRAMID, "inserted_by_organogram_id"},
            {"" + LC.EVENT_CALENDAR_SEARCH_INSERTIONDATE, "insertion_date"},
            {"" + LC.EVENT_CALENDAR_SEARCH_LASTMODIFIERUSER, "last_modifier_user"},
            {"" + LC.EVENT_CALENDAR_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_APPOINTMENT_LETTER_ONULIPI = "navAPPOINTMENT_LETTER_ONULIPI";
    public final static String VIEW_APPOINTMENT_LETTER_ONULIPI = "viewAPPOINTMENT_LETTER_ONULIPI";
    public static final String[][] SEARCH_APPOINTMENT_LETTER_ONULIPI = {
            {"" + LC.APPOINTMENT_LETTER_ONULIPI_SEARCH_APPOINTMENTLETTERID, "appointment_letter_id"},
            {"" + LC.APPOINTMENT_LETTER_ONULIPI_SEARCH_EMPLOYEERECORDID, "employee_record_id"},
            {"" + LC.APPOINTMENT_LETTER_ONULIPI_SEARCH_UNITID, "unit_id"},
            {"" + LC.APPOINTMENT_LETTER_ONULIPI_SEARCH_POSTID, "post_id"},
            {"" + LC.APPOINTMENT_LETTER_ONULIPI_SEARCH_EMPLOYEERECORDNAME, "employee_record_name"},
            {"" + LC.APPOINTMENT_LETTER_ONULIPI_SEARCH_EMPLOYEERECORDNAMEBN, "employee_record_name_bn"},
            {"" + LC.APPOINTMENT_LETTER_ONULIPI_SEARCH_UNITNAME, "unit_name"},
            {"" + LC.APPOINTMENT_LETTER_ONULIPI_SEARCH_UNITNAMEBN, "unit_name_bn"},
            {"" + LC.APPOINTMENT_LETTER_ONULIPI_SEARCH_POSTNAME, "post_name"},
            {"" + LC.APPOINTMENT_LETTER_ONULIPI_SEARCH_POSTNAMEBN, "post_name_bn"},
            {"" + LC.APPOINTMENT_LETTER_ONULIPI_SEARCH_INSERTIONDATE, "insertion_date"},
            {"" + LC.APPOINTMENT_LETTER_ONULIPI_SEARCH_INSERTEDBY, "inserted_by"},
            {"" + LC.APPOINTMENT_LETTER_ONULIPI_SEARCH_MODIFIEDBY, "modified_by"},
            {"" + LC.APPOINTMENT_LETTER_ONULIPI_SEARCH_ANYFIELD, "AnyField"}
    };
    public final static String NAV_LEAVE_RELIEVER_MAPPING = "navLEAVE_RELIEVER_MAPPING";
    public final static String VIEW_LEAVE_RELIEVER_MAPPING = "viewLEAVE_RELIEVER_MAPPING";
    public static final String[][] SEARCH_LEAVE_RELIEVER_MAPPING = {
            {"" + LC.LEAVE_RELIEVER_MAPPING_SEARCH_ORGANOGRAMID, "organogram_id"},
            {"" + LC.LEAVE_RELIEVER_MAPPING_SEARCH_LEAVERELIEVER1, "leave_reliever_1"},
            {"" + LC.LEAVE_RELIEVER_MAPPING_SEARCH_LEAVERELIEVER2, "leave_reliever_2"},
            {"" + LC.LEAVE_RELIEVER_MAPPING_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_ASSIGN_ASSET = "navASSIGN_ASSET";
    public final static String VIEW_ASSIGN_ASSET = "viewASSIGN_ASSET";
    public static final String[][] SEARCH_ASSIGN_ASSET = {
            {"" + LC.ASSIGN_ASSET_SEARCH_ORGANOGRAMID, "organogram_id"},
            {"" + LC.ASSIGN_ASSET_SEARCH_BLOCKCAT, "block_cat"},
            {"" + LC.ASSIGN_ASSET_SEARCH_FLOORNUMBER, "floor_number"},
            {"" + LC.ASSIGN_ASSET_SEARCH_COMPUTERNAME, "computer_name"},

            {"" + LC.ASSIGN_ASSET_SEARCH_SUBNETMUSK, "subnet_musk"},
            {"" + LC.ASSIGN_ASSET_SEARCH_GATEWAY, "gateway"},
            {"" + LC.ASSIGN_ASSET_SEARCH_PCMODELTYPE, "pc_model_type"},
            {"" + LC.ASSIGN_ASSET_SEARCH_LICENCEKEY, "licence_key"},
            {"" + LC.ASSIGN_ASSET_SEARCH_LICENCEID, "licence_id"},
            {"" + LC.ASSIGN_ASSET_SEARCH_ASSETLICENSEKEYTYPE, "asset_license_key_type"},
            {"" + LC.ASSIGN_ASSET_SEARCH_RAM, "ram"},
            {"" + LC.ASSIGN_ASSET_SEARCH_CPU, "cpu"},
            {"" + LC.ASSIGN_ASSET_SEARCH_HDD, "hdd"},
            {"" + LC.ASSIGN_ASSET_SEARCH_OS, "os"},
            {"" + LC.ASSIGN_ASSET_SEARCH_OFFICE, "office"},
            {"" + LC.ASSIGN_ASSET_SEARCH_MONITORTYPE, "monitor_type"},
            {"" + LC.ASSIGN_ASSET_SEARCH_PRINTERTYPE, "printer_type"},
            {"" + LC.ASSIGN_ASSET_SEARCH_SCANNERTYPE, "scanner_type"},
            {"" + LC.ASSIGN_ASSET_SEARCH_ROUTERTYPE, "router_type"},
            {"" + LC.ASSIGN_ASSET_SEARCH_PCMAC, "pc_mac"},
            {"" + LC.ASSIGN_ASSET_SEARCH_FACEPLATENUMBER, "face_plate_number"},
            {"" + LC.ASSIGN_ASSET_SEARCH_HASANTIVIRUS, "has_antivirus"},
            {"" + LC.ASSIGN_ASSET_SEARCH_ANTIVIRUS, "antivirus"},
            {"" + LC.ASSIGN_ASSET_SEARCH_REMARKS, "remarks"},
            {"" + LC.ASSIGN_ASSET_SEARCH_INSERTEDBYUSERID, "inserted_by_user_id"},
            {"" + LC.ASSIGN_ASSET_SEARCH_INSERTEDBYORGANOGRAMID, "inserted_by_organogram_id"},
            {"" + LC.ASSIGN_ASSET_SEARCH_INSERTIONDATE, "insertion_date"},
            {"" + LC.ASSIGN_ASSET_SEARCH_LASTMODIFIERUSER, "last_modifier_user"},
            {"" + LC.ASSIGN_ASSET_SEARCH_ANYFIELD, "AnyField"}
    };

    public final static String NAV_SOFTWARE_TYPE = "navSOFTWARE_TYPE";
    public final static String VIEW_SOFTWARE_TYPE = "viewSOFTWARE_TYPE";
    public static final String[][] SEARCH_SOFTWARE_TYPE = {
            {"" + LC.SOFTWARE_TYPE_SEARCH_SOFTWARECAT, "software_cat"},
//		{ ""+LC.SOFTWARE_TYPE_SEARCH_NAMEEN, "name_en" },
//		{ ""+LC.SOFTWARE_TYPE_SEARCH_NAMEBN, "name_bn" },
            {"" + LC.SOFTWARE_TYPE_SEARCH_INSERTEDBYUSERID, "inserted_by_user_id"},
            {"" + LC.SOFTWARE_TYPE_SEARCH_INSERTEDBYORGANOGRAMID, "inserted_by_organogram_id"},
            {"" + LC.SOFTWARE_TYPE_SEARCH_INSERTIONDATE, "insertion_date"},
            {"" + LC.SOFTWARE_TYPE_SEARCH_LASTMODIFIERUSER, "last_modifier_user"},
            {"" + LC.SOFTWARE_TYPE_SEARCH_ANYFIELD, "AnyField"}
    };

    public final static String NAV_RECRUITMENT_MARKS_COMMITTEE = "navRECRUITMENT_MARKS_COMMITTEE";
    public final static String VIEW_RECRUITMENT_MARKS_COMMITTEE = "viewRECRUITMENT_MARKS_COMMITTEE";
    public static final String[][] SEARCH_RECRUITMENT_MARKS_COMMITTEE = {
            {"" + LC.RECRUITMENT_MARKS_COMMITTEE_SEARCH_NAME, "name"},
            {"" + LC.RECRUITMENT_MARKS_COMMITTEE_SEARCH_RECRUITMENTJOBDESCRIPTIONIDS, "recruitment_job_description_ids"},
            {"" + LC.RECRUITMENT_MARKS_COMMITTEE_SEARCH_INSERTIONDATE, "insertion_date"},
            {"" + LC.RECRUITMENT_MARKS_COMMITTEE_SEARCH_INSERTEDBY, "inserted_by"},
            {"" + LC.RECRUITMENT_MARKS_COMMITTEE_SEARCH_MODIFIEDBY, "modified_by"},
            {"" + LC.RECRUITMENT_MARKS_COMMITTEE_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_RECRUITMENT_MARKS_COMMITTEE_MEMBERS = "navRECRUITMENT_MARKS_COMMITTEE_MEMBERS";
    public final static String VIEW_RECRUITMENT_MARKS_COMMITTEE_MEMBERS = "viewRECRUITMENT_MARKS_COMMITTEE_MEMBERS";
    public static final String[][] SEARCH_RECRUITMENT_MARKS_COMMITTEE_MEMBERS = {
            {"" + LC.RECRUITMENT_MARKS_COMMITTEE_MEMBERS_SEARCH_RECRUITMENTMARKSCOMMITTEEID, "recruitment_marks_committee_id"},
            {"" + LC.RECRUITMENT_MARKS_COMMITTEE_MEMBERS_SEARCH_MOBILE, "mobile"},
            {"" + LC.RECRUITMENT_MARKS_COMMITTEE_MEMBERS_SEARCH_OTP, "otp"},
            {"" + LC.RECRUITMENT_MARKS_COMMITTEE_MEMBERS_SEARCH_EMPLOYEERECORDID, "employee_record_id"},
            {"" + LC.RECRUITMENT_MARKS_COMMITTEE_MEMBERS_SEARCH_UNITID, "unit_id"},
            {"" + LC.RECRUITMENT_MARKS_COMMITTEE_MEMBERS_SEARCH_POSTID, "post_id"},
            {"" + LC.RECRUITMENT_MARKS_COMMITTEE_MEMBERS_SEARCH_EMPLOYEERECORDNAME, "employee_record_name"},
            {"" + LC.RECRUITMENT_MARKS_COMMITTEE_MEMBERS_SEARCH_EMPLOYEERECORDNAMEBN, "employee_record_name_bn"},
            {"" + LC.RECRUITMENT_MARKS_COMMITTEE_MEMBERS_SEARCH_UNITNAME, "unit_name"},
            {"" + LC.RECRUITMENT_MARKS_COMMITTEE_MEMBERS_SEARCH_UNITNAMEBN, "unit_name_bn"},
            {"" + LC.RECRUITMENT_MARKS_COMMITTEE_MEMBERS_SEARCH_POSTNAME, "post_name"},
            {"" + LC.RECRUITMENT_MARKS_COMMITTEE_MEMBERS_SEARCH_POSTNAMEBN, "post_name_bn"},
            {"" + LC.RECRUITMENT_MARKS_COMMITTEE_MEMBERS_SEARCH_INSERTIONDATE, "insertion_date"},
            {"" + LC.RECRUITMENT_MARKS_COMMITTEE_MEMBERS_SEARCH_INSERTEDBY, "inserted_by"},
            {"" + LC.RECRUITMENT_MARKS_COMMITTEE_MEMBERS_SEARCH_MODIFIEDBY, "modified_by"},
            {"" + LC.RECRUITMENT_MARKS_COMMITTEE_MEMBERS_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_PROCUREMENT_PACKAGE = "navPROCUREMENT_PACKAGE";
    public final static String VIEW_PROCUREMENT_PACKAGE = "viewPROCUREMENT_PACKAGE";
    public static final String[][] SEARCH_PROCUREMENT_PACKAGE = {
            {"" + LC.PROCUREMENT_PACKAGE_SEARCH_NAMEEN, "name_en"},
            {"" + LC.PROCUREMENT_PACKAGE_SEARCH_NAMEBN, "name_bn"},
            {"" + LC.PROCUREMENT_PACKAGE_SEARCH_INSERTEDBYUSERID, "inserted_by_user_id"},
            {"" + LC.PROCUREMENT_PACKAGE_SEARCH_INSERTEDBYORGANOGRAMID, "inserted_by_organogram_id"},
            {"" + LC.PROCUREMENT_PACKAGE_SEARCH_INSERTIONDATE, "insertion_date"},
            {"" + LC.PROCUREMENT_PACKAGE_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_PROCUREMENT_GOODS = "navPROCUREMENT_GOODS";
    public final static String VIEW_PROCUREMENT_GOODS = "viewPROCUREMENT_GOODS";
    public static final String[][] SEARCH_PROCUREMENT_GOODS = {
            {"" + LC.PROCUREMENT_GOODS_SEARCH_NAMEEN, "name_en"},
            {"" + LC.PROCUREMENT_GOODS_SEARCH_NAMEBN, "name_bn"},
            {"" + LC.PROCUREMENT_GOODS_SEARCH_PROCUREMENTGOODSTYPEID, "procurement_goods_type_id"},
            {"" + LC.PROCUREMENT_GOODS_SEARCH_UNITPRICE, "unit_price"},
            {"" + LC.PROCUREMENT_GOODS_SEARCH_ESTCOST, "est_cost"},
            {"" + LC.PROCUREMENT_GOODS_SEARCH_METHODANDTYPECAT, "method_and_type_cat"},
            {"" + LC.PROCUREMENT_GOODS_SEARCH_APPROVINGAUTHORITY, "approving_authority"},
            {"" + LC.PROCUREMENT_GOODS_SEARCH_SOURCEOFFUNDCAT, "source_of_fund_cat"},
            {"" + LC.PROCUREMENT_GOODS_SEARCH_TIMECODE, "time_code"},
            {"" + LC.PROCUREMENT_GOODS_SEARCH_TENDER, "tender"},
            {"" + LC.PROCUREMENT_GOODS_SEARCH_TENDEROPENING, "tender_opening"},
            {"" + LC.PROCUREMENT_GOODS_SEARCH_TENDEREVALUATION, "tender_evaluation"},
            {"" + LC.PROCUREMENT_GOODS_SEARCH_APPROVALTOWARD, "approval_toward"},
            {"" + LC.PROCUREMENT_GOODS_SEARCH_AWARDNOTIFICATION, "award_notification"},
            {"" + LC.PROCUREMENT_GOODS_SEARCH_SIGININGOFCONTRACT, "sigining_of_contract"},
            {"" + LC.PROCUREMENT_GOODS_SEARCH_CONTRACTSIGNATURETIME, "contract_signature_time"},
            {"" + LC.PROCUREMENT_GOODS_SEARCH_CONTRACTCOMPLETIONTIME, "contract_completion_time"},
            {"" + LC.PROCUREMENT_GOODS_SEARCH_INSERTEDBYUSERID, "inserted_by_user_id"},
            {"" + LC.PROCUREMENT_GOODS_SEARCH_INSERTEDBYORGANOGRAMID, "inserted_by_organogram_id"},
            {"" + LC.PROCUREMENT_GOODS_SEARCH_INSERTIONDATE, "insertion_date"},
            {"" + LC.PROCUREMENT_GOODS_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_ASSET_LIST = "navASSET_LIST";
    public final static String VIEW_ASSET_LIST = "viewASSET_LIST";
    public static final String[][] SEARCH_ASSET_LIST = {
            {"" + LC.ASSET_LIST_SEARCH_ASSETMODELTYPE, "asset_model_type"},
            {"" + LC.ASSET_LIST_SEARCH_ASSETSUPPLIERTYPE, "asset_supplier_type"},
            {"" + LC.ASSET_LIST_SEARCH_ORDERNO, "order_no"},
            {"" + LC.ASSET_LIST_SEARCH_VENDORNAME, "vendor_name"},
            {"" + LC.ASSET_LIST_SEARCH_VENDORPHONE, "vendor_phone"},
            {"" + LC.ASSET_LIST_SEARCH_VENDOREMAIL, "vendor_email"},
            {"" + LC.ASSET_LIST_SEARCH_SUPPORTDURATION, "support_duration"},
            {"" + LC.ASSET_LIST_SEARCH_TAG, "tag"},
            {"" + LC.ASSET_LIST_SEARCH_PURCHASECOST, "purchase_cost"},
            {"" + LC.ASSET_LIST_SEARCH_WARRANTY, "warranty"},
            {"" + LC.ASSET_LIST_SEARCH_DESCRIPTION, "description"},
            {"" + LC.ASSET_LIST_SEARCH_ISASSIGNED, "is_assigned"},
            {"" + LC.ASSET_LIST_SEARCH_EMPLOYEERECORDSID, "employee_records_id"},
            {"" + LC.ASSET_LIST_SEARCH_STATUS, "status"},
            {"" + LC.ASSET_LIST_SEARCH_INSERTEDBYUSERID, "inserted_by_user_id"},
            {"" + LC.ASSET_LIST_SEARCH_INSERTEDBYORGANOGRAMID, "inserted_by_organogram_id"},
            {"" + LC.ASSET_LIST_SEARCH_INSERTIONDATE, "insertion_date"},
            {"" + LC.ASSET_LIST_SEARCH_LASTMODIFIERUSER, "last_modifier_user"},
            {"" + LC.ASSET_LIST_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_VM_REQUISITION = "navVM_REQUISITION";
    public final static String VIEW_VM_REQUISITION = "viewVM_REQUISITION";
    public static final String[][] SEARCH_VM_REQUISITION = {
            {"" + LC.VM_REQUISITION_SEARCH_INSERTEDBYUSERID, "inserted_by_user_id"},
            {"" + LC.VM_REQUISITION_SEARCH_INSERTEDBYORGANOGRAMID, "inserted_by_organogram_id"},
            {"" + LC.VM_REQUISITION_SEARCH_INSERTIONDATE, "insertion_date"},
            {"" + LC.VM_REQUISITION_SEARCH_REQUESTERORGID, "requester_org_id"},
            {"" + LC.VM_REQUISITION_SEARCH_DECISIONBYORGID, "decision_by_org_id"},
            {"" + LC.VM_REQUISITION_SEARCH_DRIVERORGID, "driver_org_id"},
            {"" + LC.VM_REQUISITION_SEARCH_STARTDATE, "start_date"},
            {"" + LC.VM_REQUISITION_SEARCH_ENDDATE, "end_date"},
            {"" + LC.VM_REQUISITION_SEARCH_STARTADDRESS, "start_address"},
            {"" + LC.VM_REQUISITION_SEARCH_ENDADDRESS, "end_address"},
            {"" + LC.VM_REQUISITION_SEARCH_VEHICLEREQUISITIONPURPOSECAT, "vehicle_requisition_purpose_cat"},
            {"" + LC.VM_REQUISITION_SEARCH_VEHICLETYPECAT, "vehicle_type_cat"},
            {"" + LC.VM_REQUISITION_SEARCH_TRIPDESCRIPTION, "trip_description"},
            {"" + LC.VM_REQUISITION_SEARCH_STATUS, "status"},
            {"" + LC.VM_REQUISITION_SEARCH_DECISIONON, "decision_on"},
            {"" + LC.VM_REQUISITION_SEARCH_DECISIONDESCRIPTION, "decision_description"},
            {"" + LC.VM_REQUISITION_SEARCH_GIVENVEHICLETYPE, "given_vehicle_type"},
            {"" + LC.VM_REQUISITION_SEARCH_GIVENVEHICLEID, "given_vehicle_id"},
            {"" + LC.VM_REQUISITION_SEARCH_RECEIVEDATE, "receive_date"},
            {"" + LC.VM_REQUISITION_SEARCH_TOTALTRIPDISTANCE, "total_trip_distance"},
            {"" + LC.VM_REQUISITION_SEARCH_TOTALTRIPTIME, "total_trip_time"},
            {"" + LC.VM_REQUISITION_SEARCH_PETROLGIVEN, "petrol_given"},
            {"" + LC.VM_REQUISITION_SEARCH_PETROLAMOUNT, "petrol_amount"},
            {"" + LC.VM_REQUISITION_SEARCH_PAYMENTGIVEN, "payment_given"},
            {"" + LC.VM_REQUISITION_SEARCH_PAYMENTTYPE, "payment_type"},
            {"" + LC.VM_REQUISITION_SEARCH_STARTTIME, "start_time"},
            {"" + LC.VM_REQUISITION_SEARCH_ENDTIME, "end_time"},
            {"" + LC.VM_REQUISITION_SEARCH_RECEIVETIME, "receive_time"},
            {"" + LC.VM_REQUISITION_SEARCH_REQUESTEROFFICEID, "requester_office_id"},
            {"" + LC.VM_REQUISITION_SEARCH_DECISIONBYOFFICEID, "decision_by_office_id"},
            {"" + LC.VM_REQUISITION_SEARCH_DRIVEROFFICEID, "driver_office_id"},
            {"" + LC.VM_REQUISITION_SEARCH_REQUESTEROFFICEUNITID, "requester_office_unit_id"},
            {"" + LC.VM_REQUISITION_SEARCH_DECISIONBYOFFICEUNITID, "decision_by_office_unit_id"},
            {"" + LC.VM_REQUISITION_SEARCH_DRIVEROFFICEUNITID, "driver_office_unit_id"},
            {"" + LC.VM_REQUISITION_SEARCH_REQUESTEREMPID, "requester_emp_id"},
            {"" + LC.VM_REQUISITION_SEARCH_DECISIONBYEMPID, "decision_by_emp_id"},
            {"" + LC.VM_REQUISITION_SEARCH_DRIVEREMPID, "driver_emp_id"},
            {"" + LC.VM_REQUISITION_SEARCH_REQUESTERPHONENUM, "requester_phone_num"},
            {"" + LC.VM_REQUISITION_SEARCH_DECISIONBYPHONENUM, "decision_by_phone_num"},
            {"" + LC.VM_REQUISITION_SEARCH_DRIVERPHONENUM, "driver_phone_num"},
            {"" + LC.VM_REQUISITION_SEARCH_REQUESTERNAMEEN, "requester_name_en"},
            {"" + LC.VM_REQUISITION_SEARCH_DECISIONBYNAMEEN, "decision_by_name_en"},
            {"" + LC.VM_REQUISITION_SEARCH_DRIVERNAMEEN, "driver_name_en"},
            {"" + LC.VM_REQUISITION_SEARCH_REQUESTERNAMEBN, "requester_name_bn"},
            {"" + LC.VM_REQUISITION_SEARCH_DECISIONBYNAMEBN, "decision_by_name_bn"},
            {"" + LC.VM_REQUISITION_SEARCH_DRIVERNAMEBN, "driver_name_bn"},
            {"" + LC.VM_REQUISITION_SEARCH_REQUESTEROFFICENAMEEN, "requester_office_name_en"},
            {"" + LC.VM_REQUISITION_SEARCH_DECISIONBYOFFICENAMEEN, "decision_by_office_name_en"},
            {"" + LC.VM_REQUISITION_SEARCH_DRIVEROFFICENAMEEN, "driver_office_name_en"},
            {"" + LC.VM_REQUISITION_SEARCH_REQUESTEROFFICENAMEBN, "requester_office_name_bn"},
            {"" + LC.VM_REQUISITION_SEARCH_DECISIONBYOFFICENAMEBN, "decision_by_office_name_bn"},
            {"" + LC.VM_REQUISITION_SEARCH_DRIVEROFFICENAMEBN, "driver_office_name_bn"},
            {"" + LC.VM_REQUISITION_SEARCH_REQUESTEROFFICEUNITNAMEEN, "requester_office_unit_name_en"},
            {"" + LC.VM_REQUISITION_SEARCH_DECISIONBYOFFICEUNITNAMEEN, "decision_by_office_unit_name_en"},
            {"" + LC.VM_REQUISITION_SEARCH_DRIVEROFFICEUNITNAMEEN, "driver_office_unit_name_en"},
            {"" + LC.VM_REQUISITION_SEARCH_REQUESTEROFFICEUNITNAMEBN, "requester_office_unit_name_bn"},
            {"" + LC.VM_REQUISITION_SEARCH_DECISIONBYOFFICEUNITNAMEBN, "decision_by_office_unit_name_bn"},
            {"" + LC.VM_REQUISITION_SEARCH_DRIVEROFFICEUNITNAMEBN, "driver_office_unit_name_bn"},
            {"" + LC.VM_REQUISITION_SEARCH_REQUESTEROFFICEUNITORGNAMEEN, "requester_office_unit_org_name_en"},
            {"" + LC.VM_REQUISITION_SEARCH_DECISIONBYOFFICEUNITORGNAMEEN, "decision_by_office_unit_org_name_en"},
            {"" + LC.VM_REQUISITION_SEARCH_DRIVEROFFICEUNITORGNAMEEN, "driver_office_unit_org_name_en"},
            {"" + LC.VM_REQUISITION_SEARCH_REQUESTEROFFICEUNITORGNAMEBN, "requester_office_unit_org_name_bn"},
            {"" + LC.VM_REQUISITION_SEARCH_DECISIONBYOFFICEUNITORGNAMEBN, "decision_by_office_unit_org_name_bn"},
            {"" + LC.VM_REQUISITION_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_VM_VEHICLE = "navVM_VEHICLE";
    public final static String VIEW_VM_VEHICLE = "viewVM_VEHICLE";
    public static final String[][] SEARCH_VM_VEHICLE = {
            {"" + LC.VM_VEHICLE_SEARCH_PURCHASEDATE, "purchase_date"},
            {"" + LC.VM_VEHICLE_SEARCH_VEHICLEBRANDCAT, "vehicle_brand_cat"},
            {"" + LC.VM_VEHICLE_SEARCH_VEHICLECOLORCAT, "vehicle_color_cat"},
            {"" + LC.VM_VEHICLE_SEARCH_VEHICLETYPECAT, "vehicle_type_cat"},
            {"" + LC.VM_VEHICLE_SEARCH_REGNO, "reg_no"},
            {"" + LC.VM_VEHICLE_SEARCH_CHASISNO, "chasis_no"},
            {"" + LC.VM_VEHICLE_SEARCH_ENGINENO, "engine_no"},
            {"" + LC.VM_VEHICLE_SEARCH_NUMBEROFSEATS, "number_of_seats"},
            {"" + LC.VM_VEHICLE_SEARCH_VEHICLEFUELCAT, "vehicle_fuel_cat"},
            {"" + LC.VM_VEHICLE_SEARCH_SUPPLIERTYPE, "supplier_type"},
            {"" + LC.VM_VEHICLE_SEARCH_MODELNO, "model_no"},
            {"" + LC.VM_VEHICLE_SEARCH_PURCHASEAMOUNT, "purchase_amount"},
            {"" + LC.VM_VEHICLE_SEARCH_MANUFACTUREYEAR, "manufacture_year"},
            {"" + LC.VM_VEHICLE_SEARCH_FITNESSEXPIRYDATE, "fitness_expiry_date"},
            {"" + LC.VM_VEHICLE_SEARCH_TAXTOKENEXPIRYDATE, "tax_token_expiry_date"},
            {"" + LC.VM_VEHICLE_SEARCH_DIGITALPLATE, "digital_plate"},
            {"" + LC.VM_VEHICLE_SEARCH_STATUS, "status"},
            {"" + LC.VM_VEHICLE_SEARCH_INSERTEDBYUSERID, "inserted_by_user_id"},
            {"" + LC.VM_VEHICLE_SEARCH_INSERTEDBYORGANOGRAMID, "inserted_by_organogram_id"},
            {"" + LC.VM_VEHICLE_SEARCH_INSERTIONDATE, "insertion_date"},
            {"" + LC.VM_VEHICLE_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_VM_REQUISITION_APPROVER = "navVM_REQUISITION_APPROVER";
    public final static String VIEW_VM_REQUISITION_APPROVER = "viewVM_REQUISITION_APPROVER";
    public static final String[][] SEARCH_VM_REQUISITION_APPROVER = {
            {"" + LC.VM_REQUISITION_APPROVER_SEARCH_INSERTEDBYUSERID, "inserted_by_user_id"},
            {"" + LC.VM_REQUISITION_APPROVER_SEARCH_INSERTEDBYORGANOGRAMID, "inserted_by_organogram_id"},
            {"" + LC.VM_REQUISITION_APPROVER_SEARCH_INSERTIONDATE, "insertion_date"},
            {"" + LC.VM_REQUISITION_APPROVER_SEARCH_APPROVERORGID, "approver_org_id"},
            {"" + LC.VM_REQUISITION_APPROVER_SEARCH_APPROVEROFFICEID, "approver_office_id"},
            {"" + LC.VM_REQUISITION_APPROVER_SEARCH_APPROVEROFFICEUNITID, "approver_office_unit_id"},
            {"" + LC.VM_REQUISITION_APPROVER_SEARCH_APPROVEREMPID, "approver_emp_id"},
            {"" + LC.VM_REQUISITION_APPROVER_SEARCH_APPROVERPHONENUM, "approver_phone_num"},
            {"" + LC.VM_REQUISITION_APPROVER_SEARCH_APPROVERNAMEEN, "approver_name_en"},
            {"" + LC.VM_REQUISITION_APPROVER_SEARCH_APPROVERNAMEBN, "approver_name_bn"},
            {"" + LC.VM_REQUISITION_APPROVER_SEARCH_APPROVEROFFICENAMEEN, "approver_office_name_en"},
            {"" + LC.VM_REQUISITION_APPROVER_SEARCH_APPROVEROFFICENAMEBN, "approver_office_name_bn"},
            {"" + LC.VM_REQUISITION_APPROVER_SEARCH_APPROVEROFFICEUNITNAMEEN, "approver_office_unit_name_en"},
            {"" + LC.VM_REQUISITION_APPROVER_SEARCH_APPROVEROFFICEUNITNAMEBN, "approver_office_unit_name_bn"},
            {"" + LC.VM_REQUISITION_APPROVER_SEARCH_APPROVEROFFICEUNITORGNAMEEN, "approver_office_unit_org_name_en"},
            {"" + LC.VM_REQUISITION_APPROVER_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_VM_VEHICLE_OFFICE_ASSIGNMENT = "navVM_VEHICLE_OFFICE_ASSIGNMENT";
    public final static String VIEW_VM_VEHICLE_OFFICE_ASSIGNMENT = "viewVM_VEHICLE_OFFICE_ASSIGNMENT";
    public static final String[][] SEARCH_VM_VEHICLE_OFFICE_ASSIGNMENT = {
            {"" + LC.VM_VEHICLE_OFFICE_ASSIGNMENT_SEARCH_VEHICLETYPECAT, "vehicle_type_cat"},
            {"" + LC.VM_VEHICLE_OFFICE_ASSIGNMENT_SEARCH_VEHICLEID, "vehicle_id"},
            {"" + LC.VM_VEHICLE_OFFICE_ASSIGNMENT_SEARCH_OFFICEID, "office_id"},
            {"" + LC.VM_VEHICLE_OFFICE_ASSIGNMENT_SEARCH_REMARKS, "remarks"},
            {"" + LC.VM_VEHICLE_OFFICE_ASSIGNMENT_SEARCH_INSERTEDBYUSERID, "inserted_by_user_id"},
            {"" + LC.VM_VEHICLE_OFFICE_ASSIGNMENT_SEARCH_INSERTEDBYORGANOGRAMID, "inserted_by_organogram_id"},
            {"" + LC.VM_VEHICLE_OFFICE_ASSIGNMENT_SEARCH_INSERTIONDATE, "insertion_date"},
            {"" + LC.VM_VEHICLE_OFFICE_ASSIGNMENT_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_VM_VEHICLE_DRIVER_ASSIGNMENT = "navVM_VEHICLE_DRIVER_ASSIGNMENT";
    public final static String VIEW_VM_VEHICLE_DRIVER_ASSIGNMENT = "viewVM_VEHICLE_DRIVER_ASSIGNMENT";
    public static final String[][] SEARCH_VM_VEHICLE_DRIVER_ASSIGNMENT = {
            {"" + LC.VM_VEHICLE_DRIVER_ASSIGNMENT_SEARCH_VEHICLETYPECAT, "vehicle_type_cat"},
            {"" + LC.VM_VEHICLE_DRIVER_ASSIGNMENT_SEARCH_VEHICLEID, "vehicle_id"},
            {"" + LC.VM_VEHICLE_DRIVER_ASSIGNMENT_SEARCH_DRIVERID, "driver_id"},
            {"" + LC.VM_VEHICLE_DRIVER_ASSIGNMENT_SEARCH_EMPLOYEERECORDNAME, "employee_record_name"},
            {"" + LC.VM_VEHICLE_DRIVER_ASSIGNMENT_SEARCH_EMPLOYEERECORDNAMEBN, "employee_record_name_bn"},
            {"" + LC.VM_VEHICLE_DRIVER_ASSIGNMENT_SEARCH_UNITNAME, "unit_name"},
            {"" + LC.VM_VEHICLE_DRIVER_ASSIGNMENT_SEARCH_UNITNAMEBN, "unit_name_bn"},
            {"" + LC.VM_VEHICLE_DRIVER_ASSIGNMENT_SEARCH_POSTNAME, "post_name"},
            {"" + LC.VM_VEHICLE_DRIVER_ASSIGNMENT_SEARCH_POSTNAMEBN, "post_name_bn"},
            {"" + LC.VM_VEHICLE_DRIVER_ASSIGNMENT_SEARCH_REMARKS, "remarks"},
            {"" + LC.VM_VEHICLE_DRIVER_ASSIGNMENT_SEARCH_INSERTEDBYUSERID, "inserted_by_user_id"},
            {"" + LC.VM_VEHICLE_DRIVER_ASSIGNMENT_SEARCH_INSERTEDBYORGANOGRAMID, "inserted_by_organogram_id"},
            {"" + LC.VM_VEHICLE_DRIVER_ASSIGNMENT_SEARCH_INSERTIONDATE, "insertion_date"},
            {"" + LC.VM_VEHICLE_DRIVER_ASSIGNMENT_SEARCH_ANYFIELD, "AnyField"}
    };

    public static final Double VM_REQUISITION_PER_KM_FARE = 2.0;
    public static final Double VM_REQUISITION_HOURLY_FARE = 80.0;
    public static final Double VM_REQUISITION_PETROL_PER_LITER = 110.0;


    public static final int VM_REQUISITION_PAYMENT_GIVEN_YES = 1;
    public static final int VM_REQUISITION_PAYMENT_GIVEN_NO = 2;

    public static final int VM_REQUISITION_PAYMENT_TYPE_NAGAD = 1;
    public static final int VM_REQUISITION_PAYMENT_TYPE_CHEQUE = 2;


    public final static String NAV_VM_ROUTE_TRAVEL_WITHDRAW = "navVM_ROUTE_TRAVEL_WITHDRAW";
    public final static String VIEW_VM_ROUTE_TRAVEL_WITHDRAW = "viewVM_ROUTE_TRAVEL_WITHDRAW";
    public static final String[][] SEARCH_VM_ROUTE_TRAVEL_WITHDRAW = {
            {"" + LC.VM_ROUTE_TRAVEL_WITHDRAW_SEARCH_ROUTETRAVELID, "route_travel_id"},
            {"" + LC.VM_ROUTE_TRAVEL_WITHDRAW_SEARCH_ENDDATE, "end_date"},
            {"" + LC.VM_ROUTE_TRAVEL_WITHDRAW_SEARCH_ANYFIELD, "AnyField"}};
    public final static String NAV_VM_ROUTE = "navVM_ROUTE";
    public final static String VIEW_VM_ROUTE = "viewVM_ROUTE";
    public static final String[][] SEARCH_VM_ROUTE = {
            {"" + LC.VM_ROUTE_SEARCH_VEHICLEID, "vehicle_id"},
            {"" + LC.VM_ROUTE_SEARCH_VEHICLEDRIVERASSIGNMENTID, "vehicle_driver_assignment_id"},
            {"" + LC.VM_ROUTE_SEARCH_VEHICLETYPECAT, "vehicle_type_cat"},
            {"" + LC.VM_ROUTE_SEARCH_NAMEEN, "name_en"},
            {"" + LC.VM_ROUTE_SEARCH_NAMEBN, "name_bn"},
            {"" + LC.VM_ROUTE_SEARCH_REMARKS, "remarks"},
            {"" + LC.VM_ROUTE_SEARCH_STATUS, "status"},
            {"" + LC.VM_ROUTE_SEARCH_INSERTEDBYUSERID, "inserted_by_user_id"},
            {"" + LC.VM_ROUTE_SEARCH_INSERTEDBYORGANOGRAMID, "inserted_by_organogram_id"},
            {"" + LC.VM_ROUTE_SEARCH_INSERTIONDATE, "insertion_date"},
            {"" + LC.VM_ROUTE_SEARCH_ANYFIELD, "AnyField"}
    };
    public final static String NAV_PARLIAMENT_GALLERY = "navPARLIAMENT_GALLERY";
    public final static String VIEW_PARLIAMENT_GALLERY = "viewPARLIAMENT_GALLERY";
    public static final String[][] SEARCH_PARLIAMENT_GALLERY = {
            {"" + LC.PARLIAMENT_GALLERY_SEARCH_NAMEENG, "name_eng"},
            {"" + LC.PARLIAMENT_GALLERY_SEARCH_NAMEBNG, "name_bng"},
            {"" + LC.PARLIAMENT_GALLERY_SEARCH_ANYFIELD, "AnyField"}
    };

    /*
     * Card Info
     */
    public final static String NAV_CARD_INFO = "navCARD_INFO";
    public final static String VIEW_CARD_INFO = "viewCARD_INFO";
    public final static String[][] SEARCH_CARD_INFO = {{"", ""}};

    public final static String NAV_TASK_TYPE = "navTASK_TYPE";
    public final static String VIEW_TASK_TYPE = "viewTASK_TYPE";
    public static final String[][] SEARCH_TASK_TYPE = {
            {"" + LC.TASK_TYPE_SEARCH_NAMEEN, "name_en"},
            {"" + LC.TASK_TYPE_SEARCH_NAMEBN, "name_bn"},
            {"" + LC.TASK_TYPE_SEARCH_LEVEL, "level"},
            {"" + LC.TASK_TYPE_SEARCH_ANYFIELD, "AnyField"}
    };

    public final static String NAV_TASK_TYPE_APPROVAL_PATH = "navTASK_TYPE_APPROVAL_PATH";
    public final static String VIEW_TASK_TYPE_APPROVAL_PATH = "viewTASK_TYPE_APPROVAL_PATH";
    public static final String[][] SEARCH_TASK_TYPE_APPROVAL_PATH = {
            {"" + LC.TASK_TYPE_APPROVAL_PATH_SEARCH_TASKTYPEID, "task_type_ID"},
            {"" + LC.TASK_TYPE_APPROVAL_PATH_SEARCH_OFFICEUNITORGANOGRAMID, "office_unit_organogram_id"},
            {"" + LC.TASK_TYPE_APPROVAL_PATH_SEARCH_LEVEL, "level"},
            {"" + LC.TASK_TYPE_APPROVAL_PATH_SEARCH_ANYFIELD, "AnyField"}
    };

    public final static String NAV_TASK_TYPE_ASSIGN = "navTASK_TYPE_ASSIGN";
    public final static String VIEW_TASK_TYPE_ASSIGN = "viewTASK_TYPE_ASSIGN";
    public static final String[][] SEARCH_TASK_TYPE_ASSIGN = {
            {"" + LC.TASK_TYPE_ASSIGN_SEARCH_TASKTYPEID, "task_type_ID"},
            {"" + LC.TASK_TYPE_ASSIGN_SEARCH_ROLEID, "role_ID"},
            {"" + LC.TASK_TYPE_ASSIGN_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_CARD_APPROVAL_MAPPING = "navCARD_APPROVAL_MAPPING";
    public final static String VIEW_CARD_APPROVAL_MAPPING = "viewCARD_APPROVAL_MAPPING";
    public static final String[][] SEARCH_CARD_APPROVAL_MAPPING = {
            {"" + LC.CARD_APPROVAL_MAPPING_SEARCH_CARDINFOID, "card_info_id"},
            {"" + LC.CARD_APPROVAL_MAPPING_SEARCH_CARDAPPROVALID, "card_approval_id"},
            {"" + LC.CARD_APPROVAL_MAPPING_SEARCH_SEQUENCE, "sequence"},
            {"" + LC.CARD_APPROVAL_MAPPING_SEARCH_CARDAPPROVALSTATUSCAT, "card_approval_status_cat"},
            {"" + LC.CARD_APPROVAL_MAPPING_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_VM_ROUTE_TRAVEL = "navVM_ROUTE_TRAVEL";
    public final static String VIEW_VM_ROUTE_TRAVEL = "viewVM_ROUTE_TRAVEL";
    public static final String[][] SEARCH_VM_ROUTE_TRAVEL = {
            {"" + LC.VM_ROUTE_TRAVEL_SEARCH_INSERTEDBYUSERID, "inserted_by_user_id"},
            {"" + LC.VM_ROUTE_TRAVEL_SEARCH_INSERTEDBYORGANOGRAMID, "inserted_by_organogram_id"},
            {"" + LC.VM_ROUTE_TRAVEL_SEARCH_INSERTIONDATE, "insertion_date"},
            {"" + LC.VM_ROUTE_TRAVEL_SEARCH_REQUESTEDSTOPPAGEID, "requested_stoppage_id"},
            {"" + LC.VM_ROUTE_TRAVEL_SEARCH_REQUESTEDSTOPPAGENAME, "requested_stoppage_name"},
            {"" + LC.VM_ROUTE_TRAVEL_SEARCH_REMARKS, "remarks"},
            {"" + LC.VM_ROUTE_TRAVEL_SEARCH_REQUESTEDSTARTDATE, "requested_start_date"},
            {"" + LC.VM_ROUTE_TRAVEL_SEARCH_REQUESTERORGID, "requester_org_id"},
            {"" + LC.VM_ROUTE_TRAVEL_SEARCH_REQUESTEROFFICEID, "requester_office_id"},
            {"" + LC.VM_ROUTE_TRAVEL_SEARCH_REQUESTEROFFICEUNITID, "requester_office_unit_id"},
            {"" + LC.VM_ROUTE_TRAVEL_SEARCH_REQUESTEREMPID, "requester_emp_id"},
            {"" + LC.VM_ROUTE_TRAVEL_SEARCH_REQUESTERPHONENUM, "requester_phone_num"},
            {"" + LC.VM_ROUTE_TRAVEL_SEARCH_REQUESTERNAMEEN, "requester_name_en"},
            {"" + LC.VM_ROUTE_TRAVEL_SEARCH_REQUESTERNAMEBN, "requester_name_bn"},
            {"" + LC.VM_ROUTE_TRAVEL_SEARCH_REQUESTEROFFICENAMEEN, "requester_office_name_en"},
            {"" + LC.VM_ROUTE_TRAVEL_SEARCH_REQUESTEROFFICENAMEBN, "requester_office_name_bn"},
            {"" + LC.VM_ROUTE_TRAVEL_SEARCH_REQUESTEROFFICEUNITNAMEEN, "requester_office_unit_name_en"},
            {"" + LC.VM_ROUTE_TRAVEL_SEARCH_REQUESTEROFFICEUNITNAMEBN, "requester_office_unit_name_bn"},
            {"" + LC.VM_ROUTE_TRAVEL_SEARCH_REQUESTEROFFICEUNITORGNAMEEN, "requester_office_unit_org_name_en"},
            {"" + LC.VM_ROUTE_TRAVEL_SEARCH_REQUESTEROFFICEUNITORGNAMEBN, "requester_office_unit_org_name_bn"},
            {"" + LC.VM_ROUTE_TRAVEL_SEARCH_STATUS, "status"},
            {"" + LC.VM_ROUTE_TRAVEL_SEARCH_ROUTEID, "route_id"},
            {"" + LC.VM_ROUTE_TRAVEL_SEARCH_STOPPAGEID, "stoppage_id"},
            {"" + LC.VM_ROUTE_TRAVEL_SEARCH_FISCALYEARID, "fiscal_year_id"},
            {"" + LC.VM_ROUTE_TRAVEL_SEARCH_APPROVEDSTARTDATE, "approved_start_date"},
            {"" + LC.VM_ROUTE_TRAVEL_SEARCH_DECISIONREMARKS, "decision_remarks"},
            {"" + LC.VM_ROUTE_TRAVEL_SEARCH_APPROVEDDATE, "approved_date"},
            {"" + LC.VM_ROUTE_TRAVEL_SEARCH_WITHDRAWALDATE, "withdrawal_date"},
            {"" + LC.VM_ROUTE_TRAVEL_SEARCH_APPROVERORGID, "approver_org_id"},
            {"" + LC.VM_ROUTE_TRAVEL_SEARCH_APPROVEROFFICEID, "approver_office_id"},
            {"" + LC.VM_ROUTE_TRAVEL_SEARCH_APPROVEROFFICEUNITID, "approver_office_unit_id"},
            {"" + LC.VM_ROUTE_TRAVEL_SEARCH_APPROVEREMPID, "approver_emp_id"},
            {"" + LC.VM_ROUTE_TRAVEL_SEARCH_APPROVERPHONENUM, "approver_phone_num"},
            {"" + LC.VM_ROUTE_TRAVEL_SEARCH_APPROVERNAMEEN, "approver_name_en"},
            {"" + LC.VM_ROUTE_TRAVEL_SEARCH_APPROVERNAMEBN, "approver_name_bn"},
            {"" + LC.VM_ROUTE_TRAVEL_SEARCH_APPROVEROFFICENAMEEN, "approver_office_name_en"},
            {"" + LC.VM_ROUTE_TRAVEL_SEARCH_APPROVEROFFICENAMEBN, "approver_office_name_bn"},
            {"" + LC.VM_ROUTE_TRAVEL_SEARCH_APPROVEROFFICEUNITNAMEEN, "approver_office_unit_name_en"},
            {"" + LC.VM_ROUTE_TRAVEL_SEARCH_APPROVEROFFICEUNITNAMEBN, "approver_office_unit_name_bn"},
            {"" + LC.VM_ROUTE_TRAVEL_SEARCH_APPROVEROFFICEUNITORGNAMEEN, "approver_office_unit_org_name_en"},
            {"" + LC.VM_ROUTE_TRAVEL_SEARCH_APPROVEROFFICEUNITORGNAMEBN, "approver_office_unit_org_name_bn"},
            {"" + LC.VM_ROUTE_TRAVEL_SEARCH_WITHDRAWERORGID, "withdrawer_org_id"},
            {"" + LC.VM_ROUTE_TRAVEL_SEARCH_WITHDRAWEROFFICEID, "withdrawer_office_id"},
            {"" + LC.VM_ROUTE_TRAVEL_SEARCH_WITHDRAWEROFFICEUNITID, "withdrawer_office_unit_id"},
            {"" + LC.VM_ROUTE_TRAVEL_SEARCH_WITHDRAWEREMPID, "withdrawer_emp_id"},
            {"" + LC.VM_ROUTE_TRAVEL_SEARCH_WITHDRAWERPHONENUM, "withdrawer_phone_num"},
            {"" + LC.VM_ROUTE_TRAVEL_SEARCH_WITHDRAWERNAMEEN, "withdrawer_name_en"},
            {"" + LC.VM_ROUTE_TRAVEL_SEARCH_WITHDRAWERNAMEBN, "withdrawer_name_bn"},
            {"" + LC.VM_ROUTE_TRAVEL_SEARCH_WITHDRAWEROFFICENAMEEN, "withdrawer_office_name_en"},
            {"" + LC.VM_ROUTE_TRAVEL_SEARCH_WITHDRAWEROFFICENAMEBN, "withdrawer_office_name_bn"},
            {"" + LC.VM_ROUTE_TRAVEL_SEARCH_WITHDRAWEROFFICEUNITNAMEEN, "withdrawer_office_unit_name_en"},
            {"" + LC.VM_ROUTE_TRAVEL_SEARCH_WITHDRAWEROFFICEUNITNAMEBN, "withdrawer_office_unit_name_bn"},
            {"" + LC.VM_ROUTE_TRAVEL_SEARCH_WITHDRAWEROFFICEUNITORGNAMEEN, "withdrawer_office_unit_org_name_en"},
            {"" + LC.VM_ROUTE_TRAVEL_SEARCH_ANYFIELD, "AnyField"}
    };
    public final static String NAV_TASK_TYPE_LEVEL = "navTASK_TYPE_LEVEL";
    public final static String VIEW_TASK_TYPE_LEVEL = "viewTASK_TYPE_LEVEL";
    public static final String[][] SEARCH_TASK_TYPE_LEVEL = {
            {"" + LC.TASK_TYPE_LEVEL_SEARCH_ANYFIELD, "AnyField"}
    };

    public final static String NAV_VM_ROUTE_TRAVEL_PAYMENT = "navVM_ROUTE_TRAVEL_PAYMENT";
    public final static String VIEW_VM_ROUTE_TRAVEL_PAYMENT = "viewVM_ROUTE_TRAVEL_PAYMENT";
    public static final String[][] SEARCH_VM_ROUTE_TRAVEL_PAYMENT = {
            {"" + LC.VM_ROUTE_TRAVEL_PAYMENT_SEARCH_INSERTEDBYUSERID, "inserted_by_user_id"},
            {"" + LC.VM_ROUTE_TRAVEL_PAYMENT_SEARCH_INSERTEDBYORGANOGRAMID, "inserted_by_organogram_id"},
            {"" + LC.VM_ROUTE_TRAVEL_PAYMENT_SEARCH_INSERTIONDATE, "insertion_date"},
            {"" + LC.VM_ROUTE_TRAVEL_PAYMENT_SEARCH_ROUTETRAVELID, "route_travel_id"},
            {"" + LC.VM_ROUTE_TRAVEL_PAYMENT_SEARCH_FISCALYEARID, "fiscal_year_id"},
            {"" + LC.VM_ROUTE_TRAVEL_PAYMENT_SEARCH_ISPAID, "is_paid"},
            {"" + LC.VM_ROUTE_TRAVEL_PAYMENT_SEARCH_ANYFIELD, "AnyField"}
    };
    public final static String NAV_POLICE_VERIFICATION = "navPOLICE_VERIFICATION";
    public final static String VIEW_POLICE_VERIFICATION = "viewPOLICE_VERIFICATION";
    public static final String[][] SEARCH_POLICE_VERIFICATION = {
            {"" + LC.POLICE_VERIFICATION_SEARCH_VERIFICATIONCAT, "verification_cat"},
            {"" + LC.POLICE_VERIFICATION_SEARCH_CARDINFOID, "card_info_id"},
            {"" + LC.POLICE_VERIFICATION_SEARCH_ANYFIELD, "AnyField"}
    };

    public final static String NAV_VM_VEHICLE_PARTS = "navVM_VEHICLE_PARTS";
    public final static String VIEW_VM_VEHICLE_PARTS = "viewVM_VEHICLE_PARTS";
    public static final String[][] SEARCH_VM_VEHICLE_PARTS = {
            {"" + LC.VM_VEHICLE_PARTS_SEARCH_NAMEBN, "name_bn"},
            {"" + LC.VM_VEHICLE_PARTS_SEARCH_NAMEEN, "name_en"},
            {"" + LC.VM_VEHICLE_PARTS_SEARCH_INSERTEDBYUSERID, "inserted_by_user_id"},
            {"" + LC.VM_VEHICLE_PARTS_SEARCH_INSERTEDBYORGANOGRAMID, "inserted_by_organogram_id"},
            {"" + LC.VM_VEHICLE_PARTS_SEARCH_INSERTIONDATE, "insertion_date"},
            {"" + LC.VM_VEHICLE_PARTS_SEARCH_ANYFIELD, "AnyField"}
    };
    public final static String NAV_APTEST = "navAPTEST";
    public final static String VIEW_APTEST = "viewAPTEST";
    public static final String[][] SEARCH_APTEST = {
            {"" + LC.APTEST_SEARCH_NAMEEN, "name_en"},
            {"" + LC.APTEST_SEARCH_NAMEBN, "name_bn"},
            {"" + LC.APTEST_SEARCH_ANYFIELD, "AnyField"}
    };

    public final static String NAV_VM_FUEL_VENDOR = "navVM_FUEL_VENDOR";
    public final static String VIEW_VM_FUEL_VENDOR = "viewVM_FUEL_VENDOR";
    public static final String[][] SEARCH_VM_FUEL_VENDOR = {
            {"" + LC.VM_FUEL_VENDOR_SEARCH_NAMEBN, "name_bn"},
            {"" + LC.VM_FUEL_VENDOR_SEARCH_NAMEEN, "name_en"},
            {"" + LC.VM_FUEL_VENDOR_SEARCH_MOBILE, "mobile"},
            {"" + LC.VM_FUEL_VENDOR_SEARCH_ADDRESS, "address"},
            {"" + LC.VM_FUEL_VENDOR_SEARCH_CONTRACTSTARTDATE, "contract_start_date"},
            {"" + LC.VM_FUEL_VENDOR_SEARCH_CONTRACTENDDATE, "contract_end_date"},
            {"" + LC.VM_FUEL_VENDOR_SEARCH_STATUS, "status"},
            {"" + LC.VM_FUEL_VENDOR_SEARCH_ANYFIELD, "AnyField"}};
    public final static String NAV_VM_MAINTENANCE = "navVM_MAINTENANCE";
    public final static String VIEW_VM_MAINTENANCE = "viewVM_MAINTENANCE";
    public static final String[][] SEARCH_VM_MAINTENANCE = {
            {"" + LC.VM_MAINTENANCE_SEARCH_VEHICLETYPECAT, "vehicle_type_cat"},
            {"" + LC.VM_MAINTENANCE_SEARCH_STATUS, "status"},
            {"" + LC.VM_MAINTENANCE_SEARCH_VEHICLEID, "vehicle_id"},
            {"" + LC.VM_MAINTENANCE_SEARCH_REQUESTERPOSTID, "requester_post_id"},
            {"" + LC.VM_MAINTENANCE_SEARCH_REQUESTERUNITID, "requester_unit_id"},
            {"" + LC.VM_MAINTENANCE_SEARCH_REQUESTEREMPLOYEERECORDID, "requester_employee_record_id"},
            {"" + LC.VM_MAINTENANCE_SEARCH_VEHICLEDRIVERASSIGNMENTID, "vehicle_driver_assignment_id"},
            {"" + LC.VM_MAINTENANCE_SEARCH_APPLICATIONDATE, "application_date"},
            {"" + LC.VM_MAINTENANCE_SEARCH_LASTMAINTENANCEDATE, "last_maintenance_date"},
            {"" + LC.VM_MAINTENANCE_SEARCH_VEHICLEOFFICENAME, "vehicle_office_name"},
            {"" + LC.VM_MAINTENANCE_SEARCH_VEHICLEOFFICENAMEBN, "vehicle_office_name_bn"},
            {"" + LC.VM_MAINTENANCE_SEARCH_ISCHECKED, "is_checked"},
            {"" + LC.VM_MAINTENANCE_SEARCH_MECHANICREMARKS, "mechanic_remarks"},
            {"" + LC.VM_MAINTENANCE_SEARCH_MECHANICAPPROVEDATE, "mechanic_approve_date"},
            {"" + LC.VM_MAINTENANCE_SEARCH_AOAPPROVEDATE, "ao_approve_date"},
            {"" + LC.VM_MAINTENANCE_SEARCH_VATPERCENTAGE, "vat_percentage"},
            {"" + LC.VM_MAINTENANCE_SEARCH_TOTALWITHOUTVAT, "total_without_vat"},
            {"" + LC.VM_MAINTENANCE_SEARCH_TOTALWITHVAT, "total_with_vat"},
            {"" + LC.VM_MAINTENANCE_SEARCH_TOTALVAT, "total_vat"},
            {"" + LC.VM_MAINTENANCE_SEARCH_PAYMENTRECEIVEDDATE, "payment_received_date"},
            {"" + LC.VM_MAINTENANCE_SEARCH_FISCALYEARID, "fiscal_year_id"},
            {"" + LC.VM_MAINTENANCE_SEARCH_SAROKNUMBER, "sarok_number"},
            {"" + LC.VM_MAINTENANCE_SEARCH_PAYMENTREMARKS, "payment_remarks"},
            {"" + LC.VM_MAINTENANCE_SEARCH_INSERTEDBYUSERID, "inserted_by_user_id"},
            {"" + LC.VM_MAINTENANCE_SEARCH_INSERTEDBYORGANOGRAMID, "inserted_by_organogram_id"},
            {"" + LC.VM_MAINTENANCE_SEARCH_INSERTIONDATE, "insertion_date"},
            {"" + LC.VM_MAINTENANCE_SEARCH_ANYFIELD, "AnyField"}
    };
    public final static String NAV_APPROVAL_PATH = "navAPPROVAL_PATH";
    public final static String VIEW_APPROVAL_PATH = "viewAPPROVAL_PATH";
    public static final String[][] SEARCH_APPROVAL_PATH = {
            {"" + LC.APPROVAL_PATH_SEARCH_NAMEEN, "name_en"},
            {"" + LC.APPROVAL_PATH_SEARCH_NAMEBN, "name_bn"},
            {"" + LC.APPROVAL_PATH_SEARCH_INSERTEDBYUSERID, "inserted_by_user_id"},
            {"" + LC.APPROVAL_PATH_SEARCH_INSERTEDBYORGANOGRAMID, "inserted_by_organogram_id"},
            {"" + LC.APPROVAL_PATH_SEARCH_INSERTIONDATE, "insertion_date"},
            {"" + LC.APPROVAL_PATH_SEARCH_LASTMODIFIERUSER, "last_modifier_user"},
            {"" + LC.APPROVAL_PATH_SEARCH_OFFICEUNITID, "office_unit_id"},
            {"" + LC.APPROVAL_PATH_SEARCH_MODULE, "module"},
            {"" + LC.APPROVAL_PATH_SEARCH_MODULECAT, "module_cat"},
            {"" + LC.APPROVAL_PATH_SEARCH_ISVISIBLE, "isVisible"},
            {"" + LC.APPROVAL_PATH_SEARCH_ANYFIELD, "AnyField"}
    };

    public final static String NAV_VM_FUEL_REQUEST = "navVM_FUEL_REQUEST";
    public final static String VIEW_VM_FUEL_REQUEST = "viewVM_FUEL_REQUEST";
    public static final String[][] SEARCH_VM_FUEL_REQUEST = {
            {"" + LC.VM_FUEL_REQUEST_SEARCH_INSERTEDBYUSERID, "inserted_by_user_id"},
            {"" + LC.VM_FUEL_REQUEST_SEARCH_INSERTEDBYORGANOGRAMID, "inserted_by_organogram_id"},
            {"" + LC.VM_FUEL_REQUEST_SEARCH_INSERTIONDATE, "insertion_date"},
            {"" + LC.VM_FUEL_REQUEST_SEARCH_VEHICLETYPE, "vehicle_type"},
            {"" + LC.VM_FUEL_REQUEST_SEARCH_VEHICLEID, "vehicle_id"},
            {"" + LC.VM_FUEL_REQUEST_SEARCH_APPLICATIONDATE, "application_date"},
            {"" + LC.VM_FUEL_REQUEST_SEARCH_VEHICLEDRIVERASSIGNMENTID, "vehicle_driver_assignment_id"},
            {"" + LC.VM_FUEL_REQUEST_SEARCH_LASTFUELDATE, "last_fuel_date"},
            {"" + LC.VM_FUEL_REQUEST_SEARCH_STATUS, "status"},
            {"" + LC.VM_FUEL_REQUEST_SEARCH_FISCALYEARID, "fiscal_year_id"},
            {"" + LC.VM_FUEL_REQUEST_SEARCH_APPROVEDDATE, "approved_date"},
            {"" + LC.VM_FUEL_REQUEST_SEARCH_VENDORID, "vendor_id"},
            {"" + LC.VM_FUEL_REQUEST_SEARCH_SAROKNUMBER, "sarok_number"},
            {"" + LC.VM_FUEL_REQUEST_SEARCH_PAYMENTREMARKS, "payment_remarks"},
            {"" + LC.VM_FUEL_REQUEST_SEARCH_REQUESTERORGID, "requester_org_id"},
            {"" + LC.VM_FUEL_REQUEST_SEARCH_REQUESTEROFFICEID, "requester_office_id"},
            {"" + LC.VM_FUEL_REQUEST_SEARCH_REQUESTEROFFICEUNITID, "requester_office_unit_id"},
            {"" + LC.VM_FUEL_REQUEST_SEARCH_REQUESTEREMPID, "requester_emp_id"},
            {"" + LC.VM_FUEL_REQUEST_SEARCH_REQUESTERPHONENUM, "requester_phone_num"},
            {"" + LC.VM_FUEL_REQUEST_SEARCH_REQUESTERNAMEEN, "requester_name_en"},
            {"" + LC.VM_FUEL_REQUEST_SEARCH_REQUESTERNAMEBN, "requester_name_bn"},
            {"" + LC.VM_FUEL_REQUEST_SEARCH_REQUESTEROFFICENAMEEN, "requester_office_name_en"},
            {"" + LC.VM_FUEL_REQUEST_SEARCH_REQUESTEROFFICENAMEBN, "requester_office_name_bn"},
            {"" + LC.VM_FUEL_REQUEST_SEARCH_REQUESTEROFFICEUNITNAMEEN, "requester_office_unit_name_en"},
            {"" + LC.VM_FUEL_REQUEST_SEARCH_REQUESTEROFFICEUNITNAMEBN, "requester_office_unit_name_bn"},
            {"" + LC.VM_FUEL_REQUEST_SEARCH_REQUESTEROFFICEUNITORGNAMEEN, "requester_office_unit_org_name_en"},
            {"" + LC.VM_FUEL_REQUEST_SEARCH_REQUESTEROFFICEUNITORGNAMEBN, "requester_office_unit_org_name_bn"},
            {"" + LC.VM_FUEL_REQUEST_SEARCH_APPROVERORGID, "approver_org_id"},
            {"" + LC.VM_FUEL_REQUEST_SEARCH_APPROVEROFFICEID, "approver_office_id"},
            {"" + LC.VM_FUEL_REQUEST_SEARCH_APPROVEROFFICEUNITID, "approver_office_unit_id"},
            {"" + LC.VM_FUEL_REQUEST_SEARCH_APPROVEREMPID, "approver_emp_id"},
            {"" + LC.VM_FUEL_REQUEST_SEARCH_APPROVERPHONENUM, "approver_phone_num"},
            {"" + LC.VM_FUEL_REQUEST_SEARCH_APPROVERNAMEEN, "approver_name_en"},
            {"" + LC.VM_FUEL_REQUEST_SEARCH_APPROVERNAMEBN, "approver_name_bn"},
            {"" + LC.VM_FUEL_REQUEST_SEARCH_APPROVEROFFICENAMEEN, "approver_office_name_en"},
            {"" + LC.VM_FUEL_REQUEST_SEARCH_APPROVEROFFICENAMEBN, "approver_office_name_bn"},
            {"" + LC.VM_FUEL_REQUEST_SEARCH_APPROVEROFFICEUNITNAMEEN, "approver_office_unit_name_en"},
            {"" + LC.VM_FUEL_REQUEST_SEARCH_APPROVEROFFICEUNITNAMEBN, "approver_office_unit_name_bn"},
            {"" + LC.VM_FUEL_REQUEST_SEARCH_APPROVEROFFICEUNITORGNAMEEN, "approver_office_unit_org_name_en"},
            {"" + LC.VM_FUEL_REQUEST_SEARCH_APPROVEROFFICEUNITORGNAMEBN, "approver_office_unit_org_name_bn"},
            {"" + LC.VM_FUEL_REQUEST_SEARCH_PAYMENTRECEIVERORGID, "payment_receiver_org_id"},
            {"" + LC.VM_FUEL_REQUEST_SEARCH_PAYMENTRECEIVEROFFICEID, "payment_receiver_office_id"},
            {"" + LC.VM_FUEL_REQUEST_SEARCH_PAYMENTRECEIVEROFFICEUNITID, "payment_receiver_office_unit_id"},
            {"" + LC.VM_FUEL_REQUEST_SEARCH_PAYMENTRECEIVEREMPID, "payment_receiver_emp_id"},
            {"" + LC.VM_FUEL_REQUEST_SEARCH_PAYMENTRECEIVERPHONENUM, "payment_receiver_phone_num"},
            {"" + LC.VM_FUEL_REQUEST_SEARCH_PAYMENTRECEIVERNAMEEN, "payment_receiver_name_en"},
            {"" + LC.VM_FUEL_REQUEST_SEARCH_PAYMENTRECEIVERNAMEBN, "payment_receiver_name_bn"},
            {"" + LC.VM_FUEL_REQUEST_SEARCH_PAYMENTRECEIVEROFFICENAMEEN, "payment_receiver_office_name_en"},
            {"" + LC.VM_FUEL_REQUEST_SEARCH_PAYMENTRECEIVEROFFICENAMEBN, "payment_receiver_office_name_bn"},
            {"" + LC.VM_FUEL_REQUEST_SEARCH_PAYMENTRECEIVEROFFICEUNITNAMEEN, "payment_receiver_office_unit_name_en"},
            {"" + LC.VM_FUEL_REQUEST_SEARCH_PAYMENTRECEIVEROFFICEUNITNAMEBN, "payment_receiver_office_unit_name_bn"},
            {"" + LC.VM_FUEL_REQUEST_SEARCH_PAYMENTRECEIVEROFFICEUNITORGNAMEEN, "payment_receiver_office_unit_org_name_en"},
            {"" + LC.VM_FUEL_REQUEST_SEARCH_PAYMENTRECEIVEROFFICEUNITORGNAMEBN, "payment_receiver_office_unit_org_name_bn"},
            {"" + LC.VM_FUEL_REQUEST_SEARCH_ANYFIELD, "AnyField"}
    };
    public final static String NAV_PARLIAMENT_VISIT_COST = "navPARLIAMENT_VISIT_COST";
    public final static String VIEW_PARLIAMENT_VISIT_COST = "viewPARLIAMENT_VISIT_COST";
    public static final String[][] SEARCH_PARLIAMENT_VISIT_COST = {
            {"" + LC.PARLIAMENT_VISIT_COST_SEARCH_TYPEOFVISITOR, "type_of_visitor"},
            {"" + LC.PARLIAMENT_VISIT_COST_SEARCH_STARTDATE, "start_date"},
            {"" + LC.PARLIAMENT_VISIT_COST_SEARCH_ENDDATE, "end_date"},
            {"" + LC.PARLIAMENT_VISIT_COST_SEARCH_ANYFIELD, "AnyField"}
    };

    public final static String NAV_VM_MAINTENANCE_VEHICLE_ITEM_MAPPING = "navVM_MAINTENANCE_VEHICLE_ITEM_MAPPING";
    public final static String VIEW_VM_MAINTENANCE_VEHICLE_ITEM_MAPPING = "viewVM_MAINTENANCE_VEHICLE_ITEM_MAPPING";
    public static final String[][] SEARCH_VM_MAINTENANCE_VEHICLE_ITEM_MAPPING = {
            {"" + LC.VM_MAINTENANCE_VEHICLE_ITEM_MAPPING_SEARCH_VMID, "vm_id"},
            {"" + LC.VM_MAINTENANCE_VEHICLE_ITEM_MAPPING_SEARCH_PARTSID, "parts_id"},
            {"" + LC.VM_MAINTENANCE_VEHICLE_ITEM_MAPPING_SEARCH_DATE, "date"},
            {"" + LC.VM_MAINTENANCE_VEHICLE_ITEM_MAPPING_SEARCH_INSERTEDBYUSERID, "inserted_by_user_id"},
            {"" + LC.VM_MAINTENANCE_VEHICLE_ITEM_MAPPING_SEARCH_INSERTEDBYORGANOGRAMID, "inserted_by_organogram_id"},
            {"" + LC.VM_MAINTENANCE_VEHICLE_ITEM_MAPPING_SEARCH_INSERTIONDATE, "insertion_date"},
            {"" + LC.VM_MAINTENANCE_VEHICLE_ITEM_MAPPING_SEARCH_ANYFIELD, "AnyField"}
    };

    public final static String NAV_VISITOR_PASS = "navVISITOR_PASS";
    public final static String VIEW_VISITOR_PASS = "viewVISITOR_PASS";
    public static final String[][] SEARCH_VISITOR_PASS = {
            {"" + LC.VISITOR_PASS_SEARCH_VISITINGDATE, "visiting_date"},
            {"" + LC.VISITOR_PASS_SEARCH_VISITINGTIME, "visiting_time"},
            {"" + LC.VISITOR_PASS_SEARCH_NAME, "name"},
            {"" + LC.VISITOR_PASS_SEARCH_ADDRESS, "address"},
            {"" + LC.VISITOR_PASS_SEARCH_MOBILENUMBER, "mobile_number"},
            {"" + LC.VISITOR_PASS_SEARCH_EMAIL, "email"},
            {"" + LC.VISITOR_PASS_SEARCH_ANYFIELD, "AnyField"}
    };

    public final static String NAV_VM_TAX_TOKEN = "navVM_TAX_TOKEN";
    public final static String VIEW_VM_TAX_TOKEN = "viewVM_TAX_TOKEN";
    public static final String[][] SEARCH_VM_TAX_TOKEN = {
            {"" + LC.VM_TAX_TOKEN_SEARCH_FISCALYEARID, "fiscal_year_id"},
            {"" + LC.VM_TAX_TOKEN_SEARCH_VEHICLETYPECAT, "vehicle_type_cat"},
            {"" + LC.VM_TAX_TOKEN_SEARCH_VAT, "vat"},
            {"" + LC.VM_TAX_TOKEN_SEARCH_ANYFIELD, "AnyField"}
    };
    public final static String NAV_VISITOR_PASS_AFFILIATED_PERSON = "navVISITOR_PASS_AFFILIATED_PERSON";
    public final static String VIEW_VISITOR_PASS_AFFILIATED_PERSON = "viewVISITOR_PASS_AFFILIATED_PERSON";
    public static final String[][] SEARCH_VISITOR_PASS_AFFILIATED_PERSON = {
            {"" + LC.VISITOR_PASS_AFFILIATED_PERSON_SEARCH_NAME, "name"},
            {"" + LC.VISITOR_PASS_AFFILIATED_PERSON_SEARCH_CREDENTIALNO, "credential_no"},
            {"" + LC.VISITOR_PASS_AFFILIATED_PERSON_SEARCH_MOBILENUMBER, "mobile_number"},
            {"" + LC.VISITOR_PASS_AFFILIATED_PERSON_SEARCH_NATIONALITYTYPE, "nationality_type"},
            {"" + LC.VISITOR_PASS_AFFILIATED_PERSON_SEARCH_ANYFIELD, "AnyField"}
    };

    public final static String NAV_VM_TAX_TOKEN_PARENT = "navVM_TAX_TOKEN_PARENT";
    public final static String VIEW_VM_TAX_TOKEN_PARENT = "viewVM_TAX_TOKEN_PARENT";
    public static final String[][] SEARCH_VM_TAX_TOKEN_PARENT = {
            {"" + LC.VM_TAX_TOKEN_PARENT_SEARCH_FISCALYEARID, "fiscal_year_id"},
            {"" + LC.VM_TAX_TOKEN_PARENT_SEARCH_TOTALTAXTOKENFEES, "total_tax_token_fees"},
            {"" + LC.VM_TAX_TOKEN_PARENT_SEARCH_TOTALFITNESSFEES, "total_fitness_fees"},
            {"" + LC.VM_TAX_TOKEN_PARENT_SEARCH_TOTALFEES, "total_fees"},
            {"" + LC.VM_TAX_TOKEN_PARENT_SEARCH_ANYFIELD, "AnyField"}
    };
    public final static String NAV_LOST_CARD_INFO = "navLOST_CARD_INFO";
    public final static String VIEW_LOST_CARD_INFO = "viewLOST_CARD_INFO";
    public static final String[][] SEARCH_LOST_CARD_INFO = {
            {"" + LC.LOST_CARD_INFO_SEARCH_CARDINFOID, "card_info_id"},
            {"" + LC.LOST_CARD_INFO_SEARCH_ANYFIELD, "AnyField"}
    };

    public final static String NAV_ECONOMIC_SUB_CODE = "navECONOMIC_SUB_CODE";
    public final static String VIEW_ECONOMIC_SUB_CODE = "viewECONOMIC_SUB_CODE";
    public static final String[][] SEARCH_ECONOMIC_SUB_CODE = {
            {"" + LC.ECONOMIC_SUB_CODE_SEARCH_ECONOMICCODEID, "economic_code_id"},
            {"" + LC.ECONOMIC_SUB_CODE_SEARCH_CODE, "code"},
            {"" + LC.ECONOMIC_SUB_CODE_SEARCH_DESCRIPTIONEN, "description_en"},
            {"" + LC.ECONOMIC_SUB_CODE_SEARCH_DESCRIPTIONBN, "description_bn"},
            {"" + LC.ECONOMIC_SUB_CODE_SEARCH_ANYFIELD, "AnyField"}
    };

    public final static String NAV_BUDGET_PASSWORD = "navBUDGET_PASSWORD";
    public final static String VIEW_BUDGET_PASSWORD = "viewBUDGET_PASSWORD";
    public static final String[][] SEARCH_BUDGET_PASSWORD = {
            {"" + LC.BUDGET_PASSWORD_SEARCH_BUDGETSELECTIONID, "budget_selection_id"},
            {"" + LC.BUDGET_PASSWORD_SEARCH_BUDGETOFFICEID, "budget_office_id"},
            {"" + LC.BUDGET_PASSWORD_SEARCH_ANYFIELD, "AnyField"}
    };

    public final static String NAV_BUDGET = "navBUDGET";
    public final static String VIEW_BUDGET = "viewBUDGET";
    public static final String[][] SEARCH_BUDGET = {
            {"" + LC.BUDGET_SEARCH_ANYFIELD, "AnyField"}
    };

    public final static String NAV_NEW_USER_TASK = "navNEW_USER_TASK";
    public final static String VIEW_NEW_USER_TASK = "viewNEW_USER_TASK";
    public static final String[][] SEARCH_NEW_USER_TASK = {
            {"" + LC.NEW_USER_TASK_SEARCH_EMPLOYEERECORDSID, "employee_records_id"},
            {"" + LC.NEW_USER_TASK_SEARCH_NOTIFIEDRECORDSID, "notified_records_id"},
            {"" + LC.NEW_USER_TASK_SEARCH_STATUS, "status"},
            {"" + LC.NEW_USER_TASK_SEARCH_ANYFIELD, "AnyField"}
    };

    public final static String NAV_EMPLOYEE_PROVISION = "navNAV_EMPLOYEE_PROVISION";
    public final static String VIEW_EMPLOYEE_PROVISION = "viewVIEW_EMPLOYEE_PROVISION";
    public static final String[][] SEARCH_EMPLOYEE_PROVISION = {
            {"" + LC.NEW_USER_TASK_SEARCH_EMPLOYEERECORDSID, "employee_records_id"},
            {"" + LC.NEW_USER_TASK_SEARCH_ANYFIELD, "AnyField"}
    };

    public final static String NAV_VM_INCIDENT = "navVM_INCIDENT";
    public final static String VIEW_VM_INCIDENT = "viewVM_INCIDENT";
    public static final String[][] SEARCH_VM_INCIDENT = {
            {"" + LC.VM_INCIDENT_SEARCH_INSERTEDBYUSERID, "inserted_by_user_id"},
            {"" + LC.VM_INCIDENT_SEARCH_INSERTEDBYORGANOGRAMID, "inserted_by_organogram_id"},
            {"" + LC.VM_INCIDENT_SEARCH_INSERTIONDATE, "insertion_date"},
            {"" + LC.VM_INCIDENT_SEARCH_VEHICLETYPECAT, "vehicle_type_cat"},
            {"" + LC.VM_INCIDENT_SEARCH_VEHICLEID, "vehicle_id"},
            {"" + LC.VM_INCIDENT_SEARCH_REGNO, "reg_no"},
            {"" + LC.VM_INCIDENT_SEARCH_INCIDENTCAT, "incident_cat"},
            {"" + LC.VM_INCIDENT_SEARCH_INCIDENTDATE, "incident_date"},
            {"" + LC.VM_INCIDENT_SEARCH_INCIDENTPLACE, "incident_place"},
            {"" + LC.VM_INCIDENT_SEARCH_INCIDENTTIME, "incident_time"},
            {"" + LC.VM_INCIDENT_SEARCH_INCIDENTDETAILS, "incident_details"},
            {"" + LC.VM_INCIDENT_SEARCH_DRIVERORGID, "driver_org_id"},
            {"" + LC.VM_INCIDENT_SEARCH_DRIVEROFFICEID, "driver_office_id"},
            {"" + LC.VM_INCIDENT_SEARCH_DRIVEROFFICEUNITID, "driver_office_unit_id"},
            {"" + LC.VM_INCIDENT_SEARCH_DRIVEREMPID, "driver_emp_id"},
            {"" + LC.VM_INCIDENT_SEARCH_DRIVERPHONENUM, "driver_phone_num"},
            {"" + LC.VM_INCIDENT_SEARCH_DRIVERNAMEEN, "driver_name_en"},
            {"" + LC.VM_INCIDENT_SEARCH_DRIVERNAMEBN, "driver_name_bn"},
            {"" + LC.VM_INCIDENT_SEARCH_DRIVEROFFICENAMEEN, "driver_office_name_en"},
            {"" + LC.VM_INCIDENT_SEARCH_DRIVEROFFICENAMEBN, "driver_office_name_bn"},
            {"" + LC.VM_INCIDENT_SEARCH_DRIVEROFFICEUNITNAMEEN, "driver_office_unit_name_en"},
            {"" + LC.VM_INCIDENT_SEARCH_DRIVEROFFICEUNITNAMEBN, "driver_office_unit_name_bn"},
            {"" + LC.VM_INCIDENT_SEARCH_DRIVEROFFICEUNITORGNAMEEN, "driver_office_unit_org_name_en"},
            {"" + LC.VM_INCIDENT_SEARCH_DRIVEROFFICEUNITORGNAMEBN, "driver_office_unit_org_name_bn"},
            {"" + LC.VM_INCIDENT_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_BUSINESS_CARD_INFO = "navBUSINESS_CARD_INFO";
    public final static String VIEW_BUSINESS_CARD_INFO = "viewBUSINESS_CARD_INFO";
    public static final String[][] SEARCH_BUSINESS_CARD_INFO = {
            {"" + LC.BUSINESS_CARD_INFO_SEARCH_CARDSTATUSCAT, "card_status_cat"},
            {"" + LC.BUSINESS_CARD_INFO_SEARCH_CARDEMPLOYEEOFFICEINFOID, "card_employee_office_info_id"},
            {"" + LC.BUSINESS_CARD_INFO_SEARCH_NEXTCARDAPPROVALID, "next_card_approval_id"},
            {"" + LC.BUSINESS_CARD_INFO_SEARCH_CARDEMPLOYEEINFOID, "card_employee_info_id"},
            {"" + LC.BUSINESS_CARD_INFO_SEARCH_TOTALCOUNT, "total_count"},
            {"" + LC.BUSINESS_CARD_INFO_SEARCH_INSERTEDBY, "inserted_by"},
            {"" + LC.BUSINESS_CARD_INFO_SEARCH_INSERTIONTIME, "insertion_time"},
            {"" + LC.BUSINESS_CARD_INFO_SEARCH_MODIFIEDBY, "modified_by"},
            {"" + LC.BUSINESS_CARD_INFO_SEARCH_EMPLOYEERECORDSID, "employee_records_id"},
            {"" + LC.BUSINESS_CARD_INFO_SEARCH_TASKTYPEID, "task_type_id"},
            {"" + LC.BUSINESS_CARD_INFO_SEARCH_ANYFIELD, "AnyField"}
    };

    public final static String NAV_BUSINESS_CARD_APPROVAL_MAPPING = "navBUSINESS_CARD_APPROVAL_MAPPING";
    public final static String VIEW_BUSINESS_CARD_APPROVAL_MAPPING = "viewBUSINESS_CARD_APPROVAL_MAPPING";
    public static final String[][] SEARCH_BUSINESS_CARD_APPROVAL_MAPPING = {
            {"" + LC.CARD_APPROVAL_MAPPING_SEARCH_CARDINFOID, "card_info_id"},
            {"" + LC.CARD_APPROVAL_MAPPING_SEARCH_CARDAPPROVALID, "card_approval_id"},
            {"" + LC.CARD_APPROVAL_MAPPING_SEARCH_SEQUENCE, "sequence"},
            {"" + LC.CARD_APPROVAL_MAPPING_SEARCH_CARDAPPROVALSTATUSCAT, "card_approval_status_cat"},
            {"" + LC.CARD_APPROVAL_MAPPING_SEARCH_ANYFIELD, "AnyField"}
    };


    public final static String NAV_ASSET_CLEARANCE_LETTER = "navASSET_CLEARANCE_LETTER";
    public final static String VIEW_ASSET_CLEARANCE_LETTER = "viewASSET_CLEARANCE_LETTER";
    public static final String[][] SEARCH_ASSET_CLEARANCE_LETTER = {
            {"" + LC.ASSET_CLEARANCE_LETTER_SEARCH_ORGANOGRAMID, "organogram_id"},
            {"" + LC.ASSET_CLEARANCE_LETTER_SEARCH_USERNAME, "user_name"},
            {"" + LC.ASSET_CLEARANCE_LETTER_SEARCH_REQUESTDATE, "request_date"},
            {"" + LC.ASSET_CLEARANCE_LETTER_SEARCH_RESPONSEREQUIREDDATE, "response_required_date"},
            {"" + LC.ASSET_CLEARANCE_LETTER_SEARCH_CLEARANCESTATUSCAT, "clearance_status_cat"},
            {"" + LC.ASSET_CLEARANCE_LETTER_SEARCH_EMAILDECLARATIONSTATUSCAT, "email_declaration_status_cat"},
            {"" + LC.ASSET_CLEARANCE_LETTER_SEARCH_DIRECTORYLOGINSTATUSCAT, "directory_login_status_cat"},
            {"" + LC.ASSET_CLEARANCE_LETTER_SEARCH_PARLIAMENTSOFTWARELOGINSTATUSCAT, "parliament_software_login_status_cat"},
            {"" + LC.ASSET_CLEARANCE_LETTER_SEARCH_REPORTSERVERSTATUSCAT, "report_server_status_cat"},
            {"" + LC.ASSET_CLEARANCE_LETTER_SEARCH_ITASSETHANDOVERSTATUSCAT, "it_asset_handover_status_cat"},
            {"" + LC.ASSET_CLEARANCE_LETTER_SEARCH_REMARKS, "remarks"},
            {"" + LC.ASSET_CLEARANCE_LETTER_SEARCH_INSERTEDBYUSERID, "inserted_by_user_id"},
            {"" + LC.ASSET_CLEARANCE_LETTER_SEARCH_INSERTEDBYORGANOGRAMID, "inserted_by_organogram_id"},
            {"" + LC.ASSET_CLEARANCE_LETTER_SEARCH_INSERTIONDATE, "insertion_date"},
            {"" + LC.ASSET_CLEARANCE_LETTER_SEARCH_ANYFIELD, "AnyField"}
    };

    public final static String BUDGET_SESSION_TOKEN = "budget_session_token";
    public final static int BUDGET_SESSION_TOKEN_LENGTH = 20;
    public final static int BUDGET_PASSWORD_LENGTH = 14;

    public final static String NAV_BUDGET_CODE_SELECTION = "navBUDGET_CODE_SELECTION";
    public final static String VIEW_BUDGET_CODE_SELECTION = "viewBUDGET_CODE_SELECTION";
    public static final String[][] SEARCH_BUDGET_CODE_SELECTION = {
            {"" + LC.CODE_SELECTION_SEARCH_ECONOMIC_YEAR, "budget_selection_info_id"},
            {"" + LC.CODE_SELECTION_SEARCH_BUDGET_OPEATION_CODE, "budget_operation_id"},
    };


    public final static String NAV_COMMITTEES = "navCOMMITTEES";
    public final static String VIEW_COMMITTEES = "viewCOMMITTEES";
    public static final String[][] SEARCH_COMMITTEES = {
            {"" + LC.COMMITTEES_SEARCH_NAMEEN, "name_en"},
            {"" + LC.COMMITTEES_SEARCH_NAMEBN, "name_bn"},
            {"" + LC.COMMITTEES_SEARCH_ANYFIELD, "AnyField"}
    };

    public final static String NAV_MINISTRY_OFFICE = "navMINISTRY_OFFICE";
    public final static String VIEW_MINISTRY_OFFICE = "viewMINISTRY_OFFICE";
    public static final String[][] SEARCH_MINISTRY_OFFICE = {
            {"" + LC.MINISTRY_OFFICE_SEARCH_NAMEEN, "name_en"},
            {"" + LC.MINISTRY_OFFICE_SEARCH_NAMEBN, "name_bn"},
            {"" + LC.MINISTRY_OFFICE_SEARCH_ANYFIELD, "AnyField"}
    };

    public static Map<Integer, Double> VEHICLE_REQUISITION_PRICE_PER_HOUR = new HashMap<Integer, Double>() {{
        put(1, 10.0);
        put(2, 10.0);
        put(3, 15.0);
        put(5, 15.0);
        put(4, 20.0);
        put(6, 0.0);
        put(7, 0.0);
        put(8, 0.0);
        put(9, 0.0);
        put(10, 0.0);

    }};

    public static Map<String, Long> PurchaseStoreMap = new HashMap<String, Long>() {{

        //here Integer is office_units table id

        put("ADMIN_SECTION",43L);    //  প্রশাসন শাখা
        put("COMMON_SERVICE",47L);    //  কমন শাখা
        put("TRANSPORT_SECTION",45L);    //  পরিবহন শাখা
        put("MP_HOSTEL_SHER_E_BANGLA",52L);    //  সদস্য ভবন শাখা
        put("MP_HOSTEL_MANIK_MIA_AVENUE_1_TO_3",1901L);    //  সদস্য ভবন শাখা
        put("MP_HOSTEL_MANIK_MIA_AVENUE_4_TO_6",1902L);    //  সদস্য ভবন শাখা
        put("SERVICE_SECTION",50L);    //   সেবা শাখা

    }};

}
