package fiscal_year;

import feedback_question.Feedback_questionDTO;
import org.apache.log4j.Logger;
import pb.OptionDTO;
import pb.Utils;
import repository.Repository;
import repository.RepositoryManager;
import sessionmanager.SessionConstants;
import util.LockManager;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;


public class Fiscal_yearRepository implements Repository {
    Fiscal_yearDAO fiscal_yearDAO = new Fiscal_yearDAO();

    public void setDAO(Fiscal_yearDAO fiscal_yearDAO) {
        this.fiscal_yearDAO = fiscal_yearDAO;
    }


    static Logger logger = Logger.getLogger(Fiscal_yearRepository.class);
    Map<Long, Fiscal_yearDTO> mapOfFiscal_yearDTOToid;
    Map<String, Set<Fiscal_yearDTO>> mapOfFiscal_yearDTOTonameEn;
    Map<String, Set<Fiscal_yearDTO>> mapOfFiscal_yearDTOTonameBn;
    Map<Long, Set<Fiscal_yearDTO>> mapOfFiscal_yearDTOToorderValue;
    Map<Long, Set<Fiscal_yearDTO>> mapOfFiscal_yearDTOToinsertionDate;
    Map<String, Set<Fiscal_yearDTO>> mapOfFiscal_yearDTOToinsertedBy;
    Map<String, Set<Fiscal_yearDTO>> mapOfFiscal_yearDTOTomodifiedBy;
    Map<Long, Set<Fiscal_yearDTO>> mapOfFiscal_yearDTOTolastModificationTime;


    static Fiscal_yearRepository instance = null;

    private Fiscal_yearRepository() {
        mapOfFiscal_yearDTOToid = new ConcurrentHashMap<>();
//		mapOfFiscal_yearDTOTonameEn = new ConcurrentHashMap<>();
//		mapOfFiscal_yearDTOTonameBn = new ConcurrentHashMap<>();
//		mapOfFiscal_yearDTOToorderValue = new ConcurrentHashMap<>();
//		mapOfFiscal_yearDTOToinsertionDate = new ConcurrentHashMap<>();
//		mapOfFiscal_yearDTOToinsertedBy = new ConcurrentHashMap<>();
//		mapOfFiscal_yearDTOTomodifiedBy = new ConcurrentHashMap<>();
//		mapOfFiscal_yearDTOTolastModificationTime = new ConcurrentHashMap<>();

        RepositoryManager.getInstance().addRepository(this);
    }

    public synchronized static Fiscal_yearRepository getInstance() {
        if (instance == null) {
            instance = new Fiscal_yearRepository();
        }
        return instance;
    }

    public void reload(boolean reloadAll) {
        if (fiscal_yearDAO == null) {
            return;
        }
        try {
            List<Fiscal_yearDTO> fiscal_yearDTOs = fiscal_yearDAO.getAllFiscal_year(reloadAll);
            for (Fiscal_yearDTO fiscal_yearDTO : fiscal_yearDTOs) {
                Fiscal_yearDTO oldFiscal_yearDTO = getFiscal_yearDTOByid(fiscal_yearDTO.id);
                if (oldFiscal_yearDTO != null) {
                    mapOfFiscal_yearDTOToid.remove(oldFiscal_yearDTO.id);


                }
                if (fiscal_yearDTO.isDeleted == 0) {

                    mapOfFiscal_yearDTOToid.put(fiscal_yearDTO.id, fiscal_yearDTO);


                }
            }

        } catch (Exception e) {
            logger.debug("FATAL", e);
        }
    }

    public List<Fiscal_yearDTO> getFiscal_yearList() {
        List<Fiscal_yearDTO> fiscal_years = new ArrayList<Fiscal_yearDTO>(this.mapOfFiscal_yearDTOToid.values());
        return fiscal_years;
    }


    public Fiscal_yearDTO getFiscal_yearDTOByid(long id) {
        if (mapOfFiscal_yearDTOToid.get(id) == null) {
            synchronized (LockManager.getLock(id + "FYR")) {
                if (mapOfFiscal_yearDTOToid.get(id) == null) {
                    Fiscal_yearDTO dto = fiscal_yearDAO.getDTOByID(id);
                    if (dto != null) {
                        mapOfFiscal_yearDTOToid.put(dto.iD, dto);
                    }
                }
            }
        }
        return mapOfFiscal_yearDTOToid.get(id);
    }


    public List<Fiscal_yearDTO> getFiscal_yearDTOByname_en(String name_en) {
        return new ArrayList<>(mapOfFiscal_yearDTOTonameEn.getOrDefault(name_en, new HashSet<>()));
    }


    public List<Fiscal_yearDTO> getFiscal_yearDTOByname_bn(String name_bn) {
        return new ArrayList<>(mapOfFiscal_yearDTOTonameBn.getOrDefault(name_bn, new HashSet<>()));
    }


    public List<Fiscal_yearDTO> getFiscal_yearDTOByorder_value(long order_value) {
        return new ArrayList<>(mapOfFiscal_yearDTOToorderValue.getOrDefault(order_value, new HashSet<>()));
    }


    public List<Fiscal_yearDTO> getFiscal_yearDTOByinsertion_date(long insertion_date) {
        return new ArrayList<>(mapOfFiscal_yearDTOToinsertionDate.getOrDefault(insertion_date, new HashSet<>()));
    }


    public List<Fiscal_yearDTO> getFiscal_yearDTOByinserted_by(String inserted_by) {
        return new ArrayList<>(mapOfFiscal_yearDTOToinsertedBy.getOrDefault(inserted_by, new HashSet<>()));
    }


    public List<Fiscal_yearDTO> getFiscal_yearDTOBymodified_by(String modified_by) {
        return new ArrayList<>(mapOfFiscal_yearDTOTomodifiedBy.getOrDefault(modified_by, new HashSet<>()));
    }


    public List<Fiscal_yearDTO> getFiscal_yearDTOBylastModificationTime(long lastModificationTime) {
        return new ArrayList<>(mapOfFiscal_yearDTOTolastModificationTime.getOrDefault(lastModificationTime, new HashSet<>()));
    }


    @Override
    public String getTableName() {
        String tableName = "";
        try {
            tableName = "fiscal_year";
        } catch (Exception ex) {
            logger.debug("FATAL", ex);
        }
        return tableName;
    }

    public String getText(long id, String language) {
        Fiscal_yearDTO dto = getFiscal_yearDTOByid(id);
        return dto == null ? "" : ("Bangla".equalsIgnoreCase(language) ? dto.nameBn : dto.nameEn);
    }

    public long getFirstDateofFiscalYear(long id) {
        String firstYear = mapOfFiscal_yearDTOToid.get(id).nameEn.split("-")[0];
        String firstDay = "01-07-" + firstYear;
        SimpleDateFormat dateformatter = new SimpleDateFormat("dd-MM-yyyy");
        try {
            long firstDate = dateformatter.parse(firstDay).getTime();
            return firstDate;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return SessionConstants.MIN_DATE;
    }

    public long getLastDateofFiscalYear(long id) {
        String firstYear = mapOfFiscal_yearDTOToid.get(id).nameEn.split("-")[0];
        String lastYear = String.valueOf(Integer.parseInt(firstYear) + 1);
        String firstDay = "30-06-" + lastYear;
        SimpleDateFormat dateformatter = new SimpleDateFormat("dd-MM-yyyy");
        try {
            long firstDate = dateformatter.parse(firstDay).getTime();
            return firstDate;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return SessionConstants.MIN_DATE;
    }

    public int getFirstYear(long id) {
        String firstYear = mapOfFiscal_yearDTOToid.get(id).nameEn.split("-")[0];
        return Integer.parseInt(firstYear);
    }

    public String buildOptions(String language, Long selectedId) {
        List<OptionDTO> optionDTOList = getFiscal_yearList().stream()
                .map(dto -> new OptionDTO(dto.nameEn, dto.nameBn, String.valueOf(dto.id)))
                .collect(Collectors.toList());
        return Utils.buildOptions(optionDTOList, language, selectedId == null ? null : String.valueOf(selectedId));
    }
}


