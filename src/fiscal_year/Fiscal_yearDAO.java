package fiscal_year;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;

import common.ConnectionAndStatementUtil;
import sessionmanager.SessionConstants;
import java.sql.SQLException;


import org.apache.log4j.Logger;

import dbm.*;

import repository.RepositoryManager;

import util.*;
import pb.*;
import user.UserDTO;
import vm_maintenance.Vm_maintenanceDTO;
import vm_route_travel_withdraw.Vm_route_travel_withdrawDTO;

public class Fiscal_yearDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());
	

	
	public Fiscal_yearDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";
		commonMaps = new Fiscal_yearMAPS(tableName);
		columnNames = new String[] 
		{
			"id",
			"name_en",
			"name_bn",
			"order_value",
			"insertion_date",
			"inserted_by",
			"modified_by",
			"isDeleted",
			"lastModificationTime"
		};
	}
	
	public Fiscal_yearDAO()
	{
		this("fiscal_year");		
	}
	
	public void setSearchColumn(Fiscal_yearDTO fiscal_yearDTO)
	{
		fiscal_yearDTO.searchColumn = "";
		
	}
	
	public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException
	{
		Fiscal_yearDTO fiscal_yearDTO = (Fiscal_yearDTO)commonDTO;
		int index = 1;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(fiscal_yearDTO);
		if(isInsert)
		{
			ps.setObject(index++,fiscal_yearDTO.iD);
		}
		ps.setObject(index++,fiscal_yearDTO.nameEn);
		ps.setObject(index++,fiscal_yearDTO.nameBn);
		ps.setObject(index++,fiscal_yearDTO.orderValue);
		ps.setObject(index++,fiscal_yearDTO.insertionDate);
		ps.setObject(index++,fiscal_yearDTO.insertedBy);
		ps.setObject(index++,fiscal_yearDTO.modifiedBy);
		if(isInsert)
		{
			ps.setObject(index++, 0);
			ps.setObject(index++, lastModificationTime);
		}
	}
	
	public void get(Fiscal_yearDTO fiscal_yearDTO, ResultSet rs) throws SQLException
	{
		fiscal_yearDTO.id = rs.getLong("id");
		fiscal_yearDTO.nameEn = rs.getString("name_en");
		fiscal_yearDTO.nameBn = rs.getString("name_bn");
		fiscal_yearDTO.orderValue = rs.getLong("order_value");
		fiscal_yearDTO.insertionDate = rs.getLong("insertion_date");
		fiscal_yearDTO.insertedBy = rs.getString("inserted_by");
		fiscal_yearDTO.modifiedBy = rs.getString("modified_by");
		fiscal_yearDTO.isDeleted = rs.getInt("isDeleted");
		fiscal_yearDTO.lastModificationTime = rs.getLong("lastModificationTime");
	}

	public Fiscal_yearDTO build(ResultSet rs)
	{
		try
		{
			Fiscal_yearDTO fiscal_yearDTO = new Fiscal_yearDTO();

			fiscal_yearDTO.id = rs.getLong("id");
			fiscal_yearDTO.nameEn = rs.getString("name_en");
			fiscal_yearDTO.nameBn = rs.getString("name_bn");
			fiscal_yearDTO.orderValue = rs.getLong("order_value");
			fiscal_yearDTO.insertionDate = rs.getLong("insertion_date");
			fiscal_yearDTO.insertedBy = rs.getString("inserted_by");
			fiscal_yearDTO.modifiedBy = rs.getString("modified_by");
			fiscal_yearDTO.isDeleted = rs.getInt("isDeleted");
			fiscal_yearDTO.lastModificationTime = rs.getLong("lastModificationTime");

			return fiscal_yearDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
	
	
	
	
		
	

	//need another getter for repository
	public Fiscal_yearDTO getDTOByID (long ID)
	{

		String sql = "SELECT * ";

		sql += " FROM " + tableName;

		sql += " WHERE id = ? ";// + ID;

		printSql(sql);

		Fiscal_yearDTO fiscal_yearDTO = null;

		fiscal_yearDTO =
				ConnectionAndStatementUtil.getT(sql, Arrays.asList(ID), this::build);
		return fiscal_yearDTO;
	}

	public Fiscal_yearDTO getDTOByFiscalYear (String  fiscalYear)
	{

		String sql = "SELECT * ";

		sql += " FROM " + tableName;

		//sql += " WHERE name_en like '%" + fiscalYear + "%'";

		String fY = "%"+fiscalYear+"%";

		sql += " WHERE name_en like ? " ;

		printSql(sql);

		Fiscal_yearDTO fiscal_yearDTO = null;

		fiscal_yearDTO =
				ConnectionAndStatementUtil.getT(sql, Arrays.asList(fY), this::build);
		return fiscal_yearDTO;
	}
	
	
	
	
	public List<Fiscal_yearDTO> getDTOs(Collection recordIDs){

		if(recordIDs.isEmpty()){
			return new ArrayList<>();
		}
		String sql = "SELECT * FROM " + tableName + " WHERE ID IN ( ";
		for(int i = 0;i<recordIDs.size();i++){
			if(i!=0){
				sql+=",";
			}
			sql+= " ? ";
		}
		sql+=")  order by lastModificationTime desc";
		printSql(sql);
		return ConnectionAndStatementUtil.getListOfT(sql, Arrays.asList(recordIDs.toArray()),this::build);

	}
	
	

	
	
	
	//add repository
	public List<Fiscal_yearDTO> getAllFiscal_year (boolean isFirstReload)
    {

		String sql = "SELECT * FROM fiscal_year ";
		sql += " WHERE ";


		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;
		}
		sql += " order by fiscal_year.id ASC";
		printSql(sql);
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);
    }

	
	public List<Fiscal_yearDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO)
	{
		return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
	}

	
	public List<Fiscal_yearDTO> getDTOs(Hashtable  p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
			String filter, boolean tableHasJobCat)
	{

		String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat);
		printSql(sql);
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);

	}
	public String getSqlWithSearchCriteriaForNormalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
			UserDTO userDTO, String filter, boolean tableHasJobCat)
    {
		boolean viewAll = false;
		
		if(p_searchCriteria != null && p_searchCriteria.get("ViewAll") != null)
		{
			System.out.println("ViewAll = " + p_searchCriteria.get("ViewAll"));
			viewAll = true;
		}
		else
		{
			System.out.println("ViewAll is null ");
		}
		
		String sql = "SELECT ";
		if(category == GETIDS)
		{
			sql += " " + tableName + ".ID as ID ";
		}
		else if(category == GETCOUNT)
		{
			sql += " count( " + tableName + ".ID) as countID ";
		}
		else if(category == GETDTOS)
		{
			sql += " distinct " + tableName + ".* ";
		}
		sql += "FROM " + tableName + " ";
		sql += joinSQL;
		

		
		String AnyfieldSql = "";
		String AllFieldSql = "";
		
		if(p_searchCriteria != null)
		{
						
			
			Enumeration names = p_searchCriteria.keys();
			String str, value;
			
			AnyfieldSql = "(";
			
			if(p_searchCriteria.get("AnyField")!= null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase(""))
			{
				AnyfieldSql+= tableName + ".search_column like '%" + p_searchCriteria.get("AnyField").toString() + "%'";			
			}
			AnyfieldSql += ")";
			System.out.println("AnyfieldSql = " + AnyfieldSql);
			
			AllFieldSql = "(";
			int i = 0;
			while(names.hasMoreElements())
			{				
				str = (String) names.nextElement();
				value = (String)p_searchCriteria.get(str);
		        System.out.println(str + ": " + value);
				if(value != null && !value.equalsIgnoreCase("") && (
						 str.equals("insertion_date_start")
						|| str.equals("insertion_date_end")
				)
						
				)
				{
					if(p_searchCriteria.get(str).equals("any"))
					{
						continue;
					}

					if( i > 0)
					{
						AllFieldSql+= " AND  ";
					}
					
					 if(str.equals("insertion_date_start"))
					{
						AllFieldSql += "" + tableName + ".insertion_date >= " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("insertion_date_end"))
					{
						AllFieldSql += "" + tableName + ".insertion_date <= " + p_searchCriteria.get(str);
						i ++;
					}
					
					
				}
			}
			
			AllFieldSql += ")";
			System.out.println("AllFieldSql = " + AllFieldSql);
			
			
		}
		
		
		sql += " WHERE ";
		
		sql += " (" + tableName + ".isDeleted = 0 ";			
		sql += ")";
		
		
		if(!filter.equalsIgnoreCase(""))
		{
			sql += " and " + filter + " ";
		}
		
		if(!AnyfieldSql.equals("()") && !AnyfieldSql.equals(""))
		{
			sql += " AND " + AnyfieldSql;
			
		}
		if(!AllFieldSql.equals("()") && !AllFieldSql.equals(""))
		{			
			sql += " AND " + AllFieldSql;
		}
		
	
			
		sql += " order by " + tableName + ".name_en desc ";

		printSql(sql);
		
		if(limit >= 0)
		{
			sql += " limit " + limit;
		}
		if(offset >= 0)
		{
			sql += " offset " + offset;
		}
		
		System.out.println("-------------- sql = " + sql);
		
		return sql;
    }
	
	public String getSqlWithSearchCriteria(Hashtable p_searchCriteria, int limit, int offset, int category,
			boolean isPermanentTable, UserDTO userDTO, String filter, boolean tableHasJobCat)
    {		
		return getSqlWithSearchCriteriaForNormalSearch(p_searchCriteria, limit, offset, category, userDTO, filter, tableHasJobCat);	
    }

    public Long getPrevFiscalYearID(Long fiscalYearID){
		Long prevFiscalYearID = fiscalYearID<=1?fiscalYearID:(fiscalYearID-1);
		return prevFiscalYearID;
	}
	public Fiscal_yearDTO getPrevFiscalYearDTO(Long fiscalYearID){
		Long prevFiscalYearID = fiscalYearID<=1?fiscalYearID:(fiscalYearID-1);
		Fiscal_yearDTO getPrevDTOByID = getDTOByID(prevFiscalYearID);
		return getPrevDTOByID;
	}

	public Fiscal_yearDTO getFiscalYearBYDateString(String date){ //date format: dd-MM-yy eg. 19-05-2020
		 String[] arr = date.split("-");
		 int month = Integer.parseInt(arr[1]);
		 int year = Integer.parseInt(arr[2]);
		 String fiscalYear = "";
		 if(month<7){
		 	fiscalYear = year-1+"-"+(year - 2000);
		 }
		 else{
			 fiscalYear = year+"-"+(year+1 - 2000);
		 }

		Fiscal_yearDTO getDTOFiscalYear = getDTOByFiscalYear(fiscalYear);
		return getDTOFiscalYear;

	}

	public Fiscal_yearDTO getFiscalYearBYDateLong(Long date){

		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		String dateString = formatter.format(new Date(date));
		Fiscal_yearDTO getDTOFiscalYear = getFiscalYearBYDateString(dateString);
		return getDTOFiscalYear;

	}

	public Fiscal_yearDTO getFiscalYearFromList(List<Fiscal_yearDTO> fiscal_yearDTOS,Long ID){


		Fiscal_yearDTO getDTOFiscalYear = fiscal_yearDTOS.stream()
				                          .filter(dto->dto.id==ID)
				                          .findFirst()
				                          .orElse(null);
		return getDTOFiscalYear;

	}

				
}
	