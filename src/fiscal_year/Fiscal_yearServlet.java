package fiscal_year;

import java.io.IOException;
import java.io.*;
import java.text.SimpleDateFormat;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import org.apache.log4j.Logger;

import login.LoginDTO;
import permission.MenuConstants;
import role.PermissionRepository;


import sessionmanager.SessionConstants;

import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;
import util.CommonRequestHandler;
import util.RecordNavigationManager4;

import java.util.*;
import javax.servlet.http.*;
import java.util.UUID;





import geolocation.GeoLocationDAO2;
import java.util.StringTokenizer;

import com.google.gson.Gson;

import pb.*;
import pbReport.*;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;




/**
 * Servlet implementation class Fiscal_yearServlet
 */
@WebServlet("/Fiscal_yearServlet")
@MultipartConfig
public class Fiscal_yearServlet extends HttpServlet 
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Fiscal_yearServlet.class);

    String tableName = "fiscal_year";

	Fiscal_yearDAO fiscal_yearDAO;
	CommonRequestHandler commonRequestHandler;
    private final Gson gson = new Gson();
    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Fiscal_yearServlet() 
	{
        super();
    	try
    	{
			fiscal_yearDAO = new Fiscal_yearDAO(tableName);
			commonRequestHandler = new CommonRequestHandler(fiscal_yearDAO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }   

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
//	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
//	{
//		System.out.println("In doget request = " + request);
//		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
//		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
//		boolean isPermanentTable = true;
//		if(request.getParameter("isPermanentTable") != null)
//		{
//			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
//		}
//		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
//		try
//		{
//			String actionType = request.getParameter("actionType");
//			if(actionType.equals("getAddPage"))
//			{
//				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.FISCAL_YEAR_ADD))
//				{
//					commonRequestHandler.getAddPage(request, response);
//				}
//				else
//				{
//					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
//				}
//			}
//			else if(actionType.equals("getEditPage"))
//			{
//				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.FISCAL_YEAR_UPDATE))
//				{
//					getFiscal_year(request, response);
//				}
//				else
//				{
//					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
//				}
//			}
//			else if(actionType.equals("getURL"))
//			{
//				String URL = request.getParameter("URL");
//				System.out.println("URL = " + URL);
//				response.sendRedirect(URL);
//			}
//			else if(actionType.equals("search"))
//			{
//				System.out.println("search requested");
//				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.FISCAL_YEAR_SEARCH))
//				{
//					if(isPermanentTable)
//					{
//						String filter = request.getParameter("filter");
//						System.out.println("filter = " + filter);
//						if(filter!=null)
//						{
//							filter = ""; //shouldn't be directly used, rather manipulate it.
//							searchFiscal_year(request, response, isPermanentTable, filter);
//						}
//						else
//						{
//							searchFiscal_year(request, response, isPermanentTable, "");
//						}
//					}
//					else
//					{
//						//searchFiscal_year(request, response, tempTableName, isPermanentTable);
//					}
//				}
//			}
//			else if(actionType.equals("view"))
//			{
//				System.out.println("view requested");
//				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.FISCAL_YEAR_SEARCH))
//				{
//					commonRequestHandler.view(request, response);
//				}
//				else
//				{
//					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
//				}
//
//			}
//			else
//			{
//				request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
//			}
//		}
//		catch(Exception ex)
//		{
//			ex.printStackTrace();
//			logger.debug(ex);
//		}
//	}


	
	

//	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
//	{
//		// TODO Auto-generated method stub
//		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
//		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
//		System.out.println("doPost");
//		boolean isPermanentTable = true;
//		if(request.getParameter("isPermanentTable") != null)
//		{
//			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
//		}
//		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
//
//		try
//		{
//			String actionType = request.getParameter("actionType");
//			System.out.println("actionType = " + actionType);
//			if(actionType.equals("add"))
//			{
//
//				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.FISCAL_YEAR_ADD))
//				{
//					System.out.println("going to  addFiscal_year ");
//					addFiscal_year(request, response, true, userDTO, true);
//				}
//				else
//				{
//					System.out.println("Not going to  addFiscal_year ");
//					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
//				}
//
//			}
//
//			else if(actionType.equals("getDTO"))
//			{
//
//				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.FISCAL_YEAR_ADD))
//				{
//					getDTO(request, response);
//				}
//				else
//				{
//					System.out.println("Not going to  addFiscal_year ");
//					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
//				}
//
//			}
//			else if(actionType.equals("edit"))
//			{
//
//				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.FISCAL_YEAR_UPDATE))
//				{
//					addFiscal_year(request, response, false, userDTO, isPermanentTable);
//				}
//				else
//				{
//					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
//				}
//			}
//			else if(actionType.equals("delete"))
//			{
//				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.FISCAL_YEAR_UPDATE))
//				{
//					commonRequestHandler.delete(request, response, userDTO);
//				}
//				else
//				{
//					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
//				}
//			}
//			else if(actionType.equals("search"))
//			{
//				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.FISCAL_YEAR_SEARCH))
//				{
//					searchFiscal_year(request, response, true, "");
//				}
//				else
//				{
//					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
//				}
//			}
//			else if(actionType.equals("getGeo"))
//			{
//				System.out.println("going to geoloc ");
//				request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
//			}
//
//		}
//		catch(Exception ex)
//		{
//			ex.printStackTrace();
//			logger.debug(ex);
//		}
//	}
	
	private void getDTO(HttpServletRequest request, HttpServletResponse response) 
	{
		try 
		{
			System.out.println("In getDTO");
			Fiscal_yearDTO fiscal_yearDTO = fiscal_yearDAO.getDTOByID(Long.parseLong(request.getParameter("ID")));
			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			
			String encoded = this.gson.toJson(fiscal_yearDTO);
			System.out.println("json encoded data = " + encoded);
			out.print(encoded);
			out.flush();
		}
		catch (NumberFormatException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	private void addFiscal_year(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, boolean isPermanentTable) throws IOException 
	{
		// TODO Auto-generated method stub
		try 
		{
			request.setAttribute("failureMessage", "");
			System.out.println("%%%% addFiscal_year");
			String path = getServletContext().getRealPath("/img2/");
			Fiscal_yearDTO fiscal_yearDTO;
			SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
						
			if(addFlag == true)
			{
				fiscal_yearDTO = new Fiscal_yearDTO();
			}
			else
			{
				fiscal_yearDTO = fiscal_yearDAO.getDTOByID(Long.parseLong(request.getParameter("iD")));
			}
			
			String Value = "";

			Value = request.getParameter("nameEn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("nameEn = " + Value);
			if(Value != null)
			{
				fiscal_yearDTO.nameEn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("nameBn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("nameBn = " + Value);
			if(Value != null)
			{
				fiscal_yearDTO.nameBn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("orderValue");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("orderValue = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				fiscal_yearDTO.orderValue = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			if(addFlag)
			{
				Calendar c = Calendar.getInstance();
				c.set(Calendar.HOUR_OF_DAY, 0);
				c.set(Calendar.MINUTE, 0);
				c.set(Calendar.SECOND, 0);
				c.set(Calendar.MILLISECOND, 0);
				
				fiscal_yearDTO.insertionDate = c.getTimeInMillis();
			}			


			Value = request.getParameter("insertedBy");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("insertedBy = " + Value);
			if(Value != null)
			{
				fiscal_yearDTO.insertedBy = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("modifiedBy");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("modifiedBy = " + Value);
			if(Value != null)
			{
				fiscal_yearDTO.modifiedBy = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			
			System.out.println("Done adding  addFiscal_year dto = " + fiscal_yearDTO);
			long returnedID = -1;
			
			if(isPermanentTable == false) //add new row for validation and make the old row outdated
			{
				fiscal_yearDAO.setIsDeleted(fiscal_yearDTO.iD, CommonDTO.OUTDATED);
				returnedID = fiscal_yearDAO.add(fiscal_yearDTO);
				fiscal_yearDAO.setIsDeleted(returnedID, CommonDTO.WAITING_FOR_APPROVAL);
			}
			else if(addFlag == true)
			{
				returnedID = fiscal_yearDAO.manageWriteOperations(fiscal_yearDTO, SessionConstants.INSERT, -1, userDTO);
			}
			else
			{				
				returnedID = fiscal_yearDAO.manageWriteOperations(fiscal_yearDTO, SessionConstants.UPDATE, -1, userDTO);											
			}
			
			
			
			
			
			
			
			
			
			if(isPermanentTable)
			{
				String inPlaceSubmit = request.getParameter("inplacesubmit");
				
				if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
				{
					getFiscal_year(request, response, returnedID);
				}
				else
				{
					response.sendRedirect("Fiscal_yearServlet?actionType=search");
				}
			}
			else
			{
				commonRequestHandler.validate(fiscal_yearDAO.getDTOByID(returnedID), request, response, userDTO);
			}
					
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	



	
	
	

	private void getFiscal_year(HttpServletRequest request, HttpServletResponse response, long id) throws ServletException, IOException
	{
		System.out.println("in getFiscal_year");
		Fiscal_yearDTO fiscal_yearDTO = null;
		try 
		{
			fiscal_yearDTO = fiscal_yearDAO.getDTOByID(id);
			request.setAttribute("ID", fiscal_yearDTO.iD);
			request.setAttribute("fiscal_yearDTO",fiscal_yearDTO);
			request.setAttribute("fiscal_yearDAO",fiscal_yearDAO);
			
			String URL= "";
			
			String inPlaceEdit = request.getParameter("inplaceedit");
			String inPlaceSubmit = request.getParameter("inplacesubmit");
			String getBodyOnly = request.getParameter("getBodyOnly");
			
			if(inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase(""))
			{
				URL = "fiscal_year/fiscal_yearInPlaceEdit.jsp";	
				request.setAttribute("inplaceedit","");				
			}
			else if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
			{
				URL = "fiscal_year/fiscal_yearSearchRow.jsp";
				request.setAttribute("inplacesubmit","");					
			}
			else
			{
				if(getBodyOnly  != null && !getBodyOnly.equalsIgnoreCase(""))
				{
					URL = "fiscal_year/fiscal_yearEditBody.jsp?actionType=edit";
				}
				else
				{
					URL = "fiscal_year/fiscal_yearEdit.jsp?actionType=edit";
				}				
			}
			
			RequestDispatcher rd = request.getRequestDispatcher(URL);
			rd.forward(request, response);
		}
		catch (NumberFormatException e) 
		{
			e.printStackTrace();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	
	private void getFiscal_year(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		getFiscal_year(request, response, Long.parseLong(request.getParameter("ID")));	
	}
	
//	private void searchFiscal_year(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException
//	{
//		System.out.println("in  searchFiscal_year 1");
//		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
//		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
//		String ajax = (String)request.getParameter("ajax");
//		boolean hasAjax = false;
//		if(ajax != null && !ajax.equalsIgnoreCase(""))
//		{
//			hasAjax = true;
//		}
//		System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);
//
//        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
//			SessionConstants.NAV_FISCAL_YEAR,
//			request,
//			fiscal_yearDAO,
//			SessionConstants.VIEW_FISCAL_YEAR,
//			SessionConstants.SEARCH_FISCAL_YEAR,
//			tableName,
//			isPermanent,
//			userDTO,
//			filter,
//			true);
//        try
//        {
//			System.out.println("trying to dojob");
//            rnManager.doJob(loginDTO);
//        }
//        catch(Exception e)
//        {
//			System.out.println("failed to dojob" + e);
//        }
//
//		request.setAttribute("fiscal_yearDAO",fiscal_yearDAO);
//        RequestDispatcher rd;
//        if(!isPermanent)
//        {
//        	if(hasAjax == false)
//	        {
//	        	System.out.println("Going to fiscal_year/fiscal_yearApproval.jsp");
//	        	rd = request.getRequestDispatcher("fiscal_year/fiscal_yearApproval.jsp");
//	        }
//	        else
//	        {
//	        	System.out.println("Going to fiscal_year/fiscal_yearApprovalForm.jsp");
//	        	rd = request.getRequestDispatcher("fiscal_year/fiscal_yearApprovalForm.jsp");
//	        }
//        }
//        else
//        {
//	        if(hasAjax == false)
//	        {
//	        	System.out.println("Going to fiscal_year/fiscal_yearSearch.jsp");
//	        	rd = request.getRequestDispatcher("fiscal_year/fiscal_yearSearch.jsp");
//	        }
//	        else
//	        {
//	        	System.out.println("Going to fiscal_year/fiscal_yearSearchForm.jsp");
//	        	rd = request.getRequestDispatcher("fiscal_year/fiscal_yearSearchForm.jsp");
//	        }
//        }
//		rd.forward(request, response);
//	}
	
}

