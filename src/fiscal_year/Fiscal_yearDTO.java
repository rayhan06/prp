package fiscal_year;
import java.util.*; 
import util.*; 


public class Fiscal_yearDTO extends CommonDTO
{

	public long id = -1;
    public String nameEn = "";
    public String nameBn = "";
	public long orderValue = -1;
	public long insertionDate = -1;
    public String insertedBy = "";
    public String modifiedBy = "";
	
	
    @Override
	public String toString() {
            return "$Fiscal_yearDTO[" +
            " id = " + id +
            " nameEn = " + nameEn +
            " nameBn = " + nameBn +
            " orderValue = " + orderValue +
            " insertionDate = " + insertionDate +
            " insertedBy = " + insertedBy +
            " modifiedBy = " + modifiedBy +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}