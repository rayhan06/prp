package office_head_report;


import com.google.gson.Gson;
import language.LC;
import language.LM;
import login.LoginDTO;
import office_units.Office_unitsRepository;
import pbReport.ReportService;
import permission.MenuConstants;
import sessionmanager.SessionConstants;
import util.HttpRequestUtils;
import util.ReportRequestHandler;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@WebServlet("/Office_head_report_Servlet")
public class Office_head_report_Servlet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    private final String[][] Criteria =
            {
                    {"criteria", "", "isdeleted", "=", "", "String", "", "", "0", "isdeleted", ""},
                    {"criteria", "", "is_abstract_office ", "=", "AND", "int", "", "", "0", "none", ""},
                    {"criteria", "", "id", "in", "AND", "int", "", "", "any", "none", LC.HM_OFFICE + ""}
            };

    private final String[][] Display =
            {
                    {"display", "", "id", "office_unit", ""},
                    {"display", "", "id", "office_head", ""}
            };

    String GroupBy = "";
    String OrderBY = "";

    ReportRequestHandler reportRequestHandler;

    private final ReportService reportService = new ReportService();

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;

        String sql = "office_units";

        Display[0][4] = LM.getText(LC.HM_OFFICE, loginDTO);
        Display[1][4] = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng ? "Office Head" : "অফিস প্রধান";

        String officeUnitIds = request.getParameter("officeUnitIds");
        if (officeUnitIds == null || officeUnitIds.equalsIgnoreCase("")) {
            Criteria[2][SessionConstants.REPORT_OPERATOR_POS] = "=";
            Criteria[2][SessionConstants.REPORT_VALUE_POS] = "any";
        } else {
            Set<Long> lOfficeUnitIds = getOfficeIdsFromOfficeUnitIds(request);
            Criteria[2][SessionConstants.REPORT_OPERATOR_POS] = "in";
            Criteria[2][SessionConstants.REPORT_VALUE_POS] = lOfficeUnitIds.stream().map(String::valueOf)
                    .collect(Collectors.joining(","));

        }
        String reportName = LM.getText(LC.OFFICE_HEAD_REPORT_OTHER_OFFICE_HEAD_REPORT, loginDTO);

        reportRequestHandler = new ReportRequestHandler(null,
                Criteria, Display, GroupBy, OrderBY, sql,
                reportService);

        reportRequestHandler.handleReportGet(request, response, HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO,
                "office_head_report", MenuConstants.OFFICE_HEAD_REPORT_DETAILS,
                HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language,
                reportName, "office_head_report");
    }
    
    private Set<Long> getOfficeIdsFromOfficeUnitIds(HttpServletRequest request) {
        Long[] officeUnitIds = new Gson().fromJson(request.getParameter("officeUnitIds"), Long[].class);

        boolean onlySelectedOffice = Boolean.parseBoolean(request.getParameter("onlySelectedOffice"));

        if (onlySelectedOffice) return new HashSet<>(Arrays.asList(officeUnitIds));

        return Arrays.stream(officeUnitIds)
                .flatMap(officeUnitId -> Office_unitsRepository.getInstance().getAllChildOfficeDTOWithParent(officeUnitId)
                        .stream())
                .map(e -> e.iD)
                .collect(Collectors.toSet());
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }
}