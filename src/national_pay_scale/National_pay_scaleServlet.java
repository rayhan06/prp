package national_pay_scale;

import common.BaseServlet;
import files.FilesDAO;
import language.LC;
import language.LM;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import pb.ErrorMessage;
import permission.MenuConstants;
import sessionmanager.SessionConstants;
import user.UserDTO;
import util.CommonDTO;
import util.HttpRequestUtils;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * Servlet implementation class National_pay_scaleServlet
 */
@WebServlet("/National_pay_scaleServlet")
@MultipartConfig
public class National_pay_scaleServlet extends BaseServlet {
    private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(National_pay_scaleServlet.class);

    @Override
    public String getTableName() {
        return National_pay_scaleDAO.getInstance().getTableName();
    }

    @Override
    public String getServletName() {
        return "National_pay_scaleServlet";
    }

    @Override
    public National_pay_scaleDAO getCommonDAOService() {
        return National_pay_scaleDAO.getInstance();
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[]{MenuConstants.NATIONAL_PAY_SCALE_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[]{MenuConstants.NATIONAL_PAY_SCALE_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[]{MenuConstants.NATIONAL_PAY_SCALE_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return National_pay_scaleServlet.class;
    }

    FilesDAO filesDAO = new FilesDAO();


    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {

        String language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
        boolean isLangEng = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
        National_pay_scaleDTO national_pay_scaleDTO;
        SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");

        if (addFlag) {
            national_pay_scaleDTO = new National_pay_scaleDTO();
            national_pay_scaleDTO.insertedBy = userDTO.employee_record_id;
            national_pay_scaleDTO.insertionTime = new Date().getTime();
        } else {
            national_pay_scaleDTO = National_pay_scaleDAO.getInstance().getDTOFromID(Long.parseLong(request.getParameter("iD")));
        }
        national_pay_scaleDTO.modifiedBy = userDTO.employee_record_id;
        national_pay_scaleDTO.lastModificationTime = new Date().getTime();

        String Value;

        national_pay_scaleDTO.nameEn = (Jsoup.clean(request.getParameter("nameEn"), Whitelist.simpleText()));
        if (national_pay_scaleDTO.nameEn == null || national_pay_scaleDTO.nameEn.length() == 0) {
            throw new Exception(isLangEng ? "Name(English) not found" : "নাম(ইংরেজি) পাওয়া যায় নি");
        }
        national_pay_scaleDTO.nameBn = (Jsoup.clean(request.getParameter("nameBn"), Whitelist.simpleText()));
        if (national_pay_scaleDTO.nameBn == null || national_pay_scaleDTO.nameBn.length() == 0) {
            throw new Exception(isLangEng ? "Name(Bangla) not found" : "নাম(বাংলা) পাওয়া যায় নি");
        }
        Value = request.getParameter("startDate");

        if (Value != null && !Value.equalsIgnoreCase("")) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }
        if (Value != null && !Value.equalsIgnoreCase("")) {
            try {
                Date d = f.parse(Value);
                national_pay_scaleDTO.startDate = d.getTime();
            } catch (Exception e) {
                national_pay_scaleDTO.startDate = SessionConstants.MIN_DATE;
                e.printStackTrace();
                throw new Exception(LM.getText(LC.NATIONAL_PAY_SCALE_ADD_STARTDATE, language) + " " + ErrorMessage.getInvalidMessage(language));
            }
        } else {
            national_pay_scaleDTO.startDate = SessionConstants.MIN_DATE;
        }

        if (national_pay_scaleDTO.startDate == SessionConstants.MIN_DATE) {
            throw new Exception(
                    isLangEng ? "Start date not found" : "শুরুর তারিখ পাওয়া যায় নি");
        }

        Value = request.getParameter("isActive");

        if (Value != null && !Value.equalsIgnoreCase("")) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }
        national_pay_scaleDTO.isActive = Value != null && !Value.equalsIgnoreCase("");

        if (!national_pay_scaleDTO.isActive) {
            Value = request.getParameter("endDate");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("endDate = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                try {
                    Date d = f.parse(Value);
                    national_pay_scaleDTO.endDate = d.getTime();
                } catch (Exception e) {
                    national_pay_scaleDTO.endDate = SessionConstants.MIN_DATE;
                    e.printStackTrace();
                    throw new Exception(LM.getText(LC.NATIONAL_PAY_SCALE_ADD_ENDDATE, language) + " " + ErrorMessage.getInvalidMessage(language));
                }
            } else {
                national_pay_scaleDTO.endDate = SessionConstants.MIN_DATE;
            }

            if (national_pay_scaleDTO.endDate == SessionConstants.MIN_DATE) {
                throw new Exception(
                        isLangEng ? "End date not found" : "শেষ তারিখ পাওয়া যায় নি");
            }
        }
        Value = request.getParameter("fileDropzone");

        if (Value != null && !Value.equalsIgnoreCase("")) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }
        if (Value != null && !Value.equalsIgnoreCase("")) {

            System.out.println("fileDropzone = " + Value);

            national_pay_scaleDTO.fileDropzone = Long.parseLong(Value);


            if (!addFlag) {
                String fileDropzoneFilesToDelete = request.getParameter("fileDropzoneFilesToDelete");
                String[] deleteArray = fileDropzoneFilesToDelete.split(",");
                for (int i = 0; i < deleteArray.length; i++) {
                    System.out.println("going to delete " + deleteArray[i]);
                    if (i > 0) {
                        filesDAO.delete(Long.parseLong(deleteArray[i]));
                    }
                }
            }
        }

        System.out.println("Done adding  addNational_pay_scale dto = " + national_pay_scaleDTO);

        if (addFlag) {
            National_pay_scaleDAO.getInstance().add(national_pay_scaleDTO);
        } else {
            National_pay_scaleDAO.getInstance().update(national_pay_scaleDTO);
        }


        return national_pay_scaleDTO;

    }
}

