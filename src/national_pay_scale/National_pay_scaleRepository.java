package national_pay_scale;

import com.google.gson.Gson;
import org.apache.log4j.Logger;
import pb.OptionDTO;
import pb.Utils;
import repository.Repository;
import repository.RepositoryManager;
import util.LockManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;


public class National_pay_scaleRepository implements Repository {
    National_pay_scaleDAO national_pay_scaleDAO;

    static Logger logger = Logger.getLogger(National_pay_scaleRepository.class);
    Map<Long, National_pay_scaleDTO> mapOfNational_pay_scaleDTOToiD;
    Gson gson;


    private National_pay_scaleRepository() {
        national_pay_scaleDAO = National_pay_scaleDAO.getInstance();
        mapOfNational_pay_scaleDTOToiD = new ConcurrentHashMap<>();
        gson = new Gson();
        RepositoryManager.getInstance().addRepository(this);
    }

    private static class LazyLoader {
        final static National_pay_scaleRepository INSTANCE = new National_pay_scaleRepository();
    }

    public static National_pay_scaleRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

    public void reload(boolean reloadAll) {
        try {
            List<National_pay_scaleDTO> national_pay_scaleDTOs = national_pay_scaleDAO.getAllDTOs(reloadAll);
            for (National_pay_scaleDTO national_pay_scaleDTO : national_pay_scaleDTOs) {
                National_pay_scaleDTO oldNational_pay_scaleDTO = getNational_pay_scaleDTOByiD(national_pay_scaleDTO.iD);
                if (oldNational_pay_scaleDTO != null) {
                    mapOfNational_pay_scaleDTOToiD.remove(oldNational_pay_scaleDTO.iD);


                }
                if (national_pay_scaleDTO.isDeleted == 0) {

                    mapOfNational_pay_scaleDTOToiD.put(national_pay_scaleDTO.iD, national_pay_scaleDTO);

                }
            }

        } catch (Exception e) {
            logger.debug("FATAL", e);
        }
    }

    public National_pay_scaleDTO clone(National_pay_scaleDTO dto) {
        String raw = gson.toJson(dto);
        return gson.fromJson(raw, National_pay_scaleDTO.class);
    }


    public List<National_pay_scaleDTO> getNational_pay_scaleList() {
        return new ArrayList<>(this.mapOfNational_pay_scaleDTOToiD.values());
    }

    public List<National_pay_scaleDTO> copyNational_pay_scaleList() {
        List<National_pay_scaleDTO> national_pay_scales = getNational_pay_scaleList();
        return national_pay_scales
                .stream()
                .map(this::clone)
                .collect(Collectors.toList());
    }

    void checkCache(long iD) {
        if (mapOfNational_pay_scaleDTOToiD.get(iD) == null) {
            synchronized (LockManager.getLock(iD + "NPSR")) {
                if (mapOfNational_pay_scaleDTOToiD.get(iD) == null) {
                    National_pay_scaleDTO dto = national_pay_scaleDAO.getDTOByID(iD);
                    if (dto != null) {
                        mapOfNational_pay_scaleDTOToiD.put(dto.iD, dto);
                    }
                }
            }
        }

    }

    public National_pay_scaleDTO getNational_pay_scaleDTOByiD(long iD) {
        checkCache(iD);
        return mapOfNational_pay_scaleDTOToiD.get(iD);
    }

    public National_pay_scaleDTO copyNational_pay_scaleDTOByiD(long iD) {
        checkCache(iD);
        return clone(mapOfNational_pay_scaleDTOToiD.get(iD));
    }


    @Override
    public String getTableName() {
        return national_pay_scaleDAO.getTableName();
    }


    public String buildOptions(String language, Long selectedId) {
        List<OptionDTO> optionDTOList = null;
        List<National_pay_scaleDTO> national_pay_scaleDTOList = National_pay_scaleRepository.getInstance().
                getNational_pay_scaleList().stream().filter(dto -> dto.isActive).collect(Collectors.toList());
        if (national_pay_scaleDTOList.size() > 0) {
            optionDTOList = national_pay_scaleDTOList.stream()
                    .map(dto -> new OptionDTO(dto.nameEn, dto.nameBn, String.valueOf(dto.iD)))
                    .collect(Collectors.toList());
        }
        return Utils.buildOptions(optionDTOList, language, selectedId == null ? null : String.valueOf(selectedId));
    }

    public National_pay_scaleDTO getActivePayScale() {
        return National_pay_scaleRepository.getInstance().
                getNational_pay_scaleList().stream().filter(dto -> dto.isActive).findAny().orElse(null);
    }

}


