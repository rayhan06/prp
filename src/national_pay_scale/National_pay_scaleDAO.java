package national_pay_scale;

import common.CommonDAOService;
import org.apache.log4j.Logger;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class National_pay_scaleDAO implements CommonDAOService<National_pay_scaleDTO> {

    Logger logger = Logger.getLogger(getClass());

    private static String addQuery = "";
    private static String updateQuery = "";
    private final String[] columnNames;

    private National_pay_scaleDAO() {
        columnNames = new String[]
                {
                        "ID",
                        "name_en",
                        "name_bn",
                        "start_date",
                        "is_active",
                        "end_date",
                        "file_dropzone",
                        "search_column",
                        "inserted_by",
                        "insertion_time",
                        "isDeleted",
                        "modified_by",
                        "lastModificationTime"
                };
        updateQuery = getUpdateQuery(columnNames);
        addQuery = getAddQuery(columnNames);

        searchMap.put("name_en", " and (name_en like ?)");
        searchMap.put("name_bn", " and (name_bn like ?)");
        searchMap.put("start_date_start", " and (start_date >= ?)");
        searchMap.put("start_date_end", " and (start_date <= ?)");
        searchMap.put("end_date_start", " and (end_date >= ?)");
        searchMap.put("end_date_end", " and (end_date <= ?)");
        searchMap.put("AnyField", " and (search_column like ?)");
    }

    private final Map<String, String> searchMap = new HashMap<>();

    private static class LazyLoader {
        static final National_pay_scaleDAO INSTANCE = new National_pay_scaleDAO();
    }

    public static National_pay_scaleDAO getInstance() {
        return LazyLoader.INSTANCE;
    }

    public void setSearchColumn(National_pay_scaleDTO national_pay_scaleDTO) {
        national_pay_scaleDTO.searchColumn = "";
        national_pay_scaleDTO.searchColumn += national_pay_scaleDTO.nameEn + " ";
        national_pay_scaleDTO.searchColumn += national_pay_scaleDTO.nameBn + " ";
    }

    @Override
    public void set(PreparedStatement ps, National_pay_scaleDTO national_pay_scaleDTO, boolean isInsert) throws SQLException {
        int index = 0;
        long lastModificationTime = System.currentTimeMillis();
        setSearchColumn(national_pay_scaleDTO);
        if (isInsert) {
            ps.setObject(++index, national_pay_scaleDTO.iD);
        }
        ps.setObject(++index, national_pay_scaleDTO.nameEn);
        ps.setObject(++index, national_pay_scaleDTO.nameBn);
        ps.setObject(++index, national_pay_scaleDTO.startDate);
        ps.setObject(++index, national_pay_scaleDTO.isActive);
        ps.setObject(++index, national_pay_scaleDTO.endDate);
        ps.setObject(++index, national_pay_scaleDTO.fileDropzone);
        ps.setObject(++index, national_pay_scaleDTO.searchColumn);
        ps.setObject(++index, national_pay_scaleDTO.insertedBy);
        ps.setObject(++index, national_pay_scaleDTO.insertionTime);
        if (isInsert) {
            ps.setObject(++index, national_pay_scaleDTO.isDeleted);
        }
        ps.setObject(++index, national_pay_scaleDTO.modifiedBy);
        ps.setObject(++index, lastModificationTime);
        if (!isInsert) {
            ps.setObject(++index, national_pay_scaleDTO.iD);
        }
    }

    @Override
    public National_pay_scaleDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            National_pay_scaleDTO national_pay_scaleDTO = new National_pay_scaleDTO();
            int i = 0;
            national_pay_scaleDTO.iD = rs.getLong(columnNames[i++]);
            national_pay_scaleDTO.nameEn = rs.getString(columnNames[i++]);
            national_pay_scaleDTO.nameBn = rs.getString(columnNames[i++]);
            national_pay_scaleDTO.startDate = rs.getLong(columnNames[i++]);
            national_pay_scaleDTO.isActive = rs.getBoolean(columnNames[i++]);
            national_pay_scaleDTO.endDate = rs.getLong(columnNames[i++]);
            national_pay_scaleDTO.fileDropzone = rs.getLong(columnNames[i++]);
            national_pay_scaleDTO.searchColumn = rs.getString(columnNames[i++]);
            national_pay_scaleDTO.insertedBy = rs.getLong(columnNames[i++]);
            national_pay_scaleDTO.insertionTime = rs.getLong(columnNames[i++]);
            national_pay_scaleDTO.isDeleted = rs.getInt(columnNames[i++]);
            national_pay_scaleDTO.modifiedBy = rs.getLong(columnNames[i++]);
            national_pay_scaleDTO.lastModificationTime = rs.getLong(columnNames[i++]);
            return national_pay_scaleDTO;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    public National_pay_scaleDTO getDTOByID(long id) {
        return getDTOFromID(id);
    }

    @Override
    public String getTableName() {
        return "national_pay_scale";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((National_pay_scaleDTO) commonDTO, addQuery, true);
    }

    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((National_pay_scaleDTO) commonDTO, updateQuery, false);
    }

    @Override
    public String getLastModifierUser() {
        return "modified_by";
    }

}
	