package national_pay_scale;

import util.CommonDTO;


public class National_pay_scaleDTO extends CommonDTO {

    public String nameEn = "";
    public String nameBn = "";
    public long startDate = System.currentTimeMillis();
    public boolean isActive = false;
    public long endDate = System.currentTimeMillis();
    public long fileDropzone = -1;
    public long insertedBy = -1;
    public long insertionTime = -1;
    public long modifiedBy = -1;


    @Override
    public String toString() {
        return "$National_pay_scaleDTO[" +
                " iD = " + iD +
                " nameEn = " + nameEn +
                " nameBn = " + nameBn +
                " startDate = " + startDate +
                " isActive = " + isActive +
                " endDate = " + endDate +
                " fileDropzone = " + fileDropzone +
                " searchColumn = " + searchColumn +
                " insertedBy = " + insertedBy +
                " insertionTime = " + insertionTime +
                " isDeleted = " + isDeleted +
                " modifiedBy = " + modifiedBy +
                " lastModificationTime = " + lastModificationTime +
                "]";
    }

}