package sectionwise_ticket_report;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import language.LC;
import language.LM;

import login.LoginDTO;
import office_units.Office_unitsDTO;
import office_units.Office_unitsRepository;
import permission.MenuConstants;

import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;

import util.ReportRequestHandler;
import pbReport.*;






@WebServlet("/Sectionwise_ticket_report_Servlet")
public class Sectionwise_ticket_report_Servlet  extends HttpServlet {
	/**
	 * 
	 */
	
	private static final long serialVersionUID = 1L;

	String[][] Criteria =
	{
		{"criteria","","insertion_date",">=","","long","","",Long.MIN_VALUE + "","startDate", LC.HM_START_DATE + ""},		
		{"criteria","","insertion_date","<=","AND","long","","",Long.MAX_VALUE + "","endDate", LC.HM_END_DATE + ""},		
		{"criteria","","issue_raiser_dept","=","AND","String","","","any","none", LC.SECTIONWISE_TICKET_REPORT_WHERE_ISSUERAISERDEPT + ""}	
	};
	
	String[][] Display =
	{								
		{"display","","issue_raiser_dept","office_unit",""},
		{"display","","ticket_issues_type","type",""},
		{"display","","COUNT(id)","text",""},
	};
	
	String GroupBy = "ticket_issues_type, issue_raiser_dept";
	String OrderBY = "";
	
	ReportRequestHandler reportRequestHandler;
	
	public Sectionwise_ticket_report_Servlet(){

	}
	
	private final ReportService reportService = new ReportService();
	
	private String sql;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String language = "english";
		if(userDTO!= null && userDTO.languageID == SessionConstants.BANGLA)
		{
			language = "bangla";
		}
				
		String actionType = request.getParameter("actionType");

		System.out.println("In ssservlet doget, actiontype = " + actionType);
		
		sql = "support_ticket";

		Display[0][4] = LM.getText(LC.HM_OFFICE, loginDTO);
		Display[1][4] = LM.getText(LC.SECTIONWISE_TICKET_REPORT_SELECT_TICKETISSUESTYPE, loginDTO);
		Display[2][4] = LM.getText(LC.HM_COUNT, loginDTO);
		
		String officeUnitId = request.getParameter("officeUnitId");
		if(officeUnitId == null || officeUnitId.equalsIgnoreCase(""))
		{
			Criteria[2][SessionConstants.REPORT_OPERATOR_POS] = "=";
			Criteria[2][SessionConstants.REPORT_VALUE_POS] = "any";
		}
		else
		{
			long lOfficeUnitId = Long.parseLong(officeUnitId);
			List<Office_unitsDTO> offices = Office_unitsRepository.getInstance().getAllChildOfficeDTOWithParent(lOfficeUnitId);
			System.out.println("offices count = " + offices.size());
			String valStr = "" + lOfficeUnitId;
			for(Office_unitsDTO office_unitsDTO: offices)
			{
				valStr += ", " + office_unitsDTO.iD;
			}
			
			
			Criteria[2][SessionConstants.REPORT_OPERATOR_POS] = "in";
			Criteria[2][SessionConstants.REPORT_VALUE_POS] = valStr;
			
		}

		
		String reportName = LM.getText(LC.SECTIONWISE_TICKET_REPORT_OTHER_SECTIONWISE_TICKET_REPORT, loginDTO);
		
		reportRequestHandler = new ReportRequestHandler(null,
				Criteria, Display, GroupBy, OrderBY, sql,
				reportService);
		
		reportRequestHandler.clientSideDataFormatting = new ArrayList<Pair<Integer, Integer>>();
		reportRequestHandler.clientSideDataFormatting.add(new ImmutablePair<Integer, Integer>(2, ReportRequestHandler.RIGHT_ALIGN_INT));

		
		reportRequestHandler.handleReportGet(request, response, userDTO, "sectionwise_ticket_report",
				MenuConstants.SECTIONWISE_TICKET_REPORT_DETAILS, language, reportName, "sectionwise_ticket_report");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		doGet(request, response);
	}
}
