package edms_documents;
import java.util.*; 
import util.*;


public class DocumentChainageMAPS extends CommonMaps
{	
	public DocumentChainageMAPS(String tableName)
	{
		
		java_allfield_type_map.put("edms_documents_id".toLowerCase(), "Long");
		java_allfield_type_map.put("description".toLowerCase(), "String");
		java_allfield_type_map.put("from_chainage_km".toLowerCase(), "Integer");
		java_allfield_type_map.put("from_chainage_meter".toLowerCase(), "Integer");
		java_allfield_type_map.put("from_chainage_mm".toLowerCase(), "Integer");
		java_allfield_type_map.put("to_chainage_km".toLowerCase(), "Integer");
		java_allfield_type_map.put("to_chainage_meter".toLowerCase(), "Integer");
		java_allfield_type_map.put("to_chainage_mm".toLowerCase(), "Integer");

		java_anyfield_search_map.put("language_text.languageTextEnglish", "String");
		java_anyfield_search_map.put("language_text.languageTextBangla", "String");

		java_anyfield_search_map.put(tableName + ".edms_documents_id".toLowerCase(), "Long");
		java_anyfield_search_map.put(tableName + ".description".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".from_chainage_km".toLowerCase(), "Integer");
		java_anyfield_search_map.put(tableName + ".from_chainage_meter".toLowerCase(), "Integer");
		java_anyfield_search_map.put(tableName + ".from_chainage_mm".toLowerCase(), "Integer");
		java_anyfield_search_map.put(tableName + ".to_chainage_km".toLowerCase(), "Integer");
		java_anyfield_search_map.put(tableName + ".to_chainage_meter".toLowerCase(), "Integer");
		java_anyfield_search_map.put(tableName + ".to_chainage_mm".toLowerCase(), "Integer");

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("edmsDocumentsId".toLowerCase(), "edmsDocumentsId".toLowerCase());
		java_DTO_map.put("description".toLowerCase(), "description".toLowerCase());
		java_DTO_map.put("fromChainageKm".toLowerCase(), "fromChainageKm".toLowerCase());
		java_DTO_map.put("fromChainageMeter".toLowerCase(), "fromChainageMeter".toLowerCase());
		java_DTO_map.put("fromChainageMm".toLowerCase(), "fromChainageMm".toLowerCase());
		java_DTO_map.put("toChainageKm".toLowerCase(), "toChainageKm".toLowerCase());
		java_DTO_map.put("toChainageMeter".toLowerCase(), "toChainageMeter".toLowerCase());
		java_DTO_map.put("toChainageMm".toLowerCase(), "toChainageMm".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("edms_documents_id".toLowerCase(), "edmsDocumentsId".toLowerCase());
		java_SQL_map.put("description".toLowerCase(), "description".toLowerCase());
		java_SQL_map.put("from_chainage_km".toLowerCase(), "fromChainageKm".toLowerCase());
		java_SQL_map.put("from_chainage_meter".toLowerCase(), "fromChainageMeter".toLowerCase());
		java_SQL_map.put("from_chainage_mm".toLowerCase(), "fromChainageMm".toLowerCase());
		java_SQL_map.put("to_chainage_km".toLowerCase(), "toChainageKm".toLowerCase());
		java_SQL_map.put("to_chainage_meter".toLowerCase(), "toChainageMeter".toLowerCase());
		java_SQL_map.put("to_chainage_mm".toLowerCase(), "toChainageMm".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Edms Documents Id".toLowerCase(), "edmsDocumentsId".toLowerCase());
		java_Text_map.put("Description".toLowerCase(), "description".toLowerCase());
		java_Text_map.put("From Chainage Km".toLowerCase(), "fromChainageKm".toLowerCase());
		java_Text_map.put("From Chainage Meter".toLowerCase(), "fromChainageMeter".toLowerCase());
		java_Text_map.put("From Chainage Mm".toLowerCase(), "fromChainageMm".toLowerCase());
		java_Text_map.put("To Chainage Km".toLowerCase(), "toChainageKm".toLowerCase());
		java_Text_map.put("To Chainage Meter".toLowerCase(), "toChainageMeter".toLowerCase());
		java_Text_map.put("To Chainage Mm".toLowerCase(), "toChainageMm".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}