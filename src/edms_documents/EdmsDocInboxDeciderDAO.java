package edms_documents;

import dbm.DBMR;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class EdmsDocInboxDeciderDAO {

    Logger logger = Logger.getLogger(getClass());

    public Long getOrganogramIdForInbox( Edms_documentsDTO edms_documentsDTO ){

        return getOrganogramIdForInbox( edms_documentsDTO.pbrlpContractorType, edms_documentsDTO.documentProcessCat, edms_documentsDTO.officesType, edms_documentsDTO.documentTypeCat );
    }

    public Long getOrganogramIdForInbox( Long pbrlpContractorId, Integer docProcessId, Long officeId, Integer docType ){

        Connection connection = null;
        ResultSet rs = null;
        Statement stmt = null;
        Long organogramIdForInbox = null;

        try{

            String sql = String.format(
                    "SELECT * from edms_doc_inbox_decider where pbrlp_contractor_id = %d  and doc_process_id = %d and office_id = %d and doc_type = %d and isDeleted = %d ",
                    pbrlpContractorId, docProcessId, officeId, docType, 0 );

            logger.debug("sql " + sql);

            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();
            rs = stmt.executeQuery(sql);

            while( rs.next() ){

                organogramIdForInbox = rs.getLong("organogram_id" );
                break;
            }

        }catch(Exception ex){

            logger.debug("fatal",ex);
        }finally{
            try{
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e){}

            try{
                if (connection != null)
                {
                    DBMR.getInstance().freeConnection(connection);
                }
            }catch(Exception ex2){}
        }
        return organogramIdForInbox;
    }
}
