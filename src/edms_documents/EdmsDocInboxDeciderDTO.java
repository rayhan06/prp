package edms_documents;

import util.CommonDTO;

public class EdmsDocInboxDeciderDTO extends CommonDTO {

    private Long fileIndexSubType;
    private Long officeId;
    private Long docProcessId;
    private Long officeUnitOrganogramId;

    public Long getFileIndexSubType() {
        return fileIndexSubType;
    }

    public void setFileIndexSubType(Long fileIndexSubType) {
        this.fileIndexSubType = fileIndexSubType;
    }

    public Long getOfficeId() {
        return officeId;
    }

    public void setOfficeId(Long officeId) {
        this.officeId = officeId;
    }

    public Long getDocProcessId() {
        return docProcessId;
    }

    public void setDocProcessId(Long docProcessId) {
        this.docProcessId = docProcessId;
    }

    public Long getOfficeUnitOrganogramId() {
        return officeUnitOrganogramId;
    }

    public void setOfficeUnitOrganogramId(Long officeUnitOrganogramId) {
        this.officeUnitOrganogramId = officeUnitOrganogramId;
    }
}
