package edms_documents;
import java.util.*; 
import util.*; 


public class Edms_documentsDTO extends CommonDTO
{

	public int documentProcessCat = 0;
	public String documentNo = "";
	public long pbrlpContractorType = 0;
	public String otherContractor = "";
	public int fileIndexTypeType = 0;
	public int fileIndexSubtypeType = 0;
	public long officesType = 0;
	public int documentTypeCat = 0;
	public long documentDate = 0;
	public long documentReceiveDate = 0;
    public String documentSubject = "";

    public String referenceNo = "";
	public String refPart1 = "";
	public String refPart2 = "";
	public String refPart3 = "";
	public String refPart4 = "";
	public String refPart5 = "";
	public String refPart6 = "";

	public Integer section;
	public Integer rfiStatus;
	public String rfiStatusOther;
	public Integer rfiType = null;
	public Integer rfiDistribution = null;
	public String openReason = "";
	public String crecNo = "";
	public long ncrSubmissionDate = 0;

    public String description = "";
	public long filesDropzone = 0;
	public boolean isConfidential = false;
	public long docConfidentialDate = 0;
	public boolean isLocked = false;
	public long docLockedDate = 0;
	public long parentDocumentsId = 0;
	public long docAdded = 0L;
	public long deadline = 0;
	public long userId = 0;

	
	public List<DocumentChainageDTO> documentChainageDTOList = new ArrayList<>();

    @Override
	public String toString() {
            return "$Edms_documentsDTO[" +
            " iD = " + iD +
            " documentProcessCat = " + documentProcessCat +
            " documentNo = " + documentNo +
            " pbrlpContractorType = " + pbrlpContractorType +
            " fileIndexTypeType = " + fileIndexTypeType +
            " fileIndexSubtypeType = " + fileIndexSubtypeType +
            " officesType = " + officesType +
            " documentTypeCat = " + documentTypeCat +
            " documentDate = " + documentDate +
            " documentReceiveDate = " + documentReceiveDate +
            " documentSubject = " + documentSubject +
            " referenceNo = " + referenceNo +
            " description = " + description +
            " filesDropzone = " + filesDropzone +
            " isConfidential = " + isConfidential +
            " docConfidentialDate = " + docConfidentialDate +
            " isLocked = " + isLocked +
            " docLockedDate = " + docLockedDate +
            " parentDocumentsId = " + parentDocumentsId +
            " userId = " + userId +
            " jobCat = " + jobCat +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}