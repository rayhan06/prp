package edms_documents;

import pb.GenericTree;
import treeView.TreeDTO;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class TreeHelper {

    private String servletName = "";

    public void generateTreeAndForward(HttpServletRequest request, HttpServletResponse response, TreeDTO treeDTO, String servletName ) throws ServletException, IOException {

        this.servletName = servletName;

        String myLayer = request.getParameter("myLayer");
        if(myLayer != null && !myLayer.equalsIgnoreCase(""))
        {
            int layer = Integer.parseInt(request.getParameter("myLayer"));
            if(treeDTO.treeType.equalsIgnoreCase("tree") &&
                    layer < treeDTO.parentChildMap.length &&
                    treeDTO.sameTableParentColumnName != null &&
                    (!treeDTO.sameTableParentColumnName[layer].equals("") || !treeDTO.sameTableParentColumnName[layer].equals("")))
            {
                getRecursiveNodes(request, response, layer, treeDTO );
            }
            else
            {
                getNormalNodes(request, response, layer, treeDTO);
            }
        }
        else
        {
            getTopNodes(request, response, treeDTO);
        }
    }


    public void getRecursiveNodes(HttpServletRequest request, HttpServletResponse response, int layer, TreeDTO treeDTO )
    {
        System.out.println("METHOD: getRecursiveNodes");
        int id = Integer.parseInt(request.getParameter("id"));
        int parentTableID;

        try
        {
            parentTableID = Integer.parseInt(request.getParameter("parentID"));

            int iPrevLayer = -1;
            String prevLayer = request.getParameter("prevLayer");
            if (prevLayer != null && !prevLayer.equalsIgnoreCase(""))
            {
                iPrevLayer = Integer.parseInt(prevLayer);
            }
            // System.out.println("layer = " + layer + " id = " + id + " treeDTO.parentChildMap.length = " + treeDTO.parentChildMap.length + " iPrevLayer = " + iPrevLayer);
            if (layer != 0 &&
                    (treeDTO.sameTableParentColumnName != null &&
                            (!treeDTO.sameTableParentColumnName[layer].equals("") || !treeDTO.sameTableParentColumnName[layer].equals("")))
                    && layer != iPrevLayer)  //If a recursive node suddenly starts at midpoint
            {
                parentTableID = id;
                id = 0;
                System.out.println("AAAAAAAAAAAAAAA after reshuffling layer = " + layer + " id = " + id + " parentTableID = " + parentTableID);
            }

            if (layer < treeDTO.parentChildMap.length)
            {
                List<Integer> UnitIDs = GenericTree.getRecurviveIDs(treeDTO.parentChildMap[layer], treeDTO.sameTableParentColumnName[layer], id, treeDTO.parentIDColumn[layer], parentTableID);

                List<Integer> IDs = new ArrayList<Integer>();
                List<Integer> recursiveNodeTypes = new ArrayList<Integer>();

                setAttributes(id, layer, IDs, request, treeDTO);

                if (id != 0 && layer + 1 < treeDTO.parentChildMap.length)
                {
                    List<Integer> ChildIDs = GenericTree.getChildIDs(treeDTO.parentChildMap[layer + 1], treeDTO.parentIDColumn[layer + 1], id);
                    if(ChildIDs == null)
                    {
                        ChildIDs = new ArrayList<>();

                    }
                    ChildIDs.add(-1);

                    if (ChildIDs != null)
                    {
                        System.out.println("ooooooooooooooooooo ChildIDs = " + ChildIDs.size());
                        for (int i = 0; i < ChildIDs.size(); i++) {
                            IDs.add(ChildIDs.get(i));
                            recursiveNodeTypes.add(2);
                        }
                    }
                }

                for (int i = 0; i < UnitIDs.size(); i++)
                {
                    IDs.add(UnitIDs.get(i));
                    recursiveNodeTypes.add(1);
                }
                request.setAttribute("isRecursive", 1);
                request.setAttribute("recursiveNodeTypes", recursiveNodeTypes);
                request.setAttribute("childName", treeDTO.parentChildMap[layer + 1]);
                request.setAttribute("childEnglishNameColumn", treeDTO.englishNameColumn[layer + 1]);
                request.setAttribute("childBanglaNameColumn", treeDTO.banglaNameColumn[layer + 1]);

                String nodeJsp = "treeNode.jsp";
                System.out.println("SSSSSSSSSSSS Setting parentTableID = " + parentTableID);
                setParametersAndForward(parentTableID, nodeJsp, request, response, treeDTO);

            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }


    private void getNormalNodes(HttpServletRequest request, HttpServletResponse response, int layer, TreeDTO treeDTO) throws ServletException, IOException
    {

        System.out.println("METHOD: getNormalNodes");
        try
        {
            int id = Integer.parseInt(request.getParameter("id"));
            System.out.println("NNNNNNNNNNNNNN getNormalNodes layer = " + layer + " id = " + id);

            if(treeDTO.parentChildMap == null)
            {
                treeDTO.parentChildMap = new String[]{"office_ministries", "office_layers", "offices", "office_units", "office_unit_organograms"};
            }

            if(treeDTO.parentIDColumn == null)
            {
                treeDTO.parentIDColumn = new String[]{"", "office_ministry_id", "office_layer_id", "office_id", "office_unit_id"};
            }



            if(layer < treeDTO.parentChildMap.length)
            {
                List<Integer> IDs = GenericTree.getChildIDs(treeDTO.parentChildMap[layer], treeDTO.parentIDColumn[layer], id);
                setAttributes(id, layer, IDs, request, treeDTO);
                String nodeJsp = getNodeJSP(treeDTO);
                treeDTO.treeType = "tree";
                System.out.println("Going to geoNode, treeDTO.treeType = " + treeDTO.treeType);
                setParametersAndForward(id, nodeJsp, request, response, treeDTO );

            }
            else
            {
                System.out.println("Maximum Layer Reached");
            }

        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }

    private void getTopNodes(HttpServletRequest request, HttpServletResponse response, TreeDTO treeDTO) throws ServletException, IOException
    {
        System.out.println("METHOD: getTopNodes");
        try
        {
            List<Integer> IDs = GenericTree.getTopIDs(treeDTO.parentChildMap[0]);
            setAttributes(-1, 0, IDs, request, treeDTO);
            String nodeJsp = "tree.jsp";
            setParametersAndForward(0, nodeJsp, request, response, treeDTO );
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }

    private void setAttributes(int parentID, int layer, List<Integer> IDs, HttpServletRequest request, TreeDTO treeDTO )
    {

        request.setAttribute("nodeIDs", IDs);
        request.setAttribute("myLayer", layer);
        String request_layer_id_pairs = request.getParameter("layer_id_pairs");
        if(request_layer_id_pairs == null)
        {
            request_layer_id_pairs = "";
        }
        System.out.println("layer_id_pairs = " + request_layer_id_pairs);
        treeDTO.layer_id_pairs = request_layer_id_pairs;
        request.setAttribute("treeDTO", treeDTO);


    }

    private void setParametersAndForward(int parentID, String nodeJSP, HttpServletRequest request, HttpServletResponse response, TreeDTO treeDTO )
    {

        try
        {
            String parentElementID = request.getParameter("parentElementID");
            String checkBoxChecked = request.getParameter("checkBoxChecked");
            request.getRequestDispatcher("inbox_internal/" + nodeJSP + "?"
                    + "actionType=" + "assignPathToOrganogram"
                    + "&action=" + treeDTO.actionType
                    + "&treeType=" + treeDTO.treeType
                    + "&treeBodyGeneric=" + treeDTO.treeBody
                    + "&treeBody=" + treeDTO.treeBody
                    + "&servletType=" + "InboxInternalServlet"
                    + "&pageTitle=" + treeDTO.pageTitle
                    + "&parentID=" + parentID
                    + "&checkBoxChecked=" + checkBoxChecked
                    + "&parentElementID=" + parentElementID
                    + treeDTO.additionalParams
            ).forward(request, response);
        }
        catch (ServletException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (IOException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private String getNodeJSP(TreeDTO treeDTO)
    {
        if(treeDTO.treeType.equalsIgnoreCase("tree"))
        {
            return "treeNode.jsp";
        }
        else if(treeDTO.treeType.equalsIgnoreCase("select"))
        {
            return "dropDownNode.jsp";

        }
        else if(treeDTO.treeType.equalsIgnoreCase("flat"))
        {
            return "expandedTreeNode.jsp";

        }
        return "";
    }
}
