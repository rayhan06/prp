package edms_documents;

import util.CommonConstant;

public class EdmsDocReferencePrefixDTO {

    public static final String DEFAULT_STATUS =
            "[ " +
                "{ " +
                    "\"id\":\"rfi_status\"," +
                    "\"options\":{ " +
                        "\"Approved\":0, " +
                        "\"Rejected\":1, " +
                        "\"Did Not Attend\":2, " +
                        "\"Other\":3 " +
                    "}" +
                "}" +
            "]";

    public static final String APPROVED_FOR_SUBMISSION_AND_SENT_STATUS =
            "[ " +
                "{ " +
                    "\"id\":\"rfi_status\"," +
                    "\"options\":{ " +
                        "\"Drafted/Approved for Submission\":4, " +
                        "\"Sent\":5 " +
                    "}" +
                "}" +
            "]";

    public static final String OPEN_CLOSE_STATUS =
            "[ " +
                "{ " +
                    "\"id\":\"rfi_status\"," +
                        "\"options\":{ " +
                        "\"Open\":" + CommonConstant.STATUS_OPEN + ", " +
                        "\"Close\":" + CommonConstant.STATUS_CLOSE + " " +
                    "}" +
                "}" +
            "]";


    public Integer docProcess = -1;
    public Integer docSource = -1;
    public Integer docType = -1;

    public Integer docReceiver;

    public String selectOptions;

    public String[] hideDiv = {};
    public String[] showDiv = {};

    public String part1 = "";
    public String part2 = "";
    public String part3 = "";
    public String part4 = "";
    public String part5 = "";
    public String part6 = "";

    public String labelPart1 = ".";
    public String labelPart2 = ".";
    public String labelPart3 = ".";
    public String labelPart4 = ".";
    public String labelPart5 = ".";
    public String labelPart6 = ".";

    public String placeHolderPart1 = "";
    public String placeHolderPart2 = "";
    public String placeHolderPart3 = "";
    public String placeHolderPart4 = "";
    public String placeHolderPart5 = "";
    public String placeHolderPart6 = "";

    public String[] optionsPart1 = new String[]{};
    public String[] optionsPart2 = new String[]{};
    public String[] optionsPart3 = new String[]{};
    public String[] optionsPart4 = new String[]{};
    public String[] optionsPart5 = new String[]{};
    public String[] optionsPart6 = new String[]{};


    public static EdmsDocReferencePrefixDAOBuilder builder(){

        return new EdmsDocReferencePrefixDAOBuilder();
    }

    static class EdmsDocReferencePrefixDAOBuilder{

        private final EdmsDocReferencePrefixDTO edmsDocReferencePrefixDTO = new EdmsDocReferencePrefixDTO();

        public EdmsDocReferencePrefixDAOBuilder docProcess( Integer process ){

            this.edmsDocReferencePrefixDTO.docProcess = process;
            return this;
        }

        public EdmsDocReferencePrefixDAOBuilder docSource( Integer source ){

            this.edmsDocReferencePrefixDTO.docSource = source;
            return this;
        }

        public EdmsDocReferencePrefixDAOBuilder docReceiver( Integer receiver ){

            this.edmsDocReferencePrefixDTO.docReceiver = receiver;
            return this;
        }

        public EdmsDocReferencePrefixDAOBuilder docType( Integer docType ){

            this.edmsDocReferencePrefixDTO.docType = docType;
            return this;
        }

        public EdmsDocReferencePrefixDAOBuilder part1( String part1 ){

            this.edmsDocReferencePrefixDTO.part1 = part1;
            return this;
        }

        public EdmsDocReferencePrefixDAOBuilder part2( String part2 ){

            this.edmsDocReferencePrefixDTO.part2 = part2;
            return this;
        }

        public EdmsDocReferencePrefixDAOBuilder part3( String part3 ){

            this.edmsDocReferencePrefixDTO.part3 = part3;
            return this;
        }

        public EdmsDocReferencePrefixDAOBuilder part4( String part4 ){

            this.edmsDocReferencePrefixDTO.part4 = part4;
            return this;
        }

        public EdmsDocReferencePrefixDAOBuilder part5( String part5 ){

            this.edmsDocReferencePrefixDTO.part5 = part5;
            return this;
        }

        public EdmsDocReferencePrefixDAOBuilder part6( String part6 ){

            this.edmsDocReferencePrefixDTO.part6 = part6;
            return this;
        }

        public EdmsDocReferencePrefixDAOBuilder labelPart1( String part1 ){

            this.edmsDocReferencePrefixDTO.labelPart1 = part1;
            return this;
        }

        public EdmsDocReferencePrefixDAOBuilder labelPart2( String part2 ){

            this.edmsDocReferencePrefixDTO.labelPart2 = part2;
            return this;
        }

        public EdmsDocReferencePrefixDAOBuilder labelPart3( String part3 ){

            this.edmsDocReferencePrefixDTO.labelPart3 = part3;
            return this;
        }

        public EdmsDocReferencePrefixDAOBuilder labelPart4( String part4 ){

            this.edmsDocReferencePrefixDTO.labelPart4 = part4;
            return this;
        }

        public EdmsDocReferencePrefixDAOBuilder labelPart5( String part5 ){

            this.edmsDocReferencePrefixDTO.labelPart5 = part5;
            return this;
        }

        public EdmsDocReferencePrefixDAOBuilder labelPart6( String part6 ){

            this.edmsDocReferencePrefixDTO.labelPart6 = part6;
            return this;
        }

        public EdmsDocReferencePrefixDAOBuilder placeHolderPart1( String part1 ){

            this.edmsDocReferencePrefixDTO.placeHolderPart1 = part1;
            return this;
        }

        public EdmsDocReferencePrefixDAOBuilder placeHolderPart2( String part2 ){

            this.edmsDocReferencePrefixDTO.placeHolderPart2 = part2;
            return this;
        }

        public EdmsDocReferencePrefixDAOBuilder placeHolderPart3( String part3 ){

            this.edmsDocReferencePrefixDTO.placeHolderPart3 = part3;
            return this;
        }

        public EdmsDocReferencePrefixDAOBuilder placeHolderPart4( String part4 ){

            this.edmsDocReferencePrefixDTO.placeHolderPart4 = part4;
            return this;
        }

        public EdmsDocReferencePrefixDAOBuilder placeHolderPart5( String part5 ){

            this.edmsDocReferencePrefixDTO.placeHolderPart5 = part5;
            return this;
        }

        public EdmsDocReferencePrefixDAOBuilder placeHolderPart6( String part6 ){

            this.edmsDocReferencePrefixDTO.placeHolderPart6 = part6;
            return this;
        }

        public EdmsDocReferencePrefixDAOBuilder optionsPart1( String[] part1 ){

            this.edmsDocReferencePrefixDTO.optionsPart1 = part1;
            return this;
        }

        public EdmsDocReferencePrefixDAOBuilder optionsPart2( String[] part2 ){

            this.edmsDocReferencePrefixDTO.optionsPart2 = part2;
            return this;
        }

        public EdmsDocReferencePrefixDAOBuilder optionsPart3( String[] part3 ){

            this.edmsDocReferencePrefixDTO.optionsPart3 = part3;
            return this;
        }

        public EdmsDocReferencePrefixDAOBuilder optionsPart4( String[] part4 ){

            this.edmsDocReferencePrefixDTO.optionsPart4 = part4;
            return this;
        }

        public EdmsDocReferencePrefixDAOBuilder optionsPart5( String[] part5 ){

            this.edmsDocReferencePrefixDTO.optionsPart5 = part5;
            return this;
        }

        public EdmsDocReferencePrefixDAOBuilder optionsPart6( String[] part6 ){

            this.edmsDocReferencePrefixDTO.optionsPart6 = part6;
            return this;
        }

        public EdmsDocReferencePrefixDAOBuilder hideDiv( String[] hideDiv ){

            this.edmsDocReferencePrefixDTO.hideDiv = hideDiv;
            return this;
        }

        public EdmsDocReferencePrefixDAOBuilder showDiv( String[] showDiv ){

            this.edmsDocReferencePrefixDTO.showDiv = showDiv;
            return this;
        }

        public EdmsDocReferencePrefixDAOBuilder selectOptions( String selectOptions ){

            this.edmsDocReferencePrefixDTO.selectOptions = selectOptions;
            return this;
        }

        public EdmsDocReferencePrefixDTO build(){

            return this.edmsDocReferencePrefixDTO;
        }
    }
}
