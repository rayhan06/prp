package edms_documents;

import inbox.InboxDAO;
import inbox.InboxDTO;
import inbox_movements.Inbox_movementsDAO;
import inbox_movements.Inbox_movementsDTO;
import user.UserDTO;
import util.CommonConstant;

public class EdmsDocUtil {

    public void addToInbox( Long originatorid, UserDTO userDTO, String Message, String details, Integer type)
    {
        addToInbox( originatorid, userDTO, Message, details, type, -1L, -1 );
    }

    public static void addToInbox( Long originatorid, UserDTO userDTO,String Message, String details, Integer type, Long organogramID, Integer status )
    {

        Long userId = userDTO.ID;
        Long userOrganogramId = userDTO.organogramID;

        long lastModificationTime = System.currentTimeMillis();

        InboxDAO tempInboxDao = new InboxDAO();
        InboxDTO tempInboxDTO = new InboxDTO();

        tempInboxDTO.organogramId = organogramID;
        tempInboxDTO.userId = userDTO.ID;
        tempInboxDTO.submissionDate = lastModificationTime;
        tempInboxDTO.messageType = type;
        tempInboxDTO.subject = Message;
        tempInboxDTO.trackingNumber = "";
        tempInboxDTO.details = details;
        tempInboxDTO.inboxOriginatorId = originatorid;
        tempInboxDTO.taskDecision = "Add Edms Doc";
        tempInboxDTO.taskComments = "Add Edms Doc";
        tempInboxDTO.createdAt = lastModificationTime;
        tempInboxDTO.createdBy = userOrganogramId.intValue();
        tempInboxDTO.currentStatus = CommonConstant.DISTRIBUTION_STATE_ID;


        long tempInboxID = -1;
        try {

            tempInboxID = tempInboxDao.add(tempInboxDTO);
        }
        catch (Exception e1) {

            e1.printStackTrace();
        }

        // add to inbox movements

        Inbox_movementsDAO tempInboxMovementDao = new Inbox_movementsDAO();
        Inbox_movementsDTO tempInboxMovementDTO = new Inbox_movementsDTO();

        tempInboxMovementDTO.inboxId = tempInboxID;
        tempInboxMovementDTO.fromOrganogramId = userOrganogramId;
        tempInboxMovementDTO.toOrganogramId = organogramID;
        tempInboxMovementDTO.inbox_type = type;
        tempInboxMovementDTO.fromEmployeeUserId = userId;
        tempInboxMovementDTO.createdAt = lastModificationTime;
        tempInboxMovementDTO.createdBy = userId;
        tempInboxMovementDTO.currentStatus = CommonConstant.DISTRIBUTION_STATE_ID;
        tempInboxMovementDTO.isSeen = false;

        try {

            tempInboxMovementDao.add(tempInboxMovementDTO);

        } catch (Exception e) {

            e.printStackTrace();
        }
    }
}
