package edms_documents;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;
import sessionmanager.SessionConstants;


import org.apache.log4j.Logger;

import dbm.*;

import repository.RepositoryManager;

import util.*;
import pb.*;
import user.UserDTO;

public class DocumentChainageDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());
	
	
	public DocumentChainageDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";

		joinSQL += " join category on (";
		joinSQL += " )";
		joinSQL += " join language_text on category.language_id = language_text.id";		
		commonMaps = new DocumentChainageMAPS(tableName);
	}
	
	public DocumentChainageDAO()
	{
		this("document_chainage");		
	}
	
	
	
	public long add(CommonDTO commonDTO) throws Exception
	{
		
		DocumentChainageDTO documentchainageDTO = (DocumentChainageDTO)commonDTO;
		
		Connection connection = null;
		PreparedStatement ps = null;

		long lastModificationTime = System.currentTimeMillis();	

		try{
			connection = DBMW.getInstance().getConnection();
			
			if(connection == null)
			{
				System.out.println("nullconn");
			}

			documentchainageDTO.iD = DBMW.getInstance().getNextSequenceId(tableName);

			String sql = "INSERT INTO " + tableName;
			
			sql += " (";
			sql += "ID";
			sql += ", ";
			sql += "edms_documents_id";
			sql += ", ";
			sql += "description";
			sql += ", ";
			sql += "from_chainage_km";
			sql += ", ";
			sql += "from_chainage_meter";
			sql += ", ";
			sql += "from_chainage_mm";
			sql += ", ";
			sql += "to_chainage_km";
			sql += ", ";
			sql += "to_chainage_meter";
			sql += ", ";
			sql += "to_chainage_mm";
			sql += ", ";
			sql += "isDeleted";
			sql += ", ";
			sql += "lastModificationTime";
		
			sql += ")";
			
			
            sql += " VALUES(";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			
			sql += ")";
				

			ps = connection.prepareStatement(sql);
			
			
			

			int index = 1;

			ps.setObject(index++,documentchainageDTO.iD);
			ps.setObject(index++,documentchainageDTO.edmsDocumentsId);
			ps.setObject(index++,documentchainageDTO.description);
			ps.setObject(index++,documentchainageDTO.fromChainageKm);
			ps.setObject(index++,documentchainageDTO.fromChainageMeter);
			ps.setObject(index++,documentchainageDTO.fromChainageMm);
			ps.setObject(index++,documentchainageDTO.toChainageKm);
			ps.setObject(index++,documentchainageDTO.toChainageMeter);
			ps.setObject(index++,documentchainageDTO.toChainageMm);
			ps.setObject(index++,documentchainageDTO.isDeleted);
			ps.setObject(index++, lastModificationTime);
			
			System.out.println(ps);
			ps.execute();
			
			
			recordUpdateTime(connection, lastModificationTime);

		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if (ps != null) {
					ps.close();
				}
			} catch(Exception e){}
			try{
				if(connection != null)
				{
					DBMW.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		return documentchainageDTO.iD;		
	}
		
	
	public void deleteDocumentChainageByEdmsDocumentsID(long edmsDocumentsID) throws Exception{
		
		
		Connection connection = null;
		Statement stmt = null;
		try{
			
			//String sql = "UPDATE document_chainage SET isDeleted=0 WHERE edms_documents_id="+edmsDocumentsID;
			String sql = "delete from document_chainage WHERE edms_documents_id=" + edmsDocumentsID;
			logger.debug("sql " + sql);
			connection = DBMW.getInstance().getConnection();
			stmt = connection.createStatement();
			stmt.execute(sql);
		}catch(Exception ex){
			logger.debug("fatal",ex);
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DBMW.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
	}		
   
	public List<DocumentChainageDTO> getDocumentChainageDTOListByEdmsDocumentsID(long edmsDocumentsID) throws Exception{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		DocumentChainageDTO documentchainageDTO = null;
		List<DocumentChainageDTO> documentchainageDTOList = new ArrayList<>();
		
		try{
			
			String sql = "SELECT * FROM document_chainage where isDeleted=0 and edms_documents_id="+edmsDocumentsID+" order by document_chainage.lastModificationTime";
			
			logger.debug("sql " + sql);
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();
			rs = stmt.executeQuery(sql);

			while(rs.next()){
				documentchainageDTO = new DocumentChainageDTO();
				documentchainageDTO.iD = rs.getLong("ID");
				documentchainageDTO.edmsDocumentsId = rs.getLong("edms_documents_id");
				documentchainageDTO.description = rs.getString("description");
				documentchainageDTO.fromChainageKm = rs.getInt("from_chainage_km");
				documentchainageDTO.fromChainageMeter = rs.getInt("from_chainage_meter");
				documentchainageDTO.fromChainageMm = rs.getInt("from_chainage_mm");
				documentchainageDTO.toChainageKm = rs.getInt("to_chainage_km");
				documentchainageDTO.toChainageMeter = rs.getInt("to_chainage_meter");
				documentchainageDTO.toChainageMm = rs.getInt("to_chainage_mm");
				documentchainageDTO.isDeleted = rs.getInt("isDeleted");
				documentchainageDTO.lastModificationTime = rs.getLong("lastModificationTime");
				documentchainageDTOList.add(documentchainageDTO);

			}			
			
		}catch(Exception ex){
			logger.debug("fatal",ex);
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return documentchainageDTOList;
	}

	//need another getter for repository
	public CommonDTO getDTOByID (long ID) throws Exception
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		DocumentChainageDTO documentchainageDTO = null;
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
			
            sql += " WHERE ID=" + ID;
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			if(rs.next()){
				documentchainageDTO = new DocumentChainageDTO();

				documentchainageDTO.iD = rs.getLong("ID");
				documentchainageDTO.edmsDocumentsId = rs.getLong("edms_documents_id");
				documentchainageDTO.description = rs.getString("description");
				documentchainageDTO.fromChainageKm = rs.getInt("from_chainage_km");
				documentchainageDTO.fromChainageMeter = rs.getInt("from_chainage_meter");
				documentchainageDTO.fromChainageMm = rs.getInt("from_chainage_mm");
				documentchainageDTO.toChainageKm = rs.getInt("to_chainage_km");
				documentchainageDTO.toChainageMeter = rs.getInt("to_chainage_meter");
				documentchainageDTO.toChainageMm = rs.getInt("to_chainage_mm");
				documentchainageDTO.isDeleted = rs.getInt("isDeleted");
				documentchainageDTO.lastModificationTime = rs.getLong("lastModificationTime");

			}			
			
			
			
			DocumentChainageDAO documentChainageDAO = new DocumentChainageDAO("document_chainage");			
			List<DocumentChainageDTO> documentChainageDTOList = documentChainageDAO.getDocumentChainageDTOListByEdmsDocumentsID(documentchainageDTO.iD);
			documentchainageDTO.documentChainageDTOList = documentChainageDTOList;
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return documentchainageDTO;
	}
	
	public long update(CommonDTO commonDTO) throws Exception
	{		
		DocumentChainageDTO documentchainageDTO = (DocumentChainageDTO)commonDTO;
		
		Connection connection = null;
		PreparedStatement ps = null;

		long lastModificationTime = System.currentTimeMillis();	
		try{
			connection = DBMW.getInstance().getConnection();

			String sql = "UPDATE " + tableName;
			
			sql += " SET ";
			sql += "edms_documents_id=?";
			sql += ", ";
			sql += "description=?";
			sql += ", ";
			sql += "from_chainage_km=?";
			sql += ", ";
			sql += "from_chainage_meter=?";
			sql += ", ";
			sql += "from_chainage_mm=?";
			sql += ", ";
			sql += "to_chainage_km=?";
			sql += ", ";
			sql += "to_chainage_meter=?";
			sql += ", ";
			sql += "to_chainage_mm=?";
			sql += ", lastModificationTime = "	+ lastModificationTime + "";
            sql += " WHERE ID = " + documentchainageDTO.iD;
				

			ps = connection.prepareStatement(sql);
			
			

			int index = 1;
			
			ps.setObject(index++,documentchainageDTO.edmsDocumentsId);
			ps.setObject(index++,documentchainageDTO.description);
			ps.setObject(index++,documentchainageDTO.fromChainageKm);
			ps.setObject(index++,documentchainageDTO.fromChainageMeter);
			ps.setObject(index++,documentchainageDTO.fromChainageMm);
			ps.setObject(index++,documentchainageDTO.toChainageKm);
			ps.setObject(index++,documentchainageDTO.toChainageMeter);
			ps.setObject(index++,documentchainageDTO.toChainageMm);
			System.out.println(ps);
			ps.executeUpdate();
			


			
			recordUpdateTime(connection, lastModificationTime);

						
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if (ps != null) {
					ps.close();
				}
			} catch(Exception e){}
			try{
				if(connection != null)
				{
					DBMW.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		return documentchainageDTO.iD;
	}
	
	
	public List<DocumentChainageDTO> getDTOs(Collection recordIDs){
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		DocumentChainageDTO documentchainageDTO = null;
		List<DocumentChainageDTO> documentchainageDTOList = new ArrayList<>();
		if(recordIDs.isEmpty()){
			return documentchainageDTOList;
		}
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
            
            sql += " WHERE ID IN ( ";

			for(int i = 0;i<recordIDs.size();i++){
				if(i!=0){
					sql+=",";
				}
				sql+=((ArrayList)recordIDs).get(i);
			}
			sql+=")  order by lastModificationTime desc";
			
			printSql(sql);
			
			logger.debug("sql " + sql);
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				documentchainageDTO = new DocumentChainageDTO();
				documentchainageDTO.iD = rs.getLong("ID");
				documentchainageDTO.edmsDocumentsId = rs.getLong("edms_documents_id");
				documentchainageDTO.description = rs.getString("description");
				documentchainageDTO.fromChainageKm = rs.getInt("from_chainage_km");
				documentchainageDTO.fromChainageMeter = rs.getInt("from_chainage_meter");
				documentchainageDTO.fromChainageMm = rs.getInt("from_chainage_mm");
				documentchainageDTO.toChainageKm = rs.getInt("to_chainage_km");
				documentchainageDTO.toChainageMeter = rs.getInt("to_chainage_meter");
				documentchainageDTO.toChainageMm = rs.getInt("to_chainage_mm");
				documentchainageDTO.isDeleted = rs.getInt("isDeleted");
				documentchainageDTO.lastModificationTime = rs.getLong("lastModificationTime");
				System.out.println("got this DTO: " + documentchainageDTO);
				
				documentchainageDTOList.add(documentchainageDTO);

			}			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return documentchainageDTOList;
	
	}
	
	

	
	
	
	//add repository
	public List<DocumentChainageDTO> getAllDocumentChainage (boolean isFirstReload)
    {
		List<DocumentChainageDTO> documentchainageDTOList = new ArrayList<>();

		String sql = "SELECT * FROM document_chainage";
		sql += " WHERE ";
	

		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by documentchainage.lastModificationTime desc";
		printSql(sql);
		
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;

		
		try{
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();
			rs = stmt.executeQuery(sql);
			

			while(rs.next()){
				DocumentChainageDTO documentchainageDTO = new DocumentChainageDTO();
				documentchainageDTO.iD = rs.getLong("ID");
				documentchainageDTO.edmsDocumentsId = rs.getLong("edms_documents_id");
				documentchainageDTO.description = rs.getString("description");
				documentchainageDTO.fromChainageKm = rs.getInt("from_chainage_km");
				documentchainageDTO.fromChainageMeter = rs.getInt("from_chainage_meter");
				documentchainageDTO.fromChainageMm = rs.getInt("from_chainage_mm");
				documentchainageDTO.toChainageKm = rs.getInt("to_chainage_km");
				documentchainageDTO.toChainageMeter = rs.getInt("to_chainage_meter");
				documentchainageDTO.toChainageMm = rs.getInt("to_chainage_mm");
				documentchainageDTO.isDeleted = rs.getInt("isDeleted");
				documentchainageDTO.lastModificationTime = rs.getLong("lastModificationTime");
				
				documentchainageDTOList.add(documentchainageDTO);
			}			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}

		return documentchainageDTOList;
    }

	
	public List<DocumentChainageDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO)
	{
		return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
	}

	
	public List<DocumentChainageDTO> getDTOs(Hashtable  p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
			String filter, boolean tableHasJobCat)
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		List<DocumentChainageDTO> documentchainageDTOList = new ArrayList<>();

		try{
			
			String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat);
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				DocumentChainageDTO documentchainageDTO = new DocumentChainageDTO();
				documentchainageDTO.iD = rs.getLong("ID");
				documentchainageDTO.edmsDocumentsId = rs.getLong("edms_documents_id");
				documentchainageDTO.description = rs.getString("description");
				documentchainageDTO.fromChainageKm = rs.getInt("from_chainage_km");
				documentchainageDTO.fromChainageMeter = rs.getInt("from_chainage_meter");
				documentchainageDTO.fromChainageMm = rs.getInt("from_chainage_mm");
				documentchainageDTO.toChainageKm = rs.getInt("to_chainage_km");
				documentchainageDTO.toChainageMeter = rs.getInt("to_chainage_meter");
				documentchainageDTO.toChainageMm = rs.getInt("to_chainage_mm");
				documentchainageDTO.isDeleted = rs.getInt("isDeleted");
				documentchainageDTO.lastModificationTime = rs.getLong("lastModificationTime");
				
				documentchainageDTOList.add(documentchainageDTO);
			}					
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return documentchainageDTOList;
	
	}
				
}
	