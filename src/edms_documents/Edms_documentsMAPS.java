package edms_documents;
import util.*;

import java.util.ArrayList;
import java.util.List;


public class Edms_documentsMAPS extends CommonMaps
{	
	public Edms_documentsMAPS(String tableName)
	{
		
		java_allfield_type_map.put("document_process_cat".toLowerCase(), "Integer");
		java_allfield_type_map.put("document_no".toLowerCase(), "Long");
		java_allfield_type_map.put("pbrlp_contractor_type".toLowerCase(), "Long");
		java_allfield_type_map.put("file_index_type_type".toLowerCase(), "Integer");
		java_allfield_type_map.put("file_index_subtype_type".toLowerCase(), "Integer");
		java_allfield_type_map.put("offices_type".toLowerCase(), "Long");
		java_allfield_type_map.put("document_type_cat".toLowerCase(), "Integer");
		java_allfield_type_map.put("document_date".toLowerCase(), "date");
		java_allfield_type_map.put("document_receive_date".toLowerCase(), "Long");
		java_allfield_type_map.put("reference_no".toLowerCase(),"String");

		List<Range> rangeList = new ArrayList<>();
		rangeList.add( Range.FROM );
		rangeList.add( Range.TO );

		rangeMap.put( "document_date", rangeList );

		dateFormat.put( "document_date", "dd/MM/yyyy" );

		java_anyfield_search_map.put("language_text.languageTextEnglish", "String");
		java_anyfield_search_map.put("language_text.languageTextBangla", "String");

		java_anyfield_search_map.put(tableName + ".document_no".toLowerCase(), "Long");
		java_anyfield_search_map.put("pbrlp_contractor.name_en".toLowerCase(), "String");
		java_anyfield_search_map.put("pbrlp_contractor.name_bn".toLowerCase(), "String");
		java_anyfield_search_map.put("file_index_type.name_en".toLowerCase(), "String");
		java_anyfield_search_map.put("file_index_type.name_bn".toLowerCase(), "String");
		java_anyfield_search_map.put("file_index_subtype.name_en".toLowerCase(), "String");
		java_anyfield_search_map.put("file_index_subtype.name_bn".toLowerCase(), "String");
		java_anyfield_search_map.put("office_units.unit_name_eng".toLowerCase(), "String");
		java_anyfield_search_map.put("office_units.unit_name_bng".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".document_date".toLowerCase(), "Long");
		java_anyfield_search_map.put(tableName + ".document_receive_date".toLowerCase(), "Long");
		java_anyfield_search_map.put(tableName + ".document_subject".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".reference_no".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".description".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".is_confidential".toLowerCase(), "Boolean");
		java_anyfield_search_map.put(tableName + ".doc_confidential_date".toLowerCase(), "Long");
		java_anyfield_search_map.put(tableName + ".is_locked".toLowerCase(), "Boolean");
		java_anyfield_search_map.put(tableName + ".doc_locked_date".toLowerCase(), "Long");
		java_anyfield_search_map.put(tableName + ".parent_documents_id".toLowerCase(), "Long");
		java_anyfield_search_map.put(tableName + ".user_id".toLowerCase(), "Long");
		java_anyfield_search_map.put(tableName + ".reference_no".toLowerCase(), "String");

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("documentProcessCat".toLowerCase(), "documentProcessCat".toLowerCase());
		java_DTO_map.put("documentNo".toLowerCase(), "documentNo".toLowerCase());
		java_DTO_map.put("pbrlpContractorType".toLowerCase(), "pbrlpContractorType".toLowerCase());
		java_DTO_map.put("fileIndexTypeType".toLowerCase(), "fileIndexTypeType".toLowerCase());
		java_DTO_map.put("fileIndexSubtypeType".toLowerCase(), "fileIndexSubtypeType".toLowerCase());
		java_DTO_map.put("officesType".toLowerCase(), "officesType".toLowerCase());
		java_DTO_map.put("documentTypeCat".toLowerCase(), "documentTypeCat".toLowerCase());
		java_DTO_map.put("documentDate".toLowerCase(), "documentDate".toLowerCase());
		java_DTO_map.put("documentReceiveDate".toLowerCase(), "documentReceiveDate".toLowerCase());
		java_DTO_map.put("documentSubject".toLowerCase(), "documentSubject".toLowerCase());
		java_DTO_map.put("referenceNo".toLowerCase(), "referenceNo".toLowerCase());
		java_DTO_map.put("description".toLowerCase(), "description".toLowerCase());
		java_DTO_map.put("filesDropzone".toLowerCase(), "filesDropzone".toLowerCase());
		java_DTO_map.put("isConfidential".toLowerCase(), "isConfidential".toLowerCase());
		java_DTO_map.put("docConfidentialDate".toLowerCase(), "docConfidentialDate".toLowerCase());
		java_DTO_map.put("isLocked".toLowerCase(), "isLocked".toLowerCase());
		java_DTO_map.put("docLockedDate".toLowerCase(), "docLockedDate".toLowerCase());
		java_DTO_map.put("parentDocumentsId".toLowerCase(), "parentDocumentsId".toLowerCase());
		java_DTO_map.put("userId".toLowerCase(), "userId".toLowerCase());
		java_DTO_map.put("jobCat".toLowerCase(), "jobCat".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("document_process_cat".toLowerCase(), "documentProcessCat".toLowerCase());
		java_SQL_map.put("document_no".toLowerCase(), "documentNo".toLowerCase());
		java_SQL_map.put("pbrlp_contractor_type".toLowerCase(), "pbrlpContractorType".toLowerCase());
		java_SQL_map.put("file_index_type_type".toLowerCase(), "fileIndexTypeType".toLowerCase());
		java_SQL_map.put("file_index_subtype_type".toLowerCase(), "fileIndexSubtypeType".toLowerCase());
		java_SQL_map.put("offices_type".toLowerCase(), "officesType".toLowerCase());
		java_SQL_map.put("document_type_cat".toLowerCase(), "documentTypeCat".toLowerCase());
		java_SQL_map.put("document_date".toLowerCase(), "documentDate".toLowerCase());
		java_SQL_map.put("document_receive_date".toLowerCase(), "documentReceiveDate".toLowerCase());
		java_SQL_map.put("document_subject".toLowerCase(), "documentSubject".toLowerCase());
		java_SQL_map.put("reference_no".toLowerCase(), "referenceNo".toLowerCase());
		java_SQL_map.put("files_dropzone".toLowerCase(), "filesDropzone".toLowerCase());
		java_SQL_map.put("reference_no".toLowerCase(), "referenceNo".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Document Process Cat".toLowerCase(), "documentProcessCat".toLowerCase());
		java_Text_map.put("Document No".toLowerCase(), "documentNo".toLowerCase());
		java_Text_map.put("Pbrlp Contractor".toLowerCase(), "pbrlpContractorType".toLowerCase());
		java_Text_map.put("File Index Type".toLowerCase(), "fileIndexTypeType".toLowerCase());
		java_Text_map.put("File Index Subtype".toLowerCase(), "fileIndexSubtypeType".toLowerCase());
		java_Text_map.put("Offices".toLowerCase(), "officesType".toLowerCase());
		java_Text_map.put("Document Type Cat".toLowerCase(), "documentTypeCat".toLowerCase());
		java_Text_map.put("Document Date".toLowerCase(), "documentDate".toLowerCase());
		java_Text_map.put("Document Receive Date".toLowerCase(), "documentReceiveDate".toLowerCase());
		java_Text_map.put("Document Subject".toLowerCase(), "documentSubject".toLowerCase());
		java_Text_map.put("Reference No".toLowerCase(), "referenceNo".toLowerCase());
		java_Text_map.put("Description".toLowerCase(), "description".toLowerCase());
		java_Text_map.put("Files Dropzone".toLowerCase(), "filesDropzone".toLowerCase());
		java_Text_map.put("Is Confidential".toLowerCase(), "isConfidential".toLowerCase());
		java_Text_map.put("Doc Confidential Date".toLowerCase(), "docConfidentialDate".toLowerCase());
		java_Text_map.put("Is Locked".toLowerCase(), "isLocked".toLowerCase());
		java_Text_map.put("Doc Locked Date".toLowerCase(), "docLockedDate".toLowerCase());
		java_Text_map.put("Parent Documents Id".toLowerCase(), "parentDocumentsId".toLowerCase());
		java_Text_map.put("User Id".toLowerCase(), "userId".toLowerCase());
		java_Text_map.put("Job Cat".toLowerCase(), "jobCat".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}