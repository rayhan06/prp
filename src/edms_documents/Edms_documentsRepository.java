package edms_documents;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class Edms_documentsRepository implements Repository {
	Edms_documentsDAO edms_documentsDAO = null;
	
	public void setDAO(Edms_documentsDAO edms_documentsDAO)
	{
		this.edms_documentsDAO = edms_documentsDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Edms_documentsRepository.class);
	Map<Long, Edms_documentsDTO>mapOfEdms_documentsDTOToiD;
	Map<Integer, Set<Edms_documentsDTO> >mapOfEdms_documentsDTOTodocumentProcessCat;
	Map<String, Set<Edms_documentsDTO> >mapOfEdms_documentsDTOTodocumentNo;
	Map<Long, Set<Edms_documentsDTO> >mapOfEdms_documentsDTOTopbrlpContractorType;
	Map<Integer, Set<Edms_documentsDTO> >mapOfEdms_documentsDTOTofileIndexTypeType;
	Map<Integer, Set<Edms_documentsDTO> >mapOfEdms_documentsDTOTofileIndexSubtypeType;
	Map<Long, Set<Edms_documentsDTO> >mapOfEdms_documentsDTOToofficesType;
	Map<Integer, Set<Edms_documentsDTO> >mapOfEdms_documentsDTOTodocumentTypeCat;
	Map<Long, Set<Edms_documentsDTO> >mapOfEdms_documentsDTOTodocumentDate;
	Map<Long, Set<Edms_documentsDTO> >mapOfEdms_documentsDTOTodocumentReceiveDate;
	Map<String, Set<Edms_documentsDTO> >mapOfEdms_documentsDTOTodocumentSubject;
	Map<String, Set<Edms_documentsDTO> >mapOfEdms_documentsDTOToreferenceNo;
	Map<String, Set<Edms_documentsDTO> >mapOfEdms_documentsDTOTodescription;
	Map<Long, Set<Edms_documentsDTO> >mapOfEdms_documentsDTOTofilesDropzone;
	Map<Boolean, Set<Edms_documentsDTO> >mapOfEdms_documentsDTOToisConfidential;
	Map<Long, Set<Edms_documentsDTO> >mapOfEdms_documentsDTOTodocConfidentialDate;
	Map<Boolean, Set<Edms_documentsDTO> >mapOfEdms_documentsDTOToisLocked;
	Map<Long, Set<Edms_documentsDTO> >mapOfEdms_documentsDTOTodocLockedDate;
	Map<Long, Set<Edms_documentsDTO> >mapOfEdms_documentsDTOToparentDocumentsId;
	Map<Long, Set<Edms_documentsDTO> >mapOfEdms_documentsDTOTouserId;
	Map<Integer, Set<Edms_documentsDTO> >mapOfEdms_documentsDTOTojobCat;
	Map<Long, Set<Edms_documentsDTO> >mapOfEdms_documentsDTOTolastModificationTime;


	static Edms_documentsRepository instance = null;  
	private Edms_documentsRepository(){
		mapOfEdms_documentsDTOToiD = new ConcurrentHashMap<>();
		mapOfEdms_documentsDTOTodocumentProcessCat = new ConcurrentHashMap<>();
		mapOfEdms_documentsDTOTodocumentNo = new ConcurrentHashMap<>();
		mapOfEdms_documentsDTOTopbrlpContractorType = new ConcurrentHashMap<>();
		mapOfEdms_documentsDTOTofileIndexTypeType = new ConcurrentHashMap<>();
		mapOfEdms_documentsDTOTofileIndexSubtypeType = new ConcurrentHashMap<>();
		mapOfEdms_documentsDTOToofficesType = new ConcurrentHashMap<>();
		mapOfEdms_documentsDTOTodocumentTypeCat = new ConcurrentHashMap<>();
		mapOfEdms_documentsDTOTodocumentDate = new ConcurrentHashMap<>();
		mapOfEdms_documentsDTOTodocumentReceiveDate = new ConcurrentHashMap<>();
		mapOfEdms_documentsDTOTodocumentSubject = new ConcurrentHashMap<>();
		mapOfEdms_documentsDTOToreferenceNo = new ConcurrentHashMap<>();
		mapOfEdms_documentsDTOTodescription = new ConcurrentHashMap<>();
		mapOfEdms_documentsDTOTofilesDropzone = new ConcurrentHashMap<>();
		mapOfEdms_documentsDTOToisConfidential = new ConcurrentHashMap<>();
		mapOfEdms_documentsDTOTodocConfidentialDate = new ConcurrentHashMap<>();
		mapOfEdms_documentsDTOToisLocked = new ConcurrentHashMap<>();
		mapOfEdms_documentsDTOTodocLockedDate = new ConcurrentHashMap<>();
		mapOfEdms_documentsDTOToparentDocumentsId = new ConcurrentHashMap<>();
		mapOfEdms_documentsDTOTouserId = new ConcurrentHashMap<>();
		mapOfEdms_documentsDTOTojobCat = new ConcurrentHashMap<>();
		mapOfEdms_documentsDTOTolastModificationTime = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Edms_documentsRepository getInstance(){
		if (instance == null){
			instance = new Edms_documentsRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(edms_documentsDAO == null)
		{
			return;
		}
		try {
			List<Edms_documentsDTO> edms_documentsDTOs = edms_documentsDAO.getAllEdms_documents(reloadAll);
			for(Edms_documentsDTO edms_documentsDTO : edms_documentsDTOs) {
				Edms_documentsDTO oldEdms_documentsDTO = getEdms_documentsDTOByID(edms_documentsDTO.iD);
				if( oldEdms_documentsDTO != null ) {
					mapOfEdms_documentsDTOToiD.remove(oldEdms_documentsDTO.iD);
				
					if(mapOfEdms_documentsDTOTodocumentProcessCat.containsKey(oldEdms_documentsDTO.documentProcessCat)) {
						mapOfEdms_documentsDTOTodocumentProcessCat.get(oldEdms_documentsDTO.documentProcessCat).remove(oldEdms_documentsDTO);
					}
					if(mapOfEdms_documentsDTOTodocumentProcessCat.get(oldEdms_documentsDTO.documentProcessCat).isEmpty()) {
						mapOfEdms_documentsDTOTodocumentProcessCat.remove(oldEdms_documentsDTO.documentProcessCat);
					}
					
					if(mapOfEdms_documentsDTOTodocumentNo.containsKey(oldEdms_documentsDTO.documentNo)) {
						mapOfEdms_documentsDTOTodocumentNo.get(oldEdms_documentsDTO.documentNo).remove(oldEdms_documentsDTO);
					}
					if(mapOfEdms_documentsDTOTodocumentNo.get(oldEdms_documentsDTO.documentNo).isEmpty()) {
						mapOfEdms_documentsDTOTodocumentNo.remove(oldEdms_documentsDTO.documentNo);
					}
					
					if(mapOfEdms_documentsDTOTopbrlpContractorType.containsKey(oldEdms_documentsDTO.pbrlpContractorType)) {
						mapOfEdms_documentsDTOTopbrlpContractorType.get(oldEdms_documentsDTO.pbrlpContractorType).remove(oldEdms_documentsDTO);
					}
					if(mapOfEdms_documentsDTOTopbrlpContractorType.get(oldEdms_documentsDTO.pbrlpContractorType).isEmpty()) {
						mapOfEdms_documentsDTOTopbrlpContractorType.remove(oldEdms_documentsDTO.pbrlpContractorType);
					}
					
					if(mapOfEdms_documentsDTOTofileIndexTypeType.containsKey(oldEdms_documentsDTO.fileIndexTypeType)) {
						mapOfEdms_documentsDTOTofileIndexTypeType.get(oldEdms_documentsDTO.fileIndexTypeType).remove(oldEdms_documentsDTO);
					}
					if(mapOfEdms_documentsDTOTofileIndexTypeType.get(oldEdms_documentsDTO.fileIndexTypeType).isEmpty()) {
						mapOfEdms_documentsDTOTofileIndexTypeType.remove(oldEdms_documentsDTO.fileIndexTypeType);
					}
					
					if(mapOfEdms_documentsDTOTofileIndexSubtypeType.containsKey(oldEdms_documentsDTO.fileIndexSubtypeType)) {
						mapOfEdms_documentsDTOTofileIndexSubtypeType.get(oldEdms_documentsDTO.fileIndexSubtypeType).remove(oldEdms_documentsDTO);
					}
					if(mapOfEdms_documentsDTOTofileIndexSubtypeType.get(oldEdms_documentsDTO.fileIndexSubtypeType).isEmpty()) {
						mapOfEdms_documentsDTOTofileIndexSubtypeType.remove(oldEdms_documentsDTO.fileIndexSubtypeType);
					}
					
					if(mapOfEdms_documentsDTOToofficesType.containsKey(oldEdms_documentsDTO.officesType)) {
						mapOfEdms_documentsDTOToofficesType.get(oldEdms_documentsDTO.officesType).remove(oldEdms_documentsDTO);
					}
					if(mapOfEdms_documentsDTOToofficesType.get(oldEdms_documentsDTO.officesType).isEmpty()) {
						mapOfEdms_documentsDTOToofficesType.remove(oldEdms_documentsDTO.officesType);
					}
					
					if(mapOfEdms_documentsDTOTodocumentTypeCat.containsKey(oldEdms_documentsDTO.documentTypeCat)) {
						mapOfEdms_documentsDTOTodocumentTypeCat.get(oldEdms_documentsDTO.documentTypeCat).remove(oldEdms_documentsDTO);
					}
					if(mapOfEdms_documentsDTOTodocumentTypeCat.get(oldEdms_documentsDTO.documentTypeCat).isEmpty()) {
						mapOfEdms_documentsDTOTodocumentTypeCat.remove(oldEdms_documentsDTO.documentTypeCat);
					}
					
					if(mapOfEdms_documentsDTOTodocumentDate.containsKey(oldEdms_documentsDTO.documentDate)) {
						mapOfEdms_documentsDTOTodocumentDate.get(oldEdms_documentsDTO.documentDate).remove(oldEdms_documentsDTO);
					}
					if(mapOfEdms_documentsDTOTodocumentDate.get(oldEdms_documentsDTO.documentDate).isEmpty()) {
						mapOfEdms_documentsDTOTodocumentDate.remove(oldEdms_documentsDTO.documentDate);
					}
					
					if(mapOfEdms_documentsDTOTodocumentReceiveDate.containsKey(oldEdms_documentsDTO.documentReceiveDate)) {
						mapOfEdms_documentsDTOTodocumentReceiveDate.get(oldEdms_documentsDTO.documentReceiveDate).remove(oldEdms_documentsDTO);
					}
					if(mapOfEdms_documentsDTOTodocumentReceiveDate.get(oldEdms_documentsDTO.documentReceiveDate).isEmpty()) {
						mapOfEdms_documentsDTOTodocumentReceiveDate.remove(oldEdms_documentsDTO.documentReceiveDate);
					}
					
					if(mapOfEdms_documentsDTOTodocumentSubject.containsKey(oldEdms_documentsDTO.documentSubject)) {
						mapOfEdms_documentsDTOTodocumentSubject.get(oldEdms_documentsDTO.documentSubject).remove(oldEdms_documentsDTO);
					}
					if(mapOfEdms_documentsDTOTodocumentSubject.get(oldEdms_documentsDTO.documentSubject).isEmpty()) {
						mapOfEdms_documentsDTOTodocumentSubject.remove(oldEdms_documentsDTO.documentSubject);
					}
					
					if(mapOfEdms_documentsDTOToreferenceNo.containsKey(oldEdms_documentsDTO.referenceNo)) {
						mapOfEdms_documentsDTOToreferenceNo.get(oldEdms_documentsDTO.referenceNo).remove(oldEdms_documentsDTO);
					}
					if(mapOfEdms_documentsDTOToreferenceNo.get(oldEdms_documentsDTO.referenceNo).isEmpty()) {
						mapOfEdms_documentsDTOToreferenceNo.remove(oldEdms_documentsDTO.referenceNo);
					}
					
					if(mapOfEdms_documentsDTOTodescription.containsKey(oldEdms_documentsDTO.description)) {
						mapOfEdms_documentsDTOTodescription.get(oldEdms_documentsDTO.description).remove(oldEdms_documentsDTO);
					}
					if(mapOfEdms_documentsDTOTodescription.get(oldEdms_documentsDTO.description).isEmpty()) {
						mapOfEdms_documentsDTOTodescription.remove(oldEdms_documentsDTO.description);
					}
					
					if(mapOfEdms_documentsDTOTofilesDropzone.containsKey(oldEdms_documentsDTO.filesDropzone)) {
						mapOfEdms_documentsDTOTofilesDropzone.get(oldEdms_documentsDTO.filesDropzone).remove(oldEdms_documentsDTO);
					}
					if(mapOfEdms_documentsDTOTofilesDropzone.get(oldEdms_documentsDTO.filesDropzone).isEmpty()) {
						mapOfEdms_documentsDTOTofilesDropzone.remove(oldEdms_documentsDTO.filesDropzone);
					}
					
					if(mapOfEdms_documentsDTOToisConfidential.containsKey(oldEdms_documentsDTO.isConfidential)) {
						mapOfEdms_documentsDTOToisConfidential.get(oldEdms_documentsDTO.isConfidential).remove(oldEdms_documentsDTO);
					}
					if(mapOfEdms_documentsDTOToisConfidential.get(oldEdms_documentsDTO.isConfidential).isEmpty()) {
						mapOfEdms_documentsDTOToisConfidential.remove(oldEdms_documentsDTO.isConfidential);
					}
					
					if(mapOfEdms_documentsDTOTodocConfidentialDate.containsKey(oldEdms_documentsDTO.docConfidentialDate)) {
						mapOfEdms_documentsDTOTodocConfidentialDate.get(oldEdms_documentsDTO.docConfidentialDate).remove(oldEdms_documentsDTO);
					}
					if(mapOfEdms_documentsDTOTodocConfidentialDate.get(oldEdms_documentsDTO.docConfidentialDate).isEmpty()) {
						mapOfEdms_documentsDTOTodocConfidentialDate.remove(oldEdms_documentsDTO.docConfidentialDate);
					}
					
					if(mapOfEdms_documentsDTOToisLocked.containsKey(oldEdms_documentsDTO.isLocked)) {
						mapOfEdms_documentsDTOToisLocked.get(oldEdms_documentsDTO.isLocked).remove(oldEdms_documentsDTO);
					}
					if(mapOfEdms_documentsDTOToisLocked.get(oldEdms_documentsDTO.isLocked).isEmpty()) {
						mapOfEdms_documentsDTOToisLocked.remove(oldEdms_documentsDTO.isLocked);
					}
					
					if(mapOfEdms_documentsDTOTodocLockedDate.containsKey(oldEdms_documentsDTO.docLockedDate)) {
						mapOfEdms_documentsDTOTodocLockedDate.get(oldEdms_documentsDTO.docLockedDate).remove(oldEdms_documentsDTO);
					}
					if(mapOfEdms_documentsDTOTodocLockedDate.get(oldEdms_documentsDTO.docLockedDate).isEmpty()) {
						mapOfEdms_documentsDTOTodocLockedDate.remove(oldEdms_documentsDTO.docLockedDate);
					}
					
					if(mapOfEdms_documentsDTOToparentDocumentsId.containsKey(oldEdms_documentsDTO.parentDocumentsId)) {
						mapOfEdms_documentsDTOToparentDocumentsId.get(oldEdms_documentsDTO.parentDocumentsId).remove(oldEdms_documentsDTO);
					}
					if(mapOfEdms_documentsDTOToparentDocumentsId.get(oldEdms_documentsDTO.parentDocumentsId).isEmpty()) {
						mapOfEdms_documentsDTOToparentDocumentsId.remove(oldEdms_documentsDTO.parentDocumentsId);
					}
					
					if(mapOfEdms_documentsDTOTouserId.containsKey(oldEdms_documentsDTO.userId)) {
						mapOfEdms_documentsDTOTouserId.get(oldEdms_documentsDTO.userId).remove(oldEdms_documentsDTO);
					}
					if(mapOfEdms_documentsDTOTouserId.get(oldEdms_documentsDTO.userId).isEmpty()) {
						mapOfEdms_documentsDTOTouserId.remove(oldEdms_documentsDTO.userId);
					}
					
					if(mapOfEdms_documentsDTOTojobCat.containsKey(oldEdms_documentsDTO.jobCat)) {
						mapOfEdms_documentsDTOTojobCat.get(oldEdms_documentsDTO.jobCat).remove(oldEdms_documentsDTO);
					}
					if(mapOfEdms_documentsDTOTojobCat.get(oldEdms_documentsDTO.jobCat).isEmpty()) {
						mapOfEdms_documentsDTOTojobCat.remove(oldEdms_documentsDTO.jobCat);
					}
					
					if(mapOfEdms_documentsDTOTolastModificationTime.containsKey(oldEdms_documentsDTO.lastModificationTime)) {
						mapOfEdms_documentsDTOTolastModificationTime.get(oldEdms_documentsDTO.lastModificationTime).remove(oldEdms_documentsDTO);
					}
					if(mapOfEdms_documentsDTOTolastModificationTime.get(oldEdms_documentsDTO.lastModificationTime).isEmpty()) {
						mapOfEdms_documentsDTOTolastModificationTime.remove(oldEdms_documentsDTO.lastModificationTime);
					}
					
					
				}
				if(edms_documentsDTO.isDeleted == 0) 
				{
					
					mapOfEdms_documentsDTOToiD.put(edms_documentsDTO.iD, edms_documentsDTO);
				
					if( ! mapOfEdms_documentsDTOTodocumentProcessCat.containsKey(edms_documentsDTO.documentProcessCat)) {
						mapOfEdms_documentsDTOTodocumentProcessCat.put(edms_documentsDTO.documentProcessCat, new HashSet<>());
					}
					mapOfEdms_documentsDTOTodocumentProcessCat.get(edms_documentsDTO.documentProcessCat).add(edms_documentsDTO);
					
					if( ! mapOfEdms_documentsDTOTodocumentNo.containsKey(edms_documentsDTO.documentNo)) {
						mapOfEdms_documentsDTOTodocumentNo.put(edms_documentsDTO.documentNo, new HashSet<>());
					}
					mapOfEdms_documentsDTOTodocumentNo.get(edms_documentsDTO.documentNo).add(edms_documentsDTO);
					
					if( ! mapOfEdms_documentsDTOTopbrlpContractorType.containsKey(edms_documentsDTO.pbrlpContractorType)) {
						mapOfEdms_documentsDTOTopbrlpContractorType.put(edms_documentsDTO.pbrlpContractorType, new HashSet<>());
					}
					mapOfEdms_documentsDTOTopbrlpContractorType.get(edms_documentsDTO.pbrlpContractorType).add(edms_documentsDTO);
					
					if( ! mapOfEdms_documentsDTOTofileIndexTypeType.containsKey(edms_documentsDTO.fileIndexTypeType)) {
						mapOfEdms_documentsDTOTofileIndexTypeType.put(edms_documentsDTO.fileIndexTypeType, new HashSet<>());
					}
					mapOfEdms_documentsDTOTofileIndexTypeType.get(edms_documentsDTO.fileIndexTypeType).add(edms_documentsDTO);
					
					if( ! mapOfEdms_documentsDTOTofileIndexSubtypeType.containsKey(edms_documentsDTO.fileIndexSubtypeType)) {
						mapOfEdms_documentsDTOTofileIndexSubtypeType.put(edms_documentsDTO.fileIndexSubtypeType, new HashSet<>());
					}
					mapOfEdms_documentsDTOTofileIndexSubtypeType.get(edms_documentsDTO.fileIndexSubtypeType).add(edms_documentsDTO);
					
					if( ! mapOfEdms_documentsDTOToofficesType.containsKey(edms_documentsDTO.officesType)) {
						mapOfEdms_documentsDTOToofficesType.put(edms_documentsDTO.officesType, new HashSet<>());
					}
					mapOfEdms_documentsDTOToofficesType.get(edms_documentsDTO.officesType).add(edms_documentsDTO);
					
					if( ! mapOfEdms_documentsDTOTodocumentTypeCat.containsKey(edms_documentsDTO.documentTypeCat)) {
						mapOfEdms_documentsDTOTodocumentTypeCat.put(edms_documentsDTO.documentTypeCat, new HashSet<>());
					}
					mapOfEdms_documentsDTOTodocumentTypeCat.get(edms_documentsDTO.documentTypeCat).add(edms_documentsDTO);
					
					if( ! mapOfEdms_documentsDTOTodocumentDate.containsKey(edms_documentsDTO.documentDate)) {
						mapOfEdms_documentsDTOTodocumentDate.put(edms_documentsDTO.documentDate, new HashSet<>());
					}
					mapOfEdms_documentsDTOTodocumentDate.get(edms_documentsDTO.documentDate).add(edms_documentsDTO);
					
					if( ! mapOfEdms_documentsDTOTodocumentReceiveDate.containsKey(edms_documentsDTO.documentReceiveDate)) {
						mapOfEdms_documentsDTOTodocumentReceiveDate.put(edms_documentsDTO.documentReceiveDate, new HashSet<>());
					}
					mapOfEdms_documentsDTOTodocumentReceiveDate.get(edms_documentsDTO.documentReceiveDate).add(edms_documentsDTO);
					
					if( ! mapOfEdms_documentsDTOTodocumentSubject.containsKey(edms_documentsDTO.documentSubject)) {
						mapOfEdms_documentsDTOTodocumentSubject.put(edms_documentsDTO.documentSubject, new HashSet<>());
					}
					mapOfEdms_documentsDTOTodocumentSubject.get(edms_documentsDTO.documentSubject).add(edms_documentsDTO);
					
					if( ! mapOfEdms_documentsDTOToreferenceNo.containsKey(edms_documentsDTO.referenceNo)) {
						mapOfEdms_documentsDTOToreferenceNo.put(edms_documentsDTO.referenceNo, new HashSet<>());
					}
					mapOfEdms_documentsDTOToreferenceNo.get(edms_documentsDTO.referenceNo).add(edms_documentsDTO);
					
					if( ! mapOfEdms_documentsDTOTodescription.containsKey(edms_documentsDTO.description)) {
						mapOfEdms_documentsDTOTodescription.put(edms_documentsDTO.description, new HashSet<>());
					}
					mapOfEdms_documentsDTOTodescription.get(edms_documentsDTO.description).add(edms_documentsDTO);
					
					if( ! mapOfEdms_documentsDTOTofilesDropzone.containsKey(edms_documentsDTO.filesDropzone)) {
						mapOfEdms_documentsDTOTofilesDropzone.put(edms_documentsDTO.filesDropzone, new HashSet<>());
					}
					mapOfEdms_documentsDTOTofilesDropzone.get(edms_documentsDTO.filesDropzone).add(edms_documentsDTO);
					
					if( ! mapOfEdms_documentsDTOToisConfidential.containsKey(edms_documentsDTO.isConfidential)) {
						mapOfEdms_documentsDTOToisConfidential.put(edms_documentsDTO.isConfidential, new HashSet<>());
					}
					mapOfEdms_documentsDTOToisConfidential.get(edms_documentsDTO.isConfidential).add(edms_documentsDTO);
					
					if( ! mapOfEdms_documentsDTOTodocConfidentialDate.containsKey(edms_documentsDTO.docConfidentialDate)) {
						mapOfEdms_documentsDTOTodocConfidentialDate.put(edms_documentsDTO.docConfidentialDate, new HashSet<>());
					}
					mapOfEdms_documentsDTOTodocConfidentialDate.get(edms_documentsDTO.docConfidentialDate).add(edms_documentsDTO);
					
					if( ! mapOfEdms_documentsDTOToisLocked.containsKey(edms_documentsDTO.isLocked)) {
						mapOfEdms_documentsDTOToisLocked.put(edms_documentsDTO.isLocked, new HashSet<>());
					}
					mapOfEdms_documentsDTOToisLocked.get(edms_documentsDTO.isLocked).add(edms_documentsDTO);
					
					if( ! mapOfEdms_documentsDTOTodocLockedDate.containsKey(edms_documentsDTO.docLockedDate)) {
						mapOfEdms_documentsDTOTodocLockedDate.put(edms_documentsDTO.docLockedDate, new HashSet<>());
					}
					mapOfEdms_documentsDTOTodocLockedDate.get(edms_documentsDTO.docLockedDate).add(edms_documentsDTO);
					
					if( ! mapOfEdms_documentsDTOToparentDocumentsId.containsKey(edms_documentsDTO.parentDocumentsId)) {
						mapOfEdms_documentsDTOToparentDocumentsId.put(edms_documentsDTO.parentDocumentsId, new HashSet<>());
					}
					mapOfEdms_documentsDTOToparentDocumentsId.get(edms_documentsDTO.parentDocumentsId).add(edms_documentsDTO);
					
					if( ! mapOfEdms_documentsDTOTouserId.containsKey(edms_documentsDTO.userId)) {
						mapOfEdms_documentsDTOTouserId.put(edms_documentsDTO.userId, new HashSet<>());
					}
					mapOfEdms_documentsDTOTouserId.get(edms_documentsDTO.userId).add(edms_documentsDTO);
					
					if( ! mapOfEdms_documentsDTOTojobCat.containsKey(edms_documentsDTO.jobCat)) {
						mapOfEdms_documentsDTOTojobCat.put(edms_documentsDTO.jobCat, new HashSet<>());
					}
					mapOfEdms_documentsDTOTojobCat.get(edms_documentsDTO.jobCat).add(edms_documentsDTO);
					
					if( ! mapOfEdms_documentsDTOTolastModificationTime.containsKey(edms_documentsDTO.lastModificationTime)) {
						mapOfEdms_documentsDTOTolastModificationTime.put(edms_documentsDTO.lastModificationTime, new HashSet<>());
					}
					mapOfEdms_documentsDTOTolastModificationTime.get(edms_documentsDTO.lastModificationTime).add(edms_documentsDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Edms_documentsDTO> getEdms_documentsList() {
		List <Edms_documentsDTO> edms_documentss = new ArrayList<Edms_documentsDTO>(this.mapOfEdms_documentsDTOToiD.values());
		return edms_documentss;
	}
	
	
	public Edms_documentsDTO getEdms_documentsDTOByID( long ID){
		return mapOfEdms_documentsDTOToiD.get(ID);
	}
	
	
	public List<Edms_documentsDTO> getEdms_documentsDTOBydocument_process_cat(int document_process_cat) {
		return new ArrayList<>( mapOfEdms_documentsDTOTodocumentProcessCat.getOrDefault(document_process_cat,new HashSet<>()));
	}
	
	
	public List<Edms_documentsDTO> getEdms_documentsDTOBydocument_no(String document_no) {
		return new ArrayList<>( mapOfEdms_documentsDTOTodocumentNo.getOrDefault(document_no,new HashSet<>()));
	}
	
	
	public List<Edms_documentsDTO> getEdms_documentsDTOBypbrlp_contractor_type(long pbrlp_contractor_type) {
		return new ArrayList<>( mapOfEdms_documentsDTOTopbrlpContractorType.getOrDefault(pbrlp_contractor_type,new HashSet<>()));
	}
	
	
	public List<Edms_documentsDTO> getEdms_documentsDTOByfile_index_type_type(int file_index_type_type) {
		return new ArrayList<>( mapOfEdms_documentsDTOTofileIndexTypeType.getOrDefault(file_index_type_type,new HashSet<>()));
	}
	
	
	public List<Edms_documentsDTO> getEdms_documentsDTOByfile_index_subtype_type(int file_index_subtype_type) {
		return new ArrayList<>( mapOfEdms_documentsDTOTofileIndexSubtypeType.getOrDefault(file_index_subtype_type,new HashSet<>()));
	}
	
	
	public List<Edms_documentsDTO> getEdms_documentsDTOByoffices_type(long offices_type) {
		return new ArrayList<>( mapOfEdms_documentsDTOToofficesType.getOrDefault(offices_type,new HashSet<>()));
	}
	
	
	public List<Edms_documentsDTO> getEdms_documentsDTOBydocument_type_cat(int document_type_cat) {
		return new ArrayList<>( mapOfEdms_documentsDTOTodocumentTypeCat.getOrDefault(document_type_cat,new HashSet<>()));
	}
	
	
	public List<Edms_documentsDTO> getEdms_documentsDTOBydocument_date(long document_date) {
		return new ArrayList<>( mapOfEdms_documentsDTOTodocumentDate.getOrDefault(document_date,new HashSet<>()));
	}
	
	
	public List<Edms_documentsDTO> getEdms_documentsDTOBydocument_receive_date(long document_receive_date) {
		return new ArrayList<>( mapOfEdms_documentsDTOTodocumentReceiveDate.getOrDefault(document_receive_date,new HashSet<>()));
	}
	
	
	public List<Edms_documentsDTO> getEdms_documentsDTOBydocument_subject(String document_subject) {
		return new ArrayList<>( mapOfEdms_documentsDTOTodocumentSubject.getOrDefault(document_subject,new HashSet<>()));
	}
	
	
	public List<Edms_documentsDTO> getEdms_documentsDTOByreference_no(String reference_no) {
		return new ArrayList<>( mapOfEdms_documentsDTOToreferenceNo.getOrDefault(reference_no,new HashSet<>()));
	}
	
	
	public List<Edms_documentsDTO> getEdms_documentsDTOBydescription(String description) {
		return new ArrayList<>( mapOfEdms_documentsDTOTodescription.getOrDefault(description,new HashSet<>()));
	}
	
	
	public List<Edms_documentsDTO> getEdms_documentsDTOByfiles_dropzone(long files_dropzone) {
		return new ArrayList<>( mapOfEdms_documentsDTOTofilesDropzone.getOrDefault(files_dropzone,new HashSet<>()));
	}
	
	
	public List<Edms_documentsDTO> getEdms_documentsDTOByis_confidential(boolean is_confidential) {
		return new ArrayList<>( mapOfEdms_documentsDTOToisConfidential.getOrDefault(is_confidential,new HashSet<>()));
	}
	
	
	public List<Edms_documentsDTO> getEdms_documentsDTOBydoc_confidential_date(long doc_confidential_date) {
		return new ArrayList<>( mapOfEdms_documentsDTOTodocConfidentialDate.getOrDefault(doc_confidential_date,new HashSet<>()));
	}
	
	
	public List<Edms_documentsDTO> getEdms_documentsDTOByis_locked(boolean is_locked) {
		return new ArrayList<>( mapOfEdms_documentsDTOToisLocked.getOrDefault(is_locked,new HashSet<>()));
	}
	
	
	public List<Edms_documentsDTO> getEdms_documentsDTOBydoc_locked_date(long doc_locked_date) {
		return new ArrayList<>( mapOfEdms_documentsDTOTodocLockedDate.getOrDefault(doc_locked_date,new HashSet<>()));
	}
	
	
	public List<Edms_documentsDTO> getEdms_documentsDTOByparent_documents_id(long parent_documents_id) {
		return new ArrayList<>( mapOfEdms_documentsDTOToparentDocumentsId.getOrDefault(parent_documents_id,new HashSet<>()));
	}
	
	
	public List<Edms_documentsDTO> getEdms_documentsDTOByuser_id(long user_id) {
		return new ArrayList<>( mapOfEdms_documentsDTOTouserId.getOrDefault(user_id,new HashSet<>()));
	}
	
	
	public List<Edms_documentsDTO> getEdms_documentsDTOByjob_cat(int job_cat) {
		return new ArrayList<>( mapOfEdms_documentsDTOTojobCat.getOrDefault(job_cat,new HashSet<>()));
	}
	
	
	public List<Edms_documentsDTO> getEdms_documentsDTOBylastModificationTime(long lastModificationTime) {
		return new ArrayList<>( mapOfEdms_documentsDTOTolastModificationTime.getOrDefault(lastModificationTime,new HashSet<>()));
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "edms_documents";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


