package edms_documents;

import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Date;
import java.util.Map.Entry;
import sessionmanager.SessionConstants;


import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import dbm.*;

import repository.RepositoryManager;

import util.*;
import user.UserDTO;

public class Edms_documentsDAO  extends NavigationService4
{
	
	private final SimpleDateFormat sdf = new SimpleDateFormat( "dd/MM/yyyy" );
	private final SimpleDateFormat sdfyyyyMMdd = new SimpleDateFormat( "yyyy-MM-dd" );
	
	public CommonMaps approvalMaps = new Edms_documentsApprovalMAPS("edms_documents");

	Logger logger = Logger.getLogger(getClass());

	public List<ReferenceDocDTO> getReferenceDocListBySearhcText(
			String docSubject,
			String officeId,
			String indexType,
			String indexSubType,
			String docNumber,
			String docProcess,
			String docType,
			String docDateFrom,
			String docDateTo,
			String source,
			String refNo

	) throws Exception {

		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		List<ReferenceDocDTO> referenceDocDTOList = new ArrayList<>();

		try {

			StringBuilder sb = new StringBuilder();

			sb.append( "select " +
					"edms_documents.ID as ID, " +
					"document_subject, " +
					"document_no, " +
					"lang2.languageTextEnglish as document_process_cat, " +
					"reference_no, " +
					"document_date, " +
					"reference_no, " +
					"con.name_en as source, " +
					"lang.languageTextEnglish as docType " +
					"from edms_documents " +
					"LEFT JOIN pbrlp_contractor con on edms_documents.pbrlp_contractor_type=con.ID " +

					"Left join category cat on ( edms_documents.document_type_cat = cat.value  and cat.domain_name = '" + CommonConstant.EDMS_CATEGORY_DOMAIN_DOC_TYPE + "' ) " +
					"LEFT JOIN language_text lang on cat.language_id = lang.ID " +

					"Left join category cat2 on ( edms_documents.document_process_cat = cat2.value  and cat2.domain_name = '" + CommonConstant.EDMS_CATEGORY_DOMAIN_DOC_PROCESS + "' ) " +
					"LEFT JOIN language_text lang2 on cat2.language_id = lang2.ID "
					 );

			sb.append( " where " );

			if( !StringUtils.isBlank( officeId ) )
				sb.append( " offices_type = " + officeId + " and " );

			if( !StringUtils.isBlank( docSubject ) )
				sb.append( " document_subject like '%" + docSubject + "%' and " );

			if( !StringUtils.isBlank( indexType ) )
				sb.append( " file_index_type_type = " + indexType + " and " );

			if( !StringUtils.isBlank( indexSubType ) )
				sb.append( " file_index_subtype_type = " + indexSubType + " and " );

			if( !StringUtils.isBlank( docNumber ) )
				sb.append( " document_no = " + docNumber + " and " );

			if( !StringUtils.isBlank( docProcess ) )
				sb.append( " cat2.value = " + docProcess + " and " );

			if( !StringUtils.isBlank( docType ) )
				sb.append( " cat.value = " + docType + " and " );

			if( !StringUtils.isBlank( docDateFrom ) ){

				Long dateFrom = sdf.parse( docDateFrom ).getTime();
				sb.append( " document_date >= " + dateFrom  + " and " );
			}

			if(	!StringUtils.isBlank( docDateTo ) ){

				Long dateTo = sdf.parse( docDateTo ).getTime();
				sb.append( " document_date <= " + dateTo  + " and " );
			}

			if( !StringUtils.isBlank( source ) ){

				sb.append( " pbrlp_contractor_type = " + source + " and " );
			}

			if( !StringUtils.isBlank( refNo ) ){

				refNo = refNo.replace( "/" , "/%" );
				sb.append( " reference_no like '%" + refNo + "%' and " );
			}


			String sql = sb.toString();

			if( sql.trim().endsWith( "and" ) )
				sql = sql.substring( 0, sql.lastIndexOf( "and" ) );

			if( sql.trim().endsWith( "where" ) )
				sql = sql.substring( 0, sql.lastIndexOf( "where" ) );

			sql += " limit 15";

			printSql(sql);

			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();

			rs = stmt.executeQuery(sql);

			while (rs.next()) {

				ReferenceDocDTO referenceDocDTO = new ReferenceDocDTO();

				String documentDate = rs.getString( "document_date" );
				String formatedDocumentDate = sdf.format( new Date( Long.parseLong( documentDate ) ) );

				referenceDocDTO.setId( rs.getLong( "ID" ) );
				referenceDocDTO.setDocNumber( rs.getString( "document_no" ) );
				referenceDocDTO.setProcessType( rs.getString( "document_process_cat" ) );
				referenceDocDTO.setDocRef( rs.getString( "reference_no" ) );
				referenceDocDTO.setDate( formatedDocumentDate );
				referenceDocDTO.setDocSubject( rs.getString( "document_subject" ) );
				referenceDocDTO.setDocRef( rs.getString( "reference_no" ) );
				referenceDocDTO.setDocSource( rs.getString( "source" ) );
				referenceDocDTO.setDocType( rs.getString( "docType" ) );

				referenceDocDTOList.add(referenceDocDTO);
			}

		} catch (Exception ex) {

			ex.printStackTrace();
			throw new Exception( "Error while searching for refence document" );
		}
		finally {

			try {

				if (stmt != null) {

					stmt.close();
				}
			}
			catch (Exception e) {

			}

			try {

				if (connection != null) {
					DBMR.getInstance().freeConnection(connection);
				}
			} catch (Exception ex2) {
			}
		}
		return referenceDocDTOList;
	}

	public String getIndexSubType(long ID) {
		return null;
	}
	
	public Edms_documentsDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";
		joinSQL += " join pbrlp_contractor on " + tableName + ".pbrlp_contractor_type = pbrlp_contractor.ID ";
		joinSQL += " left join file_index_type on " + tableName + ".file_index_type_type = file_index_type.ID ";
		joinSQL += " left join file_index_subtype on " + tableName + ".file_index_subtype_type = file_index_subtype.ID ";
		joinSQL += " join office_units on " + tableName + ".offices_type = office_units.ID ";

		joinSQL += " join category on (";
		joinSQL += " (" + tableName + ".document_process_cat = category.value  and category.domain_name = 'document_process')";
		joinSQL += " or "; 
		joinSQL += " (" + tableName + ".document_type_cat = category.value  and category.domain_name = 'document_type')";
		joinSQL += " or "; 
		joinSQL += " (" + tableName + ".job_cat = category.value  and category.domain_name = 'job')";
		joinSQL += " )";
		joinSQL += " join language_text on category.language_id = language_text.id";		
		commonMaps = new Edms_documentsMAPS(tableName);
	}
	
	public Edms_documentsDAO()
	{
		this("edms_documents");		
	}
	
	
	
	public long add(CommonDTO commonDTO) throws Exception
	{
		
		Edms_documentsDTO edms_documentsDTO = (Edms_documentsDTO)commonDTO;
		
		Connection connection = null;
		PreparedStatement ps = null;

		long lastModificationTime = System.currentTimeMillis();	

		try{
			connection = DBMW.getInstance().getConnection();
			
			if(connection == null)
			{
				System.out.println("nullconn");
			}

			edms_documentsDTO.iD = DBMW.getInstance().getNextSequenceId(tableName);

			String sql = "INSERT INTO " + tableName;
			
			sql += " (";
			sql += "ID";
			sql += ", ";
			sql += "document_process_cat";
			sql += ", ";
			sql += "document_no";
			sql += ", ";
			sql += "pbrlp_contractor_type";
			sql += ", ";
			sql += "other_contractor";
			sql += ", ";
			sql += "file_index_type_type";
			sql += ", ";
			sql += "file_index_subtype_type";
			sql += ", ";
			sql += "offices_type";
			sql += ", ";
			sql += "document_type_cat";
			sql += ", ";
			sql += "document_date";
			sql += ", ";
			sql += "document_receive_date";
			sql += ", ";
			sql += "document_subject";
			sql += ", ";
			sql += "reference_no";

			sql += ", ";
			sql += "reference_no_part1";
			sql += ", ";
			sql += "reference_no_part2";
			sql += ", ";
			sql += "reference_no_part3";
			sql += ", ";
			sql += "reference_no_part4";
			sql += ", ";
			sql += "reference_no_part5";
			sql += ", ";
			sql += "reference_no_part6";

			sql += ", ";
			sql += "description";
			sql += ", ";
			sql += "files_dropzone";
			sql += ", ";
			sql += "is_confidential";
			sql += ", ";
			sql += "doc_confidential_date";
			sql += ", ";
			sql += "is_locked";
			sql += ", ";
			sql += "doc_locked_date";
			sql += ", ";
			sql += "parent_documents_id";
			sql += ", ";
			sql += "user_id";
			sql += ", ";
			sql += "job_cat";
			sql += ", ";
			sql += "doc_added";
			sql += ", ";
			sql += "deadline";

			sql += ", ";
			sql += "section";
			sql += ", ";
			sql += "rfi_status";
			sql += ", ";
			sql += "rfi_status_other";
			sql += ", ";
			sql += "rfi_type";
			sql += ", ";
			sql += "rfi_distribution";
			sql += ", ";
			sql += "crec_no";
			sql += ", ";
			sql += "ncr_submission_date";
			sql += ", ";
			sql += "open_reason";


			sql += ", ";
			sql += "isDeleted";
			sql += ", ";
			sql += "lastModificationTime";
		
			sql += ")";
			
			
            sql += " VALUES(";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";

			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";

			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";

			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			
			sql += ")";
				

			ps = connection.prepareStatement(sql);
			
			
			

			int index = 1;

			ps.setObject(index++,edms_documentsDTO.iD);
			ps.setObject(index++,edms_documentsDTO.documentProcessCat);
			ps.setObject(index++,edms_documentsDTO.documentNo);
			ps.setObject(index++,edms_documentsDTO.pbrlpContractorType);
			ps.setString(index++, edms_documentsDTO.otherContractor );
			ps.setObject(index++,edms_documentsDTO.fileIndexTypeType);
			ps.setObject(index++,edms_documentsDTO.fileIndexSubtypeType);
			ps.setObject(index++,edms_documentsDTO.officesType);
			ps.setObject(index++,edms_documentsDTO.documentTypeCat);
			ps.setObject(index++,edms_documentsDTO.documentDate);
			ps.setObject(index++,edms_documentsDTO.documentReceiveDate);
			ps.setObject(index++,edms_documentsDTO.documentSubject);

			ps.setObject(index++,edms_documentsDTO.referenceNo );
			ps.setObject(index++,edms_documentsDTO.refPart1 );
			ps.setObject(index++,edms_documentsDTO.refPart2 );
			ps.setObject(index++,edms_documentsDTO.refPart3 );
			ps.setObject(index++,edms_documentsDTO.refPart4 );
			ps.setObject(index++,edms_documentsDTO.refPart5 );
			ps.setObject(index++,edms_documentsDTO.refPart6 );

			ps.setObject(index++,edms_documentsDTO.description);
			ps.setObject(index++,edms_documentsDTO.filesDropzone);
			ps.setObject(index++,edms_documentsDTO.isConfidential);
			ps.setObject(index++,edms_documentsDTO.docConfidentialDate);
			ps.setObject(index++,edms_documentsDTO.isLocked);
			ps.setObject(index++,edms_documentsDTO.docLockedDate);
			ps.setObject(index++,edms_documentsDTO.parentDocumentsId);
			ps.setObject(index++,edms_documentsDTO.userId);
			ps.setObject(index++,edms_documentsDTO.jobCat);
			ps.setLong( index++, edms_documentsDTO.docAdded );
			ps.setLong( index++, edms_documentsDTO.deadline );

			ps.setObject(index++, edms_documentsDTO.section);
			ps.setInt(index++, edms_documentsDTO.rfiStatus);
			ps.setString(index++, edms_documentsDTO.rfiStatusOther);
			ps.setInt(index++,edms_documentsDTO.rfiType);
			ps.setInt(index++,edms_documentsDTO.rfiDistribution);
			ps.setString(index++, edms_documentsDTO.crecNo );
			ps.setLong(index++, edms_documentsDTO.ncrSubmissionDate );
			ps.setString(index++,edms_documentsDTO.openReason);

			ps.setObject(index++,edms_documentsDTO.isDeleted);
			ps.setObject(index++, lastModificationTime);
			
			System.out.println(ps);
			ps.execute();
			
			
			recordUpdateTime(connection, lastModificationTime);

		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if (ps != null) {
					ps.close();
				}
			} catch(Exception e){}
			try{
				if(connection != null)
				{
					DBMW.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		return edms_documentsDTO.iD;		
	}
	
	public Edms_documentsDTO getDTOByRef (String to, String year, String refno)
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Edms_documentsDTO edms_documentsDTO = null;
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
			
            sql += " WHERE isDeleted = 0 and reference_no_part2='" + to + "'"
            		+ " and reference_no_part5 = '" + year + "'"
            		+ " and reference_no_part6 = '" + refno + "'" ;
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			if(rs.next()){
				edms_documentsDTO = new Edms_documentsDTO();

				edms_documentsDTO.iD = rs.getLong("ID");
				edms_documentsDTO.documentProcessCat = rs.getInt("document_process_cat");
				edms_documentsDTO.documentNo = rs.getString("document_no");
				edms_documentsDTO.pbrlpContractorType = rs.getLong("pbrlp_contractor_type");
				edms_documentsDTO.otherContractor = rs.getString( "other_contractor" );
				edms_documentsDTO.fileIndexTypeType = rs.getInt("file_index_type_type");
				edms_documentsDTO.fileIndexSubtypeType = rs.getInt("file_index_subtype_type");
				edms_documentsDTO.officesType = rs.getLong("offices_type");
				edms_documentsDTO.documentTypeCat = rs.getInt("document_type_cat");
				edms_documentsDTO.documentDate = rs.getLong("document_date");
				edms_documentsDTO.documentReceiveDate = rs.getLong("document_receive_date");
				edms_documentsDTO.documentSubject = rs.getString("document_subject");

				edms_documentsDTO.referenceNo = rs.getString("reference_no");
				edms_documentsDTO.refPart1 = rs.getString( "reference_no_part1" );
				edms_documentsDTO.refPart2 = rs.getString( "reference_no_part2" );
				edms_documentsDTO.refPart3 = rs.getString( "reference_no_part3" );
				edms_documentsDTO.refPart4 = rs.getString( "reference_no_part4" );
				edms_documentsDTO.refPart5 = rs.getString( "reference_no_part5" );
				edms_documentsDTO.refPart6 = rs.getString( "reference_no_part6" );

				edms_documentsDTO.description = rs.getString("description");
				edms_documentsDTO.filesDropzone = rs.getLong("files_dropzone");
				edms_documentsDTO.isConfidential = rs.getBoolean("is_confidential");
				edms_documentsDTO.docConfidentialDate = rs.getLong("doc_confidential_date");
				edms_documentsDTO.isLocked = rs.getBoolean("is_locked");
				edms_documentsDTO.docLockedDate = rs.getLong("doc_locked_date");
				edms_documentsDTO.parentDocumentsId = rs.getLong("parent_documents_id");
				edms_documentsDTO.userId = rs.getLong("user_id");
				edms_documentsDTO.jobCat = rs.getInt("job_cat");
				edms_documentsDTO.deadline = rs.getLong( "deadline" );

				edms_documentsDTO.section = rs.getInt("section");
				edms_documentsDTO.rfiStatus = rs.getInt("rfi_status");
				edms_documentsDTO.rfiStatusOther = rs.getString("rfi_status_other");
				edms_documentsDTO.rfiType = rs.getInt( "rfi_type" );
				edms_documentsDTO.rfiDistribution = rs.getInt( "rfi_distribution" );
				edms_documentsDTO.crecNo = rs.getString( "crec_no" );
				edms_documentsDTO.ncrSubmissionDate = rs.getLong( "ncr_submission_date" );
				edms_documentsDTO.openReason = rs.getString( "open_reason" );

				edms_documentsDTO.isDeleted = rs.getInt("isDeleted");
				edms_documentsDTO.lastModificationTime = rs.getLong("lastModificationTime");


			}			

		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return edms_documentsDTO;
	}
		
	

	//need another getter for repository
	public CommonDTO getDTOByID (long ID) throws Exception
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Edms_documentsDTO edms_documentsDTO = null;
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
			
            sql += " WHERE ID=" + ID;
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			if(rs.next()){
				edms_documentsDTO = new Edms_documentsDTO();

				edms_documentsDTO.iD = rs.getLong("ID");
				edms_documentsDTO.documentProcessCat = rs.getInt("document_process_cat");
				edms_documentsDTO.documentNo = rs.getString("document_no");
				edms_documentsDTO.pbrlpContractorType = rs.getLong("pbrlp_contractor_type");
				edms_documentsDTO.otherContractor = rs.getString( "other_contractor" );
				edms_documentsDTO.fileIndexTypeType = rs.getInt("file_index_type_type");
				edms_documentsDTO.fileIndexSubtypeType = rs.getInt("file_index_subtype_type");
				edms_documentsDTO.officesType = rs.getLong("offices_type");
				edms_documentsDTO.documentTypeCat = rs.getInt("document_type_cat");
				edms_documentsDTO.documentDate = rs.getLong("document_date");
				edms_documentsDTO.documentReceiveDate = rs.getLong("document_receive_date");
				edms_documentsDTO.documentSubject = rs.getString("document_subject");

				edms_documentsDTO.referenceNo = rs.getString("reference_no");
				edms_documentsDTO.refPart1 = rs.getString( "reference_no_part1" );
				edms_documentsDTO.refPart2 = rs.getString( "reference_no_part2" );
				edms_documentsDTO.refPart3 = rs.getString( "reference_no_part3" );
				edms_documentsDTO.refPart4 = rs.getString( "reference_no_part4" );
				edms_documentsDTO.refPart5 = rs.getString( "reference_no_part5" );
				edms_documentsDTO.refPart6 = rs.getString( "reference_no_part6" );

				edms_documentsDTO.description = rs.getString("description");
				edms_documentsDTO.filesDropzone = rs.getLong("files_dropzone");
				edms_documentsDTO.isConfidential = rs.getBoolean("is_confidential");
				edms_documentsDTO.docConfidentialDate = rs.getLong("doc_confidential_date");
				edms_documentsDTO.isLocked = rs.getBoolean("is_locked");
				edms_documentsDTO.docLockedDate = rs.getLong("doc_locked_date");
				edms_documentsDTO.parentDocumentsId = rs.getLong("parent_documents_id");
				edms_documentsDTO.userId = rs.getLong("user_id");
				edms_documentsDTO.jobCat = rs.getInt("job_cat");
				edms_documentsDTO.deadline = rs.getLong( "deadline" );

				edms_documentsDTO.section = rs.getInt("section");
				edms_documentsDTO.rfiStatus = rs.getInt("rfi_status");
				edms_documentsDTO.rfiStatusOther = rs.getString("rfi_status_other");
				edms_documentsDTO.rfiType = rs.getInt( "rfi_type" );
				edms_documentsDTO.rfiDistribution = rs.getInt( "rfi_distribution" );
				edms_documentsDTO.crecNo = rs.getString( "crec_no" );
				edms_documentsDTO.ncrSubmissionDate = rs.getLong( "ncr_submission_date" );
				edms_documentsDTO.openReason = rs.getString( "open_reason" );

				edms_documentsDTO.isDeleted = rs.getInt("isDeleted");
				edms_documentsDTO.lastModificationTime = rs.getLong("lastModificationTime");


			}			
			
			
			
			DocumentChainageDAO documentChainageDAO = new DocumentChainageDAO("document_chainage");			
			List<DocumentChainageDTO> documentChainageDTOList = documentChainageDAO.getDocumentChainageDTOListByEdmsDocumentsID(edms_documentsDTO.iD);
			edms_documentsDTO.documentChainageDTOList = documentChainageDTOList;
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return edms_documentsDTO;
	}
	

	public Long getEdmsDocumentsCountByOfficeUnitIdAndDocumentProcessCat ( Long officeUnitId, Integer documentProcessCat ) throws Exception
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;

		Long edmsDocumentsCount = 0L;

		try{

			String sql = "SELECT count(*) as cnt ";

			sql += " FROM " + tableName + " " + joinSQL;

			sql += " WHERE " + tableName + ".isDeleted = 0 and document_process_cat = " + documentProcessCat + " and offices_type = " + officeUnitId;

			printSql(sql);

			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			if( rs.next() ){

				edmsDocumentsCount = rs.getLong( "cnt" );
			}


		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}

			try{
				if (connection != null)
				{
					DBMR.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}

		return edmsDocumentsCount;
	}

	public Long getEdmsDocumentsCountByDocumentProcessCat ( Integer documentProcessCat ) throws Exception
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;

		Long edmsDocumentsCount = 0L;

		try{

			String sql = "SELECT count(*) as cnt ";

			sql += " FROM " + tableName;

			sql += " WHERE isDeleted = 0 and document_process_cat = " + documentProcessCat;

			printSql(sql);

			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			if( rs.next() ){

				edmsDocumentsCount = rs.getLong( "cnt" );
			}


		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}

			try{
				if (connection != null)
				{
					DBMR.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}

		return edmsDocumentsCount;
	}


	public Long getEdmsDocumentsCountByOfficeUnitIdAndDocumentTypeCat ( Long officeUnitId, Integer documentTypeCat ) throws Exception
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;

		Long edmsDocumentsCount = 0L;

		try{

			String sql = "SELECT count(*) as cnt ";

			sql += " FROM " + tableName;

			sql += " WHERE isDeleted = 0 and document_type_cat = " + documentTypeCat + " and offices_type = " + officeUnitId;

			printSql(sql);

			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			if( rs.next() ){

				edmsDocumentsCount = rs.getLong( "cnt" );
			}


		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}

			try{
				if (connection != null)
				{
					DBMR.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}

		return edmsDocumentsCount;
	}

	public Long getEdmsDocumentsCountByDocumentTypeCat ( Integer documentTypeCat ) throws Exception
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;

		Long edmsDocumentsCount = 0L;

		try{

			String sql = "SELECT count(*) as cnt ";

			sql += " FROM " + tableName;

			sql += " WHERE isDeleted = 0 and document_type_cat = " + documentTypeCat;

			printSql(sql);

			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			if( rs.next() ){

				edmsDocumentsCount = rs.getLong( "cnt" );
			}


		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}

			try{
				if (connection != null)
				{
					DBMR.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}

		return edmsDocumentsCount;
	}


	public Long getEdmsDocumentsCountByOfficeUnitIdAndIsDeletedAndNotMinusOneJobCat(Long officeUnitId, Integer isDeleted ) throws Exception
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Integer jobCat = -1;

		Long edmsDocumentsCount = 0L;

		try{

			String sql = "SELECT count(*) as cnt ";

			sql += " FROM " + tableName;

			sql += " WHERE isDeleted = " + isDeleted + " and offices_type = " + officeUnitId + " and job_cat <> " + jobCat;

			printSql(sql);

			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			if( rs.next() ){

				edmsDocumentsCount = rs.getLong( "cnt" );
			}


		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}

			try{
				if (connection != null)
				{
					DBMR.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}

		return edmsDocumentsCount;
	}

	public Long getEdmsDocumentsCountByIsDeletedNotOneAndOfficeUnitId(Long officeUnitId) throws Exception
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Integer jobCat = -1;

		Long edmsDocumentsCount = 0L;

		try{

			String sql = "SELECT count(*) as cnt ";

			sql += " FROM " + tableName;

			sql += " WHERE isDeleted <> 1 " + " and offices_type = " + officeUnitId;

			printSql(sql);

			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			if( rs.next() ){

				edmsDocumentsCount = rs.getLong( "cnt" );
			}


		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}

			try{
				if (connection != null)
				{
					DBMR.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}

		return edmsDocumentsCount;
	}

	public Long getEdmsDocumentsCountByIsDeletedNotOne() throws Exception
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Integer jobCat = -1;

		Long edmsDocumentsCount = 0L;

		try{

			String sql = "SELECT count(*) as cnt ";

			sql += " FROM " + tableName;

			sql += " WHERE isDeleted <> 1";

			printSql(sql);

			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			if( rs.next() ){

				edmsDocumentsCount = rs.getLong( "cnt" );
			}


		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}

			try{
				if (connection != null)
				{
					DBMR.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}

		return edmsDocumentsCount;
	}

	public Long getEdmsDocumentsCountByIsDeletedAndNotMinusOneJobCat( Integer isDeleted ) throws Exception
	{
		/*
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Integer jobCat = -1;

		Long edmsDocumentsCount = 0L;

		try{

			String sql = "SELECT count(*) as cnt ";

			sql += " FROM " + tableName;

			sql += " WHERE isDeleted = " + isDeleted + " and job_cat <> " + jobCat;

			printSql(sql);

			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			if( rs.next() ){

				edmsDocumentsCount = rs.getLong( "cnt" );
			}


		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}

			try{
				if (connection != null)
				{
					DBMR.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}

		return edmsDocumentsCount;*/
		return 0L;
	}

	public Long getEdmsDocumentsCountByOfficeUnitIdAndStartAndEndLastModificationTime ( Long officeUnitId, Long startLastModificationTime, Long endLastModificationTime ) throws Exception
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;

		Long edmsDocumentsCount = 0L;

		try{

			String sql = "SELECT count(*) as cnt ";

			sql += " FROM " + tableName;

			sql += " WHERE lastModificationTime >= " + startLastModificationTime + " and lastModificationTime <= " + endLastModificationTime + " and offices_type = " + officeUnitId;

			printSql(sql);

			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			if( rs.next() ){

				edmsDocumentsCount = rs.getLong( "cnt" );
			}


		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}

			try{
				if (connection != null)
				{
					DBMR.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}

		return edmsDocumentsCount;
	}

	public Long getEdmsDocumentsCountByStartAndEndLastModificationTime ( Long startLastModificationTime, Long endLastModificationTime ) throws Exception
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;

		Long edmsDocumentsCount = 0L;

		try{

			String sql = "SELECT count(*) as cnt ";

			sql += " FROM " + tableName;

			sql += " WHERE lastModificationTime >= " + startLastModificationTime + " and lastModificationTime <= " + endLastModificationTime;

			printSql(sql);

			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			if( rs.next() ){

				edmsDocumentsCount = rs.getLong( "cnt" );
			}


		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}

			try{
				if (connection != null)
				{
					DBMR.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}

		return edmsDocumentsCount;
	}
	
	public long update(CommonDTO commonDTO) throws Exception
	{		
		Edms_documentsDTO edms_documentsDTO = (Edms_documentsDTO)commonDTO;
		
		Connection connection = null;
		PreparedStatement ps = null;

		long lastModificationTime = System.currentTimeMillis();	
		try{
			connection = DBMW.getInstance().getConnection();

			String sql = "UPDATE " + tableName;
			
			sql += " SET ";
			sql += "document_process_cat=?";
			sql += ", ";
			sql += "document_no=?";
			sql += ", ";
			sql += "pbrlp_contractor_type=?";
			sql += ", ";
			sql += "other_contractor=?";
			sql += ", ";
			sql += "file_index_type_type=?";
			sql += ", ";
			sql += "file_index_subtype_type=?";
			sql += ", ";
			sql += "offices_type=?";
			sql += ", ";
			sql += "document_type_cat=?";
			sql += ", ";
			sql += "document_date=?";
			sql += ", ";
			sql += "document_receive_date=?";
			sql += ", ";
			sql += "document_subject=?";

			sql += ", ";
			sql += "reference_no=?";
			sql += ", ";
			sql += "reference_no_part1=?";
			sql += ", ";
			sql += "reference_no_part2=?";
			sql += ", ";
			sql += "reference_no_part3=?";
			sql += ", ";
			sql += "reference_no_part4=?";
			sql += ", ";
			sql += "reference_no_part5=?";
			sql += ", ";
			sql += "reference_no_part6=?";


			sql += ", ";
			sql += "description=?";
			sql += ", ";
			sql += "files_dropzone=?";
			sql += ", ";
			sql += "is_confidential=?";
			sql += ", ";
			sql += "doc_confidential_date=?";
			sql += ", ";
			sql += "is_locked=?";
			sql += ", ";
			sql += "doc_locked_date=?";
			sql += ", ";
			sql += "parent_documents_id=?";
			sql += ", ";
			sql += "user_id=?";

			sql += ", ";
			sql += "section=?";
			sql += ", ";
			sql += "rfi_status=?";
			sql += ", ";
			sql += "rfi_status_other=?";
			sql += ", ";
			sql += "rfi_type=?";
			sql += ", ";
			sql += "rfi_distribution=?";
			sql += ", ";
			sql += "crec_no=?";
			sql += ", ";
			sql += "ncr_submission_date=?";
			sql += ", ";
			sql += "open_reason=?";

			sql += ", ";
			sql += "job_cat=?";
			sql += ", lastModificationTime = "	+ lastModificationTime + "";
            sql += " WHERE ID = " + edms_documentsDTO.iD;
				
			ps = connection.prepareStatement(sql);

			int index = 1;
			
			ps.setObject(index++,edms_documentsDTO.documentProcessCat);
			ps.setObject(index++,edms_documentsDTO.documentNo);
			ps.setObject(index++,edms_documentsDTO.pbrlpContractorType);
			ps.setString(index++,edms_documentsDTO.otherContractor);
			ps.setObject(index++,edms_documentsDTO.fileIndexTypeType);
			ps.setObject(index++,edms_documentsDTO.fileIndexSubtypeType);
			ps.setObject(index++,edms_documentsDTO.officesType);
			ps.setObject(index++,edms_documentsDTO.documentTypeCat);
			ps.setObject(index++,edms_documentsDTO.documentDate);
			ps.setObject(index++,edms_documentsDTO.documentReceiveDate);
			ps.setObject(index++,edms_documentsDTO.documentSubject);

			ps.setString(index++,edms_documentsDTO.referenceNo);
			ps.setString(index++,edms_documentsDTO.refPart1);
			ps.setString(index++,edms_documentsDTO.refPart2);
			ps.setString(index++,edms_documentsDTO.refPart3);
			ps.setString(index++,edms_documentsDTO.refPart4);
			ps.setString(index++,edms_documentsDTO.refPart5);
			ps.setString(index++,edms_documentsDTO.refPart6);

			ps.setObject(index++,edms_documentsDTO.description);
			ps.setObject(index++,edms_documentsDTO.filesDropzone);
			ps.setObject(index++,edms_documentsDTO.isConfidential);
			ps.setObject(index++,edms_documentsDTO.docConfidentialDate);
			ps.setObject(index++,edms_documentsDTO.isLocked);
			ps.setObject(index++,edms_documentsDTO.docLockedDate);
			ps.setObject(index++,edms_documentsDTO.parentDocumentsId);
			ps.setObject(index++,edms_documentsDTO.userId);

			ps.setInt(index++, edms_documentsDTO.section);
			ps.setInt(index++, edms_documentsDTO.rfiStatus);
			ps.setString(index++, edms_documentsDTO.rfiStatusOther);
			ps.setInt(index++,edms_documentsDTO.rfiType);
			ps.setInt(index++,edms_documentsDTO.rfiDistribution);
			ps.setString(index++, edms_documentsDTO.crecNo );
			ps.setLong(index++,edms_documentsDTO.ncrSubmissionDate);
			ps.setString(index++,edms_documentsDTO.openReason);

			ps.setObject(index++,edms_documentsDTO.jobCat);
			System.out.println(ps);
			ps.executeUpdate();
			


			
			recordUpdateTime(connection, lastModificationTime);

						
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if (ps != null) {
					ps.close();
				}
			} catch(Exception e){}
			try{
				if(connection != null)
				{
					DBMW.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		return edms_documentsDTO.iD;
	}
	
	
	public List<Edms_documentsDTO> getDTOs(Collection recordIDs){
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Edms_documentsDTO edms_documentsDTO = null;
		List<Edms_documentsDTO> edms_documentsDTOList = new ArrayList<>();
		if(recordIDs.isEmpty()){
			return edms_documentsDTOList;
		}
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
            
            sql += " WHERE ID IN ( ";

			for(int i = 0;i<recordIDs.size();i++){
				if(i!=0){
					sql+=",";
				}
				sql+=((ArrayList)recordIDs).get(i);
			}
			sql+=")  order by lastModificationTime desc";
			
			printSql(sql);
			
			logger.debug("sql " + sql);
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				edms_documentsDTO = new Edms_documentsDTO();
				edms_documentsDTO.iD = rs.getLong("ID");
				edms_documentsDTO.documentProcessCat = rs.getInt("document_process_cat");
				edms_documentsDTO.documentNo = rs.getString("document_no");
				edms_documentsDTO.pbrlpContractorType = rs.getLong("pbrlp_contractor_type");
				edms_documentsDTO.otherContractor = rs.getString( "other_contractor" );
				edms_documentsDTO.fileIndexTypeType = rs.getInt("file_index_type_type");
				edms_documentsDTO.fileIndexSubtypeType = rs.getInt("file_index_subtype_type");
				edms_documentsDTO.officesType = rs.getLong("offices_type");
				edms_documentsDTO.documentTypeCat = rs.getInt("document_type_cat");
				edms_documentsDTO.documentDate = rs.getLong("document_date");
				edms_documentsDTO.documentReceiveDate = rs.getLong("document_receive_date");
				edms_documentsDTO.documentSubject = rs.getString("document_subject");

				edms_documentsDTO.referenceNo = rs.getString("reference_no");
				edms_documentsDTO.refPart1 = rs.getString( "reference_no_part1" );
				edms_documentsDTO.refPart2 = rs.getString( "reference_no_part2" );
				edms_documentsDTO.refPart3 = rs.getString( "reference_no_part3" );
				edms_documentsDTO.refPart4 = rs.getString( "reference_no_part4" );
				edms_documentsDTO.refPart5 = rs.getString( "reference_no_part5" );
				edms_documentsDTO.refPart6 = rs.getString( "reference_no_part6" );

				edms_documentsDTO.description = rs.getString("description");
				edms_documentsDTO.filesDropzone = rs.getLong("files_dropzone");
				edms_documentsDTO.isConfidential = rs.getBoolean("is_confidential");
				edms_documentsDTO.docConfidentialDate = rs.getLong("doc_confidential_date");
				edms_documentsDTO.isLocked = rs.getBoolean("is_locked");
				edms_documentsDTO.docLockedDate = rs.getLong("doc_locked_date");
				edms_documentsDTO.parentDocumentsId = rs.getLong("parent_documents_id");
				edms_documentsDTO.userId = rs.getLong("user_id");
				edms_documentsDTO.jobCat = rs.getInt("job_cat");
				edms_documentsDTO.deadline = rs.getLong( "deadline" );
				edms_documentsDTO.isDeleted = rs.getInt("isDeleted");

				edms_documentsDTO.section = rs.getInt("section");
				edms_documentsDTO.rfiStatus = rs.getInt("rfi_status");
				edms_documentsDTO.rfiStatusOther = rs.getString("rfi_status_other");
				edms_documentsDTO.rfiType = rs.getInt( "rfi_type" );
				edms_documentsDTO.rfiDistribution = rs.getInt( "rfi_distribution" );
				edms_documentsDTO.crecNo = rs.getString( "crec_no" );
				edms_documentsDTO.ncrSubmissionDate = rs.getLong( "ncr_submission_date" );
				edms_documentsDTO.openReason = rs.getString( "open_reason" );

				edms_documentsDTO.lastModificationTime = rs.getLong("lastModificationTime");
				System.out.println("got this DTO: " + edms_documentsDTO);
				
				edms_documentsDTOList.add(edms_documentsDTO);

			}			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return edms_documentsDTOList;
	
	}
	
	

	
	
	
	//add repository
	public List<Edms_documentsDTO> getAllEdms_documents (boolean isFirstReload)
    {
		List<Edms_documentsDTO> edms_documentsDTOList = new ArrayList<>();

		String sql = "SELECT * FROM edms_documents";
		sql += " WHERE ";
	

		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by edms_documents.lastModificationTime desc";
		printSql(sql);
		
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;

		
		try{
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();
			rs = stmt.executeQuery(sql);
			

			while(rs.next()){
				Edms_documentsDTO edms_documentsDTO = new Edms_documentsDTO();
				edms_documentsDTO.iD = rs.getLong("ID");
				edms_documentsDTO.documentProcessCat = rs.getInt("document_process_cat");
				edms_documentsDTO.documentNo = rs.getString("document_no");
				edms_documentsDTO.pbrlpContractorType = rs.getLong("pbrlp_contractor_type");
				edms_documentsDTO.otherContractor = rs.getString( "other_contractor" );
				edms_documentsDTO.fileIndexTypeType = rs.getInt("file_index_type_type");
				edms_documentsDTO.fileIndexSubtypeType = rs.getInt("file_index_subtype_type");
				edms_documentsDTO.officesType = rs.getLong("offices_type");
				edms_documentsDTO.documentTypeCat = rs.getInt("document_type_cat");
				edms_documentsDTO.documentDate = rs.getLong("document_date");
				edms_documentsDTO.documentReceiveDate = rs.getLong("document_receive_date");
				edms_documentsDTO.documentSubject = rs.getString("document_subject");

				edms_documentsDTO.referenceNo = rs.getString("reference_no");
				edms_documentsDTO.refPart1 = rs.getString( "reference_no_part1" );
				edms_documentsDTO.refPart2 = rs.getString( "reference_no_part2" );
				edms_documentsDTO.refPart3 = rs.getString( "reference_no_part3" );
				edms_documentsDTO.refPart4 = rs.getString( "reference_no_part4" );
				edms_documentsDTO.refPart5 = rs.getString( "reference_no_part5" );
				edms_documentsDTO.refPart6 = rs.getString( "reference_no_part6" );

				edms_documentsDTO.description = rs.getString("description");
				edms_documentsDTO.filesDropzone = rs.getLong("files_dropzone");
				edms_documentsDTO.isConfidential = rs.getBoolean("is_confidential");
				edms_documentsDTO.docConfidentialDate = rs.getLong("doc_confidential_date");
				edms_documentsDTO.isLocked = rs.getBoolean("is_locked");
				edms_documentsDTO.docLockedDate = rs.getLong("doc_locked_date");
				edms_documentsDTO.parentDocumentsId = rs.getLong("parent_documents_id");
				edms_documentsDTO.userId = rs.getLong("user_id");
				edms_documentsDTO.jobCat = rs.getInt("job_cat");

				edms_documentsDTO.section = rs.getInt("section");
				edms_documentsDTO.rfiStatus = rs.getInt("rfi_status");
				edms_documentsDTO.rfiStatusOther = rs.getString("rfi_status_other");
				edms_documentsDTO.rfiType = rs.getInt( "rfi_type" );
				edms_documentsDTO.rfiDistribution = rs.getInt( "rfi_distribution" );
				edms_documentsDTO.crecNo = rs.getString( "crec_no" );
				edms_documentsDTO.ncrSubmissionDate = rs.getLong( "ncr_submission_date" );
				edms_documentsDTO.openReason = rs.getString( "open_reason" );

				edms_documentsDTO.deadline = rs.getLong( "deadline" );
				edms_documentsDTO.isDeleted = rs.getInt("isDeleted");
				edms_documentsDTO.lastModificationTime = rs.getLong("lastModificationTime");
				
				edms_documentsDTOList.add(edms_documentsDTO);
			}			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}

		return edms_documentsDTOList;
    }

	
	public List<Edms_documentsDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO)
	{
		return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
	}

	
	public List<Edms_documentsDTO> getDTOs(Hashtable  p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
			String filter, boolean tableHasJobCat)
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		List<Edms_documentsDTO> edms_documentsDTOList = new ArrayList<>();

		try{
			
			String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat);
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				Edms_documentsDTO edms_documentsDTO = new Edms_documentsDTO();
				edms_documentsDTO.iD = rs.getLong("ID");
				edms_documentsDTO.documentProcessCat = rs.getInt("document_process_cat");
				edms_documentsDTO.documentNo = rs.getString("document_no");
				edms_documentsDTO.pbrlpContractorType = rs.getLong("pbrlp_contractor_type");
				edms_documentsDTO.otherContractor = rs.getString( "other_contractor" );
				edms_documentsDTO.fileIndexTypeType = rs.getInt("file_index_type_type");
				edms_documentsDTO.fileIndexSubtypeType = rs.getInt("file_index_subtype_type");
				edms_documentsDTO.officesType = rs.getLong("offices_type");
				edms_documentsDTO.documentTypeCat = rs.getInt("document_type_cat");
				edms_documentsDTO.documentDate = rs.getLong("document_date");
				edms_documentsDTO.documentReceiveDate = rs.getLong("document_receive_date");
				edms_documentsDTO.documentSubject = rs.getString("document_subject");

				edms_documentsDTO.referenceNo = rs.getString("reference_no");
				edms_documentsDTO.refPart1 = rs.getString( "reference_no_part1" );
				edms_documentsDTO.refPart2 = rs.getString( "reference_no_part2" );
				edms_documentsDTO.refPart3 = rs.getString( "reference_no_part3" );
				edms_documentsDTO.refPart4 = rs.getString( "reference_no_part4" );
				edms_documentsDTO.refPart5 = rs.getString( "reference_no_part5" );
				edms_documentsDTO.refPart6 = rs.getString( "reference_no_part6" );

				edms_documentsDTO.description = rs.getString("description");
				edms_documentsDTO.filesDropzone = rs.getLong("files_dropzone");
				edms_documentsDTO.isConfidential = rs.getBoolean("is_confidential");
				edms_documentsDTO.docConfidentialDate = rs.getLong("doc_confidential_date");
				edms_documentsDTO.isLocked = rs.getBoolean("is_locked");
				edms_documentsDTO.docLockedDate = rs.getLong("doc_locked_date");
				edms_documentsDTO.parentDocumentsId = rs.getLong("parent_documents_id");
				edms_documentsDTO.userId = rs.getLong("user_id");

				edms_documentsDTO.section = rs.getInt("section");
				edms_documentsDTO.rfiStatus = rs.getInt("rfi_status");
				edms_documentsDTO.rfiStatusOther = rs.getString("rfi_status_other");
				edms_documentsDTO.rfiType = rs.getInt( "rfi_type" );
				edms_documentsDTO.rfiDistribution = rs.getInt( "rfi_distribution" );
				edms_documentsDTO.crecNo = rs.getString( "crec_no" );
				edms_documentsDTO.ncrSubmissionDate = rs.getLong( "ncr_submission_date" );
				edms_documentsDTO.openReason = rs.getString( "open_reason" );

				edms_documentsDTO.jobCat = rs.getInt("job_cat");
				edms_documentsDTO.isDeleted = rs.getInt("isDeleted");
				edms_documentsDTO.lastModificationTime = rs.getLong("lastModificationTime");
				
				
				edms_documentsDTOList.add(edms_documentsDTO);
			}					
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return edms_documentsDTOList;
	
	}
	
	public void updateDeadline( Long ID, Long deadline ) {

		Connection connection = null;
		Statement stmt = null;
		PreparedStatement ps = null;

		long lastModificationTime = System.currentTimeMillis();

		try{
			String sql = "UPDATE " + tableName;

			sql += " SET deadline= " + deadline + ",lastModificationTime="+ lastModificationTime +" WHERE ID = " + ID;

			printSql(sql);

			connection = DBMW.getInstance().getConnection();
			stmt  = connection.createStatement();
			stmt.execute(sql);

		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}

			try{
				if (connection != null){
					DBMW.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
	}

	
	public String getSqlWithSearchCriteriaForNormalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
			UserDTO userDTO, String filter, boolean tableHasJobCat)
    {
		String sql = "SELECT ";
		if(category == GETIDS)
		{
			sql += " " + tableName + ".ID as ID ";
		}
		else if(category == GETCOUNT)
		{
			sql += " count( " + tableName + ".ID) as countID ";
		}
		else if(category == GETDTOS)
		{
			sql += " distinct " + tableName + ".* ";
		}
		sql += "FROM " + tableName + " ";
		sql += joinSQL;
		

		
		
		String AnyfieldSql = "";
		String AllFieldSql = "";
		
		if(p_searchCriteria != null)
		{
						
			
			Enumeration names = p_searchCriteria.keys();
			String str, value;
			
			AnyfieldSql = "(";
			
			if(p_searchCriteria.get("AnyField")!= null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase(""))
			{
				int i = 0;
				Iterator it = (commonMaps).java_anyfield_search_map.entrySet().iterator();
				while(it.hasNext())
				{
					if( i > 0)
		        	{
						AnyfieldSql+= " OR  ";
		        	}
					Entry pair = (Entry)it.next();
					AnyfieldSql+= pair.getKey() + " like '%" + p_searchCriteria.get("AnyField").toString() + "%'";
					i ++;
				}						
			}
			AnyfieldSql += ")";
			System.out.println("AnyfieldSql = " + AnyfieldSql);
			
			AllFieldSql = "(";
			int i = 0;
			Boolean firstCond = true;
			while(names.hasMoreElements())
			{				
				str = (String) names.nextElement();
				value = (String)p_searchCriteria.get(str);
		        System.out.println(str + ": " + value);
				if((commonMaps).java_allfield_type_map.get(str.toLowerCase()) != null &&  !(commonMaps).java_allfield_type_map.get(str.toLowerCase()).equalsIgnoreCase("")
						&& !str.equalsIgnoreCase("AnyField")
		        		&&
						( ( value != null && !value.equalsIgnoreCase("") ) || commonMaps.rangeMap.get( str.toLowerCase() ) != null ) )
				{
					if(p_searchCriteria.get(str).equals("any"))
					{
						continue;
					}

                    String dataType = (commonMaps).java_allfield_type_map.get(str.toLowerCase());

                    if( i > 0 && !dataType.equals("date") )
					{
						AllFieldSql+= " AND  ";
					}

		        	if( dataType.equals("String") )
					{
						AllFieldSql += "" + tableName + "." + str.toLowerCase() + " like '%" + p_searchCriteria.get(str) + "%'";
						firstCond = false;
					}
					else
					{
		        		if( dataType.equals("date") ){

		        			Object valFrom = p_searchCriteria.get( str + "_from" );
							Object valTo = p_searchCriteria.get( str + "_to" );
							Boolean valFromSet = false;
							Boolean valToSet = false;

							String dateFormat = commonMaps.dateFormat.get(str.toLowerCase());
							try{

								if( !StringUtils.isBlank( (String) valFrom ) ) {
									valFrom = new SimpleDateFormat(dateFormat).parse((String) valFrom).getTime();
									valFromSet = true;
								}

								if( !StringUtils.isBlank( (String) valTo ) ) {
									valTo = new SimpleDateFormat(dateFormat).parse((String) valTo).getTime();
									valToSet = true;
								}
							}
							catch ( Exception e ){

								System.out.println( "Can't parse date dateformat " + dateFormat + " and value is " + valFrom + " and " + valTo );
								continue;
							}

		        			List<CommonMaps.Range> rangeList = commonMaps.rangeMap.get( str.toLowerCase() );
							for( int k=0; k<rangeList.size(); k++){

								CommonMaps.Range range = rangeList.get(k);
								if( range == CommonMaps.Range.FROM && valFromSet ){

									if( !firstCond )
										AllFieldSql += " and ";

									AllFieldSql += tableName + "." + str.toLowerCase() + " >= " + valFrom;
								}
								else if( range == CommonMaps.Range.TO && valToSet ){

									if( !firstCond )
										AllFieldSql += " and ";
									
									AllFieldSql += " and " + tableName + "." + str.toLowerCase() + "<=" + valTo;
								}
							}

							continue;
						}

						AllFieldSql += "" + tableName + "." + str.toLowerCase() + " = '" + p_searchCriteria.get(str) + "'";
						firstCond = false;
					}
					i ++;
				}
			}
			
			AllFieldSql += ")";
			System.out.println("AllFieldSql = " + AllFieldSql);
		}
		
		sql += " WHERE ";
		
		sql += " (" + tableName + ".isDeleted = 0 ";			
		if(userDTO.roleID != SessionConstants.ADMIN_ROLE && userDTO.unitID != 78040L )
		{
			sql += " and " +  tableName + ".offices_type = " + userDTO.unitID;
		}
		sql += ")";
		
		
		if(!filter.equalsIgnoreCase(""))
		{
			sql += " and " + filter + " ";
		}
		if(!AnyfieldSql.equals("()") && !AnyfieldSql.equals(""))
		{
			sql += " AND " + AnyfieldSql;
			
		}
		if(!AllFieldSql.equals("()") && !AllFieldSql.equals(""))
		{			
			sql += " AND " + AllFieldSql;
		}
		
		sql += " order by " + tableName + ".lastModificationTime desc ";

		printSql(sql);
		
		if(limit >= 0)
		{
			sql += " limit " + limit;
		}
		if(offset >= 0)
		{
			sql += " offset " + offset;
		}
		
		//System.out.println("-------------- sql = " + sql);
		
		return sql;
    }
	
	public String getSqlWithSearchCriteriaForApprovalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
			UserDTO userDTO, String filter, boolean tableHasJobCat)
	{
		
		boolean viewAll = false;
		
		if(p_searchCriteria != null && p_searchCriteria.get("ViewAll") != null)
		{
			System.out.println("ViewAll = " + p_searchCriteria.get("ViewAll"));
			viewAll = true;
		}
		else
		{
			////System.out.println("ViewAll is null ");
		}
		
		String sql = "SELECT ";
		if(category == GETIDS)
		{
			sql += " " + tableName + ".ID as ID ";
		}
		else if(category == GETCOUNT)
		{
			sql += " count( " + tableName + ".ID) as countID ";
		}
		else if(category == GETDTOS)
		{
			sql += " distinct " + tableName + ".* ";
		}
		sql += "FROM " + tableName + " ";

		
		sql += " join approval_execution_table as aet on ("
				+ tableName + ".id = aet.updated_row_id "
				+ ")";
		
		sql += " join approval_summary on ("
				+ "aet.previous_row_id = approval_summary.table_id and  approval_summary.table_name = '" + tableName + "'"
				+ ")";
		
		if(!viewAll)
		{
																	
			sql += " left join approval_path_details on ("
					+ "aet.approval_path_id = approval_path_details.approval_path_id "
					+ "and aet.approval_path_order = approval_path_details.approval_order "
					+ ")";
		}
									
		
	

		

		String AllFieldSql = "";
		
	
		
		if(p_searchCriteria != null)
		{
						
			
			Enumeration names = p_searchCriteria.keys();
			String str, value;
			
			
			
			AllFieldSql = "(";
			int i = 0;
			while(names.hasMoreElements())
			{				
				str = (String) names.nextElement();
				value = (String)p_searchCriteria.get(str);
		        System.out.println(str + ": " + value);
		        if(approvalMaps.java_allfield_type_map.get(str.toLowerCase()) != null &&  !approvalMaps.java_allfield_type_map.get(str.toLowerCase()).equalsIgnoreCase("")
		        		&& !str.equalsIgnoreCase("AnyField")
		        		&&
						( ( value != null && !value.equalsIgnoreCase("") ) || commonMaps.rangeMap.get( str.toLowerCase() ) != null ) )
		        {
					if(p_searchCriteria.get(str).equals("any"))
		        	{
		        		continue;
		        	}
					
		        	if( i > 0)
		        	{
		        		AllFieldSql+= " AND  ";
		        	}

		        	String dataType = approvalMaps.java_allfield_type_map.get(str.toLowerCase()); 
		        	
		        	String fromTable = tableName;
		        	if(approvalMaps.java_table_map.get(str) != null)
		    		{
		    			fromTable = approvalMaps.java_table_map.get(str);
		    		}
		    		
		        	if(str.equalsIgnoreCase("starting_date") || str.equalsIgnoreCase("ending_date"))
		        	{
		        		String string_date = (String) p_searchCriteria.get(str);
		        		long milliseconds = 0;

		        		SimpleDateFormat f = new SimpleDateFormat("dd-MM-yyyy");
		        		try {
		        		    Date d = f.parse(string_date);
		        		    milliseconds = d.getTime();
		        		} catch (Exception e) {
		        		    e.printStackTrace();
		        		}
		        		if(str.equalsIgnoreCase("starting_date"))
		        		{
		        			AllFieldSql += "approval_summary.date_of_initiation >= " +  milliseconds;
		        		}
		        		else
		        		{
		        			AllFieldSql += "approval_summary.date_of_initiation <= " +  milliseconds;
		        		}
		        	}
		        	else
		        	{
		        		if( dataType.equals("String") )
			        	{
			        		AllFieldSql += "" + fromTable + "." + str.toLowerCase() + " like '%" + p_searchCriteria.get(str) + "%'";
			        	}
			        	else if( dataType.equals("Integer") || dataType.equals("Long"))
			        	{
			        		AllFieldSql += "" + fromTable + "." + str.toLowerCase() + " = " + p_searchCriteria.get(str) ;
			        	}
		        	}
		        	i ++;
		        }
			}
			
			AllFieldSql += ")";
			System.out.println("AllFieldSql = " + AllFieldSql);
			
			
		}
		sql += " WHERE ";
		
		if(viewAll)
		{
			sql += " (" + tableName + ".isDeleted != " + CommonDTO.DELETED + " and " + tableName + ".isDeleted != " + CommonDTO.OUTDATED
					+ ")";
		}
		else
		{
			sql += " (" + tableName + ".isDeleted != " + CommonDTO.DELETED + " and " + tableName + ".isDeleted != " + CommonDTO.OUTDATED
				+ " and (approval_path_details.organogram_id = " + userDTO.organogramID 
						+ " or " +  userDTO.organogramID + " in ("
						+ " select organogram_id from approval_execution_table where previous_row_id = aet.previous_row_id"
						+ "))"
				+ ")";
		}
		
		
		if(!filter.equalsIgnoreCase(""))
		{
			sql += " and " + filter + " ";
		}

		if(!AllFieldSql.equals("()") && !AllFieldSql.equals(""))
		{			
			sql += " AND " + AllFieldSql;
		}
		
	
			
		sql += "  order by " + tableName + ".lastModificationTime desc ";

		printSql(sql);
		
		if(limit >= 0)
		{
			sql += " limit " + limit;
		}
		if(offset >= 0)
		{
			sql += " offset " + offset;
		}
		
		//System.out.println("-------------- sql = " + sql);
		
		return sql;
	}
	
	
	
	
	public String getSqlWithSearchCriteria(Hashtable p_searchCriteria, int limit, int offset, int category,
			boolean isPermanentTable, UserDTO userDTO, String filter, boolean tableHasJobCat)
    {
		if(isPermanentTable)
		{
			return getSqlWithSearchCriteriaForNormalSearch(p_searchCriteria, limit, offset, category, userDTO, filter, tableHasJobCat);
		}
		else
		{
			return getSqlWithSearchCriteriaForApprovalSearch(p_searchCriteria, limit, offset, category, userDTO, filter, tableHasJobCat);
		}
				
    }
				
}
	