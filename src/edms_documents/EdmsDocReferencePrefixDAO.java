package edms_documents;

import util.CommonConstant;

import java.util.ArrayList;
import java.util.List;

public class EdmsDocReferencePrefixDAO {

    private static final EdmsDocReferencePrefixDAO edmsDocReferencePrefixDAO = new EdmsDocReferencePrefixDAO();

    private final List<EdmsDocReferencePrefixDTO> edmsDocReferencePrefixDTOList = new ArrayList<>();

    public static EdmsDocReferencePrefixDAO getInstance(){

        return edmsDocReferencePrefixDAO;
    }

    private EdmsDocReferencePrefixDAO(){

        EdmsDocReferencePrefixDTO inLetCrec = EdmsDocReferencePrefixDTO
                .builder()
                .docProcess( CommonConstant.DOC_INCOMING )
                .docSource( CommonConstant.DOC_SOURCE_CREC )
                .docType( CommonConstant.DOC_TYPE_LETTER )

                .part1( "BD/PDMBRP" )

                .labelPart1( "ref1" )
                .labelPart2( "To" )
                .labelPart3( "Author" )
                .labelPart4( "Department" )
                .labelPart5( "Year" )
                .labelPart6( "Ref. No." )

                .placeHolderPart1( "BD/PDMBRP" )
                .placeHolderPart2( "CREC-BR" )
                .placeHolderPart3( "TM" )
                .placeHolderPart4( "OU" )
                .placeHolderPart5( "2016" )
                .placeHolderPart6( "3001" )

                .optionsPart2( new String[]{"CREC-BR", "CREC-CSC", "DI-CSC", "DII-CSC", "DIII-CSC", "DVI-CSC"} )
                .optionsPart5( new String[]{ "2016","2017","2018","2019","2020","2021","2022","2023","2024","2025","2026","2027" } )

                .hideDiv( new String[]{"sectionDiv", "sectionDivForReference", "crecNoDiv", "rfiTypeDiv", "rfiDistributionDiv"} )
                .showDiv( new String[]{"rfiStatusDiv" } )
                .selectOptions( EdmsDocReferencePrefixDTO.OPEN_CLOSE_STATUS )
                .build();

        EdmsDocReferencePrefixDTO inLetBr = EdmsDocReferencePrefixDTO
                .builder()
                .docProcess( CommonConstant.DOC_INCOMING )
                .docSource( CommonConstant.DOC_SOURCE_BR )
                .docType( CommonConstant.DOC_TYPE_LETTER )
                .part1( "54/01/0000" )

                .labelPart1( "ref1" )
                .labelPart2( "ref2" )
                .labelPart3( "ref3" )
                .labelPart4( "ref4" )
                .labelPart5( "ref5" )
                .labelPart6( "ref6" )

                .placeHolderPart1( "54.01.0000" )
                .placeHolderPart2( "631" )
                .placeHolderPart3( "39" )
                .placeHolderPart4( "398" )
                .placeHolderPart5( "19" )
                .placeHolderPart6( "4782" )

                .hideDiv( new String[]{"sectionDiv", "sectionDivForReference", "crecNoDiv", "rfiTypeDiv", "rfiDistributionDiv"} )
                .showDiv( new String[]{"rfiStatusDiv" } )
                .selectOptions( EdmsDocReferencePrefixDTO.OPEN_CLOSE_STATUS )
                .build();

        EdmsDocReferencePrefixDTO inLetBuet = EdmsDocReferencePrefixDTO
                .builder()
                .docProcess( CommonConstant.DOC_INCOMING )
                .docSource( CommonConstant.DOC_SOURCE_BUET )
                .docType( CommonConstant.DOC_TYPE_LETTER )
                .part1( "CEB" )

                .labelPart1( "ref1" )
                .labelPart2( "ref2" )
                .labelPart3( "ref3" )

                .placeHolderPart1( "CEB" )
                .placeHolderPart2( "4122" )
                .placeHolderPart3( "14941" )

                .hideDiv( new String[]{"sectionDiv", "sectionDivForReference", "crecNoDiv", "rfiTypeDiv", "rfiDistributionDiv"} )
                .showDiv( new String[]{"rfiStatusDiv" } )
                .selectOptions( EdmsDocReferencePrefixDTO.OPEN_CLOSE_STATUS )
                .build();

        EdmsDocReferencePrefixDTO inRfiBR = EdmsDocReferencePrefixDTO
                .builder()
                .docProcess( CommonConstant.DOC_INCOMING )
                .docSource( CommonConstant.DOC_SOURCE_BR )
                .docType( CommonConstant.DOC_TYPE_RFI )
                .showDiv( new String[]{"rfiTypeDiv", "rfiDistributionDiv"} )
                .build();

        EdmsDocReferencePrefixDTO inRfiBuet = EdmsDocReferencePrefixDTO
                .builder()
                .docProcess( CommonConstant.DOC_INCOMING )
                .docSource( CommonConstant.DOC_SOURCE_BUET )
                .docType( CommonConstant.DOC_TYPE_RFI )
                .showDiv( new String[]{"rfiTypeDiv", "rfiDistributionDiv"} )
                .build();

        EdmsDocReferencePrefixDTO inRfiOther = EdmsDocReferencePrefixDTO
                .builder()
                .docProcess( CommonConstant.DOC_INCOMING )
                .docSource( CommonConstant.DOC_SOURCE_OTHER )
                .docType( CommonConstant.DOC_TYPE_RFI )
                .showDiv( new String[]{"rfiTypeDiv", "rfiDistributionDiv"} )
                .build();

        EdmsDocReferencePrefixDTO inRfiCrec = EdmsDocReferencePrefixDTO
                .builder()
                .docProcess( CommonConstant.DOC_INCOMING )
                .docSource( CommonConstant.DOC_SOURCE_CREC )
                .docType( CommonConstant.DOC_TYPE_RFI )

                .part1( "BD/PDMBRP" )
                .part4( "RFI" )

                .labelPart1( "ref1" )
                .labelPart2( "From" )
                .labelPart3( "To" )
                .labelPart4( "ref4" )
                .labelPart5( "Year" )
                .labelPart6( "Ref. No." )

                .placeHolderPart1( "BD/PDMBRP" )
                .placeHolderPart2( "D1U1" )
                .placeHolderPart3( "D2G" )
                .placeHolderPart4( "RFI" )
                .placeHolderPart5( "2016" )
                .placeHolderPart6( "20368" )

                .optionsPart2( new String[]{ "D1U1", "D1U2", "D1U3", "D2U1", "D2U2", "D3U1", "D3U2", "D6U1", "D6U2"})
                .optionsPart3( new String[]{ "D2G", "G2M", "M2B", "B2J" } )
                .optionsPart5( new String[]{ "2016","2017","2018","2019","2020","2021","2022","2023","2024","2025","2026","2027" } )

                .hideDiv( new String[]{"sectionDiv", "sectionDivForReference", "crecNoDiv"} )
                .showDiv( new String[]{"rfiStatusDiv", "rfiTypeDiv", "rfiDistributionDiv"} )
                .selectOptions( EdmsDocReferencePrefixDTO.DEFAULT_STATUS )
                .build();

        EdmsDocReferencePrefixDTO outLet = EdmsDocReferencePrefixDTO
                .builder()
                .docProcess( CommonConstant.DOC_OUTGOING )
                .docType( CommonConstant.DOC_TYPE_LETTER )

                .part1( "5060127" )
                .part2( CommonConstant.NAME_INITIAL )
                .part3( CommonConstant.FILE_TAG )

                .labelPart1( "CSC" )
                .labelPart2( "Author" )
                .labelPart3( "Index.Subindex" )
                .labelPart4( "Ref No." )

                .placeHolderPart1( "5060127" )
                .placeHolderPart2( "GR" )
                .placeHolderPart3( "18.04" )
                .placeHolderPart4( "32" )

                .hideDiv( new String[]{"sectionDiv", "sectionDivForReference", "crecNoDiv", "rfiTypeDiv", "rfiDistributionDiv"} )
                .showDiv( new String[]{"rfiStatusDiv"} )
                .selectOptions( EdmsDocReferencePrefixDTO.APPROVED_FOR_SUBMISSION_AND_SENT_STATUS )
                .build();

        EdmsDocReferencePrefixDTO outQsdr = EdmsDocReferencePrefixDTO
                .builder()
                .docProcess( CommonConstant.DOC_OUTGOING )
                .docType( CommonConstant.DOC_TYPE_QSDR )

                .part1( "5060127" )
                .part2( CommonConstant.INPUT_ID_SECTION )

                .labelPart1( "ref1" )
                .labelPart2( "Section" )
                .labelPart3( "Division" )
                .labelPart4( "Unit" )
                .labelPart5( "Issuer" )
                .labelPart6( "Ref No." )

                .placeHolderPart1( "5060127" )
                .placeHolderPart2( "D2G" )
                .placeHolderPart3( "D1" )
                .placeHolderPart4( "U1" )
                .placeHolderPart5( "RE1 (M2B)" )
                .placeHolderPart6( "10" )

                .optionsPart2( new String[]{ "D2G", "G2M", "M2B", "B2J" } )
                .optionsPart3( new String[]{ "D1","D2","D3","D6" } )
                .optionsPart4( new String[]{ "U1","U2","U3" } )
                .optionsPart5( new String[]{ "RE1 (M2B)", "RE2 (D2G)", "RE2 (G2M)", "RE3 (B2J)" } )

                .hideDiv( new String[]{"rfiTypeDiv", "rfiDistributionDiv"})
                .showDiv( new String[]{"sectionDiv", "sectionDivForReference", "rfiStatusDiv", "crecNoDiv"} )
                .selectOptions( EdmsDocReferencePrefixDTO.OPEN_CLOSE_STATUS )
                .build();

        EdmsDocReferencePrefixDTO outHse = EdmsDocReferencePrefixDTO
                .builder()
                .docProcess( CommonConstant.DOC_OUTGOING )
                .docType( CommonConstant.DOC_TYPE_HSE )

                .part1( "5060127" )
                .part2( CommonConstant.INPUT_ID_SECTION )

                .labelPart1( "ref1" )
                .labelPart2( "Section" )
                .labelPart3( "Division" )
                .labelPart4( "Unit" )
                .labelPart5( "Issuer" )
                .labelPart6( "Ref No." )

                .placeHolderPart1( "5060127" )
                .placeHolderPart2( "D2G" )
                .placeHolderPart3( "D1" )
                .placeHolderPart4( "U1" )
                .placeHolderPart5( "RE1 (M2B)" )
                .placeHolderPart6( "21" )

                .optionsPart2( new String[]{ "D2G", "G2M", "M2B", "B2J" } )
                .optionsPart3( new String[]{ "D1","D2","D3","D6" } )
                .optionsPart4( new String[]{ "U1","U2","U3" } )
                .optionsPart5( new String[]{ "RE1 (M2B)", "RE2 (D2G)", "RE2 (G2M)", "RE3 (B2J)" } )

                .hideDiv( new String[]{"rfiTypeDiv", "rfiDistributionDiv"})
                .showDiv( new String[]{"sectionDiv", "sectionDivForReference", "rfiStatusDiv", "crecNoDiv"} )
                .selectOptions( EdmsDocReferencePrefixDTO.OPEN_CLOSE_STATUS )
                .build();

        EdmsDocReferencePrefixDTO outNcn = EdmsDocReferencePrefixDTO
                .builder()
                .docProcess( CommonConstant.DOC_OUTGOING )
                .docType( CommonConstant.DOC_TYPE_NCN )

                .part1( "5060127" )
                .part2( CommonConstant.INPUT_ID_SECTION )

                .labelPart1( "ref1" )
                .labelPart2( "Section" )
                .labelPart3( "Division" )
                .labelPart4( "Unit" )
                .labelPart5( "Issuer" )
                .labelPart6( "Ref No." )

                .placeHolderPart1( "5060127" )
                .placeHolderPart2( "D2G" )
                .placeHolderPart3( "D1" )
                .placeHolderPart4( "U1" )
                .placeHolderPart5( "RE1 (M2B)" )
                .placeHolderPart6( "11" )

                .optionsPart2( new String[]{ "D2G", "G2M", "M2B", "B2J" } )
                .optionsPart3( new String[]{ "D1","D2","D3","D6" } )
                .optionsPart4( new String[]{ "U1","U2","U3" } )
                .optionsPart5( new String[]{ "RE1 (M2B)", "RE2 (D2G)", "RE2 (G2M)", "RE3 (B2J)" } )

                .hideDiv( new String[]{"crecNoDiv","rfiTypeDiv", "rfiDistributionDiv"} )
                .showDiv( new String[]{"sectionDiv", "sectionDivForReference", "rfiStatusDiv" } )
                .selectOptions( EdmsDocReferencePrefixDTO.OPEN_CLOSE_STATUS )
                .build();

        EdmsDocReferencePrefixDTO outNcr = EdmsDocReferencePrefixDTO
                .builder()
                .docProcess( CommonConstant.DOC_OUTGOING )
                .docType( CommonConstant.DOC_TYPE_NCR )
                .hideDiv( new String[]{"sectionDiv", "sectionDivForReference", "crecNoDiv","rfiTypeDiv", "rfiDistributionDiv"} )
                .showDiv( new String[]{ "rfiStatusDiv" } )
                .selectOptions( EdmsDocReferencePrefixDTO.DEFAULT_STATUS )
                .build();

        EdmsDocReferencePrefixDTO outDrawing = EdmsDocReferencePrefixDTO
                .builder()
                .docProcess( CommonConstant.DOC_OUTGOING )
                .docType( CommonConstant.DOC_TYPE_DRAWING )
                .hideDiv( new String[]{"sectionDiv", "sectionDivForReference", "crecNoDiv","rfiTypeDiv", "rfiDistributionDiv"} )
                .showDiv( new String[]{ "rfiStatusDiv" } )
                .selectOptions( EdmsDocReferencePrefixDTO.DEFAULT_STATUS )
                .build();

        edmsDocReferencePrefixDTOList.add( inLetCrec );
        edmsDocReferencePrefixDTOList.add( inLetBr );
        edmsDocReferencePrefixDTOList.add( inLetBuet );
        edmsDocReferencePrefixDTOList.add( inRfiCrec );
        edmsDocReferencePrefixDTOList.add( inRfiBuet );
        edmsDocReferencePrefixDTOList.add( inRfiBR );
        edmsDocReferencePrefixDTOList.add( inRfiOther );
        edmsDocReferencePrefixDTOList.add( outLet );
        edmsDocReferencePrefixDTOList.add( outQsdr );
        edmsDocReferencePrefixDTOList.add( outHse );
        edmsDocReferencePrefixDTOList.add( outNcn );
        edmsDocReferencePrefixDTOList.add( outNcr );
        edmsDocReferencePrefixDTOList.add( outDrawing );
    }

    public List<EdmsDocReferencePrefixDTO> getAll(){

        return edmsDocReferencePrefixDTOList;
    }
}
