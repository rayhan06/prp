package edms_documents;

import util.CommonDTO;

import java.util.Date;

public class EdmsDocReferenceDocMapDTO extends CommonDTO{

    public Long edmsDocId;
    public Long referenceDocId;
    public Long docNumber;
    public String docSubject;
    public String docSource;
    public String docType;
    public String referenceNo;
    public String documentDate;
}
