package edms_documents;

import com.google.gson.Gson;
import dashboard.DashboardService;
import dbm.DBMW;

import files.FilesDAO;
import files.FilesDTO;
import inbox.InboxDAO;
import login.LoginDTO;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;

import pb.CatDAO;
import pb.Utils;
import permission.MenuConstants;
import role.PermissionRepository;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.*;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.*;



/**
 * Servlet implementation class Edms_documentsServlet
 */
@WebServlet("/Edms_documentsServlet")
@MultipartConfig
public class Edms_documentsServlet extends HttpServlet 
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Edms_documentsServlet.class);
    private final SimpleDateFormat sdf = new SimpleDateFormat( "dd/MM/yyyy" );

    String tableName = "edms_documents";

	Edms_documentsDAO edms_documentsDAO;
	CommonRequestHandler commonRequestHandler;
	FilesDAO filesDAO = new FilesDAO();
	DocumentChainageDAO documentChainageDAO;
	EdmsDocReferenceDocMapDAO edmsDocReferenceDocMapDAO;
	InboxDAO inboxDao = new InboxDAO();
    private final Gson gson = new Gson();
    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Edms_documentsServlet() 
	{
        super();
    	try
    	{
			edms_documentsDAO = new Edms_documentsDAO(tableName);
			documentChainageDAO = new DocumentChainageDAO("document_chainage");
			edmsDocReferenceDocMapDAO = new EdmsDocReferenceDocMapDAO();
			commonRequestHandler = new CommonRequestHandler(edms_documentsDAO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }   

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		System.out.println("In doget request = " + request);
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		Boolean developmentPartner = userDTO.roleID == CommonConstant.DEVELOPMENT_PARTNER_ROLE_ID;
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);

		if( userDTO.roleID == 9801 ){

			request.setAttribute( "PAGE_ACCESS", "DEVELOPMENT_PARTNER" );
			request.setAttribute( "DISPLAY_STYLE", "none" );
		}
		else{

			request.setAttribute( "PAGE_ACCESS", "FULL" );
			request.setAttribute( "DISPLAY_STYLE", "block" );
		}


		try
		{
			String actionType = request.getParameter("actionType");
			if(actionType.equals("getAddPage"))
			{
				//if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.EDMS_DOCUMENTS_ADD))
				{
					if( developmentPartner )
						get_addPageForPartner(request, response);
					else
						commonRequestHandler.getAddPage(request, response);
				}
				
			}
			if(actionType.equals("getAddPageForPartner"))
			{
				//if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.EDMS_DOCUMENTS_ADD))
				{
					get_addPageForPartner(request, response);
				}
				
			}
			else if(actionType.equals("getEditPage"))
			{
				//if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.EDMS_DOCUMENTS_UPDATE))
				{
					getEdms_documents(request, response);					
				}
									
			}
			else if(actionType.equals("getEditPageForPartner"))
			{
				//if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.EDMS_DOCUMENTS_UPDATE))
				{
					getEdms_documentsForPartner(request, response);
				}
				
			}
			else if(actionType.equals("previewDropzoneFile"))
			{
				long id = Long.parseLong(request.getParameter("id"));

				FilesDTO filesDTO = (FilesDTO) filesDAO.getDTOByID(id);

				Utils.ProcessFile( request, response, filesDTO.fileTitle, filesDTO );

			}
			else if(actionType.equals("downloadDropzoneFile"))
			{
				long id = Long.parseLong(request.getParameter("id"));

				FilesDTO filesDTO = (FilesDTO) filesDAO.getDTOByID(id);
				
				Utils.ProcessFile(request, response, filesDTO.fileTitle, filesDTO.inputStream);
				
			}
			else if(actionType.equals("DeleteFileFromDropZone"))
			{
				long id = Long.parseLong(request.getParameter("id"));
			
				
				System.out.println("In delete file");
				filesDAO.hardDeleteByID(id);
				response.getWriter().write("Deleted");
							
			}
			else if(actionType.equals("getURL"))
			{
				String URL = request.getParameter("URL");
				System.out.println("URL = " + URL);
				response.sendRedirect(URL);			
			}			
			else if(actionType.equals("search"))
			{
				System.out.println("search requested");
				//if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.EDMS_DOCUMENTS_SEARCH))
				{
					if(isPermanentTable)
					{
						String filter = request.getParameter("filter");
						System.out.println("filter = " + filter);
						if(filter!=null)
						{
							/*if(filter.equalsIgnoreCase("getAllIncoming"))
							{
								filter = DashboardService.getAllIncoming;
							}
							else if(filter.equalsIgnoreCase("getAllOutgoing"))
							{
								filter = DashboardService.getAllOutGoing;
							}
							else if(filter.equalsIgnoreCase("getApprovedRfi"))
							{
								filter = DashboardService.getApprovedRfi;
							}
							else if(filter.equalsIgnoreCase("getRejectedRfi"))
							{
								filter = DashboardService.getRejectedRfi;
							}
							else if(filter.equalsIgnoreCase("getOthersRfi"))
							{
								filter = DashboardService.getOthersRfi;
							}
							else if(filter.equalsIgnoreCase("getRfi"))
							{
								filter = DashboardService.getRfi;
							}							
							else
							{
								filter = "";
							}*/

							
							System.out.println("filter expanded = " + filter);
							
							searchEdms_documents(request, response, isPermanentTable, filter);
						}
						else
						{
							searchEdms_documents(request, response, isPermanentTable,"");
						}
						
					}
					else
					{
						//searchEdms_documents(request, response, tempTableName, isPermanentTable);
					}
				}			
			}
			else if(actionType.equals("getApprovalPage"))
			{
				System.out.println("edms getApprovalPage requested");
				//if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.EDMS_DOCUMENTS_APPROVE))
				{
					searchEdms_documents(request, response, false,"");
				}
							
			}
			else if(actionType.equals("viewApprovalNotification"))
			{
				System.out.println("view requested");
				//if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.APPROVAL_TEST_SEARCH))
				{
					commonRequestHandler.viewApprovalNotification(request, response);
				}
								
			}
			else if(actionType.equals("view"))
			{
				System.out.println("view requested");
				//if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.EDMS_DOCUMENTS_SEARCH))
				{
					commonRequestHandler.view(request, response);
				}
				
				
			}
			else if (actionType.equals("getSubIndex")) {
				response.setContentType("application/json");
				
				long id = -1;
				
				if(request.getParameter("id") != null && !request.getParameter("id").equalsIgnoreCase("") && 
						!request.getParameter("id").equalsIgnoreCase("null"))
				{
					System.out.println("id = " + request.getParameter("id"));
					id = Long.parseLong(request.getParameter("id"));
				}

				PrintWriter out = response.getWriter();
				out.print(edms_documentsDAO.getIndexSubType(id));
				out.flush();
			}
			else if( actionType.equals( "getReferenceDoc" ) ){

				response.setContentType( "application/json" );
				String searchText = request.getParameter( "searchText" );
				String officeId = request.getParameter( "officeId" );
				String indexType = request.getParameter( "fileIndex" );
				String indexSubtype = request.getParameter( "fileIndexSubType" );
				String docNumber = request.getParameter( "docNumber" );
				String docProcess = request.getParameter( "docProcess" );
				String docType = request.getParameter( "docType" );
				String docDateFrom = request.getParameter( "docDateFrom" );
				String docDateTo = request.getParameter( "docDateTo" );
				String refNo = request.getParameter( "refNo" );
				String source = request.getParameter( "source" );

				PrintWriter printWriter = response.getWriter();

				try {

					List<ReferenceDocDTO> referenceDocDTOList = edms_documentsDAO.getReferenceDocListBySearhcText( searchText, officeId, indexType, indexSubtype, docNumber, docProcess, docType, docDateFrom, docDateTo, source, refNo );

					response.setStatus( HttpServletResponse.SC_OK );
					printWriter.print( gson.toJson( referenceDocDTOList ) );
				}
				catch ( Exception e ){

					response.setStatus( HttpServletResponse.SC_INTERNAL_SERVER_ERROR );

					ErrorDTO errorDTO = new ErrorDTO();
					errorDTO.setCode( 500 );
					errorDTO.setMessage( e.getMessage() );
					errorDTO.setDate( sdf.format( new Date( System.currentTimeMillis() ) ) );

					printWriter.print( gson.toJson( errorDTO ) );
				}

				printWriter.flush();
				printWriter.close();
			}
			else if( actionType.equals( "bulkDownload" ) ){

				String[] ids = request.getParameterValues( "id" );
				bulkDownload( request, response, ids );
			}
			else if( actionType.equalsIgnoreCase( "selectForShare" ) ){

				String docs = request.getParameter( "docs" );
				EdmsDocIdNamePair[] edmsDocIdNamePairs = gson.fromJson( docs, EdmsDocIdNamePair[].class );

				Object obj = request.getSession( true ).getAttribute( SessionConstants.EDMS_DOC_SELECTED_FOR_SHARE );
				if( obj == null )
					obj = new HashSet<EdmsDocIdNamePair>();

				Set<EdmsDocIdNamePair> edmsDocIdNamePairSet = (Set<EdmsDocIdNamePair>)obj;

				for( EdmsDocIdNamePair edmsDocIdNamePair: edmsDocIdNamePairs ){

					edmsDocIdNamePairSet.add( edmsDocIdNamePair );
				 }

				request.getSession().setAttribute( SessionConstants.EDMS_DOC_SELECTED_FOR_SHARE, edmsDocIdNamePairSet );

				response.setContentType( "application/json" );
				response.getOutputStream().print( "true" );
				response.getOutputStream().close();
			}
			else if( actionType.equals( "clearShareList" ) ){

				request.getSession().setAttribute( SessionConstants.EDMS_DOC_SELECTED_FOR_SHARE, new HashSet<>() );

				response.setContentType( "application/json" );
				response.getOutputStream().print( "true" );
				response.getOutputStream().close();
			}
			else if( actionType.equals( "removeForShare" ) ){

				Long id = Long.parseLong( request.getParameter( "id" ) );
				Set<EdmsDocIdNamePair> edmsDocIdNamePairSet = (Set<EdmsDocIdNamePair>) request.getSession().getAttribute( SessionConstants.EDMS_DOC_SELECTED_FOR_SHARE );
				edmsDocIdNamePairSet.remove( new EdmsDocIdNamePair( id, "" ) );

				response.setContentType( "application/json" );
				response.getOutputStream().print( "true" );
				response.getOutputStream().close();
			}
			else
			{
				//request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}

	private void bulkDownload(HttpServletRequest request, HttpServletResponse response, String[] ids) throws Exception {

		List<Long> idList = new ArrayList<>();
		for( String id:ids ){
			idList.add( Long.parseLong( id ) );
		}
		List<Edms_documentsDTO> edms_documentsDTOS = edms_documentsDAO.getDTOs( idList );

		List<FilesDTO> filesDTOList = new ArrayList<>();
		for( Edms_documentsDTO edms_documentsDTO: edms_documentsDTOS ){

			filesDTOList.addAll( filesDAO.getDTOsByFileID( edms_documentsDTO.filesDropzone ) );
		}

		response.setContentType( "application/zip" );
		String headerKey = "Content-Disposition";
		String headerValue = String.format("attachment; filename=\"%s\"", "edms_documents.zip" );
		response.setHeader(headerKey, headerValue);

		OutputStream os = response.getOutputStream();
		Utils.writeZipToStreamFromFilesDTOList( filesDTOList, os );
		os.close();
	}

	private void get_addPageForPartner(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.setAttribute("ID", -1L);
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("/edms_documents/partner/addForPartner.jsp" );
		requestDispatcher.forward(request, response);
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		// TODO Auto-generated method stub
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		System.out.println("doPost");
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		
		try
		{
			String actionType = request.getParameter("actionType");
			System.out.println("actionType = " + actionType);
			if(actionType.equals("add"))
			{
				
				//if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.EDMS_DOCUMENTS_ADD))
				{
					System.out.println("going to  addEdms_documents ");
					addEdms_documents(request, response, true, userDTO, true);
				}
				
				
			}
			if( actionType.equals( "addForPartner" ) )
			{
				//if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.EDMS_DOCUMENTS_ADD))
				{
					System.out.println("going to  addEdms_documents for partner");
					addEdms_documentsForPartner(request, response, true, userDTO, true);
				}
				
			}
			else if(actionType.equals("UploadFilesFromDropZone"))
			{
				String Column = request.getParameter("columnName");
				long ColumnID = Long.parseLong(request.getParameter("ColumnID"));
				String pageType = request.getParameter("pageType");
				
				System.out.println("In " + pageType);				
				Utils.UploadFilesFromDropZone(request, response, userDTO.ID, ColumnID, "Edms_documentsServlet");	
			}
			
			else if(actionType.equals("getDocByRef"))
			{
				int toLetter = Integer.parseInt(request.getParameter("to"));
				String to = CatDAO.getName("English", "letter_to", toLetter);
				String year = request.getParameter("year");
				String refno = request.getParameter("refno");
				
				System.out.println("getDocByRef to = " + to + " year = " + year + " refno = " + refno);
				
				Edms_documentsDTO edms_documentsDTO = edms_documentsDAO.getDTOByRef(to, year, refno);
				
				if(edms_documentsDTO != null)
				{
					System.out.println("edms_documentsDTO found");
					response.setContentType( "application/json" );
					response.setStatus( HttpServletResponse.SC_OK );
					PrintWriter printWriter = response.getWriter();
					printWriter.print(gson.toJson(edms_documentsDTO));
					printWriter.flush();
					printWriter.close();
				}
				
			}

			else if(actionType.equals("SendToApprovalPath"))
			{

				//if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.EDMS_DOCUMENTS_APPROVE))
				{
					long id = Long.parseLong(request.getParameter("idToApprove"));
					Edms_documentsDTO edms_documentsDTO = (Edms_documentsDTO)edms_documentsDAO.getDTOByID(id);
					commonRequestHandler.sendToApprovalPath(request, response, userDTO, edms_documentsDTO.documentSubject);
				}
				

			}
			else if(actionType.equals("approve"))
			{
				
				//if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.EDMS_DOCUMENTS_ADD))
				{					
					commonRequestHandler.approve(request, response, true, userDTO, true);
				}
				
				
			}
			if(actionType.equals("reject"))
			{
				System.out.println("trying to approve");
				
				//if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.EDMS_DOCUMENTS_APPROVE))
				{					
					commonRequestHandler.approve(request, response, true, userDTO, false);
				}
							
			}
			if(actionType.equals("terminate"))
			{

				//if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.EDMS_DOCUMENTS_ADD))
				{
					commonRequestHandler.terminate(request, response, userDTO);
				}
				
			}
			else if(actionType.equals("skipStep"))
			{
				
				System.out.println("skipStep");
				commonRequestHandler.skipStep(request, response, userDTO);									
			}
			else if(actionType.equals("getDTO"))
			{
				
				//if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.EDMS_DOCUMENTS_ADD))
				{
					getDTO(request, response);					
				}
			
				
			}
			else if(actionType.equals("edit"))
			{
				
				//if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.EDMS_DOCUMENTS_UPDATE))
				{					
					addEdms_documents(request, response, false, userDTO, isPermanentTable);					
				}
				
			}
			else if(actionType.equals("editForPartner"))
			{

				//if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.EDMS_DOCUMENTS_UPDATE))
				{
					addEdms_documentsForPartner(request, response, false, userDTO, isPermanentTable);
				}
				
			}
			else if(actionType.equals("delete"))
			{								
				deleteEdms_documents(request, response, userDTO);				
			}	
			else if(actionType.equals("search"))
			{
				//if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.EDMS_DOCUMENTS_SEARCH))
				{
					searchEdms_documents(request, response, true, "");
				}
				
			}
			else if(actionType.equals("getGeo"))
			{
				System.out.println("going to geoloc ");
				request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
			}
			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}
	
	private void getDTO(HttpServletRequest request, HttpServletResponse response) 
	{
		try 
		{
			System.out.println("In getDTO");
			Edms_documentsDTO edms_documentsDTO = (Edms_documentsDTO)edms_documentsDAO.getDTOByID(Long.parseLong(request.getParameter("ID")));
			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			
			String encoded = this.gson.toJson(edms_documentsDTO);
			System.out.println("json encoded data = " + encoded);
			out.print(encoded);
			out.flush();
		}
		catch (NumberFormatException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	private void addEdms_documentsForPartner( HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, boolean isPermanentTable ){

		logger.debug( "Add Edms Doc for Partner called" );

		try {
			request.setAttribute("failureMessage", "");
			Edms_documentsDTO edms_documentsDTO;

			if (addFlag == true) {
				edms_documentsDTO = new Edms_documentsDTO();
			} else {
				edms_documentsDTO = (Edms_documentsDTO) edms_documentsDAO.getDTOByID(Long.parseLong(request.getParameter("iD")));
			}

			edms_documentsDTO.documentProcessCat = CommonConstant.DOC_INCOMING;

			String contractorTypeStr = request.getParameter("pbrlpContractorType" );
			Long contractorType = Long.parseLong( contractorTypeStr );
			edms_documentsDTO.pbrlpContractorType = contractorType;

			String officeTypeStr = request.getParameter( "officesType" );
			Long officeType = Long.parseLong( officeTypeStr );
			edms_documentsDTO.officesType = officeType;

			String docTypeStr = request.getParameter( "documentTypeCat" );
			Integer docType = Integer.parseInt( docTypeStr );
			edms_documentsDTO.documentTypeCat = docType;

			String docDateStr = request.getParameter( "documentDate" );
			edms_documentsDTO.documentDate = Long.parseLong( docDateStr );

			String documentSubject = request.getParameter( "documentSubject" );
			documentSubject = Jsoup.clean( documentSubject, Whitelist.simpleText() );
			edms_documentsDTO.documentSubject = documentSubject;

			String referenceNo = request.getParameter( "referenceNo" );
			referenceNo = Jsoup.clean( referenceNo, Whitelist.simpleText() );
			edms_documentsDTO.referenceNo = referenceNo;

			String refPart1 = Jsoup.clean( request.getParameter( "referenceNoPart1" ), Whitelist.simpleText() );
			String refPart2 = Jsoup.clean( request.getParameter( "referenceNoPart2" ), Whitelist.simpleText() );
			String refPart3 = Jsoup.clean( request.getParameter( "referenceNoPart3" ), Whitelist.simpleText() );
			String refPart4 = Jsoup.clean( request.getParameter( "referenceNoPart4" ), Whitelist.simpleText() );
			String refPart5 = Jsoup.clean( request.getParameter( "referenceNoPart5" ), Whitelist.simpleText() );
			String refPart6 = Jsoup.clean( request.getParameter( "referenceNoPart6" ), Whitelist.simpleText() );

			edms_documentsDTO.refPart1 = refPart1;
			edms_documentsDTO.refPart2 = refPart2;
			edms_documentsDTO.refPart3 = refPart3;
			edms_documentsDTO.refPart4 = refPart4;
			edms_documentsDTO.refPart5 = refPart5;
			edms_documentsDTO.refPart6 = refPart6;

			String description = request.getParameter( "description" );
			description = Jsoup.clean( description, Whitelist.simpleText() );
			edms_documentsDTO.description = description;

			String fileDropzoneStr = request.getParameter( "filesDropzone" );
			Long fileDropzone = Long.parseLong( fileDropzoneStr );
			edms_documentsDTO.filesDropzone = fileDropzone;
			if(addFlag == false)
			{
				String filesDropzoneFilesToDelete = request.getParameter("filesDropzoneFilesToDelete");
				String[] deleteArray = filesDropzoneFilesToDelete.split(",");
				for(int i = 0; i < deleteArray.length; i ++){
					if( i>0 ){
						filesDAO.delete(Long.parseLong(deleteArray[i]));
					}
				}
			}

			long returnedID = -1;
			if(addFlag == true)
			{
				edms_documentsDTO.docAdded = System.currentTimeMillis();
				edms_documentsDTO.userId = userDTO.ID;
				edms_documentsDTO.documentNo = "0";
				edms_documentsDTO.documentReceiveDate = System.currentTimeMillis();
				edms_documentsDTO.deadline = edms_documentsDTO.documentReceiveDate + CommonConstant.EDMS_DOC_DEADLINE_OFFSET;
				edms_documentsDTO.rfiStatus = 6;

				returnedID = edms_documentsDAO.manageWriteOperations( edms_documentsDTO, SessionConstants.INSERT, -1, userDTO );

				List<DocumentChainageDTO> documentChainageDTOList = createDocumentChainageDTOListByRequest(request);
				if( documentChainageDTOList != null ){

					for(DocumentChainageDTO documentChainageDTO: documentChainageDTOList){

						documentChainageDTO.edmsDocumentsId = edms_documentsDTO.iD;
						documentChainageDAO.add( documentChainageDTO );
					}
				}

				Long ofcUnitOrgIdForInbox = new EdmsDocInboxDeciderDAO().getOrganogramIdForInbox(edms_documentsDTO);
				if( ofcUnitOrgIdForInbox != null ) {

					EdmsDocUtil.addToInbox(returnedID, userDTO, "New edms doc added", "New edms doc added", 0, ofcUnitOrgIdForInbox, 0);
				}
				else {

					logger.error("No orgranogram id found for sending to inbox. for DOcument id " + edms_documentsDTO.iD);
				}
			}
			else
			{
				returnedID = edms_documentsDAO.manageWriteOperations( edms_documentsDTO, SessionConstants.UPDATE, -1, userDTO );

				List<Long> childIdsFromRequest = documentChainageDAO.getChildIdsFromRequest(request, "documentChainage");
				documentChainageDAO.deleteChildrenNotInList("edms_documents", "document_chainage", edms_documentsDTO.iD, childIdsFromRequest);
				List<Long> childIDsInDatabase = documentChainageDAO.getChilIds("edms_documents", "document_chainage", edms_documentsDTO.iD);

				if(childIdsFromRequest != null){
					for(int i = 0; i < childIdsFromRequest.size(); i ++){

						Long childIDFromRequest = childIdsFromRequest.get(i);
						if(childIDsInDatabase.contains(childIDFromRequest)){

							DocumentChainageDTO documentChainageDTO =  createDocumentChainageDTOByRequestAndIndex(request, false, i);
							documentChainageDTO.edmsDocumentsId = edms_documentsDTO.iD;
							documentChainageDAO.update(documentChainageDTO);
						}
						else{

							DocumentChainageDTO documentChainageDTO =  createDocumentChainageDTOByRequestAndIndex(request, true, i);
							documentChainageDTO.edmsDocumentsId = edms_documentsDTO.iD;
							documentChainageDAO.add(documentChainageDTO);
						}
					}
				}
				else{
					documentChainageDAO.deleteDocumentChainageByEdmsDocumentsID(edms_documentsDTO.iD);
				}
			}

			response.sendRedirect("Edms_documentsServlet?actionType=search");
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

		return;
	}

	private void addEdms_documents(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, boolean isPermanentTable) throws IOException 
	{
		// TODO Auto-generated method stub
		try 
		{
			request.setAttribute("failureMessage", "");
			System.out.println("%%%% addEdms_documents");
			String path = getServletContext().getRealPath("/img2/");
			Edms_documentsDTO edms_documentsDTO;
			Boolean developmentPartner = userDTO.roleID == 9801;
						
			if(addFlag == true)
			{
				edms_documentsDTO = new Edms_documentsDTO();
			}
			else
			{
				edms_documentsDTO = (Edms_documentsDTO)edms_documentsDAO.getDTOByID(Long.parseLong(request.getParameter("iD")));
			}
			
			String Value = "";

			if( addFlag == true ) {
				edms_documentsDTO.docAdded = System.currentTimeMillis();
				edms_documentsDTO.userId = userDTO.ID;
			}

			if( developmentPartner ){

				edms_documentsDTO.documentProcessCat = 0;
			}
			else{

				Value = request.getParameter("documentProcessCat");
				if(Value != null)
				{
					Value = Jsoup.clean(Value,Whitelist.simpleText());
				}
				System.out.println("documentProcessCat = " + Value);
				if(Value != null && !Value.equalsIgnoreCase(""))
				{

					edms_documentsDTO.documentProcessCat = Integer.parseInt(Value);
				}
				else
				{
					System.out.println("FieldName has a null Value, not updating" + " = " + Value);
				}
			}


			if( developmentPartner ){

				edms_documentsDTO.documentNo = "0";
			}
			else{

				Value = request.getParameter("documentNo");
				if(Value != null)
				{
					Value = Jsoup.clean(Value,Whitelist.simpleText());
				}
				System.out.println("documentNo = " + Value);
				if(Value != null && !Value.equalsIgnoreCase(""))
				{

					edms_documentsDTO.documentNo = Value;
				}
				else
				{
					System.out.println("FieldName has a null Value, not updating" + " = " + Value);
				}
			}


			if( developmentPartner ){

				edms_documentsDTO.pbrlpContractorType = 100L;
			}
			else{

				Value = request.getParameter("pbrlpContractorType");
				if(Value != null)
				{
					Value = Jsoup.clean(Value,Whitelist.simpleText());
				}
				System.out.println("pbrlpContractorType = " + Value);
				if(Value != null && !Value.equalsIgnoreCase(""))
				{

					edms_documentsDTO.pbrlpContractorType = Long.parseLong(Value);
				}
				else
				{
					System.out.println("FieldName has a null Value, not updating" + " = " + Value);
				}

				Value = request.getParameter( "other_contractor" );
				if( Value != null ){
					Value = Jsoup.clean( Value, Whitelist.simpleText() );
					edms_documentsDTO.otherContractor = Value;
				}
			}


			Value = request.getParameter("fileIndexTypeType");

			if( developmentPartner ){

				Value = "";
			}

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("fileIndexTypeType = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				edms_documentsDTO.fileIndexTypeType = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("fileIndexSubtypeType");

			if( developmentPartner ){

				Value = "";
			}

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("fileIndexSubtypeType = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				edms_documentsDTO.fileIndexSubtypeType = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("officesType");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("officesType = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				edms_documentsDTO.officesType = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("documentTypeCat");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("documentTypeCat = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				edms_documentsDTO.documentTypeCat = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("documentDate");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("documentDate = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				edms_documentsDTO.documentDate = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}


			if( developmentPartner ){

				edms_documentsDTO.documentReceiveDate = System.currentTimeMillis();
				edms_documentsDTO.deadline = edms_documentsDTO.documentReceiveDate + CommonConstant.EDMS_DOC_DEADLINE_OFFSET;
			}
			else{

				Value = request.getParameter("documentReceiveDate");
				if(Value != null)
				{
					Value = Jsoup.clean(Value,Whitelist.simpleText());
				}
				System.out.println("documentReceiveDate = " + Value);
				if(Value != null && !Value.equalsIgnoreCase(""))
				{

					edms_documentsDTO.documentReceiveDate = Long.parseLong(Value);
					edms_documentsDTO.deadline = edms_documentsDTO.documentReceiveDate + CommonConstant.EDMS_DOC_DEADLINE_OFFSET;
				}
				else
				{
					System.out.println("FieldName has a null Value, not updating" + " = " + Value);
				}
			}



			Value = request.getParameter("documentSubject");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("documentSubject = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				edms_documentsDTO.documentSubject = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("referenceNo");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("referenceNo = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				edms_documentsDTO.referenceNo = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			String refPart1 = Jsoup.clean( request.getParameter( "referenceNoPart1" ), Whitelist.simpleText() );
			String refPart2 = Jsoup.clean( request.getParameter( "referenceNoPart2" ), Whitelist.simpleText() );
			String refPart3 = Jsoup.clean( request.getParameter( "referenceNoPart3" ), Whitelist.simpleText() );
			String refPart4 = Jsoup.clean( request.getParameter( "referenceNoPart4" ), Whitelist.simpleText() );
			String refPart5 = Jsoup.clean( request.getParameter( "referenceNoPart5" ), Whitelist.simpleText() );
			String refPart6 = Jsoup.clean( request.getParameter( "referenceNoPart6" ), Whitelist.simpleText() );

			edms_documentsDTO.refPart1 = refPart1;
			edms_documentsDTO.refPart2 = refPart2;
			edms_documentsDTO.refPart3 = refPart3;
			edms_documentsDTO.refPart4 = refPart4;
			edms_documentsDTO.refPart5 = refPart5;
			edms_documentsDTO.refPart6 = refPart6;

			Value = request.getParameter("section");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("section = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{

				edms_documentsDTO.section = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("rfi_status");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("section = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{

				edms_documentsDTO.rfiStatus = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter( "rfi_type" );
			if( Value != null ){
				edms_documentsDTO.rfiType = Integer.parseInt( Value );
			}

			Value = request.getParameter( "rfi_distribution" );
			if( Value != null ){
				edms_documentsDTO.rfiDistribution = Integer.parseInt( Value );
			}

			Value = request.getParameter("rfi_status_other");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("section = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{

				edms_documentsDTO.rfiStatusOther = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("crec_no");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("section = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{

				edms_documentsDTO.crecNo = Value;
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter( "ncr_submission_date" );
			if( Value != null ){
				edms_documentsDTO.ncrSubmissionDate = Long.parseLong( Value );
			}

			Value = request.getParameter( "open_reason" );
			if( Value != null ){

				Value = Jsoup.clean( Value, Whitelist.simpleText() );
				edms_documentsDTO.openReason = Value;
			}

			Value = request.getParameter("description");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("description = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				edms_documentsDTO.description = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("filesDropzone");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("filesDropzone = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
								
				System.out.println("filesDropzone = " + Value);
				if(Value != null && !Value.equalsIgnoreCase(""))
				{
					edms_documentsDTO.filesDropzone = Long.parseLong(Value);
				}
				else
				{
					System.out.println("FieldName has a null Value, not updating" + " = " + Value);
				}
				
				if(addFlag == false)
				{
					String filesDropzoneFilesToDelete = request.getParameter("filesDropzoneFilesToDelete");
					String[] deleteArray = filesDropzoneFilesToDelete.split(",");
					for(int i = 0; i < deleteArray.length; i ++)
					{
						System.out.println("going to delete " + deleteArray[i]);
						if(i>0)
						{
							filesDAO.delete(Long.parseLong(deleteArray[i]));
						}
					}
				}
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("isConfidential");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("isConfidential = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				edms_documentsDTO.isConfidential = Boolean.parseBoolean(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("docConfidentialDate");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("docConfidentialDate = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				edms_documentsDTO.docConfidentialDate = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("isLocked");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("isLocked = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				edms_documentsDTO.isLocked = Boolean.parseBoolean(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("docLockedDate");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("docLockedDate = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				edms_documentsDTO.docLockedDate = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("parentDocumentsId");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("parentDocumentsId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				edms_documentsDTO.parentDocumentsId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("jobCat");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("jobCat = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				edms_documentsDTO.jobCat = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			
			System.out.println("Done adding  addEdms_documents dto = " + edms_documentsDTO);
			long returnedID = -1;
			
			if(isPermanentTable == false) //add new row for validation and make the old row outdated
			{
				edms_documentsDAO.setIsDeleted(edms_documentsDTO.iD, CommonDTO.OUTDATED);
				returnedID = edms_documentsDAO.add(edms_documentsDTO);
				edms_documentsDAO.setIsDeleted(returnedID, CommonDTO.WAITING_FOR_APPROVAL);
			}
			else if(addFlag == true)
			{
				returnedID = edms_documentsDAO.manageWriteOperations(edms_documentsDTO, SessionConstants.INSERT, -1, userDTO);
			}
			else
			{				
				returnedID = edms_documentsDAO.manageWriteOperations(edms_documentsDTO, SessionConstants.UPDATE, -1, userDTO);											
			}

			List<EdmsDocReferenceDocMapDTO> existingRefDoc = edmsDocReferenceDocMapDAO.getByEdmsDocId( edms_documentsDTO.iD );
			Set<Long> refDocNeedToBeAdded = getRefDocNeedToBeAddedFromRequest( request );
			Set<Long> refDocNeedToBeDeleted = getRefDocNeedToBeDeletedFromRequest( request );
			Set<Long> remainingRefDocToBeAdded = getReaminingRefDocNeedToBeAdded( refDocNeedToBeAdded, refDocNeedToBeDeleted );
			remainingRefDocToBeAdded = getReaminingRefDocNeedToBeAddedAfterSubtractingExisting( remainingRefDocToBeAdded, existingRefDoc );

			if(isPermanentTable == true)
			{
				edmsDocReferenceDocMapDAO.insert( edms_documentsDTO.iD, remainingRefDocToBeAdded );
				if( addFlag == false)
				{

					edmsDocReferenceDocMapDAO.deleteRefDoc( edms_documentsDTO.iD, refDocNeedToBeDeleted );
				}
			}
			else // for validation
			{
				edmsDocReferenceDocMapDAO.insert( returnedID, remainingRefDocToBeAdded );
				edmsDocReferenceDocMapDAO.deleteRefDoc( returnedID, refDocNeedToBeDeleted );
			}

			List<DocumentChainageDTO> documentChainageDTOList = createDocumentChainageDTOListByRequest(request);			
			if(addFlag == true || isPermanentTable == false) //add or validate
			{
				if(documentChainageDTOList != null)
				{				
					for(DocumentChainageDTO documentChainageDTO: documentChainageDTOList)
					{
						documentChainageDTO.edmsDocumentsId = edms_documentsDTO.iD; 
						documentChainageDAO.add(documentChainageDTO);
					}
				}
			
			}
			else
			{
				List<Long> childIdsFromRequest = documentChainageDAO.getChildIdsFromRequest(request, "documentChainage");
				//delete the removed children
				documentChainageDAO.deleteChildrenNotInList("edms_documents", "document_chainage", edms_documentsDTO.iD, childIdsFromRequest);
				List<Long> childIDsInDatabase = documentChainageDAO.getChilIds("edms_documents", "document_chainage", edms_documentsDTO.iD);
				
				
				if(childIdsFromRequest != null)
				{
					for(int i = 0; i < childIdsFromRequest.size(); i ++)
					{
						Long childIDFromRequest = childIdsFromRequest.get(i);
						if(childIDsInDatabase.contains(childIDFromRequest))
						{
							DocumentChainageDTO documentChainageDTO =  createDocumentChainageDTOByRequestAndIndex(request, false, i);
							documentChainageDTO.edmsDocumentsId = edms_documentsDTO.iD; 
							documentChainageDAO.update(documentChainageDTO);
						}
						else
						{
							DocumentChainageDTO documentChainageDTO =  createDocumentChainageDTOByRequestAndIndex(request, true, i);
							documentChainageDTO.edmsDocumentsId = edms_documentsDTO.iD; 
							documentChainageDAO.add(documentChainageDTO);
						}
					}
				}
				else
				{
					documentChainageDAO.deleteDocumentChainageByEdmsDocumentsID(edms_documentsDTO.iD);
				}
				
			}

			
			
			
			
			if(isPermanentTable)
			{
				if(addFlag == true) {
					Long ofcUnitOrgIdForInbox = new EdmsDocInboxDeciderDAO().getOrganogramIdForInbox(edms_documentsDTO);

					if( ofcUnitOrgIdForInbox != null )
						EdmsDocUtil.addToInbox(returnedID, userDTO, "New edms doc added", "New edms doc added", 0, ofcUnitOrgIdForInbox, 0);
					else
						logger.error( "No orgranogram id found for sending to inbox. for DOcument id " + edms_documentsDTO.iD );
				}
				String inPlaceSubmit = request.getParameter("inplacesubmit");
				
				if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
				{
					getEdms_documents(request, response, returnedID);
				}
				else
				{
					response.sendRedirect("Edms_documentsServlet?actionType=search");
				}
			}
			else
			{
				commonRequestHandler.validate(edms_documentsDAO.getDTOByID(returnedID), request, response, userDTO);
			}
					
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}

	private Set<Long> getReaminingRefDocNeedToBeAddedAfterSubtractingExisting(Set<Long> refDocIdSet, List<EdmsDocReferenceDocMapDTO> existingRefDoc) {

		for( EdmsDocReferenceDocMapDTO edmsDocReferenceDocMapDTO: existingRefDoc ){

			refDocIdSet.remove( edmsDocReferenceDocMapDTO.referenceDocId );
		}

		return refDocIdSet;
	}

	private Set<Long> getReaminingRefDocNeedToBeAdded(Set<Long> refDocNeedToBeAdded, Set<Long> refDocNeedToBeDeleted) {

		for( Long deleteId: refDocNeedToBeDeleted ){

			refDocNeedToBeAdded.remove( deleteId );
		}

		return refDocNeedToBeAdded;
	}

	private Set<Long> getRefDocNeedToBeDeletedFromRequest(HttpServletRequest request) {

		String[] refDocIds = request.getParameterValues( "removeRefDoc" );
		Set<Long> refDocIdSet = new HashSet<>();

		if( refDocIds == null || refDocIds.length == 0 )
			return refDocIdSet;

		for( String id: refDocIds ){

			refDocIdSet.add( Long.parseLong( id ) );
		}

		return refDocIdSet;
	}

	private Set<Long> getRefDocNeedToBeAddedFromRequest(HttpServletRequest request) {

		String[] refDocIds = request.getParameterValues( "refDocAdded" );
		Set<Long> refDocIdSet = new HashSet<>();

		if( refDocIds == null || refDocIds.length == 0 )
			return refDocIdSet;

		for( String id: refDocIds ){

			refDocIdSet.add( Long.parseLong( id ) );
		}

		return refDocIdSet;
	}


	private List<DocumentChainageDTO> createDocumentChainageDTOListByRequest(HttpServletRequest request) throws Exception{ 
		List<DocumentChainageDTO> documentChainageDTOList = new ArrayList<DocumentChainageDTO>();
		if(request.getParameterValues("documentChainage.iD") != null) 
		{
			int documentChainageItemNo = request.getParameterValues("documentChainage.iD").length;
			
			
			for(int index=0;index<documentChainageItemNo;index++){
				DocumentChainageDTO documentChainageDTO = createDocumentChainageDTOByRequestAndIndex(request,true,index);
				documentChainageDTOList.add(documentChainageDTO);
			}
			
			return documentChainageDTOList;
		}
		return null;
	}
	
	
	private DocumentChainageDTO createDocumentChainageDTOByRequestAndIndex(HttpServletRequest request, boolean addFlag,int index) throws Exception{
	
		DocumentChainageDTO documentChainageDTO;
		if(addFlag == true )
		{
			documentChainageDTO = new DocumentChainageDTO();
		}
		else
		{
			documentChainageDTO = (DocumentChainageDTO)documentChainageDAO.getDTOByID(Long.parseLong(request.getParameterValues("documentChainage.iD")[index]));
		}
		String path = getServletContext().getRealPath("/img2/");
		
		
		
				
		String Value = "";
		Value = request.getParameterValues("documentChainage.edmsDocumentsId")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		documentChainageDTO.edmsDocumentsId = Long.parseLong(request.getParameterValues("documentChainage.edmsDocumentsId")[index]);
		Value = request.getParameterValues("documentChainage.description")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		documentChainageDTO.description = (request.getParameterValues("documentChainage.description")[index]);
		Value = request.getParameterValues("documentChainage.fromChainageKm")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		documentChainageDTO.fromChainageKm = Integer.parseInt(request.getParameterValues("documentChainage.fromChainageKm")[index]);
		Value = request.getParameterValues("documentChainage.fromChainageMeter")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		documentChainageDTO.fromChainageMeter = Integer.parseInt(request.getParameterValues("documentChainage.fromChainageMeter")[index]);
		Value = request.getParameterValues("documentChainage.fromChainageMm")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		documentChainageDTO.fromChainageMm = Integer.parseInt(request.getParameterValues("documentChainage.fromChainageMm")[index]);
		Value = request.getParameterValues("documentChainage.toChainageKm")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		documentChainageDTO.toChainageKm = Integer.parseInt(request.getParameterValues("documentChainage.toChainageKm")[index]);
		Value = request.getParameterValues("documentChainage.toChainageMeter")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		documentChainageDTO.toChainageMeter = Integer.parseInt(request.getParameterValues("documentChainage.toChainageMeter")[index]);
		Value = request.getParameterValues("documentChainage.toChainageMm")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		documentChainageDTO.toChainageMm = Integer.parseInt(request.getParameterValues("documentChainage.toChainageMm")[index]);
		return documentChainageDTO;
	
	}
	
	
	

	private void deleteEdms_documents(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO) throws IOException 
	{				
		try 
		{
			String[] IDsToDelete = request.getParameterValues("ID");
			for(int i = 0; i < IDsToDelete.length; i ++)
			{
				long id = Long.parseLong(IDsToDelete[i]);
				System.out.println("------ DELETING " + IDsToDelete[i]);
				
				
				Edms_documentsDTO edms_documentsDTO = (Edms_documentsDTO)edms_documentsDAO.getDTOByID(id);
				edms_documentsDAO.manageWriteOperations(edms_documentsDTO, SessionConstants.DELETE, id, userDTO);
				documentChainageDAO.deleteDocumentChainageByEdmsDocumentsID(id);
				response.sendRedirect("Edms_documentsServlet?actionType=search");
				
			}			
		}
		catch (Exception ex) 
		{
			ex.printStackTrace();
		}
		
	}

	private void getEdms_documents(HttpServletRequest request, HttpServletResponse response, long id) throws ServletException, IOException
	{
		System.out.println("in getEdms_documents");
		Edms_documentsDTO edms_documentsDTO = null;
		try 
		{
			edms_documentsDTO = (Edms_documentsDTO)edms_documentsDAO.getDTOByID(id);
			
			boolean isPermanentTable = true;
			if(request.getParameter("isPermanentTable") != null)
			{
				isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
			}
			
			if(isPermanentTable == false)
			{
				List<FilesDTO> filesDropzoneDTOList = filesDAO.getDTOsByFileID( edms_documentsDTO.filesDropzone );
				long ColumnID = DBMW.getInstance().getNextSequenceId("fileid");
				System.out.println("ColumnID = " + ColumnID);
				for(int i = 0; i < filesDropzoneDTOList.size(); i ++)
				{
					FilesDTO filesDTO = filesDropzoneDTOList.get(i);
					filesDTO.fileID = ColumnID;
					System.out.println("filesDTO.fileID = " + filesDTO.fileID);
					filesDAO.add(filesDTO);					
				}
				edms_documentsDTO.filesDropzone = ColumnID;
			}
			
			request.setAttribute("ID", edms_documentsDTO.iD);
			request.setAttribute("edms_documentsDTO",edms_documentsDTO);
			request.setAttribute("edms_documentsDAO",edms_documentsDAO);
			
			String URL= "";
			
			String inPlaceEdit = request.getParameter("inplaceedit");
			String inPlaceSubmit = request.getParameter("inplacesubmit");
			String getBodyOnly = request.getParameter("getBodyOnly");
			
			if(inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase(""))
			{
				URL = "edms_documents/edms_documentsInPlaceEdit.jsp";	
				request.setAttribute("inplaceedit","");				
			}
			else if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
			{
				URL = "edms_documents/edms_documentsSearchRow.jsp";
				request.setAttribute("inplacesubmit","");					
			}
			else
			{
				if(getBodyOnly  != null && !getBodyOnly.equalsIgnoreCase(""))
				{
					URL = "edms_documents/edms_documentsEditBody.jsp?actionType=edit";
				}
				else
				{
					URL = "edms_documents/edms_documentsEdit.jsp?actionType=edit";
				}				
			}
			
			RequestDispatcher rd = request.getRequestDispatcher(URL);
			rd.forward(request, response);
		}
		catch (NumberFormatException e) 
		{
			e.printStackTrace();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	
	private void getEdms_documents(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		getEdms_documents(request, response, Long.parseLong(request.getParameter("ID")));
	}

	private void getEdms_documentsForPartner( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {

		Long id = Long.parseLong( request.getParameter("ID" ) );
		Edms_documentsDTO edms_documentsDTO = null;
		try{
			edms_documentsDTO = (Edms_documentsDTO)edms_documentsDAO.getDTOByID( id );

			request.setAttribute("ID", edms_documentsDTO.iD);
			request.setAttribute("edms_documentsDTO",edms_documentsDTO);
			request.setAttribute("edms_documentsDAO",edms_documentsDAO);

			String URL= "edms_documents/partner/addForPartner.jsp?actionType=edit";

			RequestDispatcher rd = request.getRequestDispatcher(URL);
			rd.forward(request, response);
		}
		catch (NumberFormatException e){
			e.printStackTrace();
		}
		catch (Exception e){
			e.printStackTrace();
		}
	}
	
	private void searchEdms_documents(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException
	{
		System.out.println("in  searchEdms_documents 1");
		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String ajax = request.getParameter("ajax");
		boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);
		
        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
			SessionConstants.NAV_EDMS_DOCUMENTS,
			request,
			edms_documentsDAO,
			SessionConstants.VIEW_EDMS_DOCUMENTS,
			SessionConstants.SEARCH_EDMS_DOCUMENTS,
			tableName,
			isPermanent,
			userDTO,
			filter,
			true);
        try
        {
			System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        }
        catch(Exception e)
        {
			System.out.println("failed to dojob" + e);
        }

		request.setAttribute("edms_documentsDAO",edms_documentsDAO);
        RequestDispatcher rd;
        if(!isPermanent)
        {
        	if(hasAjax == false)
	        {
	        	System.out.println("Going to edms_documents/edms_documentsApproval.jsp");
	        	rd = request.getRequestDispatcher("edms_documents/edms_documentsApproval.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to edms_documents/edms_documentsApprovalForm.jsp");
	        	rd = request.getRequestDispatcher("edms_documents/edms_documentsApprovalForm.jsp");
	        }
        }
        else
        {
	        if(hasAjax == false)
	        {
	        	System.out.println("Going to edms_documents/edms_documentsSearch.jsp");
	        	rd = request.getRequestDispatcher("edms_documents/edms_documentsSearch.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to edms_documents/edms_documentsSearchForm.jsp");
	        	rd = request.getRequestDispatcher("edms_documents/edms_documentsSearchForm.jsp");
	        }
        }
		rd.forward(request, response);
	}
	
}

