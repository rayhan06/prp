package edms_documents;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;

public class EdmsDocIdNamePair {

    private Long id;
    private String name;

    public EdmsDocIdNamePair(Long id, String s) {

        this.id = id;
        this.name = s;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static List<Long> getIdList( Set<EdmsDocIdNamePair> edmsDocIdNamePairSet ){

        List<Long> idList = new ArrayList<>();
        for( EdmsDocIdNamePair edmsDocIdNamePair: edmsDocIdNamePairSet ){

            idList.add( edmsDocIdNamePair.getId() );
        }

        return idList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EdmsDocIdNamePair that = (EdmsDocIdNamePair) o;
        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
