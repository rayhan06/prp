package edms_documents;
import java.util.*; 
import util.*; 


public class DocumentChainageDTO extends CommonDTO
{

	public long edmsDocumentsId = 0;
    public String description = "";
	public int fromChainageKm = 0;
	public int fromChainageMeter = 0;
	public int fromChainageMm = 0;
	public int toChainageKm = 0;
	public int toChainageMeter = 0;
	public int toChainageMm = 0;
	
	public List<DocumentChainageDTO> documentChainageDTOList = new ArrayList<>();
	
    @Override
	public String toString() {
            return "$DocumentChainageDTO[" +
            " iD = " + iD +
            " edmsDocumentsId = " + edmsDocumentsId +
            " description = " + description +
            " fromChainageKm = " + fromChainageKm +
            " fromChainageMeter = " + fromChainageMeter +
            " fromChainageMm = " + fromChainageMm +
            " toChainageKm = " + toChainageKm +
            " toChainageMeter = " + toChainageMeter +
            " toChainageMm = " + toChainageMm +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}