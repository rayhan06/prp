package edms_documents;

import OfficeUnitsEdms.OfficeUnitsEdmsDTO;
import com.sun.org.apache.xpath.internal.operations.Bool;
import dbm.DBMR;
import dbm.DBMW;
import repository.RepositoryManager;
import util.CommonConstant;
import util.DateUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class EdmsDocReferenceDocMapDAO {

    private static final String tableName = "edms_doc_reference_doc";

    public void printSql(String sql) {

        System.out.println("sql: " + sql);
    }

    public List<EdmsDocReferenceDocMapDTO> getByEdmsDocId( Long edmsDocId ){

        Connection connection = null;
        ResultSet rs = null;
        Statement stmt = null;
        List<EdmsDocReferenceDocMapDTO> edmsDocReferenceDocMapDTOList = new ArrayList<>();
        try{

            /*String sql =
                    "select erMap.edms_doc_id, erMap.reference_doc_id, doc.document_no, doc.document_subject, erMap.isDeleted, erMap.lastModificationTime from " +
                    tableName +
                    " erMap inner join edms_documents doc on erMap.reference_doc_id=doc.ID" +
                    " where erMap.edms_doc_id=" + edmsDocId;*/

            String sql = "select " +
                    "  erMap.edms_doc_id, " +
                    "  erMap.reference_doc_id, " +
                    "  doc.document_no, " +
                    "  doc.document_subject, " +
                    "  doc.reference_no, " +
                    "  con.name_en as source, " +
                    "  lang.languageTextEnglish as docType, " +
                    "  doc.document_date, " +
                    "  erMap.isDeleted, " +
                    "  erMap.lastModificationTime " +
                    "from " +
                    "  edms_doc_reference_doc erMap " +
                    "  inner join edms_documents doc on erMap.reference_doc_id=doc.ID " +
                    "  LEFT JOIN pbrlp_contractor con on doc.pbrlp_contractor_type=con.ID " +

                    "  Left join category cat on ( doc.document_type_cat = cat.value  and cat.domain_name = '" + CommonConstant.EDMS_CATEGORY_DOMAIN_DOC_TYPE + "' ) " +
                    "  LEFT JOIN language_text lang on cat.language_id = lang.ID " +
                    "where erMap.edms_doc_id=" + edmsDocId;

            printSql(sql);

            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();

            rs = stmt.executeQuery(sql);

            while ( rs.next( ) ){

                EdmsDocReferenceDocMapDTO edmsDocReferenceDocMapDTO = new EdmsDocReferenceDocMapDTO();

                edmsDocReferenceDocMapDTO.edmsDocId = rs.getLong( "edms_doc_id" );
                edmsDocReferenceDocMapDTO.referenceDocId = rs.getLong( "reference_doc_id" );
                edmsDocReferenceDocMapDTO.docNumber = rs.getLong( "document_no" );
                edmsDocReferenceDocMapDTO.docSubject = rs.getString( "document_subject" );

                edmsDocReferenceDocMapDTO.referenceNo = rs.getString( "reference_no" );
                edmsDocReferenceDocMapDTO.docSource = rs.getString( "source" );
                edmsDocReferenceDocMapDTO.docType = rs.getString( "docType" );

                Long docDateLng = rs.getLong( "document_date" );
                String docDateStr = DateUtils.getDateStringFromTimeStamp( docDateLng );
                edmsDocReferenceDocMapDTO.documentDate = docDateStr;

                edmsDocReferenceDocMapDTO.isDeleted = rs.getInt("isDeleted");
                edmsDocReferenceDocMapDTO.lastModificationTime = rs.getLong("lastModificationTime");

                edmsDocReferenceDocMapDTOList.add( edmsDocReferenceDocMapDTO );
            }

        }catch(Exception ex){

            ex.printStackTrace();
        }finally{
            try{
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e){}

            try{
                if (connection != null)
                {
                    DBMR.getInstance().freeConnection(connection);
                }
            }catch(Exception ex2){}
        }
        return edmsDocReferenceDocMapDTOList;
    }

    public Boolean insert(Long edmsDocId, Set<Long> refDocIdList ){

        if( refDocIdList == null || refDocIdList.size() == 0 )
            return false;

        Boolean success = false;
        Connection connection = null;
        PreparedStatement ps = null;

        long lastModificationTime = System.currentTimeMillis();

        try{
            connection = DBMW.getInstance().getConnection();

            if(connection == null)
            {
                System.out.println("nullconn");
            }

            StringBuilder sb = new StringBuilder();


            sb.append( "INSERT INTO " + tableName + " values " );

            for( Long refDocId: refDocIdList ){

                sb.append( "(?,?,?,?)," );
            }

            sb.setLength( sb.length() - 1 );

            String sql = sb.toString();

            ps = connection.prepareStatement( sql) ;

            int index = 1;

            for( Long refDocId: refDocIdList ){

                ps.setLong( index++, edmsDocId );
                ps.setLong( index++, refDocId );
                ps.setLong( index++, 0L );
                ps.setInt( index++, 0 );
            }

            System.out.println(ps);
            success = ps.execute();

        }catch(Exception ex){

            ex.printStackTrace();
        }finally{
            try{
                if (ps != null) {
                    ps.close();
                }
            } catch(Exception e){}
            try{
                if(connection != null)
                {
                    DBMW.getInstance().freeConnection(connection);
                }
            }catch(Exception ex2){}
        }
        return success;
    }

    public Integer deleteRefDoc( Long edmsDocId, Set<Long> refDocIdList ){

        Integer rowUpdated = 0;
        if( refDocIdList == null || refDocIdList.size() == 0 )
            return -1;

        Connection connection = null;
        PreparedStatement ps = null;

        long lastModificationTime = System.currentTimeMillis();
        try{
            connection = DBMW.getInstance().getConnection();

            StringBuilder sb = new StringBuilder();

            sb.append( "DELETE from " + tableName + " WHERE edms_doc_id = ? and reference_doc_id in (" );

            for( Long refDocId: refDocIdList ){

                sb.append( "?," );
            }

            sb.setLength( sb.length() -1 );
            sb.append( " )" );

            String sql = sb.toString();

            ps = connection.prepareStatement(sql);
            int index = 1;

            ps.setLong( index++, edmsDocId );

            for( Long refDocId: refDocIdList ){

                ps.setLong( index++, refDocId );
            }

            System.out.println(ps);
            rowUpdated = ps.executeUpdate();


        }catch(Exception ex){

            ex.printStackTrace();
        }finally{

            try{
                if (ps != null) {
                    ps.close();
                }
            } catch(Exception e){}
            try{
                if(connection != null)
                {
                    DBMW.getInstance().freeConnection(connection);
                }
            }catch(Exception ex2){}
        }

        return rowUpdated;
    }

    public Integer updateEdmsDocId( Long oldId, Long newId ) {

        if( oldId == null || newId == null )
            return -1;

        Integer rowUpdated = 0;
        Connection connection = null;
        PreparedStatement ps = null;

        long lastModificationTime = System.currentTimeMillis();

        try{
            connection = DBMW.getInstance().getConnection();

            if(connection == null)
            {
                System.out.println("nullconn");
            }

            String sql = "UPDATE " + tableName + " set edms_doc_id = ? where edms_doc_id = ? ";

            ps = connection.prepareStatement( sql) ;

            int index = 1;

            ps.setLong( index++, newId );
            ps.setLong( index++, oldId );

            System.out.println(ps);
            rowUpdated = ps.executeUpdate();

        }catch(Exception ex){

            ex.printStackTrace();
        }finally{
            try{
                if (ps != null) {
                    ps.close();
                }
            } catch(Exception e){}
            try{
                if(connection != null)
                {
                    DBMW.getInstance().freeConnection(connection);
                }
            }catch(Exception ex2){}
        }
        return rowUpdated;
    }
}
