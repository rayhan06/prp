package urine_examination_report;
import java.util.*; 
import util.*;


public class Urine_examination_reportMAPS extends CommonMaps
{	
	public Urine_examination_reportMAPS(String tableName)
	{
		
		java_allfield_type_map.put("common_lab_report_id".toLowerCase(), "Long");
		java_allfield_type_map.put("quantity".toLowerCase(), "String");
		java_allfield_type_map.put("color".toLowerCase(), "String");
		java_allfield_type_map.put("reaction".toLowerCase(), "String");
		java_allfield_type_map.put("gravity".toLowerCase(), "String");
		java_allfield_type_map.put("albumin".toLowerCase(), "String");
		java_allfield_type_map.put("sugar".toLowerCase(), "String");
		java_allfield_type_map.put("bile_salts".toLowerCase(), "String");
		java_allfield_type_map.put("puss_cells".toLowerCase(), "String");
		java_allfield_type_map.put("ep_cells".toLowerCase(), "String");
		java_allfield_type_map.put("rbc".toLowerCase(), "String");
		java_allfield_type_map.put("rbc_cast".toLowerCase(), "String");
		java_allfield_type_map.put("crystals".toLowerCase(), "String");
		java_allfield_type_map.put("other_things".toLowerCase(), "String");
		java_allfield_type_map.put("inserted_by_user_id".toLowerCase(), "Long");
		java_allfield_type_map.put("inserted_by_organogram_id".toLowerCase(), "Long");
		java_allfield_type_map.put("insertion_date".toLowerCase(), "Long");


		java_anyfield_search_map.put(tableName + ".common_lab_report_id".toLowerCase(), "Long");
		java_anyfield_search_map.put(tableName + ".quantity".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".color".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".reaction".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".gravity".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".albumin".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".sugar".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".bile_salts".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".puss_cells".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".ep_cells".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".rbc".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".rbc_cast".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".crystals".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".other_things".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".inserted_by_user_id".toLowerCase(), "Long");
		java_anyfield_search_map.put(tableName + ".inserted_by_organogram_id".toLowerCase(), "Long");
		java_anyfield_search_map.put(tableName + ".insertion_date".toLowerCase(), "Long");

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("commonLabReportId".toLowerCase(), "commonLabReportId".toLowerCase());
		java_DTO_map.put("quantity".toLowerCase(), "quantity".toLowerCase());
		java_DTO_map.put("color".toLowerCase(), "color".toLowerCase());
		java_DTO_map.put("reaction".toLowerCase(), "reaction".toLowerCase());
		java_DTO_map.put("gravity".toLowerCase(), "gravity".toLowerCase());
		java_DTO_map.put("albumin".toLowerCase(), "albumin".toLowerCase());
		java_DTO_map.put("sugar".toLowerCase(), "sugar".toLowerCase());
		java_DTO_map.put("bileSalts".toLowerCase(), "bileSalts".toLowerCase());
		java_DTO_map.put("pussCells".toLowerCase(), "pussCells".toLowerCase());
		java_DTO_map.put("epCells".toLowerCase(), "epCells".toLowerCase());
		java_DTO_map.put("rbc".toLowerCase(), "rbc".toLowerCase());
		java_DTO_map.put("rbcCast".toLowerCase(), "rbcCast".toLowerCase());
		java_DTO_map.put("crystals".toLowerCase(), "crystals".toLowerCase());
		java_DTO_map.put("otherThings".toLowerCase(), "otherThings".toLowerCase());
		java_DTO_map.put("insertedByUserId".toLowerCase(), "insertedByUserId".toLowerCase());
		java_DTO_map.put("insertedByOrganogramId".toLowerCase(), "insertedByOrganogramId".toLowerCase());
		java_DTO_map.put("insertionDate".toLowerCase(), "insertionDate".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("common_lab_report_id".toLowerCase(), "commonLabReportId".toLowerCase());
		java_SQL_map.put("quantity".toLowerCase(), "quantity".toLowerCase());
		java_SQL_map.put("color".toLowerCase(), "color".toLowerCase());
		java_SQL_map.put("reaction".toLowerCase(), "reaction".toLowerCase());
		java_SQL_map.put("gravity".toLowerCase(), "gravity".toLowerCase());
		java_SQL_map.put("albumin".toLowerCase(), "albumin".toLowerCase());
		java_SQL_map.put("sugar".toLowerCase(), "sugar".toLowerCase());
		java_SQL_map.put("bile_salts".toLowerCase(), "bileSalts".toLowerCase());
		java_SQL_map.put("puss_cells".toLowerCase(), "pussCells".toLowerCase());
		java_SQL_map.put("ep_cells".toLowerCase(), "epCells".toLowerCase());
		java_SQL_map.put("rbc".toLowerCase(), "rbc".toLowerCase());
		java_SQL_map.put("rbc_cast".toLowerCase(), "rbcCast".toLowerCase());
		java_SQL_map.put("crystals".toLowerCase(), "crystals".toLowerCase());
		java_SQL_map.put("other_things".toLowerCase(), "otherThings".toLowerCase());
		java_SQL_map.put("inserted_by_user_id".toLowerCase(), "insertedByUserId".toLowerCase());
		java_SQL_map.put("inserted_by_organogram_id".toLowerCase(), "insertedByOrganogramId".toLowerCase());
		java_SQL_map.put("insertion_date".toLowerCase(), "insertionDate".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Common Lab Report Id".toLowerCase(), "commonLabReportId".toLowerCase());
		java_Text_map.put("Quantity".toLowerCase(), "quantity".toLowerCase());
		java_Text_map.put("Color".toLowerCase(), "color".toLowerCase());
		java_Text_map.put("Reaction".toLowerCase(), "reaction".toLowerCase());
		java_Text_map.put("Gravity".toLowerCase(), "gravity".toLowerCase());
		java_Text_map.put("Albumin".toLowerCase(), "albumin".toLowerCase());
		java_Text_map.put("Sugar".toLowerCase(), "sugar".toLowerCase());
		java_Text_map.put("Bile Salts".toLowerCase(), "bileSalts".toLowerCase());
		java_Text_map.put("Puss Cells".toLowerCase(), "pussCells".toLowerCase());
		java_Text_map.put("Ep Cells".toLowerCase(), "epCells".toLowerCase());
		java_Text_map.put("Rbc".toLowerCase(), "rbc".toLowerCase());
		java_Text_map.put("Rbc Cast".toLowerCase(), "rbcCast".toLowerCase());
		java_Text_map.put("Crystals".toLowerCase(), "crystals".toLowerCase());
		java_Text_map.put("Other Things".toLowerCase(), "otherThings".toLowerCase());
		java_Text_map.put("Inserted By User Id".toLowerCase(), "insertedByUserId".toLowerCase());
		java_Text_map.put("Inserted By Organogram Id".toLowerCase(), "insertedByOrganogramId".toLowerCase());
		java_Text_map.put("Insertion Date".toLowerCase(), "insertionDate".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}