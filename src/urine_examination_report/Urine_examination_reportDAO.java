package urine_examination_report;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;
import sessionmanager.SessionConstants;
import java.sql.SQLException;


import org.apache.log4j.Logger;

import dbm.*;

import repository.RepositoryManager;

import util.*;
import pb.*;
import user.UserDTO;

public class Urine_examination_reportDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());
	
	
	public Urine_examination_reportDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";

		commonMaps = new Urine_examination_reportMAPS(tableName);
	}
	
	public Urine_examination_reportDAO()
	{
		this("urine_examination_report");		
	}
	
	public void set(PreparedStatement ps, Urine_examination_reportDTO urine_examination_reportDTO, boolean isInsert) throws SQLException
	{
		int index = 1;
		long lastModificationTime = System.currentTimeMillis();	
		if(isInsert)
		{
			ps.setObject(index++,urine_examination_reportDTO.iD);
		}
		ps.setObject(index++,urine_examination_reportDTO.commonLabReportId);
		ps.setObject(index++,urine_examination_reportDTO.quantity);
		ps.setObject(index++,urine_examination_reportDTO.color);
		ps.setObject(index++,urine_examination_reportDTO.reaction);
		ps.setObject(index++,urine_examination_reportDTO.gravity);
		ps.setObject(index++,urine_examination_reportDTO.albumin);
		ps.setObject(index++,urine_examination_reportDTO.sugar);
		ps.setObject(index++,urine_examination_reportDTO.bileSalts);
		ps.setObject(index++,urine_examination_reportDTO.pussCells);
		ps.setObject(index++,urine_examination_reportDTO.epCells);
		ps.setObject(index++,urine_examination_reportDTO.rbc);
		ps.setObject(index++,urine_examination_reportDTO.rbcCast);
		ps.setObject(index++,urine_examination_reportDTO.crystals);
		ps.setObject(index++,urine_examination_reportDTO.otherThings);
		ps.setObject(index++,urine_examination_reportDTO.insertedByUserId);
		ps.setObject(index++,urine_examination_reportDTO.insertedByOrganogramId);
		ps.setObject(index++,urine_examination_reportDTO.insertionDate);
		if(isInsert)
		{
			ps.setObject(index++, 0);
			ps.setObject(index++, lastModificationTime);
		}
	}
	
	public void get(Urine_examination_reportDTO urine_examination_reportDTO, ResultSet rs) throws SQLException
	{
		urine_examination_reportDTO.iD = rs.getLong("ID");
		urine_examination_reportDTO.commonLabReportId = rs.getLong("common_lab_report_id");
		urine_examination_reportDTO.quantity = rs.getString("quantity");
		urine_examination_reportDTO.color = rs.getString("color");
		urine_examination_reportDTO.reaction = rs.getString("reaction");
		urine_examination_reportDTO.gravity = rs.getString("gravity");
		urine_examination_reportDTO.albumin = rs.getString("albumin");
		urine_examination_reportDTO.sugar = rs.getString("sugar");
		urine_examination_reportDTO.bileSalts = rs.getString("bile_salts");
		urine_examination_reportDTO.pussCells = rs.getString("puss_cells");
		urine_examination_reportDTO.epCells = rs.getString("ep_cells");
		urine_examination_reportDTO.rbc = rs.getString("rbc");
		urine_examination_reportDTO.rbcCast = rs.getString("rbc_cast");
		urine_examination_reportDTO.crystals = rs.getString("crystals");
		urine_examination_reportDTO.otherThings = rs.getString("other_things");
		urine_examination_reportDTO.insertedByUserId = rs.getLong("inserted_by_user_id");
		urine_examination_reportDTO.insertedByOrganogramId = rs.getLong("inserted_by_organogram_id");
		urine_examination_reportDTO.insertionDate = rs.getLong("insertion_date");
		urine_examination_reportDTO.isDeleted = rs.getInt("isDeleted");
		urine_examination_reportDTO.lastModificationTime = rs.getLong("lastModificationTime");
	}
	
	
	
	public long add(CommonDTO commonDTO) throws Exception
	{
		
		Urine_examination_reportDTO urine_examination_reportDTO = (Urine_examination_reportDTO)commonDTO;
		
		Connection connection = null;
		PreparedStatement ps = null;

		long lastModificationTime = System.currentTimeMillis();	

		try{
			connection = DBMW.getInstance().getConnection();
			
			if(connection == null)
			{
				System.out.println("nullconn");
			}

			urine_examination_reportDTO.iD = DBMW.getInstance().getNextSequenceId(tableName);

			String sql = "INSERT INTO " + tableName;
			
			sql += " (";
			sql += "ID";
			sql += ", ";
			sql += "common_lab_report_id";
			sql += ", ";
			sql += "quantity";
			sql += ", ";
			sql += "color";
			sql += ", ";
			sql += "reaction";
			sql += ", ";
			sql += "gravity";
			sql += ", ";
			sql += "albumin";
			sql += ", ";
			sql += "sugar";
			sql += ", ";
			sql += "bile_salts";
			sql += ", ";
			sql += "puss_cells";
			sql += ", ";
			sql += "ep_cells";
			sql += ", ";
			sql += "rbc";
			sql += ", ";
			sql += "rbc_cast";
			sql += ", ";
			sql += "crystals";
			sql += ", ";
			sql += "other_things";
			sql += ", ";
			sql += "inserted_by_user_id";
			sql += ", ";
			sql += "inserted_by_organogram_id";
			sql += ", ";
			sql += "insertion_date";
			sql += ", ";
			sql += "isDeleted";
			sql += ", ";
			sql += "lastModificationTime";
		
			sql += ")";
			
			
            sql += " VALUES(";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			
			sql += ")";
				
			ps = connection.prepareStatement(sql);
			set(ps, urine_examination_reportDTO, true);
			ps.execute();
			
			
			recordUpdateTime(connection, lastModificationTime);

		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if (ps != null) {
					ps.close();
				}
			} catch(Exception e){}
			try{
				if(connection != null)
				{
					DBMW.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		return urine_examination_reportDTO.iD;		
	}
		
	

	//need another getter for repository
	public CommonDTO getDTOByID (long ID) throws Exception
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Urine_examination_reportDTO urine_examination_reportDTO = null;
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
			
            sql += " WHERE ID=" + ID;
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			if(rs.next()){
				urine_examination_reportDTO = new Urine_examination_reportDTO();

				get(urine_examination_reportDTO, rs);

			}			
			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return urine_examination_reportDTO;
	}
	
	public Urine_examination_reportDTO getDTOByCommonLabReportId (long CommonLabReportId)
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Urine_examination_reportDTO urine_examination_reportDTO = null;
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
			
            sql += " WHERE common_lab_report_id =" + CommonLabReportId;
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			if(rs.next()){
				urine_examination_reportDTO = new Urine_examination_reportDTO();

				get(urine_examination_reportDTO, rs);

			}			
			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return urine_examination_reportDTO;
	}
	
	public long update(CommonDTO commonDTO) throws Exception
	{		
		Urine_examination_reportDTO urine_examination_reportDTO = (Urine_examination_reportDTO)commonDTO;
		
		Connection connection = null;
		PreparedStatement ps = null;

		long lastModificationTime = System.currentTimeMillis();	
		try{
			connection = DBMW.getInstance().getConnection();

			String sql = "UPDATE " + tableName;
			
			sql += " SET ";
			sql += "common_lab_report_id=?";
			sql += ", ";
			sql += "quantity=?";
			sql += ", ";
			sql += "color=?";
			sql += ", ";
			sql += "reaction=?";
			sql += ", ";
			sql += "gravity=?";
			sql += ", ";
			sql += "albumin=?";
			sql += ", ";
			sql += "sugar=?";
			sql += ", ";
			sql += "bile_salts=?";
			sql += ", ";
			sql += "puss_cells=?";
			sql += ", ";
			sql += "ep_cells=?";
			sql += ", ";
			sql += "rbc=?";
			sql += ", ";
			sql += "rbc_cast=?";
			sql += ", ";
			sql += "crystals=?";
			sql += ", ";
			sql += "other_things=?";
			sql += ", ";
			sql += "inserted_by_user_id=?";
			sql += ", ";
			sql += "inserted_by_organogram_id=?";
			sql += ", ";
			sql += "insertion_date=?";
			sql += ", lastModificationTime = "	+ lastModificationTime + "";
            sql += " WHERE ID = " + urine_examination_reportDTO.iD;
				

			ps = connection.prepareStatement(sql);
			set(ps, urine_examination_reportDTO, false);
			ps.executeUpdate();
			

			recordUpdateTime(connection, lastModificationTime);

						
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if (ps != null) {
					ps.close();
				}
			} catch(Exception e){}
			try{
				if(connection != null)
				{
					DBMW.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		return urine_examination_reportDTO.iD;
	}
	
	
	public List<Urine_examination_reportDTO> getDTOs(Collection recordIDs){
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Urine_examination_reportDTO urine_examination_reportDTO = null;
		List<Urine_examination_reportDTO> urine_examination_reportDTOList = new ArrayList<>();
		if(recordIDs.isEmpty()){
			return urine_examination_reportDTOList;
		}
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
            
            sql += " WHERE ID IN ( ";

			for(int i = 0;i<recordIDs.size();i++){
				if(i!=0){
					sql+=",";
				}
				sql+=((ArrayList)recordIDs).get(i);
			}
			sql+=")  order by lastModificationTime desc";
			
			printSql(sql);
			
			logger.debug("sql " + sql);
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				urine_examination_reportDTO = new Urine_examination_reportDTO();
				get(urine_examination_reportDTO, rs);
				System.out.println("got this DTO: " + urine_examination_reportDTO);
				
				urine_examination_reportDTOList.add(urine_examination_reportDTO);

			}			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return urine_examination_reportDTOList;
	
	}
	
	

	
	
	
	//add repository
	public List<Urine_examination_reportDTO> getAllUrine_examination_report (boolean isFirstReload)
    {
		List<Urine_examination_reportDTO> urine_examination_reportDTOList = new ArrayList<>();

		String sql = "SELECT * FROM urine_examination_report";
		sql += " WHERE ";
	

		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by urine_examination_report.lastModificationTime desc";
		printSql(sql);
		
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;

		
		try{
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();
			rs = stmt.executeQuery(sql);
			

			while(rs.next()){
				Urine_examination_reportDTO urine_examination_reportDTO = new Urine_examination_reportDTO();
				get(urine_examination_reportDTO, rs);
				
				urine_examination_reportDTOList.add(urine_examination_reportDTO);
			}			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}

		return urine_examination_reportDTOList;
    }

	
	public List<Urine_examination_reportDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO)
	{
		return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
	}

	
	public List<Urine_examination_reportDTO> getDTOs(Hashtable  p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
			String filter, boolean tableHasJobCat)
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		List<Urine_examination_reportDTO> urine_examination_reportDTOList = new ArrayList<>();

		try{
			
			String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat);
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				Urine_examination_reportDTO urine_examination_reportDTO = new Urine_examination_reportDTO();
				get(urine_examination_reportDTO, rs);
				
				urine_examination_reportDTOList.add(urine_examination_reportDTO);
			}					
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return urine_examination_reportDTOList;
	
	}
				
}
	