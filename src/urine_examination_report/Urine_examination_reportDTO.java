package urine_examination_report;
import java.util.*; 
import util.*; 


public class Urine_examination_reportDTO extends CommonDTO
{

	public long commonLabReportId = 0;
    public String quantity = "";
    public String color = "";
    public String reaction = "";
    public String gravity = "";
    public String albumin = "";
    public String sugar = "";
    public String bileSalts = "";
    public String pussCells = "";
    public String epCells = "";
    public String rbc = "";
    public String rbcCast = "";
    public String crystals = "";
    public String otherThings = "";
	public long insertedByUserId = 0;
	public long insertedByOrganogramId = 0;
	public long insertionDate = 0;
	
	
    @Override
	public String toString() {
            return "$Urine_examination_reportDTO[" +
            " iD = " + iD +
            " commonLabReportId = " + commonLabReportId +
            " quantity = " + quantity +
            " color = " + color +
            " reaction = " + reaction +
            " gravity = " + gravity +
            " albumin = " + albumin +
            " sugar = " + sugar +
            " bileSalts = " + bileSalts +
            " pussCells = " + pussCells +
            " epCells = " + epCells +
            " rbc = " + rbc +
            " rbcCast = " + rbcCast +
            " crystals = " + crystals +
            " otherThings = " + otherThings +
            " insertedByUserId = " + insertedByUserId +
            " insertedByOrganogramId = " + insertedByOrganogramId +
            " insertionDate = " + insertionDate +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}