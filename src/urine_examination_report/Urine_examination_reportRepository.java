package urine_examination_report;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class Urine_examination_reportRepository implements Repository {
	Urine_examination_reportDAO urine_examination_reportDAO = null;
	
	public void setDAO(Urine_examination_reportDAO urine_examination_reportDAO)
	{
		this.urine_examination_reportDAO = urine_examination_reportDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Urine_examination_reportRepository.class);
	Map<Long, Urine_examination_reportDTO>mapOfUrine_examination_reportDTOToiD;
	Map<Long, Set<Urine_examination_reportDTO> >mapOfUrine_examination_reportDTOTocommonLabReportId;
	Map<String, Set<Urine_examination_reportDTO> >mapOfUrine_examination_reportDTOToquantity;
	Map<String, Set<Urine_examination_reportDTO> >mapOfUrine_examination_reportDTOTocolor;
	Map<String, Set<Urine_examination_reportDTO> >mapOfUrine_examination_reportDTOToreaction;
	Map<String, Set<Urine_examination_reportDTO> >mapOfUrine_examination_reportDTOTogravity;
	Map<String, Set<Urine_examination_reportDTO> >mapOfUrine_examination_reportDTOToalbumin;
	Map<String, Set<Urine_examination_reportDTO> >mapOfUrine_examination_reportDTOTosugar;
	Map<String, Set<Urine_examination_reportDTO> >mapOfUrine_examination_reportDTOTobileSalts;
	Map<String, Set<Urine_examination_reportDTO> >mapOfUrine_examination_reportDTOTopussCells;
	Map<String, Set<Urine_examination_reportDTO> >mapOfUrine_examination_reportDTOToepCells;
	Map<String, Set<Urine_examination_reportDTO> >mapOfUrine_examination_reportDTOTorbc;
	Map<String, Set<Urine_examination_reportDTO> >mapOfUrine_examination_reportDTOTorbcCast;
	Map<String, Set<Urine_examination_reportDTO> >mapOfUrine_examination_reportDTOTocrystals;
	Map<String, Set<Urine_examination_reportDTO> >mapOfUrine_examination_reportDTOTootherThings;
	Map<Long, Set<Urine_examination_reportDTO> >mapOfUrine_examination_reportDTOToinsertedByUserId;
	Map<Long, Set<Urine_examination_reportDTO> >mapOfUrine_examination_reportDTOToinsertedByOrganogramId;
	Map<Long, Set<Urine_examination_reportDTO> >mapOfUrine_examination_reportDTOToinsertionDate;
	Map<Long, Set<Urine_examination_reportDTO> >mapOfUrine_examination_reportDTOTolastModificationTime;


	static Urine_examination_reportRepository instance = null;  
	private Urine_examination_reportRepository(){
		mapOfUrine_examination_reportDTOToiD = new ConcurrentHashMap<>();
		mapOfUrine_examination_reportDTOTocommonLabReportId = new ConcurrentHashMap<>();
		mapOfUrine_examination_reportDTOToquantity = new ConcurrentHashMap<>();
		mapOfUrine_examination_reportDTOTocolor = new ConcurrentHashMap<>();
		mapOfUrine_examination_reportDTOToreaction = new ConcurrentHashMap<>();
		mapOfUrine_examination_reportDTOTogravity = new ConcurrentHashMap<>();
		mapOfUrine_examination_reportDTOToalbumin = new ConcurrentHashMap<>();
		mapOfUrine_examination_reportDTOTosugar = new ConcurrentHashMap<>();
		mapOfUrine_examination_reportDTOTobileSalts = new ConcurrentHashMap<>();
		mapOfUrine_examination_reportDTOTopussCells = new ConcurrentHashMap<>();
		mapOfUrine_examination_reportDTOToepCells = new ConcurrentHashMap<>();
		mapOfUrine_examination_reportDTOTorbc = new ConcurrentHashMap<>();
		mapOfUrine_examination_reportDTOTorbcCast = new ConcurrentHashMap<>();
		mapOfUrine_examination_reportDTOTocrystals = new ConcurrentHashMap<>();
		mapOfUrine_examination_reportDTOTootherThings = new ConcurrentHashMap<>();
		mapOfUrine_examination_reportDTOToinsertedByUserId = new ConcurrentHashMap<>();
		mapOfUrine_examination_reportDTOToinsertedByOrganogramId = new ConcurrentHashMap<>();
		mapOfUrine_examination_reportDTOToinsertionDate = new ConcurrentHashMap<>();
		mapOfUrine_examination_reportDTOTolastModificationTime = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Urine_examination_reportRepository getInstance(){
		if (instance == null){
			instance = new Urine_examination_reportRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(urine_examination_reportDAO == null)
		{
			return;
		}
		try {
			List<Urine_examination_reportDTO> urine_examination_reportDTOs = urine_examination_reportDAO.getAllUrine_examination_report(reloadAll);
			for(Urine_examination_reportDTO urine_examination_reportDTO : urine_examination_reportDTOs) {
				Urine_examination_reportDTO oldUrine_examination_reportDTO = getUrine_examination_reportDTOByID(urine_examination_reportDTO.iD);
				if( oldUrine_examination_reportDTO != null ) {
					mapOfUrine_examination_reportDTOToiD.remove(oldUrine_examination_reportDTO.iD);
				
					if(mapOfUrine_examination_reportDTOTocommonLabReportId.containsKey(oldUrine_examination_reportDTO.commonLabReportId)) {
						mapOfUrine_examination_reportDTOTocommonLabReportId.get(oldUrine_examination_reportDTO.commonLabReportId).remove(oldUrine_examination_reportDTO);
					}
					if(mapOfUrine_examination_reportDTOTocommonLabReportId.get(oldUrine_examination_reportDTO.commonLabReportId).isEmpty()) {
						mapOfUrine_examination_reportDTOTocommonLabReportId.remove(oldUrine_examination_reportDTO.commonLabReportId);
					}
					
					if(mapOfUrine_examination_reportDTOToquantity.containsKey(oldUrine_examination_reportDTO.quantity)) {
						mapOfUrine_examination_reportDTOToquantity.get(oldUrine_examination_reportDTO.quantity).remove(oldUrine_examination_reportDTO);
					}
					if(mapOfUrine_examination_reportDTOToquantity.get(oldUrine_examination_reportDTO.quantity).isEmpty()) {
						mapOfUrine_examination_reportDTOToquantity.remove(oldUrine_examination_reportDTO.quantity);
					}
					
					if(mapOfUrine_examination_reportDTOTocolor.containsKey(oldUrine_examination_reportDTO.color)) {
						mapOfUrine_examination_reportDTOTocolor.get(oldUrine_examination_reportDTO.color).remove(oldUrine_examination_reportDTO);
					}
					if(mapOfUrine_examination_reportDTOTocolor.get(oldUrine_examination_reportDTO.color).isEmpty()) {
						mapOfUrine_examination_reportDTOTocolor.remove(oldUrine_examination_reportDTO.color);
					}
					
					if(mapOfUrine_examination_reportDTOToreaction.containsKey(oldUrine_examination_reportDTO.reaction)) {
						mapOfUrine_examination_reportDTOToreaction.get(oldUrine_examination_reportDTO.reaction).remove(oldUrine_examination_reportDTO);
					}
					if(mapOfUrine_examination_reportDTOToreaction.get(oldUrine_examination_reportDTO.reaction).isEmpty()) {
						mapOfUrine_examination_reportDTOToreaction.remove(oldUrine_examination_reportDTO.reaction);
					}
					
					if(mapOfUrine_examination_reportDTOTogravity.containsKey(oldUrine_examination_reportDTO.gravity)) {
						mapOfUrine_examination_reportDTOTogravity.get(oldUrine_examination_reportDTO.gravity).remove(oldUrine_examination_reportDTO);
					}
					if(mapOfUrine_examination_reportDTOTogravity.get(oldUrine_examination_reportDTO.gravity).isEmpty()) {
						mapOfUrine_examination_reportDTOTogravity.remove(oldUrine_examination_reportDTO.gravity);
					}
					
					if(mapOfUrine_examination_reportDTOToalbumin.containsKey(oldUrine_examination_reportDTO.albumin)) {
						mapOfUrine_examination_reportDTOToalbumin.get(oldUrine_examination_reportDTO.albumin).remove(oldUrine_examination_reportDTO);
					}
					if(mapOfUrine_examination_reportDTOToalbumin.get(oldUrine_examination_reportDTO.albumin).isEmpty()) {
						mapOfUrine_examination_reportDTOToalbumin.remove(oldUrine_examination_reportDTO.albumin);
					}
					
					if(mapOfUrine_examination_reportDTOTosugar.containsKey(oldUrine_examination_reportDTO.sugar)) {
						mapOfUrine_examination_reportDTOTosugar.get(oldUrine_examination_reportDTO.sugar).remove(oldUrine_examination_reportDTO);
					}
					if(mapOfUrine_examination_reportDTOTosugar.get(oldUrine_examination_reportDTO.sugar).isEmpty()) {
						mapOfUrine_examination_reportDTOTosugar.remove(oldUrine_examination_reportDTO.sugar);
					}
					
					if(mapOfUrine_examination_reportDTOTobileSalts.containsKey(oldUrine_examination_reportDTO.bileSalts)) {
						mapOfUrine_examination_reportDTOTobileSalts.get(oldUrine_examination_reportDTO.bileSalts).remove(oldUrine_examination_reportDTO);
					}
					if(mapOfUrine_examination_reportDTOTobileSalts.get(oldUrine_examination_reportDTO.bileSalts).isEmpty()) {
						mapOfUrine_examination_reportDTOTobileSalts.remove(oldUrine_examination_reportDTO.bileSalts);
					}
					
					if(mapOfUrine_examination_reportDTOTopussCells.containsKey(oldUrine_examination_reportDTO.pussCells)) {
						mapOfUrine_examination_reportDTOTopussCells.get(oldUrine_examination_reportDTO.pussCells).remove(oldUrine_examination_reportDTO);
					}
					if(mapOfUrine_examination_reportDTOTopussCells.get(oldUrine_examination_reportDTO.pussCells).isEmpty()) {
						mapOfUrine_examination_reportDTOTopussCells.remove(oldUrine_examination_reportDTO.pussCells);
					}
					
					if(mapOfUrine_examination_reportDTOToepCells.containsKey(oldUrine_examination_reportDTO.epCells)) {
						mapOfUrine_examination_reportDTOToepCells.get(oldUrine_examination_reportDTO.epCells).remove(oldUrine_examination_reportDTO);
					}
					if(mapOfUrine_examination_reportDTOToepCells.get(oldUrine_examination_reportDTO.epCells).isEmpty()) {
						mapOfUrine_examination_reportDTOToepCells.remove(oldUrine_examination_reportDTO.epCells);
					}
					
					if(mapOfUrine_examination_reportDTOTorbc.containsKey(oldUrine_examination_reportDTO.rbc)) {
						mapOfUrine_examination_reportDTOTorbc.get(oldUrine_examination_reportDTO.rbc).remove(oldUrine_examination_reportDTO);
					}
					if(mapOfUrine_examination_reportDTOTorbc.get(oldUrine_examination_reportDTO.rbc).isEmpty()) {
						mapOfUrine_examination_reportDTOTorbc.remove(oldUrine_examination_reportDTO.rbc);
					}
					
					if(mapOfUrine_examination_reportDTOTorbcCast.containsKey(oldUrine_examination_reportDTO.rbcCast)) {
						mapOfUrine_examination_reportDTOTorbcCast.get(oldUrine_examination_reportDTO.rbcCast).remove(oldUrine_examination_reportDTO);
					}
					if(mapOfUrine_examination_reportDTOTorbcCast.get(oldUrine_examination_reportDTO.rbcCast).isEmpty()) {
						mapOfUrine_examination_reportDTOTorbcCast.remove(oldUrine_examination_reportDTO.rbcCast);
					}
					
					if(mapOfUrine_examination_reportDTOTocrystals.containsKey(oldUrine_examination_reportDTO.crystals)) {
						mapOfUrine_examination_reportDTOTocrystals.get(oldUrine_examination_reportDTO.crystals).remove(oldUrine_examination_reportDTO);
					}
					if(mapOfUrine_examination_reportDTOTocrystals.get(oldUrine_examination_reportDTO.crystals).isEmpty()) {
						mapOfUrine_examination_reportDTOTocrystals.remove(oldUrine_examination_reportDTO.crystals);
					}
					
					if(mapOfUrine_examination_reportDTOTootherThings.containsKey(oldUrine_examination_reportDTO.otherThings)) {
						mapOfUrine_examination_reportDTOTootherThings.get(oldUrine_examination_reportDTO.otherThings).remove(oldUrine_examination_reportDTO);
					}
					if(mapOfUrine_examination_reportDTOTootherThings.get(oldUrine_examination_reportDTO.otherThings).isEmpty()) {
						mapOfUrine_examination_reportDTOTootherThings.remove(oldUrine_examination_reportDTO.otherThings);
					}
					
					if(mapOfUrine_examination_reportDTOToinsertedByUserId.containsKey(oldUrine_examination_reportDTO.insertedByUserId)) {
						mapOfUrine_examination_reportDTOToinsertedByUserId.get(oldUrine_examination_reportDTO.insertedByUserId).remove(oldUrine_examination_reportDTO);
					}
					if(mapOfUrine_examination_reportDTOToinsertedByUserId.get(oldUrine_examination_reportDTO.insertedByUserId).isEmpty()) {
						mapOfUrine_examination_reportDTOToinsertedByUserId.remove(oldUrine_examination_reportDTO.insertedByUserId);
					}
					
					if(mapOfUrine_examination_reportDTOToinsertedByOrganogramId.containsKey(oldUrine_examination_reportDTO.insertedByOrganogramId)) {
						mapOfUrine_examination_reportDTOToinsertedByOrganogramId.get(oldUrine_examination_reportDTO.insertedByOrganogramId).remove(oldUrine_examination_reportDTO);
					}
					if(mapOfUrine_examination_reportDTOToinsertedByOrganogramId.get(oldUrine_examination_reportDTO.insertedByOrganogramId).isEmpty()) {
						mapOfUrine_examination_reportDTOToinsertedByOrganogramId.remove(oldUrine_examination_reportDTO.insertedByOrganogramId);
					}
					
					if(mapOfUrine_examination_reportDTOToinsertionDate.containsKey(oldUrine_examination_reportDTO.insertionDate)) {
						mapOfUrine_examination_reportDTOToinsertionDate.get(oldUrine_examination_reportDTO.insertionDate).remove(oldUrine_examination_reportDTO);
					}
					if(mapOfUrine_examination_reportDTOToinsertionDate.get(oldUrine_examination_reportDTO.insertionDate).isEmpty()) {
						mapOfUrine_examination_reportDTOToinsertionDate.remove(oldUrine_examination_reportDTO.insertionDate);
					}
					
					if(mapOfUrine_examination_reportDTOTolastModificationTime.containsKey(oldUrine_examination_reportDTO.lastModificationTime)) {
						mapOfUrine_examination_reportDTOTolastModificationTime.get(oldUrine_examination_reportDTO.lastModificationTime).remove(oldUrine_examination_reportDTO);
					}
					if(mapOfUrine_examination_reportDTOTolastModificationTime.get(oldUrine_examination_reportDTO.lastModificationTime).isEmpty()) {
						mapOfUrine_examination_reportDTOTolastModificationTime.remove(oldUrine_examination_reportDTO.lastModificationTime);
					}
					
					
				}
				if(urine_examination_reportDTO.isDeleted == 0) 
				{
					
					mapOfUrine_examination_reportDTOToiD.put(urine_examination_reportDTO.iD, urine_examination_reportDTO);
				
					if( ! mapOfUrine_examination_reportDTOTocommonLabReportId.containsKey(urine_examination_reportDTO.commonLabReportId)) {
						mapOfUrine_examination_reportDTOTocommonLabReportId.put(urine_examination_reportDTO.commonLabReportId, new HashSet<>());
					}
					mapOfUrine_examination_reportDTOTocommonLabReportId.get(urine_examination_reportDTO.commonLabReportId).add(urine_examination_reportDTO);
					
					if( ! mapOfUrine_examination_reportDTOToquantity.containsKey(urine_examination_reportDTO.quantity)) {
						mapOfUrine_examination_reportDTOToquantity.put(urine_examination_reportDTO.quantity, new HashSet<>());
					}
					mapOfUrine_examination_reportDTOToquantity.get(urine_examination_reportDTO.quantity).add(urine_examination_reportDTO);
					
					if( ! mapOfUrine_examination_reportDTOTocolor.containsKey(urine_examination_reportDTO.color)) {
						mapOfUrine_examination_reportDTOTocolor.put(urine_examination_reportDTO.color, new HashSet<>());
					}
					mapOfUrine_examination_reportDTOTocolor.get(urine_examination_reportDTO.color).add(urine_examination_reportDTO);
					
					if( ! mapOfUrine_examination_reportDTOToreaction.containsKey(urine_examination_reportDTO.reaction)) {
						mapOfUrine_examination_reportDTOToreaction.put(urine_examination_reportDTO.reaction, new HashSet<>());
					}
					mapOfUrine_examination_reportDTOToreaction.get(urine_examination_reportDTO.reaction).add(urine_examination_reportDTO);
					
					if( ! mapOfUrine_examination_reportDTOTogravity.containsKey(urine_examination_reportDTO.gravity)) {
						mapOfUrine_examination_reportDTOTogravity.put(urine_examination_reportDTO.gravity, new HashSet<>());
					}
					mapOfUrine_examination_reportDTOTogravity.get(urine_examination_reportDTO.gravity).add(urine_examination_reportDTO);
					
					if( ! mapOfUrine_examination_reportDTOToalbumin.containsKey(urine_examination_reportDTO.albumin)) {
						mapOfUrine_examination_reportDTOToalbumin.put(urine_examination_reportDTO.albumin, new HashSet<>());
					}
					mapOfUrine_examination_reportDTOToalbumin.get(urine_examination_reportDTO.albumin).add(urine_examination_reportDTO);
					
					if( ! mapOfUrine_examination_reportDTOTosugar.containsKey(urine_examination_reportDTO.sugar)) {
						mapOfUrine_examination_reportDTOTosugar.put(urine_examination_reportDTO.sugar, new HashSet<>());
					}
					mapOfUrine_examination_reportDTOTosugar.get(urine_examination_reportDTO.sugar).add(urine_examination_reportDTO);
					
					if( ! mapOfUrine_examination_reportDTOTobileSalts.containsKey(urine_examination_reportDTO.bileSalts)) {
						mapOfUrine_examination_reportDTOTobileSalts.put(urine_examination_reportDTO.bileSalts, new HashSet<>());
					}
					mapOfUrine_examination_reportDTOTobileSalts.get(urine_examination_reportDTO.bileSalts).add(urine_examination_reportDTO);
					
					if( ! mapOfUrine_examination_reportDTOTopussCells.containsKey(urine_examination_reportDTO.pussCells)) {
						mapOfUrine_examination_reportDTOTopussCells.put(urine_examination_reportDTO.pussCells, new HashSet<>());
					}
					mapOfUrine_examination_reportDTOTopussCells.get(urine_examination_reportDTO.pussCells).add(urine_examination_reportDTO);
					
					if( ! mapOfUrine_examination_reportDTOToepCells.containsKey(urine_examination_reportDTO.epCells)) {
						mapOfUrine_examination_reportDTOToepCells.put(urine_examination_reportDTO.epCells, new HashSet<>());
					}
					mapOfUrine_examination_reportDTOToepCells.get(urine_examination_reportDTO.epCells).add(urine_examination_reportDTO);
					
					if( ! mapOfUrine_examination_reportDTOTorbc.containsKey(urine_examination_reportDTO.rbc)) {
						mapOfUrine_examination_reportDTOTorbc.put(urine_examination_reportDTO.rbc, new HashSet<>());
					}
					mapOfUrine_examination_reportDTOTorbc.get(urine_examination_reportDTO.rbc).add(urine_examination_reportDTO);
					
					if( ! mapOfUrine_examination_reportDTOTorbcCast.containsKey(urine_examination_reportDTO.rbcCast)) {
						mapOfUrine_examination_reportDTOTorbcCast.put(urine_examination_reportDTO.rbcCast, new HashSet<>());
					}
					mapOfUrine_examination_reportDTOTorbcCast.get(urine_examination_reportDTO.rbcCast).add(urine_examination_reportDTO);
					
					if( ! mapOfUrine_examination_reportDTOTocrystals.containsKey(urine_examination_reportDTO.crystals)) {
						mapOfUrine_examination_reportDTOTocrystals.put(urine_examination_reportDTO.crystals, new HashSet<>());
					}
					mapOfUrine_examination_reportDTOTocrystals.get(urine_examination_reportDTO.crystals).add(urine_examination_reportDTO);
					
					if( ! mapOfUrine_examination_reportDTOTootherThings.containsKey(urine_examination_reportDTO.otherThings)) {
						mapOfUrine_examination_reportDTOTootherThings.put(urine_examination_reportDTO.otherThings, new HashSet<>());
					}
					mapOfUrine_examination_reportDTOTootherThings.get(urine_examination_reportDTO.otherThings).add(urine_examination_reportDTO);
					
					if( ! mapOfUrine_examination_reportDTOToinsertedByUserId.containsKey(urine_examination_reportDTO.insertedByUserId)) {
						mapOfUrine_examination_reportDTOToinsertedByUserId.put(urine_examination_reportDTO.insertedByUserId, new HashSet<>());
					}
					mapOfUrine_examination_reportDTOToinsertedByUserId.get(urine_examination_reportDTO.insertedByUserId).add(urine_examination_reportDTO);
					
					if( ! mapOfUrine_examination_reportDTOToinsertedByOrganogramId.containsKey(urine_examination_reportDTO.insertedByOrganogramId)) {
						mapOfUrine_examination_reportDTOToinsertedByOrganogramId.put(urine_examination_reportDTO.insertedByOrganogramId, new HashSet<>());
					}
					mapOfUrine_examination_reportDTOToinsertedByOrganogramId.get(urine_examination_reportDTO.insertedByOrganogramId).add(urine_examination_reportDTO);
					
					if( ! mapOfUrine_examination_reportDTOToinsertionDate.containsKey(urine_examination_reportDTO.insertionDate)) {
						mapOfUrine_examination_reportDTOToinsertionDate.put(urine_examination_reportDTO.insertionDate, new HashSet<>());
					}
					mapOfUrine_examination_reportDTOToinsertionDate.get(urine_examination_reportDTO.insertionDate).add(urine_examination_reportDTO);
					
					if( ! mapOfUrine_examination_reportDTOTolastModificationTime.containsKey(urine_examination_reportDTO.lastModificationTime)) {
						mapOfUrine_examination_reportDTOTolastModificationTime.put(urine_examination_reportDTO.lastModificationTime, new HashSet<>());
					}
					mapOfUrine_examination_reportDTOTolastModificationTime.get(urine_examination_reportDTO.lastModificationTime).add(urine_examination_reportDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Urine_examination_reportDTO> getUrine_examination_reportList() {
		List <Urine_examination_reportDTO> urine_examination_reports = new ArrayList<Urine_examination_reportDTO>(this.mapOfUrine_examination_reportDTOToiD.values());
		return urine_examination_reports;
	}
	
	
	public Urine_examination_reportDTO getUrine_examination_reportDTOByID( long ID){
		return mapOfUrine_examination_reportDTOToiD.get(ID);
	}
	
	
	public List<Urine_examination_reportDTO> getUrine_examination_reportDTOBycommon_lab_report_id(long common_lab_report_id) {
		return new ArrayList<>( mapOfUrine_examination_reportDTOTocommonLabReportId.getOrDefault(common_lab_report_id,new HashSet<>()));
	}
	
	
	public List<Urine_examination_reportDTO> getUrine_examination_reportDTOByquantity(String quantity) {
		return new ArrayList<>( mapOfUrine_examination_reportDTOToquantity.getOrDefault(quantity,new HashSet<>()));
	}
	
	
	public List<Urine_examination_reportDTO> getUrine_examination_reportDTOBycolor(String color) {
		return new ArrayList<>( mapOfUrine_examination_reportDTOTocolor.getOrDefault(color,new HashSet<>()));
	}
	
	
	public List<Urine_examination_reportDTO> getUrine_examination_reportDTOByreaction(String reaction) {
		return new ArrayList<>( mapOfUrine_examination_reportDTOToreaction.getOrDefault(reaction,new HashSet<>()));
	}
	
	
	public List<Urine_examination_reportDTO> getUrine_examination_reportDTOBygravity(String gravity) {
		return new ArrayList<>( mapOfUrine_examination_reportDTOTogravity.getOrDefault(gravity,new HashSet<>()));
	}
	
	
	public List<Urine_examination_reportDTO> getUrine_examination_reportDTOByalbumin(String albumin) {
		return new ArrayList<>( mapOfUrine_examination_reportDTOToalbumin.getOrDefault(albumin,new HashSet<>()));
	}
	
	
	public List<Urine_examination_reportDTO> getUrine_examination_reportDTOBysugar(String sugar) {
		return new ArrayList<>( mapOfUrine_examination_reportDTOTosugar.getOrDefault(sugar,new HashSet<>()));
	}
	
	
	public List<Urine_examination_reportDTO> getUrine_examination_reportDTOBybile_salts(String bile_salts) {
		return new ArrayList<>( mapOfUrine_examination_reportDTOTobileSalts.getOrDefault(bile_salts,new HashSet<>()));
	}
	
	
	public List<Urine_examination_reportDTO> getUrine_examination_reportDTOBypuss_cells(String puss_cells) {
		return new ArrayList<>( mapOfUrine_examination_reportDTOTopussCells.getOrDefault(puss_cells,new HashSet<>()));
	}
	
	
	public List<Urine_examination_reportDTO> getUrine_examination_reportDTOByep_cells(String ep_cells) {
		return new ArrayList<>( mapOfUrine_examination_reportDTOToepCells.getOrDefault(ep_cells,new HashSet<>()));
	}
	
	
	public List<Urine_examination_reportDTO> getUrine_examination_reportDTOByrbc(String rbc) {
		return new ArrayList<>( mapOfUrine_examination_reportDTOTorbc.getOrDefault(rbc,new HashSet<>()));
	}
	
	
	public List<Urine_examination_reportDTO> getUrine_examination_reportDTOByrbc_cast(String rbc_cast) {
		return new ArrayList<>( mapOfUrine_examination_reportDTOTorbcCast.getOrDefault(rbc_cast,new HashSet<>()));
	}
	
	
	public List<Urine_examination_reportDTO> getUrine_examination_reportDTOBycrystals(String crystals) {
		return new ArrayList<>( mapOfUrine_examination_reportDTOTocrystals.getOrDefault(crystals,new HashSet<>()));
	}
	
	
	public List<Urine_examination_reportDTO> getUrine_examination_reportDTOByother_things(String other_things) {
		return new ArrayList<>( mapOfUrine_examination_reportDTOTootherThings.getOrDefault(other_things,new HashSet<>()));
	}
	
	
	public List<Urine_examination_reportDTO> getUrine_examination_reportDTOByinserted_by_user_id(long inserted_by_user_id) {
		return new ArrayList<>( mapOfUrine_examination_reportDTOToinsertedByUserId.getOrDefault(inserted_by_user_id,new HashSet<>()));
	}
	
	
	public List<Urine_examination_reportDTO> getUrine_examination_reportDTOByinserted_by_organogram_id(long inserted_by_organogram_id) {
		return new ArrayList<>( mapOfUrine_examination_reportDTOToinsertedByOrganogramId.getOrDefault(inserted_by_organogram_id,new HashSet<>()));
	}
	
	
	public List<Urine_examination_reportDTO> getUrine_examination_reportDTOByinsertion_date(long insertion_date) {
		return new ArrayList<>( mapOfUrine_examination_reportDTOToinsertionDate.getOrDefault(insertion_date,new HashSet<>()));
	}
	
	
	public List<Urine_examination_reportDTO> getUrine_examination_reportDTOBylastModificationTime(long lastModificationTime) {
		return new ArrayList<>( mapOfUrine_examination_reportDTOTolastModificationTime.getOrDefault(lastModificationTime,new HashSet<>()));
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "urine_examination_report";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


