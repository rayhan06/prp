package report;

public interface ColumnConvertor {
	Object convert(Object columnValue);
}
