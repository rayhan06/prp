alter table office_units add budget_office_id bigint default -1;

update office_units set org_tree = concat('0P1P1000P9P', id) where parent_unit_id = 9;

update office_units set budget_office_id = 1 where org_tree like '0P1P1001P10%';

update office_units set budget_office_id = 2 where id = 55;

update office_units set budget_office_id = 3 where id in (1,2);

update office_units set budget_office_id = 4 where id = 3;

update office_units set budget_office_id = 5 where id in (4,6);

update office_units set budget_office_id = 6 where org_tree like '0P1P1000P8P%';

update office_units set budget_office_id = 7 where (id = 5) or (org_tree like '0P1P1000P7P%');

update office_units set budget_office_id = 8 where org_tree like '0P1P1000P9P%';

update office_units set office_order = 7 where org_tree like '0P1P1000%' and budget_office_id <> -1;