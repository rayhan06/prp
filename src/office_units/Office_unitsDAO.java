package office_units;

import common.CommonDAOService;
import common.ConnectionAndStatementUtil;
import employee_offices.EmployeeOfficesDAO;
import org.apache.log4j.Logger;
import pb.Utils;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

@SuppressWarnings({"Duplicates"})
public class Office_unitsDAO implements CommonDAOService<Office_unitsDTO> {

    private static final Logger logger = Logger.getLogger(Office_unitsDAO.class);

    private static final String addQuery =
            "INSERT INTO {tableName} (unit_name_bng,unit_name_eng,parent_unit_id,budget_office_id,modified_by,modified,lastModificationTime,org_tree, address,is_abstract_office,"
                    .concat("building_cat,building_block_cat,building_level_cat, ")
                    .concat("office_ministry_id , office_layer_id , office_id , office_origin_unit_id ,office_unit_category , ")
                    .concat("parent_origin_unit_id , unit_nothi_code , unit_level ,sarok_no_start , email , phone , fax , active_status ,office_order,")
                    .concat("status, created_by ,created, isDeleted, ID) ")
                    .concat("VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");

    private static final String updateQuery = "UPDATE {tableName} SET unit_name_bng = ?, unit_name_eng = ?, parent_unit_id = ?, " +
            "budget_office_id = ?,modified_by = ?, modified= ?, lastModificationTime= ?,org_tree=?,address=?,is_abstract_office=?" +
            ", building_cat = ?,building_block_cat=?,building_level_cat=? WHERE ID = ?";

    private final Map<String, String> searchMap = new HashMap<>();

    private static final String updateOrgTreeById = "UPDATE office_units SET org_tree= ?, lastModificationTime= ? WHERE ID = ?";

    private static final String updateOrgTreeByOrgTree = "UPDATE office_units SET org_tree= REPLACE(org_tree, ?, ?) , lastModificationTime= ?  where org_tree LIKE '{prefix_org_tree}%'";

    private Office_unitsDAO() {
        searchMap.put("officeNameEng", " and (unit_name_eng like ?)");
        searchMap.put("officeNameBng", " and (unit_name_bng like ?)");
        searchMap.put("officeUnitId", " and (parent_unit_id = ?)");
        searchMap.put("AnyField", " and (search_column like ?)");
    }

    private static final class OfficeUnitsDAOHolder {
        static final Office_unitsDAO INSTANCE = new Office_unitsDAO();
    }

    public static Office_unitsDAO getInstance() {
        return OfficeUnitsDAOHolder.INSTANCE;
    }

    @Override
    public void set(PreparedStatement ps, Office_unitsDTO office_unitsDTO, boolean isInsert) throws SQLException {
        if (isInsert) {
            office_unitsDTO.orgTree += office_unitsDTO.iD;
        }
        int index = 0;
        ps.setObject(++index, office_unitsDTO.unitNameBng);
        ps.setObject(++index, office_unitsDTO.unitNameEng);
        ps.setObject(++index, office_unitsDTO.parentUnitId);
        ps.setLong(++index, office_unitsDTO.budgetOfficeId);
        ps.setObject(++index, office_unitsDTO.modifiedBy);
        ps.setObject(++index, office_unitsDTO.modified);
        ps.setObject(++index, office_unitsDTO.lastModificationTime);
        ps.setObject(++index, office_unitsDTO.orgTree);
        ps.setObject(++index, office_unitsDTO.address);
        ps.setObject(++index, office_unitsDTO.isAbstractOffice);
        ps.setObject(++index, office_unitsDTO.buildingCat);
        ps.setObject(++index, office_unitsDTO.buildingBlockCat);
        ps.setObject(++index, office_unitsDTO.buildingLevelCat);
        if (isInsert) {
            ps.setObject(++index, office_unitsDTO.officeMinistryId);
            ps.setObject(++index, office_unitsDTO.officeLayerId);
            ps.setObject(++index, office_unitsDTO.officeId);
            ps.setObject(++index, office_unitsDTO.officeOriginUnitId);
            ps.setObject(++index, office_unitsDTO.officeUnitCategory);
            ps.setObject(++index, office_unitsDTO.parentOriginUnitId);
            ps.setObject(++index, office_unitsDTO.unitNothiCode);
            ps.setObject(++index, office_unitsDTO.unitLevel);
            ps.setObject(++index, office_unitsDTO.sarokNoStart);
            ps.setObject(++index, office_unitsDTO.email);
            ps.setObject(++index, office_unitsDTO.phone);
            ps.setObject(++index, office_unitsDTO.fax);
            ps.setObject(++index, office_unitsDTO.activeStatus);
            ps.setObject(++index, office_unitsDTO.officeOrder);
            ps.setObject(++index, office_unitsDTO.status);
            ps.setObject(++index, office_unitsDTO.createdBy);
            ps.setObject(++index, office_unitsDTO.created);
            ps.setObject(++index, 0 /* isDeleted */);
        }
        ps.setObject(++index, office_unitsDTO.iD);
    }

    @Override
    public Office_unitsDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Office_unitsDTO office_unitsDTO = new Office_unitsDTO();
            office_unitsDTO.iD = rs.getLong("ID");
            office_unitsDTO.officeMinistryId = rs.getInt("office_ministry_id");
            office_unitsDTO.officeLayerId = rs.getInt("office_layer_id");
            office_unitsDTO.officeId = rs.getInt("office_id");
            office_unitsDTO.officeOriginUnitId = rs.getInt("office_origin_unit_id");
            office_unitsDTO.unitNameBng = rs.getString("unit_name_bng");
            office_unitsDTO.unitNameEng = rs.getString("unit_name_eng");
            office_unitsDTO.officeUnitCategory = rs.getString("office_unit_category");
            office_unitsDTO.parentUnitId = rs.getLong("parent_unit_id");
            office_unitsDTO.budgetOfficeId = rs.getLong("budget_office_id");
            office_unitsDTO.parentOriginUnitId = rs.getInt("parent_origin_unit_id");
            office_unitsDTO.unitNothiCode = rs.getString("unit_nothi_code");
            office_unitsDTO.unitLevel = rs.getInt("unit_level");
            office_unitsDTO.sarokNoStart = rs.getInt("sarok_no_start");
            office_unitsDTO.email = rs.getString("email");
            office_unitsDTO.phone = rs.getString("phone");
            office_unitsDTO.fax = rs.getString("fax");
            office_unitsDTO.orgTree = rs.getString("org_tree");
            office_unitsDTO.address = rs.getString("address");
            office_unitsDTO.activeStatus = rs.getBoolean("active_status");
            office_unitsDTO.officeOrder = rs.getInt("office_order");
            office_unitsDTO.createdBy = rs.getInt("created_by");
            office_unitsDTO.modifiedBy = rs.getInt("modified_by");
            office_unitsDTO.created = rs.getLong("created");
            office_unitsDTO.modified = rs.getLong("modified");
            office_unitsDTO.status = rs.getBoolean("status");
            office_unitsDTO.isAbstractOffice = rs.getBoolean("is_abstract_office");
            office_unitsDTO.isDeleted = rs.getInt("isDeleted");
            office_unitsDTO.lastModificationTime = rs.getLong("lastModificationTime");
            office_unitsDTO.buildingCat = rs.getInt("building_cat");
            office_unitsDTO.buildingBlockCat = rs.getInt("building_block_cat");
            office_unitsDTO.buildingLevelCat = rs.getInt("building_level_cat");
            return office_unitsDTO;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    @Override
    public String getTableName() {
        return "office_units";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Office_unitsDTO) commonDTO, addQuery, true);
    }

    public void updateOrgTreeById(String orgTree, long id, long lastModificationTime) throws Exception {
        Utils.wrapWithAtomicReference(ar ->
                ConnectionAndStatementUtil.getWritePrepareStatement(ps -> {
                    try {
                        ps.setObject(1, orgTree);
                        ps.setObject(2, lastModificationTime);
                        ps.setObject(3, id);
                        logger.debug(ps);
                        ps.executeUpdate();
                        Utils.addToRepoList(getTableName());
                        recordUpdateTime(CommonDAOService.CONNECTION_THREAD_LOCAL.get(),getTableName(),lastModificationTime);
                    } catch (SQLException e) {
                        logger.error("",e);
                        ar.set(e);
                    }
                }, CommonDAOService.CONNECTION_THREAD_LOCAL.get(), updateOrgTreeById)
        );
    }

    public void updateOrgTreeByOrgTree(String orgTree, String prefixOrgTree, long lastModificationTime) throws Exception {
        String sql = updateOrgTreeByOrgTree.replace("{prefix_org_tree}", prefixOrgTree);
        Utils.wrapWithAtomicReference(ar ->
                ConnectionAndStatementUtil.getWritePrepareStatement(ps -> {
                    try {
                        ps.setObject(1, prefixOrgTree);
                        ps.setObject(2, orgTree);
                        ps.setObject(3, lastModificationTime);
                        logger.debug(ps);
                        ps.executeUpdate();
                        Utils.addToRepoList(getTableName());
                        recordUpdateTime(CommonDAOService.CONNECTION_THREAD_LOCAL.get(),getTableName(),lastModificationTime);
                    } catch (SQLException e) {
                        logger.error("",e);
                        ar.set(e);
                    }
                }, CommonDAOService.CONNECTION_THREAD_LOCAL.get(), sql)
        );
    }

    @Override
    public long update(CommonDTO commonDTO) throws Exception {
        Office_unitsDTO dto = (Office_unitsDTO) commonDTO;
        long id = executeAddOrUpdateQuery(dto, updateQuery, false);
        EmployeeOfficesDAO.getInstance().updateOfficeByOfcUnitOrganogramIdAndOfficeUnitDTO(
                dto.iD, dto.unitNameEng, dto.unitNameBng, dto.lastModificationTime);
        return id;
    }

    public static void main(String[] args) {
        String query = "SELECT id,parent_unit_id,org_tree FROM office_units";
        String update = "UPDATE office_units SET org_tree = ? WHERE id = ?";

        class Model {
            long id, parent_unit_id;
            String org_tree;
        }

        List<Model> list = ConnectionAndStatementUtil.getListOfT(query, rs -> {
            try {
                Model model = new Model();
                model.id = rs.getLong("id");
                model.parent_unit_id = rs.getLong("parent_unit_id");
                model.org_tree = rs.getString("org_tree");
                return model;
            } catch (SQLException ex) {
                logger.error("",ex);
                return null;
            }
        });

        Map<Long, Model> mapById = list.stream().collect(Collectors.toMap(e -> e.id, e -> e));

        Map<Long, List<Model>> mapByParent = list.stream().collect(Collectors.groupingBy(e -> e.parent_unit_id));
        Model root = new Model();
        root.id = 0;
        root.parent_unit_id = -1;
        root.org_tree = "0";
        mapById.put(0L, root);
        List<Model> modelList = new ArrayList<>();
        modelList.add(mapById.get(1L));
        while (modelList.size() > 0) {
            Model model = modelList.remove(0);
            Model parent = mapById.get(model.parent_unit_id);
            model.org_tree = parent.org_tree + "P" + model.id;
            if (mapByParent.get(model.id) != null) {
                modelList.addAll(mapByParent.get(model.id));
            }
        }

        ConnectionAndStatementUtil.getWritePrepareStatement(ps -> {
            list.forEach(e -> {
                try {
                    ps.setObject(1, e.org_tree);
                    ps.setObject(2, e.id);
                    logger.debug(ps);
                    ps.executeUpdate();
                } catch (SQLException ex) {
                    logger.error("",ex);
                }
            });
        }, update);
    }
}