package office_units;

import office_unit_organograms.OfficeUnitOrganograms;
import office_unit_organograms.OfficeUnitOrganogramsRepository;
import org.apache.log4j.Logger;
import pb.OptionDTO;
import pb.Utils;
import repository.Repository;
import repository.RepositoryManager;
import sessionmanager.SessionConstants;
import util.LockManager;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

@SuppressWarnings("Duplicates")
public class Office_unitsRepository implements Repository {
    private final Office_unitsDAO office_unitsDAO = Office_unitsDAO.getInstance();
    public static final int OTHER_MAXIMUM_VALUE = 999;
    private static final Long SECRETARY_OFFICE_UNIT_ID = 10L;
    private static final Long SPEAKER_OFFICE_UNIT_ID = 1L;
    public static final Long MP_OFFICE_UNIT_ID = 9L;
    private static final Logger logger = Logger.getLogger(Office_unitsRepository.class);
    private static final String orgTreeForNationalParliament = "0P1P1000";
    private static final String orgTreeForNationalParliamentSecretariat = "0P1P1001";
    private final Map<Long, Office_unitsDTO> mapById;
    private List<Office_unitsDTO> officeUnitsDTOList;

    private Office_unitsRepository() {
        mapById = new ConcurrentHashMap<>();
        RepositoryManager.getInstance().addRepository(this);
    }

    private static class LazyLoader {
        static Office_unitsRepository INSTANCE = new Office_unitsRepository();
    }

    public static Office_unitsRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

    @Override
    public void reloadWithExactModificationTime(long time) {
        List<Office_unitsDTO> list = office_unitsDAO.getAllDTOsExactLastModificationTime(time);
        doCaching(list);
    }

    public void reload(boolean reloadAll) {
        logger.debug("Office_unitsRepository loading start for reloadAll: " + reloadAll);
        List<Office_unitsDTO> office_unitsDTOs = office_unitsDAO.getAllDTOs(reloadAll);
        if (office_unitsDTOs != null && office_unitsDTOs.size() > 0) {
            doCaching(office_unitsDTOs);
        }
        logger.debug("Office_unitsRepository loading end for reloadAll: " + reloadAll);
    }

    private void doCaching(List<Office_unitsDTO> office_unitsDTOs) {
        office_unitsDTOs.stream()
                .peek(this::removeIfPresent)
                .filter(dto -> dto.isDeleted == 0)
                .forEach(dto -> mapById.put(dto.iD, dto));
        officeUnitsDTOList = new ArrayList<>(mapById.values());
        officeUnitsDTOList.sort(Comparator.comparing(o -> o.iD));
    }


    private void removeIfPresent(Office_unitsDTO dto) {
        if (dto != null && mapById.get(dto.iD) != null) {
            mapById.remove(dto.iD);
        }
    }

    public List<Office_unitsDTO> getOffice_unitsList() {
        return officeUnitsDTOList;
    }

    public Office_unitsDTO getOffice_unitsDTOByID(long ID) {
        if (mapById.get(ID) == null) {
            synchronized (LockManager.getLock("OFFICE_UNITS_ID_" + ID)) {
                if (mapById.get(ID) == null) {
                    Office_unitsDTO officeUnitsDTO = office_unitsDAO.getDTOFromID(ID);
                    if (officeUnitsDTO != null) {
                        mapById.put(officeUnitsDTO.iD, officeUnitsDTO);
                        officeUnitsDTOList = new ArrayList<>(mapById.values());
                        officeUnitsDTOList.sort(Comparator.comparing(o -> o.iD));
                    }
                }
            }
        }
        return mapById.get(ID);
    }

    public void deleteDtoById(long id) {
        Office_unitsDTO oldOffice_unitsDTO = mapById.get(id);
        if (oldOffice_unitsDTO == null) return;
        mapById.remove(oldOffice_unitsDTO.iD);
    }

    @Override
    public String getTableName() {
        return "office_units";
    }

    public String buildOptionsWithOtherOption(String language, Long selectedId) {
        List<OptionDTO> optionDTOList = null;
        if (officeUnitsDTOList != null && officeUnitsDTOList.size() > 0) {
            optionDTOList = officeUnitsDTOList.stream()
                    .map(dto -> new OptionDTO(dto.unitNameEng, dto.unitNameBng, String.valueOf(dto.iD)))
                    .collect(Collectors.toList());
        }
        optionDTOList.add(new OptionDTO("OTHER", "অন্যান্য", String.valueOf(OTHER_MAXIMUM_VALUE), ""));
        return Utils.buildOptions(optionDTOList, language, selectedId == null ? null : String.valueOf(selectedId));
    }

    public String buildOptions(String language, Long selectedId) {
        return buildOptions(language, selectedId, false, officeUnitsDTOList);
    }

    public String buildOptionsMultiSelect(String language, String selectedIds) {
        List<OptionDTO> optionDTOList = null;
        if (officeUnitsDTOList != null && !officeUnitsDTOList.isEmpty()) {
            optionDTOList = officeUnitsDTOList.stream()
                                              .map(dto -> new OptionDTO(dto.unitNameEng, dto.unitNameBng, String.valueOf(dto.iD)))
                                              .collect(Collectors.toList());
        }
        return Utils.buildOptionsMultipleSelection(optionDTOList, language, selectedIds);
    }

    public String buildOptionsWithoutSelectOption(String language) {
        return buildOptions(language, null, true, officeUnitsDTOList);
    }

    public String buildOptions(String language, Long selectedId, boolean withoutSelectOption, List<Office_unitsDTO> officeUnitsDTOList) {
        List<OptionDTO> optionDTOList = null;
        if (officeUnitsDTOList != null && !officeUnitsDTOList.isEmpty()) {
            optionDTOList = officeUnitsDTOList.stream()
                    .map(dto -> new OptionDTO(dto.unitNameEng, dto.unitNameBng, String.valueOf(dto.iD)))
                    .collect(Collectors.toList());
        }
        if (withoutSelectOption) {
            return Utils.buildOptionsWithoutSelectOption(optionDTOList, language);
        }
        return Utils.buildOptions(optionDTOList, language, selectedId == null ? null : String.valueOf(selectedId));
    }

    public List<Office_unitsDTO> getChildList(long id) {
        if (officeUnitsDTOList == null) {
            return null;
        }
        return officeUnitsDTOList.parallelStream()
                .filter(e -> e.parentUnitId == id)
                .collect(Collectors.toList());
    }

    public List<Office_unitsDTO> getAllChildOfficeDTOWithParent(long id) {
        Office_unitsDTO parentDTO = getOffice_unitsDTOByID(id);
        if (parentDTO == null) {
            System.out.println("parentDTO is null for id = " + id);

            return new ArrayList<>();
        }
        return officeUnitsDTOList
                .parallelStream()
                .filter(e -> e.orgTree.startsWith(parentDTO.orgTree))
                .collect(Collectors.toList());
    }

    public List<Long> getDescentsOfficeUnitId(long id) {
        Office_unitsDTO parentDTO = getOffice_unitsDTOByID(id);
        if (parentDTO == null) {
            System.out.println("parentDTO is null for id = " + id);

            return new ArrayList<>();
        }
        return getAllChildOfficeDTOWithParent(id)
                .stream()
                .map(e->e.iD)
                .collect(Collectors.toList());
    }

    public Set<Long> getAllChildOfficeIdsWithParent(long id) {
        return getAllChildOfficeDTOWithParent(id)
                .stream()
                .filter(Objects::nonNull)
                .map(dto -> dto.iD)
                .collect(Collectors.toSet());
    }

    public String geText(String language, long id) {
        Office_unitsDTO dto = mapById.get(id);
        return dto == null ? "" : (language.equalsIgnoreCase("English") ?
                (dto.unitNameEng == null ? "" : dto.unitNameEng)
                : (dto.unitNameBng == null ? "" : dto.unitNameBng));
    }

    public String buildOptionsByParentId(String language, long parentId) {
        return buildOptionsByParentId(language, parentId, null);
    }

    public String buildOptionsByParentId(String language, long parentId, Long selectedId) {
        List<Office_unitsDTO> childOffices = getChildList(parentId);
        if (childOffices == null || childOffices.isEmpty()) return "";
        return buildOptions(language, selectedId, false, childOffices);
    }

    public OfficeUnitModel getByOfficeUnitId(long officeUnitId) {
        OfficeUnitModel model = new OfficeUnitModel();
        String secretaryOrgTree = getOffice_unitsDTOByID(SECRETARY_OFFICE_UNIT_ID).orgTree;
        Office_unitsDTO officeUnitsDTO = getOffice_unitsDTOByID(officeUnitId);
        if (officeUnitsDTO.orgTree.startsWith(secretaryOrgTree)) {
            model.isSecretaryOrUnderOffice = true;
            if (officeUnitsDTO.orgTree.equals(secretaryOrgTree) ||
                    (officeUnitsDTO.orgTree.matches(secretaryOrgTree + "P[0-9]+")) ||
                    (officeUnitsDTO.orgTree.matches(secretaryOrgTree + "P[0-9]+P[0-9]+"))
            ) {
                model.isSecretaryOrJsOrDs = true;
            }
            if (!model.isSecretaryOrJsOrDs) {
                /*
                    secretary_org_tree is 0P1P10
                    For example officeUnitOrgTree is 0P1P10P41P42P43P44 which is split by secretary_org_tree+"P" that's why adding 1
                    tokens index 0 --> js office id
                    tokens index 1 --> ds office id
                 */
                String[] tokens = officeUnitsDTO.orgTree.substring(secretaryOrgTree.length() + 1).split("P");
                List<OfficeUnitOrganograms> organogramsList = OfficeUnitOrganogramsRepository.getInstance().getByOfficeUnitId(Long.parseLong(tokens[1]));
                model.superiorOrganogramId = organogramsList.stream()
                        .filter(e -> e.orgTree.endsWith("|"))
                        .findAny()
                        .map(e -> e.id)
                        .orElse(-1L);
            }
        }
        return model;
    }

    public boolean isSAsJsDsSas(long officeUnitId) {
        Office_unitsDTO officeUnitsDTO = getOffice_unitsDTOByID(officeUnitId);
        String secretaryOrgTree = getOffice_unitsDTOByID(SECRETARY_OFFICE_UNIT_ID).orgTree;
        if (officeUnitsDTO.orgTree.startsWith(secretaryOrgTree)) {
            return officeUnitsDTO.orgTree.equals(secretaryOrgTree) ||
                    (officeUnitsDTO.orgTree.matches(secretaryOrgTree + "P[0-9]+")) ||
                    (officeUnitsDTO.orgTree.matches(secretaryOrgTree + "P[0-9]+P[0-9]+")) ||
                    (officeUnitsDTO.orgTree.matches(secretaryOrgTree + "P[0-9]+P[0-9]+P[0-9]+"));
        }
        return false;
    }

    public Office_unitsDTO getParentOfficeUnitsDTOById(long officeUnitId) {
        Office_unitsDTO officeUnitsDTO = getOffice_unitsDTOByID(officeUnitId);
        if (officeUnitsDTO == null) {
            return null;
        }
        return getOffice_unitsDTOByID(officeUnitsDTO.parentUnitId);
    }

    public List<Long> getAllParentIds(long officeUnitId) {
        List<Long> list = new ArrayList<>();
        list.add(officeUnitId);
        Office_unitsDTO officeUnitsDTO = getOffice_unitsDTOByID(officeUnitId);
        while (officeUnitsDTO != null) {
            list.add(officeUnitsDTO.parentUnitId);
            officeUnitsDTO = getOffice_unitsDTOByID(officeUnitsDTO.parentUnitId);
        }
        Collections.reverse(list);
        return list;
    }

    public List<String> buildAllLayerOptions(String language, long selectedOfficeUnitId) {
        List<Long> allParentIds = Office_unitsRepository.getInstance().getAllParentIds(selectedOfficeUnitId);
        List<String> layerSelectOptions = new ArrayList<>();
        for (int i = 0; i < allParentIds.size(); ++i) {
            Long currentId = allParentIds.get(i);
            Long nextId = (i == (allParentIds.size() - 1)) ? null : allParentIds.get(i + 1);
            String option = Office_unitsRepository.getInstance().buildOptionsByParentId(language, currentId, nextId);
            layerSelectOptions.add(option);
        }
        return layerSelectOptions;
    }

    public Office_unitsDTO getOfficeType(long officeUnitId) {
        Office_unitsDTO officeUnitsDTO = getOffice_unitsDTOByID(officeUnitId);
        while (officeUnitsDTO != null && officeUnitsDTO.parentUnitId != SPEAKER_OFFICE_UNIT_ID) {
            officeUnitsDTO = getOffice_unitsDTOByID(officeUnitsDTO.parentUnitId);
        }
        return officeUnitsDTO;
    }

    public String buildOptionsByOfficeOrder(String language, int officeOrder, Long selectedId) {
        List<Office_unitsDTO> childOffices = getOffice_unitsList().stream().filter(dto -> dto.officeOrder == officeOrder).collect(Collectors.toList());
        if (childOffices.isEmpty()) return "";
        return buildOptions(language, selectedId, false, childOffices);
    }

    public Organization getOrganizationByOfficeUnitId(long officeUnitId) {
        Office_unitsDTO officeUnitsDTO = getOffice_unitsDTOByID(officeUnitId);
        if (officeUnitsDTO == null) {
            return Organization.OTHER;
        }
        if (officeUnitsDTO.iD == Office_unitsRepository.SPEAKER_OFFICE_UNIT_ID || officeUnitsDTO.orgTree.startsWith(orgTreeForNationalParliament)) {
            return Organization.NATIONAL_PARLIAMENT;
        }
        if (officeUnitsDTO.orgTree.startsWith(orgTreeForNationalParliamentSecretariat)) {
            return Organization.NATIONAL_PARLIAMENT_SECRETARIAT;
        }
        return Organization.OTHER;
    }

    public List<Office_unitsDTO> getOfficeUnitsDTOListAsInventoryStore() {
        List<Office_unitsDTO> office_unitsDTOS = getOffice_unitsList();
        List<Long> storeOfficeUnit = new ArrayList<>(SessionConstants.PurchaseStoreMap.values());
        office_unitsDTOS = office_unitsDTOS.stream()
                .filter(f -> storeOfficeUnit.contains(f.iD))
                .collect(Collectors.toList());
        return office_unitsDTOS;
    }

    public Office_unitsDTO getParentOffice(Office_unitsDTO dto) {
        if(dto == null){
            return null;
        }
        if(dto.iD == 1){ //SPEAKER OFFICE
            return dto;
        }
        do {
            dto = getOffice_unitsDTOByID(dto.parentUnitId);
        } while (dto != null && dto.isAbstractOffice);
        return dto;
    }

    public Office_unitsDTO getParentOffice(long id) {
        return getParentOffice(getOffice_unitsDTOByID(id));
    }

    public List<Office_unitsDTO> getConcreteChildOffices(long id){
       return getConcreteChildOffices(getOffice_unitsDTOByID(id));
    }
    public List<Office_unitsDTO> getConcreteChildOffices(Office_unitsDTO dto){
        if(dto == null){
            return new ArrayList<>();
        }
        List<Office_unitsDTO> childList= getChildList(dto.iD);
        List<Office_unitsDTO> list = new ArrayList<>();
        for(Office_unitsDTO childDTO : childList){
            if(childDTO.isAbstractOffice){
                List<Office_unitsDTO> tempList = getConcreteChildOffices(childDTO);
                list.addAll(tempList);
            }else{
                list.add(childDTO);
            }
        }
        return list;
    }

    public Office_unitsDTO getConcreteParentIncludingSelf(long id){
        Office_unitsDTO dto = getOffice_unitsDTOByID(id);
        if(dto == null){
            return null;
        }
        if(!dto.isAbstractOffice){
            return dto;
        }
        return getConcreteParentIncludingSelf(dto.parentUnitId);
    }

    public Office_unitsDTO findClosestOfficeUnitOfAnOfficeType(long officeUnitId, OfficeUnitTypeEnum officeUnitTypeEnum){
        Office_unitsDTO dto = getOffice_unitsDTOByID(officeUnitId);
        while (dto != null && !dto.isOfficeType(officeUnitTypeEnum)) {
            dto = getOffice_unitsDTOByID(dto.parentUnitId);
        }
        return dto;
    }

    public List<Office_unitsDTO> getImmediateChilds(long officeUnitId){
        return officeUnitsDTOList.parallelStream()
                .filter(e->e.parentUnitId == officeUnitId)
                .collect(Collectors.toList());
    }
}