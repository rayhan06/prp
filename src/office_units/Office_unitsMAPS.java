package office_units;
import java.util.*; 
import util.*;


public class Office_unitsMAPS extends CommonMaps
{	
	public Office_unitsMAPS(String tableName)
	{
		
		java_allfield_type_map.put("unit_name_bng".toLowerCase(), "String");
		java_allfield_type_map.put("unit_name_eng".toLowerCase(), "String");
		java_allfield_type_map.put("office_unit_category".toLowerCase(), "String");
		java_allfield_type_map.put("parent_unit_id".toLowerCase(), "Long");
		java_allfield_type_map.put("unit_nothi_code".toLowerCase(), "String");
		java_allfield_type_map.put("unit_level".toLowerCase(), "Integer");
		java_allfield_type_map.put("sarok_no_start".toLowerCase(), "Integer");
		java_allfield_type_map.put("email".toLowerCase(), "String");
		java_allfield_type_map.put("phone".toLowerCase(), "String");
		java_allfield_type_map.put("fax".toLowerCase(), "String");

		java_anyfield_search_map.put(tableName + ".office_ministry_id".toLowerCase(), "Integer");
		java_anyfield_search_map.put(tableName + ".office_layer_id".toLowerCase(), "Integer");
		java_anyfield_search_map.put(tableName + ".office_id".toLowerCase(), "Integer");
		java_anyfield_search_map.put(tableName + ".office_origin_unit_id".toLowerCase(), "Integer");
		java_anyfield_search_map.put(tableName + ".unit_name_bng".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".unit_name_eng".toLowerCase(), "String");
		java_anyfield_search_map.put("office_unit_categories.category_name_eng".toLowerCase(), "String");
		java_anyfield_search_map.put("office_unit_categories.category_name_bng".toLowerCase(), "String");
		java_anyfield_search_map.put("office_units .unit_name_eng".toLowerCase(), "String");
		java_anyfield_search_map.put("office_units .unit_name_bng".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".parent_origin_unit_id".toLowerCase(), "Integer");
		java_anyfield_search_map.put(tableName + ".unit_nothi_code".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".unit_level".toLowerCase(), "Integer");
		java_anyfield_search_map.put(tableName + ".sarok_no_start".toLowerCase(), "Integer");
		java_anyfield_search_map.put(tableName + ".email".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".phone".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".fax".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".active_status".toLowerCase(), "Boolean");
		java_anyfield_search_map.put(tableName + ".created_by".toLowerCase(), "Integer");
		java_anyfield_search_map.put(tableName + ".modified_by".toLowerCase(), "Integer");
		java_anyfield_search_map.put(tableName + ".created".toLowerCase(), "Long");
		java_anyfield_search_map.put(tableName + ".modified".toLowerCase(), "Long");
		java_anyfield_search_map.put(tableName + ".status".toLowerCase(), "Boolean");

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("officeMinistryId".toLowerCase(), "officeMinistryId".toLowerCase());
		java_DTO_map.put("officeLayerId".toLowerCase(), "officeLayerId".toLowerCase());
		java_DTO_map.put("officeId".toLowerCase(), "officeId".toLowerCase());
		java_DTO_map.put("officeOriginUnitId".toLowerCase(), "officeOriginUnitId".toLowerCase());
		java_DTO_map.put("unitNameBng".toLowerCase(), "unitNameBng".toLowerCase());
		java_DTO_map.put("unitNameEng".toLowerCase(), "unitNameEng".toLowerCase());
		java_DTO_map.put("officeUnitCategory".toLowerCase(), "officeUnitCategory".toLowerCase());
		java_DTO_map.put("parentUnitId".toLowerCase(), "parentUnitId".toLowerCase());
		java_DTO_map.put("parentOriginUnitId".toLowerCase(), "parentOriginUnitId".toLowerCase());
		java_DTO_map.put("unitNothiCode".toLowerCase(), "unitNothiCode".toLowerCase());
		java_DTO_map.put("unitLevel".toLowerCase(), "unitLevel".toLowerCase());
		java_DTO_map.put("sarokNoStart".toLowerCase(), "sarokNoStart".toLowerCase());
		java_DTO_map.put("email".toLowerCase(), "email".toLowerCase());
		java_DTO_map.put("phone".toLowerCase(), "phone".toLowerCase());
		java_DTO_map.put("fax".toLowerCase(), "fax".toLowerCase());
		java_DTO_map.put("activeStatus".toLowerCase(), "activeStatus".toLowerCase());
		java_DTO_map.put("createdBy".toLowerCase(), "createdBy".toLowerCase());
		java_DTO_map.put("modifiedBy".toLowerCase(), "modifiedBy".toLowerCase());
		java_DTO_map.put("created".toLowerCase(), "created".toLowerCase());
		java_DTO_map.put("modified".toLowerCase(), "modified".toLowerCase());
		java_DTO_map.put("status".toLowerCase(), "status".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("unit_name_bng".toLowerCase(), "unitNameBng".toLowerCase());
		java_SQL_map.put("unit_name_eng".toLowerCase(), "unitNameEng".toLowerCase());
		java_SQL_map.put("office_unit_category".toLowerCase(), "officeUnitCategory".toLowerCase());
		java_SQL_map.put("parent_unit_id".toLowerCase(), "parentUnitId".toLowerCase());
		java_SQL_map.put("unit_nothi_code".toLowerCase(), "unitNothiCode".toLowerCase());
		java_SQL_map.put("unit_level".toLowerCase(), "unitLevel".toLowerCase());
		java_SQL_map.put("sarok_no_start".toLowerCase(), "sarokNoStart".toLowerCase());
		java_SQL_map.put("email".toLowerCase(), "email".toLowerCase());
		java_SQL_map.put("phone".toLowerCase(), "phone".toLowerCase());
		java_SQL_map.put("fax".toLowerCase(), "fax".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Office Ministry Id".toLowerCase(), "officeMinistryId".toLowerCase());
		java_Text_map.put("Office Layer Id".toLowerCase(), "officeLayerId".toLowerCase());
		java_Text_map.put("Office Id".toLowerCase(), "officeId".toLowerCase());
		java_Text_map.put("Office Origin Unit Id".toLowerCase(), "officeOriginUnitId".toLowerCase());
		java_Text_map.put("Unit Name (Bangla)".toLowerCase(), "unitNameBng".toLowerCase());
		java_Text_map.put("Unit Name (English)".toLowerCase(), "unitNameEng".toLowerCase());
		java_Text_map.put("Office Unit Category".toLowerCase(), "officeUnitCategory".toLowerCase());
		java_Text_map.put("Parent Unit".toLowerCase(), "parentUnitId".toLowerCase());
		java_Text_map.put("Parent Origin Unit Id".toLowerCase(), "parentOriginUnitId".toLowerCase());
		java_Text_map.put("Unit Nothi Code".toLowerCase(), "unitNothiCode".toLowerCase());
		java_Text_map.put("Unit Level".toLowerCase(), "unitLevel".toLowerCase());
		java_Text_map.put("Sarok No Start".toLowerCase(), "sarokNoStart".toLowerCase());
		java_Text_map.put("Email".toLowerCase(), "email".toLowerCase());
		java_Text_map.put("Phone".toLowerCase(), "phone".toLowerCase());
		java_Text_map.put("Fax".toLowerCase(), "fax".toLowerCase());
		java_Text_map.put("Active Status".toLowerCase(), "activeStatus".toLowerCase());
		java_Text_map.put("Created By".toLowerCase(), "createdBy".toLowerCase());
		java_Text_map.put("Modified By".toLowerCase(), "modifiedBy".toLowerCase());
		java_Text_map.put("Created".toLowerCase(), "created".toLowerCase());
		java_Text_map.put("Modified".toLowerCase(), "modified".toLowerCase());
		java_Text_map.put("Status".toLowerCase(), "status".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}