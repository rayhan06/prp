package office_units;


import pb.OptionDTO;
import pb.Utils;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public enum OfficeUnitTypeEnum {
    SPEAKER("Speaker", "স্পিকার", 1),
    SECRETARY("Secretary", "সেক্রেটারি", 2),
    WINGS("Wing", "উইং", 3),
    BRANCH("Branch", "অধিশাখা", 4),
    DIVISION("Division", "শাখা", 5),
    UNIT("Unit", "ইউনিট", 6),
    NATIONAL_PARLIAMENT("National Parliament", "জাতীয় সংসদ", 7),
    ;

    private final String engText;
    private final String bngText;
    private final int value;

    OfficeUnitTypeEnum(String engText, String bngText, int value) {
        this.engText = engText;
        this.bngText = bngText;
        this.value = value;
    }

    public String getEngText() {
        return engText;
    }

    public String getBngText() {
        return bngText;
    }

    public int getValue() {
        return value;
    }

    private static volatile Map<Integer, OfficeUnitTypeEnum> mapEnumByValue = null;

    private static void loadMap() {
        if (mapEnumByValue == null) {
            synchronized (OfficeUnitTypeEnum.class) {
                if (mapEnumByValue == null) {
                    mapEnumByValue = Stream.of(OfficeUnitTypeEnum.values())
                                           .collect(Collectors.toMap(e -> e.value, e -> e));
                }
            }
        }
    }

    public static String getBuildOptions(String language, int selectedId) {
        return Utils.buildOptions(
                Arrays.stream(OfficeUnitTypeEnum.values())
                      .map(statusEnum -> new OptionDTO(statusEnum.engText, statusEnum.bngText, String.valueOf(statusEnum.value)))
                      .collect(Collectors.toList()),
                language,
                String.valueOf(selectedId)
        );
    }

    public static String getTextByValue(String language, int value) {
        if (mapEnumByValue == null) {
            loadMap();
        }
        OfficeUnitTypeEnum officeUnitTypeEnum = mapEnumByValue.get(value);
        if (officeUnitTypeEnum == null) return "";
        return "English".equalsIgnoreCase(language) ? officeUnitTypeEnum.engText : officeUnitTypeEnum.bngText;
    }

    public static OfficeUnitTypeEnum getByValue(int value) {
        if (mapEnumByValue == null) {
            loadMap();
        }
        return mapEnumByValue.get(value);
    }
}