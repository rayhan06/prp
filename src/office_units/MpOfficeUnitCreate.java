package office_units;

/*
 * @author Md. Erfan Hossain
 * @created 19/01/2022 - 6:37 PM
 * @project parliament
 */

import common.ConnectionAndStatementUtil;
import dbm.DBMW;
import util.StringUtils;

import java.sql.SQLException;

public class MpOfficeUnitCreate {
    public static void main(String[] args) {
        /*String sql = "INSERT INTO office_units (id,unit_name_eng,unit_name_bng,parent_unit_id, " +
                " lastModificationTime,org_tree) VALUES (?,?,?,?,?,?)";
        ConnectionAndStatementUtil.getWritePrepareStatement(ps->{
            long time = System.currentTimeMillis();
            for(int i= 1;i<=6;i++){
                try {
                    long id = DBMW.getInstance().getNextSequenceId("office_units");
                    ps.setObject(1,id);
                    ps.setObject(2,"WHIPS-"+i);
                    ps.setObject(3,"হুইপস-"+ StringUtils.convertToBanNumber(String.valueOf(i)));
                    ps.setObject(4,"7");
                    ps.setObject(5,time);
                    ps.setObject(6,"0P1P1000P8P"+id);
                    System.out.println(ps);
                    ps.execute();
                } catch (Exception e) {
                    e.printStackTrace();
                    break;
                }
            }

        },sql);*/

        /*String sql = "INSERT INTO office_unit_organograms (id,office_unit_id,superior_designation_id," +
                "designation_eng,designation_bng,org_tree,lastModificationTime) VALUES (?,?,?,?,?,?,?)";

        ConnectionAndStatementUtil.getWritePrepareStatement(ps->{
            long time = System.currentTimeMillis();
            long officeUnitId = 1701;
            for(int i= 1;i<=6;i++){
                try {
                    long id = DBMW.getInstance().getNextSequenceId("office_unit_organograms");
                    ps.setObject(1,id);
                    ps.setObject(2,officeUnitId++);
                    ps.setObject(3,129333);
                    ps.setObject(4,"WHIPS");
                    ps.setObject(5,"হুইপস");
                    ps.setObject(6,"0|129333|"+id+"|");
                    ps.setObject(7,time);
                    System.out.println(ps);
                    ps.execute();
                } catch (Exception e) {
                    e.printStackTrace();
                    break;
                }
            }
        },sql);*/

        String sql = "INSERT INTO office_unit_organograms (id,office_unit_id,superior_designation_id," +
                "designation_eng,designation_bng,org_tree,lastModificationTime) VALUES (?,?,?,?,?,?,?)";

        ConnectionAndStatementUtil.getWritePrepareStatement(ps -> {
            long time = System.currentTimeMillis();
            String[] engDesignations = {"Private Secretary", "Assistant Private Secretary", "Personal Assistant", "Support Staff", "Office Support Staff", "Cook"};
            String[] bngDesignations = {"একান্ত সচিব", "সহকারী একান্ত সচিব", "ব্যক্তিগত সহকারী", "সহায়ক", "অফিস সহায়ক", "কুক"};
            int[] count = {6, 6, 12, 12, 12, 6};
            int bossCount = 6;
            for (int j = 0; j < bossCount; j++) {
                for(int k = 1; k<=count[j]/bossCount;k++){
                    long officeUnitId = 1701;
                    long superiorDesignationId = 306301;
                    for (int i = 1; i <= bossCount; i++) {
                        try {
                            long id = DBMW.getInstance().getNextSequenceId("office_unit_organograms");
                            ps.setObject(1, id);
                            ps.setObject(2, officeUnitId++);
                            ps.setObject(3, superiorDesignationId);
                            ps.setObject(4, engDesignations[j]);
                            ps.setObject(5, bngDesignations[j]);
                            ps.setObject(6, "0|129333|" + superiorDesignationId + "|" + id + "$");
                            ps.setObject(7, time);
                            superiorDesignationId++;
                            System.out.println(ps);
                            ps.execute();
                        } catch (Exception e) {
                            e.printStackTrace();
                            break;
                        }
                    }
                }
            }
        }, sql);

        /*String sql = "INSERT INTO office_unit_organograms (id,office_unit_id,superior_designation_id," +
                "designation_eng,designation_bng,org_tree,lastModificationTime) VALUES (?,?,?,?,?,?,?)";

        ConnectionAndStatementUtil.getWritePrepareStatement(ps->{
            long time = System.currentTimeMillis();
            long officeUnitId = 1601;
            long superiorDesignationId = 305901;
            for(int i= 1;i<=44;i++){
                try {
                    long id = DBMW.getInstance().getNextSequenceId("office_unit_organograms");
                    ps.setObject(1,id);
                    ps.setObject(2,officeUnitId++);
                    ps.setObject(3,superiorDesignationId);
                    ps.setObject(4,"Personal Assistant");
                    ps.setObject(5,"ব্যক্তিগত সহকারী");
                    ps.setObject(6,"0|129333|"+superiorDesignationId+"|"+id+"$");
                    ps.setObject(7,time);
                    superiorDesignationId++;
                    System.out.println(ps);
                    ps.execute();
                } catch (Exception e) {
                    e.printStackTrace();
                    break;
                }
            }
        },sql);*/

        /*String sql = "INSERT INTO office_unit_organograms (id,office_unit_id,superior_designation_id," +
                "designation_eng,designation_bng,org_tree,lastModificationTime) VALUES (?,?,?,?,?,?,?)";

        ConnectionAndStatementUtil.getWritePrepareStatement(ps->{
            long time = System.currentTimeMillis();
            long officeUnitId = 1601;
            long superiorDesignationId = 305901;
            for(int i= 1;i<=44;i++){
                try {
                    long id = DBMW.getInstance().getNextSequenceId("office_unit_organograms");
                    ps.setObject(1,id);
                    ps.setObject(2,officeUnitId++);
                    ps.setObject(3,superiorDesignationId);
                    ps.setObject(4,"Office Support Staff ");
                    ps.setObject(5,"অফিস সহায়ক");
                    ps.setObject(6,"0|129333|"+superiorDesignationId+"|"+id+"$");
                    ps.setObject(7,time);
                    superiorDesignationId++;
                    System.out.println(ps);
                    ps.execute();
                } catch (Exception e) {
                    e.printStackTrace();
                    break;
                }
            }
        },sql);*/

        /*String sql = "UPDATE office_unit_organograms SET office_unit_id = ?,superior_unit_id = 9 WHERE id =?";

        ConnectionAndStatementUtil.getWritePrepareStatement(ps->{
            long office_unit_id = 151;
            long id = 23901;
            for (int i = 1; i <= 350; i++) {
                try {
                    ps.setObject(1,office_unit_id++);
                    ps.setObject(2,id++);
                    ps.executeUpdate();
                } catch (SQLException e) {
                    e.printStackTrace();
                    break;
                }
            }
            office_unit_id = 151;
            id = 280001;
            for (int i = 1; i <= 260; i++) {
                try {
                    ps.setObject(1,office_unit_id++);
                    ps.setObject(2,id++);
                    ps.executeUpdate();
                } catch (SQLException e) {
                    e.printStackTrace();
                    break;
                }
            }
        },sql);

        sql = "UPDATE employee_offices SET office_unit_id = ? WHERE office_unit_organogram_id =?";

        ConnectionAndStatementUtil.getWritePrepareStatement(ps->{
            long office_unit_id = 151;
            long office_unit_organogram_id = 270001;
            for (int i = 1; i <= 350; i++) {
                try {
                    ps.setObject(1,office_unit_id++);
                    ps.setObject(2,office_unit_organogram_id++);
                    ps.executeUpdate();
                } catch (SQLException e) {
                    e.printStackTrace();
                    break;
                }
            }
            office_unit_id = 151;
            office_unit_organogram_id = 280001;
            for (int i = 1; i <= 260; i++) {
                try {
                    ps.setObject(1,office_unit_id++);
                    ps.setObject(2,office_unit_organogram_id++);
                    ps.executeUpdate();
                } catch (SQLException e) {
                    e.printStackTrace();
                    break;
                }
            }
        },sql);*/
    }
}
