/*
package office_units;

import db.tables.office_origin_units;
import db.tables.office_units;
import dbm.DBMW;
import office_origin_units.Office_origin_unitsDTO;
import office_origin_units.Office_origin_unitsRepository;
import test_lib.RInsertQueryBuilder;

import java.util.List;

public class OfficeUnitService {

    public void addNewOffice(List<List<Integer>> listOfList, int office_id) {
        for (List<Integer> ids : listOfList) {
            for (Integer id : ids) {
                if (!alreadyInOfficeList(id, office_id)) {
                    office_origin_units origin_unit = getOfficeOriginUnit(id);

                    if (origin_unit != null && office_id > 0) {
                        long parentUnitId = getParentUnitId(origin_unit.getId(), office_id);
                        insert(origin_unit, parentUnitId, office_id);
                    }
                }
            }
        }
    }

    private office_origin_units getOfficeOriginUnit(int id) {

        List<Office_origin_unitsDTO> dataList = Office_origin_unitsRepository.getInstance().getOffice_origin_unitsList();
        for (Office_origin_unitsDTO s : dataList) {
            if (s.iD == id && !s.isDeleted && s.status) {
                office_origin_units o = new office_origin_units();
                o.setId(s.iD);
                o.setOffice_ministry_id(s.officeMinistryId);
                o.setOffice_layer_id(s.officeLayerId);
                o.setOffice_origin_id(s.officeOriginId);
                o.setUnit_name_bng(s.unitNameBng);
                o.setUnit_name_eng(s.unitNameEng);
                o.setOffice_unit_category(s.officeUnitCategory);
                o.setParent_unit_id(s.parentUnitId);
                o.setUnit_level(s.unitLevel);
                o.setStatus(1);

                return o;
            }
        }
        return null;
    }

    private boolean alreadyInOfficeList(int officeOriginUnitId, int officeId) {
        List<Office_unitsDTO> dataList = Office_unitsRepository.getInstance().getOffice_unitsList();
        for (Office_unitsDTO s : dataList) {
            if (s.officeOriginUnitId == officeOriginUnitId && s.officeId == officeId && !s.isDeleted) {
                return true;
            }
        }
        return false;
    }

    private long getParentUnitId(long office_origin_unit_id, int office_id) {
        Office_origin_unitsDTO origin = null;

        List<Office_origin_unitsDTO> dataList = Office_origin_unitsRepository.getInstance().getOffice_origin_unitsList();
        for (Office_origin_unitsDTO s : dataList) {
            if (s.iD == office_origin_unit_id && !s.isDeleted && s.status) {
                origin = s;
                break;
            }
        }
        if (origin == null) return 0;

        List<Office_unitsDTO> dataList0 = Office_unitsRepository.getInstance().getOffice_unitsList();
        for (Office_unitsDTO s : dataList0) {
            if (s.officeId == office_id && s.officeOriginUnitId == origin.parentUnitId && !s.isDeleted && s.status)
                return s.iD;
        }

        return 0;
    }

    public long insert(office_origin_units o, long parentUnitId, int officeId) {
        try {
            long id = DBMW.getInstance().getNextSequenceId("office_units");
            long time = System.currentTimeMillis();

            office_units u = new office_units();

            u.setId(id);
            u.setOffice_ministry_id(o.getOffice_ministry_id());
            u.setOffice_layer_id(o.getOffice_layer_id());
            u.setOffice_id(officeId);
            u.setOffice_origin_unit_id(o.getId());
            u.setUnit_name_bng(o.getUnit_name_bng());
            u.setUnit_name_eng(o.getUnit_name_eng());
            u.setOffice_unit_category(o.getOffice_unit_category());
            u.setParent_unit_id(parentUnitId);
            u.setParent_origin_unit_id(o.getParent_unit_id());
            u.setUnit_nothi_code(null);
            u.setUnit_level(o.getUnit_level());
            u.setSarok_no_start(0);
            u.setEmail(null);
            u.setPhone(null);
            u.setFax(null);
            u.setActive_status(1);
            u.setCreated_by(0);
            u.setModified_by(0);
            u.setCreated(0);
            u.setModified(0);
            u.setStatus(1);
            u.setLastModificationTime(time);

            RInsertQueryBuilder<office_units> builder = new RInsertQueryBuilder<>();
            builder.of(office_units.class).model(u).updateVbSequence(time).buildInsert();
            Office_unitsRepository.getInstance().addNewDto(getRepositoryDto(u));

            return id;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    private Office_unitsDTO getRepositoryDto(office_units u) {
        Office_unitsDTO dto = new Office_unitsDTO();
        dto.iD = u.getId();
        dto.officeMinistryId = u.getOffice_ministry_id();
        dto.officeId = u.getOffice_id();
        dto.officeOriginUnitId = (int) u.getOffice_origin_unit_id();
        dto.unitNameBng = u.getUnit_name_bng();
        dto.unitNameEng = u.getUnit_name_eng();
        dto.officeUnitCategory = u.getOffice_unit_category();
        dto.parentUnitId = u.getParent_unit_id();
        dto.parentOriginUnitId = u.getParent_origin_unit_id();
        dto.unitNothiCode = u.getUnit_nothi_code();
        dto.unitLevel = u.getUnit_level();
        dto.sarokNoStart = u.getSarok_no_start();
        dto.email = u.getEmail();
        dto.phone = u.getPhone();
        dto.fax = u.getFax();
        dto.activeStatus = u.getActive_status() >= 1;
        dto.created = u.getCreated_by();
        dto.createdBy = (int) u.getCreated();
        dto.modifiedBy = u.getModified_by();
        dto.modified = u.getModified();
        dto.status = u.getStatus() >= 1;

        return dto;
    }
}
*/
