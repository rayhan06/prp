package office_units;

import util.CommonDTO;

import java.util.ArrayList;
import java.util.List;

import office_building.*;

public class Office_unitsDTO extends CommonDTO {

    public int officeMinistryId = 0;
    public int officeLayerId = 0;
    public int officeId = 0;
    public int officeOriginUnitId = 0;
    public String unitNameBng = "";
    public String unitNameEng = "";
    public String officeUnitCategory = "";
    public long parentUnitId = 0;
    public int parentOriginUnitId = 0;
    public String unitNothiCode = "";
    public int unitLevel = 0;
    public int sarokNoStart = 0;
    public String email = "";
    public String phone = "";
    public String fax = "";
    public boolean activeStatus = false;
    public int createdBy = 0;
    public int modifiedBy = 0;
    public long created = 0;
    public long modified = 0;
    public boolean status = false;
    public String orgTree = "";
    public int officeOrder;
    public long budgetOfficeId = -1;
    public String address;
    public boolean isAbstractOffice = false;

    public int buildingCat = -1;
    public int buildingBlockCat = -1;
    public int buildingLevelCat = -1;
    
    public List<Office_buildingDTO> office_buildings = new ArrayList<Office_buildingDTO>();
    public boolean isOfficeType(OfficeUnitTypeEnum officeUnitTypeEnum) {
        return officeOrder == officeUnitTypeEnum.getValue();
    }

    public boolean hasBranchInAncestorNotSelf() {
        OfficeUnitTypeEnum officeUnitTypeEnum = OfficeUnitTypeEnum.getByValue(officeOrder);
        if (officeUnitTypeEnum == null) {
            return false;
        }
        switch (officeUnitTypeEnum) {
            case SPEAKER:
            case SECRETARY:
            case WINGS:
            case BRANCH:
            case NATIONAL_PARLIAMENT:
                return false;
            case DIVISION:
            case UNIT:
                return true;
        }
        return false;
    }

    @Override
    public String toString() {
        return "$Office_unitsDTO[" +
               " iD = " + iD +
               " officeMinistryId = " + officeMinistryId +
               " officeLayerId = " + officeLayerId +
               " officeId = " + officeId +
               " officeOriginUnitId = " + officeOriginUnitId +
               " unitNameBng = " + unitNameBng +
               " unitNameEng = " + unitNameEng +
               " officeUnitCategory = " + officeUnitCategory +
               " parentUnitId = " + parentUnitId +
               " parentOriginUnitId = " + parentOriginUnitId +
               " unitNothiCode = " + unitNothiCode +
               " unitLevel = " + unitLevel +
               " sarokNoStart = " + sarokNoStart +
               " email = " + email +
               " phone = " + phone +
               " fax = " + fax +
               " activeStatus = " + activeStatus +
               " createdBy = " + createdBy +
               " modifiedBy = " + modifiedBy +
               " created = " + created +
               " modified = " + modified +
               " status = " + status +
               " isDeleted = " + isDeleted +
               " lastModificationTime = " + lastModificationTime +
               "]";
    }

    public String nameAndId() {
        return String.format("id : %d  name : %s", iD, unitNameEng);
    }
}