package office_units;

import com.google.gson.Gson;

import building.BuildingBlockDTO;
import building.BuildingBlockRepository;
import building.BuildingDAO;
import building.BuildingDTO;
import building.BuildingRepository;
import common.BaseServlet;
import common.CacheUpdateModel;
import common.CommonDAOService;
import employee_assign.EmployeeAssignRepository;
import employee_assign.OfficeUnitAndOrganogramModel;
import employee_offices.EmployeeOfficeDTO;
import employee_offices.EmployeeOfficeRepository;
import employee_offices.EmployeeOfficesDAO;
import language.LC;
import language.LM;
import login.LoginDTO;
import office_unit_organogram.Office_unit_organogramDAO;
import office_unit_organograms.OfficeUnitOrganogramsRepository;

import office_building.*;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import pb.Utils;
import permission.MenuConstants;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.CommonConstant;
import util.CommonDTO;
import util.HttpRequestUtils;
import workflow.WorkflowController;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@WebServlet("/Office_unitsServlet")
@MultipartConfig
public class Office_unitsServlet extends BaseServlet {
    private final Office_unitsDAO officeUnitsDAO = Office_unitsDAO.getInstance();

    @Override
    public String getTableName() {
        return officeUnitsDAO.getTableName();
    }

    @Override
    public String getServletName() {
        return "Office_unitsServlet";
    }

    @Override
    public CommonDAOService<? extends CommonDTO> getCommonDAOService() {
        return officeUnitsDAO;
    }

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        boolean isLangEng = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
        logger.debug("addT called = 1" );
        
        long currentTime = System.currentTimeMillis();
        Office_unitsDTO officeUnitDTO;
        if (addFlag) {
            officeUnitDTO = new Office_unitsDTO();
            officeUnitDTO.created = currentTime;
            officeUnitDTO.createdBy = (int) userDTO.ID;
            officeUnitDTO.officeMinistryId = 62;
            officeUnitDTO.officeLayerId = 192;
            officeUnitDTO.officeId = 2294;
            officeUnitDTO.officeOriginUnitId = 1;
            officeUnitDTO.parentOriginUnitId = 1;
            officeUnitDTO.unitNothiCode = "0";
            officeUnitDTO.unitLevel = 0;
            officeUnitDTO.sarokNoStart = 0;
            officeUnitDTO.activeStatus = true;
            officeUnitDTO.status = true;
        } else {
            officeUnitDTO = officeUnitsDAO.getDTOFromID(Long.parseLong(request.getParameter("iD")));
            if (officeUnitDTO == null) {
                throw new Exception(isLangEng ? "Office information is not found" : "দপ্তরের তথ্য পাওয়া যায়নি");
            }
        }
        officeUnitDTO.modified = officeUnitDTO.lastModificationTime = currentTime + 180000;//3mins delay
        officeUnitDTO.modifiedBy = (int) userDTO.ID;
        long oldParentUnitId = officeUnitDTO.parentUnitId;
        officeUnitDTO.parentUnitId = Long.parseLong(request.getParameter("parentUnitId"));
        Office_unitsDTO parentOfficeUnitsDTO = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(officeUnitDTO.parentUnitId);
        if (parentOfficeUnitsDTO == null) {
            throw new Exception(isLangEng ? "Superior office information is not found" : "উর্ধতন দপ্তরের তথ্য পাওয়া যায়নি");
        }
        officeUnitDTO.officeOrder = parentOfficeUnitsDTO.officeOrder + 1;
        officeUnitDTO.unitNameEng = Jsoup.clean(request.getParameter("unitNameEng"), Whitelist.simpleText());
        if (officeUnitDTO.unitNameEng == null || officeUnitDTO.unitNameEng.trim().length() == 0) {
            throw new Exception(isLangEng ? "Please write office unit name in english" : "অনুগ্রহ করে ইংরেজীতে দপ্তরের নাম লিখুন");
        }
        officeUnitDTO.unitNameBng = Jsoup.clean(request.getParameter("unitNameBng"), Whitelist.simpleText());
        if (officeUnitDTO.unitNameBng == null || officeUnitDTO.unitNameBng.trim().length() == 0) {
            throw new Exception(isLangEng ? "Please write office unit name in bangla" : "অনুগ্রহ করে বাংলায় দপ্তরের নাম লিখুন");
        }
        officeUnitDTO.address = Utils.doJsoupCleanOrReturnEmpty(request.getParameter("address"));
        officeUnitDTO.budgetOfficeId = Utils.parseOptionalLong(request.getParameter("budgetOfficeId"), -1L, null);

        officeUnitDTO.isAbstractOffice = request.getParameter("isAbstractOffice") != null && request.getParameter("isAbstractOffice").equals("true");


        
        String[] officeBuilDingIds = request.getParameterValues("officeBuilding.iD");
        List<Office_buildingDTO> office_buildings = new ArrayList<>();
        if(officeBuilDingIds != null)
        {
        	logger.debug("officeBuilDingIds.length = " + officeBuilDingIds.length);
        	for(int j = 0; j < officeBuilDingIds.length; j ++)
        	{
        		Office_buildingDTO office_buildingDTO = new Office_buildingDTO();
        		office_buildingDTO.buildingType = Long.parseLong(request.getParameterValues("officeBuilding.buildingType")[j]);
        		office_buildingDTO.buildingBlockType = Long.parseLong(request.getParameterValues("officeBuilding.buildingBlockType")[j]);
        		office_buildingDTO.buildingLevel = Integer.parseInt(request.getParameterValues("officeBuilding.buildingLevel")[j]);
        		office_buildingDTO.roomNo = Utils.doJsoupCleanOrReturnEmpty(request.getParameterValues("officeBuilding.roomNo")[j]);
        		office_buildingDTO.iD = Long.parseLong(officeBuilDingIds[j]);
                office_buildingDTO.lastModificationTime = officeUnitDTO.lastModificationTime;
        		office_buildings.add(office_buildingDTO);
        	}
        }
        else
        {
        	logger.debug("officeBuilDingIds = " + "null");
        }

        CommonDAOService.CACHE_UPDATE_MODEL_THREAD_LOCAL.set(new CacheUpdateModel(officeUnitDTO.lastModificationTime));
        Utils.handleTransaction(() -> {
            if (addFlag) {
                long id = officeUnitsDAO.add(officeUnitDTO);
                officeUnitDTO.orgTree = parentOfficeUnitsDTO.orgTree + "P" + id;
                officeUnitsDAO.updateOrgTreeById(officeUnitDTO.orgTree, id, officeUnitDTO.lastModificationTime);
            } else {
                officeUnitsDAO.update(officeUnitDTO);
                if (oldParentUnitId != officeUnitDTO.parentUnitId) {
                    String oldOrgTree = officeUnitDTO.orgTree;
                    officeUnitDTO.orgTree = parentOfficeUnitsDTO.orgTree + "P" + officeUnitDTO.iD;
                    officeUnitsDAO.updateOrgTreeById(officeUnitDTO.orgTree, officeUnitDTO.iD, officeUnitDTO.lastModificationTime);
                    officeUnitsDAO.updateOrgTreeByOrgTree(officeUnitDTO.orgTree + "P", oldOrgTree + "P", officeUnitDTO.lastModificationTime);
                    Office_unit_organogramDAO.getInstance().updateOrgTreeByChangingParent(officeUnitDTO.iD, officeUnitDTO.parentUnitId, officeUnitDTO.lastModificationTime);
                }
            }
            Office_buildingDAO office_buildingDAO = Office_buildingDAO.getInstance();
            if (!addFlag)
            {
            	office_buildingDAO.deleteChildrenByParent(officeUnitDTO.iD, "office_units_id",officeUnitDTO.lastModificationTime);
            }
            
            for(Office_buildingDTO office_buildingDTO: office_buildings)
            {
            	office_buildingDTO.officeUnitsId = officeUnitDTO.iD;
            	office_buildingDAO.add(office_buildingDTO);
            }

        });
        Utils.executeCache();
        return officeUnitDTO;
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[]{MenuConstants.ADD_OFFICE_UNIT};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[]{MenuConstants.SEARCH_OFFICE_UNIT};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[]{MenuConstants.SEARCH_OFFICE_UNIT};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Office_unitsServlet.class;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }

        String actionType = request.getParameter("actionType");
        String data = "";
        if (actionType != null) {
            switch (actionType) {
                case "ajax_office_units": {
                    List<OfficeUnitAndOrganogramModel> modelList = EmployeeAssignRepository.getInstance().getAllOfficeUnits();
                    List<Map<String, Object>> result = modelList.stream()
                            .map(e -> {
                                Map<String, Object> modelMap = new HashMap<>();
                                modelMap.put("id", e.officeUnitId);
                                modelMap.put("unit_name_eng", e.unitNameEng);
                                modelMap.put("unit_name_bng", e.unitNameBng);
                                return modelMap;
                            })
                            .collect(Collectors.toList());
                    data = new Gson().toJson(result);
                    break;
                }
                case "ajax_designation_count": {
                    Map<String, Object> result = EmployeeOfficeRepository.getInstance()
                            .getDesignationCountList(Long.parseLong(request.getParameter("office_units_id")), request.getParameter("language"));
                    data = new Gson().toJson(result);
                    break;
                }
                case "ajax_age_count": {
                    long officeId = Long.parseLong(request.getParameter("office_units_id"));
                    Map<String, Integer> result = EmployeeOfficesDAO.getInstance().getAgeRangeList(officeId);
                    data = new Gson().toJson(result);
                    break;
                }
                case "ajax_gender_count": {
                    Map<String, Object> result =
                            EmployeeOfficeRepository.getInstance()
                                    .getGenderList(Long.parseLong(request.getParameter("office_units_id")), request.getParameter("language"));
                    data = new Gson().toJson(result);
                }
                break;
                case "ajax_employee_office": {
                    long empId = userDTO.employee_record_id;
                    boolean isLeafNode = EmployeeOfficesDAO.getInstance().getIsLeafNode(empId);
                    sendOfficeOptions(request, response, isLeafNode);
                    break;
                }
                case "ajax_office": {
                    sendOfficeOptions(request, response, false);
                    break;
                }
                case "ajax_buildingByOrg": {
                    long orgId = Long.parseLong(request.getParameter("orgId"));
                    long officeId = WorkflowController.getOfficeIdFromOrganogramId(orgId);
                    String options = Office_buildingRepository.getInstance().getOptions(officeId, userDTO.languageID == CommonConstant.Language_ID_English);
                    response.getWriter().write(options);
                    break;
                }
                case "ajax_getAllLayerOptions": {
                    long selectedOfficeUnitId = Long.parseLong(request.getParameter("selectedOfficeUnitId"));
                    String language = userDTO.languageID == CommonConstant.Language_ID_English ? "English" : "Bangla";
                    data = new Gson().toJson(
                            Office_unitsRepository.getInstance().buildAllLayerOptions(language, selectedOfficeUnitId)
                    );
                    break;
                }
                case "ajax_setBlockLevel": {
                    long buildingId = Long.parseLong(request.getParameter("buildingId"));
                    long blockId = Long.parseLong(request.getParameter("blockId"));
                    int levelId = Integer.parseInt(request.getParameter("levelId"));
                    String language = userDTO.languageID == CommonConstant.Language_ID_English ? "English" : "Bangla";
                    List<BuildingBlockDTO> buildingBlockDTOs = BuildingBlockRepository.getInstance().getBuildingBlockDTOBybuildingId(buildingId);
                    boolean isLangEng = userDTO.languageID == CommonConstant.Language_ID_English;
                    BuildingOptions buildingOptions = new BuildingOptions();
                    buildingOptions.blocks ="<option value='-1'> " + LM.getText(LC.HM_SELECT, loginDTO) + "</option>";
                    
                	for(BuildingBlockDTO buildingBlockDTO: buildingBlockDTOs)
                	{
                		buildingOptions.blocks += "<option value = '" + buildingBlockDTO.iD + "' "
                				+ (buildingBlockDTO.iD == blockId? "selected":"") + ">"
                				+ (isLangEng?buildingBlockDTO.nameEn:buildingBlockDTO.nameBn) + "</option>";
                		
                	}
                	
                	BuildingDTO buildingDTO = BuildingRepository.getInstance().getBuildingDTOByiD(buildingId);
                	if(buildingDTO == null)
                	{
                		buildingDTO = new BuildingDTO();
                	}
                	if(buildingDTO.minimumLevel <= buildingDTO.maximumLevel)
                	{
                		for(int j = buildingDTO.minimumLevel; j <= buildingDTO.maximumLevel; j ++)
                    	{
                    		buildingOptions.levels += "<option value = '" + j + "' "
                    				+ (j == levelId? "selected":"") + ">"
                    				+ Utils.getDigits(j, language) + "</option>";
                    	}
                	}
                	else
                	{
                		buildingOptions.levels += "<option value = '" + 1 + "' "
                				+ (1 == blockId? "selected":"") + ">"
                				+ Utils.getDigits(1, language) + "</option>";
                	}
                	
                	logger.debug(" blocks = " + buildingOptions.blocks);
                	logger.debug(" levels = " + buildingOptions.levels);
                	
                	
                	data = new Gson().toJson(buildingOptions);
                   
                    break;
                }
                case "ajax_designation":
                    long officeId = Long.parseLong(request.getParameter("office_id"));
                    Office_unitsDTO officeUnitsDTO = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(officeId);
                    String language = request.getParameter("language");
                    String designationOption = OfficeUnitOrganogramsRepository.getInstance().buildOptions(language, officeId, officeUnitsDTO.parentUnitId);
                    PrintWriter out = response.getWriter();
                    out.println(designationOption);
                    out.close();
                    return;
                case "ajax_model": {
                    long id = Long.parseLong(request.getParameter("officeId"));
                    OfficeUnitModel model = Office_unitsRepository.getInstance().getByOfficeUnitId(id);
                    data = new Gson().toJson(model);
                }
                break;

                case "ajax_concrete_childList": {
                    long parentId = Long.parseLong(request.getParameter("id"));
                    List<Office_unitsDTO> childList = Office_unitsRepository.getInstance().getConcreteChildOffices(parentId);
                    data = new Gson().toJson(childList.stream().map(Office_unitsDTO::nameAndId).collect(Collectors.toList()));
                }
                break;
                case "ajax_concrete_parent": {
                    long id = Long.parseLong(request.getParameter("id"));
                    Office_unitsDTO dto = Office_unitsRepository.getInstance().getConcreteParentIncludingSelf(id);
                    if (dto == null) {
                        data = "{}";
                    } else {
                        data = String.format("id : %d, name : %s, parent_id : %d", dto.iD, dto.unitNameEng, dto.parentUnitId);
                    }
                }
                break;
                default:
                    super.doGet(request, response);
                    return;
            }
        }
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();
        out.print(data);
        out.flush();
        out.close();
    }

    void sendOfficeOptions(HttpServletRequest request, HttpServletResponse response, boolean isLeafNode) throws IOException {
        String parentIdStr = request.getParameter("parent_id");
        long parentId;
        if (parentIdStr == null) {
            UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;
            EmployeeOfficeDTO employeeDefaultOfficeDTO = EmployeeOfficesDAO.getInstance().getByEmployeeRecordIdIsDefault(userDTO.employee_record_id);
            parentId = employeeDefaultOfficeDTO.officeUnitId;
        } else if (!parentIdStr.equals("")) {
            parentId = Long.parseLong(parentIdStr);
        } else {
            parentId = -1;
        }
        String language = request.getParameter("language");
        PrintWriter out = response.getWriter();
        if (isLeafNode) {
            out.println("");
        } else {
            out.println(Office_unitsRepository.getInstance().buildOptionsByParentId(language, parentId, null));
        }
        out.close();
    }
}