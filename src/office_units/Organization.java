package office_units;

/*
 * @author Md. Erfan Hossain
 * @created 29/12/2021 - 1:42 PM
 * @project parliament
 */
//NATIONAL PARLIAMENT
//NATIONAL PARLIAMENT SECRETARIAT
public enum Organization {
	NATIONAL_PARLIAMENT("NATIONAL PARLIAMENT","জাতীয় সংসদ", 1),
	NATIONAL_PARLIAMENT_SECRETARIAT("NATIONAL PARLIAMENT SECRETARIAT","জাতীয় সংসদ সচিবালয়", 2),
	OTHER("OTHER","অন্যান্য", 3);
	private final String nameEn;
    private final String nameBn;
    private final long id;

    Organization(String nameEn, String nameBn, long id) {
        this.nameEn = nameEn;
        this.nameBn = nameBn;
        this.id = id;
    }
    
    public long getId()
    {
    	return id;
    }

    public String getNameBn() {
        return nameBn;
    }

    public String getNameEn() {
        return nameEn;
    }
}
