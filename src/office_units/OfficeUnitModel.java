package office_units;

/*
 * @author Md. Erfan Hossain
 * @created 24/08/2021 - 1:35 PM
 * @project parliament
 */

public class OfficeUnitModel {
    public boolean isSecretaryOrUnderOffice = false;
    public boolean isSecretaryOrJsOrDs = false;
    public long superiorOrganogramId = 0;
}
