package budget_password;
import java.util.*; 
import util.*;


public class Budget_passwordMAPS extends CommonMaps
{	
	public Budget_passwordMAPS(String tableName)
	{
		

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("budgetSelectionId".toLowerCase(), "budgetSelectionId".toLowerCase());
		java_DTO_map.put("budgetOfficeId".toLowerCase(), "budgetOfficeId".toLowerCase());
		java_DTO_map.put("password".toLowerCase(), "password".toLowerCase());
		java_DTO_map.put("searchColumn".toLowerCase(), "searchColumn".toLowerCase());
		java_DTO_map.put("insertedBy".toLowerCase(), "insertedBy".toLowerCase());
		java_DTO_map.put("modifiedBy".toLowerCase(), "modifiedBy".toLowerCase());
		java_DTO_map.put("insertionDate".toLowerCase(), "insertionDate".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("budget_selection_id".toLowerCase(), "budgetSelectionId".toLowerCase());
		java_SQL_map.put("budget_office_id".toLowerCase(), "budgetOfficeId".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Budget Selection Id".toLowerCase(), "budgetSelectionId".toLowerCase());
		java_Text_map.put("Budget Office Id".toLowerCase(), "budgetOfficeId".toLowerCase());
		java_Text_map.put("Password".toLowerCase(), "password".toLowerCase());
		java_Text_map.put("Search Column".toLowerCase(), "searchColumn".toLowerCase());
		java_Text_map.put("Inserted By".toLowerCase(), "insertedBy".toLowerCase());
		java_Text_map.put("Modified By".toLowerCase(), "modifiedBy".toLowerCase());
		java_Text_map.put("Insertion Date".toLowerCase(), "insertionDate".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}