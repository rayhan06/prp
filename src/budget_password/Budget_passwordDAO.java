package budget_password;

import common.CommonDAOService;
import common.ConnectionAndStatementUtil;
import org.apache.log4j.Logger;
import sessionmanager.SessionConstants;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Budget_passwordDAO implements CommonDAOService<Budget_passwordDTO> {
    private static final Logger logger = Logger.getLogger(Budget_passwordDAO.class);

    private static final String addQuery = "INSERT INTO {tableName} (budget_selection_id,budget_office_id,password,search_column,"
            .concat("user_name,office_unit_id,office_unit_eng,office_unit_bng,")
            .concat("office_unit_organogram_id,office_unit_organogram_eng,office_unit_organogram_bng,")
            .concat("user_name_en,user_name_bn,employee_record_id,")
            .concat("modified_by,lastModificationTime,inserted_by,insertion_date,isDeleted,ID) ")
            .concat("VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

    private static final String getBySelectionIdOfficeId = "SELECT * FROM budget_password WHERE budget_selection_id = ? AND budget_office_id = ? AND isDeleted = 0";

    private static final String getDTOsByBudgetSelectionId = "SELECT * FROM budget_password WHERE budget_selection_id = ? AND isDeleted =  0 ORDER BY budget_office_id";

    private final Map<String, String> searchMap = new HashMap<>();

    private static Budget_passwordDAO INSTANCE = null;

    public static Budget_passwordDAO getInstance() {
        if (INSTANCE == null) {
            synchronized (Budget_passwordDAO.class) {
                if (INSTANCE == null) {
                    INSTANCE = new Budget_passwordDAO();
                }
            }
        }
        return INSTANCE;
    }

    private Budget_passwordDAO() {
        searchMap.put("AnyField", " AND search_column LIKE ? ");
        searchMap.put("budgetSelectionId", " AND budget_selection_id = ? ");
        searchMap.put("budgetOfficeId", " AND budget_office_id = ? ");
    }

    public void setSearchColumn(Budget_passwordDTO budget_passwordDTO) {
        budget_passwordDTO.searchColumn =
                budget_passwordDTO.userNameEn.concat(" ")
                        .concat(budget_passwordDTO.userNameBN).concat(" ")
                        .concat(budget_passwordDTO.OfficeUnitEng).concat(" ")
                        .concat(budget_passwordDTO.OfficeUnitBng).concat(" ")
                        .concat(budget_passwordDTO.OfficeUnitOrganogramEng).concat(" ")
                        .concat(budget_passwordDTO.OfficeUnitOrganogramBng).concat(" ");
    }

    @Override
    public void set(PreparedStatement ps, Budget_passwordDTO budget_passwordDTO, boolean isInsert) throws SQLException {
        setSearchColumn(budget_passwordDTO);
        int index = 0;
        ps.setLong(++index, budget_passwordDTO.budgetSelectionId);
        ps.setLong(++index, budget_passwordDTO.budgetOfficeId);
        ps.setString(++index, budget_passwordDTO.password);
        ps.setString(++index, budget_passwordDTO.searchColumn);
        ps.setString(++index, budget_passwordDTO.userName);
        ps.setLong(++index, budget_passwordDTO.officeUnitId);
        ps.setString(++index, budget_passwordDTO.OfficeUnitEng);
        ps.setString(++index, budget_passwordDTO.OfficeUnitBng);
        ps.setLong(++index, budget_passwordDTO.officeUnitOrganogramId);
        ps.setString(++index, budget_passwordDTO.OfficeUnitOrganogramEng);
        ps.setString(++index, budget_passwordDTO.OfficeUnitOrganogramBng);
        ps.setString(++index, budget_passwordDTO.userNameEn);
        ps.setString(++index, budget_passwordDTO.userNameBN);
        ps.setLong(++index, budget_passwordDTO.employeeRecordId);
        ps.setString(++index, budget_passwordDTO.modifiedBy);
        ps.setLong(++index, budget_passwordDTO.lastModificationTime);
        if (isInsert) {
            ps.setString(++index, budget_passwordDTO.insertedBy);
            ps.setLong(++index, budget_passwordDTO.insertionDate);
            ps.setInt(++index, 0 /* isDeleted */);
        }
        ps.setLong(++index, budget_passwordDTO.iD);
    }

    @Override
    public Budget_passwordDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Budget_passwordDTO budget_passwordDTO = new Budget_passwordDTO();
            budget_passwordDTO.iD = rs.getLong("ID");
            budget_passwordDTO.budgetSelectionId = rs.getLong("budget_selection_id");
            budget_passwordDTO.budgetOfficeId = rs.getLong("budget_office_id");
            budget_passwordDTO.password = rs.getString("password");
            budget_passwordDTO.searchColumn = rs.getString("search_column");
            budget_passwordDTO.insertedBy = rs.getString("inserted_by");
            budget_passwordDTO.modifiedBy = rs.getString("modified_by");
            budget_passwordDTO.insertionDate = rs.getLong("insertion_date");

            budget_passwordDTO.userName = rs.getString("user_name");
            budget_passwordDTO.officeUnitId = rs.getLong("office_unit_id");
            budget_passwordDTO.OfficeUnitEng = rs.getString("office_unit_eng");
            budget_passwordDTO.OfficeUnitBng = rs.getString("office_unit_bng");
            budget_passwordDTO.officeUnitOrganogramId = rs.getLong("office_unit_organogram_id");
            budget_passwordDTO.OfficeUnitOrganogramEng = rs.getString("office_unit_organogram_eng");
            budget_passwordDTO.OfficeUnitOrganogramBng = rs.getString("office_unit_organogram_bng");

            budget_passwordDTO.userNameEn = rs.getString("user_name_en");
            budget_passwordDTO.userNameBN = rs.getString("user_name_bn");
            budget_passwordDTO.employeeRecordId = rs.getLong("employee_record_id");

            budget_passwordDTO.isDeleted = rs.getInt("isDeleted");
            budget_passwordDTO.lastModificationTime = rs.getLong("lastModificationTime");

            return budget_passwordDTO;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    @Override
    public String getTableName() {
        return "budget_password";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    @Override
    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Budget_passwordDTO) commonDTO, addQuery, true);
    }

    @Override
    public long update(CommonDTO commonDTO) throws Exception {
        throw new UnsupportedOperationException("Budget Password DAO update Not supported");
    }

    public Budget_passwordDTO getDTOByID(long ID) {
        return getDTOFromID(ID);
    }


    public List<Budget_passwordDTO> getDTOsByBudgetSelectionId(long budgetSelectionId) {
        return ConnectionAndStatementUtil.getListOfT(
                getDTOsByBudgetSelectionId,
                Collections.singletonList(budgetSelectionId),
                this::buildObjectFromResultSet
        );
    }

    public Budget_passwordDTO getDTOBySelectionIdOfficeId(long budgetSelectionId, long budgetOfficeID) throws Exception {
        return ConnectionAndStatementUtil.getT(
                getBySelectionIdOfficeId,
                Arrays.asList(budgetSelectionId, budgetOfficeID),
                this::buildObjectFromResultSet
        );
    }

    public List<String> generatePasswords(int totalOffice) {
        return Stream.generate(() -> generatePassword(SessionConstants.BUDGET_PASSWORD_LENGTH))
                .limit(totalOffice)
                .collect(Collectors.toList());
    }

    public String generatePassword(int length) {
        return new PasswordGenerator.PasswordGeneratorBuilder()
                .useDigits(true)
                .useLower(true)
                .useUpper(true)
                .build()
                .generate(length);
    }
}