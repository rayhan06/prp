package budget_password;

import util.CommonDTO;


public class Budget_passwordDTO extends CommonDTO {

    public long budgetSelectionId = -1;
    public long budgetOfficeId = -1;
    public String password = "";
    public String insertedBy = "";
    public String modifiedBy = "";
    public long insertionDate = -1;

    public String userName = "";
    public long officeUnitId = -1;
    public String OfficeUnitEng = "";
    public String OfficeUnitBng = "";

    public long officeUnitOrganogramId = -1;
    public String OfficeUnitOrganogramEng = "";
    public String OfficeUnitOrganogramBng = "";


    public String userNameEn = "";
    public String userNameBN = "";
    public long employeeRecordId = -1;


    @Override
    public String toString() {
        return "$Budget_passwordDTO[" +
                " iD = " + iD +
                " budgetSelectionId = " + budgetSelectionId +
                " budgetOfficeId = " + budgetOfficeId +
                " password = " + password +
                " searchColumn = " + searchColumn +
                " insertedBy = " + insertedBy +
                " modifiedBy = " + modifiedBy +
                " insertionDate = " + insertionDate +
                " isDeleted = " + isDeleted +
                " lastModificationTime = " + lastModificationTime +
                " userName = " + userName +
                " officeUnitId = " + officeUnitId +
                " OfficeUnitEng = " + OfficeUnitEng +
                " OfficeUnitBng = " + OfficeUnitBng +
                " officeUnitOrganogramId = " + officeUnitOrganogramId +
                " OfficeUnitOrganogramEng = " + OfficeUnitOrganogramEng +
                " OfficeUnitOrganogramBng = " + OfficeUnitOrganogramBng +
                " userNameEn = " + userNameEn +
                " userNameBN = " + userNameBN +
                " employeeRecordId = " + employeeRecordId +
                "]";
    }

}