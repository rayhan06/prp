package budget_password;

public class BudgetPasswordNotificationData {
    public String receiverName;
    public String receiverMobile;
    public String receiverEmail;
    public String economicYear;
    public String budgetOffice;
    public String password;
}
