package budget_password;

import mail.EmailService;
import mail.SendEmailDTO;
import org.apache.log4j.Logger;
import sms.SmsService;

import java.io.IOException;

public class BudgetPasswordNotification {
    private static final Logger logger = Logger.getLogger(BudgetPasswordNotification.class);

    private static final String EMAIL_FROM = "edms@pbrlp.gov.bd";
    private static final String emailSubject = "Budget Password for %s";
    private static final String smsEmailBody = "Dear %s,\nYou are given access to submit Budget for,\n"
            .concat("Economic Year: %s,\nBudget Office: %s,\nPassword: %s");

    public static void sendSms(BudgetPasswordNotificationData notificationData) {
        try {
            if(isInvalidPhoneNumber(notificationData.receiverMobile))
                throw new IOException("Invalid Phone Number : " + notificationData.receiverMobile);

            SmsService.send(notificationData.receiverMobile, getSmsEmailBody(notificationData));
        }catch (IOException e){
            logger.error(e);
        }
    }

    private static boolean isInvalidPhoneNumber(String receiverMobile) {
        if(receiverMobile == null) return true;

        receiverMobile = receiverMobile.trim();
        return (receiverMobile.length() == 11 && !receiverMobile.startsWith("01"))
               || (receiverMobile.length() == 13 && !receiverMobile.startsWith("8801"));
    }

    public static void sendEmail(BudgetPasswordNotificationData notificationData){
        try{
            String mailId = notificationData.receiverEmail;

            if(mailId == null || mailId.trim().length() <= 0)
                throw new IOException("<<<<mail id is null or empty, mailId : " + mailId);

            SendEmailDTO sendEmailDTO = new SendEmailDTO();
            sendEmailDTO.setFrom(EMAIL_FROM);
            sendEmailDTO.setTo(new String[]{mailId});
            sendEmailDTO.setSubject(String.format(emailSubject,notificationData.economicYear));
            sendEmailDTO.setText(getSmsEmailBody(notificationData));

            new EmailService().sendMail(sendEmailDTO);
        }catch (Exception e){
            logger.error(e);
        }
    }

    private static String getSmsEmailBody(BudgetPasswordNotificationData notificationData){
        return String.format(
                smsEmailBody,
                notificationData.receiverName,
                notificationData.economicYear,
                notificationData.budgetOffice,
                notificationData.password
        );
    }
}
