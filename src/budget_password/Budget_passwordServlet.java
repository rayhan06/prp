package budget_password;

import budget.BudgetUtils;
import budget_office.Budget_officeDTO;
import budget_office.Budget_officeRepository;
import budget_selection_info.BudgetSelectionInfoDAO;
import budget_selection_info.BudgetSelectionInfoDTO;
import budget_selection_info.BudgetSelectionInfoRepository;
import common.ApiResponse;
import common.BaseServlet;
import common.CommonDAOService;
import employee_offices.EmployeeOfficeDTO;
import employee_offices.EmployeeOfficeRepository;
import employee_records.Employee_recordsDTO;
import employee_records.Employee_recordsRepository;
import login.LoginDTO;
import office_unit_organograms.OfficeUnitOrganograms;
import office_unit_organograms.OfficeUnitOrganogramsRepository;
import office_units.Office_unitsDTO;
import office_units.Office_unitsRepository;
import org.apache.log4j.Logger;
import permission.MenuConstants;
import role.PermissionRepository;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.CommonConstant;
import util.CommonDTO;
import util.StringUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


@WebServlet("/Budget_passwordServlet")
@MultipartConfig
public class Budget_passwordServlet extends BaseServlet {
    private static final long serialVersionUID = 1L;
    private static final Logger logger = Logger.getLogger(Budget_passwordServlet.class);
    private final Budget_passwordDAO budget_passwordDAO = Budget_passwordDAO.getInstance();

    @Override
    public String getTableName() {
        return "budget_password";
    }

    @Override
    public String getServletName() {
        return "Budget_passwordServlet";
    }

    @Override
    public CommonDAOService<? extends CommonDTO> getCommonDAOService() {
        return budget_passwordDAO;
    }

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        boolean isLangEng = userDTO.languageID == CommonConstant.Language_ID_English;

        // code selection and password have the same logic for economic year
        String budgetYear = BudgetUtils.getEconomicYearForCodeSelection(System.currentTimeMillis());
        BudgetSelectionInfoDTO budgetSelectionInfoDTO = BudgetSelectionInfoDAO.getInstance().getDTOByEconomicYear(budgetYear);
        if (budgetSelectionInfoDTO == null || !budgetSelectionInfoDTO.isSubmitted) {
            throw new Exception(
                    isLangEng ? "Budget code selection process has not yet been completed for Economic Year ".concat(budgetYear)
                            : StringUtils.convertToBanNumber(budgetYear).concat(" অর্থ বছরের বাজেটের কোড বাছাইকরণ প্রক্রিয়া এখনো সম্পন্ন হয়নি")
            );
        }

        List<Budget_passwordDTO> budgetPasswordDTOList = budget_passwordDAO.getDTOsByBudgetSelectionId(budgetSelectionInfoDTO.iD);
        if (budgetPasswordDTOList != null && budgetPasswordDTOList.size() > 0) {
            throw new Exception(
                    isLangEng ? "Password is already created for Economic Year " + budgetYear
                            : StringUtils.convertToBanNumber(budgetYear) + " অর্থ বছরের জন্য ইতিমধ্যে পাসওয়ার্ড তৈরি করে হয়ে গেছে"
            );
        }

        List<Budget_officeDTO> budgetOfficeDTOList = Budget_officeRepository.getInstance().getAllOfficeList();
        long time = System.currentTimeMillis();
        budgetPasswordDTOList = new ArrayList<>();
        for (int i = budgetOfficeDTOList.size() - 1; i >= 0; i--) {
            OfficeUnitOrganograms officeUnitOrganograms;
            Office_unitsDTO officeUnitsDTO = null;
            EmployeeOfficeDTO employeeOfficeDTO = null;
            Employee_recordsDTO employeeRecordsDTO = null;
            try {
                long organogramId = Long.parseLong(request.getParameter("officeUnitOrganogramId_" + i));
                officeUnitOrganograms = OfficeUnitOrganogramsRepository.getInstance().getById(organogramId);
                if (officeUnitOrganograms != null) {
                    officeUnitsDTO = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(officeUnitOrganograms.office_unit_id);
                    if (officeUnitsDTO != null) {
                        employeeOfficeDTO = EmployeeOfficeRepository.getInstance().getByOfficeUnitOrganogramId(organogramId);
                        if (employeeOfficeDTO != null) {
                            employeeRecordsDTO = Employee_recordsRepository.getInstance().getById(employeeOfficeDTO.employeeRecordId);
                        }
                    }
                }
                if (officeUnitOrganograms == null || officeUnitsDTO == null || employeeOfficeDTO == null || employeeRecordsDTO == null) {
                    throw new Exception(isLangEng ? "Employee is missing for " + budgetOfficeDTOList.get(i).nameEn :
                            budgetOfficeDTOList.get(i).nameBn + " এর জন্য কর্মকর্তা পাওয়া যায় নাই");
                }
            } catch (NumberFormatException ex) {
                throw new Exception(isLangEng ? "Employee is missing for " + budgetOfficeDTOList.get(i).nameEn :
                        budgetOfficeDTOList.get(i).nameBn + " এর জন্য কর্মকর্তা পাওয়া যায় নাই");
            }
            String password = request.getParameter("password_" + i);
            if (password == null) {
                throw new Exception(isLangEng ? "Password value is missing for " + budgetOfficeDTOList.get(i).nameEn :
                        budgetOfficeDTOList.get(i).nameBn + " এর জন্য পাসওয়ার্ড পাওয়া যায় নাই");
            } else if (password.trim().length() != SessionConstants.BUDGET_PASSWORD_LENGTH) {
                throw new Exception(isLangEng ? "Password is corrupted for " + budgetOfficeDTOList.get(i).nameEn :
                        budgetOfficeDTOList.get(i).nameBn + " এর জন্য পাসওয়ার্ড নষ্ট হয়ে গেছে");
            }

            Budget_passwordDTO dto = new Budget_passwordDTO();
            dto.budgetSelectionId = budgetSelectionInfoDTO.iD;
            dto.budgetOfficeId = budgetOfficeDTOList.get(i).iD;
            dto.password = password.trim();

            dto.userName = employeeRecordsDTO.employeeNumber;
            dto.employeeRecordId = employeeRecordsDTO.iD;
            dto.userNameEn = employeeRecordsDTO.nameEng;
            dto.userNameBN = employeeRecordsDTO.nameBng;

            dto.officeUnitId = officeUnitsDTO.iD;
            dto.OfficeUnitEng = officeUnitsDTO.unitNameEng;
            dto.OfficeUnitBng = officeUnitsDTO.unitNameBng;

            dto.officeUnitOrganogramId = officeUnitOrganograms.id;
            dto.OfficeUnitOrganogramEng = officeUnitOrganograms.designation_eng;
            dto.OfficeUnitOrganogramBng = officeUnitOrganograms.designation_bng;

            dto.insertionDate = dto.lastModificationTime = time;
            dto.insertedBy = dto.modifiedBy = String.valueOf(userDTO.employee_record_id);
            budgetPasswordDTOList.add(dto);
        }

        for (Budget_passwordDTO dto : budgetPasswordDTOList) {
            budget_passwordDAO.add(dto);
        }
        prepareAndSendBudgetNotification(budgetPasswordDTOList);
        return null;
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[0];
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[0];
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[0];
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Budget_passwordServlet.class;
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }

        try {
            String actionType = request.getParameter("actionType");
            switch (actionType) {
                case "getAddPage":
                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.BUDGET_PASSWORD_ADD)) {
                        BudgetSelectionInfoDAO budgetSelectionInfoDAO = BudgetSelectionInfoDAO.getInstance();
                        BudgetSelectionInfoDTO budgetSelectionInfoDTO = budgetSelectionInfoDAO.getOrCreateAndGetDTO(
                                BudgetUtils.getEconomicYearForCodeSelection(System.currentTimeMillis()),
                                userDTO
                        );
                        if (isPasswordAlreadyAdded(budgetSelectionInfoDTO)) {
                            request.setAttribute("budgetSelectionInfoDTO", budgetSelectionInfoDTO);
                            request.getRequestDispatcher("budget_password/budget_passwordView.jsp").forward(request, response);
                        } else {
                            super.doGet(request, response);
                        }
                        return;
                    }
                    break;
                case "regeneratePassword":
                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.BUDGET_PASSWORD_ADD)) {
                        int totalOffice = Integer.parseInt(request.getParameter("totalOffice"));
                        PrintWriter out = response.getWriter();
                        out.print(Budget_passwordDAO.getInstance().generatePasswords(totalOffice));
                        out.flush();
                        return;
                    }
                    break;
                default:
                    super.doGet(request, response);
                    return;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error(ex);
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }

        try {
            String actionType = request.getParameter("actionType");
            switch (actionType){
                case "ajax_add":
                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.BUDGET_PASSWORD_ADD)) {
                        ApiResponse apiResponse;
                        try {
                            addT(request, true, userDTO);
                            apiResponse = ApiResponse.makeSuccessResponse("Budget_passwordServlet?actionType=getAddPage");
                        } catch (Exception ex) {
                            apiResponse = ApiResponse.makeErrorResponse(ex.getMessage());
                        }
                        PrintWriter pw = response.getWriter();
                        pw.write(apiResponse.getJSONString());
                        pw.flush();
                        pw.close();
                        return;
                    }
                    break;
                case "ajax_regenerateSinglePassword":
                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.BUDGET_PASSWORD_ADD)) {
                        ApiResponse apiResponse = ApiResponse.makeSuccessResponse(Budget_passwordDAO.getInstance().generatePassword(SessionConstants.BUDGET_PASSWORD_LENGTH));
                        PrintWriter out = response.getWriter();
                        out.print(apiResponse.getJSONString());
                        out.flush();
                        return;
                    }
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error(ex);
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }

    private void prepareAndSendBudgetNotification(List<Budget_passwordDTO> budgetPasswordDTOList) {
        ExecutorService executorService = Executors.newSingleThreadExecutor((r) -> {
            Thread thread = new Thread(r);
            thread.setDaemon(true);
            return thread;
        });
        executorService.execute(
                () -> budgetPasswordDTOList.stream()
                        .map(this::buildNotificationData)
                        .filter(Objects::nonNull)
                        .forEach(notificationData -> {
                            BudgetPasswordNotification.sendSms(notificationData);
                            BudgetPasswordNotification.sendEmail(notificationData);
                        })
        );
        executorService.shutdown();
    }

    private BudgetPasswordNotificationData buildNotificationData(Budget_passwordDTO dto) {
        BudgetSelectionInfoDTO selectionInfoDTO = BudgetSelectionInfoRepository.getInstance().getDTOByID(dto.budgetSelectionId);
        if (selectionInfoDTO == null) return null;

        Budget_officeDTO officeDTO = Budget_officeRepository.getInstance().getById(dto.budgetOfficeId);
        if (officeDTO == null) return null;

        Employee_recordsDTO employeeRecordsDTO = Employee_recordsRepository.getInstance().getById(dto.employeeRecordId);
        if (employeeRecordsDTO == null) return null;

        BudgetPasswordNotificationData notificationData = new BudgetPasswordNotificationData();
        notificationData.receiverName = employeeRecordsDTO.nameEng;
        notificationData.receiverMobile = employeeRecordsDTO.personalMobile;
        notificationData.receiverEmail = employeeRecordsDTO.personalEml;
        notificationData.economicYear = selectionInfoDTO.economicYear;
        notificationData.budgetOffice = officeDTO.nameEn;
        notificationData.password = dto.password;

        return notificationData;
    }

    public boolean isPasswordAlreadyAdded(BudgetSelectionInfoDTO budgetSelectionInfoDTO) {
        if (budgetSelectionInfoDTO == null) return false;

        List<Budget_passwordDTO> dtoList = budget_passwordDAO.getDTOsByBudgetSelectionId(budgetSelectionInfoDTO.iD);
        return dtoList != null && dtoList.size() > 0;
    }
}