package occupation;


import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;

import common.BaseServlet;
import common.NameDao;
import common.NameRepository;
import common.NameInterface;
import permission.MenuConstants;
import sessionmanager.SessionConstants;
import language.LC;
import user.UserDTO;
import util.CommonDTO;

@WebServlet("/OccupationServlet")
@MultipartConfig
public class OccupationServlet extends BaseServlet implements NameInterface {

	public String commonPartOfDispatchURL(){
		return  "common/name";
	}

	@Override
	public String getTableName() {
		return OccupationDAO.getInstance().getTableName();
	}

	@Override
	public String getServletName() {
		return "OccupationServlet";
	}

	@Override
	public NameDao getCommonDAOService() {
		return OccupationDAO.getInstance();
	}

	@Override
	public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
		return addT(request,addFlag,userDTO,OccupationDAO.getInstance());
	}

	@Override
	public int[] getAddPageMenuConstants() {
		return new int[] {MenuConstants.OCCUPATION_ADD};
	}

	@Override
	public int[] getEditPageMenuConstants() {
		return new int[] {MenuConstants.OCCUPATION_UPDATE};
	}

	@Override
	public int[] getSearchMenuConstants() {
		return new int[] {MenuConstants.OCCUPATION_SEARCH};
	}

	@Override
	public Class<? extends HttpServlet> getClazz() {
		return OccupationServlet.class;
	}

	@Override
	public int getSearchTitleValue() {
		return LC.OCCUPATION_SEARCH_OCCUPATION_SEARCH_FORMNAME;
	}

	@Override
	public String get_p_navigatorName() {
		return SessionConstants.NAV_OCCUPATION;
	}

	@Override
	public String get_p_dtoCollectionName() {
		return SessionConstants.VIEW_OCCUPATION;
	}

	@Override
	public NameRepository getNameRepository() {
		return OccupationRepository.getInstance();
	}

	@Override
	public int getAddTitleValue() {
		return LC.OCCUPATION_ADD_OCCUPATION_ADD_FORMNAME;
	}

	@Override
	public int getEditTitleValue() {
		return LC.OCCUPATION_EDIT_OCCUPATION_EDIT_FORMNAME;
	}

	@Override
	public void init(HttpServletRequest request) throws ServletException {
		setCommonAttributes(request,getServletName(),getCommonDAOService());
	}
}