package occupation;

import common.NameRepository;


public class OccupationRepository extends NameRepository {

	static OccupationRepository instance = null;  
	private OccupationRepository(){
		super(OccupationDAO.getInstance(), OccupationRepository.class);
	}
	
	private static class LazyLoader {
        static OccupationRepository INSTANCE = new OccupationRepository();
    }

    public static OccupationRepository getInstance() {
        return LazyLoader.INSTANCE;
    }
}


