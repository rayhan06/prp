package occupation;

import common.NameDao;

public class OccupationDAO extends NameDao {

    private OccupationDAO() {
        super("occupation");
    }

    private static class LazyLoader{
        static final OccupationDAO INSTANCE = new OccupationDAO();
    }

    public static OccupationDAO getInstance(){
        return OccupationDAO.LazyLoader.INSTANCE;
    }
}
	