package pending_ticket_details_report;


import java.io.IOException;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import language.LC;
import language.LM;

import login.LoginDTO;
import permission.MenuConstants;

import sessionmanager.SessionConstants;
import support_ticket.Support_ticketDTO;
import user.UserDTO;
import user.UserRepository;

import util.ReportRequestHandler;

import pbReport.*;






@WebServlet("/Pending_ticket_details_report_Servlet")
public class Pending_ticket_details_report_Servlet  extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	String[][] Criteria =
	{
		{"criteria","support_ticket","ticket_issues_type","=","","int","","","any","ticketIssuesType", LC.PENDING_TICKET_DETAILS_REPORT_WHERE_TICKETISSUESTYPE + ""},		
		{"criteria","","current_assigned_organogram_id","=","AND","int","","","any","currentAssignedOrganogramId", LC.PENDING_TICKET_DETAILS_REPORT_WHERE_CURRENTASSIGNEDORGANOGRAMID + ""},		
		{"criteria","","issue_raiser_organogram_id","like","AND","String","","","%","issueRaiserOrganogramId", LC.PENDING_TICKET_DETAILS_REPORT_WHERE_ISSUERAISERORGANOGRAMID + ""},		

		{"criteria","","ticket_status_cat","<=","AND","long","","",Support_ticketDTO.OPEN + "","status", LC.HM_END_DATE + ""}
	};
	
	String[][] Display =
	{
		{"display","support_ticket","id","text",""},		
		{"display","support_ticket","issue_raiser_organogram_id","organogram",""},		
		{"display","support_ticket","description","text",""},		
		{"display","support_ticket","ticket_issues_type","type",""},		
		{"display","support_ticket","issue_raiser_organogram_id","organogram_to_wing",""},				
		{"display","support_ticket","priority_cat","cat",""},		
		{"display","support_ticket","current_assigned_organogram_id","organogram",""}		
	};
	
	String GroupBy = "";
	String OrderBY = "";
	
	ReportRequestHandler reportRequestHandler;
	
	public Pending_ticket_details_report_Servlet(){

	}
	
	private final ReportService reportService = new ReportService();
	
	private String sql;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String language = "english";
		if(userDTO!= null && userDTO.languageID == SessionConstants.BANGLA)
		{
			language = "bangla";
		}
				
		String actionType = request.getParameter("actionType");

		System.out.println("In ssservlet doget, actiontype = " + actionType);
		
		sql = "support_ticket";

		Display[0][4] = LM.getText(LC.HM_ID, loginDTO);
		Display[1][4] = LM.getText(LC.HM_ISSUE_RAISER, loginDTO);
		Display[2][4] = LM.getText(LC.SOLVED_TICKET_REPORT_SELECT_DESCRIPTION, loginDTO);
		Display[3][4] = LM.getText(LC.HM_TYPE, loginDTO);
		Display[4][4] = LM.getText(LC.HM_WING, loginDTO);
		Display[5][4] = language.equalsIgnoreCase("english")?"Priority":"গুরুত্ব";

		Display[6][4] = language.equalsIgnoreCase("english")?"Assigned Engineer":"দায়িত্বপ্রাপ্ত ইঞ্জিনিয়ার";


		
		String reportName = LM.getText(LC.PENDING_TICKET_DETAILS_REPORT_OTHER_PENDING_TICKET_DETAILS_REPORT, loginDTO);
		
		reportRequestHandler = new ReportRequestHandler(null,
				Criteria, Display, GroupBy, OrderBY, sql,
				reportService);

		
		reportRequestHandler.handleReportGet(request, response, userDTO, "pending_ticket_details_report",
				MenuConstants.PENDING_TICKET_DETAILS_REPORT_DETAILS, language, reportName, "pending_ticket_details_report");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		doGet(request, response);
	}
}
