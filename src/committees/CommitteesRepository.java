package committees;

import common.NameRepository;


public class CommitteesRepository extends NameRepository {

    private CommitteesRepository(){
        super(CommitteesDAO.getInstance(), CommitteesRepository.class);
    }

    private static class CommitteesRepositoryLoader{
        static CommitteesRepository INSTANCE = new CommitteesRepository();
    }

    public synchronized static CommitteesRepository getInstance(){
        return CommitteesRepository.CommitteesRepositoryLoader.INSTANCE;
    }
}