package committees;

import common.ApiResponse;
import common.BaseServlet;
import common.CommonDAOService;
import common.NameDTO;
import common.NameDao;
import common.NameInterface;
import common.NameRepository;
import holidays.HolidaysDTO;
import language.LC;
import language.LM;
import nurse_action.Nurse_actionDAO;
import pb.Utils;
import permission.MenuConstants;
import sessionmanager.SessionConstants;
import support_ticket.Support_ticketDTO;
import user.UserDTO;
import util.CommonDTO;
import util.CommonLoginData;
import util.CommonRequestHandler;
import util.HttpRequestUtils;
import util.RecordNavigator;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;

@WebServlet("/CommitteesServlet")
@MultipartConfig
public class CommitteesServlet extends BaseServlet implements NameInterface {
	private final CommitteesDAO committeesDAO = CommitteesDAO.getInstance();

	public String commonPartOfDispatchURL(){
		return  "common/name";
	}

	@Override
	public String getTableName() {
		return committeesDAO.getTableName();
	}

	@Override
	public String getServletName() {
		return "CommitteesServlet";
	}

	@Override
	public NameDao getCommonDAOService() {
		return committeesDAO;
	}

	@Override
	public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
		return addT(request,addFlag,userDTO,committeesDAO);
	}

	@Override
	public int[] getAddPageMenuConstants() {
		return new int[] {MenuConstants.COMMITTEES_ADD};
	}

	@Override
	public int[] getEditPageMenuConstants() {
		return new int[] {MenuConstants.COMMITTEES_UPDATE};
	}

	@Override
	public int[] getSearchMenuConstants() {
		return new int[] {MenuConstants.COMMITTEES_SEARCH};
	}

	@Override
	public Class<? extends HttpServlet> getClazz() {
		return CommitteesServlet.class;
	}

	@Override
	public int getSearchTitleValue() {
	    return LC.COMMITTEES_SEARCH_COMMITTEES_SEARCH_FORMNAME;
	}

	@Override
	public String get_p_navigatorName() {
		return SessionConstants.NAV_COMMITTEES;
	}

	@Override
	public String get_p_dtoCollectionName() {
		return SessionConstants.VIEW_COMMITTEES;
	}

	@Override
	public NameRepository getNameRepository() {
		return CommitteesRepository.getInstance();
	}

	@Override
	public int getAddTitleValue() {
		return LC.COMMITTEES_ADD_COMMITTEES_ADD_FORMNAME;
	}

	@Override
	public int getEditTitleValue() {
		return LC.COMMITTEES_EDIT_COMMITTEES_EDIT_FORMNAME;
	}

	@Override
	public void init(HttpServletRequest request) throws ServletException {
		setCommonAttributes(request,getServletName(),getCommonDAOService());
	}
	
	private String getFileName(final Part part) {
        final String partHeader = part.getHeader("content-disposition");
        System.out.println("Part Header = {0}" + partHeader);
        for (String content : part.getHeader("content-disposition").split(";")) {
            if (content.trim().startsWith("filename")) {
                return content.substring(
                        content.indexOf('=') + 1).trim().replace("\"", "");
            }
        }
        return null;
    }
	

	private void uploadXl(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO) throws IOException {
        System.out.println("%%%% ajax upload called");
        long currentTime = System.currentTimeMillis();
        try {
        	Part filePart = request.getPart("committeeXl");
        	String fileName = getFileName(filePart);
        	logger.debug("File name = " + fileName);
        	if(!fileName.endsWith(".xlsx"))
        	{
        		logger.debug("Invalid file");
        		response.getWriter().write("Error: Wrong File Type");
        		return;
        	}

            InputStream filecontent = filePart.getInputStream();                   
            XSSFWorkbook workbook = new XSSFWorkbook(filecontent);
            XSSFSheet sheet = workbook.getSheetAt(0);
            Iterator<Row> rowIt = sheet.iterator();

            while (rowIt.hasNext()) 
            {
                Row row = rowIt.next();
                Iterator<Cell> cellIterator = row.cellIterator();
                NameDTO nameDTO = new NameDTO();
                logger.debug("nameDTO newed");
                int j = 0;
                try 
                {
                    while (cellIterator.hasNext()) 
                    {
                        Cell cell = cellIterator.next();
                        if(j == 0)
                        {
                        	nameDTO.nameBn = cell.toString();
                        }
                        else  if(j == 1)
                        {
                        	nameDTO.nameEn = cell.toString();
	                    	nameDTO.insertedBy = userDTO.employee_record_id;
	                        nameDTO.insertionDate = currentTime;
	                        nameDTO.modifiedBy = userDTO.employee_record_id;
	                        nameDTO.lastModificationTime = currentTime;
	                        committeesDAO.add(nameDTO);
	                        logger.debug("nameDTO added");
	                        break;
                        }
                        j++;
                    }
                }
                catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }               
            }
            workbook.close();
            filecontent.close();
            response.getWriter().write("Success: Please referesh page");
        }
        catch (ServletException e) 
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }
	
	@Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		CommonLoginData commonLoginData = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get();
        init(request);
        String actionType = request.getParameter("actionType");
		if ("uploadXl".equals(actionType) &&  Utils.checkPermission(commonLoginData.userDTO, getAddPageMenuConstants()) && getAddPermission(request)) 
		{
			try {
				uploadXl(request, response, commonLoginData.userDTO);				
			} catch (Exception ex) {
				logger.error(ex);
				ApiResponse.sendErrorResponse(response, ex.getMessage());
			}
			return;
		}		
		else
		{
			super.doPost(request,response);
		}
		
	}
	
	@Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		CommonLoginData commonLoginData = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get();
        init(request);
        String actionType = request.getParameter("actionType");
		if ("xl".equals(actionType) &&  Utils.checkPermission(commonLoginData.userDTO, getAddPageMenuConstants()) && getAddPermission(request)) 
		{
			getXl(request, response, commonLoginData.userDTO);
		}		
		else
		{
			super.doGet(request,response);
		}
		
	}
	
	 public void getXl(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO) 
	 {
		Map<String, String> params = HttpRequestUtils.buildRequestParams(request);
		if (request.getAttribute(CommonDAOService.CRITERIA_MAP_KEY) != null) {
		    Map<String, String> extraCriteriaMap = (Map<String, String>) request.getAttribute(CommonDAOService.CRITERIA_MAP_KEY);
		    params.putAll(extraCriteriaMap);
		}
		RecordNavigator recordNavigator = getCommonDAOService().getRecordNavigatorLimitless(params, getTableJoinClause(request), getWhereClause(request), getSortClause(request));
		List<NameDTO> nameDTOs = (List<NameDTO>)recordNavigator.list;
		String Language = LM.getLanguage(userDTO);
		
		getXl(nameDTOs, response, Language);	
	 }
	 
	 public void getXl(List <NameDTO> nameDTOs, HttpServletResponse response, String Language)
	 {
		XSSFWorkbook wb = new XSSFWorkbook();
		boolean isLangEng = Language.equalsIgnoreCase("english");
		
		List<String> headers = new ArrayList<String>();
		if(isLangEng)
		{
			headers.add("Enlish Name");
		    headers.add("Bangla Name");
		    
		}
		else
		{
			headers.add("ইংরেজি নাম");
		    headers.add("বাংলা নাম");
		}
		
		CommonRequestHandler commonRequestHandler = new CommonRequestHandler();
		Sheet sheet = null;

        String tabName = "Committees";
        
        sheet = wb.createSheet(tabName);
		Row headerRow = sheet.createRow(0);
		try {
			int cellIndex = 0;
			for(String header: headers)
			{
				sheet.setColumnWidth(cellIndex, 25 * 256);
				Cell cell = headerRow.createCell(cellIndex);
				cell.setCellValue(header);
				cell.setCellStyle(commonRequestHandler.getHeaderStyle(wb));
				
				cellIndex++;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		int i = 1;
		CellStyle wrapStyle = wb.createCellStyle();
		wrapStyle.setWrapText(true);
		wrapStyle.setVerticalAlignment(VerticalAlignment.TOP);
		for(NameDTO nameDTO: nameDTOs)
		{
			Row row = sheet.createRow(i);
			
			int cellIndex = 0;
			
			Cell cell = row.createCell(cellIndex);
			cell.setCellStyle(wrapStyle);
			cell.setCellValue(nameDTO.nameEn);
			cellIndex ++;
			
			cell = row.createCell(cellIndex);
			cell.setCellStyle(wrapStyle);
			cell.setCellValue(nameDTO.nameBn);
			cellIndex ++;
			
			i ++;
		}
		
		
		try {
        	String fileName =  "Committees.xlsx";
            response.setContentType("application/vnd.ms-excel");
            response.setHeader("Content-Disposition", "attachment; filename=" + fileName);
            System.out.println("Writing xl");
			wb.write(response.getOutputStream());
			wb.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} // Write workbook to
	 }
	 
	 
}

