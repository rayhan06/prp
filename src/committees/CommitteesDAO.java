package committees;

import common.NameDao;

public class CommitteesDAO extends NameDao {

    private CommitteesDAO() {
        super("committees");
    }

    private static class LazyLoader{
        static final CommitteesDAO INSTANCE = new CommitteesDAO();
    }

    public static CommitteesDAO getInstance(){
        return CommitteesDAO.LazyLoader.INSTANCE;
    }
}