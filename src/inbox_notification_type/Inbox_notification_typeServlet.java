//package inbox_notification_type;
//
//import java.io.IOException;
//import java.io.*;
//
//
//import javax.servlet.RequestDispatcher;
//import javax.servlet.ServletException;
//import javax.servlet.annotation.MultipartConfig;
//import javax.servlet.annotation.WebServlet;
//
//
//import org.apache.log4j.Logger;
//
//import login.LoginDTO;
//import permission.MenuConstants;
//import role.PermissionRepository;
//
//
//import sessionmanager.SessionConstants;
//
//import user.UserDTO;
//import user.UserRepository;
//
//import util.RecordNavigationManager3;
//
//import java.util.*;
//import javax.servlet.http.*;
//import java.util.UUID;
//
//import inbox_notification_type.Constants;
//import approval_module_map.*;
//
//
//import com.google.gson.Gson;
//
//import pb.*;
//import pbReport.*;
//import org.jsoup.Jsoup;
//import org.jsoup.safety.Whitelist;
//
///**
// * Servlet implementation class Inbox_notification_typeServlet
// */
//@WebServlet("/Inbox_notification_typeServlet")
//@MultipartConfig
//public class Inbox_notification_typeServlet extends HttpServlet 
//{
//	private static final long serialVersionUID = 1L;
//    public static Logger logger = Logger.getLogger(Inbox_notification_typeServlet.class);
//	Approval_module_mapDAO approval_module_mapDAO = new Approval_module_mapDAO();
//    Approval_module_mapDTO approval_module_mapDTO;
//    String tableName = "inbox_notification_type";
//    String tempTableName = "inbox_notification_type_temp";
//	Inbox_notification_typeDAO inbox_notification_typeDAO;
//    private Gson gson = new Gson();
//    
//    /**
//     * @see HttpServlet#HttpServlet()
//     */
//    public Inbox_notification_typeServlet() 
//	{
//        super();
//    	try
//    	{
//			approval_module_mapDTO = approval_module_mapDAO.getApproval_module_mapDTOByTableName("inbox_notification_type");
//			inbox_notification_typeDAO = new Inbox_notification_typeDAO(tableName, tempTableName, approval_module_mapDTO);
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//    }   
//
//	/**
//	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
//	 */
//	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
//	{
//		System.out.println("In doget request = " + request);
//		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
//		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
//		boolean isPermanentTable = true;
//		if(request.getParameter("isPermanentTable") != null)
//		{
//			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
//		}
//		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
//		try
//		{
//			String actionType = request.getParameter("actionType");
//			if(actionType.equals("getAddPage"))
//			{
//				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.INBOX_NOTIFICATION_TYPE_ADD))
//				{
//					getAddPage(request, response);
//				}
//				else
//				{
//					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
//				}
//			}
//			else if(actionType.equals("getEditPage"))
//			{
//				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.INBOX_NOTIFICATION_TYPE_UPDATE))
//				{
//					if(isPermanentTable)
//					{
//						getInbox_notification_type(request, response, tableName);
//					}
//					else
//					{
//						getInbox_notification_type(request, response, tempTableName);
//					}
//				}
//				else
//				{
//					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
//				}						
//			}
//			else if(actionType.equals("getURL"))
//			{
//				String URL = request.getParameter("URL");
//				System.out.println("URL = " + URL);
//				response.sendRedirect(URL);			
//			}
//			else if(actionType.equals("search"))
//			{
//				System.out.println("search requested");
//				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.INBOX_NOTIFICATION_TYPE_SEARCH))
//				{
//					
//					if(isPermanentTable)
//					{
//						searchInbox_notification_type(request, response, tableName, isPermanentTable);
//					}
//					else
//					{
//						searchInbox_notification_type(request, response, tempTableName, isPermanentTable);
//					}
//				}			
//			}
//			else if(actionType.equals("getApprovalPage"))
//			{
//				System.out.println("getApprovalPage requested");
//				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.INBOX_NOTIFICATION_TYPE_SEARCH))
//				{
//					searchInbox_notification_type(request, response, tempTableName, false);
//				}
//				else
//				{
//					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
//				}				
//			}
//			else
//			{
//				request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
//			}
//		}
//		catch(Exception ex)
//		{
//			ex.printStackTrace();
//			logger.debug(ex);
//		}
//	}
//
//	private void getAddPage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
//	{
//		request.setAttribute("ID", -1L);
//		RequestDispatcher requestDispatcher = request.getRequestDispatcher("inbox_notification_type/inbox_notification_typeEdit.jsp");
//		requestDispatcher.forward(request, response);
//	}
//	private String getFileName(final Part part) 
//	{
//	    final String partHeader = part.getHeader("content-disposition");
//	    System.out.println("Part Header = {0}" +  partHeader);
//	    for (String content : part.getHeader("content-disposition").split(";")) {
//	        if (content.trim().startsWith("filename")) {
//	            return content.substring(
//	                    content.indexOf('=') + 1).trim().replace("\"", "");
//	        }
//	    }
//	    return null;
//	}
//
//	
//	
//
//	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
//	{
//		// TODO Auto-generated method stub
//		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
//		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
//		System.out.println("doPost");
//		boolean isPermanentTable = true;
//		if(request.getParameter("isPermanentTable") != null)
//		{
//			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
//		}
//		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
//		
//		try
//		{
//			String actionType = request.getParameter("actionType");
//			System.out.println("actionType = " + actionType);
//			if(actionType.equals("add"))
//			{
//				
//				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.INBOX_NOTIFICATION_TYPE_ADD))
//				{
//					System.out.println("going to  addInbox_notification_type ");
//					addInbox_notification_type(request, response, true, userDTO, tableName, true);
//				}
//				else
//				{
//					System.out.println("Not going to  addInbox_notification_type ");
//					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
//				}
//				
//			}
//			if(actionType.equals("approve"))
//			{
//				
//				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.INBOX_NOTIFICATION_TYPE_ADD))
//				{					
//					approveInbox_notification_type(request, response, true, userDTO);
//				}
//				else
//				{
//					System.out.println("Not going to  addInbox_notification_type ");
//					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
//				}
//				
//			}
//			if(actionType.equals("getDTO"))
//			{
//				
//				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.INBOX_NOTIFICATION_TYPE_ADD))
//				{
//					if(isPermanentTable)
//					{
//						getDTO(request, response, tableName);
//					}
//					else
//					{
//						getDTO(request, response, tempTableName);
//					}
//				}
//				else
//				{
//					System.out.println("Not going to  addInbox_notification_type ");
//					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
//				}
//				
//			}
//			else if(actionType.equals("edit"))
//			{
//				
//				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.INBOX_NOTIFICATION_TYPE_UPDATE))
//				{
//					if(isPermanentTable)
//					{
//						addInbox_notification_type(request, response, false, userDTO, tableName, isPermanentTable);
//					}
//					else
//					{
//						addInbox_notification_type(request, response, false, userDTO, tempTableName, isPermanentTable);
//					}
//				}
//				else
//				{
//					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
//				}
//			}
//			else if(actionType.equals("delete"))
//			{								
//				deleteInbox_notification_type(request, response, userDTO, isPermanentTable);				
//			}	
//			else if(actionType.equals("search"))
//			{
//				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.INBOX_NOTIFICATION_TYPE_SEARCH))
//				{
//					searchInbox_notification_type(request, response, tableName, true);
//				}
//				else
//				{
//					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
//				}
//			}
//			else if(actionType.equals("getGeo"))
//			{
//				System.out.println("going to geoloc ");
//				request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
//			}
//			
//		}
//		catch(Exception ex)
//		{
//			ex.printStackTrace();
//			logger.debug(ex);
//		}
//	}
//	
//	private void getDTO(HttpServletRequest request, HttpServletResponse response, String tableName) 
//	{
//		try 
//		{
//			System.out.println("In getDTO");
//			Inbox_notification_typeDTO inbox_notification_typeDTO = (Inbox_notification_typeDTO)inbox_notification_typeDAO.getDTOByID(Long.parseLong(request.getParameter("ID")), tableName);
//			PrintWriter out = response.getWriter();
//			response.setContentType("application/json");
//			response.setCharacterEncoding("UTF-8");
//			
//			String encoded = this.gson.toJson(inbox_notification_typeDTO);
//			System.out.println("json encoded data = " + encoded);
//			out.print(encoded);
//			out.flush();
//		}
//		catch (NumberFormatException e) 
//		{
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		
//	}
//	private void approveInbox_notification_type(HttpServletRequest request, HttpServletResponse response, boolean b, UserDTO userDTO) 
//	{
//		try
//		{
//			long id = Long.parseLong(request.getParameter("idToApprove"));
//			Inbox_notification_typeDTO inbox_notification_typeDTO = (Inbox_notification_typeDTO)inbox_notification_typeDAO.getDTOByID(id, tempTableName);
//			inbox_notification_typeDAO.manageWriteOperations(inbox_notification_typeDTO, SessionConstants.APPROVE, id, userDTO);
//		}
//		catch (Exception e) 
//		{
//			e.printStackTrace();
//		}
//		
//	}
//	private void addInbox_notification_type(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, String tableName, boolean isPermanentTable) throws IOException 
//	{
//		// TODO Auto-generated method stub
//		try 
//		{
//			request.setAttribute("failureMessage", "");
//			System.out.println("%%%% addInbox_notification_type");
//			String path = getServletContext().getRealPath("/img2/");
//			Inbox_notification_typeDTO inbox_notification_typeDTO;
//			String FileNamePrefix;
//			if(addFlag == true)
//			{
//				inbox_notification_typeDTO = new Inbox_notification_typeDTO();
//				FileNamePrefix = UUID.randomUUID().toString().substring(0, 10);
//			}
//			else
//			{
//				inbox_notification_typeDTO = (Inbox_notification_typeDTO)inbox_notification_typeDAO.getDTOByID(Long.parseLong(request.getParameter("identity")), tableName);
//				FileNamePrefix = request.getParameter("identity");
//			}
//			
//			String Value = "";
//			Value = request.getParameter("descriptionLanguageId");
//			if(Value != null)
//			{
//				Value = Jsoup.clean(Value,Whitelist.simpleText());
//			}
//			System.out.println("descriptionLanguageId = " + Value);
//			if(Value != null && !Value.equalsIgnoreCase(""))
//			{
//				inbox_notification_typeDTO.descriptionLanguageId = Long.parseLong(Value);
//			}
//			else
//			{
//				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//			}
//			
//			System.out.println("Done adding  addInbox_notification_type dto = " + inbox_notification_typeDTO);
//			
//			if(addFlag == true)
//			{
//				inbox_notification_typeDAO.manageWriteOperations(inbox_notification_typeDTO, SessionConstants.INSERT, -1, userDTO);
//			}
//			else
//			{
//				if(isPermanentTable)
//				{
//					inbox_notification_typeDAO.manageWriteOperations(inbox_notification_typeDTO, SessionConstants.UPDATE, -1, userDTO);
//				}
//				else
//				{
//					inbox_notification_typeDAO.manageWriteOperations(inbox_notification_typeDTO, SessionConstants.VALIDATE, -1, userDTO);
//				}
//				
//			}
//			
//			
//			
//			
//			
//			
//			
//			
//			String inPlaceSubmit = (String)request.getParameter("inplacesubmit");
//			
//			if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
//			{
//				getInbox_notification_type(request, response, tableName);
//			}
//			else
//			{
//				response.sendRedirect("Inbox_notification_typeServlet?actionType=search");
//			}
//					
//		}
//		catch (Exception e) 
//		{
//			e.printStackTrace();
//		}
//	}
//	
//	
//
//
//
//	
//	
//	
//
//	private void deleteInbox_notification_type(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO, boolean deleteOrReject) throws IOException 
//	{				
//		try 
//		{
//			String[] IDsToDelete = request.getParameterValues("ID");
//			for(int i = 0; i < IDsToDelete.length; i ++)
//			{
//				long id = Long.parseLong(IDsToDelete[i]);
//				System.out.println("------ DELETING " + IDsToDelete[i]);
//				
//				if(deleteOrReject)
//				{
//					Inbox_notification_typeDTO inbox_notification_typeDTO = (Inbox_notification_typeDTO)inbox_notification_typeDAO.getDTOByID(id);
//					inbox_notification_typeDAO.manageWriteOperations(inbox_notification_typeDTO, SessionConstants.DELETE, id, userDTO);
//					response.sendRedirect("Inbox_notification_typeServlet?actionType=search");
//				}
//				else
//				{
//					Inbox_notification_typeDTO inbox_notification_typeDTO = (Inbox_notification_typeDTO)inbox_notification_typeDAO.getDTOByID(id, tempTableName);
//					inbox_notification_typeDAO.manageWriteOperations(inbox_notification_typeDTO, SessionConstants.REJECT, id, userDTO);
//					response.sendRedirect("Inbox_notification_typeServlet?actionType=getApprovalPage");
//				}
//			}			
//		}
//		catch (Exception ex) 
//		{
//			ex.printStackTrace();
//		}
//		
//	}
//
//	private void getInbox_notification_type(HttpServletRequest request, HttpServletResponse response, String tableName) throws ServletException, IOException
//	{
//		System.out.println("in getInbox_notification_type");
//		Inbox_notification_typeDTO inbox_notification_typeDTO = null;
//		try 
//		{
//			inbox_notification_typeDTO = (Inbox_notification_typeDTO)inbox_notification_typeDAO.getDTOByID(Long.parseLong(request.getParameter("ID")), tableName);
//			request.setAttribute("ID", inbox_notification_typeDTO.iD);
//			request.setAttribute("inbox_notification_typeDTO",inbox_notification_typeDTO);
//			request.setAttribute("inbox_notification_typeDAO",inbox_notification_typeDAO);
//			
//			String URL= "";
//			
//			String inPlaceEdit = (String)request.getParameter("inplaceedit");
//			String inPlaceSubmit = (String)request.getParameter("inplacesubmit");
//			String getBodyOnly = (String)request.getParameter("getBodyOnly");
//			
//			if(inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase(""))
//			{
//				URL = "inbox_notification_type/inbox_notification_typeInPlaceEdit.jsp";	
//				request.setAttribute("inplaceedit","");				
//			}
//			else if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
//			{
//				URL = "inbox_notification_type/inbox_notification_typeSearchRow.jsp";
//				request.setAttribute("inplacesubmit","");					
//			}
//			else
//			{
//				if(getBodyOnly  != null && !getBodyOnly.equalsIgnoreCase(""))
//				{
//					URL = "inbox_notification_type/inbox_notification_typeEditBody.jsp?actionType=edit";
//				}
//				else
//				{
//					URL = "inbox_notification_type/inbox_notification_typeEdit.jsp?actionType=edit";
//				}				
//			}
//			
//			RequestDispatcher rd = request.getRequestDispatcher(URL);
//			rd.forward(request, response);
//		}
//		catch (NumberFormatException e) 
//		{
//			e.printStackTrace();
//		}
//		catch (Exception e) 
//		{
//			e.printStackTrace();
//		}
//	}
//	
//	private void searchInbox_notification_type(HttpServletRequest request, HttpServletResponse response, String tableName, boolean isPermanent) throws ServletException, IOException
//	{
//		System.out.println("in  searchInbox_notification_type 1");
//		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
//		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
//		String ajax = (String)request.getParameter("ajax");
//		boolean hasAjax = false;
//		if(ajax != null && !ajax.equalsIgnoreCase(""))
//		{
//			hasAjax = true;
//		}
//		System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);
//		
//        RecordNavigationManager3 rnManager = new RecordNavigationManager3(
//			SessionConstants.NAV_INBOX_NOTIFICATION_TYPE,
//			request,
//			inbox_notification_typeDAO,
//			SessionConstants.VIEW_INBOX_NOTIFICATION_TYPE,
//			SessionConstants.SEARCH_INBOX_NOTIFICATION_TYPE,
//			tableName,
//			isPermanent,
//			userDTO.approvalPathID);
//        try
//        {
//			System.out.println("trying to dojob");
//            rnManager.doJob(loginDTO);
//        }
//        catch(Exception e)
//        {
//			System.out.println("failed to dojob" + e);
//        }
//
//		request.setAttribute("inbox_notification_typeDAO",inbox_notification_typeDAO);
//        RequestDispatcher rd;
//        if(hasAjax == false)
//        {
//        	System.out.println("Going to inbox_notification_type/inbox_notification_typeSearch.jsp");
//        	rd = request.getRequestDispatcher("inbox_notification_type/inbox_notification_typeSearch.jsp");
//        }
//        else
//        {
//        	System.out.println("Going to inbox_notification_type/inbox_notification_typeSearchForm.jsp");
//        	rd = request.getRequestDispatcher("inbox_notification_type/inbox_notification_typeSearchForm.jsp");
//        }
//		rd.forward(request, response);
//	}
//	
//}
//
