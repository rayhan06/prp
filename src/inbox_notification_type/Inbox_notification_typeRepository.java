package inbox_notification_type;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class Inbox_notification_typeRepository implements Repository {
	Inbox_notification_typeDAO inbox_notification_typeDAO = null;
	
	public void setDAO(Inbox_notification_typeDAO inbox_notification_typeDAO)
	{
		this.inbox_notification_typeDAO = inbox_notification_typeDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Inbox_notification_typeRepository.class);
	Map<Long, Inbox_notification_typeDTO>mapOfInbox_notification_typeDTOToiD;
	Map<Long, Set<Inbox_notification_typeDTO> >mapOfInbox_notification_typeDTOTodescriptionLanguageId;
	Map<Long, Set<Inbox_notification_typeDTO> >mapOfInbox_notification_typeDTOTolastModificationTime;


	static Inbox_notification_typeRepository instance = null;  
	private Inbox_notification_typeRepository(){
		mapOfInbox_notification_typeDTOToiD = new ConcurrentHashMap<>();
		mapOfInbox_notification_typeDTOTodescriptionLanguageId = new ConcurrentHashMap<>();
		mapOfInbox_notification_typeDTOTolastModificationTime = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Inbox_notification_typeRepository getInstance(){
		if (instance == null){
			instance = new Inbox_notification_typeRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(inbox_notification_typeDAO == null)
		{
			return;
		}
		try {
			List<Inbox_notification_typeDTO> inbox_notification_typeDTOs = inbox_notification_typeDAO.getAllInbox_notification_type(reloadAll);
			for(Inbox_notification_typeDTO inbox_notification_typeDTO : inbox_notification_typeDTOs) {
				Inbox_notification_typeDTO oldInbox_notification_typeDTO = getInbox_notification_typeDTOByID(inbox_notification_typeDTO.iD);
				if( oldInbox_notification_typeDTO != null ) {
					mapOfInbox_notification_typeDTOToiD.remove(oldInbox_notification_typeDTO.iD);
				
					if(mapOfInbox_notification_typeDTOTodescriptionLanguageId.containsKey(oldInbox_notification_typeDTO.descriptionLanguageId)) {
						mapOfInbox_notification_typeDTOTodescriptionLanguageId.get(oldInbox_notification_typeDTO.descriptionLanguageId).remove(oldInbox_notification_typeDTO);
					}
					if(mapOfInbox_notification_typeDTOTodescriptionLanguageId.get(oldInbox_notification_typeDTO.descriptionLanguageId).isEmpty()) {
						mapOfInbox_notification_typeDTOTodescriptionLanguageId.remove(oldInbox_notification_typeDTO.descriptionLanguageId);
					}
					
					if(mapOfInbox_notification_typeDTOTolastModificationTime.containsKey(oldInbox_notification_typeDTO.lastModificationTime)) {
						mapOfInbox_notification_typeDTOTolastModificationTime.get(oldInbox_notification_typeDTO.lastModificationTime).remove(oldInbox_notification_typeDTO);
					}
					if(mapOfInbox_notification_typeDTOTolastModificationTime.get(oldInbox_notification_typeDTO.lastModificationTime).isEmpty()) {
						mapOfInbox_notification_typeDTOTolastModificationTime.remove(oldInbox_notification_typeDTO.lastModificationTime);
					}
					
					
				}
				if(inbox_notification_typeDTO.isDeleted == 0) 
				{
					
					mapOfInbox_notification_typeDTOToiD.put(inbox_notification_typeDTO.iD, inbox_notification_typeDTO);
				
					if( ! mapOfInbox_notification_typeDTOTodescriptionLanguageId.containsKey(inbox_notification_typeDTO.descriptionLanguageId)) {
						mapOfInbox_notification_typeDTOTodescriptionLanguageId.put(inbox_notification_typeDTO.descriptionLanguageId, new HashSet<>());
					}
					mapOfInbox_notification_typeDTOTodescriptionLanguageId.get(inbox_notification_typeDTO.descriptionLanguageId).add(inbox_notification_typeDTO);
					
					if( ! mapOfInbox_notification_typeDTOTolastModificationTime.containsKey(inbox_notification_typeDTO.lastModificationTime)) {
						mapOfInbox_notification_typeDTOTolastModificationTime.put(inbox_notification_typeDTO.lastModificationTime, new HashSet<>());
					}
					mapOfInbox_notification_typeDTOTolastModificationTime.get(inbox_notification_typeDTO.lastModificationTime).add(inbox_notification_typeDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Inbox_notification_typeDTO> getInbox_notification_typeList() {
		List <Inbox_notification_typeDTO> inbox_notification_types = new ArrayList<Inbox_notification_typeDTO>(this.mapOfInbox_notification_typeDTOToiD.values());
		return inbox_notification_types;
	}
	
	
	public Inbox_notification_typeDTO getInbox_notification_typeDTOByID( long ID){
		return mapOfInbox_notification_typeDTOToiD.get(ID);
	}
	
	
	public List<Inbox_notification_typeDTO> getInbox_notification_typeDTOBydescription_language_id(long description_language_id) {
		return new ArrayList<>( mapOfInbox_notification_typeDTOTodescriptionLanguageId.getOrDefault(description_language_id,new HashSet<>()));
	}
	
	
	public List<Inbox_notification_typeDTO> getInbox_notification_typeDTOBylastModificationTime(long lastModificationTime) {
		return new ArrayList<>( mapOfInbox_notification_typeDTOTolastModificationTime.getOrDefault(lastModificationTime,new HashSet<>()));
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "inbox_notification_type";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


