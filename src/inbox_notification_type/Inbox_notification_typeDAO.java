package inbox_notification_type;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;


import org.apache.log4j.Logger;

import dbm.*;

import repository.RepositoryManager;

import util.*;
import pb.*;
import approval_module_map.*;

public class Inbox_notification_typeDAO  extends NavigationService3
{
	
	Logger logger = Logger.getLogger(getClass());

	
	public Inbox_notification_typeDAO(String tableName, String tempTableName, Approval_module_mapDTO approval_module_mapDTO)
	{
		super(tableName, tempTableName, approval_module_mapDTO);		
	}
	
	
	
	public long add(CommonDTO commonDTO, String tableName, TempTableDTO tempTableDTO) throws Exception
	{
		
		Inbox_notification_typeDTO inbox_notification_typeDTO = (Inbox_notification_typeDTO)commonDTO;
		
		Connection connection = null;
		PreparedStatement ps = null;

		long lastModificationTime = System.currentTimeMillis();	

		try{
			connection = DBMW.getInstance().getConnection();
			
			if(connection == null)
			{
				System.out.println("nullconn");
			}

			inbox_notification_typeDTO.iD = DBMW.getInstance().getNextSequenceId(tableName);

			String sql = "INSERT INTO " + tableName;
			
			sql += " (";
			sql += "ID";
			sql += ", ";
			sql += "description_language_id";
			sql += ", ";
			sql += "isDeleted";
			sql += ", ";
			sql += "lastModificationTime";
			if(tempTableDTO!=null)
			{
				sql += ", permanent_table_id, operation_type, approval_path_type, approval_order";
			}
			sql += ")";
			
			
            sql += " VALUES(";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			if(tempTableDTO!=null)
			{
				sql += ", " + tempTableDTO.permanent_table_id + ", " + tempTableDTO.operation_type + ", " + tempTableDTO.approval_path_type + ", " + tempTableDTO.approval_order;
			}
			sql += ")";
				
			printSql(sql);

			ps = connection.prepareStatement(sql);
			
			
			

			int index = 1;

			ps.setObject(index++,inbox_notification_typeDTO.iD);
			ps.setObject(index++,inbox_notification_typeDTO.descriptionLanguageId);
			ps.setObject(index++,inbox_notification_typeDTO.isDeleted);
			ps.setObject(index++, lastModificationTime);
			
			System.out.println(ps);
			ps.execute();
			
			
			recordUpdateTime(connection,  lastModificationTime, tableName);

		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if (ps != null) {
					ps.close();
				}
			} catch(Exception e){}
			try{
				if(connection != null){
					DBMW.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		return inbox_notification_typeDTO.iD;		
	}
		
	

	//need another getter for repository
	public CommonDTO getDTOByID (long ID, String tableName) throws Exception
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Inbox_notification_typeDTO inbox_notification_typeDTO = null;
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
			
            sql += " WHERE ID=" + ID;
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			if(rs.next()){
				inbox_notification_typeDTO = new Inbox_notification_typeDTO();

				inbox_notification_typeDTO.iD = rs.getLong("ID");
				inbox_notification_typeDTO.descriptionLanguageId = rs.getLong("description_language_id");
				inbox_notification_typeDTO.isDeleted = rs.getInt("isDeleted");
				inbox_notification_typeDTO.lastModificationTime = rs.getLong("lastModificationTime");

			}			
			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DBMR.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
		return inbox_notification_typeDTO;
	}
	
	public long update(CommonDTO commonDTO, String tableName) throws Exception
	{		
		Inbox_notification_typeDTO inbox_notification_typeDTO = (Inbox_notification_typeDTO)commonDTO;
		
		Connection connection = null;
		PreparedStatement ps = null;

		long lastModificationTime = System.currentTimeMillis();	
		try{
			connection = DBMW.getInstance().getConnection();

			String sql = "UPDATE " + tableName;
			
			sql += " SET ";
			sql += "description_language_id=?";
			sql += ", lastModificationTime = "	+ lastModificationTime + "";
            sql += " WHERE ID = " + inbox_notification_typeDTO.iD;
				
			printSql(sql);

			ps = connection.prepareStatement(sql);
			
			

			int index = 1;
			
			ps.setObject(index++,inbox_notification_typeDTO.descriptionLanguageId);
			System.out.println(ps);
			ps.executeUpdate();
			


			
			recordUpdateTime(connection, lastModificationTime, tableName);

						
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if (ps != null) {
					ps.close();
				}
			} catch(Exception e){}
			try{
				if(connection != null){
					DBMW.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		return inbox_notification_typeDTO.iD;
	}
	
	public List<Inbox_notification_typeDTO> getDTOs(Collection recordIDs)
	{
		return getDTOs(recordIDs, tableName);
	}
	
	public List<Inbox_notification_typeDTO> getDTOs(Collection recordIDs, String tableName){
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Inbox_notification_typeDTO inbox_notification_typeDTO = null;
		List<Inbox_notification_typeDTO> inbox_notification_typeDTOList = new ArrayList<>();
		if(recordIDs.isEmpty()){
			return inbox_notification_typeDTOList;
		}
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
            
            sql += " WHERE ID IN ( ";

			for(int i = 0;i<recordIDs.size();i++){
				if(i!=0){
					sql+=",";
				}
				sql+=((ArrayList)recordIDs).get(i);
			}
			sql+=")  order by lastModificationTime desc";
			
			printSql(sql);
			
			logger.debug("sql " + sql);
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				inbox_notification_typeDTO = new Inbox_notification_typeDTO();
				inbox_notification_typeDTO.iD = rs.getLong("ID");
				inbox_notification_typeDTO.descriptionLanguageId = rs.getLong("description_language_id");
				inbox_notification_typeDTO.isDeleted = rs.getInt("isDeleted");
				inbox_notification_typeDTO.lastModificationTime = rs.getLong("lastModificationTime");
				System.out.println("got this DTO: " + inbox_notification_typeDTO);
				
				inbox_notification_typeDTOList.add(inbox_notification_typeDTO);

			}			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DBMR.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
		return inbox_notification_typeDTOList;
	
	}
	
	

	
	
	
	//add repository
	public List<Inbox_notification_typeDTO> getAllInbox_notification_type (boolean isFirstReload)
    {
		List<Inbox_notification_typeDTO> inbox_notification_typeDTOList = new ArrayList<>();

		String sql = "SELECT * FROM inbox_notification_type";
		sql += " WHERE ";
	

		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by inbox_notification_type.lastModificationTime desc";
		printSql(sql);
		
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;

		
		try{
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();
			rs = stmt.executeQuery(sql);
			

			while(rs.next()){
				Inbox_notification_typeDTO inbox_notification_typeDTO = new Inbox_notification_typeDTO();
				inbox_notification_typeDTO.iD = rs.getLong("ID");
				inbox_notification_typeDTO.descriptionLanguageId = rs.getLong("description_language_id");
				inbox_notification_typeDTO.isDeleted = rs.getInt("isDeleted");
				inbox_notification_typeDTO.lastModificationTime = rs.getLong("lastModificationTime");
				
				inbox_notification_typeDTOList.add(inbox_notification_typeDTO);
			}			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			try{ 
				if (connection != null){ 
					DBMR.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}

		return inbox_notification_typeDTOList;
    }
	
	public List<Inbox_notification_typeDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset)
	{
		return getDTOs(p_searchCriteria, limit, offset, tableName, true, 0);
	}
	
	public List<Inbox_notification_typeDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, String tableName, boolean isPermanentTable, long userApprovalPathType)
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		List<Inbox_notification_typeDTO> inbox_notification_typeDTOList = new ArrayList<>();

		try{
			
			String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, tableName, isPermanentTable, userApprovalPathType);
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				Inbox_notification_typeDTO inbox_notification_typeDTO = new Inbox_notification_typeDTO();
				inbox_notification_typeDTO.iD = rs.getLong("ID");
				inbox_notification_typeDTO.descriptionLanguageId = rs.getLong("description_language_id");
				inbox_notification_typeDTO.isDeleted = rs.getInt("isDeleted");
				inbox_notification_typeDTO.lastModificationTime = rs.getLong("lastModificationTime");
				
				inbox_notification_typeDTOList.add(inbox_notification_typeDTO);
			}					
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DBMR.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
		return inbox_notification_typeDTOList;
	
	}

		
	
	public String getSqlWithSearchCriteria(Hashtable p_searchCriteria, int limit, int offset, int category, String tableName, boolean isPermanentTable, long userApprovalPathType)
    {
		Inbox_notification_typeMAPS maps = new Inbox_notification_typeMAPS(tableName);
		String joinSQL = "";
		return getSqlWithSearchCriteria(p_searchCriteria, limit, offset, category, tableName, isPermanentTable, userApprovalPathType, maps, joinSQL);
    }			
}
	