package inbox_notification_type;
import java.util.*; 
import util.*;


public class Inbox_notification_typeMAPS extends CommonMaps
{	
	public Inbox_notification_typeMAPS(String tableName)
	{
		
		java_allfield_type_map.put("description_language_id".toLowerCase(), "Long");

		java_anyfield_search_map.put(tableName + ".description_language_id".toLowerCase(), "Long");

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("descriptionLanguageId".toLowerCase(), "descriptionLanguageId".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("description_language_id".toLowerCase(), "descriptionLanguageId".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Description Language Id".toLowerCase(), "descriptionLanguageId".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}