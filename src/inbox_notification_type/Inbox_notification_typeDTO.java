package inbox_notification_type;
import java.util.*; 
import util.*; 


public class Inbox_notification_typeDTO extends CommonDTO
{

	public long descriptionLanguageId = 0;
	
	
    @Override
	public String toString() {
            return "$Inbox_notification_typeDTO[" +
            " iD = " + iD +
            " descriptionLanguageId = " + descriptionLanguageId +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}