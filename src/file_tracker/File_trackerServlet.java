package file_tracker;

import common.BaseServlet;
import files.FilesDAO;
import language.LC;
import language.LM;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import pb.ErrorMessage;
import permission.MenuConstants;
import sessionmanager.SessionConstants;
import user.UserDTO;
import util.CommonDTO;
import util.HttpRequestUtils;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * Servlet implementation class File_trackerServlet
 */
@WebServlet("/File_trackerServlet")
@MultipartConfig
public class File_trackerServlet extends BaseServlet {
    private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(File_trackerServlet.class);

    @Override
    public String getTableName() {
        return File_trackerDAO.getInstance().getTableName();
    }

    @Override
    public String getServletName() {
        return "File_trackerServlet";
    }

    @Override
    public File_trackerDAO getCommonDAOService() {
        return File_trackerDAO.getInstance();
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[]{MenuConstants.FILE_TRACKER_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[]{MenuConstants.FILE_TRACKER_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[]{MenuConstants.FILE_TRACKER_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return File_trackerServlet.class;
    }

    FilesDAO filesDAO = new FilesDAO();


    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {


        String language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
        boolean isLangEng = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
        File_trackerDTO file_trackerDTO;
        SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
        Date currentDate = new Date(System.currentTimeMillis());
        long currentTime = currentDate.getHours() * 3600 * 1000L + currentDate.getMinutes() * 60 * 1000L + currentDate.getSeconds() * 1000L;
        if (addFlag) {
            file_trackerDTO = new File_trackerDTO();
            file_trackerDTO.insertedBy = userDTO.employee_record_id;
            file_trackerDTO.insertionTime = new Date().getTime();
            file_trackerDTO.serial = File_trackerDAO.getInstance().getAllDTOs(true)
                    .stream()
                    .mapToLong(dto -> dto.serial)
                    .max()
                    .orElse(0)
                    + 1;
        } else {
            file_trackerDTO = File_trackerDAO.getInstance().getDTOFromID(Long.parseLong(request.getParameter("iD")));
        }
        file_trackerDTO.modifiedBy = userDTO.employee_record_id;
        file_trackerDTO.lastModificationTime = new Date().getTime();

        String Value;


        file_trackerDTO.fileNumber = (Jsoup.clean(request.getParameter("fileNumber"), Whitelist.simpleText()));
        if (file_trackerDTO.fileNumber == null || file_trackerDTO.fileNumber.length() == 0) {
            throw new Exception(isLangEng ? "File number not found" : "ফাইল নাম্বার পাওয়া যায় নি");
        }
        file_trackerDTO.description = (Jsoup.clean(request.getParameter("description"), Whitelist.simpleText()));
        if (file_trackerDTO.description == null || file_trackerDTO.description.length() == 0) {
            throw new Exception(isLangEng ? "File description not found" : "ফাইলের বর্নণা পাওয়া যায় নি");
        }
        String branchStr = Jsoup.clean(request.getParameter("branchId"), Whitelist.simpleText());

        if (branchStr == null || branchStr.length() == 0) {
            throw new Exception(isLangEng ? "Office branch not found" : "অফিস/দপ্তর পাওয়া যায় নি");
        }
        file_trackerDTO.branchId = Long.parseLong(branchStr);
        Value = Jsoup.clean(request.getParameter("receiveDate"), Whitelist.simpleText());

        if (Value != null && !Value.equalsIgnoreCase("")) {
            try {
                Date d = f.parse(Value);
                file_trackerDTO.receiveDate = d.getTime() + currentTime;
            } catch (Exception e) {
                file_trackerDTO.receiveDate = SessionConstants.MIN_DATE;
                e.printStackTrace();
                throw new Exception(LM.getText(LC.FILE_TRACKER_ADD_RECEIVEDATE, language) + " " + ErrorMessage.getInvalidMessage(language));
            }
        } else {
            file_trackerDTO.receiveDate = SessionConstants.MIN_DATE;
            throw new Exception(isLangEng ? "Receive date not found" : "প্রাপ্তির তারিখ পাওয়া যায় নি");
        }

        //  file_trackerDTO.receiveTime = Jsoup.clean(request.getParameter("receiveTime"), Whitelist.simpleText());

        Value = Jsoup.clean(request.getParameter("distributionDate"), Whitelist.simpleText());

        if (Value != null && !Value.equalsIgnoreCase("")) {
            try {
                Date d = f.parse(Value);
                file_trackerDTO.distributionDate = d.getTime() + currentTime;
            } catch (Exception e) {
                file_trackerDTO.distributionDate = SessionConstants.MIN_DATE;
                e.printStackTrace();
                throw new Exception(LM.getText(LC.FILE_TRACKER_ADD_DISTRIBUTIONDATE, language) + " " + ErrorMessage.getInvalidMessage(language));
            }
        } else {
            file_trackerDTO.distributionDate = SessionConstants.MIN_DATE;
        }


        //  file_trackerDTO.distributionTime = Jsoup.clean(request.getParameter("distributionTime"), Whitelist.simpleText());


        Value = Jsoup.clean(request.getParameter("receiverRecordsId"), Whitelist.simpleText());

        if (Value != null && !Value.equalsIgnoreCase("")) {
            file_trackerDTO.receiverRecordsId = Long.parseLong(Value);
        } else {
            System.out.println("FieldName has a null Value, not updating" + " = " + Value);
        }

        Value = Jsoup.clean(request.getParameter("fileDropzone"), Whitelist.simpleText());

        if (Value != null && !Value.equalsIgnoreCase("")) {

            System.out.println("fileDropzone = " + Value);

            file_trackerDTO.fileDropzone = Long.parseLong(Value);


            if (!addFlag) {
                String fileDropzoneFilesToDelete = request.getParameter("fileDropzoneFilesToDelete");
                String[] deleteArray = fileDropzoneFilesToDelete.split(",");
                for (int i = 0; i < deleteArray.length; i++) {
                    System.out.println("going to delete " + deleteArray[i]);
                    if (i > 0) {
                        filesDAO.delete(Long.parseLong(deleteArray[i]));
                    }
                }
            }
        } else {
            System.out.println("FieldName has a null Value, not updating" + " = " + Value);
        }

        System.out.println("Done adding  addFile_tracker dto = " + file_trackerDTO);

        if (addFlag) {
            File_trackerDAO.getInstance().add(file_trackerDTO);
        } else {
            File_trackerDAO.getInstance().update(file_trackerDTO);
        }


        return file_trackerDTO;

    }
}

