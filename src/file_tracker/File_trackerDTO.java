package file_tracker;

import sessionmanager.SessionConstants;
import util.CommonDTO;

import java.util.Date;


public class File_trackerDTO extends CommonDTO {

    public long serial = -1;
    public String fileNumber = "";
    public String description = "";
    public long branchId = -1;
    public long receiveDate = new Date().getTime();
    public String receiveTime = "";
    public long distributionDate = SessionConstants.MIN_DATE;
    public String distributionTime = "";
    public long receiverRecordsId = -1;
    public long fileDropzone = -1;
    public long insertedBy = -1;
    public long insertionTime = -1;
    public long modifiedBy = -1;


    @Override
    public String toString() {
        return "$File_trackerDTO[" +
                " iD = " + iD +
                " serial = " + serial +
                " fileNumber = " + fileNumber +
                " description = " + description +
                " branchId = " + branchId +
                " receiveDate = " + receiveDate +
                " receiveTime = " + receiveTime +
                " distributionDate = " + distributionDate +
                " distributionTime = " + distributionTime +
                " receiverRecordsId = " + receiverRecordsId +
                " fileDropzone = " + fileDropzone +
                " searchColumn = " + searchColumn +
                " insertedBy = " + insertedBy +
                " insertionTime = " + insertionTime +
                " isDeleted = " + isDeleted +
                " modifiedBy = " + modifiedBy +
                " lastModificationTime = " + lastModificationTime +
                "]";
    }

}