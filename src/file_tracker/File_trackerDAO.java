package file_tracker;

import common.CommonDAOService;
import office_units.Office_unitsRepository;
import org.apache.log4j.Logger;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class File_trackerDAO implements CommonDAOService<File_trackerDTO> {

    Logger logger = Logger.getLogger(getClass());

    private static String addQuery = "";
    private static String updateQuery = "";
    private String[] columnNames = null;

    private File_trackerDAO() {
        columnNames = new String[]
                {
                        "ID",
                        "serial",
                        "file_number",
                        "description",
                        "branch_id",
                        "receive_date",
                        "receive_time",
                        "distribution_date",
                        "distribution_time",
                        "receiver_records_id",
                        "file_dropzone",
                        "search_column",
                        "inserted_by",
                        "insertion_time",
                        "isDeleted",
                        "modified_by",
                        "lastModificationTime"
                };
        updateQuery = getUpdateQuery(columnNames);
        addQuery = getAddQuery(columnNames);
        searchMap.put("serial", " and (serial = ?)");
        searchMap.put("file_number", " and (file_number like ?)");
        searchMap.put("receive_date_start", " and (receive_date >= ?)");
        searchMap.put("receive_date_end", " and (receive_date <= ?)");
        searchMap.put("distribution_date_start", " and (distribution_date >= ?)");
        searchMap.put("distribution_date_end", " and (distribution_date <= ?)");
        searchMap.put("AnyField", " and (search_column like ?)");
    }

    private final Map<String, String> searchMap = new HashMap<>();

    private static class LazyLoader {
        static final File_trackerDAO INSTANCE = new File_trackerDAO();
    }

    public static File_trackerDAO getInstance() {
        return LazyLoader.INSTANCE;
    }

    public void setSearchColumn(File_trackerDTO file_trackerDTO) {
        file_trackerDTO.searchColumn = "";
        file_trackerDTO.searchColumn += file_trackerDTO.fileNumber + " ";
        file_trackerDTO.searchColumn += Office_unitsRepository.getInstance().geText("Bangla", file_trackerDTO.branchId) + " " + Office_unitsRepository.getInstance().geText("English", file_trackerDTO.branchId) + " ";
    }

    @Override
    public void set(PreparedStatement ps, File_trackerDTO file_trackerDTO, boolean isInsert) throws SQLException {
        int index = 0;
        long lastModificationTime = System.currentTimeMillis();
        setSearchColumn(file_trackerDTO);
        if (isInsert) {
            ps.setObject(++index, file_trackerDTO.iD);
        }
        ps.setObject(++index, file_trackerDTO.serial);
        ps.setObject(++index, file_trackerDTO.fileNumber);
        ps.setObject(++index, file_trackerDTO.description);
        ps.setObject(++index, file_trackerDTO.branchId);
        ps.setObject(++index, file_trackerDTO.receiveDate);
        ps.setObject(++index, file_trackerDTO.receiveTime);
        ps.setObject(++index, file_trackerDTO.distributionDate);
        ps.setObject(++index, file_trackerDTO.distributionTime);
        ps.setObject(++index, file_trackerDTO.receiverRecordsId);
        ps.setObject(++index, file_trackerDTO.fileDropzone);
        ps.setObject(++index, file_trackerDTO.searchColumn);
        ps.setObject(++index, file_trackerDTO.insertedBy);
        ps.setObject(++index, file_trackerDTO.insertionTime);
        if (isInsert) {
            ps.setObject(++index, file_trackerDTO.isDeleted);
        }
        ps.setObject(++index, file_trackerDTO.modifiedBy);
        ps.setObject(++index, lastModificationTime);
        if (!isInsert) {
            ps.setObject(++index, file_trackerDTO.iD);
        }
    }

    @Override
    public File_trackerDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            File_trackerDTO file_trackerDTO = new File_trackerDTO();
            int i = 0;
            file_trackerDTO.iD = rs.getLong(columnNames[i++]);
            file_trackerDTO.serial = rs.getLong(columnNames[i++]);
            file_trackerDTO.fileNumber = rs.getString(columnNames[i++]);
            file_trackerDTO.description = rs.getString(columnNames[i++]);
            file_trackerDTO.branchId = rs.getLong(columnNames[i++]);
            file_trackerDTO.receiveDate = rs.getLong(columnNames[i++]);
            file_trackerDTO.receiveTime = rs.getString(columnNames[i++]);
            file_trackerDTO.distributionDate = rs.getLong(columnNames[i++]);
            file_trackerDTO.distributionTime = rs.getString(columnNames[i++]);
            file_trackerDTO.receiverRecordsId = rs.getLong(columnNames[i++]);
            file_trackerDTO.fileDropzone = rs.getLong(columnNames[i++]);
            file_trackerDTO.searchColumn = rs.getString(columnNames[i++]);
            file_trackerDTO.insertedBy = rs.getLong(columnNames[i++]);
            file_trackerDTO.insertionTime = rs.getLong(columnNames[i++]);
            file_trackerDTO.isDeleted = rs.getInt(columnNames[i++]);
            file_trackerDTO.modifiedBy = rs.getLong(columnNames[i++]);
            file_trackerDTO.lastModificationTime = rs.getLong(columnNames[i++]);
            return file_trackerDTO;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    public File_trackerDTO getDTOByID(long id) {
        return getDTOFromID(id);
    }

    @Override
    public String getTableName() {
        return "file_tracker";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((File_trackerDTO) commonDTO, addQuery, true);
    }

    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((File_trackerDTO) commonDTO, updateQuery, false);
    }

    @Override
    public String getLastModifierUser() {
        return "modified_by";
    }

}
	