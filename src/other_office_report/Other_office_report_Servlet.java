package other_office_report;


import java.io.IOException;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import employee_info_report.Employee_info_report_Servlet;
import language.LC;
import language.LM;

import login.LoginDTO;
import permission.MenuConstants;

import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;

import util.ReportRequestHandler;
import pbReport.*;






@WebServlet("/Other_office_report_Servlet")
public class Other_office_report_Servlet  extends HttpServlet {
	/**
	 * 
	 */
	private static final Logger logger = Logger.getLogger(Other_office_report_Servlet.class);
	private static final long serialVersionUID = 1L;

	String[][] Criteria =
	{
		{"criteria","","current_office",">","","int","","","0","none", ""},		
		{"criteria","","isDeleted","=","AND","int","","","0","none", ""},		
		{"criteria","","name_eng","like","AND","String","","","%","nameEng", LC.EMPLOYEE_RECORDS_ADD_NAMEENG + ""},		
		{"criteria","","name_bng","like","AND","String","","","%","nameBng", LC.EMPLOYEE_RECORDS_ADD_NAMEBNG + ""},		
		{"criteria","","current_office","=","AND","int","","","any","currentOffice_4", LC.HM_OFFICE + ""},
		{"criteria","","other_office_department_type","=","AND","int","","","any","department", LC.HM_OFFICE + ""},
		{"criteria","","personal_mobile","=","AND","String","","","any","phoneNo", LC.HM_PHONE + ""},
		{"criteria","","nid","=","AND","String","","","any","nid", LC.HM_PHONE + ""},
		{"criteria","","employee_number","=","AND","String","","","any","username", LC.HM_PHONE + ""}
	};
	
	String[][] Display =
	{
		{"display","","employee_number","text",""},	
		{"display","","name_eng","text",""},		
		{"display","","name_bng","text",""},		
		{"display","","personal_mobile","text",""},	
		{"display","","current_office","other_office",""},	
		{"display","","other_office_department_type","type",""},	
		{"display","","current_designation","text",""},		
		{"display","","status","status",""},
		{"display","","photo","file",""},
		{"display","","nid","text",""}
	};
	
	String GroupBy = "";
	String OrderBY = "";
	
	ReportRequestHandler reportRequestHandler;
	
	public Other_office_report_Servlet(){

	}
	
	private final ReportService reportService = new ReportService();
	
	private String sql;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String language = "english";
		if(userDTO!= null && userDTO.languageID == SessionConstants.BANGLA)
		{
			language = "bangla";
		}
				
		String actionType = request.getParameter("actionType");

		System.out.println("In ssservlet doget, actiontype = " + actionType);
		
		sql = "employee_records";

		Display[0][4] = LM.getText(LC.HM_USER_NAME, loginDTO);
		Display[1][4] = LM.getText(LC.EMPLOYEE_RECORDS_ADD_NAMEENG, loginDTO);
		Display[2][4] = LM.getText(LC.EMPLOYEE_RECORDS_ADD_NAMEBNG, loginDTO);
		Display[3][4] = LM.getText(LC.HM_PHONE, loginDTO);
		Display[4][4] = LM.getText(LC.HM_OFFICE, loginDTO);
		Display[5][4] = LM.getText(LC.HM_DEPARTMENT, loginDTO);
		Display[6][4] = LM.getText(LC.HM_DESIGNATION, loginDTO);
		Display[7][4] = LM.getText(LC.HM_STATUS, loginDTO);
		Display[8][4] = LM.getText(LC.HM_PHOTO, loginDTO);
		Display[9][4] = language.equalsIgnoreCase("english")?"NID":"জাতীয় পরিচয়পত্র";

		logger.debug("showPic = " + request.getParameter("showPic"));
		if(request.getParameter("showPic") == null || request.getParameter("showPic") == "false")
		{
			Display[8][2] = " ";
			Display[8][3] = "none";
		}
		else
		{
			Display[8][2] = "photo";
			Display[8][3] = "file";
		}
		String reportName = LM.getText(LC.OTHER_OFFICE_REPORT_OTHER_OTHER_OFFICE_REPORT, loginDTO);
		
		reportRequestHandler = new ReportRequestHandler(null,
				Criteria, Display, GroupBy, OrderBY, sql,
				reportService);

		
		reportRequestHandler.handleReportGet(request, response, userDTO, "other_office_report",
				MenuConstants.OTHER_OFFICE_REPORT_DETAILS, language, reportName, "other_office_report");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		doGet(request, response);
	}
}
