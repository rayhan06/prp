package common_servlet;

import login.LoginDTO;
import org.apache.log4j.Logger;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import user.UserTypeDTO;
import user.UserTypeRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/CommonServlet")
public class CommonServlet extends HttpServlet {
    public static Logger logger = Logger.getLogger(CommonServlet.class);

    public CommonServlet() {
        super();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        System.out.println("In CommonServlet");
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        UserTypeDTO userTypeDTO = UserTypeRepository.getInstance().getUserTypeByUserTypeID(userDTO.userType);

        try {
            String actionType = request.getParameter("actionType");
            if (actionType.equals("getUniqueBBSCode")) {
                String table = request.getParameter("table");
                String column = request.getParameter("column");
                String data = request.getParameter("data");
                String parent = request.getParameter("parent");
                String parentId = request.getParameter("parent_id");
                String exceptId = request.getParameter("except_id");

                response.setContentType("application/json");

                PrintWriter out = response.getWriter();
                boolean r = new CommonService().hasTableColumnData(table, column, data, parent, parentId, exceptId);
                out.print(r);
                out.flush();
            } else if (actionType.equals("getGeo")) {
                String table = request.getParameter("table");
                String column = request.getParameter("column");
                String parent = request.getParameter("parent");
                String parentID = request.getParameter("parent_id");
                String language = request.getParameter("language");

                response.setContentType("application/json");

                PrintWriter out = response.getWriter();
                String r = new CommonService().getGeo(table, column, parent, parentID, language);
                out.print(r);
                out.flush();
            } else if (actionType.equals("hasMinistryBBSCode")) {
                String bbs = request.getParameter("bbs");
                String id = request.getParameter("id");

                response.setContentType("application/json");

                PrintWriter out = response.getWriter();
                boolean r = new CommonService().hasMinistryBBSCode(bbs, id);
                out.print(r);
                out.flush();
            } else if (actionType.equals("hasOfficeCode")) {
                String code = request.getParameter("code");
                String id = request.getParameter("id");

                response.setContentType("application/json");

                PrintWriter out = response.getWriter();
                boolean r = new CommonService().hasOfficeCode(code, id);
                out.print(r);
                out.flush();
            } else if (actionType.equals("hasBatchNo")) {
                String batch = request.getParameter("batch");
                String id = request.getParameter("id");

                response.setContentType("application/json");

                PrintWriter out = response.getWriter();
                boolean r = new CommonService().hasBatchNo(batch, id);
                out.print(r);
                out.flush();
            } else if (actionType.equals("hasUnitCode")) {
                String code = request.getParameter("code");
                String id = request.getParameter("id");

                response.setContentType("application/json");

                PrintWriter out = response.getWriter();
                boolean r = new CommonService().hasUnitCode(code, id);
                out.print(r);
                out.flush();
            } else if (actionType.equals("myPage")) {
            	response.sendRedirect(request.getContextPath() + "/Employee_recordsServlet?actionType=viewMultiForm&tab=1&ID=" + userDTO.employee_record_id);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.debug(ex);
        }
    }
}
