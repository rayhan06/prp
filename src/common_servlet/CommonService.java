package common_servlet;

import dbm.DBMR;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

public class CommonService {

    public CommonService() {
    }

    public boolean hasTableColumnData(String table, String column, String data, String parentColumn, String parentColumnId, String exceptId) {
        Connection connection = null;
        ResultSet rs = null;
        Statement stmt = null;
        try {

            String sql = "SELECT * " + " FROM " + table + " WHERE " + column + " = " + data + " AND " + parentColumn + " = " + parentColumnId;
            if (exceptId != null) {
                sql += " AND id != " + exceptId;
            }
            System.out.println(sql);

            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();
            rs = stmt.executeQuery(sql);

            return rs.next();

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }

            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
        return false;
    }

    public String getGeo(String table, String column, String parentColumn, String parentColumnId, String language) {
        Connection connection = null;
        ResultSet rs = null;
        Statement stmt = null;

        String option = ("");
        try {

            String sql = "SELECT ID, " + column + " FROM " + table + " WHERE " + parentColumn + " = '" + parentColumnId + "' AND isDeleted = 0";
            System.out.println(sql);

            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();
            rs = stmt.executeQuery(sql);

            while (rs.next()) {
                option += "<option class = '" + "form-control" + "' value = '" + rs.getString("ID") + "'>";
                option += rs.getString(column);
                option += "</option>";
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("Exception in CommonServlet in Geo module.. ");
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }

            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
        if (option.length() == 0) {
            option = "<option class = '" + "form-control" + "' value = '" + "-1" + "'>" +
                    (language.equals("2") ? "-- No data found --" : "-- কোন তথ্য পাওয়া যায় নি --") +
                    "</option>" + option;
        } else {
            option = "<option class = '" + "form-control" + "' value = '" + "-1" + "'>" +
                    (language.equals("2") ? "-- Please select one --" : "-- বাছাই করুন --") +
                    "</option>" + option;
        }
        return option;
    }

    public boolean hasMinistryBBSCode(String bbs, String id) {
        Connection connection = null;
        ResultSet rs = null;
        Statement stmt = null;

        String option = ("");
        try {

            String sql = "SELECT * FROM office_ministries WHERE reference_code = '" + bbs + "'";
            if (id != null && !"0".equals(id)) {
                sql += " AND id != '" + id + "'";
            }
            System.out.println(sql);

            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();
            rs = stmt.executeQuery(sql);

            while (rs.next()) {
                return true;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("Exception in CommonServlet in Geo module.. ");
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }

            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }

        return false;
    }

    public boolean hasOfficeCode(String code, String id) {
        Connection connection = null;
        ResultSet rs = null;
        Statement stmt = null;

        String option = ("");
        try {

            String sql = "SELECT * FROM offices WHERE office_code = '" + code + "'";
            if (id != null && !"0".equals(id)) {
                sql += " AND id != '" + id + "'";
            }
            System.out.println(sql);

            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();
            rs = stmt.executeQuery(sql);

            while (rs.next()) {
                return true;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("Exception in CommonServlet in Office Code Uniqueness checking.. ");
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }

            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }

        return false;
    }

    public boolean hasBatchNo(String batch, String id) {
        Connection connection = null;
        ResultSet rs = null;
        Statement stmt = null;

        String option = ("");
        try {

            String sql = "SELECT * FROM employee_batches WHERE batch_no = '" + batch + "'";
            if (id != null && !"0".equals(id)) {
                sql += " AND id != '" + id + "'";
            }
            System.out.println(sql);

            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();
            rs = stmt.executeQuery(sql);

            while (rs.next()) {
                return true;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("Exception in CommonServlet in Office Employee Batch Uniqueness checking.. ");
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }

            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }

        return false;
    }

    public boolean hasUnitCode(String code, String id) {
        Connection connection = null;
        ResultSet rs = null;
        Statement stmt = null;

        String option = ("");
        try {

            String sql = "SELECT * FROM office_units WHERE unit_nothi_code = '" + code + "'";
            if (id != null && !"0".equals(id)) {
                sql += " AND id != '" + id + "'";
            }
            System.out.println(sql);

            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();
            rs = stmt.executeQuery(sql);

            while (rs.next()) {
                return true;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("Exception in CommonServlet in Office Unit Code Uniqueness checking.. ");
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }

            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }

        return false;
    }
}
