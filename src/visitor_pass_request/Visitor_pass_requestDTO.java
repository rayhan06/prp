package visitor_pass_request;
import java.util.*; 
import util.*; 


public class Visitor_pass_requestDTO extends CommonDTO
{

	public long employeeRecordsId = 0;
	public long visitDate = 0;
    public String visitTime = "";
	public long requestDate = 0;
    public String purpose = "";
	public boolean isKnown = false;
	public long referenceEmployeeRecordsId = 0;
	public boolean isApproved = false;
	public long insertedByUserId = 0;
	public long insertionDate = 0;
    public String modifiedBy = "";
	
	public List<VisitorDetailsDTO> visitorDetailsDTOList = new ArrayList<>();
	public List<VisitorAllowedBelongingsDTO> visitorAllowedBelongingsDTOList = new ArrayList<>();
	
    @Override
	public String toString() {
            return "$Visitor_pass_requestDTO[" +
            " iD = " + iD +
            " employeeRecordsId = " + employeeRecordsId +
            " visitDate = " + visitDate +
            " visitTime = " + visitTime +
            " requestDate = " + requestDate +
            " purpose = " + purpose +
            " isKnown = " + isKnown +
            " referenceEmployeeRecordsId = " + referenceEmployeeRecordsId +
            " isApproved = " + isApproved +
            " jobCat = " + jobCat +
            " insertedByUserId = " + insertedByUserId +
            " insertionDate = " + insertionDate +
            " modifiedBy = " + modifiedBy +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}