package visitor_pass_request;
import java.util.*; 
import util.*; 


public class VisitorDetailsDTO extends CommonDTO
{

	public long visitorPassRequestId = 0;
    public String visitorName = "";
    public String vistorFatherName = "";
	public int identificationCat = 0;
    public String identificationNumber = "";
    public String contactNumber1 = "";
    public String contactNumber2 = "";
    public String visitorAddress = "";
	public long filesDropzone = 0;
	public long insertedByUserId = 0;
	public long insertionDate = 0;
    public String modifiedBy = "";
	
	public List<VisitorDetailsDTO> visitorDetailsDTOList = new ArrayList<>();
	public List<VisitorAllowedBelongingsDTO> visitorAllowedBelongingsDTOList = new ArrayList<>();
	
    @Override
	public String toString() {
            return "$VisitorDetailsDTO[" +
            " iD = " + iD +
            " visitorPassRequestId = " + visitorPassRequestId +
            " visitorName = " + visitorName +
            " vistorFatherName = " + vistorFatherName +
            " identificationCat = " + identificationCat +
            " identificationNumber = " + identificationNumber +
            " contactNumber1 = " + contactNumber1 +
            " contactNumber2 = " + contactNumber2 +
            " visitorAddress = " + visitorAddress +
            " filesDropzone = " + filesDropzone +
            " insertedByUserId = " + insertedByUserId +
            " insertionDate = " + insertionDate +
            " modifiedBy = " + modifiedBy +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}