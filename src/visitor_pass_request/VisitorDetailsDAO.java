package visitor_pass_request;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Hashtable;
import java.util.List;

import org.apache.log4j.Logger;

import common.CommonDTOService;
import common.ConnectionAndStatementUtil;
import dbm.DBMR;
import user.UserDTO;
import util.CommonDTO;
import util.NavigationService4;

@SuppressWarnings({ "rawtypes" })
public class VisitorDetailsDAO extends NavigationService4 implements CommonDTOService<VisitorDetailsDTO>{

	private static final Logger logger = Logger.getLogger(VisitorDetailsDAO.class);
	private static final String addSqlQuery = "INSERT INTO {tableName} (visitor_pass_request_id,visitor_name,vistor_father_name,identification_cat,"
			+ " identification_number, contact_number_1,contact_number_2,visitor_address,files_dropzone,inserted_by_user_id,insertion_date,modified_by,"
			+ " lastModificationTime,isDeleted,ID) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	
	private static final String updateSqlQuery = "UPDATE {tableName} SET visitor_pass_request_id=?,visitor_name=?,vistor_father_name=?,identification_cat=?,"
			+ " identification_number=?,contact_number_1=?,contact_number_2=?,visitor_address=?,files_dropzone=?,"
			+ " inserted_by_user_id=?,insertion_date=?,modified_by=?,lastModificationTime = ?  WHERE ID = ?";
	
	private static final String deleteSqlQuery = "delete from visitor_details WHERE visitor_pass_request_id= ?";
	
	private static final String GetByVisitorPassRequestIdSqlQuery = "SELECT * FROM visitor_details where isDeleted=0 and visitor_pass_request_id = ? "
			+ "order by visitor_details.lastModificationTime";

	public VisitorDetailsDAO(String tableName) {
		super(tableName);
		joinSQL = "";

		commonMaps = new VisitorDetailsMAPS(tableName);
	}

	public VisitorDetailsDAO() {
		this("visitor_details");
	}

	@Override
	public void set(PreparedStatement ps, VisitorDetailsDTO visitordetailsDTO, boolean isInsert) throws SQLException {
		int index = 0;
		ps.setObject(++index, visitordetailsDTO.visitorPassRequestId);
		ps.setObject(++index, visitordetailsDTO.visitorName);
		ps.setObject(++index, visitordetailsDTO.vistorFatherName);
		ps.setObject(++index, visitordetailsDTO.identificationCat);
		ps.setObject(++index, visitordetailsDTO.identificationNumber);
		ps.setObject(++index, visitordetailsDTO.contactNumber1);
		ps.setObject(++index, visitordetailsDTO.contactNumber2);
		ps.setObject(++index, visitordetailsDTO.visitorAddress);
		ps.setObject(++index, visitordetailsDTO.filesDropzone);
		ps.setObject(++index, visitordetailsDTO.insertedByUserId);
		ps.setObject(++index, visitordetailsDTO.insertionDate);
		ps.setObject(++index, visitordetailsDTO.modifiedBy);
		ps.setObject(++index, System.currentTimeMillis());
		if (isInsert) {
			ps.setObject(++index, 0);
		}
		ps.setObject(++index, visitordetailsDTO.iD);
	}

	@Override
	public VisitorDetailsDTO buildObjectFromResultSet(ResultSet rs){
		try{
			VisitorDetailsDTO visitordetailsDTO = new VisitorDetailsDTO();
			visitordetailsDTO.iD = rs.getLong("ID");
			visitordetailsDTO.visitorPassRequestId = rs.getLong("visitor_pass_request_id");
			visitordetailsDTO.visitorName = rs.getString("visitor_name");
			visitordetailsDTO.vistorFatherName = rs.getString("vistor_father_name");
			visitordetailsDTO.identificationCat = rs.getInt("identification_cat");
			visitordetailsDTO.identificationNumber = rs.getString("identification_number");
			visitordetailsDTO.contactNumber1 = rs.getString("contact_number_1");
			visitordetailsDTO.contactNumber2 = rs.getString("contact_number_2");
			visitordetailsDTO.visitorAddress = rs.getString("visitor_address");
			visitordetailsDTO.filesDropzone = rs.getLong("files_dropzone");
			visitordetailsDTO.insertedByUserId = rs.getLong("inserted_by_user_id");
			visitordetailsDTO.insertionDate = rs.getLong("insertion_date");
			visitordetailsDTO.modifiedBy = rs.getString("modified_by");
			visitordetailsDTO.isDeleted = rs.getInt("isDeleted");
			visitordetailsDTO.lastModificationTime = rs.getLong("lastModificationTime");
			return visitordetailsDTO;
		}catch (SQLException ex){
			logger.error(ex);
			return null;
		}
	}

	@Override
	public String getTableName() {
		return "visitor_details";
	}

	private static final char[] banglaDigits = { '০', '১', '২', '৩', '৪', '৫', '৬', '৭', '৮', '৯' };

	public String getDigitBanglaFromEnglish(int index) {
		String number = String.valueOf(index);
		StringBuilder builder = new StringBuilder();
		try {
			for (int i = 0; i < number.length(); i++) {
				if (Character.isDigit(number.charAt(i))) {
					if (((int) (number.charAt(i)) - 48) <= 9) {
						builder.append(banglaDigits[(int) (number.charAt(i)) - 48]);
					} else {
						builder.append(number.charAt(i));
					}
				} else {
					builder.append(number.charAt(i));
				}
			}
		} catch (Exception e) {
			return "";
		}
		return builder.toString();
	}

	public String getBanglaDateFromEnglish(String englishDate) {

		StringBuilder builder = new StringBuilder();
		try {
			for (int i = 0; i < englishDate.length(); i++) {
				if (Character.isDigit(englishDate.charAt(i))) {
					if (((int) (englishDate.charAt(i)) - 48) <= 9) {
						builder.append(banglaDigits[(int) (englishDate.charAt(i)) - 48]);
					} else {
						builder.append(englishDate.charAt(i));
					}
				} else {
					builder.append(englishDate.charAt(i));
				}
			}
		} catch (Exception e) {
			return "";
		}
		return builder.toString();
	}

	public long add(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((VisitorDetailsDTO) commonDTO,addSqlQuery,true);
	}

	public void deleteVisitorDetailsByVisitorPassRequestID(long visitorPassRequestID) throws Exception {
		ConnectionAndStatementUtil.getWritePrepareStatement(ps->{
			try {
				ps.setLong(1, visitorPassRequestID);
				ps.execute();
			} catch (SQLException e) {
				logger.error(e);
			}
		}, deleteSqlQuery);
	}

	public List<VisitorDetailsDTO> getVisitorDetailsDTOListByVisitorPassRequestID(long visitorPassRequestID) throws Exception {
		return getDTOs(GetByVisitorPassRequestIdSqlQuery, ps->{
			try {
				ps.setLong(1, visitorPassRequestID);
			} catch (SQLException e) {
				logger.error(e);
			}
		});
	}

	// need another getter for repository
	public CommonDTO getDTOByID(long ID) throws Exception {
		Connection connection = null;
		ResultSet rs;
		Statement stmt = null;
		VisitorDetailsDTO visitordetailsDTO = null;
		try {

			String sql = "SELECT * ";

			sql += " FROM " + tableName;

			sql += " WHERE ID=" + ID;

			printSql(sql);

			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();

			rs = stmt.executeQuery(sql);

			if (rs.next()) {
				visitordetailsDTO = buildObjectFromResultSet(rs);
			}

			VisitorDetailsDAO visitorDetailsDAO = new VisitorDetailsDAO("visitor_details");
			visitordetailsDTO.visitorDetailsDTOList = visitorDetailsDAO
					.getVisitorDetailsDTOListByVisitorPassRequestID(visitordetailsDTO.iD);

			VisitorAllowedBelongingsDAO visitorAllowedBelongingsDAO = new VisitorAllowedBelongingsDAO(
					"visitor_allowed_belongings");
			visitordetailsDTO.visitorAllowedBelongingsDTOList = visitorAllowedBelongingsDAO
					.getVisitorAllowedBelongingsDTOListByVisitorPassRequestID(visitordetailsDTO.iD);
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception ignored) {
			}

			try {
				if (connection != null) {
					DBMR.getInstance().freeConnection(connection);
				}
			} catch (Exception ignored) {
			}
		}
		return visitordetailsDTO;
	}

	public long update(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((VisitorDetailsDTO) commonDTO,updateSqlQuery,false);
	}

	// add repository
	public List<VisitorDetailsDTO> getAllVisitorDetails(boolean isFirstReload) {
		return getAllDTOs(isFirstReload);
	}

	public List<VisitorDetailsDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable,
			UserDTO userDTO) {
		return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
	}

	public List<VisitorDetailsDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable,
			UserDTO userDTO, String filter, boolean tableHasJobCat) {
		String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat);
		return getDTOs(sql);
	}

}
