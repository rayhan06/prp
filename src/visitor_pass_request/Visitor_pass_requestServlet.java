package visitor_pass_request;

import java.io.IOException;
import java.io.*;
import java.text.SimpleDateFormat;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import org.apache.log4j.Logger;

import login.LoginDTO;
import permission.MenuConstants;
import role.PermissionRepository;


import sessionmanager.SessionConstants;

import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;
import util.CommonRequestHandler;
import util.RecordNavigationManager4;

import java.util.*;
import javax.servlet.http.*;

import geolocation.GeoLocationDAO2;

import com.google.gson.Gson;

import pb.*;
import pbReport.*;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;

import workflow.WorkflowController;
import approval_execution_table.*;
import employee_records.Employee_recordsDAO;
import employee_records.Employee_recordsDTO;
import pb_notifications.Pb_notificationsDAO;



/**
 * Servlet implementation class Visitor_pass_requestServlet
 */
@WebServlet("/Visitor_pass_requestServlet")
@MultipartConfig
public class Visitor_pass_requestServlet extends HttpServlet 
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Visitor_pass_requestServlet.class);

    String tableName = "visitor_pass_request";
    Employee_recordsDAO recordsDAO = new Employee_recordsDAO();
	Visitor_pass_requestDAO visitor_pass_requestDAO;
	CommonRequestHandler commonRequestHandler;
	VisitorDetailsDAO visitorDetailsDAO;
	VisitorAllowedBelongingsDAO visitorAllowedBelongingsDAO;
    private final Gson gson = new Gson();
    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Visitor_pass_requestServlet() 
	{
        super();
    	try
    	{
			visitor_pass_requestDAO = new Visitor_pass_requestDAO(tableName);
			visitorDetailsDAO = new VisitorDetailsDAO("visitor_details");
			visitorAllowedBelongingsDAO = new VisitorAllowedBelongingsDAO("visitor_allowed_belongings");
			commonRequestHandler = new CommonRequestHandler(visitor_pass_requestDAO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }   

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		System.out.println("In doget request = " + request);
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		try
		{
			String actionType = request.getParameter("actionType");
			if(actionType.equals("getAddPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VISITOR_PASS_REQUEST_ADD))
				{
					List<Employee_recordsDTO> employeeRecordsDto = recordsDAO.getAllEmployee_records(true);
					long recordsID = WorkflowController.getEmployeeRecordIDFromOrganogramID(userDTO.organogramID);
					request.setAttribute("employeeRecordsDto", employeeRecordsDto);
					request.setAttribute("currentEmployeeRecordsId", recordsID);
					commonRequestHandler.getAddPage(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getEditPage"))
			{
				List<Employee_recordsDTO> employeeRecordsDto = recordsDAO.getAllEmployee_records(true);
				long recordsID = WorkflowController.getEmployeeRecordIDFromOrganogramID(userDTO.organogramID);
				request.setAttribute("employeeRecordsDto", employeeRecordsDto);
				request.setAttribute("currentEmployeeRecordsId", recordsID);
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VISITOR_PASS_REQUEST_UPDATE))
				{
					getVisitor_pass_request(request, response);					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}						
			}
			else if(actionType.equals("getURL"))
			{
				String URL = request.getParameter("URL");
				System.out.println("URL = " + URL);
				response.sendRedirect(URL);			
			}			
			else if(actionType.equals("search"))
			{
				System.out.println("search requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VISITOR_PASS_REQUEST_SEARCH))
				{
					if(isPermanentTable)
					{
						String filter = request.getParameter("filter");
						System.out.println("filter = " + filter);
						if(filter!=null)
						{
							filter = ""; //shouldn't be directly used, rather manipulate it.
							searchVisitor_pass_request(request, response, isPermanentTable, filter);
						}
						else
						{
							searchVisitor_pass_request(request, response, isPermanentTable, "");
						}
					}					
					else
					{
						//searchVisitor_pass_request(request, response, tempTableName, isPermanentTable);
					}
				}			
			}
			else if(actionType.equals("getApprovalPage"))
			{
				System.out.println("Visitor_pass_request getApprovalPage requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VISITOR_PASS_REQUEST_APPROVE))
				{
					searchVisitor_pass_request(request, response, false, "");
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}				
			}
			else if(actionType.equals("viewApprovalNotification"))
			{
				System.out.println("view requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.APPROVAL_TEST_SEARCH))
				{
					commonRequestHandler.viewApprovalNotification(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}				
			}
			else if(actionType.equals("view"))
			{
				int id = Integer.parseInt(request.getParameter("ID"));
				Visitor_pass_requestDTO visitorPassRequestDto = (Visitor_pass_requestDTO) visitor_pass_requestDAO.getDTOByID(id);
				Employee_recordsDTO employeeRecordsDto = recordsDAO.getEmployee_recordsDTOByID(visitorPassRequestDto.employeeRecordsId);
				Employee_recordsDTO referenceEmployeeRecordsDto = recordsDAO.getEmployee_recordsDTOByID(visitorPassRequestDto.referenceEmployeeRecordsId);
				System.out.println("view requested");
				if(employeeRecordsDto != null) {
					request.setAttribute("requestedEmployeeNameEn", employeeRecordsDto.nameEng);
					request.setAttribute("requestedEmployeeNameBn", employeeRecordsDto.nameBng);
				} else {
					request.setAttribute("requestedEmployeeNameEng", "Not Found");
				}
				if(referenceEmployeeRecordsDto != null) {
					request.setAttribute("referenceEmployeeNameEn", referenceEmployeeRecordsDto.nameEng);
					request.setAttribute("referenceEmployeeNameBn", referenceEmployeeRecordsDto.nameBng);
				} else {
					request.setAttribute("referenceEmployeeNameEng", "Not Found");
				}
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VISITOR_PASS_REQUEST_SEARCH))
				{
					commonRequestHandler.view(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else
			{
				request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}


	
	

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		// TODO Auto-generated method stub
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		System.out.println("doPost");
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		
		try
		{
			String actionType = request.getParameter("actionType");
			System.out.println("actionType = " + actionType);
			if(actionType.equals("add"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VISITOR_PASS_REQUEST_ADD))
				{
					System.out.println("going to  addVisitor_pass_request ");
					addVisitor_pass_request(request, response, true, userDTO, true);
				}
				else
				{
					System.out.println("Not going to  addVisitor_pass_request ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}

			else if(actionType.equals("SendToApprovalPath"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VISITOR_PASS_REQUEST_APPROVE))
				{
					commonRequestHandler.sendToApprovalPath(request, response, userDTO);
				}
				else
				{
					System.out.println("Not going to SendToApprovalPath ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}
			else if(actionType.equals("approve"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VISITOR_PASS_REQUEST_ADD))
				{					
					commonRequestHandler.approve(request, response, true, userDTO, true);
				}
				else
				{
					System.out.println("Not going to  addVisitor_pass_request ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else if(actionType.equals("reject"))
			{
				System.out.println("trying to approve");
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VISITOR_PASS_REQUEST_APPROVE))
				{					
					commonRequestHandler.approve(request, response, true, userDTO, false);
				}
				else
				{
					System.out.println("Not going to  addVisitor_pass_request ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}				
			}
			else if(actionType.equals("terminate"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VISITOR_PASS_REQUEST_ADD))
				{
					commonRequestHandler.terminate(request, response, userDTO);
				}
				else
				{
					System.out.println("Not going to  addVisitor_pass_request ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}
			else if(actionType.equals("skipStep"))
			{
				
				System.out.println("skipStep");
				commonRequestHandler.skipStep(request, response, userDTO);									
			}
			else if(actionType.equals("getDTO"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VISITOR_PASS_REQUEST_ADD))
				{
					getDTO(request, response);					
				}
				else
				{
					System.out.println("Not going to  addVisitor_pass_request ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else if(actionType.equals("edit"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VISITOR_PASS_REQUEST_UPDATE))
				{					
					addVisitor_pass_request(request, response, false, userDTO, isPermanentTable);					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("delete"))
			{								
				deleteVisitor_pass_request(request, response, userDTO);				
			}	
			else if(actionType.equals("search"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VISITOR_PASS_REQUEST_SEARCH))
				{
					searchVisitor_pass_request(request, response, true, "");
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getGeo"))
			{
				System.out.println("going to geoloc ");
				request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
			}
			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}
	
	private void getDTO(HttpServletRequest request, HttpServletResponse response) 
	{
		try 
		{
			System.out.println("In getDTO");
			Visitor_pass_requestDTO visitor_pass_requestDTO = (Visitor_pass_requestDTO)visitor_pass_requestDAO.getDTOByID(Long.parseLong(request.getParameter("ID")));
			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			
			String encoded = this.gson.toJson(visitor_pass_requestDTO);
			System.out.println("json encoded data = " + encoded);
			out.print(encoded);
			out.flush();
		}
		catch (NumberFormatException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	private void addVisitor_pass_request(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, boolean isPermanentTable) throws IOException 
	{
		// TODO Auto-generated method stub
		try 
		{
			request.setAttribute("failureMessage", "");
			System.out.println("%%%% addVisitor_pass_request");
			String path = getServletContext().getRealPath("/img2/");
			Visitor_pass_requestDTO visitor_pass_requestDTO;
			SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
						
			if(addFlag == true)
			{
				visitor_pass_requestDTO = new Visitor_pass_requestDTO();
			}
			else
			{
				visitor_pass_requestDTO = (Visitor_pass_requestDTO)visitor_pass_requestDAO.getDTOByID(Long.parseLong(request.getParameter("iD")));
			}
			
			String Value = "";

			Value = request.getParameter("employeeRecordsId");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("employeeRecordsId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				visitor_pass_requestDTO.employeeRecordsId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("visitDate");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("visitDate = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				try 
				{
					Date d = f.parse(Value);
					visitor_pass_requestDTO.visitDate = d.getTime();
				}
				catch (Exception e) 
				{
					e.printStackTrace();
				}
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("visitTime");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("visitTime = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				visitor_pass_requestDTO.visitTime = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("requestDate");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("requestDate = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				try 
				{
					Date d = f.parse(Value);
					visitor_pass_requestDTO.requestDate = d.getTime();
				}
				catch (Exception e) 
				{
					e.printStackTrace();
				}
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("purpose");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("purpose = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				visitor_pass_requestDTO.purpose = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("isKnown");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("isKnown = " + Value);
            visitor_pass_requestDTO.isKnown = Value != null && !Value.equalsIgnoreCase("");

			Value = request.getParameter("referenceEmployeeRecordsId");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("referenceEmployeeRecordsId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				visitor_pass_requestDTO.referenceEmployeeRecordsId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("isApproved");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("isApproved = " + Value);
            visitor_pass_requestDTO.isApproved = Value != null && !Value.equalsIgnoreCase("");

			Value = request.getParameter("jobCat");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("jobCat = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				visitor_pass_requestDTO.jobCat = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			if(addFlag)
			{
				visitor_pass_requestDTO.insertedByUserId = userDTO.ID;
			}


			if(addFlag)
			{
				Calendar c = Calendar.getInstance();
				c.set(Calendar.HOUR_OF_DAY, 0);
				c.set(Calendar.MINUTE, 0);
				c.set(Calendar.SECOND, 0);
				c.set(Calendar.MILLISECOND, 0);
				
				visitor_pass_requestDTO.insertionDate = c.getTimeInMillis();
			}			


			Value = request.getParameter("modifiedBy");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("modifiedBy = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				visitor_pass_requestDTO.modifiedBy = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			
			System.out.println("Done adding  addVisitor_pass_request dto = " + visitor_pass_requestDTO);
			long returnedID = -1;
			
			if(isPermanentTable == false) //add new row for validation and make the old row outdated
			{
				visitor_pass_requestDAO.setIsDeleted(visitor_pass_requestDTO.iD, CommonDTO.OUTDATED);
				returnedID = visitor_pass_requestDAO.add(visitor_pass_requestDTO);
				visitor_pass_requestDAO.setIsDeleted(returnedID, CommonDTO.WAITING_FOR_APPROVAL);
			}
			else if(addFlag == true)
			{
				returnedID = visitor_pass_requestDAO.manageWriteOperations(visitor_pass_requestDTO, SessionConstants.INSERT, -1, userDTO);
			}
			else
			{				
				returnedID = visitor_pass_requestDAO.manageWriteOperations(visitor_pass_requestDTO, SessionConstants.UPDATE, -1, userDTO);											
			}
			
			
			
			
			
			List<VisitorDetailsDTO> visitorDetailsDTOList = createVisitorDetailsDTOListByRequest(request);			
			if(addFlag == true || isPermanentTable == false) //add or validate
			{
				if(visitorDetailsDTOList != null)
				{				
					for(VisitorDetailsDTO visitorDetailsDTO: visitorDetailsDTOList)
					{
						visitorDetailsDTO.visitorPassRequestId = visitor_pass_requestDTO.iD; 
						visitorDetailsDAO.add(visitorDetailsDTO);
					}
				}
			
			}
			else
			{
				List<Long> childIdsFromRequest = visitorDetailsDAO.getChildIdsFromRequest(request, "visitorDetails");
				//delete the removed children
				visitorDetailsDAO.deleteChildrenNotInList("visitor_pass_request", "visitor_details", visitor_pass_requestDTO.iD, childIdsFromRequest);
				List<Long> childIDsInDatabase = visitorDetailsDAO.getChilIds("visitor_pass_request", "visitor_details", visitor_pass_requestDTO.iD);
				
				
				if(childIdsFromRequest != null)
				{
					for(int i = 0; i < childIdsFromRequest.size(); i ++)
					{
						Long childIDFromRequest = childIdsFromRequest.get(i);
						if(childIDsInDatabase.contains(childIDFromRequest))
						{
							VisitorDetailsDTO visitorDetailsDTO =  createVisitorDetailsDTOByRequestAndIndex(request, false, i);
							visitorDetailsDTO.visitorPassRequestId = visitor_pass_requestDTO.iD; 
							visitorDetailsDAO.update(visitorDetailsDTO);
						}
						else
						{
							VisitorDetailsDTO visitorDetailsDTO =  createVisitorDetailsDTOByRequestAndIndex(request, true, i);
							visitorDetailsDTO.visitorPassRequestId = visitor_pass_requestDTO.iD; 
							visitorDetailsDAO.add(visitorDetailsDTO);
						}
					}
				}
				else
				{
					visitorDetailsDAO.deleteVisitorDetailsByVisitorPassRequestID(visitor_pass_requestDTO.iD);
				}
				
			}
			
			List<VisitorAllowedBelongingsDTO> visitorAllowedBelongingsDTOList = createVisitorAllowedBelongingsDTOListByRequest(request);			
			if(addFlag == true || isPermanentTable == false) //add or validate
			{
				if(visitorAllowedBelongingsDTOList != null)
				{				
					for(VisitorAllowedBelongingsDTO visitorAllowedBelongingsDTO: visitorAllowedBelongingsDTOList)
					{
						visitorAllowedBelongingsDTO.visitorPassRequestId = visitor_pass_requestDTO.iD; 
						visitorAllowedBelongingsDAO.add(visitorAllowedBelongingsDTO);
					}
				}
			
			}
			else
			{
				List<Long> childIdsFromRequest = visitorAllowedBelongingsDAO.getChildIdsFromRequest(request, "visitorAllowedBelongings");
				//delete the removed children
				visitorAllowedBelongingsDAO.deleteChildrenNotInList("visitor_pass_request", "visitor_allowed_belongings", visitor_pass_requestDTO.iD, childIdsFromRequest);
				List<Long> childIDsInDatabase = visitorAllowedBelongingsDAO.getChilIds("visitor_pass_request", "visitor_allowed_belongings", visitor_pass_requestDTO.iD);
				
				
				if(childIdsFromRequest != null)
				{
					for(int i = 0; i < childIdsFromRequest.size(); i ++)
					{
						Long childIDFromRequest = childIdsFromRequest.get(i);
						if(childIDsInDatabase.contains(childIDFromRequest))
						{
							VisitorAllowedBelongingsDTO visitorAllowedBelongingsDTO =  createVisitorAllowedBelongingsDTOByRequestAndIndex(request, false, i);
							visitorAllowedBelongingsDTO.visitorPassRequestId = visitor_pass_requestDTO.iD; 
							visitorAllowedBelongingsDAO.update(visitorAllowedBelongingsDTO);
						}
						else
						{
							VisitorAllowedBelongingsDTO visitorAllowedBelongingsDTO =  createVisitorAllowedBelongingsDTOByRequestAndIndex(request, true, i);
							visitorAllowedBelongingsDTO.visitorPassRequestId = visitor_pass_requestDTO.iD; 
							visitorAllowedBelongingsDAO.add(visitorAllowedBelongingsDTO);
						}
					}
				}
				else
				{
					visitorAllowedBelongingsDAO.deleteVisitorAllowedBelongingsByVisitorPassRequestID(visitor_pass_requestDTO.iD);
				}
				
			}
			
			
			
			
			
			if(isPermanentTable)
			{
				String inPlaceSubmit = request.getParameter("inplacesubmit");
				
				if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
				{
					getVisitor_pass_request(request, response, returnedID);
				}
				else
				{
					response.sendRedirect("Visitor_pass_requestServlet?actionType=search");
				}
			}
			else
			{
				commonRequestHandler.validate(visitor_pass_requestDAO.getDTOByID(returnedID), request, response, userDTO);
			}
					
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	



	private List<VisitorDetailsDTO> createVisitorDetailsDTOListByRequest(HttpServletRequest request) throws Exception{ 
		List<VisitorDetailsDTO> visitorDetailsDTOList = new ArrayList<VisitorDetailsDTO>();
		if(request.getParameterValues("visitorDetails.iD") != null) 
		{
			int visitorDetailsItemNo = request.getParameterValues("visitorDetails.iD").length;
			
			
			for(int index=0;index<visitorDetailsItemNo;index++){
				VisitorDetailsDTO visitorDetailsDTO = createVisitorDetailsDTOByRequestAndIndex(request,true,index);
				visitorDetailsDTOList.add(visitorDetailsDTO);
			}
			
			return visitorDetailsDTOList;
		}
		return null;
	}
	private List<VisitorAllowedBelongingsDTO> createVisitorAllowedBelongingsDTOListByRequest(HttpServletRequest request) throws Exception{ 
		List<VisitorAllowedBelongingsDTO> visitorAllowedBelongingsDTOList = new ArrayList<VisitorAllowedBelongingsDTO>();
		if(request.getParameterValues("visitorAllowedBelongings.iD") != null) 
		{
			int visitorAllowedBelongingsItemNo = request.getParameterValues("visitorAllowedBelongings.iD").length;
			
			
			for(int index=0;index<visitorAllowedBelongingsItemNo;index++){
				VisitorAllowedBelongingsDTO visitorAllowedBelongingsDTO = createVisitorAllowedBelongingsDTOByRequestAndIndex(request,true,index);
				visitorAllowedBelongingsDTOList.add(visitorAllowedBelongingsDTO);
			}
			
			return visitorAllowedBelongingsDTOList;
		}
		return null;
	}
	
	
	private VisitorDetailsDTO createVisitorDetailsDTOByRequestAndIndex(HttpServletRequest request, boolean addFlag,int index) throws Exception{
	
		VisitorDetailsDTO visitorDetailsDTO;
		if(addFlag == true )
		{
			visitorDetailsDTO = new VisitorDetailsDTO();
		}
		else
		{
			visitorDetailsDTO = (VisitorDetailsDTO)visitorDetailsDAO.getDTOByID(Long.parseLong(request.getParameterValues("visitorDetails.iD")[index]));
		}
		String path = getServletContext().getRealPath("/img2/");
		SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
		
		
				
		String Value = "";
		Value = request.getParameterValues("visitorDetails.visitorPassRequestId")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		visitorDetailsDTO.visitorPassRequestId = Long.parseLong(Value);
		Value = request.getParameterValues("visitorDetails.visitorName")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		visitorDetailsDTO.visitorName = (Value);
		Value = request.getParameterValues("visitorDetails.vistorFatherName")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		visitorDetailsDTO.vistorFatherName = (Value);
		Value = request.getParameterValues("visitorDetails.identificationCat")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		visitorDetailsDTO.identificationCat = Integer.parseInt(Value);
		Value = request.getParameterValues("visitorDetails.identificationNumber")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		visitorDetailsDTO.identificationNumber = (Value);
		Value = request.getParameterValues("visitorDetails.contactNumber1")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		visitorDetailsDTO.contactNumber1 = (Value);
		Value = request.getParameterValues("visitorDetails.contactNumber2")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		visitorDetailsDTO.contactNumber2 = (Value);
		Value = request.getParameterValues("visitorDetails.visitorAddress")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

				
		{
			StringTokenizer tok3=new StringTokenizer(Value, ":");
			int i = 0;
			String addressDetails = "", address_id = "";
			while(tok3.hasMoreElements())
			{
				if(i == 0)
				{
					address_id = tok3.nextElement() + "";
				}
				else if(i == 1)
				{
					addressDetails = tok3.nextElement() + "";
				}
				i ++;
			}
			try
			{
				if(address_id.matches("-?\\d+"))
				{
					visitorDetailsDTO.visitorAddress = address_id + ":" + GeoLocationDAO2.getLocationText(Integer.parseInt(address_id))
					+ ":" + addressDetails;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}			
		}
		Value = request.getParameterValues("visitorDetails.filesDropzone")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		visitorDetailsDTO.filesDropzone = Long.parseLong(Value);
		Value = request.getParameterValues("visitorDetails.insertedByUserId")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		visitorDetailsDTO.insertedByUserId = Long.parseLong(Value);
		Value = request.getParameterValues("visitorDetails.insertionDate")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		visitorDetailsDTO.insertionDate = Long.parseLong(Value);
		Value = request.getParameterValues("visitorDetails.modifiedBy")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		visitorDetailsDTO.modifiedBy = (Value);
		return visitorDetailsDTO;
	
	}
	
	
	private VisitorAllowedBelongingsDTO createVisitorAllowedBelongingsDTOByRequestAndIndex(HttpServletRequest request, boolean addFlag,int index) throws Exception{
	
		VisitorAllowedBelongingsDTO visitorAllowedBelongingsDTO;
		if(addFlag == true )
		{
			visitorAllowedBelongingsDTO = new VisitorAllowedBelongingsDTO();
		}
		else
		{
			visitorAllowedBelongingsDTO = (VisitorAllowedBelongingsDTO)visitorAllowedBelongingsDAO.getDTOByID(Long.parseLong(request.getParameterValues("visitorAllowedBelongings.iD")[index]));
		}
		String path = getServletContext().getRealPath("/img2/");
		SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
		
		
				
		String Value = "";
		Value = request.getParameterValues("visitorAllowedBelongings.visitorPassRequestId")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		visitorAllowedBelongingsDTO.visitorPassRequestId = Long.parseLong(Value);
		Value = request.getParameterValues("visitorAllowedBelongings.visitorBelongingCat")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		visitorAllowedBelongingsDTO.visitorBelongingCat = Integer.parseInt(Value);
		Value = request.getParameterValues("visitorAllowedBelongings.totalCount")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		visitorAllowedBelongingsDTO.totalCount = (Value);
		Value = request.getParameterValues("visitorAllowedBelongings.remarks")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		visitorAllowedBelongingsDTO.remarks = (Value);
		Value = request.getParameterValues("visitorAllowedBelongings.insertedByUserId")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		visitorAllowedBelongingsDTO.insertedByUserId = Long.parseLong(Value);
		Value = request.getParameterValues("visitorAllowedBelongings.insertionDate")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		visitorAllowedBelongingsDTO.insertionDate = Long.parseLong(Value);
		Value = request.getParameterValues("visitorAllowedBelongings.modifiedBy")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		visitorAllowedBelongingsDTO.modifiedBy = (Value);
		return visitorAllowedBelongingsDTO;
	
	}
	
	
	

	private void deleteVisitor_pass_request(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO) throws IOException 
	{				
		try 
		{
			String[] IDsToDelete = request.getParameterValues("ID");
			for(int i = 0; i < IDsToDelete.length; i ++)
			{
				long id = Long.parseLong(IDsToDelete[i]);
				System.out.println("------ DELETING " + IDsToDelete[i]);
				
				
				Visitor_pass_requestDTO visitor_pass_requestDTO = (Visitor_pass_requestDTO)visitor_pass_requestDAO.getDTOByID(id);
				visitor_pass_requestDAO.manageWriteOperations(visitor_pass_requestDTO, SessionConstants.DELETE, id, userDTO);
				visitorDetailsDAO.deleteVisitorDetailsByVisitorPassRequestID(id);
				visitorAllowedBelongingsDAO.deleteVisitorAllowedBelongingsByVisitorPassRequestID(id);
				response.sendRedirect("Visitor_pass_requestServlet?actionType=search");
				
			}			
		}
		catch (Exception ex) 
		{
			ex.printStackTrace();
		}
		
	}

	private void getVisitor_pass_request(HttpServletRequest request, HttpServletResponse response, long id) throws ServletException, IOException
	{
		System.out.println("in getVisitor_pass_request");
		Visitor_pass_requestDTO visitor_pass_requestDTO = null;
		try 
		{
			visitor_pass_requestDTO = (Visitor_pass_requestDTO)visitor_pass_requestDAO.getDTOByID(id);
			boolean isPermanentTable = true;
			if(request.getParameter("isPermanentTable") != null)
			{
				isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
			}
			request.setAttribute("ID", visitor_pass_requestDTO.iD);
			request.setAttribute("visitor_pass_requestDTO",visitor_pass_requestDTO);
			request.setAttribute("visitor_pass_requestDAO",visitor_pass_requestDAO);
			
			String URL= "";
			
			String inPlaceEdit = request.getParameter("inplaceedit");
			String inPlaceSubmit = request.getParameter("inplacesubmit");
			String getBodyOnly = request.getParameter("getBodyOnly");
			
			if(inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase(""))
			{
				URL = "visitor_pass_request/visitor_pass_requestInPlaceEdit.jsp";	
				request.setAttribute("inplaceedit","");				
			}
			else if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
			{
				URL = "visitor_pass_request/visitor_pass_requestSearchRow.jsp";
				request.setAttribute("inplacesubmit","");					
			}
			else
			{
				if(getBodyOnly  != null && !getBodyOnly.equalsIgnoreCase(""))
				{
					URL = "visitor_pass_request/visitor_pass_requestEditBody.jsp?actionType=edit";
				}
				else
				{
					URL = "visitor_pass_request/visitor_pass_requestEdit.jsp?actionType=edit";
				}				
			}
			
			RequestDispatcher rd = request.getRequestDispatcher(URL);
			rd.forward(request, response);
		}
		catch (NumberFormatException e) 
		{
			e.printStackTrace();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	
	private void getVisitor_pass_request(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		getVisitor_pass_request(request, response, Long.parseLong(request.getParameter("ID")));	
	}
	
	private void searchVisitor_pass_request(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException
	{
		System.out.println("in  searchVisitor_pass_request 1");
		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String ajax = request.getParameter("ajax");
		boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);
		
        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
			SessionConstants.NAV_VISITOR_PASS_REQUEST,
			request,
			visitor_pass_requestDAO,
			SessionConstants.VIEW_VISITOR_PASS_REQUEST,
			SessionConstants.SEARCH_VISITOR_PASS_REQUEST,
			tableName,
			isPermanent,
			userDTO,
			filter,
			true);
        try
        {
			System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        }
        catch(Exception e)
        {
			System.out.println("failed to dojob" + e);
        }

		request.setAttribute("visitor_pass_requestDAO",visitor_pass_requestDAO);
        RequestDispatcher rd;
        if(!isPermanent)
        {
        	if(hasAjax == false)
	        {
	        	System.out.println("Going to visitor_pass_request/visitor_pass_requestApproval.jsp");
	        	rd = request.getRequestDispatcher("visitor_pass_request/visitor_pass_requestApproval.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to visitor_pass_request/visitor_pass_requestApprovalForm.jsp");
	        	rd = request.getRequestDispatcher("visitor_pass_request/visitor_pass_requestApprovalForm.jsp");
	        }
        }
        else
        {
	        if(hasAjax == false)
	        {
	        	System.out.println("Going to visitor_pass_request/visitor_pass_requestSearch.jsp");
	        	rd = request.getRequestDispatcher("visitor_pass_request/visitor_pass_requestSearch.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to visitor_pass_request/visitor_pass_requestSearchForm.jsp");
	        	rd = request.getRequestDispatcher("visitor_pass_request/visitor_pass_requestSearchForm.jsp");
	        }
        }
		rd.forward(request, response);
	}
	
}

