package visitor_pass_request;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class Visitor_pass_requestRepository implements Repository {
	Visitor_pass_requestDAO visitor_pass_requestDAO = null;
	
	public void setDAO(Visitor_pass_requestDAO visitor_pass_requestDAO)
	{
		this.visitor_pass_requestDAO = visitor_pass_requestDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Visitor_pass_requestRepository.class);
	Map<Long, Visitor_pass_requestDTO>mapOfVisitor_pass_requestDTOToiD;
	Map<Long, Set<Visitor_pass_requestDTO> >mapOfVisitor_pass_requestDTOToemployeeRecordsId;
	Map<Long, Set<Visitor_pass_requestDTO> >mapOfVisitor_pass_requestDTOTovisitDate;
	Map<String, Set<Visitor_pass_requestDTO> >mapOfVisitor_pass_requestDTOTovisitTime;
	Map<Long, Set<Visitor_pass_requestDTO> >mapOfVisitor_pass_requestDTOTorequestDate;
	Map<String, Set<Visitor_pass_requestDTO> >mapOfVisitor_pass_requestDTOTopurpose;
	Map<Boolean, Set<Visitor_pass_requestDTO> >mapOfVisitor_pass_requestDTOToisKnown;
	Map<Long, Set<Visitor_pass_requestDTO> >mapOfVisitor_pass_requestDTOToreferenceEmployeeRecordsId;
	Map<Boolean, Set<Visitor_pass_requestDTO> >mapOfVisitor_pass_requestDTOToisApproved;
	Map<Integer, Set<Visitor_pass_requestDTO> >mapOfVisitor_pass_requestDTOTojobCat;
	Map<Long, Set<Visitor_pass_requestDTO> >mapOfVisitor_pass_requestDTOToinsertedByUserId;
	Map<Long, Set<Visitor_pass_requestDTO> >mapOfVisitor_pass_requestDTOToinsertionDate;
	Map<String, Set<Visitor_pass_requestDTO> >mapOfVisitor_pass_requestDTOTomodifiedBy;
	Map<Long, Set<Visitor_pass_requestDTO> >mapOfVisitor_pass_requestDTOTolastModificationTime;


	static Visitor_pass_requestRepository instance = null;  
	private Visitor_pass_requestRepository(){
		mapOfVisitor_pass_requestDTOToiD = new ConcurrentHashMap<>();
		mapOfVisitor_pass_requestDTOToemployeeRecordsId = new ConcurrentHashMap<>();
		mapOfVisitor_pass_requestDTOTovisitDate = new ConcurrentHashMap<>();
		mapOfVisitor_pass_requestDTOTovisitTime = new ConcurrentHashMap<>();
		mapOfVisitor_pass_requestDTOTorequestDate = new ConcurrentHashMap<>();
		mapOfVisitor_pass_requestDTOTopurpose = new ConcurrentHashMap<>();
		mapOfVisitor_pass_requestDTOToisKnown = new ConcurrentHashMap<>();
		mapOfVisitor_pass_requestDTOToreferenceEmployeeRecordsId = new ConcurrentHashMap<>();
		mapOfVisitor_pass_requestDTOToisApproved = new ConcurrentHashMap<>();
		mapOfVisitor_pass_requestDTOTojobCat = new ConcurrentHashMap<>();
		mapOfVisitor_pass_requestDTOToinsertedByUserId = new ConcurrentHashMap<>();
		mapOfVisitor_pass_requestDTOToinsertionDate = new ConcurrentHashMap<>();
		mapOfVisitor_pass_requestDTOTomodifiedBy = new ConcurrentHashMap<>();
		mapOfVisitor_pass_requestDTOTolastModificationTime = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Visitor_pass_requestRepository getInstance(){
		if (instance == null){
			instance = new Visitor_pass_requestRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(visitor_pass_requestDAO == null)
		{
			return;
		}
		try {
			List<Visitor_pass_requestDTO> visitor_pass_requestDTOs = visitor_pass_requestDAO.getAllVisitor_pass_request(reloadAll);
			for(Visitor_pass_requestDTO visitor_pass_requestDTO : visitor_pass_requestDTOs) {
				Visitor_pass_requestDTO oldVisitor_pass_requestDTO = getVisitor_pass_requestDTOByID(visitor_pass_requestDTO.iD);
				if( oldVisitor_pass_requestDTO != null ) {
					mapOfVisitor_pass_requestDTOToiD.remove(oldVisitor_pass_requestDTO.iD);
				
					if(mapOfVisitor_pass_requestDTOToemployeeRecordsId.containsKey(oldVisitor_pass_requestDTO.employeeRecordsId)) {
						mapOfVisitor_pass_requestDTOToemployeeRecordsId.get(oldVisitor_pass_requestDTO.employeeRecordsId).remove(oldVisitor_pass_requestDTO);
					}
					if(mapOfVisitor_pass_requestDTOToemployeeRecordsId.get(oldVisitor_pass_requestDTO.employeeRecordsId).isEmpty()) {
						mapOfVisitor_pass_requestDTOToemployeeRecordsId.remove(oldVisitor_pass_requestDTO.employeeRecordsId);
					}
					
					if(mapOfVisitor_pass_requestDTOTovisitDate.containsKey(oldVisitor_pass_requestDTO.visitDate)) {
						mapOfVisitor_pass_requestDTOTovisitDate.get(oldVisitor_pass_requestDTO.visitDate).remove(oldVisitor_pass_requestDTO);
					}
					if(mapOfVisitor_pass_requestDTOTovisitDate.get(oldVisitor_pass_requestDTO.visitDate).isEmpty()) {
						mapOfVisitor_pass_requestDTOTovisitDate.remove(oldVisitor_pass_requestDTO.visitDate);
					}
					
					if(mapOfVisitor_pass_requestDTOTovisitTime.containsKey(oldVisitor_pass_requestDTO.visitTime)) {
						mapOfVisitor_pass_requestDTOTovisitTime.get(oldVisitor_pass_requestDTO.visitTime).remove(oldVisitor_pass_requestDTO);
					}
					if(mapOfVisitor_pass_requestDTOTovisitTime.get(oldVisitor_pass_requestDTO.visitTime).isEmpty()) {
						mapOfVisitor_pass_requestDTOTovisitTime.remove(oldVisitor_pass_requestDTO.visitTime);
					}
					
					if(mapOfVisitor_pass_requestDTOTorequestDate.containsKey(oldVisitor_pass_requestDTO.requestDate)) {
						mapOfVisitor_pass_requestDTOTorequestDate.get(oldVisitor_pass_requestDTO.requestDate).remove(oldVisitor_pass_requestDTO);
					}
					if(mapOfVisitor_pass_requestDTOTorequestDate.get(oldVisitor_pass_requestDTO.requestDate).isEmpty()) {
						mapOfVisitor_pass_requestDTOTorequestDate.remove(oldVisitor_pass_requestDTO.requestDate);
					}
					
					if(mapOfVisitor_pass_requestDTOTopurpose.containsKey(oldVisitor_pass_requestDTO.purpose)) {
						mapOfVisitor_pass_requestDTOTopurpose.get(oldVisitor_pass_requestDTO.purpose).remove(oldVisitor_pass_requestDTO);
					}
					if(mapOfVisitor_pass_requestDTOTopurpose.get(oldVisitor_pass_requestDTO.purpose).isEmpty()) {
						mapOfVisitor_pass_requestDTOTopurpose.remove(oldVisitor_pass_requestDTO.purpose);
					}
					
					if(mapOfVisitor_pass_requestDTOToisKnown.containsKey(oldVisitor_pass_requestDTO.isKnown)) {
						mapOfVisitor_pass_requestDTOToisKnown.get(oldVisitor_pass_requestDTO.isKnown).remove(oldVisitor_pass_requestDTO);
					}
					if(mapOfVisitor_pass_requestDTOToisKnown.get(oldVisitor_pass_requestDTO.isKnown).isEmpty()) {
						mapOfVisitor_pass_requestDTOToisKnown.remove(oldVisitor_pass_requestDTO.isKnown);
					}
					
					if(mapOfVisitor_pass_requestDTOToreferenceEmployeeRecordsId.containsKey(oldVisitor_pass_requestDTO.referenceEmployeeRecordsId)) {
						mapOfVisitor_pass_requestDTOToreferenceEmployeeRecordsId.get(oldVisitor_pass_requestDTO.referenceEmployeeRecordsId).remove(oldVisitor_pass_requestDTO);
					}
					if(mapOfVisitor_pass_requestDTOToreferenceEmployeeRecordsId.get(oldVisitor_pass_requestDTO.referenceEmployeeRecordsId).isEmpty()) {
						mapOfVisitor_pass_requestDTOToreferenceEmployeeRecordsId.remove(oldVisitor_pass_requestDTO.referenceEmployeeRecordsId);
					}
					
					if(mapOfVisitor_pass_requestDTOToisApproved.containsKey(oldVisitor_pass_requestDTO.isApproved)) {
						mapOfVisitor_pass_requestDTOToisApproved.get(oldVisitor_pass_requestDTO.isApproved).remove(oldVisitor_pass_requestDTO);
					}
					if(mapOfVisitor_pass_requestDTOToisApproved.get(oldVisitor_pass_requestDTO.isApproved).isEmpty()) {
						mapOfVisitor_pass_requestDTOToisApproved.remove(oldVisitor_pass_requestDTO.isApproved);
					}
					
					if(mapOfVisitor_pass_requestDTOTojobCat.containsKey(oldVisitor_pass_requestDTO.jobCat)) {
						mapOfVisitor_pass_requestDTOTojobCat.get(oldVisitor_pass_requestDTO.jobCat).remove(oldVisitor_pass_requestDTO);
					}
					if(mapOfVisitor_pass_requestDTOTojobCat.get(oldVisitor_pass_requestDTO.jobCat).isEmpty()) {
						mapOfVisitor_pass_requestDTOTojobCat.remove(oldVisitor_pass_requestDTO.jobCat);
					}
					
					if(mapOfVisitor_pass_requestDTOToinsertedByUserId.containsKey(oldVisitor_pass_requestDTO.insertedByUserId)) {
						mapOfVisitor_pass_requestDTOToinsertedByUserId.get(oldVisitor_pass_requestDTO.insertedByUserId).remove(oldVisitor_pass_requestDTO);
					}
					if(mapOfVisitor_pass_requestDTOToinsertedByUserId.get(oldVisitor_pass_requestDTO.insertedByUserId).isEmpty()) {
						mapOfVisitor_pass_requestDTOToinsertedByUserId.remove(oldVisitor_pass_requestDTO.insertedByUserId);
					}
					
					if(mapOfVisitor_pass_requestDTOToinsertionDate.containsKey(oldVisitor_pass_requestDTO.insertionDate)) {
						mapOfVisitor_pass_requestDTOToinsertionDate.get(oldVisitor_pass_requestDTO.insertionDate).remove(oldVisitor_pass_requestDTO);
					}
					if(mapOfVisitor_pass_requestDTOToinsertionDate.get(oldVisitor_pass_requestDTO.insertionDate).isEmpty()) {
						mapOfVisitor_pass_requestDTOToinsertionDate.remove(oldVisitor_pass_requestDTO.insertionDate);
					}
					
					if(mapOfVisitor_pass_requestDTOTomodifiedBy.containsKey(oldVisitor_pass_requestDTO.modifiedBy)) {
						mapOfVisitor_pass_requestDTOTomodifiedBy.get(oldVisitor_pass_requestDTO.modifiedBy).remove(oldVisitor_pass_requestDTO);
					}
					if(mapOfVisitor_pass_requestDTOTomodifiedBy.get(oldVisitor_pass_requestDTO.modifiedBy).isEmpty()) {
						mapOfVisitor_pass_requestDTOTomodifiedBy.remove(oldVisitor_pass_requestDTO.modifiedBy);
					}
					
					if(mapOfVisitor_pass_requestDTOTolastModificationTime.containsKey(oldVisitor_pass_requestDTO.lastModificationTime)) {
						mapOfVisitor_pass_requestDTOTolastModificationTime.get(oldVisitor_pass_requestDTO.lastModificationTime).remove(oldVisitor_pass_requestDTO);
					}
					if(mapOfVisitor_pass_requestDTOTolastModificationTime.get(oldVisitor_pass_requestDTO.lastModificationTime).isEmpty()) {
						mapOfVisitor_pass_requestDTOTolastModificationTime.remove(oldVisitor_pass_requestDTO.lastModificationTime);
					}
					
					
				}
				if(visitor_pass_requestDTO.isDeleted == 0) 
				{
					
					mapOfVisitor_pass_requestDTOToiD.put(visitor_pass_requestDTO.iD, visitor_pass_requestDTO);
				
					if( ! mapOfVisitor_pass_requestDTOToemployeeRecordsId.containsKey(visitor_pass_requestDTO.employeeRecordsId)) {
						mapOfVisitor_pass_requestDTOToemployeeRecordsId.put(visitor_pass_requestDTO.employeeRecordsId, new HashSet<>());
					}
					mapOfVisitor_pass_requestDTOToemployeeRecordsId.get(visitor_pass_requestDTO.employeeRecordsId).add(visitor_pass_requestDTO);
					
					if( ! mapOfVisitor_pass_requestDTOTovisitDate.containsKey(visitor_pass_requestDTO.visitDate)) {
						mapOfVisitor_pass_requestDTOTovisitDate.put(visitor_pass_requestDTO.visitDate, new HashSet<>());
					}
					mapOfVisitor_pass_requestDTOTovisitDate.get(visitor_pass_requestDTO.visitDate).add(visitor_pass_requestDTO);
					
					if( ! mapOfVisitor_pass_requestDTOTovisitTime.containsKey(visitor_pass_requestDTO.visitTime)) {
						mapOfVisitor_pass_requestDTOTovisitTime.put(visitor_pass_requestDTO.visitTime, new HashSet<>());
					}
					mapOfVisitor_pass_requestDTOTovisitTime.get(visitor_pass_requestDTO.visitTime).add(visitor_pass_requestDTO);
					
					if( ! mapOfVisitor_pass_requestDTOTorequestDate.containsKey(visitor_pass_requestDTO.requestDate)) {
						mapOfVisitor_pass_requestDTOTorequestDate.put(visitor_pass_requestDTO.requestDate, new HashSet<>());
					}
					mapOfVisitor_pass_requestDTOTorequestDate.get(visitor_pass_requestDTO.requestDate).add(visitor_pass_requestDTO);
					
					if( ! mapOfVisitor_pass_requestDTOTopurpose.containsKey(visitor_pass_requestDTO.purpose)) {
						mapOfVisitor_pass_requestDTOTopurpose.put(visitor_pass_requestDTO.purpose, new HashSet<>());
					}
					mapOfVisitor_pass_requestDTOTopurpose.get(visitor_pass_requestDTO.purpose).add(visitor_pass_requestDTO);
					
					if( ! mapOfVisitor_pass_requestDTOToisKnown.containsKey(visitor_pass_requestDTO.isKnown)) {
						mapOfVisitor_pass_requestDTOToisKnown.put(visitor_pass_requestDTO.isKnown, new HashSet<>());
					}
					mapOfVisitor_pass_requestDTOToisKnown.get(visitor_pass_requestDTO.isKnown).add(visitor_pass_requestDTO);
					
					if( ! mapOfVisitor_pass_requestDTOToreferenceEmployeeRecordsId.containsKey(visitor_pass_requestDTO.referenceEmployeeRecordsId)) {
						mapOfVisitor_pass_requestDTOToreferenceEmployeeRecordsId.put(visitor_pass_requestDTO.referenceEmployeeRecordsId, new HashSet<>());
					}
					mapOfVisitor_pass_requestDTOToreferenceEmployeeRecordsId.get(visitor_pass_requestDTO.referenceEmployeeRecordsId).add(visitor_pass_requestDTO);
					
					if( ! mapOfVisitor_pass_requestDTOToisApproved.containsKey(visitor_pass_requestDTO.isApproved)) {
						mapOfVisitor_pass_requestDTOToisApproved.put(visitor_pass_requestDTO.isApproved, new HashSet<>());
					}
					mapOfVisitor_pass_requestDTOToisApproved.get(visitor_pass_requestDTO.isApproved).add(visitor_pass_requestDTO);
					
					if( ! mapOfVisitor_pass_requestDTOTojobCat.containsKey(visitor_pass_requestDTO.jobCat)) {
						mapOfVisitor_pass_requestDTOTojobCat.put(visitor_pass_requestDTO.jobCat, new HashSet<>());
					}
					mapOfVisitor_pass_requestDTOTojobCat.get(visitor_pass_requestDTO.jobCat).add(visitor_pass_requestDTO);
					
					if( ! mapOfVisitor_pass_requestDTOToinsertedByUserId.containsKey(visitor_pass_requestDTO.insertedByUserId)) {
						mapOfVisitor_pass_requestDTOToinsertedByUserId.put(visitor_pass_requestDTO.insertedByUserId, new HashSet<>());
					}
					mapOfVisitor_pass_requestDTOToinsertedByUserId.get(visitor_pass_requestDTO.insertedByUserId).add(visitor_pass_requestDTO);
					
					if( ! mapOfVisitor_pass_requestDTOToinsertionDate.containsKey(visitor_pass_requestDTO.insertionDate)) {
						mapOfVisitor_pass_requestDTOToinsertionDate.put(visitor_pass_requestDTO.insertionDate, new HashSet<>());
					}
					mapOfVisitor_pass_requestDTOToinsertionDate.get(visitor_pass_requestDTO.insertionDate).add(visitor_pass_requestDTO);
					
					if( ! mapOfVisitor_pass_requestDTOTomodifiedBy.containsKey(visitor_pass_requestDTO.modifiedBy)) {
						mapOfVisitor_pass_requestDTOTomodifiedBy.put(visitor_pass_requestDTO.modifiedBy, new HashSet<>());
					}
					mapOfVisitor_pass_requestDTOTomodifiedBy.get(visitor_pass_requestDTO.modifiedBy).add(visitor_pass_requestDTO);
					
					if( ! mapOfVisitor_pass_requestDTOTolastModificationTime.containsKey(visitor_pass_requestDTO.lastModificationTime)) {
						mapOfVisitor_pass_requestDTOTolastModificationTime.put(visitor_pass_requestDTO.lastModificationTime, new HashSet<>());
					}
					mapOfVisitor_pass_requestDTOTolastModificationTime.get(visitor_pass_requestDTO.lastModificationTime).add(visitor_pass_requestDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Visitor_pass_requestDTO> getVisitor_pass_requestList() {
		List <Visitor_pass_requestDTO> visitor_pass_requests = new ArrayList<Visitor_pass_requestDTO>(this.mapOfVisitor_pass_requestDTOToiD.values());
		return visitor_pass_requests;
	}
	
	
	public Visitor_pass_requestDTO getVisitor_pass_requestDTOByID( long ID){
		return mapOfVisitor_pass_requestDTOToiD.get(ID);
	}
	
	
	public List<Visitor_pass_requestDTO> getVisitor_pass_requestDTOByemployee_records_id(long employee_records_id) {
		return new ArrayList<>( mapOfVisitor_pass_requestDTOToemployeeRecordsId.getOrDefault(employee_records_id,new HashSet<>()));
	}
	
	
	public List<Visitor_pass_requestDTO> getVisitor_pass_requestDTOByvisit_date(long visit_date) {
		return new ArrayList<>( mapOfVisitor_pass_requestDTOTovisitDate.getOrDefault(visit_date,new HashSet<>()));
	}
	
	
	public List<Visitor_pass_requestDTO> getVisitor_pass_requestDTOByvisit_time(String visit_time) {
		return new ArrayList<>( mapOfVisitor_pass_requestDTOTovisitTime.getOrDefault(visit_time,new HashSet<>()));
	}
	
	
	public List<Visitor_pass_requestDTO> getVisitor_pass_requestDTOByrequest_date(long request_date) {
		return new ArrayList<>( mapOfVisitor_pass_requestDTOTorequestDate.getOrDefault(request_date,new HashSet<>()));
	}
	
	
	public List<Visitor_pass_requestDTO> getVisitor_pass_requestDTOBypurpose(String purpose) {
		return new ArrayList<>( mapOfVisitor_pass_requestDTOTopurpose.getOrDefault(purpose,new HashSet<>()));
	}
	
	
	public List<Visitor_pass_requestDTO> getVisitor_pass_requestDTOByis_known(boolean is_known) {
		return new ArrayList<>( mapOfVisitor_pass_requestDTOToisKnown.getOrDefault(is_known,new HashSet<>()));
	}
	
	
	public List<Visitor_pass_requestDTO> getVisitor_pass_requestDTOByreference_employee_records_id(long reference_employee_records_id) {
		return new ArrayList<>( mapOfVisitor_pass_requestDTOToreferenceEmployeeRecordsId.getOrDefault(reference_employee_records_id,new HashSet<>()));
	}
	
	
	public List<Visitor_pass_requestDTO> getVisitor_pass_requestDTOByis_approved(boolean is_approved) {
		return new ArrayList<>( mapOfVisitor_pass_requestDTOToisApproved.getOrDefault(is_approved,new HashSet<>()));
	}
	
	
	public List<Visitor_pass_requestDTO> getVisitor_pass_requestDTOByjob_cat(int job_cat) {
		return new ArrayList<>( mapOfVisitor_pass_requestDTOTojobCat.getOrDefault(job_cat,new HashSet<>()));
	}
	
	
	public List<Visitor_pass_requestDTO> getVisitor_pass_requestDTOByinserted_by_user_id(long inserted_by_user_id) {
		return new ArrayList<>( mapOfVisitor_pass_requestDTOToinsertedByUserId.getOrDefault(inserted_by_user_id,new HashSet<>()));
	}
	
	
	public List<Visitor_pass_requestDTO> getVisitor_pass_requestDTOByinsertion_date(long insertion_date) {
		return new ArrayList<>( mapOfVisitor_pass_requestDTOToinsertionDate.getOrDefault(insertion_date,new HashSet<>()));
	}
	
	
	public List<Visitor_pass_requestDTO> getVisitor_pass_requestDTOBymodified_by(String modified_by) {
		return new ArrayList<>( mapOfVisitor_pass_requestDTOTomodifiedBy.getOrDefault(modified_by,new HashSet<>()));
	}
	
	
	public List<Visitor_pass_requestDTO> getVisitor_pass_requestDTOBylastModificationTime(long lastModificationTime) {
		return new ArrayList<>( mapOfVisitor_pass_requestDTOTolastModificationTime.getOrDefault(lastModificationTime,new HashSet<>()));
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "visitor_pass_request";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


