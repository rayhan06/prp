package visitor_pass_request;
import java.util.*; 
import util.*; 


public class VisitorAllowedBelongingsDTO extends CommonDTO
{

	public long visitorPassRequestId = 0;
	public int visitorBelongingCat = 0;
    public String totalCount = "";
    public String remarks = "";
	public long insertedByUserId = 0;
	public long insertionDate = 0;
    public String modifiedBy = "";
	
	public List<VisitorDetailsDTO> visitorDetailsDTOList = new ArrayList<>();
	public List<VisitorAllowedBelongingsDTO> visitorAllowedBelongingsDTOList = new ArrayList<>();
	
    @Override
	public String toString() {
            return "$VisitorAllowedBelongingsDTO[" +
            " iD = " + iD +
            " visitorPassRequestId = " + visitorPassRequestId +
            " visitorBelongingCat = " + visitorBelongingCat +
            " totalCount = " + totalCount +
            " remarks = " + remarks +
            " insertedByUserId = " + insertedByUserId +
            " insertionDate = " + insertionDate +
            " modifiedBy = " + modifiedBy +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}