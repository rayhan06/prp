package visitor_pass_request;
import java.util.*; 
import util.*;


public class VisitorAllowedBelongingsMAPS extends CommonMaps
{	
	public VisitorAllowedBelongingsMAPS(String tableName)
	{
		
		java_allfield_type_map.put("visitor_pass_request_id".toLowerCase(), "Long");
		java_allfield_type_map.put("visitor_belonging_cat".toLowerCase(), "Integer");
		java_allfield_type_map.put("remarks".toLowerCase(), "String");


		java_anyfield_search_map.put(tableName + ".visitor_pass_request_id".toLowerCase(), "Long");
		java_anyfield_search_map.put(tableName + ".total_count".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".remarks".toLowerCase(), "String");

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("visitorPassRequestId".toLowerCase(), "visitorPassRequestId".toLowerCase());
		java_DTO_map.put("visitorBelongingCat".toLowerCase(), "visitorBelongingCat".toLowerCase());
		java_DTO_map.put("totalCount".toLowerCase(), "totalCount".toLowerCase());
		java_DTO_map.put("remarks".toLowerCase(), "remarks".toLowerCase());
		java_DTO_map.put("insertedByUserId".toLowerCase(), "insertedByUserId".toLowerCase());
		java_DTO_map.put("insertionDate".toLowerCase(), "insertionDate".toLowerCase());
		java_DTO_map.put("modifiedBy".toLowerCase(), "modifiedBy".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("visitor_pass_request_id".toLowerCase(), "visitorPassRequestId".toLowerCase());
		java_SQL_map.put("visitor_belonging_cat".toLowerCase(), "visitorBelongingCat".toLowerCase());
		java_SQL_map.put("total_count".toLowerCase(), "totalCount".toLowerCase());
		java_SQL_map.put("remarks".toLowerCase(), "remarks".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Visitor Pass Request Id".toLowerCase(), "visitorPassRequestId".toLowerCase());
		java_Text_map.put("Visitor Belonging Cat".toLowerCase(), "visitorBelongingCat".toLowerCase());
		java_Text_map.put("Total Count".toLowerCase(), "totalCount".toLowerCase());
		java_Text_map.put("Remarks".toLowerCase(), "remarks".toLowerCase());
		java_Text_map.put("Inserted By User Id".toLowerCase(), "insertedByUserId".toLowerCase());
		java_Text_map.put("Insertion Date".toLowerCase(), "insertionDate".toLowerCase());
		java_Text_map.put("Modified By".toLowerCase(), "modifiedBy".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}