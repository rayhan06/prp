package visitor_pass_request;
import java.util.*; 
import util.*;


public class Visitor_pass_requestMAPS extends CommonMaps
{	
	public Visitor_pass_requestMAPS(String tableName)
	{
		
		java_allfield_type_map.put("visit_date".toLowerCase(), "Long");
		java_allfield_type_map.put("request_date".toLowerCase(), "Long");
		java_allfield_type_map.put("is_known".toLowerCase(), "Boolean");
		java_allfield_type_map.put("reference_employee_records_id".toLowerCase(), "Long");
		java_allfield_type_map.put("is_approved".toLowerCase(), "Boolean");


		java_anyfield_search_map.put(tableName + ".employee_records_id".toLowerCase(), "Long");
		java_anyfield_search_map.put(tableName + ".visit_date".toLowerCase(), "Long");
		java_anyfield_search_map.put(tableName + ".visit_time".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".request_date".toLowerCase(), "Long");
		java_anyfield_search_map.put(tableName + ".purpose".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".is_known".toLowerCase(), "Boolean");
		java_anyfield_search_map.put(tableName + ".reference_employee_records_id".toLowerCase(), "Long");
		java_anyfield_search_map.put(tableName + ".is_approved".toLowerCase(), "Boolean");

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("employeeRecordsId".toLowerCase(), "employeeRecordsId".toLowerCase());
		java_DTO_map.put("visitDate".toLowerCase(), "visitDate".toLowerCase());
		java_DTO_map.put("visitTime".toLowerCase(), "visitTime".toLowerCase());
		java_DTO_map.put("requestDate".toLowerCase(), "requestDate".toLowerCase());
		java_DTO_map.put("purpose".toLowerCase(), "purpose".toLowerCase());
		java_DTO_map.put("isKnown".toLowerCase(), "isKnown".toLowerCase());
		java_DTO_map.put("referenceEmployeeRecordsId".toLowerCase(), "referenceEmployeeRecordsId".toLowerCase());
		java_DTO_map.put("isApproved".toLowerCase(), "isApproved".toLowerCase());
		java_DTO_map.put("jobCat".toLowerCase(), "jobCat".toLowerCase());
		java_DTO_map.put("insertedByUserId".toLowerCase(), "insertedByUserId".toLowerCase());
		java_DTO_map.put("insertionDate".toLowerCase(), "insertionDate".toLowerCase());
		java_DTO_map.put("modifiedBy".toLowerCase(), "modifiedBy".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("employee_records_id".toLowerCase(), "employeeRecordsId".toLowerCase());
		java_SQL_map.put("visit_date".toLowerCase(), "visitDate".toLowerCase());
		java_SQL_map.put("visit_time".toLowerCase(), "visitTime".toLowerCase());
		java_SQL_map.put("request_date".toLowerCase(), "requestDate".toLowerCase());
		java_SQL_map.put("purpose".toLowerCase(), "purpose".toLowerCase());
		java_SQL_map.put("is_known".toLowerCase(), "isKnown".toLowerCase());
		java_SQL_map.put("reference_employee_records_id".toLowerCase(), "referenceEmployeeRecordsId".toLowerCase());
		java_SQL_map.put("is_approved".toLowerCase(), "isApproved".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Employee Records Id".toLowerCase(), "employeeRecordsId".toLowerCase());
		java_Text_map.put("Visit Date".toLowerCase(), "visitDate".toLowerCase());
		java_Text_map.put("Visit Time".toLowerCase(), "visitTime".toLowerCase());
		java_Text_map.put("Request Date".toLowerCase(), "requestDate".toLowerCase());
		java_Text_map.put("Purpose".toLowerCase(), "purpose".toLowerCase());
		java_Text_map.put("Is Known".toLowerCase(), "isKnown".toLowerCase());
		java_Text_map.put("Reference Employee Records Id".toLowerCase(), "referenceEmployeeRecordsId".toLowerCase());
		java_Text_map.put("Is Approved".toLowerCase(), "isApproved".toLowerCase());
		java_Text_map.put("Job Cat".toLowerCase(), "jobCat".toLowerCase());
		java_Text_map.put("Inserted By User Id".toLowerCase(), "insertedByUserId".toLowerCase());
		java_Text_map.put("Insertion Date".toLowerCase(), "insertionDate".toLowerCase());
		java_Text_map.put("Modified By".toLowerCase(), "modifiedBy".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}