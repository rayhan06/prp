package visitor_pass_request;
import util.*;

public class Visitor_pass_requestApprovalMAPS extends CommonMaps
{	
	public Visitor_pass_requestApprovalMAPS(String tableName)
	{
		
		java_allfield_type_map.put("visit_date".toLowerCase(), "Long");
		java_allfield_type_map.put("request_date".toLowerCase(), "Long");
		java_allfield_type_map.put("is_known".toLowerCase(), "Boolean");
		java_allfield_type_map.put("reference_employee_records_id".toLowerCase(), "Long");
		java_allfield_type_map.put("is_approved".toLowerCase(), "Boolean");

		java_allfield_type_map.put("job_cat", "Integer");
		java_allfield_type_map.put("approval_status_cat", "Integer");
		java_allfield_type_map.put("initiator", "Long");
		java_allfield_type_map.put("assigned_to", "Long");
		java_allfield_type_map.put("starting_date", "Long");
		java_allfield_type_map.put("ending_date", "Long");
		
		java_table_map.put("approval_status_cat", "approval_summary");
		java_table_map.put("initiator", "approval_summary");
		java_table_map.put("assigned_to", "approval_summary");
	}

}