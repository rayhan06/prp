package visitor_pass_request;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;
import sessionmanager.SessionConstants;
import java.sql.SQLException;


import org.apache.log4j.Logger;

import dbm.*;

import repository.RepositoryManager;

import util.*;
import pb.*;
import user.UserDTO;

public class Visitor_pass_requestDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());
	
	public CommonMaps approvalMaps = new Visitor_pass_requestApprovalMAPS("visitor_pass_request");
	
	public Visitor_pass_requestDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";

		commonMaps = new Visitor_pass_requestMAPS(tableName);
	}
	
	public Visitor_pass_requestDAO()
	{
		this("visitor_pass_request");		
	}
	
	public void set(PreparedStatement ps, Visitor_pass_requestDTO visitor_pass_requestDTO, boolean isInsert) throws SQLException
	{
		int index = 1;
		long lastModificationTime = System.currentTimeMillis();	
		if(isInsert)
		{
			ps.setObject(index++,visitor_pass_requestDTO.iD);
		}
		ps.setObject(index++,visitor_pass_requestDTO.employeeRecordsId);
		ps.setObject(index++,visitor_pass_requestDTO.visitDate);
		ps.setObject(index++,visitor_pass_requestDTO.visitTime);
		ps.setObject(index++,visitor_pass_requestDTO.requestDate);
		ps.setObject(index++,visitor_pass_requestDTO.purpose);
		ps.setObject(index++,visitor_pass_requestDTO.isKnown);
		ps.setObject(index++,visitor_pass_requestDTO.referenceEmployeeRecordsId);
		ps.setObject(index++,visitor_pass_requestDTO.isApproved);
		ps.setObject(index++,visitor_pass_requestDTO.jobCat);
		ps.setObject(index++,visitor_pass_requestDTO.insertedByUserId);
		ps.setObject(index++,visitor_pass_requestDTO.insertionDate);
		ps.setObject(index++,visitor_pass_requestDTO.modifiedBy);
		if(isInsert)
		{
			ps.setObject(index++, 0);
			ps.setObject(index++, lastModificationTime);
		}
	}
	
	public void get(Visitor_pass_requestDTO visitor_pass_requestDTO, ResultSet rs) throws SQLException
	{
		visitor_pass_requestDTO.iD = rs.getLong("ID");
		visitor_pass_requestDTO.employeeRecordsId = rs.getLong("employee_records_id");
		visitor_pass_requestDTO.visitDate = rs.getLong("visit_date");
		visitor_pass_requestDTO.visitTime = rs.getString("visit_time");
		visitor_pass_requestDTO.requestDate = rs.getLong("request_date");
		visitor_pass_requestDTO.purpose = rs.getString("purpose");
		visitor_pass_requestDTO.isKnown = rs.getBoolean("is_known");
		visitor_pass_requestDTO.referenceEmployeeRecordsId = rs.getLong("reference_employee_records_id");
		visitor_pass_requestDTO.isApproved = rs.getBoolean("is_approved");
		visitor_pass_requestDTO.jobCat = rs.getInt("job_cat");
		visitor_pass_requestDTO.insertedByUserId = rs.getLong("inserted_by_user_id");
		visitor_pass_requestDTO.insertionDate = rs.getLong("insertion_date");
		visitor_pass_requestDTO.modifiedBy = rs.getString("modified_by");
		visitor_pass_requestDTO.isDeleted = rs.getInt("isDeleted");
		visitor_pass_requestDTO.lastModificationTime = rs.getLong("lastModificationTime");
	}
	
	
	
	public long add(CommonDTO commonDTO) throws Exception
	{
		
		Visitor_pass_requestDTO visitor_pass_requestDTO = (Visitor_pass_requestDTO)commonDTO;
		
		Connection connection = null;
		PreparedStatement ps = null;

		long lastModificationTime = System.currentTimeMillis();	

		try{
			connection = DBMW.getInstance().getConnection();
			
			if(connection == null)
			{
				System.out.println("nullconn");
			}

			visitor_pass_requestDTO.iD = DBMW.getInstance().getNextSequenceId(tableName);

			String sql = "INSERT INTO " + tableName;
			
			sql += " (";
			sql += "ID";
			sql += ", ";
			sql += "employee_records_id";
			sql += ", ";
			sql += "visit_date";
			sql += ", ";
			sql += "visit_time";
			sql += ", ";
			sql += "request_date";
			sql += ", ";
			sql += "purpose";
			sql += ", ";
			sql += "is_known";
			sql += ", ";
			sql += "reference_employee_records_id";
			sql += ", ";
			sql += "is_approved";
			sql += ", ";
			sql += "job_cat";
			sql += ", ";
			sql += "inserted_by_user_id";
			sql += ", ";
			sql += "insertion_date";
			sql += ", ";
			sql += "modified_by";
			sql += ", ";
			sql += "isDeleted";
			sql += ", ";
			sql += "lastModificationTime";
		
			sql += ")";
			
			
            sql += " VALUES(";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			
			sql += ")";
				
			ps = connection.prepareStatement(sql);
			set(ps, visitor_pass_requestDTO, true);
			ps.execute();
			
			
			recordUpdateTime(connection,lastModificationTime);

		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if (ps != null) {
					ps.close();
				}
			} catch(Exception e){}
			try{
				if(connection != null)
				{
					DBMW.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		return visitor_pass_requestDTO.iD;		
	}
		
	

	//need another getter for repository
	public CommonDTO getDTOByID (long ID) throws Exception
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Visitor_pass_requestDTO visitor_pass_requestDTO = null;
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
			
            sql += " WHERE ID=" + ID;
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			if(rs.next()){
				visitor_pass_requestDTO = new Visitor_pass_requestDTO();

				get(visitor_pass_requestDTO, rs);

			}			
			
			
			
			VisitorDetailsDAO visitorDetailsDAO = new VisitorDetailsDAO("visitor_details");			
			List<VisitorDetailsDTO> visitorDetailsDTOList = visitorDetailsDAO.getVisitorDetailsDTOListByVisitorPassRequestID(visitor_pass_requestDTO.iD);
			visitor_pass_requestDTO.visitorDetailsDTOList = visitorDetailsDTOList;
			
			VisitorAllowedBelongingsDAO visitorAllowedBelongingsDAO = new VisitorAllowedBelongingsDAO("visitor_allowed_belongings");			
			List<VisitorAllowedBelongingsDTO> visitorAllowedBelongingsDTOList = visitorAllowedBelongingsDAO.getVisitorAllowedBelongingsDTOListByVisitorPassRequestID(visitor_pass_requestDTO.iD);
			visitor_pass_requestDTO.visitorAllowedBelongingsDTOList = visitorAllowedBelongingsDTOList;
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return visitor_pass_requestDTO;
	}
	
	public long update(CommonDTO commonDTO) throws Exception
	{		
		Visitor_pass_requestDTO visitor_pass_requestDTO = (Visitor_pass_requestDTO)commonDTO;
		
		Connection connection = null;
		PreparedStatement ps = null;

		long lastModificationTime = System.currentTimeMillis();	
		try{
			connection = DBMW.getInstance().getConnection();

			String sql = "UPDATE " + tableName;
			
			sql += " SET ";
			sql += "employee_records_id=?";
			sql += ", ";
			sql += "visit_date=?";
			sql += ", ";
			sql += "visit_time=?";
			sql += ", ";
			sql += "request_date=?";
			sql += ", ";
			sql += "purpose=?";
			sql += ", ";
			sql += "is_known=?";
			sql += ", ";
			sql += "reference_employee_records_id=?";
			sql += ", ";
			sql += "is_approved=?";
			sql += ", ";
			sql += "job_cat=?";
			sql += ", ";
			sql += "inserted_by_user_id=?";
			sql += ", ";
			sql += "insertion_date=?";
			sql += ", ";
			sql += "modified_by=?";
			sql += ", lastModificationTime = "	+ lastModificationTime + "";
            sql += " WHERE ID = " + visitor_pass_requestDTO.iD;
				

			ps = connection.prepareStatement(sql);
			set(ps, visitor_pass_requestDTO, false);
			ps.executeUpdate();
			

			recordUpdateTime(connection,lastModificationTime);

						
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if (ps != null) {
					ps.close();
				}
			} catch(Exception e){}
			try{
				if(connection != null)
				{
					DBMW.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		return visitor_pass_requestDTO.iD;
	}
	
	
	public List<Visitor_pass_requestDTO> getDTOs(Collection recordIDs){
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Visitor_pass_requestDTO visitor_pass_requestDTO = null;
		List<Visitor_pass_requestDTO> visitor_pass_requestDTOList = new ArrayList<>();
		if(recordIDs.isEmpty()){
			return visitor_pass_requestDTOList;
		}
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
            
            sql += " WHERE ID IN ( ";

			for(int i = 0;i<recordIDs.size();i++){
				if(i!=0){
					sql+=",";
				}
				sql+=((ArrayList)recordIDs).get(i);
			}
			sql+=")  order by lastModificationTime desc";
			
			printSql(sql);
			
			logger.debug("sql " + sql);
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				visitor_pass_requestDTO = new Visitor_pass_requestDTO();
				get(visitor_pass_requestDTO, rs);
				System.out.println("got this DTO: " + visitor_pass_requestDTO);
				
				visitor_pass_requestDTOList.add(visitor_pass_requestDTO);

			}			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return visitor_pass_requestDTOList;
	
	}
	
	

	
	
	
	//add repository
	public List<Visitor_pass_requestDTO> getAllVisitor_pass_request (boolean isFirstReload)
    {
		List<Visitor_pass_requestDTO> visitor_pass_requestDTOList = new ArrayList<>();

		String sql = "SELECT * FROM visitor_pass_request";
		sql += " WHERE ";
	

		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by visitor_pass_request.lastModificationTime desc";
		printSql(sql);
		
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;

		
		try{
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();
			rs = stmt.executeQuery(sql);
			

			while(rs.next()){
				Visitor_pass_requestDTO visitor_pass_requestDTO = new Visitor_pass_requestDTO();
				get(visitor_pass_requestDTO, rs);
				
				visitor_pass_requestDTOList.add(visitor_pass_requestDTO);
			}			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}

		return visitor_pass_requestDTOList;
    }

	
	public List<Visitor_pass_requestDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO)
	{
		return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
	}

	
	public List<Visitor_pass_requestDTO> getDTOs(Hashtable  p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
			String filter, boolean tableHasJobCat)
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		List<Visitor_pass_requestDTO> visitor_pass_requestDTOList = new ArrayList<>();

		try{
			
			String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat);
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				Visitor_pass_requestDTO visitor_pass_requestDTO = new Visitor_pass_requestDTO();
				get(visitor_pass_requestDTO, rs);
				
				visitor_pass_requestDTOList.add(visitor_pass_requestDTO);
			}					
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return visitor_pass_requestDTOList;
	
	}
	public String getSqlWithSearchCriteriaForApprovalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
			UserDTO userDTO, String filter, boolean tableHasJobCat)
	{
		
		boolean viewAll = false;
		
		if(p_searchCriteria != null && p_searchCriteria.get("ViewAll") != null)
		{
			System.out.println("ViewAll = " + p_searchCriteria.get("ViewAll"));
			viewAll = true;
		}
		else
		{
			System.out.println("ViewAll is null ");
		}
		
		String sql = "SELECT ";
		if(category == GETIDS)
		{
			sql += " " + tableName + ".ID as ID ";
		}
		else if(category == GETCOUNT)
		{
			sql += " count( " + tableName + ".ID) as countID ";
		}
		else if(category == GETDTOS)
		{
			sql += " distinct " + tableName + ".* ";
		}
		sql += "FROM " + tableName + " ";

		
		sql += " join approval_execution_table as aet on ("
				+ tableName + ".id = aet.updated_row_id "
				+ ")";
		
		sql += " join approval_summary on ("
				+ "aet.previous_row_id = approval_summary.table_id and  approval_summary.table_name = '" + tableName + "'"
				+ ")";
		
		if(!viewAll)
		{
																	
			sql += " left join approval_path_details on ("
					+ "aet.approval_path_id = approval_path_details.approval_path_id "
					+ "and aet.approval_path_order = approval_path_details.approval_order "
					+ ")";
		}
									
		
	

		

		String AllFieldSql = "";
		
	
		
		if(p_searchCriteria != null)
		{
						
			
			Enumeration names = p_searchCriteria.keys();
			String str, value;
			
			
			
			AllFieldSql = "(";
			int i = 0;
			while(names.hasMoreElements())
			{				
				str = (String) names.nextElement();
				value = (String)p_searchCriteria.get(str);
		        System.out.println(str + ": " + value);
		        if(approvalMaps.java_allfield_type_map.get(str.toLowerCase()) != null &&  !approvalMaps.java_allfield_type_map.get(str.toLowerCase()).equalsIgnoreCase("")
		        		&& !str.equalsIgnoreCase("AnyField")
		        		&&
						( ( value != null && !value.equalsIgnoreCase("") ) || commonMaps.rangeMap.get( str.toLowerCase() ) != null ) )
		        {
					if(p_searchCriteria.get(str).equals("any"))
		        	{
		        		continue;
		        	}
					
		        	if( i > 0)
		        	{
		        		AllFieldSql+= " AND  ";
		        	}

		        	String dataType = approvalMaps.java_allfield_type_map.get(str.toLowerCase()); 
		        	
		        	String fromTable = tableName;
		        	if(approvalMaps.java_table_map.get(str) != null)
		    		{
		    			fromTable = approvalMaps.java_table_map.get(str);
		    		}
		    		
		        	if(str.equalsIgnoreCase("starting_date") || str.equalsIgnoreCase("ending_date"))
		        	{
		        		String string_date = (String) p_searchCriteria.get(str);
		        		long milliseconds = 0;

		        		SimpleDateFormat f = new SimpleDateFormat("dd-MM-yyyy");
		        		try {
		        		    Date d = f.parse(string_date);
		        		    milliseconds = d.getTime();
		        		} catch (Exception e) {
		        		    e.printStackTrace();
		        		}
		        		if(str.equalsIgnoreCase("starting_date"))
		        		{
		        			AllFieldSql += "approval_summary.date_of_initiation >= " +  milliseconds;
		        		}
		        		else
		        		{
		        			AllFieldSql += "approval_summary.date_of_initiation <= " +  milliseconds;
		        		}
		        	}
		        	else
		        	{
		        		if( dataType.equals("String") )
			        	{
			        		AllFieldSql += "" + fromTable + "." + str.toLowerCase() + " like '%" + p_searchCriteria.get(str) + "%'";
			        	}
			        	else if( dataType.equals("Integer") || dataType.equals("Long"))
			        	{
			        		AllFieldSql += "" + fromTable + "." + str.toLowerCase() + " = " + p_searchCriteria.get(str) ;
			        	}
		        	}
		        	
		        	
		        	
		        	
		        	i ++;
		        }
			}
			
			AllFieldSql += ")";
			System.out.println("AllFieldSql = " + AllFieldSql);
			
			
		}
		sql += " WHERE ";
		
		if(viewAll)
		{
			sql += " (" + tableName + ".isDeleted != " + CommonDTO.DELETED + " and " + tableName + ".isDeleted != " + CommonDTO.OUTDATED
					+ ")";
		}
		else
		{
			sql += " (" + tableName + ".isDeleted != " + CommonDTO.DELETED + " and " + tableName + ".isDeleted != " + CommonDTO.OUTDATED
				+ " and (approval_path_details.organogram_id = " + userDTO.organogramID 
						+ " or " +  userDTO.organogramID + " in ("
						+ " select organogram_id from approval_execution_table where previous_row_id = aet.previous_row_id"
						+ "))"
				+ ")";
		}
		
		
		if(!filter.equalsIgnoreCase(""))
		{
			sql += " and " + filter + " ";
		}

		if(!AllFieldSql.equals("()") && !AllFieldSql.equals(""))
		{			
			sql += " AND " + AllFieldSql;
		}
		
	
			
		sql += "  order by " + tableName + ".lastModificationTime desc ";

		printSql(sql);
		
		if(limit >= 0)
		{
			sql += " limit " + limit;
		}
		if(offset >= 0)
		{
			sql += " offset " + offset;
		}
		
		System.out.println("-------------- sql = " + sql);
		
		return sql;
	}
	
	public String getSqlWithSearchCriteriaForNormalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
			UserDTO userDTO, String filter, boolean tableHasJobCat)
    {
		boolean viewAll = false;
		
		if(p_searchCriteria != null && p_searchCriteria.get("ViewAll") != null)
		{
			System.out.println("ViewAll = " + p_searchCriteria.get("ViewAll"));
			viewAll = true;
		}
		else
		{
			System.out.println("ViewAll is null ");
		}
		
		String sql = "SELECT ";
		if(category == GETIDS)
		{
			sql += " " + tableName + ".ID as ID ";
		}
		else if(category == GETCOUNT)
		{
			sql += " count( " + tableName + ".ID) as countID ";
		}
		else if(category == GETDTOS)
		{
			sql += " distinct " + tableName + ".* ";
		}
		sql += "FROM " + tableName + " ";
		sql += joinSQL;
		

		
		String AnyfieldSql = "";
		String AllFieldSql = "";
		
		if(p_searchCriteria != null)
		{
						
			
			Enumeration names = p_searchCriteria.keys();
			String str, value;
			
			AnyfieldSql = "(";
			
			if(p_searchCriteria.get("AnyField")!= null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase(""))
			{
				int i = 0;
				Iterator it = (commonMaps).java_anyfield_search_map.entrySet().iterator();
				while(it.hasNext())
				{
					if( i > 0)
		        	{
						AnyfieldSql+= " OR  ";
		        	}
					Entry pair = (Entry)it.next();
					AnyfieldSql+= pair.getKey() + " like '%" + p_searchCriteria.get("AnyField").toString() + "%'";
					i ++;
				}						
			}
			AnyfieldSql += ")";
			System.out.println("AnyfieldSql = " + AnyfieldSql);
			
			AllFieldSql = "(";
			int i = 0;
			while(names.hasMoreElements())
			{				
				str = (String) names.nextElement();
				value = (String)p_searchCriteria.get(str);
		        System.out.println(str + ": " + value);
				if((commonMaps).java_allfield_type_map.get(str.toLowerCase()) != null &&  !(commonMaps).java_allfield_type_map.get(str.toLowerCase()).equalsIgnoreCase("")
						&& !str.equalsIgnoreCase("AnyField")
						&& value != null && !value.equalsIgnoreCase(""))
				{
					if(p_searchCriteria.get(str).equals("any"))
					{
						continue;
					}

					if( i > 0)
					{
						AllFieldSql+= " AND  ";
					}
					if((commonMaps).java_allfield_type_map.get(str.toLowerCase()).equals("String"))
					{
						AllFieldSql += "" + tableName + "." + str.toLowerCase() + " like '%" + p_searchCriteria.get(str) + "%'";
					}
					else
					{
						AllFieldSql += "" + tableName + "." + str.toLowerCase() + " = '" + p_searchCriteria.get(str) + "'";
					}
					i ++;
				}
			}
			
			AllFieldSql += ")";
			System.out.println("AllFieldSql = " + AllFieldSql);
			
			
		}
		
		
		sql += " WHERE ";
		
		sql += " (" + tableName + ".isDeleted = 0 ";			
		sql += ")";
		
		
		if(!filter.equalsIgnoreCase(""))
		{
			sql += " and " + filter + " ";
		}
		
		if(!AnyfieldSql.equals("()") && !AnyfieldSql.equals(""))
		{
			sql += " AND " + AnyfieldSql;
			
		}
		if(!AllFieldSql.equals("()") && !AllFieldSql.equals(""))
		{			
			sql += " AND " + AllFieldSql;
		}
		
	
			
		sql += " order by " + tableName + ".lastModificationTime desc ";

		printSql(sql);
		
		if(limit >= 0)
		{
			sql += " limit " + limit;
		}
		if(offset >= 0)
		{
			sql += " offset " + offset;
		}
		
		System.out.println("-------------- sql = " + sql);
		
		return sql;
    }
	
	
	public String getSqlWithSearchCriteria(Hashtable p_searchCriteria, int limit, int offset, int category,
			boolean isPermanentTable, UserDTO userDTO, String filter, boolean tableHasJobCat)
    {
		if(isPermanentTable)
		{
			return getSqlWithSearchCriteriaForNormalSearch(p_searchCriteria, limit, offset, category, userDTO, filter, tableHasJobCat);
		}
		else
		{
			return getSqlWithSearchCriteriaForApprovalSearch(p_searchCriteria, limit, offset, category, userDTO, filter, tableHasJobCat);
		}
				
    }
				
}
	