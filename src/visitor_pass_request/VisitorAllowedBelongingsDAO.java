package visitor_pass_request;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;
import sessionmanager.SessionConstants;
import java.sql.SQLException;


import org.apache.log4j.Logger;

import dbm.*;

import repository.RepositoryManager;

import util.*;
import pb.*;
import user.UserDTO;

public class VisitorAllowedBelongingsDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());
	
	
	public VisitorAllowedBelongingsDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";

		commonMaps = new VisitorAllowedBelongingsMAPS(tableName);
	}
	
	public VisitorAllowedBelongingsDAO()
	{
		this("visitor_allowed_belongings");		
	}
	
	public void set(PreparedStatement ps, VisitorAllowedBelongingsDTO visitorallowedbelongingsDTO, boolean isInsert) throws SQLException
	{
		int index = 1;
		long lastModificationTime = System.currentTimeMillis();	
		if(isInsert)
		{
			ps.setObject(index++,visitorallowedbelongingsDTO.iD);
		}
		ps.setObject(index++,visitorallowedbelongingsDTO.visitorPassRequestId);
		ps.setObject(index++,visitorallowedbelongingsDTO.visitorBelongingCat);
		ps.setObject(index++,visitorallowedbelongingsDTO.totalCount);
		ps.setObject(index++,visitorallowedbelongingsDTO.remarks);
		ps.setObject(index++,visitorallowedbelongingsDTO.insertedByUserId);
		ps.setObject(index++,visitorallowedbelongingsDTO.insertionDate);
		ps.setObject(index++,visitorallowedbelongingsDTO.modifiedBy);
		if(isInsert)
		{
			ps.setObject(index++, 0);
			ps.setObject(index++, lastModificationTime);
		}
	}
	
	public void get(VisitorAllowedBelongingsDTO visitorallowedbelongingsDTO, ResultSet rs) throws SQLException
	{
		visitorallowedbelongingsDTO.iD = rs.getLong("ID");
		visitorallowedbelongingsDTO.visitorPassRequestId = rs.getLong("visitor_pass_request_id");
		visitorallowedbelongingsDTO.visitorBelongingCat = rs.getInt("visitor_belonging_cat");
		visitorallowedbelongingsDTO.totalCount = rs.getString("total_count");
		visitorallowedbelongingsDTO.remarks = rs.getString("remarks");
		visitorallowedbelongingsDTO.insertedByUserId = rs.getLong("inserted_by_user_id");
		visitorallowedbelongingsDTO.insertionDate = rs.getLong("insertion_date");
		visitorallowedbelongingsDTO.modifiedBy = rs.getString("modified_by");
		visitorallowedbelongingsDTO.isDeleted = rs.getInt("isDeleted");
		visitorallowedbelongingsDTO.lastModificationTime = rs.getLong("lastModificationTime");
	}
	
	
	
	public long add(CommonDTO commonDTO) throws Exception
	{
		
		VisitorAllowedBelongingsDTO visitorallowedbelongingsDTO = (VisitorAllowedBelongingsDTO)commonDTO;
		
		Connection connection = null;
		PreparedStatement ps = null;

		long lastModificationTime = System.currentTimeMillis();	

		try{
			connection = DBMW.getInstance().getConnection();
			
			if(connection == null)
			{
				System.out.println("nullconn");
			}

			visitorallowedbelongingsDTO.iD = DBMW.getInstance().getNextSequenceId(tableName);

			String sql = "INSERT INTO " + tableName;
			
			sql += " (";
			sql += "ID";
			sql += ", ";
			sql += "visitor_pass_request_id";
			sql += ", ";
			sql += "visitor_belonging_cat";
			sql += ", ";
			sql += "total_count";
			sql += ", ";
			sql += "remarks";
			sql += ", ";
			sql += "inserted_by_user_id";
			sql += ", ";
			sql += "insertion_date";
			sql += ", ";
			sql += "modified_by";
			sql += ", ";
			sql += "isDeleted";
			sql += ", ";
			sql += "lastModificationTime";
		
			sql += ")";
			
			
            sql += " VALUES(";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			
			sql += ")";
				
			ps = connection.prepareStatement(sql);
			set(ps, visitorallowedbelongingsDTO, true);
			ps.execute();
			
			
			recordUpdateTime(connection, lastModificationTime);

		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if (ps != null) {
					ps.close();
				}
			} catch(Exception e){}
			try{
				if(connection != null)
				{
					DBMW.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		return visitorallowedbelongingsDTO.iD;		
	}
		
	
	public void deleteVisitorAllowedBelongingsByVisitorPassRequestID(long visitorPassRequestID) throws Exception{
		
		
		Connection connection = null;
		Statement stmt = null;
		try{
			
			//String sql = "UPDATE visitor_allowed_belongings SET isDeleted=0 WHERE visitor_pass_request_id="+visitorPassRequestID;
			String sql = "delete from visitor_allowed_belongings WHERE visitor_pass_request_id=" + visitorPassRequestID;
			logger.debug("sql " + sql);
			connection = DBMW.getInstance().getConnection();
			stmt = connection.createStatement();
			stmt.execute(sql);
		}catch(Exception ex){
			logger.debug("fatal",ex);
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DBMW.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
	}		
   
	public List<VisitorAllowedBelongingsDTO> getVisitorAllowedBelongingsDTOListByVisitorPassRequestID(long visitorPassRequestID) throws Exception{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		VisitorAllowedBelongingsDTO visitorallowedbelongingsDTO = null;
		List<VisitorAllowedBelongingsDTO> visitorallowedbelongingsDTOList = new ArrayList<>();
		
		try{
			
			String sql = "SELECT * FROM visitor_allowed_belongings where isDeleted=0 and visitor_pass_request_id="+visitorPassRequestID+" order by visitor_allowed_belongings.lastModificationTime";
			
			logger.debug("sql " + sql);
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();
			rs = stmt.executeQuery(sql);

			while(rs.next()){
				visitorallowedbelongingsDTO = new VisitorAllowedBelongingsDTO();
				get(visitorallowedbelongingsDTO, rs);
				visitorallowedbelongingsDTOList.add(visitorallowedbelongingsDTO);

			}			
			
		}catch(Exception ex){
			logger.debug("fatal",ex);
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return visitorallowedbelongingsDTOList;
	}

	//need another getter for repository
	public CommonDTO getDTOByID (long ID) throws Exception
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		VisitorAllowedBelongingsDTO visitorallowedbelongingsDTO = null;
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
			
            sql += " WHERE ID=" + ID;
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			if(rs.next()){
				visitorallowedbelongingsDTO = new VisitorAllowedBelongingsDTO();

				get(visitorallowedbelongingsDTO, rs);

			}			
			
			
			
			VisitorDetailsDAO visitorDetailsDAO = new VisitorDetailsDAO("visitor_details");			
			List<VisitorDetailsDTO> visitorDetailsDTOList = visitorDetailsDAO.getVisitorDetailsDTOListByVisitorPassRequestID(visitorallowedbelongingsDTO.iD);
			visitorallowedbelongingsDTO.visitorDetailsDTOList = visitorDetailsDTOList;
			
			VisitorAllowedBelongingsDAO visitorAllowedBelongingsDAO = new VisitorAllowedBelongingsDAO("visitor_allowed_belongings");			
			List<VisitorAllowedBelongingsDTO> visitorAllowedBelongingsDTOList = visitorAllowedBelongingsDAO.getVisitorAllowedBelongingsDTOListByVisitorPassRequestID(visitorallowedbelongingsDTO.iD);
			visitorallowedbelongingsDTO.visitorAllowedBelongingsDTOList = visitorAllowedBelongingsDTOList;
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return visitorallowedbelongingsDTO;
	}
	
	public long update(CommonDTO commonDTO) throws Exception
	{		
		VisitorAllowedBelongingsDTO visitorallowedbelongingsDTO = (VisitorAllowedBelongingsDTO)commonDTO;
		
		Connection connection = null;
		PreparedStatement ps = null;

		long lastModificationTime = System.currentTimeMillis();	
		try{
			connection = DBMW.getInstance().getConnection();

			String sql = "UPDATE " + tableName;
			
			sql += " SET ";
			sql += "visitor_pass_request_id=?";
			sql += ", ";
			sql += "visitor_belonging_cat=?";
			sql += ", ";
			sql += "total_count=?";
			sql += ", ";
			sql += "remarks=?";
			sql += ", ";
			sql += "inserted_by_user_id=?";
			sql += ", ";
			sql += "insertion_date=?";
			sql += ", ";
			sql += "modified_by=?";
			sql += ", lastModificationTime = "	+ lastModificationTime + "";
            sql += " WHERE ID = " + visitorallowedbelongingsDTO.iD;
				

			ps = connection.prepareStatement(sql);
			set(ps, visitorallowedbelongingsDTO, false);
			ps.executeUpdate();
			

			recordUpdateTime(connection, lastModificationTime);

						
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if (ps != null) {
					ps.close();
				}
			} catch(Exception e){}
			try{
				if(connection != null)
				{
					DBMW.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		return visitorallowedbelongingsDTO.iD;
	}
	
	
	public List<VisitorAllowedBelongingsDTO> getDTOs(Collection recordIDs){
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		VisitorAllowedBelongingsDTO visitorallowedbelongingsDTO = null;
		List<VisitorAllowedBelongingsDTO> visitorallowedbelongingsDTOList = new ArrayList<>();
		if(recordIDs.isEmpty()){
			return visitorallowedbelongingsDTOList;
		}
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
            
            sql += " WHERE ID IN ( ";

			for(int i = 0;i<recordIDs.size();i++){
				if(i!=0){
					sql+=",";
				}
				sql+=((ArrayList)recordIDs).get(i);
			}
			sql+=")  order by lastModificationTime desc";
			
			printSql(sql);
			
			logger.debug("sql " + sql);
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				visitorallowedbelongingsDTO = new VisitorAllowedBelongingsDTO();
				get(visitorallowedbelongingsDTO, rs);
				System.out.println("got this DTO: " + visitorallowedbelongingsDTO);
				
				visitorallowedbelongingsDTOList.add(visitorallowedbelongingsDTO);

			}			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return visitorallowedbelongingsDTOList;
	
	}
	
	

	
	
	
	//add repository
	public List<VisitorAllowedBelongingsDTO> getAllVisitorAllowedBelongings (boolean isFirstReload)
    {
		List<VisitorAllowedBelongingsDTO> visitorallowedbelongingsDTOList = new ArrayList<>();

		String sql = "SELECT * FROM visitor_allowed_belongings";
		sql += " WHERE ";
	

		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by visitorallowedbelongings.lastModificationTime desc";
		printSql(sql);
		
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;

		
		try{
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();
			rs = stmt.executeQuery(sql);
			

			while(rs.next()){
				VisitorAllowedBelongingsDTO visitorallowedbelongingsDTO = new VisitorAllowedBelongingsDTO();
				get(visitorallowedbelongingsDTO, rs);
				
				visitorallowedbelongingsDTOList.add(visitorallowedbelongingsDTO);
			}			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}

		return visitorallowedbelongingsDTOList;
    }

	
	public List<VisitorAllowedBelongingsDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO)
	{
		return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
	}

	
	public List<VisitorAllowedBelongingsDTO> getDTOs(Hashtable  p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
			String filter, boolean tableHasJobCat)
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		List<VisitorAllowedBelongingsDTO> visitorallowedbelongingsDTOList = new ArrayList<>();

		try{
			
			String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat);
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				VisitorAllowedBelongingsDTO visitorallowedbelongingsDTO = new VisitorAllowedBelongingsDTO();
				get(visitorallowedbelongingsDTO, rs);
				
				visitorallowedbelongingsDTOList.add(visitorallowedbelongingsDTO);
			}					
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return visitorallowedbelongingsDTOList;
	
	}
				
}
	