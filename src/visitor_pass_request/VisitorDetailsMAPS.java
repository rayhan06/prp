package visitor_pass_request;
import java.util.*; 
import util.*;


public class VisitorDetailsMAPS extends CommonMaps
{	
	public VisitorDetailsMAPS(String tableName)
	{
		
		java_allfield_type_map.put("visitor_name".toLowerCase(), "String");
		java_allfield_type_map.put("vistor_father_name".toLowerCase(), "String");
		java_allfield_type_map.put("identification_cat".toLowerCase(), "Integer");
		java_allfield_type_map.put("identification_number".toLowerCase(), "String");
		java_allfield_type_map.put("contact_number_1".toLowerCase(), "String");


		java_anyfield_search_map.put(tableName + ".visitor_pass_request_id".toLowerCase(), "Long");
		java_anyfield_search_map.put(tableName + ".visitor_name".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".vistor_father_name".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".identification_number".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".contact_number_1".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".contact_number_2".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".visitor_address".toLowerCase(), "String");

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("visitorPassRequestId".toLowerCase(), "visitorPassRequestId".toLowerCase());
		java_DTO_map.put("visitorName".toLowerCase(), "visitorName".toLowerCase());
		java_DTO_map.put("vistorFatherName".toLowerCase(), "vistorFatherName".toLowerCase());
		java_DTO_map.put("identificationCat".toLowerCase(), "identificationCat".toLowerCase());
		java_DTO_map.put("identificationNumber".toLowerCase(), "identificationNumber".toLowerCase());
		java_DTO_map.put("contactNumber1".toLowerCase(), "contactNumber1".toLowerCase());
		java_DTO_map.put("contactNumber2".toLowerCase(), "contactNumber2".toLowerCase());
		java_DTO_map.put("visitorAddress".toLowerCase(), "visitorAddress".toLowerCase());
		java_DTO_map.put("filesDropzone".toLowerCase(), "filesDropzone".toLowerCase());
		java_DTO_map.put("insertedByUserId".toLowerCase(), "insertedByUserId".toLowerCase());
		java_DTO_map.put("insertionDate".toLowerCase(), "insertionDate".toLowerCase());
		java_DTO_map.put("modifiedBy".toLowerCase(), "modifiedBy".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("visitor_pass_request_id".toLowerCase(), "visitorPassRequestId".toLowerCase());
		java_SQL_map.put("visitor_name".toLowerCase(), "visitorName".toLowerCase());
		java_SQL_map.put("vistor_father_name".toLowerCase(), "vistorFatherName".toLowerCase());
		java_SQL_map.put("identification_cat".toLowerCase(), "identificationCat".toLowerCase());
		java_SQL_map.put("identification_number".toLowerCase(), "identificationNumber".toLowerCase());
		java_SQL_map.put("contact_number_1".toLowerCase(), "contactNumber1".toLowerCase());
		java_SQL_map.put("contact_number_2".toLowerCase(), "contactNumber2".toLowerCase());
		java_SQL_map.put("visitor_address".toLowerCase(), "visitorAddress".toLowerCase());
		java_SQL_map.put("files_dropzone".toLowerCase(), "filesDropzone".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Visitor Pass Request Id".toLowerCase(), "visitorPassRequestId".toLowerCase());
		java_Text_map.put("Visitor Name".toLowerCase(), "visitorName".toLowerCase());
		java_Text_map.put("Vistor Father Name".toLowerCase(), "vistorFatherName".toLowerCase());
		java_Text_map.put("Identification Cat".toLowerCase(), "identificationCat".toLowerCase());
		java_Text_map.put("Identification Number".toLowerCase(), "identificationNumber".toLowerCase());
		java_Text_map.put("Contact Number 1".toLowerCase(), "contactNumber1".toLowerCase());
		java_Text_map.put("Contact Number 2".toLowerCase(), "contactNumber2".toLowerCase());
		java_Text_map.put("Visitor Address".toLowerCase(), "visitorAddress".toLowerCase());
		java_Text_map.put("Files Dropzone".toLowerCase(), "filesDropzone".toLowerCase());
		java_Text_map.put("Inserted By User Id".toLowerCase(), "insertedByUserId".toLowerCase());
		java_Text_map.put("Insertion Date".toLowerCase(), "insertionDate".toLowerCase());
		java_Text_map.put("Modified By".toLowerCase(), "modifiedBy".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}