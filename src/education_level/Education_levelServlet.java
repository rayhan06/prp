package education_level;

import common.*;
import language.LC;
import language.LM;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import pb.Utils;
import permission.MenuConstants;
import user.UserDTO;
import util.CommonConstant;
import util.CommonDTO;
import util.HttpRequestUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;


@WebServlet("/Education_levelServlet")
@MultipartConfig
public class Education_levelServlet extends BaseServlet implements NameInterface{
    private static final long serialVersionUID = 1L;

    public static final Logger logger = Logger.getLogger(Education_levelServlet.class);

    private final Education_levelDAO education_levelDAO = Education_levelDAO.getInstance();

    private final DegreeExamDAO degreeExamDAO = DegreeExamDAO.getInstance();

    @Override
    public String getTableName() {
        return education_levelDAO.getTableName();
    }

    @Override
    public String getServletName() {
        return "Education_levelServlet";
    }

    @Override
    public NameDao getCommonDAOService() {
        return education_levelDAO;
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[] {MenuConstants.EDUCATION_LEVEL_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[] {MenuConstants.EDUCATION_LEVEL_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[] {MenuConstants.EDUCATION_LEVEL_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Education_levelServlet.class;
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if("delete".equals(request.getParameter("actionType"))){
            deleteEducation_level(request);
            response.sendRedirect("Education_levelServlet?actionType=search");
        }else{
            super.doPost(request,response);
        }
    }

    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        boolean isLangEng = LM.getLanguageIDByUserDTO(userDTO) == CommonConstant.Language_ID_English;
        NameDTO nameDTO;
        if (addFlag) {
            nameDTO = new NameDTO();
            nameDTO.insertedBy = nameDTO.modifiedBy = userDTO.employee_record_id;
            nameDTO.insertionDate = nameDTO.lastModificationTime = System.currentTimeMillis();
        } else {
            nameDTO = education_levelDAO.getDTOFromID(Long.parseLong(request.getParameter("id")));
            if(nameDTO == null){
                throw new Exception(isLangEng ?"Education level information is not found": "শিক্ষাস্তরের তথ্য পাওয়া যায়নি");
            }
            nameDTO.modifiedBy = userDTO.employee_record_id;
            nameDTO.lastModificationTime = System.currentTimeMillis();
        }
        nameDTO.nameBn = Jsoup.clean(request.getParameter("nameBn"), Whitelist.simpleText());
        if(nameDTO.nameBn == null || nameDTO.nameBn.trim().length() == 0){
            throw new Exception(isLangEng ? "Bangla name for education level is missing": "শিক্ষাস্তরের বাংলা নাম পাওয়া যায়নি");
        }
        nameDTO.nameEn = Jsoup.clean(request.getParameter("nameEn"), Whitelist.simpleText());
        if(nameDTO.nameEn == null || nameDTO.nameEn.trim().length() == 0){
            throw new Exception(isLangEng ? "English name for education level is missing" : "শিক্ষাস্তরের ইংরেজীর নাম পাওয়া যায়নি");
        }
        Utils.handleTransaction(()->{
            List<DegreeExamDTO> degreeExamDTOList = createDegreeExamDTOListByRequest(request,isLangEng);
            if (addFlag) {
                education_levelDAO.add(nameDTO);
            } else {
                List<DegreeExamDTO> oldDegreeExamDTOList = DegreeExamRepository.getInstance().getByEducationLevelId(nameDTO.iD);
                if (request.getParameterValues("degreeExam.iD") != null){
                    Set<Long>ids = Stream.of(request.getParameterValues("degreeExam.iD"))
                            .map(Long::parseLong)
                            .filter(e->e>0)
                            .collect(Collectors.toSet());
                    List<DegreeExamDTO> deletedList = oldDegreeExamDTOList.stream()
                            .filter(e->!ids.contains(e.iD))
                            .peek(dto->dto.isDeleted=1)
                            .collect(Collectors.toList());
                    if(deletedList.size()>0){
                        if(degreeExamDTOList == null){
                            degreeExamDTOList = deletedList;
                        }else{
                            degreeExamDTOList.addAll(deletedList);
                        }
                    }
                }
                education_levelDAO.update(nameDTO);
            }
            if (degreeExamDTOList != null) {
                for (DegreeExamDTO degreeExamDTO : degreeExamDTOList) {
                    degreeExamDTO.educationLevelId = nameDTO.iD;
                    if(degreeExamDTO.iD == -1){
                        degreeExamDAO.add(degreeExamDTO);
                    }else{
                        degreeExamDAO.update(degreeExamDTO);
                    }
                }
            }
        });
        return nameDTO;
    }

    private List<DegreeExamDTO> createDegreeExamDTOListByRequest(HttpServletRequest request,boolean isLangEng) throws Exception {
        List<DegreeExamDTO> degreeExamDTOList = new ArrayList<>();
        if (request.getParameterValues("degreeExam.iD") != null) {
            int degreeExamItemNo = request.getParameterValues("degreeExam.iD").length;
            for (int index = 0; index < degreeExamItemNo; index++) {
                DegreeExamDTO degreeExamDTO = createDegreeExamDTOByRequestAndIndex(request, index,isLangEng);
                if(degreeExamDTO!=null){
                    degreeExamDTOList.add(degreeExamDTO);
                }
            }
            return degreeExamDTOList;
        }
        return null;
    }

    private DegreeExamDTO createDegreeExamDTOByRequestAndIndex(HttpServletRequest request, int index,boolean isLangEng) throws Exception {
        DegreeExamDTO degreeExamDTO;
        long id = Long.parseLong(request.getParameterValues("degreeExam.iD")[index]);
        String nameBn = Jsoup.clean(request.getParameterValues("degreeExam.nameBn")[index], Whitelist.simpleText());
        if(nameBn == null || nameBn.trim().length() == 0){
            throw new Exception(isLangEng ? "Bangla name for degree exam is missing" : "ডিগ্রী পরীক্ষার বাংলা নাম পাওয়া যায়নি");
        }
        String nameEn = Jsoup.clean(request.getParameterValues("degreeExam.nameEn")[index], Whitelist.simpleText());
        if(nameEn == null || nameEn.trim().length() == 0){
            throw new Exception(isLangEng ? "English name for degree exam is missing" : "ডিগ্রী পরীক্ষার ইংরেজীর নাম পাওয়া যায়নি");
        }
        if(id == 0){
            degreeExamDTO = new DegreeExamDTO();
        }else{
            degreeExamDTO = DegreeExamRepository.getInstance().getById(id);
            if(degreeExamDTO == null){
                throw new Exception(isLangEng ? "Degree exam information is not found" : "ডিগ্রী পরীক্ষার তথ্য পাওয়া যায়নি");
            }
            if(degreeExamDTO.nameBn.equals(nameBn) && degreeExamDTO.nameEn.equals(nameEn)){
                return null;
            }
        }
        degreeExamDTO.nameBn = nameBn;
        degreeExamDTO.nameEn = nameEn;
        return degreeExamDTO;
    }

    private void deleteEducation_level(HttpServletRequest request) {
        UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;
        try {
            String[] IDsToDelete = request.getParameterValues("ID");
            for (String s : IDsToDelete) {
                long id = Long.parseLong(s);
                logger.debug("------ DELETING " + s);
                education_levelDAO.delete(userDTO.employee_record_id, Collections.singletonList(id));
                degreeExamDAO.deleteDegreeExamByEducationLevelID(id);
            }
        } catch (Exception ex) {
            logger.error(ex);
        }
    }

    @Override
    public int getSearchTitleValue() {
        return 0;
    }

    @Override
    public String get_p_navigatorName() {
        return null;
    }

    @Override
    public String get_p_dtoCollectionName() {
        return null;
    }

    @Override
    public NameRepository getNameRepository() {
        return Education_levelRepository.getInstance();
    }

    @Override
    public int getAddTitleValue() {
        return LC.EDUCATION_LEVEL_ADD_EDUCATION_LEVEL_ADD_FORMNAME;
    }

    @Override
    public int getEditTitleValue() {
        return LC.EDUCATION_LEVEL_EDIT_EDUCATION_LEVEL_EDIT_FORMNAME;
    }
}