package education_level;

import org.apache.log4j.Logger;
import pb.OptionDTO;
import pb.Utils;
import repository.Repository;
import repository.RepositoryManager;
import common.TextInterface;
import util.LockManager;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

@SuppressWarnings("Duplicates")
public class DegreeExamRepository implements Repository, TextInterface {

    private final static Logger logger = Logger.getLogger(DegreeExamRepository.class);

    private final Map<Long, Map<Long, DegreeExamDTO>> mapByEducationLevelIdAndId = new ConcurrentHashMap<>();

    private final Map<Long, List<DegreeExamDTO>> mapByEducationLevel = new ConcurrentHashMap<>();

    private final Map<Long, DegreeExamDTO> mapById = new ConcurrentHashMap<>();

    private final DegreeExamDAO degreeExamDAO = DegreeExamDAO.getInstance();

    @Override
    public String getTableName() {
        return degreeExamDAO.getTableName();
    }

    private DegreeExamRepository() {
        RepositoryManager.getInstance().addRepository(this);
    }

    public static DegreeExamRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

    @Override
    public String getText(String language, long id) {
        DegreeExamDTO dto = mapById.get(id);
        if(dto == null){
            return "";
        }
        return "English".equalsIgnoreCase(language)?dto.nameEn:dto.nameBn;
    }

    private static class LazyLoader {
        static final DegreeExamRepository INSTANCE = new DegreeExamRepository();
    }

    @Override
    public void reload(boolean reloadAll) {
        logger.debug("reload is started for reloadAll = " + reloadAll);
        List<DegreeExamDTO> list = degreeExamDAO.getAllDTOs(reloadAll);
        if (list.size() > 0) {
            Set<Long> educationLevelIdSet;
            synchronized (this) {
                educationLevelIdSet = list.stream()
                        .peek(this::removeFromMap)
                        .filter(dto -> dto.isDeleted == 0)
                        .peek(this::addToMap)
                        .map(dto -> dto.educationLevelId)
                        .collect(Collectors.toSet());
                educationLevelIdSet.forEach(id-> mapByEducationLevel.get(id).sort(Comparator.comparingLong(o -> o.iD)));
            }
        }
        logger.debug("reload has been done for reloadAll = " + reloadAll);
    }

    private void addToMap(DegreeExamDTO dto) {
        if (dto != null) {
            Map<Long, DegreeExamDTO> map = mapByEducationLevelIdAndId.getOrDefault(dto.educationLevelId, new ConcurrentHashMap<>());
            map.put(dto.iD, dto);
            mapByEducationLevelIdAndId.put(dto.educationLevelId, map);
            List<DegreeExamDTO> list = mapByEducationLevel.getOrDefault(dto.educationLevelId, new ArrayList<>());
            list.add(dto);
            mapByEducationLevel.put(dto.educationLevelId, list);
            mapById.put(dto.iD,dto);
        }
    }

    private void removeFromMap(DegreeExamDTO dto) {
        Map<Long, DegreeExamDTO> map = mapByEducationLevelIdAndId.get(dto.educationLevelId);
        if (map != null) {
            if (map.get(dto.iD) != null) {
                DegreeExamDTO oldDTO = map.get(dto.iD);
                map.remove(dto.iD);
                mapByEducationLevelIdAndId.put(dto.educationLevelId, map);
                mapByEducationLevel.get(dto.educationLevelId).remove(oldDTO);
                mapById.remove(dto.iD);
            }
        }
    }

    public DegreeExamDTO getByIdAndEducationLevelId(long id, long educationLevelId) {
        if (mapByEducationLevelIdAndId.get(educationLevelId) == null || mapByEducationLevelIdAndId.get(educationLevelId).get(id) == null) {
            synchronized (LockManager.getLock(id + "DEP")) {
                if (mapByEducationLevelIdAndId.get(educationLevelId) == null || mapByEducationLevelIdAndId.get(educationLevelId).get(id) == null) {
                    DegreeExamDTO dto = degreeExamDAO.getByIdAndEducationLevelId(id, educationLevelId);
                    if(dto!=null){
                        Map<Long, DegreeExamDTO> map = mapByEducationLevelIdAndId.getOrDefault(educationLevelId, new ConcurrentHashMap<>());
                        map.put(id, dto);
                        mapByEducationLevelIdAndId.put(educationLevelId, map);
                    }
                }
            }
        }
        return mapByEducationLevelIdAndId.get(educationLevelId).get(id);
    }

    public List<DegreeExamDTO> getByEducationLevelId(long educationLevelId) {
        Map<Long, DegreeExamDTO> map = mapByEducationLevelIdAndId.get(educationLevelId);
        if (map != null) {
            return new ArrayList<>(map.values());
        }
        try {
            List<DegreeExamDTO> list = degreeExamDAO.getDegreeExamDTOListByEducationLevelID(educationLevelId);
            if (list.size() > 0) {
                map = list.stream()
                        .collect(Collectors.toMap(e -> e.iD, e -> e));
                mapByEducationLevelIdAndId.put(educationLevelId, map);
                return list;
            }
        } catch (Exception e) {
            logger.error(e);
        }
        return new ArrayList<>();
    }

    public String getText(String language, long id, long educationLevelId) {
        DegreeExamDTO dto = getByIdAndEducationLevelId(id, educationLevelId);
        return dto == null ? "" : ("Bangla".equalsIgnoreCase(language) ? dto.nameBn : dto.nameEn);
    }

    public String buildOption(String language, long educationLevel, Long selectedDegree) {
        List<DegreeExamDTO> list = mapByEducationLevel.get(educationLevel);
        List<OptionDTO>optionDTOList = new ArrayList<>();
        if(list != null && list.size()>0){
            optionDTOList = list.stream()
                    .map(dto->new OptionDTO(dto.nameEn,dto.nameBn,String.valueOf(dto.iD)))
                    .collect(Collectors.toList());
        }
        return Utils.buildOptions(optionDTOList,language,selectedDegree == null ? null : String.valueOf(selectedDegree));
    }

    public DegreeExamDTO getById(long id){
        if(mapById.get(id) == null){
            synchronized (LockManager.getLock(id + "DEDR")){
                DegreeExamDTO dto = degreeExamDAO.getDTOFromID(id);
                if(dto!=null){
                    addToMap(dto);
                    List<DegreeExamDTO> list = mapByEducationLevel.get(dto.educationLevelId);
                    list.sort(Comparator.comparingLong(o->o.iD));
                    mapByEducationLevel.put(dto.educationLevelId,list);
                }
            }
        }
        return mapById.get(id);
    }
}
