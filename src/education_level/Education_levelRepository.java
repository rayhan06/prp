package education_level;

import common.NameRepository;

public class Education_levelRepository extends NameRepository {
    private Education_levelRepository() {
        super(Education_levelDAO.getInstance(),Education_levelRepository.class);
    }

    private static class Education_levelRepositoryLoader {
        static Education_levelRepository INSTANCE = new Education_levelRepository();
    }

    public static Education_levelRepository getInstance() {
        return Education_levelRepositoryLoader.INSTANCE;
    }
}
