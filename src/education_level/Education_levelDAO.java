package education_level;

import common.NameDao;

public class Education_levelDAO extends NameDao {

	private Education_levelDAO() {
		super("education_level");
	}

	private static class LazyLoader{
		static final Education_levelDAO INSTANCE = new Education_levelDAO();
	}

	public static Education_levelDAO getInstance(){
		return LazyLoader.INSTANCE;
	}
}