package education_level;

import util.CommonDTO;


public class DegreeExamDTO extends CommonDTO {

    public String nameBn = "";
    public String nameEn = "";
    public long educationLevelId = 0;

    @Override
    public String toString() {
        return "$DegreeExamDTO[" +
                " iD = " + iD +
                " nameBn = " + nameBn +
                " nameEn = " + nameEn +
                " educationLevelId = " + educationLevelId +
                " isDeleted = " + isDeleted +
                " lastModificationTime = " + lastModificationTime +
                "]";
    }

}