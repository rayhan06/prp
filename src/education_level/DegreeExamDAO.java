package education_level;

import common.CommonDAOService;
import common.ConnectionAndStatementUtil;
import org.apache.log4j.Logger;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

public class DegreeExamDAO implements CommonDAOService<DegreeExamDTO> {

	private static final Logger logger = Logger.getLogger(DegreeExamDAO.class);
	
	private static final String addSqlQuery = "INSERT INTO {tableName} (name_bn,name_en,education_level_id,lastModificationTime,isDeleted,ID) VALUES(?,?,?,?,?,?)";

	private static final String updateSqlQuery = "UPDATE {tableName} SET name_bn=?,name_en=?,education_level_id=?,lastModificationTime =?,isDeleted=? WHERE ID = ?";

	private static final String getByIdAndEducationLevelIdSqlQuery = "SELECT * FROM degree_exam WHERE id = ? and education_level_id = ?";

	private static final String deleteByEducationLevelId = "UPDATE degree_exam SET isDeleted = 1,lastModificationTime = ? WHERE education_level_id= ?";

	private static final String getByEducationLevelIdSqlQuery = "SELECT * FROM degree_exam where isDeleted=0 and education_level_id= ? order by degree_exam.id";

	private DegreeExamDAO(){}

	private static class LazyLoader{
		static final DegreeExamDAO INSTANCE = new DegreeExamDAO();
	}

	public static DegreeExamDAO getInstance() {
		return LazyLoader.INSTANCE;
	}

	@Override
	public void set(PreparedStatement ps, DegreeExamDTO degreeexamDTO, boolean isInsert) throws SQLException {
		int index = 0;
		ps.setObject(++index, degreeexamDTO.nameBn);
		ps.setObject(++index, degreeexamDTO.nameEn);
		ps.setObject(++index, degreeexamDTO.educationLevelId);
		ps.setObject(++index, System.currentTimeMillis());
		if (isInsert) {
			ps.setObject(++index, 0);
		}else{
			ps.setObject(++index, degreeexamDTO.isDeleted);
		}
		ps.setObject(++index, degreeexamDTO.iD);
	}

	@Override
	public DegreeExamDTO buildObjectFromResultSet(ResultSet rs){
		try{
			DegreeExamDTO degreeexamDTO = new DegreeExamDTO();
			degreeexamDTO.iD = rs.getLong("ID");
			degreeexamDTO.nameBn = rs.getString("name_bn");
			degreeexamDTO.nameEn = rs.getString("name_en");
			degreeexamDTO.educationLevelId = rs.getLong("education_level_id");
			degreeexamDTO.isDeleted = rs.getInt("isDeleted");
			degreeexamDTO.lastModificationTime = rs.getLong("lastModificationTime");
			return degreeexamDTO;
		}catch (SQLException ex){
			logger.error(ex);
			return null;
		}
	}

	@Override
	public String getTableName() {
		return "degree_exam";
	}

	@Override
	public Map<String, String> getSearchMap() {
		return null;
	}

	public long add(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((DegreeExamDTO) commonDTO,addSqlQuery,true);
	}
	
	public long update(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((DegreeExamDTO) commonDTO,updateSqlQuery,false);
	}

	public void deleteDegreeExamByEducationLevelID(long educationLevelID) throws Exception {
		ConnectionAndStatementUtil.getWriteConnectionAndPrepareStatement(obj->{
			try {
				long currentTime = System.currentTimeMillis();
				PreparedStatement ps = (PreparedStatement) obj.getStatement();
				ps.setLong(1,currentTime);
				ps.setLong(2,educationLevelID);
				ps.execute();
				recordUpdateTime(obj.getConnection(),getTableName(),currentTime);
				return true;
			} catch (SQLException e) {
				logger.error(e);
				return false;
			}
		},deleteByEducationLevelId);
	}

	public List<DegreeExamDTO> getDegreeExamDTOListByEducationLevelID(long educationLevelID) {
		return getDTOs(getByEducationLevelIdSqlQuery,ps->{
			try {
				ps.setLong(1,educationLevelID);
			} catch (SQLException ex) {
				logger.error(ex);
			}
		});
	}

	public DegreeExamDTO getByIdAndEducationLevelId(long id, long educationLevelId){
		List<DegreeExamDTO> list = getDTOs(getByIdAndEducationLevelIdSqlQuery,ps->{
			try {
				ps.setLong(1,id);
				ps.setLong(2,educationLevelId);
			} catch (SQLException ex) {
				logger.error(ex);
			}
		});
		return list.size() == 0 ? null : list.get(0);
	}
}