package education_level;

import util.CommonDTO;

public class EducationLevelDTO  extends CommonDTO {
    public String nameBn = "";
    public String nameEn = "";

    @Override
    public String toString() {
        return "EducationLevelDTO{" +
                "nameBn='" + nameBn + '\'' +
                ", nameEn='" + nameEn + '\'' +
                '}';
    }


}
