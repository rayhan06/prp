package employee_nominee;

import common.ApiResponse;
import javax.servlet.http.HttpServletRequest;

public interface EmployeeNomineeApiPerformer {
    ApiResponse perform(HttpServletRequest request);
}

