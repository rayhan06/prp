package employee_nominee;

import common.ApiResponse;
import employee_records.Employee_recordsDAO;
import employee_records.Employee_recordsDTO;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public class GetNomineeByIdPerformer implements EmployeeNomineeApiPerformer {
    Logger logger = Logger.getLogger(GetNomineeByIdPerformer.class);
    @Override
    public ApiResponse perform(HttpServletRequest request) {
        long empId;

        try {
            empId = Long.parseLong(request.getParameter("empId"));
        } catch (NumberFormatException nfe) {
            nfe.printStackTrace();
            return ApiResponse.makeErrorResponse("Please, check the parameters provided!");
        }

        ApiResponse apiResponse;

        Employee_nomineeDAO employee_nomineeDAO = Employee_nomineeDAO.getInstance();
        Employee_recordsDAO employee_recordsDAO = new Employee_recordsDAO();


        try {

            Employee_recordsDTO employee_recordsDTO = employee_recordsDAO.getEmployee_recordsDTOByID(empId);

            if (employee_recordsDTO == null || employee_recordsDTO.isDeleted) {

                apiResponse = ApiResponse.makeErrorResponse("No Record found with this empId: "+empId+"!");

            } else {

                List<Employee_nomineeDTO> employee_nomineeDTOs =  employee_nomineeDAO.getByEmployeeId(empId);

                apiResponse = (employee_nomineeDTOs == null) ?
                        ApiResponse.makeErrorResponse("Something bad happened!") :
                        ApiResponse.makeSuccessResponse(employee_nomineeDTOs, "Successfully retrieved the Nominees.");
            }


        } catch (Exception e) {
            e.printStackTrace();
            apiResponse = ApiResponse.makeErrorResponse("Exception is Occurred!");
        }

        return apiResponse;
    }
}
