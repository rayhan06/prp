package employee_nominee;
import java.util.*; 
import util.*;


public class Employee_nomineeMAPS extends CommonMaps
{	
	public Employee_nomineeMAPS(String tableName)
	{
		
		java_allfield_type_map.put("employee_records_id".toLowerCase(), "Long");
		java_allfield_type_map.put("employee_family_info_id".toLowerCase(), "Long");
		java_allfield_type_map.put("nominee_cat".toLowerCase(), "Integer");
		java_allfield_type_map.put("percentage".toLowerCase(), "Double");

		java_anyfield_search_map.put("language_text.languageTextEnglish", "String");
		java_anyfield_search_map.put("language_text.languageTextBangla", "String");

		java_anyfield_search_map.put(tableName + ".employee_records_id".toLowerCase(), "Long");
		java_anyfield_search_map.put(tableName + ".employee_family_info_id".toLowerCase(), "Long");
		java_anyfield_search_map.put(tableName + ".percentage".toLowerCase(), "Double");

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("employeeRecordsId".toLowerCase(), "employeeRecordsId".toLowerCase());
		java_DTO_map.put("employeeFamilyInfoId".toLowerCase(), "employeeFamilyInfoId".toLowerCase());
		java_DTO_map.put("nomineeCat".toLowerCase(), "nomineeCat".toLowerCase());
		java_DTO_map.put("percentage".toLowerCase(), "percentage".toLowerCase());
		java_DTO_map.put("insertedByUserId".toLowerCase(), "insertedByUserId".toLowerCase());
		java_DTO_map.put("insertionDate".toLowerCase(), "insertionDate".toLowerCase());
		java_DTO_map.put("modifiedBy".toLowerCase(), "modifiedBy".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("employee_records_id".toLowerCase(), "employeeRecordsId".toLowerCase());
		java_SQL_map.put("employee_family_info_id".toLowerCase(), "employeeFamilyInfoId".toLowerCase());
		java_SQL_map.put("nominee_cat".toLowerCase(), "nomineeCat".toLowerCase());
		java_SQL_map.put("percentage".toLowerCase(), "percentage".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Employee Records Id".toLowerCase(), "employeeRecordsId".toLowerCase());
		java_Text_map.put("Employee Family Info Id".toLowerCase(), "employeeFamilyInfoId".toLowerCase());
		java_Text_map.put("Nominee Cat".toLowerCase(), "nomineeCat".toLowerCase());
		java_Text_map.put("Percentage".toLowerCase(), "percentage".toLowerCase());
		java_Text_map.put("Inserted By User Id".toLowerCase(), "insertedByUserId".toLowerCase());
		java_Text_map.put("Insertion Date".toLowerCase(), "insertionDate".toLowerCase());
		java_Text_map.put("Modified By".toLowerCase(), "modifiedBy".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}