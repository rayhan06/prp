package employee_nominee;

import common.ApiResponse;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/*
Two methods implemented so far!
    1. get nominees using empId:
        param: (a) empId, (b) nomineeId and (c) action=getNomineeById
        response: (a)responseCode ,(b)payload and (c) msg
    2. delete nominee by nomineeId:
        param: (a) empId, (b) nomineeId and (c) action=deleteNomineeById
        response: (a)responseCode ,(b)payload and (c) msg
 */

@WebServlet("/employee-nominee-api")
public class EmployeeNomineeApiServlet extends HttpServlet {
    private static final long serialVersionUID = -6541291713378918868L;

    private static final Logger logger = Logger.getLogger(employee_nominee.EmployeeNomineeApiServlet.class);

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ApiResponse api_response;
        String actionType = request.getParameter("action");
        logger.debug("Employee Records API actionType = " + actionType);

        EmployeeNomineeApiPerformer performer = EmployeeNomineeApiPerformerFactory.getPerformer(actionType);

        if(performer == null) {
            api_response = ApiResponse.makeErrorResponse("Action type is invalid!");
        }else {
            api_response = performer.perform(request);
        }
        response.getWriter().write(api_response.getJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    }

}