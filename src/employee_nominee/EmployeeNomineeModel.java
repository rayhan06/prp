package employee_nominee;

public class EmployeeNomineeModel {
    public Employee_nomineeDTO dto;
    public String nomineeCategoryEng;
    public String nomineeCategoryBan;
    public String percentageEng;
    public String percentageBan;
    public String firstNameEng;
    public String firstNameBan;
    public String relativeTypeEng;
    public String relativeTypeBan;
    public String mobileBan;
    public String mobileEng;
}
