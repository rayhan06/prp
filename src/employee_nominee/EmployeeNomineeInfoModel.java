package employee_nominee;

import employee_honors_awards.Employee_honors_awardsDTO;
import util.CommonDTO;

public class EmployeeNomineeInfoModel extends CommonDTO {
    public Employee_nomineeDTO dto;
    public String firstNameEn = "";
    public String lastNameEn= "";
    public String firstNameBn = "";
    public String lastNameBn= "";
    public int relationType= 0;
    public int professionType= 0;
    public String mobileNumber= "";
    public String email= "";


    @Override
    public String toString() {
        return "EmployeeNomineeInfoModel[" +
                " firstName = " + firstNameEn +
                " lastName = " + lastNameEn +
                " firstName = " + firstNameBn +
                " lastName = " + lastNameBn +
                " relationWithEmployee = " + relationType +
                " profession = " + professionType +
                " mobileNumber = " + mobileNumber +
                " email = " + email +
                "]";
    }

}