package employee_nominee;

import common.ApiResponse;
import employee_records.Employee_recordsDAO;
import employee_records.Employee_recordsDTO;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public class DeleteNomineePerformer implements EmployeeNomineeApiPerformer {
    Logger logger = Logger.getLogger(DeleteNomineePerformer.class);

    @Override
    public ApiResponse perform(HttpServletRequest request) {
        long empId, nomineeId;

        try {
            empId = Long.parseLong(request.getParameter("empId"));
            nomineeId = Long.parseLong(request.getParameter("nomineeId"));
        } catch (NumberFormatException nfe) {
            nfe.printStackTrace();
            return ApiResponse.makeErrorResponse("Please, check the parameters provided!");
        }

        ApiResponse apiResponse;

        Employee_nomineeDAO employee_nomineeDAO = Employee_nomineeDAO.getInstance();
        Employee_recordsDAO employee_recordsDAO = new Employee_recordsDAO();


        try {

            Employee_recordsDTO employee_recordsDTO = employee_recordsDAO.getEmployee_recordsDTOByID(empId);

            if (employee_recordsDTO == null || employee_recordsDTO.isDeleted) {

                apiResponse = ApiResponse.makeErrorResponse("No Record found with this empId: "+empId+"!");

            } else {

                Employee_nomineeDTO employee_nomineeDTO = (Employee_nomineeDTO) employee_nomineeDAO.getDTOByID(nomineeId);

                if (employee_nomineeDTO == null) {
                    apiResponse = ApiResponse.makeErrorResponse("No nominee found this this nomineeId: "+nomineeId+"!");
                } else {
                    employee_nomineeDAO.deleteNomineeByID(nomineeId);
                    apiResponse = ApiResponse.makeSuccessResponse("Successfully deleted the Employee.");
                }
            }


        } catch (Exception e) {
            e.printStackTrace();
            apiResponse = ApiResponse.makeErrorResponse("Exception is Occurred!");
        }

        return apiResponse;
    }
}
