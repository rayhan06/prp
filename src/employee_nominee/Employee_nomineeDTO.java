package employee_nominee;

import util.CommonEmployeeDTO;

public class Employee_nomineeDTO extends CommonEmployeeDTO {

    public long employeeFamilyInfoId = 0;
    public int nomineeCat = 0;
    public double percentage = 0;
    public long insertedByUserId = 0;
    public long insertionDate = 0;
    public String modifiedBy = "";


    @Override
    public String toString() {
        return "$Employee_nomineeDTO[" +
                " iD = " + iD +
                " employeeRecordsId = " + employeeRecordsId +
                " employeeFamilyInfoId = " + employeeFamilyInfoId +
                " nomineeCat = " + nomineeCat +
                " percentage = " + percentage +
                " insertedByUserId = " + insertedByUserId +
                " insertionDate = " + insertionDate +
                " modifiedBy = " + modifiedBy +
                " isDeleted = " + isDeleted +
                " lastModificationTime = " + lastModificationTime +
                "]";
    }

}