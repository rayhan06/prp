package employee_nominee;


import common.BaseServlet;
import common.EmployeeServletService;
import employee_records.Employee_recordsRepository;
import user.UserDTO;
import util.CommonDTO;
import util.HttpRequestUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@SuppressWarnings({"Duplicates", "unused"})
@WebServlet("/Employee_nomineeServlet")
@MultipartConfig
public class Employee_nomineeServlet extends BaseServlet implements EmployeeServletService {

    private static final long serialVersionUID = 1L;

    private final Employee_nomineeDAO employeeNomineeDAO = Employee_nomineeDAO.getInstance();

    @Override
    public String getTableName() {
        return employeeNomineeDAO.getTableName();
    }

    @Override
    public String getServletName() {
        return "Employee_nomineeServlet";
    }

    @Override
    public Employee_nomineeDAO getCommonDAOService() {
        return employeeNomineeDAO;
    }

    @Override
    public String getAjaxAddRedirectURL(HttpServletRequest request, CommonDTO commonDTO) {
        return getAddOrEditOrDeleteRedirectURL(request, 6, "nominee");
    }

    @Override
    public String getAjaxEditRedirectURL(HttpServletRequest request, CommonDTO commonDTO) {
        return getAddOrEditOrDeleteRedirectURL(request, 6, "nominee");
    }

    @Override
    public String getDeleteRedirectURL(HttpServletRequest request) {
        return getAddOrEditOrDeleteRedirectURL(request, 6, "nominee");
    }

    @Override
    public boolean getEditPagePermission(HttpServletRequest request) {
        return checkEditOrViewPagePermission(request);
    }

    @Override
    public boolean getViewPagePermission(HttpServletRequest request) {
        return checkEditOrViewPagePermission(request);
    }

    @Override
    public boolean getEditPermission(HttpServletRequest request) {
        return checkEditOrViewPagePermission(request);
    }

    @Override
    public boolean getAddPermission(HttpServletRequest request) {
        return checkAddPermission(request);
    }

    @Override
    public boolean getAddPagePermission(HttpServletRequest request) {
        return checkAddPermission(request);
    }

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        Employee_nomineeDTO employee_nomineeDTO;
        long currentTime = System.currentTimeMillis();
        boolean isLangEng = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
        if (addFlag) {
            employee_nomineeDTO = new Employee_nomineeDTO();
            long empId;
            try {
                empId = Long.parseLong(request.getParameter("empId"));
            } catch (Exception ex) {
                ex.printStackTrace();
                throw new Exception(isLangEng ? "empId is not found" : "empId পাওয়া যায়নি");
            }
            if (Employee_recordsRepository.getInstance().getById(empId) == null) {
                throw new Exception(isLangEng ? "Employee information is not found" : "কর্মকর্তার তথ্য পাওয়া যায়নি");
            }
            employee_nomineeDTO.employeeRecordsId = empId;
            employee_nomineeDTO.insertedByUserId = userDTO.ID;
            employee_nomineeDTO.insertionDate = currentTime;
        } else {
            employee_nomineeDTO = (Employee_nomineeDTO) employeeNomineeDAO.getDTOByID(Long.parseLong(request.getParameter("iD")));
        }
        employee_nomineeDTO.modifiedBy = String.valueOf(userDTO.ID);
        employee_nomineeDTO.lastModificationTime = currentTime;
        employee_nomineeDTO.employeeFamilyInfoId = Long.parseLong(request.getParameter("nomineeId"));
        employee_nomineeDTO.nomineeCat = Integer.parseInt(request.getParameter("nomineeCat"));
        employee_nomineeDTO.percentage = Double.parseDouble(request.getParameter("percentage"));
        if (addFlag) {
            employeeNomineeDAO.add(employee_nomineeDTO);
        } else {
            employeeNomineeDAO.update(employee_nomineeDTO);
        }
        return employee_nomineeDTO;
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        search(request);
        super.doGet(request, response);
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[0];
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[0];
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[0];
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Employee_nomineeServlet.class;
    }
}