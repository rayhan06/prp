package employee_nominee;


public class EmployeeNomineeApiPerformerFactory {
    private static final String GET_NOMINEE_BY_ID_REQUEST = "getNomineeById";
    private static final String DELETE_NOMINEE_BY_ID_REQUEST = "deleteNomineeById";

    public static EmployeeNomineeApiPerformer getPerformer(String actionType) {
        if (actionType != null) {
            switch (actionType) {
                case GET_NOMINEE_BY_ID_REQUEST:
                    return new GetNomineeByIdPerformer();

                case DELETE_NOMINEE_BY_ID_REQUEST:
                    return new DeleteNomineePerformer();
            }
        }
        return null;
    }
}
