package employee_nominee;

import common.CommonDAOService;
import common.ConnectionAndStatementUtil;
import common.EmployeeCommonDAOService;
import common.NameDTO;
import employee_family_info.Employee_family_infoDAO;
import employee_family_info.Employee_family_infoDTO;
import employee_records.Employee_recordsDTO;
import employee_records.Employee_recordsRepository;
import org.apache.log4j.Logger;
import pb.CatRepository;
import pb.CategoryLanguageModel;
import pb.OptionDTO;
import pb.Utils;
import relation.RelationRepository;
import util.CommonDTO;
import util.StringUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

@SuppressWarnings({"unused"})
public class Employee_nomineeDAO implements EmployeeCommonDAOService<Employee_nomineeDTO> {

    private static final Logger logger = Logger.getLogger(Employee_nomineeDAO.class);

    private static final String addQuery = "INSERT INTO {tableName} (employee_records_id,employee_family_info_id,nominee_cat,percentage,inserted_by_user_id,"
            .concat("insertion_date,modified_by,lastModificationTime,search_column,isDeleted,ID) VALUES(?,?,?,?,?,?,?,?,?,?,?)");

    private static final String updateQuery = "UPDATE {tableName} SET employee_records_id=?,employee_family_info_id=?,nominee_cat=?,percentage=?,"
            .concat("inserted_by_user_id=?,insertion_date=?,modified_by=?,lastModificationTime =?,search_column=? WHERE ID = ?");

    private static final String deleteQuery = "UPDATE employee_nominee SET isDeleted=1,lastModificationTime= ? WHERE ID = ?";

    private static final String getByEmployeeId = "SELECT * FROM employee_nominee WHERE employee_records_id= %d";

    private final Map<String, String> searchMap = new HashMap<>();

    private Employee_nomineeDAO() {
        searchMap.put("employee_records_id_internal", " and (employee_records_id = ?)");
        searchMap.put("employee_records_id", " and (employee_records_id = ?)");
        searchMap.put("nominee_cat", "and (nominee_cat = ?)");
        searchMap.put("AnyField", " and (search_column like ?)");
    }

    private static class LazyLoader {
        static final Employee_nomineeDAO INSTANCE = new Employee_nomineeDAO();
    }

    public static Employee_nomineeDAO getInstance() {
        return Employee_nomineeDAO.LazyLoader.INSTANCE;
    }

    @Override
    public void set(PreparedStatement ps, Employee_nomineeDTO employee_nomineeDTO, boolean isInsert) throws SQLException {
        int index = 0;
        Employee_recordsDTO employeeRecordDTO = Employee_recordsRepository.getInstance().getById(employee_nomineeDTO.employeeRecordsId);
        Employee_family_infoDTO employeeFamilyInfoDTO = Employee_family_infoDAO.getInstance().getDTOFromID(employee_nomineeDTO.employeeFamilyInfoId);
        if (employeeRecordDTO != null) {
            employee_nomineeDTO.searchColumn = employeeRecordDTO.nameEng + " " + employeeRecordDTO.nameBng;
        }
        if (employeeFamilyInfoDTO != null) {
            employee_nomineeDTO.searchColumn = employee_nomineeDTO.searchColumn + " " + employeeFamilyInfoDTO.firstNameEn + " " + employeeFamilyInfoDTO.firstNameBn;
        }
        ps.setObject(++index, employee_nomineeDTO.employeeRecordsId);
        ps.setObject(++index, employee_nomineeDTO.employeeFamilyInfoId);
        ps.setObject(++index, employee_nomineeDTO.nomineeCat);
        ps.setObject(++index, employee_nomineeDTO.percentage);
        ps.setObject(++index, employee_nomineeDTO.insertedByUserId);
        ps.setObject(++index, employee_nomineeDTO.insertionDate);
        ps.setObject(++index, employee_nomineeDTO.modifiedBy);
        ps.setObject(++index, System.currentTimeMillis());
        ps.setObject(++index, employee_nomineeDTO.searchColumn);
        if (isInsert) {
            ps.setObject(++index, 0);
        }
        ps.setObject(++index, employee_nomineeDTO.iD);
    }

    @Override
    public Employee_nomineeDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Employee_nomineeDTO employee_nomineeDTO = new Employee_nomineeDTO();
            employee_nomineeDTO.iD = rs.getLong("ID");
            employee_nomineeDTO.employeeRecordsId = rs.getLong("employee_records_id");
            employee_nomineeDTO.employeeFamilyInfoId = rs.getLong("employee_family_info_id");
            employee_nomineeDTO.nomineeCat = rs.getInt("nominee_cat");
            employee_nomineeDTO.percentage = rs.getDouble("percentage");
            employee_nomineeDTO.insertedByUserId = rs.getLong("inserted_by_user_id");
            employee_nomineeDTO.insertionDate = rs.getLong("insertion_date");
            employee_nomineeDTO.modifiedBy = rs.getString("modified_by");
            employee_nomineeDTO.isDeleted = rs.getInt("isDeleted");
            employee_nomineeDTO.lastModificationTime = rs.getLong("lastModificationTime");
            employee_nomineeDTO.searchColumn = rs.getString("search_column");
            return employee_nomineeDTO;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    @Override
    public String getTableName() {
        return "employee_nominee";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }


    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Employee_nomineeDTO) commonDTO, addQuery, true);
    }

    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Employee_nomineeDTO) commonDTO, updateQuery, false);
    }

    public CommonDTO getDTOByID(long ID) throws Exception {
        return getDTOFromID(ID);
    }

    public List<Employee_nomineeDTO> getAllEmployee_nominee(boolean isFirstReload) {
        return getAllDTOs(isFirstReload);
    }


    public List<EmployeeNomineeModel> getEmployeeNomineeModelListByEmployeeId(long employeeId, Long selectedId) {
        return getByEmployeeId(employeeId)
                .stream()
                .filter(dto -> (selectedId == null || dto.iD != selectedId) && dto.isDeleted == 0)
                .map(this::buildEmployeeNomineeModel)
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    private EmployeeNomineeModel buildEmployeeNomineeModel(Employee_nomineeDTO nomineeDTO) {
        EmployeeNomineeModel model = new EmployeeNomineeModel();
        model.dto = nomineeDTO;
        model.percentageEng = String.valueOf(nomineeDTO.percentage);
        model.percentageBan = StringUtils.convertToBanNumber(model.percentageEng);
        CategoryLanguageModel languageModel = CatRepository.getInstance().getCategoryLanguageModel("nominee", nomineeDTO.nomineeCat);
        model.nomineeCategoryEng = languageModel.englishText;
        model.nomineeCategoryBan = languageModel.banglaText;
        try {
            Employee_family_infoDTO familyInfoDTO = Employee_family_infoDAO.getInstance().getDTOFromID(nomineeDTO.employeeFamilyInfoId);
            if (familyInfoDTO == null) {
                return null;
            }
            model.firstNameEng = familyInfoDTO.firstNameEn;
            model.firstNameBan = familyInfoDTO.firstNameBn;
            NameDTO nameDTO = RelationRepository.getInstance().getDTOByID(familyInfoDTO.relationType);
            if (nameDTO == null) {
                return null;
            }
            model.relativeTypeEng = nameDTO.nameEn;
            model.relativeTypeBan = nameDTO.nameBn;
            model.mobileEng = familyInfoDTO.mobileNumber1;
            model.mobileBan = StringUtils.convertToBanNumber(model.mobileEng);
        } catch (Exception ex) {
            logger.error(ex);
            return null;
        }
        return model;
    }


    public void deleteNomineeByID(long ID) throws Exception {
        long lastModificationTime = System.currentTimeMillis();

        if (CommonDAOService.CONNECTION_THREAD_LOCAL.get() != null) {
            AtomicReference<Exception> atomicReference = new AtomicReference<>();
            Connection connection = CommonDAOService.CONNECTION_THREAD_LOCAL.get();
            ConnectionAndStatementUtil.getWritePrepareStatement(ps -> {
                try {
                    ps.setLong(1, lastModificationTime);
                    ps.setLong(2, ID);
                    ps.executeUpdate();
                    recordUpdateTime(connection, "employee_nominee");
                } catch (Exception e) {
                    e.printStackTrace();
                    atomicReference.set(e);
                }
            }, connection, deleteQuery);
            if (atomicReference.get() != null) {
                throw atomicReference.get();
            }
        } else {
            ConnectionAndStatementUtil.getWriteConnectionAndPrepareStatement(model -> {
                Connection connection = model.getConnection();
                PreparedStatement ps = (PreparedStatement) model.getStatement();
                try {
                    ps.setLong(1, lastModificationTime);
                    ps.setLong(2, ID);
                    ps.executeUpdate();
                    recordUpdateTime(connection, "employee_nominee");
                } catch (SQLException ex) {
                    logger.error(ex);
                }
            }, deleteQuery);
        }


    }

    public String getNomineeOptions(long employeeRecordsId, String language, Long selectedId) {
        List<OptionDTO> optionDTOList = null;
        List<Employee_family_infoDTO> familyInfoDTOList = Employee_family_infoDAO.getInstance().getByEmployeeId(employeeRecordsId);
        if (selectedId != null) {
            Employee_family_infoDTO familyInfoDTO = familyInfoDTOList.stream()
                    .filter(dto -> dto.iD == selectedId)
                    .findAny()
                    .orElse(null);
            if (familyInfoDTO != null) {
                return Utils.buildOptionsWithoutSelectOption(Collections.singletonList(buildOptionDTO(familyInfoDTO)), language);
            }
        }
        if (familyInfoDTOList != null && familyInfoDTOList.size() > 0) {
            List<Employee_nomineeDTO> alreadyGivenNomineeList = getByEmployeeId(employeeRecordsId);
            Map<Long, Employee_nomineeDTO> mapAlreadyAddedAsNominee = alreadyGivenNomineeList.stream()
                    .collect(Collectors.toMap(e -> e.employeeFamilyInfoId, e -> e));
            optionDTOList = familyInfoDTOList.stream()
                    .filter(dto -> mapAlreadyAddedAsNominee.get(dto.iD) == null)
                    .map(this::buildOptionDTO)
                    .collect(Collectors.toList());
        }
        return Utils.buildOptions(optionDTOList, language, null);
    }

    private OptionDTO buildOptionDTO(Employee_family_infoDTO dto) {
        NameDTO nameDTO = RelationRepository.getInstance().getDTOByID(dto.relationType);
        return new OptionDTO(dto.firstNameEn.concat(" | ").concat(nameDTO.nameEn),
                dto.firstNameBn.concat(" | ").concat(nameDTO.nameBn),
                String.valueOf(dto.iD));
    }
}
	