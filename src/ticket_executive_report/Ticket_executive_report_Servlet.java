package ticket_executive_report;


import java.io.IOException;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import language.LC;
import language.LM;

import login.LoginDTO;
import permission.MenuConstants;

import sessionmanager.SessionConstants;
import support_ticket.Support_ticketDTO;
import ticket_forward_history.Ticket_forward_historyDTO;
import user.UserDTO;
import user.UserRepository;

import util.ReportRequestHandler;
import pbReport.*;






@WebServlet("/Ticket_executive_report_Servlet")
public class Ticket_executive_report_Servlet  extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	String[][] Criteria =
	{
		{"criteria","","forward_from_organogram_id","=","","long","","","any","organogramType", LC.HM_NAME + ""},	
		{"criteria","","ticket_issues_type","=","AND","int","","","any","ticketIssuesType", LC.TICKET_TYPE_STATUS_REPORT_WHERE_TICKETISSUESTYPE + ""},
		{"criteria","","insertion_date",">=","AND","long","","",Long.MIN_VALUE + "","startDate", LC.HM_START_DATE + ""},		
		{"criteria","","insertion_date","<=","AND","long","","",Long.MAX_VALUE + "","endDate", LC.HM_END_DATE + ""}		
	};
	
	String[][] Display =
	{
		{"display","","forward_from_organogram_id","organogram",""},		
		{"display","","ticket_issues_type","type",""},		
		{"display","","assignment_status_cat","ticket_action",""},		
		{"display","","COUNT(id)","text",""}		
	};
	
	String GroupBy = "forward_from_organogram_id, assignment_status_cat, ticket_issues_type";
	String OrderBY = "";
	
	ReportRequestHandler reportRequestHandler;
	
	public Ticket_executive_report_Servlet(){

	}
	
	private final ReportService reportService = new ReportService();
	
	private String sql;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String language = "english";
		if(userDTO!= null && userDTO.languageID == SessionConstants.BANGLA)
		{
			language = "bangla";
		}
				
		String actionType = request.getParameter("actionType");

		System.out.println("In ssservlet doget, actiontype = " + actionType);
		
		sql = "(SELECT forward_from_organogram_id, ticket_issues_type, forward_action_cat AS assignment_status_cat, id, insertion_date FROM ticket_forward_history WHERE "
				+ "(forward_action_cat = " + Ticket_forward_historyDTO.CLOSE + " OR (forward_action_cat = " + Ticket_forward_historyDTO.FORWARD 
				+ " AND ticket_status_cat = " + Support_ticketDTO.SOLVED + " ))) temp";

		Display[0][4] = LM.getText(LC.HM_NAME, loginDTO);
		Display[1][4] = LM.getText(LC.TICKET_EXECUTIVE_REPORT_SELECT_TICKETISSUESTYPE, loginDTO);
		Display[2][4] = LM.getText(LC.HM_ACTION, loginDTO);
		Display[3][4] = LM.getText(LC.HM_COUNT, loginDTO);

		
		String reportName = LM.getText(LC.TICKET_EXECUTIVE_REPORT_OTHER_TICKET_EXECUTIVE_REPORT, loginDTO);
		
		reportRequestHandler = new ReportRequestHandler(null,
				Criteria, Display, GroupBy, OrderBY, sql,
				reportService);

		
		reportRequestHandler.handleReportGet(request, response, userDTO, "ticket_executive_report",
				MenuConstants.TICKET_EXECUTIVE_REPORT_DETAILS, language, reportName, "ticket_executive_report");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		doGet(request, response);
	}
}
