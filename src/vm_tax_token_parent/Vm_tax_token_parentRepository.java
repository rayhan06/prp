package vm_tax_token_parent;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import com.google.gson.Gson;
import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;
import vm_tax_token.Vm_tax_tokenDTO;


public class Vm_tax_token_parentRepository implements Repository {
	Vm_tax_token_parentDAO vm_tax_token_parentDAO = null;
	Gson gson;
	
	public void setDAO(Vm_tax_token_parentDAO vm_tax_token_parentDAO)
	{
		this.vm_tax_token_parentDAO = vm_tax_token_parentDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Vm_tax_token_parentRepository.class);
	Map<Long, Vm_tax_token_parentDTO>mapOfVm_tax_token_parentDTOToiD;
//	Map<Long, Set<Vm_tax_token_parentDTO> >mapOfVm_tax_token_parentDTOTofiscalYearId;
//	Map<Double, Set<Vm_tax_token_parentDTO> >mapOfVm_tax_token_parentDTOTototalTaxTokenFees;
//	Map<Double, Set<Vm_tax_token_parentDTO> >mapOfVm_tax_token_parentDTOTototalFitnessFees;
//	Map<Double, Set<Vm_tax_token_parentDTO> >mapOfVm_tax_token_parentDTOTototalFees;
//	Map<String, Set<Vm_tax_token_parentDTO> >mapOfVm_tax_token_parentDTOTosearchColumn;
//	Map<String, Set<Vm_tax_token_parentDTO> >mapOfVm_tax_token_parentDTOToinsertedBy;
//	Map<String, Set<Vm_tax_token_parentDTO> >mapOfVm_tax_token_parentDTOTomodifiedBy;
//	Map<Long, Set<Vm_tax_token_parentDTO> >mapOfVm_tax_token_parentDTOToinsertionDate;
//	Map<Long, Set<Vm_tax_token_parentDTO> >mapOfVm_tax_token_parentDTOTolastModificationTime;


	static Vm_tax_token_parentRepository instance = null;  
	private Vm_tax_token_parentRepository(){
		mapOfVm_tax_token_parentDTOToiD = new ConcurrentHashMap<>();
//		mapOfVm_tax_token_parentDTOTofiscalYearId = new ConcurrentHashMap<>();
//		mapOfVm_tax_token_parentDTOTototalTaxTokenFees = new ConcurrentHashMap<>();
//		mapOfVm_tax_token_parentDTOTototalFitnessFees = new ConcurrentHashMap<>();
//		mapOfVm_tax_token_parentDTOTototalFees = new ConcurrentHashMap<>();
//		mapOfVm_tax_token_parentDTOTosearchColumn = new ConcurrentHashMap<>();
//		mapOfVm_tax_token_parentDTOToinsertedBy = new ConcurrentHashMap<>();
//		mapOfVm_tax_token_parentDTOTomodifiedBy = new ConcurrentHashMap<>();
//		mapOfVm_tax_token_parentDTOToinsertionDate = new ConcurrentHashMap<>();
//		mapOfVm_tax_token_parentDTOTolastModificationTime = new ConcurrentHashMap<>();

		setDAO(new Vm_tax_token_parentDAO());

		gson = new Gson();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Vm_tax_token_parentRepository getInstance(){
		if (instance == null){
			instance = new Vm_tax_token_parentRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(vm_tax_token_parentDAO == null)
		{
			return;
		}
		try {
			List<Vm_tax_token_parentDTO> vm_tax_token_parentDTOs = (List<Vm_tax_token_parentDTO>) vm_tax_token_parentDAO.getAll(reloadAll);
			for(Vm_tax_token_parentDTO vm_tax_token_parentDTO : vm_tax_token_parentDTOs) {
				Vm_tax_token_parentDTO oldVm_tax_token_parentDTO = getVm_tax_token_parentDTOByIDWithoutClone(vm_tax_token_parentDTO.iD);
				if( oldVm_tax_token_parentDTO != null ) {
					mapOfVm_tax_token_parentDTOToiD.remove(oldVm_tax_token_parentDTO.iD);
				
//					if(mapOfVm_tax_token_parentDTOTofiscalYearId.containsKey(oldVm_tax_token_parentDTO.fiscalYearId)) {
//						mapOfVm_tax_token_parentDTOTofiscalYearId.get(oldVm_tax_token_parentDTO.fiscalYearId).remove(oldVm_tax_token_parentDTO);
//					}
//					if(mapOfVm_tax_token_parentDTOTofiscalYearId.get(oldVm_tax_token_parentDTO.fiscalYearId).isEmpty()) {
//						mapOfVm_tax_token_parentDTOTofiscalYearId.remove(oldVm_tax_token_parentDTO.fiscalYearId);
//					}
//
//					if(mapOfVm_tax_token_parentDTOTototalTaxTokenFees.containsKey(oldVm_tax_token_parentDTO.totalTaxTokenFees)) {
//						mapOfVm_tax_token_parentDTOTototalTaxTokenFees.get(oldVm_tax_token_parentDTO.totalTaxTokenFees).remove(oldVm_tax_token_parentDTO);
//					}
//					if(mapOfVm_tax_token_parentDTOTototalTaxTokenFees.get(oldVm_tax_token_parentDTO.totalTaxTokenFees).isEmpty()) {
//						mapOfVm_tax_token_parentDTOTototalTaxTokenFees.remove(oldVm_tax_token_parentDTO.totalTaxTokenFees);
//					}
//
//					if(mapOfVm_tax_token_parentDTOTototalFitnessFees.containsKey(oldVm_tax_token_parentDTO.totalFitnessFees)) {
//						mapOfVm_tax_token_parentDTOTototalFitnessFees.get(oldVm_tax_token_parentDTO.totalFitnessFees).remove(oldVm_tax_token_parentDTO);
//					}
//					if(mapOfVm_tax_token_parentDTOTototalFitnessFees.get(oldVm_tax_token_parentDTO.totalFitnessFees).isEmpty()) {
//						mapOfVm_tax_token_parentDTOTototalFitnessFees.remove(oldVm_tax_token_parentDTO.totalFitnessFees);
//					}
//
//					if(mapOfVm_tax_token_parentDTOTototalFees.containsKey(oldVm_tax_token_parentDTO.totalFees)) {
//						mapOfVm_tax_token_parentDTOTototalFees.get(oldVm_tax_token_parentDTO.totalFees).remove(oldVm_tax_token_parentDTO);
//					}
//					if(mapOfVm_tax_token_parentDTOTototalFees.get(oldVm_tax_token_parentDTO.totalFees).isEmpty()) {
//						mapOfVm_tax_token_parentDTOTototalFees.remove(oldVm_tax_token_parentDTO.totalFees);
//					}
//
//					if(mapOfVm_tax_token_parentDTOTosearchColumn.containsKey(oldVm_tax_token_parentDTO.searchColumn)) {
//						mapOfVm_tax_token_parentDTOTosearchColumn.get(oldVm_tax_token_parentDTO.searchColumn).remove(oldVm_tax_token_parentDTO);
//					}
//					if(mapOfVm_tax_token_parentDTOTosearchColumn.get(oldVm_tax_token_parentDTO.searchColumn).isEmpty()) {
//						mapOfVm_tax_token_parentDTOTosearchColumn.remove(oldVm_tax_token_parentDTO.searchColumn);
//					}
//
//					if(mapOfVm_tax_token_parentDTOToinsertedBy.containsKey(oldVm_tax_token_parentDTO.insertedBy)) {
//						mapOfVm_tax_token_parentDTOToinsertedBy.get(oldVm_tax_token_parentDTO.insertedBy).remove(oldVm_tax_token_parentDTO);
//					}
//					if(mapOfVm_tax_token_parentDTOToinsertedBy.get(oldVm_tax_token_parentDTO.insertedBy).isEmpty()) {
//						mapOfVm_tax_token_parentDTOToinsertedBy.remove(oldVm_tax_token_parentDTO.insertedBy);
//					}
//
//					if(mapOfVm_tax_token_parentDTOTomodifiedBy.containsKey(oldVm_tax_token_parentDTO.modifiedBy)) {
//						mapOfVm_tax_token_parentDTOTomodifiedBy.get(oldVm_tax_token_parentDTO.modifiedBy).remove(oldVm_tax_token_parentDTO);
//					}
//					if(mapOfVm_tax_token_parentDTOTomodifiedBy.get(oldVm_tax_token_parentDTO.modifiedBy).isEmpty()) {
//						mapOfVm_tax_token_parentDTOTomodifiedBy.remove(oldVm_tax_token_parentDTO.modifiedBy);
//					}
//
//					if(mapOfVm_tax_token_parentDTOToinsertionDate.containsKey(oldVm_tax_token_parentDTO.insertionDate)) {
//						mapOfVm_tax_token_parentDTOToinsertionDate.get(oldVm_tax_token_parentDTO.insertionDate).remove(oldVm_tax_token_parentDTO);
//					}
//					if(mapOfVm_tax_token_parentDTOToinsertionDate.get(oldVm_tax_token_parentDTO.insertionDate).isEmpty()) {
//						mapOfVm_tax_token_parentDTOToinsertionDate.remove(oldVm_tax_token_parentDTO.insertionDate);
//					}
//
//					if(mapOfVm_tax_token_parentDTOTolastModificationTime.containsKey(oldVm_tax_token_parentDTO.lastModificationTime)) {
//						mapOfVm_tax_token_parentDTOTolastModificationTime.get(oldVm_tax_token_parentDTO.lastModificationTime).remove(oldVm_tax_token_parentDTO);
//					}
//					if(mapOfVm_tax_token_parentDTOTolastModificationTime.get(oldVm_tax_token_parentDTO.lastModificationTime).isEmpty()) {
//						mapOfVm_tax_token_parentDTOTolastModificationTime.remove(oldVm_tax_token_parentDTO.lastModificationTime);
//					}
					
					
				}
				if(vm_tax_token_parentDTO.isDeleted == 0) 
				{
					
					mapOfVm_tax_token_parentDTOToiD.put(vm_tax_token_parentDTO.iD, vm_tax_token_parentDTO);
				
//					if( ! mapOfVm_tax_token_parentDTOTofiscalYearId.containsKey(vm_tax_token_parentDTO.fiscalYearId)) {
//						mapOfVm_tax_token_parentDTOTofiscalYearId.put(vm_tax_token_parentDTO.fiscalYearId, new HashSet<>());
//					}
//					mapOfVm_tax_token_parentDTOTofiscalYearId.get(vm_tax_token_parentDTO.fiscalYearId).add(vm_tax_token_parentDTO);
//
//					if( ! mapOfVm_tax_token_parentDTOTototalTaxTokenFees.containsKey(vm_tax_token_parentDTO.totalTaxTokenFees)) {
//						mapOfVm_tax_token_parentDTOTototalTaxTokenFees.put(vm_tax_token_parentDTO.totalTaxTokenFees, new HashSet<>());
//					}
//					mapOfVm_tax_token_parentDTOTototalTaxTokenFees.get(vm_tax_token_parentDTO.totalTaxTokenFees).add(vm_tax_token_parentDTO);
//
//					if( ! mapOfVm_tax_token_parentDTOTototalFitnessFees.containsKey(vm_tax_token_parentDTO.totalFitnessFees)) {
//						mapOfVm_tax_token_parentDTOTototalFitnessFees.put(vm_tax_token_parentDTO.totalFitnessFees, new HashSet<>());
//					}
//					mapOfVm_tax_token_parentDTOTototalFitnessFees.get(vm_tax_token_parentDTO.totalFitnessFees).add(vm_tax_token_parentDTO);
//
//					if( ! mapOfVm_tax_token_parentDTOTototalFees.containsKey(vm_tax_token_parentDTO.totalFees)) {
//						mapOfVm_tax_token_parentDTOTototalFees.put(vm_tax_token_parentDTO.totalFees, new HashSet<>());
//					}
//					mapOfVm_tax_token_parentDTOTototalFees.get(vm_tax_token_parentDTO.totalFees).add(vm_tax_token_parentDTO);
//
//					if( ! mapOfVm_tax_token_parentDTOTosearchColumn.containsKey(vm_tax_token_parentDTO.searchColumn)) {
//						mapOfVm_tax_token_parentDTOTosearchColumn.put(vm_tax_token_parentDTO.searchColumn, new HashSet<>());
//					}
//					mapOfVm_tax_token_parentDTOTosearchColumn.get(vm_tax_token_parentDTO.searchColumn).add(vm_tax_token_parentDTO);
//
//					if( ! mapOfVm_tax_token_parentDTOToinsertedBy.containsKey(vm_tax_token_parentDTO.insertedBy)) {
//						mapOfVm_tax_token_parentDTOToinsertedBy.put(vm_tax_token_parentDTO.insertedBy, new HashSet<>());
//					}
//					mapOfVm_tax_token_parentDTOToinsertedBy.get(vm_tax_token_parentDTO.insertedBy).add(vm_tax_token_parentDTO);
//
//					if( ! mapOfVm_tax_token_parentDTOTomodifiedBy.containsKey(vm_tax_token_parentDTO.modifiedBy)) {
//						mapOfVm_tax_token_parentDTOTomodifiedBy.put(vm_tax_token_parentDTO.modifiedBy, new HashSet<>());
//					}
//					mapOfVm_tax_token_parentDTOTomodifiedBy.get(vm_tax_token_parentDTO.modifiedBy).add(vm_tax_token_parentDTO);
//
//					if( ! mapOfVm_tax_token_parentDTOToinsertionDate.containsKey(vm_tax_token_parentDTO.insertionDate)) {
//						mapOfVm_tax_token_parentDTOToinsertionDate.put(vm_tax_token_parentDTO.insertionDate, new HashSet<>());
//					}
//					mapOfVm_tax_token_parentDTOToinsertionDate.get(vm_tax_token_parentDTO.insertionDate).add(vm_tax_token_parentDTO);
//
//					if( ! mapOfVm_tax_token_parentDTOTolastModificationTime.containsKey(vm_tax_token_parentDTO.lastModificationTime)) {
//						mapOfVm_tax_token_parentDTOTolastModificationTime.put(vm_tax_token_parentDTO.lastModificationTime, new HashSet<>());
//					}
//					mapOfVm_tax_token_parentDTOTolastModificationTime.get(vm_tax_token_parentDTO.lastModificationTime).add(vm_tax_token_parentDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}

	public Vm_tax_token_parentDTO clone(Vm_tax_token_parentDTO dto) {
		String raw = gson.toJson(dto);
		return gson.fromJson(raw, Vm_tax_token_parentDTO.class);
	}

	public List<Vm_tax_token_parentDTO> clone(List<Vm_tax_token_parentDTO> dtoList) {
		return dtoList
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}

	public Vm_tax_token_parentDTO getVm_tax_token_parentDTOByIDWithoutClone( long ID){
		return mapOfVm_tax_token_parentDTOToiD.get(ID);
	}
	
	public List<Vm_tax_token_parentDTO> getVm_tax_token_parentList() {
		List <Vm_tax_token_parentDTO> vm_tax_token_parents = new ArrayList<Vm_tax_token_parentDTO>(this.mapOfVm_tax_token_parentDTOToiD.values());
		return vm_tax_token_parents;
	}
	
	
	public Vm_tax_token_parentDTO getVm_tax_token_parentDTOByID( long ID){
		return clone(mapOfVm_tax_token_parentDTOToiD.get(ID));
	}
	
	
//	public List<Vm_tax_token_parentDTO> getVm_tax_token_parentDTOByfiscal_year_id(long fiscal_year_id) {
//		return new ArrayList<>( mapOfVm_tax_token_parentDTOTofiscalYearId.getOrDefault(fiscal_year_id,new HashSet<>()));
//	}
//
//
//	public List<Vm_tax_token_parentDTO> getVm_tax_token_parentDTOBytotal_tax_token_fees(double total_tax_token_fees) {
//		return new ArrayList<>( mapOfVm_tax_token_parentDTOTototalTaxTokenFees.getOrDefault(total_tax_token_fees,new HashSet<>()));
//	}
//
//
//	public List<Vm_tax_token_parentDTO> getVm_tax_token_parentDTOBytotal_fitness_fees(double total_fitness_fees) {
//		return new ArrayList<>( mapOfVm_tax_token_parentDTOTototalFitnessFees.getOrDefault(total_fitness_fees,new HashSet<>()));
//	}
//
//
//	public List<Vm_tax_token_parentDTO> getVm_tax_token_parentDTOBytotal_fees(double total_fees) {
//		return new ArrayList<>( mapOfVm_tax_token_parentDTOTototalFees.getOrDefault(total_fees,new HashSet<>()));
//	}
//
//
//	public List<Vm_tax_token_parentDTO> getVm_tax_token_parentDTOBysearch_column(String search_column) {
//		return new ArrayList<>( mapOfVm_tax_token_parentDTOTosearchColumn.getOrDefault(search_column,new HashSet<>()));
//	}
//
//
//	public List<Vm_tax_token_parentDTO> getVm_tax_token_parentDTOByinserted_by(String inserted_by) {
//		return new ArrayList<>( mapOfVm_tax_token_parentDTOToinsertedBy.getOrDefault(inserted_by,new HashSet<>()));
//	}
//
//
//	public List<Vm_tax_token_parentDTO> getVm_tax_token_parentDTOBymodified_by(String modified_by) {
//		return new ArrayList<>( mapOfVm_tax_token_parentDTOTomodifiedBy.getOrDefault(modified_by,new HashSet<>()));
//	}
//
//
//	public List<Vm_tax_token_parentDTO> getVm_tax_token_parentDTOByinsertion_date(long insertion_date) {
//		return new ArrayList<>( mapOfVm_tax_token_parentDTOToinsertionDate.getOrDefault(insertion_date,new HashSet<>()));
//	}
//
//
//	public List<Vm_tax_token_parentDTO> getVm_tax_token_parentDTOBylastModificationTime(long lastModificationTime) {
//		return new ArrayList<>( mapOfVm_tax_token_parentDTOTolastModificationTime.getOrDefault(lastModificationTime,new HashSet<>()));
//	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "vm_tax_token_parent";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


