package vm_tax_token_parent;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;

import fiscal_year.Fiscal_yearDAO;
import fiscal_year.Fiscal_yearDTO;
import sessionmanager.SessionConstants;
import java.sql.SQLException;
import common.ConnectionAndStatementUtil;


import org.apache.log4j.Logger;

import dbm.*;

import repository.RepositoryManager;

import util.*;
import pb.*;
import user.UserDTO;
import vm_tax_token.Vm_tax_tokenDTO;

public class Vm_tax_token_parentDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());
	Fiscal_yearDAO fiscal_yearDAO = new Fiscal_yearDAO();

	public Vm_tax_token_parentDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";
		commonMaps = new Vm_tax_token_parentMAPS(tableName);
		columnNames = new String[] 
		{
			"ID",
			"fiscal_year_id",
			"total_tax_token_fees",
			"total_fitness_fees",
			"total_fees",
			"search_column",
			"inserted_by",
			"modified_by",
			"insertion_date",
			"isDeleted",
			"lastModificationTime"
		};
	}
	
	public Vm_tax_token_parentDAO()
	{
		this("vm_tax_token_parent");		
	}
	
	public void setSearchColumn(Vm_tax_token_parentDTO vm_tax_token_parentDTO)
	{
		Fiscal_yearDTO fiscal_yearDTO = fiscal_yearDAO.getDTOByID(vm_tax_token_parentDTO.fiscalYearId);
		vm_tax_token_parentDTO.searchColumn = "";
		vm_tax_token_parentDTO.searchColumn += fiscal_yearDTO.nameEn + " " + fiscal_yearDTO.nameBn+" " ;
		vm_tax_token_parentDTO.searchColumn += vm_tax_token_parentDTO.totalTaxTokenFees + " ";
		vm_tax_token_parentDTO.searchColumn += vm_tax_token_parentDTO.totalFitnessFees + " ";
		vm_tax_token_parentDTO.searchColumn += vm_tax_token_parentDTO.totalFees + " ";
	}
	
	public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException
	{
		Vm_tax_token_parentDTO vm_tax_token_parentDTO = (Vm_tax_token_parentDTO)commonDTO;
		int index = 1;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(vm_tax_token_parentDTO);
		if(isInsert)
		{
			ps.setObject(index++,vm_tax_token_parentDTO.iD);
		}
		ps.setObject(index++,vm_tax_token_parentDTO.fiscalYearId);
		ps.setObject(index++,vm_tax_token_parentDTO.totalTaxTokenFees);
		ps.setObject(index++,vm_tax_token_parentDTO.totalFitnessFees);
		ps.setObject(index++,vm_tax_token_parentDTO.totalFees);
		ps.setObject(index++,vm_tax_token_parentDTO.searchColumn);
		ps.setObject(index++,vm_tax_token_parentDTO.insertedBy);
		ps.setObject(index++,vm_tax_token_parentDTO.modifiedBy);
		ps.setObject(index++,vm_tax_token_parentDTO.insertionDate);
		if(isInsert)
		{
			ps.setObject(index++, 0);
			ps.setObject(index++, lastModificationTime);
		}
	}
	
	public Vm_tax_token_parentDTO build(ResultSet rs)
	{
		try
		{
			Vm_tax_token_parentDTO vm_tax_token_parentDTO = new Vm_tax_token_parentDTO();
			vm_tax_token_parentDTO.iD = rs.getLong("ID");
			vm_tax_token_parentDTO.fiscalYearId = rs.getLong("fiscal_year_id");
			vm_tax_token_parentDTO.totalTaxTokenFees = rs.getDouble("total_tax_token_fees");
			vm_tax_token_parentDTO.totalFitnessFees = rs.getDouble("total_fitness_fees");
			vm_tax_token_parentDTO.totalFees = rs.getDouble("total_fees");
			vm_tax_token_parentDTO.searchColumn = rs.getString("search_column");
			vm_tax_token_parentDTO.insertedBy = rs.getString("inserted_by");
			vm_tax_token_parentDTO.modifiedBy = rs.getString("modified_by");
			vm_tax_token_parentDTO.insertionDate = rs.getLong("insertion_date");
			vm_tax_token_parentDTO.isDeleted = rs.getInt("isDeleted");
			vm_tax_token_parentDTO.lastModificationTime = rs.getLong("lastModificationTime");
			return vm_tax_token_parentDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}

	public String getSqlWithSearchCriteriaForNormalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
			UserDTO userDTO, String filter, boolean tableHasJobCat, List<Object> objectList)
    {
		boolean viewAll = false;
		
		if(p_searchCriteria != null && p_searchCriteria.get("ViewAll") != null)
		{
			System.out.println("ViewAll = " + p_searchCriteria.get("ViewAll"));
			viewAll = true;
		}
		else
		{
			System.out.println("ViewAll is null ");
		}
		
		String sql = "SELECT ";
		if(category == GETIDS)
		{
			sql += " " + tableName + ".ID as ID ";
		}
		else if(category == GETCOUNT)
		{
			sql += " count( " + tableName + ".ID) as countID ";
		}
		else if(category == GETDTOS)
		{
			sql += " distinct " + tableName + ".* ";
		}
		sql += "FROM " + tableName + " ";
		sql += joinSQL;
		

		
		String AnyfieldSql = "";
		String AllFieldSql = "";
		
		if(p_searchCriteria != null)
		{
						
			
			Enumeration names = p_searchCriteria.keys();
			String str, value;
			
			AnyfieldSql = "(";
			
			if(p_searchCriteria.get("AnyField")!= null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase(""))
			{
				//AnyfieldSql+= tableName + ".search_column like '%" + p_searchCriteria.get("AnyField").toString() + "%'";
				AnyfieldSql+= tableName + ".search_column like ? ";
				objectList.add("%" + p_searchCriteria.get("AnyField").toString() + "%");
			}
			AnyfieldSql += ")";
			System.out.println("AnyfieldSql = " + AnyfieldSql);
			
			AllFieldSql = "(";
			int i = 0;
			while(names.hasMoreElements())
			{				
				str = (String) names.nextElement();
				value = (String)p_searchCriteria.get(str);
		        System.out.println(str + ": " + value);
				if(value != null && !value.equalsIgnoreCase("") && (
						 str.equals("insertion_date_start")
						|| str.equals("insertion_date_end")
						|| str.equals("fiscal_year_id")
				)
						
				)
				{
					if(p_searchCriteria.get(str).equals("any"))
					{
						continue;
					}

					if( i > 0)
					{
						AllFieldSql+= " AND  ";
					}
					
					 if(str.equals("insertion_date_start"))
					{
						//AllFieldSql += "" + tableName + ".insertion_date >= " + p_searchCriteria.get(str);
						AllFieldSql += "" + tableName + ".insertion_date >= ? ";
						objectList.add(p_searchCriteria.get(str));
						i ++;
					}
					else if(str.equals("insertion_date_end"))
					{
						//AllFieldSql += "" + tableName + ".insertion_date <= " + p_searchCriteria.get(str);
						AllFieldSql += "" + tableName + ".insertion_date <= ? ";
						objectList.add(p_searchCriteria.get(str));
						i ++;
					}
					if(str.equals("fiscal_year_id"))
					{
						//AllFieldSql += "" + tableName + ".fiscal_year_id like '%" + p_searchCriteria.get(str) + "%'";
						//AllFieldSql += "" + tableName + ".fiscal_year_id = '" + p_searchCriteria.get(str) + "'";
						AllFieldSql += "" + tableName + ".fiscal_year_id = ? ";
						objectList.add(p_searchCriteria.get(str));
						i ++;
					}
					
					
				}
			}
			
			AllFieldSql += ")";
			System.out.println("AllFieldSql = " + AllFieldSql);
			
			
		}
		
		
		sql += " WHERE ";
		
		sql += " (" + tableName + ".isDeleted = 0 ";			
		sql += ")";
		
		
		if(!filter.equalsIgnoreCase(""))
		{
			sql += " and " + filter + " ";
		}
		
		if(!AnyfieldSql.equals("()") && !AnyfieldSql.equals(""))
		{
			sql += " AND " + AnyfieldSql;
			
		}
		if(!AllFieldSql.equals("()") && !AllFieldSql.equals(""))
		{			
			sql += " AND " + AllFieldSql;
		}
		
	
			
		sql += " order by " + tableName + ".lastModificationTime desc ";

		printSql(sql);
		
		if(limit >= 0)
		{
			sql += " limit " + limit;
		}
		if(offset >= 0)
		{
			sql += " offset " + offset;
		}
		
		System.out.println("-------------- sql = " + sql);
		
		return sql;
    }
	


	public List<Vm_tax_token_parentDTO> getDTOsByFiscalYearID (long fiscalYearID)
	{
		//String sql = "SELECT * FROM " + tableName + " WHERE fiscal_year_id = " + fiscalYearID;
		String sql = "SELECT * FROM " + tableName + " WHERE fiscal_year_id = ? " ;
		sql += " ORDER BY vehicle_type_cat ASC ";
		printSql(sql);
		return ConnectionAndStatementUtil.getListOfT(sql, Arrays.asList(fiscalYearID),this::build);
		//return ConnectionAndStatementUtil.getListOfT(sql,this::build);
	}

	public long deleteByFiscalYearID(long parentId, String parentColName) throws Exception {
		long lastModificationTime = System.currentTimeMillis();
		StringBuilder sqlBuilder = new StringBuilder("UPDATE ")
				.append(tableName)
				.append(" SET isDeleted=1,lastModificationTime=")
				.append(lastModificationTime)
				.append(" WHERE " + parentColName + " = ")
				.append(parentId);
		ConnectionAndStatementUtil.getWriteConnectionAndStatement(model -> {
			String sql = sqlBuilder.toString();
			Connection connection = model.getConnection();
			Statement stmt = model.getStatement();
			try {
				logger.debug(sql);
				stmt.execute(sql);
				recordUpdateTime(connection, lastModificationTime);
			} catch (SQLException ex) {
				logger.error(ex);
			}
		});
		return parentId;
	}
				
}
	