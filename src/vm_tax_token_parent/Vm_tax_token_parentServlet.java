package vm_tax_token_parent;

import java.io.IOException;
import java.io.*;
import java.text.SimpleDateFormat;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import org.apache.log4j.Logger;

import login.LoginDTO;
import permission.MenuConstants;
import role.PermissionRepository;


import sessionmanager.SessionConstants;

import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;
import util.CommonRequestHandler;
import util.RecordNavigationManager4;

import java.util.*;
import javax.servlet.http.*;
import java.util.UUID;





import geolocation.GeoLocationDAO2;
import java.util.StringTokenizer;
import java.util.stream.Collectors;

import com.google.gson.Gson;

import pb.*;
import pbReport.*;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import vm_tax_token.VmTaxTokenItemDAO;
import vm_tax_token.Vm_tax_tokenDAO;


/**
 * Servlet implementation class Vm_tax_token_parentServlet
 */
@WebServlet("/Vm_tax_token_parentServlet")
@MultipartConfig
public class Vm_tax_token_parentServlet extends HttpServlet
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Vm_tax_token_parentServlet.class);

    String tableName = "vm_tax_token_parent";

	Vm_tax_token_parentDAO vm_tax_token_parentDAO;

	Vm_tax_tokenDAO vm_tax_tokenDAO;

	VmTaxTokenItemDAO vmTaxTokenItemDAO;

	CommonRequestHandler commonRequestHandler;
    private final Gson gson = new Gson();

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Vm_tax_token_parentServlet()
	{
        super();
    	try
    	{
			vm_tax_token_parentDAO = new Vm_tax_token_parentDAO(tableName);

			vmTaxTokenItemDAO = new VmTaxTokenItemDAO("vm_tax_token_item");

			vm_tax_tokenDAO =   new Vm_tax_tokenDAO("vm_tax_token");

			commonRequestHandler = new CommonRequestHandler(vm_tax_token_parentDAO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		System.out.println("In doget request = " + request);
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		try
		{
			String actionType = request.getParameter("actionType");
			if(actionType.equals("getAddPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_TAX_TOKEN_PARENT_ADD))
				{
					commonRequestHandler.getAddPage(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getEditPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_TAX_TOKEN_PARENT_UPDATE))
				{
					getVm_tax_token_parent(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getURL"))
			{
				String URL = request.getParameter("URL");
				System.out.println("URL = " + URL);
				response.sendRedirect(URL);
			}
			else if(actionType.equals("search"))
			{
				System.out.println("search requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_TAX_TOKEN_PARENT_SEARCH))
				{
					if(isPermanentTable)
					{
						String filter = request.getParameter("filter");
						System.out.println("filter = " + filter);
						if(filter!=null)
						{
							filter = ""; //shouldn't be directly used, rather manipulate it.
							searchVm_tax_token_parent(request, response, isPermanentTable, filter);
						}
						else
						{
							searchVm_tax_token_parent(request, response, isPermanentTable, "");
						}
					}
					else
					{
						//searchVm_tax_token_parent(request, response, tempTableName, isPermanentTable);
					}
				}
			}
			else if(actionType.equals("view"))
			{
				System.out.println("view requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_TAX_TOKEN_PARENT_SEARCH))
				{
					commonRequestHandler.view(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}
			else if(actionType.equals("AllVehicleSearch"))
			{
				System.out.println("AllVehicleSearch requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_TAX_TOKEN_SEARCH))
				{
					if(isPermanentTable)
					{
						String filter = request.getParameter("filter");
						String fiscalYearID = request.getParameter("fiscalYearID");
						System.out.println("fiscalYearID in doget = " + fiscalYearID);
						if(filter!=null)
						{
							filter = ""; //shouldn't be directly used, rather manipulate it.
							allVehicleSearchVm_tax_token(request, response, isPermanentTable, filter);
						}
						else
						{
							allVehicleSearchVm_tax_token(request, response, isPermanentTable, "");
						}
					}
					else
					{
						//searchVm_tax_token(request, response, tempTableName, isPermanentTable);
					}
				}
			}
			else
			{
				request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}





	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		// TODO Auto-generated method stub
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		System.out.println("doPost");
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);

		try
		{
			String actionType = request.getParameter("actionType");
			System.out.println("actionType = " + actionType);
			if(actionType.equals("add"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_TAX_TOKEN_PARENT_ADD))
				{
					System.out.println("going to  addVm_tax_token_parent ");
					addVm_tax_token_parent(request, response, true, userDTO, true);
				}
				else
				{
					System.out.println("Not going to  addVm_tax_token_parent ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}

			else if(actionType.equals("getDTO"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_TAX_TOKEN_PARENT_ADD))
				{
					getDTO(request, response);
				}
				else
				{
					System.out.println("Not going to  addVm_tax_token_parent ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}
			else if(actionType.equals("edit"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_TAX_TOKEN_PARENT_UPDATE))
				{
					addVm_tax_token_parent(request, response, false, userDTO, isPermanentTable);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("delete"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_TAX_TOKEN_PARENT_UPDATE))
				{
					//commonRequestHandler.delete(request, response, userDTO);
					//commonRequestHandler.delete(request, response, userDTO);
					vm_tax_tokenDAO.deleteByFiscalYearID(Long.parseLong(request.getParameter("fiscalYearId")),"fiscal_year_id");
					vmTaxTokenItemDAO.deleteByFiscalYearID(Long.parseLong(request.getParameter("fiscalYearId")),"fiscalYearId");
					vm_tax_token_parentDAO.deleteByFiscalYearID(Long.parseLong(request.getParameter("fiscalYearId")),"fiscal_year_id");

					response.sendRedirect("Vm_tax_token_parentServlet?actionType=search");
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("search"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_TAX_TOKEN_PARENT_SEARCH))
				{
					searchVm_tax_token_parent(request, response, true, "");
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getGeo"))
			{
				System.out.println("going to geoloc ");
				request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
			}

		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}

	private void getDTO(HttpServletRequest request, HttpServletResponse response)
	{
		try
		{
			System.out.println("In getDTO");
			Vm_tax_token_parentDTO vm_tax_token_parentDTO = (Vm_tax_token_parentDTO)vm_tax_token_parentDAO.getDTOByID(Long.parseLong(request.getParameter("ID")));
			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");

			String encoded = this.gson.toJson(vm_tax_token_parentDTO);
			System.out.println("json encoded data = " + encoded);
			out.print(encoded);
			out.flush();
		}
		catch (NumberFormatException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void addVm_tax_token_parent(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, boolean isPermanentTable) throws IOException
	{
		// TODO Auto-generated method stub
		try
		{
			request.setAttribute("failureMessage", "");
			System.out.println("%%%% addVm_tax_token_parent");
			String path = getServletContext().getRealPath("/img2/");
			Vm_tax_token_parentDTO vm_tax_token_parentDTO = null;
			SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");

			String[] paramValues = request.getParameterValues("vehicleTypeCat");
			System.out.println("paramValues len2: "+paramValues.length);
			long returnedID = -1;

//			int[] nums = new int[paramValues.length];
//			for (int i = 0; i < paramValues.length; i++){
//				 nums[i] = Integer.parseInt(request.getParameterValues("vehicleTypeCat")[i]);
//			}
//
//
//			long returnedID = -1;
//
//
//			Map<Integer, Long> dups = Arrays.stream(nums)
//					.boxed()
//					.collect(Collectors.groupingBy(a -> a, Collectors.counting()));
//			//dups.forEach((k,v)-> System.out.println(k + " -> " + v));
//
//
//			int paramIndex=0;
//			for(Map.Entry<Integer,Long> entry : dups.entrySet()){
//				//paramIndex++;
//				if(addFlag == true)
//				{
//					vm_tax_token_parentDTO = new Vm_tax_token_parentDTO();
//				}
//				else
//				{
//					vm_tax_token_parentDTO = (Vm_tax_token_parentDTO)vm_tax_token_parentDAO.getDTOByID(Long.parseLong(request.getParameter("iD")));
//				}
//
//				String Value = "";
//
//				Value = request.getParameterValues("fiscalYearId")[paramIndex];
//
//				if(Value != null && !Value.equalsIgnoreCase(""))
//				{
//					Value = Jsoup.clean(Value,Whitelist.simpleText());
//				}
//				System.out.println("fiscalYearId = " + Value);
//				if(Value != null && !Value.equalsIgnoreCase(""))
//				{
//					vm_tax_token_parentDTO.fiscalYearId = Long.parseLong(Value);
//				}
//				else
//				{
//					System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//				}
//
//				Value = request.getParameterValues("totalTaxTokenFees")[paramIndex];
//
//				if(Value != null && !Value.equalsIgnoreCase(""))
//				{
//					Value = Jsoup.clean(Value,Whitelist.simpleText());
//				}
//				System.out.println("totalTaxTokenFees = " + Value);
//				if(Value != null && !Value.equalsIgnoreCase(""))
//				{
//					vm_tax_token_parentDTO.totalTaxTokenFees = Double.parseDouble(Value)*entry.getValue();
//				}
//				else
//				{
//					System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//				}
//
//				Value = request.getParameterValues("totalFitnessFees")[paramIndex];
//
//				if(Value != null && !Value.equalsIgnoreCase(""))
//				{
//					Value = Jsoup.clean(Value,Whitelist.simpleText());
//				}
//				System.out.println("totalFitnessFees = " + Value);
//				if(Value != null && !Value.equalsIgnoreCase(""))
//				{
//					vm_tax_token_parentDTO.totalFitnessFees = Double.parseDouble(Value)*entry.getValue();
//				}
//				else
//				{
//					System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//				}
//
//				Value = request.getParameterValues("totalFees")[entry.getKey()];
//
//				if(Value != null && !Value.equalsIgnoreCase(""))
//				{
//					Value = Jsoup.clean(Value,Whitelist.simpleText());
//				}
//				System.out.println("totalFees = " + Value);
//				if(Value != null && !Value.equalsIgnoreCase(""))
//				{
//					vm_tax_token_parentDTO.totalFees = Double.parseDouble(Value)*entry.getValue();
//				}
//				else
//				{
//					System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//				}
//
//				if(addFlag)
//				{
//					Calendar c = Calendar.getInstance();
//					c.set(Calendar.HOUR_OF_DAY, 0);
//					c.set(Calendar.MINUTE, 0);
//					c.set(Calendar.SECOND, 0);
//					c.set(Calendar.MILLISECOND, 0);
//
//					vm_tax_token_parentDTO.insertionDate = c.getTimeInMillis();
//				}
//
//
//				System.out.println("Done adding  addVm_tax_token_parent dto = " + vm_tax_token_parentDTO);
//
//
//				if(isPermanentTable == false) //add new row for validation and make the old row outdated
//				{
//					vm_tax_token_parentDAO.setIsDeleted(vm_tax_token_parentDTO.iD, CommonDTO.OUTDATED);
//					returnedID = vm_tax_token_parentDAO.add(vm_tax_token_parentDTO);
//					vm_tax_token_parentDAO.setIsDeleted(returnedID, CommonDTO.WAITING_FOR_APPROVAL);
//				}
//				else if(addFlag == true)
//				{
//					returnedID = vm_tax_token_parentDAO.manageWriteOperations(vm_tax_token_parentDTO, SessionConstants.INSERT, -1, userDTO);
//				}
//				else
//				{
//					returnedID = vm_tax_token_parentDAO.manageWriteOperations(vm_tax_token_parentDTO, SessionConstants.UPDATE, -1, userDTO);
//				}
//				paramIndex = (int) (paramIndex+ entry.getValue());
//			}



			for (int i = 0; i < paramValues.length; i++){

				if(addFlag == true && i==0)
				{
					vm_tax_token_parentDTO = new Vm_tax_token_parentDTO();
					vm_tax_token_parentDTO.totalTaxTokenFees = 0;
					vm_tax_token_parentDTO.totalFitnessFees = 0;
					vm_tax_token_parentDTO.totalFees = 0;
					//vm_tax_token_parentDTO.fiscalYearId = 0;

				}
				else
				{
					//vm_tax_token_parentDTO = (Vm_tax_token_parentDTO)vm_tax_token_parentDAO.getDTOByID(Long.parseLong(request.getParameter("iD")));
				}

				String Value = "";

				Value = request.getParameterValues("fiscalYearId")[i];

				if(Value != null && !Value.equalsIgnoreCase(""))
				{
					Value = Jsoup.clean(Value,Whitelist.simpleText());
				}
				System.out.println("fiscalYearId = " + Value);
				if(Value != null && !Value.equalsIgnoreCase(""))
				{
					vm_tax_token_parentDTO.fiscalYearId = Long.parseLong(Value);
				}
				else
				{
					System.out.println("FieldName has a null Value, not updating" + " = " + Value);
				}

				Value = request.getParameterValues("totalTaxTokenFees")[i];

				if(Value != null && !Value.equalsIgnoreCase(""))
				{
					Value = Jsoup.clean(Value,Whitelist.simpleText());
				}
				System.out.println("totalTaxTokenFees = " + Value);
				if(Value != null && !Value.equalsIgnoreCase(""))
				{
					vm_tax_token_parentDTO.totalTaxTokenFees += Double.parseDouble(Value);
				}
				else
				{
					System.out.println("FieldName has a null Value, not updating" + " = " + Value);
				}

				Value = request.getParameterValues("totalFitnessFees")[i];

				if(Value != null && !Value.equalsIgnoreCase(""))
				{
					Value = Jsoup.clean(Value,Whitelist.simpleText());
				}
				System.out.println("totalFitnessFees = " + Value);
				if(Value != null && !Value.equalsIgnoreCase(""))
				{
					vm_tax_token_parentDTO.totalFitnessFees += Double.parseDouble(Value);
				}
				else
				{
					System.out.println("FieldName has a null Value, not updating" + " = " + Value);
				}

				Value = request.getParameterValues("totalFees")[i];

				if(Value != null && !Value.equalsIgnoreCase(""))
				{
					Value = Jsoup.clean(Value,Whitelist.simpleText());
				}
				System.out.println("totalFees = " + Value);
				if(Value != null && !Value.equalsIgnoreCase(""))
				{
					vm_tax_token_parentDTO.totalFees += Double.parseDouble(Value);
				}
				else
				{
					System.out.println("FieldName has a null Value, not updating" + " = " + Value);
				}

				if(addFlag)
				{
					Calendar c = Calendar.getInstance();
					c.set(Calendar.HOUR_OF_DAY, 0);
					c.set(Calendar.MINUTE, 0);
					c.set(Calendar.SECOND, 0);
					c.set(Calendar.MILLISECOND, 0);

					vm_tax_token_parentDTO.insertionDate = c.getTimeInMillis();
				}




			}

			


			System.out.println("Done adding  addVm_tax_token_parent dto = " + vm_tax_token_parentDTO);
			//long returnedID = -1;

			if(isPermanentTable == false) //add new row for validation and make the old row outdated
			{
				vm_tax_token_parentDAO.setIsDeleted(vm_tax_token_parentDTO.iD, CommonDTO.OUTDATED);
				returnedID = vm_tax_token_parentDAO.add(vm_tax_token_parentDTO);
				vm_tax_token_parentDAO.setIsDeleted(returnedID, CommonDTO.WAITING_FOR_APPROVAL);
			}
			else if(addFlag == true)
			{
				returnedID = vm_tax_token_parentDAO.manageWriteOperations(vm_tax_token_parentDTO, SessionConstants.INSERT, -1, userDTO);
			}
			else
			{
				returnedID = vm_tax_token_parentDAO.manageWriteOperations(vm_tax_token_parentDTO, SessionConstants.UPDATE, -1, userDTO);
			}









			if(isPermanentTable)
			{
				String inPlaceSubmit = request.getParameter("inplacesubmit");

				if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
				{
					getVm_tax_token_parent(request, response, returnedID);
				}
				else
				{
					response.sendRedirect("Vm_tax_token_parentServlet?actionType=search");
				}
			}
			else
			{
				commonRequestHandler.validate(vm_tax_token_parentDAO.getDTOByID(returnedID), request, response, userDTO);
			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}









	private void getVm_tax_token_parent(HttpServletRequest request, HttpServletResponse response, long id) throws ServletException, IOException
	{
		System.out.println("in getVm_tax_token_parent");
		Vm_tax_token_parentDTO vm_tax_token_parentDTO = null;
		try
		{
			vm_tax_token_parentDTO = (Vm_tax_token_parentDTO)vm_tax_token_parentDAO.getDTOByID(id);
			request.setAttribute("ID", vm_tax_token_parentDTO.iD);
			request.setAttribute("vm_tax_token_parentDTO",vm_tax_token_parentDTO);
			request.setAttribute("vm_tax_token_parentDAO",vm_tax_token_parentDAO);

			String URL= "";

			String inPlaceEdit = request.getParameter("inplaceedit");
			String inPlaceSubmit = request.getParameter("inplacesubmit");
			String getBodyOnly = request.getParameter("getBodyOnly");

			if(inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase(""))
			{
				URL = "vm_tax_token_parent/vm_tax_token_parentInPlaceEdit.jsp";
				request.setAttribute("inplaceedit","");
			}
			else if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
			{
				URL = "vm_tax_token_parent/vm_tax_token_parentSearchRow.jsp";
				request.setAttribute("inplacesubmit","");
			}
			else
			{
				if(getBodyOnly  != null && !getBodyOnly.equalsIgnoreCase(""))
				{
					URL = "vm_tax_token_parent/vm_tax_token_parentEditBody.jsp?actionType=edit";
				}
				else
				{
					URL = "vm_tax_token_parent/vm_tax_token_parentEdit.jsp?actionType=edit";
				}
			}

			RequestDispatcher rd = request.getRequestDispatcher(URL);
			rd.forward(request, response);
		}
		catch (NumberFormatException e)
		{
			e.printStackTrace();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}


	private void getVm_tax_token_parent(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		getVm_tax_token_parent(request, response, Long.parseLong(request.getParameter("ID")));
	}

	private void searchVm_tax_token_parent(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException
	{
		System.out.println("in  searchVm_tax_token_parent 1");
		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String ajax = request.getParameter("ajax");
		boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);

        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
			SessionConstants.NAV_VM_TAX_TOKEN_PARENT,
			request,
			vm_tax_token_parentDAO,
			SessionConstants.VIEW_VM_TAX_TOKEN_PARENT,
			SessionConstants.SEARCH_VM_TAX_TOKEN_PARENT,
			tableName,
			isPermanent,
			userDTO,
			filter,
			true);
        try
        {
			System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        }
        catch(Exception e)
        {
			System.out.println("failed to dojob" + e);
        }

		request.setAttribute("vm_tax_token_parentDAO",vm_tax_token_parentDAO);
        RequestDispatcher rd;
        if(!isPermanent)
        {
        	if(hasAjax == false)
	        {
	        	System.out.println("Going to vm_tax_token_parent/vm_tax_token_parentApproval.jsp");
	        	rd = request.getRequestDispatcher("vm_tax_token_parent/vm_tax_token_parentApproval.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to vm_tax_token_parent/vm_tax_token_parentApprovalForm.jsp");
	        	rd = request.getRequestDispatcher("vm_tax_token_parent/vm_tax_token_parentApprovalForm.jsp");
	        }
        }
        else
        {
	        if(hasAjax == false)
	        {
	        	System.out.println("Going to vm_tax_token_parent/vm_tax_token_parentSearch.jsp");
	        	rd = request.getRequestDispatcher("vm_tax_token_parent/vm_tax_token_parentSearch.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to vm_tax_token_parent/vm_tax_token_parentSearchForm.jsp");
	        	rd = request.getRequestDispatcher("vm_tax_token_parent/vm_tax_token_parentSearchForm.jsp");
	        }
        }
		rd.forward(request, response);
	}

	private void allVehicleSearchVm_tax_token(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException
	{
		System.out.println("in  allVehicleSearchVm_tax_token 1");
		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String ajax = request.getParameter("ajax");
		boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);

		RequestDispatcher rd;
		if(!isPermanent)
		{
			if(hasAjax == false)
			{
				System.out.println("Going to vm_tax_token/vm_tax_tokenApproval.jsp");
				rd = request.getRequestDispatcher("vm_tax_token/vm_tax_tokenApproval.jsp");
			}
			else
			{
				System.out.println("Going to vm_tax_token/vm_tax_tokenApprovalForm.jsp");
				rd = request.getRequestDispatcher("vm_tax_token/vm_tax_tokenApprovalForm.jsp");
			}
		}
		else
		{
			if(hasAjax == false)
			{
				System.out.println("Going to vm_tax_token_parent/vm_tax_tokenAllVehicleSearch.jsp");
				rd = request.getRequestDispatcher("vm_tax_token_parent/vm_tax_tokenAllVehicleSearch.jsp");
			}
			else
			{
				System.out.println("Going to vm_tax_token_parent/vm_tax_tokenAllVehicleSearchForm.jsp");
				rd = request.getRequestDispatcher("vm_tax_token_parent/vm_tax_tokenAllVehicleSearchForm.jsp");
			}
		}
		rd.forward(request, response);
	}

}

