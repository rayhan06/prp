package vm_tax_token_parent;
import java.util.*; 
import util.*; 


public class Vm_tax_token_parentDTO extends CommonDTO
{

	public long fiscalYearId = -1;
	public double totalTaxTokenFees = -1;
	public double totalFitnessFees = -1;
	public double totalFees = -1;
    public String insertedBy = "";
    public String modifiedBy = "";
	public long insertionDate = -1;
	
	
    @Override
	public String toString() {
            return "$Vm_tax_token_parentDTO[" +
            " iD = " + iD +
            " fiscalYearId = " + fiscalYearId +
            " totalTaxTokenFees = " + totalTaxTokenFees +
            " totalFitnessFees = " + totalFitnessFees +
            " totalFees = " + totalFees +
            " searchColumn = " + searchColumn +
            " insertedBy = " + insertedBy +
            " modifiedBy = " + modifiedBy +
            " insertionDate = " + insertionDate +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}