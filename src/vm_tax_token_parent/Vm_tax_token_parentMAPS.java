package vm_tax_token_parent;
import java.util.*; 
import util.*;


public class Vm_tax_token_parentMAPS extends CommonMaps
{	
	public Vm_tax_token_parentMAPS(String tableName)
	{
		

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("fiscalYearId".toLowerCase(), "fiscalYearId".toLowerCase());
		java_DTO_map.put("totalTaxTokenFees".toLowerCase(), "totalTaxTokenFees".toLowerCase());
		java_DTO_map.put("totalFitnessFees".toLowerCase(), "totalFitnessFees".toLowerCase());
		java_DTO_map.put("totalFees".toLowerCase(), "totalFees".toLowerCase());
		java_DTO_map.put("searchColumn".toLowerCase(), "searchColumn".toLowerCase());
		java_DTO_map.put("insertedBy".toLowerCase(), "insertedBy".toLowerCase());
		java_DTO_map.put("modifiedBy".toLowerCase(), "modifiedBy".toLowerCase());
		java_DTO_map.put("insertionDate".toLowerCase(), "insertionDate".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("fiscal_year_id".toLowerCase(), "fiscalYearId".toLowerCase());
		java_SQL_map.put("total_tax_token_fees".toLowerCase(), "totalTaxTokenFees".toLowerCase());
		java_SQL_map.put("total_fitness_fees".toLowerCase(), "totalFitnessFees".toLowerCase());
		java_SQL_map.put("total_fees".toLowerCase(), "totalFees".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Fiscal Year Id".toLowerCase(), "fiscalYearId".toLowerCase());
		java_Text_map.put("Total Tax Token Fees".toLowerCase(), "totalTaxTokenFees".toLowerCase());
		java_Text_map.put("Total Fitness Fees".toLowerCase(), "totalFitnessFees".toLowerCase());
		java_Text_map.put("Total Fees".toLowerCase(), "totalFees".toLowerCase());
		java_Text_map.put("Search Column".toLowerCase(), "searchColumn".toLowerCase());
		java_Text_map.put("Inserted By".toLowerCase(), "insertedBy".toLowerCase());
		java_Text_map.put("Modified By".toLowerCase(), "modifiedBy".toLowerCase());
		java_Text_map.put("Insertion Date".toLowerCase(), "insertionDate".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}