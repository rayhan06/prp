package centre;

import java.io.IOException;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import login.LoginDTO;
import permission.MenuConstants;
import role.PermissionRepository;


import sessionmanager.SessionConstants;

import user.UserDTO;
import user.UserRepository;

import util.RecordNavigationManager;

import java.io.*;
import javax.servlet.http.*;
import java.util.UUID;

import centre.Constants;


import geolocation.GeoLocationDAO2;
import java.util.StringTokenizer;


/**
 * Servlet implementation class CentreServlet
 */
@WebServlet("/CentreServlet")
@MultipartConfig
public class CentreServlet extends HttpServlet 
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(CentreServlet.class);
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CentreServlet() 
	{
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		

		try
		{
			String actionType = request.getParameter("actionType");
			/*if(actionType.equals("getAddPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.CENTRE_ADD))
				{
					getAddPage(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getEditPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.CENTRE_UPDATE))
				{
					getCentre(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}				
			}
			else if(actionType.equals("getURL"))
			{
				String URL = request.getParameter("URL");
				logger.debug("URL = " + URL);
				response.sendRedirect(URL);			
			}
			else if(actionType.equals("search"))
			{
				logger.debug("search requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.CENTRE_SEARCH))
				{
					searchCentre(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}*/
			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}

	private void getAddPage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		request.setAttribute("ID", -1L);
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("centre/centreEdit.jsp");
		requestDispatcher.forward(request, response);
	}
	
	private String getFileName(final Part part) 
	{
	    final String partHeader = part.getHeader("content-disposition");
	    logger.debug("Part Header = {0}" +  partHeader);
	    for (String content : part.getHeader("content-disposition").split(";")) {
	        if (content.trim().startsWith("filename")) {
	            return content.substring(
	                    content.indexOf('=') + 1).trim().replace("\"", "");
	        }
	    }
	    return null;
	}
	
	

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		// TODO Auto-generated method stub
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		

		logger.debug("doPost");
		
		try
		{
			String actionType = request.getParameter("actionType");
			logger.debug("actionType = " + actionType);
			/*if(actionType.equals("add"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.CENTRE_ADD))
				{
					logger.debug("going to  addCentre ");
					addCentre(request, response, true);
				}
				else
				{
					logger.debug("Not going to  addCentre ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else if(actionType.equals("edit"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.CENTRE_UPDATE))
				{
					addCentre(request, response, false);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("delete"))
			{
				deleteCentre(request, response);
			}
			else if(actionType.equals("search"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.CENTRE_SEARCH))
				{
					searchCentre(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getGeo"))
			{
				logger.debug("going to geoloc ");
				request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
			}*/
			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}

	private void addCentre(HttpServletRequest request, HttpServletResponse response, Boolean addFlag) throws IOException 
	{
		// TODO Auto-generated method stub
		try 
		{
			request.setAttribute("failureMessage", "");
			logger.debug("%%%% addCentre");
			CentreDAO centreDAO = new CentreDAO();
			CentreDTO centreDTO;
			String FileNamePrefix;
			if(addFlag == true)
			{
				centreDTO = new CentreDTO();
				FileNamePrefix = UUID.randomUUID().toString().substring(0, 10);
			}
			else
			{
				centreDTO = centreDAO.getCentreDTOByID(Long.parseLong(request.getParameter("identity")));
				FileNamePrefix = request.getParameter("identity");
			}
			
			String Value = "";
			Value = request.getParameter("iD");
			logger.debug("iD = " + Value);
			if( addFlag == true ||(Value != null && !Value.equalsIgnoreCase("")))
			{
				centreDTO.iD = Long.parseLong(Value);
			}
			else
			{
				logger.debug("FieldName has a null value, not updating" + " = " + Value);
			}
			Value = request.getParameter("nameEn");
			logger.debug("nameEn = " + Value);
			if( addFlag == true ||(Value != null && !Value.equalsIgnoreCase("")))
			{
				centreDTO.nameEn = (Value);
			}
			else
			{
				logger.debug("FieldName has a null value, not updating" + " = " + Value);
			}
			Value = request.getParameter("nameBn");
			logger.debug("nameBn = " + Value);
			if( addFlag == true ||(Value != null && !Value.equalsIgnoreCase("")))
			{
				centreDTO.nameBn = (Value);
			}
			else
			{
				logger.debug("FieldName has a null value, not updating" + " = " + Value);
			}
			Value = request.getParameter("address");
			logger.debug("address = " + Value);
			if( addFlag == true ||(Value != null && !Value.equalsIgnoreCase("")))
			{
				
				{
					StringTokenizer tok3=new StringTokenizer(Value, ":");
					int i = 0;
					String addressDetails = "", address_id = "";
					while(tok3.hasMoreElements())
					{
						if(i == 0)
						{
							address_id = tok3.nextElement() + "";
						}
						else if(i == 1)
						{
							addressDetails = tok3.nextElement() + "";
						}
						i ++;
					}
					centreDTO.address = address_id + ":" + GeoLocationDAO2.getLocationText(Integer.parseInt(address_id))
					+ ":" + addressDetails;
				}
				
			}
			else
			{
				logger.debug("FieldName has a null value, not updating" + " = " + Value);
			}
			Value = request.getParameter("contactPerson");
			logger.debug("contactPerson = " + Value);
			if( addFlag == true ||(Value != null && !Value.equalsIgnoreCase("")))
			{
				centreDTO.contactPerson = (Value);
			}
			else
			{
				logger.debug("FieldName has a null value, not updating" + " = " + Value);
			}
			Value = request.getParameter("phoneNumber");
			logger.debug("phoneNumber = " + Value);
			if( addFlag == true ||(Value != null && !Value.equalsIgnoreCase("")))
			{
				centreDTO.phoneNumber = (Value);
			}
			else
			{
				logger.debug("FieldName has a null value, not updating" + " = " + Value);
			}
			Value = request.getParameter("email");
			logger.debug("email = " + Value);
			if( addFlag == true ||(Value != null && !Value.equalsIgnoreCase("")))
			{
				centreDTO.email = (Value);
			}
			else
			{
				logger.debug("FieldName has a null value, not updating" + " = " + Value);
			}
			Value = request.getParameter("isDeleted");
			logger.debug("isDeleted = " + Value);
			if( addFlag == true ||(Value != null && !Value.equalsIgnoreCase("")))
			{
				centreDTO.isDeleted = Integer.parseInt(Value);
			}
			else
			{
				logger.debug("FieldName has a null value, not updating" + " = " + Value);
			}
			Value = request.getParameter("description");
			logger.debug("description = " + Value);
			if( addFlag == true ||(Value != null && !Value.equalsIgnoreCase("")))
			{
				centreDTO.description = (Value);
			}
			else
			{
				logger.debug("FieldName has a null value, not updating" + " = " + Value);
			}
			
			logger.debug("Done adding  addCentre dto = " + centreDTO);
			
			if(addFlag == true)
			{
				centreDAO.addCentre(centreDTO);
			}
			else
			{
				centreDAO.updateCentre(centreDTO);
			}
			
			String inPlaceSubmit = request.getParameter("inplacesubmit");
			
			if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
			{
				getCentre(request, response);
			}
			else
			{
				response.sendRedirect("CentreServlet?actionType=search");
			}
					
		}
		catch (Exception e) 
		{
			logger.debug(e);
		}
	}

	private void deleteCentre(HttpServletRequest request, HttpServletResponse response) throws IOException 
	{				
		try 
		{
			String[] IDsToDelete = request.getParameterValues("ID");
			for(int i = 0; i < IDsToDelete.length; i ++)
			{
				long id = Long.parseLong(IDsToDelete[i]);
				logger.debug("###DELETING " + IDsToDelete[i]);				
				new CentreDAO().deleteCentreByID(id);
			}			
		}
		catch (Exception ex) 
		{
			logger.debug(ex);
		}
		response.sendRedirect("CentreServlet?actionType=search");
	}

	private void getCentre(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		logger.debug("in getCentre");
		CentreDTO centreDTO = null;
		try 
		{
			centreDTO = new CentreDAO().getCentreDTOByID(Long.parseLong(request.getParameter("ID")));
			request.setAttribute("ID", centreDTO.iD);
			request.setAttribute("centreDTO",centreDTO);
			
			String URL= "";
			
			String inPlaceEdit = request.getParameter("inplaceedit");
			String inPlaceSubmit = request.getParameter("inplacesubmit");
			
			if(inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase(""))
			{
				URL = "centre/centreInPlaceEdit.jsp";	
				request.setAttribute("inplaceedit","");				
			}
			else if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
			{
				URL = "centre/centreSearchRow.jsp";
				request.setAttribute("inplacesubmit","");					
			}
			else
			{
				URL = "centre/centreEdit.jsp?actionType=edit";
			}
			
			RequestDispatcher rd = request.getRequestDispatcher(URL);
			rd.forward(request, response);
		}
		catch (NumberFormatException e) 
		{
			e.printStackTrace();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	private void searchCentre(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		logger.debug("in  searchCentre 1");
        CentreDAO centreDAO = new CentreDAO();
		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
		String ajax = request.getParameter("ajax");
		boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        logger.debug("ajax = " + ajax + " hasajax = " + hasAjax);
		
        RecordNavigationManager rnManager = new RecordNavigationManager(SessionConstants.NAV_CENTRE, request, centreDAO, SessionConstants.VIEW_CENTRE, SessionConstants.SEARCH_CENTRE);
        try
        {
			logger.debug("trying to dojob");
            rnManager.doJob(loginDTO);
        }
        catch(Exception e)
        {
			logger.debug("failed to dojob" + e);
        }

        RequestDispatcher rd;
        if(hasAjax == false)
        {
        	logger.debug("Going to centre/centreSearch.jsp");
        	rd = request.getRequestDispatcher("centre/centreSearch.jsp");
        }
        else
        {
        	logger.debug("Going to centre/centreSearchForm.jsp");
        	rd = request.getRequestDispatcher("centre/centreSearchForm.jsp");
        }
		rd.forward(request, response);
	}
	
}

