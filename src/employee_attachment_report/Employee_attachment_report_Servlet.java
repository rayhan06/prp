package employee_attachment_report;


import language.LC;
import language.LM;
import login.LoginDTO;
import pbReport.PBReportUtils;
import pbReport.ReportCommonService;
import permission.MenuConstants;
import role.PermissionRepository;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;


@WebServlet("/Employee_attachment_report_Servlet")
public class Employee_attachment_report_Servlet extends HttpServlet implements ReportCommonService {
    private static final long serialVersionUID = 1L;

    private static final Set<String> searchParam = new HashSet<>(Arrays.asList(
            "officeUnitIds", "status", "employeeRecordId", "startDateFrom", "startDateTo", "endDateFrom", "endDateTo"
    ));
    private final Map<String, String[]> stringMap = new HashMap<>();


    public Employee_attachment_report_Servlet() {
        stringMap.put("officeUnitIds", new String[]{"criteria", "ea", "office_unit_id", "IN", "AND", "String", "", "", "any", "officeUnitIdList",
                "officeUnitIds", String.valueOf(LC.EMPLOYEE_ATTACHMENT_REPORT_WHERE_OFFICEUNITID), "office_units", null, "true", "1"});

        stringMap.put("status", new String[]{"criteria", "ea", "status", "=", "AND", "String", "", "", "any", "status",
                "status", String.valueOf(LC.EMPLOYEE_ATTACHMENT_REPORT_WHERE_STATUS), "category", "attachment_status", "true", "2"});

        stringMap.put("employeeRecordId", new String[]{"criteria", "ea", "employee_records_id", "=", "AND", "String", "", "", "any", "employeeRecordId",
                "employeeRecordId", String.valueOf(LC.EMPLOYEE_ATTACHMENT_REPORT_WHERE_EMPLOYEERECORDSID), "employee_records_id", null, "true", "3"});


        stringMap.put("startDateFrom", new String[]{"criteria", "ea", "start_date", ">=", "AND", "long", "", "", "any", "startDateFrom",
                "startDateFrom", String.valueOf(LC.EMPLOYEE_ATTACHMENT_REPORT_WHERE_STARTDATE), "date", null, "true", "4"});

        stringMap.put("startDateTo", new String[]{"criteria", "ea", "start_date", "<=", "AND", "long", "", "", "any", "startDateTo",
                "startDateTo", String.valueOf(LC.EMPLOYEE_ATTACHMENT_REPORT_WHERE_STARTDATE_5), "date", null, "true", "5"});

        stringMap.put("endDateFrom", new String[]{"criteria", "ea", "end_date", ">=", "AND", "long", "", "", "any", "endDateFrom",
                "endDateFrom", String.valueOf(LC.EMPLOYEE_ATTACHMENT_REPORT_WHERE_ENDDATE_FROM), "date", null, "true", "4"});

        stringMap.put("endDateTo", new String[]{"criteria", "ea", "end_date", "<=", "AND", "long", "", "", "any", "endDateTo",
                "endDateTo", String.valueOf(LC.EMPLOYEE_ATTACHMENT_REPORT_WHERE_ENDDATE_TO), "date", null, "true", "5"});

        stringMap.put("isDeleted", new String[]{"criteria", "ea", "isDeleted", "=", "AND", "int", "", "", "0", "isDeleted", null, null, null, null, null, null});
    }

    private String[][] Criteria;
    private final String[][] Display =
            {
                    {"display", "ea", "employee_records_id", "employee_records_id", ""},
                    {"display", "ea", "status", "attachment_status", ""},
                    {"display", "ea", "office_unit_id", "office_unit", ""},
                    {"display", "ea", "start_date", "date", ""},
                    {"display", "ea", "end_date", "date", ""}
            };
    private static final String sql = "employee_attachment ea";

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }

        Display[0][4] = LM.getText(LC.EMPLOYEE_ATTACHMENT_REPORT_SELECT_EMPLOYEERECORDSID, loginDTO);
        Display[1][4] = LM.getText(LC.EMPLOYEE_ATTACHMENT_REPORT_SELECT_STATUS, loginDTO);
        Display[2][4] = LM.getText(LC.EMPLOYEE_ATTACHMENT_REPORT_SELECT_OFFICEUNITID, loginDTO);
        Display[3][4] = LM.getText(LC.EMPLOYEE_ATTACHMENT_REPORT_SELECT_STARTDATE, loginDTO);
        Display[4][4] = LM.getText(LC.EMPLOYEE_ATTACHMENT_REPORT_SELECT_STARTDATE_4, loginDTO);


        Set<String> inputs = PBReportUtils.prepareInputSets(searchParam, request);
        inputs.add("isDeleted");
        Criteria = PBReportUtils.prepareCriteria(inputs, stringMap);
        if (inputs.contains("officeUnitIds")) {
            for (String[] arr : Criteria) {
                if ("officeUnitIdList".equals(arr[9])) {
                    arr[8] = getOfficeIdsFromOfficeUnitIds(request).stream()
                                                                   .map(String::valueOf)
                                                                   .collect(Collectors.joining(","));
                }
            }
        }

        request.setAttribute("dontUseOutDatedModal", true);
        if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.EMPLOYEE_ATTACHMENT_REPORT_DETAILS)) {
            reportGenerate(request, response);
        }
    }

    @Override
    public String[][] getCriteria() {
        return Criteria;
    }

    @Override
    public String[][] getDisplay() {
        return Display;
    }

    @Override
    public String getSQL() {
        return sql;
    }

    @Override
    public int getLCForReportName() {
        return LC.EMPLOYEE_ATTACHMENT_REPORT_OTHER_EMPLOYEE_ATTACHMENT_REPORT;
    }

    @Override
    public String getFileName() {
        return "employee_attachment_report";
    }

    @Override
    public String getTableName() {
        return "employee_attachment_report";
    }
}