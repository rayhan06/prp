package login_report;


import language.LC;
import language.LM;
import login.LoginDTO;
import pbReport.ReportService;
import permission.MenuConstants;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.ReportRequestHandler;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@WebServlet("/Login_report_Servlet")
public class Login_report_Servlet extends HttpServlet {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    String[][] Criteria =
            {
                    {"criteria", "lg", "employee_records_id", "=", "", "long", "", "", "any", "employeeRecordId", LC.LOGIN_REPORT_WHERE_EMPLOYEERECORDSID + ""},
                    {"criteria", "lg", "insertion_date", ">=", "AND", "long", "", "", Long.MIN_VALUE + "", "startDate", LC.HM_START_DATE + ""},
                    {"criteria", "lg", "insertion_date", "<=", "AND", "long", "", "", Long.MAX_VALUE + "", "endDate", LC.HM_END_DATE + ""}
            };

    String[][] Display =
            {
                    {"display", "er", "employee_number", "number", ""},
                    {"display", "lg", "name_en", "name", ""},
                    {"display", "lg", "name_bn", "name", ""},
                    {"display", "lg", "employee_office_id", "employee_office", ""},
                    {"display", "lg", "ip_address", "text", ""},
                    {"display", "lg", "device", "text", ""},
                    {"display", "lg", "browser", "text", ""},
                    {"display", "lg", "Operating_system", "text", ""},
                    {"display", "lg", "isSuccessful", "boolean", ""},
                    {"display", "lg", "error_message", "text", ""},
                    {"display", "lg", "insertion_date", "date", ""}
            };

    String GroupBy = "";
    String OrderBY = "";

    ReportRequestHandler reportRequestHandler;

    public Login_report_Servlet() {

    }

    private final ReportService reportService = new ReportService();

    private String sql;

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        String language = "english";
        if (userDTO != null && userDTO.languageID == SessionConstants.BANGLA) {
            language = "bangla";
        }

        String actionType = request.getParameter("actionType");

        System.out.println("In ssservlet doget, actiontype = " + actionType);

        sql = "login_log lg LEFT JOIN employee_records er ON lg.employee_records_id = er.id";

        Display[0][4] = LM.getText(LC.EMPLOYEE_INFO_REPORT_SELECT_USER_ID, loginDTO);
        Display[1][4] = LM.getText(LC.LOGIN_REPORT_SELECT_NAMEEN, loginDTO);
        Display[2][4] = LM.getText(LC.LOGIN_REPORT_SELECT_NAMEBN, loginDTO);
        Display[3][4] = LM.getText(LC.LOGIN_REPORT_SELECT_EMPLOYEEOFFICEID, loginDTO);
        Display[4][4] = LM.getText(LC.LOGIN_REPORT_SELECT_IPADDRESS, loginDTO);
        Display[5][4] = LM.getText(LC.LOGIN_REPORT_SELECT_DEVICE, loginDTO);
        Display[6][4] = LM.getText(LC.LOGIN_REPORT_SELECT_BROWSER, loginDTO);
        Display[7][4] = LM.getText(LC.LOGIN_REPORT_SELECT_OPERATINGSYSTEM, loginDTO);
        Display[8][4] = LM.getText(LC.LOGIN_REPORT_SELECT_ISSUCCESSFUL, loginDTO);
        Display[9][4] = LM.getText(LC.LOGIN_REPORT_SELECT_ERRORMESSAGE, loginDTO);
        Display[10][4] = LM.getText(LC.LOGIN_REPORT_SELECT_INSERTIONDATE, loginDTO);


        String reportName = LM.getText(LC.LOGIN_REPORT_OTHER_LOGIN_REPORT, loginDTO);

        reportRequestHandler = new ReportRequestHandler(null,
                Criteria, Display, GroupBy, OrderBY, sql,
                reportService);


        reportRequestHandler.handleReportGet(request, response, userDTO, "login_report",
                MenuConstants.LOGIN_REPORT_DETAILS, language, reportName, "login_report");
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }
}
