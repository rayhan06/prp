package category;

import annotation.NotEmpty;
import util.CommonDTO;


public class CategoryDTO extends CommonDTO {

    @NotEmpty(engError = "Please enter domain name",bngError = "অনুগ্রহ করে ডোমেনের নাম লিখুন")
    public String domainName = "";

    @NotEmpty(engError = "Please enter name(english)",bngError = "অনুগ্রহ করে নাম(ইংরেজি) লিখুন")
    public String nameEn = "";

    @NotEmpty(engError = "Please enter name(bangla)",bngError = "অনুগ্রহ করে নাম(বাংলা) লিখুন")
    public String nameBn = "";

    @NotEmpty(engError = "please enter value in digit",bngError = "অনুগ্রহ করে ভ্যালু সংখ্যায় লিখুন")
    public int value = 0;

    public long languageId = 0;//For legacy Purposes. Will be removed soon
    
    public int ordering = 1;
    
    public String parentDomain = "";
    public int parentDomainVal = -1;
    
    
    public CategoryDTO()
    {
    	
    }
    
    public CategoryDTO(String domainName, String nameEn, String nameBn)
    {
    	this.domainName = domainName;
    	this.nameEn = nameEn;
    	this.nameBn = nameBn;
    	value = CategoryDAO.getInstance().getValToUse(domainName);
    }



    @Override
    public String toString() {
        return "$CategoryDTO[" +
                " iD = " + iD +
                " domainName = " + domainName +
                " nameEn = " + nameEn +
                " nameBn = " + nameBn +
                " value = " + value +
                " isDeleted = " + isDeleted +
                " lastModificationTime = " + lastModificationTime +
                "]";
    }

}