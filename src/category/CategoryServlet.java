package category;

import common.BaseServlet;
import common.CacheUpdateModel;
import common.CommonDAOService;
import pb.Utils;
import permission.MenuConstants;
import user.UserDTO;
import util.CommonDTO;
import util.HttpRequestUtils;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;

import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;

@WebServlet("/CategoryServlet")
@MultipartConfig
public class CategoryServlet extends BaseServlet {
    private static final long serialVersionUID = 1L;

    private final CategoryDAO categoryDAO = CategoryDAO.getInstance();

    @Override
    public String getTableName() {
        return categoryDAO.getTableName();
    }

    @Override
    public String getServletName() {
        return "CategoryServlet";
    }

    @Override
    public CommonDAOService<? extends CommonDTO> getCommonDAOService() {
        return categoryDAO;
    }

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        CategoryDTO categoryDTO;
        boolean isLangEng = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
        if (addFlag) {
            categoryDTO = new CategoryDTO();
        } else {
            categoryDTO = categoryDAO.getDTOFromID(Long.parseLong(request.getParameter("iD")));
            if(categoryDTO == null){
                throw new Exception("Category is not found");
            }
        }
        categoryDTO.lastModificationTime = System.currentTimeMillis() + 60000;//1min delay

        validateAndSet(categoryDTO, request, isLangEng);
        CategoryDTO duplicateDTO;
        if(addFlag){
            duplicateDTO = categoryDAO.getByDomainNameAndValueSQL(categoryDTO.domainName,categoryDTO.value);
        }else{
            duplicateDTO = categoryDAO.getByDomainNameAndValueAndNotIdSQL(categoryDTO.domainName,categoryDTO.value, categoryDTO.iD);
        }
        if(duplicateDTO!=null){
            logger.error("duplicate category is found for domain name: "+categoryDTO.domainName+", value : "+categoryDTO.value);
            logger.error(duplicateDTO);
            throw new Exception("Duplicate category value is found");
        }

        if (request.getParameter("ordering") != null) {
            categoryDTO.ordering = Integer.parseInt(request.getParameter("ordering"));
        }
        
        String Value = request.getParameter("parentDomain") ;
        if (Value!= null) {
            categoryDTO.parentDomain = Jsoup.clean(Value,Whitelist.simpleText());
        }
        
        Value = request.getParameter("parentDomainVal") ;
        if (Value!= null) {
            categoryDTO.parentDomainVal = Integer.parseInt(Value);
        }

        CommonDAOService.CACHE_UPDATE_MODEL_THREAD_LOCAL.set(new CacheUpdateModel(categoryDTO.lastModificationTime));

        Utils.handleTransaction(()->{
            if (addFlag) {
                categoryDAO.add(categoryDTO);
            } else {
                categoryDAO.update(categoryDTO);
            }
        });

        Utils.executeCache();

        return categoryDTO;
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[]{MenuConstants.CATEGORY_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[]{MenuConstants.CATEGORY_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[]{MenuConstants.CATEGORY_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return CategoryServlet.class;
    }
}