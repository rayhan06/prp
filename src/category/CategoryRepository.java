package category;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class CategoryRepository implements Repository {
	CategoryDAO categoryDAO = null;
	
	public void setDAO(CategoryDAO categoryDAO)
	{
		this.categoryDAO = categoryDAO;
	}
	
	
	static Logger logger = Logger.getLogger(CategoryRepository.class);
	Map<Long, CategoryDTO>mapOfCategoryDTOToiD;
	Map<String, Set<CategoryDTO> >mapOfCategoryDTOTodomainName;
	Map<String, Set<CategoryDTO> >mapOfCategoryDTOTonameEn;
	Map<String, Set<CategoryDTO> >mapOfCategoryDTOTonameBn;
	Map<Integer, Set<CategoryDTO> >mapOfCategoryDTOTovalue;
	Map<Long, Set<CategoryDTO> >mapOfCategoryDTOTolastModificationTime;


	static CategoryRepository instance = null;  
	private CategoryRepository(){
		mapOfCategoryDTOToiD = new ConcurrentHashMap<>();
		mapOfCategoryDTOTodomainName = new ConcurrentHashMap<>();
		mapOfCategoryDTOTonameEn = new ConcurrentHashMap<>();
		mapOfCategoryDTOTonameBn = new ConcurrentHashMap<>();
		mapOfCategoryDTOTovalue = new ConcurrentHashMap<>();
		mapOfCategoryDTOTolastModificationTime = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static CategoryRepository getInstance(){
		if (instance == null){
			instance = new CategoryRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(categoryDAO == null)
		{
			categoryDAO = CategoryDAO.getInstance();
		}
		if(reloadAll)
		{
			System.out.println("Reloaded all categoryRepo");
		}
		try {
			List<CategoryDTO> categoryDTOs = categoryDAO.getAllDTOs(reloadAll);
			for(CategoryDTO categoryDTO : categoryDTOs) {
				CategoryDTO oldCategoryDTO = getCategoryDTOByID(categoryDTO.iD);
				if( oldCategoryDTO != null ) {
					mapOfCategoryDTOToiD.remove(oldCategoryDTO.iD);
				
					if(mapOfCategoryDTOTodomainName.containsKey(oldCategoryDTO.domainName)) {
						mapOfCategoryDTOTodomainName.get(oldCategoryDTO.domainName).remove(oldCategoryDTO);
					}
					if(mapOfCategoryDTOTodomainName.get(oldCategoryDTO.domainName).isEmpty()) {
						mapOfCategoryDTOTodomainName.remove(oldCategoryDTO.domainName);
					}
					
					if(mapOfCategoryDTOTonameEn.containsKey(oldCategoryDTO.nameEn)) {
						mapOfCategoryDTOTonameEn.get(oldCategoryDTO.nameEn).remove(oldCategoryDTO);
					}
					if(mapOfCategoryDTOTonameEn.get(oldCategoryDTO.nameEn).isEmpty()) {
						mapOfCategoryDTOTonameEn.remove(oldCategoryDTO.nameEn);
					}
					
					if(mapOfCategoryDTOTonameBn.containsKey(oldCategoryDTO.nameBn)) {
						mapOfCategoryDTOTonameBn.get(oldCategoryDTO.nameBn).remove(oldCategoryDTO);
					}
					if(mapOfCategoryDTOTonameBn.get(oldCategoryDTO.nameBn).isEmpty()) {
						mapOfCategoryDTOTonameBn.remove(oldCategoryDTO.nameBn);
					}
					
					if(mapOfCategoryDTOTovalue.containsKey(oldCategoryDTO.value)) {
						mapOfCategoryDTOTovalue.get(oldCategoryDTO.value).remove(oldCategoryDTO);
					}
					if(mapOfCategoryDTOTovalue.get(oldCategoryDTO.value).isEmpty()) {
						mapOfCategoryDTOTovalue.remove(oldCategoryDTO.value);
					}
					
					if(mapOfCategoryDTOTolastModificationTime.containsKey(oldCategoryDTO.lastModificationTime)) {
						mapOfCategoryDTOTolastModificationTime.get(oldCategoryDTO.lastModificationTime).remove(oldCategoryDTO);
					}
					if(mapOfCategoryDTOTolastModificationTime.get(oldCategoryDTO.lastModificationTime).isEmpty()) {
						mapOfCategoryDTOTolastModificationTime.remove(oldCategoryDTO.lastModificationTime);
					}
					
					
				}
				if(categoryDTO.isDeleted == 0) 
				{
					
					mapOfCategoryDTOToiD.put(categoryDTO.iD, categoryDTO);
				
					if( ! mapOfCategoryDTOTodomainName.containsKey(categoryDTO.domainName)) {
						mapOfCategoryDTOTodomainName.put(categoryDTO.domainName, new HashSet<>());
					}
					mapOfCategoryDTOTodomainName.get(categoryDTO.domainName).add(categoryDTO);
					
					if( ! mapOfCategoryDTOTonameEn.containsKey(categoryDTO.nameEn)) {
						mapOfCategoryDTOTonameEn.put(categoryDTO.nameEn, new HashSet<>());
					}
					mapOfCategoryDTOTonameEn.get(categoryDTO.nameEn).add(categoryDTO);
					
					if( ! mapOfCategoryDTOTonameBn.containsKey(categoryDTO.nameBn)) {
						mapOfCategoryDTOTonameBn.put(categoryDTO.nameBn, new HashSet<>());
					}
					mapOfCategoryDTOTonameBn.get(categoryDTO.nameBn).add(categoryDTO);
					
					if( ! mapOfCategoryDTOTovalue.containsKey(categoryDTO.value)) {
						mapOfCategoryDTOTovalue.put(categoryDTO.value, new HashSet<>());
					}
					mapOfCategoryDTOTovalue.get(categoryDTO.value).add(categoryDTO);
					
					if( ! mapOfCategoryDTOTolastModificationTime.containsKey(categoryDTO.lastModificationTime)) {
						mapOfCategoryDTOTolastModificationTime.put(categoryDTO.lastModificationTime, new HashSet<>());
					}
					mapOfCategoryDTOTolastModificationTime.get(categoryDTO.lastModificationTime).add(categoryDTO);
					
				}
			}
			System.out.println("Domain name cat size " + mapOfCategoryDTOTodomainName.size());
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<CategoryDTO> getCategoryList() {
		List <CategoryDTO> categorys = new ArrayList<CategoryDTO>(this.mapOfCategoryDTOToiD.values());
		return categorys;
	}
	
	
	public CategoryDTO getCategoryDTOByID( long ID){
		return mapOfCategoryDTOToiD.get(ID);
	}
	
	
	public List<CategoryDTO> getCategoryDTOBydomain_name(String domain_name) {
		return new ArrayList<>( mapOfCategoryDTOTodomainName.getOrDefault(domain_name,new HashSet<>()));
	}
	
	
	public List<CategoryDTO> getCategoryDTOByname_en(String name_en) {
		return new ArrayList<>( mapOfCategoryDTOTonameEn.getOrDefault(name_en,new HashSet<>()));
	}
	
	
	public List<CategoryDTO> getCategoryDTOByname_bn(String name_bn) {
		return new ArrayList<>( mapOfCategoryDTOTonameBn.getOrDefault(name_bn,new HashSet<>()));
	}
	
	
	public List<CategoryDTO> getCategoryDTOByvalue(int value) {
		return new ArrayList<>( mapOfCategoryDTOTovalue.getOrDefault(value,new HashSet<>()));
	}
	
	
	public List<CategoryDTO> getCategoryDTOBylastModificationTime(long lastModificationTime) {
		return new ArrayList<>( mapOfCategoryDTOTolastModificationTime.getOrDefault(lastModificationTime,new HashSet<>()));
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "category";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


