package category;

import common.CommonDAOService;
import common.ConnectionAndStatementUtil;

import org.apache.log4j.Logger;

import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class CategoryDAO implements CommonDAOService<CategoryDTO> {

    private static final Logger logger = Logger.getLogger(CategoryDAO.class);

    private static final String addQuery =
            "INSERT INTO {tableName} (domain_name, ordering, name_en,name_bn,value, parent_domain, parent_domain_val, lastModificationTime,search_column,isDeleted,ID)" +
                    " VALUES (?,?,?,?,?,?,?,?,?)";

    private static final String updateQuery = "UPDATE {tableName} SET domain_name = ?, ordering = ?, name_en = ?,name_bn = ?,value = ?, parent_domain = ?,parent_domain_val = ?," +
            "lastModificationTime = ?,search_column = ? WHERE ID = ?";

    public final HashMap<String, String> searchMap = new HashMap<>();

    private static final class CategoryDAOHolder {
        static final CategoryDAO INSTANCE = new CategoryDAO();
    }

    public static CategoryDAO getInstance() {
        return CategoryDAOHolder.INSTANCE;
    }

    private CategoryDAO() {
        searchMap.put("domain_name", " and (domain_name like ?)");
        searchMap.put("name_en", " and (name_en like ?)");
        searchMap.put("name_bn", " and (name_bn like ?)");
        searchMap.put("AnyField", " and (search_column like ?)");
    }

    @Override
    public void set(PreparedStatement ps, CategoryDTO categoryDTO, boolean isInsert) throws SQLException {
        int index = 0;
        if(categoryDTO.lastModificationTime == 0){
            categoryDTO.lastModificationTime = System.currentTimeMillis();
        }
        ps.setObject(++index, categoryDTO.domainName);
        ps.setObject(++index, categoryDTO.ordering);
        ps.setObject(++index, categoryDTO.nameEn);
        ps.setObject(++index, categoryDTO.nameBn);
        ps.setObject(++index, categoryDTO.value);
        ps.setObject(++index, categoryDTO.parentDomain);
        ps.setObject(++index, categoryDTO.parentDomainVal);
        ps.setObject(++index, categoryDTO.lastModificationTime);
        ps.setObject(++index, categoryDTO.domainName + " " + categoryDTO.nameEn + " " + categoryDTO.nameBn);
        if (isInsert) {
            ps.setObject(++index, 0);
        }
        ps.setObject(++index, categoryDTO.iD);
    }

    @Override
    public CategoryDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            CategoryDTO categoryDTO = new CategoryDTO();
            categoryDTO.iD = rs.getLong("ID");
            categoryDTO.domainName = rs.getString("domain_name");
            categoryDTO.ordering = rs.getInt("ordering");
            categoryDTO.nameEn = rs.getString("name_en");
            categoryDTO.nameBn = rs.getString("name_bn");
            categoryDTO.value = rs.getInt("value");
            categoryDTO.parentDomain = rs.getString("parent_domain");
            if(categoryDTO.parentDomain == null)
            {
            	categoryDTO.parentDomain = "";
            }
            categoryDTO.parentDomainVal = rs.getInt("parent_domain_val");
            categoryDTO.isDeleted = rs.getInt("isDeleted");
            categoryDTO.lastModificationTime = rs.getLong("lastModificationTime");
            return categoryDTO;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    @Override
    public String getTableName() {
        return "category";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    @Override
    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((CategoryDTO) commonDTO,addQuery,true);
    }
    
    public int getValToUse(String domainName)
    {
		String sql = "SELECT max(value) from category where domain_name = '" + domainName + "'";

		return ConnectionAndStatementUtil.getT(sql,this::getMax);	
    }
    
    public int getMax(ResultSet rs) {
    	int val = 0;
        try {
            
            val = rs.getInt("max(value)");
            return val + 1;
           
        } catch (SQLException ex) {
            logger.error(ex);
            return 0;
        }
    }

    @Override
    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((CategoryDTO) commonDTO,updateQuery,false);
    }
    
    @Override
	public String getLastModifierUser(){
		return "";
    }

    private static final String getByDomainNameAndValueSQL = "SELECT * FROM category WHERE domain_name= '%s' AND value = %d";

    public CategoryDTO getByDomainNameAndValueSQL(String domainName,int value){
        return ConnectionAndStatementUtil.getT(
                String.format(getByDomainNameAndValueSQL,domainName,value),this::buildObjectFromResultSet
        );
    }

    private static final String getByDomainNameAndValueAndNotIdSQL = "SELECT * FROM category WHERE domain_name= '%s' AND value = %d and ID != %d";

    public CategoryDTO getByDomainNameAndValueAndNotIdSQL(String domainName,int value,long id){
        return ConnectionAndStatementUtil.getT(
                String.format(getByDomainNameAndValueAndNotIdSQL,domainName,value,id),this::buildObjectFromResultSet
        );
    }
}