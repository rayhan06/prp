INSERT INTO economic_group (ID,name_en,name_bn) VALUES(31,'Employee Remuneration (Salary Allowances, Honorary Allowance, Overtime Allowance)','কর্মচারীদের প্রতিদান(বেতন-ভাতাদি, সম্মানী ভাতা, অধিকাল ভাতা)');
INSERT INTO economic_group (ID,name_en,name_bn) VALUES(32,'Use of products and services','পণ্য ও সেবা ব্যবহার');
INSERT INTO economic_group (ID,name_en,name_bn) VALUES(38,'Other Expenses (Voluntary Grants and Land Tax)','অন্যান্য ব্যয় ( স্বেচ্ছাধীন মঞ্জুরি ও ভূমি কর)');
INSERT INTO economic_group (ID,name_en,name_bn) VALUES(39,'General bulk allocation','সাধারণ থোক বরাদ্দ');
INSERT INTO economic_group (ID,name_en,name_bn) VALUES(41,'Non-financial assets','অ-আর্থিক সম্পদ');
INSERT INTO economic_group (ID,name_en,name_bn) VALUES(120,'Loans for government employees','সরকারি কর্মচারীদের জন্য ঋণ');
INSERT INTO economic_group (ID,name_en,name_bn) VALUES(127,'Subscriptions of international organizations','আন্তর্জাতিক সংস্থার চাঁদা');
INSERT INTO economic_group (ID,name_en,name_bn) VALUES(14,'Other Interest','অন্যান্য সুদ');
INSERT INTO economic_group (ID,name_en,name_bn) VALUES(72,'Monetary Resource','আর্থিক সম্পদ');