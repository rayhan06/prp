package economic_group;

import common.NameDao;

public class EconomicGroupDAO extends NameDao {

    private EconomicGroupDAO() {
        super("economic_group");
    }

    private static class LazyLoader{
        static final EconomicGroupDAO INSTANCE = new EconomicGroupDAO();
    }

    public static EconomicGroupDAO getInstance(){
        return LazyLoader.INSTANCE;
    }
}
