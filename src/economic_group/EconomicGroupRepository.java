package economic_group;

import common.NameDTO;
import common.NameRepository;
import pb.OptionDTO;
import pb.Utils;
import util.StringUtils;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class EconomicGroupRepository extends NameRepository {

    private EconomicGroupRepository() {
        super(EconomicGroupDAO.getInstance(), EconomicGroupRepository.class);
    }

    private static class LazyLoader {
        static EconomicGroupRepository INSTANCE = new EconomicGroupRepository();
    }

    public static EconomicGroupRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

    public String buildOptionWithCode(String language, Long selectedId) {
        List<OptionDTO> optionDTOList = getNameDTOList()
                .stream()
                .map(EconomicGroupRepository::buildOptionDTOWithCode)
                .collect(Collectors.toList());
        return Utils.buildOptions(optionDTOList, language, selectedId == null ? null : String.valueOf(selectedId));
    }

    public static OptionDTO buildOptionDTOWithCode(NameDTO economicGroupType) {
        String textEn = String.valueOf(economicGroupType.iD).concat(" - ").concat(economicGroupType.nameEn);
        String textBn = StringUtils.convertToBanNumber(String.valueOf(economicGroupType.iD)).concat(" - ").concat(economicGroupType.nameBn);
        return new OptionDTO(textEn, textBn, String.valueOf(economicGroupType.iD));
    }

    public String buildEconomicGroupOption(String language, Long selectedId, Set<Long> specificIds) {
        List<OptionDTO> optionDTOList =
                getNameDTOList()
                        .stream()
                        .filter(nameDTO -> specificIds.contains(nameDTO.iD))
                        .map(EconomicGroupRepository::buildOptionDTOWithCode)
                        .collect(Collectors.toList());
        return Utils.buildOptions(optionDTOList, language, selectedId == null ? null : String.valueOf(selectedId));
    }
}
