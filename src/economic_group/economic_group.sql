CREATE TABLE economic_group
(
    ID                   BIGINT(20) PRIMARY KEY,
    name_bn              VARCHAR(255) DEFAULT NULL,
    name_en              VARCHAR(255) DEFAULT NULL,
    isDeleted            INT(11)      DEFAULT 0,
    lastModificationTime BIGINT(20)   DEFAULT 0,
    inserted_by          BIGINT(20)   DEFAULT 0,
    modified_by          BIGINT(20)   DEFAULT 0,
    insertion_date       BIGINT(20)   DEFAULT 0
) ENGINE = MyISAM
  DEFAULT CHARSET = utf8
  COLLATE = utf8_unicode_ci;