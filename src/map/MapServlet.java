package map;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import login.LoginDTO;
import permission.MenuConstants;
import role.PermissionRepository;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.ActionTypeConstant;
import util.JSPConstant;

/**
 * Servlet implementation class MapServlet
 */
@WebServlet("/MapServlet")
public class MapServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MapServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served 2 at: ").append(request.getContextPath());
		
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);

		String actionType = request.getParameter(ActionTypeConstant.ACTION_TYPE);
		if(actionType.equals(ActionTypeConstant.MAP))
		{
			//if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.USER_ADD)){
				//response.sendRedirect(JSPConstant.LEASE_INFORMATION);
				
				RequestDispatcher rd = request.getRequestDispatcher(JSPConstant.MAP);
	    		rd.forward(request, response);
				
				//response.getWriter().append("Served at: ").append(request.getContextPath());
			//}else{
				//request.getRequestDispatcher(JSPConstant.ERROR_GLOBAL).forward(request, response);
				
				//response.sendRedirect(JSPConstant.LEASE_INFORMATION);
				
				//response.getWriter().append("Served 2 at: ").append(request.getContextPath());
			//}
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
