package office_shift;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import office_shift_details.Office_shift_detailsDTO;
import org.apache.log4j.Logger;

import pb.OptionDTO;
import pb.Utils;
import repository.Repository;
import repository.RepositoryManager;


public class Office_shiftRepository implements Repository {
	Office_shiftDAO office_shiftDAO;
	
	public void setDAO(Office_shiftDAO office_shiftDAO)
	{
		this.office_shiftDAO = office_shiftDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Office_shiftRepository.class);
	Map<Long, Office_shiftDTO>mapOfOffice_shiftDTOToid;
	Map<Long, Set<Office_shiftDTO> >mapOfOffice_shiftDTOToofficeUnitId;
	List<Office_shiftDTO> office_shiftDTOList;
	
	static Office_shiftRepository instance = null;  
	private Office_shiftRepository(){
		mapOfOffice_shiftDTOToid = new ConcurrentHashMap<>();
		mapOfOffice_shiftDTOToofficeUnitId = new ConcurrentHashMap<>();
		office_shiftDAO = new Office_shiftDAO();
		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Office_shiftRepository getInstance(){
		if (instance == null){
			instance = new Office_shiftRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(office_shiftDAO == null)
		{
			return;
		}
		try {
			List<Office_shiftDTO> office_shiftDTOs = office_shiftDAO.getAllOffice_shift(reloadAll);
			for(Office_shiftDTO office_shiftDTO : office_shiftDTOs) {
				Office_shiftDTO oldOffice_shiftDTO = getOffice_shiftDTOByid(office_shiftDTO.id);
				if( oldOffice_shiftDTO != null ) {
					mapOfOffice_shiftDTOToid.remove(oldOffice_shiftDTO.id);
				
					if(mapOfOffice_shiftDTOToofficeUnitId.containsKey(oldOffice_shiftDTO.officeUnitId)) {
						mapOfOffice_shiftDTOToofficeUnitId.get(oldOffice_shiftDTO.officeUnitId).remove(oldOffice_shiftDTO);
					}
					if(mapOfOffice_shiftDTOToofficeUnitId.get(oldOffice_shiftDTO.officeUnitId).isEmpty()) {
						mapOfOffice_shiftDTOToofficeUnitId.remove(oldOffice_shiftDTO.officeUnitId);
					}
					
					
					
					
				}
				if(office_shiftDTO.isDeleted == 0) 
				{
					
					mapOfOffice_shiftDTOToid.put(office_shiftDTO.id, office_shiftDTO);
				
					if( ! mapOfOffice_shiftDTOToofficeUnitId.containsKey(office_shiftDTO.officeUnitId)) {
						mapOfOffice_shiftDTOToofficeUnitId.put(office_shiftDTO.officeUnitId, new HashSet<>());
					}
					mapOfOffice_shiftDTOToofficeUnitId.get(office_shiftDTO.officeUnitId).add(office_shiftDTO);

				}
			}
			office_shiftDTOList = new ArrayList<>(mapOfOffice_shiftDTOToid.values());
			office_shiftDTOList.sort(Comparator.comparing(o->o.id));
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Office_shiftDTO> getOffice_shiftList() {
		List <Office_shiftDTO> office_shifts = new ArrayList<Office_shiftDTO>(this.mapOfOffice_shiftDTOToid.values());
		return office_shifts;
	}
	
	
	public Office_shiftDTO getOffice_shiftDTOByid( long id){
		return mapOfOffice_shiftDTOToid.get(id);
	}
	
	
	public List<Office_shiftDTO> getOffice_shiftDTOByoffice_unit_id(long office_unit_id) {
		return new ArrayList<>( mapOfOffice_shiftDTOToofficeUnitId.getOrDefault(office_unit_id,new HashSet<>()));
	}
	
	
	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "office_shift";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}


	public String buildOptions(String language, Long selectedId) {
		return buildOptions(language, selectedId,false);
	}

	public String buildOptionsWithoutSelectOption(String language) {
		return buildOptions(language,null,true);
	}

	private String buildOptions(String language, Long selectedId, boolean withoutSelectOption){
		List<OptionDTO> optionDTOList = null;
		if (office_shiftDTOList!=null && office_shiftDTOList.size() > 0){
			optionDTOList = office_shiftDTOList.stream()
					.map(dto->new OptionDTO(dto.nameEn,dto.nameBn,String.valueOf(dto.iD)))
					.collect(Collectors.toList());
		}
		if(withoutSelectOption){
			return Utils.buildOptionsWithoutSelectOption(optionDTOList,language);
		}
		return Utils.buildOptions(optionDTOList,language,selectedId == null ? null : String.valueOf(selectedId));
	}

	public String geText(String language, long id) {
		Office_shiftDTO dto = getOffice_shiftDTOByid(id);
		return dto == null ? "" : (language.equalsIgnoreCase("English") ?
				(dto.nameEn == null ? "" : dto.nameEn)
				: (dto.nameBn == null ? "" : dto.nameBn));
	}
}


