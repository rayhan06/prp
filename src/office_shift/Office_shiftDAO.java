package office_shift;

import employee_records.EmployeeCommonDTOService;
import office_shift_details.Office_shift_detailsDTO;
import office_shift_details.Office_shift_detailsRepository;
import office_units.Office_unitsRepository;
import org.apache.log4j.Logger;
import pb.OptionDTO;
import pb.Utils;
import user.UserDTO;
import util.CommonDTO;
import util.NavigationService4;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;

@SuppressWarnings("ALL")
public class Office_shiftDAO  extends NavigationService4 implements EmployeeCommonDTOService<Office_shiftDTO>
{
	
	public static final Logger logger = Logger.getLogger(Office_shiftDAO.class);
	
	public static final String addQuery = "INSERT INTO {tableName} (office_unit_id, name_en, name_bn, insertion_date, inserted_by, modified_by, " +
			"search_column, lastModificationTime, isDeleted, id) VALUES(?,?,?,?,?,?,?,?,?,?)";

	public static final String updateQuery = "UPDATE {tableName} SET office_unit_id=?, name_en=?, name_bn=?, insertion_date=?, inserted_by=?, " +
			"modified_by=?, search_column=?, lastModificationTime=? WHERE id = ?";

	private static final Map<String,String> searchMap = new HashMap<>();
	static  {
		searchMap.put("office_unit_id"," and (office_unit_id = ?)");
		searchMap.put("name_en"," and (name_en = ?)");
		searchMap.put("name_bn"," and (name_bn = ?)");
		searchMap.put("AnyField"," and (search_column like ?)");
	}

	public Office_shiftDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";
		useSafeSearch = true;
		commonMaps = new Office_shiftMAPS(tableName);
	}
	
	public Office_shiftDAO()
	{
		this("office_shift");		
	}
	
	public void setSearchColumn(Office_shiftDTO office_shiftDTO)
	{
		office_shiftDTO.searchColumn = Office_unitsRepository.getInstance().geText("English", office_shiftDTO.officeUnitId) + " " +
				                       Office_unitsRepository.getInstance().geText("Bangla", office_shiftDTO.officeUnitId) + " ";
		office_shiftDTO.searchColumn += office_shiftDTO.nameEn + " ";
		office_shiftDTO.searchColumn += office_shiftDTO.nameBn + " ";
		
	}
	
	public void set(PreparedStatement ps, Office_shiftDTO office_shiftDTO, boolean isInsert) throws SQLException
	{
		int index = 0;
		setSearchColumn(office_shiftDTO);
		ps.setObject(++index, office_shiftDTO.officeUnitId);
		ps.setObject(++index, office_shiftDTO.nameEn);
		ps.setObject(++index, office_shiftDTO.nameBn);
		ps.setObject(++index, office_shiftDTO.insertionDate);
		ps.setObject(++index, office_shiftDTO.insertedBy);
		ps.setObject(++index, office_shiftDTO.modifiedBy);
		ps.setObject(++index, office_shiftDTO.searchColumn);
		ps.setObject(++index, System.currentTimeMillis());
		if (isInsert) {
			ps.setObject(++index, 0);
		}
		ps.setObject(++index, office_shiftDTO.id);

	}

	@Override
	public Office_shiftDTO buildObjectFromResultSet(ResultSet rs) {
		try {
			Office_shiftDTO office_shiftDTO = new Office_shiftDTO();
			office_shiftDTO.id = rs.getLong("id");
			office_shiftDTO.officeUnitId = rs.getLong("office_unit_id");
			office_shiftDTO.nameEn = rs.getString("name_en");
			office_shiftDTO.nameBn = rs.getString("name_bn");
			office_shiftDTO.insertionDate = rs.getLong("insertion_date");
			office_shiftDTO.insertedBy = rs.getString("inserted_by");
			office_shiftDTO.modifiedBy = rs.getString("modified_by");
			office_shiftDTO.searchColumn = rs.getString("search_column");
			office_shiftDTO.isDeleted = rs.getInt("isDeleted");
			office_shiftDTO.lastModificationTime = rs.getLong("lastModificationTime");
			return office_shiftDTO;
		} catch (SQLException ex) {
			logger.error(ex);
			return null;
		}
	}

	@Override
	public String getTableName() {
		return "office_shift";
	}


	public long add(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Office_shiftDTO) commonDTO, addQuery, true);
	}


	public Office_shiftDTO getDTOByID(long ID) {
		return getDTOFromID(ID);

	}


	public long update(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Office_shiftDTO) commonDTO, updateQuery, false);
	}


	public List<Office_shiftDTO> getAllOffice_shift(boolean isFirstReload) {
		return getAllDTOs(isFirstReload);
	}

	
	public List<Office_shiftDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO)
	{
		return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
	}

	
	public List<Office_shiftDTO> getDTOs(Hashtable  p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
			String filter, boolean tableHasJobCat)
	{
		List<Object> objectList = new ArrayList<>();
		String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat,objectList);
		return getDTOs(sql,objectList);
	}


	public String getSqlWithSearchCriteriaForNormalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
			UserDTO userDTO, String filter, boolean tableHasJobCat,List<Object>objectList)
	{
		return getSearchQuery(tableName,p_searchCriteria,limit,offset,category,searchMap,objectList);
	}
	
	public String getSqlWithSearchCriteria(Hashtable p_searchCriteria, int limit, int offset, int category,
			boolean isPermanentTable, UserDTO userDTO, String filter, boolean tableHasJobCat,List<Object>objectList)
    {		
		return getSqlWithSearchCriteriaForNormalSearch(p_searchCriteria, limit, offset, category, userDTO, filter, tableHasJobCat,objectList);
    }


	public static String buildOptions(String language, Long selectedId) {
		return buildOptions(language,selectedId,false);
	}

	public static String buildOptionsNewShift(String language, Long selectedId) {
		return buildOptionsNewShift(language,selectedId,false);
	}

	private static String buildOptionsNewShift(String language, Long selectedId,boolean withoutSelectOption){
		List<OptionDTO> optionDTOList = null;
		Office_shiftDAO office_shiftDAO = new Office_shiftDAO();
		List<Office_shift_detailsDTO> allShiftDetails = Office_shift_detailsRepository.getInstance().getOffice_shift_detailsList();

		Set<Long> shiftIds = allShiftDetails.stream()
				.map(dto -> dto.officeShiftId)
				.collect(Collectors.toSet());

		List<Office_shiftDTO> office_shiftDTOList = office_shiftDAO.getAllOffice_shift(true);
		if (office_shiftDTOList!=null && office_shiftDTOList.size() > 0){
			optionDTOList = office_shiftDTOList.stream()
					.filter(dto -> !shiftIds.contains(dto.id))
					.map(dto->new OptionDTO(dto.nameEn + " | " + Office_unitsRepository.getInstance().geText("English", dto.officeUnitId),
							dto.nameBn + " | " + Office_unitsRepository.getInstance().geText("Bangla", dto.officeUnitId),String.valueOf(dto.id)))
					.collect(Collectors.toList());
		}
		if(withoutSelectOption){
			return Utils.buildOptionsWithoutSelectOption(optionDTOList,language);
		}
		return Utils.buildOptions(optionDTOList,language,selectedId == null ? null : String.valueOf(selectedId));
	}

	private static String buildOptions(String language, Long selectedId,boolean withoutSelectOption){
		List<OptionDTO> optionDTOList = null;
		Office_shiftDAO office_shiftDAO = new Office_shiftDAO();
		List<Office_shiftDTO> office_shiftDTOList = office_shiftDAO.getAllOffice_shift(true);
		if (office_shiftDTOList!=null && office_shiftDTOList.size() > 0){
			optionDTOList = office_shiftDTOList.stream()
					.map(dto->new OptionDTO(dto.nameEn + " | " + Office_unitsRepository.getInstance().geText("English", dto.officeUnitId),
							dto.nameBn + " | " + Office_unitsRepository.getInstance().geText("Bangla", dto.officeUnitId),String.valueOf(dto.id)))
					.collect(Collectors.toList());
		}
		if(withoutSelectOption){
			return Utils.buildOptionsWithoutSelectOption(optionDTOList,language);
		}
		return Utils.buildOptions(optionDTOList,language,selectedId == null ? null : String.valueOf(selectedId));
	}

	public static String geText(String language, long id) {
		Office_shiftDAO office_shiftDAO = new Office_shiftDAO();
		Office_shiftDTO dto = office_shiftDAO.getDTOByID(id);
		return dto == null ? "" : (language.equalsIgnoreCase("English") ?
				(dto.nameEn == null ? "" : dto.nameEn)
				: (dto.nameBn == null ? "" : dto.nameBn));
	}
				
}
	