package office_shift;
import java.util.*; 
import util.*; 


public class Office_shiftDTO extends CommonDTO
{

	public long id = 0;
	public long officeUnitId = 0;
    public String nameEn = "";
    public String nameBn = "";
	public long insertionDate = 0;
    public String insertedBy = "";
    public String modifiedBy = "";
	
	
    @Override
	public String toString() {
            return "$Office_shiftDTO[" +
            " id = " + id +
            " officeUnitId = " + officeUnitId +
            " nameEn = " + nameEn +
            " nameBn = " + nameBn +
            " insertionDate = " + insertionDate +
            " insertedBy = " + insertedBy +
            " modifiedBy = " + modifiedBy +
            " isDeleted = " + isDeleted +
            "]";
    }

}