package inbox_role_organogram;
import java.util.*; 
import util.*; 


public class Inbox_role_organogramDTO extends CommonDTO
{

	public long roleId = 0;
	public long organogramId = 0;
	
	
    @Override
	public String toString() {
            return "$Inbox_role_organogramDTO[" +
            " iD = " + iD +
            " roleId = " + roleId +
            " organogramId = " + organogramId +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}