package inbox_role_organogram;
import java.util.*; 
import util.*;


public class Inbox_role_organogramMAPS extends CommonMaps
{	
	public Inbox_role_organogramMAPS(String tableName)
	{
		
		java_allfield_type_map.put("role_id".toLowerCase(), "Long");
		java_allfield_type_map.put("organogram_id".toLowerCase(), "Long");

		java_anyfield_search_map.put(tableName + ".role_id".toLowerCase(), "Long");
		java_anyfield_search_map.put(tableName + ".organogram_id".toLowerCase(), "Long");

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("roleId".toLowerCase(), "roleId".toLowerCase());
		java_DTO_map.put("organogramId".toLowerCase(), "organogramId".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("role_id".toLowerCase(), "roleId".toLowerCase());
		java_SQL_map.put("organogram_id".toLowerCase(), "organogramId".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Role Id".toLowerCase(), "roleId".toLowerCase());
		java_Text_map.put("Organogram Id".toLowerCase(), "organogramId".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}