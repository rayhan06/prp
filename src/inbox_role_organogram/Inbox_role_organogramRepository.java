package inbox_role_organogram;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class Inbox_role_organogramRepository implements Repository {
	Inbox_role_organogramDAO inbox_role_organogramDAO = null;
	
	public void setDAO(Inbox_role_organogramDAO inbox_role_organogramDAO)
	{
		this.inbox_role_organogramDAO = inbox_role_organogramDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Inbox_role_organogramRepository.class);
	Map<Long, Inbox_role_organogramDTO>mapOfInbox_role_organogramDTOToiD;
	Map<Long, Set<Inbox_role_organogramDTO> >mapOfInbox_role_organogramDTOToroleId;
	Map<Long, Set<Inbox_role_organogramDTO> >mapOfInbox_role_organogramDTOToorganogramId;
	Map<Long, Set<Inbox_role_organogramDTO> >mapOfInbox_role_organogramDTOTolastModificationTime;


	static Inbox_role_organogramRepository instance = null;  
	private Inbox_role_organogramRepository(){
		mapOfInbox_role_organogramDTOToiD = new ConcurrentHashMap<>();
		mapOfInbox_role_organogramDTOToroleId = new ConcurrentHashMap<>();
		mapOfInbox_role_organogramDTOToorganogramId = new ConcurrentHashMap<>();
		mapOfInbox_role_organogramDTOTolastModificationTime = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Inbox_role_organogramRepository getInstance(){
		if (instance == null){
			instance = new Inbox_role_organogramRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(inbox_role_organogramDAO == null)
		{
			return;
		}
		try {
			List<Inbox_role_organogramDTO> inbox_role_organogramDTOs = inbox_role_organogramDAO.getAllInbox_role_organogram(reloadAll);
			for(Inbox_role_organogramDTO inbox_role_organogramDTO : inbox_role_organogramDTOs) {
				Inbox_role_organogramDTO oldInbox_role_organogramDTO = getInbox_role_organogramDTOByID(inbox_role_organogramDTO.iD);
				if( oldInbox_role_organogramDTO != null ) {
					mapOfInbox_role_organogramDTOToiD.remove(oldInbox_role_organogramDTO.iD);
				
					if(mapOfInbox_role_organogramDTOToroleId.containsKey(oldInbox_role_organogramDTO.roleId)) {
						mapOfInbox_role_organogramDTOToroleId.get(oldInbox_role_organogramDTO.roleId).remove(oldInbox_role_organogramDTO);
					}
					if(mapOfInbox_role_organogramDTOToroleId.get(oldInbox_role_organogramDTO.roleId).isEmpty()) {
						mapOfInbox_role_organogramDTOToroleId.remove(oldInbox_role_organogramDTO.roleId);
					}
					
					if(mapOfInbox_role_organogramDTOToorganogramId.containsKey(oldInbox_role_organogramDTO.organogramId)) {
						mapOfInbox_role_organogramDTOToorganogramId.get(oldInbox_role_organogramDTO.organogramId).remove(oldInbox_role_organogramDTO);
					}
					if(mapOfInbox_role_organogramDTOToorganogramId.get(oldInbox_role_organogramDTO.organogramId).isEmpty()) {
						mapOfInbox_role_organogramDTOToorganogramId.remove(oldInbox_role_organogramDTO.organogramId);
					}
					
					if(mapOfInbox_role_organogramDTOTolastModificationTime.containsKey(oldInbox_role_organogramDTO.lastModificationTime)) {
						mapOfInbox_role_organogramDTOTolastModificationTime.get(oldInbox_role_organogramDTO.lastModificationTime).remove(oldInbox_role_organogramDTO);
					}
					if(mapOfInbox_role_organogramDTOTolastModificationTime.get(oldInbox_role_organogramDTO.lastModificationTime).isEmpty()) {
						mapOfInbox_role_organogramDTOTolastModificationTime.remove(oldInbox_role_organogramDTO.lastModificationTime);
					}
					
					
				}
				if(inbox_role_organogramDTO.isDeleted == 0) 
				{
					
					mapOfInbox_role_organogramDTOToiD.put(inbox_role_organogramDTO.iD, inbox_role_organogramDTO);
				
					if( ! mapOfInbox_role_organogramDTOToroleId.containsKey(inbox_role_organogramDTO.roleId)) {
						mapOfInbox_role_organogramDTOToroleId.put(inbox_role_organogramDTO.roleId, new HashSet<>());
					}
					mapOfInbox_role_organogramDTOToroleId.get(inbox_role_organogramDTO.roleId).add(inbox_role_organogramDTO);
					
					if( ! mapOfInbox_role_organogramDTOToorganogramId.containsKey(inbox_role_organogramDTO.organogramId)) {
						mapOfInbox_role_organogramDTOToorganogramId.put(inbox_role_organogramDTO.organogramId, new HashSet<>());
					}
					mapOfInbox_role_organogramDTOToorganogramId.get(inbox_role_organogramDTO.organogramId).add(inbox_role_organogramDTO);
					
					if( ! mapOfInbox_role_organogramDTOTolastModificationTime.containsKey(inbox_role_organogramDTO.lastModificationTime)) {
						mapOfInbox_role_organogramDTOTolastModificationTime.put(inbox_role_organogramDTO.lastModificationTime, new HashSet<>());
					}
					mapOfInbox_role_organogramDTOTolastModificationTime.get(inbox_role_organogramDTO.lastModificationTime).add(inbox_role_organogramDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Inbox_role_organogramDTO> getInbox_role_organogramList() {
		List <Inbox_role_organogramDTO> inbox_role_organograms = new ArrayList<Inbox_role_organogramDTO>(this.mapOfInbox_role_organogramDTOToiD.values());
		return inbox_role_organograms;
	}
	
	
	public Inbox_role_organogramDTO getInbox_role_organogramDTOByID( long ID){
		return mapOfInbox_role_organogramDTOToiD.get(ID);
	}
	
	
	public List<Inbox_role_organogramDTO> getInbox_role_organogramDTOByrole_id(long role_id) {
		return new ArrayList<>( mapOfInbox_role_organogramDTOToroleId.getOrDefault(role_id,new HashSet<>()));
	}
	
	
	public List<Inbox_role_organogramDTO> getInbox_role_organogramDTOByorganogram_id(long organogram_id) {
		return new ArrayList<>( mapOfInbox_role_organogramDTOToorganogramId.getOrDefault(organogram_id,new HashSet<>()));
	}
	
	
	public List<Inbox_role_organogramDTO> getInbox_role_organogramDTOBylastModificationTime(long lastModificationTime) {
		return new ArrayList<>( mapOfInbox_role_organogramDTOTolastModificationTime.getOrDefault(lastModificationTime,new HashSet<>()));
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "inbox_role_organogram";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


