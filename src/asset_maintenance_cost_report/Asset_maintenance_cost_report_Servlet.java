package asset_maintenance_cost_report;


import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import language.LC;
import language.LM;

import login.LoginDTO;
import permission.MenuConstants;

import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;

import util.ReportRequestHandler;
import pbReport.*;






@WebServlet("/Asset_maintenance_cost_report_Servlet")
public class Asset_maintenance_cost_report_Servlet  extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	String[][] Criteria =
	{
		{"criteria","","transaction_date",">=","","long","","",Long.MIN_VALUE + "","startDate", LC.HM_START_DATE + ""},		
		{"criteria","","transaction_date","<=","AND","long","","",Long.MAX_VALUE + "","endDate", LC.HM_END_DATE + ""},		
		{"criteria","","asset_category_type","=","AND","int","","","any","assetCategoryType", LC.ASSET_DETAILS_REPORT_WHERE_ASSETCATEGORYTYPE + ""},		
		{"criteria","","model","like","AND","String","","","%","model", LC.HM_MODEL + ""},
		{"criteria","","cost",">","AND","double","","","0","model", LC.HM_MODEL + ""}
	};
	
	String[][] Display =
	{
		{"display","","asset_category_type","type",""},		
		{"display","","model","text",""},		
		{"display","","SUM(cost)","text",""}		
	};
	
	String GroupBy = "model, asset_category_type";
	String OrderBY = "transaction_date DESC";
	
	ReportRequestHandler reportRequestHandler;
	
	public Asset_maintenance_cost_report_Servlet(){

	}
	
	private final ReportService reportService = new ReportService();
	
	private String sql;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String language = "english";
		if(userDTO!= null && userDTO.languageID == SessionConstants.BANGLA)
		{
			language = "bangla";
		}
				
		String actionType = request.getParameter("actionType");

		System.out.println("In ssservlet doget, actiontype = " + actionType);
		
		sql = "asset_transaction";

		Display[0][4] = LM.getText(LC.ASSET_DETAILS_REPORT_WHERE_ASSETCATEGORYTYPE, loginDTO);
		Display[1][4] = LM.getText(LC.HM_MODEL, loginDTO);
		Display[2][4] = LM.getText(LC.HM_COST, loginDTO);

		
		String reportName = LM.getText(LC.ASSET_MAINTENANCE_COST_REPORT_OTHER_ASSET_MAINTENANCE_COST_REPORT, loginDTO);
		
		reportRequestHandler = new ReportRequestHandler(null,
				Criteria, Display, GroupBy, OrderBY, sql,
				reportService);
		
		reportRequestHandler.clientSideDataFormatting = new ArrayList<Pair<Integer, Integer>>();
		reportRequestHandler.clientSideDataFormatting.add(new ImmutablePair<Integer, Integer>(2, ReportRequestHandler.RIGHT_ALIGN_FLOAT));

		
		reportRequestHandler.handleReportGet(request, response, userDTO, "asset_maintenance_cost_report",
				MenuConstants.ASSET_MAINTENANCE_COST_REPORT_DETAILS, language, reportName, "asset_maintenance_cost_report");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		doGet(request, response);
	}
}
