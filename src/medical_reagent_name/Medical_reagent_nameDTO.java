package medical_reagent_name;
import java.util.*; 
import util.*; 


public class Medical_reagent_nameDTO extends CommonDTO
{

    public String nameEn = "";
    public String nameBn = "";
    public String quantityUnit = "";
	public int currentStock = -1;
	public boolean stockAlertRequired = false;
	public int stockAlertQuantity = -1;
	public long insertedByUserId = -1;
	public long insertedByOrganogramId = -1;
	public long insertionDate = -1;
	public int departmentCat = -1;
	public int quantityUnitCat = -1;
	
	
    @Override
	public String toString() {
            return "$Medical_reagent_nameDTO[" +
            " iD = " + iD +
            " nameEn = " + nameEn +
            " nameBn = " + nameBn +
            " quantityUnit = " + quantityUnit +
            " currentStock = " + currentStock +
            " stockAlertRequired = " + stockAlertRequired +
            " stockAlertQuantity = " + stockAlertQuantity +
            " insertedByUserId = " + insertedByUserId +
            " insertedByOrganogramId = " + insertedByOrganogramId +
            " insertionDate = " + insertionDate +
            " departmentCat = " + departmentCat +
            " quantityUnitCat = " + quantityUnitCat +
            " searchColumn = " + searchColumn +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}