package medical_reagent_name;

import java.io.IOException;
import java.io.*;
import java.text.SimpleDateFormat;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import org.apache.log4j.Logger;

import login.LoginDTO;
import permission.MenuConstants;
import role.PermissionRepository;


import sessionmanager.SessionConstants;

import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;
import util.CommonRequestHandler;
import util.RecordNavigationManager4;

import java.util.*;
import javax.servlet.http.*;
import java.util.UUID;





import geolocation.GeoLocationDAO2;
import java.util.StringTokenizer;

import com.google.gson.Gson;

import pb.*;
import pbReport.*;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;




/**
 * Servlet implementation class Medical_reagent_nameServlet
 */
@WebServlet("/Medical_reagent_nameServlet")
@MultipartConfig
public class Medical_reagent_nameServlet extends HttpServlet 
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Medical_reagent_nameServlet.class);

    String tableName = "medical_reagent_name";

	Medical_reagent_nameDAO medical_reagent_nameDAO;
	CommonRequestHandler commonRequestHandler;
    private final Gson gson = new Gson();
    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Medical_reagent_nameServlet() 
	{
        super();
    	try
    	{
			medical_reagent_nameDAO = new Medical_reagent_nameDAO(tableName);
			commonRequestHandler = new CommonRequestHandler(medical_reagent_nameDAO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }   

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		System.out.println("In doget request = " + request);
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		try
		{
			String actionType = request.getParameter("actionType");
			if(actionType.equals("getAddPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.MEDICAL_REAGENT_NAME_ADD))
				{
					commonRequestHandler.getAddPage(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getEditPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.MEDICAL_REAGENT_NAME_UPDATE))
				{
					getMedical_reagent_name(request, response);					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}						
			}
			else if(actionType.equals("getURL"))
			{
				String URL = request.getParameter("URL");
				System.out.println("URL = " + URL);
				response.sendRedirect(URL);			
			}			
			else if(actionType.equals("search"))
			{
				System.out.println("search requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.MEDICAL_REAGENT_NAME_SEARCH))
				{
					if(isPermanentTable)
					{
						String filter = request.getParameter("filter");
						System.out.println("filter = " + filter);
						if(filter!=null)
						{
							if(filter.equalsIgnoreCase("getCritical"))
							{
								filter = "current_stock <= stock_alert_quantity";
							}
							else if(filter.equalsIgnoreCase("getStockOut"))
							{
								filter = "current_stock <= 0";
							}
							else
							{
								filter = ""; //shouldn't be directly used, rather manipulate it.
							}
							searchMedical_reagent_name(request, response, isPermanentTable, filter);
						}
						else
						{
							searchMedical_reagent_name(request, response, isPermanentTable, "");
						}
					}					
					else
					{
						//searchMedical_reagent_name(request, response, tempTableName, isPermanentTable);
					}
				}			
			}
			else if(actionType.equals("view"))
			{
				System.out.println("view requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.MEDICAL_REAGENT_NAME_SEARCH))
				{
					commonRequestHandler.view(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else
			{
				request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}


	
	

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		// TODO Auto-generated method stub
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		System.out.println("doPost");
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		
		try
		{
			String actionType = request.getParameter("actionType");
			System.out.println("actionType = " + actionType);
			if(actionType.equals("add"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.MEDICAL_REAGENT_NAME_ADD))
				{
					System.out.println("going to  addMedical_reagent_name ");
					addMedical_reagent_name(request, response, true, userDTO, true);
				}
				else
				{
					System.out.println("Not going to  addMedical_reagent_name ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}

			else if(actionType.equals("getDTO"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.MEDICAL_REAGENT_NAME_ADD))
				{
					getDTO(request, response);					
				}
				else
				{
					System.out.println("Not going to  addMedical_reagent_name ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else if(actionType.equals("edit"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.MEDICAL_REAGENT_NAME_UPDATE))
				{					
					addMedical_reagent_name(request, response, false, userDTO, isPermanentTable);					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("delete"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.MEDICAL_REAGENT_NAME_UPDATE))
				{
					commonRequestHandler.delete(request, response, userDTO);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}	
			else if(actionType.equals("search"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.MEDICAL_REAGENT_NAME_SEARCH))
				{
					searchMedical_reagent_name(request, response, true, "");
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getGeo"))
			{
				System.out.println("going to geoloc ");
				request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
			}
			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}
	
	private void getDTO(HttpServletRequest request, HttpServletResponse response) 
	{
		try 
		{
			System.out.println("In getDTO");
			Medical_reagent_nameDTO medical_reagent_nameDTO = medical_reagent_nameDAO.getDTOByID(Long.parseLong(request.getParameter("ID")));
			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			
			String encoded = this.gson.toJson(medical_reagent_nameDTO);
			System.out.println("json encoded data = " + encoded);
			out.print(encoded);
			out.flush();
		}
		catch (NumberFormatException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	private void addMedical_reagent_name(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, boolean isPermanentTable) throws IOException 
	{
		// TODO Auto-generated method stub
		try 
		{
			request.setAttribute("failureMessage", "");
			System.out.println("%%%% addMedical_reagent_name");
			String path = getServletContext().getRealPath("/img2/");
			Medical_reagent_nameDTO medical_reagent_nameDTO;
			SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
						
			if(addFlag == true)
			{
				medical_reagent_nameDTO = new Medical_reagent_nameDTO();
			}
			else
			{
				medical_reagent_nameDTO = medical_reagent_nameDAO.getDTOByID(Long.parseLong(request.getParameter("iD")));
			}
			
			String Value = "";

			Value = request.getParameter("nameEn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("nameEn = " + Value);
			if(Value != null)
			{
				medical_reagent_nameDTO.nameEn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("nameBn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("nameBn = " + Value);
			if(Value != null)
			{
				medical_reagent_nameDTO.nameBn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("quantityUnit");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("quantityUnit = " + Value);
			if(Value != null)
			{
				medical_reagent_nameDTO.quantityUnit = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("currentStock");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("currentStock = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				medical_reagent_nameDTO.currentStock = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("stockAlertRequired");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("stockAlertRequired = " + Value);
            medical_reagent_nameDTO.stockAlertRequired = Value != null && !Value.equalsIgnoreCase("");

			Value = request.getParameter("stockAlertQuantity");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("stockAlertQuantity = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				medical_reagent_nameDTO.stockAlertQuantity = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			if(addFlag)
			{
				medical_reagent_nameDTO.insertedByUserId = userDTO.ID;
			}


			if(addFlag)
			{
				medical_reagent_nameDTO.insertedByOrganogramId = userDTO.organogramID;
			}


			if(addFlag)
			{
				Calendar c = Calendar.getInstance();
				c.set(Calendar.HOUR_OF_DAY, 0);
				c.set(Calendar.MINUTE, 0);
				c.set(Calendar.SECOND, 0);
				c.set(Calendar.MILLISECOND, 0);
				
				medical_reagent_nameDTO.insertionDate = c.getTimeInMillis();
			}			


			Value = request.getParameter("departmentCat");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("departmentCat = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				medical_reagent_nameDTO.departmentCat = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("quantityUnitCat");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("quantityUnitCat = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				medical_reagent_nameDTO.quantityUnitCat = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("searchColumn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("searchColumn = " + Value);
			if(Value != null)
			{
				medical_reagent_nameDTO.searchColumn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			
			System.out.println("Done adding  addMedical_reagent_name dto = " + medical_reagent_nameDTO);
			long returnedID = -1;
			
			if(isPermanentTable == false) //add new row for validation and make the old row outdated
			{
				medical_reagent_nameDAO.setIsDeleted(medical_reagent_nameDTO.iD, CommonDTO.OUTDATED);
				returnedID = medical_reagent_nameDAO.add(medical_reagent_nameDTO);
				medical_reagent_nameDAO.setIsDeleted(returnedID, CommonDTO.WAITING_FOR_APPROVAL);
			}
			else if(addFlag == true)
			{
				returnedID = medical_reagent_nameDAO.manageWriteOperations(medical_reagent_nameDTO, SessionConstants.INSERT, -1, userDTO);
			}
			else
			{				
				returnedID = medical_reagent_nameDAO.manageWriteOperations(medical_reagent_nameDTO, SessionConstants.UPDATE, -1, userDTO);											
			}
			
			
			
			
			
			
			
			
			
			if(isPermanentTable)
			{
				String inPlaceSubmit = request.getParameter("inplacesubmit");
				
				if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
				{
					getMedical_reagent_name(request, response, returnedID);
				}
				else
				{
					response.sendRedirect("Medical_reagent_nameServlet?actionType=search");
				}
			}
			else
			{
				commonRequestHandler.validate(medical_reagent_nameDAO.getDTOByID(returnedID), request, response, userDTO);
			}
					
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	



	
	
	

	private void getMedical_reagent_name(HttpServletRequest request, HttpServletResponse response, long id) throws ServletException, IOException
	{
		System.out.println("in getMedical_reagent_name");
		Medical_reagent_nameDTO medical_reagent_nameDTO = null;
		try 
		{
			medical_reagent_nameDTO = medical_reagent_nameDAO.getDTOByID(id);
			request.setAttribute("ID", medical_reagent_nameDTO.iD);
			request.setAttribute("medical_reagent_nameDTO",medical_reagent_nameDTO);
			request.setAttribute("medical_reagent_nameDAO",medical_reagent_nameDAO);
			
			String URL= "";
			
			String inPlaceEdit = request.getParameter("inplaceedit");
			String inPlaceSubmit = request.getParameter("inplacesubmit");
			String getBodyOnly = request.getParameter("getBodyOnly");
			
			if(inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase(""))
			{
				URL = "medical_reagent_name/medical_reagent_nameInPlaceEdit.jsp";	
				request.setAttribute("inplaceedit","");				
			}
			else if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
			{
				URL = "medical_reagent_name/medical_reagent_nameSearchRow.jsp";
				request.setAttribute("inplacesubmit","");					
			}
			else
			{
				if(getBodyOnly  != null && !getBodyOnly.equalsIgnoreCase(""))
				{
					URL = "medical_reagent_name/medical_reagent_nameEditBody.jsp?actionType=edit";
				}
				else
				{
					URL = "medical_reagent_name/medical_reagent_nameEdit.jsp?actionType=edit";
				}				
			}
			
			RequestDispatcher rd = request.getRequestDispatcher(URL);
			rd.forward(request, response);
		}
		catch (NumberFormatException e) 
		{
			e.printStackTrace();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	
	private void getMedical_reagent_name(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		getMedical_reagent_name(request, response, Long.parseLong(request.getParameter("ID")));	
	}
	
	private void searchMedical_reagent_name(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException
	{
		System.out.println("in  searchMedical_reagent_name 1");
		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String ajax = request.getParameter("ajax");
		boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);
		
        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
			SessionConstants.NAV_MEDICAL_REAGENT_NAME,
			request,
			medical_reagent_nameDAO,
			SessionConstants.VIEW_MEDICAL_REAGENT_NAME,
			SessionConstants.SEARCH_MEDICAL_REAGENT_NAME,
			tableName,
			isPermanent,
			userDTO,
			filter,
			true);
        try
        {
			System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        }
        catch(Exception e)
        {
			System.out.println("failed to dojob" + e);
        }

		request.setAttribute("medical_reagent_nameDAO",medical_reagent_nameDAO);
        RequestDispatcher rd;
        if(!isPermanent)
        {
        	if(hasAjax == false)
	        {
	        	System.out.println("Going to medical_reagent_name/medical_reagent_nameApproval.jsp");
	        	rd = request.getRequestDispatcher("medical_reagent_name/medical_reagent_nameApproval.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to medical_reagent_name/medical_reagent_nameApprovalForm.jsp");
	        	rd = request.getRequestDispatcher("medical_reagent_name/medical_reagent_nameApprovalForm.jsp");
	        }
        }
        else
        {
	        if(hasAjax == false)
	        {
	        	System.out.println("Going to medical_reagent_name/medical_reagent_nameSearch.jsp");
	        	rd = request.getRequestDispatcher("medical_reagent_name/medical_reagent_nameSearch.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to medical_reagent_name/medical_reagent_nameSearchForm.jsp");
	        	rd = request.getRequestDispatcher("medical_reagent_name/medical_reagent_nameSearchForm.jsp");
	        }
        }
		rd.forward(request, response);
	}
	
}

