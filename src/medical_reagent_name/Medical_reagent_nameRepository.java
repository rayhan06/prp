package medical_reagent_name;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class Medical_reagent_nameRepository implements Repository {
	Medical_reagent_nameDAO medical_reagent_nameDAO = null;
	
	public void setDAO(Medical_reagent_nameDAO medical_reagent_nameDAO)
	{
		this.medical_reagent_nameDAO = medical_reagent_nameDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Medical_reagent_nameRepository.class);
	Map<Long, Medical_reagent_nameDTO>mapOfMedical_reagent_nameDTOToiD;
	Map<Integer, Set<Medical_reagent_nameDTO> >mapOfMedical_reagent_nameDTOTodepartmentCat;


	static Medical_reagent_nameRepository instance = null;  
	private Medical_reagent_nameRepository(){
		mapOfMedical_reagent_nameDTOToiD = new ConcurrentHashMap<>();
		mapOfMedical_reagent_nameDTOTodepartmentCat = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Medical_reagent_nameRepository getInstance(){
		if (instance == null){
			instance = new Medical_reagent_nameRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(medical_reagent_nameDAO == null)
		{
			medical_reagent_nameDAO = new Medical_reagent_nameDAO();
		}
		try {
			List<Medical_reagent_nameDTO> medical_reagent_nameDTOs = medical_reagent_nameDAO.getAllMedical_reagent_name(reloadAll);
			for(Medical_reagent_nameDTO medical_reagent_nameDTO : medical_reagent_nameDTOs) {
				Medical_reagent_nameDTO oldMedical_reagent_nameDTO = getMedical_reagent_nameDTOByID(medical_reagent_nameDTO.iD);
				if( oldMedical_reagent_nameDTO != null ) {
					mapOfMedical_reagent_nameDTOToiD.remove(oldMedical_reagent_nameDTO.iD);
				
				
					if(mapOfMedical_reagent_nameDTOTodepartmentCat.containsKey(oldMedical_reagent_nameDTO.departmentCat)) {
						mapOfMedical_reagent_nameDTOTodepartmentCat.get(oldMedical_reagent_nameDTO.departmentCat).remove(oldMedical_reagent_nameDTO);
					}
					
					
				}
				if(medical_reagent_nameDTO.isDeleted == 0) 
				{
					
					mapOfMedical_reagent_nameDTOToiD.put(medical_reagent_nameDTO.iD, medical_reagent_nameDTO);
				
					
					if( ! mapOfMedical_reagent_nameDTOTodepartmentCat.containsKey(medical_reagent_nameDTO.departmentCat)) {
						mapOfMedical_reagent_nameDTOTodepartmentCat.put(medical_reagent_nameDTO.departmentCat, new HashSet<>());
					}
					mapOfMedical_reagent_nameDTOTodepartmentCat.get(medical_reagent_nameDTO.departmentCat).add(medical_reagent_nameDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Medical_reagent_nameDTO> getMedical_reagent_nameList() {
		List <Medical_reagent_nameDTO> medical_reagent_names = new ArrayList<Medical_reagent_nameDTO>(this.mapOfMedical_reagent_nameDTOToiD.values());
		return medical_reagent_names;
	}
	
	
	public Medical_reagent_nameDTO getMedical_reagent_nameDTOByID( long ID){
		return mapOfMedical_reagent_nameDTOToiD.get(ID);
	}
	
	
	public List<Medical_reagent_nameDTO> getMedical_reagent_nameDTOBydepartment_cat(int department_cat) {
		return new ArrayList<>( mapOfMedical_reagent_nameDTOTodepartmentCat.getOrDefault(department_cat,new HashSet<>()));
	}


	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "medical_reagent_name";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


