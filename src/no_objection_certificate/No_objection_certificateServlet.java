package no_objection_certificate;

import common.BaseServlet;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import pb.Utils;
import permission.MenuConstants;
import user.UserDTO;
import util.CommonDTO;
import util.HttpRequestUtils;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;

@WebServlet("/No_objection_certificateServlet")
@MultipartConfig
public class No_objection_certificateServlet extends BaseServlet {
    public static Logger logger = Logger.getLogger(No_objection_certificateServlet.class);

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        long currentTime = System.currentTimeMillis();
        boolean isLangEng = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;

        No_objection_certificateDTO nocDTO;
        if (addFlag) {
            nocDTO = new No_objection_certificateDTO();
            nocDTO.insertedBy = userDTO.ID;
            nocDTO.insertionTime = currentTime;
        } else {
            nocDTO = No_objection_certificateDAO.getInstance().getDTOFromID(Long.parseLong(request.getParameter("iD")));
        }
        nocDTO.modifiedBy = userDTO.ID;
        nocDTO.lastModificationTime = currentTime;

        nocDTO.subject = Jsoup.clean(
                request.getParameter("subject"), Whitelist.simpleText()
        );
        final String selectEmployeeErrorMessage = isLangEng ? "Select Recipient" : "প্রাপক নির্বাচন করুন";
        nocDTO.officeUnitId = Utils.parseMandatoryLong(
                request.getParameter("officeUnitId"),
                selectEmployeeErrorMessage
        );
        nocDTO.organogramId = Utils.parseMandatoryLong(
                request.getParameter("organogramId"),
                selectEmployeeErrorMessage
        );
        nocDTO.employeeRecordId = Utils.parseMandatoryLong(
                request.getParameter("employeeRecordId"),
                selectEmployeeErrorMessage
        );
        nocDTO.issueDate = Utils.parseMandatoryDate(
                request.getParameter("issueDate"),
                isLangEng ? "Select Issue Date" : "প্রদানের তারিখ বাছাই করুন"
        );
        nocDTO.issueAuthority = Jsoup.clean(
                request.getParameter("issueAuthority"), Whitelist.simpleText()
        );
        String fileDropzone = request.getParameter("fileDropzone");
        if (fileDropzone != null && !fileDropzone.equalsIgnoreCase("")) {
            fileDropzone = Jsoup.clean(fileDropzone, Whitelist.simpleText());
        }
        if (fileDropzone != null && !fileDropzone.equalsIgnoreCase("")) {
            nocDTO.fileDropzone = Long.parseLong(fileDropzone);
            if (!addFlag) {
                String fileDropzoneFilesToDelete = request.getParameter("fileDropzoneFilesToDelete");
                String[] deleteArray = fileDropzoneFilesToDelete.split(",");
                for (int i = 0; i < deleteArray.length; i++) {
                    logger.debug("going to delete " + deleteArray[i]);
                    if (i > 0) {
                        filesDAO.delete(Long.parseLong(deleteArray[i]));
                    }
                }
            }
        }
        if (addFlag) {
            No_objection_certificateDAO.getInstance().add(nocDTO);
        } else {
            No_objection_certificateDAO.getInstance().update(nocDTO);
        }
        return nocDTO;
    }

    @Override
    public String getTableName() {
        return getCommonDAOService().getTableName();
    }

    @Override
    public String getServletName() {
        return "No_objection_certificateServlet";
    }

    @Override
    public No_objection_certificateDAO getCommonDAOService() {
        return No_objection_certificateDAO.getInstance();
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[]{MenuConstants.NO_OBJECTION_CERTIFICATE_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[]{MenuConstants.NO_OBJECTION_CERTIFICATE_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[]{MenuConstants.NO_OBJECTION_CERTIFICATE_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return No_objection_certificateServlet.class;
    }
}
