package no_objection_certificate;

import sessionmanager.SessionConstants;
import util.CommonDTO;

public class No_objection_certificateDTO extends CommonDTO {
    public String subject = "";
    public long officeUnitId;
    public long organogramId;
    public long employeeRecordId;
    public long issueDate = SessionConstants.MIN_DATE;
    public String issueAuthority = "";
    public long fileDropzone = -1;

    public long insertedBy = -1;
    public long insertionTime = -1;
    public long modifiedBy = -1;
}
