package no_objection_certificate;

import common.CommonDAOService;
import employee_records.Employee_recordsDTO;
import employee_records.Employee_recordsRepository;
import office_unit_organograms.OfficeUnitOrganograms;
import office_unit_organograms.OfficeUnitOrganogramsRepository;
import office_units.Office_unitsDTO;
import office_units.Office_unitsRepository;
import org.apache.log4j.Logger;
import util.CommonDTO;
import util.StringUtils;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

@SuppressWarnings({"Duplicates"})
public class No_objection_certificateDAO implements CommonDAOService<No_objection_certificateDTO> {
    private static final Logger logger = Logger.getLogger(No_objection_certificateDAO.class);

    private static final String addSqlQuery =
            "INSERT INTO {tableName} (subject,office_unit_id,organogram_id,employee_record_id,issue_date,"
                    .concat("issue_authority,file_dropzone,search_column,modified_by,lastModificationTime,")
                    .concat("inserted_by,insertion_time,isDeleted,ID) ")
                    .concat("VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

    private static final String updateSqlQuery =
            "UPDATE {tableName} SET subject=?,office_unit_id=?,organogram_id=?,employee_record_id=?,issue_date=?,"
                    .concat("issue_authority=?,file_dropzone=?,search_column=?,modified_by=?,lastModificationTime=? WHERE ID=?");

    private static final Map<String, String> searchMap = new HashMap<>();

    private No_objection_certificateDAO() {
        searchMap.put("AnyField", " and (search_column LIKE ?) ");
        searchMap.put("issueDateFrom", " and (issue_date >= ?) ");
        searchMap.put("issueDateTo", " and (issue_date <= ?) ");
        searchMap.put("issueAuthority", " and (issue_authority LIKE ?)");
        searchMap.put("officeUnitsId", " and (office_unit_id = ?)");
    }

    private static class LazyLoader {
        static final No_objection_certificateDAO INSTANCE = new No_objection_certificateDAO();
    }

    public static No_objection_certificateDAO getInstance() {
        return LazyLoader.INSTANCE;
    }

    private void setSearchColumn(No_objection_certificateDTO dto) {
        StringBuilder stringBuilder = new StringBuilder();
        Employee_recordsDTO employeeRecordsDTO = Employee_recordsRepository.getInstance().getById(dto.employeeRecordId);
        if (employeeRecordsDTO != null) {
            stringBuilder.append(employeeRecordsDTO.employeeNumber).append(" ")
                         .append(StringUtils.convertToBanNumber(employeeRecordsDTO.employeeNumber)).append(" ")
                         .append(employeeRecordsDTO.nameEng).append(" ")
                         .append(employeeRecordsDTO.nameBng).append(" ")
                         .append(employeeRecordsDTO.personalMobile).append(" ")
                         .append(StringUtils.convertToBanNumber(employeeRecordsDTO.personalMobile)).append(" ");
        }
        OfficeUnitOrganograms officeUnitOrganograms = OfficeUnitOrganogramsRepository.getInstance().getById(dto.organogramId);
        if (officeUnitOrganograms != null) {
            stringBuilder.append(officeUnitOrganograms.designation_eng).append(" ")
                         .append(officeUnitOrganograms.designation_bng).append(" ");
        }
        Office_unitsDTO officeUnitsDTO = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(dto.officeUnitId);
        if (officeUnitsDTO != null) {
            stringBuilder.append(officeUnitsDTO.unitNameEng).append(" ")
                         .append(officeUnitsDTO.unitNameBng).append(" ");
        }
        dto.searchColumn = stringBuilder.toString();
    }

    @Override
    public void set(PreparedStatement ps, No_objection_certificateDTO dto, boolean isInsert) throws SQLException {
        setSearchColumn(dto);
        int index = 0;
        ps.setString(++index, dto.subject);
        ps.setLong(++index, dto.officeUnitId);
        ps.setLong(++index, dto.organogramId);
        ps.setLong(++index, dto.employeeRecordId);
        ps.setLong(++index, dto.issueDate);
        ps.setString(++index, dto.issueAuthority);
        ps.setLong(++index, dto.fileDropzone);
        ps.setString(++index, dto.searchColumn);
        ps.setLong(++index, dto.modifiedBy);
        ps.setLong(++index, dto.lastModificationTime);
        if (isInsert) {
            ps.setLong(++index, dto.insertedBy);
            ps.setLong(++index, dto.insertionTime);
            ps.setInt(++index, 0 /* isDeleted */);
        }
        ps.setLong(++index, dto.iD);
    }

    @Override
    public No_objection_certificateDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            No_objection_certificateDTO dto = new No_objection_certificateDTO();
            dto.iD = rs.getLong("ID");
            dto.subject = rs.getString("subject");
            dto.officeUnitId = rs.getLong("office_unit_id");
            dto.organogramId = rs.getLong("organogram_id");
            dto.employeeRecordId = rs.getLong("employee_record_id");
            dto.issueDate = rs.getLong("issue_date");
            dto.issueAuthority = rs.getString("issue_authority");
            dto.fileDropzone = rs.getLong("file_dropzone");
            dto.searchColumn = rs.getString("search_column");
            dto.modifiedBy = rs.getLong("modified_by");
            dto.lastModificationTime = rs.getLong("lastModificationTime");
            dto.insertedBy = rs.getLong("inserted_by");
            dto.insertionTime = rs.getLong("insertion_time");
            dto.isDeleted = rs.getInt("isDeleted");
            return dto;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }


    @Override
    public String getTableName() {
        return "no_objection_certificate";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    @Override
    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((No_objection_certificateDTO) commonDTO, addSqlQuery, true);
    }

    @Override
    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((No_objection_certificateDTO) commonDTO, updateSqlQuery, false);
    }
}
