package job_applicant_documents;

import com.google.gson.Gson;
import files.FilesDAO;
import files.FilesDTO;
import login.LoginDTO;
import org.apache.log4j.Logger;
import pb.Utils;
import permission.MenuConstants;
import recruitment_job_description.RecruitmentJobSpecificFilesDAO;
import recruitment_job_description.RecruitmentJobSpecificFilesDTO;
import recruitment_job_required_files.Recruitment_job_required_filesDAO;
import recruitment_job_required_files.Recruitment_job_required_filesDTO;
import role.PermissionRepository;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.CommonRequestHandler;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * Servlet implementation class Job_applicant_documentsServlet
 */
@WebServlet("/Job_applicant_documentsServlet")
@MultipartConfig
public class Job_applicant_documnetsServlet extends HttpServlet
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Job_applicant_documnetsServlet.class);

    String tableName = "job_applicant_documents";

	Job_applicant_documentsDAO job_applicant_documentsDAO;
	CommonRequestHandler commonRequestHandler;
	FilesDAO filesDAO = new FilesDAO();
    private final Gson gson = new Gson();

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Job_applicant_documnetsServlet()
	{
        super();
    	try
    	{
			job_applicant_documentsDAO = new Job_applicant_documentsDAO(tableName);
			commonRequestHandler = new CommonRequestHandler(job_applicant_documentsDAO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

    void getAddOrEditPage(Boolean type, HttpServletRequest request, HttpServletResponse response) throws Exception {
		long jobApplicantId = Long.parseLong(request.getParameter("jobApplicantId"));
		long jobId = Long.parseLong(request.getParameter("jobId"));

		if(type){
			List<Job_applicant_documentsDTO> job_applicant_documentsDTOS = job_applicant_documentsDAO.
					getJob_applicant_documentsDTOListByjobApplicantIdAndJobId(jobApplicantId, jobId);

			Map<Long, Job_applicant_documentsDTO> job_applicant_documentsDTOMap = new HashMap<>(
					job_applicant_documentsDTOS.stream().collect(Collectors.toMap(s -> s.recruitmentJobSpecificFilesId, s -> s))) ;

			request.setAttribute("job_applicant_documentsDTOMap",job_applicant_documentsDTOMap);

		}

		RecruitmentJobSpecificFilesDAO recruitmentJobSpecificFilesDAO = new RecruitmentJobSpecificFilesDAO();
		Recruitment_job_required_filesDAO recruitment_job_required_filesDAO = new Recruitment_job_required_filesDAO();
		List<RecruitmentJobSpecificFilesDTO>  recruitmentJobSpecificFilesDTOS =
				recruitmentJobSpecificFilesDAO.getRecruitmentJobSpecificFilesDTOListByRecruitmentJobDescriptionID(jobId);
		List<Long> reqFileIds = recruitmentJobSpecificFilesDTOS.stream().map(i -> i.recruitmentJobRequiredFilesType).collect(Collectors.toList());

		List<Recruitment_job_required_filesDTO> recruitment_job_required_filesDTOS =
				(List<Recruitment_job_required_filesDTO>) recruitment_job_required_filesDAO.getDTOs(reqFileIds);
		Map<Long, Recruitment_job_required_filesDTO> recruitment_job_required_filesDTOMap = new HashMap<>(
				recruitment_job_required_filesDTOS.stream().collect(Collectors.toMap(s -> s.iD, s -> s))) ;

		recruitmentJobSpecificFilesDTOS.forEach(i -> {
			if(recruitment_job_required_filesDTOMap.containsKey(i.recruitmentJobRequiredFilesType)){
				i.nameBn = recruitment_job_required_filesDTOMap.get(i.recruitmentJobRequiredFilesType).nameBn;
				i.nameEn = recruitment_job_required_filesDTOMap.get(i.recruitmentJobRequiredFilesType).nameEn;
			}
		});

		request.setAttribute("recruitmentJobSpecificFilesDTOS",recruitmentJobSpecificFilesDTOS);

		RequestDispatcher rd;
		rd = request.getRequestDispatcher( "job_applicant_documents/job_applicant_documentsEdit.jsp");
		rd.forward(request, response);

	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		System.out.println("In doget request = " + request);
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		try
		{
			String actionType = request.getParameter("actionType");
			if(actionType.equals("getAddPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.JOB_APPLICANT_DOCUMENT))
				{
					getAddOrEditPage(false, request, response );
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getEditPage"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.JOB_APPLICANT_DOCUMENT))
				{
					getAddOrEditPage(true, request, response );
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("downloadDropzoneFile"))
			{
				long id = Long.parseLong(request.getParameter("id"));

				FilesDTO filesDTO = (FilesDTO) filesDAO.getDTOByID(id);

				Utils.ProcessFile(request, response, filesDTO.fileTitle, filesDTO.inputStream);

			}
			else if(actionType.equals("DeleteFileFromDropZone"))
			{
				long id = Long.parseLong(request.getParameter("id"));


				System.out.println("In delete file");
				filesDAO.hardDeleteByID(id);
				response.getWriter().write("Deleted");

			}
			else if(actionType.equals("getURL"))
			{
				String URL = request.getParameter("URL");
				System.out.println("URL = " + URL);
				response.sendRedirect(URL);
			}
//			else if(actionType.equals("search"))
//			{
//				System.out.println("search requested");
//				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.JOB_APPLICANT_documents_SEARCH))
//				{
//					if(isPermanentTable)
//					{
//						String filter = request.getParameter("filter");
//						System.out.println("filter = " + filter);
//						if(filter!=null)
//						{
//							filter = ""; //shouldn't be directly used, rather manipulate it.
//							searchJob_applicant_documents(request, response, isPermanentTable, filter);
//						}
//						else
//						{
//							searchJob_applicant_documents(request, response, isPermanentTable, "");
//						}
//					}
//					else
//					{
//						//searchJob_applicant_documents(request, response, tempTableName, isPermanentTable);
//					}
//				}
//			}
//			else if(actionType.equals("view"))
//			{
//				System.out.println("view requested");
//				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.JOB_APPLICANT_documents_SEARCH))
//				{
//					commonRequestHandler.view(request, response);
//				}
//				else
//				{
//					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
//				}
//
//			}
			else
			{
				request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}





	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		// TODO Auto-generated method stub
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		System.out.println("doPost");
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);

		try
		{
			String actionType = request.getParameter("actionType");
			System.out.println("actionType = " + actionType);
			if(actionType.equals("add"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.JOB_APPLICANT_DOCUMENT))
				{
					System.out.println("going to  addJob_applicant_documents ");
					long jobApplicantId = Long.parseLong(request.getParameter("jobApplicantId"));
					long jobId = Long.parseLong(request.getParameter("jobId"));
					int rowCount = Integer.parseInt(request.getParameter("rowCount"));

					long currentTime = System.currentTimeMillis();

					for(int i = 0; i < rowCount; i++){
						Job_applicant_documentsDTO job_applicant_documentsDTO = new Job_applicant_documentsDTO();
						String id = "recruitment_job_specific_files_id_" + i;
						job_applicant_documentsDTO.recruitmentJobSpecificFilesId = Integer.parseInt(request.getParameter(id));
						id = "is_checked_" + i;
						job_applicant_documentsDTO.isChecked = Boolean.parseBoolean(request.getParameter(id));
						id = "filesDropzone_dropzone_" + i;
						job_applicant_documentsDTO.filesDropzone = Long.parseLong(request.getParameter(id));
						job_applicant_documentsDTO.jobApplicantId = jobApplicantId;
						job_applicant_documentsDTO.jobId = jobId;
						job_applicant_documentsDTO.insertedByUserId = loginDTO.userID;
						job_applicant_documentsDTO.insertionDate = currentTime;

						job_applicant_documentsDAO.add(job_applicant_documentsDTO);



					}

				}
				else
				{
					System.out.println("Not going to  addJob_applicant_documents ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}
			else if(actionType.equals("edit")){
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.JOB_APPLICANT_DOCUMENT))
				{
					int rowCount = Integer.parseInt(request.getParameter("rowCount"));
					Job_applicant_documentsDTO dto = null;

					String filesDropzoneFilesToDelete = request.getParameter("fileDeleteIds");
					String[] deleteArray = filesDropzoneFilesToDelete.split(",");
					for(int i = 0; i < deleteArray.length; i ++)
					{
						System.out.println("going to delete " + deleteArray[i]);
						if(i>0)
						{
							filesDAO.delete(Long.parseLong(deleteArray[i]));
						}
					}


					for(int i = 0; i < rowCount; i++){
						String id = "job_applicant_documents_id_" + i;
						long Id = Long.parseLong(request.getParameter(id));
						dto = job_applicant_documentsDAO.getDTOByID(Id);

						id = "is_checked_" + i;
						dto.isChecked = Boolean.parseBoolean(request.getParameter(id));
						job_applicant_documentsDAO.update(dto);

					}

				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}


			}
			else if(actionType.equals("UploadFilesFromDropZone"))
			{
				String Column = request.getParameter("columnName");
				long ColumnID = Long.parseLong(request.getParameter("ColumnID"));
//				String pageType = request.getParameter("pageType");

//				System.out.println("In " + pageType);
				Utils.UploadFilesFromDropZone(request, response, userDTO.ID, ColumnID, "Job_applicant_documentsServlet");
			}

//			else if(actionType.equals("getDTO"))
//			{
//
//				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.JOB_APPLICANT_documents_ADD))
//				{
//					getDTO(request, response);
//				}
//				else
//				{
//					System.out.println("Not going to  addJob_applicant_documents ");
//					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
//				}
//
//			}
//			else if(actionType.equals("edit"))
//			{
//
//				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.JOB_APPLICANT_documents_UPDATE))
//				{
//					addJob_applicant_documents(request, response, false, userDTO, isPermanentTable);
//				}
//				else
//				{
//					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
//				}
//			}
//			else if(actionType.equals("delete"))
//			{
//				deleteJob_applicant_documents(request, response, userDTO);
//			}
//			else if(actionType.equals("search"))
//			{
//				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.JOB_APPLICANT_documents_SEARCH))
//				{
//					searchJob_applicant_documents(request, response, true, "");
//				}
//				else
//				{
//					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
//				}
//			}
			else if(actionType.equals("getGeo"))
			{
				System.out.println("going to geoloc ");
				request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
			}

		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}

	private void getDTO(HttpServletRequest request, HttpServletResponse response)
	{
		try
		{
			System.out.println("In getDTO");
			Job_applicant_documentsDTO job_applicant_documentsDTO = job_applicant_documentsDAO.getDTOByID(Long.parseLong(request.getParameter("ID")));
			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");

			String encoded = this.gson.toJson(job_applicant_documentsDTO);
			System.out.println("json encoded data = " + encoded);
			out.print(encoded);
			out.flush();
		}
		catch (NumberFormatException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

//	private void addJob_applicant_documents(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, boolean isPermanentTable) throws IOException
//	{
//		// TODO Auto-generated method stub
//		try
//		{
//			request.setAttribute("failureMessage", "");
//			System.out.println("%%%% addJob_applicant_documents");
//			String path = getServletContext().getRealPath("/img2/");
//			Job_applicant_documentsDTO job_applicant_documentsDTO;
//			SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
//
//			if(addFlag == true)
//			{
//				job_applicant_documentsDTO = new Job_applicant_documentsDTO();
//			}
//			else
//			{
//				job_applicant_documentsDTO = (Job_applicant_documentsDTO)job_applicant_documentsDAO.getDTOByID(Long.parseLong(request.getParameter("iD")));
//			}
//
//			String Value = "";
//
//			Value = request.getParameter("jobId");
//
//			if(Value != null)
//			{
//				Value = Jsoup.clean(Value,Whitelist.simpleText());
//			}
//			System.out.println("jobId = " + Value);
//			if(Value != null && !Value.equalsIgnoreCase(""))
//			{
//
//				job_applicant_documentsDTO.jobId = Long.parseLong(Value);
//			}
//			else
//			{
//				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//			}
//
//			Value = request.getParameter("employeeRecordsId");
//
//			if(Value != null)
//			{
//				Value = Jsoup.clean(Value,Whitelist.simpleText());
//			}
//			System.out.println("employeeRecordsId = " + Value);
//			if(Value != null && !Value.equalsIgnoreCase(""))
//			{
//
//				job_applicant_documentsDTO.employeeRecordsId = Long.parseLong(Value);
//			}
//			else
//			{
//				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//			}
//
//			Value = request.getParameter("documentsName");
//
//			if(Value != null)
//			{
//				Value = Jsoup.clean(Value,Whitelist.simpleText());
//			}
//			System.out.println("documentsName = " + Value);
//			if(Value != null && !Value.equalsIgnoreCase(""))
//			{
//
//				job_applicant_documentsDTO.documentsName = (Value);
//			}
//			else
//			{
//				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//			}
//
//			Value = request.getParameter("issuingOrganization");
//
//			if(Value != null)
//			{
//				Value = Jsoup.clean(Value,Whitelist.simpleText());
//			}
//			System.out.println("issuingOrganization = " + Value);
//			if(Value != null && !Value.equalsIgnoreCase(""))
//			{
//
//				job_applicant_documentsDTO.issuingOrganization = (Value);
//			}
//			else
//			{
//				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//			}
//
//			Value = request.getParameter("issuingInstituteAddress");
//
//			if(Value != null)
//			{
//				Value = Jsoup.clean(Value,Whitelist.simpleText());
//			}
//			System.out.println("issuingInstituteAddress = " + Value);
//			if(Value != null && !Value.equalsIgnoreCase(""))
//			{
//
//
//				{
//					StringTokenizer tok3=new StringTokenizer(Value, ":");
//					int i = 0;
//					String addressDetails = "", address_id = "";
//					while(tok3.hasMoreElements())
//					{
//						if(i == 0)
//						{
//							address_id = tok3.nextElement() + "";
//						}
//						else if(i == 1)
//						{
//							addressDetails = tok3.nextElement() + "";
//						}
//						i ++;
//					}
//					try
//					{
//						if(address_id.matches("-?\\d+"))
//						{
//							job_applicant_documentsDTO.issuingInstituteAddress
//							= address_id + ":"
//							+ GeoLocationDAO2.getLocationText(Integer.parseInt(address_id), "English") + ":"
//							+ addressDetails + ":"
//							+ GeoLocationDAO2.getLocationText(Integer.parseInt(address_id), "Bangla");
//						}
//					}
//					catch(Exception ex)
//					{
//						ex.printStackTrace();
//					}
//				}
//
//			}
//			else
//			{
//				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//			}
//
//			Value = request.getParameter("validFrom");
//
//			if(Value != null)
//			{
//				Value = Jsoup.clean(Value,Whitelist.simpleText());
//			}
//			System.out.println("validFrom = " + Value);
//			if(Value != null && !Value.equalsIgnoreCase(""))
//			{
//
//				job_applicant_documentsDTO.validFrom = (Value);
//			}
//			else
//			{
//				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//			}
//
//			Value = request.getParameter("hasExpiryCat");
//
//			if(Value != null)
//			{
//				Value = Jsoup.clean(Value,Whitelist.simpleText());
//			}
//			System.out.println("hasExpiryCat = " + Value);
//			if(Value != null && !Value.equalsIgnoreCase(""))
//			{
//
//				job_applicant_documentsDTO.hasExpiryCat = Integer.parseInt(Value);
//			}
//			else
//			{
//				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//			}
//
//			Value = request.getParameter("validUpto");
//
//			if(Value != null)
//			{
//				Value = Jsoup.clean(Value,Whitelist.simpleText());
//			}
//			System.out.println("validUpto = " + Value);
//			if(Value != null && !Value.equalsIgnoreCase(""))
//			{
//
//				job_applicant_documentsDTO.validUpto = (Value);
//			}
//			else
//			{
//				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//			}
//
//			Value = request.getParameter("credentialId");
//
//			if(Value != null)
//			{
//				Value = Jsoup.clean(Value,Whitelist.simpleText());
//			}
//			System.out.println("credentialId = " + Value);
//			if(Value != null && !Value.equalsIgnoreCase(""))
//			{
//
//				job_applicant_documentsDTO.credentialId = (Value);
//			}
//			else
//			{
//				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//			}
//
//			Value = request.getParameter("filesDropzone");
//
//			if(Value != null)
//			{
//				Value = Jsoup.clean(Value,Whitelist.simpleText());
//			}
//			System.out.println("filesDropzone = " + Value);
//			if(Value != null && !Value.equalsIgnoreCase(""))
//			{
//
//
//				System.out.println("filesDropzone = " + Value);
//				if(Value != null && !Value.equalsIgnoreCase(""))
//				{
//					job_applicant_documentsDTO.filesDropzone = Long.parseLong(Value);
//				}
//				else
//				{
//					System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//				}
//
//				if(addFlag == false)
//				{
//					String filesDropzoneFilesToDelete = request.getParameter("filesDropzoneFilesToDelete");
//					String deleteArray[] = filesDropzoneFilesToDelete.split(",");
//					for(int i = 0; i < deleteArray.length; i ++)
//					{
//						System.out.println("going to delete " + deleteArray[i]);
//						if(i>0)
//						{
//							filesDAO.delete(Long.parseLong(deleteArray[i]));
//						}
//					}
//				}
//			}
//			else
//			{
//				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//			}
//
//			if(addFlag)
//			{
//				job_applicant_documentsDTO.insertedByUserId = userDTO.ID;
//			}
//
//
//			if(addFlag)
//			{
//				Calendar c = Calendar.getInstance();
//				c.set(Calendar.HOUR_OF_DAY, 0);
//				c.set(Calendar.MINUTE, 0);
//				c.set(Calendar.SECOND, 0);
//				c.set(Calendar.MILLISECOND, 0);
//
//				job_applicant_documentsDTO.insertionDate = c.getTimeInMillis();
//			}
//
//
//			Value = request.getParameter("modifiedBy");
//
//			if(Value != null)
//			{
//				Value = Jsoup.clean(Value,Whitelist.simpleText());
//			}
//			System.out.println("modifiedBy = " + Value);
//			if(Value != null && !Value.equalsIgnoreCase(""))
//			{
//
//				job_applicant_documentsDTO.modifiedBy = (Value);
//			}
//			else
//			{
//				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//			}
//
//			System.out.println("Done adding  addJob_applicant_documents dto = " + job_applicant_documentsDTO);
//			long returnedID = -1;
//
//			if(isPermanentTable == false) //add new row for validation and make the old row outdated
//			{
//				job_applicant_documentsDAO.setIsDeleted(job_applicant_documentsDTO.iD, CommonDTO.OUTDATED);
//				returnedID = job_applicant_documentsDAO.add(job_applicant_documentsDTO);
//				job_applicant_documentsDAO.setIsDeleted(returnedID, CommonDTO.WAITING_FOR_APPROVAL);
//			}
//			else if(addFlag == true)
//			{
//				returnedID = job_applicant_documentsDAO.manageWriteOperations(job_applicant_documentsDTO, SessionConstants.INSERT, -1, userDTO);
//			}
//			else
//			{
//				returnedID = job_applicant_documentsDAO.manageWriteOperations(job_applicant_documentsDTO, SessionConstants.UPDATE, -1, userDTO);
//			}
//
//
//
//
//
//
//
//
//
//			if(isPermanentTable)
//			{
//				String inPlaceSubmit = (String)request.getParameter("inplacesubmit");
//
//				if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
//				{
//					getJob_applicant_documents(request, response, returnedID);
//				}
//				else
//				{
//					response.sendRedirect("Job_applicant_documentsServlet?actionType=search");
//				}
//			}
//			else
//			{
//				commonRequestHandler.validate(job_applicant_documentsDAO.getDTOByID(returnedID), request, response, userDTO);
//			}
//
//		}
//		catch (Exception e)
//		{
//			e.printStackTrace();
//		}
//	}









	private void deleteJob_applicant_documents(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO) throws IOException
	{
		try
		{
			String[] IDsToDelete = request.getParameterValues("ID");
			for(int i = 0; i < IDsToDelete.length; i ++)
			{
				long id = Long.parseLong(IDsToDelete[i]);
				System.out.println("------ DELETING " + IDsToDelete[i]);


				Job_applicant_documentsDTO job_applicant_documentsDTO = job_applicant_documentsDAO.getDTOByID(id);
				job_applicant_documentsDAO.manageWriteOperations(job_applicant_documentsDTO, SessionConstants.DELETE, id, userDTO);
				response.sendRedirect("Job_applicant_documentsServlet?actionType=search");

			}
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}

	}

//	private void getJob_applicant_documents(HttpServletRequest request, HttpServletResponse response, long id) throws ServletException, IOException
//	{
//		System.out.println("in getJob_applicant_documents");
//		Job_applicant_documentsDTO job_applicant_documentsDTO = null;
//		try
//		{
//			job_applicant_documentsDTO = (Job_applicant_documentsDTO)job_applicant_documentsDAO.getDTOByID(id);
//			request.setAttribute("ID", job_applicant_documentsDTO.iD);
//			request.setAttribute("job_applicant_documentsDTO",job_applicant_documentsDTO);
//			request.setAttribute("job_applicant_documentsDAO",job_applicant_documentsDAO);
//
//			String URL= "";
//
//			String inPlaceEdit = (String)request.getParameter("inplaceedit");
//			String inPlaceSubmit = (String)request.getParameter("inplacesubmit");
//			String getBodyOnly = (String)request.getParameter("getBodyOnly");
//
//			if(inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase(""))
//			{
//				URL = "job_applicant_documents/job_applicant_documentsInPlaceEdit.jsp";
//				request.setAttribute("inplaceedit","");
//			}
//			else if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
//			{
//				URL = "job_applicant_documents/job_applicant_documentsSearchRow.jsp";
//				request.setAttribute("inplacesubmit","");
//			}
//			else
//			{
//				if(getBodyOnly  != null && !getBodyOnly.equalsIgnoreCase(""))
//				{
//					URL = "job_applicant_documents/job_applicant_documentsEditBody.jsp?actionType=edit";
//				}
//				else
//				{
//					URL = "job_applicant_documents/job_applicant_documentsEdit.jsp?actionType=edit";
//				}
//			}
//
//			RequestDispatcher rd = request.getRequestDispatcher(URL);
//			rd.forward(request, response);
//		}
//		catch (NumberFormatException e)
//		{
//			e.printStackTrace();
//		}
//		catch (Exception e)
//		{
//			e.printStackTrace();
//		}
//	}


//	private void getJob_applicant_documents(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
//	{
//		getJob_applicant_documents(request, response, Long.parseLong(request.getParameter("ID")));
//	}

//	private void searchJob_applicant_documents(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException
//	{
//		System.out.println("in  searchJob_applicant_documents 1");
//		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
//		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
//		String ajax = (String)request.getParameter("ajax");
//		boolean hasAjax = false;
//		if(ajax != null && !ajax.equalsIgnoreCase(""))
//		{
//			hasAjax = true;
//		}
//		System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);
//
//        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
//			SessionConstants.NAV_JOB_APPLICANT_documents,
//			request,
//			job_applicant_documentsDAO,
//			SessionConstants.VIEW_JOB_APPLICANT_documents,
//			SessionConstants.SEARCH_JOB_APPLICANT_documents,
//			tableName,
//			isPermanent,
//			userDTO,
//			filter,
//			true);
//        try
//        {
//			System.out.println("trying to dojob");
//            rnManager.doJob(loginDTO);
//        }
//        catch(Exception e)
//        {
//			System.out.println("failed to dojob" + e);
//        }
//
//		request.setAttribute("job_applicant_documentsDAO",job_applicant_documentsDAO);
//        RequestDispatcher rd;
//        if(!isPermanent)
//        {
//        	if(hasAjax == false)
//	        {
//	        	System.out.println("Going to job_applicant_documents/job_applicant_documentsApproval.jsp");
//	        	rd = request.getRequestDispatcher("job_applicant_documents/job_applicant_documentsApproval.jsp");
//	        }
//	        else
//	        {
//	        	System.out.println("Going to job_applicant_documents/job_applicant_documentsApprovalForm.jsp");
//	        	rd = request.getRequestDispatcher("job_applicant_documents/job_applicant_documentsApprovalForm.jsp");
//	        }
//        }
//        else
//        {
//	        if(hasAjax == false)
//	        {
//	        	System.out.println("Going to job_applicant_documents/job_applicant_documentsSearch.jsp");
//	        	rd = request.getRequestDispatcher("job_applicant_documents/job_applicant_documentsSearch.jsp");
//	        }
//	        else
//	        {
//	        	System.out.println("Going to job_applicant_documents/job_applicant_documentsSearchForm.jsp");
//	        	rd = request.getRequestDispatcher("job_applicant_documents/job_applicant_documentsSearchForm.jsp");
//	        }
//        }
//		rd.forward(request, response);
//	}

}

