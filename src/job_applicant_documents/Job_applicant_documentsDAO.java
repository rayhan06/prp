package job_applicant_documents;

import common.ConnectionAndStatementUtil;
import dbm.DBMR;
import dbm.DBMW;
import org.apache.log4j.Logger;
import repository.RepositoryManager;
import user.UserDTO;
import util.CommonDTO;
import util.NavigationService4;
import vm_vehicle_office_assignment.Vm_vehicle_office_assignmentDTO;

import java.sql.*;
import java.util.*;

public class Job_applicant_documentsDAO extends NavigationService4
{

	Logger logger = Logger.getLogger(getClass());


	public Job_applicant_documentsDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";
		// commonMaps = new Job_applicant_documentsMAPS(tableName);
	}

	@Override
	public Collection getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO) {
		return null;
	}

	public Job_applicant_documentsDAO()
	{
		this("job_applicant_documents");
	}

	public Job_applicant_documentsDTO build(ResultSet rs)
	{
		try
		{
			Job_applicant_documentsDTO job_applicant_documentsDTO = new Job_applicant_documentsDTO();
			job_applicant_documentsDTO.iD = rs.getLong("ID");
			job_applicant_documentsDTO.jobApplicantId = rs.getLong("job_applicant_id");
			job_applicant_documentsDTO.recruitmentJobSpecificFilesId = rs.getLong("recruitment_job_specific_files_id");
			job_applicant_documentsDTO.jobId = rs.getLong("job_id");
			job_applicant_documentsDTO.isChecked = rs.getBoolean("is_checked");
			job_applicant_documentsDTO.filesDropzone = rs.getLong("files_dropzone");
			job_applicant_documentsDTO.insertedByUserId = rs.getLong("inserted_by_user_id");
			job_applicant_documentsDTO.insertionDate = rs.getLong("insertion_date");
			job_applicant_documentsDTO.modifiedBy = rs.getString("modified_by");
			job_applicant_documentsDTO.isDeleted = rs.getInt("isDeleted");
			job_applicant_documentsDTO.lastModificationTime = rs.getLong("lastModificationTime");
			return job_applicant_documentsDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}

	public List<Job_applicant_documentsDTO> getJob_applicant_documentsDTOListByjobApplicantIdAndJobId(long appId, long jobId) throws Exception{

		String sql = "SELECT * FROM job_applicant_documents where isDeleted=0 and job_applicant_id = ? "
				+ " and job_id = ? ";
		logger.debug("sql " + sql);
		return ConnectionAndStatementUtil.getListOfT(sql, Arrays.asList(appId, jobId),this::build);
	}
	
	
	

		
	

	//need another getter for repository
	public Job_applicant_documentsDTO getDTOByID (long ID)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID = ? ";
		Job_applicant_documentsDTO job_applicant_documentsDTO =
				ConnectionAndStatementUtil.getT(sql, Arrays.asList(ID), this::build);
		return job_applicant_documentsDTO;
	}
	
	
	public List<Job_applicant_documentsDTO> getDTOs(Collection recordIDs){
		if(recordIDs.isEmpty()){
			return new ArrayList<>();
		}
		String sql = "SELECT * FROM " + tableName + " WHERE ID IN ( ";
		for(int i = 0;i<recordIDs.size();i++){
			if(i!=0){
				sql+=",";
			}
			sql+= " ? ";
		}
		sql+=")  order by lastModificationTime desc";
		printSql(sql);
		return ConnectionAndStatementUtil.getListOfT(sql, Arrays.asList(recordIDs.toArray()),this::build);
	
	}
	
	

	
	
	
	//add repository
	public List<Job_applicant_documentsDTO> getAllJob_applicant_documents (boolean isFirstReload)
    {

		String sql = "SELECT * FROM job_applicant_documents";
		sql += " WHERE ";
	

		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by job_applicant_documents.lastModificationTime desc";
		printSql(sql);
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);
    }

	public List<Job_applicant_documentsDTO> getDTOByJobApplicantID (long ID)
	{
		String sql = "SELECT * FROM job_applicant_documents";
		sql += " WHERE job_applicant_id = ? " ;
		sql+=" AND isDeleted =  0";
		sql += " order by job_applicant_documents.lastModificationTime desc";
		printSql(sql);

		return ConnectionAndStatementUtil.getListOfT(sql, Arrays.asList(ID),this::build);
	}
				
}
	