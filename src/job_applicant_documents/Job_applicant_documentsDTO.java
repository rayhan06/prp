package job_applicant_documents;

import util.CommonDTO;


public class Job_applicant_documentsDTO extends CommonDTO
{
    public Boolean isChecked = false;
    public long jobApplicantId = 0;
    public long jobId = 0;
	public long recruitmentJobSpecificFilesId = 0;
	public long filesDropzone = -1;
	public long insertedByUserId = 0;
	public long insertionDate = 0;
    public String modifiedBy = "";
	
	
    @Override
	public String toString() {
            return "Job_applicant_documentsDTO[" +
            " iD = " + iD +
            " jobApplicantId = " + jobApplicantId +
            " jobId = " + jobId +
            " recruitmentJobSpecificFilesId = " + recruitmentJobSpecificFilesId +
            " isChecked = " + isChecked +
            " filesDropzone = " + filesDropzone +
            " insertedByUserId = " + insertedByUserId +
            " insertionDate = " + insertionDate +
            " modifiedBy = " + modifiedBy +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}