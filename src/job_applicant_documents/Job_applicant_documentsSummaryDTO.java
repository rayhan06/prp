package job_applicant_documents;

import files.FilesDTO;
import util.CommonDTO;

import java.util.ArrayList;
import java.util.List;


public class Job_applicant_documentsSummaryDTO extends CommonDTO
{
    public Boolean isChecked = false;
    public long jobApplicantId = 0;
    public long jobId = 0;
	public long recruitmentJobSpecificFilesId = 0;
	public long filesDropzone = 0;
	public long insertedByUserId = 0;
	public long insertionDate = 0;
    public String modifiedBy = "";

    public List<FilesDTO> filesDropzoneFiles = new ArrayList();

    @Override
	public String toString() {
            return "Job_applicant_documentsDTO[" +
            " iD = " + iD +
            " jobApplicantId = " + jobApplicantId +
            " jobId = " + jobId +
            " recruitmentJobSpecificFilesId = " + recruitmentJobSpecificFilesId +
            " isChecked = " + isChecked +
            " filesDropzone = " + filesDropzone +
            " insertedByUserId = " + insertedByUserId +
            " insertionDate = " + insertionDate +
            " modifiedBy = " + modifiedBy +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}