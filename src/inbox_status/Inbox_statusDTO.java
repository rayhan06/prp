package inbox_status;
import java.util.*; 
import util.*; 


public class Inbox_statusDTO extends CommonDTO
{

	public long ID = 0;
	public long statusLanguageId = 0;
	
	
    @Override
	public String toString() {
            return "$Inbox_statusDTO[" +
            " id = " + ID +
            " statusLanguageId = " + statusLanguageId +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}