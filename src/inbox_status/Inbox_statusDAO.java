package inbox_status;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;


import org.apache.log4j.Logger;

import dbm.*;
import inbox.InboxDTO;
import repository.RepositoryManager;

import util.*;
import pb.*;
import approval_module_map.*;

public class Inbox_statusDAO  extends NavigationService3
{
	
	Logger logger = Logger.getLogger(getClass());

	
	public Inbox_statusDAO(String tableName, String tempTableName, Approval_module_mapDTO approval_module_mapDTO)
	{
		super(tableName, tempTableName, approval_module_mapDTO);		
	}
	
	public Inbox_statusDAO()
	{
		super("inbox_status", null, null);		
	}
	
	
	
	public long add(CommonDTO commonDTO, String tableName, TempTableDTO tempTableDTO) throws Exception
	{
		
		Inbox_statusDTO inbox_statusDTO = (Inbox_statusDTO)commonDTO;
		
		Connection connection = null;
		PreparedStatement ps = null;

		long lastModificationTime = System.currentTimeMillis();	

		try{
			connection = DBMW.getInstance().getConnection();
			
			if(connection == null)
			{
				System.out.println("nullconn");
			}

			inbox_statusDTO.iD = DBMW.getInstance().getNextSequenceId(tableName);

			String sql = "INSERT INTO " + tableName;
			
			sql += " (";
			sql += "id";
			sql += ", ";
			sql += "status_language_id";
			sql += ", ";
			sql += "isDeleted";
			sql += ", ";
			sql += "lastModificationTime";
			if(tempTableDTO!=null)
			{
				sql += ", permanent_table_id, operation_type, approval_path_type, approval_order";
			}
			sql += ")";
			
			
            sql += " VALUES(";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			if(tempTableDTO!=null)
			{
				sql += ", " + tempTableDTO.permanent_table_id + ", " + tempTableDTO.operation_type + ", " + tempTableDTO.approval_path_type + ", " + tempTableDTO.approval_order;
			}
			sql += ")";
				
			printSql(sql);

			ps = connection.prepareStatement(sql);
			
			
			

			int index = 1;

			ps.setObject(index++,inbox_statusDTO.ID);
			ps.setObject(index++,inbox_statusDTO.statusLanguageId);
			ps.setObject(index++,inbox_statusDTO.isDeleted);
			ps.setObject(index++, lastModificationTime);
			
			System.out.println(ps);
			ps.execute();
			
			
			recordUpdateTime(connection,  lastModificationTime, tableName);

		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if (ps != null) {
					ps.close();
				}
			} catch(Exception e){}
			try{
				if(connection != null){
					DBMW.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		return inbox_statusDTO.ID;		
	}
		
	

	//need another getter for repository
	public CommonDTO getDTOByID (long ID, String tableName) throws Exception
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Inbox_statusDTO inbox_statusDTO = null;
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
			
            sql += " WHERE id=" + ID;
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			if(rs.next()){
				inbox_statusDTO = new Inbox_statusDTO();

				inbox_statusDTO.ID = rs.getLong("ID");
				inbox_statusDTO.statusLanguageId = rs.getLong("status_language_id");
				inbox_statusDTO.isDeleted = rs.getInt("isDeleted");
				inbox_statusDTO.lastModificationTime = rs.getLong("lastModificationTime");

			}			
			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DBMR.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
		return inbox_statusDTO;
	}
	
	public long update(CommonDTO commonDTO, String tableName) throws Exception
	{		
		Inbox_statusDTO inbox_statusDTO = (Inbox_statusDTO)commonDTO;
		
		Connection connection = null;
		PreparedStatement ps = null;

		long lastModificationTime = System.currentTimeMillis();	
		try{
			connection = DBMW.getInstance().getConnection();

			String sql = "UPDATE " + tableName;
			
			sql += " SET ";
			sql += "status_language_id=?";
			sql += ", lastModificationTime = "	+ lastModificationTime + "";
            sql += " WHERE id = " + inbox_statusDTO.ID;
				
			printSql(sql);

			ps = connection.prepareStatement(sql);
			
			

			int index = 1;
			
			ps.setObject(index++,inbox_statusDTO.statusLanguageId);
			System.out.println(ps);
			ps.executeUpdate();
			


			
			recordUpdateTime(connection,  lastModificationTime, tableName);

						
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if (ps != null) {
					ps.close();
				}
			} catch(Exception e){}
			try{
				if(connection != null){
					DBMW.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		return inbox_statusDTO.ID;
	}
	
	public List<Inbox_statusDTO> getDTOs(Collection recordIDs)
	{
		return getDTOs(recordIDs, tableName);
	}
	
	
	
	public List<Inbox_statusDTO> getActionByRoleID(long inRoleID, long inCurrentStatus){
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Inbox_statusDTO inbox_statusDTO = null;
		List<Inbox_statusDTO> inbox_statusDTOList = new ArrayList<>();
		
		try{
			
			String sql = "SELECT * FROM inbox_status WHERE id IN ( " +					 
					"SELECT c.inbox_status FROM  inbox_action_roles b, inbox_actions c WHERE c.id = b.action_id and b.inbox_status_id = " +  inCurrentStatus + " and b.role_id = " + inRoleID + ")";
			
		//	sql+=")  order by lastModificationTime desc";
			
			printSql(sql);
			
			logger.debug("sql " + sql);
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				inbox_statusDTO = new Inbox_statusDTO();
				inbox_statusDTO.ID = rs.getLong("ID");
				inbox_statusDTO.statusLanguageId = rs.getLong("status_language_id");
				inbox_statusDTO.isDeleted = rs.getInt("isDeleted");
				inbox_statusDTO.lastModificationTime = rs.getLong("lastModificationTime");
				System.out.println("got this DTO: " + inbox_statusDTO);
				
				inbox_statusDTOList.add(inbox_statusDTO);
			}			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DBMR.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
		return inbox_statusDTOList;
	
	}
	
	

	
	
	public List<Inbox_statusDTO> getDTOs(Collection recordIDs, String tableName){
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Inbox_statusDTO inbox_statusDTO = null;
		List<Inbox_statusDTO> inbox_statusDTOList = new ArrayList<>();
		if(recordIDs.isEmpty()){
			return inbox_statusDTOList;
		}
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
            
            sql += " WHERE id IN ( ";

			for(int i = 0;i<recordIDs.size();i++){
				if(i!=0){
					sql+=",";
				}
				sql+=((ArrayList)recordIDs).get(i);
			}
			sql+=")  order by lastModificationTime desc";
			
			printSql(sql);
			
			logger.debug("sql " + sql);
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				inbox_statusDTO = new Inbox_statusDTO();
				inbox_statusDTO.ID = rs.getLong("ID");
				inbox_statusDTO.statusLanguageId = rs.getLong("status_language_id");
				inbox_statusDTO.isDeleted = rs.getInt("isDeleted");
				inbox_statusDTO.lastModificationTime = rs.getLong("lastModificationTime");
				System.out.println("got this DTO: " + inbox_statusDTO);
				
				inbox_statusDTOList.add(inbox_statusDTO);

			}			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DBMR.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
		return inbox_statusDTOList;
	
	}
	
	

	
	
	
	//add repository
	public List<Inbox_statusDTO> getAllInbox_status (boolean isFirstReload)
    {
		List<Inbox_statusDTO> inbox_statusDTOList = new ArrayList<>();

		String sql = "SELECT * FROM inbox_status";
		sql += " WHERE ";
	

		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by inbox_status.lastModificationTime desc";
		printSql(sql);
		
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;

		
		try{
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();
			rs = stmt.executeQuery(sql);
			

			while(rs.next()){
				Inbox_statusDTO inbox_statusDTO = new Inbox_statusDTO();
				inbox_statusDTO.ID = rs.getLong("ID");
				inbox_statusDTO.statusLanguageId = rs.getLong("status_language_id");
				inbox_statusDTO.isDeleted = rs.getInt("isDeleted");
				inbox_statusDTO.lastModificationTime = rs.getLong("lastModificationTime");
				
				inbox_statusDTOList.add(inbox_statusDTO);
			}			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			try{ 
				if (connection != null){ 
					DBMR.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}

		return inbox_statusDTOList;
    }
	
	public List<Inbox_statusDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset)
	{
		return getDTOs(p_searchCriteria, limit, offset, tableName, true, 0);
	}
	
	public List<Inbox_statusDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, String tableName, boolean isPermanentTable, long userApprovalPathType)
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		List<Inbox_statusDTO> inbox_statusDTOList = new ArrayList<>();

		try{
			
			String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, tableName, isPermanentTable, userApprovalPathType);
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				Inbox_statusDTO inbox_statusDTO = new Inbox_statusDTO();
				inbox_statusDTO.ID = rs.getLong("ID");
				inbox_statusDTO.statusLanguageId = rs.getLong("status_language_id");
				inbox_statusDTO.isDeleted = rs.getInt("isDeleted");
				inbox_statusDTO.lastModificationTime = rs.getLong("lastModificationTime");
				
				inbox_statusDTOList.add(inbox_statusDTO);
			}					
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DBMR.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
		return inbox_statusDTOList;
	
	}

		
	
	public String getSqlWithSearchCriteria(Hashtable p_searchCriteria, int limit, int offset, int category, String tableName, boolean isPermanentTable, long userApprovalPathType)
    {
		Inbox_statusMAPS maps = new Inbox_statusMAPS(tableName);
		String joinSQL = "";
		return getSqlWithSearchCriteria(p_searchCriteria, limit, offset, category, tableName, isPermanentTable, userApprovalPathType, maps, joinSQL);
    }			
}
	