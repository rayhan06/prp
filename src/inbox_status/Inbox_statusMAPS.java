package inbox_status;
import java.util.*; 
import util.*;


public class Inbox_statusMAPS extends CommonMaps
{	
	public Inbox_statusMAPS(String tableName)
	{
		
		java_allfield_type_map.put("status_language_id".toLowerCase(), "Long");

		java_anyfield_search_map.put(tableName + ".status_language_id".toLowerCase(), "Long");

		java_DTO_map.put("id".toLowerCase(), "id".toLowerCase());
		java_DTO_map.put("statusLanguageId".toLowerCase(), "statusLanguageId".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("status_language_id".toLowerCase(), "statusLanguageId".toLowerCase());

		java_Text_map.put("Id".toLowerCase(), "id".toLowerCase());
		java_Text_map.put("Status Language Id".toLowerCase(), "statusLanguageId".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}