package inbox_status;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class Inbox_statusRepository implements Repository {
	Inbox_statusDAO inbox_statusDAO = null;
	
	public void setDAO(Inbox_statusDAO inbox_statusDAO)
	{
		this.inbox_statusDAO = inbox_statusDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Inbox_statusRepository.class);
	Map<Long, Inbox_statusDTO>mapOfInbox_statusDTOToid;
	Map<Long, Set<Inbox_statusDTO> >mapOfInbox_statusDTOTostatusLanguageId;
	Map<Long, Set<Inbox_statusDTO> >mapOfInbox_statusDTOTolastModificationTime;


	static Inbox_statusRepository instance = null;  
	private Inbox_statusRepository(){
		mapOfInbox_statusDTOToid = new ConcurrentHashMap<>();
		mapOfInbox_statusDTOTostatusLanguageId = new ConcurrentHashMap<>();
		mapOfInbox_statusDTOTolastModificationTime = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Inbox_statusRepository getInstance(){
		if (instance == null){
			instance = new Inbox_statusRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(inbox_statusDAO == null)
		{
			return;
		}
		try {
			List<Inbox_statusDTO> inbox_statusDTOs = inbox_statusDAO.getAllInbox_status(reloadAll);
			for(Inbox_statusDTO inbox_statusDTO : inbox_statusDTOs) {
				Inbox_statusDTO oldInbox_statusDTO = getInbox_statusDTOByid(inbox_statusDTO.ID);
				if( oldInbox_statusDTO != null ) {
					mapOfInbox_statusDTOToid.remove(oldInbox_statusDTO.ID);
				
					if(mapOfInbox_statusDTOTostatusLanguageId.containsKey(oldInbox_statusDTO.statusLanguageId)) {
						mapOfInbox_statusDTOTostatusLanguageId.get(oldInbox_statusDTO.statusLanguageId).remove(oldInbox_statusDTO);
					}
					if(mapOfInbox_statusDTOTostatusLanguageId.get(oldInbox_statusDTO.statusLanguageId).isEmpty()) {
						mapOfInbox_statusDTOTostatusLanguageId.remove(oldInbox_statusDTO.statusLanguageId);
					}
					
					if(mapOfInbox_statusDTOTolastModificationTime.containsKey(oldInbox_statusDTO.lastModificationTime)) {
						mapOfInbox_statusDTOTolastModificationTime.get(oldInbox_statusDTO.lastModificationTime).remove(oldInbox_statusDTO);
					}
					if(mapOfInbox_statusDTOTolastModificationTime.get(oldInbox_statusDTO.lastModificationTime).isEmpty()) {
						mapOfInbox_statusDTOTolastModificationTime.remove(oldInbox_statusDTO.lastModificationTime);
					}
					
					
				}
				if(inbox_statusDTO.isDeleted == 0) 
				{
					
					mapOfInbox_statusDTOToid.put(inbox_statusDTO.ID, inbox_statusDTO);
				
					if( ! mapOfInbox_statusDTOTostatusLanguageId.containsKey(inbox_statusDTO.statusLanguageId)) {
						mapOfInbox_statusDTOTostatusLanguageId.put(inbox_statusDTO.statusLanguageId, new HashSet<>());
					}
					mapOfInbox_statusDTOTostatusLanguageId.get(inbox_statusDTO.statusLanguageId).add(inbox_statusDTO);
					
					if( ! mapOfInbox_statusDTOTolastModificationTime.containsKey(inbox_statusDTO.lastModificationTime)) {
						mapOfInbox_statusDTOTolastModificationTime.put(inbox_statusDTO.lastModificationTime, new HashSet<>());
					}
					mapOfInbox_statusDTOTolastModificationTime.get(inbox_statusDTO.lastModificationTime).add(inbox_statusDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Inbox_statusDTO> getInbox_statusList() {
		List <Inbox_statusDTO> inbox_statuss = new ArrayList<Inbox_statusDTO>(this.mapOfInbox_statusDTOToid.values());
		return inbox_statuss;
	}
	
	
	public Inbox_statusDTO getInbox_statusDTOByid( long id){
		return mapOfInbox_statusDTOToid.get(id);
	}
	
	
	public List<Inbox_statusDTO> getInbox_statusDTOBystatus_language_id(long status_language_id) {
		return new ArrayList<>( mapOfInbox_statusDTOTostatusLanguageId.getOrDefault(status_language_id,new HashSet<>()));
	}
	
	
	public List<Inbox_statusDTO> getInbox_statusDTOBylastModificationTime(long lastModificationTime) {
		return new ArrayList<>( mapOfInbox_statusDTOTolastModificationTime.getOrDefault(lastModificationTime,new HashSet<>()));
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "inbox_status";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


