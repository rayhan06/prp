package employee_asset_history;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class Employee_asset_historyRepository implements Repository {
	Employee_asset_historyDAO employee_asset_historyDAO = null;
	
	public void setDAO(Employee_asset_historyDAO employee_asset_historyDAO)
	{
		this.employee_asset_historyDAO = employee_asset_historyDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Employee_asset_historyRepository.class);
	Map<Long, Employee_asset_historyDTO>mapOfEmployee_asset_historyDTOToiD;
	Map<Long, Set<Employee_asset_historyDTO> >mapOfEmployee_asset_historyDTOToassetListId;
	Map<Long, Set<Employee_asset_historyDTO> >mapOfEmployee_asset_historyDTOToemployeeRecordsId;
	Map<String, Set<Employee_asset_historyDTO> >mapOfEmployee_asset_historyDTOToremarks;
	Map<Integer, Set<Employee_asset_historyDTO> >mapOfEmployee_asset_historyDTOToassignOrRevoke;
	Map<Long, Set<Employee_asset_historyDTO> >mapOfEmployee_asset_historyDTOToinsertionDate;
	Map<String, Set<Employee_asset_historyDTO> >mapOfEmployee_asset_historyDTOToinsertedBy;
	Map<String, Set<Employee_asset_historyDTO> >mapOfEmployee_asset_historyDTOTomodifiedBy;
	Map<Long, Set<Employee_asset_historyDTO> >mapOfEmployee_asset_historyDTOTolastModificationTime;


	static Employee_asset_historyRepository instance = null;  
	private Employee_asset_historyRepository(){
		mapOfEmployee_asset_historyDTOToiD = new ConcurrentHashMap<>();
		mapOfEmployee_asset_historyDTOToassetListId = new ConcurrentHashMap<>();
		mapOfEmployee_asset_historyDTOToemployeeRecordsId = new ConcurrentHashMap<>();
		mapOfEmployee_asset_historyDTOToremarks = new ConcurrentHashMap<>();
		mapOfEmployee_asset_historyDTOToassignOrRevoke = new ConcurrentHashMap<>();
		mapOfEmployee_asset_historyDTOToinsertionDate = new ConcurrentHashMap<>();
		mapOfEmployee_asset_historyDTOToinsertedBy = new ConcurrentHashMap<>();
		mapOfEmployee_asset_historyDTOTomodifiedBy = new ConcurrentHashMap<>();
		mapOfEmployee_asset_historyDTOTolastModificationTime = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Employee_asset_historyRepository getInstance(){
		if (instance == null){
			instance = new Employee_asset_historyRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(employee_asset_historyDAO == null)
		{
			return;
		}
		try {
			List<Employee_asset_historyDTO> employee_asset_historyDTOs = employee_asset_historyDAO.getAllEmployee_asset_history(reloadAll);
			for(Employee_asset_historyDTO employee_asset_historyDTO : employee_asset_historyDTOs) {
				Employee_asset_historyDTO oldEmployee_asset_historyDTO = getEmployee_asset_historyDTOByID(employee_asset_historyDTO.iD);
				if( oldEmployee_asset_historyDTO != null ) {
					mapOfEmployee_asset_historyDTOToiD.remove(oldEmployee_asset_historyDTO.iD);
				
					if(mapOfEmployee_asset_historyDTOToassetListId.containsKey(oldEmployee_asset_historyDTO.assetListId)) {
						mapOfEmployee_asset_historyDTOToassetListId.get(oldEmployee_asset_historyDTO.assetListId).remove(oldEmployee_asset_historyDTO);
					}
					if(mapOfEmployee_asset_historyDTOToassetListId.get(oldEmployee_asset_historyDTO.assetListId).isEmpty()) {
						mapOfEmployee_asset_historyDTOToassetListId.remove(oldEmployee_asset_historyDTO.assetListId);
					}
					
					if(mapOfEmployee_asset_historyDTOToemployeeRecordsId.containsKey(oldEmployee_asset_historyDTO.employeeRecordsId)) {
						mapOfEmployee_asset_historyDTOToemployeeRecordsId.get(oldEmployee_asset_historyDTO.employeeRecordsId).remove(oldEmployee_asset_historyDTO);
					}
					if(mapOfEmployee_asset_historyDTOToemployeeRecordsId.get(oldEmployee_asset_historyDTO.employeeRecordsId).isEmpty()) {
						mapOfEmployee_asset_historyDTOToemployeeRecordsId.remove(oldEmployee_asset_historyDTO.employeeRecordsId);
					}
					
					if(mapOfEmployee_asset_historyDTOToremarks.containsKey(oldEmployee_asset_historyDTO.remarks)) {
						mapOfEmployee_asset_historyDTOToremarks.get(oldEmployee_asset_historyDTO.remarks).remove(oldEmployee_asset_historyDTO);
					}
					if(mapOfEmployee_asset_historyDTOToremarks.get(oldEmployee_asset_historyDTO.remarks).isEmpty()) {
						mapOfEmployee_asset_historyDTOToremarks.remove(oldEmployee_asset_historyDTO.remarks);
					}
					
					if(mapOfEmployee_asset_historyDTOToassignOrRevoke.containsKey(oldEmployee_asset_historyDTO.status)) {
						mapOfEmployee_asset_historyDTOToassignOrRevoke.get(oldEmployee_asset_historyDTO.status).remove(oldEmployee_asset_historyDTO);
					}
					if(mapOfEmployee_asset_historyDTOToassignOrRevoke.get(oldEmployee_asset_historyDTO.status).isEmpty()) {
						mapOfEmployee_asset_historyDTOToassignOrRevoke.remove(oldEmployee_asset_historyDTO.status);
					}
					
					if(mapOfEmployee_asset_historyDTOToinsertionDate.containsKey(oldEmployee_asset_historyDTO.insertionDate)) {
						mapOfEmployee_asset_historyDTOToinsertionDate.get(oldEmployee_asset_historyDTO.insertionDate).remove(oldEmployee_asset_historyDTO);
					}
					if(mapOfEmployee_asset_historyDTOToinsertionDate.get(oldEmployee_asset_historyDTO.insertionDate).isEmpty()) {
						mapOfEmployee_asset_historyDTOToinsertionDate.remove(oldEmployee_asset_historyDTO.insertionDate);
					}
					
					if(mapOfEmployee_asset_historyDTOToinsertedBy.containsKey(oldEmployee_asset_historyDTO.insertedBy)) {
						mapOfEmployee_asset_historyDTOToinsertedBy.get(oldEmployee_asset_historyDTO.insertedBy).remove(oldEmployee_asset_historyDTO);
					}
					if(mapOfEmployee_asset_historyDTOToinsertedBy.get(oldEmployee_asset_historyDTO.insertedBy).isEmpty()) {
						mapOfEmployee_asset_historyDTOToinsertedBy.remove(oldEmployee_asset_historyDTO.insertedBy);
					}
					
					if(mapOfEmployee_asset_historyDTOTomodifiedBy.containsKey(oldEmployee_asset_historyDTO.modifiedBy)) {
						mapOfEmployee_asset_historyDTOTomodifiedBy.get(oldEmployee_asset_historyDTO.modifiedBy).remove(oldEmployee_asset_historyDTO);
					}
					if(mapOfEmployee_asset_historyDTOTomodifiedBy.get(oldEmployee_asset_historyDTO.modifiedBy).isEmpty()) {
						mapOfEmployee_asset_historyDTOTomodifiedBy.remove(oldEmployee_asset_historyDTO.modifiedBy);
					}
					
					if(mapOfEmployee_asset_historyDTOTolastModificationTime.containsKey(oldEmployee_asset_historyDTO.lastModificationTime)) {
						mapOfEmployee_asset_historyDTOTolastModificationTime.get(oldEmployee_asset_historyDTO.lastModificationTime).remove(oldEmployee_asset_historyDTO);
					}
					if(mapOfEmployee_asset_historyDTOTolastModificationTime.get(oldEmployee_asset_historyDTO.lastModificationTime).isEmpty()) {
						mapOfEmployee_asset_historyDTOTolastModificationTime.remove(oldEmployee_asset_historyDTO.lastModificationTime);
					}
					
					
				}
				if(employee_asset_historyDTO.isDeleted == 0) 
				{
					
					mapOfEmployee_asset_historyDTOToiD.put(employee_asset_historyDTO.iD, employee_asset_historyDTO);
				
					if( ! mapOfEmployee_asset_historyDTOToassetListId.containsKey(employee_asset_historyDTO.assetListId)) {
						mapOfEmployee_asset_historyDTOToassetListId.put(employee_asset_historyDTO.assetListId, new HashSet<>());
					}
					mapOfEmployee_asset_historyDTOToassetListId.get(employee_asset_historyDTO.assetListId).add(employee_asset_historyDTO);
					
					if( ! mapOfEmployee_asset_historyDTOToemployeeRecordsId.containsKey(employee_asset_historyDTO.employeeRecordsId)) {
						mapOfEmployee_asset_historyDTOToemployeeRecordsId.put(employee_asset_historyDTO.employeeRecordsId, new HashSet<>());
					}
					mapOfEmployee_asset_historyDTOToemployeeRecordsId.get(employee_asset_historyDTO.employeeRecordsId).add(employee_asset_historyDTO);
					
					if( ! mapOfEmployee_asset_historyDTOToremarks.containsKey(employee_asset_historyDTO.remarks)) {
						mapOfEmployee_asset_historyDTOToremarks.put(employee_asset_historyDTO.remarks, new HashSet<>());
					}
					mapOfEmployee_asset_historyDTOToremarks.get(employee_asset_historyDTO.remarks).add(employee_asset_historyDTO);
					
					if( ! mapOfEmployee_asset_historyDTOToassignOrRevoke.containsKey(employee_asset_historyDTO.status)) {
						mapOfEmployee_asset_historyDTOToassignOrRevoke.put(employee_asset_historyDTO.status, new HashSet<>());
					}
					mapOfEmployee_asset_historyDTOToassignOrRevoke.get(employee_asset_historyDTO.status).add(employee_asset_historyDTO);
					
					if( ! mapOfEmployee_asset_historyDTOToinsertionDate.containsKey(employee_asset_historyDTO.insertionDate)) {
						mapOfEmployee_asset_historyDTOToinsertionDate.put(employee_asset_historyDTO.insertionDate, new HashSet<>());
					}
					mapOfEmployee_asset_historyDTOToinsertionDate.get(employee_asset_historyDTO.insertionDate).add(employee_asset_historyDTO);
					
					if( ! mapOfEmployee_asset_historyDTOToinsertedBy.containsKey(employee_asset_historyDTO.insertedBy)) {
						mapOfEmployee_asset_historyDTOToinsertedBy.put(employee_asset_historyDTO.insertedBy, new HashSet<>());
					}
					mapOfEmployee_asset_historyDTOToinsertedBy.get(employee_asset_historyDTO.insertedBy).add(employee_asset_historyDTO);
					
					if( ! mapOfEmployee_asset_historyDTOTomodifiedBy.containsKey(employee_asset_historyDTO.modifiedBy)) {
						mapOfEmployee_asset_historyDTOTomodifiedBy.put(employee_asset_historyDTO.modifiedBy, new HashSet<>());
					}
					mapOfEmployee_asset_historyDTOTomodifiedBy.get(employee_asset_historyDTO.modifiedBy).add(employee_asset_historyDTO);
					
					if( ! mapOfEmployee_asset_historyDTOTolastModificationTime.containsKey(employee_asset_historyDTO.lastModificationTime)) {
						mapOfEmployee_asset_historyDTOTolastModificationTime.put(employee_asset_historyDTO.lastModificationTime, new HashSet<>());
					}
					mapOfEmployee_asset_historyDTOTolastModificationTime.get(employee_asset_historyDTO.lastModificationTime).add(employee_asset_historyDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Employee_asset_historyDTO> getEmployee_asset_historyList() {
		List <Employee_asset_historyDTO> employee_asset_historys = new ArrayList<Employee_asset_historyDTO>(this.mapOfEmployee_asset_historyDTOToiD.values());
		return employee_asset_historys;
	}
	
	
	public Employee_asset_historyDTO getEmployee_asset_historyDTOByID( long ID){
		return mapOfEmployee_asset_historyDTOToiD.get(ID);
	}
	
	
	public List<Employee_asset_historyDTO> getEmployee_asset_historyDTOByasset_list_id(long asset_list_id) {
		return new ArrayList<>( mapOfEmployee_asset_historyDTOToassetListId.getOrDefault(asset_list_id,new HashSet<>()));
	}
	
	
	public List<Employee_asset_historyDTO> getEmployee_asset_historyDTOByemployee_records_id(long employee_records_id) {
		return new ArrayList<>( mapOfEmployee_asset_historyDTOToemployeeRecordsId.getOrDefault(employee_records_id,new HashSet<>()));
	}
	
	
	public List<Employee_asset_historyDTO> getEmployee_asset_historyDTOByremarks(String remarks) {
		return new ArrayList<>( mapOfEmployee_asset_historyDTOToremarks.getOrDefault(remarks,new HashSet<>()));
	}
	
	
	public List<Employee_asset_historyDTO> getEmployee_asset_historyDTOByassign_or_revoke(int assign_or_revoke) {
		return new ArrayList<>( mapOfEmployee_asset_historyDTOToassignOrRevoke.getOrDefault(assign_or_revoke,new HashSet<>()));
	}
	
	
	public List<Employee_asset_historyDTO> getEmployee_asset_historyDTOByinsertion_date(long insertion_date) {
		return new ArrayList<>( mapOfEmployee_asset_historyDTOToinsertionDate.getOrDefault(insertion_date,new HashSet<>()));
	}
	
	
	public List<Employee_asset_historyDTO> getEmployee_asset_historyDTOByinserted_by(String inserted_by) {
		return new ArrayList<>( mapOfEmployee_asset_historyDTOToinsertedBy.getOrDefault(inserted_by,new HashSet<>()));
	}
	
	
	public List<Employee_asset_historyDTO> getEmployee_asset_historyDTOBymodified_by(String modified_by) {
		return new ArrayList<>( mapOfEmployee_asset_historyDTOTomodifiedBy.getOrDefault(modified_by,new HashSet<>()));
	}
	
	
	public List<Employee_asset_historyDTO> getEmployee_asset_historyDTOBylastModificationTime(long lastModificationTime) {
		return new ArrayList<>( mapOfEmployee_asset_historyDTOTolastModificationTime.getOrDefault(lastModificationTime,new HashSet<>()));
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "employee_asset_history";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


