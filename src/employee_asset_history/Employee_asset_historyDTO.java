package employee_asset_history;
import java.util.*; 
import util.*; 


public class Employee_asset_historyDTO extends CommonDTO
{

	public long assetListId = -1;
	public long employeeRecordsId = -1;
    public String remarks = "";
	public int status = 0;
	public long insertionDate = -1;
    public String insertedBy = "";
    public String modifiedBy = "";
	
	
    @Override
	public String toString() {
            return "$Employee_asset_historyDTO[" +
            " iD = " + iD +
            " assetListId = " + assetListId +
            " employeeRecordsId = " + employeeRecordsId +
            " remarks = " + remarks +
            " assignOrRevoke = " + status +
            " insertionDate = " + insertionDate +
            " insertedBy = " + insertedBy +
            " modifiedBy = " + modifiedBy +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}