package employee_asset_history;
import java.util.*; 
import util.*;


public class Employee_asset_historyMAPS extends CommonMaps
{	
	public Employee_asset_historyMAPS(String tableName)
	{
		

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("assetListId".toLowerCase(), "assetListId".toLowerCase());
		java_DTO_map.put("employeeRecordsId".toLowerCase(), "employeeRecordsId".toLowerCase());
		java_DTO_map.put("remarks".toLowerCase(), "remarks".toLowerCase());
		java_DTO_map.put("assignOrRevoke".toLowerCase(), "assignOrRevoke".toLowerCase());
		java_DTO_map.put("insertionDate".toLowerCase(), "insertionDate".toLowerCase());
		java_DTO_map.put("insertedBy".toLowerCase(), "insertedBy".toLowerCase());
		java_DTO_map.put("modifiedBy".toLowerCase(), "modifiedBy".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("asset_list_id".toLowerCase(), "assetListId".toLowerCase());
		java_SQL_map.put("employee_records_id".toLowerCase(), "employeeRecordsId".toLowerCase());
		java_SQL_map.put("remarks".toLowerCase(), "remarks".toLowerCase());
		java_SQL_map.put("assign_or_revoke".toLowerCase(), "assignOrRevoke".toLowerCase());
		java_SQL_map.put("inserted_by".toLowerCase(), "insertedBy".toLowerCase());
		java_SQL_map.put("modified_by".toLowerCase(), "modifiedBy".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Asset List Id".toLowerCase(), "assetListId".toLowerCase());
		java_Text_map.put("Employee Records Id".toLowerCase(), "employeeRecordsId".toLowerCase());
		java_Text_map.put("Remarks".toLowerCase(), "remarks".toLowerCase());
		java_Text_map.put("Assign Or Revoke".toLowerCase(), "assignOrRevoke".toLowerCase());
		java_Text_map.put("Insertion Date".toLowerCase(), "insertionDate".toLowerCase());
		java_Text_map.put("Inserted By".toLowerCase(), "insertedBy".toLowerCase());
		java_Text_map.put("Modified By".toLowerCase(), "modifiedBy".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}