package bill_approval_mapping;

import bill_approval_history.ApprovableDTO;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import employee_offices.EmployeeOfficeDTO;
import employee_offices.EmployeeOfficeRepository;
import office_unit_organograms.*;
import office_units.OfficeUnitTypeEnum;
import office_units.Office_unitsDTO;
import office_units.Office_unitsRepository;
import sessionmanager.SessionConstants;
import util.CommonDTO;
import util.HttpRequestUtils;
import util.StringUtils;

import java.util.*;
import java.util.stream.Collectors;

@SuppressWarnings({"Duplicates"})
public class Bill_approval_mappingDTO extends CommonDTO {
    public long taskTypeId;
    public int level;
    OrganogramIdTypeEnum organogramIdTypeEnum;
    public long organogramId;
    public String designationName = "";
    public long designationId;
    public Set<Long> forOfficeUnitIds;
    public Set<Long> notForOfficeUnitIds;

    public long modifiedBy = -1;
    public long insertedBy = -1;
    public long insertionTime = SessionConstants.MIN_DATE;

    private static String getApproverNotFoundMessage(int level, boolean isLangEn) {
        String layerStr = String.format("%d", level + 1);
        return String.format(
                isLangEn ? "Approving officer not found (in layer %s)"
                         : "অনুমোদনকারী কর্মকর্তা পাওয়া যায়নি (%s-তম স্তরে)",
                isLangEn ? layerStr : StringUtils.convertToBanNumber(layerStr)
        );
    }

    private Long considerSubstituteForOrganogramId(Long organogramId) {
        if (organogramId == null) {
            return null;
        }
        OfficeUnitOrganograms organograms = OfficeUnitOrganogramsRepository.getInstance().getById(organogramId);
        if (organograms == null) {
            return null;
        }
        if (organograms.substituteOrganogramId == -1) {
            return organograms.id;
        }
        EmployeeOfficeDTO employeeOffice =
                EmployeeOfficeRepository
                        .getInstance()
                        .getByOfficeUnitOrganogramId(organograms.substituteOrganogramId);
        if (employeeOffice == null) {
            return null;
        }
        return employeeOffice.officeUnitOrganogramId;
    }

    private List<Long> getOfficeHeadOrgIdFromOfficeUnit(Office_unitsDTO officeUnitsDTO) throws RuntimeException {
        boolean isLangEn = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
        RuntimeException officeHeadNotFoundException = new RuntimeException(getApproverNotFoundMessage(level, isLangEn));
        if (officeUnitsDTO == null) {
            throw officeHeadNotFoundException;
        }
        if (officeUnitsDTO.isOfficeType(OfficeUnitTypeEnum.UNIT)) {
            officeUnitsDTO = Office_unitsRepository.getInstance()
                                                   .findClosestOfficeUnitOfAnOfficeType(officeUnitsDTO.iD, OfficeUnitTypeEnum.DIVISION);
            if (officeUnitsDTO == null) {
                throw officeHeadNotFoundException;
            }
        }
        OfficeUnitOrganograms organograms =
                OfficeUnitOrganogramsRepository
                        .getInstance()
                        .getOfficeUnitHeadWithActiveEmpStatus(officeUnitsDTO.iD, true);
        if (organograms == null) {
            throw officeHeadNotFoundException;
        }
        long organogramsId = organograms.substituteOrganogramId != -1
                             ? organograms.substituteOrganogramId
                             : organograms.id;
        EmployeeOfficeDTO officeUnitHeadOfficesDTO =
                EmployeeOfficeRepository
                        .getInstance()
                        .getByOfficeUnitOrganogramId(organogramsId);
        if (officeUnitHeadOfficesDTO == null) {
            throw officeHeadNotFoundException;
        }
        return Collections.singletonList(officeUnitHeadOfficesDTO.officeUnitOrganogramId);
    }

    private List<Long> getOrgIdListFromDesignation(Long officeUnitId) throws RuntimeException {
        boolean isLangEn = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
        RuntimeException invalidOfficeUnitIdException = new RuntimeException(getApproverNotFoundMessage(level, isLangEn));
        if (officeUnitId == null) {
            throw invalidOfficeUnitIdException;
        }
        String mappingDesignationKey = SameDesignationGroup.getDesignationKey(designationName);
        return OfficeUnitOrganogramsRepository
                .getInstance()
                .getOffice_unit_organogramsList()
                .stream()
                .filter(dto -> dto.office_unit_id == officeUnitId)
                .filter(dto -> mappingDesignationKey.equals(SameDesignationGroup.getDesignationKey(dto.designation_eng)))
                .map(dto -> dto.id)
                .map(this::considerSubstituteForOrganogramId)
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    private List<Long> getOrgIdListFromDesignationId(Long officeUnitId) throws RuntimeException {
        boolean isLangEn = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
        RuntimeException invalidOfficeUnitIdException = new RuntimeException(getApproverNotFoundMessage(level, isLangEn));
        if (officeUnitId == null) {
            throw invalidOfficeUnitIdException;
        }
        return OfficeUnitOrganogramsRepository
                .getInstance()
                .getOffice_unit_organogramsList()
                .stream()
                .filter(dto -> dto.office_unit_id == officeUnitId)
                .filter(dto -> dto.designationsId == designationId)
                .map(dto -> dto.id)
                .map(this::considerSubstituteForOrganogramId)
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    private List<Long> getOrgIdListOfBranchHeadOfDivision(Office_unitsDTO officeUnitsDTO) throws RuntimeException {
        boolean isLangEn = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
        RuntimeException approverMissingException = new RuntimeException(getApproverNotFoundMessage(level, isLangEn));
        if (officeUnitsDTO == null) {
            throw approverMissingException;
        }
        if (!officeUnitsDTO.hasBranchInAncestorNotSelf()) {
            return new ArrayList<>();
        }
        Office_unitsDTO branchOfficeUnitDto =
                Office_unitsRepository.getInstance()
                                      .findClosestOfficeUnitOfAnOfficeType(officeUnitsDTO.iD, OfficeUnitTypeEnum.BRANCH);
        return getOfficeHeadOrgIdFromOfficeUnit(branchOfficeUnitDto);
    }

    private List<Long> getOrgIdListOfCommitteeSecretary(Office_unitsDTO officeUnitsDTO) {
        boolean isLangEn = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
        RuntimeException approverMissingException = new RuntimeException(getApproverNotFoundMessage(level, isLangEn));
        if (officeUnitsDTO == null) {
            throw approverMissingException;
        }
        Committee_post_mapDTO committeePostMapDTO =
                Committee_post_mapRepository
                        .getInstance()
                        .copyByCommittee(officeUnitsDTO.iD);
        if (committeePostMapDTO == null) {
            throw approverMissingException;
        }
        Long organogramId = considerSubstituteForOrganogramId(committeePostMapDTO.replacementPostId);
        if (organogramId == null) {
            throw approverMissingException;
        }
        return Collections.singletonList(organogramId);
    }

    private boolean isApplicableForOfficeUnitId(Long officeUnitId) {
        if (forOfficeUnitIds != null && notForOfficeUnitIds != null) {
            return forOfficeUnitIds.contains(officeUnitId)
                   && !notForOfficeUnitIds.contains(officeUnitId);
        }
        if (forOfficeUnitIds != null) {
            return forOfficeUnitIds.contains(officeUnitId);
        }
        if (notForOfficeUnitIds != null) {
            return !notForOfficeUnitIds.contains(officeUnitId);
        }
        return true;
    }

    private List<Long> getOrganogramFromFixed(long fixedOrganogramId) {
        Long organogramId = considerSubstituteForOrganogramId(fixedOrganogramId);
        if (organogramId == null) {
            boolean isLangEn = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
            throw new RuntimeException(getApproverNotFoundMessage(level, isLangEn));
        }
        return Collections.singletonList(organogramId);
    }

    public List<Long> getOrganogramIdList(ApprovableDTO approvableDTO) throws RuntimeException {
        Office_unitsDTO officeUnitsDTO = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(approvableDTO.getOfficeUnitId());
        if (officeUnitsDTO == null) {
            throw new RuntimeException("No Office_unitsDTO found with approvableDTO.getOfficeUnitId()=" + approvableDTO.getOfficeUnitId());
        }
        if (!isApplicableForOfficeUnitId(officeUnitsDTO.iD)) {
            return new ArrayList<>();
        }
        switch (organogramIdTypeEnum) {
            case FIXED:
                return getOrganogramFromFixed(organogramId);
            case OFFICE_HEAD:
                return getOfficeHeadOrgIdFromOfficeUnit(officeUnitsDTO);
            case SUPERIOR_OFFICE_HEAD:
                Office_unitsDTO parentOfficeUnit =
                        Office_unitsRepository.getInstance()
                                              .getOffice_unitsDTOByID(officeUnitsDTO.parentUnitId);
                return getOfficeHeadOrgIdFromOfficeUnit(parentOfficeUnit);
            case DESIGNATION:
                return getOrgIdListFromDesignation(officeUnitsDTO.iD);
            case DESIGNATION_ID:
                return getOrgIdListFromDesignationId(officeUnitsDTO.iD);
            case BRANCH_HEAD_IF_DIVISION:
                return getOrgIdListOfBranchHeadOfDivision(officeUnitsDTO);
            case COMMITTEE_SECRETARY:
                return getOrgIdListOfCommitteeSecretary(officeUnitsDTO);
        }
        return new ArrayList<>();
    }

    private final static Gson gson = new Gson();

    public static Set<Long> getLongSetFromString(String arrayAsStr) {
        if (arrayAsStr == null) {
            return null;
        }
        return gson.fromJson(
                arrayAsStr,
                new TypeToken<Set<Long>>() {
                }.getType()
        );
    }

    public static <T> String getStringFromSet(Set<T> set) {
        if (set == null) {
            return null;
        }
        return gson.toJson(set);
    }
}
