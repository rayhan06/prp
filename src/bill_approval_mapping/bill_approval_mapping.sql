CREATE TABLE bill_approval_mapping
(
    ID                      BIGINT PRIMARY KEY,

    task_type_id            BIGINT,
    level                   INT,
    organogram_id_type      INT,
    organogram_id           BIGINT,
    designation_name        VARCHAR(1024),
    designation_id          BIGINT,
    for_office_unit_ids     TEXT,
    not_for_office_unit_ids TEXT,

    modified_by             BIGINT,
    lastModificationTime    BIGINT DEFAULT -62135791200000,
    inserted_by             BIGINT,
    insertion_time          BIGINT DEFAULT -62135791200000,
    isDeleted               INT    DEFAULT 0
) ENGINE = MyISAM
  DEFAULT CHARSET = utf8
  COLLATE = utf8_unicode_ci;

INSERT INTO bill_approval_mapping (ID, task_type_id, level, organogram_id_type, organogram_id,
                                   designation_name, designation_id, for_office_unit_ids, not_for_office_unit_ids,
                                   modified_by, lastModificationTime, inserted_by, insertion_time, isDeleted)
VALUES (1, 06, 0, 2, -1, '', -1, null,'[10,1102,14]', -1, 0, -1, 0, 0),
       (2, 06, 1, 5, -1, '', -1, null,'[10,1102,14]', -1, 0, -1, 0, 0),
       (3, 07, 0, 1, 261781, '', -1, null, null, -1, 0, -1, 0, 0),
       -- সচীবের দপ্তর এর জন্য আলাদা
       (17,06, 0, 1, 264000, '', -1,'[10]', null, -1, 0, -1, 0, 0),
       (18,06, 1, 1, 303001, '', -1,'[10]', null, -1, 0, -1, 0, 0),
       -- প্রধানমন্ত্রীর সংসদ ভবনস্থ কার্যালয় এর জন্য আলাদা
       (19,06, 0, 1, 261856, '', -1,'[1102]', null, -1, 0, -1, 0, 0),
       (20,06, 1, 1, 303001, '', -1,'[1102]', null, -1, 0, -1, 0, 0),
       -- সিনিয়র এসিস্ট্যান্ট সার্জেন্ট এট আর্মস এর জন্য আলাদা
       (23,06, 0, 1, 261714, '', -1,'[14]', null, -1, 0, -1, 0, 0),
       (24,06, 1, 1, 261706, '', -1,'[14]', null, -1, 0, -1, 0, 0),
       (25,06, 2, 1, 261694, '', -1,'[14]', null, -1, 0, -1, 0, 0),
       -- medical center
       (4, 08, 0, 1, 262115, '', -1, null, null, -1, 0, -1, 0, 0),
       (5, 08, 1, 1, 262105, '', -1, null, null, -1, 0, -1, 0, 0),

       (6, 09, 0, 4, -1, 'Private Secretary', -1, null, null, -1, 0, -1, 0, 0),
       (7, 09, 0, 4, -1, 'Assistant Private Secretary', -1, null, null, -1, 0, -1, 0, 0),
       -- standing committee
       (8, 10, 0, 7, -1, '', -1, null, '[1610,1611,1614,1618,1622,1641,1644]', -1, 0, -1, 0, 0),   -- Committee Secretary
       (21, 10, 0, 6, -1, '', 294, '[1610,1611,1614,1618,1622,1641,1644]', null, -1, 0, -1, 0, 0), -- PS
       (22, 10, 1, 7, -1, '', -1, '[1610,1611,1614,1618,1622,1641,1644]', null, -1, 0, -1, 0, 0), -- Committee Secretary
       -- MP
       (10, 11, 0, 2, -1, '', -1, null, null, -1, 0, -1, 0, 0),     -- MP
       (15, 11, 1, 1, 261781, '', -1, null, null, -1, 0, -1, 0, 0), -- Finance 1 Head
       (16, 11, 2, 1, 261768, '', -1, null, null, -1, 0, -1, 0, 0), -- Finance & Budget Head
       -- APS/DS for speaker
       (11, 12, 0, 4, -1, 'Assistant Private Secretary', -1, '[1]', null, -1, 0, -1, 0, 0),
       -- PS/DS for deputy speaker
       (12, 12, 0, 4, -1, 'Private Secretary', -1, '[2]', null, -1, 0, -1, 0, 0),
       -- APS For (Chief Whip & Whip )
       (14, 13, 0, 4, -1, 'Private Secretary', -1, null, null, -1, 0, -1, 0, 0),
       -- driver ot bill
       (26, 14, 0, 1, 262155, '', -1, null, null, -1, 0, -1, 0, 0), -- সিনিয়র সহকারী সচিব পরিবহন 262155
       (27, 14, 1, 1, 262109, '', -1, null, null, -1, 0, -1, 0, 0); -- উপ-সচিব প্রশাসন-১ 262109