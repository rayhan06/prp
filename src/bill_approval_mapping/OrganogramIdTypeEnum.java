package bill_approval_mapping;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

public enum OrganogramIdTypeEnum {
    FIXED(1),
    OFFICE_HEAD(2),
    SUPERIOR_OFFICE_HEAD(3),
    DESIGNATION(4),
    BRANCH_HEAD_IF_DIVISION(5),
    DESIGNATION_ID(6),
    COMMITTEE_SECRETARY(7),
    ;


    private static final Map<Integer, OrganogramIdTypeEnum> mapByValue;

    static {
        mapByValue = Arrays.stream(values())
                           .collect(Collectors.toMap(e -> e.value, e -> e));
    }

    public static OrganogramIdTypeEnum getFromValue(Integer value) {
        if (value == null) {
            return null;
        }
        return mapByValue.getOrDefault(value, null);
    }

    private final int value;

    OrganogramIdTypeEnum(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
