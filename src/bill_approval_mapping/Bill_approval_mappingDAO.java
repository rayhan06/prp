package bill_approval_mapping;

import common.CommonDAOService;
import common.ConnectionAndStatementUtil;
import org.apache.log4j.Logger;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings({"Duplicates"})

public class Bill_approval_mappingDAO implements CommonDAOService<Bill_approval_mappingDTO> {
    private static final Logger logger = Logger.getLogger(Bill_approval_mappingDAO.class);

    private static final String addSqlQuery =
            "INSERT INTO {tableName} (task_type_id,level,organogram_id_type,organogram_id,designation_name,designation_id,for_office_unit_ids,"
                    .concat("not_for_office_unit_ids,modified_by,lastModificationTime,inserted_by,insertion_time,isDeleted,ID) ")
                    .concat("VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

    private static final String updateSqlQuery =
            "UPDATE {tableName} SET task_type_id=?,level=?,organogram_id_type=?,organogram_id=?,designation_name=?,designation_id=?,for_office_unit_ids=?,"
                    .concat("not_for_office_unit_ids=?,modified_by=?,lastModificationTime=? WHERE ID=?");

    private static final Map<String, String> searchMap = new HashMap<>();

    private Bill_approval_mappingDAO() {

    }

    private static class LazyLoader {
        static final Bill_approval_mappingDAO INSTANCE = new Bill_approval_mappingDAO();
    }

    public static Bill_approval_mappingDAO getInstance() {
        return LazyLoader.INSTANCE;
    }

    @Override
    public String getTableName() {
        return "bill_approval_mapping";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    @Override
    public void set(PreparedStatement ps, Bill_approval_mappingDTO dto, boolean isInsert) throws SQLException {
        int index = 0;
        ps.setLong(++index, dto.taskTypeId);
        ps.setInt(++index, dto.level);
        ps.setInt(++index, dto.organogramIdTypeEnum.getValue());
        ps.setLong(++index, dto.organogramId);
        ps.setString(++index, dto.designationName);
        ps.setLong(++index, dto.designationId);
        ps.setString(++index, Bill_approval_mappingDTO.getStringFromSet(dto.forOfficeUnitIds));
        ps.setString(++index, Bill_approval_mappingDTO.getStringFromSet(dto.notForOfficeUnitIds));
        ps.setLong(++index, dto.modifiedBy);
        ps.setLong(++index, dto.lastModificationTime);
        if (isInsert) {
            ps.setLong(++index, dto.insertedBy);
            ps.setLong(++index, dto.insertionTime);
            ps.setInt(++index, 0 /* isDeleted */);
        }
        ps.setLong(++index, dto.iD);
    }

    @Override
    public Bill_approval_mappingDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Bill_approval_mappingDTO dto = new Bill_approval_mappingDTO();
            dto.iD = rs.getLong("ID");
            dto.taskTypeId = rs.getLong("task_type_id");
            dto.level = rs.getInt("level");
            dto.organogramIdTypeEnum = OrganogramIdTypeEnum.getFromValue(rs.getInt("organogram_id_type"));
            dto.organogramId = rs.getLong("organogram_id");
            dto.designationName = rs.getString("designation_name");
            dto.designationId = rs.getLong("designation_id");
            dto.forOfficeUnitIds = Bill_approval_mappingDTO.getLongSetFromString(rs.getString("for_office_unit_ids"));
            dto.notForOfficeUnitIds = Bill_approval_mappingDTO.getLongSetFromString(rs.getString("not_for_office_unit_ids"));
            dto.modifiedBy = rs.getLong("modified_by");
            dto.lastModificationTime = rs.getLong("lastModificationTime");
            dto.insertedBy = rs.getLong("inserted_by");
            dto.insertionTime = rs.getLong("insertion_time");
            dto.isDeleted = rs.getInt("isDeleted");
            return dto;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    @Override
    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Bill_approval_mappingDTO) commonDTO, addSqlQuery, true);
    }

    @Override
    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Bill_approval_mappingDTO) commonDTO, updateSqlQuery, false);
    }

    private static final String findByTaskTypeIdSql = "SELECT * FROM bill_approval_mapping WHERE task_type_id=%d AND isDeleted=0";

    public List<Bill_approval_mappingDTO> findAllLevelsByTaskType(long taskTypeId) {
        return ConnectionAndStatementUtil.getListOfT(
                String.format(findByTaskTypeIdSql, taskTypeId),
                this::buildObjectFromResultSet
        );
    }

    private static final String findByTaskTypeAndLevelSql =
            "SELECT * FROM bill_approval_mapping WHERE task_type_id=%d AND level=%d AND isDeleted=0";

    public List<Bill_approval_mappingDTO> findByTaskTypeAndLevel(long taskTypeId, int level) {
        return ConnectionAndStatementUtil.getListOfT(
                String.format(findByTaskTypeAndLevelSql, taskTypeId, level),
                this::buildObjectFromResultSet
        );
    }
}
