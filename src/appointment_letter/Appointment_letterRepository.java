package appointment_letter;

import com.google.gson.Gson;
import org.apache.log4j.Logger;
import repository.Repository;
import repository.RepositoryManager;
import vm_route.VmRouteStoppageDTO;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;


public class Appointment_letterRepository implements Repository {
	Appointment_letterDAO appointment_letterDAO = null;
	Gson gson = new Gson();
	
	public void setDAO(Appointment_letterDAO appointment_letterDAO)
	{
		this.appointment_letterDAO = appointment_letterDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Appointment_letterRepository.class);
	Map<Long, Appointment_letterDTO>mapOfAppointment_letterDTOToiD;


	static Appointment_letterRepository instance = null;  
	private Appointment_letterRepository(){
		mapOfAppointment_letterDTOToiD = new ConcurrentHashMap<>();
		setDAO(new Appointment_letterDAO());

		RepositoryManager.getInstance().addRepository(this);
	}

	public Appointment_letterDTO clone(Appointment_letterDTO dto) {
		String raw = gson.toJson(dto);
		return gson.fromJson(raw, Appointment_letterDTO.class);
	}

	public List<Appointment_letterDTO> clone(List<Appointment_letterDTO> dtoList) {
		return dtoList
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}

	public synchronized static Appointment_letterRepository getInstance(){
		if (instance == null){
			instance = new Appointment_letterRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(appointment_letterDAO == null)
		{
			return;
		}
		try {
			List<Appointment_letterDTO> appointment_letterDTOs = appointment_letterDAO.getAllAppointment_letter(reloadAll);
			for(Appointment_letterDTO appointment_letterDTO : appointment_letterDTOs) {
				Appointment_letterDTO oldAppointment_letterDTO = getAppointment_letterDTOByIDWithoutClone
						(appointment_letterDTO.iD);
				if( oldAppointment_letterDTO != null ) {
					mapOfAppointment_letterDTOToiD.remove(oldAppointment_letterDTO.iD);

					
					
				}
				if(appointment_letterDTO.isDeleted == 0) 
				{
					
					mapOfAppointment_letterDTOToiD.put(appointment_letterDTO.iD, appointment_letterDTO);
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Appointment_letterDTO> getAppointment_letterList() {
		List <Appointment_letterDTO> appointment_letters = new ArrayList<Appointment_letterDTO>(this.mapOfAppointment_letterDTOToiD.values());
		return clone(appointment_letters);
	}
	
	
	public Appointment_letterDTO getAppointment_letterDTOByID( long ID){
		return clone(mapOfAppointment_letterDTOToiD.get(ID));
	}

	public Appointment_letterDTO getAppointment_letterDTOByIDWithoutClone( long ID){
		return mapOfAppointment_letterDTOToiD.get(ID);
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "appointment_letter";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


