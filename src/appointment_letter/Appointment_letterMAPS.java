package appointment_letter;
import java.util.*; 
import util.*;


public class Appointment_letterMAPS extends CommonMaps
{	
	public Appointment_letterMAPS(String tableName)
	{
		

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("recruitmentJobDescriptionId".toLowerCase(), "recruitmentJobDescriptionId".toLowerCase());
		java_DTO_map.put("orderingEmployeeRecordId".toLowerCase(), "orderingEmployeeRecordId".toLowerCase());
		java_DTO_map.put("orderingUnitId".toLowerCase(), "orderingUnitId".toLowerCase());
		java_DTO_map.put("orderingPostId".toLowerCase(), "orderingPostId".toLowerCase());
		java_DTO_map.put("code".toLowerCase(), "code".toLowerCase());
		java_DTO_map.put("orderingEmployeeRecordName".toLowerCase(), "orderingEmployeeRecordName".toLowerCase());
		java_DTO_map.put("orderingEmployeeRecordNameBn".toLowerCase(), "orderingEmployeeRecordNameBn".toLowerCase());
		java_DTO_map.put("orderingUnitName".toLowerCase(), "orderingUnitName".toLowerCase());
		java_DTO_map.put("orderingUnitNameBn".toLowerCase(), "orderingUnitNameBn".toLowerCase());
		java_DTO_map.put("orderingPostName".toLowerCase(), "orderingPostName".toLowerCase());
		java_DTO_map.put("orderingPostNameBn".toLowerCase(), "orderingPostNameBn".toLowerCase());
		java_DTO_map.put("rules".toLowerCase(), "rules".toLowerCase());
		java_DTO_map.put("searchColumn".toLowerCase(), "searchColumn".toLowerCase());
		java_DTO_map.put("insertionDate".toLowerCase(), "insertionDate".toLowerCase());
		java_DTO_map.put("insertedBy".toLowerCase(), "insertedBy".toLowerCase());
		java_DTO_map.put("modifiedBy".toLowerCase(), "modifiedBy".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("recruitment_job_description_id".toLowerCase(), "recruitmentJobDescriptionId".toLowerCase());
		java_SQL_map.put("ordering_employee_record_id".toLowerCase(), "orderingEmployeeRecordId".toLowerCase());
		java_SQL_map.put("ordering_unit_id".toLowerCase(), "orderingUnitId".toLowerCase());
		java_SQL_map.put("ordering_post_id".toLowerCase(), "orderingPostId".toLowerCase());
		java_SQL_map.put("code".toLowerCase(), "code".toLowerCase());
		java_SQL_map.put("ordering_employee_record_name".toLowerCase(), "orderingEmployeeRecordName".toLowerCase());
		java_SQL_map.put("ordering_employee_record_name_bn".toLowerCase(), "orderingEmployeeRecordNameBn".toLowerCase());
		java_SQL_map.put("ordering_unit_name".toLowerCase(), "orderingUnitName".toLowerCase());
		java_SQL_map.put("ordering_unit_name_bn".toLowerCase(), "orderingUnitNameBn".toLowerCase());
		java_SQL_map.put("ordering_post_name".toLowerCase(), "orderingPostName".toLowerCase());
		java_SQL_map.put("ordering_post_name_bn".toLowerCase(), "orderingPostNameBn".toLowerCase());
		java_SQL_map.put("rules".toLowerCase(), "rules".toLowerCase());
		java_SQL_map.put("inserted_by".toLowerCase(), "insertedBy".toLowerCase());
		java_SQL_map.put("modified_by".toLowerCase(), "modifiedBy".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Recruitment Job Description Id".toLowerCase(), "recruitmentJobDescriptionId".toLowerCase());
		java_Text_map.put("Ordering Employee Record Id".toLowerCase(), "orderingEmployeeRecordId".toLowerCase());
		java_Text_map.put("Ordering Unit Id".toLowerCase(), "orderingUnitId".toLowerCase());
		java_Text_map.put("Ordering Post Id".toLowerCase(), "orderingPostId".toLowerCase());
		java_Text_map.put("Code".toLowerCase(), "code".toLowerCase());
		java_Text_map.put("Ordering Employee Record Name".toLowerCase(), "orderingEmployeeRecordName".toLowerCase());
		java_Text_map.put("Ordering Employee Record Name Bn".toLowerCase(), "orderingEmployeeRecordNameBn".toLowerCase());
		java_Text_map.put("Ordering Unit Name".toLowerCase(), "orderingUnitName".toLowerCase());
		java_Text_map.put("Ordering Unit Name Bn".toLowerCase(), "orderingUnitNameBn".toLowerCase());
		java_Text_map.put("Ordering Post Name".toLowerCase(), "orderingPostName".toLowerCase());
		java_Text_map.put("Ordering Post Name Bn".toLowerCase(), "orderingPostNameBn".toLowerCase());
		java_Text_map.put("Rules".toLowerCase(), "rules".toLowerCase());
		java_Text_map.put("Search Column".toLowerCase(), "searchColumn".toLowerCase());
		java_Text_map.put("Insertion Date".toLowerCase(), "insertionDate".toLowerCase());
		java_Text_map.put("Inserted By".toLowerCase(), "insertedBy".toLowerCase());
		java_Text_map.put("Modified By".toLowerCase(), "modifiedBy".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}