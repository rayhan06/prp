package appointment_letter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;

import common.ConnectionAndStatementUtil;
import recruitment_job_description.Recruitment_job_descriptionDTO;
import recruitment_job_description.Recruitment_job_descriptionRepository;
import sessionmanager.SessionConstants;
import java.sql.SQLException;


import org.apache.log4j.Logger;

import dbm.*;

import repository.RepositoryManager;

import util.*;
import pb.*;
import user.UserDTO;
import vm_vehicle_office_assignment.Vm_vehicle_office_assignmentDTO;

public class Appointment_letterDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());
	

	
	public Appointment_letterDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";
		commonMaps = new Appointment_letterMAPS(tableName);
		columnNames = new String[] 
		{
			"ID",
			"recruitment_test_name_id",
			"recruitment_job_description_id",
			"ordering_employee_record_id",
			"ordering_unit_id",
			"ordering_post_id",
			"code",
			"ordering_employee_record_name",
			"ordering_employee_record_name_bn",
			"ordering_unit_name",
			"ordering_unit_name_bn",
			"ordering_post_name",
			"ordering_post_name_bn",
			"rules",
			"ordering_mobile",
			"ordering_email",
			"jari_date",
			"search_column",
			"insertion_date",
			"inserted_by",
			"modified_by",
			"isDeleted",
			"lastModificationTime"
		};
	}
	
	public Appointment_letterDAO()
	{
		this("appointment_letter");		
	}
	
	public void setSearchColumn(Appointment_letterDTO appointment_letterDTO)
	{
		appointment_letterDTO.searchColumn = "";
		appointment_letterDTO.searchColumn += appointment_letterDTO.code + " ";
		appointment_letterDTO.searchColumn += appointment_letterDTO.orderingEmployeeRecordName + " ";
		appointment_letterDTO.searchColumn += appointment_letterDTO.orderingEmployeeRecordNameBn + " ";
		appointment_letterDTO.searchColumn += appointment_letterDTO.orderingUnitName + " ";
		appointment_letterDTO.searchColumn += appointment_letterDTO.orderingUnitNameBn + " ";
		appointment_letterDTO.searchColumn += appointment_letterDTO.orderingPostName + " ";
		appointment_letterDTO.searchColumn += appointment_letterDTO.orderingPostNameBn + " ";


		Recruitment_job_descriptionDTO jobDescriptionDTO = Recruitment_job_descriptionRepository.getInstance().
				getRecruitment_job_descriptionDTOByID(appointment_letterDTO.recruitmentJobDescriptionId);
		appointment_letterDTO.searchColumn += jobDescriptionDTO.jobTitleBn + " ";
		appointment_letterDTO.searchColumn += jobDescriptionDTO.jobTitleEn + " ";

//		appointment_letterDTO.searchColumn += appointment_letterDTO.rules + " ";
//		appointment_letterDTO.searchColumn += appointment_letterDTO.insertedBy + " ";
//		appointment_letterDTO.searchColumn += appointment_letterDTO.modifiedBy + " ";
		
	}
	
	public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException
	{
		Appointment_letterDTO appointment_letterDTO = (Appointment_letterDTO)commonDTO;
		int index = 1;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(appointment_letterDTO);
		if(isInsert)
		{
			ps.setObject(index++,appointment_letterDTO.iD);
		}
		ps.setObject(index++,appointment_letterDTO.recruitmentTestNameId);
		ps.setObject(index++,appointment_letterDTO.recruitmentJobDescriptionId);
		ps.setObject(index++,appointment_letterDTO.orderingEmployeeRecordId);
		ps.setObject(index++,appointment_letterDTO.orderingUnitId);
		ps.setObject(index++,appointment_letterDTO.orderingPostId);
		ps.setObject(index++,appointment_letterDTO.code);
		ps.setObject(index++,appointment_letterDTO.orderingEmployeeRecordName);
		ps.setObject(index++,appointment_letterDTO.orderingEmployeeRecordNameBn);
		ps.setObject(index++,appointment_letterDTO.orderingUnitName);
		ps.setObject(index++,appointment_letterDTO.orderingUnitNameBn);
		ps.setObject(index++,appointment_letterDTO.orderingPostName);
		ps.setObject(index++,appointment_letterDTO.orderingPostNameBn);
		ps.setObject(index++,appointment_letterDTO.rules);
		ps.setObject(index++,appointment_letterDTO.ordering_mobile);
		ps.setObject(index++,appointment_letterDTO.ordering_email);
		ps.setObject(index++,appointment_letterDTO.jari_date);
		ps.setObject(index++,appointment_letterDTO.searchColumn);
		ps.setObject(index++,appointment_letterDTO.insertionDate);
		ps.setObject(index++,appointment_letterDTO.insertedBy);
		ps.setObject(index++,appointment_letterDTO.modifiedBy);
		if(isInsert)
		{
			ps.setObject(index++, 0);
			ps.setObject(index++, lastModificationTime);
		}
	}

	public Appointment_letterDTO build(ResultSet rs)
	{
		try
		{
			Appointment_letterDTO appointment_letterDTO = new Appointment_letterDTO();
			appointment_letterDTO.iD = rs.getLong("ID");
			appointment_letterDTO.recruitmentTestNameId = rs.getLong("recruitment_test_name_id");
			appointment_letterDTO.recruitmentJobDescriptionId = rs.getLong("recruitment_job_description_id");
			appointment_letterDTO.orderingEmployeeRecordId = rs.getLong("ordering_employee_record_id");
			appointment_letterDTO.orderingUnitId = rs.getLong("ordering_unit_id");
			appointment_letterDTO.orderingPostId = rs.getLong("ordering_post_id");
			appointment_letterDTO.code = rs.getString("code");
			appointment_letterDTO.orderingEmployeeRecordName = rs.getString("ordering_employee_record_name");
			appointment_letterDTO.orderingEmployeeRecordNameBn = rs.getString("ordering_employee_record_name_bn");
			appointment_letterDTO.orderingUnitName = rs.getString("ordering_unit_name");
			appointment_letterDTO.orderingUnitNameBn = rs.getString("ordering_unit_name_bn");
			appointment_letterDTO.orderingPostName = rs.getString("ordering_post_name");
			appointment_letterDTO.orderingPostNameBn = rs.getString("ordering_post_name_bn");
			appointment_letterDTO.rules = rs.getString("rules");
			appointment_letterDTO.searchColumn = rs.getString("search_column");
			appointment_letterDTO.insertionDate = rs.getLong("insertion_date");
			appointment_letterDTO.insertedBy = rs.getString("inserted_by");
			appointment_letterDTO.modifiedBy = rs.getString("modified_by");
			appointment_letterDTO.isDeleted = rs.getInt("isDeleted");
			appointment_letterDTO.lastModificationTime = rs.getLong("lastModificationTime");
			appointment_letterDTO.jari_date = rs.getLong("jari_date");
			appointment_letterDTO.ordering_email = rs.getString("ordering_email");
			appointment_letterDTO.ordering_mobile = rs.getString("ordering_mobile");
			return appointment_letterDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
	
	
	
	
		
	

	//need another getter for repository
	public Appointment_letterDTO getDTOByID (long ID)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID = ? ";
		return 	ConnectionAndStatementUtil.getT(sql, Arrays.asList(ID), this::build);
	}
	
	
	
	
	public List<Appointment_letterDTO> getDTOs(Collection recordIDs){
		if(recordIDs.isEmpty()){
			return new ArrayList<>();
		}
		String sql = "SELECT * FROM " + tableName + " WHERE ID IN ( ";
		for(int i = 0;i<recordIDs.size();i++){
			if(i!=0){
				sql+=",";
			}
			sql+= " ? ";
		}
		sql+=")  order by lastModificationTime desc";
		printSql(sql);
		return ConnectionAndStatementUtil.getListOfT(sql, Arrays.asList(recordIDs.toArray()),this::build);
	
	}
	
	

	
	
	
	//add repository
	public List<Appointment_letterDTO> getAllAppointment_letter (boolean isFirstReload)
    {

		String sql = "SELECT * FROM appointment_letter";
		sql += " WHERE ";
	

		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by appointment_letter.lastModificationTime desc";
		printSql(sql);
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);
    }

	

	public String getSqlWithSearchCriteriaForNormalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
			UserDTO userDTO, String filter, boolean tableHasJobCat, List<Object> objectList)
    {
		boolean viewAll = false;
		
		if(p_searchCriteria != null && p_searchCriteria.get("ViewAll") != null)
		{
			System.out.println("ViewAll = " + p_searchCriteria.get("ViewAll"));
			viewAll = true;
		}
		else
		{
			System.out.println("ViewAll is null ");
		}
		
		String sql = "SELECT ";
		if(category == GETIDS)
		{
			sql += " " + tableName + ".ID as ID ";
		}
		else if(category == GETCOUNT)
		{
			sql += " count( " + tableName + ".ID) as countID ";
		}
		else if(category == GETDTOS)
		{
			sql += " distinct " + tableName + ".* ";
		}
		sql += "FROM " + tableName + " ";
		sql += joinSQL;
		

		
		String AnyfieldSql = "";
		String AllFieldSql = "";
		
		if(p_searchCriteria != null)
		{
						
			
			Enumeration names = p_searchCriteria.keys();
			String str, value;
			
			AnyfieldSql = "(";
			
			if(p_searchCriteria.get("AnyField")!= null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase(""))
			{
				AnyfieldSql+= tableName + ".search_column like ? ";
				objectList.add("%" + p_searchCriteria.get("AnyField").toString() + "%");
			}
			AnyfieldSql += ")";
			System.out.println("AnyfieldSql = " + AnyfieldSql);
			
			AllFieldSql = "(";
			int i = 0;
			while(names.hasMoreElements())
			{				
				str = (String) names.nextElement();
				value = (String)p_searchCriteria.get(str);
		        System.out.println(str + ": " + value);
				if(value != null && !value.equalsIgnoreCase("") && (
						 str.equals("code")
						 || str.equals("recruitment_job_description_id")
//						|| str.equals("ordering_employee_record_name")
//						|| str.equals("ordering_employee_record_name_bn")
//						|| str.equals("ordering_unit_name")
//						|| str.equals("ordering_unit_name_bn")
//						|| str.equals("ordering_post_name")
//						|| str.equals("ordering_post_name_bn")
//						|| str.equals("rules")
//						|| str.equals("insertion_date_start")
//						|| str.equals("insertion_date_end")
//						|| str.equals("inserted_by")
//						|| str.equals("modified_by")
				)
						
				)
				{
					if(p_searchCriteria.get(str).equals("any"))
					{
						continue;
					}

					if( i > 0)
					{
						AllFieldSql+= " AND  ";
					}

					if(str.equals("recruitment_job_description_id"))
					{
						AllFieldSql += "" + tableName + ".recruitment_job_description_id = ? ";
						objectList.add(p_searchCriteria.get(str));
						i ++;
					}
					
					 if(str.equals("code"))
					{
						AllFieldSql += "" + tableName + ".code like ? ";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
//					else if(str.equals("ordering_employee_record_name"))
//					{
//						AllFieldSql += "" + tableName + ".ordering_employee_record_name like '%" + p_searchCriteria.get(str) + "%'";
//						i ++;
//					}
//					else if(str.equals("ordering_employee_record_name_bn"))
//					{
//						AllFieldSql += "" + tableName + ".ordering_employee_record_name_bn like '%" + p_searchCriteria.get(str) + "%'";
//						i ++;
//					}
//					else if(str.equals("ordering_unit_name"))
//					{
//						AllFieldSql += "" + tableName + ".ordering_unit_name like '%" + p_searchCriteria.get(str) + "%'";
//						i ++;
//					}
//					else if(str.equals("ordering_unit_name_bn"))
//					{
//						AllFieldSql += "" + tableName + ".ordering_unit_name_bn like '%" + p_searchCriteria.get(str) + "%'";
//						i ++;
//					}
//					else if(str.equals("ordering_post_name"))
//					{
//						AllFieldSql += "" + tableName + ".ordering_post_name like '%" + p_searchCriteria.get(str) + "%'";
//						i ++;
//					}
//					else if(str.equals("ordering_post_name_bn"))
//					{
//						AllFieldSql += "" + tableName + ".ordering_post_name_bn like '%" + p_searchCriteria.get(str) + "%'";
//						i ++;
//					}
//					else if(str.equals("rules"))
//					{
//						AllFieldSql += "" + tableName + ".rules like '%" + p_searchCriteria.get(str) + "%'";
//						i ++;
//					}
//					else if(str.equals("insertion_date_start"))
//					{
//						AllFieldSql += "" + tableName + ".insertion_date >= " + p_searchCriteria.get(str);
//						i ++;
//					}
//					else if(str.equals("insertion_date_end"))
//					{
//						AllFieldSql += "" + tableName + ".insertion_date <= " + p_searchCriteria.get(str);
//						i ++;
//					}
//					else if(str.equals("inserted_by"))
//					{
//						AllFieldSql += "" + tableName + ".inserted_by like '%" + p_searchCriteria.get(str) + "%'";
//						i ++;
//					}
//					else if(str.equals("modified_by"))
//					{
//						AllFieldSql += "" + tableName + ".modified_by like '%" + p_searchCriteria.get(str) + "%'";
//						i ++;
//					}
					
					
				}
			}
			
			AllFieldSql += ")";
			System.out.println("AllFieldSql = " + AllFieldSql);
			
			
		}
		
		
		sql += " WHERE ";
		
		sql += " (" + tableName + ".isDeleted = 0 ";			
		sql += ")";
		
		
		if(!filter.equalsIgnoreCase(""))
		{
			sql += " and " + filter + " ";
		}
		
		if(!AnyfieldSql.equals("()") && !AnyfieldSql.equals(""))
		{
			sql += " AND " + AnyfieldSql;
			
		}
		if(!AllFieldSql.equals("()") && !AllFieldSql.equals(""))
		{			
			sql += " AND " + AllFieldSql;
		}
		
	
			
		sql += " order by " + tableName + ".lastModificationTime desc ";

		printSql(sql);
		
		if(limit >= 0)
		{
			sql += " limit " + limit;
		}
		if(offset >= 0)
		{
			sql += " offset " + offset;
		}
		
		System.out.println("-------------- sql = " + sql);
		
		return sql;
    }
	

				
}
	