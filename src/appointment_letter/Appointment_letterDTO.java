package appointment_letter;
import java.util.*; 
import util.*; 


public class Appointment_letterDTO extends CommonDTO
{

	public long recruitmentTestNameId = -1;
	public long recruitmentJobDescriptionId = -1;
	public long orderingEmployeeRecordId = -1;
	public long orderingUnitId = -1;
	public long orderingPostId = -1;
    public String code = "";
    public String orderingEmployeeRecordName = "";
    public String orderingEmployeeRecordNameBn = "";
    public String orderingUnitName = "";
    public String orderingUnitNameBn = "";
    public String orderingPostName = "";
    public String orderingPostNameBn = "";
    public String rules = "";
	public long insertionDate = -1;
    public String insertedBy = "";
    public String modifiedBy = "";
    public long jari_date = 0;
    public String ordering_mobile = "";
    public String ordering_email = "";
	
	
    @Override
	public String toString() {
            return "$Appointment_letterDTO[" +
            " iD = " + iD +
            " recruitmentJobDescriptionId = " + recruitmentJobDescriptionId +
            " orderingEmployeeRecordId = " + orderingEmployeeRecordId +
            " orderingUnitId = " + orderingUnitId +
            " orderingPostId = " + orderingPostId +
            " code = " + code +
            " orderingEmployeeRecordName = " + orderingEmployeeRecordName +
            " orderingEmployeeRecordNameBn = " + orderingEmployeeRecordNameBn +
            " orderingUnitName = " + orderingUnitName +
            " orderingUnitNameBn = " + orderingUnitNameBn +
            " orderingPostName = " + orderingPostName +
            " orderingPostNameBn = " + orderingPostNameBn +
//            " rules = " + rules +
            " searchColumn = " + searchColumn +
            " insertionDate = " + insertionDate +
            " insertedBy = " + insertedBy +
            " modifiedBy = " + modifiedBy +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            " jari_date = " + jari_date +
            "]";
    }

}