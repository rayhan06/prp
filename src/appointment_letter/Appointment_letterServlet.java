package appointment_letter;

import java.io.IOException;
import java.io.*;
import java.text.SimpleDateFormat;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import appointment_letter_onulipi.Appointment_letter_onulipiDAO;
import appointment_letter_onulipi.Appointment_letter_onulipiDTO;
import appointment_letter_onulipi.Appointment_letter_onulipiRepository;
import common.ApiResponse;
import employee_assign.EmployeeSearchModel;
import job_applicant_application.Job_applicant_applicationDAO;
import job_applicant_application.Job_applicant_applicationDTO;
import job_applicant_application.Job_applicant_applicationRepository;
import org.apache.log4j.Logger;

import login.LoginDTO;
import permission.MenuConstants;
import recruitment_job_description.Recruitment_job_descriptionDTO;
import recruitment_job_description.Recruitment_job_descriptionRepository;
import role.PermissionRepository;


import sessionmanager.SessionConstants;

import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;
import util.CommonRequestHandler;
import util.RecordNavigationManager4;

import java.util.*;
import javax.servlet.http.*;
import java.util.UUID;





import geolocation.GeoLocationDAO2;
import java.util.StringTokenizer;
import java.util.stream.Collectors;

import com.google.gson.Gson;

import pb.*;
import pbReport.*;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;




/**
 * Servlet implementation class Appointment_letterServlet
 */
@WebServlet("/Appointment_letterServlet")
@MultipartConfig
public class Appointment_letterServlet extends HttpServlet
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Appointment_letterServlet.class);

    String tableName = "appointment_letter";

	Appointment_letterDAO appointment_letterDAO;
	CommonRequestHandler commonRequestHandler;
    private final Gson gson = new Gson();

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Appointment_letterServlet()
	{
        super();
    	try
    	{
			appointment_letterDAO = new Appointment_letterDAO(tableName);
			commonRequestHandler = new CommonRequestHandler(appointment_letterDAO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		System.out.println("In doget request = " + request);
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		try
		{
			String actionType = request.getParameter("actionType");
			if(actionType.equals("getAddPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.APPOINTMENT_LETTER_ADD))
				{
					commonRequestHandler.getAddPage(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getEditPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.APPOINTMENT_LETTER_UPDATE))
				{
					getAppointment_letter(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getURL"))
			{
				String URL = request.getParameter("URL");
				System.out.println("URL = " + URL);
				response.sendRedirect(URL);
			}
			else if(actionType.equals("search"))
			{
				System.out.println("search requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.APPOINTMENT_LETTER_SEARCH))
				{
					if(isPermanentTable)
					{
						String filter = request.getParameter("filter");
						System.out.println("filter = " + filter);
						if(filter!=null)
						{
							filter = ""; //shouldn't be directly used, rather manipulate it.
							searchAppointment_letter(request, response, isPermanentTable, filter);
						}
						else
						{
							searchAppointment_letter(request, response, isPermanentTable, "");
						}
					}
					else
					{
						//searchAppointment_letter(request, response, tempTableName, isPermanentTable);
					}
				}
			}

			else if(actionType.equals("print"))
			{
				print(request, response, userDTO);
			}

			else if(actionType.equals("printEligibilityCheck"))
			{
				Appointment_letterDTO appointment_letterDTO = Appointment_letterRepository.getInstance().
						getAppointment_letterDTOByID(Long.parseLong(request.getParameter("ID")));

//				String filter = " job_id = " + appointment_letterDTO.recruitmentJobDescriptionId + " and is_selected = 2 ";
				List<Job_applicant_applicationDTO> job_applicant_applicationDTOS = Job_applicant_applicationRepository.getInstance()
						.getJob_applicant_applicationDTOByjob_id(appointment_letterDTO.recruitmentJobDescriptionId)
						.stream()
						.filter(i -> i.isSelected == 2)
						.collect(Collectors.toList());
//						new Job_applicant_applicationDAO().
//						getDTOsByFilter(filter);
				Boolean flag = false;
				if(job_applicant_applicationDTOS.size() > 0){
					flag = true;
				}

				PrintWriter out = response.getWriter();
				out.print(flag);
				out.flush();
			}

			else if(actionType.equals("view"))
			{
				System.out.println("view requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.APPOINTMENT_LETTER_SEARCH))
				{
					commonRequestHandler.view(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}
			else
			{
				request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}





	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		// TODO Auto-generated method stub
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		System.out.println("doPost");
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);

		try
		{
			String actionType = request.getParameter("actionType");
			System.out.println("actionType = " + actionType);
			if(actionType.equals("add"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.APPOINTMENT_LETTER_ADD))
				{
					System.out.println("going to  addAppointment_letter ");
					addAppointment_letter(request, response, true, userDTO, true);
				}
				else
				{
					System.out.println("Not going to  addAppointment_letter ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}

			else if(actionType.equals("getDTO"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.APPOINTMENT_LETTER_ADD))
				{
					getDTO(request, response);
				}
				else
				{
					System.out.println("Not going to  addAppointment_letter ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}
			else if(actionType.equals("edit"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.APPOINTMENT_LETTER_UPDATE))
				{
					addAppointment_letter(request, response, false, userDTO, isPermanentTable);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("delete"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.APPOINTMENT_LETTER_UPDATE))
				{
					commonRequestHandler.delete(request, response, userDTO);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("search"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.APPOINTMENT_LETTER_SEARCH))
				{
					searchAppointment_letter(request, response, true, "");
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getGeo"))
			{
				System.out.println("going to geoloc ");
				request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
			}

		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}

	private void getDTO(HttpServletRequest request, HttpServletResponse response)
	{
		try
		{
			System.out.println("In getDTO");
			Appointment_letterDTO appointment_letterDTO = Appointment_letterRepository.getInstance().
					getAppointment_letterDTOByID(Long.parseLong(request.getParameter("ID")));
//					(Appointment_letterDTO)appointment_letterDAO.getDTOByID(Long.parseLong(request.getParameter("ID")));
			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");

			String encoded = this.gson.toJson(appointment_letterDTO);
			System.out.println("json encoded data = " + encoded);
			out.print(encoded);
			out.flush();
		}
		catch (NumberFormatException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void addAppointment_letter(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, boolean isPermanentTable) throws IOException
	{
		// TODO Auto-generated method stub
		ApiResponse apiResponse;
		try
		{
			request.setAttribute("failureMessage", "");
			System.out.println("%%%% addAppointment_letter");
			String path = getServletContext().getRealPath("/img2/");
			Appointment_letterDTO appointment_letterDTO;
			SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");

			if(addFlag == true)
			{
				appointment_letterDTO = new Appointment_letterDTO();
			}
			else
			{
				appointment_letterDTO = Appointment_letterRepository.getInstance().
						getAppointment_letterDTOByID(Long.parseLong(request.getParameter("iD")));
//						(Appointment_letterDTO)appointment_letterDAO.getDTOByID(Long.parseLong(request.getParameter("iD")));
			}

			String Value = "";

			Value = request.getParameter("recruitmentTestNameId");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("recruitmentTestNameId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				appointment_letterDTO.recruitmentTestNameId = Long.parseLong(Value);
				if(appointment_letterDTO.recruitmentTestNameId <= 0){
					throw new Exception(" Invalid Test");
				}
			}
			else
			{
				throw new Exception(" Invalid Test");
			}

			Value = request.getParameter("recruitmentJobDescriptionType");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("recruitmentJobDescriptionType = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				appointment_letterDTO.recruitmentJobDescriptionId = Long.parseLong(Value);
				if(appointment_letterDTO.recruitmentJobDescriptionId <= 0){
					throw new Exception(" Invalid job");
				}
			}
			else
			{
				throw new Exception(" Invalid job");
			}



			Value = request.getParameter("jariDate");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("jariDate = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{


					Date d = f.parse(Value);
					appointment_letterDTO.jari_date = d.getTime();

			}
			else
			{
				throw new Exception(" Invalid jari date");
			}


			Value = request.getParameter("code");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("code = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				appointment_letterDTO.code = (Value);
			}
			else
			{
				throw new Exception(" Invalid code");
			}



			Value = request.getParameter("employeeRecordsId");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("employeeRecordsId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{

//				admit_cardDTO.employeeRecordsId = Long.parseLong(Value);
				List<EmployeeSearchModel> models = Arrays.asList(gson.fromJson(Value, EmployeeSearchModel[].class));
				if(models.size() > 0){
					appointment_letterDTO.orderingEmployeeRecordId = models.get(0).employeeRecordId;
					appointment_letterDTO.orderingUnitId = models.get(0).officeUnitId;
					appointment_letterDTO.orderingPostId = models.get(0).organogramId;
					appointment_letterDTO.orderingEmployeeRecordName = models.get(0).employeeNameEn;
					appointment_letterDTO.orderingEmployeeRecordNameBn = models.get(0).employeeNameBn;
					appointment_letterDTO.orderingPostName = models.get(0).organogramNameEn;
					appointment_letterDTO.orderingPostNameBn = models.get(0).organogramNameBn;
					appointment_letterDTO.orderingUnitName = models.get(0).officeUnitNameEn;
					appointment_letterDTO.orderingUnitNameBn = models.get(0).officeUnitNameBn;
					appointment_letterDTO.ordering_mobile = models.get(0).phoneNumber;
					appointment_letterDTO.ordering_email = models.get(0).email;
				}
			}
			else
			{
				throw new Exception(" Invalid authority");
			}



			Value = request.getParameter("rules");

//			if(Value != null)
//			{
//				Value = Jsoup.clean(Value,Whitelist.simpleText());
//			}
			System.out.println("rules = " + Value);
			if(Value != null)
			{
				appointment_letterDTO.rules = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}


			if(addFlag)
			{
				Calendar c = Calendar.getInstance();
				c.set(Calendar.HOUR_OF_DAY, 0);
				c.set(Calendar.MINUTE, 0);
				c.set(Calendar.SECOND, 0);
				c.set(Calendar.MILLISECOND, 0);

				appointment_letterDTO.insertionDate = c.getTimeInMillis();
			}


			Value = request.getParameter("insertedBy");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("insertedBy = " + Value);
			if(Value != null)
			{
				appointment_letterDTO.insertedBy = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("modifiedBy");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("modifiedBy = " + Value);
			if(Value != null)
			{
				appointment_letterDTO.modifiedBy = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			System.out.println("Done adding  addAppointment_letter dto = " + appointment_letterDTO);
			long returnedID = -1;

			if(isPermanentTable == false) //add new row for validation and make the old row outdated
			{
				appointment_letterDAO.setIsDeleted(appointment_letterDTO.iD, CommonDTO.OUTDATED);
				returnedID = appointment_letterDAO.add(appointment_letterDTO);
				appointment_letterDAO.setIsDeleted(returnedID, CommonDTO.WAITING_FOR_APPROVAL);
			}
			else if(addFlag == true)
			{
				returnedID = appointment_letterDAO.manageWriteOperations(appointment_letterDTO, SessionConstants.INSERT, -1, userDTO);
			}
			else
			{
				returnedID = appointment_letterDAO.manageWriteOperations(appointment_letterDTO, SessionConstants.UPDATE, -1, userDTO);
			}

			Value = request.getParameter("onulipiId");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("onulipiId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{



				EmployeeSearchModel[] models = gson.fromJson(Value, EmployeeSearchModel[].class);
				Appointment_letter_onulipiDAO onulipiDAO = new Appointment_letter_onulipiDAO();
//				onulipiDAO.getAllAppointment_letter_onulipiByAppointmentLetterId(returnedID)
				Appointment_letter_onulipiRepository.getInstance().getAppointment_letter_onulipiDTOByappointment_letter_id
						(returnedID).forEach(i -> {
							try {
								onulipiDAO.delete(i.iD);
							} catch (Exception e) {
								e.printStackTrace();
							}
						});


				for(EmployeeSearchModel model: models){

					Appointment_letter_onulipiDTO onulipiDTO = new Appointment_letter_onulipiDTO();
					onulipiDTO.appointmentLetterId = returnedID;
					onulipiDTO.employeeRecordId = model.employeeRecordId;
					onulipiDTO.unitId = model.officeUnitId;
					onulipiDTO.postId = model.organogramId;
					onulipiDTO.employeeRecordName = model.employeeNameEn;
					onulipiDTO.employeeRecordNameBn = model.employeeNameBn;
					onulipiDTO.postName = model.organogramNameEn;
					onulipiDTO.postNameBn = model.organogramNameBn;
					onulipiDTO.unitName = model.officeUnitNameEn;
					onulipiDTO.unitNameBn = model.officeUnitNameBn;
					onulipiDAO.add(onulipiDTO);
				}



			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}




			apiResponse = ApiResponse.makeSuccessResponse("Appointment_letterServlet?actionType=search");

		}
		catch (Exception e)
		{
			e.printStackTrace();
			apiResponse = ApiResponse.makeErrorResponse(e.getMessage());
		}

		PrintWriter pw = response.getWriter();
		pw.write(apiResponse.getJSONString());
		pw.flush();
		pw.close();
	}


	private void print(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO) throws ServletException, IOException {
		Appointment_letterDTO appointment_letterDTO = Appointment_letterRepository.getInstance().
				getAppointment_letterDTOByID(Long.parseLong(request.getParameter("ID")));
		request.setAttribute("appointment_letterDTO",appointment_letterDTO);

		List<Appointment_letter_onulipiDTO> onulipiDTOS = Appointment_letter_onulipiRepository.getInstance()
				.getAppointment_letter_onulipiDTOByappointment_letter_id(appointment_letterDTO.iD);
//				new Appointment_letter_onulipiDAO().
//				getAllAppointment_letter_onulipiByAppointmentLetterId(appointment_letterDTO.iD);
		request.setAttribute("onulipiDTOS",onulipiDTOS);

		Recruitment_job_descriptionDTO jobDescriptionDTO = Recruitment_job_descriptionRepository.getInstance().
				getRecruitment_job_descriptionDTOByID(appointment_letterDTO.recruitmentJobDescriptionId);
		request.setAttribute("jobDescriptionDTO",jobDescriptionDTO);

//		String filter = " job_id = " + jobDescriptionDTO.iD + " and is_selected = 2 ";
		List<Job_applicant_applicationDTO> job_applicant_applicationDTOS = Job_applicant_applicationRepository.getInstance()
				.getJob_applicant_applicationDTOByjob_id(appointment_letterDTO.recruitmentJobDescriptionId)
				.stream()
				.filter(i -> i.isSelected == 2)
				.collect(Collectors.toList());
//				new Job_applicant_applicationDAO().
//				getDTOsByFilter(filter);

		request.setAttribute("job_applicant_applicationDTOS",job_applicant_applicationDTOS);

		RequestDispatcher rd = rd = request.getRequestDispatcher("appointment_letter/appointment_letter_print_form.jsp");
		rd.forward(request, response);
	}









	private void getAppointment_letter(HttpServletRequest request, HttpServletResponse response, long id) throws ServletException, IOException
	{
		System.out.println("in getAppointment_letter");
		Appointment_letterDTO appointment_letterDTO = null;
		try
		{
			appointment_letterDTO = Appointment_letterRepository.getInstance().getAppointment_letterDTOByID(id);
//					(Appointment_letterDTO)appointment_letterDAO.getDTOByID(id);
			request.setAttribute("ID", appointment_letterDTO.iD);
			request.setAttribute("appointment_letterDTO",appointment_letterDTO);
			request.setAttribute("appointment_letterDAO",appointment_letterDAO);

			String URL= "";

			String inPlaceEdit = request.getParameter("inplaceedit");
			String inPlaceSubmit = request.getParameter("inplacesubmit");
			String getBodyOnly = request.getParameter("getBodyOnly");

			if(inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase(""))
			{
				URL = "appointment_letter/appointment_letterInPlaceEdit.jsp";
				request.setAttribute("inplaceedit","");
			}
			else if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
			{
				URL = "appointment_letter/appointment_letterSearchRow.jsp";
				request.setAttribute("inplacesubmit","");
			}
			else
			{
				if(getBodyOnly  != null && !getBodyOnly.equalsIgnoreCase(""))
				{
					URL = "appointment_letter/appointment_letterEditBody.jsp?actionType=edit";
				}
				else
				{
					URL = "appointment_letter/appointment_letterEdit.jsp?actionType=edit";
				}
			}

			RequestDispatcher rd = request.getRequestDispatcher(URL);
			rd.forward(request, response);
		}
		catch (NumberFormatException e)
		{
			e.printStackTrace();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}


	private void getAppointment_letter(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		getAppointment_letter(request, response, Long.parseLong(request.getParameter("ID")));
	}

	private void searchAppointment_letter(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException
	{
		System.out.println("in  searchAppointment_letter 1");
		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String ajax = request.getParameter("ajax");
		boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);

        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
			SessionConstants.NAV_APPOINTMENT_LETTER,
			request,
			appointment_letterDAO,
			SessionConstants.VIEW_APPOINTMENT_LETTER,
			SessionConstants.SEARCH_APPOINTMENT_LETTER,
			tableName,
			isPermanent,
			userDTO,
			filter,
			true);
        try
        {
			System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        }
        catch(Exception e)
        {
			System.out.println("failed to dojob" + e);
        }

		request.setAttribute("appointment_letterDAO",appointment_letterDAO);
        RequestDispatcher rd;
        if(!isPermanent)
        {
        	if(hasAjax == false)
	        {
	        	System.out.println("Going to appointment_letter/appointment_letterApproval.jsp");
	        	rd = request.getRequestDispatcher("appointment_letter/appointment_letterApproval.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to appointment_letter/appointment_letterApprovalForm.jsp");
	        	rd = request.getRequestDispatcher("appointment_letter/appointment_letterApprovalForm.jsp");
	        }
        }
        else
        {
	        if(hasAjax == false)
	        {
	        	System.out.println("Going to appointment_letter/appointment_letterSearch.jsp");
	        	rd = request.getRequestDispatcher("appointment_letter/appointment_letterSearch.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to appointment_letter/appointment_letterSearchForm.jsp");
	        	rd = request.getRequestDispatcher("appointment_letter/appointment_letterSearchForm.jsp");
	        }
        }
		rd.forward(request, response);
	}

}

