package employee_publication;

import common.EmployeeCommonDAOService;
import employee_records.Employee_recordsDAO;
import employee_records.Employee_recordsDTO;
import org.apache.log4j.Logger;
import pb.CatRepository;
import pb.CategoryLanguageModel;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@SuppressWarnings({"unused","Duplicates"})
public class Employee_publicationDAO implements EmployeeCommonDAOService<Employee_publicationDTO>{

	Logger logger = Logger.getLogger(getClass());
	
	private static final String addSqlQuery = "INSERT INTO {tableName} (employee_records_id,publication_cat,contribution_cat,conference_cat,geo_countries_type,"
			+ " publication_title,conference_title,location,publication_date,brief_description,files_dropzone,inserted_by_user_id,"
			+ " insertion_date,modified_by,search_column,lastModificationTime,isDeleted,ID)"
			+ " VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	private static final String updateSqlQuery = "UPDATE {tableName} SET employee_records_id=?,publication_cat=?,contribution_cat=?,conference_cat=?,geo_countries_type=?,"
			+ "publication_title=?,conference_title=?,location=?,publication_date=?,brief_description=?,files_dropzone=?,"
			+ "inserted_by_user_id=?,insertion_date=?,modified_by=?,search_column=?,lastModificationTime =?  WHERE ID =?";

	private final Map<String,String > searchMap = new HashMap<>();

	private static class LazyLoader{
		static final Employee_publicationDAO INSTANCE = new Employee_publicationDAO();
	}

	public static Employee_publicationDAO getInstance(){
		return LazyLoader.INSTANCE;
	}
	private Employee_publicationDAO() {
		searchMap.put("publication_cat"," and (publication_cat = ?)");
		searchMap.put("conference_cat"," and (conference_cat = ?)");
		searchMap.put("geo_countries_type"," and (geo_countries_type = ?)");
		searchMap.put("publication_date"," and (publication_date = ?)");
		searchMap.put("AnyField"," and (search_column like ?)");
		searchMap.put("employee_records_id_internal"," and (employee_records_id = ?)");
	}

	public void set(PreparedStatement ps, Employee_publicationDTO employee_publicationDTO, boolean isInsert)
			throws SQLException {
		int index = 0;
		setSearchColumn(employee_publicationDTO);
		ps.setObject(++index, employee_publicationDTO.employeeRecordsId);
		ps.setObject(++index, employee_publicationDTO.publicationCat);
		ps.setObject(++index, employee_publicationDTO.contributionCat);
		ps.setObject(++index, employee_publicationDTO.conferenceCat);
		ps.setObject(++index, employee_publicationDTO.geoCountriesType);
		ps.setObject(++index, employee_publicationDTO.publicationTitle);
		ps.setObject(++index, employee_publicationDTO.conferenceTitle);
		ps.setObject(++index, employee_publicationDTO.location);
		ps.setObject(++index, employee_publicationDTO.publicationDate);
		ps.setObject(++index, employee_publicationDTO.briefDescription);
		ps.setObject(++index, employee_publicationDTO.filesDropzone);
		ps.setObject(++index, employee_publicationDTO.insertedByUserId);
		ps.setObject(++index, employee_publicationDTO.insertionDate);
		ps.setObject(++index, employee_publicationDTO.modifiedBy);
		ps.setObject(++index, employee_publicationDTO.searchColumn);
		ps.setObject(++index, System.currentTimeMillis());
		if (isInsert) {
			ps.setObject(++index, 0);
		}
		ps.setObject(++index, employee_publicationDTO.iD);
	}
	public void setSearchColumn(Employee_publicationDTO employee_publicationDTO) {
        List<String> list = new ArrayList<>();
        try {
            Employee_recordsDTO employeeRecordsDTO = new Employee_recordsDAO().getEmployee_recordsDTOByID(employee_publicationDTO.employeeRecordsId);
            if(employeeRecordsDTO!=null){
                if(employeeRecordsDTO.nameEng!=null && employeeRecordsDTO.nameEng.trim().length()>0){
                    list.add(employeeRecordsDTO.nameEng.trim());
                }
                if(employeeRecordsDTO.nameBng!=null && employeeRecordsDTO.nameBng.trim().length()>0){
                    list.add(employeeRecordsDTO.nameBng.trim());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        CategoryLanguageModel categoryLanguageModel = CatRepository.getInstance().getCategoryLanguageModel("publication",employee_publicationDTO.publicationCat);
		if(categoryLanguageModel != null) {
			list.add(categoryLanguageModel.banglaText);
			list.add(categoryLanguageModel.englishText);
		}
		
		categoryLanguageModel = CatRepository.getInstance().getCategoryLanguageModel("contribution",employee_publicationDTO.contributionCat);
		if(categoryLanguageModel != null) {
			list.add(categoryLanguageModel.banglaText);
			list.add(categoryLanguageModel.englishText);
		}
		
		categoryLanguageModel = CatRepository.getInstance().getCategoryLanguageModel("conference",employee_publicationDTO.conferenceCat);
		if(categoryLanguageModel != null) {
			list.add(categoryLanguageModel.banglaText);
			list.add(categoryLanguageModel.englishText);
		}
     
        employee_publicationDTO.searchColumn = String.join(" ", list);
    }

	public Employee_publicationDTO buildObjectFromResultSet(ResultSet rs){
		try{
			Employee_publicationDTO employee_publicationDTO = new Employee_publicationDTO();
			employee_publicationDTO.iD = rs.getLong("ID");
			employee_publicationDTO.employeeRecordsId = rs.getLong("employee_records_id");
			employee_publicationDTO.publicationCat = rs.getInt("publication_cat");
			employee_publicationDTO.contributionCat = rs.getInt("contribution_cat");
			employee_publicationDTO.conferenceCat = rs.getInt("conference_cat");
			employee_publicationDTO.geoCountriesType = rs.getLong("geo_countries_type");
			employee_publicationDTO.publicationTitle = rs.getString("publication_title");
			employee_publicationDTO.conferenceTitle = rs.getString("conference_title");
			employee_publicationDTO.location = rs.getString("location");
			employee_publicationDTO.publicationDate = rs.getLong("publication_date");
			employee_publicationDTO.briefDescription = rs.getString("brief_description");
			employee_publicationDTO.filesDropzone = rs.getLong("files_dropzone");
			employee_publicationDTO.insertedByUserId = rs.getLong("inserted_by_user_id");
			employee_publicationDTO.insertionDate = rs.getLong("insertion_date");
			employee_publicationDTO.modifiedBy = rs.getString("modified_by");
			employee_publicationDTO.searchColumn = rs.getString("search_column");
			employee_publicationDTO.isDeleted = rs.getInt("isDeleted");
			employee_publicationDTO.lastModificationTime = rs.getLong("lastModificationTime");
			return employee_publicationDTO;
		}catch (SQLException ex){
			logger.error(ex);
			return null;
		}
	}

	@Override
	public String getTableName() {
		return "employee_publication";
	}

	@Override
	public Map<String, String> getSearchMap() {
		return searchMap;
	}

	public long add(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Employee_publicationDTO) commonDTO, addSqlQuery, true);
	}

	public long update(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Employee_publicationDTO) commonDTO, updateSqlQuery, false);
	}
	
	public CommonDTO getDTOByID(long ID) throws Exception {
		return getDTOFromID(ID);
	}

	public List<Employee_publicationDetails> getEmployeePublicationDetailsByEmployeeRecordsId(long employeeRecoredsId) {
		return getByEmployeeId(employeeRecoredsId)
			   .stream()
			   .map(this::buildEmployeePublicationDetails)
			   .collect(Collectors.toList());
	}
	private Employee_publicationDetails buildEmployeePublicationDetails(Employee_publicationDTO dto) {
		Employee_publicationDetails employee_publicationDetails = new Employee_publicationDetails();
		employee_publicationDetails.dto = dto;
		
		CategoryLanguageModel categoryLanguageModel = CatRepository.getInstance().getCategoryLanguageModel("publication",dto.publicationCat);
		if(categoryLanguageModel != null) {
			employee_publicationDetails.publicationBng = categoryLanguageModel.banglaText;
			employee_publicationDetails.publicationEng = categoryLanguageModel.englishText;
		}
		
		categoryLanguageModel = CatRepository.getInstance().getCategoryLanguageModel("contribution",dto.contributionCat);
		if(categoryLanguageModel != null) {
			employee_publicationDetails.contributionBng = categoryLanguageModel.banglaText;
			employee_publicationDetails.contributionEng = categoryLanguageModel.englishText;
		}
		
		categoryLanguageModel = CatRepository.getInstance().getCategoryLanguageModel("conference",dto.conferenceCat);
		if(categoryLanguageModel != null) {
			employee_publicationDetails.conferenceBng = categoryLanguageModel.banglaText;
			employee_publicationDetails.conferenceEng = categoryLanguageModel.englishText;
		}
		return employee_publicationDetails;
	}
}
