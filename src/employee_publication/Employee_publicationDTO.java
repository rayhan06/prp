package employee_publication;

import sessionmanager.SessionConstants;
import util.CommonEmployeeDTO;


public class Employee_publicationDTO extends CommonEmployeeDTO {
    public int publicationCat = 0;
    public int contributionCat = 0;
    public int conferenceCat = 0;
    public long geoCountriesType = 0;
    public String publicationTitle = "";
    public String conferenceTitle = "";
    public String location = "";
    public long publicationDate = SessionConstants.MIN_DATE;
    public String briefDescription = "";
    public long filesDropzone = 0;
    public long insertedByUserId = 0;
    public long insertionDate = 0;
    public String modifiedBy = "";


    @Override
    public String toString() {
        return "$Employee_publicationDTO[" +
                " iD = " + iD +
                " employeeRecordsId = " + employeeRecordsId +
                " publicationCat = " + publicationCat +
                " contributionCat = " + contributionCat +
                " conferenceCat = " + conferenceCat +
                " geoCountriesType = " + geoCountriesType +
                " publicationTitle = " + publicationTitle +
                " conferenceTitle = " + conferenceTitle +
                " location = " + location +
                " publicationDate = " + publicationDate +
                " briefDescription = " + briefDescription +
                " filesDropzone = " + filesDropzone +
                " insertedByUserId = " + insertedByUserId +
                " insertionDate = " + insertionDate +
                " modifiedBy = " + modifiedBy +
                " isDeleted = " + isDeleted +
                " lastModificationTime = " + lastModificationTime +
                "]";
    }

}