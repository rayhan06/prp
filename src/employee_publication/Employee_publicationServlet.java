package employee_publication;

import common.BaseServlet;
import common.EmployeeServletService;
import employee_records.Employee_recordsRepository;
import geolocation.GeoCountryRepository;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import pb.CatRepository;
import permission.MenuConstants;
import user.UserDTO;
import util.CommonDTO;
import util.HttpRequestUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

@SuppressWarnings("Duplicates")
@WebServlet("/Employee_publicationServlet")
@MultipartConfig
public class Employee_publicationServlet extends BaseServlet implements EmployeeServletService {

    private static final long serialVersionUID = 1L;

    private final Employee_publicationDAO employeePublicationDAO = Employee_publicationDAO.getInstance();

    @Override
    public String getTableName() {
        return employeePublicationDAO.getTableName();
    }

    @Override
    public String getServletName() {
        return "Employee_publicationServlet";
    }

    @Override
    public Employee_publicationDAO getCommonDAOService() {
        return employeePublicationDAO;
    }

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        boolean isLangEnglish = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
        Employee_publicationDTO employee_publicationDTO;
        SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
        boolean isLangEng = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
        CatRepository catRepositoryInstance=CatRepository.getInstance();
        if (addFlag) {
            employee_publicationDTO = new Employee_publicationDTO();
            long empId;
            try{
                empId = Long.parseLong(request.getParameter("empId"));
            }catch (Exception ex){
                ex.printStackTrace();
                throw new Exception(isLangEng?"empId is not found":"empId পাওয়া যায়নি");
            }
            if(Employee_recordsRepository.getInstance().getById(empId) == null){
                throw new Exception(isLangEng?"Employee information is not found":"কর্মকর্তার তথ্য পাওয়া যায়নি");
            }
            employee_publicationDTO.employeeRecordsId = empId;
            employee_publicationDTO.insertedByUserId = userDTO.ID;
            employee_publicationDTO.insertionDate = new Date().getTime();
        } else {
            employee_publicationDTO = employeePublicationDAO.getDTOFromID(Long.parseLong(request.getParameter("iD")));
        }
        employee_publicationDTO.modifiedBy = String.valueOf(userDTO.ID);
        employee_publicationDTO.lastModificationTime = new Date().getTime();
        employee_publicationDTO.publicationCat = Integer.parseInt(Jsoup.clean(request.getParameter("publicationCat"),Whitelist.simpleText()));
        if(catRepositoryInstance.getCategoryLanguageModel("publication",employee_publicationDTO.publicationCat)==null){
            throw new Exception(isLangEnglish?"Please select publication type":"অনুগ্রহ করে প্রকাশনার ধরন বাছাই করুন");
        }
        employee_publicationDTO.contributionCat = Integer.parseInt(Jsoup.clean(request.getParameter("contributionCat"),Whitelist.simpleText()));
        if(catRepositoryInstance.getCategoryLanguageModel("contribution",employee_publicationDTO.contributionCat)==null){
            throw new Exception(isLangEnglish?"Please select contribution type":"অনুগ্রহ করে অবদানের ধরন বাছাই করুন");
        }
        employee_publicationDTO.conferenceCat = Integer.parseInt(Jsoup.clean(request.getParameter("conferenceCat"),Whitelist.simpleText()));
        if(catRepositoryInstance.getCategoryLanguageModel("conference",employee_publicationDTO.conferenceCat)==null){
            throw new Exception(isLangEnglish?"Please select conference type":"অনুগ্রহ করে বৈঠকের ধরন বাছাই করুন");
        }
        if(employee_publicationDTO.conferenceCat==2){
            employee_publicationDTO.geoCountriesType = Long.parseLong(Jsoup.clean(request.getParameter("geoCountriesType"),Whitelist.simpleText()));
        }else{
            employee_publicationDTO.geoCountriesType=18;////Default Bangladesh
        }
        if(GeoCountryRepository.getInstance().getDTOByID(employee_publicationDTO.geoCountriesType)==null){
            throw new Exception(isLangEnglish?"Please select country":"অনুগ্রহ করে বৈঠকের দেশ বাছাই করুন");
        }
        employee_publicationDTO.publicationTitle = Jsoup.clean(request.getParameter("publicationTitle"),Whitelist.simpleText()).trim();
        if(employee_publicationDTO.publicationTitle.length()==0){
            throw new Exception(isLangEnglish?"Please enter publication title":"অনুগ্রহ করে প্রকাশনা শিরোনাম লিখুন");
        }
        employee_publicationDTO.conferenceTitle = Jsoup.clean(request.getParameter("conferenceTitle"),Whitelist.simpleText()).trim();
        if(employee_publicationDTO.conferenceTitle.length()==0){
            throw new Exception(isLangEnglish?"Please enter conference title":"অনুগ্রহ করে বৈঠক শিরোনাম লিখুন");
        }
        employee_publicationDTO.location = Jsoup.clean(request.getParameter("location"),Whitelist.simpleText()).trim();
        if(employee_publicationDTO.location.length()==0){
            throw new Exception(isLangEnglish?"Please enter location":"অনুগ্রহ করে অবস্থান লিখুন");
        }
        Date d = f.parse(Jsoup.clean(request.getParameter("publicationDate"),Whitelist.simpleText()));
        employee_publicationDTO.publicationDate = d.getTime();
        employee_publicationDTO.briefDescription = Jsoup.clean(request.getParameter("briefDescription"),Whitelist.simpleText()).trim();
        employee_publicationDTO.filesDropzone = Long.parseLong(request.getParameter("filesDropzone"));

        if (!addFlag) {
            String filesDropzoneFilesToDelete = request.getParameter("filesDropzoneFilesToDelete");
            String[] deleteArray = filesDropzoneFilesToDelete.split(",");
            for (int i = 0; i < deleteArray.length; i++) {
                System.out.println("going to delete " + deleteArray[i]);
                if (i > 0) {
                    filesDAO.delete(Long.parseLong(deleteArray[i]));
                }
            }
        }
        if (addFlag) {
            employeePublicationDAO.add(employee_publicationDTO);
        } else {
            employeePublicationDAO.update(employee_publicationDTO);
        }
        return employee_publicationDTO;
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[0];
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[0];
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[0];
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Employee_publicationServlet.class;
    }

    @Override
    public String getAjaxAddRedirectURL(HttpServletRequest request,CommonDTO commonDTO) {
        return getAddOrEditOrDeleteRedirectURL(request,6,"publication_info");
    }

    @Override
    public String getAjaxEditRedirectURL(HttpServletRequest request,CommonDTO commonDTO) {
        return getAddOrEditOrDeleteRedirectURL(request,6,"publication_info");
    }

    @Override
    public String getDeleteRedirectURL(HttpServletRequest request) {
        return getAddOrEditOrDeleteRedirectURL(request,6,"publication_info");
    }

    @Override
    public boolean getEditPagePermission(HttpServletRequest request) {
        return checkEditOrViewPagePermission(request);
    }

    @Override
    public boolean getViewPagePermission(HttpServletRequest request) {
        return checkEditOrViewPagePermission(request);
    }

    @Override
    public boolean getEditPermission(HttpServletRequest request) {
        return checkEditOrViewPagePermission(request);
    }

    @Override
    public boolean getAddPermission(HttpServletRequest request) {
        return checkAddPermission(request);
    }

    @Override
    public boolean getAddPagePermission(HttpServletRequest request) {
        return checkAddPermission(request);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        search(request);
        super.doGet(request,response);
    }
}