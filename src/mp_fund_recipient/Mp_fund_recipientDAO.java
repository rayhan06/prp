package mp_fund_recipient;

import common.CommonDAOService;
import org.apache.log4j.Logger;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings({"Duplicates"})
public class Mp_fund_recipientDAO implements CommonDAOService<Mp_fund_recipientDTO> {
    private static final Logger logger = Logger.getLogger(Mp_fund_recipientDAO.class);

    private static final String addQuery =
            "INSERT INTO {tableName} (mp_fund_application_id,name,father_or_husband_name,allocated_amount,upazila_name,"
                    .concat("ward_number,union_name,comment,modified_by,lastModificationTime,inserted_by,insertion_time,")
                    .concat("isDeleted,ID) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

    private static final String updateQuery =
            "UPDATE {tableName} SET mp_fund_application_id=?, name=?,father_or_husband_name=?,allocated_amount=?,upazila_name=?,ward_number=?,"
                    .concat("union_name=?,comment=?,modified_by=?,lastModificationTime=? WHERE ID=?");

    public static final String getByApplicationId =
            "SELECT * FROM mp_fund_recipient WHERE mp_fund_application_id=? AND isDeleted=0";

    private Mp_fund_recipientDAO() {
    }

    private final Map<String, String> searchMap = new HashMap<>();

    private static class LazyLoader {
        static final Mp_fund_recipientDAO INSTANCE = new Mp_fund_recipientDAO();
    }

    public static Mp_fund_recipientDAO getInstance() {
        return LazyLoader.INSTANCE;
    }

    @Override
    public void set(PreparedStatement ps, Mp_fund_recipientDTO dto, boolean isInsert) throws SQLException {
        int index = 0;
        ps.setLong(++index, dto.mpFundApplicationId);
        ps.setString(++index, dto.name);
        ps.setString(++index, dto.fatherOrHusbandName);
        ps.setLong(++index, dto.allocatedAmount);
        ps.setString(++index, dto.upazilaName);
        ps.setInt(++index, dto.wardNumber);
        ps.setString(++index, dto.unionName);
        ps.setString(++index, dto.comment);
        ps.setLong(++index, dto.modifiedBy);
        ps.setLong(++index, dto.lastModificationTime);
        if (isInsert) {
            ps.setLong(++index, dto.insertedBy);
            ps.setLong(++index, dto.insertionTime);
            ps.setInt(++index, 0 /* isDeleted */);
        }
        ps.setLong(++index, dto.iD);
    }

    @Override
    public Mp_fund_recipientDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Mp_fund_recipientDTO dto = new Mp_fund_recipientDTO();
            dto.iD = rs.getLong("ID");
            dto.name = rs.getString("name");
            dto.fatherOrHusbandName = rs.getString("father_or_husband_name");
            dto.allocatedAmount = rs.getLong("allocated_amount");
            dto.upazilaName = rs.getString("upazila_name");
            dto.wardNumber = rs.getInt("ward_number");
            dto.unionName = rs.getString("union_name");
            dto.comment = rs.getString("comment");
            dto.insertedBy = rs.getLong("inserted_by");
            dto.insertionTime = rs.getLong("insertion_time");
            dto.isDeleted = rs.getInt("isDeleted");
            dto.modifiedBy = rs.getLong("modified_by");
            dto.lastModificationTime = rs.getLong("lastModificationTime");
            return dto;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    @Override
    public String getTableName() {
        return "mp_fund_recipient";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    @Override
    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Mp_fund_recipientDTO) commonDTO, addQuery, true);
    }

    @Override
    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Mp_fund_recipientDTO) commonDTO, updateQuery, false);
    }

    public List<Mp_fund_recipientDTO> getDTOsByApplicationId(long fundApplicationId) {
        return getDTOs(
                getByApplicationId,
                Collections.singletonList(fundApplicationId)
        );
    }
}