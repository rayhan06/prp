package mp_fund_recipient;

import election_constituency.Election_constituencyRepository;
import language.LC;
import language.LM;
import util.CommonDTO;
import util.StringUtils;


public class Mp_fund_recipientDTO extends CommonDTO {
    public long mpFundApplicationId = -1;
    public String name = "";
    public String fatherOrHusbandName = "";
    public long allocatedAmount = 0;
    public String upazilaName = "";
    public int wardNumber = -1;
    public String unionName = "";
    public String comment = "";

    public long insertedBy = -1;
    public long insertionTime = -1;
    public long modifiedBy = -1;

    public String getAddress(long electionConstituencyId, String language) {
        return LM.getText(LC.MP_FUND_APPLICATION_ADD_RECIPIENT_WARD_NUMBER, language) + "-"
                + StringUtils.convertBanglaIfLanguageIsBangla(language, String.valueOf(wardNumber)) + ", "
                + unionName + ", "
                + upazilaName + ", "
                + Election_constituencyRepository.getInstance().getText(electionConstituencyId, language);
    }

    @Override
    public String toString() {
        return "Mp_fund_recipientDTO{" +
                "name='" + name + '\'' +
                ", fatherOrHusbandName='" + fatherOrHusbandName + '\'' +
                ", allocatedAmount=" + allocatedAmount +
                ", upazilaName='" + upazilaName + '\'' +
                ", wardNumber=" + wardNumber +
                ", unionName='" + unionName + '\'' +
                ", comment='" + comment + '\'' +
                ", insertedBy=" + insertedBy +
                ", insertionTime=" + insertionTime +
                ", modifiedBy=" + modifiedBy +
                '}';
    }
}