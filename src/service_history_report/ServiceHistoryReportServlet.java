package service_history_report;

import language.LC;
import language.LM;
import login.LoginDTO;
import org.apache.log4j.Logger;
import pbReport.PBReportUtils;
import pbReport.ReportCommonService;
import permission.MenuConstants;
import role.PermissionRepository;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

@SuppressWarnings({"unused"})
@WebServlet("/Service_history_report_Servlet")
public class ServiceHistoryReportServlet extends HttpServlet implements ReportCommonService {
    private static final long serialVersionUID = 1L;
    private static final Logger logger = Logger.getLogger(ServiceHistoryReportServlet.class);

    private static final Set<String> searchParam = new HashSet<>(Arrays.asList("employeeRecordId", "dateFrom", "dateTo", "promotionNatureCat", "nameEng", "nameBng"));

    private final Map<String, String[]> stringMap = new HashMap<>();

    private static final String sql = " employee_service_history esh "
            .concat(" INNER JOIN promotion_history ph ON esh.promotion_history_id = ph.id  left join employee_records er on esh.employee_records_id = er.id ");

    private String[][] Criteria;
    private String[][] Display;

    public ServiceHistoryReportServlet() {
        stringMap.put("employeeRecordId", new String[]{"criteria", "esh", "employee_records_id", "=", "AND", "long", "", "", "any",
                "employeeRecordId", "employeeRecordId", String.valueOf(LC.SERVICE_HISTORY_REPORT_WHERE_EMPLOYEERECORDSID), "employee_records_id", null, "true", "1"});
        stringMap.put("promotionNatureCat", new String[]{"criteria", "ph", "promotion_nature_cat", "=", "AND", "int", "", "", "any",
                "promotionNatureCat", "promotionNatureCat", String.valueOf(LC.SERVICE_HISTORY_REPORT_WHERE_PROMOTIONNATURECAT), "category", "promotion_nature", "true", "2"});
        stringMap.put("dateFrom", new String[]{"criteria", "esh", "serving_from", ">=", "AND", "long", "", "", "any",
                "dateFrom", "dateFrom", String.valueOf(LC.SERVICE_HISTORY_REPORT_WHERE_SERVINGTO), "date", null, "true", "3"});
        stringMap.put("dateTo", new String[]{"criteria", "esh", "serving_to", "<=", "AND", "long", "", "", "any",
                "dateTo", "dateTo", String.valueOf(LC.SERVICE_HISTORY_REPORT_WHERE_SERVINGTO_2), "date", null, "true", "4"});
        stringMap.put("nameEng", new String[]{"criteria", "er", "name_eng", "Like", "AND", "String", "", "", "any", "nameEng",
                "nameEng", String.valueOf(LC.EMPLOYEE_RECORDS_ADD_NAMEENG), "text", null, "true", "5"});
        stringMap.put("nameBng", new String[]{"criteria", "er", "name_bng", "Like", "AND", "String", "", "", "any", "nameBng",
                "nameBng", String.valueOf(LC.EMPLOYEE_RECORDS_ADD_NAMEBNG), "text", null, "true", "6"});
        stringMap.put("dateToDefault", new String[]{"criteria", "esh", "serving_to", ">", "AND", "long", "", "", "-62135791200000",
                "dateToDefault", null, null, null, null, null, null});
        stringMap.put("isDeleted", new String[]{"criteria", "esh", "isDeleted", "=", "AND", "int", "", "", "0", "esh_delete", null, null, null, null, null, null});
    }


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }

        Set<String> inputs = PBReportUtils.prepareInputSets(searchParam, request);
        inputs.add("isDeleted");

        Display = new String[][]{
                {"display", "esh", "employee_records_id", "employee_records_id", LM.getText(LC.SERVICE_HISTORY_REPORT_SELECT_EMPLOYEERECORDSID, loginDTO)},
                {"display", "esh", "employee_records_id", "user_name", LM.getText(LC.EMPLOYEE_INFO_REPORT_SELECT_USER_ID, loginDTO)},
                {"display", "esh", "employee_records_id", "mobile_number", LM.getText(LC.EMPLOYEE_INFO_REPORT_SELECT_MOBILE_NUMBER, loginDTO)},
                {"display", "esh", "employee_records_id", "email", LM.getText(LC.EMPLOYEE_INFO_REPORT_SELECT_EMAIL, loginDTO)},
                {"display", "esh", "employee_records_id", "id_to_home_district", LM.getText(LC.EMPLOYEE_RECORDS_ADD_HOME_DISTRICT, loginDTO)},
                {"display", "esh", "serving_from", "date", LM.getText(LC.SERVICE_HISTORY_REPORT_SELECT_SERVINGFROM, loginDTO)},
                {"display", "esh", "serving_to", "date", LM.getText(LC.SERVICE_HISTORY_REPORT_SELECT_SERVINGTO, loginDTO)},
                {"display", "esh", "office", "text", LM.getText(LC.SERVICE_HISTORY_REPORT_SELECT_OFFICE, loginDTO)},
                {"display", "esh", "department", "text", LM.getText(LC.SERVICE_HISTORY_REPORT_SELECT_DEPARTMENT, loginDTO)},
                {"display", "esh", "designation", "text", LM.getText(LC.SERVICE_HISTORY_REPORT_SELECT_DESIGNATION, loginDTO)},
                {"display", "ph", "promotion_nature_cat", "category", LM.getText(LC.SERVICE_HISTORY_REPORT_SELECT_PROMOTIONNATURECAT, loginDTO)},
                {"display", "ph", "employee_pay_scale", "text", LM.getText(LC.SERVICE_HISTORY_REPORT_SELECT_EMPLOYEEPAYSCALE, loginDTO)},
                {"display", "esh", "employee_records_id", "employee_records_id_class", LM.getText(LC.SERVICE_HISTORY_REPORT_SELECT_EMPLOYEECLASSCAT, loginDTO)},
                {"display", "esh", "employee_records_id", "id_to_present_address", LM.getText(LC.EMPLOYEE_RECORDS_ADD_PRESENTADDRESS, loginDTO)},
                {"display", "esh", "employee_records_id", "id_to_permanent_address", LM.getText(LC.EMPLOYEE_RECORDS_ADD_PERMANENTADDRESS, loginDTO)}
        };
        if (inputs.contains("dateTo")) {
            inputs.add("dateToDefault");
        }
        Criteria = PBReportUtils.prepareCriteria(inputs, stringMap);
        request.setAttribute("dontUseOutDatedModal", true);
        if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.SERVICE_HISTORY_REPORT_DETAILS)) {
            reportGenerate(request, response);
        }
    }

    @Override
    public String[][] getCriteria() {
        return Criteria;
    }

    @Override
    public String[][] getDisplay() {
        return Display;
    }

    @Override
    public String getSQL() {
        return sql;
    }

    @Override
    public int getLCForReportName() {
        return LC.SERVICE_HISTORY_REPORT_OTHER_SERVICE_HISTORY_REPORT;
    }

    @Override
    public String getFileName() {
        return "service_history_report";
    }

    @Override
    public String getTableName() {
        return "service_history_report";
    }
}