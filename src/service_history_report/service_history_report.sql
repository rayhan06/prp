--service_history_report
SELECT esh.employee_records_id,
       esh.serving_from,
       esh.serving_to,
       esh.office,
       esh.department,
       esh.designation,
       ph.promotion_nature_cat,
       ph.employee_pay_scale,
       er.employee_class_cat
FROM (SELECT * FROM employee_service_history WHERE isDeleted = 0) esh
         INNER JOIN promotion_history ph on esh.promotion_history_id = ph.id
         INNER JOIN employee_records er on esh.employee_records_id = er.id
WHERE esh.employee_records_id = 'any'
  AND esh.serving_to >= 'any'
  AND esh.serving_to <= 'any'
  AND ph.promotion_nature_cat = 'any';