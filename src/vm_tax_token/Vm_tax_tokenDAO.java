package vm_tax_token;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;
import sessionmanager.SessionConstants;
import java.sql.SQLException;
import common.ConnectionAndStatementUtil;


import org.apache.log4j.Logger;

import dbm.*;

import repository.RepositoryManager;

import util.*;
import pb.*;
import user.UserDTO;
import vm_fuel_vendor.VmFuelVendorItemDTO;
import vm_fuel_vendor.VmFuelVendorItemRepository;
import vm_fuel_vendor.Vm_fuel_vendorDTO;
import vm_vehicle_office_assignment.Vm_vehicle_office_assignmentDTO;

public class Vm_tax_tokenDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());

	public Vm_tax_tokenDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";
		commonMaps = new Vm_tax_tokenMAPS(tableName);
		columnNames = new String[] 
		{
			"ID",
			"fiscal_year_id",
			"vehicle_type_cat",
			"tax_token_fees",
			"fitness_fees",
			"digital_number_fees",
			"vat",
			"search_column",
			"inserted_by",
			"modified_by",
			"insertion_date",
			"isDeleted",
			"lastModificationTime"
		};
	}
	
	public Vm_tax_tokenDAO()
	{
		this("vm_tax_token");		
	}
	
	public void setSearchColumn(Vm_tax_tokenDTO vm_tax_tokenDTO)
	{
		vm_tax_tokenDTO.searchColumn = "";
		vm_tax_tokenDTO.searchColumn += CatRepository.getName("English", "vehicle_type", vm_tax_tokenDTO.vehicleTypeCat) + " " + CatRepository.getName("Bangla", "vehicle_type", vm_tax_tokenDTO.vehicleTypeCat) + " ";
	}
	
	public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException
	{
		Vm_tax_tokenDTO vm_tax_tokenDTO = (Vm_tax_tokenDTO)commonDTO;
		int index = 1;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(vm_tax_tokenDTO);
		if(isInsert)
		{
			ps.setObject(index++,vm_tax_tokenDTO.iD);
		}
		ps.setObject(index++,vm_tax_tokenDTO.fiscalYearId);
		ps.setObject(index++,vm_tax_tokenDTO.vehicleTypeCat);
		ps.setObject(index++,vm_tax_tokenDTO.taxTokenFees);
		ps.setObject(index++,vm_tax_tokenDTO.fitnessFees);
		ps.setObject(index++,vm_tax_tokenDTO.digitalNumberFees);
		ps.setObject(index++,vm_tax_tokenDTO.vat);
		ps.setObject(index++,vm_tax_tokenDTO.searchColumn);
		ps.setObject(index++,vm_tax_tokenDTO.insertedBy);
		ps.setObject(index++,vm_tax_tokenDTO.modifiedBy);
		ps.setObject(index++,vm_tax_tokenDTO.insertionDate);
		if(isInsert)
		{
			ps.setObject(index++, 0);
			ps.setObject(index++, lastModificationTime);
		}
	}
	
	public Vm_tax_tokenDTO build(ResultSet rs)
	{
		try
		{
			Vm_tax_tokenDTO vm_tax_tokenDTO = new Vm_tax_tokenDTO();
			vm_tax_tokenDTO.iD = rs.getLong("ID");
			vm_tax_tokenDTO.fiscalYearId = rs.getLong("fiscal_year_id");
			vm_tax_tokenDTO.vehicleTypeCat = rs.getInt("vehicle_type_cat");
			vm_tax_tokenDTO.taxTokenFees = rs.getDouble("tax_token_fees");
			vm_tax_tokenDTO.fitnessFees = rs.getDouble("fitness_fees");
			vm_tax_tokenDTO.digitalNumberFees = rs.getDouble("digital_number_fees");
			vm_tax_tokenDTO.vat = rs.getDouble("vat");
			vm_tax_tokenDTO.searchColumn = rs.getString("search_column");
			vm_tax_tokenDTO.insertedBy = rs.getString("inserted_by");
			vm_tax_tokenDTO.modifiedBy = rs.getString("modified_by");
			vm_tax_tokenDTO.insertionDate = rs.getLong("insertion_date");
			vm_tax_tokenDTO.isDeleted = rs.getInt("isDeleted");
			vm_tax_tokenDTO.lastModificationTime = rs.getLong("lastModificationTime");
			return vm_tax_tokenDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}

	public Vm_tax_tokenDTO buildWithChildren(ResultSet rs)
	{
		try
		{
			Vm_tax_tokenDTO vm_tax_tokenDTO = new Vm_tax_tokenDTO();
			vm_tax_tokenDTO.iD = rs.getLong("ID");
			vm_tax_tokenDTO.fiscalYearId = rs.getLong("fiscal_year_id");
			vm_tax_tokenDTO.vehicleTypeCat = rs.getInt("vehicle_type_cat");
			vm_tax_tokenDTO.taxTokenFees = rs.getDouble("tax_token_fees");
			vm_tax_tokenDTO.fitnessFees = rs.getDouble("fitness_fees");
			vm_tax_tokenDTO.digitalNumberFees = rs.getDouble("digital_number_fees");
			vm_tax_tokenDTO.vat = rs.getDouble("vat");
			vm_tax_tokenDTO.searchColumn = rs.getString("search_column");
			vm_tax_tokenDTO.insertedBy = rs.getString("inserted_by");
			vm_tax_tokenDTO.modifiedBy = rs.getString("modified_by");
			vm_tax_tokenDTO.insertionDate = rs.getLong("insertion_date");
			vm_tax_tokenDTO.isDeleted = rs.getInt("isDeleted");
			vm_tax_tokenDTO.lastModificationTime = rs.getLong("lastModificationTime");

			List<VmTaxTokenItemDTO> vmTaxTokenItemDTOS = VmTaxTokenItemRepository.getInstance().getVmTaxTokenItemDTOByVmTaxTokenId(vm_tax_tokenDTO.iD);
			vm_tax_tokenDTO.vmTaxTokenItemDTOList = vmTaxTokenItemDTOS;
			return vm_tax_tokenDTO;
		}
		catch (Exception ex)
		{
			logger.error(ex);
			return null;
		}
	}
	
	

//	public Vm_tax_tokenDTO getDTOByID (long id)
//	{
//		//String sql = "SELECT * FROM " + tableName + " WHERE ID = " + id;
//		String sql = "SELECT * FROM " + tableName + " WHERE ID = ? ";
//		//Vm_tax_tokenDTO vm_tax_tokenDTO = ConnectionAndStatementUtil.getT(sql,this::build);
//		Vm_tax_tokenDTO vm_tax_tokenDTO = ConnectionAndStatementUtil.getT(sql, Arrays.asList(id), this::build);
//		try {
//			VmTaxTokenItemDAO vmTaxTokenItemDAO = new VmTaxTokenItemDAO("vm_tax_token_item");
//			List<VmTaxTokenItemDTO> vmTaxTokenItemDTOList = vmTaxTokenItemDAO.getVmTaxTokenItemDTOListByVmTaxTokenID(vm_tax_tokenDTO.iD);
//			vm_tax_tokenDTO.vmTaxTokenItemDTOList = vmTaxTokenItemDTOList;
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		return vm_tax_tokenDTO;
//	}


	public Vm_tax_tokenDTO getDTOByID (long id)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID = ? ";
		Vm_tax_tokenDTO vm_tax_tokenDTO = ConnectionAndStatementUtil.getT(sql, Arrays.asList(id), this::buildWithChildren);
		
		return vm_tax_tokenDTO;
	}




	public String getSqlWithSearchCriteriaForNormalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
			UserDTO userDTO, String filter, boolean tableHasJobCat, List<Object> objectList)
    {
		boolean viewAll = false;
		
		if(p_searchCriteria != null && p_searchCriteria.get("ViewAll") != null)
		{
			System.out.println("ViewAll = " + p_searchCriteria.get("ViewAll"));
			viewAll = true;
		}
		else
		{
			System.out.println("ViewAll is null ");
		}
		
		String sql = "SELECT ";
		if(category == GETIDS)
		{
			//System.out.println("categoryNNN GETIDS");
			sql += " " + tableName + ".ID as ID ";
		}
		else if(category == GETCOUNT)
		{
			System.out.println("categoryNNN GETCOUNT");
			sql += " count( " + tableName + ".ID) as countID ";
			//sql += " count( " + tableName + ".fiscal_year_id) as countID ";
		}
		else if(category == GETDTOS)
		{
			//System.out.println("categoryNNN GETDTOS");
			sql += " distinct " + tableName + ".* ";
			//sql += " distinct " + tableName + ".fiscal_year_id ";
		}
		sql += "FROM " + tableName + " ";
		sql += joinSQL;
		

		
		String AnyfieldSql = "";
		String AllFieldSql = "";
		
		if(p_searchCriteria != null)
		{
						
			
			Enumeration names = p_searchCriteria.keys();
			String str, value;
			
			AnyfieldSql = "(";
			
			if(p_searchCriteria.get("AnyField")!= null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase(""))
			{
				//AnyfieldSql+= tableName + ".search_column like '%" + p_searchCriteria.get("AnyField").toString() + "%'";
				AnyfieldSql+= tableName + ".search_column like ? ";
				objectList.add("%" + p_searchCriteria.get("AnyField").toString() + "%");
			}
			AnyfieldSql += ")";
			System.out.println("AnyfieldSql = " + AnyfieldSql);
			
			AllFieldSql = "(";
			int i = 0;
			while(names.hasMoreElements())
			{				
				str = (String) names.nextElement();
				value = (String)p_searchCriteria.get(str);
		        System.out.println(str + ": " + value);
				if(value != null && !value.equalsIgnoreCase("") && (
						 str.equals("vehicle_type_cat")
						|| str.equals("insertion_date_start")
						|| str.equals("insertion_date_end")
				)
						
				)
				{
					if(p_searchCriteria.get(str).equals("any"))
					{
						continue;
					}

					if( i > 0)
					{
						AllFieldSql+= " AND  ";
					}
					
					 if(str.equals("vehicle_type_cat"))
					{
						//AllFieldSql += "" + tableName + ".vehicle_type_cat = " + p_searchCriteria.get(str);
						AllFieldSql += "" + tableName + ".vehicle_type_cat = ? ";
						objectList.add(p_searchCriteria.get(str));
						i ++;
					}
					else if(str.equals("insertion_date_start"))
					{
						//AllFieldSql += "" + tableName + ".insertion_date >= " + p_searchCriteria.get(str);
						AllFieldSql += "" + tableName + ".insertion_date >= ? ";
						objectList.add(p_searchCriteria.get(str));
						i ++;
					}
					else if(str.equals("insertion_date_end"))
					{
						//AllFieldSql += "" + tableName + ".insertion_date <= " + p_searchCriteria.get(str);
						AllFieldSql += "" + tableName + ".insertion_date <= ? ";
						objectList.add(p_searchCriteria.get(str));
						i ++;
					}
					
					
				}
			}
			
			AllFieldSql += ")";
			System.out.println("AllFieldSql = " + AllFieldSql);
			
			
		}
		
		
		sql += " WHERE ";
		
		sql += " (" + tableName + ".isDeleted = 0 ";			
		sql += ")";
		
		
		if(!filter.equalsIgnoreCase(""))
		{
			sql += " and " + filter + " ";
		}
		
		if(!AnyfieldSql.equals("()") && !AnyfieldSql.equals(""))
		{
			sql += " AND " + AnyfieldSql;
			
		}
		if(!AllFieldSql.equals("()") && !AllFieldSql.equals(""))
		{			
			sql += " AND " + AllFieldSql;
		}


		sql += " GROUP BY " + tableName + ".fiscal_year_id ";
		sql += " order by " + tableName + ".lastModificationTime desc ";
		//sql += " order by " + tableName + ".fiscal_year_id desc ";


		printSql(sql);
		
		if(limit >= 0)
		{
			sql += " limit " + limit;
		}
		if(offset >= 0)
		{
			sql += " offset " + offset;
		}
		
		System.out.println("-------------- sql = " + sql);
		
		return sql;
    }
	




	public int checkDuplicateFiscalYear (long fiscalYearID)
	{
		int existence = 0;
		String sql = "SELECT * ";

		sql += " FROM " + tableName;

		//sql += " WHERE fiscal_year_id = " + fiscalYearID ;
		sql += " WHERE fiscal_year_id = ? " ;

		sql += " AND isDeleted=0 ";

		sql += " LIMIT 1 ";

		printSql(sql);

		Vm_tax_tokenDTO dto = ConnectionAndStatementUtil.
				getT(sql, Arrays.asList(fiscalYearID),this::build);
		if(dto != null){
			existence=1;
		}

		return existence;
	}

	public List<Vm_tax_tokenDTO> getDTOsByFiscalYearID (long fiscalYearID)
	{
		//String sql = "SELECT * FROM " + tableName + " WHERE fiscal_year_id = " + fiscalYearID;
		String sql = "SELECT * FROM " + tableName + " WHERE fiscal_year_id = ? " ;
		sql +=" AND isDeleted=0 ";
		sql += " ORDER BY vehicle_type_cat ASC ";
		printSql(sql);

		return ConnectionAndStatementUtil.getListOfT(sql, Arrays.asList(fiscalYearID),this::build);

		//return ConnectionAndStatementUtil.getListOfT(sql,this::build);
	}

	public long deleteByFiscalYearID(long parentId, String parentColName) throws Exception {
		long lastModificationTime = System.currentTimeMillis();
		StringBuilder sqlBuilder = new StringBuilder("UPDATE ")
				.append(tableName)
				.append(" SET isDeleted=1,lastModificationTime=")
				.append(lastModificationTime)
				.append(" WHERE " + parentColName + " = ")
				.append(parentId);
		ConnectionAndStatementUtil.getWriteConnectionAndStatement(model -> {
			String sql = sqlBuilder.toString();
			//System.out.println("delete  d sql: "+sql);
			Connection connection = model.getConnection();
			Statement stmt = model.getStatement();
			try {
				logger.debug(sql);
				stmt.execute(sql);
				recordUpdateTime(connection, lastModificationTime);
			} catch (SQLException ex) {
				logger.error(ex);
			}
		});
		return parentId;
	}
				
}
	