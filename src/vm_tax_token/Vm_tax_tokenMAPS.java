package vm_tax_token;
import java.util.*; 
import util.*;


public class Vm_tax_tokenMAPS extends CommonMaps
{	
	public Vm_tax_tokenMAPS(String tableName)
	{
		

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("fiscalYearId".toLowerCase(), "fiscalYearId".toLowerCase());
		java_DTO_map.put("vehicleTypeCat".toLowerCase(), "vehicleTypeCat".toLowerCase());
		java_DTO_map.put("taxTokenFees".toLowerCase(), "taxTokenFees".toLowerCase());
		java_DTO_map.put("fitnessFees".toLowerCase(), "fitnessFees".toLowerCase());
		java_DTO_map.put("digitalNumberFees".toLowerCase(), "digitalNumberFees".toLowerCase());
		java_DTO_map.put("vat".toLowerCase(), "vat".toLowerCase());
		java_DTO_map.put("searchColumn".toLowerCase(), "searchColumn".toLowerCase());
		java_DTO_map.put("insertedBy".toLowerCase(), "insertedBy".toLowerCase());
		java_DTO_map.put("modifiedBy".toLowerCase(), "modifiedBy".toLowerCase());
		java_DTO_map.put("insertionDate".toLowerCase(), "insertionDate".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("fiscal_year_id".toLowerCase(), "fiscalYearId".toLowerCase());
		java_SQL_map.put("vehicle_type_cat".toLowerCase(), "vehicleTypeCat".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Fiscal Year Id".toLowerCase(), "fiscalYearId".toLowerCase());
		java_Text_map.put("Vehicle Type".toLowerCase(), "vehicleTypeCat".toLowerCase());
		java_Text_map.put("Tax Token Fees".toLowerCase(), "taxTokenFees".toLowerCase());
		java_Text_map.put("Fitness Fees".toLowerCase(), "fitnessFees".toLowerCase());
		java_Text_map.put("Digital Number Fees".toLowerCase(), "digitalNumberFees".toLowerCase());
		java_Text_map.put("Vat".toLowerCase(), "vat".toLowerCase());
		java_Text_map.put("Search Column".toLowerCase(), "searchColumn".toLowerCase());
		java_Text_map.put("Inserted By".toLowerCase(), "insertedBy".toLowerCase());
		java_Text_map.put("Modified By".toLowerCase(), "modifiedBy".toLowerCase());
		java_Text_map.put("Insertion Date".toLowerCase(), "insertionDate".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}