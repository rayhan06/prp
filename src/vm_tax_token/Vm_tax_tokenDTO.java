package vm_tax_token;
import java.util.*; 
import util.*; 


public class Vm_tax_tokenDTO extends CommonDTO
{

	public long fiscalYearId = -1;
	public int vehicleTypeCat = -1;
	public double taxTokenFees = -1;
	public double fitnessFees = -1;
	public double digitalNumberFees = -1;
	public double vat = -1;
    public String insertedBy = "";
    public String modifiedBy = "";
	public long insertionDate = -1;
	
	public List<VmTaxTokenItemDTO> vmTaxTokenItemDTOList = new ArrayList<>();
	
    @Override
	public String toString() {
            return "$Vm_tax_tokenDTO[" +
            " iD = " + iD +
            " fiscalYearId = " + fiscalYearId +
            " vehicleTypeCat = " + vehicleTypeCat +
            " taxTokenFees = " + taxTokenFees +
            " fitnessFees = " + fitnessFees +
            " digitalNumberFees = " + digitalNumberFees +
            " vat = " + vat +
            " searchColumn = " + searchColumn +
            " insertedBy = " + insertedBy +
            " modifiedBy = " + modifiedBy +
            " insertionDate = " + insertionDate +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}