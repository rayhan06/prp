package vm_tax_token;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import com.google.gson.Gson;
import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;
import vm_fuel_vendor.VmFuelVendorItemRepository;
import vm_fuel_vendor.Vm_fuel_vendorDTO;


public class Vm_tax_tokenRepository implements Repository {
	Vm_tax_tokenDAO vm_tax_tokenDAO = null;
	Gson gson;
	
	public void setDAO(Vm_tax_tokenDAO vm_tax_tokenDAO)
	{
		this.vm_tax_tokenDAO = vm_tax_tokenDAO;
		gson = new Gson();
	}
	
	
	static Logger logger = Logger.getLogger(Vm_tax_tokenRepository.class);
	Map<Long, Vm_tax_tokenDTO>mapOfVm_tax_tokenDTOToiD;
	Map<Long, Set<Vm_tax_tokenDTO> >mapOfVm_tax_tokenDTOToFiscalYearId;



	static Vm_tax_tokenRepository instance = null;  
	private Vm_tax_tokenRepository(){

		mapOfVm_tax_tokenDTOToiD = new ConcurrentHashMap<>();
		mapOfVm_tax_tokenDTOToFiscalYearId = new ConcurrentHashMap<>();
		
		setDAO(new Vm_tax_tokenDAO());

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Vm_tax_tokenRepository getInstance(){
		if (instance == null){
			instance = new Vm_tax_tokenRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(vm_tax_tokenDAO == null)
		{
			return;
		}
		try {
			List<Vm_tax_tokenDTO> vm_tax_tokenDTOs = (List<Vm_tax_tokenDTO>) vm_tax_tokenDAO.getAll(reloadAll);
			for(Vm_tax_tokenDTO vm_tax_tokenDTO : vm_tax_tokenDTOs) {
				Vm_tax_tokenDTO oldVm_tax_tokenDTO = getVm_tax_tokenDTOByIDWithoutClone(vm_tax_tokenDTO.iD);
				if( oldVm_tax_tokenDTO != null ) {
					mapOfVm_tax_tokenDTOToiD.remove(oldVm_tax_tokenDTO.iD);

					if(mapOfVm_tax_tokenDTOToFiscalYearId.containsKey(oldVm_tax_tokenDTO.fiscalYearId)) {
						mapOfVm_tax_tokenDTOToFiscalYearId.get(oldVm_tax_tokenDTO.fiscalYearId).remove(oldVm_tax_tokenDTO);
					}
					if(mapOfVm_tax_tokenDTOToFiscalYearId.get(oldVm_tax_tokenDTO.fiscalYearId).isEmpty()) {
						mapOfVm_tax_tokenDTOToFiscalYearId.remove(oldVm_tax_tokenDTO.fiscalYearId);
					}

				}
				if(vm_tax_tokenDTO.isDeleted == 0) 
				{
					
					mapOfVm_tax_tokenDTOToiD.put(vm_tax_tokenDTO.iD, vm_tax_tokenDTO);

					if( ! mapOfVm_tax_tokenDTOToFiscalYearId.containsKey(vm_tax_tokenDTO.fiscalYearId)) {
						mapOfVm_tax_tokenDTOToFiscalYearId.put(vm_tax_tokenDTO.fiscalYearId, new HashSet<>());
					}
					mapOfVm_tax_tokenDTOToFiscalYearId.get(vm_tax_tokenDTO.fiscalYearId).add(vm_tax_tokenDTO);

					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Vm_tax_tokenDTO> getVm_tax_tokenList() {
		List <Vm_tax_tokenDTO> vm_tax_tokens = new ArrayList<Vm_tax_tokenDTO>(this.mapOfVm_tax_tokenDTOToiD.values());
		return vm_tax_tokens;
	}

	public Vm_tax_tokenDTO clone(Vm_tax_tokenDTO dto) {
		String raw = gson.toJson(dto);
		return gson.fromJson(raw, Vm_tax_tokenDTO.class);
	}

	public List<Vm_tax_tokenDTO> clone(List<Vm_tax_tokenDTO> dtoList) {
		return dtoList
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}

	public Vm_tax_tokenDTO getVm_tax_tokenDTOByIDWithoutClone( long ID){
		return mapOfVm_tax_tokenDTOToiD.get(ID);
	}
	
	
	public Vm_tax_tokenDTO getVm_tax_tokenDTOByID( long ID){
		//return mapOfVm_tax_tokenDTOToiD.get(ID);
		return dtoWithChildren(clone(mapOfVm_tax_tokenDTOToiD.get(ID)));
	}

	public Vm_tax_tokenDTO dtoWithChildren(Vm_tax_tokenDTO vm_tax_tokenDTO) {
		if (vm_tax_tokenDTO != null) {
			vm_tax_tokenDTO.vmTaxTokenItemDTOList = VmTaxTokenItemRepository
					.getInstance()
					.getVmTaxTokenItemDTOByVmTaxTokenId(
							vm_tax_tokenDTO.iD
					);
		}
		return vm_tax_tokenDTO;
	}

	public List<Vm_tax_tokenDTO> getVm_tax_tokenDTOByFiscal_year_id(long fiscal_year_id) {
		return new ArrayList<>( mapOfVm_tax_tokenDTOToFiscalYearId.getOrDefault(fiscal_year_id,new HashSet<>()));
	}
	
	


	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "vm_tax_token";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


