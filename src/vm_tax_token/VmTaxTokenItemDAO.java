package vm_tax_token;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;
import sessionmanager.SessionConstants;
import java.sql.SQLException;
import common.ConnectionAndStatementUtil;


import org.apache.log4j.Logger;

import dbm.*;

import repository.RepositoryManager;

import util.*;
import pb.*;
import user.UserDTO;
import vm_maintenance.Vm_maintenanceDTO;

public class VmTaxTokenItemDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());

	public VmTaxTokenItemDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";
		commonMaps = new VmTaxTokenItemMAPS(tableName);
		columnNames = new String[] 
		{
			"ID",
			"vehicle_id",
			"vm_tax_token_id",
			"isDigital",
			"total_vat",
			"total",
			"search_column",
			"inserted_by",
			"modified_by",
			"insertion_date",
			"vmRegNo",
			"vehicleTypeCat",
			"taxTokenFees",
			"fitnessFees",
			"digitalNumberFees",
			"digitalRegFees",
			"vat",
			"fiscalYearId",
			"isDeleted",
			"lastModificationTime"
		};
	}
	
	public VmTaxTokenItemDAO()
	{
		this("vm_tax_token_item");		
	}
	
	public void setSearchColumn(VmTaxTokenItemDTO vmtaxtokenitemDTO)
	{
		vmtaxtokenitemDTO.searchColumn = "";
	}
	
	public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException
	{
		VmTaxTokenItemDTO vmtaxtokenitemDTO = (VmTaxTokenItemDTO)commonDTO;
		int index = 1;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(vmtaxtokenitemDTO);
		if(isInsert)
		{
			ps.setObject(index++,vmtaxtokenitemDTO.iD);
		}
		ps.setObject(index++,vmtaxtokenitemDTO.vehicleId);
		ps.setObject(index++,vmtaxtokenitemDTO.vmTaxTokenId);
		ps.setObject(index++,vmtaxtokenitemDTO.isDigital);
		ps.setObject(index++,vmtaxtokenitemDTO.totalVat);
		ps.setObject(index++,vmtaxtokenitemDTO.total);
		ps.setObject(index++,vmtaxtokenitemDTO.searchColumn);
		ps.setObject(index++,vmtaxtokenitemDTO.insertedBy);
		ps.setObject(index++,vmtaxtokenitemDTO.modifiedBy);
		ps.setObject(index++,vmtaxtokenitemDTO.insertionDate);

		

		ps.setObject(index++,vmtaxtokenitemDTO.vmRegNo);
		ps.setObject(index++,vmtaxtokenitemDTO.vehicleTypeCat);
		ps.setObject(index++,vmtaxtokenitemDTO.taxTokenFees);
		ps.setObject(index++,vmtaxtokenitemDTO.fitnessFees);
		ps.setObject(index++,vmtaxtokenitemDTO.digitalNumberFees);
		ps.setObject(index++,vmtaxtokenitemDTO.digitalRegFees);
		ps.setObject(index++,vmtaxtokenitemDTO.vat);
		ps.setObject(index++,vmtaxtokenitemDTO.fiscalYearId);


		if(isInsert)
		{
			ps.setObject(index++, 0);
			ps.setObject(index++, lastModificationTime);
		}
	}
	
	public VmTaxTokenItemDTO build(ResultSet rs)
	{
		try
		{
			VmTaxTokenItemDTO vmtaxtokenitemDTO = new VmTaxTokenItemDTO();
			vmtaxtokenitemDTO.iD = rs.getLong("ID");
			vmtaxtokenitemDTO.vehicleId = rs.getLong("vehicle_id");
			vmtaxtokenitemDTO.vmTaxTokenId = rs.getLong("vm_tax_token_id");
			vmtaxtokenitemDTO.isDigital = rs.getBoolean("isDigital");
			vmtaxtokenitemDTO.totalVat = rs.getDouble("total_vat");
			vmtaxtokenitemDTO.total = rs.getDouble("total");
			vmtaxtokenitemDTO.searchColumn = rs.getString("search_column");
			vmtaxtokenitemDTO.insertedBy = rs.getString("inserted_by");
			vmtaxtokenitemDTO.modifiedBy = rs.getString("modified_by");
			vmtaxtokenitemDTO.insertionDate = rs.getLong("insertion_date");


			vmtaxtokenitemDTO.vmRegNo = rs.getString("vmRegNo");
			vmtaxtokenitemDTO.vehicleTypeCat = rs.getInt("vehicleTypeCat");
			vmtaxtokenitemDTO.taxTokenFees = rs.getDouble("taxTokenFees");
			vmtaxtokenitemDTO.fitnessFees = rs.getDouble("fitnessFees");
			vmtaxtokenitemDTO.digitalNumberFees = rs.getDouble("digitalNumberFees");
			vmtaxtokenitemDTO.digitalRegFees = rs.getDouble("digitalRegFees");
			vmtaxtokenitemDTO.vat = rs.getDouble("vat");
			vmtaxtokenitemDTO.fiscalYearId = rs.getLong("fiscalYearId");

			vmtaxtokenitemDTO.isDeleted = rs.getInt("isDeleted");
			vmtaxtokenitemDTO.lastModificationTime = rs.getLong("lastModificationTime");
			return vmtaxtokenitemDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
	
	
	public List<VmTaxTokenItemDTO> getVmTaxTokenItemDTOListByVmTaxTokenID(long vmTaxTokenID) throws Exception
	{
		String sql = "SELECT * FROM vm_tax_token_item where isDeleted=0 and vehicle_id="+vmTaxTokenID+" order by vm_tax_token_item.lastModificationTime";
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);		
	}

	public VmTaxTokenItemDTO getDTOByID (long id)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID = " + id;
		VmTaxTokenItemDTO vmtaxtokenitemDTO = ConnectionAndStatementUtil.getT(sql,this::build);
		return vmtaxtokenitemDTO;
	}

	
	public List<VmTaxTokenItemDTO> getDTOs(Collection recordIDs)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID IN ( ";
		for(int i = 0;i<recordIDs.size();i++){
			if(i!=0){
				sql+=",";
			}
			sql+=((ArrayList)recordIDs).get(i);
		}
		sql+=")  order by lastModificationTime desc";
		printSql(sql);
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);		
	}
	
	public List<VmTaxTokenItemDTO> getAllVmTaxTokenItem (boolean isFirstReload)
    {
		String sql = "SELECT * FROM " + tableName + " WHERE ";
		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by " + tableName + ".lastModificationTime desc";
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);	
    }

	
	public List<VmTaxTokenItemDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO)
	{
		return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
	}

	
	public List<VmTaxTokenItemDTO> getDTOs(Hashtable  p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
			String filter, boolean tableHasJobCat)
	{
		String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat);
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);			
	}

	public List<VmTaxTokenItemDTO> getAllVmTaxTokenItemByFiscalYearID (long fiscalYearID)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ";

		sql+=" fiscalYearID =  "+fiscalYearID;

		sql+=" AND isDeleted =  0";
		sql += " order by " + tableName + ".vehicleTypeCat asc";
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);
	}

	public List<VmTaxTokenItemDTO> getAllVmTaxTokenItemByFiscalYearIDWithDeleted (long fiscalYearID)
	{
		String sql = "SELECT * FROM " + tableName + " as t1, vm_tax_token as t2 WHERE ";

		sql+=" t1.fiscalYearID =  "+fiscalYearID;

		sql+=" AND t1.vm_tax_token_id =  t2.id ";

		sql+=" AND t2.isDeleted = 0 ";

		//sql+=" AND isDeleted =  0";
		//sql += " order by " + tableName + ".vehicleTypeCat asc";
		sql += " order by t1.vehicleTypeCat asc";
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);
	}

	public List<VmTaxTokenItemDTO> getDTOsByFiscalYearID (long fiscalYearID)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE fiscal_year_id = " + fiscalYearID;
		sql += " ORDER BY vehicle_type_cat ASC ";
		printSql(sql);
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);
	}

	public long deleteByFiscalYearID(long parentId, String parentColName) throws Exception {
		long lastModificationTime = System.currentTimeMillis();
		StringBuilder sqlBuilder = new StringBuilder("UPDATE ")
				.append(tableName)
				.append(" SET isDeleted=1,lastModificationTime=")
				.append(lastModificationTime)
				.append(" WHERE " + parentColName + " = ")
				.append(parentId);
		ConnectionAndStatementUtil.getWriteConnectionAndStatement(model -> {
			String sql = sqlBuilder.toString();
			Connection connection = model.getConnection();
			Statement stmt = model.getStatement();
			try {
				logger.debug(sql);
				stmt.execute(sql);
				recordUpdateTime(connection, lastModificationTime);
			} catch (SQLException ex) {
				logger.error(ex);
			}
		});
		return parentId;
	}

	public List<VmTaxTokenItemDTO> getAllApprovedVm_taxTokenFeesByVehicleIdAndFiscalYear(long vehicleId, List<Long> fiscalYearIds, Boolean flag){

//		String sql = "SELECT * FROM " + tableName + " WHERE ";
//		sql+=" isDeleted =  0";
//		sql+=" and vehicle_id = " + vehicleId ;
//
//		if(!flag){
//			sql +=  " and fiscalYearID IN ( ";
//			for(int i = 0;i<fiscalYearIds.size();i++){
//				if(i!=0){
//					sql+=",";
//				}
//				sql+=fiscalYearIds.get(i);
//			}
//			sql+=" ) ";
//		}
//		else{
//
//		}
//		sql += " order by " + tableName + ".fiscalYearID asc";
//		System.out.println("report vmTax sql: "+sql);
//		return ConnectionAndStatementUtil.getListOfT(sql,this::build);


		if(flag){
			return VmTaxTokenItemRepository.getInstance().getVmTaxTokenItemDTOByVmVehicleId(vehicleId);
		}
		else{
			
			return VmTaxTokenItemRepository.getInstance().getVmTaxTokenItemDTOByVmVehicleIdAndFiscalYearIds(vehicleId,fiscalYearIds);

		}

	}


				
}
	