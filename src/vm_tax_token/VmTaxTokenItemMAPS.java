package vm_tax_token;
import java.util.*; 
import util.*;


public class VmTaxTokenItemMAPS extends CommonMaps
{	
	public VmTaxTokenItemMAPS(String tableName)
	{
		

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("vehicleId".toLowerCase(), "vehicleId".toLowerCase());
		java_DTO_map.put("vmTaxTokenId".toLowerCase(), "vmTaxTokenId".toLowerCase());
		java_DTO_map.put("isDigital".toLowerCase(), "isDigital".toLowerCase());
		java_DTO_map.put("totalVat".toLowerCase(), "totalVat".toLowerCase());
		java_DTO_map.put("total".toLowerCase(), "total".toLowerCase());
		java_DTO_map.put("searchColumn".toLowerCase(), "searchColumn".toLowerCase());
		java_DTO_map.put("insertedBy".toLowerCase(), "insertedBy".toLowerCase());
		java_DTO_map.put("modifiedBy".toLowerCase(), "modifiedBy".toLowerCase());
		java_DTO_map.put("insertionDate".toLowerCase(), "insertionDate".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("vehicle_id".toLowerCase(), "vehicleId".toLowerCase());
		java_SQL_map.put("vm_tax_token_id".toLowerCase(), "vmTaxTokenId".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Vehicle Id".toLowerCase(), "vehicleId".toLowerCase());
		java_Text_map.put("Vm Tax Token Id".toLowerCase(), "vmTaxTokenId".toLowerCase());
		java_Text_map.put("IsDigital".toLowerCase(), "isDigital".toLowerCase());
		java_Text_map.put("Total Vat".toLowerCase(), "totalVat".toLowerCase());
		java_Text_map.put("Total".toLowerCase(), "total".toLowerCase());
		java_Text_map.put("Search Column".toLowerCase(), "searchColumn".toLowerCase());
		java_Text_map.put("Inserted By".toLowerCase(), "insertedBy".toLowerCase());
		java_Text_map.put("Modified By".toLowerCase(), "modifiedBy".toLowerCase());
		java_Text_map.put("Insertion Date".toLowerCase(), "insertionDate".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}