package vm_tax_token;
import java.util.*; 
import util.*; 


public class VmTaxTokenItemDTO extends CommonDTO
{

	public long vehicleId = -1;
	public long vmTaxTokenId = -1;
	public boolean isDigital = false;
	public double totalVat = -1;
	public double total = -1;
    public String insertedBy = "";
    public String modifiedBy = "";
	public long insertionDate = -1;

	public String vmRegNo = "";
	public int vehicleTypeCat = -1;
	public double taxTokenFees = -1;
	public double fitnessFees = -1;
	public double digitalNumberFees = -1;
	public double digitalRegFees = -1;
	public double vat = -1;

	public long fiscalYearId = -1;
	
	public List<VmTaxTokenItemDTO> vmTaxTokenItemDTOList = new ArrayList<>();
	
    @Override
	public String toString() {
            return "$VmTaxTokenItemDTO[" +
            " iD = " + iD +
            " vehicleId = " + vehicleId +
            " vmTaxTokenId = " + vmTaxTokenId +
            " isDigital = " + isDigital +
            " totalVat = " + totalVat +
            " total = " + total +
            " searchColumn = " + searchColumn +
            " insertedBy = " + insertedBy +
            " modifiedBy = " + modifiedBy +
            " insertionDate = " + insertionDate +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
			" vmRegNo = " + vmRegNo +
			" vehicleTypeCat = " + vehicleTypeCat +
			" taxTokenFees = " + taxTokenFees +
			" fitnessFees = " + fitnessFees +
			" digitalNumberFees = " + digitalNumberFees +
			" digitalRegFees = " + digitalRegFees +
			" vat = " + vat +
			" fiscalYearId = " + fiscalYearId +
            "]";
    }

}