package vm_tax_token;

import com.google.gson.Gson;
import org.apache.log4j.Logger;
import repository.Repository;
import repository.RepositoryManager;
import vm_fuel_request.Vm_fuel_requestDTO;


import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;


public class VmTaxTokenItemRepository implements Repository {
	VmTaxTokenItemDAO VmTaxTokenItemDAO = null;
	Gson gson;

	public void setDAO(VmTaxTokenItemDAO VmTaxTokenItemDAO)
	{
		this.VmTaxTokenItemDAO = VmTaxTokenItemDAO;
		gson = new Gson();

	}


	static Logger logger = Logger.getLogger(VmTaxTokenItemRepository.class);
	Map<Long, VmTaxTokenItemDTO>mapOfVmTaxTokenItemDTOToiD;
	Map<Long, Set<VmTaxTokenItemDTO> >mapOfVmTaxTokenItemDTOToVmTaxTokenId;
	Map<Long, Set<VmTaxTokenItemDTO> >mapOfVmTaxTokenItemDTOToVmVehicleId;
	Map<Long, Set<VmTaxTokenItemDTO> >mapOfVmTaxTokenItemDTOToFiscalYearId;



	static VmTaxTokenItemRepository instance = null;
	private VmTaxTokenItemRepository(){
		mapOfVmTaxTokenItemDTOToiD = new ConcurrentHashMap<>();
		mapOfVmTaxTokenItemDTOToVmTaxTokenId = new ConcurrentHashMap<>();
		mapOfVmTaxTokenItemDTOToVmVehicleId = new ConcurrentHashMap<>();
		mapOfVmTaxTokenItemDTOToFiscalYearId = new ConcurrentHashMap<>();


		setDAO(new VmTaxTokenItemDAO());

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static VmTaxTokenItemRepository getInstance(){
		if (instance == null){
			instance = new VmTaxTokenItemRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(VmTaxTokenItemDAO == null)
		{
			return;
		}

		try {
			List<VmTaxTokenItemDTO> Vm_tax_token_itemDTOs = VmTaxTokenItemDAO.getAllVmTaxTokenItem(reloadAll);
			for(VmTaxTokenItemDTO Vm_tax_token_itemDTO : Vm_tax_token_itemDTOs) {
				VmTaxTokenItemDTO oldVmTaxTokenItemDTO = getVmTaxTokenItemDTOByIDWithoutClone(Vm_tax_token_itemDTO.iD);
				if( oldVmTaxTokenItemDTO != null ) {
					mapOfVmTaxTokenItemDTOToiD.remove(oldVmTaxTokenItemDTO.iD);

					if(mapOfVmTaxTokenItemDTOToVmTaxTokenId.containsKey(oldVmTaxTokenItemDTO.vmTaxTokenId)) {
						mapOfVmTaxTokenItemDTOToVmTaxTokenId.get(oldVmTaxTokenItemDTO.vmTaxTokenId).remove(oldVmTaxTokenItemDTO);
					}
					if(mapOfVmTaxTokenItemDTOToVmTaxTokenId.get(oldVmTaxTokenItemDTO.vmTaxTokenId).isEmpty()) {
						mapOfVmTaxTokenItemDTOToVmTaxTokenId.remove(oldVmTaxTokenItemDTO.vmTaxTokenId);
					}

					if(mapOfVmTaxTokenItemDTOToVmVehicleId.containsKey(oldVmTaxTokenItemDTO.vehicleId)) {
						mapOfVmTaxTokenItemDTOToVmVehicleId.get(oldVmTaxTokenItemDTO.vehicleId).remove(oldVmTaxTokenItemDTO);
					}
					if(mapOfVmTaxTokenItemDTOToVmVehicleId.get(oldVmTaxTokenItemDTO.vehicleId).isEmpty()) {
						mapOfVmTaxTokenItemDTOToVmVehicleId.remove(oldVmTaxTokenItemDTO.vehicleId);
					}

					if(mapOfVmTaxTokenItemDTOToFiscalYearId.containsKey(oldVmTaxTokenItemDTO.fiscalYearId)) {
						mapOfVmTaxTokenItemDTOToFiscalYearId.get(oldVmTaxTokenItemDTO.fiscalYearId).remove(oldVmTaxTokenItemDTO);
					}
					if(mapOfVmTaxTokenItemDTOToFiscalYearId.get(oldVmTaxTokenItemDTO.fiscalYearId).isEmpty()) {
						mapOfVmTaxTokenItemDTOToFiscalYearId.remove(oldVmTaxTokenItemDTO.fiscalYearId);
					}


				}
				if(Vm_tax_token_itemDTO.isDeleted == 0)
				{

					mapOfVmTaxTokenItemDTOToiD.put(Vm_tax_token_itemDTO.iD, Vm_tax_token_itemDTO);

					if( ! mapOfVmTaxTokenItemDTOToVmTaxTokenId.containsKey(Vm_tax_token_itemDTO.vmTaxTokenId)) {
						mapOfVmTaxTokenItemDTOToVmTaxTokenId.put(Vm_tax_token_itemDTO.vmTaxTokenId, new HashSet<>());
					}
					mapOfVmTaxTokenItemDTOToVmTaxTokenId.get(Vm_tax_token_itemDTO.vmTaxTokenId).add(Vm_tax_token_itemDTO);

					if( ! mapOfVmTaxTokenItemDTOToVmVehicleId.containsKey(Vm_tax_token_itemDTO.vehicleId)) {
						mapOfVmTaxTokenItemDTOToVmVehicleId.put(Vm_tax_token_itemDTO.vehicleId, new HashSet<>());
					}
					mapOfVmTaxTokenItemDTOToVmVehicleId.get(Vm_tax_token_itemDTO.vehicleId).add(Vm_tax_token_itemDTO);

					if( ! mapOfVmTaxTokenItemDTOToFiscalYearId.containsKey(Vm_tax_token_itemDTO.fiscalYearId)) {
						mapOfVmTaxTokenItemDTOToFiscalYearId.put(Vm_tax_token_itemDTO.fiscalYearId, new HashSet<>());
					}
					mapOfVmTaxTokenItemDTOToFiscalYearId.get(Vm_tax_token_itemDTO.fiscalYearId).add(Vm_tax_token_itemDTO);

				}
			}

		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}

	public VmTaxTokenItemDTO clone(VmTaxTokenItemDTO dto) {
		String raw = gson.toJson(dto);
		return gson.fromJson(raw, VmTaxTokenItemDTO.class);
	}

	public List<VmTaxTokenItemDTO> clone(List<VmTaxTokenItemDTO> dtoList) {
		return dtoList
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}

	public VmTaxTokenItemDTO getVmTaxTokenItemDTOByIDWithoutClone( long ID){
		return mapOfVmTaxTokenItemDTOToiD.get(ID);
	}

	public List<VmTaxTokenItemDTO> getVmTaxTokenItemList() {
		List <VmTaxTokenItemDTO> Vm_tax_token_items = new ArrayList<VmTaxTokenItemDTO>(this.mapOfVmTaxTokenItemDTOToiD.values());
		return Vm_tax_token_items;
	}


	public VmTaxTokenItemDTO getVmTaxTokenItemDTOByID( long ID){
		return clone(mapOfVmTaxTokenItemDTOToiD.get(ID));
	}


	public List<VmTaxTokenItemDTO> getVmTaxTokenItemDTOByVmTaxTokenId(long vmTaxTokenId) {
		return new ArrayList<>( mapOfVmTaxTokenItemDTOToVmTaxTokenId.getOrDefault(vmTaxTokenId,new HashSet<>()))
				.stream()
				.map(VmTaxTokenItemDTO -> clone(VmTaxTokenItemDTO))
				.collect(Collectors.toList());
	}

	public List<VmTaxTokenItemDTO> getVmTaxTokenItemDTOByVmVehicleId(long vehicleId) {
		return new ArrayList<>( mapOfVmTaxTokenItemDTOToVmVehicleId.getOrDefault(vehicleId,new HashSet<>()))
				.stream()
				.map(VmTaxTokenItemDTO -> clone(VmTaxTokenItemDTO))
				.collect(Collectors.toList());
	}

	public List<VmTaxTokenItemDTO> getVmTaxTokenItemDTOByVmVehicleIdAndFiscalYearIds(long vehicleId, List<Long> fiscalYearIds) {
		List<VmTaxTokenItemDTO> vmTaxTokenItemDTOS = (new ArrayList<>( mapOfVmTaxTokenItemDTOToVmVehicleId.getOrDefault(vehicleId,new HashSet<>())));
		return vmTaxTokenItemDTOS
				.stream().filter(vmTaxTokenItemDTO -> fiscalYearIds.contains(vmTaxTokenItemDTO.fiscalYearId))
				.map(VmTaxTokenItemDTO -> clone(VmTaxTokenItemDTO))
				.sorted((f1, f2) -> Long.compare(f1.fiscalYearId, f2.fiscalYearId))
				.collect(Collectors.toList());
	}


//	public void deleteVmFuelRequestItemDTOByvmFuelRequestId(long vmFuelRequestId) {
//		if(mapOfVmFuelRequestItemDTOTovmFuelRequestId.containsKey(vmFuelRequestId)) {
//			mapOfVmFuelRequestItemDTOTovmFuelRequestId.remove(vmFuelRequestId);
//		}
//	}


	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "vm_tax_token_item";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


