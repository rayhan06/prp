package vm_tax_token;

import java.io.IOException;
import java.io.*;
import java.text.SimpleDateFormat;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import common.ApiResponse;
import job_applicant_application.Job_applicant_applicationDTO;
import org.apache.log4j.Logger;

import login.LoginDTO;
import permission.MenuConstants;
import recruitment_bulk_mark_entry.Recruitment_bulk_mark_entryDTO;
import role.PermissionRepository;


import sessionmanager.SessionConstants;

import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;
import util.CommonRequestHandler;
import util.RecordNavigationManager4;

import java.util.*;
import javax.servlet.http.*;
import java.util.UUID;





import geolocation.GeoLocationDAO2;
import java.util.StringTokenizer;

import com.google.gson.Gson;

import pb.*;
import pbReport.*;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import vm_tax_token_parent.Vm_tax_token_parentDAO;
import vm_tax_token_parent.Vm_tax_token_parentDTO;
import vm_vehicle.ValidationDTO;
import vm_vehicle.Vm_vehicleDAO;
import vm_vehicle.Vm_vehicleDTO;


/**
 * Servlet implementation class Vm_tax_tokenServlet
 */
@WebServlet("/Vm_tax_tokenServlet")
@MultipartConfig
public class Vm_tax_tokenServlet extends HttpServlet
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Vm_tax_tokenServlet.class);

    String tableName = "vm_tax_token";
    String vehicleTableName="vm_vehicle";
	String vmTaxTokenItemTableName="vm_tax_token_item";
	Vm_vehicleDAO vm_vehicleDAO;

	Vm_tax_tokenDAO vm_tax_tokenDAO;

	VmTaxTokenItemDAO vmTaxTokenItemDAO;

	Vm_tax_token_parentDAO vm_tax_token_parentDAO;

	CommonRequestHandler commonRequestHandler;
	CommonRequestHandler commonRequestHandler2;
	CommonRequestHandler commonRequestHandler3;
    private final Gson gson = new Gson();

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Vm_tax_tokenServlet()
	{
        super();
    	try
    	{
			vm_tax_tokenDAO = new Vm_tax_tokenDAO(tableName);
			vmTaxTokenItemDAO = new VmTaxTokenItemDAO("vm_tax_token_item");
			vm_tax_token_parentDAO = new Vm_tax_token_parentDAO("vm_tax_token_parent");
			vm_vehicleDAO = new Vm_vehicleDAO(vehicleTableName);
			commonRequestHandler = new CommonRequestHandler(vm_tax_tokenDAO);
			commonRequestHandler2 = new CommonRequestHandler(vmTaxTokenItemDAO);
			commonRequestHandler3 = new CommonRequestHandler(vm_tax_token_parentDAO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		System.out.println("In doget request = " + request);
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		try
		{
			String actionType = request.getParameter("actionType");
			if(actionType.equals("getAddPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_TAX_TOKEN_ADD))
				{
					commonRequestHandler.getAddPage(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getEditPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_TAX_TOKEN_UPDATE))
				{
					getVm_tax_token(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getNewEditPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_TAX_TOKEN_UPDATE))
				{
					getNewVm_tax_token(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getURL"))
			{
				String URL = request.getParameter("URL");
				System.out.println("URL = " + URL);
				response.sendRedirect(URL);
			}
			else if(actionType.equals("search"))
			{
				System.out.println("search requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_TAX_TOKEN_SEARCH))
				{
					if(isPermanentTable)
					{
						String filter = request.getParameter("filter");
						System.out.println("filter = " + filter);
						if(filter!=null)
						{
							filter = ""; //shouldn't be directly used, rather manipulate it.
							searchVm_tax_token(request, response, isPermanentTable, filter);
						}
						else
						{
							searchVm_tax_token(request, response, isPermanentTable, "");
						}
					}
					else
					{
						//searchVm_tax_token(request, response, tempTableName, isPermanentTable);
					}
				}
			}
			/*AllVehicleSearch started*/

			else if(actionType.equals("AllVehicleSearch"))
			{
				System.out.println("AllVehicleSearch requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_TAX_TOKEN_SEARCH))
				{
					if(isPermanentTable)
					{
						String filter = request.getParameter("filter");
						String fiscalYearID = request.getParameter("fiscalYearID");
						System.out.println("fiscalYearID in doget = " + fiscalYearID);
						if(filter!=null)
						{
							filter = ""; //shouldn't be directly used, rather manipulate it.
							allVehicleSearchVm_tax_token(request, response, isPermanentTable, filter);
						}
						else
						{
							allVehicleSearchVm_tax_token(request, response, isPermanentTable, "");
						}
					}
					else
					{
						//searchVm_tax_token(request, response, tempTableName, isPermanentTable);
					}
				}
			}

			/*AllVehicleSearch ended*/
			else if(actionType.equals("view"))
			{
				System.out.println("view requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_TAX_TOKEN_SEARCH))
				{
					commonRequestHandler.view(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}

			else if(actionType.equals("checkFiscalYearValidation")){

				checkFiscalYearValidation(request, response);
			}

			else
			{
				request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}





	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		// TODO Auto-generated method stub
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		System.out.println("doPost");
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);

		try
		{
			String actionType = request.getParameter("actionType");
			System.out.println("actionType = " + actionType);
			if(actionType.equals("add"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_TAX_TOKEN_ADD))
				{
					System.out.println("going to  addVm_tax_token ");
					addVm_tax_token(request, response, true, userDTO, true);
				}
				else
				{
					System.out.println("Not going to  addVm_tax_token ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}

			else if(actionType.equals("allExistingVehicleDeleteAndAdd"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_TAX_TOKEN_ADD))
				{

					System.out.println("going to  allExistingVehicleDeleteAndAdd ");

					//request.setAttribute("ID",request.getParameter("fiscalYearId"));

					vm_tax_tokenDAO.deleteByFiscalYearID(Long.parseLong(request.getParameter("fiscalYearId")),"fiscal_year_id");
					vmTaxTokenItemDAO.deleteByFiscalYearID(Long.parseLong(request.getParameter("fiscalYearId")),"fiscalYearId");
					vm_tax_token_parentDAO.deleteByFiscalYearID(Long.parseLong(request.getParameter("fiscalYearId")),"fiscal_year_id");


					addVm_tax_token(request, response, true, userDTO, true);
				}
				else
				{
					System.out.println("Not going to  allExistingVehicleDeleteAndAdd ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}

			else if(actionType.equals("getDTO"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_TAX_TOKEN_ADD))
				{
					getDTO(request, response);
				}
				else
				{
					System.out.println("Not going to  addVm_tax_token ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}
			else if(actionType.equals("edit"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_TAX_TOKEN_UPDATE))
				{
					addVm_tax_token(request, response, false, userDTO, isPermanentTable);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("delete"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_TAX_TOKEN_UPDATE))
				{
					commonRequestHandler.delete(request, response, userDTO);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("search"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_TAX_TOKEN_SEARCH))
				{
					searchVm_tax_token(request, response, true, "");
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("deletedSelectedRow"))
			{
				deleteSelectedRow(request, response);
			}
			else if(actionType.equals("allVehicleDataSubmit"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_TAX_TOKEN_UPDATE))
				{
					allVehicleDataSubmit(request, response);
                }
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}


			else if(actionType.equals("getGeo"))
			{
				System.out.println("going to geoloc ");
				request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
			}

		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}

	private void getDTO(HttpServletRequest request, HttpServletResponse response)
	{
		try
		{
			System.out.println("In getDTO");
			//Vm_tax_tokenDTO vm_tax_tokenDTO = (Vm_tax_tokenDTO)vm_tax_tokenDAO.getDTOByID(Long.parseLong(request.getParameter("ID")));
			Vm_tax_tokenDTO vm_tax_tokenDTO = Vm_tax_tokenRepository.getInstance().getVm_tax_tokenDTOByID(Long.parseLong(request.getParameter("ID")));
			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");

			String encoded = this.gson.toJson(vm_tax_tokenDTO);
			System.out.println("json encoded data = " + encoded);
			out.print(encoded);
			out.flush();
		}
		catch (NumberFormatException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void addVm_tax_token(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, boolean isPermanentTable) throws IOException
	{
		// TODO Auto-generated method stub
		try
		{
			request.setAttribute("failureMessage", "");
			System.out.println("%%%% addVm_tax_token");
			String path = getServletContext().getRealPath("/img2/");
			Vm_tax_tokenDTO vm_tax_tokenDTO;
			SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");

			long returnedID = -1;

			String[] paramValues = request.getParameterValues("iD");
			System.out.println("paramValues len: "+paramValues.length);

			String fiscalValue = "";

			fiscalValue = request.getParameter("fiscalYearId");

			for (int i = 0; i < paramValues.length; i++)
			{
				String Value = "";
				if(addFlag == true)
				{
					vm_tax_tokenDTO = new Vm_tax_tokenDTO();

					if(request.getParameterValues("iD") != null)
					{
						Value = request.getParameterValues("iD")[i];
						if(Value != null)
						{
							Value = Jsoup.clean(Value,Whitelist.simpleText());
						}
						System.out.println("iD = " + Value);
						if(Value != null && !Value.equalsIgnoreCase(""))
						{
							vm_tax_tokenDTO.iD = Long.parseLong(Value);
						}
						else
						{
							System.out.println("FieldName has a null Value, not updating" + " = " + Value);
						}
					}
				}
				else
				{
					//vm_tax_tokenDTO = (Vm_tax_tokenDTO)vm_tax_tokenDAO.getDTOByID(Long.parseLong(request.getParameter("iD")));
					vm_tax_tokenDTO = Vm_tax_tokenRepository.getInstance().getVm_tax_tokenDTOByID(Long.parseLong(request.getParameter("iD")));
				}



				//Value = request.getParameter("fiscalYearId");
				Value = fiscalValue;

				if(Value != null && !Value.equalsIgnoreCase(""))
				{
					Value = Jsoup.clean(Value,Whitelist.simpleText());
				}
				System.out.println("fiscalYearId = " + Value);
				if(Value != null && !Value.equalsIgnoreCase(""))
				{
					vm_tax_tokenDTO.fiscalYearId = Long.parseLong(Value);
				}
				else
				{
					System.out.println("FieldName has a null Value, not updating" + " = " + Value);
				}

				//Value = request.getParameter("vehicleTypeCat");
				Value = request.getParameterValues("vehicleTypeCat")[i];

				if(Value != null && !Value.equalsIgnoreCase(""))
				{
					Value = Jsoup.clean(Value,Whitelist.simpleText());
				}
				System.out.println("vehicleTypeCat = " + Value);
				if(Value != null && !Value.equalsIgnoreCase(""))
				{
					vm_tax_tokenDTO.vehicleTypeCat = Integer.parseInt(Value);


				}
				else
				{
					System.out.println("FieldName has a null Value, not updating" + " = " + Value);
				}

				//Value = request.getParameter("taxTokenFees");
				Value = request.getParameterValues("taxTokenFees")[i];

				if(Value != null && !Value.equalsIgnoreCase(""))
				{
					Value = Jsoup.clean(Value,Whitelist.simpleText());
				}
				System.out.println("taxTokenFees = " + Value);
				if(Value != null && !Value.equalsIgnoreCase(""))
				{
					vm_tax_tokenDTO.taxTokenFees = Double.parseDouble(Value);
				}
				else
				{
					System.out.println("FieldName has a null Value, not updating" + " = " + Value);
				}

				//Value = request.getParameter("fitnessFees");
				Value = request.getParameterValues("fitnessFees")[i];

				if(Value != null && !Value.equalsIgnoreCase(""))
				{
					Value = Jsoup.clean(Value,Whitelist.simpleText());
				}
				System.out.println("fitnessFees = " + Value);
				if(Value != null && !Value.equalsIgnoreCase(""))
				{
					vm_tax_tokenDTO.fitnessFees = Double.parseDouble(Value);
				}
				else
				{
					System.out.println("FieldName has a null Value, not updating" + " = " + Value);
				}

				//Value = request.getParameter("digitalNumberFees");
				Value = request.getParameterValues("digitalNumberFees")[i];

				if(Value != null && !Value.equalsIgnoreCase(""))
				{
					Value = Jsoup.clean(Value,Whitelist.simpleText());
				}
				System.out.println("digitalNumberFees = " + Value);
				if(Value != null && !Value.equalsIgnoreCase(""))
				{
					vm_tax_tokenDTO.digitalNumberFees = Double.parseDouble(Value);
				}
				else
				{
					System.out.println("FieldName has a null Value, not updating" + " = " + Value);
				}

				//Value = request.getParameter("vat");
				Value = request.getParameterValues("vat")[i];

				if(Value != null && !Value.equalsIgnoreCase(""))
				{
					Value = Jsoup.clean(Value,Whitelist.simpleText());
				}
				System.out.println("vat = " + Value);
				if(Value != null && !Value.equalsIgnoreCase(""))
				{
					vm_tax_tokenDTO.vat = Double.parseDouble(Value);
				}
				else
				{
					System.out.println("FieldName has a null Value, not updating" + " = " + Value);
				}

				//Value = request.getParameter("searchColumn");
//				Value = request.getParameterValues("searchColumn")[i];
//
//				if(Value != null)
//				{
//					Value = Jsoup.clean(Value,Whitelist.simpleText());
//				}
//				System.out.println("searchColumn = " + Value);
//				if(Value != null)
//				{
//					vm_tax_tokenDTO.searchColumn = (Value);
//				}
//				else
//				{
//					System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//				}
//
//				//Value = request.getParameter("insertedBy");
//				Value = request.getParameterValues("insertedBy")[i];
//
//				if(Value != null)
//				{
//					Value = Jsoup.clean(Value,Whitelist.simpleText());
//				}
//				System.out.println("insertedBy = " + Value);
//				if(Value != null)
//				{
//					vm_tax_tokenDTO.insertedBy = (Value);
//				}
//				else
//				{
//					System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//				}
//
//				//Value = request.getParameter("modifiedBy");
//				Value = request.getParameterValues("modifiedBy")[i];
//
//				if(Value != null)
//				{
//					Value = Jsoup.clean(Value,Whitelist.simpleText());
//				}
//				System.out.println("modifiedBy = " + Value);
//				if(Value != null)
//				{
//					vm_tax_tokenDTO.modifiedBy = (Value);
//				}
//				else
//				{
//					System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//				}

				if(addFlag)
				{
					Calendar c = Calendar.getInstance();
					c.set(Calendar.HOUR_OF_DAY, 0);
					c.set(Calendar.MINUTE, 0);
					c.set(Calendar.SECOND, 0);
					c.set(Calendar.MILLISECOND, 0);

					vm_tax_tokenDTO.insertionDate = c.getTimeInMillis();
				}


				System.out.println("Done adding  addVm_tax_token dto = " + vm_tax_tokenDTO);


				if(isPermanentTable == false) //add new row for validation and make the old row outdated
				{
					vm_tax_tokenDAO.setIsDeleted(vm_tax_tokenDTO.iD, CommonDTO.OUTDATED);
					returnedID = vm_tax_tokenDAO.add(vm_tax_tokenDTO);
					vm_tax_tokenDAO.setIsDeleted(returnedID, CommonDTO.WAITING_FOR_APPROVAL);
				}
				else if(addFlag == true)
				{
					returnedID = vm_tax_tokenDAO.manageWriteOperations(vm_tax_tokenDTO, SessionConstants.INSERT, -1, userDTO);
				}
				else
				{
					returnedID = vm_tax_tokenDAO.manageWriteOperations(vm_tax_tokenDTO, SessionConstants.UPDATE, -1, userDTO);
				}



				List<Vm_vehicleDTO> vm_vehicleDTOList = vm_vehicleDAO.getDTOsByVehicleType(vm_tax_tokenDTO.vehicleTypeCat);
				//System.out.println("vm_vehicleDTOList size: "+vm_vehicleDTOList.size()+ "vm_tax_tokenDTO.vehicleTypeCat: "+vm_tax_tokenDTO.vehicleTypeCat);
				for (Vm_vehicleDTO vm_vehicleDTO: vm_vehicleDTOList) {
					VmTaxTokenItemDTO vmTaxTokenItemDTO = new VmTaxTokenItemDTO();
					
					vmTaxTokenItemDTO.vehicleId =  vm_vehicleDTO.iD;
					vmTaxTokenItemDTO.vmTaxTokenId = vm_tax_tokenDTO.iD;
					vmTaxTokenItemDTO.vmRegNo = vm_vehicleDTO.regNo;
					vmTaxTokenItemDTO.vehicleTypeCat = vm_vehicleDTO.vehicleTypeCat;
					vmTaxTokenItemDTO.taxTokenFees = vm_tax_tokenDTO.taxTokenFees;
					vmTaxTokenItemDTO.fitnessFees = vm_tax_tokenDTO.fitnessFees;
					vmTaxTokenItemDTO.vat = vm_tax_tokenDTO.vat;
					vmTaxTokenItemDTO.isDigital = false;
					vmTaxTokenItemDTO.digitalNumberFees = 0;
					vmTaxTokenItemDTO.digitalRegFees = 0;
					if(vm_vehicleDTO.digitalPlate){
						vmTaxTokenItemDTO.isDigital = true;
						vmTaxTokenItemDTO.digitalNumberFees = vm_tax_tokenDTO.digitalNumberFees;
						//vmTaxTokenItemDTO.digitalRegFees = vm_tax_tokenDTO.di;
					}

					double totalExcludingVat = vmTaxTokenItemDTO.taxTokenFees+vmTaxTokenItemDTO.fitnessFees+vmTaxTokenItemDTO.digitalNumberFees;

					vmTaxTokenItemDTO.totalVat =  (totalExcludingVat)*(vmTaxTokenItemDTO.vat/100);

					vmTaxTokenItemDTO.total = totalExcludingVat+vmTaxTokenItemDTO.totalVat;

					vmTaxTokenItemDTO.fiscalYearId = vm_tax_tokenDTO.fiscalYearId;
					System.out.println("Done adding  vmTaxTokenItem dto = " + vmTaxTokenItemDTO);

					if(addFlag == true)
					{
						returnedID = vmTaxTokenItemDAO.manageWriteOperations(vmTaxTokenItemDTO, SessionConstants.INSERT, -1, userDTO);
					}
					else
					{
						returnedID = vmTaxTokenItemDAO.manageWriteOperations(vmTaxTokenItemDTO, SessionConstants.UPDATE, -1, userDTO);
					}
				}
			}







//			List<VmTaxTokenItemDTO> vmTaxTokenItemDTOList = createVmTaxTokenItemDTOListByRequest(request);
//			if(addFlag == true || isPermanentTable == false) //add or validate
//			{
//				if(vmTaxTokenItemDTOList != null)
//				{
//					for(VmTaxTokenItemDTO vmTaxTokenItemDTO: vmTaxTokenItemDTOList)
//					{
//						vmTaxTokenItemDTO.vmTaxTokenId = vm_tax_tokenDTO.iD;
//						vmTaxTokenItemDAO.add(vmTaxTokenItemDTO);
//					}
//				}
//
//			}
//			else
//			{
//				List<Long> childIdsFromRequest = vmTaxTokenItemDAO.getChildIdsFromRequest(request, "vmTaxTokenItem");
//				//delete the removed children
//				vmTaxTokenItemDAO.deleteChildrenNotInList("vm_tax_token", "vm_tax_token_item", vm_tax_tokenDTO.iD, childIdsFromRequest);
//				List<Long> childIDsInDatabase = vmTaxTokenItemDAO.getChilIds("vm_tax_token", "vm_tax_token_item", vm_tax_tokenDTO.iD);
//
//
//				if(childIdsFromRequest != null)
//				{
//					for(int i = 0; i < childIdsFromRequest.size(); i ++)
//					{
//						Long childIDFromRequest = childIdsFromRequest.get(i);
//						if(childIDsInDatabase.contains(childIDFromRequest))
//						{
//							VmTaxTokenItemDTO vmTaxTokenItemDTO =  createVmTaxTokenItemDTOByRequestAndIndex(request, false, i);
//							vmTaxTokenItemDTO.vmTaxTokenId = vm_tax_tokenDTO.iD;
//							vmTaxTokenItemDAO.update(vmTaxTokenItemDTO);
//						}
//						else
//						{
//							VmTaxTokenItemDTO vmTaxTokenItemDTO =  createVmTaxTokenItemDTOByRequestAndIndex(request, true, i);
//							vmTaxTokenItemDTO.vmTaxTokenId = vm_tax_tokenDTO.iD;
//							vmTaxTokenItemDAO.add(vmTaxTokenItemDTO);
//						}
//					}
//				}
//				else
//				{
//					vmTaxTokenItemDAO.deleteChildrenByParent(vm_tax_tokenDTO.iD, "vm_tax_token_id");
//				}
//
//			}





			if(isPermanentTable)
			{
				String inPlaceSubmit = request.getParameter("inplacesubmit");

				if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
				{
					getVm_tax_token(request, response, returnedID);
				}
				else
				{
					//response.sendRedirect("Vm_tax_tokenServlet?actionType=search");
					//request.setAttribute("fiscalYearID",fiscalValue);
					//response.sendRedirect("Vm_tax_tokenServlet?actionType=AllVehicleSearch&fiscalYearID="+fiscalValue);
					ApiResponse apiResponse=null;
					apiResponse = ApiResponse.makeSuccessResponse("Vm_tax_tokenServlet?actionType=AllVehicleSearch&fiscalYearID="+fiscalValue);

					PrintWriter pw = response.getWriter();
					pw.write(apiResponse.getJSONString());
					pw.flush();
					pw.close();
				}
			}
			else
			{
				commonRequestHandler.validate(vm_tax_tokenDAO.getDTOByID(returnedID), request, response, userDTO);
			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}





	private List<VmTaxTokenItemDTO> createVmTaxTokenItemDTOListByRequest(HttpServletRequest request) throws Exception{
		List<VmTaxTokenItemDTO> vmTaxTokenItemDTOList = new ArrayList<VmTaxTokenItemDTO>();
		if(request.getParameterValues("vmTaxTokenItem.iD") != null)
		{
			int vmTaxTokenItemItemNo = request.getParameterValues("vmTaxTokenItem.iD").length;


			for(int index=0;index<vmTaxTokenItemItemNo;index++){
				VmTaxTokenItemDTO vmTaxTokenItemDTO = createVmTaxTokenItemDTOByRequestAndIndex(request,true,index);
				vmTaxTokenItemDTOList.add(vmTaxTokenItemDTO);
			}

			return vmTaxTokenItemDTOList;
		}
		return null;
	}


	private VmTaxTokenItemDTO createVmTaxTokenItemDTOByRequestAndIndex(HttpServletRequest request, boolean addFlag,int index) throws Exception{

		VmTaxTokenItemDTO vmTaxTokenItemDTO;
		if(addFlag == true )
		{
			vmTaxTokenItemDTO = new VmTaxTokenItemDTO();
		}
		else
		{
			vmTaxTokenItemDTO = vmTaxTokenItemDAO.getDTOByID(Long.parseLong(request.getParameterValues("vmTaxTokenItem.iD")[index]));
		}
		String path = getServletContext().getRealPath("/img2/");
		SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");



		String Value = "";
		Value = request.getParameterValues("vmTaxTokenItem.vehicleId")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		vmTaxTokenItemDTO.vehicleId = Long.parseLong(Value);
		Value = request.getParameterValues("vmTaxTokenItem.vmTaxTokenId")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		vmTaxTokenItemDTO.vmTaxTokenId = Long.parseLong(Value);
		Value = request.getParameterValues("vmTaxTokenItem.isDigital")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		vmTaxTokenItemDTO.isDigital = Boolean.parseBoolean(Value);
		Value = request.getParameterValues("vmTaxTokenItem.totalVat")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		vmTaxTokenItemDTO.totalVat = Double.parseDouble(Value);
		Value = request.getParameterValues("vmTaxTokenItem.total")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		vmTaxTokenItemDTO.total = Double.parseDouble(Value);
		Value = request.getParameterValues("vmTaxTokenItem.searchColumn")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		vmTaxTokenItemDTO.searchColumn = (Value);
		Value = request.getParameterValues("vmTaxTokenItem.insertedBy")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		vmTaxTokenItemDTO.insertedBy = (Value);
		Value = request.getParameterValues("vmTaxTokenItem.modifiedBy")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		vmTaxTokenItemDTO.modifiedBy = (Value);
		Value = request.getParameterValues("vmTaxTokenItem.insertionDate")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		vmTaxTokenItemDTO.insertionDate = Long.parseLong(Value);
		return vmTaxTokenItemDTO;

	}




	private void getVm_tax_token(HttpServletRequest request, HttpServletResponse response, long id) throws ServletException, IOException
	{
		System.out.println("in getVm_tax_token");
		Vm_tax_tokenDTO vm_tax_tokenDTO = null;
		try
		{
			vm_tax_tokenDTO = vm_tax_tokenDAO.getDTOByID(id);
			request.setAttribute("ID", vm_tax_tokenDTO.iD);
			request.setAttribute("vm_tax_tokenDTO",vm_tax_tokenDTO);
			request.setAttribute("vm_tax_tokenDAO",vm_tax_tokenDAO);

			String URL= "";

			String inPlaceEdit = request.getParameter("inplaceedit");
			String inPlaceSubmit = request.getParameter("inplacesubmit");
			String getBodyOnly = request.getParameter("getBodyOnly");

			if(inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase(""))
			{
				URL = "vm_tax_token/vm_tax_tokenInPlaceEdit.jsp";
				request.setAttribute("inplaceedit","");
			}
			else if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
			{
				URL = "vm_tax_token/vm_tax_tokenSearchRow.jsp";
				request.setAttribute("inplacesubmit","");
			}
			else
			{
				if(getBodyOnly  != null && !getBodyOnly.equalsIgnoreCase(""))
				{
					URL = "vm_tax_token/vm_tax_tokenEditBody.jsp?actionType=edit";
				}
				else
				{
					URL = "vm_tax_token/vm_tax_tokenEdit.jsp?actionType=edit";
				}
			}

			RequestDispatcher rd = request.getRequestDispatcher(URL);
			rd.forward(request, response);
		}
		catch (NumberFormatException e)
		{
			e.printStackTrace();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	private void getNewVm_tax_token(HttpServletRequest request, HttpServletResponse response, long fiscalYearID) throws ServletException, IOException
	{
		System.out.println("in getNewVm_tax_token");
		Vm_tax_tokenDTO vm_tax_tokenDTO = null;
		try
		{
			//Long fiscalYearID = Long.valueOf(request.getParameter("fiscalYearID"));
			System.out.println("fiscalYearID in edit: "+fiscalYearID);
			//List<Vm_tax_tokenDTO> vm_tax_tokenDTOS = vm_tax_tokenDAO.getDTOsByFiscalYearID(fiscalYearID);
			List<Vm_tax_tokenDTO> vm_tax_tokenDTOS = Vm_tax_tokenRepository.getInstance().getVm_tax_tokenDTOByFiscal_year_id(fiscalYearID);
			//request.setAttribute("ID", vm_tax_tokenDTO.iD);
			request.setAttribute("vm_tax_tokenDTOS",vm_tax_tokenDTOS);
			request.setAttribute("fiscalYearID",fiscalYearID);
			request.setAttribute("vm_tax_tokenDAO",vm_tax_tokenDAO);

			String URL= "";

			String inPlaceEdit = request.getParameter("inplaceedit");
			String inPlaceSubmit = request.getParameter("inplacesubmit");
			String getBodyOnly = request.getParameter("getBodyOnly");

			if(inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase(""))
			{
				URL = "vm_tax_token/vm_tax_tokenInPlaceEdit.jsp";
				request.setAttribute("inplaceedit","");
			}
			else if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
			{
				URL = "vm_tax_token/vm_tax_tokenSearchRow.jsp";
				request.setAttribute("inplacesubmit","");
			}
			else
			{
				if(getBodyOnly  != null && !getBodyOnly.equalsIgnoreCase(""))
				{
					URL = "vm_tax_token/vm_tax_tokenNewEditBody.jsp?actionType=edit";
				}
				else
				{
					URL = "vm_tax_token/vm_tax_tokenNewEdit.jsp?actionType=edit";
				}
			}

			RequestDispatcher rd = request.getRequestDispatcher(URL);
			rd.forward(request, response);
		}
		catch (NumberFormatException e)
		{
			e.printStackTrace();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	//


	private void getVm_tax_token(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		getVm_tax_token(request, response, Long.parseLong(request.getParameter("ID")));
	}
	private void getNewVm_tax_token(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		getNewVm_tax_token(request, response, Long.parseLong(request.getParameter("fiscalYearID")));
	}




	private void searchVm_tax_token(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException
	{
		System.out.println("in  searchVm_tax_token 1");
		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String ajax = request.getParameter("ajax");
		boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);

		//filter = " GROUP BY fiscal_year_id  ";

        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
			SessionConstants.NAV_VM_TAX_TOKEN,
			request,
			vm_tax_tokenDAO,
			SessionConstants.VIEW_VM_TAX_TOKEN,
			SessionConstants.SEARCH_VM_TAX_TOKEN,
			tableName,
			isPermanent,
			userDTO,
			filter,
			true);
        try
        {
			System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        }
        catch(Exception e)
        {
			System.out.println("failed to dojob" + e);
        }

		request.setAttribute("vm_tax_tokenDAO",vm_tax_tokenDAO);
        RequestDispatcher rd;
        if(!isPermanent)
        {
        	if(hasAjax == false)
	        {
	        	System.out.println("Going to vm_tax_token/vm_tax_tokenApproval.jsp");
	        	rd = request.getRequestDispatcher("vm_tax_token/vm_tax_tokenApproval.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to vm_tax_token/vm_tax_tokenApprovalForm.jsp");
	        	rd = request.getRequestDispatcher("vm_tax_token/vm_tax_tokenApprovalForm.jsp");
	        }
        }
        else
        {
	        if(hasAjax == false)
	        {
	        	System.out.println("Going to vm_tax_token/vm_tax_tokenSearch.jsp");
	        	rd = request.getRequestDispatcher("vm_tax_token/vm_tax_tokenSearch.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to vm_tax_token/vm_tax_tokenSearchForm.jsp");
	        	rd = request.getRequestDispatcher("vm_tax_token/vm_tax_tokenSearchForm.jsp");
	        }
        }
		rd.forward(request, response);
	}

	private void allVehicleSearchVm_tax_token(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException
	{
		System.out.println("in  allVehicleSearchVm_tax_token 1");
		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String ajax = request.getParameter("ajax");
		boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);

//		filter = " fiscalYearID =  "+request.getParameter("fiscalYearID");
//
//		RecordNavigationManager4 rnManager = new RecordNavigationManager4(
//				SessionConstants.NAV_VM_TAX_TOKEN,
//				request,
//				vmTaxTokenItemDAO,
//				SessionConstants.VIEW_VM_TAX_TOKEN,
//				SessionConstants.SEARCH_VM_TAX_TOKEN,
//				vmTaxTokenItemTableName,
//				isPermanent,
//				userDTO,
//				filter,
//				true);
//		try
//		{
//			System.out.println("trying to dojob");
//			rnManager.doJob(loginDTO);
//		}
//		catch(Exception e)
//		{
//			System.out.println("failed to dojob" + e);
//		}
//
//		request.setAttribute("vmTaxTokenItemDAO",vmTaxTokenItemDAO);
		RequestDispatcher rd;
		if(!isPermanent)
		{
			if(hasAjax == false)
			{
				System.out.println("Going to vm_tax_token/vm_tax_tokenApproval.jsp");
				rd = request.getRequestDispatcher("vm_tax_token/vm_tax_tokenApproval.jsp");
			}
			else
			{
				System.out.println("Going to vm_tax_token/vm_tax_tokenApprovalForm.jsp");
				rd = request.getRequestDispatcher("vm_tax_token/vm_tax_tokenApprovalForm.jsp");
			}
		}
		else
		{
			if(hasAjax == false)
			{
				System.out.println("Going to vm_tax_token/vm_tax_tokenAllVehicleSearch.jsp");
				rd = request.getRequestDispatcher("vm_tax_token/vm_tax_tokenAllVehicleSearch.jsp");
			}
			else
			{
				System.out.println("Going to vm_tax_token/vm_tax_tokenAllVehicleSearchForm.jsp");
				rd = request.getRequestDispatcher("vm_tax_token/vm_tax_tokenAllVehicleSearchForm.jsp");
			}
		}
		rd.forward(request, response);
	}

	private void checkFiscalYearValidation(HttpServletRequest request, HttpServletResponse response)
	{
		try
		{
			System.out.println("In checkFiscalYearValidation");
			ValidationDTO validationDTO = new ValidationDTO();
			String fiscalYearId =  request.getParameter("fiscalYearId");
			String language =  request.getParameter("fiscalYearId");

			//int existence = vm_tax_tokenDAO.checkDuplicateFiscalYear(Long.parseLong(fiscalYearId));

			int existence = Vm_tax_tokenRepository.getInstance().getVm_tax_tokenDTOByFiscal_year_id(Long.parseLong(fiscalYearId)).size()>0?1:0;
			String errMsg = "";
			if(language.equalsIgnoreCase("english")){

				errMsg += " Duplicate Fiscal Year ";

			} else {

				errMsg += " ডুপ্লিকেট অর্থবছর ";

			}

			if(existence==0){
				validationDTO.valid = true ;
				//validationDTO.errMsg = errMsg;



			}else if(existence==1) {
				validationDTO.valid = false ;
				validationDTO.errMsg = errMsg;
			}

			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");

			String encoded = this.gson.toJson(validationDTO);
			System.out.println("json encoded data = " + encoded);
			out.print(encoded);
			out.flush();
		}
		catch (NumberFormatException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	private void deleteSelectedRow(HttpServletRequest request, HttpServletResponse response)
	{
		try
		{
			System.out.println("In deletedSelectedRow");
			ValidationDTO validationDTO = new ValidationDTO();
			//String selectedID =  request.getParameter("ID");
			String language =  request.getParameter("language");

			LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
			UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);

			long returnedID = 1;

			//VmTaxTokenItemDTO dto = vmTaxTokenItemDAO.getDTOByID(Long.parseLong(selectedID));
			//dto.isDeleted = 1;


			//vmTaxTokenItemDAO.manageWriteOperations(dto, SessionConstants.DELETE, Long.parseLong(selectedID), userDTO);
			commonRequestHandler2.ajaxDelete(request, response, userDTO);
			//System.out.println("dto delete: "+dto+" returnedID: "+returnedID);



			if(returnedID>0){
				validationDTO.valid = true ;
				validationDTO.valid = true ;
				validationDTO.errMsg = language.equalsIgnoreCase("English")?"Deleted":"মোছা হয়েছে";

			}else  {
				validationDTO.valid = false ;
				validationDTO.errMsg = language.equalsIgnoreCase("English")?"Cannot Deleted":"মোছা সম্ভব হয়নি";

			}

			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");

			String encoded = this.gson.toJson(validationDTO);
			System.out.println("json encoded data = " + encoded);
			out.print(encoded);
			out.flush();
		}
		catch (NumberFormatException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void allVehicleDataSubmit(HttpServletRequest request, HttpServletResponse response) throws IOException
	{
		String[] paramValues = request.getParameterValues("iD");
		for (int i = 0; i < paramValues.length; i++)
		{
			String paramValue = paramValues[i];
			VmTaxTokenItemDTO vmTaxTokenItemDTO;


			try
			{


				vmTaxTokenItemDTO = vmTaxTokenItemDAO.getDTOByID(Long.parseLong(request.getParameter("iD")));
				


			}
			catch (Exception e)
			{
				e.printStackTrace();
			}

		}
		response.sendRedirect("Recruitment_bulk_mark_entryServlet?actionType=search");
	}

}

