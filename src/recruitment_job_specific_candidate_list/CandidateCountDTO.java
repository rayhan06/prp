package recruitment_job_specific_candidate_list;

import util.CommonDTO;


public class CandidateCountDTO
{

    public int totalCount = 0;
    public int selectedCount = 0;
    public int rejectedCount = 0;
    public int viewCount = 0;
    public int numOfAdmitDownloaded = 0;

	
	
    @Override
	public String toString() {
            return "CandidateCountDTO[" +
            " totalCount = " + totalCount +
            " selectedCount = " + selectedCount +
            " rejectedCount = " + rejectedCount +
            " viewCount = " + viewCount +
            "]";
    }

}