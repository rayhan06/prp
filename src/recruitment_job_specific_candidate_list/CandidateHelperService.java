package recruitment_job_specific_candidate_list;

import javafx.util.Pair;
import job_applicant_application.Job_applicant_applicationDAO;
import job_applicant_application.Job_applicant_applicationDTO;
import recruitment_job_description.Recruitment_job_descriptionDAO;
import recruitment_job_description.Recruitment_job_descriptionDTO;
import recruitment_job_description.Recruitment_job_descriptionRepository;
import recruitment_job_specific_exam_type.JobSpecificExamTypeDao;

import java.util.ArrayList;
import java.util.List;

public class CandidateHelperService {

    public List<CandidateJobWithDecisionDTO> getJobWithLevelInfo(long applicantId){
        List<CandidateJobWithDecisionDTO> dtos = new ArrayList<>();
//        Recruitment_job_descriptionDAO recruitment_job_descriptionDAO = new Recruitment_job_descriptionDAO();
        JobSpecificExamTypeDao jobSpecificExamTypeDao = new JobSpecificExamTypeDao();
        Job_applicant_applicationDAO job_applicant_applicationDAO = new Job_applicant_applicationDAO();
        List<Job_applicant_applicationDTO> jobApplicantApplicationDTOS =
                job_applicant_applicationDAO.getDTOsWithMaxLevelByApplicantId(applicantId);

        for(Job_applicant_applicationDTO jobApplicantApplicationDTO: jobApplicantApplicationDTOS){
            CandidateJobWithDecisionDTO decisionDTO = new CandidateJobWithDecisionDTO();

            Recruitment_job_descriptionDTO jobDescriptionDTO = Recruitment_job_descriptionRepository.
                    getInstance().getRecruitment_job_descriptionDTOByID(jobApplicantApplicationDTO.jobId);
//                    recruitment_job_descriptionDAO.getDTOByID();
            if(jobDescriptionDTO != null){
                decisionDTO.jobName = jobDescriptionDTO.jobTitleEn;
                decisionDTO.jobNameBn = jobDescriptionDTO.jobTitleBn;
                decisionDTO.jobId = jobDescriptionDTO.iD;
            }

            Pair<String, String> levelValue = jobSpecificExamTypeDao.getNameByJobIDAndLevel(jobApplicantApplicationDTO.jobId, jobApplicantApplicationDTO.level);
            if(levelValue != null){
                decisionDTO.levelName = levelValue.getKey();
                decisionDTO.levelNameBn = levelValue.getValue();
            }

            Pair<String, String> decisionValue =
                    Job_applicant_applicationDAO.getDecision(jobApplicantApplicationDTO.isSelected, jobApplicantApplicationDTO.isRejected);

            if(decisionValue != null){
                decisionDTO.decisionName = decisionValue.getKey();
                decisionDTO.decisionNameBn = decisionValue.getValue();
            }

            decisionDTO.applyDate = jobApplicantApplicationDTO.insertionDate;



            dtos.add(decisionDTO);
        }

        return dtos;
    }

}
