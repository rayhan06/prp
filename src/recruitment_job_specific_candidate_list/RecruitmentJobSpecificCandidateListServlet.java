package recruitment_job_specific_candidate_list;

import com.google.gson.Gson;
import config.GlobalConfigConstants;
import config.GlobalConfigurationRepository;
import employee_pay_scale.Employee_pay_scaleDTO;
import employee_pay_scale.Employee_pay_scaleRepository;
import javafx.util.Pair;
import job_applicant_application.*;
import language.LC;
import language.LM;
import login.LoginDTO;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import parliament_job_applicant.Parliament_job_applicantDAO;
import pb.CatDTO;
import pb.CatRepository;
import pb.Utils;
import permission.MenuConstants;
import recruitment_job_description.Recruitment_job_descriptionDAO;
import recruitment_job_description.Recruitment_job_descriptionDTO;
import recruitment_job_description.Recruitment_job_descriptionRepository;
import recruitment_job_specific_exam_type.JobSpecificExamTypeRepository;
import recruitment_job_specific_exam_type.RecruitmentJobSpecificExamTypeDTO;
import role.PermissionRepository;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.CommonRequestHandler;
import util.RecordNavigationManager4;
import util.UtilCharacter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;

@WebServlet("/RecruitmentJobSpecificCandidateListServlet")
@MultipartConfig
public class RecruitmentJobSpecificCandidateListServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;
    Job_applicant_applicationDAO jobApplicantApplicationDAO;
    Recruitment_job_descriptionDAO recruitmentJobDescriptionDAO;
    String jobApplicantApplicationDAOTableName = "job_applicant_application";
    String recruitmentJobDescriptionDAOTableName = "recruitment_job_description";
    public static Logger logger = Logger.getLogger(RecruitmentJobSpecificCandidateListServlet.class);
    CommonRequestHandler commonRequestHandler;
    private final Gson gson = new Gson();

    /**
     * @see HttpServlet#HttpServlet()
     */
    public RecruitmentJobSpecificCandidateListServlet() {
        super();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("In do get request = " + request);
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


        try {
            String actionType = request.getParameter("actionType");
            //System.out.println("doget actionType: "+actionType);
            if (actionType.equals("getCandidateList")) {

                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.RECRUITMENT_CANDIDATE_LIST)) {
                    getCandidateList(request, response);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            }

            else if (actionType.equals("getResultNotice")) {

                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.RECRUITMENT_CANDIDATE_LIST)) {
                    getResultNotice(request, response);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            }
            else if (actionType.equals("getCandidateListForInternship")) {

                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.RECRUITMENT_CANDIDATE_LIST)) {
                    getCandidateListForInternship(request, response);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else if (actionType.equals("getCandidateSearch")) {
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.RECRUITMENT_CANDIDATE_SEARCH)) {
                    getCandidateSearch(request, response);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else if (actionType.equals("getAllDataCount")) {
                long jobId = Long.parseLong(request.getParameter("jobId"));
                long level = Long.parseLong(request.getParameter("level"));
                Job_applicant_applicationDAO job_applicant_applicationDAO = new Job_applicant_applicationDAO();
                CandidateCountDTO candidateCountDTO = new CandidateCountDTO();
                candidateCountDTO.totalCount = job_applicant_applicationDAO.getAllCountByJobIdAndLevelId(jobId, level);
                candidateCountDTO.viewCount = job_applicant_applicationDAO.getViewAllCountByJobIdAndLevelId(jobId, level);
                candidateCountDTO.selectedCount = job_applicant_applicationDAO.getSelectAllCountByJobIdAndLevelId(jobId, level);
                candidateCountDTO.rejectedCount = job_applicant_applicationDAO.getRejectAllCountByJobIdAndLevelId(jobId, level);
                candidateCountDTO.numOfAdmitDownloaded = job_applicant_applicationDAO.getAllAdmitCardByJobIdAndLevelId(jobId, level);

                PrintWriter out = response.getWriter();
                response.setContentType("application/json");
                response.setCharacterEncoding("UTF-8");

                String encoded = this.gson.toJson(candidateCountDTO);
                System.out.println("json encoded data = " + encoded);
                out.print(encoded);
                out.flush();
            } else if (actionType.equals("getDataForCandidateSearch")) {
                String filter = "";
                String name = request.getParameter("name");
                System.out.println(name);
                if (!Utils.isValidSearchString(name)) {
                    name = "";
                }

                filter += " ( name_bn like '%" + name + "%' or name_en like '%" + name + "%' ) ";

                String phoneNo = request.getParameter("phoneNo");
                if (!Utils.isValidSearchString(phoneNo)) {
                    phoneNo = "";
                }
                if (phoneNo != null && phoneNo.length() > 0) {
                    filter += " and contact_number like '%" + phoneNo + "%' ";
                }
                String nid = request.getParameter("nid");
                if (!Utils.isValidSearchString(nid)) {
                    nid = "";
                }

                if (nid != null && nid.length() > 0) {
                    filter += " and nid like '" + nid + "' ";
                }


                searchParliament_job_applicant(request, response, true, filter);
            } else if (actionType.equals("getData")) {
                long jobId = Long.parseLong(request.getParameter("jobId"));
                Recruitment_job_descriptionDAO jobDescriptionDAO = new Recruitment_job_descriptionDAO();
                Recruitment_job_descriptionDTO jobDescriptionDTO = Recruitment_job_descriptionRepository.getInstance()
                        .getRecruitment_job_descriptionDTOByID(jobId);
//                        jobDescriptionDAO.getDTOByID(jobId);

                long internShipId = Long.parseLong(GlobalConfigurationRepository.
                        getGlobalConfigDTOByID(GlobalConfigConstants.INTERNSHIP_ID).value);

                request.setAttribute("jobDescriptionDTO", jobDescriptionDTO);
                String filter = "job_id =" + jobId;

                int quota = Integer.parseInt(request.getParameter("quota"));
                if (quota != 0) {
                    filter += " and quota_id =  " + quota + " ";
                }

                String view = request.getParameter("view");
                if (view != null && !view.equalsIgnoreCase("ALL")) {
                    if (view.equalsIgnoreCase("VIEWED")) {
                        filter += " and is_viewed = 1 ";
                    } else {
                        filter += " and is_viewed = 0 ";
                    }
                }

                String approve = request.getParameter("approve");
                if (approve != null && !approve.equalsIgnoreCase("ALL")) {
                    if (approve.equalsIgnoreCase("REJECTED")) {
                        filter += " and is_rejected = 1 ";
                    } else if (approve.equalsIgnoreCase("RUNNING")) {
                        filter += " and ( is_rejected = 0 and is_selected = 0 ) ";
                    } else {
                        filter += " and (is_selected = 1 or is_selected = 2)  ";
                    }
                }

                long level = Long.parseLong(request.getParameter("level"));
                filter += " and level =  " + level;


                String ageFrom = request.getParameter("ageFrom");
                String ageTo = request.getParameter("ageTo");

                if (ageFrom != null && ageTo != null && !ageFrom.equalsIgnoreCase("")
                        && !ageTo.equalsIgnoreCase("") && jobId != internShipId) {
                    long aFrom = Long.parseLong(ageFrom);
                    long aTo = Long.parseLong(ageTo);

                    filter += " and age_in_years >=  " + aFrom + " and age_in_years <=  " + aTo;
                }

                String exFrom = request.getParameter("exFrom");
                String exTo = request.getParameter("exTo");

                if (exFrom != null && exTo != null && !exFrom.equalsIgnoreCase("")
                        && !exTo.equalsIgnoreCase("")) {
                    long aFrom = Long.parseLong(exFrom);
                    long aTo = Long.parseLong(exTo);

                    filter += " and qualification_in_years >=  " +
                            aFrom + " and qualification_in_years <=  " + aTo;
                }


                String eduLevel = request.getParameter("eduLevel");

                if (eduLevel != null && !eduLevel.equalsIgnoreCase("")) {
                    long aEduLevel = Long.parseLong(eduLevel);

                    filter += " and highest_edu_level >=  " + aEduLevel;
                }


                String university = request.getParameter("university");

                if (university != null && !university.equalsIgnoreCase("")) {
                    long universityIdCheck = Long.parseLong(university);
                    university = "%," + university + ",%";

                    filter += " and university_ids like '" + university + "'";
                }


                double passMark = 0.0;

                List<RecruitmentJobSpecificExamTypeDTO> recruitmentJobSpecificExamTypeDTOS =
                        JobSpecificExamTypeRepository.getInstance().getRecruitmentJobSpecificExamTypeDTOByjob_id(jobId)
                                .stream().filter(i -> i.isSelected && i.order == level).collect(Collectors.toList());

//                        new JobSpecificExamTypeDao().
//                        getDTOsByJobIdAndLevel(jobId, level).stream().filter(i -> i.isSelected ).collect(Collectors.toList());
                if (recruitmentJobSpecificExamTypeDTOS.size() > 0) {
                    passMark = recruitmentJobSpecificExamTypeDTOS.get(0).passMarks;
                }

                request.setAttribute("passMark", passMark);

                List<RecruitmentJobSpecificExamTypeDTO> allRecruitmentJobSpecificExamTypeDTOS =
                        JobSpecificExamTypeRepository.getInstance().getRecruitmentJobSpecificExamTypeDTOByjob_id(jobId)
                                .stream().filter(i -> i.isSelected).collect(Collectors.toList());
//                        new JobSpecificExamTypeDao().getDTOsByJobId(jobId).stream().filter(i -> i.isSelected).collect(Collectors.toList());

                Map<Integer, String> levelMap = new HashMap<>();
                int languageID = LM.getLanguageIDByUserDTO(userDTO);
                String Language = languageID == 1 ? "english" : "bangla";

                for (RecruitmentJobSpecificExamTypeDTO examTypeDTO : allRecruitmentJobSpecificExamTypeDTOS) {
                    int catId = examTypeDTO.jobExamTypeCat;
                    CatDTO catDTO = CatRepository.getDTO("job_exam_type", catId);
                    if (catDTO != null) {
                        levelMap.put(examTypeDTO.order,
                                UtilCharacter.getDataByLanguage(Language, catDTO.languageTextBangla, catDTO.languageTextEnglish));
                    }
                }

                request.setAttribute("levelMap", levelMap);

                searchJob_applicant_application(request, response, true, filter);
            } else {
                request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.debug(ex);
        }
    }


    private void searchParliament_job_applicant(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException {
        System.out.println("in  searchParliament_job_applicant 1");
        LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        String ajax = request.getParameter("ajax");
        boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);

        Parliament_job_applicantDAO parliament_job_applicantDAO = new Parliament_job_applicantDAO();

        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
                SessionConstants.NAV_PARLIAMENT_JOB_APPLICANT,
                request,
                parliament_job_applicantDAO,
                SessionConstants.VIEW_PARLIAMENT_JOB_APPLICANT,
                SessionConstants.SEARCH_PARLIAMENT_JOB_APPLICANT,
                "parliament_job_applicant",
                isPermanent,
                userDTO,
                filter,
                true);
        try {
            System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        } catch (Exception e) {
            System.out.println("failed to dojob" + e);
        }

        request.setAttribute("parliament_job_applicantDAO", parliament_job_applicantDAO);
        RequestDispatcher rd;

        System.out.println("Going to recruitment_candidate_list/parliament_job_applicantSearchForm.jsp");
        rd = request.getRequestDispatcher("recruitment_candidate_list/parliament_job_applicantSearchForm.jsp");

        rd.forward(request, response);
    }


    private void searchJob_applicant_application(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException {
        System.out.println("in  searchJob_applicant_application 1");
        LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        String ajax = request.getParameter("ajax");
        boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);

        Job_applicant_applicationDAO job_applicant_applicationDAO = new Job_applicant_applicationDAO();

        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
                SessionConstants.NAV_JOB_APPLICANT_APPLICATION,
                request,
                job_applicant_applicationDAO,
                SessionConstants.VIEW_JOB_APPLICANT_APPLICATION,
                SessionConstants.SEARCH_JOB_APPLICANT_APPLICATION,
                "job_applicant_application",
                isPermanent,
                userDTO,
                filter,
                true);
        try {
            System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        } catch (Exception e) {
            System.out.println("failed to dojob" + e);
        }

        request.setAttribute("job_applicant_applicationDAO", job_applicant_applicationDAO);
        RequestDispatcher rd;


        System.out.println("Going to recruitment_job_specific_candidate_list/job_applicant_applicationSearchForm.jsp");
        rd = request.getRequestDispatcher("recruitment_job_specific_candidate_list/job_applicant_applicationSearchForm.jsp");
        rd.forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


        try {
            String actionType = request.getParameter("actionType");

            if (actionType.equals("DownloadTemplate")) {

                jobApplicantApplicationDAO = new Job_applicant_applicationDAO(jobApplicantApplicationDAOTableName);
                recruitmentJobDescriptionDAO = new Recruitment_job_descriptionDAO(recruitmentJobDescriptionDAOTableName);
                boolean internFlag = true;


                long jobId = Long.parseLong(request.getParameter("job"));
                String approve = request.getParameter("approve");
                long level = Long.parseLong(request.getParameter("level"));


                double passMark = 0.0;
                List<RecruitmentJobSpecificExamTypeDTO> recruitmentJobSpecificExamTypeDTOS =
                        JobSpecificExamTypeRepository.getInstance().getRecruitmentJobSpecificExamTypeDTOByjob_id(jobId)
                                .stream().filter(i -> i.isSelected && i.order == level).collect(Collectors.toList());
//                        new JobSpecificExamTypeDao().
//                        getDTOsByJobIdAndLevel(jobId, level).stream().filter(i -> i.isSelected).collect(Collectors.toList());
                if (recruitmentJobSpecificExamTypeDTOS.size() > 0) {
                    passMark = recruitmentJobSpecificExamTypeDTOS.get(0).passMarks;
                }

                List<RecruitmentJobSpecificExamTypeDTO> allRecruitmentJobSpecificExamTypeDTOS =
                        JobSpecificExamTypeRepository.getInstance().getRecruitmentJobSpecificExamTypeDTOByjob_id(jobId)
                                .stream().filter(i -> i.isSelected).collect(Collectors.toList());
//                        new JobSpecificExamTypeDao().getDTOsByJobId(jobId).stream().filter(i -> i.isSelected).collect(Collectors.toList());

                Map<Integer, String> levelMap = new HashMap<>();
                int languageID = LM.getLanguageIDByUserDTO(userDTO);
                String Language = languageID == 1 ? "english" : "bangla";

                for (RecruitmentJobSpecificExamTypeDTO examTypeDTO : allRecruitmentJobSpecificExamTypeDTOS) {
                    int catId = examTypeDTO.jobExamTypeCat;
                    CatDTO catDTO = CatRepository.getDTO("job_exam_type", catId);
                    if (catDTO != null) {
                        levelMap.put(examTypeDTO.order,
                                UtilCharacter.getDataByLanguage(Language, catDTO.languageTextBangla, catDTO.languageTextEnglish));
                    }
                }


                Recruitment_job_descriptionDTO recruitment_job_descriptionDTO = Recruitment_job_descriptionRepository.getInstance().
                        getRecruitment_job_descriptionDTOByID(jobId);


                long internShipId = Long.parseLong(GlobalConfigurationRepository.
                        getGlobalConfigDTOByID(GlobalConfigConstants.INTERNSHIP_ID).value);
                internFlag = internShipId != jobId;

                //System.out.println("FILTER: "+filter);
                List<Job_applicant_applicationDTO> jobApplicantApplicationDTOS = filterVal(jobId, level, approve, false, false, request);
                List<Job_applicant_applicationDTO> acceptable = filterVal(jobId, level, approve, true, false, request);
                List<Job_applicant_applicationDTO> rejected = filterVal(jobId, level, approve, false, true, request);

                Employee_pay_scaleDTO employee_pay_scaleDTO = Employee_pay_scaleRepository.getInstance().getEmployee_pay_scaleDTOByID(recruitment_job_descriptionDTO.employeePayScaleType);
                String payScale = "";
                if (employee_pay_scaleDTO != null) {
                    payScale = CatRepository.getName(Language, "national_pay_scale_type", employee_pay_scaleDTO.nationalPayScaleCat);
                }


                Random r = new Random();

                //List<Job_applicant_applicationDTO> recruitment_bulk_attendanceDTO_job_level = job_applicant_applicationDAO.getIDDTOsByJobLevelID(Integer.parseInt(job_id),Integer.parseInt(level_id));

                String file_name = "recruitment_candidate_bulk_info_" + request.getParameter("job_name") + "_" + level + ".xlsx";
                response.setContentType("application/vnd.ms-excel");
                response.setHeader("Content-Disposition", "attachment; filename=" + file_name);

                XSSFWorkbook workbook = UtilCharacter.newWorkBook();
                int row_index = 3;
                int cell_col_index = 0;
                int reqColIter = 0;
                boolean addMerged = true;
                Sheet sheet = UtilCharacter.newSheet(workbook, LM.getText(LC.CANDIDATE_LIST_CANDIDATE_LIST, loginDTO));
                UtilCharacter.newRow(workbook, sheet, "* " + LM.getText(LC.CANDIDATE_LIST_POSITION_NAME, loginDTO) + " " + UtilCharacter.getDataByLanguage(Language, recruitment_job_descriptionDTO.jobTitleBn,
                        recruitment_job_descriptionDTO.jobTitleEn), row_index, row_index++, 0, 10, addMerged);

                if (internFlag) {
                    UtilCharacter.newRow(workbook, sheet, "* " + LM.getText(LC.CANDIDATE_LIST_SALARY_GRADE, loginDTO) + " " + payScale + " (" + CatRepository.getName(Language, "job_grade", recruitment_job_descriptionDTO.jobGradeCat) + ")", row_index, row_index++, 0, 10, addMerged);
                    UtilCharacter.newRow(workbook, sheet, "* " + LM.getText(LC.CANDIDATE_LIST_NUMBER_ADVERTISED_POSTS, loginDTO) + " " + Utils.getDigits(recruitment_job_descriptionDTO.numberOfVacancy, Language), row_index, row_index++, 0, 10, addMerged);
                }

                UtilCharacter.newRow(workbook, sheet, "* " + LM.getText(LC.CANDIDATE_LIST_NUMBER_APPLICANTS_RECEIVED, loginDTO) + " " + Utils.getDigits(jobApplicantApplicationDTOS.size(), Language), row_index, row_index++, 0, 10, addMerged);

                if (internFlag) {
                    UtilCharacter.newRow(workbook, sheet, "* " + LM.getText(LC.CANDIDATE_LIST_ELIGIBILITY, loginDTO) + " ", row_index, row_index++, 0, 10, addMerged);
                }


                UtilCharacter.newRow(workbook, sheet, "* " + LM.getText(LC.CANDIDATE_LIST_APPLICABLE_APPLICATION, loginDTO) + " " + Utils.getDigits(acceptable.size(), Language), row_index, row_index++, 0, 10, addMerged);
                UtilCharacter.newRow(workbook, sheet, "* " + LM.getText(LC.CANDIDATE_LIST_CANCELLATION_APPLICATION, loginDTO) + " " + Utils.getDigits(rejected.size(), Language), row_index, row_index++, 0, 10, addMerged);

                addMerged = false;
                row_index++;
                Row newRow = sheet.createRow(row_index);
                IndexedColors indexedColor = IndexedColors.ROYAL_BLUE;
                boolean isBold = true;


                cell_col_index = UtilCharacter.alreadyCreatedRowForColumnSelect(workbook, sheet, LM.getText(LC.VISITOR_PASS_REQUEST_SERIAL_NUMBER, loginDTO), row_index, row_index, cell_col_index, cell_col_index++, addMerged, newRow, indexedColor, true, request, reqColIter++);
                cell_col_index = UtilCharacter.alreadyCreatedRowForColumnSelect(workbook, sheet, LM.getText(LC.APPOINTMENT_ADD_ID, loginDTO), row_index, row_index, cell_col_index, cell_col_index++, addMerged, newRow, indexedColor, true, request, reqColIter++);
                cell_col_index = UtilCharacter.alreadyCreatedRowForColumnSelect(workbook, sheet, LM.getText(LC.CANDIDATE_LIST_CANDIDATE_NAME, loginDTO), row_index, row_index, cell_col_index, cell_col_index++, addMerged, newRow, indexedColor, true, request, reqColIter++);

                /*Father mother name start*/
                cell_col_index = UtilCharacter.alreadyCreatedRowForColumnSelect(workbook, sheet, LM.getText(LC.PARLIAMENT_JOB_APPLICANT_ADD_FATHERNAME, loginDTO), row_index, row_index, cell_col_index, cell_col_index++, addMerged, newRow, indexedColor, true, request, reqColIter++);
                cell_col_index = UtilCharacter.alreadyCreatedRowForColumnSelect(workbook, sheet, LM.getText(LC.PARLIAMENT_JOB_APPLICANT_ADD_MOTHERNAME, loginDTO), row_index, row_index, cell_col_index, cell_col_index++, addMerged, newRow, indexedColor, true, request, reqColIter++);
                /*Father mother name end*/

                cell_col_index = UtilCharacter.alreadyCreatedRowForColumnSelect(workbook, sheet, LM.getText(LC.USER_DETAILS_ADD_PRESENTADDRESS, loginDTO), row_index, row_index, cell_col_index, cell_col_index++, addMerged, newRow, indexedColor, true, request, reqColIter++);
                cell_col_index = UtilCharacter.alreadyCreatedRowForColumnSelect(workbook, sheet, LM.getText(LC.USER_DETAILS_ADD_PERMANENTADDRESS, loginDTO), row_index, row_index, cell_col_index, cell_col_index++, addMerged, newRow, indexedColor, true, request, reqColIter++);
                cell_col_index = UtilCharacter.alreadyCreatedRowForColumnSelect(workbook, sheet, LM.getText(LC.PARLIAMENT_JOB_APPLICANT_ADD_HOMEDISTRICTNAME, loginDTO), row_index, row_index, cell_col_index, cell_col_index++, addMerged, newRow, indexedColor, true, request, reqColIter++);
                cell_col_index = UtilCharacter.alreadyCreatedRowForColumnSelect(workbook, sheet, LM.getText(LC.APPOINTMENT_EDIT_DATEOFBIRTH, loginDTO), row_index, row_index, cell_col_index, cell_col_index++, addMerged, newRow, indexedColor, true, request, reqColIter++);
                cell_col_index = UtilCharacter.alreadyCreatedRowForColumnSelect(workbook, sheet, LM.getText(LC.CANDIDATE_LIST_NATIONALITY_CERTIFICATE, loginDTO), row_index, row_index, cell_col_index, cell_col_index++, addMerged, newRow, indexedColor, true, request, reqColIter++);

                cell_col_index = UtilCharacter.alreadyCreatedRowForColumnSelect(workbook, sheet, LM.getText(LC.CANDIDATE_LIST_QUOTA, loginDTO), row_index, row_index, cell_col_index, cell_col_index++, addMerged, newRow, indexedColor, true, request, reqColIter++);
                cell_col_index = UtilCharacter.alreadyCreatedRowForColumnSelect(workbook, sheet, LM.getText(LC.CANDIDATE_LIST_ANNUAL, loginDTO), row_index, row_index, cell_col_index, cell_col_index++, addMerged, newRow, indexedColor, true, request, reqColIter++);

                cell_col_index = UtilCharacter.alreadyCreatedRowForColumnSelect(workbook, sheet, LM.getText(LC.CANDIDATE_LIST_AUTHORITY_CANDIDATE, loginDTO), row_index, row_index, cell_col_index, cell_col_index++, addMerged, newRow, indexedColor, true, request, reqColIter++);
                //cell_col_index = UtilCharacter.alreadyCreatedRowForColumnSelect(workbook, sheet, LM.getText(LC.CANDIDATE_LIST_EDUCATIONAL_QUALIFICATIONS, loginDTO), row_index, row_index, cell_col_index, cell_col_index++, addMerged, newRow, indexedColor, true, request, reqColIter++);

                String highestEduHeading = Language.equalsIgnoreCase("english") ? "Highest Educational Qualifications" : "সর্বোচ্চ শিক্ষাগত যোগ্যতা";

                cell_col_index = UtilCharacter.alreadyCreatedRowForColumnSelect(workbook, sheet, highestEduHeading, row_index, row_index, cell_col_index, cell_col_index++, addMerged, newRow, indexedColor, true, request, reqColIter++);
                cell_col_index = UtilCharacter.alreadyCreatedRowForColumnSelect(workbook, sheet, LM.getText(LC.CANDIDATE_LIST_TOTAL_JOB_EXPERIENCE, loginDTO), row_index, row_index, cell_col_index, cell_col_index++, addMerged, newRow, indexedColor, true, request, reqColIter++);

                String markHeading = Language.equalsIgnoreCase("english") ? "Marks" : "নম্বর";
                cell_col_index = UtilCharacter.alreadyCreatedRowForColumnSelect(workbook, sheet, markHeading, row_index, row_index, cell_col_index, cell_col_index++, addMerged, newRow, indexedColor, true, request, reqColIter++);
                cell_col_index = UtilCharacter.alreadyCreatedRowForColumnSelect(workbook, sheet, UtilCharacter.getDataByLanguage(Language,
                        "পূর্বের নম্বরসমূহ ", "Previous Marks"), row_index, row_index, cell_col_index, cell_col_index++, addMerged, newRow, indexedColor, true, request, reqColIter++);

                cell_col_index = UtilCharacter.alreadyCreatedRowForColumnSelect(workbook, sheet, LM.getText(LC.CANDIDATE_LIST_INVOICE, loginDTO), row_index, row_index, cell_col_index, cell_col_index++, addMerged, newRow, indexedColor, true, request, reqColIter++);
                cell_col_index = UtilCharacter.alreadyCreatedRowForColumnSelect(workbook, sheet, LM.getText(LC.CANDIDATE_LIST_ACCEPTED_REJECTED, loginDTO), row_index, row_index, cell_col_index, cell_col_index++, addMerged, newRow, indexedColor, true, request, reqColIter++);
                cell_col_index = UtilCharacter.alreadyCreatedRowForColumnSelect(workbook, sheet, LM.getText(LC.CANDIDATE_LIST_DECISION_REMARKS, loginDTO), row_index, row_index, cell_col_index, cell_col_index++, addMerged, newRow, indexedColor, true, request, reqColIter++);

                int count = 1;
                indexedColor = IndexedColors.BLACK;

                isBold = false;

                for (Job_applicant_applicationDTO dtos : jobApplicantApplicationDTOS) {
                    row_index++;
                    cell_col_index = 0;
                    reqColIter = 0;
                    newRow = sheet.createRow(row_index);

                    Pair<String, String> decisionValue =
                            Job_applicant_applicationDAO.getDecision(dtos.isSelected, dtos.isRejected);
                    String decision = "";

                    Pair<String, String> marksPair = Job_applicant_applicationDAO.getMarks(dtos.marks, passMark);
                    String marks = UtilCharacter.getDataByLanguage(Language, marksPair.
                            getValue(), marksPair.getKey());

                    if (decisionValue != null) {

                        if (Language.equalsIgnoreCase("english")) {
                            decision = decisionValue.getKey();

                        } else {
                            decision = decisionValue.getValue();
                        }
                    }

                    String prevLevelData = "";
                    PrevLevelDataSummaryDTO convertedObject = new Gson().fromJson(dtos.prev_level_data,
                            PrevLevelDataSummaryDTO.class);
                    if (convertedObject != null && convertedObject.data != null && convertedObject.data.size() > 0) {
                        System.out.println(convertedObject.data);
                        for (PrevLevelDataDTO prevLevelDataDTO : convertedObject.data) {
                            if (prevLevelDataDTO.level != 0 && prevLevelDataDTO.marks > -1.0) {
                                String pLevelData = "";
                                if (levelMap.get(prevLevelDataDTO.level) != null) {
                                    pLevelData = levelMap.get(prevLevelDataDTO.level);
                                }
                                prevLevelData += " " + pLevelData + " : " + Utils.getDigits(prevLevelDataDTO.marks, Language) + " .";
                            }
                        }
                    }

                    cell_col_index = UtilCharacter.alreadyCreatedRowForColumnSelect(workbook, sheet, Utils.getDigits(count++, Language), row_index, row_index, cell_col_index, cell_col_index++, addMerged, newRow, indexedColor, isBold, request, reqColIter++);
                    cell_col_index = UtilCharacter.alreadyCreatedRowForColumnSelect(workbook, sheet, dtos.rollNumber, row_index, row_index, cell_col_index, cell_col_index++, addMerged, newRow, indexedColor, isBold, request, reqColIter++);
                    cell_col_index = UtilCharacter.alreadyCreatedRowForColumnSelect(workbook, sheet, UtilCharacter.getDataByLanguage(Language, dtos.applicant_name_bn, dtos.applicant_name_en), row_index, row_index, cell_col_index, cell_col_index++, addMerged, newRow, indexedColor, isBold, request, reqColIter++);

                    cell_col_index = UtilCharacter.alreadyCreatedRowForColumnSelect(workbook, sheet, UtilCharacter.getDataByLanguage(Language, dtos.father_name, dtos.father_name), row_index, row_index, cell_col_index, cell_col_index++, addMerged, newRow, indexedColor, isBold, request, reqColIter++);
                    cell_col_index = UtilCharacter.alreadyCreatedRowForColumnSelect(workbook, sheet, UtilCharacter.getDataByLanguage(Language, dtos.mother_name, dtos.mother_name), row_index, row_index, cell_col_index, cell_col_index++, addMerged, newRow, indexedColor, isBold, request, reqColIter++);

                    cell_col_index = UtilCharacter.alreadyCreatedRowForColumnSelect(workbook, sheet, UtilCharacter.getDataByLanguage(Language, dtos.present_address_name_bn, dtos.present_address_name_en), row_index, row_index, cell_col_index, cell_col_index++, addMerged, newRow, indexedColor, isBold, request, reqColIter++);
                    cell_col_index = UtilCharacter.alreadyCreatedRowForColumnSelect(workbook, sheet, UtilCharacter.getDataByLanguage(Language, dtos.permanent_address_name_bn, dtos.permanent_address_name_en), row_index, row_index, cell_col_index, cell_col_index++, addMerged, newRow, indexedColor, isBold, request, reqColIter++);
                    cell_col_index = UtilCharacter.alreadyCreatedRowForColumnSelect(workbook, sheet, UtilCharacter.getDataByLanguage(Language, dtos.district_name_bn, dtos.district_name_en), row_index, row_index, cell_col_index, cell_col_index++, addMerged, newRow, indexedColor, isBold, request, reqColIter++);
                    cell_col_index = UtilCharacter.alreadyCreatedRowForColumnSelect(workbook, sheet, UtilCharacter.getDataByLanguage(Language, dtos.applicant_dob_bn, dtos.applicant_dob_en), row_index, row_index, cell_col_index, cell_col_index++, addMerged, newRow, indexedColor, isBold, request, reqColIter++);
                    cell_col_index = UtilCharacter.alreadyCreatedRowForColumnSelect(workbook, sheet, UtilCharacter.getDataByLanguage(Language, dtos.applicant_nid, dtos.applicant_nid), row_index, row_index, cell_col_index, cell_col_index++, addMerged, newRow, indexedColor, isBold, request, reqColIter++);
                    cell_col_index = UtilCharacter.alreadyCreatedRowForColumnSelect(workbook, sheet, UtilCharacter.getDataByLanguage(Language, dtos.quota_bn, dtos.quota_en), row_index, row_index, cell_col_index, cell_col_index++, addMerged, newRow, indexedColor, isBold, request, reqColIter++);
                    cell_col_index = UtilCharacter.alreadyCreatedRowForColumnSelect(workbook, sheet, UtilCharacter.getDataByLanguage(Language, dtos.annual_bn, dtos.annual_en), row_index, row_index, cell_col_index, cell_col_index++, addMerged, newRow, indexedColor, isBold, request, reqColIter++);
                    cell_col_index = UtilCharacter.alreadyCreatedRowForColumnSelect(workbook, sheet, UtilCharacter.getDataByLanguage(Language, dtos.priviledge_bn, dtos.priviledge_en), row_index, row_index, cell_col_index, cell_col_index++, addMerged, newRow, indexedColor, isBold, request, reqColIter++);


                    cell_col_index = UtilCharacter.alreadyCreatedRowForColumnSelect(workbook, sheet, UtilCharacter.getDataByLanguage(Language, dtos.edu_info_bn, dtos.edu_info_en), row_index, row_index, cell_col_index, cell_col_index++, addMerged, newRow, indexedColor, isBold, request, reqColIter++);


                    cell_col_index = UtilCharacter.alreadyCreatedRowForColumnSelect(workbook, sheet, UtilCharacter.getDataByLanguage(Language, dtos.district_name_bn, dtos.district_name_en), row_index, row_index, cell_col_index, cell_col_index++, addMerged, newRow, indexedColor, isBold, request, reqColIter++);
                    cell_col_index = UtilCharacter.alreadyCreatedRowForColumnSelect(workbook, sheet, marks, row_index, row_index, cell_col_index, cell_col_index++, addMerged, newRow, indexedColor, isBold, request, reqColIter++);
                    cell_col_index = UtilCharacter.alreadyCreatedRowForColumnSelect(workbook, sheet, prevLevelData, row_index, row_index, cell_col_index, cell_col_index++, addMerged, newRow, indexedColor, isBold, request, reqColIter++);

                    cell_col_index = UtilCharacter.alreadyCreatedRowForColumnSelect(workbook, sheet, "", row_index, row_index, cell_col_index, cell_col_index++, addMerged, newRow, indexedColor, isBold, request, reqColIter++);
                    cell_col_index = UtilCharacter.alreadyCreatedRowForColumnSelect(workbook, sheet, decision, row_index, row_index, cell_col_index, cell_col_index++, addMerged, newRow, indexedColor, isBold, request, reqColIter++);
                    cell_col_index = UtilCharacter.alreadyCreatedRowForColumnSelect(workbook, sheet, UtilCharacter.getDataByLanguage(Language, dtos.decision_remarks, dtos.decision_remarks), row_index, row_index, cell_col_index, cell_col_index++, addMerged, newRow, indexedColor, isBold, request, reqColIter++);
                }

                workbook.write(response.getOutputStream()); // Write workbook to response.
                workbook.close();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.debug(ex);
        }


    }

    void getCandidateList(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher rd;
        rd = request.getRequestDispatcher("recruitment_job_specific_candidate_list/recruitment_job_specific_candidate_list.jsp");
        rd.forward(request, response);
    }

    void getResultNotice(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher rd;
        rd = request.getRequestDispatcher("recruitment_job_specific_candidate_list/recruitment_job_specific_result_notice.jsp");
        rd.forward(request, response);
    }

    void getCandidateListForInternship(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher rd;
        rd = request.getRequestDispatcher("recruitment_job_specific_candidate_list_for_internship/recruitment_job_specific_candidate_list.jsp");
        rd.forward(request, response);
    }


    void getCandidateSearch(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher rd;
        rd = request.getRequestDispatcher("recruitment_candidate_list/recruitment_candidate_list.jsp");
        rd.forward(request, response);
    }

    public List<Job_applicant_applicationDTO> filterVal(long jobId, long level, String approve, boolean isSelected, boolean isRejected
            , HttpServletRequest request) {
        List<Job_applicant_applicationDTO> dtos = Job_applicant_applicationRepository.getInstance().getJob_applicant_applicationDTOByjob_id(jobId);
        String filter = "job_id =" + jobId;

        long internShipId = Long.parseLong(GlobalConfigurationRepository.
                getGlobalConfigDTOByID(GlobalConfigConstants.INTERNSHIP_ID).value);


        if (!isSelected && !isRejected) {
            if (approve != null && !approve.equalsIgnoreCase("ALL")) {
                if (approve.equalsIgnoreCase("REJECTED")) {
//                    filter += " and is_rejected = 1 ";
                    dtos = dtos.stream().filter(i -> i.isRejected).collect(Collectors.toList());
                } else {
//                    filter += " and (is_selected = 1 or is_selected = 2)  ";
                    dtos = dtos.stream().filter(i -> i.isSelected == 1 || i.isSelected == 2).collect(Collectors.toList());
                }
            }
        } else if (isSelected && !isRejected) {
//                filter += " and (is_selected = 1 or is_selected = 2)  ";
            dtos = dtos.stream().filter(i -> i.isSelected == 1 || i.isSelected == 2).collect(Collectors.toList());
        } else if (!isSelected && isRejected) {
//                filter += " and is_rejected = 1 ";
            dtos = dtos.stream().filter(i -> i.isRejected).collect(Collectors.toList());
        }


//        filter += " and level =  " + level;
        dtos = dtos.stream().filter(i -> i.level == level).collect(Collectors.toList());


        String ageFrom = request.getParameter("ageFrom");
        String ageTo = request.getParameter("ageTo");

        if (ageFrom != null && ageTo != null && !ageFrom.equalsIgnoreCase("")
                && !ageTo.equalsIgnoreCase("") && jobId != internShipId) {
            long aFrom = Long.parseLong(ageFrom);
            long aTo = Long.parseLong(ageTo);

//            filter += " and age_in_years >=  " + aFrom + " and age_in_years <=  " + aTo;
            dtos = dtos.stream().filter(i -> i.age_in_years >= aFrom && i.age_in_years <= aTo).collect(Collectors.toList());
        }

        String exFrom = request.getParameter("qualificationFrom");
        String exTo = request.getParameter("qualificationTo");

        if (exFrom != null && exTo != null && !exFrom.equalsIgnoreCase("")
                && !exTo.equalsIgnoreCase("")) {
            long aFrom = Long.parseLong(exFrom);
            long aTo = Long.parseLong(exTo);

//            filter += " and qualification_in_years >=  " +
//                    aFrom + " and qualification_in_years <=  " + aTo;
            dtos = dtos.stream().filter(i -> i.qualification_in_years >= aFrom && i.qualification_in_years <= aTo).collect(Collectors.toList());
        }


        String eduLevel = request.getParameter("eduLevel");

        if (eduLevel != null && !eduLevel.equalsIgnoreCase("")) {
            long aEduLevel = Long.parseLong(eduLevel);

//            filter += " and highest_edu_level >=  " + aEduLevel ;
            dtos = dtos.stream().filter(i -> i.highest_edu_level >= aEduLevel).collect(Collectors.toList());
        }


        String university = request.getParameter("university");

        if (university != null && !university.equalsIgnoreCase("")) {
            long universityIdCheck = Long.parseLong(university);
            String universityString = "," + university + ",";

//            filter += " and university_ids like '" + university + "'" ;
            dtos = dtos.stream().filter(i -> i.university_ids.contains(universityString)).collect(Collectors.toList());
        }

        int quota = Integer.parseInt(request.getParameter("quota"));
        if (quota != 0) {
            filter += " and quota_id =  " + quota + " ";
            dtos = dtos.stream().filter(i -> i.quota_id == quota).collect(Collectors.toList());
        }


        return dtos;
    }

}
