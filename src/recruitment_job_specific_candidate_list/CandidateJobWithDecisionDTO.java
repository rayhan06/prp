package recruitment_job_specific_candidate_list;


public class CandidateJobWithDecisionDTO
{

    public String jobName = "";
    public String jobNameBn = "";
    public String decisionName = "";
    public String decisionNameBn = "";
    public String levelName = "";
    public String levelNameBn = "";
    public long applyDate = 0;
    public long jobId = 0;
	
	
    @Override
	public String toString() {
            return "CandidateJobWithDecisionDTO[" +
            " jobName = " + jobName +
            " jobNameBn = " + jobNameBn +
            " decisionName = " + decisionName +
            " decisionNameBn = " + decisionNameBn +
            " levelName = " + levelName +
            " levelNameBn = " + levelNameBn +
            "]";
    }

}