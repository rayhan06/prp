package ticket_forward_history;
import java.util.*; 
import util.*;


public class Ticket_forward_historyMAPS extends CommonMaps
{	
	public Ticket_forward_historyMAPS(String tableName)
	{
		

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("supportTicketId".toLowerCase(), "supportTicketId".toLowerCase());
		java_DTO_map.put("forwardFromUserId".toLowerCase(), "forwardFromUserId".toLowerCase());
		java_DTO_map.put("forwardToUserId".toLowerCase(), "forwardToUserId".toLowerCase());
		java_DTO_map.put("comments".toLowerCase(), "comments".toLowerCase());
		java_DTO_map.put("forwardDate".toLowerCase(), "forwardDate".toLowerCase());
		java_DTO_map.put("insertedByUserId".toLowerCase(), "insertedByUserId".toLowerCase());
		java_DTO_map.put("insertionDate".toLowerCase(), "insertionDate".toLowerCase());
		java_DTO_map.put("modifiedBy".toLowerCase(), "modifiedBy".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("support_ticket_id".toLowerCase(), "supportTicketId".toLowerCase());
		java_SQL_map.put("forward_from_user_id".toLowerCase(), "forwardFromUserId".toLowerCase());
		java_SQL_map.put("forward_to_user_id".toLowerCase(), "forwardToUserId".toLowerCase());
		java_SQL_map.put("comments".toLowerCase(), "comments".toLowerCase());
		java_SQL_map.put("forward_date".toLowerCase(), "forwardDate".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Support Ticket Id".toLowerCase(), "supportTicketId".toLowerCase());
		java_Text_map.put("Forward From User Id".toLowerCase(), "forwardFromUserId".toLowerCase());
		java_Text_map.put("Forward To User Id".toLowerCase(), "forwardToUserId".toLowerCase());
		java_Text_map.put("Comments".toLowerCase(), "comments".toLowerCase());
		java_Text_map.put("Forward Date".toLowerCase(), "forwardDate".toLowerCase());
		java_Text_map.put("Inserted By User Id".toLowerCase(), "insertedByUserId".toLowerCase());
		java_Text_map.put("Insertion Date".toLowerCase(), "insertionDate".toLowerCase());
		java_Text_map.put("Modified By".toLowerCase(), "modifiedBy".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}