package ticket_forward_history;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import employee_records.Employee_recordsDAO;
import employee_records.Employee_recordsDTO;
import javafx.util.Pair;
import repository.Repository;
import repository.RepositoryManager;
import workflow.WorkflowController;


public class Ticket_forward_historyRepository implements Repository {
	Ticket_forward_historyDAO ticket_forward_historyDAO = null;
	Employee_recordsDAO employee_recordsDAO = new Employee_recordsDAO();
	
	public void setDAO(Ticket_forward_historyDAO ticket_forward_historyDAO)
	{
		this.ticket_forward_historyDAO = ticket_forward_historyDAO;
	}
	
	static Logger logger = Logger.getLogger(Ticket_forward_historyRepository.class);
	Map<Long, Pair<String,String>> userIdToNameMap;


	static Ticket_forward_historyRepository instance = null;  
	private Ticket_forward_historyRepository(){
		userIdToNameMap = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Ticket_forward_historyRepository getInstance(){
		if (instance == null){
			instance = new Ticket_forward_historyRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(ticket_forward_historyDAO == null)
		{
			ticket_forward_historyDAO = new Ticket_forward_historyDAO();
		}
		try {
			List<Employee_recordsDTO> employee_recordsDtos = employee_recordsDAO.getAllEmployee_records(true);
			for(Employee_recordsDTO employee_recordsDTO : employee_recordsDtos) {
				    long userId = WorkflowController.getUserIdFromEmployeeRecordsId(employee_recordsDTO.iD);
				    userIdToNameMap.put(userId, new Pair<String, String>(employee_recordsDTO.nameEng,employee_recordsDTO.nameBng));
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	public String getNameFromId(long userId,boolean isEnglish) {
		if(userIdToNameMap.isEmpty()) return "Not Found";
		else if(isEnglish)
		return userIdToNameMap.get(userId)==null ? "Not Found" : userIdToNameMap.get(userId).getKey();
		else return userIdToNameMap.get(userId)==null ? "খুঁজে পাওয়া যায়নি" : userIdToNameMap.get(userId).getValue();
	}
	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "ticket_forward_history";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


