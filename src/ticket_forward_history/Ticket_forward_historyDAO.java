package ticket_forward_history;


import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.sql.SQLException;
import org.apache.log4j.Logger;
import common.ConnectionAndStatementUtil;
import util.*;


public class Ticket_forward_historyDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());
	
	
	public Ticket_forward_historyDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";
		commonMaps = new Ticket_forward_historyMAPS(tableName);
		columnNames = new String[] 
		{
			"ID",
			"support_ticket_id",
			"forward_from_user_id",
			"forward_to_user_id",
			"forward_from_organogram_id",
			"forward_to_organogram_id",
			"forward_from_name",
			"forward_to_name",
			"forward_from_designation",
			"forward_to_designation",
			"files_dropzone",
			"comments",
			"forward_date",
			"forward_time",
			"is_read",
			"inserted_by_user_id",
			"insertion_date",
			"modified_by",
			"forward_action_cat",
			"noc_status_cat",

			"ticket_issues_type",
			"ticket_issues_subtype_type",
			"ticket_status_cat",
			"task_done",
			"isDeleted",
			"lastModificationTime"
		};
	}
	
	public Ticket_forward_historyDAO()
	{
		this("ticket_forward_history");		
	}
	
	public void setSearchColumn(Ticket_forward_historyDTO ticket_forward_historyDTO)
	{
		ticket_forward_historyDTO.searchColumn = "";
		ticket_forward_historyDTO.searchColumn += ticket_forward_historyDTO.comments + " ";
		
	}
	
	public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException
	{
		Ticket_forward_historyDTO ticket_forward_historyDTO = (Ticket_forward_historyDTO)commonDTO;
		int index = 1;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(ticket_forward_historyDTO);
		if(isInsert)
		{
			ps.setObject(index++,ticket_forward_historyDTO.iD);
		}
		ps.setObject(index++,ticket_forward_historyDTO.supportTicketId);
		ps.setObject(index++,ticket_forward_historyDTO.forwardFromUserId);
		ps.setObject(index++,ticket_forward_historyDTO.forwardToUserId);
		ps.setObject(index++,ticket_forward_historyDTO.forwardFromOrganogramId);
		ps.setObject(index++,ticket_forward_historyDTO.forwardToOrganogramId);
		ps.setObject(index++,ticket_forward_historyDTO.forwardFromName);
		ps.setObject(index++,ticket_forward_historyDTO.forwardToName);
		ps.setObject(index++,ticket_forward_historyDTO.forwardFromDesignation);
		ps.setObject(index++,ticket_forward_historyDTO.forwardToDesignation);
		ps.setObject(index++,ticket_forward_historyDTO.filesDropzone);
		ps.setObject(index++,ticket_forward_historyDTO.comments);
		ps.setObject(index++,ticket_forward_historyDTO.forwardDate);
		ps.setObject(index++,ticket_forward_historyDTO.forwardTime);
		ps.setObject(index++,ticket_forward_historyDTO.isRead);
		ps.setObject(index++,ticket_forward_historyDTO.insertedByUserId);
		ps.setObject(index++,ticket_forward_historyDTO.insertionDate);
		ps.setObject(index++,ticket_forward_historyDTO.modifiedBy);
		ps.setObject(index++,ticket_forward_historyDTO.forwardActionCat);
		ps.setObject(index++,ticket_forward_historyDTO.nocStatusCat);
		ps.setObject(index++,ticket_forward_historyDTO.ticketIssuesType);
		ps.setObject(index++,ticket_forward_historyDTO.ticketIssuesSubtypeType);
		ps.setObject(index++,ticket_forward_historyDTO.ticketStatusCat);
		ps.setObject(index++,ticket_forward_historyDTO.taskDone);
		if(isInsert)
		{
			ps.setObject(index++, 0);
			ps.setObject(index++, lastModificationTime);
		}
	}
	
	public Ticket_forward_historyDTO build(ResultSet rs)
	{
		try {
			Ticket_forward_historyDTO ticket_forward_historyDTO = new Ticket_forward_historyDTO();
			ticket_forward_historyDTO.iD = rs.getLong("ID");
			ticket_forward_historyDTO.supportTicketId = rs.getLong("support_ticket_id");
			ticket_forward_historyDTO.forwardFromUserId = rs.getLong("forward_from_user_id");
			ticket_forward_historyDTO.forwardToUserId = rs.getString("forward_to_user_id");
			ticket_forward_historyDTO.forwardFromOrganogramId = rs.getLong("forward_from_organogram_id");
			ticket_forward_historyDTO.forwardToOrganogramId = rs.getString("forward_to_organogram_id");
			ticket_forward_historyDTO.forwardFromName = rs.getString("forward_from_name");
			ticket_forward_historyDTO.forwardToName = rs.getString("forward_to_name");
			ticket_forward_historyDTO.forwardFromDesignation = rs.getString("forward_from_designation");
			ticket_forward_historyDTO.forwardToDesignation = rs.getString("forward_to_designation");
			ticket_forward_historyDTO.filesDropzone = rs.getLong("files_dropzone");
			ticket_forward_historyDTO.comments = rs.getString("comments");
			ticket_forward_historyDTO.forwardDate = rs.getLong("forward_date");
			ticket_forward_historyDTO.forwardTime = rs.getString("forward_time");
			ticket_forward_historyDTO.isRead = rs.getInt("is_read");
			ticket_forward_historyDTO.insertedByUserId = rs.getLong("inserted_by_user_id");
			ticket_forward_historyDTO.insertionDate = rs.getLong("insertion_date");
			ticket_forward_historyDTO.modifiedBy = rs.getString("modified_by");
			ticket_forward_historyDTO.forwardActionCat = rs.getInt("forward_action_cat");
			ticket_forward_historyDTO.nocStatusCat = rs.getInt("noc_status_cat");
			
			ticket_forward_historyDTO.ticketIssuesType = rs.getLong("ticket_issues_type");
			ticket_forward_historyDTO.ticketIssuesSubtypeType = rs.getLong("ticket_issues_subtype_type");
			ticket_forward_historyDTO.ticketStatusCat = rs.getInt("ticket_status_cat");
			ticket_forward_historyDTO.taskDone = rs.getInt("task_done");
			
			ticket_forward_historyDTO.isDeleted = rs.getInt("isDeleted");
			ticket_forward_historyDTO.lastModificationTime = rs.getLong("lastModificationTime");
			
			return ticket_forward_historyDTO;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public Ticket_forward_historyDTO getDTOByID (long id)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID = " + id;
		Ticket_forward_historyDTO ticket_forward_historyDTO = ConnectionAndStatementUtil.getT(sql,this::build);
		return ticket_forward_historyDTO;
	}
	
	public Ticket_forward_historyDTO getLastDTOBySupportTicketId (long supportTicketId)
	{
		
		String sql = "SELECT * ";

		sql += " FROM " + tableName;
		
        sql += " WHERE support_ticket_id =" + supportTicketId + " order by id desc limit 1";
		Ticket_forward_historyDTO ticket_forward_historyDTO = ConnectionAndStatementUtil.getT(sql,this::build);
		return ticket_forward_historyDTO;
	}
	
	public Ticket_forward_historyDTO getLastAssignedDTOBySupportTicketId (long supportTicketId)
	{
		
		String sql = "SELECT * ";

		sql += " FROM " + tableName;
		
        sql += " WHERE forward_action_cat = " + Ticket_forward_historyDTO.TICKET_ASSIGN
        		+ " and support_ticket_id =" + supportTicketId + " order by id desc limit 1";
		Ticket_forward_historyDTO ticket_forward_historyDTO = ConnectionAndStatementUtil.getT(sql,this::build);
		return ticket_forward_historyDTO;
	}
	
	public List<Ticket_forward_historyDTO> getDtosByForwardToUserId(long forwardToUserId)
	{
		String sql = "SELECT * ";

		sql += " FROM " + tableName;
        
        sql += " WHERE forward_to_user_id  = " + forwardToUserId + " and is_read = 0;" ;
        return ConnectionAndStatementUtil.getListOfT(sql,this::build);
	}
	
	public List<Ticket_forward_historyDTO> getAllByTicketId (long supportTicketId)
	{
		System.out.println("getAllByTicketId = " + supportTicketId);
		String sql = "SELECT * FROM ticket_forward_history where isDeleted = 0 and support_ticket_id = " + supportTicketId
				+ " order by lastModificationTime desc";
        return ConnectionAndStatementUtil.getListOfT(sql,this::build);
	}
	
}
	