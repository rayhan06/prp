package ticket_forward_history;

import java.io.IOException;
import java.io.*;
import java.text.SimpleDateFormat;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import org.apache.log4j.Logger;

import login.LoginDTO;
import permission.MenuConstants;
import role.PermissionRepository;


import sessionmanager.SessionConstants;

import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;
import util.CommonRequestHandler;
import util.RecordNavigationManager4;

import java.util.*;
import javax.servlet.http.*;
import java.util.UUID;





import geolocation.GeoLocationDAO2;
import java.util.StringTokenizer;

import com.google.gson.Gson;

import pb.*;
import pbReport.*;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;




/**
 * Servlet implementation class Ticket_forward_historyServlet
 */
@WebServlet("/Ticket_forward_historyServlet")
@MultipartConfig
public class Ticket_forward_historyServlet extends HttpServlet 
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Ticket_forward_historyServlet.class);

    String tableName = "ticket_forward_history";

	Ticket_forward_historyDAO ticket_forward_historyDAO;
	CommonRequestHandler commonRequestHandler;
    private final Gson gson = new Gson();
    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Ticket_forward_historyServlet() 
	{
        super();
    	try
    	{
			ticket_forward_historyDAO = new Ticket_forward_historyDAO(tableName);
			commonRequestHandler = new CommonRequestHandler(ticket_forward_historyDAO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }   

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		System.out.println("In doget request = " + request);
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		try
		{
			String actionType = request.getParameter("actionType");
			if(actionType.equals("getAddPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.TICKET_FORWARD_HISTORY_ADD))
				{
					commonRequestHandler.getAddPage(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getEditPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.TICKET_FORWARD_HISTORY_UPDATE))
				{
					getTicket_forward_history(request, response);					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}						
			}
			else if(actionType.equals("getURL"))
			{
				String URL = request.getParameter("URL");
				System.out.println("URL = " + URL);
				response.sendRedirect(URL);			
			}			
			else if(actionType.equals("search"))
			{
				System.out.println("search requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.TICKET_FORWARD_HISTORY_SEARCH))
				{
					if(isPermanentTable)
					{
						request.setAttribute("supportTicketId", (long)-1000);
						String filter = request.getParameter("filter");
						System.out.println("filter = " + filter);
						if(filter!=null)
						{
							filter = ""; //shouldn't be directly used, rather manipulate it.
							searchTicket_forward_history(request, response, isPermanentTable, filter);
						}
						else
						{
							searchTicket_forward_history(request, response, isPermanentTable, "");
						}
					}					
					else
					{
						//searchTicket_forward_history(request, response, tempTableName, isPermanentTable);
					}
				}			
			}
			else if(actionType.equals("view"))
			{
				System.out.println("view requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.SUPPORT_TICKET_SEARCH))
				{
					commonRequestHandler.view(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			} else if(actionType.equals("viewHistory"))
			{
				System.out.println("search requested");
				long supportTicketId = Long.parseLong(request.getParameter("ID"));
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.SUPPORT_TICKET_SEARCH))
				{
					if(isPermanentTable)
					{
						String filter = request.getParameter("filter");
						System.out.println("filter = " + filter);
						request.setAttribute("supportTicketId", supportTicketId);
						if(filter!=null)
						{
							filter = ""; //shouldn't be directly used, rather manipulate it.
							searchTicket_forward_history(request, response, isPermanentTable, filter);
						}
						else
						{
							searchTicket_forward_history(request, response, isPermanentTable, "");
						}
					}					
					else
					{
						//searchTicket_forward_history(request, response, tempTableName, isPermanentTable);
					}
				}			
			}
			else
			{
				request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}


	
	

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		// TODO Auto-generated method stub
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		System.out.println("doPost");
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		
		try
		{
			String actionType = request.getParameter("actionType");
			System.out.println("actionType = " + actionType);
			if(actionType.equals("add"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.TICKET_FORWARD_HISTORY_ADD))
				{
					System.out.println("going to  addTicket_forward_history ");
					addTicket_forward_history(request, response, true, userDTO, true);
				}
				else
				{
					System.out.println("Not going to  addTicket_forward_history ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else if(actionType.equals("UploadFilesFromDropZone"))
			{
				String Column = request.getParameter("columnName");
				long ColumnID = Long.parseLong(request.getParameter("ColumnID"));
				String pageType = request.getParameter("pageType");
				
				System.out.println("In " + pageType);				
				Utils.UploadFilesFromDropZone(request, response, userDTO.ID, ColumnID, "Ticket_forward_historyServlet");	
			}

			else if(actionType.equals("getDTO"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.TICKET_FORWARD_HISTORY_ADD))
				{
					getDTO(request, response);					
				}
				else
				{
					System.out.println("Not going to  addTicket_forward_history ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else if(actionType.equals("edit"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.TICKET_FORWARD_HISTORY_UPDATE))
				{					
					addTicket_forward_history(request, response, false, userDTO, isPermanentTable);					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("delete"))
			{								
				deleteTicket_forward_history(request, response, userDTO);				
			}	
			else if(actionType.equals("search"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.TICKET_FORWARD_HISTORY_SEARCH))
				{
					searchTicket_forward_history(request, response, true, "");
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getGeo"))
			{
				System.out.println("going to geoloc ");
				request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
			}
			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}
	
	private void getDTO(HttpServletRequest request, HttpServletResponse response) 
	{
		try 
		{
			System.out.println("In getDTO");
			Ticket_forward_historyDTO ticket_forward_historyDTO = ticket_forward_historyDAO.getDTOByID(Long.parseLong(request.getParameter("ID")));
			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			
			String encoded = this.gson.toJson(ticket_forward_historyDTO);
			System.out.println("json encoded data = " + encoded);
			out.print(encoded);
			out.flush();
		}
		catch (NumberFormatException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	private void addTicket_forward_history(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, boolean isPermanentTable) throws IOException 
	{
		
	}
	
	



	
	
	

	private void deleteTicket_forward_history(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO) throws IOException 
	{				
		try 
		{
			String[] IDsToDelete = request.getParameterValues("ID");
			for(int i = 0; i < IDsToDelete.length; i ++)
			{
				long id = Long.parseLong(IDsToDelete[i]);
				System.out.println("------ DELETING " + IDsToDelete[i]);
				
				
				Ticket_forward_historyDTO ticket_forward_historyDTO = ticket_forward_historyDAO.getDTOByID(id);
				ticket_forward_historyDAO.manageWriteOperations(ticket_forward_historyDTO, SessionConstants.DELETE, id, userDTO);
				response.sendRedirect("Ticket_forward_historyServlet?actionType=search");
				
			}			
		}
		catch (Exception ex) 
		{
			ex.printStackTrace();
		}
		
	}

	private void getTicket_forward_history(HttpServletRequest request, HttpServletResponse response, long id) throws ServletException, IOException
	{
		System.out.println("in getTicket_forward_history");
		Ticket_forward_historyDTO ticket_forward_historyDTO = null;
		try 
		{
			ticket_forward_historyDTO = ticket_forward_historyDAO.getDTOByID(id);
			request.setAttribute("ID", ticket_forward_historyDTO.iD);
			request.setAttribute("ticket_forward_historyDTO",ticket_forward_historyDTO);
			request.setAttribute("ticket_forward_historyDAO",ticket_forward_historyDAO);
			
			String URL= "";
			
			String inPlaceEdit = request.getParameter("inplaceedit");
			String inPlaceSubmit = request.getParameter("inplacesubmit");
			String getBodyOnly = request.getParameter("getBodyOnly");
			
			if(inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase(""))
			{
				URL = "ticket_forward_history/ticket_forward_historyInPlaceEdit.jsp";	
				request.setAttribute("inplaceedit","");				
			}
			else if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
			{
				URL = "ticket_forward_history/ticket_forward_historySearchRow.jsp";
				request.setAttribute("inplacesubmit","");					
			}
			else
			{
				if(getBodyOnly  != null && !getBodyOnly.equalsIgnoreCase(""))
				{
					URL = "ticket_forward_history/ticket_forward_historyEditBody.jsp?actionType=edit";
				}
				else
				{
					URL = "ticket_forward_history/ticket_forward_historyEdit.jsp?actionType=edit";
				}				
			}
			
			RequestDispatcher rd = request.getRequestDispatcher(URL);
			rd.forward(request, response);
		}
		catch (NumberFormatException e) 
		{
			e.printStackTrace();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	
	private void getTicket_forward_history(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		getTicket_forward_history(request, response, Long.parseLong(request.getParameter("ID")));	
	}
	
	private void searchTicket_forward_history(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException
	{
		System.out.println("in  searchTicket_forward_history 1");
		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String ajax = request.getParameter("ajax");
		boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);
		
        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
			SessionConstants.NAV_TICKET_FORWARD_HISTORY,
			request,
			ticket_forward_historyDAO,
			SessionConstants.VIEW_TICKET_FORWARD_HISTORY,
			SessionConstants.SEARCH_TICKET_FORWARD_HISTORY,
			tableName,
			isPermanent,
			userDTO,
			filter,
			true);
        try
        {
			System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        }
        catch(Exception e)
        {
			System.out.println("failed to dojob" + e);
        }

		request.setAttribute("ticket_forward_historyDAO",ticket_forward_historyDAO);
        RequestDispatcher rd;
        if(!isPermanent)
        {
        	if(hasAjax == false)
	        {
	        	System.out.println("Going to ticket_forward_history/ticket_forward_historyApproval.jsp");
	        	rd = request.getRequestDispatcher("ticket_forward_history/ticket_forward_historyApproval.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to ticket_forward_history/ticket_forward_historyApprovalForm.jsp");
	        	rd = request.getRequestDispatcher("ticket_forward_history/ticket_forward_historyApprovalForm.jsp");
	        }
        }
        else
        {
	        if(hasAjax == false)
	        {
	        	System.out.println("Going to ticket_forward_history/ticket_forward_historySearch.jsp");
	        	rd = request.getRequestDispatcher("ticket_forward_history/ticket_forward_historySearch.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to ticket_forward_history/ticket_forward_historySearchForm.jsp");
	        	rd = request.getRequestDispatcher("ticket_forward_history/ticket_forward_historySearchForm.jsp");
	        }
        }
		rd.forward(request, response);
	}
	
}

