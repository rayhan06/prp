package ticket_forward_history;
import java.text.SimpleDateFormat;
import java.util.*;

import asset_clearance_letter.Asset_clearance_letterDTO;
import asset_model.AssetAssigneeDTO;
import util.*;
import workflow.WorkflowController;
import support_ticket.*;
import user.UserDTO; 


public class Ticket_forward_historyDTO extends CommonDTO
{
	
	public static final int STOCK_IN = AssetAssigneeDTO.STOCK_IN;
    public static final int ASSIGN = AssetAssigneeDTO.ASSIGNED;
    public static final int UNASSIGN = AssetAssigneeDTO.NOT_ASSIGNED;
    public static final int ACTIVATE = AssetAssigneeDTO.ACTIVATE;
    public static final int DEACTIVATE = AssetAssigneeDTO.DEACTIVATE;
	public static final int WARRANTY = AssetAssigneeDTO.WARRANTY;
	public static final int SEND_TO_REPAIR = AssetAssigneeDTO.SEND_TO_REPAIR;
	public static final int CONDEMN = AssetAssigneeDTO.CONDEMNED;
	public static final int RELEASE = AssetAssigneeDTO.RELEASED;	
	public static final int FORWARD = AssetAssigneeDTO.FORWARD;
	public static final int CLOSE = AssetAssigneeDTO.CLOSE;
	public static final int TICKET_ASSIGN = AssetAssigneeDTO.TICKET_ASSIGN;
	public static final int ADD_COMMENTS = AssetAssigneeDTO.ADD_COMMENTS;
	public static final int SOLVE_TICKET = AssetAssigneeDTO.SOLVE_TICKET;
	public static final int DELETE = 13;
	
	public static final int NOC_VERIFICATION = AssetAssigneeDTO.NOC_VERIFICATION;
    public static final int NOC_APPROVAL = AssetAssigneeDTO.NOC_APPROVAL;
    public static final int NOC_REJECTION = AssetAssigneeDTO.NOC_REJECTION;


    public long supportTicketId = 0;
	public long forwardFromUserId = 0;
	public String forwardToUserId = "";
	public long forwardFromOrganogramId = -1;
	public String forwardToOrganogramId = "";
	public String forwardFromName = "";
	public String forwardToName = "";
	public String forwardFromDesignation = "";
	public String forwardToDesignation = "";
	public long filesDropzone = 0;
    public String comments = "";
	public long forwardDate = 0;
	public String forwardTime = "";
	public int isRead = 0;
	public long insertedByUserId = 0;
	public long insertionDate = 0;
    public String modifiedBy = "";
    public int forwardActionCat = ASSIGN;
    public long ticketIssuesType = 0;
   	public long ticketIssuesSubtypeType = 0;
   	public int ticketStatusCat = Support_ticketDTO.OPEN;
   	public int nocStatusCat = Asset_clearance_letterDTO.PENDING;
   	public int taskDone = -1;
    
    public Ticket_forward_historyDTO()
    {
    	
    }
    
    public Ticket_forward_historyDTO(long forwardFromOrganogramId, String comments, int forwardActionCat, long supportTicketId)
    {
    	this.forwardFromOrganogramId =  forwardFromOrganogramId;
    	this.comments = comments;
    	this.forwardActionCat = forwardActionCat;
    	this.supportTicketId = supportTicketId;
    	forwardDate = TimeConverter.getToday();
    }
    
    public Ticket_forward_historyDTO(Support_ticketDTO support_ticketDTO, UserDTO fromUserDTO, UserDTO toUserDTO, String forwardComments,
            long filesDropzone, UserDTO userDTO, int forwardActionCat, int taskDone)
    {
    	
    	this(
    			support_ticketDTO, 
    			fromUserDTO, 
    			new ArrayList<UserDTO> () 
    			{
    				private static final long serialVersionUID = 1L;
    				{add(toUserDTO);}
    			},
    			forwardComments,
                filesDropzone,
                userDTO,
                forwardActionCat,
                taskDone);
    	
    }
    
    public Ticket_forward_historyDTO(Support_ticketDTO support_ticketDTO, UserDTO fromUserDTO, List<UserDTO> toUserDTOs, String forwardComments,
            long filesDropzone, UserDTO userDTO, int forwardActionCat, int taskDone)
    {
    	long time = System.currentTimeMillis();
    	this.taskDone = taskDone;

        this.supportTicketId = support_ticketDTO.iD;
        this.ticketIssuesType = support_ticketDTO.ticketIssuesType;
        this.ticketIssuesSubtypeType = support_ticketDTO.ticketIssuesSubtypeType;
        this.ticketStatusCat = support_ticketDTO.ticketStatusCat;
        this.nocStatusCat = support_ticketDTO.nocStatusCat;
        this.forwardFromUserId = fromUserDTO.ID;
        this.forwardFromOrganogramId = fromUserDTO.organogramID;
        this.forwardFromName = WorkflowController.getNameFromUserId(fromUserDTO.ID, false) + "$"
                + WorkflowController.getNameFromUserId(fromUserDTO.ID, true);
        this.forwardFromDesignation = WorkflowController.getOrganogramName(fromUserDTO.organogramID, false) + "$"
                + WorkflowController.getNameFromUserId(fromUserDTO.organogramID, true);

        if (toUserDTOs != null) {
        	this.forwardToUserId = "";
        	this.forwardToOrganogramId = "";
        	int i = 0;
        	for(UserDTO toUserDTO: toUserDTOs)
        	{
        		if(i > 0)
        		{
        			this.forwardToUserId += ", ";
        			this.forwardToOrganogramId += ", ";
        		}
        		this.forwardToUserId += toUserDTO.ID + "";
                this.forwardToOrganogramId += toUserDTO.organogramID + "";
                i++;
        	}
        }


        this.filesDropzone = filesDropzone;

        this.comments = forwardComments;
        this.forwardActionCat = forwardActionCat;


        this.forwardDate = time;

        this.insertedByUserId = userDTO.ID;

        this.insertionDate = TimeConverter.getToday();

        this.modifiedBy = "";

        SimpleDateFormat dateFormat = new SimpleDateFormat("hh.mm aa");
        this.forwardTime = dateFormat.format(new Date());
        this.isRead = 0;
    }
	
	
    @Override
	public String toString() {
            return "$Ticket_forward_historyDTO[" +
            " iD = " + iD +
            " supportTicketId = " + supportTicketId +
            " forwardFromUserId = " + forwardFromUserId +
            " forwardToUserId = " + forwardToUserId +
            " comments = " + comments +
            " forwardDate = " + forwardDate +
            " insertedByUserId = " + insertedByUserId +
            " insertionDate = " + insertionDate +
            " modifiedBy = " + modifiedBy +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}