package marquee_text;

import org.apache.log4j.Logger;
import repository.Repository;
import repository.RepositoryManager;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public class Marquee_textRepository implements Repository {
    private static final Logger logger = Logger.getLogger(Marquee_textRepository.class);

    private final Map<Long, Marquee_textDTO> mapById;

    private Marquee_textRepository() {
        mapById = new ConcurrentHashMap<>();
        RepositoryManager.getInstance().addRepository(this);
    }

    private static class LazyLoader {
        final static Marquee_textRepository INSTANCE = new Marquee_textRepository();
    }

    public static Marquee_textRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

    @Override
    public synchronized void reload(boolean reloadAll) {
        logger.debug("Marquee_textRepository loading start for reloadAll: " + reloadAll);
        List<Marquee_textDTO> dtoList = Marquee_textDAO.getInstance().getAllDTOs(reloadAll);
        if (dtoList != null && dtoList.size() > 0) {
            dtoList.stream()
                   .peek(this::removeIfPresent)
                   .filter(dto -> dto.isDeleted == 0)
                   .forEach(dto -> mapById.put(dto.iD, dto));
        }
        logger.debug("Marquee_textRepository loading end for reloadAll: " + reloadAll);
    }

    private void removeIfPresent(Marquee_textDTO dto) {
        if (mapById.get(dto.iD) != null) {
            mapById.remove(dto.iD);
        }
    }

    public List<Marquee_textDTO> getVisibleDtoList(long timeStamp) {
        return mapById.values()
                      .stream()
                      .filter(marqueeTextDto -> marqueeTextDto.isVisible(timeStamp))
                      .sorted(Marquee_textDTO.defaultComparator)
                      .collect(Collectors.toList());
    }

    public List<Marquee_textDTO> getVisibleDtoListForLoginPage(long timeStamp) {
        return mapById.values()
                      .stream()
                      .filter(marqueeTextDto -> marqueeTextDto.visibleInLoginPage)
                      .filter(marqueeTextDto -> marqueeTextDto.isVisible(timeStamp))
                      .sorted(Marquee_textDTO.defaultComparator)
                      .collect(Collectors.toList());
    }

    @Override
    public String getTableName() {
        return "marquee_text";
    }
}
