package marquee_text;

import common.BaseServlet;
import common.CustomException;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import pb.Utils;
import permission.MenuConstants;
import sessionmanager.SessionConstants;
import user.UserDTO;
import util.CommonDTO;
import util.DateTimeUtil;
import util.HttpRequestUtils;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.stream.IntStream;

@WebServlet("/Marquee_textServlet")
@MultipartConfig
public class Marquee_textServlet extends BaseServlet {
    public static Logger logger = Logger.getLogger(Marquee_textServlet.class);

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        long currentTime = System.currentTimeMillis();
        boolean isLangEng = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;

        Marquee_textDTO marqueeTextDTO;
        if (addFlag) {
            marqueeTextDTO = new Marquee_textDTO();
            marqueeTextDTO.insertedBy = userDTO.ID;
            marqueeTextDTO.insertionTime = currentTime;
        } else {
            marqueeTextDTO = Marquee_textDAO.getInstance().getDTOFromID(Long.parseLong(request.getParameter("iD")));
        }
        marqueeTextDTO.modifiedBy = userDTO.ID;
        marqueeTextDTO.lastModificationTime = currentTime;

        marqueeTextDTO.serial = Utils.parseMandatoryInt(
                request.getParameter("serial"),
                null,
                isLangEng ? "Enter Serial" : "ক্রম লিখুন"
        );
        marqueeTextDTO.textEn = Utils.cleanAndGetMandatoryString(
                request.getParameter("textEn"),
                isLangEng ? "Write Description (English) " : "বিবরণ (ইংরেজি) লিখুন"
        );
        marqueeTextDTO.textBn = Utils.cleanAndGetMandatoryString(
                request.getParameter("textBn"),
                isLangEng ? "Write Description (Bangla)" : "বিবরণ বিবরণ (বাংলা)"
        );
        marqueeTextDTO.url = Jsoup.clean(
                request.getParameter("url"), Whitelist.simpleText()
        );
        marqueeTextDTO.showFromTime = DateTimeUtil.getTimestamp(
                request.getParameter("showFromTime"),
                isLangEng ? "Write Description (Bangla)" : "বিবরণ বিবরণ (বাংলা)",
                SessionConstants.MIN_DATE
        );
        marqueeTextDTO.visible = Boolean.parseBoolean(request.getParameter("visible"));
        marqueeTextDTO.visibleInLoginPage = marqueeTextDTO.visible
                                            && Boolean.parseBoolean(request.getParameter("visibleInLoginPage"));
        if (addFlag) {
            Marquee_textDAO.getInstance().add(marqueeTextDTO);
        } else {
            Marquee_textDAO.getInstance().update(marqueeTextDTO);
        }
        return marqueeTextDTO;
    }

    @Override
    public String getSortClause(HttpServletRequest request) {
        return " serial ASC ";
    }

    @Override
    protected void deleteT(List<Long> ids, UserDTO userDTO) {
        if (ids.isEmpty()) {
            return;
        }
        long deleteTime = System.currentTimeMillis();
        Marquee_textDAO marqueeTextDAO = Marquee_textDAO.getInstance();
        marqueeTextDAO.delete(userDTO.ID, ids, deleteTime);
        List<Marquee_textDTO> allMarqueeTextDTOs = marqueeTextDAO.getAllDTOs();
        allMarqueeTextDTOs.sort(Marquee_textDTO.defaultComparator);
        int index = 0;
        for(Marquee_textDTO marqueeTextDTO : allMarqueeTextDTOs) {
            marqueeTextDTO.serial = ++index;
            marqueeTextDTO.modifiedBy = userDTO.ID;
            marqueeTextDTO.lastModificationTime = deleteTime;
            try {
                marqueeTextDAO.update(marqueeTextDTO);
            } catch (Exception e) {
                logger.error(e);
                throw new CustomException("server error");
            }
        }
    }

    @Override
    public String getTableName() {
        return getCommonDAOService().getTableName();
    }

    @Override
    public String getServletName() {
        return "Marquee_textServlet";
    }

    @Override
    public Marquee_textDAO getCommonDAOService() {
        return Marquee_textDAO.getInstance();
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[]{MenuConstants.MARQUEE_TEXT_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[]{MenuConstants.MARQUEE_TEXT_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[]{MenuConstants.MARQUEE_TEXT_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Marquee_textServlet.class;
    }
}
