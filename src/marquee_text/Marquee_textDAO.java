package marquee_text;

import common.CommonDAOService;
import org.apache.log4j.Logger;
import util.CommonDTO;
import util.StringUtils;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

@SuppressWarnings({"Duplicates"})
public class Marquee_textDAO implements CommonDAOService<Marquee_textDTO> {
    private static final Logger logger = Logger.getLogger(Marquee_textDAO.class);

    private static final String addSqlQuery =
            "INSERT INTO {tableName} (text_en,text_bn,url,serial,show_from_time,visible,visible_in_login_page,search_column,"
                    .concat("modified_by,lastModificationTime,inserted_by,insertion_time,isDeleted,ID) ")
                    .concat("VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

    private static final String updateSqlQuery =
            "UPDATE {tableName} SET text_en=?,text_bn=?,url=?,serial=?,show_from_time=?,visible=?,visible_in_login_page=?,"
                    .concat("search_column=?,modified_by=?,lastModificationTime=? WHERE ID=?");

    private static final Map<String, String> searchMap = new HashMap<>();

    private Marquee_textDAO() {
        searchMap.put("AnyField", " and (search_column LIKE ?) ");
    }

    private static class LazyLoader {
        static final Marquee_textDAO INSTANCE = new Marquee_textDAO();
    }

    public static Marquee_textDAO getInstance() {
        return LazyLoader.INSTANCE;
    }

    @Override
    public String getTableName() {
        return "marquee_text";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    void setSearchColumn(Marquee_textDTO dto) {
        dto.searchColumn = dto.textEn + " " + dto.textBn + " " + dto.url + " "
                           + dto.serial + " " + StringUtils.convertToBanNumber("" + dto.serial);
    }

    @Override
    public void set(PreparedStatement ps, Marquee_textDTO dto, boolean isInsert) throws SQLException {
        setSearchColumn(dto);
        int index = 0;
        ps.setString(++index, dto.textEn);
        ps.setString(++index, dto.textBn);
        ps.setString(++index, dto.url);
        ps.setInt(++index, dto.serial);
        ps.setLong(++index, dto.showFromTime);
        ps.setBoolean(++index, dto.visible);
        ps.setBoolean(++index, dto.visibleInLoginPage);
        ps.setString(++index, dto.searchColumn);
        ps.setLong(++index, dto.modifiedBy);
        ps.setLong(++index, dto.lastModificationTime);
        if (isInsert) {
            ps.setLong(++index, dto.insertedBy);
            ps.setLong(++index, dto.insertionTime);
            ps.setInt(++index, 0 /* isDeleted */);
        }
        ps.setLong(++index, dto.iD);
    }

    @Override
    public Marquee_textDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Marquee_textDTO dto = new Marquee_textDTO();
            dto.iD = rs.getLong("ID");
            dto.textEn = rs.getString("text_en");
            dto.textBn = rs.getString("text_bn");
            dto.url = rs.getString("url");
            dto.serial = rs.getInt("serial");
            dto.showFromTime = rs.getLong("show_from_time");
            dto.visible = rs.getBoolean("visible");
            dto.visibleInLoginPage = rs.getBoolean("visible_in_login_page");
            dto.searchColumn = rs.getString("search_column");
            dto.modifiedBy = rs.getLong("modified_by");
            dto.lastModificationTime = rs.getLong("lastModificationTime");
            dto.insertedBy = rs.getLong("inserted_by");
            dto.insertionTime = rs.getLong("insertion_time");
            dto.isDeleted = rs.getInt("isDeleted");
            return dto;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    @Override
    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Marquee_textDTO) commonDTO, addSqlQuery, true);
    }

    @Override
    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Marquee_textDTO) commonDTO, updateSqlQuery, false);
    }
}
