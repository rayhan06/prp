package marquee_text;

import common.StringUtils;
import sessionmanager.SessionConstants;
import util.CommonDTO;

import java.util.Comparator;

public class Marquee_textDTO extends CommonDTO {
    public String textEn = "";
    public String textBn = "";
    public String url = "";
    public int serial = -1;
    public long showFromTime = SessionConstants.MIN_DATE;
    public boolean visible = true;
    public boolean visibleInLoginPage = true;

    public long insertedBy = -1;
    public long insertionTime = -1;
    public long modifiedBy = -1;

    public static Comparator<Marquee_textDTO> defaultComparator = Comparator.comparingInt(marqueeTextDTO -> marqueeTextDTO.serial);

    public boolean isVisible(long timeStamp) {
        return visible && showFromTime <= timeStamp;
    }

    public String getUrl() {
        return StringUtils.isBlank(url) ? "" : url;
    }

    public String getText(boolean isLangEn) {
        return isLangEn ? textEn : textBn;
    }
}
