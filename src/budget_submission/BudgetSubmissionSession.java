package budget_submission;

public class BudgetSubmissionSession {
    public static String SESSION_VARIABLE_NAME = "budgetSubmissionSession";

    public long userId = -1L;
    public long budgetOfficeId = -1L;

    public BudgetSubmissionSession(long userId, long budgetOfficeId) {
        this.userId = userId;
        this.budgetOfficeId = budgetOfficeId;
    }
}
