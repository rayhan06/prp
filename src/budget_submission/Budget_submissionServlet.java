package budget_submission;

import budget.*;
import budget_mapping.Budget_mappingRepository;
import budget_password.Budget_passwordDAO;
import budget_password.Budget_passwordDTO;
import budget_password.PasswordGenerator;
import budget_selection_info.BudgetSelectionInfoDAO;
import budget_selection_info.BudgetSelectionInfoDTO;
import budget_submission_info.Budget_submission_infoDAO;
import budget_submission_info.Budget_submission_infoDTO;
import com.google.gson.Gson;
import common.ApiResponse;
import common.BaseServlet;
import common.CommonDAOService;
import login.LoginDTO;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.CommonConstant;
import util.CommonDTO;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/Budget_submissionServlet")
@MultipartConfig
public class Budget_submissionServlet extends BaseServlet {
    private static final long serialVersionUID = 1L;
    public static final Logger logger = Logger.getLogger(Budget_submissionServlet.class);

    private final BudgetDAO budgetDAO = BudgetDAO.getInstance() ;
    private final Gson gson = new Gson();

    @Override
    public String getTableName() {
        return "budget";
    }

    @Override
    public String getServletName() {
        return "Budget_submissionServlet";
    }

    @Override
    public CommonDAOService<? extends CommonDTO> getCommonDAOService() {
        return null;
    }

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        return null;
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[0];
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[0];
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[0];
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Budget_submissionServlet.class;
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO != null) {
            try {
                String language = userDTO.languageID == CommonConstant.Language_ID_English ? "English" : "Bangla";
                String actionType = request.getParameter("actionType");
                switch (actionType) {
                    case "getBudgetOperation": {
                        Long selectedValue = request.getParameter("selectedValue") != null ? Long.parseLong(request.getParameter("selectedValue")) : null;
                        int budgetCat = Integer.parseInt(request.getParameter("budget_cat"));
                        long budgetOfficeId = findOfficeIdOfUser(request);
                        boolean withCode = Boolean.parseBoolean(request.getParameter("withCode"));
                        String options = Budget_mappingRepository.getInstance().buildOperationDropDown(language, budgetCat, budgetOfficeId, selectedValue, withCode);
                        logger.debug(options);
                        response.setContentType("text/html; charset=UTF-8");
                        response.getWriter().println(options);
                        return;
                    }
                    case "budgetSubmissionLogin":
                        request.getRequestDispatcher("budget_submission/budget_submissionPassword.jsp").forward(request, response);
                        return;
                    case "getSubmissionPage":
                        String token = (String) request.getSession(true).getAttribute(SessionConstants.BUDGET_SESSION_TOKEN);
                        if (token == null) {
                            if (userDTO.languageID == CommonConstant.Language_ID_English) {
                                request.setAttribute(SessionConstants.ERROR_MESSAGE, "Your budget session has been expired");
                            } else {
                                request.setAttribute(SessionConstants.ERROR_MESSAGE, "আপনার বাজেট সেশনটির মেয়াদ শেষ হয়ে গেছে");
                            }
                            logger.error("Budget session token is expired");
                            request.getRequestDispatcher("common/error_message_page.jsp").forward(request, response);
                            return;
                        }
                        request.getSession(true).removeAttribute(SessionConstants.BUDGET_SESSION_TOKEN);
                        request.setAttribute("budgetOfficeId", findOfficeIdOfUser(request));
                        request.getRequestDispatcher("budget_submission/budget_submission.jsp").forward(request, response);
                        return;
                    case "getPreviewPage":
                        request.setAttribute("budgetOfficeId", findOfficeIdOfUser(request));
                        request.getRequestDispatcher("budget_submission/budget_submission_preview.jsp").forward(request, response);
                        return;
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                logger.debug(ex);
            }
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);

        if (userDTO != null) {
            try {
                String actionType = request.getParameter("actionType");
                switch (actionType) {
                    case "checkPassword": {
                        ApiResponse apiResponse;
                        try {
                            checkPasswordAndSendToAddPage(request, userDTO);
                            String budgetToken = new PasswordGenerator.PasswordGeneratorBuilder()
                                    .useDigits(true)
                                    .useLower(true)
                                    .useUpper(true)
                                    .build()
                                    .generate(SessionConstants.BUDGET_SESSION_TOKEN_LENGTH);
                            request.getSession(true).setAttribute(SessionConstants.BUDGET_SESSION_TOKEN, budgetToken);
                            apiResponse = ApiResponse.makeSuccessResponse("Budget_submissionServlet?actionType=getSubmissionPage", budgetToken);
                        } catch (Exception ex) {
                            logger.error(ex);
                            apiResponse = ApiResponse.makeErrorResponse(ex.getMessage());
                        }
                        PrintWriter pw = response.getWriter();
                        pw.write(apiResponse.getJSONString());
                        pw.flush();
                        pw.close();
                        return;
                    }
                    case "insertBudgetAmount": {
                        boolean saveSuccess = saveBudgetForOffice(userDTO, request);
                        String resJson = saveSuccess? "{\"success\": true}" : "{\"success\": false}";
                        PrintWriter out = response.getWriter();
                        response.setContentType("application/json");
                        response.setCharacterEncoding("UTF-8");
                        out.print(resJson);
                        out.flush();
                        return;
                    }
                    case "submitBudgetForOffice": {
                        submitBudgetForOffice(request, userDTO);
                        String resJson = "{\"success\": true}";
                        PrintWriter out = response.getWriter();
                        response.setContentType("application/json");
                        response.setCharacterEncoding("UTF-8");
                        out.print(resJson);
                        out.flush();
                        return;
                    }
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                logger.debug(ex);
            }
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }

    public static Long findOfficeIdOfUser(HttpServletRequest request) throws Exception {
        BudgetSubmissionSession budgetSubmissionSession = (BudgetSubmissionSession)
                request.getSession(true).getAttribute(BudgetSubmissionSession.SESSION_VARIABLE_NAME);

        if (budgetSubmissionSession == null) throw new Exception("Unauthorised User");

        return budgetSubmissionSession.budgetOfficeId;
    }

    private void checkPasswordAndSendToAddPage(HttpServletRequest request, UserDTO userDTO) throws Exception {
        boolean isLangEng = userDTO.languageID == CommonConstant.Language_ID_English;
        long budgetSelectionId = Long.parseLong(
                Jsoup.clean(request.getParameter("budgetSelectionId"), Whitelist.simpleText())
        );
        long budgetOfficeID = Long.parseLong(
                Jsoup.clean(request.getParameter("budgetOfficeId"), Whitelist.simpleText())
        );
        String password = Jsoup.clean(request.getParameter("password"), Whitelist.simpleText());

        Budget_passwordDTO passwordDTO = Budget_passwordDAO.getInstance().getDTOBySelectionIdOfficeId(budgetSelectionId, budgetOfficeID);
        if (passwordDTO == null) {
            throw new Exception(isLangEng ? "Budget info is not found for your selected office" : "আপনার নির্বাচিত অফিসের জন্য বাজেটের তথ্য পাওয়া যায় নাই");
        }

        if (passwordDTO.employeeRecordId != userDTO.employee_record_id) {
            throw new Exception(isLangEng ? "You are not authorized employee to submit budget for selected office" :
                    "আপনার নির্বাচিত অফিসের জন্য বাজেটের তথ্য প্রদানের অনুমতি নেই");
        }
        if (!passwordDTO.password.equals(password)) {
            throw new Exception(isLangEng ? "Your entered password is wrong" : "আপনার প্রদানকৃত পাসওয়ার্ডটি ভুল");
        }

        request.getSession(true).setAttribute(
                BudgetSubmissionSession.SESSION_VARIABLE_NAME,
                new BudgetSubmissionSession(userDTO.ID, budgetOfficeID)
        );
    }

    private boolean saveBudgetForOffice(UserDTO userDTO, HttpServletRequest request) throws Exception {
        Long budgetOfficeId = findOfficeIdOfUser(request);
        if (budgetOfficeId == null) throw new Exception("Unauthorised User");

        BudgetInfo budgetInfo = BudgetUtils.getBudgetInfoForBudgetSubmission(System.currentTimeMillis());
        BudgetSelectionInfoDTO budgetSelectionInfoDTO = BudgetSelectionInfoDAO.getInstance().getDTOByEconomicYear(budgetInfo.economicYear);
        boolean isCodeSelectionDone = budgetSelectionInfoDTO != null && budgetSelectionInfoDTO.isSubmitted;
        if (!isCodeSelectionDone) {
            logger.error("Budget Code Selection has not yet been completed for economic year ".concat(budgetInfo.economicYear));
            return false;
        }

        int budgetTypeCat = budgetInfo.budgetTypeEnum.getValue();
        Budget_submission_infoDTO budget_submission_infoDTO = Budget_submission_infoDAO.getInstance()
                .getOrCreateAndGetDTO(
                        budgetSelectionInfoDTO.iD,
                        budgetTypeCat,
                        budgetOfficeId,
                        userDTO.ID
                );

        boolean isBudgetSubmitted = budget_submission_infoDTO != null
                && budget_submission_infoDTO.isSubmit == SubmissionStatusEnum.SUBMITTED.getValue();
        if (isBudgetSubmitted) {
            logger.error("Office has already submitted budget");
            return false;
        }

        String Value = request.getParameter("budgetModels");
        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }
        BudgetModel[] budgetModels = gson.fromJson(Value, BudgetModel[].class);
        for (BudgetModel budgetModel : budgetModels) {
            updateBudget(budgetModel, budgetTypeCat, userDTO.ID, System.currentTimeMillis());
        }
        return true;
    }

    private void submitBudgetForOffice(HttpServletRequest request, UserDTO userDTO) throws Exception {
        Long budgetOfficeId = findOfficeIdOfUser(request);
        if (budgetOfficeId == null) throw new Exception("Unauthorised User.");

        BudgetInfo budgetInfo = BudgetUtils.getBudgetInfoForBudgetSubmission(System.currentTimeMillis());
        BudgetSelectionInfoDTO budgetSelectionInfoDTO = BudgetSelectionInfoDAO.getInstance().getDTOByEconomicYear(budgetInfo.economicYear);
        boolean isCodeSelectionDone = budgetSelectionInfoDTO != null && budgetSelectionInfoDTO.isSubmitted;
        if (!isCodeSelectionDone) {
            throw new Exception("Budget Code Selection has not yet been completed for economic year ".concat(budgetInfo.economicYear));
        }

        Budget_submission_infoDAO.getInstance().submitBudget(
                budgetSelectionInfoDTO.iD,
                budgetOfficeId,
                budgetInfo.budgetTypeEnum.getValue(),
                userDTO.ID
        );
    }

    private void updateBudget(BudgetModel model, int budgetTypeCat, long requestBy, long requestTime) {
        if (budgetTypeCat == BudgetTypeEnum.BUDGET.getValue()) {
            budgetDAO.updateAmountById(model, requestBy, requestTime);
        } else if (budgetTypeCat == BudgetTypeEnum.REVISED_BUDGET.getValue()) {
            budgetDAO.updateRevisedAmountById(model, requestBy, requestTime);
        }
    }
}