package test_lib;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class RQueryResultParse<T> {
    private T instance;
    private ResultSet resultSet;

    public RQueryResultParse() {
    }

    public boolean parse(T instance, ResultSet resultSet, ArrayList<Tupple> setterMethod) {
        boolean ret = true;
        this.instance = instance;
        this.resultSet = resultSet;
        try {
            for (Tupple tupple : setterMethod) {
                Method setMethod = (Method) tupple.get(0);
                String variableName = (String) tupple.get(1);
                Type dataType = (Type) tupple.get(2);
                ret ^= this.invokeData(setMethod, variableName, dataType);
            }
        } catch (Exception ignore) {
            return false;
        }
        return ret;
    }

    private boolean invokeData(Method setMethod, String variableName, Type dataType) {
        if (dataType == int.class) {
            try {
                int value = resultSet.getInt(variableName);
                setMethod.invoke(instance, value);
                return true;
            } catch (IllegalAccessException | InvocationTargetException | SQLException e) {
                System.out.println("\n--------------------------------------- Don't Worry ---------------------------------- " + variableName);
                e.printStackTrace();
                return false;
            }
        } else if (dataType == long.class) {
            try {
                long value = resultSet.getLong(variableName);
                setMethod.invoke(instance, value);
                return true;
            } catch (IllegalAccessException | InvocationTargetException | SQLException e) {
                System.out.println("\n--------------------------------------- Don't Worry ---------------------------------- " + variableName);
                e.printStackTrace();
                return false;
            }
        } else if (dataType == boolean.class) {
            try {
                boolean value = resultSet.getBoolean(variableName);
                setMethod.invoke(instance, value);
                return true;
            } catch (IllegalAccessException | InvocationTargetException | SQLException e) {
                System.out.println("\n--------------------------------------- Don't Worry ---------------------------------- " + variableName);
                e.printStackTrace();
                return false;
            }
        } else if (dataType == String.class) {
            try {
                String value = resultSet.getString(variableName);
                setMethod.invoke(instance, value);
                return true;
            } catch (IllegalAccessException | InvocationTargetException | SQLException e) {
                System.out.println("\n--------------------------------------- Don't Worry ----------------------------------");
                e.printStackTrace();
                return false;
            }
        }
        return true;
    }
}
