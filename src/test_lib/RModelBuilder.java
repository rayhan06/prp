package test_lib;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.ArrayList;

public class RModelBuilder<T> {
    private HttpServletRequest request;
    private Class<T> className;

    public RModelBuilder<T> of(Class<T> className) {
        this.className = className;
        return this;
    }

    public RModelBuilder<T> request(HttpServletRequest request) {
        this.request = request;
        return this;
    }

    public T buildModel() throws IllegalAccessException, InstantiationException {
        ClassInfoGenerator<T> tClassInfoGenerator = new ClassInfoGenerator<>(this.className);
        ArrayList<Tupple> setterMethod = tClassInfoGenerator.getSetterMethods();

        T instance = this.className.newInstance();

        for (Tupple obj : setterMethod) {
            Method method = (Method) obj.get(0);
            String variable = obj.get(1).toString();
            Type type = (Type) obj.get(2);

            this.invokeData(method, variable, type, instance);
        }
        return instance;
    }

    private boolean invokeData(Method setMethod, String variableName, Type dataType, T instance) {
        if (dataType == int.class) {
            try {
                int value = RUtility.tryInt(this.request.getParameter(variableName), 0);
                setMethod.invoke(instance, value);
                return true;
            } catch (IllegalAccessException | InvocationTargetException e) {
                e.printStackTrace();
                return false;
            }
        } else if (dataType == long.class) {
            try {
                long value = RUtility.tryLong(this.request.getParameter(variableName), 0L);
                setMethod.invoke(instance, value);
                return true;
            } catch (IllegalAccessException | InvocationTargetException e) {
                e.printStackTrace();
                return false;
            }
        } else if (dataType == boolean.class) {
            try {
                boolean value = RUtility.tryBoolean(this.request.getParameter(variableName), false);
                setMethod.invoke(instance, value);
                return true;
            } catch (IllegalAccessException | InvocationTargetException e) {
                e.printStackTrace();
                return false;
            }
        } else if (dataType == String.class) {
            try {
                String value = (this.request.getParameter(variableName));
                setMethod.invoke(instance, value);
                return true;
            } catch (IllegalAccessException | InvocationTargetException e) {
                e.printStackTrace();
                return false;
            }
        }
        return true;
    }
}
