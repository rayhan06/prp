package test_lib;

import common.VbSequencerService;
import dbm.DBMW;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.ArrayList;

public class RInsertQueryBuilder<T> implements VbSequencerService {
    private Class<T> className;
    private T model;
    private long lastModificationTime = 0;

    public RInsertQueryBuilder<T> of(Class<T> className) {
        this.className = className;
        return this;
    }

    public RInsertQueryBuilder<T> model(T model) {
        this.model = model;
        return this;
    }

    public RInsertQueryBuilder updateVbSequence(long lastModificationTime) {
        this.lastModificationTime = lastModificationTime;
        return this;
    }

    public RInsertQueryBuilder() {
    }

    public boolean buildInsert() {
        ArrayList<T> resultList = new ArrayList<>();

        ClassInfoGenerator<T> tClassInfoGenerator = new ClassInfoGenerator<>(this.className);
        ArrayList<Tupple> data = tClassInfoGenerator.getSetterMethods();

        StringBuilder sql = new StringBuilder("insert into " + this.className.getSimpleName() + " (");

        for (int i = 0; i < data.size(); i++) {
            Tupple obj = data.get(i);
            if (i == 0) sql.append(obj.get(1).toString());
            else sql.append(", ").append(obj.get(1).toString());
        }

        sql.append(") values (");
        for (int i = 0; i < data.size(); i++) {
            if (i == 0) sql.append("?");
            else sql.append(", ?");
        }
        sql.append(")");

        this.build(sql.toString(), data);

        return true;
    }

    private void build(String sql, ArrayList<Tupple> data) {
        Connection connection = null;
        PreparedStatement ps = null;

        try {
            connection = DBMW.getInstance().getConnection();

            ps = connection.prepareStatement(sql);
            for (int i = 0; i < data.size(); i++) {
                Tupple obj = data.get(i);

                Method setterMethod = (Method) obj.get(0);
                String variable = obj.get(1).toString();
                Type type = (Type) obj.get(2);
                Method getterMethod = (Method) obj.get(3);

                Object ob = invokeData(getterMethod, variable, type, this.model);
                ps.setObject(i + 1, ob);
            }
            ps.execute();

            if (lastModificationTime > 0) {
                updateVbSequence(connection, this.lastModificationTime);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception ignored) {
            }
            try {
                if (connection != null) {
                    DBMW.getInstance().freeConnection(connection);
                }
            } catch (Exception ignored) {
            }
        }
    }

    private void updateVbSequence(Connection connection, long lastModificationTime) {
        recordUpdateTime(connection,this.className.getSimpleName(),lastModificationTime);
    }

    private Object invokeData(Method getMethod, String variableName, Type dataType, T instance) {
        if (dataType == int.class) {
            try {
                return getMethod.invoke(instance);
            } catch (IllegalAccessException | InvocationTargetException e) {
                e.printStackTrace();
                return 0;
            }
        } else if (dataType == long.class) {
            try {
                return getMethod.invoke(instance);
            } catch (IllegalAccessException | InvocationTargetException e) {
                e.printStackTrace();
                return 0L;
            }
        } else if (dataType == boolean.class) {
            try {
                return getMethod.invoke(instance);
            } catch (IllegalAccessException | InvocationTargetException e) {
                e.printStackTrace();
                return false;
            }
        } else if (dataType == String.class) {
            try {
                return getMethod.invoke(instance);
            } catch (IllegalAccessException | InvocationTargetException e) {
                e.printStackTrace();
                return null;
            }
        }
        return true;
    }
}

