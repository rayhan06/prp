package test_lib;

import org.json.JSONObject;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;

@WebServlet("/TestServlet")
@MultipartConfig
public class TestServlet extends HttpServlet {

    public TestServlet() {
        super();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        String actionType = request.getParameter("actionType");

        if (actionType.equals("first")) {
            try {
                System.out.println("------------------------ Thread Slow ----------------------" + Thread.currentThread().getName());
                Thread.sleep(2000);
                response.setContentType("application/json");
                HashMap<String, Boolean> a = new HashMap<String, Boolean>();
                a.put("success", true);

                String res = new JSONObject(a).toString();
                PrintWriter out = response.getWriter();

                out.print(res);
                out.flush();
            } catch (InterruptedException | IOException e) {
                e.printStackTrace();
            }

        } else {
            try {
                String sql = "select * from employee_records limit 10";
                RQueryBuilder<EmployeeRecordDTO> rQueryBuilder = new RQueryBuilder<>();
                String data = rQueryBuilder.setSql(sql).of(EmployeeRecordDTO.class).buildJson();

                System.out.println("------------------------ Thread Fast ----------------------" + Thread.currentThread().getName());
                response.setContentType("application/json");
                HashMap<String, String> a = new HashMap<>();
                a.put("success", data);

                String res = new JSONObject(a).toString();
                PrintWriter out = response.getWriter();

                out.print(data);
                out.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
