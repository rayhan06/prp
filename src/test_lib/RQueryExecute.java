package test_lib;

import dbm.DBMR;
import dbm.DBMW;
import org.jdom.JDOMException;

import java.sql.*;

public class RQueryExecute {

    private Connection connection = null;
    private ResultSet rs = null;
    private Statement stmt = null;
    private PreparedStatement ps = null;

    public RQueryExecute() {
    }

    public boolean build(String sql, IQueryExecuteResult iQueryResult) {
        try {
            this.connection = DBMR.getInstance().getConnection();
            this.stmt = connection.createStatement();
            this.rs = stmt.executeQuery(sql);
            if (!this.rs.next()) {
                if (iQueryResult != null) {
                    iQueryResult.getResult(true, null);
                }
                return true;
            }
            do {
                if (iQueryResult != null) {
                    iQueryResult.getResult(false, this.rs);
                }
            } while (this.rs.next());
        } catch (SQLException | ClassNotFoundException | IllegalAccessException | InstantiationException e) {
            e.printStackTrace();
            return false;
        } finally {
            try {
                DBMR.getInstance().freeConnection(connection);
            } catch (SQLException | ClassNotFoundException | IllegalAccessException | InstantiationException e) {
                e.printStackTrace();
            }
        }
        return true;
    }

    public boolean update(String sql) {
        try {
            this.connection = DBMW.getInstance().getConnection();
            ps = connection.prepareStatement(sql);
            ps.execute();
            ps.close();
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | SQLException e) {
            if (this.connection != null) {
                try {
                    this.connection.close();
                    ps.close();
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
            }
            e.printStackTrace();
            return false;
        } finally {
            try {
                DBMW.getInstance().freeConnection(connection);
            } catch (SQLException | InstantiationException | IllegalAccessException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
        return true;
    }
}
