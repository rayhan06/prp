package test_lib;

import test_lib.util.Pair;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.ArrayList;

public class ClassInfoGenerator<T> {
    private final Class<T> className;

    public ClassInfoGenerator(Class<T> className) {
        this.className = className;
    }

    private ArrayList<Pair<String, Type>> getFieldAndTypes() {
        ArrayList<Pair<String, Type>> variableAndTypes = new ArrayList<>();
        for (Field field : className.getDeclaredFields()) {
            field.setAccessible(true);
            String name = field.getName();
            Type type = field.getGenericType();
            variableAndTypes.add(new Pair<>(name, type));
        }
        return variableAndTypes;
    }

    public ArrayList<Tupple> getSetterMethods() {
        ArrayList<Tupple> result = new ArrayList<>();
        ArrayList<Pair<String, Type>> fields = this.getFieldAndTypes();
        try {
            for (Pair<String, Type> field : fields) {
                Method setMethod = className.getDeclaredMethod(getSetterMethodMane(field.getKey()), (Class<?>) field.getValue());
                Method getMethod = className.getDeclaredMethod(getGetterMethodMane(field.getKey()));
                // setterMethod, variableName, variableType
                result.add(Tupple.create(setMethod, field.getKey(), field.getValue(), getMethod));
            }
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        return result;
    }

    private String getSetterMethodMane(String name) {
        name = this.capitalizeFirstLetter(name);
        return "set" + name;
    }

    private String getGetterMethodMane(String name) {
        name = this.capitalizeFirstLetter(name);
        return "get" + name;
    }

    private String capitalizeFirstLetter(String name) {
        if (name != null && name.length() > 0)
            return name.substring(0, 1).toUpperCase() + name.substring(1);
        return name;
    }
}
