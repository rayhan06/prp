package test_lib;

import dbm.DBMW;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class RQueryExecutor {

    public String execute(String sql) throws SQLException {
        Connection connection = null;
        Statement stmt = null;
        int[] recordsAffected;
        String[] sqlQueries = sql.split(";");

        try {
            connection = DBMW.getInstance().getConnection();
            stmt = connection.createStatement();
            connection.setAutoCommit(false);

            try {
                for (String s : sqlQueries) {
                    System.out.println("Batch update: " + s);
                    stmt.addBatch(s);
                }

                recordsAffected = stmt.executeBatch();
            } catch (Exception var22) {
                connection.rollback();
                return ("{" + "\"success\"" + ":" + "false," + "\"msg\"" + ":" + var22.getMessage() + "}");
            } finally {
                connection.commit();
                connection.setAutoCommit(true);
            }
        } catch (Exception e1) {
            e1.printStackTrace();
            return ("{" + "\"success\"" + ":" + "false," + "\"msg\"" + ":" + e1.getMessage() + "}");
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception e2) {
                    e2.printStackTrace();
                    return ("{" + "\"success\"" + ":" + "false," + "\"msg\"" + ":" + e2.getMessage() + "}");
                }
            }

            if (connection != null) {
                connection.setAutoCommit(true);
                try {
                    DBMW.getInstance().freeConnection(connection);
                } catch (ClassNotFoundException | IllegalAccessException | InstantiationException e) {
                    e.printStackTrace();
                    return ("{" + "\"success\"" + ":" + "false," + "\"msg\"" + ":" + e.getMessage() + "}");
                }
            }
        }
        return ("{" + "\"success\"" + ":" + "true," + "\"msg\"" + ":" + recordsAffected.length + "}");
    }
}
