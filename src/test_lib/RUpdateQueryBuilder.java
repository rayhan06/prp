package test_lib;

import dbm.DBMW;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;

public class RUpdateQueryBuilder<T> {
    private Class<T> className;
    private T model;

    public RUpdateQueryBuilder<T> of(Class<T> className) {
        this.className = className;
        return this;
    }

    public RUpdateQueryBuilder<T> model(T model) {
        this.model = model;
        return this;
    }

    public RUpdateQueryBuilder() {
    }

    public boolean buildUpdate(int id) {
        ArrayList<T> resultList = new ArrayList<>();

        ClassInfoGenerator<T> tClassInfoGenerator = new ClassInfoGenerator<>(this.className);
        ArrayList<Tupple> data = tClassInfoGenerator.getSetterMethods();

        Connection connection = null;
        PreparedStatement ps;

        StringBuilder sql = new StringBuilder("update " + this.className.getSimpleName() + " set ");

        for (int i = 0; i < data.size(); i++) {
            Tupple obj = data.get(i);

            Method setterMethod = (Method) obj.get(0);
            String variable = obj.get(1).toString();
            Type type = (Type) obj.get(2);
            Method getterMethod = (Method) obj.get(3);

            if (i == 0) sql.append(variable).append("=?");
            else sql.append(", ").append(variable).append("=?");
        }

        sql.append(" where id = ").append(id);

        try {
            connection = DBMW.getInstance().getConnection();
            ps = connection.prepareStatement(sql.toString());


            for (int i = 0; i < data.size(); i++) {
                Tupple obj = data.get(i);
                Method setterMethod = (Method) obj.get(0);
                String variable = obj.get(1).toString();
                Type type = (Type) obj.get(2);
                Method getterMethod = (Method) obj.get(3);

                Object ob = this.invokeData(getterMethod, variable, type, this.model);

                ps.setObject(i + 1, ob);
            }

            ps.executeUpdate();
        } catch (SQLException | ClassNotFoundException | IllegalAccessException | InstantiationException e) {
            e.printStackTrace();
        } finally {
            try {
                DBMW.getInstance().freeConnection(connection);
            } catch (SQLException | ClassNotFoundException | InstantiationException | IllegalAccessException e) {
                e.printStackTrace();
            }
        }

        return true;
    }

    private Object invokeData(Method getMethod, String variableName, Type dataType, T instance) {
        if (dataType == int.class) {
            try {
                return getMethod.invoke(instance);
            } catch (IllegalAccessException | InvocationTargetException e) {
                e.printStackTrace();
                return 0;
            }
        } else if (dataType == long.class) {
            try {
                return getMethod.invoke(instance);
            } catch (IllegalAccessException | InvocationTargetException e) {
                e.printStackTrace();
                return 0L;
            }
        } else if (dataType == boolean.class) {
            try {
                return getMethod.invoke(instance);
            } catch (IllegalAccessException | InvocationTargetException e) {
                e.printStackTrace();
                return false;
            }
        } else if (dataType == String.class) {
            try {
                return getMethod.invoke(instance);
            } catch (IllegalAccessException | InvocationTargetException e) {
                e.printStackTrace();
                return null;
            }
        }
        return true;
    }
}
