package test_lib;

import java.sql.ResultSet;

public interface IQueryExecuteResult {
    void getResult(boolean noDataFound, ResultSet resultSet) throws IllegalAccessException, InstantiationException;
}
