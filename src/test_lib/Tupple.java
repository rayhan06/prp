package test_lib;

import java.util.ArrayList;
import java.util.Collections;

public class Tupple<T> {
    private int length = 0;
    private final ArrayList<T> tuppleList;

    @SafeVarargs
    public Tupple(T... var) {
        this.length = (var != null && var.length > 0) ? var.length : 0;
        this.tuppleList = new ArrayList<>(this.length);
        this.invokeInsert(var);
    }

    @SafeVarargs
    public static <T> Tupple<T> create(T... var) {
        return new Tupple<>(var);
    }

    @SafeVarargs
    private final void invokeInsert(T... var) {
        if (var != null) {
            Collections.addAll(this.tuppleList, var);
        }
    }

    public int size() {
        return this.length;
    }

    public T get(int index) {
        if (!(index >= 0 && index < this.length)) throw new IndexOutOfBoundsException("Index OutOfBounds Exception");
        return this.tuppleList.get(index);
    }

    public boolean equals(Tupple<T> obj) {
        if (this == obj) return true;
        if (this.size() == obj.size()) {
            for (int i = 0; i < size(); i++) {
                if (this.get(i) != obj.get(i)) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }
}
