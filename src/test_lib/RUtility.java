package test_lib;

public class RUtility {
    public static int tryInt(String val, int defaultValue) {
        try {
            return Integer.parseInt(val);
        } catch (Exception ignore) {
        }
        return defaultValue;
    }

    public static double tryDouble(String val, double defaultValue) {
        try {
            return Double.parseDouble(val);
        } catch (Exception ignore) {
        }
        return defaultValue;
    }

    public static long tryLong(String val, long defaultValue) {
        try {
            return Long.parseLong(val);
        } catch (Exception ignore) {
        }
        return defaultValue;
    }

    public static boolean tryBoolean(String val, boolean defaultValue) {
        try {
            return Boolean.parseBoolean(val);
        } catch (Exception ignore) {
        }
        return defaultValue;
    }
}
