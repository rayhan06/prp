package test_lib;

import com.google.gson.Gson;

import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.*;

public class RQueryBuilder<T> {
    private final String empty = ("");
    private String sql;
    private Class<T> className;

    public RQueryBuilder<T> setSql(String sql) {
        this.sql = sql;
        return this;
    }

    public RQueryBuilder<T> of(Class<T> className) {
        this.className = className;
        return this;
    }

    public RQueryBuilder() {
    }

    public String buildJson() {
        ArrayList<T> resultList = new ArrayList<>();

        ClassInfoGenerator<T> tClassInfoGenerator = new ClassInfoGenerator<>(this.className);
        ArrayList<Tupple> setterMethod = tClassInfoGenerator.getSetterMethods();

        RQueryExecute rQueryExecute = new RQueryExecute();
        rQueryExecute.build(this.sql, (noDataFound, resultSet) -> {
            if (noDataFound) {
                System.out.println("No Data Found");
            } else {
                T instance = this.className.newInstance();
                RQueryResultParse<T> rQueryResultParse = new RQueryResultParse<T>();
                rQueryResultParse.parse(instance, resultSet, setterMethod);
                resultList.add(instance);
            }
        });
        if (resultList.size() == 0) return empty;
        return new Gson().toJson(resultList);
    }

    public ArrayList<T> buildRaw() {
        ArrayList<T> resultList = new ArrayList<>();

        ClassInfoGenerator<T> tClassInfoGenerator = new ClassInfoGenerator<>(this.className);
        ArrayList<Tupple> setterMethod = tClassInfoGenerator.getSetterMethods();

        RQueryExecute rQueryExecute = new RQueryExecute();
        rQueryExecute.build(this.sql, (noDataFound, resultSet) -> {
            if (noDataFound) {
                System.out.println("No Data Found");
            } else {
                T instance = this.className.newInstance();
                RQueryResultParse<T> rQueryResultParse = new RQueryResultParse<T>();
                rQueryResultParse.parse(instance, resultSet, setterMethod);
                resultList.add(instance);
            }
        });
        if (resultList.size() == 0) return null;
        return resultList;
    }

    public ArrayList<HashMap> buildHashMap() {
        ArrayList<HashMap> result = new ArrayList<>();

        RQueryExecute rQueryExecute = new RQueryExecute();
        rQueryExecute.build(this.sql, (noDataFound, resultSet) -> {
            if (noDataFound) {
                System.out.println("No Data Found");
            } else {
                ResultSetMetaData md = null;
                try {
                    md = resultSet.getMetaData();
                    int columns = md.getColumnCount();

                    HashMap<String, Object> row = new HashMap<String, Object>(columns);
                    for (int i = 1; i <= columns; i++) {
                        row.put(md.getColumnName(i), resultSet.getObject(i));
                    }
                    result.add(row);
                } catch (SQLException e) {
                    System.out.println("Don't worry");
                    e.printStackTrace();
                }
            }
        });
        return result;
    }

    public boolean buildUpdate() {
        RQueryExecute rQueryExecute = new RQueryExecute();
        return rQueryExecute.update(this.sql);
    }
}
