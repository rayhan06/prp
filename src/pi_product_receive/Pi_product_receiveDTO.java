package pi_product_receive;

import util.CommonDTO;

public class Pi_product_receiveDTO extends CommonDTO {
    public long piPurchaseParentId = -1;
    public String purchaseOrderNumber = "";
    public long purchaseDate = 0;
    public long officeUnitId = -1;
    public long fiscalYearId = -1;
    public long packageId = -1;
    public long lotId = -1;
    public long vendorId = -1;
    public int procurementMethod = -1;
    public String sarokNumber = "";
    public String goodReceivingNumber = "";
    public long receiveDate = -1;
    public String remarks  = "";
    public long filesDropZone = -1;
    public long insertedBy = -1;
    public long insertionTime = -1;
    public long modifiedBy = -1;

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Pi_product_receiveDTO{");
        sb.append("piPurchaseParentId=").append(piPurchaseParentId);
        sb.append(", purchaseOrderNumber='").append(purchaseOrderNumber).append('\'');
        sb.append(", purchaseDate=").append(purchaseDate);
        sb.append(", officeUnitId=").append(officeUnitId);
        sb.append(", fiscalYearId=").append(fiscalYearId);
        sb.append(", packageId=").append(packageId);
        sb.append(", lotId=").append(lotId);
        sb.append(", vendorId=").append(vendorId);
        sb.append(", procurementMethod=").append(procurementMethod);
        sb.append(", sarokNumber='").append(sarokNumber).append('\'');
        sb.append(", receiveDate=").append(receiveDate);
        sb.append(", remarks='").append(remarks).append('\'');
        sb.append(", filesDropZone=").append(filesDropZone);
        sb.append(", insertedBy=").append(insertedBy);
        sb.append(", insertionTime=").append(insertionTime);
        sb.append(", modifiedBy=").append(modifiedBy);
        sb.append('}');
        return sb.toString();
    }
}
