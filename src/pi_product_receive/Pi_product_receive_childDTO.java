package pi_product_receive;

import util.CommonDTO;

public class Pi_product_receive_childDTO extends CommonDTO {
    public long piPurchaseParentId = -1;
    public long piPurchaseId = -1;
    public long piProductReceiveId = -1;
    public long officeUnitId = -1;
    public long fiscalYearId = -1;
    public long allocatedAppId = -1;
    public long packageId = -1;
    public long lotId = -1;
    public long vendorId = -1;
    public String sarokNumber = "";
    public String goodReceivingNumber = "";
    public long receiveDate = -1;
    public long itemGroupId = -1;
    public long itemTypeId = -1;
    public long itemId = -1;
    public double quantity = -1;
    public double unitPrice = -1;
    public double totalPrice = -1;
    public long expiryDate = -1;
    public String remarks = "";
    public long insertedBy = -1;
    public long insertionTime = -1;
    public long modifiedBy = -1;

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Pi_product_receive_childDTO{");
        sb.append("piProductReceiveId=").append(piProductReceiveId);
        sb.append(", officeUnitId=").append(officeUnitId);
        sb.append(", fiscalYearId=").append(fiscalYearId);
        sb.append(", allocatedAppId=").append(allocatedAppId);
        sb.append(", packageId=").append(packageId);
        sb.append(", lotId=").append(lotId);
        sb.append(", vendorId=").append(vendorId);
        sb.append(", sarokNumber='").append(sarokNumber).append('\'');
        sb.append(", goodReceivingNumber='").append(goodReceivingNumber).append('\'');
        sb.append(", receiveDate=").append(receiveDate);
        sb.append(", itemGroupId=").append(itemGroupId);
        sb.append(", itemTypeId=").append(itemTypeId);
        sb.append(", itemId=").append(itemId);
        sb.append(", quantity=").append(quantity);
        sb.append(", unitPrice=").append(unitPrice);
        sb.append(", totalPrice=").append(totalPrice);
        sb.append(", expiryDate=").append(expiryDate);
        sb.append(", remarks='").append(remarks).append('\'');
        sb.append(", insertedBy=").append(insertedBy);
        sb.append(", insertionTime=").append(insertionTime);
        sb.append(", modifiedBy=").append(modifiedBy);
        sb.append('}');
        return sb.toString();
    }
}
