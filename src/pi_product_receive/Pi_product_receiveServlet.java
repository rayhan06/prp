package pi_product_receive;

import common.BaseServlet;
import common.CommonDAOService;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import pb.Utils;
import permission.MenuConstants;
import pi_purchase_parent.Pi_purchase_parentDTO;
import pi_purchase_parent.Pi_purchase_parentRepository;
import pi_unique_item.*;
import pi_vendor_auctioneer_details.Pi_vendor_auctioneer_detailsRepository;
import procurement_goods.Procurement_goodsDTO;
import procurement_goods.Procurement_goodsRepository;
import sessionmanager.SessionConstants;
import user.UserDTO;
import util.CommonDTO;
import util.HttpRequestUtils;
import util.UtilCharacter;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@WebServlet("/Pi_product_receiveServlet")
@MultipartConfig
public class Pi_product_receiveServlet extends BaseServlet {
    private final Logger logger = Logger.getLogger(Pi_product_receiveServlet.class);

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        String language = userDTO.languageID == SessionConstants.ENGLISH ? "English" : "Bangla";
        SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
        Pi_product_receiveDTO productReceiveDTO;
        if (addFlag) {
            productReceiveDTO = new Pi_product_receiveDTO();
            productReceiveDTO.insertedBy = productReceiveDTO.modifiedBy = userDTO.employee_record_id;
            productReceiveDTO.insertionTime = productReceiveDTO.lastModificationTime = System.currentTimeMillis();
        } else {
            String id = request.getParameter("ID");
            productReceiveDTO = Pi_product_receiveDAO.getInstance().getDTOByID(Long.parseLong(id));
            productReceiveDTO.modifiedBy = userDTO.employee_record_id;
            productReceiveDTO.lastModificationTime = System.currentTimeMillis();
        }

        String value = "";
        value = request.getParameter("fiscalYearSelect");
        if (isValueValid(value)) {
            value = Jsoup.clean(value, Whitelist.simpleText());
            productReceiveDTO.fiscalYearId = Long.parseLong(value);
        } else {
            UtilCharacter.throwException("অর্থবছর নির্বাচন করুন", "Please Select Fiscal Year");
        }


        value = request.getParameter("packageSelect");
        if (isValueValid(value)) {
            value = Jsoup.clean(value, Whitelist.simpleText());
            productReceiveDTO.packageId = Long.parseLong(value);
        } else {
            UtilCharacter.throwException("প্যাকেজ নির্বাচন করুন", "Please Select Package");
        }


        value = request.getParameter("lotSelect");
        if (isValueValid(value)) {
            value = Jsoup.clean(value, Whitelist.simpleText());
            productReceiveDTO.lotId = Long.parseLong(value);
        }


        value = request.getParameter("purchaseOrderNumberSelect");
        if (isValueValid(value)) {
            value = Jsoup.clean(value, Whitelist.simpleText());
            productReceiveDTO.piPurchaseParentId = Long.parseLong(value);
            bindPurchaseDate(productReceiveDTO, productReceiveDTO.piPurchaseParentId);
        } else {
            UtilCharacter.throwException("ক্রয়ের আদেশ নম্বর নির্বাচন করুন", "Please Select Purchase Order Number");
        }


        value = request.getParameter("purchaseOrderNumberText");
        if (isValueValid(value)) {
            value = Jsoup.clean(value, Whitelist.simpleText());
            productReceiveDTO.purchaseOrderNumber = value;
        }


        // HERE, OFFICE UNIT = STORE OFFICE UNIT
        value = request.getParameter("storeOfficeId");
        if (isValueValid(value)) {
            value = Jsoup.clean(value, Whitelist.simpleText());
            productReceiveDTO.officeUnitId = Long.parseLong(value);
        }


        value = request.getParameter("vendorId");
        if (isValueValid(value)) {
            value = Jsoup.clean(value, Whitelist.simpleText());
            productReceiveDTO.vendorId = Long.parseLong(value);
        }


        value = request.getParameter("receiveDate");
        if (isValueValid(value)) {
            value = Jsoup.clean(value, Whitelist.simpleText());
            Date d = f.parse(value);
            productReceiveDTO.receiveDate = d.getTime();
        } else {
            UtilCharacter.throwException("মালামাল গ্রহণের তারিখ দিন", "Please Enter Item Receive Date");
        }


        value = request.getParameter("sarokNumber");
        if (isValueValid(value)) {
            value = Jsoup.clean(value, Whitelist.simpleText());
            productReceiveDTO.sarokNumber = value;
        }


        value = request.getParameter("goodReceivingNumber");
        if (isValueValid(value)) {
            value = Jsoup.clean(value, Whitelist.simpleText());
            value = removeAllSpaces(value);
            validateGoodReceivingNumber(value);
            productReceiveDTO.goodReceivingNumber = value;
        } else {
            UtilCharacter.throwException("মালামাল গ্রহণের নম্বর লিখুন", "Please Enter Good Receiving Number");
        }


        value = request.getParameter("globalRemark");
        if (isValueValid(value)) {
            value = Jsoup.clean(value, Whitelist.simpleText());
            productReceiveDTO.remarks = value;
        }


        value = request.getParameter("filesDropzone");
        if (isValueValid(value)) {
            value = Jsoup.clean(value, Whitelist.simpleText());
            productReceiveDTO.filesDropZone = Long.parseLong(value);
            if (!addFlag) {
                String filesDropzoneFilesToDelete = request.getParameter("filesDropzoneFilesToDelete");
                String[] deleteArray = filesDropzoneFilesToDelete.split(",");
                for (int i = 0; i < deleteArray.length; i++) {
                    logger.debug("going to delete " + deleteArray[i]);
                    if (i > 0) {
                        filesDAO.delete(Long.parseLong(deleteArray[i]));
                    }
                }
            }
        }

        Utils.handleTransaction(() -> {
            long returnedId;
            if (addFlag) {
                returnedId = Pi_product_receiveDAO.getInstance().add(productReceiveDTO);
                productReceiveDTO.iD = returnedId;
            } else {
                Pi_product_receiveDAO.getInstance().update(productReceiveDTO);
            }

            List<Pi_product_receive_childDTO> newChildModels = getItemsData(request, productReceiveDTO);
            List<Pi_product_receive_childDTO> oldChildModels = (List<Pi_product_receive_childDTO>) Pi_product_receive_childDAO.getInstance()
                    .getDTOsByParent("pi_product_receive_id", productReceiveDTO.iD);
            // IDS WITH -1 ARE FOR ADD
            List<Pi_product_receive_childDTO> childModelsToAdd = newChildModels.stream().filter(dto -> dto.iD == -1).collect(Collectors.toList());
            // COMMON DTOS ARE FOR EDIT
            List<Pi_product_receive_childDTO> childModelsToEdit = newChildModels.stream().filter(dto -> fieldContains(oldChildModels, dto.iD)).collect(Collectors.toList());
            // IDS NOT COMMON AND NOT -1 ARE FOR DELETE
            List<Pi_product_receive_childDTO> childModelsToDelete = oldChildModels.stream().filter(dto -> !fieldContains(childModelsToEdit, dto.iD)).collect(Collectors.toList());

            for (Pi_product_receive_childDTO dto : childModelsToAdd) {
                dto.insertedBy = dto.modifiedBy = userDTO.employee_record_id;
                dto.insertionTime = dto.lastModificationTime = System.currentTimeMillis();
                Pi_product_receive_childDAO.getInstance().add(dto);
                populateItemsToUniqueItemAndAssignmentTable(dto, productReceiveDTO, userDTO);
                Pi_unique_item_transactionDAO.getInstance().recordItemTransactionAfterProductReceive(dto, productReceiveDTO, userDTO);
            }
            for (Pi_product_receive_childDTO dto : childModelsToEdit) {
                dto.modifiedBy = userDTO.employee_record_id;
                dto.lastModificationTime = System.currentTimeMillis();
                Pi_product_receive_childDAO.getInstance().update(dto);
            }
            for (Pi_product_receive_childDTO dto : childModelsToDelete) {
                Pi_product_receive_childDAO.getInstance().delete(userDTO.employee_record_id, dto.iD, System.currentTimeMillis());
            }
        });

        return productReceiveDTO;
    }

    private void bindPurchaseDate(Pi_product_receiveDTO productReceiveDTO, long piPurchaseParentId) {
        Pi_purchase_parentDTO dto = Pi_purchase_parentRepository.getInstance()
                .getPi_purchase_parentDTOByiD(piPurchaseParentId);
        productReceiveDTO.purchaseDate = dto.purchaseDate;
    }

    private List<Pi_product_receive_childDTO> getItemsData(HttpServletRequest request,
                                                           Pi_product_receiveDTO productReceiveDTO) throws Exception {
        List<Pi_product_receive_childDTO> childDTOs = new ArrayList<>();
        if (request.getParameterValues("productReceiveChildId") != null) {
            int itemCount = request.getParameterValues("productReceiveChildId").length;

            for (int index = 0; index < itemCount; index++) {
                Pi_product_receive_childDTO childDTO = getItemData(request, index);
                if (childDTO.quantity == 0) continue;
                childDTO.piProductReceiveId = productReceiveDTO.iD;
                childDTO.piPurchaseParentId = productReceiveDTO.piPurchaseParentId;
                childDTO.officeUnitId = productReceiveDTO.officeUnitId;
                childDTO.fiscalYearId = productReceiveDTO.fiscalYearId;
                childDTO.packageId = productReceiveDTO.packageId;
                childDTO.lotId = productReceiveDTO.lotId;
                childDTO.vendorId = productReceiveDTO.vendorId;
                childDTO.receiveDate = productReceiveDTO.receiveDate;
                childDTO.goodReceivingNumber = productReceiveDTO.goodReceivingNumber;
                childDTOs.add(childDTO);
            }
            // THIS EXCEPTION WILL ROLL BACK PARENT DATA INSERTION
            if (childDTOs.isEmpty()) {
                UtilCharacter.throwException("অনুগ্রহপূর্বক অন্তত একটা আইটেম গ্রহণ করুন!", "Please receive at least one item!");
            }
            return childDTOs;
        }
        return null;
    }

    private Pi_product_receive_childDTO getItemData(HttpServletRequest request, int index) {
        Pi_product_receive_childDTO childDTO = new Pi_product_receive_childDTO();

        String value = "";
        value = request.getParameterValues("productReceiveChildId")[index];
        if (isValueValid(value)) {
            childDTO.iD = Long.parseLong(value);
        } else {
            childDTO.iD = -1;
        }


        value = request.getParameterValues("piPurchaseId")[index];
        if (isValueValid(value)) {
            childDTO.piPurchaseId = Long.parseLong(value);
        } else {
            childDTO.piPurchaseId = -1;
        }


        value = request.getParameterValues("itemGroupId")[index];
        if (isValueValid(value)) {
            childDTO.itemGroupId = Long.parseLong(value);
        }


        value = request.getParameterValues("itemTypeId")[index];
        if (isValueValid(value)) {
            childDTO.itemTypeId = Long.parseLong(value);
        }


        value = request.getParameterValues("itemId")[index];
        if (isValueValid(value)) {
            childDTO.itemId = Long.parseLong(value);
        }


        value = request.getParameterValues("quantity")[index];
        if (isValueValid(value) && !value.equals("-1")) {
            childDTO.quantity = Long.parseLong(value);
        } else {
            childDTO.quantity = 0;
        }


        value = request.getParameterValues("remark")[index];
        if (isValueValid(value)) {
            childDTO.remarks = value;
        }

        return childDTO;
    }

    private void populateItemsToUniqueItemAndAssignmentTable(Pi_product_receive_childDTO childDTO,
                                                             Pi_product_receiveDTO parentDTO, UserDTO userDTO) throws Exception {
        Utils.handleTransaction(() -> {
            for (int i = 0; i < childDTO.quantity; i++) {
                Pi_unique_itemDTO uniqueItemDTO = new Pi_unique_itemDTO();
                uniqueItemDTO.officeUnitId = childDTO.officeUnitId;
                uniqueItemDTO.fiscalYearId = childDTO.fiscalYearId;
                uniqueItemDTO.itemId = childDTO.itemId;
                uniqueItemDTO.trackingNumber = childDTO.piPurchaseId + "-" + childDTO.iD + "-" + childDTO.itemId + "-" + i;
                uniqueItemDTO.piPurchaseId = childDTO.piPurchaseId;
                uniqueItemDTO.piReceivedId = childDTO.iD;
                uniqueItemDTO.status = 1;
                uniqueItemDTO.insertedBy = uniqueItemDTO.modifiedBy = userDTO.employee_record_id;
                uniqueItemDTO.insertionTime = uniqueItemDTO.lastModificationTime = System.currentTimeMillis();
                long uniqueItemId = Pi_unique_itemDAO.getInstance().add(uniqueItemDTO);

                PiUniqueItemAssignmentDTO uniqueItemAssignmentDTO = new PiUniqueItemAssignmentDTO();
                uniqueItemAssignmentDTO.piUniqueItemId = uniqueItemId;
                uniqueItemAssignmentDTO.officeUnitId = childDTO.officeUnitId;
                uniqueItemAssignmentDTO.fiscalYearId = childDTO.fiscalYearId;
                uniqueItemAssignmentDTO.fromDate = System.currentTimeMillis();
                uniqueItemAssignmentDTO.toDate = -1;
                uniqueItemAssignmentDTO.itemId = childDTO.itemId;
                /* Here PURCHASE means item RECEIVED from vendor */
                uniqueItemAssignmentDTO.actionType = PiActionTypeEnum.PURCHASE.getValue();
                uniqueItemAssignmentDTO.stage = PiStageEnum.IN_STOCK.getValue();
                uniqueItemAssignmentDTO.piPurchaseId = childDTO.piPurchaseId;
                uniqueItemAssignmentDTO.piReceivedId = childDTO.iD;
                uniqueItemAssignmentDTO.purchaseDate = parentDTO.purchaseDate;
                uniqueItemAssignmentDTO.receiveDate = parentDTO.receiveDate;
                uniqueItemAssignmentDTO.returnable = Procurement_goodsRepository.getInstance()
                        .getProcurement_goodsDTOByID(uniqueItemDTO.itemId).returnable;
                uniqueItemAssignmentDTO.insertedBy = uniqueItemAssignmentDTO.modifiedBy = userDTO.employee_record_id;
                uniqueItemAssignmentDTO.insertionTime = uniqueItemAssignmentDTO.lastModificationTime
                        = System.currentTimeMillis();
                long uniqueItemAssignmentId = PiUniqueItemAssignmentDAO.getInstance().add(uniqueItemAssignmentDTO);
            }
        });
    }

    private boolean fieldContains(List<Pi_product_receive_childDTO> childDTOs, Long id) {
        List<Pi_product_receive_childDTO> dto = childDTOs.stream().filter(model -> model.iD == id).collect(Collectors.toList());
        return dto.size() > 0;
    }

    private String removeAllSpaces(String str) {
        String trimmedStr = "";
        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) != ' ') trimmedStr += str.charAt(i);
        }
        return trimmedStr;
    }

    private void validateGoodReceivingNumber(String purchaseOrderNumber) throws Exception {
        List<Pi_product_receive_childDTO> models = Pi_product_receive_childDAO.getInstance()
                .getDTOsByGoodReceivingNumber(purchaseOrderNumber);
        if (!models.isEmpty()) {
            UtilCharacter.throwException("এই মালামাল গ্রহণ নম্বর ইতিমধ্যে সিস্টেমে বিদ্যমান!",
                    "This good receiving number already exist in the system");
        }
    }

    @Override
    public String getServletName() {
        return "Pi_product_receiveServlet";
    }

    @Override
    public CommonDAOService<? extends CommonDTO> getCommonDAOService() {
        return Pi_product_receiveDAO.getInstance();
    }

    @Override
    public String getTableName() {
        return getCommonDAOService().getTableName();
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[]{MenuConstants.PI_PRODUCT_RECEIVE_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[]{MenuConstants.PI_PRODUCT_RECEIVE_EDIT};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[]{MenuConstants.PI_PRODUCT_RECEIVE_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Pi_product_receiveServlet.class;
    }

    private boolean isValueValid(String value) {
        return value != null && !value.equals("");
    }
}
