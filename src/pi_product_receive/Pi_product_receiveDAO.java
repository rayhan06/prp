package pi_product_receive;

import common.CommonDAOService;
import org.apache.log4j.Logger;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Pi_product_receiveDAO implements CommonDAOService<Pi_product_receiveDTO> {
    private static final String getItemReceiveCountByPurchaseAndItem =
            "SELECT * FROM pi_product_receive_child WHERE  pi_purchase_parent_id = %d " +
                    "AND item_id = %d AND isDeleted = 0";

    private static final Logger logger = Logger.getLogger(Pi_product_receiveDAO.class);

    private static String addQuery = "";
    private static String updateQuery = "";
    private String[] columnNames = null;
    private final Map<String, String> searchMap = new HashMap<>();

    private Pi_product_receiveDAO() {
        columnNames = new String[]
                {
                        "ID",
                        "pi_purchase_parent_id",
                        "purchase_order_number",
                        "purchase_date",
                        "office_unit_id",
                        "fiscal_year_id",
                        "package_id",
                        "lot_id",
                        "vendor_id",
                        "procurement_method",
                        "sarok_number",
                        "good_receiving_number",
                        "receive_date",
                        "remarks",
                        "files_dropzone",
                        "search_column",
                        "inserted_by",
                        "insertion_time",
                        "isDeleted",
                        "modified_by",
                        "lastModificationTime",
                };
        updateQuery = getUpdateQuery(columnNames);
        addQuery = getAddQuery(columnNames);

        searchMap.put("pi_purchase_parent_id", " and (pi_purchase_parent_id = ?)");
        searchMap.put("office_unit_id", " and (office_unit_id = ?)");
        searchMap.put("fiscal_year_id", " and (fiscal_year_id = ?)");
    }

    private static class LazyLoader {
        static final Pi_product_receiveDAO INSTANCE = new Pi_product_receiveDAO();
    }

    public static Pi_product_receiveDAO getInstance() {
        return LazyLoader.INSTANCE;
    }

    private void setSearchColumn(Pi_product_receiveDTO pi_product_receiveDTO) {

    }

    @Override
    public void set(PreparedStatement ps, Pi_product_receiveDTO dto, boolean isInsert) throws SQLException {
        int index = 0;
        long lastModificationTime = System.currentTimeMillis();
        setSearchColumn(dto);

        ps.setObject(++index, dto.iD);
        ps.setObject(++index, dto.piPurchaseParentId);
        ps.setObject(++index, dto.purchaseOrderNumber);
        ps.setObject(++index, dto.purchaseDate);
        ps.setObject(++index, dto.officeUnitId);
        ps.setObject(++index, dto.fiscalYearId);
        ps.setObject(++index, dto.packageId);
        ps.setObject(++index, dto.lotId);
        ps.setObject(++index, dto.vendorId);
        ps.setObject(++index, dto.procurementMethod);
        ps.setObject(++index, dto.sarokNumber);
        ps.setObject(++index, dto.goodReceivingNumber);
        ps.setObject(++index, dto.receiveDate);
        ps.setObject(++index, dto.remarks);
        ps.setObject(++index, dto.filesDropZone);
        ps.setObject(++index, dto.searchColumn);
        ps.setObject(++index, dto.insertedBy);
        ps.setObject(++index, dto.insertionTime);
        ps.setObject(++index, dto.isDeleted);
        ps.setObject(++index, dto.modifiedBy);
        ps.setObject(++index, lastModificationTime);
    }

    @Override
    public Pi_product_receiveDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Pi_product_receiveDTO dto = new Pi_product_receiveDTO();
            dto.iD = rs.getLong("ID");
            dto.piPurchaseParentId = rs.getLong("pi_purchase_parent_id");
            dto.purchaseOrderNumber = rs.getString("purchase_order_number");
            dto.purchaseDate = rs.getLong("purchase_date");
            dto.officeUnitId = rs.getLong("office_unit_id");
            dto.fiscalYearId = rs.getLong("fiscal_year_id");
            dto.packageId = rs.getLong("package_id");
            dto.lotId = rs.getLong("lot_id");
            dto.vendorId = rs.getLong("vendor_id");
            dto.procurementMethod = rs.getInt("procurement_method");
            dto.sarokNumber = rs.getString("sarok_number");
            dto.goodReceivingNumber = rs.getString("good_receiving_number");
            dto.receiveDate = rs.getLong("receive_date");
            dto.remarks = rs.getString("remarks");
            dto.filesDropZone = rs.getLong("files_dropzone");
            dto.searchColumn = rs.getString("search_column");
            dto.insertedBy = rs.getLong("inserted_by");
            dto.insertionTime = rs.getLong("insertion_time");
            dto.modifiedBy = rs.getLong("modified_by");
            dto.lastModificationTime = rs.getLong("lastModificationTime");
            dto.isDeleted = rs.getInt("isDeleted");
            return dto;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    @Override
    public String getTableName() {
        return "pi_product_receive";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    @Override
    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Pi_product_receiveDTO) commonDTO, addQuery, true);
    }

    @Override
    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Pi_product_receiveDTO) commonDTO, updateQuery, false);
    }

    public Pi_product_receiveDTO getDTOByID(long id) {
        return getDTOFromID(id);
    }

    public long getItemReceiveCountByPurchaseAndItem(long purchaseParentId, long itemId) {
        String sql = String.format(getItemReceiveCountByPurchaseAndItem, purchaseParentId, itemId);
        List<Pi_product_receive_childDTO> dtos = Pi_product_receive_childDAO.getInstance().getDTOs(sql);
        return dtos == null ? 0 : ((Double) dtos.stream().map(dto -> dto.quantity)
                .mapToDouble(Double::doubleValue).sum()).longValue();
    }
}
