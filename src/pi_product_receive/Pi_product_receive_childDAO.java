package pi_product_receive;

import common.CommonDAOService;
import fiscal_year.Fiscal_yearRepository;
import org.apache.log4j.Logger;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Pi_product_receive_childDAO implements CommonDAOService<Pi_product_receive_childDTO> {
    private static final Logger logger = Logger.getLogger(Pi_product_receiveDAO.class);

    private static String addQuery = "";
    private static String updateQuery = "";
    private final String getDTOsForStockView = "SELECT * FROM pi_product_receive_child WHERE isDeleted = 0 " +
            "AND office_unit_id=%d AND fiscal_year_id=%d AND item_id=%d AND receive_date>=%d " +
            "ORDER BY receive_date ASC";

    private String[] columnNames = null;
    private final Map<String, String> searchMap = new HashMap<>();

    public Pi_product_receive_childDAO() {
        columnNames = new String[]
                {
                        "ID",
                        "pi_product_receive_id",
                        "pi_purchase_parent_id",
                        "pi_purchase_id",
                        "office_unit_id",
                        "fiscal_year_id",
                        "allocated_app",
                        "package_id",
                        "lot_id",
                        "vendor_id",
                        "sarok_number",
                        "good_receiving_number",
                        "receive_date",
                        "item_group_id",
                        "item_type_id",
                        "item_id",
                        "quantity",
                        "unit_price",
                        "total_price",
                        "expiry_date",
                        "remarks",
                        "inserted_by",
                        "insertion_time",
                        "isDeleted",
                        "modified_by",
                        "lastModificationTime",
                };
        updateQuery = getUpdateQuery(columnNames);
        addQuery = getAddQuery(columnNames);

        searchMap.put("pi_product_receive_id", " and (pi_product_receive_id = ?)");
        searchMap.put("office_unit_id", " and (office_unit_id = ?)");
        searchMap.put("fiscal_year_id", " and (fiscal_year_id = ?)");
    }

    private static class LazyLoader {
        static final Pi_product_receive_childDAO INSTANCE = new Pi_product_receive_childDAO();
    }

    public static Pi_product_receive_childDAO getInstance() {
        return LazyLoader.INSTANCE;
    }

    private void setSearchColumn(Pi_product_receive_childDTO dto) {

    }

    @Override
    public void set(PreparedStatement ps, Pi_product_receive_childDTO dto, boolean isInsert) throws SQLException {
        int index = 0;
        long lastModificationTime = System.currentTimeMillis();
        setSearchColumn(dto);

        ps.setObject(++index, dto.iD);
        ps.setObject(++index, dto.piProductReceiveId);
        ps.setObject(++index, dto.piPurchaseParentId);
        ps.setObject(++index, dto.piPurchaseId);
        ps.setObject(++index, dto.officeUnitId);
        ps.setObject(++index, dto.fiscalYearId);
        ps.setObject(++index, dto.allocatedAppId);
        ps.setObject(++index, dto.packageId);
        ps.setObject(++index, dto.lotId);
        ps.setObject(++index, dto.vendorId);
        ps.setObject(++index, dto.sarokNumber);
        ps.setObject(++index, dto.goodReceivingNumber);
        ps.setObject(++index, dto.receiveDate);
        ps.setObject(++index, dto.itemGroupId);
        ps.setObject(++index, dto.itemTypeId);
        ps.setObject(++index, dto.itemId);
        ps.setObject(++index, dto.quantity);
        ps.setObject(++index, dto.unitPrice);
        ps.setObject(++index, dto.totalPrice);
        ps.setObject(++index, dto.expiryDate);
        ps.setObject(++index, dto.remarks);
        ps.setObject(++index, dto.insertedBy);
        ps.setObject(++index, dto.insertionTime);
        ps.setObject(++index, dto.isDeleted);
        ps.setObject(++index, dto.modifiedBy);
        ps.setObject(++index, lastModificationTime);
    }

    @Override
    public Pi_product_receive_childDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Pi_product_receive_childDTO dto = new Pi_product_receive_childDTO();
            dto.iD = rs.getLong("ID");
            dto.piProductReceiveId = rs.getLong("pi_product_receive_id");
            dto.piPurchaseParentId = rs.getLong("pi_purchase_parent_id");
            dto.piPurchaseId = rs.getLong("pi_purchase_id");
            dto.officeUnitId = rs.getLong("office_unit_id");
            dto.fiscalYearId = rs.getLong("fiscal_year_id");
            dto.allocatedAppId = rs.getLong("allocated_app");
            dto.packageId = rs.getLong("package_id");
            dto.lotId = rs.getLong("lot_id");
            dto.vendorId = rs.getLong("vendor_id");
            dto.sarokNumber = rs.getString("sarok_number");
            dto.goodReceivingNumber = rs.getString("good_receiving_number");
            dto.receiveDate = rs.getLong("receive_date");
            dto.itemGroupId = rs.getLong("item_group_id");
            dto.itemTypeId = rs.getLong("item_type_id");
            dto.itemId = rs.getLong("item_id");
            dto.quantity = rs.getDouble("quantity");
            dto.unitPrice = rs.getDouble("unit_price");
            dto.totalPrice = rs.getDouble("total_price");
            dto.remarks = rs.getString("remarks");
            dto.expiryDate = rs.getLong("expiry_date");
            dto.insertedBy = rs.getLong("inserted_by");
            dto.insertionTime = rs.getLong("insertion_time");
            dto.modifiedBy = rs.getLong("modified_by");
            dto.lastModificationTime = rs.getLong("lastModificationTime");
            dto.isDeleted = rs.getInt("isDeleted");
            return dto;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    @Override
    public String getTableName() {
        return "pi_product_receive_child";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    @Override
    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Pi_product_receive_childDTO) commonDTO, addQuery, true);
    }

    @Override
    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Pi_product_receive_childDTO) commonDTO, updateQuery, false);
    }

    public List<Pi_product_receive_childDTO> getDTOsByGoodReceivingNumber(String goodReceivingNumber) {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT * FROM pi_product_receive_child WHERE isDeleted = 0 ")
                .append("AND good_receiving_number='")
                .append(goodReceivingNumber)
                .append("'");
        return getDTOs(sql.toString());
    }

    public List<Pi_product_receive_childDTO> getDTOsForStockModel(long officeUnitId, long fiscalYearId, long itemId) {
        long fiscalFirstDate = Fiscal_yearRepository.getInstance().getFirstDateofFiscalYear(fiscalYearId);
        String sql = String.format(getDTOsForStockView, officeUnitId, fiscalYearId, itemId, fiscalFirstDate);
        return Pi_product_receive_childDAO.getInstance().getDTOs(sql);
    }
}
