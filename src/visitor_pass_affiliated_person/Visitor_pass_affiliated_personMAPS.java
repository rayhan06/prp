package visitor_pass_affiliated_person;
import java.util.*; 
import util.*;


public class Visitor_pass_affiliated_personMAPS extends CommonMaps
{	
	public Visitor_pass_affiliated_personMAPS(String tableName)
	{
		

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("name".toLowerCase(), "name".toLowerCase());
		java_DTO_map.put("visitorPassId".toLowerCase(), "visitorPassId".toLowerCase());
		java_DTO_map.put("credentialCat".toLowerCase(), "credentialCat".toLowerCase());
		java_DTO_map.put("credentialNo".toLowerCase(), "credentialNo".toLowerCase());
		java_DTO_map.put("mobileNumber".toLowerCase(), "mobileNumber".toLowerCase());
		java_DTO_map.put("nationalityType".toLowerCase(), "nationalityType".toLowerCase());
		java_DTO_map.put("insertedByUserId".toLowerCase(), "insertedByUserId".toLowerCase());
		java_DTO_map.put("insertedByOrganogramId".toLowerCase(), "insertedByOrganogramId".toLowerCase());
		java_DTO_map.put("insertionDate".toLowerCase(), "insertionDate".toLowerCase());
		java_DTO_map.put("lastModifierUser".toLowerCase(), "lastModifierUser".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("name".toLowerCase(), "name".toLowerCase());
		java_SQL_map.put("credential_no".toLowerCase(), "credentialNo".toLowerCase());
		java_SQL_map.put("mobile_number".toLowerCase(), "mobileNumber".toLowerCase());
		java_SQL_map.put("nationality_type".toLowerCase(), "nationalityType".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Name".toLowerCase(), "name".toLowerCase());
		java_Text_map.put("Visitor Pass Id".toLowerCase(), "visitorPassId".toLowerCase());
		java_Text_map.put("Credential".toLowerCase(), "credentialCat".toLowerCase());
		java_Text_map.put("Credential No".toLowerCase(), "credentialNo".toLowerCase());
		java_Text_map.put("Mobile Number".toLowerCase(), "mobileNumber".toLowerCase());
		java_Text_map.put("Nationality".toLowerCase(), "nationalityType".toLowerCase());
		java_Text_map.put("Inserted By User Id".toLowerCase(), "insertedByUserId".toLowerCase());
		java_Text_map.put("Inserted By Organogram Id".toLowerCase(), "insertedByOrganogramId".toLowerCase());
		java_Text_map.put("Insertion Date".toLowerCase(), "insertionDate".toLowerCase());
		java_Text_map.put("Last Modifier User".toLowerCase(), "lastModifierUser".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}