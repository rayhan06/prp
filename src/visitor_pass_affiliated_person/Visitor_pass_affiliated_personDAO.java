package visitor_pass_affiliated_person;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import java.util.*;

import java.sql.SQLException;
import common.ConnectionAndStatementUtil;


import org.apache.log4j.Logger;

import dbm.*;

import repository.RepositoryManager;

import util.*;
import pb.*;
import user.UserDTO;

public class Visitor_pass_affiliated_personDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());

	public Visitor_pass_affiliated_personDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";
		commonMaps = new Visitor_pass_affiliated_personMAPS(tableName);
		columnNames = new String[] 
		{
			"ID",
			"name",
			"visitor_pass_id",
			"credential_cat",
			"credential_no",
			"mobile_number",
			"nationality_type",
			"nationality",
			"inserted_by_user_id",
			"inserted_by_organogram_id",
			"insertion_date",
			"last_modifier_user",
			"isDeleted",
			"lastModificationTime"
		};
	}
	
	public Visitor_pass_affiliated_personDAO()
	{
		this("visitor_pass_affiliated_person");		
	}
	
	public void setSearchColumn(Visitor_pass_affiliated_personDTO visitor_pass_affiliated_personDTO)
	{
		visitor_pass_affiliated_personDTO.searchColumn = "";
		visitor_pass_affiliated_personDTO.searchColumn += visitor_pass_affiliated_personDTO.name + " ";
		visitor_pass_affiliated_personDTO.searchColumn += visitor_pass_affiliated_personDTO.credentialNo + " ";
		visitor_pass_affiliated_personDTO.searchColumn += visitor_pass_affiliated_personDTO.mobileNumber + " ";
		visitor_pass_affiliated_personDTO.searchColumn += CommonDAO.getName("English", "nationality", visitor_pass_affiliated_personDTO.nationalityType) + " " + CommonDAO.getName("Bangla", "nationality", visitor_pass_affiliated_personDTO.nationalityType) + " ";
	}
	
	public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException
	{
		Visitor_pass_affiliated_personDTO visitor_pass_affiliated_personDTO = (Visitor_pass_affiliated_personDTO)commonDTO;
		int index = 1;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(visitor_pass_affiliated_personDTO);
		if(isInsert)
		{
			ps.setObject(index++,visitor_pass_affiliated_personDTO.iD);
		}
		ps.setObject(index++,visitor_pass_affiliated_personDTO.name);
		ps.setObject(index++,visitor_pass_affiliated_personDTO.visitorPassId);
		ps.setObject(index++,visitor_pass_affiliated_personDTO.credentialCat);
		ps.setObject(index++,visitor_pass_affiliated_personDTO.credentialNo);
		ps.setObject(index++,visitor_pass_affiliated_personDTO.mobileNumber);
		ps.setObject(index++,visitor_pass_affiliated_personDTO.nationalityType);
		ps.setObject(index++,visitor_pass_affiliated_personDTO.nationality);
		ps.setObject(index++,visitor_pass_affiliated_personDTO.insertedByUserId);
		ps.setObject(index++,visitor_pass_affiliated_personDTO.insertedByOrganogramId);
		ps.setObject(index++,visitor_pass_affiliated_personDTO.insertionDate);
		ps.setObject(index++,visitor_pass_affiliated_personDTO.lastModifierUser);
		if(isInsert)
		{
			ps.setObject(index++, 0);
			ps.setObject(index++, lastModificationTime);
		}
	}
	
	public Visitor_pass_affiliated_personDTO build(ResultSet rs)
	{
		try
		{
			Visitor_pass_affiliated_personDTO visitor_pass_affiliated_personDTO = new Visitor_pass_affiliated_personDTO();
			visitor_pass_affiliated_personDTO.iD = rs.getLong("ID");
			visitor_pass_affiliated_personDTO.name = rs.getString("name");
			visitor_pass_affiliated_personDTO.visitorPassId = rs.getLong("visitor_pass_id");
			visitor_pass_affiliated_personDTO.credentialCat = rs.getInt("credential_cat");
			visitor_pass_affiliated_personDTO.credentialNo = rs.getString("credential_no");
			visitor_pass_affiliated_personDTO.mobileNumber = rs.getString("mobile_number");
			visitor_pass_affiliated_personDTO.nationalityType = rs.getInt("nationality_type");
			visitor_pass_affiliated_personDTO.nationality = rs.getString("nationality");
			visitor_pass_affiliated_personDTO.insertedByUserId = rs.getLong("inserted_by_user_id");
			visitor_pass_affiliated_personDTO.insertedByOrganogramId = rs.getLong("inserted_by_organogram_id");
			visitor_pass_affiliated_personDTO.insertionDate = rs.getLong("insertion_date");
			visitor_pass_affiliated_personDTO.lastModifierUser = rs.getString("last_modifier_user");
			visitor_pass_affiliated_personDTO.isDeleted = rs.getInt("isDeleted");
			visitor_pass_affiliated_personDTO.lastModificationTime = rs.getLong("lastModificationTime");
			return visitor_pass_affiliated_personDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
	
	

	public Visitor_pass_affiliated_personDTO getDTOByID (long id)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID = " + id;
		Visitor_pass_affiliated_personDTO visitor_pass_affiliated_personDTO = ConnectionAndStatementUtil.getT(sql,this::build);
		return visitor_pass_affiliated_personDTO;
	}

	
	public List<Visitor_pass_affiliated_personDTO> getDTOs(Collection recordIDs)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID IN ( ";
		for(int i = 0;i<recordIDs.size();i++){
			if(i!=0){
				sql+=",";
			}
			sql+=((ArrayList)recordIDs).get(i);
		}
		sql+=")  order by lastModificationTime desc";
		printSql(sql);
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);		
	}
	
	public List<Visitor_pass_affiliated_personDTO> getAllVisitor_pass_affiliated_person (boolean isFirstReload)
    {
		String sql = "SELECT * FROM " + tableName + " WHERE ";
		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by " + tableName + ".lastModificationTime desc";
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);	
    }

	
	public List<Visitor_pass_affiliated_personDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO)
	{
		return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
	}

	
	public List<Visitor_pass_affiliated_personDTO> getDTOs(Hashtable  p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
			String filter, boolean tableHasJobCat)
	{
		String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat);
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);			
	}
	public String getSqlWithSearchCriteriaForNormalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
			UserDTO userDTO, String filter, boolean tableHasJobCat)
    {
		boolean viewAll = false;
		
		if(p_searchCriteria != null && p_searchCriteria.get("ViewAll") != null)
		{
			System.out.println("ViewAll = " + p_searchCriteria.get("ViewAll"));
			viewAll = true;
		}
		else
		{
			System.out.println("ViewAll is null ");
		}
		
		String sql = "SELECT ";
		if(category == GETIDS)
		{
			sql += " " + tableName + ".ID as ID ";
		}
		else if(category == GETCOUNT)
		{
			sql += " count( " + tableName + ".ID) as countID ";
		}
		else if(category == GETDTOS)
		{
			sql += " distinct " + tableName + ".* ";
		}
		sql += "FROM " + tableName + " ";
		sql += joinSQL;
		

		
		String AnyfieldSql = "";
		String AllFieldSql = "";
		
		if(p_searchCriteria != null)
		{
						
			
			Enumeration names = p_searchCriteria.keys();
			String str, value;
			
			AnyfieldSql = "(";
			
			if(p_searchCriteria.get("AnyField")!= null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase(""))
			{
				AnyfieldSql+= tableName + ".search_column like '%" + p_searchCriteria.get("AnyField").toString() + "%'";			
			}
			AnyfieldSql += ")";
			System.out.println("AnyfieldSql = " + AnyfieldSql);
			
			AllFieldSql = "(";
			int i = 0;
			while(names.hasMoreElements())
			{				
				str = (String) names.nextElement();
				value = (String)p_searchCriteria.get(str);
		        System.out.println(str + ": " + value);
				if(value != null && !value.equalsIgnoreCase("") && (
						 str.equals("name")
						|| str.equals("credential_no")
						|| str.equals("mobile_number")
						|| str.equals("nationality_type")
						|| str.equals("insertion_date_start")
						|| str.equals("insertion_date_end")
				)
						
				)
				{
					if(p_searchCriteria.get(str).equals("any"))
					{
						continue;
					}

					if( i > 0)
					{
						AllFieldSql+= " AND  ";
					}
					
					 if(str.equals("name"))
					{
						AllFieldSql += "" + tableName + ".name like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("credential_no"))
					{
						AllFieldSql += "" + tableName + ".credential_no like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("mobile_number"))
					{
						AllFieldSql += "" + tableName + ".mobile_number like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("nationality_type"))
					{
						AllFieldSql += "" + tableName + ".nationality_type = " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("insertion_date_start"))
					{
						AllFieldSql += "" + tableName + ".insertion_date >= " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("insertion_date_end"))
					{
						AllFieldSql += "" + tableName + ".insertion_date <= " + p_searchCriteria.get(str);
						i ++;
					}
					
					
				}
			}
			
			AllFieldSql += ")";
			System.out.println("AllFieldSql = " + AllFieldSql);
			
			
		}
		
		
		sql += " WHERE ";
		
		sql += " (" + tableName + ".isDeleted = 0 ";			
		sql += ")";
		
		
		if(!filter.equalsIgnoreCase(""))
		{
			sql += " and " + filter + " ";
		}
		
		if(!AnyfieldSql.equals("()") && !AnyfieldSql.equals(""))
		{
			sql += " AND " + AnyfieldSql;
			
		}
		if(!AllFieldSql.equals("()") && !AllFieldSql.equals(""))
		{			
			sql += " AND " + AllFieldSql;
		}
		
	
			
		sql += " order by " + tableName + ".lastModificationTime desc ";

		printSql(sql);
		
		if(limit >= 0)
		{
			sql += " limit " + limit;
		}
		if(offset >= 0)
		{
			sql += " offset " + offset;
		}
		
		System.out.println("-------------- sql = " + sql);
		
		return sql;
    }
	
	public String getSqlWithSearchCriteria(Hashtable p_searchCriteria, int limit, int offset, int category,
			boolean isPermanentTable, UserDTO userDTO, String filter, boolean tableHasJobCat)
    {		
		return getSqlWithSearchCriteriaForNormalSearch(p_searchCriteria, limit, offset, category, userDTO, filter, tableHasJobCat);	
    }



	public List<Visitor_pass_affiliated_personDTO> getPersonsByVisitorPassId(long visitorPassId) {
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Visitor_pass_affiliated_personDTO visitor_pass_affiliated_personDTO = null;
		List<Visitor_pass_affiliated_personDTO> visitor_pass_affiliated_personDTOList = new ArrayList<>();

		try{

			String sql = "select * from visitor_pass_affiliated_person where isDeleted=0 and visitor_pass_id=%d";
			sql = String.format(sql, visitorPassId);

			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();
			rs = stmt.executeQuery(sql);

			while(rs.next()){
				visitor_pass_affiliated_personDTO = build(rs);

                visitor_pass_affiliated_personDTOList.add(visitor_pass_affiliated_personDTO);
			}

		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}

			try{
				if (connection != null)
				{
					DBMR.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		return visitor_pass_affiliated_personDTOList;
	}
				
}
	