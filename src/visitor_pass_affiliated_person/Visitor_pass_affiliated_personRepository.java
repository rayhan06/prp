package visitor_pass_affiliated_person;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class Visitor_pass_affiliated_personRepository implements Repository {
	Visitor_pass_affiliated_personDAO visitor_pass_affiliated_personDAO = new Visitor_pass_affiliated_personDAO();
	
	public void setDAO(Visitor_pass_affiliated_personDAO visitor_pass_affiliated_personDAO)
	{
		this.visitor_pass_affiliated_personDAO = visitor_pass_affiliated_personDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Visitor_pass_affiliated_personRepository.class);
	Map<Long, Visitor_pass_affiliated_personDTO>mapOfVisitor_pass_affiliated_personDTOToiD;
	Map<String, Set<Visitor_pass_affiliated_personDTO> >mapOfVisitor_pass_affiliated_personDTOToname;
	Map<Long, Set<Visitor_pass_affiliated_personDTO> >mapOfVisitor_pass_affiliated_personDTOTovisitorPassId;
	Map<Integer, Set<Visitor_pass_affiliated_personDTO> >mapOfVisitor_pass_affiliated_personDTOTocredentialCat;
	Map<String, Set<Visitor_pass_affiliated_personDTO> >mapOfVisitor_pass_affiliated_personDTOTocredentialNo;
	Map<String, Set<Visitor_pass_affiliated_personDTO> >mapOfVisitor_pass_affiliated_personDTOTomobileNumber;
	Map<Integer, Set<Visitor_pass_affiliated_personDTO> >mapOfVisitor_pass_affiliated_personDTOTonationalityType;
	Map<Long, Set<Visitor_pass_affiliated_personDTO> >mapOfVisitor_pass_affiliated_personDTOToinsertedByUserId;
	Map<Long, Set<Visitor_pass_affiliated_personDTO> >mapOfVisitor_pass_affiliated_personDTOToinsertedByOrganogramId;
	Map<Long, Set<Visitor_pass_affiliated_personDTO> >mapOfVisitor_pass_affiliated_personDTOToinsertionDate;
	Map<String, Set<Visitor_pass_affiliated_personDTO> >mapOfVisitor_pass_affiliated_personDTOTolastModifierUser;
	Map<Long, Set<Visitor_pass_affiliated_personDTO> >mapOfVisitor_pass_affiliated_personDTOTolastModificationTime;


	static Visitor_pass_affiliated_personRepository instance = null;  
	private Visitor_pass_affiliated_personRepository(){
		mapOfVisitor_pass_affiliated_personDTOToiD = new ConcurrentHashMap<>();
		mapOfVisitor_pass_affiliated_personDTOToname = new ConcurrentHashMap<>();
		mapOfVisitor_pass_affiliated_personDTOTovisitorPassId = new ConcurrentHashMap<>();
		mapOfVisitor_pass_affiliated_personDTOTocredentialCat = new ConcurrentHashMap<>();
		mapOfVisitor_pass_affiliated_personDTOTocredentialNo = new ConcurrentHashMap<>();
		mapOfVisitor_pass_affiliated_personDTOTomobileNumber = new ConcurrentHashMap<>();
		mapOfVisitor_pass_affiliated_personDTOTonationalityType = new ConcurrentHashMap<>();
		mapOfVisitor_pass_affiliated_personDTOToinsertedByUserId = new ConcurrentHashMap<>();
		mapOfVisitor_pass_affiliated_personDTOToinsertedByOrganogramId = new ConcurrentHashMap<>();
		mapOfVisitor_pass_affiliated_personDTOToinsertionDate = new ConcurrentHashMap<>();
		mapOfVisitor_pass_affiliated_personDTOTolastModifierUser = new ConcurrentHashMap<>();
		mapOfVisitor_pass_affiliated_personDTOTolastModificationTime = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Visitor_pass_affiliated_personRepository getInstance(){
		if (instance == null){
			instance = new Visitor_pass_affiliated_personRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(visitor_pass_affiliated_personDAO == null)
		{
			return;
		}
		try {
			List<Visitor_pass_affiliated_personDTO> visitor_pass_affiliated_personDTOs = visitor_pass_affiliated_personDAO.getAllVisitor_pass_affiliated_person(reloadAll);
			for(Visitor_pass_affiliated_personDTO visitor_pass_affiliated_personDTO : visitor_pass_affiliated_personDTOs) {
				Visitor_pass_affiliated_personDTO oldVisitor_pass_affiliated_personDTO = getVisitor_pass_affiliated_personDTOByID(visitor_pass_affiliated_personDTO.iD);
				if( oldVisitor_pass_affiliated_personDTO != null ) {
					mapOfVisitor_pass_affiliated_personDTOToiD.remove(oldVisitor_pass_affiliated_personDTO.iD);
				
					if(mapOfVisitor_pass_affiliated_personDTOToname.containsKey(oldVisitor_pass_affiliated_personDTO.name)) {
						mapOfVisitor_pass_affiliated_personDTOToname.get(oldVisitor_pass_affiliated_personDTO.name).remove(oldVisitor_pass_affiliated_personDTO);
					}
					if(mapOfVisitor_pass_affiliated_personDTOToname.get(oldVisitor_pass_affiliated_personDTO.name).isEmpty()) {
						mapOfVisitor_pass_affiliated_personDTOToname.remove(oldVisitor_pass_affiliated_personDTO.name);
					}
					
					if(mapOfVisitor_pass_affiliated_personDTOTovisitorPassId.containsKey(oldVisitor_pass_affiliated_personDTO.visitorPassId)) {
						mapOfVisitor_pass_affiliated_personDTOTovisitorPassId.get(oldVisitor_pass_affiliated_personDTO.visitorPassId).remove(oldVisitor_pass_affiliated_personDTO);
					}
					if(mapOfVisitor_pass_affiliated_personDTOTovisitorPassId.get(oldVisitor_pass_affiliated_personDTO.visitorPassId).isEmpty()) {
						mapOfVisitor_pass_affiliated_personDTOTovisitorPassId.remove(oldVisitor_pass_affiliated_personDTO.visitorPassId);
					}
					
					if(mapOfVisitor_pass_affiliated_personDTOTocredentialCat.containsKey(oldVisitor_pass_affiliated_personDTO.credentialCat)) {
						mapOfVisitor_pass_affiliated_personDTOTocredentialCat.get(oldVisitor_pass_affiliated_personDTO.credentialCat).remove(oldVisitor_pass_affiliated_personDTO);
					}
					if(mapOfVisitor_pass_affiliated_personDTOTocredentialCat.get(oldVisitor_pass_affiliated_personDTO.credentialCat).isEmpty()) {
						mapOfVisitor_pass_affiliated_personDTOTocredentialCat.remove(oldVisitor_pass_affiliated_personDTO.credentialCat);
					}
					
					if(mapOfVisitor_pass_affiliated_personDTOTocredentialNo.containsKey(oldVisitor_pass_affiliated_personDTO.credentialNo)) {
						mapOfVisitor_pass_affiliated_personDTOTocredentialNo.get(oldVisitor_pass_affiliated_personDTO.credentialNo).remove(oldVisitor_pass_affiliated_personDTO);
					}
					if(mapOfVisitor_pass_affiliated_personDTOTocredentialNo.get(oldVisitor_pass_affiliated_personDTO.credentialNo).isEmpty()) {
						mapOfVisitor_pass_affiliated_personDTOTocredentialNo.remove(oldVisitor_pass_affiliated_personDTO.credentialNo);
					}
					
					if(mapOfVisitor_pass_affiliated_personDTOTomobileNumber.containsKey(oldVisitor_pass_affiliated_personDTO.mobileNumber)) {
						mapOfVisitor_pass_affiliated_personDTOTomobileNumber.get(oldVisitor_pass_affiliated_personDTO.mobileNumber).remove(oldVisitor_pass_affiliated_personDTO);
					}
					if(mapOfVisitor_pass_affiliated_personDTOTomobileNumber.get(oldVisitor_pass_affiliated_personDTO.mobileNumber).isEmpty()) {
						mapOfVisitor_pass_affiliated_personDTOTomobileNumber.remove(oldVisitor_pass_affiliated_personDTO.mobileNumber);
					}
					
					if(mapOfVisitor_pass_affiliated_personDTOTonationalityType.containsKey(oldVisitor_pass_affiliated_personDTO.nationalityType)) {
						mapOfVisitor_pass_affiliated_personDTOTonationalityType.get(oldVisitor_pass_affiliated_personDTO.nationalityType).remove(oldVisitor_pass_affiliated_personDTO);
					}
					if(mapOfVisitor_pass_affiliated_personDTOTonationalityType.get(oldVisitor_pass_affiliated_personDTO.nationalityType).isEmpty()) {
						mapOfVisitor_pass_affiliated_personDTOTonationalityType.remove(oldVisitor_pass_affiliated_personDTO.nationalityType);
					}
					
					if(mapOfVisitor_pass_affiliated_personDTOToinsertedByUserId.containsKey(oldVisitor_pass_affiliated_personDTO.insertedByUserId)) {
						mapOfVisitor_pass_affiliated_personDTOToinsertedByUserId.get(oldVisitor_pass_affiliated_personDTO.insertedByUserId).remove(oldVisitor_pass_affiliated_personDTO);
					}
					if(mapOfVisitor_pass_affiliated_personDTOToinsertedByUserId.get(oldVisitor_pass_affiliated_personDTO.insertedByUserId).isEmpty()) {
						mapOfVisitor_pass_affiliated_personDTOToinsertedByUserId.remove(oldVisitor_pass_affiliated_personDTO.insertedByUserId);
					}
					
					if(mapOfVisitor_pass_affiliated_personDTOToinsertedByOrganogramId.containsKey(oldVisitor_pass_affiliated_personDTO.insertedByOrganogramId)) {
						mapOfVisitor_pass_affiliated_personDTOToinsertedByOrganogramId.get(oldVisitor_pass_affiliated_personDTO.insertedByOrganogramId).remove(oldVisitor_pass_affiliated_personDTO);
					}
					if(mapOfVisitor_pass_affiliated_personDTOToinsertedByOrganogramId.get(oldVisitor_pass_affiliated_personDTO.insertedByOrganogramId).isEmpty()) {
						mapOfVisitor_pass_affiliated_personDTOToinsertedByOrganogramId.remove(oldVisitor_pass_affiliated_personDTO.insertedByOrganogramId);
					}
					
					if(mapOfVisitor_pass_affiliated_personDTOToinsertionDate.containsKey(oldVisitor_pass_affiliated_personDTO.insertionDate)) {
						mapOfVisitor_pass_affiliated_personDTOToinsertionDate.get(oldVisitor_pass_affiliated_personDTO.insertionDate).remove(oldVisitor_pass_affiliated_personDTO);
					}
					if(mapOfVisitor_pass_affiliated_personDTOToinsertionDate.get(oldVisitor_pass_affiliated_personDTO.insertionDate).isEmpty()) {
						mapOfVisitor_pass_affiliated_personDTOToinsertionDate.remove(oldVisitor_pass_affiliated_personDTO.insertionDate);
					}
					
					if(mapOfVisitor_pass_affiliated_personDTOTolastModifierUser.containsKey(oldVisitor_pass_affiliated_personDTO.lastModifierUser)) {
						mapOfVisitor_pass_affiliated_personDTOTolastModifierUser.get(oldVisitor_pass_affiliated_personDTO.lastModifierUser).remove(oldVisitor_pass_affiliated_personDTO);
					}
					if(mapOfVisitor_pass_affiliated_personDTOTolastModifierUser.get(oldVisitor_pass_affiliated_personDTO.lastModifierUser).isEmpty()) {
						mapOfVisitor_pass_affiliated_personDTOTolastModifierUser.remove(oldVisitor_pass_affiliated_personDTO.lastModifierUser);
					}
					
					if(mapOfVisitor_pass_affiliated_personDTOTolastModificationTime.containsKey(oldVisitor_pass_affiliated_personDTO.lastModificationTime)) {
						mapOfVisitor_pass_affiliated_personDTOTolastModificationTime.get(oldVisitor_pass_affiliated_personDTO.lastModificationTime).remove(oldVisitor_pass_affiliated_personDTO);
					}
					if(mapOfVisitor_pass_affiliated_personDTOTolastModificationTime.get(oldVisitor_pass_affiliated_personDTO.lastModificationTime).isEmpty()) {
						mapOfVisitor_pass_affiliated_personDTOTolastModificationTime.remove(oldVisitor_pass_affiliated_personDTO.lastModificationTime);
					}
					
					
				}
				if(visitor_pass_affiliated_personDTO.isDeleted == 0) 
				{
					
					mapOfVisitor_pass_affiliated_personDTOToiD.put(visitor_pass_affiliated_personDTO.iD, visitor_pass_affiliated_personDTO);
				
					if( ! mapOfVisitor_pass_affiliated_personDTOToname.containsKey(visitor_pass_affiliated_personDTO.name)) {
						mapOfVisitor_pass_affiliated_personDTOToname.put(visitor_pass_affiliated_personDTO.name, new HashSet<>());
					}
					mapOfVisitor_pass_affiliated_personDTOToname.get(visitor_pass_affiliated_personDTO.name).add(visitor_pass_affiliated_personDTO);
					
					if( ! mapOfVisitor_pass_affiliated_personDTOTovisitorPassId.containsKey(visitor_pass_affiliated_personDTO.visitorPassId)) {
						mapOfVisitor_pass_affiliated_personDTOTovisitorPassId.put(visitor_pass_affiliated_personDTO.visitorPassId, new HashSet<>());
					}
					mapOfVisitor_pass_affiliated_personDTOTovisitorPassId.get(visitor_pass_affiliated_personDTO.visitorPassId).add(visitor_pass_affiliated_personDTO);
					
					if( ! mapOfVisitor_pass_affiliated_personDTOTocredentialCat.containsKey(visitor_pass_affiliated_personDTO.credentialCat)) {
						mapOfVisitor_pass_affiliated_personDTOTocredentialCat.put(visitor_pass_affiliated_personDTO.credentialCat, new HashSet<>());
					}
					mapOfVisitor_pass_affiliated_personDTOTocredentialCat.get(visitor_pass_affiliated_personDTO.credentialCat).add(visitor_pass_affiliated_personDTO);
					
					if( ! mapOfVisitor_pass_affiliated_personDTOTocredentialNo.containsKey(visitor_pass_affiliated_personDTO.credentialNo)) {
						mapOfVisitor_pass_affiliated_personDTOTocredentialNo.put(visitor_pass_affiliated_personDTO.credentialNo, new HashSet<>());
					}
					mapOfVisitor_pass_affiliated_personDTOTocredentialNo.get(visitor_pass_affiliated_personDTO.credentialNo).add(visitor_pass_affiliated_personDTO);
					
					if( ! mapOfVisitor_pass_affiliated_personDTOTomobileNumber.containsKey(visitor_pass_affiliated_personDTO.mobileNumber)) {
						mapOfVisitor_pass_affiliated_personDTOTomobileNumber.put(visitor_pass_affiliated_personDTO.mobileNumber, new HashSet<>());
					}
					mapOfVisitor_pass_affiliated_personDTOTomobileNumber.get(visitor_pass_affiliated_personDTO.mobileNumber).add(visitor_pass_affiliated_personDTO);
					
					if( ! mapOfVisitor_pass_affiliated_personDTOTonationalityType.containsKey(visitor_pass_affiliated_personDTO.nationalityType)) {
						mapOfVisitor_pass_affiliated_personDTOTonationalityType.put(visitor_pass_affiliated_personDTO.nationalityType, new HashSet<>());
					}
					mapOfVisitor_pass_affiliated_personDTOTonationalityType.get(visitor_pass_affiliated_personDTO.nationalityType).add(visitor_pass_affiliated_personDTO);
					
					if( ! mapOfVisitor_pass_affiliated_personDTOToinsertedByUserId.containsKey(visitor_pass_affiliated_personDTO.insertedByUserId)) {
						mapOfVisitor_pass_affiliated_personDTOToinsertedByUserId.put(visitor_pass_affiliated_personDTO.insertedByUserId, new HashSet<>());
					}
					mapOfVisitor_pass_affiliated_personDTOToinsertedByUserId.get(visitor_pass_affiliated_personDTO.insertedByUserId).add(visitor_pass_affiliated_personDTO);
					
					if( ! mapOfVisitor_pass_affiliated_personDTOToinsertedByOrganogramId.containsKey(visitor_pass_affiliated_personDTO.insertedByOrganogramId)) {
						mapOfVisitor_pass_affiliated_personDTOToinsertedByOrganogramId.put(visitor_pass_affiliated_personDTO.insertedByOrganogramId, new HashSet<>());
					}
					mapOfVisitor_pass_affiliated_personDTOToinsertedByOrganogramId.get(visitor_pass_affiliated_personDTO.insertedByOrganogramId).add(visitor_pass_affiliated_personDTO);
					
					if( ! mapOfVisitor_pass_affiliated_personDTOToinsertionDate.containsKey(visitor_pass_affiliated_personDTO.insertionDate)) {
						mapOfVisitor_pass_affiliated_personDTOToinsertionDate.put(visitor_pass_affiliated_personDTO.insertionDate, new HashSet<>());
					}
					mapOfVisitor_pass_affiliated_personDTOToinsertionDate.get(visitor_pass_affiliated_personDTO.insertionDate).add(visitor_pass_affiliated_personDTO);
					
					if( ! mapOfVisitor_pass_affiliated_personDTOTolastModifierUser.containsKey(visitor_pass_affiliated_personDTO.lastModifierUser)) {
						mapOfVisitor_pass_affiliated_personDTOTolastModifierUser.put(visitor_pass_affiliated_personDTO.lastModifierUser, new HashSet<>());
					}
					mapOfVisitor_pass_affiliated_personDTOTolastModifierUser.get(visitor_pass_affiliated_personDTO.lastModifierUser).add(visitor_pass_affiliated_personDTO);
					
					if( ! mapOfVisitor_pass_affiliated_personDTOTolastModificationTime.containsKey(visitor_pass_affiliated_personDTO.lastModificationTime)) {
						mapOfVisitor_pass_affiliated_personDTOTolastModificationTime.put(visitor_pass_affiliated_personDTO.lastModificationTime, new HashSet<>());
					}
					mapOfVisitor_pass_affiliated_personDTOTolastModificationTime.get(visitor_pass_affiliated_personDTO.lastModificationTime).add(visitor_pass_affiliated_personDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Visitor_pass_affiliated_personDTO> getVisitor_pass_affiliated_personList() {
		List <Visitor_pass_affiliated_personDTO> visitor_pass_affiliated_persons = new ArrayList<Visitor_pass_affiliated_personDTO>(this.mapOfVisitor_pass_affiliated_personDTOToiD.values());
		return visitor_pass_affiliated_persons;
	}
	
	
	public Visitor_pass_affiliated_personDTO getVisitor_pass_affiliated_personDTOByID( long ID){
		return mapOfVisitor_pass_affiliated_personDTOToiD.get(ID);
	}
	
	
	public List<Visitor_pass_affiliated_personDTO> getVisitor_pass_affiliated_personDTOByname(String name) {
		return new ArrayList<>( mapOfVisitor_pass_affiliated_personDTOToname.getOrDefault(name,new HashSet<>()));
	}
	
	
	public List<Visitor_pass_affiliated_personDTO> getVisitor_pass_affiliated_personDTOByvisitor_pass_id(long visitor_pass_id) {
		return new ArrayList<>( mapOfVisitor_pass_affiliated_personDTOTovisitorPassId.getOrDefault(visitor_pass_id,new HashSet<>()));
	}
	
	
	public List<Visitor_pass_affiliated_personDTO> getVisitor_pass_affiliated_personDTOBycredential_cat(int credential_cat) {
		return new ArrayList<>( mapOfVisitor_pass_affiliated_personDTOTocredentialCat.getOrDefault(credential_cat,new HashSet<>()));
	}
	
	
	public List<Visitor_pass_affiliated_personDTO> getVisitor_pass_affiliated_personDTOBycredential_no(String credential_no) {
		return new ArrayList<>( mapOfVisitor_pass_affiliated_personDTOTocredentialNo.getOrDefault(credential_no,new HashSet<>()));
	}
	
	
	public List<Visitor_pass_affiliated_personDTO> getVisitor_pass_affiliated_personDTOBymobile_number(String mobile_number) {
		return new ArrayList<>( mapOfVisitor_pass_affiliated_personDTOTomobileNumber.getOrDefault(mobile_number,new HashSet<>()));
	}
	
	
	public List<Visitor_pass_affiliated_personDTO> getVisitor_pass_affiliated_personDTOBynationality_type(int nationality_type) {
		return new ArrayList<>( mapOfVisitor_pass_affiliated_personDTOTonationalityType.getOrDefault(nationality_type,new HashSet<>()));
	}
	
	
	public List<Visitor_pass_affiliated_personDTO> getVisitor_pass_affiliated_personDTOByinserted_by_user_id(long inserted_by_user_id) {
		return new ArrayList<>( mapOfVisitor_pass_affiliated_personDTOToinsertedByUserId.getOrDefault(inserted_by_user_id,new HashSet<>()));
	}
	
	
	public List<Visitor_pass_affiliated_personDTO> getVisitor_pass_affiliated_personDTOByinserted_by_organogram_id(long inserted_by_organogram_id) {
		return new ArrayList<>( mapOfVisitor_pass_affiliated_personDTOToinsertedByOrganogramId.getOrDefault(inserted_by_organogram_id,new HashSet<>()));
	}
	
	
	public List<Visitor_pass_affiliated_personDTO> getVisitor_pass_affiliated_personDTOByinsertion_date(long insertion_date) {
		return new ArrayList<>( mapOfVisitor_pass_affiliated_personDTOToinsertionDate.getOrDefault(insertion_date,new HashSet<>()));
	}
	
	
	public List<Visitor_pass_affiliated_personDTO> getVisitor_pass_affiliated_personDTOBylast_modifier_user(String last_modifier_user) {
		return new ArrayList<>( mapOfVisitor_pass_affiliated_personDTOTolastModifierUser.getOrDefault(last_modifier_user,new HashSet<>()));
	}
	
	
	public List<Visitor_pass_affiliated_personDTO> getVisitor_pass_affiliated_personDTOBylastModificationTime(long lastModificationTime) {
		return new ArrayList<>( mapOfVisitor_pass_affiliated_personDTOTolastModificationTime.getOrDefault(lastModificationTime,new HashSet<>()));
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "visitor_pass_affiliated_person";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


