package visitor_pass_affiliated_person;
import util.*; 


public class Visitor_pass_affiliated_personDTO extends CommonDTO
{

    public String name = "";
	public long visitorPassId = -1;
	public int credentialCat = -1;
    public String credentialNo = "";
    public String mobileNumber = "";
	public int nationalityType = -1;
	public long insertedByUserId = -1;
	public long insertedByOrganogramId = -1;
	public long insertionDate = -1;
    public String lastModifierUser = "";
    public String nationality = "";
	
	
    @Override
	public String toString() {
            return "$Visitor_pass_affiliated_personDTO[" +
            " iD = " + iD +
            " name = " + name +
            " visitorPassId = " + visitorPassId +
            " credentialCat = " + credentialCat +
            " credentialNo = " + credentialNo +
            " mobileNumber = " + mobileNumber +
            " nationalityType = " + nationalityType +
            " insertedByUserId = " + insertedByUserId +
            " insertedByOrganogramId = " + insertedByOrganogramId +
            " insertionDate = " + insertionDate +
            " lastModifierUser = " + lastModifierUser +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}