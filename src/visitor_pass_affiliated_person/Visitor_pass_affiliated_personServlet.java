package visitor_pass_affiliated_person;

import java.io.IOException;
import java.io.*;
import java.text.SimpleDateFormat;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import org.apache.log4j.Logger;

import login.LoginDTO;
import permission.MenuConstants;
import role.PermissionRepository;


import sessionmanager.SessionConstants;

import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;
import util.CommonRequestHandler;
import util.RecordNavigationManager4;

import java.util.*;
import javax.servlet.http.*;
import java.util.UUID;





import geolocation.GeoLocationDAO2;
import java.util.StringTokenizer;

import com.google.gson.Gson;

import pb.*;
import pbReport.*;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;




/**
 * Servlet implementation class Visitor_pass_affiliated_personServlet
 */
@WebServlet("/Visitor_pass_affiliated_personServlet")
@MultipartConfig
public class Visitor_pass_affiliated_personServlet extends HttpServlet
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Visitor_pass_affiliated_personServlet.class);

    String tableName = "visitor_pass_affiliated_person";

	Visitor_pass_affiliated_personDAO visitor_pass_affiliated_personDAO;
	CommonRequestHandler commonRequestHandler;
    private final Gson gson = new Gson();

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Visitor_pass_affiliated_personServlet()
	{
        super();
    	try
    	{
			visitor_pass_affiliated_personDAO = new Visitor_pass_affiliated_personDAO(tableName);
			commonRequestHandler = new CommonRequestHandler(visitor_pass_affiliated_personDAO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		System.out.println("In doget request = " + request);
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		try
		{
			String actionType = request.getParameter("actionType");
			if(actionType.equals("getAddPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VISITOR_PASS_AFFILIATED_PERSON_ADD))
				{
					commonRequestHandler.getAddPage(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if (actionType.equals("getAffiliatedPersonModal")) {
				String empId = request.getParameter("empId");
				if(empId == null){
					empId =String.valueOf(userDTO.employee_record_id);
				}
				logger.debug("EmpId : "+empId);
				request.setAttribute("empId", empId);
				request.setAttribute("ID", -1L);
				RequestDispatcher requestDispatcher = request.getRequestDispatcher("visitor_pass_affiliated_person/affiliated_person_modalBody.jsp");
				requestDispatcher.forward(request, response);
			}
			else if (actionType.equals("getAffiliatedPersonModalForeign")) {
				String empId = request.getParameter("empId");
				if(empId == null){
					empId =String.valueOf(userDTO.employee_record_id);
				}
				logger.debug("EmpId : "+empId);
				request.setAttribute("empId", empId);
				request.setAttribute("ID", -1L);
				RequestDispatcher requestDispatcher = request.getRequestDispatcher("visitor_pass_affiliated_person/affiliated_person_modalBodyForeign.jsp");
				requestDispatcher.forward(request, response);
			}
			else if(actionType.equals("getEditPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VISITOR_PASS_AFFILIATED_PERSON_UPDATE))
				{
					getVisitor_pass_affiliated_person(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getURL"))
			{
				String URL = request.getParameter("URL");
				System.out.println("URL = " + URL);
				response.sendRedirect(URL);
			}
			else if(actionType.equals("search"))
			{
				System.out.println("search requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VISITOR_PASS_AFFILIATED_PERSON_SEARCH))
				{
					if(isPermanentTable)
					{
						String filter = request.getParameter("filter");
						System.out.println("filter = " + filter);
						if(filter!=null)
						{
							filter = ""; //shouldn't be directly used, rather manipulate it.
							searchVisitor_pass_affiliated_person(request, response, isPermanentTable, filter);
						}
						else
						{
							searchVisitor_pass_affiliated_person(request, response, isPermanentTable, "");
						}
					}
					else
					{
						//searchVisitor_pass_affiliated_person(request, response, tempTableName, isPermanentTable);
					}
				}
			}
			else if(actionType.equals("view"))
			{
				System.out.println("view requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VISITOR_PASS_AFFILIATED_PERSON_SEARCH))
				{
					commonRequestHandler.view(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}
			else
			{
				request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}





	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		// TODO Auto-generated method stub
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		System.out.println("doPost");
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);

		try
		{
			String actionType = request.getParameter("actionType");
			System.out.println("actionType = " + actionType);
			if(actionType.equals("add"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VISITOR_PASS_AFFILIATED_PERSON_ADD))
				{
					System.out.println("going to  addVisitor_pass_affiliated_person ");
					addVisitor_pass_affiliated_person(request, response, true, userDTO, true);
				}
				else
				{
					System.out.println("Not going to  addVisitor_pass_affiliated_person ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}

			else if(actionType.equals("getDTO"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VISITOR_PASS_AFFILIATED_PERSON_ADD))
				{
					getDTO(request, response);
				}
				else
				{
					System.out.println("Not going to  addVisitor_pass_affiliated_person ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}
			else if(actionType.equals("edit"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VISITOR_PASS_AFFILIATED_PERSON_UPDATE))
				{
					addVisitor_pass_affiliated_person(request, response, false, userDTO, isPermanentTable);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("delete"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VISITOR_PASS_AFFILIATED_PERSON_UPDATE))
				{
					commonRequestHandler.delete(request, response, userDTO);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("search"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VISITOR_PASS_AFFILIATED_PERSON_SEARCH))
				{
					searchVisitor_pass_affiliated_person(request, response, true, "");
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getGeo"))
			{
				System.out.println("going to geoloc ");
				request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
			}

		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}

	private void getDTO(HttpServletRequest request, HttpServletResponse response)
	{
		try
		{
			System.out.println("In getDTO");
			Visitor_pass_affiliated_personDTO visitor_pass_affiliated_personDTO = visitor_pass_affiliated_personDAO.getDTOByID(Long.parseLong(request.getParameter("ID")));
			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");

			String encoded = this.gson.toJson(visitor_pass_affiliated_personDTO);
			System.out.println("json encoded data = " + encoded);
			out.print(encoded);
			out.flush();
		}
		catch (NumberFormatException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void addVisitor_pass_affiliated_person(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, boolean isPermanentTable) throws IOException
	{
		// TODO Auto-generated method stub
		try
		{
			request.setAttribute("failureMessage", "");
			System.out.println("%%%% addVisitor_pass_affiliated_person");
			String path = getServletContext().getRealPath("/img2/");
			Visitor_pass_affiliated_personDTO visitor_pass_affiliated_personDTO;
			SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");

			if(addFlag == true)
			{
				visitor_pass_affiliated_personDTO = new Visitor_pass_affiliated_personDTO();
			}
			else
			{
				visitor_pass_affiliated_personDTO = visitor_pass_affiliated_personDAO.getDTOByID(Long.parseLong(request.getParameter("iD")));
			}

			String Value = "";

			Value = request.getParameter("name");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("name = " + Value);
			if(Value != null)
			{
				visitor_pass_affiliated_personDTO.name = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("visitorPassId");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("visitorPassId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				visitor_pass_affiliated_personDTO.visitorPassId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("credentialCat");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("credentialCat = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				visitor_pass_affiliated_personDTO.credentialCat = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("credentialNo");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("credentialNo = " + Value);
			if(Value != null)
			{
				visitor_pass_affiliated_personDTO.credentialNo = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("mobileNumber");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("mobileNumber = " + Value);
			if(Value != null)
			{
				visitor_pass_affiliated_personDTO.mobileNumber = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("nationalityType");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("nationalityType = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				visitor_pass_affiliated_personDTO.nationalityType = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			if(addFlag)
			{
				visitor_pass_affiliated_personDTO.insertedByUserId = userDTO.ID;
			}


			if(addFlag)
			{
				visitor_pass_affiliated_personDTO.insertedByOrganogramId = userDTO.organogramID;
			}


			if(addFlag)
			{
				Calendar c = Calendar.getInstance();
				c.set(Calendar.HOUR_OF_DAY, 0);
				c.set(Calendar.MINUTE, 0);
				c.set(Calendar.SECOND, 0);
				c.set(Calendar.MILLISECOND, 0);

				visitor_pass_affiliated_personDTO.insertionDate = c.getTimeInMillis();
			}


			visitor_pass_affiliated_personDTO.lastModifierUser = userDTO.userName;


			System.out.println("Done adding  addVisitor_pass_affiliated_person dto = " + visitor_pass_affiliated_personDTO);
			long returnedID = -1;

			if(isPermanentTable == false) //add new row for validation and make the old row outdated
			{
				visitor_pass_affiliated_personDAO.setIsDeleted(visitor_pass_affiliated_personDTO.iD, CommonDTO.OUTDATED);
				returnedID = visitor_pass_affiliated_personDAO.add(visitor_pass_affiliated_personDTO);
				visitor_pass_affiliated_personDAO.setIsDeleted(returnedID, CommonDTO.WAITING_FOR_APPROVAL);
			}
			else if(addFlag == true)
			{
				returnedID = visitor_pass_affiliated_personDAO.manageWriteOperations(visitor_pass_affiliated_personDTO, SessionConstants.INSERT, -1, userDTO);
			}
			else
			{
				returnedID = visitor_pass_affiliated_personDAO.manageWriteOperations(visitor_pass_affiliated_personDTO, SessionConstants.UPDATE, -1, userDTO);
			}









			if(isPermanentTable)
			{
				String inPlaceSubmit = request.getParameter("inplacesubmit");

				if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
				{
					getVisitor_pass_affiliated_person(request, response, returnedID);
				}
				else
				{
					response.sendRedirect("Visitor_pass_affiliated_personServlet?actionType=search");
				}
			}
			else
			{
				commonRequestHandler.validate(visitor_pass_affiliated_personDAO.getDTOByID(returnedID), request, response, userDTO);
			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}









	private void getVisitor_pass_affiliated_person(HttpServletRequest request, HttpServletResponse response, long id) throws ServletException, IOException
	{
		System.out.println("in getVisitor_pass_affiliated_person");
		Visitor_pass_affiliated_personDTO visitor_pass_affiliated_personDTO = null;
		try
		{
			visitor_pass_affiliated_personDTO = visitor_pass_affiliated_personDAO.getDTOByID(id);
			request.setAttribute("ID", visitor_pass_affiliated_personDTO.iD);
			request.setAttribute("visitor_pass_affiliated_personDTO",visitor_pass_affiliated_personDTO);
			request.setAttribute("visitor_pass_affiliated_personDAO",visitor_pass_affiliated_personDAO);

			String URL= "";

			String inPlaceEdit = request.getParameter("inplaceedit");
			String inPlaceSubmit = request.getParameter("inplacesubmit");
			String getBodyOnly = request.getParameter("getBodyOnly");

			if(inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase(""))
			{
				URL = "visitor_pass_affiliated_person/visitor_pass_affiliated_personInPlaceEdit.jsp";
				request.setAttribute("inplaceedit","");
			}
			else if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
			{
				URL = "visitor_pass_affiliated_person/visitor_pass_affiliated_personSearchRow.jsp";
				request.setAttribute("inplacesubmit","");
			}
			else
			{
				if(getBodyOnly  != null && !getBodyOnly.equalsIgnoreCase(""))
				{
					URL = "visitor_pass_affiliated_person/visitor_pass_affiliated_personEditBody.jsp?actionType=edit";
				}
				else
				{
					URL = "visitor_pass_affiliated_person/visitor_pass_affiliated_personEdit.jsp?actionType=edit";
				}
			}

			RequestDispatcher rd = request.getRequestDispatcher(URL);
			rd.forward(request, response);
		}
		catch (NumberFormatException e)
		{
			e.printStackTrace();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}


	private void getVisitor_pass_affiliated_person(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		getVisitor_pass_affiliated_person(request, response, Long.parseLong(request.getParameter("ID")));
	}

	private void searchVisitor_pass_affiliated_person(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException
	{
		System.out.println("in  searchVisitor_pass_affiliated_person 1");
		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String ajax = request.getParameter("ajax");
		boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);

        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
			SessionConstants.NAV_VISITOR_PASS_AFFILIATED_PERSON,
			request,
			visitor_pass_affiliated_personDAO,
			SessionConstants.VIEW_VISITOR_PASS_AFFILIATED_PERSON,
			SessionConstants.SEARCH_VISITOR_PASS_AFFILIATED_PERSON,
			tableName,
			isPermanent,
			userDTO,
			filter,
			true);
        try
        {
			System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        }
        catch(Exception e)
        {
			System.out.println("failed to dojob" + e);
        }

		request.setAttribute("visitor_pass_affiliated_personDAO",visitor_pass_affiliated_personDAO);
        RequestDispatcher rd;
        if(!isPermanent)
        {
        	if(hasAjax == false)
	        {
	        	System.out.println("Going to visitor_pass_affiliated_person/visitor_pass_affiliated_personApproval.jsp");
	        	rd = request.getRequestDispatcher("visitor_pass_affiliated_person/visitor_pass_affiliated_personApproval.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to visitor_pass_affiliated_person/visitor_pass_affiliated_personApprovalForm.jsp");
	        	rd = request.getRequestDispatcher("visitor_pass_affiliated_person/visitor_pass_affiliated_personApprovalForm.jsp");
	        }
        }
        else
        {
	        if(hasAjax == false)
	        {
	        	System.out.println("Going to visitor_pass_affiliated_person/visitor_pass_affiliated_personSearch.jsp");
	        	rd = request.getRequestDispatcher("visitor_pass_affiliated_person/visitor_pass_affiliated_personSearch.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to visitor_pass_affiliated_person/visitor_pass_affiliated_personSearchForm.jsp");
	        	rd = request.getRequestDispatcher("visitor_pass_affiliated_person/visitor_pass_affiliated_personSearchForm.jsp");
	        }
        }
		rd.forward(request, response);
	}

}

