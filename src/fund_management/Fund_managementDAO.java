package fund_management;

import common.EmployeeCommonDAOService;
import org.apache.log4j.Logger;
import pb.CatRepository;
import pb.CategoryLanguageModel;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

@SuppressWarnings({"Duplicates"})
public class Fund_managementDAO implements EmployeeCommonDAOService<Fund_managementDTO> {

    private static final Logger logger = Logger.getLogger(Fund_managementDAO.class);

    private static final String addSqlQuery =
            "INSERT INTO {tableName} (fund_type_cat,cgs_number,advance_amount,q1_response,q2_response,q3_response,q4_response,q5_response,"
                    .concat("q6a_response,q6b_response,q7a_response,q7b_response,q8_response,q9_response,employee_records_id,")
                    .concat("employee_name_en,employee_name_bn,employee_user_name,office_unit_id,office_name_en,office_name_bn,")
                    .concat("organogram_id,organogram_name_en,organogram_name_bn,comment,search_column,fund_application_status_cat,modified_by,lastModificationTime,")
                    .concat("inserted_by,insertion_date,isDeleted,ID) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

    private static final String updateSqlQuery =
            "UPDATE {tableName} SET fund_type_cat=?,cgs_number=?,advance_amount=?,q1_response=?,q2_response=?,q3_response=?,"
                    .concat("q4_response=?,q5_response=?,q6a_response=?,q6b_response=?,q7a_response=?,q7b_response=?,q8_response=?,")
                    .concat("q9_response=?,employee_records_id=?,employee_name_en=?,employee_name_bn=?,employee_user_name=?,")
                    .concat("office_unit_id=?,office_name_en=?,office_name_bn=?,organogram_id=?,organogram_name_en=?,organogram_name_bn=?,")
                    .concat("comment=?,search_column=?,fund_application_status_cat=?,modified_by=?,lastModificationTime=? WHERE ID=?");

    private static final Map<String, String> searchMap = new HashMap<>();

    private Fund_managementDAO() {
        searchMap.put("fundTypeCat", " AND (fund_type_cat = ?)");
        searchMap.put("cgsNumber", " AND (cgs_number LIKE ?)");
        searchMap.put("employeeRecordId", " AND (employee_records_id = ?)");
        searchMap.put("employeeRecordIdInternal", " AND (employee_records_id = ?)");
        searchMap.put("officeUnitId", " AND (office_unit_id = ?)");
        searchMap.put("AnyField", " AND (search_column LIKE ?)");
        searchMap.put("fundApplicationStatusCat", " AND (fund_application_status_cat = ?)");
    }

    private static class LazyLoader {
        static final Fund_managementDAO INSTANCE = new Fund_managementDAO();
    }

    public static Fund_managementDAO getInstance() {
        return LazyLoader.INSTANCE;
    }

    public void setSearchColumn(Fund_managementDTO dto) {
        CategoryLanguageModel categoryLanguageModel =
                CatRepository.getInstance()
                             .getCategoryLanguageModel("fund_type", dto.fundTypeCat);

        dto.searchColumn = categoryLanguageModel.banglaText
                           + " " + categoryLanguageModel.englishText
                           + " " + dto.cgsNumber
                           + " " + dto.employeeNameEn
                           + " " + dto.employeeNameBn;
    }

    @Override
    public void set(PreparedStatement ps, Fund_managementDTO dto, boolean isInsert) throws SQLException {
        setSearchColumn(dto);
        int index = 0;
        ps.setInt(++index, dto.fundTypeCat);
        ps.setString(++index, dto.cgsNumber);
        ps.setInt(++index, dto.advanceAmount);
        ps.setInt(++index, dto.q1Response);
        ps.setString(++index, dto.q2Response);
        ps.setInt(++index, dto.q3Response);
        ps.setString(++index, dto.q4Response);
        ps.setString(++index, dto.q5Response);
        ps.setInt(++index, dto.q6aResponse);
        ps.setInt(++index, dto.q6bResponse);
        ps.setInt(++index, dto.q7aResponse);
        ps.setInt(++index, dto.q7bResponse);
        ps.setString(++index, dto.q8Response);
        ps.setString(++index, dto.q9Response);
        ps.setLong(++index, dto.employeeRecordsId);
        ps.setString(++index, dto.employeeNameEn);
        ps.setString(++index, dto.employeeNameBn);
        ps.setString(++index, dto.employeeUserName);
        ps.setLong(++index, dto.officeUnitId);
        ps.setString(++index, dto.officeNameEn);
        ps.setString(++index, dto.officeNameBn);
        ps.setLong(++index, dto.organogramId);
        ps.setString(++index, dto.organogramNameEn);
        ps.setString(++index, dto.organogramNameBn);
        ps.setString(++index, dto.comment);
        ps.setString(++index, dto.searchColumn);
        ps.setInt(++index, dto.fundApplicationStatusCat);
        ps.setLong(++index, dto.modifiedBy);
        ps.setLong(++index, dto.lastModificationTime);
        if (isInsert) {
            ps.setLong(++index, dto.insertedBy);
            ps.setLong(++index, dto.insertionDate);
            ps.setInt(++index, 0 /* isDeleted */);
        }
        ps.setLong(++index, dto.iD);
    }

    @Override
    public Fund_managementDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Fund_managementDTO dto = new Fund_managementDTO();

            dto.iD = rs.getLong("ID");
            dto.fundTypeCat = rs.getInt("fund_type_cat");
            dto.cgsNumber = rs.getString("cgs_number");
            dto.advanceAmount = rs.getInt("advance_amount");
            dto.q1Response = rs.getInt("q1_response");
            dto.q2Response = rs.getString("q2_response");
            dto.q3Response = rs.getInt("q3_response");
            dto.q4Response = rs.getString("q4_response");
            dto.q5Response = rs.getString("q5_response");
            dto.q6aResponse = rs.getInt("q6a_response");
            dto.q6bResponse = rs.getInt("q6b_response");
            dto.q7aResponse = rs.getInt("q7a_response");
            dto.q7bResponse = rs.getInt("q7b_response");
            dto.q8Response = rs.getString("q8_response");
            dto.q9Response = rs.getString("q9_response");
            dto.employeeRecordsId = rs.getLong("employee_records_id");
            dto.employeeNameEn = rs.getString("employee_name_en");
            dto.employeeNameBn = rs.getString("employee_name_bn");
            dto.employeeUserName = rs.getString("employee_user_name");
            dto.officeUnitId = rs.getLong("office_unit_id");
            dto.officeNameEn = rs.getString("office_name_en");
            dto.officeNameBn = rs.getString("office_name_bn");
            dto.organogramId = rs.getLong("organogram_id");
            dto.organogramNameEn = rs.getString("organogram_name_en");
            dto.organogramNameBn = rs.getString("organogram_name_bn");
            dto.comment = rs.getString("comment");
            dto.searchColumn = rs.getString("search_column");
            dto.fundApplicationStatusCat = rs.getInt("fund_application_status_cat");
            dto.modifiedBy = rs.getLong("modified_by");
            dto.lastModificationTime = rs.getLong("lastModificationTime");
            dto.insertedBy = rs.getLong("inserted_by");
            dto.insertionDate = rs.getLong("insertion_date");
            dto.isDeleted = rs.getInt("isDeleted");

            return dto;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    @Override
    public String getTableName() {
        return "fund_management";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Fund_managementDTO) commonDTO, addSqlQuery, true);
    }

    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Fund_managementDTO) commonDTO, updateSqlQuery, false);
    }

    public Fund_managementDTO getDTOByID(long ID) {
        return getDTOFromID(ID);
    }
}