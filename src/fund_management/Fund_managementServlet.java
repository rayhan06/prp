package fund_management;

import card_info.CardApprovalResponse;
import common.BaseServlet;
import common.CommonDAOService;
import common.RoleEnum;
import employee_records.EmployeeFlatInfoDTO;
import fund_approval_mapping.FundApprovalModel;
import fund_approval_mapping.FundApprovalNotification;
import fund_approval_mapping.Fund_approval_mappingDAO;
import language.LM;
import login.LoginDTO;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import pb.Utils;
import permission.MenuConstants;
import sessionmanager.SessionConstants;
import task_type.TaskTypeEnum;
import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@WebServlet("/Fund_managementServlet")
@MultipartConfig
@SuppressWarnings({"Duplicates"})
public class Fund_managementServlet extends BaseServlet {
    private static final long serialVersionUID = 1L;
    private static final Logger logger = Logger.getLogger(Fund_managementServlet.class);
    public static final int TEXT_AREA_MAX_LEN = 1024;


    @Override
    public String getTableName() {
        return Fund_managementDAO.getInstance().getTableName();
    }

    @Override
    public String getServletName() {
        return "Fund_managementServlet";
    }

    @Override
    public Fund_managementDAO getCommonDAOService() {
        return Fund_managementDAO.getInstance();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }

        try {
            String actionType = request.getParameter("actionType");
            if ("search".equals(actionType)) {
                if (Utils.checkPermission(String.valueOf(userDTO.employee_record_id), userDTO, MenuConstants.FUND_MANAGEMENT_SEARCH)) {
                    if (userDTO.roleID == RoleEnum.EMPLOYEE_OF_PARLIAMENT.getRoleId()) {
                        Map<String, String> extraCriteriaMap = new HashMap<>();
                        extraCriteriaMap.put("employeeRecordIdInternal", String.valueOf(userDTO.employee_record_id));
                        request.setAttribute(CommonDAOService.CRITERIA_MAP_KEY, extraCriteriaMap);
                    }
                    search(request, response);
                    return;
                }
            } else {
                super.doGet(request, response);
                return;
            }
        } catch (Exception ex) {
            logger.error(ex);
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        if (!addFlag)
            throw new UnsupportedOperationException("edit operation not supported in Fund_management");

        long currentTime = System.currentTimeMillis();
        Fund_managementDTO fundManagementDTO = new Fund_managementDTO();
        fundManagementDTO.insertedBy = userDTO.ID;
        fundManagementDTO.insertionDate = currentTime;
        fundManagementDTO.modifiedBy = userDTO.ID;
        fundManagementDTO.lastModificationTime = currentTime;

        fundManagementDTO.fundTypeCat = Integer.parseInt(Jsoup.clean(
                request.getParameter("fundTypeCat"), Whitelist.simpleText()
        ));
        fundManagementDTO.cgsNumber = Jsoup.clean(
                request.getParameter("cgsNumber"), Whitelist.simpleText()
        );
        fundManagementDTO.advanceAmount = Integer.parseInt(Jsoup.clean(
                request.getParameter("advanceAmount"), Whitelist.simpleText()
        ));
        fundManagementDTO.q1Response = Integer.parseInt(Jsoup.clean(
                request.getParameter("q1Response"), Whitelist.simpleText()
        ));
        fundManagementDTO.q2Response = Jsoup.clean(
                request.getParameter("q2Response"), Whitelist.simpleText()
        );
        fundManagementDTO.q3Response = Integer.parseInt(Jsoup.clean(
                request.getParameter("q3Response"), Whitelist.simpleText()
        ));
        fundManagementDTO.q4Response = Jsoup.clean(
                request.getParameter("q4Response"), Whitelist.simpleText()
        );
        fundManagementDTO.q5Response = Jsoup.clean(
                request.getParameter("q5Response"), Whitelist.simpleText()
        );

        String Value;
        // 6a, 6b not mandatory
        Value = request.getParameter("q6aResponse");
        if (Value != null && !Value.equalsIgnoreCase("")) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }
        if (Value != null && !Value.equalsIgnoreCase("")) {
            fundManagementDTO.q6aResponse = Integer.parseInt(Value);
        }
        Value = request.getParameter("q6bResponse");
        if (Value != null && !Value.equalsIgnoreCase("")) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }
        if (Value != null && !Value.equalsIgnoreCase("")) {
            fundManagementDTO.q6bResponse = Integer.parseInt(Value);
        }

        fundManagementDTO.q7aResponse = Integer.parseInt(Jsoup.clean(
                request.getParameter("q7aResponse"), Whitelist.simpleText()
        ));
        fundManagementDTO.q7bResponse = Integer.parseInt(Jsoup.clean(
                request.getParameter("q7bResponse"), Whitelist.simpleText()
        ));
        fundManagementDTO.q8Response = Jsoup.clean(
                request.getParameter("q8Response"), Whitelist.simpleText()
        );

        Value = request.getParameter("q9Response");
        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }
        if (Value != null) {
            fundManagementDTO.q9Response = (Value);
        }

        fundManagementDTO.setEmployeeInfo(
                EmployeeFlatInfoDTO.getFlatInfoOfDefaultOffice(
                        userDTO.employee_record_id, LM.getLanguage(userDTO)
                )
        );

        Fund_managementDAO.getInstance().add(fundManagementDTO);

        FundApprovalModel model =
                new FundApprovalModel.FundApprovalModelBuilder()
                        .setFundManagementDTO(fundManagementDTO)
                        .setRequesterEmployeeRecordId(userDTO.employee_record_id)
                        .setTaskTypeId(TaskTypeEnum.FUND_MANAGEMENT.getValue())
                        .build();
        CardApprovalResponse cardApprovalResponse = Fund_approval_mappingDAO.getInstance().createFundApproval(model);
        FundApprovalNotification.getInstance().sendWaitingForApprovalNotification(
                cardApprovalResponse.organogramIds, fundManagementDTO
        );
        return fundManagementDTO;
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[]{MenuConstants.FUND_MANAGEMENT_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[]{MenuConstants.FUND_MANAGEMENT_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[]{MenuConstants.FUND_MANAGEMENT_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Fund_managementServlet.class;
    }
}