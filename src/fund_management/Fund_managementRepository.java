package fund_management;

import org.apache.log4j.Logger;
import repository.Repository;
import repository.RepositoryManager;
import util.LockManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import static java.util.stream.Collectors.toMap;


public class Fund_managementRepository implements Repository {
    private static final Logger logger = Logger.getLogger(Fund_managementRepository.class);
    Map<Long, Fund_managementDTO> mapById;

    private Fund_managementRepository() {
        mapById = new ConcurrentHashMap<>();
        RepositoryManager.getInstance().addRepository(this);
    }

    private static class LazyLoader {
        final static Fund_managementRepository INSTANCE = new Fund_managementRepository();
    }

    public static Fund_managementRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

    public void reload(boolean reloadAll) {
        logger.debug("Fund_managementRepository reload start for, reloadAll : " + reloadAll);
        List<Fund_managementDTO> dtoList = Fund_managementDAO.getInstance().getAllDTOs(reloadAll);
        if (dtoList != null && dtoList.size() > 0) {
            dtoList.stream()
                   .peek(this::removeIfPresent)
                   .filter(dto -> dto.isDeleted == 0)
                   .forEach(dto -> mapById.put(dto.iD, dto));
        }
        logger.debug("Fund_managementRepository reload end for, reloadAll : " + reloadAll);
    }

    private void removeIfPresent(Fund_managementDTO newDTO) {
        if (newDTO == null) return;
        if (mapById.get(newDTO.iD) != null) {
            mapById.remove(newDTO.iD);
        }
    }

    public List<Fund_managementDTO> getDTOs() {
        return new ArrayList<>(mapById.values());
    }


    public Fund_managementDTO getDTOById(long id) {
        if (mapById.get(id) == null) {
            synchronized (LockManager.getLock(id + "FundManagementRepoGDBI")) {
                if (mapById.get(id) == null) {
                    Fund_managementDTO dto = Fund_managementDAO.getInstance().getDTOByID(id);
                    if (dto != null) {
                        mapById.put(dto.iD, dto);
                    }
                }
            }
        }
        return mapById.get(id);
    }

    @Override
    public String getTableName() {
        return "fund_management";
    }

    public Map<Long, Fund_managementDTO> getDTOsGroupedById(Set<Long> ids) {
        return mapById.values()
                      .stream()
                      .filter(dto -> ids.contains(dto.iD))
                      .collect(toMap(dto -> dto.iD, dto -> dto));
    }
}


