package fund_management;

import employee_records.EmployeeFlatInfoDTO;
import sessionmanager.SessionConstants;
import util.CommonDTO;

@SuppressWarnings({"Duplicates"})
public class Fund_managementDTO extends CommonDTO {
    public int fundTypeCat = 0;
    public String cgsNumber = "";
    public int advanceAmount = 0;
    public int q1Response = 0;
    public String q2Response = "";
    public int q3Response = 0;
    public String q4Response = "";
    public String q5Response = "";
    public int q6aResponse = 0;
    public int q6bResponse = 0;
    public int q7aResponse = 0;
    public int q7bResponse = 0;
    public String q8Response = "";
    public String q9Response = "";
    public long employeeRecordsId = 0;
    public String employeeNameEn = "";
    public String employeeNameBn = "";
    public String employeeUserName = "";
    public long officeUnitId = 0;
    public String officeNameEn = "";
    public String officeNameBn = "";
    public long organogramId = 0;
    public String organogramNameEn = "";
    public String organogramNameBn = "";
    public int fundApplicationStatusCat = 1;
    public long modifiedBy = 0;
    public long insertedBy = 0;
    public long insertionDate = SessionConstants.MIN_DATE;
    public String comment = "";

    public void setEmployeeInfo(EmployeeFlatInfoDTO employeeFlatInfoDTO){
        employeeRecordsId = employeeFlatInfoDTO.employeeRecordsId;
        employeeNameEn = employeeFlatInfoDTO.employeeNameEn;
        employeeNameBn = employeeFlatInfoDTO.employeeNameBn;
        employeeUserName = employeeFlatInfoDTO.employeeUserName;

        officeUnitId = employeeFlatInfoDTO.officeUnitId;
        officeNameEn = employeeFlatInfoDTO.officeNameEn;
        officeNameBn = employeeFlatInfoDTO.officeNameBn;

        organogramId = employeeFlatInfoDTO.organogramId;
        organogramNameEn = employeeFlatInfoDTO.organogramNameEn;
        organogramNameBn = employeeFlatInfoDTO.organogramNameBn;
    }

    @Override
    public String toString() {
        return "Fund_managementDTO{" +
               "fundTypeCat=" + fundTypeCat +
               ", cgsNumber='" + cgsNumber + '\'' +
               ", advanceAmount=" + advanceAmount +
               ", q1Response=" + q1Response +
               ", q2Response='" + q2Response + '\'' +
               ", q3Response=" + q3Response +
               ", q4Response=" + q4Response +
               ", q5Response=" + q5Response +
               ", q6aResponse=" + q6aResponse +
               ", q6bResponse=" + q6bResponse +
               ", q7aResponse=" + q7aResponse +
               ", q7bResponse=" + q7bResponse +
               ", q8Response=" + q8Response +
               ", q9Response='" + q9Response + '\'' +
               ", employeeRecordsId=" + employeeRecordsId +
               ", employeeNameEn='" + employeeNameEn + '\'' +
               ", employeeNameBn='" + employeeNameBn + '\'' +
               ", employeeUserName='" + employeeUserName + '\'' +
               ", officeUnitId=" + officeUnitId +
               ", officeNameEn='" + officeNameEn + '\'' +
               ", officeNameBn='" + officeNameBn + '\'' +
               ", organogramId=" + organogramId +
               ", organogramNameEn='" + organogramNameEn + '\'' +
               ", organogramNameBn='" + organogramNameBn + '\'' +
               ", modifiedBy=" + modifiedBy +
               ", insertedBy=" + insertedBy +
               ", insertionDate=" + insertionDate +
               '}';
    }
}