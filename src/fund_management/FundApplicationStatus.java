package fund_management;

import java.util.Arrays;

public enum FundApplicationStatus {
    WAITING_FOR_APPROVAL(1, "#22ccc1"),
    APPROVED(2, "green"),
    REJECTED(3, "crimson"),
    ;

    private final int value;
    private final String color;

    FundApplicationStatus(int value, String color) {
        this.value = value;
        this.color = color;
    }

    public int getValue() {
        return value;
    }

    public String getColor() {
        return color;
    }

    public static String getColor(int value) {
        return Arrays.stream(values())
                     .filter(fundApplicationStatus -> fundApplicationStatus.value == value)
                     .map(FundApplicationStatus::getColor)
                     .findFirst()
                     .orElse("");
    }
}
