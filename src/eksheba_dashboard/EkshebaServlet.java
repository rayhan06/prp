/*
package eksheba_dashboard;

import java.io.IOException;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import login.LoginDTO;
import permission.MenuConstants;
import role.PermissionRepository;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import user.UserTypeDTO;
import user.UserTypeRepository;


@WebServlet("/EkshebaServlet")
public class EkshebaServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(EkshebaServlet.class);

    */
/**
     * @see HttpServlet#HttpServlet()
     *//*

    public EkshebaServlet() {
        super();
    }

    */
/**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     *//*

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        System.out.println("In Eksheba_dashboardServlet");
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        UserTypeDTO userTypeDTO = UserTypeRepository.getInstance().getUserTypeByUserTypeID(userDTO.userType);

        try {
            String actionType = request.getParameter("actionType");
            if (actionType.equals("getDashboard")) {
                System.out.println("roleID = " + userDTO.roleID + " userID = " + userDTO.ID + " username = " + userDTO.userName);
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.SEMEN_DASHBOARD)) {
                    System.out.println("usertype = " + userDTO.userType);
                    System.out.println("user dashboard = " + userTypeDTO.dashboard);
                    request.getRequestDispatcher(userTypeDTO.dashboard).forward(request, response);
                }

            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.debug(ex);
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
*/
