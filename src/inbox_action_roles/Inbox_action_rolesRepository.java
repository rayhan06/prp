package inbox_action_roles;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class Inbox_action_rolesRepository implements Repository {
	Inbox_action_rolesDAO inbox_action_rolesDAO = null;
	
	public void setDAO(Inbox_action_rolesDAO inbox_action_rolesDAO)
	{
		this.inbox_action_rolesDAO = inbox_action_rolesDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Inbox_action_rolesRepository.class);
	Map<Long, Inbox_action_rolesDTO>mapOfInbox_action_rolesDTOToid;
	Map<Integer, Set<Inbox_action_rolesDTO> >mapOfInbox_action_rolesDTOToinboxStatusId;
	Map<Long, Set<Inbox_action_rolesDTO> >mapOfInbox_action_rolesDTOToroleId;
	Map<Long, Set<Inbox_action_rolesDTO> >mapOfInbox_action_rolesDTOToactionId;
	Map<Long, Set<Inbox_action_rolesDTO> >mapOfInbox_action_rolesDTOTolastModificationTime;


	static Inbox_action_rolesRepository instance = null;  
	private Inbox_action_rolesRepository(){
		mapOfInbox_action_rolesDTOToid = new ConcurrentHashMap<>();
		mapOfInbox_action_rolesDTOToinboxStatusId = new ConcurrentHashMap<>();
		mapOfInbox_action_rolesDTOToroleId = new ConcurrentHashMap<>();
		mapOfInbox_action_rolesDTOToactionId = new ConcurrentHashMap<>();
		mapOfInbox_action_rolesDTOTolastModificationTime = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Inbox_action_rolesRepository getInstance(){
		if (instance == null){
			instance = new Inbox_action_rolesRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(inbox_action_rolesDAO == null)
		{
			return;
		}
		try {
			List<Inbox_action_rolesDTO> inbox_action_rolesDTOs = inbox_action_rolesDAO.getAllInbox_action_roles(reloadAll);
			for(Inbox_action_rolesDTO inbox_action_rolesDTO : inbox_action_rolesDTOs) {
				Inbox_action_rolesDTO oldInbox_action_rolesDTO = getInbox_action_rolesDTOByid(inbox_action_rolesDTO.id);
				if( oldInbox_action_rolesDTO != null ) {
					mapOfInbox_action_rolesDTOToid.remove(oldInbox_action_rolesDTO.id);
				
					if(mapOfInbox_action_rolesDTOToinboxStatusId.containsKey(oldInbox_action_rolesDTO.inboxStatusId)) {
						mapOfInbox_action_rolesDTOToinboxStatusId.get(oldInbox_action_rolesDTO.inboxStatusId).remove(oldInbox_action_rolesDTO);
					}
					if(mapOfInbox_action_rolesDTOToinboxStatusId.get(oldInbox_action_rolesDTO.inboxStatusId).isEmpty()) {
						mapOfInbox_action_rolesDTOToinboxStatusId.remove(oldInbox_action_rolesDTO.inboxStatusId);
					}
					
					if(mapOfInbox_action_rolesDTOToroleId.containsKey(oldInbox_action_rolesDTO.roleId)) {
						mapOfInbox_action_rolesDTOToroleId.get(oldInbox_action_rolesDTO.roleId).remove(oldInbox_action_rolesDTO);
					}
					if(mapOfInbox_action_rolesDTOToroleId.get(oldInbox_action_rolesDTO.roleId).isEmpty()) {
						mapOfInbox_action_rolesDTOToroleId.remove(oldInbox_action_rolesDTO.roleId);
					}
					
					if(mapOfInbox_action_rolesDTOToactionId.containsKey(oldInbox_action_rolesDTO.actionId)) {
						mapOfInbox_action_rolesDTOToactionId.get(oldInbox_action_rolesDTO.actionId).remove(oldInbox_action_rolesDTO);
					}
					if(mapOfInbox_action_rolesDTOToactionId.get(oldInbox_action_rolesDTO.actionId).isEmpty()) {
						mapOfInbox_action_rolesDTOToactionId.remove(oldInbox_action_rolesDTO.actionId);
					}
					
					if(mapOfInbox_action_rolesDTOTolastModificationTime.containsKey(oldInbox_action_rolesDTO.lastModificationTime)) {
						mapOfInbox_action_rolesDTOTolastModificationTime.get(oldInbox_action_rolesDTO.lastModificationTime).remove(oldInbox_action_rolesDTO);
					}
					if(mapOfInbox_action_rolesDTOTolastModificationTime.get(oldInbox_action_rolesDTO.lastModificationTime).isEmpty()) {
						mapOfInbox_action_rolesDTOTolastModificationTime.remove(oldInbox_action_rolesDTO.lastModificationTime);
					}
					
					
				}
				if(inbox_action_rolesDTO.isDeleted == 0) 
				{
					
					mapOfInbox_action_rolesDTOToid.put(inbox_action_rolesDTO.id, inbox_action_rolesDTO);
				
					if( ! mapOfInbox_action_rolesDTOToinboxStatusId.containsKey(inbox_action_rolesDTO.inboxStatusId)) {
						mapOfInbox_action_rolesDTOToinboxStatusId.put(inbox_action_rolesDTO.inboxStatusId, new HashSet<>());
					}
					mapOfInbox_action_rolesDTOToinboxStatusId.get(inbox_action_rolesDTO.inboxStatusId).add(inbox_action_rolesDTO);
					
					if( ! mapOfInbox_action_rolesDTOToroleId.containsKey(inbox_action_rolesDTO.roleId)) {
						mapOfInbox_action_rolesDTOToroleId.put(inbox_action_rolesDTO.roleId, new HashSet<>());
					}
					mapOfInbox_action_rolesDTOToroleId.get(inbox_action_rolesDTO.roleId).add(inbox_action_rolesDTO);
					
					if( ! mapOfInbox_action_rolesDTOToactionId.containsKey(inbox_action_rolesDTO.actionId)) {
						mapOfInbox_action_rolesDTOToactionId.put(inbox_action_rolesDTO.actionId, new HashSet<>());
					}
					mapOfInbox_action_rolesDTOToactionId.get(inbox_action_rolesDTO.actionId).add(inbox_action_rolesDTO);
					
					if( ! mapOfInbox_action_rolesDTOTolastModificationTime.containsKey(inbox_action_rolesDTO.lastModificationTime)) {
						mapOfInbox_action_rolesDTOTolastModificationTime.put(inbox_action_rolesDTO.lastModificationTime, new HashSet<>());
					}
					mapOfInbox_action_rolesDTOTolastModificationTime.get(inbox_action_rolesDTO.lastModificationTime).add(inbox_action_rolesDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Inbox_action_rolesDTO> getInbox_action_rolesList() {
		List <Inbox_action_rolesDTO> inbox_action_roless = new ArrayList<Inbox_action_rolesDTO>(this.mapOfInbox_action_rolesDTOToid.values());
		return inbox_action_roless;
	}
	
	
	public Inbox_action_rolesDTO getInbox_action_rolesDTOByid( long id){
		return mapOfInbox_action_rolesDTOToid.get(id);
	}
	
	
	public List<Inbox_action_rolesDTO> getInbox_action_rolesDTOByinbox_status_id(int inbox_status_id) {
		return new ArrayList<>( mapOfInbox_action_rolesDTOToinboxStatusId.getOrDefault(inbox_status_id,new HashSet<>()));
	}
	
	
	public List<Inbox_action_rolesDTO> getInbox_action_rolesDTOByrole_id(long role_id) {
		return new ArrayList<>( mapOfInbox_action_rolesDTOToroleId.getOrDefault(role_id,new HashSet<>()));
	}
	
	
	public List<Inbox_action_rolesDTO> getInbox_action_rolesDTOByaction_id(long action_id) {
		return new ArrayList<>( mapOfInbox_action_rolesDTOToactionId.getOrDefault(action_id,new HashSet<>()));
	}
	
	
	public List<Inbox_action_rolesDTO> getInbox_action_rolesDTOBylastModificationTime(long lastModificationTime) {
		return new ArrayList<>( mapOfInbox_action_rolesDTOTolastModificationTime.getOrDefault(lastModificationTime,new HashSet<>()));
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "inbox_action_roles";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


