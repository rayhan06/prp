package inbox_action_roles;
import java.util.*; 
import util.*;


public class Inbox_action_rolesMAPS extends CommonMaps
{	
	public Inbox_action_rolesMAPS(String tableName)
	{
		
		java_allfield_type_map.put("inbox_status_id".toLowerCase(), "Integer");
		java_allfield_type_map.put("role_id".toLowerCase(), "Long");
		java_allfield_type_map.put("action_id".toLowerCase(), "Long");

		java_anyfield_search_map.put(tableName + ".inbox_status_id".toLowerCase(), "Integer");
		java_anyfield_search_map.put(tableName + ".role_id".toLowerCase(), "Long");
		java_anyfield_search_map.put(tableName + ".action_id".toLowerCase(), "Long");

		java_DTO_map.put("id".toLowerCase(), "id".toLowerCase());
		java_DTO_map.put("inboxStatusId".toLowerCase(), "inboxStatusId".toLowerCase());
		java_DTO_map.put("roleId".toLowerCase(), "roleId".toLowerCase());
		java_DTO_map.put("actionId".toLowerCase(), "actionId".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("inbox_status_id".toLowerCase(), "inboxStatusId".toLowerCase());
		java_SQL_map.put("role_id".toLowerCase(), "roleId".toLowerCase());
		java_SQL_map.put("action_id".toLowerCase(), "actionId".toLowerCase());

		java_Text_map.put("Id".toLowerCase(), "id".toLowerCase());
		java_Text_map.put("Inbox Status Id".toLowerCase(), "inboxStatusId".toLowerCase());
		java_Text_map.put("Role Id".toLowerCase(), "roleId".toLowerCase());
		java_Text_map.put("Action Id".toLowerCase(), "actionId".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}