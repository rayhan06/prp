package inbox_action_roles;
import java.util.*; 
import util.*; 


public class Inbox_action_rolesDTO extends CommonDTO
{

	public long id = 0;
	public int inboxStatusId = 0;
	public long roleId = 0;
	public long actionId = 0;
	
	
    @Override
	public String toString() {
            return "$Inbox_action_rolesDTO[" +
            " id = " + id +
            " inboxStatusId = " + inboxStatusId +
            " roleId = " + roleId +
            " actionId = " + actionId +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}