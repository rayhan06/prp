package inbox_action_roles;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;


import org.apache.log4j.Logger;

import dbm.*;

import repository.RepositoryManager;

import util.*;
import pb.*;
import approval_module_map.*;

public class Inbox_action_rolesDAO  extends NavigationService3
{
	
	Logger logger = Logger.getLogger(getClass());

	
	public Inbox_action_rolesDAO(String tableName, String tempTableName, Approval_module_mapDTO approval_module_mapDTO)
	{
		super(tableName, tempTableName, approval_module_mapDTO);		
	}
	
	
	
	public long add(CommonDTO commonDTO, String tableName, TempTableDTO tempTableDTO) throws Exception
	{
		
		Inbox_action_rolesDTO inbox_action_rolesDTO = (Inbox_action_rolesDTO)commonDTO;
		
		Connection connection = null;
		PreparedStatement ps = null;

		long lastModificationTime = System.currentTimeMillis();	

		try{
			connection = DBMW.getInstance().getConnection();
			
			if(connection == null)
			{
				System.out.println("nullconn");
			}

			inbox_action_rolesDTO.iD = DBMW.getInstance().getNextSequenceId(tableName);

			String sql = "INSERT INTO " + tableName;
			
			sql += " (";
			sql += "id";
			sql += ", ";
			sql += "inbox_status_id";
			sql += ", ";
			sql += "role_id";
			sql += ", ";
			sql += "action_id";
			sql += ", ";
			sql += "isDeleted";
			sql += ", ";
			sql += "lastModificationTime";
			if(tempTableDTO!=null)
			{
				sql += ", permanent_table_id, operation_type, approval_path_type, approval_order";
			}
			sql += ")";
			
			
            sql += " VALUES(";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			if(tempTableDTO!=null)
			{
				sql += ", " + tempTableDTO.permanent_table_id + ", " + tempTableDTO.operation_type + ", " + tempTableDTO.approval_path_type + ", " + tempTableDTO.approval_order;
			}
			sql += ")";
				
			printSql(sql);

			ps = connection.prepareStatement(sql);
			
			
			

			int index = 1;

			ps.setObject(index++,inbox_action_rolesDTO.id);
			ps.setObject(index++,inbox_action_rolesDTO.inboxStatusId);
			ps.setObject(index++,inbox_action_rolesDTO.roleId);
			ps.setObject(index++,inbox_action_rolesDTO.actionId);
			ps.setObject(index++,inbox_action_rolesDTO.isDeleted);
			ps.setObject(index++, lastModificationTime);
			
			System.out.println(ps);
			ps.execute();
			
			
			recordUpdateTime(connection, lastModificationTime, tableName);

		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if (ps != null) {
					ps.close();
				}
			} catch(Exception e){}
			try{
				if(connection != null){
					DBMW.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		return inbox_action_rolesDTO.id;		
	}
		
	

	//need another getter for repository
	public CommonDTO getDTOByID (long ID, String tableName) throws Exception
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Inbox_action_rolesDTO inbox_action_rolesDTO = null;
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
			
            sql += " WHERE id=" + ID;
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			if(rs.next()){
				inbox_action_rolesDTO = new Inbox_action_rolesDTO();

				inbox_action_rolesDTO.id = rs.getLong("id");
				inbox_action_rolesDTO.inboxStatusId = rs.getInt("inbox_status_id");
				inbox_action_rolesDTO.roleId = rs.getLong("role_id");
				inbox_action_rolesDTO.actionId = rs.getLong("action_id");
				inbox_action_rolesDTO.isDeleted = rs.getInt("isDeleted");
				inbox_action_rolesDTO.lastModificationTime = rs.getLong("lastModificationTime");

			}			
			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DBMR.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
		return inbox_action_rolesDTO;
	}
	
	public long update(CommonDTO commonDTO, String tableName) throws Exception
	{		
		Inbox_action_rolesDTO inbox_action_rolesDTO = (Inbox_action_rolesDTO)commonDTO;
		
		Connection connection = null;
		PreparedStatement ps = null;

		long lastModificationTime = System.currentTimeMillis();	
		try{
			connection = DBMW.getInstance().getConnection();

			String sql = "UPDATE " + tableName;
			
			sql += " SET ";
			sql += "inbox_status_id=?";
			sql += ", ";
			sql += "role_id=?";
			sql += ", ";
			sql += "action_id=?";
			sql += ", lastModificationTime = "	+ lastModificationTime + "";
            sql += " WHERE id = " + inbox_action_rolesDTO.id;
				
			printSql(sql);

			ps = connection.prepareStatement(sql);
			
			

			int index = 1;
			
			ps.setObject(index++,inbox_action_rolesDTO.inboxStatusId);
			ps.setObject(index++,inbox_action_rolesDTO.roleId);
			ps.setObject(index++,inbox_action_rolesDTO.actionId);
			System.out.println(ps);
			ps.executeUpdate();
			


			
			recordUpdateTime(connection,  lastModificationTime, tableName);

						
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if (ps != null) {
					ps.close();
				}
			} catch(Exception e){}
			try{
				if(connection != null){
					DBMW.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		return inbox_action_rolesDTO.id;
	}
	
	public List<Inbox_action_rolesDTO> getDTOs(Collection recordIDs)
	{
		return getDTOs(recordIDs, tableName);
	}
	
	public List<Inbox_action_rolesDTO> getDTOs(Collection recordIDs, String tableName){
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Inbox_action_rolesDTO inbox_action_rolesDTO = null;
		List<Inbox_action_rolesDTO> inbox_action_rolesDTOList = new ArrayList<>();
		if(recordIDs.isEmpty()){
			return inbox_action_rolesDTOList;
		}
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
            
            sql += " WHERE id IN ( ";

			for(int i = 0;i<recordIDs.size();i++){
				if(i!=0){
					sql+=",";
				}
				sql+=((ArrayList)recordIDs).get(i);
			}
			sql+=")  order by lastModificationTime desc";
			
			printSql(sql);
			
			logger.debug("sql " + sql);
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				inbox_action_rolesDTO = new Inbox_action_rolesDTO();
				inbox_action_rolesDTO.id = rs.getLong("id");
				inbox_action_rolesDTO.inboxStatusId = rs.getInt("inbox_status_id");
				inbox_action_rolesDTO.roleId = rs.getLong("role_id");
				inbox_action_rolesDTO.actionId = rs.getLong("action_id");
				inbox_action_rolesDTO.isDeleted = rs.getInt("isDeleted");
				inbox_action_rolesDTO.lastModificationTime = rs.getLong("lastModificationTime");
				System.out.println("got this DTO: " + inbox_action_rolesDTO);
				
				inbox_action_rolesDTOList.add(inbox_action_rolesDTO);

			}			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DBMR.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
		return inbox_action_rolesDTOList;
	
	}
	
	

	
	
	
	//add repository
	public List<Inbox_action_rolesDTO> getAllInbox_action_roles (boolean isFirstReload)
    {
		List<Inbox_action_rolesDTO> inbox_action_rolesDTOList = new ArrayList<>();

		String sql = "SELECT * FROM inbox_action_roles";
		sql += " WHERE ";
	

		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by inbox_action_roles.lastModificationTime desc";
		printSql(sql);
		
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;

		
		try{
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();
			rs = stmt.executeQuery(sql);
			

			while(rs.next()){
				Inbox_action_rolesDTO inbox_action_rolesDTO = new Inbox_action_rolesDTO();
				inbox_action_rolesDTO.id = rs.getLong("id");
				inbox_action_rolesDTO.inboxStatusId = rs.getInt("inbox_status_id");
				inbox_action_rolesDTO.roleId = rs.getLong("role_id");
				inbox_action_rolesDTO.actionId = rs.getLong("action_id");
				inbox_action_rolesDTO.isDeleted = rs.getInt("isDeleted");
				inbox_action_rolesDTO.lastModificationTime = rs.getLong("lastModificationTime");
				
				inbox_action_rolesDTOList.add(inbox_action_rolesDTO);
			}			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			try{ 
				if (connection != null){ 
					DBMR.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}

		return inbox_action_rolesDTOList;
    }
	
	public List<Inbox_action_rolesDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset)
	{
		return getDTOs(p_searchCriteria, limit, offset, tableName, true, 0);
	}
	
	public List<Inbox_action_rolesDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, String tableName, boolean isPermanentTable, long userApprovalPathType)
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		List<Inbox_action_rolesDTO> inbox_action_rolesDTOList = new ArrayList<>();

		try{
			
			String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, tableName, isPermanentTable, userApprovalPathType);
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				Inbox_action_rolesDTO inbox_action_rolesDTO = new Inbox_action_rolesDTO();
				inbox_action_rolesDTO.id = rs.getLong("id");
				inbox_action_rolesDTO.inboxStatusId = rs.getInt("inbox_status_id");
				inbox_action_rolesDTO.roleId = rs.getLong("role_id");
				inbox_action_rolesDTO.actionId = rs.getLong("action_id");
				inbox_action_rolesDTO.isDeleted = rs.getInt("isDeleted");
				inbox_action_rolesDTO.lastModificationTime = rs.getLong("lastModificationTime");
				
				inbox_action_rolesDTOList.add(inbox_action_rolesDTO);
			}					
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DBMR.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
		return inbox_action_rolesDTOList;
	
	}

		
	
	public String getSqlWithSearchCriteria(Hashtable p_searchCriteria, int limit, int offset, int category, String tableName, boolean isPermanentTable, long userApprovalPathType)
    {
		Inbox_action_rolesMAPS maps = new Inbox_action_rolesMAPS(tableName);
		String joinSQL = "";
		return getSqlWithSearchCriteria(p_searchCriteria, limit, offset, category, tableName, isPermanentTable, userApprovalPathType, maps, joinSQL);
    }			
}
	