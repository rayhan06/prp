package election_report;


import election_constituency.Election_constituencyRepository;
import language.LC;
import language.LM;
import login.LoginDTO;
import org.apache.log4j.Logger;
import pbReport.PBReportUtils;
import pbReport.ReportCommonService;
import permission.MenuConstants;
import role.PermissionRepository;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;


@WebServlet("/Election_report_Servlet")
public class Election_report_Servlet extends HttpServlet implements ReportCommonService {
    private static final long serialVersionUID = 1L;
    private static final Logger logger = Logger.getLogger(Election_report_Servlet.class);
    private static final Set<String> searchParam = new HashSet<>(Arrays.asList(
            "electionDetailsId", "electionConstituencyId", "politicalPartyId",
            "districtId", "divisionId"
    ));
    private final Map<String, String[]> stringMap = new HashMap<>();
    private static final String sql = " election_wise_mp ewm ";
    private String[][] Criteria;
    private final String[][] Display =
            {
                    {"display", "ewm", "election_details_id", "parliament_number", ""},
                    {"display", "ewm", "election_constituency_id", "election_constituency", ""},
                    {"display", "ewm", "political_party_id", "political_party", ""},
                    {"display", "ewm", "employee_records_id", "employee_records_id", ""},
                    {"display", "ewm", "start_date", "date", ""},
                    {"display", "ewm", "end_date", "date", ""}
            };

    public Election_report_Servlet() {

        stringMap.put("electionDetailsId", new String[]{"criteria", "ewm", "election_details_id", "=", "AND", "String", "", "", "any",
                "electionDetailsId", "electionDetailsId", String.valueOf(LC.ELECTION_REPORT_WHERE_ELECTIONDETAILSID), "parliament_number", null, "true", "2"});

        stringMap.put("electionConstituencyId", new String[]{"criteria", "ewm", "election_constituency_id", "=", "AND", "String", "", "", "any",
                "electionConstituencyId", "electionConstituencyId", String.valueOf(LC.ELECTION_REPORT_WHERE_ELECTIONCONSTITUENCYID), "election_constituency", null, "true", "3"});

        stringMap.put("divisionId", new String[]{"criteria", "ewm", "election_constituency_id", "IN", "AND", "String", "", "", "any",
                "divisionId", "divisionId", String.valueOf(LC.ELECTION_CONSTITUENCY_ADD_GEODIVISIONTYPE), "division", null, "true", "4"});

        stringMap.put("districtId", new String[]{"criteria", "ewm", "election_constituency_id", "IN", "AND", "String", "", "", "any",
                "districtId", "districtId", String.valueOf(LC.ELECTION_CONSTITUENCY_ADD_GEODISTRICTTYPE), "district", null, "true", "5"});

        stringMap.put("politicalPartyId", new String[]{"criteria", "ewm", "political_party_id", "=", "AND", "String", "", "", "any",
                "politicalPartyId", "politicalPartyId", String.valueOf(LC.ELECTION_REPORT_WHERE_POLITICALPARTYID), "political_party", null, "true", "6"});

        stringMap.put("isDeleted", new String[]{"criteria", "ewm", "isDeleted", "=", "AND", "String", "", "", "0", "isDeleted",
                null, null, null, null, null, null});
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }

        Set<String> inputs = PBReportUtils.prepareInputSets(searchParam, request);
        inputs.add("isDeleted");

        Display[0][4] = LM.getText(LC.ELECTION_REPORT_SELECT_ELECTIONDETAILSID, loginDTO);
        Display[1][4] = LM.getText(LC.ELECTION_REPORT_SELECT_ELECTIONCONSTITUENCYID, loginDTO);
        Display[2][4] = LM.getText(LC.ELECTION_REPORT_SELECT_POLITICALPARTYID, loginDTO);
        Display[3][4] = LM.getText(LC.ELECTION_REPORT_SELECT_EMPLOYEERECORDSID, loginDTO);
        Display[4][4] = LM.getText(LC.ELECTION_REPORT_SELECT_STARTDATE, loginDTO);
        Display[5][4] = LM.getText(LC.ELECTION_REPORT_SELECT_ENDDATE, loginDTO);

        Criteria = PBReportUtils.prepareCriteria(inputs, stringMap);

        if (inputs.contains("divisionId")) {
            long divisionId = Long.parseLong(request.getParameter("divisionId"));
            Set<Long> electionConstituencyIds = Election_constituencyRepository.getInstance()
                    .getIdsByCondition(dto -> dto.geoDivisionsType == divisionId);

            for (String[] arr : Criteria) {
                if (arr[9].equals("divisionId")) {
                    arr[8] = electionConstituencyIds.stream()
                            .map(String::valueOf)
                            .collect(Collectors.joining(","));
                    break;
                }
            }
        } else if (inputs.contains("districtId")) {
            long districtId = Long.parseLong(request.getParameter("districtId"));
            Set<Long> electionConstituencyIds = Election_constituencyRepository.getInstance()
                    .getIdsByCondition(dto -> dto.geoDistrictsType == districtId);

            for (String[] arr : Criteria) {
                if (arr[9].equals("districtId")) {
                    arr[8] = electionConstituencyIds.stream()
                            .map(String::valueOf)
                            .collect(Collectors.joining(","));
                    break;
                }
            }
        }

        if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.SERVICE_HISTORY_REPORT_DETAILS)) {
            reportGenerate(request, response);
        }
    }

    @Override
    public String[][] getCriteria() {
        return Criteria;
    }

    @Override
    public String[][] getDisplay() {
        return Display;
    }

    @Override
    public String getSQL() {
        return sql;
    }

    @Override
    public int getLCForReportName() {
        return LC.ELECTION_REPORT_OTHER_ELECTION_REPORT;
    }

    @Override
    public String getFileName() {
        return "election_report";
    }

    @Override
    public String getTableName() {
        return "election_report";
    }
}
