-- election_report
SELECT ewm.election_details_id,
       ewm.election_constituency_id,
       ewm.political_party_id,
       ewm.employee_records_id,
       ewm.start_date,
       ewm.end_date
FROM election_wise_mp ewm
WHERE ewm.isDeleted = 0
  AND ewm.election_details_id = 'any'
  AND ewm.election_constituency_id = 'any'
  AND ewm.political_party_id = 'any'
  AND ewm.search_column = 'any'