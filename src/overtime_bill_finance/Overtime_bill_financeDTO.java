package overtime_bill_finance;

import overtime_bill.Overtime_billServlet;
import pb.CatRepository;
import pbReport.DateUtils;
import sessionmanager.SessionConstants;
import user.UserDTO;
import util.CommonDTO;

public class Overtime_bill_financeDTO extends CommonDTO {
    public long budgetMappingId = -1;
    public long economicSubCodeId = -1;
    public long otBillSubmissionConfigId = -1;
    public long billStartDate = SessionConstants.MIN_DATE;
    public long billEndDate = SessionConstants.MIN_DATE;
    public int overtimeBillTypeCat = 1;
    public long budgetRegisterId = -1;
    public long billRegisterId = -1;

    public long totalEmployeeCount = 0;
    public long totalBillAmount = 0;

    public long modifiedBy = -1;
    public long insertedBy = -1;
    public long insertionTime = -1;

    public String getBillDateText(boolean isLangEn) {
        return DateUtils.getDateRangeString(
                billStartDate,
                billEndDate,
                isLangEn
        );
    }

    public String getBillTypeText(boolean isLangEn) {
        return CatRepository.getInstance().getText(
                isLangEn,
                SessionConstants.OT_BILL_TYPE_CAT_DOMAIN_NAME,
                overtimeBillTypeCat
        );
    }

    public boolean isDeletableByUser(UserDTO userDTO) {
        return Overtime_billServlet.isAllowedToSeeAllOfficeBill(userDTO);
    }
}
