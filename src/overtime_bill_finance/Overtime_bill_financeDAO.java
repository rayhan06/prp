package overtime_bill_finance;

import common.CommonDAOService;
import org.apache.log4j.Logger;
import overtime_bill.Overtime_billDAO;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

@SuppressWarnings({"Duplicates"})
public class Overtime_bill_financeDAO implements CommonDAOService<Overtime_bill_financeDTO> {

    private static final Logger logger = Logger.getLogger(Overtime_bill_financeDAO.class);

    private static final String addSqlQuery =
            "INSERT INTO {tableName} (budget_mapping_id,economic_sub_code_id,ot_bill_submission_config_id,"
                    .concat("bill_start_date,bill_end_date,overtime_bill_type_cat,budget_register_id,bill_register_id,total_employee_count,total_bill_amount,")
                    .concat("modified_by,lastModificationTime,inserted_by,insertion_time,isDeleted,ID) ")
                    .concat("VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

    private static final String updateSqlQuery =
            "UPDATE {tableName} SET budget_mapping_id = ?,economic_sub_code_id = ?,ot_bill_submission_config_id = ?,"
                    .concat("bill_start_date=?,bill_end_date=?,overtime_bill_type_cat=?,budget_register_id = ?,bill_register_id = ?,total_employee_count = ?,total_bill_amount = ?,")
                    .concat("modified_by = ?,lastModificationTime = ? WHERE ID=?");

    private static final Map<String, String> searchMap = new HashMap<>();

    private Overtime_bill_financeDAO() {
        searchMap.put("budgetMappingId", " AND (budget_mapping_id = ?) ");
        searchMap.put("economicSubCodeId", " AND (economic_sub_code_id = ?) ");
        searchMap.put("otBillSubmissionConfigId", " AND (ot_bill_submission_config_id = ?) ");
    }

    private static class LazyLoader {
        static final Overtime_bill_financeDAO INSTANCE = new Overtime_bill_financeDAO();
    }

    public static Overtime_bill_financeDAO getInstance() {
        return LazyLoader.INSTANCE;
    }

    @Override
    public void set(PreparedStatement ps, Overtime_bill_financeDTO dto, boolean isInsert) throws SQLException {
        int index = 0;
        ps.setLong(++index, dto.budgetMappingId);
        ps.setLong(++index, dto.economicSubCodeId);
        ps.setLong(++index, dto.otBillSubmissionConfigId);
        ps.setLong(++index, dto.billStartDate);
        ps.setLong(++index, dto.billEndDate);
        ps.setInt(++index, dto.overtimeBillTypeCat);
        ps.setLong(++index, dto.budgetRegisterId);
        ps.setLong(++index, dto.billRegisterId);
        ps.setLong(++index, dto.totalEmployeeCount);
        ps.setLong(++index, dto.totalBillAmount);
        ps.setLong(++index, dto.modifiedBy);
        ps.setLong(++index, dto.lastModificationTime);
        if (isInsert) {
            ps.setLong(++index, dto.insertedBy);
            ps.setLong(++index, dto.insertionTime);
            ps.setInt(++index, 0 /* isDeleted */);
        }
        ps.setLong(++index, dto.iD);
    }

    @Override
    public Overtime_bill_financeDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Overtime_bill_financeDTO dto = new Overtime_bill_financeDTO();
            dto.iD = rs.getLong("ID");
            dto.budgetMappingId = rs.getLong("budget_mapping_id");
            dto.economicSubCodeId = rs.getLong("economic_sub_code_id");
            dto.otBillSubmissionConfigId = rs.getLong("ot_bill_submission_config_id");
            dto.billStartDate = rs.getLong("bill_start_date");
            dto.billEndDate = rs.getLong("bill_end_date");
            dto.overtimeBillTypeCat = rs.getInt("overtime_bill_type_cat");
            dto.budgetRegisterId = rs.getLong("budget_register_id");
            dto.billRegisterId = rs.getLong("bill_register_id");
            dto.totalEmployeeCount = rs.getLong("total_employee_count");
            dto.totalBillAmount = rs.getLong("total_bill_amount");
            dto.modifiedBy = rs.getLong("modified_by");
            dto.lastModificationTime = rs.getLong("lastModificationTime");
            dto.insertedBy = rs.getLong("inserted_by");
            dto.insertionTime = rs.getLong("insertion_time");
            dto.isDeleted = rs.getInt("isDeleted");
            return dto;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    @Override
    public String getTableName() {
        return "overtime_bill_finance";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    @Override
    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Overtime_bill_financeDTO) commonDTO, addSqlQuery, true);
    }

    @Override
    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Overtime_bill_financeDTO) commonDTO, updateSqlQuery, false);
    }

    public boolean deleteBillById(long id, long modifiedBy, long modificationTime) {
        Overtime_billDAO.getInstance().financeBillDeleted(id, modifiedBy, modificationTime);
        return delete(modifiedBy, id, modificationTime);
    }
}
