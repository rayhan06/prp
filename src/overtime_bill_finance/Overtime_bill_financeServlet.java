package overtime_bill_finance;

import common.BaseServlet;
import common.CommonDAOService;
import common.CustomException;
import login.LoginDTO;
import org.apache.log4j.Logger;
import pb.Utils;
import permission.MenuConstants;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;
import util.HttpRequestUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@SuppressWarnings({"Duplicates"})
@WebServlet("/Overtime_bill_financeServlet")
@MultipartConfig
public class Overtime_bill_financeServlet extends BaseServlet {
    private static final long serialVersionUID = 1L;
    private final Logger logger = Logger.getLogger(Overtime_bill_financeServlet.class);

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }
        String actionType = request.getParameter("actionType");
        try {
            switch (actionType) {
                case "view":
                case "search":
                    super.doGet(request, response);
                    return;
            }
        } catch (Exception ex) {
            logger.error(ex);
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }

        String actionType = request.getParameter("actionType");
        try {
            if ("ajax_deleteBill".equals(actionType)) {
                Map<String, Object> res = deleteBill(request, userDTO);
                response.setContentType("application/json");
                response.setCharacterEncoding("UTF-8");
                response.getWriter().print(gson.toJson(res));
            }
            return;
        } catch (Exception ex) {
            logger.error(ex);
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }

    public void deleteBillDtoOrThrow(String idStr, UserDTO userDTO, boolean isLangEn) throws Exception {
        long id = Utils.parseMandatoryLong(idStr, "invalid id=" + idStr);
        Overtime_bill_financeDTO billFinanceDTO = Overtime_bill_financeDAO.getInstance().getDTOFromID(id);
        if (billFinanceDTO == null) {
            throw new CustomException(isLangEn ? "Already deleted!" : "ইতোমধ্যে ডিলিট করা হয়েছে!");
        }
        if (!billFinanceDTO.isDeletableByUser(userDTO)) {
            throw new CustomException(isLangEn ? "You are not allowed to delete" : "আপনার ডিলিট করার অনুমতি নেই");
        }
        boolean isDeleted = Overtime_bill_financeDAO.getInstance().deleteBillById(id, userDTO.ID, System.currentTimeMillis());
        if (!isDeleted) {
            throw new RuntimeException("failed to delete Overtime_bill_financeDTO.id=" + id);
        }
    }

    private Map<String, Object> deleteBill(HttpServletRequest request, UserDTO userDTO) {
        boolean isLangEn = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
        Map<String, Object> res = new HashMap<>();
        res.put("success", true);
        res.put("message", isLangEn ? "Deleted successfully" : "সফলভাবে ডিলিট করা হয়েছে");

        try {
            deleteBillDtoOrThrow(request.getParameter("ID"), userDTO, isLangEn);
        } catch (CustomException customException) {
            logger.error(customException);
            res.put("success", false);
            res.put("message", customException.getMessage());
        } catch (Exception e) {
            logger.error(e);
            res.put("success", false);
            res.put("message", isLangEn ? "Failed to delete" : "ডিলিট করতে ব্যর্থ হয়েছে");
        }
        return res;
    }

    @Override
    public String getTableName() {
        return getCommonDAOService().getTableName();
    }

    @Override
    public String getServletName() {
        return "Overtime_bill_financeServlet";
    }

    @Override
    public CommonDAOService<? extends CommonDTO> getCommonDAOService() {
        return Overtime_bill_financeDAO.getInstance();
    }

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        return null;
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[0];
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[0];
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[]{MenuConstants.OVERTIME_BILL_FINANCE_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Overtime_bill_financeServlet.class;
    }
}
