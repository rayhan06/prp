package office_front_desk;

import java.util.*;


public class Office_front_deskDTO {

    public long id = 0;
    public int officeId = 0;
    public String officeName = "";
    public String officeAddress = "";
    public long officeUnitId = 0;
    public String officeUnitName = "";
    public long officeUnitOrganogramId = 0;
    public String designationLabel = "";
    public long officerId = 0;
    public String officerName = "";
    public long createdBy = 0;
    public long created = 0;
    public long modified = 0;
    public long modifiedBy = 0;
    public boolean isDeleted = false;

    public boolean isFrontDesk;


    @Override
    public String toString() {
        return "$Office_front_deskDTO[" +
                " id = " + id +
                " officeId = " + officeId +
                " officeName = " + officeName +
                " officeAddress = " + officeAddress +
                " officeUnitId = " + officeUnitId +
                " officeUnitName = " + officeUnitName +
                " officeUnitOrganogramId = " + officeUnitOrganogramId +
                " designationLabel = " + designationLabel +
                " officerId = " + officerId +
                " officerName = " + officerName +
                " createdBy = " + createdBy +
                " created = " + created +
                " modified = " + modified +
                " modifiedBy = " + modifiedBy +
                " isDeleted = " + isDeleted +
                "]";
    }

}