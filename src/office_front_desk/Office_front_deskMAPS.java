package office_front_desk;
import java.util.*; 


public class Office_front_deskMAPS 
{

	public HashMap<String, String> java_allfield_type_map = new HashMap<String, String>();
	public HashMap<String, String> java_anyfield_search_map = new HashMap<String, String>();
	public HashMap<String, String> java_DTO_map = new HashMap<String, String>();
	public HashMap<String, String> java_SQL_map = new HashMap<String, String>();
	public HashMap<String, String> java_Text_map = new HashMap<String, String>();
	
	private static Office_front_deskMAPS self = null;
	
	private Office_front_deskMAPS()
	{
		
		java_allfield_type_map.put("office_id".toLowerCase(), "Integer");
		java_allfield_type_map.put("office_name".toLowerCase(), "String");
		java_allfield_type_map.put("office_address".toLowerCase(), "String");
		java_allfield_type_map.put("office_unit_id".toLowerCase(), "Long");
		java_allfield_type_map.put("office_unit_name".toLowerCase(), "String");
		java_allfield_type_map.put("designation_label".toLowerCase(), "String");
		java_allfield_type_map.put("officer_id".toLowerCase(), "Long");
		java_allfield_type_map.put("officer_name".toLowerCase(), "String");
		java_allfield_type_map.put("modified_by".toLowerCase(), "Long");

		java_anyfield_search_map.put("office_front_desk.office_id".toLowerCase(), "Integer");
		java_anyfield_search_map.put("office_front_desk.office_name".toLowerCase(), "String");
		java_anyfield_search_map.put("office_front_desk.office_address".toLowerCase(), "String");
		java_anyfield_search_map.put("office_front_desk.office_unit_id".toLowerCase(), "Long");
		java_anyfield_search_map.put("office_front_desk.office_unit_name".toLowerCase(), "String");
		java_anyfield_search_map.put("office_front_desk.office_unit_organogram_id".toLowerCase(), "Long");
		java_anyfield_search_map.put("office_front_desk.designation_label".toLowerCase(), "String");
		java_anyfield_search_map.put("office_front_desk.officer_id".toLowerCase(), "Long");
		java_anyfield_search_map.put("office_front_desk.officer_name".toLowerCase(), "String");
		java_anyfield_search_map.put("office_front_desk.created_by".toLowerCase(), "Long");
		java_anyfield_search_map.put("office_front_desk.created".toLowerCase(), "Long");
		java_anyfield_search_map.put("office_front_desk.modified".toLowerCase(), "Long");
		java_anyfield_search_map.put("office_front_desk.modified_by".toLowerCase(), "Long");

		java_DTO_map.put("id".toLowerCase(), "id".toLowerCase());
		java_DTO_map.put("officeId".toLowerCase(), "officeId".toLowerCase());
		java_DTO_map.put("officeName".toLowerCase(), "officeName".toLowerCase());
		java_DTO_map.put("officeAddress".toLowerCase(), "officeAddress".toLowerCase());
		java_DTO_map.put("officeUnitId".toLowerCase(), "officeUnitId".toLowerCase());
		java_DTO_map.put("officeUnitName".toLowerCase(), "officeUnitName".toLowerCase());
		java_DTO_map.put("officeUnitOrganogramId".toLowerCase(), "officeUnitOrganogramId".toLowerCase());
		java_DTO_map.put("designationLabel".toLowerCase(), "designationLabel".toLowerCase());
		java_DTO_map.put("officerId".toLowerCase(), "officerId".toLowerCase());
		java_DTO_map.put("officerName".toLowerCase(), "officerName".toLowerCase());
		java_DTO_map.put("createdBy".toLowerCase(), "createdBy".toLowerCase());
		java_DTO_map.put("created".toLowerCase(), "created".toLowerCase());
		java_DTO_map.put("modified".toLowerCase(), "modified".toLowerCase());
		java_DTO_map.put("modifiedBy".toLowerCase(), "modifiedBy".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());

		java_SQL_map.put("office_unit_name".toLowerCase(), "officeUnitName".toLowerCase());
		java_SQL_map.put("designation_label".toLowerCase(), "designationLabel".toLowerCase());
		java_SQL_map.put("officer_name".toLowerCase(), "officerName".toLowerCase());

		java_Text_map.put("Id".toLowerCase(), "id".toLowerCase());
		java_Text_map.put("Office Id".toLowerCase(), "officeId".toLowerCase());
		java_Text_map.put("Office Name".toLowerCase(), "officeName".toLowerCase());
		java_Text_map.put("Office Address".toLowerCase(), "officeAddress".toLowerCase());
		java_Text_map.put("Office Unit Id".toLowerCase(), "officeUnitId".toLowerCase());
		java_Text_map.put("Office Unit Name".toLowerCase(), "officeUnitName".toLowerCase());
		java_Text_map.put("Office Unit Organogram Id".toLowerCase(), "officeUnitOrganogramId".toLowerCase());
		java_Text_map.put("Designation Label".toLowerCase(), "designationLabel".toLowerCase());
		java_Text_map.put("Officer Id".toLowerCase(), "officerId".toLowerCase());
		java_Text_map.put("Officer Name".toLowerCase(), "officerName".toLowerCase());
		java_Text_map.put("Created By".toLowerCase(), "createdBy".toLowerCase());
		java_Text_map.put("Created".toLowerCase(), "created".toLowerCase());
		java_Text_map.put("Modified".toLowerCase(), "modified".toLowerCase());
		java_Text_map.put("Modified By".toLowerCase(), "modifiedBy".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
			
	}
	
	public static Office_front_deskMAPS GetInstance()
	{
		if(self == null)
		{
			self = new Office_front_deskMAPS ();
		}
		return self;
	}
	

}