package office_front_desk;

import dbm.DBMR;
import dbm.DBMW;
import login.LoginDTO;
import org.apache.log4j.Logger;
import repository.RepositoryManager;
import util.NavigationService;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;

public class Office_front_deskDAO implements NavigationService {

    public int officeId = 0;
    public int officeUnitId = 0;

    Logger logger = Logger.getLogger(getClass());

    private void printSql(String sql) {
        System.out.println("sql: " + sql);
    }

    private void recordUpdateTime(Connection connection, long lastModificationTime) {
        recordUpdateTime(connection,"office_front_desk",lastModificationTime);
    }

    public long addOffice_front_desk(Office_front_deskDTO office_front_deskDTO) throws Exception {

        if (!isOfficeFrontAlreadyAdded(office_front_deskDTO.officeId, office_front_deskDTO)) {

            Connection connection = null;
            PreparedStatement ps = null;

            long lastModificationTime = System.currentTimeMillis();

            try {
                connection = DBMW.getInstance().getConnection();

                if (connection == null) {
                    System.out.println("nullconn");
                }

                office_front_deskDTO.id = DBMW.getInstance().getNextSequenceId("Office_front_desk");

                String sql = "INSERT INTO office_front_desk";

                sql += " (";
                sql += "id";
                sql += ", ";
                sql += "office_id";
                sql += ", ";
                sql += "office_name";
                sql += ", ";
                sql += "office_address";
                sql += ", ";
                sql += "office_unit_id";
                sql += ", ";
                sql += "office_unit_name";
                sql += ", ";
                sql += "office_unit_organogram_id";
                sql += ", ";
                sql += "designation_label";
                sql += ", ";
                sql += "officer_id";
                sql += ", ";
                sql += "officer_name";
                sql += ", ";
                sql += "created_by";
                sql += ", ";
                sql += "created";
                sql += ", ";
                sql += "modified";
                sql += ", ";
                sql += "modified_by";
                sql += ", ";
                sql += "isDeleted";
                sql += ", lastModificationTime)";


                sql += " VALUES(";
                sql += "?";
                sql += ", ";
                sql += "?";
                sql += ", ";
                sql += "?";
                sql += ", ";
                sql += "?";
                sql += ", ";
                sql += "?";
                sql += ", ";
                sql += "?";
                sql += ", ";
                sql += "?";
                sql += ", ";
                sql += "?";
                sql += ", ";
                sql += "?";
                sql += ", ";
                sql += "?";
                sql += ", ";
                sql += "?";
                sql += ", ";
                sql += "?";
                sql += ", ";
                sql += "?";
                sql += ", ";
                sql += "?";
                sql += ", ";
                sql += "?";
                sql += ", ?)";

                printSql(sql);

                ps = connection.prepareStatement(sql);


                int index = 1;

                ps.setObject(index++, office_front_deskDTO.id);
                ps.setObject(index++, office_front_deskDTO.officeId);
                ps.setObject(index++, office_front_deskDTO.officeName);
                ps.setObject(index++, office_front_deskDTO.officeAddress);
                ps.setObject(index++, office_front_deskDTO.officeUnitId);
                ps.setObject(index++, office_front_deskDTO.officeUnitName);
                ps.setObject(index++, office_front_deskDTO.officeUnitOrganogramId);
                ps.setObject(index++, office_front_deskDTO.designationLabel);
                ps.setObject(index++, office_front_deskDTO.officerId);
                ps.setObject(index++, office_front_deskDTO.officerName);
                ps.setObject(index++, office_front_deskDTO.createdBy);
                ps.setObject(index++, office_front_deskDTO.created);
                ps.setObject(index++, office_front_deskDTO.modified);
                ps.setObject(index++, office_front_deskDTO.modifiedBy);
                ps.setObject(index++, office_front_deskDTO.isDeleted);
                ps.setObject(index++, lastModificationTime);

                System.out.println(ps);
                ps.execute();


                recordUpdateTime(connection, lastModificationTime);

            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                try {
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                }
                try {
                    if (connection != null) {
                        DBMW.getInstance().freeConnection(connection);
                    }
                } catch (Exception ex2) {
                }
            }
            return office_front_deskDTO.id;
        }
        return 0;
    }

    //need another getter for repository
    public Office_front_deskDTO getOffice_front_deskDTOByid(long id) throws Exception {
        Connection connection = null;
        ResultSet rs = null;
        Statement stmt = null;
        Office_front_deskDTO office_front_deskDTO = null;
        try {

            String sql = "SELECT * ";

            sql += " FROM office_front_desk";

            sql += " WHERE id=" + id;

            printSql(sql);

            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();


            rs = stmt.executeQuery(sql);

            if (rs.next()) {
                office_front_deskDTO = new Office_front_deskDTO();

                office_front_deskDTO.id = rs.getLong("id");
                office_front_deskDTO.officeId = rs.getInt("office_id");
                office_front_deskDTO.officeName = rs.getString("office_name");
                office_front_deskDTO.officeAddress = rs.getString("office_address");
                office_front_deskDTO.officeUnitId = rs.getLong("office_unit_id");
                office_front_deskDTO.officeUnitName = rs.getString("office_unit_name");
                office_front_deskDTO.officeUnitOrganogramId = rs.getLong("office_unit_organogram_id");
                office_front_deskDTO.designationLabel = rs.getString("designation_label");
                office_front_deskDTO.officerId = rs.getLong("officer_id");
                office_front_deskDTO.officerName = rs.getString("officer_name");
                office_front_deskDTO.createdBy = rs.getLong("created_by");
                office_front_deskDTO.created = rs.getLong("created");
                office_front_deskDTO.modified = rs.getLong("modified");
                office_front_deskDTO.modifiedBy = rs.getLong("modified_by");
                office_front_deskDTO.isDeleted = rs.getBoolean("isDeleted");

            }


        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }

            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
        return office_front_deskDTO;
    }

    public long updateOffice_front_desk(Office_front_deskDTO office_front_deskDTO) throws Exception {

        Connection connection = null;
        PreparedStatement ps = null;

        long lastModificationTime = System.currentTimeMillis();
        try {
            connection = DBMW.getInstance().getConnection();

            String sql = "UPDATE office_front_desk";

            sql += " SET ";
            sql += "office_id=?";
            sql += ", ";
            sql += "office_name=?";
            sql += ", ";
            sql += "office_address=?";
            sql += ", ";
            sql += "office_unit_id=?";
            sql += ", ";
            sql += "office_unit_name=?";
            sql += ", ";
            sql += "office_unit_organogram_id=?";
            sql += ", ";
            sql += "designation_label=?";
            sql += ", ";
            sql += "officer_id=?";
            sql += ", ";
            sql += "officer_name=?";
            sql += ", ";
            sql += "created_by=?";
            sql += ", ";
            sql += "created=?";
            sql += ", ";
            sql += "modified=?";
            sql += ", ";
            sql += "modified_by=?";
            sql += ", ";
            sql += "isDeleted=?";
            sql += ", lastModificationTime = " + lastModificationTime + "";
            sql += " WHERE id = " + office_front_deskDTO.id;

            printSql(sql);

            ps = connection.prepareStatement(sql);


            int index = 1;

            ps.setObject(index++, office_front_deskDTO.officeId);
            ps.setObject(index++, office_front_deskDTO.officeName);
            ps.setObject(index++, office_front_deskDTO.officeAddress);
            ps.setObject(index++, office_front_deskDTO.officeUnitId);
            ps.setObject(index++, office_front_deskDTO.officeUnitName);
            ps.setObject(index++, office_front_deskDTO.officeUnitOrganogramId);
            ps.setObject(index++, office_front_deskDTO.designationLabel);
            ps.setObject(index++, office_front_deskDTO.officerId);
            ps.setObject(index++, office_front_deskDTO.officerName);
            ps.setObject(index++, office_front_deskDTO.createdBy);
            ps.setObject(index++, office_front_deskDTO.created);
            ps.setObject(index++, office_front_deskDTO.modified);
            ps.setObject(index++, office_front_deskDTO.modifiedBy);
            ps.setObject(index++, office_front_deskDTO.isDeleted);
            System.out.println(ps);
            ps.executeUpdate();


            recordUpdateTime(connection, lastModificationTime);


        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (connection != null) {
                    DBMW.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
        return office_front_deskDTO.id;
    }

    public void deleteOffice_front_deskByid(long id) throws Exception {
        Connection connection = null;
        Statement stmt = null;
        PreparedStatement ps = null;

        long lastModificationTime = System.currentTimeMillis();
        try {
            String sql = "UPDATE office_front_desk";

            sql += " SET isDeleted=1,lastModificationTime=" + lastModificationTime + " WHERE id = " + id;

            printSql(sql);

            connection = DBMW.getInstance().getConnection();
            stmt = connection.createStatement();
            stmt.execute(sql);


            recordUpdateTime(connection, lastModificationTime);


        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }

            try {
                if (connection != null) {
                    DBMW.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
    }

    public List<Office_front_deskDTO> getDTOs(Collection recordIDs, LoginDTO loginDTO) {
        Connection connection = null;
        ResultSet rs = null;
        Statement stmt = null;
        Office_front_deskDTO office_front_deskDTO = null;
        List<Office_front_deskDTO> office_front_deskDTOList = new ArrayList<>();
        if (recordIDs.isEmpty()) {
            return office_front_deskDTOList;
        }
        try {

            String sql = "select " +
                    "er.id as officer_id," +
                    "er.name_bng as officer_name," +
                    "er.name_eng as officer_name_eng," +
                    "o.id as office_id," +
                    "o.office_name_bng as office_name," +
                    "o.office_address as office_address," +
                    "ou.id as office_unit_id," +
                    "ou.unit_name_bng as office_unit_name," +
                    "ouo.id as office_unit_organogram_id," +
                    "ouo.designation_bng as designation_label " +
                    "from employee_records er," +
                    "employee_offices eo," +
                    "offices o," +
                    "office_units ou," +
                    "office_unit_organograms ouo " +
                    "where er.id = eo.employee_record_id " +
                    "and eo.office_id = o.id " +
                    "and eo.office_unit_id = ou.id " +
                    "and eo.office_unit_organogram_id = ouo.id " +
                    "and eo.office_id = " + officeId + " ";

            sql += "and er.id in( ";

            for (int i = 0; i < recordIDs.size(); i++) {
                if (i != 0) {
                    sql += ",";
                }
                sql += ((ArrayList) recordIDs).get(i);
            }
            sql += ")  order by er.id desc";

            printSql(sql);

            logger.debug("sql " + sql);
            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();


            rs = stmt.executeQuery(sql);

            while (rs.next()) {
                office_front_deskDTO = new Office_front_deskDTO();
                //office_front_deskDTO.id = rs.getLong("id");
                office_front_deskDTO.officeId = rs.getInt("office_id");
                office_front_deskDTO.officeName = rs.getString("office_name");
                office_front_deskDTO.officeAddress = rs.getString("office_address");
                office_front_deskDTO.officeUnitId = rs.getLong("office_unit_id");
                office_front_deskDTO.officeUnitName = rs.getString("office_unit_name");
                office_front_deskDTO.officeUnitOrganogramId = rs.getLong("office_unit_organogram_id");
                office_front_deskDTO.designationLabel = rs.getString("designation_label");
                office_front_deskDTO.officerId = rs.getLong("officer_id");
                office_front_deskDTO.officerName = rs.getString("officer_name");

                System.out.println("got this DTO: " + office_front_deskDTO);

                office_front_deskDTOList.add(office_front_deskDTO);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }

            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
        return isOfficeFrontDesk(office_front_deskDTOList);
    }

    public Collection getIDs(LoginDTO loginDTO) {
        Collection data = new ArrayList();
        Connection connection = null;
        Statement stmt = null;
        ResultSet resultSet = null;
        String sql = "select " +
                "er.id as id " +
                "from employee_records er," +
                "employee_offices eo," +
                "offices o," +
                "office_units ou," +
                "office_unit_organograms ouo " +
                "where er.id = eo.employee_record_id " +
                "and eo.office_id = o.id " +
                "and eo.office_unit_id = ou.id " +
                "and eo.office_unit_organogram_id = ouo.id " +
                "and eo.office_id = " + officeId + " " +
                "order by er.id desc;";

        printSql(sql);

        try {
            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();
            for (resultSet = stmt.executeQuery(sql); resultSet.next(); data.add(resultSet.getString("id"))) ;
            resultSet.close();
        } catch (Exception e) {
            logger.fatal("DAO " + e, e);
        } finally {
            try {
                if (resultSet != null && !resultSet.isClosed()) {
                    resultSet.close();
                }
            } catch (Exception ex) {
            }
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception e) {
                logger.fatal("DAO finally :" + e);
            }
        }
        return data;
    }

    //add repository
    public List<Office_front_deskDTO> getAllOffice_front_desk(boolean isFirstReload) {
        List<Office_front_deskDTO> office_front_deskDTOList = new ArrayList<>();

        String sql = "SELECT * FROM office_front_desk";
        sql += " WHERE ";


        if (isFirstReload) {
            sql += " isDeleted =  0";
        }
        if (!isFirstReload) {
            sql += " lastModificationTime >= " + RepositoryManager.lastModifyTime;
        }
        printSql(sql);

        Connection connection = null;
        Statement stmt = null;
        ResultSet rs = null;


        try {
            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();
            rs = stmt.executeQuery(sql);


            while (rs.next()) {
                Office_front_deskDTO office_front_deskDTO = new Office_front_deskDTO();
                office_front_deskDTO.id = rs.getLong("id");
                office_front_deskDTO.officeId = rs.getInt("office_id");
                office_front_deskDTO.officeName = rs.getString("office_name");
                office_front_deskDTO.officeAddress = rs.getString("office_address");
                office_front_deskDTO.officeUnitId = rs.getLong("office_unit_id");
                office_front_deskDTO.officeUnitName = rs.getString("office_unit_name");
                office_front_deskDTO.officeUnitOrganogramId = rs.getLong("office_unit_organogram_id");
                office_front_deskDTO.designationLabel = rs.getString("designation_label");
                office_front_deskDTO.officerId = rs.getLong("officer_id");
                office_front_deskDTO.officerName = rs.getString("officer_name");
                office_front_deskDTO.createdBy = rs.getLong("created_by");
                office_front_deskDTO.created = rs.getLong("created");
                office_front_deskDTO.modified = rs.getLong("modified");
                office_front_deskDTO.modifiedBy = rs.getLong("modified_by");
                office_front_deskDTO.isDeleted = rs.getBoolean("isDeleted");
                int i = 0;
                long primaryKey = office_front_deskDTO.id;
                while (i < office_front_deskDTOList.size() && office_front_deskDTOList.get(i).id < primaryKey) {
                    i++;
                }
                office_front_deskDTOList.add(i, office_front_deskDTO);
                //office_front_deskDTOList.add(office_front_deskDTO);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }

        return office_front_deskDTOList;
    }

    public Collection getIDsWithSearchCriteria(Hashtable p_searchCriteria, LoginDTO loginDTO) throws Exception {
        System.out.println("table: " + p_searchCriteria);
        List<Long> idList = new ArrayList<>();
        Connection connection = null;
        PreparedStatement ps = null;

        try {

            String sql = "select " +
                    "er.id as id " +
                    "from employee_records er," +
                    "employee_offices eo," +
                    "offices o," +
                    "office_units ou," +
                    "office_unit_organograms ouo " +
                    "where er.id = eo.employee_record_id " +
                    "and eo.office_id = o.id " +
                    "and eo.office_unit_id = ou.id " +
                    "and eo.office_unit_organogram_id = ouo.id " +
                    "and eo.office_id = " + officeId + " ";

            if (p_searchCriteria.get("AnyField") != null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase("")) {
                String key = p_searchCriteria.get("AnyField").toString();
                sql += " and ( er.name_bng like '%" + key + "%' " +
                        " or o.office_name_bng like '%" + key + "%'" +
                        " or ou.unit_name_bng like '%" + key + "%'" +
                        " or ouo.designation_bng like '%" + key + "%' ) ";
            }

            Enumeration names = p_searchCriteria.keys();
            String str, value;

            while (names.hasMoreElements()) {
                str = (String) names.nextElement();
                value = (String) p_searchCriteria.get(str);
                System.out.println(str + ": " + value);
                if (Office_front_deskMAPS.GetInstance().java_allfield_type_map.get(str.toLowerCase()) != null && !Office_front_deskMAPS.GetInstance().java_allfield_type_map.get(str.toLowerCase()).equalsIgnoreCase("")
                        && !str.equalsIgnoreCase("AnyField")
                        && value != null && !value.equalsIgnoreCase("")) {
                    if (p_searchCriteria.get(str).equals("any")) {
                        continue;
                    }
                    String key = p_searchCriteria.get(str).toString();

                    sql += " and ( er.name_bng like '%" + key + "%' " +
                            " or o.office_name_bng like '%" + key + "%'" +
                            " or ou.unit_name_bng like '%" + key + "%'" +
                            " or ouo.designation_bng like '%" + key + "%' ) ";
                }
            }

            sql += " order by er.id desc ";

            printSql(sql);

            connection = DBMR.getInstance().getConnection();
            ps = connection.prepareStatement(sql);

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                idList.add(rs.getLong("ID"));
            }


        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
        return idList;
    }

    private boolean isOfficeFrontAlreadyAdded(int officeId, Office_front_deskDTO office_front_deskDTO) {
        Connection connection = null;
        Statement stmt = null;
        ResultSet resultSet = null;
        String sql = "select id from office_front_desk where office_id = " + officeId;

        printSql(sql);

        ArrayList<Integer> data = new ArrayList<>();
        try {
            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();
            resultSet = stmt.executeQuery(sql);

            while (resultSet.next()) {
                data.add(resultSet.getInt("id"));
            }
            resultSet.close();
        } catch (Exception e) {
            logger.fatal("DAO " + e, e);
        } finally {
            try {
                if (resultSet != null && !resultSet.isClosed()) {
                    resultSet.close();
                }
            } catch (Exception ex) {
            }
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception e) {
                logger.fatal("DAO finally :" + e);
            }
        }
        if (data.size() > 0) {
            this.updateOfficeFront(data.get(0), office_front_deskDTO);
            return true;
        }
        return false;
    }

    private void updateOfficeFront(int id, Office_front_deskDTO office_front_deskDTO) {
        office_front_deskDTO.id = id;
        /*
        try {
            office_front_deskDTO.officeName = new String(office_front_deskDTO.officeName.getBytes("ISO-8859-1"), "UTF-8");
            office_front_deskDTO.officeAddress = new String(office_front_deskDTO.officeAddress.getBytes("ISO-8859-1"), "UTF-8");
            office_front_deskDTO.officeUnitName = new String(office_front_deskDTO.officeUnitName.getBytes("ISO-8859-1"), "UTF-8");
            office_front_deskDTO.designationLabel = new String(office_front_deskDTO.designationLabel.getBytes("ISO-8859-1"), "UTF-8");
            office_front_deskDTO.officerName = new String(office_front_deskDTO.officerName.getBytes("ISO-8859-1"), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        */

        try {
            updateOffice_front_desk(office_front_deskDTO);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void userOfficeId(long employee_record_id) {
        Collection data = new ArrayList();
        Connection connection = null;
        Statement stmt = null;
        ResultSet resultSet = null;
        String sql = "select office_id, office_unit_id from employee_offices where employee_offices.employee_record_id = " + employee_record_id;

        printSql(sql);

        try {
            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();
            resultSet = stmt.executeQuery(sql);
            while (resultSet.next()) {
                this.officeId = resultSet.getInt("office_id");
                this.officeUnitId = resultSet.getInt("office_unit_id");
            }
            resultSet.close();
        } catch (Exception e) {
            logger.fatal("DAO " + e, e);
        } finally {
            try {
                if (resultSet != null && !resultSet.isClosed()) {
                    resultSet.close();
                }
            } catch (Exception ex) {
            }
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception e) {
                logger.fatal("DAO finally :" + e);
            }
        }
        return;
    }

    private List<Office_front_deskDTO> isOfficeFrontDesk(List<Office_front_deskDTO> office_front_deskDTOList) {
        for (Office_front_deskDTO a : office_front_deskDTOList) {

            Connection connection = null;
            Statement stmt = null;
            ResultSet resultSet = null;
            String sql = "select id from office_front_desk where office_id = " + a.officeId + " and officer_id = " + a.officerId;

            printSql(sql);
            int tableId = -1;
            try {
                connection = DBMR.getInstance().getConnection();
                stmt = connection.createStatement();
                resultSet = stmt.executeQuery(sql);
                while (resultSet.next()) {
                    tableId = resultSet.getInt("id");
                }
                resultSet.close();
            } catch (Exception e) {
                logger.fatal("DAO " + e, e);
            } finally {
                try {
                    if (resultSet != null && !resultSet.isClosed()) {
                        resultSet.close();
                    }
                } catch (Exception ex) {
                }
                try {
                    if (stmt != null) {
                        stmt.close();
                    }
                } catch (Exception e) {
                }
                try {
                    if (connection != null) {
                        DBMR.getInstance().freeConnection(connection);
                    }
                } catch (Exception e) {
                    logger.fatal("DAO finally :" + e);
                }
            }
            if (tableId != -1) a.isFrontDesk = true;
        }
        return office_front_deskDTOList;
    }
}
	