package office_front_desk;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class Office_front_deskRepository implements Repository {
	Office_front_deskDAO office_front_deskDAO = new Office_front_deskDAO();
	
	
	static Logger logger = Logger.getLogger(Office_front_deskRepository.class);
	Map<Long, Office_front_deskDTO>mapOfOffice_front_deskDTOToid;
	Map<Integer, Set<Office_front_deskDTO> >mapOfOffice_front_deskDTOToofficeId;
	Map<String, Set<Office_front_deskDTO> >mapOfOffice_front_deskDTOToofficeName;
	Map<String, Set<Office_front_deskDTO> >mapOfOffice_front_deskDTOToofficeAddress;
	Map<Long, Set<Office_front_deskDTO> >mapOfOffice_front_deskDTOToofficeUnitId;
	Map<String, Set<Office_front_deskDTO> >mapOfOffice_front_deskDTOToofficeUnitName;
	Map<Long, Set<Office_front_deskDTO> >mapOfOffice_front_deskDTOToofficeUnitOrganogramId;
	Map<String, Set<Office_front_deskDTO> >mapOfOffice_front_deskDTOTodesignationLabel;
	Map<Long, Set<Office_front_deskDTO> >mapOfOffice_front_deskDTOToofficerId;
	Map<String, Set<Office_front_deskDTO> >mapOfOffice_front_deskDTOToofficerName;
	Map<Long, Set<Office_front_deskDTO> >mapOfOffice_front_deskDTOTocreatedBy;
	Map<Long, Set<Office_front_deskDTO> >mapOfOffice_front_deskDTOTocreated;
	Map<Long, Set<Office_front_deskDTO> >mapOfOffice_front_deskDTOTomodified;
	Map<Long, Set<Office_front_deskDTO> >mapOfOffice_front_deskDTOTomodifiedBy;


	static Office_front_deskRepository instance = null;  
	private Office_front_deskRepository(){
		mapOfOffice_front_deskDTOToid = new ConcurrentHashMap<>();
		mapOfOffice_front_deskDTOToofficeId = new ConcurrentHashMap<>();
		mapOfOffice_front_deskDTOToofficeName = new ConcurrentHashMap<>();
		mapOfOffice_front_deskDTOToofficeAddress = new ConcurrentHashMap<>();
		mapOfOffice_front_deskDTOToofficeUnitId = new ConcurrentHashMap<>();
		mapOfOffice_front_deskDTOToofficeUnitName = new ConcurrentHashMap<>();
		mapOfOffice_front_deskDTOToofficeUnitOrganogramId = new ConcurrentHashMap<>();
		mapOfOffice_front_deskDTOTodesignationLabel = new ConcurrentHashMap<>();
		mapOfOffice_front_deskDTOToofficerId = new ConcurrentHashMap<>();
		mapOfOffice_front_deskDTOToofficerName = new ConcurrentHashMap<>();
		mapOfOffice_front_deskDTOTocreatedBy = new ConcurrentHashMap<>();
		mapOfOffice_front_deskDTOTocreated = new ConcurrentHashMap<>();
		mapOfOffice_front_deskDTOTomodified = new ConcurrentHashMap<>();
		mapOfOffice_front_deskDTOTomodifiedBy = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Office_front_deskRepository getInstance(){
		if (instance == null){
			instance = new Office_front_deskRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		try {
			List<Office_front_deskDTO> office_front_deskDTOs = office_front_deskDAO.getAllOffice_front_desk(reloadAll);
			for(Office_front_deskDTO office_front_deskDTO : office_front_deskDTOs) {
				Office_front_deskDTO oldOffice_front_deskDTO = getOffice_front_deskDTOByid(office_front_deskDTO.id);
				if( oldOffice_front_deskDTO != null ) {
					mapOfOffice_front_deskDTOToid.remove(oldOffice_front_deskDTO.id);
				
					if(mapOfOffice_front_deskDTOToofficeId.containsKey(oldOffice_front_deskDTO.officeId)) {
						mapOfOffice_front_deskDTOToofficeId.get(oldOffice_front_deskDTO.officeId).remove(oldOffice_front_deskDTO);
					}
					if(mapOfOffice_front_deskDTOToofficeId.get(oldOffice_front_deskDTO.officeId).isEmpty()) {
						mapOfOffice_front_deskDTOToofficeId.remove(oldOffice_front_deskDTO.officeId);
					}
					
					if(mapOfOffice_front_deskDTOToofficeName.containsKey(oldOffice_front_deskDTO.officeName)) {
						mapOfOffice_front_deskDTOToofficeName.get(oldOffice_front_deskDTO.officeName).remove(oldOffice_front_deskDTO);
					}
					if(mapOfOffice_front_deskDTOToofficeName.get(oldOffice_front_deskDTO.officeName).isEmpty()) {
						mapOfOffice_front_deskDTOToofficeName.remove(oldOffice_front_deskDTO.officeName);
					}
					
					if(mapOfOffice_front_deskDTOToofficeAddress.containsKey(oldOffice_front_deskDTO.officeAddress)) {
						mapOfOffice_front_deskDTOToofficeAddress.get(oldOffice_front_deskDTO.officeAddress).remove(oldOffice_front_deskDTO);
					}
					if(mapOfOffice_front_deskDTOToofficeAddress.get(oldOffice_front_deskDTO.officeAddress).isEmpty()) {
						mapOfOffice_front_deskDTOToofficeAddress.remove(oldOffice_front_deskDTO.officeAddress);
					}
					
					if(mapOfOffice_front_deskDTOToofficeUnitId.containsKey(oldOffice_front_deskDTO.officeUnitId)) {
						mapOfOffice_front_deskDTOToofficeUnitId.get(oldOffice_front_deskDTO.officeUnitId).remove(oldOffice_front_deskDTO);
					}
					if(mapOfOffice_front_deskDTOToofficeUnitId.get(oldOffice_front_deskDTO.officeUnitId).isEmpty()) {
						mapOfOffice_front_deskDTOToofficeUnitId.remove(oldOffice_front_deskDTO.officeUnitId);
					}
					
					if(mapOfOffice_front_deskDTOToofficeUnitName.containsKey(oldOffice_front_deskDTO.officeUnitName)) {
						mapOfOffice_front_deskDTOToofficeUnitName.get(oldOffice_front_deskDTO.officeUnitName).remove(oldOffice_front_deskDTO);
					}
					if(mapOfOffice_front_deskDTOToofficeUnitName.get(oldOffice_front_deskDTO.officeUnitName).isEmpty()) {
						mapOfOffice_front_deskDTOToofficeUnitName.remove(oldOffice_front_deskDTO.officeUnitName);
					}
					
					if(mapOfOffice_front_deskDTOToofficeUnitOrganogramId.containsKey(oldOffice_front_deskDTO.officeUnitOrganogramId)) {
						mapOfOffice_front_deskDTOToofficeUnitOrganogramId.get(oldOffice_front_deskDTO.officeUnitOrganogramId).remove(oldOffice_front_deskDTO);
					}
					if(mapOfOffice_front_deskDTOToofficeUnitOrganogramId.get(oldOffice_front_deskDTO.officeUnitOrganogramId).isEmpty()) {
						mapOfOffice_front_deskDTOToofficeUnitOrganogramId.remove(oldOffice_front_deskDTO.officeUnitOrganogramId);
					}
					
					if(mapOfOffice_front_deskDTOTodesignationLabel.containsKey(oldOffice_front_deskDTO.designationLabel)) {
						mapOfOffice_front_deskDTOTodesignationLabel.get(oldOffice_front_deskDTO.designationLabel).remove(oldOffice_front_deskDTO);
					}
					if(mapOfOffice_front_deskDTOTodesignationLabel.get(oldOffice_front_deskDTO.designationLabel).isEmpty()) {
						mapOfOffice_front_deskDTOTodesignationLabel.remove(oldOffice_front_deskDTO.designationLabel);
					}
					
					if(mapOfOffice_front_deskDTOToofficerId.containsKey(oldOffice_front_deskDTO.officerId)) {
						mapOfOffice_front_deskDTOToofficerId.get(oldOffice_front_deskDTO.officerId).remove(oldOffice_front_deskDTO);
					}
					if(mapOfOffice_front_deskDTOToofficerId.get(oldOffice_front_deskDTO.officerId).isEmpty()) {
						mapOfOffice_front_deskDTOToofficerId.remove(oldOffice_front_deskDTO.officerId);
					}
					
					if(mapOfOffice_front_deskDTOToofficerName.containsKey(oldOffice_front_deskDTO.officerName)) {
						mapOfOffice_front_deskDTOToofficerName.get(oldOffice_front_deskDTO.officerName).remove(oldOffice_front_deskDTO);
					}
					if(mapOfOffice_front_deskDTOToofficerName.get(oldOffice_front_deskDTO.officerName).isEmpty()) {
						mapOfOffice_front_deskDTOToofficerName.remove(oldOffice_front_deskDTO.officerName);
					}
					
					if(mapOfOffice_front_deskDTOTocreatedBy.containsKey(oldOffice_front_deskDTO.createdBy)) {
						mapOfOffice_front_deskDTOTocreatedBy.get(oldOffice_front_deskDTO.createdBy).remove(oldOffice_front_deskDTO);
					}
					if(mapOfOffice_front_deskDTOTocreatedBy.get(oldOffice_front_deskDTO.createdBy).isEmpty()) {
						mapOfOffice_front_deskDTOTocreatedBy.remove(oldOffice_front_deskDTO.createdBy);
					}
					
					if(mapOfOffice_front_deskDTOTocreated.containsKey(oldOffice_front_deskDTO.created)) {
						mapOfOffice_front_deskDTOTocreated.get(oldOffice_front_deskDTO.created).remove(oldOffice_front_deskDTO);
					}
					if(mapOfOffice_front_deskDTOTocreated.get(oldOffice_front_deskDTO.created).isEmpty()) {
						mapOfOffice_front_deskDTOTocreated.remove(oldOffice_front_deskDTO.created);
					}
					
					if(mapOfOffice_front_deskDTOTomodified.containsKey(oldOffice_front_deskDTO.modified)) {
						mapOfOffice_front_deskDTOTomodified.get(oldOffice_front_deskDTO.modified).remove(oldOffice_front_deskDTO);
					}
					if(mapOfOffice_front_deskDTOTomodified.get(oldOffice_front_deskDTO.modified).isEmpty()) {
						mapOfOffice_front_deskDTOTomodified.remove(oldOffice_front_deskDTO.modified);
					}
					
					if(mapOfOffice_front_deskDTOTomodifiedBy.containsKey(oldOffice_front_deskDTO.modifiedBy)) {
						mapOfOffice_front_deskDTOTomodifiedBy.get(oldOffice_front_deskDTO.modifiedBy).remove(oldOffice_front_deskDTO);
					}
					if(mapOfOffice_front_deskDTOTomodifiedBy.get(oldOffice_front_deskDTO.modifiedBy).isEmpty()) {
						mapOfOffice_front_deskDTOTomodifiedBy.remove(oldOffice_front_deskDTO.modifiedBy);
					}
					
					
				}
				if(office_front_deskDTO.isDeleted == false) 
				{
					
					mapOfOffice_front_deskDTOToid.put(office_front_deskDTO.id, office_front_deskDTO);
				
					if( ! mapOfOffice_front_deskDTOToofficeId.containsKey(office_front_deskDTO.officeId)) {
						mapOfOffice_front_deskDTOToofficeId.put(office_front_deskDTO.officeId, new HashSet<>());
					}
					mapOfOffice_front_deskDTOToofficeId.get(office_front_deskDTO.officeId).add(office_front_deskDTO);
					
					if( ! mapOfOffice_front_deskDTOToofficeName.containsKey(office_front_deskDTO.officeName)) {
						mapOfOffice_front_deskDTOToofficeName.put(office_front_deskDTO.officeName, new HashSet<>());
					}
					mapOfOffice_front_deskDTOToofficeName.get(office_front_deskDTO.officeName).add(office_front_deskDTO);
					
					if( ! mapOfOffice_front_deskDTOToofficeAddress.containsKey(office_front_deskDTO.officeAddress)) {
						mapOfOffice_front_deskDTOToofficeAddress.put(office_front_deskDTO.officeAddress, new HashSet<>());
					}
					mapOfOffice_front_deskDTOToofficeAddress.get(office_front_deskDTO.officeAddress).add(office_front_deskDTO);
					
					if( ! mapOfOffice_front_deskDTOToofficeUnitId.containsKey(office_front_deskDTO.officeUnitId)) {
						mapOfOffice_front_deskDTOToofficeUnitId.put(office_front_deskDTO.officeUnitId, new HashSet<>());
					}
					mapOfOffice_front_deskDTOToofficeUnitId.get(office_front_deskDTO.officeUnitId).add(office_front_deskDTO);
					
					if( ! mapOfOffice_front_deskDTOToofficeUnitName.containsKey(office_front_deskDTO.officeUnitName)) {
						mapOfOffice_front_deskDTOToofficeUnitName.put(office_front_deskDTO.officeUnitName, new HashSet<>());
					}
					mapOfOffice_front_deskDTOToofficeUnitName.get(office_front_deskDTO.officeUnitName).add(office_front_deskDTO);
					
					if( ! mapOfOffice_front_deskDTOToofficeUnitOrganogramId.containsKey(office_front_deskDTO.officeUnitOrganogramId)) {
						mapOfOffice_front_deskDTOToofficeUnitOrganogramId.put(office_front_deskDTO.officeUnitOrganogramId, new HashSet<>());
					}
					mapOfOffice_front_deskDTOToofficeUnitOrganogramId.get(office_front_deskDTO.officeUnitOrganogramId).add(office_front_deskDTO);
					
					if( ! mapOfOffice_front_deskDTOTodesignationLabel.containsKey(office_front_deskDTO.designationLabel)) {
						mapOfOffice_front_deskDTOTodesignationLabel.put(office_front_deskDTO.designationLabel, new HashSet<>());
					}
					mapOfOffice_front_deskDTOTodesignationLabel.get(office_front_deskDTO.designationLabel).add(office_front_deskDTO);
					
					if( ! mapOfOffice_front_deskDTOToofficerId.containsKey(office_front_deskDTO.officerId)) {
						mapOfOffice_front_deskDTOToofficerId.put(office_front_deskDTO.officerId, new HashSet<>());
					}
					mapOfOffice_front_deskDTOToofficerId.get(office_front_deskDTO.officerId).add(office_front_deskDTO);
					
					if( ! mapOfOffice_front_deskDTOToofficerName.containsKey(office_front_deskDTO.officerName)) {
						mapOfOffice_front_deskDTOToofficerName.put(office_front_deskDTO.officerName, new HashSet<>());
					}
					mapOfOffice_front_deskDTOToofficerName.get(office_front_deskDTO.officerName).add(office_front_deskDTO);
					
					if( ! mapOfOffice_front_deskDTOTocreatedBy.containsKey(office_front_deskDTO.createdBy)) {
						mapOfOffice_front_deskDTOTocreatedBy.put(office_front_deskDTO.createdBy, new HashSet<>());
					}
					mapOfOffice_front_deskDTOTocreatedBy.get(office_front_deskDTO.createdBy).add(office_front_deskDTO);
					
					if( ! mapOfOffice_front_deskDTOTocreated.containsKey(office_front_deskDTO.created)) {
						mapOfOffice_front_deskDTOTocreated.put(office_front_deskDTO.created, new HashSet<>());
					}
					mapOfOffice_front_deskDTOTocreated.get(office_front_deskDTO.created).add(office_front_deskDTO);
					
					if( ! mapOfOffice_front_deskDTOTomodified.containsKey(office_front_deskDTO.modified)) {
						mapOfOffice_front_deskDTOTomodified.put(office_front_deskDTO.modified, new HashSet<>());
					}
					mapOfOffice_front_deskDTOTomodified.get(office_front_deskDTO.modified).add(office_front_deskDTO);
					
					if( ! mapOfOffice_front_deskDTOTomodifiedBy.containsKey(office_front_deskDTO.modifiedBy)) {
						mapOfOffice_front_deskDTOTomodifiedBy.put(office_front_deskDTO.modifiedBy, new HashSet<>());
					}
					mapOfOffice_front_deskDTOTomodifiedBy.get(office_front_deskDTO.modifiedBy).add(office_front_deskDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Office_front_deskDTO> getOffice_front_deskList() {
		List <Office_front_deskDTO> office_front_desks = new ArrayList<Office_front_deskDTO>(this.mapOfOffice_front_deskDTOToid.values());
		return office_front_desks;
	}
	
	
	public Office_front_deskDTO getOffice_front_deskDTOByid( long id){
		return mapOfOffice_front_deskDTOToid.get(id);
	}
	
	
	public List<Office_front_deskDTO> getOffice_front_deskDTOByoffice_id(int office_id) {
		return new ArrayList<>( mapOfOffice_front_deskDTOToofficeId.getOrDefault(office_id,new HashSet<>()));
	}
	
	
	public List<Office_front_deskDTO> getOffice_front_deskDTOByoffice_name(String office_name) {
		return new ArrayList<>( mapOfOffice_front_deskDTOToofficeName.getOrDefault(office_name,new HashSet<>()));
	}
	
	
	public List<Office_front_deskDTO> getOffice_front_deskDTOByoffice_address(String office_address) {
		return new ArrayList<>( mapOfOffice_front_deskDTOToofficeAddress.getOrDefault(office_address,new HashSet<>()));
	}
	
	
	public List<Office_front_deskDTO> getOffice_front_deskDTOByoffice_unit_id(long office_unit_id) {
		return new ArrayList<>( mapOfOffice_front_deskDTOToofficeUnitId.getOrDefault(office_unit_id,new HashSet<>()));
	}
	
	
	public List<Office_front_deskDTO> getOffice_front_deskDTOByoffice_unit_name(String office_unit_name) {
		return new ArrayList<>( mapOfOffice_front_deskDTOToofficeUnitName.getOrDefault(office_unit_name,new HashSet<>()));
	}
	
	
	public List<Office_front_deskDTO> getOffice_front_deskDTOByoffice_unit_organogram_id(long office_unit_organogram_id) {
		return new ArrayList<>( mapOfOffice_front_deskDTOToofficeUnitOrganogramId.getOrDefault(office_unit_organogram_id,new HashSet<>()));
	}
	
	
	public List<Office_front_deskDTO> getOffice_front_deskDTOBydesignation_label(String designation_label) {
		return new ArrayList<>( mapOfOffice_front_deskDTOTodesignationLabel.getOrDefault(designation_label,new HashSet<>()));
	}
	
	
	public List<Office_front_deskDTO> getOffice_front_deskDTOByofficer_id(long officer_id) {
		return new ArrayList<>( mapOfOffice_front_deskDTOToofficerId.getOrDefault(officer_id,new HashSet<>()));
	}
	
	
	public List<Office_front_deskDTO> getOffice_front_deskDTOByofficer_name(String officer_name) {
		return new ArrayList<>( mapOfOffice_front_deskDTOToofficerName.getOrDefault(officer_name,new HashSet<>()));
	}
	
	
	public List<Office_front_deskDTO> getOffice_front_deskDTOBycreated_by(long created_by) {
		return new ArrayList<>( mapOfOffice_front_deskDTOTocreatedBy.getOrDefault(created_by,new HashSet<>()));
	}
	
	
	public List<Office_front_deskDTO> getOffice_front_deskDTOBycreated(long created) {
		return new ArrayList<>( mapOfOffice_front_deskDTOTocreated.getOrDefault(created,new HashSet<>()));
	}
	
	
	public List<Office_front_deskDTO> getOffice_front_deskDTOBymodified(long modified) {
		return new ArrayList<>( mapOfOffice_front_deskDTOTomodified.getOrDefault(modified,new HashSet<>()));
	}
	
	
	public List<Office_front_deskDTO> getOffice_front_deskDTOBymodified_by(long modified_by) {
		return new ArrayList<>( mapOfOffice_front_deskDTOTomodifiedBy.getOrDefault(modified_by,new HashSet<>()));
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "office_front_desk";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


