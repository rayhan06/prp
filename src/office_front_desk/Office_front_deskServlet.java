/*
package office_front_desk;

import java.io.IOException;
import java.io.*;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import org.apache.log4j.Logger;

import login.LoginDTO;
import org.json.JSONObject;
import permission.MenuConstants;
import role.PermissionRepository;


import sessionmanager.SessionConstants;

import user.UserDTO;
import user.UserRepository;

import util.RecordNavigationManager;

import java.util.*;
import javax.servlet.http.*;
import java.util.UUID;

*/
/**
 * Servlet implementation class Office_front_deskServlet
 *//*

@WebServlet("/Office_front_deskServlet")
@MultipartConfig
public class Office_front_deskServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Office_front_deskServlet.class);

    */
/**
     * @see HttpServlet#HttpServlet()
     *//*

    public Office_front_deskServlet() {
        super();
    }

    */
/**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     *//*

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("In doget request = " + request);
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getInstance().getUserDtoByUserId(loginDTO.userID);
        try {
            String actionType = request.getParameter("actionType");
            if (actionType.equals("getAddPage")) {
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.OFFICE_FRONT_DESK_ADD)) {
                    getAddPage(request, response);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else if (actionType.equals("getEditPage")) {
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.OFFICE_FRONT_DESK_UPDATE)) {
                    getOffice_front_desk(request, response);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else if (actionType.equals("getURL")) {
                String URL = request.getParameter("URL");
                System.out.println("URL = " + URL);
                response.sendRedirect(URL);
            } else if (actionType.equals("search")) {
                System.out.println("search requested");
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.OFFICE_FRONT_DESK_SEARCH)) {
                    searchOffice_front_desk(request, response, userDTO);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.debug(ex);
        }
    }

    private void getAddPage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("ID", -1L);
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("office_front_desk/office_front_deskEdit.jsp");
        requestDispatcher.forward(request, response);
    }

    private String getFileName(final Part part) {
        final String partHeader = part.getHeader("content-disposition");
        System.out.println("Part Header = {0}" + partHeader);
        for (String content : part.getHeader("content-disposition").split(";")) {
            if (content.trim().startsWith("filename")) {
                return content.substring(
                        content.indexOf('=') + 1).trim().replace("\"", "");
            }
        }
        return null;
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getInstance().getUserDtoByUserId(loginDTO.userID);
        System.out.println("doPost");

        try {
            String actionType = request.getParameter("actionType");
            System.out.println("actionType = " + actionType);
            if (actionType.equals("add")) {

                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.OFFICE_FRONT_DESK_ADD)) {
                    System.out.println("going to  addOffice_front_desk ");
                    addOffice_front_desk(request, response, true);
                } else {
                    System.out.println("Not going to  addOffice_front_desk ");
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }

            } else if (actionType.equals("edit")) {

                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.OFFICE_FRONT_DESK_UPDATE)) {
                    addOffice_front_desk(request, response, false);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else if (actionType.equals("delete")) {
                deleteOffice_front_desk(request, response);
            } else if (actionType.equals("search")) {
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.OFFICE_FRONT_DESK_SEARCH)) {
                    searchOffice_front_desk(request, response, userDTO);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else if (actionType.equals("getGeo")) {
                System.out.println("going to geoloc ");
                request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            logger.debug(ex);
        }
    }

    private void addOffice_front_desk(HttpServletRequest request, HttpServletResponse response, Boolean addFlag) throws IOException {
        // TODO Auto-generated method stub
        try {
            request.setAttribute("failureMessage", "");
            System.out.println("%%%% addOffice_front_desk");
            String path = getServletContext().getRealPath("/img2/");
            Office_front_deskDAO office_front_deskDAO = new Office_front_deskDAO();
            Office_front_deskDTO office_front_deskDTO;
            String FileNamePrefix;
            if (addFlag == true) {
                office_front_deskDTO = new Office_front_deskDTO();
                FileNamePrefix = UUID.randomUUID().toString().substring(0, 10);
            } else {
                office_front_deskDTO = office_front_deskDAO.getOffice_front_deskDTOByid(Long.parseLong(request.getParameter("identity")));
                FileNamePrefix = request.getParameter("identity");
            }

            String Value = "";
            Value = request.getParameter("officeId");
            System.out.println("officeId = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                office_front_deskDTO.officeId = Integer.parseInt(Value);
            } else {
                System.out.println("FieldName has a null value, not updating" + " = " + Value);
            }
            Value = request.getParameter("officeName");
            System.out.println("officeName = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                office_front_deskDTO.officeName = (Value);
            } else {
                System.out.println("FieldName has a null value, not updating" + " = " + Value);
            }
            Value = request.getParameter("officeAddress");
            System.out.println("officeAddress = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                office_front_deskDTO.officeAddress = (Value);
            } else {
                System.out.println("FieldName has a null value, not updating" + " = " + Value);
            }
            Value = request.getParameter("officeUnitId");
            System.out.println("officeUnitId = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                office_front_deskDTO.officeUnitId = Long.parseLong(Value);
            } else {
                System.out.println("FieldName has a null value, not updating" + " = " + Value);
            }
            Value = request.getParameter("officeUnitName");
            System.out.println("officeUnitName = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                office_front_deskDTO.officeUnitName = (Value);
            } else {
                System.out.println("FieldName has a null value, not updating" + " = " + Value);
            }
            Value = request.getParameter("officeUnitOrganogramId");
            System.out.println("officeUnitOrganogramId = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                office_front_deskDTO.officeUnitOrganogramId = Long.parseLong(Value);
            } else {
                System.out.println("FieldName has a null value, not updating" + " = " + Value);
            }
            Value = request.getParameter("designationLabel");
            System.out.println("designationLabel = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                office_front_deskDTO.designationLabel = (Value);
            } else {
                System.out.println("FieldName has a null value, not updating" + " = " + Value);
            }
            Value = request.getParameter("officerId");
            System.out.println("officerId = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                office_front_deskDTO.officerId = Long.parseLong(Value);
            } else {
                System.out.println("FieldName has a null value, not updating" + " = " + Value);
            }
            Value = request.getParameter("officerName");
            System.out.println("officerName = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                office_front_deskDTO.officerName = (Value);
            } else {
                System.out.println("FieldName has a null value, not updating" + " = " + Value);
            }
            Value = request.getParameter("createdBy");
            System.out.println("createdBy = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                office_front_deskDTO.createdBy = Long.parseLong(Value);
            } else {
                System.out.println("FieldName has a null value, not updating" + " = " + Value);
            }
            Value = request.getParameter("created");
            System.out.println("created = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                office_front_deskDTO.created = Long.parseLong(Value);
            } else {
                System.out.println("FieldName has a null value, not updating" + " = " + Value);
            }
            Value = request.getParameter("modified");
            System.out.println("modified = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                office_front_deskDTO.modified = Long.parseLong(Value);
            } else {
                System.out.println("FieldName has a null value, not updating" + " = " + Value);
            }
            Value = request.getParameter("modifiedBy");
            System.out.println("modifiedBy = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                office_front_deskDTO.modifiedBy = Long.parseLong(Value);
            } else {
                System.out.println("FieldName has a null value, not updating" + " = " + Value);
            }
            Value = request.getParameter("isDeleted");
            System.out.println("isDeleted = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                office_front_deskDTO.isDeleted = Boolean.parseBoolean(Value);
            } else {
                System.out.println("FieldName has a null value, not updating" + " = " + Value);
            }

            System.out.println("Done adding  addOffice_front_desk dto = " + office_front_deskDTO);

            if (addFlag == true) {
                office_front_deskDAO.addOffice_front_desk(office_front_deskDTO);
            } else {
                office_front_deskDAO.updateOffice_front_desk(office_front_deskDTO);
            }

            response.setContentType("application/json");

            Map<String, Boolean> res = new HashMap<>();
            res.put("success", true);

            PrintWriter out = response.getWriter();

            out.print(new JSONObject(res));
            out.flush();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void deleteOffice_front_desk(HttpServletRequest request, HttpServletResponse response) throws IOException {
        try {
            String[] IDsToDelete = request.getParameterValues("ID");
            for (int i = 0; i < IDsToDelete.length; i++) {
                long id = Long.parseLong(IDsToDelete[i]);
                System.out.println("###DELETING " + IDsToDelete[i]);
                new Office_front_deskDAO().deleteOffice_front_deskByid(id);
            }
        } catch (Exception ex) {
            logger.debug(ex);
        }
        response.sendRedirect("Office_front_deskServlet?actionType=search");
    }

    private void getOffice_front_desk(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("in getOffice_front_desk");
        Office_front_deskDTO office_front_deskDTO = null;
        try {
            office_front_deskDTO = new Office_front_deskDAO().getOffice_front_deskDTOByid(Long.parseLong(request.getParameter("ID")));
            request.setAttribute("ID", office_front_deskDTO.id);
            request.setAttribute("office_front_deskDTO", office_front_deskDTO);

            String URL = "";

            String inPlaceEdit = (String) request.getParameter("inplaceedit");
            String inPlaceSubmit = (String) request.getParameter("inplacesubmit");
            String getBodyOnly = (String) request.getParameter("getBodyOnly");

            if (inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase("")) {
                URL = "office_front_desk/office_front_deskInPlaceEdit.jsp";
                request.setAttribute("inplaceedit", "");
            } else if (inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase("")) {
                URL = "office_front_desk/office_front_deskSearchRow.jsp";
                request.setAttribute("inplacesubmit", "");
            } else {
                if (getBodyOnly != null && !getBodyOnly.equalsIgnoreCase("")) {
                    URL = "office_front_desk/office_front_deskEditBody.jsp?actionType=edit";
                } else {
                    URL = "office_front_desk/office_front_deskEdit.jsp?actionType=edit";
                }
            }

            RequestDispatcher rd = request.getRequestDispatcher(URL);
            rd.forward(request, response);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void searchOffice_front_desk(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO) throws ServletException, IOException {
        System.out.println("in  searchOffice_front_desk 1");
        Office_front_deskDAO office_front_deskDAO = new Office_front_deskDAO();
        LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
        String ajax = (String) request.getParameter("ajax");
        boolean hasAjax = false;
        if (ajax != null && !ajax.equalsIgnoreCase("")) {
            hasAjax = true;
        }
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);

        office_front_deskDAO.userOfficeId(userDTO.employee_record_id);

        RecordNavigationManager rnManager = new RecordNavigationManager(SessionConstants.NAV_OFFICE_FRONT_DESK, request, office_front_deskDAO, SessionConstants.VIEW_OFFICE_FRONT_DESK, SessionConstants.SEARCH_OFFICE_FRONT_DESK);
        try {
            System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        } catch (Exception e) {
            System.out.println("failed to dojob" + e);
        }

        RequestDispatcher rd;
        if (hasAjax == false) {
            System.out.println("Going to office_front_desk/office_front_deskSearch.jsp");
            rd = request.getRequestDispatcher("office_front_desk/office_front_deskSearch.jsp");
        } else {
            System.out.println("Going to office_front_desk/office_front_deskSearchForm.jsp");
            rd = request.getRequestDispatcher("office_front_desk/office_front_deskSearchForm.jsp");
        }
        rd.forward(request, response);
    }
}

*/
