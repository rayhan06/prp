package office_ministries;

import java.io.IOException;
import java.io.*;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import org.apache.log4j.Logger;

import login.LoginDTO;
import permission.MenuConstants;
import role.PermissionRepository;


import sessionmanager.SessionConstants;

import user.UserDTO;
import user.UserRepository;

import util.RecordNavigationManager2;

import java.util.*;
import javax.servlet.http.*;
import java.util.UUID;

import office_ministries.Constants;



import pb.*;
import pbReport.*;

/**
 * Servlet implementation class Office_ministriesServlet
 */
@WebServlet("/Office_ministriesServlet")
@MultipartConfig
public class Office_ministriesServlet extends HttpServlet 
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Office_ministriesServlet.class);
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Office_ministriesServlet() 
	{
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		System.out.println("In doget request = " + request);
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		try
		{
			String actionType = request.getParameter("actionType");
			if(actionType.equals("getAddPage"))
			{
				//if(PermissionRepository.getInstance().checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.OFFICE_MINISTRIES_ADD))
				{
					getAddPage(request, response);
				}
				
			}
			else if(actionType.equals("getEditPage"))
			{
				//if(PermissionRepository.getInstance().checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.OFFICE_MINISTRIES_UPDATE))
				{
					getOffice_ministries(request, response);
				}
							
			}
			else if(actionType.equals("getURL"))
			{
				String URL = request.getParameter("URL");
				System.out.println("URL = " + URL);
				response.sendRedirect(URL);			
			}
			else if(actionType.equals("search"))
			{
				System.out.println("search requested");
				//if(PermissionRepository.getInstance().checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.OFFICE_MINISTRIES_SEARCH))
				{
					searchOffice_ministries(request, response);
				}
							
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}

	private void getAddPage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		request.setAttribute("ID", -1L);
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("office_ministries/office_ministriesEdit.jsp");
		requestDispatcher.forward(request, response);
	}
	private String getFileName(final Part part) 
	{
	    final String partHeader = part.getHeader("content-disposition");
	    System.out.println("Part Header = {0}" +  partHeader);
	    for (String content : part.getHeader("content-disposition").split(";")) {
	        if (content.trim().startsWith("filename")) {
	            return content.substring(
	                    content.indexOf('=') + 1).trim().replace("\"", "");
	        }
	    }
	    return null;
	}

	
	

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		// TODO Auto-generated method stub
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		System.out.println("doPost");
		
		try
		{
			String actionType = request.getParameter("actionType");
			System.out.println("actionType = " + actionType);
			if(actionType.equals("add"))
			{
				
				//if(PermissionRepository.getInstance().checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.OFFICE_MINISTRIES_ADD))
				{
					System.out.println("going to  addOffice_ministries ");
					addOffice_ministries(request, response, true);
				}
				
				
			}
			else if(actionType.equals("edit"))
			{
				
				//if(PermissionRepository.getInstance().checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.OFFICE_MINISTRIES_UPDATE))
				{
					addOffice_ministries(request, response, false);
				}
				
			}
			else if(actionType.equals("delete"))
			{
				deleteOffice_ministries(request, response);
			}
			else if(actionType.equals("search"))
			{
				//if(PermissionRepository.getInstance().checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.OFFICE_MINISTRIES_SEARCH))
				{
					searchOffice_ministries(request, response);
				}
			
			}
			else if(actionType.equals("getGeo"))
			{
				System.out.println("going to geoloc ");
				request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
			}
			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}
	private void addOffice_ministries(HttpServletRequest request, HttpServletResponse response, Boolean addFlag) throws IOException 
	{
		// TODO Auto-generated method stub
		try 
		{
			request.setAttribute("failureMessage", "");
			System.out.println("%%%% addOffice_ministries");
			String path = getServletContext().getRealPath("/img2/");
			Office_ministriesDAO office_ministriesDAO = new Office_ministriesDAO();
			Office_ministriesDTO office_ministriesDTO;
			String FileNamePrefix;
			if(addFlag == true)
			{
				office_ministriesDTO = new Office_ministriesDTO();
				FileNamePrefix = UUID.randomUUID().toString().substring(0, 10);
			}
			else
			{
				office_ministriesDTO = office_ministriesDAO.getOffice_ministriesDTOByID(Long.parseLong(request.getParameter("identity")));
				FileNamePrefix = request.getParameter("identity");
			}
			
			String Value = "";
			Value = request.getParameter("officeType");
			System.out.println("officeType = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				office_ministriesDTO.officeType = Boolean.parseBoolean(Value);
			}
			else
			{
				System.out.println("FieldName has a null value, not updating" + " = " + Value);
			}
			Value = request.getParameter("nameEng");
			System.out.println("nameEng = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				office_ministriesDTO.nameEng = (Value);
			}
			else
			{
				System.out.println("FieldName has a null value, not updating" + " = " + Value);
			}
			Value = request.getParameter("nameBng");
			System.out.println("nameBng = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				office_ministriesDTO.nameBng = (Value);
			}
			else
			{
				System.out.println("FieldName has a null value, not updating" + " = " + Value);
			}
			Value = request.getParameter("nameEngShort");
			System.out.println("nameEngShort = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				office_ministriesDTO.nameEngShort = (Value);
			}
			else
			{
				System.out.println("FieldName has a null value, not updating" + " = " + Value);
			}
			Value = request.getParameter("referenceCode");
			System.out.println("referenceCode = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				office_ministriesDTO.referenceCode = (Value);
			}
			else
			{
				System.out.println("FieldName has a null value, not updating" + " = " + Value);
			}
			Value = request.getParameter("status");
			System.out.println("status = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				office_ministriesDTO.status = Boolean.parseBoolean(Value);
			}
			else
			{
				System.out.println("FieldName has a null value, not updating" + " = " + Value);
			}
			Value = request.getParameter("createdBy");
			System.out.println("createdBy = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				office_ministriesDTO.createdBy = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null value, not updating" + " = " + Value);
			}
			Value = request.getParameter("modifiedBy");
			System.out.println("modifiedBy = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				office_ministriesDTO.modifiedBy = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null value, not updating" + " = " + Value);
			}
			Value = request.getParameter("created");
			System.out.println("created = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				office_ministriesDTO.created = (Value);
			}
			else
			{
				System.out.println("FieldName has a null value, not updating" + " = " + Value);
			}
			Value = request.getParameter("modified");
			System.out.println("modified = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				office_ministriesDTO.modified = (Value);
			}
			else
			{
				System.out.println("FieldName has a null value, not updating" + " = " + Value);
			}
			Value = request.getParameter("approvalPathType");
			System.out.println("approvalPathType = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				office_ministriesDTO.approvalPathType = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null value, not updating" + " = " + Value);
			}
			
			System.out.println("Done adding  addOffice_ministries dto = " + office_ministriesDTO);
			
			if(addFlag == true)
			{
				office_ministriesDAO.addOffice_ministries(office_ministriesDTO);
			}
			else
			{
				office_ministriesDAO.updateOffice_ministries(office_ministriesDTO);
				
			}
			
			
			
			
			
			
			
			
			String inPlaceSubmit = request.getParameter("inplacesubmit");
			
			if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
			{
				getOffice_ministries(request, response);
			}
			else
			{
				response.sendRedirect("Office_ministriesServlet?actionType=search");
			}
					
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	



	
	
	

	private void deleteOffice_ministries(HttpServletRequest request, HttpServletResponse response) throws IOException 
	{				
		try 
		{
			String[] IDsToDelete = request.getParameterValues("ID");
			for(int i = 0; i < IDsToDelete.length; i ++)
			{
				long id = Long.parseLong(IDsToDelete[i]);
				System.out.println("###DELETING " + IDsToDelete[i]);				
				new Office_ministriesDAO().deleteOffice_ministriesByID(id);
			}			
		}
		catch (Exception ex) 
		{
			logger.debug(ex);
		}
		response.sendRedirect("Office_ministriesServlet?actionType=search");
	}

	private void getOffice_ministries(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		System.out.println("in getOffice_ministries");
		Office_ministriesDTO office_ministriesDTO = null;
		try 
		{
			office_ministriesDTO = new Office_ministriesDAO().getOffice_ministriesDTOByID(Long.parseLong(request.getParameter("ID")));
			request.setAttribute("ID", office_ministriesDTO.iD);
			request.setAttribute("office_ministriesDTO",office_ministriesDTO);
			
			String URL= "";
			
			String inPlaceEdit = request.getParameter("inplaceedit");
			String inPlaceSubmit = request.getParameter("inplacesubmit");
			String getBodyOnly = request.getParameter("getBodyOnly");
			
			if(inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase(""))
			{
				URL = "office_ministries/office_ministriesInPlaceEdit.jsp";	
				request.setAttribute("inplaceedit","");				
			}
			else if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
			{
				URL = "office_ministries/office_ministriesSearchRow.jsp";
				request.setAttribute("inplacesubmit","");					
			}
			else
			{
				if(getBodyOnly  != null && !getBodyOnly.equalsIgnoreCase(""))
				{
					URL = "office_ministries/office_ministriesEditBody.jsp?actionType=edit";
				}
				else
				{
					URL = "office_ministries/office_ministriesEdit.jsp?actionType=edit";
				}				
			}
			
			RequestDispatcher rd = request.getRequestDispatcher(URL);
			rd.forward(request, response);
		}
		catch (NumberFormatException e) 
		{
			e.printStackTrace();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	private void searchOffice_ministries(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		System.out.println("in  searchOffice_ministries 1");
        Office_ministriesDAO office_ministriesDAO = new Office_ministriesDAO();
		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
		String ajax = request.getParameter("ajax");
		boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);
		
        RecordNavigationManager2 rnManager = new RecordNavigationManager2(
			SessionConstants.NAV_OFFICE_MINISTRIES,
			request,
			office_ministriesDAO,
			SessionConstants.VIEW_OFFICE_MINISTRIES,
			SessionConstants.SEARCH_OFFICE_MINISTRIES,
			"office_ministries");
        try
        {
			System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        }
        catch(Exception e)
        {
			System.out.println("failed to dojob" + e);
        }

        RequestDispatcher rd;
        if(hasAjax == false)
        {
        	System.out.println("Going to office_ministries/office_ministriesSearch.jsp");
        	rd = request.getRequestDispatcher("office_ministries/office_ministriesSearch.jsp");
        }
        else
        {
        	System.out.println("Going to office_ministries/office_ministriesSearchForm.jsp");
        	rd = request.getRequestDispatcher("office_ministries/office_ministriesSearchForm.jsp");
        }
		rd.forward(request, response);
	}
	
}

