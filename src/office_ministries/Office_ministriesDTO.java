package office_ministries;
import java.util.*; 


public class Office_ministriesDTO {

	public long iD = 0;
	public boolean officeType = false;
    public String nameEng = "";
    public String nameBng = "";
    public String nameEngShort = "";
    public String referenceCode = "";
	public boolean status = false;
	public int createdBy = 0;
	public int modifiedBy = 0;
    public String created = "";
    public String modified = "";
	public int approvalPathType = 0;
	public int isDeleted = 0;
	public long lastModificationTime = 0;
	
	
    @Override
	public String toString() {
            return "$Office_ministriesDTO[" +
            " iD = " + iD +
            " officeType = " + officeType +
            " nameEng = " + nameEng +
            " nameBng = " + nameBng +
            " nameEngShort = " + nameEngShort +
            " referenceCode = " + referenceCode +
            " status = " + status +
            " createdBy = " + createdBy +
            " modifiedBy = " + modifiedBy +
            " created = " + created +
            " modified = " + modified +
            " approvalPathType = " + approvalPathType +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}