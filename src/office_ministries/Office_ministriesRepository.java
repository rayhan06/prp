package office_ministries;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class Office_ministriesRepository implements Repository {
	Office_ministriesDAO office_ministriesDAO = new Office_ministriesDAO();
	
	
	static Logger logger = Logger.getLogger(Office_ministriesRepository.class);
	Map<Long, Office_ministriesDTO>mapOfOffice_ministriesDTOToiD;
	Map<Boolean, Set<Office_ministriesDTO> >mapOfOffice_ministriesDTOToofficeType;
	Map<String, Set<Office_ministriesDTO> >mapOfOffice_ministriesDTOTonameEng;
	Map<String, Set<Office_ministriesDTO> >mapOfOffice_ministriesDTOTonameBng;
	Map<String, Set<Office_ministriesDTO> >mapOfOffice_ministriesDTOTonameEngShort;
	Map<String, Set<Office_ministriesDTO> >mapOfOffice_ministriesDTOToreferenceCode;
	Map<Boolean, Set<Office_ministriesDTO> >mapOfOffice_ministriesDTOTostatus;
	Map<Integer, Set<Office_ministriesDTO> >mapOfOffice_ministriesDTOTocreatedBy;
	Map<Integer, Set<Office_ministriesDTO> >mapOfOffice_ministriesDTOTomodifiedBy;
	Map<String, Set<Office_ministriesDTO> >mapOfOffice_ministriesDTOTocreated;
	Map<String, Set<Office_ministriesDTO> >mapOfOffice_ministriesDTOTomodified;
	Map<Integer, Set<Office_ministriesDTO> >mapOfOffice_ministriesDTOToapprovalPathType;
	Map<Long, Set<Office_ministriesDTO> >mapOfOffice_ministriesDTOTolastModificationTime;


	static Office_ministriesRepository instance = null;  
	private Office_ministriesRepository(){
		mapOfOffice_ministriesDTOToiD = new ConcurrentHashMap<>();
		mapOfOffice_ministriesDTOToofficeType = new ConcurrentHashMap<>();
		mapOfOffice_ministriesDTOTonameEng = new ConcurrentHashMap<>();
		mapOfOffice_ministriesDTOTonameBng = new ConcurrentHashMap<>();
		mapOfOffice_ministriesDTOTonameEngShort = new ConcurrentHashMap<>();
		mapOfOffice_ministriesDTOToreferenceCode = new ConcurrentHashMap<>();
		mapOfOffice_ministriesDTOTostatus = new ConcurrentHashMap<>();
		mapOfOffice_ministriesDTOTocreatedBy = new ConcurrentHashMap<>();
		mapOfOffice_ministriesDTOTomodifiedBy = new ConcurrentHashMap<>();
		mapOfOffice_ministriesDTOTocreated = new ConcurrentHashMap<>();
		mapOfOffice_ministriesDTOTomodified = new ConcurrentHashMap<>();
		mapOfOffice_ministriesDTOToapprovalPathType = new ConcurrentHashMap<>();
		mapOfOffice_ministriesDTOTolastModificationTime = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Office_ministriesRepository getInstance(){
		if (instance == null){
			instance = new Office_ministriesRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		try {
			List<Office_ministriesDTO> office_ministriesDTOs = office_ministriesDAO.getAllOffice_ministries(reloadAll);
			for(Office_ministriesDTO office_ministriesDTO : office_ministriesDTOs) {
				Office_ministriesDTO oldOffice_ministriesDTO = getOffice_ministriesDTOByID(office_ministriesDTO.iD);
				if( oldOffice_ministriesDTO != null ) {
					mapOfOffice_ministriesDTOToiD.remove(oldOffice_ministriesDTO.iD);
				
					if(mapOfOffice_ministriesDTOToofficeType.containsKey(oldOffice_ministriesDTO.officeType)) {
						mapOfOffice_ministriesDTOToofficeType.get(oldOffice_ministriesDTO.officeType).remove(oldOffice_ministriesDTO);
					}
					if(mapOfOffice_ministriesDTOToofficeType.get(oldOffice_ministriesDTO.officeType).isEmpty()) {
						mapOfOffice_ministriesDTOToofficeType.remove(oldOffice_ministriesDTO.officeType);
					}
					
					if(mapOfOffice_ministriesDTOTonameEng.containsKey(oldOffice_ministriesDTO.nameEng)) {
						mapOfOffice_ministriesDTOTonameEng.get(oldOffice_ministriesDTO.nameEng).remove(oldOffice_ministriesDTO);
					}
					if(mapOfOffice_ministriesDTOTonameEng.get(oldOffice_ministriesDTO.nameEng).isEmpty()) {
						mapOfOffice_ministriesDTOTonameEng.remove(oldOffice_ministriesDTO.nameEng);
					}
					
					if(mapOfOffice_ministriesDTOTonameBng.containsKey(oldOffice_ministriesDTO.nameBng)) {
						mapOfOffice_ministriesDTOTonameBng.get(oldOffice_ministriesDTO.nameBng).remove(oldOffice_ministriesDTO);
					}
					if(mapOfOffice_ministriesDTOTonameBng.get(oldOffice_ministriesDTO.nameBng).isEmpty()) {
						mapOfOffice_ministriesDTOTonameBng.remove(oldOffice_ministriesDTO.nameBng);
					}
					
					if(mapOfOffice_ministriesDTOTonameEngShort.containsKey(oldOffice_ministriesDTO.nameEngShort)) {
						mapOfOffice_ministriesDTOTonameEngShort.get(oldOffice_ministriesDTO.nameEngShort).remove(oldOffice_ministriesDTO);
					}
					if(mapOfOffice_ministriesDTOTonameEngShort.get(oldOffice_ministriesDTO.nameEngShort).isEmpty()) {
						mapOfOffice_ministriesDTOTonameEngShort.remove(oldOffice_ministriesDTO.nameEngShort);
					}
					
					if(mapOfOffice_ministriesDTOToreferenceCode.containsKey(oldOffice_ministriesDTO.referenceCode)) {
						mapOfOffice_ministriesDTOToreferenceCode.get(oldOffice_ministriesDTO.referenceCode).remove(oldOffice_ministriesDTO);
					}
					if(mapOfOffice_ministriesDTOToreferenceCode.get(oldOffice_ministriesDTO.referenceCode).isEmpty()) {
						mapOfOffice_ministriesDTOToreferenceCode.remove(oldOffice_ministriesDTO.referenceCode);
					}
					
					if(mapOfOffice_ministriesDTOTostatus.containsKey(oldOffice_ministriesDTO.status)) {
						mapOfOffice_ministriesDTOTostatus.get(oldOffice_ministriesDTO.status).remove(oldOffice_ministriesDTO);
					}
					if(mapOfOffice_ministriesDTOTostatus.get(oldOffice_ministriesDTO.status).isEmpty()) {
						mapOfOffice_ministriesDTOTostatus.remove(oldOffice_ministriesDTO.status);
					}
					
					if(mapOfOffice_ministriesDTOTocreatedBy.containsKey(oldOffice_ministriesDTO.createdBy)) {
						mapOfOffice_ministriesDTOTocreatedBy.get(oldOffice_ministriesDTO.createdBy).remove(oldOffice_ministriesDTO);
					}
					if(mapOfOffice_ministriesDTOTocreatedBy.get(oldOffice_ministriesDTO.createdBy).isEmpty()) {
						mapOfOffice_ministriesDTOTocreatedBy.remove(oldOffice_ministriesDTO.createdBy);
					}
					
					if(mapOfOffice_ministriesDTOTomodifiedBy.containsKey(oldOffice_ministriesDTO.modifiedBy)) {
						mapOfOffice_ministriesDTOTomodifiedBy.get(oldOffice_ministriesDTO.modifiedBy).remove(oldOffice_ministriesDTO);
					}
					if(mapOfOffice_ministriesDTOTomodifiedBy.get(oldOffice_ministriesDTO.modifiedBy).isEmpty()) {
						mapOfOffice_ministriesDTOTomodifiedBy.remove(oldOffice_ministriesDTO.modifiedBy);
					}
					
					if(mapOfOffice_ministriesDTOTocreated.containsKey(oldOffice_ministriesDTO.created)) {
						mapOfOffice_ministriesDTOTocreated.get(oldOffice_ministriesDTO.created).remove(oldOffice_ministriesDTO);
					}
					if(mapOfOffice_ministriesDTOTocreated.get(oldOffice_ministriesDTO.created).isEmpty()) {
						mapOfOffice_ministriesDTOTocreated.remove(oldOffice_ministriesDTO.created);
					}
					
					if(mapOfOffice_ministriesDTOTomodified.containsKey(oldOffice_ministriesDTO.modified)) {
						mapOfOffice_ministriesDTOTomodified.get(oldOffice_ministriesDTO.modified).remove(oldOffice_ministriesDTO);
					}
					if(mapOfOffice_ministriesDTOTomodified.get(oldOffice_ministriesDTO.modified).isEmpty()) {
						mapOfOffice_ministriesDTOTomodified.remove(oldOffice_ministriesDTO.modified);
					}
					
					if(mapOfOffice_ministriesDTOToapprovalPathType.containsKey(oldOffice_ministriesDTO.approvalPathType)) {
						mapOfOffice_ministriesDTOToapprovalPathType.get(oldOffice_ministriesDTO.approvalPathType).remove(oldOffice_ministriesDTO);
					}
					if(mapOfOffice_ministriesDTOToapprovalPathType.get(oldOffice_ministriesDTO.approvalPathType).isEmpty()) {
						mapOfOffice_ministriesDTOToapprovalPathType.remove(oldOffice_ministriesDTO.approvalPathType);
					}
					
					if(mapOfOffice_ministriesDTOTolastModificationTime.containsKey(oldOffice_ministriesDTO.lastModificationTime)) {
						mapOfOffice_ministriesDTOTolastModificationTime.get(oldOffice_ministriesDTO.lastModificationTime).remove(oldOffice_ministriesDTO);
					}
					if(mapOfOffice_ministriesDTOTolastModificationTime.get(oldOffice_ministriesDTO.lastModificationTime).isEmpty()) {
						mapOfOffice_ministriesDTOTolastModificationTime.remove(oldOffice_ministriesDTO.lastModificationTime);
					}
					
					
				}
				if(office_ministriesDTO.isDeleted == 0) 
				{
					
					mapOfOffice_ministriesDTOToiD.put(office_ministriesDTO.iD, office_ministriesDTO);
				
					if( ! mapOfOffice_ministriesDTOToofficeType.containsKey(office_ministriesDTO.officeType)) {
						mapOfOffice_ministriesDTOToofficeType.put(office_ministriesDTO.officeType, new HashSet<>());
					}
					mapOfOffice_ministriesDTOToofficeType.get(office_ministriesDTO.officeType).add(office_ministriesDTO);
					
					if( ! mapOfOffice_ministriesDTOTonameEng.containsKey(office_ministriesDTO.nameEng)) {
						mapOfOffice_ministriesDTOTonameEng.put(office_ministriesDTO.nameEng, new HashSet<>());
					}
					mapOfOffice_ministriesDTOTonameEng.get(office_ministriesDTO.nameEng).add(office_ministriesDTO);
					
					if( ! mapOfOffice_ministriesDTOTonameBng.containsKey(office_ministriesDTO.nameBng)) {
						mapOfOffice_ministriesDTOTonameBng.put(office_ministriesDTO.nameBng, new HashSet<>());
					}
					mapOfOffice_ministriesDTOTonameBng.get(office_ministriesDTO.nameBng).add(office_ministriesDTO);
					
					if( ! mapOfOffice_ministriesDTOTonameEngShort.containsKey(office_ministriesDTO.nameEngShort)) {
						mapOfOffice_ministriesDTOTonameEngShort.put(office_ministriesDTO.nameEngShort, new HashSet<>());
					}
					mapOfOffice_ministriesDTOTonameEngShort.get(office_ministriesDTO.nameEngShort).add(office_ministriesDTO);
					
					if( ! mapOfOffice_ministriesDTOToreferenceCode.containsKey(office_ministriesDTO.referenceCode)) {
						mapOfOffice_ministriesDTOToreferenceCode.put(office_ministriesDTO.referenceCode, new HashSet<>());
					}
					mapOfOffice_ministriesDTOToreferenceCode.get(office_ministriesDTO.referenceCode).add(office_ministriesDTO);
					
					if( ! mapOfOffice_ministriesDTOTostatus.containsKey(office_ministriesDTO.status)) {
						mapOfOffice_ministriesDTOTostatus.put(office_ministriesDTO.status, new HashSet<>());
					}
					mapOfOffice_ministriesDTOTostatus.get(office_ministriesDTO.status).add(office_ministriesDTO);
					
					if( ! mapOfOffice_ministriesDTOTocreatedBy.containsKey(office_ministriesDTO.createdBy)) {
						mapOfOffice_ministriesDTOTocreatedBy.put(office_ministriesDTO.createdBy, new HashSet<>());
					}
					mapOfOffice_ministriesDTOTocreatedBy.get(office_ministriesDTO.createdBy).add(office_ministriesDTO);
					
					if( ! mapOfOffice_ministriesDTOTomodifiedBy.containsKey(office_ministriesDTO.modifiedBy)) {
						mapOfOffice_ministriesDTOTomodifiedBy.put(office_ministriesDTO.modifiedBy, new HashSet<>());
					}
					mapOfOffice_ministriesDTOTomodifiedBy.get(office_ministriesDTO.modifiedBy).add(office_ministriesDTO);
					
					if( ! mapOfOffice_ministriesDTOTocreated.containsKey(office_ministriesDTO.created)) {
						mapOfOffice_ministriesDTOTocreated.put(office_ministriesDTO.created, new HashSet<>());
					}
					mapOfOffice_ministriesDTOTocreated.get(office_ministriesDTO.created).add(office_ministriesDTO);
					
					if( ! mapOfOffice_ministriesDTOTomodified.containsKey(office_ministriesDTO.modified)) {
						mapOfOffice_ministriesDTOTomodified.put(office_ministriesDTO.modified, new HashSet<>());
					}
					mapOfOffice_ministriesDTOTomodified.get(office_ministriesDTO.modified).add(office_ministriesDTO);
					
					if( ! mapOfOffice_ministriesDTOToapprovalPathType.containsKey(office_ministriesDTO.approvalPathType)) {
						mapOfOffice_ministriesDTOToapprovalPathType.put(office_ministriesDTO.approvalPathType, new HashSet<>());
					}
					mapOfOffice_ministriesDTOToapprovalPathType.get(office_ministriesDTO.approvalPathType).add(office_ministriesDTO);
					
					if( ! mapOfOffice_ministriesDTOTolastModificationTime.containsKey(office_ministriesDTO.lastModificationTime)) {
						mapOfOffice_ministriesDTOTolastModificationTime.put(office_ministriesDTO.lastModificationTime, new HashSet<>());
					}
					mapOfOffice_ministriesDTOTolastModificationTime.get(office_ministriesDTO.lastModificationTime).add(office_ministriesDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Office_ministriesDTO> getOffice_ministriesList() {
		List <Office_ministriesDTO> office_ministriess = new ArrayList<Office_ministriesDTO>(this.mapOfOffice_ministriesDTOToiD.values());
		return office_ministriess;
	}
	
	
	public Office_ministriesDTO getOffice_ministriesDTOByID( long ID){
		return mapOfOffice_ministriesDTOToiD.get(ID);
	}
	
	
	public List<Office_ministriesDTO> getOffice_ministriesDTOByoffice_type(boolean office_type) {
		return new ArrayList<>( mapOfOffice_ministriesDTOToofficeType.getOrDefault(office_type,new HashSet<>()));
	}
	
	
	public List<Office_ministriesDTO> getOffice_ministriesDTOByname_eng(String name_eng) {
		return new ArrayList<>( mapOfOffice_ministriesDTOTonameEng.getOrDefault(name_eng,new HashSet<>()));
	}
	
	
	public List<Office_ministriesDTO> getOffice_ministriesDTOByname_bng(String name_bng) {
		return new ArrayList<>( mapOfOffice_ministriesDTOTonameBng.getOrDefault(name_bng,new HashSet<>()));
	}
	
	
	public List<Office_ministriesDTO> getOffice_ministriesDTOByname_eng_short(String name_eng_short) {
		return new ArrayList<>( mapOfOffice_ministriesDTOTonameEngShort.getOrDefault(name_eng_short,new HashSet<>()));
	}
	
	
	public List<Office_ministriesDTO> getOffice_ministriesDTOByreference_code(String reference_code) {
		return new ArrayList<>( mapOfOffice_ministriesDTOToreferenceCode.getOrDefault(reference_code,new HashSet<>()));
	}
	
	
	public List<Office_ministriesDTO> getOffice_ministriesDTOBystatus(boolean status) {
		return new ArrayList<>( mapOfOffice_ministriesDTOTostatus.getOrDefault(status,new HashSet<>()));
	}
	
	
	public List<Office_ministriesDTO> getOffice_ministriesDTOBycreated_by(int created_by) {
		return new ArrayList<>( mapOfOffice_ministriesDTOTocreatedBy.getOrDefault(created_by,new HashSet<>()));
	}
	
	
	public List<Office_ministriesDTO> getOffice_ministriesDTOBymodified_by(int modified_by) {
		return new ArrayList<>( mapOfOffice_ministriesDTOTomodifiedBy.getOrDefault(modified_by,new HashSet<>()));
	}
	
	
	public List<Office_ministriesDTO> getOffice_ministriesDTOBycreated(String created) {
		return new ArrayList<>( mapOfOffice_ministriesDTOTocreated.getOrDefault(created,new HashSet<>()));
	}
	
	
	public List<Office_ministriesDTO> getOffice_ministriesDTOBymodified(String modified) {
		return new ArrayList<>( mapOfOffice_ministriesDTOTomodified.getOrDefault(modified,new HashSet<>()));
	}
	
	
	public List<Office_ministriesDTO> getOffice_ministriesDTOByapproval_path_type(int approval_path_type) {
		return new ArrayList<>( mapOfOffice_ministriesDTOToapprovalPathType.getOrDefault(approval_path_type,new HashSet<>()));
	}
	
	
	public List<Office_ministriesDTO> getOffice_ministriesDTOBylastModificationTime(long lastModificationTime) {
		return new ArrayList<>( mapOfOffice_ministriesDTOTolastModificationTime.getOrDefault(lastModificationTime,new HashSet<>()));
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "office_ministries";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


