package office_ministries;
import java.util.*; 


public class Office_ministriesMAPS 
{

	public HashMap<String, String> java_allfield_type_map = new HashMap<String, String>();
	public HashMap<String, String> java_anyfield_search_map = new HashMap<String, String>();
	public HashMap<String, String> java_DTO_map = new HashMap<String, String>();
	public HashMap<String, String> java_SQL_map = new HashMap<String, String>();
	public HashMap<String, String> java_Text_map = new HashMap<String, String>();
	
	private static Office_ministriesMAPS self = null;
	
	private Office_ministriesMAPS()
	{
		
		java_allfield_type_map.put("office_type".toLowerCase(), "Boolean");
		java_allfield_type_map.put("name_eng".toLowerCase(), "String");
		java_allfield_type_map.put("name_bng".toLowerCase(), "String");
		java_allfield_type_map.put("name_eng_short".toLowerCase(), "String");
		java_allfield_type_map.put("reference_code".toLowerCase(), "String");
		java_allfield_type_map.put("status".toLowerCase(), "Boolean");
		java_allfield_type_map.put("created_by".toLowerCase(), "Integer");
		java_allfield_type_map.put("modified_by".toLowerCase(), "Integer");
		java_allfield_type_map.put("created".toLowerCase(), "String");
		java_allfield_type_map.put("modified".toLowerCase(), "String");
		java_allfield_type_map.put("approval_path_type".toLowerCase(), "Integer");

		java_anyfield_search_map.put("office.name_en".toLowerCase(), "String");
		java_anyfield_search_map.put("office.name_bn".toLowerCase(), "String");
		java_anyfield_search_map.put("office_ministries.name_eng".toLowerCase(), "String");
		java_anyfield_search_map.put("office_ministries.name_bng".toLowerCase(), "String");
		java_anyfield_search_map.put("office_ministries.name_eng_short".toLowerCase(), "String");
		java_anyfield_search_map.put("office_ministries.reference_code".toLowerCase(), "String");
		java_anyfield_search_map.put("office_ministries.status".toLowerCase(), "Boolean");
		java_anyfield_search_map.put("office_ministries.created_by".toLowerCase(), "Integer");
		java_anyfield_search_map.put("office_ministries.modified_by".toLowerCase(), "Integer");
		java_anyfield_search_map.put("office_ministries.created".toLowerCase(), "String");
		java_anyfield_search_map.put("office_ministries.modified".toLowerCase(), "String");
		java_anyfield_search_map.put("approval_path.name_en".toLowerCase(), "String");
		java_anyfield_search_map.put("approval_path.name_bn".toLowerCase(), "String");

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("officeType".toLowerCase(), "officeType".toLowerCase());
		java_DTO_map.put("nameEng".toLowerCase(), "nameEng".toLowerCase());
		java_DTO_map.put("nameBng".toLowerCase(), "nameBng".toLowerCase());
		java_DTO_map.put("nameEngShort".toLowerCase(), "nameEngShort".toLowerCase());
		java_DTO_map.put("referenceCode".toLowerCase(), "referenceCode".toLowerCase());
		java_DTO_map.put("status".toLowerCase(), "status".toLowerCase());
		java_DTO_map.put("createdBy".toLowerCase(), "createdBy".toLowerCase());
		java_DTO_map.put("modifiedBy".toLowerCase(), "modifiedBy".toLowerCase());
		java_DTO_map.put("created".toLowerCase(), "created".toLowerCase());
		java_DTO_map.put("modified".toLowerCase(), "modified".toLowerCase());
		java_DTO_map.put("approvalPathType".toLowerCase(), "approvalPathType".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("office_type".toLowerCase(), "officeType".toLowerCase());
		java_SQL_map.put("name_eng".toLowerCase(), "nameEng".toLowerCase());
		java_SQL_map.put("name_bng".toLowerCase(), "nameBng".toLowerCase());
		java_SQL_map.put("name_eng_short".toLowerCase(), "nameEngShort".toLowerCase());
		java_SQL_map.put("reference_code".toLowerCase(), "referenceCode".toLowerCase());
		java_SQL_map.put("status".toLowerCase(), "status".toLowerCase());
		java_SQL_map.put("created_by".toLowerCase(), "createdBy".toLowerCase());
		java_SQL_map.put("modified_by".toLowerCase(), "modifiedBy".toLowerCase());
		java_SQL_map.put("created".toLowerCase(), "created".toLowerCase());
		java_SQL_map.put("modified".toLowerCase(), "modified".toLowerCase());
		java_SQL_map.put("approval_path_type".toLowerCase(), "approvalPathType".toLowerCase());
		java_SQL_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Office".toLowerCase(), "officeType".toLowerCase());
		java_Text_map.put("Name Eng".toLowerCase(), "nameEng".toLowerCase());
		java_Text_map.put("Name Bng".toLowerCase(), "nameBng".toLowerCase());
		java_Text_map.put("Name Eng Short".toLowerCase(), "nameEngShort".toLowerCase());
		java_Text_map.put("Reference Code".toLowerCase(), "referenceCode".toLowerCase());
		java_Text_map.put("Status".toLowerCase(), "status".toLowerCase());
		java_Text_map.put("Created By".toLowerCase(), "createdBy".toLowerCase());
		java_Text_map.put("Modified By".toLowerCase(), "modifiedBy".toLowerCase());
		java_Text_map.put("Created".toLowerCase(), "created".toLowerCase());
		java_Text_map.put("Modified".toLowerCase(), "modified".toLowerCase());
		java_Text_map.put("Approval Path".toLowerCase(), "approvalPathType".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}
	
	public static Office_ministriesMAPS GetInstance()
	{
		if(self == null)
		{
			self = new Office_ministriesMAPS ();
		}
		return self;
	}
	

}