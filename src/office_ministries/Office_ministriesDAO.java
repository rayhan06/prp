package office_ministries;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;


import org.apache.log4j.Logger;

import dbm.*;
import login.LoginDTO;
import repository.RepositoryManager;

import util.NavigationService2;


public class Office_ministriesDAO  extends NavigationService2
{
	
	Logger logger = Logger.getLogger(getClass());
	
	
	public long addOffice_ministries(Office_ministriesDTO office_ministriesDTO) throws Exception{
		
		Connection connection = null;
		PreparedStatement ps = null;

		long lastModificationTime = System.currentTimeMillis();	

		try{
			connection = DBMW.getInstance().getConnection();
			
			if(connection == null)
			{
				System.out.println("nullconn");
			}

			office_ministriesDTO.iD = DBMW.getInstance().getNextSequenceId("office_ministries");

			String sql = "INSERT INTO office_ministries";
			
			sql += " (";
			sql += "ID";
			sql += ", ";
			sql += "office_type";
			sql += ", ";
			sql += "name_eng";
			sql += ", ";
			sql += "name_bng";
			sql += ", ";
			sql += "name_eng_short";
			sql += ", ";
			sql += "reference_code";
			sql += ", ";
			sql += "status";
			sql += ", ";
			sql += "created_by";
			sql += ", ";
			sql += "modified_by";
			sql += ", ";
			sql += "created";
			sql += ", ";
			sql += "modified";
			sql += ", ";
			sql += "approval_path_type";
			sql += ", ";
			sql += "isDeleted";
			sql += ", ";
			sql += "lastModificationTime";
			sql += ")";
			
			
            sql += " VALUES(";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ")";
				
			printSql(sql);

			ps = connection.prepareStatement(sql);
			
			
			

			int index = 1;

			ps.setObject(index++,office_ministriesDTO.iD);
			ps.setObject(index++,office_ministriesDTO.officeType);
			ps.setObject(index++,office_ministriesDTO.nameEng);
			ps.setObject(index++,office_ministriesDTO.nameBng);
			ps.setObject(index++,office_ministriesDTO.nameEngShort);
			ps.setObject(index++,office_ministriesDTO.referenceCode);
			ps.setObject(index++,office_ministriesDTO.status);
			ps.setObject(index++,office_ministriesDTO.createdBy);
			ps.setObject(index++,office_ministriesDTO.modifiedBy);
			ps.setObject(index++,office_ministriesDTO.created);
			ps.setObject(index++,office_ministriesDTO.modified);
			ps.setObject(index++,office_ministriesDTO.approvalPathType);
			ps.setObject(index++,office_ministriesDTO.isDeleted);
			ps.setObject(index++, lastModificationTime);
			
			System.out.println(ps);
			ps.execute();
			
			
			recordUpdateTime(connection,lastModificationTime);

		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if (ps != null) {
					ps.close();
				}
			} catch(Exception e){}
			try{
				if(connection != null){
					DBMW.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		return office_ministriesDTO.iD;		
	}
	
	
	//need another getter for repository
	public Office_ministriesDTO getOffice_ministriesDTOByID (long ID) throws Exception{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Office_ministriesDTO office_ministriesDTO = null;
		try{
			
			String sql = "SELECT * ";

			sql += " FROM office_ministries";
			
            sql += " WHERE ID=" + ID;
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			if(rs.next()){
				office_ministriesDTO = new Office_ministriesDTO();

				office_ministriesDTO.iD = rs.getLong("ID");
				office_ministriesDTO.officeType = rs.getBoolean("office_type");
				office_ministriesDTO.nameEng = rs.getString("name_eng");
				office_ministriesDTO.nameBng = rs.getString("name_bng");
				office_ministriesDTO.nameEngShort = rs.getString("name_eng_short");
				office_ministriesDTO.referenceCode = rs.getString("reference_code");
				office_ministriesDTO.status = rs.getBoolean("status");
				office_ministriesDTO.createdBy = rs.getInt("created_by");
				office_ministriesDTO.modifiedBy = rs.getInt("modified_by");
				office_ministriesDTO.created = rs.getString("created");
				office_ministriesDTO.modified = rs.getString("modified");
				office_ministriesDTO.approvalPathType = rs.getInt("approval_path_type");
				office_ministriesDTO.isDeleted = rs.getInt("isDeleted");
				office_ministriesDTO.lastModificationTime = rs.getLong("lastModificationTime");

			}			
			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DBMR.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
		return office_ministriesDTO;
	}
	public void updateOfficeMinistries_approvalPathByID(long ID, int approval_path_type) throws Exception{
		
		Connection connection = null;
		PreparedStatement ps = null;

		long lastModificationTime = System.currentTimeMillis();	
		try{
			connection = DBMW.getInstance().getConnection();

			String sql = "UPDATE office_ministries";
			
			sql += " SET ";
			sql += "approval_path_type=? , lastModificationTime=" + lastModificationTime;
			sql += " WHERE id  =  " + ID;

				
			printSql(sql);

			ps = connection.prepareStatement(sql);
			
			

			int index = 1;
			
			ps.setObject(index++,approval_path_type);
			System.out.println(ps);
			ps.executeUpdate();
			


			
			recordUpdateTime(connection, lastModificationTime);

						
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if (ps != null) {
					ps.close();
				}
			} catch(Exception e){}
			try{
				if(connection != null){
					DBMW.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
	}
	public long updateOffice_ministries(Office_ministriesDTO office_ministriesDTO) throws Exception{
		
		Connection connection = null;
		PreparedStatement ps = null;

		long lastModificationTime = System.currentTimeMillis();	
		try{
			connection = DBMW.getInstance().getConnection();

			String sql = "UPDATE office_ministries";
			
			sql += " SET ";
			sql += "office_type=?";
			sql += ", ";
			sql += "name_eng=?";
			sql += ", ";
			sql += "name_bng=?";
			sql += ", ";
			sql += "name_eng_short=?";
			sql += ", ";
			sql += "reference_code=?";
			sql += ", ";
			sql += "status=?";
			sql += ", ";
			sql += "created_by=?";
			sql += ", ";
			sql += "modified_by=?";
			sql += ", ";
			sql += "created=?";
			sql += ", ";
			sql += "modified=?";
			sql += ", ";
			sql += "approval_path_type=?";
			sql += ", lastModificationTime = "	+ lastModificationTime + "";
            sql += " WHERE ID = " + office_ministriesDTO.iD;
				
			printSql(sql);

			ps = connection.prepareStatement(sql);
			
			

			int index = 1;
			
			ps.setObject(index++,office_ministriesDTO.officeType);
			ps.setObject(index++,office_ministriesDTO.nameEng);
			ps.setObject(index++,office_ministriesDTO.nameBng);
			ps.setObject(index++,office_ministriesDTO.nameEngShort);
			ps.setObject(index++,office_ministriesDTO.referenceCode);
			ps.setObject(index++,office_ministriesDTO.status);
			ps.setObject(index++,office_ministriesDTO.createdBy);
			ps.setObject(index++,office_ministriesDTO.modifiedBy);
			ps.setObject(index++,office_ministriesDTO.created);
			ps.setObject(index++,office_ministriesDTO.modified);
			ps.setObject(index++,office_ministriesDTO.approvalPathType);
			System.out.println(ps);
			ps.executeUpdate();
			


			
			recordUpdateTime(connection, lastModificationTime);

						
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if (ps != null) {
					ps.close();
				}
			} catch(Exception e){}
			try{
				if(connection != null){
					DBMW.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		return office_ministriesDTO.iD;
	}
	
	public void deleteOffice_ministriesByID(long ID) throws Exception{
		Connection connection = null;
		Statement stmt = null;
		PreparedStatement ps = null;
		
		long lastModificationTime = System.currentTimeMillis();	
		try{
			String sql = "UPDATE office_ministries";
			
			sql += " SET isDeleted=1,lastModificationTime="+ lastModificationTime +" WHERE ID = " + ID;
			
			printSql(sql);

			connection = DBMW.getInstance().getConnection();
			stmt  = connection.createStatement();
			stmt.execute(sql);
			

			
			recordUpdateTime(connection, lastModificationTime);

			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DBMW.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
	}

	
	
	public List<Office_ministriesDTO> getDTOs(Collection recordIDs){
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Office_ministriesDTO office_ministriesDTO = null;
		List<Office_ministriesDTO> office_ministriesDTOList = new ArrayList<>();
		if(recordIDs.isEmpty()){
			return office_ministriesDTOList;
		}
		try{
			
			String sql = "SELECT * ";

			sql += " FROM office_ministries";
            
            sql += " WHERE ID IN ( ";

			for(int i = 0;i<recordIDs.size();i++){
				if(i!=0){
					sql+=",";
				}
				sql+=((ArrayList)recordIDs).get(i);
			}
			sql+=")  order by lastModificationTime desc";
			
			printSql(sql);
			
			logger.debug("sql " + sql);
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				office_ministriesDTO = new Office_ministriesDTO();
				office_ministriesDTO.iD = rs.getLong("ID");
				office_ministriesDTO.officeType = rs.getBoolean("office_type");
				office_ministriesDTO.nameEng = rs.getString("name_eng");
				office_ministriesDTO.nameBng = rs.getString("name_bng");
				office_ministriesDTO.nameEngShort = rs.getString("name_eng_short");
				office_ministriesDTO.referenceCode = rs.getString("reference_code");
				office_ministriesDTO.status = rs.getBoolean("status");
				office_ministriesDTO.createdBy = rs.getInt("created_by");
				office_ministriesDTO.modifiedBy = rs.getInt("modified_by");
				office_ministriesDTO.created = rs.getString("created");
				office_ministriesDTO.modified = rs.getString("modified");
				office_ministriesDTO.approvalPathType = rs.getInt("approval_path_type");
				office_ministriesDTO.isDeleted = rs.getInt("isDeleted");
				office_ministriesDTO.lastModificationTime = rs.getLong("lastModificationTime");
				System.out.println("got this DTO: " + office_ministriesDTO);
				
				office_ministriesDTOList.add(office_ministriesDTO);

			}			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DBMR.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
		return office_ministriesDTOList;
	
	}
	
	

	
	
	
	//add repository
	public List<Office_ministriesDTO> getAllOffice_ministries (boolean isFirstReload)
    {
		List<Office_ministriesDTO> office_ministriesDTOList = new ArrayList<>();

		String sql = "SELECT * FROM office_ministries";
		sql += " WHERE ";
	

		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by office_ministries.lastModificationTime desc";
		printSql(sql);
		
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;

		
		try{
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();
			rs = stmt.executeQuery(sql);
			

			while(rs.next()){
				Office_ministriesDTO office_ministriesDTO = new Office_ministriesDTO();
				office_ministriesDTO.iD = rs.getLong("ID");
				office_ministriesDTO.officeType = rs.getBoolean("office_type");
				office_ministriesDTO.nameEng = rs.getString("name_eng");
				office_ministriesDTO.nameBng = rs.getString("name_bng");
				office_ministriesDTO.nameEngShort = rs.getString("name_eng_short");
				office_ministriesDTO.referenceCode = rs.getString("reference_code");
				office_ministriesDTO.status = rs.getBoolean("status");
				office_ministriesDTO.createdBy = rs.getInt("created_by");
				office_ministriesDTO.modifiedBy = rs.getInt("modified_by");
				office_ministriesDTO.created = rs.getString("created");
				office_ministriesDTO.modified = rs.getString("modified");
				//office_ministriesDTO.approvalPathType = rs.getInt("approval_path_type");
				office_ministriesDTO.isDeleted = rs.getInt("isDeleted");
				office_ministriesDTO.lastModificationTime = rs.getLong("lastModificationTime");
				
				office_ministriesDTOList.add(office_ministriesDTO);
			}			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			try{ 
				if (connection != null){ 
					DBMR.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}

		return office_ministriesDTOList;
    }
	
	public List<Office_ministriesDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset)
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		List<Office_ministriesDTO> office_ministriesDTOList = new ArrayList<>();

		try{
			
			String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS);
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				Office_ministriesDTO office_ministriesDTO = new Office_ministriesDTO();
				office_ministriesDTO.iD = rs.getLong("ID");
				office_ministriesDTO.officeType = rs.getBoolean("office_type");
				office_ministriesDTO.nameEng = rs.getString("name_eng");
				office_ministriesDTO.nameBng = rs.getString("name_bng");
				office_ministriesDTO.nameEngShort = rs.getString("name_eng_short");
				office_ministriesDTO.referenceCode = rs.getString("reference_code");
				office_ministriesDTO.status = rs.getBoolean("status");
				office_ministriesDTO.createdBy = rs.getInt("created_by");
				office_ministriesDTO.modifiedBy = rs.getInt("modified_by");
				office_ministriesDTO.created = rs.getString("created");
				office_ministriesDTO.modified = rs.getString("modified");
				office_ministriesDTO.approvalPathType = rs.getInt("approval_path_type");
				office_ministriesDTO.isDeleted = rs.getInt("isDeleted");
				office_ministriesDTO.lastModificationTime = rs.getLong("lastModificationTime");
				
				office_ministriesDTOList.add(office_ministriesDTO);
			}					
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DBMR.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
		return office_ministriesDTOList;
	
	}
	public Collection getIDs() 
    {
        Collection data = new ArrayList();
        Connection connection=null;
    	Statement stmt=null;
    	ResultSet resultSet = null;
    	
        String sql = getSqlWithSearchCriteria(null, -1, -1, GETIDS);
		
		printSql(sql);
		
        try
        {
	        connection = DBMR.getInstance().getConnection();
	        stmt = connection.createStatement();
	        
	        for(resultSet = stmt.executeQuery(sql); resultSet.next(); data.add(resultSet.getString("ID")));
	
	        resultSet.close();
        }
        catch (Exception e)
        {
	    	logger.fatal("DAO " + e, e);
        }
	    finally
        {
	    	try
            {
          	  if(resultSet!= null && !resultSet.isClosed())
          	  {
          		  resultSet.close();
          	  }
            }
            catch(Exception ex)
            {
          	  
            }
          try{if (stmt != null){stmt.close();}}catch (Exception e){}
          try{if (connection != null){DBMR.getInstance().freeConnection(connection);}}
          catch (Exception e){logger.fatal("DAO finally :" + e);}
        }
        return data;
    }
	
	public String getSqlWithSearchCriteria(Hashtable p_searchCriteria, int limit, int offset, int category)
    {
		String sql = "SELECT ";
		if(category == GETIDS)
		{
			sql += "distinct office_ministries.ID as ID ";
		}
		else if(category == GETCOUNT)
		{
			sql += " count(office_ministries.ID) as countID ";
		}
		else if(category == GETDTOS)
		{
			sql += "  office_ministries.* ";
		}
		sql += "FROM office_ministries ";
		
		String AnyfieldSql = "";
		String AllFieldSql = "";
		
		if(p_searchCriteria != null)
		{
						
			
			Enumeration names = p_searchCriteria.keys();
			String str, value;
			
			AnyfieldSql = "(";
			
			if(p_searchCriteria.get("AnyField")!= null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase(""))
			{
				int i = 0;
				Iterator it = Office_ministriesMAPS.GetInstance().java_anyfield_search_map.entrySet().iterator();
				while(it.hasNext())
				{
					if( i > 0)
		        	{
						AnyfieldSql+= " OR  ";
		        	}
					Map.Entry pair = (Map.Entry)it.next();
					AnyfieldSql+= pair.getKey() + " like '%" + p_searchCriteria.get("AnyField").toString() + "%'";
					i ++;
				}						
			}
			AnyfieldSql += ")";
			System.out.println("AnyfieldSql = " + AnyfieldSql);
			
			AllFieldSql = "(";
			int i = 0;
			while(names.hasMoreElements())
			{				
				str = (String) names.nextElement();
				value = (String)p_searchCriteria.get(str);
		        System.out.println(str + ": " + value);
		        if(Office_ministriesMAPS.GetInstance().java_allfield_type_map.get(str.toLowerCase()) != null &&  !Office_ministriesMAPS.GetInstance().java_allfield_type_map.get(str.toLowerCase()).equalsIgnoreCase("")
		        		&& !str.equalsIgnoreCase("AnyField")
		        		&& value != null && !value.equalsIgnoreCase(""))
		        {
					if(p_searchCriteria.get(str).equals("any"))
		        	{
		        		continue;
		        	}
					
		        	if( i > 0)
		        	{
		        		AllFieldSql+= " AND  ";
		        	}
		        	if(Office_ministriesMAPS.GetInstance().java_allfield_type_map.get(str.toLowerCase()).equals("String")) 
		        	{
		        		AllFieldSql += "office_ministries." + str.toLowerCase() + " like '%" + p_searchCriteria.get(str) + "%'";
		        	}
		        	else
		        	{
		        		AllFieldSql += "office_ministries." + str.toLowerCase() + " = '" + p_searchCriteria.get(str) + "'";
		        	}
		        	i ++;
		        }
			}
			
			AllFieldSql += ")";
			System.out.println("AllFieldSql = " + AllFieldSql);
			
			
		}
		
		if(!AnyfieldSql.equals("()") && !AnyfieldSql.equals(""))
		{
			sql += " join office on office_ministries.office_type = office.ID ";
			sql += " join approval_path on office_ministries.approval_path_type = approval_path.ID ";
			
		}
		sql += " WHERE ";
		sql += " office_ministries.isDeleted = 0 ";
		
		if(!AnyfieldSql.equals("()") && !AnyfieldSql.equals(""))
		{
			sql += " AND " + AnyfieldSql;
			
		}
		if(!AllFieldSql.equals("()") && !AllFieldSql.equals(""))
		{			
			sql += " AND " + AllFieldSql;
		}

			
		sql += " order by office_ministries.lastModificationTime desc ";

		printSql(sql);
		
		if(limit >= 0)
		{
			sql += " limit " + limit;
		}
		if(offset >= 0)
		{
			sql += " offset " + offset;
		}
		
		//System.out.println("-------------- sql = " + sql);
		
		return sql;
    }			
}
	