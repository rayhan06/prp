/*
package general_purpose_servlet;

import login.LoginDTO;
import office_head.OfficeHeadServlet;
import org.apache.log4j.Logger;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/GeneralPurposeServlet")
@MultipartConfig
public class GeneralPurposeServlet extends HttpServlet {
    public static Logger logger = Logger.getLogger(OfficeHeadServlet.class);

    public GeneralPurposeServlet() {
        super();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getInstance().getUserDtoByUserId(loginDTO.userID);
        try {
            String actionType = request.getParameter("actionType");
            if (actionType.equals("getUserInfo")) {
                GeneralPurposeDAO generalPurposeDAO = new GeneralPurposeDAO();
                response.setContentType("application/json");

                PrintWriter out = response.getWriter();
                out.print(generalPurposeDAO.getUserInformation(userDTO.employee_record_id, userDTO.officeID));
                out.flush();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.debug(ex);
        }
    }

}
*/
