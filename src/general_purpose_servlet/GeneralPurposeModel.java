package general_purpose_servlet;

public class GeneralPurposeModel {
    public static class OfficeModel {
        private int id;
        private int office_ministry_id;
        private int office_layer_id;
        private int custom_layer_id;
        private int office_origin_id;
        private String office_name_eng;
        private String office_name_bng;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getOffice_ministry_id() {
            return office_ministry_id;
        }

        public void setOffice_ministry_id(int office_ministry_id) {
            this.office_ministry_id = office_ministry_id;
        }

        public int getOffice_layer_id() {
            return office_layer_id;
        }

        public void setOffice_layer_id(int office_layer_id) {
            this.office_layer_id = office_layer_id;
        }

        public int getCustom_layer_id() {
            return custom_layer_id;
        }

        public void setCustom_layer_id(int custom_layer_id) {
            this.custom_layer_id = custom_layer_id;
        }

        public int getOffice_origin_id() {
            return office_origin_id;
        }

        public void setOffice_origin_id(int office_origin_id) {
            this.office_origin_id = office_origin_id;
        }

        public String getOffice_name_eng() {
            return office_name_eng;
        }

        public void setOffice_name_eng(String office_name_eng) {
            this.office_name_eng = office_name_eng;
        }

        public String getOffice_name_bng() {
            return office_name_bng;
        }

        public void setOffice_name_bng(String office_name_bng) {
            this.office_name_bng = office_name_bng;
        }
    }

    public static class UserModel {
        public int employee_record_id;
        public String user_name_eng;
        public String user_name_bng;
        public String nid;
        public String personal_mobile;
        public String office_name_bng;
        public String office_name_eng;
        public int office_id;
        public int office_unit_id;
        public int office_unit_organogram_id;
        public String designation;

        public int getEmployee_record_id() {
            return employee_record_id;
        }

        public void setEmployee_record_id(int employee_record_id) {
            this.employee_record_id = employee_record_id;
        }

        public String getUser_name_eng() {
            return user_name_eng;
        }

        public void setUser_name_eng(String user_name_eng) {
            this.user_name_eng = user_name_eng;
        }

        public String getUser_name_bng() {
            return user_name_bng;
        }

        public void setUser_name_bng(String user_name_bng) {
            this.user_name_bng = user_name_bng;
        }

        public String getNid() {
            return nid;
        }

        public void setNid(String nid) {
            this.nid = nid;
        }

        public String getPersonal_mobile() {
            return personal_mobile;
        }

        public void setPersonal_mobile(String personal_mobile) {
            this.personal_mobile = personal_mobile;
        }

        public String getOffice_name_bng() {
            return office_name_bng;
        }

        public void setOffice_name_bng(String office_name_bng) {
            this.office_name_bng = office_name_bng;
        }

        public String getOffice_name_eng() {
            return office_name_eng;
        }

        public void setOffice_name_eng(String office_name_eng) {
            this.office_name_eng = office_name_eng;
        }

        public int getOffice_id() {
            return office_id;
        }

        public void setOffice_id(int office_id) {
            this.office_id = office_id;
        }

        public int getOffice_unit_id() {
            return office_unit_id;
        }

        public void setOffice_unit_id(int office_unit_id) {
            this.office_unit_id = office_unit_id;
        }

        public int getOffice_unit_organogram_id() {
            return office_unit_organogram_id;
        }

        public void setOffice_unit_organogram_id(int office_unit_organogram_id) {
            this.office_unit_organogram_id = office_unit_organogram_id;
        }

        public String getDesignation() {
            return designation;
        }

        public void setDesignation(String designation) {
            this.designation = designation;
        }
    }
}
