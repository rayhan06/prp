package general_purpose_servlet;

import test_lib.RQueryBuilder;

import java.util.ArrayList;

public class GeneralPurposeDAO {

    public GeneralPurposeModel.OfficeModel getOfficeModelByOfficeId(long officeId) {
        String sql = "SELECT id, office_ministry_id, office_layer_id, custom_layer_id, office_origin_id, office_name_eng, office_name_bng from offices WHERE id =  " + officeId;

        RQueryBuilder<GeneralPurposeModel.OfficeModel> rQueryBuilder = new RQueryBuilder<>();
        ArrayList<GeneralPurposeModel.OfficeModel> data = rQueryBuilder.setSql(sql).of(GeneralPurposeModel.OfficeModel.class).buildRaw();
        return (data != null && data.size() > 0) ? data.get(0) : null;
    }

    String getUserInformation(long employeeRecordId, long officeId) {
        String sql = String.format("SELECT employee_records.id as employee_record_id, " +
                        "employee_records.name_eng as user_name_eng, \n" +
                        "employee_records.name_bng as user_name_bng, \n" +
                        "employee_records.nid, \n" +
                        "employee_records.personal_mobile, \n" +
                        "offices.office_name_bng, \n" +
                        "offices.office_name_eng,\n" +
                        "employee_offices.office_id, \n" +
                        "employee_offices.office_unit_id, \n" +
                        "employee_offices.office_unit_organogram_id, \n" +
                        "employee_offices.designation \n" +
                        "from employee_records, employee_offices, offices\n" +
                        "where employee_records.id = %d and offices.id = %d and employee_offices.employee_record_id = %d and employee_offices.office_id = %d;",
                employeeRecordId, officeId, employeeRecordId, officeId);

        RQueryBuilder<GeneralPurposeModel.UserModel> rQueryBuilder = new RQueryBuilder<>();
        String data = rQueryBuilder.setSql(sql).of(GeneralPurposeModel.UserModel.class).buildJson();
        return data;
    }
}
