package workflow;

import common.ConnectionAndStatementUtil;
import employee_offices.EmployeeOfficeDTO;
import employee_offices.EmployeeOfficeRepository;
import employee_offices.Employee_officesDTO;
import employee_records.EmployeeDesignationModel;
import employee_records.EmployeeOrganogramModel;
import employee_records.Employee_recordsDTO;
import employee_records.Employee_recordsRepository;
import employee_records.EmploymentStatusEnum;
import family.FamilyMemberDTO;
import files.FilesDAO;
import files.FilesDTO;
import geolocation.GeoLocationDAO2;
import office_unit_organograms.OfficeUnitOrganograms;
import office_unit_organograms.OfficeUnitOrganogramsRepository;
import office_units.OfficeUnitTypeEnum;
import office_units.Office_unitsDTO;
import office_units.Office_unitsRepository;
import office_units.Organization;
import other_office.Other_officeRepository;

import org.apache.log4j.Logger;

import com.google.gson.Gson;

import pb.CatDAO;
import pb.CommonDAO;
import pb.Utils;
import pbReport.OtherOfficeConverter;
import sessionmanager.SessionConstants;

import user.UserDAO;
import user.UserDTO;
import user.UserNameAndDetails;
import user.UserRepository;
import util.StringUtils;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;

import java.awt.image.BufferedImage;
import java.io.InputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@SuppressWarnings({"unused", "Duplicates"})
public class WorkflowController {

    private static final Logger logger = Logger.getLogger(WorkflowController.class);

    private static final String OrgTreeForSecretary = "0P1P1001P10";

    private static final int OrgTreeForSecretaryLength = OrgTreeForSecretary.length();

    private static final int wingOrgIndex = 4;

    private static final String getFilesDropZoneByOrgId = "select rec.filesDropzone as filesDropzone from employee_offices ofc inner join employee_records rec on ofc.employee_record_id=rec.id where ofc.office_unit_organogram_id = %d";

    private static final String getDoctors = "SELECT office_unit_organogram_id, %s FROM employee_offices JOIN " +
            " employee_records ON employee_records.id = employee_offices.employee_record_id" +
            " WHERE organogram_role = %d and employee_records.isDeleted = 0 and employee_offices.isDeleted = 0";

    private static final String getFamilyMemberByOrganogramId = "SELECT name_eng,name_bng,date_of_birth,personal_mobile,gender_cat FROM employee_records " +
            " WHERE id = (SELECT min(employee_record_id) FROM employee_offices WHERE office_unit_organogram_id = %d AND status = 1 and isDeleted = 0)";

    private static final String getFamilyMemberByEmployeeRecordsId = "SELECT name_eng,name_bng,date_of_birth,personal_mobile,gender_cat FROM employee_records " +
            " WHERE id = %d and isDeleted = 0 and (employment_status = " + EmploymentStatusEnum.ACTIVE.getValue() 
            + " or employment_status = " + EmploymentStatusEnum.RELEASED.getValue() 
            + " or employment_status = " + EmploymentStatusEnum.LPR.getValue() 
            + ") and status = 1";
    
    private static final String getByRefOriginUnitOrgIdAndOfficeId = "SELECT id FROM office_unit_organograms where ref_origin_unit_org_id = %d and office_id = %d";

    private static final String getUserIdByTableNameAndPreviousRowId = "SELECT user_id FROM approval_execution_table WHERE table_name = ? " +
            " AND previous_row_id = ? AND action = ? HAVING MAX(id)";

    private static final String getWorkflowDetailsDTO = "SELECT  approval_path.id AS approval_path_id, approval_path.name_en AS approval_path_name, approval_path_order," +
            "    aet.lastModificationTime AS completionDate, action, approval_status_cat, apd.organogram_id, aet.user_id" +
            " FROM  approval_execution_table aet LEFT JOIN approval_path ON (aet.approval_path_id = approval_path.id)" +
            " LEFT JOIN approval_path_details apd ON (approval_path.id = apd.approval_path_id AND aet.approval_path_order = apd.approval_order)" +
            " WHERE table_name = ? AND previous_row_id = (SELECT  previous_row_id FROM approval_execution_table" +
            " WHERE updated_row_id =? AND table_name = ?)";

    private static final String getOrganogramIDFromUserID = "SELECT office_unit_organogram_id FROM employee_offices WHERE employee_record_id = " +
            " (SELECT employee_record_id FROM users WHERE id = %d) and status = 1";

    private static final String getOrganogramIDFromApprovalPath = "SELECT organogram_id FROM approval_path_details where approval_path_id = %d and approval_order = %d";

    private static final String getOrganogramIdFromEmployeeRecordsId = "SELECT office_unit_organogram_id FROM employee_offices WHERE employee_record_id = %d  AND status = 1";

    private static final String getEmployee_officesDTOByEmployeeRecordsId = "SELECT  employee_records.id AS employeeRecordId, employee_records.name_eng AS employeeName,"
            + " office_units.id AS officeUnitId,office_units.unit_name_eng AS officeUnitName, employee_offices.office_unit_organogram_id AS officeUnitOrganogramId,"
            + " employee_offices.designation AS designation FROM employee_records"
            + " INNER JOIN employee_offices ON ( employee_records.id = employee_offices.employee_record_id AND employee_offices.status = 1 )"
            + " INNER JOIN office_units ON employee_offices.office_unit_id = office_units.id WHERE employee_records.id = %d";

    private static final String getNameFromEmployeeRecordID = "SELECT name_eng FROM employee_records where id = %d";

    private static final String getNameFromEmployeeRecordIDAndLanguage = "SELECT %s FROM employee_records where id = %d";

    private static final String getEmployeeRecordIDFromOrganogramID = "SELECT employee_record_id FROM employee_offices WHERE office_unit_organogram_id = %d AND status = 1 and isDeleted = 0";

    private static final String getUserIdFromEmployeeRecordsId = "SELECT id FROM users WHERE employee_record_id = %d AND status = 1";

    private static final String getUserIDFromOrganogramId = "SELECT  users.id FROM users WHERE employee_record_id = (SELECT employee_record_id FROM employee_offices WHERE" +
            " office_unit_organogram_id = %d AND status = 1)";

    private static final String getEmployeeJoiningDateByRecordsId = "SELECT joining_date FROM employee_offices WHERE employee_record_id = %d";

    private static final String getUserNameFromOrganogramId = "SELECT  username FROM users WHERE employee_record_id = (SELECT  employee_record_id FROM employee_offices" +
            " WHERE office_unit_organogram_id = %d AND status = 1 limit 1)";

    private static final String getUserNameAndDetailsFromOrganogramId = "SELECT  emp_rec.%s, u.%s, dept FROM  employee_offices emp_ofc" +
            " INNER JOIN employee_records emp_rec on emp_ofc.employee_record_id = emp_rec.id" +
            " INNER JOIN users u on u.employee_record_id=emp_rec.id" +
            " WHERE emp_ofc.office_unit_organogram_id = %d and emp_ofc.status = 1";

    private static final String getNextApprovalOrderWithApprovalID = "select min(approval_order) from approval_path_details where " +
            " approval_path_id = %d approval_order >  %d";

    private static final String getMaxApprovalOrderWithApprovalID = "select max(approval_order) from approval_path_details where approval_path_id = %d";

    private static final String getOfficeUnitName = "select %s from office_units where id = %d";

    private static final String getMinApprovalOrderWithApprovalID = "select min(approval_order) from approval_path_details where approval_path_id = %d";

    private static final String deleteJobCat = "delete from category where domain_name = 'Job' and value = %d";

    private static final String getNameFromUserId = "SELECT %s FROM employee_records where id = (select employee_record_id from users where id = %d)";

    private static final String getUnitNameFromOrganogramId = "select %s from office_units where id = (select office_unit_id from office_unit_organograms where id = %d )";

    private static final String getEmployeeRecordsIdFromUserId = "SELECT * FROM users where id = %d";

    private static final String getEmailFromOrganogramId = "SELECT personal_email FROM employee_records WHERE id = (SELECT  min(employee_record_id)" +
            "FROM employee_offices WHERE office_unit_organogram_id = %d AND status = 1)";
    private static final String getAddressFromOrganogramId = "SELECT present_address FROM employee_records WHERE id = (SELECT  min(employee_record_id)" +
            "FROM employee_offices WHERE office_unit_organogram_id = %d AND status = 1)";


    private static final String getPhoneNumberFromOrganogramId = "SELECT personal_mobile FROM employee_records WHERE id = (SELECT  min(employee_record_id) " +
            "FROM employee_offices WHERE office_unit_organogram_id = %d AND status = 1)";

    private static final String getNameFromOrganogramId = "SELECT %s FROM" + " employee_records WHERE id = (SELECT  min(employee_record_id)" +
            " FROM employee_offices WHERE office_unit_organogram_id = %d AND status = 1)";

    private static final String getEmployeeDesignationFromEmployeeRecordsId = "SELECT employee_records.id AS employeeRecordId, employee_offices.designation AS designation FROM employee_records " +
            " INNER JOIN employee_offices  ON ( employee_records.id = employee_offices.employee_record_id) WHERE employee_records.id = %d";

    public static long getUnitOrganogramIDByUnitOriginOrganogramID(long UnitOriginOrganogramID) {
        return getUnitOrganogramIDByUnitOriginOrganogramID(UnitOriginOrganogramID, SessionConstants.OFFICE_ID);
    }
    
    
    public static Set<Long> getOfficeIdsFromOfficeUnitIds(HttpServletRequest request) {
        Long[] officeUnitIds = new Gson().fromJson(request.getParameter("officeUnitIds"), Long[].class);

        boolean onlySelectedOffice = Boolean.parseBoolean(request.getParameter("onlySelectedOffice"));

        if (onlySelectedOffice) return new HashSet<>(Arrays.asList(officeUnitIds));

        return Arrays.stream(officeUnitIds)
                .flatMap(officeUnitId -> Office_unitsRepository.getInstance().getAllChildOfficeDTOWithParent(officeUnitId)
                        .stream())
                .map(e -> e.iD)
                .collect(Collectors.toSet());
    }
    
    public static Set<Long> getOfficeIdsFromOfficeUnitIdsParsed(HttpServletRequest request) {
        List<Long> officeUnitIds = null;
        String officeUnitIdsReq = request.getParameter("officeUnitIds");
        if(officeUnitIdsReq != null && !officeUnitIdsReq.equalsIgnoreCase(""))
        {
        	String[] ids = officeUnitIdsReq.split(",");
        	officeUnitIds = new ArrayList<Long>();
        	for(String officeUnitId: ids)
        	{
        		officeUnitIds.add(Long.parseLong(officeUnitId));
        	}
        }
        if(officeUnitIds == null)
        {
        	return null;
        }
       

        boolean onlySelectedOffice = Boolean.parseBoolean(request.getParameter("onlySelectedOffice"));

        if (onlySelectedOffice) return new HashSet<>(officeUnitIds);
        
        HashSet<Long> idSet = new HashSet<>();
        for(long id: officeUnitIds)
        {
        	List<Office_unitsDTO> oids = Office_unitsRepository.getInstance().getAllChildOfficeDTOWithParent(id);
        	if(oids != null)
        	{
        		for(Office_unitsDTO oid: oids)
        		{
        			idSet.add(oid.iD);
        		}
        	}
        	
        }
        return idSet;
      
    }
    
    public static long getUserIdFromOrganogramId(long organogramId)
    {
    	UserDTO userDTO = UserRepository.getUserDTOByOrganogramID(organogramId);
    	if(userDTO == null)
    	{
    		return -1;
    	}
    	return userDTO.ID;
    }

    private static final UserRepository userRepository = UserRepository.getInstance();

    public static long getFileDropzoneIdFromOrganogramId(Long orgId) {
    	if(orgId < 0)
    	{
    		return -1;
    	}
        String sql = String.format(getFilesDropZoneByOrgId, orgId);
        return ConnectionAndStatementUtil.getT(sql, rs -> {
            try {
                return rs.getLong("filesDropzone");
            } catch (SQLException ex) {
                ex.printStackTrace();
                return -1L;
            }
        }, -1L);
    }
    
    public static long getEmployeeRecordIDFromOrganogramIdDBOnly(Long orgId) {
    	
        String sql = String.format(getEmployeeRecordIDFromOrganogramID, orgId);
        return ConnectionAndStatementUtil.getT(sql, rs -> {
            try {
                return rs.getLong("employee_record_id");
            } catch (SQLException ex) {
                ex.printStackTrace();
                return -1L;
            }
        }, -1L);
    }

    public static UserDAO userDAO = new UserDAO();
    
    public static byte[] getBase64SignatureFromUserName(String userName)
    {
    	UserDTO userDTO = UserRepository.getUserDTOByUserName(userName);
    	if(userDTO == null)
    	{
    		return null;
    	}
    	else
    	{
    		Employee_recordsDTO employeeRecordsDTO = Employee_recordsRepository.getInstance().getById(userDTO.employee_record_id);
    		if(employeeRecordsDTO != null)
    		{
    			byte[] encodeBase64Photo = org.apache.commons.codec.binary.Base64.encodeBase64(employeeRecordsDTO.signature);
    	    	return encodeBase64Photo;
    		}
    	}
    	return null;
    	
    }

    public static long getRoleOrganogramIdFromOrgId(long organogramId, int roleId) {
    	if(organogramId < 0)
    	{
    		return -1;
    	}
        long userId;
        try {
            userId = getUserIDFromOrganogramId(organogramId);
            List<UserDTO> otherUserDTOs = userDAO.getOtherUserDTOsByUserId(userId, organogramId);
            if(otherUserDTOs != null)
            {
            	 for (UserDTO userDTO : otherUserDTOs) {
                     if (userDTO.roleID == roleId) {
                         return userDTO.organogramID;
                     }
                 }
            }
           
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return -1;
    }
    
    public static String getUserNameFromUserId(long id) {
    	if(id < 0)
    	{
    		return "";
    	}
        UserDTO userDTO = UserRepository.getUserDTOByUserID(id);
        return userDTO != null ? userDTO.userName : "";
    }

    public static BufferedImage getSignatureFromOrganogramId(Long orgId) throws Exception {
    	if(orgId < 0)
    	{
    		return null;
    	}
        try {
            long filesDropzone = getFileDropzoneIdFromOrganogramId(orgId);
            List<FilesDTO> filesDTOList = new FilesDAO().getDTOsByFileID(filesDropzone);
            InputStream data;
            if (filesDTOList != null && filesDTOList.size() > 0)
                data = filesDTOList.get(0).inputStream;
            else
                return null;
            return ImageIO.read(data);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String getSignatureTypeFromOrganogramId(Long orgId) throws Exception {
    	if(orgId < 0)
    	{
    		return "";
    	}
        long filesDropzone = getFileDropzoneIdFromOrganogramId(orgId);
        List<FilesDTO> filesDTOList = new FilesDAO().getDTOsByFileID(filesDropzone);
        String type = null;
        if (filesDTOList != null && filesDTOList.size() > 0) {
            type = filesDTOList.get(0).fileTypes;
        }
        return type;
    }

    private static UserDTO buildDTOForDoctor(ResultSet rs) {
        try {
            UserDTO userDTO = new UserDTO();
            userDTO.organogramID = rs.getLong(1);
            userDTO.fullName = rs.getString(2);
            return userDTO;
        } catch (SQLException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public static List<UserDTO> getDoctors(String language) {
        String nameCol = language.equalsIgnoreCase("english") ? "employee_records.name_eng" : "employee_records.name_bng";
        String doctorSql = String.format(getDoctors, nameCol, SessionConstants.DOCTOR_ROLE);
        return ConnectionAndStatementUtil.getListOfT(doctorSql, WorkflowController::buildDTOForDoctor);
    }


    public static FamilyMemberDTO getFamilyMemberDTOFromOrganogramId(long organogramID) throws Exception {
    	if(organogramID < 0)
    	{
    		return null;
    	}
        String memberSQL = String.format(getFamilyMemberByOrganogramId, organogramID);
        return ConnectionAndStatementUtil.getT(memberSQL, rs -> {
            try {
                FamilyMemberDTO familyMemberDTO = new FamilyMemberDTO();
                familyMemberDTO.nameEn = rs.getString("name_eng");
                familyMemberDTO.nameBn = rs.getString("name_bng");
                familyMemberDTO.dateOfBirth = rs.getLong("date_of_birth");
                familyMemberDTO.genderCat = rs.getInt("gender_cat");
                familyMemberDTO.mobile = rs.getString("personal_mobile");
                return familyMemberDTO;
            } catch (SQLException ex) {
                logger.error(ex);
                ex.printStackTrace();
                return null;
            }
        });
    }

    public static FamilyMemberDTO getFamilyMemberDTOFromEmployeeRecordId(long erId) throws Exception {
    	if(erId < 0)
    	{
    		return null;
    	}
        String memberSQL = String.format(getFamilyMemberByEmployeeRecordsId, erId);
        return ConnectionAndStatementUtil.getT(memberSQL, rs -> {
            try {
                FamilyMemberDTO familyMemberDTO = new FamilyMemberDTO();
                familyMemberDTO.nameEn = rs.getString("name_eng");
                familyMemberDTO.nameBn = rs.getString("name_bng");
                familyMemberDTO.dateOfBirth = rs.getLong("date_of_birth");
                familyMemberDTO.genderCat = rs.getInt("gender_cat");
                familyMemberDTO.mobile = rs.getString("personal_mobile");
                return familyMemberDTO;
            } catch (SQLException ex) {
                logger.error(ex);
                ex.printStackTrace();
                return null;
            }
        });
    }

    public static long getUnitOrganogramIDByUnitOriginOrganogramID(long UnitOriginOrganogramID, long OfficeID) {
        String sql = String.format(getByRefOriginUnitOrgIdAndOfficeId, UnitOriginOrganogramID, OfficeID);
        Long result = ConnectionAndStatementUtil.getT(sql, rs -> {
            try {
                return rs.getLong("id");
            } catch (SQLException ex) {
                ex.printStackTrace();
                logger.error(ex);
                return null;
            }
        });
        return result == null ? -1 : result;
    }

    public static boolean isInitiator(String tableName, long previousRowID, long userOrganOgramID) {
        try {
            long userID = getInitiatorBypreviousRowID(tableName, previousRowID);
            long organogramID = getOrganogramIDFromUserID(userID);
            return (organogramID == userOrganOgramID);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static long getInitiatorBypreviousRowID(String tableName, long previousRowID) throws Exception {
        return ConnectionAndStatementUtil.getT(getUserIdByTableNameAndPreviousRowId, Arrays.asList(tableName, previousRowID, SessionConstants.INITIATE), rs -> {
            try {
                return rs.getLong("user_id");
            } catch (SQLException ex) {
                ex.printStackTrace();
                return -1L;
            }
        });
    }

    public static List<WorkflowDetailsDTO> getWorkflowDetails(String tableName, long updatedRowID) throws Exception {
        return ConnectionAndStatementUtil.getListOfT(getWorkflowDetailsDTO, Arrays.asList(tableName, updatedRowID, tableName), WorkflowController::buildWorkflowDetailsDTO);
    }

    private static WorkflowDetailsDTO buildWorkflowDetailsDTO(ResultSet rs) {
        try {
            WorkflowDetailsDTO workflowDetailsDTO = new WorkflowDetailsDTO();
            workflowDetailsDTO.approvalPathID = rs.getLong("approval_path_id");
            workflowDetailsDTO.approvalPathName = rs.getString("approval_path_name");
            workflowDetailsDTO.approvalPathOrder = rs.getInt("approval_path_order");
            workflowDetailsDTO.date = rs.getLong("completionDate");
            workflowDetailsDTO.action = rs.getInt("action");
            workflowDetailsDTO.actionStr = SessionConstants.operation_names[workflowDetailsDTO.action];
            workflowDetailsDTO.status = rs.getInt("approval_status_cat");
            workflowDetailsDTO.statusStr = CatDAO.getName("English", "approval_status", workflowDetailsDTO.status);
            workflowDetailsDTO.organogramID = rs.getLong("apd.organogram_id");
            workflowDetailsDTO.userID = rs.getLong("aet.user_id");
            workflowDetailsDTO.employeeName = getNameFromOrganogramId(workflowDetailsDTO.organogramID, "English");
            workflowDetailsDTO.employeeDesignation = CommonDAO.getName((int) workflowDetailsDTO.organogramID, "office_unit_organograms", "English", "id");
            return workflowDetailsDTO;
        } catch (Exception ex) {
            logger.error(ex);
            ex.printStackTrace();
            return null;
        }
    }

    public static long getOrganogramIDFromUserID(long userID) throws Exception {
    	if(userID < 0)
    	{
    		return -1;
    	}
        UserDTO userDTO = UserRepository.getUserDTOByUserID(userID);
        if (userDTO == null) {
            return -1;
        }
        return getOrganogramIDFromErID(userDTO.employee_record_id);
    }
    
    public static long getOrganogramIDFromErID(long erId)  {   
        EmployeeOfficeDTO employeeOfficeDTO = EmployeeOfficeRepository.getInstance().getByEmployeeRecordIdIsDefault(erId);
        return employeeOfficeDTO == null ? -1 : employeeOfficeDTO.officeUnitOrganogramId;
    }
    
    public static long getOrganogramIDFromUserName(String userName) {
    	if(userName == null)
    	{
    		return -1;
    	}
        UserDTO userDTO = UserRepository.getUserDTOByUserName(userName);
        if (userDTO == null) {
            return -1;
        }
        EmployeeOfficeDTO employeeOfficeDTO = EmployeeOfficeRepository.getInstance().getByEmployeeRecordIdIsDefault(userDTO.employee_record_id);
        return employeeOfficeDTO == null ? -1 : employeeOfficeDTO.officeUnitOrganogramId;
    }

    public static long getOrganogramIDFromApprovalPath(long approvalPathID, int approvalOrder) {
        String sql = String.format(getOrganogramIDFromApprovalPath, approvalPathID, approvalOrder);
        Long organogramID = ConnectionAndStatementUtil.getT(sql, rs -> {
            try {
                return rs.getLong("organogram_id");
            } catch (SQLException ex) {
                ex.printStackTrace();
                return null;
            }
        });
        return organogramID == null ? -1 : organogramID;
    }

    public static long getUserIdFromEmployeeRecordsId(long employeeRecordsId) throws Exception {
    	if(employeeRecordsId < 0)
    	{
    		return -1;
    	}
        UserDTO userDTO = UserRepository.getInstance().getUserDtoByEmployeeRecordId(employeeRecordsId);
        return userDTO == null || !userDTO.active ? -1 : userDTO.ID;
    }

    public static long getUserIDFromOrganogramId(long organogramID) throws Exception {
    	if(organogramID < 0)
		{
			return -1;
		}
        UserDTO userDTO = UserRepository.getUserDTOByOrganogramID(organogramID);
        return userDTO == null ? -1 : userDTO.ID;
    }

    public static String getEmployeeJoiningDateByRecordsId(long employeeRecordsId) throws Exception {
    	if(employeeRecordsId < 0)
    	{
    		return "";
    	}
        String sql = String.format(getEmployeeJoiningDateByRecordsId, employeeRecordsId);
        Long joiningDate = ConnectionAndStatementUtil.getT(sql, rs -> {
            try {
                return rs.getLong("joining_date");
            } catch (SQLException ex) {
                ex.printStackTrace();
                return SessionConstants.MIN_DATE;
            }
        });
        if (Objects.isNull(joiningDate) || joiningDate == SessionConstants.MIN_DATE) {
            return "";
        }
        return new SimpleDateFormat("dd-MM-yyyy").format(new Date(joiningDate));
    }
    
    public static String getNameFromUserName(String userName, boolean isLangEng)
    {
    	return getNameFromUserName(userName, isLangEng?"english":"bangla");
    }
    
    public static String getNameFromUserName(String userName, String language)
    {
    	String name = Employee_recordsRepository.getInstance().getByUserName(userName, language); 
    	if(name == null || name.equalsIgnoreCase(""))
    	{
    		UserDTO userDTO = UserRepository.getUserDTOByUserNameFromUsersTable(userName);
    		if(userDTO != null)
    		{
    			try {
					name = getNameFromEmployeeRecordID(userDTO.employee_record_id, language);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
    		}
    	}
    	return name;
    }

    public static String getUserNameFromOrganogramId(long organogramID)
    {
    	long employeeRecordId = getEmployeeRecordIDFromOrganogramID(organogramID);
        return getUserNameFromErId(employeeRecordId, true);
    }
    
    public static String getUserNameFromOrganogramId(long organogramID, boolean isLangEng)
    {
    	long employeeRecordId = getEmployeeRecordIDFromOrganogramID(organogramID);
        return getUserNameFromErId(employeeRecordId, isLangEng);
    }

    public static UserNameAndDetails getUserNameAndDetailsFromOrganogramId(long organogramID, String language) throws Exception {
    	if(organogramID < 0)
		{
    		UserNameAndDetails userNameAndDetails = new UserNameAndDetails();
    		return userNameAndDetails;
		}
        String nameColumn = language.equalsIgnoreCase("english") ? "name_eng" : "name_bng";
        String aliasColumn = "user_alias";
        String sql = String.format(getUserNameAndDetailsFromOrganogramId, nameColumn, aliasColumn, organogramID);
        return ConnectionAndStatementUtil.getT(sql, rs -> {
            try {
                UserNameAndDetails userNameAndDetails = new UserNameAndDetails();
                userNameAndDetails.name = rs.getString(nameColumn);
                userNameAndDetails.alias = rs.getString(aliasColumn);
                userNameAndDetails.dept = rs.getInt("dept");
                return userNameAndDetails;
            } catch (SQLException ex) {
                ex.printStackTrace();
                return null;
            }
        });
    }

    public static String getOfficeTypeNameFromOrganogramId(long organogramId, String language) {
    	if(organogramId < 0)
		{
			return "";
		}
        String officeTypeName = "";
        Office_unitsDTO officeTypeDTO = OfficeUnitOrganogramsRepository.getInstance().getOfficeType(organogramId);
        if (officeTypeDTO != null) {
            officeTypeName = language.equalsIgnoreCase("english") ? officeTypeDTO.unitNameEng : officeTypeDTO.unitNameBng;
        }
        return officeTypeName;
    }

    public static String getNameFromOrganogramId(long organogramID, String language) {
    	
        return getNameFromOrganogramId(organogramID, "English".equalsIgnoreCase(language));
    }

    public static String getNameFromOrganogramId(long organogramID, boolean isLangEng) {
        if (organogramID <= 0) {
            return "";
        }
        else if (organogramID >= SessionConstants.ROOM_OFFSET && organogramID < SessionConstants.OFFICE_OFFSET)
    	{
    		long roomNo = organogramID - SessionConstants.ROOM_OFFSET;
    		return CatDAO.getName(isLangEng?"english":"bangla", "room_no", roomNo);
    	}
        else if(organogramID >= SessionConstants.OFFICE_OFFSET)
        {
        	long unitId = organogramID - SessionConstants.OFFICE_OFFSET;
        	Office_unitsDTO office_unitsDTO = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(unitId);
        	if(office_unitsDTO == null)
        	{
        		if(isLangEng)
    			{
    				return "No office found";
    			}
    			else
    			{
    				return "অফিস পাওয়া যায় নি";
    			}
        	}
            return (isLangEng ? office_unitsDTO.unitNameEng : office_unitsDTO.unitNameBng);
        }
        UserDTO userDTO = UserRepository.getUserDTOByOrganogramID(organogramID);
        if (userDTO == null) {
            return "";
        }
        Employee_recordsDTO employeeRecordsDTO = Employee_recordsRepository.getInstance().getById(userDTO.employee_record_id);
        return employeeRecordsDTO == null ? "" :
                (isLangEng ? employeeRecordsDTO.nameEng : employeeRecordsDTO.nameBng);
    }
    
    public static String getNameFromOrganogramIdLight(long organogramID, boolean isLangEng) {

        long employeeRecordId = getEmployeeRecordIDFromOrganogramID(organogramID);
        return getNameFromEmployeeRecordID(employeeRecordId, isLangEng);
    }
    


    public static String getOfficeNameFromOrganogramId(long organogramID, String language) {
        boolean isLangEng = language.equalsIgnoreCase("english");
        return getOfficeNameFromOrganogramId(organogramID,  isLangEng);
    }
    
    public static String getOfficeNameFromOrganogramId(long organogramID, boolean isLangEng) {
        //logger.debug("getOfficeNameFromOrganogramId, organogramID = " + organogramID);
        if (organogramID < 0) {
            return "";
        }
        EmployeeOfficeDTO employeeOfficeDTO = EmployeeOfficeRepository.getInstance().getByOfficeUnitOrganogramIdRepoOnly(organogramID);

        if (employeeOfficeDTO == null) {
            logger.debug("employeeOfficeDTO is null ");
            return "";
        }
        String name = isLangEng ? employeeOfficeDTO.unitNameEn : employeeOfficeDTO.unitNameBn;
        //logger.debug("unit name = " + name);
        return name;
    }
    

    public static long getOfficeIdFromOrganogramId(long organogramID) {
        if (organogramID < 0) {
            return -1;
        }
        EmployeeOfficeDTO employeeOfficeDTO = EmployeeOfficeRepository.getInstance().getByOfficeUnitOrganogramId(organogramID);

        if (employeeOfficeDTO == null) {
            return -1;
        }
        return employeeOfficeDTO.officeUnitId;
    }

    public static WingModel getWingModelFromOrganogramId(long organogramID) {
        logger.debug("getWingModelFromOrganogramId for organogramID : "+organogramID);
        WingModel wingModel = new WingModel();
        if (organogramID < 0) {
            return wingModel;
        }
        OfficeUnitOrganograms organograms = OfficeUnitOrganogramsRepository.getInstance().getById(organogramID);
        if(organograms == null){
            return wingModel;
        }
             
        return getWingFromOfficeUnitId(organograms.office_unit_id);
    }
    
    public static WingModel getWingFromOfficeUnitId(long officeUnitId)
    {
        WingModel wingModel = new WingModel();
        Office_unitsDTO officeUnitsDTO = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(officeUnitId);
        if (officeUnitsDTO == null) {
            return wingModel;
        }
        if (officeUnitsDTO.parentUnitId == SessionConstants.MP_OFFICE) {
            wingModel.id = 9;
            wingModel.nameEn = "MP Wing";
            wingModel.nameBn = "এম পি শাখা";
            return wingModel;
        }
        Office_unitsDTO wingOfOfficeUnitId = Office_unitsRepository.getInstance().findClosestOfficeUnitOfAnOfficeType(officeUnitId, OfficeUnitTypeEnum.WINGS);
        if (wingOfOfficeUnitId != null) {
            wingModel.id = wingOfOfficeUnitId.iD;
            wingModel.nameEn = wingOfOfficeUnitId.unitNameEng;
            wingModel.nameBn = wingOfOfficeUnitId.unitNameBng;
        }
        return wingModel;
    }
    
    public static String getWingNameFromOfficeUnitId(long officeUnitId, boolean isLangEgg)
    {
    	WingModel wingModel = getWingFromOfficeUnitId(officeUnitId);
    	return isLangEgg? wingModel.nameEn: wingModel.nameBn;
    }
    
   

    public static long getWingIdFromOrganogramId(long organogramID) {
    	if(organogramID < 0)
		{
			return -1;
		}
        return getWingModelFromOrganogramId(organogramID).id;
    }
    
    public static String getOrganizationNameFromOrganogramId(long organogramID, boolean isLangEnglish) {
    	if(organogramID < 0)
		{
    		if(isLangEnglish)
    		{
    			return "Other";
    		}
    		else
    		{
    			return "অন্যান্য";
    		}
			
		}
    	Organization organization = OfficeUnitOrganogramsRepository.getInstance().getOrganizationByOrganogramId(organogramID);
    	if(organization == null)
    	{
    		if(isLangEnglish)
    		{
    			return "Other";
    		}
    		else
    		{
    			return "অন্যান্য";
    		}
    	}
    	else
    	{
    		if(isLangEnglish)
    		{
    			return organization.getNameEn();
    		}
    		else
    		{
    			return organization.getNameBn();
    		}
    	}
        
    }
    
    public static long getOrganizationIdFromOrganogramId(long organogramID) {
    	if(organogramID <= 0)
		{
    		return Organization.OTHER.getId();			
		}
    	Organization organization = OfficeUnitOrganogramsRepository.getInstance().getOrganizationByOrganogramId(organogramID);
    	if(organization == null)
    	{
    		return Organization.OTHER.getId();
    	}
    	else
    	{
    		return organization.getId();
    	}
        
    }
    
  

    public static String getWingNameFromOrganogramId(long organogramID, String language) {
    	if(organogramID < 0)
		{
			return "";
		}
        return getWingNameFromOrganogramId(organogramID,language.equalsIgnoreCase("english"));
    }

    public static String getWingNameFromOrganogramId(long organogramID, boolean isLangEng) {
    	if(organogramID < 0)
		{
			return "";
		}
        WingModel wingModel = getWingModelFromOrganogramId(organogramID);
        return isLangEng?wingModel.nameEn:wingModel.nameBn;
    }
    
    public static String getPhoneNumberFromErId(long erId) throws Exception {
    	
        Employee_recordsDTO employeeRecordsDTO = Employee_recordsRepository.getInstance().getById(erId);
        if (employeeRecordsDTO == null) {
            return "";
        }
        return employeeRecordsDTO.personalMobile;
    }
    

    public static String getPhoneNumberFromOrganogramId(long organogramID) throws Exception {
    	if(organogramID < 0)
		{
			return "";
		}
        EmployeeOfficeDTO employeeOfficeDTO = EmployeeOfficeRepository.getInstance().getByOfficeUnitOrganogramId(organogramID);
        if (employeeOfficeDTO == null) {
            return "";
        }
        Employee_recordsDTO employeeRecordsDTO = Employee_recordsRepository.getInstance().getById(employeeOfficeDTO.employeeRecordId);
        if (employeeRecordsDTO == null) {
            return "";
        }
        return employeeRecordsDTO.personalMobile;
    }
    public static String getPhoneNumberFromOrganogramId(long organogramID, String language) throws Exception {
    	return Utils.getDigits(getPhoneNumberFromOrganogramId(organogramID), language);
    }

    public static long getEmployeeRecordsIdFromUserId(long userId) throws Exception {
    	if(userId < 0)
		{
			return -1;
		}
        UserDTO userDTO = UserRepository.getUserDTOByUserID(userId);
        if (userDTO == null) {
            return -1;
        }
        Employee_recordsDTO employeeRecordsDTO = Employee_recordsRepository.getInstance().getById(userDTO.employee_record_id);
        return employeeRecordsDTO == null ? 0 : employeeRecordsDTO.iD;
    }
    
    public static long getEmployeeRecordsIdFromUserName(String userName){
    	if(userName == null || userName.equalsIgnoreCase(""))
		{
			return -1;
		}
    	long id = Employee_recordsRepository.getInstance().getIdByUserName(Utils.getDigits(userName, "english"));
    	if(id != -1)
    	{
    		return id;
    	}
        UserDTO userDTO = UserRepository.getUserDTOByUserName(userName);
        if (userDTO == null) {
            return -1;
        }
        Employee_recordsDTO employeeRecordsDTO = Employee_recordsRepository.getInstance().getById(userDTO.employee_record_id);
        return employeeRecordsDTO == null ? -1 : employeeRecordsDTO.iD;
    }
    
    public static String getUserNameFromErId(long erId, String Language)
    {
    	
        Employee_recordsDTO employeeRecordsDTO = Employee_recordsRepository.getInstance().getById(erId);
        if(employeeRecordsDTO != null)
        {
        	return Utils.getDigits(employeeRecordsDTO.employeeNumber, Language);
        }
        return "";
    }
    
    public static String getUserNameFromErId(long erId, boolean isLangEng)
    {
    	
        Employee_recordsDTO employeeRecordsDTO = Employee_recordsRepository.getInstance().getByIdRepoOnly(erId);
        if(employeeRecordsDTO != null)
        {
        	return Utils.getDigits(employeeRecordsDTO.employeeNumber, isLangEng?"english":"bangla");
        }
        return "";
    }
    
    public static String getPhoneFromErId(long erId, String Language)
    {
    	
        Employee_recordsDTO employeeRecordsDTO = Employee_recordsRepository.getInstance().getById(erId);
        if(employeeRecordsDTO != null)
        {
        	return Utils.getDigits(employeeRecordsDTO.personalMobile, Language);
        }
        return "";
    }

    public static String getEmailFromOrganogramId(long organogramID) throws Exception {
    	if(organogramID < 0)
    	{
    		return "";
    	}
        EmployeeOfficeDTO employeeOfficeDTO = EmployeeOfficeRepository.getInstance().getByOfficeUnitOrganogramId(organogramID);
        if (employeeOfficeDTO == null) {
            return "";
        }
        Employee_recordsDTO employeeRecordsDTO = Employee_recordsRepository.getInstance().getById(employeeOfficeDTO.employeeRecordId);
        return employeeRecordsDTO == null ? "" : (employeeRecordsDTO.personalEml == null ? "" : employeeRecordsDTO.personalEml);
    }

    public static String getAddressFromOrganogramId(long organogramID, String language) throws Exception {
    	if(organogramID < 0)
    	{
    		return "";
    	}
        EmployeeOfficeDTO employeeOfficeDTO = EmployeeOfficeRepository.getInstance().getByOfficeUnitOrganogramId(organogramID);
        if (employeeOfficeDTO == null) {
            return "";
        }
        Employee_recordsDTO employeeRecordsDTO = Employee_recordsRepository.getInstance().getById(employeeOfficeDTO.employeeRecordId);
        if(employeeRecordsDTO == null)
        {
        	return "";
        }
        return employeeRecordsDTO.presentAddress == null ? "" : GeoLocationDAO2.getAddressToShow(employeeRecordsDTO.presentAddress, language);
    }


    public static String getUnitNameFromOrganogramId(long organogramID, String language) throws Exception {
    	if(organogramID < 0)
    	{
    		return "";
    	}
        return getUnitNameFromOrganogramId(organogramID,language.equalsIgnoreCase("english"));
    }
    
    public static String getUnitNameFromUserName(String userName, String language) throws Exception {
    	long organogramId = getOrganogramIDFromUserName(userName);
        return getUnitNameFromOrganogramId(organogramId,language.equalsIgnoreCase("english"));
    }

    public static String getUnitNameFromOrganogramId(long organogramID, boolean isLangEng) throws Exception {
    	if(organogramID < 0)
    	{
    		return "";
    	}
        OfficeUnitOrganograms organograms = OfficeUnitOrganogramsRepository.getInstance().getById(organogramID);
        if (organograms == null) {
            return "";
        }
        Office_unitsDTO officeUnitsDTO = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(organograms.office_unit_id);
        return officeUnitsDTO == null ? "" :
                isLangEng ? officeUnitsDTO.unitNameEng : officeUnitsDTO.unitNameBng;
    }

    public static String getNameFromUserId(long userID, String language) throws Exception {
    	if(userID < 0)
    	{
    		return "";
    	}
        return getNameFromUserId(userID, language.equalsIgnoreCase("english"));
    }

    public static String getNameFromUserId(long userID, boolean isLangEng) {
    	if(userID < 0)
    	{
    		return "";
    	}
        UserDTO userDTO = UserRepository.getUserDTOByUserID(userID);
        if (userDTO == null) {
            return "";
        }
        Employee_recordsDTO employeeRecordsDTO = Employee_recordsRepository.getInstance().getById(userDTO.employee_record_id);
        if (employeeRecordsDTO == null) {
            return "";
        }
        return isLangEng ? employeeRecordsDTO.nameEng : employeeRecordsDTO.nameBng;
    }

    public static void deleteJobcat(long value) throws Exception {
        String sql = String.format(deleteJobCat, value);
        ConnectionAndStatementUtil.getWriteStatement(st -> {
            try {
                st.executeUpdate(sql);
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        });
    }

    public static long getApprovalPathIDFromJobCatandOfficeID(int jobCat, UserDTO userDTO) {
        return jobCat;
    }

    public static int getMinApprovalOrderWithApprovalID(long approvalID) throws Exception {
        String sql = String.format(getMinApprovalOrderWithApprovalID, approvalID);
        Integer order = ConnectionAndStatementUtil.getT(sql, rs -> {
            try {
                return rs.getInt("min(approval_order)");
            } catch (SQLException ex) {
                ex.printStackTrace();
                return null;
            }
        });
        return order == null ? -1 : order;
    }
    
    public static String getOrganogramName(String userName, String language) {
    	long organogramId = getOrganogramIDFromUserName(userName);
    	return getOrganogramName(organogramId, language);
    }

    public static String getOrganogramName(long organogemId, String language) {
    	if(organogemId < 0)
    	{
    		return "";
    	}
        return getOrganogramName(organogemId, language.equalsIgnoreCase("English"));
    }

    public static String getOrganogramName(long organogemId, boolean isLangEng) {
    	if(organogemId < 0)
    	{
    		if(isLangEng)
			{
				return "No Designation found";
			}
			else
			{
				return "পদবী পাওয়া যায় নি";
			}
    	}
        OfficeUnitOrganograms officeUnitOrganograms = OfficeUnitOrganogramsRepository.getInstance().getById(organogemId);
        if (officeUnitOrganograms == null) {
        	if(isLangEng)
			{
				return "No Designation found";
			}
			else
			{
				return "পদবী পাওয়া যায় নি";
			}
        }
        return isLangEng ? officeUnitOrganograms.designation_eng : officeUnitOrganograms.designation_bng;
    }

    public static String getOfficeUnitName(long officeUnitId) {
    	if(officeUnitId < 0)
    	{
    		return "";
    	}
        return getOfficeUnitName(officeUnitId, "English");
    }

    public static String getOfficeUnitName(long officeUnitId, String language) {
    	if(officeUnitId < 0)
    	{
    		return "";
    	}
        Office_unitsDTO officeUnitsDTO = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(officeUnitId);
        if (officeUnitsDTO == null) {
            return "";
        }
        return language.equalsIgnoreCase("English") ? officeUnitsDTO.unitNameEng : officeUnitsDTO.unitNameBng;
    }

    public static int getMaxApprovalOrderWithApprovalID(long approvalID) {
        String sql = String.format(getMaxApprovalOrderWithApprovalID, approvalID);
        Integer order = ConnectionAndStatementUtil.getT(sql, rs -> {
            try {
                return rs.getInt("max(approval_order)");
            } catch (SQLException ex) {
                ex.printStackTrace();
                return null;
            }
        });
        return order == null ? -1 : order;
    }


    public static int getNextApprovalOrderWithApprovalID(long approvalID, long currentOrder) throws Exception {
        String sql = String.format(getNextApprovalOrderWithApprovalID, approvalID, currentOrder);
        Integer order = ConnectionAndStatementUtil.getT(sql, rs -> {
            try {
                return rs.getInt("min(approval_order)");
            } catch (SQLException ex) {
                ex.printStackTrace();
                return null;
            }
        });
        return order == null ? -1 : order;
    }


    public static long getEmployeeRecordIDFromOrganogramID(long organogramID)
    {

    	if(organogramID < 0)
    	{
    		return -1;
    	}
    	EmployeeOfficeDTO employeeOfficeDTO = EmployeeOfficeRepository.getInstance().getByOfficeUnitOrganogramIdRepoOnly(organogramID);
        if(employeeOfficeDTO == null)
        {
        	return -1;
        }
        return employeeOfficeDTO.employeeRecordId;
    }

    public static String getNameFromEmployeeRecordID(long employeeRecordID, String language) throws Exception {
    	if(employeeRecordID < 0)
    	{
    		if(language.equalsIgnoreCase("english"))
			{
				return "N/A" ;
			}
			else
			{
				return "রেকর্ড পাওয়া যায় নি" ;
			}
    	}
        return getNameFromEmployeeRecordID(employeeRecordID, language.equalsIgnoreCase("english"));
    }

    public static String getNameFromEmployeeRecordID(long employeeRecordID, boolean isLangEng)
    {
    	if(employeeRecordID < 0)
    	{
    		return "";
    	}
        Employee_recordsDTO dto = Employee_recordsRepository.getInstance().getById(employeeRecordID);
        return dto == null ? "" : (isLangEng ? dto.nameEng : dto.nameBng);
    }
    
    public static String getOfficeNameFromEmployeeRecordID(long employeeRecordID, boolean isLangEng) {
    	if(employeeRecordID < 0)
    	{
    		if(isLangEng)
			{
				return "N/A" ;
			}
			else
			{
				return "রেকর্ড পাওয়া যায় নি" ;
			}
    	}
    	Employee_recordsDTO erDTO = Employee_recordsRepository.getInstance().getById(employeeRecordID);
    	if(erDTO == null)
    	{
    		if(isLangEng)
			{
				return "N?A" ;
			}
			else
			{
				return "রেকর্ড পাওয়া যায় নি" ;
			}
    	}
    	
    	EmployeeOfficeDTO dto = EmployeeOfficeRepository.getInstance().getByEmployeeRecordIdIsDefaultOrAny(employeeRecordID);
    	if(dto == null)
    	{
    		if(erDTO.currentOffice > 0)
        	{
        		return (isLangEng?"Other Office: ":"অন্যান্য অফিস: ") +  Other_officeRepository.getInstance().getText(isLangEng, erDTO.currentOffice);
        	}
    		if(isLangEng)
			{
				return "No office found";
			}
			else
			{
				return "অফিস পাওয়া যায় নি";
			}
    	}
    	
    	Office_unitsDTO office_unitsDTO = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(dto.officeUnitId);
    	if(office_unitsDTO == null)
    	{
    		if(isLangEng)
			{
				return "No office found";
			}
			else
			{
				return "অফিস পাওয়া যায় নি";
			}
    	}
        return (isLangEng ? office_unitsDTO.unitNameEng : office_unitsDTO.unitNameBng);
        
    }
    
    public static String getOrganogramNameFromEmployeeRecordID(long employeeRecordID, boolean isLangEng) {
    	if(employeeRecordID < 0)
    	{
    		if(isLangEng)
			{
				return "Employee is probably deleted" ;
			}
			else
			{
				return "কর্মকর্তার রেকর্ড পাওয়া যায় নি" ;
			}
    	}
    	
    	Employee_recordsDTO erDTO = Employee_recordsRepository.getInstance().getById(employeeRecordID);
    	if(erDTO == null)
    	{
    		if(isLangEng)
			{
				return "Employee is probably deleted" ;
			}
			else
			{
				return "কর্মকর্তার রেকর্ড পাওয়া যায় নি" ;
			}
    	}
    	
    	EmployeeOfficeDTO dto = EmployeeOfficeRepository.getInstance().getByEmployeeRecordIdIsDefaultOrAny(employeeRecordID);
    	if(dto == null)
    	{
    		if(erDTO.currentOffice > 0)
        	{
    			if(isLangEng)
    			{
    				return "Other Office designation: " + erDTO.currentDesignation;
    			}
    			else
    			{
    				return "অন্যান্য অফিসের পদবী : " + erDTO.currentDesignation;
    			}
        		
        	}
    	}
    	if(dto == null)
    	{
    		if(isLangEng)
			{
				return "No office found";
			}
			else
			{
				return "অফিস পাওয়া যায় নি";
			}
    	}
        return getOrganogramName(dto.officeUnitOrganogramId, isLangEng);
    }

    public static String getNameFromEmployeeRecordID(long employeeRecordID) throws Exception {
    	if(employeeRecordID < 0)
    	{
    		return "";
    	}
        return getNameFromEmployeeRecordID(employeeRecordID, true);
    }

    public static List<EmployeeDesignationModel> getEmployeeDesignationFromEmployeeRecordsId(List<Long> idList) {
        if (idList == null || idList.size() == 0) {
            return new ArrayList<>();
        }
        String sqlQuery = "select employee_record_id,designation from employee_offices where employee_record_id in (SELECT id from employee_records where id in (%s))";
        return ConnectionAndStatementUtil.getDTOListByNumbers(sqlQuery, idList, WorkflowController::buildEmployeeDesignationModel);
    }

    private static EmployeeDesignationModel buildEmployeeDesignationModel(ResultSet rs) {
        try {
            EmployeeDesignationModel model = new EmployeeDesignationModel();
            model.empRecordId = rs.getLong("employee_record_id");
            model.designation = rs.getString("designation");
            return model;
        } catch (SQLException ex) {
            logger.error(ex);
            ex.printStackTrace();
            return null;
        }
    }

    public static List<EmployeeOrganogramModel> getEmployeeOrganogramFromEmployeeRecordsId(List<Long> idList) {
        if (idList == null || idList.size() == 0) {
            return new ArrayList<>();
        }
        String sqlQuery = "select employee_record_id,office_unit_organogram_id from employee_offices where employee_record_id in (SELECT id from employee_records where id in (%s))";
        return ConnectionAndStatementUtil.getDTOListByNumbers(sqlQuery, idList, WorkflowController::buildEmployeeOrganogramModel);
    }

    private static EmployeeOrganogramModel buildEmployeeOrganogramModel(ResultSet rs) {
        try {
            EmployeeOrganogramModel model = new EmployeeOrganogramModel();
            model.empRecordId = rs.getLong("employee_record_id");
            model.organogramId = rs.getLong("office_unit_organogram_id");
            return model;
        } catch (SQLException ex) {
            logger.error(ex);
            ex.printStackTrace();
            return null;
        }
    }

    public static Employee_officesDTO getEmployee_officesDTOByEmployeeRecordsId(long employeeRecordsId) throws Exception {
        String sql = String.format(getEmployee_officesDTOByEmployeeRecordsId, employeeRecordsId);
        return ConnectionAndStatementUtil.getT(sql, rs -> {
            try {
                Employee_officesDTO employee_officesDTO = new Employee_officesDTO();
                employee_officesDTO.employeeRecordId = rs.getLong("employeeRecordId");
                employee_officesDTO.employeeName = rs.getString("employeeName");
                employee_officesDTO.officeUnitId = rs.getLong("officeUnitId");
                employee_officesDTO.officeUnitName = rs.getString("officeUnitName");
                employee_officesDTO.officeUnitOrganogramId = rs.getLong("officeUnitOrganogramId");
                employee_officesDTO.designation = rs.getString("designation");
                return employee_officesDTO;
            } catch (SQLException ex) {
                ex.printStackTrace();
                return null;
            }
        });
    }

    public static OrganogramDetails getOrganogramDetailsByOrganogramId(long id,boolean isLangEng){
        logger.debug("getOrganogramDetailsByOrganogramId for id : "+id);
        OrganogramDetails organogramDetails = new OrganogramDetails();
        organogramDetails.organogramId = id;
        if(id<=0){
            return organogramDetails;
        }
        OfficeUnitOrganograms officeUnitOrganograms = OfficeUnitOrganogramsRepository.getInstance().getById(id);
        if(officeUnitOrganograms == null){
            return organogramDetails;
        }
        organogramDetails.organogramName = isLangEng? officeUnitOrganograms.designation_eng :officeUnitOrganograms.designation_bng;
        Office_unitsDTO officeUnitsDTO = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(officeUnitOrganograms.office_unit_id);
        if(officeUnitsDTO == null){
            return organogramDetails;
        }
        organogramDetails.officeUnitId = officeUnitsDTO.iD;
        organogramDetails.officeName = isLangEng? officeUnitsDTO.unitNameEng : officeUnitsDTO.unitNameBng;
        organogramDetails.wingModel = getWingModelFromOrganogramId(id);
        EmployeeOfficeDTO employeeOfficeDTO = EmployeeOfficeRepository.getInstance().getByOfficeUnitOrganogramId(id);
        if(employeeOfficeDTO == null){
            return organogramDetails;
        }
        organogramDetails.employeeRecordId = employeeOfficeDTO.employeeRecordId;
        Employee_recordsDTO employeeRecordsDTO = Employee_recordsRepository.getInstance().getById(employeeOfficeDTO.employeeRecordId);
        if(employeeRecordsDTO == null){
            return organogramDetails;
        }
        organogramDetails.empName = isLangEng ? employeeRecordsDTO.nameEng : employeeRecordsDTO.nameBng;
        String mobileNo = employeeRecordsDTO.personalMobile;
        if(mobileNo!=null){
            if(mobileNo.startsWith("880")){
                mobileNo = mobileNo.substring(2);
            }
            organogramDetails.empMobileNo = isLangEng ? mobileNo : StringUtils.convertToBanNumber(mobileNo);
        }
        organogramDetails.username = isLangEng ? employeeRecordsDTO.employeeNumber : StringUtils.convertToBanNumber(employeeRecordsDTO.employeeNumber);
        organogramDetails.empAddress =  GeoLocationDAO2.getAddressToShow(employeeRecordsDTO.presentAddress, isLangEng ? "English":"Bangla");

        UserDTO userDTO= UserRepository.getUserDTOByUserName(employeeRecordsDTO.employeeNumber);
        if(userDTO!=null){
            organogramDetails.userId = userDTO.ID;
            organogramDetails.roleID = userDTO.roleID;
        }
        logger.debug("returning getOrganogramDetailsByOrganogramId for id : "+id);
        return organogramDetails;
    }
}