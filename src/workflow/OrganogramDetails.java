package workflow;

/*
 * @author Md. Erfan Hossain
 * @created 16/12/2021 - 1:02 PM
 * @project parliament
 */

public class OrganogramDetails {
    public long organogramId = -1;
    public String organogramName = "";
    public long officeUnitId = -1;
    public String officeName = "";
    public long employeeRecordId = -1;
    public String empName = "";
    public String empMobileNo="";
    public String empAddress = "";
    public String username = "";
    public long userId = -1;
    public long roleID = 1;
    public WingModel wingModel;
}
