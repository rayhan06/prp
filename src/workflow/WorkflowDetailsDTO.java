package workflow;

public class WorkflowDetailsDTO 
{
	public long approvalPathID = 0;
	public String  approvalPathName = "";
	public int approvalPathOrder = 0;
	public long date = 0;
	public int action = 0;
	public String actionStr = "";
	public int status = 0;
	public String statusStr = "";
	public long organogramID = 0;
	public long userID = 0;
	public String employeeName = "";
	public String employeeDesignation = "";
	
}
