package attendance_alert_details;

import common.CommonDTOService;
import config.GlobalConfigDTO;
import config.GlobalConfigurationRepository;
import employee_records.Employee_recordsRepository;
import employee_shift.Employee_shiftDTO;
import employee_shift.Employee_shiftRepository;
import mail.EmailService;
import mail.SendEmailDTO;
import office_shift_details.Office_shift_detailsDTO;
import office_shift_details.Office_shift_detailsRepository;
import org.apache.log4j.Logger;
import user.UserDTO;
import util.CommonDTO;
import util.NavigationService4;
import util.StringUtils;
import workflow.WorkflowController;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@SuppressWarnings({"rawtypes","unused","Duplicates"})
public class Attendance_alert_detailsDAO extends NavigationService4 implements CommonDTOService<Attendance_alert_detailsDTO> {

    private static final Logger logger = Logger.getLogger(Attendance_alert_detailsDAO.class);
    EmailService emailService = new EmailService();
    private final static String regularShiftInTime = "10:00:00";
    private final static String regularShiftOutTime = "18:00:00";

    public static final String addQuery = "INSERT INTO {tableName} (employee_records_id,supervisor_organogram_id,supervisor_name,supervisor_designation,"
            + "alert_send_time,attendance_date,in_time,out_time,inserted_by,modified_by,search_column,lastModificationTime,"
            + "isDeleted,ID) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
    public static final String updateQuery = "UPDATE {tableName} SET employee_records_id=?,supervisor_organogram_id=?,supervisor_name=?,supervisor_designation=?"
            + "alert_send_time=?,attendance_date=?,in_time=?,out_time=?,inserted_by=?,modified_by=?,search_column=?,lastModificationTime=? WHERE ID =? ";
    private static final Map<String, String> searchMap = new HashMap<>();

    public Attendance_alert_detailsDAO(String tableName) {
        super(tableName);
        joinSQL = "";
        useSafeSearch = true;
        commonMaps = new Attendance_alert_detailsMAPS(tableName);
    }

    public Attendance_alert_detailsDAO() {
        this("attendance_alert_details");
    }

    public void setSearchColumn(Attendance_alert_detailsDTO attendance_alert_detailsDTO) {
        attendance_alert_detailsDTO.searchColumn = "";
        attendance_alert_detailsDTO.searchColumn += attendance_alert_detailsDTO.supervisorName + " ";
        attendance_alert_detailsDTO.searchColumn += attendance_alert_detailsDTO.supervisorDesignation + " ";

    }

    public void set(PreparedStatement ps, Attendance_alert_detailsDTO attendance_alert_detailsDTO, boolean isInsert) throws SQLException {
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm aa");
        long inTimeMS = 0, outTimeMS = 0;
        try {
            inTimeMS = sdf.parse(attendance_alert_detailsDTO.inTime).getTime();
            outTimeMS = sdf.parse(attendance_alert_detailsDTO.outTime).getTime();
        } catch (Exception ex) {
            logger.debug(ex);
        }

        int index = 0;
        long lastModificationTime = System.currentTimeMillis();
        setSearchColumn(attendance_alert_detailsDTO);
        ps.setObject(++index, attendance_alert_detailsDTO.employeeRecordsId);
        ps.setObject(++index, attendance_alert_detailsDTO.supervisorOrganogramId);
        ps.setObject(++index, attendance_alert_detailsDTO.supervisorName);
        ps.setObject(++index, attendance_alert_detailsDTO.supervisorDesignation);
        ps.setObject(++index, attendance_alert_detailsDTO.alertSendTime);
        ps.setObject(++index, attendance_alert_detailsDTO.attendanceDate);
        ps.setObject(++index, new Time(inTimeMS));
        ps.setObject(++index, new Time(outTimeMS));
        ps.setObject(++index, attendance_alert_detailsDTO.insertedBy);
        ps.setObject(++index, attendance_alert_detailsDTO.modifiedBy);
        ps.setObject(++index, attendance_alert_detailsDTO.searchColumn);
        if (isInsert) {
            ps.setObject(++index, lastModificationTime);
            ps.setObject(++index, 0);
        }
        ps.setObject(++index, attendance_alert_detailsDTO.iD);
    }

    @Override
    public Attendance_alert_detailsDTO buildObjectFromResultSet(ResultSet rs) {
        Attendance_alert_detailsDTO attendance_alert_detailsDTO = new Attendance_alert_detailsDTO();
        try {
            attendance_alert_detailsDTO.iD = rs.getLong("ID");
            attendance_alert_detailsDTO.employeeRecordsId = rs.getLong("employee_records_id");
            attendance_alert_detailsDTO.supervisorOrganogramId = rs.getLong("supervisor_organogram_id");
            attendance_alert_detailsDTO.supervisorName = rs.getString("supervisor_name");
            attendance_alert_detailsDTO.supervisorDesignation = rs.getString("supervisor_designation");
            attendance_alert_detailsDTO.alertSendTime = rs.getString("alert_send_time");
            attendance_alert_detailsDTO.attendanceDate = rs.getLong("attendance_date");
            Time inTime = rs.getTime("in_time");
            if (inTime != null) {
                attendance_alert_detailsDTO.inTime = inTime.toString();
            }
            Time outTime = rs.getTime("out_time");
            if (outTime != null) {
                attendance_alert_detailsDTO.outTime = outTime.toString();
            }
            attendance_alert_detailsDTO.insertedBy = rs.getString("inserted_by");
            attendance_alert_detailsDTO.modifiedBy = rs.getString("modified_by");
            attendance_alert_detailsDTO.searchColumn = rs.getString("search_column");
            attendance_alert_detailsDTO.isDeleted = rs.getInt("isDeleted");
            attendance_alert_detailsDTO.lastModificationTime = rs.getLong("lastModificationTime");
            return attendance_alert_detailsDTO;
        } catch (Exception e) {
            logger.error(e);
            return null;
        }


    }

    @Override
    public String getTableName() {
        return "attendance_alert_details";
    }


    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Attendance_alert_detailsDTO) commonDTO, addQuery, true);
    }


    //need another getter for repository
    public Attendance_alert_detailsDTO getDTOByID(long ID) {
        return getDTOFromID(ID);
    }

    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Attendance_alert_detailsDTO) commonDTO, updateQuery, false);
    }

    //add repository
    public List<Attendance_alert_detailsDTO> getAllAttendance_alert_details(boolean isFirstReload) {
        return getAllDTOs(isFirstReload);
    }


    public List<Attendance_alert_detailsDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO) {
        return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
    }


    public List<Attendance_alert_detailsDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
                                                     String filter, boolean tableHasJobCat) {
        String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO,
                filter, tableHasJobCat, null);
        return getDTOs(sql);

    }

    public String getSqlWithSearchCriteriaForNormalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
                                                          UserDTO userDTO, String filter, boolean tableHasJobCat, List<Object> objectList) {
        return getSearchQuery(tableName, p_searchCriteria, limit, offset, category, searchMap, objectList);
    }

    public String getSqlWithSearchCriteria(Hashtable p_searchCriteria, int limit, int offset, int category,
                                           boolean isPermanentTable, UserDTO userDTO, String filter, boolean tableHasJobCat, List<Object> objectList) {
        return getSqlWithSearchCriteriaForNormalSearch(p_searchCriteria, limit, offset, category, userDTO, filter, tableHasJobCat, objectList);
    }

    public void alertEmployeeIfLateEntry(long employeeRecordsId, long date, String inTime, String outTime) throws Exception {
        if (isEmployeeLate(employeeRecordsId, date, inTime, true)) {
            Attendance_alert_detailsDTO dto = new Attendance_alert_detailsDTO();
            dto.employeeRecordsId = employeeRecordsId;
            dto.supervisorOrganogramId = 0;
            dto.supervisorName = "Dummy Name";
            dto.supervisorDesignation = "Dummy Designation";
            dto.alertSendTime = "12:10 AM";
            dto.attendanceDate = date;
            dto.inTime = inTime;
            dto.outTime = outTime;
            this.add(dto);
        }
        if (isEmployeeLeaveEarly(employeeRecordsId, date, outTime, true)) {
            Attendance_alert_detailsDTO dto = new Attendance_alert_detailsDTO();
            dto.employeeRecordsId = employeeRecordsId;
            dto.supervisorOrganogramId = 0;
            dto.supervisorName = "Dummy Name";
            dto.supervisorDesignation = "Dummy Designation";
            dto.alertSendTime = "12:10 AM";
            dto.attendanceDate = date;
            dto.inTime = inTime;
            dto.outTime = outTime;
            this.add(dto);
        }
    }

    public boolean isEmployeeLate(long employeeRecordsId, long date, String inTime, boolean isAlert) {
        List<Employee_shiftDTO> employeeShiftDtos = Employee_shiftRepository.getInstance().getEmployee_shiftDTOByemployee_records_id(employeeRecordsId);
        long officeShiftDetailsId = 0;
        for (Employee_shiftDTO dto : employeeShiftDtos) {
            if (date >= dto.startDate && date <= dto.endDate) {
                officeShiftDetailsId = dto.officeShiftDetailsId; // get the shift currently running for the employee
            }
        }
        Office_shift_detailsDTO office_shift_detailsDTO = Office_shift_detailsRepository.getInstance().getOffice_shift_detailsDTOByid(officeShiftDetailsId);

        long bufferTimeInMin;

        if (office_shift_detailsDTO == null) {
            office_shift_detailsDTO = new Office_shift_detailsDTO();
            office_shift_detailsDTO.inTime = regularShiftInTime;
            bufferTimeInMin = 10;
        } else {
            if (isAlert) {
                bufferTimeInMin = office_shift_detailsDTO.lateInAlertMinute;
            } else {
                bufferTimeInMin = office_shift_detailsDTO.graceInPeriodMins;
            }
        }

        long actualEntryTimeMs = date + getTimeMs(new SimpleDateFormat("hh:mm aa"), inTime);
        long lateAlertTimeMs = date + getTimeMs(new SimpleDateFormat("HH:mm:ss"), office_shift_detailsDTO.inTime) + TimeUnit.MINUTES.toMillis(bufferTimeInMin);

        return actualEntryTimeMs > lateAlertTimeMs;

    }

    public boolean isEmployeeLeaveEarly(long employeeRecordsId, long date, String outTime, boolean isAlert) {
        List<Employee_shiftDTO> officeShiftDtos = Employee_shiftRepository.getInstance().getEmployee_shiftDTOByemployee_records_id(employeeRecordsId);
        long officeShiftDetailsId = 0;
        for (Employee_shiftDTO dto : officeShiftDtos) {
            if (date >= dto.startDate && date <= dto.endDate) {
                officeShiftDetailsId = dto.officeShiftDetailsId; // get the shift currently running for the employee
            }
        }
        Office_shift_detailsDTO office_shift_detailsDTO = Office_shift_detailsRepository.getInstance().getOffice_shift_detailsDTOByid(officeShiftDetailsId);

        long bufferTimeInMin;

        if (office_shift_detailsDTO == null) {
            office_shift_detailsDTO = new Office_shift_detailsDTO();
            office_shift_detailsDTO.outTime = regularShiftOutTime;
            bufferTimeInMin = 10; // assume 10 mins
        } else {
            if (isAlert) {
                bufferTimeInMin = office_shift_detailsDTO.earlyOutAlertMinute;
            } else {
                bufferTimeInMin = office_shift_detailsDTO.graceOutPeriodMins;
            }
        }

        long actualLeaveTimeMs = date + getTimeMs(new SimpleDateFormat("hh:mm aa"), outTime);
        long earlyAlertTimeMs = date + getTimeMs(new SimpleDateFormat("HH:mm:ss"), office_shift_detailsDTO.outTime) - TimeUnit.MINUTES.toMillis(bufferTimeInMin);

        return actualLeaveTimeMs < earlyAlertTimeMs;
    }

    private long getTimeMs(SimpleDateFormat sdf, String time) {
        long timeMS = 0;
        try {
            timeMS = sdf.parse(time).getTime();
        } catch (Exception ex) {
            logger.debug(ex);
        }
        return timeMS;
    }

    public void sendAlertToSupervisor() throws Exception {

        SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy");
        Date today = new Date();
        today = dateFormatter.parse(dateFormatter.format(today));

        Date previousDay = new Date(today.getTime() - (24 * 60 * 60 * 1000));

        String sql = "select * from attendance_alert_details where attendance_date >= " + previousDay.getTime() + " and attendance_date < " + today.getTime();
        List<Attendance_alert_detailsDTO> dtos = getDTOs(sql);

        Map<Long, List<Attendance_alert_detailsDTO>> supervisorOrganogramToDTOmap = dtos.stream().collect(Collectors.groupingBy(Attendance_alert_detailsDTO::getSupervisorOrganogramId));

        for (Map.Entry<Long, List<Attendance_alert_detailsDTO>> entry : supervisorOrganogramToDTOmap.entrySet()) {

            sendEmail(entry.getKey(), entry.getValue());

        }
    }

    // Email sending is temporary off in email service
    private void sendEmail(long supervisorOrganogramId, List<Attendance_alert_detailsDTO> dtos) throws Exception {
        SendEmailDTO sendEmailDTO = new SendEmailDTO();

        sendEmailDTO.setFrom("dummy@gmail.com");

        String[] to = new String[1];
        to[0] = WorkflowController.getEmailFromOrganogramId(supervisorOrganogramId);

        sendEmailDTO.setTo(to);
        sendEmailDTO.setSubject("Aleart for late Employees");
        sendEmailDTO.setText(buildEmailText(dtos));

        emailService.sendMail(sendEmailDTO);
    }

    public String buildEmailText(List<Attendance_alert_detailsDTO> dtos) {

        StringBuilder emailTextSb = new StringBuilder();

        for (Attendance_alert_detailsDTO dto : dtos) {

            emailTextSb.append("Employee Name: ");
            emailTextSb.append(Employee_recordsRepository.getInstance().getEmployeeName(dto.employeeRecordsId, "English"));
            emailTextSb.append(System.lineSeparator());
            emailTextSb.append("Attendance Date: ").append(StringUtils.getFormattedDate("English", dto.attendanceDate));
            emailTextSb.append(System.lineSeparator());
            emailTextSb.append("Entry Time: ").append(dto.inTime).append(System.lineSeparator());
            emailTextSb.append("Leave Time: ").append(dto.outTime).append(System.lineSeparator());
        }

        return emailTextSb.toString();
    }

    public static int getTimePeriodInSecFromGlobalConfig() {

        List<GlobalConfigDTO> allConfigs = GlobalConfigurationRepository.getInstance().getAllConfigs();

        Optional<GlobalConfigDTO> dto = allConfigs.stream()
                .filter(v -> v.name.equals("ATTENDANCE_ALERT_FREQUENCY"))
                .findFirst();

        if (dto.isPresent() && dto.get().value.equalsIgnoreCase("daily")) {
            return 24 * 60 * 60;
        } else if (dto.isPresent() && dto.get().value.equalsIgnoreCase("weekly")) {
            return 7 * 24 * 60 * 60;
        } else if (dto.isPresent() && dto.get().value.equalsIgnoreCase("bi-weekly")) {
            return 15 * 24 * 60 * 60;
        } else if (dto.isPresent() && dto.get().value.equalsIgnoreCase("monthly")) {
            return 30 * 24 * 60 * 60;
        } else
            return 30 * 24 * 60 * 60;  // if nothing found set monthly as default


    }


}
	