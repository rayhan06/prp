package attendance_alert_details;
import java.util.*; 
import util.*; 


public class Attendance_alert_detailsDTO extends CommonDTO
{

	public long employeeRecordsId = 0;
	public long supervisorOrganogramId = 0;
    public String supervisorName = "";
    public String supervisorDesignation = "";
    public String alertSendTime = "";
    public String insertedBy = "";
    public String modifiedBy = "";
    public long attendanceDate = 0;
    public String inTime = "";
    public String outTime = "";
	
	
    @Override
	public String toString() {
            return "$Attendance_alert_detailsDTO[" +
            " iD = " + iD +
            " employeeRecordsId = " + employeeRecordsId +
            " supervisorOrganogramId = " + supervisorOrganogramId +
            " supervisorName = " + supervisorName +
            " supervisorDesignation = " + supervisorDesignation +
            " alertSendTime = " + alertSendTime +
            " insertedBy = " + insertedBy +
            " modifiedBy = " + modifiedBy +
            " searchColumn = " + searchColumn +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

    public long getSupervisorOrganogramId() {
        return supervisorOrganogramId;
    }

    public void setSupervisorOrganogramId(long supervisorOrganogramId) {
        this.supervisorOrganogramId = supervisorOrganogramId;
    }
}