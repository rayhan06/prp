package attendance_alert_details;

import org.apache.log4j.Logger;

public class AttendanceAlertSender implements Runnable {

	private static final Logger logger = Logger.getLogger( AttendanceAlertSender.class );
	@Override
	public void run() {

		try {

			new Attendance_alert_detailsDAO().sendAlertToSupervisor();
		} catch (Exception e) {

			logger.error( "Error, when sending email to supervisor , ", e );
		}
	}
}
