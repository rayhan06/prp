package attendance_alert_details;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class Attendance_alert_detailsRepository implements Repository {
	Attendance_alert_detailsDAO attendance_alert_detailsDAO = null;
	
	public void setDAO(Attendance_alert_detailsDAO attendance_alert_detailsDAO)
	{
		this.attendance_alert_detailsDAO = attendance_alert_detailsDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Attendance_alert_detailsRepository.class);
	Map<Long, Attendance_alert_detailsDTO>mapOfAttendance_alert_detailsDTOToiD;
	Map<Long, Set<Attendance_alert_detailsDTO> >mapOfAttendance_alert_detailsDTOToemployeeRecordsId;
	Map<Long, Set<Attendance_alert_detailsDTO> >mapOfAttendance_alert_detailsDTOTosupervisorOrganogramId;
	Map<String, Set<Attendance_alert_detailsDTO> >mapOfAttendance_alert_detailsDTOTosupervisorName;
	Map<String, Set<Attendance_alert_detailsDTO> >mapOfAttendance_alert_detailsDTOTosupervisorDesignation;


	static Attendance_alert_detailsRepository instance = null;  
	private Attendance_alert_detailsRepository(){
		mapOfAttendance_alert_detailsDTOToiD = new ConcurrentHashMap<>();
		mapOfAttendance_alert_detailsDTOToemployeeRecordsId = new ConcurrentHashMap<>();
		mapOfAttendance_alert_detailsDTOTosupervisorOrganogramId = new ConcurrentHashMap<>();
		mapOfAttendance_alert_detailsDTOTosupervisorName = new ConcurrentHashMap<>();
		mapOfAttendance_alert_detailsDTOTosupervisorDesignation = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Attendance_alert_detailsRepository getInstance(){
		if (instance == null){
			instance = new Attendance_alert_detailsRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(attendance_alert_detailsDAO == null)
		{
			return;
		}
		try {
			List<Attendance_alert_detailsDTO> attendance_alert_detailsDTOs = attendance_alert_detailsDAO.getAllAttendance_alert_details(reloadAll);
			for(Attendance_alert_detailsDTO attendance_alert_detailsDTO : attendance_alert_detailsDTOs) {
				Attendance_alert_detailsDTO oldAttendance_alert_detailsDTO = getAttendance_alert_detailsDTOByID(attendance_alert_detailsDTO.iD);
				if( oldAttendance_alert_detailsDTO != null ) {
					mapOfAttendance_alert_detailsDTOToiD.remove(oldAttendance_alert_detailsDTO.iD);
				
					if(mapOfAttendance_alert_detailsDTOToemployeeRecordsId.containsKey(oldAttendance_alert_detailsDTO.employeeRecordsId)) {
						mapOfAttendance_alert_detailsDTOToemployeeRecordsId.get(oldAttendance_alert_detailsDTO.employeeRecordsId).remove(oldAttendance_alert_detailsDTO);
					}
					if(mapOfAttendance_alert_detailsDTOToemployeeRecordsId.get(oldAttendance_alert_detailsDTO.employeeRecordsId).isEmpty()) {
						mapOfAttendance_alert_detailsDTOToemployeeRecordsId.remove(oldAttendance_alert_detailsDTO.employeeRecordsId);
					}
					
					if(mapOfAttendance_alert_detailsDTOTosupervisorOrganogramId.containsKey(oldAttendance_alert_detailsDTO.supervisorOrganogramId)) {
						mapOfAttendance_alert_detailsDTOTosupervisorOrganogramId.get(oldAttendance_alert_detailsDTO.supervisorOrganogramId).remove(oldAttendance_alert_detailsDTO);
					}
					if(mapOfAttendance_alert_detailsDTOTosupervisorOrganogramId.get(oldAttendance_alert_detailsDTO.supervisorOrganogramId).isEmpty()) {
						mapOfAttendance_alert_detailsDTOTosupervisorOrganogramId.remove(oldAttendance_alert_detailsDTO.supervisorOrganogramId);
					}
					
					if(mapOfAttendance_alert_detailsDTOTosupervisorName.containsKey(oldAttendance_alert_detailsDTO.supervisorName)) {
						mapOfAttendance_alert_detailsDTOTosupervisorName.get(oldAttendance_alert_detailsDTO.supervisorName).remove(oldAttendance_alert_detailsDTO);
					}
					if(mapOfAttendance_alert_detailsDTOTosupervisorName.get(oldAttendance_alert_detailsDTO.supervisorName).isEmpty()) {
						mapOfAttendance_alert_detailsDTOTosupervisorName.remove(oldAttendance_alert_detailsDTO.supervisorName);
					}
					
					if(mapOfAttendance_alert_detailsDTOTosupervisorDesignation.containsKey(oldAttendance_alert_detailsDTO.supervisorDesignation)) {
						mapOfAttendance_alert_detailsDTOTosupervisorDesignation.get(oldAttendance_alert_detailsDTO.supervisorDesignation).remove(oldAttendance_alert_detailsDTO);
					}
					if(mapOfAttendance_alert_detailsDTOTosupervisorDesignation.get(oldAttendance_alert_detailsDTO.supervisorDesignation).isEmpty()) {
						mapOfAttendance_alert_detailsDTOTosupervisorDesignation.remove(oldAttendance_alert_detailsDTO.supervisorDesignation);
					}
					
					
				}
				if(attendance_alert_detailsDTO.isDeleted == 0) 
				{
					
					mapOfAttendance_alert_detailsDTOToiD.put(attendance_alert_detailsDTO.iD, attendance_alert_detailsDTO);
				
					if( ! mapOfAttendance_alert_detailsDTOToemployeeRecordsId.containsKey(attendance_alert_detailsDTO.employeeRecordsId)) {
						mapOfAttendance_alert_detailsDTOToemployeeRecordsId.put(attendance_alert_detailsDTO.employeeRecordsId, new HashSet<>());
					}
					mapOfAttendance_alert_detailsDTOToemployeeRecordsId.get(attendance_alert_detailsDTO.employeeRecordsId).add(attendance_alert_detailsDTO);
					
					if( ! mapOfAttendance_alert_detailsDTOTosupervisorOrganogramId.containsKey(attendance_alert_detailsDTO.supervisorOrganogramId)) {
						mapOfAttendance_alert_detailsDTOTosupervisorOrganogramId.put(attendance_alert_detailsDTO.supervisorOrganogramId, new HashSet<>());
					}
					mapOfAttendance_alert_detailsDTOTosupervisorOrganogramId.get(attendance_alert_detailsDTO.supervisorOrganogramId).add(attendance_alert_detailsDTO);
					
					if( ! mapOfAttendance_alert_detailsDTOTosupervisorName.containsKey(attendance_alert_detailsDTO.supervisorName)) {
						mapOfAttendance_alert_detailsDTOTosupervisorName.put(attendance_alert_detailsDTO.supervisorName, new HashSet<>());
					}
					mapOfAttendance_alert_detailsDTOTosupervisorName.get(attendance_alert_detailsDTO.supervisorName).add(attendance_alert_detailsDTO);
					
					if( ! mapOfAttendance_alert_detailsDTOTosupervisorDesignation.containsKey(attendance_alert_detailsDTO.supervisorDesignation)) {
						mapOfAttendance_alert_detailsDTOTosupervisorDesignation.put(attendance_alert_detailsDTO.supervisorDesignation, new HashSet<>());
					}
					mapOfAttendance_alert_detailsDTOTosupervisorDesignation.get(attendance_alert_detailsDTO.supervisorDesignation).add(attendance_alert_detailsDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Attendance_alert_detailsDTO> getAttendance_alert_detailsList() {
		List <Attendance_alert_detailsDTO> attendance_alert_detailss = new ArrayList<Attendance_alert_detailsDTO>(this.mapOfAttendance_alert_detailsDTOToiD.values());
		return attendance_alert_detailss;
	}
	
	
	public Attendance_alert_detailsDTO getAttendance_alert_detailsDTOByID( long ID){
		return mapOfAttendance_alert_detailsDTOToiD.get(ID);
	}
	
	
	public List<Attendance_alert_detailsDTO> getAttendance_alert_detailsDTOByemployee_records_id(long employee_records_id) {
		return new ArrayList<>( mapOfAttendance_alert_detailsDTOToemployeeRecordsId.getOrDefault(employee_records_id,new HashSet<>()));
	}
	
	
	public List<Attendance_alert_detailsDTO> getAttendance_alert_detailsDTOBysupervisor_organogram_id(long supervisor_organogram_id) {
		return new ArrayList<>( mapOfAttendance_alert_detailsDTOTosupervisorOrganogramId.getOrDefault(supervisor_organogram_id,new HashSet<>()));
	}
	
	
	public List<Attendance_alert_detailsDTO> getAttendance_alert_detailsDTOBysupervisor_name(String supervisor_name) {
		return new ArrayList<>( mapOfAttendance_alert_detailsDTOTosupervisorName.getOrDefault(supervisor_name,new HashSet<>()));
	}
	
	
	public List<Attendance_alert_detailsDTO> getAttendance_alert_detailsDTOBysupervisor_designation(String supervisor_designation) {
		return new ArrayList<>( mapOfAttendance_alert_detailsDTOTosupervisorDesignation.getOrDefault(supervisor_designation,new HashSet<>()));
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "attendance_alert_details";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


