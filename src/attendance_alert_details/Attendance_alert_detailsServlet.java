package attendance_alert_details;

import java.io.IOException;
import java.io.*;
import java.text.SimpleDateFormat;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import org.apache.log4j.Logger;

import login.LoginDTO;
import permission.MenuConstants;
import role.PermissionRepository;


import sessionmanager.SessionConstants;

import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;
import util.CommonRequestHandler;
import util.RecordNavigationManager4;

import java.util.*;
import javax.servlet.http.*;
import java.util.UUID;





import geolocation.GeoLocationDAO2;
import java.util.StringTokenizer;

import com.google.gson.Gson;

import pb.*;
import pbReport.*;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;




/**
 * Servlet implementation class Attendance_alert_detailsServlet
 */
@WebServlet("/Attendance_alert_detailsServlet")
@MultipartConfig
public class Attendance_alert_detailsServlet extends HttpServlet 
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Attendance_alert_detailsServlet.class);

    String tableName = "attendance_alert_details";

	Attendance_alert_detailsDAO attendance_alert_detailsDAO;
	CommonRequestHandler commonRequestHandler;
    private final Gson gson = new Gson();
    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Attendance_alert_detailsServlet() 
	{
        super();
    	try
    	{
			attendance_alert_detailsDAO = new Attendance_alert_detailsDAO(tableName);
			commonRequestHandler = new CommonRequestHandler(attendance_alert_detailsDAO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }   

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		System.out.println("In doget request = " + request);
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		try
		{
			String actionType = request.getParameter("actionType");
			if(actionType.equals("getAddPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ATTENDANCE_ALERT_DETAILS_ADD))
				{
					commonRequestHandler.getAddPage(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getEditPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ATTENDANCE_ALERT_DETAILS_UPDATE))
				{
					getAttendance_alert_details(request, response);					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}						
			}
			else if(actionType.equals("getURL"))
			{
				String URL = request.getParameter("URL");
				System.out.println("URL = " + URL);
				response.sendRedirect(URL);			
			}			
			else if(actionType.equals("search"))
			{
				System.out.println("search requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ATTENDANCE_ALERT_DETAILS_SEARCH))
				{
					if(isPermanentTable)
					{
						String filter = request.getParameter("filter");
						System.out.println("filter = " + filter);
						if(filter!=null)
						{
							filter = ""; //shouldn't be directly used, rather manipulate it.
							searchAttendance_alert_details(request, response, isPermanentTable, filter);
						}
						else
						{
							searchAttendance_alert_details(request, response, isPermanentTable, "");
						}
					}					
					else
					{
						//searchAttendance_alert_details(request, response, tempTableName, isPermanentTable);
					}
				}			
			}
			else if(actionType.equals("view"))
			{
				System.out.println("view requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ATTENDANCE_ALERT_DETAILS_SEARCH))
				{
					commonRequestHandler.view(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else
			{
				request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}


	
	

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		// TODO Auto-generated method stub
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		System.out.println("doPost");
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		
		try
		{
			String actionType = request.getParameter("actionType");
			System.out.println("actionType = " + actionType);
			if(actionType.equals("add"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ATTENDANCE_ALERT_DETAILS_ADD))
				{
					System.out.println("going to  addAttendance_alert_details ");
					addAttendance_alert_details(request, response, true, userDTO, true);
				}
				else
				{
					System.out.println("Not going to  addAttendance_alert_details ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}

			else if(actionType.equals("getDTO"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ATTENDANCE_ALERT_DETAILS_ADD))
				{
					getDTO(request, response);					
				}
				else
				{
					System.out.println("Not going to  addAttendance_alert_details ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else if(actionType.equals("edit"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ATTENDANCE_ALERT_DETAILS_UPDATE))
				{					
					addAttendance_alert_details(request, response, false, userDTO, isPermanentTable);					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("delete"))
			{								
				deleteAttendance_alert_details(request, response, userDTO);				
			}	
			else if(actionType.equals("search"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ATTENDANCE_ALERT_DETAILS_SEARCH))
				{
					searchAttendance_alert_details(request, response, true, "");
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getGeo"))
			{
				System.out.println("going to geoloc ");
				request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
			}
			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}
	
	private void getDTO(HttpServletRequest request, HttpServletResponse response) 
	{
		try 
		{
			System.out.println("In getDTO");
			Attendance_alert_detailsDTO attendance_alert_detailsDTO = attendance_alert_detailsDAO.getDTOByID(Long.parseLong(request.getParameter("ID")));
			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			
			String encoded = this.gson.toJson(attendance_alert_detailsDTO);
			System.out.println("json encoded data = " + encoded);
			out.print(encoded);
			out.flush();
		}
		catch (NumberFormatException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	private void addAttendance_alert_details(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, boolean isPermanentTable) throws IOException 
	{
		// TODO Auto-generated method stub
		try 
		{
			request.setAttribute("failureMessage", "");
			System.out.println("%%%% addAttendance_alert_details");
			String path = getServletContext().getRealPath("/img2/");
			Attendance_alert_detailsDTO attendance_alert_detailsDTO;
			SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
						
			if(addFlag == true)
			{
				attendance_alert_detailsDTO = new Attendance_alert_detailsDTO();
			}
			else
			{
				attendance_alert_detailsDTO = attendance_alert_detailsDAO.getDTOByID(Long.parseLong(request.getParameter("iD")));
			}
			
			String Value = "";

			Value = request.getParameter("employeeRecordsId");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("employeeRecordsId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				attendance_alert_detailsDTO.employeeRecordsId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("supervisorOrganogramId");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("supervisorOrganogramId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				attendance_alert_detailsDTO.supervisorOrganogramId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("supervisorName");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("supervisorName = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				attendance_alert_detailsDTO.supervisorName = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("supervisorDesignation");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("supervisorDesignation = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				attendance_alert_detailsDTO.supervisorDesignation = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("alertSendTime");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("alertSendTime = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				attendance_alert_detailsDTO.alertSendTime = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("insertedBy");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("insertedBy = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				attendance_alert_detailsDTO.insertedBy = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("modifiedBy");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("modifiedBy = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				attendance_alert_detailsDTO.modifiedBy = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("searchColumn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("searchColumn = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				attendance_alert_detailsDTO.searchColumn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			
			System.out.println("Done adding  addAttendance_alert_details dto = " + attendance_alert_detailsDTO);
			long returnedID = -1;
			
			if(isPermanentTable == false) //add new row for validation and make the old row outdated
			{
				attendance_alert_detailsDAO.setIsDeleted(attendance_alert_detailsDTO.iD, CommonDTO.OUTDATED);
				returnedID = attendance_alert_detailsDAO.add(attendance_alert_detailsDTO);
				attendance_alert_detailsDAO.setIsDeleted(returnedID, CommonDTO.WAITING_FOR_APPROVAL);
			}
			else if(addFlag == true)
			{
				returnedID = attendance_alert_detailsDAO.manageWriteOperations(attendance_alert_detailsDTO, SessionConstants.INSERT, -1, userDTO);
			}
			else
			{				
				returnedID = attendance_alert_detailsDAO.manageWriteOperations(attendance_alert_detailsDTO, SessionConstants.UPDATE, -1, userDTO);											
			}
			
			
			
			
			
			
			
			
			
			if(isPermanentTable)
			{
				String inPlaceSubmit = request.getParameter("inplacesubmit");
				
				if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
				{
					getAttendance_alert_details(request, response, returnedID);
				}
				else
				{
					response.sendRedirect("Attendance_alert_detailsServlet?actionType=search");
				}
			}
			else
			{
				commonRequestHandler.validate(attendance_alert_detailsDAO.getDTOByID(returnedID), request, response, userDTO);
			}
					
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	



	
	
	

	private void deleteAttendance_alert_details(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO) throws IOException 
	{				
		try 
		{
			String[] IDsToDelete = request.getParameterValues("ID");
			for(int i = 0; i < IDsToDelete.length; i ++)
			{
				long id = Long.parseLong(IDsToDelete[i]);
				System.out.println("------ DELETING " + IDsToDelete[i]);
				
				
				Attendance_alert_detailsDTO attendance_alert_detailsDTO = attendance_alert_detailsDAO.getDTOByID(id);
				attendance_alert_detailsDAO.manageWriteOperations(attendance_alert_detailsDTO, SessionConstants.DELETE, id, userDTO);
				
				
			}
			response.sendRedirect("Attendance_alert_detailsServlet?actionType=search");
		}
		catch (Exception ex) 
		{
			ex.printStackTrace();
		}
		
	}

	private void getAttendance_alert_details(HttpServletRequest request, HttpServletResponse response, long id) throws ServletException, IOException
	{
		System.out.println("in getAttendance_alert_details");
		Attendance_alert_detailsDTO attendance_alert_detailsDTO = null;
		try 
		{
			attendance_alert_detailsDTO = attendance_alert_detailsDAO.getDTOByID(id);
			request.setAttribute("ID", attendance_alert_detailsDTO.iD);
			request.setAttribute("attendance_alert_detailsDTO",attendance_alert_detailsDTO);
			request.setAttribute("attendance_alert_detailsDAO",attendance_alert_detailsDAO);
			
			String URL= "";
			
			String inPlaceEdit = request.getParameter("inplaceedit");
			String inPlaceSubmit = request.getParameter("inplacesubmit");
			String getBodyOnly = request.getParameter("getBodyOnly");
			
			if(inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase(""))
			{
				URL = "attendance_alert_details/attendance_alert_detailsInPlaceEdit.jsp";	
				request.setAttribute("inplaceedit","");				
			}
			else if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
			{
				URL = "attendance_alert_details/attendance_alert_detailsSearchRow.jsp";
				request.setAttribute("inplacesubmit","");					
			}
			else
			{
				if(getBodyOnly  != null && !getBodyOnly.equalsIgnoreCase(""))
				{
					URL = "attendance_alert_details/attendance_alert_detailsEditBody.jsp?actionType=edit";
				}
				else
				{
					URL = "attendance_alert_details/attendance_alert_detailsEdit.jsp?actionType=edit";
				}				
			}
			
			RequestDispatcher rd = request.getRequestDispatcher(URL);
			rd.forward(request, response);
		}
		catch (NumberFormatException e) 
		{
			e.printStackTrace();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	
	private void getAttendance_alert_details(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		getAttendance_alert_details(request, response, Long.parseLong(request.getParameter("ID")));	
	}
	
	private void searchAttendance_alert_details(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException
	{
		System.out.println("in  searchAttendance_alert_details 1");
		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String ajax = request.getParameter("ajax");
		boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);
		
        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
			SessionConstants.NAV_ATTENDANCE_ALERT_DETAILS,
			request,
			attendance_alert_detailsDAO,
			SessionConstants.VIEW_ATTENDANCE_ALERT_DETAILS,
			SessionConstants.SEARCH_ATTENDANCE_ALERT_DETAILS,
			tableName,
			isPermanent,
			userDTO,
			filter,
			true);
        try
        {
			System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        }
        catch(Exception e)
        {
			System.out.println("failed to dojob" + e);
        }

		request.setAttribute("attendance_alert_detailsDAO",attendance_alert_detailsDAO);
        RequestDispatcher rd;
        if(!isPermanent)
        {
        	if(hasAjax == false)
	        {
	        	System.out.println("Going to attendance_alert_details/attendance_alert_detailsApproval.jsp");
	        	rd = request.getRequestDispatcher("attendance_alert_details/attendance_alert_detailsApproval.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to attendance_alert_details/attendance_alert_detailsApprovalForm.jsp");
	        	rd = request.getRequestDispatcher("attendance_alert_details/attendance_alert_detailsApprovalForm.jsp");
	        }
        }
        else
        {
	        if(hasAjax == false)
	        {
	        	System.out.println("Going to attendance_alert_details/attendance_alert_detailsSearch.jsp");
	        	rd = request.getRequestDispatcher("attendance_alert_details/attendance_alert_detailsSearch.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to attendance_alert_details/attendance_alert_detailsSearchForm.jsp");
	        	rd = request.getRequestDispatcher("attendance_alert_details/attendance_alert_detailsSearchForm.jsp");
	        }
        }
		rd.forward(request, response);
	}
	
}

