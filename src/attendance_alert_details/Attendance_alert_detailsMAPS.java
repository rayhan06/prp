package attendance_alert_details;
import java.util.*; 
import util.*;


public class Attendance_alert_detailsMAPS extends CommonMaps
{	
	public Attendance_alert_detailsMAPS(String tableName)
	{
		

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("employeeRecordsId".toLowerCase(), "employeeRecordsId".toLowerCase());
		java_DTO_map.put("supervisorOrganogramId".toLowerCase(), "supervisorOrganogramId".toLowerCase());
		java_DTO_map.put("supervisorName".toLowerCase(), "supervisorName".toLowerCase());
		java_DTO_map.put("supervisorDesignation".toLowerCase(), "supervisorDesignation".toLowerCase());
		java_DTO_map.put("alertSendTime".toLowerCase(), "alertSendTime".toLowerCase());
		java_DTO_map.put("insertedBy".toLowerCase(), "insertedBy".toLowerCase());
		java_DTO_map.put("modifiedBy".toLowerCase(), "modifiedBy".toLowerCase());
		java_DTO_map.put("searchColumn".toLowerCase(), "searchColumn".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("employee_records_id".toLowerCase(), "employeeRecordsId".toLowerCase());
		java_SQL_map.put("supervisor_organogram_id".toLowerCase(), "supervisorOrganogramId".toLowerCase());
		java_SQL_map.put("supervisor_name".toLowerCase(), "supervisorName".toLowerCase());
		java_SQL_map.put("supervisor_designation".toLowerCase(), "supervisorDesignation".toLowerCase());
		java_SQL_map.put("alert_send_time".toLowerCase(), "alertSendTime".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Employee Records Id".toLowerCase(), "employeeRecordsId".toLowerCase());
		java_Text_map.put("Supervisor Organogram Id".toLowerCase(), "supervisorOrganogramId".toLowerCase());
		java_Text_map.put("Supervisor Name".toLowerCase(), "supervisorName".toLowerCase());
		java_Text_map.put("Supervisor Designation".toLowerCase(), "supervisorDesignation".toLowerCase());
		java_Text_map.put("Alert Send Time".toLowerCase(), "alertSendTime".toLowerCase());
		java_Text_map.put("Inserted By".toLowerCase(), "insertedBy".toLowerCase());
		java_Text_map.put("Modified By".toLowerCase(), "modifiedBy".toLowerCase());
		java_Text_map.put("Search Column".toLowerCase(), "searchColumn".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}