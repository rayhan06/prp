package am_parliament_building_room;

import am_office_assignment.Am_office_assignmentDAO;
import am_parliament_building_block.Am_parliament_building_blockDTO;
import am_parliament_building_block.Am_parliament_building_blockRepository;
import am_parliament_building_level.Am_parliament_building_levelDTO;
import am_parliament_building_level.Am_parliament_building_levelRepository;
import common.CommonDAOService;
import common.ConnectionAndStatementUtil;
import org.apache.log4j.Logger;
import pb.CatRepository;
import pb.CategoryLanguageModel;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class Am_parliament_building_roomDAO  implements CommonDAOService<Am_parliament_building_roomDTO>
{

	private static final Logger logger = Logger.getLogger(Am_parliament_building_roomDAO.class);
	private final Map<String,String> searchMap = new HashMap<>();

	private static final String addQuery = "INSERT INTO {tableName} (inserted_by_user_id,inserted_by_organogram_id," +
			"insertion_date, modified_by, search_column, level_type, block_type, room_no, remarks, status, not_available_reason," +
			"  lastModificationTime, isDeleted,ID) VALUES (?,?,?,?,?,?,?,?, ?, ?, ?, ?, ?, ?)";

	private static final String updateQuery = "UPDATE {tableName} SET inserted_by_user_id = ?,inserted_by_organogram_id=?," +
			" insertion_date=?, modified_by = ?, search_column=?,level_type=?,block_type=?," +
			" room_no = ?, remarks = ?, status = ?,not_available_reason =?, lastModificationTime=? WHERE ID = ?";

	private static class LazyLoader{
		static final Am_parliament_building_roomDAO INSTANCE = new Am_parliament_building_roomDAO();
	}

	public static Am_parliament_building_roomDAO getInstance(){
		return Am_parliament_building_roomDAO.LazyLoader.INSTANCE;
	}

	private Am_parliament_building_roomDAO() {
		searchMap.put("level_type"," and (level_type = ?)");
		searchMap.put("block_type"," and (block_type = ?)");
		searchMap.put("room_no"," and (room_no like ?)");
		searchMap.put("insertion_date_start"," and (insertion_date>= ?)");
		searchMap.put("insertion_date_end"," and (insertion_date <= ?)");
		searchMap.put("status"," and (status = ?)");
		searchMap.put("AnyField"," and (search_column like ?)");
	}


	public void setSearchColumn(Am_parliament_building_roomDTO am_parliament_building_roomDTO)
	{
		am_parliament_building_roomDTO.searchColumn = "";
		Am_parliament_building_levelDTO levelDTO = Am_parliament_building_levelRepository.getInstance().
				getAm_parliament_building_levelDTOByID(am_parliament_building_roomDTO.levelType);
		if(levelDTO != null){
			am_parliament_building_roomDTO.searchColumn += levelDTO.levelNo + " ";
		}

		Am_parliament_building_blockDTO blockDTO =  Am_parliament_building_blockRepository.getInstance().
				getAm_parliament_building_blockDTOByID(am_parliament_building_roomDTO.blockType);
		if(blockDTO != null){
			CategoryLanguageModel model = CatRepository.getInstance().getCategoryLanguageModel
					("parliament_block",blockDTO.parliamentBlockCat);
			if(model != null){
				am_parliament_building_roomDTO.searchColumn += model.englishText+" "+model.banglaText+" ";
			}
		}

		am_parliament_building_roomDTO.searchColumn += am_parliament_building_roomDTO.roomNo + " ";
	}


	@Override
	public void set(PreparedStatement ps, Am_parliament_building_roomDTO am_parliament_building_roomDTO, boolean isInsert) throws SQLException {
		setSearchColumn(am_parliament_building_roomDTO);
		int index = 0;
		ps.setObject(++index, am_parliament_building_roomDTO.insertedByUserId);
		ps.setObject(++index, am_parliament_building_roomDTO.insertedByOrganogramId);
		ps.setObject(++index, am_parliament_building_roomDTO.insertionDate);
		ps.setObject(++index, am_parliament_building_roomDTO.modifiedBy);
		ps.setObject(++index, am_parliament_building_roomDTO.searchColumn);
		ps.setObject(++index, am_parliament_building_roomDTO.levelType);
		ps.setObject(++index, am_parliament_building_roomDTO.blockType);
		ps.setObject(++index, am_parliament_building_roomDTO.roomNo);
		ps.setObject(++index, am_parliament_building_roomDTO.remarks);
		ps.setObject(++index, am_parliament_building_roomDTO.status);
		ps.setObject(++index, am_parliament_building_roomDTO.not_available_reason);
		ps.setObject(++index, am_parliament_building_roomDTO.lastModificationTime);
		if (isInsert) {
			ps.setObject(++index, 0);
		}
		ps.setObject(++index, am_parliament_building_roomDTO.iD);
	}

	@Override
	public Am_parliament_building_roomDTO buildObjectFromResultSet(ResultSet rs) {
		try
		{
			Am_parliament_building_roomDTO am_parliament_building_roomDTO = new Am_parliament_building_roomDTO();
			am_parliament_building_roomDTO.iD = rs.getLong("ID");
			am_parliament_building_roomDTO.levelType = rs.getLong("level_type");
			am_parliament_building_roomDTO.blockType = rs.getLong("block_type");
			am_parliament_building_roomDTO.status = rs.getInt("status");
			am_parliament_building_roomDTO.roomNo = rs.getString("room_no");
			am_parliament_building_roomDTO.remarks = rs.getString("remarks");
			am_parliament_building_roomDTO.not_available_reason = rs.getString("not_available_reason");
			am_parliament_building_roomDTO.insertedByUserId = rs.getLong("inserted_by_user_id");
			am_parliament_building_roomDTO.insertedByOrganogramId = rs.getLong("inserted_by_organogram_id");
			am_parliament_building_roomDTO.modifiedBy = rs.getLong("modified_by");
			am_parliament_building_roomDTO.insertionDate = rs.getLong("insertion_date");
			am_parliament_building_roomDTO.searchColumn = rs.getString("search_column");
			am_parliament_building_roomDTO.isDeleted = rs.getInt("isDeleted");
			am_parliament_building_roomDTO.lastModificationTime = rs.getLong("lastModificationTime");
			return am_parliament_building_roomDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}

	@Override
	public String getTableName() {
		return "am_parliament_building_room";
	}

	@Override
	public Map<String, String> getSearchMap() {
		return searchMap;
	}

	@Override
	public long add(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Am_parliament_building_roomDTO) commonDTO,addQuery,true);
	}

	@Override
	public long update(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Am_parliament_building_roomDTO) commonDTO,updateQuery,false);
	}

	public int getCountByBlockId(long blockId){
		String countQuery = "SELECT count(*) as countID FROM "
				+ getTableName() + " where isDeleted = 0 and block_type = ? ";
		return ConnectionAndStatementUtil.getT(countQuery, Arrays.asList(blockId), rs -> {
			try {
				return rs.getInt("countID");
			} catch (SQLException ex) {
				ex.printStackTrace();
				return 0;
			}
		}, 0);
	}

	public boolean isNotUsed(Long iD){
        return Am_office_assignmentDAO.getInstance().getCountByParliamentBuildingRoomId(iD) <= 0;
    }

	public int getCountOfBlockIdAndRoomNo (long blockId, String roomNo) {
		return (int) Am_parliament_building_roomRepository.getInstance().
				getAm_parliament_building_roomDTOByBlock_id(blockId).
				stream().filter(i -> i.roomNo.equals(roomNo)).count();
	}
}
	