package am_parliament_building_room;

import com.google.gson.Gson;
import org.apache.log4j.Logger;
import pb.CatDTO;
import repository.Repository;
import repository.RepositoryManager;
import vm_requisition.CommonApprovalStatus;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;


public class Am_parliament_building_roomRepository implements Repository {
	Am_parliament_building_roomDAO am_parliament_building_roomDAO;
	Gson gson = new Gson();
	
	public void setDAO(Am_parliament_building_roomDAO am_parliament_building_roomDAO)
	{
		this.am_parliament_building_roomDAO = am_parliament_building_roomDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Am_parliament_building_roomRepository.class);
	Map<Long, Am_parliament_building_roomDTO>mapOfAm_parliament_building_roomDTOToiD;
	Map<Long, Set<Am_parliament_building_roomDTO> >mapOfAm_parliament_building_roomDTOToBlockId;

  
	private Am_parliament_building_roomRepository(){
		am_parliament_building_roomDAO = Am_parliament_building_roomDAO.getInstance();
		mapOfAm_parliament_building_roomDTOToiD = new ConcurrentHashMap<>();
		mapOfAm_parliament_building_roomDTOToBlockId = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	private static class LazyLoader{
        static final Am_parliament_building_roomRepository INSTANCE = new Am_parliament_building_roomRepository();
    }

    public static Am_parliament_building_roomRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

	public void reload(boolean reloadAll){
		try {
			List<Am_parliament_building_roomDTO> am_parliament_building_roomDTOs =
					am_parliament_building_roomDAO.getAllDTOs(reloadAll);
			for(Am_parliament_building_roomDTO am_parliament_building_roomDTO : am_parliament_building_roomDTOs) {
				Am_parliament_building_roomDTO oldAm_parliament_building_roomDTO = 
						getAm_parliament_building_roomDTOByIDWithoutClone(am_parliament_building_roomDTO.iD);
				if( oldAm_parliament_building_roomDTO != null ) {
					mapOfAm_parliament_building_roomDTOToiD.remove(oldAm_parliament_building_roomDTO.iD);

					if(mapOfAm_parliament_building_roomDTOToBlockId.containsKey(oldAm_parliament_building_roomDTO.blockType)) {
						mapOfAm_parliament_building_roomDTOToBlockId.get(oldAm_parliament_building_roomDTO.blockType).
								remove(oldAm_parliament_building_roomDTO);
					}
					if(mapOfAm_parliament_building_roomDTOToBlockId.get(oldAm_parliament_building_roomDTO.blockType).isEmpty()) {
						mapOfAm_parliament_building_roomDTOToBlockId.remove(oldAm_parliament_building_roomDTO.blockType);
					}
				
					
				}
				if(am_parliament_building_roomDTO.isDeleted == 0) 
				{
					
					mapOfAm_parliament_building_roomDTOToiD.put(am_parliament_building_roomDTO.iD, am_parliament_building_roomDTO);

					if( ! mapOfAm_parliament_building_roomDTOToBlockId.containsKey(am_parliament_building_roomDTO.blockType)) {
						mapOfAm_parliament_building_roomDTOToBlockId.put(am_parliament_building_roomDTO.blockType, new HashSet<>());
					}
					mapOfAm_parliament_building_roomDTOToBlockId.get(am_parliament_building_roomDTO.blockType).add(am_parliament_building_roomDTO);
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Am_parliament_building_roomDTO> getAm_parliament_building_roomList() {
		return clone(new ArrayList<>(this.mapOfAm_parliament_building_roomDTOToiD.values()));
	}
	
	
	public Am_parliament_building_roomDTO getAm_parliament_building_roomDTOByID( long ID){
		return clone(mapOfAm_parliament_building_roomDTOToiD.get(ID));
	}

	public Am_parliament_building_roomDTO getAm_parliament_building_roomDTOByIDWithoutClone( long ID){
		return mapOfAm_parliament_building_roomDTOToiD.get(ID);
	}

	public List<Am_parliament_building_roomDTO> getAm_parliament_building_roomDTOByBlock_id(long blockId) {
		return clone(new ArrayList<>( mapOfAm_parliament_building_roomDTOToBlockId.
				getOrDefault(blockId,new HashSet<>())));
	}

	
	@Override
	public String getTableName() {
		return "am_parliament_building_room";
	}

	public Am_parliament_building_roomDTO clone(Am_parliament_building_roomDTO dto) {
		String raw = gson.toJson(dto);
		return gson.fromJson(raw, Am_parliament_building_roomDTO.class);
	}

	public List<Am_parliament_building_roomDTO> clone(List<Am_parliament_building_roomDTO> dtoList) {
		return dtoList
				.stream()
				.map(this::clone)
				.collect(Collectors.toList());
	}

	public String getAvailableOptionsByBlockId(String language, long blockId, long defaultValue){
		StringBuilder sOptions =new StringBuilder();
		if (defaultValue == -1)
		{
			if (language.equalsIgnoreCase("English"))
			{
				sOptions.append("<option value = ''>Select</option>");
			}
			else
			{
				sOptions.append("<option value = ''>বাছাই করুন</option>");
			}
		}

//		List<Am_parliament_building_roomDTO> dtos =
//				getAm_parliament_building_roomDTOByBlock_id(blockId)
//						.stream()
//						.filter( i -> i.status == CommonApprovalStatus.AVAILABLE.getValue())
//						.collect(Collectors.toList());

		List<Am_parliament_building_roomDTO> dtos =
				getAm_parliament_building_roomDTOByBlock_id(blockId)
						.stream()
						.collect(Collectors.toList());

		for(Am_parliament_building_roomDTO dto: dtos){
				StringBuilder sOption =new StringBuilder();
				sOption.append("<option value = '").append( dto.iD).append("'");
				if(defaultValue != CatDTO.CATDEFAULTNOTREQUIRED && dto.iD == defaultValue)
				{
					sOption.append(" selected ");
				}
				sOption.append(">").append(dto.roomNo).append("</option>");
				sOptions.append(sOption);

		}


		return sOptions.toString();

	}

	public String getAllRoomOptionsByBlockId(String language, long blockId, long defaultValue){
		StringBuilder sOptions =new StringBuilder();
		if (defaultValue == -1)
		{
			if (language.equalsIgnoreCase("English"))
			{
				sOptions.append("<option value = ''>Select</option>");
			}
			else
			{
				sOptions.append("<option value = ''>বাছাই করুন</option>");
			}
		}

		List<Am_parliament_building_roomDTO> dtos =
				getAm_parliament_building_roomDTOByBlock_id(blockId)
						.stream()
						.filter( i -> i.isDeleted == 0)
						.collect(Collectors.toList());

		for(Am_parliament_building_roomDTO dto: dtos){
			StringBuilder sOption =new StringBuilder();
			sOption.append("<option value = '").append( dto.iD).append("'");
			if(defaultValue != CatDTO.CATDEFAULTNOTREQUIRED && dto.iD == defaultValue)
			{
				sOption.append(" selected ");
			}
			sOption.append(">").append(dto.roomNo).append("</option>");
			sOptions.append(sOption);

		}


		return sOptions.toString();

	}


	public String getText(long id) {
		Am_parliament_building_roomDTO dto = getAm_parliament_building_roomDTOByID(id);
		return dto == null ? "": dto.roomNo;
	}
}


