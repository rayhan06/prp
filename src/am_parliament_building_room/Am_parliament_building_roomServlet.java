package am_parliament_building_room;

import am_parliament_building_block.Am_parliament_building_blockDTO;
import am_parliament_building_block.Am_parliament_building_blockRepository;
import am_parliament_building_level.Am_parliament_building_levelDTO;
import am_parliament_building_level.Am_parliament_building_levelRepository;
import common.ApiResponse;
import common.BaseServlet;
import common.CommonDAOService;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import pb.Utils;
import permission.MenuConstants;
import user.UserDTO;
import util.CommonDTO;
import util.CommonLoginData;
import util.HttpRequestUtils;
import util.UtilCharacter;
import vm_requisition.CommonApprovalStatus;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;


/**
 * Servlet implementation class Am_parliament_building_roomServlet
 */
@WebServlet("/Am_parliament_building_roomServlet")
@MultipartConfig
public class Am_parliament_building_roomServlet extends BaseServlet
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Am_parliament_building_roomServlet.class);



	@Override
	public String getTableName() {
		return Am_parliament_building_roomDAO.getInstance().getTableName();
	}

	@Override
	public String getServletName() {
		return "Am_parliament_building_roomServlet";
	}

	@Override
	public CommonDAOService<? extends CommonDTO> getCommonDAOService() {
		return Am_parliament_building_roomDAO.getInstance();
	}

	@Override
	public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
		Am_parliament_building_roomDTO am_parliament_building_roomDTO;


		if(Boolean.TRUE.equals(addFlag))
		{
			am_parliament_building_roomDTO = new Am_parliament_building_roomDTO();
			am_parliament_building_roomDTO.insertionDate = am_parliament_building_roomDTO.lastModificationTime
					= System.currentTimeMillis();
			am_parliament_building_roomDTO.insertedByUserId = userDTO.ID;
			am_parliament_building_roomDTO.insertedByOrganogramId = userDTO.organogramID;
			am_parliament_building_roomDTO.status = CommonApprovalStatus.AVAILABLE.getValue();
		}
		else
		{
			am_parliament_building_roomDTO = Am_parliament_building_roomRepository.getInstance().
					getAm_parliament_building_roomDTOByID(Long.parseLong(request.getParameter("iD")));
			if (am_parliament_building_roomDTO == null) {
				UtilCharacter.throwException("রুম খুঁজে পাওয়া যায় নি ", "Room not found");
			}

			if(!Am_parliament_building_roomDAO.getInstance().isNotUsed(am_parliament_building_roomDTO.iD)){
				UtilCharacter.throwException("ইতোমধ্যে ব্যবহৃত, পরিবর্তন সম্ভব নয় ", "Can't Update, Already Used.");
			}

			am_parliament_building_roomDTO.lastModificationTime = System.currentTimeMillis();
			am_parliament_building_roomDTO.modifiedBy = userDTO.ID;
		}

		am_parliament_building_roomDTO.levelType = Long.parseLong(request.getParameter("levelType"));
		Am_parliament_building_levelDTO levelDTO = Am_parliament_building_levelRepository.getInstance().
				getAm_parliament_building_levelDTOByID(am_parliament_building_roomDTO.levelType);
		if(levelDTO == null){
			UtilCharacter.throwException("লেভেল খুঁজে পাওয়া যায় নি ", "Level not found");
		}

		am_parliament_building_roomDTO.blockType = Long.parseLong(request.getParameter("blockType"));
		Am_parliament_building_blockDTO blockDTO = Am_parliament_building_blockRepository.getInstance().
				getAm_parliament_building_blockDTOByID(am_parliament_building_roomDTO.blockType);
		if(blockDTO == null){
			UtilCharacter.throwException("ব্লক খুঁজে পাওয়া যায় নি", "Block not found");
		}

		am_parliament_building_roomDTO.roomNo = Jsoup.clean(request.getParameter("roomNo"),
				Whitelist.simpleText());

		if (am_parliament_building_roomDTO.roomNo.isEmpty()) {
			UtilCharacter.throwException("রুম নম্বর প্রয়োজনীয় ", "Room Number required");
		}

		if(!uniquenessCheck(am_parliament_building_roomDTO.blockType, am_parliament_building_roomDTO.roomNo,
				addFlag, am_parliament_building_roomDTO.iD)){
			UtilCharacter.throwException("ব্লক এবং রুম নম্বর কম্বিনেশন ইউনিক নয়",
					"Block and Room No combination not unique");
		}

		am_parliament_building_roomDTO.remarks = Jsoup.clean(request.getParameter("remarks"),
				Whitelist.simpleText());

		if (Boolean.TRUE.equals(addFlag)) {
			getCommonDAOService().add(am_parliament_building_roomDTO);
		} else {
			getCommonDAOService().update(am_parliament_building_roomDTO);
		}
		return am_parliament_building_roomDTO;
	}

	@Override
	public int[] getAddPageMenuConstants() {
		return new int[]{MenuConstants.AM_PARLIAMENT_BUILDING_ROOM_ADD};
	}

	@Override
	public int[] getEditPageMenuConstants() {
		return new int[]{MenuConstants.AM_PARLIAMENT_BUILDING_ROOM_UPDATE};
	}

	@Override
	public int[] getSearchMenuConstants() {
		return new int[]{MenuConstants.AM_PARLIAMENT_BUILDING_ROOM_SEARCH};
	}

	@Override
	public Class<? extends HttpServlet> getClazz() {
		return Am_parliament_building_roomServlet.class;
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String actionType = request.getParameter("actionType");
		if ("getRoomByBlockId".equals(actionType)) {
			try {
				long blockId = Long.parseLong(request.getParameter("blockId"));
				long defaultValue = Long.parseLong(request.getParameter("defaultValue"));
				String language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
				String responseText = Am_parliament_building_roomRepository.getInstance().
						getAvailableOptionsByBlockId(language, blockId, defaultValue);
				ApiResponse.sendSuccessResponse(response, responseText);
			} catch (Exception ex) {
				logger.error(ex);
				ApiResponse.sendErrorResponse(response, ex.getMessage());
			}
			return;
		}
		if ("getAllRoomByBlockId".equals(actionType)) {
			try {
				long blockId = Long.parseLong(request.getParameter("blockId"));
				long defaultValue = Long.parseLong(request.getParameter("defaultValue"));
				String language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
				String responseText = Am_parliament_building_roomRepository.getInstance().
						getAllRoomOptionsByBlockId(language, blockId, defaultValue);
				ApiResponse.sendSuccessResponse(response, responseText);
			} catch (Exception ex) {
				logger.error(ex);
				ApiResponse.sendErrorResponse(response, ex.getMessage());
			}
			return;
		}
		super.doGet(request,response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String actionType = request.getParameter("actionType");
		CommonLoginData commonLoginData = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get();

		if ("setNotAvailable".equals(actionType) && Utils.checkPermission(commonLoginData.userDTO, getEditPageMenuConstants())
				&& getEditPermission(request)) {
			try {

				Am_parliament_building_roomDTO roomDTO =
						Am_parliament_building_roomRepository.getInstance().getAm_parliament_building_roomDTOByID
								(Long.parseLong(request.getParameter("roomId")));
				if (roomDTO == null) {
					UtilCharacter.throwException("রুম খুঁজে পাওয়া যায় নি ", "Room not found");
				}


				if(roomDTO.status != CommonApprovalStatus.AVAILABLE.getValue()){
					UtilCharacter.throwException("অবস্থা ব্যবহারযোগ্য হতে হবে ", "Status Have to be Available");
				}

				roomDTO.not_available_reason = Jsoup.clean(request.getParameter("remarks"),
						Whitelist.simpleText());
				roomDTO.status = CommonApprovalStatus.NOT_AVAILABLE.getValue();
				roomDTO.lastModificationTime = System.currentTimeMillis();
				roomDTO.modifiedBy = commonLoginData.userDTO.ID;
				Am_parliament_building_roomDAO.getInstance().update(roomDTO);

				String redirectUrl = "Am_parliament_building_roomServlet?actionType=view&ID=" + roomDTO.iD;
				ApiResponse.sendSuccessResponse(response, redirectUrl);


			} catch (Exception ex) {
				logger.error(ex);
				ApiResponse.sendErrorResponse(response, ex.getMessage());
			}
			return;
		}

		super.doPost(request,response);
	}


	@Override
	protected void deleteT(HttpServletRequest request, UserDTO userDTO) {
		String[] IDsToDelete = request.getParameterValues("ID");
		if(IDsToDelete.length>0){
			List<Long> ids = Stream.of(IDsToDelete)
					.map(Long::parseLong)
					.filter(i -> Am_parliament_building_roomDAO.getInstance().isNotUsed(i))
					.collect(Collectors.toList());
			if(!ids.isEmpty()) {
				getCommonDAOService().deletePb(userDTO.employee_record_id, ids);
			}
		}
	}

	private boolean uniquenessCheck(long blockId, String roomNo, boolean addFlag, Long ID){
		boolean flag = false;
		int count = Am_parliament_building_roomDAO.getInstance().getCountOfBlockIdAndRoomNo(blockId, roomNo);
		if (addFlag) {
			if (count == 0) {
				flag = true;
			}
		} else {
			Am_parliament_building_roomDTO roomDTO = Am_parliament_building_roomRepository.getInstance()
					.getAm_parliament_building_roomDTOByID(ID);
			if(roomDTO != null){
				Boolean sameValueSaveLogic = roomDTO.blockType == blockId &&
						roomDTO.roomNo.equals(roomNo) && count == 1;
				Boolean differentValueSaveLogic = !(roomDTO.blockType == blockId &&
						roomDTO.roomNo.equals(roomNo)) && count == 0;
				if (sameValueSaveLogic || differentValueSaveLogic) {
					flag = true;
				}
			}
		}

		return flag;
	}
}

