package am_parliament_building_room;

import util.CommonDTO;


public class Am_parliament_building_roomDTO extends CommonDTO
{

	public long levelType = -1;
	public long blockType = -1;
    public String roomNo = "";
	public long insertedByUserId = -1;
	public long insertedByOrganogramId = -1;
	public long modifiedBy = -1;
	public long insertionDate = -1;
	public int status = -1;
	public String not_available_reason = "";
	
	
    @Override
	public String toString() {
            return "$Am_parliament_building_roomDTO[" +
            " iD = " + iD +
            " levelType = " + levelType +
            " blockType = " + blockType +
            " roomNo = " + roomNo +
            " remarks = " + remarks +
            " insertedByUserId = " + insertedByUserId +
            " insertedByOrganogramId = " + insertedByOrganogramId +
            " modifiedBy = " + modifiedBy +
            " insertionDate = " + insertionDate +
            " searchColumn = " + searchColumn +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}