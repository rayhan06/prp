//package inbox_notification_template;
//
//import java.io.IOException;
//import java.io.*;
//
//
//import javax.servlet.RequestDispatcher;
//import javax.servlet.ServletException;
//import javax.servlet.annotation.MultipartConfig;
//import javax.servlet.annotation.WebServlet;
//
//
//import org.apache.log4j.Logger;
//
//import login.LoginDTO;
//import permission.MenuConstants;
//import role.PermissionRepository;
//
//
//import sessionmanager.SessionConstants;
//
//import user.UserDTO;
//import user.UserRepository;
//
//import util.RecordNavigationManager3;
//
//import java.util.*;
//import javax.servlet.http.*;
//import java.util.UUID;
//
//import inbox_notification_template.Constants;
//import approval_module_map.*;
//
//
//import com.google.gson.Gson;
//
//import pb.*;
//import pbReport.*;
//import org.jsoup.Jsoup;
//import org.jsoup.safety.Whitelist;
//
///**
// * Servlet implementation class Inbox_notification_templateServlet
// */
//@WebServlet("/Inbox_notification_templateServlet")
//@MultipartConfig
//public class Inbox_notification_templateServlet extends HttpServlet 
//{
//	private static final long serialVersionUID = 1L;
//    public static Logger logger = Logger.getLogger(Inbox_notification_templateServlet.class);
//	Approval_module_mapDAO approval_module_mapDAO = new Approval_module_mapDAO();
//    Approval_module_mapDTO approval_module_mapDTO;
//    String tableName = "inbox_notification_template";
//    String tempTableName = "inbox_notification_template_temp";
//	Inbox_notification_templateDAO inbox_notification_templateDAO;
//    private Gson gson = new Gson();
//    
//    /**
//     * @see HttpServlet#HttpServlet()
//     */
//    public Inbox_notification_templateServlet() 
//	{
//        super();
//    	try
//    	{
//			approval_module_mapDTO = approval_module_mapDAO.getApproval_module_mapDTOByTableName("inbox_notification_template");
//			inbox_notification_templateDAO = new Inbox_notification_templateDAO(tableName, tempTableName, approval_module_mapDTO);
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//    }   
//
//	/**
//	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
//	 */
//	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
//	{
//		System.out.println("In doget request = " + request);
//		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
//		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
//		boolean isPermanentTable = true;
//		if(request.getParameter("isPermanentTable") != null)
//		{
//			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
//		}
//		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
//		try
//		{
//			String actionType = request.getParameter("actionType");
//			if(actionType.equals("getAddPage"))
//			{
//				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.INBOX_NOTIFICATION_TEMPLATE_ADD))
//				{
//					getAddPage(request, response);
//				}
//				else
//				{
//					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
//				}
//			}
//			else if(actionType.equals("getEditPage"))
//			{
//				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.INBOX_NOTIFICATION_TEMPLATE_UPDATE))
//				{
//					if(isPermanentTable)
//					{
//						getInbox_notification_template(request, response, tableName);
//					}
//					else
//					{
//						getInbox_notification_template(request, response, tempTableName);
//					}
//				}
//				else
//				{
//					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
//				}						
//			}
//			else if(actionType.equals("getURL"))
//			{
//				String URL = request.getParameter("URL");
//				System.out.println("URL = " + URL);
//				response.sendRedirect(URL);			
//			}
//			else if(actionType.equals("search"))
//			{
//				System.out.println("search requested");
//				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.INBOX_NOTIFICATION_TEMPLATE_SEARCH))
//				{
//					
//					if(isPermanentTable)
//					{
//						searchInbox_notification_template(request, response, tableName, isPermanentTable);
//					}
//					else
//					{
//						searchInbox_notification_template(request, response, tempTableName, isPermanentTable);
//					}
//				}			
//			}
//			else if(actionType.equals("getApprovalPage"))
//			{
//				System.out.println("getApprovalPage requested");
//				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.INBOX_NOTIFICATION_TEMPLATE_SEARCH))
//				{
//					searchInbox_notification_template(request, response, tempTableName, false);
//				}
//				else
//				{
//					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
//				}				
//			}
//			else
//			{
//				request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
//			}
//		}
//		catch(Exception ex)
//		{
//			ex.printStackTrace();
//			logger.debug(ex);
//		}
//	}
//
//	private void getAddPage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
//	{
//		request.setAttribute("ID", -1L);
//		RequestDispatcher requestDispatcher = request.getRequestDispatcher("inbox_notification_template/inbox_notification_templateEdit.jsp");
//		requestDispatcher.forward(request, response);
//	}
//	private String getFileName(final Part part) 
//	{
//	    final String partHeader = part.getHeader("content-disposition");
//	    System.out.println("Part Header = {0}" +  partHeader);
//	    for (String content : part.getHeader("content-disposition").split(";")) {
//	        if (content.trim().startsWith("filename")) {
//	            return content.substring(
//	                    content.indexOf('=') + 1).trim().replace("\"", "");
//	        }
//	    }
//	    return null;
//	}
//
//	
//	
//
//	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
//	{
//		// TODO Auto-generated method stub
//		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
//		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
//		System.out.println("doPost");
//		boolean isPermanentTable = true;
//		if(request.getParameter("isPermanentTable") != null)
//		{
//			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
//		}
//		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
//		
//		try
//		{
//			String actionType = request.getParameter("actionType");
//			System.out.println("actionType = " + actionType);
//			if(actionType.equals("add"))
//			{
//				
//				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.INBOX_NOTIFICATION_TEMPLATE_ADD))
//				{
//					System.out.println("going to  addInbox_notification_template ");
//					addInbox_notification_template(request, response, true, userDTO, tableName, true);
//				}
//				else
//				{
//					System.out.println("Not going to  addInbox_notification_template ");
//					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
//				}
//				
//			}
//			if(actionType.equals("approve"))
//			{
//				
//				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.INBOX_NOTIFICATION_TEMPLATE_ADD))
//				{					
//					approveInbox_notification_template(request, response, true, userDTO);
//				}
//				else
//				{
//					System.out.println("Not going to  addInbox_notification_template ");
//					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
//				}
//				
//			}
//			if(actionType.equals("getDTO"))
//			{
//				
//				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.INBOX_NOTIFICATION_TEMPLATE_ADD))
//				{
//					if(isPermanentTable)
//					{
//						getDTO(request, response, tableName);
//					}
//					else
//					{
//						getDTO(request, response, tempTableName);
//					}
//				}
//				else
//				{
//					System.out.println("Not going to  addInbox_notification_template ");
//					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
//				}
//				
//			}
//			else if(actionType.equals("edit"))
//			{
//				
//				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.INBOX_NOTIFICATION_TEMPLATE_UPDATE))
//				{
//					if(isPermanentTable)
//					{
//						addInbox_notification_template(request, response, false, userDTO, tableName, isPermanentTable);
//					}
//					else
//					{
//						addInbox_notification_template(request, response, false, userDTO, tempTableName, isPermanentTable);
//					}
//				}
//				else
//				{
//					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
//				}
//			}
//			else if(actionType.equals("delete"))
//			{								
//				deleteInbox_notification_template(request, response, userDTO, isPermanentTable);				
//			}	
//			else if(actionType.equals("search"))
//			{
//				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.INBOX_NOTIFICATION_TEMPLATE_SEARCH))
//				{
//					searchInbox_notification_template(request, response, tableName, true);
//				}
//				else
//				{
//					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
//				}
//			}
//			else if(actionType.equals("getGeo"))
//			{
//				System.out.println("going to geoloc ");
//				request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
//			}
//			
//		}
//		catch(Exception ex)
//		{
//			ex.printStackTrace();
//			logger.debug(ex);
//		}
//	}
//	
//	private void getDTO(HttpServletRequest request, HttpServletResponse response, String tableName) 
//	{
//		try 
//		{
//			System.out.println("In getDTO");
//			Inbox_notification_templateDTO inbox_notification_templateDTO = (Inbox_notification_templateDTO)inbox_notification_templateDAO.getDTOByID(Long.parseLong(request.getParameter("ID")), tableName);
//			PrintWriter out = response.getWriter();
//			response.setContentType("application/json");
//			response.setCharacterEncoding("UTF-8");
//			
//			String encoded = this.gson.toJson(inbox_notification_templateDTO);
//			System.out.println("json encoded data = " + encoded);
//			out.print(encoded);
//			out.flush();
//		}
//		catch (NumberFormatException e) 
//		{
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		
//	}
//	private void approveInbox_notification_template(HttpServletRequest request, HttpServletResponse response, boolean b, UserDTO userDTO) 
//	{
//		try
//		{
//			long id = Long.parseLong(request.getParameter("idToApprove"));
//			Inbox_notification_templateDTO inbox_notification_templateDTO = (Inbox_notification_templateDTO)inbox_notification_templateDAO.getDTOByID(id, tempTableName);
//			inbox_notification_templateDAO.manageWriteOperations(inbox_notification_templateDTO, SessionConstants.APPROVE, id, userDTO);
//		}
//		catch (Exception e) 
//		{
//			e.printStackTrace();
//		}
//		
//	}
//	private void addInbox_notification_template(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, String tableName, boolean isPermanentTable) throws IOException 
//	{
//		// TODO Auto-generated method stub
//		try 
//		{
//			request.setAttribute("failureMessage", "");
//			System.out.println("%%%% addInbox_notification_template");
//			String path = getServletContext().getRealPath("/img2/");
//			Inbox_notification_templateDTO inbox_notification_templateDTO;
//			String FileNamePrefix;
//			if(addFlag == true)
//			{
//				inbox_notification_templateDTO = new Inbox_notification_templateDTO();
//				FileNamePrefix = UUID.randomUUID().toString().substring(0, 10);
//			}
//			else
//			{
//				inbox_notification_templateDTO = (Inbox_notification_templateDTO)inbox_notification_templateDAO.getDTOByID(Long.parseLong(request.getParameter("identity")), tableName);
//				FileNamePrefix = request.getParameter("identity");
//			}
//			
//			String Value = "";
//			Value = request.getParameter("inboxActionRoleId");
//			if(Value != null)
//			{
//				Value = Jsoup.clean(Value,Whitelist.simpleText());
//			}
//			System.out.println("inboxActionRoleId = " + Value);
//			if(Value != null && !Value.equalsIgnoreCase(""))
//			{
//				inbox_notification_templateDTO.inboxActionRoleId = Long.parseLong(Value);
//			}
//			else
//			{
//				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//			}
//			Value = request.getParameter("templateName");
//			if(Value != null)
//			{
//				Value = Jsoup.clean(Value,Whitelist.simpleText());
//			}
//			System.out.println("templateName = " + Value);
//			if(Value != null && !Value.equalsIgnoreCase(""))
//			{
//				inbox_notification_templateDTO.templateName = (Value);
//			}
//			else
//			{
//				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//			}
//			Value = request.getParameter("subjectLanguageId");
//			if(Value != null)
//			{
//				Value = Jsoup.clean(Value,Whitelist.simpleText());
//			}
//			System.out.println("subjectLanguageId = " + Value);
//			if(Value != null && !Value.equalsIgnoreCase(""))
//			{
//				inbox_notification_templateDTO.subjectLanguageId = Long.parseLong(Value);
//			}
//			else
//			{
//				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//			}
//			Value = request.getParameter("bodyEn");
//			if(Value != null)
//			{
//				Value = Jsoup.clean(Value,Whitelist.simpleText());
//			}
//			System.out.println("bodyEn = " + Value);
//			if(Value != null && !Value.equalsIgnoreCase(""))
//			{
//				inbox_notification_templateDTO.bodyEn = (Value);
//			}
//			else
//			{
//				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//			}
//			Value = request.getParameter("bodyBn");
//			if(Value != null)
//			{
//				Value = Jsoup.clean(Value,Whitelist.simpleText());
//			}
//			System.out.println("bodyBn = " + Value);
//			if(Value != null && !Value.equalsIgnoreCase(""))
//			{
//				inbox_notification_templateDTO.bodyBn = (Value);
//			}
//			else
//			{
//				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//			}
//			Value = request.getParameter("status");
//			if(Value != null)
//			{
//				Value = Jsoup.clean(Value,Whitelist.simpleText());
//			}
//			System.out.println("status = " + Value);
//			if(Value != null && !Value.equalsIgnoreCase(""))
//			{
//				inbox_notification_templateDTO.status = Boolean.parseBoolean(Value);
//			}
//			else
//			{
//				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//			}
//			Value = request.getParameter("inboxNotificationTypes");
//			if(Value != null)
//			{
//				Value = Jsoup.clean(Value,Whitelist.simpleText());
//			}
//			System.out.println("inboxNotificationTypes = " + Value);
//			if(Value != null && !Value.equalsIgnoreCase(""))
//			{
//				inbox_notification_templateDTO.inboxNotificationTypes = Integer.parseInt(Value);
//			}
//			else
//			{
//				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//			}
//			
//			System.out.println("Done adding  addInbox_notification_template dto = " + inbox_notification_templateDTO);
//			
//			if(addFlag == true)
//			{
//				inbox_notification_templateDAO.manageWriteOperations(inbox_notification_templateDTO, SessionConstants.INSERT, -1, userDTO);
//			}
//			else
//			{
//				if(isPermanentTable)
//				{
//					inbox_notification_templateDAO.manageWriteOperations(inbox_notification_templateDTO, SessionConstants.UPDATE, -1, userDTO);
//				}
//				else
//				{
//					inbox_notification_templateDAO.manageWriteOperations(inbox_notification_templateDTO, SessionConstants.VALIDATE, -1, userDTO);
//				}
//				
//			}
//			
//			
//			
//			
//			
//			
//			
//			
//			String inPlaceSubmit = (String)request.getParameter("inplacesubmit");
//			
//			if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
//			{
//				getInbox_notification_template(request, response, tableName);
//			}
//			else
//			{
//				response.sendRedirect("Inbox_notification_templateServlet?actionType=search");
//			}
//					
//		}
//		catch (Exception e) 
//		{
//			e.printStackTrace();
//		}
//	}
//	
//	
//
//
//
//	
//	
//	
//
//	private void deleteInbox_notification_template(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO, boolean deleteOrReject) throws IOException 
//	{				
//		try 
//		{
//			String[] IDsToDelete = request.getParameterValues("ID");
//			for(int i = 0; i < IDsToDelete.length; i ++)
//			{
//				long id = Long.parseLong(IDsToDelete[i]);
//				System.out.println("------ DELETING " + IDsToDelete[i]);
//				
//				if(deleteOrReject)
//				{
//					Inbox_notification_templateDTO inbox_notification_templateDTO = (Inbox_notification_templateDTO)inbox_notification_templateDAO.getDTOByID(id);
//					inbox_notification_templateDAO.manageWriteOperations(inbox_notification_templateDTO, SessionConstants.DELETE, id, userDTO);
//					response.sendRedirect("Inbox_notification_templateServlet?actionType=search");
//				}
//				else
//				{
//					Inbox_notification_templateDTO inbox_notification_templateDTO = (Inbox_notification_templateDTO)inbox_notification_templateDAO.getDTOByID(id, tempTableName);
//					inbox_notification_templateDAO.manageWriteOperations(inbox_notification_templateDTO, SessionConstants.REJECT, id, userDTO);
//					response.sendRedirect("Inbox_notification_templateServlet?actionType=getApprovalPage");
//				}
//			}			
//		}
//		catch (Exception ex) 
//		{
//			ex.printStackTrace();
//		}
//		
//	}
//
//	private void getInbox_notification_template(HttpServletRequest request, HttpServletResponse response, String tableName) throws ServletException, IOException
//	{
//		System.out.println("in getInbox_notification_template");
//		Inbox_notification_templateDTO inbox_notification_templateDTO = null;
//		try 
//		{
//			inbox_notification_templateDTO = (Inbox_notification_templateDTO)inbox_notification_templateDAO.getDTOByID(Long.parseLong(request.getParameter("ID")), tableName);
//			request.setAttribute("ID", inbox_notification_templateDTO.iD);
//			request.setAttribute("inbox_notification_templateDTO",inbox_notification_templateDTO);
//			request.setAttribute("inbox_notification_templateDAO",inbox_notification_templateDAO);
//			
//			String URL= "";
//			
//			String inPlaceEdit = (String)request.getParameter("inplaceedit");
//			String inPlaceSubmit = (String)request.getParameter("inplacesubmit");
//			String getBodyOnly = (String)request.getParameter("getBodyOnly");
//			
//			if(inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase(""))
//			{
//				URL = "inbox_notification_template/inbox_notification_templateInPlaceEdit.jsp";	
//				request.setAttribute("inplaceedit","");				
//			}
//			else if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
//			{
//				URL = "inbox_notification_template/inbox_notification_templateSearchRow.jsp";
//				request.setAttribute("inplacesubmit","");					
//			}
//			else
//			{
//				if(getBodyOnly  != null && !getBodyOnly.equalsIgnoreCase(""))
//				{
//					URL = "inbox_notification_template/inbox_notification_templateEditBody.jsp?actionType=edit";
//				}
//				else
//				{
//					URL = "inbox_notification_template/inbox_notification_templateEdit.jsp?actionType=edit";
//				}				
//			}
//			
//			RequestDispatcher rd = request.getRequestDispatcher(URL);
//			rd.forward(request, response);
//		}
//		catch (NumberFormatException e) 
//		{
//			e.printStackTrace();
//		}
//		catch (Exception e) 
//		{
//			e.printStackTrace();
//		}
//	}
//	
//	private void searchInbox_notification_template(HttpServletRequest request, HttpServletResponse response, String tableName, boolean isPermanent) throws ServletException, IOException
//	{
//		System.out.println("in  searchInbox_notification_template 1");
//		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
//		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
//		String ajax = (String)request.getParameter("ajax");
//		boolean hasAjax = false;
//		if(ajax != null && !ajax.equalsIgnoreCase(""))
//		{
//			hasAjax = true;
//		}
//		System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);
//		
//        RecordNavigationManager3 rnManager = new RecordNavigationManager3(
//			SessionConstants.NAV_INBOX_NOTIFICATION_TEMPLATE,
//			request,
//			inbox_notification_templateDAO,
//			SessionConstants.VIEW_INBOX_NOTIFICATION_TEMPLATE,
//			SessionConstants.SEARCH_INBOX_NOTIFICATION_TEMPLATE,
//			tableName,
//			isPermanent,
//			userDTO.approvalPathID);
//        try
//        {
//			System.out.println("trying to dojob");
//            rnManager.doJob(loginDTO);
//        }
//        catch(Exception e)
//        {
//			System.out.println("failed to dojob" + e);
//        }
//
//		request.setAttribute("inbox_notification_templateDAO",inbox_notification_templateDAO);
//        RequestDispatcher rd;
//        if(hasAjax == false)
//        {
//        	System.out.println("Going to inbox_notification_template/inbox_notification_templateSearch.jsp");
//        	rd = request.getRequestDispatcher("inbox_notification_template/inbox_notification_templateSearch.jsp");
//        }
//        else
//        {
//        	System.out.println("Going to inbox_notification_template/inbox_notification_templateSearchForm.jsp");
//        	rd = request.getRequestDispatcher("inbox_notification_template/inbox_notification_templateSearchForm.jsp");
//        }
//		rd.forward(request, response);
//	}
//	
//}
//
