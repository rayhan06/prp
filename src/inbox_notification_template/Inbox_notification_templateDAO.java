package inbox_notification_template;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;


import org.apache.log4j.Logger;

import dbm.*;

import repository.RepositoryManager;

import util.*;
import pb.*;
import approval_module_map.*;

public class Inbox_notification_templateDAO  extends NavigationService3
{
	
	Logger logger = Logger.getLogger(getClass());

	
	public Inbox_notification_templateDAO(String tableName, String tempTableName, Approval_module_mapDTO approval_module_mapDTO)
	{
		super(tableName, tempTableName, approval_module_mapDTO);		
	}
	
	
	
	public long add(CommonDTO commonDTO, String tableName, TempTableDTO tempTableDTO) throws Exception
	{
		
		Inbox_notification_templateDTO inbox_notification_templateDTO = (Inbox_notification_templateDTO)commonDTO;
		
		Connection connection = null;
		PreparedStatement ps = null;

		long lastModificationTime = System.currentTimeMillis();	

		try{
			connection = DBMW.getInstance().getConnection();
			
			if(connection == null)
			{
				System.out.println("nullconn");
			}

			inbox_notification_templateDTO.iD = DBMW.getInstance().getNextSequenceId(tableName);

			String sql = "INSERT INTO " + tableName;
			
			sql += " (";
			sql += "ID";
			sql += ", ";
			sql += "inbox_action_role_id";
			sql += ", ";
			sql += "template_name";
			sql += ", ";
			sql += "subject_language_id";
			sql += ", ";
			sql += "body_en";
			sql += ", ";
			sql += "body_bn";
			sql += ", ";
			sql += "status";
			sql += ", ";
			sql += "inbox_notification_types";
			sql += ", ";
			sql += "isDeleted";
			sql += ", ";
			sql += "lastModificationTime";
			if(tempTableDTO!=null)
			{
				sql += ", permanent_table_id, operation_type, approval_path_type, approval_order";
			}
			sql += ")";
			
			
            sql += " VALUES(";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			if(tempTableDTO!=null)
			{
				sql += ", " + tempTableDTO.permanent_table_id + ", " + tempTableDTO.operation_type + ", " + tempTableDTO.approval_path_type + ", " + tempTableDTO.approval_order;
			}
			sql += ")";
				
			printSql(sql);

			ps = connection.prepareStatement(sql);
			
			
			

			int index = 1;

			ps.setObject(index++,inbox_notification_templateDTO.iD);
			ps.setObject(index++,inbox_notification_templateDTO.inboxActionRoleId);
			ps.setObject(index++,inbox_notification_templateDTO.templateName);
			ps.setObject(index++,inbox_notification_templateDTO.subjectLanguageId);
			ps.setObject(index++,inbox_notification_templateDTO.bodyEn);
			ps.setObject(index++,inbox_notification_templateDTO.bodyBn);
			ps.setObject(index++,inbox_notification_templateDTO.status);
			ps.setObject(index++,inbox_notification_templateDTO.inboxNotificationTypes);
			ps.setObject(index++,inbox_notification_templateDTO.isDeleted);
			ps.setObject(index++, lastModificationTime);
			
			System.out.println(ps);
			ps.execute();
			
			
			recordUpdateTime(connection, lastModificationTime, tableName);

		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if (ps != null) {
					ps.close();
				}
			} catch(Exception e){}
			try{
				if(connection != null){
					DBMW.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		return inbox_notification_templateDTO.iD;		
	}
		
	

	//need another getter for repository
	public CommonDTO getDTOByID (long ID, String tableName) throws Exception
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Inbox_notification_templateDTO inbox_notification_templateDTO = null;
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
			
            sql += " WHERE ID=" + ID;
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			if(rs.next()){
				inbox_notification_templateDTO = new Inbox_notification_templateDTO();

				inbox_notification_templateDTO.iD = rs.getLong("ID");
				inbox_notification_templateDTO.inboxActionRoleId = rs.getLong("inbox_action_role_id");
				inbox_notification_templateDTO.templateName = rs.getString("template_name");
				inbox_notification_templateDTO.subjectLanguageId = rs.getLong("subject_language_id");
				inbox_notification_templateDTO.bodyEn = rs.getString("body_en");
				inbox_notification_templateDTO.bodyBn = rs.getString("body_bn");
				inbox_notification_templateDTO.status = rs.getBoolean("status");
				inbox_notification_templateDTO.inboxNotificationTypes = rs.getInt("inbox_notification_types");
				inbox_notification_templateDTO.isDeleted = rs.getInt("isDeleted");
				inbox_notification_templateDTO.lastModificationTime = rs.getLong("lastModificationTime");

			}			
			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DBMR.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
		return inbox_notification_templateDTO;
	}
	
	public long update(CommonDTO commonDTO, String tableName) throws Exception
	{		
		Inbox_notification_templateDTO inbox_notification_templateDTO = (Inbox_notification_templateDTO)commonDTO;
		
		Connection connection = null;
		PreparedStatement ps = null;

		long lastModificationTime = System.currentTimeMillis();	
		try{
			connection = DBMW.getInstance().getConnection();

			String sql = "UPDATE " + tableName;
			
			sql += " SET ";
			sql += "inbox_action_role_id=?";
			sql += ", ";
			sql += "template_name=?";
			sql += ", ";
			sql += "subject_language_id=?";
			sql += ", ";
			sql += "body_en=?";
			sql += ", ";
			sql += "body_bn=?";
			sql += ", ";
			sql += "status=?";
			sql += ", ";
			sql += "inbox_notification_types=?";
			sql += ", lastModificationTime = "	+ lastModificationTime + "";
            sql += " WHERE ID = " + inbox_notification_templateDTO.iD;
				
			printSql(sql);

			ps = connection.prepareStatement(sql);
			
			

			int index = 1;
			
			ps.setObject(index++,inbox_notification_templateDTO.inboxActionRoleId);
			ps.setObject(index++,inbox_notification_templateDTO.templateName);
			ps.setObject(index++,inbox_notification_templateDTO.subjectLanguageId);
			ps.setObject(index++,inbox_notification_templateDTO.bodyEn);
			ps.setObject(index++,inbox_notification_templateDTO.bodyBn);
			ps.setObject(index++,inbox_notification_templateDTO.status);
			ps.setObject(index++,inbox_notification_templateDTO.inboxNotificationTypes);
			System.out.println(ps);
			ps.executeUpdate();
			


			
			recordUpdateTime(connection,  lastModificationTime, tableName);

						
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if (ps != null) {
					ps.close();
				}
			} catch(Exception e){}
			try{
				if(connection != null){
					DBMW.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		return inbox_notification_templateDTO.iD;
	}
	
	public List<Inbox_notification_templateDTO> getDTOs(Collection recordIDs)
	{
		return getDTOs(recordIDs, tableName);
	}
	
	public List<Inbox_notification_templateDTO> getDTOs(Collection recordIDs, String tableName){
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Inbox_notification_templateDTO inbox_notification_templateDTO = null;
		List<Inbox_notification_templateDTO> inbox_notification_templateDTOList = new ArrayList<>();
		if(recordIDs.isEmpty()){
			return inbox_notification_templateDTOList;
		}
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
            
            sql += " WHERE ID IN ( ";

			for(int i = 0;i<recordIDs.size();i++){
				if(i!=0){
					sql+=",";
				}
				sql+=((ArrayList)recordIDs).get(i);
			}
			sql+=")  order by lastModificationTime desc";
			
			printSql(sql);
			
			logger.debug("sql " + sql);
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				inbox_notification_templateDTO = new Inbox_notification_templateDTO();
				inbox_notification_templateDTO.iD = rs.getLong("ID");
				inbox_notification_templateDTO.inboxActionRoleId = rs.getLong("inbox_action_role_id");
				inbox_notification_templateDTO.templateName = rs.getString("template_name");
				inbox_notification_templateDTO.subjectLanguageId = rs.getLong("subject_language_id");
				inbox_notification_templateDTO.bodyEn = rs.getString("body_en");
				inbox_notification_templateDTO.bodyBn = rs.getString("body_bn");
				inbox_notification_templateDTO.status = rs.getBoolean("status");
				inbox_notification_templateDTO.inboxNotificationTypes = rs.getInt("inbox_notification_types");
				inbox_notification_templateDTO.isDeleted = rs.getInt("isDeleted");
				inbox_notification_templateDTO.lastModificationTime = rs.getLong("lastModificationTime");
				System.out.println("got this DTO: " + inbox_notification_templateDTO);
				
				inbox_notification_templateDTOList.add(inbox_notification_templateDTO);

			}			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DBMR.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
		return inbox_notification_templateDTOList;
	
	}
	
	

	
	
	
	//add repository
	public List<Inbox_notification_templateDTO> getAllInbox_notification_template (boolean isFirstReload)
    {
		List<Inbox_notification_templateDTO> inbox_notification_templateDTOList = new ArrayList<>();

		String sql = "SELECT * FROM inbox_notification_template";
		sql += " WHERE ";
	

		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by inbox_notification_template.lastModificationTime desc";
		printSql(sql);
		
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;

		
		try{
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();
			rs = stmt.executeQuery(sql);
			

			while(rs.next()){
				Inbox_notification_templateDTO inbox_notification_templateDTO = new Inbox_notification_templateDTO();
				inbox_notification_templateDTO.iD = rs.getLong("ID");
				inbox_notification_templateDTO.inboxActionRoleId = rs.getLong("inbox_action_role_id");
				inbox_notification_templateDTO.templateName = rs.getString("template_name");
				inbox_notification_templateDTO.subjectLanguageId = rs.getLong("subject_language_id");
				inbox_notification_templateDTO.bodyEn = rs.getString("body_en");
				inbox_notification_templateDTO.bodyBn = rs.getString("body_bn");
				inbox_notification_templateDTO.status = rs.getBoolean("status");
				inbox_notification_templateDTO.inboxNotificationTypes = rs.getInt("inbox_notification_types");
				inbox_notification_templateDTO.isDeleted = rs.getInt("isDeleted");
				inbox_notification_templateDTO.lastModificationTime = rs.getLong("lastModificationTime");
				
				inbox_notification_templateDTOList.add(inbox_notification_templateDTO);
			}			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			try{ 
				if (connection != null){ 
					DBMR.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}

		return inbox_notification_templateDTOList;
    }
	
	public List<Inbox_notification_templateDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset)
	{
		return getDTOs(p_searchCriteria, limit, offset, tableName, true, 0);
	}
	
	public List<Inbox_notification_templateDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, String tableName, boolean isPermanentTable, long userApprovalPathType)
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		List<Inbox_notification_templateDTO> inbox_notification_templateDTOList = new ArrayList<>();

		try{
			
			String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, tableName, isPermanentTable, userApprovalPathType);
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				Inbox_notification_templateDTO inbox_notification_templateDTO = new Inbox_notification_templateDTO();
				inbox_notification_templateDTO.iD = rs.getLong("ID");
				inbox_notification_templateDTO.inboxActionRoleId = rs.getLong("inbox_action_role_id");
				inbox_notification_templateDTO.templateName = rs.getString("template_name");
				inbox_notification_templateDTO.subjectLanguageId = rs.getLong("subject_language_id");
				inbox_notification_templateDTO.bodyEn = rs.getString("body_en");
				inbox_notification_templateDTO.bodyBn = rs.getString("body_bn");
				inbox_notification_templateDTO.status = rs.getBoolean("status");
				inbox_notification_templateDTO.inboxNotificationTypes = rs.getInt("inbox_notification_types");
				inbox_notification_templateDTO.isDeleted = rs.getInt("isDeleted");
				inbox_notification_templateDTO.lastModificationTime = rs.getLong("lastModificationTime");
				
				inbox_notification_templateDTOList.add(inbox_notification_templateDTO);
			}					
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DBMR.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
		return inbox_notification_templateDTOList;
	
	}

		
	
	public String getSqlWithSearchCriteria(Hashtable p_searchCriteria, int limit, int offset, int category, String tableName, boolean isPermanentTable, long userApprovalPathType)
    {
		Inbox_notification_templateMAPS maps = new Inbox_notification_templateMAPS(tableName);
		String joinSQL = "";
		return getSqlWithSearchCriteria(p_searchCriteria, limit, offset, category, tableName, isPermanentTable, userApprovalPathType, maps, joinSQL);
    }			
}
	