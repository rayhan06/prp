package inbox_notification_template;
import java.util.*; 
import util.*;


public class Inbox_notification_templateMAPS extends CommonMaps
{	
	public Inbox_notification_templateMAPS(String tableName)
	{
		
		java_allfield_type_map.put("inbox_action_role_id".toLowerCase(), "Long");
		java_allfield_type_map.put("template_name".toLowerCase(), "String");
		java_allfield_type_map.put("subject_language_id".toLowerCase(), "Long");
		java_allfield_type_map.put("body_en".toLowerCase(), "String");
		java_allfield_type_map.put("body_bn".toLowerCase(), "String");
		java_allfield_type_map.put("status".toLowerCase(), "Boolean");
		java_allfield_type_map.put("inbox_notification_types".toLowerCase(), "Integer");

		java_anyfield_search_map.put(tableName + ".inbox_action_role_id".toLowerCase(), "Long");
		java_anyfield_search_map.put(tableName + ".template_name".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".subject_language_id".toLowerCase(), "Long");
		java_anyfield_search_map.put(tableName + ".body_en".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".body_bn".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".status".toLowerCase(), "Boolean");
		java_anyfield_search_map.put(tableName + ".inbox_notification_types".toLowerCase(), "Integer");

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("inboxActionRoleId".toLowerCase(), "inboxActionRoleId".toLowerCase());
		java_DTO_map.put("templateName".toLowerCase(), "templateName".toLowerCase());
		java_DTO_map.put("subjectLanguageId".toLowerCase(), "subjectLanguageId".toLowerCase());
		java_DTO_map.put("bodyEn".toLowerCase(), "bodyEn".toLowerCase());
		java_DTO_map.put("bodyBn".toLowerCase(), "bodyBn".toLowerCase());
		java_DTO_map.put("status".toLowerCase(), "status".toLowerCase());
		java_DTO_map.put("inboxNotificationTypes".toLowerCase(), "inboxNotificationTypes".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("inbox_action_role_id".toLowerCase(), "inboxActionRoleId".toLowerCase());
		java_SQL_map.put("template_name".toLowerCase(), "templateName".toLowerCase());
		java_SQL_map.put("subject_language_id".toLowerCase(), "subjectLanguageId".toLowerCase());
		java_SQL_map.put("body_en".toLowerCase(), "bodyEn".toLowerCase());
		java_SQL_map.put("body_bn".toLowerCase(), "bodyBn".toLowerCase());
		java_SQL_map.put("status".toLowerCase(), "status".toLowerCase());
		java_SQL_map.put("inbox_notification_types".toLowerCase(), "inboxNotificationTypes".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Inbox Action Role Id".toLowerCase(), "inboxActionRoleId".toLowerCase());
		java_Text_map.put("Template Name".toLowerCase(), "templateName".toLowerCase());
		java_Text_map.put("Subject Language Id".toLowerCase(), "subjectLanguageId".toLowerCase());
		java_Text_map.put("Body En".toLowerCase(), "bodyEn".toLowerCase());
		java_Text_map.put("Body Bn".toLowerCase(), "bodyBn".toLowerCase());
		java_Text_map.put("Status".toLowerCase(), "status".toLowerCase());
		java_Text_map.put("Inbox Notification Types".toLowerCase(), "inboxNotificationTypes".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}