package inbox_notification_template;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class Inbox_notification_templateRepository implements Repository {
	Inbox_notification_templateDAO inbox_notification_templateDAO = null;
	
	public void setDAO(Inbox_notification_templateDAO inbox_notification_templateDAO)
	{
		this.inbox_notification_templateDAO = inbox_notification_templateDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Inbox_notification_templateRepository.class);
	Map<Long, Inbox_notification_templateDTO>mapOfInbox_notification_templateDTOToiD;
	Map<Long, Set<Inbox_notification_templateDTO> >mapOfInbox_notification_templateDTOToinboxActionRoleId;
	Map<String, Set<Inbox_notification_templateDTO> >mapOfInbox_notification_templateDTOTotemplateName;
	Map<Long, Set<Inbox_notification_templateDTO> >mapOfInbox_notification_templateDTOTosubjectLanguageId;
	Map<String, Set<Inbox_notification_templateDTO> >mapOfInbox_notification_templateDTOTobodyEn;
	Map<String, Set<Inbox_notification_templateDTO> >mapOfInbox_notification_templateDTOTobodyBn;
	Map<Boolean, Set<Inbox_notification_templateDTO> >mapOfInbox_notification_templateDTOTostatus;
	Map<Integer, Set<Inbox_notification_templateDTO> >mapOfInbox_notification_templateDTOToinboxNotificationTypes;
	Map<Long, Set<Inbox_notification_templateDTO> >mapOfInbox_notification_templateDTOTolastModificationTime;


	static Inbox_notification_templateRepository instance = null;  
	private Inbox_notification_templateRepository(){
		mapOfInbox_notification_templateDTOToiD = new ConcurrentHashMap<>();
		mapOfInbox_notification_templateDTOToinboxActionRoleId = new ConcurrentHashMap<>();
		mapOfInbox_notification_templateDTOTotemplateName = new ConcurrentHashMap<>();
		mapOfInbox_notification_templateDTOTosubjectLanguageId = new ConcurrentHashMap<>();
		mapOfInbox_notification_templateDTOTobodyEn = new ConcurrentHashMap<>();
		mapOfInbox_notification_templateDTOTobodyBn = new ConcurrentHashMap<>();
		mapOfInbox_notification_templateDTOTostatus = new ConcurrentHashMap<>();
		mapOfInbox_notification_templateDTOToinboxNotificationTypes = new ConcurrentHashMap<>();
		mapOfInbox_notification_templateDTOTolastModificationTime = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Inbox_notification_templateRepository getInstance(){
		if (instance == null){
			instance = new Inbox_notification_templateRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(inbox_notification_templateDAO == null)
		{
			return;
		}
		try {
			List<Inbox_notification_templateDTO> inbox_notification_templateDTOs = inbox_notification_templateDAO.getAllInbox_notification_template(reloadAll);
			for(Inbox_notification_templateDTO inbox_notification_templateDTO : inbox_notification_templateDTOs) {
				Inbox_notification_templateDTO oldInbox_notification_templateDTO = getInbox_notification_templateDTOByID(inbox_notification_templateDTO.iD);
				if( oldInbox_notification_templateDTO != null ) {
					mapOfInbox_notification_templateDTOToiD.remove(oldInbox_notification_templateDTO.iD);
				
					if(mapOfInbox_notification_templateDTOToinboxActionRoleId.containsKey(oldInbox_notification_templateDTO.inboxActionRoleId)) {
						mapOfInbox_notification_templateDTOToinboxActionRoleId.get(oldInbox_notification_templateDTO.inboxActionRoleId).remove(oldInbox_notification_templateDTO);
					}
					if(mapOfInbox_notification_templateDTOToinboxActionRoleId.get(oldInbox_notification_templateDTO.inboxActionRoleId).isEmpty()) {
						mapOfInbox_notification_templateDTOToinboxActionRoleId.remove(oldInbox_notification_templateDTO.inboxActionRoleId);
					}
					
					if(mapOfInbox_notification_templateDTOTotemplateName.containsKey(oldInbox_notification_templateDTO.templateName)) {
						mapOfInbox_notification_templateDTOTotemplateName.get(oldInbox_notification_templateDTO.templateName).remove(oldInbox_notification_templateDTO);
					}
					if(mapOfInbox_notification_templateDTOTotemplateName.get(oldInbox_notification_templateDTO.templateName).isEmpty()) {
						mapOfInbox_notification_templateDTOTotemplateName.remove(oldInbox_notification_templateDTO.templateName);
					}
					
					if(mapOfInbox_notification_templateDTOTosubjectLanguageId.containsKey(oldInbox_notification_templateDTO.subjectLanguageId)) {
						mapOfInbox_notification_templateDTOTosubjectLanguageId.get(oldInbox_notification_templateDTO.subjectLanguageId).remove(oldInbox_notification_templateDTO);
					}
					if(mapOfInbox_notification_templateDTOTosubjectLanguageId.get(oldInbox_notification_templateDTO.subjectLanguageId).isEmpty()) {
						mapOfInbox_notification_templateDTOTosubjectLanguageId.remove(oldInbox_notification_templateDTO.subjectLanguageId);
					}
					
					if(mapOfInbox_notification_templateDTOTobodyEn.containsKey(oldInbox_notification_templateDTO.bodyEn)) {
						mapOfInbox_notification_templateDTOTobodyEn.get(oldInbox_notification_templateDTO.bodyEn).remove(oldInbox_notification_templateDTO);
					}
					if(mapOfInbox_notification_templateDTOTobodyEn.get(oldInbox_notification_templateDTO.bodyEn).isEmpty()) {
						mapOfInbox_notification_templateDTOTobodyEn.remove(oldInbox_notification_templateDTO.bodyEn);
					}
					
					if(mapOfInbox_notification_templateDTOTobodyBn.containsKey(oldInbox_notification_templateDTO.bodyBn)) {
						mapOfInbox_notification_templateDTOTobodyBn.get(oldInbox_notification_templateDTO.bodyBn).remove(oldInbox_notification_templateDTO);
					}
					if(mapOfInbox_notification_templateDTOTobodyBn.get(oldInbox_notification_templateDTO.bodyBn).isEmpty()) {
						mapOfInbox_notification_templateDTOTobodyBn.remove(oldInbox_notification_templateDTO.bodyBn);
					}
					
					if(mapOfInbox_notification_templateDTOTostatus.containsKey(oldInbox_notification_templateDTO.status)) {
						mapOfInbox_notification_templateDTOTostatus.get(oldInbox_notification_templateDTO.status).remove(oldInbox_notification_templateDTO);
					}
					if(mapOfInbox_notification_templateDTOTostatus.get(oldInbox_notification_templateDTO.status).isEmpty()) {
						mapOfInbox_notification_templateDTOTostatus.remove(oldInbox_notification_templateDTO.status);
					}
					
					if(mapOfInbox_notification_templateDTOToinboxNotificationTypes.containsKey(oldInbox_notification_templateDTO.inboxNotificationTypes)) {
						mapOfInbox_notification_templateDTOToinboxNotificationTypes.get(oldInbox_notification_templateDTO.inboxNotificationTypes).remove(oldInbox_notification_templateDTO);
					}
					if(mapOfInbox_notification_templateDTOToinboxNotificationTypes.get(oldInbox_notification_templateDTO.inboxNotificationTypes).isEmpty()) {
						mapOfInbox_notification_templateDTOToinboxNotificationTypes.remove(oldInbox_notification_templateDTO.inboxNotificationTypes);
					}
					
					if(mapOfInbox_notification_templateDTOTolastModificationTime.containsKey(oldInbox_notification_templateDTO.lastModificationTime)) {
						mapOfInbox_notification_templateDTOTolastModificationTime.get(oldInbox_notification_templateDTO.lastModificationTime).remove(oldInbox_notification_templateDTO);
					}
					if(mapOfInbox_notification_templateDTOTolastModificationTime.get(oldInbox_notification_templateDTO.lastModificationTime).isEmpty()) {
						mapOfInbox_notification_templateDTOTolastModificationTime.remove(oldInbox_notification_templateDTO.lastModificationTime);
					}
					
					
				}
				if(inbox_notification_templateDTO.isDeleted == 0) 
				{
					
					mapOfInbox_notification_templateDTOToiD.put(inbox_notification_templateDTO.iD, inbox_notification_templateDTO);
				
					if( ! mapOfInbox_notification_templateDTOToinboxActionRoleId.containsKey(inbox_notification_templateDTO.inboxActionRoleId)) {
						mapOfInbox_notification_templateDTOToinboxActionRoleId.put(inbox_notification_templateDTO.inboxActionRoleId, new HashSet<>());
					}
					mapOfInbox_notification_templateDTOToinboxActionRoleId.get(inbox_notification_templateDTO.inboxActionRoleId).add(inbox_notification_templateDTO);
					
					if( ! mapOfInbox_notification_templateDTOTotemplateName.containsKey(inbox_notification_templateDTO.templateName)) {
						mapOfInbox_notification_templateDTOTotemplateName.put(inbox_notification_templateDTO.templateName, new HashSet<>());
					}
					mapOfInbox_notification_templateDTOTotemplateName.get(inbox_notification_templateDTO.templateName).add(inbox_notification_templateDTO);
					
					if( ! mapOfInbox_notification_templateDTOTosubjectLanguageId.containsKey(inbox_notification_templateDTO.subjectLanguageId)) {
						mapOfInbox_notification_templateDTOTosubjectLanguageId.put(inbox_notification_templateDTO.subjectLanguageId, new HashSet<>());
					}
					mapOfInbox_notification_templateDTOTosubjectLanguageId.get(inbox_notification_templateDTO.subjectLanguageId).add(inbox_notification_templateDTO);
					
					if( ! mapOfInbox_notification_templateDTOTobodyEn.containsKey(inbox_notification_templateDTO.bodyEn)) {
						mapOfInbox_notification_templateDTOTobodyEn.put(inbox_notification_templateDTO.bodyEn, new HashSet<>());
					}
					mapOfInbox_notification_templateDTOTobodyEn.get(inbox_notification_templateDTO.bodyEn).add(inbox_notification_templateDTO);
					
					if( ! mapOfInbox_notification_templateDTOTobodyBn.containsKey(inbox_notification_templateDTO.bodyBn)) {
						mapOfInbox_notification_templateDTOTobodyBn.put(inbox_notification_templateDTO.bodyBn, new HashSet<>());
					}
					mapOfInbox_notification_templateDTOTobodyBn.get(inbox_notification_templateDTO.bodyBn).add(inbox_notification_templateDTO);
					
					if( ! mapOfInbox_notification_templateDTOTostatus.containsKey(inbox_notification_templateDTO.status)) {
						mapOfInbox_notification_templateDTOTostatus.put(inbox_notification_templateDTO.status, new HashSet<>());
					}
					mapOfInbox_notification_templateDTOTostatus.get(inbox_notification_templateDTO.status).add(inbox_notification_templateDTO);
					
					if( ! mapOfInbox_notification_templateDTOToinboxNotificationTypes.containsKey(inbox_notification_templateDTO.inboxNotificationTypes)) {
						mapOfInbox_notification_templateDTOToinboxNotificationTypes.put(inbox_notification_templateDTO.inboxNotificationTypes, new HashSet<>());
					}
					mapOfInbox_notification_templateDTOToinboxNotificationTypes.get(inbox_notification_templateDTO.inboxNotificationTypes).add(inbox_notification_templateDTO);
					
					if( ! mapOfInbox_notification_templateDTOTolastModificationTime.containsKey(inbox_notification_templateDTO.lastModificationTime)) {
						mapOfInbox_notification_templateDTOTolastModificationTime.put(inbox_notification_templateDTO.lastModificationTime, new HashSet<>());
					}
					mapOfInbox_notification_templateDTOTolastModificationTime.get(inbox_notification_templateDTO.lastModificationTime).add(inbox_notification_templateDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Inbox_notification_templateDTO> getInbox_notification_templateList() {
		List <Inbox_notification_templateDTO> inbox_notification_templates = new ArrayList<Inbox_notification_templateDTO>(this.mapOfInbox_notification_templateDTOToiD.values());
		return inbox_notification_templates;
	}
	
	
	public Inbox_notification_templateDTO getInbox_notification_templateDTOByID( long ID){
		return mapOfInbox_notification_templateDTOToiD.get(ID);
	}
	
	
	public List<Inbox_notification_templateDTO> getInbox_notification_templateDTOByinbox_action_role_id(long inbox_action_role_id) {
		return new ArrayList<>( mapOfInbox_notification_templateDTOToinboxActionRoleId.getOrDefault(inbox_action_role_id,new HashSet<>()));
	}
	
	
	public List<Inbox_notification_templateDTO> getInbox_notification_templateDTOBytemplate_name(String template_name) {
		return new ArrayList<>( mapOfInbox_notification_templateDTOTotemplateName.getOrDefault(template_name,new HashSet<>()));
	}
	
	
	public List<Inbox_notification_templateDTO> getInbox_notification_templateDTOBysubject_language_id(long subject_language_id) {
		return new ArrayList<>( mapOfInbox_notification_templateDTOTosubjectLanguageId.getOrDefault(subject_language_id,new HashSet<>()));
	}
	
	
	public List<Inbox_notification_templateDTO> getInbox_notification_templateDTOBybody_en(String body_en) {
		return new ArrayList<>( mapOfInbox_notification_templateDTOTobodyEn.getOrDefault(body_en,new HashSet<>()));
	}
	
	
	public List<Inbox_notification_templateDTO> getInbox_notification_templateDTOBybody_bn(String body_bn) {
		return new ArrayList<>( mapOfInbox_notification_templateDTOTobodyBn.getOrDefault(body_bn,new HashSet<>()));
	}
	
	
	public List<Inbox_notification_templateDTO> getInbox_notification_templateDTOBystatus(boolean status) {
		return new ArrayList<>( mapOfInbox_notification_templateDTOTostatus.getOrDefault(status,new HashSet<>()));
	}
	
	
	public List<Inbox_notification_templateDTO> getInbox_notification_templateDTOByinbox_notification_types(int inbox_notification_types) {
		return new ArrayList<>( mapOfInbox_notification_templateDTOToinboxNotificationTypes.getOrDefault(inbox_notification_types,new HashSet<>()));
	}
	
	
	public List<Inbox_notification_templateDTO> getInbox_notification_templateDTOBylastModificationTime(long lastModificationTime) {
		return new ArrayList<>( mapOfInbox_notification_templateDTOTolastModificationTime.getOrDefault(lastModificationTime,new HashSet<>()));
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "inbox_notification_template";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


