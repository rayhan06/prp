package inbox_notification_template;
import java.util.*; 
import util.*; 


public class Inbox_notification_templateDTO extends CommonDTO
{

	public long inboxActionRoleId = 0;
    public String templateName = "";
	public long subjectLanguageId = 0;
    public String bodyEn = "";
    public String bodyBn = "";
	public boolean status = false;
	public int inboxNotificationTypes = 0;
	
	
    @Override
	public String toString() {
            return "$Inbox_notification_templateDTO[" +
            " iD = " + iD +
            " inboxActionRoleId = " + inboxActionRoleId +
            " templateName = " + templateName +
            " subjectLanguageId = " + subjectLanguageId +
            " bodyEn = " + bodyEn +
            " bodyBn = " + bodyBn +
            " status = " + status +
            " inboxNotificationTypes = " + inboxNotificationTypes +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}