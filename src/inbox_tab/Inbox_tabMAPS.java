package inbox_tab;
import java.util.*; 
import util.*;


public class Inbox_tabMAPS extends CommonMaps
{	
	public Inbox_tabMAPS(String tableName)
	{
		
		java_allfield_type_map.put("name_language_id".toLowerCase(), "Long");
		java_allfield_type_map.put("query_tab".toLowerCase(), "String");
		java_allfield_type_map.put("tab_table_header".toLowerCase(), "String");
		java_allfield_type_map.put("details_link".toLowerCase(), "String");
		java_allfield_type_map.put("query_details".toLowerCase(), "String");
		java_allfield_type_map.put("status".toLowerCase(), "Boolean");
		java_allfield_type_map.put("action_column".toLowerCase(), "String");

		java_anyfield_search_map.put(tableName + ".name_language_id".toLowerCase(), "Long");
		java_anyfield_search_map.put(tableName + ".query_tab".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".tab_table_header".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".details_link".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".query_details".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".status".toLowerCase(), "Boolean");
		java_anyfield_search_map.put(tableName + ".action_column".toLowerCase(), "String");

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("nameLanguageId".toLowerCase(), "nameLanguageId".toLowerCase());
		java_DTO_map.put("queryTab".toLowerCase(), "queryTab".toLowerCase());
		java_DTO_map.put("tabTableHeader".toLowerCase(), "tabTableHeader".toLowerCase());
		java_DTO_map.put("detailsLink".toLowerCase(), "detailsLink".toLowerCase());
		java_DTO_map.put("queryDetails".toLowerCase(), "queryDetails".toLowerCase());
		java_DTO_map.put("status".toLowerCase(), "status".toLowerCase());
		java_DTO_map.put("actionColumn".toLowerCase(), "actionColumn".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("name_language_id".toLowerCase(), "nameLanguageId".toLowerCase());
		java_SQL_map.put("query_tab".toLowerCase(), "queryTab".toLowerCase());
		java_SQL_map.put("tab_table_header".toLowerCase(), "tabTableHeader".toLowerCase());
		java_SQL_map.put("details_link".toLowerCase(), "detailsLink".toLowerCase());
		java_SQL_map.put("query_details".toLowerCase(), "queryDetails".toLowerCase());
		java_SQL_map.put("status".toLowerCase(), "status".toLowerCase());
		java_SQL_map.put("action_column".toLowerCase(), "actionColumn".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Name Language Id".toLowerCase(), "nameLanguageId".toLowerCase());
		java_Text_map.put("Query Tab".toLowerCase(), "queryTab".toLowerCase());
		java_Text_map.put("Tab Table Header".toLowerCase(), "tabTableHeader".toLowerCase());
		java_Text_map.put("Details Link".toLowerCase(), "detailsLink".toLowerCase());
		java_Text_map.put("Query Details".toLowerCase(), "queryDetails".toLowerCase());
		java_Text_map.put("Status".toLowerCase(), "status".toLowerCase());
		java_Text_map.put("Action Column".toLowerCase(), "actionColumn".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}