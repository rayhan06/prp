package inbox_tab;
import java.util.*; 
import util.*; 


public class Inbox_tabDTO extends CommonDTO
{

	public long nameLanguageId = 0;
    public String queryTab = "";
    public String tabTableHeader = "";
    public String detailsLink = "";
    public String queryDetails = "";
	public boolean status = false;
	public String querySearch = "";
    //public String actionColumn = "";
	
	
    @Override
	public String toString() {
            return "$Inbox_tabDTO[" +
            " iD = " + iD +
            " nameLanguageId = " + nameLanguageId +
            " queryTab = " + queryTab +
            " tabTableHeader = " + tabTableHeader +
            " detailsLink = " + detailsLink +
            " queryDetails = " + queryDetails +
            " status = " + status +
           // " actionColumn = " + actionColumn +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}