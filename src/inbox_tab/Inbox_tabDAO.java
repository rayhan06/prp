package inbox_tab;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;


import org.apache.log4j.Logger;

import dbm.*;

import repository.RepositoryManager;

import util.*;
import pb.*;
import approval_module_map.*;

public class Inbox_tabDAO  extends NavigationService3
{
	
	Logger logger = Logger.getLogger(getClass());
	
	public Inbox_tabDAO()
	{
		super("inbox_tab", null, null);		
	}


	
	public Inbox_tabDAO(String tableName, String tempTableName, Approval_module_mapDTO approval_module_mapDTO)
	{
		super(tableName, tempTableName, approval_module_mapDTO);		
	}
	
	
	
	public long add(CommonDTO commonDTO, String tableName, TempTableDTO tempTableDTO) throws Exception
	{
		
		Inbox_tabDTO inbox_tabDTO = (Inbox_tabDTO)commonDTO;
		
		Connection connection = null;
		PreparedStatement ps = null;

		long lastModificationTime = System.currentTimeMillis();	

		try{
			connection = DBMW.getInstance().getConnection();
			
			if(connection == null)
			{
				System.out.println("nullconn");
			}

			inbox_tabDTO.iD = DBMW.getInstance().getNextSequenceId(tableName);

			String sql = "INSERT INTO " + tableName;
			
			sql += " (";
			sql += "ID";
			sql += ", ";
			sql += "name_language_id";
			sql += ", ";
			sql += "query_tab";
			sql += ", ";
			sql += "tab_table_header";
			sql += ", ";
			sql += "details_link";
			sql += ", ";
			sql += "query_details";
			sql += ", ";
			sql += "status";
			sql += ", ";
			sql += "query_search";
			sql += ", ";
			sql += "isDeleted";
			sql += ", ";
			sql += "lastModificationTime";
			if(tempTableDTO!=null)
			{
				sql += ", permanent_table_id, operation_type, approval_path_type, approval_order";
			}
			sql += ")";
			
			
            sql += " VALUES(";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			if(tempTableDTO!=null)
			{
				sql += ", " + tempTableDTO.permanent_table_id + ", " + tempTableDTO.operation_type + ", " + tempTableDTO.approval_path_type + ", " + tempTableDTO.approval_order;
			}
			sql += ")";
				
			printSql(sql);

			ps = connection.prepareStatement(sql);
			
			
			

			int index = 1;

			ps.setObject(index++,inbox_tabDTO.iD);
			ps.setObject(index++,inbox_tabDTO.nameLanguageId);
			ps.setObject(index++,inbox_tabDTO.queryTab);
			ps.setObject(index++,inbox_tabDTO.tabTableHeader);
			ps.setObject(index++,inbox_tabDTO.detailsLink);
			ps.setObject(index++,inbox_tabDTO.queryDetails);
			ps.setObject(index++,inbox_tabDTO.status);
			ps.setObject(index++,inbox_tabDTO.querySearch);
			ps.setObject(index++,inbox_tabDTO.isDeleted);
			ps.setObject(index++, lastModificationTime);
			
			System.out.println(ps);
			ps.execute();
			
			
			recordUpdateTime(connection, lastModificationTime, tableName);

		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if (ps != null) {
					ps.close();
				}
			} catch(Exception e){}
			try{
				if(connection != null){
					DBMW.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		return inbox_tabDTO.iD;		
	}
		
	

	//need another getter for repository
	public CommonDTO getDTOByID (long ID, String tableName) throws Exception
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Inbox_tabDTO inbox_tabDTO = null;
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
			
            sql += " WHERE ID=" + ID;
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			if(rs.next()){
				inbox_tabDTO = new Inbox_tabDTO();

				inbox_tabDTO.iD = rs.getLong("ID");
				inbox_tabDTO.nameLanguageId = rs.getLong("name_language_id");
				inbox_tabDTO.queryTab = rs.getString("query_tab");
				inbox_tabDTO.tabTableHeader = rs.getString("tab_table_header");
				inbox_tabDTO.detailsLink = rs.getString("details_link");
				inbox_tabDTO.queryDetails = rs.getString("query_details");
				inbox_tabDTO.status = rs.getBoolean("status");
				inbox_tabDTO.querySearch = rs.getString("query_search");
				inbox_tabDTO.isDeleted = rs.getInt("isDeleted");
				inbox_tabDTO.lastModificationTime = rs.getLong("lastModificationTime");

			}			
			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DBMR.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
		return inbox_tabDTO;
	}
	
	public long update(CommonDTO commonDTO, String tableName) throws Exception
	{		
		Inbox_tabDTO inbox_tabDTO = (Inbox_tabDTO)commonDTO;
		
		Connection connection = null;
		PreparedStatement ps = null;

		long lastModificationTime = System.currentTimeMillis();	
		try{
			connection = DBMW.getInstance().getConnection();

			String sql = "UPDATE " + tableName;
			
			sql += " SET ";
			sql += "name_language_id=?";
			sql += ", ";
			sql += "query_tab=?";
			sql += ", ";
			sql += "tab_table_header=?";
			sql += ", ";
			sql += "details_link=?";
			sql += ", ";
			sql += "query_details=?";
			sql += ", ";
			sql += "status=?";
			sql += ", ";
			sql += "query_search=?";
			sql += ", lastModificationTime = "	+ lastModificationTime + "";
            sql += " WHERE ID = " + inbox_tabDTO.iD;
				
			printSql(sql);

			ps = connection.prepareStatement(sql);
			
			

			int index = 1;
			
			ps.setObject(index++,inbox_tabDTO.nameLanguageId);
			ps.setObject(index++,inbox_tabDTO.queryTab);
			ps.setObject(index++,inbox_tabDTO.tabTableHeader);
			ps.setObject(index++,inbox_tabDTO.detailsLink);
			ps.setObject(index++,inbox_tabDTO.queryDetails);
			ps.setObject(index++,inbox_tabDTO.status);
			ps.setObject(index++,inbox_tabDTO.querySearch);
			System.out.println(ps);
			ps.executeUpdate();
			


			
			recordUpdateTime(connection,  lastModificationTime, tableName);

						
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if (ps != null) {
					ps.close();
				}
			} catch(Exception e){}
			try{
				if(connection != null){
					DBMW.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		return inbox_tabDTO.iD;
	}
	
	public List<Inbox_tabDTO> getDTOs(Collection recordIDs)
	{
		return getDTOs(recordIDs, tableName);
	}
	
	public List<Inbox_tabDTO> getDTOs(Collection recordIDs, String tableName){
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Inbox_tabDTO inbox_tabDTO = null;
		List<Inbox_tabDTO> inbox_tabDTOList = new ArrayList<>();
		if(recordIDs.isEmpty()){
			return inbox_tabDTOList;
		}
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
            
            sql += " WHERE ID IN ( ";

			for(int i = 0;i<recordIDs.size();i++){
				if(i!=0){
					sql+=",";
				}
				sql+=((ArrayList)recordIDs).get(i);
			}
			sql+=")  order by lastModificationTime desc";
			
			printSql(sql);
			
			logger.debug("sql " + sql);
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				inbox_tabDTO = new Inbox_tabDTO();
				inbox_tabDTO.iD = rs.getLong("ID");
				inbox_tabDTO.nameLanguageId = rs.getLong("name_language_id");
				inbox_tabDTO.queryTab = rs.getString("query_tab");
				inbox_tabDTO.tabTableHeader = rs.getString("tab_table_header");
				inbox_tabDTO.detailsLink = rs.getString("details_link");
				inbox_tabDTO.queryDetails = rs.getString("query_details");
				inbox_tabDTO.status = rs.getBoolean("status");
				inbox_tabDTO.querySearch = rs.getString("query_search");
				inbox_tabDTO.isDeleted = rs.getInt("isDeleted");
				inbox_tabDTO.lastModificationTime = rs.getLong("lastModificationTime");
				System.out.println("got this DTO: " + inbox_tabDTO);
				
				inbox_tabDTOList.add(inbox_tabDTO);

			}			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DBMR.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
		return inbox_tabDTOList;
	
	}
	
	

	
	
	
	//add repository
	public List<Inbox_tabDTO> getAllInbox_tab (boolean isFirstReload)
    {
		List<Inbox_tabDTO> inbox_tabDTOList = new ArrayList<>();

		String sql = "SELECT * FROM inbox_tab";
		sql += " WHERE ";
	

		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by inbox_tab.lastModificationTime desc";
		printSql(sql);
		
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;

		
		try{
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();
			rs = stmt.executeQuery(sql);
			

			while(rs.next()){
				Inbox_tabDTO inbox_tabDTO = new Inbox_tabDTO();
				inbox_tabDTO.iD = rs.getLong("ID");
				inbox_tabDTO.nameLanguageId = rs.getLong("name_language_id");
				inbox_tabDTO.queryTab = rs.getString("query_tab");
				inbox_tabDTO.tabTableHeader = rs.getString("tab_table_header");
				inbox_tabDTO.detailsLink = rs.getString("details_link");
				inbox_tabDTO.queryDetails = rs.getString("query_details");
				inbox_tabDTO.status = rs.getBoolean("status");
				inbox_tabDTO.querySearch = rs.getString("query_search");
				inbox_tabDTO.isDeleted = rs.getInt("isDeleted");
				inbox_tabDTO.lastModificationTime = rs.getLong("lastModificationTime");
				
				inbox_tabDTOList.add(inbox_tabDTO);
			}			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			try{ 
				if (connection != null){ 
					DBMR.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}

		return inbox_tabDTOList;
    }
	
	public List<Inbox_tabDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset)
	{
		return getDTOs(p_searchCriteria, limit, offset, tableName, true, 0);
	}
	
	public List<Inbox_tabDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, String tableName, boolean isPermanentTable, long userApprovalPathType)
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		List<Inbox_tabDTO> inbox_tabDTOList = new ArrayList<>();

		try{
			
			String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, tableName, isPermanentTable, userApprovalPathType);
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				Inbox_tabDTO inbox_tabDTO = new Inbox_tabDTO();
				inbox_tabDTO.iD = rs.getLong("ID");
				inbox_tabDTO.nameLanguageId = rs.getLong("name_language_id");
				inbox_tabDTO.queryTab = rs.getString("query_tab");
				inbox_tabDTO.tabTableHeader = rs.getString("tab_table_header");
				inbox_tabDTO.detailsLink = rs.getString("details_link");
				inbox_tabDTO.queryDetails = rs.getString("query_details");
				inbox_tabDTO.status = rs.getBoolean("status");
				inbox_tabDTO.querySearch = rs.getString("query_search");
				inbox_tabDTO.isDeleted = rs.getInt("isDeleted");
				inbox_tabDTO.lastModificationTime = rs.getLong("lastModificationTime");
				
				inbox_tabDTOList.add(inbox_tabDTO);
			}					
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DBMR.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
		return inbox_tabDTOList;
	
	}

		
	
	public String getSqlWithSearchCriteria(Hashtable p_searchCriteria, int limit, int offset, int category, String tableName, boolean isPermanentTable, long userApprovalPathType)
    {
		Inbox_tabMAPS maps = new Inbox_tabMAPS(tableName);
		String joinSQL = "";
		return getSqlWithSearchCriteria(p_searchCriteria, limit, offset, category, tableName, isPermanentTable, userApprovalPathType, maps, joinSQL);
    }			
}
	