package inbox_tab;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class Inbox_tabRepository implements Repository {
	Inbox_tabDAO inbox_tabDAO = null;
	
	public void setDAO(Inbox_tabDAO inbox_tabDAO)
	{
		this.inbox_tabDAO = inbox_tabDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Inbox_tabRepository.class);
	Map<Long, Inbox_tabDTO>mapOfInbox_tabDTOToiD;
	Map<Long, Set<Inbox_tabDTO> >mapOfInbox_tabDTOTonameLanguageId;
	Map<String, Set<Inbox_tabDTO> >mapOfInbox_tabDTOToqueryTab;
	Map<String, Set<Inbox_tabDTO> >mapOfInbox_tabDTOTotabTableHeader;
	Map<String, Set<Inbox_tabDTO> >mapOfInbox_tabDTOTodetailsLink;
	Map<String, Set<Inbox_tabDTO> >mapOfInbox_tabDTOToqueryDetails;
	Map<Boolean, Set<Inbox_tabDTO> >mapOfInbox_tabDTOTostatus;
	Map<String, Set<Inbox_tabDTO> >mapOfInbox_tabDTOToactionColumn;
	Map<Long, Set<Inbox_tabDTO> >mapOfInbox_tabDTOTolastModificationTime;


	static Inbox_tabRepository instance = null;  
	private Inbox_tabRepository(){
		mapOfInbox_tabDTOToiD = new ConcurrentHashMap<>();
		mapOfInbox_tabDTOTonameLanguageId = new ConcurrentHashMap<>();
		mapOfInbox_tabDTOToqueryTab = new ConcurrentHashMap<>();
		mapOfInbox_tabDTOTotabTableHeader = new ConcurrentHashMap<>();
		mapOfInbox_tabDTOTodetailsLink = new ConcurrentHashMap<>();
		mapOfInbox_tabDTOToqueryDetails = new ConcurrentHashMap<>();
		mapOfInbox_tabDTOTostatus = new ConcurrentHashMap<>();
		mapOfInbox_tabDTOToactionColumn = new ConcurrentHashMap<>();
		mapOfInbox_tabDTOTolastModificationTime = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Inbox_tabRepository getInstance(){
		if (instance == null){
			instance = new Inbox_tabRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(inbox_tabDAO == null)
		{
			return;
		}
		try {
			List<Inbox_tabDTO> inbox_tabDTOs = inbox_tabDAO.getAllInbox_tab(reloadAll);
			for(Inbox_tabDTO inbox_tabDTO : inbox_tabDTOs) {
				Inbox_tabDTO oldInbox_tabDTO = getInbox_tabDTOByID(inbox_tabDTO.iD);
				if( oldInbox_tabDTO != null ) {
					mapOfInbox_tabDTOToiD.remove(oldInbox_tabDTO.iD);
				
					if(mapOfInbox_tabDTOTonameLanguageId.containsKey(oldInbox_tabDTO.nameLanguageId)) {
						mapOfInbox_tabDTOTonameLanguageId.get(oldInbox_tabDTO.nameLanguageId).remove(oldInbox_tabDTO);
					}
					if(mapOfInbox_tabDTOTonameLanguageId.get(oldInbox_tabDTO.nameLanguageId).isEmpty()) {
						mapOfInbox_tabDTOTonameLanguageId.remove(oldInbox_tabDTO.nameLanguageId);
					}
					
					if(mapOfInbox_tabDTOToqueryTab.containsKey(oldInbox_tabDTO.queryTab)) {
						mapOfInbox_tabDTOToqueryTab.get(oldInbox_tabDTO.queryTab).remove(oldInbox_tabDTO);
					}
					if(mapOfInbox_tabDTOToqueryTab.get(oldInbox_tabDTO.queryTab).isEmpty()) {
						mapOfInbox_tabDTOToqueryTab.remove(oldInbox_tabDTO.queryTab);
					}
					
					if(mapOfInbox_tabDTOTotabTableHeader.containsKey(oldInbox_tabDTO.tabTableHeader)) {
						mapOfInbox_tabDTOTotabTableHeader.get(oldInbox_tabDTO.tabTableHeader).remove(oldInbox_tabDTO);
					}
					if(mapOfInbox_tabDTOTotabTableHeader.get(oldInbox_tabDTO.tabTableHeader).isEmpty()) {
						mapOfInbox_tabDTOTotabTableHeader.remove(oldInbox_tabDTO.tabTableHeader);
					}
					
					if(mapOfInbox_tabDTOTodetailsLink.containsKey(oldInbox_tabDTO.detailsLink)) {
						mapOfInbox_tabDTOTodetailsLink.get(oldInbox_tabDTO.detailsLink).remove(oldInbox_tabDTO);
					}
					if(mapOfInbox_tabDTOTodetailsLink.get(oldInbox_tabDTO.detailsLink).isEmpty()) {
						mapOfInbox_tabDTOTodetailsLink.remove(oldInbox_tabDTO.detailsLink);
					}
					
					if(mapOfInbox_tabDTOToqueryDetails.containsKey(oldInbox_tabDTO.queryDetails)) {
						mapOfInbox_tabDTOToqueryDetails.get(oldInbox_tabDTO.queryDetails).remove(oldInbox_tabDTO);
					}
					if(mapOfInbox_tabDTOToqueryDetails.get(oldInbox_tabDTO.queryDetails).isEmpty()) {
						mapOfInbox_tabDTOToqueryDetails.remove(oldInbox_tabDTO.queryDetails);
					}
					
					if(mapOfInbox_tabDTOTostatus.containsKey(oldInbox_tabDTO.status)) {
						mapOfInbox_tabDTOTostatus.get(oldInbox_tabDTO.status).remove(oldInbox_tabDTO);
					}
					if(mapOfInbox_tabDTOTostatus.get(oldInbox_tabDTO.status).isEmpty()) {
						mapOfInbox_tabDTOTostatus.remove(oldInbox_tabDTO.status);
					}
					
//					if(mapOfInbox_tabDTOToactionColumn.containsKey(oldInbox_tabDTO.actionColumn)) {
//						mapOfInbox_tabDTOToactionColumn.get(oldInbox_tabDTO.actionColumn).remove(oldInbox_tabDTO);
//					}
//					if(mapOfInbox_tabDTOToactionColumn.get(oldInbox_tabDTO.actionColumn).isEmpty()) {
//						mapOfInbox_tabDTOToactionColumn.remove(oldInbox_tabDTO.actionColumn);
//					}
					
					if(mapOfInbox_tabDTOTolastModificationTime.containsKey(oldInbox_tabDTO.lastModificationTime)) {
						mapOfInbox_tabDTOTolastModificationTime.get(oldInbox_tabDTO.lastModificationTime).remove(oldInbox_tabDTO);
					}
					if(mapOfInbox_tabDTOTolastModificationTime.get(oldInbox_tabDTO.lastModificationTime).isEmpty()) {
						mapOfInbox_tabDTOTolastModificationTime.remove(oldInbox_tabDTO.lastModificationTime);
					}
					
					
				}
				if(inbox_tabDTO.isDeleted == 0) 
				{
					
					mapOfInbox_tabDTOToiD.put(inbox_tabDTO.iD, inbox_tabDTO);
				
					if( ! mapOfInbox_tabDTOTonameLanguageId.containsKey(inbox_tabDTO.nameLanguageId)) {
						mapOfInbox_tabDTOTonameLanguageId.put(inbox_tabDTO.nameLanguageId, new HashSet<>());
					}
					mapOfInbox_tabDTOTonameLanguageId.get(inbox_tabDTO.nameLanguageId).add(inbox_tabDTO);
					
					if( ! mapOfInbox_tabDTOToqueryTab.containsKey(inbox_tabDTO.queryTab)) {
						mapOfInbox_tabDTOToqueryTab.put(inbox_tabDTO.queryTab, new HashSet<>());
					}
					mapOfInbox_tabDTOToqueryTab.get(inbox_tabDTO.queryTab).add(inbox_tabDTO);
					
					if( ! mapOfInbox_tabDTOTotabTableHeader.containsKey(inbox_tabDTO.tabTableHeader)) {
						mapOfInbox_tabDTOTotabTableHeader.put(inbox_tabDTO.tabTableHeader, new HashSet<>());
					}
					mapOfInbox_tabDTOTotabTableHeader.get(inbox_tabDTO.tabTableHeader).add(inbox_tabDTO);
					
					if( ! mapOfInbox_tabDTOTodetailsLink.containsKey(inbox_tabDTO.detailsLink)) {
						mapOfInbox_tabDTOTodetailsLink.put(inbox_tabDTO.detailsLink, new HashSet<>());
					}
					mapOfInbox_tabDTOTodetailsLink.get(inbox_tabDTO.detailsLink).add(inbox_tabDTO);
					
					if( ! mapOfInbox_tabDTOToqueryDetails.containsKey(inbox_tabDTO.queryDetails)) {
						mapOfInbox_tabDTOToqueryDetails.put(inbox_tabDTO.queryDetails, new HashSet<>());
					}
					mapOfInbox_tabDTOToqueryDetails.get(inbox_tabDTO.queryDetails).add(inbox_tabDTO);
					
					if( ! mapOfInbox_tabDTOTostatus.containsKey(inbox_tabDTO.status)) {
						mapOfInbox_tabDTOTostatus.put(inbox_tabDTO.status, new HashSet<>());
					}
					mapOfInbox_tabDTOTostatus.get(inbox_tabDTO.status).add(inbox_tabDTO);
					
//					if( ! mapOfInbox_tabDTOToactionColumn.containsKey(inbox_tabDTO.actionColumn)) {
//						mapOfInbox_tabDTOToactionColumn.put(inbox_tabDTO.actionColumn, new HashSet<>());
//					}
//					mapOfInbox_tabDTOToactionColumn.get(inbox_tabDTO.actionColumn).add(inbox_tabDTO);
//					
					if( ! mapOfInbox_tabDTOTolastModificationTime.containsKey(inbox_tabDTO.lastModificationTime)) {
						mapOfInbox_tabDTOTolastModificationTime.put(inbox_tabDTO.lastModificationTime, new HashSet<>());
					}
					mapOfInbox_tabDTOTolastModificationTime.get(inbox_tabDTO.lastModificationTime).add(inbox_tabDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Inbox_tabDTO> getInbox_tabList() {
		List <Inbox_tabDTO> inbox_tabs = new ArrayList<Inbox_tabDTO>(this.mapOfInbox_tabDTOToiD.values());
		return inbox_tabs;
	}
	
	
	public Inbox_tabDTO getInbox_tabDTOByID( long ID){
		return mapOfInbox_tabDTOToiD.get(ID);
	}
	
	
	public List<Inbox_tabDTO> getInbox_tabDTOByname_language_id(long name_language_id) {
		return new ArrayList<>( mapOfInbox_tabDTOTonameLanguageId.getOrDefault(name_language_id,new HashSet<>()));
	}
	
	
	public List<Inbox_tabDTO> getInbox_tabDTOByquery_tab(String query_tab) {
		return new ArrayList<>( mapOfInbox_tabDTOToqueryTab.getOrDefault(query_tab,new HashSet<>()));
	}
	
	
	public List<Inbox_tabDTO> getInbox_tabDTOBytab_table_header(String tab_table_header) {
		return new ArrayList<>( mapOfInbox_tabDTOTotabTableHeader.getOrDefault(tab_table_header,new HashSet<>()));
	}
	
	
	public List<Inbox_tabDTO> getInbox_tabDTOBydetails_link(String details_link) {
		return new ArrayList<>( mapOfInbox_tabDTOTodetailsLink.getOrDefault(details_link,new HashSet<>()));
	}
	
	
	public List<Inbox_tabDTO> getInbox_tabDTOByquery_details(String query_details) {
		return new ArrayList<>( mapOfInbox_tabDTOToqueryDetails.getOrDefault(query_details,new HashSet<>()));
	}
	
	
	public List<Inbox_tabDTO> getInbox_tabDTOBystatus(boolean status) {
		return new ArrayList<>( mapOfInbox_tabDTOTostatus.getOrDefault(status,new HashSet<>()));
	}
	
	
	public List<Inbox_tabDTO> getInbox_tabDTOByaction_column(String action_column) {
		return new ArrayList<>( mapOfInbox_tabDTOToactionColumn.getOrDefault(action_column,new HashSet<>()));
	}
	
	
	public List<Inbox_tabDTO> getInbox_tabDTOBylastModificationTime(long lastModificationTime) {
		return new ArrayList<>( mapOfInbox_tabDTOTolastModificationTime.getOrDefault(lastModificationTime,new HashSet<>()));
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "inbox_tab";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


