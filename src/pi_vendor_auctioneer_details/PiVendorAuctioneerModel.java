package pi_vendor_auctioneer_details;
public class PiVendorAuctioneerModel {
    public String iD = "";
    public String address = "";
    public String mobile = "";


    public PiVendorAuctioneerModel(){ }
    public PiVendorAuctioneerModel(Pi_vendor_auctioneer_detailsDTO dto, String language){
        
        boolean isLanguageEnglish = "English".equalsIgnoreCase(language);
        iD = String.valueOf(dto.iD);
        address = dto.address;
        mobile = dto.mobile;
    }

    @Override
    public String toString() {
        return "PiProductModel{" +
                "iD='" + iD + '\'' +
                ", address='" + address + '\'' +
                ", mobile='" + mobile + '\'' +
                '}';
    }
}
