package pi_vendor_auctioneer_details;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import am_other_office_unit.Am_other_office_unitDTO;
import am_parliament_building_level.Am_parliament_building_levelDTO;
import org.apache.log4j.Logger;
import com.google.gson.Gson;

import pb.CatDTO;
import pb.OptionDTO;
import pb.Utils;
import recruitment_test_name.Recruitment_test_nameDTO;
import repository.Repository;
import repository.RepositoryManager;
import util.UtilCharacter;
import vm_fuel_vendor.Vm_fuel_vendorDTO;


public class Pi_vendor_auctioneer_detailsRepository implements Repository {
	Pi_vendor_auctioneer_detailsDAO pi_vendor_auctioneer_detailsDAO = null;
	
	static Logger logger = Logger.getLogger(Pi_vendor_auctioneer_detailsRepository.class);
	Map<Long, Pi_vendor_auctioneer_detailsDTO>mapOfPi_vendor_auctioneer_detailsDTOToiD;
	Gson gson;

  
	private Pi_vendor_auctioneer_detailsRepository(){
		pi_vendor_auctioneer_detailsDAO = Pi_vendor_auctioneer_detailsDAO.getInstance();
		mapOfPi_vendor_auctioneer_detailsDTOToiD = new ConcurrentHashMap<>();
		gson = new Gson();
		RepositoryManager.getInstance().addRepository(this);
	}

	private static class LazyLoader{
        final static Pi_vendor_auctioneer_detailsRepository INSTANCE = new Pi_vendor_auctioneer_detailsRepository();
    }

    public static Pi_vendor_auctioneer_detailsRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

	public void reload(boolean reloadAll){
		try {
			List<Pi_vendor_auctioneer_detailsDTO> pi_vendor_auctioneer_detailsDTOs = pi_vendor_auctioneer_detailsDAO.getAllDTOs(reloadAll);
			for(Pi_vendor_auctioneer_detailsDTO pi_vendor_auctioneer_detailsDTO : pi_vendor_auctioneer_detailsDTOs) {
				Pi_vendor_auctioneer_detailsDTO oldPi_vendor_auctioneer_detailsDTO = getPi_vendor_auctioneer_detailsDTOByiD(pi_vendor_auctioneer_detailsDTO.iD);
				if( oldPi_vendor_auctioneer_detailsDTO != null ) {
					mapOfPi_vendor_auctioneer_detailsDTOToiD.remove(oldPi_vendor_auctioneer_detailsDTO.iD);
				
					
				}
				if(pi_vendor_auctioneer_detailsDTO.isDeleted == 0) 
				{
					
					mapOfPi_vendor_auctioneer_detailsDTOToiD.put(pi_vendor_auctioneer_detailsDTO.iD, pi_vendor_auctioneer_detailsDTO);
				
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public Pi_vendor_auctioneer_detailsDTO clone(Pi_vendor_auctioneer_detailsDTO dto) {
		String raw = gson.toJson(dto);
		return gson.fromJson(raw, Pi_vendor_auctioneer_detailsDTO.class);
	}
	
	
	public List<Pi_vendor_auctioneer_detailsDTO> getPi_vendor_auctioneer_detailsList() {
		List <Pi_vendor_auctioneer_detailsDTO> pi_vendor_auctioneer_detailss = new ArrayList<Pi_vendor_auctioneer_detailsDTO>(this.mapOfPi_vendor_auctioneer_detailsDTOToiD.values());
		return pi_vendor_auctioneer_detailss;
	}
	
	public List<Pi_vendor_auctioneer_detailsDTO> copyPi_vendor_auctioneer_detailsList() {
		List <Pi_vendor_auctioneer_detailsDTO> pi_vendor_auctioneer_detailss = getPi_vendor_auctioneer_detailsList();
		return pi_vendor_auctioneer_detailss
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}
	
	
	
	public Pi_vendor_auctioneer_detailsDTO getPi_vendor_auctioneer_detailsDTOByiD( long iD){
		return mapOfPi_vendor_auctioneer_detailsDTOToiD.get(iD);
	}
	
	public Pi_vendor_auctioneer_detailsDTO copyPi_vendor_auctioneer_detailsDTOByiD( long iD){
		return clone(mapOfPi_vendor_auctioneer_detailsDTOToiD.get(iD));
	}

	
	@Override
	public String getTableName() {
		return pi_vendor_auctioneer_detailsDAO.getTableName();
	}

	public String getOptions(String language, long defaultValue, String vendorAuctioneerType){
		StringBuilder sOptions =new StringBuilder();
		if (defaultValue == -1)
		{
			if (language.equalsIgnoreCase("English"))
			{
				sOptions.append("<option value = ''>Select</option>");
			}
			else
			{
				sOptions.append("<option value = ''>বাছাই করুন</option>");
			}
		}

		List<Pi_vendor_auctioneer_detailsDTO> dtos = getPi_vendor_auctioneer_detailsList().stream()
													 .filter(dto -> dto.piVendorAuctioneerCat.contains(vendorAuctioneerType))
				                                     .collect(Collectors.toList());

		for(Pi_vendor_auctioneer_detailsDTO dto: dtos){
			StringBuilder sOption =new StringBuilder();
			sOption.append("<option value = '").append( dto.iD).append("'");
			if(defaultValue != CatDTO.CATDEFAULTNOTREQUIRED && dto.iD == defaultValue)
			{
				sOption.append(" selected ");
			}
			sOption.append(">").append(UtilCharacter.getDataByLanguage(language,dto.nameBn,dto.nameEn)).append("</option>");
			sOptions.append(sOption);
		}


		return sOptions.toString();

	}

	public List<Pi_vendor_auctioneer_detailsDTO> getAllPi_vendor_auctioneer_detailsDTOs(String vendorAuctioneerType) {
		List<Pi_vendor_auctioneer_detailsDTO> dtos = getPi_vendor_auctioneer_detailsList().stream()
				.filter(dto -> dto.piVendorAuctioneerCat.contains(vendorAuctioneerType))
				.collect(Collectors.toList());
		return dtos;
	}

	public String getText(long id,String language){
		Pi_vendor_auctioneer_detailsDTO dto = getPi_vendor_auctioneer_detailsDTOByiD(id);
		return dto==null?"": ("Bangla".equalsIgnoreCase(language)?dto.nameBn: dto.nameEn);
	}

	public String buildOptions(String language, Long selectedId) {
		List<OptionDTO> optionDTOList = null;
		List<Pi_vendor_auctioneer_detailsDTO> pi_vendor_auctioneer_detailsDTOS = getPi_vendor_auctioneer_detailsList();
		if (pi_vendor_auctioneer_detailsDTOS != null && pi_vendor_auctioneer_detailsDTOS.size() > 0) {
			optionDTOList = pi_vendor_auctioneer_detailsDTOS.stream()
					.map(Pi_vendor_auctioneer_detailsDTO::getOptionDTO)
					.collect(Collectors.toList());
		}
		return Utils.buildOptions(optionDTOList, language, selectedId == null ? null : String.valueOf(selectedId));
	}
}


