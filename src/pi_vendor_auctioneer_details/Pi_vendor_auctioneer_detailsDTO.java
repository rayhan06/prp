package pi_vendor_auctioneer_details;
import java.util.*;

import pb.OptionDTO;
import util.*;


public class Pi_vendor_auctioneer_detailsDTO extends CommonDTO
{

    public String nameEn = "";
    public String nameBn = "";
    public String address = "";
    public String permanentAddress = "";
    public String nid = "";
    public String mobile = "";
    public String email = "";
    public String website = "";
    public String contactPerson = "";
	public String piVendorAuctioneerCat = "";
	public long insertionDate = -1;
    public String insertedBy = "";
    public String modifiedBy = "";

    public OptionDTO getOptionDTO() {

        return new OptionDTO(
                nameEn,
                nameBn,
                String.format("%d", iD)
        );
    }
	
	
    @Override
	public String toString() {
            return "$Pi_vendor_auctioneer_detailsDTO[" +
            " iD = " + iD +
            " nameEn = " + nameEn +
            " nameBn = " + nameBn +
            " address = " + address +
            " permanentAddress = " + permanentAddress +
            " nid = " + nid +
            " mobile = " + mobile +
            " piVendorAuctioneerCat = " + piVendorAuctioneerCat +
            " insertionDate = " + insertionDate +
            " insertedBy = " + insertedBy +
            " modifiedBy = " + modifiedBy +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            " searchColumn = " + searchColumn +
            "]";
    }

}