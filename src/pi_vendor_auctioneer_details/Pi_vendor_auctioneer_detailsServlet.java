package pi_vendor_auctioneer_details;

import java.io.IOException;
import java.io.*;
import java.text.SimpleDateFormat;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;

import am_other_office_unit.Am_other_office_unitDTO;
import am_other_office_unit.Am_other_office_unitRepository;
import category.CategoryDTO;
import org.apache.log4j.Logger;
import login.LoginDTO;
import permission.MenuConstants;
import pi_auction_items.PiProductModel;
import pi_package_vendor.Pi_package_vendorDAO;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.*;
import javax.servlet.http.*;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import common.BaseServlet;
import com.google.gson.Gson;
import pb.*;

@WebServlet("/Pi_vendor_auctioneer_detailsServlet")
@MultipartConfig
public class Pi_vendor_auctioneer_detailsServlet extends BaseServlet
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Pi_vendor_auctioneer_detailsServlet.class);

    @Override
    public String getTableName() {
        return Pi_vendor_auctioneer_detailsDAO.getInstance().getTableName();
    }

    @Override
    public String getServletName() {
        return "Pi_vendor_auctioneer_detailsServlet";
    }

    @Override
    public Pi_vendor_auctioneer_detailsDAO getCommonDAOService() {
        return Pi_vendor_auctioneer_detailsDAO.getInstance();
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[] {MenuConstants.PI_VENDOR_AUCTIONEER_DETAILS_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[] {MenuConstants.PI_VENDOR_AUCTIONEER_DETAILS_ADD};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[] {MenuConstants.PI_VENDOR_AUCTIONEER_DETAILS_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Pi_vendor_auctioneer_detailsServlet.class;
    }
    private final Gson gson = new Gson();


	@Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception
	{


		Pi_vendor_auctioneer_detailsDTO pi_vendor_auctioneer_detailsDTO;
		SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
		String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
		boolean isLanEng = Language.equalsIgnoreCase("English");

		if(addFlag)
		{
			pi_vendor_auctioneer_detailsDTO = new Pi_vendor_auctioneer_detailsDTO();

			pi_vendor_auctioneer_detailsDTO.insertionDate = pi_vendor_auctioneer_detailsDTO.lastModificationTime = System.currentTimeMillis();
			pi_vendor_auctioneer_detailsDTO.insertedBy = pi_vendor_auctioneer_detailsDTO.modifiedBy = String.valueOf(userDTO.ID);
		}
		else
		{
			pi_vendor_auctioneer_detailsDTO = Pi_vendor_auctioneer_detailsDAO
										     .getInstance()
										     .getDTOFromID(Long.parseLong(request.getParameter("iD")));

			if (pi_vendor_auctioneer_detailsDTO == null) {
				throw new Exception(isLanEng?"Vendor/Auctioneer information is not found":"ভেন্ডর/নিলামে অংশগ্রহণকারী তথ্য খুঁজে পাওয়া যায়নি");
			}
			pi_vendor_auctioneer_detailsDTO.lastModificationTime = System.currentTimeMillis();
			pi_vendor_auctioneer_detailsDTO.modifiedBy = String.valueOf(userDTO.ID);
		}

		String Value = "";

		Value = request.getParameter("nameEn");
		if (Value == null ||Value.equals("") || Value.equals("-1")) {
			throw new Exception(isLanEng?"Please provide vendor/auctioneer english name":"অনুগ্রহপূর্বক ভেন্ডর/নিলামে অংশগ্রহণকারীর ইংরেজি নাম দিন");
		}
		boolean hasNonEnglish =
				Value.chars()
				.anyMatch(ch -> !(ch>=' ' && ch<='~'));
		if(!hasNonEnglish){
			pi_vendor_auctioneer_detailsDTO.nameEn = (Value);
		}
		else{
			throw new Exception(isLanEng?"Please provide vendor/auctioneer english name":"অনুগ্রহপূর্বক ভেন্ডর/নিলামে অংশগ্রহণকারীর ইংরেজি নাম দিন");
		}



		Value = request.getParameter("nameBn");
		if (Value == null ||Value.equals("") || Value.equals("-1")) {
			throw new Exception(isLanEng?"Please provide vendor/auctioneer bengali name":"অনুগ্রহপূর্বক ভেন্ডর/নিলামে অংশগ্রহণকারীর বাংলা নাম দিন");
		}

		boolean hasAllBangla = Value.chars().allMatch(Utils::isValidBanglaCharacter);
		if(!hasAllBangla) {
			throw new Exception(isLanEng?"Please provide vendor/auctioneer bengali name":"অনুগ্রহপূর্বক ভেন্ডর/নিলামে অংশগ্রহণকারীর বাংলা নাম দিন");
		}

		pi_vendor_auctioneer_detailsDTO.nameBn = (Value);

		Value = request.getParameter("address");
		if (Value == null ||Value.equals("") || Value.equals("-1")) {
			throw new Exception(isLanEng?"Please provide vendor/auctioneer present address":"অনুগ্রহপূর্বক ভেন্ডর/নিলামে অংশগ্রহণকারীর বর্তমান ঠিকানা দিন");
		}
		pi_vendor_auctioneer_detailsDTO.address = (Value);

		Value = request.getParameter("permanentAddress");
		if (Value == null ||Value.equals("") || Value.equals("-1")) {
			throw new Exception(isLanEng?"Please provide vendor/auctioneer permanent address":"অনুগ্রহপূর্বক ভেন্ডর/নিলামে অংশগ্রহণকারীর স্থায়ী ঠিকানা দিন");
		}
		pi_vendor_auctioneer_detailsDTO.permanentAddress = (Value);

		Value = request.getParameter("nid");
		if (Value == null ||Value.equals("") || Value.equals("-1")) {
			throw new Exception(isLanEng?"Please provide vendor/auctioneer nid number":"অনুগ্রহপূর্বক ভেন্ডর/নিলামে অংশগ্রহণকারীর এন. আই. ডি. নম্বর দিন");
		}
		pi_vendor_auctioneer_detailsDTO.nid = (Value);

		Value = request.getParameter("mobile");
		if (Value == null ||Value.equals("") || Value.equals("-1")) {
			throw new Exception(isLanEng?"Please provide vendor/auctioneer mobile number":"অনুগ্রহপূর্বক ভেন্ডর/নিলামে অংশগ্রহণকারীর মোবাইল নম্বর দিন");
		}
		pi_vendor_auctioneer_detailsDTO.mobile = (Value);




		Value = request.getParameter("email");
//		if (Value == null ||Value.equals("") || Value.equals("-1")) {
//			throw new Exception(isLanEng?"Please provide vendor/auctioneer mobile number":"অনুগ্রহপূর্বক ভেন্ডর/নিলামে অংশগ্রহণকারীর মোবাইল নম্বর দিন");
//		}
		pi_vendor_auctioneer_detailsDTO.email = (Value);


		Value = request.getParameter("website");
//		if (Value == null ||Value.equals("") || Value.equals("-1")) {
//			throw new Exception(isLanEng?"Please provide vendor/auctioneer mobile number":"অনুগ্রহপূর্বক ভেন্ডর/নিলামে অংশগ্রহণকারীর মোবাইল নম্বর দিন");
//		}
		pi_vendor_auctioneer_detailsDTO.website = (Value);


		Value = request.getParameter("contactPerson");
//		if (Value == null ||Value.equals("") || Value.equals("-1")) {
//			throw new Exception(isLanEng?"Please provide vendor/auctioneer mobile number":"অনুগ্রহপূর্বক ভেন্ডর/নিলামে অংশগ্রহণকারীর মোবাইল নম্বর দিন");
//		}
		pi_vendor_auctioneer_detailsDTO.contactPerson = (Value);





		Value = request.getParameter("piVendorAuctioneerCat");
		if (Value == null ||Value.equals("") || Value.equals("-1")) {
			throw new Exception(isLanEng?"Please provide at least one vendor/auctioneer":"অনুগ্রহপূর্বক ভেন্ডর/নিলামে অংশগ্রহণকারীর মধ্যে অন্তত একজন বাছাই করুন");
		}

		if(!addFlag){
			String vendorAuctioneerId = request.getParameter("iD");
			if (vendorAuctioneerId != null && vendorAuctioneerId.length() > 0) {

				if(Pi_vendor_auctioneer_detailsDAO.getInstance().isNotUsed(Long.parseLong(vendorAuctioneerId))) {
					pi_vendor_auctioneer_detailsDTO.piVendorAuctioneerCat = (Value);
				}

				else if(!Pi_vendor_auctioneer_detailsDAO.getInstance().isNotUsed(Long.parseLong(vendorAuctioneerId)) &&
						Value.contains(pi_vendor_auctioneer_detailsDTO.piVendorAuctioneerCat)){
					pi_vendor_auctioneer_detailsDTO.piVendorAuctioneerCat = (Value);
				}
				else{
					throw new Exception(isLanEng?"vendor/auctioneer field is not changeable for this vendor":"অত্র ভেন্ডরের জন্য ভেন্ডর/নিলামে অংশগ্রহণকারীর ফিল্ড পরিবর্তনযোগ্য নয়");
				}
			}
		}
		else{
			pi_vendor_auctioneer_detailsDTO.piVendorAuctioneerCat = (Value);
		}



		logger.debug("Done adding  addPi_vendor_auctioneer_details dto = " + pi_vendor_auctioneer_detailsDTO);

		if(addFlag)
		{
			Pi_vendor_auctioneer_detailsDAO.getInstance().add(pi_vendor_auctioneer_detailsDTO);
		}
		else
		{
			Pi_vendor_auctioneer_detailsDAO.getInstance().update(pi_vendor_auctioneer_detailsDTO);
		}
		return pi_vendor_auctioneer_detailsDTO;
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		if (userDTO == null) {
			request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			return;
		}

		try {
			String actionType = request.getParameter("actionType");
			switch (actionType) {
				case "getAllVendorAuctioneerCat":
					if (Utils.checkPermission(String.valueOf(userDTO.employee_record_id), userDTO, MenuConstants.PI_VENDOR_AUCTIONEER_DETAILS_ADD)) {

						List<OptionDTO> OptionDTOList = new ArrayList<>();
						OptionDTO optionDTO;
						List<CategoryDTO> otherOfficeDTOList = CatRepository.getInstance().getCategoryDTOList("pi_vendor_auctioneer");

						for (CategoryDTO dto : otherOfficeDTOList) {

							optionDTO = new OptionDTO(dto.nameEn, dto.nameBn, dto.value + "");
							OptionDTOList.add(optionDTO);
						}

						response.getWriter().write(gson.toJson(OptionDTOList));
						response.getWriter().flush();
						response.getWriter().close();

						return;
					}

				case "getAddressAndMobile":
					if (Utils.checkPermission(String.valueOf(userDTO.employee_record_id), userDTO, MenuConstants.PI_PACKAGE_VENDOR_ADD)) {

						response.setContentType("application/json");
						response.setCharacterEncoding("UTF-8");

						long vendorAuctioneerId = Long.parseLong(request.getParameter("vendorAuctioneerId"));
						Pi_vendor_auctioneer_detailsDTO pi_vendor_auctioneer_detailsDTO = Pi_vendor_auctioneer_detailsRepository
																						  .getInstance()
																						  .getPi_vendor_auctioneer_detailsDTOByiD(vendorAuctioneerId);


						PiVendorAuctioneerModel piVendorAuctioneerModel =  new PiVendorAuctioneerModel(pi_vendor_auctioneer_detailsDTO, HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language);
						response.getWriter().println(gson.toJson(piVendorAuctioneerModel));

						return;
					}

				case "getVendorOrAuctioneer":

					OptionDTO optionDTO;
					Gson gson = new Gson();
					String venAucType = request.getParameter("venAucType");

					List<OptionDTO> OptionDTOList = new ArrayList<>();
					List<Pi_vendor_auctioneer_detailsDTO> pi_vendor_auctioneer_detailsDTOS = Pi_vendor_auctioneer_detailsRepository
							.getInstance()
							.getAllPi_vendor_auctioneer_detailsDTOs(venAucType);

					for (Pi_vendor_auctioneer_detailsDTO dto : pi_vendor_auctioneer_detailsDTOS) {

						optionDTO = new OptionDTO(dto.nameEn, dto.nameBn, dto.iD + "");
						OptionDTOList.add(optionDTO);
					}
					response.getWriter().write(gson.toJson(OptionDTOList));
					response.getWriter().flush();
					response.getWriter().close();
					return;

				default:
					super.doGet(request, response);
					return;
			}
		} catch (Exception ex) {
			logger.error(ex);
		}
		request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
	}

	@Override
	protected void deleteT(HttpServletRequest request, UserDTO userDTO) {
		String[] IDsToDelete = request.getParameterValues("ID");
		if (IDsToDelete.length > 0) {
			List<Long> ids = Stream.of(IDsToDelete)
					.map(Long::parseLong)
					.filter(i -> Pi_vendor_auctioneer_detailsDAO.getInstance().isNotUsed(i))
					.collect(Collectors.toList());

			if (!ids.isEmpty()) {
				getCommonDAOService().deletePb(userDTO.employee_record_id, ids);
			}
		}
	}
}

