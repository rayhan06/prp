package pi_vendor_auctioneer_details;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.sql.SQLException;
import common.CommonDAOService;
import common.ConnectionAndStatementUtil;
import org.apache.log4j.Logger;
import pi_package_vendor.Pi_package_vendorDTO;
import pi_package_vendor.Pi_package_vendorRepository;
import util.*;
import pb.*;

public class Pi_vendor_auctioneer_detailsDAO  implements CommonDAOService<Pi_vendor_auctioneer_detailsDTO>
{
	
	Logger logger = Logger.getLogger(getClass());
	
	private static String addQuery = "";
	private static String updateQuery = "";
	private String[] columnNames = null;

	private Pi_vendor_auctioneer_detailsDAO()
	{
		columnNames = new String[] 
		{
			"ID",
			"name_en",
			"name_bn",
			"address",
			"permanent_address",
			"nid",
			"mobile",
			"email",
			"website",
			"contact_person",
			"pi_vendor_auctioneer_cat",
			"insertion_date",
			"inserted_by",
			"modified_by",
			"search_column",
			"isDeleted",
			"lastModificationTime"
		};
		updateQuery = getUpdateQuery(columnNames);
        addQuery = getAddQuery(columnNames);
		
		searchMap.put("name_en"," and (name_en like ?)");
		searchMap.put("name_bn"," and (name_bn like ?)");
		searchMap.put("nid"," and (nid = ?)");
		searchMap.put("mobile"," and (mobile = ?)");
		searchMap.put("pi_vendor_auctioneer_cat"," and (pi_vendor_auctioneer_cat = ?)");
		searchMap.put("insertion_date_start"," and (insertion_date >= ?)");
		searchMap.put("insertion_date_end"," and (insertion_date <= ?)");
		searchMap.put("AnyField"," and (search_column like ?)");
	}

	private final Map<String,String> searchMap = new HashMap<>();

    private static class LazyLoader
    {
    	static final Pi_vendor_auctioneer_detailsDAO INSTANCE = new Pi_vendor_auctioneer_detailsDAO();
	}

	public static Pi_vendor_auctioneer_detailsDAO getInstance()
	{
    	return LazyLoader.INSTANCE;
	}
	
	public void setSearchColumn(Pi_vendor_auctioneer_detailsDTO pi_vendor_auctioneer_detailsDTO)
	{
		pi_vendor_auctioneer_detailsDTO.searchColumn = "";
		pi_vendor_auctioneer_detailsDTO.searchColumn += pi_vendor_auctioneer_detailsDTO.nameEn + " ";
		pi_vendor_auctioneer_detailsDTO.searchColumn += pi_vendor_auctioneer_detailsDTO.nameBn + " ";
		pi_vendor_auctioneer_detailsDTO.searchColumn += pi_vendor_auctioneer_detailsDTO.nid + " ";
		pi_vendor_auctioneer_detailsDTO.searchColumn += pi_vendor_auctioneer_detailsDTO.mobile + " ";
		pi_vendor_auctioneer_detailsDTO.searchColumn += pi_vendor_auctioneer_detailsDTO.address + " ";
		pi_vendor_auctioneer_detailsDTO.searchColumn += pi_vendor_auctioneer_detailsDTO.permanentAddress + " ";
		String[] arr = pi_vendor_auctioneer_detailsDTO.piVendorAuctioneerCat.split(",");
		if(arr.length>1){
			pi_vendor_auctioneer_detailsDTO.searchColumn += CatRepository.getInstance().getText("English", "pi_vendor_auctioneer", Long.valueOf(arr[0].trim())) + " " + CatRepository.getInstance().getText("English", "pi_vendor_auctioneer", Long.valueOf(arr[1].trim())) + " ";
			pi_vendor_auctioneer_detailsDTO.searchColumn += CatRepository.getInstance().getText("Bangla", "pi_vendor_auctioneer", Long.valueOf(arr[0].trim())) + " " + CatRepository.getInstance().getText("Bangla", "pi_vendor_auctioneer", Long.valueOf(arr[1].trim())) + " ";
		}
		else{
			pi_vendor_auctioneer_detailsDTO.searchColumn += CatRepository.getInstance().getText("English", "pi_vendor_auctioneer", Long.valueOf(pi_vendor_auctioneer_detailsDTO.piVendorAuctioneerCat)) + " " + CatRepository.getInstance().getText("Bangla", "pi_vendor_auctioneer", Long.valueOf(pi_vendor_auctioneer_detailsDTO.piVendorAuctioneerCat)) + " ";
		}

	}
	
	@Override
	public void set(PreparedStatement ps, Pi_vendor_auctioneer_detailsDTO pi_vendor_auctioneer_detailsDTO, boolean isInsert) throws SQLException
	{
		int index = 0;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(pi_vendor_auctioneer_detailsDTO);
		if(isInsert)
		{
			ps.setObject(++index,pi_vendor_auctioneer_detailsDTO.iD);
		}
		ps.setObject(++index,pi_vendor_auctioneer_detailsDTO.nameEn);
		ps.setObject(++index,pi_vendor_auctioneer_detailsDTO.nameBn);
		ps.setObject(++index,pi_vendor_auctioneer_detailsDTO.address);
		ps.setObject(++index,pi_vendor_auctioneer_detailsDTO.permanentAddress);
		ps.setObject(++index,pi_vendor_auctioneer_detailsDTO.nid);
		ps.setObject(++index,pi_vendor_auctioneer_detailsDTO.mobile);
		ps.setObject(++index,pi_vendor_auctioneer_detailsDTO.email);
		ps.setObject(++index,pi_vendor_auctioneer_detailsDTO.website);
		ps.setObject(++index,pi_vendor_auctioneer_detailsDTO.contactPerson);
		ps.setObject(++index,pi_vendor_auctioneer_detailsDTO.piVendorAuctioneerCat);
		ps.setObject(++index,pi_vendor_auctioneer_detailsDTO.insertionDate);
		ps.setObject(++index,pi_vendor_auctioneer_detailsDTO.insertedBy);
		ps.setObject(++index,pi_vendor_auctioneer_detailsDTO.modifiedBy);
		ps.setObject(++index,pi_vendor_auctioneer_detailsDTO.searchColumn);
		if(isInsert)
		{
			ps.setObject(++index,pi_vendor_auctioneer_detailsDTO.isDeleted);
		}
		ps.setObject(++index, lastModificationTime);
		if(!isInsert)
		{
			ps.setObject(++index,pi_vendor_auctioneer_detailsDTO.iD);
		}
	}
	
	@Override
	public Pi_vendor_auctioneer_detailsDTO buildObjectFromResultSet(ResultSet rs)
	{
		try
		{
			Pi_vendor_auctioneer_detailsDTO pi_vendor_auctioneer_detailsDTO = new Pi_vendor_auctioneer_detailsDTO();
			int i = 0;
			pi_vendor_auctioneer_detailsDTO.iD = rs.getLong(columnNames[i++]);
			pi_vendor_auctioneer_detailsDTO.nameEn = rs.getString(columnNames[i++]);
			pi_vendor_auctioneer_detailsDTO.nameBn = rs.getString(columnNames[i++]);
			pi_vendor_auctioneer_detailsDTO.address = rs.getString(columnNames[i++]);
			pi_vendor_auctioneer_detailsDTO.permanentAddress = rs.getString(columnNames[i++]);
			pi_vendor_auctioneer_detailsDTO.nid = rs.getString(columnNames[i++]);
			pi_vendor_auctioneer_detailsDTO.mobile = rs.getString(columnNames[i++]);
			pi_vendor_auctioneer_detailsDTO.email = rs.getString(columnNames[i++]);
			pi_vendor_auctioneer_detailsDTO.website = rs.getString(columnNames[i++]);
			pi_vendor_auctioneer_detailsDTO.contactPerson = rs.getString(columnNames[i++]);
			pi_vendor_auctioneer_detailsDTO.piVendorAuctioneerCat = rs.getString(columnNames[i++]);
			pi_vendor_auctioneer_detailsDTO.insertionDate = rs.getLong(columnNames[i++]);
			pi_vendor_auctioneer_detailsDTO.insertedBy = rs.getString(columnNames[i++]);
			pi_vendor_auctioneer_detailsDTO.modifiedBy = rs.getString(columnNames[i++]);
			pi_vendor_auctioneer_detailsDTO.searchColumn = rs.getString(columnNames[i++]);
			pi_vendor_auctioneer_detailsDTO.isDeleted = rs.getInt(columnNames[i++]);
			pi_vendor_auctioneer_detailsDTO.lastModificationTime = rs.getLong(columnNames[i++]);

			return pi_vendor_auctioneer_detailsDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
		
	public Pi_vendor_auctioneer_detailsDTO getDTOByID (long id)
	{
		return getDTOFromID(id);
	}

	@Override
	public String getTableName() {
		return "pi_vendor_auctioneer_details";
	}

	@Override
	public Map<String, String> getSearchMap() {
		return searchMap;
	}

	public long add(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Pi_vendor_auctioneer_detailsDTO) commonDTO,addQuery,true);
	}

	public long update(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Pi_vendor_auctioneer_detailsDTO) commonDTO,updateQuery,false);
	}
	
	@Override
	public String getLastModifierUser(){
		return "modified_by";
    }

	public int getCountByVendor(long vendorId) {
		String countQuery = " SELECT count(*) as countID FROM "
				+ " pi_package_vendor_children where isDeleted = 0 and actual_vendor_id = ?  ";
		return ConnectionAndStatementUtil.getT(countQuery, Arrays.asList(vendorId), rs -> {
			try {
				return rs.getInt("countID");
			} catch (SQLException ex) {
				ex.printStackTrace();
				return 0;
			}
		}, 0);
	}
	public int getCountByAuctioneer(long vendorId) {
		String countQuery = " SELECT count(*) as countID FROM "
				+ " pi_package_auctioneer_children where isDeleted = 0 and actual_auctioneer_id = ?  ";
		return ConnectionAndStatementUtil.getT(countQuery, Arrays.asList(vendorId), rs -> {
			try {
				return rs.getInt("countID");
			} catch (SQLException ex) {
				ex.printStackTrace();
				return 0;
			}
		}, 0);
	}

	public boolean isNotUsed(long iD) {

		Pi_vendor_auctioneer_detailsDTO pi_vendor_auctioneer_detailsDTO = Pi_vendor_auctioneer_detailsDAO.getInstance().getDTOByID(iD);

        return getCountByVendor(pi_vendor_auctioneer_detailsDTO.iD) <= 0 && getCountByAuctioneer(pi_vendor_auctioneer_detailsDTO.iD) <= 0;
    }
				
}
	