package fast_search_test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;
import sessionmanager.SessionConstants;
import java.sql.SQLException;


import org.apache.log4j.Logger;

import dbm.*;

import repository.RepositoryManager;

import util.*;
import pb.*;
import user.UserDTO;

public class FastSearchTestChildDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());
	

	
	public FastSearchTestChildDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";
		commonMaps = new FastSearchTestChildMAPS(tableName);
		columnNames = new String[] 
		{
			"ID",
			"fast_search_test_id",
			"religion_cat",
			"medical_equipment_name_type",
			"name_en",
			"name_bn",
			"isDeleted",
			"lastModificationTime"
		};
	}
	
	public FastSearchTestChildDAO()
	{
		this("fast_search_test_child");		
	}
	
	public void setSearchColumn(FastSearchTestChildDTO fastsearchtestchildDTO)
	{
		fastsearchtestchildDTO.searchColumn = "";
		fastsearchtestchildDTO.searchColumn += CatDAO.getName("English", "religion", fastsearchtestchildDTO.religionCat) + " " + CatDAO.getName("Bangla", "religion", fastsearchtestchildDTO.religionCat) + " ";
		fastsearchtestchildDTO.searchColumn += CommonDAO.getName("English", "medical_equipment_name", fastsearchtestchildDTO.medicalEquipmentNameType) + " " + CommonDAO.getName("Bangla", "medical_equipment_name", fastsearchtestchildDTO.medicalEquipmentNameType) + " ";
		fastsearchtestchildDTO.searchColumn += fastsearchtestchildDTO.nameEn + " ";
		fastsearchtestchildDTO.searchColumn += fastsearchtestchildDTO.nameBn + " ";
		
	}
	
	public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException
	{
		FastSearchTestChildDTO fastsearchtestchildDTO = (FastSearchTestChildDTO)commonDTO;
		int index = 1;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(fastsearchtestchildDTO);
		if(isInsert)
		{
			ps.setObject(index++,fastsearchtestchildDTO.iD);
		}
		ps.setObject(index++,fastsearchtestchildDTO.fastSearchTestId);
		ps.setObject(index++,fastsearchtestchildDTO.religionCat);
		ps.setObject(index++,fastsearchtestchildDTO.medicalEquipmentNameType);
		ps.setObject(index++,fastsearchtestchildDTO.nameEn);
		ps.setObject(index++,fastsearchtestchildDTO.nameBn);
		if(isInsert)
		{
			ps.setObject(index++, 0);
			ps.setObject(index++, lastModificationTime);
		}
	}
	
	public void get(FastSearchTestChildDTO fastsearchtestchildDTO, ResultSet rs) throws SQLException
	{
		fastsearchtestchildDTO.iD = rs.getLong("ID");
		fastsearchtestchildDTO.fastSearchTestId = rs.getLong("fast_search_test_id");
		fastsearchtestchildDTO.religionCat = rs.getLong("religion_cat");
		fastsearchtestchildDTO.medicalEquipmentNameType = rs.getLong("medical_equipment_name_type");
		fastsearchtestchildDTO.nameEn = rs.getString("name_en");
		fastsearchtestchildDTO.nameBn = rs.getString("name_bn");
		fastsearchtestchildDTO.isDeleted = rs.getInt("isDeleted");
		fastsearchtestchildDTO.lastModificationTime = rs.getLong("lastModificationTime");
	}
	
	
	
	
		
	
	public void deleteFastSearchTestChildByFastSearchTestID(long fastSearchTestID) throws Exception{
		
		
		Connection connection = null;
		Statement stmt = null;
		try{
			
			String sql = "UPDATE fast_search_test_child SET isDeleted=0 WHERE fast_search_test_id="+fastSearchTestID;			
			logger.debug("sql " + sql);
			connection = DBMW.getInstance().getConnection();
			stmt = connection.createStatement();
			stmt.execute(sql);
		}catch(Exception ex){
			logger.debug("fatal",ex);
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DBMW.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
	}		
   
	public List<FastSearchTestChildDTO> getFastSearchTestChildDTOListByFastSearchTestID(long fastSearchTestID) throws Exception{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		FastSearchTestChildDTO fastsearchtestchildDTO = null;
		List<FastSearchTestChildDTO> fastsearchtestchildDTOList = new ArrayList<>();
		
		try{
			
			String sql = "SELECT * FROM fast_search_test_child where isDeleted=0 and fast_search_test_id="+fastSearchTestID+" order by fast_search_test_child.lastModificationTime";
			
			logger.debug("sql " + sql);
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();
			rs = stmt.executeQuery(sql);

			while(rs.next()){
				fastsearchtestchildDTO = new FastSearchTestChildDTO();
				get(fastsearchtestchildDTO, rs);
				fastsearchtestchildDTOList.add(fastsearchtestchildDTO);

			}			
			
		}catch(Exception ex){
			logger.debug("fatal",ex);
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return fastsearchtestchildDTOList;
	}

	//need another getter for repository
	public FastSearchTestChildDTO getDTOByID (long ID)
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		FastSearchTestChildDTO fastsearchtestchildDTO = null;
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
			
            sql += " WHERE ID=" + ID;
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			if(rs.next()){
				fastsearchtestchildDTO = new FastSearchTestChildDTO();

				get(fastsearchtestchildDTO, rs);

			}			
			
			
			
			FastSearchTestChildDAO fastSearchTestChildDAO = new FastSearchTestChildDAO("fast_search_test_child");			
			List<FastSearchTestChildDTO> fastSearchTestChildDTOList = fastSearchTestChildDAO.getFastSearchTestChildDTOListByFastSearchTestID(fastsearchtestchildDTO.iD);
			fastsearchtestchildDTO.fastSearchTestChildDTOList = fastSearchTestChildDTOList;
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return fastsearchtestchildDTO;
	}
	
	
	
	
	public List<FastSearchTestChildDTO> getDTOs(Collection recordIDs){
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		FastSearchTestChildDTO fastsearchtestchildDTO = null;
		List<FastSearchTestChildDTO> fastsearchtestchildDTOList = new ArrayList<>();
		if(recordIDs.isEmpty()){
			return fastsearchtestchildDTOList;
		}
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
            
            sql += " WHERE ID IN ( ";

			for(int i = 0;i<recordIDs.size();i++){
				if(i!=0){
					sql+=",";
				}
				sql+=((ArrayList)recordIDs).get(i);
			}
			sql+=")  order by lastModificationTime desc";
			
			printSql(sql);
			
			logger.debug("sql " + sql);
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				fastsearchtestchildDTO = new FastSearchTestChildDTO();
				get(fastsearchtestchildDTO, rs);
				System.out.println("got this DTO: " + fastsearchtestchildDTO);
				
				fastsearchtestchildDTOList.add(fastsearchtestchildDTO);

			}			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return fastsearchtestchildDTOList;
	
	}
	
	

	
	
	
	//add repository
	public List<FastSearchTestChildDTO> getAllFastSearchTestChild (boolean isFirstReload)
    {
		List<FastSearchTestChildDTO> fastsearchtestchildDTOList = new ArrayList<>();

		String sql = "SELECT * FROM fast_search_test_child";
		sql += " WHERE ";
	

		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by fastsearchtestchild.lastModificationTime desc";
		printSql(sql);
		
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;

		
		try{
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();
			rs = stmt.executeQuery(sql);
			

			while(rs.next()){
				FastSearchTestChildDTO fastsearchtestchildDTO = new FastSearchTestChildDTO();
				get(fastsearchtestchildDTO, rs);
				
				fastsearchtestchildDTOList.add(fastsearchtestchildDTO);
			}			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}

		return fastsearchtestchildDTOList;
    }

	
	public List<FastSearchTestChildDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO)
	{
		return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
	}

	
	public List<FastSearchTestChildDTO> getDTOs(Hashtable  p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
			String filter, boolean tableHasJobCat)
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		List<FastSearchTestChildDTO> fastsearchtestchildDTOList = new ArrayList<>();

		try{
			
			String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat);
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				FastSearchTestChildDTO fastsearchtestchildDTO = new FastSearchTestChildDTO();
				get(fastsearchtestchildDTO, rs);
				
				fastsearchtestchildDTOList.add(fastsearchtestchildDTO);
			}					
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return fastsearchtestchildDTOList;
	
	}
				
}
	