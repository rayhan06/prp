package fast_search_test;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class Fast_search_testRepository implements Repository {
	Fast_search_testDAO fast_search_testDAO = null;
	
	public void setDAO(Fast_search_testDAO fast_search_testDAO)
	{
		this.fast_search_testDAO = fast_search_testDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Fast_search_testRepository.class);
	Map<Long, Fast_search_testDTO>mapOfFast_search_testDTOToiD;
	Map<Long, Set<Fast_search_testDTO> >mapOfFast_search_testDTOToreligionCat;
	Map<Long, Set<Fast_search_testDTO> >mapOfFast_search_testDTOTomedicalEquipmentNameType;
	Map<String, Set<Fast_search_testDTO> >mapOfFast_search_testDTOTonameEn;
	Map<String, Set<Fast_search_testDTO> >mapOfFast_search_testDTOTonameBn;
	Map<Long, Set<Fast_search_testDTO> >mapOfFast_search_testDTOToinsertedByUserId;
	Map<Long, Set<Fast_search_testDTO> >mapOfFast_search_testDTOToinsertedByOrganogramId;
	Map<String, Set<Fast_search_testDTO> >mapOfFast_search_testDTOTosearchColumn;
	Map<String, Set<Fast_search_testDTO> >mapOfFast_search_testDTOTohomeAddress;
	Map<Integer, Set<Fast_search_testDTO> >mapOfFast_search_testDTOTojobCat;
	Map<Long, Set<Fast_search_testDTO> >mapOfFast_search_testDTOTofilesDropzone;
	Map<Long, Set<Fast_search_testDTO> >mapOfFast_search_testDTOToinsertionDate;
	Map<Long, Set<Fast_search_testDTO> >mapOfFast_search_testDTOTolastModificationTime;


	static Fast_search_testRepository instance = null;  
	private Fast_search_testRepository(){
		mapOfFast_search_testDTOToiD = new ConcurrentHashMap<>();
		mapOfFast_search_testDTOToreligionCat = new ConcurrentHashMap<>();
		mapOfFast_search_testDTOTomedicalEquipmentNameType = new ConcurrentHashMap<>();
		mapOfFast_search_testDTOTonameEn = new ConcurrentHashMap<>();
		mapOfFast_search_testDTOTonameBn = new ConcurrentHashMap<>();
		mapOfFast_search_testDTOToinsertedByUserId = new ConcurrentHashMap<>();
		mapOfFast_search_testDTOToinsertedByOrganogramId = new ConcurrentHashMap<>();
		mapOfFast_search_testDTOTosearchColumn = new ConcurrentHashMap<>();
		mapOfFast_search_testDTOTohomeAddress = new ConcurrentHashMap<>();
		mapOfFast_search_testDTOTojobCat = new ConcurrentHashMap<>();
		mapOfFast_search_testDTOTofilesDropzone = new ConcurrentHashMap<>();
		mapOfFast_search_testDTOToinsertionDate = new ConcurrentHashMap<>();
		mapOfFast_search_testDTOTolastModificationTime = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Fast_search_testRepository getInstance(){
		if (instance == null){
			instance = new Fast_search_testRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(fast_search_testDAO == null)
		{
			return;
		}
		try {
			List<Fast_search_testDTO> fast_search_testDTOs = fast_search_testDAO.getAllFast_search_test(reloadAll);
			for(Fast_search_testDTO fast_search_testDTO : fast_search_testDTOs) {
				Fast_search_testDTO oldFast_search_testDTO = getFast_search_testDTOByID(fast_search_testDTO.iD);
				if( oldFast_search_testDTO != null ) {
					mapOfFast_search_testDTOToiD.remove(oldFast_search_testDTO.iD);
				
					if(mapOfFast_search_testDTOToreligionCat.containsKey(oldFast_search_testDTO.religionCat)) {
						mapOfFast_search_testDTOToreligionCat.get(oldFast_search_testDTO.religionCat).remove(oldFast_search_testDTO);
					}
					if(mapOfFast_search_testDTOToreligionCat.get(oldFast_search_testDTO.religionCat).isEmpty()) {
						mapOfFast_search_testDTOToreligionCat.remove(oldFast_search_testDTO.religionCat);
					}
					
					if(mapOfFast_search_testDTOTomedicalEquipmentNameType.containsKey(oldFast_search_testDTO.medicalEquipmentNameType)) {
						mapOfFast_search_testDTOTomedicalEquipmentNameType.get(oldFast_search_testDTO.medicalEquipmentNameType).remove(oldFast_search_testDTO);
					}
					if(mapOfFast_search_testDTOTomedicalEquipmentNameType.get(oldFast_search_testDTO.medicalEquipmentNameType).isEmpty()) {
						mapOfFast_search_testDTOTomedicalEquipmentNameType.remove(oldFast_search_testDTO.medicalEquipmentNameType);
					}
					
					if(mapOfFast_search_testDTOTonameEn.containsKey(oldFast_search_testDTO.nameEn)) {
						mapOfFast_search_testDTOTonameEn.get(oldFast_search_testDTO.nameEn).remove(oldFast_search_testDTO);
					}
					if(mapOfFast_search_testDTOTonameEn.get(oldFast_search_testDTO.nameEn).isEmpty()) {
						mapOfFast_search_testDTOTonameEn.remove(oldFast_search_testDTO.nameEn);
					}
					
					if(mapOfFast_search_testDTOTonameBn.containsKey(oldFast_search_testDTO.nameBn)) {
						mapOfFast_search_testDTOTonameBn.get(oldFast_search_testDTO.nameBn).remove(oldFast_search_testDTO);
					}
					if(mapOfFast_search_testDTOTonameBn.get(oldFast_search_testDTO.nameBn).isEmpty()) {
						mapOfFast_search_testDTOTonameBn.remove(oldFast_search_testDTO.nameBn);
					}
					
					if(mapOfFast_search_testDTOToinsertedByUserId.containsKey(oldFast_search_testDTO.insertedByUserId)) {
						mapOfFast_search_testDTOToinsertedByUserId.get(oldFast_search_testDTO.insertedByUserId).remove(oldFast_search_testDTO);
					}
					if(mapOfFast_search_testDTOToinsertedByUserId.get(oldFast_search_testDTO.insertedByUserId).isEmpty()) {
						mapOfFast_search_testDTOToinsertedByUserId.remove(oldFast_search_testDTO.insertedByUserId);
					}
					
					if(mapOfFast_search_testDTOToinsertedByOrganogramId.containsKey(oldFast_search_testDTO.insertedByOrganogramId)) {
						mapOfFast_search_testDTOToinsertedByOrganogramId.get(oldFast_search_testDTO.insertedByOrganogramId).remove(oldFast_search_testDTO);
					}
					if(mapOfFast_search_testDTOToinsertedByOrganogramId.get(oldFast_search_testDTO.insertedByOrganogramId).isEmpty()) {
						mapOfFast_search_testDTOToinsertedByOrganogramId.remove(oldFast_search_testDTO.insertedByOrganogramId);
					}
					
					if(mapOfFast_search_testDTOTosearchColumn.containsKey(oldFast_search_testDTO.searchColumn)) {
						mapOfFast_search_testDTOTosearchColumn.get(oldFast_search_testDTO.searchColumn).remove(oldFast_search_testDTO);
					}
					if(mapOfFast_search_testDTOTosearchColumn.get(oldFast_search_testDTO.searchColumn).isEmpty()) {
						mapOfFast_search_testDTOTosearchColumn.remove(oldFast_search_testDTO.searchColumn);
					}
					
					if(mapOfFast_search_testDTOTohomeAddress.containsKey(oldFast_search_testDTO.homeAddress)) {
						mapOfFast_search_testDTOTohomeAddress.get(oldFast_search_testDTO.homeAddress).remove(oldFast_search_testDTO);
					}
					if(mapOfFast_search_testDTOTohomeAddress.get(oldFast_search_testDTO.homeAddress).isEmpty()) {
						mapOfFast_search_testDTOTohomeAddress.remove(oldFast_search_testDTO.homeAddress);
					}
					
					if(mapOfFast_search_testDTOTojobCat.containsKey(oldFast_search_testDTO.jobCat)) {
						mapOfFast_search_testDTOTojobCat.get(oldFast_search_testDTO.jobCat).remove(oldFast_search_testDTO);
					}
					if(mapOfFast_search_testDTOTojobCat.get(oldFast_search_testDTO.jobCat).isEmpty()) {
						mapOfFast_search_testDTOTojobCat.remove(oldFast_search_testDTO.jobCat);
					}
					
					if(mapOfFast_search_testDTOTofilesDropzone.containsKey(oldFast_search_testDTO.filesDropzone)) {
						mapOfFast_search_testDTOTofilesDropzone.get(oldFast_search_testDTO.filesDropzone).remove(oldFast_search_testDTO);
					}
					if(mapOfFast_search_testDTOTofilesDropzone.get(oldFast_search_testDTO.filesDropzone).isEmpty()) {
						mapOfFast_search_testDTOTofilesDropzone.remove(oldFast_search_testDTO.filesDropzone);
					}
					
					if(mapOfFast_search_testDTOToinsertionDate.containsKey(oldFast_search_testDTO.insertionDate)) {
						mapOfFast_search_testDTOToinsertionDate.get(oldFast_search_testDTO.insertionDate).remove(oldFast_search_testDTO);
					}
					if(mapOfFast_search_testDTOToinsertionDate.get(oldFast_search_testDTO.insertionDate).isEmpty()) {
						mapOfFast_search_testDTOToinsertionDate.remove(oldFast_search_testDTO.insertionDate);
					}
					
					if(mapOfFast_search_testDTOTolastModificationTime.containsKey(oldFast_search_testDTO.lastModificationTime)) {
						mapOfFast_search_testDTOTolastModificationTime.get(oldFast_search_testDTO.lastModificationTime).remove(oldFast_search_testDTO);
					}
					if(mapOfFast_search_testDTOTolastModificationTime.get(oldFast_search_testDTO.lastModificationTime).isEmpty()) {
						mapOfFast_search_testDTOTolastModificationTime.remove(oldFast_search_testDTO.lastModificationTime);
					}
					
					
				}
				if(fast_search_testDTO.isDeleted == 0) 
				{
					
					mapOfFast_search_testDTOToiD.put(fast_search_testDTO.iD, fast_search_testDTO);
				
					if( ! mapOfFast_search_testDTOToreligionCat.containsKey(fast_search_testDTO.religionCat)) {
						mapOfFast_search_testDTOToreligionCat.put(fast_search_testDTO.religionCat, new HashSet<>());
					}
					mapOfFast_search_testDTOToreligionCat.get(fast_search_testDTO.religionCat).add(fast_search_testDTO);
					
					if( ! mapOfFast_search_testDTOTomedicalEquipmentNameType.containsKey(fast_search_testDTO.medicalEquipmentNameType)) {
						mapOfFast_search_testDTOTomedicalEquipmentNameType.put(fast_search_testDTO.medicalEquipmentNameType, new HashSet<>());
					}
					mapOfFast_search_testDTOTomedicalEquipmentNameType.get(fast_search_testDTO.medicalEquipmentNameType).add(fast_search_testDTO);
					
					if( ! mapOfFast_search_testDTOTonameEn.containsKey(fast_search_testDTO.nameEn)) {
						mapOfFast_search_testDTOTonameEn.put(fast_search_testDTO.nameEn, new HashSet<>());
					}
					mapOfFast_search_testDTOTonameEn.get(fast_search_testDTO.nameEn).add(fast_search_testDTO);
					
					if( ! mapOfFast_search_testDTOTonameBn.containsKey(fast_search_testDTO.nameBn)) {
						mapOfFast_search_testDTOTonameBn.put(fast_search_testDTO.nameBn, new HashSet<>());
					}
					mapOfFast_search_testDTOTonameBn.get(fast_search_testDTO.nameBn).add(fast_search_testDTO);
					
					if( ! mapOfFast_search_testDTOToinsertedByUserId.containsKey(fast_search_testDTO.insertedByUserId)) {
						mapOfFast_search_testDTOToinsertedByUserId.put(fast_search_testDTO.insertedByUserId, new HashSet<>());
					}
					mapOfFast_search_testDTOToinsertedByUserId.get(fast_search_testDTO.insertedByUserId).add(fast_search_testDTO);
					
					if( ! mapOfFast_search_testDTOToinsertedByOrganogramId.containsKey(fast_search_testDTO.insertedByOrganogramId)) {
						mapOfFast_search_testDTOToinsertedByOrganogramId.put(fast_search_testDTO.insertedByOrganogramId, new HashSet<>());
					}
					mapOfFast_search_testDTOToinsertedByOrganogramId.get(fast_search_testDTO.insertedByOrganogramId).add(fast_search_testDTO);
					
					if( ! mapOfFast_search_testDTOTosearchColumn.containsKey(fast_search_testDTO.searchColumn)) {
						mapOfFast_search_testDTOTosearchColumn.put(fast_search_testDTO.searchColumn, new HashSet<>());
					}
					mapOfFast_search_testDTOTosearchColumn.get(fast_search_testDTO.searchColumn).add(fast_search_testDTO);
					
					if( ! mapOfFast_search_testDTOTohomeAddress.containsKey(fast_search_testDTO.homeAddress)) {
						mapOfFast_search_testDTOTohomeAddress.put(fast_search_testDTO.homeAddress, new HashSet<>());
					}
					mapOfFast_search_testDTOTohomeAddress.get(fast_search_testDTO.homeAddress).add(fast_search_testDTO);
					
					if( ! mapOfFast_search_testDTOTojobCat.containsKey(fast_search_testDTO.jobCat)) {
						mapOfFast_search_testDTOTojobCat.put(fast_search_testDTO.jobCat, new HashSet<>());
					}
					mapOfFast_search_testDTOTojobCat.get(fast_search_testDTO.jobCat).add(fast_search_testDTO);
					
					if( ! mapOfFast_search_testDTOTofilesDropzone.containsKey(fast_search_testDTO.filesDropzone)) {
						mapOfFast_search_testDTOTofilesDropzone.put(fast_search_testDTO.filesDropzone, new HashSet<>());
					}
					mapOfFast_search_testDTOTofilesDropzone.get(fast_search_testDTO.filesDropzone).add(fast_search_testDTO);
					
					if( ! mapOfFast_search_testDTOToinsertionDate.containsKey(fast_search_testDTO.insertionDate)) {
						mapOfFast_search_testDTOToinsertionDate.put(fast_search_testDTO.insertionDate, new HashSet<>());
					}
					mapOfFast_search_testDTOToinsertionDate.get(fast_search_testDTO.insertionDate).add(fast_search_testDTO);
					
					if( ! mapOfFast_search_testDTOTolastModificationTime.containsKey(fast_search_testDTO.lastModificationTime)) {
						mapOfFast_search_testDTOTolastModificationTime.put(fast_search_testDTO.lastModificationTime, new HashSet<>());
					}
					mapOfFast_search_testDTOTolastModificationTime.get(fast_search_testDTO.lastModificationTime).add(fast_search_testDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Fast_search_testDTO> getFast_search_testList() {
		List <Fast_search_testDTO> fast_search_tests = new ArrayList<Fast_search_testDTO>(this.mapOfFast_search_testDTOToiD.values());
		return fast_search_tests;
	}
	
	
	public Fast_search_testDTO getFast_search_testDTOByID( long ID){
		return mapOfFast_search_testDTOToiD.get(ID);
	}
	
	
	public List<Fast_search_testDTO> getFast_search_testDTOByreligion_cat(long religion_cat) {
		return new ArrayList<>( mapOfFast_search_testDTOToreligionCat.getOrDefault(religion_cat,new HashSet<>()));
	}
	
	
	public List<Fast_search_testDTO> getFast_search_testDTOBymedical_equipment_name_type(long medical_equipment_name_type) {
		return new ArrayList<>( mapOfFast_search_testDTOTomedicalEquipmentNameType.getOrDefault(medical_equipment_name_type,new HashSet<>()));
	}
	
	
	public List<Fast_search_testDTO> getFast_search_testDTOByname_en(String name_en) {
		return new ArrayList<>( mapOfFast_search_testDTOTonameEn.getOrDefault(name_en,new HashSet<>()));
	}
	
	
	public List<Fast_search_testDTO> getFast_search_testDTOByname_bn(String name_bn) {
		return new ArrayList<>( mapOfFast_search_testDTOTonameBn.getOrDefault(name_bn,new HashSet<>()));
	}
	
	
	public List<Fast_search_testDTO> getFast_search_testDTOByinserted_by_user_id(long inserted_by_user_id) {
		return new ArrayList<>( mapOfFast_search_testDTOToinsertedByUserId.getOrDefault(inserted_by_user_id,new HashSet<>()));
	}
	
	
	public List<Fast_search_testDTO> getFast_search_testDTOByinserted_by_organogram_id(long inserted_by_organogram_id) {
		return new ArrayList<>( mapOfFast_search_testDTOToinsertedByOrganogramId.getOrDefault(inserted_by_organogram_id,new HashSet<>()));
	}
	
	
	public List<Fast_search_testDTO> getFast_search_testDTOBysearch_column(String search_column) {
		return new ArrayList<>( mapOfFast_search_testDTOTosearchColumn.getOrDefault(search_column,new HashSet<>()));
	}
	
	
	public List<Fast_search_testDTO> getFast_search_testDTOByhome_address(String home_address) {
		return new ArrayList<>( mapOfFast_search_testDTOTohomeAddress.getOrDefault(home_address,new HashSet<>()));
	}
	
	
	public List<Fast_search_testDTO> getFast_search_testDTOByjob_cat(int job_cat) {
		return new ArrayList<>( mapOfFast_search_testDTOTojobCat.getOrDefault(job_cat,new HashSet<>()));
	}
	
	
	public List<Fast_search_testDTO> getFast_search_testDTOByfiles_dropzone(long files_dropzone) {
		return new ArrayList<>( mapOfFast_search_testDTOTofilesDropzone.getOrDefault(files_dropzone,new HashSet<>()));
	}
	
	
	public List<Fast_search_testDTO> getFast_search_testDTOByinsertion_date(long insertion_date) {
		return new ArrayList<>( mapOfFast_search_testDTOToinsertionDate.getOrDefault(insertion_date,new HashSet<>()));
	}
	
	
	public List<Fast_search_testDTO> getFast_search_testDTOBylastModificationTime(long lastModificationTime) {
		return new ArrayList<>( mapOfFast_search_testDTOTolastModificationTime.getOrDefault(lastModificationTime,new HashSet<>()));
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "fast_search_test";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


