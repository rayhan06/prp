package fast_search_test;
import java.util.*; 
import util.*;


public class FastSearchTestChildMAPS extends CommonMaps
{	
	public FastSearchTestChildMAPS(String tableName)
	{
		

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("fastSearchTestId".toLowerCase(), "fastSearchTestId".toLowerCase());
		java_DTO_map.put("religionCat".toLowerCase(), "religionCat".toLowerCase());
		java_DTO_map.put("medicalEquipmentNameType".toLowerCase(), "medicalEquipmentNameType".toLowerCase());
		java_DTO_map.put("nameEn".toLowerCase(), "nameEn".toLowerCase());
		java_DTO_map.put("nameBn".toLowerCase(), "nameBn".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("fast_search_test_id".toLowerCase(), "fastSearchTestId".toLowerCase());
		java_SQL_map.put("religion_cat".toLowerCase(), "religionCat".toLowerCase());
		java_SQL_map.put("medical_equipment_name_type".toLowerCase(), "medicalEquipmentNameType".toLowerCase());
		java_SQL_map.put("name_en".toLowerCase(), "nameEn".toLowerCase());
		java_SQL_map.put("name_bn".toLowerCase(), "nameBn".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Fast Search Test Id".toLowerCase(), "fastSearchTestId".toLowerCase());
		java_Text_map.put("Religion".toLowerCase(), "religionCat".toLowerCase());
		java_Text_map.put("Medical Equipment Name".toLowerCase(), "medicalEquipmentNameType".toLowerCase());
		java_Text_map.put("English Name".toLowerCase(), "nameEn".toLowerCase());
		java_Text_map.put("Bangla Name".toLowerCase(), "nameBn".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}