package fast_search_test;
import java.util.*; 
import util.*; 


public class Fast_search_testDTO extends CommonDTO
{

	public long religionCat = -1;
	public long medicalEquipmentNameType = -1;
    public String nameEn = "";
    public String nameBn = "";
	public long insertedByUserId = -1;
	public long insertedByOrganogramId = -1;
    public String homeAddress = "";
	public long filesDropzone = -1;
	public long insertionDate = -1;
	
	public List<FastSearchTestChildDTO> fastSearchTestChildDTOList = new ArrayList<>();
	
    @Override
	public String toString() {
            return "$Fast_search_testDTO[" +
            " iD = " + iD +
            " religionCat = " + religionCat +
            " medicalEquipmentNameType = " + medicalEquipmentNameType +
            " nameEn = " + nameEn +
            " nameBn = " + nameBn +
            " insertedByUserId = " + insertedByUserId +
            " insertedByOrganogramId = " + insertedByOrganogramId +
            " searchColumn = " + searchColumn +
            " homeAddress = " + homeAddress +
            " jobCat = " + jobCat +
            " filesDropzone = " + filesDropzone +
            " insertionDate = " + insertionDate +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}