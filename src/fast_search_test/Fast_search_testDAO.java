package fast_search_test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;
import sessionmanager.SessionConstants;
import java.sql.SQLException;


import org.apache.log4j.Logger;

import dbm.*;

import repository.RepositoryManager;

import util.*;
import pb.*;
import user.UserDTO;

public class Fast_search_testDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());
	

	
	public Fast_search_testDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";
		commonMaps = new Fast_search_testMAPS(tableName);
		approvalMaps = new Fast_search_testApprovalMAPS("fast_search_test");
		columnNames = new String[] 
		{
			"ID",
			"religion_cat",
			"medical_equipment_name_type",
			"name_en",
			"name_bn",
			"inserted_by_user_id",
			"inserted_by_organogram_id",
			"search_column",
			"home_address",
			"job_cat",
			"files_dropzone",
			"insertion_date",
			"isDeleted",
			"lastModificationTime"
		};
	}
	
	public Fast_search_testDAO()
	{
		this("fast_search_test");		
	}
	
	public void setSearchColumn(Fast_search_testDTO fast_search_testDTO)
	{
		fast_search_testDTO.searchColumn = "";
		fast_search_testDTO.searchColumn += CatDAO.getName("English", "religion", fast_search_testDTO.religionCat) + " " + CatDAO.getName("Bangla", "religion", fast_search_testDTO.religionCat) + " ";
		fast_search_testDTO.searchColumn += CommonDAO.getName("English", "medical_equipment_name", fast_search_testDTO.medicalEquipmentNameType) + " " + CommonDAO.getName("Bangla", "medical_equipment_name", fast_search_testDTO.medicalEquipmentNameType) + " ";
		fast_search_testDTO.searchColumn += fast_search_testDTO.nameEn + " ";
		fast_search_testDTO.searchColumn += fast_search_testDTO.nameBn + " ";
		
	}
	
	public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException
	{
		Fast_search_testDTO fast_search_testDTO = (Fast_search_testDTO)commonDTO;
		int index = 1;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(fast_search_testDTO);
		if(isInsert)
		{
			ps.setObject(index++,fast_search_testDTO.iD);
		}
		ps.setObject(index++,fast_search_testDTO.religionCat);
		ps.setObject(index++,fast_search_testDTO.medicalEquipmentNameType);
		ps.setObject(index++,fast_search_testDTO.nameEn);
		ps.setObject(index++,fast_search_testDTO.nameBn);
		ps.setObject(index++,fast_search_testDTO.insertedByUserId);
		ps.setObject(index++,fast_search_testDTO.insertedByOrganogramId);
		ps.setObject(index++,fast_search_testDTO.searchColumn);
		ps.setObject(index++,fast_search_testDTO.homeAddress);
		ps.setObject(index++,fast_search_testDTO.jobCat);
		ps.setObject(index++,fast_search_testDTO.filesDropzone);
		ps.setObject(index++,fast_search_testDTO.insertionDate);
		if(isInsert)
		{
			ps.setObject(index++, 0);
			ps.setObject(index++, lastModificationTime);
		}
	}
	
	public void get(Fast_search_testDTO fast_search_testDTO, ResultSet rs) throws SQLException
	{
		fast_search_testDTO.iD = rs.getLong("ID");
		fast_search_testDTO.religionCat = rs.getLong("religion_cat");
		fast_search_testDTO.medicalEquipmentNameType = rs.getLong("medical_equipment_name_type");
		fast_search_testDTO.nameEn = rs.getString("name_en");
		fast_search_testDTO.nameBn = rs.getString("name_bn");
		fast_search_testDTO.insertedByUserId = rs.getLong("inserted_by_user_id");
		fast_search_testDTO.insertedByOrganogramId = rs.getLong("inserted_by_organogram_id");
		fast_search_testDTO.searchColumn = rs.getString("search_column");
		fast_search_testDTO.homeAddress = rs.getString("home_address");
		fast_search_testDTO.jobCat = rs.getInt("job_cat");
		fast_search_testDTO.filesDropzone = rs.getLong("files_dropzone");
		fast_search_testDTO.insertionDate = rs.getLong("insertion_date");
		fast_search_testDTO.isDeleted = rs.getInt("isDeleted");
		fast_search_testDTO.lastModificationTime = rs.getLong("lastModificationTime");
	}
	
	
	
	
		
	

	//need another getter for repository
	public Fast_search_testDTO getDTOByID (long ID)
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Fast_search_testDTO fast_search_testDTO = null;
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
			
            sql += " WHERE ID=" + ID;
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			if(rs.next()){
				fast_search_testDTO = new Fast_search_testDTO();

				get(fast_search_testDTO, rs);

			}			
			
			
			
			FastSearchTestChildDAO fastSearchTestChildDAO = new FastSearchTestChildDAO("fast_search_test_child");			
			List<FastSearchTestChildDTO> fastSearchTestChildDTOList = fastSearchTestChildDAO.getFastSearchTestChildDTOListByFastSearchTestID(fast_search_testDTO.iD);
			fast_search_testDTO.fastSearchTestChildDTOList = fastSearchTestChildDTOList;
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return fast_search_testDTO;
	}
	
	
	
	
	public List<Fast_search_testDTO> getDTOs(Collection recordIDs){
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Fast_search_testDTO fast_search_testDTO = null;
		List<Fast_search_testDTO> fast_search_testDTOList = new ArrayList<>();
		if(recordIDs.isEmpty()){
			return fast_search_testDTOList;
		}
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
            
            sql += " WHERE ID IN ( ";

			for(int i = 0;i<recordIDs.size();i++){
				if(i!=0){
					sql+=",";
				}
				sql+=((ArrayList)recordIDs).get(i);
			}
			sql+=")  order by lastModificationTime desc";
			
			printSql(sql);
			
			logger.debug("sql " + sql);
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				fast_search_testDTO = new Fast_search_testDTO();
				get(fast_search_testDTO, rs);
				System.out.println("got this DTO: " + fast_search_testDTO);
				
				fast_search_testDTOList.add(fast_search_testDTO);

			}			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return fast_search_testDTOList;
	
	}
	
	

	
	
	
	//add repository
	public List<Fast_search_testDTO> getAllFast_search_test (boolean isFirstReload)
    {
		List<Fast_search_testDTO> fast_search_testDTOList = new ArrayList<>();

		String sql = "SELECT * FROM fast_search_test";
		sql += " WHERE ";
	

		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by fast_search_test.lastModificationTime desc";
		printSql(sql);
		
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;

		
		try{
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();
			rs = stmt.executeQuery(sql);
			

			while(rs.next()){
				Fast_search_testDTO fast_search_testDTO = new Fast_search_testDTO();
				get(fast_search_testDTO, rs);
				
				fast_search_testDTOList.add(fast_search_testDTO);
			}			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}

		return fast_search_testDTOList;
    }

	
	public List<Fast_search_testDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO)
	{
		return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
	}

	
	public List<Fast_search_testDTO> getDTOs(Hashtable  p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
			String filter, boolean tableHasJobCat)
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		List<Fast_search_testDTO> fast_search_testDTOList = new ArrayList<>();

		try{
			
			String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat);
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				Fast_search_testDTO fast_search_testDTO = new Fast_search_testDTO();
				get(fast_search_testDTO, rs);
				
				fast_search_testDTOList.add(fast_search_testDTO);
			}					
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return fast_search_testDTOList;
	
	}
	public String getSqlWithSearchCriteriaForNormalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
			UserDTO userDTO, String filter, boolean tableHasJobCat)
    {
		boolean viewAll = false;
		
		if(p_searchCriteria != null && p_searchCriteria.get("ViewAll") != null)
		{
			System.out.println("ViewAll = " + p_searchCriteria.get("ViewAll"));
			viewAll = true;
		}
		else
		{
			System.out.println("ViewAll is null ");
		}
		
		String sql = "SELECT ";
		if(category == GETIDS)
		{
			sql += " " + tableName + ".ID as ID ";
		}
		else if(category == GETCOUNT)
		{
			sql += " count( " + tableName + ".ID) as countID ";
		}
		else if(category == GETDTOS)
		{
			sql += " distinct " + tableName + ".* ";
		}
		sql += "FROM " + tableName + " ";
		sql += joinSQL;
		

		
		String AnyfieldSql = "";
		String AllFieldSql = "";
		
		if(p_searchCriteria != null)
		{
						
			
			Enumeration names = p_searchCriteria.keys();
			String str, value;
			
			AnyfieldSql = "(";
			
			if(p_searchCriteria.get("AnyField")!= null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase(""))
			{
				AnyfieldSql+= tableName + ".search_column like '%" + p_searchCriteria.get("AnyField").toString() + "%'";			
			}
			AnyfieldSql += ")";
			System.out.println("AnyfieldSql = " + AnyfieldSql);
			
			AllFieldSql = "(";
			int i = 0;
			while(names.hasMoreElements())
			{				
				str = (String) names.nextElement();
				value = (String)p_searchCriteria.get(str);
		        System.out.println(str + ": " + value);
				if(value != null && !value.equalsIgnoreCase("") && (
						 str.equals("religion_cat")
						|| str.equals("medical_equipment_name_type")
						|| str.equals("name_en")
						|| str.equals("name_bn")
						|| str.equals("insertion_date_start")
						|| str.equals("insertion_date_end")
				)
						
				)
				{
					if(p_searchCriteria.get(str).equals("any"))
					{
						continue;
					}

					if( i > 0)
					{
						AllFieldSql+= " AND  ";
					}
					
					 if(str.equals("religion_cat"))
					{
						AllFieldSql += "" + tableName + ".religion_cat = " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("medical_equipment_name_type"))
					{
						AllFieldSql += "" + tableName + ".medical_equipment_name_type = " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("name_en"))
					{
						AllFieldSql += "" + tableName + ".name_en like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("name_bn"))
					{
						AllFieldSql += "" + tableName + ".name_bn like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("insertion_date_start"))
					{
						AllFieldSql += "" + tableName + ".insertion_date >= " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("insertion_date_end"))
					{
						AllFieldSql += "" + tableName + ".insertion_date <= " + p_searchCriteria.get(str);
						i ++;
					}
					
					
				}
			}
			
			AllFieldSql += ")";
			System.out.println("AllFieldSql = " + AllFieldSql);
			
			
		}
		
		
		sql += " WHERE ";
		
		sql += " (" + tableName + ".isDeleted = 0 ";			
		sql += ")";
		
		
		if(!filter.equalsIgnoreCase(""))
		{
			sql += " and " + filter + " ";
		}
		
		if(!AnyfieldSql.equals("()") && !AnyfieldSql.equals(""))
		{
			sql += " AND " + AnyfieldSql;
			
		}
		if(!AllFieldSql.equals("()") && !AllFieldSql.equals(""))
		{			
			sql += " AND " + AllFieldSql;
		}
		
	
			
		sql += " order by " + tableName + ".lastModificationTime desc ";

		printSql(sql);
		
		if(limit >= 0)
		{
			sql += " limit " + limit;
		}
		if(offset >= 0)
		{
			sql += " offset " + offset;
		}
		
		System.out.println("-------------- sql = " + sql);
		
		return sql;
    }
	
	public String getSqlWithSearchCriteria(Hashtable p_searchCriteria, int limit, int offset, int category,
			boolean isPermanentTable, UserDTO userDTO, String filter, boolean tableHasJobCat)
    {
		if(isPermanentTable)
		{
			return getSqlWithSearchCriteriaForNormalSearch(p_searchCriteria, limit, offset, category, userDTO, filter, tableHasJobCat);
		}
		else
		{
			return getSqlWithSearchCriteriaForApprovalSearch(p_searchCriteria, limit, offset, category, userDTO, filter, tableHasJobCat);
		}
				
    }
				
}
	