package fast_search_test;
import util.*;

public class Fast_search_testApprovalMAPS extends CommonMaps
{	
	public Fast_search_testApprovalMAPS(String tableName)
	{
		
		java_allfield_type_map.put("religion_cat".toLowerCase(), "Long");
		java_allfield_type_map.put("medical_equipment_name_type".toLowerCase(), "Long");
		java_allfield_type_map.put("name_en".toLowerCase(), "String");
		java_allfield_type_map.put("name_bn".toLowerCase(), "String");
		java_allfield_type_map.put("inserted_by_user_id".toLowerCase(), "Long");
		java_allfield_type_map.put("inserted_by_organogram_id".toLowerCase(), "Long");
		java_allfield_type_map.put("home_address".toLowerCase(), "String");
		java_allfield_type_map.put("insertion_date".toLowerCase(), "Long");

		java_allfield_type_map.put("job_cat", "Integer");
		java_allfield_type_map.put("approval_status_cat", "Integer");
		java_allfield_type_map.put("initiator", "Long");
		java_allfield_type_map.put("assigned_to", "Long");
		java_allfield_type_map.put("starting_date", "Long");
		java_allfield_type_map.put("ending_date", "Long");
		
		java_table_map.put("approval_status_cat", "approval_summary");
		java_table_map.put("initiator", "approval_summary");
		java_table_map.put("assigned_to", "approval_summary");
	}

}