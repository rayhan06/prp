package fast_search_test;

import java.io.IOException;
import java.io.*;
import java.text.SimpleDateFormat;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import org.apache.log4j.Logger;

import login.LoginDTO;
import permission.MenuConstants;
import role.PermissionRepository;


import sessionmanager.SessionConstants;

import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;
import util.CommonRequestHandler;
import util.RecordNavigationManager4;

import java.util.*;
import javax.servlet.http.*;
import java.util.UUID;





import geolocation.GeoLocationDAO2;
import java.util.StringTokenizer;

import com.google.gson.Gson;

import pb.*;
import pbReport.*;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;

import files.*;
import workflow.WorkflowController;
import approval_execution_table.*;
import pb_notifications.Pb_notificationsDAO;



/**
 * Servlet implementation class Fast_search_testServlet
 */
@WebServlet("/Fast_search_testServlet")
@MultipartConfig
public class Fast_search_testServlet extends HttpServlet 
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Fast_search_testServlet.class);

    String tableName = "fast_search_test";

	Fast_search_testDAO fast_search_testDAO;
	CommonRequestHandler commonRequestHandler;
	FilesDAO filesDAO = new FilesDAO();
	FastSearchTestChildDAO fastSearchTestChildDAO;
    private final Gson gson = new Gson();
    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Fast_search_testServlet() 
	{
        super();
    	try
    	{
			fast_search_testDAO = new Fast_search_testDAO(tableName);
			fastSearchTestChildDAO = new FastSearchTestChildDAO("fast_search_test_child");
			commonRequestHandler = new CommonRequestHandler(fast_search_testDAO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }   

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		System.out.println("In doget request = " + request);
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		try
		{
			String actionType = request.getParameter("actionType");
			if(actionType.equals("getAddPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.FAST_SEARCH_TEST_ADD))
				{
					commonRequestHandler.getAddPage(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getEditPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.FAST_SEARCH_TEST_UPDATE))
				{
					getFast_search_test(request, response);					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}						
			}
			else if(actionType.equals("downloadDropzoneFile"))
			{
				commonRequestHandler.downloadDropzoneFile(request, response, filesDAO);					
			}
			else if(actionType.equals("DeleteFileFromDropZone"))
			{
				commonRequestHandler.deleteFileFromDropZone(request, response, filesDAO);
			}
			else if(actionType.equals("getURL"))
			{
				String URL = request.getParameter("URL");
				System.out.println("URL = " + URL);
				response.sendRedirect(URL);			
			}			
			else if(actionType.equals("search"))
			{
				System.out.println("search requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.FAST_SEARCH_TEST_SEARCH))
				{
					if(isPermanentTable)
					{
						String filter = request.getParameter("filter");
						System.out.println("filter = " + filter);
						if(filter!=null)
						{
							filter = ""; //shouldn't be directly used, rather manipulate it.
							searchFast_search_test(request, response, isPermanentTable, filter);
						}
						else
						{
							searchFast_search_test(request, response, isPermanentTable, "");
						}
					}					
					else
					{
						//searchFast_search_test(request, response, tempTableName, isPermanentTable);
					}
				}			
			}
			else if(actionType.equals("getApprovalPage"))
			{
				System.out.println("Fast_search_test getApprovalPage requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.FAST_SEARCH_TEST_APPROVE))
				{
					searchFast_search_test(request, response, false, "");
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}				
			}
			else if(actionType.equals("viewApprovalNotification"))
			{
				System.out.println("view requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.FAST_SEARCH_TEST_APPROVE))
				{
					commonRequestHandler.viewApprovalNotification(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}				
			}
			else if(actionType.equals("view"))
			{
				System.out.println("view requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.FAST_SEARCH_TEST_SEARCH))
				{
					commonRequestHandler.view(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else
			{
				request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}


	
	

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		// TODO Auto-generated method stub
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		System.out.println("doPost");
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		
		try
		{
			String actionType = request.getParameter("actionType");
			System.out.println("actionType = " + actionType);
			if(actionType.equals("add"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.FAST_SEARCH_TEST_ADD))
				{
					System.out.println("going to  addFast_search_test ");
					addFast_search_test(request, response, true, userDTO, true);
				}
				else
				{
					System.out.println("Not going to  addFast_search_test ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else if(actionType.equals("UploadFilesFromDropZone"))
			{
				commonRequestHandler.UploadFilesFromDropZone(request, response, userDTO);
			}

			else if(actionType.equals("SendToApprovalPath"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.FAST_SEARCH_TEST_APPROVE))
				{
					commonRequestHandler.sendToApprovalPath(request, response, userDTO);
				}
				else
				{
					System.out.println("Not going to SendToApprovalPath ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}
			else if(actionType.equals("approve"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.FAST_SEARCH_TEST_ADD))
				{					
					commonRequestHandler.approve(request, response, true, userDTO, true);
				}
				else
				{
					System.out.println("Not going to  addFast_search_test ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else if(actionType.equals("reject"))
			{
				System.out.println("trying to approve");
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.FAST_SEARCH_TEST_APPROVE))
				{					
					commonRequestHandler.approve(request, response, true, userDTO, false);
				}
				else
				{
					System.out.println("Not going to  addFast_search_test ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}				
			}
			else if(actionType.equals("terminate"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.FAST_SEARCH_TEST_ADD))
				{
					commonRequestHandler.terminate(request, response, userDTO);
				}
				else
				{
					System.out.println("Not going to  addFast_search_test ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}
			else if(actionType.equals("skipStep"))
			{
				
				System.out.println("skipStep");
				commonRequestHandler.skipStep(request, response, userDTO);									
			}
			else if(actionType.equals("getDTO"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.FAST_SEARCH_TEST_ADD))
				{
					getDTO(request, response);					
				}
				else
				{
					System.out.println("Not going to  addFast_search_test ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else if(actionType.equals("edit"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.FAST_SEARCH_TEST_UPDATE))
				{					
					addFast_search_test(request, response, false, userDTO, isPermanentTable);					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("delete"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.FAST_SEARCH_TEST_UPDATE))
				{
					commonRequestHandler.delete(request, response, userDTO);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}	
			else if(actionType.equals("search"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.FAST_SEARCH_TEST_SEARCH))
				{
					searchFast_search_test(request, response, true, "");
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getGeo"))
			{
				System.out.println("going to geoloc ");
				request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
			}
			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}
	
	private void getDTO(HttpServletRequest request, HttpServletResponse response) 
	{
		try 
		{
			System.out.println("In getDTO");
			Fast_search_testDTO fast_search_testDTO = fast_search_testDAO.getDTOByID(Long.parseLong(request.getParameter("ID")));
			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			
			String encoded = this.gson.toJson(fast_search_testDTO);
			System.out.println("json encoded data = " + encoded);
			out.print(encoded);
			out.flush();
		}
		catch (NumberFormatException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	private void addFast_search_test(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, boolean isPermanentTable) throws IOException 
	{
		// TODO Auto-generated method stub
		try 
		{
			request.setAttribute("failureMessage", "");
			System.out.println("%%%% addFast_search_test");
			String path = getServletContext().getRealPath("/img2/");
			Fast_search_testDTO fast_search_testDTO;
			SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
						
			if(addFlag == true)
			{
				fast_search_testDTO = new Fast_search_testDTO();
			}
			else
			{
				fast_search_testDTO = fast_search_testDAO.getDTOByID(Long.parseLong(request.getParameter("iD")));
			}
			
			String Value = "";

			Value = request.getParameter("religionCat");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("religionCat = " + Value);
			if(Value != null)
			{
				
				fast_search_testDTO.religionCat = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("medicalEquipmentNameType");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("medicalEquipmentNameType = " + Value);
			if(Value != null)
			{
				
				fast_search_testDTO.medicalEquipmentNameType = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("nameEn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("nameEn = " + Value);
			if(Value != null)
			{
				
				fast_search_testDTO.nameEn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("nameBn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("nameBn = " + Value);
			if(Value != null)
			{
				
				fast_search_testDTO.nameBn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			if(addFlag)
			{
				fast_search_testDTO.insertedByUserId = userDTO.ID;
			}


			if(addFlag)
			{
				fast_search_testDTO.insertedByOrganogramId = userDTO.organogramID;
			}


			Value = request.getParameter("searchColumn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("searchColumn = " + Value);
			if(Value != null)
			{
				
				fast_search_testDTO.searchColumn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("homeAddress");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("homeAddress = " + Value);
			if(Value != null)
			{
				
				fast_search_testDTO.homeAddress = GeoLocationDAO2.getAddressToSave(Value);				
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("jobCat");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("jobCat = " + Value);
			if(Value != null)
			{
				
				fast_search_testDTO.jobCat = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("filesDropzone");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("filesDropzone = " + Value);
			if(Value != null)
			{
				
				
				System.out.println("filesDropzone = " + Value);
				
				fast_search_testDTO.filesDropzone = Long.parseLong(Value);
				
				
				if(addFlag == false)
				{
					String filesDropzoneFilesToDelete = request.getParameter("filesDropzoneFilesToDelete");
					String[] deleteArray = filesDropzoneFilesToDelete.split(",");
					for(int i = 0; i < deleteArray.length; i ++)
					{
						System.out.println("going to delete " + deleteArray[i]);
						if(i>0)
						{
							filesDAO.delete(Long.parseLong(deleteArray[i]));
						}
					}
				}
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			if(addFlag)
			{
				Calendar c = Calendar.getInstance();
				c.set(Calendar.HOUR_OF_DAY, 0);
				c.set(Calendar.MINUTE, 0);
				c.set(Calendar.SECOND, 0);
				c.set(Calendar.MILLISECOND, 0);
				
				fast_search_testDTO.insertionDate = c.getTimeInMillis();
			}			

			
			System.out.println("Done adding  addFast_search_test dto = " + fast_search_testDTO);
			long returnedID = -1;
			
			if(isPermanentTable == false) //add new row for validation and make the old row outdated
			{
				fast_search_testDAO.setIsDeleted(fast_search_testDTO.iD, CommonDTO.OUTDATED);
				returnedID = fast_search_testDAO.add(fast_search_testDTO);
				fast_search_testDAO.setIsDeleted(returnedID, CommonDTO.WAITING_FOR_APPROVAL);
			}
			else if(addFlag == true)
			{
				returnedID = fast_search_testDAO.manageWriteOperations(fast_search_testDTO, SessionConstants.INSERT, -1, userDTO);
			}
			else
			{				
				returnedID = fast_search_testDAO.manageWriteOperations(fast_search_testDTO, SessionConstants.UPDATE, -1, userDTO);											
			}
			
			
			
			
			
			List<FastSearchTestChildDTO> fastSearchTestChildDTOList = createFastSearchTestChildDTOListByRequest(request);			
			if(addFlag == true || isPermanentTable == false) //add or validate
			{
				if(fastSearchTestChildDTOList != null)
				{				
					for(FastSearchTestChildDTO fastSearchTestChildDTO: fastSearchTestChildDTOList)
					{
						fastSearchTestChildDTO.fastSearchTestId = fast_search_testDTO.iD; 
						fastSearchTestChildDAO.add(fastSearchTestChildDTO);
					}
				}
			
			}
			else
			{
				List<Long> childIdsFromRequest = fastSearchTestChildDAO.getChildIdsFromRequest(request, "fastSearchTestChild");
				//delete the removed children
				fastSearchTestChildDAO.deleteChildrenNotInList("fast_search_test", "fast_search_test_child", fast_search_testDTO.iD, childIdsFromRequest);
				List<Long> childIDsInDatabase = fastSearchTestChildDAO.getChilIds("fast_search_test", "fast_search_test_child", fast_search_testDTO.iD);
				
				
				if(childIdsFromRequest != null)
				{
					for(int i = 0; i < childIdsFromRequest.size(); i ++)
					{
						Long childIDFromRequest = childIdsFromRequest.get(i);
						if(childIDsInDatabase.contains(childIDFromRequest))
						{
							FastSearchTestChildDTO fastSearchTestChildDTO =  createFastSearchTestChildDTOByRequestAndIndex(request, false, i);
							fastSearchTestChildDTO.fastSearchTestId = fast_search_testDTO.iD; 
							fastSearchTestChildDAO.update(fastSearchTestChildDTO);
						}
						else
						{
							FastSearchTestChildDTO fastSearchTestChildDTO =  createFastSearchTestChildDTOByRequestAndIndex(request, true, i);
							fastSearchTestChildDTO.fastSearchTestId = fast_search_testDTO.iD; 
							fastSearchTestChildDAO.add(fastSearchTestChildDTO);
						}
					}
				}
				else
				{
					fastSearchTestChildDAO.deleteFastSearchTestChildByFastSearchTestID(fast_search_testDTO.iD);
				}
				
			}
			
			
			
			
			
			if(isPermanentTable)
			{
				String inPlaceSubmit = request.getParameter("inplacesubmit");
				
				if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
				{
					getFast_search_test(request, response, returnedID);
				}
				else
				{
					response.sendRedirect("Fast_search_testServlet?actionType=search");
				}
			}
			else
			{
				commonRequestHandler.validate(fast_search_testDAO.getDTOByID(returnedID), request, response, userDTO);
			}
					
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	



	private List<FastSearchTestChildDTO> createFastSearchTestChildDTOListByRequest(HttpServletRequest request) throws Exception{ 
		List<FastSearchTestChildDTO> fastSearchTestChildDTOList = new ArrayList<FastSearchTestChildDTO>();
		if(request.getParameterValues("fastSearchTestChild.iD") != null) 
		{
			int fastSearchTestChildItemNo = request.getParameterValues("fastSearchTestChild.iD").length;
			
			
			for(int index=0;index<fastSearchTestChildItemNo;index++){
				FastSearchTestChildDTO fastSearchTestChildDTO = createFastSearchTestChildDTOByRequestAndIndex(request,true,index);
				fastSearchTestChildDTOList.add(fastSearchTestChildDTO);
			}
			
			return fastSearchTestChildDTOList;
		}
		return null;
	}
	
	
	private FastSearchTestChildDTO createFastSearchTestChildDTOByRequestAndIndex(HttpServletRequest request, boolean addFlag,int index) throws Exception{
	
		FastSearchTestChildDTO fastSearchTestChildDTO;
		if(addFlag == true )
		{
			fastSearchTestChildDTO = new FastSearchTestChildDTO();
		}
		else
		{
			fastSearchTestChildDTO = fastSearchTestChildDAO.getDTOByID(Long.parseLong(request.getParameterValues("fastSearchTestChild.iD")[index]));
		}
		String path = getServletContext().getRealPath("/img2/");
		SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
		
		
				
		String Value = "";
		Value = request.getParameterValues("fastSearchTestChild.fastSearchTestId")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		fastSearchTestChildDTO.fastSearchTestId = Long.parseLong(Value);
		Value = request.getParameterValues("fastSearchTestChild.religionCat")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		fastSearchTestChildDTO.religionCat = Long.parseLong(Value);
		Value = request.getParameterValues("fastSearchTestChild.medicalEquipmentNameType")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		fastSearchTestChildDTO.medicalEquipmentNameType = Long.parseLong(Value);
		Value = request.getParameterValues("fastSearchTestChild.nameEn")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		fastSearchTestChildDTO.nameEn = (Value);
		Value = request.getParameterValues("fastSearchTestChild.nameBn")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		fastSearchTestChildDTO.nameBn = (Value);
		return fastSearchTestChildDTO;
	
	}
	
	
	

	private void getFast_search_test(HttpServletRequest request, HttpServletResponse response, long id) throws ServletException, IOException
	{
		System.out.println("in getFast_search_test");
		Fast_search_testDTO fast_search_testDTO = null;
		try 
		{
			fast_search_testDTO = fast_search_testDAO.getDTOByID(id);
			boolean isPermanentTable = true;
			if(request.getParameter("isPermanentTable") != null)
			{
				isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
			}
			request.setAttribute("ID", fast_search_testDTO.iD);
			request.setAttribute("fast_search_testDTO",fast_search_testDTO);
			request.setAttribute("fast_search_testDAO",fast_search_testDAO);
			
			String URL= "";
			
			String inPlaceEdit = request.getParameter("inplaceedit");
			String inPlaceSubmit = request.getParameter("inplacesubmit");
			String getBodyOnly = request.getParameter("getBodyOnly");
			
			if(inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase(""))
			{
				URL = "fast_search_test/fast_search_testInPlaceEdit.jsp";	
				request.setAttribute("inplaceedit","");				
			}
			else if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
			{
				URL = "fast_search_test/fast_search_testSearchRow.jsp";
				request.setAttribute("inplacesubmit","");					
			}
			else
			{
				if(getBodyOnly  != null && !getBodyOnly.equalsIgnoreCase(""))
				{
					URL = "fast_search_test/fast_search_testEditBody.jsp?actionType=edit";
				}
				else
				{
					URL = "fast_search_test/fast_search_testEdit.jsp?actionType=edit";
				}				
			}
			
			RequestDispatcher rd = request.getRequestDispatcher(URL);
			rd.forward(request, response);
		}
		catch (NumberFormatException e) 
		{
			e.printStackTrace();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	
	private void getFast_search_test(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		getFast_search_test(request, response, Long.parseLong(request.getParameter("ID")));	
	}
	
	private void searchFast_search_test(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException
	{
		System.out.println("in  searchFast_search_test 1");
		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String ajax = request.getParameter("ajax");
		boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);
		
        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
			SessionConstants.NAV_FAST_SEARCH_TEST,
			request,
			fast_search_testDAO,
			SessionConstants.VIEW_FAST_SEARCH_TEST,
			SessionConstants.SEARCH_FAST_SEARCH_TEST,
			tableName,
			isPermanent,
			userDTO,
			filter,
			true);
        try
        {
			System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        }
        catch(Exception e)
        {
			System.out.println("failed to dojob" + e);
        }

		request.setAttribute("fast_search_testDAO",fast_search_testDAO);
        RequestDispatcher rd;
        if(!isPermanent)
        {
        	if(hasAjax == false)
	        {
	        	System.out.println("Going to fast_search_test/fast_search_testApproval.jsp");
	        	rd = request.getRequestDispatcher("fast_search_test/fast_search_testApproval.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to fast_search_test/fast_search_testApprovalForm.jsp");
	        	rd = request.getRequestDispatcher("fast_search_test/fast_search_testApprovalForm.jsp");
	        }
        }
        else
        {
	        if(hasAjax == false)
	        {
	        	System.out.println("Going to fast_search_test/fast_search_testSearch.jsp");
	        	rd = request.getRequestDispatcher("fast_search_test/fast_search_testSearch.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to fast_search_test/fast_search_testSearchForm.jsp");
	        	rd = request.getRequestDispatcher("fast_search_test/fast_search_testSearchForm.jsp");
	        }
        }
		rd.forward(request, response);
	}
	
}

