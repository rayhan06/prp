package fast_search_test;
import java.util.*; 
import util.*; 


public class FastSearchTestChildDTO extends CommonDTO
{

	public long fastSearchTestId = -1;
	public long religionCat = -1;
	public long medicalEquipmentNameType = -1;
    public String nameEn = "";
    public String nameBn = "";
	
	public List<FastSearchTestChildDTO> fastSearchTestChildDTOList = new ArrayList<>();
	
    @Override
	public String toString() {
            return "$FastSearchTestChildDTO[" +
            " iD = " + iD +
            " fastSearchTestId = " + fastSearchTestId +
            " religionCat = " + religionCat +
            " medicalEquipmentNameType = " + medicalEquipmentNameType +
            " nameEn = " + nameEn +
            " nameBn = " + nameBn +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}