package card_info;

import common.EmployeeCommonDAOService;
import employee_offices.EmployeeOfficeDTO;
import employee_offices.EmployeeOfficeRepository;
import employee_records.Employee_recordsDTO;
import employee_records.Employee_recordsRepository;
import org.apache.log4j.Logger;
import user.UserDTO;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

@SuppressWarnings({"Duplicates"})
public class CardEmployeeOfficeInfoDAO implements EmployeeCommonDAOService<CardEmployeeOfficeInfoDTO> {
    Logger logger = Logger.getLogger(getClass());
    private static final String addQuery = "INSERT INTO {tableName} (employee_records_id, emp_officer_cat, employee_class_cat, "
            .concat("employment_cat, user_name, office_unit_id, office_unit_eng, office_unit_bng,")
            .concat("organogram_id, organogram_eng, organogram_bng,")
            .concat("modified_by, lastModificationTime, inserted_by, insertion_time, isDeleted,id) ")
            .concat("VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

    private static final String updateQuery = "UPDATE {tableName} SET employee_records_id=?, emp_officer_cat=?, employee_class_cat=?, "
            .concat("employment_cat=?, user_name=?, office_unit_id=?, office_unit_eng=?, office_unit_bng=?,")
            .concat("organogram_id=?, organogram_eng=?, organogram_bng=?,  ")
            .concat("modified_by=?, lastModificationTime=?, inserted_by=?, insertion_time=?, isDeleted=? where id=? ");

    private static CardEmployeeOfficeInfoDAO INSTANCE = null;

    private CardEmployeeOfficeInfoDAO() {
    }

    public static CardEmployeeOfficeInfoDAO getInstance(){
        if(INSTANCE == null){
            synchronized (CardEmployeeOfficeInfoDAO.class){
                if(INSTANCE == null){
                    INSTANCE = new CardEmployeeOfficeInfoDAO();
                }
            }
        }
        return INSTANCE;
    }


    public void set(PreparedStatement ps, CardEmployeeOfficeInfoDTO card_employeeOfficeInfoDTO, boolean isInsert) throws SQLException {
        int index = 0;

        ps.setObject(++index, card_employeeOfficeInfoDTO.employeeRecordsId);
        ps.setObject(++index, card_employeeOfficeInfoDTO.empOfficerCat);
        ps.setObject(++index, card_employeeOfficeInfoDTO.employeeClassCat);
        ps.setObject(++index, card_employeeOfficeInfoDTO.employmentCat);
        ps.setObject(++index, card_employeeOfficeInfoDTO.userName);
        ps.setObject(++index, card_employeeOfficeInfoDTO.officeUnitId);
        ps.setObject(++index, card_employeeOfficeInfoDTO.officeUnitEng);
        ps.setObject(++index, card_employeeOfficeInfoDTO.officeUnitBng);
        ps.setObject(++index, card_employeeOfficeInfoDTO.organogramId);
        ps.setObject(++index, card_employeeOfficeInfoDTO.organogramEng);
        ps.setObject(++index, card_employeeOfficeInfoDTO.organogramBng);
        ps.setObject(++index, card_employeeOfficeInfoDTO.modifiedBy);
        ps.setLong(++index, card_employeeOfficeInfoDTO.lastModificationTime);
        if (isInsert) {
            ps.setLong(++index, card_employeeOfficeInfoDTO.insertedBy);
            ps.setLong(++index, card_employeeOfficeInfoDTO.insertionTime);
            ps.setInt(++index, 0);
        }

        ps.setLong(++index, card_employeeOfficeInfoDTO.iD);
    }

    @Override
    public CardEmployeeOfficeInfoDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            CardEmployeeOfficeInfoDTO dto = new CardEmployeeOfficeInfoDTO();
            dto.iD = rs.getLong("ID");
            dto.employeeRecordsId = rs.getLong("employee_records_id");
            dto.empOfficerCat = rs.getInt("emp_officer_cat");
            dto.employeeClassCat = rs.getInt("employee_class_cat");
            dto.employmentCat = rs.getInt("employment_cat");
            dto.userName = rs.getString("user_name");
            dto.officeUnitId = rs.getLong("office_unit_id");
            dto.officeUnitEng = rs.getString("office_unit_eng");
            dto.officeUnitBng = rs.getString("office_unit_bng");
            dto.organogramId = rs.getLong("organogram_id");
            dto.organogramEng = rs.getString("organogram_eng");
            dto.organogramBng = rs.getString("organogram_bng");
            dto.insertedBy = rs.getLong("inserted_by");
            dto.insertionTime = rs.getLong("insertion_time");
            dto.isDeleted = rs.getInt("isDeleted");
            dto.modifiedBy = rs.getLong("modified_by");
            dto.lastModificationTime = rs.getLong("lastModificationTime");
            return dto;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    @Override
    public String getTableName() {
        return "card_employee_office_info";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return null;
    }

    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((CardEmployeeOfficeInfoDTO) commonDTO, updateQuery, false);
    }

    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((CardEmployeeOfficeInfoDTO) commonDTO, addQuery, true);
    }

    public long getCardEmployeeOfficeInfoId(long employeeRecordId, UserDTO userDTO) throws Exception {
        EmployeeOfficeDTO employeeOfficeDTO = EmployeeOfficeRepository.getInstance().getByEmployeeRecordIdIsDefault(employeeRecordId);
        Employee_recordsDTO empDto = Employee_recordsRepository.getInstance().getById(employeeRecordId);
        CardEmployeeOfficeInfoDTO card_employeeOfficeInfoDTO = new CardEmployeeOfficeInfoDTO(empDto, employeeOfficeDTO);


        List<CardEmployeeOfficeInfoDTO> dtoList = getByEmployeeId(employeeRecordId);
        if (dtoList != null && dtoList.size() > 0) {
            CardEmployeeOfficeInfoDTO currentCardEmployeeOfficeInfoDTO = dtoList.get(0);
            if (currentCardEmployeeOfficeInfoDTO.hasSameData(card_employeeOfficeInfoDTO)) {
                return currentCardEmployeeOfficeInfoDTO.iD;
            }
            delete(userDTO.employee_record_id,currentCardEmployeeOfficeInfoDTO.iD);
        }

        card_employeeOfficeInfoDTO.insertedBy
                = card_employeeOfficeInfoDTO.modifiedBy
                = userDTO.employee_record_id;

        card_employeeOfficeInfoDTO.insertionTime
                = card_employeeOfficeInfoDTO.lastModificationTime
                = System.currentTimeMillis();

        return add(card_employeeOfficeInfoDTO);
    }
}
