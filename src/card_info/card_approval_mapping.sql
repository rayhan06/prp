CREATE TABLE card_approval_mapping
(
    id                   BIGINT(20) UNSIGNED PRIMARY KEY,
    card_info_id         BIGINT(20) NOT NULL,
    card_approval_id     BIGINT(20) NOT NULL,
    sequence             int(3) default 1,
    card_approval_status_cat  int(11) default 0,
    isDeleted            INT(11) DEFAULT 0,
    inserted_by          BIGINT(20) DEFAULT 0,
    insertion_time       BIGINT(20) DEFAULT 0,
    modified_by          BIGINT(20) DEFAULT 0,
    lastModificationTime BIGINT(20) DEFAULT 0
) COLLATE='utf8_general_ci'
ENGINE=MyISAM
;