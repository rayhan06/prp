package card_info;

/*
 * @author Md. Erfan Hossain
 * @created 03/05/2021 - 4:46 PM
 * @project parliament
 */

public class CardApproverNotFoundException extends Exception{
    public CardApproverNotFoundException(String message) {
        super(message);
    }
}
