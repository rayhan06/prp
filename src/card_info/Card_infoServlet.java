package card_info;

import card_approval_mapping.Card_approval_mappingDAO;
import card_approval_mapping.CreateCardApprovalModel;
import com.google.gson.Gson;
import common.ApiResponse;
import common.BaseServlet;
import common.EmployeeServletService;
import common.RoleEnum;
import employee_offices.EmployeeOfficeDTO;
import employee_offices.EmployeeOfficeRepository;
import employee_records.Employee_recordsDTO;
import employee_records.Employee_recordsRepository;
import language.LC;
import language.LM;
import lost_card_info.Lost_card_infoDAO;
import lost_card_info.Lost_card_infoDTO;
import office_unit_organograms.OfficeUnitOrganograms;
import office_unit_organograms.OfficeUnitOrganogramsRepository;
import office_units.Office_unitsDTO;
import office_units.Office_unitsRepository;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import pb.Utils;
import permission.MenuConstants;
import police_verification.Police_verificationDAO;
import police_verification.Police_verificationDTO;
import sessionmanager.SessionConstants;
import task_type.TaskTypeEnum;
import user.UserDTO;
import util.CommonDTO;
import util.HttpRequestUtils;
import util.StringUtils;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@SuppressWarnings("Duplicates")
@WebServlet("/Card_infoServlet")
@MultipartConfig
public class Card_infoServlet extends BaseServlet implements EmployeeServletService {
    public static final int DEFAULT_IMAGE_WIDTH = 250;
    public static final int DEFAULT_IMAGE_HEIGHT = 250;
    public static final int PROFILE_IMAGE_WIDTH = 300;
    public static final int PROFILE_IMAGE_HEIGHT = 300;
    public static final int SIGNATURE_IMAGE_WIDTH = 300;
    public static final int SIGNATURE_IMAGE_HEIGHT = 80;
    public static final int nid_IMAGE_WIDTH = 600;
    public static final int nid_IMAGE_HEIGHT = 400;
    private static final long serialVersionUID = 1L;
    private static final Logger logger = Logger.getLogger(Card_infoServlet.class);

    private final CardInfoDAO cardInfoDAO = CardInfoDAO.getInstance();


    @Override
    public String getTableName() {
        return cardInfoDAO.getTableName();
    }

    @Override
    public String getServletName() {
        return "Card_infoServlet";
    }

    @Override
    public CardInfoDAO getCommonDAOService() {
        return cardInfoDAO;
    }

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        return null;
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[0];
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[0];
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[]{MenuConstants.CARD_INFO_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Card_infoServlet.class;
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;
        try {
            switch (request.getParameter("actionType")) {
                case "getPermanentCardAddPage":
                    if (Utils.checkPermission(userDTO, MenuConstants.CARD_INFO_PERMANENT_CARD)) {
                        request.getRequestDispatcher("card_info/card_infoPermanentCardEdit.jsp").forward(request, response);
                        return;
                    }
                    break;
                case "reissue_card":
                    if (Utils.checkPermission(userDTO, MenuConstants.LOST_CARD_INFO_SEARCH)) {
                        request.getRequestDispatcher("card_info/card_infoPermanentCardEdit.jsp").forward(request, response);
                        return;
                    }
                    break;
                case "getTemporaryCardAddPage":
                    if (Utils.checkPermission(userDTO, MenuConstants.CARD_INFO_TEMPORARY_CARD_ADD)) {
                        request.getRequestDispatcher("card_info/card_infoTemporaryCardEdit.jsp").forward(request, response);
                        return;
                    }
                    break;
                case "search": {
                    search(request);
                    super.doGet(request, response);
                    return;
                }
                case "ajax_getCardInfoModel":
                    long employeeRecordId;
                    if (Utils.checkPermission(userDTO, MenuConstants.CARD_INFO_PERMANENT_CARD)) {
                        if(userDTO.roleID == RoleEnum.ADMIN.getRoleId() || userDTO.roleID == RoleEnum.HR_ADMIN.getRoleId()){
                            employeeRecordId = Long.parseLong(request.getParameter("employeeRecordId"));
                        }else{
                            employeeRecordId = userDTO.employee_record_id;
                        }
                        sendCardInfoModelJSON(employeeRecordId, response);
                        return;
                    }
                    break;
                default:
                    super.doGet(request, response);
                    return;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error(ex);
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;
        try {
            switch (request.getParameter("actionType")) {
                case "ajax_addPermanentCard":
                    if (Utils.checkPermission(userDTO, MenuConstants.CARD_INFO_PERMANENT_CARD)) {
                        long employeeRecordId = Long.parseLong(request.getParameter("employeeRecordId"));
                        boolean isAdmin = userDTO.roleID == RoleEnum.ADMIN.getRoleId() || userDTO.roleID == RoleEnum.HR_ADMIN.getRoleId();
                        if(isAdmin || userDTO.employee_record_id == employeeRecordId){
                            Card_infoDTO lastCardInfoDTO = null;
                            List<Card_infoDTO> cardInfoDTOList = CardInfoDAO.getInstance().getByEmployeeId(employeeRecordId);
                            if (cardInfoDTOList != null && cardInfoDTOList.size() > 0) {
                                lastCardInfoDTO = cardInfoDTOList.get(0);
                            }
                            long lastCardInfoId = 0;
                            if (lastCardInfoDTO != null && lastCardInfoDTO.cardStatusCat == CardStatusEnum.ACTIVATE.getValue()) {
                                lastCardInfoId = lastCardInfoDTO.iD;
                            }
                            try {
                                addPermanentCardInfo(employeeRecordId, userDTO, lastCardInfoId);
                                ApiResponse.sendSuccessResponse(response, "Card_infoServlet?actionType=search");
                            } catch (Exception ex) {
                                logger.error(ex);
                                ApiResponse.sendErrorResponse(response, ex.getMessage());
                            }
                            return;
                        }
                    }
                    break;
                case "ajax_reissue_card":
                    if (Utils.checkPermission(userDTO, MenuConstants.CARD_INFO_PERMANENT_CARD)) {
                        try{
                            long lostCardId = Long.parseLong(request.getParameter("lostCardId"));
                            Lost_card_infoDTO lostCardInfoDTO = Lost_card_infoDAO.getInstance().getDTOFromID(lostCardId);
                            if (userDTO.roleID == RoleEnum.EMPLOYEE_OF_PARLIAMENT.getRoleId() && userDTO.employee_record_id != lostCardInfoDTO.employeeRecordsId) {
                                break;
                            } else {
                                Card_infoDTO cardInfoDTO = addPermanentCardInfo(lostCardInfoDTO.employeeRecordsId, userDTO, lostCardInfoDTO.cardInfoId);
                                Lost_card_infoDAO.getInstance().updateReIssueCardInfoId(lostCardInfoDTO.iD, cardInfoDTO.iD);
                                ApiResponse.sendSuccessResponse(response, "Card_infoServlet?actionType=search");
                            }
                        }catch (Exception ex){
                            ex.printStackTrace();
                            ApiResponse.sendErrorResponse(response, ex.getMessage());
                        }
                        return;
                    }
                    break;
                case "ajax_addTemporaryCard":
                    if (Utils.checkPermission(userDTO, MenuConstants.CARD_INFO_TEMPORARY_CARD_ADD)) {
                        try {
                            addTemporaryCardInfo(request, userDTO);
                            ApiResponse.sendSuccessResponse(response, "Card_infoServlet?actionType=search");
                        } catch (Exception ex) {
                            logger.error(ex);
                            ApiResponse.sendErrorResponse(response, ex.getMessage());
                        }
                        return;
                    }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error(ex);
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }

    private Card_infoDTO addPermanentCardInfo(long employeeRecordId, UserDTO userDTO, long lastCardInfoId) throws Exception {
        Card_infoDTO dto = new Card_infoDTO();
        dto.lastCardInfoId = lastCardInfoId;
        dto.cardCat = CardCategoryEnum.PERMANENT.getValue();
        CardEmployeeOfficeInfoDTO cardEmployeeOfficeInfoDTO = CardEmployeeOfficeInfoRepository.getInstance().getByEmployeeRecordId(employeeRecordId, userDTO.ID);
        dto.cardEmployeeOfficeInfoId = cardEmployeeOfficeInfoDTO.iD;
        Employee_recordsDTO employeeRecordsDTO = Employee_recordsRepository.getInstance().getById(employeeRecordId);
        CardEmployeeInfoDTO cardEmployeeInfoDTO = CardEmployeeInfoRepository.getInstance().getByEmployeeRecordId(employeeRecordId, userDTO.ID);
        dto.cardEmployeeInfoId = cardEmployeeInfoDTO.iD;
        dto.cardEmployeeImagesId = CardEmployeeImagesDAO.getInstance().getCardEmployeeImagesId(employeeRecordsDTO, userDTO);
        dto.employeeRecordsId = employeeRecordId;
        TaskTypeEnum taskTypeEnum;
        if (employeeRecordsDTO.employeeClass == 3 || employeeRecordsDTO.employeeClass == 4) {
            taskTypeEnum = TaskTypeEnum.PERMANENT_CARD_FOR_3RD_OR_4TH_CLASS_EMPLOYEE;
            dto.taskTypeId = TaskTypeEnum.PERMANENT_CARD_FOR_3RD_OR_4TH_CLASS_EMPLOYEE.getValue();
            Police_verificationDTO policeVerificationDTO = Police_verificationDAO.getInstance().getByEmployeeRecordIdWhichPoliceVerificationIsDone(employeeRecordId);
            if (policeVerificationDTO == null) {
                dto.isPoliceVerificationRequired = true;
            }
        } else {
            taskTypeEnum = TaskTypeEnum.PERMANENT_CARD_FOR_1ST_OR_2ND_CLASS_EMPLOYEE;
            dto.taskTypeId = TaskTypeEnum.PERMANENT_CARD_FOR_1ST_OR_2ND_CLASS_EMPLOYEE.getValue();
        }
        setCommonDataForAddCard(dto, userDTO);
        cardInfoDAO.add(dto);
        CreateCardApprovalModel model = new CreateCardApprovalModel.CreateCardApprovalModelBuilder()
                .setCardCategoryEnum(CardCategoryEnum.PERMANENT)
                .setTaskTypeId(taskTypeEnum.getValue())
                .setCardInfoDTO(dto)
                .setRequesterEmployeeRecordId(employeeRecordId)
                .build();
        CardApprovalResponse cardApprovalResponse = Card_approval_mappingDAO.getInstance().createCardApproval(model);
        CardApprovalNotification.getInstance().sendPendingNotification(cardApprovalResponse.organogramIds, dto);
        return dto;
    }

    private void setCommonDataForAddCard(Card_infoDTO dto, UserDTO userDTO) {
        dto.cardStatusCat = CardStatusEnum.WAITING_FOR_APPROVAL.getValue();
        dto.validationFrom = dto.validationTo = SessionConstants.MIN_DATE;
        dto.insertedBy = dto.modifiedBy = userDTO.employee_record_id;
        dto.insertionTime = dto.lastModificationTime = System.currentTimeMillis();
        Employee_recordsDTO employee_recordsDTO = Employee_recordsRepository.getInstance().getById(userDTO.employee_record_id);
        if (employee_recordsDTO != null) {
            dto.insertByNameEng = employee_recordsDTO.nameEng;
            dto.insertByNameBng = employee_recordsDTO.nameBng;
        }
        EmployeeOfficeDTO employeeOfficeDTO = EmployeeOfficeRepository.getInstance().getByEmployeeRecordIdIsDefault(userDTO.employee_record_id);
        if (employeeOfficeDTO != null) {
            Office_unitsDTO officeUnitsDTO = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(employeeOfficeDTO.officeUnitId);
            if (officeUnitsDTO != null) {
                dto.insertByOfficeUnitId = officeUnitsDTO.iD;
                dto.insertByOfficeUnitEng = officeUnitsDTO.unitNameEng;
                dto.insertByOfficeUnitBng = officeUnitsDTO.unitNameBng;
            }
            OfficeUnitOrganograms officeUnitOrganograms = OfficeUnitOrganogramsRepository.getInstance().getById(employeeOfficeDTO.officeUnitOrganogramId);
            if (officeUnitOrganograms != null) {
                dto.insertByOfficeUnitOrganogramId = officeUnitOrganograms.id;
                dto.insertByOfficeUnitOrganogramEng = officeUnitOrganograms.designation_eng;
                dto.insertByOfficeUnitOrganogramBng = officeUnitOrganograms.designation_bng;
            }
        }
    }

    private void addTemporaryCardInfo(HttpServletRequest request, UserDTO userDTO) throws Exception {
        boolean isLangEng = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
        Card_infoDTO dto = new Card_infoDTO();
        dto.lastCardInfoId = -1;
        dto.cardCat = CardCategoryEnum.TEMPORARY.getValue();

        CardEmployeeInfoDTO cardEmployeeInfoDTO = new CardEmployeeInfoDTO();

        cardEmployeeInfoDTO.nameEn
                = cardEmployeeInfoDTO.nameBn = Jsoup.clean(request.getParameter("name"), Whitelist.simpleText());
        if (StringUtils.isBlank(cardEmployeeInfoDTO.nameEn))
            throw new Exception(LM.getText(LC.ERROR_MESSAGE_PLEASE_ENTER_YOUR_NAME, userDTO));

        cardEmployeeInfoDTO.fatherNameEn
                = cardEmployeeInfoDTO.fatherNameBn = Jsoup.clean(request.getParameter("fatherName"), Whitelist.simpleText());
        if (StringUtils.isBlank(cardEmployeeInfoDTO.fatherNameEn))
            throw new Exception(LM.getText(LC.ERROR_MESSAGE_PLEASE_ENTER_YOUR_FATHERS_NAME, userDTO));

        cardEmployeeInfoDTO.motherNameEn
                = cardEmployeeInfoDTO.motherNameBn = Jsoup.clean(request.getParameter("motherName"), Whitelist.simpleText());
        if (StringUtils.isBlank(cardEmployeeInfoDTO.motherNameEn))
            throw new Exception(LM.getText(LC.ERROR_MESSAGE_PLEASE_ENTER_YOUR_MOTHERS_NAME, userDTO));

        SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
        Date d = f.parse(Jsoup.clean(request.getParameter("dateOfBirth"), Whitelist.simpleText()));
        cardEmployeeInfoDTO.dob = d.getTime();

        cardEmployeeInfoDTO.nid = Jsoup.clean(request.getParameter("nid"), Whitelist.simpleText());
        String value = Jsoup.clean(request.getParameter("height"), Whitelist.simpleText());
        if (value != null && value.trim().length() > 0) {
            try {
                cardEmployeeInfoDTO.height = Double.parseDouble(value.trim());
            } catch (NumberFormatException ex) {
                logger.error(ex);
            }
        }
        value = Jsoup.clean(request.getParameter("bloodGroup"), Whitelist.simpleText());
        if (value != null && value.trim().length() > 0) {
            try {
                cardEmployeeInfoDTO.bloodGroup = Integer.parseInt(value.trim());
            } catch (NumberFormatException ex) {
                logger.error(ex);
            }
        }
        value = request.getParameter("identificationMark");
        if (value != null) {
            cardEmployeeInfoDTO.identificationSign = value;
        }
        value = request.getParameter("personalMobile");
        value = value == null ? null : value.trim();
        if (value!= null && value.length()>0) {
            if(Utils.isMobileNumberValid("88"+value)){
                cardEmployeeInfoDTO.mobileNumber = "88"+value;
            }else{
                throw new Exception(isLangEng?"Personal mobile number must start with 01 and then contains 9 digits"
                        :"ব্যক্তিগত মোবাইল নাম্বার 01 দিয়ে শুরু হবে, তারপর ৯টি সংখ্যা হবে");
            }
        }else{
            cardEmployeeInfoDTO.mobileNumber = "";
        }

        cardEmployeeInfoDTO.presentAddressEn
                = cardEmployeeInfoDTO.presentAddressBn = Jsoup.clean(request.getParameter("presentAddress"), Whitelist.simpleText());
        if (StringUtils.isBlank(cardEmployeeInfoDTO.presentAddressEn))
            throw new Exception(LM.getText(LC.ERROR_MESSAGE_ENTER_PRESENT_ADDRESS, userDTO));

        cardEmployeeInfoDTO.permanentAddressEn
                = cardEmployeeInfoDTO.permanentAddressBn = Jsoup.clean(request.getParameter("permanentAddress"), Whitelist.simpleText());
        if (StringUtils.isBlank(cardEmployeeInfoDTO.permanentAddressEn))
            throw new Exception(LM.getText(LC.ERROR_MESSAGE_ENTER_PERMANENT_ADDRESS, userDTO));

        cardEmployeeInfoDTO.insertBy
                = cardEmployeeInfoDTO.modifiedBy = userDTO.employee_record_id;

        cardEmployeeInfoDTO.insertionTime
                = cardEmployeeInfoDTO.lastModificationTime = System.currentTimeMillis();

        dto.cardEmployeeInfoId = cardEmployeeInfoDTO.iD = CardEmployeeInfoDAO.getInstance().add(cardEmployeeInfoDTO);

        CardEmployeeOfficeInfoDTO cardEmployeeOfficeInfoDTO = new CardEmployeeOfficeInfoDTO();
        cardEmployeeOfficeInfoDTO.officeUnitEng
                = cardEmployeeOfficeInfoDTO.officeUnitBng = Jsoup.clean(request.getParameter("currentOffice"), Whitelist.simpleText());

        cardEmployeeOfficeInfoDTO.organogramEng
                = cardEmployeeOfficeInfoDTO.organogramBng = Jsoup.clean(request.getParameter("currentPost"), Whitelist.simpleText());

        cardEmployeeOfficeInfoDTO.insertedBy
                = cardEmployeeOfficeInfoDTO.modifiedBy = userDTO.employee_record_id;

        cardEmployeeOfficeInfoDTO.insertionTime
                = cardEmployeeOfficeInfoDTO.lastModificationTime = System.currentTimeMillis();

        dto.cardEmployeeOfficeInfoId
                = cardEmployeeOfficeInfoDTO.iD = CardEmployeeOfficeInfoDAO.getInstance().add(cardEmployeeOfficeInfoDTO);

        CardEmployeeImagesDTO cardEmployeeImagesDTO = new CardEmployeeImagesDTO();
        cardEmployeeImagesDTO.photo = convertByteArray(request.getPart("photo"), "profile");
        cardEmployeeImagesDTO.signature = convertByteArray(request.getPart("signature"), "signature");
        cardEmployeeImagesDTO.nidFrontSide = convertByteArray(request.getPart("nid_front_photo"), "nid");
        cardEmployeeImagesDTO.nidBackSide = convertByteArray(request.getPart("nid_back_photo"), "nid");
        dto.cardEmployeeImagesId = cardEmployeeImagesDTO.iD = CardEmployeeImagesDAO.getInstance().add(cardEmployeeImagesDTO);
        dto.taskTypeId = TaskTypeEnum.TEMPORARY_CARD_FOR_EMPLOYEE.getValue();
        setCommonDataForAddCard(dto, userDTO);
        cardInfoDAO.add(dto);
        TaskTypeEnum taskTypeEnum;
        taskTypeEnum = TaskTypeEnum.TEMPORARY_CARD_FOR_EMPLOYEE;
        CreateCardApprovalModel model = new CreateCardApprovalModel.CreateCardApprovalModelBuilder()
                .setCardCategoryEnum(CardCategoryEnum.TEMPORARY)
                .setTaskTypeId(taskTypeEnum.getValue())
                .setCardInfoDTO(dto)
                .setRequesterEmployeeRecordId(userDTO.employee_record_id)
                .build();
        CardApprovalResponse cardApprovalResponse = Card_approval_mappingDAO.getInstance().createCardApproval(model);
        CardApprovalNotification.getInstance().sendPendingNotification(cardApprovalResponse.organogramIds, dto);
    }

    private void sendCardInfoModelJSON(long employeeRecordId, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        out.print(new Gson().toJson(new CardInfoModel(employeeRecordId, HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language)));
        out.flush();
    }

    private String getFileName(final Part part) {
        final String partHeader = part.getHeader("content-disposition");
        logger.debug("Part Header = {0}" + partHeader);
        for (String content : part.getHeader("content-disposition").split(";")) {
            if (content.trim().startsWith("filename")) {
                return content.substring(
                        content.indexOf('=') + 1).trim().replace("\"", "");
            }
        }
        return null;
    }

    private byte[] resizeImage(byte[] image, String fileType, String imageType) throws IOException {

        int imageWidth = DEFAULT_IMAGE_WIDTH;
        int imageHeight = DEFAULT_IMAGE_HEIGHT;

        if (imageType.equalsIgnoreCase("profile")) {
            imageWidth = PROFILE_IMAGE_WIDTH;
            imageHeight = PROFILE_IMAGE_HEIGHT;
        } else if (imageType.equalsIgnoreCase("signature")) {
            imageWidth = SIGNATURE_IMAGE_WIDTH;
            imageHeight = SIGNATURE_IMAGE_HEIGHT;
        } else if (imageType.equalsIgnoreCase("nid")) {
            imageWidth = nid_IMAGE_WIDTH;
            imageHeight = nid_IMAGE_HEIGHT;
        }

        InputStream inputStream = new ByteArrayInputStream(image);
        BufferedImage bufferedImage = ImageIO.read(inputStream);
        BufferedImage resultBufferedImage = Utils.resize(bufferedImage, imageWidth, imageHeight);
        return Utils.toByteArrayAutoClosable(resultBufferedImage, fileType);
    }

    private byte[] convertByteArray(Part photoPart, String imageType) throws IOException {
        String alternateImage = getFileName(photoPart);
        byte[] tempByteImage;
        if (alternateImage != null) {
            alternateImage = Jsoup.clean(alternateImage, Whitelist.simpleText());
        }
        logger.debug("alternateImage = " + alternateImage);
        if (alternateImage != null && !alternateImage.equalsIgnoreCase("")) {

            if (alternateImage.toLowerCase().endsWith(".jpg") || alternateImage.toLowerCase().endsWith(".png")
                    || alternateImage.toLowerCase().endsWith(".jpeg")) {

                tempByteImage = Utils.uploadFileToByteAray(photoPart);

                if (alternateImage.toLowerCase().endsWith(".jpg")) {
                    return resizeImage(tempByteImage, "jpg", imageType);
                } else if (alternateImage.toLowerCase().endsWith(".png")) {
                    return resizeImage(tempByteImage, "png", imageType);
                } else {
                    return resizeImage(tempByteImage, "jpeg", imageType);
                }
            }
        }
        return null;
    }
}