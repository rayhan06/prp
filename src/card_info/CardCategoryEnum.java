package card_info;

/*
 * @author Md. Erfan Hossain
 * @created 01/05/2021 - 7:36 PM
 * @project parliament
 */

public enum CardCategoryEnum {
    PERMANENT(1),
    TEMPORARY(2);

    private final int value;

    CardCategoryEnum(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public static String getColor(int value){
        if(value == 1){
            return "#0abb87";
        }else {
            return "#0a6aa1";
        }
    }
}
