package card_info;

/*
 * @author Md. Erfan Hossain
 * @created 03/05/2021 - 11:18 PM
 * @project parliament
 */

import java.util.List;

public class CardApprovalResponse {
    public boolean hasNextApproval;
    public List<Long> organogramIds;
    public int nextLevel;
}
