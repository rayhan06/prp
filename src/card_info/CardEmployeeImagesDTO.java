package card_info;

import employee_records.Employee_recordsDTO;
import sessionmanager.SessionConstants;
import util.CommonDTO;

public class CardEmployeeImagesDTO extends CommonDTO {
    public byte[] photo = null;
    public byte[] signature = null;
    public byte[] nidFrontSide = null;
    public byte[] nidBackSide = null;

    public long insertBy = 0L;
    public long insertionTime = SessionConstants.MIN_DATE;
    public long modifiedBy = 0L;

    public CardEmployeeImagesDTO(){ }

    public CardEmployeeImagesDTO(Employee_recordsDTO employee_recordsDTO){
        photo = employee_recordsDTO.photo;
        signature = employee_recordsDTO.signature;
        nidFrontSide = employee_recordsDTO.nidFrontPhoto;
        nidBackSide = employee_recordsDTO.nidBackPhoto;
    }
}
