package card_info;

/*
 * @author Md. Erfan Hossain
 * @created 03/05/2021 - 5:55 PM
 * @project parliament
 */

public class DuplicateCardInfoException extends Exception{

    public DuplicateCardInfoException(String message) {
        super(message);
    }
}
