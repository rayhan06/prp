package card_info;

import employee_records.Employee_recordsDTO;
import sessionmanager.SessionConstants;
import util.CommonDTO;

public class CardEmployeeInfoDTO extends CommonDTO {
    public long employeeRecordId = 0;
    public String nameEn = "";
    public String nameBn = "";
    public String fatherNameEn = "";
    public String fatherNameBn = "";
    public String motherNameEn = "";
    public String motherNameBn = "";
    public long dob = SessionConstants.MIN_DATE;
    public String nid = "";
    public double height = 0;
    public int bloodGroup = 0;
    public String identificationSign = "";
    public String mobileNumber = "";
    public String email = "";
    public String presentAddressEn = "";
    public String presentAddressBn = "";
    public String permanentAddressEn = "";
    public String permanentAddressBn = "";

    public long insertBy = 0;
    public long insertionTime = SessionConstants.MIN_DATE;
    public long modifiedBy = 0;

    public CardEmployeeInfoDTO() {
    }

    public CardEmployeeInfoDTO(Employee_recordsDTO employeeRecordsDTO) {
        employeeRecordId = employeeRecordsDTO.iD;
        if(employeeRecordsDTO.nameEng!=null && employeeRecordsDTO.nameEng.trim().length()>0){
            nameEn = employeeRecordsDTO.nameEng.trim();
        }
        if(employeeRecordsDTO.nameBng!=null && employeeRecordsDTO.nameBng.trim().length()>0){
            nameBn = employeeRecordsDTO.nameBng.trim();
        }
        if(employeeRecordsDTO.fatherNameEng!=null && employeeRecordsDTO.fatherNameEng.trim().length()>0){
            fatherNameEn = employeeRecordsDTO.fatherNameEng;
        }
        if(employeeRecordsDTO.fatherNameBng!=null && employeeRecordsDTO.fatherNameBng.trim().length()>0){
            fatherNameBn = employeeRecordsDTO.fatherNameBng;
        }
        if(employeeRecordsDTO.motherNameEng!=null && employeeRecordsDTO.motherNameEng.trim().length()>0){
            motherNameEn = employeeRecordsDTO.motherNameEng;
        }
        if(employeeRecordsDTO.motherNameBng!=null && employeeRecordsDTO.motherNameBng.trim().length()>0){
            motherNameBn = employeeRecordsDTO.motherNameBng;
        }
        dob = employeeRecordsDTO.dateOfBirth;
        if(employeeRecordsDTO.nid!=null && employeeRecordsDTO.nid.trim().length()>0){
            nid = employeeRecordsDTO.nid;
        }
        height = employeeRecordsDTO.height;
        bloodGroup = employeeRecordsDTO.bloodGroup;
        if(employeeRecordsDTO.identificationMark!=null && employeeRecordsDTO.identificationMark.trim().length()>0){
            identificationSign = employeeRecordsDTO.identificationMark;
        }
        if(employeeRecordsDTO.officePhoneNumber!=null && employeeRecordsDTO.officePhoneNumber.trim().length()>0){
            mobileNumber = employeeRecordsDTO.officePhoneNumber;
        }
        if(employeeRecordsDTO.personalEml!=null && employeeRecordsDTO.personalEml.trim().length()>0){
            email = employeeRecordsDTO.personalEml;
        }
        if(employeeRecordsDTO.presentAddress!=null && employeeRecordsDTO.presentAddress.trim().length()>0){
            presentAddressEn = employeeRecordsDTO.presentAddress;
        }
        if(employeeRecordsDTO.presentAddressBng!=null && employeeRecordsDTO.presentAddressBng.trim().length()>0){
            presentAddressBn = employeeRecordsDTO.presentAddressBng;
        }
        if(employeeRecordsDTO.permanentAddress!=null && employeeRecordsDTO.permanentAddress.trim().length()>0){
            permanentAddressEn = employeeRecordsDTO.permanentAddress;
        }
        if(employeeRecordsDTO.permanentAddressBng!=null && employeeRecordsDTO.permanentAddressBng.trim().length()>0){
            permanentAddressBn = employeeRecordsDTO.permanentAddressBng;
        }
    }

    public boolean hasSameData(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CardEmployeeInfoDTO that = (CardEmployeeInfoDTO) o;
        return employeeRecordId == that.employeeRecordId
                && mobileNumber.equals(that.mobileNumber)
                && email.equals(that.email)
                && presentAddressEn.equals(that.presentAddressEn)
                && presentAddressBn.equals(that.presentAddressBn)
                && nid.equals(that.nid)
                && Double.compare(that.height, height) == 0
                && permanentAddressEn.equals(that.permanentAddressEn)
                && permanentAddressBn.equals(that.permanentAddressBn)
                && nameEn.equals(that.nameEn)
                && nameBn.equals(that.nameBn)
                && fatherNameEn.equals(that.fatherNameEn)
                && fatherNameBn.equals(that.fatherNameBn)
                && motherNameEn.equals(that.motherNameEn)
                && motherNameBn.equals(that.motherNameBn)
                && dob == that.dob
                && bloodGroup == that.bloodGroup
                && identificationSign.equals(that.identificationSign);
    }

    @Override
    public String toString() {
        return "CardEmployeeInfoDTO{" +
                "employeeRecordId=" + employeeRecordId +
                ", nameEn='" + nameEn + '\'' +
                ", nameBn='" + nameBn + '\'' +
                ", fatherNameEn='" + fatherNameEn + '\'' +
                ", fatherNameBn='" + fatherNameBn + '\'' +
                ", motherNameEn='" + motherNameEn + '\'' +
                ", motherNameBn='" + motherNameBn + '\'' +
                ", dob=" + dob +
                ", nid='" + nid + '\'' +
                ", height=" + height +
                ", bloodGroup=" + bloodGroup +
                ", identificationSign='" + identificationSign + '\'' +
                ", mobileNumber='" + mobileNumber + '\'' +
                ", email='" + email + '\'' +
                ", presentAddressEn='" + presentAddressEn + '\'' +
                ", presentAddressBn='" + presentAddressBn + '\'' +
                ", permanentAddressEn='" + permanentAddressEn + '\'' +
                ", permanentAddressBn='" + permanentAddressBn + '\'' +
                ", insertBy='" + insertBy + '\'' +
                ", insertionTime=" + insertionTime +
                ", modifiedBy='" + modifiedBy + '\'' +
                '}';
    }
}
