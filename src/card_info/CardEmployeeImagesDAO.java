package card_info;

import common.CommonDAOService;
import common.ConnectionAndStatementUtil;
import employee_records.Employee_recordsDTO;
import org.apache.log4j.Logger;
import user.UserDTO;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

@SuppressWarnings({"unused"})
public class CardEmployeeImagesDAO implements CommonDAOService<CardEmployeeImagesDTO> {
    private static final Logger logger = Logger.getLogger(CardEmployeeImagesDAO.class);

    private static final String addQuery = "INSERT INTO {tableName} (photo, signature, nid_front_side, nid_back_side, "
            .concat("modified_by, lastModificationTime, insert_by, insertion_time, isDeleted, id) ")
            .concat("VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");

    private static final String updateQuery = "UPDATE {tableName} SET photo = ?, signature = ?, nid_front_side = ?, "
            .concat("nid_back_side = ?, modified_by = ?, lastModificationTime = ? WHERE id = ?");

    private static final String getPhotoByIds = "SELECT photo,id FROM card_employee_images WHERE id IN (%s)";

    private CardEmployeeImagesDAO() {
    }

    private static class LazyLoader{
        static final CardEmployeeImagesDAO INSTANCE = new CardEmployeeImagesDAO();
    }

    public static CardEmployeeImagesDAO getInstance(){
        return LazyLoader.INSTANCE;
    }

    @Override
    public void set(PreparedStatement ps, CardEmployeeImagesDTO dto, boolean isInsert) throws SQLException {
        int index = 0;
        ps.setBytes(++index, dto.photo);
        ps.setBytes(++index, dto.signature);
        ps.setBytes(++index, dto.nidFrontSide);
        ps.setBytes(++index, dto.nidBackSide);
        ps.setLong(++index, dto.modifiedBy);
        ps.setLong(++index, dto.lastModificationTime);

        if(isInsert){
            ps.setLong(++index, dto.insertBy);
            ps.setLong(++index, dto.insertionTime);
            ps.setInt(++index, 0 /* dto.isDeleted */);
        }

        ps.setLong(++index, dto.iD);
    }

    @Override
    public CardEmployeeImagesDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            CardEmployeeImagesDTO dto = new CardEmployeeImagesDTO();

            dto.iD = rs.getLong("id");
            dto.photo = rs.getBytes("photo");
            dto.signature = rs.getBytes("signature");
            dto.nidFrontSide = rs.getBytes("nid_front_side");
            dto.nidBackSide = rs.getBytes("nid_back_side");
            dto.isDeleted = rs.getInt("isDeleted");
            dto.insertBy = rs.getLong("insert_by");
            dto.insertionTime = rs.getLong("insertion_time");
            dto.modifiedBy = rs.getLong("modified_by");
            dto.lastModificationTime = rs.getLong("lastModificationTime");

            return dto;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    @Override
    public String getTableName() {
        return "card_employee_images";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return null;
    }

    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((CardEmployeeImagesDTO) commonDTO, updateQuery, false);
    }

    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((CardEmployeeImagesDTO) commonDTO, addQuery, true);
    }

    public long getCardEmployeeImagesId(Employee_recordsDTO employeeRecordsDTO, UserDTO userDTO) throws Exception{
        CardEmployeeImagesDTO dto = new CardEmployeeImagesDTO(employeeRecordsDTO);
        dto.insertBy = dto.modifiedBy = userDTO.ID;
        dto.insertionTime = dto.lastModificationTime = System.currentTimeMillis();
        return add(dto);
    }

    public List<CardEmployeeImagesDTO> getPhotosByIds(List<Long>ids){
        return ConnectionAndStatementUtil.getDTOListByNumbers(getPhotoByIds,ids,rs->{
            try{
                CardEmployeeImagesDTO dto = new CardEmployeeImagesDTO();
                dto.iD = rs.getLong("id");
                dto.photo = rs.getBytes("photo");
                return dto;
            }catch (SQLException ex){
                logger.error(ex);
                return null;
            }
        });
    }
}