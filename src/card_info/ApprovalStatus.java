package card_info;

/*
 * @author Md. Erfan Hossain
 * @created 01/05/2021 - 7:48 PM
 * @project parliament
 */

public enum ApprovalStatus {
    PENDING(1),
    SATISFIED(2),
    DISSATISFIED(3),
    ;
    private final int value;

    ApprovalStatus(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public static String getColor(int value){
        if (value == SATISFIED.getValue()) {
            return  "green";
        } else if (value == DISSATISFIED.getValue()) {
            return  "crimson";
        }
        return "#22ccc1";
    }
}
