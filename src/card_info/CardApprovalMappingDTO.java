package card_info;

/*
 * @author Md. Erfan Hossain
 * @created 28/04/2021 - 6:19 PM
 * @project parliament
 */

import util.CommonDTO;

public class CardApprovalMappingDTO extends CommonDTO {
    public long cardInfoId;
    public long cardApprovalId;
    public int cardApprovalStatusCat;
    public int sequence;
    public long taskTypeId;
    public long insertBy;
    public long insertionTime;
    public long modifiedBy;
    public long employeeRecordsId;

    @Override
    public String toString() {
        return "CardApprovalMappingDTO{" +
                "cardInfoId=" + cardInfoId +
                ", cardApprovalId=" + cardApprovalId +
                ", cardApprovalStatusCat=" + cardApprovalStatusCat +
                ", sequence=" + sequence +
                ", taskTypeId=" + taskTypeId +
                ", insertBy=" + insertBy +
                ", insertionTime=" + insertionTime +
                ", modifiedBy=" + modifiedBy +
                ", iD=" + iD +
                ", isDeleted=" + isDeleted +
                '}';
    }
}
