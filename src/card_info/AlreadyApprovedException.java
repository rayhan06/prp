package card_info;

/*
 * @author Md. Erfan Hossain
 * @created 05/05/2021 - 4:21 PM
 * @project parliament
 */

public class AlreadyApprovedException extends Exception {
    public AlreadyApprovedException(String message) {
        super(message);
    }

    public AlreadyApprovedException() {
        this("");
    }
}
