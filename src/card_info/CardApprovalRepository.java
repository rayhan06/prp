package card_info;

/*
 * @author Md. Erfan Hossain
 * @created 28/04/2021 - 12:32 PM
 * @project parliament
 */


import employee_offices.EmployeeOfficeDTO;
import employee_offices.EmployeeOfficeRepository;
import employee_records.Employee_recordsDTO;
import employee_records.Employee_recordsRepository;
import office_unit_organograms.OfficeUnitOrganograms;
import office_unit_organograms.OfficeUnitOrganogramsRepository;
import office_units.Office_unitsDTO;
import office_units.Office_unitsRepository;
import org.apache.log4j.Logger;
import util.LockManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

@SuppressWarnings({"unused", "Duplicates"})
public class CardApprovalRepository {
    private static final Logger logger = Logger.getLogger(CardApprovalRepository.class);
    private final CardApprovalDAO cardApprovalDAO;
    private final Map<Long, CardApprovalDTO> mapById;
    private final Map<Long, CardApprovalDTO> mapByEmployeeRecordId;

    private CardApprovalRepository() {
        cardApprovalDAO = CardApprovalDAO.getInstance();
        mapById = new ConcurrentHashMap<>();
        mapByEmployeeRecordId = new ConcurrentHashMap<>();
        reload();
    }

    private static class CardApprovalRepositoryLazyLoader {
        final static CardApprovalRepository INSTANCE = new CardApprovalRepository();
    }

    public static CardApprovalRepository getInstance() {
        return CardApprovalRepositoryLazyLoader.INSTANCE;
    }

    private void reload() {
        List<CardApprovalDTO> list = cardApprovalDAO.getAllDTOs(true);
        list.forEach(dto -> {
            mapById.put(dto.iD, dto);
            mapByEmployeeRecordId.put(dto.employeeRecordId, dto);
        });
    }

    public CardApprovalDTO getByEmployeeRecordId(long employeeRecordId) {
        return getByEmployeeRecordId(employeeRecordId, 0);
    }

    public CardApprovalDTO getById(long id) {
        if (mapById.get(id) == null) {
            synchronized (LockManager.getLock(id + "CAR")) {
                if (mapById.get(id) == null) {
                    CardApprovalDTO dto = cardApprovalDAO.getDTOFromIdDeletedOrNot(id);
                    if (dto != null) {
                        mapById.put(dto.iD, dto);
                        if (dto.isDeleted == 0) {
                            mapByEmployeeRecordId.put(dto.employeeRecordId, dto);
                        }
                    }
                }
            }
        }
        return mapById.get(id);
    }

    public CardApprovalDTO getByEmployeeRecordId(long employeeRecordId, long requesterId) {
        synchronized (LockManager.getLock(employeeRecordId + "CAR")) {
            if (mapByEmployeeRecordId.get(employeeRecordId) == null) {
                List<CardApprovalDTO> dtoList = cardApprovalDAO.getByEmployeeId(employeeRecordId);
                if (dtoList != null && dtoList.size() > 0) {
                    CardApprovalDTO dto = dtoList.get(0);
                    mapByEmployeeRecordId.put(dto.employeeRecordId, dto);
                    mapById.put(dto.iD, dto);
                }
            }
            if (mapByEmployeeRecordId.get(employeeRecordId) == null) {
                createApprovalDTOForEmployeeRecordId(employeeRecordId, requesterId);
            } else {
                CardApprovalDTO dto = mapByEmployeeRecordId.get(employeeRecordId);
                if (!isMatch(dto)) {
                    boolean deleteResult = cardApprovalDAO.deleteById(dto.iD, requesterId);
                    if (deleteResult) {
                        createApprovalDTOForEmployeeRecordId(employeeRecordId, requesterId);
                        mapById.remove(dto.iD);
                    }
                }
            }
        }
        return mapByEmployeeRecordId.get(employeeRecordId);
    }

    private void createApprovalDTOForEmployeeRecordId(long employeeRecordId, long requesterId) {
        Employee_recordsDTO employeeRecordsDTO = Employee_recordsRepository.getInstance().getById(employeeRecordId);
        if (employeeRecordsDTO == null) {
            return;
        }
        EmployeeOfficeDTO employeeOfficeDTO = EmployeeOfficeRepository.getInstance().getByEmployeeRecordIdIsDefault(employeeRecordId);
        if (employeeOfficeDTO == null) {
            return;
        }
        Office_unitsDTO officeUnitsDTO = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(employeeOfficeDTO.officeUnitId);
        OfficeUnitOrganograms officeUnitOrganograms = OfficeUnitOrganogramsRepository.getInstance().getById(employeeOfficeDTO.officeUnitOrganogramId);
        CardApprovalDTO dto = new CardApprovalDTO();
        dto.employeeRecordId = employeeRecordId;
        dto.nameEng = employeeRecordsDTO.nameEng;
        dto.nameBng = employeeRecordsDTO.nameBng;
        dto.userName = employeeRecordsDTO.employeeNumber;
        dto.officeUnitId = employeeOfficeDTO.officeUnitId;
        dto.officeUnitEng = officeUnitsDTO.unitNameEng;
        dto.officeUnitBng = officeUnitsDTO.unitNameBng;
        dto.organogramId = employeeOfficeDTO.officeUnitOrganogramId;
        dto.organogramEng = officeUnitOrganograms.designation_eng;
        dto.organogramBng = officeUnitOrganograms.designation_bng;
        dto.insertBy = requesterId;
        dto.insertionTime = System.currentTimeMillis();
        dto.modifiedBy = requesterId;
        dto.lastModificationTime = System.currentTimeMillis();
        try {
            cardApprovalDAO.add(dto);
            mapById.put(dto.iD, dto);
            mapByEmployeeRecordId.put(dto.employeeRecordId, dto);
        } catch (Exception e) {
            logger.error(e);
        }
    }

    private boolean isMatch(CardApprovalDTO dto) {
        Employee_recordsDTO employeeRecordsDTO = Employee_recordsRepository.getInstance().getById(dto.employeeRecordId);
        EmployeeOfficeDTO employeeOfficeDTO = EmployeeOfficeRepository.getInstance().getByEmployeeRecordIdIsDefault(dto.employeeRecordId);
        Office_unitsDTO officeUnitsDTO = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(employeeOfficeDTO.officeUnitId);
        OfficeUnitOrganograms officeUnitOrganograms = OfficeUnitOrganogramsRepository.getInstance().getById(employeeOfficeDTO.officeUnitOrganogramId);
        return employeeRecordsDTO.nameBng.equals(dto.nameBng)
                && employeeRecordsDTO.nameEng.equals(dto.nameEng)
                && officeUnitsDTO.iD == dto.officeUnitId
                && officeUnitsDTO.unitNameEng.equals(dto.officeUnitEng)
                && officeUnitsDTO.unitNameBng.equals(dto.officeUnitBng)
                && officeUnitOrganograms.id == dto.organogramId
                && officeUnitOrganograms.designation_eng.equals(dto.organogramEng)
                && officeUnitOrganograms.designation_bng.equals(dto.organogramBng);
    }

    public List<CardApprovalDTO> getByIds(List<Long> ids) {
        List<CardApprovalDTO> list = new ArrayList<>();
        List<Long> notFoundInCacheList = ids.stream()
                .peek(id -> addToListIfFoundInCache(id, list))
                .filter(id -> mapById.get(id) == null)
                .collect(Collectors.toList());
        if (notFoundInCacheList.size() > 0) {
            List<CardApprovalDTO> list1 = cardApprovalDAO.getDTOs(notFoundInCacheList);
            if (list1 != null && list1.size() > 0) {
                list.addAll(list1);
            }
        }
        return list;
    }

    private void addToListIfFoundInCache(long id, List<CardApprovalDTO> list) {
        CardApprovalDTO dto = mapById.get(id);
        if (dto != null) {
            list.add(dto);
        }
    }
}