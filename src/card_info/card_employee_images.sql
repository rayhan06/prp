CREATE TABLE card_employee_images
(
    id                   BIGINT(20) UNSIGNED PRIMARY KEY,
    photo                LONGBLOB     DEFAULT NULL,
    signature            LONGBLOB     DEFAULT NULL,
    nid_front_side       LONGBLOB     DEFAULT NULL,
    nid_back_side        LONGBLOB     DEFAULT NULL,
    isDeleted            INT(11)      DEFAULT 0,
    insert_by            VARCHAR(255) DEFAULT NULL,
    insertion_time       BIGINT(20)   DEFAULT -62135791200000,
    modified_by          VARCHAR(255) DEFAULT NULL,
    lastModificationTime BIGINT(20)   DEFAULT -62135791200000
) ENGINE = MyISAM
  DEFAULT CHARSET = utf8
  COLLATE = utf8_unicode_ci;