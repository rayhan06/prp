package card_info;

import util.CommonEmployeeDTO;


public class Card_infoDTO extends CommonEmployeeDTO {
    public int cardStatusCat = 0;
    public long lastCardInfoId = 0;
    public int cardCat = 0;
    public long cardEmployeeOfficeInfoId = 0;
    public boolean isPoliceVerificationRequired = false;
    public long nextCardApprovalId = 0;
    public long cardEmployeeInfoId = 0;
    public long cardEmployeeImagesId = 0;
    public long validationFrom = 0;
    public long validationTo = 0;
    public boolean qcDone = false;
    public long insertedBy = 0;
    public long insertionTime = 0;
    public long modifiedBy = 0;
    public long taskTypeId = 0;
    public String insertByNameEng = "";
    public String insertByNameBng = "";
    public long insertByOfficeUnitId = 0;
    public String insertByOfficeUnitEng = "";
    public String insertByOfficeUnitBng = "";
    public long   insertByOfficeUnitOrganogramId = 0;
    public String insertByOfficeUnitOrganogramEng = "";
    public String insertByOfficeUnitOrganogramBng = "";
    public String comment = "";

    @Override
    public String toString() {
        return "Card_infoDTO{" +
                "employeeRecordId=" + employeeRecordsId +
                ", cardStatusCat=" + cardStatusCat +
                ", lastCardInfoId=" + lastCardInfoId +
                ", cardCat=" + cardCat +
                ", cardEmployeeOfficeInfoId=" + cardEmployeeOfficeInfoId +
                ", isPoliceVerificationRequired=" + isPoliceVerificationRequired +
                ", nextCardApprovalId=" + nextCardApprovalId +
                ", cardEmployeeInfoId=" + cardEmployeeInfoId +
                ", cardEmployeeImagesId=" + cardEmployeeImagesId +
                ", validationFrom=" + validationFrom +
                ", validationTo=" + validationTo +
                ", qcDone=" + qcDone +
                ", insertedBy=" + insertedBy +
                ", insertionTime=" + insertionTime +
                ", modifiedBy=" + modifiedBy +
                ", taskTypeId=" + taskTypeId +
                ", insertByNameEng='" + insertByNameEng + '\'' +
                ", insertByNameBng='" + insertByNameBng + '\'' +
                ", insertByOfficeUnitId=" + insertByOfficeUnitId +
                ", insertByOfficeUnitEng='" + insertByOfficeUnitEng + '\'' +
                ", insertByOfficeUnitBng='" + insertByOfficeUnitBng + '\'' +
                ", insertByOfficeUnitOrganogramId=" + insertByOfficeUnitOrganogramId +
                ", insertByOfficeUnitOrganogramEng='" + insertByOfficeUnitOrganogramEng + '\'' +
                ", insertByOfficeUnitOrganogramBng='" + insertByOfficeUnitOrganogramBng + '\'' +
                ", comment='" + comment + '\'' +
                ", iD=" + iD +
                ", isDeleted=" + isDeleted +
                '}';
    }
}