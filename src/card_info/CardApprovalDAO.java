package card_info;

/*
 * @author Md. Erfan Hossain
 * @created 28/04/2021 - 11:47 AM
 * @project parliament
 */

import employee_records.EmployeeCommonDTOService;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@SuppressWarnings({"unused","Duplicates"})
public class CardApprovalDAO implements EmployeeCommonDTOService<CardApprovalDTO> {
    private static final String addSqlQuery = "INSERT INTO {tableName} (employee_records_id,employee_name_eng,employee_name_bng," +
            "office_unit_id,office_unit_eng,office_unit_bng,organogram_id,organogram_eng,organogram_bng," +
            "modified_by,lastModificationTime,inserted_by,insertion_time,isDeleted,id) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

    private static final String deleteSqlQuery = "UPDATE card_approval SET isDeleted = 1, lastModificationTime = %d, modified_by = %d WHERE id = %d";

    private CardApprovalDAO(){
    }

    private static final class InstanceHolder {
        static final CardApprovalDAO INSTANCE = new CardApprovalDAO();
    }

    public static CardApprovalDAO getInstance(){
        return InstanceHolder.INSTANCE;
    }

    @Override
    public void set(PreparedStatement ps, CardApprovalDTO dto, boolean isInsert) throws SQLException {
        int index = 0;
        ps.setLong(++index,dto.employeeRecordId);
        ps.setString(++index,dto.nameEng);
        ps.setString(++index,dto.nameBng);
        ps.setLong(++index,dto.officeUnitId);
        ps.setString(++index,dto.officeUnitEng);
        ps.setString(++index,dto.officeUnitBng);
        ps.setLong(++index,dto.organogramId);
        ps.setString(++index,dto.organogramEng);
        ps.setString(++index,dto.organogramBng);
        ps.setLong(++index,dto.modifiedBy);
        ps.setLong(++index,dto.lastModificationTime);
        if(isInsert){
            ps.setLong(++index,dto.insertBy);
            ps.setLong(++index,dto.insertionTime);
            ps.setInt(++index,0);
        }
        ps.setLong(++index,dto.iD);
    }

    @Override
    public CardApprovalDTO buildObjectFromResultSet(ResultSet rs) {
        try{
            CardApprovalDTO dto = new CardApprovalDTO();
            dto.iD = rs.getLong("id");
            dto.employeeRecordId = rs.getLong("employee_records_id");
            dto.nameEng = rs.getString("employee_name_eng");
            dto.nameBng = rs.getString("employee_name_bng");
            dto.officeUnitId = rs.getLong("office_unit_id");
            dto.officeUnitEng = rs.getString("office_unit_eng");
            dto.officeUnitBng = rs.getString("office_unit_bng");
            dto.organogramId = rs.getLong("organogram_id");
            dto.organogramEng = rs.getString("organogram_eng");
            dto.organogramBng = rs.getString("organogram_bng");
            dto.insertionTime = rs.getLong("insertion_time");
            dto.insertBy = rs.getLong("inserted_by");
            dto.modifiedBy = rs.getLong("modified_by");
            dto.isDeleted = rs.getInt("isDeleted");
            dto.lastModificationTime = rs.getLong("lastModificationTime");
            return dto;
        }catch (SQLException ex){
            return null;
        }
    }

    @Override
    public String getTableName() {
        return "card_approval";
    }

    public long add(CardApprovalDTO dto) throws Exception {
        return executeAddOrUpdateQuery(dto, addSqlQuery, true);
    }
}