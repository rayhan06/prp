package card_info;

/*
 * @author Md. Erfan Hossain
 * @created 11/05/2021 - 3:26 PM
 * @project parliament
 */

import pb.Utils;
import pb_notifications.Pb_notificationsDAO;
import util.StringUtils;

import java.util.List;

@SuppressWarnings("Duplicates")
public class CardApprovalNotification {
    private final Pb_notificationsDAO pb_notificationsDAO;

    private CardApprovalNotification(){
        pb_notificationsDAO = Pb_notificationsDAO.getInstance();
    }

    private static class CardApprovalNotificationLazyLoader{
        static final CardApprovalNotification INSTANCE = new CardApprovalNotification();
    }
    public static CardApprovalNotification getInstance(){
        return CardApprovalNotificationLazyLoader.INSTANCE;
    }

    public void sendPendingNotification(List<Long> organogramIds, Card_infoDTO cardInfoDTO){
        String cardEngText = cardInfoDTO.cardCat == CardCategoryEnum.PERMANENT.getValue() ? "permanent" : "temporary";
        String cardBngText = cardInfoDTO.cardCat == CardCategoryEnum.PERMANENT.getValue() ? "স্থায়ী" : "অস্থায়ী";
        CardEmployeeInfoDTO cardEmployeeInfoDTO = CardEmployeeInfoRepository.getInstance().getById(cardInfoDTO.cardEmployeeInfoId);
        CardEmployeeOfficeInfoDTO cardEmployeeOfficeInfoDTO = CardEmployeeOfficeInfoRepository.getInstance().getById(cardInfoDTO.cardEmployeeOfficeInfoId);
        String empEngText = cardEmployeeInfoDTO.nameEn + "("+cardEmployeeOfficeInfoDTO.organogramEng+","+cardEmployeeOfficeInfoDTO.officeUnitEng+")s";
        String empBngText = cardEmployeeInfoDTO.nameBn + "("+cardEmployeeOfficeInfoDTO.organogramBng+","+cardEmployeeOfficeInfoDTO.officeUnitBng+") এর";
        String notificationMessage = empEngText+" "+cardEngText+" "+"card-"+cardInfoDTO.iD+" "+"is waiting for your approval.$"
                +empBngText+" "+cardBngText+" "+"কার্ড-"+ StringUtils.convertToBanNumber(String.valueOf(cardInfoDTO.iD))+" "+ "অনুমোদনের অপেক্ষায় আছে।";
        String url = "Card_approval_mappingServlet?actionType=getApprovalPage&cardInfoId="+cardInfoDTO.iD;
        sendNotification(organogramIds,notificationMessage,url);
    }

    public void sendApproveOrRejectNotification(List<Long> organogramIds, Card_infoDTO cardInfoDTO, boolean isApproved){
        String cardTypeEngText = cardInfoDTO.cardCat == CardCategoryEnum.PERMANENT.getValue() ? "permanent" : "temporary";
        String cardTypeBngText = cardInfoDTO.cardCat == CardCategoryEnum.PERMANENT.getValue() ? "স্থায়ী" : "অস্থায়ী";
        String cardEngText = isApproved ? "You requested "+cardTypeEngText+" card-"+cardInfoDTO.iD+" has been approved."
                : "You requested "+cardTypeEngText+" card-"+cardInfoDTO.iD+" has been rejected.";
        String cardBngText = isApproved ? "আপনার অনুরোধ করা "+cardTypeBngText+" কার্ড-"+StringUtils.convertToBanNumber(String.valueOf(cardInfoDTO.iD))+" অনুমোদিত হয়েছে।"
                : "আপনার অনুরোধ করা "+cardTypeBngText+" কার্ড-"+StringUtils.convertToBanNumber(String.valueOf(cardInfoDTO.iD))+" প্রত্যাখ্যান করা হয়েছে।";
        String notificationMessage = cardEngText +"$"+cardBngText;
        String url = "Card_infoServlet?actionType=view&ID="+cardInfoDTO.iD;
        sendNotification(organogramIds,notificationMessage,url);
    }

    private void sendNotification(List<Long> organogramIds,String notificationMessage,String url){
        if(organogramIds == null || organogramIds.size() == 0){
            return;
        }
        Utils.runIOTaskInASeparateThread(()->()->organogramIds.forEach(id-> pb_notificationsDAO.addPb_notifications(id, System.currentTimeMillis(), url, notificationMessage,false)));
    }

    public void sendLostCardNotification(List<Long> organogramIds, Card_infoDTO cardInfoDTO){
        String cardEngText = cardInfoDTO.cardCat == CardCategoryEnum.PERMANENT.getValue() ? "Permanent " : "Temporary ";
        String cardBngText = cardInfoDTO.cardCat == CardCategoryEnum.PERMANENT.getValue() ? "স্থায়ী " : "অস্থায়ী ";
        String msgEng = cardEngText+"-"+cardInfoDTO.iD+" is reported as lost card.";
        String msgBng = cardBngText+"-"+cardInfoDTO.iD+" কার্ডটি হারানো গিয়াছে।";
        String notificationMessage = msgEng +"$"+msgBng;
        String url = "Card_approval_mappingServlet?actionType=getApprovalPage&cardInfoId="+cardInfoDTO.iD;
        sendNotification(organogramIds,notificationMessage,url);
    }
}