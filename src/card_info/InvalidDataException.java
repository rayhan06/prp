package card_info;

/*
 * @author Md. Erfan Hossain
 * @created 04/05/2021 - 2:18 AM
 * @project parliament
 */

public class InvalidDataException extends Exception{
    public InvalidDataException(String message) {
        super(message);
    }
}
