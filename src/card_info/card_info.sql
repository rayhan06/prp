CREATE TABLE card_info
(
    ID                              bigint(20) UNSIGNED PRIMARY KEY,
    card_status_cat                 bigint(20) DEFAULT NULL,
    last_card_info_id               bigint(20) DEFAULT NULL,
    card_cat                        bigint(20) DEFAULT NULL,
    card_employee_office_info_id    bigint(20) DEFAULT NULL,
    is_police_verification_required int(11)    DEFAULT 0,
    next_card_approval_id           bigint(20) DEFAULT NULL,
    card_employee_info_id           bigint(20) DEFAULT NULL,
    card_employee_images_id         bigint(20) DEFAULT NULL,
    validation_from                 bigint(20),
    validation_to                   bigint(20),
    qc_done                         int(11)    DEFAULT 0,
    inserted_by                     bigint(20) DEFAULT NULL,
    insertion_time                  bigint(20) DEFAULT '-62135791200000',
    isDeleted                       int(11)    DEFAULT 0,
    modified_by                     bigint(20) DEFAULT NULL,
    lastModificationTime            bigint(20) DEFAULT '-62135791200000'
) ENGINE = MyISAM
  DEFAULT CHARSET = utf8
  COLLATE = utf8_unicode_ci;

