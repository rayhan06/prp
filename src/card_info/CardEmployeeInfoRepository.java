package card_info;

/*
 * @author Md. Erfan Hossain
 * @created 02/05/2021 - 2:46 AM
 * @project parliament
 */

import employee_records.Employee_recordsDTO;
import employee_records.Employee_recordsRepository;
import geolocation.GeoLocationDAO2;
import geolocation.GeoLocationUtils;
import org.apache.log4j.Logger;
import util.LockManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public class CardEmployeeInfoRepository {

    private final Logger logger = Logger.getLogger(CardEmployeeInfoRepository.class);
    private final CardEmployeeInfoDAO cardEmployeeInfoDAO = CardEmployeeInfoDAO.getInstance();
    private final Map<Long, CardEmployeeInfoDTO> mapById = new ConcurrentHashMap<>();
    private final Map<Long, CardEmployeeInfoDTO> mapByEmployeeRecordId = new ConcurrentHashMap<>();

    private CardEmployeeInfoRepository() {
        reload();
    }

    private static class CardEmployeeInfoRepositoryLazyLoader {
        static final CardEmployeeInfoRepository INSTANCE = new CardEmployeeInfoRepository();
    }

    public static CardEmployeeInfoRepository getInstance() {
        return CardEmployeeInfoRepositoryLazyLoader.INSTANCE;
    }

    private void reload() {
        logger.debug("CardEmployeeInfoRepository reload start");
        List<CardEmployeeInfoDTO> list = cardEmployeeInfoDAO.getAllDTOs(true);
        if (list != null && list.size() > 0) {
            list.forEach(dto -> {
                mapById.put(dto.iD, dto);
                mapByEmployeeRecordId.put(dto.employeeRecordId, dto);
            });
        }
        logger.debug("CardEmployeeInfoRepository reload end");
    }

    public CardEmployeeInfoDTO getById(long id) {
        if (mapById.get(id) == null) {
            synchronized (LockManager.getLock(id + "CER")) {
                if (mapById.get(id) == null) {
                    CardEmployeeInfoDTO dto = cardEmployeeInfoDAO.getDTOFromIdDeletedOrNot(id);
                    if (dto != null) {
                        mapById.put(dto.iD, dto);
                        if (dto.isDeleted == 0) {
                            mapByEmployeeRecordId.put(dto.employeeRecordId, dto);
                        }
                    }
                }
            }

        }
        return mapById.get(id);
    }

    public CardEmployeeInfoDTO getByEmployeeRecordId(long employeeRecordId) {
        return getByEmployeeRecordId(employeeRecordId, 0);
    }

    public CardEmployeeInfoDTO getByEmployeeRecordId(long employeeRecordId, long requesterId) {
        synchronized (LockManager.getLock(employeeRecordId + "CER")) {
            if (mapByEmployeeRecordId.get(employeeRecordId) == null) {
                List<CardEmployeeInfoDTO> list = cardEmployeeInfoDAO.getByEmployeeId(employeeRecordId);
                if (list != null && list.size() > 0) {
                    CardEmployeeInfoDTO dto = list.get(0);
                    mapByEmployeeRecordId.put(dto.employeeRecordId, dto);
                    mapById.put(dto.iD, dto);
                }
            }
            if (mapByEmployeeRecordId.get(employeeRecordId) == null) {
                createCardEmployeeInfoDTO(employeeRecordId, requesterId);
            } else {
                CardEmployeeInfoDTO dto = mapByEmployeeRecordId.get(employeeRecordId);
                if (!isMatch(dto)) {
                    boolean deleteResult = cardEmployeeInfoDAO.delete(requesterId, dto.iD);
                    if (deleteResult) {
                        createCardEmployeeInfoDTO(employeeRecordId, requesterId);
                        mapById.remove(dto.iD);
                    }
                }
            }
        }

        return mapByEmployeeRecordId.get(employeeRecordId);
    }

    private void createCardEmployeeInfoDTO(long employeeRecordId, long requesterId) {
        Employee_recordsDTO employeeRecordsDTO = Employee_recordsRepository.getInstance().getById(employeeRecordId);
        if (employeeRecordsDTO == null) {
            return;
        }
        CardEmployeeInfoDTO dto = new CardEmployeeInfoDTO();
        dto.employeeRecordId = employeeRecordId;
        dto.nameEn = employeeRecordsDTO.nameEng;
        dto.nameBn = employeeRecordsDTO.nameBng;
        dto.fatherNameEn = employeeRecordsDTO.fatherNameEng;
        dto.fatherNameBn = employeeRecordsDTO.fatherNameBng;
        dto.motherNameEn = employeeRecordsDTO.motherNameEng;
        dto.motherNameBn = employeeRecordsDTO.motherNameBng;
        dto.dob = employeeRecordsDTO.dateOfBirth;
        dto.nid = employeeRecordsDTO.nid;
        dto.height = employeeRecordsDTO.height;
        dto.bloodGroup = employeeRecordsDTO.bloodGroup;
        dto.identificationSign = employeeRecordsDTO.identificationMark;
        if (employeeRecordsDTO.officePhoneNumber != null) {
            dto.mobileNumber = employeeRecordsDTO.officePhoneNumber;
        }
        dto.presentAddressEn = GeoLocationUtils.getGeoLocationString(employeeRecordsDTO.presentAddress, "English");
        dto.presentAddressBn = GeoLocationUtils.getGeoLocationString(employeeRecordsDTO.presentAddressBng, "Bangla");
        dto.permanentAddressEn = GeoLocationUtils.getGeoLocationString(employeeRecordsDTO.permanentAddress, "English");
        dto.permanentAddressBn = GeoLocationUtils.getGeoLocationString(employeeRecordsDTO.permanentAddressBng, "Bangla");
        dto.insertBy = dto.modifiedBy = requesterId;
        dto.insertionTime = dto.lastModificationTime = System.currentTimeMillis();
        try {
            cardEmployeeInfoDAO.add(dto);
            mapById.put(dto.iD, dto);
            mapByEmployeeRecordId.put(dto.employeeRecordId, dto);
        } catch (Exception e) {
            logger.error(e);
        }
    }

    private boolean isMatch(CardEmployeeInfoDTO dto) {
        Employee_recordsDTO employeeRecordsDTO = Employee_recordsRepository.getInstance().getById(dto.employeeRecordId);
        if (dto.mobileNumber == null) {
            dto.mobileNumber = "";
        }
        if (employeeRecordsDTO.officePhoneNumber == null) {
            employeeRecordsDTO.officePhoneNumber = "";
        }
        return dto.nameEn.equals(employeeRecordsDTO.nameEng)
                && dto.nameBn.equals(employeeRecordsDTO.nameBng)
                && dto.fatherNameEn.equals(employeeRecordsDTO.fatherNameEng)
                && dto.fatherNameBn.equals(employeeRecordsDTO.fatherNameBng)
                && dto.motherNameEn.equals(employeeRecordsDTO.motherNameEng)
                && dto.motherNameBn.equals(employeeRecordsDTO.motherNameBng)
                && dto.dob == employeeRecordsDTO.dateOfBirth
                && dto.nid.equals(employeeRecordsDTO.nid)
                && dto.height == employeeRecordsDTO.height
                && dto.bloodGroup == employeeRecordsDTO.bloodGroup
                && dto.identificationSign.equals(employeeRecordsDTO.identificationMark)
                && dto.mobileNumber.equals(employeeRecordsDTO.officePhoneNumber)
                && dto.presentAddressEn.equals(employeeRecordsDTO.presentAddress)
                && dto.presentAddressBn.equals(employeeRecordsDTO.presentAddressBng)
                && dto.permanentAddressEn.equals(employeeRecordsDTO.permanentAddress)
                && dto.permanentAddressBn.equals(employeeRecordsDTO.permanentAddressBng);
    }

    public List<CardEmployeeInfoDTO> getByIds(List<Long> ids) {
        List<CardEmployeeInfoDTO> list = new ArrayList<>();
        List<Long> notFoundInCacheList = ids.stream()
                .peek(id -> addToListIfFoundInCache(id, list))
                .filter(id -> mapById.get(id) == null)
                .collect(Collectors.toList());
        if (notFoundInCacheList.size() > 0) {
            List<CardEmployeeInfoDTO> list1 = cardEmployeeInfoDAO.getDTOs(notFoundInCacheList);
            if (list1 != null && list1.size() > 0) {
                list.addAll(list1);
            }
        }
        return list;
    }

    private void addToListIfFoundInCache(long id, List<CardEmployeeInfoDTO> list) {
        CardEmployeeInfoDTO dto = mapById.get(id);
        if (dto != null) {
            list.add(dto);
        }
    }
}