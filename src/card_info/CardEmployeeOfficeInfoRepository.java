package card_info;

/*
 * @author Md. Erfan Hossain
 * @created 01/05/2021 - 10:42 PM
 * @project parliament
 */

import employee_offices.EmployeeOfficeDTO;
import employee_offices.EmployeeOfficeRepository;
import employee_records.Employee_recordsDTO;
import employee_records.Employee_recordsRepository;
import office_unit_organograms.OfficeUnitOrganograms;
import office_unit_organograms.OfficeUnitOrganogramsRepository;
import office_units.Office_unitsDTO;
import office_units.Office_unitsRepository;
import org.apache.log4j.Logger;
import util.LockManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

@SuppressWarnings({"Duplicates"})
public class CardEmployeeOfficeInfoRepository {

    private final Logger logger = Logger.getLogger(CardEmployeeOfficeInfoRepository.class);
    private final CardEmployeeOfficeInfoDAO cardEmployeeOfficeInfoDAO = CardEmployeeOfficeInfoDAO.getInstance();

    private final Map<Long,CardEmployeeOfficeInfoDTO> mapById = new ConcurrentHashMap<>();
    private final Map<Long,CardEmployeeOfficeInfoDTO> mapByEmployeeRecordId = new ConcurrentHashMap<>();

    private CardEmployeeOfficeInfoRepository() {
        reload();
    }

    private static class CardEmployeeOfficeInfoRepositoryLazyLoader {
        static final CardEmployeeOfficeInfoRepository INSTANCE = new CardEmployeeOfficeInfoRepository();
    }

    public static CardEmployeeOfficeInfoRepository getInstance() {
        return CardEmployeeOfficeInfoRepositoryLazyLoader.INSTANCE;
    }

    public void reload() {
        List<CardEmployeeOfficeInfoDTO> list = cardEmployeeOfficeInfoDAO.getAllDTOs(true);
        if (list != null && list.size() > 0) {
            list.stream()
                    .filter(dto -> dto.isDeleted == 0)
                    .forEach(dto -> {
                        mapById.put(dto.iD, dto);
                        mapByEmployeeRecordId.put(dto.employeeRecordsId, dto);
                    });
        }
    }


    public CardEmployeeOfficeInfoDTO getById(long id){
        if(mapById.get(id) == null){
            synchronized (LockManager.getLock(id+"CEOIRBI")){
                CardEmployeeOfficeInfoDTO dto = cardEmployeeOfficeInfoDAO.getDTOFromIdDeletedOrNot(id);
                if(dto != null){
                    mapById.put(dto.iD,dto);
                    if(dto.isDeleted == 0){
                        mapByEmployeeRecordId.put(dto.employeeRecordsId,dto);
                    }
                }
            }

        }
        return mapById.get(id);
    }

    public CardEmployeeOfficeInfoDTO getByEmployeeRecordId(long employeeRecordIdd) {
        return getByEmployeeRecordId(employeeRecordIdd, 0);
    }


    public CardEmployeeOfficeInfoDTO getByEmployeeRecordId(long employeeRecordId,long requesterId){
        synchronized (LockManager.getLock(employeeRecordId+"CEOIRBERI")){
            if(mapByEmployeeRecordId.get(employeeRecordId) == null){
                List<CardEmployeeOfficeInfoDTO> list = cardEmployeeOfficeInfoDAO.getByEmployeeId(employeeRecordId);
                if(list != null && list.size()>0){
                    CardEmployeeOfficeInfoDTO dto = list.get(0);
                    mapById.put(dto.iD,dto);
                    mapByEmployeeRecordId.put(employeeRecordId,dto);
                }
            }
            if(mapByEmployeeRecordId.get(employeeRecordId) == null){
                createCardEmployeeOfficeInfoDTO(employeeRecordId,requesterId);
            }else{
                CardEmployeeOfficeInfoDTO dto = mapByEmployeeRecordId.get(employeeRecordId);
                if(!isMatch(dto)){
                    boolean deleteResult = cardEmployeeOfficeInfoDAO.delete(requesterId,dto.iD);
                    if(deleteResult){
                        createCardEmployeeOfficeInfoDTO(employeeRecordId,requesterId);
                        mapById.remove(dto.iD);
                    }
                }
            }
        }

        return mapByEmployeeRecordId.get(employeeRecordId);
    }

    private boolean isMatch(CardEmployeeOfficeInfoDTO dto) {
        Employee_recordsDTO employeeRecordsDTO = Employee_recordsRepository.getInstance().getById(dto.employeeRecordsId);
        EmployeeOfficeDTO employeeOfficeDTO = EmployeeOfficeRepository.getInstance().getByEmployeeRecordIdIsDefault(dto.employeeRecordsId);
        OfficeUnitOrganograms officeUnitOrganograms = OfficeUnitOrganogramsRepository.getInstance().getById(employeeOfficeDTO.officeUnitOrganogramId);
        Office_unitsDTO officeUnitsDTO = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(employeeOfficeDTO.officeUnitId);
        return dto.empOfficerCat == employeeRecordsDTO.officerTypeCat
                && dto.employeeClassCat == employeeRecordsDTO.employeeClass
                && dto.employmentCat == employeeRecordsDTO.employmentType
                && dto.userName.equals(employeeRecordsDTO.employeeNumber)
                && dto.officeUnitId == officeUnitsDTO.iD
                && dto.officeUnitEng.equals(officeUnitsDTO.unitNameEng)
                && dto.officeUnitBng.equals(officeUnitsDTO.unitNameBng)
                && dto.organogramId == officeUnitOrganograms.id
                && dto.organogramEng.equals(officeUnitOrganograms.designation_eng)
                && dto.organogramBng.equals(officeUnitOrganograms.designation_bng);
    }

    private void createCardEmployeeOfficeInfoDTO(long employeeRecordId, long requesterId) {
        Employee_recordsDTO employeeRecordsDTO = Employee_recordsRepository.getInstance().getById(employeeRecordId);
        if (employeeRecordsDTO == null) {
            return;
        }
        EmployeeOfficeDTO employeeOfficeDTO = EmployeeOfficeRepository.getInstance().getByEmployeeRecordIdIsDefault(employeeRecordId);
        if (employeeOfficeDTO == null) {
            return;
        }
        OfficeUnitOrganograms officeUnitOrganograms = OfficeUnitOrganogramsRepository.getInstance().getById(employeeOfficeDTO.officeUnitOrganogramId);
        if (officeUnitOrganograms == null) {
            return;
        }

        Office_unitsDTO officeUnitsDTO = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(employeeOfficeDTO.officeUnitId);
        if (officeUnitsDTO == null) {
            return;
        }
        CardEmployeeOfficeInfoDTO dto = new CardEmployeeOfficeInfoDTO();
        dto.employeeRecordsId = employeeRecordId;
        dto.empOfficerCat = employeeRecordsDTO.officerTypeCat;
        dto.employeeClassCat = employeeRecordsDTO.employeeClass;
        dto.employmentCat = employeeRecordsDTO.employmentType;
        dto.userName = employeeRecordsDTO.employeeNumber;
        dto.officeUnitId = officeUnitsDTO.iD;
        dto.officeUnitEng = officeUnitsDTO.unitNameEng;
        dto.officeUnitBng = officeUnitsDTO.unitNameBng;
        dto.organogramId = officeUnitOrganograms.id;
        dto.organogramEng = officeUnitOrganograms.designation_eng;
        dto.organogramBng = officeUnitOrganograms.designation_bng;

        dto.insertedBy = dto.modifiedBy = requesterId;
        dto.insertionTime = dto.lastModificationTime = System.currentTimeMillis();
        try {
            cardEmployeeOfficeInfoDAO.add(dto);
            mapById.put(dto.iD, dto);
            mapByEmployeeRecordId.put(dto.employeeRecordsId, dto);
        } catch (Exception e) {
            logger.error(e);
        }
    }

    public List<CardEmployeeOfficeInfoDTO> getByIds(List<Long> ids) {
        List<CardEmployeeOfficeInfoDTO> list = new ArrayList<>();
        List<Long> notFoundInCacheList = ids.stream()
                .peek(id -> addToListIfFoundInCache(id, list))
                .filter(id -> mapById.get(id) == null)
                .collect(Collectors.toList());
        if (notFoundInCacheList.size() > 0) {
            List<CardEmployeeOfficeInfoDTO> list1 = cardEmployeeOfficeInfoDAO.getDTOs(notFoundInCacheList);
            if (list1 != null && list1.size() > 0) {
                list.addAll(list1);
            }
        }
        return list;
    }

    private void addToListIfFoundInCache(long id, List<CardEmployeeOfficeInfoDTO> list) {
        CardEmployeeOfficeInfoDTO dto = mapById.get(id);
        if (dto != null) {
            list.add(dto);
        }
    }
}