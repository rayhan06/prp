package card_info;

/*
 * @author Md. Erfan Hossain
 * @created 01/05/2021 - 7:39 PM
 * @project parliament
 */

public enum
CardStatusEnum {
    WAITING_FOR_APPROVAL(1),
    WAITING_FOR_POLICE_VERIFICATION(2),
    APPROVED(3),
    REJECTED(4),
    PRINTING(5),
    READY_FOR_DELIVER(6),
    DELIVERED(7),
    ACTIVATE(8),
    LOST(9),
    BLOCKED(10),
    TEMPORARY_BLOCKED(11)
    ;

    private final int value;

    CardStatusEnum(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
    
    public static String getColor(int value){
        if(value == CardStatusEnum.WAITING_FOR_APPROVAL.getValue()){
            return "#22ccc1";
        }else if(value == CardStatusEnum.WAITING_FOR_POLICE_VERIFICATION.getValue()){
            return "#0debaa";
        } else if(value == CardStatusEnum.APPROVED.getValue()){
            return "green";
        } else if(value == CardStatusEnum.REJECTED.getValue()){
            return "crimson";
        } else if(value == CardStatusEnum.PRINTING.getValue()){
            return "#0c91e5";
        } else if(value == CardStatusEnum.READY_FOR_DELIVER.getValue()){
            return "#0bd398";
        } else if(value == CardStatusEnum.DELIVERED.getValue()){
            return "#0f9af0";
        } else if(value == CardStatusEnum.ACTIVATE.getValue()){
            return "forestgreen";
        } else if(value == CardStatusEnum.LOST.getValue()){
            return "maroon";
        }else if(value == CardStatusEnum.BLOCKED.getValue()){
            return "darkred";
        }else{ //CardStatusEnum.TEMPORARY_BLOCKED
            return "red";
        }
    }
}
