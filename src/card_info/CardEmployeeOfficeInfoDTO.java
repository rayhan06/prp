package card_info;

import employee_offices.EmployeeOfficeDTO;
import employee_records.Employee_recordsDTO;
import office_unit_organograms.OfficeUnitOrganograms;
import office_unit_organograms.OfficeUnitOrganogramsRepository;
import office_units.Office_unitsDTO;
import office_units.Office_unitsRepository;
import util.CommonDTO;

public class CardEmployeeOfficeInfoDTO extends CommonDTO {
    public long employeeRecordsId = 0;
    public int empOfficerCat = 0;
    public int employeeClassCat = 0;
    public int employmentCat = 0;

    public String userName = "";

    public long officeUnitId = 0;
    public String officeUnitEng = "";
    public String officeUnitBng = "";

    public long organogramId = 0;
    public String organogramEng = "";
    public String organogramBng = "";

    public long insertedBy = 0;
    public long insertionTime = 0;
    public long modifiedBy = 0;

    public CardEmployeeOfficeInfoDTO() {

    }

    public CardEmployeeOfficeInfoDTO(Employee_recordsDTO empDto, EmployeeOfficeDTO employeeOfficeDTO) {
        this.employeeRecordsId = empDto.iD;
        this.empOfficerCat = empDto.officerTypeCat;
        this.employeeClassCat = empDto.employeeClass;
        this.employmentCat = empDto.employmentType;

        this.userName = empDto.employeeNumber;
        if (employeeOfficeDTO != null) {
            this.officeUnitId = employeeOfficeDTO.officeUnitId;
            Office_unitsDTO office_unitsDTO = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(this.officeUnitId);
            if(office_unitsDTO!=null){
                this.officeUnitEng = office_unitsDTO.unitNameEng;
                this.officeUnitBng = office_unitsDTO.unitNameBng;
            }

            this.organogramId = employeeOfficeDTO.officeUnitOrganogramId;
            OfficeUnitOrganograms officeUnitOrganograms = OfficeUnitOrganogramsRepository.getInstance().getById(this.organogramId);
            if(officeUnitOrganograms!=null){
                this.organogramEng = officeUnitOrganograms.designation_eng;
                this.organogramBng = officeUnitOrganograms.designation_bng;
            }
        }
    }

    public boolean hasSameData(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CardEmployeeOfficeInfoDTO that = (CardEmployeeOfficeInfoDTO) o;
        return employeeRecordsId == that.employeeRecordsId
                && empOfficerCat == that.empOfficerCat
                && employeeClassCat == that.employeeClassCat
                && employmentCat == that.employmentCat
                && userName.equals(that.userName)
                && officeUnitEng.equals(that.officeUnitEng)
                && officeUnitBng.equals(that.officeUnitBng)
                && organogramEng.equals(that.organogramEng)
                && organogramBng.equals(that.organogramBng)
                && officeUnitId == that.officeUnitId
                && organogramId == that.organogramId;
    }

    @Override
    public String toString() {
        return "Card_employee_office_infoDTO{" +
                "employeeRecordsId=" + employeeRecordsId +
                ", empOfficerCat=" + empOfficerCat +
                ", employeeClassCat=" + employeeClassCat +
                ", employmentCat=" + employmentCat +
                ", userName='" + userName + '\'' +
                ", officeUnitId=" + officeUnitId +
                ", officeUnitEng='" + officeUnitEng + '\'' +
                ", officeUnitBng='" + officeUnitBng + '\'' +
                ", organogramId=" + organogramId +
                ", organogramEng='" + organogramEng + '\'' +
                ", organogramBng='" + organogramBng + '\'' +
                ", insertedBy=" + insertedBy +
                ", insertionTime=" + insertionTime +
                ", modifiedBy=" + modifiedBy +
                '}';
    }

}
