package card_info;

import employee_offices.EmployeeOfficeDTO;
import employee_offices.EmployeeOfficeRepository;
import employee_records.Employee_recordsDTO;
import employee_records.Employee_recordsRepository;
import geolocation.GeoLocationDAO2;
import geolocation.GeoLocationUtils;
import org.apache.commons.codec.binary.Base64;
import pb.CatRepository;
import util.StringUtils;

public class CardInfoModel {
    public String name = "";
    public String post = "";
    public String fatherName = "";
    public String motherName = "";
    public String dateOfBirth = "";
    public String nidNumber = "";
    public String presentAddress = "";
    public String permanentAddress = "";
    public String office = "";
    public String height = "";
    public String identificationSign = "";
    public String bloodGroup = "";
    public String mobileNumber = "";
    public String email = "";
    public String signatureSrc = "";
    public String photoSrc = "";
    public String nidFrontSrc = "";
    public String nidBackSrc = "";

    public CardInfoModel(long employeeRecordId,String Language) {
        boolean isLanguageEnglish = Language.equalsIgnoreCase("English");
        Employee_recordsDTO employeeRecordsDTO = Employee_recordsRepository.getInstance().getById(employeeRecordId);
        EmployeeOfficeDTO employeeOfficeDTO = EmployeeOfficeRepository.getInstance().getByEmployeeRecordIdIsDefault(employeeRecordId);
        CardEmployeeInfoDTO employeeInfoDTO = new CardEmployeeInfoDTO(employeeRecordsDTO);
        CardEmployeeOfficeInfoDTO officeInfoDTO = new CardEmployeeOfficeInfoDTO(employeeRecordsDTO, employeeOfficeDTO);
        name = isLanguageEnglish ? employeeInfoDTO.nameEn : employeeInfoDTO.nameBn;
        fatherName = isLanguageEnglish ? employeeInfoDTO.fatherNameEn : employeeInfoDTO.fatherNameBn;
        motherName = isLanguageEnglish ? employeeInfoDTO.motherNameEn : employeeInfoDTO.motherNameBn;

        dateOfBirth = StringUtils.getFormattedDate(Language, employeeInfoDTO.dob);
        nidNumber = StringUtils.convertBanglaIfLanguageIsBangla(Language, employeeInfoDTO.nid);

        presentAddress = isLanguageEnglish ? GeoLocationUtils.getGeoLocationString(employeeInfoDTO.presentAddressEn, Language)
                                           : GeoLocationUtils.getGeoLocationString(employeeInfoDTO.presentAddressBn, Language);
        permanentAddress = isLanguageEnglish ? GeoLocationUtils.getGeoLocationString(employeeInfoDTO.permanentAddressEn, Language)
                                           : GeoLocationUtils.getGeoLocationString(employeeInfoDTO.permanentAddressBn, Language);

        post = isLanguageEnglish ? officeInfoDTO.organogramEng : officeInfoDTO.organogramBng;
        office = isLanguageEnglish ? officeInfoDTO.officeUnitEng : officeInfoDTO.officeUnitBng;

        if(employeeInfoDTO.height>0){
            height = StringUtils.convertBanglaIfLanguageIsBangla(Language, String.valueOf(employeeInfoDTO.height));
        }
        identificationSign = employeeInfoDTO.identificationSign;
        bloodGroup = CatRepository.getInstance().getText(Language, "blood_group", employeeInfoDTO.bloodGroup);
        mobileNumber = StringUtils.convertBanglaIfLanguageIsBangla(Language, employeeInfoDTO.mobileNumber);
        email = employeeInfoDTO.email;

        photoSrc = "data:image/jpg;base64,".concat(
                employeeRecordsDTO.photo != null ? new String(Base64.encodeBase64(employeeRecordsDTO.photo)) : ""
        );

        signatureSrc = "data:image/jpg;base64,".concat(
                employeeRecordsDTO.signature != null ? new String(Base64.encodeBase64(employeeRecordsDTO.signature)) : ""
        );
        
        nidFrontSrc = "data:image/jpg;base64,".concat(
                employeeRecordsDTO.nidFrontPhoto != null ? new String(Base64.encodeBase64(employeeRecordsDTO.nidFrontPhoto)) : ""
        );
        
        nidBackSrc = "data:image/jpg;base64,".concat(
                employeeRecordsDTO.nidBackPhoto != null ? new String(Base64.encodeBase64(employeeRecordsDTO.nidBackPhoto)) : ""
        );
    }
}
