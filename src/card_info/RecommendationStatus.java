package card_info;

/*
 * @author Md. Erfan Hossain
 * @created 01/05/2021 - 7:52 PM
 * @project parliament
 */

public enum RecommendationStatus {
    RECOMMENDED(1),
    NOT_RECOMMENDED(2),
    ;

    private final int value;

    RecommendationStatus(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
