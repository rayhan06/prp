package card_info;

import common.EmployeeCommonDAOService;
import employee_records.Employee_recordsDTO;
import org.apache.log4j.Logger;
import user.UserDTO;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

@SuppressWarnings({"unused", "Duplicates"})
public class CardEmployeeInfoDAO implements EmployeeCommonDAOService<CardEmployeeInfoDTO> {
    private static final Logger logger = Logger.getLogger(CardEmployeeInfoDAO.class);

    private static final String addQuery = "INSERT INTO {tableName} (employee_records_id, name_eng, name_bng, "
            .concat("father_name_eng, father_name_bng, mother_name_eng, mother_name_bng, dob, nid, height, blood_group, ")
            .concat("identification_sign, mobile_number, email, present_address_en, present_address_bn, permanent_address_en, ")
            .concat("permanent_address_bn, modified_by, lastModificationTime, insert_by, insertion_time, isDeleted, id) ")
            .concat("VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

    private static final String updateQuery = "UPDATE {tableName} SET employee_records_id = ?, name_eng = ?, name_bng = ?, "
            .concat("father_name_eng = ?, father_name_bng = ?, mother_name_eng = ?, mother_name_bng = ?, dob = ?, nid = ?, ")
            .concat(" height = ?, blood_group = ?, identification_sign = ?, mobile_number = ?, email = ?, present_address_en = ?, present_address_bn = ?, ")
            .concat("permanent_address_en = ?, permanent_address_bn = ?, modified_by = ?, lastModificationTime = ? WHERE id = ?");

    private static CardEmployeeInfoDAO INSTANCE = null;

    private CardEmployeeInfoDAO() {
    }

    public static CardEmployeeInfoDAO getInstance(){
        if(INSTANCE == null){
            synchronized (CardEmployeeInfoDAO.class){
                if(INSTANCE == null){
                    INSTANCE = new CardEmployeeInfoDAO();
                }
            }
        }
        return INSTANCE;
    }

    @Override
    public void set(PreparedStatement ps, CardEmployeeInfoDTO dto, boolean isInsert) throws SQLException {
        int index = 0;

        ps.setLong(++index, dto.employeeRecordId);
        ps.setString(++index, dto.nameEn);
        ps.setString(++index, dto.nameBn);
        ps.setString(++index, dto.fatherNameEn);
        ps.setString(++index, dto.fatherNameBn);
        ps.setString(++index, dto.motherNameEn);
        ps.setString(++index, dto.motherNameBn);
        ps.setLong(++index, dto.dob);
        ps.setString(++index, dto.nid);
        ps.setDouble(++index, dto.height);
        ps.setLong(++index, dto.bloodGroup);
        ps.setString(++index, dto.identificationSign);
        ps.setString(++index, dto.mobileNumber);
        ps.setString(++index, dto.email);
        ps.setString(++index, dto.presentAddressEn);
        ps.setString(++index, dto.presentAddressBn);
        ps.setString(++index, dto.permanentAddressEn);
        ps.setString(++index, dto.permanentAddressBn);
        ps.setLong(++index, dto.modifiedBy);
        ps.setLong(++index, dto.lastModificationTime);

        if (isInsert) {
            ps.setLong(++index, dto.insertBy);
            ps.setLong(++index, dto.insertionTime);
            ps.setInt(++index, 0 /* dto.isDeleted */);
        }

        ps.setLong(++index, dto.iD);
    }

    @Override
    public CardEmployeeInfoDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            CardEmployeeInfoDTO dto = new CardEmployeeInfoDTO();

            dto.iD = rs.getLong("id");
            dto.employeeRecordId = rs.getLong("employee_records_id");
            dto.nameEn = rs.getString("name_eng");
            dto.nameBn = rs.getString("name_bng");
            dto.fatherNameEn = rs.getString("father_name_eng");
            dto.fatherNameBn = rs.getString("father_name_bng");
            dto.motherNameEn = rs.getString("mother_name_eng");
            dto.motherNameBn = rs.getString("mother_name_bng");
            dto.dob = rs.getLong("dob");
            dto.nid = rs.getString("nid");
            dto.height = rs.getDouble("height");
            dto.bloodGroup = rs.getInt("blood_group");
            dto.identificationSign = rs.getString("identification_sign");
            dto.mobileNumber = rs.getString("mobile_number");
            dto.email = rs.getString("email");
            dto.presentAddressEn = rs.getString("present_address_en");
            dto.presentAddressBn = rs.getString("present_address_bn");
            dto.permanentAddressEn = rs.getString("permanent_address_en");
            dto.permanentAddressBn = rs.getString("permanent_address_bn");
            dto.isDeleted = rs.getInt("isDeleted");
            dto.insertBy = rs.getLong("insert_by");
            dto.insertionTime = rs.getLong("insertion_time");
            dto.modifiedBy = rs.getLong("modified_by");
            dto.lastModificationTime = rs.getLong("lastModificationTime");

            return dto;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    @Override
    public String getTableName() {
        return "card_employee_info";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return null;
    }

    @Override
    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((CardEmployeeInfoDTO) commonDTO, updateQuery, false);
    }

    @Override
    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((CardEmployeeInfoDTO) commonDTO, addQuery, true);
    }

    public long getCardEmployeeInfoId(Employee_recordsDTO employeeRecordsDTO, UserDTO userDTO) throws Exception {
        CardEmployeeInfoDTO cardEmployeeInfoDTOFromEmpDto = new CardEmployeeInfoDTO(employeeRecordsDTO);

        List<CardEmployeeInfoDTO> dtoList = getByEmployeeId(employeeRecordsDTO.iD);
        if (dtoList != null && dtoList.size() > 0) {
            CardEmployeeInfoDTO currentCardEmployeeInfoDTO = dtoList.get(0);
            if(currentCardEmployeeInfoDTO.hasSameData(cardEmployeeInfoDTOFromEmpDto)){
                return currentCardEmployeeInfoDTO.iD;
            }
            delete(userDTO.ID, currentCardEmployeeInfoDTO.iD);
        }

        cardEmployeeInfoDTOFromEmpDto.insertBy
                = cardEmployeeInfoDTOFromEmpDto.modifiedBy
                = userDTO.ID;

        cardEmployeeInfoDTOFromEmpDto.insertionTime
                = cardEmployeeInfoDTOFromEmpDto.lastModificationTime
                = System.currentTimeMillis();

        return add(cardEmployeeInfoDTOFromEmpDto);
    }
}
