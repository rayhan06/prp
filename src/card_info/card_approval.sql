CREATE TABLE card_approval (
    ID BIGINT(20) UNSIGNED PRIMARY KEY,
    employee_record_id BIGINT(20) NOT NULL,
    employee_name_eng VARCHAR(255) NOT NULL,
    employee_name_bng VARCHAR(512) NOT NULL,
    office_unit_id BIGINT(20) NOT NULL,
    office_unit_eng VARCHAR(255) NOT NULL,
    office_unit_bng VARCHAR(512) NOT NULL,
    organogram_id BIGINT(20) NOT NULL,
    organogram_eng VARCHAR(255) NOT NULL,
    organogram_bng VARCHAR(512) NOT NULL,
    insertion_time BIGINT(20) DEFAULT 0,
    inserted_by VARCHAR(255)  DEFAULT 0,
    modified_by VARCHAR(255)  DEFAULT 0,
    isDeleted INT(11) DEFAULT '0',
    lastModificationTime BIGINT(20) DEFAULT 0
) COLLATE='utf8_general_ci' ENGINE=MyISAM;