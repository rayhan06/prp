CREATE TABLE card_employee_info
(
    id                   BIGINT(20) UNSIGNED PRIMARY KEY,
    employee_records_id  BIGINT(20)   DEFAULT NULL,
    name_eng             VARCHAR(255) DEFAULT NULL,
    name_bng             VARCHAR(255) DEFAULT NULL,
    father_name_eng      VARCHAR(255) DEFAULT NULL,
    father_name_bng      VARCHAR(255) DEFAULT NULL,
    mother_name_eng      VARCHAR(255) DEFAULT NULL,
    mother_name_bng      VARCHAR(255) DEFAULT NULL,
    dob                  BIGINT(20)   DEFAULT -62135791200000,
    nid                  VARCHAR(255) DEFAULT NULL,
    height               DOUBLE       DEFAULT 0,
    blood_group          BIGINT(20)   DEFAULT 0,
    identification_sign  VARCHAR(255) DEFAULT NULL,
    mobile_number        VARCHAR(255) DEFAULT NULL,
    present_address_en   VARCHAR(255) DEFAULT NULL,
    present_address_bn   VARCHAR(255) DEFAULT NULL,
    permanent_address_en VARCHAR(255) DEFAULT NULL,
    permanent_address_bn VARCHAR(255) DEFAULT NULL,
    isDeleted            INT(11)      DEFAULT 0,
    insert_by            VARCHAR(255) DEFAULT NULL,
    insertion_time       BIGINT(20)   DEFAULT -62135791200000,
    modified_by          VARCHAR(255) DEFAULT NULL,
    lastModificationTime BIGINT(20)   DEFAULT -62135791200000
) ENGINE = MyISAM
  DEFAULT CHARSET = utf8
  COLLATE = utf8_unicode_ci;