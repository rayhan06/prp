package public_access;

import config.GlobalConfigConstants;
import config.GlobalConfigurationRepository;
import login.LoginDTO;
import org.apache.log4j.Logger;
import permission.MenuConstants;
import recruitment_job_description.Recruitment_job_descriptionDAO;
import role.PermissionRepository;
import sessionmanager.SessionConstants;
import user.UserDTO;
import util.CommonRequestHandler;
import util.RecordNavigationManager4;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


/**
 * Servlet implementation class Recruitment_job_descriptionServlet
 */
@WebServlet("/PublicServlet")
@MultipartConfig
public class PublicServlet extends HttpServlet
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(PublicServlet.class);

    String tableName = "recruitment_job_description";

	Recruitment_job_descriptionDAO recruitment_job_descriptionDAO;
	CommonRequestHandler commonRequestHandler;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public PublicServlet()
	{
        super();
    	try
    	{
			recruitment_job_descriptionDAO = new Recruitment_job_descriptionDAO(tableName);
			commonRequestHandler = new CommonRequestHandler(recruitment_job_descriptionDAO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		System.out.println("In doget request = " + request);
		try
		{
//			LoginDTO loginDTO = new LoginDTO();
//			long defaultUserID = Long.parseLong(GlobalConfigurationRepository.getInstance().getGlobalConfigDTOByID(GlobalConfigConstants.DEFAULT_USER_ID).value);
//			loginDTO.userID = defaultUserID;
//			loginDTO.isOisf = 0;
//			request.getSession(true).setAttribute(SessionConstants.USER_LOGIN, loginDTO);

			String actionType = request.getParameter("actionType");
			if(actionType.equals("public_search"))
			{
				System.out.println("search requested");
				String filter = ""; //"";
				long currentDate = System.currentTimeMillis();
				filter = currentDate + " < last_application_date "; //shouldn't be directly used, rather manipulate it.
				filter +=  " AND " + currentDate + " > first_application_date ";
				searchPublicRecruitment_job_description(request, response, true, filter);


			}
			else if(actionType.equals("viewResultNotice")){
				RequestDispatcher rd;
				rd = request.getRequestDispatcher("result_notice/result_notice_view.jsp");
				rd.forward(request, response);
			}

			/*circular start*/

			else if(actionType.equals("circularNotice")){
				RequestDispatcher rd;
				rd = request.getRequestDispatcher("recruitment_exam_notice/recruitment_circular_noticeViewBody.jsp");
				rd.forward(request, response);
			}
			/*circular end*/

			/*exam notice start*/

			else if(actionType.equals("viewExamNotice")){
				RequestDispatcher rd;
				rd = request.getRequestDispatcher("recruitment_exam_notice/recruitment_exam_notice_view.jsp");
				rd.forward(request, response);
			}

			else if(actionType.equals("viewWrittenExamNotice")){
				RequestDispatcher rd;
				rd = request.getRequestDispatcher("recruitment_exam_notice/recruitment_job_specific_exam_written_notice_publicEditBody.jsp");
				rd.forward(request, response);
			}

			else if(actionType.equals("viewPracticalExamNotice")){
				RequestDispatcher rd;
				rd = request.getRequestDispatcher("recruitment_exam_notice/recruitment_job_specific_exam_practical_notice_publicEditBody.jsp");
				rd.forward(request, response);
			}

			/*exam notice end*/

			/*final result start*/

			else if(actionType.equals("viewFinalResultNotice")){
				RequestDispatcher rd;
				rd = request.getRequestDispatcher("result_notice/final_result_notice_view.jsp");
				rd.forward(request, response);
			}

			/*final result end*/

			else if (actionType.equals("getResultNotice")) {

				RequestDispatcher rd;
				rd = request.getRequestDispatcher("recruitment_job_specific_candidate_list/recruitment_job_specific_result_notice_publicEditBody.jsp");
				rd.forward(request, response);
			}
			else
			{
				request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}





	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{

	}






	private void searchPublicRecruitment_job_description(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException
	{
		System.out.println("in  searchRecruitment_job_description 1");
		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
		UserDTO userDTO = new UserDTO();

		boolean hasAjax = false;

		String ajax = request.getParameter("ajax");
		if(ajax != null && !ajax.equalsIgnoreCase(""))
		{
			hasAjax = true;
		}

		RecordNavigationManager4 rnManager = new RecordNavigationManager4(
				SessionConstants.NAV_RECRUITMENT_JOB_DESCRIPTION,
				request,
				recruitment_job_descriptionDAO,
				SessionConstants.VIEW_RECRUITMENT_JOB_DESCRIPTION,
				SessionConstants.SEARCH_RECRUITMENT_JOB_DESCRIPTION,
				tableName,
				isPermanent,
				userDTO,
				filter,
				true);
		try
		{
			System.out.println("trying to dojob");
			rnManager.doJob(loginDTO);
		}
		catch(Exception e)
		{
			System.out.println("failed to dojob" + e);
		}

		request.setAttribute("recruitment_job_descriptionDAO",recruitment_job_descriptionDAO);
		RequestDispatcher rd;

		if(hasAjax == false)
		{

			System.out.println("Going to recruitment_job_description/recruitment_job_descriptionPublicSearch.jsp");
			rd = request.getRequestDispatcher("recruitment_job_description/recruitment_job_descriptionPublicSearch.jsp");
		}
		else
		{
			System.out.println("Going to recruitment_job_description/recruitment_job_descriptionSearchForm.jsp");
			rd = request.getRequestDispatcher("recruitment_job_description/recruitment_job_descriptionPublicSearchForm.jsp");
		}

		rd.forward(request, response);
	}

}

