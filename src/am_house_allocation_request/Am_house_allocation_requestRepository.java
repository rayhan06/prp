package am_house_allocation_request;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class Am_house_allocation_requestRepository implements Repository {
	Am_house_allocation_requestDAO am_house_allocation_requestDAO = null;
	
	static Logger logger = Logger.getLogger(Am_house_allocation_requestRepository.class);
	Map<Long, Am_house_allocation_requestDTO>mapOfAm_house_allocation_requestDTOToiD;

  
	private Am_house_allocation_requestRepository(){
		am_house_allocation_requestDAO = Am_house_allocation_requestDAO.getInstance();
		mapOfAm_house_allocation_requestDTOToiD = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	private static class LazyLoader{
        final static Am_house_allocation_requestRepository INSTANCE = new Am_house_allocation_requestRepository();
    }

    public static Am_house_allocation_requestRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

	public void reload(boolean reloadAll){
		try {
			List<Am_house_allocation_requestDTO> am_house_allocation_requestDTOs = am_house_allocation_requestDAO.getAllDTOs(reloadAll);
			for(Am_house_allocation_requestDTO am_house_allocation_requestDTO : am_house_allocation_requestDTOs) {
				Am_house_allocation_requestDTO oldAm_house_allocation_requestDTO = getAm_house_allocation_requestDTOByID(am_house_allocation_requestDTO.iD);
				if( oldAm_house_allocation_requestDTO != null ) {
					mapOfAm_house_allocation_requestDTOToiD.remove(oldAm_house_allocation_requestDTO.iD);
				
					
				}
				if(am_house_allocation_requestDTO.isDeleted == 0) 
				{
					
					mapOfAm_house_allocation_requestDTOToiD.put(am_house_allocation_requestDTO.iD, am_house_allocation_requestDTO);
				
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Am_house_allocation_requestDTO> getAm_house_allocation_requestList() {
		List <Am_house_allocation_requestDTO> am_house_allocation_requests = new ArrayList<Am_house_allocation_requestDTO>(this.mapOfAm_house_allocation_requestDTOToiD.values());
		return am_house_allocation_requests;
	}
	
	
	public Am_house_allocation_requestDTO getAm_house_allocation_requestDTOByID( long ID){
		return mapOfAm_house_allocation_requestDTOToiD.get(ID);
	}

	
	@Override
	public String getTableName() {
		return "am_house_allocation_request";
	}
}


