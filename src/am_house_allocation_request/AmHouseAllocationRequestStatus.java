package am_house_allocation_request;

import java.util.Arrays;

public enum AmHouseAllocationRequestStatus {
    WAITING_FOR_APPROVAL(1, "#22ccc1"),
    APPROVED(3, "green"),
    APPROVED_BY_FIRST_APPROVER(33, "#3474eb"),
    REJECTED(4, "crimson"),
    REJECTED_BY_FIRST_APPROVER(32, "crimson"),
    WITHDRAWN(15, "#9400D3"),
    ;

    private final int value;
    private final String color;

    AmHouseAllocationRequestStatus(int value, String color) {
        this.value = value;
        this.color = color;
    }

    public int getValue() {
        return value;
    }

    public String getColor() {
        return color;
    }

    public static String getColor(int value) {
        return Arrays.stream(values())
                     .filter(status -> status.value == value)
                     .map(AmHouseAllocationRequestStatus::getColor)
                     .findFirst()
                     .orElse("");
    }
}
