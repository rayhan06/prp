package am_house_allocation_request;
import java.io.IOException;
import java.io.*;
import java.text.SimpleDateFormat;
import javax.rmi.CORBA.Util;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;

import am_grade_house_mapping.Am_grade_house_mappingRepository;
import am_house_allocation_approval_mapping.*;
import am_reliable_family_member.Am_reliable_family_memberDAO;
import am_reliable_family_member.Am_reliable_family_memberDTO;
import card_info.CardApprovalResponse;
import category.CategoryDTO;
import common.ApiResponse;
import common.CommonDAOService;
import employee_assign.EmployeeSearchIds;
import employee_assign.EmployeeSearchModalUtil;
import employee_assign.EmployeeSearchModel;
import employee_family_info.Employee_family_infoDAO;
import employee_family_info.Employee_family_infoDTO;
import employee_offices.EmployeeOfficeDTO;
import employee_offices.EmployeeOfficeRepository;
import employee_offices.EmployeeOfficesDAO;
import employee_records.Employee_recordsDTO;
import employee_records.Employee_recordsRepository;
import employee_service_history.Employee_service_historyDAO;
import employee_service_history.Employee_service_historyDTO;
import grade_wise_pay_scale.Grade_wise_pay_scaleRepository;
import office_unit_organograms.OfficeUnitOrganograms;
import office_unit_organograms.OfficeUnitOrganogramsRepository;
import office_units.Office_unitsRepository;
import offices.OfficesDTO;
import offices.OfficesRepository;
import org.apache.log4j.Logger;
import login.LoginDTO;
import permission.MenuConstants;
import relation.RelationRepository;
import role.PermissionRepository;
import role.RoleDTO;
import sessionmanager.SessionConstants;
import task_type.TaskTypeEnum;
import user.UserDTO;
import util.*;
import javax.servlet.http.*;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import language.LC;
import language.LM;
import common.BaseServlet;
import com.google.gson.Gson;
import pb.*;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import files.*;
import vm_requisition.CommonApprovalStatus;

@WebServlet("/Am_house_allocation_requestServlet")
@MultipartConfig
public class Am_house_allocation_requestServlet extends BaseServlet
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Am_house_allocation_requestServlet.class);

    @Override
    public String getTableName() {
        return Am_house_allocation_requestDAO.getInstance().getTableName();
    }

    @Override
    public String getServletName() {
        return "Am_house_allocation_requestServlet";
    }

    @Override
    public Am_house_allocation_requestDAO getCommonDAOService() {
        return Am_house_allocation_requestDAO.getInstance();
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[] {MenuConstants.AM_HOUSE_ALLOCATION_REQUEST_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[] {MenuConstants.AM_HOUSE_ALLOCATION_REQUEST_UPDATE};
    }

    @Override
	public boolean getDeletePermission(HttpServletRequest request){
		String[] IDsToDelete = request.getParameterValues("ID");
		if(IDsToDelete.length>0){
			List<Long> ids = Stream.of(IDsToDelete)
					.map(Long::parseLong)
					.collect(Collectors.toList());
			int notPendingCountById = Am_house_allocation_requestDAO.getInstance().getNotPendingCountById(ids);
			return notPendingCountById == 0;
		}
		return true;
	}

    @Override
    public int[] getSearchMenuConstants() {
        return new int[] {MenuConstants.AM_HOUSE_ALLOCATION_REQUEST_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Am_house_allocation_requestServlet.class;
    }
	FilesDAO filesDAO = new FilesDAO();
    private final Gson gson = new Gson();

    @Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		CommonLoginData commonLoginData = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get();
		init(request);
		try {
			String actionType = request.getParameter("actionType");
			switch (actionType) {
				case "getRequesterDetails":
					if (Utils.checkPermission(commonLoginData.userDTO, getEditPageMenuConstants()) && getEditPagePermission(request)) {
						getRequesterDetails(request, response);
						return;
					}
					break;
				case "getAddPage":
					if (Utils.checkPermission(commonLoginData.userDTO, getEditPageMenuConstants()) && getEditPagePermission(request)) {
						getMultiPageView(request, response);
						return;
					}
					break;
				case "getEditPage":
					if (Utils.checkPermission(commonLoginData.userDTO, getEditPageMenuConstants()) && getEditPagePermission(request)) {
						getMultiPageView(request, response);
						return;
					}
					break;
				case "downloadDropzoneFile": {
					long id = Long.parseLong(request.getParameter("id"));
					FilesDTO filesDTO = (FilesDTO) filesDAO.getDTOByID(id);
					Utils.ProcessFile(request, response, filesDTO.fileTitle, filesDTO.inputStream);
					return;
				}
				case "DeleteFileFromDropZone": {
					long id = Long.parseLong(request.getParameter("id"));
					logger.debug("In delete file");
					filesDAO.hardDeleteByID(id);
					response.getWriter().write("Deleted");
					return;
				}
				case "getURL":
					String URL = request.getParameter("URL");
					response.sendRedirect(URL);
					return;
				case "search":
					if (Utils.checkPermission(commonLoginData.userDTO, getSearchMenuConstants()) && getSearchPagePermission(request)) {
						UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;
						RoleDTO role = PermissionRepository.getRoleDTOByRoleID(userDTO.roleID);
//						boolean isAdmin = role.ID == SessionConstants.VEHICLE_ADMIN_ROLE || role.ID == SessionConstants.ADMIN_ROLE;
						boolean isAdmin = role.ID == SessionConstants.ADMIN_ROLE || role.ID == SessionConstants.ASSET_ADMIN_ROLE;
						if (!isAdmin) {
							Map<String, String> extraCriteriaMap = new HashMap<>();
							extraCriteriaMap.put("inserted_by_organogram_id", String.valueOf(userDTO.organogramID));
							request.setAttribute(CommonDAOService.CRITERIA_MAP_KEY, extraCriteriaMap);
						}
						search(request, response);
						return;
					}
					break;
				case "prePendingSearch":
					if (Utils.checkPermission(commonLoginData.userDTO, getSearchMenuConstants()) && getSearchPagePermission(request)) {
						UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;
						RoleDTO role = PermissionRepository.getRoleDTOByRoleID(userDTO.roleID);
//						boolean isAdmin = role.ID == SessionConstants.VEHICLE_ADMIN_ROLE || role.ID == SessionConstants.ADMIN_ROLE;
						boolean isAdmin = role.ID == SessionConstants.ADMIN_ROLE || role.ID == SessionConstants.ASSET_ADMIN_ROLE;
						if (!isAdmin || true) {
							Map<String, String> extraCriteriaMap = new HashMap<>();
//							extraCriteriaMap.put("status", String.valueOf(CommonApprovalStatus.PENDING.getValue()));
							extraCriteriaMap.put("first_layer_approver_emp_id", String.valueOf(userDTO.employee_record_id));
							request.setAttribute(CommonDAOService.CRITERIA_MAP_KEY, extraCriteriaMap);
						}
						searchPrePending(request, response);
						return;
					}
					break;
				case "giveFirstLayerApproval":
					boolean hasPermission = true;
					if(hasPermission){
						long requestedHouseId = Long.parseLong(request.getParameter("amHouseAllocationRequestId"));
						Am_house_allocation_requestDTO amHouseAllocationDTO = Am_house_allocation_requestDAO.getInstance()
								.getDTOByID(requestedHouseId);

						amHouseAllocationDTO.firstLayerApprovalOrRejectionDate = System.currentTimeMillis();
					}
					return;
				case "getApprovalPage":
					logger.debug("here read house app");
					long amHouseAllocationRequestId = Long.parseLong(request.getParameter("amHouseAllocationRequestId"));
					setApprovalPageData(amHouseAllocationRequestId, request);
					request.getRequestDispatcher("am_house_allocation_first_approval/am_house_allocation_first_approvalView.jsp").forward(request, response);  //TODO AM HOUSE ALLOCATION CHANGE THIS
					return;
				case "view":
					logger.debug("view requested");
					if (Utils.checkPermission(commonLoginData.userDTO, getViewMenuConstants()) && getViewPagePermission(request)) {
						setDTOForJsp(request);
						finalize(request);
						request.getRequestDispatcher(commonPartOfDispatchURL() + "_newView.jsp?ID=" + getId(request)).forward(request,response);
						return;
					}
					break;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			logger.error(ex);
		}
		request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		CommonLoginData commonLoginData = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get();
		try {
			String actionType = request.getParameter("actionType");
			switch (actionType) {
				case "giveFirstLayerApproval":
					boolean hasPermission = true;
					if(hasPermission){
						long requestedHouseId = Long.parseLong(request.getParameter("amHouseAllocationRequestId"));
						Am_house_allocation_requestDTO am_house_allocation_requestDTO = Am_house_allocation_requestRepository
								.getInstance().getAm_house_allocation_requestDTOByID(requestedHouseId);

						UserDTO userDTO = commonLoginData.userDTO;
						String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
						am_house_allocation_requestDTO.firstLayerApproverOfficeNameEn = userDTO.fullName;
						am_house_allocation_requestDTO.status = CommonApprovalStatus.REQUISITION_APPROVED_BY_FIRST_APPROVER.getValue();
						am_house_allocation_requestDTO.firstLayerApprovalStatus = CommonApprovalStatus.REQUISITION_APPROVED_BY_FIRST_APPROVER.getValue();
						am_house_allocation_requestDTO.lastModificationTime = System.currentTimeMillis();

						Am_house_allocation_requestDAO.getInstance().update(am_house_allocation_requestDTO);

						List<Am_house_allocation_approval_mappingDTO> list =
								Am_house_allocation_approval_mappingRepository.getInstance().getAm_house_allocation_approval_mappingList()
										.stream().filter(dto->dto.amHouseAllocationRequestId == requestedHouseId)
										.collect(Collectors.toList());
						for(Am_house_allocation_approval_mappingDTO dto : list){
							dto.amHouseAllocationStatusCat =  am_house_allocation_requestDTO.status;
							Am_house_allocation_approval_mappingDAO.getInstance().update(dto);
						}

						List<Long> orgIds = new ArrayList<>();

						for(Am_house_allocation_approval_mappingDTO dto : list){
							EmployeeSearchIds defaultOfficeIds = EmployeeSearchModalUtil.getDefaultOfficeIds(dto.approverEmployeeRecordsId);
							EmployeeSearchModel model = EmployeeSearchModalUtil.getEmployeeSearchModel(defaultOfficeIds);
							orgIds.add(model.organogramId);
						}

						AmHouseAllocationNotification.getInstance().sendWaitingForApprovalNotification(
								orgIds, am_house_allocation_requestDTO
						);

						ApiResponse.sendSuccessResponse(response, "House Application Accepted");
					}
					return;
				case "giveFirstLayerRejection":
					hasPermission = true;
					if(hasPermission){
						long requestedHouseId = Long.parseLong(request.getParameter("amHouseAllocationRequestId"));
						Am_house_allocation_requestDTO amHouseAllocationDTO = Am_house_allocation_requestDAO.getInstance()
								.getDTOByID(requestedHouseId);

						UserDTO userDTO = commonLoginData.userDTO;
						String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
						amHouseAllocationDTO.firstLayerApproverOfficeNameEn = userDTO.fullName;
						amHouseAllocationDTO.status = CommonApprovalStatus.REQUISITION_REJECTED_BY_FIRST_APPROVER.getValue();
						amHouseAllocationDTO.firstLayerApprovalStatus = CommonApprovalStatus.REQUISITION_REJECTED_BY_FIRST_APPROVER.getValue();
						amHouseAllocationDTO.lastModificationTime = System.currentTimeMillis();

						String value = "";
						value = request.getParameter("reject_reason");
						if(value != null && !value.equals("")){
							amHouseAllocationDTO.firstLayerApproverRejectionReason = value;
							amHouseAllocationDTO.rejectionReason = amHouseAllocationDTO.firstLayerApproverRejectionReason;
						} else {
							ApiResponse.sendErrorResponse(response, UtilCharacter.getDataByLanguage(Language,
									"দয়া করে অনুমোদন বাতিলের কারণ লিখুন", "Please enter a rejection reason"));
						}

						Am_house_allocation_requestDAO.getInstance().update(amHouseAllocationDTO);

						List<Am_house_allocation_approval_mappingDTO> list =
								Am_house_allocation_approval_mappingRepository.getInstance().getAm_house_allocation_approval_mappingList()
										.stream().filter(dto->dto.amHouseAllocationRequestId == requestedHouseId)
										.collect(Collectors.toList());
						for(Am_house_allocation_approval_mappingDTO dto : list){
							dto.amHouseAllocationStatusCat =  amHouseAllocationDTO.status;
							Am_house_allocation_approval_mappingDAO.getInstance().update(dto);
						}
						ApiResponse.sendSuccessResponse(response, "House Application Rejected");
					}
					return;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			logger.error(ex);
		}
		super.doPost(request, response);
	}

	protected void searchPrePending(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, String> params = buildRequestParams(request);
		if (request.getAttribute(CommonDAOService.CRITERIA_MAP_KEY) != null) {
			Map<String, String> extraCriteriaMap = (Map<String, String>) request.getAttribute(CommonDAOService.CRITERIA_MAP_KEY);
			params.putAll(extraCriteriaMap);
		}
		RecordNavigator recordNavigator = getCommonDAOService().getRecordNavigator(params, getTableJoinClause(request), getWhereClause(request), getSortClause(request));
		request.setAttribute(RECORD_NAVIGATOR, recordNavigator);
		String ajax = request.getParameter("ajax");
		String url;
		if(ajax != null && ajax.equalsIgnoreCase("true")){
			url  = "am_house_allocation_first_approval/am_house_allocation_first_approvalSearchForm.jsp";
		}else {
			url  = "am_house_allocation_first_approval/am_house_allocation_first_approvalSearch.jsp";
		}
		logger.debug("url : " + url);
		request.getRequestDispatcher(url).forward(request, response);
	}

	private void setApprovalPageData(long amHouseAllocationRequestId, HttpServletRequest request) throws Exception {
		Am_house_allocation_requestDTO am_house_allocation_requestDTO = Am_house_allocation_requestDAO.getInstance().getDTOByID(amHouseAllocationRequestId);
		if (am_house_allocation_requestDTO == null)
			throw new Exception("No application for house allocation is not found with id = " + amHouseAllocationRequestId);
		request.setAttribute("am_house_allocation_requestDTO", am_house_allocation_requestDTO);
	}
	@Override
	public boolean getEditPermission(HttpServletRequest request) {
		return checkEditPermission(request);
	}

	@Override
	public boolean getEditPagePermission(HttpServletRequest request) {
		return checkEditPagePermission(request);
	}

	@Override
	public boolean getViewPagePermission(HttpServletRequest request) {
		return checkEditPagePermission(request);
	}

	private boolean checkEditPagePermission(HttpServletRequest request) {
		UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;
		RoleDTO role = PermissionRepository.getRoleDTOByRoleID(userDTO.roleID);
		boolean isAdmin = role.ID == SessionConstants.ADMIN_ROLE || role.ID == SessionConstants.ASSET_ADMIN_ROLE;
		if (isAdmin) {
			return true;
		}
		if (request.getParameter("ID") == null) {
			return true;
		}
		Am_house_allocation_requestDTO am_house_allocation_requestDTO = getCommonDAOService().getDTOFromID(Long.parseLong(request.getParameter("ID")));
		if (am_house_allocation_requestDTO == null) {
			return false;
		}
		return am_house_allocation_requestDTO.insertedByOrganogramId == userDTO.organogramID;
	}

	private boolean checkEditPermission(HttpServletRequest request) {
		UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;
		RoleDTO role = PermissionRepository.getRoleDTOByRoleID(userDTO.roleID);
		boolean isAdmin = role.ID == SessionConstants.ADMIN_ROLE || role.ID == SessionConstants.ASSET_ADMIN_ROLE;
		if (isAdmin) {
			return true;
		}
		if (request.getParameter("iD") == null) {
			return true;
		}
		Am_house_allocation_requestDTO am_house_allocation_requestDTO = getCommonDAOService().getDTOFromID(Long.parseLong(request.getParameter("iD")));
		if (am_house_allocation_requestDTO == null) {
			return false;
		}
		return am_house_allocation_requestDTO.insertedByOrganogramId == userDTO.organogramID;
	}


	@Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception
	{
		// TODO Auto-generated method stub

		    request.setAttribute("failureMessage", "");
			System.out.println("%%%% addAm_house_allocation_request");
			String language = userDTO.languageID == SessionConstants.ENGLISH? "English":"Bangla";
			Am_house_allocation_requestDTO am_house_allocation_requestDTO;
			SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");

			if(addFlag == true)
			{
				am_house_allocation_requestDTO = new Am_house_allocation_requestDTO();
			}
			else
			{
				am_house_allocation_requestDTO = Am_house_allocation_requestDAO.getInstance().getDTOFromID(Long.parseLong(request.getParameter("iD")));
			}

			String Value = "";

			Value = request.getParameter("requesterOrgId");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("requesterOrgId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				am_house_allocation_requestDTO.requesterOrgId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("requesterOfficeId");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("requesterOfficeId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				am_house_allocation_requestDTO.requesterOfficeId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("requesterOfficeUnitId");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("requesterOfficeUnitId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				am_house_allocation_requestDTO.requesterOfficeUnitId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

//			Value = request.getParameter("requesterEmpId");
//
//			if(Value != null && !Value.equalsIgnoreCase(""))
//			{
//				Value = Jsoup.clean(Value,Whitelist.simpleText());
//			}
//			System.out.println("requesterEmpId = " + Value);
//			if(Value != null && !Value.equalsIgnoreCase(""))
//			{
//				am_house_allocation_requestDTO.requesterEmpId = Long.parseLong(Value);
//			}
//			else
//			{
//				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//			}

			Value = request.getParameter("requesterPhoneNum");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("requesterPhoneNum = " + Value);
			if(Value != null)
			{
				am_house_allocation_requestDTO.requesterPhoneNum = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("requesterNameEn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("requesterNameEn = " + Value);
			if(Value != null)
			{
				am_house_allocation_requestDTO.requesterNameEn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("requesterNameBn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("requesterNameBn = " + Value);
			if(Value != null)
			{
				am_house_allocation_requestDTO.requesterNameBn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("requesterOfficeNameEn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("requesterOfficeNameEn = " + Value);
			if(Value != null)
			{
				am_house_allocation_requestDTO.requesterOfficeNameEn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("requesterOfficeNameBn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("requesterOfficeNameBn = " + Value);
			if(Value != null)
			{
				am_house_allocation_requestDTO.requesterOfficeNameBn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("requesterOfficeUnitNameEn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("requesterOfficeUnitNameEn = " + Value);
			if(Value != null)
			{
				am_house_allocation_requestDTO.requesterOfficeUnitNameEn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("requesterOfficeUnitNameBn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("requesterOfficeUnitNameBn = " + Value);
			if(Value != null)
			{
				am_house_allocation_requestDTO.requesterOfficeUnitNameBn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("requesterOfficeUnitOrgNameEn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("requesterOfficeUnitOrgNameEn = " + Value);
			if(Value != null)
			{
				am_house_allocation_requestDTO.requesterOfficeUnitOrgNameEn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("requesterOfficeUnitOrgNameBn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("requesterOfficeUnitOrgNameBn = " + Value);
			if(Value != null)
			{
				am_house_allocation_requestDTO.requesterOfficeUnitOrgNameBn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("dateOfBirth");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("dateOfBirth = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				try
				{
					Date d = f.parse(Value);
					am_house_allocation_requestDTO.dateOfBirth = d.getTime();
				}
				catch (Exception e)
				{
					e.printStackTrace();
					throw new Exception(LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_DATEOFBIRTH, language) + " " + ErrorMessage.getInvalidMessage(language));
				}
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("maritalStatusCat");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("maritalStatusCat = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				am_house_allocation_requestDTO.maritalStatusCat = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("employeePayScaleId");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("employeePayScaleId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				am_house_allocation_requestDTO.employeePayScaleId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("employeePayScaleTypeEn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("employeePayScaleTypeEn = " + Value);
			if(Value != null)
			{
				am_house_allocation_requestDTO.employeePayScaleTypeEn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("employeePayScaleTypeBn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("employeePayScaleTypeBn = " + Value);
			if(Value != null)
			{
				am_house_allocation_requestDTO.employeePayScaleTypeBn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("basicSalaryEn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("basicSalaryEn = " + Value);
			if(Value != null)
			{
				am_house_allocation_requestDTO.basicSalaryEn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("govtFirstSalary");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("govtFirstSalary = " + Value);
			if(Value != null)
			{
				am_house_allocation_requestDTO.govtFirstSalary = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("basicSalaryBn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("basicSalaryBn = " + Value);
			if(Value != null)
			{
				am_house_allocation_requestDTO.basicSalaryBn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("hasSpecialSalary");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("hasSpecialSalary = " + Value);
        am_house_allocation_requestDTO.hasSpecialSalary = Value != null && !Value.equalsIgnoreCase("");

			Value = request.getParameter("specialSalaryAmount");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("specialSalaryAmount = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				am_house_allocation_requestDTO.specialSalaryAmount = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("nextIncrementDate");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("nextIncrementDate = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				try
				{
					Date d = f.parse(Value);
					am_house_allocation_requestDTO.nextIncrementDate = d.getTime();
				}
				catch (Exception e)
				{
					e.printStackTrace();
					throw new Exception(LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_NEXTINCREMENTDATE, language) + " " + ErrorMessage.getInvalidMessage(language));
				}
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("salaryDescriptionUpdateFilesDropzone");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("salaryDescriptionUpdateFilesDropzone = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{

				System.out.println("salaryDescriptionUpdateFilesDropzone = " + Value);

				am_house_allocation_requestDTO.salaryDescriptionUpdateFilesDropzone = Long.parseLong(Value);


				if(addFlag == false)
				{
					String salaryDescriptionUpdateFilesDropzoneFilesToDelete = request.getParameter("salaryDescriptionUpdateFilesDropzoneFilesToDelete");
					String[] deleteArray = salaryDescriptionUpdateFilesDropzoneFilesToDelete.split(",");
					for(int i = 0; i < deleteArray.length; i ++)
					{
						System.out.println("going to delete " + deleteArray[i]);
						if(i>0)
						{
							filesDAO.delete(Long.parseLong(deleteArray[i]));
						}
					}
				}
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("amHouseOldNewCat");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("amHouseOldNewCat = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				am_house_allocation_requestDTO.amHouseOldNewCat = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("amHouseLocationCat");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("amHouseLocationCat = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				am_house_allocation_requestDTO.amHouseLocationCat = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("amHouseClassCat");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("amHouseClassCat = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				am_house_allocation_requestDTO.amHouseClassCat = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}


            Value = request.getParameter("amHouseClassSubCat");

            if(Value != null && !Value.equalsIgnoreCase(""))
            {
                Value = Jsoup.clean(Value,Whitelist.simpleText());
            }
            System.out.println("amHouseClassCat = " + Value);
            if(Value != null && !Value.equalsIgnoreCase(""))
            {
                am_house_allocation_requestDTO.amHouseClassSubCat = Integer.parseInt(Value);
            }
            else
            {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }


			Value = request.getParameter("amHouseId");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("amHouseId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				am_house_allocation_requestDTO.amHouseId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("govtFirstAppointmentDate");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("govtFirstAppointmentDate = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				try
				{
					Date d = f.parse(Value);
					am_house_allocation_requestDTO.govtFirstAppointmentDate = d.getTime();
				}
				catch (Exception e)
				{
					e.printStackTrace();
					throw new Exception(LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_GOVTFIRSTAPPOINTMENTDATE, language) + " " + ErrorMessage.getInvalidMessage(language));
				}
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("prpFirstJoiningDate");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("prpFirstJoiningDate = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				try
				{
					Date d = f.parse(Value);
					am_house_allocation_requestDTO.prpFirstJoiningDate = d.getTime();
				}
				catch (Exception e)
				{
					e.printStackTrace();
					throw new Exception(LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_PRPFIRSTJOININGDATE, language) + " " + ErrorMessage.getInvalidMessage(language));
				}
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("currentDesignationJoiningDate");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("currentDesignationJoiningDate = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				try
				{
					Date d = f.parse(Value);
					am_house_allocation_requestDTO.currentDesignationJoiningDate = d.getTime();
				}
				catch (Exception e)
				{
					e.printStackTrace();
					throw new Exception(LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_CURRENTDESIGNATIONJOININGDATE, language) + " " + ErrorMessage.getInvalidMessage(language));
				}
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("govtFirstAppointmentDesignationEn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("govtFirstAppointmentDesignationEn = " + Value);
			if(Value != null)
			{
				am_house_allocation_requestDTO.govtFirstAppointmentDesignationEn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("govtFirstAppointmentDesignationBn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("govtFirstAppointmentDesignationBn = " + Value);
			if(Value != null)
			{
				am_house_allocation_requestDTO.govtFirstAppointmentDesignationBn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("prpFirstJoiningDesignationEn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("prpFirstJoiningDesignationEn = " + Value);
			if(Value != null)
			{
				am_house_allocation_requestDTO.prpFirstJoiningDesignationEn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("prpFirstJoiningDesignationBn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("prpFirstJoiningDesignationBn = " + Value);
			if(Value != null)
			{
				am_house_allocation_requestDTO.prpFirstJoiningDesignationBn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("requestedHomeAvailablitySalaryEn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("requestedHomeAvailablitySalaryEn = " + Value);
			if(Value != null)
			{
				am_house_allocation_requestDTO.requestedHomeAvailablitySalaryEn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("requestedHomeAvailablitySalaryBn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("requestedHomeAvailablitySalaryBn = " + Value);
			if(Value != null)
			{
				am_house_allocation_requestDTO.requestedHomeAvailablitySalaryBn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("requestedHomeAvailablityDate");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("requestedHomeAvailablityDate = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				try
				{
					Date d = f.parse(Value);
					am_house_allocation_requestDTO.requestedHomeAvailablityDate = d.getTime();
				}
				catch (Exception e)
				{
					e.printStackTrace();
					throw new Exception(LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_REQUESTEDHOMEAVAILABLITYDATE, language) + " " + ErrorMessage.getInvalidMessage(language));
				}
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("requestedHomeAvailablitySalaryFilesDropzone");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("requestedHomeAvailablitySalaryFilesDropzone = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{

				System.out.println("requestedHomeAvailablitySalaryFilesDropzone = " + Value);

				am_house_allocation_requestDTO.requestedHomeAvailablitySalaryFilesDropzone = Long.parseLong(Value);


				if(addFlag == false)
				{
					String requestedHomeAvailablitySalaryFilesDropzoneFilesToDelete = request.getParameter("requestedHomeAvailablitySalaryFilesDropzoneFilesToDelete");
					String[] deleteArray = requestedHomeAvailablitySalaryFilesDropzoneFilesToDelete.split(",");
					for(int i = 0; i < deleteArray.length; i ++)
					{
						System.out.println("going to delete " + deleteArray[i]);
						if(i>0)
						{
							filesDAO.delete(Long.parseLong(deleteArray[i]));
						}
					}
				}
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("isHigherAvailability");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("isHigherAvailability = " + Value);
        am_house_allocation_requestDTO.isHigherAvailability = Value != null && !Value.equalsIgnoreCase("");

			Value = request.getParameter("higherStepNumber");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("higherStepNumber = " + Value);
			if(Value != null)
			{
				am_house_allocation_requestDTO.higherStepNumber = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("houseDivisionCat");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("houseDivisionCat = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				am_house_allocation_requestDTO.houseDivisionCat = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("currentHouseDescription");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("currentHouseDescription = " + Value);
			if(Value != null)
			{
				am_house_allocation_requestDTO.currentHouseDescription = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("houseInDhakaLocationDetails");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("houseInDhakaLocationDetails = " + Value);
			if(Value != null)
			{
				am_house_allocation_requestDTO.houseInDhakaLocationDetails = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("hasHouseInDhaka");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("hasHouseInDhaka = " + Value);
        am_house_allocation_requestDTO.hasHouseInDhaka = Value != null && !Value.equalsIgnoreCase("");

			Value = request.getParameter("houseInDhakaConstructionDate");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("houseInDhakaConstructionDate = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				try
				{
					Date d = f.parse(Value);
					am_house_allocation_requestDTO.houseInDhakaConstructionDate = d.getTime();
				}
				catch (Exception e)
				{
					e.printStackTrace();
					throw new Exception(LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_HOUSEINDHAKACONSTRUCTIONDATE, language) + " " + ErrorMessage.getInvalidMessage(language));
				}
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("hasRelativeGovtHouse");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("hasRelativeGovtHouse = " + Value);
        am_house_allocation_requestDTO.hasRelativeGovtHouse = Value != null && !Value.equalsIgnoreCase("");

			Value = request.getParameter("govtHouseRelativeName");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("govtHouseRelativeName = " + Value);
			if(Value != null)
			{
				am_house_allocation_requestDTO.govtHouseRelativeName = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("relationshipCat");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("relationshipCat = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				am_house_allocation_requestDTO.relationshipCat = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("currentHouseCat");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("currentHouseCat = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				am_house_allocation_requestDTO.currentHouseCat = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("relativeDesignation");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("relativeDesignation = " + Value);
			if(Value != null)
			{
				am_house_allocation_requestDTO.relativeDesignation = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("realtiveOffice");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("realtiveOffice = " + Value);
			if(Value != null)
			{
				am_house_allocation_requestDTO.realtiveOffice = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("relativeSalary");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("relativeSalary = " + Value);
			if(Value != null)
			{
				am_house_allocation_requestDTO.relativeSalary = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("houseNeededDescription");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("houseNeededDescription = " + Value);
			if(Value != null)
			{
				am_house_allocation_requestDTO.houseNeededDescription = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("decisionDate");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("decisionDate = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				try
				{
					Date d = f.parse(Value);
					am_house_allocation_requestDTO.decisionDate = d.getTime();
				}
				catch (Exception e)
				{
					e.printStackTrace();
					throw new Exception(LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_DECISIONDATE, language) + " " + ErrorMessage.getInvalidMessage(language));
				}
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("rejectionReason");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("rejectionReason = " + Value);
			if(Value != null)
			{
				am_house_allocation_requestDTO.rejectionReason = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("approvedDate");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("approvedDate = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				try
				{
					Date d = f.parse(Value);
					am_house_allocation_requestDTO.approvedDate = d.getTime();
				}
				catch (Exception e)
				{
					e.printStackTrace();
					throw new Exception(LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_APPROVEDDATE, language) + " " + ErrorMessage.getInvalidMessage(language));
				}
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("approverOrgId");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("approverOrgId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				am_house_allocation_requestDTO.approverOrgId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("approverOfficeId");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("approverOfficeId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				am_house_allocation_requestDTO.approverOfficeId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("approverOfficeUnitId");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("approverOfficeUnitId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				am_house_allocation_requestDTO.approverOfficeUnitId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("approverEmpId");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("approverEmpId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				am_house_allocation_requestDTO.approverEmpId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("approverPhoneNum");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("approverPhoneNum = " + Value);
			if(Value != null)
			{
				am_house_allocation_requestDTO.approverPhoneNum = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("approverNameEn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("approverNameEn = " + Value);
			if(Value != null)
			{
				am_house_allocation_requestDTO.approverNameEn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("approverNameBn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("approverNameBn = " + Value);
			if(Value != null)
			{
				am_house_allocation_requestDTO.approverNameBn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("approverOfficeNameEn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("approverOfficeNameEn = " + Value);
			if(Value != null)
			{
				am_house_allocation_requestDTO.approverOfficeNameEn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("approverOfficeNameBn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("approverOfficeNameBn = " + Value);
			if(Value != null)
			{
				am_house_allocation_requestDTO.approverOfficeNameBn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("approverOfficeUnitNameEn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("approverOfficeUnitNameEn = " + Value);
			if(Value != null)
			{
				am_house_allocation_requestDTO.approverOfficeUnitNameEn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("approverOfficeUnitNameBn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("approverOfficeUnitNameBn = " + Value);
			if(Value != null)
			{
				am_house_allocation_requestDTO.approverOfficeUnitNameBn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("approverOfficeUnitOrgNameEn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("approverOfficeUnitOrgNameEn = " + Value);
			if(Value != null)
			{
				am_house_allocation_requestDTO.approverOfficeUnitOrgNameEn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("approverOfficeUnitOrgNameBn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("approverOfficeUnitOrgNameBn = " + Value);
			if(Value != null)
			{
				am_house_allocation_requestDTO.approverOfficeUnitOrgNameBn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}



			Value = request.getParameter("requesterEmpId");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("requesterEmpId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				EmployeeSearchModel[] models = gson.fromJson(Value, EmployeeSearchModel[].class);

				for(EmployeeSearchModel model: models){

					am_house_allocation_requestDTO.requesterEmpId = model.employeeRecordId;
					am_house_allocation_requestDTO.requesterOfficeUnitId = model.officeUnitId;
					am_house_allocation_requestDTO.requesterOrgId = model.organogramId;

					am_house_allocation_requestDTO.requesterNameEn = model.employeeNameEn;
					am_house_allocation_requestDTO.requesterNameBn = model.employeeNameBn;
					am_house_allocation_requestDTO.requesterOfficeUnitNameEn = model.officeUnitNameEn;
					am_house_allocation_requestDTO.requesterOfficeUnitNameBn = model.officeUnitNameBn;
					am_house_allocation_requestDTO.requesterOfficeUnitOrgNameEn = model.organogramNameEn;
					am_house_allocation_requestDTO.requesterOfficeUnitOrgNameBn = model.organogramNameBn;

					am_house_allocation_requestDTO.requesterPhoneNum = model.phoneNumber;

					OfficesDTO requesterOffice = OfficesRepository.getInstance().getOfficesDTOByID(am_house_allocation_requestDTO.requesterOfficeId);

					if (requesterOffice != null) {
						am_house_allocation_requestDTO.requesterOfficeId = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(model.officeUnitId).officeId;

						am_house_allocation_requestDTO.requesterOfficeNameEn = requesterOffice.officeNameEng;
						am_house_allocation_requestDTO.requesterOfficeNameBn = requesterOffice.officeNameBng;
					}

				}
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			// FIRST LAYER APPROVER DATA
			Gson gson = new Gson();
			OfficeUnitOrganograms lineManager = OfficeUnitOrganogramsRepository.getInstance().
				getImmediateSuperiorToOrganogramIdWithActiveEmpStatusAndConsiderSubstitute(am_house_allocation_requestDTO.requesterOrgId);
			EmployeeSearchModel firstApprover =
					gson.fromJson(
							EmployeeSearchModalUtil.getEmployeeSearchModelJson(lineManager.assignedEmployeeRecordId,
									lineManager.office_unit_id, lineManager.id),
							EmployeeSearchModel.class);
			am_house_allocation_requestDTO.firstLayerApproverEmpId = firstApprover.employeeRecordId;
			am_house_allocation_requestDTO.firstLayerApproverOfficeUnitId = firstApprover.officeUnitId;
			am_house_allocation_requestDTO.firstLayerApproverOrgId = firstApprover.organogramId;

			am_house_allocation_requestDTO.firstLayerApproverNameEn = firstApprover.employeeNameEn;
			am_house_allocation_requestDTO.firstLayerApproverNameBn = firstApprover.employeeNameBn;
			am_house_allocation_requestDTO.firstLayerApproverOfficeUnitNameEn = firstApprover.officeUnitNameEn;
			am_house_allocation_requestDTO.firstLayerApproverOfficeUnitNameBn = firstApprover.officeUnitNameBn;
			am_house_allocation_requestDTO.firstLayerApproverOfficeUnitOrgNameEn = firstApprover.organogramNameEn;
			am_house_allocation_requestDTO.firstLayerApproverOfficeUnitOrgNameBn = firstApprover.organogramNameBn;

			am_house_allocation_requestDTO.firstLayerApproverPhoneNumber = firstApprover.phoneNumber;

			OfficesDTO decisionByOffice = OfficesRepository.getInstance().getOfficesDTOByID(am_house_allocation_requestDTO.firstLayerApproverOfficeId);

			if (decisionByOffice != null) {
				am_house_allocation_requestDTO.firstLayerApproverOfficeId = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(firstApprover.officeUnitId).officeId;

				am_house_allocation_requestDTO.firstLayerApproverOfficeNameEn = decisionByOffice.officeNameEng;
				am_house_allocation_requestDTO.firstLayerApproverOfficeNameBn = decisionByOffice.officeNameBng;
			}


			// CHECK FOR DUPLICATE HOUSE ALLOCATION REQUEST
			/*List<Am_house_allocation_requestDTO> list = Am_house_allocation_requestRepository.getInstance()
					.getAm_house_allocation_requestList()
					.stream().filter(dto -> dto.requesterEmpId == am_house_allocation_requestDTO.requesterEmpId)
					.filter(dto -> dto.status == CommonApprovalStatus.PENDING.getValue() ||
							dto.status == CommonApprovalStatus.REQUISITION_APPROVED_BY_FIRST_APPROVER.getValue())
					.collect(Collectors.toList());
			if(addFlag && list.size()>0){
				UtilCharacter.throwException("আপনি ইতিমধ্যে আবেদন করেছেন", "You have already applied");
			}*/


			Value = request.getParameter("searchColumn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("searchColumn = " + Value);
			if(Value != null)
			{
				am_house_allocation_requestDTO.searchColumn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			if(addFlag)
			{
				am_house_allocation_requestDTO.insertedByUserId = userDTO.ID;
			}


			if(addFlag)
			{
				am_house_allocation_requestDTO.insertedByOrganogramId = userDTO.organogramID;
			}


			if(addFlag)
			{
				am_house_allocation_requestDTO.insertionDate = TimeConverter.getToday();
			}


			am_house_allocation_requestDTO.lastModifierUser = userDTO.userName;


			System.out.println("Done adding  addAm_house_allocation_request dto = " + am_house_allocation_requestDTO);



				if(addFlag == true)
				{
//					List<Am_house_allocation_requestDTO> existing = Am_house_allocation_requestDAO.getInstance()
//							.getExistingRequestByOrgId(am_house_allocation_requestDTO.requesterOrgId);
//					if (!existing.isEmpty()) {
//						throw new Exception(language.equals("English") ? "Another request exists" : "অন্য রিকোয়েস্ট বিদ্যমান");
//					}
//					else {
//
//					}


					am_house_allocation_requestDTO.status = AmHouseAllocationRequestStatus.WAITING_FOR_APPROVAL.getValue();

					Utils.handleTransaction(()->{
						boolean isWaitingForApproval = true;
						Am_house_allocation_requestDAO.getInstance().add(am_house_allocation_requestDTO);



						AmHouseAllocationApprovalModel model =
								new AmHouseAllocationApprovalModel.AmHouseAllocationApprovalModelBuilder()
										.setAmHouseAllocationRequestDTO(am_house_allocation_requestDTO)
										.setRequesterEmployeeRecordId(userDTO.employee_record_id)
										.setTaskTypeId(TaskTypeEnum.AM_HOUSE_ASSIGNMENT.getValue())
										.build();

						// INSERTING ROW IN 'am_house_allocation_approval_mapping' FOR ESTATE LEVEL APPROVAL
						CardApprovalResponse cardApprovalResponse = Am_house_allocation_approval_mappingDAO.getInstance()
								.createAmOfficeApproval(model,true);

						AmHouseAllocationNotification.getInstance().sendWaitingForFirstApprovalNotification(
						Collections.singletonList(am_house_allocation_requestDTO.firstLayerApproverOrgId), am_house_allocation_requestDTO
						);

						Am_reliable_family_memberDAO.getInstance().bulkAddOrUpdate(request, userDTO, am_house_allocation_requestDTO.requesterEmpId);

					});


				}
				else
				{
					if (am_house_allocation_requestDTO.status != AmHouseAllocationRequestStatus.WAITING_FOR_APPROVAL.getValue()) {
						throw new Exception("This is not pending");
					}
					else {

						Utils.handleTransaction(()->{
							Am_house_allocation_requestDAO.getInstance().update(am_house_allocation_requestDTO);

							Am_reliable_family_memberDAO.getInstance().bulkAddOrUpdate(request, userDTO, am_house_allocation_requestDTO.requesterEmpId);
						});
					}
				}

			return am_house_allocation_requestDTO;



	}

	private void getMultiPageView(HttpServletRequest request, HttpServletResponse response) throws Exception {
		String actionType = request.getParameter("actionType");
		String tabStr = request.getParameter("tab");
		tabStr = tabStr == null ? "1" : tabStr;
		request.setAttribute("tab", Integer.parseInt(tabStr));
//		if (actionType.equals("viewMultiForm") && Integer.parseInt(tabStr) > 1) {
//			String id = request.getParameter("ID");
//			if (id == null) {
//				Utils.addErrorMessage(request, "Please, Fillup personal Information form first");
//				response.sendRedirect(request.getContextPath() + "/Employee_recordsServlet?actionType=addMultiForm&tab=1");
//				return;
//			}
//		}
		switch (tabStr) {
			case "1":
				//setTab1Data(request);
				break;
			case "2":
				//setTab2Data(request);
				break;
			case "3":
				//setTab3Data(request);
				break;
			case "4":
				//setTab4Data(request);
				break;
			case "5":
				//setTab5Data(request);
				break;
			case "6":
				//setTab6Data(request);
				break;
		}
//		RequestDispatcher rd = request.getRequestDispatcher("am_house_allocation_request/multiform/tab" + tabStr + "/tab" + tabStr + ".jsp");
		RequestDispatcher rd = request.getRequestDispatcher("am_house_allocation_request/multiform/full.jsp");
		rd.forward(request, response);
	}

	private void getRequesterDetails(HttpServletRequest request, HttpServletResponse response) throws Exception {
		LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
		String Language = LM.getText(LC.APPLICANT_PROFILE_EDIT_LANGUAGE, loginDTO);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");

		long id = -1;
		String idString = request.getParameter("id");
		if (StringUtils.isValidString(idString)) {
			id = Long.parseLong(idString);

			Am_house_allocation_requestDTO am_house_allocation_requestDTO = new Am_house_allocation_requestDTO();

			Employee_recordsDTO employee_recordsDTO = Employee_recordsRepository.getInstance().getById(id);
			List<EmployeeOfficeDTO> employee_officesDTOS = EmployeeOfficeRepository.getInstance().getByEmployeeRecordId(id);
			EmployeeOfficeDTO currentEmployeeOffice = employee_officesDTOS.get(0);
			EmployeeOfficeDTO firstEmployeeOffice = EmployeeOfficesDAO.getInstance().getByEmployeeRecordIdsWithMinJoiningDate(Arrays.asList(id)).get(0);
			List<Employee_service_historyDTO> employee_service_historyDTOS = Employee_service_historyDAO.getInstance().getDTOSGovJobByEmployeeIds(Arrays.asList(id));

			am_house_allocation_requestDTO.dateOfBirthString = Utils.getDigits(f.format(new Date(employee_recordsDTO.dateOfBirth)), Language);
			am_house_allocation_requestDTO.maritalStatusCat = employee_recordsDTO.maritalStatus;
			am_house_allocation_requestDTO.maritalStatusCatString = CatRepository.getInstance().getText(Language, "marital_status", am_house_allocation_requestDTO.maritalStatusCat);

			if (!employee_service_historyDTOS.isEmpty()) {
				Employee_service_historyDTO serviceHistoryDTO = employee_service_historyDTOS.get(0);
				am_house_allocation_requestDTO.govtFirstAppointmentDateString = Utils.getDigits(f.format(new Date(serviceHistoryDTO.servingFrom)), Language);
			}

			am_house_allocation_requestDTO.prpFirstJoiningDateString = Utils.getDigits(f.format(new Date(firstEmployeeOffice.joiningDate)), Language);
			am_house_allocation_requestDTO.currentDesignationJoiningDateString = Utils.getDigits(f.format(new Date(currentEmployeeOffice.joiningDate)), Language);
			am_house_allocation_requestDTO.prpFirstJoiningDesignationEn = firstEmployeeOffice.designationEn;
			am_house_allocation_requestDTO.prpFirstJoiningDesignationBn = firstEmployeeOffice.designationBn;

			int gradeId = Employee_recordsRepository.getInstance().getGradeByEmployeeRecordId(id);
			CategoryDTO gradeCat = gradeId != 0 ? CatRepository.getInstance().getCategoryDTOListByValue("job_grade_type", gradeId) : null;
			am_house_allocation_requestDTO.employeePayScaleId = gradeId;
			am_house_allocation_requestDTO.employeePayScaleTypeEn = gradeId != 0 ? (Language.equals("English") ? gradeCat.nameEn : gradeCat.nameBn) : "";
			am_house_allocation_requestDTO.employeePayScaleTypeBn = gradeId != 0 ? gradeCat.nameBn : "";
			am_house_allocation_requestDTO.higherEligibilityMsg = gradeId != 0 ? "*" + Am_grade_house_mappingRepository.getInstance().getMsg(gradeId, Language) : "";

			int basicSalaryEn = Grade_wise_pay_scaleRepository.getInstance().getPayScale(am_house_allocation_requestDTO.employeePayScaleId);
			if(basicSalaryEn != -1) am_house_allocation_requestDTO.basicSalaryEn = String.valueOf(basicSalaryEn);

			if (Language.equals("English")) {
				am_house_allocation_requestDTO.requesterOfficeUnitOrgNameEn = currentEmployeeOffice.designationEn;
				am_house_allocation_requestDTO.requesterOfficeUnitNameEn = currentEmployeeOffice.unitNameEn;
			}
			else {
				am_house_allocation_requestDTO.requesterOfficeUnitOrgNameEn = currentEmployeeOffice.designationBn;
				am_house_allocation_requestDTO.requesterOfficeUnitNameEn = currentEmployeeOffice.unitNameBn;
			}
			am_house_allocation_requestDTO.remarks = getReliableFamilyMemberTableBody(id, Language);
			String encoded = this.gson.toJson(am_house_allocation_requestDTO);
			out.print(encoded);
		}
		else {
			String tempString = "No id found";

			String encoded = this.gson.toJson(tempString);
			out.print(encoded);
		}
		out.flush();
	}

	public String getReliableFamilyMemberTableBody(long requesterEmpId, String Language) {

		List<Employee_family_infoDTO> employee_family_infoDTOS = Employee_family_infoDAO.getInstance().getByEmployeeId(requesterEmpId);
		List<Am_reliable_family_memberDTO> am_reliable_family_memberDTOS = Am_reliable_family_memberDAO.getInstance().getByEmployeeRecordId(requesterEmpId);

		HashMap<Long, Am_reliable_family_memberDTO> familyInfoIdToReliableMap = new HashMap<>();

		am_reliable_family_memberDTOS
				.forEach(am_reliable_family_memberDTO -> {
					familyInfoIdToReliableMap.put(am_reliable_family_memberDTO.employeeFamilyInfoId, am_reliable_family_memberDTO);
				});


		String value = "";
		String tempString = "";

		ArrayList data = (ArrayList<Employee_family_infoDTO>) employee_family_infoDTOS;

		if (data != null) {
			int size = data.size();
			System.out.println("data not null and size = " + size + " data = " + data);
			for (int iReliable = 0; iReliable < size; iReliable++) {
				Employee_family_infoDTO employee_family_infoDTO = (Employee_family_infoDTO) data.get(iReliable);

				tempString += "        <tr>\n" +
						"\n" +
						"\n" +
						"            <td>";

				if (Language.equals("Bangla")) {
					value = employee_family_infoDTO.firstNameBn;
				}
				else {
					value = employee_family_infoDTO.firstNameEn;
				}

				tempString += Utils.getDigits(value, Language);

				tempString += "<input type='hidden' class='form-control'  name='familyMemberId' id = 'familyMemberId_hidden_" + iReliable + "'\n" +
						"                       value='" + employee_family_infoDTO.iD + "' tag='pb_html'/>\n" +
						"\n" +
						"                \n" +
						"                <input type='hidden' class='form-control'  name='isReliableValues' id = 'isReliableValues_hidden_" + iReliable + "'\n" +
						"                       tag='pb_html'/>";

				tempString += "</td>\n" +
						"\n" +
						"            <td>";

				value = Utils.calculateCompleteAgeInFullFormat(employee_family_infoDTO.dob, Language) + "";

				tempString += Utils.getDigits(value, Language);

				tempString += "</td>\n" +
						"\n" +
						"            <td>";

				value = RelationRepository.getInstance().getText(Language, employee_family_infoDTO.relationType) + "";

				tempString += Utils.getDigits(value, Language);

				tempString += "</td>\n" +
						"\n" +
						"            <td>";

				value = familyInfoIdToReliableMap.getOrDefault(employee_family_infoDTO.iD, new Am_reliable_family_memberDTO()).remarks + "";

				tempString += "<textarea class='form-control' name='reliableFamilyMemberRemarks'\n" +
						"                          id='reliableFamilyMemberRemarks_text_" + iReliable + "'>" + Utils.getDigits(value, Language) + "</textarea>\n" +
						"\n" +
						"\n" +
						"            </td>";

				String checked = familyInfoIdToReliableMap.containsKey(employee_family_infoDTO.iD) && familyInfoIdToReliableMap.get(employee_family_infoDTO.iD).isReliable == 1?"checked":"";

				tempString += " <td class=\"text-right\">\n" +
						"                <div class='checker'>\n" +
						"                    <span class='chkEdit' ><input type='checkbox' " + checked + "\n" +
						"                                                  name='isReliable'/></span>\n" +
						"                </div>\n" +
						"            </td>\n" +
						"\n" +
						"        </tr>";

			}
		}

		return tempString;
	}
}

