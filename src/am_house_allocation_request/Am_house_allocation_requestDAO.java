package am_house_allocation_request;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.sql.SQLException;
import java.util.stream.Collectors;

import am_dashboard.CountDTO;
import common.CommonDAOService;
import common.ConnectionAndStatementUtil;
import org.apache.log4j.Logger;
import util.*;
import pb.*;
import vm_requisition.CommonApprovalStatus;

public class Am_house_allocation_requestDAO  implements CommonDAOService<Am_house_allocation_requestDTO>
{
	
	Logger logger = Logger.getLogger(getClass());
	
	private static String addQuery = "";
	private static String updateQuery = "";
	private String[] columnNames = null;

	private Am_house_allocation_requestDAO()
	{
		columnNames = new String[] 
		{
			"ID",
			"requester_org_id",
			"requester_office_id",
			"requester_office_unit_id",
			"requester_emp_id",
			"requester_phone_num",
			"requester_name_en",
			"requester_name_bn",
			"requester_office_name_en",
			"requester_office_name_bn",
			"requester_office_unit_name_en",
			"requester_office_unit_name_bn",
			"requester_office_unit_org_name_en",
			"requester_office_unit_org_name_bn",
			"date_of_birth",
			"marital_status_cat",
			"employee_pay_scale_id",
			"govt_first_salary",
			"house_in_dhaka_location_details",
			"employee_pay_scale_type_en",
			"employee_pay_scale_type_bn",
			"basic_salary_en",
			"basic_salary_bn",
			"has_special_salary",
			"special_salary_amount",
			"next_increment_date",
			"salary_description_update_files_dropzone",
			"current_house_cat",
			"am_house_old_new_cat",
			"am_house_location_cat",
			"am_house_class_cat",
			"am_house_class_sub_cat",
			"am_house_id",
			"govt_first_appointment_date",
			"prp_first_joining_date",
			"current_designation_joining_date",
			"govt_first_appointment_designation_en",
			"govt_first_appointment_designation_bn",
			"prp_first_joining_designation_en",
			"prp_first_joining_designation_bn",
			"requested_home_availablity_salary_en",
			"requested_home_availablity_salary_bn",
			"requested_home_availablity_date",
			"requested_home_availablity_salary_files_dropzone",
			"is_higher_availability",
			"higher_step_number",
			"house_division_cat",
			"current_house_description",
			"has_house_in_dhaka",
			"house_in_dhaka_construction_date",
			"has_relative_govt_house",
			"govt_house_relative_name",
			"relationship_cat",
			"relative_designation",
			"realtive_office",
			"relative_salary",
			"house_needed_description",
			"status",
			"first_layer_approval_status",
			"first_layer_approval_or_rejection_date",
			"first_layer_approver_org_id",
			"first_layer_approver_office_id",
			"first_layer_approver_office_unit_id",
			"first_layer_approver_emp_id",
			"first_layer_approver_phone_num",
			"first_layer_approver_name_en",
			"first_layer_approver_name_bn",
			"first_layer_approver_office_name_en",
			"first_layer_approver_office_name_bn",
			"first_layer_approver_office_unit_name_en",
			"first_layer_approver_office_unit_name_bn",
			"first_layer_approver_office_unit_org_name_en",
			"first_layer_approver_office_unit_org_name_bn",
			"first_layer_approver_rejection_reason",
			"decision_date",
			"rejection_reason",
			"approved_date",
			"approver_org_id",
			"approver_office_id",
			"approver_office_unit_id",
			"approver_emp_id",
			"approver_phone_num",
			"approver_name_en",
			"approver_name_bn",
			"approver_office_name_en",
			"approver_office_name_bn",
			"approver_office_unit_name_en",
			"approver_office_unit_name_bn",
			"approver_office_unit_org_name_en",
			"approver_office_unit_org_name_bn",
			"search_column",
			"inserted_by_user_id",
			"inserted_by_organogram_id",
			"insertion_date",
			"last_modifier_user",
			"withdrawer_org_id",
			"withdrawer_office_id",
			"withdrawer_office_unit_id",
			"withdrawer_emp_id",
			"withdrawer_phone_num",
			"withdrawer_name_en",
			"withdrawer_name_bn",
			"withdrawer_office_name_en",
			"withdrawer_office_name_bn",
			"withdrawer_office_unit_name_en",
			"withdrawer_office_unit_name_bn",
			"withdrawer_office_unit_org_name_en",
			"withdrawer_office_unit_org_name_bn",
			"withdrawal_date",
			"withdrawal_reason",
			"isDeleted",
			"lastModificationTime"
		};
		updateQuery = getUpdateQuery(columnNames);
        addQuery = getAddQuery(columnNames);

		searchMap.put("requester_phone_num"," and (requester_phone_num like ?)");
		searchMap.put("employeeRecordsId"," and (requester_emp_id = ?)");
		searchMap.put("requester_name_en"," and (requester_name_en like ?)");
		searchMap.put("requester_name_bn"," and (requester_name_bn like ?)");
		searchMap.put("requester_office_name_en"," and (requester_office_name_en like ?)");
		searchMap.put("requester_office_name_bn"," and (requester_office_name_bn like ?)");
		searchMap.put("requester_office_unit_name_en"," and (requester_office_unit_name_en like ?)");
		searchMap.put("requester_office_unit_name_bn"," and (requester_office_unit_name_bn like ?)");
		searchMap.put("requester_office_unit_org_name_en"," and (requester_office_unit_org_name_en like ?)");
		searchMap.put("requester_office_unit_org_name_bn"," and (requester_office_unit_org_name_bn like ?)");
		searchMap.put("date_of_birth_start"," and (date_of_birth >= ?)");
		searchMap.put("date_of_birth_end"," and (date_of_birth <= ?)");
		searchMap.put("marital_status_cat"," and (marital_status_cat = ?)");
		searchMap.put("status"," and (status = ?)");
		searchMap.put("govt_first_salary"," and (govt_first_salary like ?)");
		searchMap.put("house_in_dhaka_location_details"," and (house_in_dhaka_location_details like ?)");
		searchMap.put("employee_pay_scale_type_en"," and (employee_pay_scale_type_en like ?)");
		searchMap.put("employee_pay_scale_type_bn"," and (employee_pay_scale_type_bn like ?)");
		searchMap.put("basic_salary_en"," and (basic_salary_en like ?)");
		searchMap.put("basic_salary_bn"," and (basic_salary_bn like ?)");
		searchMap.put("special_salary_amount"," and (special_salary_amount like ?)");
		searchMap.put("next_increment_date_start"," and (next_increment_date >= ?)");
		searchMap.put("next_increment_date_end"," and (next_increment_date <= ?)");
		searchMap.put("current_house_cat"," and (current_house_cat = ?)");
		searchMap.put("am_house_old_new_cat"," and (am_house_old_new_cat = ?)");
		searchMap.put("am_house_location_cat"," and (am_house_location_cat = ?)");
		searchMap.put("am_house_class_cat"," and (am_house_class_cat = ?)");
		searchMap.put("am_house_class_sub_cat"," and (am_house_class_sub_cat = ?)");
		searchMap.put("govt_first_appointment_date_start"," and (govt_first_appointment_date >= ?)");
		searchMap.put("govt_first_appointment_date_end"," and (govt_first_appointment_date <= ?)");
		searchMap.put("prp_first_joining_date_start"," and (prp_first_joining_date >= ?)");
		searchMap.put("prp_first_joining_date_end"," and (prp_first_joining_date <= ?)");
		searchMap.put("current_designation_joining_date_start"," and (current_designation_joining_date >= ?)");
		searchMap.put("current_designation_joining_date_end"," and (current_designation_joining_date <= ?)");
		searchMap.put("govt_first_appointment_designation_en"," and (govt_first_appointment_designation_en like ?)");
		searchMap.put("govt_first_appointment_designation_bn"," and (govt_first_appointment_designation_bn like ?)");
		searchMap.put("prp_first_joining_designation_en"," and (prp_first_joining_designation_en like ?)");
		searchMap.put("prp_first_joining_designation_bn"," and (prp_first_joining_designation_bn like ?)");
		searchMap.put("requested_home_availablity_salary_en"," and (requested_home_availablity_salary_en like ?)");
		searchMap.put("requested_home_availablity_salary_bn"," and (requested_home_availablity_salary_bn like ?)");
		searchMap.put("requested_home_availablity_date_start"," and (requested_home_availablity_date >= ?)");
		searchMap.put("requested_home_availablity_date_end"," and (requested_home_availablity_date <= ?)");
		searchMap.put("higher_step_number"," and (higher_step_number like ?)");
		searchMap.put("house_division_cat"," and (house_division_cat = ?)");
		searchMap.put("current_house_description"," and (current_house_description like ?)");
		searchMap.put("house_in_dhaka_construction_date_start"," and (house_in_dhaka_construction_date >= ?)");
		searchMap.put("house_in_dhaka_construction_date_end"," and (house_in_dhaka_construction_date <= ?)");
		searchMap.put("govt_house_relative_name"," and (govt_house_relative_name like ?)");
		searchMap.put("relationship_cat"," and (relationship_cat = ?)");
		searchMap.put("relative_designation"," and (relative_designation like ?)");
		searchMap.put("realtive_office"," and (realtive_office like ?)");
		searchMap.put("relative_salary"," and (relative_salary like ?)");
		searchMap.put("house_needed_description"," and (house_needed_description like ?)");
		searchMap.put("am_house_allocation_status_cat"," and (status = ?)");
		searchMap.put("first_layer_approver_emp_id"," and (first_layer_approver_emp_id = ?)");
		searchMap.put("first_layer_approval_date_start"," and (first_layer_approval_or_rejection_date >= ?)");
		searchMap.put("first_layer_approval_date_end"," and (first_layer_approval_or_rejection_date <= ?)");
		searchMap.put("first_layer_approver_phone_num"," and (first_layer_approver_phone_num like ?)");
		searchMap.put("first_layer_approver_name_en"," and (first_layer_approver_name_en like ?)");
		searchMap.put("first_layer_approver_name_bn"," and (first_layer_approver_name_bn like ?)");
		searchMap.put("first_layer_approver_office_name_en"," and (first_layer_approver_office_name_en like ?)");
		searchMap.put("first_layer_approver_office_name_bn"," and (first_layer_approver_office_name_bn like ?)");
		searchMap.put("first_layer_approver_office_unit_name_en"," and (first_layer_approver_office_unit_name_en like ?)");
		searchMap.put("first_layer_approver_office_unit_name_bn"," and (first_layer_approver_office_unit_name_bn like ?)");
		searchMap.put("first_layer_approver_office_unit_org_name_en"," and (first_layer_approver_office_unit_org_name_en like ?)");
		searchMap.put("first_layer_approver_office_unit_org_name_bn"," and (first_layer_approver_office_unit_org_name_bn like ?)");
		searchMap.put("decision_date_start"," and (decision_date >= ?)");
		searchMap.put("decision_date_end"," and (decision_date <= ?)");
		searchMap.put("rejection_reason"," and (rejection_reason like ?)");
		searchMap.put("approved_date_start"," and (approved_date >= ?)");
		searchMap.put("approved_date_end"," and (approved_date <= ?)");
		searchMap.put("approver_phone_num"," and (approver_phone_num like ?)");
		searchMap.put("approver_name_en"," and (approver_name_en like ?)");
		searchMap.put("approver_name_bn"," and (approver_name_bn like ?)");
		searchMap.put("approver_office_name_en"," and (approver_office_name_en like ?)");
		searchMap.put("approver_office_name_bn"," and (approver_office_name_bn like ?)");
		searchMap.put("approver_office_unit_name_en"," and (approver_office_unit_name_en like ?)");
		searchMap.put("approver_office_unit_name_bn"," and (approver_office_unit_name_bn like ?)");
		searchMap.put("approver_office_unit_org_name_en"," and (approver_office_unit_org_name_en like ?)");
		searchMap.put("approver_office_unit_org_name_bn"," and (approver_office_unit_org_name_bn like ?)");
		searchMap.put("insertion_date_start"," and (insertion_date >= ?)");
		searchMap.put("insertion_date_end"," and (insertion_date <= ?)");
		searchMap.put("inserted_by_organogram_id"," and (inserted_by_organogram_id = ?)");
		searchMap.put("AnyField"," and (search_column like ?)");
	}

	private final Map<String,String> searchMap = new HashMap<>();

    private static class LazyLoader
    {
    	static final Am_house_allocation_requestDAO INSTANCE = new Am_house_allocation_requestDAO();
	}

	public static Am_house_allocation_requestDAO getInstance()
	{
    	return LazyLoader.INSTANCE;
	}
	
	public void setSearchColumn(Am_house_allocation_requestDTO am_house_allocation_requestDTO)
	{
		am_house_allocation_requestDTO.searchColumn = "";
		am_house_allocation_requestDTO.searchColumn += am_house_allocation_requestDTO.requesterPhoneNum + " ";
		am_house_allocation_requestDTO.searchColumn += am_house_allocation_requestDTO.requesterNameEn + " ";
		am_house_allocation_requestDTO.searchColumn += am_house_allocation_requestDTO.requesterNameBn + " ";
		am_house_allocation_requestDTO.searchColumn += am_house_allocation_requestDTO.requesterOfficeNameEn + " ";
		am_house_allocation_requestDTO.searchColumn += am_house_allocation_requestDTO.requesterOfficeNameBn + " ";
		am_house_allocation_requestDTO.searchColumn += am_house_allocation_requestDTO.requesterOfficeUnitNameEn + " ";
		am_house_allocation_requestDTO.searchColumn += am_house_allocation_requestDTO.requesterOfficeUnitNameBn + " ";
		am_house_allocation_requestDTO.searchColumn += am_house_allocation_requestDTO.requesterOfficeUnitOrgNameEn + " ";
		am_house_allocation_requestDTO.searchColumn += am_house_allocation_requestDTO.requesterOfficeUnitOrgNameBn + " ";
		am_house_allocation_requestDTO.searchColumn += CatDAO.getName("English", "marital_status", am_house_allocation_requestDTO.maritalStatusCat) + " " + CatDAO.getName("Bangla", "marital_status", am_house_allocation_requestDTO.maritalStatusCat) + " ";
		am_house_allocation_requestDTO.searchColumn += am_house_allocation_requestDTO.govtFirstSalary + " ";
		am_house_allocation_requestDTO.searchColumn += am_house_allocation_requestDTO.houseInDhakaLocationDetails + " ";
		am_house_allocation_requestDTO.searchColumn += am_house_allocation_requestDTO.employeePayScaleTypeEn + " ";
		am_house_allocation_requestDTO.searchColumn += am_house_allocation_requestDTO.employeePayScaleTypeBn + " ";
		am_house_allocation_requestDTO.searchColumn += am_house_allocation_requestDTO.basicSalaryEn + " ";
		am_house_allocation_requestDTO.searchColumn += am_house_allocation_requestDTO.basicSalaryBn + " ";
		am_house_allocation_requestDTO.searchColumn += am_house_allocation_requestDTO.specialSalaryAmount + " ";
		am_house_allocation_requestDTO.searchColumn += CatDAO.getName("English", "am_current_house", am_house_allocation_requestDTO.currentHouseCat) + " " + CatDAO.getName("Bangla", "am_current_house", am_house_allocation_requestDTO.currentHouseCat) + " ";
		am_house_allocation_requestDTO.searchColumn += CatDAO.getName("English", "am_house_old_new", am_house_allocation_requestDTO.amHouseOldNewCat) + " " + CatDAO.getName("Bangla", "am_house_old_new", am_house_allocation_requestDTO.amHouseOldNewCat) + " ";
		am_house_allocation_requestDTO.searchColumn += CatDAO.getName("English", "am_house_location", am_house_allocation_requestDTO.amHouseLocationCat) + " " + CatDAO.getName("Bangla", "am_house_location", am_house_allocation_requestDTO.amHouseLocationCat) + " ";
		am_house_allocation_requestDTO.searchColumn += CatDAO.getName("English", "am_house_class", am_house_allocation_requestDTO.amHouseClassCat) + " " + CatDAO.getName("Bangla", "am_house_class", am_house_allocation_requestDTO.amHouseClassCat) + " ";
		am_house_allocation_requestDTO.searchColumn += am_house_allocation_requestDTO.govtFirstAppointmentDesignationEn + " ";
		am_house_allocation_requestDTO.searchColumn += am_house_allocation_requestDTO.govtFirstAppointmentDesignationBn + " ";
		am_house_allocation_requestDTO.searchColumn += am_house_allocation_requestDTO.prpFirstJoiningDesignationEn + " ";
		am_house_allocation_requestDTO.searchColumn += am_house_allocation_requestDTO.prpFirstJoiningDesignationBn + " ";
		am_house_allocation_requestDTO.searchColumn += am_house_allocation_requestDTO.requestedHomeAvailablitySalaryEn + " ";
		am_house_allocation_requestDTO.searchColumn += am_house_allocation_requestDTO.requestedHomeAvailablitySalaryBn + " ";
		am_house_allocation_requestDTO.searchColumn += am_house_allocation_requestDTO.higherStepNumber + " ";
		am_house_allocation_requestDTO.searchColumn += CatDAO.getName("English", "house_division", am_house_allocation_requestDTO.houseDivisionCat) + " " + CatDAO.getName("Bangla", "house_division", am_house_allocation_requestDTO.houseDivisionCat) + " ";
		am_house_allocation_requestDTO.searchColumn += am_house_allocation_requestDTO.currentHouseDescription + " ";
		am_house_allocation_requestDTO.searchColumn += am_house_allocation_requestDTO.govtHouseRelativeName + " ";
		am_house_allocation_requestDTO.searchColumn += CatDAO.getName("English", "am_relationship", am_house_allocation_requestDTO.relationshipCat) + " " + CatDAO.getName("Bangla", "am_relationship", am_house_allocation_requestDTO.relationshipCat) + " ";
		am_house_allocation_requestDTO.searchColumn += am_house_allocation_requestDTO.relativeDesignation + " ";
		am_house_allocation_requestDTO.searchColumn += am_house_allocation_requestDTO.realtiveOffice + " ";
		am_house_allocation_requestDTO.searchColumn += am_house_allocation_requestDTO.relativeSalary + " ";
		am_house_allocation_requestDTO.searchColumn += am_house_allocation_requestDTO.houseNeededDescription + " ";
		am_house_allocation_requestDTO.searchColumn += am_house_allocation_requestDTO.firstLayerApproverRejectionReason + " ";
		am_house_allocation_requestDTO.searchColumn += am_house_allocation_requestDTO.firstLayerApproverPhoneNumber + " ";
		am_house_allocation_requestDTO.searchColumn += am_house_allocation_requestDTO.firstLayerApproverNameEn + " ";
		am_house_allocation_requestDTO.searchColumn += am_house_allocation_requestDTO.firstLayerApproverNameBn + " ";
		am_house_allocation_requestDTO.searchColumn += am_house_allocation_requestDTO.firstLayerApproverOfficeNameEn + " ";
		am_house_allocation_requestDTO.searchColumn += am_house_allocation_requestDTO.firstLayerApproverOfficeNameBn + " ";
		am_house_allocation_requestDTO.searchColumn += am_house_allocation_requestDTO.firstLayerApproverOfficeUnitNameEn + " ";
		am_house_allocation_requestDTO.searchColumn += am_house_allocation_requestDTO.firstLayerApproverOfficeUnitNameBn + " ";
		am_house_allocation_requestDTO.searchColumn += am_house_allocation_requestDTO.firstLayerApproverOfficeUnitOrgNameEn + " ";
		am_house_allocation_requestDTO.searchColumn += am_house_allocation_requestDTO.firstLayerApproverOfficeUnitOrgNameBn + " ";
		am_house_allocation_requestDTO.searchColumn += am_house_allocation_requestDTO.rejectionReason + " ";
		am_house_allocation_requestDTO.searchColumn += am_house_allocation_requestDTO.approverPhoneNum + " ";
		am_house_allocation_requestDTO.searchColumn += am_house_allocation_requestDTO.approverNameEn + " ";
		am_house_allocation_requestDTO.searchColumn += am_house_allocation_requestDTO.approverNameBn + " ";
		am_house_allocation_requestDTO.searchColumn += am_house_allocation_requestDTO.approverOfficeNameEn + " ";
		am_house_allocation_requestDTO.searchColumn += am_house_allocation_requestDTO.approverOfficeNameBn + " ";
		am_house_allocation_requestDTO.searchColumn += am_house_allocation_requestDTO.approverOfficeUnitNameEn + " ";
		am_house_allocation_requestDTO.searchColumn += am_house_allocation_requestDTO.approverOfficeUnitNameBn + " ";
		am_house_allocation_requestDTO.searchColumn += am_house_allocation_requestDTO.approverOfficeUnitOrgNameEn + " ";
		am_house_allocation_requestDTO.searchColumn += am_house_allocation_requestDTO.approverOfficeUnitOrgNameBn + " ";
	}
	
	@Override
	public void set(PreparedStatement ps, Am_house_allocation_requestDTO am_house_allocation_requestDTO, boolean isInsert) throws SQLException
	{
		int index = 0;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(am_house_allocation_requestDTO);
		if(isInsert)
		{
			ps.setObject(++index,am_house_allocation_requestDTO.iD);
		}
		ps.setObject(++index,am_house_allocation_requestDTO.requesterOrgId);
		ps.setObject(++index,am_house_allocation_requestDTO.requesterOfficeId);
		ps.setObject(++index,am_house_allocation_requestDTO.requesterOfficeUnitId);
		ps.setObject(++index,am_house_allocation_requestDTO.requesterEmpId);
		ps.setObject(++index,am_house_allocation_requestDTO.requesterPhoneNum);
		ps.setObject(++index,am_house_allocation_requestDTO.requesterNameEn);
		ps.setObject(++index,am_house_allocation_requestDTO.requesterNameBn);
		ps.setObject(++index,am_house_allocation_requestDTO.requesterOfficeNameEn);
		ps.setObject(++index,am_house_allocation_requestDTO.requesterOfficeNameBn);
		ps.setObject(++index,am_house_allocation_requestDTO.requesterOfficeUnitNameEn);
		ps.setObject(++index,am_house_allocation_requestDTO.requesterOfficeUnitNameBn);
		ps.setObject(++index,am_house_allocation_requestDTO.requesterOfficeUnitOrgNameEn);
		ps.setObject(++index,am_house_allocation_requestDTO.requesterOfficeUnitOrgNameBn);
		ps.setObject(++index,am_house_allocation_requestDTO.dateOfBirth);
		ps.setObject(++index,am_house_allocation_requestDTO.maritalStatusCat);
		ps.setObject(++index,am_house_allocation_requestDTO.employeePayScaleId);
		ps.setObject(++index,am_house_allocation_requestDTO.govtFirstSalary);
		ps.setObject(++index,am_house_allocation_requestDTO.houseInDhakaLocationDetails);
		ps.setObject(++index,am_house_allocation_requestDTO.employeePayScaleTypeEn);
		ps.setObject(++index,am_house_allocation_requestDTO.employeePayScaleTypeBn);
		ps.setObject(++index,am_house_allocation_requestDTO.basicSalaryEn);
		ps.setObject(++index,am_house_allocation_requestDTO.basicSalaryBn);
		ps.setObject(++index,am_house_allocation_requestDTO.hasSpecialSalary);
		ps.setObject(++index,am_house_allocation_requestDTO.specialSalaryAmount);
		ps.setObject(++index,am_house_allocation_requestDTO.nextIncrementDate);
		ps.setObject(++index,am_house_allocation_requestDTO.salaryDescriptionUpdateFilesDropzone);
		ps.setObject(++index,am_house_allocation_requestDTO.currentHouseCat);
		ps.setObject(++index,am_house_allocation_requestDTO.amHouseOldNewCat);
		ps.setObject(++index,am_house_allocation_requestDTO.amHouseLocationCat);
		ps.setObject(++index,am_house_allocation_requestDTO.amHouseClassCat);
		ps.setObject(++index,am_house_allocation_requestDTO.amHouseClassSubCat);
		ps.setObject(++index,am_house_allocation_requestDTO.amHouseId);
		ps.setObject(++index,am_house_allocation_requestDTO.govtFirstAppointmentDate);
		ps.setObject(++index,am_house_allocation_requestDTO.prpFirstJoiningDate);
		ps.setObject(++index,am_house_allocation_requestDTO.currentDesignationJoiningDate);
		ps.setObject(++index,am_house_allocation_requestDTO.govtFirstAppointmentDesignationEn);
		ps.setObject(++index,am_house_allocation_requestDTO.govtFirstAppointmentDesignationBn);
		ps.setObject(++index,am_house_allocation_requestDTO.prpFirstJoiningDesignationEn);
		ps.setObject(++index,am_house_allocation_requestDTO.prpFirstJoiningDesignationBn);
		ps.setObject(++index,am_house_allocation_requestDTO.requestedHomeAvailablitySalaryEn);
		ps.setObject(++index,am_house_allocation_requestDTO.requestedHomeAvailablitySalaryBn);
		ps.setObject(++index,am_house_allocation_requestDTO.requestedHomeAvailablityDate);
		ps.setObject(++index,am_house_allocation_requestDTO.requestedHomeAvailablitySalaryFilesDropzone);
		ps.setObject(++index,am_house_allocation_requestDTO.isHigherAvailability);
		ps.setObject(++index,am_house_allocation_requestDTO.higherStepNumber);
		ps.setObject(++index,am_house_allocation_requestDTO.houseDivisionCat);
		ps.setObject(++index,am_house_allocation_requestDTO.currentHouseDescription);
		ps.setObject(++index,am_house_allocation_requestDTO.hasHouseInDhaka);
		ps.setObject(++index,am_house_allocation_requestDTO.houseInDhakaConstructionDate);
		ps.setObject(++index,am_house_allocation_requestDTO.hasRelativeGovtHouse);
		ps.setObject(++index,am_house_allocation_requestDTO.govtHouseRelativeName);
		ps.setObject(++index,am_house_allocation_requestDTO.relationshipCat);
		ps.setObject(++index,am_house_allocation_requestDTO.relativeDesignation);
		ps.setObject(++index,am_house_allocation_requestDTO.realtiveOffice);
		ps.setObject(++index,am_house_allocation_requestDTO.relativeSalary);
		ps.setObject(++index,am_house_allocation_requestDTO.houseNeededDescription);
		ps.setObject(++index,am_house_allocation_requestDTO.status);
		ps.setObject(++index,am_house_allocation_requestDTO.firstLayerApprovalStatus);
		ps.setObject(++index,am_house_allocation_requestDTO.firstLayerApprovalOrRejectionDate);
		ps.setObject(++index,am_house_allocation_requestDTO.firstLayerApproverOrgId);
		ps.setObject(++index,am_house_allocation_requestDTO.firstLayerApproverOfficeId);
		ps.setObject(++index,am_house_allocation_requestDTO.firstLayerApproverOfficeUnitId);
		ps.setObject(++index,am_house_allocation_requestDTO.firstLayerApproverEmpId);
		ps.setObject(++index,am_house_allocation_requestDTO.firstLayerApproverPhoneNumber);
		ps.setObject(++index,am_house_allocation_requestDTO.firstLayerApproverNameEn);
		ps.setObject(++index,am_house_allocation_requestDTO.firstLayerApproverNameBn);
		ps.setObject(++index,am_house_allocation_requestDTO.firstLayerApproverOfficeNameEn);
		ps.setObject(++index,am_house_allocation_requestDTO.firstLayerApproverOfficeNameBn);
		ps.setObject(++index,am_house_allocation_requestDTO.firstLayerApproverOfficeUnitNameEn);
		ps.setObject(++index,am_house_allocation_requestDTO.firstLayerApproverOfficeUnitNameBn);
		ps.setObject(++index,am_house_allocation_requestDTO.firstLayerApproverOfficeUnitOrgNameEn);
		ps.setObject(++index,am_house_allocation_requestDTO.firstLayerApproverOfficeUnitOrgNameBn);
		ps.setObject(++index,am_house_allocation_requestDTO.firstLayerApproverRejectionReason);
		ps.setObject(++index,am_house_allocation_requestDTO.decisionDate);
		ps.setObject(++index,am_house_allocation_requestDTO.rejectionReason);
		ps.setObject(++index,am_house_allocation_requestDTO.approvedDate);
		ps.setObject(++index,am_house_allocation_requestDTO.approverOrgId);
		ps.setObject(++index,am_house_allocation_requestDTO.approverOfficeId);
		ps.setObject(++index,am_house_allocation_requestDTO.approverOfficeUnitId);
		ps.setObject(++index,am_house_allocation_requestDTO.approverEmpId);
		ps.setObject(++index,am_house_allocation_requestDTO.approverPhoneNum);
		ps.setObject(++index,am_house_allocation_requestDTO.approverNameEn);
		ps.setObject(++index,am_house_allocation_requestDTO.approverNameBn);
		ps.setObject(++index,am_house_allocation_requestDTO.approverOfficeNameEn);
		ps.setObject(++index,am_house_allocation_requestDTO.approverOfficeNameBn);
		ps.setObject(++index,am_house_allocation_requestDTO.approverOfficeUnitNameEn);
		ps.setObject(++index,am_house_allocation_requestDTO.approverOfficeUnitNameBn);
		ps.setObject(++index,am_house_allocation_requestDTO.approverOfficeUnitOrgNameEn);
		ps.setObject(++index,am_house_allocation_requestDTO.approverOfficeUnitOrgNameBn);
		ps.setObject(++index,am_house_allocation_requestDTO.searchColumn);
		ps.setObject(++index,am_house_allocation_requestDTO.insertedByUserId);
		ps.setObject(++index,am_house_allocation_requestDTO.insertedByOrganogramId);
		ps.setObject(++index,am_house_allocation_requestDTO.insertionDate);
		ps.setObject(++index,am_house_allocation_requestDTO.lastModifierUser);
		ps.setObject(++index,am_house_allocation_requestDTO.withdrawerOrgId);
		ps.setObject(++index,am_house_allocation_requestDTO.withdrawerOfficeId);
		ps.setObject(++index,am_house_allocation_requestDTO.withdrawerOfficeUnitId);
		ps.setObject(++index,am_house_allocation_requestDTO.withdrawerEmpId);
		ps.setObject(++index,am_house_allocation_requestDTO.withdrawerPhoneNum);
		ps.setObject(++index,am_house_allocation_requestDTO.withdrawerNameEn);
		ps.setObject(++index,am_house_allocation_requestDTO.withdrawerNameBn);
		ps.setObject(++index,am_house_allocation_requestDTO.withdrawerOfficeNameEn);
		ps.setObject(++index,am_house_allocation_requestDTO.withdrawerOfficeNameBn);
		ps.setObject(++index,am_house_allocation_requestDTO.withdrawerOfficeUnitNameEn);
		ps.setObject(++index,am_house_allocation_requestDTO.withdrawerOfficeUnitNameBn);
		ps.setObject(++index,am_house_allocation_requestDTO.withdrawerOfficeUnitOrgNameEn);
		ps.setObject(++index,am_house_allocation_requestDTO.withdrawerOfficeUnitOrgNameBn);
		ps.setObject(++index,am_house_allocation_requestDTO.withdrawalDate);
		ps.setObject(++index,am_house_allocation_requestDTO.withdrawalReason);
		if(isInsert)
		{
			ps.setObject(++index,am_house_allocation_requestDTO.isDeleted);
		}
		ps.setObject(++index, lastModificationTime);
		if(!isInsert)
		{
			ps.setObject(++index,am_house_allocation_requestDTO.iD);
		}
	}
	
	@Override
	public Am_house_allocation_requestDTO buildObjectFromResultSet(ResultSet rs)
	{
		try
		{
			Am_house_allocation_requestDTO am_house_allocation_requestDTO = new Am_house_allocation_requestDTO();
			int i = 0;
			am_house_allocation_requestDTO.iD = rs.getLong(columnNames[i++]);
			am_house_allocation_requestDTO.requesterOrgId = rs.getLong(columnNames[i++]);
			am_house_allocation_requestDTO.requesterOfficeId = rs.getLong(columnNames[i++]);
			am_house_allocation_requestDTO.requesterOfficeUnitId = rs.getLong(columnNames[i++]);
			am_house_allocation_requestDTO.requesterEmpId = rs.getLong(columnNames[i++]);
			am_house_allocation_requestDTO.requesterPhoneNum = rs.getString(columnNames[i++]);
			am_house_allocation_requestDTO.requesterNameEn = rs.getString(columnNames[i++]);
			am_house_allocation_requestDTO.requesterNameBn = rs.getString(columnNames[i++]);
			am_house_allocation_requestDTO.requesterOfficeNameEn = rs.getString(columnNames[i++]);
			am_house_allocation_requestDTO.requesterOfficeNameBn = rs.getString(columnNames[i++]);
			am_house_allocation_requestDTO.requesterOfficeUnitNameEn = rs.getString(columnNames[i++]);
			am_house_allocation_requestDTO.requesterOfficeUnitNameBn = rs.getString(columnNames[i++]);
			am_house_allocation_requestDTO.requesterOfficeUnitOrgNameEn = rs.getString(columnNames[i++]);
			am_house_allocation_requestDTO.requesterOfficeUnitOrgNameBn = rs.getString(columnNames[i++]);
			am_house_allocation_requestDTO.dateOfBirth = rs.getLong(columnNames[i++]);
			am_house_allocation_requestDTO.maritalStatusCat = rs.getInt(columnNames[i++]);
			am_house_allocation_requestDTO.employeePayScaleId = rs.getLong(columnNames[i++]);
			am_house_allocation_requestDTO.govtFirstSalary = rs.getString(columnNames[i++]);
			am_house_allocation_requestDTO.houseInDhakaLocationDetails = rs.getString(columnNames[i++]);
			am_house_allocation_requestDTO.employeePayScaleTypeEn = rs.getString(columnNames[i++]);
			am_house_allocation_requestDTO.employeePayScaleTypeBn = rs.getString(columnNames[i++]);
			am_house_allocation_requestDTO.basicSalaryEn = rs.getString(columnNames[i++]);
			am_house_allocation_requestDTO.basicSalaryBn = rs.getString(columnNames[i++]);
			am_house_allocation_requestDTO.hasSpecialSalary = rs.getBoolean(columnNames[i++]);
			am_house_allocation_requestDTO.specialSalaryAmount = rs.getLong(columnNames[i++]);
			am_house_allocation_requestDTO.nextIncrementDate = rs.getLong(columnNames[i++]);
			am_house_allocation_requestDTO.salaryDescriptionUpdateFilesDropzone = rs.getLong(columnNames[i++]);
			am_house_allocation_requestDTO.currentHouseCat = rs.getInt(columnNames[i++]);
			am_house_allocation_requestDTO.amHouseOldNewCat = rs.getInt(columnNames[i++]);
			am_house_allocation_requestDTO.amHouseLocationCat = rs.getInt(columnNames[i++]);
			am_house_allocation_requestDTO.amHouseClassCat = rs.getInt(columnNames[i++]);
			am_house_allocation_requestDTO.amHouseClassSubCat = rs.getInt(columnNames[i++]);
			am_house_allocation_requestDTO.amHouseId = rs.getLong(columnNames[i++]);
			am_house_allocation_requestDTO.govtFirstAppointmentDate = rs.getLong(columnNames[i++]);
			am_house_allocation_requestDTO.prpFirstJoiningDate = rs.getLong(columnNames[i++]);
			am_house_allocation_requestDTO.currentDesignationJoiningDate = rs.getLong(columnNames[i++]);
			am_house_allocation_requestDTO.govtFirstAppointmentDesignationEn = rs.getString(columnNames[i++]);
			am_house_allocation_requestDTO.govtFirstAppointmentDesignationBn = rs.getString(columnNames[i++]);
			am_house_allocation_requestDTO.prpFirstJoiningDesignationEn = rs.getString(columnNames[i++]);
			am_house_allocation_requestDTO.prpFirstJoiningDesignationBn = rs.getString(columnNames[i++]);
			am_house_allocation_requestDTO.requestedHomeAvailablitySalaryEn = rs.getString(columnNames[i++]);
			am_house_allocation_requestDTO.requestedHomeAvailablitySalaryBn = rs.getString(columnNames[i++]);
			am_house_allocation_requestDTO.requestedHomeAvailablityDate = rs.getLong(columnNames[i++]);
			am_house_allocation_requestDTO.requestedHomeAvailablitySalaryFilesDropzone = rs.getLong(columnNames[i++]);
			am_house_allocation_requestDTO.isHigherAvailability = rs.getBoolean(columnNames[i++]);
			am_house_allocation_requestDTO.higherStepNumber = rs.getString(columnNames[i++]);
			am_house_allocation_requestDTO.houseDivisionCat = rs.getInt(columnNames[i++]);
			am_house_allocation_requestDTO.currentHouseDescription = rs.getString(columnNames[i++]);
			am_house_allocation_requestDTO.hasHouseInDhaka = rs.getBoolean(columnNames[i++]);
			am_house_allocation_requestDTO.houseInDhakaConstructionDate = rs.getLong(columnNames[i++]);
			am_house_allocation_requestDTO.hasRelativeGovtHouse = rs.getBoolean(columnNames[i++]);
			am_house_allocation_requestDTO.govtHouseRelativeName = rs.getString(columnNames[i++]);
			am_house_allocation_requestDTO.relationshipCat = rs.getInt(columnNames[i++]);
			am_house_allocation_requestDTO.relativeDesignation = rs.getString(columnNames[i++]);
			am_house_allocation_requestDTO.realtiveOffice = rs.getString(columnNames[i++]);
			am_house_allocation_requestDTO.relativeSalary = rs.getString(columnNames[i++]);
			am_house_allocation_requestDTO.houseNeededDescription = rs.getString(columnNames[i++]);
			am_house_allocation_requestDTO.status = rs.getInt(columnNames[i++]);
			am_house_allocation_requestDTO.firstLayerApprovalStatus = rs.getInt(columnNames[i++]);
			am_house_allocation_requestDTO.firstLayerApprovalOrRejectionDate = rs.getLong(columnNames[i++]);
			am_house_allocation_requestDTO.firstLayerApproverOrgId = rs.getInt(columnNames[i++]);
			am_house_allocation_requestDTO.firstLayerApproverOfficeId = rs.getInt(columnNames[i++]);
			am_house_allocation_requestDTO.firstLayerApproverOfficeUnitId = rs.getInt(columnNames[i++]);
			am_house_allocation_requestDTO.firstLayerApproverEmpId = rs.getInt(columnNames[i++]);
			am_house_allocation_requestDTO.firstLayerApproverPhoneNumber = rs.getString(columnNames[i++]);
			am_house_allocation_requestDTO.firstLayerApproverNameEn = rs.getString(columnNames[i++]);
			am_house_allocation_requestDTO.firstLayerApproverNameBn = rs.getString(columnNames[i++]);
			am_house_allocation_requestDTO.firstLayerApproverOfficeNameEn = rs.getString(columnNames[i++]);
			am_house_allocation_requestDTO.firstLayerApproverOfficeNameBn = rs.getString(columnNames[i++]);
			am_house_allocation_requestDTO.firstLayerApproverOfficeUnitNameEn = rs.getString(columnNames[i++]);
			am_house_allocation_requestDTO.firstLayerApproverOfficeUnitNameBn = rs.getString(columnNames[i++]);
			am_house_allocation_requestDTO.firstLayerApproverOfficeUnitOrgNameEn = rs.getString(columnNames[i++]);
			am_house_allocation_requestDTO.firstLayerApproverOfficeUnitOrgNameBn = rs.getString(columnNames[i++]);
			am_house_allocation_requestDTO.firstLayerApproverRejectionReason = rs.getString(columnNames[i++]);
			am_house_allocation_requestDTO.decisionDate = rs.getLong(columnNames[i++]);
			am_house_allocation_requestDTO.rejectionReason = rs.getString(columnNames[i++]);
			am_house_allocation_requestDTO.approvedDate = rs.getLong(columnNames[i++]);
			am_house_allocation_requestDTO.approverOrgId = rs.getLong(columnNames[i++]);
			am_house_allocation_requestDTO.approverOfficeId = rs.getLong(columnNames[i++]);
			am_house_allocation_requestDTO.approverOfficeUnitId = rs.getLong(columnNames[i++]);
			am_house_allocation_requestDTO.approverEmpId = rs.getLong(columnNames[i++]);
			am_house_allocation_requestDTO.approverPhoneNum = rs.getString(columnNames[i++]);
			am_house_allocation_requestDTO.approverNameEn = rs.getString(columnNames[i++]);
			am_house_allocation_requestDTO.approverNameBn = rs.getString(columnNames[i++]);
			am_house_allocation_requestDTO.approverOfficeNameEn = rs.getString(columnNames[i++]);
			am_house_allocation_requestDTO.approverOfficeNameBn = rs.getString(columnNames[i++]);
			am_house_allocation_requestDTO.approverOfficeUnitNameEn = rs.getString(columnNames[i++]);
			am_house_allocation_requestDTO.approverOfficeUnitNameBn = rs.getString(columnNames[i++]);
			am_house_allocation_requestDTO.approverOfficeUnitOrgNameEn = rs.getString(columnNames[i++]);
			am_house_allocation_requestDTO.approverOfficeUnitOrgNameBn = rs.getString(columnNames[i++]);
			am_house_allocation_requestDTO.searchColumn = rs.getString(columnNames[i++]);
			am_house_allocation_requestDTO.insertedByUserId = rs.getLong(columnNames[i++]);
			am_house_allocation_requestDTO.insertedByOrganogramId = rs.getLong(columnNames[i++]);
			am_house_allocation_requestDTO.insertionDate = rs.getLong(columnNames[i++]);
			am_house_allocation_requestDTO.lastModifierUser = rs.getString(columnNames[i++]);

			am_house_allocation_requestDTO.withdrawerOrgId = rs.getLong(columnNames[i++]);
			am_house_allocation_requestDTO.withdrawerOfficeId = rs.getLong(columnNames[i++]);
			am_house_allocation_requestDTO.withdrawerOfficeUnitId = rs.getLong(columnNames[i++]);
			am_house_allocation_requestDTO.withdrawerEmpId = rs.getLong(columnNames[i++]);
			am_house_allocation_requestDTO.withdrawerPhoneNum = rs.getString(columnNames[i++]);
			am_house_allocation_requestDTO.withdrawerNameEn = rs.getString(columnNames[i++]);
			am_house_allocation_requestDTO.withdrawerNameBn = rs.getString(columnNames[i++]);
			am_house_allocation_requestDTO.withdrawerOfficeNameEn = rs.getString(columnNames[i++]);
			am_house_allocation_requestDTO.withdrawerOfficeNameBn = rs.getString(columnNames[i++]);
			am_house_allocation_requestDTO.withdrawerOfficeUnitNameEn = rs.getString(columnNames[i++]);
			am_house_allocation_requestDTO.withdrawerOfficeUnitNameBn = rs.getString(columnNames[i++]);
			am_house_allocation_requestDTO.withdrawerOfficeUnitOrgNameEn = rs.getString(columnNames[i++]);
			am_house_allocation_requestDTO.withdrawerOfficeUnitOrgNameBn = rs.getString(columnNames[i++]);
			am_house_allocation_requestDTO.withdrawalDate = rs.getLong(columnNames[i++]);
			am_house_allocation_requestDTO.withdrawalReason = rs.getString(columnNames[i++]);

			am_house_allocation_requestDTO.isDeleted = rs.getInt(columnNames[i++]);
			am_house_allocation_requestDTO.lastModificationTime = rs.getLong(columnNames[i++]);
			
			return am_house_allocation_requestDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
		
	public Am_house_allocation_requestDTO getDTOByID (long id)
	{
		return getDTOFromID(id);
	}

	@Override
	public String getTableName() {
		return "am_house_allocation_request";
	}

	@Override
	public Map<String, String> getSearchMap() {
		return searchMap;
	}

	public long add(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Am_house_allocation_requestDTO) commonDTO,addQuery,true);
	}

	public long update(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Am_house_allocation_requestDTO) commonDTO,updateQuery,false);
	}
	
	@Override
	public String getLastModifierUser(){
		return "last_modifier_user";
    }

	public List<Am_house_allocation_requestDTO> getPendingRequest(){
    	String sql = "select * from " + getTableName() +
				" where status = " + CommonApprovalStatus.PENDING.getValue() +
				" order by lastModificationTime desc ";
    	return getDTOs(sql, this::buildObjectFromResultSet);
	}

	public List<Am_house_allocation_requestDTO> getExistingRequestByOrgId(long orgId){
		String sql = "select * from " + getTableName() +
				" where " +
				" (status = " + CommonApprovalStatus.SATISFIED.getValue() + ")"+
				" and isDeleted = 0" +
				" and requester_org_id = " + orgId +
				" order by lastModificationTime desc ";
		return getDTOs(sql, this::buildObjectFromResultSet);
	}

	public List<CountDTO> getCountByStatus(){
		String countQuery = "SELECT status, COUNT(*) AS count FROM "
				+ getTableName() + " where isDeleted = 0 and status > 0 GROUP BY status  ";
		return ConnectionAndStatementUtil.getListOfT(countQuery,this::getCountBuild);
	}

	public CountDTO getCountBuild(ResultSet rs)
	{
		try
		{
			CountDTO countDTO = new CountDTO();
			countDTO.type4 = rs.getInt("status");
			countDTO.type5 = rs.getInt("count");
			return countDTO;
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
			return null;
		}
	}

	public int getCountByHouseId(long houseID){
		String countQuery = "SELECT count(*) as countID FROM "
				+ getTableName() + " where isDeleted = 0 and am_house_id = ? ";
		return ConnectionAndStatementUtil.getT(countQuery, Arrays.asList(houseID), rs -> {
			try {
				return rs.getInt("countID");
			} catch (SQLException ex) {
				ex.printStackTrace();
				return 0;
			}
		}, 0);
	}

	public int getNotPendingCountById(List<Long> ids){
		String idStr = ids.stream()
				.distinct()
				.map(String::valueOf)
				.collect(Collectors.joining(","));
		String countQuery = "select count(*) as countID from " + getTableName();
		countQuery += " WHERE id IN (" + idStr + ")";
		countQuery += " AND status != " + CommonApprovalStatus.PENDING.getValue();
		countQuery += " AND isDeleted = 0";

		return ConnectionAndStatementUtil.getT(countQuery, rs -> {
			try {
				return rs.getInt("countID");
			} catch (SQLException ex) {
				ex.printStackTrace();
				return 0;
			}
		}, 0);
	}
				
}
	