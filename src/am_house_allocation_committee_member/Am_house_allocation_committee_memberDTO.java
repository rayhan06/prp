package am_house_allocation_committee_member;
import java.util.*; 
import util.*; 


public class Am_house_allocation_committee_memberDTO extends CommonDTO
{

	public long committeeId = -1;
	public long memberOrgId = -1;
	public long memberOfficeId = -1;
	public long memberOfficeUnitId = -1;
	public long memberEmpId = -1;
    public String memberPhoneNum = "";
    public String memberNameEn = "";
    public String memberNameBn = "";
    public String memberOfficeNameEn = "";
    public String memberOfficeNameBn = "";
    public String memberOfficeUnitNameEn = "";
    public String memberOfficeUnitNameBn = "";
    public String memberOfficeUnitOrgNameEn = "";
    public String memberOfficeUnitOrgNameBn = "";
	public long insertedByUserId = -1;
	public long insertedByOrganogramId = -1;
	public long modifiedBy = -1;
	public long insertionDate = -1;
	
	
    @Override
	public String toString() {
            return "$Am_house_allocation_committee_memberDTO[" +
            " iD = " + iD +
            " committeeId = " + committeeId +
            " memberOrgId = " + memberOrgId +
            " memberOfficeId = " + memberOfficeId +
            " memberOfficeUnitId = " + memberOfficeUnitId +
            " memberEmpId = " + memberEmpId +
            " memberPhoneNum = " + memberPhoneNum +
            " memberNameEn = " + memberNameEn +
            " memberNameBn = " + memberNameBn +
            " memberOfficeNameEn = " + memberOfficeNameEn +
            " memberOfficeNameBn = " + memberOfficeNameBn +
            " memberOfficeUnitNameEn = " + memberOfficeUnitNameEn +
            " memberOfficeUnitNameBn = " + memberOfficeUnitNameBn +
            " memberOfficeUnitOrgNameEn = " + memberOfficeUnitOrgNameEn +
            " memberOfficeUnitOrgNameBn = " + memberOfficeUnitOrgNameBn +
            " insertedByUserId = " + insertedByUserId +
            " insertedByOrganogramId = " + insertedByOrganogramId +
            " modifiedBy = " + modifiedBy +
            " insertionDate = " + insertionDate +
            " searchColumn = " + searchColumn +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}