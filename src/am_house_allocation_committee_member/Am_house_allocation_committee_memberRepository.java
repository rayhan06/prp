package am_house_allocation_committee_member;

import com.google.gson.Gson;
import org.apache.log4j.Logger;
import repository.Repository;
import repository.RepositoryManager;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;


public class Am_house_allocation_committee_memberRepository implements Repository {
	Am_house_allocation_committee_memberDAO am_house_allocation_committee_memberDAO;
	Gson gson = new Gson();
	
	static Logger logger = Logger.getLogger(Am_house_allocation_committee_memberRepository.class);
	Map<Long, Am_house_allocation_committee_memberDTO>mapOfAm_house_allocation_committee_memberDTOToiD;
	Map<Long, Set<Am_house_allocation_committee_memberDTO>>mapOfAm_house_allocation_committee_memberDTOToComId;

  
	private Am_house_allocation_committee_memberRepository(){
		am_house_allocation_committee_memberDAO = Am_house_allocation_committee_memberDAO.getInstance();
		mapOfAm_house_allocation_committee_memberDTOToiD = new ConcurrentHashMap<>();
		mapOfAm_house_allocation_committee_memberDTOToComId = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	private static class LazyLoader{
        final static Am_house_allocation_committee_memberRepository INSTANCE = new Am_house_allocation_committee_memberRepository();
    }

    public static Am_house_allocation_committee_memberRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

	public void reload(boolean reloadAll){
		try {
			List<Am_house_allocation_committee_memberDTO> am_house_allocation_committee_memberDTOs =
					am_house_allocation_committee_memberDAO.getAllDTOs(reloadAll);
			for(Am_house_allocation_committee_memberDTO am_house_allocation_committee_memberDTO : am_house_allocation_committee_memberDTOs) {
				Am_house_allocation_committee_memberDTO oldAm_house_allocation_committee_memberDTO =
						getAm_house_allocation_committee_memberDTOByIDWithoutClone(am_house_allocation_committee_memberDTO.iD);
				if( oldAm_house_allocation_committee_memberDTO != null ) {
					mapOfAm_house_allocation_committee_memberDTOToiD.remove(oldAm_house_allocation_committee_memberDTO.iD);

					if(mapOfAm_house_allocation_committee_memberDTOToComId.containsKey(oldAm_house_allocation_committee_memberDTO.committeeId)) {
						mapOfAm_house_allocation_committee_memberDTOToComId.get(oldAm_house_allocation_committee_memberDTO.committeeId).
								remove(oldAm_house_allocation_committee_memberDTO);
					}
					if(mapOfAm_house_allocation_committee_memberDTOToComId.get(oldAm_house_allocation_committee_memberDTO.committeeId).isEmpty()) {
						mapOfAm_house_allocation_committee_memberDTOToComId.remove(oldAm_house_allocation_committee_memberDTO.committeeId);
					}
					
				}
				if(am_house_allocation_committee_memberDTO.isDeleted == 0) 
				{
					
					mapOfAm_house_allocation_committee_memberDTOToiD.put(am_house_allocation_committee_memberDTO.iD, am_house_allocation_committee_memberDTO);

					if( ! mapOfAm_house_allocation_committee_memberDTOToComId.containsKey(am_house_allocation_committee_memberDTO.committeeId)) {
						mapOfAm_house_allocation_committee_memberDTOToComId.put(am_house_allocation_committee_memberDTO.committeeId, new HashSet<>());
					}
					mapOfAm_house_allocation_committee_memberDTOToComId.get(am_house_allocation_committee_memberDTO.committeeId).add(am_house_allocation_committee_memberDTO);
				
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Am_house_allocation_committee_memberDTO> getAm_house_allocation_committee_memberList() {
		return clone(new ArrayList<>(this.mapOfAm_house_allocation_committee_memberDTOToiD.values()));

	}
	
	
	public Am_house_allocation_committee_memberDTO getAm_house_allocation_committee_memberDTOByIDWithoutClone( long ID){
		return clone(mapOfAm_house_allocation_committee_memberDTOToiD.get(ID));
	}

	public Am_house_allocation_committee_memberDTO getAm_house_allocation_committee_memberDTOByID( long ID){
		return mapOfAm_house_allocation_committee_memberDTOToiD.get(ID);
	}

	public List<Am_house_allocation_committee_memberDTO> getAm_house_allocation_committee_memberDTOByCom_id(long comId) {
		return clone(new ArrayList<>( mapOfAm_house_allocation_committee_memberDTOToComId.getOrDefault(comId,new HashSet<>())));
	}

	
	@Override
	public String getTableName() {
		return "am_house_allocation_committee_member";
	}

	public Am_house_allocation_committee_memberDTO clone(Am_house_allocation_committee_memberDTO dto) {
		String raw = gson.toJson(dto);
		return gson.fromJson(raw, Am_house_allocation_committee_memberDTO.class);
	}

	public List<Am_house_allocation_committee_memberDTO> clone(List<Am_house_allocation_committee_memberDTO> dtoList) {
		return dtoList
				.stream()
				.map(this::clone)
				.collect(Collectors.toList());
	}
}


