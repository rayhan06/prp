package am_house_allocation_committee_member;

import java.io.IOException;
import java.io.*;
import java.text.SimpleDateFormat;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import org.apache.log4j.Logger;

import login.LoginDTO;
import permission.MenuConstants;
import role.PermissionRepository;

import sessionmanager.SessionConstants;
import user.UserDTO;
import util.*;
import javax.servlet.http.*;
import java.util.*;


import geolocation.GeoLocationDAO2;
import language.LC;
import language.LM;
import common.ApiResponse;
import common.BaseServlet;
import com.google.gson.Gson;
import pb.*;
import pbReport.*;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;




/**
 * Servlet implementation class Am_house_allocation_committee_memberServlet
 */
@WebServlet("/Am_house_allocation_committee_memberServlet")
@MultipartConfig
public class Am_house_allocation_committee_memberServlet extends BaseServlet
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Am_house_allocation_committee_memberServlet.class);

    @Override
    public String getTableName() {
        return Am_house_allocation_committee_memberDAO.getInstance().getTableName();
    }

    @Override
    public String getServletName() {
        return "Am_house_allocation_committee_memberServlet";
    }

    @Override
    public Am_house_allocation_committee_memberDAO getCommonDAOService() {
        return Am_house_allocation_committee_memberDAO.getInstance();
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[] {MenuConstants.AM_HOUSE_ALLOCATION_COMMITTEE_MEMBER_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[] {MenuConstants.AM_HOUSE_ALLOCATION_COMMITTEE_MEMBER_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[] {MenuConstants.AM_HOUSE_ALLOCATION_COMMITTEE_MEMBER_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Am_house_allocation_committee_memberServlet.class;
    }
    private final Gson gson = new Gson();


	@Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception
	{
		// TODO Auto-generated method stub
		try
		{
			request.setAttribute("failureMessage", "");
			System.out.println("%%%% addAm_house_allocation_committee_member");
			String language = userDTO.languageID == SessionConstants.ENGLISH? "English":"Bangla";
			Am_house_allocation_committee_memberDTO am_house_allocation_committee_memberDTO;
			SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");

			if(addFlag == true)
			{
				am_house_allocation_committee_memberDTO = new Am_house_allocation_committee_memberDTO();
			}
			else
			{
				am_house_allocation_committee_memberDTO = Am_house_allocation_committee_memberDAO.getInstance().getDTOFromID(Long.parseLong(request.getParameter("iD")));
			}

			String Value = "";

			Value = request.getParameter("committeeId");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("committeeId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				am_house_allocation_committee_memberDTO.committeeId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("memberOrgId");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("memberOrgId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				am_house_allocation_committee_memberDTO.memberOrgId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("memberOfficeId");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("memberOfficeId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				am_house_allocation_committee_memberDTO.memberOfficeId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("memberOfficeUnitId");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("memberOfficeUnitId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				am_house_allocation_committee_memberDTO.memberOfficeUnitId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("memberEmpId");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("memberEmpId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				am_house_allocation_committee_memberDTO.memberEmpId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("memberPhoneNum");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("memberPhoneNum = " + Value);
			if(Value != null)
			{
				am_house_allocation_committee_memberDTO.memberPhoneNum = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("memberNameEn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("memberNameEn = " + Value);
			if(Value != null)
			{
				am_house_allocation_committee_memberDTO.memberNameEn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("memberNameBn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("memberNameBn = " + Value);
			if(Value != null)
			{
				am_house_allocation_committee_memberDTO.memberNameBn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("memberOfficeNameEn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("memberOfficeNameEn = " + Value);
			if(Value != null)
			{
				am_house_allocation_committee_memberDTO.memberOfficeNameEn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("memberOfficeNameBn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("memberOfficeNameBn = " + Value);
			if(Value != null)
			{
				am_house_allocation_committee_memberDTO.memberOfficeNameBn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("memberOfficeUnitNameEn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("memberOfficeUnitNameEn = " + Value);
			if(Value != null)
			{
				am_house_allocation_committee_memberDTO.memberOfficeUnitNameEn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("memberOfficeUnitNameBn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("memberOfficeUnitNameBn = " + Value);
			if(Value != null)
			{
				am_house_allocation_committee_memberDTO.memberOfficeUnitNameBn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("memberOfficeUnitOrgNameEn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("memberOfficeUnitOrgNameEn = " + Value);
			if(Value != null)
			{
				am_house_allocation_committee_memberDTO.memberOfficeUnitOrgNameEn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("memberOfficeUnitOrgNameBn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("memberOfficeUnitOrgNameBn = " + Value);
			if(Value != null)
			{
				am_house_allocation_committee_memberDTO.memberOfficeUnitOrgNameBn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			if(addFlag)
			{
				am_house_allocation_committee_memberDTO.insertedByUserId = userDTO.ID;
			}


			if(addFlag)
			{
				am_house_allocation_committee_memberDTO.insertedByOrganogramId = userDTO.organogramID;
			}


			Value = request.getParameter("modifiedBy");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("modifiedBy = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				am_house_allocation_committee_memberDTO.modifiedBy = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			if(addFlag)
			{
				am_house_allocation_committee_memberDTO.insertionDate = TimeConverter.getToday();
			}


			Value = request.getParameter("searchColumn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("searchColumn = " + Value);
			if(Value != null)
			{
				am_house_allocation_committee_memberDTO.searchColumn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			System.out.println("Done adding  addAm_house_allocation_committee_member dto = " + am_house_allocation_committee_memberDTO);
			long returnedID = -1;


			if(addFlag == true)
			{
				returnedID = Am_house_allocation_committee_memberDAO.getInstance().add(am_house_allocation_committee_memberDTO);
			}
			else
			{
				returnedID = Am_house_allocation_committee_memberDAO.getInstance().update(am_house_allocation_committee_memberDTO);
			}


			return am_house_allocation_committee_memberDTO;

		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}
}

