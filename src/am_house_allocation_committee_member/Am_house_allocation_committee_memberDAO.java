package am_house_allocation_committee_member;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.sql.SQLException;
import common.CommonDAOService;
import org.apache.log4j.Logger;
import util.*;
import pb.*;

public class Am_house_allocation_committee_memberDAO  implements CommonDAOService<Am_house_allocation_committee_memberDTO>
{
	
	Logger logger = Logger.getLogger(getClass());
	
	private static String addQuery = "";
	private static String updateQuery = "";
	private String[] columnNames = null;

	public Am_house_allocation_committee_memberDAO()
	{
		columnNames = new String[] 
		{
			"ID",
			"committee_id",
			"member_org_id",
			"member_office_id",
			"member_office_unit_id",
			"member_emp_id",
			"member_phone_num",
			"member_name_en",
			"member_name_bn",
			"member_office_name_en",
			"member_office_name_bn",
			"member_office_unit_name_en",
			"member_office_unit_name_bn",
			"member_office_unit_org_name_en",
			"member_office_unit_org_name_bn",
			"inserted_by_user_id",
			"inserted_by_organogram_id",
			"modified_by",
			"insertion_date",
			"search_column",
			"isDeleted",
			"lastModificationTime"
		};
		updateQuery = getUpdateQuery(columnNames);
        addQuery = getAddQuery(columnNames);
		
		searchMap.put("member_phone_num"," and (member_phone_num like ?)");
		searchMap.put("member_name_en"," and (member_name_en like ?)");
		searchMap.put("member_name_bn"," and (member_name_bn like ?)");
		searchMap.put("member_office_name_en"," and (member_office_name_en like ?)");
		searchMap.put("member_office_name_bn"," and (member_office_name_bn like ?)");
		searchMap.put("member_office_unit_name_en"," and (member_office_unit_name_en like ?)");
		searchMap.put("member_office_unit_name_bn"," and (member_office_unit_name_bn like ?)");
		searchMap.put("member_office_unit_org_name_en"," and (member_office_unit_org_name_en like ?)");
		searchMap.put("member_office_unit_org_name_bn"," and (member_office_unit_org_name_bn like ?)");
		searchMap.put("modified_by"," and (modified_by like ?)");
		searchMap.put("insertion_date_start"," and (insertion_date >= ?)");
		searchMap.put("insertion_date_end"," and (insertion_date <= ?)");
		searchMap.put("AnyField"," and (search_column like ?)");
	}

	private final Map<String,String> searchMap = new HashMap<>();

    private static class LazyLoader
    {
    	static final Am_house_allocation_committee_memberDAO INSTANCE = new Am_house_allocation_committee_memberDAO();
	}

	public static Am_house_allocation_committee_memberDAO getInstance()
	{
    	return LazyLoader.INSTANCE;
	}
	
	public void setSearchColumn(Am_house_allocation_committee_memberDTO am_house_allocation_committee_memberDTO)
	{
		am_house_allocation_committee_memberDTO.searchColumn = "";
		am_house_allocation_committee_memberDTO.searchColumn += am_house_allocation_committee_memberDTO.memberPhoneNum + " ";
		am_house_allocation_committee_memberDTO.searchColumn += am_house_allocation_committee_memberDTO.memberNameEn + " ";
		am_house_allocation_committee_memberDTO.searchColumn += am_house_allocation_committee_memberDTO.memberNameBn + " ";
		am_house_allocation_committee_memberDTO.searchColumn += am_house_allocation_committee_memberDTO.memberOfficeNameEn + " ";
		am_house_allocation_committee_memberDTO.searchColumn += am_house_allocation_committee_memberDTO.memberOfficeNameBn + " ";
		am_house_allocation_committee_memberDTO.searchColumn += am_house_allocation_committee_memberDTO.memberOfficeUnitNameEn + " ";
		am_house_allocation_committee_memberDTO.searchColumn += am_house_allocation_committee_memberDTO.memberOfficeUnitNameBn + " ";
		am_house_allocation_committee_memberDTO.searchColumn += am_house_allocation_committee_memberDTO.memberOfficeUnitOrgNameEn + " ";
		am_house_allocation_committee_memberDTO.searchColumn += am_house_allocation_committee_memberDTO.memberOfficeUnitOrgNameBn + " ";
		am_house_allocation_committee_memberDTO.searchColumn += am_house_allocation_committee_memberDTO.modifiedBy + " ";
	}
	
	@Override
	public void set(PreparedStatement ps, Am_house_allocation_committee_memberDTO am_house_allocation_committee_memberDTO, boolean isInsert) throws SQLException
	{
		int index = 0;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(am_house_allocation_committee_memberDTO);
		if(isInsert)
		{
			ps.setObject(++index,am_house_allocation_committee_memberDTO.iD);
		}
		ps.setObject(++index,am_house_allocation_committee_memberDTO.committeeId);
		ps.setObject(++index,am_house_allocation_committee_memberDTO.memberOrgId);
		ps.setObject(++index,am_house_allocation_committee_memberDTO.memberOfficeId);
		ps.setObject(++index,am_house_allocation_committee_memberDTO.memberOfficeUnitId);
		ps.setObject(++index,am_house_allocation_committee_memberDTO.memberEmpId);
		ps.setObject(++index,am_house_allocation_committee_memberDTO.memberPhoneNum);
		ps.setObject(++index,am_house_allocation_committee_memberDTO.memberNameEn);
		ps.setObject(++index,am_house_allocation_committee_memberDTO.memberNameBn);
		ps.setObject(++index,am_house_allocation_committee_memberDTO.memberOfficeNameEn);
		ps.setObject(++index,am_house_allocation_committee_memberDTO.memberOfficeNameBn);
		ps.setObject(++index,am_house_allocation_committee_memberDTO.memberOfficeUnitNameEn);
		ps.setObject(++index,am_house_allocation_committee_memberDTO.memberOfficeUnitNameBn);
		ps.setObject(++index,am_house_allocation_committee_memberDTO.memberOfficeUnitOrgNameEn);
		ps.setObject(++index,am_house_allocation_committee_memberDTO.memberOfficeUnitOrgNameBn);
		ps.setObject(++index,am_house_allocation_committee_memberDTO.insertedByUserId);
		ps.setObject(++index,am_house_allocation_committee_memberDTO.insertedByOrganogramId);
		ps.setObject(++index,am_house_allocation_committee_memberDTO.modifiedBy);
		ps.setObject(++index,am_house_allocation_committee_memberDTO.insertionDate);
		ps.setObject(++index,am_house_allocation_committee_memberDTO.searchColumn);
		if(isInsert)
		{
			ps.setObject(++index,am_house_allocation_committee_memberDTO.isDeleted);
		}
		ps.setObject(++index, lastModificationTime);
		if(!isInsert)
		{
			ps.setObject(++index,am_house_allocation_committee_memberDTO.iD);
		}
	}
	
	@Override
	public Am_house_allocation_committee_memberDTO buildObjectFromResultSet(ResultSet rs)
	{
		try
		{
			Am_house_allocation_committee_memberDTO am_house_allocation_committee_memberDTO = new Am_house_allocation_committee_memberDTO();
			int i = 0;
			am_house_allocation_committee_memberDTO.iD = rs.getLong(columnNames[i++]);
			am_house_allocation_committee_memberDTO.committeeId = rs.getLong(columnNames[i++]);
			am_house_allocation_committee_memberDTO.memberOrgId = rs.getLong(columnNames[i++]);
			am_house_allocation_committee_memberDTO.memberOfficeId = rs.getLong(columnNames[i++]);
			am_house_allocation_committee_memberDTO.memberOfficeUnitId = rs.getLong(columnNames[i++]);
			am_house_allocation_committee_memberDTO.memberEmpId = rs.getLong(columnNames[i++]);
			am_house_allocation_committee_memberDTO.memberPhoneNum = rs.getString(columnNames[i++]);
			am_house_allocation_committee_memberDTO.memberNameEn = rs.getString(columnNames[i++]);
			am_house_allocation_committee_memberDTO.memberNameBn = rs.getString(columnNames[i++]);
			am_house_allocation_committee_memberDTO.memberOfficeNameEn = rs.getString(columnNames[i++]);
			am_house_allocation_committee_memberDTO.memberOfficeNameBn = rs.getString(columnNames[i++]);
			am_house_allocation_committee_memberDTO.memberOfficeUnitNameEn = rs.getString(columnNames[i++]);
			am_house_allocation_committee_memberDTO.memberOfficeUnitNameBn = rs.getString(columnNames[i++]);
			am_house_allocation_committee_memberDTO.memberOfficeUnitOrgNameEn = rs.getString(columnNames[i++]);
			am_house_allocation_committee_memberDTO.memberOfficeUnitOrgNameBn = rs.getString(columnNames[i++]);
			am_house_allocation_committee_memberDTO.insertedByUserId = rs.getLong(columnNames[i++]);
			am_house_allocation_committee_memberDTO.insertedByOrganogramId = rs.getLong(columnNames[i++]);
			am_house_allocation_committee_memberDTO.modifiedBy = rs.getLong(columnNames[i++]);
			am_house_allocation_committee_memberDTO.insertionDate = rs.getLong(columnNames[i++]);
			am_house_allocation_committee_memberDTO.searchColumn = rs.getString(columnNames[i++]);
			am_house_allocation_committee_memberDTO.isDeleted = rs.getInt(columnNames[i++]);
			am_house_allocation_committee_memberDTO.lastModificationTime = rs.getLong(columnNames[i++]);
			return am_house_allocation_committee_memberDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
		
	public Am_house_allocation_committee_memberDTO getDTOByID (long id)
	{
		return getDTOFromID(id);
	}

	@Override
	public String getTableName() {
		return "am_house_allocation_committee_member";
	}

	@Override
	public Map<String, String> getSearchMap() {
		return searchMap;
	}

	public long add(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Am_house_allocation_committee_memberDTO) commonDTO,addQuery,true);
	}

	public long update(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Am_house_allocation_committee_memberDTO) commonDTO,updateQuery,false);
	}
	
	@Override
	public String getLastModifierUser(){
		return "modified_by";
    }

    public List<Am_house_allocation_committee_memberDTO> getDTOsByCommitteeId(long comId){
		String sql = "SELECT * FROM am_house_allocation_committee_member";
		sql += " WHERE  committee_id = ?  and isDeleted =  0";
		sql += " order by am_house_allocation_committee_member.lastModificationTime desc";

		return getDTOs(sql, Arrays.asList(comId), this::buildObjectFromResultSet);
	}
				
}
	