package reagent_requisition_lot;
import java.util.*; 
import util.*;


public class ReagentRequisitionItemMAPS extends CommonMaps
{	
	public ReagentRequisitionItemMAPS(String tableName)
	{
		

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("reagentRequisitionLotId".toLowerCase(), "reagentRequisitionLotId".toLowerCase());
		java_DTO_map.put("departmentCat".toLowerCase(), "departmentCat".toLowerCase());
		java_DTO_map.put("medicalReagentNameType".toLowerCase(), "medicalReagentNameType".toLowerCase());
		java_DTO_map.put("totalCurrentStock".toLowerCase(), "totalCurrentStock".toLowerCase());
		java_DTO_map.put("expiryDateList".toLowerCase(), "expiryDateList".toLowerCase());
		java_DTO_map.put("currentStockList".toLowerCase(), "currentStockList".toLowerCase());
		java_DTO_map.put("quantity".toLowerCase(), "quantity".toLowerCase());
		java_DTO_map.put("remarks".toLowerCase(), "remarks".toLowerCase());
		java_DTO_map.put("unit".toLowerCase(), "unit".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("reagent_requisition_lot_id".toLowerCase(), "reagentRequisitionLotId".toLowerCase());
		java_SQL_map.put("department_cat".toLowerCase(), "departmentCat".toLowerCase());
		java_SQL_map.put("medical_reagent_name_type".toLowerCase(), "medicalReagentNameType".toLowerCase());
		java_SQL_map.put("total_current_stock".toLowerCase(), "totalCurrentStock".toLowerCase());
		java_SQL_map.put("expiry_date_list".toLowerCase(), "expiryDateList".toLowerCase());
		java_SQL_map.put("current_stock_list".toLowerCase(), "currentStockList".toLowerCase());
		java_SQL_map.put("quantity".toLowerCase(), "quantity".toLowerCase());
		java_SQL_map.put("remarks".toLowerCase(), "remarks".toLowerCase());
		java_SQL_map.put("unit".toLowerCase(), "unit".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Reagent Requisition Lot Id".toLowerCase(), "reagentRequisitionLotId".toLowerCase());
		java_Text_map.put("Department".toLowerCase(), "departmentCat".toLowerCase());
		java_Text_map.put("Medical Reagent Name".toLowerCase(), "medicalReagentNameType".toLowerCase());
		java_Text_map.put("Total Current Stock".toLowerCase(), "totalCurrentStock".toLowerCase());
		java_Text_map.put("Expiry Date List".toLowerCase(), "expiryDateList".toLowerCase());
		java_Text_map.put("Current Stock List".toLowerCase(), "currentStockList".toLowerCase());
		java_Text_map.put("Quantity".toLowerCase(), "quantity".toLowerCase());
		java_Text_map.put("Remarks".toLowerCase(), "remarks".toLowerCase());
		java_Text_map.put("Unit".toLowerCase(), "unit".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}