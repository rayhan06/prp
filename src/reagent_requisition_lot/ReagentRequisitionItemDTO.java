package reagent_requisition_lot;
import java.util.*; 
import util.*; 


public class ReagentRequisitionItemDTO extends CommonDTO
{

	public long reagentRequisitionLotId = -1;
	public int departmentCat = -1;
	public long medicalReagentNameType = -1;
	public int totalCurrentStock = -1;
    public String expiryDateList = "";
    public String currentStockList = "";
	public int quantity = -1;
    public String remarks = "";
    public String unit = "";
	
	public List<ReagentRequisitionItemDTO> reagentRequisitionItemDTOList = new ArrayList<>();
	
    @Override
	public String toString() {
            return "$ReagentRequisitionItemDTO[" +
            " iD = " + iD +
            " reagentRequisitionLotId = " + reagentRequisitionLotId +
            " departmentCat = " + departmentCat +
            " medicalReagentNameType = " + medicalReagentNameType +
            " totalCurrentStock = " + totalCurrentStock +
            " expiryDateList = " + expiryDateList +
            " currentStockList = " + currentStockList +
            " quantity = " + quantity +
            " remarks = " + remarks +
            " unit = " + unit +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}