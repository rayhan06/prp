package reagent_requisition_lot;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;
import sessionmanager.SessionConstants;
import java.sql.SQLException;
import common.ConnectionAndStatementUtil;


import org.apache.log4j.Logger;

import dbm.*;

import repository.RepositoryManager;

import util.*;
import pb.*;
import user.UserDTO;

public class ReagentRequisitionItemDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());

	public ReagentRequisitionItemDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";
		commonMaps = new ReagentRequisitionItemMAPS(tableName);
		columnNames = new String[] 
		{
			"ID",
			"reagent_requisition_lot_id",
			"department_cat",
			"medical_reagent_name_type",
			"total_current_stock",
			"expiry_date_list",
			"current_stock_list",
			"quantity",
			"remarks",
			"unit",
			"isDeleted",
			"lastModificationTime"
		};
	}
	
	public ReagentRequisitionItemDAO()
	{
		this("reagent_requisition_item");		
	}
	
	public void setSearchColumn(ReagentRequisitionItemDTO reagentrequisitionitemDTO)
	{
		}
	
	public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException
	{
		ReagentRequisitionItemDTO reagentrequisitionitemDTO = (ReagentRequisitionItemDTO)commonDTO;
		int index = 1;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(reagentrequisitionitemDTO);
		if(isInsert)
		{
			ps.setObject(index++,reagentrequisitionitemDTO.iD);
		}
		ps.setObject(index++,reagentrequisitionitemDTO.reagentRequisitionLotId);
		ps.setObject(index++,reagentrequisitionitemDTO.departmentCat);
		ps.setObject(index++,reagentrequisitionitemDTO.medicalReagentNameType);
		ps.setObject(index++,reagentrequisitionitemDTO.totalCurrentStock);
		ps.setObject(index++,reagentrequisitionitemDTO.expiryDateList);
		ps.setObject(index++,reagentrequisitionitemDTO.currentStockList);
		ps.setObject(index++,reagentrequisitionitemDTO.quantity);
		ps.setObject(index++,reagentrequisitionitemDTO.remarks);
		ps.setObject(index++,reagentrequisitionitemDTO.unit);
		if(isInsert)
		{
			ps.setObject(index++, 0);
			ps.setObject(index++, lastModificationTime);
		}
	}
	
	public ReagentRequisitionItemDTO build(ResultSet rs)
	{
		try
		{
			ReagentRequisitionItemDTO reagentrequisitionitemDTO = new ReagentRequisitionItemDTO();
			reagentrequisitionitemDTO.iD = rs.getLong("ID");
			reagentrequisitionitemDTO.reagentRequisitionLotId = rs.getLong("reagent_requisition_lot_id");
			reagentrequisitionitemDTO.departmentCat = rs.getInt("department_cat");
			reagentrequisitionitemDTO.medicalReagentNameType = rs.getLong("medical_reagent_name_type");
			reagentrequisitionitemDTO.totalCurrentStock = rs.getInt("total_current_stock");
			reagentrequisitionitemDTO.expiryDateList = rs.getString("expiry_date_list");
			reagentrequisitionitemDTO.currentStockList = rs.getString("current_stock_list");
			reagentrequisitionitemDTO.quantity = rs.getInt("quantity");
			reagentrequisitionitemDTO.remarks = rs.getString("remarks");
			reagentrequisitionitemDTO.unit = rs.getString("unit");
			reagentrequisitionitemDTO.isDeleted = rs.getInt("isDeleted");
			reagentrequisitionitemDTO.lastModificationTime = rs.getLong("lastModificationTime");
			return reagentrequisitionitemDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
	
	
	public List<ReagentRequisitionItemDTO> getReagentRequisitionItemDTOListByReagentRequisitionLotID(long reagentRequisitionLotID) throws Exception
	{
		String sql = "SELECT * FROM reagent_requisition_item where isDeleted=0 and reagent_requisition_lot_id="+reagentRequisitionLotID+" order by reagent_requisition_item.lastModificationTime";
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);		
	}

	public ReagentRequisitionItemDTO getDTOByID (long id)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID = " + id;
		ReagentRequisitionItemDTO reagentrequisitionitemDTO = ConnectionAndStatementUtil.getT(sql,this::build);
		return reagentrequisitionitemDTO;
	}

	
	public List<ReagentRequisitionItemDTO> getDTOs(Collection recordIDs)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID IN ( ";
		for(int i = 0;i<recordIDs.size();i++){
			if(i!=0){
				sql+=",";
			}
			sql+=((ArrayList)recordIDs).get(i);
		}
		sql+=")  order by lastModificationTime desc";
		printSql(sql);
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);		
	}
	
	public List<ReagentRequisitionItemDTO> getAllReagentRequisitionItem (boolean isFirstReload)
    {
		String sql = "SELECT * FROM " + tableName + " WHERE ";
		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by " + tableName + ".lastModificationTime desc";
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);	
    }

	
	public List<ReagentRequisitionItemDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO)
	{
		return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
	}

	
	public List<ReagentRequisitionItemDTO> getDTOs(Hashtable  p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
			String filter, boolean tableHasJobCat)
	{
		String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat);
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);			
	}
				
}
	