package reagent_requisition_lot;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class Reagent_requisition_lotRepository implements Repository {
	Reagent_requisition_lotDAO reagent_requisition_lotDAO = null;
	
	public void setDAO(Reagent_requisition_lotDAO reagent_requisition_lotDAO)
	{
		this.reagent_requisition_lotDAO = reagent_requisition_lotDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Reagent_requisition_lotRepository.class);
	Map<Long, Reagent_requisition_lotDTO>mapOfReagent_requisition_lotDTOToiD;
	Map<Long, Set<Reagent_requisition_lotDTO> >mapOfReagent_requisition_lotDTOTorequisitionToOrganogramId;
	Map<String, Set<Reagent_requisition_lotDTO> >mapOfReagent_requisition_lotDTOTosubject;
	Map<String, Set<Reagent_requisition_lotDTO> >mapOfReagent_requisition_lotDTOTodescription;
	Map<Long, Set<Reagent_requisition_lotDTO> >mapOfReagent_requisition_lotDTOToinsertedByUserId;
	Map<Long, Set<Reagent_requisition_lotDTO> >mapOfReagent_requisition_lotDTOToinsertedByOrganogramId;
	Map<Integer, Set<Reagent_requisition_lotDTO> >mapOfReagent_requisition_lotDTOTojobCat;
	Map<Long, Set<Reagent_requisition_lotDTO> >mapOfReagent_requisition_lotDTOToinsertionDate;
	Map<String, Set<Reagent_requisition_lotDTO> >mapOfReagent_requisition_lotDTOTosearchColumn;
	Map<Long, Set<Reagent_requisition_lotDTO> >mapOfReagent_requisition_lotDTOTolastModificationTime;


	static Reagent_requisition_lotRepository instance = null;  
	private Reagent_requisition_lotRepository(){
		mapOfReagent_requisition_lotDTOToiD = new ConcurrentHashMap<>();
		mapOfReagent_requisition_lotDTOTorequisitionToOrganogramId = new ConcurrentHashMap<>();
		mapOfReagent_requisition_lotDTOTosubject = new ConcurrentHashMap<>();
		mapOfReagent_requisition_lotDTOTodescription = new ConcurrentHashMap<>();
		mapOfReagent_requisition_lotDTOToinsertedByUserId = new ConcurrentHashMap<>();
		mapOfReagent_requisition_lotDTOToinsertedByOrganogramId = new ConcurrentHashMap<>();
		mapOfReagent_requisition_lotDTOTojobCat = new ConcurrentHashMap<>();
		mapOfReagent_requisition_lotDTOToinsertionDate = new ConcurrentHashMap<>();
		mapOfReagent_requisition_lotDTOTosearchColumn = new ConcurrentHashMap<>();
		mapOfReagent_requisition_lotDTOTolastModificationTime = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Reagent_requisition_lotRepository getInstance(){
		if (instance == null){
			instance = new Reagent_requisition_lotRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(reagent_requisition_lotDAO == null)
		{
			return;
		}
		try {
			List<Reagent_requisition_lotDTO> reagent_requisition_lotDTOs = reagent_requisition_lotDAO.getAllReagent_requisition_lot(reloadAll);
			for(Reagent_requisition_lotDTO reagent_requisition_lotDTO : reagent_requisition_lotDTOs) {
				Reagent_requisition_lotDTO oldReagent_requisition_lotDTO = getReagent_requisition_lotDTOByID(reagent_requisition_lotDTO.iD);
				if( oldReagent_requisition_lotDTO != null ) {
					mapOfReagent_requisition_lotDTOToiD.remove(oldReagent_requisition_lotDTO.iD);
				
					if(mapOfReagent_requisition_lotDTOTorequisitionToOrganogramId.containsKey(oldReagent_requisition_lotDTO.requisitionToOrganogramId)) {
						mapOfReagent_requisition_lotDTOTorequisitionToOrganogramId.get(oldReagent_requisition_lotDTO.requisitionToOrganogramId).remove(oldReagent_requisition_lotDTO);
					}
					if(mapOfReagent_requisition_lotDTOTorequisitionToOrganogramId.get(oldReagent_requisition_lotDTO.requisitionToOrganogramId).isEmpty()) {
						mapOfReagent_requisition_lotDTOTorequisitionToOrganogramId.remove(oldReagent_requisition_lotDTO.requisitionToOrganogramId);
					}
					
					if(mapOfReagent_requisition_lotDTOTosubject.containsKey(oldReagent_requisition_lotDTO.subject)) {
						mapOfReagent_requisition_lotDTOTosubject.get(oldReagent_requisition_lotDTO.subject).remove(oldReagent_requisition_lotDTO);
					}
					if(mapOfReagent_requisition_lotDTOTosubject.get(oldReagent_requisition_lotDTO.subject).isEmpty()) {
						mapOfReagent_requisition_lotDTOTosubject.remove(oldReagent_requisition_lotDTO.subject);
					}
					
					if(mapOfReagent_requisition_lotDTOTodescription.containsKey(oldReagent_requisition_lotDTO.description)) {
						mapOfReagent_requisition_lotDTOTodescription.get(oldReagent_requisition_lotDTO.description).remove(oldReagent_requisition_lotDTO);
					}
					if(mapOfReagent_requisition_lotDTOTodescription.get(oldReagent_requisition_lotDTO.description).isEmpty()) {
						mapOfReagent_requisition_lotDTOTodescription.remove(oldReagent_requisition_lotDTO.description);
					}
					
					if(mapOfReagent_requisition_lotDTOToinsertedByUserId.containsKey(oldReagent_requisition_lotDTO.insertedByUserId)) {
						mapOfReagent_requisition_lotDTOToinsertedByUserId.get(oldReagent_requisition_lotDTO.insertedByUserId).remove(oldReagent_requisition_lotDTO);
					}
					if(mapOfReagent_requisition_lotDTOToinsertedByUserId.get(oldReagent_requisition_lotDTO.insertedByUserId).isEmpty()) {
						mapOfReagent_requisition_lotDTOToinsertedByUserId.remove(oldReagent_requisition_lotDTO.insertedByUserId);
					}
					
					if(mapOfReagent_requisition_lotDTOToinsertedByOrganogramId.containsKey(oldReagent_requisition_lotDTO.insertedByOrganogramId)) {
						mapOfReagent_requisition_lotDTOToinsertedByOrganogramId.get(oldReagent_requisition_lotDTO.insertedByOrganogramId).remove(oldReagent_requisition_lotDTO);
					}
					if(mapOfReagent_requisition_lotDTOToinsertedByOrganogramId.get(oldReagent_requisition_lotDTO.insertedByOrganogramId).isEmpty()) {
						mapOfReagent_requisition_lotDTOToinsertedByOrganogramId.remove(oldReagent_requisition_lotDTO.insertedByOrganogramId);
					}
					
					if(mapOfReagent_requisition_lotDTOTojobCat.containsKey(oldReagent_requisition_lotDTO.jobCat)) {
						mapOfReagent_requisition_lotDTOTojobCat.get(oldReagent_requisition_lotDTO.jobCat).remove(oldReagent_requisition_lotDTO);
					}
					if(mapOfReagent_requisition_lotDTOTojobCat.get(oldReagent_requisition_lotDTO.jobCat).isEmpty()) {
						mapOfReagent_requisition_lotDTOTojobCat.remove(oldReagent_requisition_lotDTO.jobCat);
					}
					
					if(mapOfReagent_requisition_lotDTOToinsertionDate.containsKey(oldReagent_requisition_lotDTO.insertionDate)) {
						mapOfReagent_requisition_lotDTOToinsertionDate.get(oldReagent_requisition_lotDTO.insertionDate).remove(oldReagent_requisition_lotDTO);
					}
					if(mapOfReagent_requisition_lotDTOToinsertionDate.get(oldReagent_requisition_lotDTO.insertionDate).isEmpty()) {
						mapOfReagent_requisition_lotDTOToinsertionDate.remove(oldReagent_requisition_lotDTO.insertionDate);
					}
					
					if(mapOfReagent_requisition_lotDTOTosearchColumn.containsKey(oldReagent_requisition_lotDTO.searchColumn)) {
						mapOfReagent_requisition_lotDTOTosearchColumn.get(oldReagent_requisition_lotDTO.searchColumn).remove(oldReagent_requisition_lotDTO);
					}
					if(mapOfReagent_requisition_lotDTOTosearchColumn.get(oldReagent_requisition_lotDTO.searchColumn).isEmpty()) {
						mapOfReagent_requisition_lotDTOTosearchColumn.remove(oldReagent_requisition_lotDTO.searchColumn);
					}
					
					if(mapOfReagent_requisition_lotDTOTolastModificationTime.containsKey(oldReagent_requisition_lotDTO.lastModificationTime)) {
						mapOfReagent_requisition_lotDTOTolastModificationTime.get(oldReagent_requisition_lotDTO.lastModificationTime).remove(oldReagent_requisition_lotDTO);
					}
					if(mapOfReagent_requisition_lotDTOTolastModificationTime.get(oldReagent_requisition_lotDTO.lastModificationTime).isEmpty()) {
						mapOfReagent_requisition_lotDTOTolastModificationTime.remove(oldReagent_requisition_lotDTO.lastModificationTime);
					}
					
					
				}
				if(reagent_requisition_lotDTO.isDeleted == 0) 
				{
					
					mapOfReagent_requisition_lotDTOToiD.put(reagent_requisition_lotDTO.iD, reagent_requisition_lotDTO);
				
					if( ! mapOfReagent_requisition_lotDTOTorequisitionToOrganogramId.containsKey(reagent_requisition_lotDTO.requisitionToOrganogramId)) {
						mapOfReagent_requisition_lotDTOTorequisitionToOrganogramId.put(reagent_requisition_lotDTO.requisitionToOrganogramId, new HashSet<>());
					}
					mapOfReagent_requisition_lotDTOTorequisitionToOrganogramId.get(reagent_requisition_lotDTO.requisitionToOrganogramId).add(reagent_requisition_lotDTO);
					
					if( ! mapOfReagent_requisition_lotDTOTosubject.containsKey(reagent_requisition_lotDTO.subject)) {
						mapOfReagent_requisition_lotDTOTosubject.put(reagent_requisition_lotDTO.subject, new HashSet<>());
					}
					mapOfReagent_requisition_lotDTOTosubject.get(reagent_requisition_lotDTO.subject).add(reagent_requisition_lotDTO);
					
					if( ! mapOfReagent_requisition_lotDTOTodescription.containsKey(reagent_requisition_lotDTO.description)) {
						mapOfReagent_requisition_lotDTOTodescription.put(reagent_requisition_lotDTO.description, new HashSet<>());
					}
					mapOfReagent_requisition_lotDTOTodescription.get(reagent_requisition_lotDTO.description).add(reagent_requisition_lotDTO);
					
					if( ! mapOfReagent_requisition_lotDTOToinsertedByUserId.containsKey(reagent_requisition_lotDTO.insertedByUserId)) {
						mapOfReagent_requisition_lotDTOToinsertedByUserId.put(reagent_requisition_lotDTO.insertedByUserId, new HashSet<>());
					}
					mapOfReagent_requisition_lotDTOToinsertedByUserId.get(reagent_requisition_lotDTO.insertedByUserId).add(reagent_requisition_lotDTO);
					
					if( ! mapOfReagent_requisition_lotDTOToinsertedByOrganogramId.containsKey(reagent_requisition_lotDTO.insertedByOrganogramId)) {
						mapOfReagent_requisition_lotDTOToinsertedByOrganogramId.put(reagent_requisition_lotDTO.insertedByOrganogramId, new HashSet<>());
					}
					mapOfReagent_requisition_lotDTOToinsertedByOrganogramId.get(reagent_requisition_lotDTO.insertedByOrganogramId).add(reagent_requisition_lotDTO);
					
					if( ! mapOfReagent_requisition_lotDTOTojobCat.containsKey(reagent_requisition_lotDTO.jobCat)) {
						mapOfReagent_requisition_lotDTOTojobCat.put(reagent_requisition_lotDTO.jobCat, new HashSet<>());
					}
					mapOfReagent_requisition_lotDTOTojobCat.get(reagent_requisition_lotDTO.jobCat).add(reagent_requisition_lotDTO);
					
					if( ! mapOfReagent_requisition_lotDTOToinsertionDate.containsKey(reagent_requisition_lotDTO.insertionDate)) {
						mapOfReagent_requisition_lotDTOToinsertionDate.put(reagent_requisition_lotDTO.insertionDate, new HashSet<>());
					}
					mapOfReagent_requisition_lotDTOToinsertionDate.get(reagent_requisition_lotDTO.insertionDate).add(reagent_requisition_lotDTO);
					
					if( ! mapOfReagent_requisition_lotDTOTosearchColumn.containsKey(reagent_requisition_lotDTO.searchColumn)) {
						mapOfReagent_requisition_lotDTOTosearchColumn.put(reagent_requisition_lotDTO.searchColumn, new HashSet<>());
					}
					mapOfReagent_requisition_lotDTOTosearchColumn.get(reagent_requisition_lotDTO.searchColumn).add(reagent_requisition_lotDTO);
					
					if( ! mapOfReagent_requisition_lotDTOTolastModificationTime.containsKey(reagent_requisition_lotDTO.lastModificationTime)) {
						mapOfReagent_requisition_lotDTOTolastModificationTime.put(reagent_requisition_lotDTO.lastModificationTime, new HashSet<>());
					}
					mapOfReagent_requisition_lotDTOTolastModificationTime.get(reagent_requisition_lotDTO.lastModificationTime).add(reagent_requisition_lotDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Reagent_requisition_lotDTO> getReagent_requisition_lotList() {
		List <Reagent_requisition_lotDTO> reagent_requisition_lots = new ArrayList<Reagent_requisition_lotDTO>(this.mapOfReagent_requisition_lotDTOToiD.values());
		return reagent_requisition_lots;
	}
	
	
	public Reagent_requisition_lotDTO getReagent_requisition_lotDTOByID( long ID){
		return mapOfReagent_requisition_lotDTOToiD.get(ID);
	}
	
	
	public List<Reagent_requisition_lotDTO> getReagent_requisition_lotDTOByrequisition_to_organogram_id(long requisition_to_organogram_id) {
		return new ArrayList<>( mapOfReagent_requisition_lotDTOTorequisitionToOrganogramId.getOrDefault(requisition_to_organogram_id,new HashSet<>()));
	}
	
	
	public List<Reagent_requisition_lotDTO> getReagent_requisition_lotDTOBysubject(String subject) {
		return new ArrayList<>( mapOfReagent_requisition_lotDTOTosubject.getOrDefault(subject,new HashSet<>()));
	}
	
	
	public List<Reagent_requisition_lotDTO> getReagent_requisition_lotDTOBydescription(String description) {
		return new ArrayList<>( mapOfReagent_requisition_lotDTOTodescription.getOrDefault(description,new HashSet<>()));
	}
	
	
	public List<Reagent_requisition_lotDTO> getReagent_requisition_lotDTOByinserted_by_user_id(long inserted_by_user_id) {
		return new ArrayList<>( mapOfReagent_requisition_lotDTOToinsertedByUserId.getOrDefault(inserted_by_user_id,new HashSet<>()));
	}
	
	
	public List<Reagent_requisition_lotDTO> getReagent_requisition_lotDTOByinserted_by_organogram_id(long inserted_by_organogram_id) {
		return new ArrayList<>( mapOfReagent_requisition_lotDTOToinsertedByOrganogramId.getOrDefault(inserted_by_organogram_id,new HashSet<>()));
	}
	
	
	public List<Reagent_requisition_lotDTO> getReagent_requisition_lotDTOByjob_cat(int job_cat) {
		return new ArrayList<>( mapOfReagent_requisition_lotDTOTojobCat.getOrDefault(job_cat,new HashSet<>()));
	}
	
	
	public List<Reagent_requisition_lotDTO> getReagent_requisition_lotDTOByinsertion_date(long insertion_date) {
		return new ArrayList<>( mapOfReagent_requisition_lotDTOToinsertionDate.getOrDefault(insertion_date,new HashSet<>()));
	}
	
	
	public List<Reagent_requisition_lotDTO> getReagent_requisition_lotDTOBysearch_column(String search_column) {
		return new ArrayList<>( mapOfReagent_requisition_lotDTOTosearchColumn.getOrDefault(search_column,new HashSet<>()));
	}
	
	
	public List<Reagent_requisition_lotDTO> getReagent_requisition_lotDTOBylastModificationTime(long lastModificationTime) {
		return new ArrayList<>( mapOfReagent_requisition_lotDTOTolastModificationTime.getOrDefault(lastModificationTime,new HashSet<>()));
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "reagent_requisition_lot";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


