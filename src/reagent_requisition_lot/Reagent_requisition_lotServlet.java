package reagent_requisition_lot;

import java.io.IOException;
import java.io.*;
import java.text.SimpleDateFormat;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import org.apache.log4j.Logger;

import login.LoginDTO;
import medical_requisition_lot.Medical_requisition_lotDTO;
import permission.MenuConstants;
import role.PermissionRepository;


import sessionmanager.SessionConstants;

import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;
import util.CommonRequestHandler;
import util.RecordNavigationManager4;

import java.util.*;
import javax.servlet.http.*;
import java.util.UUID;





import geolocation.GeoLocationDAO2;
import java.util.StringTokenizer;

import com.google.gson.Gson;

import pb.*;
import pbReport.*;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;

import workflow.WorkflowController;
import approval_execution_table.*;
import pb_notifications.Pb_notificationsDAO;



/**
 * Servlet implementation class Reagent_requisition_lotServlet
 */
@WebServlet("/Reagent_requisition_lotServlet")
@MultipartConfig
public class Reagent_requisition_lotServlet extends HttpServlet 
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Reagent_requisition_lotServlet.class);

    String tableName = "reagent_requisition_lot";

	Reagent_requisition_lotDAO reagent_requisition_lotDAO;
	CommonRequestHandler commonRequestHandler;
	ReagentRequisitionItemDAO reagentRequisitionItemDAO;
    private final Gson gson = new Gson();
    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Reagent_requisition_lotServlet() 
	{
        super();
    	try
    	{
			reagent_requisition_lotDAO = new Reagent_requisition_lotDAO(tableName);
			reagentRequisitionItemDAO = new ReagentRequisitionItemDAO("reagent_requisition_item");
			commonRequestHandler = new CommonRequestHandler(reagent_requisition_lotDAO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }   

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		System.out.println("In doget request = " + request);
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		try
		{
			String actionType = request.getParameter("actionType");
			if(actionType.equals("getAddPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.REAGENT_REQUISITION_LOT_ADD))
				{
					commonRequestHandler.getAddPage(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("copy"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.REAGENT_REQUISITION_LOT_ADD))
				{
					getFormattedAddPage(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			if(actionType.equals("getReagentsByDept"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.REAGENT_REQUISITION_LOT_ADD))
				{
					int dept = Integer.parseInt(request.getParameter("dept"));
					String options = CommonDAO.getReagentsByDept(-1, dept);
					response.getWriter().write(options);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getEditPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.REAGENT_REQUISITION_LOT_UPDATE))
				{
					getReagent_requisition_lot(request, response);					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}						
			}
			else if(actionType.equals("getURL"))
			{
				String URL = request.getParameter("URL");
				System.out.println("URL = " + URL);
				response.sendRedirect(URL);			
			}			
			else if(actionType.equals("search"))
			{
				System.out.println("search requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.REAGENT_REQUISITION_LOT_SEARCH))
				{
					if(isPermanentTable)
					{
						String filter = request.getParameter("filter");
						System.out.println("filter = " + filter);
						if(filter!=null)
						{
							filter = ""; //shouldn't be directly used, rather manipulate it.
							searchReagent_requisition_lot(request, response, isPermanentTable, filter);
						}
						else
						{
							searchReagent_requisition_lot(request, response, isPermanentTable, "");
						}
					}					
					else
					{
						//searchReagent_requisition_lot(request, response, tempTableName, isPermanentTable);
					}
				}			
			}
			else if(actionType.equals("getApprovalPage"))
			{
				System.out.println("Reagent_requisition_lot getApprovalPage requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.REAGENT_REQUISITION_LOT_APPROVE))
				{
					searchReagent_requisition_lot(request, response, false, "");
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}				
			}
			else if(actionType.equals("viewApprovalNotification"))
			{
				System.out.println("view requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.REAGENT_REQUISITION_LOT_APPROVE))
				{
					commonRequestHandler.viewApprovalNotification(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}				
			}
			else if(actionType.equals("view"))
			{
				System.out.println("view requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.REAGENT_REQUISITION_LOT_SEARCH))
				{
					commonRequestHandler.view(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else
			{
				request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}


	
	

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		// TODO Auto-generated method stub
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		System.out.println("doPost");
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		
		try
		{
			String actionType = request.getParameter("actionType");
			System.out.println("actionType = " + actionType);
			if(actionType.equals("add"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.REAGENT_REQUISITION_LOT_ADD))
				{
					System.out.println("going to  addReagent_requisition_lot ");
					addReagent_requisition_lot(request, response, true, userDTO, true);
				}
				else
				{
					System.out.println("Not going to  addReagent_requisition_lot ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}

			else if(actionType.equals("SendToApprovalPath"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.REAGENT_REQUISITION_LOT_APPROVE))
				{
					commonRequestHandler.sendToApprovalPath(request, response, userDTO);
				}
				else
				{
					System.out.println("Not going to SendToApprovalPath ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}
			else if(actionType.equals("approve"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.REAGENT_REQUISITION_LOT_ADD))
				{					
					commonRequestHandler.approve(request, response, true, userDTO, true);
				}
				else
				{
					System.out.println("Not going to  addReagent_requisition_lot ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else if(actionType.equals("reject"))
			{
				System.out.println("trying to approve");
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.REAGENT_REQUISITION_LOT_APPROVE))
				{					
					commonRequestHandler.approve(request, response, true, userDTO, false);
				}
				else
				{
					System.out.println("Not going to  addReagent_requisition_lot ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}				
			}
			else if(actionType.equals("terminate"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.REAGENT_REQUISITION_LOT_ADD))
				{
					commonRequestHandler.terminate(request, response, userDTO);
				}
				else
				{
					System.out.println("Not going to  addReagent_requisition_lot ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}
			else if(actionType.equals("skipStep"))
			{
				
				System.out.println("skipStep");
				commonRequestHandler.skipStep(request, response, userDTO);									
			}
			else if(actionType.equals("getDTO"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.REAGENT_REQUISITION_LOT_ADD))
				{
					getDTO(request, response);					
				}
				else
				{
					System.out.println("Not going to  addReagent_requisition_lot ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else if(actionType.equals("edit"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.REAGENT_REQUISITION_LOT_UPDATE))
				{					
					addReagent_requisition_lot(request, response, false, userDTO, isPermanentTable);					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("delete"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.REAGENT_REQUISITION_LOT_UPDATE))
				{
					commonRequestHandler.delete(request, response, userDTO);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}	
			else if(actionType.equals("search"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.REAGENT_REQUISITION_LOT_SEARCH))
				{
					searchReagent_requisition_lot(request, response, true, "");
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getGeo"))
			{
				System.out.println("going to geoloc ");
				request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
			}
			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}
	
	private void getDTO(HttpServletRequest request, HttpServletResponse response) 
	{
		try 
		{
			System.out.println("In getDTO");
			Reagent_requisition_lotDTO reagent_requisition_lotDTO = reagent_requisition_lotDAO.getDTOByID(Long.parseLong(request.getParameter("ID")));
			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			
			String encoded = this.gson.toJson(reagent_requisition_lotDTO);
			System.out.println("json encoded data = " + encoded);
			out.print(encoded);
			out.flush();
		}
		catch (NumberFormatException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	private void addReagent_requisition_lot(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, boolean isPermanentTable) throws IOException 
	{
		// TODO Auto-generated method stub
		try 
		{
			request.setAttribute("failureMessage", "");
			System.out.println("%%%% addReagent_requisition_lot");
			String path = getServletContext().getRealPath("/img2/");
			Reagent_requisition_lotDTO reagent_requisition_lotDTO;
			SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
						
			if(addFlag == true)
			{
				reagent_requisition_lotDTO = new Reagent_requisition_lotDTO();
			}
			else
			{
				reagent_requisition_lotDTO = reagent_requisition_lotDAO.getDTOByID(Long.parseLong(request.getParameter("iD")));
			}
			
			String Value = "";

			Value = request.getParameter("requisitionToOrganogramId");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("requisitionToOrganogramId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				reagent_requisition_lotDTO.requisitionToOrganogramId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("subject");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("subject = " + Value);
			if(Value != null)
			{
				reagent_requisition_lotDTO.subject = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("description");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("description = " + Value);
			if(Value != null)
			{
				reagent_requisition_lotDTO.description = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			if(addFlag)
			{
				reagent_requisition_lotDTO.insertedByUserId = userDTO.ID;
			}


			if(addFlag)
			{
				reagent_requisition_lotDTO.insertedByOrganogramId = userDTO.organogramID;
			}


			Value = request.getParameter("jobCat");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("jobCat = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				reagent_requisition_lotDTO.jobCat = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			if(addFlag)
			{
				Calendar c = Calendar.getInstance();
				c.set(Calendar.HOUR_OF_DAY, 0);
				c.set(Calendar.MINUTE, 0);
				c.set(Calendar.SECOND, 0);
				c.set(Calendar.MILLISECOND, 0);
				
				reagent_requisition_lotDTO.insertionDate = c.getTimeInMillis();
			}			


			System.out.println("Done adding  addReagent_requisition_lot dto = " + reagent_requisition_lotDTO);
			long returnedID = -1;
			
			if(isPermanentTable == false) //add new row for validation and make the old row outdated
			{
				reagent_requisition_lotDAO.setIsDeleted(reagent_requisition_lotDTO.iD, CommonDTO.OUTDATED);
				returnedID = reagent_requisition_lotDAO.add(reagent_requisition_lotDTO);
				reagent_requisition_lotDAO.setIsDeleted(returnedID, CommonDTO.WAITING_FOR_APPROVAL);
			}
			else if(addFlag == true)
			{
				returnedID = reagent_requisition_lotDAO.manageWriteOperations(reagent_requisition_lotDTO, SessionConstants.INSERT, -1, userDTO);
			}
			else
			{				
				returnedID = reagent_requisition_lotDAO.manageWriteOperations(reagent_requisition_lotDTO, SessionConstants.UPDATE, -1, userDTO);											
			}
			
			
			
			
			
			List<ReagentRequisitionItemDTO> reagentRequisitionItemDTOList = createReagentRequisitionItemDTOListByRequest(request);			
			if(addFlag == true || isPermanentTable == false) //add or validate
			{
				if(reagentRequisitionItemDTOList != null)
				{				
					for(ReagentRequisitionItemDTO reagentRequisitionItemDTO: reagentRequisitionItemDTOList)
					{
						reagentRequisitionItemDTO.reagentRequisitionLotId = reagent_requisition_lotDTO.iD; 
						reagentRequisitionItemDAO.add(reagentRequisitionItemDTO);
					}
				}
			
			}
			else
			{
				List<Long> childIdsFromRequest = reagentRequisitionItemDAO.getChildIdsFromRequest(request, "reagentRequisitionItem");
				//delete the removed children
				reagentRequisitionItemDAO.deleteChildrenNotInList("reagent_requisition_lot", "reagent_requisition_item", reagent_requisition_lotDTO.iD, childIdsFromRequest);
				List<Long> childIDsInDatabase = reagentRequisitionItemDAO.getChilIds("reagent_requisition_lot", "reagent_requisition_item", reagent_requisition_lotDTO.iD);
				
				
				if(childIdsFromRequest != null)
				{
					for(int i = 0; i < childIdsFromRequest.size(); i ++)
					{
						Long childIDFromRequest = childIdsFromRequest.get(i);
						if(childIDsInDatabase.contains(childIDFromRequest))
						{
							ReagentRequisitionItemDTO reagentRequisitionItemDTO =  createReagentRequisitionItemDTOByRequestAndIndex(request, false, i);
							reagentRequisitionItemDTO.reagentRequisitionLotId = reagent_requisition_lotDTO.iD; 
							reagentRequisitionItemDAO.update(reagentRequisitionItemDTO);
						}
						else
						{
							ReagentRequisitionItemDTO reagentRequisitionItemDTO =  createReagentRequisitionItemDTOByRequestAndIndex(request, true, i);
							reagentRequisitionItemDTO.reagentRequisitionLotId = reagent_requisition_lotDTO.iD; 
							reagentRequisitionItemDAO.add(reagentRequisitionItemDTO);
						}
					}
				}
				else
				{
					reagentRequisitionItemDAO.deleteChildrenByParent(reagent_requisition_lotDTO.iD, "reagent_requisition_lot_id");
				}
				
			}
			
			
			
			
			
			if(isPermanentTable)
			{
				String inPlaceSubmit = request.getParameter("inplacesubmit");
				
				if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
				{
					getReagent_requisition_lot(request, response, returnedID);
				}
				else
				{
					response.sendRedirect("Reagent_requisition_lotServlet?actionType=search");
				}
			}
			else
			{
				commonRequestHandler.validate(reagent_requisition_lotDAO.getDTOByID(returnedID), request, response, userDTO);
			}
					
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	



	private List<ReagentRequisitionItemDTO> createReagentRequisitionItemDTOListByRequest(HttpServletRequest request) throws Exception{ 
		List<ReagentRequisitionItemDTO> reagentRequisitionItemDTOList = new ArrayList<ReagentRequisitionItemDTO>();
		if(request.getParameterValues("reagentRequisitionItem.iD") != null) 
		{
			int reagentRequisitionItemItemNo = request.getParameterValues("reagentRequisitionItem.iD").length;
			
			
			for(int index=0;index<reagentRequisitionItemItemNo;index++){
				ReagentRequisitionItemDTO reagentRequisitionItemDTO = createReagentRequisitionItemDTOByRequestAndIndex(request,true,index);
				reagentRequisitionItemDTOList.add(reagentRequisitionItemDTO);
			}
			
			return reagentRequisitionItemDTOList;
		}
		return null;
	}
	
	
	private ReagentRequisitionItemDTO createReagentRequisitionItemDTOByRequestAndIndex(HttpServletRequest request, boolean addFlag,int index) throws Exception{
	
		ReagentRequisitionItemDTO reagentRequisitionItemDTO;
		if(addFlag == true )
		{
			reagentRequisitionItemDTO = new ReagentRequisitionItemDTO();
		}
		else
		{
			reagentRequisitionItemDTO = reagentRequisitionItemDAO.getDTOByID(Long.parseLong(request.getParameterValues("reagentRequisitionItem.iD")[index]));
		}
		String path = getServletContext().getRealPath("/img2/");
		SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
		
		
				
		String Value = "";
		Value = request.getParameterValues("reagentRequisitionItem.reagentRequisitionLotId")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		reagentRequisitionItemDTO.reagentRequisitionLotId = Long.parseLong(Value);
		Value = request.getParameterValues("reagentRequisitionItem.departmentCat")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		reagentRequisitionItemDTO.departmentCat = Integer.parseInt(Value);
		Value = request.getParameterValues("reagentRequisitionItem.medicalReagentNameType")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		reagentRequisitionItemDTO.medicalReagentNameType = Long.parseLong(Value);
		Value = request.getParameterValues("reagentRequisitionItem.totalCurrentStock")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		reagentRequisitionItemDTO.totalCurrentStock = Integer.parseInt(Value);
		Value = request.getParameterValues("reagentRequisitionItem.expiryDateList")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		reagentRequisitionItemDTO.expiryDateList = (Value);
		Value = request.getParameterValues("reagentRequisitionItem.currentStockList")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		reagentRequisitionItemDTO.currentStockList = (Value);
		Value = request.getParameterValues("reagentRequisitionItem.quantity")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		reagentRequisitionItemDTO.quantity = Integer.parseInt(Value);
		Value = request.getParameterValues("reagentRequisitionItem.remarks")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		reagentRequisitionItemDTO.remarks = (Value);
		Value = request.getParameterValues("reagentRequisitionItem.unit")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		reagentRequisitionItemDTO.unit = (Value);
		return reagentRequisitionItemDTO;
	
	}
	
	
	

	private void getReagent_requisition_lot(HttpServletRequest request, HttpServletResponse response, long id) throws ServletException, IOException
	{
		System.out.println("in getReagent_requisition_lot");
		Reagent_requisition_lotDTO reagent_requisition_lotDTO = null;
		try 
		{
			reagent_requisition_lotDTO = reagent_requisition_lotDAO.getDTOByID(id);
			boolean isPermanentTable = true;
			if(request.getParameter("isPermanentTable") != null)
			{
				isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
			}
			request.setAttribute("ID", reagent_requisition_lotDTO.iD);
			request.setAttribute("reagent_requisition_lotDTO",reagent_requisition_lotDTO);
			request.setAttribute("reagent_requisition_lotDAO",reagent_requisition_lotDAO);
			
			String URL= "";
			
			String inPlaceEdit = request.getParameter("inplaceedit");
			String inPlaceSubmit = request.getParameter("inplacesubmit");
			String getBodyOnly = request.getParameter("getBodyOnly");
			
			if(inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase(""))
			{
				URL = "reagent_requisition_lot/reagent_requisition_lotInPlaceEdit.jsp";	
				request.setAttribute("inplaceedit","");				
			}
			else if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
			{
				URL = "reagent_requisition_lot/reagent_requisition_lotSearchRow.jsp";
				request.setAttribute("inplacesubmit","");					
			}
			else
			{
				if(getBodyOnly  != null && !getBodyOnly.equalsIgnoreCase(""))
				{
					URL = "reagent_requisition_lot/reagent_requisition_lotEditBody.jsp?actionType=edit";
				}
				else
				{
					URL = "reagent_requisition_lot/reagent_requisition_lotEdit.jsp?actionType=edit";
				}				
			}
			
			RequestDispatcher rd = request.getRequestDispatcher(URL);
			rd.forward(request, response);
		}
		catch (NumberFormatException e) 
		{
			e.printStackTrace();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	public void getFormattedAddPage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String empId = request.getParameter("empId");
		if(empId == null){
			LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
			UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
			empId =String.valueOf(userDTO.employee_record_id);
		}
		logger.debug("EmpId : "+empId);
		request.setAttribute("empId", empId);
		
		long id = Long.parseLong(request.getParameter("ID"));
		
		
		Reagent_requisition_lotDTO reagent_requisition_lotDTO = reagent_requisition_lotDAO.getDTOByID(id);
		if(reagent_requisition_lotDTO != null)
		{
			request.setAttribute("ID", reagent_requisition_lotDTO.iD);
			request.setAttribute("reagent_requisition_lotDTO",reagent_requisition_lotDTO);
		}
		else
		{
			request.setAttribute("ID", -1L);
		}

		
		request.setAttribute("reagent_requisition_lotDAO",reagent_requisition_lotDAO);
		RequestDispatcher requestDispatcher = request.getRequestDispatcher(tableName + "/" + tableName + "Edit.jsp");
		requestDispatcher.forward(request, response);
	}
	
	
	private void getReagent_requisition_lot(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		getReagent_requisition_lot(request, response, Long.parseLong(request.getParameter("ID")));	
	}
	
	private void searchReagent_requisition_lot(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException
	{
		System.out.println("in  searchReagent_requisition_lot 1");
		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String ajax = request.getParameter("ajax");
		boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);
		
        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
			SessionConstants.NAV_REAGENT_REQUISITION_LOT,
			request,
			reagent_requisition_lotDAO,
			SessionConstants.VIEW_REAGENT_REQUISITION_LOT,
			SessionConstants.SEARCH_REAGENT_REQUISITION_LOT,
			tableName,
			isPermanent,
			userDTO,
			filter,
			true);
        try
        {
			System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        }
        catch(Exception e)
        {
			System.out.println("failed to dojob" + e);
        }

		request.setAttribute("reagent_requisition_lotDAO",reagent_requisition_lotDAO);
        RequestDispatcher rd;
        if(!isPermanent)
        {
        	if(hasAjax == false)
	        {
	        	System.out.println("Going to reagent_requisition_lot/reagent_requisition_lotApproval.jsp");
	        	rd = request.getRequestDispatcher("reagent_requisition_lot/reagent_requisition_lotApproval.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to reagent_requisition_lot/reagent_requisition_lotApprovalForm.jsp");
	        	rd = request.getRequestDispatcher("reagent_requisition_lot/reagent_requisition_lotApprovalForm.jsp");
	        }
        }
        else
        {
	        if(hasAjax == false)
	        {
	        	System.out.println("Going to reagent_requisition_lot/reagent_requisition_lotSearch.jsp");
	        	rd = request.getRequestDispatcher("reagent_requisition_lot/reagent_requisition_lotSearch.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to reagent_requisition_lot/reagent_requisition_lotSearchForm.jsp");
	        	rd = request.getRequestDispatcher("reagent_requisition_lot/reagent_requisition_lotSearchForm.jsp");
	        }
        }
		rd.forward(request, response);
	}
	
}

