package reagent_requisition_lot;
import java.util.*; 
import util.*;


public class Reagent_requisition_lotMAPS extends CommonMaps
{	
	public Reagent_requisition_lotMAPS(String tableName)
	{
		

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("requisitionToOrganogramId".toLowerCase(), "requisitionToOrganogramId".toLowerCase());
		java_DTO_map.put("subject".toLowerCase(), "subject".toLowerCase());
		java_DTO_map.put("description".toLowerCase(), "description".toLowerCase());
		java_DTO_map.put("insertedByUserId".toLowerCase(), "insertedByUserId".toLowerCase());
		java_DTO_map.put("insertedByOrganogramId".toLowerCase(), "insertedByOrganogramId".toLowerCase());
		java_DTO_map.put("jobCat".toLowerCase(), "jobCat".toLowerCase());
		java_DTO_map.put("insertionDate".toLowerCase(), "insertionDate".toLowerCase());
		java_DTO_map.put("searchColumn".toLowerCase(), "searchColumn".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("requisition_to_organogram_id".toLowerCase(), "requisitionToOrganogramId".toLowerCase());
		java_SQL_map.put("subject".toLowerCase(), "subject".toLowerCase());
		java_SQL_map.put("description".toLowerCase(), "description".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Requisition To Organogram Id".toLowerCase(), "requisitionToOrganogramId".toLowerCase());
		java_Text_map.put("Subject".toLowerCase(), "subject".toLowerCase());
		java_Text_map.put("Description".toLowerCase(), "description".toLowerCase());
		java_Text_map.put("Inserted By User Id".toLowerCase(), "insertedByUserId".toLowerCase());
		java_Text_map.put("Inserted By Organogram Id".toLowerCase(), "insertedByOrganogramId".toLowerCase());
		java_Text_map.put("Job".toLowerCase(), "jobCat".toLowerCase());
		java_Text_map.put("Insertion Date".toLowerCase(), "insertionDate".toLowerCase());
		java_Text_map.put("Search Column".toLowerCase(), "searchColumn".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}