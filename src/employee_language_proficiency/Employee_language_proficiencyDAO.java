package employee_language_proficiency;

import common.EmployeeCommonDAOService;
import employee_records.Employee_recordsDAO;
import employee_records.Employee_recordsDTO;
import org.apache.log4j.Logger;
import pb.CatRepository;
import pb.CategoryLanguageModel;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings({"unused", "Duplicates"})
public class Employee_language_proficiencyDAO implements EmployeeCommonDAOService<Employee_language_proficiencyDTO> {

    private static final Logger logger = Logger.getLogger(Employee_language_proficiencyDAO.class);

    private static final String addSqlQuery = "INSERT INTO {tableName} (employee_records_id,language_cat,read_skill_cat,write_skill_cat,"
            .concat("speak_skill_cat,modified_by,search_column,lastModificationTime,")
            .concat("inserted_by,insertion_date,isDeleted,ID) VALUES(?,?,?,?,?,?,?,?,?,?,?,?)");

    private static final String updateSqlQuery = "UPDATE {tableName} SET employee_records_id=?,language_cat=?,read_skill_cat=?,write_skill_cat=?,"
            .concat("speak_skill_cat=?,modified_by=?,search_column=?,lastModificationTime = ? WHERE ID = ?");

    private static final Map<String, String> searchMap = new HashMap<>();

    private Employee_language_proficiencyDAO() {
        searchMap.put("language_cat", " and (employee_language_proficiency.language_cat = ?)");
        searchMap.put("read_skill_cat", " and (employee_language_proficiency.read_skill_cat = ?)");
        searchMap.put("write_skill_cat", " and (employee_language_proficiency.write_skill_cat = ?)");
        searchMap.put("speak_skill_cat", " and (employee_language_proficiency.speak_skill_cat = ?)");
        searchMap.put("employee_records_id_internal", " and (employee_language_proficiency.employee_records_id = ?)");
        searchMap.put("AnyField", " and (employee_language_proficiency.search_column like ?)");
    }

    private static class LazyLoader {
        static final Employee_language_proficiencyDAO INSTANCE = new Employee_language_proficiencyDAO();
    }

    public static Employee_language_proficiencyDAO getInstance() {
        return Employee_language_proficiencyDAO.LazyLoader.INSTANCE;
    }


    @Override
    public void set(PreparedStatement ps, Employee_language_proficiencyDTO employee_language_proficiencyDTO, boolean isInsert) throws SQLException {
        int index = 0;
        setSearchColumn(employee_language_proficiencyDTO);
        ps.setObject(++index, employee_language_proficiencyDTO.employeeRecordsId);
        ps.setObject(++index, employee_language_proficiencyDTO.languageCat);
        ps.setObject(++index, employee_language_proficiencyDTO.readSkillCat);
        ps.setObject(++index, employee_language_proficiencyDTO.writeSkillCat);
        ps.setObject(++index, employee_language_proficiencyDTO.speakSkillCat);
        ps.setObject(++index, employee_language_proficiencyDTO.modifiedBy);
        ps.setObject(++index, employee_language_proficiencyDTO.searchColumn);
        ps.setObject(++index, employee_language_proficiencyDTO.lastModificationTime);
        if (isInsert) {
            ps.setObject(++index, employee_language_proficiencyDTO.insertedBy);
            ps.setObject(++index, employee_language_proficiencyDTO.insertionDate);
            ps.setObject(++index, 0);
        }
        ps.setObject(++index, employee_language_proficiencyDTO.iD);
    }

    public void setSearchColumn(Employee_language_proficiencyDTO employee_language_proficiencyDTO) {
        StringBuilder searchColumnBuilder = new StringBuilder();
        try {
            Employee_recordsDTO employeeRecordsDTO = new Employee_recordsDAO().getEmployee_recordsDTOByID(employee_language_proficiencyDTO.employeeRecordsId);
            if (employeeRecordsDTO != null) {
                if (employeeRecordsDTO.nameEng != null && employeeRecordsDTO.nameEng.trim().length() > 0) {
                    searchColumnBuilder.append(employeeRecordsDTO.nameEng.trim()).append(" ");
                }
                if (employeeRecordsDTO.nameBng != null && employeeRecordsDTO.nameBng.trim().length() > 0) {
                    searchColumnBuilder.append(employeeRecordsDTO.nameBng.trim()).append(" ");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        CategoryLanguageModel categoryLanguageModel = CatRepository.getInstance().getCategoryLanguageModel("language", employee_language_proficiencyDTO.languageCat);
        searchColumnBuilder.append(categoryLanguageModel.banglaText).append(" ");
        searchColumnBuilder.append(categoryLanguageModel.englishText).append(" ");

        employee_language_proficiencyDTO.searchColumn = searchColumnBuilder.toString();
    }

    @Override
    public Employee_language_proficiencyDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Employee_language_proficiencyDTO employee_language_proficiencyDTO = new Employee_language_proficiencyDTO();
            employee_language_proficiencyDTO.iD = rs.getLong("ID");
            employee_language_proficiencyDTO.employeeRecordsId = rs.getLong("employee_records_id");
            employee_language_proficiencyDTO.languageCat = rs.getInt("language_cat");
            employee_language_proficiencyDTO.readSkillCat = rs.getInt("read_skill_cat");
            employee_language_proficiencyDTO.writeSkillCat = rs.getInt("write_skill_cat");
            employee_language_proficiencyDTO.speakSkillCat = rs.getInt("speak_skill_cat");
            employee_language_proficiencyDTO.insertionDate = rs.getLong("insertion_date");
            employee_language_proficiencyDTO.insertedBy = rs.getLong("inserted_by");
            employee_language_proficiencyDTO.modifiedBy = rs.getLong("modified_by");
            employee_language_proficiencyDTO.searchColumn = rs.getString("search_column");
            employee_language_proficiencyDTO.isDeleted = rs.getInt("isDeleted");
            employee_language_proficiencyDTO.lastModificationTime = rs.getLong("lastModificationTime");
            return employee_language_proficiencyDTO;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    @Override
    public String getTableName() {
        return "employee_language_proficiency";
    }

    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Employee_language_proficiencyDTO) commonDTO, addSqlQuery, true);
    }

    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Employee_language_proficiencyDTO) commonDTO, updateSqlQuery, false);
    }


    public Employee_language_proficiencyDTO getDTOByID(long ID) {
        return getDTOFromID(ID);
    }

    public List<Employee_language_proficiencyDTO> getAllElection_details(boolean isFirstReload) {
        return getAllDTOs(isFirstReload);
    }

    public List<EmployeeLanguageProficiencyModel> getEmployeeLanguageProficiencyModels(long employeeId) {
        return getModelList(employeeId, this::buildEmployeeLanguageProficiencyModel);
    }

    private EmployeeLanguageProficiencyModel buildEmployeeLanguageProficiencyModel(Employee_language_proficiencyDTO dto) {
        EmployeeLanguageProficiencyModel model = new EmployeeLanguageProficiencyModel();
        model.dto = dto;
        CategoryLanguageModel categoryLanguageModel = CatRepository.getInstance().getCategoryLanguageModel("language", dto.languageCat);
        model.languageBan = categoryLanguageModel.banglaText;
        model.languageEng = categoryLanguageModel.englishText;
        categoryLanguageModel = CatRepository.getInstance().getCategoryLanguageModel("read_skill", dto.readSkillCat);
        model.readSkillBan = categoryLanguageModel.banglaText;
        model.readSkillEng = categoryLanguageModel.englishText;
        categoryLanguageModel = CatRepository.getInstance().getCategoryLanguageModel("write_skill", dto.writeSkillCat);
        model.writeSkillBan = categoryLanguageModel.banglaText;
        model.writeSkillEng = categoryLanguageModel.englishText;
        categoryLanguageModel = CatRepository.getInstance().getCategoryLanguageModel("speak_skill", dto.speakSkillCat);
        model.speakSkillBan = categoryLanguageModel.banglaText;
        model.speakSkillEng = categoryLanguageModel.englishText;
        return model;
    }
}