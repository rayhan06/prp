package employee_language_proficiency;

public class EmployeeLanguageProficiencyModel {
    public Employee_language_proficiencyDTO dto;
    public String languageBan;
    public String languageEng;
    public String readSkillEng;
    public String readSkillBan;
    public String writeSkillEng;
    public String writeSkillBan;
    public String speakSkillEng;
    public String speakSkillBan;
}
