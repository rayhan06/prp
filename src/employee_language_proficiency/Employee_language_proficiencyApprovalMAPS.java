package employee_language_proficiency;
import util.*;

public class Employee_language_proficiencyApprovalMAPS extends CommonMaps
{	
	public Employee_language_proficiencyApprovalMAPS(String tableName)
	{
		
		java_allfield_type_map.put("employee_records_id".toLowerCase(), "Long");
		java_allfield_type_map.put("language_cat".toLowerCase(), "Integer");
		java_allfield_type_map.put("read_skill_cat".toLowerCase(), "Integer");
		java_allfield_type_map.put("write_skill_cat".toLowerCase(), "Integer");
		java_allfield_type_map.put("speak_skill_cat".toLowerCase(), "Integer");
		java_allfield_type_map.put("insertion_date".toLowerCase(), "Long");
		java_allfield_type_map.put("inserted_by".toLowerCase(), "Long");
		java_allfield_type_map.put("modified_by".toLowerCase(), "Long");

		java_allfield_type_map.put("job_cat", "Integer");
		java_allfield_type_map.put("approval_status_cat", "Integer");
		java_allfield_type_map.put("initiator", "Long");
		java_allfield_type_map.put("assigned_to", "Long");
		java_allfield_type_map.put("starting_date", "Long");
		java_allfield_type_map.put("ending_date", "Long");
		
		java_table_map.put("approval_status_cat", "approval_summary");
		java_table_map.put("initiator", "approval_summary");
		java_table_map.put("assigned_to", "approval_summary");
	}

}