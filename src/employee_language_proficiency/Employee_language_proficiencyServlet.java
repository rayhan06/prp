package employee_language_proficiency;

import common.BaseServlet;
import common.CommonDAOService;
import common.EmployeeServletService;
import common.RoleEnum;
import employee_records.Employee_recordsRepository;
import user.UserDTO;
import util.CommonDTO;
import util.HttpRequestUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@SuppressWarnings({"Duplicates"})
@WebServlet("/Employee_language_proficiencyServlet")
@MultipartConfig
public class Employee_language_proficiencyServlet extends BaseServlet implements EmployeeServletService {

    private static final long serialVersionUID = 1L;

    private final Employee_language_proficiencyDAO employeeLanguageProficiencyDAO = Employee_language_proficiencyDAO.getInstance();

    @Override
    public String getTableName() {
        return Employee_language_proficiencyDAO.getInstance().getTableName();
    }

    @Override
    public String getServletName() {
        return "employee_language_proficiency";
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[0];
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[0];
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[0];
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Employee_language_proficiencyServlet.class;
    }

    @Override
    public Employee_language_proficiencyDAO getCommonDAOService() {
        return employeeLanguageProficiencyDAO;
    }

    @Override
    public String getAjaxAddRedirectURL(HttpServletRequest request, CommonDTO commonDTO) {
        return getRedirectURL(request);
    }

    @Override
    public String getAjaxEditRedirectURL(HttpServletRequest request, CommonDTO commonDTO) {
        return getRedirectURL(request);
    }

    @Override
    public String getDeleteRedirectURL(HttpServletRequest request) {
        return getRedirectURL(request);
    }

    @Override
    public boolean getAddPagePermission(HttpServletRequest request) {
        return checkAddPermission(request);
    }

    @Override
    public boolean getEditPagePermission(HttpServletRequest request) {
        return checkEditOrViewPagePermission(request);
    }

    @Override
    public boolean getViewPagePermission(HttpServletRequest request) {
        return checkEditOrViewPagePermission(request);
    }

    @Override
    public boolean getAddPermission(HttpServletRequest request) {
        return checkAddPermission(request);
    }

    @Override
    public boolean getEditPermission(HttpServletRequest request) {
        return checkEditOrViewPagePermission(request);
    }

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        Employee_language_proficiencyDTO employee_language_proficiencyDTO;
        boolean isLangEng = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
        long currentTime = System.currentTimeMillis();
        if (addFlag) {
            employee_language_proficiencyDTO = new Employee_language_proficiencyDTO();
            long empId;
            try {
                empId = Long.parseLong(request.getParameter("empId"));
            } catch (Exception ex) {
                logger.error(ex);
                throw new Exception(isLangEng ? "empId is not found" : "empId পাওয়া যায়নি");
            }
            if (Employee_recordsRepository.getInstance().getById(empId) == null) {
                throw new Exception(isLangEng ? "Employee information is not found" : "কর্মকর্তার তথ্য পাওয়া যায়নি");
            }
            employee_language_proficiencyDTO.employeeRecordsId = empId;
            employee_language_proficiencyDTO.insertedBy = userDTO.ID;
            employee_language_proficiencyDTO.insertionDate = currentTime;
        } else {
            employee_language_proficiencyDTO = Employee_language_proficiencyDAO.getInstance().getDTOByID(Long.parseLong(request.getParameter("iD")));
        }
        employee_language_proficiencyDTO.modifiedBy = userDTO.ID;
        employee_language_proficiencyDTO.lastModificationTime = currentTime;
        validateAndSet(employee_language_proficiencyDTO,request,isLangEng);

        if (addFlag) {
            Employee_language_proficiencyDAO.getInstance().add(employee_language_proficiencyDTO);
        } else {
            Employee_language_proficiencyDAO.getInstance().update(employee_language_proficiencyDTO);
        }
        return employee_language_proficiencyDTO;
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if ("search".equals(request.getParameter("actionType"))) {
            UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;
            boolean isAdmin = userDTO.roleID == RoleEnum.ADMIN.getRoleId() || userDTO.roleID == RoleEnum.HR_ADMIN.getRoleId() || userDTO.roleID == RoleEnum.DATA_ENTRY.getRoleId();
            if (!isAdmin) {
                Map<String, String> extraCriteriaMap = new HashMap<>();
                extraCriteriaMap.put("employee_records_id_internal", String.valueOf(userDTO.employee_record_id));
                request.setAttribute(CommonDAOService.CRITERIA_MAP_KEY, extraCriteriaMap);
            }
        }
        super.doGet(request, response);
    }

    private String getRedirectURL(HttpServletRequest request) {
        UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;
        if (request.getParameter("tab") != null) {
            if (userDTO.roleID == RoleEnum.ADMIN.getRoleId() || userDTO.roleID == RoleEnum.DATA_ENTRY.getRoleId()) {
                long empId;
                if ("ajax_add".equals(request.getParameter("actionType"))) {
                    empId = Long.parseLong(request.getParameter("empId"));
                } else {
                    long id;
                    if (request.getParameter("ID") != null) {
                        id = Long.parseLong(request.getParameter("ID"));
                    } else {
                        id = Long.parseLong(request.getParameter("iD"));
                    }
                    Employee_language_proficiencyDTO dto = employeeLanguageProficiencyDAO.getDTOFromIdDeletedOrNot(id);
                    empId = dto.employeeRecordsId;
                }
                return "Employee_recordsServlet?actionType=viewMultiForm&data=language&tab=2&ID=" + empId;
            } else {
                return "Employee_recordsServlet?actionType=viewMyProfile&data=language&tab=2";
            }
        } else {
            return "Employee_language_proficiencyServlet?actionType=search";
        }
    }
}