package employee_language_proficiency;
import java.util.*; 
import util.*;


public class Employee_language_proficiencyMAPS extends CommonMaps
{	
	public Employee_language_proficiencyMAPS(String tableName)
	{
		
		java_allfield_type_map.put("employee_records_id".toLowerCase(), "Long");
		java_allfield_type_map.put("language_cat".toLowerCase(), "Integer");
		java_allfield_type_map.put("read_skill_cat".toLowerCase(), "Integer");
		java_allfield_type_map.put("write_skill_cat".toLowerCase(), "Integer");
		java_allfield_type_map.put("speak_skill_cat".toLowerCase(), "Integer");
		java_allfield_type_map.put("insertion_date".toLowerCase(), "Long");
		java_allfield_type_map.put("inserted_by".toLowerCase(), "Long");
		java_allfield_type_map.put("modified_by".toLowerCase(), "Long");

		java_anyfield_search_map.put("language_text.languageTextEnglish", "String");
		java_anyfield_search_map.put("language_text.languageTextBangla", "String");

		java_anyfield_search_map.put(tableName + ".employee_records_id".toLowerCase(), "Long");
		java_anyfield_search_map.put(tableName + ".insertion_date".toLowerCase(), "Long");
		java_anyfield_search_map.put(tableName + ".inserted_by".toLowerCase(), "Long");
		java_anyfield_search_map.put(tableName + ".modified_by".toLowerCase(), "Long");

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("employeeRecordsId".toLowerCase(), "employeeRecordsId".toLowerCase());
		java_DTO_map.put("languageCat".toLowerCase(), "languageCat".toLowerCase());
		java_DTO_map.put("readSkillCat".toLowerCase(), "readSkillCat".toLowerCase());
		java_DTO_map.put("writeSkillCat".toLowerCase(), "writeSkillCat".toLowerCase());
		java_DTO_map.put("speakSkillCat".toLowerCase(), "speakSkillCat".toLowerCase());
		java_DTO_map.put("insertionDate".toLowerCase(), "insertionDate".toLowerCase());
		java_DTO_map.put("insertedBy".toLowerCase(), "insertedBy".toLowerCase());
		java_DTO_map.put("modifiedBy".toLowerCase(), "modifiedBy".toLowerCase());
		java_DTO_map.put("jobCat".toLowerCase(), "jobCat".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("employee_records_id".toLowerCase(), "employeeRecordsId".toLowerCase());
		java_SQL_map.put("language_cat".toLowerCase(), "languageCat".toLowerCase());
		java_SQL_map.put("read_skill_cat".toLowerCase(), "readSkillCat".toLowerCase());
		java_SQL_map.put("write_skill_cat".toLowerCase(), "writeSkillCat".toLowerCase());
		java_SQL_map.put("speak_skill_cat".toLowerCase(), "speakSkillCat".toLowerCase());
		java_SQL_map.put("insertion_date".toLowerCase(), "insertionDate".toLowerCase());
		java_SQL_map.put("inserted_by".toLowerCase(), "insertedBy".toLowerCase());
		java_SQL_map.put("modified_by".toLowerCase(), "modifiedBy".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Employee Records Id".toLowerCase(), "employeeRecordsId".toLowerCase());
		java_Text_map.put("Language".toLowerCase(), "languageCat".toLowerCase());
		java_Text_map.put("Reading Skill".toLowerCase(), "readSkillCat".toLowerCase());
		java_Text_map.put("Writing Skill".toLowerCase(), "writeSkillCat".toLowerCase());
		java_Text_map.put("Speaking Skill".toLowerCase(), "speakSkillCat".toLowerCase());
		java_Text_map.put("Insertion Date".toLowerCase(), "insertionDate".toLowerCase());
		java_Text_map.put("Inserted By".toLowerCase(), "insertedBy".toLowerCase());
		java_Text_map.put("Modified By".toLowerCase(), "modifiedBy".toLowerCase());
		java_Text_map.put("Job Cat".toLowerCase(), "jobCat".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}