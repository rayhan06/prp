package employee_language_proficiency;

import annotation.CategoryValidation;
import util.CommonEmployeeDTO;

public class Employee_language_proficiencyDTO extends CommonEmployeeDTO {
    @CategoryValidation(name = "languageCat",domainName = "language",
            engError = "Please select language", bngError = "অনুগ্রহ করে ভাষা নির্বাচন করুন",
            properEngError = "Please select correct language", properBngError = "অনুগ্রহ করে সঠিক ভাষা নির্বাচন করুন")
    public int languageCat = 0;

    @CategoryValidation(name = "readSkillCat",domainName = "read_skill",
            engError = "Please select read skill", bngError = "অনুগ্রহ করে পড়ার দক্ষতা নির্বাচন করুন",
            properEngError = "Please select correct read skill", properBngError = "অনুগ্রহ করে সঠিক পড়ার দক্ষতা নির্বাচন করুন")
    public int readSkillCat = 0;

    @CategoryValidation(name = "writeSkillCat",domainName = "write_skill",
            engError = "Please select write skill", bngError = "অনুগ্রহ করে লেখার দক্ষতা নির্বাচন করুন",
            properEngError = "Please select correct write skill", properBngError = "অনুগ্রহ করে সঠিক লেখার দক্ষতা নির্বাচন করুন")
    public int writeSkillCat = 0;
    @CategoryValidation(name = "speakSkillCat",domainName = "speak_skill",
            engError = "Please select speak skill", bngError = "অনুগ্রহ করে বলার দক্ষতা নির্বাচন করুন",
            properEngError = "Please select correct speak skill", properBngError = "অনুগ্রহ করে সঠিক বলার দক্ষতা নির্বাচন করুন")
    public int speakSkillCat = 0;

    public long insertionDate = 0;
    public long insertedBy = 0;
    public long modifiedBy = 0;

    @Override
    public String toString() {
        return "$Employee_language_proficiencyDTO[" +
                " iD = " + iD +
                " employeeRecordsId = " + employeeRecordsId +
                " languageCat = " + languageCat +
                " readSkillCat = " + readSkillCat +
                " writeSkillCat = " + writeSkillCat +
                " speakSkillCat = " + speakSkillCat +
                " insertionDate = " + insertionDate +
                " insertedBy = " + insertedBy +
                " modifiedBy = " + modifiedBy +
                " jobCat = " + jobCat +
                " isDeleted = " + isDeleted +
                " lastModificationTime = " + lastModificationTime +
                "]";
    }

}