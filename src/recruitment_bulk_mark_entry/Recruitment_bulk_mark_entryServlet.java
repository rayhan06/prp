package recruitment_bulk_mark_entry;

import java.io.IOException;
import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import geolocation.GeoDistrictDAO;
import geolocation.GeoDistrictDTO;
import job_applicant_application.Job_applicant_applicationDAO;
import job_applicant_application.Job_applicant_applicationDTO;
import job_applicant_application.Job_applicant_applicationServlet;
import language.LC;
import language.LM;
import org.apache.log4j.Logger;

import login.LoginDTO;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddressList;
import org.apache.poi.xssf.usermodel.*;
import parliament_job_applicant.Parliament_job_applicantDTO;
import parliament_job_applicant.Parliament_job_applicantRepository;
import permission.MenuConstants;
import recruitment_bulk_attendance.Recruitment_bulk_attendanceDAO;
import recruitment_bulk_attendance.Recruitment_bulk_attendanceDTO;
import recruitment_job_description.Recruitment_job_descriptionDTO;
import recruitment_job_description.Recruitment_job_descriptionRepository;
import recruitment_job_specific_exam_type.JobSpecificExamTypeDao;
import recruitment_job_specific_exam_type.RecruitmentJobSpecificExamTypeDTO;
import role.PermissionRepository;


import sessionmanager.SessionConstants;

import sms.SmsService;
import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;
import util.CommonRequestHandler;
import util.RecordNavigationManager4;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import javax.servlet.http.*;
import java.util.UUID;


import java.util.ArrayList;
import java.util.Iterator;


import geolocation.GeoLocationDAO2;

import java.util.StringTokenizer;

import com.google.gson.Gson;

import pb.*;
import pbReport.*;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import util.UtilCharacter;

import static sessionmanager.SessionConstants.AttendanceStatusMap;
import static sessionmanager.SessionConstants.PassFailStatusMap;


/**
 * Servlet implementation class Recruitment_bulk_mark_entryServlet
 */
@WebServlet("/Recruitment_bulk_mark_entryServlet")
@MultipartConfig
public class Recruitment_bulk_mark_entryServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Recruitment_bulk_mark_entryServlet.class);

    String tableName = "recruitment_bulk_mark_entry";
    String parentAttTableName = "recruitment_bulk_attendance";
    String parentTableName = "job_applicant_application";

    Recruitment_bulk_mark_entryDAO recruitment_bulk_mark_entryDAO;
    Recruitment_bulk_attendanceDAO recruitment_bulk_attendanceDAO;
    Job_applicant_applicationDAO job_applicant_applicationDAO;
    CommonRequestHandler commonRequestHandler;
    private final Gson gson = new Gson();

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Recruitment_bulk_mark_entryServlet() {
        super();
        try {
            recruitment_bulk_mark_entryDAO = new Recruitment_bulk_mark_entryDAO(tableName);
            recruitment_bulk_attendanceDAO = new Recruitment_bulk_attendanceDAO(parentAttTableName);
            job_applicant_applicationDAO = new Job_applicant_applicationDAO(parentTableName);
            commonRequestHandler = new CommonRequestHandler(recruitment_bulk_mark_entryDAO);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("In doget request = " + request);
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        boolean isPermanentTable = true;
        if (request.getParameter("isPermanentTable") != null) {
            isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
        }
        System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
        try {
            String actionType = request.getParameter("actionType");

            if (actionType.equals("getCandidateInfoExcelList")) {

                String actionMessage = request.getParameter("message");

                if (actionMessage != null && actionMessage.equalsIgnoreCase("success")) {


                }

                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.RECRUITMENT_CANDIDATE_LIST)) {
                    getCandidateList(request, response);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else if (actionType.equals("getAddPage")) {
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.RECRUITMENT_BULK_MARK_ENTRY_ADD)) {
                    commonRequestHandler.getAddPage(request, response);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else if (actionType.equals("getEditPage")) {
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.RECRUITMENT_BULK_MARK_ENTRY_UPDATE)) {
                    getRecruitment_bulk_mark_entry(request, response);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else if (actionType.equals("getUploadPage")) {
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.RECRUITMENT_BULK_MARK_ENTRY_UPLOAD)) {
                    commonRequestHandler.geUploadPage(request, response);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else if (actionType.equals("getURL")) {
                String URL = request.getParameter("URL");
                System.out.println("URL = " + URL);
                response.sendRedirect(URL);
            } else if (actionType.equals("search")) {
                System.out.println("search requested");
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.RECRUITMENT_BULK_MARK_ENTRY_SEARCH)) {
                    if (isPermanentTable) {
                        String filter = request.getParameter("filter");
                        System.out.println("filter = " + filter);
                        if (filter != null) {
                            filter = ""; //shouldn't be directly used, rather manipulate it.
                            searchRecruitment_bulk_mark_entry(request, response, isPermanentTable, filter);
                        } else {
                            searchRecruitment_bulk_mark_entry(request, response, isPermanentTable, "");
                        }
                    } else {
                        //searchRecruitment_bulk_mark_entry(request, response, tempTableName, isPermanentTable);
                    }
                }
            } else if (actionType.equals("view")) {
                System.out.println("view requested");
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.RECRUITMENT_BULK_MARK_ENTRY_SEARCH)) {
                    commonRequestHandler.view(request, response);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }

            } else if (actionType.equals("CheckExistenceJobLevel")) {
                long job_id = Long.parseLong((request.getParameter("job_id")));
                long level_id = Long.parseLong((request.getParameter("level_id")));
                //int existence = recruitment_bulk_mark_entryDAO.JobLevelIdExistence(job_id,level_id);

                List<Recruitment_bulk_mark_entryDTO> recruitment_bulk_mark_entryDTOS = Recruitment_bulk_mark_entryRepository.getInstance().getRecruitment_bulk_mark_entryDTOByJobIdAndLevel(job_id, level_id);
                int existence = recruitment_bulk_mark_entryDTOS.size() > 0 ? 1 : 0;

                //System.out.println("nnn existence: "+existence);
                PrintWriter out = response.getWriter();
                out.println(existence);
                out.close();
            } else {
                request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.debug(ex);
        }
    }


    public ArrayList<Recruitment_bulk_mark_entryDTO> ReadXLsToArraylist(HttpServletRequest request, String fileName) throws IOException {
        String path = getServletContext().getRealPath("/img2/");
        File excelFile = new File(path + File.separator
                + fileName);
        FileInputStream fis = new FileInputStream(excelFile);


        XSSFWorkbook workbook = new XSSFWorkbook(fis);
        XSSFSheet sheet = workbook.getSheetAt(0);
        Iterator<Row> rowIt = sheet.iterator();
        ArrayList<String> Rows = new ArrayList<String>();
        ArrayList<Recruitment_bulk_mark_entryDTO> recruitment_bulk_mark_entryDTOs = new ArrayList<Recruitment_bulk_mark_entryDTO>();


        Recruitment_bulk_mark_entryDTO recruitment_bulk_mark_entryDTO;

        String failureMessage = "";
        int i = 0;

        Recruitment_bulk_mark_entryMAPS recruitment_bulk_mark_entryMAPS = new Recruitment_bulk_mark_entryMAPS("recruitment_bulk_mark_entry");
        while (rowIt.hasNext()) {
            Row row = rowIt.next();

            Iterator<Cell> cellIterator = row.cellIterator();

            recruitment_bulk_mark_entryDTO = new Recruitment_bulk_mark_entryDTO();

            int j = 0;

            try {
                while (cellIterator.hasNext()) {
                    Cell cell = cellIterator.next();


                    if (i == 0) {
                        Rows.add(cell.toString());
                        System.out.println("Rows found: " + cell + " ");
                    } else {

                        String rowName = Rows.get(j).toLowerCase();
                        System.out.println("rowname: " + rowName + " rowname from map = " + recruitment_bulk_mark_entryMAPS.java_Text_map.get(rowName));

                        if (rowName == null || rowName.equalsIgnoreCase("")) {
                            System.out.println("null row name");
                            break;
                        }
                        if (cell == null || cell.toString().equalsIgnoreCase("")) {
                            System.out.println("null cell");
                            j++;
                            continue;
                        } else {
                            System.out.println("Inserting Value = " + cell + " to row " + rowName);

                        }
                        if (recruitment_bulk_mark_entryMAPS.java_Text_map.get(rowName) != null && recruitment_bulk_mark_entryMAPS.java_Text_map.get(rowName).equalsIgnoreCase("id")) {
                            recruitment_bulk_mark_entryDTO.id = (long) Double.parseDouble(cell.toString());
                        } else if (recruitment_bulk_mark_entryMAPS.java_Text_map.get(rowName) != null && recruitment_bulk_mark_entryMAPS.java_Text_map.get(rowName).equalsIgnoreCase("jobId")) {
                            recruitment_bulk_mark_entryDTO.jobId = (long) Double.parseDouble(cell.toString());
                        } else if (recruitment_bulk_mark_entryMAPS.java_Text_map.get(rowName) != null && recruitment_bulk_mark_entryMAPS.java_Text_map.get(rowName).equalsIgnoreCase("levelId")) {
                            recruitment_bulk_mark_entryDTO.levelId = (long) Double.parseDouble(cell.toString());
                        } else if (recruitment_bulk_mark_entryMAPS.java_Text_map.get(rowName) != null && recruitment_bulk_mark_entryMAPS.java_Text_map.get(rowName).equalsIgnoreCase("roll")) {
                            try {
                                recruitment_bulk_mark_entryDTO.roll = (long) Double.parseDouble(cell.toString());
                            } catch (NumberFormatException ex) { // handle your exception
                                System.out.println("roll NumberFormatException: " + ex);
                            }
                            //recruitment_bulk_mark_entryDTO.roll = (long)Double.parseDouble(cell.toString());
                        } else if (recruitment_bulk_mark_entryMAPS.java_Text_map.get(rowName) != null && recruitment_bulk_mark_entryMAPS.java_Text_map.get(rowName).equalsIgnoreCase("marks")) {
                            try {
                                recruitment_bulk_mark_entryDTO.marks = Double.parseDouble(cell.toString());
                            } catch (NumberFormatException ex) { // handle your exception
                                System.out.println("marks NumberFormatException: " + ex);
                            }
                            //recruitment_bulk_mark_entryDTO.marks = Double.parseDouble(cell.toString());
                        } else if (recruitment_bulk_mark_entryMAPS.java_Text_map.get(rowName) != null && recruitment_bulk_mark_entryMAPS.java_Text_map.get(rowName).equalsIgnoreCase("grade")) {
                            recruitment_bulk_mark_entryDTO.grade = (cell.toString());
                        } else if (recruitment_bulk_mark_entryMAPS.java_Text_map.get(rowName) != null && recruitment_bulk_mark_entryMAPS.java_Text_map.get(rowName).equalsIgnoreCase("insertionDate")) {
                            recruitment_bulk_mark_entryDTO.insertionDate = (long) Double.parseDouble(cell.toString());
                        } else if (recruitment_bulk_mark_entryMAPS.java_Text_map.get(rowName) != null && recruitment_bulk_mark_entryMAPS.java_Text_map.get(rowName).equalsIgnoreCase("insertedBy")) {
                            recruitment_bulk_mark_entryDTO.insertedBy = (cell.toString());
                        } else if (recruitment_bulk_mark_entryMAPS.java_Text_map.get(rowName) != null && recruitment_bulk_mark_entryMAPS.java_Text_map.get(rowName).equalsIgnoreCase("modifiedBy")) {
                            recruitment_bulk_mark_entryDTO.modifiedBy = (cell.toString());
                        }
                    }
                    j++;

                }


                if (i != 0) {
                    System.out.println("INSERTING to the list: " + recruitment_bulk_mark_entryDTO);
                    recruitment_bulk_mark_entryDTOs.add(recruitment_bulk_mark_entryDTO);
                }
            } catch (Exception e) {
                e.printStackTrace();
                failureMessage += (i + 1) + " ";
            }
            i++;

            System.out.println();

        }
        if (failureMessage.equalsIgnoreCase("")) {
            failureMessage = " Successfully parsed all rows";
        } else {
            failureMessage = " Failed on rows: " + failureMessage;
        }
        request.setAttribute("failureMessage", failureMessage);

        workbook.close();
        fis.close();
        return recruitment_bulk_mark_entryDTOs;

    }


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        System.out.println("doPost");
        boolean isPermanentTable = true;
        if (request.getParameter("isPermanentTable") != null) {
            isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
        }
        System.out.println("In servlet, isPermanentTable = " + isPermanentTable);

        try {
            String actionType = request.getParameter("actionType");
            System.out.println("actionType = " + actionType);

            if (actionType.equals("DownloadTemplate")) {
                String isDownload = request.getParameter("download_excel");
                String isUpload = request.getParameter("upload_excel");
                String job_id = request.getParameter("job");
                String level_id = request.getParameter("level");

                Random r = new Random();

                DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                Date date = new Date();

                if (isDownload != null) {

                    //System.out.println("isDownload: "+isDownload);
                    //List<Recruitment_bulk_attendanceDTO> recruitment_bulk_attendanceDTO_job_level = recruitment_bulk_attendanceDAO.getRollDTOsByJobLevelID(Integer.parseInt(job_id),Integer.parseInt(level_id));
                    List<Job_applicant_applicationDTO> job_applicant_applicationDTO_job_level = job_applicant_applicationDAO.getIDDTOsByJobLevelID(Integer.parseInt(job_id), Integer.parseInt(level_id));
                    String job_name = request.getParameter("job_name");
                    String level_name = request.getParameter("level_name");
                    response.setContentType("application/vnd.ms-excel");
                    //response.setHeader("Content-Disposition", "attachment; filename=employee_grade.xlsx");
                    String file_name = "recruitment_bulk_mark_" + job_name + "_" + level_name + "_" + dateFormat.format(date) + ".xlsx";
                    response.setHeader("Content-Disposition", "attachment; filename=" + file_name);

//					GeoDistrictDAO geoDistrictDAO = new GeoDistrictDAO();
//
//					List<GeoDistrictDTO>getAllDistricts =  geoDistrictDAO.getAllDistricts();

                    XSSFWorkbook workbook = UtilCharacter.newWorkBook();
                    Sheet sheet = UtilCharacter.newSheet(workbook, LM.getText(LC.CANDIDATE_LIST_CANDIDATE_LIST, loginDTO));
                    List<String> attendanceStatus = new ArrayList<String>(PassFailStatusMap.values());

                    int row_index = 0;
                    int cell_col_index = 0;
                    boolean addMerged = false;
                    Row newRow = sheet.createRow(row_index);
                    IndexedColors indexedColor = IndexedColors.BLACK1;
                    boolean isBold = true;
                    int languageID = LM.getLanguageIDByUserDTO(userDTO);
                    String Language = languageID == 1 ? "english" : "bangla";
                    List<Job_applicant_applicationDTO> job_applicant_applicationDTOS = job_applicant_applicationDAO.getIDDTOsByJobLevelID(Integer.parseInt(job_id), Integer.parseInt(level_id));

                    UtilCharacter.alreadyCreatedRow(workbook, sheet, "Identity", row_index, row_index, cell_col_index, cell_col_index++, addMerged, newRow, indexedColor, isBold);
                    UtilCharacter.alreadyCreatedRow(workbook, sheet, "Father Name", row_index, row_index, cell_col_index, cell_col_index++, addMerged, newRow, indexedColor, isBold);
                    UtilCharacter.alreadyCreatedRow(workbook, sheet, "District", row_index, row_index, cell_col_index, cell_col_index++, addMerged, newRow, indexedColor, isBold);
                    UtilCharacter.alreadyCreatedRow(workbook, sheet, "Roll", row_index, row_index, cell_col_index, cell_col_index++, addMerged, newRow, indexedColor, isBold);
                    UtilCharacter.alreadyCreatedRow(workbook, sheet, "Marks", row_index, row_index, cell_col_index, cell_col_index++, addMerged, newRow, indexedColor, isBold);

                    isBold = false;
                    for (Job_applicant_applicationDTO records : job_applicant_applicationDTO_job_level) {

                        row_index++;
                        cell_col_index = 0;
                        newRow = sheet.createRow(row_index);

                        UtilCharacter.alreadyCreatedRow(workbook, sheet, UtilCharacter.getDataByLanguage(Language, records.applicant_name_bn, records.applicant_name_en), row_index, row_index, cell_col_index, cell_col_index++, addMerged, newRow, indexedColor, isBold);

                        UtilCharacter.alreadyCreatedRow(workbook, sheet, String.valueOf(records.father_name), row_index, row_index, cell_col_index, cell_col_index++, addMerged, newRow, indexedColor, isBold);
                        UtilCharacter.alreadyCreatedRow(workbook, sheet, UtilCharacter.getDataByLanguage(Language, records.district_name_bn, records.district_name_en), row_index, row_index, cell_col_index, cell_col_index++, addMerged, newRow, indexedColor, isBold);

                        UtilCharacter.alreadyCreatedRow(workbook, sheet, String.valueOf(records.rollNumber), row_index, row_index, cell_col_index, cell_col_index++, addMerged, newRow, indexedColor, isBold);
                        Cell cell = UtilCharacter.alreadyCreatedRow(workbook, sheet, "", row_index, row_index, cell_col_index, cell_col_index, addMerged, newRow, indexedColor, isBold);
                        //}

                        if (level_id.trim().equals("2")) {
                            cell.setCellValue(attendanceStatus.get(0));
                            XSSFDataValidationHelper dvHelper = new
                                    XSSFDataValidationHelper((XSSFSheet) sheet);
                            XSSFDataValidationConstraint dvConstraint = (XSSFDataValidationConstraint)
                                    dvHelper.createExplicitListConstraint(attendanceStatus.toArray(new String[0]));
                            CellRangeAddressList addressList = new CellRangeAddressList(row_index, row_index, cell_col_index, cell_col_index);
                            XSSFDataValidation validation = (XSSFDataValidation) dvHelper.createValidation(
                                    dvConstraint, addressList);
                            validation.setShowErrorBox(true);
                            sheet.addValidationData(validation);
                        }


                    }

                    workbook.write(response.getOutputStream()); // Write workbook to response.
                    workbook.close();

                }

            } else if (actionType.equals("excelReviewWithoutUpload")) {

                String job_id = request.getParameter("job_id");
                String level_id = request.getParameter("level_id");
                System.out.println("new job_id: " + job_id + " level: " + level_id);
                //List<Recruitment_bulk_attendanceDTO> recruitment_bulk_attendanceDTO_job_level = recruitment_bulk_attendanceDAO.getRollDTOsByJobLevelID(Integer.parseInt(job_id),Integer.parseInt(level_id));
                List<Job_applicant_applicationDTO> job_applicant_applicationDTO_job_level = job_applicant_applicationDAO.
                        getIDDTOsByJobLevelID(Integer.parseInt(job_id), Integer.parseInt(level_id));
                List<String> attendanceStatus = new ArrayList<String>(AttendanceStatusMap.values());
                ArrayList<Recruitment_bulk_mark_entryDTO> recruitment_bulk_mark_entryDTOS = new ArrayList<Recruitment_bulk_mark_entryDTO>();
                Recruitment_bulk_mark_entryDTO recruitment_bulk_mark_entryDTO;


                for (Job_applicant_applicationDTO dto : job_applicant_applicationDTO_job_level) {
                    recruitment_bulk_mark_entryDTO = new Recruitment_bulk_mark_entryDTO();
                    //recruitment_bulk_mark_entryDTO.roll = Long.parseLong(String.valueOf(dto.roll));
                    recruitment_bulk_mark_entryDTO.roll = Long.parseLong(dto.rollNumber);
                    recruitment_bulk_mark_entryDTOS.add(recruitment_bulk_mark_entryDTO);
                }

                //ArrayList<Recruitment_bulk_attendanceDTO> recruitment_bulk_attendanceDTOs = ReadXLsToArraylist(request, FileName);
                HttpSession session = request.getSession(true);
                session.setAttribute("recruitment_bulk_mark_entryDTOs", recruitment_bulk_mark_entryDTOS);
                RequestDispatcher rd = request.getRequestDispatcher("recruitment_bulk_mark_entry/recruitment_bulk_mark_entryReview.jsp?actionType=edit");
                rd.forward(request, response);

            } else if (actionType.equals("add")) {

                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.RECRUITMENT_BULK_MARK_ENTRY_ADD)) {
                    System.out.println("going to  addRecruitment_bulk_mark_entry ");
                    addRecruitment_bulk_mark_entry(request, response, true, userDTO, true);
                } else {
                    System.out.println("Not going to  addRecruitment_bulk_mark_entry ");
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }

            } else if (actionType.equals("getDTO")) {

                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.RECRUITMENT_BULK_MARK_ENTRY_ADD)) {
                    getDTO(request, response);
                } else {
                    System.out.println("Not going to  addRecruitment_bulk_mark_entry ");
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }

            } else if (actionType.equals("edit")) {

                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.RECRUITMENT_BULK_MARK_ENTRY_UPDATE)) {
                    addRecruitment_bulk_mark_entry(request, response, false, userDTO, isPermanentTable);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else if (actionType.equals("upload")) {
                String job_id = request.getParameter("job_id");
                String level_id = request.getParameter("level_id");
                boolean existence = true;
                //int existence = testing_excelDAO.JobLevelIdExistence(Integer.parseInt(job_id),Integer.parseInt(level_id));
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.RECRUITMENT_BULK_MARK_ENTRY_UPLOAD)) {
                    if (existence) {
                        uploadRecruitment_bulk_mark_entry(request, response, false);
                    } else {

                    }

                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else if (actionType.equals("uploadConfirmed")) {

                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.RECRUITMENT_BULK_MARK_ENTRY_UPLOAD)) {
                    System.out.println("uploadConfirmed");
                    addRecruitment_bulk_mark_entrys(request, response);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else if (actionType.equals("delete")) {
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.FAST_SEARCH_TEST_UPDATE)) {
                    commonRequestHandler.delete(request, response, userDTO);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else if (actionType.equals("search")) {
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.RECRUITMENT_BULK_MARK_ENTRY_SEARCH)) {
                    searchRecruitment_bulk_mark_entry(request, response, true, "");
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else if (actionType.equals("getGeo")) {
                System.out.println("going to geoloc ");
                request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
            } else if (actionType.equals("checkJobLevelAttExistence")) {

                String job = request.getParameter("job_id");
                String level = request.getParameter("level_id");
                long job_id = Long.parseLong((job));
                long level_id = Long.parseLong(level);


                //int attendace_existence = recruitment_bulk_attendanceDAO.JobLevelAttendanceExistence(job_id,lebel_id);
                int attendace_existence = 0;
                //int mark_existence=recruitment_bulk_mark_entryDAO.JobLevelIdMarkEntryExistence(job_id,level_id);

                List<Recruitment_bulk_mark_entryDTO> recruitment_bulk_mark_entryDTOS = Recruitment_bulk_mark_entryRepository.getInstance().getRecruitment_bulk_mark_entryDTOByJobIdAndLevel(job_id, level_id);
                int mark_existence = recruitment_bulk_mark_entryDTOS.size() > 0 ? 0 : 1;
                System.out.println("attendance_existence: " + attendace_existence + " mark_existence: " + mark_existence);
                PrintWriter out = response.getWriter();
                out.println(attendace_existence + "," + mark_existence);
                out.close();

            }

        } catch (Exception ex) {
            ex.printStackTrace();
            logger.debug(ex);
        }
    }

    private void getDTO(HttpServletRequest request, HttpServletResponse response) {
        try {
            System.out.println("In getDTO");
            //Recruitment_bulk_mark_entryDTO recruitment_bulk_mark_entryDTO = (Recruitment_bulk_mark_entryDTO)recruitment_bulk_mark_entryDAO.getDTOByID(Long.parseLong(request.getParameter("ID")));

            Recruitment_bulk_mark_entryDTO recruitment_bulk_mark_entryDTO = Recruitment_bulk_mark_entryRepository.getInstance().getRecruitment_bulk_mark_entryDTOByid(Long.parseLong(request.getParameter("ID")));

            PrintWriter out = response.getWriter();
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");

            String encoded = this.gson.toJson(recruitment_bulk_mark_entryDTO);
            System.out.println("json encoded data = " + encoded);
            out.print(encoded);
            out.flush();
        } catch (NumberFormatException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    private void uploadRecruitment_bulk_mark_entry(HttpServletRequest request, HttpServletResponse response, Boolean addFlag) throws IOException {
        System.out.println("%%%% ajax upload called");
        Part filePart_recruitment_bulk_mark_entryDatabase;
        try {
            filePart_recruitment_bulk_mark_entryDatabase = request.getPart("testing_excelDatabase");
            String Value = commonRequestHandler.getFileName(filePart_recruitment_bulk_mark_entryDatabase);
            System.out.println("recruitment_bulk_mark_entryDatabase = " + Value);
            if (addFlag == true || (Value != null && !Value.equalsIgnoreCase(""))) {
                if (Value != null && !Value.equalsIgnoreCase("")) {
                    String FileName = "recruitment_bulk_mark_entrydatabase";
                    String path = getServletContext().getRealPath("/img2/");
                    Utils.uploadFile(filePart_recruitment_bulk_mark_entryDatabase, FileName, path);
                    ArrayList<Recruitment_bulk_mark_entryDTO> recruitment_bulk_mark_entryDTOs = ReadXLsToArraylist(request, FileName);
                    HttpSession session = request.getSession(true);
                    session.setAttribute("recruitment_bulk_mark_entryDTOs", recruitment_bulk_mark_entryDTOs);

                    RequestDispatcher rd = request.getRequestDispatcher("recruitment_bulk_mark_entry/recruitment_bulk_mark_entryReview.jsp?actionType=edit");
                    rd.forward(request, response);
                }
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }
        } catch (ServletException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    private void addRecruitment_bulk_mark_entrys(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String[] paramValues = request.getParameterValues("id");
        Long jobId = Long.valueOf(request.getParameterValues("jobId")[0]);
        int levelId = Integer.parseInt((request.getParameterValues("levelId")[0]));
        JobSpecificExamTypeDao jobSpecificExamTypeDAO = new  JobSpecificExamTypeDao();
        double passMark =  jobSpecificExamTypeDAO.getPassMarkFromJobIdAndCurrentLevel(jobId,levelId) ;
        
        LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        for (int i = 0; i < paramValues.length; i++) {

            Recruitment_bulk_mark_entryDTO recruitment_bulk_mark_entryDTO = new Recruitment_bulk_mark_entryDTO();

            Job_applicant_applicationDTO job_applicant_applicationDTO = job_applicant_applicationDAO.
                    getDTOByJobIdAndLevelAndRollNumber(Long.parseLong(request.getParameterValues("jobId")[i]),
                            Long.parseLong(request.getParameterValues("levelId")[i]),
                            request.getParameterValues("roll")[i]);
            try {
                String Value = "";

                if (request.getParameterValues("id") != null) {
                    Value = request.getParameterValues("id")[i];
                    if (Value != null) {
                        Value = Jsoup.clean(Value, Whitelist.simpleText());
                    }
                    System.out.println("id = " + Value);
                    if (Value != null && !Value.equalsIgnoreCase("")) {
                        recruitment_bulk_mark_entryDTO.id = Long.parseLong(Value);
                    } else {
                        System.out.println("FieldName has a null Value, not updating" + " = " + Value);
                    }
                }
                if (request.getParameterValues("jobId") != null) {
                    Value = request.getParameterValues("jobId")[i];
                    if (Value != null) {
                        Value = Jsoup.clean(Value, Whitelist.simpleText());
                    }
                    System.out.println("jobId = " + Value);
                    if (Value != null && !Value.equalsIgnoreCase("")) {
                        recruitment_bulk_mark_entryDTO.jobId = Long.parseLong(Value);
                        //job_applicant_applicationDTO.jobId = recruitment_bulk_mark_entryDTO.jobId;
                    } else {
                        System.out.println("FieldName has a null Value, not updating" + " = " + Value);
                    }
                }
                if (request.getParameterValues("levelId") != null) {
                    Value = request.getParameterValues("levelId")[i];
                    if (Value != null) {
                        Value = Jsoup.clean(Value, Whitelist.simpleText());
                    }
                    System.out.println("levelId = " + Value);
                    if (Value != null && !Value.equalsIgnoreCase("")) {
                        recruitment_bulk_mark_entryDTO.levelId = Long.parseLong(Value);
                        //job_applicant_applicationDTO.level = Integer.parseInt(Value);
                    } else {
                        System.out.println("FieldName has a null Value, not updating" + " = " + Value);
                    }
                }
                if (request.getParameterValues("roll") != null) {
                    Value = request.getParameterValues("roll")[i];
                    if (Value != null) {
                        Value = Jsoup.clean(Value, Whitelist.simpleText());
                    }
                    System.out.println("roll = " + Value);
                    if (Value != null && !Value.equalsIgnoreCase("")) {
                        recruitment_bulk_mark_entryDTO.roll = Long.parseLong(Value);
                        //job_applicant_applicationDTO.rollNumber = Value;
                        //job_applicant_applicationDTO.iD =  recruitment_bulk_mark_entryDTO.roll-1000;
                    } else {
                        System.out.println("FieldName has a null Value, not updating" + " = " + Value);
                    }
                }
                if (request.getParameterValues("marks") != null) {
                    Value = request.getParameterValues("marks")[i];
                    if (Value != null) {
                        Value = Jsoup.clean(Value, Whitelist.simpleText());
                    }
                    System.out.println("marks = " + Value);
                    if (Value != null && !Value.equalsIgnoreCase("")) {
                        recruitment_bulk_mark_entryDTO.marks = Double.parseDouble(Value);
                        job_applicant_applicationDTO.marks = Double.parseDouble(Value);

                    } else {
                        System.out.println("FieldName has a null Value, not updating" + " = " + Value);
                    }
                }
                if (request.getParameterValues("grade") != null) {
                    Value = request.getParameterValues("grade")[i];
                    if (Value != null) {
                        Value = Jsoup.clean(Value, Whitelist.simpleText());
                    }
                    System.out.println("grade = " + Value);
                    if (Value != null && !Value.equalsIgnoreCase("")) {
                        recruitment_bulk_mark_entryDTO.grade = (Value);
                    } else {
                        System.out.println("FieldName has a null Value, not updating" + " = " + Value);
                    }
                }
                if (request.getParameterValues("insertionDate") != null) {
                    Value = request.getParameterValues("insertionDate")[i];
                    if (Value != null) {
                        Value = Jsoup.clean(Value, Whitelist.simpleText());
                    }
                    System.out.println("insertionDate = " + Value);
                    if (Value != null && !Value.equalsIgnoreCase("")) {
                        recruitment_bulk_mark_entryDTO.insertionDate = Long.parseLong(Value);
                    } else {
                        System.out.println("FieldName has a null Value, not updating" + " = " + Value);
                    }
                }
                if (request.getParameterValues("insertedBy") != null) {
                    Value = request.getParameterValues("insertedBy")[i];
                    if (Value != null) {
                        Value = Jsoup.clean(Value, Whitelist.simpleText());
                    }
                    System.out.println("insertedBy = " + Value);
                    if (Value != null && !Value.equalsIgnoreCase("")) {
                        recruitment_bulk_mark_entryDTO.insertedBy = (Value);
                    } else {
                        System.out.println("FieldName has a null Value, not updating" + " = " + Value);
                    }
                }
                if (request.getParameterValues("modifiedBy") != null) {
                    Value = request.getParameterValues("modifiedBy")[i];
                    if (Value != null) {
                        Value = Jsoup.clean(Value, Whitelist.simpleText());
                    }
                    System.out.println("modifiedBy = " + Value);
                    if (Value != null && !Value.equalsIgnoreCase("")) {
                        recruitment_bulk_mark_entryDTO.modifiedBy = (Value);
                    } else {
                        System.out.println("FieldName has a null Value, not updating" + " = " + Value);
                    }
                }


                System.out.println("Done adding  addRecruitment_bulk_mark_entry dto = " + recruitment_bulk_mark_entryDTO);


                recruitment_bulk_mark_entryDAO.add(recruitment_bulk_mark_entryDTO);
                job_applicant_applicationDTO.lastModificationTime = System.currentTimeMillis();

                if(job_applicant_applicationDTO.marks >= passMark){
                    job_applicant_applicationDTO.isRejected = Boolean.FALSE;
                    job_applicant_applicationDTO.isSelected = isSelected(job_applicant_applicationDTO,userDTO);
                }
                else{
                    job_applicant_applicationDTO.isRejected = isRejected(job_applicant_applicationDTO,userDTO);
                    job_applicant_applicationDTO.isSelected = 0;
                }
                job_applicant_applicationDTO.lastModificationTime = System.currentTimeMillis();
                job_applicant_applicationDAO.update(job_applicant_applicationDTO);

            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        response.sendRedirect("Recruitment_bulk_mark_entryServlet?actionType=search");
    }

    public boolean isRejected(Job_applicant_applicationDTO job_applicant_applicationDTO,UserDTO userDTO) throws Exception {

        boolean rejected=true;
        Job_applicant_applicationServlet jobApplicantApplicationServlet = new Job_applicant_applicationServlet();

        job_applicant_applicationDAO.update(job_applicant_applicationDTO);

        // next steps data delete
        jobApplicantApplicationServlet.deleteNextLevelData(job_applicant_applicationDTO);
        return  rejected;

    }

    public int isSelected(Job_applicant_applicationDTO job_applicant_applicationDTO,UserDTO userDTO) throws Exception {

        int selected=0;
        Job_applicant_applicationServlet jobApplicantApplicationServlet = new Job_applicant_applicationServlet();

        int nextLevel = new JobSpecificExamTypeDao().
                getNextLevelFromJobIdAndCurrentLevel(job_applicant_applicationDTO.jobId, job_applicant_applicationDTO.level);
        if(nextLevel > 0){
            // next level data add
            jobApplicantApplicationServlet.deleteNextLevelData(job_applicant_applicationDTO);
            jobApplicantApplicationServlet.duplicateEntryWithNextLevel(job_applicant_applicationDTO, nextLevel, userDTO);
            selected = 1;
        } else {
            selected = 2;
        }
        job_applicant_applicationDAO.update(job_applicant_applicationDTO);

        // send msg
        Parliament_job_applicantDTO jobApplicantDTO = Parliament_job_applicantRepository.getInstance().
                getParliament_job_applicantDTOByID(job_applicant_applicationDTO.jobApplicantId);

        Recruitment_job_descriptionDTO jobDescriptionDTO =
                Recruitment_job_descriptionRepository.getInstance().
                        getRecruitment_job_descriptionDTOByID(job_applicant_applicationDTO.jobId);
        String msg = "";
        if(selected == 1){
            msg = "You have been shortlisted for next level of " + jobDescriptionDTO.jobTitleEn + " post.";
        } else if(selected == 2){
            msg = "You have been final-shortlisted for the post of " + jobDescriptionDTO.jobTitleEn + ".";
        }
        SmsService.send(jobApplicantDTO.contactNumber,
                String.format("Dear %s, " + msg, jobApplicantDTO.nameEn ));
        new Job_applicant_applicationServlet().sendMail(jobApplicantDTO.email,
                String.format("Dear %s, " + msg, jobApplicantDTO.nameEn ),
                "Parliament Recruitment");


        return  selected;

    }

    private void addRecruitment_bulk_mark_entry(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, boolean isPermanentTable) throws IOException {
        // TODO Auto-generated method stub
        try {
            request.setAttribute("failureMessage", "");
            System.out.println("%%%% addRecruitment_bulk_mark_entry");
            String path = getServletContext().getRealPath("/img2/");
            Recruitment_bulk_mark_entryDTO recruitment_bulk_mark_entryDTO;
            SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");

            if (addFlag == true) {
                recruitment_bulk_mark_entryDTO = new Recruitment_bulk_mark_entryDTO();
            } else {
                //recruitment_bulk_mark_entryDTO = (Recruitment_bulk_mark_entryDTO)recruitment_bulk_mark_entryDAO.getDTOByID(Long.parseLong(request.getParameter("id")));
                recruitment_bulk_mark_entryDTO = Recruitment_bulk_mark_entryRepository.getInstance().getRecruitment_bulk_mark_entryDTOByid(Long.parseLong(request.getParameter("id")));
                recruitment_bulk_mark_entryDTO.iD = recruitment_bulk_mark_entryDTO.id;

            }
            String FileNamePrefix;
            if (addFlag == true) {
                FileNamePrefix = UUID.randomUUID().toString().substring(0, 10);
            } else {
                FileNamePrefix = request.getParameter("id");
            }

            String Value = "";

            Value = request.getParameter("recruitmentTestNameId");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("recruitmentTestNameId = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                recruitment_bulk_mark_entryDTO.recruitmentTestNameId = Long.parseLong(Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("jobId");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("jobId = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                recruitment_bulk_mark_entryDTO.jobId = Long.parseLong(Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("levelId");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("levelId = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                recruitment_bulk_mark_entryDTO.levelId = Long.parseLong(Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("roll");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("roll = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                recruitment_bulk_mark_entryDTO.roll = Long.parseLong(Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("marks");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("marks = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                recruitment_bulk_mark_entryDTO.marks = Double.parseDouble(Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("grade");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("grade = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                recruitment_bulk_mark_entryDTO.grade = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            if (addFlag) {
                Calendar c = Calendar.getInstance();
                c.set(Calendar.HOUR_OF_DAY, 0);
                c.set(Calendar.MINUTE, 0);
                c.set(Calendar.SECOND, 0);
                c.set(Calendar.MILLISECOND, 0);

                recruitment_bulk_mark_entryDTO.insertionDate = c.getTimeInMillis();
            }


            Value = request.getParameter("insertedBy");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("insertedBy = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                recruitment_bulk_mark_entryDTO.insertedBy = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("modifiedBy");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("modifiedBy = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                recruitment_bulk_mark_entryDTO.modifiedBy = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            System.out.println("Done adding  addRecruitment_bulk_mark_entry dto = " + recruitment_bulk_mark_entryDTO);
            long returnedID = -1;

            if (isPermanentTable == false) //add new row for validation and make the old row outdated
            {
                recruitment_bulk_mark_entryDAO.setIsDeleted(recruitment_bulk_mark_entryDTO.iD, CommonDTO.OUTDATED);
                returnedID = recruitment_bulk_mark_entryDAO.add(recruitment_bulk_mark_entryDTO);
                recruitment_bulk_mark_entryDAO.setIsDeleted(returnedID, CommonDTO.WAITING_FOR_APPROVAL);
            } else if (addFlag == true) {
                returnedID = recruitment_bulk_mark_entryDAO.manageWriteOperations(recruitment_bulk_mark_entryDTO, SessionConstants.INSERT, -1, userDTO);
            } else {
                returnedID = recruitment_bulk_mark_entryDAO.manageWriteOperations(recruitment_bulk_mark_entryDTO, SessionConstants.UPDATE, -1, userDTO);
            }


            if (isPermanentTable) {
                String inPlaceSubmit = request.getParameter("inplacesubmit");

                if (inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase("")) {
                    getRecruitment_bulk_mark_entry(request, response, returnedID);
                } else {
                    response.sendRedirect("Recruitment_bulk_mark_entryServlet?actionType=search");
                }
            } else {
                commonRequestHandler.validate(recruitment_bulk_mark_entryDAO.getDTOByID(returnedID), request, response, userDTO);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void getRecruitment_bulk_mark_entry(HttpServletRequest request, HttpServletResponse response, long id) throws ServletException, IOException {
        System.out.println("in getRecruitment_bulk_mark_entry");
        Recruitment_bulk_mark_entryDTO recruitment_bulk_mark_entryDTO = null;
        try {
            //recruitment_bulk_mark_entryDTO = (Recruitment_bulk_mark_entryDTO)recruitment_bulk_mark_entryDAO.getDTOByID(id);
            recruitment_bulk_mark_entryDTO = Recruitment_bulk_mark_entryRepository.getInstance().getRecruitment_bulk_mark_entryDTOByid(id);
//			request.setAttribute("ID", recruitment_bulk_mark_entryDTO.iD);
            request.setAttribute("ID", recruitment_bulk_mark_entryDTO.id);
            request.setAttribute("recruitment_bulk_mark_entryDTO", recruitment_bulk_mark_entryDTO);
            request.setAttribute("recruitment_bulk_mark_entryDAO", recruitment_bulk_mark_entryDAO);

            String URL = "";

            String inPlaceEdit = request.getParameter("inplaceedit");
            String inPlaceSubmit = request.getParameter("inplacesubmit");
            String getBodyOnly = request.getParameter("getBodyOnly");

            if (inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase("")) {
                URL = "recruitment_bulk_mark_entry/recruitment_bulk_mark_entryInPlaceEdit.jsp";
                request.setAttribute("inplaceedit", "");
            } else if (inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase("")) {
                URL = "recruitment_bulk_mark_entry/recruitment_bulk_mark_entrySearchRow.jsp";
                request.setAttribute("inplacesubmit", "");
            } else {
                if (getBodyOnly != null && !getBodyOnly.equalsIgnoreCase("")) {
                    URL = "recruitment_bulk_mark_entry/recruitment_bulk_mark_entryEditBody.jsp?actionType=edit";
                } else {
                    URL = "recruitment_bulk_mark_entry/recruitment_bulk_mark_entryEdit.jsp?actionType=edit";
                }
            }

            RequestDispatcher rd = request.getRequestDispatcher(URL);
            rd.forward(request, response);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void getRecruitment_bulk_mark_entry(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        getRecruitment_bulk_mark_entry(request, response, Long.parseLong(request.getParameter("ID")));
    }

    private void searchRecruitment_bulk_mark_entry(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException {
        System.out.println("in  searchRecruitment_bulk_mark_entry 1");
        LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        String ajax = request.getParameter("ajax");
        boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);

        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
                SessionConstants.NAV_RECRUITMENT_BULK_MARK_ENTRY,
                request,
                recruitment_bulk_mark_entryDAO,
                SessionConstants.VIEW_RECRUITMENT_BULK_MARK_ENTRY,
                SessionConstants.SEARCH_RECRUITMENT_BULK_MARK_ENTRY,
                tableName,
                isPermanent,
                userDTO,
                filter,
                true);
        try {
            System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        } catch (Exception e) {
            System.out.println("failed to dojob" + e);
        }

        request.setAttribute("recruitment_bulk_mark_entryDAO", recruitment_bulk_mark_entryDAO);
        RequestDispatcher rd;
        if (!isPermanent) {
            if (hasAjax == false) {
                System.out.println("Going to recruitment_bulk_mark_entry/recruitment_bulk_mark_entryApproval.jsp");
                rd = request.getRequestDispatcher("recruitment_bulk_mark_entry/recruitment_bulk_mark_entryApproval.jsp");
            } else {
                System.out.println("Going to recruitment_bulk_mark_entry/recruitment_bulk_mark_entryApprovalForm.jsp");
                rd = request.getRequestDispatcher("recruitment_bulk_mark_entry/recruitment_bulk_mark_entryApprovalForm.jsp");
            }
        } else {
            if (hasAjax == false) {
                System.out.println("Going to recruitment_bulk_mark_entry/recruitment_bulk_mark_entrySearch.jsp");
                rd = request.getRequestDispatcher("recruitment_bulk_mark_entry/recruitment_bulk_mark_entrySearch.jsp");
            } else {
                System.out.println("Going to recruitment_bulk_mark_entry/recruitment_bulk_mark_entrySearchForm.jsp");
                rd = request.getRequestDispatcher("recruitment_bulk_mark_entry/recruitment_bulk_mark_entrySearchForm.jsp");
            }
        }
        rd.forward(request, response);
    }

    void getCandidateList(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher rd;
        rd = request.getRequestDispatcher("recruitment_bulk_mark_entry/recruitment_job_specific_candidate_info_excel_list.jsp");
        rd.forward(request, response);
    }


}

