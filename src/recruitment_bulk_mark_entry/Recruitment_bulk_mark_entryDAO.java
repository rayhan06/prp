package recruitment_bulk_mark_entry;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;

import common.ConnectionAndStatementUtil;
import sessionmanager.SessionConstants;
import java.sql.SQLException;


import org.apache.log4j.Logger;

import dbm.*;

import repository.RepositoryManager;

import util.*;
import pb.*;
import user.UserDTO;
import vm_vehicle_office_assignment.Vm_vehicle_office_assignmentDTO;

public class Recruitment_bulk_mark_entryDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());
	

	
	public Recruitment_bulk_mark_entryDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";
		commonMaps = new Recruitment_bulk_mark_entryMAPS(tableName);
		columnNames = new String[] 
		{
			"ID",
			"recruitment_test_name_id",
			"job_id",
			"level_id",
			"roll",
			"marks",
			"grade",
			"insertion_date",
			"inserted_by",
			"modified_by",
			"isDeleted",
			"lastModificationTime"
		};
	}
	
	public Recruitment_bulk_mark_entryDAO()
	{
		this("recruitment_bulk_mark_entry");		
	}
	
	public void setSearchColumn(Recruitment_bulk_mark_entryDTO recruitment_bulk_mark_entryDTO)
	{
		recruitment_bulk_mark_entryDTO.searchColumn = "";
		recruitment_bulk_mark_entryDTO.searchColumn += recruitment_bulk_mark_entryDTO.roll + " ";
		recruitment_bulk_mark_entryDTO.searchColumn += recruitment_bulk_mark_entryDTO.marks + " ";
		recruitment_bulk_mark_entryDTO.searchColumn += recruitment_bulk_mark_entryDTO.grade + " ";
		
	}
	
	public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException
	{
		Recruitment_bulk_mark_entryDTO recruitment_bulk_mark_entryDTO = (Recruitment_bulk_mark_entryDTO)commonDTO;
		int index = 1;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(recruitment_bulk_mark_entryDTO);
		if(isInsert)
		{
			ps.setObject(index++,recruitment_bulk_mark_entryDTO.iD);
		}
		ps.setObject(index++,recruitment_bulk_mark_entryDTO.recruitmentTestNameId);
		ps.setObject(index++,recruitment_bulk_mark_entryDTO.jobId);
		ps.setObject(index++,recruitment_bulk_mark_entryDTO.levelId);
		ps.setObject(index++,recruitment_bulk_mark_entryDTO.roll);
		ps.setObject(index++,recruitment_bulk_mark_entryDTO.marks);
		ps.setObject(index++,recruitment_bulk_mark_entryDTO.grade);
		ps.setObject(index++,recruitment_bulk_mark_entryDTO.insertionDate);
		ps.setObject(index++,recruitment_bulk_mark_entryDTO.insertedBy);
		ps.setObject(index++,recruitment_bulk_mark_entryDTO.modifiedBy);
		if(isInsert)
		{
			ps.setObject(index++, 0);
			ps.setObject(index++, lastModificationTime);
		}
	}

	public Recruitment_bulk_mark_entryDTO build(ResultSet rs)
	{
		try
		{
			Recruitment_bulk_mark_entryDTO recruitment_bulk_mark_entryDTO = new Recruitment_bulk_mark_entryDTO();
			recruitment_bulk_mark_entryDTO.id = rs.getLong("id");
			recruitment_bulk_mark_entryDTO.recruitmentTestNameId = rs.getLong("recruitment_test_name_id");
			recruitment_bulk_mark_entryDTO.jobId = rs.getLong("job_id");
			recruitment_bulk_mark_entryDTO.levelId = rs.getLong("level_id");
			recruitment_bulk_mark_entryDTO.roll = rs.getLong("roll");
			recruitment_bulk_mark_entryDTO.marks = rs.getDouble("marks");
			recruitment_bulk_mark_entryDTO.grade = rs.getString("grade");
			recruitment_bulk_mark_entryDTO.insertionDate = rs.getLong("insertion_date");
			recruitment_bulk_mark_entryDTO.insertedBy = rs.getString("inserted_by");
			recruitment_bulk_mark_entryDTO.modifiedBy = rs.getString("modified_by");
			recruitment_bulk_mark_entryDTO.isDeleted = rs.getInt("isDeleted");
			recruitment_bulk_mark_entryDTO.lastModificationTime = rs.getLong("lastModificationTime");
			return recruitment_bulk_mark_entryDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
	
	public void get(Recruitment_bulk_mark_entryDTO recruitment_bulk_mark_entryDTO, ResultSet rs) throws SQLException
	{

	}
	
	
	
	
		
	

	//need another getter for repository
	public Recruitment_bulk_mark_entryDTO getDTOByID (long ID)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID = ? ";
		return 	ConnectionAndStatementUtil.getT(sql, Arrays.asList(ID), this::build);
	}


	
	
	
	
	public List<Recruitment_bulk_mark_entryDTO> getDTOs(Collection recordIDs){
		if(recordIDs.isEmpty()){
			return new ArrayList<>();
		}
		String sql = "SELECT * FROM " + tableName + " WHERE ID IN ( ";
		for(int i = 0;i<recordIDs.size();i++){
			if(i!=0){
				sql+=",";
			}
			sql+= " ? ";
		}
		sql+=")  order by lastModificationTime desc";
		printSql(sql);
		return ConnectionAndStatementUtil.getListOfT(sql, Arrays.asList(recordIDs.toArray()),this::build);
	
	}
	
	

	
	
	
	//add repository
	public List<Recruitment_bulk_mark_entryDTO> getAllRecruitment_bulk_mark_entry (boolean isFirstReload)
    {
		List<Recruitment_bulk_mark_entryDTO> recruitment_bulk_mark_entryDTOList = new ArrayList<>();

		String sql = "SELECT * FROM recruitment_bulk_mark_entry";
		sql += " WHERE ";
	

		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by recruitment_bulk_mark_entry.lastModificationTime desc";
		printSql(sql);
		return ConnectionAndStatementUtil.getListOfT(sql, this::build);
    }

	

	public String getSqlWithSearchCriteriaForNormalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
			UserDTO userDTO, String filter, boolean tableHasJobCat, List<Object> objectList)
    {
		boolean viewAll = false;
		
		if(p_searchCriteria != null && p_searchCriteria.get("ViewAll") != null)
		{
			System.out.println("ViewAll = " + p_searchCriteria.get("ViewAll"));
			viewAll = true;
		}
		else
		{
			System.out.println("ViewAll is null ");
		}
		
		String sql = "SELECT ";
		if(category == GETIDS)
		{
			sql += " " + tableName + ".ID as ID ";
		}
		else if(category == GETCOUNT)
		{
			sql += " count( " + tableName + ".ID) as countID ";
		}
		else if(category == GETDTOS)
		{
			sql += " distinct " + tableName + ".* ";
		}
		sql += "FROM " + tableName + " ";
		sql += joinSQL;
		

		
		String AnyfieldSql = "";
		String AllFieldSql = "";
		
		if(p_searchCriteria != null)
		{
						
			
			Enumeration names = p_searchCriteria.keys();
			String str, value;
			
			AnyfieldSql = "(";
			
			if(p_searchCriteria.get("AnyField")!= null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase(""))
			{
				//AnyfieldSql+= tableName + ".search_column like '%" + p_searchCriteria.get("AnyField").toString() + "%'";
				AnyfieldSql+= tableName + ".search_column like ? ";
				objectList.add("%" + p_searchCriteria.get("AnyField").toString() + "%");
			}
			AnyfieldSql += ")";
			System.out.println("AnyfieldSql = " + AnyfieldSql);
			
			AllFieldSql = "(";
			int i = 0;
			while(names.hasMoreElements())
			{				
				str = (String) names.nextElement();
				value = (String)p_searchCriteria.get(str);
		        System.out.println(str + ": " + value);
				if(value != null && !value.equalsIgnoreCase("") && (
						 str.equals("roll")
						|| str.equals("job_id")
						|| str.equals("level_id")
						|| str.equals("marks")
						|| str.equals("grade")
						|| str.equals("insertion_date_start")
						|| str.equals("insertion_date_end")
				)
						
				)
				{
					if(p_searchCriteria.get(str).equals("any"))
					{
						continue;
					}

					if( i > 0)
					{
						AllFieldSql+= " AND  ";
					}
					
					 if(str.equals("roll"))
					{
						//AllFieldSql += "" + tableName + ".roll like '%" + p_searchCriteria.get(str) + "%'";
						AllFieldSql += "" + tableName + ".roll like ? ";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("job_id"))
					{
						//AllFieldSql += "" + tableName + ".job_id like '%" + p_searchCriteria.get(str) + "%'";
						AllFieldSql += "" + tableName + ".job_id like ? ";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					 else if(str.equals("level_id"))
					 {
						 //AllFieldSql += "" + tableName + ".level_id like '%" + p_searchCriteria.get(str) + "%'";
						 AllFieldSql += "" + tableName + ".level_id like ? ";
						 objectList.add("%" + p_searchCriteria.get(str) + "%");
						 i ++;
					 }
					else if(str.equals("marks"))
					{
						//AllFieldSql += "" + tableName + ".marks like '%" + p_searchCriteria.get(str) + "%'";
						AllFieldSql += "" + tableName + ".marks like ? ";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("grade"))
					{
						//AllFieldSql += "" + tableName + ".grade like '%" + p_searchCriteria.get(str) + "%'";
						AllFieldSql += "" + tableName + ".grade like ? ";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("insertion_date_start"))
					{
						//AllFieldSql += "" + tableName + ".insertion_date >= " + p_searchCriteria.get(str);
						AllFieldSql += "" + tableName + ".insertion_date >= ? ";
						objectList.add(p_searchCriteria.get(str));
						i ++;
					}
					else if(str.equals("insertion_date_end"))
					{
						//AllFieldSql += "" + tableName + ".insertion_date <= " + p_searchCriteria.get(str);
						AllFieldSql += "" + tableName + ".insertion_date <= ? ";
						objectList.add(p_searchCriteria.get(str));
						i ++;
					}
					
					
				}
			}
			
			AllFieldSql += ")";
			System.out.println("AllFieldSql = " + AllFieldSql);
			
			
		}
		
		
		sql += " WHERE ";
		
		sql += " (" + tableName + ".isDeleted = 0 ";			
		sql += ")";
		
		
		if(!filter.equalsIgnoreCase(""))
		{
			sql += " and " + filter + " ";
		}
		
		if(!AnyfieldSql.equals("()") && !AnyfieldSql.equals(""))
		{
			sql += " AND " + AnyfieldSql;
			
		}
		if(!AllFieldSql.equals("()") && !AllFieldSql.equals(""))
		{			
			sql += " AND " + AllFieldSql;
		}
		
	
			
		sql += " order by " + tableName + ".lastModificationTime desc ";

		printSql(sql);
		
		if(limit >= 0)
		{
			sql += " limit " + limit;
		}
		if(offset >= 0)
		{
			sql += " offset " + offset;
		}
		
		System.out.println("-------------- sql = " + sql);
		
		return sql;
    }
	


	public int JobLevelIdMarkEntryExistence (long job_id,long level_id)
	{

		String sql = "SELECT * ";

		sql += " FROM " + tableName;

		sql += " WHERE job_id = ?  and level_id = ? ";

		sql += " AND isDeleted=0 ";

		printSql(sql);

		return ConnectionAndStatementUtil.
				getListOfT(sql, Arrays.asList(job_id, level_id), this::build).size() > 0 ? 0 : 1;
	}

	//TESTING JOB ID AND LEVEL ID EXISTENCE

	public int JobLevelIdExistence (int job_id,int level_id)
	{
		String sql = "SELECT * ";
		sql += " FROM " + tableName;
		sql += " WHERE job_id = ? and level_id = ? ";
		sql += " AND isDeleted=0 ";
		printSql(sql);

		return 	ConnectionAndStatementUtil.getListOfT
				(sql, Arrays.asList(job_id, level_id), this::build).size() > 0 ? 1 : 0;
	}
				
}
	