package recruitment_bulk_mark_entry;
import java.util.*; 
import util.*;


public class Recruitment_bulk_mark_entryMAPS extends CommonMaps
{	
	public Recruitment_bulk_mark_entryMAPS(String tableName)
	{
		

		java_DTO_map.put("id".toLowerCase(), "id".toLowerCase());
		java_DTO_map.put("jobId".toLowerCase(), "jobId".toLowerCase());
		java_DTO_map.put("levelId".toLowerCase(), "levelId".toLowerCase());
		java_DTO_map.put("roll".toLowerCase(), "roll".toLowerCase());
		java_DTO_map.put("marks".toLowerCase(), "marks".toLowerCase());
		java_DTO_map.put("grade".toLowerCase(), "grade".toLowerCase());
		java_DTO_map.put("insertionDate".toLowerCase(), "insertionDate".toLowerCase());
		java_DTO_map.put("insertedBy".toLowerCase(), "insertedBy".toLowerCase());
		java_DTO_map.put("modifiedBy".toLowerCase(), "modifiedBy".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("roll".toLowerCase(), "roll".toLowerCase());
		java_SQL_map.put("marks".toLowerCase(), "marks".toLowerCase());
		java_SQL_map.put("grade".toLowerCase(), "grade".toLowerCase());

		java_Text_map.put("Id".toLowerCase(), "id".toLowerCase());
		java_Text_map.put("Job Id".toLowerCase(), "jobId".toLowerCase());
		java_Text_map.put("Level Id".toLowerCase(), "levelId".toLowerCase());
		java_Text_map.put("Roll".toLowerCase(), "roll".toLowerCase());
		java_Text_map.put("Marks".toLowerCase(), "marks".toLowerCase());
		java_Text_map.put("Grade".toLowerCase(), "grade".toLowerCase());
		java_Text_map.put("Insertion Date".toLowerCase(), "insertionDate".toLowerCase());
		java_Text_map.put("Inserted By".toLowerCase(), "insertedBy".toLowerCase());
		java_Text_map.put("Modified By".toLowerCase(), "modifiedBy".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}