package recruitment_bulk_mark_entry;
import java.util.*; 
import util.*; 


public class Recruitment_bulk_mark_entryDTO extends CommonDTO
{

	public long id = 0;
	public long recruitmentTestNameId = 0;
	public long jobId = 0;
	public long levelId = 0;
	public long roll = 0;
	public double marks = 0;
    public String grade = "";
	public long insertionDate = 0;
    public String insertedBy = "";
    public String modifiedBy = "";
	
	
    @Override
	public String toString() {
            return "$Recruitment_bulk_mark_entryDTO[" +
            " id = " + id +
            " jobId = " + jobId +
            " levelId = " + levelId +
            " roll = " + roll +
            " marks = " + marks +
            " grade = " + grade +
            " insertionDate = " + insertionDate +
            " insertedBy = " + insertedBy +
            " modifiedBy = " + modifiedBy +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}