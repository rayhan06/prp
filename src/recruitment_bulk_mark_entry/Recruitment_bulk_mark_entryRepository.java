package recruitment_bulk_mark_entry;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import com.google.gson.Gson;
import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;
import vm_fuel_request.Vm_fuel_requestDTO;


public class Recruitment_bulk_mark_entryRepository implements Repository {
	Recruitment_bulk_mark_entryDAO recruitment_bulk_mark_entryDAO = null;
	Gson gson;
	
	public void setDAO(Recruitment_bulk_mark_entryDAO recruitment_bulk_mark_entryDAO)
	{
		this.recruitment_bulk_mark_entryDAO = recruitment_bulk_mark_entryDAO;
		gson = new Gson();
	}
	
	
	static Logger logger = Logger.getLogger(Recruitment_bulk_mark_entryRepository.class);
	Map<Long, Recruitment_bulk_mark_entryDTO>mapOfRecruitment_bulk_mark_entryDTOToid;

	Map<Long, Set<Recruitment_bulk_mark_entryDTO> >mapOfRecruitment_bulk_mark_entryDTOToJobId;
	Map<Long, Set<Recruitment_bulk_mark_entryDTO> >mapOfRecruitment_bulk_mark_entryDTOToLevelId;



	static Recruitment_bulk_mark_entryRepository instance = null;  
	private Recruitment_bulk_mark_entryRepository(){

		mapOfRecruitment_bulk_mark_entryDTOToid = new ConcurrentHashMap<>();
		mapOfRecruitment_bulk_mark_entryDTOToJobId = new ConcurrentHashMap<>();
		mapOfRecruitment_bulk_mark_entryDTOToLevelId = new ConcurrentHashMap<>();

		setDAO(new Recruitment_bulk_mark_entryDAO());

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Recruitment_bulk_mark_entryRepository getInstance(){
		if (instance == null){
			instance = new Recruitment_bulk_mark_entryRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(recruitment_bulk_mark_entryDAO == null)
		{
			return;
		}
		try {
			List<Recruitment_bulk_mark_entryDTO> recruitment_bulk_mark_entryDTOs = recruitment_bulk_mark_entryDAO.getAllRecruitment_bulk_mark_entry(reloadAll);
			for(Recruitment_bulk_mark_entryDTO recruitment_bulk_mark_entryDTO : recruitment_bulk_mark_entryDTOs) {
				Recruitment_bulk_mark_entryDTO oldRecruitment_bulk_mark_entryDTO = getRecruitment_bulk_mark_entryDTOByIDWithoutClone(recruitment_bulk_mark_entryDTO.id);
				if( oldRecruitment_bulk_mark_entryDTO != null ) {
					mapOfRecruitment_bulk_mark_entryDTOToid.remove(oldRecruitment_bulk_mark_entryDTO.id);

					if(mapOfRecruitment_bulk_mark_entryDTOToJobId.containsKey(oldRecruitment_bulk_mark_entryDTO.jobId)) {
						mapOfRecruitment_bulk_mark_entryDTOToJobId.get(oldRecruitment_bulk_mark_entryDTO.jobId).remove(oldRecruitment_bulk_mark_entryDTO);
					}
					if(mapOfRecruitment_bulk_mark_entryDTOToJobId.get(oldRecruitment_bulk_mark_entryDTO.jobId).isEmpty()) {
						mapOfRecruitment_bulk_mark_entryDTOToJobId.remove(oldRecruitment_bulk_mark_entryDTO.jobId);
					}

					if(mapOfRecruitment_bulk_mark_entryDTOToLevelId.containsKey(oldRecruitment_bulk_mark_entryDTO.levelId)) {
						mapOfRecruitment_bulk_mark_entryDTOToLevelId.get(oldRecruitment_bulk_mark_entryDTO.levelId).remove(oldRecruitment_bulk_mark_entryDTO);
					}
					if(mapOfRecruitment_bulk_mark_entryDTOToLevelId.get(oldRecruitment_bulk_mark_entryDTO.levelId).isEmpty()) {
						mapOfRecruitment_bulk_mark_entryDTOToLevelId.remove(oldRecruitment_bulk_mark_entryDTO.levelId);
					}

				}
				if(recruitment_bulk_mark_entryDTO.isDeleted == 0) 
				{
					
					mapOfRecruitment_bulk_mark_entryDTOToid.put(recruitment_bulk_mark_entryDTO.id, recruitment_bulk_mark_entryDTO);

					if( ! mapOfRecruitment_bulk_mark_entryDTOToJobId.containsKey(recruitment_bulk_mark_entryDTO.jobId)) {
						mapOfRecruitment_bulk_mark_entryDTOToJobId.put(recruitment_bulk_mark_entryDTO.jobId, new HashSet<>());
					}
					mapOfRecruitment_bulk_mark_entryDTOToJobId.get(recruitment_bulk_mark_entryDTO.jobId).add(recruitment_bulk_mark_entryDTO);

					if( ! mapOfRecruitment_bulk_mark_entryDTOToLevelId.containsKey(recruitment_bulk_mark_entryDTO.levelId)) {
						mapOfRecruitment_bulk_mark_entryDTOToLevelId.put(recruitment_bulk_mark_entryDTO.levelId, new HashSet<>());
					}
					mapOfRecruitment_bulk_mark_entryDTOToLevelId.get(recruitment_bulk_mark_entryDTO.levelId).add(recruitment_bulk_mark_entryDTO);

					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}

	public Recruitment_bulk_mark_entryDTO clone(Recruitment_bulk_mark_entryDTO dto) {
		String raw = gson.toJson(dto);
		return gson.fromJson(raw, Recruitment_bulk_mark_entryDTO.class);
	}

	public List<Recruitment_bulk_mark_entryDTO> clone(List<Recruitment_bulk_mark_entryDTO> dtoList) {
		return dtoList
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}

	public Recruitment_bulk_mark_entryDTO getRecruitment_bulk_mark_entryDTOByIDWithoutClone( long ID){
		return mapOfRecruitment_bulk_mark_entryDTOToid.get(ID);
	}
	
	public List<Recruitment_bulk_mark_entryDTO> getRecruitment_bulk_mark_entryList() {
		List <Recruitment_bulk_mark_entryDTO> recruitment_bulk_mark_entrys = new ArrayList<Recruitment_bulk_mark_entryDTO>(this.mapOfRecruitment_bulk_mark_entryDTOToid.values());
		return recruitment_bulk_mark_entrys;
	}
	
	
	public Recruitment_bulk_mark_entryDTO getRecruitment_bulk_mark_entryDTOByid( long id){
		return clone(mapOfRecruitment_bulk_mark_entryDTOToid.get(id));
	}

	public List<Recruitment_bulk_mark_entryDTO> getRecruitment_bulk_mark_entryDTOByJobIdAndLevel(long job_id, long level) {
		List<Recruitment_bulk_mark_entryDTO> recruitment_bulk_mark_entryDTOS = (new ArrayList<>( mapOfRecruitment_bulk_mark_entryDTOToJobId.getOrDefault(job_id,new HashSet<>())));
		return recruitment_bulk_mark_entryDTOS
				.stream()
				.filter(recruitment_bulk_mark_entryDTO -> recruitment_bulk_mark_entryDTO.levelId == level)
				.map(recruitment_bulk_mark_entryDTO -> clone(recruitment_bulk_mark_entryDTO))
				.collect(Collectors.toList());
	}


	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "recruitment_bulk_mark_entry";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


