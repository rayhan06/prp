package employee_office_hour;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class Employee_office_hourRepository implements Repository {
	Employee_office_hourDAO employee_office_hourDAO = null;
	
	public void setDAO(Employee_office_hourDAO employee_office_hourDAO)
	{
		this.employee_office_hourDAO = employee_office_hourDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Employee_office_hourRepository.class);
	Map<Long, Employee_office_hourDTO>mapOfEmployee_office_hourDTOToiD;
	Map<Integer, Set<Employee_office_hourDTO> >mapOfEmployee_office_hourDTOToofficeHourCat;
	Map<String, Set<Employee_office_hourDTO> >mapOfEmployee_office_hourDTOTostartTime;
	Map<String, Set<Employee_office_hourDTO> >mapOfEmployee_office_hourDTOToendTime;
	Map<Long, Set<Employee_office_hourDTO> >mapOfEmployee_office_hourDTOTostartDate;
	Map<Long, Set<Employee_office_hourDTO> >mapOfEmployee_office_hourDTOToendDate;
	Map<Integer, Set<Employee_office_hourDTO> >mapOfEmployee_office_hourDTOToisActive;
	Map<Long, Set<Employee_office_hourDTO> >mapOfEmployee_office_hourDTOToinsertedBy;
	Map<Long, Set<Employee_office_hourDTO> >mapOfEmployee_office_hourDTOTomodifiedBy;
	Map<Long, Set<Employee_office_hourDTO> >mapOfEmployee_office_hourDTOTolastModificationTime;


	static Employee_office_hourRepository instance = null;  
	private Employee_office_hourRepository(){
		mapOfEmployee_office_hourDTOToiD = new ConcurrentHashMap<>();
		mapOfEmployee_office_hourDTOToofficeHourCat = new ConcurrentHashMap<>();
		mapOfEmployee_office_hourDTOTostartTime = new ConcurrentHashMap<>();
		mapOfEmployee_office_hourDTOToendTime = new ConcurrentHashMap<>();
		mapOfEmployee_office_hourDTOTostartDate = new ConcurrentHashMap<>();
		mapOfEmployee_office_hourDTOToendDate = new ConcurrentHashMap<>();
		mapOfEmployee_office_hourDTOToisActive = new ConcurrentHashMap<>();
		mapOfEmployee_office_hourDTOToinsertedBy = new ConcurrentHashMap<>();
		mapOfEmployee_office_hourDTOTomodifiedBy = new ConcurrentHashMap<>();
		mapOfEmployee_office_hourDTOTolastModificationTime = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Employee_office_hourRepository getInstance(){
		if (instance == null){
			instance = new Employee_office_hourRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(employee_office_hourDAO == null)
		{
			return;
		}
		try {
			List<Employee_office_hourDTO> employee_office_hourDTOs = employee_office_hourDAO.getAllEmployee_office_hour(reloadAll);
			for(Employee_office_hourDTO employee_office_hourDTO : employee_office_hourDTOs) {
				Employee_office_hourDTO oldEmployee_office_hourDTO = getEmployee_office_hourDTOByID(employee_office_hourDTO.iD);
				if( oldEmployee_office_hourDTO != null ) {
					mapOfEmployee_office_hourDTOToiD.remove(oldEmployee_office_hourDTO.iD);
				
					if(mapOfEmployee_office_hourDTOToofficeHourCat.containsKey(oldEmployee_office_hourDTO.officeHourCat)) {
						mapOfEmployee_office_hourDTOToofficeHourCat.get(oldEmployee_office_hourDTO.officeHourCat).remove(oldEmployee_office_hourDTO);
					}
					if(mapOfEmployee_office_hourDTOToofficeHourCat.get(oldEmployee_office_hourDTO.officeHourCat).isEmpty()) {
						mapOfEmployee_office_hourDTOToofficeHourCat.remove(oldEmployee_office_hourDTO.officeHourCat);
					}
					
					if(mapOfEmployee_office_hourDTOTostartTime.containsKey(oldEmployee_office_hourDTO.startTime)) {
						mapOfEmployee_office_hourDTOTostartTime.get(oldEmployee_office_hourDTO.startTime).remove(oldEmployee_office_hourDTO);
					}
					if(mapOfEmployee_office_hourDTOTostartTime.get(oldEmployee_office_hourDTO.startTime).isEmpty()) {
						mapOfEmployee_office_hourDTOTostartTime.remove(oldEmployee_office_hourDTO.startTime);
					}
					
					if(mapOfEmployee_office_hourDTOToendTime.containsKey(oldEmployee_office_hourDTO.endTime)) {
						mapOfEmployee_office_hourDTOToendTime.get(oldEmployee_office_hourDTO.endTime).remove(oldEmployee_office_hourDTO);
					}
					if(mapOfEmployee_office_hourDTOToendTime.get(oldEmployee_office_hourDTO.endTime).isEmpty()) {
						mapOfEmployee_office_hourDTOToendTime.remove(oldEmployee_office_hourDTO.endTime);
					}
					
					if(mapOfEmployee_office_hourDTOTostartDate.containsKey(oldEmployee_office_hourDTO.startDate)) {
						mapOfEmployee_office_hourDTOTostartDate.get(oldEmployee_office_hourDTO.startDate).remove(oldEmployee_office_hourDTO);
					}
					if(mapOfEmployee_office_hourDTOTostartDate.get(oldEmployee_office_hourDTO.startDate).isEmpty()) {
						mapOfEmployee_office_hourDTOTostartDate.remove(oldEmployee_office_hourDTO.startDate);
					}
					
					if(mapOfEmployee_office_hourDTOToendDate.containsKey(oldEmployee_office_hourDTO.endDate)) {
						mapOfEmployee_office_hourDTOToendDate.get(oldEmployee_office_hourDTO.endDate).remove(oldEmployee_office_hourDTO);
					}
					if(mapOfEmployee_office_hourDTOToendDate.get(oldEmployee_office_hourDTO.endDate).isEmpty()) {
						mapOfEmployee_office_hourDTOToendDate.remove(oldEmployee_office_hourDTO.endDate);
					}
					
					if(mapOfEmployee_office_hourDTOToisActive.containsKey(oldEmployee_office_hourDTO.isActive)) {
						mapOfEmployee_office_hourDTOToisActive.get(oldEmployee_office_hourDTO.isActive).remove(oldEmployee_office_hourDTO);
					}
					if(mapOfEmployee_office_hourDTOToisActive.get(oldEmployee_office_hourDTO.isActive).isEmpty()) {
						mapOfEmployee_office_hourDTOToisActive.remove(oldEmployee_office_hourDTO.isActive);
					}
					
					if(mapOfEmployee_office_hourDTOToinsertedBy.containsKey(oldEmployee_office_hourDTO.insertedBy)) {
						mapOfEmployee_office_hourDTOToinsertedBy.get(oldEmployee_office_hourDTO.insertedBy).remove(oldEmployee_office_hourDTO);
					}
					if(mapOfEmployee_office_hourDTOToinsertedBy.get(oldEmployee_office_hourDTO.insertedBy).isEmpty()) {
						mapOfEmployee_office_hourDTOToinsertedBy.remove(oldEmployee_office_hourDTO.insertedBy);
					}
					
					if(mapOfEmployee_office_hourDTOTomodifiedBy.containsKey(oldEmployee_office_hourDTO.modifiedBy)) {
						mapOfEmployee_office_hourDTOTomodifiedBy.get(oldEmployee_office_hourDTO.modifiedBy).remove(oldEmployee_office_hourDTO);
					}
					if(mapOfEmployee_office_hourDTOTomodifiedBy.get(oldEmployee_office_hourDTO.modifiedBy).isEmpty()) {
						mapOfEmployee_office_hourDTOTomodifiedBy.remove(oldEmployee_office_hourDTO.modifiedBy);
					}
					
					if(mapOfEmployee_office_hourDTOTolastModificationTime.containsKey(oldEmployee_office_hourDTO.lastModificationTime)) {
						mapOfEmployee_office_hourDTOTolastModificationTime.get(oldEmployee_office_hourDTO.lastModificationTime).remove(oldEmployee_office_hourDTO);
					}
					if(mapOfEmployee_office_hourDTOTolastModificationTime.get(oldEmployee_office_hourDTO.lastModificationTime).isEmpty()) {
						mapOfEmployee_office_hourDTOTolastModificationTime.remove(oldEmployee_office_hourDTO.lastModificationTime);
					}
					
					
				}
				if(employee_office_hourDTO.isDeleted == 0) 
				{
					
					mapOfEmployee_office_hourDTOToiD.put(employee_office_hourDTO.iD, employee_office_hourDTO);
				
					if( ! mapOfEmployee_office_hourDTOToofficeHourCat.containsKey(employee_office_hourDTO.officeHourCat)) {
						mapOfEmployee_office_hourDTOToofficeHourCat.put(employee_office_hourDTO.officeHourCat, new HashSet<>());
					}
					mapOfEmployee_office_hourDTOToofficeHourCat.get(employee_office_hourDTO.officeHourCat).add(employee_office_hourDTO);
					
					if( ! mapOfEmployee_office_hourDTOTostartTime.containsKey(employee_office_hourDTO.startTime)) {
						mapOfEmployee_office_hourDTOTostartTime.put(employee_office_hourDTO.startTime, new HashSet<>());
					}
					mapOfEmployee_office_hourDTOTostartTime.get(employee_office_hourDTO.startTime).add(employee_office_hourDTO);
					
					if( ! mapOfEmployee_office_hourDTOToendTime.containsKey(employee_office_hourDTO.endTime)) {
						mapOfEmployee_office_hourDTOToendTime.put(employee_office_hourDTO.endTime, new HashSet<>());
					}
					mapOfEmployee_office_hourDTOToendTime.get(employee_office_hourDTO.endTime).add(employee_office_hourDTO);
					
					if( ! mapOfEmployee_office_hourDTOTostartDate.containsKey(employee_office_hourDTO.startDate)) {
						mapOfEmployee_office_hourDTOTostartDate.put(employee_office_hourDTO.startDate, new HashSet<>());
					}
					mapOfEmployee_office_hourDTOTostartDate.get(employee_office_hourDTO.startDate).add(employee_office_hourDTO);
					
					if( ! mapOfEmployee_office_hourDTOToendDate.containsKey(employee_office_hourDTO.endDate)) {
						mapOfEmployee_office_hourDTOToendDate.put(employee_office_hourDTO.endDate, new HashSet<>());
					}
					mapOfEmployee_office_hourDTOToendDate.get(employee_office_hourDTO.endDate).add(employee_office_hourDTO);
					
					if( ! mapOfEmployee_office_hourDTOToisActive.containsKey(employee_office_hourDTO.isActive)) {
						mapOfEmployee_office_hourDTOToisActive.put(employee_office_hourDTO.isActive, new HashSet<>());
					}
					mapOfEmployee_office_hourDTOToisActive.get(employee_office_hourDTO.isActive).add(employee_office_hourDTO);
					
					if( ! mapOfEmployee_office_hourDTOToinsertedBy.containsKey(employee_office_hourDTO.insertedBy)) {
						mapOfEmployee_office_hourDTOToinsertedBy.put(employee_office_hourDTO.insertedBy, new HashSet<>());
					}
					mapOfEmployee_office_hourDTOToinsertedBy.get(employee_office_hourDTO.insertedBy).add(employee_office_hourDTO);
					
					if( ! mapOfEmployee_office_hourDTOTomodifiedBy.containsKey(employee_office_hourDTO.modifiedBy)) {
						mapOfEmployee_office_hourDTOTomodifiedBy.put(employee_office_hourDTO.modifiedBy, new HashSet<>());
					}
					mapOfEmployee_office_hourDTOTomodifiedBy.get(employee_office_hourDTO.modifiedBy).add(employee_office_hourDTO);
					
					if( ! mapOfEmployee_office_hourDTOTolastModificationTime.containsKey(employee_office_hourDTO.lastModificationTime)) {
						mapOfEmployee_office_hourDTOTolastModificationTime.put(employee_office_hourDTO.lastModificationTime, new HashSet<>());
					}
					mapOfEmployee_office_hourDTOTolastModificationTime.get(employee_office_hourDTO.lastModificationTime).add(employee_office_hourDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Employee_office_hourDTO> getEmployee_office_hourList() {
		List <Employee_office_hourDTO> employee_office_hours = new ArrayList<Employee_office_hourDTO>(this.mapOfEmployee_office_hourDTOToiD.values());
		return employee_office_hours;
	}
	
	
	public Employee_office_hourDTO getEmployee_office_hourDTOByID( long ID){
		return mapOfEmployee_office_hourDTOToiD.get(ID);
	}
	
	
	public List<Employee_office_hourDTO> getEmployee_office_hourDTOByoffice_hour_cat(int office_hour_cat) {
		return new ArrayList<>( mapOfEmployee_office_hourDTOToofficeHourCat.getOrDefault(office_hour_cat,new HashSet<>()));
	}
	
	
	public List<Employee_office_hourDTO> getEmployee_office_hourDTOBystart_time(String start_time) {
		return new ArrayList<>( mapOfEmployee_office_hourDTOTostartTime.getOrDefault(start_time,new HashSet<>()));
	}
	
	
	public List<Employee_office_hourDTO> getEmployee_office_hourDTOByend_time(String end_time) {
		return new ArrayList<>( mapOfEmployee_office_hourDTOToendTime.getOrDefault(end_time,new HashSet<>()));
	}
	
	
	public List<Employee_office_hourDTO> getEmployee_office_hourDTOBystart_date(long start_date) {
		return new ArrayList<>( mapOfEmployee_office_hourDTOTostartDate.getOrDefault(start_date,new HashSet<>()));
	}
	
	
	public List<Employee_office_hourDTO> getEmployee_office_hourDTOByend_date(long end_date) {
		return new ArrayList<>( mapOfEmployee_office_hourDTOToendDate.getOrDefault(end_date,new HashSet<>()));
	}
	
	
	public List<Employee_office_hourDTO> getEmployee_office_hourDTOByis_active(int is_active) {
		return new ArrayList<>( mapOfEmployee_office_hourDTOToisActive.getOrDefault(is_active,new HashSet<>()));
	}
	
	
	public List<Employee_office_hourDTO> getEmployee_office_hourDTOByinserted_by(long inserted_by) {
		return new ArrayList<>( mapOfEmployee_office_hourDTOToinsertedBy.getOrDefault(inserted_by,new HashSet<>()));
	}
	
	
	public List<Employee_office_hourDTO> getEmployee_office_hourDTOBymodified_by(long modified_by) {
		return new ArrayList<>( mapOfEmployee_office_hourDTOTomodifiedBy.getOrDefault(modified_by,new HashSet<>()));
	}
	
	
	public List<Employee_office_hourDTO> getEmployee_office_hourDTOBylastModificationTime(long lastModificationTime) {
		return new ArrayList<>( mapOfEmployee_office_hourDTOTolastModificationTime.getOrDefault(lastModificationTime,new HashSet<>()));
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "employee_office_hour";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


