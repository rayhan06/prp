package employee_office_hour;

import dbm.DBMR;
import dbm.DBMW;
import org.apache.log4j.Logger;
import repository.RepositoryManager;
import user.UserDTO;
import util.CommonDTO;
import util.NavigationService4;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.List;

@SuppressWarnings({"rawtypes", "unused"})
public class Employee_office_hourDAO extends NavigationService4 {

    Logger logger = Logger.getLogger(getClass());


    public Employee_office_hourDAO(String tableName) {
        super(tableName);
        joinSQL = "";

        joinSQL += " join category on (";
        joinSQL += " (" + tableName + ".office_hour_cat = category.value  and category.domain_name = 'office_hour')";
        joinSQL += " )";
        joinSQL += " join language_text on category.language_id = language_text.id";
        commonMaps = new Employee_office_hourMAPS(tableName);
    }

    public Employee_office_hourDAO() {
        this("employee_office_hour");
    }

    public void set(PreparedStatement ps, Employee_office_hourDTO employee_office_hourDTO, boolean isInsert) throws SQLException {
        int index = 1;
        long lastModificationTime = System.currentTimeMillis();
        if (isInsert) {
            ps.setObject(index++, employee_office_hourDTO.iD);
        }
        ps.setObject(index++, employee_office_hourDTO.officeHourCat);
        ps.setObject(index++, employee_office_hourDTO.startTime);
        ps.setObject(index++, employee_office_hourDTO.endTime);
        ps.setObject(index++, employee_office_hourDTO.startDate);
        ps.setObject(index++, employee_office_hourDTO.endDate);
        ps.setObject(index++, employee_office_hourDTO.isActive);
        ps.setObject(index++, employee_office_hourDTO.insertedBy);
        ps.setObject(index++, employee_office_hourDTO.modifiedBy);
        if (isInsert) {
            ps.setObject(index++, 0);
            ps.setObject(index++, lastModificationTime);
        }
    }

    public void get(Employee_office_hourDTO employee_office_hourDTO, ResultSet rs) throws SQLException {
        employee_office_hourDTO.iD = rs.getLong("ID");
        employee_office_hourDTO.officeHourCat = rs.getInt("office_hour_cat");
        employee_office_hourDTO.startTime = rs.getString("start_time");
        employee_office_hourDTO.endTime = rs.getString("end_time");
        employee_office_hourDTO.startDate = rs.getLong("start_date");
        employee_office_hourDTO.endDate = rs.getLong("end_date");
        employee_office_hourDTO.isActive = rs.getInt("is_active");
        employee_office_hourDTO.insertedBy = rs.getLong("inserted_by");
        employee_office_hourDTO.modifiedBy = rs.getLong("modified_by");
        employee_office_hourDTO.isDeleted = rs.getInt("isDeleted");
        employee_office_hourDTO.lastModificationTime = rs.getLong("lastModificationTime");
    }


    public long add(CommonDTO commonDTO) throws Exception {

        Employee_office_hourDTO employee_office_hourDTO = (Employee_office_hourDTO) commonDTO;

        Connection connection = null;
        PreparedStatement ps = null;

        long lastModificationTime = System.currentTimeMillis();

        try {
            connection = DBMW.getInstance().getConnection();

            if (connection == null) {
                System.out.println("nullconn");
            }

            employee_office_hourDTO.iD = DBMW.getInstance().getNextSequenceId(tableName);

            String sql = "INSERT INTO " + tableName;

            sql += " (";
            sql += "ID";
            sql += ", ";
            sql += "office_hour_cat";
            sql += ", ";
            sql += "start_time";
            sql += ", ";
            sql += "end_time";
            sql += ", ";
            sql += "start_date";
            sql += ", ";
            sql += "end_date";
            sql += ", ";
            sql += "is_active";
            sql += ", ";
            sql += "inserted_by";
            sql += ", ";
            sql += "modified_by";
            sql += ", ";
            sql += "isDeleted";
            sql += ", ";
            sql += "lastModificationTime";

            sql += ")";


            sql += " VALUES(";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";

            sql += ")";

            ps = connection.prepareStatement(sql);
            set(ps, employee_office_hourDTO, true);
            ps.execute();


            recordUpdateTime(connection, lastModificationTime);

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (connection != null) {
                    DBMW.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
        return employee_office_hourDTO.iD;
    }


    //need another getter for repository
    public CommonDTO getDTOByID(long ID) throws Exception {
        Connection connection = null;
        ResultSet rs = null;
        Statement stmt = null;
        Employee_office_hourDTO employee_office_hourDTO = null;
        try {

            String sql = "SELECT * ";

            sql += " FROM " + tableName;

            sql += " WHERE ID=" + ID;

            printSql(sql);

            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();


            rs = stmt.executeQuery(sql);

            if (rs.next()) {
                employee_office_hourDTO = new Employee_office_hourDTO();

                get(employee_office_hourDTO, rs);

            }


        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }

            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
        return employee_office_hourDTO;
    }

    public long update(CommonDTO commonDTO) throws Exception {
        Employee_office_hourDTO employee_office_hourDTO = (Employee_office_hourDTO) commonDTO;

        Connection connection = null;
        PreparedStatement ps = null;

        long lastModificationTime = System.currentTimeMillis();
        try {
            connection = DBMW.getInstance().getConnection();

            String sql = "UPDATE " + tableName;

            sql += " SET ";
            sql += "office_hour_cat=?";
            sql += ", ";
            sql += "start_time=?";
            sql += ", ";
            sql += "end_time=?";
            sql += ", ";
            sql += "start_date=?";
            sql += ", ";
            sql += "end_date=?";
            sql += ", ";
            sql += "is_active=?";
            sql += ", ";
            sql += "inserted_by=?";
            sql += ", ";
            sql += "modified_by=?";
            sql += ", lastModificationTime = " + lastModificationTime + "";
            sql += " WHERE ID = " + employee_office_hourDTO.iD;


            ps = connection.prepareStatement(sql);
            set(ps, employee_office_hourDTO, false);
            ps.executeUpdate();


            recordUpdateTime(connection, lastModificationTime);


        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (connection != null) {
                    DBMW.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
        return employee_office_hourDTO.iD;
    }


    public List<Employee_office_hourDTO> getDTOs(Collection recordIDs) {
        Connection connection = null;
        ResultSet rs = null;
        Statement stmt = null;
        Employee_office_hourDTO employee_office_hourDTO = null;
        List<Employee_office_hourDTO> employee_office_hourDTOList = new ArrayList<>();
        if (recordIDs.isEmpty()) {
            return employee_office_hourDTOList;
        }
        try {

            String sql = "SELECT * ";

            sql += " FROM " + tableName;

            sql += " WHERE ID IN ( ";

            for (int i = 0; i < recordIDs.size(); i++) {
                if (i != 0) {
                    sql += ",";
                }
                sql += ((ArrayList) recordIDs).get(i);
            }
            sql += ")  order by lastModificationTime desc";

            printSql(sql);

            logger.debug("sql " + sql);
            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();


            rs = stmt.executeQuery(sql);

            while (rs.next()) {
                employee_office_hourDTO = new Employee_office_hourDTO();
                get(employee_office_hourDTO, rs);
                System.out.println("got this DTO: " + employee_office_hourDTO);

                employee_office_hourDTOList.add(employee_office_hourDTO);

            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }

            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
        return employee_office_hourDTOList;

    }


    //add repository
    public List<Employee_office_hourDTO> getAllEmployee_office_hour(boolean isFirstReload) {
        List<Employee_office_hourDTO> employee_office_hourDTOList = new ArrayList<>();

        String sql = "SELECT * FROM employee_office_hour";
        sql += " WHERE ";


        if (isFirstReload) {
            sql += " isDeleted =  0";
        }
        if (!isFirstReload) {
            sql += " lastModificationTime >= " + RepositoryManager.lastModifyTime;
        }
        sql += " order by employee_office_hour.lastModificationTime desc";
        printSql(sql);

        Connection connection = null;
        Statement stmt = null;
        ResultSet rs = null;


        try {
            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();
            rs = stmt.executeQuery(sql);


            while (rs.next()) {
                Employee_office_hourDTO employee_office_hourDTO = new Employee_office_hourDTO();
                get(employee_office_hourDTO, rs);

                employee_office_hourDTOList.add(employee_office_hourDTO);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }

        return employee_office_hourDTOList;
    }


    public List<Employee_office_hourDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO) {
        return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
    }


    public List<Employee_office_hourDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
                                                 String filter, boolean tableHasJobCat) {
        Connection connection = null;
        ResultSet rs = null;
        Statement stmt = null;
        List<Employee_office_hourDTO> employee_office_hourDTOList = new ArrayList<>();

        try {

            String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat);

            printSql(sql);

            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();


            rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Employee_office_hourDTO employee_office_hourDTO = new Employee_office_hourDTO();
                get(employee_office_hourDTO, rs);

                employee_office_hourDTOList.add(employee_office_hourDTO);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }

            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
        return employee_office_hourDTOList;

    }

}
	