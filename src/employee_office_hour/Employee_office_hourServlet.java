package employee_office_hour;

import com.google.gson.Gson;
import common.ApiResponse;
import login.LoginDTO;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import permission.MenuConstants;
import role.PermissionRepository;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.CommonConstant;
import util.CommonRequestHandler;
import util.RecordNavigationManager4;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;


@WebServlet("/Employee_office_hourServlet")
@MultipartConfig
public class Employee_office_hourServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Employee_office_hourServlet.class);

    String tableName = "employee_office_hour";

    Employee_office_hourDAO employee_office_hourDAO;
    CommonRequestHandler commonRequestHandler;
    private final Gson gson = new Gson();

    public Employee_office_hourServlet() {
        super();

        employee_office_hourDAO = new Employee_office_hourDAO(tableName);
        commonRequestHandler = new CommonRequestHandler(employee_office_hourDAO);

    }


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("In doget request = " + request);
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }
        try {
            String actionType = request.getParameter("actionType");
            switch (actionType) {
                case "getAddPage":
                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.EMPLOYEE_OFFICE_HOUR_ADD)) {
                        commonRequestHandler.getAddPage(request, response);
                        return;
                    }
                    break;
                case "getEditPage":
                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.EMPLOYEE_OFFICE_HOUR_UPDATE)) {
                        String URL = getEmployee_office_hour(request, response);
                        request.getRequestDispatcher(URL).forward(request, response);
                        return;
                    }
                    break;
                case "getURL":
                    String URL = request.getParameter("URL");
                    System.out.println("URL = " + URL);
                    response.sendRedirect(URL);
                    return;
                case "search":
                    System.out.println("search requested");
                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.EMPLOYEE_OFFICE_HOUR_SEARCH)) {

                        searchEmployee_office_hour(request, response);
                        return;
                    }
                    break;
                case "view":
                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.EMPLOYEE_OFFICE_HOUR_SEARCH)) {
                        commonRequestHandler.view(request, response);
                        return;
                    }
                    break;

            }
        } catch (Exception ex) {
            logger.debug(ex);
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);

    }


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }
        try {
            String actionType = request.getParameter("actionType");
            switch (actionType) {
                case "ajax_add":

                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.EMPLOYEE_OFFICE_HOUR_ADD)) {
                        try {
                            String URL = addEmployee_office_hour(request, response, true, userDTO);
                            ApiResponse.sendSuccessResponse(response, URL);
                        } catch (Exception ex) {
                            logger.error(ex);
                            ApiResponse.sendErrorResponse(response, ex.getMessage());
                        }
                        return;
                    }

                    break;
                case "getDTO":

                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.EMPLOYEE_OFFICE_HOUR_ADD)) {
                        getDTO(request, response);
                        return;
                    }

                    break;
                case "ajax_edit":

                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.EMPLOYEE_OFFICE_HOUR_UPDATE)) {
                        try {
                            String URL = addEmployee_office_hour(request, response, false, userDTO);
                            ApiResponse.sendSuccessResponse(response, URL);
                        } catch (Exception ex) {
                            logger.error(ex);
                            ApiResponse.sendErrorResponse(response, ex.getMessage());
                        }
                        return;
                    }
                    break;
                case "delete":
                    deleteEmployee_office_hour(request, response, userDTO);
                    return;
                case "search":
                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.EMPLOYEE_OFFICE_HOUR_SEARCH)) {
                        searchEmployee_office_hour(request, response);
                        return;
                    }

                    break;
                case "getGeo":
                    request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
                    return;
            }

        } catch (Exception ex) {
            logger.debug(ex);
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);

    }

    private void getDTO(HttpServletRequest request, HttpServletResponse response) throws Exception {

        Employee_office_hourDTO employee_office_hourDTO = (Employee_office_hourDTO) employee_office_hourDAO.getDTOByID(Long.parseLong(request.getParameter("ID")));
        PrintWriter out = response.getWriter();
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        String encoded = this.gson.toJson(employee_office_hourDTO);
        out.print(encoded);
        out.flush();

    }

    private String addEmployee_office_hour(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO) throws Exception {
        boolean isLangEng = userDTO.languageID == CommonConstant.Language_ID_English;
        request.setAttribute("failureMessage", "");
        Employee_office_hourDTO employee_office_hourDTO;
        SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");

        if (addFlag) {
            employee_office_hourDTO = new Employee_office_hourDTO();
            employee_office_hourDTO.insertedBy = userDTO.employee_record_id;
        } else {
            employee_office_hourDTO = (Employee_office_hourDTO) employee_office_hourDAO.getDTOByID(Long.parseLong(request.getParameter("iD")));
        }
        employee_office_hourDTO.modifiedBy = userDTO.employee_record_id;
        employee_office_hourDTO.lastModificationTime = System.currentTimeMillis();
        String Value;


        employee_office_hourDTO.officeHourCat = Integer.parseInt(request.getParameter("officeHourCat"));

        employee_office_hourDTO.startTime = (request.getParameter("startTime"));

        employee_office_hourDTO.endTime = (request.getParameter("endTime"));


        Date d = f.parse(request.getParameter("startDate"));
        employee_office_hourDTO.startDate = d.getTime();

        d = f.parse(request.getParameter("endDate"));
        employee_office_hourDTO.endDate = d.getTime();


        if (employee_office_hourDTO.startDate > employee_office_hourDTO.endDate)
            throw new Exception(
                    isLangEng ? "From date can not be greater than to date"
                            : "তারিখ হতে, তারিখ পর্যন্ত থেকে বড় হতে পারে না"
            );

        Value = request.getParameter("isActive");

        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }
        if (Value != null && !Value.equalsIgnoreCase("")) {

            employee_office_hourDTO.isActive = 1;
        } else {
            employee_office_hourDTO.isActive = 0;
        }


        System.out.println("Done adding  addEmployee_office_hour dto = " + employee_office_hourDTO);
        long returnedID;
        if (addFlag) {
            returnedID = employee_office_hourDAO.manageWriteOperations(employee_office_hourDTO, SessionConstants.INSERT, -1, userDTO);
        } else {
            returnedID = employee_office_hourDAO.manageWriteOperations(employee_office_hourDTO, SessionConstants.UPDATE, -1, userDTO);
        }


        String inPlaceSubmit = request.getParameter("inplacesubmit");

        if (inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase("")) {
            return getEmployee_office_hour(request, response, returnedID);
        } else {
            return "Employee_office_hourServlet?actionType=search";
        }


    }


    private void deleteEmployee_office_hour(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO) throws Exception {

        String[] IDsToDelete = request.getParameterValues("ID");
        for (String s : IDsToDelete) {
            long id = Long.parseLong(s);
            System.out.println("------ DELETING " + s);
            Employee_office_hourDTO employee_office_hourDTO = (Employee_office_hourDTO) employee_office_hourDAO.getDTOByID(id);
            employee_office_hourDAO.manageWriteOperations(employee_office_hourDTO, SessionConstants.DELETE, id, userDTO);
            response.sendRedirect("Employee_office_hourServlet?actionType=search");

        }

    }

    private String getEmployee_office_hour(HttpServletRequest request, HttpServletResponse response, long id) throws Exception {
        Employee_office_hourDTO employee_office_hourDTO;
        employee_office_hourDTO = (Employee_office_hourDTO) employee_office_hourDAO.getDTOByID(id);
        request.setAttribute("ID", employee_office_hourDTO.iD);
        request.setAttribute("employee_office_hourDTO", employee_office_hourDTO);
        request.setAttribute("employee_office_hourDAO", employee_office_hourDAO);

        String URL;

        String inPlaceEdit = request.getParameter("inplaceedit");
        String inPlaceSubmit = request.getParameter("inplacesubmit");
        String getBodyOnly = request.getParameter("getBodyOnly");

        if (inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase("")) {
            URL = "employee_office_hour/employee_office_hourInPlaceEdit.jsp";
            request.setAttribute("inplaceedit", "");
        } else if (inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase("")) {
            URL = "employee_office_hour/employee_office_hourSearchRow.jsp";
            request.setAttribute("inplacesubmit", "");
        } else {
            if (getBodyOnly != null && !getBodyOnly.equalsIgnoreCase("")) {
                URL = "employee_office_hour/employee_office_hourEditBody.jsp?actionType=edit";
            } else {
                URL = "employee_office_hour/employee_office_hourEdit.jsp?actionType=edit";
            }
        }
        return URL;


    }


    private String getEmployee_office_hour(HttpServletRequest request, HttpServletResponse response) throws Exception {
        return getEmployee_office_hour(request, response, Long.parseLong(request.getParameter("ID")));
    }

    private void searchEmployee_office_hour(HttpServletRequest request, HttpServletResponse response) throws Exception {
        LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        String ajax = request.getParameter("ajax");
        boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);

        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
                SessionConstants.NAV_EMPLOYEE_OFFICE_HOUR,
                request,
                employee_office_hourDAO,
                SessionConstants.VIEW_EMPLOYEE_OFFICE_HOUR,
                SessionConstants.SEARCH_EMPLOYEE_OFFICE_HOUR,
                tableName,
                true,
                userDTO,
                "",
                true);

        rnManager.doJob(loginDTO);

        request.setAttribute("employee_office_hourDAO", employee_office_hourDAO);

        if (!hasAjax) {
            System.out.println("Going to employee_office_hour/employee_office_hourSearch.jsp");
            request.getRequestDispatcher("employee_office_hour/employee_office_hourSearch.jsp").forward(request, response);
        } else {
            System.out.println("Going to employee_office_hour/employee_office_hourSearchForm.jsp");
            request.getRequestDispatcher("employee_office_hour/employee_office_hourSearchForm.jsp").forward(request, response);
        }

    }

}

