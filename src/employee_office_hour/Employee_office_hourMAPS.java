package employee_office_hour;
import java.util.*; 
import util.*;


public class Employee_office_hourMAPS extends CommonMaps
{	
	public Employee_office_hourMAPS(String tableName)
	{
		
		java_allfield_type_map.put("office_hour_cat".toLowerCase(), "Integer");
		java_allfield_type_map.put("start_time".toLowerCase(), "String");
		java_allfield_type_map.put("end_time".toLowerCase(), "String");
		java_allfield_type_map.put("start_date".toLowerCase(), "Long");
		java_allfield_type_map.put("end_date".toLowerCase(), "Long");
		java_allfield_type_map.put("is_active".toLowerCase(), "Integer");
		java_allfield_type_map.put("inserted_by".toLowerCase(), "Long");
		java_allfield_type_map.put("modified_by".toLowerCase(), "Long");

		java_anyfield_search_map.put("language_text.languageTextEnglish", "String");
		java_anyfield_search_map.put("language_text.languageTextBangla", "String");

		java_anyfield_search_map.put(tableName + ".start_time".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".end_time".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".start_date".toLowerCase(), "Long");
		java_anyfield_search_map.put(tableName + ".end_date".toLowerCase(), "Long");
		java_anyfield_search_map.put(tableName + ".is_active".toLowerCase(), "Integer");
		java_anyfield_search_map.put(tableName + ".inserted_by".toLowerCase(), "Long");
		java_anyfield_search_map.put(tableName + ".modified_by".toLowerCase(), "Long");

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("officeHourCat".toLowerCase(), "officeHourCat".toLowerCase());
		java_DTO_map.put("startTime".toLowerCase(), "startTime".toLowerCase());
		java_DTO_map.put("endTime".toLowerCase(), "endTime".toLowerCase());
		java_DTO_map.put("startDate".toLowerCase(), "startDate".toLowerCase());
		java_DTO_map.put("endDate".toLowerCase(), "endDate".toLowerCase());
		java_DTO_map.put("isActive".toLowerCase(), "isActive".toLowerCase());
		java_DTO_map.put("insertedBy".toLowerCase(), "insertedBy".toLowerCase());
		java_DTO_map.put("modifiedBy".toLowerCase(), "modifiedBy".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("office_hour_cat".toLowerCase(), "officeHourCat".toLowerCase());
		java_SQL_map.put("start_time".toLowerCase(), "startTime".toLowerCase());
		java_SQL_map.put("end_time".toLowerCase(), "endTime".toLowerCase());
		java_SQL_map.put("start_date".toLowerCase(), "startDate".toLowerCase());
		java_SQL_map.put("end_date".toLowerCase(), "endDate".toLowerCase());
		java_SQL_map.put("is_active".toLowerCase(), "isActive".toLowerCase());
		java_SQL_map.put("inserted_by".toLowerCase(), "insertedBy".toLowerCase());
		java_SQL_map.put("modified_by".toLowerCase(), "modifiedBy".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Office Hour Category".toLowerCase(), "officeHourCat".toLowerCase());
		java_Text_map.put("Start Time".toLowerCase(), "startTime".toLowerCase());
		java_Text_map.put("End Time".toLowerCase(), "endTime".toLowerCase());
		java_Text_map.put("Start Date".toLowerCase(), "startDate".toLowerCase());
		java_Text_map.put("End Date".toLowerCase(), "endDate".toLowerCase());
		java_Text_map.put("Is Active".toLowerCase(), "isActive".toLowerCase());
		java_Text_map.put("Inserted By".toLowerCase(), "insertedBy".toLowerCase());
		java_Text_map.put("Modified By".toLowerCase(), "modifiedBy".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}