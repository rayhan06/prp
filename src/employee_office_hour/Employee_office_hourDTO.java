package employee_office_hour;
import java.util.*; 
import util.*; 


public class Employee_office_hourDTO extends CommonDTO
{

	public int officeHourCat = 0;
    public String startTime = "";
    public String endTime = "";
	public long startDate = 0;
	public long endDate = 0;
	public int isActive = 0;
	public long insertedBy = 0;
	public long modifiedBy = 0;
	
	
    @Override
	public String toString() {
            return "$Employee_office_hourDTO[" +
            " iD = " + iD +
            " officeHourCat = " + officeHourCat +
            " startTime = " + startTime +
            " endTime = " + endTime +
            " startDate = " + startDate +
            " endDate = " + endDate +
            " isActive = " + isActive +
            " insertedBy = " + insertedBy +
            " modifiedBy = " + modifiedBy +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}