package medical_allowance;
import java.util.*;

import sessionmanager.SessionConstants;
import util.*;


public class Medical_allowanceDTO extends CommonDTO
{

	public long voucherDate = SessionConstants.MIN_DATE;
	public String voucherNumber="";
	public int totalAllowance = 0;
	public long employeeRecordsId = 0;
    public String nameEn = "";
    public String nameBn = "";
	public long officeUnitId = 0;
    public String officeUnitNameEn = "";
    public String officeUnitNameBn = "";
	public long organogramId = 0;
    public String organogramNameEn = "";
    public String organogramNameBn = "";
	public long insertedBy = -1;
	public long insertionTime = -1;
	public long modifiedBy = -1;
	
	public List<MedicalAllowanceDetailsDTO> medicalAllowanceDetailsDTOList = new ArrayList<>();
	
    @Override
	public String toString() {
            return "$Medical_allowanceDTO[" +
            " iD = " + iD +
            " voucherDate = " + voucherDate +
            " totalAllowance = " + totalAllowance +
            " employeeRecordsId = " + employeeRecordsId +
            " nameEn = " + nameEn +
            " nameBn = " + nameBn +
            " officeUnitId = " + officeUnitId +
            " officeUnitNameEn = " + officeUnitNameEn +
            " officeUnitNameBn = " + officeUnitNameBn +
            " organogramId = " + organogramId +
            " organogramNameEn = " + organogramNameEn +
            " organogramNameBn = " + organogramNameBn +
            " insertedBy = " + insertedBy +
            " insertionTime = " + insertionTime +
            " isDeleted = " + isDeleted +
            " modifiedBy = " + modifiedBy +
            " lastModificationTime = " + lastModificationTime +
            " searchColumn = " + searchColumn +
            "]";
    }

}