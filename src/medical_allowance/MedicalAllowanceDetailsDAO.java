package medical_allowance;

import common.EmployeeCommonDAOService;
import org.apache.log4j.Logger;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

public class MedicalAllowanceDetailsDAO implements EmployeeCommonDAOService<MedicalAllowanceDetailsDTO> {

    private static final Logger logger = Logger.getLogger(Medical_allowanceDAO.class);

    private static final String addQuery =
            "INSERT INTO {tableName} (medical_allowance_id,date,description,allowance_amount,files_dropzone,"
                    .concat("modified_by,lastModificationTime,isDeleted,")
                    .concat("inserted_by,insertion_time,ID) VALUES (?,?,?,?,?,?,?,?,?,?,?)");

    private static final String updateQuery =
            "UPDATE {tableName} SET medical_allowance_id=?,date=?,description=?,allowance_amount=?,files_dropzone=?,"
                    .concat("modified_by=?,lastModificationTime=?,isDeleted=? WHERE ID=?");

    private static final String getByMedicalAllowanceId="SELECT * FROM medical_allowance_details WHERE medical_allowance_id=%d AND isDeleted=0";

    private MedicalAllowanceDetailsDAO() {
    }

    private static class LazyLoader {
        static final MedicalAllowanceDetailsDAO INSTANCE = new MedicalAllowanceDetailsDAO();
    }

    public static MedicalAllowanceDetailsDAO getInstance() {
        return MedicalAllowanceDetailsDAO.LazyLoader.INSTANCE;
    }

    @Override
    public void set(PreparedStatement ps, MedicalAllowanceDetailsDTO dto, boolean isInsert) throws SQLException {
        int index = 0;
        ps.setObject(++index, dto.medicalAllowanceId);
        ps.setObject(++index, dto.date);
        ps.setObject(++index, dto.description);
        ps.setObject(++index, dto.allowanceAmount);
        ps.setObject(++index, dto.filesDropzone);
        ps.setObject(++index, dto.modifiedBy);
        ps.setObject(++index, dto.lastModificationTime);
        ps.setObject(++index, dto.isDeleted);
        if (isInsert) {
            ps.setObject(++index, dto.insertedBy);
            ps.setObject(++index, dto.insertionTime);
        }
        ps.setObject(++index, dto.iD);
    }

    @Override
    public MedicalAllowanceDetailsDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            MedicalAllowanceDetailsDTO medicalallowancedetailsDTO = new MedicalAllowanceDetailsDTO();
            medicalallowancedetailsDTO.iD = rs.getLong("ID");
            medicalallowancedetailsDTO.medicalAllowanceId = rs.getLong("medical_allowance_id");
            medicalallowancedetailsDTO.date = rs.getLong("date");
            medicalallowancedetailsDTO.description = rs.getString("description");
            medicalallowancedetailsDTO.allowanceAmount = rs.getInt("allowance_amount");
            medicalallowancedetailsDTO.filesDropzone = rs.getInt("files_dropzone");
            medicalallowancedetailsDTO.insertedBy = rs.getLong("inserted_by");
            medicalallowancedetailsDTO.insertionTime = rs.getLong("insertion_time");
            medicalallowancedetailsDTO.isDeleted = rs.getInt("isDeleted");
            medicalallowancedetailsDTO.modifiedBy = rs.getLong("modified_by");
            medicalallowancedetailsDTO.lastModificationTime = rs.getLong("lastModificationTime");
            return medicalallowancedetailsDTO;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    @Override
    public String getTableName() {
        return "medical_allowance_details";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return null;
    }

    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((MedicalAllowanceDetailsDTO) commonDTO, addQuery, true);
    }

    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((MedicalAllowanceDetailsDTO) commonDTO, updateQuery, false);
    }
    public List<MedicalAllowanceDetailsDTO> getByMedicalAllowanceId(long id){
        return getDTOs(String.format(getByMedicalAllowanceId,id));
    }
}
	