package medical_allowance;

import audit.log.AuditLogDAO;
import audit.log.LogEvent;
import common.ConnectionAndStatementUtil;
import common.EmployeeCommonDAOService;
import org.apache.log4j.Logger;
import util.CommonDTO;
import util.HttpRequestUtils;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Medical_allowanceDAO implements EmployeeCommonDAOService<Medical_allowanceDTO> {

    private static final Logger logger = Logger.getLogger(Medical_allowanceDAO.class);
    public static final String VOUCHER_PREFIX = "MB";
    private static final String addQuery =
            "INSERT INTO {tableName} (voucher_number,voucher_date,total_allowance,employee_records_id,"
                    .concat("name_en,name_bn,office_unit_id,office_unit_name_en,office_unit_name_bn,")
                    .concat("organogram_id,organogram_name_en,organogram_name_bn,search_column,modified_by,lastModificationTime,")
                    .concat("inserted_by,insertion_time,isDeleted,ID) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
    private static final String updateQuery =
            "UPDATE {tableName} SET voucher_number=?,voucher_date=?,total_allowance=?,employee_records_id=?,"
                    .concat("name_en=?,name_bn=?,office_unit_id=?,office_unit_name_en=?,office_unit_name_bn=?,")
                    .concat("organogram_id=?,organogram_name_en=?,organogram_name_bn=?,")
                    .concat("search_column=?,modified_by=?,lastModificationTime=? WHERE ID=?");
    private static final String getListForBill = "SELECT * FROM medical_allowance WHERE  employee_records_id=%d "
            .concat("AND voucher_date>=%d AND voucher_date<=%d AND isDeleted=0 ORDER BY voucher_date ASC");
    private static final Map<String, String> searchMap = new HashMap<>();

    private Medical_allowanceDAO() {
        searchMap.put("AnyField", " and (search_column like ?)");
        searchMap.put("employee_records_id", " and (employee_records_id = ?)");
        searchMap.put("employee_records_id_internal", " and (employee_records_id = ?)");
        searchMap.put("voucher_date", " and (voucher_date = ?)");
        searchMap.put("voucher_number", " and (voucher_number = ?)");
    }

    private static class LazyLoader {
        static final Medical_allowanceDAO INSTANCE = new Medical_allowanceDAO();
    }

    public static Medical_allowanceDAO getInstance() {
        return Medical_allowanceDAO.LazyLoader.INSTANCE;
    }


    public void setSearchColumn(Medical_allowanceDTO medical_allowanceDTO) {
        medical_allowanceDTO.searchColumn = "";
        medical_allowanceDTO.searchColumn += medical_allowanceDTO.nameEn + " ";
        medical_allowanceDTO.searchColumn += medical_allowanceDTO.nameBn + " ";
        medical_allowanceDTO.searchColumn += medical_allowanceDTO.officeUnitNameEn + " ";
        medical_allowanceDTO.searchColumn += medical_allowanceDTO.officeUnitNameBn + " ";
        medical_allowanceDTO.searchColumn += medical_allowanceDTO.organogramNameEn + " ";
        medical_allowanceDTO.searchColumn += medical_allowanceDTO.organogramNameBn + " ";
    }

    @Override
    public void set(PreparedStatement ps, Medical_allowanceDTO dto, boolean isInsert) throws SQLException {
        setSearchColumn(dto);
        int index = 0;
        ps.setObject(++index, dto.voucherNumber);
        ps.setObject(++index, dto.voucherDate);
        ps.setObject(++index, dto.totalAllowance);
        ps.setObject(++index, dto.employeeRecordsId);
        ps.setObject(++index, dto.nameEn);
        ps.setObject(++index, dto.nameBn);
        ps.setObject(++index, dto.officeUnitId);
        ps.setObject(++index, dto.officeUnitNameEn);
        ps.setObject(++index, dto.officeUnitNameBn);
        ps.setObject(++index, dto.organogramId);
        ps.setObject(++index, dto.organogramNameEn);
        ps.setObject(++index, dto.organogramNameBn);
        ps.setObject(++index, dto.searchColumn);
        ps.setObject(++index, dto.modifiedBy);
        ps.setObject(++index, dto.lastModificationTime);
        if (isInsert) {
            ps.setObject(++index, dto.insertedBy);
            ps.setObject(++index, dto.insertionTime);
            ps.setObject(++index, 0);
        }
        ps.setObject(++index, dto.iD);
    }

    @Override
    public Medical_allowanceDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Medical_allowanceDTO medical_allowanceDTO = new Medical_allowanceDTO();
            medical_allowanceDTO.iD = rs.getLong("ID");
            medical_allowanceDTO.voucherNumber = rs.getString("voucher_number");
            medical_allowanceDTO.voucherDate = rs.getLong("voucher_date");
            medical_allowanceDTO.totalAllowance = rs.getInt("total_allowance");
            medical_allowanceDTO.employeeRecordsId = rs.getLong("employee_records_id");
            medical_allowanceDTO.nameEn = rs.getString("name_en");
            medical_allowanceDTO.nameBn = rs.getString("name_bn");
            medical_allowanceDTO.officeUnitId = rs.getLong("office_unit_id");
            medical_allowanceDTO.officeUnitNameEn = rs.getString("office_unit_name_en");
            medical_allowanceDTO.officeUnitNameBn = rs.getString("office_unit_name_bn");
            medical_allowanceDTO.organogramId = rs.getLong("organogram_id");
            medical_allowanceDTO.organogramNameEn = rs.getString("organogram_name_en");
            medical_allowanceDTO.organogramNameBn = rs.getString("organogram_name_bn");
            medical_allowanceDTO.insertedBy = rs.getLong("inserted_by");
            medical_allowanceDTO.insertionTime = rs.getLong("insertion_time");
            medical_allowanceDTO.isDeleted = rs.getInt("isDeleted");
            medical_allowanceDTO.modifiedBy = rs.getLong("modified_by");
            medical_allowanceDTO.lastModificationTime = rs.getLong("lastModificationTime");
            medical_allowanceDTO.searchColumn = rs.getString("search_column");
            return medical_allowanceDTO;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    @Override
    public String getTableName() {
        return "medical_allowance";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    public long add(CommonDTO commonDTO) throws Exception {
        //return executeAddOrUpdateQuery((Medical_allowanceDTO) commonDTO, addQuery, true);
        String sql = addQuery.replace("{tableName}", getTableName());
        return (long) ConnectionAndStatementUtil.getWriteConnectionAndPrepareStatement(obj -> {
            PreparedStatement ps = (PreparedStatement) obj.getStatement();
            long returnId = -1L;
            try {
                set(ps, (Medical_allowanceDTO) commonDTO, true);
                logger.debug(ps);
                ps.execute();
                returnId = commonDTO.iD;
                AuditLogDAO.add(null, (Medical_allowanceDTO) commonDTO, LogEvent.ADD, getTableName(), HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().ipAddress,
                        HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO.ID, HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO.organogramID);
                recordUpdateTime(obj.getConnection(), getTableName());
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return returnId;
        }, sql);
    }

    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Medical_allowanceDTO) commonDTO, updateQuery, false);
    }

    public List<Medical_allowanceDTO> getForBill(long employeeRecordsId, long startDate, long endDate) {
        return getDTOs(String.format(getListForBill, employeeRecordsId, startDate, endDate));
    }
}
	