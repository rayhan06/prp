package medical_allowance;

import common.*;
import dbm.DBMW;
import employee_nominee.Employee_nomineeServlet;
import language.LC;
import language.LM;
import login.LoginDTO;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import pb.ErrorMessage;
import pb.Utils;
import permission.MenuConstants;
import role.PermissionRepository;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.CommonConstant;
import util.CommonDTO;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;


/**
 * Servlet implementation class Medical_allowanceServlet
 */
@SuppressWarnings({"Duplicates"})
@WebServlet("/Medical_allowanceServlet")
@MultipartConfig
public class Medical_allowanceServlet extends BaseServlet {
    private static final long serialVersionUID = 1L;
    public static int MEDICAL_ALLOWANCE_SUB_CODE=3111311;
    public static Logger logger = Logger.getLogger(Employee_nomineeServlet.class);
    private final Medical_allowanceDAO medical_allowanceDAO = Medical_allowanceDAO.getInstance();
    private final MedicalAllowanceDetailsDAO medicalAllowanceDetailsDAO = MedicalAllowanceDetailsDAO.getInstance();

    @Override
    public String getTableName() {
        return Medical_allowanceDAO.getInstance().getTableName();
    }

    @Override
    public String getServletName() {
        return "Medical_allowanceServlet";
    }

    @Override
    public Medical_allowanceDAO getCommonDAOService() {
        return medical_allowanceDAO;
    }

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        String language = userDTO.languageID == SessionConstants.ENGLISH ? "English" : "Bangla";
        boolean isLangEng = LM.getLanguageIDByUserDTO(userDTO) == CommonConstant.Language_ID_English;
        long curTime = System.currentTimeMillis();
        long modifiedBy = userDTO.ID;
        Medical_allowanceDTO medical_allowanceDTO;
        if (addFlag) {
            medical_allowanceDTO = new Medical_allowanceDTO();
            medical_allowanceDTO.iD=Long.parseLong(Jsoup.clean(request.getParameter("iD"), Whitelist.simpleText()));
        } else {
            medical_allowanceDTO = medical_allowanceDAO.getDTOFromID(Long.parseLong(request.getParameter("iD")));
        }
        String Value;
        medical_allowanceDTO.voucherNumber = Jsoup.clean(request.getParameter("voucher_number"), Whitelist.simpleText());
        Value = Jsoup.clean(request.getParameter("voucherDate"), Whitelist.simpleText());
        if (Value != null && !Value.equalsIgnoreCase("")) {
            medical_allowanceDTO.voucherDate = Long.parseLong(Value);
        } else {
            throw new Exception(LM.getText(LC.MEDICAL_ALLOWANCE_ADD_VOUCHERDATE, language) + " " + ErrorMessage.getInvalidMessage(language));
        }
        medical_allowanceDTO.totalAllowance = Integer.parseInt(Jsoup.clean(request.getParameter("totalAllowance"), Whitelist.simpleText()));

//        Value = request.getParameter("filesDropzone");
//
//        if (Value != null && !Value.equalsIgnoreCase("")) {
//            Value = Jsoup.clean(Value, Whitelist.simpleText());
//        }
//        if (Value != null && !Value.equalsIgnoreCase("")) {
//            medical_allowanceDTO.filesDropzone = Long.parseLong(Value);
//            if (!addFlag) {
//                String filesDropzoneFilesToDelete = request.getParameter("filesDropzoneFilesToDelete");
//                String[] deleteArray = filesDropzoneFilesToDelete.split(",");
//                for (int i = 0; i < deleteArray.length; i++) {
//                    if (i > 0) {
//                        filesDAO.delete(Long.parseLong(deleteArray[i]));
//                    }
//                }
//            }
//        } else {
//            logger.debug("FieldName has a null Value, not updating" + " = " + Value);
//        }
        medical_allowanceDTO.employeeRecordsId = Long.parseLong(Jsoup.clean(request.getParameter("employeeRecordsId"), Whitelist.simpleText()));
        medical_allowanceDTO.nameEn = Jsoup.clean(request.getParameter("nameEn"), Whitelist.simpleText());
        medical_allowanceDTO.nameBn = Jsoup.clean(request.getParameter("nameBn"), Whitelist.simpleText());
        medical_allowanceDTO.officeUnitId = Long.parseLong(Jsoup.clean(request.getParameter("officeUnitId"), Whitelist.simpleText()));
        medical_allowanceDTO.officeUnitNameEn = Jsoup.clean(request.getParameter("officeUnitNameEn"), Whitelist.simpleText());
        medical_allowanceDTO.officeUnitNameBn = Jsoup.clean(request.getParameter("officeUnitNameBn"), Whitelist.simpleText());
        medical_allowanceDTO.organogramId = Long.parseLong(Jsoup.clean(request.getParameter("organogramId"), Whitelist.simpleText()));
        medical_allowanceDTO.organogramNameEn = Jsoup.clean(request.getParameter("organogramNameEn"), Whitelist.simpleText());
        medical_allowanceDTO.organogramNameBn = Jsoup.clean(request.getParameter("organogramNameBn"), Whitelist.simpleText());
        medical_allowanceDTO.modifiedBy = modifiedBy;
        medical_allowanceDTO.lastModificationTime = curTime;
        List<MedicalAllowanceDetailsDTO> medicalAllowanceDetailsDTOList = createMedicalAllowanceDetailsDTOListByRequest(request, isLangEng, userDTO);
        if (addFlag) {
            medical_allowanceDTO.insertedBy = modifiedBy;
            medical_allowanceDTO.insertionTime = curTime;
            medical_allowanceDAO.add(medical_allowanceDTO);
        } else {
            List<MedicalAllowanceDetailsDTO> oldDTOList = medicalAllowanceDetailsDAO.getByMedicalAllowanceId(medical_allowanceDTO.iD);
            if (request.getParameterValues("medicalAllowanceDetails.iD") != null) {
                Set<Long> ids = Stream.of(request.getParameterValues("medicalAllowanceDetails.iD"))
                        .map(Long::parseLong)
                        .filter(e -> e >= 0)
                        .collect(Collectors.toSet());
                List<MedicalAllowanceDetailsDTO> deletedList = oldDTOList.stream()
                        .filter(e -> !ids.contains(e.iD))
                        .peek(dto -> dto.isDeleted = 1)
                        .collect(Collectors.toList());
                for (MedicalAllowanceDetailsDTO dto : deletedList) {
                    dto.modifiedBy = modifiedBy;
                    dto.lastModificationTime = curTime;
                }
                if (deletedList.size() > 0) {
                    if (medicalAllowanceDetailsDTOList == null) {
                        medicalAllowanceDetailsDTOList = deletedList;
                    } else {
                        medicalAllowanceDetailsDTOList.addAll(deletedList);
                    }
                }
            }
            medical_allowanceDAO.update(medical_allowanceDTO);
        }
        if (medicalAllowanceDetailsDTOList != null) {
            for (MedicalAllowanceDetailsDTO dto : medicalAllowanceDetailsDTOList) {
                dto.medicalAllowanceId = medical_allowanceDTO.iD;
                if (dto.iD == -1)
                    medicalAllowanceDetailsDAO.add(dto);
                else
                    medicalAllowanceDetailsDAO.update(dto);
            }
        }
        return medical_allowanceDTO;
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }
        try {
            String actionType = request.getParameter("actionType");
            switch (actionType) {
                case "getAddPage":
                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.MEDICAL_ALLOWANCE_ADD)) {
                        long nextId= DBMW.getInstance().getNextSequenceId(getTableName());
                        request.setAttribute("ID",String.valueOf(nextId));
                        request.getRequestDispatcher(commonPartOfDispatchURL()+"Edit.jsp").forward(request,response);
                        return;
                    } else {
                        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                    }
                    break;
                case "getFileNextSequenceId":

                    System.out.println("In getFileNextSequenceId");
                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.MEDICAL_ALLOWANCE_ADD)) {
                        try {
                            String responseText = String.valueOf(DBMW.getInstance().getNextSequenceId("fileid"));
                            ApiResponse.sendSuccessResponse(response, responseText);
                        } catch (Exception ex) {
                            logger.error(ex);
                            ApiResponse.sendErrorResponse(response, ex.getMessage());
                        }
                        return;
                    } else {
                        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                    }
                    break;
                case "search":
                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.MEDICAL_ALLOWANCE_SEARCH)) {
                        if (userDTO.roleID == RoleEnum.EMPLOYEE_OF_PARLIAMENT.getRoleId()) {
                            Map<String, String> extraCriteriaMap = new HashMap<>();
                            extraCriteriaMap.put("employee_records_id_internal", String.valueOf(userDTO.employee_record_id));
                            request.setAttribute(CommonDAOService.CRITERIA_MAP_KEY, extraCriteriaMap);
                        }
                        search(request, response);
                    } else {
                        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                    }
                    break;
                case "billGenerate":
                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.MEDICAL_ALLOWANCE_BILL_GENERATE)) {
                        request.getRequestDispatcher(commonPartOfDispatchURL() + "BillForm.jsp").forward(request, response);
                        return;
                    } else {
                        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                    }
                    break;
                case "viewBill":
                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.MEDICAL_ALLOWANCE_BILL_GENERATE)) {
                        request.getRequestDispatcher(commonPartOfDispatchURL() + "Bill.jsp").forward(request, response);
                        return;
                    } else {
                        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                    }
                    break;
                default:
                    super.doGet(request, response);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.debug(ex);
        }
    }
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }
        try {
            String actionType = request.getParameter("actionType");
            switch (actionType) {
                case "getBill":
                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.MEDICAL_ALLOWANCE_BILL_GENERATE)) {
                        long employeeRecordsId=Long.parseLong(Jsoup.clean(request.getParameter("employeeRecordsId"),Whitelist.simpleText()));
                        long startDate=Long.parseLong(Jsoup.clean(request.getParameter("startDate"),Whitelist.simpleText()));
                        long endDate=Long.parseLong(Jsoup.clean(request.getParameter("endDate"),Whitelist.simpleText()));
                        Calendar endCalendar=Calendar.getInstance();
                        endCalendar.setTime(new Date(endDate));
                        endCalendar.add(Calendar.MONTH,1);
                        endCalendar.add(Calendar.DAY_OF_MONTH,-1);
                        endDate=endCalendar.getTimeInMillis();
                        logger.debug("employeeRecordsId :"+employeeRecordsId);
                        logger.debug("startDate :"+startDate);
                        logger.debug("endDate :"+endDate);
                        List<Medical_allowanceDTO>medical_allowanceDTOList=medical_allowanceDAO.getForBill(employeeRecordsId,startDate,endDate);
                        request.setAttribute("medicalAllowanceDTOList",medical_allowanceDTOList);
                        request.getRequestDispatcher(commonPartOfDispatchURL() + "Bill.jsp").forward(request, response);
                        return;
                    } else {
                        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                    }
                    break;
                default:
                    super.doPost(request, response);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.debug(ex);
        }
    }

    private List<MedicalAllowanceDetailsDTO> createMedicalAllowanceDetailsDTOListByRequest(HttpServletRequest request, Boolean isLangEng, UserDTO userDTO) throws Exception {
        long curTime = System.currentTimeMillis();
        long modifiedBy = userDTO.ID;
        List<MedicalAllowanceDetailsDTO> medicalAllowanceDetailsDTOList = new ArrayList<>();
        if (request.getParameterValues("medicalAllowanceDetails.iD") != null) {
            int medicalAllowanceDetailsItemNo = request.getParameterValues("medicalAllowanceDetails.iD").length;


            for (int index = 0; index < medicalAllowanceDetailsItemNo; index++) {
                MedicalAllowanceDetailsDTO medicalAllowanceDetailsDTO = createMedicalAllowanceDetailsDTOByRequestAndIndex(request, index, isLangEng);
                medicalAllowanceDetailsDTO.insertedBy = modifiedBy;
                medicalAllowanceDetailsDTO.insertionTime = curTime;
                medicalAllowanceDetailsDTO.modifiedBy = modifiedBy;
                medicalAllowanceDetailsDTO.lastModificationTime = curTime;
                medicalAllowanceDetailsDTOList.add(medicalAllowanceDetailsDTO);
            }

            return medicalAllowanceDetailsDTOList;
        }
        return null;
    }


    private MedicalAllowanceDetailsDTO createMedicalAllowanceDetailsDTOByRequestAndIndex(HttpServletRequest request, int index, Boolean isLangEng) throws Exception {
        MedicalAllowanceDetailsDTO medicalAllowanceDetailsDTO;
        String language = isLangEng ? "ENGLISH" : "BANGLA";
        long id = Long.parseLong(request.getParameterValues("medicalAllowanceDetails.iD")[index]);
        String Value;
//        long medicalAllowanceId = Long.parseLong(Jsoup.clean(request.getParameterValues("medicalAllowanceDetails.medicalAllowanceId")[index], Whitelist.simpleText()));
        long allowanceDetailsDate = SessionConstants.MIN_DATE;
        Value = request.getParameterValues("medicalAllowanceDetails.date")[index];
        if (Value != null && !Value.equalsIgnoreCase("")) {
            allowanceDetailsDate = Long.parseLong(Jsoup.clean(Value, Whitelist.simpleText()));
        }
        String allowanceDetails = Jsoup.clean(request.getParameterValues("medicalAllowanceDetails.description")[index], Whitelist.simpleText());
        Value = request.getParameterValues("medicalAllowanceDetails.allowanceAmount")[index];
        if (Value != null && !Value.equalsIgnoreCase("")) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        } else {
            throw new Exception(LM.getText(LC.MEDICAL_ALLOWANCE_ADD_MEDICAL_ALLOWANCE_DETAILS_ALLOWANCEAMOUNT, language) + " " + ErrorMessage.getInvalidMessage(language));
        }
        int allowanceAmount = Integer.parseInt(Value);
        long filesDropzone = -1;
        Value = request.getParameterValues("medicalAllowanceDetails.filesDropzone")[index];
        if (Value != null && !Value.equalsIgnoreCase("")) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
            filesDropzone = Long.parseLong(Value);
        }
        if(id!=-1) {
            String filesDropzoneFilesToDelete = request.getParameterValues("medicalAllowanceDetails.filesDropzoneFilesToDelete")[index];
            if (filesDropzoneFilesToDelete != null && !filesDropzoneFilesToDelete.equalsIgnoreCase("")) {
                String[] deleteArray = filesDropzoneFilesToDelete.split(",");
                for (int i = 0; i < deleteArray.length; i++) {
                    if (i > 0) {
                        filesDAO.delete(Long.parseLong(deleteArray[i]));
                    }
                }
            }
        }
//        else {
//            throw new Exception(LM.getText(LC.AM_OFFICE_ASSIGNMENT_ADD_AM_OFFICE_ASSIGNMENT_FILES_FILESDROPZONE, language) + " " + ErrorMessage.getInvalidMessage(language));
//        }
        if (id == -1) {
            medicalAllowanceDetailsDTO = new MedicalAllowanceDetailsDTO();
//            medicalAllowanceDetailsDTO.medicalAllowanceId = medicalAllowanceId;
            medicalAllowanceDetailsDTO.date = allowanceDetailsDate;
            medicalAllowanceDetailsDTO.description = allowanceDetails;
            medicalAllowanceDetailsDTO.allowanceAmount = allowanceAmount;
            medicalAllowanceDetailsDTO.filesDropzone = filesDropzone;
        } else {
            medicalAllowanceDetailsDTO = medicalAllowanceDetailsDAO.getDTOFromID(id);
            if (medicalAllowanceDetailsDTO == null) {
                throw new Exception(isLangEng ? "Voucher information is not found" : "ভাউচার তথ্য পাওয়া যায়নি");
            }
            medicalAllowanceDetailsDTO.date = allowanceDetailsDate;
            medicalAllowanceDetailsDTO.description = allowanceDetails;
            medicalAllowanceDetailsDTO.allowanceAmount = allowanceAmount;
            medicalAllowanceDetailsDTO.filesDropzone = filesDropzone;
        }
        return medicalAllowanceDetailsDTO;

    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[]{MenuConstants.MEDICAL_ALLOWANCE_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[]{MenuConstants.MEDICAL_ALLOWANCE_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[]{MenuConstants.MEDICAL_ALLOWANCE_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Medical_allowanceServlet.class;
    }
}