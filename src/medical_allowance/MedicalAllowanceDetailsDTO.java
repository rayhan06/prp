package medical_allowance;

import sessionmanager.SessionConstants;
import util.CommonDTO;


public class MedicalAllowanceDetailsDTO extends CommonDTO {

    public long medicalAllowanceId = -1;
    public long date = SessionConstants.MIN_DATE;
    public String description = "";
    public int allowanceAmount = 0;
    public long filesDropzone=-1;
    public long insertedBy = -1;
    public long insertionTime = SessionConstants.MIN_DATE;
    public long modifiedBy = -1;

    @Override
    public String toString() {
        return "$MedicalAllowanceDetailsDTO[" +
                " iD = " + iD +
                " medicalAllowanceId = " + medicalAllowanceId +
                " date = " + date +
                " description = " + description +
                " allowanceAmount = " + allowanceAmount +
                " insertedBy = " + insertedBy +
                " insertionTime = " + insertionTime +
                " isDeleted = " + isDeleted +
                " modifiedBy = " + modifiedBy +
                " lastModificationTime = " + lastModificationTime +
                " searchColumn = " + searchColumn +
                "]";
    }

}