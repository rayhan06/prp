package medical_allowance;
import java.util.*; 
import util.*;


public class MedicalAllowanceDetailsMAPS extends CommonMaps
{	
	public MedicalAllowanceDetailsMAPS(String tableName)
	{
		



		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Voucher Number".toLowerCase(), "medicalAllowanceId".toLowerCase());
		java_Text_map.put("Date".toLowerCase(), "date".toLowerCase());
		java_Text_map.put("Description".toLowerCase(), "description".toLowerCase());
		java_Text_map.put("Allowance Amount".toLowerCase(), "allowanceAmount".toLowerCase());
		java_Text_map.put("Inserted By".toLowerCase(), "insertedBy".toLowerCase());
		java_Text_map.put("Insertion Time".toLowerCase(), "insertionTime".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("Modified By".toLowerCase(), "modifiedBy".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
		java_Text_map.put("Search Column".toLowerCase(), "searchColumn".toLowerCase());
			
	}

}