package employee_employment;

import common.CommonDAOService;
import org.apache.log4j.Logger;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

@SuppressWarnings({"Duplicates"})
public class Employee_employmentDAO implements CommonDAOService<Employee_employmentDTO> {

    private static final Logger logger = Logger.getLogger(Employee_employmentDAO.class);

    private static final String addQuery = "INSERT INTO {tableName} (name_en,name_bn,description,lastModificationTime," +
            "search_column,insertion_date,inserted_by,modified_by,isDeleted,ID) VALUES(?,?,?,?,?,?,?,?,?,?)";

    private static final String updateQuery = "UPDATE {tableName} SET name_en=?,name_bn=?,description=?,lastModificationTime = ?," +
            "search_column=?,insertion_date=?,inserted_by=?,modified_by=? WHERE ID = ?";

    private final Map<String, String> searchMap = new HashMap<>();

    private static final class InstanceHolder {
        static final Employee_employmentDAO INSTANCE = new Employee_employmentDAO();
    }

    public static Employee_employmentDAO getInstance(){
        return InstanceHolder.INSTANCE;
    }

    private Employee_employmentDAO() {
        searchMap.put("AnyField", " AND search_column LIKE ? ");
        searchMap.put("name_en", " AND name_en LIKE ? ");
        searchMap.put("name_bn", " AND name_bn LIKE ? ");
        searchMap.put("description", " AND description LIKE ? ");
    }

    @Override
    public void set(PreparedStatement ps, Employee_employmentDTO employee_employmentDTO, boolean isInsert) throws SQLException {
        int index = 0;
        ps.setObject(++index, employee_employmentDTO.nameEn);
        ps.setObject(++index, employee_employmentDTO.nameBn);
        ps.setObject(++index, employee_employmentDTO.description);
        ps.setObject(++index, System.currentTimeMillis());
        String searchColumn = employee_employmentDTO.nameEn + " "+employee_employmentDTO.nameBn;
        if(employee_employmentDTO.description != null && employee_employmentDTO.description.trim().length()>0){
            searchColumn += " "+employee_employmentDTO.description.trim();
        }
        ps.setObject(++index, searchColumn);
        ps.setObject(++index, employee_employmentDTO.insertionDate);
        ps.setObject(++index, employee_employmentDTO.insertedBy);
        ps.setObject(++index, employee_employmentDTO.modifiedBy);
        if (isInsert) {
            ps.setObject(++index, 0);
        }
        ps.setObject(++index, employee_employmentDTO.iD);
    }

    @Override
    public Employee_employmentDTO buildObjectFromResultSet(ResultSet rs) {
        try{
            Employee_employmentDTO employee_employmentDTO = new Employee_employmentDTO();
            employee_employmentDTO.iD = rs.getLong("ID");
            employee_employmentDTO.nameEn = rs.getString("name_en");
            employee_employmentDTO.nameBn = rs.getString("name_bn");
            employee_employmentDTO.description = rs.getString("description");
            employee_employmentDTO.isDeleted = rs.getInt("isDeleted");
            employee_employmentDTO.lastModificationTime = rs.getLong("lastModificationTime");
            employee_employmentDTO.searchColumn = rs.getString("search_column");
            return employee_employmentDTO;
        }catch (SQLException ex){
            logger.error(ex);
            return null;
        }
    }

    @Override
    public String getTableName() {
        return "employee_employment";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Employee_employmentDTO) commonDTO, addQuery, true);
    }

    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Employee_employmentDTO) commonDTO, updateQuery, false);
    }
}