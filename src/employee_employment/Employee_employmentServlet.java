package employee_employment;

import common.BaseServlet;
import common.CommonDAOService;
import permission.MenuConstants;
import user.UserDTO;
import util.CommonDTO;
import util.HttpRequestUtils;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;


@WebServlet("/Employee_employmentServlet")
@MultipartConfig
public class Employee_employmentServlet extends BaseServlet {
    private static final long serialVersionUID = 1L;

    private final Employee_employmentDAO employee_employmentDAO = Employee_employmentDAO.getInstance();

    @Override
    public String getTableName() {
        return employee_employmentDAO.getTableName();
    }

    @Override
    public String getServletName() {
        return "Employee_employmentServlet";
    }

    @Override
    public CommonDAOService<Employee_employmentDTO> getCommonDAOService() {
        return employee_employmentDAO;
    }

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        Employee_employmentDTO employee_employmentDTO;
        if (addFlag) {
            employee_employmentDTO = new Employee_employmentDTO();
            employee_employmentDTO.insertionDate = System.currentTimeMillis();
            employee_employmentDTO.insertedBy = userDTO.ID;
        } else {
            employee_employmentDTO = employee_employmentDAO.getDTOFromID(Long.parseLong(request.getParameter("iD")));
        }
        validateAndSet(employee_employmentDTO, request, HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng);
        employee_employmentDTO.lastModificationTime = System.currentTimeMillis();
        employee_employmentDTO.modifiedBy = userDTO.ID;

        if (addFlag) {
            employee_employmentDAO.add(employee_employmentDTO);
        } else {
            employee_employmentDAO.update(employee_employmentDTO);
        }
        return employee_employmentDTO;
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[]{MenuConstants.EMPLOYEE_EMPLOYMENT_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[]{MenuConstants.EMPLOYEE_EMPLOYMENT_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[]{MenuConstants.EMPLOYEE_EMPLOYMENT_SEARCH};
    }

    @Override
    public Class<Employee_employmentServlet> getClazz() {
        return Employee_employmentServlet.class;
    }
}
