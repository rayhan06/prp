package employee_employment;

import org.apache.log4j.Logger;
import pb.OptionDTO;
import pb.Utils;
import repository.Repository;
import repository.RepositoryManager;
import util.LockManager;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

@SuppressWarnings("Duplicates")
public class Employee_employmentRepository implements Repository {
    private final Employee_employmentDAO employee_employmentDAO;

    private static final Logger logger = Logger.getLogger(Employee_employmentRepository.class);

    private final Map<Long, Employee_employmentDTO> mapOfEmployee_employmentDTOToiD;

    private List<Employee_employmentDTO> employeeEmploymentDTOList;

    private Employee_employmentRepository() {
        employee_employmentDAO = Employee_employmentDAO.getInstance();
        mapOfEmployee_employmentDTOToiD = new ConcurrentHashMap<>();
        RepositoryManager.getInstance().addRepository(this);
    }

    private static class LazyLoader {
        static Employee_employmentRepository INSTANCE = new Employee_employmentRepository();
    }

    public static Employee_employmentRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

    public void reload(boolean reloadAll) {
        employee_employmentDAO.getAllDTOs(reloadAll)
                .stream()
                .peek(this::removeDTOIfPresent)
                .filter(dto -> dto.isDeleted == 0)
                .forEach(dto -> mapOfEmployee_employmentDTOToiD.put(dto.iD, dto));
        employeeEmploymentDTOList = new ArrayList<>(this.mapOfEmployee_employmentDTOToiD.values());
        employeeEmploymentDTOList.sort(Comparator.comparingLong(o -> o.iD));
    }

    private void removeDTOIfPresent(Employee_employmentDTO dto) {
        if (mapOfEmployee_employmentDTOToiD.get(dto.iD) != null) {
            mapOfEmployee_employmentDTOToiD.remove(dto.iD);
        }
    }

    public List<Employee_employmentDTO> getEmployee_employmentList() {
        return employeeEmploymentDTOList;
    }


    public Employee_employmentDTO getEmployee_employmentDTOByID(long id) {
        if (mapOfEmployee_employmentDTOToiD.get(id) == null) {
            synchronized (LockManager.getLock(id + "EEP")) {
                if (mapOfEmployee_employmentDTOToiD.get(id) == null) {
                    try {
                        Employee_employmentDTO commonDTO = employee_employmentDAO.getDTOFromID(id);
                        if (commonDTO != null) {
                            mapOfEmployee_employmentDTOToiD.put(commonDTO.iD, commonDTO);
                        }
                    } catch (Exception e) {
                        logger.error(e);
                    }
                }
            }
        }
        return mapOfEmployee_employmentDTOToiD.get(id);
    }

    @Override
    public String getTableName() {
        return "employee_employment";
    }

    public String buildOptions(String language, Long selectedId) {
        List<OptionDTO> optionDTOList = null;
        if (employeeEmploymentDTOList != null && employeeEmploymentDTOList.size() > 0) {
            optionDTOList = employeeEmploymentDTOList.stream()
                    .map(dto -> new OptionDTO(dto.nameEn, dto.nameBn, String.valueOf(dto.iD)))
                    .collect(Collectors.toList());
        }
        return Utils.buildOptions(optionDTOList, language, selectedId == null ? null : String.valueOf(selectedId));
    }

    public String getText(long id, String language) {
        Employee_employmentDTO dto = getEmployee_employmentDTOByID(id);
        return dto == null ? "" : ("Bangla".equalsIgnoreCase(language) ? dto.nameBn : dto.nameEn);
    }
}


