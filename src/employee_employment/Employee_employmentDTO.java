package employee_employment;

import annotation.NotEmpty;
import util.CommonDTO;

public class Employee_employmentDTO extends CommonDTO {
    @NotEmpty(engError = "Please Insert English Name",bngError = "অনুগ্রহকরে ইংরেজীতে নাম লিখুন")
    public String nameEn = "";
    @NotEmpty(engError = "Please Insert English Name",bngError = "অনুগ্রহকরে বাংলায় নাম লিখুন")
    public String nameBn = "";
    @NotEmpty(engError = "Please Insert Description",bngError = "অনুগ্রহকরে বিবরন লিখুন")
    public String description = "";

    public long insertionDate = 0;
    public long insertedBy = 0;
    public long modifiedBy = 0;

    @Override
    public String toString() {
        return "$Employee_employmentDTO[" +
                " iD = " + iD +
                " nameEn = " + nameEn +
                " nameBn = " + nameBn +
                " description = " + description +
                " isDeleted = " + isDeleted +
                " lastModificationTime = " + lastModificationTime +
                "]";
    }

}