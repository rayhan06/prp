package available_stock_report;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import language.LC;
import language.LM;
import login.LoginDTO;
import pbReport.ReportService;
import permission.MenuConstants;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.ReportRequestHandler;

@WebServlet("/Available_stock_report_Servlet")
public class Available_stock_report_Servlet  extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	String[][] Criteria =
	{
		{"criteria","","medical_agent_type","like","","String","","","%","medicalAgentType", LC.CURRENT_STOCK_REPORT_SELECT_MEDICALAGENTTYPE + ""},		
		{"criteria","","item_id","=","AND","long","","","any","item_id", LC.CURRENT_STOCK_REPORT_SELECT_ITEMNAME + ""},	

	};
	
	String[][] Display =
	{
		{"display","","medical_agent_type","basic",""},		
		{"display","","item_name","basic",""},		
		{"display","","current_stock","int",""},
		{"display","","available_stock","int",""},
		{"display","","alert_level","int",""},
		{"display","","unit_price","double",""}
	};

	
	String GroupBy = "";
	String OrderBY = "";
	
	ReportRequestHandler reportRequestHandler;
	
	public Available_stock_report_Servlet(){

	}
	
	private final ReportService reportService = new ReportService();
	
	private String sql;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String language = "english";
		if(userDTO!= null && userDTO.languageID == SessionConstants.BANGLA)
		{
			language = "bangla";
		}
				
		String actionType = request.getParameter("actionType");

		System.out.println("In ssservlet doget, actiontype = " + actionType);
		
		sql = "(" +
				"SELECT \r\n" + 
				"        CONCAT(name_en, ', ', strength) AS item_name,\r\n" + 
				"            current_stock,\r\n" + 
				"            available_stock,\r\n" + 
				"            'drug' AS medical_agent_type,\r\n" + 
				"            minimul_level_for_alert AS alert_level,\r\n" + 
				"			drug_information.id as item_id, " +
				"			unit_price " +
				"    FROM\r\n" + 
				"        drug_information where current_stock > 0" +
				" UNION " +
			
				" SELECT \r\n" + 
				"        name_en AS item_name,\r\n" + 
				"            current_stock,\r\n" + 
				"            current_stock as available_stock,\r\n" + 
				"            'xray or equipment' AS medical_agent_type,\r\n" + 
				"            stock_alert_quantity AS alert_level,\r\n" + 
				"			medical_equipment_name.id as item_id, " +
				"			unit_price " +
				"    FROM\r\n" + 
				"        medical_equipment_name where current_stock > 0 " +
				
				
				") AS temp";
		

		Display[0][4] = LM.getText(LC.CURRENT_STOCK_REPORT_SELECT_MEDICALAGENTTYPE, loginDTO);
		Display[1][4] = LM.getText(LC.CURRENT_STOCK_REPORT_SELECT_ITEMNAME, loginDTO);
		Display[2][4] = LM.getText(LC.CURRENT_STOCK_REPORT_SELECT_CURRENTSTOCK, loginDTO);
		Display[3][4] = language.equalsIgnoreCase("english")?"Available Stock":"ব্যবহার্য স্টক";
		Display[4][4] = LM.getText(LC.DRUG_INFORMATION_ADD_MINIMULLEVELFORALERT, loginDTO);
		Display[5][4] = language.equalsIgnoreCase("english")?"Unit Price":"এককের মূল্য";

		
		String reportName = language.equalsIgnoreCase("english")?"Available Stock Report":"ব্যবহার্য স্টক রিপোর্ট";
		
		reportRequestHandler = new ReportRequestHandler(null,
				Criteria, Display, GroupBy, OrderBY, sql,
				reportService);
		
		reportRequestHandler.clientSideDataFormatting = new ArrayList<Pair<Integer, Integer>>();
		reportRequestHandler.clientSideDataFormatting.add(new ImmutablePair<Integer, Integer>(2, ReportRequestHandler.RIGHT_ALIGN_INT));
		reportRequestHandler.clientSideDataFormatting.add(new ImmutablePair<Integer, Integer>(3, ReportRequestHandler.RIGHT_ALIGN_INT));
		reportRequestHandler.clientSideDataFormatting.add(new ImmutablePair<Integer, Integer>(4, ReportRequestHandler.RIGHT_ALIGN_FLOAT));

		
		reportRequestHandler.handleReportGet(request, response, userDTO, "available_stock_report",
				MenuConstants.AVAILABLE_STOCK_REPORT_DETAILS, language, reportName, "available_stock_report");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		doGet(request, response);
	}
}
