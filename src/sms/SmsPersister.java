package sms;

import org.apache.log4j.Logger;
import sms_log.Sms_logDAO;
import sms_log.Sms_logDTO;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class SmsPersister implements Runnable{

    Logger logger = Logger.getLogger( SmsPersister.class );

    private SmsPersister(){}

    private static final SmsPersister smsPersister = new SmsPersister();
    private static final BlockingQueue<Sms_logDTO> queue = new LinkedBlockingQueue<>();
    private final Sms_logDAO sms_logDAO = new Sms_logDAO();

    public static SmsPersister getInstance(){

        return smsPersister;
    }

    public void persist( Sms_logDTO sms_logDTO ){

        SmsPersister.queue.offer( sms_logDTO );
    }

    @Override
    public void run() {

        while( true ){

            Sms_logDTO smsLogDTO = null;

            try {

                synchronized (this) {
                    smsLogDTO = SmsPersister.queue.take();
                }
                sms_logDAO.add( smsLogDTO );

            } catch (InterruptedException e) {

                logger.error( "Error while reading sms log dto from blocking queue, ", e );
            } catch (Exception e) {

                logger.error( "Error while persisting sms log dto, ", e );
            }
        }
    }
}
