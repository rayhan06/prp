package sms;

import org.apache.log4j.Logger;
import sms_log.Sms_logDAO;
import sms_log.Sms_logDTO;
import util.CommonConstant;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class SmsJobIdUpdater implements Runnable {
    private static final Logger logger = Logger.getLogger(SmsJobIdUpdater.class);
    private static final BlockingQueue<Sms_logDTO> queue = new LinkedBlockingQueue<>();
    private final List<Sms_logDTO> sms_logDTOList = new LinkedList<>();
    private final Sms_logDAO sms_logDAO = Sms_logDAO.getInstance();
    private Long lastUpdateTime = 0L;

    public static void updateJobIdAndStatus(Sms_logDTO sms_logDTO ){

        queue.offer( sms_logDTO );
    }

    @Override
    public void run() {

        while (true){

            Sms_logDTO smsLogDTO = null;

            try{

                smsLogDTO = queue.take();
                update( smsLogDTO );

            }catch (InterruptedException e) {

                flushQueue();
            }
            catch ( Exception e ){

                //putInQueueAgainIfTimeout( e, mailInstance, 5000L );
            }
            catch ( Throwable t ){

                logger.error("",t);
            }
            finally {

                if( Thread.currentThread().isInterrupted() ){

                    queue.offer( smsLogDTO );
                    flushQueue();
                }
            }
        }
    }

    private void update( Sms_logDTO smsLogDTO ) {

        sms_logDTOList.add( smsLogDTO );
        if( sms_logDTOList.size() == CommonConstant.SMS_JOB_ID_UPDATE_BATCH_SIZE || System.currentTimeMillis() - lastUpdateTime > CommonConstant.JOB_ID_UPDATE_THRESHOLD_TIME_MILLISECOND ){

            lastUpdateTime = System.currentTimeMillis();
            sms_logDAO.updateJobIdAndStatusBySmsLogList( sms_logDTOList );
            sms_logDTOList.clear();
        }
    }

    private void flushQueue() {

    }
}
