package sms;

import org.apache.log4j.Logger;

import sms_log.Sms_logDAO;
import sms_log.Sms_logDTO;
import util.HttpUtil;

import java.io.IOException;
import java.io.InputStream;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Properties;

public class SmsService {

    private static String url;
    private static boolean smsEnabled;
    private static final Logger logger = Logger.getLogger(SmsService.class);
    

    static {
        Properties smsProperties = new Properties();
        InputStream is = SmsService.class.getResourceAsStream("/app.properties");
        try {
            smsProperties.load(is);
            url = smsProperties.getProperty("sms.url");
            smsEnabled = Boolean.parseBoolean(smsProperties.getProperty("sms.enable"));
            logger.debug("smsEnabled : " + smsEnabled);           
        } catch (IOException e) {
            logger.error("",e);
        }
    }

    public static String send(String receiver, String message) throws IOException {
        if (smsEnabled) {
        	Sms_logDAO sms_logDAO = new Sms_logDAO();
            String urlEncodedMessage = URLEncoder.encode(message, StandardCharsets.UTF_8.toString());
            //String urlEncodedReceiver = URLEncoder.encode(receiver, StandardCharsets.UTF_8.toString());
            String smsUrl = url + "&mobile=" + receiver + "&sms=" + urlEncodedMessage;
            logger.debug("smsUrl = " + smsUrl + " receiver = " + receiver);
            Sms_logDTO sms_logDTO = new Sms_logDTO();
            sms_logDTO.receiver = receiver;
            sms_logDTO.deliveryTime = System.currentTimeMillis();
            sms_logDTO.message = message;
            sms_logDTO.insertionTime = sms_logDTO.sendingTime = sms_logDTO.deliveryTime;
            try {
				sms_logDAO.add(sms_logDTO);
			} catch (Exception e) {
				// TODO Auto-generated catch block
                logger.error("",e);
                logger.error("--------  "+e.getMessage()+" ---------");
			}
            return HttpUtil.sendGet(smsUrl, null);
        } else {
            logger.debug("smsEnabled: false");
        }
        return "";
    }
}