package sms;

import sms_log.Sms_logDAO;
import sms_log.Sms_logDTO;

import java.util.List;

public class SmsQueuePopulator implements Runnable{

    Long lastInsertionTime = 0L;
    Sms_logDAO smsLogDAO = Sms_logDAO.getInstance();

    @Override
    public void run() {

        List<Sms_logDTO> smsLogDTOList = smsLogDAO.getAllSmsLogByInsertionTimeGreater( lastInsertionTime );
        for( Sms_logDTO smsLogDTO: smsLogDTOList ){

            SmsSender.getInstance().send( smsLogDTO );
            lastInsertionTime = Math.max( lastInsertionTime, smsLogDTO.insertionTime );
        }
    }
}
