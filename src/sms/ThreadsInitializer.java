package sms;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class ThreadsInitializer implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {

        //Thread smsSenderThread = new Thread( new SmsSender() );
        //Thread smsJobIdUpdaterThread = new Thread( new SmsJobIdUpdater() );

        //smsSenderThread.start();
        //smsJobIdUpdaterThread.start();

        //ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();
        //executorService.scheduleWithFixedDelay( new SmsQueuePopulator(), 0, CommonConstant.SMS_QUEUE_POPULATOR_DELAY_SECOND, TimeUnit.SECONDS);
    	
//    	ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor(r -> {
//    	    Thread thread = new Thread(r);
//    	    thread.setDaemon(true);
//    	    return thread;
//        });
//    	executorService.scheduleAtFixedRate(new AttendanceAlertSender(), 5, new Attendance_alert_detailsDAO().getTimePeriodInSecFromGlobalConfig(), TimeUnit.SECONDS);
//
//        ExecutorService executorService1 = Executors.newFixedThreadPool(2,r -> {
//            Thread thread = new Thread(r);
//            thread.setDaemon(true);
//            return thread;
//        });
//        CountDownLatch countDownLatch = new CountDownLatch(3);
//
//        executorService1.submit(() -> {
//            AuditLogRepository.getInstance();
//            countDownLatch.countDown();
//        });
//
//        executorService1.submit( () -> {
//            SmsSender.getInstance();
//            countDownLatch.countDown();
//        } );
//
//        executorService1.submit( () -> {
//            SmsPersister.getInstance();
//            countDownLatch.countDown();
//        });
//        try {
//            countDownLatch.await();
//            System.out.println("3 tasks has been completed successfully...");
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//        executorService1.shutdown();

    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {

    }
}
