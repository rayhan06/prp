package sms;

import java.util.List;

public class SmsResponse {

    String ErrorCode;
    String ErrorMessage;
    String JobId;
    List<MessageDataResponse> MessageData;

    public String getErrorCode() {
        return ErrorCode;
    }

    public void setErrorCode(String errorCode) {
        ErrorCode = errorCode;
    }

    public String getErrorMessage() {
        return ErrorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        ErrorMessage = errorMessage;
    }

    public String getJobId() {
        return JobId;
    }

    public void setJobId(String jobId) {
        JobId = jobId;
    }

    public List<MessageDataResponse> getMessageData() {
        return MessageData;
    }

    public void setMessageData(List<MessageDataResponse> messageData) {
        MessageData = messageData;
    }
}
