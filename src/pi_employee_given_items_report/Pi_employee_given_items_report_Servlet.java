package pi_employee_given_items_report;


import java.io.IOException;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import language.LC;
import language.LM;

import login.LoginDTO;
import permission.MenuConstants;

import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;

import util.ReportRequestHandler;
import pbReport.*;






@WebServlet("/Pi_employee_given_items_report_Servlet")
public class Pi_employee_given_items_report_Servlet  extends HttpServlet {
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	String[][] Criteria =
	{
		{"criteria","puia","office_unit_id","=","","String","","","any","officeUnitId", LC.PI_OFFICE_WISE_INVENTORY_REPORT_WHERE_OFFICEUNITID + ""},
		{"criteria","puia","org_id","=","AND","String","","","any","orgId", LC.PI_EMPLOYEE_GIVEN_ITEMS_REPORT_WHERE_ORGID + ""},
		{"criteria","puia","isDeleted","=","AND","String","","","0","isDeleted", LC.PI_EMPLOYEE_GIVEN_ITEMS_REPORT_WHERE_ISDELETED + ""}
	};

	String[][] Display =
	{
		{"display","puia","office_unit_id","office_unit",""},
		{"display","puia","org_id","organogram",""},
		{"display","puia","item_id","productIdConverter",""},
		{"display","","SUM(CASE WHEN action_type = 2 THEN 1 ELSE 0 END) ","text",""}
	};

	String GroupBy = "puia.org_id";
	String OrderBY = "";

	ReportRequestHandler reportRequestHandler;

	public Pi_employee_given_items_report_Servlet(){

	}

	private final ReportService reportService = new ReportService();

	private String sql;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{

		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String language = "english";
		if(userDTO!= null && userDTO.languageID == SessionConstants.BANGLA)
		{
			language = "bangla";
		}

		String actionType = request.getParameter("actionType");

		System.out.println("In ssservlet doget, actiontype = " + actionType);

		sql = "pi_unique_item_assignment puia";

		Display[0][4] = LM.getText(LC.PI_EMPLOYEE_GIVEN_ITEMS_REPORT_SELECT_OFFICEUNITID, loginDTO);
		Display[1][4] = LM.getText(LC.PI_EMPLOYEE_GIVEN_ITEMS_REPORT_SELECT_ORGID, loginDTO);
		Display[2][4] = LM.getText(LC.PI_EMPLOYEE_GIVEN_ITEMS_REPORT_SELECT_ITEMID, loginDTO);
		Display[3][4] = LM.getText(LC.PI_OFFICE_WISE_INVENTORY_REPORT_SELECT_GIVEN, loginDTO);


		String reportName = LM.getText(LC.PI_EMPLOYEE_GIVEN_ITEMS_REPORT_OTHER_PI_EMPLOYEE_GIVEN_ITEMS_REPORT, loginDTO);

		reportRequestHandler = new ReportRequestHandler(null,
				Criteria, Display, GroupBy, OrderBY, sql,
				reportService);


		reportRequestHandler.handleReportGet(request, response, userDTO, "pi_employee_given_items_report",
				MenuConstants.PI_OFFICE_WISE_INVENTORY_REPORT_DETAILS, language, reportName, "pi_employee_given_items_report");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		doGet(request, response);
	}
}
