package transaction_report;


import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import language.LC;
import language.LM;

import login.LoginDTO;
import permission.MenuConstants;

import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;

import util.ReportRequestHandler;
import pbReport.*;






@WebServlet("/Transaction_report_Servlet")
public class Transaction_report_Servlet  extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	String[][] Criteria =
	{
		{"criteria","","transaction_date",">=","","long","","",Long.MIN_VALUE + "","startDate", LC.HM_START_DATE + ""},		
		{"criteria","","transaction_date","<=","AND","long","","",Long.MAX_VALUE + "","endDate", LC.HM_END_DATE + ""},		
		{"criteria","","drug_information_id","=","AND","long","","","any","drug_information_id",LC.HM_DRUGS + ""},
		{"criteria","","transaction_type","=","AND","int","","","any","medical_transaction_cat", LC.TRANSACTION_REPORT_SELECT_TRANSACTIONTYPE + ""},
		{"criteria","","employee_record_id","=","AND","String","","","any","userName",LC.PHARMACY_REPORT_WHERE_EMPLOYEEID + "", "userNameToEmployeeRecordId"}
		
	};
	
	String[][] Display =
	{
		{"display","","transaction_date","date",""},		
		{"display","","CONCAT(drug_information.name_en, ', ', strength)","basic",""},		
		{"display","","quantity","int",""},	

		{"display","","quantity * drug_information.unit_price","double",""},	
		{"display","","transaction_type","medical_transaction",""},
		{"display","","employee_record_id","erIdToUserName",""},
		{"display","","employee_record_id","erIdToName",""}
	};
	
	String GroupBy = "";
	String OrderBY = "transaction_time DESC";
	
	ReportRequestHandler reportRequestHandler;
	
	public Transaction_report_Servlet(){

	}
	
	private final ReportService reportService = new ReportService();
	
	private String sql;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String language = "english";
		if(userDTO!= null && userDTO.languageID == SessionConstants.BANGLA)
		{
			language = "bangla";
		}
				
		String actionType = request.getParameter("actionType");

		System.out.println("In ssservlet doget, actiontype = " + actionType);
		
		sql = "medical_transaction\r\n" + 
				"        JOIN\r\n" + 
				"    drug_information ON (drug_information.id = medical_transaction.drug_information_id)";

		Display[0][4] = LM.getText(LC.TRANSACTION_REPORT_SELECT_TRANSACTIONDATE, loginDTO);
		Display[1][4] = LM.getText(LC.TRANSACTION_REPORT_SELECT_ITEM, loginDTO);
		Display[2][4] = LM.getText(LC.HM_COUNT, loginDTO);
		Display[3][4] = LM.getText(LC.HM_COST, loginDTO);
		Display[4][4] = LM.getText(LC.TRANSACTION_REPORT_SELECT_TRANSACTIONTYPE, loginDTO);
		Display[5][4] = LM.getText(LC.HM_USER_NAME, loginDTO);
		Display[6][4] = LM.getText(LC.HM_NAME, loginDTO);

		
		String reportName = LM.getText(LC.TRANSACTION_REPORT_OTHER_TRANSACTION_REPORT, loginDTO);
		
		reportRequestHandler = new ReportRequestHandler(null,
				Criteria, Display, GroupBy, OrderBY, sql,
				reportService);
		
		reportRequestHandler.clientSideDataFormatting = new ArrayList<Pair<Integer, Integer>>();
		reportRequestHandler.clientSideDataFormatting.add(new ImmutablePair<Integer, Integer>(2, ReportRequestHandler.RIGHT_ALIGN_INT));		
		reportRequestHandler.clientSideDataFormatting.add(new ImmutablePair<Integer, Integer>(3, ReportRequestHandler.RIGHT_ALIGN_FLOAT));

		
		reportRequestHandler.handleReportGet(request, response, userDTO, "transaction_report",
				MenuConstants.TRANSACTION_REPORT_DETAILS, language, reportName, "transaction_report");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		doGet(request, response);
	}
}
