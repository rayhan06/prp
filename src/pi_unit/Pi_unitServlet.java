package pi_unit;

import com.google.gson.Gson;
import common.BaseServlet;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import pb.Utils;
import permission.MenuConstants;
import sessionmanager.SessionConstants;
import user.UserDTO;
import util.CommonDTO;
import util.TimeConverter;
import util.UtilCharacter;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;


/**
 * Servlet implementation class Pi_unitServlet
 */
@WebServlet("/Pi_unitServlet")
@MultipartConfig
public class Pi_unitServlet extends BaseServlet
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Pi_unitServlet.class);

    @Override
    public String getTableName() {
        return Pi_unitDAO.getInstance().getTableName();
    }

    @Override
    public String getServletName() {
        return "Pi_unitServlet";
    }

    @Override
    public Pi_unitDAO getCommonDAOService() {
        return Pi_unitDAO.getInstance();
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[] {MenuConstants.PI_UNIT_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[] {MenuConstants.PI_UNIT_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[] {MenuConstants.PI_UNIT_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Pi_unitServlet.class;
    }
    private final Gson gson = new Gson();


	@Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception
	{
		// TODO Auto-generated method stub

		{
			request.setAttribute("failureMessage", "");
			System.out.println("%%%% addPi_unit");
			String language = userDTO.languageID == SessionConstants.ENGLISH? "English":"Bangla";
			Pi_unitDTO pi_unitDTO;
			SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");

			if(addFlag == true)
			{
				pi_unitDTO = new Pi_unitDTO();
			}
			else
			{
				pi_unitDTO = Pi_unitDAO.getInstance().getDTOFromID(Long.parseLong(request.getParameter("iD")));
			}

			String Value = "";

			Value = request.getParameter("nameEn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("nameEn = " + Value);
			if(Value != null)
			{
				if(!Utils.notBangla(Value)){
					String errMsg = UtilCharacter.getDataByLanguage(language, "ইংরেজি মান আবশ্যক",
							"English Value Required");
					throw new Exception(errMsg);
				}
				pi_unitDTO.nameEn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
				throw new Exception("Required Field Missing");
			}

			Value = request.getParameter("nameBn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("nameBn = " + Value);
			if(Value != null)
			{
				if(!Utils.notEnglish(Value)){
					String errMsg = UtilCharacter.getDataByLanguage(language, "বাংলা মান আবশ্যক",
							"Bangla Value Required");
					throw new Exception(errMsg);
				}
				pi_unitDTO.nameBn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
                throw new Exception("Required Field Missing");
            }

			Value = request.getParameter("searchColumn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("searchColumn = " + Value);
			if(Value != null)
			{
				pi_unitDTO.searchColumn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			if(addFlag)
			{
				pi_unitDTO.insertedByUserId = userDTO.ID;
			}


			if(addFlag)
			{
				pi_unitDTO.insertedByOrganogramId = userDTO.organogramID;
			}


			if(addFlag)
			{
				pi_unitDTO.insertionDate = TimeConverter.getToday();
			}


			pi_unitDTO.lastModifierUser = userDTO.userName;
			pi_unitDTO.lastModificationTime = System.currentTimeMillis();


			System.out.println("Done adding  addPi_unit dto = " + pi_unitDTO);

			uniquenessCheck(pi_unitDTO, language);


			Utils.handleTransaction(()->{
				if(addFlag)
				{
					Pi_unitDAO.getInstance().add(pi_unitDTO);
				}
				else
				{
					Pi_unitDAO.getInstance().update(pi_unitDTO);
				}
			});


			return pi_unitDTO;

		}

	}

	private void uniquenessCheck(Pi_unitDTO unitDTO, String language) throws Exception {
		int existingCount = Pi_unitDAO.getInstance().getCount(null, "",
				" and ( name_en = '" + unitDTO.nameEn + "' or name_bn = '" + unitDTO.nameBn +
						"' ) and iD != " + unitDTO.iD);
		if (existingCount > 0) {
			String errMSg = language.equalsIgnoreCase("English") ? "Duplicate Value" : "ডুপ্লিকেট মান";
			throw new Exception(errMSg);
		}
	}

	@Override
	public void deleteT(List<Long> ids, UserDTO userDTO) {
		List<Long> childExists = null;
		try {
			childExists = Pi_unitDAO.getInstance().getDistinctColumnIds
					("pi_unit","procurement_goods", "pi_unit_id", ids);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if(childExists != null){
			childExists.forEach(ids::remove);
		}

		if (!ids.isEmpty()) {
			try {
				Utils.handleTransaction(()-> getCommonDAOService().deletePb(userDTO.employee_record_id, ids));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}

