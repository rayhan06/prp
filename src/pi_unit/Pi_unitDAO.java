package pi_unit;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.sql.SQLException;
import common.CommonDAOService;
import org.apache.log4j.Logger;
import util.*;
import pb.*;

public class Pi_unitDAO  implements CommonDAOService<Pi_unitDTO>
{
	
	Logger logger = Logger.getLogger(getClass());
	
	private static String addQuery = "";
	private static String updateQuery = "";
	private String[] columnNames = null;

	private Pi_unitDAO()
	{
		columnNames = new String[] 
		{
			"ID",
			"name_en",
			"name_bn",
			"search_column",
			"inserted_by_user_id",
			"inserted_by_organogram_id",
			"insertion_date",
			"last_modifier_user",
			"isDeleted",
			"lastModificationTime"
		};
		updateQuery = getUpdateQuery(columnNames);
        addQuery = getAddQuery(columnNames);
		
		searchMap.put("name_en"," and (name_en like ?)");
		searchMap.put("name_bn"," and (name_bn like ?)");
		searchMap.put("insertion_date_start"," and (insertion_date >= ?)");
		searchMap.put("insertion_date_end"," and (insertion_date <= ?)");
		searchMap.put("AnyField"," and (search_column like ?)");
	}

	private final Map<String,String> searchMap = new HashMap<>();

    private static class LazyLoader
    {
    	static final Pi_unitDAO INSTANCE = new Pi_unitDAO();
	}

	public static Pi_unitDAO getInstance()
	{
    	return LazyLoader.INSTANCE;
	}
	
	public void setSearchColumn(Pi_unitDTO pi_unitDTO)
	{
		pi_unitDTO.searchColumn = "";
		pi_unitDTO.searchColumn += pi_unitDTO.nameEn + " ";
		pi_unitDTO.searchColumn += pi_unitDTO.nameBn + " ";
	}
	
	@Override
	public void set(PreparedStatement ps, Pi_unitDTO pi_unitDTO, boolean isInsert) throws SQLException
	{
		int index = 0;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(pi_unitDTO);
		if(isInsert)
		{
			ps.setObject(++index,pi_unitDTO.iD);
		}
		ps.setObject(++index,pi_unitDTO.nameEn);
		ps.setObject(++index,pi_unitDTO.nameBn);
		ps.setObject(++index,pi_unitDTO.searchColumn);
		ps.setObject(++index,pi_unitDTO.insertedByUserId);
		ps.setObject(++index,pi_unitDTO.insertedByOrganogramId);
		ps.setObject(++index,pi_unitDTO.insertionDate);
		ps.setObject(++index,pi_unitDTO.lastModifierUser);
		if(isInsert)
		{
			ps.setObject(++index,pi_unitDTO.isDeleted);
		}
		ps.setObject(++index, lastModificationTime);
		if(!isInsert)
		{
			ps.setObject(++index,pi_unitDTO.iD);
		}
	}
	
	@Override
	public Pi_unitDTO buildObjectFromResultSet(ResultSet rs)
	{
		try
		{
			Pi_unitDTO pi_unitDTO = new Pi_unitDTO();
			int i = 0;
			pi_unitDTO.iD = rs.getLong(columnNames[i++]);
			pi_unitDTO.nameEn = rs.getString(columnNames[i++]);
			pi_unitDTO.nameBn = rs.getString(columnNames[i++]);
			pi_unitDTO.searchColumn = rs.getString(columnNames[i++]);
			pi_unitDTO.insertedByUserId = rs.getLong(columnNames[i++]);
			pi_unitDTO.insertedByOrganogramId = rs.getLong(columnNames[i++]);
			pi_unitDTO.insertionDate = rs.getLong(columnNames[i++]);
			pi_unitDTO.lastModifierUser = rs.getString(columnNames[i++]);
			pi_unitDTO.isDeleted = rs.getInt(columnNames[i++]);
			pi_unitDTO.lastModificationTime = rs.getLong(columnNames[i++]);
			return pi_unitDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			ex.printStackTrace();
			return null;
		}
	}
		
	public Pi_unitDTO getDTOByID (long id)
	{
		return getDTOFromID(id);
	}

	@Override
	public String getTableName() {
		return "pi_unit";
	}

	@Override
	public Map<String, String> getSearchMap() {
		return searchMap;
	}

	public long add(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Pi_unitDTO) commonDTO,addQuery,true);
	}

	public long update(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Pi_unitDTO) commonDTO,updateQuery,false);
	}
	
	@Override
	public String getLastModifierUser(){
		return "last_modifier_user";
    }
				
}
	