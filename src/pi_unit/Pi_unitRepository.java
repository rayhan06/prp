package pi_unit;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import com.google.gson.Gson;

import pb.CatDTO;
import procurement_package.Procurement_packageDTO;
import repository.Repository;
import repository.RepositoryManager;


public class Pi_unitRepository implements Repository {
	Pi_unitDAO pi_unitDAO = null;
	
	static Logger logger = Logger.getLogger(Pi_unitRepository.class);
	Map<Long, Pi_unitDTO>mapOfPi_unitDTOToiD;
	Gson gson;

  
	private Pi_unitRepository(){
		pi_unitDAO = Pi_unitDAO.getInstance();
		mapOfPi_unitDTOToiD = new ConcurrentHashMap<>();
		gson = new Gson();
		RepositoryManager.getInstance().addRepository(this);
	}

	private static class LazyLoader{
        final static Pi_unitRepository INSTANCE = new Pi_unitRepository();
    }

    public static Pi_unitRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

	public void reload(boolean reloadAll){
		try {
			List<Pi_unitDTO> pi_unitDTOs = pi_unitDAO.getAllDTOs(reloadAll);
			for(Pi_unitDTO pi_unitDTO : pi_unitDTOs) {
				Pi_unitDTO oldPi_unitDTO = getPi_unitDTOByiD(pi_unitDTO.iD);
				if( oldPi_unitDTO != null ) {
					mapOfPi_unitDTOToiD.remove(oldPi_unitDTO.iD);
				
					
				}
				if(pi_unitDTO.isDeleted == 0) 
				{
					
					mapOfPi_unitDTOToiD.put(pi_unitDTO.iD, pi_unitDTO);
				
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public Pi_unitDTO clone(Pi_unitDTO dto) {
		String raw = gson.toJson(dto);
		return gson.fromJson(raw, Pi_unitDTO.class);
	}
	
	
	public List<Pi_unitDTO> getPi_unitList() {
		List <Pi_unitDTO> pi_units = new ArrayList<Pi_unitDTO>(this.mapOfPi_unitDTOToiD.values());
		return pi_units;
	}
	
	public List<Pi_unitDTO> copyPi_unitList() {
		List <Pi_unitDTO> pi_units = getPi_unitList();
		return pi_units
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}
	
	
	
	public Pi_unitDTO getPi_unitDTOByiD( long iD){
		return mapOfPi_unitDTOToiD.get(iD);
	}
	
	public Pi_unitDTO copyPi_unitDTOByiD( long iD){
		return clone(mapOfPi_unitDTOToiD.get(iD));
	}


	public String getOptions (String language, long defaultValue)
	{

		String sOptions = "";

		if (defaultValue == CatDTO.CATDEFAULTNOTREQUIRED)
		{
			if (language.equalsIgnoreCase("English"))
			{
				sOptions += "<option value = '" + CatDTO.CATDEFAULTNOTREQUIRED + "'>Select</option>";
			}
			else
			{
				sOptions += "<option value = '" + CatDTO.CATDEFAULTNOTREQUIRED + "'>বাছাই করুন</option>";
			}
		}
		else if (defaultValue == CatDTO.CATDEFAULT)
		{
			if (language.equalsIgnoreCase("English"))
			{
				sOptions += "<option value = ''>Select</option>";
			}
			else
			{
				sOptions += "<option value = ''>বাছাই করুন</option>";
			}
		}

		List<Pi_unitDTO> dtos = copyPi_unitList();

		for (Pi_unitDTO dto : dtos) {
			String sOption =  "<option value = '" + dto.iD + "'";
			if(defaultValue != CatDTO.CATDEFAULTNOTREQUIRED && dto.iD == defaultValue)
			{
				sOption += " selected ";
			}
			String name = language.equals("English") ? dto.nameEn : dto.nameBn;
			sOption += ">"
					+ name
					+ "</option>";
			sOptions += sOption;
		}

        return sOptions;
	}


	public String getOptionsForSuggestion (String language, long defaultValue)
	{

		String sOptions = "";

		List<Pi_unitDTO> dtos = copyPi_unitList();

		for (Pi_unitDTO dto : dtos) {
			String name = language.equals("English") ? dto.nameEn : dto.nameBn;
			String sOption =  "<option value = '" + name + "'";
			sOption += ">";
			sOptions += sOption;
		}

        return sOptions;
	}

	public String getName (String language, long defaultValue)
	{
		Pi_unitDTO pi_unitDTO = getPi_unitDTOByiD(defaultValue);
		return language.equals("English") ? pi_unitDTO.nameEn : pi_unitDTO.nameBn;
	}

	@Override
	public String getTableName() {
		return pi_unitDAO.getTableName();
	}
}


