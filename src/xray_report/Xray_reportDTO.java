package xray_report;
import java.util.*; 
import util.*; 


public class Xray_reportDTO extends CommonDTO
{

    public String nameEn = "";
    public String nameBn = "";
    public String comment = "";
	public long insertedByUserId = -1;
	public long insertedByOrganogramId = -1;
	public long insertionDate = -1;
    public String lastModifierUser = "";
	
	public List<XrayReportDetailsDTO> xrayReportDetailsDTOList = new ArrayList<>();
	
    @Override
	public String toString() {
            return "$Xray_reportDTO[" +
            " iD = " + iD +
            " nameEn = " + nameEn +
            " nameBn = " + nameBn +
            " comment = " + comment +
            " insertedByUserId = " + insertedByUserId +
            " insertedByOrganogramId = " + insertedByOrganogramId +
            " insertionDate = " + insertionDate +
            " lastModifierUser = " + lastModifierUser +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}