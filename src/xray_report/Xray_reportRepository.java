package xray_report;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class Xray_reportRepository implements Repository {
	Xray_reportDAO xray_reportDAO = null;
	
	public void setDAO(Xray_reportDAO xray_reportDAO)
	{
		this.xray_reportDAO = xray_reportDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Xray_reportRepository.class);
	Map<Long, Xray_reportDTO>mapOfXray_reportDTOToiD;
	


	static Xray_reportRepository instance = null;  
	private Xray_reportRepository(){
		mapOfXray_reportDTOToiD = new ConcurrentHashMap<>();
		

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Xray_reportRepository getInstance(){
		if (instance == null){
			instance = new Xray_reportRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(xray_reportDAO == null)
		{
			xray_reportDAO = new Xray_reportDAO();
		}
		try {
			List<Xray_reportDTO> xray_reportDTOs = xray_reportDAO.getAllXray_report(reloadAll);
			for(Xray_reportDTO xray_reportDTO : xray_reportDTOs) {
				Xray_reportDTO oldXray_reportDTO = getXray_reportDTOByID(xray_reportDTO.iD);
				if( oldXray_reportDTO != null ) {
					mapOfXray_reportDTOToiD.remove(oldXray_reportDTO.iD);
				
					
					
				}
				if(xray_reportDTO.isDeleted == 0) 
				{
					
					mapOfXray_reportDTOToiD.put(xray_reportDTO.iD, xray_reportDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Xray_reportDTO> getXray_reportList() {
		List <Xray_reportDTO> xray_reports = new ArrayList<Xray_reportDTO>(this.mapOfXray_reportDTOToiD.values());
		return xray_reports;
	}
	
	
	public Xray_reportDTO getXray_reportDTOByID( long ID){
		return mapOfXray_reportDTOToiD.get(ID);
	}
	
	
	

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "xray_report";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


