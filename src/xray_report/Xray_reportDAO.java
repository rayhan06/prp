package xray_report;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;
import sessionmanager.SessionConstants;
import java.sql.SQLException;


import org.apache.log4j.Logger;

import dbm.*;

import repository.RepositoryManager;

import util.*;
import pb.*;
import user.UserDTO;

public class Xray_reportDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());
	XrayReportDetailsDAO xrayReportDetailsDAO = new XrayReportDetailsDAO();
	

	
	public Xray_reportDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";
		commonMaps = new Xray_reportMAPS(tableName);
		columnNames = new String[] 
		{
			"ID",
			"name_en",
			"name_bn",
			"comment",
			"inserted_by_user_id",
			"inserted_by_organogram_id",
			"insertion_date",
			"last_modifier_user",
			"isDeleted",
			"lastModificationTime"
		};
	}
	
	public Xray_reportDAO()
	{
		this("xray_report");		
	}
	
	public void setSearchColumn(Xray_reportDTO xray_reportDTO)
	{
		xray_reportDTO.searchColumn = "";
		xray_reportDTO.searchColumn += xray_reportDTO.nameEn + " ";
		xray_reportDTO.searchColumn += xray_reportDTO.nameBn + " ";
		xray_reportDTO.searchColumn += xray_reportDTO.comment + " ";
		
	}
	
	public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException
	{
		Xray_reportDTO xray_reportDTO = (Xray_reportDTO)commonDTO;
		int index = 1;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(xray_reportDTO);
		if(isInsert)
		{
			ps.setObject(index++,xray_reportDTO.iD);
		}
		ps.setObject(index++,xray_reportDTO.nameEn);
		ps.setObject(index++,xray_reportDTO.nameBn);
		ps.setObject(index++,xray_reportDTO.comment);
		ps.setObject(index++,xray_reportDTO.insertedByUserId);
		ps.setObject(index++,xray_reportDTO.insertedByOrganogramId);
		ps.setObject(index++,xray_reportDTO.insertionDate);
		ps.setObject(index++,xray_reportDTO.lastModifierUser);
		if(isInsert)
		{
			ps.setObject(index++, 0);
			ps.setObject(index++, lastModificationTime);
		}
	}
	
	public void get(Xray_reportDTO xray_reportDTO, ResultSet rs) throws SQLException
	{
		xray_reportDTO.iD = rs.getLong("ID");
		xray_reportDTO.nameEn = rs.getString("name_en");
		xray_reportDTO.nameBn = rs.getString("name_bn");
		xray_reportDTO.comment = rs.getString("comment");
		xray_reportDTO.insertedByUserId = rs.getLong("inserted_by_user_id");
		xray_reportDTO.insertedByOrganogramId = rs.getLong("inserted_by_organogram_id");
		xray_reportDTO.insertionDate = rs.getLong("insertion_date");
		xray_reportDTO.lastModifierUser = rs.getString("last_modifier_user");
		xray_reportDTO.isDeleted = rs.getInt("isDeleted");
		xray_reportDTO.lastModificationTime = rs.getLong("lastModificationTime");
	}
	
	
	
	
		
	

	//need another getter for repository
	public Xray_reportDTO getDTOByID (long ID)
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Xray_reportDTO xray_reportDTO = null;
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
			
            sql += " WHERE ID=" + ID;
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			if(rs.next()){
				xray_reportDTO = new Xray_reportDTO();

				get(xray_reportDTO, rs);

			}			
			
			
			
						
			List<XrayReportDetailsDTO> xrayReportDetailsDTOList = xrayReportDetailsDAO.getXrayReportDetailsDTOListByXrayReportID(xray_reportDTO.iD);
			xray_reportDTO.xrayReportDetailsDTOList = xrayReportDetailsDTOList;
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return xray_reportDTO;
	}
	
	
	
	
	public List<Xray_reportDTO> getDTOs(Collection recordIDs){
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Xray_reportDTO xray_reportDTO = null;
		List<Xray_reportDTO> xray_reportDTOList = new ArrayList<>();
		if(recordIDs.isEmpty()){
			return xray_reportDTOList;
		}
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
            
            sql += " WHERE ID IN ( ";

			for(int i = 0;i<recordIDs.size();i++){
				if(i!=0){
					sql+=",";
				}
				sql+=((ArrayList)recordIDs).get(i);
			}
			sql+=")  order by lastModificationTime desc";
			
			printSql(sql);
			
			logger.debug("sql " + sql);
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				xray_reportDTO = new Xray_reportDTO();
				get(xray_reportDTO, rs);
				System.out.println("got this DTO: " + xray_reportDTO);
				
				xray_reportDTOList.add(xray_reportDTO);

			}			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return xray_reportDTOList;
	
	}
	
	

	
	
	
	//add repository
	public List<Xray_reportDTO> getAllXray_report (boolean isFirstReload)
    {
		List<Xray_reportDTO> xray_reportDTOList = new ArrayList<>();

		String sql = "SELECT * FROM xray_report";
		sql += " WHERE ";
	

		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by xray_report.lastModificationTime desc";
		printSql(sql);
		
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;

		
		try{
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();
			rs = stmt.executeQuery(sql);
			

			while(rs.next()){
				Xray_reportDTO xray_reportDTO = new Xray_reportDTO();
				get(xray_reportDTO, rs);
				xray_reportDTO.xrayReportDetailsDTOList = xrayReportDetailsDAO.getXrayReportDetailsDTOListByXrayReportID(xray_reportDTO.iD);
				
				xray_reportDTOList.add(xray_reportDTO);
			}			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}

		return xray_reportDTOList;
    }

	
	public List<Xray_reportDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO)
	{
		return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
	}

	
	public List<Xray_reportDTO> getDTOs(Hashtable  p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
			String filter, boolean tableHasJobCat)
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		List<Xray_reportDTO> xray_reportDTOList = new ArrayList<>();

		try{
			
			String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat);
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				Xray_reportDTO xray_reportDTO = new Xray_reportDTO();
				get(xray_reportDTO, rs);
				
				xray_reportDTOList.add(xray_reportDTO);
			}					
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return xray_reportDTOList;
	
	}
	public String getSqlWithSearchCriteriaForNormalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
			UserDTO userDTO, String filter, boolean tableHasJobCat)
    {
		boolean viewAll = false;
		
		if(p_searchCriteria != null && p_searchCriteria.get("ViewAll") != null)
		{
			System.out.println("ViewAll = " + p_searchCriteria.get("ViewAll"));
			viewAll = true;
		}
		else
		{
			System.out.println("ViewAll is null ");
		}
		
		String sql = "SELECT ";
		if(category == GETIDS)
		{
			sql += " " + tableName + ".ID as ID ";
		}
		else if(category == GETCOUNT)
		{
			sql += " count( " + tableName + ".ID) as countID ";
		}
		else if(category == GETDTOS)
		{
			sql += " distinct " + tableName + ".* ";
		}
		sql += "FROM " + tableName + " ";
		sql += joinSQL;
		

		
		String AnyfieldSql = "";
		String AllFieldSql = "";
		
		if(p_searchCriteria != null)
		{
						
			
			Enumeration names = p_searchCriteria.keys();
			String str, value;
			
			AnyfieldSql = "(";
			
			if(p_searchCriteria.get("AnyField")!= null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase(""))
			{
				AnyfieldSql+= tableName + ".search_column like '%" + p_searchCriteria.get("AnyField").toString() + "%'";			
			}
			AnyfieldSql += ")";
			System.out.println("AnyfieldSql = " + AnyfieldSql);
			
			AllFieldSql = "(";
			int i = 0;
			while(names.hasMoreElements())
			{				
				str = (String) names.nextElement();
				value = (String)p_searchCriteria.get(str);
		        System.out.println(str + ": " + value);
				if(value != null && !value.equalsIgnoreCase("") && (
						 str.equals("name_en")
						|| str.equals("name_bn")
						|| str.equals("comment")
						|| str.equals("insertion_date_start")
						|| str.equals("insertion_date_end")
				)
						
				)
				{
					if(p_searchCriteria.get(str).equals("any"))
					{
						continue;
					}

					if( i > 0)
					{
						AllFieldSql+= " AND  ";
					}
					
					 if(str.equals("name_en"))
					{
						AllFieldSql += "" + tableName + ".name_en like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("name_bn"))
					{
						AllFieldSql += "" + tableName + ".name_bn like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("comment"))
					{
						AllFieldSql += "" + tableName + ".comment like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("insertion_date_start"))
					{
						AllFieldSql += "" + tableName + ".insertion_date >= " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("insertion_date_end"))
					{
						AllFieldSql += "" + tableName + ".insertion_date <= " + p_searchCriteria.get(str);
						i ++;
					}
					
					
				}
			}
			
			AllFieldSql += ")";
			System.out.println("AllFieldSql = " + AllFieldSql);
			
			
		}
		
		
		sql += " WHERE ";
		
		sql += " (" + tableName + ".isDeleted = 0 ";			
		sql += ")";
		
		
		if(!filter.equalsIgnoreCase(""))
		{
			sql += " and " + filter + " ";
		}
		
		if(!AnyfieldSql.equals("()") && !AnyfieldSql.equals(""))
		{
			sql += " AND " + AnyfieldSql;
			
		}
		if(!AllFieldSql.equals("()") && !AllFieldSql.equals(""))
		{			
			sql += " AND " + AllFieldSql;
		}
		
	
			
		sql += " order by " + tableName + ".lastModificationTime desc ";

		printSql(sql);
		
		if(limit >= 0)
		{
			sql += " limit " + limit;
		}
		if(offset >= 0)
		{
			sql += " offset " + offset;
		}
		
		System.out.println("-------------- sql = " + sql);
		
		return sql;
    }
	
	public String getSqlWithSearchCriteria(Hashtable p_searchCriteria, int limit, int offset, int category,
			boolean isPermanentTable, UserDTO userDTO, String filter, boolean tableHasJobCat)
    {		
		return getSqlWithSearchCriteriaForNormalSearch(p_searchCriteria, limit, offset, category, userDTO, filter, tableHasJobCat);	
    }
				
}
	