package xray_report;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;
import sessionmanager.SessionConstants;
import java.sql.SQLException;


import org.apache.log4j.Logger;

import dbm.*;

import repository.RepositoryManager;

import util.*;
import pb.*;
import user.UserDTO;

public class XrayReportDetailsDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());
	

	
	public XrayReportDetailsDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";
		commonMaps = new XrayReportDetailsMAPS(tableName);
		columnNames = new String[] 
		{
			"ID",
			"xray_report_id",
			"name_en",
			"name_bn",
			"title",
			"comment",
			"isDeleted",
			"lastModificationTime"
		};
	}
	
	public XrayReportDetailsDAO()
	{
		this("xray_report_details");		
	}
	
	public void setSearchColumn(XrayReportDetailsDTO xrayreportdetailsDTO)
	{
		xrayreportdetailsDTO.searchColumn = "";
		xrayreportdetailsDTO.searchColumn += xrayreportdetailsDTO.nameEn + " ";
		xrayreportdetailsDTO.searchColumn += xrayreportdetailsDTO.nameBn + " ";
		xrayreportdetailsDTO.searchColumn += xrayreportdetailsDTO.title + " ";
		xrayreportdetailsDTO.searchColumn += xrayreportdetailsDTO.comment + " ";
		
	}
	
	public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException
	{
		XrayReportDetailsDTO xrayreportdetailsDTO = (XrayReportDetailsDTO)commonDTO;
		int index = 1;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(xrayreportdetailsDTO);
		if(isInsert)
		{
			ps.setObject(index++,xrayreportdetailsDTO.iD);
		}
		ps.setObject(index++,xrayreportdetailsDTO.xrayReportId);
		ps.setObject(index++,xrayreportdetailsDTO.nameEn);
		ps.setObject(index++,xrayreportdetailsDTO.nameBn);
		ps.setObject(index++,xrayreportdetailsDTO.title);
		ps.setObject(index++,xrayreportdetailsDTO.comment);
		if(isInsert)
		{
			ps.setObject(index++, 0);
			ps.setObject(index++, lastModificationTime);
		}
	}
	
	public void get(XrayReportDetailsDTO xrayreportdetailsDTO, ResultSet rs) throws SQLException
	{
		xrayreportdetailsDTO.iD = rs.getLong("ID");
		xrayreportdetailsDTO.xrayReportId = rs.getLong("xray_report_id");
		xrayreportdetailsDTO.nameEn = rs.getString("name_en");
		xrayreportdetailsDTO.nameBn = rs.getString("name_bn");
		xrayreportdetailsDTO.title = rs.getString("title");
		xrayreportdetailsDTO.comment = rs.getString("comment");
		xrayreportdetailsDTO.isDeleted = rs.getInt("isDeleted");
		xrayreportdetailsDTO.lastModificationTime = rs.getLong("lastModificationTime");
	}
	
	
	
	
		
	
	public void deleteXrayReportDetailsByXrayReportID(long xrayReportID) throws Exception{
		
		
		Connection connection = null;
		Statement stmt = null;
		try{
			
			String sql = "UPDATE xray_report_details SET isDeleted=0 WHERE xray_report_id="+xrayReportID;			
			logger.debug("sql " + sql);
			connection = DBMW.getInstance().getConnection();
			stmt = connection.createStatement();
			stmt.execute(sql);
		}catch(Exception ex){
			logger.debug("fatal",ex);
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DBMW.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
	}		
   
	public List<XrayReportDetailsDTO> getXrayReportDetailsDTOListByXrayReportID(long xrayReportID) throws Exception{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		XrayReportDetailsDTO xrayreportdetailsDTO = null;
		List<XrayReportDetailsDTO> xrayreportdetailsDTOList = new ArrayList<>();
		
		try{
			
			String sql = "SELECT * FROM xray_report_details where isDeleted=0 and xray_report_id="+xrayReportID+" order by xray_report_details.lastModificationTime";
			
			logger.debug("sql " + sql);
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();
			rs = stmt.executeQuery(sql);

			while(rs.next()){
				xrayreportdetailsDTO = new XrayReportDetailsDTO();
				get(xrayreportdetailsDTO, rs);
				xrayreportdetailsDTOList.add(xrayreportdetailsDTO);

			}			
			
		}catch(Exception ex){
			logger.debug("fatal",ex);
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return xrayreportdetailsDTOList;
	}

	//need another getter for repository
	public XrayReportDetailsDTO getDTOByID (long ID)
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		XrayReportDetailsDTO xrayreportdetailsDTO = null;
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
			
            sql += " WHERE ID=" + ID;
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			if(rs.next()){
				xrayreportdetailsDTO = new XrayReportDetailsDTO();

				get(xrayreportdetailsDTO, rs);

			}			
			
			
			
			XrayReportDetailsDAO xrayReportDetailsDAO = new XrayReportDetailsDAO("xray_report_details");			
			List<XrayReportDetailsDTO> xrayReportDetailsDTOList = xrayReportDetailsDAO.getXrayReportDetailsDTOListByXrayReportID(xrayreportdetailsDTO.iD);
			xrayreportdetailsDTO.xrayReportDetailsDTOList = xrayReportDetailsDTOList;
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return xrayreportdetailsDTO;
	}
	
	
	
	
	public List<XrayReportDetailsDTO> getDTOs(Collection recordIDs){
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		XrayReportDetailsDTO xrayreportdetailsDTO = null;
		List<XrayReportDetailsDTO> xrayreportdetailsDTOList = new ArrayList<>();
		if(recordIDs.isEmpty()){
			return xrayreportdetailsDTOList;
		}
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
            
            sql += " WHERE ID IN ( ";

			for(int i = 0;i<recordIDs.size();i++){
				if(i!=0){
					sql+=",";
				}
				sql+=((ArrayList)recordIDs).get(i);
			}
			sql+=")  order by lastModificationTime desc";
			
			printSql(sql);
			
			logger.debug("sql " + sql);
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				xrayreportdetailsDTO = new XrayReportDetailsDTO();
				get(xrayreportdetailsDTO, rs);
				System.out.println("got this DTO: " + xrayreportdetailsDTO);
				
				xrayreportdetailsDTOList.add(xrayreportdetailsDTO);

			}			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return xrayreportdetailsDTOList;
	
	}
	
	

	
	
	
	//add repository
	public List<XrayReportDetailsDTO> getAllXrayReportDetails (boolean isFirstReload)
    {
		List<XrayReportDetailsDTO> xrayreportdetailsDTOList = new ArrayList<>();

		String sql = "SELECT * FROM xray_report_details";
		sql += " WHERE ";
	

		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by xrayreportdetails.lastModificationTime desc";
		printSql(sql);
		
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;

		
		try{
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();
			rs = stmt.executeQuery(sql);
			

			while(rs.next()){
				XrayReportDetailsDTO xrayreportdetailsDTO = new XrayReportDetailsDTO();
				get(xrayreportdetailsDTO, rs);
				
				xrayreportdetailsDTOList.add(xrayreportdetailsDTO);
			}			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}

		return xrayreportdetailsDTOList;
    }

	
	public List<XrayReportDetailsDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO)
	{
		return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
	}

	
	public List<XrayReportDetailsDTO> getDTOs(Hashtable  p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
			String filter, boolean tableHasJobCat)
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		List<XrayReportDetailsDTO> xrayreportdetailsDTOList = new ArrayList<>();

		try{
			
			String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat);
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				XrayReportDetailsDTO xrayreportdetailsDTO = new XrayReportDetailsDTO();
				get(xrayreportdetailsDTO, rs);
				
				xrayreportdetailsDTOList.add(xrayreportdetailsDTO);
			}					
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return xrayreportdetailsDTOList;
	
	}
				
}
	