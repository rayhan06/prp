package xray_report;
import java.util.*; 
import util.*; 


public class XrayReportDetailsDTO extends CommonDTO
{

	public long xrayReportId = -1;
    public String nameEn = "";
    public String nameBn = "";
    public String title = "";
    public String comment = "";
	
	public List<XrayReportDetailsDTO> xrayReportDetailsDTOList = new ArrayList<>();
	
    @Override
	public String toString() {
            return "$XrayReportDetailsDTO[" +
            " iD = " + iD +
            " xrayReportId = " + xrayReportId +
            " nameEn = " + nameEn +
            " nameBn = " + nameBn +
            " title = " + title +
            " comment = " + comment +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}