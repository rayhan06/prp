package xray_report;

import java.io.IOException;
import java.io.*;
import java.text.SimpleDateFormat;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import org.apache.log4j.Logger;

import login.LoginDTO;
import permission.MenuConstants;
import role.PermissionRepository;


import sessionmanager.SessionConstants;
import ultrasono_type.Ultrasono_typeDTO;
import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;
import util.CommonRequestHandler;
import util.RecordNavigationManager4;

import java.util.*;
import javax.servlet.http.*;
import java.util.UUID;





import geolocation.GeoLocationDAO2;
import language.LC;
import language.LM;

import java.util.StringTokenizer;

import com.google.gson.Gson;

import pb.*;
import pbReport.*;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;




/**
 * Servlet implementation class Xray_reportServlet
 */
@WebServlet("/Xray_reportServlet")
@MultipartConfig
public class Xray_reportServlet extends HttpServlet 
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Xray_reportServlet.class);

    String tableName = "xray_report";

	Xray_reportDAO xray_reportDAO;
	CommonRequestHandler commonRequestHandler;
	XrayReportDetailsDAO xrayReportDetailsDAO;
    private final Gson gson = new Gson();
    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Xray_reportServlet() 
	{
        super();
    	try
    	{
			xray_reportDAO = new Xray_reportDAO(tableName);
			xrayReportDetailsDAO = new XrayReportDetailsDAO("xray_report_details");
			commonRequestHandler = new CommonRequestHandler(xray_reportDAO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }   

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		System.out.println("In doget request = " + request);
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		try
		{
			String actionType = request.getParameter("actionType");
			if(actionType.equals("getAddPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.XRAY_REPORT_ADD))
				{
					commonRequestHandler.getAddPage(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getEditPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.XRAY_REPORT_UPDATE))
				{
					getXray_report(request, response);					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}						
			}
			else if(actionType.equals("getURL"))
			{
				String URL = request.getParameter("URL");
				System.out.println("URL = " + URL);
				response.sendRedirect(URL);			
			}			
			else if(actionType.equals("search"))
			{
				System.out.println("search requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.XRAY_REPORT_SEARCH))
				{
					if(isPermanentTable)
					{
						String filter = request.getParameter("filter");
						System.out.println("filter = " + filter);
						if(filter!=null)
						{
							filter = ""; //shouldn't be directly used, rather manipulate it.
							searchXray_report(request, response, isPermanentTable, filter);
						}
						else
						{
							searchXray_report(request, response, isPermanentTable, "");
						}
					}					
					else
					{
						//searchXray_report(request, response, tempTableName, isPermanentTable);
					}
				}			
			}
			else if(actionType.equals("view"))
			{
				System.out.println("view requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.XRAY_REPORT_SEARCH))
				{
					commonRequestHandler.view(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else
			{
				request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}


	
	

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		// TODO Auto-generated method stub
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		System.out.println("doPost");
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		
		try
		{
			String actionType = request.getParameter("actionType");
			System.out.println("actionType = " + actionType);
			if(actionType.equals("add"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.XRAY_REPORT_ADD))
				{
					System.out.println("going to  addXray_report ");
					addXray_report(request, response, true, userDTO, true);
				}
				else
				{
					System.out.println("Not going to  addXray_report ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}

			else if(actionType.equals("getDTO"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.XRAY_REPORT_ADD))
				{
					getDTO(request, response);					
				}
				else
				{
					System.out.println("Not going to  addXray_report ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else if(actionType.equals("edit"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.XRAY_REPORT_UPDATE))
				{					
					addXray_report(request, response, false, userDTO, isPermanentTable);					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getSubType"))
			{
				long type = Long.parseLong(request.getParameter("type"));
				String language = request.getParameter("language");
				String options = CommonDAO.getOptionsWithWhere(language, "xray_report_details", "xray_report_id = " + type);
				options = "<option value = ''>" + LM.getText(LC.HM_SELECT, loginDTO) +  "</option>" + options;
				
				response.getWriter().write(options);
			}
			else if(actionType.equals("getComment"))
			{
				XrayReportDetailsDTO xrayReportDetailsDTO = xrayReportDetailsDAO.getDTOByID(Long.parseLong(request.getParameter("ID")));
				if(xrayReportDetailsDTO != null)
				{
					response.getWriter().write(xrayReportDetailsDTO.comment);
				}				
			}
			else if(actionType.equals("delete"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.XRAY_REPORT_UPDATE))
				{
					commonRequestHandler.delete(request, response, userDTO);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}	
			else if(actionType.equals("search"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.XRAY_REPORT_SEARCH))
				{
					searchXray_report(request, response, true, "");
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getGeo"))
			{
				System.out.println("going to geoloc ");
				request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
			}
			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}
	
	private void getDTO(HttpServletRequest request, HttpServletResponse response) 
	{
		try 
		{
			System.out.println("In getDTO");
			Xray_reportDTO xray_reportDTO = xray_reportDAO.getDTOByID(Long.parseLong(request.getParameter("ID")));
			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			
			String encoded = this.gson.toJson(xray_reportDTO);
			System.out.println("json encoded data = " + encoded);
			out.print(encoded);
			out.flush();
		}
		catch (NumberFormatException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	private void addXray_report(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, boolean isPermanentTable) throws IOException 
	{
		// TODO Auto-generated method stub
		try 
		{
			request.setAttribute("failureMessage", "");
			System.out.println("%%%% addXray_report");
			String path = getServletContext().getRealPath("/img2/");
			Xray_reportDTO xray_reportDTO;
			SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
						
			if(addFlag == true)
			{
				xray_reportDTO = new Xray_reportDTO();
			}
			else
			{
				xray_reportDTO = xray_reportDAO.getDTOByID(Long.parseLong(request.getParameter("iD")));
			}
			
			String Value = "";

			Value = request.getParameter("nameEn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("nameEn = " + Value);
			if(Value != null)
			{
				xray_reportDTO.nameEn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			

			Value = request.getParameter("comment");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("comment = " + Value);
			if(Value != null)
			{
				xray_reportDTO.comment = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			if(addFlag)
			{
				xray_reportDTO.insertedByUserId = userDTO.ID;
			}


			if(addFlag)
			{
				xray_reportDTO.insertedByOrganogramId = userDTO.organogramID;
			}


			if(addFlag)
			{
				Calendar c = Calendar.getInstance();
				c.set(Calendar.HOUR_OF_DAY, 0);
				c.set(Calendar.MINUTE, 0);
				c.set(Calendar.SECOND, 0);
				c.set(Calendar.MILLISECOND, 0);
				
				xray_reportDTO.insertionDate = c.getTimeInMillis();
			}			


			xray_reportDTO.lastModifierUser = userDTO.userName;

			
			System.out.println("Done adding  addXray_report dto = " + xray_reportDTO);
			long returnedID = -1;
			
			if(isPermanentTable == false) //add new row for validation and make the old row outdated
			{
				xray_reportDAO.setIsDeleted(xray_reportDTO.iD, CommonDTO.OUTDATED);
				returnedID = xray_reportDAO.add(xray_reportDTO);
				xray_reportDAO.setIsDeleted(returnedID, CommonDTO.WAITING_FOR_APPROVAL);
			}
			else if(addFlag == true)
			{
				returnedID = xray_reportDAO.manageWriteOperations(xray_reportDTO, SessionConstants.INSERT, -1, userDTO);
			}
			else
			{				
				returnedID = xray_reportDAO.manageWriteOperations(xray_reportDTO, SessionConstants.UPDATE, -1, userDTO);											
			}
			
			
			
			
			
			List<XrayReportDetailsDTO> xrayReportDetailsDTOList = createXrayReportDetailsDTOListByRequest(request);			
			if(addFlag == true || isPermanentTable == false) //add or validate
			{
				if(xrayReportDetailsDTOList != null)
				{				
					for(XrayReportDetailsDTO xrayReportDetailsDTO: xrayReportDetailsDTOList)
					{
						xrayReportDetailsDTO.xrayReportId = xray_reportDTO.iD; 
						xrayReportDetailsDAO.add(xrayReportDetailsDTO);
					}
				}
			
			}
			else
			{
				List<Long> childIdsFromRequest = xrayReportDetailsDAO.getChildIdsFromRequest(request, "xrayReportDetails");
				//delete the removed children
				xrayReportDetailsDAO.deleteChildrenNotInList("xray_report", "xray_report_details", xray_reportDTO.iD, childIdsFromRequest);
				List<Long> childIDsInDatabase = xrayReportDetailsDAO.getChilIds("xray_report", "xray_report_details", xray_reportDTO.iD);
				
				
				if(childIdsFromRequest != null)
				{
					for(int i = 0; i < childIdsFromRequest.size(); i ++)
					{
						Long childIDFromRequest = childIdsFromRequest.get(i);
						if(childIDsInDatabase.contains(childIDFromRequest))
						{
							XrayReportDetailsDTO xrayReportDetailsDTO =  createXrayReportDetailsDTOByRequestAndIndex(request, false, i);
							xrayReportDetailsDTO.xrayReportId = xray_reportDTO.iD; 
							xrayReportDetailsDAO.update(xrayReportDetailsDTO);
						}
						else
						{
							XrayReportDetailsDTO xrayReportDetailsDTO =  createXrayReportDetailsDTOByRequestAndIndex(request, true, i);
							xrayReportDetailsDTO.xrayReportId = xray_reportDTO.iD; 
							xrayReportDetailsDAO.add(xrayReportDetailsDTO);
						}
					}
				}
				else
				{
					xrayReportDetailsDAO.deleteXrayReportDetailsByXrayReportID(xray_reportDTO.iD);
				}
				
			}
			
			
			
			
			
			if(isPermanentTable)
			{
				String inPlaceSubmit = request.getParameter("inplacesubmit");
				
				if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
				{
					getXray_report(request, response, returnedID);
				}
				else
				{
					response.sendRedirect("Xray_reportServlet?actionType=search");
				}
			}
			else
			{
				commonRequestHandler.validate(xray_reportDAO.getDTOByID(returnedID), request, response, userDTO);
			}
					
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	



	private List<XrayReportDetailsDTO> createXrayReportDetailsDTOListByRequest(HttpServletRequest request) throws Exception{ 
		List<XrayReportDetailsDTO> xrayReportDetailsDTOList = new ArrayList<XrayReportDetailsDTO>();
		if(request.getParameterValues("xrayReportDetails.iD") != null) 
		{
			int xrayReportDetailsItemNo = request.getParameterValues("xrayReportDetails.iD").length;
			
			
			for(int index=0;index<xrayReportDetailsItemNo;index++){
				XrayReportDetailsDTO xrayReportDetailsDTO = createXrayReportDetailsDTOByRequestAndIndex(request,true,index);
				xrayReportDetailsDTOList.add(xrayReportDetailsDTO);
			}
			
			return xrayReportDetailsDTOList;
		}
		return null;
	}
	
	
	private XrayReportDetailsDTO createXrayReportDetailsDTOByRequestAndIndex(HttpServletRequest request, boolean addFlag,int index) throws Exception{
	
		XrayReportDetailsDTO xrayReportDetailsDTO;
		if(addFlag == true )
		{
			xrayReportDetailsDTO = new XrayReportDetailsDTO();
		}
		else
		{
			xrayReportDetailsDTO = xrayReportDetailsDAO.getDTOByID(Long.parseLong(request.getParameterValues("xrayReportDetails.iD")[index]));
		}
		String path = getServletContext().getRealPath("/img2/");
		SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
		
		
				
		String Value = "";
		Value = request.getParameterValues("xrayReportDetails.xrayReportId")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		xrayReportDetailsDTO.xrayReportId = Long.parseLong(Value);
		Value = request.getParameterValues("xrayReportDetails.nameEn")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		xrayReportDetailsDTO.nameEn = (Value);
		
		Value = request.getParameterValues("xrayReportDetails.comment")[index];
		xrayReportDetailsDTO.comment = (Value);
		return xrayReportDetailsDTO;
	
	}
	
	
	

	private void getXray_report(HttpServletRequest request, HttpServletResponse response, long id) throws ServletException, IOException
	{
		System.out.println("in getXray_report");
		Xray_reportDTO xray_reportDTO = null;
		try 
		{
			xray_reportDTO = xray_reportDAO.getDTOByID(id);
			request.setAttribute("ID", xray_reportDTO.iD);
			request.setAttribute("xray_reportDTO",xray_reportDTO);
			request.setAttribute("xray_reportDAO",xray_reportDAO);
			
			String URL= "";
			
			String inPlaceEdit = request.getParameter("inplaceedit");
			String inPlaceSubmit = request.getParameter("inplacesubmit");
			String getBodyOnly = request.getParameter("getBodyOnly");
			
			if(inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase(""))
			{
				URL = "xray_report/xray_reportInPlaceEdit.jsp";	
				request.setAttribute("inplaceedit","");				
			}
			else if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
			{
				URL = "xray_report/xray_reportSearchRow.jsp";
				request.setAttribute("inplacesubmit","");					
			}
			else
			{
				if(getBodyOnly  != null && !getBodyOnly.equalsIgnoreCase(""))
				{
					URL = "xray_report/xray_reportEditBody.jsp?actionType=edit";
				}
				else
				{
					URL = "xray_report/xray_reportEdit.jsp?actionType=edit";
				}				
			}
			
			RequestDispatcher rd = request.getRequestDispatcher(URL);
			rd.forward(request, response);
		}
		catch (NumberFormatException e) 
		{
			e.printStackTrace();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	
	private void getXray_report(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		getXray_report(request, response, Long.parseLong(request.getParameter("ID")));	
	}
	
	private void searchXray_report(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException
	{
		System.out.println("in  searchXray_report 1");
		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String ajax = request.getParameter("ajax");
		boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);
		
        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
			SessionConstants.NAV_XRAY_REPORT,
			request,
			xray_reportDAO,
			SessionConstants.VIEW_XRAY_REPORT,
			SessionConstants.SEARCH_XRAY_REPORT,
			tableName,
			isPermanent,
			userDTO,
			filter,
			true);
        try
        {
			System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        }
        catch(Exception e)
        {
			System.out.println("failed to dojob" + e);
        }

		request.setAttribute("xray_reportDAO",xray_reportDAO);
        RequestDispatcher rd;
        if(!isPermanent)
        {
        	if(hasAjax == false)
	        {
	        	System.out.println("Going to xray_report/xray_reportApproval.jsp");
	        	rd = request.getRequestDispatcher("xray_report/xray_reportApproval.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to xray_report/xray_reportApprovalForm.jsp");
	        	rd = request.getRequestDispatcher("xray_report/xray_reportApprovalForm.jsp");
	        }
        }
        else
        {
	        if(hasAjax == false)
	        {
	        	System.out.println("Going to xray_report/xray_reportSearch.jsp");
	        	rd = request.getRequestDispatcher("xray_report/xray_reportSearch.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to xray_report/xray_reportSearchForm.jsp");
	        	rd = request.getRequestDispatcher("xray_report/xray_reportSearchForm.jsp");
	        }
        }
		rd.forward(request, response);
	}
	
}

