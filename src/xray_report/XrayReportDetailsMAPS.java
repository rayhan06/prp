package xray_report;
import java.util.*; 
import util.*;


public class XrayReportDetailsMAPS extends CommonMaps
{	
	public XrayReportDetailsMAPS(String tableName)
	{
		

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("xrayReportId".toLowerCase(), "xrayReportId".toLowerCase());
		java_DTO_map.put("nameEn".toLowerCase(), "nameEn".toLowerCase());
		java_DTO_map.put("nameBn".toLowerCase(), "nameBn".toLowerCase());
		java_DTO_map.put("title".toLowerCase(), "title".toLowerCase());
		java_DTO_map.put("comment".toLowerCase(), "comment".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("xray_report_id".toLowerCase(), "xrayReportId".toLowerCase());
		java_SQL_map.put("name_en".toLowerCase(), "nameEn".toLowerCase());
		java_SQL_map.put("name_bn".toLowerCase(), "nameBn".toLowerCase());
		java_SQL_map.put("title".toLowerCase(), "title".toLowerCase());
		java_SQL_map.put("comment".toLowerCase(), "comment".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Xray Report Id".toLowerCase(), "xrayReportId".toLowerCase());
		java_Text_map.put("English Name".toLowerCase(), "nameEn".toLowerCase());
		java_Text_map.put("Bangla Name".toLowerCase(), "nameBn".toLowerCase());
		java_Text_map.put("Title".toLowerCase(), "title".toLowerCase());
		java_Text_map.put("Comment".toLowerCase(), "comment".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}