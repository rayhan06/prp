/*
package office_origins;

import java.io.IOException;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import org.apache.log4j.Logger;

import login.LoginDTO;
import permission.MenuConstants;
import role.PermissionRepository;


import sessionmanager.SessionConstants;

import user.UserDTO;
import user.UserRepository;

import util.RecordNavigationManager;

import javax.servlet.http.*;
import java.util.UUID;

*/
/**
 * Servlet implementation class Office_originsServlet
 *//*

@WebServlet("/Office_originsServlet")
@MultipartConfig
public class Office_originsServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Office_originsServlet.class);

    */
/**
     * @see HttpServlet#HttpServlet()
     *//*

    public Office_originsServlet() {
        super();
    }

    */
/**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     *//*

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("In doget request = " + request);
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getInstance().getUserDtoByUserId(loginDTO.userID);
        try {
            String actionType = request.getParameter("actionType");
            if (actionType.equals("getAddPage")) {
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.OFFICE_ORIGINS_ADD)) {
                    getAddPage(request, response);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else if (actionType.equals("getEditPage")) {
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.OFFICE_ORIGINS_UPDATE)) {
                    getOffice_origins(request, response);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else if (actionType.equals("getURL")) {
                String URL = request.getParameter("URL");
                System.out.println("URL = " + URL);
                response.sendRedirect(URL);
            } else if (actionType.equals("search")) {
                System.out.println("search requested");
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.OFFICE_ORIGINS_SEARCH)) {
                    searchOffice_origins(request, response);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.debug(ex);
        }
    }

    private void getAddPage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("ID", -1L);
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("office_origins/office_originsEdit.jsp");
        requestDispatcher.forward(request, response);
    }

    private String getFileName(final Part part) {
        final String partHeader = part.getHeader("content-disposition");
        System.out.println("Part Header = {0}" + partHeader);
        for (String content : part.getHeader("content-disposition").split(";")) {
            if (content.trim().startsWith("filename")) {
                return content.substring(
                        content.indexOf('=') + 1).trim().replace("\"", "");
            }
        }
        return null;
    }


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getInstance().getUserDtoByUserId(loginDTO.userID);
        System.out.println("doPost");

        try {
            String actionType = request.getParameter("actionType");
            System.out.println("actionType = " + actionType);
            if (actionType.equals("add")) {

                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.OFFICE_ORIGINS_ADD)) {
                    System.out.println("going to  addOffice_origins ");
                    addOffice_origins(request, response, true);
                } else {
                    System.out.println("Not going to  addOffice_origins ");
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }

            } else if (actionType.equals("edit")) {

                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.OFFICE_ORIGINS_UPDATE)) {
                    addOffice_origins(request, response, false);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else if (actionType.equals("delete")) {
                deleteOffice_origins(request, response);
            } else if (actionType.equals("search")) {
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.OFFICE_ORIGINS_SEARCH)) {
                    searchOffice_origins(request, response);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else if (actionType.equals("getGeo")) {
                System.out.println("going to geoloc ");
                request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            logger.debug(ex);
        }
    }

    private void addOffice_origins(HttpServletRequest request, HttpServletResponse response, Boolean addFlag) throws IOException {
        // TODO Auto-generated method stub
        try {
            request.setAttribute("failureMessage", "");
            System.out.println("%%%% addOffice_origins");
            String path = getServletContext().getRealPath("/img2/");
            Office_originsDAO office_originsDAO = new Office_originsDAO();
            Office_originsDTO office_originsDTO;
            String FileNamePrefix;
            if (addFlag == true) {
                office_originsDTO = new Office_originsDTO();
                FileNamePrefix = UUID.randomUUID().toString().substring(0, 10);
            } else {
                office_originsDTO = office_originsDAO.getOffice_originsDTOByID(Long.parseLong(request.getParameter("identity")));
                FileNamePrefix = request.getParameter("identity");
            }

            String Value = "";
            Value = request.getParameter("officeMinistryType");
            System.out.println("officeMinistryType = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                office_originsDTO.officeMinistryType = Integer.parseInt(Value);
            } else {
                System.out.println("FieldName has a null value, not updating" + " = " + Value);
            }
            Value = request.getParameter("officeLayerType");
            System.out.println("officeLayerType = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                office_originsDTO.officeLayerType = Integer.parseInt(Value);
            } else {
                System.out.println("FieldName has a null value, not updating" + " = " + Value);
            }
            Value = request.getParameter("officeNameEng");
            System.out.println("officeNameEng = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                office_originsDTO.officeNameEng = (Value);
            } else {
                System.out.println("FieldName has a null value, not updating" + " = " + Value);
            }
            Value = request.getParameter("officeNameBng");
            System.out.println("officeNameBng = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                office_originsDTO.officeNameBng = (Value);
            } else {
                System.out.println("FieldName has a null value, not updating" + " = " + Value);
            }
            Value = request.getParameter("parentOfficeType");
            System.out.println("parentOfficeType = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                office_originsDTO.parentOfficeType = Integer.parseInt(Value);
                if (office_originsDTO.parentOfficeType < 0) office_originsDTO.parentOfficeType = 0;
            } else {
                System.out.println("FieldName has a null value, not updating" + " = " + Value);
            }
            Value = request.getParameter("officeLevel");
            System.out.println("officeLevel = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                office_originsDTO.officeLevel = Integer.parseInt(Value);
            } else {
                System.out.println("FieldName has a null value, not updating" + " = " + Value);
            }
            Value = request.getParameter("officeSequence");
            System.out.println("officeSequence = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                office_originsDTO.officeSequence = Integer.parseInt(Value);
            } else {
                System.out.println("FieldName has a null value, not updating" + " = " + Value);
            }
            Value = request.getParameter("status");
            System.out.println("status = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                office_originsDTO.status = Boolean.parseBoolean(Value);
            } else {
                System.out.println("FieldName has a null value, not updating" + " = " + Value);
            }
            Value = request.getParameter("createdBy");
            System.out.println("createdBy = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                office_originsDTO.createdBy = Integer.parseInt(Value);
            } else {
                System.out.println("FieldName has a null value, not updating" + " = " + Value);
            }
            Value = request.getParameter("modifiedBy");
            System.out.println("modifiedBy = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                office_originsDTO.modifiedBy = Integer.parseInt(Value);
            } else {
                System.out.println("FieldName has a null value, not updating" + " = " + Value);
            }
            Value = request.getParameter("created");
            System.out.println("created = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                office_originsDTO.created = Long.parseLong(Value);
            } else {
                System.out.println("FieldName has a null value, not updating" + " = " + Value);
            }
            Value = request.getParameter("modified");
            System.out.println("modified = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                office_originsDTO.modified = Long.parseLong(Value);
            } else {
                System.out.println("FieldName has a null value, not updating" + " = " + Value);
            }
            Value = request.getParameter("isDeleted");
            System.out.println("isDeleted = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                office_originsDTO.isDeleted = Boolean.parseBoolean(Value);
            } else {
                System.out.println("FieldName has a null value, not updating" + " = " + Value);
            }

            System.out.println("Done adding  addOffice_origins dto = " + office_originsDTO);

            if (addFlag == true) {
                office_originsDAO.addOffice_origins(office_originsDTO);
            } else {
                office_originsDAO.updateOffice_origins(office_originsDTO);

            }


            String inPlaceSubmit = (String) request.getParameter("inplacesubmit");

            if (inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase("")) {
                getOffice_origins(request, response);
            } else {
                response.sendRedirect("Office_originsServlet?actionType=search&success=true");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void deleteOffice_origins(HttpServletRequest request, HttpServletResponse response) throws IOException {
        int count = 0;
        OfficeOriginService officeOriginService = new OfficeOriginService();
        try {
            String[] IDsToDelete = request.getParameterValues("ID");
            for (int i = 0; i < IDsToDelete.length; i++) {
                long id = Long.parseLong(IDsToDelete[i]);
                if (officeOriginService.canDelete(id)) {
                    System.out.println("###DELETING " + IDsToDelete[i]);
                    new Office_originsDAO().deleteOffice_originsByID(id);
                } else {
                    count++;
                }
            }
        } catch (Exception ex) {
            logger.debug(ex);
        }
        response.sendRedirect("Office_originsServlet?actionType=search&delete=" + (count == 0));
    }

    private void getOffice_origins(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("in getOffice_origins");
        Office_originsDTO office_originsDTO = null;
        try {
            office_originsDTO = new Office_originsDAO().getOffice_originsDTOByID(Long.parseLong(request.getParameter("ID")));
            request.setAttribute("ID", office_originsDTO.iD);
            request.setAttribute("office_originsDTO", office_originsDTO);

            String URL = "";

            String inPlaceEdit = (String) request.getParameter("inplaceedit");
            String inPlaceSubmit = (String) request.getParameter("inplacesubmit");
            String getBodyOnly = (String) request.getParameter("getBodyOnly");

            if (inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase("")) {
                URL = "office_origins/office_originsInPlaceEdit.jsp";
                request.setAttribute("inplaceedit", "");
            } else if (inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase("")) {
                URL = "office_origins/office_originsSearchRow.jsp";
                request.setAttribute("inplacesubmit", "");
            } else {
                if (getBodyOnly != null && !getBodyOnly.equalsIgnoreCase("")) {
                    URL = "office_origins/office_originsEditBody.jsp?actionType=edit";
                } else {
                    URL = "office_origins/office_originsEdit.jsp?actionType=edit";
                }
            }

            RequestDispatcher rd = request.getRequestDispatcher(URL);
            rd.forward(request, response);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void searchOffice_origins(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("in  searchOffice_origins 1");
        Office_originsDAO office_originsDAO = new Office_originsDAO();
        LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
        String ajax = (String) request.getParameter("ajax");
        boolean hasAjax = false;
        if (ajax != null && !ajax.equalsIgnoreCase("")) {
            hasAjax = true;
        }
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);

        RecordNavigationManager rnManager = new RecordNavigationManager(SessionConstants.NAV_OFFICE_ORIGINS, request, office_originsDAO, SessionConstants.VIEW_OFFICE_ORIGINS, SessionConstants.SEARCH_OFFICE_ORIGINS);
        try {
            System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        } catch (Exception e) {
            System.out.println("failed to dojob" + e);
        }

        RequestDispatcher rd;
        if (hasAjax == false) {
            System.out.println("Going to office_origins/office_originsSearch.jsp");
            rd = request.getRequestDispatcher("office_origins/office_originsSearch.jsp");
        } else {
            System.out.println("Going to office_origins/office_originsSearchForm.jsp");
            rd = request.getRequestDispatcher("office_origins/office_originsSearchForm.jsp");
        }
        rd.forward(request, response);
    }

}

*/
