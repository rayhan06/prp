package office_origins;
import java.util.*; 


public class Office_originsDTO {

	public long iD = 0;
	public int officeMinistryType = 0;
	public int officeLayerType = 0;
    public String officeNameEng = "";
    public String officeNameBng = "";
	public int parentOfficeType = 0;
	public int officeLevel = 0;
	public int officeSequence = 0;
	public boolean status = false;
	public int createdBy = 0;
	public int modifiedBy = 0;
	public long created = 0;
	public long modified = 0;
	public boolean isDeleted = false;
	
	
    @Override
	public String toString() {
            return "$Office_originsDTO[" +
            " id = " + iD +
            " officeMinistryType = " + officeMinistryType +
            " officeLayerType = " + officeLayerType +
            " officeNameEng = " + officeNameEng +
            " officeNameBng = " + officeNameBng +
            " parentOfficeType = " + parentOfficeType +
            " officeLevel = " + officeLevel +
            " officeSequence = " + officeSequence +
            " status = " + status +
            " createdBy = " + createdBy +
            " modifiedBy = " + modifiedBy +
            " created = " + created +
            " modified = " + modified +
            " isDeleted = " + isDeleted +
            "]";
    }

}