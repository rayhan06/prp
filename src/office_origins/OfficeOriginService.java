package office_origins;

import office_origin_units.Office_origin_unitsDTO;
import office_origin_units.Office_origin_unitsRepository;

import java.util.List;

public class OfficeOriginService {

    public boolean canDelete(long id) {
        Office_originsDTO dto = getDtoById(id);
        if (dto == null) return false;

        return hasUnit(id);
    }

    private boolean hasUnit(long officeOriginId) {
        List<Office_origin_unitsDTO> dataList = Office_origin_unitsRepository.getInstance().getOffice_origin_unitsList();
        for (Office_origin_unitsDTO s : dataList) {
            if (s.officeOriginId == officeOriginId && !s.isDeleted && s.status) {
                return false;
            }
        }
        return true;
    }

    private Office_originsDTO getDtoById(long id) {
        List<Office_originsDTO> dataList = Office_originsRepository.getInstance().getOffice_originsList();
        for (Office_originsDTO s : dataList) {
            if (s.iD == id) return s;
        }
        return null;
    }
}
