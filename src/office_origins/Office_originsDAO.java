package office_origins;

import dbm.DBMR;
import dbm.DBMW;
import login.LoginDTO;
import org.apache.log4j.Logger;
import repository.RepositoryManager;
import util.NavigationService;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;


public class Office_originsDAO implements NavigationService {

    Logger logger = Logger.getLogger(getClass());


    private void printSql(String sql) {
        System.out.println("sql: " + sql);
    }


    private void recordUpdateTime(Connection connection, long lastModificationTime) {
        recordUpdateTime(connection,"office_origins",lastModificationTime);
    }


    public long addOffice_origins(Office_originsDTO office_originsDTO) throws Exception {

        Connection connection = null;
        PreparedStatement ps = null;

        long lastModificationTime = System.currentTimeMillis();

        try {
            connection = DBMW.getInstance().getConnection();

            if (connection == null) {
                System.out.println("nullconn");
            }

            office_originsDTO.iD = DBMW.getInstance().getNextSequenceId("office_origins");

            String sql = "INSERT INTO office_origins";

            sql += " (";
            sql += "ID";
            sql += ", ";
            sql += "office_ministry_id";
            sql += ", ";
            sql += "office_layer_id";
            sql += ", ";
            sql += "office_name_eng";
            sql += ", ";
            sql += "office_name_bng";
            sql += ", ";
            //if (office_originsDTO.parentOfficeType > 0) {
            sql += "parent_office_id";
            sql += ", ";
            //}
            sql += "office_level";
            sql += ", ";
            sql += "office_sequence";
            sql += ", ";
            sql += "status";
            sql += ", ";
            sql += "created_by";
            sql += ", ";
            sql += "modified_by";
            sql += ", ";
            sql += "created";
            sql += ", ";
            sql += "modified";
            sql += ", ";
            sql += "isDeleted";
            sql += ", lastModificationTime)";


            sql += " VALUES(";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            //if (office_originsDTO.parentOfficeType > 0) {
            sql += "?";
            sql += ", ";
            //}
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ?)";

            printSql(sql);

            ps = connection.prepareStatement(sql);

            int index = 1;

            ps.setObject(index++, office_originsDTO.iD);
            ps.setObject(index++, office_originsDTO.officeMinistryType);
            ps.setObject(index++, office_originsDTO.officeLayerType);
            ps.setObject(index++, office_originsDTO.officeNameEng);
            ps.setObject(index++, office_originsDTO.officeNameBng);
            ps.setObject(index++, office_originsDTO.parentOfficeType > 0 ? office_originsDTO.parentOfficeType : 0);
            ps.setObject(index++, office_originsDTO.officeLevel);
            ps.setObject(index++, office_originsDTO.officeSequence);
            ps.setObject(index++, office_originsDTO.status);
            ps.setObject(index++, office_originsDTO.createdBy);
            ps.setObject(index++, office_originsDTO.modifiedBy);
            ps.setObject(index++, office_originsDTO.created);
            ps.setObject(index++, office_originsDTO.modified);
            ps.setObject(index++, office_originsDTO.isDeleted);
            ps.setObject(index++, lastModificationTime);

            System.out.println(ps);
            ps.execute();

            recordUpdateTime(connection, lastModificationTime);
            Office_originsRepository.getInstance().addNewDto(office_originsDTO);

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (connection != null) {
                    DBMW.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
        return office_originsDTO.iD;
    }


    //need another getter for repository
    public Office_originsDTO getOffice_originsDTOByID(long ID) throws Exception {
        Connection connection = null;
        ResultSet rs = null;
        Statement stmt = null;
        Office_originsDTO office_originsDTO = null;
        try {

            String sql = "SELECT * ";

            sql += " FROM office_origins";

            sql += " WHERE ID=" + ID;

            printSql(sql);

            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();


            rs = stmt.executeQuery(sql);

            if (rs.next()) {
                office_originsDTO = new Office_originsDTO();

                office_originsDTO.iD = rs.getLong("ID");
                office_originsDTO.officeMinistryType = rs.getInt("office_ministry_id");
                office_originsDTO.officeLayerType = rs.getInt("office_layer_id");
                office_originsDTO.officeNameEng = rs.getString("office_name_eng");
                office_originsDTO.officeNameBng = rs.getString("office_name_bng");
                office_originsDTO.parentOfficeType = rs.getInt("parent_office_id");
                office_originsDTO.officeLevel = rs.getInt("office_level");
                office_originsDTO.officeSequence = rs.getInt("office_sequence");
                office_originsDTO.status = rs.getBoolean("status");
                office_originsDTO.createdBy = rs.getInt("created_by");
                office_originsDTO.modifiedBy = rs.getInt("modified_by");
                office_originsDTO.created = rs.getLong("created");
                office_originsDTO.modified = rs.getLong("modified");
                office_originsDTO.isDeleted = rs.getBoolean("isDeleted");

            }


        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }

            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
        return office_originsDTO;
    }

    public long updateOffice_origins(Office_originsDTO office_originsDTO) throws Exception {

        Connection connection = null;
        PreparedStatement ps = null;

        long lastModificationTime = System.currentTimeMillis();
        try {
            connection = DBMW.getInstance().getConnection();

            String sql = "UPDATE office_origins";

            sql += " SET ";
            sql += "office_ministry_id=?";
            sql += ", ";
            sql += "office_layer_id=?";
            sql += ", ";
            sql += "office_name_eng=?";
            sql += ", ";
            sql += "office_name_bng=?";
            sql += ", ";
            sql += "parent_office_id=?";
            sql += ", ";
            sql += "office_level=?";
            sql += ", ";
            sql += "office_sequence=?";
            sql += ", ";
            sql += "status=?";
            sql += ", ";
            sql += "created_by=?";
            sql += ", ";
            sql += "modified_by=?";
            sql += ", ";
            sql += "created=?";
            sql += ", ";
            sql += "modified=?";
            sql += ", ";
            sql += "isDeleted=?";
            sql += ", lastModificationTime = " + lastModificationTime + "";
            sql += " WHERE ID = " + office_originsDTO.iD;

            printSql(sql);

            ps = connection.prepareStatement(sql);


            int index = 1;

            ps.setObject(index++, office_originsDTO.officeMinistryType);
            ps.setObject(index++, office_originsDTO.officeLayerType);
            ps.setObject(index++, office_originsDTO.officeNameEng);
            ps.setObject(index++, office_originsDTO.officeNameBng);
            ps.setObject(index++, office_originsDTO.parentOfficeType);
            ps.setObject(index++, office_originsDTO.officeLevel);
            ps.setObject(index++, office_originsDTO.officeSequence);
            ps.setObject(index++, office_originsDTO.status);
            ps.setObject(index++, office_originsDTO.createdBy);
            ps.setObject(index++, office_originsDTO.modifiedBy);
            ps.setObject(index++, office_originsDTO.created);
            ps.setObject(index++, office_originsDTO.modified);
            ps.setObject(index++, office_originsDTO.isDeleted);
            System.out.println(ps);
            ps.executeUpdate();


            recordUpdateTime(connection,lastModificationTime);


        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (connection != null) {
                    DBMW.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
        return office_originsDTO.iD;
    }

    public void deleteOffice_originsByID(long ID) throws Exception {
        Connection connection = null;
        Statement stmt = null;
        PreparedStatement ps = null;

        long lastModificationTime = System.currentTimeMillis();
        try {
            String sql = "UPDATE office_origins";

            sql += " SET isDeleted=1,lastModificationTime=" + lastModificationTime + " WHERE ID = " + ID;

            printSql(sql);

            connection = DBMW.getInstance().getConnection();
            stmt = connection.createStatement();
            stmt.execute(sql);


            recordUpdateTime(connection, lastModificationTime);


        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }

            try {
                if (connection != null) {
                    DBMW.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
    }


    public List<Office_originsDTO> getDTOs(Collection recordIDs, LoginDTO loginDTO) {
        Connection connection = null;
        ResultSet rs = null;
        Statement stmt = null;
        Office_originsDTO office_originsDTO = null;
        List<Office_originsDTO> office_originsDTOList = new ArrayList<>();
        if (recordIDs.isEmpty()) {
            return office_originsDTOList;
        }
        try {

            String sql = "SELECT * ";

            sql += " FROM office_origins";

            sql += " WHERE ID IN ( ";

            for (int i = 0; i < recordIDs.size(); i++) {
                if (i != 0) {
                    sql += ",";
                }
                sql += ((ArrayList) recordIDs).get(i);
            }
            sql += ")  order by ID desc";

            printSql(sql);

            logger.debug("sql " + sql);
            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();


            rs = stmt.executeQuery(sql);

            while (rs.next()) {
                office_originsDTO = new Office_originsDTO();
                office_originsDTO.iD = rs.getLong("ID");
                office_originsDTO.officeMinistryType = rs.getInt("office_ministry_id");
                office_originsDTO.officeLayerType = rs.getInt("office_layer_id");
                office_originsDTO.officeNameEng = rs.getString("office_name_eng");
                office_originsDTO.officeNameBng = rs.getString("office_name_bng");
                office_originsDTO.parentOfficeType = rs.getInt("parent_office_id");
                office_originsDTO.officeLevel = rs.getInt("office_level");
                office_originsDTO.officeSequence = rs.getInt("office_sequence");
                office_originsDTO.status = rs.getBoolean("status");
                office_originsDTO.createdBy = rs.getInt("created_by");
                office_originsDTO.modifiedBy = rs.getInt("modified_by");
                office_originsDTO.created = rs.getLong("created");
                office_originsDTO.modified = rs.getLong("modified");
                office_originsDTO.isDeleted = rs.getBoolean("isDeleted");
                System.out.println("got this DTO: " + office_originsDTO);

                office_originsDTOList.add(office_originsDTO);

            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }

            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
        return office_originsDTOList;

    }


    public Collection getIDs(LoginDTO loginDTO) {
        Collection data = new ArrayList();
        Connection connection = null;
        Statement stmt = null;
        ResultSet resultSet = null;

        String sql = "SELECT ID FROM office_origins";

        sql += " WHERE isDeleted = 0  order by ID desc ";

        printSql(sql);

        try {
            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();

            for (resultSet = stmt.executeQuery(sql); resultSet.next(); data.add(resultSet.getString("ID"))) ;

            resultSet.close();
        } catch (Exception e) {
            logger.fatal("DAO " + e, e);
        } finally {
            try {
                if (resultSet != null && !resultSet.isClosed()) {
                    resultSet.close();
                }
            } catch (Exception ex) {

            }
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception e) {
                logger.fatal("DAO finally :" + e);
            }
        }
        return data;
    }

    //add repository
    public List<Office_originsDTO> getAllOffice_origins(boolean isFirstReload) {
        List<Office_originsDTO> office_originsDTOList = new ArrayList<>();

        String sql = "SELECT * FROM office_origins";
        sql += " WHERE ";


        if (isFirstReload) {
            sql += " isDeleted =  0";
        }
        if (!isFirstReload) {
            sql += " lastModificationTime >= " + RepositoryManager.lastModifyTime;
        }
        printSql(sql);

        Connection connection = null;
        Statement stmt = null;
        ResultSet rs = null;


        try {
            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();
            rs = stmt.executeQuery(sql);


            while (rs.next()) {
                Office_originsDTO office_originsDTO = new Office_originsDTO();
                office_originsDTO.iD = rs.getLong("ID");
                office_originsDTO.officeMinistryType = rs.getInt("office_ministry_id");
                office_originsDTO.officeLayerType = rs.getInt("office_layer_id");
                office_originsDTO.officeNameEng = rs.getString("office_name_eng");
                office_originsDTO.officeNameBng = rs.getString("office_name_bng");
                office_originsDTO.parentOfficeType = rs.getInt("parent_office_id");
                office_originsDTO.officeLevel = rs.getInt("office_level");
                office_originsDTO.officeSequence = rs.getInt("office_sequence");
                office_originsDTO.status = rs.getBoolean("status");
                office_originsDTO.createdBy = rs.getInt("created_by");
                office_originsDTO.modifiedBy = rs.getInt("modified_by");
                office_originsDTO.created = rs.getLong("created");
                office_originsDTO.modified = rs.getLong("modified");
                office_originsDTO.isDeleted = rs.getBoolean("isDeleted");
                int i = 0;
                long primaryKey = office_originsDTO.iD;
                while (i < office_originsDTOList.size() && office_originsDTOList.get(i).iD < primaryKey) {
                    i++;
                }
                office_originsDTOList.add(i, office_originsDTO);
                //office_originsDTOList.add(office_originsDTO);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }

        return office_originsDTOList;
    }


    public Collection getIDsWithSearchCriteria(Hashtable p_searchCriteria, LoginDTO loginDTO) throws Exception {
        System.out.println("table: " + p_searchCriteria);
        List<Long> idList = new ArrayList<>();
        Connection connection = null;
        PreparedStatement ps = null;

        try {

            String sql = "SELECT distinct office_origins.ID as ID FROM office_origins ";
            sql += " left  join office_ministries on office_origins.office_ministry_id = office_ministries.ID ";
            sql += " left  join office_layers on office_origins.office_layer_id = office_layers.ID ";
            sql += " left join office_origins as oo2  on office_origins.parent_office_id = oo2 .ID ";


            Enumeration names = p_searchCriteria.keys();
            String str, value;

            String AnyfieldSql = "(";

            if (p_searchCriteria.get("AnyField") != null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase("")) {
                int i = 0;
                Iterator it = Office_originsMAPS.GetInstance().java_anyfield_search_map.entrySet().iterator();
                while (it.hasNext()) {
                    if (i > 0) {
                        AnyfieldSql += " OR  ";
                    }
                    Map.Entry pair = (Map.Entry) it.next();
                    AnyfieldSql += pair.getKey() + " like '%" + p_searchCriteria.get("AnyField").toString() + "%'";
                    i++;
                }
            }
            AnyfieldSql += ")";
            System.out.println("AnyfieldSql = " + AnyfieldSql);

            String AllFieldSql = "(";
            int i = 0;
            while (names.hasMoreElements()) {
                str = (String) names.nextElement();
                value = (String) p_searchCriteria.get(str);
                System.out.println(str + ": " + value);
                if (Office_originsMAPS.GetInstance().java_allfield_type_map.get(str.toLowerCase()) != null && !Office_originsMAPS.GetInstance().java_allfield_type_map.get(str.toLowerCase()).equalsIgnoreCase("")
                        && !str.equalsIgnoreCase("AnyField")
                        && value != null && !value.equalsIgnoreCase("")) {
                    if (p_searchCriteria.get(str).equals("any")) {
                        continue;
                    }

                    if (i > 0) {
                        AllFieldSql += " AND  ";
                    }
                    if (Office_originsMAPS.GetInstance().java_allfield_type_map.get(str.toLowerCase()).equals("String")) {
                        AllFieldSql += "office_origins." + str.toLowerCase() + " like '%" + p_searchCriteria.get(str) + "%'";
                    } else {
                        AllFieldSql += "office_origins." + str.toLowerCase() + " = '" + p_searchCriteria.get(str) + "'";
                    }
                    i++;
                }
            }

            AllFieldSql += ")";

            System.out.println("AllFieldSql = " + AllFieldSql);


            sql += " WHERE ";
            sql += " office_origins.isDeleted = false";


            if (!AnyfieldSql.equals("()")) {
                sql += " AND " + AnyfieldSql;

            }
            if (!AllFieldSql.equals("()")) {
                sql += " AND " + AllFieldSql;
            }

            sql += " order by office_origins.ID desc ";

            printSql(sql);

            connection = DBMR.getInstance().getConnection();
            ps = connection.prepareStatement(sql);

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                idList.add(rs.getLong("ID"));
            }


        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
        return idList;
    }
}
	