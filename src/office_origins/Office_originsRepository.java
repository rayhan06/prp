package office_origins;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;

public class Office_originsRepository implements Repository {
    Office_originsDAO office_originsDAO = new Office_originsDAO();

    static Logger logger = Logger.getLogger(Office_originsRepository.class);
    Map<Long, Office_originsDTO> mapOfOffice_originsDTOToiD;
    Map<Integer, Set<Office_originsDTO>> mapOfOffice_originsDTOToofficeMinistryType;
    Map<Integer, Set<Office_originsDTO>> mapOfOffice_originsDTOToofficeLayerType;
    Map<String, Set<Office_originsDTO>> mapOfOffice_originsDTOToofficeNameEng;
    Map<String, Set<Office_originsDTO>> mapOfOffice_originsDTOToofficeNameBng;
    Map<Integer, Set<Office_originsDTO>> mapOfOffice_originsDTOToparentOfficeType;
    Map<Integer, Set<Office_originsDTO>> mapOfOffice_originsDTOToofficeLevel;
    Map<Integer, Set<Office_originsDTO>> mapOfOffice_originsDTOToofficeSequence;
    Map<Boolean, Set<Office_originsDTO>> mapOfOffice_originsDTOTostatus;
    Map<Integer, Set<Office_originsDTO>> mapOfOffice_originsDTOTocreatedBy;
    Map<Integer, Set<Office_originsDTO>> mapOfOffice_originsDTOTomodifiedBy;
    Map<Long, Set<Office_originsDTO>> mapOfOffice_originsDTOTocreated;
    Map<Long, Set<Office_originsDTO>> mapOfOffice_originsDTOTomodified;

    static Office_originsRepository instance = null;

    private Office_originsRepository() {
        mapOfOffice_originsDTOToiD = new ConcurrentHashMap<>();
        mapOfOffice_originsDTOToofficeMinistryType = new ConcurrentHashMap<>();
        mapOfOffice_originsDTOToofficeLayerType = new ConcurrentHashMap<>();
        mapOfOffice_originsDTOToofficeNameEng = new ConcurrentHashMap<>();
        mapOfOffice_originsDTOToofficeNameBng = new ConcurrentHashMap<>();
        mapOfOffice_originsDTOToparentOfficeType = new ConcurrentHashMap<>();
        mapOfOffice_originsDTOToofficeLevel = new ConcurrentHashMap<>();
        mapOfOffice_originsDTOToofficeSequence = new ConcurrentHashMap<>();
        mapOfOffice_originsDTOTostatus = new ConcurrentHashMap<>();
        mapOfOffice_originsDTOTocreatedBy = new ConcurrentHashMap<>();
        mapOfOffice_originsDTOTomodifiedBy = new ConcurrentHashMap<>();
        mapOfOffice_originsDTOTocreated = new ConcurrentHashMap<>();
        mapOfOffice_originsDTOTomodified = new ConcurrentHashMap<>();

        RepositoryManager.getInstance().addRepository(this);
    }

    public synchronized static Office_originsRepository getInstance() {
        if (instance == null) {
            instance = new Office_originsRepository();
        }
        return instance;
    }

    public void reload(boolean reloadAll) {
        try {
            List<Office_originsDTO> office_originsDTOs = office_originsDAO.getAllOffice_origins(reloadAll);
            for (Office_originsDTO office_originsDTO : office_originsDTOs) {
                Office_originsDTO oldOffice_originsDTO = getOffice_originsDTOByID(office_originsDTO.iD);
                if (oldOffice_originsDTO != null) {
                    mapOfOffice_originsDTOToiD.remove(oldOffice_originsDTO.iD);

                    if (mapOfOffice_originsDTOToofficeMinistryType.containsKey(oldOffice_originsDTO.officeMinistryType)) {
                        mapOfOffice_originsDTOToofficeMinistryType.get(oldOffice_originsDTO.officeMinistryType).remove(oldOffice_originsDTO);
                    }
                    if (mapOfOffice_originsDTOToofficeMinistryType.get(oldOffice_originsDTO.officeMinistryType).isEmpty()) {
                        mapOfOffice_originsDTOToofficeMinistryType.remove(oldOffice_originsDTO.officeMinistryType);
                    }

                    if (mapOfOffice_originsDTOToofficeLayerType.containsKey(oldOffice_originsDTO.officeLayerType)) {
                        mapOfOffice_originsDTOToofficeLayerType.get(oldOffice_originsDTO.officeLayerType).remove(oldOffice_originsDTO);
                    }
                    if (mapOfOffice_originsDTOToofficeLayerType.get(oldOffice_originsDTO.officeLayerType).isEmpty()) {
                        mapOfOffice_originsDTOToofficeLayerType.remove(oldOffice_originsDTO.officeLayerType);
                    }

                    if (mapOfOffice_originsDTOToofficeNameEng.containsKey(oldOffice_originsDTO.officeNameEng)) {
                        mapOfOffice_originsDTOToofficeNameEng.get(oldOffice_originsDTO.officeNameEng).remove(oldOffice_originsDTO);
                    }
                    if (mapOfOffice_originsDTOToofficeNameEng.get(oldOffice_originsDTO.officeNameEng).isEmpty()) {
                        mapOfOffice_originsDTOToofficeNameEng.remove(oldOffice_originsDTO.officeNameEng);
                    }

                    if (mapOfOffice_originsDTOToofficeNameBng.containsKey(oldOffice_originsDTO.officeNameBng)) {
                        mapOfOffice_originsDTOToofficeNameBng.get(oldOffice_originsDTO.officeNameBng).remove(oldOffice_originsDTO);
                    }
                    if (mapOfOffice_originsDTOToofficeNameBng.get(oldOffice_originsDTO.officeNameBng).isEmpty()) {
                        mapOfOffice_originsDTOToofficeNameBng.remove(oldOffice_originsDTO.officeNameBng);
                    }

                    if (mapOfOffice_originsDTOToparentOfficeType.containsKey(oldOffice_originsDTO.parentOfficeType)) {
                        mapOfOffice_originsDTOToparentOfficeType.get(oldOffice_originsDTO.parentOfficeType).remove(oldOffice_originsDTO);
                    }
                    if (mapOfOffice_originsDTOToparentOfficeType.get(oldOffice_originsDTO.parentOfficeType).isEmpty()) {
                        mapOfOffice_originsDTOToparentOfficeType.remove(oldOffice_originsDTO.parentOfficeType);
                    }

                    if (mapOfOffice_originsDTOToofficeLevel.containsKey(oldOffice_originsDTO.officeLevel)) {
                        mapOfOffice_originsDTOToofficeLevel.get(oldOffice_originsDTO.officeLevel).remove(oldOffice_originsDTO);
                    }
                    if (mapOfOffice_originsDTOToofficeLevel.get(oldOffice_originsDTO.officeLevel).isEmpty()) {
                        mapOfOffice_originsDTOToofficeLevel.remove(oldOffice_originsDTO.officeLevel);
                    }

                    if (mapOfOffice_originsDTOToofficeSequence.containsKey(oldOffice_originsDTO.officeSequence)) {
                        mapOfOffice_originsDTOToofficeSequence.get(oldOffice_originsDTO.officeSequence).remove(oldOffice_originsDTO);
                    }
                    if (mapOfOffice_originsDTOToofficeSequence.get(oldOffice_originsDTO.officeSequence).isEmpty()) {
                        mapOfOffice_originsDTOToofficeSequence.remove(oldOffice_originsDTO.officeSequence);
                    }

                    if (mapOfOffice_originsDTOTostatus.containsKey(oldOffice_originsDTO.status)) {
                        mapOfOffice_originsDTOTostatus.get(oldOffice_originsDTO.status).remove(oldOffice_originsDTO);
                    }
                    if (mapOfOffice_originsDTOTostatus.get(oldOffice_originsDTO.status).isEmpty()) {
                        mapOfOffice_originsDTOTostatus.remove(oldOffice_originsDTO.status);
                    }

                    if (mapOfOffice_originsDTOTocreatedBy.containsKey(oldOffice_originsDTO.createdBy)) {
                        mapOfOffice_originsDTOTocreatedBy.get(oldOffice_originsDTO.createdBy).remove(oldOffice_originsDTO);
                    }
                    if (mapOfOffice_originsDTOTocreatedBy.get(oldOffice_originsDTO.createdBy).isEmpty()) {
                        mapOfOffice_originsDTOTocreatedBy.remove(oldOffice_originsDTO.createdBy);
                    }

                    if (mapOfOffice_originsDTOTomodifiedBy.containsKey(oldOffice_originsDTO.modifiedBy)) {
                        mapOfOffice_originsDTOTomodifiedBy.get(oldOffice_originsDTO.modifiedBy).remove(oldOffice_originsDTO);
                    }
                    if (mapOfOffice_originsDTOTomodifiedBy.get(oldOffice_originsDTO.modifiedBy).isEmpty()) {
                        mapOfOffice_originsDTOTomodifiedBy.remove(oldOffice_originsDTO.modifiedBy);
                    }

                    if (mapOfOffice_originsDTOTocreated.containsKey(oldOffice_originsDTO.created)) {
                        mapOfOffice_originsDTOTocreated.get(oldOffice_originsDTO.created).remove(oldOffice_originsDTO);
                    }
                    if (mapOfOffice_originsDTOTocreated.get(oldOffice_originsDTO.created).isEmpty()) {
                        mapOfOffice_originsDTOTocreated.remove(oldOffice_originsDTO.created);
                    }

                    if (mapOfOffice_originsDTOTomodified.containsKey(oldOffice_originsDTO.modified)) {
                        mapOfOffice_originsDTOTomodified.get(oldOffice_originsDTO.modified).remove(oldOffice_originsDTO);
                    }
                    if (mapOfOffice_originsDTOTomodified.get(oldOffice_originsDTO.modified).isEmpty()) {
                        mapOfOffice_originsDTOTomodified.remove(oldOffice_originsDTO.modified);
                    }


                }
                if (office_originsDTO.isDeleted == false) {

                    mapOfOffice_originsDTOToiD.put(office_originsDTO.iD, office_originsDTO);

                    if (!mapOfOffice_originsDTOToofficeMinistryType.containsKey(office_originsDTO.officeMinistryType)) {
                        mapOfOffice_originsDTOToofficeMinistryType.put(office_originsDTO.officeMinistryType, new HashSet<>());
                    }
                    mapOfOffice_originsDTOToofficeMinistryType.get(office_originsDTO.officeMinistryType).add(office_originsDTO);

                    if (!mapOfOffice_originsDTOToofficeLayerType.containsKey(office_originsDTO.officeLayerType)) {
                        mapOfOffice_originsDTOToofficeLayerType.put(office_originsDTO.officeLayerType, new HashSet<>());
                    }
                    mapOfOffice_originsDTOToofficeLayerType.get(office_originsDTO.officeLayerType).add(office_originsDTO);

                    if (!mapOfOffice_originsDTOToofficeNameEng.containsKey(office_originsDTO.officeNameEng)) {
                        mapOfOffice_originsDTOToofficeNameEng.put(office_originsDTO.officeNameEng, new HashSet<>());
                    }
                    mapOfOffice_originsDTOToofficeNameEng.get(office_originsDTO.officeNameEng).add(office_originsDTO);

                    if (!mapOfOffice_originsDTOToofficeNameBng.containsKey(office_originsDTO.officeNameBng)) {
                        mapOfOffice_originsDTOToofficeNameBng.put(office_originsDTO.officeNameBng, new HashSet<>());
                    }
                    mapOfOffice_originsDTOToofficeNameBng.get(office_originsDTO.officeNameBng).add(office_originsDTO);

                    if (!mapOfOffice_originsDTOToparentOfficeType.containsKey(office_originsDTO.parentOfficeType)) {
                        mapOfOffice_originsDTOToparentOfficeType.put(office_originsDTO.parentOfficeType, new HashSet<>());
                    }
                    mapOfOffice_originsDTOToparentOfficeType.get(office_originsDTO.parentOfficeType).add(office_originsDTO);

                    if (!mapOfOffice_originsDTOToofficeLevel.containsKey(office_originsDTO.officeLevel)) {
                        mapOfOffice_originsDTOToofficeLevel.put(office_originsDTO.officeLevel, new HashSet<>());
                    }
                    mapOfOffice_originsDTOToofficeLevel.get(office_originsDTO.officeLevel).add(office_originsDTO);

                    if (!mapOfOffice_originsDTOToofficeSequence.containsKey(office_originsDTO.officeSequence)) {
                        mapOfOffice_originsDTOToofficeSequence.put(office_originsDTO.officeSequence, new HashSet<>());
                    }
                    mapOfOffice_originsDTOToofficeSequence.get(office_originsDTO.officeSequence).add(office_originsDTO);

                    if (!mapOfOffice_originsDTOTostatus.containsKey(office_originsDTO.status)) {
                        mapOfOffice_originsDTOTostatus.put(office_originsDTO.status, new HashSet<>());
                    }
                    mapOfOffice_originsDTOTostatus.get(office_originsDTO.status).add(office_originsDTO);

                    if (!mapOfOffice_originsDTOTocreatedBy.containsKey(office_originsDTO.createdBy)) {
                        mapOfOffice_originsDTOTocreatedBy.put(office_originsDTO.createdBy, new HashSet<>());
                    }
                    mapOfOffice_originsDTOTocreatedBy.get(office_originsDTO.createdBy).add(office_originsDTO);

                    if (!mapOfOffice_originsDTOTomodifiedBy.containsKey(office_originsDTO.modifiedBy)) {
                        mapOfOffice_originsDTOTomodifiedBy.put(office_originsDTO.modifiedBy, new HashSet<>());
                    }
                    mapOfOffice_originsDTOTomodifiedBy.get(office_originsDTO.modifiedBy).add(office_originsDTO);

                    if (!mapOfOffice_originsDTOTocreated.containsKey(office_originsDTO.created)) {
                        mapOfOffice_originsDTOTocreated.put(office_originsDTO.created, new HashSet<>());
                    }
                    mapOfOffice_originsDTOTocreated.get(office_originsDTO.created).add(office_originsDTO);

                    if (!mapOfOffice_originsDTOTomodified.containsKey(office_originsDTO.modified)) {
                        mapOfOffice_originsDTOTomodified.put(office_originsDTO.modified, new HashSet<>());
                    }
                    mapOfOffice_originsDTOTomodified.get(office_originsDTO.modified).add(office_originsDTO);

                }
            }

        } catch (Exception e) {
            logger.debug("FATAL", e);
        }
    }

    public List<Office_originsDTO> getOffice_originsList() {
        List<Office_originsDTO> office_originss = new ArrayList<Office_originsDTO>(this.mapOfOffice_originsDTOToiD.values());
        return office_originss;
    }


    public Office_originsDTO getOffice_originsDTOByID(long ID) {
        return mapOfOffice_originsDTOToiD.get(ID);
    }


    public List<Office_originsDTO> getOffice_originsDTOByoffice_ministry_type(int office_ministry_type) {
        return new ArrayList<>(mapOfOffice_originsDTOToofficeMinistryType.getOrDefault(office_ministry_type, new HashSet<>()));
    }


    public List<Office_originsDTO> getOffice_originsDTOByoffice_layer_type(int office_layer_type) {
        return new ArrayList<>(mapOfOffice_originsDTOToofficeLayerType.getOrDefault(office_layer_type, new HashSet<>()));
    }


    public List<Office_originsDTO> getOffice_originsDTOByoffice_name_eng(String office_name_eng) {
        return new ArrayList<>(mapOfOffice_originsDTOToofficeNameEng.getOrDefault(office_name_eng, new HashSet<>()));
    }


    public List<Office_originsDTO> getOffice_originsDTOByoffice_name_bng(String office_name_bng) {
        return new ArrayList<>(mapOfOffice_originsDTOToofficeNameBng.getOrDefault(office_name_bng, new HashSet<>()));
    }


    public List<Office_originsDTO> getOffice_originsDTOByparent_office_type(int parent_office_type) {
        return new ArrayList<>(mapOfOffice_originsDTOToparentOfficeType.getOrDefault(parent_office_type, new HashSet<>()));
    }


    public List<Office_originsDTO> getOffice_originsDTOByoffice_level(int office_level) {
        return new ArrayList<>(mapOfOffice_originsDTOToofficeLevel.getOrDefault(office_level, new HashSet<>()));
    }


    public List<Office_originsDTO> getOffice_originsDTOByoffice_sequence(int office_sequence) {
        return new ArrayList<>(mapOfOffice_originsDTOToofficeSequence.getOrDefault(office_sequence, new HashSet<>()));
    }


    public List<Office_originsDTO> getOffice_originsDTOBystatus(boolean status) {
        return new ArrayList<>(mapOfOffice_originsDTOTostatus.getOrDefault(status, new HashSet<>()));
    }


    public List<Office_originsDTO> getOffice_originsDTOBycreated_by(int created_by) {
        return new ArrayList<>(mapOfOffice_originsDTOTocreatedBy.getOrDefault(created_by, new HashSet<>()));
    }


    public List<Office_originsDTO> getOffice_originsDTOBymodified_by(int modified_by) {
        return new ArrayList<>(mapOfOffice_originsDTOTomodifiedBy.getOrDefault(modified_by, new HashSet<>()));
    }


    public List<Office_originsDTO> getOffice_originsDTOBycreated(long created) {
        return new ArrayList<>(mapOfOffice_originsDTOTocreated.getOrDefault(created, new HashSet<>()));
    }


    public List<Office_originsDTO> getOffice_originsDTOBymodified(long modified) {
        return new ArrayList<>(mapOfOffice_originsDTOTomodified.getOrDefault(modified, new HashSet<>()));
    }

    public void addNewDto(Office_originsDTO dto) {
        mapOfOffice_originsDTOToiD.put(dto.iD, dto);
    }


    @Override
    public String getTableName() {
        String tableName = "";
        try {
            tableName = "office_origins";
        } catch (Exception ex) {
            logger.debug("FATAL", ex);
        }
        return tableName;
    }
}


