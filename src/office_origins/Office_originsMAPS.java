package office_origins;
import java.util.*; 


public class Office_originsMAPS 
{

	public HashMap<String, String> java_allfield_type_map = new HashMap<String, String>();
	public HashMap<String, String> java_anyfield_search_map = new HashMap<String, String>();
	public HashMap<String, String> java_DTO_map = new HashMap<String, String>();
	public HashMap<String, String> java_SQL_map = new HashMap<String, String>();
	public HashMap<String, String> java_Text_map = new HashMap<String, String>();
	
	private static Office_originsMAPS self = null;
	
	private Office_originsMAPS()
	{
		
		java_allfield_type_map.put("office_ministry_id".toLowerCase(), "Integer");
		java_allfield_type_map.put("office_layer_id".toLowerCase(), "Integer");
		java_allfield_type_map.put("office_name_eng".toLowerCase(), "String");
		java_allfield_type_map.put("office_name_bng".toLowerCase(), "String");
		java_allfield_type_map.put("parent_office_id".toLowerCase(), "Integer");
		java_allfield_type_map.put("office_level".toLowerCase(), "Integer");
		java_allfield_type_map.put("office_sequence".toLowerCase(), "Integer");
		java_allfield_type_map.put("status".toLowerCase(), "Boolean");

		java_anyfield_search_map.put("office_ministries.name_eng".toLowerCase(), "String");
		java_anyfield_search_map.put("office_ministries.name_bng".toLowerCase(), "String");
		java_anyfield_search_map.put("office_layers.layer_name_eng".toLowerCase(), "String");
		java_anyfield_search_map.put("office_layers.layer_name_bng".toLowerCase(), "String");
		java_anyfield_search_map.put("office_origins.office_name_eng".toLowerCase(), "String");
		java_anyfield_search_map.put("office_origins.office_name_bng".toLowerCase(), "String");
		java_anyfield_search_map.put("office_origins.office_name_eng".toLowerCase(), "String");
		java_anyfield_search_map.put("office_origins.office_name_bng".toLowerCase(), "String");
		java_anyfield_search_map.put("office_origins.office_level".toLowerCase(), "Integer");
		java_anyfield_search_map.put("office_origins.office_sequence".toLowerCase(), "Integer");

		java_DTO_map.put("id".toLowerCase(), "id".toLowerCase());
		java_DTO_map.put("officeMinistryType".toLowerCase(), "officeMinistryType".toLowerCase());
		java_DTO_map.put("officeLayerType".toLowerCase(), "officeLayerType".toLowerCase());
		java_DTO_map.put("officeNameEng".toLowerCase(), "officeNameEng".toLowerCase());
		java_DTO_map.put("officeNameBng".toLowerCase(), "officeNameBng".toLowerCase());
		java_DTO_map.put("parentOfficeType".toLowerCase(), "parentOfficeType".toLowerCase());
		java_DTO_map.put("officeLevel".toLowerCase(), "officeLevel".toLowerCase());
		java_DTO_map.put("officeSequence".toLowerCase(), "officeSequence".toLowerCase());
		java_DTO_map.put("status".toLowerCase(), "status".toLowerCase());
		java_DTO_map.put("createdBy".toLowerCase(), "createdBy".toLowerCase());
		java_DTO_map.put("modifiedBy".toLowerCase(), "modifiedBy".toLowerCase());
		java_DTO_map.put("created".toLowerCase(), "created".toLowerCase());
		java_DTO_map.put("modified".toLowerCase(), "modified".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());

		java_SQL_map.put("office_ministry_id".toLowerCase(), "officeMinistryType".toLowerCase());
		java_SQL_map.put("office_layer_id".toLowerCase(), "officeLayerType".toLowerCase());
		java_SQL_map.put("office_name_eng".toLowerCase(), "officeNameEng".toLowerCase());
		java_SQL_map.put("office_name_bng".toLowerCase(), "officeNameBng".toLowerCase());
		java_SQL_map.put("parent_office_id".toLowerCase(), "parentOfficeType".toLowerCase());
		java_SQL_map.put("office_level".toLowerCase(), "officeLevel".toLowerCase());
		java_SQL_map.put("office_sequence".toLowerCase(), "officeSequence".toLowerCase());
		java_SQL_map.put("status".toLowerCase(), "status".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "id".toLowerCase());
		java_Text_map.put("Office Ministry".toLowerCase(), "officeMinistryType".toLowerCase());
		java_Text_map.put("Office Layer".toLowerCase(), "officeLayerType".toLowerCase());
		java_Text_map.put("Office Name Eng".toLowerCase(), "officeNameEng".toLowerCase());
		java_Text_map.put("Office Name Bng".toLowerCase(), "officeNameBng".toLowerCase());
		java_Text_map.put("Parent Office".toLowerCase(), "parentOfficeType".toLowerCase());
		java_Text_map.put("Office Level".toLowerCase(), "officeLevel".toLowerCase());
		java_Text_map.put("Office Sequence".toLowerCase(), "officeSequence".toLowerCase());
		java_Text_map.put("Status".toLowerCase(), "status".toLowerCase());
		java_Text_map.put("Created By".toLowerCase(), "createdBy".toLowerCase());
		java_Text_map.put("Modified By".toLowerCase(), "modifiedBy".toLowerCase());
		java_Text_map.put("Created".toLowerCase(), "created".toLowerCase());
		java_Text_map.put("Modified".toLowerCase(), "modified".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
			
	}
	
	public static Office_originsMAPS GetInstance()
	{
		if(self == null)
		{
			self = new Office_originsMAPS ();
		}
		return self;
	}
	

}