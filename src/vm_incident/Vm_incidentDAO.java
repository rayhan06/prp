package vm_incident;

import common.CommonDAOService;
import org.apache.log4j.Logger;
import pb.CatRepository;
import pb.CategoryLanguageModel;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

@SuppressWarnings("Duplicates")
public class Vm_incidentDAO implements CommonDAOService<Vm_incidentDTO> {

    private static final Logger logger = Logger.getLogger(Vm_incidentDAO.class);

    private static final String addQuery = "INSERT INTO {tableName} (inserted_by_user_id,inserted_by_organogram_id," +
			"insertion_date,search_column,vehicle_type_cat,vehicle_id,reg_no,incident_cat,incident_date," +
			"incident_place,incident_time,incident_details,files_dropzone,driver_org_id,driver_office_id," +
			"driver_office_unit_id,driver_emp_id,driver_phone_num,driver_name_en,driver_name_bn,driver_office_name_en," +
			"driver_office_name_bn,driver_office_unit_name_en,driver_office_unit_name_bn," +
			"driver_office_unit_org_name_en,driver_office_unit_org_name_bn,lastModificationTime," +
			"isDeleted,ID) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

    private static final String updateQuery = "UPDATE {tableName} SET inserted_by_user_id = ?,inserted_by_organogram_id=?,insertion_date=?," +
			"search_column=?,vehicle_type_cat=?,vehicle_id=?,reg_no=?,incident_cat=?,incident_date=?," +
			"incident_place=?,incident_time=?,incident_details=?,files_dropzone=?,driver_org_id=?,driver_office_id?," +
			"driver_office_unit_id=?,driver_emp_id=?,driver_phone_num=?,driver_name_en=?,driver_name_bn=?,driver_office_name_en=?," +
			"driver_office_name_bn=?,driver_office_unit_name_en=?,driver_office_unit_name_bn=?," +
			"driver_office_unit_org_name_en=?,driver_office_unit_org_name_bn=?,lastModificationTime=? WHERE ID = ?";

    private final Map<String,String> searchMap = new HashMap<>();

    private static class LazyLoader{
    	static final Vm_incidentDAO INSTANCE = new Vm_incidentDAO();
	}

	public static Vm_incidentDAO getInstance(){
    	return LazyLoader.INSTANCE;
	}

    private Vm_incidentDAO() {
    	searchMap.put("vehicle_type_cat"," and (vehicle_type_cat = ?)");
    	searchMap.put("vehicle_id"," and (vehicle_id = ?)");
    	searchMap.put("incident_cat"," and (incident_cat = ?)");
    	searchMap.put("incident_date_start"," and (incident_date>= ?)");
    	searchMap.put("incident_date_end"," and (incident_date <= ?)");
		searchMap.put("AnyField"," and (search_column like ?)");
		searchMap.put("inserted_by_organogram_id_internal"," and (inserted_by_organogram_id = ?)");
    }

    public void setSearchColumn(Vm_incidentDTO vm_incidentDTO) {
        vm_incidentDTO.searchColumn = "";
		CategoryLanguageModel model = CatRepository.getInstance().getCategoryLanguageModel("vehicle_type",vm_incidentDTO.vehicleTypeCat);
        vm_incidentDTO.searchColumn += model.englishText+" "+model.banglaText+" ";
        vm_incidentDTO.searchColumn += vm_incidentDTO.regNo + " ";
		model = CatRepository.getInstance().getCategoryLanguageModel("incident",vm_incidentDTO.incidentCat);
        vm_incidentDTO.searchColumn += model.englishText+" "+model.banglaText+" ";
        vm_incidentDTO.searchColumn += vm_incidentDTO.incidentPlace + " ";
        vm_incidentDTO.searchColumn += vm_incidentDTO.incidentDetails + " ";
        vm_incidentDTO.searchColumn += vm_incidentDTO.driverPhoneNum + " ";
        vm_incidentDTO.searchColumn += vm_incidentDTO.driverNameEn + " ";
        vm_incidentDTO.searchColumn += vm_incidentDTO.driverNameBn + " ";
        vm_incidentDTO.searchColumn += vm_incidentDTO.driverOfficeNameEn + " ";
        vm_incidentDTO.searchColumn += vm_incidentDTO.driverOfficeNameBn + " ";
        vm_incidentDTO.searchColumn += vm_incidentDTO.driverOfficeUnitNameEn + " ";
        vm_incidentDTO.searchColumn += vm_incidentDTO.driverOfficeUnitNameBn + " ";
        vm_incidentDTO.searchColumn += vm_incidentDTO.driverOfficeUnitOrgNameEn + " ";
        vm_incidentDTO.searchColumn += vm_incidentDTO.driverOfficeUnitOrgNameBn + " ";
    }

	@Override
	public void set(PreparedStatement ps, Vm_incidentDTO vm_incidentDTO, boolean isInsert) throws SQLException {
		setSearchColumn(vm_incidentDTO);
		int index = 0;
		ps.setObject(++index, vm_incidentDTO.insertedByUserId);
		ps.setObject(++index, vm_incidentDTO.insertedByOrganogramId);
		ps.setObject(++index, vm_incidentDTO.insertionDate);
		ps.setObject(++index, vm_incidentDTO.searchColumn);
		ps.setObject(++index, vm_incidentDTO.vehicleTypeCat);
		ps.setObject(++index, vm_incidentDTO.vehicleId);
		ps.setObject(++index, vm_incidentDTO.regNo);
		ps.setObject(++index, vm_incidentDTO.incidentCat);
		ps.setObject(++index, vm_incidentDTO.incidentDate);
		ps.setObject(++index, vm_incidentDTO.incidentPlace);
		ps.setObject(++index, vm_incidentDTO.incidentTime);
		ps.setObject(++index, vm_incidentDTO.incidentDetails);
		ps.setObject(++index, vm_incidentDTO.filesDropzone);
		ps.setObject(++index, vm_incidentDTO.driverOrgId);
		ps.setObject(++index, vm_incidentDTO.driverOfficeId);
		ps.setObject(++index, vm_incidentDTO.driverOfficeUnitId);
		ps.setObject(++index, vm_incidentDTO.driverEmpId);
		ps.setObject(++index, vm_incidentDTO.driverPhoneNum);
		ps.setObject(++index, vm_incidentDTO.driverNameEn);
		ps.setObject(++index, vm_incidentDTO.driverNameBn);
		ps.setObject(++index, vm_incidentDTO.driverOfficeNameEn);
		ps.setObject(++index, vm_incidentDTO.driverOfficeNameBn);
		ps.setObject(++index, vm_incidentDTO.driverOfficeUnitNameEn);
		ps.setObject(++index, vm_incidentDTO.driverOfficeUnitNameBn);
		ps.setObject(++index, vm_incidentDTO.driverOfficeUnitOrgNameEn);
		ps.setObject(++index, vm_incidentDTO.driverOfficeUnitOrgNameBn);
		ps.setObject(++index, vm_incidentDTO.lastModificationTime);
		if (isInsert) {
			ps.setObject(++index, 0);
		}
		ps.setObject(++index, vm_incidentDTO.iD);
	}

	@Override
	public Vm_incidentDTO buildObjectFromResultSet(ResultSet rs) {
		try {
			Vm_incidentDTO vm_incidentDTO = new Vm_incidentDTO();
			vm_incidentDTO.iD = rs.getLong("ID");
			vm_incidentDTO.insertedByUserId = rs.getLong("inserted_by_user_id");
			vm_incidentDTO.insertedByOrganogramId = rs.getLong("inserted_by_organogram_id");
			vm_incidentDTO.insertionDate = rs.getLong("insertion_date");
			vm_incidentDTO.lastModificationTime = rs.getLong("lastModificationTime");
			vm_incidentDTO.searchColumn = rs.getString("search_column");
			vm_incidentDTO.vehicleTypeCat = rs.getInt("vehicle_type_cat");
			vm_incidentDTO.vehicleId = rs.getLong("vehicle_id");
			vm_incidentDTO.regNo = rs.getString("reg_no");
			vm_incidentDTO.incidentCat = rs.getInt("incident_cat");
			vm_incidentDTO.incidentDate = rs.getLong("incident_date");
			vm_incidentDTO.incidentPlace = rs.getString("incident_place");
			vm_incidentDTO.incidentTime = rs.getString("incident_time");
			vm_incidentDTO.incidentDetails = rs.getString("incident_details");
			vm_incidentDTO.filesDropzone = rs.getLong("files_dropzone");
			vm_incidentDTO.driverOrgId = rs.getLong("driver_org_id");
			vm_incidentDTO.driverOfficeId = rs.getLong("driver_office_id");
			vm_incidentDTO.driverOfficeUnitId = rs.getLong("driver_office_unit_id");
			vm_incidentDTO.driverEmpId = rs.getLong("driver_emp_id");
			vm_incidentDTO.driverPhoneNum = rs.getString("driver_phone_num");
			vm_incidentDTO.driverNameEn = rs.getString("driver_name_en");
			vm_incidentDTO.driverNameBn = rs.getString("driver_name_bn");
			vm_incidentDTO.driverOfficeNameEn = rs.getString("driver_office_name_en");
			vm_incidentDTO.driverOfficeNameBn = rs.getString("driver_office_name_bn");
			vm_incidentDTO.driverOfficeUnitNameEn = rs.getString("driver_office_unit_name_en");
			vm_incidentDTO.driverOfficeUnitNameBn = rs.getString("driver_office_unit_name_bn");
			vm_incidentDTO.driverOfficeUnitOrgNameEn = rs.getString("driver_office_unit_org_name_en");
			vm_incidentDTO.driverOfficeUnitOrgNameBn = rs.getString("driver_office_unit_org_name_bn");
			vm_incidentDTO.isDeleted = rs.getInt("isDeleted");
			return vm_incidentDTO;
		} catch (SQLException ex) {
			logger.error(ex);
			return null;
		}
	}

	@Override
	public String getTableName() {
		return "vm_incident";
	}

	@Override
	public Map<String, String> getSearchMap() {
		return searchMap;
	}

	public long add(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Vm_incidentDTO) commonDTO,addQuery,true);
	}

	public long update(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Vm_incidentDTO) commonDTO,updateQuery,false);
	}
}