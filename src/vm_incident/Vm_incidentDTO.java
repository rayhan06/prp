package vm_incident;

import util.CommonDTO;


public class Vm_incidentDTO extends CommonDTO {
    public long insertedByUserId = -1;
    public long insertedByOrganogramId = -1;
    public long insertionDate = -1;
    public int vehicleTypeCat = -1;
    public long vehicleId = -1;
    public String regNo = "";
    public int incidentCat = -1;
    public long incidentDate = System.currentTimeMillis();
    public String incidentPlace = "";
    public String incidentTime = "";
    public String incidentDetails = "";
    public long filesDropzone = -1;
    public long driverOrgId = -1;
    public long driverOfficeId = -1;
    public long driverOfficeUnitId = -1;
    public long driverEmpId = -1;
    public String driverPhoneNum = "";
    public String driverNameEn = "";
    public String driverNameBn = "";
    public String driverOfficeNameEn = "";
    public String driverOfficeNameBn = "";
    public String driverOfficeUnitNameEn = "";
    public String driverOfficeUnitNameBn = "";
    public String driverOfficeUnitOrgNameEn = "";
    public String driverOfficeUnitOrgNameBn = "";

    @Override
    public String toString() {
        return "$Vm_incidentDTO[" +
                " iD = " + iD +
                " insertedByUserId = " + insertedByUserId +
                " insertedByOrganogramId = " + insertedByOrganogramId +
                " insertionDate = " + insertionDate +
                " lastModificationTime = " + lastModificationTime +
                " searchColumn = " + searchColumn +
                " vehicleTypeCat = " + vehicleTypeCat +
                " vehicleId = " + vehicleId +
                " regNo = " + regNo +
                " incidentCat = " + incidentCat +
                " incidentDate = " + incidentDate +
                " incidentPlace = " + incidentPlace +
                " incidentTime = " + incidentTime +
                " incidentDetails = " + incidentDetails +
                " filesDropzone = " + filesDropzone +
                " driverOrgId = " + driverOrgId +
                " driverOfficeId = " + driverOfficeId +
                " driverOfficeUnitId = " + driverOfficeUnitId +
                " driverEmpId = " + driverEmpId +
                " driverPhoneNum = " + driverPhoneNum +
                " driverNameEn = " + driverNameEn +
                " driverNameBn = " + driverNameBn +
                " driverOfficeNameEn = " + driverOfficeNameEn +
                " driverOfficeNameBn = " + driverOfficeNameBn +
                " driverOfficeUnitNameEn = " + driverOfficeUnitNameEn +
                " driverOfficeUnitNameBn = " + driverOfficeUnitNameBn +
                " driverOfficeUnitOrgNameEn = " + driverOfficeUnitOrgNameEn +
                " driverOfficeUnitOrgNameBn = " + driverOfficeUnitOrgNameBn +
                " isDeleted = " + isDeleted +
                "]";
    }
}