package vm_incident;

import common.BaseServlet;
import common.CommonDAOService;
import employee_assign.EmployeeSearchIds;
import employee_assign.EmployeeSearchModalUtil;
import employee_assign.EmployeeSearchModel;
import office_units.Office_unitsRepository;
import offices.OfficesDTO;
import offices.OfficesRepository;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import pb.CatRepository;
import pb.CategoryLanguageModel;
import pb.Utils;
import permission.MenuConstants;
import role.PermissionRepository;
import role.RoleDTO;
import sessionmanager.SessionConstants;
import user.UserDTO;
import util.CommonDTO;
import util.HttpRequestUtils;
import vm_requisition.Vm_requisitionDAO;
import vm_requisition.Vm_requisitionDTO;
import vm_vehicle.Vm_vehicleDTO;
import vm_vehicle.Vm_vehicleRepository;
import vm_vehicle_driver_assignment.Vm_vehicle_driver_assignmentDAO;
import vm_vehicle_driver_assignment.Vm_vehicle_driver_assignmentDTO;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings({"Duplicates"})
@WebServlet("/Vm_incidentServlet")
@MultipartConfig
public class Vm_incidentServlet extends BaseServlet {
    private static final long serialVersionUID = 1L;

    public static Logger logger = Logger.getLogger(Vm_incidentServlet.class);

    @Override
    public String getTableName() {
        return Vm_incidentDAO.getInstance().getTableName();
    }

    @Override
    public String getServletName() {
        return "Vm_incidentServlet";
    }

    @Override
    public Vm_incidentDAO getCommonDAOService() {
        return Vm_incidentDAO.getInstance();
    }

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        Vm_incidentDTO vm_incidentDTO;
        SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
        if (addFlag) {
            vm_incidentDTO = new Vm_incidentDTO();
            vm_incidentDTO.insertionDate = vm_incidentDTO.lastModificationTime = System.currentTimeMillis();
            vm_incidentDTO.insertedByUserId = userDTO.ID;
            vm_incidentDTO.insertedByOrganogramId = userDTO.organogramID;
        } else {
            vm_incidentDTO = Vm_incidentDAO.getInstance().getDTOFromID(Long.parseLong(request.getParameter("iD")));
            if (vm_incidentDTO == null) {
                throw new Exception("Vehicle incident information is not found");
            }
            vm_incidentDTO.lastModificationTime = System.currentTimeMillis();
        }

        vm_incidentDTO.vehicleTypeCat = Integer.parseInt(request.getParameter("vehicleTypeCat"));
        CategoryLanguageModel categoryLanguageModel = CatRepository.getInstance().getCategoryLanguageModel("vehicle_type", vm_incidentDTO.vehicleTypeCat);
        if (categoryLanguageModel == null) {
            throw new Exception("Vehicle type is invalid");
        }

        vm_incidentDTO.vehicleId = Long.parseLong(request.getParameter("vehicleId"));
        Vm_vehicleDTO vmVehicleDTO = Vm_vehicleRepository.getInstance().getVm_vehicleDTOByID(vm_incidentDTO.vehicleId);
        if (vmVehicleDTO == null) {
            throw new Exception("Vehicle id is invalid");
        }

        vm_incidentDTO.incidentCat = Integer.parseInt(request.getParameter("incidentCat"));
        categoryLanguageModel = CatRepository.getInstance().getCategoryLanguageModel("incident", vm_incidentDTO.incidentCat);
        if (categoryLanguageModel == null) {
            throw new Exception("Incident type is invalid");
        }

        vm_incidentDTO.incidentPlace = Jsoup.clean(request.getParameter("incidentPlace"), Whitelist.simpleText());
        if (vm_incidentDTO.incidentPlace.isEmpty()) {
            throw new Exception("Please insert incident place");
        }

        vm_incidentDTO.incidentDetails = Jsoup.clean(request.getParameter("incidentDetails"), Whitelist.simpleText());
        if (vm_incidentDTO.incidentDetails.isEmpty()) {
            throw new Exception("Please insert incident details");
        }

        vm_incidentDTO.incidentTime = Jsoup.clean(request.getParameter("incidentTime"), Whitelist.simpleText());
        if (vm_incidentDTO.incidentTime.isEmpty()) {
            throw new Exception("Please insert incident time");
        }

        String Value = request.getParameter("incidentDate");

        if (Value != null && !Value.equalsIgnoreCase("")) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }
        logger.debug("incidentDate = " + Value);
        if (Value != null && !Value.equalsIgnoreCase("")) {
            try {
                Date d = f.parse(Value);
                vm_incidentDTO.incidentDate = d.getTime();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            logger.debug("FieldName has a null Value, not updating" + " = " + Value);
        }

        Value = request.getParameter("filesDropzone");

        if (Value != null && !Value.equalsIgnoreCase("")) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }
        logger.debug("filesDropzone = " + Value);
        if (Value != null && !Value.equalsIgnoreCase("")) {
            logger.debug("filesDropzone = " + Value);
            vm_incidentDTO.filesDropzone = Long.parseLong(Value);
            if (!addFlag) {
                String filesDropzoneFilesToDelete = request.getParameter("filesDropzoneFilesToDelete");
                String[] deleteArray = filesDropzoneFilesToDelete.split(",");
                for (int i = 0; i < deleteArray.length; i++) {
                    logger.debug("going to delete " + deleteArray[i]);
                    if (i > 0) {
                        filesDAO.delete(Long.parseLong(deleteArray[i]));
                    }
                }
            }
        } else {
            logger.debug("FieldName has a null Value, not updating" + " = " + Value);
        }


        vm_incidentDTO.regNo = vmVehicleDTO.regNo;

        boolean driverFound = false;

        Vm_vehicle_driver_assignmentDTO vm_vehicle_driver_assignmentDTO = new Vm_vehicle_driver_assignmentDAO().getAllVm_vehicle_driver_assignmentByVehicleId(vm_incidentDTO.vehicleId);

        if (vm_vehicle_driver_assignmentDTO != null) {
            EmployeeSearchIds employeeSearchIds = EmployeeSearchModalUtil.getDefaultOfficeIds(vm_vehicle_driver_assignmentDTO.driverId);

            EmployeeSearchModel model = EmployeeSearchModalUtil.getEmployeeSearchModel(employeeSearchIds);

            vm_incidentDTO.driverEmpId = model.employeeRecordId;
            vm_incidentDTO.driverOfficeUnitId = model.officeUnitId;
            vm_incidentDTO.driverOrgId = model.organogramId;

            vm_incidentDTO.driverNameEn = model.employeeNameEn;
            vm_incidentDTO.driverNameBn = model.employeeNameBn;
            vm_incidentDTO.driverOfficeUnitNameEn = model.officeUnitNameEn;
            vm_incidentDTO.driverOfficeUnitNameBn = model.officeUnitNameBn;
            vm_incidentDTO.driverOfficeUnitOrgNameEn = model.organogramNameEn;
            vm_incidentDTO.driverOfficeUnitOrgNameBn = model.organogramNameBn;

            vm_incidentDTO.driverPhoneNum = model.phoneNumber;

            OfficesDTO driverOffice = OfficesRepository.getInstance().getOfficesDTOByID(vm_incidentDTO.driverOfficeId);

            if (driverOffice != null) {
                vm_incidentDTO.driverOfficeId = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(model.officeUnitId).officeId;
                vm_incidentDTO.driverOfficeNameEn = driverOffice.officeNameEng;
                vm_incidentDTO.driverOfficeNameBn = driverOffice.officeNameBng;
            }

            driverFound = true;
        } else {
            String filter = " (start_date < " + vm_incidentDTO.incidentDate;
            filter += " OR (start_date = " + vm_incidentDTO.incidentDate;
            filter += " AND (STR_TO_DATE( start_time , '%l:%i %p' )) <= " + "(STR_TO_DATE( '" + vm_incidentDTO.incidentTime + "' , '%l:%i %p' ))))";
            filter += " AND (end_date > " + vm_incidentDTO.incidentDate;
            filter += " OR (end_date = " + vm_incidentDTO.incidentDate;
            filter += " AND (STR_TO_DATE( end_time , '%l:%i %p' )) >= " + "(STR_TO_DATE( '" + vm_incidentDTO.incidentTime + "' , '%l:%i %p' ))))" + " ";


            List<Vm_requisitionDTO> vm_requisitionDTOS = new Vm_requisitionDAO().getDTOs(null, 1, 0, true, userDTO, filter, false);

            if (!vm_requisitionDTOS.isEmpty()) {

                Vm_requisitionDTO model = vm_requisitionDTOS.get(0);

                vm_incidentDTO.driverEmpId = model.driverEmpId;
                vm_incidentDTO.driverOfficeUnitId = model.driverOfficeUnitId;
                vm_incidentDTO.driverOrgId = model.driverOrgId;

                vm_incidentDTO.driverNameEn = model.driverNameEn;
                vm_incidentDTO.driverNameBn = model.driverNameBn;
                vm_incidentDTO.driverOfficeUnitNameEn = model.driverOfficeUnitNameEn;
                vm_incidentDTO.driverOfficeUnitNameBn = model.driverOfficeUnitNameBn;
                vm_incidentDTO.driverOfficeUnitOrgNameEn = model.driverOfficeUnitOrgNameEn;
                vm_incidentDTO.driverOfficeUnitOrgNameBn = model.driverOfficeUnitOrgNameBn;

                vm_incidentDTO.driverPhoneNum = model.driverPhoneNum;

                OfficesDTO driverOffice = OfficesRepository.getInstance().getOfficesDTOByID(vm_incidentDTO.driverOfficeId);

                if (driverOffice != null) {
                    vm_incidentDTO.driverOfficeId = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(model.driverOfficeUnitId).officeId;

                    vm_incidentDTO.driverOfficeNameEn = driverOffice.officeNameEng;
                    vm_incidentDTO.driverOfficeNameBn = driverOffice.officeNameBng;
                }

                driverFound = true;
            }
        }

        if (driverFound) {
            Utils.handleTransactionForNavigationService4(()->{
                if (addFlag) {
                    getCommonDAOService().add(vm_incidentDTO);
                } else {
                    getCommonDAOService().update(vm_incidentDTO);
                }            });
            return vm_incidentDTO;
        } else {
            throw new Exception("Driver is not found");
        }
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[] {MenuConstants.VM_INCIDENT_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[] {MenuConstants.VM_INCIDENT_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[] {MenuConstants.VM_INCIDENT_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Vm_incidentServlet.class;
    }

    @Override
    public boolean getEditPagePermission(HttpServletRequest request) {
        return checkPermission(request);
    }

    @Override
    public boolean getViewPagePermission(HttpServletRequest request) {
        return checkPermission(request);
    }

    @Override
    public boolean getEditPermission(HttpServletRequest request) {
        return checkPermission(request);
    }

    private boolean checkPermission(HttpServletRequest request) {
        UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;
        RoleDTO role = PermissionRepository.getRoleDTOByRoleID(userDTO.roleID);
        boolean isAdmin = role.ID == SessionConstants.VEHICLE_ADMIN_ROLE || role.ID == SessionConstants.ADMIN_ROLE;
        if (isAdmin) {
            return true;
        }
        Vm_incidentDTO vmIncidentDTO = getCommonDAOService().getDTOFromID(Long.parseLong(request.getParameter("ID")));
        if (vmIncidentDTO == null) {
            return false;
        }
        return vmIncidentDTO.insertedByOrganogramId == userDTO.organogramID;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String actionType = request.getParameter("actionType");
        if ("search".equals(actionType)) {
            UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;
            RoleDTO role = PermissionRepository.getRoleDTOByRoleID(userDTO.roleID);
            boolean isAdmin = role.ID == SessionConstants.VEHICLE_ADMIN_ROLE || role.ID == SessionConstants.ADMIN_ROLE;
            if (!isAdmin) {
                Map<String, String> extraCriteriaMap = new HashMap<>();
                extraCriteriaMap.put("inserted_by_organogram_id_internal", String.valueOf(userDTO.organogramID));
                request.setAttribute(CommonDAOService.CRITERIA_MAP_KEY, extraCriteriaMap);
            }
        }
        super.doGet(request,response);
    }
}