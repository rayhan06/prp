package cash_management;

import bangladehi_number_format_util.BangladeshiNumberFormatter;
import finance.CashTypeEnum;
import sessionmanager.SessionConstants;
import util.CommonDTO;

import static util.StringUtils.convertBanglaIfLanguageIsBangla;


public class Cash_managementDTO extends CommonDTO {

    public long chequeRegisterId = 0;
    public long billRegisterId = -1;
    public String billDescription = "";
    public long billAmount = 0;
    public int permanentAdvancedExcluded = 0;
    public int toBeGivenAdvancedExcluded = 0;
    public int miscellaneous = 0;
    public int cashStatusCat = 0;
    public int cashType = 0;
    public long salaryAmount = 0;
    public long allowanceAmount = 0;
    public long insertedBy = -1;
    public long insertionTime = SessionConstants.MIN_DATE;
    public long modifiedBy = -1;

    public String getFormattedSalaryAmount(String language) {
        if (cashType != CashTypeEnum.SALARY.getValue()) return "";
        return BangladeshiNumberFormatter.getFormattedNumber(
                convertBanglaIfLanguageIsBangla(language, String.format("%d", salaryAmount))
        );
    }

    public String getFormattedAllowanceAmount(String language) {
        if (cashType != CashTypeEnum.ALLOWANCE.getValue()) return "";
        return BangladeshiNumberFormatter.getFormattedNumber(
                convertBanglaIfLanguageIsBangla(language, String.format("%d", allowanceAmount))
        );
    }

    public String getFormattedOthersAmount(String language) {
        if (cashType != CashTypeEnum.OTHERS.getValue()) return "";
        return BangladeshiNumberFormatter.getFormattedNumber(
                convertBanglaIfLanguageIsBangla(language, String.format("%d", miscellaneous))
        );
    }

    public String getFormattedTotalAmount(String language) {
        return BangladeshiNumberFormatter.getFormattedNumber(
                convertBanglaIfLanguageIsBangla(language, String.format(
                        "%d",
                        salaryAmount + allowanceAmount + miscellaneous
                ))
        );
    }

    @Override
    public String toString() {
        return "$Cash_managementDTO[" +
               " iD = " + iD +
               " chequeRegisterId = " + chequeRegisterId +
               " billNumber = " + billRegisterId +
               " billDescription = " + billDescription +
               " billAmount = " + billAmount +
               " permanentAdvancedExcluded = " + permanentAdvancedExcluded +
               " toBeGivenAdvancedExcluded = " + toBeGivenAdvancedExcluded +
               " miscellaneous = " + miscellaneous +
               " cashStatusCat = " + cashStatusCat +
               " insertedBy = " + insertedBy +
               " insertionTime = " + insertionTime +
               " isDeleted = " + isDeleted +
               " modifiedBy = " + modifiedBy +
               " lastModificationTime = " + lastModificationTime +
               "]";
    }

}