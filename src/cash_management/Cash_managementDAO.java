package cash_management;

import common.CommonDAOService;
import common.ConnectionAndStatementUtil;
import org.apache.log4j.Logger;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

@SuppressWarnings({"Duplicates"})
public class Cash_managementDAO implements CommonDAOService<Cash_managementDTO> {
    private static final Logger logger = Logger.getLogger(Cash_managementDAO.class);

    private static final String addQuery =
            "INSERT INTO {tableName} (cheque_register_id,bill_register_id,bill_description,bill_amount,"
                    .concat("permanent_advanced_excluded,to_be_given_advanced_excluded,miscellaneous,cash_status_cat,")
                    .concat("cash_type,salary_amount,allowance_amount,modified_by,lastModificationTime,")
                    .concat("inserted_by,insertion_time,isDeleted,ID) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

    private static final String updateQuery =
            "UPDATE {tableName} SET cheque_register_id=?,bill_register_id=?,bill_description=?,bill_amount=?,"
                    .concat("permanent_advanced_excluded=?,to_be_given_advanced_excluded=?,miscellaneous=?,cash_status_cat=?,")
                    .concat("cash_type=?,salary_amount=?,allowance_amount=?,modified_by=?,lastModificationTime=? WHERE ID=?");

    private static final Map<String, String> searchMap = new HashMap<>();

    private Cash_managementDAO() {
        searchMap.put("bill_number", " and (bill_register_id like ?) ");
        searchMap.put("date_from", " AND (insertion_time >= ?)");
        searchMap.put("date_to", " AND (insertion_time <= ?)");
    }

    private static class LazyLoader {
        static final Cash_managementDAO INSTANCE = new Cash_managementDAO();
    }

    public static Cash_managementDAO getInstance() {
        return Cash_managementDAO.LazyLoader.INSTANCE;
    }

    @Override
    public void set(PreparedStatement ps, Cash_managementDTO dto, boolean isInsert) throws SQLException {
        int index = 0;
        ps.setLong(++index, dto.chequeRegisterId);
        ps.setLong(++index, dto.billRegisterId);
        ps.setObject(++index, dto.billDescription);
        ps.setLong(++index, dto.billAmount);
        ps.setInt(++index, dto.permanentAdvancedExcluded);
        ps.setInt(++index, dto.toBeGivenAdvancedExcluded);
        ps.setInt(++index, dto.miscellaneous);
        ps.setInt(++index, dto.cashStatusCat);
        ps.setInt(++index, dto.cashType);
        ps.setLong(++index, dto.salaryAmount);
        ps.setLong(++index, dto.allowanceAmount);
        ps.setLong(++index, dto.modifiedBy);
        ps.setLong(++index, dto.lastModificationTime);
        if (isInsert) {
            ps.setLong(++index, dto.insertedBy);
            ps.setLong(++index, dto.insertionTime);
            ps.setInt(++index, 0);
        }
        ps.setLong(++index, dto.iD);
    }

    @Override
    public Cash_managementDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Cash_managementDTO dto = new Cash_managementDTO();
            dto.iD = rs.getLong("ID");
            dto.chequeRegisterId = rs.getLong("cheque_register_id");
            dto.billRegisterId = rs.getLong("bill_register_id");
            dto.billDescription = rs.getString("bill_description");
            dto.billAmount = rs.getLong("bill_amount");
            dto.permanentAdvancedExcluded = rs.getInt("permanent_advanced_excluded");
            dto.toBeGivenAdvancedExcluded = rs.getInt("to_be_given_advanced_excluded");
            dto.miscellaneous = rs.getInt("miscellaneous");
            dto.cashStatusCat = rs.getInt("cash_status_cat");
            dto.cashType = rs.getInt("cash_type");
            dto.salaryAmount = rs.getLong("salary_amount");
            dto.allowanceAmount = rs.getLong("allowance_amount");
            dto.insertedBy = rs.getLong("inserted_by");
            dto.insertionTime = rs.getLong("insertion_time");
            dto.isDeleted = rs.getInt("isDeleted");
            dto.modifiedBy = rs.getLong("modified_by");
            dto.lastModificationTime = rs.getLong("lastModificationTime");
            return dto;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    @Override
    public String getTableName() {
        return "cash_management";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    @Override
    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Cash_managementDTO) commonDTO, addQuery, true);
    }

    @Override
    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Cash_managementDTO) commonDTO, updateQuery, false);
    }

    private static final String getByChequeRegisterAndCashStatus =
            "SELECT * FROM cash_management WHERE cheque_register_id=%d AND cash_status_cat=%d AND isDeleted=0";

    public Cash_managementDTO getDTOByChequeRegisterAndCashStatus(long chequeRegisterId, int cashStatusCat) {
        return ConnectionAndStatementUtil.getT(
                String.format(getByChequeRegisterAndCashStatus, chequeRegisterId, cashStatusCat),
                this::buildObjectFromResultSet
        );
    }
}