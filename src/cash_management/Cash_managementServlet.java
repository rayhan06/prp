package cash_management;

import cheque_register.Cheque_registerDTO;
import common.BaseServlet;
import finance.CashStatusEnum;
import finance.CashTypeEnum;
import language.LC;
import language.LM;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import pb.ErrorMessage;
import permission.MenuConstants;
import sessionmanager.SessionConstants;
import user.UserDTO;
import util.CommonDTO;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;


@WebServlet("/Cash_managementServlet")
@MultipartConfig
public class Cash_managementServlet extends BaseServlet {
    private static final long serialVersionUID = 1L;
    public static final long PERMANENT_ADVANCE_AMOUNT = 1000L;
    public static Logger logger = Logger.getLogger(Cash_managementServlet.class);
    private final Cash_managementDAO cash_managementDAO = Cash_managementDAO.getInstance();

    @Override
    public String getTableName() {
        return Cash_managementDAO.getInstance().getTableName();
    }

    @Override
    public String getServletName() {
        return "Cash_managementServlet";
    }

    @Override
    public Cash_managementDAO getCommonDAOService() {
        return cash_managementDAO;
    }

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        String language = userDTO.languageID == SessionConstants.ENGLISH ? "English" : "Bangla";
        Cash_managementDTO cash_managementDTO;
        if (addFlag) {
            cash_managementDTO = new Cash_managementDTO();
        } else {
            cash_managementDTO = cash_managementDAO.getDTOFromID(Long.parseLong(request.getParameter("iD")));
        }
        long curTime = System.currentTimeMillis();
        long modifiedBy = userDTO.ID;
        String Value;
        cash_managementDTO.chequeRegisterId = Long.parseLong(Jsoup.clean(request.getParameter("chequeRegisterId"), Whitelist.simpleText()));
        cash_managementDTO.billRegisterId = Long.parseLong(Jsoup.clean(request.getParameter("billNumber"), Whitelist.simpleText()));
        cash_managementDTO.billDescription = Jsoup.clean(request.getParameter("billDescription"), Whitelist.simpleText());
        cash_managementDTO.billAmount = Integer.parseInt(Jsoup.clean(request.getParameter("billAmount"), Whitelist.simpleText()));
        Value = request.getParameter("permanentAdvancedExcluded");
        if (Value != null && !Value.equalsIgnoreCase("")) {
            cash_managementDTO.permanentAdvancedExcluded = Integer.parseInt(Jsoup.clean(Value, Whitelist.simpleText()));
        } else {
            throw new Exception(LM.getText(LC.CASH_MANAGEMENT_ADD_PERMANENTADVANCEDEXCLUDED, language) + " " + ErrorMessage.getInvalidMessage(language));
        }
        Value = request.getParameter("toBeGivenAdvancedExcluded");
        if (Value != null && !Value.equalsIgnoreCase("")) {
            cash_managementDTO.toBeGivenAdvancedExcluded = Integer.parseInt(Jsoup.clean(Value, Whitelist.simpleText()));
        } else {
            throw new Exception(LM.getText(LC.CASH_MANAGEMENT_ADD_TOBEGIVENADVANCEDEXCLUDED, language) + " " + ErrorMessage.getInvalidMessage(language));
        }
        Value = request.getParameter("miscellaneous");
        if (Value != null && !Value.equalsIgnoreCase("")) {
            cash_managementDTO.miscellaneous = Integer.parseInt(Jsoup.clean(Value, Whitelist.simpleText()));
        } else {
            throw new Exception(LM.getText(LC.CASH_MANAGEMENT_ADD_MISCELLANEOUS, language) + " " + ErrorMessage.getInvalidMessage(language));
        }
        cash_managementDTO.cashStatusCat = Integer.parseInt(Jsoup.clean(request.getParameter("cashStatusCat"), Whitelist.simpleText()));
        cash_managementDTO.insertedBy = modifiedBy;
        cash_managementDTO.modifiedBy = modifiedBy;
        cash_managementDTO.insertionTime = curTime;
        cash_managementDTO.lastModificationTime = curTime;
        if (addFlag)
            cash_managementDAO.add(cash_managementDTO);
        else
            cash_managementDAO.update(cash_managementDTO);
        return cash_managementDTO;
    }

    public static void addCashRegister(Cheque_registerDTO chequeRegisterDTO, CashStatusEnum cashStatusEnum, boolean addFlag) throws Exception {
        Cash_managementDTO cashManagementDTO;
        if (addFlag) {
            cashManagementDTO = new Cash_managementDTO();
            cashManagementDTO.chequeRegisterId = chequeRegisterDTO.iD;
            cashManagementDTO.cashStatusCat = cashStatusEnum.getValue();
            cashManagementDTO.billRegisterId = chequeRegisterDTO.billRegisterId;
            cashManagementDTO.billDescription = chequeRegisterDTO.billDescription;
            cashManagementDTO.billAmount = chequeRegisterDTO.billAmount;
            cashManagementDTO.toBeGivenAdvancedExcluded = 0;
            cashManagementDTO.permanentAdvancedExcluded = 0;
            cashManagementDTO.insertionTime = chequeRegisterDTO.lastModificationTime;
            cashManagementDTO.insertedBy = chequeRegisterDTO.modifiedBy;
        } else {
            cashManagementDTO = Cash_managementDAO.getInstance()
                                                  .getDTOByChequeRegisterAndCashStatus(chequeRegisterDTO.iD, cashStatusEnum.getValue());
        }
        cashManagementDTO.lastModificationTime = chequeRegisterDTO.lastModificationTime;
        cashManagementDTO.modifiedBy = chequeRegisterDTO.modifiedBy;

        cashManagementDTO.cashType = chequeRegisterDTO.cashType;
        if (chequeRegisterDTO.cashType == CashTypeEnum.OTHERS.getValue()) {
            cashManagementDTO.miscellaneous = (int) chequeRegisterDTO.chequeAmount;
        } else if (chequeRegisterDTO.cashType == CashTypeEnum.SALARY.getValue()) {
            cashManagementDTO.salaryAmount = chequeRegisterDTO.chequeAmount;
        } else if (chequeRegisterDTO.cashType == CashTypeEnum.ALLOWANCE.getValue()) {
            cashManagementDTO.allowanceAmount = chequeRegisterDTO.chequeAmount;
        }

        if (addFlag)
            Cash_managementDAO.getInstance().add(cashManagementDTO);
        else
            Cash_managementDAO.getInstance().update(cashManagementDTO);
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[]{MenuConstants.CASH_MANAGEMENT_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[]{MenuConstants.CASH_MANAGEMENT_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[]{MenuConstants.CASH_MANAGEMENT_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Cash_managementServlet.class;
    }
}