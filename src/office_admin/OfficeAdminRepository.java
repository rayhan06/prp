package office_admin;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class OfficeAdminRepository implements Repository {
	OfficeAdminDAO officeHeadManagerDAO = new OfficeAdminDAO();
	
	
	static Logger logger = Logger.getLogger(OfficeAdminRepository.class);
	Map<Long, OfficeAdminDTO>mapOfEmployee_recordsDTOToiD;
	Map<String, Set<OfficeAdminDTO> >mapOfEmployee_recordsDTOTonameEng;
	Map<String, Set<OfficeAdminDTO> >mapOfEmployee_recordsDTOTonameBng;
	Map<String, Set<OfficeAdminDTO> >mapOfEmployee_recordsDTOTopersonalMobile;
	Map<String, Set<OfficeAdminDTO> >mapOfEmployee_recordsDTOTopersonalEmail;
	Map<String, Set<OfficeAdminDTO> >mapOfEmployee_recordsDTOToemployeeRecordId;
	Map<String, Set<OfficeAdminDTO> >mapOfEmployee_recordsDTOToemployeeRecordId_6;
	Map<String, Set<OfficeAdminDTO> >mapOfEmployee_recordsDTOToofficeUnitId;


	static OfficeAdminRepository instance = null;
	private OfficeAdminRepository(){
		mapOfEmployee_recordsDTOToiD = new ConcurrentHashMap<>();
		mapOfEmployee_recordsDTOTonameEng = new ConcurrentHashMap<>();
		mapOfEmployee_recordsDTOTonameBng = new ConcurrentHashMap<>();
		mapOfEmployee_recordsDTOTopersonalMobile = new ConcurrentHashMap<>();
		mapOfEmployee_recordsDTOTopersonalEmail = new ConcurrentHashMap<>();
		mapOfEmployee_recordsDTOToemployeeRecordId = new ConcurrentHashMap<>();
		mapOfEmployee_recordsDTOToemployeeRecordId_6 = new ConcurrentHashMap<>();
		mapOfEmployee_recordsDTOToofficeUnitId = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static OfficeAdminRepository getInstance(){
		if (instance == null){
			instance = new OfficeAdminRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		try {
			List<OfficeAdminDTO> officeHeadManagerDTOS = officeHeadManagerDAO.getAllEmployee_records(reloadAll);
			for(OfficeAdminDTO officeHeadManagerDTO : officeHeadManagerDTOS) {
				OfficeAdminDTO oldOfficeHeadManagerDTO = getEmployee_recordsDTOByID(officeHeadManagerDTO.iD);
				if( oldOfficeHeadManagerDTO != null ) {
					mapOfEmployee_recordsDTOToiD.remove(oldOfficeHeadManagerDTO.iD);
				
					if(mapOfEmployee_recordsDTOTonameEng.containsKey(oldOfficeHeadManagerDTO.nameEng)) {
						mapOfEmployee_recordsDTOTonameEng.get(oldOfficeHeadManagerDTO.nameEng).remove(oldOfficeHeadManagerDTO);
					}
					if(mapOfEmployee_recordsDTOTonameEng.get(oldOfficeHeadManagerDTO.nameEng).isEmpty()) {
						mapOfEmployee_recordsDTOTonameEng.remove(oldOfficeHeadManagerDTO.nameEng);
					}
					
					if(mapOfEmployee_recordsDTOTonameBng.containsKey(oldOfficeHeadManagerDTO.nameBng)) {
						mapOfEmployee_recordsDTOTonameBng.get(oldOfficeHeadManagerDTO.nameBng).remove(oldOfficeHeadManagerDTO);
					}
					if(mapOfEmployee_recordsDTOTonameBng.get(oldOfficeHeadManagerDTO.nameBng).isEmpty()) {
						mapOfEmployee_recordsDTOTonameBng.remove(oldOfficeHeadManagerDTO.nameBng);
					}
					
					if(mapOfEmployee_recordsDTOTopersonalMobile.containsKey(oldOfficeHeadManagerDTO.personalMobile)) {
						mapOfEmployee_recordsDTOTopersonalMobile.get(oldOfficeHeadManagerDTO.personalMobile).remove(oldOfficeHeadManagerDTO);
					}
					if(mapOfEmployee_recordsDTOTopersonalMobile.get(oldOfficeHeadManagerDTO.personalMobile).isEmpty()) {
						mapOfEmployee_recordsDTOTopersonalMobile.remove(oldOfficeHeadManagerDTO.personalMobile);
					}
					
					if(mapOfEmployee_recordsDTOTopersonalEmail.containsKey(oldOfficeHeadManagerDTO.personalEmail)) {
						mapOfEmployee_recordsDTOTopersonalEmail.get(oldOfficeHeadManagerDTO.personalEmail).remove(oldOfficeHeadManagerDTO);
					}
					if(mapOfEmployee_recordsDTOTopersonalEmail.get(oldOfficeHeadManagerDTO.personalEmail).isEmpty()) {
						mapOfEmployee_recordsDTOTopersonalEmail.remove(oldOfficeHeadManagerDTO.personalEmail);
					}
					
					if(mapOfEmployee_recordsDTOToemployeeRecordId.containsKey(oldOfficeHeadManagerDTO.userName)) {
						mapOfEmployee_recordsDTOToemployeeRecordId.get(oldOfficeHeadManagerDTO.userName).remove(oldOfficeHeadManagerDTO);
					}
					if(mapOfEmployee_recordsDTOToemployeeRecordId.get(oldOfficeHeadManagerDTO.userName).isEmpty()) {
						mapOfEmployee_recordsDTOToemployeeRecordId.remove(oldOfficeHeadManagerDTO.userName);
					}
					
					if(mapOfEmployee_recordsDTOToemployeeRecordId_6.containsKey(oldOfficeHeadManagerDTO.designation)) {
						mapOfEmployee_recordsDTOToemployeeRecordId_6.get(oldOfficeHeadManagerDTO.designation).remove(oldOfficeHeadManagerDTO);
					}
					if(mapOfEmployee_recordsDTOToemployeeRecordId_6.get(oldOfficeHeadManagerDTO.designation).isEmpty()) {
						mapOfEmployee_recordsDTOToemployeeRecordId_6.remove(oldOfficeHeadManagerDTO.designation);
					}
					
					if(mapOfEmployee_recordsDTOToofficeUnitId.containsKey(oldOfficeHeadManagerDTO.unit)) {
						mapOfEmployee_recordsDTOToofficeUnitId.get(oldOfficeHeadManagerDTO.unit).remove(oldOfficeHeadManagerDTO);
					}
					if(mapOfEmployee_recordsDTOToofficeUnitId.get(oldOfficeHeadManagerDTO.unit).isEmpty()) {
						mapOfEmployee_recordsDTOToofficeUnitId.remove(oldOfficeHeadManagerDTO.unit);
					}
					
					
				}
				if(true)
				{
					
					mapOfEmployee_recordsDTOToiD.put(officeHeadManagerDTO.iD, officeHeadManagerDTO);
				
					if( ! mapOfEmployee_recordsDTOTonameEng.containsKey(officeHeadManagerDTO.nameEng)) {
						mapOfEmployee_recordsDTOTonameEng.put(officeHeadManagerDTO.nameEng, new HashSet<>());
					}
					mapOfEmployee_recordsDTOTonameEng.get(officeHeadManagerDTO.nameEng).add(officeHeadManagerDTO);
					
					if( ! mapOfEmployee_recordsDTOTonameBng.containsKey(officeHeadManagerDTO.nameBng)) {
						mapOfEmployee_recordsDTOTonameBng.put(officeHeadManagerDTO.nameBng, new HashSet<>());
					}
					mapOfEmployee_recordsDTOTonameBng.get(officeHeadManagerDTO.nameBng).add(officeHeadManagerDTO);
					
					if( ! mapOfEmployee_recordsDTOTopersonalMobile.containsKey(officeHeadManagerDTO.personalMobile)) {
						mapOfEmployee_recordsDTOTopersonalMobile.put(officeHeadManagerDTO.personalMobile, new HashSet<>());
					}
					mapOfEmployee_recordsDTOTopersonalMobile.get(officeHeadManagerDTO.personalMobile).add(officeHeadManagerDTO);
					
					if( ! mapOfEmployee_recordsDTOTopersonalEmail.containsKey(officeHeadManagerDTO.personalEmail)) {
						mapOfEmployee_recordsDTOTopersonalEmail.put(officeHeadManagerDTO.personalEmail, new HashSet<>());
					}
					mapOfEmployee_recordsDTOTopersonalEmail.get(officeHeadManagerDTO.personalEmail).add(officeHeadManagerDTO);
					
					if( ! mapOfEmployee_recordsDTOToemployeeRecordId.containsKey(officeHeadManagerDTO.userName)) {
						mapOfEmployee_recordsDTOToemployeeRecordId.put(officeHeadManagerDTO.userName, new HashSet<>());
					}
					mapOfEmployee_recordsDTOToemployeeRecordId.get(officeHeadManagerDTO.userName).add(officeHeadManagerDTO);
					
					if( ! mapOfEmployee_recordsDTOToemployeeRecordId_6.containsKey(officeHeadManagerDTO.designation)) {
						mapOfEmployee_recordsDTOToemployeeRecordId_6.put(officeHeadManagerDTO.designation, new HashSet<>());
					}
					mapOfEmployee_recordsDTOToemployeeRecordId_6.get(officeHeadManagerDTO.designation).add(officeHeadManagerDTO);
					
					if( ! mapOfEmployee_recordsDTOToofficeUnitId.containsKey(officeHeadManagerDTO.unit)) {
						mapOfEmployee_recordsDTOToofficeUnitId.put(officeHeadManagerDTO.unit, new HashSet<>());
					}
					mapOfEmployee_recordsDTOToofficeUnitId.get(officeHeadManagerDTO.unit).add(officeHeadManagerDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<OfficeAdminDTO> getEmployee_recordsList() {
		List <OfficeAdminDTO> employee_recordss = new ArrayList<OfficeAdminDTO>(this.mapOfEmployee_recordsDTOToiD.values());
		return employee_recordss;
	}
	
	
	public OfficeAdminDTO getEmployee_recordsDTOByID(long ID){
		return mapOfEmployee_recordsDTOToiD.get(ID);
	}
	
	
	public List<OfficeAdminDTO> getEmployee_recordsDTOByname_eng(String name_eng) {
		return new ArrayList<>( mapOfEmployee_recordsDTOTonameEng.getOrDefault(name_eng,new HashSet<>()));
	}
	
	
	public List<OfficeAdminDTO> getEmployee_recordsDTOByname_bng(String name_bng) {
		return new ArrayList<>( mapOfEmployee_recordsDTOTonameBng.getOrDefault(name_bng,new HashSet<>()));
	}
	
	
	public List<OfficeAdminDTO> getEmployee_recordsDTOBypersonal_mobile(String personal_mobile) {
		return new ArrayList<>( mapOfEmployee_recordsDTOTopersonalMobile.getOrDefault(personal_mobile,new HashSet<>()));
	}
	
	
	public List<OfficeAdminDTO> getEmployee_recordsDTOBypersonal_email(String personal_email) {
		return new ArrayList<>( mapOfEmployee_recordsDTOTopersonalEmail.getOrDefault(personal_email,new HashSet<>()));
	}
	
	
	public List<OfficeAdminDTO> getEmployee_recordsDTOByemployee_record_id(int employee_record_id) {
		return new ArrayList<>( mapOfEmployee_recordsDTOToemployeeRecordId.getOrDefault(employee_record_id,new HashSet<>()));
	}
	
	
	public List<OfficeAdminDTO> getEmployee_recordsDTOByemployee_record_id_6(int employee_record_id) {
		return new ArrayList<>( mapOfEmployee_recordsDTOToemployeeRecordId_6.getOrDefault(employee_record_id,new HashSet<>()));
	}
	
	
	public List<OfficeAdminDTO> getEmployee_recordsDTOByoffice_unit_id(int office_unit_id) {
		return new ArrayList<>( mapOfEmployee_recordsDTOToofficeUnitId.getOrDefault(office_unit_id,new HashSet<>()));
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "office_admin";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


