package office_admin;
import java.util.*; 


public class OfficeAdminMAPS
{

	public HashMap<String, String> java_allfield_type_map = new HashMap<String, String>();
	public HashMap<String, String> java_anyfield_search_map = new HashMap<String, String>();
	public HashMap<String, String> java_DTO_map = new HashMap<String, String>();
	public HashMap<String, String> java_SQL_map = new HashMap<String, String>();
	public HashMap<String, String> java_Text_map = new HashMap<String, String>();
	
	private static OfficeAdminMAPS self = null;
	
	private OfficeAdminMAPS()
	{
		
		java_allfield_type_map.put("name_eng".toLowerCase(), "String");
		java_allfield_type_map.put("name_bng".toLowerCase(), "String");
		java_allfield_type_map.put("personal_mobile".toLowerCase(), "String");
		java_allfield_type_map.put("personal_email".toLowerCase(), "String");
		java_allfield_type_map.put("employee_record_id".toLowerCase(), "Integer");
		java_allfield_type_map.put("employee_record_id".toLowerCase(), "Integer");
		java_allfield_type_map.put("office_unit_id".toLowerCase(), "Integer");

		java_anyfield_search_map.put("employee_records.name_eng".toLowerCase(), "String");
		java_anyfield_search_map.put("employee_records.name_bng".toLowerCase(), "String");
		java_anyfield_search_map.put("employee_records.personal_mobile".toLowerCase(), "String");
		java_anyfield_search_map.put("employee_records.personal_email".toLowerCase(), "String");
		java_anyfield_search_map.put("users.username".toLowerCase(), "String");
		java_anyfield_search_map.put("users.username".toLowerCase(), "String");
		java_anyfield_search_map.put("employee_offices.designation".toLowerCase(), "String");
		java_anyfield_search_map.put("employee_offices.designation".toLowerCase(), "String");
		java_anyfield_search_map.put("office_units.unit_name_bng".toLowerCase(), "String");
		java_anyfield_search_map.put("office_units.unit_name_eng".toLowerCase(), "String");

		java_DTO_map.put("id".toLowerCase(), "id".toLowerCase());
		java_DTO_map.put("nameEng".toLowerCase(), "nameEng".toLowerCase());
		java_DTO_map.put("nameBng".toLowerCase(), "nameBng".toLowerCase());
		java_DTO_map.put("personalMobile".toLowerCase(), "personalMobile".toLowerCase());
		java_DTO_map.put("personalEmail".toLowerCase(), "personalEmail".toLowerCase());
		java_DTO_map.put("userName".toLowerCase(), "userName".toLowerCase());
		java_DTO_map.put("designation".toLowerCase(), "designation".toLowerCase());
		java_DTO_map.put("unit".toLowerCase(), "unit".toLowerCase());

		java_SQL_map.put("name_eng".toLowerCase(), "nameEng".toLowerCase());
		java_SQL_map.put("name_bng".toLowerCase(), "nameBng".toLowerCase());
		java_SQL_map.put("personal_mobile".toLowerCase(), "personalMobile".toLowerCase());
		java_SQL_map.put("personal_email".toLowerCase(), "personalEmail".toLowerCase());
		java_SQL_map.put("employee_record_id".toLowerCase(), "userName".toLowerCase());
		java_SQL_map.put("employee_record_id".toLowerCase(), "designation".toLowerCase());
		java_SQL_map.put("office_unit_id".toLowerCase(), "unit".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "id".toLowerCase());
		java_Text_map.put("Name (English)".toLowerCase(), "nameEng".toLowerCase());
		java_Text_map.put("Name (Bangla)".toLowerCase(), "nameBng".toLowerCase());
		java_Text_map.put("Personal Mobile".toLowerCase(), "personalMobile".toLowerCase());
		java_Text_map.put("Personal Email".toLowerCase(), "personalEmail".toLowerCase());
		java_Text_map.put("Employee Record Id".toLowerCase(), "userName".toLowerCase());
		java_Text_map.put("Employee Record Id".toLowerCase(), "designation".toLowerCase());
		java_Text_map.put("Office Unit Id".toLowerCase(), "unit".toLowerCase());
			
	}
	
	public static OfficeAdminMAPS GetInstance()
	{
		if(self == null)
		{
			self = new OfficeAdminMAPS();
		}
		return self;
	}
	

}