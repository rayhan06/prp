package office_admin;

import dbm.DBMR;
import dbm.DBMW;
import login.LoginDTO;
import org.apache.log4j.Logger;
import repository.RepositoryManager;
import util.NavigationService;

import java.sql.*;
import java.util.*;


public class OfficeAdminDAO implements NavigationService {

    public int ministry_id;
    public int office_layer;
    public int office_origin;
    public int office;
    public int page_number;
    public String id;

    Logger logger = Logger.getLogger(getClass());

    private void printSql(String sql) {
        logger.debug("sql: " + sql);
    }

    private void recordUpdateTime(Connection connection, long lastModificationTime) throws SQLException {
        recordUpdateTime(connection,"office_admin",lastModificationTime);
    }

    public long addEmployee_records(OfficeAdminDTO officeHeadManagerDTO) throws Exception {

        Connection connection = null;
        PreparedStatement ps = null;

        long lastModificationTime = System.currentTimeMillis();

        try {
            connection = DBMW.getInstance().getConnection();

            if (connection == null) {
                logger.debug("nullconn");
            }

            officeHeadManagerDTO.iD = DBMW.getInstance().getNextSequenceId("Employee_records");

            String sql = "INSERT INTO employee_records";

            sql += " (";
            sql += "ID";
            sql += ", ";
            sql += "name_eng";
            sql += ", ";
            sql += "name_bng";
            sql += ", ";
            sql += "personal_mobile";
            sql += ", ";
            sql += "personal_email";
            sql += ", ";
            sql += "employee_record_id";
            sql += ", ";
            sql += "employee_record_id";
            sql += ", ";
            sql += "office_unit_id";
            sql += ", lastModificationTime)";


            sql += " VALUES(";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ?)";

            printSql(sql);

            ps = connection.prepareStatement(sql);


            int index = 1;

            ps.setObject(index++, officeHeadManagerDTO.iD);
            ps.setObject(index++, officeHeadManagerDTO.nameEng);
            ps.setObject(index++, officeHeadManagerDTO.nameBng);
            ps.setObject(index++, officeHeadManagerDTO.personalMobile);
            ps.setObject(index++, officeHeadManagerDTO.personalEmail);
            ps.setObject(index++, officeHeadManagerDTO.userName);
            ps.setObject(index++, officeHeadManagerDTO.designation);
            ps.setObject(index++, officeHeadManagerDTO.unit);
            ps.setObject(index++, lastModificationTime);

            logger.debug(ps);
            ps.execute();


            recordUpdateTime(connection, lastModificationTime);

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (connection != null) {
                    DBMW.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
        return officeHeadManagerDTO.iD;
    }

    //need another getter for repository
    public OfficeAdminDTO getEmployee_recordsDTOByID(long ID) throws Exception {
        Connection connection = null;
        ResultSet rs = null;
        Statement stmt = null;
        OfficeAdminDTO officeHeadManagerDTO = null;
        try {

            String sql = "SELECT * ";

            sql += " FROM employee_records";

            sql += " WHERE ID=" + ID;

            printSql(sql);

            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();


            rs = stmt.executeQuery(sql);

            if (rs.next()) {
                officeHeadManagerDTO = new OfficeAdminDTO();

                officeHeadManagerDTO.iD = rs.getLong("ID");
                officeHeadManagerDTO.nameEng = rs.getString("name_eng");
                officeHeadManagerDTO.nameBng = rs.getString("name_bng");
                officeHeadManagerDTO.personalMobile = rs.getString("personal_mobile");
                officeHeadManagerDTO.personalEmail = rs.getString("personal_email");
            }


        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }

            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
        return officeHeadManagerDTO;
    }

    public long updateEmployee_records(OfficeAdminDTO officeHeadManagerDTO) throws Exception {

        Connection connection = null;
        PreparedStatement ps = null;

        long lastModificationTime = System.currentTimeMillis();
        try {
            connection = DBMW.getInstance().getConnection();

            String sql = "UPDATE employee_records";

            sql += " SET ";
            sql += "name_eng=?";
            sql += ", ";
            sql += "name_bng=?";
            sql += ", ";
            sql += "personal_mobile=?";
            sql += ", ";
            sql += "personal_email=?";
            sql += ", ";
            sql += "employee_record_id=?";
            sql += ", ";
            sql += "employee_record_id=?";
            sql += ", ";
            sql += "office_unit_id=?";
            sql += ", lastModificationTime = " + lastModificationTime + "";
            sql += " WHERE ID = " + officeHeadManagerDTO.iD;

            printSql(sql);

            ps = connection.prepareStatement(sql);


            int index = 1;

            ps.setObject(index++, officeHeadManagerDTO.nameEng);
            ps.setObject(index++, officeHeadManagerDTO.nameBng);
            ps.setObject(index++, officeHeadManagerDTO.personalMobile);
            ps.setObject(index++, officeHeadManagerDTO.personalEmail);
            ps.setObject(index++, officeHeadManagerDTO.userName);
            ps.setObject(index++, officeHeadManagerDTO.designation);
            ps.setObject(index++, officeHeadManagerDTO.unit);
            logger.debug(ps);
            ps.executeUpdate();


            recordUpdateTime(connection, lastModificationTime);


        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (connection != null) {
                    DBMW.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
        return officeHeadManagerDTO.iD;
    }

    public void deleteEmployee_recordsByID(long ID) throws Exception {
        Connection connection = null;
        Statement stmt = null;
        PreparedStatement ps = null;

        long lastModificationTime = System.currentTimeMillis();
        try {
            String sql = "UPDATE employee_records";

            sql += " SET isDeleted=1,lastModificationTime=" + lastModificationTime + " WHERE ID = " + ID;

            printSql(sql);

            connection = DBMW.getInstance().getConnection();
            stmt = connection.createStatement();
            stmt.execute(sql);


            recordUpdateTime(connection, lastModificationTime);


        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }

            try {
                if (connection != null) {
                    DBMW.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
    }

    public List<OfficeAdminDTO> getDTOs(Collection recordIDs, LoginDTO loginDTO) {
        Connection connection = null;
        ResultSet rs = null;
        Statement stmt = null;
        OfficeAdminDTO officeHeadManagerDTO = null;
        List<OfficeAdminDTO> officeHeadManagerDTOList = new ArrayList<>();
        if (recordIDs.isEmpty()) {
            return officeHeadManagerDTOList;
        }
        try {

            String sql1 = "SELECT * ";

            sql1 += " FROM employee_records";

            sql1 += " WHERE ID IN ( ";

            for (int i = 0; i < recordIDs.size(); i++) {
                if (i != 0) {
                    sql1 += ",";
                }
                sql1 += ((ArrayList) recordIDs).get(i);
            }
            sql1 += ")  order by ID desc";

            printSql(sql1);

            String sql = "SELECT distinct employee_records.ID as ID, " +
                    " employee_records.name_eng as name_eng, " +
                    " employee_records.name_bng as name_bng, " +
                    " employee_records.personal_mobile as personal_mobile, " +
                    " employee_records.personal_email as personal_email, " +
                    " employee_records.personal_email as personal_email, " +
                    " users.username as userName, " +
                    " employee_offices.designation as designation, " +
                    " office_units.unit_name_bng as unit, " +
                    " employee_offices.office_unit_organogram_id as officeUnitOrganogramId, " +
                    " office_unit_organograms.office_id as officeId, " +
                    " office_unit_organograms.office_unit_id as officeUnitId, " +
                    " office_unit_organograms.is_admin as isAdmin " +
                    " FROM employee_records ";
            sql += " join users on employee_records.ID = users.employee_record_id ";
            sql += " join employee_offices on employee_records.ID = employee_offices.employee_record_id ";
            sql += " join office_units on employee_offices.office_unit_id = office_units.id  ";
            sql += " join office_unit_organograms on office_unit_organograms.id = employee_offices.office_unit_organogram_id  ";
            sql += " join offices on offices.id = employee_offices.office_id ";
            sql += " where offices.office_ministry_id = " + ministry_id +
                    " and offices.office_layer_id = " + office_layer +
                    " and offices.office_origin_id = " + office_origin +
                    " and offices.id = " + office +
                    " and employee_records.ID IN (";

            for (int i = 0; i < recordIDs.size(); i++) {
                if (i != 0) {
                    sql += ",";
                }
                sql += ((ArrayList) recordIDs).get(i);
            }
            sql += ")  order by ID desc";

            logger.debug("sql " + sql);
            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();


            rs = stmt.executeQuery(sql);

            while (rs.next()) {
                officeHeadManagerDTO = new OfficeAdminDTO();
                officeHeadManagerDTO.iD = rs.getLong("ID");
                officeHeadManagerDTO.nameEng = rs.getString("name_eng");
                officeHeadManagerDTO.nameBng = rs.getString("name_bng");
                officeHeadManagerDTO.personalMobile = rs.getString("personal_mobile");
                officeHeadManagerDTO.personalEmail = rs.getString("personal_email");
                officeHeadManagerDTO.userName = rs.getString("userName");
                officeHeadManagerDTO.designation = rs.getString("designation");
                officeHeadManagerDTO.unit = rs.getString("unit");
                officeHeadManagerDTO.officeUnitOrganogramId = rs.getInt("officeUnitOrganogramId");
                officeHeadManagerDTO.officeId = rs.getInt("officeId");
                officeHeadManagerDTO.officeUnitId = rs.getInt("officeUnitId");
                officeHeadManagerDTO.isAdmin = rs.getInt("isAdmin");
                logger.debug("got this DTO: " + officeHeadManagerDTO);

                officeHeadManagerDTOList.add(officeHeadManagerDTO);

            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }

            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
        return officeHeadManagerDTOList;

    }

    public Collection getIDs(LoginDTO loginDTO) {
        Collection data = new ArrayList();
        Connection connection = null;
        Statement stmt = null;
        ResultSet resultSet = null;

        String sql = "SELECT distinct employee_records.ID as ID from employee_records";

        sql += " join users on employee_records.ID = users.employee_record_id ";
        sql += " join employee_offices on employee_records.ID = employee_offices.employee_record_id ";
        sql += " join office_units on employee_offices.office_unit_id = office_units.id  ";
        sql += " join office_unit_organograms on office_unit_organograms.id = employee_offices.office_unit_organogram_id  ";
        sql += " join offices on offices.id = employee_offices.office_id ";
        sql += " where offices.office_ministry_id = " + ministry_id +
                " and offices.office_layer_id = " + office_layer +
                " and offices.office_origin_id = " + office_origin +
                " and offices.id = " + office;

        printSql(sql);

        try {
            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();

            for (resultSet = stmt.executeQuery(sql); resultSet.next(); data.add(resultSet.getString("ID"))) ;

            resultSet.close();
        } catch (Exception e) {
            logger.fatal("DAO " + e, e);
        } finally {
            try {
                if (resultSet != null && !resultSet.isClosed()) {
                    resultSet.close();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception e) {
                logger.fatal("DAO finally :" + e);
            }
        }
        return data;
    }

    //add repository
    public List<OfficeAdminDTO> getAllEmployee_records(boolean isFirstReload) {
        List<OfficeAdminDTO> officeHeadManagerDTOList = new ArrayList<>();

        String sql = "SELECT * FROM employee_records";
        sql += " WHERE ";


        if (isFirstReload) {
            sql += " isDeleted =  0";
        }
        if (!isFirstReload) {
            sql += " lastModificationTime >= " + RepositoryManager.lastModifyTime;
        }
        printSql(sql);

        Connection connection = null;
        Statement stmt = null;
        ResultSet rs = null;


        try {
            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();
            rs = stmt.executeQuery(sql);


            while (rs.next()) {
                OfficeAdminDTO officeHeadManagerDTO = new OfficeAdminDTO();
                officeHeadManagerDTO.iD = rs.getLong("ID");
                officeHeadManagerDTO.nameEng = rs.getString("name_eng");
                officeHeadManagerDTO.nameBng = rs.getString("name_bng");
                officeHeadManagerDTO.personalMobile = rs.getString("personal_mobile");
                officeHeadManagerDTO.personalEmail = rs.getString("personal_email");
                int i = 0;
                long primaryKey = officeHeadManagerDTO.iD;
                while (i < officeHeadManagerDTOList.size() && officeHeadManagerDTOList.get(i).iD < primaryKey) {
                    i++;
                }
                officeHeadManagerDTOList.add(i, officeHeadManagerDTO);
                //officeHeadManagerDTOList.add(officeHeadManagerDTO);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }

        return officeHeadManagerDTOList;
    }

    public Collection getIDsWithSearchCriteria(Hashtable p_searchCriteria, LoginDTO loginDTO) throws Exception {
        logger.debug("table: " + p_searchCriteria);
        List<Long> idList = new ArrayList<>();
        Connection connection = null;
        PreparedStatement ps = null;

        try {

            String sql = "SELECT distinct employee_records.ID as ID from employee_records";

            sql += " join users on employee_records.ID = users.employee_record_id ";
            sql += " join employee_offices on employee_records.ID = employee_offices.employee_record_id ";
            sql += " join office_units on employee_offices.office_unit_id = office_units.id  ";
            sql += " join office_unit_organograms on office_unit_organograms.id = employee_offices.office_unit_organogram_id  ";
            sql += " join offices on offices.id = employee_offices.office_id ";
            sql += " where offices.office_ministry_id = " + ministry_id +
                    " and offices.office_layer_id = " + office_layer +
                    " and offices.office_origin_id = " + office_origin +
                    " and offices.id = " + office;

            String anyFieldSQL = "(";
            if (p_searchCriteria.get("AnyField") != null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase("")) {
                boolean i = false;
                Iterator it = OfficeAdminMAPS.GetInstance().java_anyfield_search_map.entrySet().iterator();
                while (it.hasNext()) {
                    if (i) {
                        anyFieldSQL += " OR  ";
                    }
                    Map.Entry pair = (Map.Entry) it.next();
                    anyFieldSQL += pair.getKey() + " like '%" + p_searchCriteria.get("AnyField").toString() + "%'";
                    i = true;
                }
            }
            anyFieldSQL += ")";

            String AllFieldSql = "(";
            int i = 0;
            Enumeration names = p_searchCriteria.keys();
            String str, value;

            while (names.hasMoreElements()) {
                str = (String) names.nextElement();
                value = (String) p_searchCriteria.get(str);
                logger.debug(str + ": " + value);
                if (OfficeAdminMAPS.GetInstance().java_allfield_type_map.get(str.toLowerCase()) != null && !OfficeAdminMAPS.GetInstance().java_allfield_type_map.get(str.toLowerCase()).equalsIgnoreCase("")
                        && !str.equalsIgnoreCase("AnyField")
                        && value != null && !value.equalsIgnoreCase("")) {
                    if (p_searchCriteria.get(str).equals("any")) {
                        continue;
                    }

                    if (i > 0) {
                        AllFieldSql += " AND  ";
                    }
                    if (OfficeAdminMAPS.GetInstance().java_allfield_type_map.get(str.toLowerCase()).equals("String")) {
                        AllFieldSql += "employee_records." + str.toLowerCase() + " like '%" + p_searchCriteria.get(str) + "%'";
                    } else {
                        AllFieldSql += "employee_records." + str.toLowerCase() + " = '" + p_searchCriteria.get(str) + "'";
                    }
                    i++;
                }
            }

            AllFieldSql += ")";

            logger.debug("AllFieldSql = " + AllFieldSql);

            if (!anyFieldSQL.equals("()"))
                sql += " AND " + anyFieldSQL;
            if (!AllFieldSql.equals("()")) {
                sql += " AND " + AllFieldSql;
            }
            sql += " order by employee_records.ID desc ";

            printSql(sql);

            connection = DBMR.getInstance().getConnection();
            ps = connection.prepareStatement(sql);

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                idList.add(rs.getLong("ID"));
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
        return idList;
    }

    public boolean updateAsAdmin(int officeUnitOrganogramId, int officeId, int officeUnitId) {
        Connection connection = null;
        Statement stmt = null;

        if (removeAsAdmin(officeUnitOrganogramId, officeId, officeUnitId)) {
            try {
                String sql = "update office_unit_organograms set is_admin = 1 where id = " + officeUnitOrganogramId + ";";
                printSql(sql);

                connection = DBMW.getInstance().getConnection();
                stmt = connection.createStatement();
                stmt.execute(sql);

            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                try {
                    if (stmt != null) {
                        stmt.close();
                    }
                } catch (Exception e) {
                }
                try {
                    if (connection != null) {
                        DBMW.getInstance().freeConnection(connection);
                    }
                } catch (Exception ex2) {
                }
            }
            return true;
        }
        return false;
    }

    private boolean removeAsAdmin(int officeUnitOrganogramId, int officeId, int officeUnitId) {
        Connection connection = null;
        Statement stmt = null;

        try {
            String sql = "update office_unit_organograms set is_admin = 0 where office_id = " + officeId + " AND is_admin = 1;";
            printSql(sql);

            connection = DBMW.getInstance().getConnection();
            stmt = connection.createStatement();
            stmt.execute(sql);

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (connection != null) {
                    DBMW.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
        return true;
    }
}
	