package office_admin;


public class OfficeAdminDTO {

    public long iD = 0;
    public String nameEng = "";
    public String nameBng = "";
    public String personalMobile = "";
    public String personalEmail = "";
    public String userName = "";
    public String designation = "";
    public String unit = "";
    public int officeUnitOrganogramId = 0;
    public int officeId = 0;
    public int officeUnitId = 0;
    public int isAdmin = 0;

    @Override
    public String toString() {
        return "$OfficeAdminDTO[" +
                " id = " + iD +
                " nameEng = " + nameEng +
                " nameBng = " + nameBng +
                " personalMobile = " + personalMobile +
                " personalEmail = " + personalEmail +
                " userName = " + userName +
                " designation = " + designation +
                " unit = " + unit +
                "]";
    }

}