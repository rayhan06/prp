package approval_test;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import sessionmanager.SessionConstants;
import java.sql.SQLException;
import common.ConnectionAndStatementUtil;
import org.apache.log4j.Logger;
import repository.RepositoryManager;
import util.*;
import pb.*;
import user.UserDTO;

public class ApprovalTestBabyDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());

	public ApprovalTestBabyDAO(String tableName)
	{
		super(tableName);
		useSafeSearch = true;
		joinSQL = "";
		columnNames = new String[] 
		{
			"ID",
			"approval_test_id",
			"inscription",
			"a_number",
			"isDeleted",
			"lastModificationTime"
		};
	}
	
	public ApprovalTestBabyDAO()
	{
		this("approval_test_baby");		
	}
	
	public void setSearchColumn(ApprovalTestBabyDTO approvaltestbabyDTO)
	{
		approvaltestbabyDTO.searchColumn = "";
		approvaltestbabyDTO.searchColumn += approvaltestbabyDTO.inscription + " ";
	}
	
	public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException
	{
		ApprovalTestBabyDTO approvaltestbabyDTO = (ApprovalTestBabyDTO)commonDTO;
		int index = 1;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(approvaltestbabyDTO);
		if(isInsert)
		{
			ps.setObject(index++,approvaltestbabyDTO.iD);
		}
		ps.setObject(index++,approvaltestbabyDTO.approvalTestId);
		ps.setObject(index++,approvaltestbabyDTO.inscription);
		ps.setObject(index++,approvaltestbabyDTO.aNumber);
		if(isInsert)
		{
			ps.setObject(index++, 0);
			ps.setObject(index++, lastModificationTime);
		}
	}
	
	public ApprovalTestBabyDTO build(ResultSet rs)
	{
		try
		{
			ApprovalTestBabyDTO approvaltestbabyDTO = new ApprovalTestBabyDTO();
			approvaltestbabyDTO.iD = rs.getLong("ID");
			approvaltestbabyDTO.approvalTestId = rs.getLong("approval_test_id");
			approvaltestbabyDTO.inscription = rs.getString("inscription");
			approvaltestbabyDTO.aNumber = rs.getInt("a_number");
			approvaltestbabyDTO.isDeleted = rs.getInt("isDeleted");
			approvaltestbabyDTO.lastModificationTime = rs.getLong("lastModificationTime");
			return approvaltestbabyDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
		

				
}
	