package approval_test;

import java.io.IOException;
import java.io.*;
import java.text.SimpleDateFormat;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import org.apache.log4j.Logger;

import login.LoginDTO;
import permission.MenuConstants;
import role.PermissionRepository;


import sessionmanager.SessionConstants;

import user.UserDTO;
import user.UserRepository;
import util.*;
import java.util.*;
import javax.servlet.http.*;
import java.util.UUID;


import geolocation.GeoLocationDAO2;
import language.LC;
import language.LM;
import common.ApiResponse;
import com.google.gson.Gson;
import pb.*;
import pbReport.*;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;

import workflow.WorkflowController;
import approval_execution_table.*;
import pb_notifications.Pb_notificationsDAO;



/**
 * Servlet implementation class Approval_testServlet
 */
@WebServlet("/Approval_testServlet")
@MultipartConfig
public class Approval_testServlet extends HttpServlet 
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Approval_testServlet.class);

    String tableName = "approval_test";

	Approval_testDAO approval_testDAO;
	CommonRequestHandler commonRequestHandler;
	ApprovalTestBabyDAO approvalTestBabyDAO;
    private final Gson gson = new Gson();
    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Approval_testServlet() 
	{
        super();
    	try
    	{
			approval_testDAO = new Approval_testDAO(tableName);
			approvalTestBabyDAO = new ApprovalTestBabyDAO("approval_test_baby");
			commonRequestHandler = new CommonRequestHandler(approval_testDAO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }   

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		System.out.println("In doget request = " + request);
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		try
		{
			String actionType = request.getParameter("actionType");
			if(actionType.equals("getAddPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.APPROVAL_TEST_ADD))
				{
					commonRequestHandler.getAddPage(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getEditPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.APPROVAL_TEST_UPDATE))
				{
					getApproval_test(request, response);					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}						
			}
			else if(actionType.equals("getURL"))
			{
				String URL = request.getParameter("URL");
				System.out.println("URL = " + URL);
				response.sendRedirect(URL);			
			}			
			else if(actionType.equals("search"))
			{
				System.out.println("search requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.APPROVAL_TEST_SEARCH))
				{
					if(isPermanentTable)
					{
						String filter = request.getParameter("filter");
						System.out.println("filter = " + filter);
						if(filter!=null)
						{
							filter = ""; //shouldn't be directly used, rather manipulate it.
							searchApproval_test(request, response, isPermanentTable, filter);
						}
						else
						{
							searchApproval_test(request, response, isPermanentTable, "");
						}
					}					
					else
					{
						//searchApproval_test(request, response, tempTableName, isPermanentTable);
					}
				}			
			}
			else if(actionType.equals("getApprovalPage"))
			{
				System.out.println("Approval_test getApprovalPage requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.APPROVAL_TEST_APPROVE))
				{
					searchApproval_test(request, response, false, "");
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}				
			}
			else if(actionType.equals("viewApprovalNotification"))
			{
				System.out.println("view requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.APPROVAL_TEST_APPROVE))
				{
					commonRequestHandler.viewApprovalNotification(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}				
			}
			else if(actionType.equals("view"))
			{
				System.out.println("view requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.APPROVAL_TEST_SEARCH))
				{
					commonRequestHandler.view(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else
			{
				request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}


	
	

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		// TODO Auto-generated method stub
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		System.out.println("doPost");
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		
		try
		{
			String actionType = request.getParameter("actionType");
			System.out.println("actionType = " + actionType);
			if(actionType.equals("add"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.APPROVAL_TEST_ADD))
				{
					System.out.println("going to  addApproval_test ");
					addApproval_test(request, response, true, userDTO, true);
				}
				else
				{
					System.out.println("Not going to  addApproval_test ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}

			else if(actionType.equals("SendToApprovalPath"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.APPROVAL_TEST_APPROVE))
				{
					commonRequestHandler.sendToApprovalPath(request, response, userDTO);
				}
				else
				{
					System.out.println("Not going to SendToApprovalPath ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}
			else if(actionType.equals("approve"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.APPROVAL_TEST_ADD))
				{					
					commonRequestHandler.approve(request, response, true, userDTO, true);
				}
				else
				{
					System.out.println("Not going to  addApproval_test ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else if(actionType.equals("reject"))
			{
				System.out.println("trying to approve");
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.APPROVAL_TEST_APPROVE))
				{					
					commonRequestHandler.approve(request, response, true, userDTO, false);
				}
				else
				{
					System.out.println("Not going to  addApproval_test ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}				
			}
			else if(actionType.equals("terminate"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.APPROVAL_TEST_ADD))
				{
					commonRequestHandler.terminate(request, response, userDTO);
				}
				else
				{
					System.out.println("Not going to  addApproval_test ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}
			else if(actionType.equals("skipStep"))
			{
				
				System.out.println("skipStep");
				commonRequestHandler.skipStep(request, response, userDTO);									
			}
			else if(actionType.equals("getDTO"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.APPROVAL_TEST_ADD))
				{
					getDTO(request, response);					
				}
				else
				{
					System.out.println("Not going to  addApproval_test ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else if(actionType.equals("edit"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.APPROVAL_TEST_UPDATE))
				{					
					addApproval_test(request, response, false, userDTO, isPermanentTable);					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("delete"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.APPROVAL_TEST_UPDATE))
				{
					commonRequestHandler.delete(request, response, userDTO);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}	
			else if(actionType.equals("search"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.APPROVAL_TEST_SEARCH))
				{
					searchApproval_test(request, response, true, "");
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getGeo"))
			{
				System.out.println("going to geoloc ");
				request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
			}
			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}
	
	private void getDTO(HttpServletRequest request, HttpServletResponse response) 
	{
		try 
		{
			System.out.println("In getDTO");
			Approval_testDTO approval_testDTO = approval_testDAO.getDTOByID(Long.parseLong(request.getParameter("ID")));
			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			
			String encoded = this.gson.toJson(approval_testDTO);
			System.out.println("json encoded data = " + encoded);
			out.print(encoded);
			out.flush();
		}
		catch (NumberFormatException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	private void addApproval_test(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, boolean isPermanentTable) throws IOException 
	{
		// TODO Auto-generated method stub
		try 
		{
			request.setAttribute("failureMessage", "");
			System.out.println("%%%% addApproval_test");
			String language = userDTO.languageID == SessionConstants.ENGLISH? "English":"Bangla";
			Approval_testDTO approval_testDTO;
			SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
						
			if(addFlag == true)
			{
				approval_testDTO = new Approval_testDTO();
			}
			else
			{
				approval_testDTO = approval_testDAO.getDTOByID(Long.parseLong(request.getParameter("iD")));
			}
			
			String Value = "";

			Value = request.getParameter("description");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("description = " + Value);
			if(Value != null)
			{
				approval_testDTO.description = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			
			System.out.println("Done adding  addApproval_test dto = " + approval_testDTO);
			long returnedID = -1;
			
			List<ApprovalTestBabyDTO> approvalTestBabyDTOList = createApprovalTestBabyDTOListByRequest(request, language);
			
			if(isPermanentTable == false) //add new row for validation and make the old row outdated
			{
				approval_testDAO.setIsDeleted(approval_testDTO.iD, CommonDTO.OUTDATED);
				returnedID = approval_testDAO.add(approval_testDTO);
				approval_testDAO.setIsDeleted(returnedID, CommonDTO.WAITING_FOR_APPROVAL);
			}
			else if(addFlag == true)
			{
				returnedID = approval_testDAO.manageWriteOperations(approval_testDTO, SessionConstants.INSERT, -1, userDTO);
			}
			else
			{				
				returnedID = approval_testDAO.manageWriteOperations(approval_testDTO, SessionConstants.UPDATE, -1, userDTO);											
			}
			
		
			if(addFlag == true || isPermanentTable == false) //add or validate
			{
				if(approvalTestBabyDTOList != null)
				{				
					for(ApprovalTestBabyDTO approvalTestBabyDTO: approvalTestBabyDTOList)
					{
						approvalTestBabyDTO.approvalTestId = approval_testDTO.iD; 
						approvalTestBabyDAO.add(approvalTestBabyDTO);
					}
				}
			
			}			
			else
			{
				List<Long> childIdsFromRequest = approvalTestBabyDAO.getChildIdsFromRequest(request, "approvalTestBaby");
				//delete the removed children
				approvalTestBabyDAO.deleteChildrenNotInList("approval_test", "approval_test_baby", approval_testDTO.iD, childIdsFromRequest);
				List<Long> childIDsInDatabase = approvalTestBabyDAO.getChilIds("approval_test", "approval_test_baby", approval_testDTO.iD);
				
				
				if(childIdsFromRequest != null)
				{
					for(int i = 0; i < childIdsFromRequest.size(); i ++)
					{
						Long childIDFromRequest = childIdsFromRequest.get(i);
						if(childIDsInDatabase.contains(childIDFromRequest))
						{
							ApprovalTestBabyDTO approvalTestBabyDTO =  createApprovalTestBabyDTOByRequestAndIndex(request, false, i, language);
							approvalTestBabyDTO.approvalTestId = approval_testDTO.iD; 
							approvalTestBabyDAO.update(approvalTestBabyDTO);
						}
						else
						{
							ApprovalTestBabyDTO approvalTestBabyDTO =  createApprovalTestBabyDTOByRequestAndIndex(request, true, i, language);
							approvalTestBabyDTO.approvalTestId = approval_testDTO.iD; 
							approvalTestBabyDAO.add(approvalTestBabyDTO);
						}
					}
				}
				else
				{
					approvalTestBabyDAO.deleteChildrenByParent(approval_testDTO.iD, "approval_test_id");
				}
				
			}					
			if(!isPermanentTable)
			{
				commonRequestHandler.validate(approval_testDAO.getDTOByID(returnedID), request, response, userDTO);
			}
			else
			{
				ApiResponse.sendSuccessResponse(response, "Approval_testServlet?actionType=search");
			}
					
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			ApiResponse.sendErrorResponse(response, e.getMessage());
		}
	}
	
	



	private List<ApprovalTestBabyDTO> createApprovalTestBabyDTOListByRequest(HttpServletRequest request, String language) throws Exception{ 
		List<ApprovalTestBabyDTO> approvalTestBabyDTOList = new ArrayList<ApprovalTestBabyDTO>();
		if(request.getParameterValues("approvalTestBaby.iD") != null) 
		{
			int approvalTestBabyItemNo = request.getParameterValues("approvalTestBaby.iD").length;
			
			
			for(int index=0;index<approvalTestBabyItemNo;index++){
				ApprovalTestBabyDTO approvalTestBabyDTO = createApprovalTestBabyDTOByRequestAndIndex(request,true,index, language);
				approvalTestBabyDTOList.add(approvalTestBabyDTO);
			}
			
			return approvalTestBabyDTOList;
		}
		return null;
	}
	
	
	private ApprovalTestBabyDTO createApprovalTestBabyDTOByRequestAndIndex(HttpServletRequest request, boolean addFlag, int index, String language) throws Exception{
	
		ApprovalTestBabyDTO approvalTestBabyDTO;
		if(addFlag == true )
		{
			approvalTestBabyDTO = new ApprovalTestBabyDTO();
		}
		else
		{
			approvalTestBabyDTO = (ApprovalTestBabyDTO)approvalTestBabyDAO.getDTOByID(Long.parseLong(request.getParameterValues("approvalTestBaby.iD")[index]));
		}
		String path = getServletContext().getRealPath("/img2/");
		SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
		
		
				
		String Value = "";
		Value = request.getParameterValues("approvalTestBaby.approvalTestId")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		else
		{
			throw new Exception(LM.getText(LC.APPROVAL_TEST_ADD_APPROVAL_TEST_BABY_APPROVALTESTID, language) + " " + ErrorMessage.getInvalidMessage(language));
		}

		approvalTestBabyDTO.approvalTestId = Long.parseLong(Value);
		Value = request.getParameterValues("approvalTestBaby.inscription")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		else
		{
			throw new Exception(LM.getText(LC.APPROVAL_TEST_ADD_APPROVAL_TEST_BABY_INSCRIPTION, language) + " " + ErrorMessage.getInvalidMessage(language));
		}

		approvalTestBabyDTO.inscription = (Value);
		Value = request.getParameterValues("approvalTestBaby.aNumber")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		else
		{
			throw new Exception(LM.getText(LC.APPROVAL_TEST_ADD_APPROVAL_TEST_BABY_ANUMBER, language) + " " + ErrorMessage.getInvalidMessage(language));
		}

		approvalTestBabyDTO.aNumber = Integer.parseInt(Value);
		return approvalTestBabyDTO;
	
	}
	
	
	

	private void getApproval_test(HttpServletRequest request, HttpServletResponse response, long id) throws ServletException, IOException
	{
		System.out.println("in getApproval_test");
		Approval_testDTO approval_testDTO = null;
		try 
		{
			approval_testDTO = approval_testDAO.getDTOByID(id);
			boolean isPermanentTable = true;
			if(request.getParameter("isPermanentTable") != null)
			{
				isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
			}
			request.setAttribute("ID", approval_testDTO.iD);
			request.setAttribute("approval_testDTO",approval_testDTO);
			request.setAttribute("approval_testDAO",approval_testDAO);
			
			String URL= "";
			
			String inPlaceEdit = request.getParameter("inplaceedit");
			String inPlaceSubmit = request.getParameter("inplacesubmit");
			String getBodyOnly = request.getParameter("getBodyOnly");
			
			if(inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase(""))
			{
				URL = "approval_test/approval_testInPlaceEdit.jsp";	
				request.setAttribute("inplaceedit","");				
			}
			else if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
			{
				URL = "approval_test/approval_testSearchRow.jsp";
				request.setAttribute("inplacesubmit","");					
			}
			else
			{
				if(getBodyOnly  != null && !getBodyOnly.equalsIgnoreCase(""))
				{
					URL = "approval_test/approval_testEditBody.jsp?actionType=edit";
				}
				else
				{
					URL = "approval_test/approval_testEdit.jsp?actionType=edit";
				}				
			}
			
			RequestDispatcher rd = request.getRequestDispatcher(URL);
			rd.forward(request, response);
		}
		catch (NumberFormatException e) 
		{
			e.printStackTrace();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	
	private void getApproval_test(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		getApproval_test(request, response, Long.parseLong(request.getParameter("ID")));	
	}
	
	private void searchApproval_test(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException
	{
		System.out.println("in  searchApproval_test 1");
		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String ajax = request.getParameter("ajax");
		boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);
		request.setAttribute("navigator", "navAPPROVAL_TEST");
        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
			"navAPPROVAL_TEST",
			request,
			approval_testDAO,
			"viewAPPROVAL_TEST",
			null,
			tableName,
			isPermanent,
			userDTO,
			filter,
			true);
        try
        {
			System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        }
        catch(Exception e)
        {
			System.out.println("failed to dojob" + e);
        }

		request.setAttribute("approval_testDAO",approval_testDAO);
        RequestDispatcher rd;
        if(!isPermanent)
        {
        	if(hasAjax == false)
	        {
	        	System.out.println("Going to approval_test/approval_testApproval.jsp");
	        	rd = request.getRequestDispatcher("approval_test/approval_testApproval.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to approval_test/approval_testApprovalForm.jsp");
	        	rd = request.getRequestDispatcher("approval_test/approval_testApprovalForm.jsp");
	        }
        }
        else
        {
	        if(hasAjax == false)
	        {
	        	System.out.println("Going to approval_test/approval_testSearch.jsp");
	        	rd = request.getRequestDispatcher("pb/search.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to approval_test/approval_testSearchForm.jsp");
	        	rd = request.getRequestDispatcher("approval_test/approval_testSearchForm.jsp");
	        }
        }
		rd.forward(request, response);
	}
	
}

