package approval_test;
import java.util.*; 
import util.*; 


public class Approval_testDTO extends CommonDTO
{

    public String description = "";
	
	public List<ApprovalTestBabyDTO> approvalTestBabyDTOList = new ArrayList<>();
	
    @Override
	public String toString() {
            return "$Approval_testDTO[" +
            " iD = " + iD +
            " description = " + description +
            " jobCat = " + jobCat +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}