package approval_test;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import sessionmanager.SessionConstants;
import java.sql.SQLException;
import common.ConnectionAndStatementUtil;
import org.apache.log4j.Logger;
import repository.RepositoryManager;
import util.*;
import pb.*;
import user.UserDTO;

public class Approval_testDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());

	public Approval_testDAO(String tableName)
	{
		super(tableName);
		useSafeSearch = true;
		joinSQL = "";
		approvalMaps = new Approval_testApprovalMAPS("approval_test");
		columnNames = new String[] 
		{
			"ID",
			"description",
			"job_cat",
			"isDeleted",
			"lastModificationTime"
		};
	}
	
	public Approval_testDAO()
	{
		this("approval_test");		
	}
	
	public void setSearchColumn(Approval_testDTO approval_testDTO)
	{
		approval_testDTO.searchColumn = "";
		approval_testDTO.searchColumn += approval_testDTO.description + " ";
	}
	
	public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException
	{
		Approval_testDTO approval_testDTO = (Approval_testDTO)commonDTO;
		int index = 1;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(approval_testDTO);
		if(isInsert)
		{
			ps.setObject(index++,approval_testDTO.iD);
		}
		ps.setObject(index++,approval_testDTO.description);
		ps.setObject(index++,approval_testDTO.jobCat);
		if(isInsert)
		{
			ps.setObject(index++, 0);
			ps.setObject(index++, lastModificationTime);
		}
	}
	
	public Approval_testDTO build(ResultSet rs)
	{
		try
		{
			Approval_testDTO approval_testDTO = new Approval_testDTO();
			approval_testDTO.iD = rs.getLong("ID");
			approval_testDTO.description = rs.getString("description");
			approval_testDTO.jobCat = rs.getInt("job_cat");
			approval_testDTO.isDeleted = rs.getInt("isDeleted");
			approval_testDTO.lastModificationTime = rs.getLong("lastModificationTime");
			return approval_testDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
		
	public Approval_testDTO getDTOByID (long id)
	{
		Approval_testDTO approval_testDTO = null;
		try 
		{
			approval_testDTO = (Approval_testDTO)super.getDTOByID(id);
			if(approval_testDTO != null)
			{
				ApprovalTestBabyDAO approvalTestBabyDAO = new ApprovalTestBabyDAO("approval_test_baby");				
				List<ApprovalTestBabyDTO> approvalTestBabyDTOList = (List<ApprovalTestBabyDTO>)approvalTestBabyDAO.getDTOsByParent("approval_test_id", approval_testDTO.iD);
				approval_testDTO.approvalTestBabyDTOList = approvalTestBabyDTOList;
			}
		}
		catch (Exception e) 
		{
				// TODO Auto-generated catch block
				e.printStackTrace();
		}
		return approval_testDTO;
	}

	public String getSqlWithSearchCriteriaForNormalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
			UserDTO userDTO, String filter, boolean tableHasJobCat, List<Object> objectList)
    {

		String sql = "SELECT ";
		if(category == GETIDS)
		{
			sql += " " + tableName + ".ID as ID ";
		}
		else if(category == GETCOUNT)
		{
			sql += " count( " + tableName + ".ID) as countID ";
		}
		else if(category == GETDTOS)
		{
			sql += " distinct " + tableName + ".* ";
		}
		sql += "FROM " + tableName + " ";
		sql += joinSQL;
		

		
		String AnyfieldSql = "";
		String AllFieldSql = "";
		
		if(p_searchCriteria != null)
		{
						
			
			Enumeration names = p_searchCriteria.keys();
			String str, value;
			
			AnyfieldSql = "(";
			
			if(p_searchCriteria.get("AnyField")!= null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase(""))
			{
				AnyfieldSql+= tableName + ".search_column like ?";
				objectList.add("%" + p_searchCriteria.get("AnyField").toString() + "%");			
			}
			AnyfieldSql += ")";
			System.out.println("AnyfieldSql = " + AnyfieldSql);
			
			AllFieldSql = "(";
			int i = 0;
			while(names.hasMoreElements())
			{				
				str = (String) names.nextElement();
				value = (String)p_searchCriteria.get(str);
		        System.out.println(str + ": " + value);
				if(value != null && !value.equalsIgnoreCase("") && (
						 str.equals("description")
				)
						
				)
				{
					if(p_searchCriteria.get(str).equals("any"))
					{
						continue;
					}

					if( i > 0)
					{
						AllFieldSql+= " AND  ";
					}
					
					 if(str.equals("description"))
					{
						AllFieldSql += "" + tableName + ".description like ?";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					
					
				}
			}
			
			AllFieldSql += ")";
			System.out.println("AllFieldSql = " + AllFieldSql);
			
			
		}
		
		
		sql += " WHERE ";
		
		sql += " (" + tableName + ".isDeleted = 0 ";			
		sql += ")";
		
		
		if(!filter.equalsIgnoreCase(""))
		{
			sql += " and " + filter + " ";
		}
		
		if(!AnyfieldSql.equals("()") && !AnyfieldSql.equals(""))
		{
			sql += " AND " + AnyfieldSql;
			
		}
		if(!AllFieldSql.equals("()") && !AllFieldSql.equals(""))
		{			
			sql += " AND " + AllFieldSql;
		}
		
	
			
		
		if(category == GETDTOS)
		{
			sql += " order by " + tableName + ".lastModificationTime desc ";
		}

		//printSql(sql);
		
		if(limit >= 0)
		{
			sql += " limit " + limit;
		}
		if(offset >= 0)
		{
			sql += " offset " + offset;
		}

		
		return sql;
    }
	
	public String getSqlWithSearchCriteria(Hashtable p_searchCriteria, int limit, int offset, int category,
			boolean isPermanentTable, UserDTO userDTO, String filter, boolean tableHasJobCat, List<Object> objectList)
    {
		if(isPermanentTable)
		{
			return getSqlWithSearchCriteriaForNormalSearch(p_searchCriteria, limit, offset, category, userDTO, filter, tableHasJobCat, objectList);
		}
		else
		{
			return getSqlWithSearchCriteriaForApprovalSearch(p_searchCriteria, limit, offset, category, userDTO, filter, tableHasJobCat);
		}
				
    }
				
}
	