package approval_test;
import java.util.*; 
import util.*; 


public class ApprovalTestBabyDTO extends CommonDTO
{

	public long approvalTestId = -1;
    public String inscription = "";
	public int aNumber = -1;
	
	public List<ApprovalTestBabyDTO> approvalTestBabyDTOList = new ArrayList<>();
	
    @Override
	public String toString() {
            return "$ApprovalTestBabyDTO[" +
            " iD = " + iD +
            " approvalTestId = " + approvalTestId +
            " inscription = " + inscription +
            " aNumber = " + aNumber +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}