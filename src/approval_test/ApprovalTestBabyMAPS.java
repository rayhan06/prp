package approval_test;
import java.util.*; 
import util.*;


public class ApprovalTestBabyMAPS extends CommonMaps
{	
	public ApprovalTestBabyMAPS(String tableName)
	{
		


		java_SQL_map.put("approval_test_id".toLowerCase(), "approvalTestId".toLowerCase());
		java_SQL_map.put("inscription".toLowerCase(), "inscription".toLowerCase());
		java_SQL_map.put("a_number".toLowerCase(), "aNumber".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Approval Test Id".toLowerCase(), "approvalTestId".toLowerCase());
		java_Text_map.put("Inscription".toLowerCase(), "inscription".toLowerCase());
		java_Text_map.put("A Number".toLowerCase(), "aNumber".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}