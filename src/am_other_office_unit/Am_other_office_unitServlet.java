package am_other_office_unit;


import java.io.IOException;
import java.text.SimpleDateFormat;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import am_house.Am_houseDAO;
import geolocation.*;
import org.apache.log4j.Logger;


import permission.MenuConstants;


import sessionmanager.SessionConstants;
import user.UserDTO;
import util.*;

import javax.servlet.http.*;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;


import language.LC;
import language.LM;
import common.ApiResponse;
import common.BaseServlet;
import com.google.gson.Gson;
import pb.*;
import pbReport.*;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;


/**
 * Servlet implementation class Am_other_office_unitServlet
 */
@WebServlet("/Am_other_office_unitServlet")
@MultipartConfig
public class Am_other_office_unitServlet extends BaseServlet {
    private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Am_other_office_unitServlet.class);

    @Override
    public String getTableName() {
        return Am_other_office_unitDAO.getInstance().getTableName();
    }

    @Override
    public String getServletName() {
        return "Am_other_office_unitServlet";
    }

    @Override
    public Am_other_office_unitDAO getCommonDAOService() {
        return Am_other_office_unitDAO.getInstance();
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[]{MenuConstants.AM_OTHER_OFFICE_UNIT_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[]{MenuConstants.AM_OTHER_OFFICE_UNIT_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[]{MenuConstants.AM_OTHER_OFFICE_UNIT_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Am_other_office_unitServlet.class;
    }

    private final Gson gson = new Gson();


    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {

        Am_other_office_unitDTO am_other_office_unitDTO;
        SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
        String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
        boolean isLanEng = Language.equalsIgnoreCase("English");

        if (addFlag == true) {
            am_other_office_unitDTO = new Am_other_office_unitDTO();

            am_other_office_unitDTO.insertionDate = am_other_office_unitDTO.lastModificationTime = System.currentTimeMillis();
            am_other_office_unitDTO.insertedBy = am_other_office_unitDTO.modifiedBy = String.valueOf(userDTO.ID);
        } else {
            am_other_office_unitDTO = Am_other_office_unitDAO.getInstance().getDTOFromID(Long.parseLong(request.getParameter("iD")));
            if (am_other_office_unitDTO == null) {
                throw new Exception("Office information is not found");
            }

            am_other_office_unitDTO.lastModificationTime = System.currentTimeMillis();
            am_other_office_unitDTO.modifiedBy = String.valueOf(userDTO.ID);
        }

        String officeNameEn = request.getParameter("officeNameEn");
        String officeNameBn = request.getParameter("officeNameBn");

        if (officeNameEn != null && !officeNameEn.isEmpty()) {
            officeNameEn = Jsoup.clean(officeNameEn, Whitelist.simpleText());
            officeNameEn = officeNameEn.replace("'", " ");
            officeNameEn = officeNameEn.replace("\"", " ");

            am_other_office_unitDTO.officeNameEn = officeNameEn;

        } else {
            throw new Exception(isLanEng ? "Please provide office name in english" : "অনুগ্রহপূর্বক অফিসের ইংরেজি নাম দিন");
        }

        for (char currentChar : officeNameEn.toCharArray()) {
            if (currentChar == ' ') {
                continue;
            }
            if (Character.UnicodeBlock.of(currentChar) == Character.UnicodeBlock.BENGALI) {
                throw new Exception(isLanEng ? "Please provide office name in english" : "অনুগ্রহপূর্বক অফিসের ইংরেজি নাম দিন");
            }
        }

        if (officeNameBn != null && !officeNameBn.isEmpty()) {
            officeNameBn = Jsoup.clean(officeNameBn, Whitelist.simpleText());
            officeNameBn = officeNameBn.replace("'", " ");
            officeNameBn = officeNameBn.replace("\"", " ");
            am_other_office_unitDTO.officeNameBn = officeNameBn;
        } else {
            throw new Exception(isLanEng ? "Please provide office name in bangla" : "অনুগ্রহপূর্বক অফিসের বাংলা নাম দিন");
        }

        for (char currentChar : officeNameBn.toCharArray()) {
            if (currentChar == ' ') {
                continue;
            }
            if (Character.UnicodeBlock.of(currentChar) != Character.UnicodeBlock.BENGALI) {
                throw new Exception(isLanEng ? "Please provide office name in bangla" : "অনুগ্রহপূর্বক অফিসের বাংলা নাম দিন");
            }
        }
        List<Am_other_office_unitDTO> am_other_office_unitDTOS = Am_other_office_unitRepository.getInstance().getAllAm_other_office_unitList();
        for (Am_other_office_unitDTO amOtherOfficeUnitDTO : am_other_office_unitDTOS) {
            if (amOtherOfficeUnitDTO.iD == am_other_office_unitDTO.iD) {
                continue;
            } else if ((amOtherOfficeUnitDTO.officeNameEn.equalsIgnoreCase(am_other_office_unitDTO.officeNameEn)) || (amOtherOfficeUnitDTO.officeNameBn.equalsIgnoreCase(am_other_office_unitDTO.officeNameBn))) {
                throw new Exception(isLanEng ? "Please provide unique office name" : "অনুগ্রহপূর্বক সঠিক অফিসের নাম দিন");
            }
        }
        logger.debug("Done adding  addAm_other_office_unit dto = " + am_other_office_unitDTO);

        if (addFlag == true) {
            Am_other_office_unitDAO.getInstance().add(am_other_office_unitDTO);
        } else {
            Am_other_office_unitDAO.getInstance().update(am_other_office_unitDTO);
        }

        return am_other_office_unitDTO;


    }

    @Override
    protected void deleteT(HttpServletRequest request, UserDTO userDTO) {
        String[] IDsToDelete = request.getParameterValues("ID");
        if (IDsToDelete.length > 0) {
            List<Long> ids = Stream.of(IDsToDelete)
                    .map(Long::parseLong)
                    .filter(i -> Am_other_office_unitDAO.getInstance().isNotUsed(i, 2))
                    .collect(Collectors.toList());

            if (!ids.isEmpty()) {
                getCommonDAOService().deletePb(userDTO.employee_record_id, ids);
            }
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        OptionDTO optionDTO;
        Gson gson = new Gson();
        String actionType = request.getParameter("actionType");
        logger.debug("actionType = " + actionType);
        switch (actionType) {
            case "getAllOtherOffices":

                List<OptionDTO> OptionDTOList = new ArrayList<>();
                List<Am_other_office_unitDTO> otherOfficeDTOList = Am_other_office_unitRepository.getInstance().getAllAm_other_office_unitList();

                for (Am_other_office_unitDTO dto : otherOfficeDTOList) {

                    optionDTO = new OptionDTO(dto.officeNameEn, dto.officeNameBn, dto.iD + "");
                    OptionDTOList.add(optionDTO);
                }
                response.getWriter().write(gson.toJson(OptionDTOList));
                response.getWriter().flush();
                response.getWriter().close();
                return;

            default:
                super.doGet(request, response);
                return;
        }
    }
}

