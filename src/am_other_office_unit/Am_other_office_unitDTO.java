package am_other_office_unit;
import java.util.*; 
import util.*; 


public class Am_other_office_unitDTO extends CommonDTO
{

    public String officeNameEn = "";
    public String officeNameBn = "";
	public long insertionDate = -1;
    public String insertedBy = "";
    public String modifiedBy = "";
	
	
    @Override
	public String toString() {
            return "$Am_other_office_unitDTO[" +
            " iD = " + iD +
            " officeNameEn = " + officeNameEn +
            " officeNameBn = " + officeNameBn +
            " insertionDate = " + insertionDate +
            " insertedBy = " + insertedBy +
            " modifiedBy = " + modifiedBy +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            " searchColumn = " + searchColumn +
            "]";
    }

}