package am_other_office_unit;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

import geolocation.GeoDistrictDAO;
import geolocation.GeoDistrictDTO;
import office_units.Office_unitsDTO;
import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class Am_other_office_unitRepository implements Repository {
	Am_other_office_unitDAO am_other_office_unitDAO = null;
	
	static Logger logger = Logger.getLogger(Am_other_office_unitRepository.class);
	Map<Long, Am_other_office_unitDTO>mapOfAm_other_office_unitDTOToiD;
	private Map<String, Am_other_office_unitDTO> mapByEngText;
	private Map<String,Am_other_office_unitDTO> mapByBngText;
	private List<Am_other_office_unitDTO> allOtherOfficesDTO;

  
	private Am_other_office_unitRepository(){
		am_other_office_unitDAO = Am_other_office_unitDAO.getInstance();
		mapOfAm_other_office_unitDTOToiD = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	private static class LazyLoader{
        final static Am_other_office_unitRepository INSTANCE = new Am_other_office_unitRepository();
    }

    public static Am_other_office_unitRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

	public void reload(boolean reloadAll){
//		try {
//			List<Am_other_office_unitDTO> am_other_office_unitDTOs = am_other_office_unitDAO.getAllDTOs(reloadAll);
//			for(Am_other_office_unitDTO am_other_office_unitDTO : am_other_office_unitDTOs) {
//				Am_other_office_unitDTO oldAm_other_office_unitDTO = getAm_other_office_unitDTOByID(am_other_office_unitDTO.iD);
//				if( oldAm_other_office_unitDTO != null ) {
//					mapOfAm_other_office_unitDTOToiD.remove(oldAm_other_office_unitDTO.iD);
//
//
//				}
//				if(am_other_office_unitDTO.isDeleted == 0)
//				{
//
//					mapOfAm_other_office_unitDTOToiD.put(am_other_office_unitDTO.iD, am_other_office_unitDTO);
//
//				}
//			}
//
//		} catch (Exception e) {
//			logger.debug("FATAL", e);
//		}

		List<Am_other_office_unitDTO> am_other_office_unitDTOS = am_other_office_unitDAO.getAllDTOs(reloadAll);
		if (am_other_office_unitDTOS != null && am_other_office_unitDTOS.size() > 0) {
			am_other_office_unitDTOS.stream()
					.peek(dto -> removeIfPresent(mapOfAm_other_office_unitDTOToiD.get(dto.iD)))
					.filter(dto -> dto.isDeleted == 0)
					.forEach(dto -> {
						mapOfAm_other_office_unitDTOToiD.put(dto.iD, dto);

					});

			allOtherOfficesDTO = new ArrayList<>(mapOfAm_other_office_unitDTOToiD.values());
            allOtherOfficesDTO.sort(Comparator.comparing(o -> o.iD));

		}



	}

	private void removeIfPresent(Am_other_office_unitDTO oldDTO) {
		if (oldDTO == null) {
			return;
		}
		if (mapOfAm_other_office_unitDTOToiD.get(oldDTO.iD) != null) {
			mapOfAm_other_office_unitDTOToiD.remove(oldDTO.iD);
		}



	}

	public List<Am_other_office_unitDTO> getAllAm_other_office_unitList() {
		return allOtherOfficesDTO;
	}
	
	public List<Am_other_office_unitDTO> getAm_other_office_unitList() {
		List <Am_other_office_unitDTO> am_other_office_units = new ArrayList<Am_other_office_unitDTO>(this.mapOfAm_other_office_unitDTOToiD.values());
		return am_other_office_units;
	}
	
	
	public Am_other_office_unitDTO getAm_other_office_unitDTOByID( long ID){
		return mapOfAm_other_office_unitDTOToiD.get(ID);
	}

	
	@Override
	public String getTableName() {
		return "am_other_office_unit";
	}
}


