package am_other_office_unit;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.sql.SQLException;

import am_house_allocation.Am_house_allocationDAO;
import common.CommonDAOService;
import common.ConnectionAndStatementUtil;
import geolocation.GeoDistrictDTO;
import org.apache.log4j.Logger;
import util.*;
import pb.*;

public class Am_other_office_unitDAO  implements CommonDAOService<Am_other_office_unitDTO>
{
	
	Logger logger = Logger.getLogger(getClass());
	
	private static String addQuery = "";
	private static String updateQuery = "";
	private String[] columnNames = null;

	private static final String  getAllSqlQuery= "SELECT * FROM am_other_office_unit WHERE isDeleted = 0 ORDER BY ID DESC";

	public Am_other_office_unitDAO()
	{
		columnNames = new String[] 
		{
			"ID",
			"office_name_en",
			"office_name_bn",
			"insertion_date",
			"inserted_by",
			"modified_by",
			"search_column",
			"isDeleted",
			"lastModificationTime"
		};
		updateQuery = getUpdateQuery(columnNames);
        addQuery = getAddQuery(columnNames);
		
		searchMap.put("office_name_en"," and (office_name_en like ?)");
		searchMap.put("office_name_bn"," and (office_name_bn like ?)");
		searchMap.put("insertion_date_start"," and (insertion_date >= ?)");
		searchMap.put("insertion_date_end"," and (insertion_date <= ?)");
		searchMap.put("AnyField"," and (search_column like ?)");
	}

	private final Map<String,String> searchMap = new HashMap<>();

    private static class LazyLoader
    {
    	static final Am_other_office_unitDAO INSTANCE = new Am_other_office_unitDAO();
	}

	public static Am_other_office_unitDAO getInstance()
	{
    	return LazyLoader.INSTANCE;
	}
	
	public void setSearchColumn(Am_other_office_unitDTO am_other_office_unitDTO)
	{
		am_other_office_unitDTO.searchColumn = "";
		am_other_office_unitDTO.searchColumn += am_other_office_unitDTO.officeNameEn + " ";
		am_other_office_unitDTO.searchColumn += am_other_office_unitDTO.officeNameBn + " ";
	}
	
	@Override
	public void set(PreparedStatement ps, Am_other_office_unitDTO am_other_office_unitDTO, boolean isInsert) throws SQLException
	{
		int index = 0;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(am_other_office_unitDTO);
		if(isInsert)
		{
			ps.setObject(++index,am_other_office_unitDTO.iD);
		}
		ps.setObject(++index,am_other_office_unitDTO.officeNameEn);
		ps.setObject(++index,am_other_office_unitDTO.officeNameBn);
		ps.setObject(++index,am_other_office_unitDTO.insertionDate);
		ps.setObject(++index,am_other_office_unitDTO.insertedBy);
		ps.setObject(++index,am_other_office_unitDTO.modifiedBy);
		ps.setObject(++index,am_other_office_unitDTO.searchColumn);
		if(isInsert)
		{
			ps.setObject(++index,am_other_office_unitDTO.isDeleted);
		}
		ps.setObject(++index, lastModificationTime);
		if(!isInsert)
		{
			ps.setObject(++index,am_other_office_unitDTO.iD);
		}
	}
	
	@Override
	public Am_other_office_unitDTO buildObjectFromResultSet(ResultSet rs)
	{
		try
		{
			Am_other_office_unitDTO am_other_office_unitDTO = new Am_other_office_unitDTO();
			int i = 0;
			am_other_office_unitDTO.iD = rs.getLong(columnNames[i++]);
			am_other_office_unitDTO.officeNameEn = rs.getString(columnNames[i++]);
			am_other_office_unitDTO.officeNameBn = rs.getString(columnNames[i++]);
			am_other_office_unitDTO.insertionDate = rs.getLong(columnNames[i++]);
			am_other_office_unitDTO.insertedBy = rs.getString(columnNames[i++]);
			am_other_office_unitDTO.modifiedBy = rs.getString(columnNames[i++]);
			am_other_office_unitDTO.searchColumn = rs.getString(columnNames[i++]);
			am_other_office_unitDTO.isDeleted = rs.getInt(columnNames[i++]);
			am_other_office_unitDTO.lastModificationTime = rs.getLong(columnNames[i++]);

			return am_other_office_unitDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
		
	public Am_other_office_unitDTO getDTOByID (long id)
	{
		return getDTOFromID(id);
	}

	@Override
	public String getTableName() {
		return "am_other_office_unit";
	}

	@Override
	public Map<String, String> getSearchMap() {
		return searchMap;
	}

	public long add(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Am_other_office_unitDTO) commonDTO,addQuery,true);
	}

	public long update(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Am_other_office_unitDTO) commonDTO,updateQuery,false);
	}
	
	@Override
	public String getLastModifierUser(){
		return "modified_by";
    }

	public List<Am_other_office_unitDTO> getAllOtherOfficesDTO(){
		return ConnectionAndStatementUtil.getListOfT(getAllSqlQuery,this::buildObjectFromResultSet);
	}

	public boolean isNotUsed(long iD,long officeType){
        return Am_other_office_unitDAO.getInstance().getCountByOfficeId(iD, officeType) <= 0;
    }

	public int getCountByOfficeId(long officeId,long officeType){
		String countQuery = "SELECT count(*) as countID FROM "
				+  " am_office_assignment where isDeleted = 0 and office_unit_id = ? and office_type = ? ";
		return ConnectionAndStatementUtil.getT(countQuery, Arrays.asList(officeId,officeType), rs -> {
			try {
				return rs.getInt("countID");
			} catch (SQLException ex) {
				ex.printStackTrace();
				return 0;
			}
		}, 0);
	}
				
}
	