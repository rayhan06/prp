package vm_fuel_request;

/*
 * @author Md. Erfan Hossain
 * @created 01/05/2021 - 7:39 PM
 * @project parliament
 */

public enum Vm_fuel_requestStatusEnum {
    WAITING_FOR_APPROVAL(1),
    WAITING_FOR_POLICE_VERIFICATION(2),
    APPROVED(3),
    REJECTED(4),
    PRINTING(5),
    READY_FOR_DELIVER(6),
    DELIVERED(7),
    ACTIVATE(8),
    LOST(9),
    BLOCKED(10),
    TEMPORARY_BLOCKED(11),
    PAID(12)
    ;

    private final int value;

    Vm_fuel_requestStatusEnum(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
    
    public static String getColor(int value){
        if(value == Vm_fuel_requestStatusEnum.WAITING_FOR_APPROVAL.getValue()){
            return "#22ccc1";
        }else if(value == Vm_fuel_requestStatusEnum.WAITING_FOR_POLICE_VERIFICATION.getValue()){
            return "#0debaa";
        } else if(value == Vm_fuel_requestStatusEnum.APPROVED.getValue()){
            return "green";
        } else if(value == Vm_fuel_requestStatusEnum.REJECTED.getValue()){
            return "crimson";
        } else if(value == Vm_fuel_requestStatusEnum.PRINTING.getValue()){
            return "#0c91e5";
        } else if(value == Vm_fuel_requestStatusEnum.READY_FOR_DELIVER.getValue()){
            return "#0bd398";
        } else if(value == Vm_fuel_requestStatusEnum.DELIVERED.getValue()){
            return "#0f9af0";
        } else if(value == Vm_fuel_requestStatusEnum.ACTIVATE.getValue()){
            return "forestgreen";
        } else if(value == Vm_fuel_requestStatusEnum.LOST.getValue()){
            return "maroon";
        }else if(value == Vm_fuel_requestStatusEnum.BLOCKED.getValue()){
            return "darkred";
        }else{ //CardStatusEnum.TEMPORARY_BLOCKED
            return "red";
        }
    }
}
