package vm_fuel_request;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;

import common.ConnectionAndStatementUtil;
import sessionmanager.SessionConstants;
import java.sql.SQLException;


import org.apache.log4j.Logger;

import dbm.*;

import repository.RepositoryManager;

import util.*;
import pb.*;
import user.UserDTO;
import vm_fuel_vendor.VmFuelVendorItemDAO;

public class Vm_fuel_requestDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());
	

	
	public Vm_fuel_requestDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";
		commonMaps = new Vm_fuel_requestMAPS(tableName);
		columnNames = new String[] 
		{
			"ID",
			"inserted_by_user_id",
			"inserted_by_organogram_id",
			"insertion_date",
			"search_column",
			"vehicle_type",
			"vehicle_id",
			"application_date",
			"vehicle_driver_assignment_id",
			"last_fuel_date",
			"status",
			"fiscal_year_id",
			"approved_date",
			"vendor_id",
			"sarok_number",
			"files_dropzone",
			"payment_remarks",
			"requester_org_id",
			"requester_office_id",
			"requester_office_unit_id",
			"requester_emp_id",
			"requester_phone_num",
			"requester_name_en",
			"requester_name_bn",
			"requester_office_name_en",
			"requester_office_name_bn",
			"requester_office_unit_name_en",
			"requester_office_unit_name_bn",
			"requester_office_unit_org_name_en",
			"requester_office_unit_org_name_bn",
			"approver_org_id",
			"approver_office_id",
			"approver_office_unit_id",
			"approver_emp_id",
			"approver_phone_num",
			"approver_name_en",
			"approver_name_bn",
			"approver_office_name_en",
			"approver_office_name_bn",
			"approver_office_unit_name_en",
			"approver_office_unit_name_bn",
			"approver_office_unit_org_name_en",
			"approver_office_unit_org_name_bn",
			"payment_receiver_org_id",
			"payment_receiver_office_id",
			"payment_receiver_office_unit_id",
			"payment_receiver_emp_id",
			"payment_receiver_phone_num",
			"payment_receiver_name_en",
			"payment_receiver_name_bn",
			"payment_receiver_office_name_en",
			"payment_receiver_office_name_bn",
			"payment_receiver_office_unit_name_en",
			"payment_receiver_office_unit_name_bn",
			"payment_receiver_office_unit_org_name_en",
			"payment_receiver_office_unit_org_name_bn",
			"isDeleted",
			"lastModificationTime"
		};
	}
	
	public Vm_fuel_requestDAO()
	{
		this("vm_fuel_request");		
	}
	
	public void setSearchColumn(Vm_fuel_requestDTO vm_fuel_requestDTO)
	{
		vm_fuel_requestDTO.searchColumn = "";
		vm_fuel_requestDTO.searchColumn += CatRepository.getName("English", "vehicle_type", vm_fuel_requestDTO.vehicleType) + " " + CatRepository.getName("Bangla", "vehicle_type", vm_fuel_requestDTO.vehicleType) + " ";
		vm_fuel_requestDTO.searchColumn += vm_fuel_requestDTO.sarokNumber + " ";
		vm_fuel_requestDTO.searchColumn += vm_fuel_requestDTO.paymentRemarks + " ";
		vm_fuel_requestDTO.searchColumn += vm_fuel_requestDTO.requesterPhoneNum + " ";
		vm_fuel_requestDTO.searchColumn += vm_fuel_requestDTO.requesterNameEn + " ";
		vm_fuel_requestDTO.searchColumn += vm_fuel_requestDTO.requesterNameBn + " ";
		vm_fuel_requestDTO.searchColumn += vm_fuel_requestDTO.requesterOfficeNameEn + " ";
		vm_fuel_requestDTO.searchColumn += vm_fuel_requestDTO.requesterOfficeNameBn + " ";
		vm_fuel_requestDTO.searchColumn += vm_fuel_requestDTO.requesterOfficeUnitNameEn + " ";
		vm_fuel_requestDTO.searchColumn += vm_fuel_requestDTO.requesterOfficeUnitNameBn + " ";
		vm_fuel_requestDTO.searchColumn += vm_fuel_requestDTO.requesterOfficeUnitOrgNameEn + " ";
		vm_fuel_requestDTO.searchColumn += vm_fuel_requestDTO.requesterOfficeUnitOrgNameBn + " ";
		vm_fuel_requestDTO.searchColumn += vm_fuel_requestDTO.approverPhoneNum + " ";
		vm_fuel_requestDTO.searchColumn += vm_fuel_requestDTO.approverNameEn + " ";
		vm_fuel_requestDTO.searchColumn += vm_fuel_requestDTO.approverNameBn + " ";
		vm_fuel_requestDTO.searchColumn += vm_fuel_requestDTO.approverOfficeNameEn + " ";
		vm_fuel_requestDTO.searchColumn += vm_fuel_requestDTO.approverOfficeNameBn + " ";
		vm_fuel_requestDTO.searchColumn += vm_fuel_requestDTO.approverOfficeUnitNameEn + " ";
		vm_fuel_requestDTO.searchColumn += vm_fuel_requestDTO.approverOfficeUnitNameBn + " ";
		vm_fuel_requestDTO.searchColumn += vm_fuel_requestDTO.approverOfficeUnitOrgNameEn + " ";
		vm_fuel_requestDTO.searchColumn += vm_fuel_requestDTO.approverOfficeUnitOrgNameBn + " ";
		vm_fuel_requestDTO.searchColumn += vm_fuel_requestDTO.paymentReceiverPhoneNum + " ";
		vm_fuel_requestDTO.searchColumn += vm_fuel_requestDTO.paymentReceiverNameEn + " ";
		vm_fuel_requestDTO.searchColumn += vm_fuel_requestDTO.paymentReceiverNameBn + " ";
		vm_fuel_requestDTO.searchColumn += vm_fuel_requestDTO.paymentReceiverOfficeNameEn + " ";
		vm_fuel_requestDTO.searchColumn += vm_fuel_requestDTO.paymentReceiverOfficeNameBn + " ";
		vm_fuel_requestDTO.searchColumn += vm_fuel_requestDTO.paymentReceiverOfficeUnitNameEn + " ";
		vm_fuel_requestDTO.searchColumn += vm_fuel_requestDTO.paymentReceiverOfficeUnitNameBn + " ";
		vm_fuel_requestDTO.searchColumn += vm_fuel_requestDTO.paymentReceiverOfficeUnitOrgNameEn + " ";
		vm_fuel_requestDTO.searchColumn += vm_fuel_requestDTO.paymentReceiverOfficeUnitOrgNameBn + " ";
		
	}
	
	public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException
	{
		Vm_fuel_requestDTO vm_fuel_requestDTO = (Vm_fuel_requestDTO)commonDTO;
		int index = 1;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(vm_fuel_requestDTO);
		if(isInsert)
		{
			ps.setObject(index++,vm_fuel_requestDTO.iD);
		}
		ps.setObject(index++,vm_fuel_requestDTO.insertedByUserId);
		ps.setObject(index++,vm_fuel_requestDTO.insertedByOrganogramId);
		ps.setObject(index++,vm_fuel_requestDTO.insertionDate);
		ps.setObject(index++,vm_fuel_requestDTO.searchColumn);
		ps.setObject(index++,vm_fuel_requestDTO.vehicleType);
		ps.setObject(index++,vm_fuel_requestDTO.vehicleId);
		ps.setObject(index++,vm_fuel_requestDTO.applicationDate);
		ps.setObject(index++,vm_fuel_requestDTO.vehicleDriverAssignmentId);
		ps.setObject(index++,vm_fuel_requestDTO.lastFuelDate);
		ps.setObject(index++,vm_fuel_requestDTO.status);
		ps.setObject(index++,vm_fuel_requestDTO.fiscalYearId);
		ps.setObject(index++,vm_fuel_requestDTO.approvedDate);
		ps.setObject(index++,vm_fuel_requestDTO.vendorId);
		ps.setObject(index++,vm_fuel_requestDTO.sarokNumber);
		ps.setObject(index++,vm_fuel_requestDTO.filesDropzone);
		ps.setObject(index++,vm_fuel_requestDTO.paymentRemarks);
		ps.setObject(index++,vm_fuel_requestDTO.requesterOrgId);
		ps.setObject(index++,vm_fuel_requestDTO.requesterOfficeId);
		ps.setObject(index++,vm_fuel_requestDTO.requesterOfficeUnitId);
		ps.setObject(index++,vm_fuel_requestDTO.requesterEmpId);
		ps.setObject(index++,vm_fuel_requestDTO.requesterPhoneNum);
		ps.setObject(index++,vm_fuel_requestDTO.requesterNameEn);
		ps.setObject(index++,vm_fuel_requestDTO.requesterNameBn);
		ps.setObject(index++,vm_fuel_requestDTO.requesterOfficeNameEn);
		ps.setObject(index++,vm_fuel_requestDTO.requesterOfficeNameBn);
		ps.setObject(index++,vm_fuel_requestDTO.requesterOfficeUnitNameEn);
		ps.setObject(index++,vm_fuel_requestDTO.requesterOfficeUnitNameBn);
		ps.setObject(index++,vm_fuel_requestDTO.requesterOfficeUnitOrgNameEn);
		ps.setObject(index++,vm_fuel_requestDTO.requesterOfficeUnitOrgNameBn);
		ps.setObject(index++,vm_fuel_requestDTO.approverOrgId);
		ps.setObject(index++,vm_fuel_requestDTO.approverOfficeId);
		ps.setObject(index++,vm_fuel_requestDTO.approverOfficeUnitId);
		ps.setObject(index++,vm_fuel_requestDTO.approverEmpId);
		ps.setObject(index++,vm_fuel_requestDTO.approverPhoneNum);
		ps.setObject(index++,vm_fuel_requestDTO.approverNameEn);
		ps.setObject(index++,vm_fuel_requestDTO.approverNameBn);
		ps.setObject(index++,vm_fuel_requestDTO.approverOfficeNameEn);
		ps.setObject(index++,vm_fuel_requestDTO.approverOfficeNameBn);
		ps.setObject(index++,vm_fuel_requestDTO.approverOfficeUnitNameEn);
		ps.setObject(index++,vm_fuel_requestDTO.approverOfficeUnitNameBn);
		ps.setObject(index++,vm_fuel_requestDTO.approverOfficeUnitOrgNameEn);
		ps.setObject(index++,vm_fuel_requestDTO.approverOfficeUnitOrgNameBn);
		ps.setObject(index++,vm_fuel_requestDTO.paymentReceiverOrgId);
		ps.setObject(index++,vm_fuel_requestDTO.paymentReceiverOfficeId);
		ps.setObject(index++,vm_fuel_requestDTO.paymentReceiverOfficeUnitId);
		ps.setObject(index++,vm_fuel_requestDTO.paymentReceiverEmpId);
		ps.setObject(index++,vm_fuel_requestDTO.paymentReceiverPhoneNum);
		ps.setObject(index++,vm_fuel_requestDTO.paymentReceiverNameEn);
		ps.setObject(index++,vm_fuel_requestDTO.paymentReceiverNameBn);
		ps.setObject(index++,vm_fuel_requestDTO.paymentReceiverOfficeNameEn);
		ps.setObject(index++,vm_fuel_requestDTO.paymentReceiverOfficeNameBn);
		ps.setObject(index++,vm_fuel_requestDTO.paymentReceiverOfficeUnitNameEn);
		ps.setObject(index++,vm_fuel_requestDTO.paymentReceiverOfficeUnitNameBn);
		ps.setObject(index++,vm_fuel_requestDTO.paymentReceiverOfficeUnitOrgNameEn);
		ps.setObject(index++,vm_fuel_requestDTO.paymentReceiverOfficeUnitOrgNameBn);
		if(isInsert)
		{
			ps.setObject(index++, 0);
			ps.setObject(index++, lastModificationTime);
		}
	}
	
	public void get(Vm_fuel_requestDTO vm_fuel_requestDTO, ResultSet rs) throws SQLException
	{
		vm_fuel_requestDTO.iD = rs.getLong("ID");
		vm_fuel_requestDTO.insertedByUserId = rs.getLong("inserted_by_user_id");
		vm_fuel_requestDTO.insertedByOrganogramId = rs.getLong("inserted_by_organogram_id");
		vm_fuel_requestDTO.insertionDate = rs.getLong("insertion_date");
		vm_fuel_requestDTO.lastModificationTime = rs.getLong("lastModificationTime");
		vm_fuel_requestDTO.searchColumn = rs.getString("search_column");
		vm_fuel_requestDTO.vehicleType = rs.getInt("vehicle_type");
		vm_fuel_requestDTO.vehicleId = rs.getLong("vehicle_id");
		vm_fuel_requestDTO.applicationDate = rs.getLong("application_date");
		vm_fuel_requestDTO.vehicleDriverAssignmentId = rs.getLong("vehicle_driver_assignment_id");
		vm_fuel_requestDTO.lastFuelDate = rs.getLong("last_fuel_date");
		vm_fuel_requestDTO.status = rs.getInt("status");
		vm_fuel_requestDTO.fiscalYearId = rs.getLong("fiscal_year_id");
		vm_fuel_requestDTO.approvedDate = rs.getLong("approved_date");
		vm_fuel_requestDTO.vendorId = rs.getLong("vendor_id");
		vm_fuel_requestDTO.sarokNumber = rs.getString("sarok_number");
		vm_fuel_requestDTO.filesDropzone = rs.getLong("files_dropzone");
		vm_fuel_requestDTO.paymentRemarks = rs.getString("payment_remarks");
		vm_fuel_requestDTO.requesterOrgId = rs.getLong("requester_org_id");
		vm_fuel_requestDTO.requesterOfficeId = rs.getLong("requester_office_id");
		vm_fuel_requestDTO.requesterOfficeUnitId = rs.getLong("requester_office_unit_id");
		vm_fuel_requestDTO.requesterEmpId = rs.getLong("requester_emp_id");
		vm_fuel_requestDTO.requesterPhoneNum = rs.getString("requester_phone_num");
		vm_fuel_requestDTO.requesterNameEn = rs.getString("requester_name_en");
		vm_fuel_requestDTO.requesterNameBn = rs.getString("requester_name_bn");
		vm_fuel_requestDTO.requesterOfficeNameEn = rs.getString("requester_office_name_en");
		vm_fuel_requestDTO.requesterOfficeNameBn = rs.getString("requester_office_name_bn");
		vm_fuel_requestDTO.requesterOfficeUnitNameEn = rs.getString("requester_office_unit_name_en");
		vm_fuel_requestDTO.requesterOfficeUnitNameBn = rs.getString("requester_office_unit_name_bn");
		vm_fuel_requestDTO.requesterOfficeUnitOrgNameEn = rs.getString("requester_office_unit_org_name_en");
		vm_fuel_requestDTO.requesterOfficeUnitOrgNameBn = rs.getString("requester_office_unit_org_name_bn");
		vm_fuel_requestDTO.approverOrgId = rs.getLong("approver_org_id");
		vm_fuel_requestDTO.approverOfficeId = rs.getLong("approver_office_id");
		vm_fuel_requestDTO.approverOfficeUnitId = rs.getLong("approver_office_unit_id");
		vm_fuel_requestDTO.approverEmpId = rs.getLong("approver_emp_id");
		vm_fuel_requestDTO.approverPhoneNum = rs.getString("approver_phone_num");
		vm_fuel_requestDTO.approverNameEn = rs.getString("approver_name_en");
		vm_fuel_requestDTO.approverNameBn = rs.getString("approver_name_bn");
		vm_fuel_requestDTO.approverOfficeNameEn = rs.getString("approver_office_name_en");
		vm_fuel_requestDTO.approverOfficeNameBn = rs.getString("approver_office_name_bn");
		vm_fuel_requestDTO.approverOfficeUnitNameEn = rs.getString("approver_office_unit_name_en");
		vm_fuel_requestDTO.approverOfficeUnitNameBn = rs.getString("approver_office_unit_name_bn");
		vm_fuel_requestDTO.approverOfficeUnitOrgNameEn = rs.getString("approver_office_unit_org_name_en");
		vm_fuel_requestDTO.approverOfficeUnitOrgNameBn = rs.getString("approver_office_unit_org_name_bn");
		vm_fuel_requestDTO.paymentReceiverOrgId = rs.getLong("payment_receiver_org_id");
		vm_fuel_requestDTO.paymentReceiverOfficeId = rs.getLong("payment_receiver_office_id");
		vm_fuel_requestDTO.paymentReceiverOfficeUnitId = rs.getLong("payment_receiver_office_unit_id");
		vm_fuel_requestDTO.paymentReceiverEmpId = rs.getLong("payment_receiver_emp_id");
		vm_fuel_requestDTO.paymentReceiverPhoneNum = rs.getString("payment_receiver_phone_num");
		vm_fuel_requestDTO.paymentReceiverNameEn = rs.getString("payment_receiver_name_en");
		vm_fuel_requestDTO.paymentReceiverNameBn = rs.getString("payment_receiver_name_bn");
		vm_fuel_requestDTO.paymentReceiverOfficeNameEn = rs.getString("payment_receiver_office_name_en");
		vm_fuel_requestDTO.paymentReceiverOfficeNameBn = rs.getString("payment_receiver_office_name_bn");
		vm_fuel_requestDTO.paymentReceiverOfficeUnitNameEn = rs.getString("payment_receiver_office_unit_name_en");
		vm_fuel_requestDTO.paymentReceiverOfficeUnitNameBn = rs.getString("payment_receiver_office_unit_name_bn");
		vm_fuel_requestDTO.paymentReceiverOfficeUnitOrgNameEn = rs.getString("payment_receiver_office_unit_org_name_en");
		vm_fuel_requestDTO.paymentReceiverOfficeUnitOrgNameBn = rs.getString("payment_receiver_office_unit_org_name_bn");
		vm_fuel_requestDTO.isDeleted = rs.getInt("isDeleted");
	}



	public Vm_fuel_requestDTO build(ResultSet rs)
	{
		try
		{
			Vm_fuel_requestDTO vm_fuel_requestDTO = new Vm_fuel_requestDTO();
			vm_fuel_requestDTO.iD = rs.getLong("ID");
			vm_fuel_requestDTO.insertedByUserId = rs.getLong("inserted_by_user_id");
			vm_fuel_requestDTO.insertedByOrganogramId = rs.getLong("inserted_by_organogram_id");
			vm_fuel_requestDTO.insertionDate = rs.getLong("insertion_date");
			vm_fuel_requestDTO.lastModificationTime = rs.getLong("lastModificationTime");
			vm_fuel_requestDTO.searchColumn = rs.getString("search_column");
			vm_fuel_requestDTO.vehicleType = rs.getInt("vehicle_type");
			vm_fuel_requestDTO.vehicleId = rs.getLong("vehicle_id");
			vm_fuel_requestDTO.applicationDate = rs.getLong("application_date");
			vm_fuel_requestDTO.vehicleDriverAssignmentId = rs.getLong("vehicle_driver_assignment_id");
			vm_fuel_requestDTO.lastFuelDate = rs.getLong("last_fuel_date");
			vm_fuel_requestDTO.status = rs.getInt("status");
			vm_fuel_requestDTO.fiscalYearId = rs.getLong("fiscal_year_id");
			vm_fuel_requestDTO.approvedDate = rs.getLong("approved_date");
			vm_fuel_requestDTO.vendorId = rs.getLong("vendor_id");
			vm_fuel_requestDTO.sarokNumber = rs.getString("sarok_number");
			vm_fuel_requestDTO.filesDropzone = rs.getLong("files_dropzone");
			vm_fuel_requestDTO.paymentRemarks = rs.getString("payment_remarks");
			vm_fuel_requestDTO.requesterOrgId = rs.getLong("requester_org_id");
			vm_fuel_requestDTO.requesterOfficeId = rs.getLong("requester_office_id");
			vm_fuel_requestDTO.requesterOfficeUnitId = rs.getLong("requester_office_unit_id");
			vm_fuel_requestDTO.requesterEmpId = rs.getLong("requester_emp_id");
			vm_fuel_requestDTO.requesterPhoneNum = rs.getString("requester_phone_num");
			vm_fuel_requestDTO.requesterNameEn = rs.getString("requester_name_en");
			vm_fuel_requestDTO.requesterNameBn = rs.getString("requester_name_bn");
			vm_fuel_requestDTO.requesterOfficeNameEn = rs.getString("requester_office_name_en");
			vm_fuel_requestDTO.requesterOfficeNameBn = rs.getString("requester_office_name_bn");
			vm_fuel_requestDTO.requesterOfficeUnitNameEn = rs.getString("requester_office_unit_name_en");
			vm_fuel_requestDTO.requesterOfficeUnitNameBn = rs.getString("requester_office_unit_name_bn");
			vm_fuel_requestDTO.requesterOfficeUnitOrgNameEn = rs.getString("requester_office_unit_org_name_en");
			vm_fuel_requestDTO.requesterOfficeUnitOrgNameBn = rs.getString("requester_office_unit_org_name_bn");
			vm_fuel_requestDTO.approverOrgId = rs.getLong("approver_org_id");
			vm_fuel_requestDTO.approverOfficeId = rs.getLong("approver_office_id");
			vm_fuel_requestDTO.approverOfficeUnitId = rs.getLong("approver_office_unit_id");
			vm_fuel_requestDTO.approverEmpId = rs.getLong("approver_emp_id");
			vm_fuel_requestDTO.approverPhoneNum = rs.getString("approver_phone_num");
			vm_fuel_requestDTO.approverNameEn = rs.getString("approver_name_en");
			vm_fuel_requestDTO.approverNameBn = rs.getString("approver_name_bn");
			vm_fuel_requestDTO.approverOfficeNameEn = rs.getString("approver_office_name_en");
			vm_fuel_requestDTO.approverOfficeNameBn = rs.getString("approver_office_name_bn");
			vm_fuel_requestDTO.approverOfficeUnitNameEn = rs.getString("approver_office_unit_name_en");
			vm_fuel_requestDTO.approverOfficeUnitNameBn = rs.getString("approver_office_unit_name_bn");
			vm_fuel_requestDTO.approverOfficeUnitOrgNameEn = rs.getString("approver_office_unit_org_name_en");
			vm_fuel_requestDTO.approverOfficeUnitOrgNameBn = rs.getString("approver_office_unit_org_name_bn");
			vm_fuel_requestDTO.paymentReceiverOrgId = rs.getLong("payment_receiver_org_id");
			vm_fuel_requestDTO.paymentReceiverOfficeId = rs.getLong("payment_receiver_office_id");
			vm_fuel_requestDTO.paymentReceiverOfficeUnitId = rs.getLong("payment_receiver_office_unit_id");
			vm_fuel_requestDTO.paymentReceiverEmpId = rs.getLong("payment_receiver_emp_id");
			vm_fuel_requestDTO.paymentReceiverPhoneNum = rs.getString("payment_receiver_phone_num");
			vm_fuel_requestDTO.paymentReceiverNameEn = rs.getString("payment_receiver_name_en");
			vm_fuel_requestDTO.paymentReceiverNameBn = rs.getString("payment_receiver_name_bn");
			vm_fuel_requestDTO.paymentReceiverOfficeNameEn = rs.getString("payment_receiver_office_name_en");
			vm_fuel_requestDTO.paymentReceiverOfficeNameBn = rs.getString("payment_receiver_office_name_bn");
			vm_fuel_requestDTO.paymentReceiverOfficeUnitNameEn = rs.getString("payment_receiver_office_unit_name_en");
			vm_fuel_requestDTO.paymentReceiverOfficeUnitNameBn = rs.getString("payment_receiver_office_unit_name_bn");
			vm_fuel_requestDTO.paymentReceiverOfficeUnitOrgNameEn = rs.getString("payment_receiver_office_unit_org_name_en");
			vm_fuel_requestDTO.paymentReceiverOfficeUnitOrgNameBn = rs.getString("payment_receiver_office_unit_org_name_bn");
			vm_fuel_requestDTO.isDeleted = rs.getInt("isDeleted");
			return vm_fuel_requestDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}

	public Vm_fuel_requestDTO buildWithChildren(ResultSet rs)
	{
		try
		{
			Vm_fuel_requestDTO vm_fuel_requestDTO = new Vm_fuel_requestDTO();
			vm_fuel_requestDTO.iD = rs.getLong("ID");
			vm_fuel_requestDTO.insertedByUserId = rs.getLong("inserted_by_user_id");
			vm_fuel_requestDTO.insertedByOrganogramId = rs.getLong("inserted_by_organogram_id");
			vm_fuel_requestDTO.insertionDate = rs.getLong("insertion_date");
			vm_fuel_requestDTO.lastModificationTime = rs.getLong("lastModificationTime");
			vm_fuel_requestDTO.searchColumn = rs.getString("search_column");
			vm_fuel_requestDTO.vehicleType = rs.getInt("vehicle_type");
			vm_fuel_requestDTO.vehicleId = rs.getLong("vehicle_id");
			vm_fuel_requestDTO.applicationDate = rs.getLong("application_date");
			vm_fuel_requestDTO.vehicleDriverAssignmentId = rs.getLong("vehicle_driver_assignment_id");
			vm_fuel_requestDTO.lastFuelDate = rs.getLong("last_fuel_date");
			vm_fuel_requestDTO.status = rs.getInt("status");
			vm_fuel_requestDTO.fiscalYearId = rs.getLong("fiscal_year_id");
			vm_fuel_requestDTO.approvedDate = rs.getLong("approved_date");
			vm_fuel_requestDTO.vendorId = rs.getLong("vendor_id");
			vm_fuel_requestDTO.sarokNumber = rs.getString("sarok_number");
			vm_fuel_requestDTO.filesDropzone = rs.getLong("files_dropzone");
			vm_fuel_requestDTO.paymentRemarks = rs.getString("payment_remarks");
			vm_fuel_requestDTO.requesterOrgId = rs.getLong("requester_org_id");
			vm_fuel_requestDTO.requesterOfficeId = rs.getLong("requester_office_id");
			vm_fuel_requestDTO.requesterOfficeUnitId = rs.getLong("requester_office_unit_id");
			vm_fuel_requestDTO.requesterEmpId = rs.getLong("requester_emp_id");
			vm_fuel_requestDTO.requesterPhoneNum = rs.getString("requester_phone_num");
			vm_fuel_requestDTO.requesterNameEn = rs.getString("requester_name_en");
			vm_fuel_requestDTO.requesterNameBn = rs.getString("requester_name_bn");
			vm_fuel_requestDTO.requesterOfficeNameEn = rs.getString("requester_office_name_en");
			vm_fuel_requestDTO.requesterOfficeNameBn = rs.getString("requester_office_name_bn");
			vm_fuel_requestDTO.requesterOfficeUnitNameEn = rs.getString("requester_office_unit_name_en");
			vm_fuel_requestDTO.requesterOfficeUnitNameBn = rs.getString("requester_office_unit_name_bn");
			vm_fuel_requestDTO.requesterOfficeUnitOrgNameEn = rs.getString("requester_office_unit_org_name_en");
			vm_fuel_requestDTO.requesterOfficeUnitOrgNameBn = rs.getString("requester_office_unit_org_name_bn");
			vm_fuel_requestDTO.approverOrgId = rs.getLong("approver_org_id");
			vm_fuel_requestDTO.approverOfficeId = rs.getLong("approver_office_id");
			vm_fuel_requestDTO.approverOfficeUnitId = rs.getLong("approver_office_unit_id");
			vm_fuel_requestDTO.approverEmpId = rs.getLong("approver_emp_id");
			vm_fuel_requestDTO.approverPhoneNum = rs.getString("approver_phone_num");
			vm_fuel_requestDTO.approverNameEn = rs.getString("approver_name_en");
			vm_fuel_requestDTO.approverNameBn = rs.getString("approver_name_bn");
			vm_fuel_requestDTO.approverOfficeNameEn = rs.getString("approver_office_name_en");
			vm_fuel_requestDTO.approverOfficeNameBn = rs.getString("approver_office_name_bn");
			vm_fuel_requestDTO.approverOfficeUnitNameEn = rs.getString("approver_office_unit_name_en");
			vm_fuel_requestDTO.approverOfficeUnitNameBn = rs.getString("approver_office_unit_name_bn");
			vm_fuel_requestDTO.approverOfficeUnitOrgNameEn = rs.getString("approver_office_unit_org_name_en");
			vm_fuel_requestDTO.approverOfficeUnitOrgNameBn = rs.getString("approver_office_unit_org_name_bn");
			vm_fuel_requestDTO.paymentReceiverOrgId = rs.getLong("payment_receiver_org_id");
			vm_fuel_requestDTO.paymentReceiverOfficeId = rs.getLong("payment_receiver_office_id");
			vm_fuel_requestDTO.paymentReceiverOfficeUnitId = rs.getLong("payment_receiver_office_unit_id");
			vm_fuel_requestDTO.paymentReceiverEmpId = rs.getLong("payment_receiver_emp_id");
			vm_fuel_requestDTO.paymentReceiverPhoneNum = rs.getString("payment_receiver_phone_num");
			vm_fuel_requestDTO.paymentReceiverNameEn = rs.getString("payment_receiver_name_en");
			vm_fuel_requestDTO.paymentReceiverNameBn = rs.getString("payment_receiver_name_bn");
			vm_fuel_requestDTO.paymentReceiverOfficeNameEn = rs.getString("payment_receiver_office_name_en");
			vm_fuel_requestDTO.paymentReceiverOfficeNameBn = rs.getString("payment_receiver_office_name_bn");
			vm_fuel_requestDTO.paymentReceiverOfficeUnitNameEn = rs.getString("payment_receiver_office_unit_name_en");
			vm_fuel_requestDTO.paymentReceiverOfficeUnitNameBn = rs.getString("payment_receiver_office_unit_name_bn");
			vm_fuel_requestDTO.paymentReceiverOfficeUnitOrgNameEn = rs.getString("payment_receiver_office_unit_org_name_en");
			vm_fuel_requestDTO.paymentReceiverOfficeUnitOrgNameBn = rs.getString("payment_receiver_office_unit_org_name_bn");
			vm_fuel_requestDTO.isDeleted = rs.getInt("isDeleted");
			List<VmFuelRequestItemDTO> vmFuelRequestItemDTOS = VmFuelRequestItemRepository.getInstance().getVmFuelRequestItemDTOByvmFuelRequestId(vm_fuel_requestDTO.iD);
			vm_fuel_requestDTO.vmFuelRequestItemDTOList = vmFuelRequestItemDTOS;
			return vm_fuel_requestDTO;
		}
		catch (Exception ex)
		{
			logger.error(ex);
			return null;
		}
	}

	//need another getter for repository
	public Vm_fuel_requestDTO getDTOByID (long ID)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID = ? ";
		Vm_fuel_requestDTO vm_fuel_requestDTO =
				ConnectionAndStatementUtil.getT(sql, Arrays.asList(ID), this::buildWithChildren);
		return vm_fuel_requestDTO;
	}
	
	
	
	
	public List<Vm_fuel_requestDTO> getDTOs(Collection recordIDs)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID IN ( ";
		for(int i = 0;i<recordIDs.size();i++){
			if(i!=0){
				sql+=",";
			}
			sql+=((ArrayList)recordIDs).get(i);
		}
		sql+=")  order by lastModificationTime desc";
		printSql(sql);
		return ConnectionAndStatementUtil.getListOfT(sql, Arrays.asList(recordIDs.toArray()),this::build);
	}
	
	

	
	
	
	//add repository
	public List<Vm_fuel_requestDTO> getAllVm_fuel_request (boolean isFirstReload)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ";
		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;
		}
		sql += " order by " + tableName + ".lastModificationTime desc";
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);
	}

	
	public List<Vm_fuel_requestDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO)
	{
		return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
	}


	public List<Vm_fuel_requestDTO> getDTOs(Hashtable  p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
			String filter, boolean tableHasJobCat)
	{
		List<Object> objectList = new ArrayList<Object>();

		String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat, objectList);
		return ConnectionAndStatementUtil.getListOfT(sql,objectList,this::buildWithChildren);
	}

	public List<Vm_fuel_requestDTO> getDTOsSortedBy(Hashtable  p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
											String filter, boolean tableHasJobCat, String sortColumn)
	{
		String sql = getSqlWithSearchCriteriaForNormalSearch(p_searchCriteria, limit, offset, GETDTOS, userDTO, filter, tableHasJobCat, new ArrayList<>(), sortColumn);
		return ConnectionAndStatementUtil.getListOfT(sql,this::buildWithChildren);
	}

	public String getSqlWithSearchCriteriaForNormalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
			UserDTO userDTO, String filter, boolean tableHasJobCat, List<Object> objectList)
    {
		boolean viewAll = false;
		
		if(p_searchCriteria != null && p_searchCriteria.get("ViewAll") != null)
		{
			System.out.println("ViewAll = " + p_searchCriteria.get("ViewAll"));
			viewAll = true;
		}
		else
		{
			System.out.println("ViewAll is null ");
		}
		
		String sql = "SELECT ";
		if(category == GETIDS)
		{
			sql += " " + tableName + ".ID as ID ";
		}
		else if(category == GETCOUNT)
		{
			sql += " count( " + tableName + ".ID) as countID ";
		}
		else if(category == GETDTOS)
		{
			sql += " distinct " + tableName + ".* ";
		}
		sql += "FROM " + tableName + " ";
		sql += joinSQL;
		

		
		String AnyfieldSql = "";
		String AllFieldSql = "";
		
		if(p_searchCriteria != null)
		{
						
			
			Enumeration names = p_searchCriteria.keys();
			String str, value;
			
			AnyfieldSql = "(";
			
			if(p_searchCriteria.get("AnyField")!= null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase(""))
			{
				AnyfieldSql+= tableName + ".search_column like ?";
				objectList.add("%" + p_searchCriteria.get("AnyField").toString() + "%");
			}
			AnyfieldSql += ")";
			System.out.println("AnyfieldSql = " + AnyfieldSql);
			
			AllFieldSql = "(";
			int i = 0;
			while(names.hasMoreElements())
			{				
				str = (String) names.nextElement();
				value = (String)p_searchCriteria.get(str);
		        System.out.println(str + ": " + value);
				if(value != null && !value.equalsIgnoreCase("") && (
						 str.equals("insertion_date_start")
						|| str.equals("insertion_date_end")
						|| str.equals("fiscal_year_id")
						|| str.equals("vehicle_type")
						|| str.equals("vehicle_id")
						|| str.equals("vendor_id")
						|| str.equals("application_date_start")
						|| str.equals("application_date_end")
						|| str.equals("last_fuel_date_start")
						|| str.equals("last_fuel_date_end")
						|| str.equals("approved_date_start")
						|| str.equals("approved_date_end")
						|| str.equals("sarok_number")
						|| str.equals("payment_remarks")
						|| str.equals("requester_phone_num")
						|| str.equals("requester_name_en")
						|| str.equals("requester_name_bn")
						|| str.equals("requester_office_name_en")
						|| str.equals("requester_office_name_bn")
						|| str.equals("requester_office_unit_name_en")
						|| str.equals("requester_office_unit_name_bn")
						|| str.equals("requester_office_unit_org_name_en")
						|| str.equals("requester_office_unit_org_name_bn")
						|| str.equals("approver_phone_num")
						|| str.equals("approver_name_en")
						|| str.equals("approver_name_bn")
						|| str.equals("approver_office_name_en")
						|| str.equals("approver_office_name_bn")
						|| str.equals("approver_office_unit_name_en")
						|| str.equals("approver_office_unit_name_bn")
						|| str.equals("approver_office_unit_org_name_en")
						|| str.equals("approver_office_unit_org_name_bn")
						|| str.equals("payment_receiver_phone_num")
						|| str.equals("payment_receiver_name_en")
						|| str.equals("payment_receiver_name_bn")
						|| str.equals("payment_receiver_office_name_en")
						|| str.equals("payment_receiver_office_name_bn")
						|| str.equals("payment_receiver_office_unit_name_en")
						|| str.equals("payment_receiver_office_unit_name_bn")
						|| str.equals("payment_receiver_office_unit_org_name_en")
						|| str.equals("payment_receiver_office_unit_org_name_bn")
				)
						
				)
				{
					if(p_searchCriteria.get(str).equals("any"))
					{
						continue;
					}

					if( i > 0)
					{
						AllFieldSql+= " AND  ";
					}
					
					 if(str.equals("insertion_date_start"))
					{
						AllFieldSql += "" + tableName + ".insertion_date >= " + "?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
					else if(str.equals("insertion_date_end"))
					{
						AllFieldSql += "" + tableName + ".insertion_date <= " + "?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
					else if(str.equals("fiscal_year_id"))
					{
						AllFieldSql += "" + tableName + ".fiscal_year_id = " + "?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
					else if(str.equals("vehicle_type"))
					{
						AllFieldSql += "" + tableName + ".vehicle_type = " + "?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
					else if(str.equals("vehicle_id"))
					{
						AllFieldSql += "" + tableName + ".vehicle_id = " + "?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
					else if(str.equals("vendor_id"))
					{
						AllFieldSql += "" + tableName + ".vendor_id = " + "?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
					else if(str.equals("application_date_start"))
					{
						AllFieldSql += "" + tableName + ".application_date >= " + "?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
					else if(str.equals("application_date_end"))
					{
						AllFieldSql += "" + tableName + ".application_date <= " + "?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
					else if(str.equals("last_fuel_date_start"))
					{
						AllFieldSql += "" + tableName + ".last_fuel_date >= " + "?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
					else if(str.equals("last_fuel_date_end"))
					{
						AllFieldSql += "" + tableName + ".last_fuel_date <= " + "?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
					else if(str.equals("approved_date_start"))
					{
						AllFieldSql += "" + tableName + ".approved_date >= " + "?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
					else if(str.equals("approved_date_end"))
					{
						AllFieldSql += "" + tableName + ".approved_date <= " + "?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
					else if(str.equals("sarok_number"))
					{
						AllFieldSql += "" + tableName + ".sarok_number like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("payment_remarks"))
					{
						AllFieldSql += "" + tableName + ".payment_remarks like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("requester_phone_num"))
					{
						AllFieldSql += "" + tableName + ".requester_phone_num like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("requester_name_en"))
					{
						AllFieldSql += "" + tableName + ".requester_name_en like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("requester_name_bn"))
					{
						AllFieldSql += "" + tableName + ".requester_name_bn like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("requester_office_name_en"))
					{
						AllFieldSql += "" + tableName + ".requester_office_name_en like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("requester_office_name_bn"))
					{
						AllFieldSql += "" + tableName + ".requester_office_name_bn like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("requester_office_unit_name_en"))
					{
						AllFieldSql += "" + tableName + ".requester_office_unit_name_en like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("requester_office_unit_name_bn"))
					{
						AllFieldSql += "" + tableName + ".requester_office_unit_name_bn like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("requester_office_unit_org_name_en"))
					{
						AllFieldSql += "" + tableName + ".requester_office_unit_org_name_en like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("requester_office_unit_org_name_bn"))
					{
						AllFieldSql += "" + tableName + ".requester_office_unit_org_name_bn like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("approver_phone_num"))
					{
						AllFieldSql += "" + tableName + ".approver_phone_num like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("approver_name_en"))
					{
						AllFieldSql += "" + tableName + ".approver_name_en like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("approver_name_bn"))
					{
						AllFieldSql += "" + tableName + ".approver_name_bn like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("approver_office_name_en"))
					{
						AllFieldSql += "" + tableName + ".approver_office_name_en like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("approver_office_name_bn"))
					{
						AllFieldSql += "" + tableName + ".approver_office_name_bn like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("approver_office_unit_name_en"))
					{
						AllFieldSql += "" + tableName + ".approver_office_unit_name_en like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("approver_office_unit_name_bn"))
					{
						AllFieldSql += "" + tableName + ".approver_office_unit_name_bn like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("approver_office_unit_org_name_en"))
					{
						AllFieldSql += "" + tableName + ".approver_office_unit_org_name_en like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("approver_office_unit_org_name_bn"))
					{
						AllFieldSql += "" + tableName + ".approver_office_unit_org_name_bn like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("payment_receiver_phone_num"))
					{
						AllFieldSql += "" + tableName + ".payment_receiver_phone_num like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("payment_receiver_name_en"))
					{
						AllFieldSql += "" + tableName + ".payment_receiver_name_en like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("payment_receiver_name_bn"))
					{
						AllFieldSql += "" + tableName + ".payment_receiver_name_bn like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("payment_receiver_office_name_en"))
					{
						AllFieldSql += "" + tableName + ".payment_receiver_office_name_en like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("payment_receiver_office_name_bn"))
					{
						AllFieldSql += "" + tableName + ".payment_receiver_office_name_bn like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("payment_receiver_office_unit_name_en"))
					{
						AllFieldSql += "" + tableName + ".payment_receiver_office_unit_name_en like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("payment_receiver_office_unit_name_bn"))
					{
						AllFieldSql += "" + tableName + ".payment_receiver_office_unit_name_bn like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("payment_receiver_office_unit_org_name_en"))
					{
						AllFieldSql += "" + tableName + ".payment_receiver_office_unit_org_name_en like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("payment_receiver_office_unit_org_name_bn"))
					{
						AllFieldSql += "" + tableName + ".payment_receiver_office_unit_org_name_bn like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					
					
				}
			}
			
			AllFieldSql += ")";
			System.out.println("AllFieldSql = " + AllFieldSql);
			
			
		}
		
		
		sql += " WHERE ";
		
		sql += " (" + tableName + ".isDeleted = 0 ";			
		sql += ")";
		
		
		if(!filter.equalsIgnoreCase(""))
		{
			sql += " and " + filter + " ";
		}
		
		if(!AnyfieldSql.equals("()") && !AnyfieldSql.equals(""))
		{
			sql += " AND " + AnyfieldSql;
			
		}
		if(!AllFieldSql.equals("()") && !AllFieldSql.equals(""))
		{			
			sql += " AND " + AllFieldSql;
		}
		
	
			
		sql += " order by " + tableName + ".lastModificationTime desc ";

		printSql(sql);
		
		if(limit >= 0)
		{
			sql += " limit " + limit;
		}
		if(offset >= 0)
		{
			sql += " offset " + offset;
		}
		
		System.out.println("-------------- sql = " + sql);
		
		return sql;
    }

	public String getSqlWithSearchCriteriaForNormalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
														  UserDTO userDTO, String filter, boolean tableHasJobCat, List<Object> objectList, String sortClumn)
	{
		boolean viewAll = false;

		if(p_searchCriteria != null && p_searchCriteria.get("ViewAll") != null)
		{
			System.out.println("ViewAll = " + p_searchCriteria.get("ViewAll"));
			viewAll = true;
		}
		else
		{
			System.out.println("ViewAll is null ");
		}

		String sql = "SELECT ";
		if(category == GETIDS)
		{
			sql += " " + tableName + ".ID as ID ";
		}
		else if(category == GETCOUNT)
		{
			sql += " count( " + tableName + ".ID) as countID ";
		}
		else if(category == GETDTOS)
		{
			sql += " distinct " + tableName + ".* ";
		}
		sql += "FROM " + tableName + " ";
		sql += joinSQL;



		String AnyfieldSql = "";
		String AllFieldSql = "";

		if(p_searchCriteria != null)
		{


			Enumeration names = p_searchCriteria.keys();
			String str, value;

			AnyfieldSql = "(";

			if(p_searchCriteria.get("AnyField")!= null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase(""))
			{
				AnyfieldSql+= tableName + ".search_column like ?";
				objectList.add("%" + p_searchCriteria.get("AnyField").toString() + "%");
			}
			AnyfieldSql += ")";
			System.out.println("AnyfieldSql = " + AnyfieldSql);

			AllFieldSql = "(";
			int i = 0;
			while(names.hasMoreElements())
			{
				str = (String) names.nextElement();
				value = (String)p_searchCriteria.get(str);
				System.out.println(str + ": " + value);
				if(value != null && !value.equalsIgnoreCase("") && (
						str.equals("insertion_date_start")
								|| str.equals("insertion_date_end")
								|| str.equals("fiscal_year_id")
								|| str.equals("vehicle_type")
								|| str.equals("vehicle_id")
								|| str.equals("vendor_id")
								|| str.equals("application_date_start")
								|| str.equals("application_date_end")
								|| str.equals("last_fuel_date_start")
								|| str.equals("last_fuel_date_end")
								|| str.equals("approved_date_start")
								|| str.equals("approved_date_end")
								|| str.equals("sarok_number")
								|| str.equals("payment_remarks")
								|| str.equals("requester_phone_num")
								|| str.equals("requester_name_en")
								|| str.equals("requester_name_bn")
								|| str.equals("requester_office_name_en")
								|| str.equals("requester_office_name_bn")
								|| str.equals("requester_office_unit_name_en")
								|| str.equals("requester_office_unit_name_bn")
								|| str.equals("requester_office_unit_org_name_en")
								|| str.equals("requester_office_unit_org_name_bn")
								|| str.equals("approver_phone_num")
								|| str.equals("approver_name_en")
								|| str.equals("approver_name_bn")
								|| str.equals("approver_office_name_en")
								|| str.equals("approver_office_name_bn")
								|| str.equals("approver_office_unit_name_en")
								|| str.equals("approver_office_unit_name_bn")
								|| str.equals("approver_office_unit_org_name_en")
								|| str.equals("approver_office_unit_org_name_bn")
								|| str.equals("payment_receiver_phone_num")
								|| str.equals("payment_receiver_name_en")
								|| str.equals("payment_receiver_name_bn")
								|| str.equals("payment_receiver_office_name_en")
								|| str.equals("payment_receiver_office_name_bn")
								|| str.equals("payment_receiver_office_unit_name_en")
								|| str.equals("payment_receiver_office_unit_name_bn")
								|| str.equals("payment_receiver_office_unit_org_name_en")
								|| str.equals("payment_receiver_office_unit_org_name_bn")
				)

				)
				{
					if(p_searchCriteria.get(str).equals("any"))
					{
						continue;
					}

					if( i > 0)
					{
						AllFieldSql+= " AND  ";
					}

					if(str.equals("insertion_date_start"))
					{
						AllFieldSql += "" + tableName + ".insertion_date >= " + "?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
					else if(str.equals("insertion_date_end"))
					{
						AllFieldSql += "" + tableName + ".insertion_date <= " + "?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
					else if(str.equals("fiscal_year_id"))
					{
						AllFieldSql += "" + tableName + ".fiscal_year_id = " + "?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
					else if(str.equals("vehicle_type"))
					{
						AllFieldSql += "" + tableName + ".vehicle_type = " + "?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
					else if(str.equals("vehicle_id"))
					{
						AllFieldSql += "" + tableName + ".vehicle_id = " + "?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
					else if(str.equals("vendor_id"))
					{
						AllFieldSql += "" + tableName + ".vendor_id = " + "?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
					else if(str.equals("application_date_start"))
					{
						AllFieldSql += "" + tableName + ".application_date >= " + "?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
					else if(str.equals("application_date_end"))
					{
						AllFieldSql += "" + tableName + ".application_date <= " + "?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
					else if(str.equals("last_fuel_date_start"))
					{
						AllFieldSql += "" + tableName + ".last_fuel_date >= " + "?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
					else if(str.equals("last_fuel_date_end"))
					{
						AllFieldSql += "" + tableName + ".last_fuel_date <= " + "?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
					else if(str.equals("approved_date_start"))
					{
						AllFieldSql += "" + tableName + ".approved_date >= " + "?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
					else if(str.equals("approved_date_end"))
					{
						AllFieldSql += "" + tableName + ".approved_date <= " + "?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
					else if(str.equals("sarok_number"))
					{
						AllFieldSql += "" + tableName + ".sarok_number like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("payment_remarks"))
					{
						AllFieldSql += "" + tableName + ".payment_remarks like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("requester_phone_num"))
					{
						AllFieldSql += "" + tableName + ".requester_phone_num like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("requester_name_en"))
					{
						AllFieldSql += "" + tableName + ".requester_name_en like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("requester_name_bn"))
					{
						AllFieldSql += "" + tableName + ".requester_name_bn like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("requester_office_name_en"))
					{
						AllFieldSql += "" + tableName + ".requester_office_name_en like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("requester_office_name_bn"))
					{
						AllFieldSql += "" + tableName + ".requester_office_name_bn like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("requester_office_unit_name_en"))
					{
						AllFieldSql += "" + tableName + ".requester_office_unit_name_en like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("requester_office_unit_name_bn"))
					{
						AllFieldSql += "" + tableName + ".requester_office_unit_name_bn like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("requester_office_unit_org_name_en"))
					{
						AllFieldSql += "" + tableName + ".requester_office_unit_org_name_en like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("requester_office_unit_org_name_bn"))
					{
						AllFieldSql += "" + tableName + ".requester_office_unit_org_name_bn like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("approver_phone_num"))
					{
						AllFieldSql += "" + tableName + ".approver_phone_num like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("approver_name_en"))
					{
						AllFieldSql += "" + tableName + ".approver_name_en like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("approver_name_bn"))
					{
						AllFieldSql += "" + tableName + ".approver_name_bn like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("approver_office_name_en"))
					{
						AllFieldSql += "" + tableName + ".approver_office_name_en like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("approver_office_name_bn"))
					{
						AllFieldSql += "" + tableName + ".approver_office_name_bn like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("approver_office_unit_name_en"))
					{
						AllFieldSql += "" + tableName + ".approver_office_unit_name_en like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("approver_office_unit_name_bn"))
					{
						AllFieldSql += "" + tableName + ".approver_office_unit_name_bn like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("approver_office_unit_org_name_en"))
					{
						AllFieldSql += "" + tableName + ".approver_office_unit_org_name_en like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("approver_office_unit_org_name_bn"))
					{
						AllFieldSql += "" + tableName + ".approver_office_unit_org_name_bn like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("payment_receiver_phone_num"))
					{
						AllFieldSql += "" + tableName + ".payment_receiver_phone_num like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("payment_receiver_name_en"))
					{
						AllFieldSql += "" + tableName + ".payment_receiver_name_en like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("payment_receiver_name_bn"))
					{
						AllFieldSql += "" + tableName + ".payment_receiver_name_bn like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("payment_receiver_office_name_en"))
					{
						AllFieldSql += "" + tableName + ".payment_receiver_office_name_en like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("payment_receiver_office_name_bn"))
					{
						AllFieldSql += "" + tableName + ".payment_receiver_office_name_bn like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("payment_receiver_office_unit_name_en"))
					{
						AllFieldSql += "" + tableName + ".payment_receiver_office_unit_name_en like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("payment_receiver_office_unit_name_bn"))
					{
						AllFieldSql += "" + tableName + ".payment_receiver_office_unit_name_bn like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("payment_receiver_office_unit_org_name_en"))
					{
						AllFieldSql += "" + tableName + ".payment_receiver_office_unit_org_name_en like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("payment_receiver_office_unit_org_name_bn"))
					{
						AllFieldSql += "" + tableName + ".payment_receiver_office_unit_org_name_bn like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}


				}
			}

			AllFieldSql += ")";
			System.out.println("AllFieldSql = " + AllFieldSql);


		}


		sql += " WHERE ";

		sql += " (" + tableName + ".isDeleted = 0 ";
		sql += ")";


		if(!filter.equalsIgnoreCase(""))
		{
			sql += " and " + filter + " ";
		}

		if(!AnyfieldSql.equals("()") && !AnyfieldSql.equals(""))
		{
			sql += " AND " + AnyfieldSql;

		}
		if(!AllFieldSql.equals("()") && !AllFieldSql.equals(""))
		{
			sql += " AND " + AllFieldSql;
		}



		sql += " order by " + tableName + "." + sortClumn + " desc ";

		printSql(sql);

		if(limit >= 0)
		{
			sql += " limit " + limit;
		}
		if(offset >= 0)
		{
			sql += " offset " + offset;
		}

		System.out.println("-------------- sql = " + sql);

		return sql;
	}
				
}
	