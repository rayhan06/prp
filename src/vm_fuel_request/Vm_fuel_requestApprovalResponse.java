package vm_fuel_request;

/*
 * @author Md. Erfan Hossain
 * @created 03/05/2021 - 11:18 PM
 * @project parliament
 */

import java.util.List;

public class Vm_fuel_requestApprovalResponse {
    public boolean hasNextApproval;
    public List<Long> organogramIds;
}
