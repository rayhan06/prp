package vm_fuel_request;

import java.io.IOException;
import java.io.*;
import java.text.SimpleDateFormat;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import employee_assign.EmployeeSearchModel;
import fiscal_year.Fiscal_yearDAO;
import office_units.Office_unitsRepository;
import offices.OfficesDTO;
import offices.OfficesRepository;
import org.apache.log4j.Logger;

import login.LoginDTO;
import permission.MenuConstants;
import role.PermissionRepository;


import role.RoleDTO;
import sessionmanager.SessionConstants;

import task_type.TaskTypeEnum;
import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;
import util.CommonRequestHandler;
import util.RecordNavigationManager4;

import java.util.*;
import javax.servlet.http.*;
import java.util.UUID;


import geolocation.GeoLocationDAO2;

import java.util.StringTokenizer;
import java.util.stream.Collectors;

import com.google.gson.Gson;

import pb.*;
import pbReport.*;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;

import files.*;
import vm_fuel_request_approval_mapping.CreateVm_fuel_requestApprovalModel;
import vm_fuel_request_approval_mapping.Vm_fuel_request_approval_mappingDAO;
import vm_fuel_vendor.VmFuelVendorItemDAO;
import vm_fuel_vendor.VmFuelVendorItemDTO;
import vm_requisition.CommonApprovalStatus;
import vm_requisition.Vm_requisitionDAO;
import vm_requisition.Vm_requisitionDTO;
import vm_vehicle.*;
import vm_vehicle_driver_assignment.Vm_vehicle_driver_assignmentDAO;
import vm_vehicle_driver_assignment.Vm_vehicle_driver_assignmentDTO;
import vm_vehicle_office_assignment.Vm_vehicle_office_assignmentDAO;
import vm_vehicle_office_assignment.Vm_vehicle_office_assignmentDTO;


/**
 * Servlet implementation class Vm_fuel_requestServlet
 */
@WebServlet("/Vm_fuel_requestServlet")
@MultipartConfig
public class Vm_fuel_requestServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Vm_fuel_requestServlet.class);

    String tableName = "vm_fuel_request";

    Vm_fuel_requestDAO vm_fuel_requestDAO;
    CommonRequestHandler commonRequestHandler;
    FilesDAO filesDAO = new FilesDAO();
    VmFuelRequestItemDAO vmFuelRequestItemDAO;
    private final Gson gson = new Gson();

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Vm_fuel_requestServlet() {
        super();
        try {
            vm_fuel_requestDAO = new Vm_fuel_requestDAO(tableName);
            vmFuelRequestItemDAO = new VmFuelRequestItemDAO("vm_fuel_request_item");
            commonRequestHandler = new CommonRequestHandler(vm_fuel_requestDAO);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("In doget request = " + request);
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        boolean isPermanentTable = true;
        if (request.getParameter("isPermanentTable") != null) {
            isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
        }
        System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
        try {
            String actionType = request.getParameter("actionType");

            if (!actionType.equals("search") && !actionType.equals("add")) {
                if (request.getParameter("ID") != null) {
                    RoleDTO role = PermissionRepository.getRoleDTOByRoleID(userDTO.roleID);
                    boolean isAdmin = role.ID == SessionConstants.VEHICLE_ADMIN_ROLE || role.ID == SessionConstants.ADMIN_ROLE;

                    String filterOwn = " (requester_org_id = " + userDTO.organogramID + " OR inserted_by_organogram_id = " + userDTO.organogramID;
                    filterOwn += isAdmin ? " OR true)" : ")";
                    filterOwn += " AND ";
                    filterOwn += " ID = " + Long.parseLong(request.getParameter("ID")) + " ";
                    int pendingRequest = vm_fuel_requestDAO.getCount(null, 1, 0, true, userDTO, filterOwn, false);
                    if (pendingRequest != 1) {
                        throw new card_info.InvalidDataException("This employee has no request for id : " + request.getParameter("ID"));
                    }
                }
            }

            if (actionType.equals("getAddPage")) {
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_FUEL_REQUEST_ADD)) {
                    commonRequestHandler.getAddPage(request, response);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else if (actionType.equals("getEditPage")) {
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_FUEL_REQUEST_UPDATE)) {
                    getVm_fuel_request(request, response);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else if (actionType.equals("getReportDataFuelRequest")) {
                getReportDataFuelRequest(request, response);
            } else if (actionType.equals("downloadDropzoneFile")) {
                commonRequestHandler.downloadDropzoneFile(request, response, filesDAO);
            } else if (actionType.equals("DeleteFileFromDropZone")) {
                commonRequestHandler.deleteFileFromDropZone(request, response, filesDAO);
            } else if (actionType.equals("getURL")) {
                String URL = request.getParameter("URL");
                System.out.println("URL = " + URL);
                response.sendRedirect(URL);
            } else if (actionType.equals("search")) {
                System.out.println("search requested");
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_FUEL_REQUEST_SEARCH)) {
                    if (isPermanentTable) {
                        String filter = request.getParameter("filter");
                        System.out.println("filter = " + filter);
                        if (filter != null) {
                            filter = ""; //shouldn't be directly used, rather manipulate it.
                            searchVm_fuel_request(request, response, isPermanentTable, filter);
                        } else {
                            searchVm_fuel_request(request, response, isPermanentTable, "");
                        }
                    } else {
                        //searchVm_fuel_request(request, response, tempTableName, isPermanentTable);
                    }
                }
            } else if (actionType.equals("billRegister")) {
                System.out.println("search requested");
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_FUEL_REQUEST_SEARCH)) {
                    if (isPermanentTable) {
                        String filter = request.getParameter("filter");
                        billRegister(request, response, isPermanentTable, filter != null ? filter : "");
                    }

                }
            } else if (actionType.equals("view")) {
                System.out.println("view requested");
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_FUEL_REQUEST_SEARCH)) {
                    commonRequestHandler.view(request, response);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }

            } else {
                request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.debug(ex);
        }
    }


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        System.out.println("doPost");
        boolean isPermanentTable = true;
        if (request.getParameter("isPermanentTable") != null) {
            isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
        }
        System.out.println("In servlet, isPermanentTable = " + isPermanentTable);

        try {
            String actionType = request.getParameter("actionType");
            System.out.println("actionType = " + actionType);

            if (!actionType.equals("search") && !actionType.equals("add")) {
                if (request.getParameter("iD") != null) {
                    RoleDTO role = PermissionRepository.getRoleDTOByRoleID(userDTO.roleID);
                    boolean isAdmin = role.ID == SessionConstants.VEHICLE_ADMIN_ROLE || role.ID == SessionConstants.ADMIN_ROLE;

                    String filterOwn = " (requester_org_id = " + userDTO.organogramID + " OR inserted_by_organogram_id = " + userDTO.organogramID;
                    filterOwn += isAdmin ? " OR true)" : ")";
                    filterOwn += " AND ";
                    filterOwn += " ID = " + Long.parseLong(request.getParameter("iD")) + " ";
                    int pendingRequest = vm_fuel_requestDAO.getCount(null, 1, 0, true, userDTO, filterOwn, false);
                    if (pendingRequest != 1) {
                        throw new card_info.InvalidDataException("This employee has no request for id : " + request.getParameter("iD"));
                    }
                }
            }

            if (actionType.equals("add")) {

                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_FUEL_REQUEST_ADD)) {
                    System.out.println("going to  addVm_fuel_request ");
                    addVm_fuel_request(request, response, true, userDTO, true);
                } else {
                    System.out.println("Not going to  addVm_fuel_request ");
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }

            } else if (actionType.equals("UploadFilesFromDropZone")) {
                commonRequestHandler.UploadFilesFromDropZone(request, response, userDTO);
            } else if (actionType.equals("getDTO")) {

                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_FUEL_REQUEST_ADD)) {
                    getDTO(request, response);
                } else {
                    System.out.println("Not going to  addVm_fuel_request ");
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }

            } else if (actionType.equals("edit")) {

                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_FUEL_REQUEST_UPDATE)) {
                    addVm_fuel_request(request, response, false, userDTO, isPermanentTable);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else if (actionType.equals("delete")) {
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_FUEL_REQUEST_UPDATE)) {
                    commonRequestHandler.delete(request, response, userDTO);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else if (actionType.equals("search")) {
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_FUEL_REQUEST_SEARCH)) {
                    searchVm_fuel_request(request, response, true, "");
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else if (actionType.equals("billRegister")) {
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_FUEL_REQUEST_SEARCH)) {
                    billRegister(request, response, true, "");
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else if (actionType.equals("getGeo")) {
                System.out.println("going to geoloc ");
                request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            logger.debug(ex);
        }
    }

    private void getDTO(HttpServletRequest request, HttpServletResponse response) {
        try {
            System.out.println("In getDTO");
            Vm_fuel_requestDTO vm_fuel_requestDTO = Vm_fuel_requestRepository.getInstance().getVm_fuel_requestDTOByID(Long.parseLong(request.getParameter("ID")));
            PrintWriter out = response.getWriter();
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");

            String encoded = this.gson.toJson(vm_fuel_requestDTO);
            System.out.println("json encoded data = " + encoded);
            out.print(encoded);
            out.flush();
        } catch (NumberFormatException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    private void addVm_fuel_request(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, boolean isPermanentTable) throws IOException {
        // TODO Auto-generated method stub
        try {
            request.setAttribute("failureMessage", "");
            System.out.println("%%%% addVm_fuel_request");
            String path = getServletContext().getRealPath("/img2/");
            Vm_fuel_requestDTO vm_fuel_requestDTO;
            SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
            boolean valid = true;

            if (addFlag == true) {
                vm_fuel_requestDTO = new Vm_fuel_requestDTO();
            } else {
                vm_fuel_requestDTO = Vm_fuel_requestRepository.getInstance().getVm_fuel_requestDTOByID(Long.parseLong(request.getParameter("iD")));
            }

            String Value = "";

            if (addFlag) {
                vm_fuel_requestDTO.insertedByUserId = userDTO.ID;
            }


            if (addFlag) {
                vm_fuel_requestDTO.insertedByOrganogramId = userDTO.organogramID;
            }


            if (addFlag) {
                Calendar c = Calendar.getInstance();
                c.set(Calendar.HOUR_OF_DAY, 0);
                c.set(Calendar.MINUTE, 0);
                c.set(Calendar.SECOND, 0);
                c.set(Calendar.MILLISECOND, 0);

                vm_fuel_requestDTO.insertionDate = c.getTimeInMillis();
            }


            Value = request.getParameter("searchColumn");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("searchColumn = " + Value);
            if (Value != null) {
                vm_fuel_requestDTO.searchColumn = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("vehicleType");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("vehicleType = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                vm_fuel_requestDTO.vehicleType = Integer.parseInt(Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("vehicleId");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("vehicleId = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("") && !Value.equals("-1")) {
                vm_fuel_requestDTO.vehicleId = Long.parseLong(Value);
            } else {
                valid = false;
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("applicationDate");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("applicationDate = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                try {
                    Date d = f.parse(Value);
                    vm_fuel_requestDTO.applicationDate = d.getTime();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("vehicleDriverAssignmentId");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("vehicleDriverAssignmentId = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                vm_fuel_requestDTO.vehicleDriverAssignmentId = Long.parseLong(Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("lastFuelDate");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("lastFuelDate = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                try {
                    Date d = f.parse(Value);
                    vm_fuel_requestDTO.lastFuelDate = d.getTime();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("status");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("status = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                vm_fuel_requestDTO.status = Integer.parseInt(Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            vm_fuel_requestDTO.status = CommonApprovalStatus.PENDING.getValue();

            vm_fuel_requestDTO.fiscalYearId = new Fiscal_yearDAO().getFiscalYearBYDateLong(vm_fuel_requestDTO.applicationDate).id;

            Value = request.getParameter("approvedDate");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("approvedDate = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                try {
                    Date d = f.parse(Value);
                    vm_fuel_requestDTO.approvedDate = d.getTime();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("vendorId");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("vendorId = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                vm_fuel_requestDTO.vendorId = Long.parseLong(Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("sarokNumber");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("sarokNumber = " + Value);
            if (Value != null) {
                vm_fuel_requestDTO.sarokNumber = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("filesDropzone");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("filesDropzone = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                System.out.println("filesDropzone = " + Value);

                vm_fuel_requestDTO.filesDropzone = Long.parseLong(Value);


                if (addFlag == false) {
                    String filesDropzoneFilesToDelete = request.getParameter("filesDropzoneFilesToDelete");
                    String[] deleteArray = filesDropzoneFilesToDelete.split(",");
                    for (int i = 0; i < deleteArray.length; i++) {
                        System.out.println("going to delete " + deleteArray[i]);
                        if (i > 0) {
                            filesDAO.delete(Long.parseLong(deleteArray[i]));
                        }
                    }
                }
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("paymentRemarks");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("paymentRemarks = " + Value);
            if (Value != null) {
                vm_fuel_requestDTO.paymentRemarks = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("requesterOrgId");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("requesterOrgId = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                vm_fuel_requestDTO.requesterOrgId = Long.parseLong(Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("requesterOfficeId");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("requesterOfficeId = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                vm_fuel_requestDTO.requesterOfficeId = Long.parseLong(Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("requesterOfficeUnitId");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("requesterOfficeUnitId = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                vm_fuel_requestDTO.requesterOfficeUnitId = Long.parseLong(Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("requesterPhoneNum");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("requesterPhoneNum = " + Value);
            if (Value != null) {
                vm_fuel_requestDTO.requesterPhoneNum = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("requesterNameEn");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("requesterNameEn = " + Value);
            if (Value != null) {
                vm_fuel_requestDTO.requesterNameEn = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("requesterNameBn");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("requesterNameBn = " + Value);
            if (Value != null) {
                vm_fuel_requestDTO.requesterNameBn = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("requesterOfficeNameEn");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("requesterOfficeNameEn = " + Value);
            if (Value != null) {
                vm_fuel_requestDTO.requesterOfficeNameEn = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("requesterOfficeNameBn");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("requesterOfficeNameBn = " + Value);
            if (Value != null) {
                vm_fuel_requestDTO.requesterOfficeNameBn = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("requesterOfficeUnitNameEn");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("requesterOfficeUnitNameEn = " + Value);
            if (Value != null) {
                vm_fuel_requestDTO.requesterOfficeUnitNameEn = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("requesterOfficeUnitNameBn");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("requesterOfficeUnitNameBn = " + Value);
            if (Value != null) {
                vm_fuel_requestDTO.requesterOfficeUnitNameBn = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("requesterOfficeUnitOrgNameEn");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("requesterOfficeUnitOrgNameEn = " + Value);
            if (Value != null) {
                vm_fuel_requestDTO.requesterOfficeUnitOrgNameEn = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("requesterOfficeUnitOrgNameBn");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("requesterOfficeUnitOrgNameBn = " + Value);
            if (Value != null) {
                vm_fuel_requestDTO.requesterOfficeUnitOrgNameBn = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("approverOrgId");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("approverOrgId = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                vm_fuel_requestDTO.approverOrgId = Long.parseLong(Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("approverOfficeId");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("approverOfficeId = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                vm_fuel_requestDTO.approverOfficeId = Long.parseLong(Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("approverOfficeUnitId");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("approverOfficeUnitId = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                vm_fuel_requestDTO.approverOfficeUnitId = Long.parseLong(Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("approverEmpId");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("approverEmpId = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                vm_fuel_requestDTO.approverEmpId = Long.parseLong(Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("approverPhoneNum");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("approverPhoneNum = " + Value);
            if (Value != null) {
                vm_fuel_requestDTO.approverPhoneNum = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("approverNameEn");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("approverNameEn = " + Value);
            if (Value != null) {
                vm_fuel_requestDTO.approverNameEn = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("approverNameBn");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("approverNameBn = " + Value);
            if (Value != null) {
                vm_fuel_requestDTO.approverNameBn = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("approverOfficeNameEn");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("approverOfficeNameEn = " + Value);
            if (Value != null) {
                vm_fuel_requestDTO.approverOfficeNameEn = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("approverOfficeNameBn");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("approverOfficeNameBn = " + Value);
            if (Value != null) {
                vm_fuel_requestDTO.approverOfficeNameBn = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("approverOfficeUnitNameEn");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("approverOfficeUnitNameEn = " + Value);
            if (Value != null) {
                vm_fuel_requestDTO.approverOfficeUnitNameEn = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("approverOfficeUnitNameBn");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("approverOfficeUnitNameBn = " + Value);
            if (Value != null) {
                vm_fuel_requestDTO.approverOfficeUnitNameBn = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("approverOfficeUnitOrgNameEn");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("approverOfficeUnitOrgNameEn = " + Value);
            if (Value != null) {
                vm_fuel_requestDTO.approverOfficeUnitOrgNameEn = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("approverOfficeUnitOrgNameBn");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("approverOfficeUnitOrgNameBn = " + Value);
            if (Value != null) {
                vm_fuel_requestDTO.approverOfficeUnitOrgNameBn = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("paymentReceiverOrgId");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("paymentReceiverOrgId = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                vm_fuel_requestDTO.paymentReceiverOrgId = Long.parseLong(Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("paymentReceiverOfficeId");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("paymentReceiverOfficeId = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                vm_fuel_requestDTO.paymentReceiverOfficeId = Long.parseLong(Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("paymentReceiverOfficeUnitId");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("paymentReceiverOfficeUnitId = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                vm_fuel_requestDTO.paymentReceiverOfficeUnitId = Long.parseLong(Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("paymentReceiverEmpId");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("paymentReceiverEmpId = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                vm_fuel_requestDTO.paymentReceiverEmpId = Long.parseLong(Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("paymentReceiverPhoneNum");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("paymentReceiverPhoneNum = " + Value);
            if (Value != null) {
                vm_fuel_requestDTO.paymentReceiverPhoneNum = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("paymentReceiverNameEn");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("paymentReceiverNameEn = " + Value);
            if (Value != null) {
                vm_fuel_requestDTO.paymentReceiverNameEn = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("paymentReceiverNameBn");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("paymentReceiverNameBn = " + Value);
            if (Value != null) {
                vm_fuel_requestDTO.paymentReceiverNameBn = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("paymentReceiverOfficeNameEn");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("paymentReceiverOfficeNameEn = " + Value);
            if (Value != null) {
                vm_fuel_requestDTO.paymentReceiverOfficeNameEn = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("paymentReceiverOfficeNameBn");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("paymentReceiverOfficeNameBn = " + Value);
            if (Value != null) {
                vm_fuel_requestDTO.paymentReceiverOfficeNameBn = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("paymentReceiverOfficeUnitNameEn");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("paymentReceiverOfficeUnitNameEn = " + Value);
            if (Value != null) {
                vm_fuel_requestDTO.paymentReceiverOfficeUnitNameEn = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("paymentReceiverOfficeUnitNameBn");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("paymentReceiverOfficeUnitNameBn = " + Value);
            if (Value != null) {
                vm_fuel_requestDTO.paymentReceiverOfficeUnitNameBn = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("paymentReceiverOfficeUnitOrgNameEn");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("paymentReceiverOfficeUnitOrgNameEn = " + Value);
            if (Value != null) {
                vm_fuel_requestDTO.paymentReceiverOfficeUnitOrgNameEn = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("paymentReceiverOfficeUnitOrgNameBn");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("paymentReceiverOfficeUnitOrgNameBn = " + Value);
            if (Value != null) {
                vm_fuel_requestDTO.paymentReceiverOfficeUnitOrgNameBn = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("requesterEmpId");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("requesterEmpId = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                EmployeeSearchModel[] models = gson.fromJson(Value, EmployeeSearchModel[].class);

                for (EmployeeSearchModel model : models) {

                    vm_fuel_requestDTO.requesterEmpId = model.employeeRecordId;
                    vm_fuel_requestDTO.requesterOfficeUnitId = model.officeUnitId;
                    vm_fuel_requestDTO.requesterOrgId = model.organogramId;

                    vm_fuel_requestDTO.requesterNameEn = model.employeeNameEn;
                    vm_fuel_requestDTO.requesterNameBn = model.employeeNameBn;
                    vm_fuel_requestDTO.requesterOfficeUnitNameEn = model.officeUnitNameEn;
                    vm_fuel_requestDTO.requesterOfficeUnitNameBn = model.officeUnitNameBn;
                    vm_fuel_requestDTO.requesterOfficeUnitOrgNameEn = model.organogramNameEn;
                    vm_fuel_requestDTO.requesterOfficeUnitOrgNameBn = model.organogramNameBn;

                    vm_fuel_requestDTO.requesterPhoneNum = model.phoneNumber;

                    OfficesDTO requesterOffice = OfficesRepository.getInstance().getOfficesDTOByID(vm_fuel_requestDTO.requesterOfficeId);

                    if (requesterOffice != null) {
                        vm_fuel_requestDTO.requesterOfficeId = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(model.officeUnitId).officeId;

                        vm_fuel_requestDTO.requesterOfficeNameEn = requesterOffice.officeNameEng;
                        vm_fuel_requestDTO.requesterOfficeNameBn = requesterOffice.officeNameBng;
                    }

                }
            } else {
                valid = false;
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            if (valid) {

                boolean driverFound = false;

                Vm_vehicle_driver_assignmentDTO vm_vehicle_driver_assignmentDTO = new Vm_vehicle_driver_assignmentDAO().getAllVm_vehicle_driver_assignmentByVehicleId(vm_fuel_requestDTO.vehicleId);

                if (vm_vehicle_driver_assignmentDTO != null) {
                    vm_fuel_requestDTO.vehicleDriverAssignmentId = vm_vehicle_driver_assignmentDTO.driverId;
                    driverFound = true;
                } else {
                    String filterApplicationTime = " start_date <= " + vm_fuel_requestDTO.applicationDate +
                            " AND end_date >= " + vm_fuel_requestDTO.applicationDate +
                            " AND given_vehicle_id = " + vm_fuel_requestDTO.vehicleId;

                    List<Vm_requisitionDTO> vm_requisitionDTOS = new Vm_requisitionDAO().getDTOs(null, 1, 0, true, userDTO, filterApplicationTime, false);

                    if (!vm_requisitionDTOS.isEmpty()) {
                        vm_fuel_requestDTO.vehicleDriverAssignmentId = vm_requisitionDTOS.get(0).driverEmpId;
                        driverFound = true;
                    }
                }

                if (driverFound) {

                    Utils.handleTransactionForNavigationService4(() -> {
                        System.out.println("Done adding  addVm_fuel_request dto = " + vm_fuel_requestDTO);
                        long returnedID = -1;

                        HashMap<Integer, Long> previousAmountMap = new HashMap<>();
                        HashMap<Integer, Long> previousDateMap = new HashMap<>();

                        if (isPermanentTable == false) //add new row for validation and make the old row outdated
                        {
                            vm_fuel_requestDAO.setIsDeleted(vm_fuel_requestDTO.iD, CommonDTO.OUTDATED);
                            returnedID = vm_fuel_requestDAO.add(vm_fuel_requestDTO);
                            vm_fuel_requestDAO.setIsDeleted(returnedID, CommonDTO.WAITING_FOR_APPROVAL);
                        } else if (addFlag == true) {
                            returnedID = vm_fuel_requestDAO.manageWriteOperations(vm_fuel_requestDTO, SessionConstants.INSERT, -1, userDTO);
                        } else {
                            returnedID = vm_fuel_requestDAO.manageWriteOperations(vm_fuel_requestDTO, SessionConstants.UPDATE, -1, userDTO);
                        }


                        List<VmFuelRequestItemDTO> vmFuelRequestItemDTOList = createVmFuelRequestItemDTOListByRequest(request);
                        if (addFlag == true || isPermanentTable == false) //add or validate
                        {
                            if (vmFuelRequestItemDTOList != null) {
                                for (VmFuelRequestItemDTO vmFuelRequestItemDTO : vmFuelRequestItemDTOList) {
                                    vmFuelRequestItemDTO.vmFuelRequestId = vm_fuel_requestDTO.iD;
                                    vmFuelRequestItemDTO.lastFuelAmount = previousAmountMap.getOrDefault(vmFuelRequestItemDTO.vehicleFuelCat, (long) 0);
                                    vmFuelRequestItemDTO.lastFuelDate = previousDateMap.getOrDefault(vmFuelRequestItemDTO.vehicleFuelCat, (long) 0);
                                    vmFuelRequestItemDAO.add(vmFuelRequestItemDTO);
                                }
                            }

                        } else {
                            List<Long> childIdsFromRequest = vmFuelRequestItemDAO.getChildIdsFromRequest(request, "vmFuelRequestItem");
                            //delete the removed children
                            vmFuelRequestItemDAO.deleteChildrenNotInList("vm_fuel_request", "vm_fuel_request_item", vm_fuel_requestDTO.iD, childIdsFromRequest);
                            List<Long> childIDsInDatabase = vmFuelRequestItemDAO.getChilIds("vm_fuel_request", "vm_fuel_request_item", vm_fuel_requestDTO.iD);


                            if (childIdsFromRequest != null) {
                                for (int i = 0; i < childIdsFromRequest.size(); i++) {
                                    Long childIDFromRequest = childIdsFromRequest.get(i);
                                    if (childIDsInDatabase.contains(childIDFromRequest)) {
                                        VmFuelRequestItemDTO vmFuelRequestItemDTO = createVmFuelRequestItemDTOByRequestAndIndex(request, false, i);
                                        vmFuelRequestItemDTO.vmFuelRequestId = vm_fuel_requestDTO.iD;
                                        vmFuelRequestItemDAO.update(vmFuelRequestItemDTO);
                                    } else {
                                        VmFuelRequestItemDTO vmFuelRequestItemDTO = createVmFuelRequestItemDTOByRequestAndIndex(request, true, i);
                                        vmFuelRequestItemDTO.vmFuelRequestId = vm_fuel_requestDTO.iD;
                                        vmFuelRequestItemDAO.add(vmFuelRequestItemDTO);
                                    }
                                }
                            } else {
                                vmFuelRequestItemDAO.deleteVmFuelRequestItemByVmFuelRequestID(vm_fuel_requestDTO.iD);
                            }

                        }


                        if (addFlag == true) {
                            TaskTypeEnum taskTypeEnum = TaskTypeEnum.VM_FUEL_REQUEST;

                            CreateVm_fuel_requestApprovalModel model = new CreateVm_fuel_requestApprovalModel.CreateVm_fuel_requestApprovalModelBuilder()
                                    .setTaskTypeId(taskTypeEnum.getValue())
                                    .setVm_fuel_requestId(vm_fuel_requestDTO.iD)
                                    .setRequesterEmployeeRecordId(vm_fuel_requestDTO.requesterEmpId)
                                    .build();
                            new Vm_fuel_request_approval_mappingDAO().createVm_fuel_requestApproval(model);
                        }


                        PrintWriter out = response.getWriter();
                        out.println(new Gson().toJson("Success"));
                        out.close();
                    });

                } else {
                    PrintWriter out = response.getWriter();
                    out.println(new Gson().toJson("No driver"));
                    out.close();
                }

            } else {
                PrintWriter out = response.getWriter();
                out.println(new Gson().toJson("Invalid Input"));
                out.close();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private List<VmFuelRequestItemDTO> createVmFuelRequestItemDTOListByRequest(HttpServletRequest request) throws Exception {
        List<VmFuelRequestItemDTO> vmFuelRequestItemDTOList = new ArrayList<VmFuelRequestItemDTO>();
        if (request.getParameterValues("vmFuelRequestItem.iD") != null) {
            int vmFuelRequestItemItemNo = request.getParameterValues("vmFuelRequestItem.iD").length;


            for (int index = 0; index < vmFuelRequestItemItemNo; index++) {
                VmFuelRequestItemDTO vmFuelRequestItemDTO = createVmFuelRequestItemDTOByRequestAndIndex(request, true, index);
                vmFuelRequestItemDTOList.add(vmFuelRequestItemDTO);
            }

            return vmFuelRequestItemDTOList;
        }
        return null;
    }


    private VmFuelRequestItemDTO createVmFuelRequestItemDTOByRequestAndIndex(HttpServletRequest request, boolean addFlag, int index) throws Exception {

        VmFuelRequestItemDTO vmFuelRequestItemDTO;
        if (addFlag == true) {
            vmFuelRequestItemDTO = new VmFuelRequestItemDTO();
        } else {
            vmFuelRequestItemDTO = VmFuelRequestItemRepository.getInstance().getVmFuelRequestItemDTOByID(Long.parseLong(request.getParameterValues("vmFuelRequestItem.iD")[index]));
        }
        String path = getServletContext().getRealPath("/img2/");
        SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");


        String Value = "";
        Value = request.getParameterValues("vmFuelRequestItem.insertedByUserId")[index];

        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }

        vmFuelRequestItemDTO.insertedByUserId = Long.parseLong(Value);
        Value = request.getParameterValues("vmFuelRequestItem.insertedByOrganogramId")[index];

        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }

        vmFuelRequestItemDTO.insertedByOrganogramId = Long.parseLong(Value);
        Value = request.getParameterValues("vmFuelRequestItem.insertionDate")[index];

        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }

        vmFuelRequestItemDTO.insertionDate = Long.parseLong(Value);
        Value = request.getParameterValues("vmFuelRequestItem.searchColumn")[index];

        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }

        vmFuelRequestItemDTO.searchColumn = (Value);
        Value = request.getParameterValues("vmFuelRequestItem.vmFuelRequestId")[index];

        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }

        vmFuelRequestItemDTO.vmFuelRequestId = Long.parseLong(Value);
        Value = request.getParameterValues("vmFuelRequestItem.vehicleFuelCat")[index];

        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }

        vmFuelRequestItemDTO.vehicleFuelCat = Integer.parseInt(Value);
        Value = request.getParameterValues("vmFuelRequestItem.requestedAmount")[index];

        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }

        vmFuelRequestItemDTO.requestedAmount = Long.parseLong(Value);
        Value = request.getParameterValues("vmFuelRequestItem.lastFuelAmount")[index];

        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }

        vmFuelRequestItemDTO.lastFuelAmount = Long.parseLong(Value);
        Value = request.getParameterValues("vmFuelRequestItem.lastFuelDate")[index];

        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }

        try {
            Date d = f.parse(Value);
            vmFuelRequestItemDTO.lastFuelDate = d.getTime();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return vmFuelRequestItemDTO;

    }


    private void getVm_fuel_request(HttpServletRequest request, HttpServletResponse response, long id) throws ServletException, IOException {
        System.out.println("in getVm_fuel_request");
        Vm_fuel_requestDTO vm_fuel_requestDTO = null;
        try {
            vm_fuel_requestDTO = Vm_fuel_requestRepository.getInstance().getVm_fuel_requestDTOByID(id);
            request.setAttribute("ID", vm_fuel_requestDTO.iD);
            request.setAttribute("vm_fuel_requestDTO", vm_fuel_requestDTO);
            request.setAttribute("vm_fuel_requestDAO", vm_fuel_requestDAO);

            String URL = "";

            String inPlaceEdit = request.getParameter("inplaceedit");
            String inPlaceSubmit = request.getParameter("inplacesubmit");
            String getBodyOnly = request.getParameter("getBodyOnly");

            if (inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase("")) {
                URL = "vm_fuel_request/vm_fuel_requestInPlaceEdit.jsp";
                request.setAttribute("inplaceedit", "");
            } else if (inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase("")) {
                URL = "vm_fuel_request/vm_fuel_requestSearchRow.jsp";
                request.setAttribute("inplacesubmit", "");
            } else {
                if (getBodyOnly != null && !getBodyOnly.equalsIgnoreCase("")) {
                    URL = "vm_fuel_request/vm_fuel_requestEditBody.jsp?actionType=edit";
                } else {
                    URL = "vm_fuel_request/vm_fuel_requestEdit.jsp?actionType=edit";
                }
            }

            RequestDispatcher rd = request.getRequestDispatcher(URL);
            rd.forward(request, response);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void getVm_fuel_request(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        getVm_fuel_request(request, response, Long.parseLong(request.getParameter("ID")));
    }

    private void searchVm_fuel_request(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException {
        System.out.println("in  searchVm_fuel_request 1");
        LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        String ajax = request.getParameter("ajax");
        boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);

        RoleDTO role = PermissionRepository.getRoleDTOByRoleID(userDTO.roleID);
        boolean isAdmin = role.ID == SessionConstants.VEHICLE_ADMIN_ROLE || role.ID == SessionConstants.ADMIN_ROLE;

        String filterOwn = " (requester_org_id = " + userDTO.organogramID + " OR inserted_by_organogram_id = " + userDTO.organogramID;
        filterOwn += isAdmin ? " OR true) " : ") ";
        if (filter != null && !filter.isEmpty()) filter += " AND ";
        filter += filterOwn;

        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
                SessionConstants.NAV_VM_FUEL_REQUEST,
                request,
                vm_fuel_requestDAO,
                SessionConstants.VIEW_VM_FUEL_REQUEST,
                SessionConstants.SEARCH_VM_FUEL_REQUEST,
                tableName,
                isPermanent,
                userDTO,
                filter,
                true);
        try {
            System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        } catch (Exception e) {
            System.out.println("failed to dojob" + e);
        }

        request.setAttribute("vm_fuel_requestDAO", vm_fuel_requestDAO);
        RequestDispatcher rd;
        if (!isPermanent) {
            if (hasAjax == false) {
                System.out.println("Going to vm_fuel_request/vm_fuel_requestApproval.jsp");
                rd = request.getRequestDispatcher("vm_fuel_request/vm_fuel_requestApproval.jsp");
            } else {
                System.out.println("Going to vm_fuel_request/vm_fuel_requestApprovalForm.jsp");
                rd = request.getRequestDispatcher("vm_fuel_request/vm_fuel_requestApprovalForm.jsp");
            }
        } else {
            if (hasAjax == false) {
                System.out.println("Going to vm_fuel_request/vm_fuel_requestSearch.jsp");
                rd = request.getRequestDispatcher("vm_fuel_request/vm_fuel_requestSearch.jsp");
            } else {
                System.out.println("Going to vm_fuel_request/vm_fuel_requestSearchForm.jsp");
                rd = request.getRequestDispatcher("vm_fuel_request/vm_fuel_requestSearchForm.jsp");
            }
        }
        rd.forward(request, response);
    }

    private void billRegister(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException {
        System.out.println("in  searchVm_fuel_request 1");
        LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        String ajax = request.getParameter("ajax");
        boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);

        String officeUnitId = request.getParameter("officeUnitId");

        StringBuilder filterBuilder = new StringBuilder();

        filterBuilder.append("," + officeUnitId);

        HashMap<String, String> coupleOfficeMap = new HashMap<>();
        coupleOfficeMap.put("1", ",2");
        coupleOfficeMap.put("4", ",6");
        coupleOfficeMap.put("5", ",7");

        filterBuilder.append(coupleOfficeMap.getOrDefault(officeUnitId, ""));

        filterBuilder.replace(0, 1, "(");
        filterBuilder.append(")");

        String officeFilter = " office_id in " + filterBuilder;
        List<Vm_vehicle_office_assignmentDTO> vm_vehicle_office_assignmentDTOS = (List<Vm_vehicle_office_assignmentDTO>)
                new Vm_vehicle_office_assignmentDAO().getDTOs(null, 10000, 0, true, userDTO, officeFilter, false);

        filterBuilder.delete(0, filterBuilder.length());

        vm_vehicle_office_assignmentDTOS
                .forEach(dto -> {
                    filterBuilder.append("," + dto.vehicleId);
                });

        filterBuilder.replace(0, 1, "(");
        filterBuilder.append(")");

        if (!filter.isEmpty()) filter = " AND " + filter;
        filter = " status = 12" + filter;
        if (!vm_vehicle_office_assignmentDTOS.isEmpty())
            filter = " vehicle_id in " + filterBuilder + " AND " + filter;
        else filter = " false AND " + filter;

        String Value = request.getParameter("fiscalYearId");
        if (Value != null && !Value.isEmpty()) {
            filter = " AND " + filter;
            filter = " fiscal_year_id = " + Value + filter;
        }

        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
                SessionConstants.NAV_VM_FUEL_REQUEST,
                request,
                vm_fuel_requestDAO,
                SessionConstants.VIEW_VM_FUEL_REQUEST,
                SessionConstants.SEARCH_VM_FUEL_REQUEST,
                tableName,
                isPermanent,
                userDTO,
                filter,
                true);
        try {
            System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        } catch (Exception e) {
            System.out.println("failed to dojob" + e);
        }

        request.setAttribute("vm_fuel_requestDAO", vm_fuel_requestDAO);
        RequestDispatcher rd;
        if (!isPermanent) {
            if (hasAjax == false) {
                System.out.println("Going to vm_fuel_request/vm_fuel_requestApproval.jsp");
                rd = request.getRequestDispatcher("vm_fuel_request/vm_fuel_requestApproval.jsp");
            } else {
                System.out.println("Going to vm_fuel_request/vm_fuel_requestApprovalForm.jsp");
                rd = request.getRequestDispatcher("vm_fuel_request/vm_fuel_requestApprovalForm.jsp");
            }
        } else {
            if (hasAjax == false) {
                System.out.println("Going to vm_fuel_request/vm_fuel_requestBillRegisterSearch.jsp");
                rd = request.getRequestDispatcher("vm_fuel_request/vm_fuel_requestBillRegisterSearch.jsp");
            } else {
                System.out.println("Going to vm_fuel_request/vm_fuel_requestBillRegisterSearchForm.jsp");
                rd = request.getRequestDispatcher("vm_fuel_request/vm_fuel_requestBillRegisterSearchForm.jsp");
            }
        }
        rd.forward(request, response);
    }


    private void getReportDataFuelRequest(HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        System.out.println("In getReportData");

        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);

        String Language = request.getParameter("language");
        long vehicleId = Long.parseLong(request.getParameter("vehicleId"));
        String fiscalYearIdsInString = request.getParameter("fiscalYearIds");
        List<String> fiscalYearIds = Arrays.asList(fiscalYearIdsInString.split(","));


        Boolean allSelected = false;
        if (fiscalYearIds.contains("-100")) {
            allSelected = true;
        }

        List<Long> fiscalYearIdsInLong = fiscalYearIds.stream()
                .map(s -> Long.parseLong(s))
                .collect(Collectors.toList());


        StringBuilder filterBuilder = new StringBuilder();
        fiscalYearIdsInLong
                .forEach(id -> {
                    filterBuilder.append("," + id);
                });

        filterBuilder.replace(0, 1, "(");
        filterBuilder.append(")");

//		System.out.println(fiscalYearIds);
//		System.out.println(allSelected);

        // vehicle Data
        Vm_vehicleReportDTO reportDTO = new Vm_vehicleReportDTO();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
        reportDTO.vm_vehicleDTO = Vm_vehicleRepository.getInstance().getVm_vehicleDTOByID(vehicleId);
//				(Vm_vehicleDTO) new Vm_vehicleDAO().
//				getDTOByID(vehicleId);

        // maintenance Data
        Vm_fuel_requestDAO vm_fuel_requestDAO = new Vm_fuel_requestDAO();
        VmFuelRequestItemDAO itemDAO = new VmFuelRequestItemDAO();

        String filter = " vehicle_id = " + vehicleId;

        List<Vm_fuel_requestDTO> maintenanceDTOS = new ArrayList<>();

        if (!allSelected) {
            filter += " AND fiscal_year_id in " + filterBuilder;
            maintenanceDTOS = vm_fuel_requestDAO.getDTOsSortedBy(null, -1, -1, true, userDTO, filter, false, "application_date");
        } else maintenanceDTOS = Vm_fuel_requestRepository.getInstance().getVm_fuel_requestDTOByvehicle_id(vehicleId);


        if (!maintenanceDTOS.isEmpty()) {

            HashMap<Long, List<VmFuelVendorItemDTO>> vendorIdToItems = new HashMap<>();
            HashMap<Long, Double> fuelRequestToPaidPrice = new HashMap<>();
            HashMap<Long, Double> fuelRequestToPaidPriceAccumulated = new HashMap<>();
            HashMap<Long, Double> vehicleToPaidPriceAccumulated = new HashMap<>();
            filterBuilder.delete(0, filterBuilder.length());

            maintenanceDTOS
                    .forEach(d -> {
                        filterBuilder.append("," + d.vendorId);
                    });

            filterBuilder.replace(0, 1, "(");
            filterBuilder.append(")");

            String filterFuelRequest = " vm_fuel_vendor_id in " + filterBuilder;
            List<VmFuelVendorItemDTO> vmFuelVendorItemDTOS = new VmFuelVendorItemDAO().getDTOs(null, -1, -1, true, userDTO, filterFuelRequest, false);

            vmFuelVendorItemDTOS
                    .forEach(vmFuelVendorItemDTO -> {
                        List<VmFuelVendorItemDTO> existingItems = vendorIdToItems.getOrDefault(vmFuelVendorItemDTO.vmFuelVendorId, new ArrayList<>());
                        existingItems.add(vmFuelVendorItemDTO);
                        vendorIdToItems.put(vmFuelVendorItemDTO.vmFuelVendorId, existingItems);
                    });

            maintenanceDTOS
                    .stream()
                    .forEach(d -> {
                        List<VmFuelVendorItemDTO> vendorItemDTOS = vendorIdToItems.getOrDefault(d.vendorId, new ArrayList<>());

                        HashMap<Integer, Double> fuelUnitPriceMap = new HashMap<>();
                        vendorItemDTOS
                                .forEach(vendorItemDTO -> {
                                    fuelUnitPriceMap.put(vendorItemDTO.vehicleFuelCat, vendorItemDTO.price);
                                });

                        List<VmFuelRequestItemDTO> vmFuelRequestItemDTOS = d.vmFuelRequestItemDTOList;

                        double totalPriceAtomic = 0.0;
                        for (VmFuelRequestItemDTO vmFuelRequestItemDTO : vmFuelRequestItemDTOS) {
                            double price = fuelUnitPriceMap.getOrDefault(vmFuelRequestItemDTO.vehicleFuelCat, 0.0) * vmFuelRequestItemDTO.requestedAmount;
                            totalPriceAtomic = (totalPriceAtomic + price);
                        }

                        fuelRequestToPaidPrice.put(d.iD, totalPriceAtomic);

                        double totalPrice = vehicleToPaidPriceAccumulated.getOrDefault(d.vehicleId, 0.0).doubleValue() + totalPriceAtomic;
                        fuelRequestToPaidPriceAccumulated.put(d.iD, totalPrice);
                        vehicleToPaidPriceAccumulated.put(d.vehicleId, totalPrice);
                    });


            reportDTO.maintenanceDTOListForMaintenance = new ArrayList<>();

            for (Vm_fuel_requestDTO maintenanceDTO : maintenanceDTOS) {

                String approvedDate = simpleDateFormat.format(new Date(Long.parseLong
                        (maintenanceDTO.applicationDate + "")));
                double maintenanceValue = 0.0;

                maintenanceValue += (fuelRequestToPaidPrice.get(maintenanceDTO.iD) * 1.075);

                if (maintenanceValue > 0.0) {
                    ReportItemDTO reportItemDTO = new ReportItemDTO();
                    reportItemDTO.amount = (int) (Math.round(maintenanceValue * 100)) / 100.0;
                    reportItemDTO.date = Utils.getDigits(approvedDate, Language);
                    reportDTO.maintenanceDTOListForMaintenance.add(reportItemDTO);

                    reportDTO.totalMaintenanceCost += maintenanceValue;
                }

            }

            reportDTO.totalMaintenanceCost = (int) (Math.round(reportDTO.totalMaintenanceCost * 100)) / 100.0;

        }


//		System.out.println(maintenanceDTOS);

        // maintenance end


        PrintWriter out = response.getWriter();
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");

        String encoded = this.gson.toJson(reportDTO);
        System.out.println("json encoded data = " + encoded);
        out.print(encoded);
        out.flush();

    }

}

