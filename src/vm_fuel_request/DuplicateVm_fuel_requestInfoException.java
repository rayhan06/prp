package vm_fuel_request;

/*
 * @author Md. Erfan Hossain
 * @created 03/05/2021 - 5:55 PM
 * @project parliament
 */

public class DuplicateVm_fuel_requestInfoException extends Exception{

    public DuplicateVm_fuel_requestInfoException(String message) {
        super(message);
    }
}
