package vm_fuel_request;
import java.util.*; 
import util.*; 


public class VmFuelRequestItemDTO extends CommonDTO
{

	public long insertedByUserId = -1;
	public long insertedByOrganogramId = -1;
	public long insertionDate = -1;
	public long vmFuelRequestId = -1;
	public int vehicleFuelCat = -1;
	public long requestedAmount = -1;
	public long lastFuelAmount = -1;
	public long lastFuelDate = System.currentTimeMillis();
	
	public List<VmFuelRequestItemDTO> vmFuelRequestItemDTOList = new ArrayList<>();
	
    @Override
	public String toString() {
            return "$VmFuelRequestItemDTO[" +
            " iD = " + iD +
            " insertedByUserId = " + insertedByUserId +
            " insertedByOrganogramId = " + insertedByOrganogramId +
            " insertionDate = " + insertionDate +
            " lastModificationTime = " + lastModificationTime +
            " searchColumn = " + searchColumn +
            " vmFuelRequestId = " + vmFuelRequestId +
            " vehicleFuelCat = " + vehicleFuelCat +
            " requestedAmount = " + requestedAmount +
            " lastFuelAmount = " + lastFuelAmount +
            " lastFuelDate = " + lastFuelDate +
            " isDeleted = " + isDeleted +
            "]";
    }

}