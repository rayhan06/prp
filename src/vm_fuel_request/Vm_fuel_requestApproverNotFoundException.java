package vm_fuel_request;

/*
 * @author Md. Erfan Hossain
 * @created 03/05/2021 - 4:46 PM
 * @project parliament
 */

public class Vm_fuel_requestApproverNotFoundException extends Exception{
    public Vm_fuel_requestApproverNotFoundException(String message) {
        super(message);
    }
}
