package vm_fuel_request;

/*
 * @author Md. Erfan Hossain
 * @created 01/05/2021 - 7:48 PM
 * @project parliament
 */

public enum ApprovalStatus {
    PENDING(1),
    SATISFIED(3),
    DISSATISFIED(4),
    PAID(12),
    ;
    private final int value;

    ApprovalStatus(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public static String getColor(int value){
        if (value == SATISFIED.getValue()) {
            return  "green";
        } else if (value == DISSATISFIED.getValue()) {
            return  "crimson";
        } else if (value == PAID.getValue()) {
            return  "#5867dd";
        }
        return "#22ccc1";
    }
}
