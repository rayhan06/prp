package vm_fuel_request;

import com.google.gson.Gson;
import org.apache.log4j.Logger;
import repository.Repository;
import repository.RepositoryManager;
import vm_route_travel.Vm_route_travelDAO;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;


public class VmFuelRequestItemRepository implements Repository {
	VmFuelRequestItemDAO Vm_fuel_request_itemDAO = null;
	Gson gson;

	public void setDAO(VmFuelRequestItemDAO Vm_fuel_request_itemDAO)
	{
		this.Vm_fuel_request_itemDAO = Vm_fuel_request_itemDAO;
		gson = new Gson();

	}
	
	
	static Logger logger = Logger.getLogger(VmFuelRequestItemRepository.class);
	Map<Long, VmFuelRequestItemDTO>mapOfVmFuelRequestItemDTOToiD;
//	Map<String, Set<VmFuelRequestItemDTO> >mapOfVmFuelRequestItemDTOTonameEn;
//	Map<String, Set<VmFuelRequestItemDTO> >mapOfVmFuelRequestItemDTOTonameBn;
	Map<Long, Set<VmFuelRequestItemDTO> >mapOfVmFuelRequestItemDTOTovmFuelRequestId;
//	Map<Double, Set<VmFuelRequestItemDTO> >mapOfVmFuelRequestItemDTOTounitPrice;
//	Map<Double, Set<VmFuelRequestItemDTO> >mapOfVmFuelRequestItemDTOToestCost;
//	Map<Integer, Set<VmFuelRequestItemDTO> >mapOfVmFuelRequestItemDTOTomethodAndTypeCat;
//	Map<String, Set<VmFuelRequestItemDTO> >mapOfVmFuelRequestItemDTOToapprovingAuthority;
//	Map<Integer, Set<VmFuelRequestItemDTO> >mapOfVmFuelRequestItemDTOTosourceOfFundCat;
//	Map<String, Set<VmFuelRequestItemDTO> >mapOfVmFuelRequestItemDTOTotimeCode;
//	Map<String, Set<VmFuelRequestItemDTO> >mapOfVmFuelRequestItemDTOTotender;
//	Map<String, Set<VmFuelRequestItemDTO> >mapOfVmFuelRequestItemDTOTotenderOpening;
//	Map<String, Set<VmFuelRequestItemDTO> >mapOfVmFuelRequestItemDTOTotenderEvaluation;
//	Map<String, Set<VmFuelRequestItemDTO> >mapOfVmFuelRequestItemDTOToapprovalToward;
//	Map<String, Set<VmFuelRequestItemDTO> >mapOfVmFuelRequestItemDTOToawardNotification;
//	Map<String, Set<VmFuelRequestItemDTO> >mapOfVmFuelRequestItemDTOTosiginingOfContract;
//	Map<Long, Set<VmFuelRequestItemDTO> >mapOfVmFuelRequestItemDTOTocontractSignatureTime;
//	Map<Long, Set<VmFuelRequestItemDTO> >mapOfVmFuelRequestItemDTOTocontractCompletionTime;
//	Map<Long, Set<VmFuelRequestItemDTO> >mapOfVmFuelRequestItemDTOToinsertedByUserId;
//	Map<Long, Set<VmFuelRequestItemDTO> >mapOfVmFuelRequestItemDTOToinsertedByOrganogramId;
//	Map<Long, Set<VmFuelRequestItemDTO> >mapOfVmFuelRequestItemDTOToinsertionDate;
//	Map<Long, Set<VmFuelRequestItemDTO> >mapOfVmFuelRequestItemDTOTolastModificationTime;
//	Map<String, Set<VmFuelRequestItemDTO> >mapOfVmFuelRequestItemDTOTosearchColumn;


	static VmFuelRequestItemRepository instance = null;  
	private VmFuelRequestItemRepository(){
		mapOfVmFuelRequestItemDTOToiD = new ConcurrentHashMap<>();
// 		mapOfVmFuelRequestItemDTOTonameEn = new ConcurrentHashMap<>();
//		mapOfVmFuelRequestItemDTOTonameBn = new ConcurrentHashMap<>();
		mapOfVmFuelRequestItemDTOTovmFuelRequestId = new ConcurrentHashMap<>();
//		mapOfVmFuelRequestItemDTOTounitPrice = new ConcurrentHashMap<>();
//		mapOfVmFuelRequestItemDTOToestCost = new ConcurrentHashMap<>();
//		mapOfVmFuelRequestItemDTOTomethodAndTypeCat = new ConcurrentHashMap<>();
//		mapOfVmFuelRequestItemDTOToapprovingAuthority = new ConcurrentHashMap<>();
//		mapOfVmFuelRequestItemDTOTosourceOfFundCat = new ConcurrentHashMap<>();
//		mapOfVmFuelRequestItemDTOTotimeCode = new ConcurrentHashMap<>();
//		mapOfVmFuelRequestItemDTOTotender = new ConcurrentHashMap<>();
//		mapOfVmFuelRequestItemDTOTotenderOpening = new ConcurrentHashMap<>();
//		mapOfVmFuelRequestItemDTOTotenderEvaluation = new ConcurrentHashMap<>();
//		mapOfVmFuelRequestItemDTOToapprovalToward = new ConcurrentHashMap<>();
//		mapOfVmFuelRequestItemDTOToawardNotification = new ConcurrentHashMap<>();
//		mapOfVmFuelRequestItemDTOTosiginingOfContract = new ConcurrentHashMap<>();
//		mapOfVmFuelRequestItemDTOTocontractSignatureTime = new ConcurrentHashMap<>();
//		mapOfVmFuelRequestItemDTOTocontractCompletionTime = new ConcurrentHashMap<>();
//		mapOfVmFuelRequestItemDTOToinsertedByUserId = new ConcurrentHashMap<>();
//		mapOfVmFuelRequestItemDTOToinsertedByOrganogramId = new ConcurrentHashMap<>();
//		mapOfVmFuelRequestItemDTOToinsertionDate = new ConcurrentHashMap<>();
//		mapOfVmFuelRequestItemDTOTolastModificationTime = new ConcurrentHashMap<>();
//		mapOfVmFuelRequestItemDTOTosearchColumn = new ConcurrentHashMap<>();

		setDAO(new VmFuelRequestItemDAO());

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static VmFuelRequestItemRepository getInstance(){
		if (instance == null){
			instance = new VmFuelRequestItemRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(Vm_fuel_request_itemDAO == null)
		{
			return;
		}
		try {
			List<VmFuelRequestItemDTO> Vm_fuel_request_itemDTOs = Vm_fuel_request_itemDAO.getAllVmFuelRequestItem(reloadAll);
			for(VmFuelRequestItemDTO Vm_fuel_request_itemDTO : Vm_fuel_request_itemDTOs) {
				VmFuelRequestItemDTO oldVmFuelRequestItemDTO = getVmFuelRequestItemDTOByIDWithoutClone(Vm_fuel_request_itemDTO.iD);
				if( oldVmFuelRequestItemDTO != null ) {
					mapOfVmFuelRequestItemDTOToiD.remove(oldVmFuelRequestItemDTO.iD);
					
// 					if(mapOfVmFuelRequestItemDTOTonameEn.containsKey(oldVmFuelRequestItemDTO.nameEn)) {
//						mapOfVmFuelRequestItemDTOTonameEn.get(oldVmFuelRequestItemDTO.nameEn).remove(oldVmFuelRequestItemDTO);
//					}
//					if(mapOfVmFuelRequestItemDTOTonameEn.get(oldVmFuelRequestItemDTO.nameEn).isEmpty()) {
//						mapOfVmFuelRequestItemDTOTonameEn.remove(oldVmFuelRequestItemDTO.nameEn);
//					}
//
//					if(mapOfVmFuelRequestItemDTOTonameBn.containsKey(oldVmFuelRequestItemDTO.nameBn)) {
//						mapOfVmFuelRequestItemDTOTonameBn.get(oldVmFuelRequestItemDTO.nameBn).remove(oldVmFuelRequestItemDTO);
//					}
//					if(mapOfVmFuelRequestItemDTOTonameBn.get(oldVmFuelRequestItemDTO.nameBn).isEmpty()) {
//						mapOfVmFuelRequestItemDTOTonameBn.remove(oldVmFuelRequestItemDTO.nameBn);
//					}
//
					if(mapOfVmFuelRequestItemDTOTovmFuelRequestId.containsKey(oldVmFuelRequestItemDTO.vmFuelRequestId)) {
						mapOfVmFuelRequestItemDTOTovmFuelRequestId.get(oldVmFuelRequestItemDTO.vmFuelRequestId).remove(oldVmFuelRequestItemDTO);
					}
					if(mapOfVmFuelRequestItemDTOTovmFuelRequestId.get(oldVmFuelRequestItemDTO.vmFuelRequestId).isEmpty()) {
						mapOfVmFuelRequestItemDTOTovmFuelRequestId.remove(oldVmFuelRequestItemDTO.vmFuelRequestId);
					}
//
//					if(mapOfVmFuelRequestItemDTOTounitPrice.containsKey(oldVmFuelRequestItemDTO.unitPrice)) {
//						mapOfVmFuelRequestItemDTOTounitPrice.get(oldVmFuelRequestItemDTO.unitPrice).remove(oldVmFuelRequestItemDTO);
//					}
//					if(mapOfVmFuelRequestItemDTOTounitPrice.get(oldVmFuelRequestItemDTO.unitPrice).isEmpty()) {
//						mapOfVmFuelRequestItemDTOTounitPrice.remove(oldVmFuelRequestItemDTO.unitPrice);
//					}
//
//					if(mapOfVmFuelRequestItemDTOToestCost.containsKey(oldVmFuelRequestItemDTO.estCost)) {
//						mapOfVmFuelRequestItemDTOToestCost.get(oldVmFuelRequestItemDTO.estCost).remove(oldVmFuelRequestItemDTO);
//					}
//					if(mapOfVmFuelRequestItemDTOToestCost.get(oldVmFuelRequestItemDTO.estCost).isEmpty()) {
//						mapOfVmFuelRequestItemDTOToestCost.remove(oldVmFuelRequestItemDTO.estCost);
//					}
//
//					if(mapOfVmFuelRequestItemDTOTomethodAndTypeCat.containsKey(oldVmFuelRequestItemDTO.methodAndTypeCat)) {
//						mapOfVmFuelRequestItemDTOTomethodAndTypeCat.get(oldVmFuelRequestItemDTO.methodAndTypeCat).remove(oldVmFuelRequestItemDTO);
//					}
//					if(mapOfVmFuelRequestItemDTOTomethodAndTypeCat.get(oldVmFuelRequestItemDTO.methodAndTypeCat).isEmpty()) {
//						mapOfVmFuelRequestItemDTOTomethodAndTypeCat.remove(oldVmFuelRequestItemDTO.methodAndTypeCat);
//					}
//
//					if(mapOfVmFuelRequestItemDTOToapprovingAuthority.containsKey(oldVmFuelRequestItemDTO.approvingAuthority)) {
//						mapOfVmFuelRequestItemDTOToapprovingAuthority.get(oldVmFuelRequestItemDTO.approvingAuthority).remove(oldVmFuelRequestItemDTO);
//					}
//					if(mapOfVmFuelRequestItemDTOToapprovingAuthority.get(oldVmFuelRequestItemDTO.approvingAuthority).isEmpty()) {
//						mapOfVmFuelRequestItemDTOToapprovingAuthority.remove(oldVmFuelRequestItemDTO.approvingAuthority);
//					}
//
//					if(mapOfVmFuelRequestItemDTOTosourceOfFundCat.containsKey(oldVmFuelRequestItemDTO.sourceOfFundCat)) {
//						mapOfVmFuelRequestItemDTOTosourceOfFundCat.get(oldVmFuelRequestItemDTO.sourceOfFundCat).remove(oldVmFuelRequestItemDTO);
//					}
//					if(mapOfVmFuelRequestItemDTOTosourceOfFundCat.get(oldVmFuelRequestItemDTO.sourceOfFundCat).isEmpty()) {
//						mapOfVmFuelRequestItemDTOTosourceOfFundCat.remove(oldVmFuelRequestItemDTO.sourceOfFundCat);
//					}
//
//					if(mapOfVmFuelRequestItemDTOTotimeCode.containsKey(oldVmFuelRequestItemDTO.timeCode)) {
//						mapOfVmFuelRequestItemDTOTotimeCode.get(oldVmFuelRequestItemDTO.timeCode).remove(oldVmFuelRequestItemDTO);
//					}
//					if(mapOfVmFuelRequestItemDTOTotimeCode.get(oldVmFuelRequestItemDTO.timeCode).isEmpty()) {
//						mapOfVmFuelRequestItemDTOTotimeCode.remove(oldVmFuelRequestItemDTO.timeCode);
//					}
//
//					if(mapOfVmFuelRequestItemDTOTotender.containsKey(oldVmFuelRequestItemDTO.tender)) {
//						mapOfVmFuelRequestItemDTOTotender.get(oldVmFuelRequestItemDTO.tender).remove(oldVmFuelRequestItemDTO);
//					}
//					if(mapOfVmFuelRequestItemDTOTotender.get(oldVmFuelRequestItemDTO.tender).isEmpty()) {
//						mapOfVmFuelRequestItemDTOTotender.remove(oldVmFuelRequestItemDTO.tender);
//					}
//
//					if(mapOfVmFuelRequestItemDTOTotenderOpening.containsKey(oldVmFuelRequestItemDTO.tenderOpening)) {
//						mapOfVmFuelRequestItemDTOTotenderOpening.get(oldVmFuelRequestItemDTO.tenderOpening).remove(oldVmFuelRequestItemDTO);
//					}
//					if(mapOfVmFuelRequestItemDTOTotenderOpening.get(oldVmFuelRequestItemDTO.tenderOpening).isEmpty()) {
//						mapOfVmFuelRequestItemDTOTotenderOpening.remove(oldVmFuelRequestItemDTO.tenderOpening);
//					}
//
//					if(mapOfVmFuelRequestItemDTOTotenderEvaluation.containsKey(oldVmFuelRequestItemDTO.tenderEvaluation)) {
//						mapOfVmFuelRequestItemDTOTotenderEvaluation.get(oldVmFuelRequestItemDTO.tenderEvaluation).remove(oldVmFuelRequestItemDTO);
//					}
//					if(mapOfVmFuelRequestItemDTOTotenderEvaluation.get(oldVmFuelRequestItemDTO.tenderEvaluation).isEmpty()) {
//						mapOfVmFuelRequestItemDTOTotenderEvaluation.remove(oldVmFuelRequestItemDTO.tenderEvaluation);
//					}
//
//					if(mapOfVmFuelRequestItemDTOToapprovalToward.containsKey(oldVmFuelRequestItemDTO.approvalToward)) {
//						mapOfVmFuelRequestItemDTOToapprovalToward.get(oldVmFuelRequestItemDTO.approvalToward).remove(oldVmFuelRequestItemDTO);
//					}
//					if(mapOfVmFuelRequestItemDTOToapprovalToward.get(oldVmFuelRequestItemDTO.approvalToward).isEmpty()) {
//						mapOfVmFuelRequestItemDTOToapprovalToward.remove(oldVmFuelRequestItemDTO.approvalToward);
//					}
//
//					if(mapOfVmFuelRequestItemDTOToawardNotification.containsKey(oldVmFuelRequestItemDTO.awardNotification)) {
//						mapOfVmFuelRequestItemDTOToawardNotification.get(oldVmFuelRequestItemDTO.awardNotification).remove(oldVmFuelRequestItemDTO);
//					}
//					if(mapOfVmFuelRequestItemDTOToawardNotification.get(oldVmFuelRequestItemDTO.awardNotification).isEmpty()) {
//						mapOfVmFuelRequestItemDTOToawardNotification.remove(oldVmFuelRequestItemDTO.awardNotification);
//					}
//
//					if(mapOfVmFuelRequestItemDTOTosiginingOfContract.containsKey(oldVmFuelRequestItemDTO.siginingOfContract)) {
//						mapOfVmFuelRequestItemDTOTosiginingOfContract.get(oldVmFuelRequestItemDTO.siginingOfContract).remove(oldVmFuelRequestItemDTO);
//					}
//					if(mapOfVmFuelRequestItemDTOTosiginingOfContract.get(oldVmFuelRequestItemDTO.siginingOfContract).isEmpty()) {
//						mapOfVmFuelRequestItemDTOTosiginingOfContract.remove(oldVmFuelRequestItemDTO.siginingOfContract);
//					}
//
//					if(mapOfVmFuelRequestItemDTOTocontractSignatureTime.containsKey(oldVmFuelRequestItemDTO.contractSignatureTime)) {
//						mapOfVmFuelRequestItemDTOTocontractSignatureTime.get(oldVmFuelRequestItemDTO.contractSignatureTime).remove(oldVmFuelRequestItemDTO);
//					}
//					if(mapOfVmFuelRequestItemDTOTocontractSignatureTime.get(oldVmFuelRequestItemDTO.contractSignatureTime).isEmpty()) {
//						mapOfVmFuelRequestItemDTOTocontractSignatureTime.remove(oldVmFuelRequestItemDTO.contractSignatureTime);
//					}
//
//					if(mapOfVmFuelRequestItemDTOTocontractCompletionTime.containsKey(oldVmFuelRequestItemDTO.contractCompletionTime)) {
//						mapOfVmFuelRequestItemDTOTocontractCompletionTime.get(oldVmFuelRequestItemDTO.contractCompletionTime).remove(oldVmFuelRequestItemDTO);
//					}
//					if(mapOfVmFuelRequestItemDTOTocontractCompletionTime.get(oldVmFuelRequestItemDTO.contractCompletionTime).isEmpty()) {
//						mapOfVmFuelRequestItemDTOTocontractCompletionTime.remove(oldVmFuelRequestItemDTO.contractCompletionTime);
//					}
//
//					if(mapOfVmFuelRequestItemDTOToinsertedByUserId.containsKey(oldVmFuelRequestItemDTO.insertedByUserId)) {
//						mapOfVmFuelRequestItemDTOToinsertedByUserId.get(oldVmFuelRequestItemDTO.insertedByUserId).remove(oldVmFuelRequestItemDTO);
//					}
//					if(mapOfVmFuelRequestItemDTOToinsertedByUserId.get(oldVmFuelRequestItemDTO.insertedByUserId).isEmpty()) {
//						mapOfVmFuelRequestItemDTOToinsertedByUserId.remove(oldVmFuelRequestItemDTO.insertedByUserId);
//					}
//
//					if(mapOfVmFuelRequestItemDTOToinsertedByOrganogramId.containsKey(oldVmFuelRequestItemDTO.insertedByOrganogramId)) {
//						mapOfVmFuelRequestItemDTOToinsertedByOrganogramId.get(oldVmFuelRequestItemDTO.insertedByOrganogramId).remove(oldVmFuelRequestItemDTO);
//					}
//					if(mapOfVmFuelRequestItemDTOToinsertedByOrganogramId.get(oldVmFuelRequestItemDTO.insertedByOrganogramId).isEmpty()) {
//						mapOfVmFuelRequestItemDTOToinsertedByOrganogramId.remove(oldVmFuelRequestItemDTO.insertedByOrganogramId);
//					}
//
//					if(mapOfVmFuelRequestItemDTOToinsertionDate.containsKey(oldVmFuelRequestItemDTO.insertionDate)) {
//						mapOfVmFuelRequestItemDTOToinsertionDate.get(oldVmFuelRequestItemDTO.insertionDate).remove(oldVmFuelRequestItemDTO);
//					}
//					if(mapOfVmFuelRequestItemDTOToinsertionDate.get(oldVmFuelRequestItemDTO.insertionDate).isEmpty()) {
//						mapOfVmFuelRequestItemDTOToinsertionDate.remove(oldVmFuelRequestItemDTO.insertionDate);
//					}
//
//					if(mapOfVmFuelRequestItemDTOTolastModificationTime.containsKey(oldVmFuelRequestItemDTO.lastModificationTime)) {
//						mapOfVmFuelRequestItemDTOTolastModificationTime.get(oldVmFuelRequestItemDTO.lastModificationTime).remove(oldVmFuelRequestItemDTO);
//					}
//					if(mapOfVmFuelRequestItemDTOTolastModificationTime.get(oldVmFuelRequestItemDTO.lastModificationTime).isEmpty()) {
//						mapOfVmFuelRequestItemDTOTolastModificationTime.remove(oldVmFuelRequestItemDTO.lastModificationTime);
//					}
//
//					if(mapOfVmFuelRequestItemDTOTosearchColumn.containsKey(oldVmFuelRequestItemDTO.searchColumn)) {
//						mapOfVmFuelRequestItemDTOTosearchColumn.get(oldVmFuelRequestItemDTO.searchColumn).remove(oldVmFuelRequestItemDTO);
//					}
//					if(mapOfVmFuelRequestItemDTOTosearchColumn.get(oldVmFuelRequestItemDTO.searchColumn).isEmpty()) {
//						mapOfVmFuelRequestItemDTOTosearchColumn.remove(oldVmFuelRequestItemDTO.searchColumn);
//					}
					
					
				}
				if(Vm_fuel_request_itemDTO.isDeleted == 0) 
				{
					
					mapOfVmFuelRequestItemDTOToiD.put(Vm_fuel_request_itemDTO.iD, Vm_fuel_request_itemDTO);

					// 					if( ! mapOfVmFuelRequestItemDTOTonameEn.containsKey(Vm_fuel_request_itemDTO.nameEn)) {
//						mapOfVmFuelRequestItemDTOTonameEn.put(Vm_fuel_request_itemDTO.nameEn, new HashSet<>());
//					}
//					mapOfVmFuelRequestItemDTOTonameEn.get(Vm_fuel_request_itemDTO.nameEn).add(Vm_fuel_request_itemDTO);
//
//					if( ! mapOfVmFuelRequestItemDTOTonameBn.containsKey(Vm_fuel_request_itemDTO.nameBn)) {
//						mapOfVmFuelRequestItemDTOTonameBn.put(Vm_fuel_request_itemDTO.nameBn, new HashSet<>());
//					}
//					mapOfVmFuelRequestItemDTOTonameBn.get(Vm_fuel_request_itemDTO.nameBn).add(Vm_fuel_request_itemDTO);
//
					if( ! mapOfVmFuelRequestItemDTOTovmFuelRequestId.containsKey(Vm_fuel_request_itemDTO.vmFuelRequestId)) {
						mapOfVmFuelRequestItemDTOTovmFuelRequestId.put(Vm_fuel_request_itemDTO.vmFuelRequestId, new HashSet<>());
					}
					mapOfVmFuelRequestItemDTOTovmFuelRequestId.get(Vm_fuel_request_itemDTO.vmFuelRequestId).add(Vm_fuel_request_itemDTO);
//
//					if( ! mapOfVmFuelRequestItemDTOTounitPrice.containsKey(Vm_fuel_request_itemDTO.unitPrice)) {
//						mapOfVmFuelRequestItemDTOTounitPrice.put(Vm_fuel_request_itemDTO.unitPrice, new HashSet<>());
//					}
//					mapOfVmFuelRequestItemDTOTounitPrice.get(Vm_fuel_request_itemDTO.unitPrice).add(Vm_fuel_request_itemDTO);
//
//					if( ! mapOfVmFuelRequestItemDTOToestCost.containsKey(Vm_fuel_request_itemDTO.estCost)) {
//						mapOfVmFuelRequestItemDTOToestCost.put(Vm_fuel_request_itemDTO.estCost, new HashSet<>());
//					}
//					mapOfVmFuelRequestItemDTOToestCost.get(Vm_fuel_request_itemDTO.estCost).add(Vm_fuel_request_itemDTO);
//
//					if( ! mapOfVmFuelRequestItemDTOTomethodAndTypeCat.containsKey(Vm_fuel_request_itemDTO.methodAndTypeCat)) {
//						mapOfVmFuelRequestItemDTOTomethodAndTypeCat.put(Vm_fuel_request_itemDTO.methodAndTypeCat, new HashSet<>());
//					}
//					mapOfVmFuelRequestItemDTOTomethodAndTypeCat.get(Vm_fuel_request_itemDTO.methodAndTypeCat).add(Vm_fuel_request_itemDTO);
//
//					if( ! mapOfVmFuelRequestItemDTOToapprovingAuthority.containsKey(Vm_fuel_request_itemDTO.approvingAuthority)) {
//						mapOfVmFuelRequestItemDTOToapprovingAuthority.put(Vm_fuel_request_itemDTO.approvingAuthority, new HashSet<>());
//					}
//					mapOfVmFuelRequestItemDTOToapprovingAuthority.get(Vm_fuel_request_itemDTO.approvingAuthority).add(Vm_fuel_request_itemDTO);
//
//					if( ! mapOfVmFuelRequestItemDTOTosourceOfFundCat.containsKey(Vm_fuel_request_itemDTO.sourceOfFundCat)) {
//						mapOfVmFuelRequestItemDTOTosourceOfFundCat.put(Vm_fuel_request_itemDTO.sourceOfFundCat, new HashSet<>());
//					}
//					mapOfVmFuelRequestItemDTOTosourceOfFundCat.get(Vm_fuel_request_itemDTO.sourceOfFundCat).add(Vm_fuel_request_itemDTO);
//
//					if( ! mapOfVmFuelRequestItemDTOTotimeCode.containsKey(Vm_fuel_request_itemDTO.timeCode)) {
//						mapOfVmFuelRequestItemDTOTotimeCode.put(Vm_fuel_request_itemDTO.timeCode, new HashSet<>());
//					}
//					mapOfVmFuelRequestItemDTOTotimeCode.get(Vm_fuel_request_itemDTO.timeCode).add(Vm_fuel_request_itemDTO);
//
//					if( ! mapOfVmFuelRequestItemDTOTotender.containsKey(Vm_fuel_request_itemDTO.tender)) {
//						mapOfVmFuelRequestItemDTOTotender.put(Vm_fuel_request_itemDTO.tender, new HashSet<>());
//					}
//					mapOfVmFuelRequestItemDTOTotender.get(Vm_fuel_request_itemDTO.tender).add(Vm_fuel_request_itemDTO);
//
//					if( ! mapOfVmFuelRequestItemDTOTotenderOpening.containsKey(Vm_fuel_request_itemDTO.tenderOpening)) {
//						mapOfVmFuelRequestItemDTOTotenderOpening.put(Vm_fuel_request_itemDTO.tenderOpening, new HashSet<>());
//					}
//					mapOfVmFuelRequestItemDTOTotenderOpening.get(Vm_fuel_request_itemDTO.tenderOpening).add(Vm_fuel_request_itemDTO);
//
//					if( ! mapOfVmFuelRequestItemDTOTotenderEvaluation.containsKey(Vm_fuel_request_itemDTO.tenderEvaluation)) {
//						mapOfVmFuelRequestItemDTOTotenderEvaluation.put(Vm_fuel_request_itemDTO.tenderEvaluation, new HashSet<>());
//					}
//					mapOfVmFuelRequestItemDTOTotenderEvaluation.get(Vm_fuel_request_itemDTO.tenderEvaluation).add(Vm_fuel_request_itemDTO);
//
//					if( ! mapOfVmFuelRequestItemDTOToapprovalToward.containsKey(Vm_fuel_request_itemDTO.approvalToward)) {
//						mapOfVmFuelRequestItemDTOToapprovalToward.put(Vm_fuel_request_itemDTO.approvalToward, new HashSet<>());
//					}
//					mapOfVmFuelRequestItemDTOToapprovalToward.get(Vm_fuel_request_itemDTO.approvalToward).add(Vm_fuel_request_itemDTO);
//
//					if( ! mapOfVmFuelRequestItemDTOToawardNotification.containsKey(Vm_fuel_request_itemDTO.awardNotification)) {
//						mapOfVmFuelRequestItemDTOToawardNotification.put(Vm_fuel_request_itemDTO.awardNotification, new HashSet<>());
//					}
//					mapOfVmFuelRequestItemDTOToawardNotification.get(Vm_fuel_request_itemDTO.awardNotification).add(Vm_fuel_request_itemDTO);
//
//					if( ! mapOfVmFuelRequestItemDTOTosiginingOfContract.containsKey(Vm_fuel_request_itemDTO.siginingOfContract)) {
//						mapOfVmFuelRequestItemDTOTosiginingOfContract.put(Vm_fuel_request_itemDTO.siginingOfContract, new HashSet<>());
//					}
//					mapOfVmFuelRequestItemDTOTosiginingOfContract.get(Vm_fuel_request_itemDTO.siginingOfContract).add(Vm_fuel_request_itemDTO);
//
//					if( ! mapOfVmFuelRequestItemDTOTocontractSignatureTime.containsKey(Vm_fuel_request_itemDTO.contractSignatureTime)) {
//						mapOfVmFuelRequestItemDTOTocontractSignatureTime.put(Vm_fuel_request_itemDTO.contractSignatureTime, new HashSet<>());
//					}
//					mapOfVmFuelRequestItemDTOTocontractSignatureTime.get(Vm_fuel_request_itemDTO.contractSignatureTime).add(Vm_fuel_request_itemDTO);
//
//					if( ! mapOfVmFuelRequestItemDTOTocontractCompletionTime.containsKey(Vm_fuel_request_itemDTO.contractCompletionTime)) {
//						mapOfVmFuelRequestItemDTOTocontractCompletionTime.put(Vm_fuel_request_itemDTO.contractCompletionTime, new HashSet<>());
//					}
//					mapOfVmFuelRequestItemDTOTocontractCompletionTime.get(Vm_fuel_request_itemDTO.contractCompletionTime).add(Vm_fuel_request_itemDTO);
//
//					if( ! mapOfVmFuelRequestItemDTOToinsertedByUserId.containsKey(Vm_fuel_request_itemDTO.insertedByUserId)) {
//						mapOfVmFuelRequestItemDTOToinsertedByUserId.put(Vm_fuel_request_itemDTO.insertedByUserId, new HashSet<>());
//					}
//					mapOfVmFuelRequestItemDTOToinsertedByUserId.get(Vm_fuel_request_itemDTO.insertedByUserId).add(Vm_fuel_request_itemDTO);
//
//					if( ! mapOfVmFuelRequestItemDTOToinsertedByOrganogramId.containsKey(Vm_fuel_request_itemDTO.insertedByOrganogramId)) {
//						mapOfVmFuelRequestItemDTOToinsertedByOrganogramId.put(Vm_fuel_request_itemDTO.insertedByOrganogramId, new HashSet<>());
//					}
//					mapOfVmFuelRequestItemDTOToinsertedByOrganogramId.get(Vm_fuel_request_itemDTO.insertedByOrganogramId).add(Vm_fuel_request_itemDTO);
//
//					if( ! mapOfVmFuelRequestItemDTOToinsertionDate.containsKey(Vm_fuel_request_itemDTO.insertionDate)) {
//						mapOfVmFuelRequestItemDTOToinsertionDate.put(Vm_fuel_request_itemDTO.insertionDate, new HashSet<>());
//					}
//					mapOfVmFuelRequestItemDTOToinsertionDate.get(Vm_fuel_request_itemDTO.insertionDate).add(Vm_fuel_request_itemDTO);
//
//					if( ! mapOfVmFuelRequestItemDTOTolastModificationTime.containsKey(Vm_fuel_request_itemDTO.lastModificationTime)) {
//						mapOfVmFuelRequestItemDTOTolastModificationTime.put(Vm_fuel_request_itemDTO.lastModificationTime, new HashSet<>());
//					}
//					mapOfVmFuelRequestItemDTOTolastModificationTime.get(Vm_fuel_request_itemDTO.lastModificationTime).add(Vm_fuel_request_itemDTO);
//
//					if( ! mapOfVmFuelRequestItemDTOTosearchColumn.containsKey(Vm_fuel_request_itemDTO.searchColumn)) {
//						mapOfVmFuelRequestItemDTOTosearchColumn.put(Vm_fuel_request_itemDTO.searchColumn, new HashSet<>());
//					}
//					mapOfVmFuelRequestItemDTOTosearchColumn.get(Vm_fuel_request_itemDTO.searchColumn).add(Vm_fuel_request_itemDTO);
//
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}

	public VmFuelRequestItemDTO clone(VmFuelRequestItemDTO dto) {
		String raw = gson.toJson(dto);
		return gson.fromJson(raw, VmFuelRequestItemDTO.class);
	}

	public List<VmFuelRequestItemDTO> clone(List<VmFuelRequestItemDTO> dtoList) {
		return dtoList
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}

	public VmFuelRequestItemDTO getVmFuelRequestItemDTOByIDWithoutClone( long ID){
		return mapOfVmFuelRequestItemDTOToiD.get(ID);
	}
	
	public List<VmFuelRequestItemDTO> getVmFuelRequestItemList() {
		List <VmFuelRequestItemDTO> Vm_fuel_request_items = new ArrayList<VmFuelRequestItemDTO>(this.mapOfVmFuelRequestItemDTOToiD.values());
		return Vm_fuel_request_items;
	}
	
	
	public VmFuelRequestItemDTO getVmFuelRequestItemDTOByID( long ID){
		return clone(mapOfVmFuelRequestItemDTOToiD.get(ID));
	}

	
// 	public List<VmFuelRequestItemDTO> getVmFuelRequestItemDTOByname_en(String name_en) {
//		return new ArrayList<>( mapOfVmFuelRequestItemDTOTonameEn.getOrDefault(name_en,new HashSet<>()));
//	}
//
//
//	public List<VmFuelRequestItemDTO> getVmFuelRequestItemDTOByname_bn(String name_bn) {
//		return new ArrayList<>( mapOfVmFuelRequestItemDTOTonameBn.getOrDefault(name_bn,new HashSet<>()));
//	}
//
//
	public List<VmFuelRequestItemDTO> getVmFuelRequestItemDTOByvmFuelRequestId(long vmFuelRequestId) {
		return new ArrayList<>( mapOfVmFuelRequestItemDTOTovmFuelRequestId.getOrDefault(vmFuelRequestId,new HashSet<>()))
				.stream()
				.map(vmFuelRequestItemDTO -> clone(vmFuelRequestItemDTO))
				.sorted(Comparator.comparingInt(dto -> dto.vehicleFuelCat))
				.collect(Collectors.toList());
	}


	public void deleteVmFuelRequestItemDTOByvmFuelRequestId(long vmFuelRequestId) {
        mapOfVmFuelRequestItemDTOTovmFuelRequestId.remove(vmFuelRequestId);
	}

//
//
//	public List<VmFuelRequestItemDTO> getVmFuelRequestItemDTOByunit_price(double unit_price) {
//		return new ArrayList<>( mapOfVmFuelRequestItemDTOTounitPrice.getOrDefault(unit_price,new HashSet<>()));
//	}
//
//
//	public List<VmFuelRequestItemDTO> getVmFuelRequestItemDTOByest_cost(double est_cost) {
//		return new ArrayList<>( mapOfVmFuelRequestItemDTOToestCost.getOrDefault(est_cost,new HashSet<>()));
//	}
//
//
//	public List<VmFuelRequestItemDTO> getVmFuelRequestItemDTOBymethod_and_type_cat(long method_and_type_cat) {
//		return new ArrayList<>( mapOfVmFuelRequestItemDTOTomethodAndTypeCat.getOrDefault(method_and_type_cat,new HashSet<>()));
//	}
//
//
//	public List<VmFuelRequestItemDTO> getVmFuelRequestItemDTOByapproving_authority(String approving_authority) {
//		return new ArrayList<>( mapOfVmFuelRequestItemDTOToapprovingAuthority.getOrDefault(approving_authority,new HashSet<>()));
//	}
//
//
//	public List<VmFuelRequestItemDTO> getVmFuelRequestItemDTOBysource_of_fund_cat(long source_of_fund_cat) {
//		return new ArrayList<>( mapOfVmFuelRequestItemDTOTosourceOfFundCat.getOrDefault(source_of_fund_cat,new HashSet<>()));
//	}
//
//
//	public List<VmFuelRequestItemDTO> getVmFuelRequestItemDTOBytime_code(String time_code) {
//		return new ArrayList<>( mapOfVmFuelRequestItemDTOTotimeCode.getOrDefault(time_code,new HashSet<>()));
//	}
//
//
//	public List<VmFuelRequestItemDTO> getVmFuelRequestItemDTOBytender(String tender) {
//		return new ArrayList<>( mapOfVmFuelRequestItemDTOTotender.getOrDefault(tender,new HashSet<>()));
//	}
//
//
//	public List<VmFuelRequestItemDTO> getVmFuelRequestItemDTOBytender_opening(String tender_opening) {
//		return new ArrayList<>( mapOfVmFuelRequestItemDTOTotenderOpening.getOrDefault(tender_opening,new HashSet<>()));
//	}
//
//
//	public List<VmFuelRequestItemDTO> getVmFuelRequestItemDTOBytender_evaluation(String tender_evaluation) {
//		return new ArrayList<>( mapOfVmFuelRequestItemDTOTotenderEvaluation.getOrDefault(tender_evaluation,new HashSet<>()));
//	}
//
//
//	public List<VmFuelRequestItemDTO> getVmFuelRequestItemDTOByapproval_toward(String approval_toward) {
//		return new ArrayList<>( mapOfVmFuelRequestItemDTOToapprovalToward.getOrDefault(approval_toward,new HashSet<>()));
//	}
//
//
//	public List<VmFuelRequestItemDTO> getVmFuelRequestItemDTOByaward_notification(String award_notification) {
//		return new ArrayList<>( mapOfVmFuelRequestItemDTOToawardNotification.getOrDefault(award_notification,new HashSet<>()));
//	}
//
//
//	public List<VmFuelRequestItemDTO> getVmFuelRequestItemDTOBysigining_of_contract(String sigining_of_contract) {
//		return new ArrayList<>( mapOfVmFuelRequestItemDTOTosiginingOfContract.getOrDefault(sigining_of_contract,new HashSet<>()));
//	}
//
//
//	public List<VmFuelRequestItemDTO> getVmFuelRequestItemDTOBycontract_signature_time(long contract_signature_time) {
//		return new ArrayList<>( mapOfVmFuelRequestItemDTOTocontractSignatureTime.getOrDefault(contract_signature_time,new HashSet<>()));
//	}
//
//
//	public List<VmFuelRequestItemDTO> getVmFuelRequestItemDTOBycontract_completion_time(long contract_completion_time) {
//		return new ArrayList<>( mapOfVmFuelRequestItemDTOTocontractCompletionTime.getOrDefault(contract_completion_time,new HashSet<>()));
//	}
//
//
//	public List<VmFuelRequestItemDTO> getVmFuelRequestItemDTOByinserted_by_user_id(long inserted_by_user_id) {
//		return new ArrayList<>( mapOfVmFuelRequestItemDTOToinsertedByUserId.getOrDefault(inserted_by_user_id,new HashSet<>()));
//	}
//
//
//	public List<VmFuelRequestItemDTO> getVmFuelRequestItemDTOByinserted_by_organogram_id(long inserted_by_organogram_id) {
//		return new ArrayList<>( mapOfVmFuelRequestItemDTOToinsertedByOrganogramId.getOrDefault(inserted_by_organogram_id,new HashSet<>()));
//	}
//
//
//	public List<VmFuelRequestItemDTO> getVmFuelRequestItemDTOByinsertion_date(long insertion_date) {
//		return new ArrayList<>( mapOfVmFuelRequestItemDTOToinsertionDate.getOrDefault(insertion_date,new HashSet<>()));
//	}
//
//
//	public List<VmFuelRequestItemDTO> getVmFuelRequestItemDTOBylastModificationTime(long lastModificationTime) {
//		return new ArrayList<>( mapOfVmFuelRequestItemDTOTolastModificationTime.getOrDefault(lastModificationTime,new HashSet<>()));
//	}
//
//
//	public List<VmFuelRequestItemDTO> getVmFuelRequestItemDTOBysearch_column(String search_column) {
//		return new ArrayList<>( mapOfVmFuelRequestItemDTOTosearchColumn.getOrDefault(search_column,new HashSet<>()));
//	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "vm_fuel_request_item";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


