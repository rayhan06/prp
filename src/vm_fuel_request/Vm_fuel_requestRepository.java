package vm_fuel_request;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import com.google.gson.Gson;
import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;
import vm_requisition.CommonApprovalStatus;


public class Vm_fuel_requestRepository implements Repository {
	Vm_fuel_requestDAO vm_fuel_requestDAO = null;
	Gson gson;

	public void setDAO(Vm_fuel_requestDAO vm_fuel_requestDAO)
	{
		this.vm_fuel_requestDAO = vm_fuel_requestDAO;
		gson = new Gson();

	}
	
	
	static Logger logger = Logger.getLogger(Vm_fuel_requestRepository.class);
	Map<Long, Vm_fuel_requestDTO>mapOfVm_fuel_requestDTOToiD;
//	Map<Long, Set<Vm_fuel_requestDTO> >mapOfVm_fuel_requestDTOToinsertedByUserId;
//	Map<Long, Set<Vm_fuel_requestDTO> >mapOfVm_fuel_requestDTOToinsertedByOrganogramId;
//	Map<Long, Set<Vm_fuel_requestDTO> >mapOfVm_fuel_requestDTOToinsertionDate;
//	Map<Long, Set<Vm_fuel_requestDTO> >mapOfVm_fuel_requestDTOTolastModificationTime;
//	Map<String, Set<Vm_fuel_requestDTO> >mapOfVm_fuel_requestDTOTosearchColumn;
//	Map<Integer, Set<Vm_fuel_requestDTO> >mapOfVm_fuel_requestDTOTovehicleType;
	Map<Long, Set<Vm_fuel_requestDTO> >mapOfVm_fuel_requestDTOTovehicleId;
//	Map<Long, Set<Vm_fuel_requestDTO> >mapOfVm_fuel_requestDTOToapplicationDate;
//	Map<Long, Set<Vm_fuel_requestDTO> >mapOfVm_fuel_requestDTOTovehicleDriverAssignmentId;
//	Map<Long, Set<Vm_fuel_requestDTO> >mapOfVm_fuel_requestDTOTolastFuelDate;
//	Map<Integer, Set<Vm_fuel_requestDTO> >mapOfVm_fuel_requestDTOTostatus;
//	Map<Long, Set<Vm_fuel_requestDTO> >mapOfVm_fuel_requestDTOTofiscalYearId;
//	Map<Long, Set<Vm_fuel_requestDTO> >mapOfVm_fuel_requestDTOToapprovedDate;
//	Map<Long, Set<Vm_fuel_requestDTO> >mapOfVm_fuel_requestDTOTovendorId;
//	Map<String, Set<Vm_fuel_requestDTO> >mapOfVm_fuel_requestDTOTosarokNumber;
//	Map<Long, Set<Vm_fuel_requestDTO> >mapOfVm_fuel_requestDTOTofilesDropzone;
//	Map<String, Set<Vm_fuel_requestDTO> >mapOfVm_fuel_requestDTOTopaymentRemarks;
//	Map<Long, Set<Vm_fuel_requestDTO> >mapOfVm_fuel_requestDTOTorequesterOrgId;
//	Map<Long, Set<Vm_fuel_requestDTO> >mapOfVm_fuel_requestDTOTorequesterOfficeId;
//	Map<Long, Set<Vm_fuel_requestDTO> >mapOfVm_fuel_requestDTOTorequesterOfficeUnitId;
//	Map<Long, Set<Vm_fuel_requestDTO> >mapOfVm_fuel_requestDTOTorequesterEmpId;
//	Map<String, Set<Vm_fuel_requestDTO> >mapOfVm_fuel_requestDTOTorequesterPhoneNum;
//	Map<String, Set<Vm_fuel_requestDTO> >mapOfVm_fuel_requestDTOTorequesterNameEn;
//	Map<String, Set<Vm_fuel_requestDTO> >mapOfVm_fuel_requestDTOTorequesterNameBn;
//	Map<String, Set<Vm_fuel_requestDTO> >mapOfVm_fuel_requestDTOTorequesterOfficeNameEn;
//	Map<String, Set<Vm_fuel_requestDTO> >mapOfVm_fuel_requestDTOTorequesterOfficeNameBn;
//	Map<String, Set<Vm_fuel_requestDTO> >mapOfVm_fuel_requestDTOTorequesterOfficeUnitNameEn;
//	Map<String, Set<Vm_fuel_requestDTO> >mapOfVm_fuel_requestDTOTorequesterOfficeUnitNameBn;
//	Map<String, Set<Vm_fuel_requestDTO> >mapOfVm_fuel_requestDTOTorequesterOfficeUnitOrgNameEn;
//	Map<String, Set<Vm_fuel_requestDTO> >mapOfVm_fuel_requestDTOTorequesterOfficeUnitOrgNameBn;
//	Map<Long, Set<Vm_fuel_requestDTO> >mapOfVm_fuel_requestDTOToapproverOrgId;
//	Map<Long, Set<Vm_fuel_requestDTO> >mapOfVm_fuel_requestDTOToapproverOfficeId;
//	Map<Long, Set<Vm_fuel_requestDTO> >mapOfVm_fuel_requestDTOToapproverOfficeUnitId;
//	Map<Long, Set<Vm_fuel_requestDTO> >mapOfVm_fuel_requestDTOToapproverEmpId;
//	Map<String, Set<Vm_fuel_requestDTO> >mapOfVm_fuel_requestDTOToapproverPhoneNum;
//	Map<String, Set<Vm_fuel_requestDTO> >mapOfVm_fuel_requestDTOToapproverNameEn;
//	Map<String, Set<Vm_fuel_requestDTO> >mapOfVm_fuel_requestDTOToapproverNameBn;
//	Map<String, Set<Vm_fuel_requestDTO> >mapOfVm_fuel_requestDTOToapproverOfficeNameEn;
//	Map<String, Set<Vm_fuel_requestDTO> >mapOfVm_fuel_requestDTOToapproverOfficeNameBn;
//	Map<String, Set<Vm_fuel_requestDTO> >mapOfVm_fuel_requestDTOToapproverOfficeUnitNameEn;
//	Map<String, Set<Vm_fuel_requestDTO> >mapOfVm_fuel_requestDTOToapproverOfficeUnitNameBn;
//	Map<String, Set<Vm_fuel_requestDTO> >mapOfVm_fuel_requestDTOToapproverOfficeUnitOrgNameEn;
//	Map<String, Set<Vm_fuel_requestDTO> >mapOfVm_fuel_requestDTOToapproverOfficeUnitOrgNameBn;
//	Map<Long, Set<Vm_fuel_requestDTO> >mapOfVm_fuel_requestDTOTopaymentReceiverOrgId;
//	Map<Long, Set<Vm_fuel_requestDTO> >mapOfVm_fuel_requestDTOTopaymentReceiverOfficeId;
//	Map<Long, Set<Vm_fuel_requestDTO> >mapOfVm_fuel_requestDTOTopaymentReceiverOfficeUnitId;
//	Map<Long, Set<Vm_fuel_requestDTO> >mapOfVm_fuel_requestDTOTopaymentReceiverEmpId;
//	Map<String, Set<Vm_fuel_requestDTO> >mapOfVm_fuel_requestDTOTopaymentReceiverPhoneNum;
//	Map<String, Set<Vm_fuel_requestDTO> >mapOfVm_fuel_requestDTOTopaymentReceiverNameEn;
//	Map<String, Set<Vm_fuel_requestDTO> >mapOfVm_fuel_requestDTOTopaymentReceiverNameBn;
//	Map<String, Set<Vm_fuel_requestDTO> >mapOfVm_fuel_requestDTOTopaymentReceiverOfficeNameEn;
//	Map<String, Set<Vm_fuel_requestDTO> >mapOfVm_fuel_requestDTOTopaymentReceiverOfficeNameBn;
//	Map<String, Set<Vm_fuel_requestDTO> >mapOfVm_fuel_requestDTOTopaymentReceiverOfficeUnitNameEn;
//	Map<String, Set<Vm_fuel_requestDTO> >mapOfVm_fuel_requestDTOTopaymentReceiverOfficeUnitNameBn;
//	Map<String, Set<Vm_fuel_requestDTO> >mapOfVm_fuel_requestDTOTopaymentReceiverOfficeUnitOrgNameEn;
//	Map<String, Set<Vm_fuel_requestDTO> >mapOfVm_fuel_requestDTOTopaymentReceiverOfficeUnitOrgNameBn;


	static Vm_fuel_requestRepository instance = null;  
	private Vm_fuel_requestRepository(){
		mapOfVm_fuel_requestDTOToiD = new ConcurrentHashMap<>();
//		mapOfVm_fuel_requestDTOToinsertedByUserId = new ConcurrentHashMap<>();
//		mapOfVm_fuel_requestDTOToinsertedByOrganogramId = new ConcurrentHashMap<>();
//		mapOfVm_fuel_requestDTOToinsertionDate = new ConcurrentHashMap<>();
//		mapOfVm_fuel_requestDTOTolastModificationTime = new ConcurrentHashMap<>();
//		mapOfVm_fuel_requestDTOTosearchColumn = new ConcurrentHashMap<>();
//		mapOfVm_fuel_requestDTOTovehicleType = new ConcurrentHashMap<>();
		mapOfVm_fuel_requestDTOTovehicleId = new ConcurrentHashMap<>();
//		mapOfVm_fuel_requestDTOToapplicationDate = new ConcurrentHashMap<>();
//		mapOfVm_fuel_requestDTOTovehicleDriverAssignmentId = new ConcurrentHashMap<>();
//		mapOfVm_fuel_requestDTOTolastFuelDate = new ConcurrentHashMap<>();
//		mapOfVm_fuel_requestDTOTostatus = new ConcurrentHashMap<>();
//		mapOfVm_fuel_requestDTOTofiscalYearId = new ConcurrentHashMap<>();
//		mapOfVm_fuel_requestDTOToapprovedDate = new ConcurrentHashMap<>();
//		mapOfVm_fuel_requestDTOTovendorId = new ConcurrentHashMap<>();
//		mapOfVm_fuel_requestDTOTosarokNumber = new ConcurrentHashMap<>();
//		mapOfVm_fuel_requestDTOTofilesDropzone = new ConcurrentHashMap<>();
//		mapOfVm_fuel_requestDTOTopaymentRemarks = new ConcurrentHashMap<>();
//		mapOfVm_fuel_requestDTOTorequesterOrgId = new ConcurrentHashMap<>();
//		mapOfVm_fuel_requestDTOTorequesterOfficeId = new ConcurrentHashMap<>();
//		mapOfVm_fuel_requestDTOTorequesterOfficeUnitId = new ConcurrentHashMap<>();
//		mapOfVm_fuel_requestDTOTorequesterEmpId = new ConcurrentHashMap<>();
//		mapOfVm_fuel_requestDTOTorequesterPhoneNum = new ConcurrentHashMap<>();
//		mapOfVm_fuel_requestDTOTorequesterNameEn = new ConcurrentHashMap<>();
//		mapOfVm_fuel_requestDTOTorequesterNameBn = new ConcurrentHashMap<>();
//		mapOfVm_fuel_requestDTOTorequesterOfficeNameEn = new ConcurrentHashMap<>();
//		mapOfVm_fuel_requestDTOTorequesterOfficeNameBn = new ConcurrentHashMap<>();
//		mapOfVm_fuel_requestDTOTorequesterOfficeUnitNameEn = new ConcurrentHashMap<>();
//		mapOfVm_fuel_requestDTOTorequesterOfficeUnitNameBn = new ConcurrentHashMap<>();
//		mapOfVm_fuel_requestDTOTorequesterOfficeUnitOrgNameEn = new ConcurrentHashMap<>();
//		mapOfVm_fuel_requestDTOTorequesterOfficeUnitOrgNameBn = new ConcurrentHashMap<>();
//		mapOfVm_fuel_requestDTOToapproverOrgId = new ConcurrentHashMap<>();
//		mapOfVm_fuel_requestDTOToapproverOfficeId = new ConcurrentHashMap<>();
//		mapOfVm_fuel_requestDTOToapproverOfficeUnitId = new ConcurrentHashMap<>();
//		mapOfVm_fuel_requestDTOToapproverEmpId = new ConcurrentHashMap<>();
//		mapOfVm_fuel_requestDTOToapproverPhoneNum = new ConcurrentHashMap<>();
//		mapOfVm_fuel_requestDTOToapproverNameEn = new ConcurrentHashMap<>();
//		mapOfVm_fuel_requestDTOToapproverNameBn = new ConcurrentHashMap<>();
//		mapOfVm_fuel_requestDTOToapproverOfficeNameEn = new ConcurrentHashMap<>();
//		mapOfVm_fuel_requestDTOToapproverOfficeNameBn = new ConcurrentHashMap<>();
//		mapOfVm_fuel_requestDTOToapproverOfficeUnitNameEn = new ConcurrentHashMap<>();
//		mapOfVm_fuel_requestDTOToapproverOfficeUnitNameBn = new ConcurrentHashMap<>();
//		mapOfVm_fuel_requestDTOToapproverOfficeUnitOrgNameEn = new ConcurrentHashMap<>();
//		mapOfVm_fuel_requestDTOToapproverOfficeUnitOrgNameBn = new ConcurrentHashMap<>();
//		mapOfVm_fuel_requestDTOTopaymentReceiverOrgId = new ConcurrentHashMap<>();
//		mapOfVm_fuel_requestDTOTopaymentReceiverOfficeId = new ConcurrentHashMap<>();
//		mapOfVm_fuel_requestDTOTopaymentReceiverOfficeUnitId = new ConcurrentHashMap<>();
//		mapOfVm_fuel_requestDTOTopaymentReceiverEmpId = new ConcurrentHashMap<>();
//		mapOfVm_fuel_requestDTOTopaymentReceiverPhoneNum = new ConcurrentHashMap<>();
//		mapOfVm_fuel_requestDTOTopaymentReceiverNameEn = new ConcurrentHashMap<>();
//		mapOfVm_fuel_requestDTOTopaymentReceiverNameBn = new ConcurrentHashMap<>();
//		mapOfVm_fuel_requestDTOTopaymentReceiverOfficeNameEn = new ConcurrentHashMap<>();
//		mapOfVm_fuel_requestDTOTopaymentReceiverOfficeNameBn = new ConcurrentHashMap<>();
//		mapOfVm_fuel_requestDTOTopaymentReceiverOfficeUnitNameEn = new ConcurrentHashMap<>();
//		mapOfVm_fuel_requestDTOTopaymentReceiverOfficeUnitNameBn = new ConcurrentHashMap<>();
//		mapOfVm_fuel_requestDTOTopaymentReceiverOfficeUnitOrgNameEn = new ConcurrentHashMap<>();
//		mapOfVm_fuel_requestDTOTopaymentReceiverOfficeUnitOrgNameBn = new ConcurrentHashMap<>();

		setDAO(new Vm_fuel_requestDAO());

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Vm_fuel_requestRepository getInstance(){
		if (instance == null){
			instance = new Vm_fuel_requestRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(vm_fuel_requestDAO == null)
		{
			return;
		}
		try {
			List<Vm_fuel_requestDTO> vm_fuel_requestDTOs = vm_fuel_requestDAO.getAllVm_fuel_request(reloadAll);
			for(Vm_fuel_requestDTO vm_fuel_requestDTO : vm_fuel_requestDTOs) {
				Vm_fuel_requestDTO oldVm_fuel_requestDTO = getVm_fuel_requestDTOByIDWithoutClone(vm_fuel_requestDTO.iD);
				if( oldVm_fuel_requestDTO != null ) {
					mapOfVm_fuel_requestDTOToiD.remove(oldVm_fuel_requestDTO.iD);
				
//					if(mapOfVm_fuel_requestDTOToinsertedByUserId.containsKey(oldVm_fuel_requestDTO.insertedByUserId)) {
//						mapOfVm_fuel_requestDTOToinsertedByUserId.get(oldVm_fuel_requestDTO.insertedByUserId).remove(oldVm_fuel_requestDTO);
//					}
//					if(mapOfVm_fuel_requestDTOToinsertedByUserId.get(oldVm_fuel_requestDTO.insertedByUserId).isEmpty()) {
//						mapOfVm_fuel_requestDTOToinsertedByUserId.remove(oldVm_fuel_requestDTO.insertedByUserId);
//					}
//
//					if(mapOfVm_fuel_requestDTOToinsertedByOrganogramId.containsKey(oldVm_fuel_requestDTO.insertedByOrganogramId)) {
//						mapOfVm_fuel_requestDTOToinsertedByOrganogramId.get(oldVm_fuel_requestDTO.insertedByOrganogramId).remove(oldVm_fuel_requestDTO);
//					}
//					if(mapOfVm_fuel_requestDTOToinsertedByOrganogramId.get(oldVm_fuel_requestDTO.insertedByOrganogramId).isEmpty()) {
//						mapOfVm_fuel_requestDTOToinsertedByOrganogramId.remove(oldVm_fuel_requestDTO.insertedByOrganogramId);
//					}
//
//					if(mapOfVm_fuel_requestDTOToinsertionDate.containsKey(oldVm_fuel_requestDTO.insertionDate)) {
//						mapOfVm_fuel_requestDTOToinsertionDate.get(oldVm_fuel_requestDTO.insertionDate).remove(oldVm_fuel_requestDTO);
//					}
//					if(mapOfVm_fuel_requestDTOToinsertionDate.get(oldVm_fuel_requestDTO.insertionDate).isEmpty()) {
//						mapOfVm_fuel_requestDTOToinsertionDate.remove(oldVm_fuel_requestDTO.insertionDate);
//					}
//
//					if(mapOfVm_fuel_requestDTOTolastModificationTime.containsKey(oldVm_fuel_requestDTO.lastModificationTime)) {
//						mapOfVm_fuel_requestDTOTolastModificationTime.get(oldVm_fuel_requestDTO.lastModificationTime).remove(oldVm_fuel_requestDTO);
//					}
//					if(mapOfVm_fuel_requestDTOTolastModificationTime.get(oldVm_fuel_requestDTO.lastModificationTime).isEmpty()) {
//						mapOfVm_fuel_requestDTOTolastModificationTime.remove(oldVm_fuel_requestDTO.lastModificationTime);
//					}
//
//					if(mapOfVm_fuel_requestDTOTosearchColumn.containsKey(oldVm_fuel_requestDTO.searchColumn)) {
//						mapOfVm_fuel_requestDTOTosearchColumn.get(oldVm_fuel_requestDTO.searchColumn).remove(oldVm_fuel_requestDTO);
//					}
//					if(mapOfVm_fuel_requestDTOTosearchColumn.get(oldVm_fuel_requestDTO.searchColumn).isEmpty()) {
//						mapOfVm_fuel_requestDTOTosearchColumn.remove(oldVm_fuel_requestDTO.searchColumn);
//					}
//
//					if(mapOfVm_fuel_requestDTOTovehicleType.containsKey(oldVm_fuel_requestDTO.vehicleType)) {
//						mapOfVm_fuel_requestDTOTovehicleType.get(oldVm_fuel_requestDTO.vehicleType).remove(oldVm_fuel_requestDTO);
//					}
//					if(mapOfVm_fuel_requestDTOTovehicleType.get(oldVm_fuel_requestDTO.vehicleType).isEmpty()) {
//						mapOfVm_fuel_requestDTOTovehicleType.remove(oldVm_fuel_requestDTO.vehicleType);
//					}
//
					if(mapOfVm_fuel_requestDTOTovehicleId.containsKey(oldVm_fuel_requestDTO.vehicleId)) {
						mapOfVm_fuel_requestDTOTovehicleId.get(oldVm_fuel_requestDTO.vehicleId).remove(oldVm_fuel_requestDTO);
					}
					if(mapOfVm_fuel_requestDTOTovehicleId.get(oldVm_fuel_requestDTO.vehicleId).isEmpty()) {
						mapOfVm_fuel_requestDTOTovehicleId.remove(oldVm_fuel_requestDTO.vehicleId);
					}
//
//					if(mapOfVm_fuel_requestDTOToapplicationDate.containsKey(oldVm_fuel_requestDTO.applicationDate)) {
//						mapOfVm_fuel_requestDTOToapplicationDate.get(oldVm_fuel_requestDTO.applicationDate).remove(oldVm_fuel_requestDTO);
//					}
//					if(mapOfVm_fuel_requestDTOToapplicationDate.get(oldVm_fuel_requestDTO.applicationDate).isEmpty()) {
//						mapOfVm_fuel_requestDTOToapplicationDate.remove(oldVm_fuel_requestDTO.applicationDate);
//					}
//
//					if(mapOfVm_fuel_requestDTOTovehicleDriverAssignmentId.containsKey(oldVm_fuel_requestDTO.vehicleDriverAssignmentId)) {
//						mapOfVm_fuel_requestDTOTovehicleDriverAssignmentId.get(oldVm_fuel_requestDTO.vehicleDriverAssignmentId).remove(oldVm_fuel_requestDTO);
//					}
//					if(mapOfVm_fuel_requestDTOTovehicleDriverAssignmentId.get(oldVm_fuel_requestDTO.vehicleDriverAssignmentId).isEmpty()) {
//						mapOfVm_fuel_requestDTOTovehicleDriverAssignmentId.remove(oldVm_fuel_requestDTO.vehicleDriverAssignmentId);
//					}
//
//					if(mapOfVm_fuel_requestDTOTolastFuelDate.containsKey(oldVm_fuel_requestDTO.lastFuelDate)) {
//						mapOfVm_fuel_requestDTOTolastFuelDate.get(oldVm_fuel_requestDTO.lastFuelDate).remove(oldVm_fuel_requestDTO);
//					}
//					if(mapOfVm_fuel_requestDTOTolastFuelDate.get(oldVm_fuel_requestDTO.lastFuelDate).isEmpty()) {
//						mapOfVm_fuel_requestDTOTolastFuelDate.remove(oldVm_fuel_requestDTO.lastFuelDate);
//					}
//
//					if(mapOfVm_fuel_requestDTOTostatus.containsKey(oldVm_fuel_requestDTO.status)) {
//						mapOfVm_fuel_requestDTOTostatus.get(oldVm_fuel_requestDTO.status).remove(oldVm_fuel_requestDTO);
//					}
//					if(mapOfVm_fuel_requestDTOTostatus.get(oldVm_fuel_requestDTO.status).isEmpty()) {
//						mapOfVm_fuel_requestDTOTostatus.remove(oldVm_fuel_requestDTO.status);
//					}
//
//					if(mapOfVm_fuel_requestDTOTofiscalYearId.containsKey(oldVm_fuel_requestDTO.fiscalYearId)) {
//						mapOfVm_fuel_requestDTOTofiscalYearId.get(oldVm_fuel_requestDTO.fiscalYearId).remove(oldVm_fuel_requestDTO);
//					}
//					if(mapOfVm_fuel_requestDTOTofiscalYearId.get(oldVm_fuel_requestDTO.fiscalYearId).isEmpty()) {
//						mapOfVm_fuel_requestDTOTofiscalYearId.remove(oldVm_fuel_requestDTO.fiscalYearId);
//					}
//
//					if(mapOfVm_fuel_requestDTOToapprovedDate.containsKey(oldVm_fuel_requestDTO.approvedDate)) {
//						mapOfVm_fuel_requestDTOToapprovedDate.get(oldVm_fuel_requestDTO.approvedDate).remove(oldVm_fuel_requestDTO);
//					}
//					if(mapOfVm_fuel_requestDTOToapprovedDate.get(oldVm_fuel_requestDTO.approvedDate).isEmpty()) {
//						mapOfVm_fuel_requestDTOToapprovedDate.remove(oldVm_fuel_requestDTO.approvedDate);
//					}
//
//					if(mapOfVm_fuel_requestDTOTovendorId.containsKey(oldVm_fuel_requestDTO.vendorId)) {
//						mapOfVm_fuel_requestDTOTovendorId.get(oldVm_fuel_requestDTO.vendorId).remove(oldVm_fuel_requestDTO);
//					}
//					if(mapOfVm_fuel_requestDTOTovendorId.get(oldVm_fuel_requestDTO.vendorId).isEmpty()) {
//						mapOfVm_fuel_requestDTOTovendorId.remove(oldVm_fuel_requestDTO.vendorId);
//					}
//
//					if(mapOfVm_fuel_requestDTOTosarokNumber.containsKey(oldVm_fuel_requestDTO.sarokNumber)) {
//						mapOfVm_fuel_requestDTOTosarokNumber.get(oldVm_fuel_requestDTO.sarokNumber).remove(oldVm_fuel_requestDTO);
//					}
//					if(mapOfVm_fuel_requestDTOTosarokNumber.get(oldVm_fuel_requestDTO.sarokNumber).isEmpty()) {
//						mapOfVm_fuel_requestDTOTosarokNumber.remove(oldVm_fuel_requestDTO.sarokNumber);
//					}
//
//					if(mapOfVm_fuel_requestDTOTofilesDropzone.containsKey(oldVm_fuel_requestDTO.filesDropzone)) {
//						mapOfVm_fuel_requestDTOTofilesDropzone.get(oldVm_fuel_requestDTO.filesDropzone).remove(oldVm_fuel_requestDTO);
//					}
//					if(mapOfVm_fuel_requestDTOTofilesDropzone.get(oldVm_fuel_requestDTO.filesDropzone).isEmpty()) {
//						mapOfVm_fuel_requestDTOTofilesDropzone.remove(oldVm_fuel_requestDTO.filesDropzone);
//					}
//
//					if(mapOfVm_fuel_requestDTOTopaymentRemarks.containsKey(oldVm_fuel_requestDTO.paymentRemarks)) {
//						mapOfVm_fuel_requestDTOTopaymentRemarks.get(oldVm_fuel_requestDTO.paymentRemarks).remove(oldVm_fuel_requestDTO);
//					}
//					if(mapOfVm_fuel_requestDTOTopaymentRemarks.get(oldVm_fuel_requestDTO.paymentRemarks).isEmpty()) {
//						mapOfVm_fuel_requestDTOTopaymentRemarks.remove(oldVm_fuel_requestDTO.paymentRemarks);
//					}
//
//					if(mapOfVm_fuel_requestDTOTorequesterOrgId.containsKey(oldVm_fuel_requestDTO.requesterOrgId)) {
//						mapOfVm_fuel_requestDTOTorequesterOrgId.get(oldVm_fuel_requestDTO.requesterOrgId).remove(oldVm_fuel_requestDTO);
//					}
//					if(mapOfVm_fuel_requestDTOTorequesterOrgId.get(oldVm_fuel_requestDTO.requesterOrgId).isEmpty()) {
//						mapOfVm_fuel_requestDTOTorequesterOrgId.remove(oldVm_fuel_requestDTO.requesterOrgId);
//					}
//
//					if(mapOfVm_fuel_requestDTOTorequesterOfficeId.containsKey(oldVm_fuel_requestDTO.requesterOfficeId)) {
//						mapOfVm_fuel_requestDTOTorequesterOfficeId.get(oldVm_fuel_requestDTO.requesterOfficeId).remove(oldVm_fuel_requestDTO);
//					}
//					if(mapOfVm_fuel_requestDTOTorequesterOfficeId.get(oldVm_fuel_requestDTO.requesterOfficeId).isEmpty()) {
//						mapOfVm_fuel_requestDTOTorequesterOfficeId.remove(oldVm_fuel_requestDTO.requesterOfficeId);
//					}
//
//					if(mapOfVm_fuel_requestDTOTorequesterOfficeUnitId.containsKey(oldVm_fuel_requestDTO.requesterOfficeUnitId)) {
//						mapOfVm_fuel_requestDTOTorequesterOfficeUnitId.get(oldVm_fuel_requestDTO.requesterOfficeUnitId).remove(oldVm_fuel_requestDTO);
//					}
//					if(mapOfVm_fuel_requestDTOTorequesterOfficeUnitId.get(oldVm_fuel_requestDTO.requesterOfficeUnitId).isEmpty()) {
//						mapOfVm_fuel_requestDTOTorequesterOfficeUnitId.remove(oldVm_fuel_requestDTO.requesterOfficeUnitId);
//					}
//
//					if(mapOfVm_fuel_requestDTOTorequesterEmpId.containsKey(oldVm_fuel_requestDTO.requesterEmpId)) {
//						mapOfVm_fuel_requestDTOTorequesterEmpId.get(oldVm_fuel_requestDTO.requesterEmpId).remove(oldVm_fuel_requestDTO);
//					}
//					if(mapOfVm_fuel_requestDTOTorequesterEmpId.get(oldVm_fuel_requestDTO.requesterEmpId).isEmpty()) {
//						mapOfVm_fuel_requestDTOTorequesterEmpId.remove(oldVm_fuel_requestDTO.requesterEmpId);
//					}
//
//					if(mapOfVm_fuel_requestDTOTorequesterPhoneNum.containsKey(oldVm_fuel_requestDTO.requesterPhoneNum)) {
//						mapOfVm_fuel_requestDTOTorequesterPhoneNum.get(oldVm_fuel_requestDTO.requesterPhoneNum).remove(oldVm_fuel_requestDTO);
//					}
//					if(mapOfVm_fuel_requestDTOTorequesterPhoneNum.get(oldVm_fuel_requestDTO.requesterPhoneNum).isEmpty()) {
//						mapOfVm_fuel_requestDTOTorequesterPhoneNum.remove(oldVm_fuel_requestDTO.requesterPhoneNum);
//					}
//
//					if(mapOfVm_fuel_requestDTOTorequesterNameEn.containsKey(oldVm_fuel_requestDTO.requesterNameEn)) {
//						mapOfVm_fuel_requestDTOTorequesterNameEn.get(oldVm_fuel_requestDTO.requesterNameEn).remove(oldVm_fuel_requestDTO);
//					}
//					if(mapOfVm_fuel_requestDTOTorequesterNameEn.get(oldVm_fuel_requestDTO.requesterNameEn).isEmpty()) {
//						mapOfVm_fuel_requestDTOTorequesterNameEn.remove(oldVm_fuel_requestDTO.requesterNameEn);
//					}
//
//					if(mapOfVm_fuel_requestDTOTorequesterNameBn.containsKey(oldVm_fuel_requestDTO.requesterNameBn)) {
//						mapOfVm_fuel_requestDTOTorequesterNameBn.get(oldVm_fuel_requestDTO.requesterNameBn).remove(oldVm_fuel_requestDTO);
//					}
//					if(mapOfVm_fuel_requestDTOTorequesterNameBn.get(oldVm_fuel_requestDTO.requesterNameBn).isEmpty()) {
//						mapOfVm_fuel_requestDTOTorequesterNameBn.remove(oldVm_fuel_requestDTO.requesterNameBn);
//					}
//
//					if(mapOfVm_fuel_requestDTOTorequesterOfficeNameEn.containsKey(oldVm_fuel_requestDTO.requesterOfficeNameEn)) {
//						mapOfVm_fuel_requestDTOTorequesterOfficeNameEn.get(oldVm_fuel_requestDTO.requesterOfficeNameEn).remove(oldVm_fuel_requestDTO);
//					}
//					if(mapOfVm_fuel_requestDTOTorequesterOfficeNameEn.get(oldVm_fuel_requestDTO.requesterOfficeNameEn).isEmpty()) {
//						mapOfVm_fuel_requestDTOTorequesterOfficeNameEn.remove(oldVm_fuel_requestDTO.requesterOfficeNameEn);
//					}
//
//					if(mapOfVm_fuel_requestDTOTorequesterOfficeNameBn.containsKey(oldVm_fuel_requestDTO.requesterOfficeNameBn)) {
//						mapOfVm_fuel_requestDTOTorequesterOfficeNameBn.get(oldVm_fuel_requestDTO.requesterOfficeNameBn).remove(oldVm_fuel_requestDTO);
//					}
//					if(mapOfVm_fuel_requestDTOTorequesterOfficeNameBn.get(oldVm_fuel_requestDTO.requesterOfficeNameBn).isEmpty()) {
//						mapOfVm_fuel_requestDTOTorequesterOfficeNameBn.remove(oldVm_fuel_requestDTO.requesterOfficeNameBn);
//					}
//
//					if(mapOfVm_fuel_requestDTOTorequesterOfficeUnitNameEn.containsKey(oldVm_fuel_requestDTO.requesterOfficeUnitNameEn)) {
//						mapOfVm_fuel_requestDTOTorequesterOfficeUnitNameEn.get(oldVm_fuel_requestDTO.requesterOfficeUnitNameEn).remove(oldVm_fuel_requestDTO);
//					}
//					if(mapOfVm_fuel_requestDTOTorequesterOfficeUnitNameEn.get(oldVm_fuel_requestDTO.requesterOfficeUnitNameEn).isEmpty()) {
//						mapOfVm_fuel_requestDTOTorequesterOfficeUnitNameEn.remove(oldVm_fuel_requestDTO.requesterOfficeUnitNameEn);
//					}
//
//					if(mapOfVm_fuel_requestDTOTorequesterOfficeUnitNameBn.containsKey(oldVm_fuel_requestDTO.requesterOfficeUnitNameBn)) {
//						mapOfVm_fuel_requestDTOTorequesterOfficeUnitNameBn.get(oldVm_fuel_requestDTO.requesterOfficeUnitNameBn).remove(oldVm_fuel_requestDTO);
//					}
//					if(mapOfVm_fuel_requestDTOTorequesterOfficeUnitNameBn.get(oldVm_fuel_requestDTO.requesterOfficeUnitNameBn).isEmpty()) {
//						mapOfVm_fuel_requestDTOTorequesterOfficeUnitNameBn.remove(oldVm_fuel_requestDTO.requesterOfficeUnitNameBn);
//					}
//
//					if(mapOfVm_fuel_requestDTOTorequesterOfficeUnitOrgNameEn.containsKey(oldVm_fuel_requestDTO.requesterOfficeUnitOrgNameEn)) {
//						mapOfVm_fuel_requestDTOTorequesterOfficeUnitOrgNameEn.get(oldVm_fuel_requestDTO.requesterOfficeUnitOrgNameEn).remove(oldVm_fuel_requestDTO);
//					}
//					if(mapOfVm_fuel_requestDTOTorequesterOfficeUnitOrgNameEn.get(oldVm_fuel_requestDTO.requesterOfficeUnitOrgNameEn).isEmpty()) {
//						mapOfVm_fuel_requestDTOTorequesterOfficeUnitOrgNameEn.remove(oldVm_fuel_requestDTO.requesterOfficeUnitOrgNameEn);
//					}
//
//					if(mapOfVm_fuel_requestDTOTorequesterOfficeUnitOrgNameBn.containsKey(oldVm_fuel_requestDTO.requesterOfficeUnitOrgNameBn)) {
//						mapOfVm_fuel_requestDTOTorequesterOfficeUnitOrgNameBn.get(oldVm_fuel_requestDTO.requesterOfficeUnitOrgNameBn).remove(oldVm_fuel_requestDTO);
//					}
//					if(mapOfVm_fuel_requestDTOTorequesterOfficeUnitOrgNameBn.get(oldVm_fuel_requestDTO.requesterOfficeUnitOrgNameBn).isEmpty()) {
//						mapOfVm_fuel_requestDTOTorequesterOfficeUnitOrgNameBn.remove(oldVm_fuel_requestDTO.requesterOfficeUnitOrgNameBn);
//					}
//
//					if(mapOfVm_fuel_requestDTOToapproverOrgId.containsKey(oldVm_fuel_requestDTO.approverOrgId)) {
//						mapOfVm_fuel_requestDTOToapproverOrgId.get(oldVm_fuel_requestDTO.approverOrgId).remove(oldVm_fuel_requestDTO);
//					}
//					if(mapOfVm_fuel_requestDTOToapproverOrgId.get(oldVm_fuel_requestDTO.approverOrgId).isEmpty()) {
//						mapOfVm_fuel_requestDTOToapproverOrgId.remove(oldVm_fuel_requestDTO.approverOrgId);
//					}
//
//					if(mapOfVm_fuel_requestDTOToapproverOfficeId.containsKey(oldVm_fuel_requestDTO.approverOfficeId)) {
//						mapOfVm_fuel_requestDTOToapproverOfficeId.get(oldVm_fuel_requestDTO.approverOfficeId).remove(oldVm_fuel_requestDTO);
//					}
//					if(mapOfVm_fuel_requestDTOToapproverOfficeId.get(oldVm_fuel_requestDTO.approverOfficeId).isEmpty()) {
//						mapOfVm_fuel_requestDTOToapproverOfficeId.remove(oldVm_fuel_requestDTO.approverOfficeId);
//					}
//
//					if(mapOfVm_fuel_requestDTOToapproverOfficeUnitId.containsKey(oldVm_fuel_requestDTO.approverOfficeUnitId)) {
//						mapOfVm_fuel_requestDTOToapproverOfficeUnitId.get(oldVm_fuel_requestDTO.approverOfficeUnitId).remove(oldVm_fuel_requestDTO);
//					}
//					if(mapOfVm_fuel_requestDTOToapproverOfficeUnitId.get(oldVm_fuel_requestDTO.approverOfficeUnitId).isEmpty()) {
//						mapOfVm_fuel_requestDTOToapproverOfficeUnitId.remove(oldVm_fuel_requestDTO.approverOfficeUnitId);
//					}
//
//					if(mapOfVm_fuel_requestDTOToapproverEmpId.containsKey(oldVm_fuel_requestDTO.approverEmpId)) {
//						mapOfVm_fuel_requestDTOToapproverEmpId.get(oldVm_fuel_requestDTO.approverEmpId).remove(oldVm_fuel_requestDTO);
//					}
//					if(mapOfVm_fuel_requestDTOToapproverEmpId.get(oldVm_fuel_requestDTO.approverEmpId).isEmpty()) {
//						mapOfVm_fuel_requestDTOToapproverEmpId.remove(oldVm_fuel_requestDTO.approverEmpId);
//					}
//
//					if(mapOfVm_fuel_requestDTOToapproverPhoneNum.containsKey(oldVm_fuel_requestDTO.approverPhoneNum)) {
//						mapOfVm_fuel_requestDTOToapproverPhoneNum.get(oldVm_fuel_requestDTO.approverPhoneNum).remove(oldVm_fuel_requestDTO);
//					}
//					if(mapOfVm_fuel_requestDTOToapproverPhoneNum.get(oldVm_fuel_requestDTO.approverPhoneNum).isEmpty()) {
//						mapOfVm_fuel_requestDTOToapproverPhoneNum.remove(oldVm_fuel_requestDTO.approverPhoneNum);
//					}
//
//					if(mapOfVm_fuel_requestDTOToapproverNameEn.containsKey(oldVm_fuel_requestDTO.approverNameEn)) {
//						mapOfVm_fuel_requestDTOToapproverNameEn.get(oldVm_fuel_requestDTO.approverNameEn).remove(oldVm_fuel_requestDTO);
//					}
//					if(mapOfVm_fuel_requestDTOToapproverNameEn.get(oldVm_fuel_requestDTO.approverNameEn).isEmpty()) {
//						mapOfVm_fuel_requestDTOToapproverNameEn.remove(oldVm_fuel_requestDTO.approverNameEn);
//					}
//
//					if(mapOfVm_fuel_requestDTOToapproverNameBn.containsKey(oldVm_fuel_requestDTO.approverNameBn)) {
//						mapOfVm_fuel_requestDTOToapproverNameBn.get(oldVm_fuel_requestDTO.approverNameBn).remove(oldVm_fuel_requestDTO);
//					}
//					if(mapOfVm_fuel_requestDTOToapproverNameBn.get(oldVm_fuel_requestDTO.approverNameBn).isEmpty()) {
//						mapOfVm_fuel_requestDTOToapproverNameBn.remove(oldVm_fuel_requestDTO.approverNameBn);
//					}
//
//					if(mapOfVm_fuel_requestDTOToapproverOfficeNameEn.containsKey(oldVm_fuel_requestDTO.approverOfficeNameEn)) {
//						mapOfVm_fuel_requestDTOToapproverOfficeNameEn.get(oldVm_fuel_requestDTO.approverOfficeNameEn).remove(oldVm_fuel_requestDTO);
//					}
//					if(mapOfVm_fuel_requestDTOToapproverOfficeNameEn.get(oldVm_fuel_requestDTO.approverOfficeNameEn).isEmpty()) {
//						mapOfVm_fuel_requestDTOToapproverOfficeNameEn.remove(oldVm_fuel_requestDTO.approverOfficeNameEn);
//					}
//
//					if(mapOfVm_fuel_requestDTOToapproverOfficeNameBn.containsKey(oldVm_fuel_requestDTO.approverOfficeNameBn)) {
//						mapOfVm_fuel_requestDTOToapproverOfficeNameBn.get(oldVm_fuel_requestDTO.approverOfficeNameBn).remove(oldVm_fuel_requestDTO);
//					}
//					if(mapOfVm_fuel_requestDTOToapproverOfficeNameBn.get(oldVm_fuel_requestDTO.approverOfficeNameBn).isEmpty()) {
//						mapOfVm_fuel_requestDTOToapproverOfficeNameBn.remove(oldVm_fuel_requestDTO.approverOfficeNameBn);
//					}
//
//					if(mapOfVm_fuel_requestDTOToapproverOfficeUnitNameEn.containsKey(oldVm_fuel_requestDTO.approverOfficeUnitNameEn)) {
//						mapOfVm_fuel_requestDTOToapproverOfficeUnitNameEn.get(oldVm_fuel_requestDTO.approverOfficeUnitNameEn).remove(oldVm_fuel_requestDTO);
//					}
//					if(mapOfVm_fuel_requestDTOToapproverOfficeUnitNameEn.get(oldVm_fuel_requestDTO.approverOfficeUnitNameEn).isEmpty()) {
//						mapOfVm_fuel_requestDTOToapproverOfficeUnitNameEn.remove(oldVm_fuel_requestDTO.approverOfficeUnitNameEn);
//					}
//
//					if(mapOfVm_fuel_requestDTOToapproverOfficeUnitNameBn.containsKey(oldVm_fuel_requestDTO.approverOfficeUnitNameBn)) {
//						mapOfVm_fuel_requestDTOToapproverOfficeUnitNameBn.get(oldVm_fuel_requestDTO.approverOfficeUnitNameBn).remove(oldVm_fuel_requestDTO);
//					}
//					if(mapOfVm_fuel_requestDTOToapproverOfficeUnitNameBn.get(oldVm_fuel_requestDTO.approverOfficeUnitNameBn).isEmpty()) {
//						mapOfVm_fuel_requestDTOToapproverOfficeUnitNameBn.remove(oldVm_fuel_requestDTO.approverOfficeUnitNameBn);
//					}
//
//					if(mapOfVm_fuel_requestDTOToapproverOfficeUnitOrgNameEn.containsKey(oldVm_fuel_requestDTO.approverOfficeUnitOrgNameEn)) {
//						mapOfVm_fuel_requestDTOToapproverOfficeUnitOrgNameEn.get(oldVm_fuel_requestDTO.approverOfficeUnitOrgNameEn).remove(oldVm_fuel_requestDTO);
//					}
//					if(mapOfVm_fuel_requestDTOToapproverOfficeUnitOrgNameEn.get(oldVm_fuel_requestDTO.approverOfficeUnitOrgNameEn).isEmpty()) {
//						mapOfVm_fuel_requestDTOToapproverOfficeUnitOrgNameEn.remove(oldVm_fuel_requestDTO.approverOfficeUnitOrgNameEn);
//					}
//
//					if(mapOfVm_fuel_requestDTOToapproverOfficeUnitOrgNameBn.containsKey(oldVm_fuel_requestDTO.approverOfficeUnitOrgNameBn)) {
//						mapOfVm_fuel_requestDTOToapproverOfficeUnitOrgNameBn.get(oldVm_fuel_requestDTO.approverOfficeUnitOrgNameBn).remove(oldVm_fuel_requestDTO);
//					}
//					if(mapOfVm_fuel_requestDTOToapproverOfficeUnitOrgNameBn.get(oldVm_fuel_requestDTO.approverOfficeUnitOrgNameBn).isEmpty()) {
//						mapOfVm_fuel_requestDTOToapproverOfficeUnitOrgNameBn.remove(oldVm_fuel_requestDTO.approverOfficeUnitOrgNameBn);
//					}
//
//					if(mapOfVm_fuel_requestDTOTopaymentReceiverOrgId.containsKey(oldVm_fuel_requestDTO.paymentReceiverOrgId)) {
//						mapOfVm_fuel_requestDTOTopaymentReceiverOrgId.get(oldVm_fuel_requestDTO.paymentReceiverOrgId).remove(oldVm_fuel_requestDTO);
//					}
//					if(mapOfVm_fuel_requestDTOTopaymentReceiverOrgId.get(oldVm_fuel_requestDTO.paymentReceiverOrgId).isEmpty()) {
//						mapOfVm_fuel_requestDTOTopaymentReceiverOrgId.remove(oldVm_fuel_requestDTO.paymentReceiverOrgId);
//					}
//
//					if(mapOfVm_fuel_requestDTOTopaymentReceiverOfficeId.containsKey(oldVm_fuel_requestDTO.paymentReceiverOfficeId)) {
//						mapOfVm_fuel_requestDTOTopaymentReceiverOfficeId.get(oldVm_fuel_requestDTO.paymentReceiverOfficeId).remove(oldVm_fuel_requestDTO);
//					}
//					if(mapOfVm_fuel_requestDTOTopaymentReceiverOfficeId.get(oldVm_fuel_requestDTO.paymentReceiverOfficeId).isEmpty()) {
//						mapOfVm_fuel_requestDTOTopaymentReceiverOfficeId.remove(oldVm_fuel_requestDTO.paymentReceiverOfficeId);
//					}
//
//					if(mapOfVm_fuel_requestDTOTopaymentReceiverOfficeUnitId.containsKey(oldVm_fuel_requestDTO.paymentReceiverOfficeUnitId)) {
//						mapOfVm_fuel_requestDTOTopaymentReceiverOfficeUnitId.get(oldVm_fuel_requestDTO.paymentReceiverOfficeUnitId).remove(oldVm_fuel_requestDTO);
//					}
//					if(mapOfVm_fuel_requestDTOTopaymentReceiverOfficeUnitId.get(oldVm_fuel_requestDTO.paymentReceiverOfficeUnitId).isEmpty()) {
//						mapOfVm_fuel_requestDTOTopaymentReceiverOfficeUnitId.remove(oldVm_fuel_requestDTO.paymentReceiverOfficeUnitId);
//					}
//
//					if(mapOfVm_fuel_requestDTOTopaymentReceiverEmpId.containsKey(oldVm_fuel_requestDTO.paymentReceiverEmpId)) {
//						mapOfVm_fuel_requestDTOTopaymentReceiverEmpId.get(oldVm_fuel_requestDTO.paymentReceiverEmpId).remove(oldVm_fuel_requestDTO);
//					}
//					if(mapOfVm_fuel_requestDTOTopaymentReceiverEmpId.get(oldVm_fuel_requestDTO.paymentReceiverEmpId).isEmpty()) {
//						mapOfVm_fuel_requestDTOTopaymentReceiverEmpId.remove(oldVm_fuel_requestDTO.paymentReceiverEmpId);
//					}
//
//					if(mapOfVm_fuel_requestDTOTopaymentReceiverPhoneNum.containsKey(oldVm_fuel_requestDTO.paymentReceiverPhoneNum)) {
//						mapOfVm_fuel_requestDTOTopaymentReceiverPhoneNum.get(oldVm_fuel_requestDTO.paymentReceiverPhoneNum).remove(oldVm_fuel_requestDTO);
//					}
//					if(mapOfVm_fuel_requestDTOTopaymentReceiverPhoneNum.get(oldVm_fuel_requestDTO.paymentReceiverPhoneNum).isEmpty()) {
//						mapOfVm_fuel_requestDTOTopaymentReceiverPhoneNum.remove(oldVm_fuel_requestDTO.paymentReceiverPhoneNum);
//					}
//
//					if(mapOfVm_fuel_requestDTOTopaymentReceiverNameEn.containsKey(oldVm_fuel_requestDTO.paymentReceiverNameEn)) {
//						mapOfVm_fuel_requestDTOTopaymentReceiverNameEn.get(oldVm_fuel_requestDTO.paymentReceiverNameEn).remove(oldVm_fuel_requestDTO);
//					}
//					if(mapOfVm_fuel_requestDTOTopaymentReceiverNameEn.get(oldVm_fuel_requestDTO.paymentReceiverNameEn).isEmpty()) {
//						mapOfVm_fuel_requestDTOTopaymentReceiverNameEn.remove(oldVm_fuel_requestDTO.paymentReceiverNameEn);
//					}
//
//					if(mapOfVm_fuel_requestDTOTopaymentReceiverNameBn.containsKey(oldVm_fuel_requestDTO.paymentReceiverNameBn)) {
//						mapOfVm_fuel_requestDTOTopaymentReceiverNameBn.get(oldVm_fuel_requestDTO.paymentReceiverNameBn).remove(oldVm_fuel_requestDTO);
//					}
//					if(mapOfVm_fuel_requestDTOTopaymentReceiverNameBn.get(oldVm_fuel_requestDTO.paymentReceiverNameBn).isEmpty()) {
//						mapOfVm_fuel_requestDTOTopaymentReceiverNameBn.remove(oldVm_fuel_requestDTO.paymentReceiverNameBn);
//					}
//
//					if(mapOfVm_fuel_requestDTOTopaymentReceiverOfficeNameEn.containsKey(oldVm_fuel_requestDTO.paymentReceiverOfficeNameEn)) {
//						mapOfVm_fuel_requestDTOTopaymentReceiverOfficeNameEn.get(oldVm_fuel_requestDTO.paymentReceiverOfficeNameEn).remove(oldVm_fuel_requestDTO);
//					}
//					if(mapOfVm_fuel_requestDTOTopaymentReceiverOfficeNameEn.get(oldVm_fuel_requestDTO.paymentReceiverOfficeNameEn).isEmpty()) {
//						mapOfVm_fuel_requestDTOTopaymentReceiverOfficeNameEn.remove(oldVm_fuel_requestDTO.paymentReceiverOfficeNameEn);
//					}
//
//					if(mapOfVm_fuel_requestDTOTopaymentReceiverOfficeNameBn.containsKey(oldVm_fuel_requestDTO.paymentReceiverOfficeNameBn)) {
//						mapOfVm_fuel_requestDTOTopaymentReceiverOfficeNameBn.get(oldVm_fuel_requestDTO.paymentReceiverOfficeNameBn).remove(oldVm_fuel_requestDTO);
//					}
//					if(mapOfVm_fuel_requestDTOTopaymentReceiverOfficeNameBn.get(oldVm_fuel_requestDTO.paymentReceiverOfficeNameBn).isEmpty()) {
//						mapOfVm_fuel_requestDTOTopaymentReceiverOfficeNameBn.remove(oldVm_fuel_requestDTO.paymentReceiverOfficeNameBn);
//					}
//
//					if(mapOfVm_fuel_requestDTOTopaymentReceiverOfficeUnitNameEn.containsKey(oldVm_fuel_requestDTO.paymentReceiverOfficeUnitNameEn)) {
//						mapOfVm_fuel_requestDTOTopaymentReceiverOfficeUnitNameEn.get(oldVm_fuel_requestDTO.paymentReceiverOfficeUnitNameEn).remove(oldVm_fuel_requestDTO);
//					}
//					if(mapOfVm_fuel_requestDTOTopaymentReceiverOfficeUnitNameEn.get(oldVm_fuel_requestDTO.paymentReceiverOfficeUnitNameEn).isEmpty()) {
//						mapOfVm_fuel_requestDTOTopaymentReceiverOfficeUnitNameEn.remove(oldVm_fuel_requestDTO.paymentReceiverOfficeUnitNameEn);
//					}
//
//					if(mapOfVm_fuel_requestDTOTopaymentReceiverOfficeUnitNameBn.containsKey(oldVm_fuel_requestDTO.paymentReceiverOfficeUnitNameBn)) {
//						mapOfVm_fuel_requestDTOTopaymentReceiverOfficeUnitNameBn.get(oldVm_fuel_requestDTO.paymentReceiverOfficeUnitNameBn).remove(oldVm_fuel_requestDTO);
//					}
//					if(mapOfVm_fuel_requestDTOTopaymentReceiverOfficeUnitNameBn.get(oldVm_fuel_requestDTO.paymentReceiverOfficeUnitNameBn).isEmpty()) {
//						mapOfVm_fuel_requestDTOTopaymentReceiverOfficeUnitNameBn.remove(oldVm_fuel_requestDTO.paymentReceiverOfficeUnitNameBn);
//					}
//
//					if(mapOfVm_fuel_requestDTOTopaymentReceiverOfficeUnitOrgNameEn.containsKey(oldVm_fuel_requestDTO.paymentReceiverOfficeUnitOrgNameEn)) {
//						mapOfVm_fuel_requestDTOTopaymentReceiverOfficeUnitOrgNameEn.get(oldVm_fuel_requestDTO.paymentReceiverOfficeUnitOrgNameEn).remove(oldVm_fuel_requestDTO);
//					}
//					if(mapOfVm_fuel_requestDTOTopaymentReceiverOfficeUnitOrgNameEn.get(oldVm_fuel_requestDTO.paymentReceiverOfficeUnitOrgNameEn).isEmpty()) {
//						mapOfVm_fuel_requestDTOTopaymentReceiverOfficeUnitOrgNameEn.remove(oldVm_fuel_requestDTO.paymentReceiverOfficeUnitOrgNameEn);
//					}
//
//					if(mapOfVm_fuel_requestDTOTopaymentReceiverOfficeUnitOrgNameBn.containsKey(oldVm_fuel_requestDTO.paymentReceiverOfficeUnitOrgNameBn)) {
//						mapOfVm_fuel_requestDTOTopaymentReceiverOfficeUnitOrgNameBn.get(oldVm_fuel_requestDTO.paymentReceiverOfficeUnitOrgNameBn).remove(oldVm_fuel_requestDTO);
//					}
//					if(mapOfVm_fuel_requestDTOTopaymentReceiverOfficeUnitOrgNameBn.get(oldVm_fuel_requestDTO.paymentReceiverOfficeUnitOrgNameBn).isEmpty()) {
//						mapOfVm_fuel_requestDTOTopaymentReceiverOfficeUnitOrgNameBn.remove(oldVm_fuel_requestDTO.paymentReceiverOfficeUnitOrgNameBn);
//					}
					
					
				}
				if(vm_fuel_requestDTO.isDeleted == 0) 
				{
					
					mapOfVm_fuel_requestDTOToiD.put(vm_fuel_requestDTO.iD, vm_fuel_requestDTO);
				
//					if( ! mapOfVm_fuel_requestDTOToinsertedByUserId.containsKey(vm_fuel_requestDTO.insertedByUserId)) {
//						mapOfVm_fuel_requestDTOToinsertedByUserId.put(vm_fuel_requestDTO.insertedByUserId, new HashSet<>());
//					}
//					mapOfVm_fuel_requestDTOToinsertedByUserId.get(vm_fuel_requestDTO.insertedByUserId).add(vm_fuel_requestDTO);
//
//					if( ! mapOfVm_fuel_requestDTOToinsertedByOrganogramId.containsKey(vm_fuel_requestDTO.insertedByOrganogramId)) {
//						mapOfVm_fuel_requestDTOToinsertedByOrganogramId.put(vm_fuel_requestDTO.insertedByOrganogramId, new HashSet<>());
//					}
//					mapOfVm_fuel_requestDTOToinsertedByOrganogramId.get(vm_fuel_requestDTO.insertedByOrganogramId).add(vm_fuel_requestDTO);
//
//					if( ! mapOfVm_fuel_requestDTOToinsertionDate.containsKey(vm_fuel_requestDTO.insertionDate)) {
//						mapOfVm_fuel_requestDTOToinsertionDate.put(vm_fuel_requestDTO.insertionDate, new HashSet<>());
//					}
//					mapOfVm_fuel_requestDTOToinsertionDate.get(vm_fuel_requestDTO.insertionDate).add(vm_fuel_requestDTO);
//
//					if( ! mapOfVm_fuel_requestDTOTolastModificationTime.containsKey(vm_fuel_requestDTO.lastModificationTime)) {
//						mapOfVm_fuel_requestDTOTolastModificationTime.put(vm_fuel_requestDTO.lastModificationTime, new HashSet<>());
//					}
//					mapOfVm_fuel_requestDTOTolastModificationTime.get(vm_fuel_requestDTO.lastModificationTime).add(vm_fuel_requestDTO);
//
//					if( ! mapOfVm_fuel_requestDTOTosearchColumn.containsKey(vm_fuel_requestDTO.searchColumn)) {
//						mapOfVm_fuel_requestDTOTosearchColumn.put(vm_fuel_requestDTO.searchColumn, new HashSet<>());
//					}
//					mapOfVm_fuel_requestDTOTosearchColumn.get(vm_fuel_requestDTO.searchColumn).add(vm_fuel_requestDTO);
//
//					if( ! mapOfVm_fuel_requestDTOTovehicleType.containsKey(vm_fuel_requestDTO.vehicleType)) {
//						mapOfVm_fuel_requestDTOTovehicleType.put(vm_fuel_requestDTO.vehicleType, new HashSet<>());
//					}
//					mapOfVm_fuel_requestDTOTovehicleType.get(vm_fuel_requestDTO.vehicleType).add(vm_fuel_requestDTO);
//
					if( ! mapOfVm_fuel_requestDTOTovehicleId.containsKey(vm_fuel_requestDTO.vehicleId)) {
						mapOfVm_fuel_requestDTOTovehicleId.put(vm_fuel_requestDTO.vehicleId, new HashSet<>());
					}
					mapOfVm_fuel_requestDTOTovehicleId.get(vm_fuel_requestDTO.vehicleId).add(vm_fuel_requestDTO);
//
//					if( ! mapOfVm_fuel_requestDTOToapplicationDate.containsKey(vm_fuel_requestDTO.applicationDate)) {
//						mapOfVm_fuel_requestDTOToapplicationDate.put(vm_fuel_requestDTO.applicationDate, new HashSet<>());
//					}
//					mapOfVm_fuel_requestDTOToapplicationDate.get(vm_fuel_requestDTO.applicationDate).add(vm_fuel_requestDTO);
//
//					if( ! mapOfVm_fuel_requestDTOTovehicleDriverAssignmentId.containsKey(vm_fuel_requestDTO.vehicleDriverAssignmentId)) {
//						mapOfVm_fuel_requestDTOTovehicleDriverAssignmentId.put(vm_fuel_requestDTO.vehicleDriverAssignmentId, new HashSet<>());
//					}
//					mapOfVm_fuel_requestDTOTovehicleDriverAssignmentId.get(vm_fuel_requestDTO.vehicleDriverAssignmentId).add(vm_fuel_requestDTO);
//
//					if( ! mapOfVm_fuel_requestDTOTolastFuelDate.containsKey(vm_fuel_requestDTO.lastFuelDate)) {
//						mapOfVm_fuel_requestDTOTolastFuelDate.put(vm_fuel_requestDTO.lastFuelDate, new HashSet<>());
//					}
//					mapOfVm_fuel_requestDTOTolastFuelDate.get(vm_fuel_requestDTO.lastFuelDate).add(vm_fuel_requestDTO);
//
//					if( ! mapOfVm_fuel_requestDTOTostatus.containsKey(vm_fuel_requestDTO.status)) {
//						mapOfVm_fuel_requestDTOTostatus.put(vm_fuel_requestDTO.status, new HashSet<>());
//					}
//					mapOfVm_fuel_requestDTOTostatus.get(vm_fuel_requestDTO.status).add(vm_fuel_requestDTO);
//
//					if( ! mapOfVm_fuel_requestDTOTofiscalYearId.containsKey(vm_fuel_requestDTO.fiscalYearId)) {
//						mapOfVm_fuel_requestDTOTofiscalYearId.put(vm_fuel_requestDTO.fiscalYearId, new HashSet<>());
//					}
//					mapOfVm_fuel_requestDTOTofiscalYearId.get(vm_fuel_requestDTO.fiscalYearId).add(vm_fuel_requestDTO);
//
//					if( ! mapOfVm_fuel_requestDTOToapprovedDate.containsKey(vm_fuel_requestDTO.approvedDate)) {
//						mapOfVm_fuel_requestDTOToapprovedDate.put(vm_fuel_requestDTO.approvedDate, new HashSet<>());
//					}
//					mapOfVm_fuel_requestDTOToapprovedDate.get(vm_fuel_requestDTO.approvedDate).add(vm_fuel_requestDTO);
//
//					if( ! mapOfVm_fuel_requestDTOTovendorId.containsKey(vm_fuel_requestDTO.vendorId)) {
//						mapOfVm_fuel_requestDTOTovendorId.put(vm_fuel_requestDTO.vendorId, new HashSet<>());
//					}
//					mapOfVm_fuel_requestDTOTovendorId.get(vm_fuel_requestDTO.vendorId).add(vm_fuel_requestDTO);
//
//					if( ! mapOfVm_fuel_requestDTOTosarokNumber.containsKey(vm_fuel_requestDTO.sarokNumber)) {
//						mapOfVm_fuel_requestDTOTosarokNumber.put(vm_fuel_requestDTO.sarokNumber, new HashSet<>());
//					}
//					mapOfVm_fuel_requestDTOTosarokNumber.get(vm_fuel_requestDTO.sarokNumber).add(vm_fuel_requestDTO);
//
//					if( ! mapOfVm_fuel_requestDTOTofilesDropzone.containsKey(vm_fuel_requestDTO.filesDropzone)) {
//						mapOfVm_fuel_requestDTOTofilesDropzone.put(vm_fuel_requestDTO.filesDropzone, new HashSet<>());
//					}
//					mapOfVm_fuel_requestDTOTofilesDropzone.get(vm_fuel_requestDTO.filesDropzone).add(vm_fuel_requestDTO);
//
//					if( ! mapOfVm_fuel_requestDTOTopaymentRemarks.containsKey(vm_fuel_requestDTO.paymentRemarks)) {
//						mapOfVm_fuel_requestDTOTopaymentRemarks.put(vm_fuel_requestDTO.paymentRemarks, new HashSet<>());
//					}
//					mapOfVm_fuel_requestDTOTopaymentRemarks.get(vm_fuel_requestDTO.paymentRemarks).add(vm_fuel_requestDTO);
//
//					if( ! mapOfVm_fuel_requestDTOTorequesterOrgId.containsKey(vm_fuel_requestDTO.requesterOrgId)) {
//						mapOfVm_fuel_requestDTOTorequesterOrgId.put(vm_fuel_requestDTO.requesterOrgId, new HashSet<>());
//					}
//					mapOfVm_fuel_requestDTOTorequesterOrgId.get(vm_fuel_requestDTO.requesterOrgId).add(vm_fuel_requestDTO);
//
//					if( ! mapOfVm_fuel_requestDTOTorequesterOfficeId.containsKey(vm_fuel_requestDTO.requesterOfficeId)) {
//						mapOfVm_fuel_requestDTOTorequesterOfficeId.put(vm_fuel_requestDTO.requesterOfficeId, new HashSet<>());
//					}
//					mapOfVm_fuel_requestDTOTorequesterOfficeId.get(vm_fuel_requestDTO.requesterOfficeId).add(vm_fuel_requestDTO);
//
//					if( ! mapOfVm_fuel_requestDTOTorequesterOfficeUnitId.containsKey(vm_fuel_requestDTO.requesterOfficeUnitId)) {
//						mapOfVm_fuel_requestDTOTorequesterOfficeUnitId.put(vm_fuel_requestDTO.requesterOfficeUnitId, new HashSet<>());
//					}
//					mapOfVm_fuel_requestDTOTorequesterOfficeUnitId.get(vm_fuel_requestDTO.requesterOfficeUnitId).add(vm_fuel_requestDTO);
//
//					if( ! mapOfVm_fuel_requestDTOTorequesterEmpId.containsKey(vm_fuel_requestDTO.requesterEmpId)) {
//						mapOfVm_fuel_requestDTOTorequesterEmpId.put(vm_fuel_requestDTO.requesterEmpId, new HashSet<>());
//					}
//					mapOfVm_fuel_requestDTOTorequesterEmpId.get(vm_fuel_requestDTO.requesterEmpId).add(vm_fuel_requestDTO);
//
//					if( ! mapOfVm_fuel_requestDTOTorequesterPhoneNum.containsKey(vm_fuel_requestDTO.requesterPhoneNum)) {
//						mapOfVm_fuel_requestDTOTorequesterPhoneNum.put(vm_fuel_requestDTO.requesterPhoneNum, new HashSet<>());
//					}
//					mapOfVm_fuel_requestDTOTorequesterPhoneNum.get(vm_fuel_requestDTO.requesterPhoneNum).add(vm_fuel_requestDTO);
//
//					if( ! mapOfVm_fuel_requestDTOTorequesterNameEn.containsKey(vm_fuel_requestDTO.requesterNameEn)) {
//						mapOfVm_fuel_requestDTOTorequesterNameEn.put(vm_fuel_requestDTO.requesterNameEn, new HashSet<>());
//					}
//					mapOfVm_fuel_requestDTOTorequesterNameEn.get(vm_fuel_requestDTO.requesterNameEn).add(vm_fuel_requestDTO);
//
//					if( ! mapOfVm_fuel_requestDTOTorequesterNameBn.containsKey(vm_fuel_requestDTO.requesterNameBn)) {
//						mapOfVm_fuel_requestDTOTorequesterNameBn.put(vm_fuel_requestDTO.requesterNameBn, new HashSet<>());
//					}
//					mapOfVm_fuel_requestDTOTorequesterNameBn.get(vm_fuel_requestDTO.requesterNameBn).add(vm_fuel_requestDTO);
//
//					if( ! mapOfVm_fuel_requestDTOTorequesterOfficeNameEn.containsKey(vm_fuel_requestDTO.requesterOfficeNameEn)) {
//						mapOfVm_fuel_requestDTOTorequesterOfficeNameEn.put(vm_fuel_requestDTO.requesterOfficeNameEn, new HashSet<>());
//					}
//					mapOfVm_fuel_requestDTOTorequesterOfficeNameEn.get(vm_fuel_requestDTO.requesterOfficeNameEn).add(vm_fuel_requestDTO);
//
//					if( ! mapOfVm_fuel_requestDTOTorequesterOfficeNameBn.containsKey(vm_fuel_requestDTO.requesterOfficeNameBn)) {
//						mapOfVm_fuel_requestDTOTorequesterOfficeNameBn.put(vm_fuel_requestDTO.requesterOfficeNameBn, new HashSet<>());
//					}
//					mapOfVm_fuel_requestDTOTorequesterOfficeNameBn.get(vm_fuel_requestDTO.requesterOfficeNameBn).add(vm_fuel_requestDTO);
//
//					if( ! mapOfVm_fuel_requestDTOTorequesterOfficeUnitNameEn.containsKey(vm_fuel_requestDTO.requesterOfficeUnitNameEn)) {
//						mapOfVm_fuel_requestDTOTorequesterOfficeUnitNameEn.put(vm_fuel_requestDTO.requesterOfficeUnitNameEn, new HashSet<>());
//					}
//					mapOfVm_fuel_requestDTOTorequesterOfficeUnitNameEn.get(vm_fuel_requestDTO.requesterOfficeUnitNameEn).add(vm_fuel_requestDTO);
//
//					if( ! mapOfVm_fuel_requestDTOTorequesterOfficeUnitNameBn.containsKey(vm_fuel_requestDTO.requesterOfficeUnitNameBn)) {
//						mapOfVm_fuel_requestDTOTorequesterOfficeUnitNameBn.put(vm_fuel_requestDTO.requesterOfficeUnitNameBn, new HashSet<>());
//					}
//					mapOfVm_fuel_requestDTOTorequesterOfficeUnitNameBn.get(vm_fuel_requestDTO.requesterOfficeUnitNameBn).add(vm_fuel_requestDTO);
//
//					if( ! mapOfVm_fuel_requestDTOTorequesterOfficeUnitOrgNameEn.containsKey(vm_fuel_requestDTO.requesterOfficeUnitOrgNameEn)) {
//						mapOfVm_fuel_requestDTOTorequesterOfficeUnitOrgNameEn.put(vm_fuel_requestDTO.requesterOfficeUnitOrgNameEn, new HashSet<>());
//					}
//					mapOfVm_fuel_requestDTOTorequesterOfficeUnitOrgNameEn.get(vm_fuel_requestDTO.requesterOfficeUnitOrgNameEn).add(vm_fuel_requestDTO);
//
//					if( ! mapOfVm_fuel_requestDTOTorequesterOfficeUnitOrgNameBn.containsKey(vm_fuel_requestDTO.requesterOfficeUnitOrgNameBn)) {
//						mapOfVm_fuel_requestDTOTorequesterOfficeUnitOrgNameBn.put(vm_fuel_requestDTO.requesterOfficeUnitOrgNameBn, new HashSet<>());
//					}
//					mapOfVm_fuel_requestDTOTorequesterOfficeUnitOrgNameBn.get(vm_fuel_requestDTO.requesterOfficeUnitOrgNameBn).add(vm_fuel_requestDTO);
//
//					if( ! mapOfVm_fuel_requestDTOToapproverOrgId.containsKey(vm_fuel_requestDTO.approverOrgId)) {
//						mapOfVm_fuel_requestDTOToapproverOrgId.put(vm_fuel_requestDTO.approverOrgId, new HashSet<>());
//					}
//					mapOfVm_fuel_requestDTOToapproverOrgId.get(vm_fuel_requestDTO.approverOrgId).add(vm_fuel_requestDTO);
//
//					if( ! mapOfVm_fuel_requestDTOToapproverOfficeId.containsKey(vm_fuel_requestDTO.approverOfficeId)) {
//						mapOfVm_fuel_requestDTOToapproverOfficeId.put(vm_fuel_requestDTO.approverOfficeId, new HashSet<>());
//					}
//					mapOfVm_fuel_requestDTOToapproverOfficeId.get(vm_fuel_requestDTO.approverOfficeId).add(vm_fuel_requestDTO);
//
//					if( ! mapOfVm_fuel_requestDTOToapproverOfficeUnitId.containsKey(vm_fuel_requestDTO.approverOfficeUnitId)) {
//						mapOfVm_fuel_requestDTOToapproverOfficeUnitId.put(vm_fuel_requestDTO.approverOfficeUnitId, new HashSet<>());
//					}
//					mapOfVm_fuel_requestDTOToapproverOfficeUnitId.get(vm_fuel_requestDTO.approverOfficeUnitId).add(vm_fuel_requestDTO);
//
//					if( ! mapOfVm_fuel_requestDTOToapproverEmpId.containsKey(vm_fuel_requestDTO.approverEmpId)) {
//						mapOfVm_fuel_requestDTOToapproverEmpId.put(vm_fuel_requestDTO.approverEmpId, new HashSet<>());
//					}
//					mapOfVm_fuel_requestDTOToapproverEmpId.get(vm_fuel_requestDTO.approverEmpId).add(vm_fuel_requestDTO);
//
//					if( ! mapOfVm_fuel_requestDTOToapproverPhoneNum.containsKey(vm_fuel_requestDTO.approverPhoneNum)) {
//						mapOfVm_fuel_requestDTOToapproverPhoneNum.put(vm_fuel_requestDTO.approverPhoneNum, new HashSet<>());
//					}
//					mapOfVm_fuel_requestDTOToapproverPhoneNum.get(vm_fuel_requestDTO.approverPhoneNum).add(vm_fuel_requestDTO);
//
//					if( ! mapOfVm_fuel_requestDTOToapproverNameEn.containsKey(vm_fuel_requestDTO.approverNameEn)) {
//						mapOfVm_fuel_requestDTOToapproverNameEn.put(vm_fuel_requestDTO.approverNameEn, new HashSet<>());
//					}
//					mapOfVm_fuel_requestDTOToapproverNameEn.get(vm_fuel_requestDTO.approverNameEn).add(vm_fuel_requestDTO);
//
//					if( ! mapOfVm_fuel_requestDTOToapproverNameBn.containsKey(vm_fuel_requestDTO.approverNameBn)) {
//						mapOfVm_fuel_requestDTOToapproverNameBn.put(vm_fuel_requestDTO.approverNameBn, new HashSet<>());
//					}
//					mapOfVm_fuel_requestDTOToapproverNameBn.get(vm_fuel_requestDTO.approverNameBn).add(vm_fuel_requestDTO);
//
//					if( ! mapOfVm_fuel_requestDTOToapproverOfficeNameEn.containsKey(vm_fuel_requestDTO.approverOfficeNameEn)) {
//						mapOfVm_fuel_requestDTOToapproverOfficeNameEn.put(vm_fuel_requestDTO.approverOfficeNameEn, new HashSet<>());
//					}
//					mapOfVm_fuel_requestDTOToapproverOfficeNameEn.get(vm_fuel_requestDTO.approverOfficeNameEn).add(vm_fuel_requestDTO);
//
//					if( ! mapOfVm_fuel_requestDTOToapproverOfficeNameBn.containsKey(vm_fuel_requestDTO.approverOfficeNameBn)) {
//						mapOfVm_fuel_requestDTOToapproverOfficeNameBn.put(vm_fuel_requestDTO.approverOfficeNameBn, new HashSet<>());
//					}
//					mapOfVm_fuel_requestDTOToapproverOfficeNameBn.get(vm_fuel_requestDTO.approverOfficeNameBn).add(vm_fuel_requestDTO);
//
//					if( ! mapOfVm_fuel_requestDTOToapproverOfficeUnitNameEn.containsKey(vm_fuel_requestDTO.approverOfficeUnitNameEn)) {
//						mapOfVm_fuel_requestDTOToapproverOfficeUnitNameEn.put(vm_fuel_requestDTO.approverOfficeUnitNameEn, new HashSet<>());
//					}
//					mapOfVm_fuel_requestDTOToapproverOfficeUnitNameEn.get(vm_fuel_requestDTO.approverOfficeUnitNameEn).add(vm_fuel_requestDTO);
//
//					if( ! mapOfVm_fuel_requestDTOToapproverOfficeUnitNameBn.containsKey(vm_fuel_requestDTO.approverOfficeUnitNameBn)) {
//						mapOfVm_fuel_requestDTOToapproverOfficeUnitNameBn.put(vm_fuel_requestDTO.approverOfficeUnitNameBn, new HashSet<>());
//					}
//					mapOfVm_fuel_requestDTOToapproverOfficeUnitNameBn.get(vm_fuel_requestDTO.approverOfficeUnitNameBn).add(vm_fuel_requestDTO);
//
//					if( ! mapOfVm_fuel_requestDTOToapproverOfficeUnitOrgNameEn.containsKey(vm_fuel_requestDTO.approverOfficeUnitOrgNameEn)) {
//						mapOfVm_fuel_requestDTOToapproverOfficeUnitOrgNameEn.put(vm_fuel_requestDTO.approverOfficeUnitOrgNameEn, new HashSet<>());
//					}
//					mapOfVm_fuel_requestDTOToapproverOfficeUnitOrgNameEn.get(vm_fuel_requestDTO.approverOfficeUnitOrgNameEn).add(vm_fuel_requestDTO);
//
//					if( ! mapOfVm_fuel_requestDTOToapproverOfficeUnitOrgNameBn.containsKey(vm_fuel_requestDTO.approverOfficeUnitOrgNameBn)) {
//						mapOfVm_fuel_requestDTOToapproverOfficeUnitOrgNameBn.put(vm_fuel_requestDTO.approverOfficeUnitOrgNameBn, new HashSet<>());
//					}
//					mapOfVm_fuel_requestDTOToapproverOfficeUnitOrgNameBn.get(vm_fuel_requestDTO.approverOfficeUnitOrgNameBn).add(vm_fuel_requestDTO);
//
//					if( ! mapOfVm_fuel_requestDTOTopaymentReceiverOrgId.containsKey(vm_fuel_requestDTO.paymentReceiverOrgId)) {
//						mapOfVm_fuel_requestDTOTopaymentReceiverOrgId.put(vm_fuel_requestDTO.paymentReceiverOrgId, new HashSet<>());
//					}
//					mapOfVm_fuel_requestDTOTopaymentReceiverOrgId.get(vm_fuel_requestDTO.paymentReceiverOrgId).add(vm_fuel_requestDTO);
//
//					if( ! mapOfVm_fuel_requestDTOTopaymentReceiverOfficeId.containsKey(vm_fuel_requestDTO.paymentReceiverOfficeId)) {
//						mapOfVm_fuel_requestDTOTopaymentReceiverOfficeId.put(vm_fuel_requestDTO.paymentReceiverOfficeId, new HashSet<>());
//					}
//					mapOfVm_fuel_requestDTOTopaymentReceiverOfficeId.get(vm_fuel_requestDTO.paymentReceiverOfficeId).add(vm_fuel_requestDTO);
//
//					if( ! mapOfVm_fuel_requestDTOTopaymentReceiverOfficeUnitId.containsKey(vm_fuel_requestDTO.paymentReceiverOfficeUnitId)) {
//						mapOfVm_fuel_requestDTOTopaymentReceiverOfficeUnitId.put(vm_fuel_requestDTO.paymentReceiverOfficeUnitId, new HashSet<>());
//					}
//					mapOfVm_fuel_requestDTOTopaymentReceiverOfficeUnitId.get(vm_fuel_requestDTO.paymentReceiverOfficeUnitId).add(vm_fuel_requestDTO);
//
//					if( ! mapOfVm_fuel_requestDTOTopaymentReceiverEmpId.containsKey(vm_fuel_requestDTO.paymentReceiverEmpId)) {
//						mapOfVm_fuel_requestDTOTopaymentReceiverEmpId.put(vm_fuel_requestDTO.paymentReceiverEmpId, new HashSet<>());
//					}
//					mapOfVm_fuel_requestDTOTopaymentReceiverEmpId.get(vm_fuel_requestDTO.paymentReceiverEmpId).add(vm_fuel_requestDTO);
//
//					if( ! mapOfVm_fuel_requestDTOTopaymentReceiverPhoneNum.containsKey(vm_fuel_requestDTO.paymentReceiverPhoneNum)) {
//						mapOfVm_fuel_requestDTOTopaymentReceiverPhoneNum.put(vm_fuel_requestDTO.paymentReceiverPhoneNum, new HashSet<>());
//					}
//					mapOfVm_fuel_requestDTOTopaymentReceiverPhoneNum.get(vm_fuel_requestDTO.paymentReceiverPhoneNum).add(vm_fuel_requestDTO);
//
//					if( ! mapOfVm_fuel_requestDTOTopaymentReceiverNameEn.containsKey(vm_fuel_requestDTO.paymentReceiverNameEn)) {
//						mapOfVm_fuel_requestDTOTopaymentReceiverNameEn.put(vm_fuel_requestDTO.paymentReceiverNameEn, new HashSet<>());
//					}
//					mapOfVm_fuel_requestDTOTopaymentReceiverNameEn.get(vm_fuel_requestDTO.paymentReceiverNameEn).add(vm_fuel_requestDTO);
//
//					if( ! mapOfVm_fuel_requestDTOTopaymentReceiverNameBn.containsKey(vm_fuel_requestDTO.paymentReceiverNameBn)) {
//						mapOfVm_fuel_requestDTOTopaymentReceiverNameBn.put(vm_fuel_requestDTO.paymentReceiverNameBn, new HashSet<>());
//					}
//					mapOfVm_fuel_requestDTOTopaymentReceiverNameBn.get(vm_fuel_requestDTO.paymentReceiverNameBn).add(vm_fuel_requestDTO);
//
//					if( ! mapOfVm_fuel_requestDTOTopaymentReceiverOfficeNameEn.containsKey(vm_fuel_requestDTO.paymentReceiverOfficeNameEn)) {
//						mapOfVm_fuel_requestDTOTopaymentReceiverOfficeNameEn.put(vm_fuel_requestDTO.paymentReceiverOfficeNameEn, new HashSet<>());
//					}
//					mapOfVm_fuel_requestDTOTopaymentReceiverOfficeNameEn.get(vm_fuel_requestDTO.paymentReceiverOfficeNameEn).add(vm_fuel_requestDTO);
//
//					if( ! mapOfVm_fuel_requestDTOTopaymentReceiverOfficeNameBn.containsKey(vm_fuel_requestDTO.paymentReceiverOfficeNameBn)) {
//						mapOfVm_fuel_requestDTOTopaymentReceiverOfficeNameBn.put(vm_fuel_requestDTO.paymentReceiverOfficeNameBn, new HashSet<>());
//					}
//					mapOfVm_fuel_requestDTOTopaymentReceiverOfficeNameBn.get(vm_fuel_requestDTO.paymentReceiverOfficeNameBn).add(vm_fuel_requestDTO);
//
//					if( ! mapOfVm_fuel_requestDTOTopaymentReceiverOfficeUnitNameEn.containsKey(vm_fuel_requestDTO.paymentReceiverOfficeUnitNameEn)) {
//						mapOfVm_fuel_requestDTOTopaymentReceiverOfficeUnitNameEn.put(vm_fuel_requestDTO.paymentReceiverOfficeUnitNameEn, new HashSet<>());
//					}
//					mapOfVm_fuel_requestDTOTopaymentReceiverOfficeUnitNameEn.get(vm_fuel_requestDTO.paymentReceiverOfficeUnitNameEn).add(vm_fuel_requestDTO);
//
//					if( ! mapOfVm_fuel_requestDTOTopaymentReceiverOfficeUnitNameBn.containsKey(vm_fuel_requestDTO.paymentReceiverOfficeUnitNameBn)) {
//						mapOfVm_fuel_requestDTOTopaymentReceiverOfficeUnitNameBn.put(vm_fuel_requestDTO.paymentReceiverOfficeUnitNameBn, new HashSet<>());
//					}
//					mapOfVm_fuel_requestDTOTopaymentReceiverOfficeUnitNameBn.get(vm_fuel_requestDTO.paymentReceiverOfficeUnitNameBn).add(vm_fuel_requestDTO);
//
//					if( ! mapOfVm_fuel_requestDTOTopaymentReceiverOfficeUnitOrgNameEn.containsKey(vm_fuel_requestDTO.paymentReceiverOfficeUnitOrgNameEn)) {
//						mapOfVm_fuel_requestDTOTopaymentReceiverOfficeUnitOrgNameEn.put(vm_fuel_requestDTO.paymentReceiverOfficeUnitOrgNameEn, new HashSet<>());
//					}
//					mapOfVm_fuel_requestDTOTopaymentReceiverOfficeUnitOrgNameEn.get(vm_fuel_requestDTO.paymentReceiverOfficeUnitOrgNameEn).add(vm_fuel_requestDTO);
//
//					if( ! mapOfVm_fuel_requestDTOTopaymentReceiverOfficeUnitOrgNameBn.containsKey(vm_fuel_requestDTO.paymentReceiverOfficeUnitOrgNameBn)) {
//						mapOfVm_fuel_requestDTOTopaymentReceiverOfficeUnitOrgNameBn.put(vm_fuel_requestDTO.paymentReceiverOfficeUnitOrgNameBn, new HashSet<>());
//					}
//					mapOfVm_fuel_requestDTOTopaymentReceiverOfficeUnitOrgNameBn.get(vm_fuel_requestDTO.paymentReceiverOfficeUnitOrgNameBn).add(vm_fuel_requestDTO);
//
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}

	public Vm_fuel_requestDTO clone(Vm_fuel_requestDTO dto) {
		String raw = gson.toJson(dto);
		return gson.fromJson(raw, Vm_fuel_requestDTO.class);
	}

	public List<Vm_fuel_requestDTO> clone(List<Vm_fuel_requestDTO> dtoList) {
		return dtoList
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}

	public Vm_fuel_requestDTO getVm_fuel_requestDTOByIDWithoutClone( long ID){
		return mapOfVm_fuel_requestDTOToiD.get(ID);
	}
	
	public Vm_fuel_requestDTO dtoWithChildren(Vm_fuel_requestDTO vm_fuel_requestDTO) {
		if (vm_fuel_requestDTO != null) {
			vm_fuel_requestDTO.vmFuelRequestItemDTOList = VmFuelRequestItemRepository
					.getInstance()
					.getVmFuelRequestItemDTOByvmFuelRequestId(
							vm_fuel_requestDTO.iD
					);
		}
		return vm_fuel_requestDTO;
	}

	public List<Vm_fuel_requestDTO> getVm_fuel_requestList() {
		List <Vm_fuel_requestDTO> vm_fuel_requests = new ArrayList<Vm_fuel_requestDTO>(this.mapOfVm_fuel_requestDTOToiD.values());
		return vm_fuel_requests;
	}
	
	
	public Vm_fuel_requestDTO getVm_fuel_requestDTOByID( long ID){
		return dtoWithChildren(clone(mapOfVm_fuel_requestDTOToiD.get(ID)));
	}
	
	
//	public List<Vm_fuel_requestDTO> getVm_fuel_requestDTOByinserted_by_user_id(long inserted_by_user_id) {
//		return new ArrayList<>( mapOfVm_fuel_requestDTOToinsertedByUserId.getOrDefault(inserted_by_user_id,new HashSet<>()));
//	}
//
//
//	public List<Vm_fuel_requestDTO> getVm_fuel_requestDTOByinserted_by_organogram_id(long inserted_by_organogram_id) {
//		return new ArrayList<>( mapOfVm_fuel_requestDTOToinsertedByOrganogramId.getOrDefault(inserted_by_organogram_id,new HashSet<>()));
//	}
//
//
//	public List<Vm_fuel_requestDTO> getVm_fuel_requestDTOByinsertion_date(long insertion_date) {
//		return new ArrayList<>( mapOfVm_fuel_requestDTOToinsertionDate.getOrDefault(insertion_date,new HashSet<>()));
//	}
//
//
//	public List<Vm_fuel_requestDTO> getVm_fuel_requestDTOBylastModificationTime(long lastModificationTime) {
//		return new ArrayList<>( mapOfVm_fuel_requestDTOTolastModificationTime.getOrDefault(lastModificationTime,new HashSet<>()));
//	}
//
//
//	public List<Vm_fuel_requestDTO> getVm_fuel_requestDTOBysearch_column(String search_column) {
//		return new ArrayList<>( mapOfVm_fuel_requestDTOTosearchColumn.getOrDefault(search_column,new HashSet<>()));
//	}
//
//
//	public List<Vm_fuel_requestDTO> getVm_fuel_requestDTOByvehicle_type(int vehicle_type) {
//		return new ArrayList<>( mapOfVm_fuel_requestDTOTovehicleType.getOrDefault(vehicle_type,new HashSet<>()));
//	}
//
//
	public List<Vm_fuel_requestDTO> getVm_fuel_requestDTOByvehicle_id(long vehicle_id) {
		List<Vm_fuel_requestDTO> vm_fuel_requestDTOS = (new ArrayList<>( mapOfVm_fuel_requestDTOTovehicleId.getOrDefault(vehicle_id,new HashSet<>())));
		return vm_fuel_requestDTOS
				.stream()
				.map(vm_fuel_requestDTO -> clone(vm_fuel_requestDTO))
				.map(vm_fuel_requestDTO -> dtoWithChildren(vm_fuel_requestDTO))
				.collect(Collectors.toList());
	}

	public List<Vm_fuel_requestDTO> getVm_fuel_requestDTOByvehicle_idAndStatus(long vehicle_id, int status) {
		List<Vm_fuel_requestDTO> vm_fuel_requestDTOS = (new ArrayList<>( mapOfVm_fuel_requestDTOTovehicleId.getOrDefault(vehicle_id,new HashSet<>())));
		return vm_fuel_requestDTOS
				.stream().filter(vm_fuel_requestDTO -> vm_fuel_requestDTO.status == status)
				.map(vm_fuel_requestDTO -> clone(vm_fuel_requestDTO))
				.map(vm_fuel_requestDTO -> dtoWithChildren(vm_fuel_requestDTO))
				.collect(Collectors.toList());
	}

	public List<Vm_fuel_requestDTO> getVm_fuel_requestDTOByvehicle_idAndFiscal(long vehicle_id, long firstDate, long secondDate) {
		List<Vm_fuel_requestDTO> vm_fuel_requestDTOS = (new ArrayList<>( mapOfVm_fuel_requestDTOTovehicleId.getOrDefault(vehicle_id,new HashSet<>())));
		return vm_fuel_requestDTOS
				.stream()
				.filter(vm_fuel_requestDTO -> (vm_fuel_requestDTO.applicationDate >= firstDate
						&& vm_fuel_requestDTO.applicationDate <= secondDate
						&& vm_fuel_requestDTO.status != CommonApprovalStatus.PENDING.getValue()
						&& vm_fuel_requestDTO.status != CommonApprovalStatus.CANCELLED.getValue()
						&& vm_fuel_requestDTO.status != CommonApprovalStatus.DISSATISFIED.getValue()))
				.map(vm_fuel_requestDTO -> clone(vm_fuel_requestDTO))
				.map(vm_fuel_requestDTO -> dtoWithChildren(vm_fuel_requestDTO))
				.collect(Collectors.toList());
	}
//
//
//	public List<Vm_fuel_requestDTO> getVm_fuel_requestDTOByapplication_date(long application_date) {
//		return new ArrayList<>( mapOfVm_fuel_requestDTOToapplicationDate.getOrDefault(application_date,new HashSet<>()));
//	}
//
//
//	public List<Vm_fuel_requestDTO> getVm_fuel_requestDTOByvehicle_driver_assignment_id(long vehicle_driver_assignment_id) {
//		return new ArrayList<>( mapOfVm_fuel_requestDTOTovehicleDriverAssignmentId.getOrDefault(vehicle_driver_assignment_id,new HashSet<>()));
//	}
//
//
//	public List<Vm_fuel_requestDTO> getVm_fuel_requestDTOBylast_fuel_date(long last_fuel_date) {
//		return new ArrayList<>( mapOfVm_fuel_requestDTOTolastFuelDate.getOrDefault(last_fuel_date,new HashSet<>()));
//	}
//
//
//	public List<Vm_fuel_requestDTO> getVm_fuel_requestDTOBystatus(int status) {
//		return new ArrayList<>( mapOfVm_fuel_requestDTOTostatus.getOrDefault(status,new HashSet<>()));
//	}
//
//
//	public List<Vm_fuel_requestDTO> getVm_fuel_requestDTOByfiscal_year_id(long fiscal_year_id) {
//		return new ArrayList<>( mapOfVm_fuel_requestDTOTofiscalYearId.getOrDefault(fiscal_year_id,new HashSet<>()));
//	}
//
//
//	public List<Vm_fuel_requestDTO> getVm_fuel_requestDTOByapproved_date(long approved_date) {
//		return new ArrayList<>( mapOfVm_fuel_requestDTOToapprovedDate.getOrDefault(approved_date,new HashSet<>()));
//	}
//
//
//	public List<Vm_fuel_requestDTO> getVm_fuel_requestDTOByvendor_id(long vendor_id) {
//		return new ArrayList<>( mapOfVm_fuel_requestDTOTovendorId.getOrDefault(vendor_id,new HashSet<>()));
//	}
//
//
//	public List<Vm_fuel_requestDTO> getVm_fuel_requestDTOBysarok_number(String sarok_number) {
//		return new ArrayList<>( mapOfVm_fuel_requestDTOTosarokNumber.getOrDefault(sarok_number,new HashSet<>()));
//	}
//
//
//	public List<Vm_fuel_requestDTO> getVm_fuel_requestDTOByfiles_dropzone(long files_dropzone) {
//		return new ArrayList<>( mapOfVm_fuel_requestDTOTofilesDropzone.getOrDefault(files_dropzone,new HashSet<>()));
//	}
//
//
//	public List<Vm_fuel_requestDTO> getVm_fuel_requestDTOBypayment_remarks(String payment_remarks) {
//		return new ArrayList<>( mapOfVm_fuel_requestDTOTopaymentRemarks.getOrDefault(payment_remarks,new HashSet<>()));
//	}
//
//
//	public List<Vm_fuel_requestDTO> getVm_fuel_requestDTOByrequester_org_id(long requester_org_id) {
//		return new ArrayList<>( mapOfVm_fuel_requestDTOTorequesterOrgId.getOrDefault(requester_org_id,new HashSet<>()));
//	}
//
//
//	public List<Vm_fuel_requestDTO> getVm_fuel_requestDTOByrequester_office_id(long requester_office_id) {
//		return new ArrayList<>( mapOfVm_fuel_requestDTOTorequesterOfficeId.getOrDefault(requester_office_id,new HashSet<>()));
//	}
//
//
//	public List<Vm_fuel_requestDTO> getVm_fuel_requestDTOByrequester_office_unit_id(long requester_office_unit_id) {
//		return new ArrayList<>( mapOfVm_fuel_requestDTOTorequesterOfficeUnitId.getOrDefault(requester_office_unit_id,new HashSet<>()));
//	}
//
//
//	public List<Vm_fuel_requestDTO> getVm_fuel_requestDTOByrequester_emp_id(long requester_emp_id) {
//		return new ArrayList<>( mapOfVm_fuel_requestDTOTorequesterEmpId.getOrDefault(requester_emp_id,new HashSet<>()));
//	}
//
//
//	public List<Vm_fuel_requestDTO> getVm_fuel_requestDTOByrequester_phone_num(String requester_phone_num) {
//		return new ArrayList<>( mapOfVm_fuel_requestDTOTorequesterPhoneNum.getOrDefault(requester_phone_num,new HashSet<>()));
//	}
//
//
//	public List<Vm_fuel_requestDTO> getVm_fuel_requestDTOByrequester_name_en(String requester_name_en) {
//		return new ArrayList<>( mapOfVm_fuel_requestDTOTorequesterNameEn.getOrDefault(requester_name_en,new HashSet<>()));
//	}
//
//
//	public List<Vm_fuel_requestDTO> getVm_fuel_requestDTOByrequester_name_bn(String requester_name_bn) {
//		return new ArrayList<>( mapOfVm_fuel_requestDTOTorequesterNameBn.getOrDefault(requester_name_bn,new HashSet<>()));
//	}
//
//
//	public List<Vm_fuel_requestDTO> getVm_fuel_requestDTOByrequester_office_name_en(String requester_office_name_en) {
//		return new ArrayList<>( mapOfVm_fuel_requestDTOTorequesterOfficeNameEn.getOrDefault(requester_office_name_en,new HashSet<>()));
//	}
//
//
//	public List<Vm_fuel_requestDTO> getVm_fuel_requestDTOByrequester_office_name_bn(String requester_office_name_bn) {
//		return new ArrayList<>( mapOfVm_fuel_requestDTOTorequesterOfficeNameBn.getOrDefault(requester_office_name_bn,new HashSet<>()));
//	}
//
//
//	public List<Vm_fuel_requestDTO> getVm_fuel_requestDTOByrequester_office_unit_name_en(String requester_office_unit_name_en) {
//		return new ArrayList<>( mapOfVm_fuel_requestDTOTorequesterOfficeUnitNameEn.getOrDefault(requester_office_unit_name_en,new HashSet<>()));
//	}
//
//
//	public List<Vm_fuel_requestDTO> getVm_fuel_requestDTOByrequester_office_unit_name_bn(String requester_office_unit_name_bn) {
//		return new ArrayList<>( mapOfVm_fuel_requestDTOTorequesterOfficeUnitNameBn.getOrDefault(requester_office_unit_name_bn,new HashSet<>()));
//	}
//
//
//	public List<Vm_fuel_requestDTO> getVm_fuel_requestDTOByrequester_office_unit_org_name_en(String requester_office_unit_org_name_en) {
//		return new ArrayList<>( mapOfVm_fuel_requestDTOTorequesterOfficeUnitOrgNameEn.getOrDefault(requester_office_unit_org_name_en,new HashSet<>()));
//	}
//
//
//	public List<Vm_fuel_requestDTO> getVm_fuel_requestDTOByrequester_office_unit_org_name_bn(String requester_office_unit_org_name_bn) {
//		return new ArrayList<>( mapOfVm_fuel_requestDTOTorequesterOfficeUnitOrgNameBn.getOrDefault(requester_office_unit_org_name_bn,new HashSet<>()));
//	}
//
//
//	public List<Vm_fuel_requestDTO> getVm_fuel_requestDTOByapprover_org_id(long approver_org_id) {
//		return new ArrayList<>( mapOfVm_fuel_requestDTOToapproverOrgId.getOrDefault(approver_org_id,new HashSet<>()));
//	}
//
//
//	public List<Vm_fuel_requestDTO> getVm_fuel_requestDTOByapprover_office_id(long approver_office_id) {
//		return new ArrayList<>( mapOfVm_fuel_requestDTOToapproverOfficeId.getOrDefault(approver_office_id,new HashSet<>()));
//	}
//
//
//	public List<Vm_fuel_requestDTO> getVm_fuel_requestDTOByapprover_office_unit_id(long approver_office_unit_id) {
//		return new ArrayList<>( mapOfVm_fuel_requestDTOToapproverOfficeUnitId.getOrDefault(approver_office_unit_id,new HashSet<>()));
//	}
//
//
//	public List<Vm_fuel_requestDTO> getVm_fuel_requestDTOByapprover_emp_id(long approver_emp_id) {
//		return new ArrayList<>( mapOfVm_fuel_requestDTOToapproverEmpId.getOrDefault(approver_emp_id,new HashSet<>()));
//	}
//
//
//	public List<Vm_fuel_requestDTO> getVm_fuel_requestDTOByapprover_phone_num(String approver_phone_num) {
//		return new ArrayList<>( mapOfVm_fuel_requestDTOToapproverPhoneNum.getOrDefault(approver_phone_num,new HashSet<>()));
//	}
//
//
//	public List<Vm_fuel_requestDTO> getVm_fuel_requestDTOByapprover_name_en(String approver_name_en) {
//		return new ArrayList<>( mapOfVm_fuel_requestDTOToapproverNameEn.getOrDefault(approver_name_en,new HashSet<>()));
//	}
//
//
//	public List<Vm_fuel_requestDTO> getVm_fuel_requestDTOByapprover_name_bn(String approver_name_bn) {
//		return new ArrayList<>( mapOfVm_fuel_requestDTOToapproverNameBn.getOrDefault(approver_name_bn,new HashSet<>()));
//	}
//
//
//	public List<Vm_fuel_requestDTO> getVm_fuel_requestDTOByapprover_office_name_en(String approver_office_name_en) {
//		return new ArrayList<>( mapOfVm_fuel_requestDTOToapproverOfficeNameEn.getOrDefault(approver_office_name_en,new HashSet<>()));
//	}
//
//
//	public List<Vm_fuel_requestDTO> getVm_fuel_requestDTOByapprover_office_name_bn(String approver_office_name_bn) {
//		return new ArrayList<>( mapOfVm_fuel_requestDTOToapproverOfficeNameBn.getOrDefault(approver_office_name_bn,new HashSet<>()));
//	}
//
//
//	public List<Vm_fuel_requestDTO> getVm_fuel_requestDTOByapprover_office_unit_name_en(String approver_office_unit_name_en) {
//		return new ArrayList<>( mapOfVm_fuel_requestDTOToapproverOfficeUnitNameEn.getOrDefault(approver_office_unit_name_en,new HashSet<>()));
//	}
//
//
//	public List<Vm_fuel_requestDTO> getVm_fuel_requestDTOByapprover_office_unit_name_bn(String approver_office_unit_name_bn) {
//		return new ArrayList<>( mapOfVm_fuel_requestDTOToapproverOfficeUnitNameBn.getOrDefault(approver_office_unit_name_bn,new HashSet<>()));
//	}
//
//
//	public List<Vm_fuel_requestDTO> getVm_fuel_requestDTOByapprover_office_unit_org_name_en(String approver_office_unit_org_name_en) {
//		return new ArrayList<>( mapOfVm_fuel_requestDTOToapproverOfficeUnitOrgNameEn.getOrDefault(approver_office_unit_org_name_en,new HashSet<>()));
//	}
//
//
//	public List<Vm_fuel_requestDTO> getVm_fuel_requestDTOByapprover_office_unit_org_name_bn(String approver_office_unit_org_name_bn) {
//		return new ArrayList<>( mapOfVm_fuel_requestDTOToapproverOfficeUnitOrgNameBn.getOrDefault(approver_office_unit_org_name_bn,new HashSet<>()));
//	}
//
//
//	public List<Vm_fuel_requestDTO> getVm_fuel_requestDTOBypayment_receiver_org_id(long payment_receiver_org_id) {
//		return new ArrayList<>( mapOfVm_fuel_requestDTOTopaymentReceiverOrgId.getOrDefault(payment_receiver_org_id,new HashSet<>()));
//	}
//
//
//	public List<Vm_fuel_requestDTO> getVm_fuel_requestDTOBypayment_receiver_office_id(long payment_receiver_office_id) {
//		return new ArrayList<>( mapOfVm_fuel_requestDTOTopaymentReceiverOfficeId.getOrDefault(payment_receiver_office_id,new HashSet<>()));
//	}
//
//
//	public List<Vm_fuel_requestDTO> getVm_fuel_requestDTOBypayment_receiver_office_unit_id(long payment_receiver_office_unit_id) {
//		return new ArrayList<>( mapOfVm_fuel_requestDTOTopaymentReceiverOfficeUnitId.getOrDefault(payment_receiver_office_unit_id,new HashSet<>()));
//	}
//
//
//	public List<Vm_fuel_requestDTO> getVm_fuel_requestDTOBypayment_receiver_emp_id(long payment_receiver_emp_id) {
//		return new ArrayList<>( mapOfVm_fuel_requestDTOTopaymentReceiverEmpId.getOrDefault(payment_receiver_emp_id,new HashSet<>()));
//	}
//
//
//	public List<Vm_fuel_requestDTO> getVm_fuel_requestDTOBypayment_receiver_phone_num(String payment_receiver_phone_num) {
//		return new ArrayList<>( mapOfVm_fuel_requestDTOTopaymentReceiverPhoneNum.getOrDefault(payment_receiver_phone_num,new HashSet<>()));
//	}
//
//
//	public List<Vm_fuel_requestDTO> getVm_fuel_requestDTOBypayment_receiver_name_en(String payment_receiver_name_en) {
//		return new ArrayList<>( mapOfVm_fuel_requestDTOTopaymentReceiverNameEn.getOrDefault(payment_receiver_name_en,new HashSet<>()));
//	}
//
//
//	public List<Vm_fuel_requestDTO> getVm_fuel_requestDTOBypayment_receiver_name_bn(String payment_receiver_name_bn) {
//		return new ArrayList<>( mapOfVm_fuel_requestDTOTopaymentReceiverNameBn.getOrDefault(payment_receiver_name_bn,new HashSet<>()));
//	}
//
//
//	public List<Vm_fuel_requestDTO> getVm_fuel_requestDTOBypayment_receiver_office_name_en(String payment_receiver_office_name_en) {
//		return new ArrayList<>( mapOfVm_fuel_requestDTOTopaymentReceiverOfficeNameEn.getOrDefault(payment_receiver_office_name_en,new HashSet<>()));
//	}
//
//
//	public List<Vm_fuel_requestDTO> getVm_fuel_requestDTOBypayment_receiver_office_name_bn(String payment_receiver_office_name_bn) {
//		return new ArrayList<>( mapOfVm_fuel_requestDTOTopaymentReceiverOfficeNameBn.getOrDefault(payment_receiver_office_name_bn,new HashSet<>()));
//	}
//
//
//	public List<Vm_fuel_requestDTO> getVm_fuel_requestDTOBypayment_receiver_office_unit_name_en(String payment_receiver_office_unit_name_en) {
//		return new ArrayList<>( mapOfVm_fuel_requestDTOTopaymentReceiverOfficeUnitNameEn.getOrDefault(payment_receiver_office_unit_name_en,new HashSet<>()));
//	}
//
//
//	public List<Vm_fuel_requestDTO> getVm_fuel_requestDTOBypayment_receiver_office_unit_name_bn(String payment_receiver_office_unit_name_bn) {
//		return new ArrayList<>( mapOfVm_fuel_requestDTOTopaymentReceiverOfficeUnitNameBn.getOrDefault(payment_receiver_office_unit_name_bn,new HashSet<>()));
//	}
//
//
//	public List<Vm_fuel_requestDTO> getVm_fuel_requestDTOBypayment_receiver_office_unit_org_name_en(String payment_receiver_office_unit_org_name_en) {
//		return new ArrayList<>( mapOfVm_fuel_requestDTOTopaymentReceiverOfficeUnitOrgNameEn.getOrDefault(payment_receiver_office_unit_org_name_en,new HashSet<>()));
//	}
//
//
//	public List<Vm_fuel_requestDTO> getVm_fuel_requestDTOBypayment_receiver_office_unit_org_name_bn(String payment_receiver_office_unit_org_name_bn) {
//		return new ArrayList<>( mapOfVm_fuel_requestDTOTopaymentReceiverOfficeUnitOrgNameBn.getOrDefault(payment_receiver_office_unit_org_name_bn,new HashSet<>()));
//	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "vm_fuel_request";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


