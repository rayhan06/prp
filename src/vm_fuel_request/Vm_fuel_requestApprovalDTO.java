package vm_fuel_request;

/*
 * @author Md. Erfan Hossain
 * @created 28/04/2021 - 12:02 PM
 * @project parliament
 */

import util.CommonDTO;

public class Vm_fuel_requestApprovalDTO extends CommonDTO {
    public long employeeRecordId;
    public String nameEng;
    public String nameBng;
    public String userName;
    public long officeUnitId;
    public String officeUnitEng;
    public String officeUnitBng;
    public long organogramId;
    public String organogramEng;
    public String organogramBng;
    public long insertBy;
    public long insertionTime;
    public long modifiedBy;
}
