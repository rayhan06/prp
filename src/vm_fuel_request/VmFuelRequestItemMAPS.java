package vm_fuel_request;
import java.util.*; 
import util.*;


public class VmFuelRequestItemMAPS extends CommonMaps
{	
	public VmFuelRequestItemMAPS(String tableName)
	{
		

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("insertedByUserId".toLowerCase(), "insertedByUserId".toLowerCase());
		java_DTO_map.put("insertedByOrganogramId".toLowerCase(), "insertedByOrganogramId".toLowerCase());
		java_DTO_map.put("insertionDate".toLowerCase(), "insertionDate".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
		java_DTO_map.put("searchColumn".toLowerCase(), "searchColumn".toLowerCase());
		java_DTO_map.put("vmFuelRequestId".toLowerCase(), "vmFuelRequestId".toLowerCase());
		java_DTO_map.put("vehicleFuelCat".toLowerCase(), "vehicleFuelCat".toLowerCase());
		java_DTO_map.put("requestedAmount".toLowerCase(), "requestedAmount".toLowerCase());
		java_DTO_map.put("lastFuelAmount".toLowerCase(), "lastFuelAmount".toLowerCase());
		java_DTO_map.put("lastFuelDate".toLowerCase(), "lastFuelDate".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());

		java_SQL_map.put("vm_fuel_request_id".toLowerCase(), "vmFuelRequestId".toLowerCase());
		java_SQL_map.put("vehicle_fuel_cat".toLowerCase(), "vehicleFuelCat".toLowerCase());
		java_SQL_map.put("requested_amount".toLowerCase(), "requestedAmount".toLowerCase());
		java_SQL_map.put("last_fuel_amount".toLowerCase(), "lastFuelAmount".toLowerCase());
		java_SQL_map.put("last_fuel_date".toLowerCase(), "lastFuelDate".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Inserted By User Id".toLowerCase(), "insertedByUserId".toLowerCase());
		java_Text_map.put("Inserted By Organogram Id".toLowerCase(), "insertedByOrganogramId".toLowerCase());
		java_Text_map.put("Insertion Date".toLowerCase(), "insertionDate".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
		java_Text_map.put("Search Column".toLowerCase(), "searchColumn".toLowerCase());
		java_Text_map.put("Vm Fuel Request Id".toLowerCase(), "vmFuelRequestId".toLowerCase());
		java_Text_map.put("Vehicle Fuel".toLowerCase(), "vehicleFuelCat".toLowerCase());
		java_Text_map.put("Requested Amount".toLowerCase(), "requestedAmount".toLowerCase());
		java_Text_map.put("Last Fuel Amount".toLowerCase(), "lastFuelAmount".toLowerCase());
		java_Text_map.put("Last Fuel Date".toLowerCase(), "lastFuelDate".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
			
	}

}