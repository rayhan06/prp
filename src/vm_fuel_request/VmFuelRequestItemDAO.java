package vm_fuel_request;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;

import common.ConnectionAndStatementUtil;
import procurement_package.ProcurementGoodsTypeDTO;
import sessionmanager.SessionConstants;
import java.sql.SQLException;


import org.apache.log4j.Logger;

import dbm.*;

import repository.RepositoryManager;

import util.*;
import pb.*;
import user.UserDTO;

public class VmFuelRequestItemDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());
	

	
	public VmFuelRequestItemDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";
		commonMaps = new VmFuelRequestItemMAPS(tableName);
		columnNames = new String[] 
		{
			"ID",
			"inserted_by_user_id",
			"inserted_by_organogram_id",
			"insertion_date",
			"search_column",
			"vm_fuel_request_id",
			"vehicle_fuel_cat",
			"requested_amount",
			"last_fuel_amount",
			"last_fuel_date",
			"isDeleted",
			"lastModificationTime"
		};
	}
	
	public VmFuelRequestItemDAO()
	{
		this("vm_fuel_request_item");		
	}
	
	public void setSearchColumn(VmFuelRequestItemDTO vmfuelrequestitemDTO)
	{
		vmfuelrequestitemDTO.searchColumn = "";
		vmfuelrequestitemDTO.searchColumn += CatDAO.getName("English", "vehicle_fuel", vmfuelrequestitemDTO.vehicleFuelCat) + " " + CatDAO.getName("Bangla", "vehicle_fuel", vmfuelrequestitemDTO.vehicleFuelCat) + " ";
		vmfuelrequestitemDTO.searchColumn += vmfuelrequestitemDTO.requestedAmount + " ";
		vmfuelrequestitemDTO.searchColumn += vmfuelrequestitemDTO.lastFuelAmount + " ";
		
	}
	
	public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException
	{
		VmFuelRequestItemDTO vmfuelrequestitemDTO = (VmFuelRequestItemDTO)commonDTO;
		int index = 1;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(vmfuelrequestitemDTO);
		if(isInsert)
		{
			ps.setObject(index++,vmfuelrequestitemDTO.iD);
		}
		ps.setObject(index++,vmfuelrequestitemDTO.insertedByUserId);
		ps.setObject(index++,vmfuelrequestitemDTO.insertedByOrganogramId);
		ps.setObject(index++,vmfuelrequestitemDTO.insertionDate);
		ps.setObject(index++,vmfuelrequestitemDTO.searchColumn);
		ps.setObject(index++,vmfuelrequestitemDTO.vmFuelRequestId);
		ps.setObject(index++,vmfuelrequestitemDTO.vehicleFuelCat);
		ps.setObject(index++,vmfuelrequestitemDTO.requestedAmount);
		ps.setObject(index++,vmfuelrequestitemDTO.lastFuelAmount);
		ps.setObject(index++,vmfuelrequestitemDTO.lastFuelDate);
		if(isInsert)
		{
			ps.setObject(index++, 0);
			ps.setObject(index++, lastModificationTime);
		}
	}
	
	public void get(VmFuelRequestItemDTO vmfuelrequestitemDTO, ResultSet rs) throws SQLException
	{
		vmfuelrequestitemDTO.iD = rs.getLong("ID");
		vmfuelrequestitemDTO.insertedByUserId = rs.getLong("inserted_by_user_id");
		vmfuelrequestitemDTO.insertedByOrganogramId = rs.getLong("inserted_by_organogram_id");
		vmfuelrequestitemDTO.insertionDate = rs.getLong("insertion_date");
		vmfuelrequestitemDTO.lastModificationTime = rs.getLong("lastModificationTime");
		vmfuelrequestitemDTO.searchColumn = rs.getString("search_column");
		vmfuelrequestitemDTO.vmFuelRequestId = rs.getLong("vm_fuel_request_id");
		vmfuelrequestitemDTO.vehicleFuelCat = rs.getInt("vehicle_fuel_cat");
		vmfuelrequestitemDTO.requestedAmount = rs.getLong("requested_amount");
		vmfuelrequestitemDTO.lastFuelAmount = rs.getLong("last_fuel_amount");
		vmfuelrequestitemDTO.lastFuelDate = rs.getLong("last_fuel_date");
		vmfuelrequestitemDTO.isDeleted = rs.getInt("isDeleted");
	}


	public VmFuelRequestItemDTO build(ResultSet rs)
	{
		try
		{
			VmFuelRequestItemDTO vmfuelrequestitemDTO = new VmFuelRequestItemDTO();
			vmfuelrequestitemDTO.iD = rs.getLong("ID");
			vmfuelrequestitemDTO.insertedByUserId = rs.getLong("inserted_by_user_id");
			vmfuelrequestitemDTO.insertedByOrganogramId = rs.getLong("inserted_by_organogram_id");
			vmfuelrequestitemDTO.insertionDate = rs.getLong("insertion_date");
			vmfuelrequestitemDTO.lastModificationTime = rs.getLong("lastModificationTime");
			vmfuelrequestitemDTO.searchColumn = rs.getString("search_column");
			vmfuelrequestitemDTO.vmFuelRequestId = rs.getLong("vm_fuel_request_id");
			vmfuelrequestitemDTO.vehicleFuelCat = rs.getInt("vehicle_fuel_cat");
			vmfuelrequestitemDTO.requestedAmount = rs.getLong("requested_amount");
			vmfuelrequestitemDTO.lastFuelAmount = rs.getLong("last_fuel_amount");
			vmfuelrequestitemDTO.lastFuelDate = rs.getLong("last_fuel_date");
			vmfuelrequestitemDTO.isDeleted = rs.getInt("isDeleted");
			return vmfuelrequestitemDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}


	public VmFuelRequestItemDTO buildSQL(ResultSet rs)
	{
		try
		{
			VmFuelRequestItemDTO vmfuelrequestitemDTO = new VmFuelRequestItemDTO();
			vmfuelrequestitemDTO.lastModificationTime = rs.getLong("application_date");
			vmfuelrequestitemDTO.vmFuelRequestId = rs.getLong("vm_fuel_request_id");
			vmfuelrequestitemDTO.vehicleFuelCat = rs.getInt("vehicle_fuel_cat");
			vmfuelrequestitemDTO.requestedAmount = rs.getLong("requested_amount");
			return vmfuelrequestitemDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}

	
	public long deleteVmFuelRequestItemByVmFuelRequestID(long vmFuelRequestID) throws Exception
	{

		long lastModificationTime = System.currentTimeMillis();
		StringBuilder sqlBuilder = new StringBuilder("UPDATE ")
				.append(tableName)
				.append(" SET isDeleted=1,lastModificationTime=")
				.append(lastModificationTime)
				.append(" WHERE vm_fuel_request_id = ")
				.append(vmFuelRequestID);
		ConnectionAndStatementUtil.getWriteConnectionAndStatement(model -> {
			String sql = sqlBuilder.toString();
			Connection connection = model.getConnection();
			Statement stmt = model.getStatement();
			try {
				logger.debug(sql);
				stmt.execute(sql);
				recordUpdateTime(connection, lastModificationTime);
			} catch (SQLException ex) {
				logger.error(ex);
			}
		});
		return vmFuelRequestID;
	}		
   
	public List<VmFuelRequestItemDTO> getVmFuelRequestItemDTOListByVmFuelRequestID(long vmFuelRequestID) throws Exception{

		String sql = "SELECT * FROM vm_fuel_request_item where isDeleted=0 and vm_fuel_request_id="+vmFuelRequestID+" order by vm_fuel_request_item.lastModificationTime";

		return ConnectionAndStatementUtil.getListOfT(sql,this::build);
	}

	//need another getter for repository
	public VmFuelRequestItemDTO getDTOByID (long ID)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID = ? ";
		VmFuelRequestItemDTO vmFuelRequestItemDTO =
				ConnectionAndStatementUtil.getT(sql, Arrays.asList(ID), this::build);
		return vmFuelRequestItemDTO;
	}
	
	
	
	
	public List<VmFuelRequestItemDTO> getDTOs(Collection recordIDs)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID IN ( ";
		for(int i = 0;i<recordIDs.size();i++){
			if(i!=0){
				sql+=",";
			}
			sql+=((ArrayList)recordIDs).get(i);
		}
		sql+=")  order by lastModificationTime desc";
		printSql(sql);
		return ConnectionAndStatementUtil.getListOfT(sql, Arrays.asList(recordIDs.toArray()),this::build);
	}
	
	

	
	
	
	//add repository
	public List<VmFuelRequestItemDTO> getAllVmFuelRequestItem (boolean isFirstReload)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ";
		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;
		}
		sql += " order by " + tableName + ".lastModificationTime desc";
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);
	}

	
	public List<VmFuelRequestItemDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO)
	{
		return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
	}

	
	public List<VmFuelRequestItemDTO> getDTOs(Hashtable  p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
			String filter, boolean tableHasJobCat)
	{
		List<Object> objectList = new ArrayList<Object>();

		String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat, objectList);

		return ConnectionAndStatementUtil.getListOfT(sql,objectList,this::build);
	}
	public String getSqlWithSearchCriteriaForNormalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
														  UserDTO userDTO, String filter, boolean tableHasJobCat, List<Object> objectList)
	{
		boolean viewAll = false;

		if(p_searchCriteria != null && p_searchCriteria.get("ViewAll") != null)
		{
			System.out.println("ViewAll = " + p_searchCriteria.get("ViewAll"));
			viewAll = true;
		}
		else
		{
			System.out.println("ViewAll is null ");
		}

		String sql = "SELECT ";
		if(category == GETIDS)
		{
			sql += " " + tableName + ".ID as ID ";
		}
		else if(category == GETCOUNT)
		{
			sql += " count( " + tableName + ".ID) as countID ";
		}
		else if(category == GETDTOS)
		{
			sql += " distinct " + tableName + ".* ";
		}
		sql += "FROM " + tableName + " ";
		sql += joinSQL;



		String AnyfieldSql = "";
		String AllFieldSql = "";

		if(p_searchCriteria != null)
		{


			Enumeration names = p_searchCriteria.keys();
			String str, value;

			AnyfieldSql = "(";

			if(p_searchCriteria.get("AnyField")!= null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase(""))
			{
				AnyfieldSql+= tableName + ".search_column like ?";
				objectList.add("%" + p_searchCriteria.get("AnyField").toString() + "%");
			}
			AnyfieldSql += ")";
			System.out.println("AnyfieldSql = " + AnyfieldSql);

			AllFieldSql = "(";
			int i = 0;
			while(names.hasMoreElements())
			{
				str = (String) names.nextElement();
				value = (String)p_searchCriteria.get(str);
				System.out.println("vm req search: "+str + ": " + value);
				if(value != null && !value.equalsIgnoreCase("") && (
						str.equals("insertion_date_start")
								|| str.equals("insertion_date_end")
								|| str.equals("start_date_start")
								|| str.equals("start_date_end")
								|| str.equals("end_date_start")
								|| str.equals("end_date_end")
								|| str.equals("vehicle_requisition_purpose_cat")
								|| str.equals("vehicle_type_cat")
								|| str.equals("status")
								|| str.equals("trip_description")
								|| str.equals("decision_on")
								|| str.equals("decision_description")
								|| str.equals("given_vehicle_type")
								|| str.equals("receive_date_start")
								|| str.equals("receive_date_end")
								|| str.equals("payment_type")
								|| str.equals("requester_phone_num")
								|| str.equals("decision_by_phone_num")
								|| str.equals("driver_phone_num")
								|| str.equals("requester_name_en")
								|| str.equals("decision_by_name_en")
								|| str.equals("driver_name_en")
								|| str.equals("requester_name_bn")
								|| str.equals("decision_by_name_bn")
								|| str.equals("driver_name_bn")
								|| str.equals("requester_office_name_en")
								|| str.equals("decision_by_office_name_en")
								|| str.equals("driver_office_name_en")
								|| str.equals("requester_office_name_bn")
								|| str.equals("decision_by_office_name_bn")
								|| str.equals("driver_office_name_bn")
								|| str.equals("requester_office_unit_name_en")
								|| str.equals("decision_by_office_unit_name_en")
								|| str.equals("driver_office_unit_name_en")
								|| str.equals("requester_office_unit_name_bn")
								|| str.equals("decision_by_office_unit_name_bn")
								|| str.equals("driver_office_unit_name_bn")
								|| str.equals("requester_office_unit_org_name_en")
								|| str.equals("decision_by_office_unit_org_name_en")
								|| str.equals("driver_office_unit_org_name_en")
								|| str.equals("requester_office_unit_org_name_bn")
								|| str.equals("decision_by_office_unit_org_name_bn")
								|| str.equals("driver_office_unit_org_name_bn")
				)

				)
				{
					if(p_searchCriteria.get(str).equals("any"))
					{
						continue;
					}

					if( i > 0)
					{
						AllFieldSql+= " AND  ";
					}

					if(str.equals("insertion_date_start"))
					{
						AllFieldSql += "" + tableName + ".insertion_date >= " + "?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
					else if(str.equals("insertion_date_end"))
					{
						AllFieldSql += "" + tableName + ".insertion_date <= " + "?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
					else if(str.equals("start_date_start"))
					{
						AllFieldSql += "" + tableName + ".start_date >= " + "?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
					else if(str.equals("start_date_end"))
					{
						AllFieldSql += "" + tableName + ".start_date <= " + "?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
					else if(str.equals("end_date_start"))
					{
						AllFieldSql += "" + tableName + ".end_date >= " + "?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
					else if(str.equals("end_date_end"))
					{
						AllFieldSql += "" + tableName + ".end_date <= " + "?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
					else if(str.equals("vehicle_requisition_purpose_cat"))
					{
						AllFieldSql += "" + tableName + ".vehicle_requisition_purpose_cat = " + "?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
					else if(str.equals("vehicle_type_cat"))
					{
						AllFieldSql += "" + tableName + ".vehicle_type_cat = " + "?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
					else if(str.equals("status"))
					{
						AllFieldSql += "" + tableName + ".status = " + "?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
					else if(str.equals("trip_description"))
					{
						AllFieldSql += "" + tableName + ".trip_description like " + "?";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("decision_on"))
					{
						AllFieldSql += "" + tableName + ".decision_on like " + "?";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("decision_description"))
					{
						AllFieldSql += "" + tableName + ".decision_description like " + "?";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("given_vehicle_type"))
					{
						AllFieldSql += "" + tableName + ".given_vehicle_type = " + "?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
					else if(str.equals("receive_date_start"))
					{
						AllFieldSql += "" + tableName + ".receive_date >= " + "?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
					else if(str.equals("receive_date_end"))
					{
						AllFieldSql += "" + tableName + ".receive_date <= " + "?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
					else if(str.equals("payment_type"))
					{
						AllFieldSql += "" + tableName + ".payment_type = " + "?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
					else if(str.equals("requester_phone_num"))
					{
						AllFieldSql += "" + tableName + ".requester_phone_num like " + "?";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("decision_by_phone_num"))
					{
						AllFieldSql += "" + tableName + ".decision_by_phone_num like " + "?";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("driver_phone_num"))
					{
						AllFieldSql += "" + tableName + ".driver_phone_num like " + "?";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("requester_name_en"))
					{
						AllFieldSql += "" + tableName + ".requester_name_en like " + "?";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("decision_by_name_en"))
					{
						AllFieldSql += "" + tableName + ".decision_by_name_en like " + "?";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("driver_name_en"))
					{
						AllFieldSql += "" + tableName + ".driver_name_en like " + "?";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("requester_name_bn"))
					{
						AllFieldSql += "" + tableName + ".requester_name_bn like " + "?";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("decision_by_name_bn"))
					{
						AllFieldSql += "" + tableName + ".decision_by_name_bn like " + "?";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("driver_name_bn"))
					{
						AllFieldSql += "" + tableName + ".driver_name_bn like " + "?";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("requester_office_name_en"))
					{
						AllFieldSql += "" + tableName + ".requester_office_name_en like " + "?";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("decision_by_office_name_en"))
					{
						AllFieldSql += "" + tableName + ".decision_by_office_name_en like " + "?";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("driver_office_name_en"))
					{
						AllFieldSql += "" + tableName + ".driver_office_name_en like " + "?";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("requester_office_name_bn"))
					{
						AllFieldSql += "" + tableName + ".requester_office_name_bn like " + "?";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("decision_by_office_name_bn"))
					{
						AllFieldSql += "" + tableName + ".decision_by_office_name_bn like " + "?";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("driver_office_name_bn"))
					{
						AllFieldSql += "" + tableName + ".driver_office_name_bn like " + "?";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("requester_office_unit_name_en"))
					{
						AllFieldSql += "" + tableName + ".requester_office_unit_name_en like " + "?";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("decision_by_office_unit_name_en"))
					{
						AllFieldSql += "" + tableName + ".decision_by_office_unit_name_en like " + "?";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("driver_office_unit_name_en"))
					{
						AllFieldSql += "" + tableName + ".driver_office_unit_name_en like " + "?";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("requester_office_unit_name_bn"))
					{
						AllFieldSql += "" + tableName + ".requester_office_unit_name_bn like " + "?";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("decision_by_office_unit_name_bn"))
					{
						AllFieldSql += "" + tableName + ".decision_by_office_unit_name_bn like " + "?";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("driver_office_unit_name_bn"))
					{
						AllFieldSql += "" + tableName + ".driver_office_unit_name_bn like " + "?";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("requester_office_unit_org_name_en"))
					{
						AllFieldSql += "" + tableName + ".requester_office_unit_org_name_en like " + "?";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("decision_by_office_unit_org_name_en"))
					{
						AllFieldSql += "" + tableName + ".decision_by_office_unit_org_name_en like " + "?";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("driver_office_unit_org_name_en"))
					{
						AllFieldSql += "" + tableName + ".driver_office_unit_org_name_en like " + "?";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("requester_office_unit_org_name_bn"))
					{
						AllFieldSql += "" + tableName + ".requester_office_unit_org_name_bn like " + "?";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("decision_by_office_unit_org_name_bn"))
					{
						AllFieldSql += "" + tableName + ".decision_by_office_unit_org_name_bn like " + "?";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("driver_office_unit_org_name_bn"))
					{
						AllFieldSql += "" + tableName + ".driver_office_unit_org_name_bn like " + "?";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}


				}
			}

			AllFieldSql += ")";
			System.out.println("AllFieldSql = " + AllFieldSql);


		}


		sql += " WHERE ";

		sql += " (" + tableName + ".isDeleted = 0 ";
		sql += ")";


		if(!filter.equalsIgnoreCase(""))
		{
			sql += " and " + filter + " ";
		}

		if(!AnyfieldSql.equals("()") && !AnyfieldSql.equals(""))
		{
			sql += " AND " + AnyfieldSql;

		}
		if(!AllFieldSql.equals("()") && !AllFieldSql.equals(""))
		{
			sql += " AND " + AllFieldSql;
		}



		sql += " order by " + tableName + ".lastModificationTime desc ";

		printSql(sql);

		if(limit >= 0)
		{
			sql += " limit " + limit;
		}
		if(offset >= 0)
		{
			sql += " offset " + offset;
		}

		System.out.println("-------------- sql = " + sql);

		return sql;
	}

	public List<VmFuelRequestItemDTO> getDTOSBySQL(String sql)
	{
		return ConnectionAndStatementUtil.getListOfT(sql,this::buildSQL);
	}

}
	