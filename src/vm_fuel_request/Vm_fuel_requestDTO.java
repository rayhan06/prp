package vm_fuel_request;
import java.util.*; 
import util.*; 


public class Vm_fuel_requestDTO extends CommonDTO
{

	public long insertedByUserId = -1;
	public long insertedByOrganogramId = -1;
	public long insertionDate = -1;
	public int vehicleType = -1;
	public long vehicleId = -1;
	public long applicationDate = System.currentTimeMillis();
	public long vehicleDriverAssignmentId = -1;
	public long lastFuelDate = System.currentTimeMillis();
	public int status = 1;
	public long fiscalYearId = -1;
	public long approvedDate = System.currentTimeMillis();
	public long vendorId = -1;
    public String sarokNumber = "";
	public long filesDropzone = -1;
    public String paymentRemarks = "";
	public long requesterOrgId = -1;
	public long requesterOfficeId = -1;
	public long requesterOfficeUnitId = -1;
	public long requesterEmpId = -1;
    public String requesterPhoneNum = "";
    public String requesterNameEn = "";
    public String requesterNameBn = "";
    public String requesterOfficeNameEn = "";
    public String requesterOfficeNameBn = "";
    public String requesterOfficeUnitNameEn = "";
    public String requesterOfficeUnitNameBn = "";
    public String requesterOfficeUnitOrgNameEn = "";
    public String requesterOfficeUnitOrgNameBn = "";
	public long approverOrgId = -1;
	public long approverOfficeId = -1;
	public long approverOfficeUnitId = -1;
	public long approverEmpId = -1;
    public String approverPhoneNum = "";
    public String approverNameEn = "";
    public String approverNameBn = "";
    public String approverOfficeNameEn = "";
    public String approverOfficeNameBn = "";
    public String approverOfficeUnitNameEn = "";
    public String approverOfficeUnitNameBn = "";
    public String approverOfficeUnitOrgNameEn = "";
    public String approverOfficeUnitOrgNameBn = "";
	public long paymentReceiverOrgId = -1;
	public long paymentReceiverOfficeId = -1;
	public long paymentReceiverOfficeUnitId = -1;
	public long paymentReceiverEmpId = -1;
    public String paymentReceiverPhoneNum = "";
    public String paymentReceiverNameEn = "";
    public String paymentReceiverNameBn = "";
    public String paymentReceiverOfficeNameEn = "";
    public String paymentReceiverOfficeNameBn = "";
    public String paymentReceiverOfficeUnitNameEn = "";
    public String paymentReceiverOfficeUnitNameBn = "";
    public String paymentReceiverOfficeUnitOrgNameEn = "";
    public String paymentReceiverOfficeUnitOrgNameBn = "";
	
	public List<VmFuelRequestItemDTO> vmFuelRequestItemDTOList = new ArrayList<>();
	
    @Override
	public String toString() {
            return "$Vm_fuel_requestDTO[" +
            " iD = " + iD +
            " insertedByUserId = " + insertedByUserId +
            " insertedByOrganogramId = " + insertedByOrganogramId +
            " insertionDate = " + insertionDate +
            " lastModificationTime = " + lastModificationTime +
            " searchColumn = " + searchColumn +
            " vehicleType = " + vehicleType +
            " vehicleId = " + vehicleId +
            " applicationDate = " + applicationDate +
            " vehicleDriverAssignmentId = " + vehicleDriverAssignmentId +
            " lastFuelDate = " + lastFuelDate +
            " status = " + status +
            " fiscalYearId = " + fiscalYearId +
            " approvedDate = " + approvedDate +
            " vendorId = " + vendorId +
            " sarokNumber = " + sarokNumber +
            " filesDropzone = " + filesDropzone +
            " paymentRemarks = " + paymentRemarks +
            " requesterOrgId = " + requesterOrgId +
            " requesterOfficeId = " + requesterOfficeId +
            " requesterOfficeUnitId = " + requesterOfficeUnitId +
            " requesterEmpId = " + requesterEmpId +
            " requesterPhoneNum = " + requesterPhoneNum +
            " requesterNameEn = " + requesterNameEn +
            " requesterNameBn = " + requesterNameBn +
            " requesterOfficeNameEn = " + requesterOfficeNameEn +
            " requesterOfficeNameBn = " + requesterOfficeNameBn +
            " requesterOfficeUnitNameEn = " + requesterOfficeUnitNameEn +
            " requesterOfficeUnitNameBn = " + requesterOfficeUnitNameBn +
            " requesterOfficeUnitOrgNameEn = " + requesterOfficeUnitOrgNameEn +
            " requesterOfficeUnitOrgNameBn = " + requesterOfficeUnitOrgNameBn +
            " approverOrgId = " + approverOrgId +
            " approverOfficeId = " + approverOfficeId +
            " approverOfficeUnitId = " + approverOfficeUnitId +
            " approverEmpId = " + approverEmpId +
            " approverPhoneNum = " + approverPhoneNum +
            " approverNameEn = " + approverNameEn +
            " approverNameBn = " + approverNameBn +
            " approverOfficeNameEn = " + approverOfficeNameEn +
            " approverOfficeNameBn = " + approverOfficeNameBn +
            " approverOfficeUnitNameEn = " + approverOfficeUnitNameEn +
            " approverOfficeUnitNameBn = " + approverOfficeUnitNameBn +
            " approverOfficeUnitOrgNameEn = " + approverOfficeUnitOrgNameEn +
            " approverOfficeUnitOrgNameBn = " + approverOfficeUnitOrgNameBn +
            " paymentReceiverOrgId = " + paymentReceiverOrgId +
            " paymentReceiverOfficeId = " + paymentReceiverOfficeId +
            " paymentReceiverOfficeUnitId = " + paymentReceiverOfficeUnitId +
            " paymentReceiverEmpId = " + paymentReceiverEmpId +
            " paymentReceiverPhoneNum = " + paymentReceiverPhoneNum +
            " paymentReceiverNameEn = " + paymentReceiverNameEn +
            " paymentReceiverNameBn = " + paymentReceiverNameBn +
            " paymentReceiverOfficeNameEn = " + paymentReceiverOfficeNameEn +
            " paymentReceiverOfficeNameBn = " + paymentReceiverOfficeNameBn +
            " paymentReceiverOfficeUnitNameEn = " + paymentReceiverOfficeUnitNameEn +
            " paymentReceiverOfficeUnitNameBn = " + paymentReceiverOfficeUnitNameBn +
            " paymentReceiverOfficeUnitOrgNameEn = " + paymentReceiverOfficeUnitOrgNameEn +
            " paymentReceiverOfficeUnitOrgNameBn = " + paymentReceiverOfficeUnitOrgNameBn +
            " isDeleted = " + isDeleted +
            "]";
    }

}