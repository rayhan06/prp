package promotion_history;

import sessionmanager.SessionConstants;
import util.CommonEmployeeDTO;


public class Promotion_historyDTO extends CommonEmployeeDTO {
    public long officeUnitsId = 0;
    public String officeUnitsText = "";
    public long rankCat = 0;
    public String rankCatText = "";
    public long promotionDate = SessionConstants.MIN_DATE;
    public long gODate = SessionConstants.MIN_DATE;
    public long joiningDate = SessionConstants.MIN_DATE;
    public Integer promotionNatureCat = 0;
    public long previousRank = 0;
    public String previousRankText = "";
    public long filesDropzone = 0;
    public long insertionDate = 0;
    public String insertedBy = "";
    public String modifiedBy = "";
    public long previousEmployeeOfficeId = 0;
    public int jobGradeTypeCat = -1;
    public long gradeTypeLevel = 0;

    @Override
    public String toString() {
        return "Promotion_historyDTO{" +
                "employeeRecordsId=" + employeeRecordsId +
                ", officeUnitsId=" + officeUnitsId +
                ", rankCat=" + rankCat +
                ", promotionDate=" + promotionDate +
                ", gODate=" + gODate +
                ", joiningDate=" + joiningDate +
                ", promotionNatureCat=" + promotionNatureCat +
                ", previousRank=" + previousRank +
                ", filesDropzone=" + filesDropzone +
                ", insertionDate=" + insertionDate +
                ", insertedBy='" + insertedBy + '\'' +
                ", modifiedBy='" + modifiedBy + '\'' +
                ", searchColumn='" + searchColumn + '\'' +
                ", previousEmployeeOfficeId=" + previousEmployeeOfficeId +
                '}';
    }


}