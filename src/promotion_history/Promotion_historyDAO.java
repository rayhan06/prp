package promotion_history;

import common.EmployeeCommonDAOService;
import employee_records.Employee_recordsDAO;
import employee_records.Employee_recordsDTO;
import office_unit_organograms.OfficeUnitOrganograms;
import office_unit_organograms.OfficeUnitOrganogramsRepository;
import office_units.Office_unitsDAO;
import office_units.Office_unitsDTO;
import office_units.Office_unitsRepository;
import org.apache.log4j.Logger;
import pb.CatRepository;
import pb.CategoryLanguageModel;
import sessionmanager.SessionConstants;
import util.CommonDTO;
import util.StringUtils;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@SuppressWarnings({"unused", "Duplicates"})
public class Promotion_historyDAO implements EmployeeCommonDAOService<Promotion_historyDTO> {

    private static final Logger logger = Logger.getLogger(Promotion_historyDAO.class);
    private static final String addQuery = "INSERT INTO {tableName} (employee_records_id, office_units_id,office_units_text, rank_cat,rank_cat_text, promotion_date, g_o_date,"
            + "joining_date, promotion_nature_cat, previous_rank,previous_rank_text,previous_employee_office_id, "
            + "files_dropzone, insertion_date,inserted_by,modified_by,search_column,lastModificationTime,"
            + "isDeleted,ID) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
    private static final String updateQuery = "UPDATE {tableName} SET employee_records_id=?,office_units_id=?,office_units_text=?,rank_cat=?,rank_cat_text=?,promotion_date=?,"
            + "g_o_date=?,joining_date=?,promotion_nature_cat=?,"
            + "previous_rank=?,previous_rank_text=?,previous_employee_office_id=?,files_dropzone=?,insertion_date=?,inserted_by=?,modified_by=?,"
            + "search_column=?,lastModificationTime = ? WHERE ID = ? ";
    private static final String getByEmployeeRecordId = "SELECT * FROM %s WHERE employee_records_id = %d AND isDeleted = 0 order by lastModificationTime desc";
    private static final Map<String, String> searchMap = new HashMap<>();
    private static final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

    private Promotion_historyDAO() {
        searchMap.put("rank_cat", " and (rank_cat = ?)");
        searchMap.put("office_units_id", " and (office_units_id = ?)");
        searchMap.put("promotion_date_start", " and (promotion_date >= ?)");
        searchMap.put("promotion_date_end", " and (promotion_date <= ?)");
        searchMap.put("g_o_date_start", " and (g_o_date >= ?)");
        searchMap.put("g_o_date_end", " and (g_o_date <= ?)");
        searchMap.put("joining_date_start", " and (joining_date >= ?)");
        searchMap.put("joining_date_end", " and (joining_date <= ?)");
        searchMap.put("promotion_nature_cat", " and (promotion_nature_cat = ?)");
        searchMap.put("previous_rank", " and (previous_rank = ?)");
        searchMap.put("employee_records_id_internal", "and (employee_records_id = ?)");
        searchMap.put("AnyField", " and (search_column like ?)");
    }

    private static class LazyLoader {
        static final Promotion_historyDAO INSTANCE = new Promotion_historyDAO();
    }

    public static Promotion_historyDAO getInstance() {
        return Promotion_historyDAO.LazyLoader.INSTANCE;
    }

    public void setSearchColumn(Promotion_historyDTO promotion_historyDTO) {
        List<String> list = new ArrayList<>();
        CategoryLanguageModel languageModel = CatRepository.getInstance().getCategoryLanguageModel("promotion_nature", promotion_historyDTO.promotionNatureCat);
        if (languageModel != null) {
            list.add(languageModel.englishText);
            list.add(languageModel.banglaText);
        }
        try {
            Employee_recordsDTO employeeRecordsDTO = new Employee_recordsDAO().getEmployee_recordsDTOByID(promotion_historyDTO.employeeRecordsId);
            if (employeeRecordsDTO != null) {
                if (employeeRecordsDTO.nameEng != null && employeeRecordsDTO.nameEng.trim().length() > 0) {
                    list.add(employeeRecordsDTO.nameEng.trim());
                }
                if (employeeRecordsDTO.nameBng != null && employeeRecordsDTO.nameBng.trim().length() > 0) {
                    list.add(employeeRecordsDTO.nameBng.trim());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        Office_unitsDTO officeUnitsDTO = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(promotion_historyDTO.officeUnitsId);
        if (officeUnitsDTO != null) {
            if (officeUnitsDTO.unitNameEng != null && officeUnitsDTO.unitNameEng.trim().length() > 0) {
                list.add(officeUnitsDTO.unitNameEng.trim());
            }
            if (officeUnitsDTO.unitNameBng != null && officeUnitsDTO.unitNameBng.trim().length() > 0) {
                list.add(officeUnitsDTO.unitNameBng.trim());
            }
        }

        OfficeUnitOrganograms officeUnitOrganograms = OfficeUnitOrganogramsRepository.getInstance().getById(promotion_historyDTO.rankCat);
        if (officeUnitOrganograms != null) {
            if (officeUnitOrganograms.designation_eng != null && officeUnitOrganograms.designation_eng.trim().length() > 0) {
                list.add(officeUnitOrganograms.designation_eng.trim());
            }
            if (officeUnitOrganograms.designation_bng != null && officeUnitOrganograms.designation_bng.trim().length() > 0) {
                list.add(officeUnitOrganograms.designation_bng.trim());
            }
        }
        promotion_historyDTO.searchColumn = String.join(" ", list);
    }

    public void set(PreparedStatement ps, Promotion_historyDTO promotion_historyDTO, boolean isInsert) throws SQLException {
        int index = 0;
        setSearchColumn(promotion_historyDTO);
        ps.setObject(++index, promotion_historyDTO.employeeRecordsId);
        ps.setObject(++index, promotion_historyDTO.officeUnitsId);
        ps.setObject(++index, promotion_historyDTO.officeUnitsText);
        ps.setObject(++index, promotion_historyDTO.rankCat);
        ps.setObject(++index, promotion_historyDTO.rankCatText);
        ps.setObject(++index, promotion_historyDTO.promotionDate);
        ps.setObject(++index, promotion_historyDTO.gODate);
        ps.setObject(++index, promotion_historyDTO.joiningDate);
        ps.setObject(++index, promotion_historyDTO.promotionNatureCat);
        ps.setObject(++index, promotion_historyDTO.previousRank);
        ps.setObject(++index, promotion_historyDTO.previousRankText);
        ps.setObject(++index, promotion_historyDTO.previousEmployeeOfficeId);
        ps.setObject(++index, promotion_historyDTO.filesDropzone);
        ps.setObject(++index, promotion_historyDTO.insertionDate);
        ps.setObject(++index, promotion_historyDTO.insertedBy);
        ps.setObject(++index, promotion_historyDTO.modifiedBy);
        ps.setObject(++index, promotion_historyDTO.searchColumn);
        ps.setObject(++index, System.currentTimeMillis());
        if (isInsert) {
            ps.setObject(++index, 0);
        }
        ps.setObject(++index, promotion_historyDTO.iD);
    }

    public Promotion_historyDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Promotion_historyDTO promotion_historyDTO = new Promotion_historyDTO();
            promotion_historyDTO.iD = rs.getLong("ID");
            promotion_historyDTO.employeeRecordsId = rs.getLong("employee_records_id");
            promotion_historyDTO.officeUnitsId = rs.getLong("office_units_id");
            promotion_historyDTO.officeUnitsText = rs.getString("office_units_text");
            promotion_historyDTO.rankCat = rs.getLong("rank_cat");
            promotion_historyDTO.rankCatText = rs.getString("rank_cat_text");
            promotion_historyDTO.promotionDate = rs.getLong("promotion_date");
            promotion_historyDTO.gODate = rs.getLong("g_o_date");
            promotion_historyDTO.joiningDate = rs.getLong("joining_date");
            promotion_historyDTO.promotionNatureCat = rs.getInt("promotion_nature_cat");
            promotion_historyDTO.previousRank = rs.getLong("previous_rank");
            promotion_historyDTO.previousRankText = rs.getString("previous_rank_text");
            promotion_historyDTO.previousEmployeeOfficeId = rs.getLong("previous_employee_office_id");
            promotion_historyDTO.filesDropzone = rs.getLong("files_dropzone");
            promotion_historyDTO.insertionDate = rs.getLong("insertion_date");
            promotion_historyDTO.insertedBy = rs.getString("inserted_by");
            promotion_historyDTO.modifiedBy = rs.getString("modified_by");
            promotion_historyDTO.searchColumn = rs.getString("search_column");
            promotion_historyDTO.isDeleted = rs.getInt("isDeleted");
            promotion_historyDTO.lastModificationTime = rs.getLong("lastModificationTime");
            return promotion_historyDTO;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    @Override
    public String getTableName() {
        return "promotion_history";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }


    @Override
    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Promotion_historyDTO) commonDTO, updateQuery, false);
    }

    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Promotion_historyDTO) commonDTO, addQuery, true);
    }

    public Promotion_historyDTO getDTOByID(long ID) {
        return getDTOFromID(ID);
    }

    public List<Promotion_historyDTOWithValues> getAllPromotionDetailsByEmployeeRecordsId(long employeeRecordsId) {
        return getDtosByEmployeeRecordsId(employeeRecordsId)
                .stream()
                .map(this::buildPromotion_historyDTOWithValuesByDto)
                .collect(Collectors.toList());
    }

    private Promotion_historyDTOWithValues buildPromotion_historyDTOWithValuesByDto(Promotion_historyDTO dto) {
        Promotion_historyDTOWithValues model = new Promotion_historyDTOWithValues();
        model.dto = dto;

        Office_unitsDTO officeUnit = null;
        try {
            officeUnit = Office_unitsDAO.getInstance().getDTOFromID(dto.officeUnitsId);
        } catch (Exception e) {
            logger.error(e);
        }
        if (officeUnit != null) {
            model.newOfficeUnitEn = officeUnit.unitNameEng;
            model.newOfficeUnitBn = officeUnit.unitNameBng;
        } else {
            model.newOfficeUnitEn =dto.officeUnitsText;
            model.newOfficeUnitBn = dto.officeUnitsText;
        }

        OfficeUnitOrganograms org = OfficeUnitOrganogramsRepository.getInstance().getById(dto.rankCat);
        if (org != null) {
            model.newDesignationEn = org.designation_eng;
            model.newDesignationBn = org.designation_bng;
        } else {
            model.newDesignationEn = dto.rankCatText;
            model.newDesignationBn = dto.rankCatText;
        }

        model.promotionDateEn = dto.promotionDate > SessionConstants.MIN_DATE ? simpleDateFormat.format(new Date(dto.promotionDate)) : "";
        model.promotionDateBn = !model.promotionDateEn.equals("") ? StringUtils.convertToBanNumber(model.promotionDateEn) : "";
        CategoryLanguageModel catModel = CatRepository.getInstance().getCategoryLanguageModel("promotion_nature", dto.promotionNatureCat);
        if (catModel != null) {
            model.promotionNatureTypeEn = catModel.englishText;
            model.promotionNatureTypeBn = catModel.banglaText;
        } else {
            model.promotionNatureTypeEn = "";
            model.promotionNatureTypeBn = "";
        }

        return model;
    }

    public List<Promotion_historyDTO> getDtosByEmployeeRecordsId(long employeeRecordsId) {
        return getDTOs(String.format(getByEmployeeRecordId, getTableName(), employeeRecordsId));
    }

    //add repository
    public List<Promotion_historyDTO> getAllPromotion_history(boolean isFirstReload) {
        return getAllDTOs(isFirstReload);
    }
}
	