package promotion_history;

public class Promotion_historyDTOWithValues {
	
	public Promotion_historyDTO dto;
	public String newOfficeUnitEn;
	public String newOfficeUnitBn;
	public String newDesignationEn;
	public String newDesignationBn;
	public String rankTypeEn;
	public String rankTypeBn;
	public String previousRankTypeEn;
	public String previousRankTypeBn;
	public String promotionDateEn;
	public String promotionDateBn;
	public String promotionNatureTypeEn;
	public String promotionNatureTypeBn;

}
