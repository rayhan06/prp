package promotion_history;

import util.CommonMaps;


public class Promotion_historyMAPS extends CommonMaps {
    public Promotion_historyMAPS(String tableName) {


        java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
        java_DTO_map.put("employeeRecordsId".toLowerCase(), "employeeRecordsId".toLowerCase());
        java_DTO_map.put("rankCat".toLowerCase(), "rankCat".toLowerCase());
        java_DTO_map.put("promotionDate".toLowerCase(), "promotionDate".toLowerCase());
        java_DTO_map.put("gODate".toLowerCase(), "gODate".toLowerCase());
        java_DTO_map.put("joiningDate".toLowerCase(), "joiningDate".toLowerCase());
        java_DTO_map.put("promotionNatureCat".toLowerCase(), "promotionNatureCat".toLowerCase());
        java_DTO_map.put("previousRankType".toLowerCase(), "previousRankType".toLowerCase());
        java_DTO_map.put("filesDropzone".toLowerCase(), "filesDropzone".toLowerCase());
        java_DTO_map.put("insertionDate".toLowerCase(), "insertionDate".toLowerCase());
        java_DTO_map.put("insertedBy".toLowerCase(), "insertedBy".toLowerCase());
        java_DTO_map.put("modifiedBy".toLowerCase(), "modifiedBy".toLowerCase());
        java_DTO_map.put("searchColumn".toLowerCase(), "searchColumn".toLowerCase());
        java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
        java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

        java_SQL_map.put("employee_records_id".toLowerCase(), "employeeRecordsId".toLowerCase());
        java_SQL_map.put("rank_cat".toLowerCase(), "rankCat".toLowerCase());
        java_SQL_map.put("promotion_date".toLowerCase(), "promotionDate".toLowerCase());
        java_SQL_map.put("g_o_date".toLowerCase(), "gODate".toLowerCase());
        java_SQL_map.put("joining_date".toLowerCase(), "joiningDate".toLowerCase());
        java_SQL_map.put("promotion_nature_cat".toLowerCase(), "promotionNatureCat".toLowerCase());
        java_SQL_map.put("previous_rank_type".toLowerCase(), "previousRankType".toLowerCase());
        java_SQL_map.put("files_dropzone".toLowerCase(), "filesDropzone".toLowerCase());

        java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
        java_Text_map.put("Employee Records Id".toLowerCase(), "employeeRecordsId".toLowerCase());
        java_Text_map.put("Rank".toLowerCase(), "rankCat".toLowerCase());
        java_Text_map.put("Promotion Date".toLowerCase(), "promotionDate".toLowerCase());
        java_Text_map.put("G O Date".toLowerCase(), "gODate".toLowerCase());
        java_Text_map.put("Joining Date".toLowerCase(), "joiningDate".toLowerCase());
        java_Text_map.put("Promotion Nature".toLowerCase(), "promotionNatureCat".toLowerCase());
        java_Text_map.put("Previous Rank".toLowerCase(), "previousRankType".toLowerCase());
        java_Text_map.put("Files Dropzone".toLowerCase(), "filesDropzone".toLowerCase());
        java_Text_map.put("Insertion Date".toLowerCase(), "insertionDate".toLowerCase());
        java_Text_map.put("Inserted By".toLowerCase(), "insertedBy".toLowerCase());
        java_Text_map.put("Modified By".toLowerCase(), "modifiedBy".toLowerCase());
        java_Text_map.put("Search Column".toLowerCase(), "searchColumn".toLowerCase());
        java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
        java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

    }

}