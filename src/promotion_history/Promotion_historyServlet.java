package promotion_history;

import common.*;
import employee_offices.EmployeeOfficeDTO;
import employee_offices.EmployeeOfficeRepository;
import employee_offices.EmployeeOfficesDAO;
import login.LoginDTO;
import office_unit_organogram.Office_unit_organogramDAO;
import office_unit_organograms.OfficeUnitOrganograms;
import office_unit_organograms.OfficeUnitOrganogramsRepository;
import office_units.Office_unitsRepository;
import offices.OfficesRepository;
import org.apache.log4j.Logger;
import pb.Utils;
import permission.MenuConstants;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.CommonConstant;
import util.CommonDTO;
import util.HttpRequestUtils;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;


@WebServlet("/Promotion_historyServlet")
@MultipartConfig
public class Promotion_historyServlet extends BaseServlet implements EmployeeServletService {
    private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Promotion_historyServlet.class);

    private final static String tableName = "promotion_history";

    private final Promotion_historyDAO promotion_historyDAO = Promotion_historyDAO.getInstance();

    @Override
    public String getTableName() {
        return tableName;
    }

    @Override
    public String getServletName() {
        return "Promotion_historyServlet";
    }

    @Override
    public Promotion_historyDAO getCommonDAOService() {
        return promotion_historyDAO;
    }

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        Promotion_historyDTO promotion_historyDTO;
        SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
        String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
        long lastModificationTime = System.currentTimeMillis() + 180000;//3mins delay
        if (addFlag) {
            promotion_historyDTO = new Promotion_historyDTO();
            promotion_historyDTO.employeeRecordsId = Long.parseLong(request.getParameter("empId"));
            promotion_historyDTO.insertionDate = lastModificationTime;
            promotion_historyDTO.insertedBy = userDTO.userName;

        } else {
            promotion_historyDTO = promotion_historyDAO.getDTOByID(Long.parseLong(request.getParameter("iD")));
        }
        promotion_historyDTO.modifiedBy = String.valueOf(userDTO.ID);
        promotion_historyDTO.lastModificationTime = lastModificationTime;

        promotion_historyDTO.officeUnitsId = Long.parseLong(request.getParameter("officeUnitsId"));
        promotion_historyDTO.officeUnitsText = Office_unitsRepository.getInstance().geText(Language, promotion_historyDTO.officeUnitsId);

        promotion_historyDTO.rankCat = Integer.parseInt(request.getParameter("rankCat"));
        promotion_historyDTO.rankCatText = OfficeUnitOrganogramsRepository.getInstance().getDesignation(Language, promotion_historyDTO.rankCat);

        Date d = f.parse(request.getParameter("promotionDate"));
        promotion_historyDTO.promotionDate = d.getTime();
        d = f.parse(request.getParameter("gODate"));
        promotion_historyDTO.gODate = d.getTime();
        d = f.parse(request.getParameter("joiningDate"));
        promotion_historyDTO.joiningDate = d.getTime();
        promotion_historyDTO.promotionNatureCat = Integer.parseInt(request.getParameter("promotionNatureCat"));
        promotion_historyDTO.previousRank = 0;
        OfficeUnitOrganograms officeUnitOrganograms = OfficeUnitOrganogramsRepository.getInstance().getById(promotion_historyDTO.rankCat);
        promotion_historyDTO.jobGradeTypeCat = officeUnitOrganograms.job_grade_type_cat;
        String gradeLevel = request.getParameter("gradeCatLevel");
        logger.debug("gradeLevel = " + gradeLevel);
        if (gradeLevel != null && !gradeLevel.equalsIgnoreCase("")) {
            promotion_historyDTO.gradeTypeLevel = Long.parseLong(gradeLevel);
        }

        EmployeeOfficeDTO employeeOfficeDTO = EmployeeOfficesDAO.getInstance().getByEmployeeRecordIdIsDefault(promotion_historyDTO.employeeRecordsId);

        CommonDAOService.CACHE_UPDATE_MODEL_THREAD_LOCAL.set(new CacheUpdateModel(lastModificationTime));

        Utils.handleTransaction(()->{
            if (employeeOfficeDTO != null) {
                promotion_historyDTO.previousEmployeeOfficeId = employeeOfficeDTO.id;
                EmployeeOfficesDAO.getInstance().updateLastOffice(employeeOfficeDTO, promotion_historyDTO.joiningDate,lastModificationTime);
                Office_unit_organogramDAO.getInstance().updateVacancy(
                        employeeOfficeDTO.officeUnitOrganogramId, 0, true, userDTO.ID, lastModificationTime
                );
            }
            promotion_historyDTO.filesDropzone = Long.parseLong(request.getParameter("filesDropzone"));

            if (!addFlag) {
                String filesDropzoneFilesToDelete = request.getParameter("filesDropzoneFilesToDelete");
                if(filesDropzoneFilesToDelete!=null){
                    List<Long> idList =  Stream.of(filesDropzoneFilesToDelete.split(","))
                            .map(e->Utils.parseOptionalLong(e,null))
                            .filter(Objects::nonNull)
                            .filter(e->e>0)
                            .collect(Collectors.toList());

                    filesDAO.delete(idList,lastModificationTime,CommonDAOService.CONNECTION_THREAD_LOCAL.get());
                }
            }
            promotion_historyDTO.searchColumn = (request.getParameter("searchColumn"));

            if (addFlag) {
                promotion_historyDAO.add(promotion_historyDTO);
            } else {
                promotion_historyDAO.update(promotion_historyDTO);
            }
            buildEmployeeOfficeDTOAndSendtoDAO(promotion_historyDTO, userDTO, addFlag,lastModificationTime);
        });

        Utils.executeCache();

        return promotion_historyDTO;
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[]{MenuConstants.PROMOTION_HISTORY_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[]{MenuConstants.PROMOTION_HISTORY_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[]{MenuConstants.PROMOTION_HISTORY_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Promotion_historyServlet.class;
    }

    @Override
    public String getAjaxAddRedirectURL(HttpServletRequest request, CommonDTO commonDTO) {
        return getAddOrEditOrDeleteRedirectURL(request, 3, "promotion");
    }

    @Override
    public String getAjaxEditRedirectURL(HttpServletRequest request, CommonDTO commonDTO) {
        return getAddOrEditOrDeleteRedirectURL(request, 3, "promotion");
    }

    @Override
    public String getDeleteRedirectURL(HttpServletRequest request) {
        return getAddOrEditOrDeleteRedirectURL(request, 3, "promotion");
    }

    @Override
    public boolean getAddPagePermission(HttpServletRequest request) {
        return checkAddPermission(request);
    }

    @Override
    public boolean getEditPagePermission(HttpServletRequest request) {
        return checkEditOrViewPagePermission(request);
    }

    @Override
    public boolean getViewPagePermission(HttpServletRequest request) {
        return checkEditOrViewPagePermission(request);
    }

    @Override
    public boolean getEditPermission(HttpServletRequest request) {
        return checkEditOrViewPagePermission(request);
    }

    @Override
    public boolean getAddPermission(HttpServletRequest request) {
        return checkAddPermission(request);
    }


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }
        try {
            String actionType = request.getParameter("actionType");
            switch (actionType) {
                case "getDesignationOption": {
                    String officeUnitStr = request.getParameter("office_unit_id");
                    long officeUnitId = 0;
                    if (officeUnitStr != null && !officeUnitStr.isEmpty()) {
                        officeUnitId = Long.parseLong(request.getParameter("office_unit_id"));
                    }

                    String language = userDTO.languageID == CommonConstant.Language_ID_English ? "English" : "Bangla";

                    String selectedIdStr = request.getParameter("selectedId");

                    long selectedId = 0;
                    if (selectedIdStr != null && !selectedIdStr.isEmpty()) {
                        selectedId = Long.parseLong(selectedIdStr);
                    }

                    String options = OfficeUnitOrganogramsRepository.getInstance().buildOptionsForDesignation(language, selectedId, officeUnitId);
                    PrintWriter out = response.getWriter();
                    out.println(options);
                    out.close();
                    return;
                }
                case "getDesignationAddTable": {
                    long officeUnitId = Long.parseLong(request.getParameter("office_unit_id"));

                    List<OfficeUnitOrganograms> allData = OfficeUnitOrganogramsRepository.getInstance().getByOfficeUnitId(officeUnitId);
                    List<OfficeUnitOrganograms> data = new ArrayList<>();
                    for (OfficeUnitOrganograms officeUnitOrganogram : allData) {
                        EmployeeOfficeDTO employeeOfficeDTO = EmployeeOfficeRepository.getInstance().getByOfficeUnitOrganogramId(officeUnitOrganogram.id);
                        if (employeeOfficeDTO == null || employeeOfficeDTO.isDeleted != 0) {
                            data.add(officeUnitOrganogram);
                        }
                    }
                    request.getSession().setAttribute("data", data);
                    RequestDispatcher rd;
                    rd = request.getRequestDispatcher("office_assign/officeSearchModalTable.jsp");
                    rd.forward(request, response);

                    return;
                }
                case "getPreviousPromotionAddPage": {
                    RequestDispatcher rd;
                    rd = request.getRequestDispatcher("promotion_history/previous_promotion_historyEdit.jsp");
                    rd.forward(request, response);

                    return;
                }
                default:
                    search(request);
                    super.doGet(request, response);
            }
        } catch (Exception ex) {
            logger.debug(ex);
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }
        try {
            String actionType = request.getParameter("actionType");
            if ("ajax_addPreviousPromotion".equals(actionType)) {
                try {
                    CommonDTO commonDTO = addPreviousHistory(request, true, userDTO);
                    finalize(request);
                    ApiResponse.sendSuccessResponse(response, getAjaxAddRedirectURL(request, commonDTO));
                } catch (Exception ex) {
                    ex.printStackTrace();
                    logger.error(ex);
                    ApiResponse.sendErrorResponse(response, ex.getMessage());
                }
            } else {
                search(request);
                super.doPost(request, response);
            }
        } catch (Exception ex) {
            logger.debug(ex);
        }
    }

    private CommonDTO addPreviousHistory(HttpServletRequest request, boolean addFlag, UserDTO userDTO) throws Exception {
        Promotion_historyDTO promotion_historyDTO;
        SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");

        long currentTime = System.currentTimeMillis();
        if (addFlag) {
            promotion_historyDTO = new Promotion_historyDTO();
            promotion_historyDTO.employeeRecordsId = Long.parseLong(request.getParameter("empId"));
            promotion_historyDTO.insertionDate = currentTime;
            promotion_historyDTO.insertedBy = userDTO.userName;

        } else {
            promotion_historyDTO = promotion_historyDAO.getDTOByID(Long.parseLong(request.getParameter("iD")));
        }
        promotion_historyDTO.modifiedBy = String.valueOf(userDTO.ID);
        promotion_historyDTO.lastModificationTime = currentTime;
        String value;
        value = request.getParameter("officeUnitsId");
        if (value != null && !value.isEmpty())
            promotion_historyDTO.officeUnitsId = Long.parseLong(value);
        promotion_historyDTO.officeUnitsText = request.getParameter("officeUnitsText");


        value = request.getParameter("rankCat");
        if (value != null && !value.isEmpty())
            promotion_historyDTO.rankCat = Long.parseLong(value);
        promotion_historyDTO.rankCatText = request.getParameter("rankText");

        value = request.getParameter("promotionDate");
        Date d;
        if (value != null && !value.isEmpty()) {
            d = f.parse(value);
            promotion_historyDTO.promotionDate = d.getTime();
        }

        value = request.getParameter("gODate");
        if (value != null && !value.isEmpty()) {
            d = f.parse(request.getParameter("gODate"));
            promotion_historyDTO.gODate = d.getTime();
        }

        value = request.getParameter("joiningDate");
        if (value != null && !value.isEmpty()) {
            d = f.parse(request.getParameter("joiningDate"));
            promotion_historyDTO.joiningDate = d.getTime();
        }
        promotion_historyDTO.promotionNatureCat = Integer.parseInt(request.getParameter("promotionNatureCat"));

        String previousRankStr = request.getParameter("previousRank");
        if (previousRankStr != null && !previousRankStr.isEmpty())
            promotion_historyDTO.previousRank = Long.parseLong(previousRankStr);
        promotion_historyDTO.previousRankText = request.getParameter("previousRankCat");

        if (request.getParameter("filesDropzone") != null && !request.getParameter("filesDropzone").isEmpty())
            promotion_historyDTO.filesDropzone = Long.parseLong(request.getParameter("filesDropzone"));

        if (!addFlag) {
            String filesDropzoneFilesToDelete = request.getParameter("filesDropzoneFilesToDelete");
            String[] deleteArray = filesDropzoneFilesToDelete.split(",");
            for (int i = 0; i < deleteArray.length; i++) {
                System.out.println("going to delete " + deleteArray[i]);
                if (i > 0) {
                    filesDAO.delete(Long.parseLong(deleteArray[i]));
                }
            }
        }
        promotion_historyDTO.searchColumn = (request.getParameter("searchColumn"));

        if (addFlag) {
            promotion_historyDAO.add(promotion_historyDTO);
        } else {
            promotion_historyDAO.update(promotion_historyDTO);
        }

        return promotion_historyDTO;
    }

    private void buildEmployeeOfficeDTOAndSendtoDAO(Promotion_historyDTO promotion_historyDTO, UserDTO userDTO, boolean addFlag,long lastModificationTime) throws Exception {

        EmployeeOfficeDTO employeeOfficeDTO = new EmployeeOfficeDTO();
        employeeOfficeDTO.employeeRecordId = promotion_historyDTO.employeeRecordsId;
        employeeOfficeDTO.officeId = (int) CommonConstant.DEFAULT_OFFICE_ID;
        employeeOfficeDTO.officeUnitId = promotion_historyDTO.officeUnitsId;
        employeeOfficeDTO.officeUnitOrganogramId = promotion_historyDTO.rankCat;
        employeeOfficeDTO.officeNameEn = OfficesRepository.getInstance().getOfficesDTOByID(employeeOfficeDTO.officeId).officeNameEng;
        employeeOfficeDTO.officeNameBn = OfficesRepository.getInstance().getOfficesDTOByID(employeeOfficeDTO.officeId).officeNameBng;
        employeeOfficeDTO.unitNameEn = Office_unitsRepository.getInstance().geText("English", employeeOfficeDTO.officeUnitId);
        employeeOfficeDTO.unitNameBn = Office_unitsRepository.getInstance().geText("BANGLA", employeeOfficeDTO.officeUnitId);
        employeeOfficeDTO.designationEn = OfficeUnitOrganogramsRepository.getInstance().getDesignation("ENGLISH", employeeOfficeDTO.officeUnitOrganogramId);
        employeeOfficeDTO.designationBn = OfficeUnitOrganogramsRepository.getInstance().getDesignation("BANGLA", employeeOfficeDTO.officeUnitOrganogramId);
        employeeOfficeDTO.designation = employeeOfficeDTO.designationEn;
        employeeOfficeDTO.inchargeLabel = "Routine responsibility";
        employeeOfficeDTO.joiningDate = promotion_historyDTO.joiningDate;
        if (addFlag) {
            employeeOfficeDTO.created = promotion_historyDTO.insertionDate;
            employeeOfficeDTO.createdBy = userDTO.ID;
        }
        employeeOfficeDTO.modified = promotion_historyDTO.lastModificationTime;
        employeeOfficeDTO.modifiedBy = userDTO.ID;
        employeeOfficeDTO.lastModificationTime = promotion_historyDTO.lastModificationTime;
        employeeOfficeDTO.isDefault = 1;
        employeeOfficeDTO.promotionHistoryId = promotion_historyDTO.iD;
        employeeOfficeDTO.jobGradeTypeCat = promotion_historyDTO.jobGradeTypeCat;
        employeeOfficeDTO.gradeTypeLevel = promotion_historyDTO.gradeTypeLevel;
        EmployeeOfficesDAO.getInstance().updateAfterPromotionHistory(employeeOfficeDTO, addFlag,lastModificationTime);
    }
}

