package mp_payroll_bill_allowance;

import sessionmanager.SessionConstants;
import util.CommonDTO;

public class Mp_payroll_bill_allowanceDTO extends CommonDTO {
    public long mpPayrollBillId = -1;
    public long mpPayrollAllowanceConfigurationId = -1;
    public int amount = 0;
    public long insertedBy = -1;
    public long insertionTime = SessionConstants.MIN_DATE;
    public long modifiedBy = -1;

    public Mp_payroll_bill_allowanceDTO() {}

    public Mp_payroll_bill_allowanceDTO(long configurationId, int amount) {
        this.mpPayrollAllowanceConfigurationId = configurationId;
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "Mp_payroll_bill_allowanceDTO{" +
                "mpPayrollBillId=" + mpPayrollBillId +
                ", mpPayrollAllowanceConfigurationId=" + mpPayrollAllowanceConfigurationId +
                ", amount=" + amount +
                ", insertedBy=" + insertedBy +
                ", insertionTime=" + insertionTime +
                ", modifiedBy=" + modifiedBy +
                '}';
    }
}
