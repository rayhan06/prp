package mp_payroll_bill_allowance;

import common.CommonDAOService;
import org.apache.log4j.Logger;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings({"Duplicates"})
public class Mp_payroll_bill_allowanceDAO implements CommonDAOService<Mp_payroll_bill_allowanceDTO> {
    private static final Logger logger = Logger.getLogger(Mp_payroll_bill_allowanceDAO.class);

    private static final String addQuery =
            "INSERT INTO {tableName} (mp_payroll_bill_id,mp_payroll_allowance_configuration_id,amount,modified_by,lastModificationTime,"
                    .concat("inserted_by,insertion_time,isDeleted,ID) VALUES (?,?,?,?,?,?,?,?,?)");

    private static final String updateQuery =
            "UPDATE {tableName} SET mp_payroll_bill_id=?,mp_payroll_allowance_configuration_id=?,amount=?,"
                    .concat("modified_by=?,lastModificationTime=? WHERE ID=?");

    private static final String getByMpBillId =
            "SELECT * From mp_payroll_bill_allowance where mp_payroll_bill_id = %d AND isDeleted=0;";

    private static final Map<String, String> searchMap = new HashMap<>();

    private Mp_payroll_bill_allowanceDAO() {
    }

    private static class LazyLoader {
        static final Mp_payroll_bill_allowanceDAO INSTANCE = new Mp_payroll_bill_allowanceDAO();
    }

    public static Mp_payroll_bill_allowanceDAO getInstance() {
        return Mp_payroll_bill_allowanceDAO.LazyLoader.INSTANCE;
    }

    @Override
    public void set(PreparedStatement ps, Mp_payroll_bill_allowanceDTO dto, boolean isInsert) throws SQLException {
        int index = 0;
        ps.setLong(++index, dto.mpPayrollBillId);
        ps.setLong(++index, dto.mpPayrollAllowanceConfigurationId);
        ps.setInt(++index, dto.amount);
        ps.setLong(++index, dto.modifiedBy);
        ps.setLong(++index, dto.lastModificationTime);
        if (isInsert) {
            ps.setLong(++index, dto.insertedBy);
            ps.setLong(++index, dto.insertionTime);
            ps.setInt(++index, 0);
        }
        ps.setLong(++index, dto.iD);
    }

    @Override
    public Mp_payroll_bill_allowanceDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Mp_payroll_bill_allowanceDTO dto = new Mp_payroll_bill_allowanceDTO();
            dto.iD = rs.getLong("ID");
            dto.mpPayrollBillId = rs.getInt("mp_payroll_bill_id");
            dto.mpPayrollAllowanceConfigurationId = rs.getInt("mp_payroll_allowance_configuration_id");
            dto.amount = rs.getInt("amount");
            dto.insertedBy = rs.getLong("inserted_by");
            dto.insertionTime = rs.getLong("insertion_time");
            dto.isDeleted = rs.getInt("isDeleted");
            dto.modifiedBy = rs.getLong("modified_by");
            dto.lastModificationTime = rs.getLong("lastModificationTime");
            return dto;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    @Override
    public String getTableName() {
        return "mp_payroll_bill_allowance";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    @Override
    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Mp_payroll_bill_allowanceDTO) commonDTO, addQuery, true);
    }

    @Override
    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Mp_payroll_bill_allowanceDTO) commonDTO, updateQuery, false);
    }

    public List<Mp_payroll_bill_allowanceDTO> getByMpBillId(long mpPayrollBillId) {
        return getDTOs(String.format(getByMpBillId, mpPayrollBillId));
    }
}