package brand;

import java.io.IOException;
import java.io.*;
import java.text.SimpleDateFormat;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import org.apache.log4j.Logger;

import login.LoginDTO;
import permission.MenuConstants;
import role.PermissionRepository;


import sessionmanager.SessionConstants;

import user.UserDTO;
import user.UserRepository;
import util.*;
import java.util.*;
import javax.servlet.http.*;
import java.util.UUID;


import geolocation.GeoLocationDAO2;
import language.LC;
import language.LM;
import common.ApiResponse;
import com.google.gson.Gson;
import pb.*;
import pbReport.*;
import pb_notifications.Pb_notificationsRepository;

import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;




/**
 * Servlet implementation class BrandServlet
 */
@WebServlet("/BrandServlet")
@MultipartConfig
public class BrandServlet extends HttpServlet 
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(BrandServlet.class);

    String tableName = "brand";

	BrandDAO brandDAO;
	CommonRequestHandler commonRequestHandler;
    private final Gson gson = new Gson();
    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public BrandServlet() 
	{
        super();
    	try
    	{
			brandDAO = new BrandDAO(tableName);
			commonRequestHandler = new CommonRequestHandler(brandDAO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error("",e);
		}
    }   

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		System.out.println("In doget request = " + request);
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		try
		{
			String actionType = request.getParameter("actionType");
			if(actionType.equals("getAddPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.BRAND_ADD))
				{
					commonRequestHandler.getAddPage(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getEditPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.BRAND_UPDATE))
				{
					getBrand(request, response);					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}						
			}
			else if(actionType.equals("getURL"))
			{
				String URL = request.getParameter("URL");
				System.out.println("URL = " + URL);
				response.sendRedirect(URL);			
			}			
			else if(actionType.equals("search"))
			{
				System.out.println("search requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.BRAND_SEARCH))
				{
					if(isPermanentTable)
					{
						String filter = request.getParameter("filter");
						System.out.println("filter = " + filter);
						if(filter!=null)
						{
							filter = ""; //shouldn't be directly used, rather manipulate it.
							searchBrand(request, response, isPermanentTable, filter);
						}
						else
						{
							searchBrand(request, response, isPermanentTable, "");
						}
					}					
					else
					{
						//searchBrand(request, response, tempTableName, isPermanentTable);
					}
				}			
			}
			else if(actionType.equals("view"))
			{
				System.out.println("view requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.BRAND_SEARCH))
				{
					commonRequestHandler.view(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else
			{
				request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			}
		}
		catch(Exception ex)
		{
			logger.error("",ex);
		}
	}


	
	

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		// TODO Auto-generated method stub
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		System.out.println("doPost");
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		
		try
		{
			String actionType = request.getParameter("actionType");
			System.out.println("actionType = " + actionType);
			if(actionType.equals("add"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.BRAND_ADD))
				{
					System.out.println("going to  addBrand ");
					addBrand(request, response, true, userDTO, true);
				}
				else
				{
					System.out.println("Not going to  addBrand ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}

			else if(actionType.equals("getDTO"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.BRAND_ADD))
				{
					getDTO(request, response);					
				}
				else
				{
					System.out.println("Not going to  addBrand ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else if(actionType.equals("edit"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.BRAND_UPDATE))
				{					
					addBrand(request, response, false, userDTO, isPermanentTable);					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("delete"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.BRAND_UPDATE))
				{
					commonRequestHandler.delete(request, response, userDTO);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}	
			else if(actionType.equals("search"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.BRAND_SEARCH))
				{
					searchBrand(request, response, true, "");
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getGeo"))
			{
				System.out.println("going to geoloc ");
				request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
			}
			else
			{
				request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			}
			
		}
		catch(Exception ex)
		{
			logger.error("",ex);
			logger.debug(ex);
		}
	}
	
	private void getDTO(HttpServletRequest request, HttpServletResponse response) 
	{
		try 
		{
			System.out.println("In getDTO");
			BrandDTO brandDTO = (BrandDTO)brandDAO.getDTOByID(Long.parseLong(request.getParameter("ID")));
			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			
			String encoded = this.gson.toJson(brandDTO);
			System.out.println("json encoded data = " + encoded);
			out.print(encoded);
			out.flush();
		}
		catch (NumberFormatException e) 
		{
			// TODO Auto-generated catch block
			logger.error("",e);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error("",e);
		}
		
	}

	private void addBrand(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, boolean isPermanentTable) throws IOException 
	{
		// TODO Auto-generated method stub
		try 
		{
			request.setAttribute("failureMessage", "");
			System.out.println("%%%% addBrand");
			String language = userDTO.languageID == SessionConstants.ENGLISH? "English":"Bangla";
			BrandDTO brandDTO;
			SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
						
			if(addFlag == true)
			{
				brandDTO = new BrandDTO();
			}
			else
			{
				brandDTO = (BrandDTO)brandDAO.getDTOByID(Long.parseLong(request.getParameter("iD")));
			}
			
			String Value = "";

			Value = request.getParameter("nameEn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("nameEn = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				brandDTO.nameEn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
				throw new Exception(LM.getText(LC.BRAND_ADD_NAMEEN, language) + " " + ErrorMessage.getEmptyMessage(language));
			}

			Value = request.getParameter("nameBn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("nameBn = " + Value);
			if(Value != null)
			{
				brandDTO.nameBn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("assetCategoryType");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("assetCategoryType = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				brandDTO.assetCategoryType = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("searchColumn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("searchColumn = " + Value);
			if(Value != null)
			{
				brandDTO.searchColumn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			if(addFlag)
			{
				brandDTO.insertedByUserId = userDTO.ID;
			}


			if(addFlag)
			{
				brandDTO.insertedByOrganogramId = userDTO.organogramID;
			}


			if(addFlag)
			{				
				brandDTO.insertionDate = TimeConverter.getToday();
			}			


			brandDTO.lastModifierUser = userDTO.userName;

			
			System.out.println("Done adding  addBrand dto = " + brandDTO);
			long returnedID = -1;
			
			
			if(isPermanentTable == false) //add new row for validation and make the old row outdated
			{
				brandDAO.setIsDeleted(brandDTO.iD, CommonDTO.OUTDATED);
				returnedID = brandDAO.add(brandDTO);
				brandDAO.setIsDeleted(returnedID, CommonDTO.WAITING_FOR_APPROVAL);
			}
			else if(addFlag == true)
			{
				returnedID = brandDAO.manageWriteOperations(brandDTO, SessionConstants.INSERT, -1, userDTO);
			}
			else
			{				
				returnedID = brandDAO.manageWriteOperations(brandDTO, SessionConstants.UPDATE, -1, userDTO);											
			}
			PBNameRepository.getInstance().reload(false);
			
		
			if(!isPermanentTable)
			{
				commonRequestHandler.validate(brandDAO.getDTOByID(returnedID), request, response, userDTO);
			}
			else
			{
				ApiResponse.sendSuccessResponse(response, "BrandServlet?actionType=search");
			}
					
		}
		catch (Exception e) 
		{
			logger.error("",e);
			ApiResponse.sendErrorResponse(response, e.getMessage());
		}
	}
	
	



	
	
	

	private void getBrand(HttpServletRequest request, HttpServletResponse response, long id) throws ServletException, IOException
	{
		System.out.println("in getBrand");
		BrandDTO brandDTO = null;
		try 
		{
			brandDTO = (BrandDTO)brandDAO.getDTOByID(id);
			request.setAttribute("ID", brandDTO.iD);
			request.setAttribute("brandDTO",brandDTO);
			request.setAttribute("brandDAO",brandDAO);
			
			String URL= "";
			String getBodyOnly = request.getParameter("getBodyOnly");
						
			if(getBodyOnly  != null && !getBodyOnly.equalsIgnoreCase(""))
			{
				URL = "brand/brandEditBody.jsp?actionType=edit";
			}
			else
			{
				URL = "brand/brandEdit.jsp?actionType=edit";
			}				
						
			RequestDispatcher rd = request.getRequestDispatcher(URL);
			rd.forward(request, response);
		}
		catch (NumberFormatException e) 
		{
			logger.error("",e);
		}
		catch (Exception e) 
		{
			logger.error("",e);
		}
	}
	
	
	private void getBrand(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		getBrand(request, response, Long.parseLong(request.getParameter("ID")));	
	}
	
	private void searchBrand(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException
	{
		System.out.println("in  searchBrand 1");
		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String ajax = request.getParameter("ajax");
		boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);
		request.setAttribute("navigator", "navBRAND");
        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
			"navBRAND",
			request,
			brandDAO,
			"viewBRAND",
			null,
			tableName,
			isPermanent,
			userDTO,
			filter,
			true);
        try
        {
			System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        }
        catch(Exception e)
        {
			System.out.println("failed to dojob" + e);
        }

		request.setAttribute("brandDAO",brandDAO);
        RequestDispatcher rd;
        if(!isPermanent)
        {
        	if(hasAjax == false)
	        {
	        	System.out.println("Going to approval_path/commonApproval.jsp");
	        	rd = request.getRequestDispatcher("approval_path/commonApproval.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to brand/brandApprovalForm.jsp");
	        	rd = request.getRequestDispatcher("brand/brandApprovalForm.jsp");
	        }
        }
        else
        {
	        if(hasAjax == false)
	        {
	        	System.out.println("Going to pb/search.jsp");
	        	rd = request.getRequestDispatcher("pb/search.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to brand/brandSearchForm.jsp");
	        	rd = request.getRequestDispatcher("brand/brandSearchForm.jsp");
	        }
        }
		rd.forward(request, response);
	}
	
}

