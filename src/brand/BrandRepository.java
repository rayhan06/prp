package brand;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class BrandRepository implements Repository {
	BrandDAO brandDAO = null;
	
	public void setDAO(BrandDAO brandDAO)
	{
		this.brandDAO = brandDAO;
	}
	
	
	static Logger logger = Logger.getLogger(BrandRepository.class);
	Map<Long, BrandDTO>mapOfBrandDTOToiD;

  
	private BrandRepository(){
		brandDAO = new BrandDAO();
		mapOfBrandDTOToiD = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	private static class LazyLoader{
        final static BrandRepository INSTANCE = new BrandRepository();
    }

    public static BrandRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

	public void reload(boolean reloadAll){
		try {
			List<BrandDTO> brandDTOs = (List<BrandDTO>)brandDAO.getAll(reloadAll);
			for(BrandDTO brandDTO : brandDTOs) {
				BrandDTO oldBrandDTO = getBrandDTOByID(brandDTO.iD);
				if( oldBrandDTO != null ) {
					mapOfBrandDTOToiD.remove(oldBrandDTO.iD);
				
					
				}
				if(brandDTO.isDeleted == 0) 
				{
					
					mapOfBrandDTOToiD.put(brandDTO.iD, brandDTO);
				
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<BrandDTO> getBrandList() {
		List <BrandDTO> brands = new ArrayList<BrandDTO>(this.mapOfBrandDTOToiD.values());
		return brands;
	}
	
	
	public BrandDTO getBrandDTOByID( long ID){
		return mapOfBrandDTOToiD.get(ID);
	}

	
	@Override
	public String getTableName() {
		return "brand";
	}


	public String getBrandText(long ID, boolean isLanguageEnglish) {
		BrandDTO dto = mapOfBrandDTOToiD.get(ID);

		return dto==null ? "" : isLanguageEnglish ? dto.nameEn : dto.nameBn;
	}
}


