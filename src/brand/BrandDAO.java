package brand;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import sessionmanager.SessionConstants;
import java.sql.SQLException;
import common.ConnectionAndStatementUtil;
import org.apache.log4j.Logger;
import repository.RepositoryManager;
import util.*;
import pb.*;
import user.UserDTO;

public class BrandDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());

	public BrandDAO(String tableName)
	{
		super(tableName);
		useSafeSearch = true;
		joinSQL = "";
		columnNames = new String[] 
		{
			"ID",
			"name_en",
			"name_bn",
			"asset_category_type",
			"search_column",
			"inserted_by_user_id",
			"inserted_by_organogram_id",
			"insertion_date",
			"last_modifier_user",
			"isDeleted",
			"lastModificationTime"
		};
	}
	
	public BrandDAO()
	{
		this("brand");		
	}
	
	public void setSearchColumn(BrandDTO brandDTO)
	{
		brandDTO.searchColumn = "";
		brandDTO.searchColumn += brandDTO.nameEn + " ";
		brandDTO.searchColumn += brandDTO.nameBn + " ";
		brandDTO.searchColumn += CommonDAO.getName("English", "asset_category", brandDTO.assetCategoryType) + " " + CommonDAO.getName("Bangla", "asset_category", brandDTO.assetCategoryType) + " ";
	}
	
	public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException
	{
		BrandDTO brandDTO = (BrandDTO)commonDTO;
		int index = 0;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(brandDTO);
		if(isInsert)
		{
			ps.setObject(++index,brandDTO.iD);
		}
		ps.setObject(++index,brandDTO.nameEn);
		ps.setObject(++index,brandDTO.nameBn);
		ps.setObject(++index,brandDTO.assetCategoryType);
		ps.setObject(++index,brandDTO.searchColumn);
		ps.setObject(++index,brandDTO.insertedByUserId);
		ps.setObject(++index,brandDTO.insertedByOrganogramId);
		ps.setObject(++index,brandDTO.insertionDate);
		ps.setObject(++index,brandDTO.lastModifierUser);
		if(isInsert)
		{
			ps.setObject(++index, 0);
			
		}
		ps.setObject(++index, lastModificationTime);
	}
	
    public long update(CommonDTO commonDTO) throws Exception {
        long lastModificationTime = System.currentTimeMillis();
        StringBuilder sqlBuilder = new StringBuilder("UPDATE ").append(tableName).append(" SET ");
        for (String columnName : columnNames) {
            if (!columnName.equals("ID") && !columnName.equals("isDeleted")) {
                sqlBuilder.append(columnName).append(" = ?, ");
            }
        }
        sqlBuilder.append("lastModificationTime = ").append(lastModificationTime);
        sqlBuilder.append(" WHERE ID = ").append(commonDTO.iD);
        return (Long) ConnectionAndStatementUtil.getWriteConnectionAndPrepareStatement(model -> {
            Connection connection = model.getConnection();
            PreparedStatement ps = (PreparedStatement) model.getStatement();
            try {
                doSet(ps, commonDTO, false);
                logger.debug(ps);
                ps.executeUpdate();
                recordUpdateTime(connection, lastModificationTime);
                return commonDTO.iD;
            } catch (SQLException ex) {
                logger.error(ex);
                return -1L;
            }
        }, sqlBuilder.toString());
    }
	
	public BrandDTO build(ResultSet rs)
	{
		try
		{
			BrandDTO brandDTO = new BrandDTO();
			int i = 0;
			brandDTO.iD = rs.getLong(columnNames[i++]);
			brandDTO.nameEn = rs.getString(columnNames[i++]);
			brandDTO.nameBn = rs.getString(columnNames[i++]);
			brandDTO.assetCategoryType = rs.getLong(columnNames[i++]);
			brandDTO.searchColumn = rs.getString(columnNames[i++]);
			brandDTO.insertedByUserId = rs.getLong(columnNames[i++]);
			brandDTO.insertedByOrganogramId = rs.getLong(columnNames[i++]);
			brandDTO.insertionDate = rs.getLong(columnNames[i++]);
			brandDTO.lastModifierUser = rs.getString(columnNames[i++]);
			brandDTO.isDeleted = rs.getInt(columnNames[i++]);
			brandDTO.lastModificationTime = rs.getLong(columnNames[i++]);
			return brandDTO;
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
			logger.error(ex);
			return null;
		}
	}
		

	public String getSqlWithSearchCriteriaForNormalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
			UserDTO userDTO, String filter, boolean tableHasJobCat, List<Object> objectList)
    {

		String sql = "SELECT ";
		if(category == GETIDS)
		{
			sql += " " + tableName + ".ID as ID ";
		}
		else if(category == GETCOUNT)
		{
			sql += " count( " + tableName + ".ID) as countID ";
		}
		else if(category == GETDTOS)
		{
			sql += " distinct " + tableName + ".* ";
		}
		sql += "FROM " + tableName + " ";
		sql += joinSQL;
		

		
		String AnyfieldSql = "";
		String AllFieldSql = "";
		
		if(p_searchCriteria != null)
		{
						
			
			Enumeration names = p_searchCriteria.keys();
			String str, value;
			
			AnyfieldSql = "(";
			
			if(p_searchCriteria.get("AnyField")!= null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase(""))
			{
				AnyfieldSql+= tableName + ".search_column like ?";
				objectList.add("%" + p_searchCriteria.get("AnyField").toString() + "%");			
			}
			AnyfieldSql += ")";
			System.out.println("AnyfieldSql = " + AnyfieldSql);
			
			AllFieldSql = "(";
			int i = 0;
			while(names.hasMoreElements())
			{				
				str = (String) names.nextElement();
				value = (String)p_searchCriteria.get(str);
		        System.out.println(str + ": " + value);
				if(value != null && !value.equalsIgnoreCase("") && (
						 str.equals("name_en")
						|| str.equals("name_bn")
						|| str.equals("asset_category_type")
						|| str.equals("insertion_date_start")
						|| str.equals("insertion_date_end")
				)
						
				)
				{
					if(p_searchCriteria.get(str).equals("any"))
					{
						continue;
					}

					if( i > 0)
					{
						AllFieldSql+= " AND  ";
					}
					
					 if(str.equals("name_en"))
					{
						AllFieldSql += "" + tableName + ".name_en like ?";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("name_bn"))
					{
						AllFieldSql += "" + tableName + ".name_bn like ?";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("asset_category_type"))
					{
						AllFieldSql += "" + tableName + ".asset_category_type = ?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
					else if(str.equals("insertion_date_start"))
					{
						AllFieldSql += "" + tableName + ".insertion_date >= ?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
					else if(str.equals("insertion_date_end"))
					{
						AllFieldSql += "" + tableName + ".insertion_date <= ?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
					
					
				}
			}
			
			AllFieldSql += ")";
			System.out.println("AllFieldSql = " + AllFieldSql);
			
			
		}
		
		
		sql += " WHERE ";
		
		sql += " (" + tableName + ".isDeleted = 0 ";			
		sql += ")";
		
		
		if(!filter.equalsIgnoreCase(""))
		{
			sql += " and " + filter + " ";
		}
		
		if(!AnyfieldSql.equals("()") && !AnyfieldSql.equals(""))
		{
			sql += " AND " + AnyfieldSql;
			
		}
		if(!AllFieldSql.equals("()") && !AllFieldSql.equals(""))
		{			
			sql += " AND " + AllFieldSql;
		}
		
	
			
		
		if(category == GETDTOS)
		{
			sql += " order by " + tableName + ".lastModificationTime desc ";
		}

		//printSql(sql);
		
		if(limit >= 0)
		{
			sql += " limit " + limit;
		}
		if(offset >= 0)
		{
			sql += " offset " + offset;
		}

		
		return sql;
    }
	
				
}
	