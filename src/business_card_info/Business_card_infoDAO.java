package business_card_info;

import card_info.CardEmployeeInfoDTO;
import card_info.CardEmployeeInfoRepository;
import card_info.CardEmployeeOfficeInfoDTO;
import card_info.CardEmployeeOfficeInfoRepository;
import common.EmployeeCommonDAOService;
import org.apache.log4j.Logger;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

@SuppressWarnings({"rawtypes", "unused", "unchecked", "Duplicates"})
public class Business_card_infoDAO implements EmployeeCommonDAOService<Business_card_infoDTO> {
    private static final Logger logger = Logger.getLogger(Business_card_infoDAO.class);

    private static final String updateQuery = "UPDATE {tableName} SET card_status_cat = ? , modified_by = ?, lastModificationTime = ?,search_column=?,comment=?  WHERE ID = ?";

    private static final String addQuery = "INSERT INTO {tableName} (card_status_cat," +
            " modified_by, lastModificationTime,search_column,total_count,employee_records_id," +
            " card_employee_office_info_id,card_employee_info_id," +
            " inserted_by, insertion_time, insert_by_name_en, insert_by_name_bn,insert_by_office_id,insert_by_office_en,insert_by_office_bn," +
            " insert_by_designation_id,insert_by_designation_en,insert_by_designation_bn,isDeleted,task_type_id, ID)" +
            " VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

    public void set(PreparedStatement ps, Business_card_infoDTO card_infoDTO, boolean isInsert) throws SQLException {
        int index = 0;
        setSearchColumn(card_infoDTO);
        ps.setLong(++index, card_infoDTO.cardStatusCat);
        ps.setLong(++index, card_infoDTO.modifiedBy);
        ps.setLong(++index, card_infoDTO.lastModificationTime);
        ps.setString(++index, card_infoDTO.searchColumn);
        if (isInsert) {
            ps.setLong(++index, card_infoDTO.totalCount);
            ps.setLong(++index, card_infoDTO.employeeRecordsId);
            ps.setLong(++index, card_infoDTO.cardEmployeeOfficeInfoId);
            ps.setLong(++index, card_infoDTO.cardEmployeeInfoId);
            ps.setLong(++index, card_infoDTO.insertedBy);
            ps.setLong(++index, card_infoDTO.insertionTime);
            ps.setString(++index, card_infoDTO.insertByNameEng);
            ps.setString(++index, card_infoDTO.insertByNameBng);
            ps.setLong(++index, card_infoDTO.insertByOfficeUnitId);
            ps.setString(++index, card_infoDTO.insertByOfficeUnitEng);
            ps.setString(++index, card_infoDTO.insertByOfficeUnitBng);
            ps.setLong(++index, card_infoDTO.insertByOfficeUnitOrganogramId);
            ps.setString(++index, card_infoDTO.insertByOfficeUnitOrganogramEng);
            ps.setString(++index, card_infoDTO.insertByOfficeUnitOrganogramBng);
            ps.setInt(++index, 0);
            ps.setLong(++index, card_infoDTO.taskTypeId);
        } else {
            ps.setString(++index, card_infoDTO.comment);
        }
        ps.setLong(++index, card_infoDTO.iD);
    }

    public Business_card_infoDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Business_card_infoDTO card_infoDTO = new Business_card_infoDTO();
            card_infoDTO.totalCount = rs.getInt("total_count");
            card_infoDTO.iD = rs.getLong("ID");
            card_infoDTO.cardStatusCat = rs.getInt("card_status_cat");
            card_infoDTO.cardEmployeeOfficeInfoId = rs.getLong("card_employee_office_info_id");
            card_infoDTO.cardEmployeeInfoId = rs.getLong("card_employee_info_id");
            card_infoDTO.insertedBy = rs.getLong("inserted_by");
            card_infoDTO.insertionTime = rs.getLong("insertion_time");
            card_infoDTO.insertByNameEng = rs.getString("insert_by_name_en");
            card_infoDTO.insertByNameBng = rs.getString("insert_by_name_bn");
            card_infoDTO.insertByOfficeUnitId = rs.getLong("insert_by_office_id");
            card_infoDTO.insertByOfficeUnitEng = rs.getString("insert_by_office_en");
            card_infoDTO.insertByOfficeUnitBng = rs.getString("insert_by_office_bn");
            card_infoDTO.insertByOfficeUnitOrganogramId = rs.getLong("insert_by_designation_id");
            card_infoDTO.insertByOfficeUnitOrganogramEng = rs.getString("insert_by_designation_en");
            card_infoDTO.insertByOfficeUnitOrganogramBng = rs.getString("insert_by_designation_bn");
            card_infoDTO.isDeleted = rs.getInt("isDeleted");
            card_infoDTO.modifiedBy = rs.getLong("modified_by");
            card_infoDTO.lastModificationTime = rs.getLong("lastModificationTime");
            card_infoDTO.searchColumn = rs.getString("search_column");
            card_infoDTO.taskTypeId = rs.getLong("task_type_id");
            card_infoDTO.comment = rs.getString("comment");
            return card_infoDTO;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    private static final String tableName = "business_card_info";
    private static final Map<String, String> searchMap = new HashMap<>();

    private static class LazyLoader {
        static final Business_card_infoDAO INSTANCE = new Business_card_infoDAO();
    }

    public static Business_card_infoDAO getInstance() {
        return Business_card_infoDAO.LazyLoader.INSTANCE;
    }


    private Business_card_infoDAO() {
        searchMap.put("card_status_cat", " and (card_status_cat = ?)");
        searchMap.put("employee_records_id_internal", " and (employee_records_id = ? OR inserted_by = ?)");
        searchMap.put("AnyField", " and (search_column like ?)");
    }

    public void setSearchColumn(Business_card_infoDTO card_infoDTO) {
        card_infoDTO.searchColumn = "";
        CardEmployeeOfficeInfoDTO cardEmployeeOfficeInfoDTO = CardEmployeeOfficeInfoRepository.getInstance().getById(card_infoDTO.cardEmployeeOfficeInfoId);
        if (cardEmployeeOfficeInfoDTO != null) {
            card_infoDTO.searchColumn = cardEmployeeOfficeInfoDTO.officeUnitEng.toLowerCase();
            if (!card_infoDTO.searchColumn.contains(cardEmployeeOfficeInfoDTO.officeUnitBng.toLowerCase())) {
                card_infoDTO.searchColumn += " " + cardEmployeeOfficeInfoDTO.officeUnitBng.toLowerCase();
            }
            if (!card_infoDTO.searchColumn.contains(cardEmployeeOfficeInfoDTO.organogramEng.toLowerCase())) {
                card_infoDTO.searchColumn += " " + cardEmployeeOfficeInfoDTO.organogramEng.toLowerCase();
            }
            if (!card_infoDTO.searchColumn.contains(cardEmployeeOfficeInfoDTO.organogramBng.toLowerCase())) {
                card_infoDTO.searchColumn += " " + cardEmployeeOfficeInfoDTO.organogramBng.toLowerCase();
            }
        }
        CardEmployeeInfoDTO employeeDTO = CardEmployeeInfoRepository.getInstance().getById(card_infoDTO.cardEmployeeInfoId);
        if (employeeDTO != null) {
            if (!card_infoDTO.searchColumn.contains(employeeDTO.nameEn.toLowerCase())) {
                card_infoDTO.searchColumn += " " + employeeDTO.nameEn.toLowerCase();
            }
            if (!card_infoDTO.searchColumn.contains(employeeDTO.nameBn.toLowerCase())) {
                card_infoDTO.searchColumn += " " + employeeDTO.nameBn.toLowerCase();
            }
        }
    }

    @Override
    public String getTableName() {
        return tableName;
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Business_card_infoDTO) commonDTO, addQuery, true);
    }


    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Business_card_infoDTO) commonDTO, updateQuery, false);
    }


    public void updateCardStatus(long cardInfoId, int statusValue, long modifiedBy, long modifiedTime) throws Exception {
        Business_card_infoDTO card_infoDTO = getDTOFromID(cardInfoId);
        if (card_infoDTO == null)
            return;
        card_infoDTO.cardStatusCat = statusValue;
        card_infoDTO.modifiedBy = modifiedBy;
        card_infoDTO.lastModificationTime = modifiedTime;
        update(card_infoDTO);
    }
}