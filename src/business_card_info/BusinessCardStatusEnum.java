package business_card_info;

import pb.CatRepository;
import pb.CategoryLanguageModel;
import pb.OptionDTO;
import pb.Utils;

import java.util.List;
import java.util.stream.Collectors;

public enum BusinessCardStatusEnum {
    WAITING_FOR_APPROVAL(1),
    APPROVED(2),
    REJECTED(3),
    PRINTING(4),
    READY_FOR_DELIVER(5),
    DELIVERED(6);
    private final int value;

    BusinessCardStatusEnum(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public static String getColor(int value) {
        if (value == BusinessCardStatusEnum.WAITING_FOR_APPROVAL.getValue()) {
            return "#22ccc1";
        } else if (value == BusinessCardStatusEnum.APPROVED.getValue()) {
            return "green";
        } else if (value == BusinessCardStatusEnum.REJECTED.getValue()) {
            return "crimson";
        } else if (value == BusinessCardStatusEnum.PRINTING.getValue()) {
            return "darkcyan";
        } else if (value == BusinessCardStatusEnum.READY_FOR_DELIVER.getValue()) {
            return "darkgreen";
        } else if (value == BusinessCardStatusEnum.DELIVERED.getValue()) {
            return "forestgreen";
        } else {
            return "red";
        }
    }

    public static String buildNextOptions(int currentStatus, String language) {
        if (currentStatus == REJECTED.value || currentStatus == DELIVERED.value || currentStatus == WAITING_FOR_APPROVAL.value) {
            return null;
        }
        List<CategoryLanguageModel> list = CatRepository.getInstance().getCategoryLanguageModelList("business_card_status");
        List<OptionDTO> optionDTOList = list.stream()
                .filter(e -> e.categoryValue > currentStatus && e.categoryValue != REJECTED.value)
                .map(e -> new OptionDTO(e.englishText, e.banglaText, String.valueOf(e.categoryValue)))
                .collect(Collectors.toList());
        return Utils.buildOptions(optionDTOList, language, null);
    }
}
