create table business_card_info
(
	ID bigint(20) unsigned not null
		primary key,
	card_status_cat int(2) default 0 null,
	card_employee_office_info_id bigint(20) null,
	next_card_approval_id bigint(20) null,
	card_employee_info_id bigint(20) null,
	total_count int(11) null,
	inserted_by bigint(20) null,
	insertion_time bigint(20) default -62135791200000 null,
	isDeleted int(11) default 0 null,
	modified_by bigint(20) null,
	lastModificationTime bigint(20) default -62135791200000 null,
	employee_records_id bigint(20) not null,
	task_type_id int(11) not null,
	search_column varchar(1000) null
)
engine=MyISAM collate=utf8_unicode_ci;

