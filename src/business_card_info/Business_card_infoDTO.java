package business_card_info;

import util.CommonEmployeeDTO;

public class Business_card_infoDTO extends CommonEmployeeDTO {
	public int totalCount = 0;
	public int cardStatusCat = 0;
	public long cardEmployeeOfficeInfoId = 0;
	public long cardEmployeeInfoId = 0;
	public long insertedBy = 0;
	public long insertionTime = 0;
	public long modifiedBy = 0;
	public long taskTypeId = 0;
	public String insertByNameEng = "";
	public String insertByNameBng = "";
	public long insertByOfficeUnitId = 0;
	public String insertByOfficeUnitEng = "";
	public String insertByOfficeUnitBng = "";
	public long   insertByOfficeUnitOrganogramId = 0;
	public String insertByOfficeUnitOrganogramEng = "";
	public String insertByOfficeUnitOrganogramBng = "";
	public String comment = "";
}