package business_card_info;

import business_card_approval_mapping.BusinessCardApprovalNotification;
import business_card_approval_mapping.Business_card_approval_mappingDAO;
import business_card_approval_mapping.CreateBusinessCardApprovalModel;
import card_info.*;
import common.*;
import employee_offices.EmployeeOfficeDTO;
import employee_offices.EmployeeOfficeRepository;
import employee_records.Employee_recordsDTO;
import employee_records.Employee_recordsRepository;
import office_unit_organograms.OfficeUnitOrganograms;
import office_unit_organograms.OfficeUnitOrganogramsRepository;
import office_units.Office_unitsDTO;
import office_units.Office_unitsRepository;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import pb.Utils;
import permission.MenuConstants;
import role.PermissionRepository;
import task_type.TaskTypeEnum;
import user.UserDTO;
import util.CommonConstant;
import util.CommonDTO;
import util.HttpRequestUtils;
import util.LockManager;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;

@SuppressWarnings("ALL")
@WebServlet("/Business_card_infoServlet")
@MultipartConfig
public class Business_card_infoServlet extends BaseServlet implements EmployeeServletService {
    private static final long serialVersionUID = 1L;
    private static final Logger logger = Logger.getLogger(Business_card_infoServlet.class);
    private static final String tableName = "business_card_info";
    private final Business_card_infoDAO business_card_infoDAO = Business_card_infoDAO.getInstance();


    @Override
    public String getTableName() {
        return tableName;
    }

    @Override
    public String getServletName() {
        return "Business_card_infoServlet";
    }

    @Override
    public Business_card_infoDAO getCommonDAOService() {
        return business_card_infoDAO;
    }

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        boolean isLangEng = userDTO.languageID == CommonConstant.Language_ID_English;
        long employeeRecordId = Long.parseLong(request.getParameter("employeeRecordId"));
        Business_card_infoDTO dto = new Business_card_infoDTO();
        CardEmployeeOfficeInfoDTO cardEmployeeOfficeInfoDTO = CardEmployeeOfficeInfoRepository.getInstance().getByEmployeeRecordId(employeeRecordId, userDTO.ID);
        dto.cardEmployeeOfficeInfoId = cardEmployeeOfficeInfoDTO.iD;
        CardEmployeeInfoDTO cardEmployeeInfoDTO = CardEmployeeInfoRepository.getInstance().getByEmployeeRecordId(employeeRecordId, userDTO.ID);
        dto.cardEmployeeInfoId = cardEmployeeInfoDTO.iD;
        dto.employeeRecordsId = employeeRecordId;
        dto.totalCount = Integer.parseInt(Jsoup.clean(request.getParameter("totalCount"), Whitelist.simpleText()));
        if (dto.totalCount < 1)
            throw new Exception(
                    isLangEng ? "Invalid Number of Business Cards"
                            : "ভুল বিজনেস কার্ডের সংখ্যা"
            );

        dto.cardStatusCat = BusinessCardStatusEnum.WAITING_FOR_APPROVAL.getValue();
        dto.insertedBy = dto.modifiedBy = userDTO.employee_record_id;
        dto.insertionTime = dto.lastModificationTime = System.currentTimeMillis();
        Employee_recordsDTO employee_recordsDTO = Employee_recordsRepository.getInstance().getById(userDTO.employee_record_id);
        if (employee_recordsDTO != null) {
            dto.insertByNameEng = employee_recordsDTO.nameEng;
            dto.insertByNameBng = employee_recordsDTO.nameBng;
        }
        EmployeeOfficeDTO employeeOfficeDTO = EmployeeOfficeRepository.getInstance().getByEmployeeRecordIdIsDefault(userDTO.employee_record_id);
        if (employeeOfficeDTO != null) {
            Office_unitsDTO officeUnitsDTO = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(employeeOfficeDTO.officeUnitId);
            if (officeUnitsDTO != null) {
                dto.insertByOfficeUnitId = officeUnitsDTO.iD;
                dto.insertByOfficeUnitEng = officeUnitsDTO.unitNameEng;
                dto.insertByOfficeUnitBng = officeUnitsDTO.unitNameBng;
            }
            OfficeUnitOrganograms officeUnitOrganograms = OfficeUnitOrganogramsRepository.getInstance().getById(employeeOfficeDTO.officeUnitOrganogramId);
            if (officeUnitOrganograms != null) {
                dto.insertByOfficeUnitOrganogramId = officeUnitOrganograms.id;
                dto.insertByOfficeUnitOrganogramEng = officeUnitOrganograms.designation_eng;
                dto.insertByOfficeUnitOrganogramBng = officeUnitOrganograms.designation_bng;
            }
        }

        Connection connection = ConnectionAndStatementUtil.getConnection(ConnectionType.WRITE);
        try{
            connection.setAutoCommit(false);
            CommonDAOService.CONNECTION_THREAD_LOCAL.set(connection);
            business_card_infoDAO.add(dto);
            CreateBusinessCardApprovalModel model = new CreateBusinessCardApprovalModel.CreateBusinessCardApprovalModelBuilder()
                    .setCardInfoDTO(dto)
                    .setTaskTypeId(TaskTypeEnum.BUSINESS_CARD.getValue())
                    .setRequesterEmployeeRecordId(employeeRecordId)
                    .build();
            CardApprovalResponse cardApprovalResponse = Business_card_approval_mappingDAO.getInstance().createCardApproval(model);
            BusinessCardApprovalNotification.getInstance().sendPendingNotification(cardApprovalResponse.organogramIds, dto);
            connection.commit();
        }catch (Exception ex){
            connection.rollback();
            throw ex;
        }finally {
            connection.setAutoCommit(true);
            CommonDAOService.CONNECTION_THREAD_LOCAL.remove();
            ConnectionAndStatementUtil.closeConnection(connection,ConnectionType.WRITE);
        }


        return dto;
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[0];
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[0];
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[0];
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Business_card_infoServlet.class;
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }
        try {
            switch (request.getParameter("actionType")) {
                case "getAddPage":
                    if (Utils.checkPermission(String.valueOf(userDTO.employee_record_id), userDTO, MenuConstants.BUSINESS_CARD_INFO_ADD)) {
                        if (userDTO.roleID == RoleEnum.EMPLOYEE_OF_PARLIAMENT.getRoleId()) {
                            request.setAttribute("employeeRecordId", userDTO.employee_record_id);
                            request.setAttribute("isAdmin", false);
                        } else {
                            request.setAttribute("isAdmin", true);
                            Employee_recordsDTO dto = Employee_recordsRepository.getInstance().getById(userDTO.employee_record_id);
                            if (dto != null) {
                                request.setAttribute("employeeRecordId", userDTO.employee_record_id);
                            }
                        }
                        request.getRequestDispatcher("business_card_info/business_card_info.jsp").forward(request, response);
                        return;
                    }
                    break;
                case "search": {
                    search(request);
                    super.doGet(request, response);
                    return;
                }
                default:
                    super.doGet(request, response);
                    return;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error(ex);
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);


    }


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }

        try {
            String actionType = request.getParameter("actionType");
            if ("ajax_addBusinessCard".equals(actionType)) {
                if (Utils.checkPermission(String.valueOf(userDTO.employee_record_id), userDTO, MenuConstants.BUSINESS_CARD_INFO_ADD)) {
                    long employeeRecordId = Long.parseLong(request.getParameter("employeeRecordId"));
                    if (userDTO.roleID == RoleEnum.EMPLOYEE_OF_PARLIAMENT.getRoleId() && userDTO.employee_record_id != employeeRecordId) {
                    } else {
                        try {
                            addT(request, true, userDTO);
                            ApiResponse.sendSuccessResponse(response, "Business_card_infoServlet?actionType=search");
                        } catch (Exception ex) {
                            logger.error(ex);
                            ApiResponse.sendErrorResponse(response, ex.getMessage());
                        }
                        return;
                    }
                }
            } else if ("cardStatusChange".equals(actionType)) {
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.BUSINESS_CARD_APPROVAL_MAPPING_SEARCH)) {
                    long cardInfoId = Long.parseLong(request.getParameter("cardInfoId"));
                    int cardStatusValue = Integer.parseInt(request.getParameter("cardStatusValue"));
                    Business_card_infoDTO dto = business_card_infoDAO.getDTOFromID(cardInfoId);
                    if (dto.cardStatusCat < cardStatusValue) {
                        synchronized (LockManager.getLock(cardInfoId + "BCICII")) {
                            dto = business_card_infoDAO.getDTOFromID(cardInfoId);
                            boolean isOldStatusAccepted = dto.cardStatusCat == BusinessCardStatusEnum.APPROVED.getValue();
                            if (dto.cardStatusCat < cardStatusValue) {
                                dto.cardStatusCat = cardStatusValue;
                                dto.modifiedBy = userDTO.employee_record_id;
                                dto.lastModificationTime = System.currentTimeMillis();
                                business_card_infoDAO.update(dto);
                            }
                            if (isOldStatusAccepted) {
                                CreateBusinessCardApprovalModel model = new CreateBusinessCardApprovalModel.CreateBusinessCardApprovalModelBuilder()
                                        .setCardInfoDTO(dto)
                                        .setTaskTypeId(TaskTypeEnum.BUSINESS_CARD.getValue())
                                        .setRequesterEmployeeRecordId(userDTO.employee_record_id)
                                        .build();
                                try {
                                    Business_card_approval_mappingDAO.getInstance().approveOfCurrentLevel(model);
                                } catch (InvalidDataException | AlreadyApprovedException ex) {
                                    logger.error(ex);
                                }
                            }
                            Business_card_approval_mappingDAO.getInstance().updateCardStatusByBusinessCardInfoId(cardStatusValue, cardInfoId);
                        }
                    }
                    response.sendRedirect("Business_card_approval_mappingServlet?actionType=search");
                    return;
                }
            } else {
                super.doPost(request, response);
                return;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error(ex);
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }


}