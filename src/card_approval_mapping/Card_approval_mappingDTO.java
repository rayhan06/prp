package card_approval_mapping;

import util.CommonEmployeeDTO;


public class Card_approval_mappingDTO extends CommonEmployeeDTO {
    public long id = 0;
    public long cardInfoId = 0;
    public long cardApprovalId = 0;
    public int sequence = 0;
    public int cardCat = 0;
    public long insertedBy = 0;
    public long insertionTime = 0;
    public long modifiedBy = 0;
    public int cardApprovalStatusCat = 0;
    public long approverEmployeeRecordsId = 0;
    public long taskTypeId = 0;

    @Override
    public String toString() {
        return "Card_approval_mappingDTO{" +
                "id=" + id +
                ", cardInfoId=" + cardInfoId +
                ", cardApprovalId=" + cardApprovalId +
                ", sequence=" + sequence +
                ", insertedBy=" + insertedBy +
                ", insertionTime=" + insertionTime +
                ", modifiedBy=" + modifiedBy +
                ", cardApprovalStatusCat=" + cardApprovalStatusCat +
                ", employeeRecordsId=" + employeeRecordsId +
                ", approverEmployeeRecordsId=" + approverEmployeeRecordsId +
                ", taskTypeId=" + taskTypeId +
                ", iD=" + iD +
                ", lastModificationTime=" + lastModificationTime +
                '}';
    }
}