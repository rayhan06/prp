package card_approval_mapping;

/*
 * @author Md. Erfan Hossain
 * @created 07/05/2021 - 12:31 AM
 * @project parliament
 */

import card_info.CardCategoryEnum;
import card_info.Card_infoDTO;

public class CreateCardApprovalModel {
    private final CardCategoryEnum cardCategoryEnum;
    private final long taskTypeId;
    private final Card_infoDTO cardInfoDTO;
    private final long requesterEmployeeRecordId;

    private CreateCardApprovalModel(CreateCardApprovalModelBuilder builder){
        cardCategoryEnum = builder.cardCategoryEnum;
        taskTypeId = builder.taskTypeId;
        cardInfoDTO = builder.cardInfoDTO;
        requesterEmployeeRecordId = builder.requesterEmployeeRecordId;
    }

    public CardCategoryEnum getCardCategoryEnum() {
        return cardCategoryEnum;
    }

    public long getTaskTypeId() {
        return taskTypeId;
    }

    public Card_infoDTO getCardInfoDTO() {
        return cardInfoDTO;
    }

    public long getRequesterEmployeeRecordId() {
        return requesterEmployeeRecordId;
    }

    public static class CreateCardApprovalModelBuilder{
        private CardCategoryEnum cardCategoryEnum;
        private long taskTypeId;
        private Card_infoDTO cardInfoDTO;
        private long requesterEmployeeRecordId;

        public CreateCardApprovalModelBuilder setCardCategoryEnum(CardCategoryEnum cardCategoryEnum) {
            this.cardCategoryEnum = cardCategoryEnum;
            return this;
        }

        public CreateCardApprovalModelBuilder setTaskTypeId(long taskTypeId) {
            this.taskTypeId = taskTypeId;
            return this;
        }

        public CreateCardApprovalModelBuilder setCardInfoDTO(Card_infoDTO cardInfoDTO) {
            this.cardInfoDTO = cardInfoDTO;
            return this;
        }

        public CreateCardApprovalModelBuilder setRequesterEmployeeRecordId(long requesterEmployeeRecordId) {
            this.requesterEmployeeRecordId = requesterEmployeeRecordId;
            return this;
        }

        public CreateCardApprovalModel build(){
            return new CreateCardApprovalModel(this);
        }
    }
}
