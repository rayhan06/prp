package card_approval_mapping;

import card_info.*;
import common.ConnectionAndStatementUtil;
import common.EmployeeCommonDAOService;
import employee_offices.EmployeeOfficeDTO;
import employee_offices.EmployeeOfficeRepository;
import office_unit_organograms.OfficeUnitOrganograms;
import office_unit_organograms.OfficeUnitOrganogramsRepository;
import org.apache.log4j.Logger;
import task_type.TaskTypeEnum;
import task_type_approval_path.TaskTypeApprovalPathDTO;
import task_type_approval_path.TaskTypeApprovalPathRepository;
import util.CommonDTO;
import util.LockManager;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;

@SuppressWarnings({"unused", "Duplicates"})
public class Card_approval_mappingDAO implements EmployeeCommonDAOService<Card_approval_mappingDTO> {
    private static final Logger logger = Logger.getLogger(Card_approval_mappingDAO.class);

    private static final String addSqlQuery = "INSERT INTO {tableName} (card_approval_status_cat,sequence,modified_by,lastModificationTime,search_column," +
            "card_info_id,card_cat,card_approval_id,inserted_by,insertion_time,task_type_id," +
            "employee_records_id,approver_employee_records_id,isDeleted,id) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

    private static final String updateSqlQuery = "UPDATE {tableName} SET card_approval_status_cat = ?,sequence = ?,modified_by = ?,lastModificationTime = ?,search_column=?," +
            "isDeleted = ? WHERE id = ?";

    private static final String getByCardInfoId = "SELECT * FROM card_approval_mapping WHERE card_info_id = %d AND isDeleted = 0 ORDER BY sequence DESC";

    private static final String getByCardInfoIdAndApproverEmployeeRecordsId = "SELECT * FROM card_approval_mapping WHERE card_info_id = %d AND approver_employee_records_id = %d";

    private static final String getByCardInfoIdAndStatus = "SELECT * FROM card_approval_mapping WHERE card_info_id = %d AND card_approval_status_cat = %d AND isDeleted = 0";

    private static final String updateStatus = "UPDATE card_approval_mapping SET card_approval_status_cat = %d,isDeleted = %d,modified_by = %d,lastModificationTime = %d WHERE id IN (%s)";

    private static final String updateStatusByCardInfoId = "UPDATE card_approval_mapping SET card_approval_status_cat = ?,comment= ?,isDeleted =?," +
            "modified_by = ?,lastModificationTime = ? WHERE card_approval_status_cat = ? AND card_info_id = ?";

    private final Map<String, String> searchMap = new HashMap<>();

    private static Card_approval_mappingDAO INSTANCE = null;

    public static Card_approval_mappingDAO getInstance(){
        if(INSTANCE == null){
            synchronized (Card_approval_mappingDAO.class){
                if(INSTANCE == null){
                    INSTANCE = new Card_approval_mappingDAO();
                }
            }
        }
        return INSTANCE;
    }


    private Card_approval_mappingDAO() {
        searchMap.put("card_approval_status_cat", "and (card_approval_status_cat = ?)");
        searchMap.put("employee_records_id_internal", "and (approver_employee_records_id = ?)");
        searchMap.put("card_cat", "and (card_cat = ?)");
        searchMap.put("AnyField", " and (search_column like ?)");
    }

    public void setSearchColumn(Card_approval_mappingDTO card_approval_mappingDTO) {
        Card_infoDTO card_infoDTO = CardInfoDAO.getInstance().getDTOFromID(card_approval_mappingDTO.cardInfoId);
        CardEmployeeInfoDTO employeeInfoDTO = CardEmployeeInfoRepository.getInstance().getById(card_infoDTO.cardEmployeeInfoId);
        card_approval_mappingDTO.searchColumn = employeeInfoDTO.nameEn.toLowerCase();
        if(!card_approval_mappingDTO.searchColumn.contains(employeeInfoDTO.nameBn.toLowerCase())){
            card_approval_mappingDTO.searchColumn += " "+employeeInfoDTO.nameBn.toLowerCase();
        }
        CardEmployeeOfficeInfoDTO officeInfoDTO = CardEmployeeOfficeInfoRepository.getInstance().getById(card_infoDTO.cardEmployeeOfficeInfoId);
        if(!card_approval_mappingDTO.searchColumn.contains(officeInfoDTO.officeUnitEng.toLowerCase())){
            card_approval_mappingDTO.searchColumn += " " + officeInfoDTO.officeUnitEng.toLowerCase();
        }
        if(!card_approval_mappingDTO.searchColumn.contains(officeInfoDTO.officeUnitBng.toLowerCase())){
            card_approval_mappingDTO.searchColumn += " " + officeInfoDTO.officeUnitBng.toLowerCase();
        }
        if(!card_approval_mappingDTO.searchColumn.contains(officeInfoDTO.organogramEng.toLowerCase())){
            card_approval_mappingDTO.searchColumn += " " + officeInfoDTO.organogramEng.toLowerCase();
        }
        if(!card_approval_mappingDTO.searchColumn.contains(officeInfoDTO.organogramBng.toLowerCase())){
            card_approval_mappingDTO.searchColumn += " " + officeInfoDTO.organogramBng.toLowerCase();
        }
        if(!card_approval_mappingDTO.searchColumn.contains(card_infoDTO.insertByNameEng.toLowerCase())){
            card_approval_mappingDTO.searchColumn += " " + card_infoDTO.insertByNameEng.toLowerCase();
        }
        if(!card_approval_mappingDTO.searchColumn.contains(card_infoDTO.insertByNameBng.toLowerCase())){
            card_approval_mappingDTO.searchColumn += " " + card_infoDTO.insertByNameBng.toLowerCase();
        }
        if(!card_approval_mappingDTO.searchColumn.contains(card_infoDTO.insertByOfficeUnitEng.toLowerCase())){
            card_approval_mappingDTO.searchColumn += " " + card_infoDTO.insertByOfficeUnitEng.toLowerCase();
        }
        if(!card_approval_mappingDTO.searchColumn.contains(card_infoDTO.insertByOfficeUnitBng.toLowerCase())){
            card_approval_mappingDTO.searchColumn += " " + card_infoDTO.insertByOfficeUnitBng.toLowerCase();
        }
        if(!card_approval_mappingDTO.searchColumn.contains(card_infoDTO.insertByOfficeUnitOrganogramEng.toLowerCase())){
            card_approval_mappingDTO.searchColumn += " " + card_infoDTO.insertByOfficeUnitOrganogramEng.toLowerCase();
        }
        if(!card_approval_mappingDTO.searchColumn.contains(card_infoDTO.insertByOfficeUnitOrganogramBng.toLowerCase())){
            card_approval_mappingDTO.searchColumn += " " + card_infoDTO.insertByOfficeUnitOrganogramBng.toLowerCase();
        }
    }

    @Override
    public void set(PreparedStatement ps, Card_approval_mappingDTO dto, boolean isInsert) throws SQLException {
        int index = 0;
        setSearchColumn(dto);
        ps.setInt(++index, dto.cardApprovalStatusCat);
        ps.setInt(++index, dto.sequence);
        ps.setLong(++index, dto.modifiedBy);
        ps.setLong(++index, dto.lastModificationTime);
        ps.setObject(++index, dto.searchColumn);
        if (isInsert) {
            ps.setLong(++index, dto.cardInfoId);
            ps.setInt(++index, dto.cardCat);
            ps.setLong(++index, dto.cardApprovalId);
            ps.setLong(++index, dto.insertedBy);
            ps.setLong(++index, dto.insertionTime);
            ps.setLong(++index, dto.taskTypeId);
            ps.setLong(++index, dto.employeeRecordsId);
            ps.setLong(++index, dto.approverEmployeeRecordsId);
        }
        ps.setInt(++index, dto.isDeleted);
        ps.setLong(++index, dto.iD);
    }


    @Override
    public Card_approval_mappingDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Card_approval_mappingDTO dto = new Card_approval_mappingDTO();
            dto.iD = rs.getLong("id");
            dto.cardInfoId = rs.getLong("card_info_id");
            dto.cardApprovalId = rs.getLong("card_approval_id");
            dto.cardApprovalStatusCat = rs.getInt("card_approval_status_cat");
            dto.sequence = rs.getInt("sequence");
            dto.cardCat = rs.getInt("card_cat");
            dto.taskTypeId = rs.getLong("task_type_id");
            dto.insertedBy = rs.getLong("inserted_by");
            dto.insertionTime = rs.getLong("insertion_time");
            dto.modifiedBy = rs.getLong("modified_by");
            dto.lastModificationTime = rs.getLong("lastModificationTime");
            dto.isDeleted = rs.getInt("isDeleted");
            dto.searchColumn = rs.getString("search_column");
            dto.employeeRecordsId = rs.getInt("employee_records_id");
            dto.approverEmployeeRecordsId = rs.getInt("approver_employee_records_id");
            return dto;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    @Override
    public String getTableName() {
        return "card_approval_mapping";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    @Override
    public long add(CommonDTO dto) throws Exception {
        return executeAddOrUpdateQuery((Card_approval_mappingDTO) dto, addSqlQuery, true);
    }

    @Override
    public long update(CommonDTO dto) throws Exception {
        return executeAddOrUpdateQuery((Card_approval_mappingDTO) dto, updateSqlQuery, false);
    }

    public CardApprovalResponse createCardApproval(CreateCardApprovalModel model) throws Exception {
        String sql = String.format(getByCardInfoId, model.getCardInfoDTO().iD);
        Card_approval_mappingDTO dto = ConnectionAndStatementUtil.getT(sql, this::buildObjectFromResultSet);
        if (dto != null) {
            throw new DuplicateCardInfoException("Approval is already created for " + model.getCardInfoDTO().iD);
        }
        CardApprovalResponse cardApprovalResponse = new CardApprovalResponse();
        EmployeeOfficeDTO employeeOfficeDTO = EmployeeOfficeRepository.getInstance().getByEmployeeRecordIdIsDefault(model.getRequesterEmployeeRecordId());
        OfficeUnitOrganograms officeUnitOrganograms = OfficeUnitOrganogramsRepository.getInstance().getById(employeeOfficeDTO.officeUnitOrganogramId);
        if (model.getCardCategoryEnum() == CardCategoryEnum.TEMPORARY && officeUnitOrganograms !=null && officeUnitOrganograms.orgTree.endsWith("|")) {
            Card_approval_mappingDTO cardApprovalMappingDTO;
            cardApprovalMappingDTO = buildDTO(model.getRequesterEmployeeRecordId(), model, 1, 1, ApprovalStatus.SATISFIED);
            if (cardApprovalMappingDTO == null) {
                throw new CardApproverNotFoundException("Card Approver is not found for " + employeeOfficeDTO.employeeRecordId);
            }
            add(cardApprovalMappingDTO);
            List<TaskTypeApprovalPathDTO> nextApprovalPath = TaskTypeApprovalPathRepository.getInstance().getByTaskTypeIdAndLevel(model.getTaskTypeId(), 2);
            if (nextApprovalPath == null || nextApprovalPath.size() == 0) {
                cardApprovalResponse.hasNextApproval = false;
                cardApprovalResponse.organogramIds = null;
            } else {
                List<Long> organogramIds = add(nextApprovalPath, model, 2);
                cardApprovalResponse.hasNextApproval = true;
                cardApprovalResponse.organogramIds = organogramIds;
            }
        } else {
            TaskTypeApprovalPathDTO taskTypeApprovalPathDTO = new TaskTypeApprovalPathDTO();
            if(officeUnitOrganograms == null){
                taskTypeApprovalPathDTO.officeUnitOrganogramId = 0;
            }else{
                taskTypeApprovalPathDTO.officeUnitOrganogramId = officeUnitOrganograms.superior_designation_id;
            }
            if(taskTypeApprovalPathDTO.officeUnitOrganogramId == 0){
                logger.debug("taskTypeApprovalPathDTO.officeUnitOrganogramId = 0; For that card approval direct moved to next level");
                return movedToNextLevel(model,2);
            }else{
                cardApprovalResponse.hasNextApproval = true;
                cardApprovalResponse.organogramIds = add(Collections.singletonList(taskTypeApprovalPathDTO), model, 1);
            }
        }
        return cardApprovalResponse;
    }

    private Card_approval_mappingDTO buildDTO(long approverEmployeeRecordId, CreateCardApprovalModel model, int level,
                                              int isDeleted, ApprovalStatus approvalStatus) {
        Card_approval_mappingDTO cardApprovalMappingDTO = new Card_approval_mappingDTO();
        CardApprovalDTO cardApprovalDTO = CardApprovalRepository.getInstance().getByEmployeeRecordId(approverEmployeeRecordId, model.getRequesterEmployeeRecordId());
        if (cardApprovalDTO == null) {
            return null;
        }
        cardApprovalMappingDTO.cardInfoId = model.getCardInfoDTO().iD;
        cardApprovalMappingDTO.cardCat = model.getCardCategoryEnum().getValue();
        cardApprovalMappingDTO.cardApprovalId = cardApprovalDTO.iD;
        cardApprovalMappingDTO.cardApprovalStatusCat = approvalStatus.getValue();
        cardApprovalMappingDTO.sequence = level;
        cardApprovalMappingDTO.taskTypeId = model.getTaskTypeId();
        cardApprovalMappingDTO.insertedBy = cardApprovalMappingDTO.modifiedBy = model.getRequesterEmployeeRecordId();
        cardApprovalMappingDTO.insertionTime = cardApprovalMappingDTO.lastModificationTime = System.currentTimeMillis();
        cardApprovalMappingDTO.isDeleted = isDeleted;
        cardApprovalMappingDTO.employeeRecordsId = model.getCardInfoDTO().employeeRecordsId;
        cardApprovalMappingDTO.approverEmployeeRecordsId = cardApprovalDTO.employeeRecordId;
        return cardApprovalMappingDTO;
    }

    private List<Long> add(List<TaskTypeApprovalPathDTO> nextApprovalPath, CreateCardApprovalModel model, int level) {
        long currentTime = System.currentTimeMillis();
        List<Long> addedToApproval = new ArrayList<>();
        nextApprovalPath.forEach(approver -> {
            EmployeeOfficeDTO approverOfficeDTO = EmployeeOfficeRepository.getInstance().getByOfficeUnitOrganogramId(approver.officeUnitOrganogramId);
            if (approverOfficeDTO != null) {
                Card_approval_mappingDTO dto = buildDTO(approverOfficeDTO.employeeRecordId, model, level, 0, ApprovalStatus.PENDING);
                if (dto != null) {
                    try {
                        add(dto);
                        addedToApproval.add(approverOfficeDTO.officeUnitOrganogramId);
                    } catch (Exception e) {
                        logger.error(e);
                    }
                }
            }
        });
        return addedToApproval;
    }

    public CardApprovalResponse movedToNextLevelOfApproval(CreateCardApprovalModel model) throws InvalidDataException, AlreadyApprovedException {
        synchronized (LockManager.getLock(model.getCardInfoDTO().iD+"CCAM")) {
            ValidateResponse validateResponse = validate(model);
            String ids = validateResponse.dtoList.stream()
                    .map(e -> String.valueOf(e.iD))
                    .collect(Collectors.joining(","));
            String sql2 = String.format(updateStatus, ApprovalStatus.SATISFIED.getValue(), 0, model.getRequesterEmployeeRecordId(), System.currentTimeMillis(), ids);

            boolean res = (Boolean) ConnectionAndStatementUtil.getWriteStatement(st -> {
                try {
                    st.executeUpdate(sql2);
                    return true;
                } catch (SQLException ex) {
                    logger.error(ex);
                    return false;
                }
            });
            if (!res) {
                throw new InvalidDataException("Exception occurred during updating approval status");
            }
            return movedToNextLevel(model, validateResponse.currentLevel + 1);
        }
    }

    private CardApprovalResponse movedToNextLevel(CreateCardApprovalModel model, int nextLevel) {
        CardApprovalResponse response = new CardApprovalResponse();
        if(nextLevel == 2 && model.getCardInfoDTO().taskTypeId == TaskTypeEnum.PERMANENT_CARD_FOR_3RD_OR_4TH_CLASS_EMPLOYEE.getValue()
                          && !model.getCardInfoDTO().isPoliceVerificationRequired){
            nextLevel = 3;
        }
        List<TaskTypeApprovalPathDTO> nextApprovalPath = TaskTypeApprovalPathRepository.getInstance().getByTaskTypeIdAndLevel(model.getTaskTypeId(), nextLevel);

        if (nextApprovalPath == null || nextApprovalPath.size() == 0) {
            response.hasNextApproval = false;
            response.organogramIds = null;
        } else {
            List<Long> organogramIds = add(nextApprovalPath, model, nextLevel);
            response.hasNextApproval = true;
            response.organogramIds = organogramIds;
        }
        return response;
    }

    public boolean rejectPendingApproval(CreateCardApprovalModel model,String rejectReason) throws InvalidDataException, AlreadyApprovedException {
        synchronized (LockManager.getLock(model.getCardInfoDTO().iD+"CCAM")) {
            validate(model);
            return (Boolean) ConnectionAndStatementUtil.getWritePrepareStatement(ps -> {
                try {
                    ps.setObject(1,ApprovalStatus.DISSATISFIED.getValue());
                    ps.setObject(2,rejectReason);
                    ps.setObject(3,0);
                    ps.setObject(4,model.getRequesterEmployeeRecordId());
                    ps.setObject(5,System.currentTimeMillis());
                    ps.setObject(6,ApprovalStatus.PENDING.getValue());
                    ps.setObject(7,model.getCardInfoDTO().iD);
                    logger.info(ps);
                    ps.executeUpdate();
                    return true;
                } catch (SQLException ex) {
                    logger.error(ex);
                    return false;
                }
            },updateStatusByCardInfoId);
        }
    }

    private ValidateResponse validate(CreateCardApprovalModel model) throws InvalidDataException, AlreadyApprovedException {
        String sql = String.format(getByCardInfoIdAndStatus, model.getCardInfoDTO().iD, ApprovalStatus.PENDING.getValue());
        List<Card_approval_mappingDTO> dtoList = ConnectionAndStatementUtil.getListOfT(sql, this::buildObjectFromResultSet);
        if (dtoList == null || dtoList.size() == 0) {
            throw new InvalidDataException("No Pending approval is found for cardInfoId : " + model.getCardInfoDTO().iD);
        }
        Card_approval_mappingDTO validRequester = dtoList.stream()
                .filter(dto -> dto.approverEmployeeRecordsId == model.getRequesterEmployeeRecordId())
                .findAny()
                .orElse(null);

        if (validRequester == null) {
            sql = String.format(getByCardInfoIdAndApproverEmployeeRecordsId, model.getCardInfoDTO().iD, model.getRequesterEmployeeRecordId());
            validRequester = ConnectionAndStatementUtil.getT(sql, this::buildObjectFromResultSet);
            if (validRequester != null) {
                throw new AlreadyApprovedException();
            }
            throw new InvalidDataException("Invalid employee request to approve");
        }
        Set<Integer> sequenceValueSet = dtoList.stream()
                .map(e -> e.sequence)
                .collect(Collectors.toSet());
        if (sequenceValueSet.size() > 1) {
            throw new InvalidDataException("Multiple sequence value is found for Pending approval of cardInfoId : " + model.getCardInfoDTO().iD);
        }
        return new ValidateResponse(dtoList, (Integer) sequenceValueSet.toArray()[0], validRequester);
    }

    private static class ValidateResponse {
        List<Card_approval_mappingDTO> dtoList;
        int currentLevel;
        Card_approval_mappingDTO cardApprovalMappingDTO;

        public ValidateResponse(List<Card_approval_mappingDTO> dtoList, int currentLevel, Card_approval_mappingDTO cardApprovalMappingDTO) {
            this.dtoList = dtoList;
            this.currentLevel = currentLevel;
            this.cardApprovalMappingDTO = cardApprovalMappingDTO;
        }
    }

    public Card_approval_mappingDTO getByCardInfoIdAndApproverEmployeeRecordId(long cardInfoId, long approverEmployeeId) {
        String sql = String.format(getByCardInfoIdAndApproverEmployeeRecordsId, cardInfoId, approverEmployeeId);
        return ConnectionAndStatementUtil.getT(sql, this::buildObjectFromResultSet);
    }

    public List<Card_approval_mappingDTO> getAllApprovalDTOByCardInfoId(long cardInfoId) {
        String sql = String.format(getByCardInfoId, cardInfoId);
        List<Card_approval_mappingDTO> list = ConnectionAndStatementUtil.getListOfT(sql, this::buildObjectFromResultSet);
        return list.stream()
                .filter(dto -> dto.cardApprovalStatusCat == ApprovalStatus.PENDING.getValue() || dto.approverEmployeeRecordsId == dto.modifiedBy)
                .collect(Collectors.toList());
    }

    public List<Card_approval_mappingDTO> getAllApprovalDTOByCardInfoIdNotPending(long cardInfoId) {
        String sql = String.format(getByCardInfoId, cardInfoId);
        List<Card_approval_mappingDTO> list = ConnectionAndStatementUtil.getListOfT(sql, this::buildObjectFromResultSet);
        return list.stream()
                .filter(dto -> dto.cardApprovalStatusCat != ApprovalStatus.PENDING.getValue() && dto.approverEmployeeRecordsId == dto.modifiedBy)
                .collect(Collectors.toList());
    }
}
	