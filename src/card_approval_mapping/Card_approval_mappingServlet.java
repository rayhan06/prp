package card_approval_mapping;

import card_info.*;
import common.ApiResponse;
import common.BaseServlet;
import common.CommonDAOService;
import employee_offices.EmployeeOfficeDTO;
import employee_offices.EmployeeOfficeRepository;
import employee_records.Employee_recordsDTO;
import employee_records.Employee_recordsRepository;
import language.LM;
import lost_card_info.Lost_card_infoDAO;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import pb.Utils;
import permission.MenuConstants;
import role.PermissionRepository;
import task_type.TaskTypeEnum;
import user.UserDTO;
import util.CommonDTO;
import util.HttpRequestUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@SuppressWarnings("Duplicates")
@WebServlet("/Card_approval_mappingServlet")
@MultipartConfig
public class Card_approval_mappingServlet extends BaseServlet {
    private static final long serialVersionUID = 1L;

    private static final Logger logger = Logger.getLogger(Card_approval_mappingServlet.class);

    private final Card_approval_mappingDAO cardApprovalMappingDAO = Card_approval_mappingDAO.getInstance();

    @Override
    public String getTableName() {
        return cardApprovalMappingDAO.getTableName();
    }

    @Override
    public String getServletName() {
        return "Card_approval_mappingServlet";
    }

    @Override
    public Card_approval_mappingDAO getCommonDAOService() {
        return cardApprovalMappingDAO;
    }

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        return null;
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[0];
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[0];
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[]{MenuConstants.CARD_APPROVAL_MAPPING_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Card_approval_mappingServlet.class;
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;
        try {
            String actionType = request.getParameter("actionType");
            logger.debug("actionType : "+actionType);
            switch (actionType) {
                case "getApprovalPage":
                    long cardInfoId = Long.parseLong(request.getParameter("cardInfoId"));
                    getCardApprovalModel(cardInfoId, request, userDTO);
                    request.getRequestDispatcher("card_approval_mapping/card_approval_mappingEdit.jsp").forward(request, response);
                    return;
                case "search":
                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.CARD_APPROVAL_MAPPING_SEARCH)) {
                        Map<String,String> extraMap = new HashMap<>();
                        extraMap.put("employee_records_id_internal",String.valueOf(userDTO.employee_record_id));
                        request.setAttribute(CommonDAOService.CRITERIA_MAP_KEY,extraMap);
                        super.doGet(request,response);
                        return;
                    }
                    break;
                case "ajax_approvedCard":
                    if (Utils.checkPermission(userDTO, MenuConstants.CARD_APPROVAL_MAPPING_ADD,MenuConstants.CARD_APPROVAL_MAPPING_SEARCH)) {
                        approve(request, userDTO, true,null,null);
                        response.sendRedirect("Card_approval_mappingServlet?actionType=search");
                        return;
                    }
                    break;
                case "ajax_rejectCard":
                    if (Utils.checkPermission(userDTO, MenuConstants.CARD_APPROVAL_MAPPING_ADD,MenuConstants.CARD_APPROVAL_MAPPING_SEARCH)) {
                        approve(request, userDTO, false, null, null);
                        response.sendRedirect("Card_approval_mappingServlet?actionType=search");
                        return;
                    }
            }
        } catch (Exception ex) {
            logger.debug(ex);
            ex.printStackTrace();
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;
        try {
            switch (request.getParameter("actionType")) {
                case "ajax_approved_card":
                    if (Utils.checkPermission(userDTO, MenuConstants.CARD_APPROVAL_MAPPING_ADD,MenuConstants.CARD_APPROVAL_MAPPING_SEARCH)) {
                        try{
                            approve(request, userDTO, true,null,null);
                            ApiResponse.sendSuccessResponse(response,"Card_approval_mappingServlet?actionType=search");
                        }catch (Exception ex){
                            ApiResponse.sendErrorResponse(response,ex.getMessage());
                        }
                        return;
                    }
                    break;
                case "ajax_approvedCard":
                    if (Utils.checkPermission(userDTO, MenuConstants.CARD_APPROVAL_MAPPING_ADD,MenuConstants.CARD_APPROVAL_MAPPING_SEARCH)) {
                        try{
                            SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
                            Date from = f.parse(Jsoup.clean(request.getParameter("validationStartDate"), Whitelist.simpleText()));
                            Date to = f.parse(Jsoup.clean(request.getParameter("validationEndDate"), Whitelist.simpleText()));
                            approve(request, userDTO, true,from,to);
                            ApiResponse.sendSuccessResponse(response,"Card_approval_mappingServlet?actionType=search");
                        }catch (Exception ex){
                            ApiResponse.sendErrorResponse(response,ex.getMessage());
                        }
                        return;
                    }
                    break;
                case "ajax_rejectCard":
                    if (Utils.checkPermission(userDTO, MenuConstants.CARD_APPROVAL_MAPPING_ADD,MenuConstants.CARD_APPROVAL_MAPPING_SEARCH)) {
                        try{
                            approve(request, userDTO, false,null,null);
                            ApiResponse.sendSuccessResponse(response,"Card_approval_mappingServlet?actionType=search");
                        }catch (Exception ex){
                            ApiResponse.sendErrorResponse(response,ex.getMessage());
                        }
                        return;
                    }
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error(ex);
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }

    private void getCardApprovalModel(long cardInfoId, HttpServletRequest request, UserDTO userDTO) {

        CardApprovalModel approvalModel;
        String language = LM.getLanguage(userDTO);
        Card_infoDTO card_infoDTO = CardInfoDAO.getInstance().getDTOFromID(cardInfoId);
        if (card_infoDTO != null) {
            CardEmployeeInfoDTO employeeInfoDTO = CardEmployeeInfoRepository.getInstance().getById(card_infoDTO.cardEmployeeInfoId);
            CardEmployeeImagesDTO imagesDTO = CardEmployeeImagesDAO.getInstance().getDTOFromID(card_infoDTO.cardEmployeeImagesId);
            CardEmployeeOfficeInfoDTO officeInfoDTO = CardEmployeeOfficeInfoRepository.getInstance().getById(card_infoDTO.cardEmployeeOfficeInfoId);
            CardApprovalDTO cardApprovalDTO = CardApprovalRepository.getInstance().getByEmployeeRecordId(userDTO.employee_record_id);
            Card_approval_mappingDTO cardApprovalMappingDTO = Card_approval_mappingDAO.getInstance().getByCardInfoIdAndApproverEmployeeRecordId(cardInfoId,userDTO.employee_record_id);
            approvalModel = new CardApprovalModel(card_infoDTO, employeeInfoDTO, officeInfoDTO, imagesDTO, cardApprovalDTO, language,cardApprovalMappingDTO);
            request.setAttribute("cardApprovalModel", approvalModel);
        }
    }

    private void approve(HttpServletRequest request, UserDTO userDTO, boolean isAccepted,Date validFrom,Date validTo) throws Exception {

        long requesterEmployeeRecordId = userDTO.employee_record_id;
        long cardInfoId = Long.parseLong(request.getParameter("cardInfoId"));
        Card_infoDTO card_infoDTO = CardInfoDAO.getInstance().getDTOFromID(cardInfoId);
        if(validFrom !=null){
            card_infoDTO.validationFrom = validFrom.getTime();
        }
        if(validTo!=null){
            card_infoDTO.validationTo = validTo.getTime();
        }
        CardInfoDAO cardInfoDAO = CardInfoDAO.getInstance();
        cardInfoDAO.update(card_infoDTO);

        Employee_recordsDTO employeeRecordsDTO = Employee_recordsRepository.getInstance().getById(card_infoDTO.employeeRecordsId);
        TaskTypeEnum taskTypeEnum;
        if (card_infoDTO.cardCat == CardCategoryEnum.PERMANENT.getValue()) {
            if (employeeRecordsDTO.employeeClass == 3 || employeeRecordsDTO.employeeClass == 4) {
                taskTypeEnum = TaskTypeEnum.PERMANENT_CARD_FOR_3RD_OR_4TH_CLASS_EMPLOYEE;
            } else {
                taskTypeEnum = TaskTypeEnum.PERMANENT_CARD_FOR_1ST_OR_2ND_CLASS_EMPLOYEE;
            }
        } else {
            taskTypeEnum = TaskTypeEnum.TEMPORARY_CARD_FOR_EMPLOYEE;
        }
        CreateCardApprovalModel model = new CreateCardApprovalModel.CreateCardApprovalModelBuilder()
                .setCardCategoryEnum(CardCategoryEnum.PERMANENT)
                .setTaskTypeId(taskTypeEnum.getValue())
                .setCardInfoDTO(card_infoDTO)
                .setRequesterEmployeeRecordId(requesterEmployeeRecordId)
                .build();
        boolean sendNotificationToUser = false;
        if (isAccepted) {
            CardApprovalResponse response = Card_approval_mappingDAO.getInstance().movedToNextLevelOfApproval(model);
            if(!response.hasNextApproval){
                card_infoDTO.cardStatusCat = CardStatusEnum.APPROVED.getValue();
                cardInfoDAO.update(card_infoDTO);
                sendNotificationToUser = true;
            }else{
                CardApprovalNotification.getInstance().sendPendingNotification(response.organogramIds,card_infoDTO);
            }
        } else {
            String rejectReason = request.getParameter("reject_reason");
            if(rejectReason!=null){
                rejectReason = Jsoup.clean(rejectReason, Whitelist.simpleText());
                rejectReason = rejectReason.replace("'"," ");
                rejectReason = rejectReason.replace("\""," ");
            }else {
                rejectReason = "";
            }
            Card_approval_mappingDAO.getInstance().rejectPendingApproval(model,rejectReason);
            card_infoDTO.cardStatusCat = CardStatusEnum.REJECTED.getValue();
            card_infoDTO.comment = rejectReason;
            cardInfoDAO.update(card_infoDTO);
            Lost_card_infoDAO.getInstance().resetReIssueId(card_infoDTO.iD);
            sendNotificationToUser = true;
        }
        if(sendNotificationToUser){
            Long organogramId = null;
            if(card_infoDTO.cardCat == CardCategoryEnum.PERMANENT.getValue()){
                CardEmployeeOfficeInfoDTO cardEmployeeOfficeInfoDTO = CardEmployeeOfficeInfoRepository.getInstance().getById(card_infoDTO.cardEmployeeOfficeInfoId);
                if(cardEmployeeOfficeInfoDTO!=null){
                    organogramId = cardEmployeeOfficeInfoDTO.organogramId;
                }
            }else{
                EmployeeOfficeDTO employeeOfficeDTO = EmployeeOfficeRepository.getInstance().getByEmployeeRecordIdIsDefault(card_infoDTO.insertedBy);
                if(employeeOfficeDTO!=null){
                    organogramId = employeeOfficeDTO.officeUnitOrganogramId;
                }
            }
            if(organogramId!=null){
                CardApprovalNotification.getInstance().sendApproveOrRejectNotification(Collections.singletonList(organogramId),card_infoDTO,isAccepted);
            }
        }
    }
}