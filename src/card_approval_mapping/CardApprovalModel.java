package card_approval_mapping;

import card_info.*;
import election_details.Election_detailsDTO;
import election_details.Election_detailsRepository;
import employee_records.Employee_recordsDTO;
import employee_records.Employee_recordsRepository;
import geolocation.GeoLocationDAO2;
import org.apache.commons.codec.binary.Base64;
import pb.CatRepository;
import sessionmanager.SessionConstants;
import util.StringUtils;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;

public class CardApprovalModel {
    public long cardInfoId = -1;
    public long employeeRecordId=0;
    public String name = "";
    public String post = "";
    public String fatherName = "";
    public String motherName = "";
    public String dateOfBirth = "";
    public String nidNumber = "";
    public String presentAddress = "";
    public String permanentAddress = "";
    public String office = "";
    public String height = "";
    public String identificationSign = "";
    public String bloodGroup = "";
    public String mobileNumber = "";
    public String signatureSrc = "";
    public String photoSrc = "";
    public String nidFrontSrc = "";
    public String nidBackSrc = "";
    public String approvedBy = "";
    public String approvedOn = "";
    public String policeVerificationStatus = "";
    public long validationStartDate= SessionConstants.MIN_DATE;
    public long validationEndDate= SessionConstants.MIN_DATE;
    public Card_approval_mappingDTO cardApprovalMappingDTO;


    public CardApprovalModel() {
    }

    public CardApprovalModel(Card_infoDTO card_infoDTO, CardEmployeeInfoDTO employeeInfoDTO, CardEmployeeOfficeInfoDTO officeInfoDTO,
                             CardEmployeeImagesDTO imagesDTO, CardApprovalDTO cardApprovalDTO, String Language,Card_approval_mappingDTO cardApprovalMappingDTO) {
        boolean isLanguageEnglish = Language.equalsIgnoreCase("English");
        cardInfoId = card_infoDTO.iD;
        this.employeeRecordId = card_infoDTO.employeeRecordsId;
        this.cardApprovalMappingDTO = cardApprovalMappingDTO;
        if (employeeInfoDTO != null) {
            name = isLanguageEnglish ? employeeInfoDTO.nameEn : employeeInfoDTO.nameBn;
            fatherName = isLanguageEnglish ? employeeInfoDTO.fatherNameEn : employeeInfoDTO.fatherNameBn;
            motherName = isLanguageEnglish ? employeeInfoDTO.motherNameEn : employeeInfoDTO.motherNameBn;

            dateOfBirth = StringUtils.getFormattedDate(Language, employeeInfoDTO.dob);
            nidNumber = StringUtils.convertBanglaIfLanguageIsBangla(Language, employeeInfoDTO.nid);

            presentAddress = isLanguageEnglish ? employeeInfoDTO.presentAddressEn : employeeInfoDTO.presentAddressBn;
            permanentAddress = isLanguageEnglish ? employeeInfoDTO.permanentAddressEn : employeeInfoDTO.permanentAddressBn;
            height = employeeInfoDTO.height > 0 ?StringUtils.convertBanglaIfLanguageIsBangla(Language, String.valueOf(employeeInfoDTO.height)) : "";
            identificationSign = employeeInfoDTO.identificationSign;
            bloodGroup = CatRepository.getInstance().getText(Language, "blood_group", employeeInfoDTO.bloodGroup);
            mobileNumber = StringUtils.convertBanglaIfLanguageIsBangla(Language, employeeInfoDTO.mobileNumber);
        }
        if (officeInfoDTO != null) {
            post = isLanguageEnglish ? officeInfoDTO.organogramEng : officeInfoDTO.organogramBng;
            office = isLanguageEnglish ? officeInfoDTO.officeUnitEng : officeInfoDTO.officeUnitBng;

        }
        if (imagesDTO != null) {
            photoSrc = "data:image/jpg;base64,".concat(
                    imagesDTO.photo != null ? new String(Base64.encodeBase64(imagesDTO.photo)) : ""
            );

            signatureSrc = "data:image/jpg;base64,".concat(
                    imagesDTO.signature != null ? new String(Base64.encodeBase64(imagesDTO.signature)) : ""
            );

            nidFrontSrc = "data:image/jpg;base64,".concat(
                    imagesDTO.nidFrontSide != null ? new String(Base64.encodeBase64(imagesDTO.nidFrontSide)) : ""
            );

            nidBackSrc = "data:image/jpg;base64,".concat(
                    imagesDTO.nidBackSide != null ? new String(Base64.encodeBase64(imagesDTO.nidBackSide)) : ""
            );
        }

        if (cardApprovalDTO != null) {
            approvedBy = cardApprovalDTO.nameEng;
            approvedOn = StringUtils.getFormattedDate(Language, cardApprovalDTO.insertionTime);
        }
        LocalDate today = LocalDate.now();
        validationStartDate = Date.from(today.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant()).getTime();
        if(card_infoDTO.cardCat == CardCategoryEnum.PERMANENT.getValue()){
            Employee_recordsDTO employeeRecordsDTO = Employee_recordsRepository.getInstance().getById(card_infoDTO.employeeRecordsId);
            if (employeeRecordsDTO.isMP == 1) {
                Election_detailsDTO election_detailsDTO = Election_detailsRepository.getInstance().getElectionDetailsDTOByID(employeeRecordsDTO.parliamentNumberId);
                validationEndDate = election_detailsDTO.parliamentLastDate;
            } else {
                long addYear;
                if (employeeRecordsDTO.isMP == 2){
                    addYear = 5;
                }else {
                    addYear = 3;
                }
                LocalDate endDate = today.plusYears(addYear);
                endDate = endDate.minusDays(1);
                validationEndDate = Date.from(endDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant()).getTime();
            }
        }else{
            LocalDate endDate = today.plusMonths(6);
            endDate = endDate.minusDays(1);
            validationEndDate = Date.from(endDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant()).getTime();
        }
    }

    @Override
    public String toString() {
        return "CardApprovalModel{" +
                "cardInfoId=" + cardInfoId +
                ", name='" + name + '\'' +
                ", post='" + post + '\'' +
                ", fatherName='" + fatherName + '\'' +
                ", motherName='" + motherName + '\'' +
                ", dateOfBirth='" + dateOfBirth + '\'' +
                ", nidNumber='" + nidNumber + '\'' +
                ", presentAddress='" + presentAddress + '\'' +
                ", permanentAddress='" + permanentAddress + '\'' +
                ", office='" + office + '\'' +
                ", height='" + height + '\'' +
                ", identificationSign='" + identificationSign + '\'' +
                ", bloodGroup='" + bloodGroup + '\'' +
                ", mobileNumber='" + mobileNumber + '\'' +
                ", signatureSrc='" + signatureSrc + '\'' +
                ", photoSrc='" + photoSrc + '\'' +
                ", nidFrontSrc='" + nidFrontSrc + '\'' +
                ", nidBackSrc='" + nidBackSrc + '\'' +
                ", approvedBy='" + approvedBy + '\'' +
                ", approvedOn='" + approvedOn + '\'' +
                ", policeVerificationStatus='" + policeVerificationStatus + '\'' +
                ", cardApprovalMappingDTO=" + cardApprovalMappingDTO +
                '}';
    }
}
