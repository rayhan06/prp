package mp_report;


import language.LC;
import language.LM;
import login.LoginDTO;
import pbReport.*;
import permission.MenuConstants;
import role.PermissionRepository;
import user.UserDTO;
import util.HttpRequestUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;


@WebServlet("/Mp_report_Servlet")
public class Mp_report_Servlet extends HttpServlet implements ReportCommonService {

    private static final long serialVersionUID = 1L;

    private static final Set<String> searchParam = new HashSet<>(
            Arrays.asList("electionDetailsId", "electionConstituencyId", "politicalPartyId", "username", "mpStatusCat",
                    "startDate", "endDate", "startAge", "endAge", "mpTerm", "parliamentaryCommittee", "ministryOffice", "nameEng", "nameBng"));

    private final String[][] Display =
            {
                    {"display", "distinct ewm", "election_details_id", "parliament_number", ""},
                    {"display", "ewm", "election_constituency_id", "election_constituency_with_number", ""},
                    {"display", "ewm", "employee_records_id", "employee_records_id_bn", ""},
                    {"display", "ewm", "employee_records_id", "employee_records_id_en", ""},
                    {"display", "ewm", "political_party_id", "political_party", ""},
                    {"display", "ec", "boundary_details", "text", ""},
                    {"display", "er", "nid", "number", ""},
                    {"display", "er", "home_district", "home_district", ""},
                    {"display", "ewm", "mp_status_cat", "category", ""},
                    {"display", "ewm", "username", "text", ""},
                    {"display", "er", "date_of_birth", "date", ""},
                    {"display", "ewm", "date_of_birth", "complete_age", ""},
                    {"display", "ewm", "mp_profession", "text", ""},
                    {"display", "ewm", "mp_parliament_address", "geolocation", ""},
                    {"display", "ewm", "employee_records_id", "id_to_present_address", ""},
                    {"display", "ewm", "employee_records_id", "id_to_permanent_address", ""},
                    {"display", "er", "personal_mobile", "number", ""},
                    {"display", "er", "personal_email", "plain", ""},
                    {"display", "er", "religion", "religion", ""},
                    {"display", "er", "gender_cat", "category", ""},
                    {"display", "er", "mp_elected_count", "number", ""},
                    {"display", "ewm", "employee_records_id", "mp_report_committee", ""},
                    {"display", "ewm", "employee_records_id", "mp_report_ministry", ""},
                    {"display", "ewm", "start_date", "date", ""},
                    {"display", "ewm", "end_date", "date", ""},
                    {"display", "ewm", "mp_remarks", "text", ""},
            };

    private final Map<String, String[]> stringMap = new HashMap<>();

    private String[][] Criteria;

    public Mp_report_Servlet() {
        stringMap.put("electionDetailsId", new String[]{"criteria", "ewm", "election_details_id", "=", "AND", "long", "", "", ""
                , "electionDetailsId", "electionDetailsId", String.valueOf(LC.MP_REPORT_WHERE_ELECTIONDETAILSID), "election_details", null, "true", "1"});
        stringMap.put("electionConstituencyId", new String[]{"criteria", "ewm", "election_constituency_id", "=", "AND", "long", "", "", "any"
                , "electionConstituencyId", "electionConstituencyId", String.valueOf(LC.MP_REPORT_WHERE_ELECTIONCONSTITUENCYID), "election_constituency", null, "true", "2"});
        stringMap.put("politicalPartyId", new String[]{"criteria", "ewm", "political_party_id", "=", "AND", "long", "", "", "any"
                , "politicalPartyId", "politicalPartyId", String.valueOf(LC.MP_REPORT_WHERE_POLITICALPARTYID), "political_party", null, "true", "3"});
        stringMap.put("username", new String[]{"criteria", "ewm", "username", "=", "AND", "String", "", "", "any"
                , "username", "username", String.valueOf(LC.MP_REPORT_WHERE_USERNAME), "username", null, "true", "4"});
        stringMap.put("mpStatusCat", new String[]{"criteria", "ewm", "mp_status_cat", "=", "AND", "int", "", "", "any"
                , "mpStatusCat", "mpStatusCat", String.valueOf(LC.MP_REPORT_SELECT_MPSTATUSCAT), "category", "mp_status", "true", "5"});
        stringMap.put("startDate", new String[]{"criteria", "ewm", "start_date", ">=", "AND", "long", "", "", Long.MIN_VALUE + ""
                , "startDate", "startDate", String.valueOf(LC.MP_REPORT_SELECT_STARTDATE), "date", null, "true", "6"});
        stringMap.put("endDate", new String[]{"criteria", "ewm", "end_date", "<=", "AND", "long", "", "", Long.MAX_VALUE + ""
                , "endDate", "endDate", String.valueOf(LC.MP_REPORT_SELECT_ENDDATE), "date", null, "true", "7"});
        stringMap.put("startAge", new String[]{"criteria", "ewm", "date_of_birth", "<=", "AND", "long", "", "", ""
                , "age_start", "age_start", String.valueOf(LC.HR_REPORT_AGE_RANGE_START), "number", null, "true", "8"});
        stringMap.put("endAge", new String[]{"criteria", "ewm", "date_of_birth", ">=", "AND", "long", "", "", ""
                , "age_end", "age_end", String.valueOf(LC.HR_REPORT_AGE_RANGE_TO), "number", null, "true", "9"});
        stringMap.put("mpTerm", new String[]{"criteria", "er", "mp_elected_count", "=", "AND", "int", "", "", "any"
                , "mpTerm", "mpTerm", String.valueOf(LC.PALCEHOLDER_HOW_MANY_TIMES_ELECTED), "", null, "true", "20"});
        stringMap.put("nameEng", new String[]{"criteria", "er", "name_eng", "Like", "AND", "String", "", "", "any", "nameEng",
                "nameEng", String.valueOf(LC.EMPLOYEE_RECORDS_ADD_NAMEENG), "text", null, "true", "21"});
        stringMap.put("nameBng", new String[]{"criteria", "er", "name_bng", "Like", "AND", "String", "", "", "any", "nameBng",
                "nameBng", String.valueOf(LC.EMPLOYEE_RECORDS_ADD_NAMEBNG), "text", null, "true", "22"});
        stringMap.put("parliamentaryCommittee", new String[]{"criteria", "cm", "committees_id", "=", "AND", "long", "", "", "any"
                , "parliamentaryCommittee", "parliamentaryCommittee", null, null, null, null, null});
        stringMap.put("ministryOffice", new String[]{"criteria", "mom", "ministry_office_id", "=", "AND", "long", "", "", "any"
                , "ministryOffice", "ministryOffice", null, null, null, null, null});
        stringMap.put("isDeleted", new String[]{"criteria", "ewm", "isDeleted", "=", "AND", "long", "", "", "0", "isDeleted", null, null, null, null, null, null});
        stringMap.put("isDeleted_er", new String[]{"criteria", "er", "isDeleted", "=", "AND", "long", "", "", "0", "isDeleted", null, null, null, null, null, null});
    }

    private String sql;

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;
        UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;
        boolean isLangEng = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
        sql = " election_wise_mp as ewm inner join employee_records as er on ewm.employee_records_id = er.id left join " +
                "committees_mapping cm on ewm.employee_records_id=cm.employee_records_id left join ministry_office_mapping mom " +
                "on ewm.employee_records_id=mom.employee_records_id left join election_constituency ec on ec.ID=ewm.election_constituency_id ";

        Display[0][4] = LM.getText(LC.MP_REPORT_SELECT_ELECTIONDETAILSID, loginDTO);
        Display[1][4] = LM.getText(LC.MP_REPORT_SELECT_ELECTIONCONSTITUENCYID, loginDTO);
        Display[2][4] = isLangEng ? "MP Name(Bangla)" : "সংসদ সদস্যের নাম(বাংলা)";
        Display[3][4] = isLangEng ? "MP Name(English)" : "সংসদ সদস্যের নাম(ইংরেজি)";
        Display[4][4] = LM.getText(LC.MP_REPORT_SELECT_POLITICALPARTYID, loginDTO);
        Display[5][4] = LM.getText(LC.ELECTION_CONSTITUENCY_ADD_BOUNDARYDETAILS, loginDTO);
        Display[6][4] = LM.getText(LC.EMPLOYEE_RECORDS_EDIT_NID, loginDTO);
        Display[7][4] = LM.getText(LC.EMPLOYEE_RECORDS_EDIT_HOME_DISTRICT, loginDTO);
        Display[8][4] = LM.getText(LC.MP_REPORT_SELECT_MPSTATUSCAT, loginDTO);
        Display[9][4] = LM.getText(LC.MP_REPORT_SELECT_USERNAME, loginDTO);
        Display[10][4] = LM.getText(LC.EMPLOYEE_RECORDS_ADD_DATEOFBIRTH, loginDTO);
        Display[11][4] = LM.getText(LC.PHYSIOTHERAPY_PLAN_ADD_AGE, loginDTO);
        Display[12][4] = LM.getText(LC.MP_REPORT_SELECT_MPPROFESSION, loginDTO);
        Display[13][4] = LM.getText(LC.MP_REPORT_SELECT_MPPARLIAMENTADDRESS, loginDTO);
        Display[14][4] = LM.getText(LC.EMPLOYEE_RECORDS_ADD_PRESENTADDRESS, loginDTO);
        Display[15][4] = LM.getText(LC.EMPLOYEE_RECORDS_ADD_PERMANENTADDRESS, loginDTO);
        Display[16][4] = LM.getText(LC.EMPLOYEE_INFO_REPORT_SELECT_MOBILE_NUMBER, loginDTO);
        Display[17][4] = LM.getText(LC.EMPLOYEE_INFO_REPORT_SELECT_EMAIL, loginDTO);
        Display[18][4] = LM.getText(LC.EMPLOYEE_INFO_REPORT_SELECT_RELIGION, loginDTO);
        Display[19][4] = LM.getText(LC.EMPLOYEE_INFO_REPORT_SELECT_GENDERCAT, loginDTO);
        Display[20][4] = LM.getText(LC.PALCEHOLDER_HOW_MANY_TIMES_ELECTED, loginDTO);
        Display[21][4] = LM.getText(LC.COMMITTEES_MAPPING_ADD_COMMITTEESID, loginDTO);
        Display[22][4] = LM.getText(LC.MINISTRY_OFFICE_MAPPING_ADD_MINISTRYOFFICEID, loginDTO);
        Display[23][4] = LM.getText(LC.MP_REPORT_SELECT_STARTDATE, loginDTO);
        Display[24][4] = LM.getText(LC.MP_REPORT_SELECT_ENDDATE, loginDTO);
        Display[25][4] = LM.getText(LC.MP_REPORT_SELECT_MPREMARKS, loginDTO);

        Set<String> inputs = PBReportUtils.prepareInputSets(searchParam, request);
        inputs.add("isDeleted");
        inputs.add("isDeleted_er");
        Criteria = PBReportUtils.prepareCriteria(inputs, stringMap);
        for (String[] arr : Criteria) {
            switch (arr[9]) {
                case "age_start":
                    long ageStart = Long.parseLong(request.getParameter("startAge"));
                    arr[8] = String.valueOf(calculateLongDateValue(ageStart));
                    break;
                case "age_end":
                    long ageEnd = Long.parseLong(request.getParameter("endAge"));
                    arr[8] = String.valueOf(calculateLongDateValue(ageEnd));
                    break;
            }
        }
        request.setAttribute("dontUseOutDatedModal", true);
        if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.WORKPLACE_AGE_REPORT_DETAILS)) {
            reportGenerate(request, response);
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }

    @Override
    public String[][] getCriteria() {
        return Criteria;
    }

    @Override
    public Integer getTotalEntryCount(String tableJoinSql, HttpServletRequest request, String[][] criteria, String GroupBy) throws Exception {
        SqlPair sqlPair = createMpReportCountSQL(new ReportMetadata(request, criteria, GroupBy), tableJoinSql);
        return ReportProcessor.executeCountSql(sqlPair);
    }

    private static SqlPair createMpReportCountSQL(ReportMetadata reportMetadata, String tableName) throws Exception {

        StringBuilder sqlBuilder = new StringBuilder();
        sqlBuilder.append("SELECT COUNT(distinct(ewm.employee_records_id)) ");
        return ReportProcessor.createCommonReportCountSQL(reportMetadata, tableName, sqlBuilder);
    }

    @Override
    public String[][] getDisplay() {
        return Display;
    }

    @Override
    public String getSQL() {
        return sql;
    }

    @Override
    public int getLCForReportName() {
        return LC.MP_REPORT_OTHER_MP_REPORT;
    }

    @Override
    public String getFileName() {
        return "mp_report";
    }

    @Override
    public String getTableName() {
        return "mp_report";
    }

    private long calculateLongDateValue(long ageYears) {
        LocalDate ld = LocalDate.now();
        ld = ld.minusYears(ageYears);
        return Date.from(ld.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant()).getTime();
    }

    @Override
    public String getOrderBy(HttpServletRequest request) {
        String orderBy = ReportCommonService.super.getOrderBy(request);
        if (orderBy == null || orderBy.length() == 0) {
            orderBy = " ewm.election_details_id DESC,  ec.constituency_number ASC ";
        }
        return orderBy;
    }
}
