package asset_license_key;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.sql.SQLException;
import common.ConnectionAndStatementUtil;
import org.apache.log4j.Logger;

import appointment.KeyCountDTO;
import repository.RepositoryManager;
import util.*;
import user.UserDTO;

public class Asset_license_keyDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());

	public Asset_license_keyDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";
		commonMaps = new Asset_license_keyMAPS(tableName);
		columnNames = new String[] 
		{
			"ID",
			"asset_license_id",
			"product_key",
			"insertion_date",
			"inserted_by",
			"modified_by",
			"search_column",
			"is_used",
			"employee_records_id",
			"isDeleted",
			"lastModificationTime"
		};
	}
	
	public Asset_license_keyDAO()
	{
		this("asset_license_key");		
	}
	
	public void setSearchColumn(Asset_license_keyDTO asset_license_keyDTO)
	{
		asset_license_keyDTO.searchColumn = "";


	}
	
	public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException
	{
		Asset_license_keyDTO asset_license_keyDTO = (Asset_license_keyDTO)commonDTO;
		int index = 1;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(asset_license_keyDTO);
		if(isInsert)
		{
			ps.setObject(index++,asset_license_keyDTO.iD);
		}
		ps.setObject(index++,asset_license_keyDTO.assetLicenseId);
		ps.setObject(index++,asset_license_keyDTO.productKey);
		ps.setObject(index++,asset_license_keyDTO.insertionDate);
		ps.setObject(index++,asset_license_keyDTO.insertedBy);
		ps.setObject(index++,asset_license_keyDTO.modifiedBy);
		ps.setObject(index++,asset_license_keyDTO.searchColumn);
		ps.setObject(index++,asset_license_keyDTO.isUsed);
		ps.setObject(index++,asset_license_keyDTO.employeeRecordsId);
		if(isInsert)
		{
			ps.setObject(index++, 0);
			ps.setObject(index++, lastModificationTime);
		}
	}
	
	public Asset_license_keyDTO build(ResultSet rs)
	{
		try
		{
			Asset_license_keyDTO asset_license_keyDTO = new Asset_license_keyDTO();
			asset_license_keyDTO.iD = rs.getLong("ID");
			asset_license_keyDTO.assetLicenseId = rs.getLong("asset_license_id");
			asset_license_keyDTO.productKey = rs.getString("product_key");
			asset_license_keyDTO.insertionDate = rs.getLong("insertion_date");
			asset_license_keyDTO.insertedBy = rs.getString("inserted_by");
			asset_license_keyDTO.modifiedBy = rs.getString("modified_by");
			asset_license_keyDTO.searchColumn = rs.getString("search_column");
			asset_license_keyDTO.isUsed = rs.getBoolean("is_used");
			asset_license_keyDTO.employeeRecordsId = rs.getLong("employee_records_id");
			asset_license_keyDTO.isDeleted = rs.getInt("isDeleted");
			asset_license_keyDTO.lastModificationTime = rs.getLong("lastModificationTime");
			return asset_license_keyDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
	
	

	public Asset_license_keyDTO getDTOByID (long id)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID = " + id;
		Asset_license_keyDTO asset_license_keyDTO = ConnectionAndStatementUtil.getT(sql,this::build);
		return asset_license_keyDTO;
	}

	
	public List<Asset_license_keyDTO> getDTOs(Collection recordIDs)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID IN ( ";
		for(int i = 0;i<recordIDs.size();i++){
			if(i!=0){
				sql+=",";
			}
			sql+=((ArrayList)recordIDs).get(i);
		}
		sql+=")  order by lastModificationTime desc";
		printSql(sql);
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);		
	}

	public long unassign(long ID) throws Exception {
		long lastModificationTime = System.currentTimeMillis();
		String sql = "UPDATE " + tableName;

		sql += " SET is_used = 0, lastModificationTime = " + lastModificationTime + ","
				+ " employee_records_id = -1"
				+ " WHERE ID = " + ID + " and is_used = 1";

		printSql(sql);
		return (long) ConnectionAndStatementUtil.getWriteConnectionAndPrepareStatement(model -> {
            Connection connection = model.getConnection();
            PreparedStatement ps = (PreparedStatement) model.getStatement();
            try {
                ps.executeUpdate();
                recordUpdateTime(connection, lastModificationTime);
                return ID;
            } catch (SQLException ex) {
                logger.error(ex);
                return -1L;
            }
        }, sql);
	}
	
	public long assign(long ID, long organogramId) throws Exception {
		long lastModificationTime = System.currentTimeMillis();
		String sql = "UPDATE " + tableName;

		sql += " SET is_used = 1, lastModificationTime = " + lastModificationTime + ","
				+ " employee_records_id = " + organogramId
				+ " WHERE ID = " + ID + " and is_used = 0";

		printSql(sql);
		return (long) ConnectionAndStatementUtil.getWriteConnectionAndPrepareStatement(model -> {
            Connection connection = model.getConnection();
            PreparedStatement ps = (PreparedStatement) model.getStatement();
            try {
                ps.executeUpdate();
                recordUpdateTime(connection, lastModificationTime);
                return ID;
            } catch (SQLException ex) {
                logger.error(ex);
                return -1L;
            }
        }, sql);
	}
	
	public KeyCountDTO getLicenceCount(ResultSet rs)
	{
		try
		{
			KeyCountDTO keyCountDTO = new KeyCountDTO();
			keyCountDTO.key = rs.getLong("is_used");
			keyCountDTO.count = rs.getInt("count(id)");

			return keyCountDTO;
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
			return null;
		}
	}
	
	public List<KeyCountDTO> getLicenceStatusCount()
    {
		String sql = "SELECT is_used, count(id) FROM asset_license_key where isdeleted = 0 group by is_used";				
		return ConnectionAndStatementUtil.getListOfT(sql,this::getLicenceCount);	
    }
	
	public List<Asset_license_keyDTO> getUnassignedKeysWithSoftwareSubtype(long software_subtype_type, long default_id){
		String sql = "SELECT * ";

		sql += " FROM " + tableName;
        
        sql += " WHERE isDeleted = 0 and is_used = 0 and asset_license_id in ("
        		+ " select id from asset_license where isDeleted = 0 and software_subtype_type =  " + software_subtype_type + ") "
        		+ " or id = " + default_id;
        return ConnectionAndStatementUtil.getListOfT(sql,this::build);
	}
	
	public Asset_license_keyDTO get1stUnassignedKeyWithSoftwareSubtype(long software_subtype_type){
		String sql = "SELECT * ";

		sql += " FROM " + tableName;
        
        sql += " WHERE isDeleted = 0 and is_used = 0 and asset_license_id in ("
        		+ " select id from asset_license where isDeleted = 0 and software_subtype_type =  " + software_subtype_type + ") "
        		+ " limit 1";
		return ConnectionAndStatementUtil.getT(sql,this::build);
	}
	
	public Asset_license_keyDTO get1stUnassignedKeyWithLicenceId(long asset_license_id){
		String sql = "SELECT * ";

		sql += " FROM " + tableName;
        
        sql += " WHERE isDeleted = 0 and is_used = 0 and and asset_license_id =  " + asset_license_id + " limit 1";
        return ConnectionAndStatementUtil.getT(sql,this::build);
	}
	
	public List<Asset_license_keyDTO> getUnassignedKeys(long asset_license_id){
		String sql = "SELECT * ";

		sql += " FROM " + tableName;
        
        sql += " WHERE isDeleted = 0 and is_used = 0 and and asset_license_id =  " + asset_license_id;
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);
	}
	
	public int getCount(ResultSet rs)
	{
		try {
			return rs.getInt("count(id)");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return 0;
		}
	
	}
	
	public int getDTOsLicenseCount(long asset_license_id, String filter){
		String sql = "SELECT count(id) ";

		sql += " FROM " + tableName;
        
        sql += " WHERE isDeleted = 0 and asset_license_id =  " + asset_license_id;
        
        if(filter != null && !filter.equalsIgnoreCase(""))
        {
        	sql += " and " + filter;
        }
        return ConnectionAndStatementUtil.getT(sql,this::getCount, 0);
	}
	
	public List<Asset_license_keyDTO> getDTOsByLicenseId(long asset_license_id){
		String sql = "SELECT * ";

		sql += " FROM " + tableName;
        
        sql += " WHERE isDeleted = 0 and asset_license_id =  " + asset_license_id;
        return ConnectionAndStatementUtil.getListOfT(sql,this::build);
	
	}
	
	public List<Asset_license_keyDTO> getAllAsset_license_key (boolean isFirstReload)
    {
		String sql = "SELECT * FROM " + tableName + " WHERE ";
		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by " + tableName + ".lastModificationTime desc";
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);	
    }

	
	public List<Asset_license_keyDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO)
	{
		return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
	}

	
	public List<Asset_license_keyDTO> getDTOs(Hashtable  p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
			String filter, boolean tableHasJobCat)
	{
		String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat);
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);			
	}
	public String getSqlWithSearchCriteriaForNormalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
			UserDTO userDTO, String filter, boolean tableHasJobCat)
    {
		boolean viewAll = false;
		
		if(p_searchCriteria != null && p_searchCriteria.get("ViewAll") != null)
		{
			System.out.println("ViewAll = " + p_searchCriteria.get("ViewAll"));
			viewAll = true;
		}
		else
		{
			System.out.println("ViewAll is null ");
		}
		
		String sql = "SELECT ";
		if(category == GETIDS)
		{
			sql += " " + tableName + ".ID as ID ";
		}
		else if(category == GETCOUNT)
		{
			sql += " count( " + tableName + ".ID) as countID ";
		}
		else if(category == GETDTOS)
		{
			sql += " distinct " + tableName + ".* ";
		}
		sql += "FROM " + tableName + " ";
		sql += joinSQL;
		

		
		String AnyfieldSql = "";
		String AllFieldSql = "";
		
		if(p_searchCriteria != null)
		{
						
			
			Enumeration names = p_searchCriteria.keys();
			String str, value;
			
			AnyfieldSql = "(";
			
			if(p_searchCriteria.get("AnyField")!= null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase(""))
			{
				AnyfieldSql+= tableName + ".search_column like '%" + p_searchCriteria.get("AnyField").toString() + "%'";			
			}
			AnyfieldSql += ")";
			System.out.println("AnyfieldSql = " + AnyfieldSql);
			
			AllFieldSql = "(";
			int i = 0;
			while(names.hasMoreElements())
			{				
				str = (String) names.nextElement();
				value = (String)p_searchCriteria.get(str);
		        System.out.println(str + ": " + value);
				if(value != null && !value.equalsIgnoreCase("") && (
						 str.equals("product_key")
						|| str.equals("insertion_date_start")
						|| str.equals("insertion_date_end")
				)
						
				)
				{
					if(p_searchCriteria.get(str).equals("any"))
					{
						continue;
					}

					if( i > 0)
					{
						AllFieldSql+= " AND  ";
					}
					
					 if(str.equals("product_key"))
					{
						AllFieldSql += "" + tableName + ".product_key like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("insertion_date_start"))
					{
						AllFieldSql += "" + tableName + ".insertion_date >= " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("insertion_date_end"))
					{
						AllFieldSql += "" + tableName + ".insertion_date <= " + p_searchCriteria.get(str);
						i ++;
					}
					
					
				}
			}
			
			AllFieldSql += ")";
			System.out.println("AllFieldSql = " + AllFieldSql);
			
			
		}
		
		
		sql += " WHERE ";
		
		sql += " (" + tableName + ".isDeleted = 0 ";			
		sql += ")";
		
		
		if(!filter.equalsIgnoreCase(""))
		{
			sql += " and " + filter + " ";
		}
		
		if(!AnyfieldSql.equals("()") && !AnyfieldSql.equals(""))
		{
			sql += " AND " + AnyfieldSql;
			
		}
		if(!AllFieldSql.equals("()") && !AllFieldSql.equals(""))
		{			
			sql += " AND " + AllFieldSql;
		}
		
	
			
		sql += " order by " + tableName + ".lastModificationTime desc ";

		printSql(sql);
		
		if(limit >= 0)
		{
			sql += " limit " + limit;
		}
		if(offset >= 0)
		{
			sql += " offset " + offset;
		}
		
		System.out.println("-------------- sql = " + sql);
		
		return sql;
    }
	
	public String getSqlWithSearchCriteria(Hashtable p_searchCriteria, int limit, int offset, int category,
			boolean isPermanentTable, UserDTO userDTO, String filter, boolean tableHasJobCat)
    {		
		return getSqlWithSearchCriteriaForNormalSearch(p_searchCriteria, limit, offset, category, userDTO, filter, tableHasJobCat);	
    }
				
}
	