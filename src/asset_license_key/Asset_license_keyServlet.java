package asset_license_key;

import java.io.*;
import java.text.SimpleDateFormat;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import org.apache.log4j.Logger;

import login.LoginDTO;
import permission.MenuConstants;
import role.PermissionRepository;


import sessionmanager.SessionConstants;

import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;
import util.CommonRequestHandler;
import util.RecordNavigationManager4;

import java.util.*;
import javax.servlet.http.*;

import geolocation.GeoLocationDAO2;
import language.LC;
import language.LM;

import com.google.gson.Gson;

import pb.*;
import pbReport.*;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;




/**
 * Servlet implementation class Asset_license_keyServlet
 */
@WebServlet("/Asset_license_keyServlet")
@MultipartConfig
public class Asset_license_keyServlet extends HttpServlet 
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Asset_license_keyServlet.class);

    String tableName = "asset_license_key";

	Asset_license_keyDAO asset_license_keyDAO;
	CommonRequestHandler commonRequestHandler;
    private final Gson gson = new Gson();
    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Asset_license_keyServlet() 
	{
        super();
    	try
    	{
			asset_license_keyDAO = new Asset_license_keyDAO(tableName);
			commonRequestHandler = new CommonRequestHandler(asset_license_keyDAO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }   

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		System.out.println("In doget request = " + request);
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		try
		{
			String actionType = request.getParameter("actionType");
			if(actionType.equals("getAddPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ASSET_LICENSE_KEY_ADD))
				{
					commonRequestHandler.getAddPage(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getEditPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ASSET_LICENSE_KEY_UPDATE))
				{
					getAsset_license_key(request, response);					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}						
			}
			else if(actionType.equals("getURL"))
			{
				String URL = request.getParameter("URL");
				System.out.println("URL = " + URL);
				response.sendRedirect(URL);			
			}			
			else if(actionType.equals("search"))
			{
				System.out.println("search requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ASSET_LICENSE_KEY_SEARCH))
				{
					if(isPermanentTable)
					{
						String filter = request.getParameter("filter");
						System.out.println("filter = " + filter);
						if(filter!=null)
						{
							filter = ""; //shouldn't be directly used, rather manipulate it.
							searchAsset_license_key(request, response, isPermanentTable, filter);
						}
						else
						{
							searchAsset_license_key(request, response, isPermanentTable, "");
						}
					}					
					else
					{
						//searchAsset_license_key(request, response, tempTableName, isPermanentTable);
					}
				}			
			}
			else if(actionType.equals("view"))
			{
				System.out.println("view requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ASSET_LICENSE_KEY_SEARCH))
				{
					commonRequestHandler.view(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else
			{
				request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}


	
	

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		// TODO Auto-generated method stub
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		System.out.println("doPost");
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		
		try
		{
			String actionType = request.getParameter("actionType");
			System.out.println("actionType = " + actionType);
			if(actionType.equals("add"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ASSET_LICENSE_KEY_ADD))
				{
					System.out.println("going to  addAsset_license_key ");
					addAsset_license_key(request, response, true, userDTO, true);
				}
				else
				{
					System.out.println("Not going to  addAsset_license_key ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}

			else if(actionType.equals("getDTO"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ASSET_LICENSE_KEY_ADD))
				{
					getDTO(request, response);					
				}
				else
				{
					System.out.println("Not going to  addAsset_license_key ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else if(actionType.equals("getKey"))
			{
				long software_subtype = Long.parseLong(request.getParameter("software_subtype"));
				Asset_license_keyDTO asset_license_keyDTO = asset_license_keyDAO.get1stUnassignedKeyWithSoftwareSubtype(software_subtype);
				PrintWriter out = response.getWriter();
				response.setContentType("application/json");
				response.setCharacterEncoding("UTF-8");				
				String encoded = this.gson.toJson(asset_license_keyDTO);
				System.out.println("json encoded data = " + encoded);
				out.print(encoded);
				out.flush();
			}
			else if(actionType.equals("getKeys"))
			{
				long software_subtype = Long.parseLong(request.getParameter("software_subtype"));
				long default_id = Long.parseLong(request.getParameter("default_id"));
				
				System.out.println("default_id = " + default_id);
				
				List<Asset_license_keyDTO> asset_license_keyDTOs = asset_license_keyDAO.getUnassignedKeysWithSoftwareSubtype(software_subtype, default_id);
				String options;
				
				Asset_license_keyDTO defaultAsset_license_keyDTO = asset_license_keyDAO.getDTOByID(default_id);
				if(defaultAsset_license_keyDTO == null)
				{
					options = "<option value='-1'>" + LM.getText(LC.HM_SELECT, loginDTO) + "</option>";
				}
				else
				{
					options = "<option value='" + default_id + "'>" + defaultAsset_license_keyDTO.productKey + "</option>";
				}
				for(Asset_license_keyDTO asset_license_keyDTO: asset_license_keyDTOs)
				{
					if(asset_license_keyDTO.iD != default_id)
					{
						options += "<option value='" + asset_license_keyDTO.iD + "'>" + asset_license_keyDTO.productKey + "</option>";
					}
					
				}
				options += "<option value='-1'>" + LM.getText(LC.HM_NONE) + "</option>";
				
				response.getWriter().write(options);
			}
			else if(actionType.equals("edit"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ASSET_LICENSE_KEY_UPDATE))
				{					
					addAsset_license_key(request, response, false, userDTO, isPermanentTable);					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("delete"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ASSET_LICENSE_KEY_UPDATE))
				{
					commonRequestHandler.delete(request, response, userDTO);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}	
			else if(actionType.equals("search"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ASSET_LICENSE_KEY_SEARCH))
				{
					searchAsset_license_key(request, response, true, "");
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getGeo"))
			{
				System.out.println("going to geoloc ");
				request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
			}
			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}
	
	private void getDTO(HttpServletRequest request, HttpServletResponse response) 
	{
		try 
		{
			System.out.println("In getDTO");
			Asset_license_keyDTO asset_license_keyDTO = asset_license_keyDAO.getDTOByID(Long.parseLong(request.getParameter("ID")));
			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			
			String encoded = this.gson.toJson(asset_license_keyDTO);
			System.out.println("json encoded data = " + encoded);
			out.print(encoded);
			out.flush();
		}
		catch (NumberFormatException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	private void addAsset_license_key(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, boolean isPermanentTable) throws IOException 
	{
		// TODO Auto-generated method stub
		try 
		{
			request.setAttribute("failureMessage", "");
			System.out.println("%%%% addAsset_license_key");
			String path = getServletContext().getRealPath("/img2/");
			Asset_license_keyDTO asset_license_keyDTO;
			SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
						
			if(addFlag == true)
			{
				asset_license_keyDTO = new Asset_license_keyDTO();
			}
			else
			{
				asset_license_keyDTO = asset_license_keyDAO.getDTOByID(Long.parseLong(request.getParameter("iD")));
			}
			
			String Value = "";

			Value = request.getParameter("iD");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("iD = " + Value);
			if(Value != null)
			{
				
				asset_license_keyDTO.iD = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("assetLicenseId");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("assetLicenseId = " + Value);
			if(Value != null)
			{
				
				asset_license_keyDTO.assetLicenseId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("productKey");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("productKey = " + Value);
			if(Value != null)
			{
				
				asset_license_keyDTO.productKey = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			if(addFlag)
			{
				Calendar c = Calendar.getInstance();
				c.set(Calendar.HOUR_OF_DAY, 0);
				c.set(Calendar.MINUTE, 0);
				c.set(Calendar.SECOND, 0);
				c.set(Calendar.MILLISECOND, 0);
				
				asset_license_keyDTO.insertionDate = c.getTimeInMillis();
			}			


			Value = request.getParameter("insertedBy");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("insertedBy = " + Value);
			if(Value != null)
			{
				
				asset_license_keyDTO.insertedBy = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("modifiedBy");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("modifiedBy = " + Value);
			if(Value != null)
			{
				
				asset_license_keyDTO.modifiedBy = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("searchColumn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("searchColumn = " + Value);
			if(Value != null)
			{
				
				asset_license_keyDTO.searchColumn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			
			System.out.println("Done adding  addAsset_license_key dto = " + asset_license_keyDTO);
			long returnedID = -1;
			
			if(isPermanentTable == false) //add new row for validation and make the old row outdated
			{
				asset_license_keyDAO.setIsDeleted(asset_license_keyDTO.iD, CommonDTO.OUTDATED);
				returnedID = asset_license_keyDAO.add(asset_license_keyDTO);
				asset_license_keyDAO.setIsDeleted(returnedID, CommonDTO.WAITING_FOR_APPROVAL);
			}
			else if(addFlag == true)
			{
				returnedID = asset_license_keyDAO.manageWriteOperations(asset_license_keyDTO, SessionConstants.INSERT, -1, userDTO);
			}
			else
			{				
				returnedID = asset_license_keyDAO.manageWriteOperations(asset_license_keyDTO, SessionConstants.UPDATE, -1, userDTO);											
			}
			
			
			
			
			
			
			
			
			
			if(isPermanentTable)
			{
				String inPlaceSubmit = request.getParameter("inplacesubmit");
				
				if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
				{
					getAsset_license_key(request, response, returnedID);
				}
				else
				{
					response.sendRedirect("Asset_license_keyServlet?actionType=search");
				}
			}
			else
			{
				commonRequestHandler.validate(asset_license_keyDAO.getDTOByID(returnedID), request, response, userDTO);
			}
					
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	



	
	
	

	private void getAsset_license_key(HttpServletRequest request, HttpServletResponse response, long id) throws ServletException, IOException
	{
		System.out.println("in getAsset_license_key");
		Asset_license_keyDTO asset_license_keyDTO = null;
		try 
		{
			asset_license_keyDTO = asset_license_keyDAO.getDTOByID(id);
			request.setAttribute("ID", asset_license_keyDTO.iD);
			request.setAttribute("asset_license_keyDTO",asset_license_keyDTO);
			request.setAttribute("asset_license_keyDAO",asset_license_keyDAO);
			
			String URL= "";
			
			String inPlaceEdit = request.getParameter("inplaceedit");
			String inPlaceSubmit = request.getParameter("inplacesubmit");
			String getBodyOnly = request.getParameter("getBodyOnly");
			
			if(inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase(""))
			{
				URL = "asset_license_key/asset_license_keyInPlaceEdit.jsp";	
				request.setAttribute("inplaceedit","");				
			}
			else if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
			{
				URL = "asset_license_key/asset_license_keySearchRow.jsp";
				request.setAttribute("inplacesubmit","");					
			}
			else
			{
				if(getBodyOnly  != null && !getBodyOnly.equalsIgnoreCase(""))
				{
					URL = "asset_license_key/asset_license_keyEditBody.jsp?actionType=edit";
				}
				else
				{
					URL = "asset_license_key/asset_license_keyEdit.jsp?actionType=edit";
				}				
			}
			
			RequestDispatcher rd = request.getRequestDispatcher(URL);
			rd.forward(request, response);
		}
		catch (NumberFormatException e) 
		{
			e.printStackTrace();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	
	private void getAsset_license_key(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		getAsset_license_key(request, response, Long.parseLong(request.getParameter("ID")));	
	}
	
	private void searchAsset_license_key(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException
	{
		System.out.println("in  searchAsset_license_key 1");
		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String ajax = request.getParameter("ajax");
		boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);
		
        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
			SessionConstants.NAV_ASSET_LICENSE_KEY,
			request,
			asset_license_keyDAO,
			SessionConstants.VIEW_ASSET_LICENSE_KEY,
			SessionConstants.SEARCH_ASSET_LICENSE_KEY,
			tableName,
			isPermanent,
			userDTO,
			filter,
			true);
        try
        {
			System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        }
        catch(Exception e)
        {
			System.out.println("failed to dojob" + e);
        }

		request.setAttribute("asset_license_keyDAO",asset_license_keyDAO);
        RequestDispatcher rd;
        if(!isPermanent)
        {
        	if(hasAjax == false)
	        {
	        	System.out.println("Going to asset_license_key/asset_license_keyApproval.jsp");
	        	rd = request.getRequestDispatcher("asset_license_key/asset_license_keyApproval.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to asset_license_key/asset_license_keyApprovalForm.jsp");
	        	rd = request.getRequestDispatcher("asset_license_key/asset_license_keyApprovalForm.jsp");
	        }
        }
        else
        {
	        if(hasAjax == false)
	        {
	        	System.out.println("Going to asset_license_key/asset_license_keySearch.jsp");
	        	rd = request.getRequestDispatcher("asset_license_key/asset_license_keySearch.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to asset_license_key/asset_license_keySearchForm.jsp");
	        	rd = request.getRequestDispatcher("asset_license_key/asset_license_keySearchForm.jsp");
	        }
        }
		rd.forward(request, response);
	}
	
}

