
package asset_license_key;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;

import pb.OptionDTO;
import pb.Utils;
import repository.Repository;
import repository.RepositoryManager;


public class Asset_license_keyRepository implements Repository {
	Asset_license_keyDAO asset_license_keyDAO = null;
	
	public void setDAO(Asset_license_keyDAO asset_license_keyDAO)
	{
		this.asset_license_keyDAO = asset_license_keyDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Asset_license_keyRepository.class);
	Map<Long, Asset_license_keyDTO> mapOfAsset_license_keyDTOToiD;
	Map<Long, Set<Asset_license_keyDTO> > mapOfAsset_license_keyDTOToassetLicenseId;
	Map<String, Set<Asset_license_keyDTO> > mapOfAsset_license_keyDTOToproductKey;


	static Asset_license_keyRepository instance = null;  
	private Asset_license_keyRepository(){
		mapOfAsset_license_keyDTOToiD = new ConcurrentHashMap<>();
		mapOfAsset_license_keyDTOToassetLicenseId = new ConcurrentHashMap<>();
		mapOfAsset_license_keyDTOToproductKey = new ConcurrentHashMap<>();
        asset_license_keyDAO = new Asset_license_keyDAO();
		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Asset_license_keyRepository getInstance(){
		if (instance == null){
			instance = new Asset_license_keyRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(asset_license_keyDAO == null)
		{
			return;
		}
		try {
			List<Asset_license_keyDTO> asset_license_keyDTOs = asset_license_keyDAO.getAllAsset_license_key(reloadAll);
			for(Asset_license_keyDTO asset_license_keyDTO : asset_license_keyDTOs) {
				Asset_license_keyDTO oldAsset_license_keyDTO = getAsset_license_keyDTOByID(asset_license_keyDTO.iD);
				if( oldAsset_license_keyDTO != null ) {
                    mapOfAsset_license_keyDTOToiD.remove(oldAsset_license_keyDTO.iD);
					
					if(mapOfAsset_license_keyDTOToassetLicenseId.containsKey(oldAsset_license_keyDTO.assetLicenseId)) {
						mapOfAsset_license_keyDTOToassetLicenseId.get(oldAsset_license_keyDTO.assetLicenseId).remove(oldAsset_license_keyDTO);
					}
					if(mapOfAsset_license_keyDTOToassetLicenseId.get(oldAsset_license_keyDTO.assetLicenseId).isEmpty()) {
						mapOfAsset_license_keyDTOToassetLicenseId.remove(oldAsset_license_keyDTO.assetLicenseId);
					}
					
					if(mapOfAsset_license_keyDTOToproductKey.containsKey(oldAsset_license_keyDTO.productKey)) {
						mapOfAsset_license_keyDTOToproductKey.get(oldAsset_license_keyDTO.productKey).remove(oldAsset_license_keyDTO);
					}
					if(mapOfAsset_license_keyDTOToproductKey.get(oldAsset_license_keyDTO.productKey).isEmpty()) {
						mapOfAsset_license_keyDTOToproductKey.remove(oldAsset_license_keyDTO.productKey);
					}
					

					
					
				}
				if(asset_license_keyDTO.isDeleted == 0) 
				{
                    mapOfAsset_license_keyDTOToiD.put(asset_license_keyDTO.iD, asset_license_keyDTO);
					
					if( ! mapOfAsset_license_keyDTOToassetLicenseId.containsKey(asset_license_keyDTO.assetLicenseId)) {
						mapOfAsset_license_keyDTOToassetLicenseId.put(asset_license_keyDTO.assetLicenseId, new HashSet<>());
					}
					mapOfAsset_license_keyDTOToassetLicenseId.get(asset_license_keyDTO.assetLicenseId).add(asset_license_keyDTO);
					
					if( ! mapOfAsset_license_keyDTOToproductKey.containsKey(asset_license_keyDTO.productKey)) {
						mapOfAsset_license_keyDTOToproductKey.put(asset_license_keyDTO.productKey, new HashSet<>());
					}
					mapOfAsset_license_keyDTOToproductKey.get(asset_license_keyDTO.productKey).add(asset_license_keyDTO);

				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Asset_license_keyDTO> getAsset_license_keyList() {
		List <Asset_license_keyDTO> asset_license_keys = new ArrayList<Asset_license_keyDTO>(this.mapOfAsset_license_keyDTOToiD.values());
		return asset_license_keys;
	}
	
	
	
	public Asset_license_keyDTO getAsset_license_keyDTOByID(long ID) {
		return mapOfAsset_license_keyDTOToiD.get(ID);
	}
	
	
	public List<Asset_license_keyDTO> getAsset_license_keyDTOByasset_license_id(long asset_license_id) {
		return new ArrayList<>( mapOfAsset_license_keyDTOToassetLicenseId.getOrDefault(asset_license_id,new HashSet<>()));
	}
	
	
	public List<Asset_license_keyDTO> getAsset_license_keyDTOByproduct_key(String product_key) {
		return new ArrayList<>( mapOfAsset_license_keyDTOToproductKey.getOrDefault(product_key,new HashSet<>()));
	}

	public String buildOptionsKey(long assetLicenseId,String language) {
		String smLanguage = language.toLowerCase();
    	if(!smLanguage.equalsIgnoreCase("bangla") && !smLanguage.equalsIgnoreCase("english"))
    	{
    		language = "english";
    	}
		List<Asset_license_keyDTO> dtosByLicense = getAsset_license_keyDTOByasset_license_id(assetLicenseId);
		List<OptionDTO> optionDTOList = null;
		optionDTOList = dtosByLicense.stream()
				.filter(dto -> !dto.isUsed)
				.map(dto -> new OptionDTO(dto.productKey, dto.productKey, String.valueOf(dto.iD)))
				.collect(Collectors.toList());
		return Utils.buildOptions(optionDTOList,language, null);
	}

	public long numberOfAvailableKey(long assetLicenseId) {
		List<Asset_license_keyDTO> dtosByLicense = getAsset_license_keyDTOByasset_license_id(assetLicenseId);
		return dtosByLicense.stream()
				.filter(dto -> !dto.isUsed)
				.count();
	}
	
	

	
	@Override
	public String getTableName() {
		return "asset_license_key";
	}
}



