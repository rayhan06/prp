package asset_license_key;
import java.util.*; 
import util.*; 


public class Asset_license_keyDTO extends CommonDTO
{

	public long assetLicenseId = -1;
    public String productKey = "";
	public long insertionDate = -1;
    public String insertedBy = "";
    public String modifiedBy = "";
    public boolean isUsed = false;
    public long employeeRecordsId = -1;
	
	
    @Override
	public String toString() {
            return "$Asset_license_keyDTO[" +
            " iD = " + iD +
            " assetLicenseId = " + assetLicenseId +
            " productKey = " + productKey +
            " insertionDate = " + insertionDate +
            " insertedBy = " + insertedBy +
            " modifiedBy = " + modifiedBy +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            " searchColumn = " + searchColumn +
            "]";
    }

}