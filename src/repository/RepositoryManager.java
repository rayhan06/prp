package repository;

import common.ConnectionAndStatementUtil;
import org.apache.log4j.Logger;
import util.LockManager;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class RepositoryManager{
    public static Logger logger = Logger.getLogger(RepositoryManager.class);
    public static long lastModifyTime = System.currentTimeMillis();
    private final Map<String, Repository> registeredRepositoryMap = new ConcurrentHashMap<>();
    public static final int REPOSITORY_CHECK_DELAY_TIME = 1000;
    public static final int REPOSITORY_SLEEP_TIME = 10000;

    private static final String getTableNamesSQLQuery = "select table_name, table_LastModificationTime from vbSequencer where table_LastModificationTime >= %d AND table_LastModificationTime<= %d";

    private RepositoryManager() {
        logger.debug("<<<<RepositoryManager start");
        start();
    }

    private static class LazyLoader{
        static final RepositoryManager INSTANCE = new RepositoryManager();
    }

    public static RepositoryManager getInstance() {
        return LazyLoader.INSTANCE;
    }

    public void addRepository(Repository p_repository) {
        if (p_repository == null || p_repository.getTableName() == null || p_repository.getTableName().length() == 0)
            return;
        if(registeredRepositoryMap.get(p_repository.getTableName()) == null){
            synchronized (LockManager.getLock(p_repository.getTableName())){
                if(registeredRepositoryMap.get(p_repository.getTableName()) == null){
                    registeredRepositoryMap.put(p_repository.getTableName(), p_repository);
                    p_repository.reload(true);
                    logger.debug("Reloading repo done from manager: " + p_repository.getTableName());
                }
            }
        }
    }

    private void start(){
        ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor(r->{
           Thread thread = new Thread(r);
           thread.setDaemon(true);
           return thread;
        });

        executorService.scheduleAtFixedRate(()-> ConnectionAndStatementUtil.getReadStatement(st->{
            long currentTime = System.currentTimeMillis();
            String sql = String.format(getTableNamesSQLQuery,lastModifyTime-REPOSITORY_CHECK_DELAY_TIME,currentTime);
            try {
                ResultSet rs = st.executeQuery(sql);
                while (rs.next()) {
                    Repository repository = registeredRepositoryMap.get(rs.getString(1));
                    if (repository != null) {
                        repository.reload(false);
                    }
                }
                lastModifyTime = currentTime-REPOSITORY_CHECK_DELAY_TIME;
                
            } catch (SQLException ex) {
                logger.error(ex);
            }
        }),0,REPOSITORY_SLEEP_TIME, TimeUnit.MILLISECONDS);
    }

    public Repository getRepository(String tableName) {
        return registeredRepositoryMap.get(tableName);
    }
}