package employee_certification;

import common.BaseServlet;
import common.EmployeeServletService;
import employee_records.Employee_recordsRepository;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import permission.MenuConstants;
import user.UserDTO;
import util.CommonDTO;
import util.HttpRequestUtils;
import util.StringUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@SuppressWarnings("Duplicates")
@WebServlet("/Employee_certificationServlet")
@MultipartConfig
public class Employee_certificationServlet extends BaseServlet implements EmployeeServletService {
    private static final long serialVersionUID = 1L;

    private final Employee_certificationDAO employee_certificationDAO = Employee_certificationDAO.getInstance();

    @Override
    public String getTableName() {
        return "employee_certification";
    }

    @Override
    public String getServletName() {
        return "Employee_certificationServlet";
    }

    @Override
    public Employee_certificationDAO getCommonDAOService() {
        return employee_certificationDAO;
    }

    @Override
    public String getAjaxAddRedirectURL(HttpServletRequest request,CommonDTO commonDTO) {
        return getAddOrEditOrDeleteRedirectURL(request,5,"certificate");
    }

    @Override
    public String getAjaxEditRedirectURL(HttpServletRequest request,CommonDTO commonDTO) {
        return getAddOrEditOrDeleteRedirectURL(request,5,"certificate");
    }

    @Override
    public String getDeleteRedirectURL(HttpServletRequest request) {
        return getAddOrEditOrDeleteRedirectURL(request,5,"certificate");
    }

    @Override
    public boolean getEditPagePermission(HttpServletRequest request) {
        return checkEditOrViewPagePermission(request);
    }

    @Override
    public boolean getViewPagePermission(HttpServletRequest request) {
        return checkEditOrViewPagePermission(request);
    }

    @Override
    public boolean getEditPermission(HttpServletRequest request) {
        return checkEditOrViewPagePermission(request);
    }

    @Override
    public boolean getAddPermission(HttpServletRequest request) {
        return checkAddPermission(request);
    }

    @Override
    public boolean getAddPagePermission(HttpServletRequest request) {
        return checkAddPermission(request);
    }

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        boolean isLangEng = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
        Employee_certificationDTO employee_certificationDTO;
        long currentTime = System.currentTimeMillis();
        if (addFlag) {
            employee_certificationDTO = new Employee_certificationDTO();
            employee_certificationDTO.insertionDate = currentTime;
            employee_certificationDTO.insertedByUserId = userDTO.ID;
            long empId;
            try{
                empId = Long.parseLong(request.getParameter("empId"));
            }catch (Exception ex){
                ex.printStackTrace();
                throw new Exception(isLangEng?"empId is not found":"empId পাওয়া যায়নি");
            }
            if(Employee_recordsRepository.getInstance().getById(empId) == null){
                throw new Exception(isLangEng?"Employee information is not found":"কর্মকর্তার তথ্য পাওয়া যায়নি");
            }
            employee_certificationDTO.employeeRecordsId = empId;
        } else {
            employee_certificationDTO = employee_certificationDAO.getDTOFromID(Long.parseLong(request.getParameter("iD")));
        }
        employee_certificationDTO.lastModificationTime = currentTime;
        employee_certificationDTO.modifiedBy = String.valueOf(userDTO.ID);
        employee_certificationDTO.certificationName = Jsoup.clean(request.getParameter("certificationName"), Whitelist.simpleText());
        if (StringUtils.isBlank(employee_certificationDTO.certificationName)){
            throw new Exception(isLangEng ? "Certification Name cannot be empty" : "সার্টিফিকেটের নাম খালি রাখা যাবে না");
        }

        employee_certificationDTO.issuingOrganization = Jsoup.clean(request.getParameter("issuingOrganization"), Whitelist.simpleText());
        if (StringUtils.isBlank(employee_certificationDTO.issuingOrganization)){
            throw new Exception(isLangEng ? "Issuing Organization cannot be empty" : "প্রদানকারী সংস্থা খালি রাখা যাবে না");
        }

        employee_certificationDTO.validFrom = Jsoup.clean(request.getParameter("validFrom"), Whitelist.simpleText());
        String Value;

        Value = request.getParameter("issuingInstituteAddress");

        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }
        logger.debug("issuingInstituteAddress = " + Value);
        if (Value != null && !Value.equalsIgnoreCase("")) {

            employee_certificationDTO.issuingInstituteAddress = (Value);
        } else {
            logger.debug("FieldName has a null Value, not updating" + " = " + Value);
        }


        Value = request.getParameter("hasExpiryCat");

        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }
        logger.debug("hasExpiryCat = " + Value);
        if (Value != null && !Value.equalsIgnoreCase("")) {

            employee_certificationDTO.hasExpiryCat = Integer.parseInt(Value);
        } else {
            logger.debug("FieldName has a null Value, not updating" + " = " + Value);
        }

        Value = request.getParameter("validUpto");

        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }
        logger.debug("validUpto = " + Value);
        if (Value != null) {

            employee_certificationDTO.validUpto = (Value);
        } else {
            logger.debug("FieldName has a null Value, not updating");
        }

        Value = request.getParameter("credentialId");

        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }
        logger.debug("credentialId = " + Value);
        if (Value != null && !Value.equalsIgnoreCase("")) {

            employee_certificationDTO.credentialId = (Value);
        } else {
            logger.debug("FieldName has a null Value, not updating" + " = " + Value);
        }

        Value = request.getParameter("filesDropzone");

        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }
        logger.debug("filesDropzone = " + Value);
        if (Value != null && !Value.equalsIgnoreCase("")) {


            logger.debug("filesDropzone = " + Value);
            if (!Value.equalsIgnoreCase("")) {
                employee_certificationDTO.filesDropzone = Long.parseLong(Value);
            } else {
                logger.debug("FieldName has a null Value, not updating" + " = " + Value);
            }

            if (!addFlag) {
                String filesDropzoneFilesToDelete = request.getParameter("filesDropzoneFilesToDelete");
                String[] deleteArray = filesDropzoneFilesToDelete.split(",");
                for (int i = 0; i < deleteArray.length; i++) {
                    if (i > 0) {
                        filesDAO.delete(Long.parseLong(deleteArray[i]));
                    }
                }
            }
        } else {
            logger.debug("FieldName has a null Value, not updating" + " = " + Value);
        }
        if (addFlag)
            employee_certificationDAO.add(employee_certificationDTO);
        else
            employee_certificationDAO.update(employee_certificationDTO);
        return employee_certificationDTO;
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[0];
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[0];
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[0];
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Employee_certificationServlet.class;
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        search(request);
        super.doGet(request, response);
    }
}

