package employee_certification;

import common.EmployeeCommonDAOService;
import employee_records.Employee_recordsDAO;
import employee_records.Employee_recordsDTO;
import org.apache.log4j.Logger;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings({"unused", "Duplicates"})
public class Employee_certificationDAO implements EmployeeCommonDAOService<Employee_certificationDTO> {

    private static final Logger logger = Logger.getLogger(Employee_certificationDAO.class);
    private static final String addSqlQuery = "INSERT INTO {tableName} (employee_records_id,certification_name,issuing_organization,issuing_institute_address,valid_from,"
            + "has_expiry_cat,valid_upto,credential_id,files_dropzone,inserted_by_user_id,insertion_date,modified_by,search_column,lastModificationTime,"
            + "isDeleted,ID) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
    private static final String updateSqlQuery = "UPDATE {tableName} SET employee_records_id=?,certification_name=?,issuing_organization=?,issuing_institute_address=?,"
            + "valid_from=?,has_expiry_cat=?,valid_upto=?,credential_id=?,files_dropzone=?,inserted_by_user_id=?,insertion_date=?,"
            + "modified_by=?,search_column=?,lastModificationTime = ? WHERE ID = ?";
    private static final Map<String, String> searchMap = new HashMap<>();

    private Employee_certificationDAO() {
        searchMap.put("certification_name", " and (certification_name = ?)");
        searchMap.put("issuing_organization", " and (issuing_organization = ?)");
        searchMap.put("credential_id", " and (credential_id = ?)");
        searchMap.put("employee_records_id_internal", " and (employee_records_id = ?)");
        searchMap.put("AnyField", " and (search_column like ?)");
    }

    private static class LazyLoader {
        static final Employee_certificationDAO INSTANCE = new Employee_certificationDAO();
    }

    public static Employee_certificationDAO getInstance() {
        return Employee_certificationDAO.LazyLoader.INSTANCE;
    }

    public void set(PreparedStatement ps, Employee_certificationDTO employee_certificationDTO, boolean isInsert) throws SQLException {
        int index = 0;
        setSearchColumn(employee_certificationDTO);
        ps.setObject(++index, employee_certificationDTO.employeeRecordsId);
        ps.setObject(++index, employee_certificationDTO.certificationName);
        ps.setObject(++index, employee_certificationDTO.issuingOrganization);
        ps.setObject(++index, employee_certificationDTO.issuingInstituteAddress);
        ps.setObject(++index, employee_certificationDTO.validFrom);
        ps.setObject(++index, employee_certificationDTO.hasExpiryCat);
        ps.setObject(++index, employee_certificationDTO.validUpto);
        ps.setObject(++index, employee_certificationDTO.credentialId);
        ps.setObject(++index, employee_certificationDTO.filesDropzone);
        ps.setObject(++index, employee_certificationDTO.insertedByUserId);
        ps.setObject(++index, employee_certificationDTO.insertionDate);
        ps.setObject(++index, employee_certificationDTO.modifiedBy);
        ps.setObject(++index, employee_certificationDTO.searchColumn);
        ps.setObject(++index, System.currentTimeMillis());
        if (isInsert) {
            ps.setObject(++index, 0);
        }
        ps.setObject(++index, employee_certificationDTO.iD);
    }

    public void setSearchColumn(Employee_certificationDTO employee_certificationDTO) {
        List<String> list = new ArrayList<>();
        try {
            Employee_recordsDTO employeeRecordsDTO = new Employee_recordsDAO().getEmployee_recordsDTOByID(employee_certificationDTO.employeeRecordsId);
            if (employeeRecordsDTO != null) {
                if (employeeRecordsDTO.nameEng != null && employeeRecordsDTO.nameEng.trim().length() > 0) {
                    list.add(employeeRecordsDTO.nameEng.trim());
                }
                if (employeeRecordsDTO.nameBng != null && employeeRecordsDTO.nameBng.trim().length() > 0) {
                    list.add(employeeRecordsDTO.nameBng.trim());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        list.add(employee_certificationDTO.certificationName);
        list.add(employee_certificationDTO.issuingOrganization);
        list.add(employee_certificationDTO.credentialId);
        list.add(employee_certificationDTO.issuingInstituteAddress);

        employee_certificationDTO.searchColumn = String.join(" ", list);
    }

    public Employee_certificationDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Employee_certificationDTO employee_certificationDTO = new Employee_certificationDTO();
            employee_certificationDTO.iD = rs.getLong("ID");
            employee_certificationDTO.employeeRecordsId = rs.getLong("employee_records_id");
            employee_certificationDTO.certificationName = rs.getString("certification_name");
            employee_certificationDTO.issuingOrganization = rs.getString("issuing_organization");
            employee_certificationDTO.issuingInstituteAddress = rs.getString("issuing_institute_address");
            employee_certificationDTO.validFrom = rs.getString("valid_from");
            employee_certificationDTO.hasExpiryCat = rs.getInt("has_expiry_cat");
            employee_certificationDTO.validUpto = rs.getString("valid_upto");
            employee_certificationDTO.credentialId = rs.getString("credential_id");
            employee_certificationDTO.filesDropzone = rs.getLong("files_dropzone");
            employee_certificationDTO.insertedByUserId = rs.getLong("inserted_by_user_id");
            employee_certificationDTO.insertionDate = rs.getLong("insertion_date");
            employee_certificationDTO.modifiedBy = rs.getString("modified_by");
            employee_certificationDTO.searchColumn = rs.getString("search_column");
            employee_certificationDTO.isDeleted = rs.getInt("isDeleted");
            employee_certificationDTO.lastModificationTime = rs.getLong("lastModificationTime");
            return employee_certificationDTO;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    @Override
    public String getTableName() {
        return "employee_certification";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Employee_certificationDTO) commonDTO, addSqlQuery, true);
    }

    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Employee_certificationDTO) commonDTO, updateSqlQuery, false);
    }

    //add repository
    public List<Employee_certificationDTO> getAllEmployee_certification(boolean isFirstReload) {
        return getAllDTOs(isFirstReload);
    }

}
	