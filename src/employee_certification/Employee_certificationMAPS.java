package employee_certification;
import java.util.*; 
import util.*;


public class Employee_certificationMAPS extends CommonMaps
{	
	public Employee_certificationMAPS(String tableName)
	{
		
		java_allfield_type_map.put("certification_name".toLowerCase(), "String");
		java_allfield_type_map.put("issuing_organization".toLowerCase(), "String");
		java_allfield_type_map.put("issuing_institute_address".toLowerCase(), "String");
		java_allfield_type_map.put("valid_from".toLowerCase(), "String");
		java_allfield_type_map.put("valid_upto".toLowerCase(), "String");
		java_allfield_type_map.put("credential_id".toLowerCase(), "String");

		java_anyfield_search_map.put("language_text.languageTextEnglish", "String");
		java_anyfield_search_map.put("language_text.languageTextBangla", "String");

		java_anyfield_search_map.put(tableName + ".employee_records_id".toLowerCase(), "Long");
		java_anyfield_search_map.put(tableName + ".certification_name".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".issuing_organization".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".issuing_institute_address".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".valid_from".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".valid_upto".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".credential_id".toLowerCase(), "String");

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("employeeRecordsId".toLowerCase(), "employeeRecordsId".toLowerCase());
		java_DTO_map.put("certificationName".toLowerCase(), "certificationName".toLowerCase());
		java_DTO_map.put("issuingOrganization".toLowerCase(), "issuingOrganization".toLowerCase());
		java_DTO_map.put("issuingInstituteAddress".toLowerCase(), "issuingInstituteAddress".toLowerCase());
		java_DTO_map.put("validFrom".toLowerCase(), "validFrom".toLowerCase());
		java_DTO_map.put("hasExpiryCat".toLowerCase(), "hasExpiryCat".toLowerCase());
		java_DTO_map.put("validUpto".toLowerCase(), "validUpto".toLowerCase());
		java_DTO_map.put("credentialId".toLowerCase(), "credentialId".toLowerCase());
		java_DTO_map.put("filesDropzone".toLowerCase(), "filesDropzone".toLowerCase());
		java_DTO_map.put("insertedByUserId".toLowerCase(), "insertedByUserId".toLowerCase());
		java_DTO_map.put("insertionDate".toLowerCase(), "insertionDate".toLowerCase());
		java_DTO_map.put("modifiedBy".toLowerCase(), "modifiedBy".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("employee_records_id".toLowerCase(), "employeeRecordsId".toLowerCase());
		java_SQL_map.put("certification_name".toLowerCase(), "certificationName".toLowerCase());
		java_SQL_map.put("issuing_organization".toLowerCase(), "issuingOrganization".toLowerCase());
		java_SQL_map.put("issuing_institute_address".toLowerCase(), "issuingInstituteAddress".toLowerCase());
		java_SQL_map.put("valid_from".toLowerCase(), "validFrom".toLowerCase());
		java_SQL_map.put("has_expiry_cat".toLowerCase(), "hasExpiryCat".toLowerCase());
		java_SQL_map.put("valid_upto".toLowerCase(), "validUpto".toLowerCase());
		java_SQL_map.put("credential_id".toLowerCase(), "credentialId".toLowerCase());
		java_SQL_map.put("files_dropzone".toLowerCase(), "filesDropzone".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Employee Records Id".toLowerCase(), "employeeRecordsId".toLowerCase());
		java_Text_map.put("Certification Name".toLowerCase(), "certificationName".toLowerCase());
		java_Text_map.put("Issuing Organization".toLowerCase(), "issuingOrganization".toLowerCase());
		java_Text_map.put("Issuing Institute Address".toLowerCase(), "issuingInstituteAddress".toLowerCase());
		java_Text_map.put("Valid From".toLowerCase(), "validFrom".toLowerCase());
		java_Text_map.put("Has Expiry Cat".toLowerCase(), "hasExpiryCat".toLowerCase());
		java_Text_map.put("Valid Upto".toLowerCase(), "validUpto".toLowerCase());
		java_Text_map.put("Credential Id".toLowerCase(), "credentialId".toLowerCase());
		java_Text_map.put("Files Dropzone".toLowerCase(), "filesDropzone".toLowerCase());
		java_Text_map.put("Inserted By User Id".toLowerCase(), "insertedByUserId".toLowerCase());
		java_Text_map.put("Insertion Date".toLowerCase(), "insertionDate".toLowerCase());
		java_Text_map.put("Modified By".toLowerCase(), "modifiedBy".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}