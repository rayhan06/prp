package employee_certification;

import util.*;


public class Employee_certificationDTO extends CommonEmployeeDTO {
    public String certificationName = "";
    public String issuingOrganization = "";
    public String issuingInstituteAddress = "";
    public String validFrom = "";
    public int hasExpiryCat = 0;
    public String validUpto = "";
    public String credentialId = "";
    public long filesDropzone = 0;
    public long insertedByUserId = 0;
    public long insertionDate = 0;
    public String modifiedBy = "";


    @Override
    public String toString() {
        return "$Employee_certificationDTO[" +
                " iD = " + iD +
                " employeeRecordsId = " + employeeRecordsId +
                " certificationName = " + certificationName +
                " issuingOrganization = " + issuingOrganization +
                " issuingInstituteAddress = " + issuingInstituteAddress +
                " validFrom = " + validFrom +
                " hasExpiryCat = " + hasExpiryCat +
                " validUpto = " + validUpto +
                " credentialId = " + credentialId +
                " filesDropzone = " + filesDropzone +
                " insertedByUserId = " + insertedByUserId +
                " insertionDate = " + insertionDate +
                " modifiedBy = " + modifiedBy +
                " isDeleted = " + isDeleted +
                " lastModificationTime = " + lastModificationTime +
                "]";
    }

}