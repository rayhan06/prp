package employee_absent_approved_history;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class Employee_absent_approved_historyRepository implements Repository {
	Employee_absent_approved_historyDAO employee_absent_approved_historyDAO = null;
	
	public void setDAO(Employee_absent_approved_historyDAO employee_absent_approved_historyDAO)
	{
		this.employee_absent_approved_historyDAO = employee_absent_approved_historyDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Employee_absent_approved_historyRepository.class);
	Map<Long, Employee_absent_approved_historyDTO>mapOfEmployee_absent_approved_historyDTOToiD;
	Map<Long, Set<Employee_absent_approved_historyDTO> >mapOfEmployee_absent_approved_historyDTOToemployeeRecordsId;
	Map<Long, Set<Employee_absent_approved_historyDTO> >mapOfEmployee_absent_approved_historyDTOToapprovedByEmpRecordsId;
	Map<Long, Set<Employee_absent_approved_historyDTO> >mapOfEmployee_absent_approved_historyDTOToabsentDate;
	Map<Long, Set<Employee_absent_approved_historyDTO> >mapOfEmployee_absent_approved_historyDTOToapprovalDate;
	Map<String, Set<Employee_absent_approved_historyDTO> >mapOfEmployee_absent_approved_historyDTOTocomments;
	Map<Long, Set<Employee_absent_approved_historyDTO> >mapOfEmployee_absent_approved_historyDTOToinsertionDate;
	Map<String, Set<Employee_absent_approved_historyDTO> >mapOfEmployee_absent_approved_historyDTOToinsertedBy;
	Map<String, Set<Employee_absent_approved_historyDTO> >mapOfEmployee_absent_approved_historyDTOTomodifiedBy;
	Map<Long, Set<Employee_absent_approved_historyDTO> >mapOfEmployee_absent_approved_historyDTOTolastModificationTime;


	static Employee_absent_approved_historyRepository instance = null;  
	private Employee_absent_approved_historyRepository(){
		mapOfEmployee_absent_approved_historyDTOToiD = new ConcurrentHashMap<>();
		mapOfEmployee_absent_approved_historyDTOToemployeeRecordsId = new ConcurrentHashMap<>();
		mapOfEmployee_absent_approved_historyDTOToapprovedByEmpRecordsId = new ConcurrentHashMap<>();
		mapOfEmployee_absent_approved_historyDTOToabsentDate = new ConcurrentHashMap<>();
		mapOfEmployee_absent_approved_historyDTOToapprovalDate = new ConcurrentHashMap<>();
		mapOfEmployee_absent_approved_historyDTOTocomments = new ConcurrentHashMap<>();
		mapOfEmployee_absent_approved_historyDTOToinsertionDate = new ConcurrentHashMap<>();
		mapOfEmployee_absent_approved_historyDTOToinsertedBy = new ConcurrentHashMap<>();
		mapOfEmployee_absent_approved_historyDTOTomodifiedBy = new ConcurrentHashMap<>();
		mapOfEmployee_absent_approved_historyDTOTolastModificationTime = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Employee_absent_approved_historyRepository getInstance(){
		if (instance == null){
			instance = new Employee_absent_approved_historyRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(employee_absent_approved_historyDAO == null)
		{
			return;
		}
		try {
			List<Employee_absent_approved_historyDTO> employee_absent_approved_historyDTOs = employee_absent_approved_historyDAO.getAllEmployee_absent_approved_history(reloadAll);
			for(Employee_absent_approved_historyDTO employee_absent_approved_historyDTO : employee_absent_approved_historyDTOs) {
				Employee_absent_approved_historyDTO oldEmployee_absent_approved_historyDTO = getEmployee_absent_approved_historyDTOByID(employee_absent_approved_historyDTO.iD);
				if( oldEmployee_absent_approved_historyDTO != null ) {
					mapOfEmployee_absent_approved_historyDTOToiD.remove(oldEmployee_absent_approved_historyDTO.iD);
				
					if(mapOfEmployee_absent_approved_historyDTOToemployeeRecordsId.containsKey(oldEmployee_absent_approved_historyDTO.employeeRecordsId)) {
						mapOfEmployee_absent_approved_historyDTOToemployeeRecordsId.get(oldEmployee_absent_approved_historyDTO.employeeRecordsId).remove(oldEmployee_absent_approved_historyDTO);
					}
					if(mapOfEmployee_absent_approved_historyDTOToemployeeRecordsId.get(oldEmployee_absent_approved_historyDTO.employeeRecordsId).isEmpty()) {
						mapOfEmployee_absent_approved_historyDTOToemployeeRecordsId.remove(oldEmployee_absent_approved_historyDTO.employeeRecordsId);
					}
					
					if(mapOfEmployee_absent_approved_historyDTOToapprovedByEmpRecordsId.containsKey(oldEmployee_absent_approved_historyDTO.approvedByEmpRecordsId)) {
						mapOfEmployee_absent_approved_historyDTOToapprovedByEmpRecordsId.get(oldEmployee_absent_approved_historyDTO.approvedByEmpRecordsId).remove(oldEmployee_absent_approved_historyDTO);
					}
					if(mapOfEmployee_absent_approved_historyDTOToapprovedByEmpRecordsId.get(oldEmployee_absent_approved_historyDTO.approvedByEmpRecordsId).isEmpty()) {
						mapOfEmployee_absent_approved_historyDTOToapprovedByEmpRecordsId.remove(oldEmployee_absent_approved_historyDTO.approvedByEmpRecordsId);
					}
					
					if(mapOfEmployee_absent_approved_historyDTOToabsentDate.containsKey(oldEmployee_absent_approved_historyDTO.absentDate)) {
						mapOfEmployee_absent_approved_historyDTOToabsentDate.get(oldEmployee_absent_approved_historyDTO.absentDate).remove(oldEmployee_absent_approved_historyDTO);
					}
					if(mapOfEmployee_absent_approved_historyDTOToabsentDate.get(oldEmployee_absent_approved_historyDTO.absentDate).isEmpty()) {
						mapOfEmployee_absent_approved_historyDTOToabsentDate.remove(oldEmployee_absent_approved_historyDTO.absentDate);
					}
					
					if(mapOfEmployee_absent_approved_historyDTOToapprovalDate.containsKey(oldEmployee_absent_approved_historyDTO.approvalDate)) {
						mapOfEmployee_absent_approved_historyDTOToapprovalDate.get(oldEmployee_absent_approved_historyDTO.approvalDate).remove(oldEmployee_absent_approved_historyDTO);
					}
					if(mapOfEmployee_absent_approved_historyDTOToapprovalDate.get(oldEmployee_absent_approved_historyDTO.approvalDate).isEmpty()) {
						mapOfEmployee_absent_approved_historyDTOToapprovalDate.remove(oldEmployee_absent_approved_historyDTO.approvalDate);
					}
					
					if(mapOfEmployee_absent_approved_historyDTOTocomments.containsKey(oldEmployee_absent_approved_historyDTO.comments)) {
						mapOfEmployee_absent_approved_historyDTOTocomments.get(oldEmployee_absent_approved_historyDTO.comments).remove(oldEmployee_absent_approved_historyDTO);
					}
					if(mapOfEmployee_absent_approved_historyDTOTocomments.get(oldEmployee_absent_approved_historyDTO.comments).isEmpty()) {
						mapOfEmployee_absent_approved_historyDTOTocomments.remove(oldEmployee_absent_approved_historyDTO.comments);
					}
					
					if(mapOfEmployee_absent_approved_historyDTOToinsertionDate.containsKey(oldEmployee_absent_approved_historyDTO.insertionDate)) {
						mapOfEmployee_absent_approved_historyDTOToinsertionDate.get(oldEmployee_absent_approved_historyDTO.insertionDate).remove(oldEmployee_absent_approved_historyDTO);
					}
					if(mapOfEmployee_absent_approved_historyDTOToinsertionDate.get(oldEmployee_absent_approved_historyDTO.insertionDate).isEmpty()) {
						mapOfEmployee_absent_approved_historyDTOToinsertionDate.remove(oldEmployee_absent_approved_historyDTO.insertionDate);
					}
					
					if(mapOfEmployee_absent_approved_historyDTOToinsertedBy.containsKey(oldEmployee_absent_approved_historyDTO.insertedBy)) {
						mapOfEmployee_absent_approved_historyDTOToinsertedBy.get(oldEmployee_absent_approved_historyDTO.insertedBy).remove(oldEmployee_absent_approved_historyDTO);
					}
					if(mapOfEmployee_absent_approved_historyDTOToinsertedBy.get(oldEmployee_absent_approved_historyDTO.insertedBy).isEmpty()) {
						mapOfEmployee_absent_approved_historyDTOToinsertedBy.remove(oldEmployee_absent_approved_historyDTO.insertedBy);
					}
					
					if(mapOfEmployee_absent_approved_historyDTOTomodifiedBy.containsKey(oldEmployee_absent_approved_historyDTO.modifiedBy)) {
						mapOfEmployee_absent_approved_historyDTOTomodifiedBy.get(oldEmployee_absent_approved_historyDTO.modifiedBy).remove(oldEmployee_absent_approved_historyDTO);
					}
					if(mapOfEmployee_absent_approved_historyDTOTomodifiedBy.get(oldEmployee_absent_approved_historyDTO.modifiedBy).isEmpty()) {
						mapOfEmployee_absent_approved_historyDTOTomodifiedBy.remove(oldEmployee_absent_approved_historyDTO.modifiedBy);
					}
					
					if(mapOfEmployee_absent_approved_historyDTOTolastModificationTime.containsKey(oldEmployee_absent_approved_historyDTO.lastModificationTime)) {
						mapOfEmployee_absent_approved_historyDTOTolastModificationTime.get(oldEmployee_absent_approved_historyDTO.lastModificationTime).remove(oldEmployee_absent_approved_historyDTO);
					}
					if(mapOfEmployee_absent_approved_historyDTOTolastModificationTime.get(oldEmployee_absent_approved_historyDTO.lastModificationTime).isEmpty()) {
						mapOfEmployee_absent_approved_historyDTOTolastModificationTime.remove(oldEmployee_absent_approved_historyDTO.lastModificationTime);
					}
					
					
				}
				if(employee_absent_approved_historyDTO.isDeleted == 0) 
				{
					
					mapOfEmployee_absent_approved_historyDTOToiD.put(employee_absent_approved_historyDTO.iD, employee_absent_approved_historyDTO);
				
					if( ! mapOfEmployee_absent_approved_historyDTOToemployeeRecordsId.containsKey(employee_absent_approved_historyDTO.employeeRecordsId)) {
						mapOfEmployee_absent_approved_historyDTOToemployeeRecordsId.put(employee_absent_approved_historyDTO.employeeRecordsId, new HashSet<>());
					}
					mapOfEmployee_absent_approved_historyDTOToemployeeRecordsId.get(employee_absent_approved_historyDTO.employeeRecordsId).add(employee_absent_approved_historyDTO);
					
					if( ! mapOfEmployee_absent_approved_historyDTOToapprovedByEmpRecordsId.containsKey(employee_absent_approved_historyDTO.approvedByEmpRecordsId)) {
						mapOfEmployee_absent_approved_historyDTOToapprovedByEmpRecordsId.put(employee_absent_approved_historyDTO.approvedByEmpRecordsId, new HashSet<>());
					}
					mapOfEmployee_absent_approved_historyDTOToapprovedByEmpRecordsId.get(employee_absent_approved_historyDTO.approvedByEmpRecordsId).add(employee_absent_approved_historyDTO);
					
					if( ! mapOfEmployee_absent_approved_historyDTOToabsentDate.containsKey(employee_absent_approved_historyDTO.absentDate)) {
						mapOfEmployee_absent_approved_historyDTOToabsentDate.put(employee_absent_approved_historyDTO.absentDate, new HashSet<>());
					}
					mapOfEmployee_absent_approved_historyDTOToabsentDate.get(employee_absent_approved_historyDTO.absentDate).add(employee_absent_approved_historyDTO);
					
					if( ! mapOfEmployee_absent_approved_historyDTOToapprovalDate.containsKey(employee_absent_approved_historyDTO.approvalDate)) {
						mapOfEmployee_absent_approved_historyDTOToapprovalDate.put(employee_absent_approved_historyDTO.approvalDate, new HashSet<>());
					}
					mapOfEmployee_absent_approved_historyDTOToapprovalDate.get(employee_absent_approved_historyDTO.approvalDate).add(employee_absent_approved_historyDTO);
					
					if( ! mapOfEmployee_absent_approved_historyDTOTocomments.containsKey(employee_absent_approved_historyDTO.comments)) {
						mapOfEmployee_absent_approved_historyDTOTocomments.put(employee_absent_approved_historyDTO.comments, new HashSet<>());
					}
					mapOfEmployee_absent_approved_historyDTOTocomments.get(employee_absent_approved_historyDTO.comments).add(employee_absent_approved_historyDTO);
					
					if( ! mapOfEmployee_absent_approved_historyDTOToinsertionDate.containsKey(employee_absent_approved_historyDTO.insertionDate)) {
						mapOfEmployee_absent_approved_historyDTOToinsertionDate.put(employee_absent_approved_historyDTO.insertionDate, new HashSet<>());
					}
					mapOfEmployee_absent_approved_historyDTOToinsertionDate.get(employee_absent_approved_historyDTO.insertionDate).add(employee_absent_approved_historyDTO);
					
					if( ! mapOfEmployee_absent_approved_historyDTOToinsertedBy.containsKey(employee_absent_approved_historyDTO.insertedBy)) {
						mapOfEmployee_absent_approved_historyDTOToinsertedBy.put(employee_absent_approved_historyDTO.insertedBy, new HashSet<>());
					}
					mapOfEmployee_absent_approved_historyDTOToinsertedBy.get(employee_absent_approved_historyDTO.insertedBy).add(employee_absent_approved_historyDTO);
					
					if( ! mapOfEmployee_absent_approved_historyDTOTomodifiedBy.containsKey(employee_absent_approved_historyDTO.modifiedBy)) {
						mapOfEmployee_absent_approved_historyDTOTomodifiedBy.put(employee_absent_approved_historyDTO.modifiedBy, new HashSet<>());
					}
					mapOfEmployee_absent_approved_historyDTOTomodifiedBy.get(employee_absent_approved_historyDTO.modifiedBy).add(employee_absent_approved_historyDTO);
					
					if( ! mapOfEmployee_absent_approved_historyDTOTolastModificationTime.containsKey(employee_absent_approved_historyDTO.lastModificationTime)) {
						mapOfEmployee_absent_approved_historyDTOTolastModificationTime.put(employee_absent_approved_historyDTO.lastModificationTime, new HashSet<>());
					}
					mapOfEmployee_absent_approved_historyDTOTolastModificationTime.get(employee_absent_approved_historyDTO.lastModificationTime).add(employee_absent_approved_historyDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Employee_absent_approved_historyDTO> getEmployee_absent_approved_historyList() {
		List <Employee_absent_approved_historyDTO> employee_absent_approved_historys = new ArrayList<Employee_absent_approved_historyDTO>(this.mapOfEmployee_absent_approved_historyDTOToiD.values());
		return employee_absent_approved_historys;
	}
	
	
	public Employee_absent_approved_historyDTO getEmployee_absent_approved_historyDTOByID( long ID){
		return mapOfEmployee_absent_approved_historyDTOToiD.get(ID);
	}
	
	
	public List<Employee_absent_approved_historyDTO> getEmployee_absent_approved_historyDTOByemployee_records_id(long employee_records_id) {
		return new ArrayList<>( mapOfEmployee_absent_approved_historyDTOToemployeeRecordsId.getOrDefault(employee_records_id,new HashSet<>()));
	}
	
	
	public List<Employee_absent_approved_historyDTO> getEmployee_absent_approved_historyDTOByapproved_by_emp_records_id(long approved_by_emp_records_id) {
		return new ArrayList<>( mapOfEmployee_absent_approved_historyDTOToapprovedByEmpRecordsId.getOrDefault(approved_by_emp_records_id,new HashSet<>()));
	}
	
	
	public List<Employee_absent_approved_historyDTO> getEmployee_absent_approved_historyDTOByabsent_date(long absent_date) {
		return new ArrayList<>( mapOfEmployee_absent_approved_historyDTOToabsentDate.getOrDefault(absent_date,new HashSet<>()));
	}
	
	
	public List<Employee_absent_approved_historyDTO> getEmployee_absent_approved_historyDTOByapproval_date(long approval_date) {
		return new ArrayList<>( mapOfEmployee_absent_approved_historyDTOToapprovalDate.getOrDefault(approval_date,new HashSet<>()));
	}
	
	
	public List<Employee_absent_approved_historyDTO> getEmployee_absent_approved_historyDTOBycomments(String comments) {
		return new ArrayList<>( mapOfEmployee_absent_approved_historyDTOTocomments.getOrDefault(comments,new HashSet<>()));
	}
	
	
	public List<Employee_absent_approved_historyDTO> getEmployee_absent_approved_historyDTOByinsertion_date(long insertion_date) {
		return new ArrayList<>( mapOfEmployee_absent_approved_historyDTOToinsertionDate.getOrDefault(insertion_date,new HashSet<>()));
	}
	
	
	public List<Employee_absent_approved_historyDTO> getEmployee_absent_approved_historyDTOByinserted_by(String inserted_by) {
		return new ArrayList<>( mapOfEmployee_absent_approved_historyDTOToinsertedBy.getOrDefault(inserted_by,new HashSet<>()));
	}
	
	
	public List<Employee_absent_approved_historyDTO> getEmployee_absent_approved_historyDTOBymodified_by(String modified_by) {
		return new ArrayList<>( mapOfEmployee_absent_approved_historyDTOTomodifiedBy.getOrDefault(modified_by,new HashSet<>()));
	}
	
	
	public List<Employee_absent_approved_historyDTO> getEmployee_absent_approved_historyDTOBylastModificationTime(long lastModificationTime) {
		return new ArrayList<>( mapOfEmployee_absent_approved_historyDTOTolastModificationTime.getOrDefault(lastModificationTime,new HashSet<>()));
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "employee_absent_approved_history";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


