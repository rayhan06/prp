package employee_absent_approved_history;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;
import sessionmanager.SessionConstants;
import java.sql.SQLException;


import org.apache.log4j.Logger;

import dbm.*;

import repository.RepositoryManager;

import util.*;
import pb.*;
import user.UserDTO;

public class Employee_absent_approved_historyDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());
	

	
	public Employee_absent_approved_historyDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";
		commonMaps = new Employee_absent_approved_historyMAPS(tableName);
		columnNames = new String[] 
		{
			"ID",
			"employee_records_id",
			"approved_by_emp_records_id",
			"absent_date",
			"approval_date",
			"comments",
			"insertion_date",
			"inserted_by",
			"modified_by",
			"isDeleted",
			"lastModificationTime"
		};
	}
	
	public Employee_absent_approved_historyDAO()
	{
		this("employee_absent_approved_history");		
	}
	
	public void setSearchColumn(Employee_absent_approved_historyDTO employee_absent_approved_historyDTO)
	{
		employee_absent_approved_historyDTO.searchColumn = "";
		employee_absent_approved_historyDTO.searchColumn += employee_absent_approved_historyDTO.comments + " ";
		employee_absent_approved_historyDTO.searchColumn += employee_absent_approved_historyDTO.insertedBy + " ";
		employee_absent_approved_historyDTO.searchColumn += employee_absent_approved_historyDTO.modifiedBy + " ";
		
	}
	
	public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException
	{
		Employee_absent_approved_historyDTO employee_absent_approved_historyDTO = (Employee_absent_approved_historyDTO)commonDTO;
		int index = 1;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(employee_absent_approved_historyDTO);
		if(isInsert)
		{
			ps.setObject(index++,employee_absent_approved_historyDTO.iD);
		}
		ps.setObject(index++,employee_absent_approved_historyDTO.employeeRecordsId);
		ps.setObject(index++,employee_absent_approved_historyDTO.approvedByEmpRecordsId);
		ps.setObject(index++,employee_absent_approved_historyDTO.absentDate);
		ps.setObject(index++,employee_absent_approved_historyDTO.approvalDate);
		ps.setObject(index++,employee_absent_approved_historyDTO.comments);
		ps.setObject(index++,employee_absent_approved_historyDTO.insertionDate);
		ps.setObject(index++,employee_absent_approved_historyDTO.insertedBy);
		ps.setObject(index++,employee_absent_approved_historyDTO.modifiedBy);
		if(isInsert)
		{
			ps.setObject(index++, 0);
			ps.setObject(index++, lastModificationTime);
		}
	}
	
	public void get(Employee_absent_approved_historyDTO employee_absent_approved_historyDTO, ResultSet rs) throws SQLException
	{
		employee_absent_approved_historyDTO.iD = rs.getLong("ID");
		employee_absent_approved_historyDTO.employeeRecordsId = rs.getLong("employee_records_id");
		employee_absent_approved_historyDTO.approvedByEmpRecordsId = rs.getLong("approved_by_emp_records_id");
		employee_absent_approved_historyDTO.absentDate = rs.getLong("absent_date");
		employee_absent_approved_historyDTO.approvalDate = rs.getLong("approval_date");
		employee_absent_approved_historyDTO.comments = rs.getString("comments");
		employee_absent_approved_historyDTO.insertionDate = rs.getLong("insertion_date");
		employee_absent_approved_historyDTO.insertedBy = rs.getString("inserted_by");
		employee_absent_approved_historyDTO.modifiedBy = rs.getString("modified_by");
		employee_absent_approved_historyDTO.isDeleted = rs.getInt("isDeleted");
		employee_absent_approved_historyDTO.lastModificationTime = rs.getLong("lastModificationTime");
	}
	
	
	
	
		
	

	//need another getter for repository
	public Employee_absent_approved_historyDTO getDTOByID (long ID)
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Employee_absent_approved_historyDTO employee_absent_approved_historyDTO = null;
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
			
            sql += " WHERE ID=" + ID;
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			if(rs.next()){
				employee_absent_approved_historyDTO = new Employee_absent_approved_historyDTO();

				get(employee_absent_approved_historyDTO, rs);

			}			
			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return employee_absent_approved_historyDTO;
	}
	
	
	
	
	public List<Employee_absent_approved_historyDTO> getDTOs(Collection recordIDs){
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Employee_absent_approved_historyDTO employee_absent_approved_historyDTO = null;
		List<Employee_absent_approved_historyDTO> employee_absent_approved_historyDTOList = new ArrayList<>();
		if(recordIDs.isEmpty()){
			return employee_absent_approved_historyDTOList;
		}
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
            
            sql += " WHERE ID IN ( ";

			for(int i = 0;i<recordIDs.size();i++){
				if(i!=0){
					sql+=",";
				}
				sql+=((ArrayList)recordIDs).get(i);
			}
			sql+=")  order by lastModificationTime desc";
			
			printSql(sql);
			
			logger.debug("sql " + sql);
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				employee_absent_approved_historyDTO = new Employee_absent_approved_historyDTO();
				get(employee_absent_approved_historyDTO, rs);
				System.out.println("got this DTO: " + employee_absent_approved_historyDTO);
				
				employee_absent_approved_historyDTOList.add(employee_absent_approved_historyDTO);

			}			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return employee_absent_approved_historyDTOList;
	
	}
	
	

	
	
	
	//add repository
	public List<Employee_absent_approved_historyDTO> getAllEmployee_absent_approved_history (boolean isFirstReload)
    {
		List<Employee_absent_approved_historyDTO> employee_absent_approved_historyDTOList = new ArrayList<>();

		String sql = "SELECT * FROM employee_absent_approved_history";
		sql += " WHERE ";
	

		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by employee_absent_approved_history.lastModificationTime desc";
		printSql(sql);
		
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;

		
		try{
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();
			rs = stmt.executeQuery(sql);
			

			while(rs.next()){
				Employee_absent_approved_historyDTO employee_absent_approved_historyDTO = new Employee_absent_approved_historyDTO();
				get(employee_absent_approved_historyDTO, rs);
				
				employee_absent_approved_historyDTOList.add(employee_absent_approved_historyDTO);
			}			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}

		return employee_absent_approved_historyDTOList;
    }

	
	public List<Employee_absent_approved_historyDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO)
	{
		return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
	}

	
	public List<Employee_absent_approved_historyDTO> getDTOs(Hashtable  p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
			String filter, boolean tableHasJobCat)
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		List<Employee_absent_approved_historyDTO> employee_absent_approved_historyDTOList = new ArrayList<>();

		try{
			
			String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat);
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				Employee_absent_approved_historyDTO employee_absent_approved_historyDTO = new Employee_absent_approved_historyDTO();
				get(employee_absent_approved_historyDTO, rs);
				
				employee_absent_approved_historyDTOList.add(employee_absent_approved_historyDTO);
			}					
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return employee_absent_approved_historyDTOList;
	
	}
	public String getSqlWithSearchCriteriaForNormalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
			UserDTO userDTO, String filter, boolean tableHasJobCat)
    {
		boolean viewAll = false;
		
		if(p_searchCriteria != null && p_searchCriteria.get("ViewAll") != null)
		{
			System.out.println("ViewAll = " + p_searchCriteria.get("ViewAll"));
			viewAll = true;
		}
		else
		{
			System.out.println("ViewAll is null ");
		}
		
		String sql = "SELECT ";
		if(category == GETIDS)
		{
			sql += " " + tableName + ".ID as ID ";
		}
		else if(category == GETCOUNT)
		{
			sql += " count( " + tableName + ".ID) as countID ";
		}
		else if(category == GETDTOS)
		{
			sql += " distinct " + tableName + ".* ";
		}
		sql += "FROM " + tableName + " ";
		sql += joinSQL;
		

		
		String AnyfieldSql = "";
		String AllFieldSql = "";
		
		if(p_searchCriteria != null)
		{
						
			
			Enumeration names = p_searchCriteria.keys();
			String str, value;
			
			AnyfieldSql = "(";
			
			if(p_searchCriteria.get("AnyField")!= null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase(""))
			{
				AnyfieldSql+= tableName + ".search_column like '%" + p_searchCriteria.get("AnyField").toString() + "%'";			
			}
			AnyfieldSql += ")";
			System.out.println("AnyfieldSql = " + AnyfieldSql);
			
			AllFieldSql = "(";
			int i = 0;
			while(names.hasMoreElements())
			{				
				str = (String) names.nextElement();
				value = (String)p_searchCriteria.get(str);
		        System.out.println(str + ": " + value);
				if(value != null && !value.equalsIgnoreCase("") && (
						 str.equals("absent_date_start")
						|| str.equals("absent_date_end")
						|| str.equals("approval_date_start")
						|| str.equals("approval_date_end")
						|| str.equals("comments")
						|| str.equals("insertion_date_start")
						|| str.equals("insertion_date_end")
						|| str.equals("inserted_by")
						|| str.equals("modified_by")
				)
						
				)
				{
					if(p_searchCriteria.get(str).equals("any"))
					{
						continue;
					}

					if( i > 0)
					{
						AllFieldSql+= " AND  ";
					}
					
					 if(str.equals("absent_date_start"))
					{
						AllFieldSql += "" + tableName + ".absent_date >= " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("absent_date_end"))
					{
						AllFieldSql += "" + tableName + ".absent_date <= " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("approval_date_start"))
					{
						AllFieldSql += "" + tableName + ".approval_date >= " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("approval_date_end"))
					{
						AllFieldSql += "" + tableName + ".approval_date <= " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("comments"))
					{
						AllFieldSql += "" + tableName + ".comments like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("insertion_date_start"))
					{
						AllFieldSql += "" + tableName + ".insertion_date >= " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("insertion_date_end"))
					{
						AllFieldSql += "" + tableName + ".insertion_date <= " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("inserted_by"))
					{
						AllFieldSql += "" + tableName + ".inserted_by like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("modified_by"))
					{
						AllFieldSql += "" + tableName + ".modified_by like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					
					
				}
			}
			
			AllFieldSql += ")";
			System.out.println("AllFieldSql = " + AllFieldSql);
			
			
		}
		
		
		sql += " WHERE ";
		
		sql += " (" + tableName + ".isDeleted = 0 ";			
		sql += ")";
		
		
		if(!filter.equalsIgnoreCase(""))
		{
			sql += " and " + filter + " ";
		}
		
		if(!AnyfieldSql.equals("()") && !AnyfieldSql.equals(""))
		{
			sql += " AND " + AnyfieldSql;
			
		}
		if(!AllFieldSql.equals("()") && !AllFieldSql.equals(""))
		{			
			sql += " AND " + AllFieldSql;
		}
		
	
			
		sql += " order by " + tableName + ".lastModificationTime desc ";

		printSql(sql);
		
		if(limit >= 0)
		{
			sql += " limit " + limit;
		}
		if(offset >= 0)
		{
			sql += " offset " + offset;
		}
		
		System.out.println("-------------- sql = " + sql);
		
		return sql;
    }
	
	public String getSqlWithSearchCriteria(Hashtable p_searchCriteria, int limit, int offset, int category,
			boolean isPermanentTable, UserDTO userDTO, String filter, boolean tableHasJobCat)
    {		
		return getSqlWithSearchCriteriaForNormalSearch(p_searchCriteria, limit, offset, category, userDTO, filter, tableHasJobCat);	
    }

    public int approvedDaysInRange(long employeeRecordsId,long fromDate, long toDate) {
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		int cnt = 0;
		Employee_absent_approved_historyDTO employee_absent_approved_historyDTO = null;
		try{
			String sql = "SELECT count(*) as cnt ";
			sql += " FROM " + tableName;
			sql += " WHERE employee_records_id = " + employeeRecordsId;
			sql += " And absent_date >= " + fromDate;
			sql += " And absent_date <= " + toDate;
			sql += " And isDeleted = 0";

			printSql(sql);

			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			if(rs.next()){

				cnt = rs.getInt( "cnt" );

			}


		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}

			try{
				if (connection != null)
				{
					DBMR.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		return cnt;
	}
				
}
	