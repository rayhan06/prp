package employee_absent_approved_history;
import java.util.*; 
import util.*; 


public class Employee_absent_approved_historyDTO extends CommonDTO
{

	public long employeeRecordsId = -1;
	public long approvedByEmpRecordsId = -1;
	public long absentDate = -1;
	public long approvalDate = -1;
    public String comments = "";
	public long insertionDate = -1;
    public String insertedBy = "";
    public String modifiedBy = "";
	
	
    @Override
	public String toString() {
            return "$Employee_absent_approved_historyDTO[" +
            " iD = " + iD +
            " employeeRecordsId = " + employeeRecordsId +
            " approvedByEmpRecordsId = " + approvedByEmpRecordsId +
            " absentDate = " + absentDate +
            " approvalDate = " + approvalDate +
            " comments = " + comments +
            " insertionDate = " + insertionDate +
            " insertedBy = " + insertedBy +
            " modifiedBy = " + modifiedBy +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}