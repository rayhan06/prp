package employee_absent_approved_history;
import java.util.*; 
import util.*;


public class Employee_absent_approved_historyMAPS extends CommonMaps
{	
	public Employee_absent_approved_historyMAPS(String tableName)
	{
		

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("employeeRecordsId".toLowerCase(), "employeeRecordsId".toLowerCase());
		java_DTO_map.put("approvedByEmpRecordsId".toLowerCase(), "approvedByEmpRecordsId".toLowerCase());
		java_DTO_map.put("absentDate".toLowerCase(), "absentDate".toLowerCase());
		java_DTO_map.put("approvalDate".toLowerCase(), "approvalDate".toLowerCase());
		java_DTO_map.put("comments".toLowerCase(), "comments".toLowerCase());
		java_DTO_map.put("insertionDate".toLowerCase(), "insertionDate".toLowerCase());
		java_DTO_map.put("insertedBy".toLowerCase(), "insertedBy".toLowerCase());
		java_DTO_map.put("modifiedBy".toLowerCase(), "modifiedBy".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("employee_records_id".toLowerCase(), "employeeRecordsId".toLowerCase());
		java_SQL_map.put("approved_by_emp_records_id".toLowerCase(), "approvedByEmpRecordsId".toLowerCase());
		java_SQL_map.put("absent_date".toLowerCase(), "absentDate".toLowerCase());
		java_SQL_map.put("approval_date".toLowerCase(), "approvalDate".toLowerCase());
		java_SQL_map.put("comments".toLowerCase(), "comments".toLowerCase());
		java_SQL_map.put("inserted_by".toLowerCase(), "insertedBy".toLowerCase());
		java_SQL_map.put("modified_by".toLowerCase(), "modifiedBy".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Employee Records Id".toLowerCase(), "employeeRecordsId".toLowerCase());
		java_Text_map.put("Approved By Emp Records Id".toLowerCase(), "approvedByEmpRecordsId".toLowerCase());
		java_Text_map.put("Absent Date".toLowerCase(), "absentDate".toLowerCase());
		java_Text_map.put("Approval Date".toLowerCase(), "approvalDate".toLowerCase());
		java_Text_map.put("Comments".toLowerCase(), "comments".toLowerCase());
		java_Text_map.put("Insertion Date".toLowerCase(), "insertionDate".toLowerCase());
		java_Text_map.put("Inserted By".toLowerCase(), "insertedBy".toLowerCase());
		java_Text_map.put("Modified By".toLowerCase(), "modifiedBy".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}