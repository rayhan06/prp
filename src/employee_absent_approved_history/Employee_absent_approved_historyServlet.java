package employee_absent_approved_history;

import java.io.IOException;
import java.io.*;
import java.text.SimpleDateFormat;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import org.apache.log4j.Logger;

import login.LoginDTO;
import permission.MenuConstants;
import role.PermissionRepository;


import sessionmanager.SessionConstants;

import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;
import util.CommonRequestHandler;
import util.RecordNavigationManager4;

import java.util.*;
import javax.servlet.http.*;
import java.util.UUID;





import geolocation.GeoLocationDAO2;
import java.util.StringTokenizer;

import com.google.gson.Gson;

import pb.*;
import pbReport.*;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;




/**
 * Servlet implementation class Employee_absent_approved_historyServlet
 */
@WebServlet("/Employee_absent_approved_historyServlet")
@MultipartConfig
public class Employee_absent_approved_historyServlet extends HttpServlet 
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Employee_absent_approved_historyServlet.class);

    String tableName = "employee_absent_approved_history";

	Employee_absent_approved_historyDAO employee_absent_approved_historyDAO;
	CommonRequestHandler commonRequestHandler;
    private final Gson gson = new Gson();
    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Employee_absent_approved_historyServlet() 
	{
        super();
    	try
    	{
			employee_absent_approved_historyDAO = new Employee_absent_approved_historyDAO(tableName);
			commonRequestHandler = new CommonRequestHandler(employee_absent_approved_historyDAO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }   

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		System.out.println("In doget request = " + request);
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		try
		{
			String actionType = request.getParameter("actionType");
			if(actionType.equals("getAddPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.EMPLOYEE_ABSENT_APPROVED_HISTORY_ADD))
				{
					commonRequestHandler.getAddPage(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getEditPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.EMPLOYEE_ABSENT_APPROVED_HISTORY_UPDATE))
				{
					getEmployee_absent_approved_history(request, response);					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}						
			}
			else if(actionType.equals("getURL"))
			{
				String URL = request.getParameter("URL");
				System.out.println("URL = " + URL);
				response.sendRedirect(URL);			
			}			
			else if(actionType.equals("search"))
			{
				System.out.println("search requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.EMPLOYEE_ABSENT_APPROVED_HISTORY_SEARCH))
				{
					if(isPermanentTable)
					{
						String filter = request.getParameter("filter");
						System.out.println("filter = " + filter);
						if(filter!=null)
						{
							filter = ""; //shouldn't be directly used, rather manipulate it.
							searchEmployee_absent_approved_history(request, response, isPermanentTable, filter);
						}
						else
						{
							searchEmployee_absent_approved_history(request, response, isPermanentTable, "");
						}
					}					
					else
					{
						//searchEmployee_absent_approved_history(request, response, tempTableName, isPermanentTable);
					}
				}			
			}
			else if(actionType.equals("view"))
			{
				System.out.println("view requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.EMPLOYEE_ABSENT_APPROVED_HISTORY_SEARCH))
				{
					commonRequestHandler.view(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else
			{
				request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}


	
	

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		// TODO Auto-generated method stub
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		System.out.println("doPost");
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		
		try
		{
			String actionType = request.getParameter("actionType");
			System.out.println("actionType = " + actionType);
			if(actionType.equals("add"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.EMPLOYEE_ABSENT_APPROVED_HISTORY_ADD))
				{
					System.out.println("going to  addEmployee_absent_approved_history ");
					addEmployee_absent_approved_history(request, response, true, userDTO, true);
				}
				else
				{
					System.out.println("Not going to  addEmployee_absent_approved_history ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}

			else if(actionType.equals("getDTO"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.EMPLOYEE_ABSENT_APPROVED_HISTORY_ADD))
				{
					getDTO(request, response);					
				}
				else
				{
					System.out.println("Not going to  addEmployee_absent_approved_history ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else if(actionType.equals("edit"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.EMPLOYEE_ABSENT_APPROVED_HISTORY_UPDATE))
				{					
					addEmployee_absent_approved_history(request, response, false, userDTO, isPermanentTable);					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("delete"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.EMPLOYEE_ABSENT_APPROVED_HISTORY_UPDATE))
				{
					commonRequestHandler.delete(request, response, userDTO);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}	
			else if(actionType.equals("search"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.EMPLOYEE_ABSENT_APPROVED_HISTORY_SEARCH))
				{
					searchEmployee_absent_approved_history(request, response, true, "");
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getGeo"))
			{
				System.out.println("going to geoloc ");
				request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
			}
			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}
	
	private void getDTO(HttpServletRequest request, HttpServletResponse response) 
	{
		try 
		{
			System.out.println("In getDTO");
			Employee_absent_approved_historyDTO employee_absent_approved_historyDTO = employee_absent_approved_historyDAO.getDTOByID(Long.parseLong(request.getParameter("ID")));
			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			
			String encoded = this.gson.toJson(employee_absent_approved_historyDTO);
			System.out.println("json encoded data = " + encoded);
			out.print(encoded);
			out.flush();
		}
		catch (NumberFormatException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	private void addEmployee_absent_approved_history(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, boolean isPermanentTable) throws IOException 
	{
		// TODO Auto-generated method stub
		try 
		{
			request.setAttribute("failureMessage", "");
			System.out.println("%%%% addEmployee_absent_approved_history");
			String path = getServletContext().getRealPath("/img2/");
			Employee_absent_approved_historyDTO employee_absent_approved_historyDTO;
			SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
						
			if(addFlag == true)
			{
				employee_absent_approved_historyDTO = new Employee_absent_approved_historyDTO();
			}
			else
			{
				employee_absent_approved_historyDTO = employee_absent_approved_historyDAO.getDTOByID(Long.parseLong(request.getParameter("iD")));
			}
			
			String Value = "";

			Value = request.getParameter("employeeRecordsId");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("employeeRecordsId = " + Value);
			if(Value != null)
			{
				
				employee_absent_approved_historyDTO.employeeRecordsId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("approvedByEmpRecordsId");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("approvedByEmpRecordsId = " + Value);
			if(Value != null)
			{
				
				employee_absent_approved_historyDTO.approvedByEmpRecordsId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("absentDate");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("absentDate = " + Value);
			if(Value != null)
			{
				
				try 
				{
					Date d = f.parse(Value);
					employee_absent_approved_historyDTO.absentDate = d.getTime();
				}
				catch (Exception e) 
				{
					e.printStackTrace();
				}
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("approvalDate");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("approvalDate = " + Value);
			if(Value != null)
			{
				
				try 
				{
					Date d = f.parse(Value);
					employee_absent_approved_historyDTO.approvalDate = d.getTime();
				}
				catch (Exception e) 
				{
					e.printStackTrace();
				}
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("comments");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("comments = " + Value);
			if(Value != null)
			{
				
				employee_absent_approved_historyDTO.comments = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			if(addFlag)
			{
				Calendar c = Calendar.getInstance();
				c.set(Calendar.HOUR_OF_DAY, 0);
				c.set(Calendar.MINUTE, 0);
				c.set(Calendar.SECOND, 0);
				c.set(Calendar.MILLISECOND, 0);
				
				employee_absent_approved_historyDTO.insertionDate = c.getTimeInMillis();
			}			


			Value = request.getParameter("insertedBy");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("insertedBy = " + Value);
			if(Value != null)
			{
				
				employee_absent_approved_historyDTO.insertedBy = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("modifiedBy");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("modifiedBy = " + Value);
			if(Value != null)
			{
				
				employee_absent_approved_historyDTO.modifiedBy = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			
			System.out.println("Done adding  addEmployee_absent_approved_history dto = " + employee_absent_approved_historyDTO);
			long returnedID = -1;
			
			if(isPermanentTable == false) //add new row for validation and make the old row outdated
			{
				employee_absent_approved_historyDAO.setIsDeleted(employee_absent_approved_historyDTO.iD, CommonDTO.OUTDATED);
				returnedID = employee_absent_approved_historyDAO.add(employee_absent_approved_historyDTO);
				employee_absent_approved_historyDAO.setIsDeleted(returnedID, CommonDTO.WAITING_FOR_APPROVAL);
			}
			else if(addFlag == true)
			{
				returnedID = employee_absent_approved_historyDAO.manageWriteOperations(employee_absent_approved_historyDTO, SessionConstants.INSERT, -1, userDTO);
			}
			else
			{				
				returnedID = employee_absent_approved_historyDAO.manageWriteOperations(employee_absent_approved_historyDTO, SessionConstants.UPDATE, -1, userDTO);											
			}
			
			
			
			
			
			
			
			
			
			if(isPermanentTable)
			{
				String inPlaceSubmit = request.getParameter("inplacesubmit");
				
				if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
				{
					getEmployee_absent_approved_history(request, response, returnedID);
				}
				else
				{
					response.sendRedirect("Employee_absent_approved_historyServlet?actionType=search");
				}
			}
			else
			{
				commonRequestHandler.validate(employee_absent_approved_historyDAO.getDTOByID(returnedID), request, response, userDTO);
			}
					
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	



	
	
	

	private void getEmployee_absent_approved_history(HttpServletRequest request, HttpServletResponse response, long id) throws ServletException, IOException
	{
		System.out.println("in getEmployee_absent_approved_history");
		Employee_absent_approved_historyDTO employee_absent_approved_historyDTO = null;
		try 
		{
			employee_absent_approved_historyDTO = employee_absent_approved_historyDAO.getDTOByID(id);
			request.setAttribute("ID", employee_absent_approved_historyDTO.iD);
			request.setAttribute("employee_absent_approved_historyDTO",employee_absent_approved_historyDTO);
			request.setAttribute("employee_absent_approved_historyDAO",employee_absent_approved_historyDAO);
			
			String URL= "";
			
			String inPlaceEdit = request.getParameter("inplaceedit");
			String inPlaceSubmit = request.getParameter("inplacesubmit");
			String getBodyOnly = request.getParameter("getBodyOnly");
			
			if(inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase(""))
			{
				URL = "employee_absent_approved_history/employee_absent_approved_historyInPlaceEdit.jsp";	
				request.setAttribute("inplaceedit","");				
			}
			else if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
			{
				URL = "employee_absent_approved_history/employee_absent_approved_historySearchRow.jsp";
				request.setAttribute("inplacesubmit","");					
			}
			else
			{
				if(getBodyOnly  != null && !getBodyOnly.equalsIgnoreCase(""))
				{
					URL = "employee_absent_approved_history/employee_absent_approved_historyEditBody.jsp?actionType=edit";
				}
				else
				{
					URL = "employee_absent_approved_history/employee_absent_approved_historyEdit.jsp?actionType=edit";
				}				
			}
			
			RequestDispatcher rd = request.getRequestDispatcher(URL);
			rd.forward(request, response);
		}
		catch (NumberFormatException e) 
		{
			e.printStackTrace();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	
	private void getEmployee_absent_approved_history(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		getEmployee_absent_approved_history(request, response, Long.parseLong(request.getParameter("ID")));	
	}
	
	private void searchEmployee_absent_approved_history(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException
	{
		System.out.println("in  searchEmployee_absent_approved_history 1");
		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String ajax = request.getParameter("ajax");
		boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);
		
        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
			SessionConstants.NAV_EMPLOYEE_ABSENT_APPROVED_HISTORY,
			request,
			employee_absent_approved_historyDAO,
			SessionConstants.VIEW_EMPLOYEE_ABSENT_APPROVED_HISTORY,
			SessionConstants.SEARCH_EMPLOYEE_ABSENT_APPROVED_HISTORY,
			tableName,
			isPermanent,
			userDTO,
			filter,
			true);
        try
        {
			System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        }
        catch(Exception e)
        {
			System.out.println("failed to dojob" + e);
        }

		request.setAttribute("employee_absent_approved_historyDAO",employee_absent_approved_historyDAO);
        RequestDispatcher rd;
        if(!isPermanent)
        {
        	if(hasAjax == false)
	        {
	        	System.out.println("Going to employee_absent_approved_history/employee_absent_approved_historyApproval.jsp");
	        	rd = request.getRequestDispatcher("employee_absent_approved_history/employee_absent_approved_historyApproval.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to employee_absent_approved_history/employee_absent_approved_historyApprovalForm.jsp");
	        	rd = request.getRequestDispatcher("employee_absent_approved_history/employee_absent_approved_historyApprovalForm.jsp");
	        }
        }
        else
        {
	        if(hasAjax == false)
	        {
	        	System.out.println("Going to employee_absent_approved_history/employee_absent_approved_historySearch.jsp");
	        	rd = request.getRequestDispatcher("employee_absent_approved_history/employee_absent_approved_historySearch.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to employee_absent_approved_history/employee_absent_approved_historySearchForm.jsp");
	        	rd = request.getRequestDispatcher("employee_absent_approved_history/employee_absent_approved_historySearchForm.jsp");
	        }
        }
		rd.forward(request, response);
	}
	
}

