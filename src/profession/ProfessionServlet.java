package profession;

import common.BaseServlet;
import common.NameDao;
import common.NameRepository;
import common.NameInterface;
import language.LC;
import permission.MenuConstants;
import sessionmanager.SessionConstants;
import user.UserDTO;
import util.CommonDTO;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;


@WebServlet("/ProfessionServlet")
@MultipartConfig
public class ProfessionServlet extends BaseServlet implements NameInterface {

	public String commonPartOfDispatchURL(){
		return  "common/name";
	}

	@Override
	public String getTableName() {
		return ProfessionDAO.getInstance().getTableName();
	}

	@Override
	public String getServletName() {
		return "ProfessionServlet";
	}

	@Override
	public NameDao getCommonDAOService() {
		return ProfessionDAO.getInstance();
	}

	@Override
	public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
		return addT(request,addFlag,userDTO,ProfessionDAO.getInstance());
	}

	@Override
	public int[] getAddPageMenuConstants() {
		return new int[] {MenuConstants.PROFESSION_ADD};
	}

	@Override
	public int[] getEditPageMenuConstants() {
		return new int[] {MenuConstants.PROFESSION_UPDATE};
	}

	@Override
	public int[] getSearchMenuConstants() {
		return new int[] {MenuConstants.PROFESSION_SEARCH};
	}

	@Override
	public Class<? extends HttpServlet> getClazz() {
		return ProfessionServlet.class;
	}

	@Override
	public int getSearchTitleValue() {
		return LC.PROFESSION_SEARCH_PROFESSION_SEARCH_FORMNAME;
	}

	@Override
	public String get_p_navigatorName() {
		return SessionConstants.NAV_PROFESSION;
	}

	@Override
	public String get_p_dtoCollectionName() {
		return SessionConstants.VIEW_PROFESSION;
	}

	@Override
	public NameRepository getNameRepository() {
		return ProfessionRepository.getInstance();
	}

	@Override
	public int getAddTitleValue() {
		return LC.PROFESSION_ADD_PROFESSION_ADD_FORMNAME;
	}

	@Override
	public int getEditTitleValue() {
		return LC.PROFESSION_ADD_PROFESSION_ADD_FORMNAME;
	}

	@Override
	public void init(HttpServletRequest request) throws ServletException {
		setCommonAttributes(request,getServletName(),getCommonDAOService());
	}
}