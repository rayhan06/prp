package profession;

import common.NameDao;

public class ProfessionDAO extends NameDao {

    private ProfessionDAO() {
        super("profession");
    }

    private static class LazyLoader{
        static final ProfessionDAO INSTANCE = new ProfessionDAO();
    }

    public static ProfessionDAO getInstance(){
        return LazyLoader.INSTANCE;
    }
}
	