package profession;

import common.NameRepository;


public class ProfessionRepository extends NameRepository {

	private ProfessionRepository(){
		super(ProfessionDAO.getInstance(), ProfessionRepository.class);
	}

	private static class LazyLoader{
		static ProfessionRepository INSTANCE = new ProfessionRepository();
	}

	public static ProfessionRepository getInstance(){
		return LazyLoader.INSTANCE;
	}
}


