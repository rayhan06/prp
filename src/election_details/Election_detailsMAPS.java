package election_details;

import util.CommonMaps;


public class Election_detailsMAPS extends CommonMaps {
    public Election_detailsMAPS() {
        java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
        java_DTO_map.put("parliamentNumber".toLowerCase(), "parliamentNumber".toLowerCase());
        java_DTO_map.put("electionDate".toLowerCase(), "electionDate".toLowerCase());
        java_DTO_map.put("generralOathDate".toLowerCase(), "generralOathDate".toLowerCase());
        java_DTO_map.put("gazetteDate".toLowerCase(), "gazetteDate".toLowerCase());
        java_DTO_map.put("parliamentLastDate".toLowerCase(), "parliamentLastDate".toLowerCase());
        java_DTO_map.put("filesDropzone".toLowerCase(), "filesDropzone".toLowerCase());
        java_DTO_map.put("insertionDate".toLowerCase(), "insertionDate".toLowerCase());
        java_DTO_map.put("insertedBy".toLowerCase(), "insertedBy".toLowerCase());
        java_DTO_map.put("modifiedBy".toLowerCase(), "modifiedBy".toLowerCase());
        java_DTO_map.put("searchColumn".toLowerCase(), "searchColumn".toLowerCase());
        java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
        java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

        java_SQL_map.put("parliament_number".toLowerCase(), "parliamentNumber".toLowerCase());
        java_SQL_map.put("election_date".toLowerCase(), "electionDate".toLowerCase());
        java_SQL_map.put("generral_oath_date".toLowerCase(), "generralOathDate".toLowerCase());
        java_SQL_map.put("gazette_date".toLowerCase(), "gazetteDate".toLowerCase());
        java_SQL_map.put("parliament_last_date".toLowerCase(), "parliamentLastDate".toLowerCase());
        java_SQL_map.put("files_dropzone".toLowerCase(), "filesDropzone".toLowerCase());

        java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
        java_Text_map.put("Parliament Number".toLowerCase(), "parliamentNumber".toLowerCase());
        java_Text_map.put("Election Date".toLowerCase(), "electionDate".toLowerCase());
        java_Text_map.put("Generral Oath Date".toLowerCase(), "generralOathDate".toLowerCase());
        java_Text_map.put("Gazette Date".toLowerCase(), "gazetteDate".toLowerCase());
        java_Text_map.put("Parliament Last Date".toLowerCase(), "parliamentLastDate".toLowerCase());
        java_Text_map.put("Files Dropzone".toLowerCase(), "filesDropzone".toLowerCase());
        java_Text_map.put("Insertion Date".toLowerCase(), "insertionDate".toLowerCase());
        java_Text_map.put("Inserted By".toLowerCase(), "insertedBy".toLowerCase());
        java_Text_map.put("Modified By".toLowerCase(), "modifiedBy".toLowerCase());
        java_Text_map.put("Search Column".toLowerCase(), "searchColumn".toLowerCase());
        java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
        java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

    }

}