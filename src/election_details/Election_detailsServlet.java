package election_details;

import common.ApiResponse;
import common.BaseServlet;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import permission.MenuConstants;
import user.UserDTO;
import util.CommonDTO;
import util.HttpRequestUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

@SuppressWarnings("Duplicates")
@WebServlet("/Election_detailsServlet")
@MultipartConfig
public class Election_detailsServlet extends BaseServlet {
    private static final long serialVersionUID = 1L;

    private static final Logger logger = Logger.getLogger(Election_detailsServlet.class);

    private final Election_detailsDAO electionDetailsDAO = Election_detailsDAO.getInstance();
    @Override
    public String getTableName() {
        return Election_detailsDAO.getInstance().getTableName();
    }

    @Override
    public String getSortClause(HttpServletRequest request) {
        return "parliament_number DESC";
    }

    @Override
    public String getServletName() {
        return "Election_detailsServlet";
    }

    @Override
    public Election_detailsDAO getCommonDAOService() {
        return electionDetailsDAO;
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        boolean isLangEng = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
        switch (request.getParameter("actionType")){
            case "active":
                try{
                    long id  = Long.parseLong(request.getParameter("id"));
                    Election_detailsDTO electionDetailsDTO = Election_detailsRepository.getInstance().getElectionDetailsDTOByID(id);
                    if(electionDetailsDTO == null){
                        throw new Exception(isLangEng?"Election information not found":"নির্বাচনের তথ্য পাওয়া যায়নি");
                    }
                    if(electionDetailsDTO.parliamentStatusCat != ElectionDetailsStatusEnum.UPCOMING.getValue()){
                        throw new Exception(isLangEng?"Election status should be in upcoming state":"নির্বাচনের অব্যশই আসন্ন অবস্থায় থাকতে হবে");
                    }
                    electionDetailsDAO.active(id);
                    ApiResponse.sendSuccessResponse(response,"");
                }catch (Exception ex){
                    ex.printStackTrace();
                    ApiResponse.sendErrorResponse(response,ex.getMessage());
                }
                return;
            case "closed":
                try{
                    long id  = Long.parseLong(request.getParameter("id"));
                    Election_detailsDTO electionDetailsDTO = Election_detailsRepository.getInstance().getElectionDetailsDTOByID(id);
                    if(electionDetailsDTO == null){
                        throw new Exception(isLangEng?"Election information not found":"নির্বাচনের তথ্য পাওয়া যায়নি");
                    }
                    if(electionDetailsDTO.parliamentStatusCat != ElectionDetailsStatusEnum.RUNNING.getValue()){
                        throw new Exception(isLangEng?"Election status should be in running state":"নির্বাচনের অব্যশই চলমান অবস্থায় থাকতে হবে");
                    }
                    electionDetailsDAO.dismiss(id,System.currentTimeMillis(),HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO.employee_record_id);
                    ApiResponse.sendSuccessResponse(response,"");
                }catch (Exception ex){
                    ex.printStackTrace();
                    ApiResponse.sendErrorResponse(response,ex.getMessage());
                }
                return;
            default:
                super.doPost(request, response);
        }
    }

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        Election_detailsDTO election_detailsDTO;
        boolean isLangEng = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
        SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
        if (addFlag) {
            election_detailsDTO = Election_detailsRepository.getInstance().getUpcomingElectionDetailsDTO();
            if(election_detailsDTO!=null){
                throw new Exception(isLangEng?"Already upcoming election details has been created":"ইতিমধ্রে আসন্ন নির্বাচন বিস্তারিত প্রস্তুত করা আছে");
            }
            election_detailsDTO = new Election_detailsDTO();
            election_detailsDTO.insertedBy = String.valueOf(userDTO.employee_record_id);
            election_detailsDTO.insertionDate = new Date().getTime();
        } else {
            election_detailsDTO = Election_detailsRepository.getInstance().getElectionDetailsDTOByID(Long.parseLong(request.getParameter("iD")));
        }
        election_detailsDTO.modifiedBy = String.valueOf(userDTO.ID);
        election_detailsDTO.lastModificationTime = new Date().getTime();

        election_detailsDTO.parliamentNumber = Integer.parseInt(request.getParameter("parliamentNumber"));

        Date d = f.parse(Jsoup.clean(request.getParameter("electionDate"), Whitelist.simpleText()));
        election_detailsDTO.electionDate = d.getTime();

        d = f.parse(Jsoup.clean(request.getParameter("generralOathDate"), Whitelist.simpleText()));
        election_detailsDTO.generralOathDate = d.getTime();

        d = f.parse(Jsoup.clean(request.getParameter("gazetteDate"), Whitelist.simpleText()));
        election_detailsDTO.gazetteDate = d.getTime();

        d = f.parse(Jsoup.clean(request.getParameter("parliamentLastDate"), Whitelist.simpleText()));
        election_detailsDTO.parliamentLastDate = d.getTime();

        election_detailsDTO.filesDropzone = Long.parseLong(request.getParameter("filesDropzone"));

        if (addFlag) {
            Election_detailsDAO.getInstance().add(election_detailsDTO);
        }else {
            String filesDropzoneFilesToDelete = request.getParameter("filesDropzoneFilesToDelete");
            String[] deleteArray = filesDropzoneFilesToDelete.split(",");
            for (int i = 0; i < deleteArray.length; i++) {
                logger.debug("going to delete " + deleteArray[i]);
                if (i > 0) {
                    filesDAO.delete(Long.parseLong(deleteArray[i]));
                }
            }
            Election_detailsDAO.getInstance().update(election_detailsDTO);
        }
        return election_detailsDTO;
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[] {MenuConstants.ELECTION_DETAILS_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[] {MenuConstants.ELECTION_DETAILS_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[] {MenuConstants.ELECTION_DETAILS_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Election_detailsServlet.class;
    }
}