package election_details;

/*
 * @author Md. Erfan Hossain
 * @created 05/08/2021 - 8:01 PM
 * @project parliament
 */

import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public enum ElectionDetailsStatusEnum {
    UPCOMING(0,"UPCOMING","আসন্ন","gray"),
    RUNNING(1,"RUNNING","চলমান","forestgreen"),
    CLOSED(2,"CLOSED","শেষ","crimson");

    private final int value;
    private final String engText;
    private final String bngText;
    private final String color;

    ElectionDetailsStatusEnum(int value, String engText, String bngText,String color) {
        this.value = value;
        this.engText = engText;
        this.bngText = bngText;
        this.color = color;
    }

    public int getValue() {
        return value;
    }

    public String getEngText() {
        return engText;
    }

    public String getBngText() {
        return bngText;
    }

    public String getColor() {
        return color;
    }

    private static Map<Integer,ElectionDetailsStatusEnum> mapByValue = null;

    private static void reload(){
        mapByValue = Stream.of(ElectionDetailsStatusEnum.values())
                .collect(Collectors.toMap(e->e.value,e->e));

    }

    public static ElectionDetailsStatusEnum getByValue(int value){
        if(mapByValue == null){
            synchronized (ElectionDetailsStatusEnum.class){
                if(mapByValue == null){
                    reload();
                }
            }
        }
        return mapByValue.get(value);
    }
}
