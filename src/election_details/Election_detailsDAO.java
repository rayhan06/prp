package election_details;

import common.CommonDAOService;
import common.ConnectionAndStatementUtil;
import election_wise_mp.Election_wise_mpDAO;
import org.apache.log4j.Logger;
import util.CommonDTO;
import util.HttpRequestUtils;
import util.StringUtils;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

@SuppressWarnings({"unused"})
public class Election_detailsDAO implements CommonDAOService<Election_detailsDTO> {

    private static final Logger logger = Logger.getLogger(Election_detailsDAO.class);

    private static final String addSqlQuery = "INSERT INTO {tableName} (parliament_number,election_date,generral_oath_date,gazette_date,parliament_last_date," +
            " files_dropzone,insertion_date,inserted_by,modified_by,search_column,lastModificationTime,parliament_status_cat,isDeleted,ID) " +
            " VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
    private static final String updateSqlQuery = "UPDATE {tableName} SET parliament_number=?,election_date=?,generral_oath_date=?,gazette_date=?,parliament_last_date=?," +
            " files_dropzone=?,insertion_date=?,inserted_by=?,modified_by=?,search_column=?,lastModificationTime = ?,parliament_status_cat=? WHERE ID = ?";

    private static final String changeStatusSQL = "UPDATE election_details SET parliament_status_cat = ?,lastModificationTime=?,modified_by=? WHERE ID = ?";

    private final Map<String, String> searchMap = new HashMap<>();

    private Election_detailsDAO() {
        searchMap.put("parliament_number", " and (ID = ?)");
        searchMap.put("election_date_start", " and (election_date >= ?)");
        searchMap.put("election_date_end", " and (election_date <= ?)");
        searchMap.put("AnyField"," and (search_column like ?)");
    }

    private static class LazyLoader {
        static final Election_detailsDAO INSTANCE = new Election_detailsDAO();
    }

    public static Election_detailsDAO getInstance() {
        return LazyLoader.INSTANCE;
    }

    public void setSearchColumn(Election_detailsDTO dto) {
        StringBuilder column = new StringBuilder();
        column.append(dto.parliamentNumber + " ");
        column.append(StringUtils.convertToBanNumber(String.valueOf(dto.parliamentNumber)) + " ");
        dto.searchColumn = column.toString();
    }

    @Override
    public void set(PreparedStatement ps, Election_detailsDTO election_detailsDTO, boolean isInsert) throws SQLException {
        int index = 0;
        setSearchColumn(election_detailsDTO);
        ps.setObject(++index, election_detailsDTO.parliamentNumber);
        ps.setObject(++index, election_detailsDTO.electionDate);
        ps.setObject(++index, election_detailsDTO.generralOathDate);
        ps.setObject(++index, election_detailsDTO.gazetteDate);
        ps.setObject(++index, election_detailsDTO.parliamentLastDate);
        ps.setObject(++index, election_detailsDTO.filesDropzone);
        ps.setObject(++index, election_detailsDTO.insertionDate);
        ps.setObject(++index, election_detailsDTO.insertedBy);
        ps.setObject(++index, election_detailsDTO.modifiedBy);
        ps.setObject(++index, election_detailsDTO.searchColumn);
        ps.setObject(++index, System.currentTimeMillis());
        ps.setObject(++index, election_detailsDTO.parliamentStatusCat);
        if (isInsert) {
            ps.setObject(++index, 0);
        }
        ps.setObject(++index, election_detailsDTO.iD);
    }

    @Override
    public Election_detailsDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Election_detailsDTO election_detailsDTO = new Election_detailsDTO();
            election_detailsDTO.iD = rs.getLong("ID");
            election_detailsDTO.parliamentNumber = rs.getInt("parliament_number");
            election_detailsDTO.electionDate = rs.getLong("election_date");
            election_detailsDTO.generralOathDate = rs.getLong("generral_oath_date");
            election_detailsDTO.gazetteDate = rs.getLong("gazette_date");
            election_detailsDTO.parliamentLastDate = rs.getLong("parliament_last_date");
            election_detailsDTO.filesDropzone = rs.getLong("files_dropzone");
            election_detailsDTO.insertionDate = rs.getLong("insertion_date");
            election_detailsDTO.insertedBy = rs.getString("inserted_by");
            election_detailsDTO.modifiedBy = rs.getString("modified_by");
            election_detailsDTO.searchColumn = rs.getString("search_column");
            election_detailsDTO.isDeleted = rs.getInt("isDeleted");
            election_detailsDTO.lastModificationTime = rs.getLong("lastModificationTime");
            election_detailsDTO.parliamentStatusCat = rs.getInt("parliament_status_cat");
            return election_detailsDTO;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    @Override
    public String getTableName() {
        return "election_details";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Election_detailsDTO) commonDTO, addSqlQuery, true);
    }

    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Election_detailsDTO) commonDTO, updateSqlQuery, false);
    }

    public int getParliamentNumberById(long id) {
        return getDTOFromID(id).parliamentNumber;
    }

    public void active(long id) {
        changeStatus(id, 1);
    }

    public void dismiss(long id, long dismissTime, long modifiedBy) {
        changeStatus(id, 2);
        Election_wise_mpDAO.getInstance().setElectionBreakDown(id, dismissTime, modifiedBy);
    }

    private void changeStatus(long id, int status) {
        ConnectionAndStatementUtil.getWriteConnectionAndPrepareStatement(model -> {
            try {
                long currentTimeMillis = System.currentTimeMillis();
                PreparedStatement ps = (PreparedStatement) model.getStatement();
                ps.setInt(1, status);
                ps.setLong(2, currentTimeMillis);
                ps.setString(3, String.valueOf(HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO.ID));
                ps.setLong(4, id);
                logger.debug(ps);
                ps.executeUpdate();
                recordUpdateTime(model.getConnection(), getTableName(), currentTimeMillis);
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }, changeStatusSQL);
    }
}