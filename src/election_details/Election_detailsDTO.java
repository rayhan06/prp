package election_details;

import pb.OptionDTO;
import sessionmanager.SessionConstants;
import util.CommonDTO;
import util.StringUtils;


public class Election_detailsDTO extends CommonDTO {

    public int parliamentNumber = 0;
    public long electionDate = SessionConstants.MIN_DATE;
    public long generralOathDate = SessionConstants.MIN_DATE;
    public long gazetteDate = SessionConstants.MIN_DATE;
    public long parliamentLastDate = SessionConstants.MIN_DATE;
    public long filesDropzone = 0;
    public long insertionDate = 0;
    public String insertedBy = "";
    public String modifiedBy = "";
    public int parliamentStatusCat = 0;

    public OptionDTO getOptionDTO() {
        String parliamentNumberStr = String.format("%d", parliamentNumber);
        return new OptionDTO(
                parliamentNumberStr,
                StringUtils.convertToBanNumber(parliamentNumberStr),
                String.format("%d", iD)
        );
    }

    public String getParliamentText(boolean isLangEng) {
        return isLangEng ? String.format("%d-th Parliament", parliamentNumber)
                         : String.format("%d-তম সংসদ", parliamentNumber);
    }

    @Override
    public String toString() {
        return "$Election_detailsDTO[" +
               " iD = " + iD +
               " parliamentNumber = " + parliamentNumber +
               " electionDate = " + electionDate +
               " generralOathDate = " + generralOathDate +
               " gazetteDate = " + gazetteDate +
               " parliamentLastDate = " + parliamentLastDate +
               " filesDropzone = " + filesDropzone +
               " insertionDate = " + insertionDate +
               " insertedBy = " + insertedBy +
               " modifiedBy = " + modifiedBy +
               " searchColumn = " + searchColumn +
               " isDeleted = " + isDeleted +
               " lastModificationTime = " + lastModificationTime +
               "]";
    }
}