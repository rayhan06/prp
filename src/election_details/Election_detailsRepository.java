package election_details;

import election_constituency.Election_constituencyDTO;
import org.apache.log4j.Logger;
import pb.OptionDTO;
import pb.Utils;
import repository.Repository;
import repository.RepositoryManager;
import util.LockManager;
import util.StringUtils;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Predicate;
import java.util.stream.Collectors;


public class Election_detailsRepository implements Repository {

    private static final Logger logger = Logger.getLogger(Election_detailsRepository.class);
    private final Map<Long, Election_detailsDTO> mapOfElection_detailsDTOToiD;
    private final Map<Integer, Election_detailsDTO> mapOfElection_detailsDTOToParliamentNumber;
    private List<Election_detailsDTO> election_detailsDTOList;

    private Election_detailsRepository() {
        mapOfElection_detailsDTOToiD = new ConcurrentHashMap<>();
        mapOfElection_detailsDTOToParliamentNumber = new ConcurrentHashMap<>();
        RepositoryManager.getInstance().addRepository(this);
    }

    private static class LazyLoader {
        static Election_detailsRepository INSTANCE = new Election_detailsRepository();
    }

    public static Election_detailsRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

    public void reload(boolean reloadAll) {
        logger.debug("Election_detailsRepository reload start reloadAll : " + reloadAll);
        List<Election_detailsDTO> dtoList = Election_detailsDAO.getInstance().getAllDTOs(reloadAll);
        if (dtoList != null && !dtoList.isEmpty()) {
            dtoList.stream()
                   .peek(this::removeIfPresent)
                   .filter(dto -> dto.isDeleted == 0)
                   .forEach(dto -> {
                       mapOfElection_detailsDTOToiD.put(dto.iD, dto);
                       mapOfElection_detailsDTOToParliamentNumber.put(dto.parliamentNumber, dto);
                   });
        }
        election_detailsDTOList = new ArrayList<>(mapOfElection_detailsDTOToiD.values());
        election_detailsDTOList.sort((o1, o2) -> Integer.compare(o2.parliamentNumber, o1.parliamentNumber));
        logger.debug("Election_detailsRepository reload end reloadAll : " + reloadAll);
    }

    private void removeIfPresent(Election_detailsDTO dto) {
        if (mapOfElection_detailsDTOToiD.get(dto.iD) != null) {
            mapOfElection_detailsDTOToiD.remove(dto.iD);
        }
        if (mapOfElection_detailsDTOToParliamentNumber.get(dto.parliamentNumber) != null) {
            mapOfElection_detailsDTOToParliamentNumber.remove(dto.parliamentNumber);
        }
    }

    public List<Election_detailsDTO> getElectionDetailsList() {
        return election_detailsDTOList;
    }

    public Election_detailsDTO getElectionDetailsDTOByID(long ID) {
        if (mapOfElection_detailsDTOToiD.get(ID) == null) {
            synchronized (LockManager.getLock(ID + "EDR")) {
                Election_detailsDTO dto = Election_detailsDAO.getInstance().getDTOFromID(ID);
                if (dto != null) {
                    mapOfElection_detailsDTOToiD.put(dto.iD, dto);
                    mapOfElection_detailsDTOToParliamentNumber.put(dto.parliamentNumber, dto);
                    election_detailsDTOList = new ArrayList<>(mapOfElection_detailsDTOToiD.values());
                    election_detailsDTOList.sort((o1, o2) -> Integer.compare(o2.parliamentNumber, o1.parliamentNumber));
                }
            }
        }
        return mapOfElection_detailsDTOToiD.get(ID);
    }

    public Election_detailsDTO getElection_detailsDTOByParliamentNumber(int parliamentNumber) {
        return mapOfElection_detailsDTOToParliamentNumber.get(parliamentNumber);
    }

    @Override
    public String getTableName() {
        return "election_details";
    }

    public Map<Long, Election_detailsDTO> getByIds(List<Long> ids) {
        return ids.stream()
                  .filter(id -> mapOfElection_detailsDTOToiD.get(id) != null)
                  .map(mapOfElection_detailsDTOToiD::get)
                  .collect(Collectors.toMap(e -> e.iD, e -> e));
    }

    public String buildOptions(String language, Long selectedId) {
        List<OptionDTO> optionDTOList = null;
        if (election_detailsDTOList != null && election_detailsDTOList.size() > 0) {
            optionDTOList = election_detailsDTOList.stream()
                                                   .map(Election_detailsDTO::getOptionDTO)
                                                   .collect(Collectors.toList());
        }
        return Utils.buildOptions(optionDTOList, language, selectedId == null ? null : String.valueOf(selectedId));
    }

    public String getText(long id, String language) {
        return getText(id, "English".equalsIgnoreCase(language));
    }

    public String getText(long id, boolean isLangEng) {
        Election_detailsDTO dto = mapOfElection_detailsDTOToiD.get(id);
        return dto == null ? "" : (isLangEng ? String.valueOf(dto.parliamentNumber)
                                             : StringUtils.convertToBanNumber(String.valueOf(dto.parliamentNumber)));
    }

    public Election_detailsDTO getRunningElectionDetailsDTO() {
        return getByFilter(e -> e.parliamentStatusCat == ElectionDetailsStatusEnum.RUNNING.getValue());
    }

    public Election_detailsDTO getUpcomingElectionDetailsDTO() {
        return getByFilter(e -> e.parliamentStatusCat == ElectionDetailsStatusEnum.UPCOMING.getValue());
    }

    private Election_detailsDTO getByFilter(Predicate<Election_detailsDTO> filter) {
        return election_detailsDTOList.stream()
                                      .filter(filter)
                                      .findFirst()
                                      .orElse(null);
    }
}