package roomwise_asset_count_report;


import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import language.LC;
import language.LM;

import login.LoginDTO;
import permission.MenuConstants;

import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;

import util.ReportRequestHandler;
import pbReport.*;






@WebServlet("/Roomwise_asset_count_report_Servlet")
public class Roomwise_asset_count_report_Servlet  extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	String[][] Criteria =
	{
		{"criteria","","room_no_cat","!=","","int","","","-1","none",  ""},		
		{"criteria","","room_no_cat","=","AND","int","","","any","roomNoCat_1", LC.ROOMWISE_ASSET_COUNT_REPORT_WHERE_ROOMNOCAT_1 + ""}		
	};
	
	String[][] Display =
	{
		{"display","","room_no_cat","cat",""},		
		{"display","","SUM(CASE WHEN asset_category_type = 1404 THEN 1 ELSE 0 END)","int",""},		
		{"display","","SUM(CASE WHEN asset_category_type = 1401 THEN 1 ELSE 0 END)","int",""},		
		{"display","","SUM(CASE WHEN asset_category_type = 1400 THEN 1 ELSE 0 END)","int",""},		
		{"display","","SUM(CASE WHEN asset_category_type = 1403 THEN 1 ELSE 0 END)","int",""},		
		{"display","","SUM(CASE WHEN asset_category_type = 1405 THEN 1 ELSE 0 END)","int",""},		
		{"display","","SUM(CASE WHEN asset_category_type = 1402 THEN 1 ELSE 0 END)","int",""}		
	};
	
	String GroupBy = "room_no_cat";
	String OrderBY = "";
	
	ReportRequestHandler reportRequestHandler;
	
	public Roomwise_asset_count_report_Servlet(){

	}
	
	private final ReportService reportService = new ReportService();
	
	private String sql;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String language = "english";
		if(userDTO!= null && userDTO.languageID == SessionConstants.BANGLA)
		{
			language = "bangla";
		}
		boolean isLangEng = language.equalsIgnoreCase("english");
				
		String actionType = request.getParameter("actionType");

		System.out.println("In ssservlet doget, actiontype = " + actionType);
		
		sql = "asset_assignee";

		Display[0][4] = isLangEng ? "Room" : "কক্ষ";
        Display[1][4] = isLangEng ? "CPU Count" : "সি পি ইউর সংখ্যা";
		Display[2][4] = isLangEng ? "Laptop Count" : "ল্যাপটপের সংখ্যা";		
		Display[3][4] = isLangEng ? "Monitor Count" : "মনিটরের সংখ্যা";
		Display[4][4] = isLangEng ? "Printer Count" : "প্রিন্টারের সংখ্যা";
		Display[5][4] = isLangEng ? "Scanner Count" : "স্ক্যানারের সংখ্যা";
		Display[6][4] = isLangEng ? "UPS Count" : "ইউ পি এসের সংখ্যা";
		
		String reportName = LM.getText(LC.ROOMWISE_ASSET_COUNT_REPORT_OTHER_ROOMWISE_ASSET_COUNT_REPORT, loginDTO);
		
		reportRequestHandler = new ReportRequestHandler(null,
				Criteria, Display, GroupBy, OrderBY, sql,
				reportService);
		reportRequestHandler.clientSideDataFormatting = new ArrayList<Pair<Integer, Integer>>();
		reportRequestHandler.clientSideDataFormatting.add(new ImmutablePair<Integer, Integer>(1, ReportRequestHandler.RIGHT_ALIGN_INT));
		reportRequestHandler.clientSideDataFormatting.add(new ImmutablePair<Integer, Integer>(2, ReportRequestHandler.RIGHT_ALIGN_INT));
		reportRequestHandler.clientSideDataFormatting.add(new ImmutablePair<Integer, Integer>(3, ReportRequestHandler.RIGHT_ALIGN_INT));
		reportRequestHandler.clientSideDataFormatting.add(new ImmutablePair<Integer, Integer>(4, ReportRequestHandler.RIGHT_ALIGN_INT));
		reportRequestHandler.clientSideDataFormatting.add(new ImmutablePair<Integer, Integer>(5, ReportRequestHandler.RIGHT_ALIGN_INT));
		reportRequestHandler.clientSideDataFormatting.add(new ImmutablePair<Integer, Integer>(6, ReportRequestHandler.RIGHT_ALIGN_INT));

		
		reportRequestHandler.handleReportGet(request, response, userDTO, "roomwise_asset_count_report",
				MenuConstants.ROOMWISE_ASSET_COUNT_REPORT_DETAILS, language, reportName, "roomwise_asset_count_report");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		doGet(request, response);
	}
}
