package dbm;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.apache.log4j.Logger;
import util.LockManager;

import java.io.File;
import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

@SuppressWarnings("ALL")
public class DBMW {
    private String m_driverClassName;
    private String m_databaseURL;
    private String m_userName;
    private String m_Password;
    private static final DBMW dbmw;
    private final Map<String, SequenceDTO> nextIDMap = new ConcurrentHashMap<>();
    public static String configFileName = "DBMW.properties";
    private static final Logger logger = Logger.getLogger(DBMW.class);

    private static HikariDataSource hikariDataSource;

    static {
        dbmw = new DBMW();
    }

    private DBMW() {
        this.readConfigFile();
        this.createDatabaseManager();
    }

    public static DBMW getInstance() throws SQLException, ClassNotFoundException, IllegalAccessException, InstantiationException {
        return dbmw;
    }

    private void createDatabaseManager() {
        HikariConfig hikariConfig = new HikariConfig();
        try {
            hikariConfig.setPoolName("writePool");
            hikariConfig.setDriverClassName(this.m_driverClassName);
            hikariConfig.setJdbcUrl(this.m_databaseURL);
            hikariConfig.setUsername(this.m_userName);
            hikariConfig.setPassword(this.m_Password);
            hikariConfig.setMaximumPoolSize(20);
            hikariConfig.setConnectionTimeout(60000);
            hikariConfig.setLeakDetectionThreshold(300000);
            hikariDataSource = new HikariDataSource(hikariConfig);
        } catch (Exception ex) {
            logger.error("Can't instantiate Hikari data-source");
        }
    }

    public synchronized Connection getConnection() throws SQLException {
        try{
            return hikariDataSource.getConnection();
        }catch (SQLException ex){
            logger.debug("Get Active Connection : "+hikariDataSource.getHikariPoolMXBean().getActiveConnections());
            logger.debug("Get Idle Connection : "+hikariDataSource.getHikariPoolMXBean().getIdleConnections());
            logger.debug("Get Thread awaiting Connection : "+hikariDataSource.getHikariPoolMXBean().getThreadsAwaitingConnection());
            logger.debug("Get Total Connection : "+hikariDataSource.getHikariPoolMXBean().getTotalConnections());
            throw ex;
        }
    }

    public synchronized void freeConnection(Connection connection) {
        hikariDataSource.evictConnection(connection);
    }

    private void readConfigFile() {
        block14:
        {
            FileInputStream fin = null;
            try {
                try {
                    Properties prop = new Properties();
                    File posFile = new File(this.getClass().getClassLoader().getResource(DBMW.configFileName).toURI());
                    if (posFile.exists()) {
                        fin = new FileInputStream(posFile);
                        prop.load(fin);
                        this.m_driverClassName = prop.getProperty("driverClassName");
                        this.m_databaseURL = prop.getProperty("databaseURL");
                        this.m_userName = prop.getProperty("userName");
                        this.m_Password = prop.getProperty("password");
                        break block14;
                    }
                    logger.error("DBMW Configuration file '" + configFileName + "' does not found.");
                } catch (Exception ee) {
                    logger.error("Exception in reading configuration file :" + ee);
                    if (fin == null) break block14;
                    try {
                        fin.close();
                    } catch (Exception p) {
                        logger.error("Error while closeing configuration file:" + p.getMessage());
                    }
                }
            } finally {
                if (fin != null) {
                    try {
                        fin.close();
                    } catch (Exception p) {
                        logger.error("Error while closeing configuration file:" + p.getMessage());
                    }
                }
            }
        }
    }

    public long getNextSequenceId(String tableName) throws Exception {
        SequenceDTO dto = this.nextIDMap.get(tableName);
        if (dto == null || dto.currentID == dto.maxID) {
            synchronized (LockManager.getLock("DBMW" + tableName)) {
                dto = this.nextIDMap.get(tableName);
                if (dto == null || dto.currentID == dto.maxID) {
                    if (dto == null) {
                        dto = new SequenceDTO();
                        this.nextIDMap.put(tableName, dto);
                    }
                    Connection connection = this.getConnection();
                    connection.setAutoCommit(false);
                    try (Statement stmt = connection.createStatement()) {
                        String query = String.format("select next_id from vb_sequencer where table_name = '%s' FOR UPDATE", tableName);
                        logger.debug(query);
                        ResultSet r = stmt.executeQuery(query);
                        if (r.next()) {
                            dto.currentID = r.getLong("next_id");
                            dto.maxID = dto.currentID + 100L;
                            if (dto.maxID >= Long.MAX_VALUE) {
                                dto.maxID = 1L;
                            }
                        }
                        query = String.format("update vb_sequencer set next_id = %d where table_name = '%s'", dto.maxID, tableName);
                        logger.debug(query);
                        stmt.executeUpdate(query);
                        connection.commit();
                    } catch (Exception ex) {
                        connection.rollback();
                        throw ex;
                    } finally {
                        if (connection != null) {
                            connection.setAutoCommit(true);
                            this.freeConnection(connection);
                        }
                    }
                }
            }
        }
        return dto.currentID++;
    }
}