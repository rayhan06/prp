/*
 * Decompiled with CFR 0.150.
 */
package dbm;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

@SuppressWarnings("ALL")
public class DBMR {
    private String m_driverClassName;
    private String m_databaseURL;
    private String m_userName;
    private String m_Password;
    private static final DBMR dbmr;
    public static String configFileName = "DBMR.properties";
    private static final Logger logger = Logger.getLogger(DBMR.class);
    private static HikariDataSource hikariDataSource;

    static {
        dbmr = new DBMR();
    }
    private DBMR() {
        this.readConfigFile();
        this.createDatabaseManager();
    }

    public static DBMR getInstance() throws SQLException, ClassNotFoundException, IllegalAccessException, InstantiationException {
        return dbmr;
    }

    private void createDatabaseManager() {
        HikariConfig hikariConfig = new HikariConfig();
        try {
            hikariConfig.setPoolName("readPool");
            hikariConfig.setDriverClassName(this.m_driverClassName);
            hikariConfig.setJdbcUrl(this.m_databaseURL);
            hikariConfig.setUsername(this.m_userName);
            hikariConfig.setPassword(this.m_Password);
            hikariConfig.setMaximumPoolSize(20);
            hikariConfig.setConnectionTimeout(60000);
            hikariConfig.setLeakDetectionThreshold(300000);
            hikariDataSource = new HikariDataSource(hikariConfig);
        } catch (Exception ex) {
            logger.error("Can't instantiate Hikari data-source");
        }
    }

    public synchronized Connection getConnection() throws SQLException {
        try{
            return hikariDataSource.getConnection();
        }catch (SQLException ex){
            logger.debug("Get Active Connection : "+hikariDataSource.getHikariPoolMXBean().getActiveConnections());
            logger.debug("Get Idle Connection : "+hikariDataSource.getHikariPoolMXBean().getIdleConnections());
            logger.debug("Get Thread awaiting Connection : "+hikariDataSource.getHikariPoolMXBean().getThreadsAwaitingConnection());
            logger.debug("Get Total Connection : "+hikariDataSource.getHikariPoolMXBean().getTotalConnections());
            throw ex;
        }
    }

    public void freeConnection(Connection connection) {
        hikariDataSource.evictConnection(connection);
    }

    private void readConfigFile() {
        block13:
        {
            FileInputStream fin = null;
            try {
                try {
                    Properties prop = new Properties();
                    File posFile = new File(this.getClass().getClassLoader().getResource(configFileName).toURI());
                    if (posFile.exists()) {
                        fin = new FileInputStream(posFile);
                        prop.load(fin);
                        this.m_driverClassName = prop.getProperty("driverClassName");
                        this.m_databaseURL = prop.getProperty("databaseURL");
                        this.m_userName = prop.getProperty("userName");
                        this.m_Password = prop.getProperty("password");
                        break block13;
                    }
                    logger.error("DBMR Configuration file '" + configFileName + "' does not found.");
                } catch (Exception ee) {
                    logger.error("Exception in reading configuration file :" + ee.getMessage());
                    if (fin == null) break block13;
                    try {
                        fin.close();
                    } catch (Exception p) {
                        logger.error("Error while closing DBMR configuration file:" + p.getMessage());
                    }
                }
            } finally {
                if (fin != null) {
                    try {
                        fin.close();
                    } catch (Exception p) {
                        logger.error("Error while closing DBMR configuration file:" + p.getMessage());
                    }
                }
            }
        }
    }
}