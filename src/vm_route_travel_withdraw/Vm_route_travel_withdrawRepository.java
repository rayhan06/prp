package vm_route_travel_withdraw;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import com.google.gson.Gson;
import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;
import vm_fuel_request.Vm_fuel_requestDTO;
import vm_requisition.CommonApprovalStatus;
import vm_route_travel.Vm_route_travelDTO;


public class Vm_route_travel_withdrawRepository implements Repository {
	Vm_route_travel_withdrawDAO vm_route_travel_withdrawDAO = null;
	Gson gson;
	
	public void setDAO(Vm_route_travel_withdrawDAO vm_route_travel_withdrawDAO)
	{
		this.vm_route_travel_withdrawDAO = vm_route_travel_withdrawDAO;
		gson = new Gson();
	}
	
	
	static Logger logger = Logger.getLogger(Vm_route_travel_withdrawRepository.class);

	Map<Long, Vm_route_travel_withdrawDTO>mapOfVm_route_travel_withdrawDTOToid;
	Map<Long, Set<Vm_route_travel_withdrawDTO> >mapOfVm_route_travel_withdrawDTOToRequesterOrgId;



	static Vm_route_travel_withdrawRepository instance = null;  
	private Vm_route_travel_withdrawRepository(){
		mapOfVm_route_travel_withdrawDTOToid = new ConcurrentHashMap<>();
		mapOfVm_route_travel_withdrawDTOToRequesterOrgId = new ConcurrentHashMap<>();

		setDAO(new Vm_route_travel_withdrawDAO());

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Vm_route_travel_withdrawRepository getInstance(){
		if (instance == null){
			instance = new Vm_route_travel_withdrawRepository();
		}
		return instance;
	}

	public Vm_route_travel_withdrawDTO clone(Vm_route_travel_withdrawDTO dto) {
		String raw = gson.toJson(dto);
		return gson.fromJson(raw, Vm_route_travel_withdrawDTO.class);
	}

	public List<Vm_route_travel_withdrawDTO> clone(List<Vm_route_travel_withdrawDTO> dtoList) {
		return dtoList
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}

	public Vm_route_travel_withdrawDTO getVm_route_travel_withdrawDTOByIDWithoutClone( long ID){
		return mapOfVm_route_travel_withdrawDTOToid.get(ID);
	}

	public void reload(boolean reloadAll){
		if(vm_route_travel_withdrawDAO == null)
		{
			return;
		}
		try {
			List<Vm_route_travel_withdrawDTO> vm_route_travel_withdrawDTOs = (List<Vm_route_travel_withdrawDTO>) vm_route_travel_withdrawDAO.getAll(reloadAll);
			for(Vm_route_travel_withdrawDTO vm_route_travel_withdrawDTO : vm_route_travel_withdrawDTOs) {
				Vm_route_travel_withdrawDTO oldVm_route_travel_withdrawDTO = getVm_route_travel_withdrawDTOByIDWithoutClone(vm_route_travel_withdrawDTO.id);


				if( oldVm_route_travel_withdrawDTO != null ) {
					mapOfVm_route_travel_withdrawDTOToid.remove(oldVm_route_travel_withdrawDTO.id);

					if(mapOfVm_route_travel_withdrawDTOToRequesterOrgId.containsKey(oldVm_route_travel_withdrawDTO.requesterOrgId)) {
						mapOfVm_route_travel_withdrawDTOToRequesterOrgId.get(oldVm_route_travel_withdrawDTO.requesterOrgId).remove(oldVm_route_travel_withdrawDTO);
					}
					if(mapOfVm_route_travel_withdrawDTOToRequesterOrgId.get(oldVm_route_travel_withdrawDTO.requesterOrgId).isEmpty()) {
						mapOfVm_route_travel_withdrawDTOToRequesterOrgId.remove(oldVm_route_travel_withdrawDTO.requesterOrgId);
					}

				}
				if(vm_route_travel_withdrawDTO.isDeleted == 0) 
				{
					
					mapOfVm_route_travel_withdrawDTOToid.put(vm_route_travel_withdrawDTO.id, vm_route_travel_withdrawDTO);

					if( ! mapOfVm_route_travel_withdrawDTOToRequesterOrgId.containsKey(vm_route_travel_withdrawDTO.requesterOrgId)) {
						mapOfVm_route_travel_withdrawDTOToRequesterOrgId.put(vm_route_travel_withdrawDTO.requesterOrgId, new HashSet<>());
					}
					mapOfVm_route_travel_withdrawDTOToRequesterOrgId.get(vm_route_travel_withdrawDTO.requesterOrgId).add(vm_route_travel_withdrawDTO);

					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Vm_route_travel_withdrawDTO> getVm_route_travel_withdrawList() {
		List <Vm_route_travel_withdrawDTO> vm_route_travel_withdraws = new ArrayList<Vm_route_travel_withdrawDTO>(this.mapOfVm_route_travel_withdrawDTOToid.values());
		return vm_route_travel_withdraws;
	}
	
	
	public Vm_route_travel_withdrawDTO getVm_route_travel_withdrawDTOByid( long id){
		return clone(mapOfVm_route_travel_withdrawDTOToid.get(id));
	}

	public List<Vm_route_travel_withdrawDTO> getVm_route_travelDTOByRequesterOrgID(long requester_org_id) {
		return clone(new ArrayList<>( mapOfVm_route_travel_withdrawDTOToRequesterOrgId.getOrDefault(requester_org_id,new HashSet<>())));
	}

	public List<Vm_route_travel_withdrawDTO> getSatisfiedVm_route_travel_withdrawDTOByRequesterOrgID(long requester_org_id) {
		List<Vm_route_travel_withdrawDTO> vm_route_travel_withdrawDTOs = new ArrayList<>( mapOfVm_route_travel_withdrawDTOToRequesterOrgId.getOrDefault(requester_org_id,new HashSet<>()));
		return vm_route_travel_withdrawDTOs
				.stream()
				.filter(vm_route_travel_withdrawDTO -> vm_route_travel_withdrawDTO.status == CommonApprovalStatus.SATISFIED.getValue())
				.map(vm_route_travel_withdrawDTO -> clone(vm_route_travel_withdrawDTO))
				.sorted((f1, f2) -> Long.compare(f2.id, f1.id))
				.collect(Collectors.toList());
	}

	public List<Vm_route_travel_withdrawDTO> getPendingVm_route_travel_withdrawDTOByRequesterOrgID(long requester_org_id) {
		List<Vm_route_travel_withdrawDTO> vm_route_travel_withdrawDTOs = new ArrayList<>( mapOfVm_route_travel_withdrawDTOToRequesterOrgId.getOrDefault(requester_org_id,new HashSet<>()));




		List<Vm_route_travel_withdrawDTO> dtos = vm_route_travel_withdrawDTOs
				.stream()
				.filter(vm_route_travel_withdrawDTO -> vm_route_travel_withdrawDTO.status == CommonApprovalStatus.PENDING.getValue())
				.map(vm_route_travel_withdrawDTO -> clone(vm_route_travel_withdrawDTO))
				.sorted((f1, f2) -> Long.compare(f2.id, f1.id))
				.collect(Collectors.toList());


		return dtos;


	}
	
	
	

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "vm_route_travel_withdraw";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


