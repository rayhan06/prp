package vm_route_travel_withdraw;

import java.io.IOException;
import java.io.*;
import java.text.SimpleDateFormat;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import card_approval_mapping.Card_approval_mappingDAO;
import card_approval_mapping.CreateCardApprovalModel;
import card_info.CardApprovalResponse;
import card_info.CardCategoryEnum;
import common.ApiResponse;
import org.apache.log4j.Logger;

import login.LoginDTO;
import permission.MenuConstants;
import role.PermissionRepository;


import role.RoleDTO;
import sessionmanager.SessionConstants;

import task_type.TaskTypeEnum;
import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;
import util.CommonRequestHandler;
import util.RecordNavigationManager4;

import java.util.*;
import javax.servlet.http.*;
import java.util.UUID;





import geolocation.GeoLocationDAO2;
import java.util.StringTokenizer;

import com.google.gson.Gson;

import pb.*;
import pbReport.*;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import vm_fuel_request.Vm_fuel_requestStatusEnum;
import vm_requisition.CommonApprovalStatus;
import vm_route_travel.Vm_route_travelDAO;
import vm_route_travel.Vm_route_travelDTO;
import vm_route_travel_withdraw_approval_mapping.CreateWithdrawApprovalModel;
import vm_route_travel_withdraw_approval_mapping.Vm_route_travel_withdraw_approval_mappingDAO;


/**
 * Servlet implementation class Vm_route_travel_withdrawServlet
 */
@WebServlet("/Vm_route_travel_withdrawServlet")
@MultipartConfig
public class Vm_route_travel_withdrawServlet extends HttpServlet
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Vm_route_travel_withdrawServlet.class);

    String tableName = "vm_route_travel_withdraw";

	Vm_route_travel_withdrawDAO vm_route_travel_withdrawDAO;
	CommonRequestHandler commonRequestHandler;
    private final Gson gson = new Gson();

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Vm_route_travel_withdrawServlet()
	{
        super();
    	try
    	{
			vm_route_travel_withdrawDAO = new Vm_route_travel_withdrawDAO(tableName);
			commonRequestHandler = new CommonRequestHandler(vm_route_travel_withdrawDAO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		System.out.println("In doget request = " + request);
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		try
		{
			String actionType = request.getParameter("actionType");
			if(actionType.equals("getAddPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_ROUTE_TRAVEL_WITHDRAW_ADD))
				{
					commonRequestHandler.getAddPage(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getEditPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_ROUTE_TRAVEL_WITHDRAW_REQUEST))
				{
					getVm_route_travel_withdraw(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getURL"))
			{
				String URL = request.getParameter("URL");
				System.out.println("URL = " + URL);
				response.sendRedirect(URL);
			}
			else if(actionType.equals("search"))
			{
				System.out.println("search requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_ROUTE_TRAVEL_WITHDRAW_SEARCH))
				{
					if(isPermanentTable)
					{
						String filter = request.getParameter("filter");
						System.out.println("filter = " + filter);
						if(filter!=null)
						{
							filter = ""; //shouldn't be directly used, rather manipulate it.
							searchVm_route_travel_withdraw(request, response, isPermanentTable, filter);
						}
						else
						{
							searchVm_route_travel_withdraw(request, response, isPermanentTable, "");
						}
					}
					else
					{
						//searchVm_route_travel_withdraw(request, response, tempTableName, isPermanentTable);
					}
				}
			}

			else if(actionType.equals("request"))
			{
				System.out.println("request requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_ROUTE_TRAVEL_WITHDRAW_REQUEST))
				{
					if(isPermanentTable)
					{
						String filter = request.getParameter("filter");
						System.out.println("filter = " + filter);
						if(filter!=null)
						{
							filter = ""; //shouldn't be directly used, rather manipulate it.
							requestVm_route_travel_withdraw(request, response, isPermanentTable, filter);
						}
						else
						{
							requestVm_route_travel_withdraw(request, response, isPermanentTable, "");
						}
					}
					else
					{

					}
				}
			}

			else if(actionType.equals("view"))
			{
				System.out.println("view requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_ROUTE_TRAVEL_WITHDRAW_SEARCH))
				{
					//commonRequestHandler.view(request, response);
					searchVm_route_travelPayment(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}
			else
			{
				request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}





	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		// TODO Auto-generated method stub
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		System.out.println("doPost");
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);

		try
		{
			String actionType = request.getParameter("actionType");
			System.out.println("actionType = " + actionType);
			if(actionType.equals("add"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_ROUTE_TRAVEL_WITHDRAW_ADD))
				{
					System.out.println("going to  addVm_route_travel_withdraw ");
					addVm_route_travel_withdraw(request, response, true, userDTO, true);
				}
				else
				{
					System.out.println("Not going to  addVm_route_travel_withdraw ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}

			else if(actionType.equals("getDTO"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_ROUTE_TRAVEL_WITHDRAW_ADD))
				{
					getDTO(request, response);
				}
				else
				{
					System.out.println("Not going to  addVm_route_travel_withdraw ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}
			else if(actionType.equals("edit"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_ROUTE_TRAVEL_WITHDRAW_REQUEST))
				{
					addVm_route_travel_withdraw(request, response, false, userDTO, isPermanentTable);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("delete"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_ROUTE_TRAVEL_WITHDRAW_REQUEST))
				{
					commonRequestHandler.delete(request, response, userDTO);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("search"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_ROUTE_TRAVEL_WITHDRAW_SEARCH))
				{
					searchVm_route_travel_withdraw(request, response, true, "");
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getGeo"))
			{
				System.out.println("going to geoloc ");
				request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
			}

			else if(actionType.equals("approveRequest"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_ROUTE_TRAVEL_WITHDRAW_REQUEST))
				{
					System.out.println("going to  addRequestVm_route_travel_withdraw ");
					addRequestVm_route_travel_withdraw(request, response, false, userDTO, true);
				}
				else
				{
					System.out.println("Not going to  addVm_route_travel_withdraw ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}


			}



		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}

	private void getDTO(HttpServletRequest request, HttpServletResponse response)
	{
		try
		{
			System.out.println("In getDTO");
			//Vm_route_travel_withdrawDTO vm_route_travel_withdrawDTO = (Vm_route_travel_withdrawDTO)vm_route_travel_withdrawDAO.getDTOByID(Long.parseLong(request.getParameter("ID")));

			Vm_route_travel_withdrawDTO vm_route_travel_withdrawDTO = Vm_route_travel_withdrawRepository.getInstance().getVm_route_travel_withdrawDTOByid(Long.parseLong(request.getParameter("ID")));

			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");

			String encoded = this.gson.toJson(vm_route_travel_withdrawDTO);
			System.out.println("json encoded data = " + encoded);
			out.print(encoded);
			out.flush();
		}
		catch (NumberFormatException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void addRequestVm_route_travel_withdraw(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, boolean isPermanentTable) throws IOException
	{
		// TODO Auto-generated method stub
		try
		{
			request.setAttribute("failureMessage", "");
			System.out.println("%%%% addVm_route_travel_withdraw");
			String path = getServletContext().getRealPath("/img2/");
			Vm_route_travel_withdrawDTO vm_route_travel_withdrawDTO;





			if(addFlag == true)
			{
				vm_route_travel_withdrawDTO = new Vm_route_travel_withdrawDTO();
			}
			else
			{

				//vm_route_travel_withdrawDTO = (Vm_route_travel_withdrawDTO)vm_route_travel_withdrawDAO.getDTOByID(Long.parseLong(request.getParameter("iD")));

				vm_route_travel_withdrawDTO = Vm_route_travel_withdrawRepository.getInstance().getVm_route_travel_withdrawDTOByid(Long.parseLong(request.getParameter("iD")));

				vm_route_travel_withdrawDTO.iD =  Long.parseLong(request.getParameter("iD"));
			}



			String Value = "";

			Value = request.getParameter("status");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("status = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				vm_route_travel_withdrawDTO.status = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}



			System.out.println("Done adding  addVm_route_travel_withdraw dto = " + vm_route_travel_withdrawDTO);
			long returnedID = -1;

			if(isPermanentTable == false) //add new row for validation and make the old row outdated
			{
				vm_route_travel_withdrawDAO.setIsDeleted(vm_route_travel_withdrawDTO.iD, CommonDTO.OUTDATED);
				returnedID = vm_route_travel_withdrawDAO.add(vm_route_travel_withdrawDTO);
				vm_route_travel_withdrawDAO.setIsDeleted(returnedID, CommonDTO.WAITING_FOR_APPROVAL);
			}
			else if(addFlag == true)
			{
				returnedID = vm_route_travel_withdrawDAO.manageWriteOperations(vm_route_travel_withdrawDTO, SessionConstants.INSERT, -1, userDTO);
			}
			else
			{
				returnedID = vm_route_travel_withdrawDAO.manageWriteOperations(vm_route_travel_withdrawDTO, SessionConstants.UPDATE, -1, userDTO);
			}

			if(isPermanentTable)
			{
				String inPlaceSubmit = request.getParameter("inplacesubmit");

				if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
				{
					getVm_route_travel_withdraw(request, response, returnedID);
				}
				else
				{
					response.sendRedirect("Vm_route_travel_withdrawServlet?actionType=search");
				}
			}
			else
			{
				commonRequestHandler.validate(vm_route_travel_withdrawDAO.getDTOByID(returnedID), request, response, userDTO);
			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	private void addVm_route_travel_withdraw(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, boolean isPermanentTable) throws IOException
	{
		// TODO Auto-generated method stub
		try
		{
			request.setAttribute("failureMessage", "");
			System.out.println("%%%% addVm_route_travel_withdraw");
			String path = getServletContext().getRealPath("/img2/");
			Vm_route_travel_withdrawDTO vm_route_travel_withdrawDTO;
			SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");

//			String routeTravelTableName = "vm_route_travel";
//			Vm_route_travelDAO vm_route_travelDAO = new Vm_route_travelDAO(routeTravelTableName);
//			Vm_route_travelDTO vm_route_travelDTO = vm_route_travelDAO.getDTOByOrganogramID(userDTO.organogramID);
//			if(vm_route_travelDTO!=null){
//				//vm_route_travelDTO.status=5;
//				vm_route_travelDTO.status= CommonApprovalStatus.WITHDRAWN.getValue();
//				vm_route_travelDAO.manageWriteOperations(vm_route_travelDTO, SessionConstants.UPDATE, -1, userDTO);
//			}


			if(addFlag == true)
			{
				vm_route_travel_withdrawDTO = new Vm_route_travel_withdrawDTO();
			}
			else
			{

				//vm_route_travel_withdrawDTO = (Vm_route_travel_withdrawDTO)vm_route_travel_withdrawDAO.getDTOByID(Long.parseLong(request.getParameter("iD")));
				vm_route_travel_withdrawDTO = Vm_route_travel_withdrawRepository.getInstance().getVm_route_travel_withdrawDTOByid(Long.parseLong(request.getParameter("iD")));
			}

			String Value = "";

			Value = request.getParameter("routeTravelId");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("routeTravelId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				vm_route_travel_withdrawDTO.routeTravelId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("endDate");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("endDate = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				try
				{
					Date d = f.parse(Value);
					vm_route_travel_withdrawDTO.endDate = d.getTime();
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = String.valueOf(CommonApprovalStatus.PENDING.getValue());//request.getParameter("status");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("status = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				vm_route_travel_withdrawDTO.status = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("requesterOrgId");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("requesterOrgId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				vm_route_travel_withdrawDTO.requesterOrgId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("requesterOfficeId");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("requesterOfficeId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				vm_route_travel_withdrawDTO.requesterOfficeId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("requesterOfficeUnitId");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("requesterOfficeUnitId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				vm_route_travel_withdrawDTO.requesterOfficeUnitId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("requesterEmpId");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("requesterEmpId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				vm_route_travel_withdrawDTO.requesterEmpId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("requesterPhoneNum");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("requesterPhoneNum = " + Value);
			if(Value != null)
			{
				vm_route_travel_withdrawDTO.requesterPhoneNum = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("requesterNameEn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("requesterNameEn = " + Value);
			if(Value != null)
			{
				vm_route_travel_withdrawDTO.requesterNameEn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("requesterNameBn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("requesterNameBn = " + Value);
			if(Value != null)
			{
				vm_route_travel_withdrawDTO.requesterNameBn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("requesterOfficeNameEn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("requesterOfficeNameEn = " + Value);
			if(Value != null)
			{
				vm_route_travel_withdrawDTO.requesterOfficeNameEn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("requesterOfficeNameBn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("requesterOfficeNameBn = " + Value);
			if(Value != null)
			{
				vm_route_travel_withdrawDTO.requesterOfficeNameBn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("requesterOfficeUnitNameEn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("requesterOfficeUnitNameEn = " + Value);
			if(Value != null)
			{
				vm_route_travel_withdrawDTO.requesterOfficeUnitNameEn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("requesterOfficeUnitNameBn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("requesterOfficeUnitNameBn = " + Value);
			if(Value != null)
			{
				vm_route_travel_withdrawDTO.requesterOfficeUnitNameBn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("requesterOfficeUnitOrgNameEn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("requesterOfficeUnitOrgNameEn = " + Value);
			if(Value != null)
			{
				vm_route_travel_withdrawDTO.requesterOfficeUnitOrgNameEn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("requesterOfficeUnitOrgNameBn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("requesterOfficeUnitOrgNameBn = " + Value);
			if(Value != null)
			{
				vm_route_travel_withdrawDTO.requesterOfficeUnitOrgNameBn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			if(addFlag)
			{
				Calendar c = Calendar.getInstance();
				c.set(Calendar.HOUR_OF_DAY, 0);
				c.set(Calendar.MINUTE, 0);
				c.set(Calendar.SECOND, 0);
				c.set(Calendar.MILLISECOND, 0);

				vm_route_travel_withdrawDTO.insertionDate = c.getTimeInMillis();
			}


			Value = request.getParameter("insertedBy");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("insertedBy = " + Value);
			if(Value != null)
			{
				vm_route_travel_withdrawDTO.insertedBy = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("modifiedBy");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("modifiedBy = " + Value);
			if(Value != null)
			{
				vm_route_travel_withdrawDTO.modifiedBy = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			System.out.println("Done adding  addVm_route_travel_withdraw dto = " + vm_route_travel_withdrawDTO);
			long returnedID = -1;

			if(isPermanentTable == false) //add new row for validation and make the old row outdated
			{
				vm_route_travel_withdrawDAO.setIsDeleted(vm_route_travel_withdrawDTO.iD, CommonDTO.OUTDATED);
				returnedID = vm_route_travel_withdrawDAO.add(vm_route_travel_withdrawDTO);
				vm_route_travel_withdrawDAO.setIsDeleted(returnedID, CommonDTO.WAITING_FOR_APPROVAL);
			}
			else if(addFlag == true)
			{
				returnedID = vm_route_travel_withdrawDAO.manageWriteOperations(vm_route_travel_withdrawDTO, SessionConstants.INSERT, -1, userDTO);
			}
			else
			{
				returnedID = vm_route_travel_withdrawDAO.manageWriteOperations(vm_route_travel_withdrawDTO, SessionConstants.UPDATE, -1, userDTO);
			}

			TaskTypeEnum taskTypeEnum;
			taskTypeEnum = TaskTypeEnum.ROUTE_WITHDRAW;

			CreateWithdrawApprovalModel model = new CreateWithdrawApprovalModel.CreateCardApprovalModelBuilder()
					.setTaskTypeId(taskTypeEnum.getValue())
					.setWithdrawInfoId(vm_route_travel_withdrawDTO.iD)
					.setRequesterEmployeeRecordId(vm_route_travel_withdrawDTO.requesterEmpId)
					.setCardEmployeeInfoId(userDTO.employeeID)
					.setCardEmployeeOfficeInfoId(userDTO.officeID)
					.setEmployeeRecordId(userDTO.employee_record_id)
					.build();
			CardApprovalResponse cardApprovalResponse = new Vm_route_travel_withdraw_approval_mappingDAO().createWithdrawApproval(model);

			//System.out.println("cardApprovalResponse: "+cardApprovalResponse);


			if(isPermanentTable)
			{
				String inPlaceSubmit = request.getParameter("inplacesubmit");

				if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
				{
					getVm_route_travel_withdraw(request, response, returnedID);
				}
				else
				{
					//response.sendRedirect("Vm_route_travel_withdrawServlet?actionType=search");
					ApiResponse apiResponse=null;
					apiResponse = ApiResponse.makeSuccessResponse("Vm_route_travel_withdrawServlet?actionType=search");

					PrintWriter pw = response.getWriter();
					pw.write(apiResponse.getJSONString());
					pw.flush();
					pw.close();
				}
			}
			else
			{
				commonRequestHandler.validate(vm_route_travel_withdrawDAO.getDTOByID(returnedID), request, response, userDTO);
			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}









	private void getVm_route_travel_withdraw(HttpServletRequest request, HttpServletResponse response, long id) throws ServletException, IOException
	{
		System.out.println("in getVm_route_travel_withdraw");
		Vm_route_travel_withdrawDTO vm_route_travel_withdrawDTO = null;
		try
		{
			//vm_route_travel_withdrawDTO = (Vm_route_travel_withdrawDTO)vm_route_travel_withdrawDAO.getDTOByID(id);
			vm_route_travel_withdrawDTO = Vm_route_travel_withdrawRepository.getInstance().getVm_route_travel_withdrawDTOByid(id);
			request.setAttribute("ID", vm_route_travel_withdrawDTO.iD);
			request.setAttribute("vm_route_travel_withdrawDTO",vm_route_travel_withdrawDTO);
			request.setAttribute("vm_route_travel_withdrawDAO",vm_route_travel_withdrawDAO);

			String URL= "";

			String inPlaceEdit = request.getParameter("inplaceedit");
			String inPlaceSubmit = request.getParameter("inplacesubmit");
			String getBodyOnly = request.getParameter("getBodyOnly");

			if(inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase(""))
			{
				URL = "vm_route_travel_withdraw/vm_route_travel_withdrawInPlaceEdit.jsp";
				request.setAttribute("inplaceedit","");
			}
			else if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
			{
				URL = "vm_route_travel_withdraw/vm_route_travel_withdrawSearchRow.jsp";
				request.setAttribute("inplacesubmit","");
			}
			else
			{
				if(getBodyOnly  != null && !getBodyOnly.equalsIgnoreCase(""))
				{
					URL = "vm_route_travel_withdraw/vm_route_travel_withdrawEditBody.jsp?actionType=edit";
				}
				else
				{
					URL = "vm_route_travel_withdraw/vm_route_travel_withdrawEdit.jsp?actionType=edit";
				}
			}

			RequestDispatcher rd = request.getRequestDispatcher(URL);
			rd.forward(request, response);
		}
		catch (NumberFormatException e)
		{
			e.printStackTrace();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}


	private void getVm_route_travel_withdraw(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		getVm_route_travel_withdraw(request, response, Long.parseLong(request.getParameter("ID")));
	}

	//requesthVm_route_travel_withdraw

	private void requestVm_route_travel_withdraw(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException
	{
		System.out.println("in  requestVm_route_travel_withdraw 1");
		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String ajax = request.getParameter("ajax");
		boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);

		//filter = " status = 0 ";

		filter = " status = "+CommonApprovalStatus.PENDING.getValue()+" ";

		RecordNavigationManager4 rnManager = new RecordNavigationManager4(
				SessionConstants.NAV_VM_ROUTE_TRAVEL_WITHDRAW,
				request,
				vm_route_travel_withdrawDAO,
				SessionConstants.VIEW_VM_ROUTE_TRAVEL_WITHDRAW,
				SessionConstants.SEARCH_VM_ROUTE_TRAVEL_WITHDRAW,
				tableName,
				isPermanent,
				userDTO,
				filter,
				true);
		try
		{
			System.out.println("trying to dojob");
			rnManager.doJob(loginDTO);
		}
		catch(Exception e)
		{
			System.out.println("failed to dojob" + e);
		}

		request.setAttribute("vm_route_travel_withdrawDAO",vm_route_travel_withdrawDAO);
		RequestDispatcher rd;
		if(!isPermanent)
		{
			if(hasAjax == false)
			{
				System.out.println("Going to vm_route_travel_withdraw/vm_route_travel_withdrawApproval.jsp");
				rd = request.getRequestDispatcher("vm_route_travel_withdraw/vm_route_travel_withdrawApproval.jsp");
			}
			else
			{
				System.out.println("Going to vm_route_travel_withdraw/vm_route_travel_withdrawApprovalForm.jsp");
				rd = request.getRequestDispatcher("vm_route_travel_withdraw/vm_route_travel_withdrawApprovalForm.jsp");
			}
		}
		else
		{
			if(hasAjax == false)
			{
				System.out.println("Going to vm_route_travel_withdraw/vm_route_travel_withdrawRequest.jsp");
				rd = request.getRequestDispatcher("vm_route_travel_withdraw/vm_route_travel_withdrawRequest.jsp");
			}
			else
			{
				System.out.println("Going to vm_route_travel_withdraw/vm_route_travel_withdrawSearchForm.jsp");
				rd = request.getRequestDispatcher("vm_route_travel_withdraw/vm_route_travel_withdrawRequestBody.jsp");
			}
		}
		rd.forward(request, response);
	}

	private void searchVm_route_travel_withdraw(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException
	{

		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		RoleDTO role = PermissionRepository.getRoleDTOByRoleID(userDTO.roleID);
		boolean isAdmin = role.ID == SessionConstants.VEHICLE_ADMIN_ROLE || role.ID == SessionConstants.ADMIN_ROLE;
		String ajax = request.getParameter("ajax");
		boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);

		if(!isAdmin){
			filter = " requester_emp_id = "+userDTO.employee_record_id+" ";
		}

		//filter = " status = "+ Vm_fuel_requestStatusEnum.APPROVED.getValue()+" ";

        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
			SessionConstants.NAV_VM_ROUTE_TRAVEL_WITHDRAW,
			request,
			vm_route_travel_withdrawDAO,
			SessionConstants.VIEW_VM_ROUTE_TRAVEL_WITHDRAW,
			SessionConstants.SEARCH_VM_ROUTE_TRAVEL_WITHDRAW,
			tableName,
			isPermanent,
			userDTO,
			filter,
			true);
        try
        {
			System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        }
        catch(Exception e)
        {
			System.out.println("failed to dojob" + e);
        }

		request.setAttribute("vm_route_travel_withdrawDAO",vm_route_travel_withdrawDAO);
        RequestDispatcher rd;
        if(!isPermanent)
        {
        	if(hasAjax == false)
	        {
	        	System.out.println("Going to vm_route_travel_withdraw/vm_route_travel_withdrawApproval.jsp");
	        	rd = request.getRequestDispatcher("vm_route_travel_withdraw/vm_route_travel_withdrawApproval.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to vm_route_travel_withdraw/vm_route_travel_withdrawApprovalForm.jsp");
	        	rd = request.getRequestDispatcher("vm_route_travel_withdraw/vm_route_travel_withdrawApprovalForm.jsp");
	        }
        }
        else
        {
	        if(hasAjax == false)
	        {
	        	System.out.println("Going to vm_route_travel_withdraw/vm_route_travel_withdrawSearch.jsp");
	        	rd = request.getRequestDispatcher("vm_route_travel_withdraw/vm_route_travel_withdrawSearch.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to vm_route_travel_withdraw/vm_route_travel_withdrawSearchForm.jsp");
	        	rd = request.getRequestDispatcher("vm_route_travel_withdraw/vm_route_travel_withdrawSearchForm.jsp");
	        }
        }
		rd.forward(request, response);
	}

	private void searchVm_route_travelPayment(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		String travelTableName = "vm_route_travel";
		Vm_route_travelDAO vm_route_travelDAO;
		vm_route_travelDAO = new Vm_route_travelDAO(travelTableName);
		System.out.println("in  searchVm_route_travel 1");
		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String ajax = request.getParameter("ajax");
		boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);

		request.setAttribute("requester_org_id",request.getParameter("ID"));
		request.setAttribute("vm_route_travelDAO",vm_route_travelDAO);
		request.setAttribute("totalArrearsBill",request.getParameter("routeTravelId"));
		System.out.println("hidden_totalArrearsBill servlet: "+request.getParameter("hidden_totalArrearsBill"));
		RequestDispatcher rd;

		{
			if(hasAjax == false)
			{
				System.out.println("Going to vm_route_travel_withdraw/vm_route_travelSearchPayment.jsp");
				rd = request.getRequestDispatcher("vm_route_travel_withdraw/vm_route_travelSearchPayment.jsp");

//				System.out.println("Going to vm_route_travel_withdraw/vm_route_travel_withdrawSearch.jsp");
//				rd = request.getRequestDispatcher("vm_route_travel_withdraw/vm_route_travel_withdrawSearch.jsp");

			}
			else
			{
				System.out.println("Going to vm_route_travel/vm_route_travelSearchPaymentForm.jsp");
				rd = request.getRequestDispatcher("vm_route_travel/vm_route_travelSearchPaymentForm.jsp");
			}
		}
		rd.forward(request, response);
	}

}

