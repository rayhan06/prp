package vm_route_travel_withdraw;
import java.util.*; 
import util.*;


public class Vm_route_travel_withdrawMAPS extends CommonMaps
{	
	public Vm_route_travel_withdrawMAPS(String tableName)
	{
		

		java_DTO_map.put("id".toLowerCase(), "id".toLowerCase());
		java_DTO_map.put("routeTravelId".toLowerCase(), "routeTravelId".toLowerCase());
		java_DTO_map.put("endDate".toLowerCase(), "endDate".toLowerCase());
		java_DTO_map.put("status".toLowerCase(), "status".toLowerCase());
		java_DTO_map.put("requesterOrgId".toLowerCase(), "requesterOrgId".toLowerCase());
		java_DTO_map.put("requesterOfficeId".toLowerCase(), "requesterOfficeId".toLowerCase());
		java_DTO_map.put("requesterOfficeUnitId".toLowerCase(), "requesterOfficeUnitId".toLowerCase());
		java_DTO_map.put("requesterEmpId".toLowerCase(), "requesterEmpId".toLowerCase());
		java_DTO_map.put("requesterPhoneNum".toLowerCase(), "requesterPhoneNum".toLowerCase());
		java_DTO_map.put("requesterNameEn".toLowerCase(), "requesterNameEn".toLowerCase());
		java_DTO_map.put("requesterNameBn".toLowerCase(), "requesterNameBn".toLowerCase());
		java_DTO_map.put("requesterOfficeNameEn".toLowerCase(), "requesterOfficeNameEn".toLowerCase());
		java_DTO_map.put("requesterOfficeNameBn".toLowerCase(), "requesterOfficeNameBn".toLowerCase());
		java_DTO_map.put("requesterOfficeUnitNameEn".toLowerCase(), "requesterOfficeUnitNameEn".toLowerCase());
		java_DTO_map.put("requesterOfficeUnitNameBn".toLowerCase(), "requesterOfficeUnitNameBn".toLowerCase());
		java_DTO_map.put("requesterOfficeUnitOrgNameEn".toLowerCase(), "requesterOfficeUnitOrgNameEn".toLowerCase());
		java_DTO_map.put("requesterOfficeUnitOrgNameBn".toLowerCase(), "requesterOfficeUnitOrgNameBn".toLowerCase());
		java_DTO_map.put("insertionDate".toLowerCase(), "insertionDate".toLowerCase());
		java_DTO_map.put("insertedBy".toLowerCase(), "insertedBy".toLowerCase());
		java_DTO_map.put("modifiedBy".toLowerCase(), "modifiedBy".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("route_travel_id".toLowerCase(), "routeTravelId".toLowerCase());
		java_SQL_map.put("end_date".toLowerCase(), "endDate".toLowerCase());

		java_Text_map.put("Id".toLowerCase(), "id".toLowerCase());
		java_Text_map.put("Route Travel Id".toLowerCase(), "routeTravelId".toLowerCase());
		java_Text_map.put("End Date".toLowerCase(), "endDate".toLowerCase());
		java_Text_map.put("Status".toLowerCase(), "status".toLowerCase());
		java_Text_map.put("Requester Org Id".toLowerCase(), "requesterOrgId".toLowerCase());
		java_Text_map.put("Requester Office Id".toLowerCase(), "requesterOfficeId".toLowerCase());
		java_Text_map.put("Requester Office Unit Id".toLowerCase(), "requesterOfficeUnitId".toLowerCase());
		java_Text_map.put("Requester Emp Id".toLowerCase(), "requesterEmpId".toLowerCase());
		java_Text_map.put("Requester Phone Num".toLowerCase(), "requesterPhoneNum".toLowerCase());
		java_Text_map.put("Requester Name En".toLowerCase(), "requesterNameEn".toLowerCase());
		java_Text_map.put("Requester Name Bn".toLowerCase(), "requesterNameBn".toLowerCase());
		java_Text_map.put("Requester Office Name En".toLowerCase(), "requesterOfficeNameEn".toLowerCase());
		java_Text_map.put("Requester Office Name Bn".toLowerCase(), "requesterOfficeNameBn".toLowerCase());
		java_Text_map.put("Requester Office Unit Name En".toLowerCase(), "requesterOfficeUnitNameEn".toLowerCase());
		java_Text_map.put("Requester Office Unit Name Bn".toLowerCase(), "requesterOfficeUnitNameBn".toLowerCase());
		java_Text_map.put("Requester Office Unit Org Name En".toLowerCase(), "requesterOfficeUnitOrgNameEn".toLowerCase());
		java_Text_map.put("Requester Office Unit Org Name Bn".toLowerCase(), "requesterOfficeUnitOrgNameBn".toLowerCase());
		java_Text_map.put("Insertion Date".toLowerCase(), "insertionDate".toLowerCase());
		java_Text_map.put("Inserted By".toLowerCase(), "insertedBy".toLowerCase());
		java_Text_map.put("Modified By".toLowerCase(), "modifiedBy".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}