package vm_route_travel_withdraw;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;

import common.ConnectionAndStatementUtil;
import sessionmanager.SessionConstants;
import java.sql.SQLException;


import org.apache.log4j.Logger;

import dbm.*;

import repository.RepositoryManager;

import util.*;
import pb.*;
import user.UserDTO;
import vm_requisition.CommonApprovalStatus;
import vm_vehicle.Vm_vehicleDTO;
import vm_vehicle_office_assignment.Vm_vehicle_office_assignmentDTO;

public class Vm_route_travel_withdrawDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());
	

	
	public Vm_route_travel_withdrawDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";
		commonMaps = new Vm_route_travel_withdrawMAPS(tableName);
		columnNames = new String[] 
		{
			"ID",
			"route_travel_id",
			"end_date",
			"status",
			"requester_org_id",
			"requester_office_id",
			"requester_office_unit_id",
			"requester_emp_id",
			"requester_phone_num",
			"requester_name_en",
			"requester_name_bn",
			"requester_office_name_en",
			"requester_office_name_bn",
			"requester_office_unit_name_en",
			"requester_office_unit_name_bn",
			"requester_office_unit_org_name_en",
			"requester_office_unit_org_name_bn",
			"insertion_date",
			"inserted_by",
			"modified_by",
			"search_column",
			"isDeleted",
			"lastModificationTime"
		};
	}
	
	public Vm_route_travel_withdrawDAO()
	{
		this("vm_route_travel_withdraw");		
	}


	public void setSearchColumn(Vm_route_travel_withdrawDTO vm_route_travel_withdrawDTO)
	{
		vm_route_travel_withdrawDTO.searchColumn = "";

		vm_route_travel_withdrawDTO.searchColumn += vm_route_travel_withdrawDTO.requesterNameEn+" " ;
		vm_route_travel_withdrawDTO.searchColumn += vm_route_travel_withdrawDTO.requesterNameBn+" " ;
		vm_route_travel_withdrawDTO.searchColumn += vm_route_travel_withdrawDTO.requesterPhoneNum+" ";
		vm_route_travel_withdrawDTO.searchColumn += vm_route_travel_withdrawDTO.requesterOfficeUnitOrgNameEn+" ";
		vm_route_travel_withdrawDTO.searchColumn += vm_route_travel_withdrawDTO.requesterOfficeUnitOrgNameBn+" ";
		vm_route_travel_withdrawDTO.searchColumn += vm_route_travel_withdrawDTO.requesterOfficeUnitNameEn+" ";
		vm_route_travel_withdrawDTO.searchColumn += vm_route_travel_withdrawDTO.requesterOfficeUnitNameBn+" ";
		vm_route_travel_withdrawDTO.searchColumn += vm_route_travel_withdrawDTO.endDate+" ";
		

		
	}
	
	public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException
	{
		Vm_route_travel_withdrawDTO vm_route_travel_withdrawDTO = (Vm_route_travel_withdrawDTO)commonDTO;
		int index = 1;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(vm_route_travel_withdrawDTO);
		if(isInsert)
		{
			ps.setObject(index++,vm_route_travel_withdrawDTO.iD);
		}
		ps.setObject(index++,vm_route_travel_withdrawDTO.routeTravelId);
		ps.setObject(index++,vm_route_travel_withdrawDTO.endDate);
		ps.setObject(index++,vm_route_travel_withdrawDTO.status);
		ps.setObject(index++,vm_route_travel_withdrawDTO.requesterOrgId);
		ps.setObject(index++,vm_route_travel_withdrawDTO.requesterOfficeId);
		ps.setObject(index++,vm_route_travel_withdrawDTO.requesterOfficeUnitId);
		ps.setObject(index++,vm_route_travel_withdrawDTO.requesterEmpId);
		ps.setObject(index++,vm_route_travel_withdrawDTO.requesterPhoneNum);
		ps.setObject(index++,vm_route_travel_withdrawDTO.requesterNameEn);
		ps.setObject(index++,vm_route_travel_withdrawDTO.requesterNameBn);
		ps.setObject(index++,vm_route_travel_withdrawDTO.requesterOfficeNameEn);
		ps.setObject(index++,vm_route_travel_withdrawDTO.requesterOfficeNameBn);
		ps.setObject(index++,vm_route_travel_withdrawDTO.requesterOfficeUnitNameEn);
		ps.setObject(index++,vm_route_travel_withdrawDTO.requesterOfficeUnitNameBn);
		ps.setObject(index++,vm_route_travel_withdrawDTO.requesterOfficeUnitOrgNameEn);
		ps.setObject(index++,vm_route_travel_withdrawDTO.requesterOfficeUnitOrgNameBn);
		ps.setObject(index++,vm_route_travel_withdrawDTO.insertionDate);
		ps.setObject(index++,vm_route_travel_withdrawDTO.insertedBy);
		ps.setObject(index++,vm_route_travel_withdrawDTO.modifiedBy);
		ps.setObject(index++,vm_route_travel_withdrawDTO.searchColumn);
		if(isInsert)
		{
			ps.setObject(index++, 0);
			ps.setObject(index++, lastModificationTime);
		}
	}
	
	public void get(Vm_route_travel_withdrawDTO vm_route_travel_withdrawDTO, ResultSet rs) throws SQLException
	{
		vm_route_travel_withdrawDTO.id = rs.getLong("id");
		vm_route_travel_withdrawDTO.routeTravelId = rs.getLong("route_travel_id");
		vm_route_travel_withdrawDTO.endDate = rs.getLong("end_date");
		vm_route_travel_withdrawDTO.status = rs.getInt("status");
		vm_route_travel_withdrawDTO.requesterOrgId = rs.getLong("requester_org_id");
		vm_route_travel_withdrawDTO.requesterOfficeId = rs.getLong("requester_office_id");
		vm_route_travel_withdrawDTO.requesterOfficeUnitId = rs.getLong("requester_office_unit_id");
		vm_route_travel_withdrawDTO.requesterEmpId = rs.getLong("requester_emp_id");
		vm_route_travel_withdrawDTO.requesterPhoneNum = rs.getString("requester_phone_num");
		vm_route_travel_withdrawDTO.requesterNameEn = rs.getString("requester_name_en");
		vm_route_travel_withdrawDTO.requesterNameBn = rs.getString("requester_name_bn");
		vm_route_travel_withdrawDTO.requesterOfficeNameEn = rs.getString("requester_office_name_en");
		vm_route_travel_withdrawDTO.requesterOfficeNameBn = rs.getString("requester_office_name_bn");
		vm_route_travel_withdrawDTO.requesterOfficeUnitNameEn = rs.getString("requester_office_unit_name_en");
		vm_route_travel_withdrawDTO.requesterOfficeUnitNameBn = rs.getString("requester_office_unit_name_bn");
		vm_route_travel_withdrawDTO.requesterOfficeUnitOrgNameEn = rs.getString("requester_office_unit_org_name_en");
		vm_route_travel_withdrawDTO.requesterOfficeUnitOrgNameBn = rs.getString("requester_office_unit_org_name_bn");
		vm_route_travel_withdrawDTO.insertionDate = rs.getLong("insertion_date");
		vm_route_travel_withdrawDTO.insertedBy = rs.getString("inserted_by");
		vm_route_travel_withdrawDTO.modifiedBy = rs.getString("modified_by");
		vm_route_travel_withdrawDTO.searchColumn = rs.getString("search_column");
		vm_route_travel_withdrawDTO.isDeleted = rs.getInt("isDeleted");
		vm_route_travel_withdrawDTO.lastModificationTime = rs.getLong("lastModificationTime");
	}

	public Vm_route_travel_withdrawDTO build(ResultSet rs)
	{
		try
		{
			Vm_route_travel_withdrawDTO vm_route_travel_withdrawDTO = new Vm_route_travel_withdrawDTO();

			vm_route_travel_withdrawDTO.id = rs.getLong("id");
			vm_route_travel_withdrawDTO.routeTravelId = rs.getLong("route_travel_id");
			vm_route_travel_withdrawDTO.endDate = rs.getLong("end_date");
			vm_route_travel_withdrawDTO.status = rs.getInt("status");
			vm_route_travel_withdrawDTO.requesterOrgId = rs.getLong("requester_org_id");
			vm_route_travel_withdrawDTO.requesterOfficeId = rs.getLong("requester_office_id");
			vm_route_travel_withdrawDTO.requesterOfficeUnitId = rs.getLong("requester_office_unit_id");
			vm_route_travel_withdrawDTO.requesterEmpId = rs.getLong("requester_emp_id");
			vm_route_travel_withdrawDTO.requesterPhoneNum = rs.getString("requester_phone_num");
			vm_route_travel_withdrawDTO.requesterNameEn = rs.getString("requester_name_en");
			vm_route_travel_withdrawDTO.requesterNameBn = rs.getString("requester_name_bn");
			vm_route_travel_withdrawDTO.requesterOfficeNameEn = rs.getString("requester_office_name_en");
			vm_route_travel_withdrawDTO.requesterOfficeNameBn = rs.getString("requester_office_name_bn");
			vm_route_travel_withdrawDTO.requesterOfficeUnitNameEn = rs.getString("requester_office_unit_name_en");
			vm_route_travel_withdrawDTO.requesterOfficeUnitNameBn = rs.getString("requester_office_unit_name_bn");
			vm_route_travel_withdrawDTO.requesterOfficeUnitOrgNameEn = rs.getString("requester_office_unit_org_name_en");
			vm_route_travel_withdrawDTO.requesterOfficeUnitOrgNameBn = rs.getString("requester_office_unit_org_name_bn");
			vm_route_travel_withdrawDTO.insertionDate = rs.getLong("insertion_date");
			vm_route_travel_withdrawDTO.insertedBy = rs.getString("inserted_by");
			vm_route_travel_withdrawDTO.modifiedBy = rs.getString("modified_by");
			vm_route_travel_withdrawDTO.searchColumn = rs.getString("search_column");
			vm_route_travel_withdrawDTO.isDeleted = rs.getInt("isDeleted");
			vm_route_travel_withdrawDTO.lastModificationTime = rs.getLong("lastModificationTime");

			return vm_route_travel_withdrawDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}

	public String getSqlWithSearchCriteriaForNormalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
			UserDTO userDTO, String filter, boolean tableHasJobCat, List<Object> objectList)
    {
		boolean viewAll = false;
		
		if(p_searchCriteria != null && p_searchCriteria.get("ViewAll") != null)
		{
			System.out.println("ViewAll = " + p_searchCriteria.get("ViewAll"));
			viewAll = true;
		}
		else
		{
			System.out.println("ViewAll is null ");
		}
		
		String sql = "SELECT ";
		if(category == GETIDS)
		{
			sql += " " + tableName + ".ID as ID ";
		}
		else if(category == GETCOUNT)
		{
			sql += " count( " + tableName + ".ID) as countID ";
		}
		else if(category == GETDTOS)
		{
			sql += " distinct " + tableName + ".* ";
		}
		sql += "FROM " + tableName + " ";
		sql += joinSQL;
		

		
		String AnyfieldSql = "";
		String AllFieldSql = "";
		
		if(p_searchCriteria != null)
		{
						
			
			Enumeration names = p_searchCriteria.keys();
			String str, value;
			
			AnyfieldSql = "(";
			
			if(p_searchCriteria.get("AnyField")!= null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase(""))
			{
				//AnyfieldSql+= tableName + ".search_column like '%" + p_searchCriteria.get("AnyField").toString() + "%'";

				AnyfieldSql+= tableName + ".search_column like ? ";
				objectList.add("%" + p_searchCriteria.get("AnyField").toString() + "%");
			}
			AnyfieldSql += ")";
			System.out.println("AnyfieldSql = " + AnyfieldSql);
			
			AllFieldSql = "(";
			int i = 0;
			while(names.hasMoreElements())
			{				
				str = (String) names.nextElement();
				value = (String)p_searchCriteria.get(str);
		        System.out.println(str + ": " + value);
				if(value != null && !value.equalsIgnoreCase("") && (
						 str.equals("end_date_start")
						|| str.equals("end_date_end")
						|| str.equals("insertion_date_start")
						|| str.equals("insertion_date_end")
				)
						
				)
				{
					if(p_searchCriteria.get(str).equals("any"))
					{
						continue;
					}

					if( i > 0)
					{
						AllFieldSql+= " AND  ";
					}
					
					 if(str.equals("end_date_start"))
					{
						//AllFieldSql += "" + tableName + ".end_date >= " + p_searchCriteria.get(str);
						AllFieldSql += "" + tableName + ".end_date >= ? ";
						objectList.add(p_searchCriteria.get(str));
						i ++;
					}
					else if(str.equals("end_date_end"))
					{
						//AllFieldSql += "" + tableName + ".end_date <= " + p_searchCriteria.get(str);
						//AllFieldSql += "" + tableName + ".end_date = " + p_searchCriteria.get(str);
						AllFieldSql += "" + tableName + ".end_date = ? ";
						objectList.add(p_searchCriteria.get(str));
						i ++;
					}
					else if(str.equals("insertion_date_start"))
					{
						//AllFieldSql += "" + tableName + ".insertion_date >= " + p_searchCriteria.get(str);
						AllFieldSql += "" + tableName + ".insertion_date >= ? ";
						objectList.add(p_searchCriteria.get(str));
						i ++;
					}
					else if(str.equals("insertion_date_end"))
					{
						//AllFieldSql += "" + tableName + ".insertion_date <= " + p_searchCriteria.get(str);
						AllFieldSql += "" + tableName + ".insertion_date <= ? ";
						objectList.add(p_searchCriteria.get(str));
						i ++;
					}
					
					
				}
			}
			
			AllFieldSql += ")";
			System.out.println("AllFieldSql = " + AllFieldSql);
			
			
		}
		
		
		sql += " WHERE ";
		
		sql += " (" + tableName + ".isDeleted = 0 ";			
		sql += ")";
		
		
		if(!filter.equalsIgnoreCase(""))
		{
			sql += " and " + filter + " ";
		}
		
		if(!AnyfieldSql.equals("()") && !AnyfieldSql.equals(""))
		{
			sql += " AND " + AnyfieldSql;
			
		}
		if(!AllFieldSql.equals("()") && !AllFieldSql.equals(""))
		{			
			sql += " AND " + AllFieldSql;
		}
		
	
			
		sql += " order by " + tableName + ".lastModificationTime desc ";

		printSql(sql);
		
		if(limit >= 0)
		{
			sql += " limit " + limit;
		}
		if(offset >= 0)
		{
			sql += " offset " + offset;
		}
		
		System.out.println("-------------- sql = " + sql);
		
		return sql;
    }
	


	public Vm_route_travel_withdrawDTO getDTOByRequesterOrgID (long requester_org_id)
	{

		String sql = "SELECT * ";

		sql += " FROM " + tableName;

		sql += " WHERE requester_org_id= ? "; //+ requester_org_id;

		//sql += " AND status=" + 1;

		sql += " AND status = "+ CommonApprovalStatus.SATISFIED.getValue();

		sql += " AND isDeleted = 0 ";// + 0;

		sql += "  ORDER BY ID DESC LIMIT 1 " ;

		printSql(sql);

		Vm_route_travel_withdrawDTO vm_route_travel_withdrawDTO = null;

		vm_route_travel_withdrawDTO  = ConnectionAndStatementUtil.getT(sql, Arrays.asList(requester_org_id),this::build);

		//vm_route_travel_withdrawDTO  = ConnectionAndStatementUtil.getT(sql, Arrays.asList(requester_org_id,CommonApprovalStatus.SATISFIED.getValue()),this::build);

		return vm_route_travel_withdrawDTO;
	}

	public Vm_route_travel_withdrawDTO getDTOForRouteEnrollmentByRequesterOrgID (long requester_org_id)
	{

		String sql = "SELECT * ";

		sql += " FROM " + tableName;

		sql += " WHERE requester_org_id = ? ";// + requester_org_id;

		//sql += " AND status=" + 1;

		sql += " AND status = " + CommonApprovalStatus.PENDING.getValue();

		sql += " AND isDeleted = " + 0;

		sql += "  ORDER BY ID DESC LIMIT 1" ;

		printSql(sql);

		Vm_route_travel_withdrawDTO vm_route_travel_withdrawDTO = null;

		vm_route_travel_withdrawDTO  = ConnectionAndStatementUtil.getT(sql, Arrays.asList(requester_org_id),this::build);

		return vm_route_travel_withdrawDTO;

	}

	public Vm_route_travel_withdrawDTO getDTOByRequesterEmpID (long requester_emp_record_id)
	{

		String sql = "SELECT * ";

		sql += " FROM " + tableName;

		sql += " WHERE requester_emp_id = ? ";// + requester_emp_record_id;

		//sql += " AND status=" + 1;

		sql += " AND status = " + CommonApprovalStatus.SATISFIED.getValue();

		sql += " AND isDeleted = " + 0;

		sql += "  ORDER BY ID DESC LIMIT 1" ;

		printSql(sql);

		Vm_route_travel_withdrawDTO vm_route_travel_withdrawDTO = null;

		vm_route_travel_withdrawDTO  = ConnectionAndStatementUtil.getT(sql, Arrays.asList(requester_emp_record_id),this::build);

		return vm_route_travel_withdrawDTO;

	}
				
}
	