package am_house_applicants_list_report;

import language.LC;
import language.LM;
import login.LoginDTO;
import pbReport.ReportService;
import permission.MenuConstants;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.ReportRequestHandler;
import util.UtilCharacter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/Am_house_applicants_list_report_Servlet")
public class Am_house_applicants_list_report_Servlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    String[][] Criteria =
            {
                    {"criteria", "tr", "am_house_old_new_cat", "=", "", "int", "", "", "any", "amHouseOldNewCat", LC.AM_HOUSE_ALLOCATION_HISTORY_REPORT_WHERE_AMHOUSEOLDNEWCAT + ""},
                    {"criteria", "tr", "am_house_location_cat", "=", "AND", "int", "", "", "any", "amHouseLocationCat", LC.AM_HOUSE_ALLOCATION_HISTORY_REPORT_WHERE_AMHOUSELOCATIONCAT + ""},
                    {"criteria", "tr", "am_house_class_cat", "=", "AND", "int", "", "", "any", "amHouseClassCat", LC.AM_HOUSE_ALLOCATION_HISTORY_REPORT_WHERE_AMHOUSECLASSCAT + ""},
                    {"criteria", "tr", "am_house_id", "=", "AND", "String", "", "", "any", "houseNumber", LC.AM_HOUSE_ALLOCATION_HISTORY_REPORT_WHERE_HOUSEID + ""},
                    {"criteria", "tr", "status", "=", "AND", "String", "", "", "any", "houseStatus", LC.AM_HOUSE_ALLOCATION_HISTORY_REPORT_WHERE_HOUSEID + ""},
                    {"criteria", "tr", "insertion_date", ">=", "AND", "long", "", "", Long.MIN_VALUE + "", "startDate", LC.HM_START_DATE + ""},
                    {"criteria", "tr", "insertion_date", "<=", "AND", "long", "", "", Long.MAX_VALUE + "", "endDate", LC.HM_START_DATE + ""},
                    {"criteria","tr","requester_emp_id","=","AND","String","","","any","requesterEmpId", LC.AM_HOUSE_ALLOCATION_EMPLOYEE_WISE_REPORT_WHERE_REQUESTEREMPID + ""},
                    {"criteria", "tr", "status", "!=", "AND", "String", "", "", "3", "isDeleted", LC.AM_HOUSE_ALLOCATION_HISTORY_REPORT_WHERE_ISDELETED + ""},
                    {"criteria", "tr", "status", "!=", "AND", "String", "", "", "15", "isDeleted", LC.AM_HOUSE_ALLOCATION_HISTORY_REPORT_WHERE_ISDELETED + ""},
                    {"criteria", "tr", "isDeleted", "=", "AND", "String", "", "", "0", "isDeleted", LC.AM_HOUSE_ALLOCATION_HISTORY_REPORT_WHERE_ISDELETED + ""}
            };

    String[][] Display =
            {
                    {"display", "tr", "ID", "am_house_allocation_emp_name", ""},
                    {"display", "tr", "ID", "am_house_allocation_emp_org", ""},
                    {"display", "tr", "ID", "am_house_allocation_emp_office", ""},
                    {"display", "tr", "employee_pay_scale_id", "text", ""},
                    {"display", "tr", "am_house_old_new_cat", "cat", ""},
                    {"display", "tr", "am_house_location_cat", "cat", ""},
                    {"display", "tr", "am_house_class_cat", "cat", ""},
                    {"display", "tr", "am_house_class_sub_cat", "am_house_sub_class", ""},
                    {"display", "ah", "house_number", "text", ""},
                    {"display", "tr", "insertion_date", "date", ""},
                    {"display", "tr", "status", "am_house_status_check", ""}
            };

    String GroupBy = "";
    String OrderBY = "";

    ReportRequestHandler reportRequestHandler;

    public Am_house_applicants_list_report_Servlet() {

    }

    private final ReportService reportService = new ReportService();

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        String language = "english";
        if (userDTO != null && userDTO.languageID == SessionConstants.BANGLA) {
            language = "bangla";
        }

        String actionType = request.getParameter("actionType");

        System.out.println("In ssservlet doget, actiontype = " + actionType);

        String sql = "am_house_allocation_request tr INNER JOIN am_house ah ON tr.am_house_id = ah.ID";

        Display[0][4] = UtilCharacter.getDataByLanguage(language, "কর্মকর্তার নাম", "Employee Name");
        Display[1][4] = UtilCharacter.getDataByLanguage(language, "পদবি", "Designation");
        Display[2][4] = UtilCharacter.getDataByLanguage(language, "দপ্তর", "Office");
        Display[3][4] = UtilCharacter.getDataByLanguage(language, "গ্র্যাড", "Grade");
        Display[4][4] = LM.getText(LC.AM_HOUSE_ALLOCATION_HISTORY_REPORT_SELECT_AMHOUSEOLDNEWCAT, loginDTO);
        Display[5][4] = LM.getText(LC.AM_HOUSE_ALLOCATION_HISTORY_REPORT_SELECT_AMHOUSELOCATIONCAT, loginDTO);
        Display[6][4] = LM.getText(LC.AM_HOUSE_ALLOCATION_HISTORY_REPORT_SELECT_AMHOUSECLASSCAT, loginDTO);
        Display[7][4] = UtilCharacter.getDataByLanguage(language, "বাসার উপশ্রেণি", "House Sub Class");
        Display[8][4] = LM.getText(LC.AM_HOUSE_ALLOCATION_HISTORY_REPORT_SELECT_HOUSEID, loginDTO);
        Display[9][4] = UtilCharacter.getDataByLanguage(language, "আবেদনের তারিখ", "Application Date");
        Display[10][4] = LM.getText(LC.AM_HOUSE_ALLOCATION_HISTORY_REPORT_SELECT_STATUS, loginDTO);


        String reportName = UtilCharacter.getDataByLanguage(language, "বাসা বরাদ্দের জন্য আবেদনের রিপোর্ট", "House Applications Report");

        reportRequestHandler = new ReportRequestHandler(null, Criteria, Display, GroupBy, OrderBY, sql, reportService);

        reportRequestHandler.handleReportGet(request, response, userDTO, "am_house_applicants_list_report",
                MenuConstants.AM_HOUSE_ALLOCATION_HISTORY_REPORT_DETAILS, language, reportName, "am_house_applicants_list_report");
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }
}
