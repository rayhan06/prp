package entry_page;

import java.io.IOException;
import java.io.*;
import java.text.SimpleDateFormat;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import org.apache.log4j.Logger;

import login.LoginDTO;
import permission.MenuConstants;
import role.PermissionRepository;
import role_actions.ActionDescriptionDAO;
import role_actions.ActionDescriptionDTO;
import sessionmanager.SessionConstants;

import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;
import util.CommonRequestHandler;
import util.RecordNavigationManager4;

import java.util.*;
import javax.servlet.http.*;
import java.util.UUID;

import entry_page.Constants;
import family.FamilyDTO;
import geolocation.GeoLocationDAO2;
import language.LC;
import language.LM;

import java.util.StringTokenizer;

import com.google.gson.Gson;

import appointment.AppointmentDTO;
import pb.*;
import user.*;
import pbReport.*;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;




/**
 * Servlet implementation class Entry_pageServlet
 */
@WebServlet("/Entry_pageServlet")
@MultipartConfig
public class Entry_pageServlet extends HttpServlet
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Entry_pageServlet.class);

    String tableName = "entry_page";

	Entry_pageDAO entry_pageDAO;
	CommonRequestHandler commonRequestHandler;
    private final Gson gson = new Gson();
    UserDAO userDAO = new UserDAO();

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Entry_pageServlet()
	{
        super();
    	try
    	{
			entry_pageDAO = new Entry_pageDAO(tableName);
			commonRequestHandler = new CommonRequestHandler(entry_pageDAO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		System.out.println("In doget request = " + request);

//		boolean isPermanentTable = true;
//		if(request.getParameter("isPermanentTable") != null)
//		{
//			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
//		}
//		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);

		try
		{
			String actionType = request.getParameter("actionType");
			if(actionType.equals("getAddPage"))
			{
				LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
				UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ENTRY_PAGE_ADD))
				{
					commonRequestHandler.getAddPage(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}

			else if(actionType.equals("recruitmentRegistration"))
			{
//				if(PermissionRepository.getInstance().checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ENTRY_PAGE_ADD))
//				{
					request.getRequestDispatcher("entry_page/recruitmentRegistration.jsp").forward(request, response);
//				}
//				else
//				{
//					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
//				}
			}

			else if(actionType.equals("pinCodeRetrieve"))
			{
				logger.debug("actionType found in loginservlet: pinCodeRetrieve" );
				request.getRequestDispatcher("entry_page/pinCodeRetrieve.jsp").forward(request, response);
			}

			else if(actionType.equals("getURL"))
			{
				String URL = request.getParameter("URL");
				System.out.println("URL = " + URL);
				response.sendRedirect(URL);
			}

			else
			{
				request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}





	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		// TODO Auto-generated method stub
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		System.out.println("doPost");
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);

		try
		{
			String actionType = request.getParameter("actionType");
			System.out.println("actionType = " + actionType);
			if(actionType.equals("add"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ENTRY_PAGE_ADD))
				{
					System.out.println("going to  addEntry_page ");
					addEntry_page(request, response, true, userDTO, true);
				}
				else
				{
					System.out.println("Not going to  addEntry_page ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}

		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}

	private void addEntry_page(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, boolean isPermanentTable) throws IOException
	{
		// TODO Auto-generated method stub
		try
		{
			request.setAttribute("failureMessage", "");
			System.out.println("%%%% addEntry_page");

			ActionDescriptionDAO actionDescriptionDAO = new ActionDescriptionDAO();
			ActionDescriptionDTO actionDescriptionDTO = actionDescriptionDAO.getDTOByID(Long.parseLong(request.getParameter("actionDescriptionType")));

			String userName = Utils.getDigitEnglishFromBangla(request.getParameter("employeeUserName"));
			if(!Utils.isValidUserName(userName))
			{
				return;
			}
			if(userName.equalsIgnoreCase(""))
			{
				System.out.println("Invalid name, returning");
				userName= FamilyDTO.defaultUser;
			}
			UserDTO employeeUserDTO = userDAO.getUserDTOByUsernameFromOnlyUsers(userName);
			if(employeeUserDTO == null)
			{
				System.out.println("Even superadmin did not work");
			}

			int whoIsThePatientCat = FamilyDTO.UNSELECTED;
			if(request.getParameter("whoIsThePatientCat")!= null && !request.getParameter("whoIsThePatientCat").equalsIgnoreCase(""))
			{
				whoIsThePatientCat = Integer.parseInt(request.getParameter("whoIsThePatientCat"));
			}



			String params = "&userName=" + userName
			+ "&whoIsThePatientCat=" + whoIsThePatientCat
			+ "&phoneNumber=" + request.getParameter("phoneNumber")
			+ "&patientName=" + request.getParameter("patientName");

			if(employeeUserDTO != null)
			{
				UserDTO oldEmployeeUserDTO = employeeUserDTO;
				employeeUserDTO = UserRepository.getInstance().getUserDtoByEmployeeUserName(userName);
				
				if(employeeUserDTO != null)
				{
					params += "&organogramId=" + employeeUserDTO.organogramID;
				}
				employeeUserDTO = oldEmployeeUserDTO;
				
			}
			else
			{
				params += "&organogramId=" + FamilyDTO.UNSELECTED;
			}

			if(employeeUserDTO != null ||
					(!actionDescriptionDTO.url.contains("AppointmentServlet") && !actionDescriptionDTO.url.contains("Prescription_detailsServlet")))
			{
				response.sendRedirect(actionDescriptionDTO.url + params);
			}
			else
			{
				response.sendRedirect("Entry_pageServlet?actionType=getAddPage");
			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
			response.sendRedirect("Entry_pageServlet?actionType=getAddPage");
		}
	}








	private void getEntry_page(HttpServletRequest request, HttpServletResponse response, long id) throws ServletException, IOException
	{
		System.out.println("in getEntry_page");
		Entry_pageDTO entry_pageDTO = null;
		try
		{
			entry_pageDTO = (Entry_pageDTO)entry_pageDAO.getDTOByID(id);
			request.setAttribute("ID", entry_pageDTO.iD);
			request.setAttribute("entry_pageDTO",entry_pageDTO);
			request.setAttribute("entry_pageDAO",entry_pageDAO);

			String URL= "";

			String inPlaceEdit = request.getParameter("inplaceedit");
			String inPlaceSubmit = request.getParameter("inplacesubmit");
			String getBodyOnly = request.getParameter("getBodyOnly");

			if(inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase(""))
			{
				URL = "entry_page/entry_pageInPlaceEdit.jsp";
				request.setAttribute("inplaceedit","");
			}
			else if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
			{
				URL = "entry_page/entry_pageSearchRow.jsp";
				request.setAttribute("inplacesubmit","");
			}
			else
			{
				if(getBodyOnly  != null && !getBodyOnly.equalsIgnoreCase(""))
				{
					URL = "entry_page/entry_pageEditBody.jsp?actionType=edit";
				}
				else
				{
					URL = "entry_page/entry_pageEdit.jsp?actionType=edit";
				}
			}

			RequestDispatcher rd = request.getRequestDispatcher(URL);
			rd.forward(request, response);
		}
		catch (NumberFormatException e)
		{
			e.printStackTrace();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}



}

