package entry_page;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class Entry_pageRepository implements Repository {
	Entry_pageDAO entry_pageDAO = null;
	
	public void setDAO(Entry_pageDAO entry_pageDAO)
	{
		this.entry_pageDAO = entry_pageDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Entry_pageRepository.class);
	Map<Long, Entry_pageDTO>mapOfEntry_pageDTOToiD;
	Map<Long, Set<Entry_pageDTO> >mapOfEntry_pageDTOToorganogramId;
	Map<Long, Set<Entry_pageDTO> >mapOfEntry_pageDTOToactionDescriptionType;
	Map<Long, Set<Entry_pageDTO> >mapOfEntry_pageDTOTolastModificationTime;


	static Entry_pageRepository instance = null;  
	private Entry_pageRepository(){
		mapOfEntry_pageDTOToiD = new ConcurrentHashMap<>();
		mapOfEntry_pageDTOToorganogramId = new ConcurrentHashMap<>();
		mapOfEntry_pageDTOToactionDescriptionType = new ConcurrentHashMap<>();
		mapOfEntry_pageDTOTolastModificationTime = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Entry_pageRepository getInstance(){
		if (instance == null){
			instance = new Entry_pageRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(entry_pageDAO == null)
		{
			return;
		}
		try {
			List<Entry_pageDTO> entry_pageDTOs = entry_pageDAO.getAllEntry_page(reloadAll);
			for(Entry_pageDTO entry_pageDTO : entry_pageDTOs) {
				Entry_pageDTO oldEntry_pageDTO = getEntry_pageDTOByID(entry_pageDTO.iD);
				if( oldEntry_pageDTO != null ) {
					mapOfEntry_pageDTOToiD.remove(oldEntry_pageDTO.iD);
				

					
					if(mapOfEntry_pageDTOToactionDescriptionType.containsKey(oldEntry_pageDTO.actionDescriptionType)) {
						mapOfEntry_pageDTOToactionDescriptionType.get(oldEntry_pageDTO.actionDescriptionType).remove(oldEntry_pageDTO);
					}
					if(mapOfEntry_pageDTOToactionDescriptionType.get(oldEntry_pageDTO.actionDescriptionType).isEmpty()) {
						mapOfEntry_pageDTOToactionDescriptionType.remove(oldEntry_pageDTO.actionDescriptionType);
					}
					
					if(mapOfEntry_pageDTOTolastModificationTime.containsKey(oldEntry_pageDTO.lastModificationTime)) {
						mapOfEntry_pageDTOTolastModificationTime.get(oldEntry_pageDTO.lastModificationTime).remove(oldEntry_pageDTO);
					}
					if(mapOfEntry_pageDTOTolastModificationTime.get(oldEntry_pageDTO.lastModificationTime).isEmpty()) {
						mapOfEntry_pageDTOTolastModificationTime.remove(oldEntry_pageDTO.lastModificationTime);
					}
					
					
				}
				if(entry_pageDTO.isDeleted == 0) 
				{
					
					mapOfEntry_pageDTOToiD.put(entry_pageDTO.iD, entry_pageDTO);
				

					
					if( ! mapOfEntry_pageDTOToactionDescriptionType.containsKey(entry_pageDTO.actionDescriptionType)) {
						mapOfEntry_pageDTOToactionDescriptionType.put(entry_pageDTO.actionDescriptionType, new HashSet<>());
					}
					mapOfEntry_pageDTOToactionDescriptionType.get(entry_pageDTO.actionDescriptionType).add(entry_pageDTO);
					
					if( ! mapOfEntry_pageDTOTolastModificationTime.containsKey(entry_pageDTO.lastModificationTime)) {
						mapOfEntry_pageDTOTolastModificationTime.put(entry_pageDTO.lastModificationTime, new HashSet<>());
					}
					mapOfEntry_pageDTOTolastModificationTime.get(entry_pageDTO.lastModificationTime).add(entry_pageDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Entry_pageDTO> getEntry_pageList() {
		List <Entry_pageDTO> entry_pages = new ArrayList<Entry_pageDTO>(this.mapOfEntry_pageDTOToiD.values());
		return entry_pages;
	}
	
	
	public Entry_pageDTO getEntry_pageDTOByID( long ID){
		return mapOfEntry_pageDTOToiD.get(ID);
	}
	
	
	public List<Entry_pageDTO> getEntry_pageDTOByorganogram_id(long organogram_id) {
		return new ArrayList<>( mapOfEntry_pageDTOToorganogramId.getOrDefault(organogram_id,new HashSet<>()));
	}
	
	
	public List<Entry_pageDTO> getEntry_pageDTOByaction_description_type(long action_description_type) {
		return new ArrayList<>( mapOfEntry_pageDTOToactionDescriptionType.getOrDefault(action_description_type,new HashSet<>()));
	}
	
	
	public List<Entry_pageDTO> getEntry_pageDTOBylastModificationTime(long lastModificationTime) {
		return new ArrayList<>( mapOfEntry_pageDTOTolastModificationTime.getOrDefault(lastModificationTime,new HashSet<>()));
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "entry_page";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


