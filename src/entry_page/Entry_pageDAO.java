package entry_page;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;
import sessionmanager.SessionConstants;


import org.apache.log4j.Logger;

import dbm.*;

import repository.RepositoryManager;

import util.*;
import pb.*;
import user.UserDTO;

public class Entry_pageDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());
	
	
	public Entry_pageDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";
		joinSQL += " join action_description on " + tableName + ".action_description_type = action_description.ID ";

		commonMaps = new Entry_pageMAPS(tableName);
	}
	
	public Entry_pageDAO()
	{
		this("entry_page");		
	}
	
	
	
	public long add(CommonDTO commonDTO) throws Exception
	{
		
		Entry_pageDTO entry_pageDTO = (Entry_pageDTO)commonDTO;
		
		Connection connection = null;
		PreparedStatement ps = null;

		long lastModificationTime = System.currentTimeMillis();	

		try{
			connection = DBMW.getInstance().getConnection();
			
			if(connection == null)
			{
				System.out.println("nullconn");
			}

			entry_pageDTO.iD = DBMW.getInstance().getNextSequenceId(tableName);

			String sql = "INSERT INTO " + tableName;
			
			sql += " (";
			sql += "ID";
			sql += ", ";
			sql += "organogram_id";
			sql += ", ";
			sql += "action_description_type";
			sql += ", ";
			sql += "isDeleted";
			sql += ", ";
			sql += "lastModificationTime";
		
			sql += ")";
			
			
            sql += " VALUES(";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			
			sql += ")";
				

			ps = connection.prepareStatement(sql);
			
			
			

			int index = 1;

			ps.setObject(index++,entry_pageDTO.iD);
			ps.setObject(index++,entry_pageDTO.userName);
			ps.setObject(index++,entry_pageDTO.actionDescriptionType);
			ps.setObject(index++,entry_pageDTO.isDeleted);
			ps.setObject(index++, lastModificationTime);
			
			System.out.println(ps);
			ps.execute();
			
			
			recordUpdateTime(connection, lastModificationTime);

		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if (ps != null) {
					ps.close();
				}
			} catch(Exception e){}
			try{
				if(connection != null)
				{
					DBMW.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		return entry_pageDTO.iD;		
	}
		
	

	//need another getter for repository
	public CommonDTO getDTOByID (long ID) throws Exception
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Entry_pageDTO entry_pageDTO = null;
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
			
            sql += " WHERE ID=" + ID;
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			if(rs.next()){
				entry_pageDTO = new Entry_pageDTO();

				entry_pageDTO.iD = rs.getLong("ID");
				entry_pageDTO.userName = rs.getString("userName");
				entry_pageDTO.actionDescriptionType = rs.getLong("action_description_type");
				entry_pageDTO.isDeleted = rs.getInt("isDeleted");
				entry_pageDTO.lastModificationTime = rs.getLong("lastModificationTime");

			}			
			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return entry_pageDTO;
	}
	
	public long update(CommonDTO commonDTO) throws Exception
	{		
		Entry_pageDTO entry_pageDTO = (Entry_pageDTO)commonDTO;
		
		Connection connection = null;
		PreparedStatement ps = null;

		long lastModificationTime = System.currentTimeMillis();	
		try{
			connection = DBMW.getInstance().getConnection();

			String sql = "UPDATE " + tableName;
			
			sql += " SET ";
			sql += "organogram_id=?";
			sql += ", ";
			sql += "action_description_type=?";
			sql += ", lastModificationTime = "	+ lastModificationTime + "";
            sql += " WHERE ID = " + entry_pageDTO.iD;
				

			ps = connection.prepareStatement(sql);
			
			

			int index = 1;
			
			ps.setObject(index++,entry_pageDTO.userName);
			ps.setObject(index++,entry_pageDTO.actionDescriptionType);
			System.out.println(ps);
			ps.executeUpdate();
			


			
			recordUpdateTime(connection,lastModificationTime);

						
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if (ps != null) {
					ps.close();
				}
			} catch(Exception e){}
			try{
				if(connection != null)
				{
					DBMW.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		return entry_pageDTO.iD;
	}
	
	
	public List<Entry_pageDTO> getDTOs(Collection recordIDs){
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Entry_pageDTO entry_pageDTO = null;
		List<Entry_pageDTO> entry_pageDTOList = new ArrayList<>();
		if(recordIDs.isEmpty()){
			return entry_pageDTOList;
		}
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
            
            sql += " WHERE ID IN ( ";

			for(int i = 0;i<recordIDs.size();i++){
				if(i!=0){
					sql+=",";
				}
				sql+=((ArrayList)recordIDs).get(i);
			}
			sql+=")  order by lastModificationTime desc";
			
			printSql(sql);
			
			logger.debug("sql " + sql);
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				entry_pageDTO = new Entry_pageDTO();
				entry_pageDTO.iD = rs.getLong("ID");
				entry_pageDTO.userName = rs.getString("userName");
				entry_pageDTO.actionDescriptionType = rs.getLong("action_description_type");
				entry_pageDTO.isDeleted = rs.getInt("isDeleted");
				entry_pageDTO.lastModificationTime = rs.getLong("lastModificationTime");
				System.out.println("got this DTO: " + entry_pageDTO);
				
				entry_pageDTOList.add(entry_pageDTO);

			}			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return entry_pageDTOList;
	
	}
	
	

	
	
	
	//add repository
	public List<Entry_pageDTO> getAllEntry_page (boolean isFirstReload)
    {
		List<Entry_pageDTO> entry_pageDTOList = new ArrayList<>();

		String sql = "SELECT * FROM entry_page";
		sql += " WHERE ";
	

		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by entry_page.lastModificationTime desc";
		printSql(sql);
		
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;

		
		try{
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();
			rs = stmt.executeQuery(sql);
			

			while(rs.next()){
				Entry_pageDTO entry_pageDTO = new Entry_pageDTO();
				entry_pageDTO.iD = rs.getLong("ID");
				entry_pageDTO.userName = rs.getString("userName");
				entry_pageDTO.actionDescriptionType = rs.getLong("action_description_type");
				entry_pageDTO.isDeleted = rs.getInt("isDeleted");
				entry_pageDTO.lastModificationTime = rs.getLong("lastModificationTime");
				
				entry_pageDTOList.add(entry_pageDTO);
			}			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}

		return entry_pageDTOList;
    }

	
	public List<Entry_pageDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO)
	{
		return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
	}

	
	public List<Entry_pageDTO> getDTOs(Hashtable  p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
			String filter, boolean tableHasJobCat)
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		List<Entry_pageDTO> entry_pageDTOList = new ArrayList<>();

		try{
			
			String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat);
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				Entry_pageDTO entry_pageDTO = new Entry_pageDTO();
				entry_pageDTO.iD = rs.getLong("ID");
				entry_pageDTO.userName = rs.getString("userName");
				entry_pageDTO.actionDescriptionType = rs.getLong("action_description_type");
				entry_pageDTO.isDeleted = rs.getInt("isDeleted");
				entry_pageDTO.lastModificationTime = rs.getLong("lastModificationTime");
				
				entry_pageDTOList.add(entry_pageDTO);
			}					
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return entry_pageDTOList;
	
	}
				
}
	