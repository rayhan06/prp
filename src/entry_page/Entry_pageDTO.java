package entry_page;
import java.util.*; 
import util.*; 


public class Entry_pageDTO extends CommonDTO
{

	public long actionDescriptionType = 0;
	public int whoIsThePatientCat = -1;
	public String phoneNumber = "";
	public String patientName = "";
	public String userName = "";
	
	
    @Override
	public String toString() {
            return "$Entry_pageDTO[" +
            " iD = " + iD +
            " userName = " + userName +
            " actionDescriptionType = " + actionDescriptionType +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}