package entry_page;
import java.util.*; 
import util.*;


public class Entry_pageMAPS extends CommonMaps
{	
	public Entry_pageMAPS(String tableName)
	{
		
		java_allfield_type_map.put("organogram_id".toLowerCase(), "Long");
		java_allfield_type_map.put("action_description_type".toLowerCase(), "Long");


		java_anyfield_search_map.put(tableName + ".organogram_id".toLowerCase(), "Long");
		java_anyfield_search_map.put("action_description.name_en".toLowerCase(), "String");
		java_anyfield_search_map.put("action_description.name_bn".toLowerCase(), "String");

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("organogramId".toLowerCase(), "organogramId".toLowerCase());
		java_DTO_map.put("actionDescriptionType".toLowerCase(), "actionDescriptionType".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("organogram_id".toLowerCase(), "organogramId".toLowerCase());
		java_SQL_map.put("action_description_type".toLowerCase(), "actionDescriptionType".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Organogram Id".toLowerCase(), "organogramId".toLowerCase());
		java_Text_map.put("Action Description".toLowerCase(), "actionDescriptionType".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}