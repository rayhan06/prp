package entry_page;

import config.GlobalConfigConstants;
import config.GlobalConfigurationRepository;
import forgetPassword.VerificationConstants;
import login.LoginDTO;
import login.LoginService;
import login.RememberMeOptionDAO;
import oisf.OisfDAO;
import oisf.lib.LibConstants;
import oisf.lib.sso.SSOResponseDTO;
import pb.Utils;

import org.apache.log4j.Logger;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.RootstockUtils;
import util.ServletConstant;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.UUID;

/**
 * @author Kayesh Parvez
 *
 */

@WebServlet("/RecruitmentLoginServlet")
public class RecruitmentLoginServlet extends HttpServlet {

	Logger logger = Logger.getLogger(RecruitmentLoginServlet.class);
	private static final long serialVersionUID = 1L;

	public RecruitmentLoginServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);

		boolean isDefaultLoginEnabled = (Integer.parseInt(GlobalConfigurationRepository.getGlobalConfigDTOByID(GlobalConfigConstants.ENABLE_DEFAULT_LOGIN).value) == 1);

		if(!isDefaultLoginEnabled){

			if(loginDTO==null){
				String tempNonce = RootstockUtils.generateRandomHexToken(10);

				request.setAttribute("nonce", tempNonce);

				request.getSession().setAttribute("nonce", tempNonce);
				//logger..("nonce :", tempNonce);
				logger.debug("nonce : "+tempNonce);
				request.getRequestDispatcher("/entry_page/recruitmentLogin.jsp").forward(request, response);
			}else{
				response.sendRedirect(" ");
			}

		}else{

			// here logindto will never be  null

			long defaultUserID = Long.parseLong(GlobalConfigurationRepository.getGlobalConfigDTOByID(GlobalConfigConstants.DEFAULT_USER_ID).value);

			if(defaultUserID == loginDTO.userID){

				String tempNonce = RootstockUtils.generateRandomHexToken(10);

				request.setAttribute("nonce", tempNonce);

				request.getSession().setAttribute("nonce", tempNonce);

				logger.debug("nonce 2 : "+tempNonce);

				request.getRequestDispatcher("/entry_page/recruitmentLogin.jsp").forward(request, response);
			}else{
				response.sendRedirect(request.getContextPath());
			}

		}

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// insert login credentials

		LoginDTO loginDTOTemp = new LoginDTO();
		String username = Utils.getDigitEnglishFromBangla(request.getParameter("username"));
		if(!Utils.isValidUserName(username))
		{
			return;
		}
		String password = request.getParameter("password");
		String tempOisf = "";

		String loginIP = request.getRemoteAddr();
		loginDTOTemp.loginSourceIP = loginIP;
		logger.debug("userDTO " + loginDTOTemp);
		LoginService service = new LoginService();
		String loginUrl = "entry_page/recruitmentLogin.jsp";
		String homeUrl = "PublicServlet?actionType=public_search";
		String otpUrl = "home/otpVerifier.jsp";

		try
		{

			request.getSession(true).setAttribute(VerificationConstants.LOGIN_IP, loginIP);
			request.getSession(true).setAttribute(VerificationConstants.CACHE_LOGIN_DTO, loginDTOTemp);
			request.getSession(true).setAttribute(VerificationConstants.CACHE_LOGIN_USERNAME, username);
			request.getSession(true).setAttribute(VerificationConstants.LOGIN_URL, loginUrl);

			if(!service.capchaMatched(request))
			{
				if(Integer.parseInt(GlobalConfigurationRepository.getGlobalConfigDTOByID(GlobalConfigConstants.OISF_LOGIN_METHOD).value) == 1)
				{
					//1 for web redirect
					OisfDAO tempDao = new OisfDAO();

					String tempRedirect = tempDao.processLoginPage(request, "");
					response.sendRedirect(tempRedirect);
					return;

				}
				else
				{
					//response.sendRedirect(loginUrl);
					request.getRequestDispatcher(loginUrl).forward(request, response);

					return;
				}
			}
			if(service.hasRestriction(loginDTOTemp, username, request))
			{
				if(Integer.parseInt(GlobalConfigurationRepository.getGlobalConfigDTOByID(GlobalConfigConstants.OISF_LOGIN_METHOD).value) == 1)
				{
					//1 for web redirect
					OisfDAO tempDao = new OisfDAO();

					String tempRedirect = tempDao.processLoginPage(request, "");
					response.sendRedirect(tempRedirect);
					return;

				}
				else
				{
					// response.sendRedirect(loginUrl);
					request.getRequestDispatcher(loginUrl).forward(request, response);
					return;
				}
			}


			if(Integer.parseInt(GlobalConfigurationRepository.getGlobalConfigDTOByID(GlobalConfigConstants.OISF_LOGIN_METHOD).value) == 1)
			{
				//send to oisf , web redirect

				LoginDTO loginDTO = service.validateLogin(username, password, loginDTOTemp);
				if(loginDTO == null)
				{
//						service.processInvalidLogin(loginIP, username, request);
//						response.sendRedirect(loginUrl);
//						return;



					OisfDAO tempDao = new OisfDAO();

					String tempRedirect = tempDao.processLoginPage(request, "");
					response.sendRedirect(tempRedirect);
					return;
				}
				else
				{
					UserDTO loginUserDTO = UserRepository.getUserDTOByUserID(loginDTO);
					if(service.needOTPCheck(loginUserDTO, request))
					{
						request.getSession().removeAttribute(SessionConstants.USER_LOGIN);
						//request.getSession().setAttribute(SessionConstants.USER_LOGIN, loginDTO);
						request.getSession().setAttribute(VerificationConstants.CACHE_LOGIN_USERNAME, username);
						//response.sendRedirect(otpUrl);
						request.getRequestDispatcher(otpUrl).forward(request, response);
					}
					else
					{	//valid login
						if(request.getParameter("stay_logged_in")!=null){
							Cookie cookie = new Cookie(ServletConstant.REMEMBER_ME_COOKIE_NAME, UUID.randomUUID().toString());
							cookie.setMaxAge(60 * 60 * 24 * 365 * 10);
							RememberMeOptionDAO.getInstance().insertRememberMeOption(loginDTO.userID, cookie.getValue(),0);
							response.addCookie(cookie);
						}

						service.processValidLogin(loginDTO, username, request);
						String lastVisitedUrl = service.getLastRequestedUrl(request);
						if(lastVisitedUrl != null && !lastVisitedUrl.endsWith("LogoutServlet")) response.sendRedirect(lastVisitedUrl);
							//else response.sendRedirect(homeUrl);
						else
						{
							goToHomePage(request, response, homeUrl);
//							RequestDispatcher rd = request.getRequestDispatcher(homeUrl);
//							rd.forward(request, response);
						}
					}
				}



			}
			else if(Integer.parseInt(GlobalConfigurationRepository.getGlobalConfigDTOByID(GlobalConfigConstants.OISF_LOGIN_METHOD).value) == 2)
			{
				//send to oisf , api redirect


				LoginDTO loginDTO2 = service.validateLogin(username, password, loginDTOTemp);

				if(loginDTO2 == null)
				{

					OisfDAO tempDao = new OisfDAO();

					SSOResponseDTO tempOisfDto  = tempDao.processLoginAPI(request, username, password);

					// LoginDTO loginDTO = service.validateLogin(username, password, loginDTOTemp);
					if(tempOisfDto == null)
					{
						String tempIp = (String)request.getSession(true).getAttribute(VerificationConstants.LOGIN_IP);
						// 	request.getSession(true).setAttribute(VerificationConstants.CACHE_LOGIN_DTO, loginDTOTemp);
						String tempUsername = (String)request.getSession(true).getAttribute(VerificationConstants.CACHE_LOGIN_USERNAME);

						String tempUrl = (String) request.getSession(true).getAttribute(VerificationConstants.LOGIN_URL);

						logger.debug("login #################"+ tempIp + ":" + tempUsername);


						service.processInvalidLogin(tempIp, tempUsername, request);
						response.sendRedirect(tempUrl);
						return;
					}
					else
					{	LoginDTO loginDTO = new LoginDTO();

						loginDTO.isOisf =1;

						loginDTO.userID = tempOisfDto.getOfficeUnitOrgId();


						UserDTO loginUserDTO = UserRepository.getUserDTOByOrganogramID(loginDTO.userID);
						loginUserDTO.userTypeName = tempOisfDto.getDesignation();

						String tempIP = (String) request.getSession(true).getAttribute(VerificationConstants.LOGIN_IP);
						request.getSession(true).setAttribute(SessionConstants.USER_LOGIN, loginDTO);
						// 	request.getSession(true).setAttribute(VerificationConstants.CACHE_LOGIN_USERNAME, username);

						String tempUsername = (String)request.getSession(true).getAttribute(VerificationConstants.CACHE_LOGIN_USERNAME);
						//request.getSession(true).setAttribute(VerificationConstants.LOGIN_URL, loginUrl);



						if(service.needOTPCheck(loginUserDTO, request))
						{
							request.getSession().removeAttribute(SessionConstants.USER_LOGIN);
							//request.getSession().setAttribute(SessionConstants.USER_LOGIN, loginDTO);
							request.getSession().setAttribute(VerificationConstants.CACHE_LOGIN_USERNAME, username);
							//response.sendRedirect(otpUrl);
							request.getRequestDispatcher(otpUrl).forward(request, response);
						}
						else
						{	//valid login
							if(request.getParameter("stay_logged_in")!=null){
								Cookie cookie = new Cookie(ServletConstant.REMEMBER_ME_COOKIE_NAME, UUID.randomUUID().toString());
								cookie.setMaxAge(60 * 60 * 24 * 365 * 10);
								RememberMeOptionDAO.getInstance().insertRememberMeOption(loginDTO.userID, cookie.getValue(),0);
								response.addCookie(cookie);
							}

							loginDTO.loginSourceIP = tempIP;

							service.processValidLogin(loginDTO, tempUsername, request);

							request.getSession().setAttribute(LibConstants.SSO_DESIGNATION, tempOisfDto.getOfficeUnitOrgId());
							request.getSession().setAttribute("name",tempOisfDto.getname_bn() );
							request.getSession().setAttribute("designation",tempOisfDto.getDesignation() );
							request.getSession().setAttribute("officeName",tempOisfDto.getOfficeNameBng() );


							String lastVisitedUrl = service.getLastRequestedUrl(request);
							if(lastVisitedUrl != null && !lastVisitedUrl.endsWith("LogoutServlet")) response.sendRedirect(lastVisitedUrl);
								//else response.sendRedirect(homeUrl);
							else
							{
//								RequestDispatcher rd = request.getRequestDispatcher(homeUrl);
//								rd.forward(request, response);
								goToHomePage(request, response, homeUrl);
							}
						}
					}




				}
				else
				{
					UserDTO loginUserDTO = UserRepository.getUserDTOByUserID(loginDTO2);
					if(service.needOTPCheck(loginUserDTO, request))
					{
						request.getSession().removeAttribute(SessionConstants.USER_LOGIN);
						request.getSession().setAttribute(VerificationConstants.CACHE_LOGIN_USERNAME, username);
						//response.sendRedirect(otpUrl);
						request.getRequestDispatcher(otpUrl).forward(request, response);

					}
					else
					{	//valid login
						if(request.getParameter("stay_logged_in")!=null){
							Cookie cookie = new Cookie(ServletConstant.REMEMBER_ME_COOKIE_NAME, UUID.randomUUID().toString());
							cookie.setMaxAge(60 * 60 * 24 * 365 * 10);
							RememberMeOptionDAO.getInstance().insertRememberMeOption(loginDTO2.userID, cookie.getValue(),0);
							response.addCookie(cookie);
						}

						service.processValidLogin(loginDTO2, username, request);
						String lastVisitedUrl = service.getLastRequestedUrl(request);
						if(lastVisitedUrl != null && !lastVisitedUrl.endsWith("LogoutServlet")) response.sendRedirect(lastVisitedUrl);
							//else response.sendRedirect(homeUrl);
						else
						{
//							RequestDispatcher rd = request.getRequestDispatcher(homeUrl);
//							rd.forward(request, response);
							goToHomePage(request, response, homeUrl);
						}
					}

				}

			}
			else
			{
				LoginDTO loginDTO = service.validateLogin(username, password, loginDTOTemp);
				if(loginDTO == null)
				{
					service.processInvalidLogin(loginIP, username, request);
					request.getRequestDispatcher(loginUrl).forward(request, response);
					//response.sendRedirect(loginUrl);
					return;
				}
				else
				{
					UserDTO loginUserDTO = UserRepository.getUserDTOByUserID(loginDTO);
					if(service.needOTPCheck(loginUserDTO, request))
					{
						request.getSession().removeAttribute(SessionConstants.USER_LOGIN);
						//request.getSession().setAttribute(SessionConstants.USER_LOGIN, loginDTO);
						request.getSession().setAttribute(VerificationConstants.CACHE_LOGIN_USERNAME, username);
						//response.sendRedirect(otpUrl);
						request.getRequestDispatcher(otpUrl).forward(request, response);

					}
					else
					{	//valid login
						if(request.getParameter("stay_logged_in")!=null){
							Cookie cookie = new Cookie(ServletConstant.REMEMBER_ME_COOKIE_NAME, UUID.randomUUID().toString());
							cookie.setMaxAge(60 * 60 * 24 * 365 * 10);
							RememberMeOptionDAO.getInstance().insertRememberMeOption(loginDTO.userID, cookie.getValue(),0);
							response.addCookie(cookie);
						}

						service.processValidLogin(loginDTO, username, request);
						String lastVisitedUrl = service.getLastRequestedUrl(request);
						if(lastVisitedUrl != null && !lastVisitedUrl.endsWith("LogoutServlet")) response.sendRedirect(lastVisitedUrl);
							//else response.sendRedirect(homeUrl);
						else
						{
//							RequestDispatcher rd = request.getRequestDispatcher(homeUrl);
//							rd.forward(request, response);

							goToHomePage(request,response, homeUrl);

						}
					}
				}
			}

		}
		catch(Exception e)
		{
			logger.fatal("Exception during login", e);
			//response.sendRedirect(loginUrl);
			request.getRequestDispatcher(loginUrl).forward(request, response);
		}
	}


	void goToHomePage(HttpServletRequest request, HttpServletResponse response, String homeUrl) throws IOException {
		// setting recruitmentUser in session
		request.getSession().setAttribute(SessionConstants.RECRUITMENT_USER, true);
		response.sendRedirect(homeUrl);
	}
}
