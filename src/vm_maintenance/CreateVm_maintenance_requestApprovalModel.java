package vm_maintenance;

/*
 * @author Md. Erfan Hossain
 * @created 07/05/2021 - 12:31 AM
 * @project parliament
 */


public class CreateVm_maintenance_requestApprovalModel {
    private final long taskTypeId;
    private final long vm_fuel_requestId;
    private final long requesterEmployeeRecordId;

    private CreateVm_maintenance_requestApprovalModel(CreateVm_fuel_requestApprovalModelBuilder builder){
        taskTypeId = builder.taskTypeId;
        vm_fuel_requestId = builder.vm_fuel_requestId;
        requesterEmployeeRecordId = builder.requesterEmployeeRecordId;
    }

    public long getTaskTypeId() {
        return taskTypeId;
    }

    public long getVm_fuel_requestInfoId() {
        return vm_fuel_requestId;
    }

    public long getRequesterEmployeeRecordId() {
        return requesterEmployeeRecordId;
    }

    public static class CreateVm_fuel_requestApprovalModelBuilder{
        private long taskTypeId;
        private long vm_fuel_requestId;
        private long requesterEmployeeRecordId;

        public CreateVm_fuel_requestApprovalModelBuilder setTaskTypeId(long taskTypeId) {
            this.taskTypeId = taskTypeId;
            return this;
        }

        public CreateVm_fuel_requestApprovalModelBuilder setVm_fuel_requestId(long cardInfoId) {
            this.vm_fuel_requestId = cardInfoId;
            return this;
        }

        public CreateVm_fuel_requestApprovalModelBuilder setRequesterEmployeeRecordId(long requesterEmployeeRecordId) {
            this.requesterEmployeeRecordId = requesterEmployeeRecordId;
            return this;
        }

        public CreateVm_maintenance_requestApprovalModel build(){
            return new CreateVm_maintenance_requestApprovalModel(this);
        }
    }
}
