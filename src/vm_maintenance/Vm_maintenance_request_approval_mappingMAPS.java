package vm_maintenance;

import util.CommonMaps;

@SuppressWarnings("unused")
public class Vm_maintenance_request_approval_mappingMAPS extends CommonMaps {
    public Vm_maintenance_request_approval_mappingMAPS(String tableName) {
        java_DTO_map.put("id".toLowerCase(), "id".toLowerCase());
        java_DTO_map.put("cardInfoId".toLowerCase(), "cardInfoId".toLowerCase());
        java_DTO_map.put("cardApprovalId".toLowerCase(), "cardApprovalId".toLowerCase());
        java_DTO_map.put("sequence".toLowerCase(), "sequence".toLowerCase());
        java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
        java_DTO_map.put("insertedBy".toLowerCase(), "insertedBy".toLowerCase());
        java_DTO_map.put("insertionTime".toLowerCase(), "insertionTime".toLowerCase());
        java_DTO_map.put("modifiedBy".toLowerCase(), "modifiedBy".toLowerCase());
        java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
        java_DTO_map.put("cardApprovalStatusCat".toLowerCase(), "cardApprovalStatusCat".toLowerCase());
        java_DTO_map.put("employeeRecordsId".toLowerCase(), "employeeRecordsId".toLowerCase());

        java_SQL_map.put("card_info_id".toLowerCase(), "cardInfoId".toLowerCase());
        java_SQL_map.put("card_approval_id".toLowerCase(), "cardApprovalId".toLowerCase());
        java_SQL_map.put("sequence".toLowerCase(), "sequence".toLowerCase());
        java_SQL_map.put("card_approval_status_cat".toLowerCase(), "cardApprovalStatusCat".toLowerCase());
        java_SQL_map.put("employee_records_id".toLowerCase(), "employeeRecordsId".toLowerCase());

        java_Text_map.put("Id".toLowerCase(), "id".toLowerCase());
        java_Text_map.put("Vm_fuel_request Info Id".toLowerCase(), "cardInfoId".toLowerCase());
        java_Text_map.put("Vm_fuel_request Approval Id".toLowerCase(), "cardApprovalId".toLowerCase());
        java_Text_map.put("Sequence".toLowerCase(), "sequence".toLowerCase());
        java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
        java_Text_map.put("Inserted By".toLowerCase(), "insertedBy".toLowerCase());
        java_Text_map.put("Insertion Time".toLowerCase(), "insertionTime".toLowerCase());
        java_Text_map.put("Modified By".toLowerCase(), "modifiedBy".toLowerCase());
        java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
        java_Text_map.put("Vm_fuel_request Approval Status".toLowerCase(), "cardApprovalStatusCat".toLowerCase());
        java_Text_map.put("Employee Records Id".toLowerCase(), "employeeRecordsId".toLowerCase());
    }
}