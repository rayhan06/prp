package vm_maintenance;

import employee_records.Employee_recordsDTO;
import employee_records.Employee_recordsRepository;
import login.LoginDTO;
import office_unit_organograms.OfficeUnitOrganograms;
import office_unit_organograms.OfficeUnitOrganogramsRepository;
import office_units.Office_unitsDTO;
import office_units.Office_unitsRepository;
import org.apache.log4j.Logger;
import permission.MenuConstants;
import role.PermissionRepository;
import sessionmanager.SessionConstants;
import task_type.TaskTypeEnum;
import user.UserDTO;
import user.UserRepository;
import util.RecordNavigationManager4;
import vm_maintenance_vehicle_item_mapping.Vm_maintenance_vehicle_item_mappingDAO;
import vm_maintenance_vehicle_item_mapping.Vm_maintenance_vehicle_item_mappingDTO;
import vm_vehicle.Vm_vehicleDAO;
import vm_vehicle.Vm_vehicleDTO;
import vm_vehicle.Vm_vehicleRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


@WebServlet("/Vm_maintenance_request_approval_mappingServlet")
@MultipartConfig
public class Vm_maintenance_request_approval_mappingServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private static final Logger logger = Logger.getLogger(Vm_maintenance_request_approval_mappingServlet.class);

    private static final String tableName = "vm_maintenance_request_approval_mapping";

    private final Vm_maintenance_request_approval_mappingDAO card_approval_mappingDAO;

    public Vm_maintenance_request_approval_mappingServlet() {
        super();
        card_approval_mappingDAO = new Vm_maintenance_request_approval_mappingDAO(tableName);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }
        try {
            String actionType = request.getParameter("actionType");
            logger.debug("actionType : "+actionType);
            switch (actionType) {
                case "getApprovalPage":
                    request.getRequestDispatcher("vm_maintenance_mechanic_approve/vm_maintenanceMechanicApproveEdit.jsp").forward(request, response);
                    return;
                case "getApprovalPageForAO":
                    request.getRequestDispatcher("vm_maintenance_ao_approve/vm_maintenance_ao_Edit.jsp").forward(request, response);
                    return;
                case "search":
//                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.CARD_APPROVAL_MAPPING_SEARCH)) {

//                        if (userDTO.roleID == RoleEnum.EMPLOYEE_OF_PARLIAMENT.getRoleId()) {
                        searchVm_fuel_request_approval_mapping(request, response, String.valueOf(userDTO.employee_record_id), loginDTO, userDTO);
//                        } else {
//                            searchVm_fuel_request_approval_mapping(request, response, "", loginDTO, userDTO);
//                        }
                        return;
//                    }
//                    break;
                case "approvedVm_fuel_request":
//                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.CARD_APPROVAL_MAPPING_ADD)) {
                        Approve(request, userDTO, true,null,null);
                        response.sendRedirect("Vm_maintenance_request_approval_mappingServlet?actionType=search");
                        return;
//                    }
//                    break;
                case "rejectVm_fuel_request":
//                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.CARD_APPROVAL_MAPPING_ADD)) {
                        Approve(request, userDTO, false,null,null);
                        response.sendRedirect("Vm_maintenance_request_approval_mappingServlet?actionType=search");
                        return;
//                    }
//                    break;
            }
        } catch (Exception ex) {
            logger.debug(ex);
            ex.printStackTrace();
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }

        try {
            String actionType = request.getParameter("actionType");
            switch (actionType) {
                case "approvedVm_fuel_request":
//                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.CARD_APPROVAL_MAPPING_ADD)) {
                    if (true) {
                        SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
//                        Date from = f.parse(Jsoup.clean(request.getParameter("validationStartDate"), Whitelist.simpleText()));
//                        Date to = f.parse(Jsoup.clean(request.getParameter("validationEndDate"), Whitelist.simpleText()));
                        Approve(request, userDTO, true,null,null);
                        response.sendRedirect("Vm_maintenance_request_approval_mappingServlet?actionType=search");
                        return;
                    }
                    break;
                case "rejectVm_fuel_request":
//                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.CARD_APPROVAL_MAPPING_ADD)) {
                    if (true) {
                        Approve(request, userDTO, false,null,null);
                        response.sendRedirect("Vm_maintenance_request_approval_mappingServlet?actionType=search");
                        return;
                    }
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error(ex);
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }

    private void Approve(HttpServletRequest request, UserDTO userDTO, boolean isAccepted,Date validFrom,Date validTo) throws Exception {

        long requesterEmployeeRecordId = userDTO.employee_record_id;
        long cardInfoId = Long.parseLong(request.getParameter("ID"));

        Vm_maintenanceDAO cardInfoDAO = new Vm_maintenanceDAO();
        Vm_maintenanceDTO card_infoDTO = cardInfoDAO.getDTOByID(cardInfoId);

        TaskTypeEnum taskTypeEnum = TaskTypeEnum.VM_MAINTENANCE_REQUEST;

        CreateVm_maintenance_requestApprovalModel model = new CreateVm_maintenance_requestApprovalModel.CreateVm_fuel_requestApprovalModelBuilder()
                .setTaskTypeId(taskTypeEnum.getValue())
                .setVm_fuel_requestId(card_infoDTO.iD)
                .setRequesterEmployeeRecordId(requesterEmployeeRecordId)
                .build();

        Vm_maintenance_request_approval_mappingDAO vm_fuel_request_approval_mappingDAO = new Vm_maintenance_request_approval_mappingDAO();

        Vm_maintenance_request_approval_mappingDAO.ValidateResponse validateResponse = vm_fuel_request_approval_mappingDAO.validate(model);


        if (isAccepted) {
            Vm_maintenance_requestApprovalResponse response = vm_fuel_request_approval_mappingDAO.movedToNextLevelOfApproval(model);
            if(response.hasNextApproval){


                card_infoDTO.mecha_approver_employee_record_id = userDTO.employee_record_id;
                Employee_recordsDTO employee_recordsDTO = Employee_recordsRepository.getInstance().
                        getById(userDTO.employee_record_id);

                if(employee_recordsDTO != null){
                    card_infoDTO.mecha_approver_employee_record_name = employee_recordsDTO.nameEng;
                    card_infoDTO.mecha_approver_employee_record_name_bn = employee_recordsDTO.nameBng;
                }

                card_infoDTO.mecha_approver_post_id = userDTO.organogramID;
                OfficeUnitOrganograms org = OfficeUnitOrganogramsRepository.getInstance().getById(userDTO.organogramID);
                if(org != null){
                    card_infoDTO.mecha_approver_post_name_bn = org.designation_bng;
                    card_infoDTO.mecha_approver_post_name = org.designation_eng;

                    card_infoDTO.mecha_approver_unit_id = org.office_unit_id;

                    Office_unitsDTO unit = Office_unitsRepository.getInstance().
                            getOffice_unitsDTOByID(org.office_unit_id);

                    if(unit != null){
                        card_infoDTO.mecha_approver_unit_name = unit.unitNameEng;
                        card_infoDTO.mecha_approver_unit_name_bn = unit.unitNameBng;
                    }
                }

                long currentTime = System.currentTimeMillis();
                card_infoDTO.status = 3;
                card_infoDTO.mechanicApproveDate = currentTime;
                cardInfoDAO.update(card_infoDTO);

                VmMaintenanceItemDAO vmMaintenanceItemDAO = new VmMaintenanceItemDAO();
                int itemCount = Integer.parseInt(request.getParameter("itemLength"));
                for(int i =itemCount -1; i >= 0 ; i--){
                    VmMaintenanceItemDTO itemDTO = null;
                    long itemId = Long.parseLong(request.getParameter("itemId_" + i));
                    if(itemId == -100){
                        itemDTO = new VmMaintenanceItemDTO();
                    } else {
                        itemDTO = vmMaintenanceItemDAO.getDTOByID(itemId);
                    }
                    boolean selected = Boolean.parseBoolean(request.getParameter("checked_" + i));
                    itemDTO.selected = selected;
                    String remarks = request.getParameter("mRemarks_" + i);
                    itemDTO.mechanic_remarks = remarks;
                    if(selected){
                        double mValue = Double.parseDouble(request.getParameter("mPrice_" + i));
                        itemDTO.mechanic_price = mValue;
                    }
                    if(itemId == -100){
                        itemDTO.vmMaintenanceId = card_infoDTO.iD;
                        int part = Integer.parseInt(request.getParameter("parts_" + i));
                        itemDTO.vmVehiclePartsType = part;
                        int type = Integer.parseInt(request.getParameter("type_" + i));
                        itemDTO.vehicleMaintenanceCat = type;
                        String r = request.getParameter("dRemarks_" + i);
                        itemDTO.remarks =  r;
                        int amount = Integer.parseInt(request.getParameter("amount_" + i));
                        itemDTO.amount = amount;
                        itemDTO.driverPrice = itemDTO.mechanic_price;
                        vmMaintenanceItemDAO.add(itemDTO);
                    } else {
                        vmMaintenanceItemDAO.update(itemDTO);
                    }

                }
            } else {
                // approval logic for level 2

                card_infoDTO.ao_approver_employee_record_id = userDTO.employee_record_id;
                Employee_recordsDTO employee_recordsDTO = Employee_recordsRepository.getInstance().
                        getById(userDTO.employee_record_id);

                if(employee_recordsDTO != null){
                    card_infoDTO.ao_approver_employee_record_name = employee_recordsDTO.nameEng;
                    card_infoDTO.ao_approver_employee_record_name_bn = employee_recordsDTO.nameBng;
                }

                card_infoDTO.ao_approver_post_id = userDTO.organogramID;
                OfficeUnitOrganograms org = OfficeUnitOrganogramsRepository.getInstance().getById(userDTO.organogramID);
                if(org != null){
                    card_infoDTO.ao_approver_post_name_bn = org.designation_bng;
                    card_infoDTO.ao_approver_post_name = org.designation_eng;

                    card_infoDTO.ao_approver_unit_id = org.office_unit_id;

                    Office_unitsDTO unit = Office_unitsRepository.getInstance().
                            getOffice_unitsDTOByID(org.office_unit_id);

                    if(unit != null){
                        card_infoDTO.ao_approver_unit_name = unit.unitNameEng;
                        card_infoDTO.ao_approver_unit_name_bn = unit.unitNameBng;
                    }
                }

                long currentTime = System.currentTimeMillis();
                Vm_vehicleDTO vm_vehicleDTO = Vm_vehicleRepository.getInstance()
                        .getVm_vehicleDTOByID(card_infoDTO.vehicleId);
                vm_vehicleDTO.last_maintenance_date = currentTime;
                vm_vehicleDTO.last_maintenance_id = card_infoDTO.iD;
                new Vm_vehicleDAO().update(vm_vehicleDTO);

                card_infoDTO.status = 5;
                card_infoDTO.aoApproveDate = currentTime;

                double vat = Double.parseDouble(request.getParameter("vat"));
                card_infoDTO.vatPercentage = vat;

                double totalEstimatedValue = Double.parseDouble(request.getParameter("totalEstimatedValue"));
                card_infoDTO.totalWithoutVat = totalEstimatedValue;

                double totalVatValue = Double.parseDouble(request.getParameter("totalVatValue"));
                card_infoDTO.totalVat = totalVatValue;

                double totalSumValue = Double.parseDouble(request.getParameter("totalSumValue"));
                card_infoDTO.totalWithVat = totalSumValue;


                cardInfoDAO.update(card_infoDTO);

                Vm_maintenance_vehicle_item_mappingDAO maintenance_vehicle_item_mappingDAO =
                        new Vm_maintenance_vehicle_item_mappingDAO();

                VmMaintenanceItemDAO vmMaintenanceItemDAO = new VmMaintenanceItemDAO();
                int itemCount = Integer.parseInt(request.getParameter("itemLength"));
                for(int i =itemCount -1; i >= 0 ; i--){
                    VmMaintenanceItemDTO itemDTO = null;
                    long itemId = Long.parseLong(request.getParameter("itemId_" + i));
                    itemDTO = vmMaintenanceItemDAO.getDTOByID(itemId);

                    double mValue = Double.parseDouble(request.getParameter("mPrice_" + i));
                    itemDTO.ao_price = mValue;
                    vmMaintenanceItemDAO.update(itemDTO);

                    List<Vm_maintenance_vehicle_item_mappingDTO> item_mappingDTOS = maintenance_vehicle_item_mappingDAO.
                            getAllVm_maintenance_vehicle_item_mappingByVehicleIdAndPartsId(card_infoDTO.vehicleId, itemDTO.vmVehiclePartsType);

                    for(Vm_maintenance_vehicle_item_mappingDTO item_mappingDTO: item_mappingDTOS){
                        maintenance_vehicle_item_mappingDAO.delete(item_mappingDTO.iD);
                    }

                    Vm_maintenance_vehicle_item_mappingDTO newItemMappingDTO = new Vm_maintenance_vehicle_item_mappingDTO();
                    newItemMappingDTO.vmId = card_infoDTO.vehicleId;
                    newItemMappingDTO.partsId = itemDTO.vmVehiclePartsType;
                    newItemMappingDTO.date = currentTime;
                    maintenance_vehicle_item_mappingDAO.add(newItemMappingDTO);


                }
            }
        } else {
            vm_fuel_request_approval_mappingDAO.rejectPendingApproval(model);
            long currentTime = System.currentTimeMillis();
            if(validateResponse.currentLevel == 1){
                card_infoDTO.status = 2;
                card_infoDTO.mechanicApproveDate = currentTime;

                card_infoDTO.mecha_approver_employee_record_id = userDTO.employee_record_id;
                Employee_recordsDTO employee_recordsDTO = Employee_recordsRepository.getInstance().
                        getById(userDTO.employee_record_id);

                if(employee_recordsDTO != null){
                    card_infoDTO.mecha_approver_employee_record_name = employee_recordsDTO.nameEng;
                    card_infoDTO.mecha_approver_employee_record_name_bn = employee_recordsDTO.nameBng;
                }

                card_infoDTO.mecha_approver_post_id = userDTO.organogramID;
                OfficeUnitOrganograms org = OfficeUnitOrganogramsRepository.getInstance().getById(userDTO.organogramID);
                if(org != null){
                    card_infoDTO.mecha_approver_post_name_bn = org.designation_bng;
                    card_infoDTO.mecha_approver_post_name = org.designation_eng;

                    card_infoDTO.mecha_approver_unit_id = org.office_unit_id;

                    Office_unitsDTO unit = Office_unitsRepository.getInstance().
                            getOffice_unitsDTOByID(org.office_unit_id);

                    if(unit != null){
                        card_infoDTO.mecha_approver_unit_name = unit.unitNameEng;
                        card_infoDTO.mecha_approver_unit_name_bn = unit.unitNameBng;
                    }
                }


                cardInfoDAO.update(card_infoDTO);
            } else {
                // reject logic for level 2

                card_infoDTO.ao_approver_employee_record_id = userDTO.employee_record_id;
                Employee_recordsDTO employee_recordsDTO = Employee_recordsRepository.getInstance().
                        getById(userDTO.employee_record_id);

                if(employee_recordsDTO != null){
                    card_infoDTO.ao_approver_employee_record_name = employee_recordsDTO.nameEng;
                    card_infoDTO.ao_approver_employee_record_name_bn = employee_recordsDTO.nameBng;
                }

                card_infoDTO.ao_approver_post_id = userDTO.organogramID;
                OfficeUnitOrganograms org = OfficeUnitOrganogramsRepository.getInstance().getById(userDTO.organogramID);
                if(org != null){
                    card_infoDTO.ao_approver_post_name_bn = org.designation_bng;
                    card_infoDTO.ao_approver_post_name = org.designation_eng;

                    card_infoDTO.ao_approver_unit_id = org.office_unit_id;

                    Office_unitsDTO unit = Office_unitsRepository.getInstance().
                            getOffice_unitsDTOByID(org.office_unit_id);

                    if(unit != null){
                        card_infoDTO.ao_approver_unit_name = unit.unitNameEng;
                        card_infoDTO.ao_approver_unit_name_bn = unit.unitNameBng;
                    }
                }

                card_infoDTO.status = 4;
                card_infoDTO.aoApproveDate = currentTime;
                cardInfoDAO.update(card_infoDTO);
            }

        }
    }


    private void searchVm_fuel_request_approval_mapping(HttpServletRequest request, HttpServletResponse response, String filter,
                                             LoginDTO loginDTO, UserDTO userDTO) throws Exception {
        String ajax = request.getParameter("ajax");
        boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");

        logger.debug("ajax = " + ajax + " hasajax = " + hasAjax);

        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
                SessionConstants.NAV_CARD_APPROVAL_MAPPING,
                request,
                card_approval_mappingDAO,
                SessionConstants.VIEW_CARD_APPROVAL_MAPPING,
                SessionConstants.SEARCH_CARD_APPROVAL_MAPPING,
                tableName,
                true,
                userDTO,
                filter,
                true);

        rnManager.doJob(loginDTO);


        request.setAttribute("vm_fuel_request_approval_mappingDAO", card_approval_mappingDAO);

        if (!hasAjax) {
            request.getRequestDispatcher("vm_maintenance_request_approval_mapping/vm_maintenance_request_approval_mappingSearch.jsp").forward(request, response);
        } else {
            request.getRequestDispatcher("vm_maintenance_request_approval_mapping/vm_maintenance_request_approval_mappingSearchForm.jsp").forward(request, response);
        }
    }
}