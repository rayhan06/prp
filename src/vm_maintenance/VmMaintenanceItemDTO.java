package vm_maintenance;
import java.util.*; 
import util.*; 


public class VmMaintenanceItemDTO extends CommonDTO
{

	public long vmMaintenanceId = -1;
	public long vmVehiclePartsType = -1;
	public int vehicleMaintenanceCat = -1;
	public int amount = -1;
//    public String remarks = "";
	public String mechanic_remarks = "";
	public double ao_price = 0;
	public double driverPrice = 0;
	public double mechanic_price = 0;
	public long insertedByUserId = -1;
	public long insertedByOrganogramId = -1;
	public long insertionDate = -1;
	public boolean selected = false;
	
	public List<VmMaintenanceItemDTO> vmMaintenanceItemDTOList = new ArrayList<>();
	
    @Override
	public String toString() {
            return "$VmMaintenanceItemDTO[" +
            " iD = " + iD +
            " vmMaintenanceId = " + vmMaintenanceId +
            " vmVehiclePartsType = " + vmVehiclePartsType +
            " vehicleMaintenanceCat = " + vehicleMaintenanceCat +
            " amount = " + amount +
            " remarks = " + remarks +
            " driverPrice = " + driverPrice +
            " searchColumn = " + searchColumn +
            " insertedByUserId = " + insertedByUserId +
            " insertedByOrganogramId = " + insertedByOrganogramId +
            " insertionDate = " + insertionDate +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}