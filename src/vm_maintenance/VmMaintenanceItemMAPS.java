package vm_maintenance;
import java.util.*; 
import util.*;


public class VmMaintenanceItemMAPS extends CommonMaps
{	
	public VmMaintenanceItemMAPS(String tableName)
	{
		

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("vmMaintenanceId".toLowerCase(), "vmMaintenanceId".toLowerCase());
		java_DTO_map.put("vmVehiclePartsType".toLowerCase(), "vmVehiclePartsType".toLowerCase());
		java_DTO_map.put("vehicleMaintenanceCat".toLowerCase(), "vehicleMaintenanceCat".toLowerCase());
		java_DTO_map.put("amount".toLowerCase(), "amount".toLowerCase());
		java_DTO_map.put("remarks".toLowerCase(), "remarks".toLowerCase());
		java_DTO_map.put("driverPrice".toLowerCase(), "driverPrice".toLowerCase());
		java_DTO_map.put("searchColumn".toLowerCase(), "searchColumn".toLowerCase());
		java_DTO_map.put("insertedByUserId".toLowerCase(), "insertedByUserId".toLowerCase());
		java_DTO_map.put("insertedByOrganogramId".toLowerCase(), "insertedByOrganogramId".toLowerCase());
		java_DTO_map.put("insertionDate".toLowerCase(), "insertionDate".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("vm_maintenance_id".toLowerCase(), "vmMaintenanceId".toLowerCase());
		java_SQL_map.put("vm_vehicle_parts_type".toLowerCase(), "vmVehiclePartsType".toLowerCase());
		java_SQL_map.put("vehicle_maintenance_cat".toLowerCase(), "vehicleMaintenanceCat".toLowerCase());
		java_SQL_map.put("amount".toLowerCase(), "amount".toLowerCase());
		java_SQL_map.put("remarks".toLowerCase(), "remarks".toLowerCase());
		java_SQL_map.put("driver_price".toLowerCase(), "driverPrice".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Vm Maintenance Id".toLowerCase(), "vmMaintenanceId".toLowerCase());
		java_Text_map.put("Vm Vehicle Parts".toLowerCase(), "vmVehiclePartsType".toLowerCase());
		java_Text_map.put("Vehicle Maintenance".toLowerCase(), "vehicleMaintenanceCat".toLowerCase());
		java_Text_map.put("Amount".toLowerCase(), "amount".toLowerCase());
		java_Text_map.put("Remarks".toLowerCase(), "remarks".toLowerCase());
		java_Text_map.put("Driver Price".toLowerCase(), "driverPrice".toLowerCase());
		java_Text_map.put("Search Column".toLowerCase(), "searchColumn".toLowerCase());
		java_Text_map.put("Inserted By User Id".toLowerCase(), "insertedByUserId".toLowerCase());
		java_Text_map.put("Inserted By Organogram Id".toLowerCase(), "insertedByOrganogramId".toLowerCase());
		java_Text_map.put("Insertion Date".toLowerCase(), "insertionDate".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}