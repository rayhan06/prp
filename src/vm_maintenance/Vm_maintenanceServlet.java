package vm_maintenance;

import com.google.gson.Gson;
import common.ApiResponse;
import employee_records.Employee_recordsDTO;
import employee_records.Employee_recordsRepository;
import files.FilesDAO;
import fiscal_year.Fiscal_yearDAO;
import fiscal_year.Fiscal_yearDTO;
import login.LoginDTO;
import office_unit_organograms.OfficeUnitOrganograms;
import office_unit_organograms.OfficeUnitOrganogramsRepository;
import office_units.Office_unitsDTO;
import office_units.Office_unitsRepository;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import pb.Utils;
import permission.MenuConstants;
import role.PermissionRepository;
import sessionmanager.SessionConstants;
import task_type.TaskTypeEnum;
import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;
import util.CommonRequestHandler;
import util.RecordNavigationManager4;
import vm_vehicle.Vm_vehicleDAO;
import vm_vehicle.Vm_vehicleDTO;
import vm_vehicle.Vm_vehicleRepository;
import vm_vehicle_office_assignment.Vm_vehicle_office_assignmentDAO;
import vm_vehicle_office_assignment.Vm_vehicle_office_assignmentDTO;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;


/**
 * Servlet implementation class Vm_maintenanceServlet
 */
@WebServlet("/Vm_maintenanceServlet")
@MultipartConfig
public class Vm_maintenanceServlet extends HttpServlet
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Vm_maintenanceServlet.class);

    String tableName = "vm_maintenance";

	Vm_maintenanceDAO vm_maintenanceDAO;
	CommonRequestHandler commonRequestHandler;
	FilesDAO filesDAO = new FilesDAO();
	VmMaintenanceItemDAO vmMaintenanceItemDAO;
    private final Gson gson = new Gson();

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Vm_maintenanceServlet()
	{
        super();
    	try
    	{
			vm_maintenanceDAO = new Vm_maintenanceDAO(tableName);
			vmMaintenanceItemDAO = new VmMaintenanceItemDAO("vm_maintenance_item");
			commonRequestHandler = new CommonRequestHandler(vm_maintenanceDAO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		System.out.println("In doget request = " + request);
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		try
		{
			String actionType = request.getParameter("actionType");
			if(actionType.equals("getAddPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_MAINTENANCE_ADD))
				{
					commonRequestHandler.getAddPage(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getEditPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_MAINTENANCE_UPDATE))
				{
					getVm_maintenance(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getReportPage")){
				request.getRequestDispatcher("vm_vehicle_wise_log_book_report/vm_vehicle_wise_log_bookEdit.jsp").forward(request, response);
			}
			else if(actionType.equals("downloadDropzoneFile"))
			{
				commonRequestHandler.downloadDropzoneFile(request, response, filesDAO);
			}
			else if(actionType.equals("DeleteFileFromDropZone"))
			{
				commonRequestHandler.deleteFileFromDropZone(request, response, filesDAO);
			}
			else if(actionType.equals("getURL"))
			{
				String URL = request.getParameter("URL");
				System.out.println("URL = " + URL);
				response.sendRedirect(URL);
			}
			else if(actionType.equals("billRegisterPage")){
				System.out.println("billRegisterPage requested");
				String URL = "vm_bill_register_maintenance/vm_maintenanceEdit.jsp";
				RequestDispatcher rd = request.getRequestDispatcher(URL);
				rd.forward(request, response);
			}
			else if(actionType.equals("getBillRegisterData")){
				System.out.println("getBillRegisterData requested");
				getBillRegisterData(request, response);
			}
			else if(actionType.equals("search"))
			{
				System.out.println("search requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_MAINTENANCE_SEARCH))
				{
					if(isPermanentTable)
					{
						String filter = request.getParameter("filter");
						System.out.println("filter = " + filter);
						if(filter!=null)
						{
							filter = ""; //shouldn't be directly used, rather manipulate it.
							searchVm_maintenance(request, response, isPermanentTable, filter);
						}
						else
						{
							searchVm_maintenance(request, response, isPermanentTable, "");
						}
					}
					else
					{
						//searchVm_maintenance(request, response, tempTableName, isPermanentTable);
					}
				}
			}

			else if(actionType.equals("paymentSearch"))
			{
				System.out.println("search requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_MAINTENANCE_PAYMENT))
				{
					if(isPermanentTable)
					{
//						String filter = request.getParameter("filter");
//						System.out.println("filter = " + filter);

						String	filter = " status = 5 "; //shouldn't be directly used, rather manipulate it.
						searchVm_maintenanceForPayment(request, response, isPermanentTable, filter);

					}
					else
					{
						//searchVm_maintenance(request, response, tempTableName, isPermanentTable);
					}
				}
			}
			else if(actionType.equals("view"))
			{
				System.out.println("view requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_MAINTENANCE_SEARCH))
				{
					commonRequestHandler.view(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}

//			else if(actionType.equals("mechanicApproval"))
//			{
//				System.out.println("mechanicApproval requested");
//				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_MAINTENANCE_MECHANIC_APPROVE))
//				{
//					String URL = "vm_maintenance_mechanic_approve/vm_maintenanceMechanicApproveEdit.jsp";
//					RequestDispatcher rd = request.getRequestDispatcher(URL);
//					rd.forward(request, response);
//
//				}
//				else
//				{
//					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
//				}
//
//			}

			else if(actionType.equals("paymentView"))
			{
				System.out.println("paymentView requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_MAINTENANCE_PAYMENT))
				{
					String URL = "vm_maintenance_payment/payment_view.jsp";
					RequestDispatcher rd = request.getRequestDispatcher(URL);
					rd.forward(request, response);

				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}

			else if(actionType.equals("payment"))
			{
				System.out.println("payment requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_MAINTENANCE_PAYMENT))
				{
					String URL = "vm_maintenance_payment/payment_edit.jsp";
					RequestDispatcher rd = request.getRequestDispatcher(URL);
					rd.forward(request, response);

				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}

			else
			{
				request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}





	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		// TODO Auto-generated method stub
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		System.out.println("doPost");
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);

		try
		{
			String actionType = request.getParameter("actionType");
			System.out.println("actionType = " + actionType);
			if(actionType.equals("add"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_MAINTENANCE_ADD))
				{
					System.out.println("going to  addVm_maintenance ");
					addVm_maintenance(request, response, true, userDTO, true);
				}
				else
				{
					System.out.println("Not going to  addVm_maintenance ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}
			else if(actionType.equals("UploadFilesFromDropZone"))
			{
				commonRequestHandler.UploadFilesFromDropZone(request, response, userDTO);
			}

			else if(actionType.equals("mechanicReject")){
				mechanicReject(request, response, userDTO);
			}

			else if(actionType.equals("mechanicApprove")){
				mechanicApprove(request, response, userDTO);
			}

			else if(actionType.equals("directMechanicApprove")){
				mechanicApproveDirect(request, response, userDTO);
			}

			else if(actionType.equals("savePayment")){
				savePayment(request, response, userDTO);
			}

			else if(actionType.equals("getDTO"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_MAINTENANCE_ADD))
				{
					getDTO(request, response);
				}
				else
				{
					System.out.println("Not going to  addVm_maintenance ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}
			else if(actionType.equals("edit"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_MAINTENANCE_UPDATE))
				{
					addVm_maintenance(request, response, false, userDTO, isPermanentTable);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("delete"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_MAINTENANCE_UPDATE))
				{
					commonRequestHandler.delete(request, response, userDTO);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("search"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_MAINTENANCE_SEARCH))
				{
					searchVm_maintenance(request, response, true, "");
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getGeo"))
			{
				System.out.println("going to geoloc ");
				request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
			}

		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}

	private void getDTO(HttpServletRequest request, HttpServletResponse response)
	{
		try
		{
			System.out.println("In getDTO");
			Vm_maintenanceDTO vm_maintenanceDTO = Vm_maintenanceRepository.getInstance().
					getVm_maintenanceDTOByID(Long.parseLong(request.getParameter("ID")));
//					(Vm_maintenanceDTO)vm_maintenanceDAO.getDTOByID(Long.parseLong(request.getParameter("ID")));
			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");

			String encoded = this.gson.toJson(vm_maintenanceDTO);
			System.out.println("json encoded data = " + encoded);
			out.print(encoded);
			out.flush();
		}
		catch (NumberFormatException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void mechanicReject(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO) throws Exception
	{
		Vm_maintenanceDTO vm_maintenanceDTO = Vm_maintenanceRepository.getInstance().
				getVm_maintenanceDTOByID(Long.parseLong(request.getParameter("ID")));
//				(Vm_maintenanceDTO)vm_maintenanceDAO.
//				getDTOByID(Long.parseLong(request.getParameter("ID")));
		VmMaintenanceItemDAO vmMaintenanceItemDAO = new VmMaintenanceItemDAO();
		List<VmMaintenanceItemDTO> vmMaintenanceItemDTOs = vmMaintenanceItemDAO.
				getVmMaintenanceItemDTOListByVmMaintenanceID(vm_maintenanceDTO.iD);

		vm_maintenanceDTO.status = 2;
		vm_maintenanceDAO.update(vm_maintenanceDTO);

//		for()


	}

	private void savePayment(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO) throws Exception
	{

		ApiResponse apiResponse;

		try {
			Vm_maintenanceDTO vm_maintenanceDTO;
			vm_maintenanceDTO = Vm_maintenanceRepository.getInstance().getVm_maintenanceDTOByID
					(Long.parseLong(request.getParameter("ID")));

			String Value = "";

			Value = request.getParameter("sarokNumber");

			if (Value != null) {
				Value = Jsoup.clean(Value, Whitelist.simpleText());
			}
			System.out.println("sarokNumber = " + Value);
			if (Value != null && !Value.equalsIgnoreCase("")) {
				vm_maintenanceDTO.sarokNumber = (Value);
			} else {
				throw new Exception(" Invalid sarok number");
			}


			Value = request.getParameter("paymentRemarks");


			System.out.println("paymentRemarks = " + Value);
			if (Value != null && !Value.equalsIgnoreCase("")) {
				Value = Jsoup.clean(Value, Whitelist.simpleText());
				vm_maintenanceDTO.paymentRemarks = (Value);
			} else {
				System.out.println("rules has a null Value, not updating" + " = " + Value);
			}


			Value = request.getParameter("filesDropzone");

			if (Value != null && !Value.equalsIgnoreCase("")) {
				Value = Jsoup.clean(Value, Whitelist.simpleText());
			}
			System.out.println("filesDropzone = " + Value);
			if (Value != null && !Value.equalsIgnoreCase("")) {

				System.out.println("filesDropzone = " + Value);

				vm_maintenanceDTO.filesDropzone = Long.parseLong(Value);
			} else {
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			vm_maintenanceDTO.payment_receiver_employee_record_id = userDTO.employee_record_id;
			Employee_recordsDTO employee_recordsDTO = Employee_recordsRepository.getInstance().
					getById(userDTO.employee_record_id);

			if (employee_recordsDTO != null) {
				vm_maintenanceDTO.payment_receiver_employee_record_name = employee_recordsDTO.nameEng;
				vm_maintenanceDTO.payment_receiver_employee_record_name_bn = employee_recordsDTO.nameBng;
			}

			vm_maintenanceDTO.payment_receiver_post_id = userDTO.organogramID;
			OfficeUnitOrganograms org = OfficeUnitOrganogramsRepository.getInstance().getById(userDTO.organogramID);
			if (org != null) {
				vm_maintenanceDTO.payment_receiver_post_name_bn = org.designation_bng;
				vm_maintenanceDTO.payment_receiver_post_name = org.designation_eng;

				vm_maintenanceDTO.payment_receiver_unit_id = org.office_unit_id;

				Office_unitsDTO unit = Office_unitsRepository.getInstance().
						getOffice_unitsDTOByID(org.office_unit_id);

				if (unit != null) {
					vm_maintenanceDTO.payment_receiver_unit_name = unit.unitNameEng;
					vm_maintenanceDTO.payment_receiver_unit_name_bn = unit.unitNameBng;
				}
			}

			vm_maintenanceDTO.paymentReceivedDate = System.currentTimeMillis();
			vm_maintenanceDTO.status = 6;
			vm_maintenanceDAO.update(vm_maintenanceDTO);

			apiResponse = ApiResponse.makeSuccessResponse("Vm_maintenanceServlet?actionType=paymentSearch");


		} catch (Exception e)
		{
			e.printStackTrace();
			apiResponse = ApiResponse.makeErrorResponse(e.getMessage());
		}

		PrintWriter pw = response.getWriter();
		pw.write(apiResponse.getJSONString());
		pw.flush();
		pw.close();
	}

	private void mechanicApproveDirect(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO) throws Exception
	{
		Vm_maintenanceDTO vm_maintenanceDTO = Vm_maintenanceRepository.getInstance().
				getVm_maintenanceDTOByID(Long.parseLong(request.getParameter("ID")));
//				(Vm_maintenanceDTO)vm_maintenanceDAO.
//				getDTOByID(Long.parseLong(request.getParameter("ID")));
		VmMaintenanceItemDAO vmMaintenanceItemDAO = new VmMaintenanceItemDAO();

		vm_maintenanceDTO.status = 3;
		vm_maintenanceDAO.update(vm_maintenanceDTO);

		List<VmMaintenanceItemDTO> vmMaintenanceItemDTOs = vmMaintenanceItemDAO.
				getVmMaintenanceItemDTOListByVmMaintenanceID(vm_maintenanceDTO.iD);

		for(VmMaintenanceItemDTO itemDTO: vmMaintenanceItemDTOs){
			itemDTO.selected = true;
			itemDTO.mechanic_price = itemDTO.driverPrice;
			vmMaintenanceItemDAO.update(itemDTO);
		}

	}

	private void mechanicApprove(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO) throws Exception
	{
		Vm_maintenanceDTO vm_maintenanceDTO = Vm_maintenanceRepository.getInstance().
				getVm_maintenanceDTOByID(Long.parseLong(request.getParameter("ID")));
//				(Vm_maintenanceDTO)vm_maintenanceDAO.
//				getDTOByID(Long.parseLong(request.getParameter("ID")));
		VmMaintenanceItemDAO vmMaintenanceItemDAO = new VmMaintenanceItemDAO();

		vm_maintenanceDTO.status = 3;
		vm_maintenanceDAO.update(vm_maintenanceDTO);

		int itemCount = Integer.parseInt(request.getParameter("itemLength"));
		for(int i =itemCount -1; i >= 0 ; i--){
			VmMaintenanceItemDTO itemDTO = null;
			long itemId = Long.parseLong(request.getParameter("itemId_" + i));
			if(itemId == -100){
				itemDTO = new VmMaintenanceItemDTO();
			} else {
				itemDTO = vmMaintenanceItemDAO.getDTOByID(itemId);
			}
			boolean selected = Boolean.parseBoolean(request.getParameter("checked_" + i));
			itemDTO.selected = selected;
			String remarks = request.getParameter("mRemarks_" + i);
			itemDTO.mechanic_remarks = remarks;
			if(selected){
				double mValue = Double.parseDouble(request.getParameter("mPrice_" + i));
				itemDTO.mechanic_price = mValue;
			}
			if(itemId == -100){
				itemDTO.vmMaintenanceId = vm_maintenanceDTO.iD;
				int part = Integer.parseInt(request.getParameter("parts_" + i));
				itemDTO.vmVehiclePartsType = part;
				int type = Integer.parseInt(request.getParameter("type_" + i));
				itemDTO.vehicleMaintenanceCat = type;
				String r = request.getParameter("dRemarks_" + i);
				itemDTO.remarks =  r;
				int amount = Integer.parseInt(request.getParameter("amount_" + i));
				itemDTO.amount = amount;
				itemDTO.driverPrice = itemDTO.mechanic_price;
				vmMaintenanceItemDAO.add(itemDTO);
			} else {
				vmMaintenanceItemDAO.update(itemDTO);
			}


		}

	}


	private void addVm_maintenance(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, boolean isPermanentTable) throws IOException
	{
		// TODO Auto-generated method stub

		ApiResponse apiResponse;

		try
		{
			request.setAttribute("failureMessage", "");
			System.out.println("%%%% addVm_maintenance");
			String path = getServletContext().getRealPath("/img2/");
			Vm_maintenanceDTO vm_maintenanceDTO;
			SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");

			if(addFlag == true)
			{
				vm_maintenanceDTO = new Vm_maintenanceDTO();
			}
			else
			{
				vm_maintenanceDTO = Vm_maintenanceRepository.getInstance().
						getVm_maintenanceDTOByID(Long.parseLong(request.getParameter("iD")));
			}

			String Value = "";

			Value = request.getParameter("vehicleTypeCat");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("vehicleTypeCat = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				vm_maintenanceDTO.vehicleTypeCat = Integer.parseInt(Value);
			}
			else
			{
				throw new Exception(" Invalid vehicle type");
			}

			vm_maintenanceDTO.status = 1;

//			Value = request.getParameter("status");
//
//			if(Value != null && !Value.equalsIgnoreCase(""))
//			{
//				Value = Jsoup.clean(Value,Whitelist.simpleText());
//			}
//			System.out.println("status = " + Value);
//			if(Value != null && !Value.equalsIgnoreCase(""))
//			{
//				vm_maintenanceDTO.status = Integer.parseInt(Value);
//			}
//			else
//			{
//				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//			}

			Value = request.getParameter("vehicleId");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("vehicleId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				vm_maintenanceDTO.vehicleId = Long.parseLong(Value);

				List<Vm_vehicle_office_assignmentDTO> office_assignmentDTOS =
						new Vm_vehicle_office_assignmentDAO().getAllVm_vehicle_office_assignmentByVehicleId
						(vm_maintenanceDTO.vehicleId);
				if(office_assignmentDTOS.size() > 0){
					Office_unitsDTO unit = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(office_assignmentDTOS.get(0).officeId);
					if(unit != null){
						vm_maintenanceDTO.vehicleOfficeName = unit.unitNameEng;
						vm_maintenanceDTO.vehicleOfficeNameBn = unit.unitNameBng;
					}

				} else {
					throw new Exception(" Vehicle not assigned to any office");
				}


			}
			else
			{
				throw new Exception(" Invalid vehicle");


			}

			Value = request.getParameter("requesterPostId");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("requesterPostId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				vm_maintenanceDTO.requesterPostId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("requesterUnitId");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("requesterUnitId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				vm_maintenanceDTO.requesterUnitId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("requesterEmployeeRecordId");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("requesterEmployeeRecordId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				vm_maintenanceDTO.requesterEmployeeRecordId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("vehicleDriverAssignmentId");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("vehicleDriverAssignmentId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				vm_maintenanceDTO.vehicleDriverAssignmentId = Long.parseLong(Value);
			}
			else
			{
				throw new Exception(" Driver assignment missing");
			}

			Value = request.getParameter("applicationDate");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("applicationDate = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				try
				{
					Date d = f.parse(Value);
					vm_maintenanceDTO.applicationDate = d.getTime();
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("lastMaintenanceDate");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("lastMaintenanceDate = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				try
				{
					Date d = f.parse(Value);
					vm_maintenanceDTO.lastMaintenanceDate = d.getTime();
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("vehicleOfficeName");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("vehicleOfficeName = " + Value);
			if(Value != null)
			{
				vm_maintenanceDTO.vehicleOfficeName = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("vehicleOfficeNameBn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("vehicleOfficeNameBn = " + Value);
			if(Value != null)
			{
				vm_maintenanceDTO.vehicleOfficeNameBn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("isChecked");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("isChecked = " + Value);
            vm_maintenanceDTO.isChecked = Value != null && !Value.equalsIgnoreCase("");

			Value = request.getParameter("mechanicRemarks");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("mechanicRemarks = " + Value);
			if(Value != null)
			{
				vm_maintenanceDTO.mechanicRemarks = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("mechanicApproveDate");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("mechanicApproveDate = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				try
				{
					Date d = f.parse(Value);
					vm_maintenanceDTO.mechanicApproveDate = d.getTime();
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("aoApproveDate");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("aoApproveDate = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				try
				{
					Date d = f.parse(Value);
					vm_maintenanceDTO.aoApproveDate = d.getTime();
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			vm_maintenanceDTO.aoApproveDate = -1;
			vm_maintenanceDTO.mechanicApproveDate = -1;
			vm_maintenanceDTO.paymentReceivedDate = -1;

			Value = request.getParameter("vatPercentage");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("vatPercentage = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				vm_maintenanceDTO.vatPercentage = Double.parseDouble(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("totalWithoutVat");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("totalWithoutVat = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				vm_maintenanceDTO.totalWithoutVat = Double.parseDouble(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("totalWithVat");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("totalWithVat = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				vm_maintenanceDTO.totalWithVat = Double.parseDouble(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("totalVat");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("totalVat = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				vm_maintenanceDTO.totalVat = Double.parseDouble(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("paymentReceivedDate");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("paymentReceivedDate = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				try
				{
					Date d = f.parse(Value);
					vm_maintenanceDTO.paymentReceivedDate = d.getTime();
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("fiscalYearId");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("fiscalYearId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				vm_maintenanceDTO.fiscalYearId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("sarokNumber");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("sarokNumber = " + Value);
			if(Value != null)
			{
				vm_maintenanceDTO.sarokNumber = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("filesDropzone");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("filesDropzone = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{

				System.out.println("filesDropzone = " + Value);

				vm_maintenanceDTO.filesDropzone = Long.parseLong(Value);


				if(addFlag == false)
				{
					String filesDropzoneFilesToDelete = request.getParameter("filesDropzoneFilesToDelete");
					String[] deleteArray = filesDropzoneFilesToDelete.split(",");
					for(int i = 0; i < deleteArray.length; i ++)
					{
						System.out.println("going to delete " + deleteArray[i]);
						if(i>0)
						{
							filesDAO.delete(Long.parseLong(deleteArray[i]));
						}
					}
				}
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("paymentRemarks");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("paymentRemarks = " + Value);
			if(Value != null)
			{
				vm_maintenanceDTO.paymentRemarks = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}




			if(addFlag)
			{

				vm_maintenanceDTO.insertedByUserId = userDTO.ID;
				vm_maintenanceDTO.insertedByOrganogramId = userDTO.organogramID;

				Calendar c = Calendar.getInstance();
				c.set(Calendar.HOUR_OF_DAY, 0);
				c.set(Calendar.MINUTE, 0);
				c.set(Calendar.SECOND, 0);
				c.set(Calendar.MILLISECOND, 0);

				vm_maintenanceDTO.insertionDate = c.getTimeInMillis();
				vm_maintenanceDTO.applicationDate = c.getTimeInMillis();

				Fiscal_yearDAO fiscal_yearDAO = new Fiscal_yearDAO();
				Fiscal_yearDTO fiscalYearDTO = fiscal_yearDAO.getFiscalYearBYDateLong(c.getTimeInMillis());
				vm_maintenanceDTO.fiscalYearId = fiscalYearDTO.id;

//
				vm_maintenanceDTO.requesterEmployeeRecordId = userDTO.employee_record_id;
				Employee_recordsDTO employee_recordsDTO = Employee_recordsRepository.getInstance().
						getById(userDTO.employee_record_id);

				if(employee_recordsDTO != null){
					vm_maintenanceDTO.requester_employee_record_name = employee_recordsDTO.nameEng;
					vm_maintenanceDTO.requester_employee_record_name_bn = employee_recordsDTO.nameBng;
				}

				vm_maintenanceDTO.requesterPostId = userDTO.organogramID;
				OfficeUnitOrganograms org = OfficeUnitOrganogramsRepository.getInstance().getById(userDTO.organogramID);
				if(org != null){
					vm_maintenanceDTO.requester_post_name_bn = org.designation_bng;
					vm_maintenanceDTO.requester_post_name = org.designation_eng;

					vm_maintenanceDTO.requesterUnitId = org.office_unit_id;

					Office_unitsDTO unit = Office_unitsRepository.getInstance().
							getOffice_unitsDTOByID(org.office_unit_id);

					if(unit != null){
						vm_maintenanceDTO.requester_unit_name = unit.unitNameEng;
						vm_maintenanceDTO.requester_unit_name_bn = unit.unitNameBng;
					}
				}
//
			}

			childValidation(request);


			System.out.println("Done adding  addVm_maintenance dto = " + vm_maintenanceDTO);
			long returnedID = -1;

			if(isPermanentTable == false) //add new row for validation and make the old row outdated
			{
				vm_maintenanceDAO.setIsDeleted(vm_maintenanceDTO.iD, CommonDTO.OUTDATED);
				returnedID = vm_maintenanceDAO.add(vm_maintenanceDTO);
				vm_maintenanceDAO.setIsDeleted(returnedID, CommonDTO.WAITING_FOR_APPROVAL);
			}
			else if(addFlag == true)
			{
				returnedID = vm_maintenanceDAO.manageWriteOperations(vm_maintenanceDTO, SessionConstants.INSERT, -1, userDTO);
			}
			else
			{
				returnedID = vm_maintenanceDAO.manageWriteOperations(vm_maintenanceDTO, SessionConstants.UPDATE, -1, userDTO);
			}





			List<VmMaintenanceItemDTO> vmMaintenanceItemDTOList = createVmMaintenanceItemDTOListByRequest(request);
			if(addFlag == true || isPermanentTable == false) //add or validate
			{
				if(vmMaintenanceItemDTOList != null)
				{
					for(VmMaintenanceItemDTO vmMaintenanceItemDTO: vmMaintenanceItemDTOList)
					{
						vmMaintenanceItemDTO.vmMaintenanceId = vm_maintenanceDTO.iD;
						vmMaintenanceItemDAO.add(vmMaintenanceItemDTO);
					}
				}

			}
			else
			{
				List<Long> childIdsFromRequest = vmMaintenanceItemDAO.getChildIdsFromRequest(request, "vmMaintenanceItem");
				//delete the removed children
				vmMaintenanceItemDAO.deleteChildrenNotInList("vm_maintenance", "vm_maintenance_item", vm_maintenanceDTO.iD, childIdsFromRequest);
				List<Long> childIDsInDatabase = vmMaintenanceItemDAO.getChilIds("vm_maintenance", "vm_maintenance_item", vm_maintenanceDTO.iD);


				if(childIdsFromRequest != null)
				{
					for(int i = 0; i < childIdsFromRequest.size(); i ++)
					{
						Long childIDFromRequest = childIdsFromRequest.get(i);
						if(childIDsInDatabase.contains(childIDFromRequest))
						{
							VmMaintenanceItemDTO vmMaintenanceItemDTO =  createVmMaintenanceItemDTOByRequestAndIndex(request, false, i);
							vmMaintenanceItemDTO.vmMaintenanceId = vm_maintenanceDTO.iD;
							vmMaintenanceItemDAO.update(vmMaintenanceItemDTO);
						}
						else
						{
							VmMaintenanceItemDTO vmMaintenanceItemDTO =  createVmMaintenanceItemDTOByRequestAndIndex(request, true, i);
							vmMaintenanceItemDTO.vmMaintenanceId = vm_maintenanceDTO.iD;
							vmMaintenanceItemDAO.add(vmMaintenanceItemDTO);
						}
					}
				}
				else
				{
					vmMaintenanceItemDAO.deleteChildrenByParent(vm_maintenanceDTO.iD, "vm_maintenance_id");
				}

			}

			if(addFlag == true)
			{
				TaskTypeEnum taskTypeEnum = TaskTypeEnum.VM_MAINTENANCE_REQUEST;

				CreateVm_maintenance_requestApprovalModel model = new CreateVm_maintenance_requestApprovalModel.CreateVm_fuel_requestApprovalModelBuilder()
						.setTaskTypeId(taskTypeEnum.getValue())
						.setVm_fuel_requestId(vm_maintenanceDTO.iD)
						.setRequesterEmployeeRecordId(userDTO.employee_record_id)
						.build();
				new Vm_maintenance_request_approval_mappingDAO().createVm_fuel_requestApproval(model);
			}

			//  start requested amount sum add in parent

			double requestedAmount;
			List<VmMaintenanceItemDTO> itemDTOS = vmMaintenanceItemDAO.
					getVmMaintenanceItemDTOListByVmMaintenanceID(returnedID);

			requestedAmount = itemDTOS.stream().map(i -> i.driverPrice).mapToDouble(i -> i).sum();

			Vm_maintenanceDTO maintenanceDTO = vm_maintenanceDAO.getDTOByID(returnedID);
			maintenanceDTO.requested_total = requestedAmount;
			vm_maintenanceDAO.update(maintenanceDTO);

			//  end requested amount sum add in parent



			apiResponse = ApiResponse.makeSuccessResponse("Vm_maintenanceServlet?actionType=search");





		}
		catch (Exception e)
		{
			e.printStackTrace();
			apiResponse = ApiResponse.makeErrorResponse(e.getMessage());
		}

		PrintWriter pw = response.getWriter();
		pw.write(apiResponse.getJSONString());
		pw.flush();
		pw.close();
	}



	private void childValidation(HttpServletRequest request) throws Exception {
		if(request.getParameterValues("vmMaintenanceItem.iD") != null)
		{
			int vmMaintenanceItemItemNo = request.getParameterValues("vmMaintenanceItem.iD").length;
			String Value = "";
			for(int index=0;index<vmMaintenanceItemItemNo;index++){
				Value = request.getParameterValues("vmMaintenanceItem.vmVehiclePartsType")[index];

				if(Value != null)
				{
					Value = Jsoup.clean(Value,Whitelist.simpleText());
					Long.parseLong(Value);
				} else {
					throw new Exception(" Invalid parts type");
				}


				Value = request.getParameterValues("vmMaintenanceItem.vehicleMaintenanceCat")[index];

				if(Value != null)
				{
					Value = Jsoup.clean(Value,Whitelist.simpleText());
					Integer.parseInt(Value);
				}else {
					throw new Exception(" Invalid maintenance type");
				}


				Value = request.getParameterValues("vmMaintenanceItem.amount")[index];

				if(Value != null)
				{
					Value = Jsoup.clean(Value,Whitelist.simpleText());
					Integer.parseInt(Value);
				}else {
					throw new Exception(" Invalid amount");
				}


				Value = request.getParameterValues("vmMaintenanceItem.driverPrice")[index];
				if(Value != null)
				{
					Value = Jsoup.clean(Value,Whitelist.simpleText());
					Double.parseDouble(Value);
				}else {
					throw new Exception(" Invalid price");
				}
			}


		}
	}


	private List<VmMaintenanceItemDTO> createVmMaintenanceItemDTOListByRequest(HttpServletRequest request) throws Exception{
		List<VmMaintenanceItemDTO> vmMaintenanceItemDTOList = new ArrayList<VmMaintenanceItemDTO>();
		if(request.getParameterValues("vmMaintenanceItem.iD") != null)
		{
			int vmMaintenanceItemItemNo = request.getParameterValues("vmMaintenanceItem.iD").length;


			for(int index=0;index<vmMaintenanceItemItemNo;index++){
				VmMaintenanceItemDTO vmMaintenanceItemDTO = createVmMaintenanceItemDTOByRequestAndIndex(request,true,index);
				vmMaintenanceItemDTOList.add(vmMaintenanceItemDTO);
			}

			return vmMaintenanceItemDTOList;
		}
		return null;
	}


	private VmMaintenanceItemDTO createVmMaintenanceItemDTOByRequestAndIndex(HttpServletRequest request, boolean addFlag,int index) throws Exception{

		VmMaintenanceItemDTO vmMaintenanceItemDTO;
		if(addFlag == true )
		{
			vmMaintenanceItemDTO = new VmMaintenanceItemDTO();
		}
		else
		{
			vmMaintenanceItemDTO = vmMaintenanceItemDAO.getDTOByID(Long.parseLong(request.getParameterValues("vmMaintenanceItem.iD")[index]));
		}
		String path = getServletContext().getRealPath("/img2/");
		SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");



		String Value = "";
		Value = request.getParameterValues("vmMaintenanceItem.vmMaintenanceId")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		vmMaintenanceItemDTO.vmMaintenanceId = Long.parseLong(Value);
		Value = request.getParameterValues("vmMaintenanceItem.vmVehiclePartsType")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		vmMaintenanceItemDTO.vmVehiclePartsType = Long.parseLong(Value);
		Value = request.getParameterValues("vmMaintenanceItem.vehicleMaintenanceCat")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		vmMaintenanceItemDTO.vehicleMaintenanceCat = Integer.parseInt(Value);
		Value = request.getParameterValues("vmMaintenanceItem.amount")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		vmMaintenanceItemDTO.amount = Integer.parseInt(Value);
		Value = request.getParameterValues("vmMaintenanceItem.remarks")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		vmMaintenanceItemDTO.remarks = (Value);
		Value = request.getParameterValues("vmMaintenanceItem.driverPrice")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		vmMaintenanceItemDTO.driverPrice = Double.parseDouble(Value);
		Value = request.getParameterValues("vmMaintenanceItem.searchColumn")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		vmMaintenanceItemDTO.searchColumn = (Value);
		Value = request.getParameterValues("vmMaintenanceItem.insertedByUserId")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		vmMaintenanceItemDTO.insertedByUserId = Long.parseLong(Value);
		Value = request.getParameterValues("vmMaintenanceItem.insertedByOrganogramId")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		vmMaintenanceItemDTO.insertedByOrganogramId = Long.parseLong(Value);
		Value = request.getParameterValues("vmMaintenanceItem.insertionDate")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		vmMaintenanceItemDTO.insertionDate = Long.parseLong(Value);
		return vmMaintenanceItemDTO;

	}




	private void getVm_maintenance(HttpServletRequest request, HttpServletResponse response, long id) throws ServletException, IOException
	{
		System.out.println("in getVm_maintenance");
		Vm_maintenanceDTO vm_maintenanceDTO = null;
		try
		{
			vm_maintenanceDTO = Vm_maintenanceRepository.getInstance().getVm_maintenanceDTOByID(id);
			vm_maintenanceDTO.vmMaintenanceItemDTOList = VmMaintenanceItemRepository.getInstance().
					getVmMaintenanceItemDTOByMaintenanceId(id);
//					(Vm_maintenanceDTO)vm_maintenanceDAO.getDTOByID(id);
			request.setAttribute("ID", vm_maintenanceDTO.iD);
			request.setAttribute("vm_maintenanceDTO",vm_maintenanceDTO);
			request.setAttribute("vm_maintenanceDAO",vm_maintenanceDAO);

			String URL= "";

			String inPlaceEdit = request.getParameter("inplaceedit");
			String inPlaceSubmit = request.getParameter("inplacesubmit");
			String getBodyOnly = request.getParameter("getBodyOnly");

			if(inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase(""))
			{
				URL = "vm_maintenance/vm_maintenanceInPlaceEdit.jsp";
				request.setAttribute("inplaceedit","");
			}
			else if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
			{
				URL = "vm_maintenance/vm_maintenanceSearchRow.jsp";
				request.setAttribute("inplacesubmit","");
			}
			else
			{
				if(getBodyOnly  != null && !getBodyOnly.equalsIgnoreCase(""))
				{
					URL = "vm_maintenance/vm_maintenanceEditBody.jsp?actionType=edit";
				}
				else
				{
					URL = "vm_maintenance/vm_maintenanceEdit.jsp?actionType=edit";
				}
			}

			RequestDispatcher rd = request.getRequestDispatcher(URL);
			rd.forward(request, response);
		}
		catch (NumberFormatException e)
		{
			e.printStackTrace();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}


	private void getVm_maintenance(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		getVm_maintenance(request, response, Long.parseLong(request.getParameter("ID")));
	}

	private void searchVm_maintenance(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException
	{
		System.out.println("in  searchVm_maintenance 1");
		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String ajax = request.getParameter("ajax");
		boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);

        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
			SessionConstants.NAV_VM_MAINTENANCE,
			request,
			vm_maintenanceDAO,
			SessionConstants.VIEW_VM_MAINTENANCE,
			SessionConstants.SEARCH_VM_MAINTENANCE,
			tableName,
			isPermanent,
			userDTO,
			filter,
			true);
        try
        {
			System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        }
        catch(Exception e)
        {
			System.out.println("failed to dojob" + e);
        }

		request.setAttribute("vm_maintenanceDAO",vm_maintenanceDAO);
        RequestDispatcher rd;
        if(!isPermanent)
        {
        	if(hasAjax == false)
	        {
	        	System.out.println("Going to vm_maintenance/vm_maintenanceApproval.jsp");
	        	rd = request.getRequestDispatcher("vm_maintenance/vm_maintenanceApproval.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to vm_maintenance/vm_maintenanceApprovalForm.jsp");
	        	rd = request.getRequestDispatcher("vm_maintenance/vm_maintenanceApprovalForm.jsp");
	        }
        }
        else
        {
	        if(hasAjax == false)
	        {
	        	System.out.println("Going to vm_maintenance/vm_maintenanceSearch.jsp");
	        	rd = request.getRequestDispatcher("vm_maintenance/vm_maintenanceSearch.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to vm_maintenance/vm_maintenanceSearchForm.jsp");
	        	rd = request.getRequestDispatcher("vm_maintenance/vm_maintenanceSearchForm.jsp");
	        }
        }
		rd.forward(request, response);
	}

	private void searchVm_maintenanceForPayment(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException
	{
		System.out.println("in  searchVm_maintenance 1");
		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String ajax = request.getParameter("ajax");
		boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);

		RecordNavigationManager4 rnManager = new RecordNavigationManager4(
				SessionConstants.NAV_VM_MAINTENANCE,
				request,
				vm_maintenanceDAO,
				SessionConstants.VIEW_VM_MAINTENANCE,
				SessionConstants.SEARCH_VM_MAINTENANCE,
				tableName,
				isPermanent,
				userDTO,
				filter,
				true);
		try
		{
			System.out.println("trying to dojob");
			rnManager.doJob(loginDTO);
		}
		catch(Exception e)
		{
			System.out.println("failed to dojob" + e);
		}

		request.setAttribute("vm_maintenanceDAO",vm_maintenanceDAO);
		RequestDispatcher rd;
//		if(!isPermanent)
//		{
//			if(hasAjax == false)
//			{
//				System.out.println("Going to vm_maintenance/vm_maintenanceApproval.jsp");
//				rd = request.getRequestDispatcher("vm_maintenance/vm_maintenanceApproval.jsp");
//			}
//			else
//			{
//				System.out.println("Going to vm_maintenance/vm_maintenanceApprovalForm.jsp");
//				rd = request.getRequestDispatcher("vm_maintenance/vm_maintenanceApprovalForm.jsp");
//			}
//		}
//		else
//		{
			if(hasAjax == false)
			{
				System.out.println("Going to vm_maintenance_payment/vm_maintenanceSearch.jsp");
				rd = request.getRequestDispatcher("vm_maintenance_payment/vm_maintenanceSearch.jsp");
			}
			else
			{
				System.out.println("Going to vm_maintenance_payment/vm_maintenanceSearchForm.jsp");
				rd = request.getRequestDispatcher("vm_maintenance_payment/vm_maintenanceSearchForm.jsp");
			}
//		}
		rd.forward(request, response);
	}

	private void getBillRegisterData(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		String officeIds = request.getParameter("officeIds");
		List<String> officeIdsInArray = Arrays.asList(officeIds.split(","));
		List<Long> officeIdsInLong = officeIdsInArray.stream().map(s -> Long.parseLong(s))
				.collect(Collectors.toList());
		Long fiscalYearId = Long.parseLong(request.getParameter("fiscalYearIds"));
		System.out.println(officeIds);

		String Language = request.getParameter("language");

		List<Vm_vehicle_office_assignmentDTO> office_assignmentDTOS = new Vm_vehicle_office_assignmentDAO().
				getDTOsByOfficeIds(officeIdsInLong);
		Set<Long> vehicleIdsSet = office_assignmentDTOS.stream().map(i -> i.vehicleId).collect(Collectors.toSet());
//		List<Long> vehicleIdsList = vehicleIdsSet.stream().collect(Collectors.toList());

		List<Vm_vehicleDTO> vm_vehicleDTOS = new ArrayList<>();
		vehicleIdsSet.forEach(i -> {
			Vm_vehicleDTO vehicleDTO = Vm_vehicleRepository.getInstance().getVm_vehicleDTOByID(i);
			if(vehicleDTO != null){
				vm_vehicleDTOS.add(vehicleDTO);
			}
		});

//				(List<Vm_vehicleDTO>)
//				new Vm_vehicleDAO().getDTOs(vehicleIdsList);
//		System.out.println(vm_vehicleDTOS);
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

		List<BillRegisterItemDTO> billRegisterItemDTOS = new ArrayList<>();
		for(Vm_vehicleDTO vm_vehicleDTO: vm_vehicleDTOS){

			List<Vm_maintenanceDTO> maintenanceDTOS = new Vm_maintenanceDAO().
					getAllVm_maintenanceByVehicleIdAndFiscalYear(vm_vehicleDTO.iD, fiscalYearId).
					stream().filter(i -> i.status == 6).collect(Collectors.toList());

			double totalCost = 0.0;
			for(Vm_maintenanceDTO maintenanceDTO: maintenanceDTOS){
				totalCost += maintenanceDTO.totalWithVat;

				BillRegisterItemDTO billRegisterItemDTO = new BillRegisterItemDTO();
				billRegisterItemDTO.cost = maintenanceDTO.totalWithVat;
				billRegisterItemDTO.sarokNumber = maintenanceDTO.sarokNumber;
				billRegisterItemDTO.vehicleNumber = vm_vehicleDTO.regNo;
				billRegisterItemDTO.totalCost = (int)(Math.round(totalCost * 100))/100.0;
				String formatted_purchaseDate = simpleDateFormat.format(new Date(Long.parseLong
						(maintenanceDTO.paymentReceivedDate + "")));
				billRegisterItemDTO.paymentDate = Utils.getDigits(formatted_purchaseDate, Language);
				billRegisterItemDTOS.add(billRegisterItemDTO);


			}

		}

//		System.out.println(billRegisterItemDTOS);
		request.setAttribute("billRegisterItemDTOS",billRegisterItemDTOS);

		RequestDispatcher rd = request.getRequestDispatcher
				("vm_bill_register_maintenance/SearchForm.jsp");
		rd.forward(request, response);
	}



}

