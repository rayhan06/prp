package vm_maintenance;

public class BillRegisterItemDTO
{

	public String sarokNumber = "";
	public String paymentDate = "";
	public String vehicleNumber = "";
	public Double allocated = 0.0;
	public Double totalCost = 0.0;
	public Double cost = 0.0;
	public Double remaining = 0.0;

	
    @Override
	public String toString() {
            return "BillRegisterItemDTO[" +
            " sarokNumber = " + sarokNumber +
            " paymentDate = " + paymentDate +
            " vehicleNumber = " + vehicleNumber +
            " allocated = " + allocated +
            " totalCost = " + totalCost +
            " cost = " + cost +
            " remaining = " + remaining +
            "]";
    }

}