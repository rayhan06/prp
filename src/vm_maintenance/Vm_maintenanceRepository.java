package vm_maintenance;

import com.google.gson.Gson;
import org.apache.log4j.Logger;
import repository.Repository;
import repository.RepositoryManager;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;


public class Vm_maintenanceRepository implements Repository {
	Vm_maintenanceDAO vm_maintenanceDAO = new Vm_maintenanceDAO();
	
	public void setDAO(Vm_maintenanceDAO vm_maintenanceDAO)
	{
		this.vm_maintenanceDAO = vm_maintenanceDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Vm_maintenanceRepository.class);
	Map<Long, Vm_maintenanceDTO>mapOfVm_maintenanceDTOToiD;
	Map<Long, Set<Vm_maintenanceDTO>>mapOfVm_maintenanceDTOTovehicleId;

	Gson gson = new Gson();

//	Map<Integer, Set<Vm_maintenanceDTO> >mapOfVm_maintenanceDTOTovehicleTypeCat;
//	Map<Integer, Set<Vm_maintenanceDTO> >mapOfVm_maintenanceDTOTostatus;

//	Map<Long, Set<Vm_maintenanceDTO> >mapOfVm_maintenanceDTOTorequesterPostId;
//	Map<Long, Set<Vm_maintenanceDTO> >mapOfVm_maintenanceDTOTorequesterUnitId;
//	Map<Long, Set<Vm_maintenanceDTO> >mapOfVm_maintenanceDTOTorequesterEmployeeRecordId;
//	Map<Long, Set<Vm_maintenanceDTO> >mapOfVm_maintenanceDTOTovehicleDriverAssignmentId;
//	Map<Long, Set<Vm_maintenanceDTO> >mapOfVm_maintenanceDTOToapplicationDate;
//	Map<Long, Set<Vm_maintenanceDTO> >mapOfVm_maintenanceDTOTolastMaintenanceDate;
//	Map<String, Set<Vm_maintenanceDTO> >mapOfVm_maintenanceDTOTovehicleOfficeName;
//	Map<String, Set<Vm_maintenanceDTO> >mapOfVm_maintenanceDTOTovehicleOfficeNameBn;
//	Map<Boolean, Set<Vm_maintenanceDTO> >mapOfVm_maintenanceDTOToisChecked;
//	Map<String, Set<Vm_maintenanceDTO> >mapOfVm_maintenanceDTOTomechanicRemarks;
//	Map<Long, Set<Vm_maintenanceDTO> >mapOfVm_maintenanceDTOTomechanicApproveDate;
//	Map<Long, Set<Vm_maintenanceDTO> >mapOfVm_maintenanceDTOToaoApproveDate;
//	Map<Double, Set<Vm_maintenanceDTO> >mapOfVm_maintenanceDTOTovatPercentage;
//	Map<Double, Set<Vm_maintenanceDTO> >mapOfVm_maintenanceDTOTototalWithoutVat;
//	Map<Double, Set<Vm_maintenanceDTO> >mapOfVm_maintenanceDTOTototalWithVat;
//	Map<Double, Set<Vm_maintenanceDTO> >mapOfVm_maintenanceDTOTototalVat;
//	Map<Long, Set<Vm_maintenanceDTO> >mapOfVm_maintenanceDTOTopaymentReceivedDate;
//	Map<Long, Set<Vm_maintenanceDTO> >mapOfVm_maintenanceDTOTofiscalYearId;
//	Map<String, Set<Vm_maintenanceDTO> >mapOfVm_maintenanceDTOTosarokNumber;
//	Map<Long, Set<Vm_maintenanceDTO> >mapOfVm_maintenanceDTOTofilesDropzone;
//	Map<String, Set<Vm_maintenanceDTO> >mapOfVm_maintenanceDTOTopaymentRemarks;
//	Map<String, Set<Vm_maintenanceDTO> >mapOfVm_maintenanceDTOTosearchColumn;
//	Map<Long, Set<Vm_maintenanceDTO> >mapOfVm_maintenanceDTOToinsertedByUserId;
//	Map<Long, Set<Vm_maintenanceDTO> >mapOfVm_maintenanceDTOToinsertedByOrganogramId;
//	Map<Long, Set<Vm_maintenanceDTO> >mapOfVm_maintenanceDTOToinsertionDate;
//	Map<Long, Set<Vm_maintenanceDTO> >mapOfVm_maintenanceDTOTolastModificationTime;


	static Vm_maintenanceRepository instance = null;  
	private Vm_maintenanceRepository(){
		mapOfVm_maintenanceDTOToiD = new ConcurrentHashMap<>();
		mapOfVm_maintenanceDTOTovehicleId = new ConcurrentHashMap<>();
//		mapOfVm_maintenanceDTOTovehicleTypeCat = new ConcurrentHashMap<>();
//		mapOfVm_maintenanceDTOTostatus = new ConcurrentHashMap<>();
//		mapOfVm_maintenanceDTOTovehicleId = new ConcurrentHashMap<>();
//		mapOfVm_maintenanceDTOTorequesterPostId = new ConcurrentHashMap<>();
//		mapOfVm_maintenanceDTOTorequesterUnitId = new ConcurrentHashMap<>();
//		mapOfVm_maintenanceDTOTorequesterEmployeeRecordId = new ConcurrentHashMap<>();
//		mapOfVm_maintenanceDTOTovehicleDriverAssignmentId = new ConcurrentHashMap<>();
//		mapOfVm_maintenanceDTOToapplicationDate = new ConcurrentHashMap<>();
//		mapOfVm_maintenanceDTOTolastMaintenanceDate = new ConcurrentHashMap<>();
//		mapOfVm_maintenanceDTOTovehicleOfficeName = new ConcurrentHashMap<>();
//		mapOfVm_maintenanceDTOTovehicleOfficeNameBn = new ConcurrentHashMap<>();
//		mapOfVm_maintenanceDTOToisChecked = new ConcurrentHashMap<>();
//		mapOfVm_maintenanceDTOTomechanicRemarks = new ConcurrentHashMap<>();
//		mapOfVm_maintenanceDTOTomechanicApproveDate = new ConcurrentHashMap<>();
//		mapOfVm_maintenanceDTOToaoApproveDate = new ConcurrentHashMap<>();
//		mapOfVm_maintenanceDTOTovatPercentage = new ConcurrentHashMap<>();
//		mapOfVm_maintenanceDTOTototalWithoutVat = new ConcurrentHashMap<>();
//		mapOfVm_maintenanceDTOTototalWithVat = new ConcurrentHashMap<>();
//		mapOfVm_maintenanceDTOTototalVat = new ConcurrentHashMap<>();
//		mapOfVm_maintenanceDTOTopaymentReceivedDate = new ConcurrentHashMap<>();
//		mapOfVm_maintenanceDTOTofiscalYearId = new ConcurrentHashMap<>();
//		mapOfVm_maintenanceDTOTosarokNumber = new ConcurrentHashMap<>();
//		mapOfVm_maintenanceDTOTofilesDropzone = new ConcurrentHashMap<>();
//		mapOfVm_maintenanceDTOTopaymentRemarks = new ConcurrentHashMap<>();
//		mapOfVm_maintenanceDTOTosearchColumn = new ConcurrentHashMap<>();
//		mapOfVm_maintenanceDTOToinsertedByUserId = new ConcurrentHashMap<>();
//		mapOfVm_maintenanceDTOToinsertedByOrganogramId = new ConcurrentHashMap<>();
//		mapOfVm_maintenanceDTOToinsertionDate = new ConcurrentHashMap<>();
//		mapOfVm_maintenanceDTOTolastModificationTime = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Vm_maintenanceRepository getInstance(){
		if (instance == null){
			instance = new Vm_maintenanceRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(vm_maintenanceDAO == null)
		{
			return;
		}
		try {
			List<Vm_maintenanceDTO> vm_maintenanceDTOs = (List<Vm_maintenanceDTO>)
					vm_maintenanceDAO.getAll(reloadAll);
			for(Vm_maintenanceDTO vm_maintenanceDTO : vm_maintenanceDTOs) {
				Vm_maintenanceDTO oldVm_maintenanceDTO = getVm_maintenanceDTOByIDWithoutClone(vm_maintenanceDTO.iD);
				if( oldVm_maintenanceDTO != null ) {
					mapOfVm_maintenanceDTOToiD.remove(oldVm_maintenanceDTO.iD);
				
//					if(mapOfVm_maintenanceDTOTovehicleTypeCat.containsKey(oldVm_maintenanceDTO.vehicleTypeCat)) {
//						mapOfVm_maintenanceDTOTovehicleTypeCat.get(oldVm_maintenanceDTO.vehicleTypeCat).remove(oldVm_maintenanceDTO);
//					}
//					if(mapOfVm_maintenanceDTOTovehicleTypeCat.get(oldVm_maintenanceDTO.vehicleTypeCat).isEmpty()) {
//						mapOfVm_maintenanceDTOTovehicleTypeCat.remove(oldVm_maintenanceDTO.vehicleTypeCat);
//					}
//
//					if(mapOfVm_maintenanceDTOTostatus.containsKey(oldVm_maintenanceDTO.status)) {
//						mapOfVm_maintenanceDTOTostatus.get(oldVm_maintenanceDTO.status).remove(oldVm_maintenanceDTO);
//					}
//					if(mapOfVm_maintenanceDTOTostatus.get(oldVm_maintenanceDTO.status).isEmpty()) {
//						mapOfVm_maintenanceDTOTostatus.remove(oldVm_maintenanceDTO.status);
//					}
//
					if(mapOfVm_maintenanceDTOTovehicleId.containsKey(oldVm_maintenanceDTO.vehicleId)) {
						mapOfVm_maintenanceDTOTovehicleId.get(oldVm_maintenanceDTO.vehicleId).remove(oldVm_maintenanceDTO);
					}
					if(mapOfVm_maintenanceDTOTovehicleId.get(oldVm_maintenanceDTO.vehicleId).isEmpty()) {
						mapOfVm_maintenanceDTOTovehicleId.remove(oldVm_maintenanceDTO.vehicleId);
					}
//
//					if(mapOfVm_maintenanceDTOTorequesterPostId.containsKey(oldVm_maintenanceDTO.requesterPostId)) {
//						mapOfVm_maintenanceDTOTorequesterPostId.get(oldVm_maintenanceDTO.requesterPostId).remove(oldVm_maintenanceDTO);
//					}
//					if(mapOfVm_maintenanceDTOTorequesterPostId.get(oldVm_maintenanceDTO.requesterPostId).isEmpty()) {
//						mapOfVm_maintenanceDTOTorequesterPostId.remove(oldVm_maintenanceDTO.requesterPostId);
//					}
//
//					if(mapOfVm_maintenanceDTOTorequesterUnitId.containsKey(oldVm_maintenanceDTO.requesterUnitId)) {
//						mapOfVm_maintenanceDTOTorequesterUnitId.get(oldVm_maintenanceDTO.requesterUnitId).remove(oldVm_maintenanceDTO);
//					}
//					if(mapOfVm_maintenanceDTOTorequesterUnitId.get(oldVm_maintenanceDTO.requesterUnitId).isEmpty()) {
//						mapOfVm_maintenanceDTOTorequesterUnitId.remove(oldVm_maintenanceDTO.requesterUnitId);
//					}
//
//					if(mapOfVm_maintenanceDTOTorequesterEmployeeRecordId.containsKey(oldVm_maintenanceDTO.requesterEmployeeRecordId)) {
//						mapOfVm_maintenanceDTOTorequesterEmployeeRecordId.get(oldVm_maintenanceDTO.requesterEmployeeRecordId).remove(oldVm_maintenanceDTO);
//					}
//					if(mapOfVm_maintenanceDTOTorequesterEmployeeRecordId.get(oldVm_maintenanceDTO.requesterEmployeeRecordId).isEmpty()) {
//						mapOfVm_maintenanceDTOTorequesterEmployeeRecordId.remove(oldVm_maintenanceDTO.requesterEmployeeRecordId);
//					}
//
//					if(mapOfVm_maintenanceDTOTovehicleDriverAssignmentId.containsKey(oldVm_maintenanceDTO.vehicleDriverAssignmentId)) {
//						mapOfVm_maintenanceDTOTovehicleDriverAssignmentId.get(oldVm_maintenanceDTO.vehicleDriverAssignmentId).remove(oldVm_maintenanceDTO);
//					}
//					if(mapOfVm_maintenanceDTOTovehicleDriverAssignmentId.get(oldVm_maintenanceDTO.vehicleDriverAssignmentId).isEmpty()) {
//						mapOfVm_maintenanceDTOTovehicleDriverAssignmentId.remove(oldVm_maintenanceDTO.vehicleDriverAssignmentId);
//					}
//
//					if(mapOfVm_maintenanceDTOToapplicationDate.containsKey(oldVm_maintenanceDTO.applicationDate)) {
//						mapOfVm_maintenanceDTOToapplicationDate.get(oldVm_maintenanceDTO.applicationDate).remove(oldVm_maintenanceDTO);
//					}
//					if(mapOfVm_maintenanceDTOToapplicationDate.get(oldVm_maintenanceDTO.applicationDate).isEmpty()) {
//						mapOfVm_maintenanceDTOToapplicationDate.remove(oldVm_maintenanceDTO.applicationDate);
//					}
//
//					if(mapOfVm_maintenanceDTOTolastMaintenanceDate.containsKey(oldVm_maintenanceDTO.lastMaintenanceDate)) {
//						mapOfVm_maintenanceDTOTolastMaintenanceDate.get(oldVm_maintenanceDTO.lastMaintenanceDate).remove(oldVm_maintenanceDTO);
//					}
//					if(mapOfVm_maintenanceDTOTolastMaintenanceDate.get(oldVm_maintenanceDTO.lastMaintenanceDate).isEmpty()) {
//						mapOfVm_maintenanceDTOTolastMaintenanceDate.remove(oldVm_maintenanceDTO.lastMaintenanceDate);
//					}
//
//					if(mapOfVm_maintenanceDTOTovehicleOfficeName.containsKey(oldVm_maintenanceDTO.vehicleOfficeName)) {
//						mapOfVm_maintenanceDTOTovehicleOfficeName.get(oldVm_maintenanceDTO.vehicleOfficeName).remove(oldVm_maintenanceDTO);
//					}
//					if(mapOfVm_maintenanceDTOTovehicleOfficeName.get(oldVm_maintenanceDTO.vehicleOfficeName).isEmpty()) {
//						mapOfVm_maintenanceDTOTovehicleOfficeName.remove(oldVm_maintenanceDTO.vehicleOfficeName);
//					}
//
//					if(mapOfVm_maintenanceDTOTovehicleOfficeNameBn.containsKey(oldVm_maintenanceDTO.vehicleOfficeNameBn)) {
//						mapOfVm_maintenanceDTOTovehicleOfficeNameBn.get(oldVm_maintenanceDTO.vehicleOfficeNameBn).remove(oldVm_maintenanceDTO);
//					}
//					if(mapOfVm_maintenanceDTOTovehicleOfficeNameBn.get(oldVm_maintenanceDTO.vehicleOfficeNameBn).isEmpty()) {
//						mapOfVm_maintenanceDTOTovehicleOfficeNameBn.remove(oldVm_maintenanceDTO.vehicleOfficeNameBn);
//					}
//
//					if(mapOfVm_maintenanceDTOToisChecked.containsKey(oldVm_maintenanceDTO.isChecked)) {
//						mapOfVm_maintenanceDTOToisChecked.get(oldVm_maintenanceDTO.isChecked).remove(oldVm_maintenanceDTO);
//					}
//					if(mapOfVm_maintenanceDTOToisChecked.get(oldVm_maintenanceDTO.isChecked).isEmpty()) {
//						mapOfVm_maintenanceDTOToisChecked.remove(oldVm_maintenanceDTO.isChecked);
//					}
//
//					if(mapOfVm_maintenanceDTOTomechanicRemarks.containsKey(oldVm_maintenanceDTO.mechanicRemarks)) {
//						mapOfVm_maintenanceDTOTomechanicRemarks.get(oldVm_maintenanceDTO.mechanicRemarks).remove(oldVm_maintenanceDTO);
//					}
//					if(mapOfVm_maintenanceDTOTomechanicRemarks.get(oldVm_maintenanceDTO.mechanicRemarks).isEmpty()) {
//						mapOfVm_maintenanceDTOTomechanicRemarks.remove(oldVm_maintenanceDTO.mechanicRemarks);
//					}
//
//					if(mapOfVm_maintenanceDTOTomechanicApproveDate.containsKey(oldVm_maintenanceDTO.mechanicApproveDate)) {
//						mapOfVm_maintenanceDTOTomechanicApproveDate.get(oldVm_maintenanceDTO.mechanicApproveDate).remove(oldVm_maintenanceDTO);
//					}
//					if(mapOfVm_maintenanceDTOTomechanicApproveDate.get(oldVm_maintenanceDTO.mechanicApproveDate).isEmpty()) {
//						mapOfVm_maintenanceDTOTomechanicApproveDate.remove(oldVm_maintenanceDTO.mechanicApproveDate);
//					}
//
//					if(mapOfVm_maintenanceDTOToaoApproveDate.containsKey(oldVm_maintenanceDTO.aoApproveDate)) {
//						mapOfVm_maintenanceDTOToaoApproveDate.get(oldVm_maintenanceDTO.aoApproveDate).remove(oldVm_maintenanceDTO);
//					}
//					if(mapOfVm_maintenanceDTOToaoApproveDate.get(oldVm_maintenanceDTO.aoApproveDate).isEmpty()) {
//						mapOfVm_maintenanceDTOToaoApproveDate.remove(oldVm_maintenanceDTO.aoApproveDate);
//					}
//
//					if(mapOfVm_maintenanceDTOTovatPercentage.containsKey(oldVm_maintenanceDTO.vatPercentage)) {
//						mapOfVm_maintenanceDTOTovatPercentage.get(oldVm_maintenanceDTO.vatPercentage).remove(oldVm_maintenanceDTO);
//					}
//					if(mapOfVm_maintenanceDTOTovatPercentage.get(oldVm_maintenanceDTO.vatPercentage).isEmpty()) {
//						mapOfVm_maintenanceDTOTovatPercentage.remove(oldVm_maintenanceDTO.vatPercentage);
//					}
//
//					if(mapOfVm_maintenanceDTOTototalWithoutVat.containsKey(oldVm_maintenanceDTO.totalWithoutVat)) {
//						mapOfVm_maintenanceDTOTototalWithoutVat.get(oldVm_maintenanceDTO.totalWithoutVat).remove(oldVm_maintenanceDTO);
//					}
//					if(mapOfVm_maintenanceDTOTototalWithoutVat.get(oldVm_maintenanceDTO.totalWithoutVat).isEmpty()) {
//						mapOfVm_maintenanceDTOTototalWithoutVat.remove(oldVm_maintenanceDTO.totalWithoutVat);
//					}
//
//					if(mapOfVm_maintenanceDTOTototalWithVat.containsKey(oldVm_maintenanceDTO.totalWithVat)) {
//						mapOfVm_maintenanceDTOTototalWithVat.get(oldVm_maintenanceDTO.totalWithVat).remove(oldVm_maintenanceDTO);
//					}
//					if(mapOfVm_maintenanceDTOTototalWithVat.get(oldVm_maintenanceDTO.totalWithVat).isEmpty()) {
//						mapOfVm_maintenanceDTOTototalWithVat.remove(oldVm_maintenanceDTO.totalWithVat);
//					}
//
//					if(mapOfVm_maintenanceDTOTototalVat.containsKey(oldVm_maintenanceDTO.totalVat)) {
//						mapOfVm_maintenanceDTOTototalVat.get(oldVm_maintenanceDTO.totalVat).remove(oldVm_maintenanceDTO);
//					}
//					if(mapOfVm_maintenanceDTOTototalVat.get(oldVm_maintenanceDTO.totalVat).isEmpty()) {
//						mapOfVm_maintenanceDTOTototalVat.remove(oldVm_maintenanceDTO.totalVat);
//					}
//
//					if(mapOfVm_maintenanceDTOTopaymentReceivedDate.containsKey(oldVm_maintenanceDTO.paymentReceivedDate)) {
//						mapOfVm_maintenanceDTOTopaymentReceivedDate.get(oldVm_maintenanceDTO.paymentReceivedDate).remove(oldVm_maintenanceDTO);
//					}
//					if(mapOfVm_maintenanceDTOTopaymentReceivedDate.get(oldVm_maintenanceDTO.paymentReceivedDate).isEmpty()) {
//						mapOfVm_maintenanceDTOTopaymentReceivedDate.remove(oldVm_maintenanceDTO.paymentReceivedDate);
//					}
//
//					if(mapOfVm_maintenanceDTOTofiscalYearId.containsKey(oldVm_maintenanceDTO.fiscalYearId)) {
//						mapOfVm_maintenanceDTOTofiscalYearId.get(oldVm_maintenanceDTO.fiscalYearId).remove(oldVm_maintenanceDTO);
//					}
//					if(mapOfVm_maintenanceDTOTofiscalYearId.get(oldVm_maintenanceDTO.fiscalYearId).isEmpty()) {
//						mapOfVm_maintenanceDTOTofiscalYearId.remove(oldVm_maintenanceDTO.fiscalYearId);
//					}
//
//					if(mapOfVm_maintenanceDTOTosarokNumber.containsKey(oldVm_maintenanceDTO.sarokNumber)) {
//						mapOfVm_maintenanceDTOTosarokNumber.get(oldVm_maintenanceDTO.sarokNumber).remove(oldVm_maintenanceDTO);
//					}
//					if(mapOfVm_maintenanceDTOTosarokNumber.get(oldVm_maintenanceDTO.sarokNumber).isEmpty()) {
//						mapOfVm_maintenanceDTOTosarokNumber.remove(oldVm_maintenanceDTO.sarokNumber);
//					}
//
//					if(mapOfVm_maintenanceDTOTofilesDropzone.containsKey(oldVm_maintenanceDTO.filesDropzone)) {
//						mapOfVm_maintenanceDTOTofilesDropzone.get(oldVm_maintenanceDTO.filesDropzone).remove(oldVm_maintenanceDTO);
//					}
//					if(mapOfVm_maintenanceDTOTofilesDropzone.get(oldVm_maintenanceDTO.filesDropzone).isEmpty()) {
//						mapOfVm_maintenanceDTOTofilesDropzone.remove(oldVm_maintenanceDTO.filesDropzone);
//					}
//
//					if(mapOfVm_maintenanceDTOTopaymentRemarks.containsKey(oldVm_maintenanceDTO.paymentRemarks)) {
//						mapOfVm_maintenanceDTOTopaymentRemarks.get(oldVm_maintenanceDTO.paymentRemarks).remove(oldVm_maintenanceDTO);
//					}
//					if(mapOfVm_maintenanceDTOTopaymentRemarks.get(oldVm_maintenanceDTO.paymentRemarks).isEmpty()) {
//						mapOfVm_maintenanceDTOTopaymentRemarks.remove(oldVm_maintenanceDTO.paymentRemarks);
//					}
//
//					if(mapOfVm_maintenanceDTOTosearchColumn.containsKey(oldVm_maintenanceDTO.searchColumn)) {
//						mapOfVm_maintenanceDTOTosearchColumn.get(oldVm_maintenanceDTO.searchColumn).remove(oldVm_maintenanceDTO);
//					}
//					if(mapOfVm_maintenanceDTOTosearchColumn.get(oldVm_maintenanceDTO.searchColumn).isEmpty()) {
//						mapOfVm_maintenanceDTOTosearchColumn.remove(oldVm_maintenanceDTO.searchColumn);
//					}
//
//					if(mapOfVm_maintenanceDTOToinsertedByUserId.containsKey(oldVm_maintenanceDTO.insertedByUserId)) {
//						mapOfVm_maintenanceDTOToinsertedByUserId.get(oldVm_maintenanceDTO.insertedByUserId).remove(oldVm_maintenanceDTO);
//					}
//					if(mapOfVm_maintenanceDTOToinsertedByUserId.get(oldVm_maintenanceDTO.insertedByUserId).isEmpty()) {
//						mapOfVm_maintenanceDTOToinsertedByUserId.remove(oldVm_maintenanceDTO.insertedByUserId);
//					}
//
//					if(mapOfVm_maintenanceDTOToinsertedByOrganogramId.containsKey(oldVm_maintenanceDTO.insertedByOrganogramId)) {
//						mapOfVm_maintenanceDTOToinsertedByOrganogramId.get(oldVm_maintenanceDTO.insertedByOrganogramId).remove(oldVm_maintenanceDTO);
//					}
//					if(mapOfVm_maintenanceDTOToinsertedByOrganogramId.get(oldVm_maintenanceDTO.insertedByOrganogramId).isEmpty()) {
//						mapOfVm_maintenanceDTOToinsertedByOrganogramId.remove(oldVm_maintenanceDTO.insertedByOrganogramId);
//					}
//
//					if(mapOfVm_maintenanceDTOToinsertionDate.containsKey(oldVm_maintenanceDTO.insertionDate)) {
//						mapOfVm_maintenanceDTOToinsertionDate.get(oldVm_maintenanceDTO.insertionDate).remove(oldVm_maintenanceDTO);
//					}
//					if(mapOfVm_maintenanceDTOToinsertionDate.get(oldVm_maintenanceDTO.insertionDate).isEmpty()) {
//						mapOfVm_maintenanceDTOToinsertionDate.remove(oldVm_maintenanceDTO.insertionDate);
//					}
//
//					if(mapOfVm_maintenanceDTOTolastModificationTime.containsKey(oldVm_maintenanceDTO.lastModificationTime)) {
//						mapOfVm_maintenanceDTOTolastModificationTime.get(oldVm_maintenanceDTO.lastModificationTime).remove(oldVm_maintenanceDTO);
//					}
//					if(mapOfVm_maintenanceDTOTolastModificationTime.get(oldVm_maintenanceDTO.lastModificationTime).isEmpty()) {
//						mapOfVm_maintenanceDTOTolastModificationTime.remove(oldVm_maintenanceDTO.lastModificationTime);
//					}
					
					
				}
				if(vm_maintenanceDTO.isDeleted == 0) 
				{
					
					mapOfVm_maintenanceDTOToiD.put(vm_maintenanceDTO.iD, vm_maintenanceDTO);
				
//					if( ! mapOfVm_maintenanceDTOTovehicleTypeCat.containsKey(vm_maintenanceDTO.vehicleTypeCat)) {
//						mapOfVm_maintenanceDTOTovehicleTypeCat.put(vm_maintenanceDTO.vehicleTypeCat, new HashSet<>());
//					}
//					mapOfVm_maintenanceDTOTovehicleTypeCat.get(vm_maintenanceDTO.vehicleTypeCat).add(vm_maintenanceDTO);
//
//					if( ! mapOfVm_maintenanceDTOTostatus.containsKey(vm_maintenanceDTO.status)) {
//						mapOfVm_maintenanceDTOTostatus.put(vm_maintenanceDTO.status, new HashSet<>());
//					}
//					mapOfVm_maintenanceDTOTostatus.get(vm_maintenanceDTO.status).add(vm_maintenanceDTO);
//
					if( ! mapOfVm_maintenanceDTOTovehicleId.containsKey(vm_maintenanceDTO.vehicleId)) {
						mapOfVm_maintenanceDTOTovehicleId.put(vm_maintenanceDTO.vehicleId, new HashSet<>());
					}
					mapOfVm_maintenanceDTOTovehicleId.get(vm_maintenanceDTO.vehicleId).add(vm_maintenanceDTO);
//
//					if( ! mapOfVm_maintenanceDTOTorequesterPostId.containsKey(vm_maintenanceDTO.requesterPostId)) {
//						mapOfVm_maintenanceDTOTorequesterPostId.put(vm_maintenanceDTO.requesterPostId, new HashSet<>());
//					}
//					mapOfVm_maintenanceDTOTorequesterPostId.get(vm_maintenanceDTO.requesterPostId).add(vm_maintenanceDTO);
//
//					if( ! mapOfVm_maintenanceDTOTorequesterUnitId.containsKey(vm_maintenanceDTO.requesterUnitId)) {
//						mapOfVm_maintenanceDTOTorequesterUnitId.put(vm_maintenanceDTO.requesterUnitId, new HashSet<>());
//					}
//					mapOfVm_maintenanceDTOTorequesterUnitId.get(vm_maintenanceDTO.requesterUnitId).add(vm_maintenanceDTO);
//
//					if( ! mapOfVm_maintenanceDTOTorequesterEmployeeRecordId.containsKey(vm_maintenanceDTO.requesterEmployeeRecordId)) {
//						mapOfVm_maintenanceDTOTorequesterEmployeeRecordId.put(vm_maintenanceDTO.requesterEmployeeRecordId, new HashSet<>());
//					}
//					mapOfVm_maintenanceDTOTorequesterEmployeeRecordId.get(vm_maintenanceDTO.requesterEmployeeRecordId).add(vm_maintenanceDTO);
//
//					if( ! mapOfVm_maintenanceDTOTovehicleDriverAssignmentId.containsKey(vm_maintenanceDTO.vehicleDriverAssignmentId)) {
//						mapOfVm_maintenanceDTOTovehicleDriverAssignmentId.put(vm_maintenanceDTO.vehicleDriverAssignmentId, new HashSet<>());
//					}
//					mapOfVm_maintenanceDTOTovehicleDriverAssignmentId.get(vm_maintenanceDTO.vehicleDriverAssignmentId).add(vm_maintenanceDTO);
//
//					if( ! mapOfVm_maintenanceDTOToapplicationDate.containsKey(vm_maintenanceDTO.applicationDate)) {
//						mapOfVm_maintenanceDTOToapplicationDate.put(vm_maintenanceDTO.applicationDate, new HashSet<>());
//					}
//					mapOfVm_maintenanceDTOToapplicationDate.get(vm_maintenanceDTO.applicationDate).add(vm_maintenanceDTO);
//
//					if( ! mapOfVm_maintenanceDTOTolastMaintenanceDate.containsKey(vm_maintenanceDTO.lastMaintenanceDate)) {
//						mapOfVm_maintenanceDTOTolastMaintenanceDate.put(vm_maintenanceDTO.lastMaintenanceDate, new HashSet<>());
//					}
//					mapOfVm_maintenanceDTOTolastMaintenanceDate.get(vm_maintenanceDTO.lastMaintenanceDate).add(vm_maintenanceDTO);
//
//					if( ! mapOfVm_maintenanceDTOTovehicleOfficeName.containsKey(vm_maintenanceDTO.vehicleOfficeName)) {
//						mapOfVm_maintenanceDTOTovehicleOfficeName.put(vm_maintenanceDTO.vehicleOfficeName, new HashSet<>());
//					}
//					mapOfVm_maintenanceDTOTovehicleOfficeName.get(vm_maintenanceDTO.vehicleOfficeName).add(vm_maintenanceDTO);
//
//					if( ! mapOfVm_maintenanceDTOTovehicleOfficeNameBn.containsKey(vm_maintenanceDTO.vehicleOfficeNameBn)) {
//						mapOfVm_maintenanceDTOTovehicleOfficeNameBn.put(vm_maintenanceDTO.vehicleOfficeNameBn, new HashSet<>());
//					}
//					mapOfVm_maintenanceDTOTovehicleOfficeNameBn.get(vm_maintenanceDTO.vehicleOfficeNameBn).add(vm_maintenanceDTO);
//
//					if( ! mapOfVm_maintenanceDTOToisChecked.containsKey(vm_maintenanceDTO.isChecked)) {
//						mapOfVm_maintenanceDTOToisChecked.put(vm_maintenanceDTO.isChecked, new HashSet<>());
//					}
//					mapOfVm_maintenanceDTOToisChecked.get(vm_maintenanceDTO.isChecked).add(vm_maintenanceDTO);
//
//					if( ! mapOfVm_maintenanceDTOTomechanicRemarks.containsKey(vm_maintenanceDTO.mechanicRemarks)) {
//						mapOfVm_maintenanceDTOTomechanicRemarks.put(vm_maintenanceDTO.mechanicRemarks, new HashSet<>());
//					}
//					mapOfVm_maintenanceDTOTomechanicRemarks.get(vm_maintenanceDTO.mechanicRemarks).add(vm_maintenanceDTO);
//
//					if( ! mapOfVm_maintenanceDTOTomechanicApproveDate.containsKey(vm_maintenanceDTO.mechanicApproveDate)) {
//						mapOfVm_maintenanceDTOTomechanicApproveDate.put(vm_maintenanceDTO.mechanicApproveDate, new HashSet<>());
//					}
//					mapOfVm_maintenanceDTOTomechanicApproveDate.get(vm_maintenanceDTO.mechanicApproveDate).add(vm_maintenanceDTO);
//
//					if( ! mapOfVm_maintenanceDTOToaoApproveDate.containsKey(vm_maintenanceDTO.aoApproveDate)) {
//						mapOfVm_maintenanceDTOToaoApproveDate.put(vm_maintenanceDTO.aoApproveDate, new HashSet<>());
//					}
//					mapOfVm_maintenanceDTOToaoApproveDate.get(vm_maintenanceDTO.aoApproveDate).add(vm_maintenanceDTO);
//
//					if( ! mapOfVm_maintenanceDTOTovatPercentage.containsKey(vm_maintenanceDTO.vatPercentage)) {
//						mapOfVm_maintenanceDTOTovatPercentage.put(vm_maintenanceDTO.vatPercentage, new HashSet<>());
//					}
//					mapOfVm_maintenanceDTOTovatPercentage.get(vm_maintenanceDTO.vatPercentage).add(vm_maintenanceDTO);
//
//					if( ! mapOfVm_maintenanceDTOTototalWithoutVat.containsKey(vm_maintenanceDTO.totalWithoutVat)) {
//						mapOfVm_maintenanceDTOTototalWithoutVat.put(vm_maintenanceDTO.totalWithoutVat, new HashSet<>());
//					}
//					mapOfVm_maintenanceDTOTototalWithoutVat.get(vm_maintenanceDTO.totalWithoutVat).add(vm_maintenanceDTO);
//
//					if( ! mapOfVm_maintenanceDTOTototalWithVat.containsKey(vm_maintenanceDTO.totalWithVat)) {
//						mapOfVm_maintenanceDTOTototalWithVat.put(vm_maintenanceDTO.totalWithVat, new HashSet<>());
//					}
//					mapOfVm_maintenanceDTOTototalWithVat.get(vm_maintenanceDTO.totalWithVat).add(vm_maintenanceDTO);
//
//					if( ! mapOfVm_maintenanceDTOTototalVat.containsKey(vm_maintenanceDTO.totalVat)) {
//						mapOfVm_maintenanceDTOTototalVat.put(vm_maintenanceDTO.totalVat, new HashSet<>());
//					}
//					mapOfVm_maintenanceDTOTototalVat.get(vm_maintenanceDTO.totalVat).add(vm_maintenanceDTO);
//
//					if( ! mapOfVm_maintenanceDTOTopaymentReceivedDate.containsKey(vm_maintenanceDTO.paymentReceivedDate)) {
//						mapOfVm_maintenanceDTOTopaymentReceivedDate.put(vm_maintenanceDTO.paymentReceivedDate, new HashSet<>());
//					}
//					mapOfVm_maintenanceDTOTopaymentReceivedDate.get(vm_maintenanceDTO.paymentReceivedDate).add(vm_maintenanceDTO);
//
//					if( ! mapOfVm_maintenanceDTOTofiscalYearId.containsKey(vm_maintenanceDTO.fiscalYearId)) {
//						mapOfVm_maintenanceDTOTofiscalYearId.put(vm_maintenanceDTO.fiscalYearId, new HashSet<>());
//					}
//					mapOfVm_maintenanceDTOTofiscalYearId.get(vm_maintenanceDTO.fiscalYearId).add(vm_maintenanceDTO);
//
//					if( ! mapOfVm_maintenanceDTOTosarokNumber.containsKey(vm_maintenanceDTO.sarokNumber)) {
//						mapOfVm_maintenanceDTOTosarokNumber.put(vm_maintenanceDTO.sarokNumber, new HashSet<>());
//					}
//					mapOfVm_maintenanceDTOTosarokNumber.get(vm_maintenanceDTO.sarokNumber).add(vm_maintenanceDTO);
//
//					if( ! mapOfVm_maintenanceDTOTofilesDropzone.containsKey(vm_maintenanceDTO.filesDropzone)) {
//						mapOfVm_maintenanceDTOTofilesDropzone.put(vm_maintenanceDTO.filesDropzone, new HashSet<>());
//					}
//					mapOfVm_maintenanceDTOTofilesDropzone.get(vm_maintenanceDTO.filesDropzone).add(vm_maintenanceDTO);
//
//					if( ! mapOfVm_maintenanceDTOTopaymentRemarks.containsKey(vm_maintenanceDTO.paymentRemarks)) {
//						mapOfVm_maintenanceDTOTopaymentRemarks.put(vm_maintenanceDTO.paymentRemarks, new HashSet<>());
//					}
//					mapOfVm_maintenanceDTOTopaymentRemarks.get(vm_maintenanceDTO.paymentRemarks).add(vm_maintenanceDTO);
//
//					if( ! mapOfVm_maintenanceDTOTosearchColumn.containsKey(vm_maintenanceDTO.searchColumn)) {
//						mapOfVm_maintenanceDTOTosearchColumn.put(vm_maintenanceDTO.searchColumn, new HashSet<>());
//					}
//					mapOfVm_maintenanceDTOTosearchColumn.get(vm_maintenanceDTO.searchColumn).add(vm_maintenanceDTO);
//
//					if( ! mapOfVm_maintenanceDTOToinsertedByUserId.containsKey(vm_maintenanceDTO.insertedByUserId)) {
//						mapOfVm_maintenanceDTOToinsertedByUserId.put(vm_maintenanceDTO.insertedByUserId, new HashSet<>());
//					}
//					mapOfVm_maintenanceDTOToinsertedByUserId.get(vm_maintenanceDTO.insertedByUserId).add(vm_maintenanceDTO);
//
//					if( ! mapOfVm_maintenanceDTOToinsertedByOrganogramId.containsKey(vm_maintenanceDTO.insertedByOrganogramId)) {
//						mapOfVm_maintenanceDTOToinsertedByOrganogramId.put(vm_maintenanceDTO.insertedByOrganogramId, new HashSet<>());
//					}
//					mapOfVm_maintenanceDTOToinsertedByOrganogramId.get(vm_maintenanceDTO.insertedByOrganogramId).add(vm_maintenanceDTO);
//
//					if( ! mapOfVm_maintenanceDTOToinsertionDate.containsKey(vm_maintenanceDTO.insertionDate)) {
//						mapOfVm_maintenanceDTOToinsertionDate.put(vm_maintenanceDTO.insertionDate, new HashSet<>());
//					}
//					mapOfVm_maintenanceDTOToinsertionDate.get(vm_maintenanceDTO.insertionDate).add(vm_maintenanceDTO);
//
//					if( ! mapOfVm_maintenanceDTOTolastModificationTime.containsKey(vm_maintenanceDTO.lastModificationTime)) {
//						mapOfVm_maintenanceDTOTolastModificationTime.put(vm_maintenanceDTO.lastModificationTime, new HashSet<>());
//					}
//					mapOfVm_maintenanceDTOTolastModificationTime.get(vm_maintenanceDTO.lastModificationTime).add(vm_maintenanceDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Vm_maintenanceDTO> getVm_maintenanceList() {
		List <Vm_maintenanceDTO> vm_maintenances = new ArrayList<Vm_maintenanceDTO>(this.mapOfVm_maintenanceDTOToiD.values());
		return clone(vm_maintenances);
	}
	
	
	public Vm_maintenanceDTO getVm_maintenanceDTOByID( long ID){
		return clone(mapOfVm_maintenanceDTOToiD.get(ID));
	}

	public Vm_maintenanceDTO getVm_maintenanceDTOByIDWithoutClone( long ID){
		return mapOfVm_maintenanceDTOToiD.get(ID);
	}

	public Vm_maintenanceDTO clone(Vm_maintenanceDTO dto) {
		String raw = gson.toJson(dto);
		return gson.fromJson(raw, Vm_maintenanceDTO.class);
	}

	public List<Vm_maintenanceDTO> clone(List<Vm_maintenanceDTO> dtoList) {
		return dtoList
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}
	
	
//	public List<Vm_maintenanceDTO> getVm_maintenanceDTOByvehicle_type_cat(int vehicle_type_cat) {
//		return new ArrayList<>( mapOfVm_maintenanceDTOTovehicleTypeCat.getOrDefault(vehicle_type_cat,new HashSet<>()));
//	}
//
//
//	public List<Vm_maintenanceDTO> getVm_maintenanceDTOBystatus(int status) {
//		return new ArrayList<>( mapOfVm_maintenanceDTOTostatus.getOrDefault(status,new HashSet<>()));
//	}
//
//
	public List<Vm_maintenanceDTO> getVm_maintenanceDTOByvehicle_id(long vehicle_id) {
		return clone(new ArrayList<>( mapOfVm_maintenanceDTOTovehicleId.getOrDefault(vehicle_id,new HashSet<>())));
	}
//
//
//	public List<Vm_maintenanceDTO> getVm_maintenanceDTOByrequester_post_id(long requester_post_id) {
//		return new ArrayList<>( mapOfVm_maintenanceDTOTorequesterPostId.getOrDefault(requester_post_id,new HashSet<>()));
//	}
//
//
//	public List<Vm_maintenanceDTO> getVm_maintenanceDTOByrequester_unit_id(long requester_unit_id) {
//		return new ArrayList<>( mapOfVm_maintenanceDTOTorequesterUnitId.getOrDefault(requester_unit_id,new HashSet<>()));
//	}
//
//
//	public List<Vm_maintenanceDTO> getVm_maintenanceDTOByrequester_employee_record_id(long requester_employee_record_id) {
//		return new ArrayList<>( mapOfVm_maintenanceDTOTorequesterEmployeeRecordId.getOrDefault(requester_employee_record_id,new HashSet<>()));
//	}
//
//
//	public List<Vm_maintenanceDTO> getVm_maintenanceDTOByvehicle_driver_assignment_id(long vehicle_driver_assignment_id) {
//		return new ArrayList<>( mapOfVm_maintenanceDTOTovehicleDriverAssignmentId.getOrDefault(vehicle_driver_assignment_id,new HashSet<>()));
//	}
//
//
//	public List<Vm_maintenanceDTO> getVm_maintenanceDTOByapplication_date(long application_date) {
//		return new ArrayList<>( mapOfVm_maintenanceDTOToapplicationDate.getOrDefault(application_date,new HashSet<>()));
//	}
//
//
//	public List<Vm_maintenanceDTO> getVm_maintenanceDTOBylast_maintenance_date(long last_maintenance_date) {
//		return new ArrayList<>( mapOfVm_maintenanceDTOTolastMaintenanceDate.getOrDefault(last_maintenance_date,new HashSet<>()));
//	}
//
//
//	public List<Vm_maintenanceDTO> getVm_maintenanceDTOByvehicle_office_name(String vehicle_office_name) {
//		return new ArrayList<>( mapOfVm_maintenanceDTOTovehicleOfficeName.getOrDefault(vehicle_office_name,new HashSet<>()));
//	}
//
//
//	public List<Vm_maintenanceDTO> getVm_maintenanceDTOByvehicle_office_name_bn(String vehicle_office_name_bn) {
//		return new ArrayList<>( mapOfVm_maintenanceDTOTovehicleOfficeNameBn.getOrDefault(vehicle_office_name_bn,new HashSet<>()));
//	}
//
//
//	public List<Vm_maintenanceDTO> getVm_maintenanceDTOByis_checked(boolean is_checked) {
//		return new ArrayList<>( mapOfVm_maintenanceDTOToisChecked.getOrDefault(is_checked,new HashSet<>()));
//	}
//
//
//	public List<Vm_maintenanceDTO> getVm_maintenanceDTOBymechanic_remarks(String mechanic_remarks) {
//		return new ArrayList<>( mapOfVm_maintenanceDTOTomechanicRemarks.getOrDefault(mechanic_remarks,new HashSet<>()));
//	}
//
//
//	public List<Vm_maintenanceDTO> getVm_maintenanceDTOBymechanic_approve_date(long mechanic_approve_date) {
//		return new ArrayList<>( mapOfVm_maintenanceDTOTomechanicApproveDate.getOrDefault(mechanic_approve_date,new HashSet<>()));
//	}
//
//
//	public List<Vm_maintenanceDTO> getVm_maintenanceDTOByao_approve_date(long ao_approve_date) {
//		return new ArrayList<>( mapOfVm_maintenanceDTOToaoApproveDate.getOrDefault(ao_approve_date,new HashSet<>()));
//	}
//
//
//	public List<Vm_maintenanceDTO> getVm_maintenanceDTOByvat_percentage(double vat_percentage) {
//		return new ArrayList<>( mapOfVm_maintenanceDTOTovatPercentage.getOrDefault(vat_percentage,new HashSet<>()));
//	}
//
//
//	public List<Vm_maintenanceDTO> getVm_maintenanceDTOBytotal_without_vat(double total_without_vat) {
//		return new ArrayList<>( mapOfVm_maintenanceDTOTototalWithoutVat.getOrDefault(total_without_vat,new HashSet<>()));
//	}
//
//
//	public List<Vm_maintenanceDTO> getVm_maintenanceDTOBytotal_with_vat(double total_with_vat) {
//		return new ArrayList<>( mapOfVm_maintenanceDTOTototalWithVat.getOrDefault(total_with_vat,new HashSet<>()));
//	}
//
//
//	public List<Vm_maintenanceDTO> getVm_maintenanceDTOBytotal_vat(double total_vat) {
//		return new ArrayList<>( mapOfVm_maintenanceDTOTototalVat.getOrDefault(total_vat,new HashSet<>()));
//	}
//
//
//	public List<Vm_maintenanceDTO> getVm_maintenanceDTOBypayment_received_date(long payment_received_date) {
//		return new ArrayList<>( mapOfVm_maintenanceDTOTopaymentReceivedDate.getOrDefault(payment_received_date,new HashSet<>()));
//	}
//
//
//	public List<Vm_maintenanceDTO> getVm_maintenanceDTOByfiscal_year_id(long fiscal_year_id) {
//		return new ArrayList<>( mapOfVm_maintenanceDTOTofiscalYearId.getOrDefault(fiscal_year_id,new HashSet<>()));
//	}
//
//
//	public List<Vm_maintenanceDTO> getVm_maintenanceDTOBysarok_number(String sarok_number) {
//		return new ArrayList<>( mapOfVm_maintenanceDTOTosarokNumber.getOrDefault(sarok_number,new HashSet<>()));
//	}
//
//
//	public List<Vm_maintenanceDTO> getVm_maintenanceDTOByfiles_dropzone(long files_dropzone) {
//		return new ArrayList<>( mapOfVm_maintenanceDTOTofilesDropzone.getOrDefault(files_dropzone,new HashSet<>()));
//	}
//
//
//	public List<Vm_maintenanceDTO> getVm_maintenanceDTOBypayment_remarks(String payment_remarks) {
//		return new ArrayList<>( mapOfVm_maintenanceDTOTopaymentRemarks.getOrDefault(payment_remarks,new HashSet<>()));
//	}
//
//
//	public List<Vm_maintenanceDTO> getVm_maintenanceDTOBysearch_column(String search_column) {
//		return new ArrayList<>( mapOfVm_maintenanceDTOTosearchColumn.getOrDefault(search_column,new HashSet<>()));
//	}
//
//
//	public List<Vm_maintenanceDTO> getVm_maintenanceDTOByinserted_by_user_id(long inserted_by_user_id) {
//		return new ArrayList<>( mapOfVm_maintenanceDTOToinsertedByUserId.getOrDefault(inserted_by_user_id,new HashSet<>()));
//	}
//
//
//	public List<Vm_maintenanceDTO> getVm_maintenanceDTOByinserted_by_organogram_id(long inserted_by_organogram_id) {
//		return new ArrayList<>( mapOfVm_maintenanceDTOToinsertedByOrganogramId.getOrDefault(inserted_by_organogram_id,new HashSet<>()));
//	}
//
//
//	public List<Vm_maintenanceDTO> getVm_maintenanceDTOByinsertion_date(long insertion_date) {
//		return new ArrayList<>( mapOfVm_maintenanceDTOToinsertionDate.getOrDefault(insertion_date,new HashSet<>()));
//	}
//
//
//	public List<Vm_maintenanceDTO> getVm_maintenanceDTOBylastModificationTime(long lastModificationTime) {
//		return new ArrayList<>( mapOfVm_maintenanceDTOTolastModificationTime.getOrDefault(lastModificationTime,new HashSet<>()));
//	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "vm_maintenance";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


