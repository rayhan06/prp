package vm_maintenance;

import common.ConnectionAndStatementUtil;
import org.apache.log4j.Logger;
import pb.CatRepository;
import pb.CommonDAO;
import repository.RepositoryManager;
import user.UserDTO;
import util.CommonDTO;
import util.NavigationService4;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Hashtable;
import java.util.List;

public class VmMaintenanceItemDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());

	public VmMaintenanceItemDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";
		commonMaps = new VmMaintenanceItemMAPS(tableName);
		columnNames = new String[] 
		{
			"ID",
			"vm_maintenance_id",
			"vm_vehicle_parts_type",
			"vehicle_maintenance_cat",
			"amount",
			"remarks",
			"mechanic_remarks",
			"mechanic_price",
			"ao_price",
			"selected",
			"driver_price",
			"search_column",
			"inserted_by_user_id",
			"inserted_by_organogram_id",
			"insertion_date",
			"isDeleted",
			"lastModificationTime"
		};
	}
	
	public VmMaintenanceItemDAO()
	{
		this("vm_maintenance_item");		
	}
	
	public void setSearchColumn(VmMaintenanceItemDTO vmmaintenanceitemDTO)
	{
		vmmaintenanceitemDTO.searchColumn = "";
		vmmaintenanceitemDTO.searchColumn += CommonDAO.getName("English", "vm_vehicle_parts", vmmaintenanceitemDTO.vmVehiclePartsType) + " " + CommonDAO.getName("Bangla", "vm_vehicle_parts", vmmaintenanceitemDTO.vmVehiclePartsType) + " ";
		vmmaintenanceitemDTO.searchColumn += CatRepository.getName("English", "vehicle_maintenance", vmmaintenanceitemDTO.vehicleMaintenanceCat) + " " + CatRepository.getName("Bangla", "vehicle_maintenance", vmmaintenanceitemDTO.vehicleMaintenanceCat) + " ";
		vmmaintenanceitemDTO.searchColumn += vmmaintenanceitemDTO.remarks + " ";
	}
	
	public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException
	{
		VmMaintenanceItemDTO vmmaintenanceitemDTO = (VmMaintenanceItemDTO)commonDTO;
		int index = 1;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(vmmaintenanceitemDTO);
		if(isInsert)
		{
			ps.setObject(index++,vmmaintenanceitemDTO.iD);
		}
		ps.setObject(index++,vmmaintenanceitemDTO.vmMaintenanceId);
		ps.setObject(index++,vmmaintenanceitemDTO.vmVehiclePartsType);
		ps.setObject(index++,vmmaintenanceitemDTO.vehicleMaintenanceCat);
		ps.setObject(index++,vmmaintenanceitemDTO.amount);
		ps.setObject(index++,vmmaintenanceitemDTO.remarks);
		ps.setObject(index++,vmmaintenanceitemDTO.mechanic_remarks);
		ps.setObject(index++,vmmaintenanceitemDTO.mechanic_price);
		ps.setObject(index++,vmmaintenanceitemDTO.ao_price);
		ps.setObject(index++,vmmaintenanceitemDTO.selected);
		ps.setObject(index++,vmmaintenanceitemDTO.driverPrice);
		ps.setObject(index++,vmmaintenanceitemDTO.searchColumn);
		ps.setObject(index++,vmmaintenanceitemDTO.insertedByUserId);
		ps.setObject(index++,vmmaintenanceitemDTO.insertedByOrganogramId);
		ps.setObject(index++,vmmaintenanceitemDTO.insertionDate);
		if(isInsert)
		{
			ps.setObject(index++, 0);
			ps.setObject(index++, lastModificationTime);
		}
	}
	
	public VmMaintenanceItemDTO build(ResultSet rs)
	{
		try
		{
			VmMaintenanceItemDTO vmmaintenanceitemDTO = new VmMaintenanceItemDTO();
			vmmaintenanceitemDTO.iD = rs.getLong("ID");
			vmmaintenanceitemDTO.vmMaintenanceId = rs.getLong("vm_maintenance_id");
			vmmaintenanceitemDTO.vmVehiclePartsType = rs.getLong("vm_vehicle_parts_type");
			vmmaintenanceitemDTO.vehicleMaintenanceCat = rs.getInt("vehicle_maintenance_cat");
			vmmaintenanceitemDTO.amount = rs.getInt("amount");
			vmmaintenanceitemDTO.remarks = rs.getString("remarks");
			vmmaintenanceitemDTO.mechanic_remarks = rs.getString("mechanic_remarks");
			vmmaintenanceitemDTO.selected = rs.getBoolean("selected");
			vmmaintenanceitemDTO.driverPrice = rs.getDouble("driver_price");
			vmmaintenanceitemDTO.mechanic_price = rs.getDouble("mechanic_price");
			vmmaintenanceitemDTO.ao_price = rs.getDouble("ao_price");
			vmmaintenanceitemDTO.searchColumn = rs.getString("search_column");
			vmmaintenanceitemDTO.insertedByUserId = rs.getLong("inserted_by_user_id");
			vmmaintenanceitemDTO.insertedByOrganogramId = rs.getLong("inserted_by_organogram_id");
			vmmaintenanceitemDTO.insertionDate = rs.getLong("insertion_date");
			vmmaintenanceitemDTO.isDeleted = rs.getInt("isDeleted");
			vmmaintenanceitemDTO.lastModificationTime = rs.getLong("lastModificationTime");
			return vmmaintenanceitemDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
	
	
	public List<VmMaintenanceItemDTO> getVmMaintenanceItemDTOListByVmMaintenanceID(long vmMaintenanceID) throws Exception
	{
		String sql = "SELECT * FROM vm_maintenance_item where isDeleted=0 and vm_maintenance_id = ? order by vm_maintenance_item.lastModificationTime";
		return ConnectionAndStatementUtil.getListOfT(sql, Arrays.asList(vmMaintenanceID), this::build);
	}

	public VmMaintenanceItemDTO getDTOByID (long id)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID = ? ";
		VmMaintenanceItemDTO vmmaintenanceitemDTO = ConnectionAndStatementUtil.getT(sql,Arrays.asList(id), this::build);
		return vmmaintenanceitemDTO;
	}

	
	public List<VmMaintenanceItemDTO> getDTOs(Collection recordIDs)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID IN ( ";
		for(int i = 0;i<recordIDs.size();i++){
			if(i!=0){
				sql+=",";
			}
			sql+= " ? ";
		}
		sql+=")  order by lastModificationTime desc";
		printSql(sql);
		return ConnectionAndStatementUtil.getListOfT(sql, Arrays.asList(recordIDs.toArray()),this::build);
	}
	
	public List<VmMaintenanceItemDTO> getAllVmMaintenanceItem (boolean isFirstReload)
    {
		String sql = "SELECT * FROM " + tableName + " WHERE ";
		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by " + tableName + ".lastModificationTime desc";
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);	
    }

	
	public List<VmMaintenanceItemDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO)
	{
		return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
	}

	
	public List<VmMaintenanceItemDTO> getDTOs(Hashtable  p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
			String filter, boolean tableHasJobCat)
	{
		String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat);
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);			
	}
				
}
	