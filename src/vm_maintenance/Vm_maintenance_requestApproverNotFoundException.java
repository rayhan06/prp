package vm_maintenance;

/*
 * @author Md. Erfan Hossain
 * @created 03/05/2021 - 4:46 PM
 * @project parliament
 */

public class Vm_maintenance_requestApproverNotFoundException extends Exception{
    public Vm_maintenance_requestApproverNotFoundException(String message) {
        super(message);
    }
}
