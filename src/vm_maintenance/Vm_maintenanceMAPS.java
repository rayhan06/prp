package vm_maintenance;
import java.util.*; 
import util.*;


public class Vm_maintenanceMAPS extends CommonMaps
{	
	public Vm_maintenanceMAPS(String tableName)
	{
		

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("vehicleTypeCat".toLowerCase(), "vehicleTypeCat".toLowerCase());
		java_DTO_map.put("status".toLowerCase(), "status".toLowerCase());
		java_DTO_map.put("vehicleId".toLowerCase(), "vehicleId".toLowerCase());
		java_DTO_map.put("requesterPostId".toLowerCase(), "requesterPostId".toLowerCase());
		java_DTO_map.put("requesterUnitId".toLowerCase(), "requesterUnitId".toLowerCase());
		java_DTO_map.put("requesterEmployeeRecordId".toLowerCase(), "requesterEmployeeRecordId".toLowerCase());
		java_DTO_map.put("vehicleDriverAssignmentId".toLowerCase(), "vehicleDriverAssignmentId".toLowerCase());
		java_DTO_map.put("applicationDate".toLowerCase(), "applicationDate".toLowerCase());
		java_DTO_map.put("lastMaintenanceDate".toLowerCase(), "lastMaintenanceDate".toLowerCase());
		java_DTO_map.put("vehicleOfficeName".toLowerCase(), "vehicleOfficeName".toLowerCase());
		java_DTO_map.put("vehicleOfficeNameBn".toLowerCase(), "vehicleOfficeNameBn".toLowerCase());
		java_DTO_map.put("isChecked".toLowerCase(), "isChecked".toLowerCase());
		java_DTO_map.put("mechanicRemarks".toLowerCase(), "mechanicRemarks".toLowerCase());
		java_DTO_map.put("mechanicApproveDate".toLowerCase(), "mechanicApproveDate".toLowerCase());
		java_DTO_map.put("aoApproveDate".toLowerCase(), "aoApproveDate".toLowerCase());
		java_DTO_map.put("vatPercentage".toLowerCase(), "vatPercentage".toLowerCase());
		java_DTO_map.put("totalWithoutVat".toLowerCase(), "totalWithoutVat".toLowerCase());
		java_DTO_map.put("totalWithVat".toLowerCase(), "totalWithVat".toLowerCase());
		java_DTO_map.put("totalVat".toLowerCase(), "totalVat".toLowerCase());
		java_DTO_map.put("paymentReceivedDate".toLowerCase(), "paymentReceivedDate".toLowerCase());
		java_DTO_map.put("fiscalYearId".toLowerCase(), "fiscalYearId".toLowerCase());
		java_DTO_map.put("sarokNumber".toLowerCase(), "sarokNumber".toLowerCase());
		java_DTO_map.put("filesDropzone".toLowerCase(), "filesDropzone".toLowerCase());
		java_DTO_map.put("paymentRemarks".toLowerCase(), "paymentRemarks".toLowerCase());
		java_DTO_map.put("searchColumn".toLowerCase(), "searchColumn".toLowerCase());
		java_DTO_map.put("insertedByUserId".toLowerCase(), "insertedByUserId".toLowerCase());
		java_DTO_map.put("insertedByOrganogramId".toLowerCase(), "insertedByOrganogramId".toLowerCase());
		java_DTO_map.put("insertionDate".toLowerCase(), "insertionDate".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("vehicle_type_cat".toLowerCase(), "vehicleTypeCat".toLowerCase());
		java_SQL_map.put("status".toLowerCase(), "status".toLowerCase());
		java_SQL_map.put("vehicle_id".toLowerCase(), "vehicleId".toLowerCase());
		java_SQL_map.put("requester_post_id".toLowerCase(), "requesterPostId".toLowerCase());
		java_SQL_map.put("requester_unit_id".toLowerCase(), "requesterUnitId".toLowerCase());
		java_SQL_map.put("requester_employee_record_id".toLowerCase(), "requesterEmployeeRecordId".toLowerCase());
		java_SQL_map.put("vehicle_driver_assignment_id".toLowerCase(), "vehicleDriverAssignmentId".toLowerCase());
		java_SQL_map.put("application_date".toLowerCase(), "applicationDate".toLowerCase());
		java_SQL_map.put("last_maintenance_date".toLowerCase(), "lastMaintenanceDate".toLowerCase());
		java_SQL_map.put("vehicle_office_name".toLowerCase(), "vehicleOfficeName".toLowerCase());
		java_SQL_map.put("vehicle_office_name_bn".toLowerCase(), "vehicleOfficeNameBn".toLowerCase());
		java_SQL_map.put("is_checked".toLowerCase(), "isChecked".toLowerCase());
		java_SQL_map.put("mechanic_remarks".toLowerCase(), "mechanicRemarks".toLowerCase());
		java_SQL_map.put("mechanic_approve_date".toLowerCase(), "mechanicApproveDate".toLowerCase());
		java_SQL_map.put("ao_approve_date".toLowerCase(), "aoApproveDate".toLowerCase());
		java_SQL_map.put("vat_percentage".toLowerCase(), "vatPercentage".toLowerCase());
		java_SQL_map.put("total_without_vat".toLowerCase(), "totalWithoutVat".toLowerCase());
		java_SQL_map.put("total_with_vat".toLowerCase(), "totalWithVat".toLowerCase());
		java_SQL_map.put("total_vat".toLowerCase(), "totalVat".toLowerCase());
		java_SQL_map.put("payment_received_date".toLowerCase(), "paymentReceivedDate".toLowerCase());
		java_SQL_map.put("fiscal_year_id".toLowerCase(), "fiscalYearId".toLowerCase());
		java_SQL_map.put("sarok_number".toLowerCase(), "sarokNumber".toLowerCase());
		java_SQL_map.put("files_dropzone".toLowerCase(), "filesDropzone".toLowerCase());
		java_SQL_map.put("payment_remarks".toLowerCase(), "paymentRemarks".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Vehicle Type".toLowerCase(), "vehicleTypeCat".toLowerCase());
		java_Text_map.put("Status".toLowerCase(), "status".toLowerCase());
		java_Text_map.put("Vehicle Id".toLowerCase(), "vehicleId".toLowerCase());
		java_Text_map.put("Requester Post Id".toLowerCase(), "requesterPostId".toLowerCase());
		java_Text_map.put("Requester Unit Id".toLowerCase(), "requesterUnitId".toLowerCase());
		java_Text_map.put("Requester Employee Record Id".toLowerCase(), "requesterEmployeeRecordId".toLowerCase());
		java_Text_map.put("Vehicle Driver Assignment Id".toLowerCase(), "vehicleDriverAssignmentId".toLowerCase());
		java_Text_map.put("Application Date".toLowerCase(), "applicationDate".toLowerCase());
		java_Text_map.put("Last Maintenance Date".toLowerCase(), "lastMaintenanceDate".toLowerCase());
		java_Text_map.put("Vehicle Office Name".toLowerCase(), "vehicleOfficeName".toLowerCase());
		java_Text_map.put("Vehicle Office Name Bn".toLowerCase(), "vehicleOfficeNameBn".toLowerCase());
		java_Text_map.put("Is Checked".toLowerCase(), "isChecked".toLowerCase());
		java_Text_map.put("Mechanic Remarks".toLowerCase(), "mechanicRemarks".toLowerCase());
		java_Text_map.put("Mechanic Approve Date".toLowerCase(), "mechanicApproveDate".toLowerCase());
		java_Text_map.put("Ao Approve Date".toLowerCase(), "aoApproveDate".toLowerCase());
		java_Text_map.put("Vat Percentage".toLowerCase(), "vatPercentage".toLowerCase());
		java_Text_map.put("Total Without Vat".toLowerCase(), "totalWithoutVat".toLowerCase());
		java_Text_map.put("Total With Vat".toLowerCase(), "totalWithVat".toLowerCase());
		java_Text_map.put("Total Vat".toLowerCase(), "totalVat".toLowerCase());
		java_Text_map.put("Payment Received Date".toLowerCase(), "paymentReceivedDate".toLowerCase());
		java_Text_map.put("Fiscal Year Id".toLowerCase(), "fiscalYearId".toLowerCase());
		java_Text_map.put("Sarok Number".toLowerCase(), "sarokNumber".toLowerCase());
		java_Text_map.put("Files Dropzone".toLowerCase(), "filesDropzone".toLowerCase());
		java_Text_map.put("Payment Remarks".toLowerCase(), "paymentRemarks".toLowerCase());
		java_Text_map.put("Search Column".toLowerCase(), "searchColumn".toLowerCase());
		java_Text_map.put("Inserted By User Id".toLowerCase(), "insertedByUserId".toLowerCase());
		java_Text_map.put("Inserted By Organogram Id".toLowerCase(), "insertedByOrganogramId".toLowerCase());
		java_Text_map.put("Insertion Date".toLowerCase(), "insertionDate".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}