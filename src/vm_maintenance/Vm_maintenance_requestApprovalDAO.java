package vm_maintenance;

/*
 * @author Md. Erfan Hossain
 * @created 28/04/2021 - 11:47 AM
 * @project parliament
 */

import employee_records.EmployeeCommonDTOService;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@SuppressWarnings({"unused","Duplicates"})
public class Vm_maintenance_requestApprovalDAO implements EmployeeCommonDTOService<Vm_maintenance_requestApprovalDTO> {
    private static final String addSqlQuery = "INSERT INTO {tableName} (employee_records_id,employee_name_eng,employee_name_bng," +
            "office_unit_id,office_unit_eng,office_unit_bng,organogram_id,organogram_eng,organogram_bng," +
            "modified_by,lastModificationTime,inserted_by,insertion_time,isDeleted,id) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
    private static final String deleteSqlQuery = "UPDATE vm_maintenance_request_approval SET isDeleted = 1, lastModificationTime = %d, modified_by = %d WHERE id = %d";

    @Override
    public void set(PreparedStatement ps, Vm_maintenance_requestApprovalDTO dto, boolean isInsert) throws SQLException {
        int index = 0;
        ps.setLong(++index,dto.employeeRecordId);
        ps.setString(++index,dto.nameEng);
        ps.setString(++index,dto.nameBng);
        ps.setLong(++index,dto.officeUnitId);
        ps.setString(++index,dto.officeUnitEng);
        ps.setString(++index,dto.officeUnitBng);
        ps.setLong(++index,dto.organogramId);
        ps.setString(++index,dto.organogramEng);
        ps.setString(++index,dto.organogramBng);
        ps.setLong(++index,dto.modifiedBy);
        ps.setLong(++index,dto.lastModificationTime);
        if(isInsert){
            ps.setLong(++index,dto.insertBy);
            ps.setLong(++index,dto.insertionTime);
            ps.setInt(++index,0);
        }
        ps.setLong(++index,dto.iD);
    }

    @Override
    public Vm_maintenance_requestApprovalDTO buildObjectFromResultSet(ResultSet rs) {
        try{
            Vm_maintenance_requestApprovalDTO dto = new Vm_maintenance_requestApprovalDTO();
            dto.iD = rs.getLong("id");
            dto.employeeRecordId = rs.getLong("employee_records_id");
            dto.nameEng = rs.getString("employee_name_eng");
            dto.nameBng = rs.getString("employee_name_bng");
            dto.officeUnitId = rs.getLong("office_unit_id");
            dto.officeUnitEng = rs.getString("office_unit_eng");
            dto.officeUnitBng = rs.getString("office_unit_bng");
            dto.organogramId = rs.getLong("organogram_id");
            dto.organogramEng = rs.getString("organogram_eng");
            dto.organogramBng = rs.getString("organogram_bng");
            dto.insertionTime = rs.getLong("insertion_time");
            dto.insertBy = rs.getLong("inserted_by");
            dto.modifiedBy = rs.getLong("modified_by");
            dto.isDeleted = rs.getInt("isDeleted");
            dto.lastModificationTime = rs.getLong("lastModificationTime");
            return dto;
        }catch (SQLException ex){
            return null;
        }
    }

    @Override
    public String getTableName() {
        return "vm_maintenance_request_approval";
    }

    public long add(Vm_maintenance_requestApprovalDTO dto) throws Exception {
        return executeAddOrUpdateQuery(dto, addSqlQuery, true);
    }
}
