package vm_maintenance;

import common.ConnectionAndStatementUtil;
import org.apache.log4j.Logger;
import pb.CatRepository;
import user.UserDTO;
import util.CommonDTO;
import util.NavigationService4;
import util.UtilCharacter;
import vm_vehicle.Vm_vehicleDTO;
import vm_vehicle.Vm_vehicleRepository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;

public class Vm_maintenanceDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());

	public static HashMap<Integer, String[]> statusMap = new HashMap() {{
		put(1, new String[]{"Mechanic Approval Pending", "মেকানিক অনুমোদন চলমান "});
		put(2, new String[]{"Mechanic Rejected", "মেকানিক অনুমোদিত নহে "});
		put(3, new String[]{"Mechanic Approved", "মেকানিক অনুমোদিত "});
		put(4, new String[]{"AO Rejected", "AO অনুমোদিত নহে "});
		put(5, new String[]{"AO Approved", "AO অনুমোদিত "});
		put(6, new String[]{"Payment Received", "পেমেন্ট সম্পাদিত  "});
	}};

	public static String getStatus(int value, String language){
		String[] s = statusMap.get(value);
		if(s != null){
			return UtilCharacter.getDataByLanguage(language, s[1], s[0]);

		} else {
			return UtilCharacter.getDataByLanguage(language, "পাওয়া যায় নি ", "Not Available");
		}

	}

	public Vm_maintenanceDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";
		commonMaps = new Vm_maintenanceMAPS(tableName);
		columnNames = new String[] 
		{
			"ID",
			"vehicle_type_cat",
			"status",
			"vehicle_id",
			"requester_post_id",
			"requester_unit_id",
			"requester_employee_record_id",
			"vehicle_driver_assignment_id",
			"ao_approver_post_name",
			"ao_approver_post_name_bn",
			"requester_post_name",
			"requester_post_name_bn",
			"mecha_approver_post_name",
			"mecha_approver_post_name_bn",
			"payment_receiver_post_name",
			"payment_receiver_post_name_bn",
			"requester_unit_name",
			"requester_unit_name_bn",
			"mecha_approver_unit_name",
			"mecha_approver_unit_name_bn",
			"ao_approver_unit_name",
			"ao_approver_unit_name_bn",
			"payment_receiver_unit_name",
			"payment_receiver_unit_name_bn",
			"requester_employee_record_name",
			"requester_employee_record_name_bn",
			"mecha_approver_employee_record_name",
			"mecha_approver_employee_record_name_bn",
			"ao_approver_employee_record_name",
			"ao_approver_employee_record_name_bn",
			"payment_receiver_employee_record_name",
			"payment_receiver_employee_record_name_bn",
			"mecha_approver_post_id",
			"ao_approver_post_id",
			"payment_receiver_post_id",
			"mecha_approver_unit_id",
			"ao_approver_unit_id",
			"payment_receiver_unit_id",
			"mecha_approver_employee_record_id",
			"ao_approver_employee_record_id",
			"payment_receiver_employee_record_id",
			"application_date",
			"last_maintenance_date",
			"vehicle_office_name",
			"vehicle_office_name_bn",
			"is_checked",
			"mechanic_remarks",
			"mechanic_approve_date",
			"ao_approve_date",
			"vat_percentage",
			"total_without_vat",
			"total_with_vat",
			"total_vat",
			"requested_total",
			"payment_received_date",
			"fiscal_year_id",
			"sarok_number",
			"files_dropzone",
			"payment_remarks",
			"search_column",
			"inserted_by_user_id",
			"inserted_by_organogram_id",
			"insertion_date",
			"isDeleted",
			"lastModificationTime"
		};
	}
	
	public Vm_maintenanceDAO()
	{
		this("vm_maintenance");		
	}
	
	public void setSearchColumn(Vm_maintenanceDTO vm_maintenanceDTO)
	{
		vm_maintenanceDTO.searchColumn = "";
		vm_maintenanceDTO.searchColumn += CatRepository.getName("English", "vehicle_type", vm_maintenanceDTO.vehicleTypeCat) + " " + CatRepository.getName("Bangla", "vehicle_type", vm_maintenanceDTO.vehicleTypeCat) + " ";
		vm_maintenanceDTO.searchColumn += vm_maintenanceDTO.vehicleOfficeName + " ";
		vm_maintenanceDTO.searchColumn += vm_maintenanceDTO.vehicleOfficeNameBn + " ";
//		vm_maintenanceDTO.searchColumn += vm_maintenanceDTO.mechanicRemarks + " ";
		vm_maintenanceDTO.searchColumn += vm_maintenanceDTO.sarokNumber + " ";
//		vm_maintenanceDTO.searchColumn += vm_maintenanceDTO.paymentRemarks + " ";

		Vm_vehicleDTO vm_vehicleDTO = Vm_vehicleRepository.getInstance().getVm_vehicleDTOByID(vm_maintenanceDTO.vehicleId);
		if(vm_vehicleDTO != null){
			vm_maintenanceDTO.searchColumn += vm_vehicleDTO.regNo + " ";
		}
	}
	
	public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException
	{
		Vm_maintenanceDTO vm_maintenanceDTO = (Vm_maintenanceDTO)commonDTO;
		int index = 1;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(vm_maintenanceDTO);
		if(isInsert)
		{
			ps.setObject(index++,vm_maintenanceDTO.iD);
		}
		ps.setObject(index++,vm_maintenanceDTO.vehicleTypeCat);
		ps.setObject(index++,vm_maintenanceDTO.status);
		ps.setObject(index++,vm_maintenanceDTO.vehicleId);
		ps.setObject(index++,vm_maintenanceDTO.requesterPostId);
		ps.setObject(index++,vm_maintenanceDTO.requesterUnitId);
		ps.setObject(index++,vm_maintenanceDTO.requesterEmployeeRecordId);
		ps.setObject(index++,vm_maintenanceDTO.vehicleDriverAssignmentId);



		ps.setObject(index++,vm_maintenanceDTO.ao_approver_post_name);
		ps.setObject(index++,vm_maintenanceDTO.ao_approver_post_name_bn);
		ps.setObject(index++,vm_maintenanceDTO.requester_post_name);
		ps.setObject(index++,vm_maintenanceDTO.requester_post_name_bn);
		ps.setObject(index++,vm_maintenanceDTO.mecha_approver_post_name);
		ps.setObject(index++,vm_maintenanceDTO.mecha_approver_post_name_bn);
		ps.setObject(index++,vm_maintenanceDTO.payment_receiver_post_name);
		ps.setObject(index++,vm_maintenanceDTO.payment_receiver_post_name_bn);
		ps.setObject(index++,vm_maintenanceDTO.requester_unit_name);
		ps.setObject(index++,vm_maintenanceDTO.requester_unit_name_bn);
		ps.setObject(index++,vm_maintenanceDTO.mecha_approver_unit_name);
		ps.setObject(index++,vm_maintenanceDTO.mecha_approver_unit_name_bn);
		ps.setObject(index++,vm_maintenanceDTO.ao_approver_unit_name);
		ps.setObject(index++,vm_maintenanceDTO.ao_approver_unit_name_bn);
		ps.setObject(index++,vm_maintenanceDTO.payment_receiver_unit_name);
		ps.setObject(index++,vm_maintenanceDTO.payment_receiver_unit_name_bn);
		ps.setObject(index++,vm_maintenanceDTO.requester_employee_record_name);
		ps.setObject(index++,vm_maintenanceDTO.requester_employee_record_name_bn);
		ps.setObject(index++,vm_maintenanceDTO.mecha_approver_employee_record_name);
		ps.setObject(index++,vm_maintenanceDTO.mecha_approver_employee_record_name_bn);
		ps.setObject(index++,vm_maintenanceDTO.ao_approver_employee_record_name);
		ps.setObject(index++,vm_maintenanceDTO.ao_approver_employee_record_name_bn);
		ps.setObject(index++,vm_maintenanceDTO.payment_receiver_employee_record_name);
		ps.setObject(index++,vm_maintenanceDTO.payment_receiver_employee_record_name_bn);


		ps.setObject(index++,vm_maintenanceDTO.mecha_approver_post_id);
		ps.setObject(index++,vm_maintenanceDTO.ao_approver_post_id);
		ps.setObject(index++,vm_maintenanceDTO.payment_receiver_post_id);
		ps.setObject(index++,vm_maintenanceDTO.mecha_approver_unit_id);
		ps.setObject(index++,vm_maintenanceDTO.ao_approver_unit_id);
		ps.setObject(index++,vm_maintenanceDTO.payment_receiver_unit_id);
		ps.setObject(index++,vm_maintenanceDTO.mecha_approver_employee_record_id);
		ps.setObject(index++,vm_maintenanceDTO.ao_approver_employee_record_id);
		ps.setObject(index++,vm_maintenanceDTO.payment_receiver_employee_record_id);


		ps.setObject(index++,vm_maintenanceDTO.applicationDate);
		ps.setObject(index++,vm_maintenanceDTO.lastMaintenanceDate);
		ps.setObject(index++,vm_maintenanceDTO.vehicleOfficeName);
		ps.setObject(index++,vm_maintenanceDTO.vehicleOfficeNameBn);
		ps.setObject(index++,vm_maintenanceDTO.isChecked);
		ps.setObject(index++,vm_maintenanceDTO.mechanicRemarks);
		ps.setObject(index++,vm_maintenanceDTO.mechanicApproveDate);
		ps.setObject(index++,vm_maintenanceDTO.aoApproveDate);
		ps.setObject(index++,vm_maintenanceDTO.vatPercentage);
		ps.setObject(index++,vm_maintenanceDTO.totalWithoutVat);
		ps.setObject(index++,vm_maintenanceDTO.totalWithVat);
		ps.setObject(index++,vm_maintenanceDTO.totalVat);
		ps.setObject(index++,vm_maintenanceDTO.requested_total);
		ps.setObject(index++,vm_maintenanceDTO.paymentReceivedDate);
		ps.setObject(index++,vm_maintenanceDTO.fiscalYearId);
		ps.setObject(index++,vm_maintenanceDTO.sarokNumber);
		ps.setObject(index++,vm_maintenanceDTO.filesDropzone);
		ps.setObject(index++,vm_maintenanceDTO.paymentRemarks);
		ps.setObject(index++,vm_maintenanceDTO.searchColumn);
		ps.setObject(index++,vm_maintenanceDTO.insertedByUserId);
		ps.setObject(index++,vm_maintenanceDTO.insertedByOrganogramId);
		ps.setObject(index++,vm_maintenanceDTO.insertionDate);
		if(isInsert)
		{
			ps.setObject(index++, 0);
			ps.setObject(index++, lastModificationTime);
		}
	}
	
	public Vm_maintenanceDTO build(ResultSet rs)
	{
		try
		{
			Vm_maintenanceDTO vm_maintenanceDTO = new Vm_maintenanceDTO();
			vm_maintenanceDTO.iD = rs.getLong("ID");
			vm_maintenanceDTO.vehicleTypeCat = rs.getInt("vehicle_type_cat");
			vm_maintenanceDTO.status = rs.getInt("status");
			vm_maintenanceDTO.vehicleId = rs.getLong("vehicle_id");
			vm_maintenanceDTO.requesterPostId = rs.getLong("requester_post_id");
			vm_maintenanceDTO.requesterUnitId = rs.getLong("requester_unit_id");
			vm_maintenanceDTO.requesterEmployeeRecordId = rs.getLong("requester_employee_record_id");
			vm_maintenanceDTO.vehicleDriverAssignmentId = rs.getLong("vehicle_driver_assignment_id");
			vm_maintenanceDTO.applicationDate = rs.getLong("application_date");
			vm_maintenanceDTO.lastMaintenanceDate = rs.getLong("last_maintenance_date");
			vm_maintenanceDTO.vehicleOfficeName = rs.getString("vehicle_office_name");
			vm_maintenanceDTO.vehicleOfficeNameBn = rs.getString("vehicle_office_name_bn");
			vm_maintenanceDTO.isChecked = rs.getBoolean("is_checked");
			vm_maintenanceDTO.mechanicRemarks = rs.getString("mechanic_remarks");
			vm_maintenanceDTO.mechanicApproveDate = rs.getLong("mechanic_approve_date");
			vm_maintenanceDTO.aoApproveDate = rs.getLong("ao_approve_date");
			vm_maintenanceDTO.vatPercentage = rs.getDouble("vat_percentage");
			vm_maintenanceDTO.totalWithoutVat = rs.getDouble("total_without_vat");
			vm_maintenanceDTO.totalWithVat = rs.getDouble("total_with_vat");
			vm_maintenanceDTO.totalVat = rs.getDouble("total_vat");
			vm_maintenanceDTO.requested_total = rs.getDouble("requested_total");
			vm_maintenanceDTO.paymentReceivedDate = rs.getLong("payment_received_date");
			vm_maintenanceDTO.fiscalYearId = rs.getLong("fiscal_year_id");
			vm_maintenanceDTO.sarokNumber = rs.getString("sarok_number");
			vm_maintenanceDTO.filesDropzone = rs.getLong("files_dropzone");
			vm_maintenanceDTO.paymentRemarks = rs.getString("payment_remarks");
			vm_maintenanceDTO.searchColumn = rs.getString("search_column");
			vm_maintenanceDTO.insertedByUserId = rs.getLong("inserted_by_user_id");
			vm_maintenanceDTO.insertedByOrganogramId = rs.getLong("inserted_by_organogram_id");
			vm_maintenanceDTO.insertionDate = rs.getLong("insertion_date");
			vm_maintenanceDTO.isDeleted = rs.getInt("isDeleted");
			vm_maintenanceDTO.lastModificationTime = rs.getLong("lastModificationTime");

			vm_maintenanceDTO.ao_approver_post_name = rs.getString("ao_approver_post_name");
			vm_maintenanceDTO.ao_approver_post_name_bn = rs.getString("ao_approver_post_name_bn");
			vm_maintenanceDTO.requester_post_name = rs.getString("requester_post_name");
			vm_maintenanceDTO.requester_post_name_bn = rs.getString("requester_post_name_bn");
			vm_maintenanceDTO.mecha_approver_post_name = rs.getString("mecha_approver_post_name");
			vm_maintenanceDTO.mecha_approver_post_name_bn = rs.getString("mecha_approver_post_name_bn");

			vm_maintenanceDTO.payment_receiver_post_name = rs.getString("payment_receiver_post_name");
			vm_maintenanceDTO.payment_receiver_post_name_bn = rs.getString("payment_receiver_post_name_bn");
			vm_maintenanceDTO.requester_unit_name = rs.getString("requester_unit_name");
			vm_maintenanceDTO.requester_unit_name_bn = rs.getString("requester_unit_name_bn");
			vm_maintenanceDTO.mecha_approver_unit_name = rs.getString("mecha_approver_unit_name");
			vm_maintenanceDTO.mecha_approver_unit_name_bn = rs.getString("mecha_approver_unit_name_bn");

			vm_maintenanceDTO.ao_approver_unit_name = rs.getString("ao_approver_unit_name");
			vm_maintenanceDTO.ao_approver_unit_name_bn = rs.getString("ao_approver_unit_name_bn");
			vm_maintenanceDTO.payment_receiver_unit_name = rs.getString("payment_receiver_unit_name");
			vm_maintenanceDTO.payment_receiver_unit_name_bn = rs.getString("payment_receiver_unit_name_bn");
			vm_maintenanceDTO.requester_employee_record_name = rs.getString("requester_employee_record_name");
			vm_maintenanceDTO.requester_employee_record_name_bn = rs.getString("requester_employee_record_name_bn");


			vm_maintenanceDTO.mecha_approver_employee_record_name = rs.getString("mecha_approver_employee_record_name");
			vm_maintenanceDTO.mecha_approver_employee_record_name_bn = rs.getString("mecha_approver_employee_record_name_bn");
			vm_maintenanceDTO.ao_approver_employee_record_name = rs.getString("ao_approver_employee_record_name");
			vm_maintenanceDTO.ao_approver_employee_record_name_bn = rs.getString("ao_approver_employee_record_name_bn");
			vm_maintenanceDTO.payment_receiver_employee_record_name = rs.getString("payment_receiver_employee_record_name");
			vm_maintenanceDTO.payment_receiver_employee_record_name_bn = rs.getString("payment_receiver_employee_record_name_bn");



			vm_maintenanceDTO.mecha_approver_post_id = rs.getLong("mecha_approver_post_id");
			vm_maintenanceDTO.ao_approver_post_id = rs.getLong("ao_approver_post_id");
			vm_maintenanceDTO.payment_receiver_post_id = rs.getLong("payment_receiver_post_id");
			vm_maintenanceDTO.mecha_approver_unit_id = rs.getLong("mecha_approver_unit_id");
			vm_maintenanceDTO.ao_approver_unit_id = rs.getLong("ao_approver_unit_id");
			vm_maintenanceDTO.payment_receiver_unit_id = rs.getLong("payment_receiver_unit_id");
			vm_maintenanceDTO.mecha_approver_employee_record_id = rs.getLong("mecha_approver_employee_record_id");
			vm_maintenanceDTO.ao_approver_employee_record_id = rs.getLong("ao_approver_employee_record_id");
			vm_maintenanceDTO.payment_receiver_employee_record_id = rs.getLong("payment_receiver_employee_record_id");












			return vm_maintenanceDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			ex.printStackTrace();
			return null;
		}
	}

	public Vm_maintenanceDTO buildForDashboardForMonthlyCost(ResultSet rs)
	{
		try
		{
			Vm_maintenanceDTO vm_maintenanceDTO = new Vm_maintenanceDTO();
			vm_maintenanceDTO.requested_total = rs.getDouble("requested_total");
			vm_maintenanceDTO.totalWithoutVat = rs.getDouble("total_without_vat");
			return vm_maintenanceDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			ex.printStackTrace();
			return null;
		}
	}
	
	

	public Vm_maintenanceDTO getDTOByID (long id)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID = ? ";
		Vm_maintenanceDTO vm_maintenanceDTO = ConnectionAndStatementUtil.getT(sql, Arrays.asList(id), this::build);
		try {
			VmMaintenanceItemDAO vmMaintenanceItemDAO = new VmMaintenanceItemDAO("vm_maintenance_item");			
			List<VmMaintenanceItemDTO> vmMaintenanceItemDTOList = vmMaintenanceItemDAO.getVmMaintenanceItemDTOListByVmMaintenanceID(vm_maintenanceDTO.iD);
			vm_maintenanceDTO.vmMaintenanceItemDTOList = vmMaintenanceItemDTOList;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}			
		return vm_maintenanceDTO;
	}

	


	public List<Vm_maintenanceDTO> getMonthlyCost (long startTime, long endTime)
	{
		String sql = "SELECT SUM(requested_total) AS requested_total ," +
				" SUM(total_without_vat) AS total_without_vat FROM vm_maintenance ";
		sql += " WHERE isDeleted = 0 AND ( status = 5 OR status = 6 )";
		sql += " AND ao_approve_date >= ? ";
		sql += " AND ao_approve_date < ? " ;

		return ConnectionAndStatementUtil.getListOfT(sql,Arrays.asList(startTime, endTime), this::buildForDashboardForMonthlyCost);
	}

	public List<Vm_maintenanceDTO> getAllVm_maintenanceByVehicleIdAndFiscalYear (long vehicleId, long fisaclYearId)
	{

		return Vm_maintenanceRepository.getInstance().getVm_maintenanceDTOByvehicle_id(vehicleId)
				.stream()
				.filter(i -> i.fiscalYearId == fisaclYearId).collect(Collectors.toList());

//		String sql = "SELECT * FROM " + tableName + " WHERE ";
//		sql+=" isDeleted =  0 and vehicle_id = ? and fiscal_year_id = ? ";
//		sql += " order by " + tableName + ".lastModificationTime desc";
//		return ConnectionAndStatementUtil.getListOfT(sql, Arrays.asList(vehicleId, fisaclYearId), this::build);
	}

	public List<Vm_maintenanceDTO> getAllApprovedVm_maintenanceByVehicleIdAndFiscalYear
			(long vehicleId, List<Long> fiscalYearIds, Boolean flag)
	{

		List<Vm_maintenanceDTO> dtos = Vm_maintenanceRepository.getInstance().getVm_maintenanceDTOByvehicle_id(vehicleId)
				.stream()
				.filter(i -> i.status == 5 || i.status == 6)
				.collect(Collectors.toList());

		if(flag){
			dtos = dtos.stream()
					.filter(i -> fiscalYearIds.contains(i.fiscalYearId))
					.collect(Collectors.toList());
		}

		return dtos;

//		String sql = "SELECT * FROM " + tableName + " WHERE ";
//		sql+=" isDeleted =  0 and (status = 5 or status = 6) and vehicle_id = ? "  ;
//		List<Long> ids = new ArrayList<>();
//		ids.add(vehicleId);
//;
//		if(flag){
//			sql +=  " and fiscal_year_id IN ( ";
//			for(int i = 0;i<fiscalYearIds.size();i++){
//				if(i!=0){
//					sql+=",";
//				}
//				sql+= " ? ";
//				ids.add(fiscalYearIds.get(i));
//			}
//			sql+=" ) ";
//		}
////				+ " and fiscal_year_id = " + fisaclYearId;
//		sql += " order by " + tableName + ".lastModificationTime desc";
//		System.out.println(sql);
//		return ConnectionAndStatementUtil.getListOfT(sql, Arrays.asList(ids.toArray()), this::build);
	}

	

	public String getSqlWithSearchCriteriaForNormalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
			UserDTO userDTO, String filter, boolean tableHasJobCat,  List<Object> objectList)
    {
		boolean viewAll = false;
		
		if(p_searchCriteria != null && p_searchCriteria.get("ViewAll") != null)
		{
			System.out.println("ViewAll = " + p_searchCriteria.get("ViewAll"));
			viewAll = true;
		}
		else
		{
			System.out.println("ViewAll is null ");
		}
		
		String sql = "SELECT ";
		if(category == GETIDS)
		{
			sql += " " + tableName + ".ID as ID ";
		}
		else if(category == GETCOUNT)
		{
			sql += " count( " + tableName + ".ID) as countID ";
		}
		else if(category == GETDTOS)
		{
			sql += " distinct " + tableName + ".* ";
		}
		sql += "FROM " + tableName + " ";
		sql += joinSQL;
		

		
		String AnyfieldSql = "";
		String AllFieldSql = "";
		
		if(p_searchCriteria != null)
		{
						
			
			Enumeration names = p_searchCriteria.keys();
			String str, value;
			
			AnyfieldSql = "(";
			
			if(p_searchCriteria.get("AnyField")!= null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase(""))
			{
				AnyfieldSql+= tableName + ".search_column like ? ";
				objectList.add("%" + p_searchCriteria.get("AnyField").toString() + "%");
			}
			AnyfieldSql += ")";
			System.out.println("AnyfieldSql = " + AnyfieldSql);
			
			AllFieldSql = "(";
			int i = 0;
			while(names.hasMoreElements())
			{				
				str = (String) names.nextElement();
				value = (String)p_searchCriteria.get(str);
		        System.out.println(str + ": " + value);
				if(value != null && !value.equalsIgnoreCase("") && (
						 str.equals("vehicle_type_cat")
//						|| str.equals("application_date_start")
//						|| str.equals("application_date_end")
//						|| str.equals("last_maintenance_date_start")
//						|| str.equals("last_maintenance_date_end")
//						|| str.equals("vehicle_office_name")
//						|| str.equals("vehicle_office_name_bn")
//						|| str.equals("mechanic_remarks")
//						|| str.equals("mechanic_approve_date_start")
//						|| str.equals("mechanic_approve_date_end")
//						|| str.equals("ao_approve_date_start")
//						|| str.equals("ao_approve_date_end")
//						|| str.equals("payment_received_date_start")
//						|| str.equals("payment_received_date_end")
//						|| str.equals("sarok_number")
//						|| str.equals("payment_remarks")
//						|| str.equals("insertion_date_start")
//						|| str.equals("insertion_date_end")
				)
						
				)
				{
					if(p_searchCriteria.get(str).equals("any"))
					{
						continue;
					}

					if( i > 0)
					{
						AllFieldSql+= " AND  ";
					}
					
					 if(str.equals("vehicle_type_cat"))
					{
						AllFieldSql += "" + tableName + ".vehicle_type_cat = ? ";
						objectList.add(p_searchCriteria.get(str));
						i ++;
					}
//					else if(str.equals("application_date_start"))
//					{
//						AllFieldSql += "" + tableName + ".application_date >= " + p_searchCriteria.get(str);
//						i ++;
//					}
//					else if(str.equals("application_date_end"))
//					{
//						AllFieldSql += "" + tableName + ".application_date <= " + p_searchCriteria.get(str);
//						i ++;
//					}
//					else if(str.equals("last_maintenance_date_start"))
//					{
//						AllFieldSql += "" + tableName + ".last_maintenance_date >= " + p_searchCriteria.get(str);
//						i ++;
//					}
//					else if(str.equals("last_maintenance_date_end"))
//					{
//						AllFieldSql += "" + tableName + ".last_maintenance_date <= " + p_searchCriteria.get(str);
//						i ++;
//					}
//					else if(str.equals("vehicle_office_name"))
//					{
//						AllFieldSql += "" + tableName + ".vehicle_office_name like '%" + p_searchCriteria.get(str) + "%'";
//						i ++;
//					}
//					else if(str.equals("vehicle_office_name_bn"))
//					{
//						AllFieldSql += "" + tableName + ".vehicle_office_name_bn like '%" + p_searchCriteria.get(str) + "%'";
//						i ++;
//					}
//					else if(str.equals("mechanic_remarks"))
//					{
//						AllFieldSql += "" + tableName + ".mechanic_remarks like '%" + p_searchCriteria.get(str) + "%'";
//						i ++;
//					}
//					else if(str.equals("mechanic_approve_date_start"))
//					{
//						AllFieldSql += "" + tableName + ".mechanic_approve_date >= " + p_searchCriteria.get(str);
//						i ++;
//					}
//					else if(str.equals("mechanic_approve_date_end"))
//					{
//						AllFieldSql += "" + tableName + ".mechanic_approve_date <= " + p_searchCriteria.get(str);
//						i ++;
//					}
//					else if(str.equals("ao_approve_date_start"))
//					{
//						AllFieldSql += "" + tableName + ".ao_approve_date >= " + p_searchCriteria.get(str);
//						i ++;
//					}
//					else if(str.equals("ao_approve_date_end"))
//					{
//						AllFieldSql += "" + tableName + ".ao_approve_date <= " + p_searchCriteria.get(str);
//						i ++;
//					}
//					else if(str.equals("payment_received_date_start"))
//					{
//						AllFieldSql += "" + tableName + ".payment_received_date >= " + p_searchCriteria.get(str);
//						i ++;
//					}
//					else if(str.equals("payment_received_date_end"))
//					{
//						AllFieldSql += "" + tableName + ".payment_received_date <= " + p_searchCriteria.get(str);
//						i ++;
//					}
//					else if(str.equals("sarok_number"))
//					{
//						AllFieldSql += "" + tableName + ".sarok_number like '%" + p_searchCriteria.get(str) + "%'";
//						i ++;
//					}
//					else if(str.equals("payment_remarks"))
//					{
//						AllFieldSql += "" + tableName + ".payment_remarks like '%" + p_searchCriteria.get(str) + "%'";
//						i ++;
//					}
//					else if(str.equals("insertion_date_start"))
//					{
//						AllFieldSql += "" + tableName + ".insertion_date >= " + p_searchCriteria.get(str);
//						i ++;
//					}
//					else if(str.equals("insertion_date_end"))
//					{
//						AllFieldSql += "" + tableName + ".insertion_date <= " + p_searchCriteria.get(str);
//						i ++;
//					}
					
					
				}
			}
			
			AllFieldSql += ")";
			System.out.println("AllFieldSql = " + AllFieldSql);
			
			
		}
		
		
		sql += " WHERE ";
		
		sql += " (" + tableName + ".isDeleted = 0 ";			
		sql += ")";
		
		
		if(!filter.equalsIgnoreCase(""))
		{
			sql += " and " + filter + " ";
		}
		
		if(!AnyfieldSql.equals("()") && !AnyfieldSql.equals(""))
		{
			sql += " AND " + AnyfieldSql;
			
		}
		if(!AllFieldSql.equals("()") && !AllFieldSql.equals(""))
		{			
			sql += " AND " + AllFieldSql;
		}
		
	
			
		sql += " order by " + tableName + ".lastModificationTime desc ";

		printSql(sql);
		
		if(limit >= 0)
		{
			sql += " limit " + limit;
		}
		if(offset >= 0)
		{
			sql += " offset " + offset;
		}
		
		System.out.println("-------------- sql = " + sql);
		
		return sql;
    }
	

				
}
	