package vm_maintenance;
import java.util.*; 
import util.*; 


public class Vm_maintenanceDTO extends CommonDTO
{

	public int vehicleTypeCat = -1;
	public int status = -1;
	public long vehicleId = -1;
	public long requesterPostId = -1;
	public long requesterUnitId = -1;
	public long requesterEmployeeRecordId = -1;
	public long vehicleDriverAssignmentId = -1;
	public long applicationDate = -1;
	public long lastMaintenanceDate = -1;
    public String vehicleOfficeName = "";
    public String vehicleOfficeNameBn = "";
	public boolean isChecked = false;
    public String mechanicRemarks = "";
	public long mechanicApproveDate = -1;
	public long aoApproveDate = -1;
	public double vatPercentage = -1;
	public double totalWithoutVat = -1;
	public double totalWithVat = -1;
	public double totalVat = -1;
	public long paymentReceivedDate = -1;
	public long fiscalYearId = -1;
    public String sarokNumber = "";
	public long filesDropzone = -1;
    public String paymentRemarks = "";
	public long insertedByUserId = -1;
	public long insertedByOrganogramId = -1;
	public long insertionDate = -1;


	public String ao_approver_post_name = "";
	public String ao_approver_post_name_bn = "";
	public String requester_post_name = "";
	public String requester_post_name_bn = "";
	public String mecha_approver_post_name = "";
	public String mecha_approver_post_name_bn = "";
	public String payment_receiver_post_name = "";
	public String payment_receiver_post_name_bn = "";
	public String requester_unit_name = "";
	public String requester_unit_name_bn = "";
	public String mecha_approver_unit_name = "";
	public String mecha_approver_unit_name_bn = "";
	public String ao_approver_unit_name = "";
	public String ao_approver_unit_name_bn = "";
	public String payment_receiver_unit_name = "";
	public String payment_receiver_unit_name_bn = "";
	public String requester_employee_record_name = "";
	public String requester_employee_record_name_bn = "";
	public String mecha_approver_employee_record_name = "";
	public String mecha_approver_employee_record_name_bn = "";
	public String ao_approver_employee_record_name = "";
	public String ao_approver_employee_record_name_bn = "";
	public String payment_receiver_employee_record_name = "";
	public String payment_receiver_employee_record_name_bn = "";


	public long mecha_approver_post_id = -1;
	public long ao_approver_post_id = -1;
	public long payment_receiver_post_id = -1;
	public long mecha_approver_unit_id = -1;
	public long ao_approver_unit_id = -1;
	public long payment_receiver_unit_id = -1;
	public long mecha_approver_employee_record_id = -1;
	public long ao_approver_employee_record_id = -1;
	public long payment_receiver_employee_record_id = -1;


	public double requested_total = 0;




















	
	public List<VmMaintenanceItemDTO> vmMaintenanceItemDTOList = new ArrayList<>();
	
    @Override
	public String toString() {
            return "$Vm_maintenanceDTO[" +
            " iD = " + iD +
            " vehicleTypeCat = " + vehicleTypeCat +
            " status = " + status +
            " vehicleId = " + vehicleId +
            " requesterPostId = " + requesterPostId +
            " requesterUnitId = " + requesterUnitId +
            " requesterEmployeeRecordId = " + requesterEmployeeRecordId +
            " vehicleDriverAssignmentId = " + vehicleDriverAssignmentId +
            " applicationDate = " + applicationDate +
            " lastMaintenanceDate = " + lastMaintenanceDate +
            " vehicleOfficeName = " + vehicleOfficeName +
            " vehicleOfficeNameBn = " + vehicleOfficeNameBn +
            " isChecked = " + isChecked +
            " mechanicRemarks = " + mechanicRemarks +
            " mechanicApproveDate = " + mechanicApproveDate +
            " aoApproveDate = " + aoApproveDate +
            " vatPercentage = " + vatPercentage +
            " totalWithoutVat = " + totalWithoutVat +
            " totalWithVat = " + totalWithVat +
            " totalVat = " + totalVat +
            " paymentReceivedDate = " + paymentReceivedDate +
            " fiscalYearId = " + fiscalYearId +
            " sarokNumber = " + sarokNumber +
            " filesDropzone = " + filesDropzone +
            " paymentRemarks = " + paymentRemarks +
            " searchColumn = " + searchColumn +
            " insertedByUserId = " + insertedByUserId +
            " insertedByOrganogramId = " + insertedByOrganogramId +
            " insertionDate = " + insertionDate +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}