package vm_maintenance;

/*
 * @author Md. Erfan Hossain
 * @created 03/05/2021 - 5:55 PM
 * @project parliament
 */

public class DuplicateVm_maintenance_requestInfoException extends Exception{

    public DuplicateVm_maintenance_requestInfoException(String message) {
        super(message);
    }
}
