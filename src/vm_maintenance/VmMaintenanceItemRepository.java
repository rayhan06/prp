package vm_maintenance;

import com.google.gson.Gson;
import org.apache.log4j.Logger;
import repository.Repository;
import repository.RepositoryManager;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public class VmMaintenanceItemRepository implements Repository {
    VmMaintenanceItemDAO vmMaintenanceItemDAO = null;

    public void setDAO(VmMaintenanceItemDAO vmMaintenanceItemDAO)
    {
        this.vmMaintenanceItemDAO = vmMaintenanceItemDAO;
    }


    static Logger logger = Logger.getLogger(VmMaintenanceItemRepository.class);

    Map<Long, VmMaintenanceItemDTO> mapOfVmMaintenanceItemDTOToiD;
    Map<Long, Set<VmMaintenanceItemDTO>>mapOfVmMaintenanceItemDTOToMaintenanceID;
    Gson gson;



    static VmMaintenanceItemRepository instance = null;
    private VmMaintenanceItemRepository(){
        mapOfVmMaintenanceItemDTOToiD = new ConcurrentHashMap<>();
        mapOfVmMaintenanceItemDTOToMaintenanceID =  new ConcurrentHashMap<>();
        gson = new Gson();

        setDAO(new VmMaintenanceItemDAO());
        RepositoryManager.getInstance().addRepository(this);
    }

    public synchronized static VmMaintenanceItemRepository getInstance(){
        if (instance == null){
            instance = new VmMaintenanceItemRepository();
        }
        return instance;
    }

    public void reload(boolean reloadAll){
        if(vmMaintenanceItemDAO == null)
        {
            return;
        }
        try {
            List<VmMaintenanceItemDTO> dtos =(List<VmMaintenanceItemDTO>) vmMaintenanceItemDAO.getAll(reloadAll);
            for(VmMaintenanceItemDTO dto : dtos) {
                VmMaintenanceItemDTO oldDTO = getVmMaintenanceItemDTOByIDWithoutClone(dto.iD);
                if( oldDTO != null ) {
                    mapOfVmMaintenanceItemDTOToiD.remove(oldDTO.iD);

                    if(mapOfVmMaintenanceItemDTOToMaintenanceID.containsKey(oldDTO.vmMaintenanceId)) {
                        mapOfVmMaintenanceItemDTOToMaintenanceID.get(oldDTO.vmMaintenanceId).remove(oldDTO);
                    }
                    if(mapOfVmMaintenanceItemDTOToMaintenanceID.get(oldDTO.vmMaintenanceId).isEmpty()) {
                        mapOfVmMaintenanceItemDTOToMaintenanceID.remove(oldDTO.vmMaintenanceId);
                    }
                }
                if(dto.isDeleted == 0)
                {

                    mapOfVmMaintenanceItemDTOToiD.put(dto.iD, dto);

                    if( ! mapOfVmMaintenanceItemDTOToMaintenanceID.containsKey(dto.vmMaintenanceId)) {
                        mapOfVmMaintenanceItemDTOToMaintenanceID.put(dto.vmMaintenanceId, new HashSet<>());
                    }
                    mapOfVmMaintenanceItemDTOToMaintenanceID.get(dto.vmMaintenanceId).add(dto);

                }
            }

        } catch (Exception e) {
            logger.debug("FATAL", e);
        }
    }

    public List<VmMaintenanceItemDTO> getVmMaintenanceItemDTOByMaintenanceId(long maintenanceId) {
        return clone(new ArrayList<>( mapOfVmMaintenanceItemDTOToMaintenanceID.
                getOrDefault(maintenanceId,new HashSet<>())));
    }

    public List<VmMaintenanceItemDTO> getVmMaintenanceItemDTOs() {
        List <VmMaintenanceItemDTO> vmRouteStoppageDTOS = new ArrayList<>(this.mapOfVmMaintenanceItemDTOToiD.values());
        return clone(vmRouteStoppageDTOS);
    }

    public VmMaintenanceItemDTO clone(VmMaintenanceItemDTO dto) {
        String raw = gson.toJson(dto);
        return gson.fromJson(raw, VmMaintenanceItemDTO.class);
    }

    public List<VmMaintenanceItemDTO> clone(List<VmMaintenanceItemDTO> dtoList) {
        return dtoList
                .stream()
                .map(dto -> clone(dto))
                .collect(Collectors.toList());
    }

    public VmMaintenanceItemDTO getVmMaintenanceItemDTOByID( long ID){
        return clone(mapOfVmMaintenanceItemDTOToiD.get(ID));
    }

    public VmMaintenanceItemDTO getVmMaintenanceItemDTOByIDWithoutClone( long ID){
        return mapOfVmMaintenanceItemDTOToiD.get(ID);
    }

    @Override
    public String getTableName() {
        String tableName = "";
        try{
            tableName = "vm_maintenance_item";
        }catch(Exception ex){
            logger.debug("FATAL",ex);
        }
        return tableName;
    }


}
