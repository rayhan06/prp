package vm_maintenance;

import org.apache.log4j.Logger;
import repository.Repository;
import repository.RepositoryManager;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;


public class Vm_maintenance_request_approval_mappingRepository implements Repository {
	Vm_maintenance_request_approval_mappingDAO card_approval_mappingDAO = null;
	
	public void setDAO(Vm_maintenance_request_approval_mappingDAO card_approval_mappingDAO)
	{
		this.card_approval_mappingDAO = card_approval_mappingDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Vm_maintenance_request_approval_mappingRepository.class);
	Map<Long, Vm_maintenance_request_approval_mappingDTO>mapOfVm_fuel_request_approval_mappingDTOToid;
	Map<Long, Set<Vm_maintenance_request_approval_mappingDTO> >mapOfVm_fuel_request_approval_mappingDTOTovm_fuel_requestId;
	Map<Long, Set<Vm_maintenance_request_approval_mappingDTO> >mapOfVm_fuel_request_approval_mappingDTOTovm_fuel_requestApprovalId;


	static Vm_maintenance_request_approval_mappingRepository instance = null;
	private Vm_maintenance_request_approval_mappingRepository(){
		mapOfVm_fuel_request_approval_mappingDTOToid = new ConcurrentHashMap<>();
		mapOfVm_fuel_request_approval_mappingDTOTovm_fuel_requestId = new ConcurrentHashMap<>();
		mapOfVm_fuel_request_approval_mappingDTOTovm_fuel_requestApprovalId = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Vm_maintenance_request_approval_mappingRepository getInstance(){
		if (instance == null){
			instance = new Vm_maintenance_request_approval_mappingRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(card_approval_mappingDAO == null)
		{
			return;
		}
		try {
			List<Vm_maintenance_request_approval_mappingDTO> card_approval_mappingDTOs = card_approval_mappingDAO.getAllVm_fuel_request_approval_mapping(reloadAll);
			for(Vm_maintenance_request_approval_mappingDTO card_approval_mappingDTO : card_approval_mappingDTOs) {
				Vm_maintenance_request_approval_mappingDTO oldVm_fuel_request_approval_mappingDTO = getVm_fuel_request_approval_mappingDTOByid(card_approval_mappingDTO.id);
				if( oldVm_fuel_request_approval_mappingDTO != null ) {
					mapOfVm_fuel_request_approval_mappingDTOToid.remove(oldVm_fuel_request_approval_mappingDTO.id);
				
					if(mapOfVm_fuel_request_approval_mappingDTOTovm_fuel_requestId.containsKey(oldVm_fuel_request_approval_mappingDTO.vm_fuel_requestId)) {
						mapOfVm_fuel_request_approval_mappingDTOTovm_fuel_requestId.get(oldVm_fuel_request_approval_mappingDTO.vm_fuel_requestId).remove(oldVm_fuel_request_approval_mappingDTO);
					}
					if(mapOfVm_fuel_request_approval_mappingDTOTovm_fuel_requestId.get(oldVm_fuel_request_approval_mappingDTO.vm_fuel_requestId).isEmpty()) {
						mapOfVm_fuel_request_approval_mappingDTOTovm_fuel_requestId.remove(oldVm_fuel_request_approval_mappingDTO.vm_fuel_requestId);
					}
					
					if(mapOfVm_fuel_request_approval_mappingDTOTovm_fuel_requestApprovalId.containsKey(oldVm_fuel_request_approval_mappingDTO.vm_fuel_requestApprovalId)) {
						mapOfVm_fuel_request_approval_mappingDTOTovm_fuel_requestApprovalId.get(oldVm_fuel_request_approval_mappingDTO.vm_fuel_requestApprovalId).remove(oldVm_fuel_request_approval_mappingDTO);
					}
					if(mapOfVm_fuel_request_approval_mappingDTOTovm_fuel_requestApprovalId.get(oldVm_fuel_request_approval_mappingDTO.vm_fuel_requestApprovalId).isEmpty()) {
						mapOfVm_fuel_request_approval_mappingDTOTovm_fuel_requestApprovalId.remove(oldVm_fuel_request_approval_mappingDTO.vm_fuel_requestApprovalId);
					}
					
					
				}
				if(card_approval_mappingDTO.isDeleted == 0) 
				{
					
					mapOfVm_fuel_request_approval_mappingDTOToid.put(card_approval_mappingDTO.id, card_approval_mappingDTO);
				
					if( ! mapOfVm_fuel_request_approval_mappingDTOTovm_fuel_requestId.containsKey(card_approval_mappingDTO.vm_fuel_requestId)) {
						mapOfVm_fuel_request_approval_mappingDTOTovm_fuel_requestId.put(card_approval_mappingDTO.vm_fuel_requestId, new HashSet<>());
					}
					mapOfVm_fuel_request_approval_mappingDTOTovm_fuel_requestId.get(card_approval_mappingDTO.vm_fuel_requestId).add(card_approval_mappingDTO);
					
					if( ! mapOfVm_fuel_request_approval_mappingDTOTovm_fuel_requestApprovalId.containsKey(card_approval_mappingDTO.vm_fuel_requestApprovalId)) {
						mapOfVm_fuel_request_approval_mappingDTOTovm_fuel_requestApprovalId.put(card_approval_mappingDTO.vm_fuel_requestApprovalId, new HashSet<>());
					}
					mapOfVm_fuel_request_approval_mappingDTOTovm_fuel_requestApprovalId.get(card_approval_mappingDTO.vm_fuel_requestApprovalId).add(card_approval_mappingDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Vm_maintenance_request_approval_mappingDTO> getVm_fuel_request_approval_mappingList() {
		List <Vm_maintenance_request_approval_mappingDTO> card_approval_mappings = new ArrayList<Vm_maintenance_request_approval_mappingDTO>(this.mapOfVm_fuel_request_approval_mappingDTOToid.values());
		return card_approval_mappings;
	}
	
	
	public Vm_maintenance_request_approval_mappingDTO getVm_fuel_request_approval_mappingDTOByid(long id){
		return mapOfVm_fuel_request_approval_mappingDTOToid.get(id);
	}
	
	
	public List<Vm_maintenance_request_approval_mappingDTO> getVm_fuel_request_approval_mappingDTOBycard_info_id(long card_info_id) {
		return new ArrayList<>( mapOfVm_fuel_request_approval_mappingDTOTovm_fuel_requestId.getOrDefault(card_info_id,new HashSet<>()));
	}
	
	
	public List<Vm_maintenance_request_approval_mappingDTO> getVm_fuel_request_approval_mappingDTOBycard_approval_id(long card_approval_id) {
		return new ArrayList<>( mapOfVm_fuel_request_approval_mappingDTOTovm_fuel_requestApprovalId.getOrDefault(card_approval_id,new HashSet<>()));
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "card_approval_mapping";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


