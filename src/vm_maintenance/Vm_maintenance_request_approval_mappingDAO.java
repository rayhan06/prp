package vm_maintenance;

import common.ConnectionAndStatementUtil;
import employee_offices.EmployeeOfficeDTO;
import employee_offices.EmployeeOfficeRepository;
import employee_records.EmployeeCommonDTOService;
import org.apache.log4j.Logger;
import pb.CatRepository;
import pb.CategoryLanguageModel;
import task_type_approval_path.TaskTypeApprovalPathDTO;
import task_type_approval_path.TaskTypeApprovalPathRepository;
import user.UserDTO;
import util.CommonDTO;
import util.LockManager;
import util.NavigationService4;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;

@SuppressWarnings("ALL")
public class Vm_maintenance_request_approval_mappingDAO extends NavigationService4 implements EmployeeCommonDTOService<Vm_maintenance_request_approval_mappingDTO> {
    private static final Logger logger = Logger.getLogger(Vm_maintenance_request_approval_mappingDAO.class);

    private static final String updateSqlQuery = "UPDATE {tableName} SET vm_fuel_request_approval_status_cat = ?,sequence = ?, modified_by = ?,lastModificationTime = ?," +
            "isDeleted = ? WHERE id = ?";
    private static final String getByVm_fuel_requestInfoId = "SELECT * FROM vm_maintenance_request_approval_mapping WHERE vm_fuel_request_id = %d AND isDeleted = 0 ORDER BY sequence DESC";
    private static final String getByVm_fuel_requestInfoIdAndApproverEmployeeRecordsId = "SELECT * FROM vm_maintenance_request_approval_mapping WHERE vm_fuel_request_id = %d AND approver_employee_records_id = %d";
    private static final String getByVm_fuel_requestInfoIdAndStatus = "SELECT * FROM vm_maintenance_request_approval_mapping WHERE vm_fuel_request_id = %d AND vm_fuel_request_approval_status_cat = %d AND isDeleted = 0";
    private static final String updateStatus = "UPDATE vm_maintenance_request_approval_mapping SET vm_fuel_request_approval_status_cat = %d,isDeleted = %d,modified_by = %d,lastModificationTime = %d WHERE id IN (%s)";
    private static final String updateStatusByVm_fuel_requestInfoId = "UPDATE vm_maintenance_request_approval_mapping SET vm_fuel_request_approval_status_cat = %d,isDeleted = %d," +
            "modified_by = %d,lastModificationTime = %d WHERE vm_fuel_request_approval_status_cat = %d AND vm_fuel_request_id = %d";

    private static final Map<String, String> searchMap = new HashMap<>();

    static {
        searchMap.put("vm_fuel_request_approval_status_cat", "and (vm_fuel_request_approval_status_cat = ?)");
        searchMap.put("employee_records_id_internal", "and (approver_employee_records_id = ?)");
    }

    public Vm_maintenance_request_approval_mappingDAO(String tableName) {
        super(tableName);
        useSafeSearch= true;
        commonMaps = new Vm_maintenance_request_approval_mappingMAPS(tableName);
    }

    public Vm_maintenance_request_approval_mappingDAO() {
        this("vm_maintenance_request_approval_mapping");
    }

    public void setSearchColumn(Vm_maintenance_request_approval_mappingDTO vm_fuel_request_approval_mappingDTO) {
        CategoryLanguageModel model = CatRepository.getInstance().getCategoryLanguageModel("card_approval_status", vm_fuel_request_approval_mappingDTO.vm_fuel_requestApprovalStatusCat);
        vm_fuel_request_approval_mappingDTO.searchColumn = model.englishText + " " + model.banglaText;
    }

    private static final String addSqlQuery = "INSERT INTO {tableName} (vm_fuel_request_approval_status_cat,sequence,modified_by,lastModificationTime," +
            "vm_fuel_request_approval_id,vm_fuel_request_id,inserted_by,insertion_time,task_type_id," +
            "approver_employee_records_id,isDeleted,id) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";

    @Override
    public void set(PreparedStatement ps, Vm_maintenance_request_approval_mappingDTO dto, boolean isInsert) throws SQLException {
        int index = 0;
        ps.setInt(++index, dto.vm_fuel_requestApprovalStatusCat);
        ps.setInt(++index, dto.sequence);
        ps.setLong(++index, dto.modifiedBy);
        ps.setLong(++index, dto.lastModificationTime);
        if (isInsert) {
            ps.setLong(++index, dto.vm_fuel_requestApprovalId);
            ps.setLong(++index, dto.vm_fuel_requestId);
            ps.setLong(++index, dto.insertedBy);
            ps.setLong(++index, dto.insertionTime);
            ps.setLong(++index, dto.taskTypeId);
            ps.setLong(++index, dto.approverEmployeeRecordsId);
        }
        ps.setInt(++index, dto.isDeleted);
        ps.setLong(++index, dto.iD);
    }


    @Override
    public Vm_maintenance_request_approval_mappingDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Vm_maintenance_request_approval_mappingDTO dto = new Vm_maintenance_request_approval_mappingDTO();
            dto.iD = rs.getLong("id");
            dto.vm_fuel_requestId = rs.getLong("vm_fuel_request_id");
            dto.vm_fuel_requestApprovalId = rs.getLong("vm_fuel_request_approval_id");
            dto.vm_fuel_requestApprovalStatusCat = rs.getInt("vm_fuel_request_approval_status_cat");
            dto.sequence = rs.getInt("sequence");
            dto.taskTypeId = rs.getLong("task_type_id");
            dto.insertedBy = rs.getLong("inserted_by");
            dto.insertionTime = rs.getLong("insertion_time");
            dto.modifiedBy = rs.getLong("modified_by");
            dto.lastModificationTime = rs.getLong("lastModificationTime");
            dto.isDeleted = rs.getInt("isDeleted");
            dto.approverEmployeeRecordsId = rs.getInt("approver_employee_records_id");
            return dto;
        } catch (SQLException ex) {
            logger.error(ex);
            ex.printStackTrace();
            return null;
        }
    }

    @Override
    public String getTableName() {
        return tableName;
    }

    @Override
    public long add(CommonDTO dto) throws Exception {
        return executeAddOrUpdateQuery((Vm_maintenance_request_approval_mappingDTO) dto, addSqlQuery, true);
    }

    @Override
    public long update(CommonDTO dto) throws Exception {
        return executeAddOrUpdateQuery((Vm_maintenance_request_approval_mappingDTO) dto, updateSqlQuery, false);
    }

    public Vm_maintenance_request_approval_mappingDTO getDTOByID(long ID) {
        return getDTOFromID(ID);
    }

    public List<Vm_maintenance_request_approval_mappingDTO> getAllVm_fuel_request_approval_mapping(boolean isFirstReload) {
        return getAllDTOs(isFirstReload);
    }

    public List<Vm_maintenance_request_approval_mappingDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO) {
        return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
    }


    public List<Vm_maintenance_request_approval_mappingDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable,
                                                                    UserDTO userDTO, String filter, boolean tableHasJobCat) {
        List<Object> objectList = new ArrayList<>();
        String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat,objectList);
        return getDTOs(sql,objectList);

    }

    public String getSqlWithSearchCriteriaForNormalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
                                                          UserDTO userDTO, String filter, boolean tableHasJobCat,List<Object>objectList) {
        if (filter != null && filter.trim().length() > 0) {
            p_searchCriteria.put("employee_records_id_internal", String.valueOf(userDTO.employee_record_id));
        }
        return getSearchQuery(tableName, p_searchCriteria, limit, offset, category, searchMap,objectList);
    }

    public String getSqlWithSearchCriteria(Hashtable p_searchCriteria, int limit, int offset, int category,
                                           boolean isPermanentTable, UserDTO userDTO, String filter, boolean tableHasJobCat,List<Object>objectList) {
        return getSqlWithSearchCriteriaForNormalSearch(p_searchCriteria, limit, offset, category, userDTO, filter, tableHasJobCat,objectList);
    }

    public Vm_maintenance_requestApprovalResponse createVm_fuel_requestApproval(CreateVm_maintenance_requestApprovalModel model) throws Exception {
        String sql = String.format(getByVm_fuel_requestInfoId, model.getVm_fuel_requestInfoId());
        Vm_maintenance_request_approval_mappingDTO dto = ConnectionAndStatementUtil.getT(sql, this::buildObjectFromResultSet);
        if (dto != null) {
            throw new DuplicateVm_maintenance_requestInfoException("Approval is already created for " + model.getVm_fuel_requestInfoId());
        }
        Vm_maintenance_requestApprovalResponse cardApprovalResponse = new Vm_maintenance_requestApprovalResponse();
        EmployeeOfficeDTO employeeOfficeDTO = EmployeeOfficeRepository.getInstance().getByEmployeeRecordIdIsDefault(model.getRequesterEmployeeRecordId());
//        OfficeUnitOrganograms officeUnitOrganograms = OfficeUnitOrganogramsRepository.getInstance().getById(employeeOfficeDTO.officeUnitOrganogramId);
        if (true) {
            Vm_maintenance_request_approval_mappingDTO cardApprovalMappingDTO;


            int nextLevel = 1;
            List<TaskTypeApprovalPathDTO> nextApprovalPath = TaskTypeApprovalPathRepository.getInstance().getByTaskTypeIdAndLevel(model.getTaskTypeId(), nextLevel);


            if (nextApprovalPath == null || nextApprovalPath.size() == 0) {
                cardApprovalResponse.hasNextApproval = false;
                cardApprovalResponse.organogramIds = null;
            } else {

                List<Long> organogramIds = add(nextApprovalPath, model, nextLevel);
                cardApprovalResponse.hasNextApproval = true;
                cardApprovalResponse.organogramIds = organogramIds;
            }
        }
//        else {
//            TaskTypeApprovalPathDTO taskTypeApprovalPathDTO = new TaskTypeApprovalPathDTO();
//            taskTypeApprovalPathDTO.officeUnitOrganogramId = officeUnitOrganograms.superior_designation_id;
//            List<Long> organogramIds = add(Collections.singletonList(taskTypeApprovalPathDTO), model, 1);
//            cardApprovalResponse.hasNextApproval = true;
//            cardApprovalResponse.organogramIds = organogramIds;
//        }
        return cardApprovalResponse;
    }

    private Vm_maintenance_request_approval_mappingDTO buildDTO(long approverEmployeeRecordId, CreateVm_maintenance_requestApprovalModel model, int level,
                                                                int isDeleted, ApprovalStatus approvalStatus) {
        Vm_maintenance_request_approval_mappingDTO cardApprovalMappingDTO = new Vm_maintenance_request_approval_mappingDTO();
        Vm_maintenance_requestApprovalDTO cardApprovalDTO = Vm_maintenance_requestApprovalRepository.getInstance().getByEmployeeRecordId(approverEmployeeRecordId, model.getRequesterEmployeeRecordId());
        if (cardApprovalDTO == null) {
            return null;
        }
        cardApprovalMappingDTO.vm_fuel_requestId = model.getVm_fuel_requestInfoId();
        cardApprovalMappingDTO.vm_fuel_requestApprovalId = cardApprovalDTO.iD;
        cardApprovalMappingDTO.vm_fuel_requestApprovalStatusCat = approvalStatus.getValue();
        cardApprovalMappingDTO.sequence = level;
        cardApprovalMappingDTO.taskTypeId = model.getTaskTypeId();
        cardApprovalMappingDTO.insertedBy = cardApprovalMappingDTO.modifiedBy = model.getRequesterEmployeeRecordId();
        cardApprovalMappingDTO.insertionTime = cardApprovalMappingDTO.lastModificationTime = System.currentTimeMillis();
        cardApprovalMappingDTO.isDeleted = isDeleted;
        cardApprovalMappingDTO.approverEmployeeRecordsId = cardApprovalDTO.employeeRecordId;
        return cardApprovalMappingDTO;
    }

    private List<Long> add(List<TaskTypeApprovalPathDTO> nextApprovalPath, CreateVm_maintenance_requestApprovalModel model, int level) {
        long currentTime = System.currentTimeMillis();
        List<Long> addedToApproval = new ArrayList<>();
        nextApprovalPath.forEach(approver -> {
            EmployeeOfficeDTO approverOfficeDTO = EmployeeOfficeRepository.getInstance().getByOfficeUnitOrganogramId(approver.officeUnitOrganogramId);
            if (approverOfficeDTO != null) {
                Vm_maintenance_request_approval_mappingDTO dto = buildDTO(approverOfficeDTO.employeeRecordId, model, level, 0, ApprovalStatus.PENDING);
                if (dto != null) {
                    try {
                        add(dto);
                        addedToApproval.add(approverOfficeDTO.officeUnitOrganogramId);
                    } catch (Exception e) {
                        logger.error(e);
                        e.printStackTrace();
                    }
                }
            }
        });
        return addedToApproval;
    }

    public Vm_maintenance_requestApprovalResponse movedToNextLevelOfApproval(CreateVm_maintenance_requestApprovalModel model) throws InvalidDataException, AlreadyApprovedException {
        Vm_maintenance_requestApprovalResponse response = new Vm_maintenance_requestApprovalResponse();
        synchronized (LockManager.getLock(model.getVm_fuel_requestInfoId()+"CVMRAM")) {
            ValidateResponse validateResponse = validate(model);
            String ids = validateResponse.dtoList.stream()
                    .map(e -> String.valueOf(e.iD))
                    .collect(Collectors.joining(","));
            String sql2 = String.format(updateStatus, ApprovalStatus.SATISFIED.getValue(), 0, model.getRequesterEmployeeRecordId(), System.currentTimeMillis(), ids);

            boolean res = (Boolean) ConnectionAndStatementUtil.getWriteStatement(st -> {
                try {
                    st.executeUpdate(sql2);
                    return true;
                } catch (SQLException ex) {
                    logger.error(ex);
                    return false;
                }
            });
            if (!res) {
                throw new InvalidDataException("Exception occurred during updating approval status");
            }
            int nextLevel = validateResponse.currentLevel + 1;
            List<TaskTypeApprovalPathDTO> nextApprovalPath = TaskTypeApprovalPathRepository.getInstance().getByTaskTypeIdAndLevel(validateResponse.cardApprovalMappingDTO.taskTypeId, nextLevel);

            if (nextApprovalPath == null || nextApprovalPath.size() == 0) {
                response.hasNextApproval = false;
                response.organogramIds = null;
            } else {
                List<Long> organogramIds = add(nextApprovalPath, model, nextLevel);
                response.hasNextApproval = true;
                response.organogramIds = organogramIds;
            }
        }
        return response;
    }

    public boolean rejectPendingApproval(CreateVm_maintenance_requestApprovalModel model) throws InvalidDataException, AlreadyApprovedException {
        synchronized (LockManager.getLock(model.getVm_fuel_requestInfoId()+"CVMRAM")) {
            validate(model);
            String sql = String.format(updateStatusByVm_fuel_requestInfoId, ApprovalStatus.DISSATISFIED.getValue(), 0, model.getRequesterEmployeeRecordId(), System.currentTimeMillis(),
                    ApprovalStatus.PENDING.getValue(), model.getVm_fuel_requestInfoId());
            return (Boolean) ConnectionAndStatementUtil.getWriteStatement(st -> {
                try {
                    st.executeUpdate(sql);
                    return true;
                } catch (SQLException ex) {
                    logger.error(ex);
                    return false;
                }
            });
        }
    }

    public ValidateResponse validate(CreateVm_maintenance_requestApprovalModel model) throws InvalidDataException, AlreadyApprovedException {
        String sql = String.format(getByVm_fuel_requestInfoIdAndStatus, model.getVm_fuel_requestInfoId(), ApprovalStatus.PENDING.getValue());
        List<Vm_maintenance_request_approval_mappingDTO> dtoList = ConnectionAndStatementUtil.getListOfT(sql, this::buildObjectFromResultSet);
        if (dtoList == null || dtoList.size() == 0) {
            throw new InvalidDataException("No Pending approval is found for cardInfoId : " + model.getVm_fuel_requestInfoId());
        }
        Vm_maintenance_request_approval_mappingDTO validRequester = dtoList.stream()
                .filter(dto -> dto.approverEmployeeRecordsId == model.getRequesterEmployeeRecordId())
                .findAny()
                .orElse(null);

        if (validRequester == null) {
            sql = String.format(getByVm_fuel_requestInfoIdAndApproverEmployeeRecordsId, model.getVm_fuel_requestInfoId(), model.getRequesterEmployeeRecordId());
            validRequester = ConnectionAndStatementUtil.getT(sql, this::buildObjectFromResultSet);
            if (validRequester != null) {
                throw new AlreadyApprovedException();
            }
            throw new InvalidDataException("Invalid employee request to approve");
        }
        Set<Integer> sequenceValueSet = dtoList.stream()
                .map(e -> e.sequence)
                .collect(Collectors.toSet());
        if (sequenceValueSet.size() > 1) {
            throw new InvalidDataException("Multiple sequence value is found for Pending approval of cardInfoId : " + model.getVm_fuel_requestInfoId());
        }
        return new ValidateResponse(dtoList, (Integer) sequenceValueSet.toArray()[0],validRequester);
    }

    public static class ValidateResponse {
        List<Vm_maintenance_request_approval_mappingDTO> dtoList;
        int currentLevel;
        Vm_maintenance_request_approval_mappingDTO cardApprovalMappingDTO;

        public ValidateResponse(List<Vm_maintenance_request_approval_mappingDTO> dtoList, int currentLevel, Vm_maintenance_request_approval_mappingDTO cardApprovalMappingDTO) {
            this.dtoList = dtoList;
            this.currentLevel = currentLevel;
            this.cardApprovalMappingDTO = cardApprovalMappingDTO;
        }
    }

    public Vm_maintenance_request_approval_mappingDTO getByVm_fuel_requestIdAndApproverEmployeeRecordId(long cardInfoId, long approverEmployeeId){
        String sql = String.format(getByVm_fuel_requestInfoIdAndApproverEmployeeRecordsId, cardInfoId, approverEmployeeId);
        return ConnectionAndStatementUtil.getT(sql, this::buildObjectFromResultSet);
    }

    public List<Vm_maintenance_request_approval_mappingDTO> getAllApprovalDTOByVm_fuel_requestInfoId(long cardInfoId){
        String sql = String.format(getByVm_fuel_requestInfoId,cardInfoId);
        List<Vm_maintenance_request_approval_mappingDTO> list = ConnectionAndStatementUtil.getListOfT(sql,this::buildObjectFromResultSet);
        return list.stream()
                .filter(dto->dto.vm_fuel_requestApprovalStatusCat == ApprovalStatus.PENDING.getValue() || dto.approverEmployeeRecordsId == dto.modifiedBy)
                .collect(Collectors.toList());
    }
}
	