package employee_shift;
import java.util.*; 
import util.*;


public class Employee_shiftMAPS extends CommonMaps
{	
	public Employee_shiftMAPS(String tableName)
	{
		

		java_DTO_map.put("id".toLowerCase(), "id".toLowerCase());
		java_DTO_map.put("employeeRecordsId".toLowerCase(), "employeeRecordsId".toLowerCase());
		java_DTO_map.put("employeeOfficesId".toLowerCase(), "employeeOfficesId".toLowerCase());
		java_DTO_map.put("officeShiftDetailsId".toLowerCase(), "officeShiftDetailsId".toLowerCase());
		java_DTO_map.put("overtimeApplicable".toLowerCase(), "overtimeApplicable".toLowerCase());
		java_DTO_map.put("selfAttendanceEnabled".toLowerCase(), "selfAttendanceEnabled".toLowerCase());
		java_DTO_map.put("startDate".toLowerCase(), "startDate".toLowerCase());
		java_DTO_map.put("endDate".toLowerCase(), "endDate".toLowerCase());
		java_DTO_map.put("isActive".toLowerCase(), "isActive".toLowerCase());
		java_DTO_map.put("insertionDate".toLowerCase(), "insertionDate".toLowerCase());
		java_DTO_map.put("insertedBy".toLowerCase(), "insertedBy".toLowerCase());
		java_DTO_map.put("modifiedBy".toLowerCase(), "modifiedBy".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
		java_DTO_map.put("searchColumn".toLowerCase(), "searchColumn".toLowerCase());

		java_SQL_map.put("employee_records_id".toLowerCase(), "employeeRecordsId".toLowerCase());
		java_SQL_map.put("employee_offices_id".toLowerCase(), "employeeOfficesId".toLowerCase());
		java_SQL_map.put("office_shift_details_id".toLowerCase(), "officeShiftDetailsId".toLowerCase());
		java_SQL_map.put("start_date".toLowerCase(), "startDate".toLowerCase());
		java_SQL_map.put("end_date".toLowerCase(), "endDate".toLowerCase());

		java_Text_map.put("Id".toLowerCase(), "id".toLowerCase());
		java_Text_map.put("Employee Records Id".toLowerCase(), "employeeRecordsId".toLowerCase());
		java_Text_map.put("Employee Offices Id".toLowerCase(), "employeeOfficesId".toLowerCase());
		java_Text_map.put("Office Shift Details Id".toLowerCase(), "officeShiftDetailsId".toLowerCase());
		java_Text_map.put("Overtime Applicable".toLowerCase(), "overtimeApplicable".toLowerCase());
		java_Text_map.put("Self Attendance Enabled".toLowerCase(), "selfAttendanceEnabled".toLowerCase());
		java_Text_map.put("Start Date".toLowerCase(), "startDate".toLowerCase());
		java_Text_map.put("End Date".toLowerCase(), "endDate".toLowerCase());
		java_Text_map.put("Is Active".toLowerCase(), "isActive".toLowerCase());
		java_Text_map.put("Insertion Date".toLowerCase(), "insertionDate".toLowerCase());
		java_Text_map.put("Inserted By".toLowerCase(), "insertedBy".toLowerCase());
		java_Text_map.put("Modified By".toLowerCase(), "modifiedBy".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
		java_Text_map.put("Search Column".toLowerCase(), "searchColumn".toLowerCase());
			
	}

}