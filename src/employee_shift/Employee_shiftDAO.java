package employee_shift;

import dbm.DBMR;
import employee_records.EmployeeCommonDTOService;
import employee_records.Employee_recordsDTO;
import employee_records.Employee_recordsRepository;
import office_shift.Office_shiftDAO;
import office_shift.Office_shiftDTO;
import office_shift_details.Office_shift_detailsDAO;
import office_shift_details.Office_shift_detailsDTO;
import office_shift_details.Office_shift_detailsRepository;
import office_units.Office_unitsRepository;
import org.apache.log4j.Logger;
import pb.OptionDTO;
import pb.Utils;
import user.UserDTO;
import util.CommonDTO;
import util.NavigationService4;

import java.sql.*;
import java.util.*;
import java.util.stream.Collectors;

@SuppressWarnings({"unused","rawtypes","Duplicates"})
public class Employee_shiftDAO extends NavigationService4 implements EmployeeCommonDTOService<Employee_shiftDTO> {

    public static final Logger logger = Logger.getLogger(Employee_shiftDAO.class);

    public static final String addQuery = "INSERT INTO {tableName} (employee_records_id, employee_offices_id, office_shift_details_id, " +
            "overtime_applicable, self_attendance_enabled, start_date, end_date, is_active, insertion_date, inserted_by, modified_by, search_column, " +
            "lastModificationTime, isDeleted, id) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

    public static final String updateQuery = "UPDATE {tableName} SET employee_records_id=?, employee_offices_id=?, office_shift_details_id=?, " +
            "overtime_applicable=?, self_attendance_enabled=?, start_date=?, end_date=?, is_active=?, insertion_date=?, inserted_by=?, modified_by=?, " +
            "search_column=?, lastModificationTime=? WHERE id=?";

    private static final Map<String, String> searchMap = new HashMap<>();

    static {
        searchMap.put("employee_records_id", " and (employee_records_id = ?)");
        searchMap.put("employee_offices_id", " and (employee_offices_id = ?)");
        searchMap.put("office_shift_details_id", " and (office_shift_details_id = ?)");
        searchMap.put("start_date", " and (start_date = ?)");
        searchMap.put("end_date", " and (end_date = ?)");
        searchMap.put("AnyField", " and (search_column like ?)");
    }

    public Employee_shiftDAO(String tableName) {
        super(tableName);
        joinSQL = "";
        useSafeSearch = true;
        commonMaps = new Employee_shiftMAPS(tableName);
    }

    public Employee_shiftDAO() {
        this("employee_shift");
    }

    public void setSearchColumn(Employee_shiftDTO employee_shiftDTO) {
        employee_shiftDTO.searchColumn = Employee_recordsRepository.getInstance().getEmployeeName(employee_shiftDTO.employeeRecordsId, "English") +
                Employee_recordsRepository.getInstance().getEmployeeName(employee_shiftDTO.employeeRecordsId, "Bangla") +
                Office_unitsRepository.getInstance().geText("English", employee_shiftDTO.employeeOfficesId) +
                Office_unitsRepository.getInstance().geText("Bangla", employee_shiftDTO.employeeOfficesId);
    }

    public void set(PreparedStatement ps, Employee_shiftDTO employee_shiftDTO, boolean isInsert) throws SQLException {
        int index = 0;
        setSearchColumn(employee_shiftDTO);
        ps.setObject(++index, employee_shiftDTO.employeeRecordsId);
        ps.setObject(++index, employee_shiftDTO.employeeOfficesId);
        ps.setObject(++index, employee_shiftDTO.officeShiftDetailsId);
        ps.setObject(++index, employee_shiftDTO.overtimeApplicable);
        ps.setObject(++index, employee_shiftDTO.selfAttendanceEnabled);
        ps.setObject(++index, employee_shiftDTO.startDate);
        ps.setObject(++index, employee_shiftDTO.endDate);
        ps.setObject(++index, employee_shiftDTO.isActive);
        ps.setObject(++index, employee_shiftDTO.insertionDate);
        ps.setObject(++index, employee_shiftDTO.insertedBy);
        ps.setObject(++index, employee_shiftDTO.modifiedBy);
        ps.setObject(++index, employee_shiftDTO.searchColumn);
        ps.setObject(++index, System.currentTimeMillis());
        if (isInsert) {
            ps.setObject(++index, 0);
        }
        ps.setObject(++index, employee_shiftDTO.id);
    }

    public Employee_shiftDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Employee_shiftDTO employee_shiftDTO = new Employee_shiftDTO();
            employee_shiftDTO.id = rs.getLong("id");
            employee_shiftDTO.employeeRecordsId = rs.getLong("employee_records_id");
            employee_shiftDTO.employeeOfficesId = rs.getLong("employee_offices_id");
            employee_shiftDTO.officeShiftDetailsId = rs.getLong("office_shift_details_id");
            employee_shiftDTO.overtimeApplicable = rs.getInt("overtime_applicable");
            employee_shiftDTO.selfAttendanceEnabled = rs.getInt("self_attendance_enabled");
            employee_shiftDTO.startDate = rs.getLong("start_date");
            employee_shiftDTO.endDate = rs.getLong("end_date");
            employee_shiftDTO.isActive = rs.getInt("is_active");
            employee_shiftDTO.insertionDate = rs.getLong("insertion_date");
            employee_shiftDTO.insertedBy = rs.getString("inserted_by");
            employee_shiftDTO.modifiedBy = rs.getString("modified_by");
            employee_shiftDTO.isDeleted = rs.getInt("isDeleted");
            employee_shiftDTO.lastModificationTime = rs.getLong("lastModificationTime");
            employee_shiftDTO.searchColumn = rs.getString("search_column");
            return employee_shiftDTO;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    @Override
    public String getTableName() {
        return "employee_shift";
    }

    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Employee_shiftDTO) commonDTO, addQuery, true);
    }

    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Employee_shiftDTO) commonDTO, updateQuery, false);
    }

    public Employee_shiftDTO getDTOByID(long ID) {
        return getDTOFromID(ID);
    }

    public List<Employee_shiftDTO> getAllEmployee_shift(boolean isFirstReload) {
        return getAllDTOs(isFirstReload);
    }


    public List<Employee_shiftDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO) {
        return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
    }


    public List<Employee_shiftDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
                                           String filter, boolean tableHasJobCat) {
        List<Object> objectList = new ArrayList<>();
        String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat,objectList);
        return getDTOs(sql,objectList);
    }

    public String getSqlWithSearchCriteriaForNormalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
                                                          UserDTO userDTO, String filter, boolean tableHasJobCat,List<Object>objectList) {
        return getSearchQuery(tableName, p_searchCriteria, limit, offset, category, searchMap,objectList);
    }

    public String getSqlWithSearchCriteria(Hashtable p_searchCriteria, int limit, int offset, int category,
                                           boolean isPermanentTable, UserDTO userDTO, String filter, boolean tableHasJobCat,List<Object>objectList) {
        return getSqlWithSearchCriteriaForNormalSearch(p_searchCriteria, limit, offset, category, userDTO, filter, tableHasJobCat,objectList);
    }


    // building optionList for employees in a particular office
    public static String buildOptionsForEmployees(long officeUnitId, String language, Long selectedId) {
        Connection connection = null;
        ResultSet rs;
        Statement stmt = null;
        List<OptionDTO> optionDTOList = new ArrayList<>();
        String name_en, name_bn;
        long employeeRecordId;

        try {

            String sql = "SELECT employee_record_id, name_eng, name_bng FROM employee_records R INNER JOIN " +
                    "employee_offices O ON R.id=O.employee_record_id WHERE O.office_unit_id=" + officeUnitId;

            logger.debug(sql);

            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();
            rs = stmt.executeQuery(sql);

            while (rs.next()) {
                name_en = rs.getString("name_eng");
                name_bn = rs.getString("name_bng");
                employeeRecordId = rs.getLong("employee_record_id");

                optionDTOList.add(new OptionDTO(name_en, name_bn, String.valueOf(employeeRecordId)));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception ignored) {
            }

            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ignored) {
            }
        }

        return Utils.buildOptions(optionDTOList, language, selectedId == null ? null : String.valueOf(selectedId));
    }

    public static String getEmployeeText(String language, long id) {

        Employee_recordsDTO dto = Employee_recordsRepository.getInstance().getById(id);

        return dto == null ? "" : (language.equalsIgnoreCase("English") ?
                (dto.nameEng == null ? "" : dto.nameEng)
                : (dto.nameBng == null ? "" : dto.nameBng));
    }


    public static String buildOptionForShifts(long officeUnitId, String language, Long selectedId) {
        Connection connection = null;
        ResultSet rs;
        Statement stmt = null;
        List<OptionDTO> optionDTOList = new ArrayList<>();
        String name_en, name_bn;
        long officeShiftDetailsId;

        try {

            String sql = "select os.name_en, os.name_bn, osd.id from office_shift_details osd inner join office_shift os " +
                    "on os.id=osd.office_shift_id where os.office_unit_id=" + officeUnitId + " and osd.is_active=1";

            logger.debug(sql);

            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();
            rs = stmt.executeQuery(sql);

            while (rs.next()) {
                name_en = rs.getString("os.name_en");
                name_bn = rs.getString("os.name_bn");
                officeShiftDetailsId = rs.getLong("osd.id");

                optionDTOList.add(new OptionDTO(name_en, name_bn, String.valueOf(officeShiftDetailsId)));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception ignored) {
            }

            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ignored) {
            }
        }

        return Utils.buildOptions(optionDTOList, language, selectedId == null ? null : String.valueOf(selectedId));
    }

    public static String buildOptionForEmployeeShifts(long officeUnitId, long employeeRecordsId, String language, Long selectedId) {
        Connection connection = null;
        ResultSet rs;
        Statement stmt = null;
        List<OptionDTO> optionDTOList = new ArrayList<>();
        String name_en, name_bn;
        long officeShiftDetailsId;

        try {

            String sql = "select os.name_en, os.name_bn, osd.id from office_shift_details osd inner join office_shift os " +
                    "on os.id=osd.office_shift_id where os.office_unit_id=" + officeUnitId + " and osd.is_active=1";

            logger.debug(sql);

            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();
            rs = stmt.executeQuery(sql);

            while (rs.next()) {
                name_en = rs.getString("os.name_en");
                name_bn = rs.getString("os.name_bn");
                officeShiftDetailsId = rs.getLong("osd.id");

                optionDTOList.add(new OptionDTO(name_en, name_bn, String.valueOf(officeShiftDetailsId)));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception ignored) {
            }

            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ignored) {
            }
        }

        return Utils.buildOptions(filterOfficeShift(optionDTOList,employeeRecordsId), language, selectedId == null ? null : String.valueOf(selectedId));
    }

    private static List<OptionDTO> filterOfficeShift( List<OptionDTO> officeShifts , long employeeRecordsId) {

        List<Employee_shiftDTO> shiftDTOS = Employee_shiftRepository.getInstance().getEmployee_shiftDTOByemployee_records_id(employeeRecordsId);

        long currTime = System.currentTimeMillis();

        shiftDTOS = shiftDTOS.stream()
                .filter(dto -> dto.isActive==1)
                .filter(dto -> dto.endDate >= currTime)
                .collect(Collectors.toList());

        List<Employee_shiftDTO> finalShiftDTOS = shiftDTOS;

        return officeShifts.stream()
                .filter(dto -> isDateRangeOutside(finalShiftDTOS, Long.parseLong(dto.value)))
                .collect(Collectors.toList());
    }

    private static boolean isDateRangeOutside(List<Employee_shiftDTO> shiftDTOS , long officeShiftDetailsId) {

        Office_shift_detailsDTO office_shift_detailsDTO = Office_shift_detailsRepository.getInstance().getOffice_shift_detailsDTOByid(officeShiftDetailsId);

        if(office_shift_detailsDTO==null) return false;

        if(office_shift_detailsDTO.isActive==0) return false;

        long currTime = System.currentTimeMillis();

        if(office_shift_detailsDTO.endDate<currTime) return false;

        for(Employee_shiftDTO dto: shiftDTOS) {

            if(office_shift_detailsDTO.startDate<=dto.startDate && office_shift_detailsDTO.endDate>=dto.startDate)
                return false;

            if(office_shift_detailsDTO.startDate<=dto.endDate && office_shift_detailsDTO.endDate>=dto.endDate)
                return false;
        }

        return true;
    }



    public static String getShiftText(String language, long id) {
        Office_shiftDAO office_shiftDAO = new Office_shiftDAO();
        Office_shift_detailsDAO office_shift_detailsDAO = new Office_shift_detailsDAO();
        Office_shift_detailsDTO officeShiftDetailsDto = office_shift_detailsDAO.getDTOByID(id);
        if (officeShiftDetailsDto == null) return "";
        Office_shiftDTO dto = office_shiftDAO.getDTOByID(officeShiftDetailsDto.officeShiftId);
        return dto == null ? "" : (language.equalsIgnoreCase("English") ?
                (dto.nameEn == null ? "" : dto.nameEn)
                : (dto.nameBn == null ? "" : dto.nameBn));
    }

}
	