package employee_shift;
import java.util.*; 
import util.*; 


public class Employee_shiftDTO extends CommonDTO
{

	public long id = 0;
	public long employeeRecordsId = 0;
	public long employeeOfficesId = 0;
	public long officeShiftDetailsId = 0;
	public int overtimeApplicable = 0;
	public int selfAttendanceEnabled = 0;
	public long startDate = 0;
	public long endDate = 0;
	public int isActive = 0;
	public long insertionDate = 0;
    public String insertedBy = "";
    public String modifiedBy = "";
	
	
    @Override
	public String toString() {
            return "$Employee_shiftDTO[" +
            " id = " + id +
            " employeeRecordsId = " + employeeRecordsId +
            " employeeOfficesId = " + employeeOfficesId +
            " officeShiftDetailsId = " + officeShiftDetailsId +
            " overtimeApplicable = " + overtimeApplicable +
            " selfAttendanceEnabled = " + selfAttendanceEnabled +
            " startDate = " + startDate +
            " endDate = " + endDate +
            " isActive = " + isActive +
            " insertionDate = " + insertionDate +
            " insertedBy = " + insertedBy +
            " modifiedBy = " + modifiedBy +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}