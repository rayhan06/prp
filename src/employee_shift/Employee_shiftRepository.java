package employee_shift;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class Employee_shiftRepository implements Repository {
	Employee_shiftDAO employee_shiftDAO;
	
	public void setDAO(Employee_shiftDAO employee_shiftDAO)
	{
		this.employee_shiftDAO = employee_shiftDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Employee_shiftRepository.class);
	Map<Long, Employee_shiftDTO>mapOfEmployee_shiftDTOToid;
	Map<Long, Set<Employee_shiftDTO> >mapOfEmployee_shiftDTOToemployeeRecordsId;
	Map<Long, Set<Employee_shiftDTO> >mapOfEmployee_shiftDTOToemployeeOfficesId;
	Map<Long, Set<Employee_shiftDTO> >mapOfEmployee_shiftDTOToofficeShiftDetailsId;


	static Employee_shiftRepository instance = null;  
	private Employee_shiftRepository(){
		mapOfEmployee_shiftDTOToid = new ConcurrentHashMap<>();
		mapOfEmployee_shiftDTOToemployeeRecordsId = new ConcurrentHashMap<>();
		mapOfEmployee_shiftDTOToemployeeOfficesId = new ConcurrentHashMap<>();
		mapOfEmployee_shiftDTOToofficeShiftDetailsId = new ConcurrentHashMap<>();
		employee_shiftDAO = new Employee_shiftDAO();
		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Employee_shiftRepository getInstance(){
		if (instance == null){
			instance = new Employee_shiftRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(employee_shiftDAO == null)
		{
			return;
		}
		try {
			List<Employee_shiftDTO> employee_shiftDTOs = employee_shiftDAO.getAllEmployee_shift(reloadAll);
			for(Employee_shiftDTO employee_shiftDTO : employee_shiftDTOs) {
				Employee_shiftDTO oldEmployee_shiftDTO = getEmployee_shiftDTOByid(employee_shiftDTO.id);
				if( oldEmployee_shiftDTO != null ) {
					mapOfEmployee_shiftDTOToid.remove(oldEmployee_shiftDTO.id);
				
					if(mapOfEmployee_shiftDTOToemployeeRecordsId.containsKey(oldEmployee_shiftDTO.employeeRecordsId)) {
						mapOfEmployee_shiftDTOToemployeeRecordsId.get(oldEmployee_shiftDTO.employeeRecordsId).remove(oldEmployee_shiftDTO);
					}
					if(mapOfEmployee_shiftDTOToemployeeRecordsId.get(oldEmployee_shiftDTO.employeeRecordsId).isEmpty()) {
						mapOfEmployee_shiftDTOToemployeeRecordsId.remove(oldEmployee_shiftDTO.employeeRecordsId);
					}
					
					if(mapOfEmployee_shiftDTOToemployeeOfficesId.containsKey(oldEmployee_shiftDTO.employeeOfficesId)) {
						mapOfEmployee_shiftDTOToemployeeOfficesId.get(oldEmployee_shiftDTO.employeeOfficesId).remove(oldEmployee_shiftDTO);
					}
					if(mapOfEmployee_shiftDTOToemployeeOfficesId.get(oldEmployee_shiftDTO.employeeOfficesId).isEmpty()) {
						mapOfEmployee_shiftDTOToemployeeOfficesId.remove(oldEmployee_shiftDTO.employeeOfficesId);
					}
					
					if(mapOfEmployee_shiftDTOToofficeShiftDetailsId.containsKey(oldEmployee_shiftDTO.officeShiftDetailsId)) {
						mapOfEmployee_shiftDTOToofficeShiftDetailsId.get(oldEmployee_shiftDTO.officeShiftDetailsId).remove(oldEmployee_shiftDTO);
					}
					if(mapOfEmployee_shiftDTOToofficeShiftDetailsId.get(oldEmployee_shiftDTO.officeShiftDetailsId).isEmpty()) {
						mapOfEmployee_shiftDTOToofficeShiftDetailsId.remove(oldEmployee_shiftDTO.officeShiftDetailsId);
					}
				
					
				}
				if(employee_shiftDTO.isDeleted == 0) 
				{
					
					mapOfEmployee_shiftDTOToid.put(employee_shiftDTO.id, employee_shiftDTO);
				
					if( ! mapOfEmployee_shiftDTOToemployeeRecordsId.containsKey(employee_shiftDTO.employeeRecordsId)) {
						mapOfEmployee_shiftDTOToemployeeRecordsId.put(employee_shiftDTO.employeeRecordsId, new HashSet<>());
					}
					mapOfEmployee_shiftDTOToemployeeRecordsId.get(employee_shiftDTO.employeeRecordsId).add(employee_shiftDTO);
					
					if( ! mapOfEmployee_shiftDTOToemployeeOfficesId.containsKey(employee_shiftDTO.employeeOfficesId)) {
						mapOfEmployee_shiftDTOToemployeeOfficesId.put(employee_shiftDTO.employeeOfficesId, new HashSet<>());
					}
					mapOfEmployee_shiftDTOToemployeeOfficesId.get(employee_shiftDTO.employeeOfficesId).add(employee_shiftDTO);
					
					if( ! mapOfEmployee_shiftDTOToofficeShiftDetailsId.containsKey(employee_shiftDTO.officeShiftDetailsId)) {
						mapOfEmployee_shiftDTOToofficeShiftDetailsId.put(employee_shiftDTO.officeShiftDetailsId, new HashSet<>());
					}
					mapOfEmployee_shiftDTOToofficeShiftDetailsId.get(employee_shiftDTO.officeShiftDetailsId).add(employee_shiftDTO);
							
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Employee_shiftDTO> getEmployee_shiftList() {
		List <Employee_shiftDTO> employee_shifts = new ArrayList<Employee_shiftDTO>(this.mapOfEmployee_shiftDTOToid.values());
		return employee_shifts;
	}
	
	
	public Employee_shiftDTO getEmployee_shiftDTOByid( long id){
		return mapOfEmployee_shiftDTOToid.get(id);
	}
	
	
	public List<Employee_shiftDTO> getEmployee_shiftDTOByemployee_records_id(long employee_records_id) {
		return new ArrayList<>( mapOfEmployee_shiftDTOToemployeeRecordsId.getOrDefault(employee_records_id,new HashSet<>()));
	}
	
	
	public List<Employee_shiftDTO> getEmployee_shiftDTOByemployee_offices_id(long employee_offices_id) {
		return new ArrayList<>( mapOfEmployee_shiftDTOToemployeeOfficesId.getOrDefault(employee_offices_id,new HashSet<>()));
	}
	
	
	public List<Employee_shiftDTO> getEmployee_shiftDTOByoffice_shift_details_id(long office_shift_details_id) {
		return new ArrayList<>( mapOfEmployee_shiftDTOToofficeShiftDetailsId.getOrDefault(office_shift_details_id,new HashSet<>()));
	}
	
	
	@Override
	public String getTableName() {
		return "employee_shift";
	}
}


