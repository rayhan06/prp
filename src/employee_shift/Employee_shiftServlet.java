package employee_shift;

import java.io.IOException;
import java.io.*;
import java.text.SimpleDateFormat;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import org.apache.log4j.Logger;

import login.LoginDTO;
import office_shift_details.Office_shift_detailsRepository;
import permission.MenuConstants;
import role.PermissionRepository;


import sessionmanager.SessionConstants;

import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;
import util.CommonRequestHandler;
import util.RecordNavigationManager4;

import java.util.*;
import javax.servlet.http.*;


import com.google.gson.Gson;

import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;




/**
 * Servlet implementation class Employee_shiftServlet
 */
@WebServlet("/Employee_shiftServlet")
@MultipartConfig
public class Employee_shiftServlet extends HttpServlet
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Employee_shiftServlet.class);

    String tableName = "employee_shift";

	Employee_shiftDAO employee_shiftDAO;
	CommonRequestHandler commonRequestHandler;
    private final Gson gson = new Gson();

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Employee_shiftServlet()
	{
        super();
    	try
    	{
			employee_shiftDAO = new Employee_shiftDAO(tableName);
			commonRequestHandler = new CommonRequestHandler(employee_shiftDAO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		System.out.println("In doget request = " + request);
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		try
		{
			String actionType = request.getParameter("actionType");
			request.setAttribute("officeShiftDetailsDTOs", Office_shift_detailsRepository.getInstance().getOffice_shift_detailsList());
			if(actionType.equals("getAddPage"))
			{
//				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.EMPLOYEE_SHIFT_ADD))
//				{
					commonRequestHandler.getAddPage(request, response);
//				}
//				else
//				{
//					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
//				}
			}
			else if(actionType.equals("getEditPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.EMPLOYEE_SHIFT_UPDATE))
				{
					getEmployee_shift(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getURL"))
			{
				String URL = request.getParameter("URL");
				System.out.println("URL = " + URL);
				response.sendRedirect(URL);
			}
			else if(actionType.equals("search"))
			{
				System.out.println("search requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.EMPLOYEE_SHIFT_SEARCH))
				{
					if(isPermanentTable)
					{
						String filter = request.getParameter("filter");
						System.out.println("filter = " + filter);
						if(filter!=null)
						{
							filter = ""; //shouldn't be directly used, rather manipulate it.
							searchEmployee_shift(request, response, isPermanentTable, filter);
						}
						else
						{
							searchEmployee_shift(request, response, isPermanentTable, "");
						}
					}
					else
					{
						//searchEmployee_shift(request, response, tempTableName, isPermanentTable);
					}
				}
			}
			else if(actionType.equals("view"))
			{
				System.out.println("view requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.EMPLOYEE_SHIFT_SEARCH))
				{
					commonRequestHandler.view(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}
			else if(actionType.equals("getEmployeeName")) {
				long officeUnitId = Long.parseLong(request.getParameter("office_unit_id"));
				String language = request.getParameter("language");
				String val = request.getParameter("selectedId");
				Long selectedId = null;
				if(val != null && !val.isEmpty()) {
					selectedId = Long.parseLong(val);
				}

				String options = Employee_shiftDAO.buildOptionsForEmployees(officeUnitId, language, selectedId);
				PrintWriter out = response.getWriter();
				out.println(options);
				out.close();
			}
			else if(actionType.equals("getShiftName")) {
				long officeUnitId = Long.parseLong(request.getParameter("office_unit_id"));
				String language = request.getParameter("language");
				String val = request.getParameter("selectedId");
				String employeeRecordsId = request.getParameter("employeeRecordId");
				Long selectedId = null;
				if(val != null && !val.isEmpty()) {
					selectedId = Long.parseLong(val);
				}

				String options = Employee_shiftDAO.buildOptionForEmployeeShifts(officeUnitId,Long.parseLong(employeeRecordsId), language, selectedId);
				PrintWriter out = response.getWriter();
				out.println(options);
				out.close();
			}
			else
			{
				request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}





	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		// TODO Auto-generated method stub
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		System.out.println("doPost");
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);

		try
		{
			String actionType = request.getParameter("actionType");
			System.out.println("actionType = " + actionType);
			if(actionType.equals("add"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.EMPLOYEE_SHIFT_ADD))
				{
					System.out.println("going to  addEmployee_shift ");
					addEmployee_shift(request, response, true, userDTO, true);
				}
				else
				{
					System.out.println("Not going to  addEmployee_shift ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}

			else if(actionType.equals("getDTO"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.EMPLOYEE_SHIFT_ADD))
				{
					getDTO(request, response);
				}
				else
				{
					System.out.println("Not going to  addEmployee_shift ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}
			else if(actionType.equals("edit"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.EMPLOYEE_SHIFT_UPDATE))
				{
					addEmployee_shift(request, response, false, userDTO, isPermanentTable);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("delete"))
			{
				deleteEmployee_shift(request, response, userDTO);
			}
			else if(actionType.equals("search"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.EMPLOYEE_SHIFT_SEARCH))
				{
					searchEmployee_shift(request, response, true, "");
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getGeo"))
			{
				System.out.println("going to geoloc ");
				request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
			}

		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}

	private void getDTO(HttpServletRequest request, HttpServletResponse response)
	{
		try
		{
			System.out.println("In getDTO");
			Employee_shiftDTO employee_shiftDTO = employee_shiftDAO.getDTOByID(Long.parseLong(request.getParameter("ID")));
			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");

			String encoded = this.gson.toJson(employee_shiftDTO);
			System.out.println("json encoded data = " + encoded);
			out.print(encoded);
			out.flush();
		}
		catch (NumberFormatException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void addEmployee_shift(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, boolean isPermanentTable) throws IOException
	{
		// TODO Auto-generated method stub
		try
		{
			request.setAttribute("failureMessage", "");
			System.out.println("%%%% addEmployee_shift");
			String path = getServletContext().getRealPath("/img2/");
			Employee_shiftDTO employee_shiftDTO;
			SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");

			if(addFlag == true)
			{
				employee_shiftDTO = new Employee_shiftDTO();
			}
			else
			{
				employee_shiftDTO = employee_shiftDAO.getDTOByID(Long.parseLong(request.getParameter("id")));
			}

			String Value = "";

			Value = request.getParameter("employeeRecordsId");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("employeeRecordsId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{

				employee_shiftDTO.employeeRecordsId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("employeeOfficesId");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("employeeOfficesId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{

				employee_shiftDTO.employeeOfficesId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("officeShiftDetailsId");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("officeShiftDetailsId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{

				employee_shiftDTO.officeShiftDetailsId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("overtimeApplicable");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("overtimeApplicable = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{

				employee_shiftDTO.overtimeApplicable = 1;
			}
			else
			{
				employee_shiftDTO.overtimeApplicable = 0;
			}

			Value = request.getParameter("selfAttendanceEnabled");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("selfAttendanceEnabled = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{

				employee_shiftDTO.selfAttendanceEnabled = 1;
			}
			else
			{
				employee_shiftDTO.selfAttendanceEnabled = 0;
			}

			Value = request.getParameter("startDate");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("startDate = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{

				try
				{
					Date d = f.parse(Value);
					employee_shiftDTO.startDate = d.getTime();
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("endDate");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("endDate = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{

				try
				{
					Date d = f.parse(Value);
					employee_shiftDTO.endDate = d.getTime();
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("isActive");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("isActive = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{

				employee_shiftDTO.isActive = 1;
			}
			else
			{
				employee_shiftDTO.isActive = 0;
			}

			if(addFlag)
			{
				Calendar c = Calendar.getInstance();
				c.set(Calendar.HOUR_OF_DAY, 0);
				c.set(Calendar.MINUTE, 0);
				c.set(Calendar.SECOND, 0);
				c.set(Calendar.MILLISECOND, 0);

				employee_shiftDTO.insertionDate = c.getTimeInMillis();
			}


			Value = request.getParameter("insertedBy");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("insertedBy = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{

				employee_shiftDTO.insertedBy = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("modifiedBy");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("modifiedBy = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{

				employee_shiftDTO.modifiedBy = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("searchColumn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("searchColumn = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{

				employee_shiftDTO.searchColumn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			System.out.println("Done adding  addEmployee_shift dto = " + employee_shiftDTO);
			long returnedID = -1;

			if(isPermanentTable == false) //add new row for validation and make the old row outdated
			{
				employee_shiftDAO.setIsDeleted(employee_shiftDTO.iD, CommonDTO.OUTDATED);
				returnedID = employee_shiftDAO.add(employee_shiftDTO);
				employee_shiftDAO.setIsDeleted(returnedID, CommonDTO.WAITING_FOR_APPROVAL);
			}
			else if(addFlag == true)
			{
				returnedID = employee_shiftDAO.manageWriteOperations(employee_shiftDTO, SessionConstants.INSERT, -1, userDTO);
			}
			else
			{
				returnedID = employee_shiftDAO.manageWriteOperations(employee_shiftDTO, SessionConstants.UPDATE, -1, userDTO);
			}









			if(isPermanentTable)
			{
				String inPlaceSubmit = request.getParameter("inplacesubmit");

				if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
				{
					getEmployee_shift(request, response, returnedID);
				}
				else
				{
					response.sendRedirect("Employee_shiftServlet?actionType=search");
				}
			}
			else
			{
				commonRequestHandler.validate(employee_shiftDAO.getDTOByID(returnedID), request, response, userDTO);
			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}









	private void deleteEmployee_shift(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO) throws IOException
	{
		try
		{
			String[] IDsToDelete = request.getParameterValues("ID");
			for(int i = 0; i < IDsToDelete.length; i ++)
			{
				long id = Long.parseLong(IDsToDelete[i]);
				System.out.println("------ DELETING " + IDsToDelete[i]);


				Employee_shiftDTO employee_shiftDTO = employee_shiftDAO.getDTOByID(id);
				employee_shiftDAO.manageWriteOperations(employee_shiftDTO, SessionConstants.DELETE, id, userDTO);


			}
			response.sendRedirect("Employee_shiftServlet?actionType=search");
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}

	}

	private void getEmployee_shift(HttpServletRequest request, HttpServletResponse response, long id) throws ServletException, IOException
	{
		System.out.println("in getEmployee_shift");
		Employee_shiftDTO employee_shiftDTO = null;
		try
		{
			employee_shiftDTO = employee_shiftDAO.getDTOByID(id);
			request.setAttribute("ID", employee_shiftDTO.iD);
			request.setAttribute("employee_shiftDTO",employee_shiftDTO);
			request.setAttribute("employee_shiftDAO",employee_shiftDAO);

			String URL= "";

			String inPlaceEdit = request.getParameter("inplaceedit");
			String inPlaceSubmit = request.getParameter("inplacesubmit");
			String getBodyOnly = request.getParameter("getBodyOnly");

			if(inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase(""))
			{
				URL = "employee_shift/employee_shiftInPlaceEdit.jsp";
				request.setAttribute("inplaceedit","");
			}
			else if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
			{
				URL = "employee_shift/employee_shiftSearchRow.jsp";
				request.setAttribute("inplacesubmit","");
			}
			else
			{
				if(getBodyOnly  != null && !getBodyOnly.equalsIgnoreCase(""))
				{
					URL = "employee_shift/employee_shiftEditBody.jsp?actionType=edit";
				}
				else
				{
					URL = "employee_shift/employee_shiftEdit.jsp?actionType=edit";
				}
			}

			RequestDispatcher rd = request.getRequestDispatcher(URL);
			rd.forward(request, response);
		}
		catch (NumberFormatException e)
		{
			e.printStackTrace();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}


	private void getEmployee_shift(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		getEmployee_shift(request, response, Long.parseLong(request.getParameter("ID")));
	}

	private void searchEmployee_shift(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException
	{
		System.out.println("in  searchEmployee_shift 1");
		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String ajax = request.getParameter("ajax");
		boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);

        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
			SessionConstants.NAV_EMPLOYEE_SHIFT,
			request,
			employee_shiftDAO,
			SessionConstants.VIEW_EMPLOYEE_SHIFT,
			SessionConstants.SEARCH_EMPLOYEE_SHIFT,
			tableName,
			isPermanent,
			userDTO,
			filter,
			true);
        try
        {
			System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        }
        catch(Exception e)
        {
			System.out.println("failed to dojob" + e);
        }

		request.setAttribute("employee_shiftDAO",employee_shiftDAO);
        RequestDispatcher rd;
        if(!isPermanent)
        {
        	if(hasAjax == false)
	        {
	        	System.out.println("Going to employee_shift/employee_shiftApproval.jsp");
	        	rd = request.getRequestDispatcher("employee_shift/employee_shiftApproval.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to employee_shift/employee_shiftApprovalForm.jsp");
	        	rd = request.getRequestDispatcher("employee_shift/employee_shiftApprovalForm.jsp");
	        }
        }
        else
        {
	        if(hasAjax == false)
	        {
	        	System.out.println("Going to employee_shift/employee_shiftSearch.jsp");
	        	rd = request.getRequestDispatcher("employee_shift/employee_shiftSearch.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to employee_shift/employee_shiftSearchForm.jsp");
	        	rd = request.getRequestDispatcher("employee_shift/employee_shiftSearchForm.jsp");
	        }
        }
		rd.forward(request, response);
	}

}

