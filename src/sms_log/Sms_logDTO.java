package sms_log;

import util.CommonDTO;


public class Sms_logDTO extends CommonDTO
{

    public String sender = "";
    public String receiver = "";
	public int smsReceiverTypeCat = 0;
    public String message = "";
	public int smsStatusCat = 0;
	public long insertionTime = 0;
	public long fetchingTime = 0;
	public long sendingTime = 0;
	public long deliveryTime = 0;
	public Long jobId = 0L;
	
	
    @Override
	public String toString() {
            return "$Sms_logDTO[" +
            " iD = " + iD +
            " sender = " + sender +
            " receiver = " + receiver +
            " smsReceiverTypeCat = " + smsReceiverTypeCat +
            " message = " + message +
            " smsStatusCat = " + smsStatusCat +
            " insertionTime = " + insertionTime +
            " fetchingTime = " + fetchingTime +
            " sendingTime = " + sendingTime +
            " deliveryTime = " + deliveryTime +
            "]";
    }

}