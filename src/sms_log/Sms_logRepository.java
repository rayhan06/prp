package sms_log;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class Sms_logRepository implements Repository {
	Sms_logDAO sms_logDAO = null;
	
	public void setDAO(Sms_logDAO sms_logDAO)
	{
		this.sms_logDAO = sms_logDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Sms_logRepository.class);
	Map<Long, Sms_logDTO>mapOfSms_logDTOToiD;
	Map<String, Set<Sms_logDTO> >mapOfSms_logDTOTosender;
	Map<String, Set<Sms_logDTO> >mapOfSms_logDTOToreceiver;
	Map<Integer, Set<Sms_logDTO> >mapOfSms_logDTOTosmsReceiverTypeCat;
	Map<String, Set<Sms_logDTO> >mapOfSms_logDTOTomessage;
	Map<Integer, Set<Sms_logDTO> >mapOfSms_logDTOTosmsStatusCat;
	Map<Long, Set<Sms_logDTO> >mapOfSms_logDTOToinsertionTime;
	Map<Long, Set<Sms_logDTO> >mapOfSms_logDTOTofetchingTime;
	Map<Long, Set<Sms_logDTO> >mapOfSms_logDTOTosendingTime;
	Map<Long, Set<Sms_logDTO> >mapOfSms_logDTOTodeliveryTime;


	static Sms_logRepository instance = null;  
	private Sms_logRepository(){
		mapOfSms_logDTOToiD = new ConcurrentHashMap<>();
		mapOfSms_logDTOTosender = new ConcurrentHashMap<>();
		mapOfSms_logDTOToreceiver = new ConcurrentHashMap<>();
		mapOfSms_logDTOTosmsReceiverTypeCat = new ConcurrentHashMap<>();
		mapOfSms_logDTOTomessage = new ConcurrentHashMap<>();
		mapOfSms_logDTOTosmsStatusCat = new ConcurrentHashMap<>();
		mapOfSms_logDTOToinsertionTime = new ConcurrentHashMap<>();
		mapOfSms_logDTOTofetchingTime = new ConcurrentHashMap<>();
		mapOfSms_logDTOTosendingTime = new ConcurrentHashMap<>();
		mapOfSms_logDTOTodeliveryTime = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Sms_logRepository getInstance(){
		if (instance == null){
			instance = new Sms_logRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(sms_logDAO == null)
		{
			return;
		}
		try {
			List<Sms_logDTO> sms_logDTOs = sms_logDAO.getAllSms_log(reloadAll);
			for(Sms_logDTO sms_logDTO : sms_logDTOs) {
				Sms_logDTO oldSms_logDTO = getSms_logDTOByID(sms_logDTO.iD);
				if( oldSms_logDTO != null ) {
					mapOfSms_logDTOToiD.remove(oldSms_logDTO.iD);
				
					if(mapOfSms_logDTOTosender.containsKey(oldSms_logDTO.sender)) {
						mapOfSms_logDTOTosender.get(oldSms_logDTO.sender).remove(oldSms_logDTO);
					}
					if(mapOfSms_logDTOTosender.get(oldSms_logDTO.sender).isEmpty()) {
						mapOfSms_logDTOTosender.remove(oldSms_logDTO.sender);
					}
					
					if(mapOfSms_logDTOToreceiver.containsKey(oldSms_logDTO.receiver)) {
						mapOfSms_logDTOToreceiver.get(oldSms_logDTO.receiver).remove(oldSms_logDTO);
					}
					if(mapOfSms_logDTOToreceiver.get(oldSms_logDTO.receiver).isEmpty()) {
						mapOfSms_logDTOToreceiver.remove(oldSms_logDTO.receiver);
					}
					
					if(mapOfSms_logDTOTosmsReceiverTypeCat.containsKey(oldSms_logDTO.smsReceiverTypeCat)) {
						mapOfSms_logDTOTosmsReceiverTypeCat.get(oldSms_logDTO.smsReceiverTypeCat).remove(oldSms_logDTO);
					}
					if(mapOfSms_logDTOTosmsReceiverTypeCat.get(oldSms_logDTO.smsReceiverTypeCat).isEmpty()) {
						mapOfSms_logDTOTosmsReceiverTypeCat.remove(oldSms_logDTO.smsReceiverTypeCat);
					}
					
					if(mapOfSms_logDTOTomessage.containsKey(oldSms_logDTO.message)) {
						mapOfSms_logDTOTomessage.get(oldSms_logDTO.message).remove(oldSms_logDTO);
					}
					if(mapOfSms_logDTOTomessage.get(oldSms_logDTO.message).isEmpty()) {
						mapOfSms_logDTOTomessage.remove(oldSms_logDTO.message);
					}
					
					if(mapOfSms_logDTOTosmsStatusCat.containsKey(oldSms_logDTO.smsStatusCat)) {
						mapOfSms_logDTOTosmsStatusCat.get(oldSms_logDTO.smsStatusCat).remove(oldSms_logDTO);
					}
					if(mapOfSms_logDTOTosmsStatusCat.get(oldSms_logDTO.smsStatusCat).isEmpty()) {
						mapOfSms_logDTOTosmsStatusCat.remove(oldSms_logDTO.smsStatusCat);
					}
					
					if(mapOfSms_logDTOToinsertionTime.containsKey(oldSms_logDTO.insertionTime)) {
						mapOfSms_logDTOToinsertionTime.get(oldSms_logDTO.insertionTime).remove(oldSms_logDTO);
					}
					if(mapOfSms_logDTOToinsertionTime.get(oldSms_logDTO.insertionTime).isEmpty()) {
						mapOfSms_logDTOToinsertionTime.remove(oldSms_logDTO.insertionTime);
					}
					
					if(mapOfSms_logDTOTofetchingTime.containsKey(oldSms_logDTO.fetchingTime)) {
						mapOfSms_logDTOTofetchingTime.get(oldSms_logDTO.fetchingTime).remove(oldSms_logDTO);
					}
					if(mapOfSms_logDTOTofetchingTime.get(oldSms_logDTO.fetchingTime).isEmpty()) {
						mapOfSms_logDTOTofetchingTime.remove(oldSms_logDTO.fetchingTime);
					}
					
					if(mapOfSms_logDTOTosendingTime.containsKey(oldSms_logDTO.sendingTime)) {
						mapOfSms_logDTOTosendingTime.get(oldSms_logDTO.sendingTime).remove(oldSms_logDTO);
					}
					if(mapOfSms_logDTOTosendingTime.get(oldSms_logDTO.sendingTime).isEmpty()) {
						mapOfSms_logDTOTosendingTime.remove(oldSms_logDTO.sendingTime);
					}
					
					if(mapOfSms_logDTOTodeliveryTime.containsKey(oldSms_logDTO.deliveryTime)) {
						mapOfSms_logDTOTodeliveryTime.get(oldSms_logDTO.deliveryTime).remove(oldSms_logDTO);
					}
					if(mapOfSms_logDTOTodeliveryTime.get(oldSms_logDTO.deliveryTime).isEmpty()) {
						mapOfSms_logDTOTodeliveryTime.remove(oldSms_logDTO.deliveryTime);
					}
					
					
				}
				if(sms_logDTO.isDeleted == 0) 
				{
					
					mapOfSms_logDTOToiD.put(sms_logDTO.iD, sms_logDTO);
				
					if( ! mapOfSms_logDTOTosender.containsKey(sms_logDTO.sender)) {
						mapOfSms_logDTOTosender.put(sms_logDTO.sender, new HashSet<>());
					}
					mapOfSms_logDTOTosender.get(sms_logDTO.sender).add(sms_logDTO);
					
					if( ! mapOfSms_logDTOToreceiver.containsKey(sms_logDTO.receiver)) {
						mapOfSms_logDTOToreceiver.put(sms_logDTO.receiver, new HashSet<>());
					}
					mapOfSms_logDTOToreceiver.get(sms_logDTO.receiver).add(sms_logDTO);
					
					if( ! mapOfSms_logDTOTosmsReceiverTypeCat.containsKey(sms_logDTO.smsReceiverTypeCat)) {
						mapOfSms_logDTOTosmsReceiverTypeCat.put(sms_logDTO.smsReceiverTypeCat, new HashSet<>());
					}
					mapOfSms_logDTOTosmsReceiverTypeCat.get(sms_logDTO.smsReceiverTypeCat).add(sms_logDTO);
					
					if( ! mapOfSms_logDTOTomessage.containsKey(sms_logDTO.message)) {
						mapOfSms_logDTOTomessage.put(sms_logDTO.message, new HashSet<>());
					}
					mapOfSms_logDTOTomessage.get(sms_logDTO.message).add(sms_logDTO);
					
					if( ! mapOfSms_logDTOTosmsStatusCat.containsKey(sms_logDTO.smsStatusCat)) {
						mapOfSms_logDTOTosmsStatusCat.put(sms_logDTO.smsStatusCat, new HashSet<>());
					}
					mapOfSms_logDTOTosmsStatusCat.get(sms_logDTO.smsStatusCat).add(sms_logDTO);
					
					if( ! mapOfSms_logDTOToinsertionTime.containsKey(sms_logDTO.insertionTime)) {
						mapOfSms_logDTOToinsertionTime.put(sms_logDTO.insertionTime, new HashSet<>());
					}
					mapOfSms_logDTOToinsertionTime.get(sms_logDTO.insertionTime).add(sms_logDTO);
					
					if( ! mapOfSms_logDTOTofetchingTime.containsKey(sms_logDTO.fetchingTime)) {
						mapOfSms_logDTOTofetchingTime.put(sms_logDTO.fetchingTime, new HashSet<>());
					}
					mapOfSms_logDTOTofetchingTime.get(sms_logDTO.fetchingTime).add(sms_logDTO);
					
					if( ! mapOfSms_logDTOTosendingTime.containsKey(sms_logDTO.sendingTime)) {
						mapOfSms_logDTOTosendingTime.put(sms_logDTO.sendingTime, new HashSet<>());
					}
					mapOfSms_logDTOTosendingTime.get(sms_logDTO.sendingTime).add(sms_logDTO);
					
					if( ! mapOfSms_logDTOTodeliveryTime.containsKey(sms_logDTO.deliveryTime)) {
						mapOfSms_logDTOTodeliveryTime.put(sms_logDTO.deliveryTime, new HashSet<>());
					}
					mapOfSms_logDTOTodeliveryTime.get(sms_logDTO.deliveryTime).add(sms_logDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Sms_logDTO> getSms_logList() {
		List <Sms_logDTO> sms_logs = new ArrayList<Sms_logDTO>(this.mapOfSms_logDTOToiD.values());
		return sms_logs;
	}
	
	
	public Sms_logDTO getSms_logDTOByID( long ID){
		return mapOfSms_logDTOToiD.get(ID);
	}
	
	
	public List<Sms_logDTO> getSms_logDTOBysender(String sender) {
		return new ArrayList<>( mapOfSms_logDTOTosender.getOrDefault(sender,new HashSet<>()));
	}
	
	
	public List<Sms_logDTO> getSms_logDTOByreceiver(String receiver) {
		return new ArrayList<>( mapOfSms_logDTOToreceiver.getOrDefault(receiver,new HashSet<>()));
	}
	
	
	public List<Sms_logDTO> getSms_logDTOBysms_receiver_type_cat(int sms_receiver_type_cat) {
		return new ArrayList<>( mapOfSms_logDTOTosmsReceiverTypeCat.getOrDefault(sms_receiver_type_cat,new HashSet<>()));
	}
	
	
	public List<Sms_logDTO> getSms_logDTOBymessage(String message) {
		return new ArrayList<>( mapOfSms_logDTOTomessage.getOrDefault(message,new HashSet<>()));
	}
	
	
	public List<Sms_logDTO> getSms_logDTOBysms_status_cat(int sms_status_cat) {
		return new ArrayList<>( mapOfSms_logDTOTosmsStatusCat.getOrDefault(sms_status_cat,new HashSet<>()));
	}
	
	
	public List<Sms_logDTO> getSms_logDTOByinsertionTime(long insertionTime) {
		return new ArrayList<>( mapOfSms_logDTOToinsertionTime.getOrDefault(insertionTime,new HashSet<>()));
	}
	
	
	public List<Sms_logDTO> getSms_logDTOByfetchingTime(long fetchingTime) {
		return new ArrayList<>( mapOfSms_logDTOTofetchingTime.getOrDefault(fetchingTime,new HashSet<>()));
	}
	
	
	public List<Sms_logDTO> getSms_logDTOBysendingTime(long sendingTime) {
		return new ArrayList<>( mapOfSms_logDTOTosendingTime.getOrDefault(sendingTime,new HashSet<>()));
	}
	
	
	public List<Sms_logDTO> getSms_logDTOBydeliveryTime(long deliveryTime) {
		return new ArrayList<>( mapOfSms_logDTOTodeliveryTime.getOrDefault(deliveryTime,new HashSet<>()));
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "sms_log";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


