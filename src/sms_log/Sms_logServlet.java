package sms_log;

import java.io.IOException;
import java.io.*;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import org.apache.log4j.Logger;

import login.LoginDTO;
import permission.MenuConstants;
import role.PermissionRepository;


import sessionmanager.SessionConstants;

import sms.SmsSender;
import sms.SmsService;
import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;
import util.CommonRequestHandler;
import util.RecordNavigationManager4;

import java.util.*;
import javax.servlet.http.*;
import java.util.UUID;

import sms_log.Constants;




import geolocation.GeoLocationDAO2;
import java.util.StringTokenizer;

import com.google.gson.Gson;

import pb.*;
import pbReport.*;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;




/**
 * Servlet implementation class Sms_logServlet
 */
@WebServlet("/Sms_logServlet")
@MultipartConfig
public class Sms_logServlet extends HttpServlet 
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Sms_logServlet.class);

    String tableName = "sms_log";

	Sms_logDAO sms_logDAO;
	CommonRequestHandler commonRequestHandler;
    private final Gson gson = new Gson();
    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Sms_logServlet() 
	{
        super();
    	try
    	{
			sms_logDAO = new Sms_logDAO(tableName);
			commonRequestHandler = new CommonRequestHandler(sms_logDAO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }   

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		System.out.println("In doget request = " + request);
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		try
		{
			String actionType = request.getParameter("actionType");
			if(actionType.equals("getAddPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.SMS_LOG_ADD))
				{
					commonRequestHandler.getAddPage(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getEditPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.SMS_LOG_UPDATE))
				{
					getSms_log(request, response);					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}						
			}
			else if(actionType.equals("getURL"))
			{
				String URL = request.getParameter("URL");
				System.out.println("URL = " + URL);
				response.sendRedirect(URL);			
			}			
			else if(actionType.equals("search"))
			{
				System.out.println("search requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.SMS_LOG_SEARCH))
				{
					
					if(isPermanentTable)
					{
						searchSms_log(request, response, isPermanentTable);
					}
					else
					{
						//searchSms_log(request, response, tempTableName, isPermanentTable);
					}
				}			
			}
			else if(actionType.equals("view"))
			{
				System.out.println("view requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.SMS_LOG_SEARCH))
				{
					commonRequestHandler.view(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else
			{
				request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}


	
	

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		// TODO Auto-generated method stub
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		System.out.println("doPost");
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		
		try
		{
			String actionType = request.getParameter("actionType");
			System.out.println("actionType = " + actionType);
			if(actionType.equals("add"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.SMS_LOG_ADD))
				{
					System.out.println("going to  addSms_log ");
					addSms_log(request, response, true, userDTO, true);
				}
				else
				{
					System.out.println("Not going to  addSms_log ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}

			else if(actionType.equals("getDTO"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.SMS_LOG_ADD))
				{
					getDTO(request, response);					
				}
				else
				{
					System.out.println("Not going to  addSms_log ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else if(actionType.equals("edit"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.SMS_LOG_UPDATE))
				{					
					addSms_log(request, response, false, userDTO, isPermanentTable);					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("delete"))
			{								
				deleteSms_log(request, response, userDTO);				
			}	
			else if(actionType.equals("search"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.SMS_LOG_SEARCH))
				{
					searchSms_log(request, response, true);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getGeo"))
			{
				System.out.println("going to geoloc ");
				request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
			}
			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}
	
	private void getDTO(HttpServletRequest request, HttpServletResponse response) 
	{
		try 
		{
			System.out.println("In getDTO");
			Sms_logDTO sms_logDTO = (Sms_logDTO)sms_logDAO.getDTOByID(Long.parseLong(request.getParameter("ID")));
			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			
			String encoded = this.gson.toJson(sms_logDTO);
			System.out.println("json encoded data = " + encoded);
			out.print(encoded);
			out.flush();
		}
		catch (NumberFormatException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	private void addSms_log(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, boolean isPermanentTable) throws IOException 
	{
		// TODO Auto-generated method stub
		try 
		{
			request.setAttribute("failureMessage", "");
			System.out.println("%%%% addSms_log");
			String path = getServletContext().getRealPath("/img2/");
			Sms_logDTO sms_logDTO;
						
			if(addFlag == true)
			{
				sms_logDTO = new Sms_logDTO();
			}
			else
			{
				sms_logDTO = (Sms_logDTO)sms_logDAO.getDTOByID(Long.parseLong(request.getParameter("iD")));
			}
			
			String Value = "";

			Value = request.getParameter("sender");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("sender = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				sms_logDTO.sender = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("receiver");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("receiver = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				sms_logDTO.receiver = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("smsReceiverTypeCat");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("smsReceiverTypeCat = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				sms_logDTO.smsReceiverTypeCat = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("message");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("message = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				sms_logDTO.message = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("smsStatusCat");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("smsStatusCat = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				sms_logDTO.smsStatusCat = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			if( addFlag == true ) {
				sms_logDTO.insertionTime = System.currentTimeMillis();
				sms_logDTO.fetchingTime = System.currentTimeMillis();
				sms_logDTO.sendingTime = System.currentTimeMillis();
				sms_logDTO.deliveryTime = System.currentTimeMillis();
			}
			
			System.out.println("Done adding  addSms_log dto = " + sms_logDTO);
			long returnedID = -1;
			
			if(isPermanentTable == false) //add new row for validation and make the old row outdated
			{
				sms_logDAO.setIsDeleted(sms_logDTO.iD, CommonDTO.OUTDATED);
				returnedID = sms_logDAO.add(sms_logDTO);
				sms_logDAO.setIsDeleted(returnedID, CommonDTO.WAITING_FOR_APPROVAL);
			}
			else if(addFlag == true)
			{
				returnedID = sms_logDAO.manageWriteOperations(sms_logDTO, SessionConstants.INSERT, -1, userDTO);
			}
			else
			{				
				returnedID = sms_logDAO.manageWriteOperations(sms_logDTO, SessionConstants.UPDATE, -1, userDTO);											
			}
			
			if(isPermanentTable)
			{
				String inPlaceSubmit = request.getParameter("inplacesubmit");
				
				if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
				{
					getSms_log(request, response, returnedID);
				}
				else
				{
					response.sendRedirect("Sms_logServlet?actionType=search");
				}
			}
			else
			{
				commonRequestHandler.validate(sms_logDAO.getDTOByID(returnedID), request, response, userDTO);
			}
					
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	



	
	
	

	private void deleteSms_log(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO) throws IOException 
	{				
		try 
		{
			String[] IDsToDelete = request.getParameterValues("ID");
			for(int i = 0; i < IDsToDelete.length; i ++)
			{
				long id = Long.parseLong(IDsToDelete[i]);
				System.out.println("------ DELETING " + IDsToDelete[i]);
				
				
				Sms_logDTO sms_logDTO = (Sms_logDTO)sms_logDAO.getDTOByID(id);
				sms_logDAO.manageWriteOperations(sms_logDTO, SessionConstants.DELETE, id, userDTO);
				response.sendRedirect("Sms_logServlet?actionType=search");
				
			}			
		}
		catch (Exception ex) 
		{
			ex.printStackTrace();
		}
		
	}

	private void getSms_log(HttpServletRequest request, HttpServletResponse response, long id) throws ServletException, IOException
	{
		System.out.println("in getSms_log");
		Sms_logDTO sms_logDTO = null;
		try 
		{
			sms_logDTO = (Sms_logDTO)sms_logDAO.getDTOByID(id);
			request.setAttribute("ID", sms_logDTO.iD);
			request.setAttribute("sms_logDTO",sms_logDTO);
			request.setAttribute("sms_logDAO",sms_logDAO);
			
			String URL= "";
			
			String inPlaceEdit = request.getParameter("inplaceedit");
			String inPlaceSubmit = request.getParameter("inplacesubmit");
			String getBodyOnly = request.getParameter("getBodyOnly");
			
			if(inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase(""))
			{
				URL = "sms_log/sms_logInPlaceEdit.jsp";	
				request.setAttribute("inplaceedit","");				
			}
			else if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
			{
				URL = "sms_log/sms_logSearchRow.jsp";
				request.setAttribute("inplacesubmit","");					
			}
			else
			{
				if(getBodyOnly  != null && !getBodyOnly.equalsIgnoreCase(""))
				{
					URL = "sms_log/sms_logEditBody.jsp?actionType=edit";
				}
				else
				{
					URL = "sms_log/sms_logEdit.jsp?actionType=edit";
				}				
			}
			
			RequestDispatcher rd = request.getRequestDispatcher(URL);
			rd.forward(request, response);
		}
		catch (NumberFormatException e) 
		{
			e.printStackTrace();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	
	private void getSms_log(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		getSms_log(request, response, Long.parseLong(request.getParameter("ID")));	
	}
	
	private void searchSms_log(HttpServletRequest request, HttpServletResponse response, boolean isPermanent) throws ServletException, IOException
	{
		System.out.println("in  searchSms_log 1");
		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String ajax = request.getParameter("ajax");
		boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);
		
        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
			SessionConstants.NAV_SMS_LOG,
			request,
			sms_logDAO,
			SessionConstants.VIEW_SMS_LOG,
			SessionConstants.SEARCH_SMS_LOG,
			tableName,
			isPermanent,
			userDTO,
			"",
			true);
        try
        {
			System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        }
        catch(Exception e)
        {
			System.out.println("failed to dojob" + e);
        }

		request.setAttribute("sms_logDAO",sms_logDAO);
        RequestDispatcher rd;
        if(!isPermanent)
        {
        	if(hasAjax == false)
	        {
	        	System.out.println("Going to sms_log/sms_logApproval.jsp");
	        	rd = request.getRequestDispatcher("sms_log/sms_logApproval.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to sms_log/sms_logApprovalForm.jsp");
	        	rd = request.getRequestDispatcher("sms_log/sms_logApprovalForm.jsp");
	        }
        }
        else
        {
	        if(hasAjax == false)
	        {
	        	System.out.println("Going to sms_log/sms_logSearch.jsp");
	        	rd = request.getRequestDispatcher("sms_log/sms_logSearch.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to sms_log/sms_logSearchForm.jsp");
	        	rd = request.getRequestDispatcher("sms_log/sms_logSearchForm.jsp");
	        }
        }
		rd.forward(request, response);
	}
	
}

