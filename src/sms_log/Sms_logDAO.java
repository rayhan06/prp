package sms_log;

import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;
import sessionmanager.SessionConstants;


import org.apache.log4j.Logger;

import dbm.*;

import repository.RepositoryManager;

import util.*;
import pb.*;
import user.UserDTO;

public class Sms_logDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());

	private static final Sms_logDAO sms_logDAO = new Sms_logDAO();

	public static Sms_logDAO getInstance(){

		return sms_logDAO;
	}
	
	public Sms_logDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";

		joinSQL += " join category on (";
		joinSQL += " (" + tableName + ".sms_receiver_type_cat = category.value  and category.domain_name = 'sms_receiver_type')";
		joinSQL += " or "; 
		joinSQL += " (" + tableName + ".sms_status_cat = category.value  and category.domain_name = 'sms_status')";
		joinSQL += " )";
		joinSQL += " join language_text on category.language_id = language_text.id";		
		commonMaps = new Sms_logMAPS(tableName);
	}
	
	public Sms_logDAO()
	{
		this("sms_log");		
	}

	
	public long add(CommonDTO commonDTO) throws Exception
	{
		
		Sms_logDTO sms_logDTO = (Sms_logDTO)commonDTO;
		
		Connection connection = null;
		PreparedStatement ps = null;

		long lastModificationTime = System.currentTimeMillis();	

		try{
			connection = DBMW.getInstance().getConnection();
			
			if(connection == null)
			{
				System.out.println("nullconn");
			}

			sms_logDTO.iD = DBMW.getInstance().getNextSequenceId(tableName);

			String sql = "INSERT INTO " + tableName;
			
			sql += " (";
			sql += "ID";
			sql += ", ";
			sql += "sender";
			sql += ", ";
			sql += "receiver";
			sql += ", ";
			sql += "sms_receiver_type_cat";
			sql += ", ";
			sql += "message";
			sql += ", ";
			sql += "sms_status_cat";
			sql += ", ";
			sql += "insertionTime";
			sql += ", ";
			sql += "fetchingTime";
			sql += ", ";
			sql += "sendingTime";
			sql += ", ";
			sql += "deliveryTime";
			sql += ", ";
			sql += "lastModificationTime";
		
			sql += ")";
			
			
            sql += " VALUES(";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			
			sql += ")";
				
			ps = connection.prepareStatement(sql);
			
			int index = 1;

			ps.setObject(index++,sms_logDTO.iD);
			ps.setObject(index++,sms_logDTO.sender);
			ps.setObject(index++,sms_logDTO.receiver);
			ps.setObject(index++,sms_logDTO.smsReceiverTypeCat);
			ps.setObject(index++,sms_logDTO.message);
			ps.setObject(index++,sms_logDTO.smsStatusCat);
			ps.setObject(index++,sms_logDTO.insertionTime);
			ps.setObject(index++,sms_logDTO.fetchingTime);
			ps.setObject(index++,sms_logDTO.sendingTime);
			ps.setObject(index++,sms_logDTO.deliveryTime);
			ps.setObject(index++, lastModificationTime);
			
			System.out.println(ps);
			ps.execute();
			
			
			recordUpdateTime(connection, lastModificationTime);

		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if (ps != null) {
					ps.close();
				}
			} catch(Exception e){}
			try{
				if(connection != null)
				{
					DBMW.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		return sms_logDTO.iD;		
	}
		
	

	//need another getter for repository
	public CommonDTO getDTOByID (long ID) throws Exception
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Sms_logDTO sms_logDTO = null;
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
			
            sql += " WHERE ID=" + ID;
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			if(rs.next()){

				sms_logDTO = getSmsLogDTFromResultSet( rs );
			}

		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return sms_logDTO;
	}

	public int[] updateJobIdAndStatusBySmsLogList(List<Sms_logDTO> smsLogDTOList ){

		Connection connection = null;
		PreparedStatement ps = null;
		int[] affectedRecords = null;

		try{

			String sql = "UPDATE " + tableName + " set job_id = ?, sms_status_cat = ? where ID = ?";

			connection = DBMW.getInstance().getConnection();
			ps = connection.prepareStatement(sql);

			for( Sms_logDTO smsLogDTO:smsLogDTOList ){

				if( smsLogDTO.jobId != null )
					ps.setLong( 1, smsLogDTO.jobId );
				else
					ps.setLong( 1, -1L );

				ps.setLong( 2, smsLogDTO.smsStatusCat );
				ps.setLong( 3, smsLogDTO.iD );
				ps.addBatch();
			}

			affectedRecords = ps.executeBatch();

		}catch(Exception ex){

			ex.printStackTrace();
		}
		finally{
			try{
				if (ps != null) {

					ps.close();
				}
			}
			catch(Exception e){

			}
			try{
				if(connection != null){

					DBMW.getInstance().freeConnection(connection);
				}
			}
			catch(Exception ex2){

			}
		}

		return affectedRecords;
	}
	
	public long update(CommonDTO commonDTO) throws Exception
	{		
		Sms_logDTO sms_logDTO = (Sms_logDTO)commonDTO;
		
		Connection connection = null;
		PreparedStatement ps = null;

		long lastModificationTime = System.currentTimeMillis();	
		try{
			connection = DBMW.getInstance().getConnection();

			String sql = "UPDATE " + tableName;
			
			sql += " SET ";
			sql += "sender=?";
			sql += ", ";
			sql += "receiver=?";
			sql += ", ";
			sql += "sms_receiver_type_cat=?";
			sql += ", ";
			sql += "message=?";
			sql += ", ";
			sql += "sms_status_cat=?";
			sql += ", ";
			sql += "insertionTime=?";
			sql += ", ";
			sql += "fetchingTime=?";
			sql += ", ";
			sql += "sendingTime=?";
			sql += ", ";
			sql += "deliveryTime=?";
			sql += ", lastModificationTime = "	+ lastModificationTime + "";
            sql += " WHERE ID = " + sms_logDTO.iD;
				

			ps = connection.prepareStatement(sql);
			
			

			int index = 1;
			
			ps.setObject(index++,sms_logDTO.sender);
			ps.setObject(index++,sms_logDTO.receiver);
			ps.setObject(index++,sms_logDTO.smsReceiverTypeCat);
			ps.setObject(index++,sms_logDTO.message);
			ps.setObject(index++,sms_logDTO.smsStatusCat);
			ps.setObject(index++,sms_logDTO.insertionTime);
			ps.setObject(index++,sms_logDTO.fetchingTime);
			ps.setObject(index++,sms_logDTO.sendingTime);
			ps.setObject(index++,sms_logDTO.deliveryTime);
			System.out.println(ps);
			ps.executeUpdate();
			
			recordUpdateTime(connection, lastModificationTime);

		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if (ps != null) {
					ps.close();
				}
			} catch(Exception e){}
			try{
				if(connection != null)
				{
					DBMW.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		return sms_logDTO.iD;
	}
	
	
	public List<Sms_logDTO> getDTOs(Collection recordIDs){
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Sms_logDTO sms_logDTO = null;
		List<Sms_logDTO> sms_logDTOList = new ArrayList<>();
		if(recordIDs.isEmpty()){
			return sms_logDTOList;
		}
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
            
            sql += " WHERE ID IN ( ";

			for(int i = 0;i<recordIDs.size();i++){
				if(i!=0){
					sql+=",";
				}
				sql+=((ArrayList)recordIDs).get(i);
			}
			sql+=")  order by lastModificationTime desc";
			
			printSql(sql);
			
			logger.debug("sql " + sql);
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){

				sms_logDTO = getSmsLogDTFromResultSet( rs );
				sms_logDTOList.add(sms_logDTO);
			}
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return sms_logDTOList;
	
	}
	
	//add repository
	public List<Sms_logDTO> getAllSms_log (boolean isFirstReload)
    {
		List<Sms_logDTO> sms_logDTOList = new ArrayList<>();

		String sql = "SELECT * FROM sms_log";
		sql += " WHERE ";
	

		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by sms_log.lastModificationTime desc";
		printSql(sql);
		
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;

		
		try{

			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();
			rs = stmt.executeQuery(sql);

			while(rs.next()){

				Sms_logDTO sms_logDTO = getSmsLogDTFromResultSet( rs );
				sms_logDTOList.add(sms_logDTO);
			}
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}

		return sms_logDTOList;
    }

	public List<Sms_logDTO> getAllSmsLogByInsertionTimeGreater ( Long insertionTime ){

		List<Sms_logDTO> sms_logDTOList = new ArrayList<>();

		String sql = "SELECT * FROM sms_log WHERE sms_status_cat = 0 and insertionTime > " + insertionTime + " limit 50";
		printSql(sql);

		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;

		try{

			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();
			rs = stmt.executeQuery(sql);

			while( rs.next() ){

				Sms_logDTO sms_logDTO = getSmsLogDTFromResultSet( rs );
				sms_logDTOList.add(sms_logDTO);
			}
		}
		catch(Exception ex){

			ex.printStackTrace();
		}
		finally{

			try{
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			try{
				if (connection != null)
				{
					DBMR.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}

		return sms_logDTOList;
	}

	public List<Sms_logDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO)
	{
		return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
	}

	
	public List<Sms_logDTO> getDTOs(Hashtable  p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
			String filter, boolean tableHasJobCat)
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		List<Sms_logDTO> sms_logDTOList = new ArrayList<>();

		try{
			
			String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat);
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){

				Sms_logDTO sms_logDTO = getSmsLogDTFromResultSet( rs );
				sms_logDTOList.add(sms_logDTO);
			}					
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return sms_logDTOList;
	
	}

	private Sms_logDTO getSmsLogDTFromResultSet( ResultSet rs ) throws SQLException {

		Sms_logDTO sms_logDTO = new Sms_logDTO();

		sms_logDTO.iD = rs.getLong("ID");
		sms_logDTO.sender = rs.getString("sender");
		sms_logDTO.receiver = rs.getString("receiver");
		sms_logDTO.smsReceiverTypeCat = rs.getInt("sms_receiver_type_cat");
		sms_logDTO.message = rs.getString("message");
		sms_logDTO.smsStatusCat = rs.getInt("sms_status_cat");
		sms_logDTO.insertionTime = rs.getLong("insertionTime");
		sms_logDTO.fetchingTime = rs.getLong("fetchingTime");
		sms_logDTO.sendingTime = rs.getLong("sendingTime");
		sms_logDTO.deliveryTime = rs.getLong("deliveryTime");
		sms_logDTO.jobId = rs.getLong( "job_id" );

		return sms_logDTO;
	}
				
}
	