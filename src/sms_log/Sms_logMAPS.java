package sms_log;
import java.util.*; 
import util.*;


public class Sms_logMAPS extends CommonMaps
{	
	public Sms_logMAPS(String tableName)
	{
		
		java_allfield_type_map.put("sender".toLowerCase(), "String");
		java_allfield_type_map.put("receiver".toLowerCase(), "String");
		java_allfield_type_map.put("sms_receiver_type_cat".toLowerCase(), "Integer");
		java_allfield_type_map.put("message".toLowerCase(), "String");
		java_allfield_type_map.put("sms_status_cat".toLowerCase(), "Integer");
		java_allfield_type_map.put("insertionTime".toLowerCase(), "Long");
		java_allfield_type_map.put("fetchingTime".toLowerCase(), "Long");
		java_allfield_type_map.put("sendingTime".toLowerCase(), "Long");
		java_allfield_type_map.put("deliveryTime".toLowerCase(), "Long");

		java_anyfield_search_map.put("language_text.languageTextEnglish", "String");
		java_anyfield_search_map.put("language_text.languageTextBangla", "String");

		java_anyfield_search_map.put(tableName + ".sender".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".receiver".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".message".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".insertionTime".toLowerCase(), "Long");
		java_anyfield_search_map.put(tableName + ".fetchingTime".toLowerCase(), "Long");
		java_anyfield_search_map.put(tableName + ".sendingTime".toLowerCase(), "Long");
		java_anyfield_search_map.put(tableName + ".deliveryTime".toLowerCase(), "Long");

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("sender".toLowerCase(), "sender".toLowerCase());
		java_DTO_map.put("receiver".toLowerCase(), "receiver".toLowerCase());
		java_DTO_map.put("smsReceiverTypeCat".toLowerCase(), "smsReceiverTypeCat".toLowerCase());
		java_DTO_map.put("message".toLowerCase(), "message".toLowerCase());
		java_DTO_map.put("smsStatusCat".toLowerCase(), "smsStatusCat".toLowerCase());
		java_DTO_map.put("insertionTime".toLowerCase(), "insertionTime".toLowerCase());
		java_DTO_map.put("fetchingTime".toLowerCase(), "fetchingTime".toLowerCase());
		java_DTO_map.put("sendingTime".toLowerCase(), "sendingTime".toLowerCase());
		java_DTO_map.put("deliveryTime".toLowerCase(), "deliveryTime".toLowerCase());

		java_SQL_map.put("sender".toLowerCase(), "sender".toLowerCase());
		java_SQL_map.put("receiver".toLowerCase(), "receiver".toLowerCase());
		java_SQL_map.put("sms_receiver_type_cat".toLowerCase(), "smsReceiverTypeCat".toLowerCase());
		java_SQL_map.put("message".toLowerCase(), "message".toLowerCase());
		java_SQL_map.put("sms_status_cat".toLowerCase(), "smsStatusCat".toLowerCase());
		java_SQL_map.put("insertionTime".toLowerCase(), "insertionTime".toLowerCase());
		java_SQL_map.put("fetchingTime".toLowerCase(), "fetchingTime".toLowerCase());
		java_SQL_map.put("sendingTime".toLowerCase(), "sendingTime".toLowerCase());
		java_SQL_map.put("deliveryTime".toLowerCase(), "deliveryTime".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Sender".toLowerCase(), "sender".toLowerCase());
		java_Text_map.put("Receiver".toLowerCase(), "receiver".toLowerCase());
		java_Text_map.put("Sms Receiver Type Cat".toLowerCase(), "smsReceiverTypeCat".toLowerCase());
		java_Text_map.put("Message".toLowerCase(), "message".toLowerCase());
		java_Text_map.put("Sms Status Cat".toLowerCase(), "smsStatusCat".toLowerCase());
		java_Text_map.put("InsertionTime".toLowerCase(), "insertionTime".toLowerCase());
		java_Text_map.put("FetchingTime".toLowerCase(), "fetchingTime".toLowerCase());
		java_Text_map.put("SendingTime".toLowerCase(), "sendingTime".toLowerCase());
		java_Text_map.put("DeliveryTime".toLowerCase(), "deliveryTime".toLowerCase());
			
	}

}