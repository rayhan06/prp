package approval_path;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;
import sessionmanager.SessionConstants;
import java.sql.SQLException;
import common.ConnectionAndStatementUtil;


import org.apache.log4j.Logger;

import dbm.*;

import repository.RepositoryManager;

import util.*;
import pb.*;
import user.UserDTO;

public class Approval_pathDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());

	public Approval_pathDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";
		commonMaps = new Approval_pathMAPS(tableName);
		approvalMaps = new Approval_pathApprovalMAPS("approval_path");
		columnNames = new String[] 
		{
			"ID",
			"name_en",
			"name_bn",
			"inserted_by_user_id",
			"inserted_by_organogram_id",
			"insertion_date",
			"last_modifier_user",
			"job_cat",
			"office_unit_id",
			"module",
			"module_cat",
			"search_column",
			"isVisible",
			"isDeleted",
			"lastModificationTime"
		};
	}
	
	public Approval_pathDAO()
	{
		this("approval_path");		
	}
	
	public void setSearchColumn(Approval_pathDTO approval_pathDTO)
	{
		approval_pathDTO.searchColumn = "";
		approval_pathDTO.searchColumn += approval_pathDTO.nameEn + " ";
		approval_pathDTO.searchColumn += approval_pathDTO.nameBn + " ";
		approval_pathDTO.searchColumn += approval_pathDTO.module + " ";
		approval_pathDTO.searchColumn += CatDAO.getName("English", "module", approval_pathDTO.moduleCat) + " " + CatDAO.getName("Bangla", "module", approval_pathDTO.moduleCat) + " ";
	}
	
	public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException
	{
		Approval_pathDTO approval_pathDTO = (Approval_pathDTO)commonDTO;
		int index = 1;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(approval_pathDTO);
		if(isInsert)
		{
			ps.setObject(index++,approval_pathDTO.iD);
		}
		ps.setObject(index++,approval_pathDTO.nameEn);
		ps.setObject(index++,approval_pathDTO.nameBn);
		ps.setObject(index++,approval_pathDTO.insertedByUserId);
		ps.setObject(index++,approval_pathDTO.insertedByOrganogramId);
		ps.setObject(index++,approval_pathDTO.insertionDate);
		ps.setObject(index++,approval_pathDTO.lastModifierUser);
		ps.setObject(index++,approval_pathDTO.jobCat);
		ps.setObject(index++,approval_pathDTO.officeUnitId);
		ps.setObject(index++,approval_pathDTO.module);
		ps.setObject(index++,approval_pathDTO.moduleCat);
		ps.setObject(index++,approval_pathDTO.searchColumn);
		ps.setObject(index++,approval_pathDTO.isVisible);
		if(isInsert)
		{
			ps.setObject(index++, 0);
			ps.setObject(index++, lastModificationTime);
		}
	}
	
	public Approval_pathDTO build(ResultSet rs)
	{
		try
		{
			Approval_pathDTO approval_pathDTO = new Approval_pathDTO();
			approval_pathDTO.iD = rs.getLong("ID");
			approval_pathDTO.nameEn = rs.getString("name_en");
			approval_pathDTO.nameBn = rs.getString("name_bn");
			approval_pathDTO.insertedByUserId = rs.getLong("inserted_by_user_id");
			approval_pathDTO.insertedByOrganogramId = rs.getLong("inserted_by_organogram_id");
			approval_pathDTO.insertionDate = rs.getLong("insertion_date");
			approval_pathDTO.lastModifierUser = rs.getString("last_modifier_user");
			approval_pathDTO.jobCat = (int)approval_pathDTO.iD;
			approval_pathDTO.officeUnitId = rs.getLong("office_unit_id");
			approval_pathDTO.module = rs.getString("module");
			approval_pathDTO.moduleCat = rs.getInt("module_cat");
			approval_pathDTO.searchColumn = rs.getString("search_column");
			approval_pathDTO.isVisible = rs.getBoolean("isVisible");
			approval_pathDTO.isDeleted = rs.getInt("isDeleted");
			approval_pathDTO.lastModificationTime = rs.getLong("lastModificationTime");
			return approval_pathDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
	
	

	public Approval_pathDTO getDTOByID (long id)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID = " + id;
		Approval_pathDTO approval_pathDTO = ConnectionAndStatementUtil.getT(sql,this::build);
		try {
			ApprovalPathDetailsDAO approvalPathDetailsDAO = new ApprovalPathDetailsDAO("approval_path_details");			
			List<ApprovalPathDetailsDTO> approvalPathDetailsDTOList = approvalPathDetailsDAO.getApprovalPathDetailsDTOListByApprovalPathID(approval_pathDTO.iD);
			approval_pathDTO.approvalPathDetailsDTOList = approvalPathDetailsDTOList;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}			
		try {
			ApprovalNotificationRecipientsDAO approvalNotificationRecipientsDAO = new ApprovalNotificationRecipientsDAO("approval_notification_recipients");			
			List<ApprovalNotificationRecipientsDTO> approvalNotificationRecipientsDTOList = approvalNotificationRecipientsDAO.getApprovalNotificationRecipientsDTOListByApprovalPathID(approval_pathDTO.iD);
			approval_pathDTO.approvalNotificationRecipientsDTOList = approvalNotificationRecipientsDTOList;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}			
		return approval_pathDTO;
	}

	
	public List<Approval_pathDTO> getDTOs(Collection recordIDs)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID IN ( ";
		for(int i = 0;i<recordIDs.size();i++){
			if(i!=0){
				sql+=",";
			}
			sql+=((ArrayList)recordIDs).get(i);
		}
		sql+=")  order by lastModificationTime desc";
		printSql(sql);
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);		
	}
	
	public List<Approval_pathDTO> getAllApproval_path (boolean isFirstReload)
    {
		String sql = "SELECT * FROM " + tableName + " WHERE ";
		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by " + tableName + ".lastModificationTime desc";
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);	
    }
	
	public List<Approval_pathDTO> getallByOfficeAndModule(long officeUnit, String module)
    {
		if(!module.matches("^[a-zA-Z0-9_]*$"))
		{
			return null;
		}
		String sql = "SELECT * FROM approval_path where isDeleted = 0 and isVisible = 1 and name_en !='' "
				+ "and (module_cat = -1  or module = '" + module + "') "
				+ "and (office_unit_id = -1  or office_unit_id = " + officeUnit + ") ";
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);	
    }

	
	public List<Approval_pathDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO)
	{
		return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
	}

	
	public List<Approval_pathDTO> getDTOs(Hashtable  p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
			String filter, boolean tableHasJobCat)
	{
		String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat);
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);			
	}
	public String getSqlWithSearchCriteriaForNormalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
			UserDTO userDTO, String filter, boolean tableHasJobCat)
    {
		boolean viewAll = false;
		
		if(p_searchCriteria != null && p_searchCriteria.get("ViewAll") != null)
		{
			System.out.println("ViewAll = " + p_searchCriteria.get("ViewAll"));
			viewAll = true;
		}
		else
		{
			System.out.println("ViewAll is null ");
		}
		
		String sql = "SELECT ";
		if(category == GETIDS)
		{
			sql += " " + tableName + ".ID as ID ";
		}
		else if(category == GETCOUNT)
		{
			sql += " count( " + tableName + ".ID) as countID ";
		}
		else if(category == GETDTOS)
		{
			sql += " distinct " + tableName + ".* ";
		}
		sql += "FROM " + tableName + " ";
		sql += joinSQL;
		

		
		String AnyfieldSql = "";
		String AllFieldSql = "";
		
		if(p_searchCriteria != null)
		{
						
			
			Enumeration names = p_searchCriteria.keys();
			String str, value;
			
			AnyfieldSql = "(";
			
			if(p_searchCriteria.get("AnyField")!= null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase(""))
			{
				AnyfieldSql+= tableName + ".search_column like '%" + p_searchCriteria.get("AnyField").toString() + "%'";			
			}
			AnyfieldSql += ")";
			System.out.println("AnyfieldSql = " + AnyfieldSql);
			
			AllFieldSql = "(";
			int i = 0;
			while(names.hasMoreElements())
			{				
				str = (String) names.nextElement();
				value = (String)p_searchCriteria.get(str);
		        System.out.println(str + ": " + value);
				if(value != null && !value.equalsIgnoreCase("") && (
						 str.equals("name_en")
						|| str.equals("name_bn")
						|| str.equals("insertion_date_start")
						|| str.equals("insertion_date_end")
						|| str.equals("module")
						|| str.equals("module_cat")
				)
						
				)
				{
					if(p_searchCriteria.get(str).equals("any"))
					{
						continue;
					}

					if( i > 0)
					{
						AllFieldSql+= " AND  ";
					}
					
					 if(str.equals("name_en"))
					{
						AllFieldSql += "" + tableName + ".name_en like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("name_bn"))
					{
						AllFieldSql += "" + tableName + ".name_bn like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("insertion_date_start"))
					{
						AllFieldSql += "" + tableName + ".insertion_date >= " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("insertion_date_end"))
					{
						AllFieldSql += "" + tableName + ".insertion_date <= " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("module"))
					{
						AllFieldSql += "" + tableName + ".module like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("module_cat"))
					{
						AllFieldSql += "" + tableName + ".module_cat = " + p_searchCriteria.get(str);
						i ++;
					}
					
					
				}
			}
			
			AllFieldSql += ")";
			System.out.println("AllFieldSql = " + AllFieldSql);
			
			
		}
		
		
		sql += " WHERE ";
		
		sql += " (" + tableName + ".isDeleted = 0 ";			
		sql += ")";
		
		
		if(!filter.equalsIgnoreCase(""))
		{
			sql += " and " + filter + " ";
		}
		
		if(!AnyfieldSql.equals("()") && !AnyfieldSql.equals(""))
		{
			sql += " AND " + AnyfieldSql;
			
		}
		if(!AllFieldSql.equals("()") && !AllFieldSql.equals(""))
		{			
			sql += " AND " + AllFieldSql;
		}
		
	
			
		sql += " order by " + tableName + ".lastModificationTime desc ";

		printSql(sql);
		
		if(limit >= 0)
		{
			sql += " limit " + limit;
		}
		if(offset >= 0)
		{
			sql += " offset " + offset;
		}
		
		System.out.println("-------------- sql = " + sql);
		
		return sql;
    }
	
	public String getSqlWithSearchCriteria(Hashtable p_searchCriteria, int limit, int offset, int category,
			boolean isPermanentTable, UserDTO userDTO, String filter, boolean tableHasJobCat)
    {
		if(isPermanentTable)
		{
			return getSqlWithSearchCriteriaForNormalSearch(p_searchCriteria, limit, offset, category, userDTO, filter, tableHasJobCat);
		}
		else
		{
			return getSqlWithSearchCriteriaForApprovalSearch(p_searchCriteria, limit, offset, category, userDTO, filter, tableHasJobCat);
		}
				
    }
				
}
	