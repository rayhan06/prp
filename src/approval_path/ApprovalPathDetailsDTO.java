package approval_path;
import java.util.*; 
import util.*; 


public class ApprovalPathDetailsDTO extends CommonDTO
{

	public long approvalPathId = -1;
	public int approvalRoleCat = 0;
	public int approvalOrder = 0;
	public long organogramId = -1;
	public int daysRequired = 0;
	public long permanentOrganogramId = -1;
	
	public List<ApprovalPathDetailsDTO> approvalPathDetailsDTOList = new ArrayList<>();
	public List<ApprovalNotificationRecipientsDTO> approvalNotificationRecipientsDTOList = new ArrayList<>();
	
    @Override
	public String toString() {
            return "$ApprovalPathDetailsDTO[" +
            " iD = " + iD +
            " approvalPathId = " + approvalPathId +
            " approvalRoleCat = " + approvalRoleCat +
            " approvalOrder = " + approvalOrder +
            " organogramId = " + organogramId +
            " daysRequired = " + daysRequired +
            " permanentOrganogramId = " + permanentOrganogramId +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}