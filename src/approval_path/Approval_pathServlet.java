package approval_path;

import java.io.IOException;
import java.io.*;
import java.text.SimpleDateFormat;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import org.apache.log4j.Logger;

import login.LoginDTO;
import permission.MenuConstants;
import role.PermissionRepository;


import sessionmanager.SessionConstants;

import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;
import util.CommonRequestHandler;
import util.RecordNavigationManager4;

import java.util.*;
import javax.servlet.http.*;
import java.util.UUID;





import geolocation.GeoLocationDAO2;
import java.util.StringTokenizer;

import com.google.gson.Gson;

import pb.*;
import pbReport.*;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;

import workflow.WorkflowController;
import approval_execution_table.*;
import pb_notifications.Pb_notificationsDAO;



/**
 * Servlet implementation class Approval_pathServlet
 */
@WebServlet("/Approval_pathServlet")
@MultipartConfig
public class Approval_pathServlet extends HttpServlet 
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Approval_pathServlet.class);

    String tableName = "approval_path";

	Approval_pathDAO approval_pathDAO;
	CommonRequestHandler commonRequestHandler;
	ApprovalPathDetailsDAO approvalPathDetailsDAO;
	ApprovalNotificationRecipientsDAO approvalNotificationRecipientsDAO;
    private final Gson gson = new Gson();
    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Approval_pathServlet() 
	{
        super();
    	try
    	{
			approval_pathDAO = new Approval_pathDAO(tableName);
			approvalPathDetailsDAO = new ApprovalPathDetailsDAO("approval_path_details");
			approvalNotificationRecipientsDAO = new ApprovalNotificationRecipientsDAO("approval_notification_recipients");
			commonRequestHandler = new CommonRequestHandler(approval_pathDAO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }   

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		System.out.println("In doget request = " + request);
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		try
		{
			String actionType = request.getParameter("actionType");
			if(actionType.equals("getAddPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.APPROVAL_PATH_ADD))
				{
					commonRequestHandler.getAddPage(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getEditPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.APPROVAL_PATH_UPDATE))
				{
					getApproval_path(request, response);					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}						
			}
			else if(actionType.equals("getURL"))
			{
				String URL = request.getParameter("URL");
				System.out.println("URL = " + URL);
				response.sendRedirect(URL);			
			}			
			else if(actionType.equals("search"))
			{
				System.out.println("search requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.APPROVAL_PATH_SEARCH))
				{
					if(isPermanentTable)
					{
						String filter = request.getParameter("filter");
						System.out.println("filter = " + filter);
						if(filter!=null)
						{
							filter = ""; //shouldn't be directly used, rather manipulate it.
							searchApproval_path(request, response, isPermanentTable, filter);
						}
						else
						{
							searchApproval_path(request, response, isPermanentTable, "");
						}
					}					
					else
					{
						//searchApproval_path(request, response, tempTableName, isPermanentTable);
					}
				}			
			}
			else if(actionType.equals("getApprovalPage"))
			{
				System.out.println("Approval_path getApprovalPage requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.APPROVAL_PATH_APPROVE))
				{
					searchApproval_path(request, response, false, "");
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}				
			}
			else if(actionType.equals("viewApprovalNotification"))
			{
				System.out.println("view requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.APPROVAL_PATH_APPROVE))
				{
					commonRequestHandler.viewApprovalNotification(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}				
			}
			else if(actionType.equals("view"))
			{
				System.out.println("view requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.APPROVAL_PATH_SEARCH))
				{
					commonRequestHandler.view(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else
			{
				request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}


	
	

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		// TODO Auto-generated method stub
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		System.out.println("doPost");
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		
		try
		{
			String actionType = request.getParameter("actionType");
			System.out.println("actionType = " + actionType);
			if(actionType.equals("add"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.APPROVAL_PATH_ADD))
				{
					System.out.println("going to  addApproval_path ");
					addApproval_path(request, response, true, userDTO, true);
				}
				else
				{
					System.out.println("Not going to  addApproval_path ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else if(actionType.equals("getPathDetails"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.APPROVAL_PATH_SEARCH))
				{
					getPathDetails(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getAPDetailsPlain"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.APPROVAL_PATH_SEARCH))
				{
					getPathDetailsPlain(request, response, userDTO);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}

			else if(actionType.equals("SendToApprovalPath"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.APPROVAL_PATH_APPROVE))
				{
					commonRequestHandler.sendToApprovalPath(request, response, userDTO);
				}
				else
				{
					System.out.println("Not going to SendToApprovalPath ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}
			else if(actionType.equals("approve"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.APPROVAL_PATH_ADD))
				{					
					commonRequestHandler.approve(request, response, true, userDTO, true);
				}
				else
				{
					System.out.println("Not going to  addApproval_path ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else if(actionType.equals("reject"))
			{
				System.out.println("trying to approve");
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.APPROVAL_PATH_APPROVE))
				{					
					commonRequestHandler.approve(request, response, true, userDTO, false);
				}
				else
				{
					System.out.println("Not going to  addApproval_path ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}				
			}
			else if(actionType.equals("terminate"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.APPROVAL_PATH_ADD))
				{
					commonRequestHandler.terminate(request, response, userDTO);
				}
				else
				{
					System.out.println("Not going to  addApproval_path ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}
			else if(actionType.equals("skipStep"))
			{
				
				System.out.println("skipStep");
				commonRequestHandler.skipStep(request, response, userDTO);									
			}
			else if(actionType.equals("getDTO"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.APPROVAL_PATH_ADD))
				{
					getDTO(request, response);					
				}
				else
				{
					System.out.println("Not going to  addApproval_path ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else if(actionType.equals("edit"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.APPROVAL_PATH_UPDATE))
				{					
					addApproval_path(request, response, false, userDTO, isPermanentTable);					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("delete"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.APPROVAL_PATH_UPDATE))
				{
					commonRequestHandler.delete(request, response, userDTO);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}	
			else if(actionType.equals("search"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.APPROVAL_PATH_SEARCH))
				{
					searchApproval_path(request, response, true, "");
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getGeo"))
			{
				System.out.println("going to geoloc ");
				request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
			}
			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}
	
	private void getPathDetailsPlain(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO) 
	{
		// TODO Auto-generated method stub
		int jobCat = Integer.parseInt(request.getParameter("jobCat"));
		long id = jobCat;
		System.out.println("Got id = " + id);
		request.setAttribute("id", id);
		try 
		{
			RequestDispatcher rd = request.getRequestDispatcher("approval_path/approval_pathDetailsPlain.jsp");
			rd.forward(request, response);
		}
		catch (ServletException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	private void getDTO(HttpServletRequest request, HttpServletResponse response) 
	{
		try 
		{
			System.out.println("In getDTO");
			Approval_pathDTO approval_pathDTO = approval_pathDAO.getDTOByID(Long.parseLong(request.getParameter("ID")));
			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			
			String encoded = this.gson.toJson(approval_pathDTO);
			System.out.println("json encoded data = " + encoded);
			out.print(encoded);
			out.flush();
		}
		catch (NumberFormatException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	private void addApproval_path(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, boolean isPermanentTable) throws IOException 
	{
		// TODO Auto-generated method stub
		try 
		{
			request.setAttribute("failureMessage", "");
			System.out.println("%%%% addApproval_path");
			String path = getServletContext().getRealPath("/img2/");
			Approval_pathDTO approval_pathDTO;
			SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
						
			if(addFlag == true)
			{
				approval_pathDTO = new Approval_pathDTO();
			}
			else
			{
				approval_pathDTO = approval_pathDAO.getDTOByID(Long.parseLong(request.getParameter("iD")));
			}
			
			String Value = "";

			Value = request.getParameter("nameEn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("nameEn = " + Value);
			if(Value != null)
			{
				approval_pathDTO.nameEn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("nameBn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("nameBn = " + Value);
			if(Value != null)
			{
				approval_pathDTO.nameBn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			if(addFlag)
			{
				approval_pathDTO.insertedByUserId = userDTO.ID;
			}


			if(addFlag)
			{
				approval_pathDTO.insertedByOrganogramId = userDTO.organogramID;
			}


			if(addFlag)
			{
				Calendar c = Calendar.getInstance();
				c.set(Calendar.HOUR_OF_DAY, 0);
				c.set(Calendar.MINUTE, 0);
				c.set(Calendar.SECOND, 0);
				c.set(Calendar.MILLISECOND, 0);
				
				approval_pathDTO.insertionDate = c.getTimeInMillis();
			}			


			approval_pathDTO.lastModifierUser = userDTO.userName;


			Value = request.getParameter("officeUnitId");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("officeUnitId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				approval_pathDTO.officeUnitId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}


			Value = request.getParameter("moduleCat");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("moduleCat = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				approval_pathDTO.moduleCat = Integer.parseInt(Value);
				approval_pathDTO.module = CatDAO.getName("english", "module", approval_pathDTO.moduleCat);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("searchColumn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("searchColumn = " + Value);
			if(Value != null)
			{
				approval_pathDTO.searchColumn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			
			if(addFlag)
			{
				approval_pathDTO.isVisible = true;
			}

			
			System.out.println("Done adding  addApproval_path dto = " + approval_pathDTO);
			long returnedID = -1;
			
			if(isPermanentTable == false) //add new row for validation and make the old row outdated
			{
				approval_pathDAO.setIsDeleted(approval_pathDTO.iD, CommonDTO.OUTDATED);
				returnedID = approval_pathDAO.add(approval_pathDTO);
				approval_pathDAO.setIsDeleted(returnedID, CommonDTO.WAITING_FOR_APPROVAL);
			}
			else if(addFlag == true)
			{
				returnedID = approval_pathDAO.manageWriteOperations(approval_pathDTO, SessionConstants.INSERT, -1, userDTO);
			}
			else
			{				
				returnedID = approval_pathDAO.manageWriteOperations(approval_pathDTO, SessionConstants.UPDATE, -1, userDTO);											
			}
			
			
			
			
			
			List<ApprovalPathDetailsDTO> approvalPathDetailsDTOList = createApprovalPathDetailsDTOListByRequest(request);			
			if(addFlag == true || isPermanentTable == false) //add or validate
			{
				if(approvalPathDetailsDTOList != null)
				{				
					for(ApprovalPathDetailsDTO approvalPathDetailsDTO: approvalPathDetailsDTOList)
					{
						approvalPathDetailsDTO.approvalPathId = approval_pathDTO.iD; 
						approvalPathDetailsDAO.add(approvalPathDetailsDTO);
					}
				}
			
			}
			else
			{
				List<Long> childIdsFromRequest = approvalPathDetailsDAO.getChildIdsFromRequest(request, "approvalPathDetails");
				//delete the removed children
				approvalPathDetailsDAO.deleteChildrenNotInList("approval_path", "approval_path_details", approval_pathDTO.iD, childIdsFromRequest);
				List<Long> childIDsInDatabase = approvalPathDetailsDAO.getChilIds("approval_path", "approval_path_details", approval_pathDTO.iD);
				
				
				if(childIdsFromRequest != null)
				{
					for(int i = 0; i < childIdsFromRequest.size(); i ++)
					{
						Long childIDFromRequest = childIdsFromRequest.get(i);
						if(childIDsInDatabase.contains(childIDFromRequest))
						{
							ApprovalPathDetailsDTO approvalPathDetailsDTO =  createApprovalPathDetailsDTOByRequestAndIndex(request, false, i);
							approvalPathDetailsDTO.approvalPathId = approval_pathDTO.iD; 
							approvalPathDetailsDAO.update(approvalPathDetailsDTO);
						}
						else
						{
							ApprovalPathDetailsDTO approvalPathDetailsDTO =  createApprovalPathDetailsDTOByRequestAndIndex(request, true, i);
							approvalPathDetailsDTO.approvalPathId = approval_pathDTO.iD; 
							approvalPathDetailsDAO.add(approvalPathDetailsDTO);
						}
					}
				}
				else
				{
					approvalPathDetailsDAO.deleteChildrenByParent(approval_pathDTO.iD, "approval_path_id");
				}
				
			}
			
			List<ApprovalNotificationRecipientsDTO> approvalNotificationRecipientsDTOList = createApprovalNotificationRecipientsDTOListByRequest(request);			
			if(addFlag == true || isPermanentTable == false) //add or validate
			{
				if(approvalNotificationRecipientsDTOList != null)
				{				
					for(ApprovalNotificationRecipientsDTO approvalNotificationRecipientsDTO: approvalNotificationRecipientsDTOList)
					{
						approvalNotificationRecipientsDTO.approvalPathId = approval_pathDTO.iD; 
						approvalNotificationRecipientsDAO.add(approvalNotificationRecipientsDTO);
					}
				}
			
			}
			else
			{
				List<Long> childIdsFromRequest = approvalNotificationRecipientsDAO.getChildIdsFromRequest(request, "approvalNotificationRecipients");
				//delete the removed children
				approvalNotificationRecipientsDAO.deleteChildrenNotInList("approval_path", "approval_notification_recipients", approval_pathDTO.iD, childIdsFromRequest);
				List<Long> childIDsInDatabase = approvalNotificationRecipientsDAO.getChilIds("approval_path", "approval_notification_recipients", approval_pathDTO.iD);
				
				
				if(childIdsFromRequest != null)
				{
					for(int i = 0; i < childIdsFromRequest.size(); i ++)
					{
						Long childIDFromRequest = childIdsFromRequest.get(i);
						if(childIDsInDatabase.contains(childIDFromRequest))
						{
							ApprovalNotificationRecipientsDTO approvalNotificationRecipientsDTO =  createApprovalNotificationRecipientsDTOByRequestAndIndex(request, false, i);
							approvalNotificationRecipientsDTO.approvalPathId = approval_pathDTO.iD; 
							approvalNotificationRecipientsDAO.update(approvalNotificationRecipientsDTO);
						}
						else
						{
							ApprovalNotificationRecipientsDTO approvalNotificationRecipientsDTO =  createApprovalNotificationRecipientsDTOByRequestAndIndex(request, true, i);
							approvalNotificationRecipientsDTO.approvalPathId = approval_pathDTO.iD; 
							approvalNotificationRecipientsDAO.add(approvalNotificationRecipientsDTO);
						}
					}
				}
				else
				{
					approvalNotificationRecipientsDAO.deleteChildrenByParent(approval_pathDTO.iD, "approval_path_id");
				}
				
			}
			
			
			
			
			
			if(isPermanentTable)
			{
				String inPlaceSubmit = request.getParameter("inplacesubmit");
				
				if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
				{
					getApproval_path(request, response, returnedID);
				}
				else
				{
					response.sendRedirect("Approval_pathServlet?actionType=search");
				}
			}
			else
			{
				commonRequestHandler.validate(approval_pathDAO.getDTOByID(returnedID), request, response, userDTO);
			}
					
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	



	private List<ApprovalPathDetailsDTO> createApprovalPathDetailsDTOListByRequest(HttpServletRequest request) throws Exception{ 
		List<ApprovalPathDetailsDTO> approvalPathDetailsDTOList = new ArrayList<ApprovalPathDetailsDTO>();
		if(request.getParameterValues("approvalPathDetails.iD") != null) 
		{
			int approvalPathDetailsItemNo = request.getParameterValues("approvalPathDetails.iD").length;
			
			
			for(int index=0;index<approvalPathDetailsItemNo;index++){
				ApprovalPathDetailsDTO approvalPathDetailsDTO = createApprovalPathDetailsDTOByRequestAndIndex(request,true,index);
				approvalPathDetailsDTOList.add(approvalPathDetailsDTO);
			}
			
			return approvalPathDetailsDTOList;
		}
		return null;
	}
	private List<ApprovalNotificationRecipientsDTO> createApprovalNotificationRecipientsDTOListByRequest(HttpServletRequest request) throws Exception{ 
		List<ApprovalNotificationRecipientsDTO> approvalNotificationRecipientsDTOList = new ArrayList<ApprovalNotificationRecipientsDTO>();
		if(request.getParameterValues("approvalNotificationRecipients.iD") != null) 
		{
			int approvalNotificationRecipientsItemNo = request.getParameterValues("approvalNotificationRecipients.iD").length;
			
			
			for(int index=0;index<approvalNotificationRecipientsItemNo;index++){
				ApprovalNotificationRecipientsDTO approvalNotificationRecipientsDTO = createApprovalNotificationRecipientsDTOByRequestAndIndex(request,true,index);
				approvalNotificationRecipientsDTOList.add(approvalNotificationRecipientsDTO);
			}
			
			return approvalNotificationRecipientsDTOList;
		}
		return null;
	}
	
	private void getPathDetails(HttpServletRequest request, HttpServletResponse response) 
	{
		long approvalPathID = Long.parseLong(request.getParameter("ApprovalPathID"));
		String language = request.getParameter("Language");
		ApprovalPathDetailsDAO approval_path_detailsDAO = new ApprovalPathDetailsDAO();
		
		
		RequestDispatcher rd = request.getRequestDispatcher("approval_path/approval_pathDetails.jsp");
		try {
			System.out.println("approvalPathID = " + approvalPathID);
			List<ApprovalPathDetailsDTO> approval_path_detailsDTOList = approval_path_detailsDAO.getApprovalPathDetailsDTOListByApprovalPathID(approvalPathID);
			request.setAttribute("approval_path_detailsDTOList", approval_path_detailsDTOList);
			long officeID = Long.parseLong(request.getParameter("OfficeID"));
			request.setAttribute("officeID", officeID);
			rd.forward(request, response);
		} catch (ServletException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	
	
	
	private ApprovalPathDetailsDTO createApprovalPathDetailsDTOByRequestAndIndex(HttpServletRequest request, boolean addFlag,int index) throws Exception{
	
		ApprovalPathDetailsDTO approvalPathDetailsDTO;
		if(addFlag == true )
		{
			approvalPathDetailsDTO = new ApprovalPathDetailsDTO();
		}
		else
		{
			approvalPathDetailsDTO = approvalPathDetailsDAO.getDTOByID(Long.parseLong(request.getParameterValues("approvalPathDetails.iD")[index]));
		}
		String path = getServletContext().getRealPath("/img2/");
		SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
		
		
				
		String Value = "";
		Value = request.getParameterValues("approvalPathDetails.approvalPathId")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		approvalPathDetailsDTO.approvalPathId = Long.parseLong(Value);
		Value = request.getParameterValues("approvalPathDetails.approvalRoleCat")[index];

		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
			approvalPathDetailsDTO.approvalRoleCat = Integer.parseInt(Value);
		}

		
		Value = request.getParameterValues("approvalPathDetails.approvalOrder")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		approvalPathDetailsDTO.approvalOrder = Integer.parseInt(Value);
		Value = request.getParameterValues("approvalPathDetails.organogramId")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		approvalPathDetailsDTO.organogramId = Long.parseLong(Value);
		Value = request.getParameterValues("approvalPathDetails.daysRequired")[index];

		

		approvalPathDetailsDTO.permanentOrganogramId = approvalPathDetailsDTO.organogramId;
		return approvalPathDetailsDTO;
	
	}
	
	
	private ApprovalNotificationRecipientsDTO createApprovalNotificationRecipientsDTOByRequestAndIndex(HttpServletRequest request, boolean addFlag,int index) throws Exception{
	
		ApprovalNotificationRecipientsDTO approvalNotificationRecipientsDTO;
		if(addFlag == true )
		{
			approvalNotificationRecipientsDTO = new ApprovalNotificationRecipientsDTO();
		}
		else
		{
			approvalNotificationRecipientsDTO = approvalNotificationRecipientsDAO.getDTOByID(Long.parseLong(request.getParameterValues("approvalNotificationRecipients.iD")[index]));
		}
		String path = getServletContext().getRealPath("/img2/");
		SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
		
		
				
		String Value = "";
		Value = request.getParameterValues("approvalNotificationRecipients.approvalPathId")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		approvalNotificationRecipientsDTO.approvalPathId = Long.parseLong(Value);
		Value = request.getParameterValues("approvalNotificationRecipients.organogramId")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}

		approvalNotificationRecipientsDTO.organogramId = Long.parseLong(Value);
		return approvalNotificationRecipientsDTO;
	
	}
	
	
	

	private void getApproval_path(HttpServletRequest request, HttpServletResponse response, long id) throws ServletException, IOException
	{
		System.out.println("in getApproval_path");
		Approval_pathDTO approval_pathDTO = null;
		try 
		{
			approval_pathDTO = approval_pathDAO.getDTOByID(id);
			boolean isPermanentTable = true;
			if(request.getParameter("isPermanentTable") != null)
			{
				isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
			}
			request.setAttribute("ID", approval_pathDTO.iD);
			request.setAttribute("approval_pathDTO",approval_pathDTO);
			request.setAttribute("approval_pathDAO",approval_pathDAO);
			
			String URL= "";
			
			String inPlaceEdit = request.getParameter("inplaceedit");
			String inPlaceSubmit = request.getParameter("inplacesubmit");
			String getBodyOnly = request.getParameter("getBodyOnly");
			
			if(inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase(""))
			{
				URL = "approval_path/approval_pathInPlaceEdit.jsp";	
				request.setAttribute("inplaceedit","");				
			}
			else if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
			{
				URL = "approval_path/approval_pathSearchRow.jsp";
				request.setAttribute("inplacesubmit","");					
			}
			else
			{
				if(getBodyOnly  != null && !getBodyOnly.equalsIgnoreCase(""))
				{
					URL = "approval_path/approval_pathEditBody.jsp?actionType=edit";
				}
				else
				{
					URL = "approval_path/approval_pathEdit.jsp?actionType=edit";
				}				
			}
			
			RequestDispatcher rd = request.getRequestDispatcher(URL);
			rd.forward(request, response);
		}
		catch (NumberFormatException e) 
		{
			e.printStackTrace();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	
	private void getApproval_path(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		getApproval_path(request, response, Long.parseLong(request.getParameter("ID")));	
	}
	
	private void searchApproval_path(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException
	{
		System.out.println("in  searchApproval_path 1");
		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String ajax = request.getParameter("ajax");
		boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);
		
        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
			SessionConstants.NAV_APPROVAL_PATH,
			request,
			approval_pathDAO,
			SessionConstants.VIEW_APPROVAL_PATH,
			SessionConstants.SEARCH_APPROVAL_PATH,
			tableName,
			isPermanent,
			userDTO,
			filter,
			true);
        try
        {
			System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        }
        catch(Exception e)
        {
			System.out.println("failed to dojob" + e);
        }

		request.setAttribute("approval_pathDAO",approval_pathDAO);
        RequestDispatcher rd;
        if(!isPermanent)
        {
        	if(hasAjax == false)
	        {
	        	System.out.println("Going to approval_path/approval_pathApproval.jsp");
	        	rd = request.getRequestDispatcher("approval_path/approval_pathApproval.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to approval_path/approval_pathApprovalForm.jsp");
	        	rd = request.getRequestDispatcher("approval_path/approval_pathApprovalForm.jsp");
	        }
        }
        else
        {
	        if(hasAjax == false)
	        {
	        	System.out.println("Going to approval_path/approval_pathSearch.jsp");
	        	rd = request.getRequestDispatcher("approval_path/approval_pathSearch.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to approval_path/approval_pathSearchForm.jsp");
	        	rd = request.getRequestDispatcher("approval_path/approval_pathSearchForm.jsp");
	        }
        }
		rd.forward(request, response);
	}
	
}

