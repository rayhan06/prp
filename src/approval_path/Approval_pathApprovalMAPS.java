package approval_path;
import util.*;

public class Approval_pathApprovalMAPS extends CommonMaps
{	
	public Approval_pathApprovalMAPS(String tableName)
	{
		
		java_allfield_type_map.put("name_en".toLowerCase(), "String");
		java_allfield_type_map.put("name_bn".toLowerCase(), "String");
		java_allfield_type_map.put("inserted_by_user_id".toLowerCase(), "Long");
		java_allfield_type_map.put("inserted_by_organogram_id".toLowerCase(), "Long");
		java_allfield_type_map.put("insertion_date".toLowerCase(), "Long");
		java_allfield_type_map.put("last_modifier_user".toLowerCase(), "String");
		java_allfield_type_map.put("office_unit_id".toLowerCase(), "Long");
		java_allfield_type_map.put("module".toLowerCase(), "String");
		java_allfield_type_map.put("module_cat".toLowerCase(), "Integer");
		java_allfield_type_map.put("isVisible".toLowerCase(), "Integer");

		java_allfield_type_map.put("job_cat", "Integer");
		java_allfield_type_map.put("approval_status_cat", "Integer");
		java_allfield_type_map.put("initiator", "Long");
		java_allfield_type_map.put("assigned_to", "Long");
		java_allfield_type_map.put("starting_date", "Long");
		java_allfield_type_map.put("ending_date", "Long");
		
		java_table_map.put("approval_status_cat", "approval_summary");
		java_table_map.put("initiator", "approval_summary");
		java_table_map.put("assigned_to", "approval_summary");
	}

}