package approval_path;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class Approval_pathRepository implements Repository {
	Approval_pathDAO approval_pathDAO = null;
	
	public void setDAO(Approval_pathDAO approval_pathDAO)
	{
		this.approval_pathDAO = approval_pathDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Approval_pathRepository.class);
	Map<Long, Approval_pathDTO>mapOfApproval_pathDTOToiD;
	Map<String, Set<Approval_pathDTO> >mapOfApproval_pathDTOTonameEn;
	Map<String, Set<Approval_pathDTO> >mapOfApproval_pathDTOTonameBn;
	Map<Long, Set<Approval_pathDTO> >mapOfApproval_pathDTOToinsertedByUserId;
	Map<Long, Set<Approval_pathDTO> >mapOfApproval_pathDTOToinsertedByOrganogramId;
	Map<Long, Set<Approval_pathDTO> >mapOfApproval_pathDTOToinsertionDate;
	Map<String, Set<Approval_pathDTO> >mapOfApproval_pathDTOTolastModifierUser;
	Map<Integer, Set<Approval_pathDTO> >mapOfApproval_pathDTOTojobCat;
	Map<Long, Set<Approval_pathDTO> >mapOfApproval_pathDTOToofficeUnitId;
	Map<String, Set<Approval_pathDTO> >mapOfApproval_pathDTOTomodule;
	Map<Integer, Set<Approval_pathDTO> >mapOfApproval_pathDTOTomoduleCat;
	Map<String, Set<Approval_pathDTO> >mapOfApproval_pathDTOTosearchColumn;
	Map<Integer, Set<Approval_pathDTO> >mapOfApproval_pathDTOToisVisible;
	Map<Long, Set<Approval_pathDTO> >mapOfApproval_pathDTOTolastModificationTime;


	static Approval_pathRepository instance = null;  
	private Approval_pathRepository(){
		mapOfApproval_pathDTOToiD = new ConcurrentHashMap<>();
		mapOfApproval_pathDTOTonameEn = new ConcurrentHashMap<>();
		mapOfApproval_pathDTOTonameBn = new ConcurrentHashMap<>();
		mapOfApproval_pathDTOToinsertedByUserId = new ConcurrentHashMap<>();
		mapOfApproval_pathDTOToinsertedByOrganogramId = new ConcurrentHashMap<>();
		mapOfApproval_pathDTOToinsertionDate = new ConcurrentHashMap<>();
		mapOfApproval_pathDTOTolastModifierUser = new ConcurrentHashMap<>();
		mapOfApproval_pathDTOTojobCat = new ConcurrentHashMap<>();
		mapOfApproval_pathDTOToofficeUnitId = new ConcurrentHashMap<>();
		mapOfApproval_pathDTOTomodule = new ConcurrentHashMap<>();
		mapOfApproval_pathDTOTomoduleCat = new ConcurrentHashMap<>();
		mapOfApproval_pathDTOTosearchColumn = new ConcurrentHashMap<>();
		mapOfApproval_pathDTOToisVisible = new ConcurrentHashMap<>();
		mapOfApproval_pathDTOTolastModificationTime = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Approval_pathRepository getInstance(){
		if (instance == null){
			instance = new Approval_pathRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(approval_pathDAO == null)
		{
			return;
		}
		try {
			List<Approval_pathDTO> approval_pathDTOs = approval_pathDAO.getAllApproval_path(reloadAll);
			for(Approval_pathDTO approval_pathDTO : approval_pathDTOs) {
				Approval_pathDTO oldApproval_pathDTO = getApproval_pathDTOByID(approval_pathDTO.iD);
				if( oldApproval_pathDTO != null ) {
					mapOfApproval_pathDTOToiD.remove(oldApproval_pathDTO.iD);
				
					if(mapOfApproval_pathDTOTonameEn.containsKey(oldApproval_pathDTO.nameEn)) {
						mapOfApproval_pathDTOTonameEn.get(oldApproval_pathDTO.nameEn).remove(oldApproval_pathDTO);
					}
					if(mapOfApproval_pathDTOTonameEn.get(oldApproval_pathDTO.nameEn).isEmpty()) {
						mapOfApproval_pathDTOTonameEn.remove(oldApproval_pathDTO.nameEn);
					}
					
					if(mapOfApproval_pathDTOTonameBn.containsKey(oldApproval_pathDTO.nameBn)) {
						mapOfApproval_pathDTOTonameBn.get(oldApproval_pathDTO.nameBn).remove(oldApproval_pathDTO);
					}
					if(mapOfApproval_pathDTOTonameBn.get(oldApproval_pathDTO.nameBn).isEmpty()) {
						mapOfApproval_pathDTOTonameBn.remove(oldApproval_pathDTO.nameBn);
					}
					
					if(mapOfApproval_pathDTOToinsertedByUserId.containsKey(oldApproval_pathDTO.insertedByUserId)) {
						mapOfApproval_pathDTOToinsertedByUserId.get(oldApproval_pathDTO.insertedByUserId).remove(oldApproval_pathDTO);
					}
					if(mapOfApproval_pathDTOToinsertedByUserId.get(oldApproval_pathDTO.insertedByUserId).isEmpty()) {
						mapOfApproval_pathDTOToinsertedByUserId.remove(oldApproval_pathDTO.insertedByUserId);
					}
					
					if(mapOfApproval_pathDTOToinsertedByOrganogramId.containsKey(oldApproval_pathDTO.insertedByOrganogramId)) {
						mapOfApproval_pathDTOToinsertedByOrganogramId.get(oldApproval_pathDTO.insertedByOrganogramId).remove(oldApproval_pathDTO);
					}
					if(mapOfApproval_pathDTOToinsertedByOrganogramId.get(oldApproval_pathDTO.insertedByOrganogramId).isEmpty()) {
						mapOfApproval_pathDTOToinsertedByOrganogramId.remove(oldApproval_pathDTO.insertedByOrganogramId);
					}
					
					if(mapOfApproval_pathDTOToinsertionDate.containsKey(oldApproval_pathDTO.insertionDate)) {
						mapOfApproval_pathDTOToinsertionDate.get(oldApproval_pathDTO.insertionDate).remove(oldApproval_pathDTO);
					}
					if(mapOfApproval_pathDTOToinsertionDate.get(oldApproval_pathDTO.insertionDate).isEmpty()) {
						mapOfApproval_pathDTOToinsertionDate.remove(oldApproval_pathDTO.insertionDate);
					}
					
					if(mapOfApproval_pathDTOTolastModifierUser.containsKey(oldApproval_pathDTO.lastModifierUser)) {
						mapOfApproval_pathDTOTolastModifierUser.get(oldApproval_pathDTO.lastModifierUser).remove(oldApproval_pathDTO);
					}
					if(mapOfApproval_pathDTOTolastModifierUser.get(oldApproval_pathDTO.lastModifierUser).isEmpty()) {
						mapOfApproval_pathDTOTolastModifierUser.remove(oldApproval_pathDTO.lastModifierUser);
					}
					
					if(mapOfApproval_pathDTOTojobCat.containsKey(oldApproval_pathDTO.jobCat)) {
						mapOfApproval_pathDTOTojobCat.get(oldApproval_pathDTO.jobCat).remove(oldApproval_pathDTO);
					}
					if(mapOfApproval_pathDTOTojobCat.get(oldApproval_pathDTO.jobCat).isEmpty()) {
						mapOfApproval_pathDTOTojobCat.remove(oldApproval_pathDTO.jobCat);
					}
					
					if(mapOfApproval_pathDTOToofficeUnitId.containsKey(oldApproval_pathDTO.officeUnitId)) {
						mapOfApproval_pathDTOToofficeUnitId.get(oldApproval_pathDTO.officeUnitId).remove(oldApproval_pathDTO);
					}
					if(mapOfApproval_pathDTOToofficeUnitId.get(oldApproval_pathDTO.officeUnitId).isEmpty()) {
						mapOfApproval_pathDTOToofficeUnitId.remove(oldApproval_pathDTO.officeUnitId);
					}
					
					if(mapOfApproval_pathDTOTomodule.containsKey(oldApproval_pathDTO.module)) {
						mapOfApproval_pathDTOTomodule.get(oldApproval_pathDTO.module).remove(oldApproval_pathDTO);
					}
					if(mapOfApproval_pathDTOTomodule.get(oldApproval_pathDTO.module).isEmpty()) {
						mapOfApproval_pathDTOTomodule.remove(oldApproval_pathDTO.module);
					}
					
					if(mapOfApproval_pathDTOTomoduleCat.containsKey(oldApproval_pathDTO.moduleCat)) {
						mapOfApproval_pathDTOTomoduleCat.get(oldApproval_pathDTO.moduleCat).remove(oldApproval_pathDTO);
					}
					if(mapOfApproval_pathDTOTomoduleCat.get(oldApproval_pathDTO.moduleCat).isEmpty()) {
						mapOfApproval_pathDTOTomoduleCat.remove(oldApproval_pathDTO.moduleCat);
					}
					
					if(mapOfApproval_pathDTOTosearchColumn.containsKey(oldApproval_pathDTO.searchColumn)) {
						mapOfApproval_pathDTOTosearchColumn.get(oldApproval_pathDTO.searchColumn).remove(oldApproval_pathDTO);
					}
					if(mapOfApproval_pathDTOTosearchColumn.get(oldApproval_pathDTO.searchColumn).isEmpty()) {
						mapOfApproval_pathDTOTosearchColumn.remove(oldApproval_pathDTO.searchColumn);
					}
					
					if(mapOfApproval_pathDTOToisVisible.containsKey(oldApproval_pathDTO.isVisible)) {
						mapOfApproval_pathDTOToisVisible.get(oldApproval_pathDTO.isVisible).remove(oldApproval_pathDTO);
					}
					if(mapOfApproval_pathDTOToisVisible.get(oldApproval_pathDTO.isVisible).isEmpty()) {
						mapOfApproval_pathDTOToisVisible.remove(oldApproval_pathDTO.isVisible);
					}
					
					if(mapOfApproval_pathDTOTolastModificationTime.containsKey(oldApproval_pathDTO.lastModificationTime)) {
						mapOfApproval_pathDTOTolastModificationTime.get(oldApproval_pathDTO.lastModificationTime).remove(oldApproval_pathDTO);
					}
					if(mapOfApproval_pathDTOTolastModificationTime.get(oldApproval_pathDTO.lastModificationTime).isEmpty()) {
						mapOfApproval_pathDTOTolastModificationTime.remove(oldApproval_pathDTO.lastModificationTime);
					}
					
					
				}
				if(approval_pathDTO.isDeleted == 0) 
				{
					
					mapOfApproval_pathDTOToiD.put(approval_pathDTO.iD, approval_pathDTO);
				
					if( ! mapOfApproval_pathDTOTonameEn.containsKey(approval_pathDTO.nameEn)) {
						mapOfApproval_pathDTOTonameEn.put(approval_pathDTO.nameEn, new HashSet<>());
					}
					mapOfApproval_pathDTOTonameEn.get(approval_pathDTO.nameEn).add(approval_pathDTO);
					
					if( ! mapOfApproval_pathDTOTonameBn.containsKey(approval_pathDTO.nameBn)) {
						mapOfApproval_pathDTOTonameBn.put(approval_pathDTO.nameBn, new HashSet<>());
					}
					mapOfApproval_pathDTOTonameBn.get(approval_pathDTO.nameBn).add(approval_pathDTO);
					
					if( ! mapOfApproval_pathDTOToinsertedByUserId.containsKey(approval_pathDTO.insertedByUserId)) {
						mapOfApproval_pathDTOToinsertedByUserId.put(approval_pathDTO.insertedByUserId, new HashSet<>());
					}
					mapOfApproval_pathDTOToinsertedByUserId.get(approval_pathDTO.insertedByUserId).add(approval_pathDTO);
					
					if( ! mapOfApproval_pathDTOToinsertedByOrganogramId.containsKey(approval_pathDTO.insertedByOrganogramId)) {
						mapOfApproval_pathDTOToinsertedByOrganogramId.put(approval_pathDTO.insertedByOrganogramId, new HashSet<>());
					}
					mapOfApproval_pathDTOToinsertedByOrganogramId.get(approval_pathDTO.insertedByOrganogramId).add(approval_pathDTO);
					
					if( ! mapOfApproval_pathDTOToinsertionDate.containsKey(approval_pathDTO.insertionDate)) {
						mapOfApproval_pathDTOToinsertionDate.put(approval_pathDTO.insertionDate, new HashSet<>());
					}
					mapOfApproval_pathDTOToinsertionDate.get(approval_pathDTO.insertionDate).add(approval_pathDTO);
					
					if( ! mapOfApproval_pathDTOTolastModifierUser.containsKey(approval_pathDTO.lastModifierUser)) {
						mapOfApproval_pathDTOTolastModifierUser.put(approval_pathDTO.lastModifierUser, new HashSet<>());
					}
					mapOfApproval_pathDTOTolastModifierUser.get(approval_pathDTO.lastModifierUser).add(approval_pathDTO);
					
					if( ! mapOfApproval_pathDTOTojobCat.containsKey(approval_pathDTO.jobCat)) {
						mapOfApproval_pathDTOTojobCat.put(approval_pathDTO.jobCat, new HashSet<>());
					}
					mapOfApproval_pathDTOTojobCat.get(approval_pathDTO.jobCat).add(approval_pathDTO);
					
					if( ! mapOfApproval_pathDTOToofficeUnitId.containsKey(approval_pathDTO.officeUnitId)) {
						mapOfApproval_pathDTOToofficeUnitId.put(approval_pathDTO.officeUnitId, new HashSet<>());
					}
					mapOfApproval_pathDTOToofficeUnitId.get(approval_pathDTO.officeUnitId).add(approval_pathDTO);
					
					if( ! mapOfApproval_pathDTOTomodule.containsKey(approval_pathDTO.module)) {
						mapOfApproval_pathDTOTomodule.put(approval_pathDTO.module, new HashSet<>());
					}
					mapOfApproval_pathDTOTomodule.get(approval_pathDTO.module).add(approval_pathDTO);
					
					if( ! mapOfApproval_pathDTOTomoduleCat.containsKey(approval_pathDTO.moduleCat)) {
						mapOfApproval_pathDTOTomoduleCat.put(approval_pathDTO.moduleCat, new HashSet<>());
					}
					mapOfApproval_pathDTOTomoduleCat.get(approval_pathDTO.moduleCat).add(approval_pathDTO);
					
					if( ! mapOfApproval_pathDTOTosearchColumn.containsKey(approval_pathDTO.searchColumn)) {
						mapOfApproval_pathDTOTosearchColumn.put(approval_pathDTO.searchColumn, new HashSet<>());
					}
					mapOfApproval_pathDTOTosearchColumn.get(approval_pathDTO.searchColumn).add(approval_pathDTO);
					
				
					
					if( ! mapOfApproval_pathDTOTolastModificationTime.containsKey(approval_pathDTO.lastModificationTime)) {
						mapOfApproval_pathDTOTolastModificationTime.put(approval_pathDTO.lastModificationTime, new HashSet<>());
					}
					mapOfApproval_pathDTOTolastModificationTime.get(approval_pathDTO.lastModificationTime).add(approval_pathDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Approval_pathDTO> getApproval_pathList() {
		List <Approval_pathDTO> approval_paths = new ArrayList<Approval_pathDTO>(this.mapOfApproval_pathDTOToiD.values());
		return approval_paths;
	}
	
	
	public Approval_pathDTO getApproval_pathDTOByID( long ID){
		return mapOfApproval_pathDTOToiD.get(ID);
	}
	
	
	public List<Approval_pathDTO> getApproval_pathDTOByname_en(String name_en) {
		return new ArrayList<>( mapOfApproval_pathDTOTonameEn.getOrDefault(name_en,new HashSet<>()));
	}
	
	
	public List<Approval_pathDTO> getApproval_pathDTOByname_bn(String name_bn) {
		return new ArrayList<>( mapOfApproval_pathDTOTonameBn.getOrDefault(name_bn,new HashSet<>()));
	}
	
	
	public List<Approval_pathDTO> getApproval_pathDTOByinserted_by_user_id(long inserted_by_user_id) {
		return new ArrayList<>( mapOfApproval_pathDTOToinsertedByUserId.getOrDefault(inserted_by_user_id,new HashSet<>()));
	}
	
	
	public List<Approval_pathDTO> getApproval_pathDTOByinserted_by_organogram_id(long inserted_by_organogram_id) {
		return new ArrayList<>( mapOfApproval_pathDTOToinsertedByOrganogramId.getOrDefault(inserted_by_organogram_id,new HashSet<>()));
	}
	
	
	public List<Approval_pathDTO> getApproval_pathDTOByinsertion_date(long insertion_date) {
		return new ArrayList<>( mapOfApproval_pathDTOToinsertionDate.getOrDefault(insertion_date,new HashSet<>()));
	}
	
	
	public List<Approval_pathDTO> getApproval_pathDTOBylast_modifier_user(String last_modifier_user) {
		return new ArrayList<>( mapOfApproval_pathDTOTolastModifierUser.getOrDefault(last_modifier_user,new HashSet<>()));
	}
	
	
	public List<Approval_pathDTO> getApproval_pathDTOByjob_cat(int job_cat) {
		return new ArrayList<>( mapOfApproval_pathDTOTojobCat.getOrDefault(job_cat,new HashSet<>()));
	}
	
	
	public List<Approval_pathDTO> getApproval_pathDTOByoffice_unit_id(long office_unit_id) {
		return new ArrayList<>( mapOfApproval_pathDTOToofficeUnitId.getOrDefault(office_unit_id,new HashSet<>()));
	}
	
	
	public List<Approval_pathDTO> getApproval_pathDTOBymodule(String module) {
		return new ArrayList<>( mapOfApproval_pathDTOTomodule.getOrDefault(module,new HashSet<>()));
	}
	
	
	public List<Approval_pathDTO> getApproval_pathDTOBymodule_cat(int module_cat) {
		return new ArrayList<>( mapOfApproval_pathDTOTomoduleCat.getOrDefault(module_cat,new HashSet<>()));
	}
	
	
	public List<Approval_pathDTO> getApproval_pathDTOBysearch_column(String search_column) {
		return new ArrayList<>( mapOfApproval_pathDTOTosearchColumn.getOrDefault(search_column,new HashSet<>()));
	}
	
	
	public List<Approval_pathDTO> getApproval_pathDTOByisVisible(int isVisible) {
		return new ArrayList<>( mapOfApproval_pathDTOToisVisible.getOrDefault(isVisible,new HashSet<>()));
	}
	
	
	public List<Approval_pathDTO> getApproval_pathDTOBylastModificationTime(long lastModificationTime) {
		return new ArrayList<>( mapOfApproval_pathDTOTolastModificationTime.getOrDefault(lastModificationTime,new HashSet<>()));
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "approval_path";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


