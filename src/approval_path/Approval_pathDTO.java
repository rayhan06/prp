package approval_path;
import java.util.*; 
import util.*; 


public class Approval_pathDTO extends CommonDTO
{

    public String nameEn = "";
    public String nameBn = "";
	public long insertedByUserId = -1;
	public long insertedByOrganogramId = -1;
	public long insertionDate = -1;
    public String lastModifierUser = "";
	public long officeUnitId = -1;
    public String module = "";
	public int moduleCat = -1;
	public boolean isVisible = true;
	
	public List<ApprovalPathDetailsDTO> approvalPathDetailsDTOList = new ArrayList<>();
	public List<ApprovalNotificationRecipientsDTO> approvalNotificationRecipientsDTOList = new ArrayList<>();
	
    @Override
	public String toString() {
            return "$Approval_pathDTO[" +
            " iD = " + iD +
            " nameEn = " + nameEn +
            " nameBn = " + nameBn +
            " insertedByUserId = " + insertedByUserId +
            " insertedByOrganogramId = " + insertedByOrganogramId +
            " insertionDate = " + insertionDate +
            " lastModifierUser = " + lastModifierUser +
            " jobCat = " + jobCat +
            " officeUnitId = " + officeUnitId +
            " module = " + module +
            " moduleCat = " + moduleCat +
            " searchColumn = " + searchColumn +
            " isVisible = " + isVisible +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}