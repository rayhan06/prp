package approval_path;
import java.util.*; 
import util.*; 


public class ApprovalNotificationRecipientsDTO extends CommonDTO
{

	public long approvalPathId = -1;
	public long organogramId = -1;
	
	public List<ApprovalPathDetailsDTO> approvalPathDetailsDTOList = new ArrayList<>();
	public List<ApprovalNotificationRecipientsDTO> approvalNotificationRecipientsDTOList = new ArrayList<>();
	
    @Override
	public String toString() {
            return "$ApprovalNotificationRecipientsDTO[" +
            " iD = " + iD +
            " approvalPathId = " + approvalPathId +
            " organogramId = " + organogramId +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}