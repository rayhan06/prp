package approval_path;
import java.util.*; 
import util.*;


public class ApprovalNotificationRecipientsMAPS extends CommonMaps
{	
	public ApprovalNotificationRecipientsMAPS(String tableName)
	{
		

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("approvalPathId".toLowerCase(), "approvalPathId".toLowerCase());
		java_DTO_map.put("organogramId".toLowerCase(), "organogramId".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("approval_path_id".toLowerCase(), "approvalPathId".toLowerCase());
		java_SQL_map.put("organogram_id".toLowerCase(), "organogramId".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Approval Path Id".toLowerCase(), "approvalPathId".toLowerCase());
		java_Text_map.put("Organogram Id".toLowerCase(), "organogramId".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}