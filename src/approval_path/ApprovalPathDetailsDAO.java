package approval_path;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;
import sessionmanager.SessionConstants;
import java.sql.SQLException;
import common.ConnectionAndStatementUtil;


import org.apache.log4j.Logger;

import dbm.*;

import repository.RepositoryManager;

import util.*;
import pb.*;
import user.UserDTO;

public class ApprovalPathDetailsDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());

	public ApprovalPathDetailsDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";
		commonMaps = new ApprovalPathDetailsMAPS(tableName);
		columnNames = new String[] 
		{
			"ID",
			"approval_path_id",
			"approval_role_cat",
			"approval_order",
			"organogram_id",
			"days_required",
			"permanent_organogram_id",
			"isDeleted",
			"lastModificationTime"
		};
	}
	
	public ApprovalPathDetailsDAO()
	{
		this("approval_path_details");		
	}
	
	public void setSearchColumn(ApprovalPathDetailsDTO approvalpathdetailsDTO)
	{
		approvalpathdetailsDTO.searchColumn = "";
		approvalpathdetailsDTO.searchColumn += CatDAO.getName("English", "approval_role", approvalpathdetailsDTO.approvalRoleCat) + " " + CatDAO.getName("Bangla", "approval_role", approvalpathdetailsDTO.approvalRoleCat) + " ";
	}
	
	public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException
	{
		ApprovalPathDetailsDTO approvalpathdetailsDTO = (ApprovalPathDetailsDTO)commonDTO;
		int index = 1;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(approvalpathdetailsDTO);
		if(isInsert)
		{
			ps.setObject(index++,approvalpathdetailsDTO.iD);
		}
		ps.setObject(index++,approvalpathdetailsDTO.approvalPathId);
		ps.setObject(index++,approvalpathdetailsDTO.approvalRoleCat);
		ps.setObject(index++,approvalpathdetailsDTO.approvalOrder);
		ps.setObject(index++,approvalpathdetailsDTO.organogramId);
		ps.setObject(index++,approvalpathdetailsDTO.daysRequired);
		ps.setObject(index++,approvalpathdetailsDTO.permanentOrganogramId);
		if(isInsert)
		{
			ps.setObject(index++, 0);
			ps.setObject(index++, lastModificationTime);
		}
	}
	
	public ApprovalPathDetailsDTO build(ResultSet rs)
	{
		try
		{
			ApprovalPathDetailsDTO approvalpathdetailsDTO = new ApprovalPathDetailsDTO();
			approvalpathdetailsDTO.iD = rs.getLong("ID");
			approvalpathdetailsDTO.approvalPathId = rs.getLong("approval_path_id");
			approvalpathdetailsDTO.approvalRoleCat = rs.getInt("approval_role_cat");
			approvalpathdetailsDTO.approvalOrder = rs.getInt("approval_order");
			approvalpathdetailsDTO.organogramId = rs.getLong("organogram_id");
			approvalpathdetailsDTO.daysRequired = rs.getInt("days_required");
			approvalpathdetailsDTO.permanentOrganogramId = rs.getLong("permanent_organogram_id");
			approvalpathdetailsDTO.isDeleted = rs.getInt("isDeleted");
			approvalpathdetailsDTO.lastModificationTime = rs.getLong("lastModificationTime");
			return approvalpathdetailsDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
	
	
	public List<ApprovalPathDetailsDTO> getApprovalPathDetailsDTOListByApprovalPathID(long approvalPathID) throws Exception
	{
		String sql = "SELECT * FROM approval_path_details where isDeleted=0 and approval_path_id="+approvalPathID+" order by approval_path_details.lastModificationTime";
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);		
	}
	
	public List<ApprovalPathDetailsDTO> getAllLessThanOrder(long approvalPathID, int order){
		String sql = "SELECT * FROM approval_path_details where isDeleted=0 and"
				+ " approval_path_id=" + approvalPathID + " and approval_order < " + order
				+ " order by approval_order desc";
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);	
	}
	
	
	public ApprovalPathDetailsDTO getApprovalPathDetailsDTOListByApprovalPathIDandApprovalOrder (long approval_path_id, int approval_order)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE isDeleted = 0 and approval_path_id = " 
				+ approval_path_id + " and approval_order = " + approval_order + " limit 1";
		ApprovalPathDetailsDTO approvalpathdetailsDTO = ConnectionAndStatementUtil.getT(sql,this::build);
		return approvalpathdetailsDTO;
	}
	
	public ApprovalPathDetailsDTO getDTOByID (long id)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID = " + id;
		ApprovalPathDetailsDTO approvalpathdetailsDTO = ConnectionAndStatementUtil.getT(sql,this::build);
		return approvalpathdetailsDTO;
	}
	
	 public void replaceForLeaveStarting (long organogramToReplace, long replaceWith) 
	 {
	        long lastModificationTime = System.currentTimeMillis();
	        String sql =  "update approval_path_details set organogram_id = " + replaceWith + " where organogram_id = " + organogramToReplace ;
	        ConnectionAndStatementUtil.getWriteConnectionAndStatement(model -> {
	            Connection connection = model.getConnection();
	            Statement stmt = model.getStatement();
	            try {
	                logger.debug(sql);
	                stmt.execute(sql);
	                recordUpdateTime(connection, null, lastModificationTime);
	            } catch (SQLException ex) {
	                logger.error(ex);
	            }
	        });
	   }
	 public void replaceForLeaveEnding (long permanentOrganogramId) 
	 {
	        long lastModificationTime = System.currentTimeMillis();
	        String sql =  "update approval_path_details set organogram_id = " + permanentOrganogramId + " where permanent_organogram_id = " + permanentOrganogramId ;
	        ConnectionAndStatementUtil.getWriteConnectionAndStatement(model -> {
	            Connection connection = model.getConnection();
	            Statement stmt = model.getStatement();
	            try {
	                logger.debug(sql);
	                stmt.execute(sql);
	                recordUpdateTime(connection, null, lastModificationTime);
	            } catch (SQLException ex) {
	                logger.error(ex);
	            }
	        });
	   }

	
	public List<ApprovalPathDetailsDTO> getDTOs(Collection recordIDs)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID IN ( ";
		for(int i = 0;i<recordIDs.size();i++){
			if(i!=0){
				sql+=",";
			}
			sql+=((ArrayList)recordIDs).get(i);
		}
		sql+=")  order by lastModificationTime desc";
		printSql(sql);
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);		
	}
	
	public List<ApprovalPathDetailsDTO> getAllApprovalPathDetails (boolean isFirstReload)
    {
		String sql = "SELECT * FROM " + tableName + " WHERE ";
		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by " + tableName + ".lastModificationTime desc";
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);	
    }

	
	public List<ApprovalPathDetailsDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO)
	{
		return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
	}

	
	public List<ApprovalPathDetailsDTO> getDTOs(Hashtable  p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
			String filter, boolean tableHasJobCat)
	{
		String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat);
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);			
	}
				
}
	