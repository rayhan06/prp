package approval_path;
import java.util.*; 
import util.*;


public class ApprovalPathDetailsMAPS extends CommonMaps
{	
	public ApprovalPathDetailsMAPS(String tableName)
	{
		

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("approvalPathId".toLowerCase(), "approvalPathId".toLowerCase());
		java_DTO_map.put("approvalRoleCat".toLowerCase(), "approvalRoleCat".toLowerCase());
		java_DTO_map.put("approvalOrder".toLowerCase(), "approvalOrder".toLowerCase());
		java_DTO_map.put("organogramId".toLowerCase(), "organogramId".toLowerCase());
		java_DTO_map.put("daysRequired".toLowerCase(), "daysRequired".toLowerCase());
		java_DTO_map.put("permanentOrganogramId".toLowerCase(), "permanentOrganogramId".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("approval_path_id".toLowerCase(), "approvalPathId".toLowerCase());
		java_SQL_map.put("approval_role_cat".toLowerCase(), "approvalRoleCat".toLowerCase());
		java_SQL_map.put("approval_order".toLowerCase(), "approvalOrder".toLowerCase());
		java_SQL_map.put("organogram_id".toLowerCase(), "organogramId".toLowerCase());
		java_SQL_map.put("days_required".toLowerCase(), "daysRequired".toLowerCase());
		java_SQL_map.put("permanent_organogram_id".toLowerCase(), "permanentOrganogramId".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Approval Path Id".toLowerCase(), "approvalPathId".toLowerCase());
		java_Text_map.put("Approval Role".toLowerCase(), "approvalRoleCat".toLowerCase());
		java_Text_map.put("Approval Order".toLowerCase(), "approvalOrder".toLowerCase());
		java_Text_map.put("Organogram Id".toLowerCase(), "organogramId".toLowerCase());
		java_Text_map.put("Days Required".toLowerCase(), "daysRequired".toLowerCase());
		java_Text_map.put("Permanent Organogram Id".toLowerCase(), "permanentOrganogramId".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}