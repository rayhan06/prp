package approval_path;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;
import sessionmanager.SessionConstants;
import java.sql.SQLException;
import common.ConnectionAndStatementUtil;


import org.apache.log4j.Logger;

import dbm.*;

import repository.RepositoryManager;

import util.*;
import pb.*;
import user.UserDTO;

public class ApprovalNotificationRecipientsDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());

	public ApprovalNotificationRecipientsDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";
		commonMaps = new ApprovalNotificationRecipientsMAPS(tableName);
		columnNames = new String[] 
		{
			"ID",
			"approval_path_id",
			"organogram_id",
			"isDeleted",
			"lastModificationTime"
		};
	}
	
	public ApprovalNotificationRecipientsDAO()
	{
		this("approval_notification_recipients");		
	}
	
	public void setSearchColumn(ApprovalNotificationRecipientsDTO approvalnotificationrecipientsDTO)
	{
		approvalnotificationrecipientsDTO.searchColumn = "";
	}
	
	public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException
	{
		ApprovalNotificationRecipientsDTO approvalnotificationrecipientsDTO = (ApprovalNotificationRecipientsDTO)commonDTO;
		int index = 1;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(approvalnotificationrecipientsDTO);
		if(isInsert)
		{
			ps.setObject(index++,approvalnotificationrecipientsDTO.iD);
		}
		ps.setObject(index++,approvalnotificationrecipientsDTO.approvalPathId);
		ps.setObject(index++,approvalnotificationrecipientsDTO.organogramId);
		if(isInsert)
		{
			ps.setObject(index++, 0);
			ps.setObject(index++, lastModificationTime);
		}
	}
	
	public ApprovalNotificationRecipientsDTO build(ResultSet rs)
	{
		try
		{
			ApprovalNotificationRecipientsDTO approvalnotificationrecipientsDTO = new ApprovalNotificationRecipientsDTO();
			approvalnotificationrecipientsDTO.iD = rs.getLong("ID");
			approvalnotificationrecipientsDTO.approvalPathId = rs.getLong("approval_path_id");
			approvalnotificationrecipientsDTO.organogramId = rs.getLong("organogram_id");
			approvalnotificationrecipientsDTO.isDeleted = rs.getInt("isDeleted");
			approvalnotificationrecipientsDTO.lastModificationTime = rs.getLong("lastModificationTime");
			return approvalnotificationrecipientsDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
	
	
	public List<ApprovalNotificationRecipientsDTO> getApprovalNotificationRecipientsDTOListByApprovalPathID(long approvalPathID) throws Exception
	{
		String sql = "SELECT * FROM approval_notification_recipients where isDeleted=0 and approval_path_id="+approvalPathID+" order by approval_notification_recipients.lastModificationTime";
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);		
	}

	public ApprovalNotificationRecipientsDTO getDTOByID (long id)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID = " + id;
		ApprovalNotificationRecipientsDTO approvalnotificationrecipientsDTO = ConnectionAndStatementUtil.getT(sql,this::build);
		return approvalnotificationrecipientsDTO;
	}

	
	public List<ApprovalNotificationRecipientsDTO> getDTOs(Collection recordIDs)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID IN ( ";
		for(int i = 0;i<recordIDs.size();i++){
			if(i!=0){
				sql+=",";
			}
			sql+=((ArrayList)recordIDs).get(i);
		}
		sql+=")  order by lastModificationTime desc";
		printSql(sql);
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);		
	}
	
	public List<ApprovalNotificationRecipientsDTO> getAllApprovalNotificationRecipients (boolean isFirstReload)
    {
		String sql = "SELECT * FROM " + tableName + " WHERE ";
		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by " + tableName + ".lastModificationTime desc";
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);	
    }

	
	public List<ApprovalNotificationRecipientsDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO)
	{
		return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
	}

	
	public List<ApprovalNotificationRecipientsDTO> getDTOs(Hashtable  p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
			String filter, boolean tableHasJobCat)
	{
		String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat);
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);			
	}
				
}
	