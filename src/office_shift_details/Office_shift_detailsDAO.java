package office_shift_details;

import employee_education_info.Employee_education_infoDAO;
import employee_records.EmployeeCommonDTOService;
import office_shift.Office_shiftDAO;
import org.apache.log4j.Logger;
import user.UserDTO;
import util.CommonDTO;
import util.NavigationService4;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.*;

@SuppressWarnings({"rawtypes","unused","Duplicates"})
public class Office_shift_detailsDAO extends NavigationService4 implements EmployeeCommonDTOService<Office_shift_detailsDTO> {

    private static final Logger logger = Logger.getLogger(Employee_education_infoDAO.class);

    public static final String addQuery = "INSERT INTO {tableName} (office_shift_id, in_time, out_time, grace_in_period_mins, grace_out_period_mins, " +
            "start_date, end_date, is_active, late_minute, early_minute, insertion_date, inserted_by, modified_by, " +
            "search_column, lastModificationTime, isDeleted, id) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

    public static final String updateQuery = "UPDATE {tableName} SET office_shift_id=?, in_time=?, out_time=?, grace_in_period_mins=?, grace_out_period_mins=?, " +
            "start_date=?, end_date=?, is_active=?, late_minute=?, early_minute=?, insertion_date=?, inserted_by=?, modified_by=?, " +
            "search_column=?, lastModificationTime=? WHERE id = ?";
    private static final Map<String, String> searchMap = new HashMap<>();

    static {
        searchMap.put("office_shift_id", " and (office_shift_id = ?)");
        searchMap.put("in_time_start", " and (in_time >= ?)");
        searchMap.put("in_time_end", " and (in_time <= ?)");
        searchMap.put("out_time_start", " and (out_time >= ?)");
        searchMap.put("out_time_end", " and (out_time <= ?)");
        searchMap.put("AnyField", " and (search_column like ?)");
    }

    public Office_shift_detailsDAO(String tableName) {
        super(tableName);
        joinSQL = "";
        useSafeSearch = true;
        commonMaps = new Office_shift_detailsMAPS(tableName);
    }

    public Office_shift_detailsDAO() {
        this("office_shift_details");
    }

    public void setSearchColumn(Office_shift_detailsDTO office_shift_detailsDTO) {
        office_shift_detailsDTO.searchColumn = "";
        office_shift_detailsDTO.searchColumn += Office_shiftDAO.geText("English", office_shift_detailsDTO.officeShiftId) + " ";
        office_shift_detailsDTO.searchColumn += Office_shiftDAO.geText("Bangla", office_shift_detailsDTO.officeShiftId) + " ";


        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm aa");
        long inTimeMS = 0, outTimeMS = 0;
        try {
            inTimeMS = sdf.parse(office_shift_detailsDTO.inTime).getTime();
            outTimeMS = sdf.parse(office_shift_detailsDTO.outTime).getTime();
        } catch (Exception ex) {
            logger.debug(ex);
        }
        office_shift_detailsDTO.searchColumn += new Time(inTimeMS) + " ";
        office_shift_detailsDTO.searchColumn += new Time(outTimeMS);
    }

    public void set(PreparedStatement ps, Office_shift_detailsDTO office_shift_detailsDTO, boolean isInsert) throws SQLException {
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm aa");
        long inTimeMS = 0, outTimeMS = 0;
        try {
            inTimeMS = sdf.parse(office_shift_detailsDTO.inTime).getTime();
            outTimeMS = sdf.parse(office_shift_detailsDTO.outTime).getTime();
        } catch (Exception ex) {
            logger.debug(ex);
        }

        int index = 0;
        setSearchColumn(office_shift_detailsDTO);
        ps.setObject(++index, office_shift_detailsDTO.officeShiftId);
        ps.setObject(++index, new Time(inTimeMS));
        ps.setObject(++index, new Time(outTimeMS));
        ps.setObject(++index, office_shift_detailsDTO.graceInPeriodMins);
        ps.setObject(++index, office_shift_detailsDTO.graceOutPeriodMins);
        ps.setObject(++index, office_shift_detailsDTO.startDate);
        ps.setObject(++index, office_shift_detailsDTO.endDate);
        ps.setObject(++index, office_shift_detailsDTO.isActive);
        ps.setObject(++index, office_shift_detailsDTO.lateInAlertMinute);
        ps.setObject(++index, office_shift_detailsDTO.earlyOutAlertMinute);
        ps.setObject(++index, office_shift_detailsDTO.insertionDate);
        ps.setObject(++index, office_shift_detailsDTO.insertedBy);
        ps.setObject(++index, office_shift_detailsDTO.modifiedBy);
        ps.setObject(++index, office_shift_detailsDTO.searchColumn);
        ps.setObject(++index, System.currentTimeMillis());
        if (isInsert) {
            ps.setObject(++index, 0);
        }
        ps.setObject(++index, office_shift_detailsDTO.id);
    }


    @Override
    public Office_shift_detailsDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Office_shift_detailsDTO office_shift_detailsDTO = new Office_shift_detailsDTO();
            office_shift_detailsDTO.id = rs.getLong("id");
            office_shift_detailsDTO.officeShiftId = rs.getLong("office_shift_id");
            office_shift_detailsDTO.inTime = rs.getString("in_time");
            office_shift_detailsDTO.outTime = rs.getString("out_time");
            office_shift_detailsDTO.graceInPeriodMins = rs.getLong("grace_in_period_mins");
            office_shift_detailsDTO.graceOutPeriodMins = rs.getLong("grace_out_period_mins");
            office_shift_detailsDTO.startDate = rs.getLong("start_date");
            office_shift_detailsDTO.endDate = rs.getLong("end_date");
            office_shift_detailsDTO.isActive = rs.getInt("is_active");
            office_shift_detailsDTO.lateInAlertMinute = rs.getInt("late_minute");
            office_shift_detailsDTO.earlyOutAlertMinute = rs.getInt("early_minute");
            office_shift_detailsDTO.insertionDate = rs.getLong("insertion_date");
            office_shift_detailsDTO.insertedBy = rs.getString("inserted_by");
            office_shift_detailsDTO.modifiedBy = rs.getString("modified_by");
            office_shift_detailsDTO.searchColumn = rs.getString("search_column");
            office_shift_detailsDTO.isDeleted = rs.getInt("isDeleted");
            office_shift_detailsDTO.lastModificationTime = rs.getLong("lastModificationTime");
            return office_shift_detailsDTO;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    @Override
    public String getTableName() {
        return "office_shift_details";
    }


    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Office_shift_detailsDTO) commonDTO, addQuery, true);
    }


    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Office_shift_detailsDTO) commonDTO, updateQuery, false);
    }


    //need another getter for repository
    public Office_shift_detailsDTO getDTOByID(long ID) {
        return getDTOFromID(ID);
    }


    //add repository
    public List<Office_shift_detailsDTO> getAllOffice_shift_details(boolean isFirstReload) {
        return getAllDTOs(isFirstReload);
    }


    public List<Office_shift_detailsDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO) {
        return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
    }


    public List<Office_shift_detailsDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
                                                 String filter, boolean tableHasJobCat) {
        List<Object> objectList = new ArrayList<>();
        String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO,
                filter, tableHasJobCat,objectList);
        return getDTOs(sql,objectList);
    }


    public String getSqlWithSearchCriteriaForNormalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
                                                          UserDTO userDTO, String filter, boolean tableHasJobCat,List<Object>objectList) {
        return getSearchQuery(tableName, p_searchCriteria, limit, offset, category, searchMap,objectList);
    }

    public String getSqlWithSearchCriteria(Hashtable p_searchCriteria, int limit, int offset, int category,
                                           boolean isPermanentTable, UserDTO userDTO, String filter, boolean tableHasJobCat,List<Object>objectList) {
        return getSqlWithSearchCriteriaForNormalSearch(p_searchCriteria, limit, offset, category, userDTO, filter, tableHasJobCat,objectList);
    }

}
	