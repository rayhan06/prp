package office_shift_details;

import java.io.IOException;
import java.io.*;
import java.text.SimpleDateFormat;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import org.apache.log4j.Logger;

import login.LoginDTO;
import permission.MenuConstants;
import role.PermissionRepository;


import sessionmanager.SessionConstants;

import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;
import util.CommonRequestHandler;
import util.RecordNavigationManager4;

import java.util.*;
import javax.servlet.http.*;
import java.util.UUID;





import geolocation.GeoLocationDAO2;
import java.util.StringTokenizer;

import com.google.gson.Gson;

import pb.*;
import pbReport.*;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;




/**
 * Servlet implementation class Office_shift_detailsServlet
 */
@WebServlet("/Office_shift_detailsServlet")
@MultipartConfig
public class Office_shift_detailsServlet extends HttpServlet
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Office_shift_detailsServlet.class);

    String tableName = "office_shift_details";

	Office_shift_detailsDAO office_shift_detailsDAO;
	CommonRequestHandler commonRequestHandler;
    private final Gson gson = new Gson();

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Office_shift_detailsServlet()
	{
        super();
    	try
    	{
			office_shift_detailsDAO = new Office_shift_detailsDAO(tableName);
			commonRequestHandler = new CommonRequestHandler(office_shift_detailsDAO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		System.out.println("In doget request = " + request);
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		try
		{
			String actionType = request.getParameter("actionType");
			if(actionType.equals("getAddPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.OFFICE_SHIFT_DETAILS_ADD))
				{
					commonRequestHandler.getAddPage(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getEditPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.OFFICE_SHIFT_DETAILS_UPDATE))
				{
					getOffice_shift_details(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getURL"))
			{
				String URL = request.getParameter("URL");
				System.out.println("URL = " + URL);
				response.sendRedirect(URL);
			}
			else if(actionType.equals("search"))
			{
				System.out.println("search requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.OFFICE_SHIFT_DETAILS_SEARCH))
				{
					if(isPermanentTable)
					{
						String filter = request.getParameter("filter");
						System.out.println("filter = " + filter);
						if(filter!=null)
						{
							filter = ""; //shouldn't be directly used, rather manipulate it.
							searchOffice_shift_details(request, response, isPermanentTable, filter);
						}
						else
						{
							searchOffice_shift_details(request, response, isPermanentTable, "");
						}
					}
					else
					{
						//searchOffice_shift_details(request, response, tempTableName, isPermanentTable);
					}
				}
			}
			else if(actionType.equals("view"))
			{
				System.out.println("view requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.OFFICE_SHIFT_DETAILS_SEARCH))
				{
					commonRequestHandler.view(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}
			else
			{
				request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}





	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		// TODO Auto-generated method stub
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		System.out.println("doPost");
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);

		try
		{
			String actionType = request.getParameter("actionType");
			System.out.println("actionType = " + actionType);
			if(actionType.equals("add"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.OFFICE_SHIFT_DETAILS_ADD))
				{
					System.out.println("going to  addOffice_shift_details ");
					addOffice_shift_details(request, response, true, userDTO, true);
				}
				else
				{
					System.out.println("Not going to  addOffice_shift_details ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}

			else if(actionType.equals("getDTO"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.OFFICE_SHIFT_DETAILS_ADD))
				{
					getDTO(request, response);
				}
				else
				{
					System.out.println("Not going to  addOffice_shift_details ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}
			else if(actionType.equals("edit"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.OFFICE_SHIFT_DETAILS_UPDATE))
				{
					addOffice_shift_details(request, response, false, userDTO, isPermanentTable);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("delete"))
			{
				deleteOffice_shift_details(request, response, userDTO);
			}
			else if(actionType.equals("search"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.OFFICE_SHIFT_DETAILS_SEARCH))
				{
					searchOffice_shift_details(request, response, true, "");
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getGeo"))
			{
				System.out.println("going to geoloc ");
				request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
			}

		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}

	private void getDTO(HttpServletRequest request, HttpServletResponse response)
	{
		try
		{
			System.out.println("In getDTO");
			Office_shift_detailsDTO office_shift_detailsDTO = office_shift_detailsDAO.getDTOByID(Long.parseLong(request.getParameter("ID")));
			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");

			String encoded = this.gson.toJson(office_shift_detailsDTO);
			System.out.println("json encoded data = " + encoded);
			out.print(encoded);
			out.flush();
		}
		catch (NumberFormatException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void addOffice_shift_details(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, boolean isPermanentTable) throws IOException
	{
		// TODO Auto-generated method stub
		try
		{
			request.setAttribute("failureMessage", "");
			System.out.println("%%%% addOffice_shift_details");
			String path = getServletContext().getRealPath("/img2/");
			Office_shift_detailsDTO office_shift_detailsDTO;
			SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");

			if(addFlag == true)
			{
				office_shift_detailsDTO = new Office_shift_detailsDTO();
			}
			else
			{
				office_shift_detailsDTO = office_shift_detailsDAO.getDTOByID(Long.parseLong(request.getParameter("id")));
			}

			String Value = "";

			Value = request.getParameter("officeShiftId");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("officeShiftId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{

				office_shift_detailsDTO.officeShiftId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("inTime");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("inTime = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{

				office_shift_detailsDTO.inTime = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("outTime");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("outTime = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{

				office_shift_detailsDTO.outTime = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("graceInPeriodMins");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("graceInPeriodMins = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{

				office_shift_detailsDTO.graceInPeriodMins = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("graceOutPeriodMins");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("graceOutPeriodMins = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{

				office_shift_detailsDTO.graceOutPeriodMins = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("startDate");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("startDate = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{

				try
				{
					Date d = f.parse(Value);
					office_shift_detailsDTO.startDate = d.getTime();
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("endDate");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("endDate = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{

				try
				{
					Date d = f.parse(Value);
					office_shift_detailsDTO.endDate = d.getTime();
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("isActive");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("isActive = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{

				office_shift_detailsDTO.isActive = 1;
			}
			else
			{
				office_shift_detailsDTO.isActive = 0;
			}

			Value = request.getParameter("lateInAlert");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("lateInAlert = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{

				try
				{
					office_shift_detailsDTO.lateInAlertMinute = Integer.parseInt(Value);
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("earlyOutAlert");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("earlyOutAlert = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{

				try
				{
					office_shift_detailsDTO.earlyOutAlertMinute = Integer.parseInt(Value);
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			if(addFlag)
			{
				Calendar c = Calendar.getInstance();
				c.set(Calendar.HOUR_OF_DAY, 0);
				c.set(Calendar.MINUTE, 0);
				c.set(Calendar.SECOND, 0);
				c.set(Calendar.MILLISECOND, 0);

				office_shift_detailsDTO.insertionDate = c.getTimeInMillis();
			}


			Value = request.getParameter("insertedBy");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("insertedBy = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{

				office_shift_detailsDTO.insertedBy = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("modifiedBy");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("modifiedBy = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{

				office_shift_detailsDTO.modifiedBy = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			System.out.println("Done adding  addOffice_shift_details dto = " + office_shift_detailsDTO);
			long returnedID = -1;

			if(isPermanentTable == false) //add new row for validation and make the old row outdated
			{
				office_shift_detailsDAO.setIsDeleted(office_shift_detailsDTO.iD, CommonDTO.OUTDATED);
				returnedID = office_shift_detailsDAO.add(office_shift_detailsDTO);
				office_shift_detailsDAO.setIsDeleted(returnedID, CommonDTO.WAITING_FOR_APPROVAL);
			}
			else if(addFlag == true)
			{
				returnedID = office_shift_detailsDAO.manageWriteOperations(office_shift_detailsDTO, SessionConstants.INSERT, -1, userDTO);
			}
			else
			{
				returnedID = office_shift_detailsDAO.manageWriteOperations(office_shift_detailsDTO, SessionConstants.UPDATE, -1, userDTO);
			}









			if(isPermanentTable)
			{
				String inPlaceSubmit = request.getParameter("inplacesubmit");

				if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
				{
					getOffice_shift_details(request, response, returnedID);
				}
				else
				{
					response.sendRedirect("Office_shift_detailsServlet?actionType=search");
				}
			}
			else
			{
				commonRequestHandler.validate(office_shift_detailsDAO.getDTOByID(returnedID), request, response, userDTO);
			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}









	private void deleteOffice_shift_details(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO) throws IOException
	{
		try
		{
			String[] IDsToDelete = request.getParameterValues("ID");
			for(int i = 0; i < IDsToDelete.length; i ++)
			{
				long id = Long.parseLong(IDsToDelete[i]);
				System.out.println("------ DELETING " + IDsToDelete[i]);


				Office_shift_detailsDTO office_shift_detailsDTO = office_shift_detailsDAO.getDTOByID(id);
				office_shift_detailsDAO.manageWriteOperations(office_shift_detailsDTO, SessionConstants.DELETE, id, userDTO);
				response.sendRedirect("Office_shift_detailsServlet?actionType=search");

			}
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}

	}

	private void getOffice_shift_details(HttpServletRequest request, HttpServletResponse response, long id) throws ServletException, IOException
	{
		System.out.println("in getOffice_shift_details");
		Office_shift_detailsDTO office_shift_detailsDTO = null;
		try
		{
			office_shift_detailsDTO = office_shift_detailsDAO.getDTOByID(id);
			request.setAttribute("ID", office_shift_detailsDTO.iD);
			request.setAttribute("office_shift_detailsDTO",office_shift_detailsDTO);
			request.setAttribute("office_shift_detailsDAO",office_shift_detailsDAO);

			String URL= "";

			String inPlaceEdit = request.getParameter("inplaceedit");
			String inPlaceSubmit = request.getParameter("inplacesubmit");
			String getBodyOnly = request.getParameter("getBodyOnly");

			if(inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase(""))
			{
				URL = "office_shift_details/office_shift_detailsInPlaceEdit.jsp";
				request.setAttribute("inplaceedit","");
			}
			else if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
			{
				URL = "office_shift_details/office_shift_detailsSearchRow.jsp";
				request.setAttribute("inplacesubmit","");
			}
			else
			{
				if(getBodyOnly  != null && !getBodyOnly.equalsIgnoreCase(""))
				{
					URL = "office_shift_details/office_shift_detailsEditBody.jsp?actionType=edit";
				}
				else
				{
					URL = "office_shift_details/office_shift_detailsEdit.jsp?actionType=edit";
				}
			}

			RequestDispatcher rd = request.getRequestDispatcher(URL);
			rd.forward(request, response);
		}
		catch (NumberFormatException e)
		{
			e.printStackTrace();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}


	private void getOffice_shift_details(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		getOffice_shift_details(request, response, Long.parseLong(request.getParameter("ID")));
	}

	private void searchOffice_shift_details(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException
	{
		System.out.println("in  searchOffice_shift_details 1");
		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String ajax = request.getParameter("ajax");
		boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);

        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
			SessionConstants.NAV_OFFICE_SHIFT_DETAILS,
			request,
			office_shift_detailsDAO,
			SessionConstants.VIEW_OFFICE_SHIFT_DETAILS,
			SessionConstants.SEARCH_OFFICE_SHIFT_DETAILS,
			tableName,
			isPermanent,
			userDTO,
			filter,
			true);
        try
        {
			System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        }
        catch(Exception e)
        {
			System.out.println("failed to dojob" + e);
        }

		request.setAttribute("office_shift_detailsDAO",office_shift_detailsDAO);
        RequestDispatcher rd;
        if(!isPermanent)
        {
        	if(hasAjax == false)
	        {
	        	System.out.println("Going to office_shift_details/office_shift_detailsApproval.jsp");
	        	rd = request.getRequestDispatcher("office_shift_details/office_shift_detailsApproval.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to office_shift_details/office_shift_detailsApprovalForm.jsp");
	        	rd = request.getRequestDispatcher("office_shift_details/office_shift_detailsApprovalForm.jsp");
	        }
        }
        else
        {
	        if(hasAjax == false)
	        {
	        	System.out.println("Going to office_shift_details/office_shift_detailsSearch.jsp");
	        	rd = request.getRequestDispatcher("office_shift_details/office_shift_detailsSearch.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to office_shift_details/office_shift_detailsSearchForm.jsp");
	        	rd = request.getRequestDispatcher("office_shift_details/office_shift_detailsSearchForm.jsp");
	        }
        }
		rd.forward(request, response);
	}

}

