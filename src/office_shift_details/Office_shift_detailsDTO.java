package office_shift_details;

import java.util.*;

import util.*;


public class Office_shift_detailsDTO extends CommonDTO {

    public long id = 0;
    public long officeShiftId = 0;
    public String inTime = "";
    public String outTime = "";
    public long graceInPeriodMins = 0;
    public long graceOutPeriodMins = 0;
    public long startDate = 0;
    public long endDate = 0;
    public int isActive = 0;
    public int lateInAlertMinute = 0;
    public int earlyOutAlertMinute = 0;
    public long insertionDate = 0;
    public String insertedBy = "";
    public String modifiedBy = "";


    @Override
    public String toString() {
        return "$Office_shift_detailsDTO[" +
                " id = " + id +
                " officeShiftId = " + officeShiftId +
                " inTime = " + inTime +
                " outTime = " + outTime +
                " graceInPeriodMins = " + graceInPeriodMins +
                " graceOutPeriodMins = " + graceOutPeriodMins +
                " startDate = " + startDate +
                " endDate = " + endDate +
                " isActive = " + isActive +
                " lateInAlertMinute = " + lateInAlertMinute +
                " earlyOutAlertMinute = " + earlyOutAlertMinute +
                " insertionDate = " + insertionDate +
                " insertedBy = " + insertedBy +
                " modifiedBy = " + modifiedBy +
                " isDeleted = " + isDeleted +
                " lastModificationTime = " + lastModificationTime +
                "]";
    }

}