package office_shift_details;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class Office_shift_detailsRepository implements Repository {
	Office_shift_detailsDAO office_shift_detailsDAO;
	
	public void setDAO(Office_shift_detailsDAO office_shift_detailsDAO)
	{
		this.office_shift_detailsDAO = office_shift_detailsDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Office_shift_detailsRepository.class);
	Map<Long, Office_shift_detailsDTO>mapOfOffice_shift_detailsDTOToid;
	Map<Long, Set<Office_shift_detailsDTO> >mapOfOffice_shift_detailsDTOToofficeShiftId;


	static Office_shift_detailsRepository instance = null;  
	private Office_shift_detailsRepository(){
		mapOfOffice_shift_detailsDTOToid = new ConcurrentHashMap<>();
		mapOfOffice_shift_detailsDTOToofficeShiftId = new ConcurrentHashMap<>();
		office_shift_detailsDAO = new Office_shift_detailsDAO();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Office_shift_detailsRepository getInstance(){
		if (instance == null){
			instance = new Office_shift_detailsRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(office_shift_detailsDAO == null)
		{
			return;
		}
		try {
			List<Office_shift_detailsDTO> office_shift_detailsDTOs = office_shift_detailsDAO.getAllOffice_shift_details(reloadAll);
			for(Office_shift_detailsDTO office_shift_detailsDTO : office_shift_detailsDTOs) {
				Office_shift_detailsDTO oldOffice_shift_detailsDTO = getOffice_shift_detailsDTOByid(office_shift_detailsDTO.id);
				if( oldOffice_shift_detailsDTO != null ) {
					mapOfOffice_shift_detailsDTOToid.remove(oldOffice_shift_detailsDTO.id);
				
					if(mapOfOffice_shift_detailsDTOToofficeShiftId.containsKey(oldOffice_shift_detailsDTO.officeShiftId)) {
						mapOfOffice_shift_detailsDTOToofficeShiftId.get(oldOffice_shift_detailsDTO.officeShiftId).remove(oldOffice_shift_detailsDTO);
					}
					if(mapOfOffice_shift_detailsDTOToofficeShiftId.get(oldOffice_shift_detailsDTO.officeShiftId).isEmpty()) {
						mapOfOffice_shift_detailsDTOToofficeShiftId.remove(oldOffice_shift_detailsDTO.officeShiftId);
					}
					
				
					
				}
				if(office_shift_detailsDTO.isDeleted == 0) 
				{
					
					mapOfOffice_shift_detailsDTOToid.put(office_shift_detailsDTO.id, office_shift_detailsDTO);
				
					if( ! mapOfOffice_shift_detailsDTOToofficeShiftId.containsKey(office_shift_detailsDTO.officeShiftId)) {
						mapOfOffice_shift_detailsDTOToofficeShiftId.put(office_shift_detailsDTO.officeShiftId, new HashSet<>());
					}
					mapOfOffice_shift_detailsDTOToofficeShiftId.get(office_shift_detailsDTO.officeShiftId).add(office_shift_detailsDTO);
					
				
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Office_shift_detailsDTO> getOffice_shift_detailsList() {
		List <Office_shift_detailsDTO> office_shift_detailss = new ArrayList<Office_shift_detailsDTO>(this.mapOfOffice_shift_detailsDTOToid.values());
		return office_shift_detailss;
	}
	
	
	public Office_shift_detailsDTO getOffice_shift_detailsDTOByid( long id){
		return mapOfOffice_shift_detailsDTOToid.get(id);
	}
	
	
	public List<Office_shift_detailsDTO> getOffice_shift_detailsDTOByoffice_shift_id(long office_shift_id) {
		return new ArrayList<>( mapOfOffice_shift_detailsDTOToofficeShiftId.getOrDefault(office_shift_id,new HashSet<>()));
	}


	
	@Override
	public String getTableName() {
		return "office_shift_details";
	}
}


