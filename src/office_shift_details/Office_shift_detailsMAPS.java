package office_shift_details;
import java.util.*; 
import util.*;


public class Office_shift_detailsMAPS extends CommonMaps
{	
	public Office_shift_detailsMAPS(String tableName)
	{
		

		java_DTO_map.put("id".toLowerCase(), "id".toLowerCase());
		java_DTO_map.put("officeShiftId".toLowerCase(), "officeShiftId".toLowerCase());
		java_DTO_map.put("inTime".toLowerCase(), "inTime".toLowerCase());
		java_DTO_map.put("outTime".toLowerCase(), "outTime".toLowerCase());
		java_DTO_map.put("graceInPeriodMins".toLowerCase(), "graceInPeriodMins".toLowerCase());
		java_DTO_map.put("graceOutPeriodMins".toLowerCase(), "graceOutPeriodMins".toLowerCase());
		java_DTO_map.put("startDate".toLowerCase(), "startDate".toLowerCase());
		java_DTO_map.put("endDate".toLowerCase(), "endDate".toLowerCase());
		java_DTO_map.put("insertionDate".toLowerCase(), "insertionDate".toLowerCase());
		java_DTO_map.put("insertedBy".toLowerCase(), "insertedBy".toLowerCase());
		java_DTO_map.put("modifiedBy".toLowerCase(), "modifiedBy".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("office_shift_id".toLowerCase(), "officeShiftId".toLowerCase());
		java_SQL_map.put("in_time".toLowerCase(), "inTime".toLowerCase());
		java_SQL_map.put("out_time".toLowerCase(), "outTime".toLowerCase());
		java_SQL_map.put("grace_in_period_mins".toLowerCase(), "graceInPeriodMins".toLowerCase());
		java_SQL_map.put("grace_out_period_mins".toLowerCase(), "graceOutPeriodMins".toLowerCase());
		java_SQL_map.put("start_date".toLowerCase(), "startDate".toLowerCase());
		java_SQL_map.put("end_date".toLowerCase(), "endDate".toLowerCase());

		java_Text_map.put("Id".toLowerCase(), "id".toLowerCase());
		java_Text_map.put("Office Shift Id".toLowerCase(), "officeShiftId".toLowerCase());
		java_Text_map.put("In Time".toLowerCase(), "inTime".toLowerCase());
		java_Text_map.put("Out Time".toLowerCase(), "outTime".toLowerCase());
		java_Text_map.put("Grace In Period Mins".toLowerCase(), "graceInPeriodMins".toLowerCase());
		java_Text_map.put("Grace Out Period Mins".toLowerCase(), "graceOutPeriodMins".toLowerCase());
		java_Text_map.put("Start Date".toLowerCase(), "startDate".toLowerCase());
		java_Text_map.put("End Date".toLowerCase(), "endDate".toLowerCase());
		java_Text_map.put("Insertion Date".toLowerCase(), "insertionDate".toLowerCase());
		java_Text_map.put("Inserted By".toLowerCase(), "insertedBy".toLowerCase());
		java_Text_map.put("Modified By".toLowerCase(), "modifiedBy".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}