package recruitment_job_specific_exam_type;

import pb.CatRepository;
import pb.OptionDTO;
import util.CommonDTO;

public class RecruitmentJobSpecificExamTypeDTO extends CommonDTO {

    public long recruitmentJobDescriptionId = 0;
    public int jobExamTypeCat = 0;
    public String jobExamTypeNameEn = "";
    public String jobExamTypeNameBn = "";
    public double marks = 0;
    public double passMarks = 0;
    public boolean isSelected = false;
    public String description = "";
    public long insertionDate = 0;
    public long insertedByUserId = 0;
    public String modifiedBy = "";
    public long examStartDate = 0;
    public int order = 0;

    public OptionDTO getOptionDTO() {

        return new OptionDTO(
                CatRepository.getName("English", "job_exam_type", jobExamTypeCat),
                CatRepository.getName("Bangla", "job_exam_type", jobExamTypeCat),
                String.format("%d", order)
        );
    }
}
