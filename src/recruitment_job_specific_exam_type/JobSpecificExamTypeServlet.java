package recruitment_job_specific_exam_type;

import login.LoginDTO;
import org.apache.log4j.Logger;
import pb.CatDTO;
import pb.CatRepository;
import permission.MenuConstants;
import role.PermissionRepository;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.CommonRequestHandler;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * Servlet implementation class Job_applicant_documentsServlet
 */
@WebServlet("/JobSpecificExamTypeServlet")
@MultipartConfig
public class JobSpecificExamTypeServlet extends HttpServlet
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(JobSpecificExamTypeServlet.class);

    String tableName = "recruitment_job_specific_exam_type";

	JobSpecificExamTypeDao jobSpecificExamTypeDao;
	CommonRequestHandler commonRequestHandler;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public JobSpecificExamTypeServlet()
	{
        super();
    	try
    	{
			jobSpecificExamTypeDao = new JobSpecificExamTypeDao(tableName);
			commonRequestHandler = new CommonRequestHandler(jobSpecificExamTypeDao);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

    void getEditPage(HttpServletRequest request, HttpServletResponse response) throws Exception {
		long jobId = Long.parseLong(request.getParameter("jobId"));
		List<RecruitmentJobSpecificExamTypeDTO> jobSpecificExamTypeDTOS = JobSpecificExamTypeRepository.getInstance()
				.getRecruitmentJobSpecificExamTypeDTOByjob_id(jobId);
		if(jobSpecificExamTypeDTOS.size() == 0){
			jobSpecificExamTypeDao.getDTOsByJobId(jobId);
		}


		List<CatDTO> examTypes = CatRepository.getDTOs("job_exam_type");
		Map<Integer, CatDTO> examTypeMap = new HashMap<>(
				examTypes.stream().collect(Collectors.toMap(s -> s.value, s -> s))) ;

		jobSpecificExamTypeDTOS.forEach(i -> {
					if(examTypeMap.containsKey(i.jobExamTypeCat)){
						i.jobExamTypeNameBn = examTypeMap.get(i.jobExamTypeCat).languageTextBangla;
						i.jobExamTypeNameEn = examTypeMap.get(i.jobExamTypeCat).languageTextEnglish;
					}

		});

		request.setAttribute("jobSpecificExamTypeDTOS",jobSpecificExamTypeDTOS);

		RequestDispatcher rd;
		rd = request.getRequestDispatcher( "recruitment_job_specific_exam_type/recruitment_job_specific_exam_typeEdit.jsp");
		rd.forward(request, response);

	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		System.out.println("In doget request = " + request);
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		try
		{
			String actionType = request.getParameter("actionType");
			if(actionType.equals("getEditPage"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.JOB_EXAM_TYPE))
				{
					getEditPage(request, response );
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}

			else if(actionType.equals("getMark")){
				long jobId = Long.parseLong(request.getParameter("jobId"));
				long order = Long.parseLong(request.getParameter("order"));

				double data = 0.0;

				List<RecruitmentJobSpecificExamTypeDTO> dtos =
						JobSpecificExamTypeRepository.getInstance()
								.getRecruitmentJobSpecificExamTypeDTOByjob_id(jobId)
								.stream()
						        .filter(i -> i.order == order)
								.collect(Collectors.toList());
//						jobSpecificExamTypeDao.
//						getDTOsByJobIdAndLevel(jobId, order);
				if(dtos.size() > 0){
					data = dtos.get(0).marks;
				}

				PrintWriter out = response.getWriter();
				out.print(data);
				out.flush();

			}


			else
			{
				request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}





	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		// TODO Auto-generated method stub
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		System.out.println("doPost");
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);

		try
		{
			String actionType = request.getParameter("actionType");
			System.out.println("actionType = " + actionType);

			 if(actionType.equals("edit")){
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.JOB_EXAM_TYPE))
				{
					SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
				    JobSpecificExamTypeDao jobSpecificExamTypeDao = new JobSpecificExamTypeDao();
					int rowCount = Integer.parseInt(request.getParameter("rowCount"));
					RecruitmentJobSpecificExamTypeDTO dto = null;

					for(int i = 0; i < rowCount; i++){
						String id = "recruitment_job_specific_exam_type_id_" + i;
						long Id = Long.parseLong(request.getParameter(id));
						dto = JobSpecificExamTypeRepository.getInstance().recruitmentJobSpecificExamTypeDTOByID(Id);

						id = "is_selected_" + i;
						dto.isSelected = Boolean.parseBoolean(request.getParameter(id));

						id = "marks_" + i;
						dto.marks = Double.parseDouble(request.getParameter(id));

						id = "pass_marks_" + i;
						dto.passMarks = Double.parseDouble(request.getParameter(id));

						id = "order_" + i;
						dto.order = Integer.parseInt(request.getParameter(id));

						id = "description_" + i;
						dto.description = request.getParameter(id);
						dto.lastModificationTime = System.currentTimeMillis();

						id = "exam_start_date_" + i;
						String Value = request.getParameter(id);

						try
						{
							Date d = f.parse(Value);
							dto.examStartDate= d.getTime();
						}
						catch (Exception e)
						{
							e.printStackTrace();
						}

						jobSpecificExamTypeDao.update(dto);

					}
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}

		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}

}

