package recruitment_job_specific_exam_type;

import common.ConnectionAndStatementUtil;
import dbm.DBMR;
import dbm.DBMW;
import javafx.util.Pair;
import job_applicant_documents.Job_applicant_documentsDTO;
import org.apache.log4j.Logger;
import pb.CatDAO;
import pb.CatDTO;
import pb.CatRepository;
import user.UserDTO;
import util.CommonDTO;
import util.NavigationService4;
import vm_vehicle_office_assignment.Vm_vehicle_office_assignmentDTO;

import java.sql.*;
import java.util.*;
import java.util.stream.Collectors;

public class JobSpecificExamTypeDao extends NavigationService4 {

    Logger logger = Logger.getLogger(getClass());


    public JobSpecificExamTypeDao(String tableName)
    {
        super(tableName);
        joinSQL = "";
        columnNames = new String[]
        {
            "ID",
            "recruitment_job_description_id",
            "job_exam_type_cat",
            "marks",
            "pass_marks",
            "is_selected",
            "description",
            "inserted_by_user_id",
            "insertion_date",
            "modified_by",
            "exam_start_date",
            "exam_order",
            "isDeleted",
            "lastModificationTime"
        };
    }



    @Override
    public Collection getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO) {
        return null;
    }

    public JobSpecificExamTypeDao()
    {
        this("recruitment_job_specific_exam_type");
    }

    public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException
    {
        RecruitmentJobSpecificExamTypeDTO jobSpecificExamTypeDTO = (RecruitmentJobSpecificExamTypeDTO)commonDTO;

        int index = 1;
        long lastModificationTime = System.currentTimeMillis();
//		setSearchColumn(job_applicant_documentsDTO);
        if(isInsert)
        {
            ps.setObject(index++,jobSpecificExamTypeDTO.iD);
        }
        ps.setObject(index++,jobSpecificExamTypeDTO.recruitmentJobDescriptionId);
        ps.setObject(index++,jobSpecificExamTypeDTO.jobExamTypeCat);
        ps.setObject(index++,jobSpecificExamTypeDTO.marks);
        ps.setObject(index++,jobSpecificExamTypeDTO.passMarks);
        ps.setObject(index++,jobSpecificExamTypeDTO.isSelected);
        ps.setObject(index++,jobSpecificExamTypeDTO.description);
        ps.setObject(index++,jobSpecificExamTypeDTO.insertedByUserId);
        ps.setObject(index++,jobSpecificExamTypeDTO.insertionDate);
        ps.setObject(index++,jobSpecificExamTypeDTO.modifiedBy);
        ps.setObject(index++,jobSpecificExamTypeDTO.examStartDate);
        ps.setObject(index++,jobSpecificExamTypeDTO.order);
        if(isInsert)
        {
            ps.setObject(index++, 0);
            ps.setObject(index++, lastModificationTime);
        }
    }

    public RecruitmentJobSpecificExamTypeDTO build(ResultSet rs)
    {
        try
        {
            RecruitmentJobSpecificExamTypeDTO jobSpecificExamTypeDTO = new RecruitmentJobSpecificExamTypeDTO();
            jobSpecificExamTypeDTO.iD = rs.getLong("ID");
            jobSpecificExamTypeDTO.recruitmentJobDescriptionId = rs.getLong("recruitment_job_description_id");
            jobSpecificExamTypeDTO.marks = rs.getLong("marks");
            jobSpecificExamTypeDTO.passMarks = rs.getLong("pass_marks");
            jobSpecificExamTypeDTO.jobExamTypeCat = rs.getInt("job_exam_type_cat");
            jobSpecificExamTypeDTO.isSelected = rs.getBoolean("is_selected");
            jobSpecificExamTypeDTO.description = rs.getString("description");
            jobSpecificExamTypeDTO.insertedByUserId = rs.getLong("inserted_by_user_id");
            jobSpecificExamTypeDTO.insertionDate = rs.getLong("insertion_date");
            jobSpecificExamTypeDTO.modifiedBy = rs.getString("modified_by");
            jobSpecificExamTypeDTO.examStartDate = rs.getLong("exam_start_date");
            jobSpecificExamTypeDTO.order = rs.getInt("exam_order");
            jobSpecificExamTypeDTO.isDeleted = rs.getInt("isDeleted");
            jobSpecificExamTypeDTO.lastModificationTime = rs.getLong("lastModificationTime");
            return jobSpecificExamTypeDTO;
        }
        catch (SQLException ex)
        {
            logger.error(ex);
            return null;
        }
    }

    public List<RecruitmentJobSpecificExamTypeDTO> getDTOs(Collection recordIDs){
        if(recordIDs.isEmpty()){
            return new ArrayList<>();
        }
        String sql = "SELECT * FROM " + tableName + " WHERE ID IN ( ";
        for(int i = 0;i<recordIDs.size();i++){
            if(i!=0){
                sql+=",";
            }
            sql+= " ? ";
        }
        sql+=")  order by lastModificationTime desc";
        printSql(sql);
        return ConnectionAndStatementUtil.getListOfT(sql, Arrays.asList(recordIDs.toArray()),this::build);

    }

    public RecruitmentJobSpecificExamTypeDTO getDTOByID (long ID)
    {
        String sql = "SELECT * FROM " + tableName + " WHERE ID = ? ";
        RecruitmentJobSpecificExamTypeDTO dto =
                ConnectionAndStatementUtil.getT(sql, Arrays.asList(ID), this::build);
        return dto;
    }


    public List<RecruitmentJobSpecificExamTypeDTO> getDTOsByJobId(long jobDescriptionId){


        String sql = "SELECT * ";

        sql += " FROM " + tableName;

        sql += " WHERE isDeleted = 0 and recruitment_job_description_id =  ? ";

        sql+="  order by insertion_date asc";

        printSql(sql);

        return ConnectionAndStatementUtil.getListOfT(sql, Arrays.asList(jobDescriptionId),this::build);

    }

    public Pair<String, String> getNameByJobIDAndLevel(long jobId, long level){
        Pair<String, String> pair =  new Pair<>("", "");
        if(level == 0){
            pair = new Pair<>("Primary", "প্রাথমিক");
        }
        else {
            List<RecruitmentJobSpecificExamTypeDTO> dtos = JobSpecificExamTypeRepository.getInstance()
                    .getRecruitmentJobSpecificExamTypeDTOByjob_id(jobId).stream().filter(i -> i.order == level).collect(Collectors.toList());
//                    getDTOsByJobIdAndLevel(jobId, level);
            if(dtos.size()> 0){
                int catId = dtos.get(0).jobExamTypeCat;
                CatDTO catDTO = CatRepository.getDTO("job_exam_type", catId);
                if(catDTO != null){
                    pair = new Pair<>(catDTO.languageTextEnglish, catDTO.languageTextBangla);
                }
            }
        }

        return pair;

    }

    public List<RecruitmentJobSpecificExamTypeDTO> getDTOsByJobIdAndLevel(long jobId, long level){


        String sql = "SELECT * ";

        sql += " FROM " + tableName;

        sql += " WHERE isDeleted = 0 and recruitment_job_description_id = ? ";
        sql += " and exam_order = ? ";
        sql+="  order by lastModificationTime desc";

        printSql(sql);

        return ConnectionAndStatementUtil.getListOfT(sql, Arrays.asList(jobId, level),this::build);

    }


    public int getNextLevelFromJobIdAndCurrentLevel(long jobId, int level){
        int nextLevel = -1;
        List<RecruitmentJobSpecificExamTypeDTO> dtos = JobSpecificExamTypeRepository.getInstance().
                getRecruitmentJobSpecificExamTypeDTOByjob_id(jobId);
//                getDTOsByJobId(jobId);
        if(dtos.size() > 0){
            Optional<Integer> value = dtos.stream().filter(j -> j.isSelected).map(j -> j.order)
                                     .filter(j -> j > level)
                                     .min(Integer::compare);
            if(value.isPresent()){
                nextLevel = value.get();
            }
        }

        return nextLevel;
    }

    public double getPassMarkFromJobIdAndCurrentLevel(long jobId, int level){
        double passMark = -1;
        List<RecruitmentJobSpecificExamTypeDTO> dtos = JobSpecificExamTypeRepository.getInstance().
                getRecruitmentJobSpecificExamTypeDTOByjob_id(jobId);
        if(dtos.size() > 0){
            dtos = dtos.stream().filter(e -> e.order == level).collect(Collectors.toList());
            if(dtos.size()>0){
                passMark = dtos.get(0).passMarks;
            }
        }

        return passMark;
    }

    public List<Integer> getNextLevelsFromJobIdAndCurrentLevel(long jobId, int level){
        List<Integer> nextLevels = new ArrayList<>();
        List<RecruitmentJobSpecificExamTypeDTO> dtos = JobSpecificExamTypeRepository.getInstance().
                getRecruitmentJobSpecificExamTypeDTOByjob_id(jobId);
//                getDTOsByJobId(jobId);
        if(dtos.size() > 0){
            nextLevels = dtos.stream().filter(j -> j.isSelected).map(j -> j.order)
                    .filter(j -> j > level).collect(Collectors.toList());
        }

        return nextLevels;
    }

    public int getPreviousLevelFromJobIdAndCurrentLevel(long jobId, int level){
        int prevLevel = -1;
        List<RecruitmentJobSpecificExamTypeDTO> dtos =
                JobSpecificExamTypeRepository.getInstance().
                        getRecruitmentJobSpecificExamTypeDTOByjob_id(jobId);
//                getDTOsByJobId(jobId);
        if(dtos.size() > 0){
            List<Integer> orders = dtos.stream().filter(j -> j.isSelected)
                    .map(j -> j.order).collect(Collectors.toList());
            orders.add(0);
            Optional<Integer> value = orders.stream().filter(j -> j < level)
                    .max(Integer::compare);
            if(value.isPresent()){
                prevLevel = value.get();
            }
        }

        return prevLevel;
    }


}
