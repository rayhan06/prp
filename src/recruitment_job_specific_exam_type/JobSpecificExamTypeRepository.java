package recruitment_job_specific_exam_type;

import com.google.gson.Gson;
import org.apache.log4j.Logger;
import pb.OptionDTO;
import pb.Utils;
import recruitment_job_description.Recruitment_job_descriptionDTO;
import repository.Repository;
import repository.RepositoryManager;
import vm_route.VmRouteStoppageDTO;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public class JobSpecificExamTypeRepository implements Repository {
    static Logger logger = Logger.getLogger(JobSpecificExamTypeRepository.class);
    JobSpecificExamTypeDao jobSpecificExamTypeDao = null;
    Map<Long, RecruitmentJobSpecificExamTypeDTO> mapOfRecruitmentJobSpecificExamTypeDTOToiD;
    Map<Long, Set<RecruitmentJobSpecificExamTypeDTO> >mapOfRecruitmentJobSpecificExamTypeDTOTojobId;
    static JobSpecificExamTypeRepository instance = null;
    Gson gson = new Gson();

    public void setDAO(JobSpecificExamTypeDao jobSpecificExamTypeDao)
    {
        this.jobSpecificExamTypeDao = jobSpecificExamTypeDao;
    }

    private JobSpecificExamTypeRepository(){
        mapOfRecruitmentJobSpecificExamTypeDTOToiD = new ConcurrentHashMap<>();
        mapOfRecruitmentJobSpecificExamTypeDTOTojobId = new ConcurrentHashMap<>();

        setDAO(new JobSpecificExamTypeDao());
        RepositoryManager.getInstance().addRepository(this);
    }

    public synchronized static JobSpecificExamTypeRepository getInstance(){
        if (instance == null){
            instance = new JobSpecificExamTypeRepository();
        }
        return instance;
    }



    public void reload(boolean reloadAll){
        if(jobSpecificExamTypeDao == null)
        {
            return;
        }
        try {
            List<RecruitmentJobSpecificExamTypeDTO> recruitmentJobSpecificExamTypeDTOS = (List<RecruitmentJobSpecificExamTypeDTO>)
                    jobSpecificExamTypeDao.getAll(reloadAll);
            for(RecruitmentJobSpecificExamTypeDTO dto : recruitmentJobSpecificExamTypeDTOS) {
                RecruitmentJobSpecificExamTypeDTO oldDTO = recruitmentJobSpecificExamTypeDTOByIDWithoutClone(dto.iD);
                if( oldDTO != null ) {
                    mapOfRecruitmentJobSpecificExamTypeDTOToiD.remove(oldDTO.iD);

                    if(mapOfRecruitmentJobSpecificExamTypeDTOTojobId.containsKey(oldDTO.recruitmentJobDescriptionId)) {
                        mapOfRecruitmentJobSpecificExamTypeDTOTojobId.get(oldDTO.recruitmentJobDescriptionId).remove(oldDTO);
                    }
                    if(mapOfRecruitmentJobSpecificExamTypeDTOTojobId.get(oldDTO.recruitmentJobDescriptionId).isEmpty()) {
                        mapOfRecruitmentJobSpecificExamTypeDTOTojobId.remove(oldDTO.recruitmentJobDescriptionId);
                    }
                }
                if(dto.isDeleted == 0)
                {
                    mapOfRecruitmentJobSpecificExamTypeDTOToiD.put(dto.iD, dto);

                    if( ! mapOfRecruitmentJobSpecificExamTypeDTOTojobId.containsKey(dto.recruitmentJobDescriptionId)) {
                        mapOfRecruitmentJobSpecificExamTypeDTOTojobId.put(dto.recruitmentJobDescriptionId, new HashSet<>());
                    }
                    mapOfRecruitmentJobSpecificExamTypeDTOTojobId.get(dto.recruitmentJobDescriptionId).add(dto);
                }
            }

        } catch (Exception e) {
            logger.debug("FATAL", e);
        }
    }

    public List<RecruitmentJobSpecificExamTypeDTO> getRecruitmentJobSpecificExamTypeDTOList() {
         return clone(new ArrayList<>(this.mapOfRecruitmentJobSpecificExamTypeDTOToiD.values()));
    }

    public RecruitmentJobSpecificExamTypeDTO recruitmentJobSpecificExamTypeDTOByIDWithoutClone( long ID){
        return mapOfRecruitmentJobSpecificExamTypeDTOToiD.get(ID);
    }

    public RecruitmentJobSpecificExamTypeDTO recruitmentJobSpecificExamTypeDTOByID( long ID){
//        RecruitmentJobSpecificExamTypeDTO dto = mapOfRecruitmentJobSpecificExamTypeDTOToiD.get(ID);
//        return dto == null ? jobSpecificExamTypeDao.getDTOByID(ID): dto;
        return clone(mapOfRecruitmentJobSpecificExamTypeDTOToiD.get(ID));
    }


    public List<RecruitmentJobSpecificExamTypeDTO> getRecruitmentJobSpecificExamTypeDTOByjob_id(long job_id) {
//        System.out.println(mapOfRecruitmentJobSpecificExamTypeDTOTojobId);
        return clone(new ArrayList<>( mapOfRecruitmentJobSpecificExamTypeDTOTojobId.getOrDefault(job_id,new HashSet<>()))
                .stream().sorted(Comparator.comparingLong(i -> i.insertionDate)).collect(Collectors.toList()))
                ;
    }

    public RecruitmentJobSpecificExamTypeDTO clone(RecruitmentJobSpecificExamTypeDTO dto) {
        String raw = gson.toJson(dto);
        return gson.fromJson(raw, RecruitmentJobSpecificExamTypeDTO.class);
    }

    public List<RecruitmentJobSpecificExamTypeDTO> clone(List<RecruitmentJobSpecificExamTypeDTO> dtoList) {
        return dtoList
                .stream()
                .map(dto -> clone(dto))
                .collect(Collectors.toList());
    }

    public String buildOptions(String language,Long selectedId,long jobId){

        List<RecruitmentJobSpecificExamTypeDTO> recruitmentJobSpecificExamTypeDTOS = getRecruitmentJobSpecificExamTypeDTOByjob_id(jobId);

        return buildOptions(language,selectedId,recruitmentJobSpecificExamTypeDTOS);
    }

    public static String buildOptions(String language,Long selectedId,List<RecruitmentJobSpecificExamTypeDTO> list){
        List<OptionDTO> optionDTOList = null;
        if (list != null && list.size() > 0) {
            optionDTOList = list.stream()
                    .map(RecruitmentJobSpecificExamTypeDTO::getOptionDTO)
                    .collect(Collectors.toList());
        }
        return Utils.buildOptions(optionDTOList, language, selectedId == null ? null : String.valueOf(selectedId));
    }

    public String getTableName() {
        String tableName = "";
        try{
            tableName = "recruitment_job_specific_exam_type";
        }catch(Exception ex){
            logger.debug("FATAL",ex);
        }
        return tableName;
    }
}
