package economic_code;

import economic_sub_code.Economic_sub_codeDTO;
import employee_records.Employee_recordsDTO;
import org.apache.log4j.Logger;
import pb.OptionDTO;
import pb.Utils;
import util.LockManager;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Economic_codeRepository {
    private static final Logger logger = Logger.getLogger(Economic_codeRepository.class);
    private final Economic_codeDAO economic_codeDAO;
    private Map<Long, Economic_codeDTO> mapById;
    private Map<Long, List<Economic_codeDTO>> mapByEconomicGroup;
    private List<Economic_codeDTO> economicCodeDTOList;

    private Economic_codeRepository() {
        economic_codeDAO = Economic_codeDAO.getInstance();
        reload();
    }

    private static class EconomicCodeRepositoryLazyLoader {
        final static Economic_codeRepository INSTANCE = new Economic_codeRepository();
    }

    public static Economic_codeRepository getInstance() {
        return Economic_codeRepository.EconomicCodeRepositoryLazyLoader.INSTANCE;
    }

    private void reload() {
        logger.debug("Economic Code Repository loading start for reload: ");
        economicCodeDTOList = economic_codeDAO.getAllEconomicCode(true);
        if (economicCodeDTOList != null && economicCodeDTOList.size() > 0) {
            mapById = economicCodeDTOList.parallelStream()
                    .collect(Collectors.toMap(e -> e.iD, e -> e, (e1, e2) -> e1));
            mapByEconomicGroup = economicCodeDTOList.parallelStream()
                    .collect(Collectors.groupingBy(dto -> dto.economicGroupType));
            economicCodeDTOList.sort(Comparator.comparing(o -> o.iD));
            mapByEconomicGroup.values().forEach(ls -> ls.sort(Comparator.comparing(o -> o.iD)));
        }
        logger.debug("Economic Code Repository loading end for reload: ");
    }

    public Economic_codeDTO getById(long id) {
        if (mapById.get(id) == null) {
            synchronized (LockManager.getLock(id + "ECR")) {
                Economic_codeDTO dto = economic_codeDAO.getDTOByID(id);
                if (dto != null) {
                    mapById.put(dto.iD, dto);
                    List<Economic_codeDTO> dtoListByEconomicGroupType = mapByEconomicGroup.getOrDefault(
                            dto.economicGroupType, new ArrayList<>()
                    );
                    dtoListByEconomicGroupType.add(dto);
                    mapByEconomicGroup.put(dto.economicGroupType, dtoListByEconomicGroupType);
                }
            }
        }
        return mapById.get(id);
    }

    public List<Economic_codeDTO> getEconomic_codeDTOList() {
        return economicCodeDTOList;
    }

    public String buildEconomicCodeOption(String language, Long selectedId) {
        List<OptionDTO> optionDTOList = economicCodeDTOList.stream()
                .map(Economic_codeDTO::getOptionDTO)
                .collect(Collectors.toList());
        return Utils.buildOptions(optionDTOList, language, selectedId == null ? null : String.valueOf(selectedId));
    }

    public String buildEconomicCodeOptionFromEconomicGroup(String language, long economicGroup, Long selectedId) {
        List<OptionDTO> optionDTOList = mapByEconomicGroup.get(economicGroup)
                .stream()
                .map(Economic_codeDTO::getOptionDTO)
                .collect(Collectors.toList());
        return Utils.buildOptions(optionDTOList, language, selectedId == null ? null : String.valueOf(selectedId));
    }
}