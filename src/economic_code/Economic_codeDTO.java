package economic_code;

import pb.OptionDTO;
import pb.Utils;
import util.CommonDTO;
import util.StringUtils;

public class Economic_codeDTO extends CommonDTO {
    public long economicGroupType = 0;
    public String code = "";
    public String descriptionEn = "";
    public String descriptionBn = "";
    public long insertedBy = 0;
    public long insertionTime = 0;
    public long modifiedBy = 0;

    public OptionDTO getOptionDTO() {
        String textEn = code.concat(" - ").concat(descriptionEn);
        String textBn = StringUtils.convertToBanNumber(code).concat(" - ").concat(descriptionBn);
        return new OptionDTO(textEn, textBn, String.valueOf(iD));
    }

    public String getFormattedCodeAndName(String language) {
        return "English".equalsIgnoreCase(language)
                ? code.concat(" - ").concat(descriptionEn)
                : StringUtils.convertToBanNumber(code).concat(" - ").concat(descriptionBn);
    }

    @Override
    public String toString() {
        return "Economic_codeDTO{" +
                "economicGroup=" + economicGroupType +
                ", code='" + code + '\'' +
                ", descriptionEn='" + descriptionEn + '\'' +
                ", descriptionBn='" + descriptionBn + '\'' +
                ", insertedBy=" + insertedBy +
                ", insertionTime=" + insertionTime +
                ", modifiedBy=" + modifiedBy +
                '}';
    }


}
