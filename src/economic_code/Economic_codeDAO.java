package economic_code;

import common.CommonDAOService;
import org.apache.log4j.Logger;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings({"unused", "Duplicates"})
public class Economic_codeDAO implements CommonDAOService<Economic_codeDTO> {
    private static final Logger logger = Logger.getLogger(Economic_codeDAO.class);

    private static final String addQuery = "INSERT INTO {tableName} (economic_group_type,code,description_eng,description_bng,"
            .concat("modified_by,lastModificationTime,insertion_time,inserted_by,isDeleted,ID) VALUES(?,?,?,?,?,?,?,?,?,?)");

    private static final String updateQuery = "UPDATE {tableName} SET economic_group_type=?,code=?,description_eng=?,description_bng=?,"
            .concat("modified_by=?,lastModificationTime=? WHERE ID=?");

    private static final String getByEconomicGroup = "SELECT * FROM economic_code WHERE economic_group_type=%d AND isDeleted=0";

    private static final Map<String, String> searchMap = new HashMap<>();

    private static Economic_codeDAO INSTANCE = null;

    public static Economic_codeDAO getInstance() {
        if (INSTANCE == null) {
            synchronized (Economic_codeDAO.class) {
                if (INSTANCE == null) {
                    INSTANCE = new Economic_codeDAO();
                }
            }
        }
        return INSTANCE;
    }

    private Economic_codeDAO() {

    }

    @Override
    public void set(PreparedStatement ps, Economic_codeDTO economic_codeDTO, boolean isInsert) throws SQLException {
        int index = 0;
        ps.setObject(++index, economic_codeDTO.economicGroupType);
        ps.setObject(++index, economic_codeDTO.code);
        ps.setObject(++index, economic_codeDTO.descriptionEn);
        ps.setObject(++index, economic_codeDTO.descriptionBn);
        ps.setObject(++index, economic_codeDTO.modifiedBy);
        ps.setObject(++index, economic_codeDTO.lastModificationTime);
        if (isInsert) {
            ps.setObject(++index, economic_codeDTO.insertionTime);
            ps.setObject(++index, economic_codeDTO.insertedBy);
            ps.setObject(++index, 0);
        }
        ps.setObject(++index, economic_codeDTO.iD);
    }

    @Override
    public Economic_codeDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Economic_codeDTO dto = new Economic_codeDTO();
            dto.iD = rs.getLong("ID");
            dto.economicGroupType = rs.getLong("economic_group_type");
            dto.code = rs.getString("code");
            dto.descriptionEn = rs.getString("description_eng");
            dto.descriptionBn = rs.getString("description_bng");
            dto.insertionTime = rs.getLong("insertion_time");
            dto.insertedBy = rs.getLong("inserted_by");
            dto.modifiedBy = rs.getLong("modified_by");
            dto.isDeleted = rs.getInt("isDeleted");
            dto.lastModificationTime = rs.getLong("lastModificationTime");
            return dto;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    @Override
    public String getTableName() {
        return "economic_code";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    @Override
    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Economic_codeDTO) commonDTO, updateQuery, false);
    }

    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Economic_codeDTO) commonDTO, addQuery, true);
    }

    public Economic_codeDTO getDTOByID(long ID) {
        return getDTOFromID(ID);
    }

    public List<Economic_codeDTO> getDTOsByEconomicGroup(long economicGroupType) {
        return getDTOs(String.format(getByEconomicGroup, economicGroupType));
    }

    public List<Economic_codeDTO> getAllEconomicCode(boolean isFirstReload) {
        return getAllDTOs(isFirstReload);
    }

}