create table economic_code
(
    ID                   bigint(20) primary key,
    economic_group_type  bigint(20)           null,
    code                 varchar(255)         null,
    description_eng      varchar(255)         null,
    description_bng      varchar(255)         null,
    inserted_by          bigint(20) default 0 null,
    insertion_time       bigint(20) default 0 null,
    isDeleted            int(4)     default 0 null,
    modified_by          bigint(20) default 0 null,
    lastModificationTime bigint(20) default 0 null
)
    engine = MyISAM
    collate = utf8_unicode_ci;