package pi_package_item_map;

import java.util.*;

import org.apache.commons.lang3.builder.ToStringBuilder;
import util.*;

public class PiPackageItemMapChildDTO extends CommonDTO {
    public long piPackageItemMapId = -1;
    public long officeUnitId = -1;
    public long fiscalYearId = -1;
    public long serialNumber = -1;
    public long piPackageFinalId = -1;
    public long piLotFinalId = -1;
    public long itemGroupId = -1;
    public long itemTypeId = -1;
    public long itemId = -1;
    public long piUnitId = -1;
    public String description = "";
    public long itemQuantity = -1;
    public long itemUnitPrice = -1;
    public long itemTotalPrice = -1;
    public long annualDemandId = -1;
    public long annualDemandChildId = -1;
    public boolean isSelected = false;
    public String insertedBy = "";
    public String modifiedBy = "";
    public long insertionDate = -1;

    public List<PiPackageItemMapChildDTO> piPackageItemMapChildDTOList = new ArrayList<>();

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("piPackageItemMapId", piPackageItemMapId)
                .append("fiscalYearId", fiscalYearId)
                .append("serialNumber", serialNumber)
                .append("piPackageFinalId", piPackageFinalId)
                .append("piLotFinalId", piLotFinalId)
                .append("itemGroupId", itemGroupId)
                .append("itemTypeId", itemTypeId)
                .append("itemId", itemId)
                .append("piUnitId", piUnitId)
                .append("description", description)
                .append("itemQuantity", itemQuantity)
                .append("itemUnitPrice", itemUnitPrice)
                .append("itemTotalPrice", itemTotalPrice)
                .append("isSelected", isSelected)
                .append("insertedBy", insertedBy)
                .append("modifiedBy", modifiedBy)
                .append("insertionDate", insertionDate)
                .append("piPackageItemMapChildDTOList", piPackageItemMapChildDTOList)
                .toString();
    }
}