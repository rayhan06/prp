package pi_package_item_map;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.sql.SQLException;

import common.CommonDAOService;
import org.apache.log4j.Logger;
import util.*;
import pb.*;

public class PiPackageItemMapChildDAO implements CommonDAOService<PiPackageItemMapChildDTO> {

    Logger logger = Logger.getLogger(getClass());

    private static String addQuery = "";
    private static String updateQuery = "";
    private String[] columnNames = null;

    private PiPackageItemMapChildDAO() {
        columnNames = new String[]
                {
                        "ID",
                        "pi_package_item_map_id",
                        "office_unit_id",
                        "fiscal_year_id",
                        "serial_number",
                        "pi_package_final_id",
                        "pi_lot_final_id",
                        "item_group_id",
                        "item_type_id",
                        "item_id",
                        "pi_unit_id",
                        "description",
                        "item_quantity",
                        "item_unit_price",
                        "item_total_price",
                        "annual_demand_id",
                        "annual_demand_child_id",
                        "is_selected",
                        "search_column",
                        "inserted_by",
                        "modified_by",
                        "insertion_date",
                        "isDeleted",
                        "lastModificationTime"
                };
        updateQuery = getUpdateQuery(columnNames);
        addQuery = getAddQuery(columnNames);

        searchMap.put("fiscal_year_id", " and (fiscal_year_id = ?)");
        searchMap.put("insertion_date_start", " and (insertion_date >= ?)");
        searchMap.put("insertion_date_end", " and (insertion_date <= ?)");
        searchMap.put("AnyField", " and (search_column like ?)");
    }

    private final Map<String, String> searchMap = new HashMap<>();

    private static class LazyLoader {
        static final PiPackageItemMapChildDAO INSTANCE = new PiPackageItemMapChildDAO();
    }

    public static PiPackageItemMapChildDAO getInstance() {
        return LazyLoader.INSTANCE;
    }

    public void setSearchColumn(PiPackageItemMapChildDTO pipackageitemmapchildDTO) {
        pipackageitemmapchildDTO.searchColumn = "";
    }

    @Override
    public void set(PreparedStatement ps, PiPackageItemMapChildDTO pipackageitemmapchildDTO, boolean isInsert) throws SQLException {
        int index = 0;
        long lastModificationTime = System.currentTimeMillis();
        setSearchColumn(pipackageitemmapchildDTO);
        if (isInsert) {
            ps.setObject(++index, pipackageitemmapchildDTO.iD);
        }
        ps.setObject(++index, pipackageitemmapchildDTO.piPackageItemMapId);
        ps.setObject(++index, pipackageitemmapchildDTO.officeUnitId);
        ps.setObject(++index, pipackageitemmapchildDTO.fiscalYearId);
        ps.setObject(++index, pipackageitemmapchildDTO.serialNumber);
        ps.setObject(++index, pipackageitemmapchildDTO.piPackageFinalId);
        ps.setObject(++index, pipackageitemmapchildDTO.piLotFinalId);
        ps.setObject(++index, pipackageitemmapchildDTO.itemGroupId);
        ps.setObject(++index, pipackageitemmapchildDTO.itemTypeId);
        ps.setObject(++index, pipackageitemmapchildDTO.itemId);
        ps.setObject(++index, pipackageitemmapchildDTO.piUnitId);
        ps.setObject(++index, pipackageitemmapchildDTO.description);
        ps.setObject(++index, pipackageitemmapchildDTO.itemQuantity);
        ps.setObject(++index, pipackageitemmapchildDTO.itemUnitPrice);
        ps.setObject(++index, pipackageitemmapchildDTO.itemTotalPrice);
        ps.setObject(++index, pipackageitemmapchildDTO.annualDemandId);
        ps.setObject(++index, pipackageitemmapchildDTO.annualDemandChildId);
        ps.setObject(++index, pipackageitemmapchildDTO.isSelected);
        ps.setObject(++index, pipackageitemmapchildDTO.searchColumn);
        ps.setObject(++index, pipackageitemmapchildDTO.insertedBy);
        ps.setObject(++index, pipackageitemmapchildDTO.modifiedBy);
        ps.setObject(++index, pipackageitemmapchildDTO.insertionDate);
        if (isInsert) {
            ps.setObject(++index, pipackageitemmapchildDTO.isDeleted);
        }
        ps.setObject(++index, lastModificationTime);
        if (!isInsert) {
            ps.setObject(++index, pipackageitemmapchildDTO.iD);
        }
    }

    @Override
    public PiPackageItemMapChildDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            PiPackageItemMapChildDTO pipackageitemmapchildDTO = new PiPackageItemMapChildDTO();
            int i = 0;
            pipackageitemmapchildDTO.iD = rs.getLong(columnNames[i++]);
            pipackageitemmapchildDTO.piPackageItemMapId = rs.getLong(columnNames[i++]);
            pipackageitemmapchildDTO.officeUnitId = rs.getLong(columnNames[i++]);
            pipackageitemmapchildDTO.fiscalYearId = rs.getLong(columnNames[i++]);
            pipackageitemmapchildDTO.serialNumber = rs.getLong(columnNames[i++]);
            pipackageitemmapchildDTO.piPackageFinalId = rs.getLong(columnNames[i++]);
            pipackageitemmapchildDTO.piLotFinalId = rs.getLong(columnNames[i++]);
            pipackageitemmapchildDTO.itemGroupId = rs.getLong(columnNames[i++]);
            pipackageitemmapchildDTO.itemTypeId = rs.getLong(columnNames[i++]);
            pipackageitemmapchildDTO.itemId = rs.getLong(columnNames[i++]);
            pipackageitemmapchildDTO.piUnitId = rs.getLong(columnNames[i++]);
            pipackageitemmapchildDTO.description = rs.getString(columnNames[i++]);
            pipackageitemmapchildDTO.itemQuantity = rs.getLong(columnNames[i++]);
            pipackageitemmapchildDTO.itemUnitPrice = rs.getLong(columnNames[i++]);
            pipackageitemmapchildDTO.itemTotalPrice = rs.getLong(columnNames[i++]);
            pipackageitemmapchildDTO.annualDemandId = rs.getLong(columnNames[i++]);
            pipackageitemmapchildDTO.annualDemandChildId = rs.getLong(columnNames[i++]);
            pipackageitemmapchildDTO.isSelected = rs.getBoolean(columnNames[i++]);
            pipackageitemmapchildDTO.searchColumn = rs.getString(columnNames[i++]);
            pipackageitemmapchildDTO.insertedBy = rs.getString(columnNames[i++]);
            pipackageitemmapchildDTO.modifiedBy = rs.getString(columnNames[i++]);
            pipackageitemmapchildDTO.insertionDate = rs.getLong(columnNames[i++]);
            pipackageitemmapchildDTO.isDeleted = rs.getInt(columnNames[i++]);
            pipackageitemmapchildDTO.lastModificationTime = rs.getLong(columnNames[i++]);
            return pipackageitemmapchildDTO;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    public PiPackageItemMapChildDTO getDTOByID(long id) {
        return getDTOFromID(id);
    }

    @Override
    public String getTableName() {
        return "pi_package_item_map_child";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((PiPackageItemMapChildDTO) commonDTO, addQuery, true);
    }

    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((PiPackageItemMapChildDTO) commonDTO, updateQuery, false);
    }

    @Override
    public String getLastModifierUser() {
        return "modified_by";
    }
}
	