package pi_package_item_map;

public class FinalPackageLotModel {
    public long piPackageItemMapId = -1;
    public long serialNumber = -1;
    public long fiscalYearId = -1;
    public String fiscalYearEn = "";
    public String fiscalYearBn = "";
    public long piPackageFinalId = -1;
    public String packageNumberEn = "";
    public String packageNumberBn = "";
    public String packageNameEn = "";
    public String packageNameBn = "";
    public long piLotFinalId = -1;
    public String lotNumberEn = "";
    public String lotNumberBn = "";
    public String lotNameEn = "";
    public String lotNameBn = "";
    public String descriptionEn = "";
    public String descriptionBn = "";
    public String quantityEn = "";
    public String quantityBn = "";
    public long estCost = -1;
    public String estCostText = "";
    public long totalPrice = -1;
}
