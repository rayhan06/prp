package pi_package_item_map;
import java.util.*; 
import util.*;


public class PiPackageItemMapChildMAPS extends CommonMaps
{	
	public PiPackageItemMapChildMAPS(String tableName)
	{
		


		java_SQL_map.put("pi_package_item_map_id".toLowerCase(), "piPackageItemMapId".toLowerCase());
		java_SQL_map.put("pi_package_new_id".toLowerCase(), "piPackageNewId".toLowerCase());
		java_SQL_map.put("item_group_id".toLowerCase(), "itemGroupId".toLowerCase());
		java_SQL_map.put("item_type_id".toLowerCase(), "itemTypeId".toLowerCase());
		java_SQL_map.put("item_id".toLowerCase(), "itemId".toLowerCase());
		java_SQL_map.put("pi_unit_id".toLowerCase(), "piUnitId".toLowerCase());
		java_SQL_map.put("is_selected".toLowerCase(), "isSelected".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Package Item Map Id".toLowerCase(), "piPackageItemMapId".toLowerCase());
		java_Text_map.put("Pi Package New Id".toLowerCase(), "piPackageNewId".toLowerCase());
		java_Text_map.put("Item Group Id".toLowerCase(), "itemGroupId".toLowerCase());
		java_Text_map.put("Item Type Id".toLowerCase(), "itemTypeId".toLowerCase());
		java_Text_map.put("Item".toLowerCase(), "itemId".toLowerCase());
		java_Text_map.put("Unit".toLowerCase(), "piUnitId".toLowerCase());
		java_Text_map.put("Description".toLowerCase(), "description".toLowerCase());
		java_Text_map.put("Selected".toLowerCase(), "isSelected".toLowerCase());
		java_Text_map.put("Search Column".toLowerCase(), "searchColumn".toLowerCase());
		java_Text_map.put("Inserted By".toLowerCase(), "insertedBy".toLowerCase());
		java_Text_map.put("Modified By".toLowerCase(), "modifiedBy".toLowerCase());
		java_Text_map.put("Insertion Date".toLowerCase(), "insertionDate".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}