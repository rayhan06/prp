package pi_package_item_map;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import com.google.gson.Gson;

import repository.Repository;
import repository.RepositoryManager;

public class PiPackageItemMapChildRepository implements Repository {
    PiPackageItemMapChildDAO pipackageitemmapchildDAO = null;
    static Logger logger = Logger.getLogger(PiPackageItemMapChildRepository.class);
    Map<Long, PiPackageItemMapChildDTO> mapOfPiPackageItemMapChildDTOToiD;
    Map<Long, Set<PiPackageItemMapChildDTO>> mapOfPiPackageItemMapChildDTOTopiPackageItemMapId;
    Map<Long, Set<PiPackageItemMapChildDTO>> mapOfPiPackageItemMapChildDTOTopiPackageNewId;
    Map<Long, Set<PiPackageItemMapChildDTO>> mapOfPiPackageItemMapChildDTOToitemGroupId;
    Map<Long, Set<PiPackageItemMapChildDTO>> mapOfPiPackageItemMapChildDTOToitemTypeId;
    Gson gson;

    private PiPackageItemMapChildRepository() {
        pipackageitemmapchildDAO = PiPackageItemMapChildDAO.getInstance();
        mapOfPiPackageItemMapChildDTOToiD = new ConcurrentHashMap<>();
        mapOfPiPackageItemMapChildDTOTopiPackageItemMapId = new ConcurrentHashMap<>();
        mapOfPiPackageItemMapChildDTOTopiPackageNewId = new ConcurrentHashMap<>();
        mapOfPiPackageItemMapChildDTOToitemGroupId = new ConcurrentHashMap<>();
        mapOfPiPackageItemMapChildDTOToitemTypeId = new ConcurrentHashMap<>();
        gson = new Gson();
        RepositoryManager.getInstance().addRepository(this);
    }

    private static class LazyLoader {
        final static PiPackageItemMapChildRepository INSTANCE = new PiPackageItemMapChildRepository();
    }

    public static PiPackageItemMapChildRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

    public void reload(boolean reloadAll) {
        try {
            List<PiPackageItemMapChildDTO> pipackageitemmapchildDTOs = pipackageitemmapchildDAO.getAllDTOs(reloadAll);
            for (PiPackageItemMapChildDTO pipackageitemmapchildDTO : pipackageitemmapchildDTOs) {
                PiPackageItemMapChildDTO oldPiPackageItemMapChildDTO = getPiPackageItemMapChildDTOByiD(pipackageitemmapchildDTO.iD);
                if (oldPiPackageItemMapChildDTO != null) {
                    mapOfPiPackageItemMapChildDTOToiD.remove(oldPiPackageItemMapChildDTO.iD);

                    if (mapOfPiPackageItemMapChildDTOTopiPackageItemMapId.containsKey(oldPiPackageItemMapChildDTO.piPackageItemMapId)) {
                        mapOfPiPackageItemMapChildDTOTopiPackageItemMapId.get(oldPiPackageItemMapChildDTO.piPackageItemMapId).remove(oldPiPackageItemMapChildDTO);
                    }
                    if (mapOfPiPackageItemMapChildDTOTopiPackageItemMapId.get(oldPiPackageItemMapChildDTO.piPackageItemMapId).isEmpty()) {
                        mapOfPiPackageItemMapChildDTOTopiPackageItemMapId.remove(oldPiPackageItemMapChildDTO.piPackageItemMapId);
                    }

                    if (mapOfPiPackageItemMapChildDTOTopiPackageNewId.containsKey(oldPiPackageItemMapChildDTO.piPackageFinalId)) {
                        mapOfPiPackageItemMapChildDTOTopiPackageNewId.get(oldPiPackageItemMapChildDTO.piPackageFinalId).remove(oldPiPackageItemMapChildDTO);
                    }
                    if (mapOfPiPackageItemMapChildDTOTopiPackageNewId.get(oldPiPackageItemMapChildDTO.piPackageFinalId).isEmpty()) {
                        mapOfPiPackageItemMapChildDTOTopiPackageNewId.remove(oldPiPackageItemMapChildDTO.piPackageFinalId);
                    }

                    if (mapOfPiPackageItemMapChildDTOToitemGroupId.containsKey(oldPiPackageItemMapChildDTO.itemGroupId)) {
                        mapOfPiPackageItemMapChildDTOToitemGroupId.get(oldPiPackageItemMapChildDTO.itemGroupId).remove(oldPiPackageItemMapChildDTO);
                    }
                    if (mapOfPiPackageItemMapChildDTOToitemGroupId.get(oldPiPackageItemMapChildDTO.itemGroupId).isEmpty()) {
                        mapOfPiPackageItemMapChildDTOToitemGroupId.remove(oldPiPackageItemMapChildDTO.itemGroupId);
                    }

                    if (mapOfPiPackageItemMapChildDTOToitemTypeId.containsKey(oldPiPackageItemMapChildDTO.itemTypeId)) {
                        mapOfPiPackageItemMapChildDTOToitemTypeId.get(oldPiPackageItemMapChildDTO.itemTypeId).remove(oldPiPackageItemMapChildDTO);
                    }
                    if (mapOfPiPackageItemMapChildDTOToitemTypeId.get(oldPiPackageItemMapChildDTO.itemTypeId).isEmpty()) {
                        mapOfPiPackageItemMapChildDTOToitemTypeId.remove(oldPiPackageItemMapChildDTO.itemTypeId);
                    }


                }
                if (pipackageitemmapchildDTO.isDeleted == 0) {

                    mapOfPiPackageItemMapChildDTOToiD.put(pipackageitemmapchildDTO.iD, pipackageitemmapchildDTO);

                    if (!mapOfPiPackageItemMapChildDTOTopiPackageItemMapId.containsKey(pipackageitemmapchildDTO.piPackageItemMapId)) {
                        mapOfPiPackageItemMapChildDTOTopiPackageItemMapId.put(pipackageitemmapchildDTO.piPackageItemMapId, new HashSet<>());
                    }
                    mapOfPiPackageItemMapChildDTOTopiPackageItemMapId.get(pipackageitemmapchildDTO.piPackageItemMapId).add(pipackageitemmapchildDTO);

                    if (!mapOfPiPackageItemMapChildDTOTopiPackageNewId.containsKey(pipackageitemmapchildDTO.piPackageFinalId)) {
                        mapOfPiPackageItemMapChildDTOTopiPackageNewId.put(pipackageitemmapchildDTO.piPackageFinalId, new HashSet<>());
                    }
                    mapOfPiPackageItemMapChildDTOTopiPackageNewId.get(pipackageitemmapchildDTO.piPackageFinalId).add(pipackageitemmapchildDTO);

                    if (!mapOfPiPackageItemMapChildDTOToitemGroupId.containsKey(pipackageitemmapchildDTO.itemGroupId)) {
                        mapOfPiPackageItemMapChildDTOToitemGroupId.put(pipackageitemmapchildDTO.itemGroupId, new HashSet<>());
                    }
                    mapOfPiPackageItemMapChildDTOToitemGroupId.get(pipackageitemmapchildDTO.itemGroupId).add(pipackageitemmapchildDTO);

                    if (!mapOfPiPackageItemMapChildDTOToitemTypeId.containsKey(pipackageitemmapchildDTO.itemTypeId)) {
                        mapOfPiPackageItemMapChildDTOToitemTypeId.put(pipackageitemmapchildDTO.itemTypeId, new HashSet<>());
                    }
                    mapOfPiPackageItemMapChildDTOToitemTypeId.get(pipackageitemmapchildDTO.itemTypeId).add(pipackageitemmapchildDTO);

                }
            }

        } catch (Exception e) {
            logger.debug("FATAL", e);
        }
    }

    public PiPackageItemMapChildDTO clone(PiPackageItemMapChildDTO dto) {
        String raw = gson.toJson(dto);
        return gson.fromJson(raw, PiPackageItemMapChildDTO.class);
    }


    public List<PiPackageItemMapChildDTO> getPiPackageItemMapChildList() {
        List<PiPackageItemMapChildDTO> pipackageitemmapchilds = new ArrayList<PiPackageItemMapChildDTO>(this.mapOfPiPackageItemMapChildDTOToiD.values());
        return pipackageitemmapchilds;
    }

    public List<PiPackageItemMapChildDTO> copyPiPackageItemMapChildList() {
        List<PiPackageItemMapChildDTO> pipackageitemmapchilds = getPiPackageItemMapChildList();
        return pipackageitemmapchilds
                .stream()
                .map(dto -> clone(dto))
                .collect(Collectors.toList());
    }

    public PiPackageItemMapChildDTO getPiPackageItemMapChildDTOByiD(long iD) {
        return mapOfPiPackageItemMapChildDTOToiD.get(iD);
    }

    public PiPackageItemMapChildDTO copyPiPackageItemMapChildDTOByiD(long iD) {
        return clone(mapOfPiPackageItemMapChildDTOToiD.get(iD));
    }


    public List<PiPackageItemMapChildDTO> getPiPackageItemMapChildDTOBypiPackageItemMapId(long piPackageItemMapId) {
        return new ArrayList<>(mapOfPiPackageItemMapChildDTOTopiPackageItemMapId.getOrDefault(piPackageItemMapId, new HashSet<>()));
    }

    public List<PiPackageItemMapChildDTO> copyPiPackageItemMapChildDTOBypiPackageItemMapId(long piPackageItemMapId) {
        List<PiPackageItemMapChildDTO> pipackageitemmapchilds = getPiPackageItemMapChildDTOBypiPackageItemMapId(piPackageItemMapId);
        return pipackageitemmapchilds
                .stream()
                .map(dto -> clone(dto))
                .collect(Collectors.toList());
    }

    public List<PiPackageItemMapChildDTO> getPiPackageItemMapChildDTOBypiPackageNewId(long piPackageNewId) {
        return new ArrayList<>(mapOfPiPackageItemMapChildDTOTopiPackageNewId.getOrDefault(piPackageNewId, new HashSet<>()));
    }

    public List<PiPackageItemMapChildDTO> copyPiPackageItemMapChildDTOBypiPackageNewId(long piPackageNewId) {
        List<PiPackageItemMapChildDTO> pipackageitemmapchilds = getPiPackageItemMapChildDTOBypiPackageNewId(piPackageNewId);
        return pipackageitemmapchilds
                .stream()
                .map(dto -> clone(dto))
                .collect(Collectors.toList());
    }

    public List<PiPackageItemMapChildDTO> getPiPackageItemMapChildDTOByitemGroupId(long itemGroupId) {
        return new ArrayList<>(mapOfPiPackageItemMapChildDTOToitemGroupId.getOrDefault(itemGroupId, new HashSet<>()));
    }

    public List<PiPackageItemMapChildDTO> copyPiPackageItemMapChildDTOByitemGroupId(long itemGroupId) {
        List<PiPackageItemMapChildDTO> pipackageitemmapchilds = getPiPackageItemMapChildDTOByitemGroupId(itemGroupId);
        return pipackageitemmapchilds
                .stream()
                .map(dto -> clone(dto))
                .collect(Collectors.toList());
    }

    public List<PiPackageItemMapChildDTO> getPiPackageItemMapChildDTOByitemTypeId(long itemTypeId) {
        return new ArrayList<>(mapOfPiPackageItemMapChildDTOToitemTypeId.getOrDefault(itemTypeId, new HashSet<>()));
    }

    public List<PiPackageItemMapChildDTO> copyPiPackageItemMapChildDTOByitemTypeId(long itemTypeId) {
        List<PiPackageItemMapChildDTO> pipackageitemmapchilds = getPiPackageItemMapChildDTOByitemTypeId(itemTypeId);
        return pipackageitemmapchilds
                .stream()
                .map(dto -> clone(dto))
                .collect(Collectors.toList());
    }

    @Override
    public String getTableName() {
        return pipackageitemmapchildDAO.getTableName();
    }
}


