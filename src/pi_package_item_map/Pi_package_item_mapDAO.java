package pi_package_item_map;

import common.CommonDAOService;
import fiscal_year.Fiscal_yearDTO;
import fiscal_year.Fiscal_yearRepository;
import org.apache.log4j.Logger;
import pi_package_final.PiPackageLotFinalRepository;
import pi_package_final.Pi_package_finalRepository;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Pi_package_item_mapDAO implements CommonDAOService<Pi_package_item_mapDTO> {
    Logger logger = Logger.getLogger(getClass());

    private static String addQuery = "";
    private static String updateQuery = "";
    private static final String getPackageItemMapsByFiscalYearAndPackage =
            "SELECT * FROM pi_package_item_map WHERE isDeleted = 0 " +
                    "AND fiscal_year_id=%d " +
                    "AND pi_package_final_id=%d " +
                    "ORDER BY lastModificationTime DESC";
    private static final String getPackageItemMapsByFiscalYearAndLot =
            "SELECT * FROM pi_package_item_map WHERE isDeleted = 0 " +
                    "AND fiscal_year_id=%d " +
                    "AND pi_lot_final_id=%d " +
                    "ORDER BY lastModificationTime DESC";

    private String[] columnNames = null;

    private Pi_package_item_mapDAO() {
        columnNames = new String[]
                {
                        "ID",
                        "fiscal_year_id",
                        "has_lot",
                        "pi_package_final_id",
                        "pi_lot_final_id",
                        "search_column",
                        "inserted_by",
                        "modified_by",
                        "insertion_date",
                        "isDeleted",
                        "lastModificationTime"
                };
        updateQuery = getUpdateQuery(columnNames);
        addQuery = getAddQuery(columnNames);

        searchMap.put("fiscal_year_id", " and (fiscal_year_id = ?)");
        searchMap.put("pi_package_final_id", " and (pi_package_final_id = ?)");
        searchMap.put("pi_lot_final_id", " and (pi_lot_final_id = ?)");
        searchMap.put("insertion_date_start", " and (insertion_date >= ?)");
        searchMap.put("insertion_date_end", " and (insertion_date <= ?)");
        searchMap.put("AnyField", " and (search_column like ?)");
    }

    private final Map<String, String> searchMap = new HashMap<>();

    private static class LazyLoader {
        static final Pi_package_item_mapDAO INSTANCE = new Pi_package_item_mapDAO();
    }

    public static Pi_package_item_mapDAO getInstance() {
        return LazyLoader.INSTANCE;
    }

    public void setSearchColumn(Pi_package_item_mapDTO pi_package_item_mapDTO) {
        pi_package_item_mapDTO.searchColumn = "";
        Fiscal_yearDTO fiscalYearDTO = Fiscal_yearRepository.getInstance()
                .getFiscal_yearDTOByid(pi_package_item_mapDTO.fiscalYearId);
        if(fiscalYearDTO != null){
            pi_package_item_mapDTO.searchColumn += fiscalYearDTO.nameEn + " " + fiscalYearDTO.nameBn + " ";
        }
        pi_package_item_mapDTO.searchColumn += Pi_package_finalRepository.getInstance()
                .getName(pi_package_item_mapDTO.piPackageFinalId, "English") + " " +
                Pi_package_finalRepository.getInstance()
                        .getName(pi_package_item_mapDTO.piPackageFinalId, "Bangla") + " ";
        pi_package_item_mapDTO.searchColumn += PiPackageLotFinalRepository.getInstance()
                .getName(pi_package_item_mapDTO.piLotFinalId, "English") + " " +
                PiPackageLotFinalRepository.getInstance()
                        .getName(pi_package_item_mapDTO.piLotFinalId, "Bangla") + " ";
    }

    @Override
    public void set(PreparedStatement ps, Pi_package_item_mapDTO pi_package_item_mapDTO, boolean isInsert) throws SQLException {
        int index = 0;
        long lastModificationTime = System.currentTimeMillis();
        setSearchColumn(pi_package_item_mapDTO);
        if (isInsert) {
            ps.setObject(++index, pi_package_item_mapDTO.iD);
        }
        ps.setObject(++index, pi_package_item_mapDTO.fiscalYearId);
        ps.setObject(++index, pi_package_item_mapDTO.hasLot);
        ps.setObject(++index, pi_package_item_mapDTO.piPackageFinalId);
        ps.setObject(++index, pi_package_item_mapDTO.piLotFinalId);
        ps.setObject(++index, pi_package_item_mapDTO.searchColumn);
        ps.setObject(++index, pi_package_item_mapDTO.insertedBy);
        ps.setObject(++index, pi_package_item_mapDTO.modifiedBy);
        ps.setObject(++index, pi_package_item_mapDTO.insertionDate);
        if (isInsert) {
            ps.setObject(++index, pi_package_item_mapDTO.isDeleted);
        }
        ps.setObject(++index, lastModificationTime);
        if (!isInsert) {
            ps.setObject(++index, pi_package_item_mapDTO.iD);
        }
    }

    @Override
    public Pi_package_item_mapDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Pi_package_item_mapDTO pi_package_item_mapDTO = new Pi_package_item_mapDTO();
            int i = 0;
            pi_package_item_mapDTO.iD = rs.getLong(columnNames[i++]);
            pi_package_item_mapDTO.fiscalYearId = rs.getLong(columnNames[i++]);
            pi_package_item_mapDTO.hasLot = rs.getBoolean(columnNames[i++]);
            pi_package_item_mapDTO.piPackageFinalId = rs.getLong(columnNames[i++]);
            pi_package_item_mapDTO.piLotFinalId = rs.getLong(columnNames[i++]);
            pi_package_item_mapDTO.searchColumn = rs.getString(columnNames[i++]);
            pi_package_item_mapDTO.insertedBy = rs.getString(columnNames[i++]);
            pi_package_item_mapDTO.modifiedBy = rs.getString(columnNames[i++]);
            pi_package_item_mapDTO.insertionDate = rs.getLong(columnNames[i++]);
            pi_package_item_mapDTO.isDeleted = rs.getInt(columnNames[i++]);
            pi_package_item_mapDTO.lastModificationTime = rs.getLong(columnNames[i++]);
            return pi_package_item_mapDTO;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    public Pi_package_item_mapDTO getDTOByID(long id) {
        Pi_package_item_mapDTO pi_package_item_mapDTO = null;
        try {
            pi_package_item_mapDTO = getDTOFromID(id);
            if (pi_package_item_mapDTO != null) {
                PiPackageItemMapChildDAO piPackageItemMapChildDAO = PiPackageItemMapChildDAO.getInstance();
                List<PiPackageItemMapChildDTO> piPackageItemMapChildDTOList = (List<PiPackageItemMapChildDTO>) piPackageItemMapChildDAO
                        .getDTOsByParent("pi_package_item_map_id", pi_package_item_mapDTO.iD);
                pi_package_item_mapDTO.piPackageItemMapChildDTOList = piPackageItemMapChildDTOList;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return pi_package_item_mapDTO;
    }

    @Override
    public String getTableName() {
        return "pi_package_item_map";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Pi_package_item_mapDTO) commonDTO, addQuery, true);
    }

    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Pi_package_item_mapDTO) commonDTO, updateQuery, false);
    }

    @Override
    public String getLastModifierUser() {
        return "modified_by";
    }

    public List<Pi_package_item_mapDTO> getPackageItemMapsByFiscalYearAndPackage(long fiscalYearId, long packageId) {
        String sql = String.format(String.format(getPackageItemMapsByFiscalYearAndPackage, fiscalYearId, packageId));
        return Pi_package_item_mapDAO.getInstance().getDTOs(sql);
    }

    public List<Pi_package_item_mapDTO> getPackageItemMapsByFiscalYearAndLot(long fiscalYearId, long lotId) {
        String sql = String.format(String.format(getPackageItemMapsByFiscalYearAndLot, fiscalYearId, lotId));
        return Pi_package_item_mapDAO.getInstance().getDTOs(sql);
    }
}
	