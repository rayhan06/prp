package pi_package_item_map;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import com.google.gson.Gson;

import repository.Repository;
import repository.RepositoryManager;

public class Pi_package_item_mapRepository implements Repository {
    Pi_package_item_mapDAO pi_package_item_mapDAO = null;
    static Logger logger = Logger.getLogger(Pi_package_item_mapRepository.class);
    Map<Long, Pi_package_item_mapDTO> mapOfPi_package_item_mapDTOToiD;
    Map<Long, Set<Pi_package_item_mapDTO>> mapOfPi_package_item_mapDTOTopiPackageNewId;
    Map<Long, Set<Pi_package_item_mapDTO>> mapOfPi_package_item_mapDTOToitemGroupId;
    Map<Long, Set<Pi_package_item_mapDTO>> mapOfPi_package_item_mapDTOToitemTypeId;
    Gson gson;

    private Pi_package_item_mapRepository() {
        pi_package_item_mapDAO = Pi_package_item_mapDAO.getInstance();
        mapOfPi_package_item_mapDTOToiD = new ConcurrentHashMap<>();
        mapOfPi_package_item_mapDTOTopiPackageNewId = new ConcurrentHashMap<>();
        mapOfPi_package_item_mapDTOToitemGroupId = new ConcurrentHashMap<>();
        mapOfPi_package_item_mapDTOToitemTypeId = new ConcurrentHashMap<>();
        gson = new Gson();
        RepositoryManager.getInstance().addRepository(this);
    }

    private static class LazyLoader {
        final static Pi_package_item_mapRepository INSTANCE = new Pi_package_item_mapRepository();
    }

    public static Pi_package_item_mapRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

    public void reload(boolean reloadAll) {
        try {
            List<Pi_package_item_mapDTO> pi_package_item_mapDTOs = pi_package_item_mapDAO.getAllDTOs(reloadAll);
            for (Pi_package_item_mapDTO pi_package_item_mapDTO : pi_package_item_mapDTOs) {
                Pi_package_item_mapDTO oldPi_package_item_mapDTO = getPi_package_item_mapDTOByiD(pi_package_item_mapDTO.iD);
                if (oldPi_package_item_mapDTO != null) {
                    mapOfPi_package_item_mapDTOToiD.remove(oldPi_package_item_mapDTO.iD);

                    if (mapOfPi_package_item_mapDTOTopiPackageNewId.containsKey(oldPi_package_item_mapDTO.piPackageFinalId)) {
                        mapOfPi_package_item_mapDTOTopiPackageNewId.get(oldPi_package_item_mapDTO.piPackageFinalId).remove(oldPi_package_item_mapDTO);
                    }
                    if (mapOfPi_package_item_mapDTOTopiPackageNewId.get(oldPi_package_item_mapDTO.piPackageFinalId).isEmpty()) {
                        mapOfPi_package_item_mapDTOTopiPackageNewId.remove(oldPi_package_item_mapDTO.piPackageFinalId);
                    }
                }
                if (pi_package_item_mapDTO.isDeleted == 0) {

                    mapOfPi_package_item_mapDTOToiD.put(pi_package_item_mapDTO.iD, pi_package_item_mapDTO);

                    if (!mapOfPi_package_item_mapDTOTopiPackageNewId.containsKey(pi_package_item_mapDTO.piPackageFinalId)) {
                        mapOfPi_package_item_mapDTOTopiPackageNewId.put(pi_package_item_mapDTO.piPackageFinalId, new HashSet<>());
                    }
                    mapOfPi_package_item_mapDTOTopiPackageNewId.get(pi_package_item_mapDTO.piPackageFinalId).add(pi_package_item_mapDTO);
                }
            }

        } catch (Exception e) {
            logger.debug("FATAL", e);
        }
    }

    public Pi_package_item_mapDTO clone(Pi_package_item_mapDTO dto) {
        String raw = gson.toJson(dto);
        return gson.fromJson(raw, Pi_package_item_mapDTO.class);
    }

    public List<Pi_package_item_mapDTO> getPi_package_item_mapList() {
        List<Pi_package_item_mapDTO> pi_package_item_maps = new ArrayList<Pi_package_item_mapDTO>(this.mapOfPi_package_item_mapDTOToiD.values());
        return pi_package_item_maps;
    }

    public List<Pi_package_item_mapDTO> copyPi_package_item_mapList() {
        List<Pi_package_item_mapDTO> pi_package_item_maps = getPi_package_item_mapList();
        return pi_package_item_maps
                .stream()
                .map(dto -> clone(dto))
                .collect(Collectors.toList());
    }

    public Pi_package_item_mapDTO getPi_package_item_mapDTOByiD(long iD) {
        return mapOfPi_package_item_mapDTOToiD.get(iD);
    }

    public Pi_package_item_mapDTO copyPi_package_item_mapDTOByiD(long iD) {
        return clone(mapOfPi_package_item_mapDTOToiD.get(iD));
    }

    public List<Pi_package_item_mapDTO> getPi_package_item_mapDTOBypiPackageNewId(long piPackageNewId) {
        return new ArrayList<>(mapOfPi_package_item_mapDTOTopiPackageNewId.getOrDefault(piPackageNewId, new HashSet<>()));
    }

    public List<Pi_package_item_mapDTO> copyPi_package_item_mapDTOBypiPackageNewId(long piPackageNewId) {
        List<Pi_package_item_mapDTO> pi_package_item_maps = getPi_package_item_mapDTOBypiPackageNewId(piPackageNewId);
        return pi_package_item_maps
                .stream()
                .map(dto -> clone(dto))
                .collect(Collectors.toList());
    }

    public List<Pi_package_item_mapDTO> getPi_package_item_mapDTOByitemGroupId(long itemGroupId) {
        return new ArrayList<>(mapOfPi_package_item_mapDTOToitemGroupId.getOrDefault(itemGroupId, new HashSet<>()));
    }

    public List<Pi_package_item_mapDTO> copyPi_package_item_mapDTOByitemGroupId(long itemGroupId) {
        List<Pi_package_item_mapDTO> pi_package_item_maps = getPi_package_item_mapDTOByitemGroupId(itemGroupId);
        return pi_package_item_maps
                .stream()
                .map(dto -> clone(dto))
                .collect(Collectors.toList());
    }

    public List<Pi_package_item_mapDTO> getPi_package_item_mapDTOByitemTypeId(long itemTypeId) {
        return new ArrayList<>(mapOfPi_package_item_mapDTOToitemTypeId.getOrDefault(itemTypeId, new HashSet<>()));
    }

    public List<Pi_package_item_mapDTO> copyPi_package_item_mapDTOByitemTypeId(long itemTypeId) {
        List<Pi_package_item_mapDTO> pi_package_item_maps = getPi_package_item_mapDTOByitemTypeId(itemTypeId);
        return pi_package_item_maps
                .stream()
                .map(dto -> clone(dto))
                .collect(Collectors.toList());
    }

    @Override
    public String getTableName() {
        return pi_package_item_mapDAO.getTableName();
    }
}


