package pi_package_item_map;

import java.util.*;

import util.*;


public class Pi_package_item_mapDTO extends CommonDTO {
    public long fiscalYearId = -1;
    public boolean hasLot = false;
    public long piPackageFinalId = -1;
    public long piLotFinalId = -1;
    public String insertedBy = "";
    public String modifiedBy = "";
    public long insertionDate = -1;

    public List<PiPackageItemMapChildDTO> piPackageItemMapChildDTOList = new ArrayList<>();

    @Override
    public String toString() {
        return "$Pi_package_item_mapDTO[" +
                " iD = " + iD +
                " fiscalYearId = " + fiscalYearId +
                " piPackageFinalId = " + piPackageFinalId +
                " piLotFinalId = " + piLotFinalId +
                " searchColumn = " + searchColumn +
                " insertedBy = " + insertedBy +
                " modifiedBy = " + modifiedBy +
                " insertionDate = " + insertionDate +
                " isDeleted = " + isDeleted +
                " lastModificationTime = " + lastModificationTime +
                "]";
    }
}