package pi_package_item_map;

import com.google.gson.Gson;
import common.ApiResponse;
import common.BaseServlet;
import fiscal_year.Fiscal_yearDAO;
import fiscal_year.Fiscal_yearDTO;
import language.LC;
import language.LM;
import office_units.Office_unitsRepository;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import pb.ErrorMessage;
import pb.Utils;
import permission.MenuConstants;
import pi_annual_demand.PiAnnualDemandChildDAO;
import pi_annual_demand.PiAnnualDemandChildDTO;
import pi_annual_demand.PiAnnualDemandChildRepository;
import pi_app_request_details.Pi_app_request_detailsDAO;
import pi_app_request_details.Pi_app_request_detailsDTO;
import pi_package_final.*;
import sessionmanager.SessionConstants;
import user.UserDTO;
import util.CommonDTO;
import util.CommonLoginData;
import util.HttpRequestUtils;
import util.UtilCharacter;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@WebServlet("/Pi_package_item_mapServlet")
@MultipartConfig
public class Pi_package_item_mapServlet extends BaseServlet {
    private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Pi_package_item_mapServlet.class);

    @Override
    public String getTableName() {
        return Pi_package_item_mapDAO.getInstance().getTableName();
    }

    @Override
    public String getServletName() {
        return "Pi_package_item_mapServlet";
    }

    @Override
    public Pi_package_item_mapDAO getCommonDAOService() {
        return Pi_package_item_mapDAO.getInstance();
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[]{MenuConstants.PI_PACKAGE_ITEM_MAP_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[]{MenuConstants.PI_PACKAGE_ITEM_MAP_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[]{MenuConstants.PI_PACKAGE_ITEM_MAP_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Pi_package_item_mapServlet.class;
    }

    PiPackageItemMapChildDAO piPackageItemMapChildDAO = PiPackageItemMapChildDAO.getInstance();
    private final Gson gson = new Gson();

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        CommonLoginData commonLoginData = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get();
        String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
        init(request);
        try {
            switch (request.getParameter("actionType")) {
                case "getItemListByPackageLot":
                    if (Utils.checkPermission(commonLoginData.userDTO, getAddPageMenuConstants()) && getAddPagePermission(request)) {
                        String fiscalYearIdStr = request.getParameter("fiscalYearId");
                        Fiscal_yearDAO fiscalYearDAO = new Fiscal_yearDAO();
                        Fiscal_yearDTO currentFiscalYear = fiscalYearIdStr==null ?
                                fiscalYearDAO.getFiscalYearBYDateLong(System.currentTimeMillis()) :
                                fiscalYearDAO.getDTOByID(Long.parseLong(fiscalYearIdStr));

                        List<Pi_package_item_mapDTO> models = Pi_package_item_mapRepository.getInstance()
                                .getPi_package_item_mapList()
                                .stream().filter(model -> model.fiscalYearId == currentFiscalYear.id).collect(Collectors.toList());

                        List<FinalPackageLotModel> packageLotModels = new ArrayList<>();
                        int serial = 1;
                        for (Pi_package_item_mapDTO model : models) {
                            FinalPackageLotModel newModel = new FinalPackageLotModel();
                            newModel.piPackageItemMapId = model.iD;
                            newModel.serialNumber = serial++;
                            newModel.fiscalYearId = model.fiscalYearId;
                            newModel.fiscalYearEn = currentFiscalYear.nameEn;
                            newModel.fiscalYearBn = currentFiscalYear.nameBn;
                            newModel.piPackageFinalId = model.piPackageFinalId;
                            newModel.piLotFinalId = model.piLotFinalId;
                            Pi_package_finalDTO finalPackage = Pi_package_finalRepository.getInstance().getPi_package_finalDTOByiD(model.piPackageFinalId);
                            PiPackageLotFinalDTO finalLot = PiPackageLotFinalRepository.getInstance().getPiPackageLotFinalDTOByiD(model.piLotFinalId);
                            newModel.packageNumberEn = finalPackage.packageNumberEn;
                            newModel.packageNumberBn = finalPackage.packageNumberBn;
                            newModel.packageNameEn = finalPackage.packageNameEn;
                            newModel.packageNameBn = finalPackage.packageNameBn;
                            newModel.lotNumberEn = finalLot == null ? "" : finalLot.lotNumberEn;
                            newModel.lotNumberBn = finalLot == null ? "" : finalLot.lotNumberBn;
                            newModel.lotNameEn = finalLot == null ? "" : finalLot.lotNameEn;
                            newModel.lotNameBn = finalLot == null ? "" : finalLot.lotNameBn;
                            newModel.descriptionEn = finalLot == null ? "" : finalLot.lotNameEn;
                            newModel.descriptionBn = finalLot == null ? "" : finalLot.lotNameBn;
                            List<PiPackageItemMapChildDTO> childModels = PiPackageItemMapChildRepository.getInstance()
                                    .getPiPackageItemMapChildDTOBypiPackageItemMapId(model.iD);
                            newModel.quantityEn = getItemsQuantity("English", newModel.piPackageItemMapId);
                            newModel.quantityBn = getItemsQuantity("Bangla", newModel.piPackageItemMapId);
                            newModel.estCost = childModels == null ? 0 :
                                    childModels.stream().map(c -> c.itemTotalPrice).mapToLong(Long::longValue).sum();
                            newModel.estCostText = getItemsEstCost(Language, newModel.piPackageItemMapId);
                            long totalPrice;
                            if (childModels == null) totalPrice = 0;
                            else
                                totalPrice = childModels.stream().map(dto -> dto.itemTotalPrice).mapToLong(Long::longValue).sum();
                            newModel.totalPrice = totalPrice;
                            packageLotModels.add(newModel);
                        }
                        ApiResponse.sendSuccessResponse(response, gson.toJson(packageLotModels));
                    }
                    break;
                case "deletePackageItemMapWithValidation":
                    if (getDeletePermission(request)) {
                        String packageItemMapIdStr = request.getParameter("packageItemMapId");
                        if (packageItemMapIdStr == null) {
                            UtilCharacter.throwException("ডিলিট ব্যর্থ হয়েছে!", "Delete Failed!");
                        }
                        Long packageItemMapId = Long.parseLong(packageItemMapIdStr);
                        List<Pi_app_request_detailsDTO> DTOs = (List<Pi_app_request_detailsDTO>) Pi_app_request_detailsDAO.getInstance()
                                .getDTOsByParent("pi_package_item_map_id", packageItemMapId);
                        if (DTOs != null && DTOs.size() == 0) {
                            Pi_package_item_mapDAO.getInstance().delete(commonLoginData.userDTO.employee_record_id, packageItemMapId);
                            String successMessage = UtilCharacter.getDataByLanguage(Language, "ডিলিট সফল হয়েছে", "Delete Successful");
                            ApiResponse.sendSuccessResponse(response, successMessage);
                        } else {
                            String errorMessage = UtilCharacter.getDataByLanguage(Language, "ডিলিট করা যাবে না। কারণ বার্ষিক ক্রয় পরিকল্পনায় ব্যবহার করা হচ্ছে!",
                                    "Can not be deleted. Because this is used in annual purchase plan item map!");
                            ApiResponse.sendErrorResponse(response, errorMessage);
                        }
                    }
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error(ex);
        }
        super.doGet(request, response);
    }


    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        request.setAttribute("failureMessage", "");
        logger.debug("%%%% addPi_package_item_map");
        String language = userDTO.languageID == SessionConstants.ENGLISH ? "English" : "Bangla";
        Pi_package_item_mapDTO pi_package_item_mapDTO;
        SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
        String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
        boolean isLanEng = Language.equalsIgnoreCase("English");

        if (addFlag) {
            pi_package_item_mapDTO = new Pi_package_item_mapDTO();
            pi_package_item_mapDTO.insertionDate = pi_package_item_mapDTO.lastModificationTime = System.currentTimeMillis();
            pi_package_item_mapDTO.insertedBy = pi_package_item_mapDTO.modifiedBy = String.valueOf(userDTO.ID);
        } else {
            pi_package_item_mapDTO = Pi_package_item_mapDAO.getInstance().getDTOFromID(Long.parseLong(request.getParameter("iD")));
            if (pi_package_item_mapDTO == null) {
                throw new Exception(isLanEng ? "Information is not found" : "তথ্য খুঁজে পাওয়া যায়নি");
            }
            pi_package_item_mapDTO.lastModificationTime = System.currentTimeMillis();
            pi_package_item_mapDTO.modifiedBy = String.valueOf(userDTO.ID);
        }

        String Value = "";

        if (addFlag) {
            Value = request.getParameter("fiscalYear");
            if (isValueValid(Value)) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
                pi_package_item_mapDTO.fiscalYearId = Long.parseLong(Value);
            } else {
                throw new Exception(isLanEng ? "Please select fiscal year" : "অনুগ্রহপূর্বক অর্থবছর বাছাই করুন");
            }

            Value = request.getParameter("piPackageFinalId");
            if (isValueValid(Value)) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
                pi_package_item_mapDTO.piPackageFinalId = Long.parseLong(Value);
                validatePackage(pi_package_item_mapDTO.fiscalYearId, pi_package_item_mapDTO.piPackageFinalId, Language);
            } else {
                throw new Exception(isLanEng ? "Please select a package" : "অনুগ্রহপূর্বক প্যাকেজ বাছাই করুন");
            }


            Value = request.getParameter("piLotFinalId");
            if (isValueValid(Value)) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
                pi_package_item_mapDTO.piLotFinalId = Long.parseLong(Value);
                pi_package_item_mapDTO.hasLot = true;
                validateLot(pi_package_item_mapDTO.fiscalYearId, pi_package_item_mapDTO.piLotFinalId, Language);
            } else {
                validateIfLotExistInsideThisPackage(pi_package_item_mapDTO.piPackageFinalId, Language);
                pi_package_item_mapDTO.piLotFinalId = -1;
            }
        }

        long returnedID = -1;
        if (addFlag) {
            returnedID = Pi_package_item_mapDAO.getInstance().add(pi_package_item_mapDTO);
            pi_package_item_mapDTO.iD = returnedID;
        } else {
            returnedID = Pi_package_item_mapDAO.getInstance().update(pi_package_item_mapDTO);
        }
        List<PiPackageItemMapChildDTO> newModels = createPiPackageItemMapChildDTOListByRequest(request, language, pi_package_item_mapDTO);

        List<PiPackageItemMapChildDTO> oldModels = (List<PiPackageItemMapChildDTO>) PiPackageItemMapChildDAO.getInstance()
                .getDTOsByParent("pi_package_item_map_id", pi_package_item_mapDTO.iD);
        if (newModels == null) return null;
        // IDS WITH -1 ARE FOR ADD
        List<PiPackageItemMapChildDTO> modelsToAdd = newModels.stream().filter(dto -> dto.iD == -1).collect(Collectors.toList());
        // COMMON DTOS ARE FOR EDIT
        List<PiPackageItemMapChildDTO> modelsToEdit = newModels.stream().filter(dto -> fieldContains(oldModels, dto.iD)).collect(Collectors.toList());
        // IDS NOT COMMON AND NOT -1 ARE FOR DELETE
        List<PiPackageItemMapChildDTO> modelsToDelete = oldModels.stream().filter(dto -> !fieldContains(modelsToEdit, dto.iD)).collect(Collectors.toList());

        for (PiPackageItemMapChildDTO model : modelsToAdd) {
            model.insertedBy = model.modifiedBy = String.valueOf(userDTO.employee_record_id);
            model.insertionDate = model.lastModificationTime = System.currentTimeMillis();
            PiPackageItemMapChildDAO.getInstance().add(model);
            PiAnnualDemandChildDTO annualDemandChildDTO = PiAnnualDemandChildRepository.getInstance()
                    .getPiAnnualDemandChildDTOByiD(model.annualDemandChildId);
            annualDemandChildDTO.piPackageFinalId = model.piPackageFinalId;
            annualDemandChildDTO.piLotFinalId = model.piLotFinalId;
            annualDemandChildDTO.lastModificationTime = System.currentTimeMillis();
            PiAnnualDemandChildDAO.getInstance().update(annualDemandChildDTO);
        }
        for (PiPackageItemMapChildDTO model : modelsToEdit) {
            model.modifiedBy = String.valueOf(userDTO.employee_record_id);
            model.lastModificationTime = System.currentTimeMillis();
            PiPackageItemMapChildDAO.getInstance().update(model);
        }
        for (PiPackageItemMapChildDTO model : modelsToDelete) {
            PiPackageItemMapChildDAO.getInstance().delete(userDTO.employee_record_id, model.iD, System.currentTimeMillis());
            PiAnnualDemandChildDTO annualDemandChildDTO = PiAnnualDemandChildDAO.getInstance()
                    .getDTOByID(model.annualDemandChildId);
            annualDemandChildDTO.piPackageFinalId = -1;
            annualDemandChildDTO.piLotFinalId = -1;
            annualDemandChildDTO.lastModificationTime = System.currentTimeMillis();
            PiAnnualDemandChildDAO.getInstance().update(annualDemandChildDTO);
        }

        return pi_package_item_mapDTO;
    }

    private List<PiPackageItemMapChildDTO> createPiPackageItemMapChildDTOListByRequest(HttpServletRequest request,
                                                                                       String language, Pi_package_item_mapDTO parentModel) throws Exception {
        List<PiPackageItemMapChildDTO> childList = new ArrayList<>();
        if (request.getParameterValues("packageItemMapChild.iD") != null) {
            int count = request.getParameterValues("packageItemMapChild.iD").length;

            for (int index = 0; index < count; index++) {
                PiPackageItemMapChildDTO model = createPiPackageItemMapChildDTOByRequestAndIndex(request, true, index, language);
                model.piPackageItemMapId = parentModel.iD;
                model.piPackageFinalId = parentModel.piPackageFinalId;
                model.piLotFinalId = parentModel.piLotFinalId;
                model.fiscalYearId = parentModel.fiscalYearId;
                childList.add(model);
            }
        }
        return childList;
    }

    private PiPackageItemMapChildDTO createPiPackageItemMapChildDTOByRequestAndIndex(HttpServletRequest request, boolean addFlag, int index,
                                                                                     String language) throws Exception {
        PiPackageItemMapChildDTO piPackageItemMapChildDTO = new PiPackageItemMapChildDTO();


        String Value = "";
        Value = request.getParameterValues("packageItemMapChild.iD")[index];
        if (isValueValid(Value)) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
            piPackageItemMapChildDTO.iD = Long.parseLong(Value);
        } else {
            piPackageItemMapChildDTO.iD = -1;
        }


        Value = request.getParameterValues("packageItemMapChild.annualDemandID")[index];
        if (isValueValid(Value)) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
            piPackageItemMapChildDTO.annualDemandId = Long.parseLong(Value);
        } else {
            piPackageItemMapChildDTO.annualDemandId = -1;
        }


        Value = request.getParameterValues("packageItemMapChild.annualDemandChildID")[index];
        if (isValueValid(Value)) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
            piPackageItemMapChildDTO.annualDemandChildId = Long.parseLong(Value);
        } else {
            piPackageItemMapChildDTO.annualDemandChildId = -1;
        }


        Value = request.getParameterValues("packageItemMapChild.officeUnitId")[index];
        if (isValueValid(Value)) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
            piPackageItemMapChildDTO.officeUnitId = Long.parseLong(Value);
        } else {
            piPackageItemMapChildDTO.officeUnitId = -1;
        }


        Value = request.getParameterValues("packageItemMapChild.piItemGroupId")[index];
        if (isValueValid(Value)) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        } else {
            throw new Exception(LM.getText(LC.PI_PACKAGE_ITEM_MAP_ADD_PI_PACKAGE_ITEM_MAP_CHILD_ITEMID, language) + " " + ErrorMessage.getInvalidMessage(language));
        }
        piPackageItemMapChildDTO.itemGroupId = Long.parseLong(Value);


        Value = request.getParameterValues("packageItemMapChild.piItemTypeId")[index];
        if (isValueValid(Value)) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        } else {
            throw new Exception(LM.getText(LC.PI_PACKAGE_ITEM_MAP_ADD_PI_PACKAGE_ITEM_MAP_CHILD_ITEMID, language) + " " + ErrorMessage.getInvalidMessage(language));
        }
        piPackageItemMapChildDTO.itemTypeId = Long.parseLong(Value);


        Value = request.getParameterValues("packageItemMapChild.piItemId")[index];
        if (isValueValid(Value)) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        } else {
            throw new Exception(LM.getText(LC.PI_PACKAGE_ITEM_MAP_ADD_PI_PACKAGE_ITEM_MAP_CHILD_ITEMID, language) + " " + ErrorMessage.getInvalidMessage(language));
        }
        piPackageItemMapChildDTO.itemId = Long.parseLong(Value);


        Value = request.getParameterValues("packageItemMapChild.piUnitId")[index];
        if (isValueValid(Value)) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        } else {
            throw new Exception(LM.getText(LC.PI_PACKAGE_ITEM_MAP_ADD_PI_PACKAGE_ITEM_MAP_CHILD_PIUNITID, language) + " " + ErrorMessage.getInvalidMessage(language));
        }
        piPackageItemMapChildDTO.piUnitId = Long.parseLong(Value);


        Value = request.getParameterValues("packageItemMapChild.serialNumber")[index];
        if (isValueValid(Value)) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        } else {
            throw new Exception(UtilCharacter.getDataByLanguage(language, "আইটেম গুলোর সিরিয়াল নাম্বার লিখুন", "Please write serial number of the items"));
        }
        piPackageItemMapChildDTO.serialNumber = Long.parseLong(Value);


        Value = request.getParameterValues("packageItemMapChild.itemDescription")[index];
        if (isValueValid(Value)) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
            piPackageItemMapChildDTO.description = Value;
        }


        Value = request.getParameterValues("packageItemMapChild.itemQuantity")[index];
        if (isValueValid(Value)) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        } else {
            throw new Exception(UtilCharacter.getDataByLanguage(language, "আইটেম গুলোর পরিমাণ লিখুন", "Please write amount number of the items"));
        }
        piPackageItemMapChildDTO.itemQuantity = Long.parseLong(Value);


        Value = request.getParameterValues("packageItemMapChild.itemUnitPrice")[index];
        if (isValueValid(Value)) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        } else {
            throw new Exception(UtilCharacter.getDataByLanguage(language, "আইটেম গুলোর একক দর লিখুন", "Please write unit price of the items"));
        }
        piPackageItemMapChildDTO.itemUnitPrice = Long.parseLong(Value);


        Value = request.getParameterValues("packageItemMapChild.itemTotalPrice")[index];
        if (isValueValid(Value)) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        } else {
            throw new Exception(UtilCharacter.getDataByLanguage(language, "আইটেম গুলোর মোট দর লিখুন", "Please write total price of the items"));
        }
        piPackageItemMapChildDTO.itemTotalPrice = Long.parseLong(Value);


        return piPackageItemMapChildDTO;
    }

    private boolean isValueValid(String value) {
        return value != null && !value.equals("");
    }

    private boolean fieldContains(List<PiPackageItemMapChildDTO> childDTOS, Long id) {
        List<PiPackageItemMapChildDTO> dto = childDTOS.stream().filter(model -> model.iD == id).collect(Collectors.toList());
        return dto.size() > 0;
    }

    private String getItemsQuantity(String Language, Long packageItemMapId) {
        List<PiPackageItemMapChildDTO> models = (List<PiPackageItemMapChildDTO>) PiPackageItemMapChildDAO.getInstance()
                .getDTOsByParent("pi_package_item_map_id", packageItemMapId);
        HashMap<Long, Long> officeItemCount = new HashMap<Long, Long>();
        for (PiPackageItemMapChildDTO model : models) {
            if (officeItemCount.containsKey(model.officeUnitId)) {
                long prevCount = officeItemCount.get(model.officeUnitId);
                officeItemCount.put(model.officeUnitId, prevCount + 1);
            } else {
                officeItemCount.put(model.officeUnitId, 1l);
            }
        }

        StringBuilder quantity = new StringBuilder();

        for (Map.Entry<Long, Long> entity : officeItemCount.entrySet()) {
            quantity.append(Office_unitsRepository.getInstance().geText(Language, entity.getKey()))
                    .append(" - ")
                    .append(Utils.getDigits(entity.getValue(), Language))
                    .append(UtilCharacter.getDataByLanguage(Language, " টি", " Piece"))
                    .append("\n");
        }
        return quantity.toString();
    }

    private String getItemsEstCost(String Language, Long packageItemMapId) {
        List<PiPackageItemMapChildDTO> models = (List<PiPackageItemMapChildDTO>) PiPackageItemMapChildDAO.getInstance()
                .getDTOsByParent("pi_package_item_map_id", packageItemMapId);
        HashMap<Long, Long> officeItemCount = new HashMap<Long, Long>();
        long grandTotal = 0;
        for (PiPackageItemMapChildDTO model : models) {
            if (officeItemCount.containsKey(model.officeUnitId)) {
                long prevCount = officeItemCount.get(model.officeUnitId);
                officeItemCount.put(model.officeUnitId, prevCount + model.itemTotalPrice);
            } else {
                officeItemCount.put(model.officeUnitId, model.itemTotalPrice);
            }
            grandTotal += model.itemTotalPrice;
        }

        StringBuilder estCost = new StringBuilder();

        for (Map.Entry<Long, Long> entity : officeItemCount.entrySet()) {
            estCost.append(Utils.getDigits(entity.getValue(), Language)).append("\n");
        }
        estCost.append("-------------------\n");
        estCost.append(Utils.getDigits(grandTotal, Language));
        return estCost.toString();
    }

    private void validatePackage(long fiscalYear, long packageId, String Language) throws Exception {
        // IF ANY PACKAGE HAS LOT, THEN LOT VALIDATION WILL BE EXECUTED
        List<PiPackageLotFinalDTO> lotDTOs = PiPackageLotFinalDAO.getInstance().getLotsByPackageId(packageId);
        if (lotDTOs != null && !lotDTOs.isEmpty()) {
            return;
        }

        List<Pi_package_item_mapDTO> itemMapDTOs = Pi_package_item_mapDAO.getInstance()
                .getPackageItemMapsByFiscalYearAndPackage(fiscalYear, packageId);
        // THE PACKAGE IS NOT VALID, IF THIS IS ALREADY USED IN THIS FISCAL YEAR
        if (!(itemMapDTOs == null || itemMapDTOs.isEmpty())) {
            throw new Exception(UtilCharacter
                    .getDataByLanguage(Language, "এই প্যাকেজটি এই অর্থবছরে ইতিমধ্যে ব্যাবহৃত হচ্ছে!",
                            "This package is already used in this fiscal year!"));
        }
    }

    private void validateLot(long fiscalYear, long lotId, String Language) throws Exception {
        List<Pi_package_item_mapDTO> itemMapDTOs = Pi_package_item_mapDAO.getInstance()
                .getPackageItemMapsByFiscalYearAndLot(fiscalYear, lotId);
        // THE LOT IS NOT VALID, IF THIS IS ALREADY USED IN THIS FISCAL YEAR
        if (!(itemMapDTOs == null || itemMapDTOs.isEmpty())) {
            throw new Exception(UtilCharacter
                    .getDataByLanguage(Language, "এই লট এই অর্থবছরে ইতিমধ্যে ব্যাবহৃত হচ্ছে!",
                            "This lot is already used in this fiscal year!"));
        }
    }

    private void validateIfLotExistInsideThisPackage(long packageId, String Language) throws Exception {
        List<PiPackageLotFinalDTO> lotDTOs = PiPackageLotFinalDAO.getInstance().getLotsByPackageId(packageId);
        if (lotDTOs != null && !lotDTOs.isEmpty()) {
            throw new Exception(UtilCharacter
                    .getDataByLanguage(Language, "অনুগ্রহপূর্বক লট নির্বাচন করুন!", "Please select lot!"));
        }
    }
}

