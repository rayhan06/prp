package pi_dashboard;

import java.util.List;

public class PiDashboardModel {
    public List<HelperDTO> lastSixMonthPurchaseAmount;
    public List<HelperDTO> lastSixMonthTenderAmount;
    public List<HelperDTO> reqStatus;
    public List<HelperDTO> topFiveItems;
}
