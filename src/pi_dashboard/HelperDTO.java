package pi_dashboard;

public class HelperDTO {
    public String month = "";
    public double amount = 0.0;
    public long count = 0;
    public String countInString = "";
    public String redirectStatus = "";
}
