package pi_dashboard;

import com.google.gson.Gson;
import dashboard.DashboardDTO;
import login.LoginDTO;
import org.apache.log4j.Logger;
import pb.Utils;
import pi_package_vendor.Pi_package_vendorDAO;
import pi_package_vendor.Pi_package_vendorDTO;
import pi_package_vendor_items.Pi_package_vendor_itemsDAO;
import pi_package_vendor_items.Pi_package_vendor_itemsDTO;
import pi_purchase.Pi_purchaseDAO;
import pi_purchase.Pi_purchaseDTO;
import pi_requisition.Pi_requisitionDAO;
import pi_requisition.Pi_requisitionDTO;
import procurement_goods.Procurement_goodsDTO;
import procurement_goods.Procurement_goodsRepository;
import util.HttpRequestUtils;
import util.TimeConverter;
import util.UtilCharacter;
import vm_requisition.CommonApprovalStatus;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;
import java.util.stream.Collectors;


@WebServlet("/ProcurementDashboardServlet")
@MultipartConfig
public class ProcurementDashboardServlet extends HttpServlet
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(ProcurementDashboardServlet.class);
    public ProcurementDashboardServlet()
	{
        super();
    }

    @Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		logger.debug("In do get request = " + request);
		try
		{
			String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
			LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;

			String actionType = request.getParameter("actionType");
			if(actionType.equals("ajax_data")){
				long officeUnitId = Long.parseLong(request.getParameter("officeUnitId"));
				PiDashboardModel piDashboardModel = new PiDashboardModel();
				DashboardDTO dashboardDTO = new DashboardDTO();

				int START = 5;
				int END = 1;
				long startTime = TimeConverter.get1stDayOfNthtMonth(-START);
				long endTime = TimeConverter.get1stDayOfNthtMonth(END);

				// Last 6 month purchase cost
				List<Pi_purchaseDTO> purchaseDTOS = Pi_purchaseDAO.getInstance().getByStartAndEndTime
						(officeUnitId, startTime, endTime);
				List<HelperDTO> purchaseHelperDTOs = new ArrayList<>();
				for(int i = START; i > -END; i-- ){
					long sTime = TimeConverter.get1stDayOfNthtMonth(-i);
					long eTime = TimeConverter.get1stDayOfNthtMonth(-i + 1);
					HelperDTO helperDTO = new HelperDTO();
					helperDTO.amount = purchaseDTOS.stream()
							.filter(j -> j.purchaseDate >= sTime && j.purchaseDate < eTime)
							.mapToDouble(j -> j.totalBill).sum();
					helperDTO.month = Utils.getDigits(dashboardDTO.last6Months[START - i + 1], Language);
					purchaseHelperDTOs.add(helperDTO);
				}
				piDashboardModel.lastSixMonthPurchaseAmount = purchaseHelperDTOs;


				// Last 6 month tender count

				List<Pi_package_vendorDTO> vendorDTOS = Pi_package_vendorDAO.getInstance().getByStartAndEndTime
						(officeUnitId, startTime, endTime);
				List<HelperDTO> vendorHelperDTOs = new ArrayList<>();
				for(int i = START; i > -END; i-- ){
					long sTime = TimeConverter.get1stDayOfNthtMonth(-i);
					long eTime = TimeConverter.get1stDayOfNthtMonth(-i + 1);
					HelperDTO helperDTO = new HelperDTO();
					helperDTO.count = vendorDTOS.stream()
							.filter(j -> j.agreementDate >= sTime && j.agreementDate < eTime)
							.count();
					helperDTO.month = Utils.getDigits(dashboardDTO.last6Months[START - i + 1], Language);
					vendorHelperDTOs.add(helperDTO);
				}
				piDashboardModel.lastSixMonthTenderAmount = vendorHelperDTOs;

				// status wise requisition count
				List<Pi_requisitionDTO> requisitionDTOS = Pi_requisitionDAO.getInstance().
						getCountByStatus(officeUnitId);

				List<Integer> rejectedStatus = Arrays.asList(CommonApprovalStatus.REQUISITION_REJECTED_BY_FIRST_APPROVER.getValue(),
						CommonApprovalStatus.REQUISITION_REJECTED_BY_SECOND_APPROVER.getValue());

				int totalCount = 0;

				List<HelperDTO> reqHelperDTOs = new ArrayList<>();
				HelperDTO helperDTO = new HelperDTO();
				helperDTO.month = UtilCharacter.getDataByLanguage(Language, "চলমান",
						"Pending");
				helperDTO.count = requisitionDTOS.stream()
						.filter(i -> i.status == CommonApprovalStatus.REQUISITION_NOT_APPROVED.getValue())
						.mapToInt(i -> i.count).sum();
				totalCount += helperDTO.count;
				helperDTO.countInString = Utils.getDigits(helperDTO.count, Language);
				helperDTO.redirectStatus = "&filter=pending";
				reqHelperDTOs.add(helperDTO);


				helperDTO = new HelperDTO();
				helperDTO.month = UtilCharacter.getDataByLanguage(Language, "দ্বিতীয় অনুমোদন পেন্ডিং",
						"Second Approval Pending");
				helperDTO.count = requisitionDTOS.stream()
						.filter(i -> i.status == CommonApprovalStatus.REQUISITION_APPROVED_BY_FIRST_APPROVER.getValue())
						.mapToInt(i -> i.count).sum();
				totalCount += helperDTO.count;
				helperDTO.countInString = Utils.getDigits(helperDTO.count, Language);
				helperDTO.redirectStatus = "&filter=second_pending";
				reqHelperDTOs.add(helperDTO);

				helperDTO = new HelperDTO();
				helperDTO.month = UtilCharacter.getDataByLanguage(Language, "বাতিলকৃত",
						"Rejected");
				helperDTO.count = requisitionDTOS.stream()
						.filter(i -> rejectedStatus.contains(i.status))
						.mapToInt(i -> i.count).sum();
				totalCount += helperDTO.count;
				helperDTO.redirectStatus = "&filter=rejected";
				helperDTO.countInString = Utils.getDigits(helperDTO.count, Language);
				reqHelperDTOs.add(helperDTO);

				helperDTO = new HelperDTO();
				helperDTO.month = UtilCharacter.getDataByLanguage(Language, "অনুমোদিত",
						"Approved");
				helperDTO.count = requisitionDTOS.stream()
						.filter(i -> i.status == CommonApprovalStatus.REQUISITION_APPROVED_BY_SECOND_APPROVER.getValue())
						.mapToInt(i -> i.count).sum();
				totalCount += helperDTO.count;
				helperDTO.countInString = Utils.getDigits(helperDTO.count, Language);
				helperDTO.redirectStatus = "&filter=approved";
				reqHelperDTOs.add(helperDTO);


				helperDTO = new HelperDTO();
				helperDTO.month = CommonApprovalStatus.getRequisitionText
						(CommonApprovalStatus.REQUISITION_SUPPLIED_BY_STORE_KEEPER.getValue(), Language);
				helperDTO.count = requisitionDTOS.stream()
						.filter(i -> i.status == CommonApprovalStatus.REQUISITION_SUPPLIED_BY_STORE_KEEPER.getValue())
						.mapToInt(i -> i.count).sum();
				totalCount += helperDTO.count;
				helperDTO.countInString = Utils.getDigits(helperDTO.count, Language);
				helperDTO.redirectStatus = "&filter=supplied";
				reqHelperDTOs.add(helperDTO);



				helperDTO = new HelperDTO();
				helperDTO.month = UtilCharacter.getDataByLanguage(Language, "সর্বমোট",
						"Total");
				helperDTO.count = totalCount;
				helperDTO.countInString = Utils.getDigits(helperDTO.count, Language);
				reqHelperDTOs.add(helperDTO);

				piDashboardModel.reqStatus = reqHelperDTOs;


				// top 5 item

				startTime = TimeConverter.get1stDayOfNthtMonth(-(START + 6));
				vendorDTOS = Pi_package_vendorDAO.getInstance().getByStartAndEndTime(officeUnitId, startTime, endTime);
				List<Long> parentIds = vendorDTOS.stream().map(i -> i.iD).collect(Collectors.toList());
				List<Pi_package_vendor_itemsDTO> itemsDTOS = Pi_package_vendor_itemsDAO.getInstance().
						getWinningTopFiveItem(parentIds);

				List<HelperDTO> itemHelperDTOs = new ArrayList<>();
				for(Pi_package_vendor_itemsDTO itemsDTO: itemsDTOS){
					helperDTO = new HelperDTO();
					Procurement_goodsDTO procurementGoodsDTO = Procurement_goodsRepository.getInstance().
							getProcurement_goodsDTOByID(itemsDTO.productId);
					if(procurementGoodsDTO != null){
						helperDTO.month = UtilCharacter.getDataByLanguage(Language,
								procurementGoodsDTO.nameBn, procurementGoodsDTO.nameEn);
					}
					helperDTO.amount = itemsDTO.totalBill;
					itemHelperDTOs.add(helperDTO);

				}

				piDashboardModel.topFiveItems = itemHelperDTOs;






				String data = "";
				data = new Gson().toJson(piDashboardModel);
				response.setContentType("application/json");
				PrintWriter out = response.getWriter();
				out.print(data);
				out.flush();
				out.close();
			}
			else
			{
				request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}





	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{

	}






}

