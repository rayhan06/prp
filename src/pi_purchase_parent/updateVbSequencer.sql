update prp.vbSequencer set next_id = (SELECT max(id) + 1 FROM prp.pi_app_request) where table_name = 'pi_app_request';
update prp.vbSequencer set next_id = (SELECT max(id) + 1 FROM prp.pi_app_request_details) where table_name = 'pi_app_request_details';
update prp.vbSequencer set next_id = (SELECT max(id) + 1 FROM prp.pi_auction) where table_name = 'pi_auction';
update prp.vbSequencer set next_id = (SELECT max(id) + 1 FROM prp.pi_auction_items) where table_name = 'pi_auction_items';
update prp.vbSequencer set next_id = (SELECT max(id) + 1 FROM prp.pi_office_unit_store_keeper_mapping) where table_name = 'pi_office_unit_store_keeper_mapping';
update prp.vbSequencer set next_id = (SELECT max(id) + 1 FROM prp.pi_package_auctioneer) where table_name = 'pi_package_auctioneer';
update prp.vbSequencer set next_id = (SELECT max(id) + 1 FROM prp.pi_package_auctioneer_children) where table_name = 'pi_package_auctioneer_children';
update prp.vbSequencer set next_id = (SELECT max(id) + 1 FROM prp.pi_package_auctioneer_items) where table_name = 'pi_package_auctioneer_items';
update prp.vbSequencer set next_id = (SELECT max(id) + 1 FROM prp.pi_package_vendor) where table_name = 'pi_package_vendor';
update prp.vbSequencer set next_id = (SELECT max(id) + 1 FROM prp.pi_package_vendor_children) where table_name = 'pi_package_vendor_children';
update prp.vbSequencer set next_id = (SELECT max(id) + 1 FROM prp.pi_package_vendor_items) where table_name = 'pi_package_vendor_items';
update prp.vbSequencer set next_id = (SELECT max(id) + 1 FROM prp.pi_purchase) where table_name = 'pi_purchase';
update prp.vbSequencer set next_id = (SELECT max(id) + 1 FROM prp.pi_purchase_parent) where table_name = 'pi_purchase_parent';
update prp.vbSequencer set next_id = (SELECT max(id) + 1 FROM prp.pi_requisition) where table_name = 'pi_requisition';
update prp.vbSequencer set next_id = (SELECT max(id) + 1 FROM prp.pi_requisition_item) where table_name = 'pi_requisition_item';
update prp.vbSequencer set next_id = (SELECT max(id) + 1 FROM prp.pi_return) where table_name = 'pi_return';
update prp.vbSequencer set next_id = (SELECT max(id) + 1 FROM prp.pi_return_item) where table_name = 'pi_return_item';
update prp.vbSequencer set next_id = (SELECT max(id) + 1 FROM prp.pi_unique_item) where table_name = 'pi_unique_item';
update prp.vbSequencer set next_id = (SELECT max(id) + 1 FROM prp.pi_unique_item_assignment) where table_name = 'pi_unique_item_assignment';
update prp.vbSequencer set next_id = (SELECT max(id) + 1 FROM prp.pi_unit) where table_name = 'pi_unit';
update prp.vbSequencer set next_id = (SELECT max(id) + 1 FROM prp.pi_vendor_auctioneer_details) where table_name = 'pi_vendor_auctioneer_details';
update prp.vbSequencer set next_id = (SELECT max(id) + 1 FROM prp.procurement_goods) where table_name = 'procurement_goods';
update prp.vbSequencer set next_id = (SELECT max(id) + 1 FROM prp.procurement_goods_type) where table_name = 'procurement_goods_type';
update prp.vbSequencer set next_id = (SELECT max(id) + 1 FROM prp.procurement_package) where table_name = 'procurement_package';