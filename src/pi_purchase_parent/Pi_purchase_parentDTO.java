package pi_purchase_parent;
import java.util.*;

import pb.CommonDAO;
import util.*;


public class Pi_purchase_parentDTO extends CommonDTO
{

	public long officeUnitId = -1;
	public long fiscalYearId = -1;
	public long packageId = -1;
	public long lotId = -1;
	public long vendorId = -1;
    public String sarokNumber = "";
	public String purchaseOrderNumber = "";
	public long purchaseDate = System.currentTimeMillis();
	public String purchaseTime = TimeUtils.DATE_FORMAT_TIME_AM_PM.format(new Date());
	public long filesDropzone = -1;
	public long insertedBy = -1;
	public long insertionTime = -1;
	public long modifiedBy = -1;

	public String assignedOfcUnitIds = "";

	public int procurementMethod = -1;
	public long piPackageNewId = -1;



	@Override
	public String toString() {
            return "$Pi_purchase_parentDTO[" +
            " iD = " + iD +
            " officeUnitId = " + officeUnitId +
            " fiscalYearId = " + fiscalYearId +
            " packageId = " + packageId +
            " vendorId = " + vendorId +
            " sarokNumber = " + sarokNumber +
            " purchaseDate = " + purchaseDate +
            " remarks = " + remarks +
            " filesDropzone = " + filesDropzone +
            " insertedBy = " + insertedBy +
            " insertionTime = " + insertionTime +
            " isDeleted = " + isDeleted +
            " modifiedBy = " + modifiedBy +
            " lastModificationTime = " + lastModificationTime +
			" assignedOfcUnitIds = " + assignedOfcUnitIds +
            "]";
    }

}