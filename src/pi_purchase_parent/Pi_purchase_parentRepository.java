package pi_purchase_parent;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import com.google.gson.Gson;

import repository.Repository;
import repository.RepositoryManager;


public class Pi_purchase_parentRepository implements Repository {
	Pi_purchase_parentDAO pi_purchase_parentDAO = null;
	
	static Logger logger = Logger.getLogger(Pi_purchase_parentRepository.class);
	Map<Long, Pi_purchase_parentDTO>mapOfPi_purchase_parentDTOToiD;
	Gson gson;

  
	private Pi_purchase_parentRepository(){
		pi_purchase_parentDAO = Pi_purchase_parentDAO.getInstance();
		mapOfPi_purchase_parentDTOToiD = new ConcurrentHashMap<>();
		gson = new Gson();
		RepositoryManager.getInstance().addRepository(this);
	}

	private static class LazyLoader{
        final static Pi_purchase_parentRepository INSTANCE = new Pi_purchase_parentRepository();
    }

    public static Pi_purchase_parentRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

	public void reload(boolean reloadAll){
		try {
			List<Pi_purchase_parentDTO> pi_purchase_parentDTOs = pi_purchase_parentDAO.getAllDTOs(reloadAll);
			for(Pi_purchase_parentDTO pi_purchase_parentDTO : pi_purchase_parentDTOs) {
				Pi_purchase_parentDTO oldPi_purchase_parentDTO = getPi_purchase_parentDTOByiD(pi_purchase_parentDTO.iD);
				if( oldPi_purchase_parentDTO != null ) {
					mapOfPi_purchase_parentDTOToiD.remove(oldPi_purchase_parentDTO.iD);
				
					
				}
				if(pi_purchase_parentDTO.isDeleted == 0) 
				{
					
					mapOfPi_purchase_parentDTOToiD.put(pi_purchase_parentDTO.iD, pi_purchase_parentDTO);
				
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public Pi_purchase_parentDTO clone(Pi_purchase_parentDTO dto) {
		String raw = gson.toJson(dto);
		return gson.fromJson(raw, Pi_purchase_parentDTO.class);
	}
	
	
	public List<Pi_purchase_parentDTO> getPi_purchase_parentList() {
		List <Pi_purchase_parentDTO> pi_purchase_parents = new ArrayList<Pi_purchase_parentDTO>(this.mapOfPi_purchase_parentDTOToiD.values());
		return pi_purchase_parents;
	}
	
	public List<Pi_purchase_parentDTO> copyPi_purchase_parentList() {
		List <Pi_purchase_parentDTO> pi_purchase_parents = getPi_purchase_parentList();
		return pi_purchase_parents
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}
	
	
	
	public Pi_purchase_parentDTO getPi_purchase_parentDTOByiD( long iD){
		return mapOfPi_purchase_parentDTOToiD.get(iD);
	}
	
	public Pi_purchase_parentDTO copyPi_purchase_parentDTOByiD( long iD){
		return clone(mapOfPi_purchase_parentDTOToiD.get(iD));
	}

	
	@Override
	public String getTableName() {
		return pi_purchase_parentDAO.getTableName();
	}

	public String getSarokNumber(long purchaseId){
		Pi_purchase_parentDTO dto = Pi_purchase_parentRepository.getInstance().getPi_purchase_parentDTOByiD(purchaseId);
		return dto == null ? "" : dto.sarokNumber;
	}

	public String getPurchaseOrderNumber(long purchaseId){
		Pi_purchase_parentDTO dto = Pi_purchase_parentRepository.getInstance().getPi_purchase_parentDTOByiD(purchaseId);
		return dto == null ? "" : dto.purchaseOrderNumber;
	}
}


