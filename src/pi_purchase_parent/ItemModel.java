package pi_purchase_parent;

public class ItemModel {
    public long piPurchaseId = -1;
    public long packageId = -1;
    public String packageName = "";
    public long itemTypeId = -1;
    public String itemTypeName = "";
    public long itemId = -1;
    public String itemName = "";
    public long currentStock = 0;
    public long currentReceivedCount = 0;
    public long amount = 0;
    public long purchaseCount = 0;
    public long remainingPurchaseCount = 0;
    public long unitPrice = -1;
    public long vendorId = -1;
    public String vendorName = "";
    public String remark = "";
    public long storeOfficeUnitId = -1;
    public String storeOfficeUnitName = "";
}
