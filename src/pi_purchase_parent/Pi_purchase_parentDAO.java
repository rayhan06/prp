package pi_purchase_parent;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.sql.SQLException;
import common.CommonDAOService;
import common.ConnectionAndStatementUtil;
import fiscal_year.Fiscal_yearDTO;
import fiscal_year.Fiscal_yearRepository;
import org.apache.log4j.Logger;
import pi_package_new.Pi_package_newDTO;
import pi_package_new.Pi_package_newRepository;
import pi_package_vendor_items.Pi_package_vendor_itemsDTO;
import pi_vendor_auctioneer_details.Pi_vendor_auctioneer_detailsDTO;
import pi_vendor_auctioneer_details.Pi_vendor_auctioneer_detailsRepository;
import procurement_package.Procurement_packageDTO;
import procurement_package.Procurement_packageRepository;
import util.*;
import pb.*;

public class Pi_purchase_parentDAO  implements CommonDAOService<Pi_purchase_parentDTO>
{
	
	Logger logger = Logger.getLogger(getClass());
	
	private static String addQuery = "";
	private static String updateQuery = "";
	private String[] columnNames = null;

	private static final String alreadyPurchasedByFiscalYearAndOfficeUnitAndPackageId =
			"SELECT * FROM pi_purchase_parent WHERE  fiscal_year_id = %d AND " +
					" office_unit_id = %d AND package_id = %d  AND isDeleted = 0 LIMIT 1";

	private Pi_purchase_parentDAO()
	{
		columnNames = new String[] 
		{
			"ID",
			"office_unit_id",
			"fiscal_year_type",
			"pi_package_final_id",
			"pi_lot_final_id",
			"pi_vendor_auctioneer_details_type",
			"sarok_number",
			"purchase_order_number",
			"purchase_date",
			"remarks",
			"files_dropzone",
			"inserted_by",
			"insertion_time",
			"modified_by",
			"search_column",
			"assigned_office_unit_ids",
			"procurement_method",
			"isDeleted",
			"lastModificationTime"
		};
		updateQuery = getUpdateQuery(columnNames);
        addQuery = getAddQuery(columnNames);
		
		searchMap.put("sarok_number"," and (sarok_number like ?)");
		searchMap.put("purchase_date_start"," and (purchase_date >= ?)");
		searchMap.put("purchase_date_end"," and (purchase_date <= ?)");
		searchMap.put("remarks"," and (remarks like ?)");
		searchMap.put("inserted_by"," and (inserted_by like ?)");
		searchMap.put("modified_by"," and (modified_by like ?)");
		searchMap.put("AnyField"," and (search_column like ?)");
	}

	private final Map<String,String> searchMap = new HashMap<>();

    private static class LazyLoader
    {
    	static final Pi_purchase_parentDAO INSTANCE = new Pi_purchase_parentDAO();
	}

	public static Pi_purchase_parentDAO getInstance()
	{
    	return LazyLoader.INSTANCE;
	}
	
	public void setSearchColumn(Pi_purchase_parentDTO pi_purchase_parentDTO)
	{
		pi_purchase_parentDTO.searchColumn = "";
		pi_purchase_parentDTO.searchColumn += pi_purchase_parentDTO.sarokNumber + " ";
		pi_purchase_parentDTO.searchColumn += pi_purchase_parentDTO.remarks + " ";
		pi_purchase_parentDTO.searchColumn += pi_purchase_parentDTO.insertedBy + " ";
		pi_purchase_parentDTO.searchColumn += pi_purchase_parentDTO.modifiedBy + " ";

		Fiscal_yearDTO fiscalYearDTO = Fiscal_yearRepository.getInstance().
				getFiscal_yearDTOByid(pi_purchase_parentDTO.fiscalYearId);
		if(fiscalYearDTO != null){
			pi_purchase_parentDTO.searchColumn += fiscalYearDTO.nameBn + " " + fiscalYearDTO.nameEn + " ";
		}

		Pi_package_newDTO packageDTO =  Pi_package_newRepository.getInstance().
				getPi_package_newDTOByiD(pi_purchase_parentDTO.packageId);
		if(packageDTO != null){
			pi_purchase_parentDTO.searchColumn += packageDTO.packageNameEn + " " + packageDTO.packageNameBn + " ";
		}

		Pi_vendor_auctioneer_detailsDTO vendorDTO = Pi_vendor_auctioneer_detailsRepository.getInstance().
				getPi_vendor_auctioneer_detailsDTOByiD(pi_purchase_parentDTO.vendorId);
		if(vendorDTO != null){
			pi_purchase_parentDTO.searchColumn += vendorDTO.nameBn + " " + vendorDTO.nameEn + " ";
		}

	}
	
	@Override
	public void set(PreparedStatement ps, Pi_purchase_parentDTO pi_purchase_parentDTO, boolean isInsert) throws SQLException
	{
		int index = 0;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(pi_purchase_parentDTO);
		if(isInsert)
		{
			ps.setObject(++index,pi_purchase_parentDTO.iD);
		}
		ps.setObject(++index,pi_purchase_parentDTO.officeUnitId);
		ps.setObject(++index,pi_purchase_parentDTO.fiscalYearId);
		ps.setObject(++index,pi_purchase_parentDTO.packageId);
		ps.setObject(++index,pi_purchase_parentDTO.lotId);
		ps.setObject(++index,pi_purchase_parentDTO.vendorId);
		ps.setObject(++index,pi_purchase_parentDTO.sarokNumber);
		ps.setObject(++index,pi_purchase_parentDTO.purchaseOrderNumber);
		ps.setObject(++index,pi_purchase_parentDTO.purchaseDate);
		ps.setObject(++index,pi_purchase_parentDTO.remarks);
		ps.setObject(++index,pi_purchase_parentDTO.filesDropzone);
		ps.setObject(++index,pi_purchase_parentDTO.insertedBy);
		ps.setObject(++index,pi_purchase_parentDTO.insertionTime);
		ps.setObject(++index,pi_purchase_parentDTO.modifiedBy);
		ps.setObject(++index,pi_purchase_parentDTO.searchColumn);
		ps.setObject(++index,pi_purchase_parentDTO.assignedOfcUnitIds);
		ps.setObject(++index,pi_purchase_parentDTO.procurementMethod);
		if(isInsert)
		{
			ps.setObject(++index,pi_purchase_parentDTO.isDeleted);
		}
		ps.setObject(++index, lastModificationTime);
		if(!isInsert)
		{
			ps.setObject(++index,pi_purchase_parentDTO.iD);
		}
	}
	
	@Override
	public Pi_purchase_parentDTO buildObjectFromResultSet(ResultSet rs)
	{
		try
		{
			Pi_purchase_parentDTO pi_purchase_parentDTO = new Pi_purchase_parentDTO();
			int i = 0;
			pi_purchase_parentDTO.iD = rs.getLong(columnNames[i++]);
			pi_purchase_parentDTO.officeUnitId = rs.getLong(columnNames[i++]);
			pi_purchase_parentDTO.fiscalYearId = rs.getLong(columnNames[i++]);
			pi_purchase_parentDTO.packageId = rs.getLong(columnNames[i++]);
			pi_purchase_parentDTO.lotId = rs.getLong(columnNames[i++]);
			pi_purchase_parentDTO.vendorId = rs.getLong(columnNames[i++]);
			pi_purchase_parentDTO.sarokNumber = rs.getString(columnNames[i++]);
			pi_purchase_parentDTO.purchaseOrderNumber = rs.getString(columnNames[i++]);
			pi_purchase_parentDTO.purchaseDate = rs.getLong(columnNames[i++]);
			pi_purchase_parentDTO.remarks = rs.getString(columnNames[i++]);
			pi_purchase_parentDTO.filesDropzone = rs.getLong(columnNames[i++]);
			pi_purchase_parentDTO.insertedBy = rs.getLong(columnNames[i++]);
			pi_purchase_parentDTO.insertionTime = rs.getLong(columnNames[i++]);
			pi_purchase_parentDTO.modifiedBy = rs.getInt(columnNames[i++]);
			pi_purchase_parentDTO.searchColumn = rs.getString(columnNames[i++]);
			pi_purchase_parentDTO.assignedOfcUnitIds= rs.getString(columnNames[i++]);
			pi_purchase_parentDTO.procurementMethod= rs.getInt(columnNames[i++]);
			pi_purchase_parentDTO.isDeleted = rs.getInt(columnNames[i++]);
			pi_purchase_parentDTO.lastModificationTime = rs.getLong(columnNames[i++]);
			return pi_purchase_parentDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
		
	public Pi_purchase_parentDTO getDTOByID (long id)
	{
		return getDTOFromID(id);
	}

	@Override
	public String getTableName() {
		return "pi_purchase_parent";
	}

	@Override
	public Map<String, String> getSearchMap() {
		return searchMap;
	}

	public long add(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Pi_purchase_parentDTO) commonDTO,addQuery,true);
	}

	public long update(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Pi_purchase_parentDTO) commonDTO,updateQuery,false);
	}
	
	@Override
	public String getLastModifierUser(){
		return "modified_by";
    }

	public boolean isAlreadyPurchasedFiscalYearAndOfficeUnitAndPackageId(long fiscalYearId,long officeUnitId, long packageId) {
		String sql = String.format(alreadyPurchasedByFiscalYearAndOfficeUnitAndPackageId, fiscalYearId,officeUnitId,packageId);
		boolean isAlreadyPurchased = ConnectionAndStatementUtil.getListOfT(sql, this::buildObjectFromResultSet).size() > 0;
		return isAlreadyPurchased;
	}
				
}
	