package pi_purchase_parent;

import com.google.gson.Gson;
import common.ApiResponse;
import common.BaseServlet;
import files.FilesDAO;
import fiscal_year.Fiscal_yearDAO;
import fiscal_year.Fiscal_yearDTO;
import language.LC;
import language.LM;
import office_units.Office_unitsRepository;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import pb.ErrorMessage;
import pb.OptionDTO;
import pb.Utils;
import permission.MenuConstants;
import pi_package_final.*;
import pi_product_receive.Pi_product_receiveDAO;
import pi_purchase.Pi_purchaseDAO;
import pi_purchase.Pi_purchaseDTO;
import pi_unique_item.*;
import pi_vendor_auctioneer_details.Pi_vendor_auctioneer_detailsRepository;
import procurement_goods.Procurement_goodsDAO;
import procurement_goods.Procurement_goodsDTO;
import procurement_goods.Procurement_goodsRepository;
import procurement_package.ProcurementGoodsTypeDTO;
import procurement_package.ProcurementGoodsTypeRepository;
import sessionmanager.SessionConstants;
import user.UserDTO;
import util.*;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

@WebServlet("/Pi_purchase_parentServlet")
@MultipartConfig
public class Pi_purchase_parentServlet extends BaseServlet {
    private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Pi_purchase_parentServlet.class);
    Fiscal_yearDAO fiscal_yearDAO = new Fiscal_yearDAO();

    @Override
    public String getTableName() {
        return Pi_purchase_parentDAO.getInstance().getTableName();
    }

    @Override
    public String getServletName() {
        return "Pi_purchase_parentServlet";
    }

    @Override
    public Pi_purchase_parentDAO getCommonDAOService() {
        return Pi_purchase_parentDAO.getInstance();
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[]{MenuConstants.PI_PURCHASE_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[]{MenuConstants.PI_PURCHASE_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[]{MenuConstants.PI_PURCHASE_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Pi_purchase_parentServlet.class;
    }

    FilesDAO filesDAO = new FilesDAO();
    private final Gson gson = new Gson();

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        String language = userDTO.languageID == SessionConstants.ENGLISH ? "English" : "Bangla";
        Pi_purchase_parentDTO pi_purchase_parentDTO;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        HashMap<Long, Pi_purchaseDTO> idToPurchaseMap = new HashMap<>();

        if (addFlag) {
            pi_purchase_parentDTO = new Pi_purchase_parentDTO();
            pi_purchase_parentDTO.insertionTime = pi_purchase_parentDTO.lastModificationTime = System.currentTimeMillis();
            pi_purchase_parentDTO.insertedBy = pi_purchase_parentDTO.modifiedBy = userDTO.employee_record_id;
        } else {
            pi_purchase_parentDTO = Pi_purchase_parentDAO.getInstance()
                    .getDTOFromID(Long.parseLong(request.getParameter("mainPurchaseId")));
            pi_purchase_parentDTO.lastModificationTime = System.currentTimeMillis();
            pi_purchase_parentDTO.modifiedBy = userDTO.employee_record_id;

            List<Pi_purchaseDTO> purchaseDTOS = (List<Pi_purchaseDTO>) Pi_purchaseDAO.getInstance()
                    .getDTOsByParent("pi_purchase_parent_id", pi_purchase_parentDTO.iD);
            purchaseDTOS.forEach(purchaseDTO -> {
                idToPurchaseMap.put(purchaseDTO.iD, purchaseDTO);
            });
        }

        String value = "";
        value = request.getParameter("officeUnitId");
        if (StringUtils.isValidString(value)) {
            value = Jsoup.clean(value, Whitelist.simpleText());
            pi_purchase_parentDTO.officeUnitId = Long.parseLong(value);
        } else {
            UtilCharacter.throwException("স্টোর সংশ্লিষ্ট দপ্তর সিলেক্ট করুন", "Please select Store at Office");
        }


        value = request.getParameter("fiscalYearId");
        if (StringUtils.isValidString(value) && !value.equalsIgnoreCase("-1")) {
            value = Jsoup.clean(value, Whitelist.simpleText());
            pi_purchase_parentDTO.fiscalYearId = Long.parseLong(value);
        } else {
            UtilCharacter.throwException("অর্থবছর সিলেক্ট করুন", "Please select fiscal year");
        }


        value = request.getParameter("procurement_method");
        if (StringUtils.isValidString(value) && !value.equalsIgnoreCase("-1")) {
            value = Jsoup.clean(value, Whitelist.simpleText());
            pi_purchase_parentDTO.procurementMethod = Integer.parseInt(value);
        } else {
            UtilCharacter.throwException("ক্রয় পদ্ধতি বাছাই করুন", "Please select procurement method");
        }


        value = request.getParameter("packageId");
        if (StringUtils.isValidString(value) && !value.equalsIgnoreCase("-1")) {
            value = Jsoup.clean(value, Whitelist.simpleText());
            pi_purchase_parentDTO.packageId = Long.parseLong(value);
        } else {
            UtilCharacter.throwException("প্যাকেজ সিলেক্ট করুন", "Please select package");
        }


        value = request.getParameter("piLotFinalId");
        if (StringUtils.isValidString(value) && !value.equalsIgnoreCase("-1")) {
            value = Jsoup.clean(value, Whitelist.simpleText());
            pi_purchase_parentDTO.lotId = Long.parseLong(value);
        }


        value = request.getParameter("vendorId");
        if (StringUtils.isValidString(value) && !value.equalsIgnoreCase("-1")) {
            value = Jsoup.clean(value, Whitelist.simpleText());
            pi_purchase_parentDTO.vendorId = Long.parseLong(value);
        } else {
            UtilCharacter.throwException("ভেন্ডর সিলেক্ট করুন", "Please select vendor");
        }


        value = request.getParameter("sarokNumber");
        if (StringUtils.isValidString(value)) {
            value = Jsoup.clean(value, Whitelist.simpleText());
            pi_purchase_parentDTO.sarokNumber = value;
        }


        value = request.getParameter("purchaseOrderNumber");
        if (StringUtils.isValidString(value)) {
            value = Jsoup.clean(value, Whitelist.simpleText());
            value = removeAllSpaces(value);
            validatePurchaseOrderNumber(value);
            pi_purchase_parentDTO.purchaseOrderNumber = value;
        } else {
            UtilCharacter.throwException("অনুগ্রহপূর্বক ক্রয় আদেশ নম্বর লিখুন!", "Please write purchase order number!");
        }


        value = request.getParameter("purchaseDate");
        System.out.println("purchaseDate = " + value);
        if (StringUtils.isValidString(value) && addFlag) {
            Date d = simpleDateFormat.parse(value);
            pi_purchase_parentDTO.purchaseDate = d.getTime();
        } else {
            UtilCharacter.throwException("অনুগ্রহপূর্বক ক্রয় তারিখ লিখুন!", "Please enter purchase date!");
        }


        Fiscal_yearDTO purchaseDateFiscalYear = fiscal_yearDAO
                .getFiscalYearBYDateLong(pi_purchase_parentDTO.purchaseDate);
        if (purchaseDateFiscalYear.id != pi_purchase_parentDTO.fiscalYearId) {
            throw new Exception(language.equals("English") ?
                    "Purchase date should be within selected fiscal year" :
                    "ক্রয়ের তারিখ বাছাইকৃত অর্থবছরের মধ্যে হতে হবে");
        }


        value = request.getParameter("remarks");
        if (StringUtils.isValidString(value)) {
            pi_purchase_parentDTO.remarks = value;
        }


        value = request.getParameter("filesDropzone");
        if (StringUtils.isValidString(value)) {
            pi_purchase_parentDTO.filesDropzone = Long.parseLong(value);

            if (!addFlag) {
                String filesDropzoneFilesToDelete = request.getParameter("filesDropzoneFilesToDelete");
                String[] deleteArray = filesDropzoneFilesToDelete.split(",");
                for (int i = 0; i < deleteArray.length; i++) {
                    logger.debug("going to delete " + deleteArray[i]);
                    if (i > 0) {
                        filesDAO.delete(Long.parseLong(deleteArray[i]));
                    }
                }
            }
        }


        Utils.handleTransaction(() -> {
            if (addFlag) {
                long returnedId = Pi_purchase_parentDAO.getInstance().add(pi_purchase_parentDTO);
                pi_purchase_parentDTO.iD = returnedId;
            } else {
                Pi_purchase_parentDAO.getInstance().update(pi_purchase_parentDTO);
            }

            List<Pi_purchaseDTO> newChildModels = getItemsData(request, pi_purchase_parentDTO, simpleDateFormat);
            List<Pi_purchaseDTO> oldChildModels = (List<Pi_purchaseDTO>) Pi_purchaseDAO.getInstance()
                    .getDTOsByParent("pi_purchase_parent_id", pi_purchase_parentDTO.iD);
            // IDS WITH -1 ARE FOR ADD
            List<Pi_purchaseDTO> childModelsToAdd = newChildModels.stream().filter(dto -> dto.iD == -1).collect(Collectors.toList());
            // COMMON DTOS ARE FOR EDIT
            List<Pi_purchaseDTO> childModelsToEdit = newChildModels.stream().filter(dto -> fieldContains(oldChildModels, dto.iD)).collect(Collectors.toList());
            // IDS NOT COMMON AND NOT -1 ARE FOR DELETE
            List<Pi_purchaseDTO> childModelsToDelete = oldChildModels.stream().filter(dto -> !fieldContains(childModelsToEdit, dto.iD)).collect(Collectors.toList());

            for (Pi_purchaseDTO dto : childModelsToAdd) {
                dto.insertedBy = dto.modifiedBy = userDTO.employee_record_id;
                dto.insertionTime = dto.lastModificationTime = System.currentTimeMillis();
                Pi_purchaseDAO.getInstance().add(dto);
            }
            for (Pi_purchaseDTO dto : childModelsToEdit) {
                dto.modifiedBy = userDTO.employee_record_id;
                dto.lastModificationTime = System.currentTimeMillis();
                Pi_purchaseDAO.getInstance().update(dto);
            }
            for (Pi_purchaseDTO dto : childModelsToDelete) {
                Pi_purchaseDAO.getInstance().delete(userDTO.employee_record_id, dto.iD, System.currentTimeMillis());
            }
        });

        return pi_purchase_parentDTO;
    }

    private List<Pi_purchaseDTO> getItemsData(HttpServletRequest request, Pi_purchase_parentDTO pi_purchase_parentDTO,
                                              SimpleDateFormat simpleDateFormat) throws Exception {
        String[] Values = request.getParameterValues("selected");
        List<Pi_purchaseDTO> childDTOs = new ArrayList<>();

        for (int index = 0; index < Values.length; index++) {
            Pi_purchaseDTO model = getItemData(request, pi_purchase_parentDTO, simpleDateFormat, index);
            if (model == null) {
                continue;
            }
            model.parentId = pi_purchase_parentDTO.iD;
            childDTOs.add(model);
        }
        return childDTOs;
    }

    private Pi_purchaseDTO getItemData(HttpServletRequest request, Pi_purchase_parentDTO pi_purchase_parentDTO,
                                       SimpleDateFormat simpleDateFormat, int index) throws Exception {
        String language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
        String[] Values = request.getParameterValues("selected");
        final boolean[] insertedParent = {false};
        final String[] existingRequisitionItems = {""};
        String amountGreaterThanAPP = "";
        String purchaseDateGreaterThanExpiryDate = "";
        long[] returnedID = {-1};

        List<Double> amountList = new ArrayList<>();
        List<Double> unitPriceList = new ArrayList<>();

        String Value = "";
        Value = request.getParameterValues("selected")[index];
        System.out.println("selected = " + Value);

        Boolean selected = Boolean.parseBoolean(Value);
        if (!selected) {
            return null;
        }

        Pi_purchaseDTO pi_purchaseDTO = gson.fromJson(
                gson.toJson(pi_purchase_parentDTO),
                Pi_purchaseDTO.class
        );

        Value = request.getParameterValues("iD")[index];

        if (Value != null && !Value.equalsIgnoreCase("")) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }
        System.out.println("iD = " + Value);
        if (Value != null && !Value.equalsIgnoreCase("")) {
            pi_purchaseDTO.iD = Long.parseLong(Value);
        } else {
            System.out.println("FieldName has a null Value, not updating" + " = " + Value);
        }

//				Value = request.getParameterValues("totalBill")[index];
//
//				if(Value != null && !Value.equalsIgnoreCase(""))
//				{
//					Value = Jsoup.clean(Value,Whitelist.simpleText());
//				}
//				System.out.println("totalBill = " + Value);
//				if(Value != null && !Value.equalsIgnoreCase(""))
//				{
//					pi_purchaseDTO.totalBill = Double.parseDouble(Value);
//				}
//				else
//				{
//					System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//				}

        Value = request.getParameterValues("itemTypeId")[index];

        if (Value != null && !Value.equalsIgnoreCase("")) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }
        System.out.println("itemTypeId = " + Value);
        if (Value != null && !Value.equalsIgnoreCase("")) {
            pi_purchaseDTO.itemTypeId = Long.parseLong(Value);
        } else {
            System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            throw new Exception(language.equals("English") ?
                    "item type needed" :
                    "আইটেম টাইপ আবশ্যক");
        }

        Value = request.getParameterValues("itemId")[index];

        if (Value != null && !Value.equalsIgnoreCase("")) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }
        System.out.println("itemId = " + Value);
        if (Value != null && !Value.equalsIgnoreCase("")) {
            pi_purchaseDTO.itemId = Long.parseLong(Value);
            Procurement_goodsDTO itemDTO = Procurement_goodsDAO.getInstance().getDTOByID(pi_purchaseDTO.itemId);
            ProcurementGoodsTypeDTO goodsTypeDTO = ProcurementGoodsTypeRepository.getInstance()
                    .getProcurementGoodsTypeDTOByID(itemDTO.procurementGoodsTypeId);
            pi_purchaseDTO.itemGroupId = goodsTypeDTO.procurementPackageId;
        } else {
            System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            throw new Exception(language.equals("English") ?
                    "item needed" :
                    "আইটেম আবশ্যক");
        }

        Value = request.getParameterValues("amount")[index];

        if (Value != null && !Value.equalsIgnoreCase("")) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }
        System.out.println("amount = " + Value);
        double amount = -1;
        if (Value != null && !Value.equalsIgnoreCase("") && Double.parseDouble(Value) >= 0.0) {
            pi_purchaseDTO.amount = Double.parseDouble(Value);
            amount = pi_purchaseDTO.amount;
            amountList.add(pi_purchaseDTO.amount);
        } else {
            System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            throw new Exception(language.equals("English") ?
                    "amount needed(Greater than or equal zero): " + Procurement_goodsRepository.getInstance().getProcurement_goodsDTOByID(pi_purchaseDTO.itemId).nameEn :
                    "পরিমান আবশ্যক(শূন্য বা ততোধিক): " + Procurement_goodsRepository.getInstance().getProcurement_goodsDTOByID(pi_purchaseDTO.itemId).nameBn);
        }

        /*purchase remarks start*/

        Value = request.getParameterValues("purchaseItemRemarks")[index];

        if (Value != null && !Value.equalsIgnoreCase("")) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }
        if (Value != null && !Value.equalsIgnoreCase("")) {
            pi_purchaseDTO.purchaseItemRemarks = Value;
        }


        /*purchase remarks end*/

        Value = request.getParameterValues("expiryDate")[index];

        if (Value != null && !Value.equalsIgnoreCase("")) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }
        System.out.println("expiryDate = " + Value);
        if (Value != null && !Value.equalsIgnoreCase("")) {
            try {
                Date d = simpleDateFormat.parse(Value);
                pi_purchaseDTO.expiryDate = d.getTime();
            } catch (Exception e) {
                e.printStackTrace();
                throw new Exception(LM.getText(LC.PI_PURCHASE_ADD_EXPIRYDATE, language) + " " + ErrorMessage.getInvalidMessage(language));
            }
            if (pi_purchaseDTO.expiryDate < pi_purchaseDTO.purchaseDate) {
                purchaseDateGreaterThanExpiryDate += ",";
                purchaseDateGreaterThanExpiryDate += language.equals("English") ?
                        Procurement_goodsRepository.getInstance().getProcurement_goodsDTOByID(pi_purchaseDTO.itemId).nameEn :
                        Procurement_goodsRepository.getInstance().getProcurement_goodsDTOByID(pi_purchaseDTO.itemId).nameBn;
                return null;
            }
        } else {
            System.out.println("FieldName has a null Value, not updating" + " = " + Value);
        }

        Value = request.getParameterValues("insertedBy")[index];

        if (Value != null && !Value.equalsIgnoreCase("")) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }
        System.out.println("insertedBy = " + Value);
        if (Value != null && !Value.equalsIgnoreCase("")) {
            pi_purchaseDTO.insertedBy = Long.parseLong(Value);
        } else {
            System.out.println("FieldName has a null Value, not updating" + " = " + Value);
        }

        Value = request.getParameterValues("insertionTime")[index];

        if (Value != null && !Value.equalsIgnoreCase("")) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }
        System.out.println("insertionTime = " + Value);
        if (Value != null && !Value.equalsIgnoreCase("")) {
            pi_purchaseDTO.insertionTime = Long.parseLong(Value);
        } else {
            System.out.println("FieldName has a null Value, not updating" + " = " + Value);
        }

        Value = request.getParameterValues("modifiedBy")[index];

        if (Value != null && !Value.equalsIgnoreCase("")) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }
        System.out.println("modifiedBy = " + Value);
        if (Value != null && !Value.equalsIgnoreCase("")) {
            pi_purchaseDTO.modifiedBy = Long.parseLong(Value);
        } else {
            System.out.println("FieldName has a null Value, not updating" + " = " + Value);
        }

        System.out.println("Done adding  addPi_purchase dto = " + pi_purchaseDTO);

				/*List<Pi_package_vendor_itemsDTO> items = Pi_package_vendor_itemsDAO.getInstance().
						getPriceByFiscalYearIdAndOfficeUnitIdAndPackageIdAndVendorIdAndProductId(pi_purchase_parentDTO[0].fiscalYearId,
								pi_purchase_parentDTO[0].officeUnitId, pi_purchase_parentDTO[0].packageId, pi_purchase_parentDTO[0].vendorId, pi_purchaseDTO.itemId );

				double unitPrice = !items.isEmpty()? items.get(0).price : 0;
				unitPriceList.add(unitPrice);*/


        /*unit price from request start*/
        Value = request.getParameterValues("unitPrice")[index];

        if (Value != null && !Value.equalsIgnoreCase("")) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }
        System.out.println("unitPrice = " + Value);
        if (Value != null && !Value.equalsIgnoreCase("")) {
            pi_purchaseDTO.unitPrice = Double.parseDouble(Value);
        } else {
            System.out.println("FieldName has a null Value, not updating" + " = " + Value);
        }
        /*unit price from request end*/

        unitPriceList.add(pi_purchaseDTO.unitPrice);

        pi_purchaseDTO.totalBill = amount * pi_purchaseDTO.unitPrice;
        pi_purchaseDTO.totalValue = pi_purchaseDTO.totalBill;

        /*app allocation from request start*/
        Value = request.getParameterValues("allocatedApp")[index];

        if (Value != null && !Value.equalsIgnoreCase("")) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }
        System.out.println("allocatedApp = " + Value);
        if (Value != null && !Value.equalsIgnoreCase("")) {
            pi_purchaseDTO.allocatedApp = Double.parseDouble(Value);
        } else {
            System.out.println("FieldName has a null Value, not updating" + " = " + Value);
        }
        /*app allocation from request end*/


        int currentYearStockInt = PiUniqueItemAssignmentDAO.getInstance().getItemsCountWithoutStore(
                pi_purchase_parentDTO.fiscalYearId, pi_purchaseDTO.itemId,
                PiActionTypeEnum.PURCHASE.getValue());
        double appAllocated = pi_purchaseDTO.allocatedApp;
        double appAllocatedRemaining = appAllocated - (currentYearStockInt * 1.0);

        if (amount > appAllocatedRemaining) {
            amountGreaterThanAPP += ",";
            Procurement_goodsDTO procurement_goodsDTO = Procurement_goodsRepository.getInstance().getProcurement_goodsDTOByID(pi_purchaseDTO.itemId);
            amountGreaterThanAPP += language.equals("English") ?
                    " Item Name: " + procurement_goodsDTO.nameEn :
                    " আইটেম নাম: " + procurement_goodsDTO.nameBn;
            return null;
        }
        return pi_purchaseDTO;
    }

    private boolean fieldContains(List<Pi_purchaseDTO> childDTOs, Long id) {
        List<Pi_purchaseDTO> dto = childDTOs.stream().filter(model -> model.iD == id).collect(Collectors.toList());
        return dto.size() > 0;
    }

    private String removeAllSpaces(String str) {
        String trimmedStr = "";
        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) != ' ') trimmedStr += str.charAt(i);
        }
        return trimmedStr;
    }

    private void validatePurchaseOrderNumber(String purchaseOrderNumber) throws Exception {
        List<Pi_purchaseDTO> models = Pi_purchaseDAO.getInstance().getDTOsByPurchaseOrderNumber(purchaseOrderNumber);
        if (!models.isEmpty()) {
            UtilCharacter.throwException("এই ক্রয় আদেশ নম্বর ইতিমধ্যে সিস্টেমে বিদ্যমান!", "This purchase order number already exist in the system");
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        CommonLoginData commonLoginData = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get();
        String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
        List<OptionDTO> optionDTOList;
        init(request);
        try {
            switch (request.getParameter("actionType")) {
                case "getPackageListByFiscalYear":
                    if (Utils.checkPermission(commonLoginData.userDTO, getViewMenuConstants()) && getViewPagePermission(request)) {
                        String fiscalYearIdStr = request.getParameter("fiscalYearId");
                        if (fiscalYearIdStr == null) {
                            String errorMessage = UtilCharacter.getDataByLanguage(Language,
                                    "অর্থবছর নির্বাচন করা হয়নি!", "Fiscal Year Not Selected!");
                            ApiResponse.sendErrorResponse(response, errorMessage);
                            return;
                        }

                        long fiscalYearId = Long.parseLong(fiscalYearIdStr);
                        List<Pi_purchaseDTO> purchaseModels = (List<Pi_purchaseDTO>) Pi_purchaseDAO.getInstance()
                                .getDTOsByParent("fiscal_year_type", fiscalYearId);
                        List<Long> packageIds = purchaseModels.stream().map(dto -> dto.packageId).distinct()
                                .collect(Collectors.toList());
                        List<Pi_package_finalDTO> packageDTOS = Pi_package_finalRepository.getInstance().
                                getDTOsByIds(packageIds);
                        optionDTOList = packageDTOS
                                .stream()
                                .map(e -> new OptionDTO(e.packageNumberEn, e.packageNumberBn, String.valueOf(e.iD)))
                                .collect(Collectors.toList());
                        String responseText = Utils.buildOptions(optionDTOList, Language, "-1");
                        ApiResponse.sendSuccessResponse(response, gson.toJson(responseText));
                        return;
                    }
                    break;
                case "getLotListByFiscalYear":
                    if (Utils.checkPermission(commonLoginData.userDTO, getViewMenuConstants()) && getViewPagePermission(request)) {
                        String fiscalYearIdStr = request.getParameter("fiscalYearId");
                        if (fiscalYearIdStr == null) {
                            String errorMessage = UtilCharacter.getDataByLanguage(Language,
                                    "অর্থবছর নির্বাচন করা হয়নি!", "Fiscal Year Not Selected!");
                            ApiResponse.sendErrorResponse(response, errorMessage);
                            return;
                        }

                        long fiscalYearId = Long.parseLong(fiscalYearIdStr);
                        List<Pi_purchaseDTO> purchaseModels = (List<Pi_purchaseDTO>) Pi_purchaseDAO.getInstance()
                                .getDTOsByParent("fiscal_year_type", fiscalYearId);
                        List<Long> lotIds = purchaseModels.stream().map(dto -> dto.lotId).distinct()
                                .collect(Collectors.toList());
                        List<PiPackageLotFinalDTO> lotDTOs = PiPackageLotFinalRepository.getInstance().
                                getDTOsByIds(lotIds);
                        optionDTOList = lotDTOs
                                .stream()
                                .map(e -> new OptionDTO(e.lotNumberEn, e.lotNumberBn, String.valueOf(e.iD)))
                                .collect(Collectors.toList());
                        String responseText = Utils.buildOptions(optionDTOList, Language, "-1");
                        ApiResponse.sendSuccessResponse(response, gson.toJson(responseText));
                        return;
                    }
                    break;
                case "getSarokNumberByFiscalYearAndPackage":
                    if (Utils.checkPermission(commonLoginData.userDTO, getViewMenuConstants()) && getViewPagePermission(request)) {
                        String fiscalYearIdStr = request.getParameter("fiscalYearId");
                        if (fiscalYearIdStr == null) {
                            String errorMessage = UtilCharacter.getDataByLanguage(Language,
                                    "অর্থবছর নির্বাচন করা হয়নি!", "Fiscal Year Not Selected!");
                            ApiResponse.sendErrorResponse(response, errorMessage);
                            return;
                        }

                        String packageIdStr = request.getParameter("packageId");
                        if (packageIdStr == null) {
                            String errorMessage = UtilCharacter.getDataByLanguage(Language,
                                    "প্যাকেজ নির্বাচন করা হয়নি!", "Package Not Selected!");
                            ApiResponse.sendErrorResponse(response, errorMessage);
                            return;
                        }

                        long fiscalYearId = Long.parseLong(fiscalYearIdStr);
                        long packageId = Long.parseLong(packageIdStr);
                        List<Pi_purchaseDTO> purchaseModels = (List<Pi_purchaseDTO>) Pi_purchaseDAO.getInstance()
                                .getDTOsByParent("fiscal_year_type", fiscalYearId);
                        purchaseModels = purchaseModels.stream().filter(dto -> dto.packageId == packageId)
                                .collect(Collectors.toList());
                        optionDTOList = purchaseModels
                                .stream()
                                .map(e -> new OptionDTO(e.sarokNumber, e.sarokNumber, String.valueOf(e.iD)))
                                .collect(Collectors.toList());
                        String responseText = Utils.buildOptions(optionDTOList, Language, "-1");
                        ApiResponse.sendSuccessResponse(response, gson.toJson(responseText));
                        return;
                    }
                    break;
                case "getPurchaseOrderNumbersByFiscalYearAndPackageAndLot":
                    if (Utils.checkPermission(commonLoginData.userDTO, getViewMenuConstants()) && getViewPagePermission(request)) {
                        String fiscalYearIdStr = request.getParameter("fiscalYearId");
                        if (fiscalYearIdStr == null) {
                            String errorMessage = UtilCharacter.getDataByLanguage(Language,
                                    "অর্থবছর নির্বাচন করা হয়নি!", "Fiscal Year Not Selected!");
                            ApiResponse.sendErrorResponse(response, errorMessage);
                            return;
                        }
                        long fiscalYearId = Long.parseLong(fiscalYearIdStr);

                        List<Pi_purchase_parentDTO> purchaseModels = (List<Pi_purchase_parentDTO>) Pi_purchase_parentDAO.getInstance()
                                .getDTOsByParent("fiscal_year_type", fiscalYearId);

                        String packageIdStr = request.getParameter("packageId");
                        String lotIdStr = request.getParameter("lotId");
                        if (StringUtils.isValidString(lotIdStr)) {
                            purchaseModels = purchaseModels.stream().filter(dto -> dto.lotId == Long.parseLong(lotIdStr))
                                    .collect(Collectors.toList());
                        } else {
                            purchaseModels = purchaseModels.stream().filter(dto -> dto.packageId == Long.parseLong(packageIdStr))
                                    .collect(Collectors.toList());
                        }

                        optionDTOList = purchaseModels
                                .stream()
                                .map(e -> new OptionDTO(e.purchaseOrderNumber, e.purchaseOrderNumber, String.valueOf(e.iD)))
                                .collect(Collectors.toList());
                        String responseText = Utils.buildOptions(optionDTOList, Language, "-1");
                        ApiResponse.sendSuccessResponse(response, gson.toJson(responseText));
                        return;
                    }
                    break;
                case "getItemsByPurchaseOrderNumber":
                    String piPurchaseParentIdStr = request.getParameter("piPurchaseParentId");
                    if (piPurchaseParentIdStr == null) {
                        String errorMessage = UtilCharacter.getDataByLanguage(Language,
                                "ক্রয়ের আদেশ নির্বাচন করা হয়নি!", "Purchase Order Number Not Selected!");
                        ApiResponse.sendErrorResponse(response, errorMessage);
                        return;
                    }
                    long piPurchaseParentId = Long.parseLong(piPurchaseParentIdStr);

                    List<Pi_purchaseDTO> purchaseModels = (List<Pi_purchaseDTO>) Pi_purchaseDAO.getInstance()
                            .getDTOsByParent("pi_purchase_parent_id", piPurchaseParentId);
                    long storeOfficeUnitId = storeOfficeUnitId = purchaseModels.stream().map(model -> model.officeUnitId)
                            .findFirst().get();
                    List<ItemModel> itemModels = new ArrayList<>();
                    for (Pi_purchaseDTO model : purchaseModels) {
                        ItemModel itemModel = new ItemModel();
                        itemModel.piPurchaseId = model.iD;
                        itemModel.packageId = model.packageId;
                        itemModel.packageName = Pi_package_finalDAO.getInstance().getName(Language, model.packageId);
                        itemModel.itemTypeId = model.itemTypeId;
                        itemModel.itemTypeName = ProcurementGoodsTypeRepository.getInstance()
                                .getName(Language, model.itemTypeId);
                        itemModel.itemId = model.itemId;
                        itemModel.itemName = Procurement_goodsRepository.getInstance().getName(Language, model.itemId);
                        itemModel.currentStock = PiUniqueItemAssignmentDAO.getInstance()
                                .getTotalItemCountWithStageAllOver(storeOfficeUnitId, model.itemId,
                                        PiStageEnum.IN_STOCK.getValue());
                        itemModel.currentReceivedCount = Pi_product_receiveDAO.getInstance()
                                .getItemReceiveCountByPurchaseAndItem(piPurchaseParentId, itemModel.itemId);
                        itemModel.purchaseCount = (long) model.amount;
                        itemModel.remainingPurchaseCount = itemModel.purchaseCount - itemModel.currentReceivedCount;
                        itemModel.unitPrice = (long) model.unitPrice;
                        itemModel.storeOfficeUnitId = storeOfficeUnitId;
                        itemModel.storeOfficeUnitName = Office_unitsRepository.getInstance()
                                .geText(Language, storeOfficeUnitId);
                        itemModel.vendorId = model.vendorId;
                        itemModel.vendorName = Pi_vendor_auctioneer_detailsRepository.getInstance()
                                .getText(itemModel.vendorId, Language);
                        itemModels.add(itemModel);
                    }
                    ApiResponse.sendSuccessResponse(response, gson.toJson(itemModels));
                    return;
            }
        } catch (Exception ex) {
            logger.error("Exception : ", ex);
        }
        super.doGet(request, response);
    }
}

