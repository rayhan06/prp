package mps_attendance;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;
import sessionmanager.SessionConstants;
import java.sql.SQLException;


import org.apache.log4j.Logger;

import dbm.*;

import repository.RepositoryManager;

import util.*;
import pb.*;
import user.UserDTO;

public class Mps_attendanceDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());
	
	
	public Mps_attendanceDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";
		commonMaps = new Mps_attendanceMAPS(tableName);
	}
	
	public Mps_attendanceDAO()
	{
		this("mps_attendance");		
	}
	
	public void setSearchColumn(Mps_attendanceDTO mps_attendanceDTO)
	{
		mps_attendanceDTO.searchColumn = "";
		
	}
	
	public void set(PreparedStatement ps, Mps_attendanceDTO mps_attendanceDTO, boolean isInsert) throws SQLException
	{
		int index = 1;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(mps_attendanceDTO);
		if(isInsert)
		{
			ps.setObject(index++,mps_attendanceDTO.iD);
		}
		ps.setObject(index++,mps_attendanceDTO.iD);
		ps.setObject(index++,mps_attendanceDTO.electionDetailsId);
		ps.setObject(index++,mps_attendanceDTO.electionConstituencyId);
		ps.setObject(index++,mps_attendanceDTO.employeeRecordsId);
		ps.setObject(index++,mps_attendanceDTO.politicalPartyId);
		ps.setObject(index++,mps_attendanceDTO.startDate);
		ps.setObject(index++,mps_attendanceDTO.endDate);
		ps.setObject(index++,mps_attendanceDTO.insertionDate);
		ps.setObject(index++,mps_attendanceDTO.insertedBy);
		ps.setObject(index++,mps_attendanceDTO.modifiedBy);
		ps.setObject(index++,mps_attendanceDTO.searchColumn);
		if(isInsert)
		{
			ps.setObject(index++, 0);
			ps.setObject(index++, lastModificationTime);
		}
	}
	
	public void get(Mps_attendanceDTO mps_attendanceDTO, ResultSet rs) throws SQLException
	{
		mps_attendanceDTO.iD = rs.getLong("ID");
		mps_attendanceDTO.electionDetailsId = rs.getLong("election_details_id");
		mps_attendanceDTO.electionConstituencyId = rs.getLong("election_constituency_id");
		mps_attendanceDTO.employeeRecordsId = rs.getLong("employee_records_id");
		mps_attendanceDTO.politicalPartyId = rs.getLong("political_party_id");
		mps_attendanceDTO.startDate = rs.getLong("start_date");
		mps_attendanceDTO.endDate = rs.getLong("end_date");
		mps_attendanceDTO.insertionDate = rs.getLong("insertion_date");
		mps_attendanceDTO.insertedBy = rs.getString("inserted_by");
		mps_attendanceDTO.modifiedBy = rs.getString("modified_by");
		mps_attendanceDTO.searchColumn = rs.getString("search_column");
		mps_attendanceDTO.isDeleted = rs.getInt("isDeleted");
		mps_attendanceDTO.lastModificationTime = rs.getLong("lastModificationTime");
	}
	
	
	
	public long add(CommonDTO commonDTO) throws Exception
	{
		
		Mps_attendanceDTO mps_attendanceDTO = (Mps_attendanceDTO)commonDTO;
		
		Connection connection = null;
		PreparedStatement ps = null;

		long lastModificationTime = System.currentTimeMillis();	

		try{
			connection = DBMW.getInstance().getConnection();
			
			if(connection == null)
			{
				System.out.println("nullconn");
			}

			mps_attendanceDTO.iD = DBMW.getInstance().getNextSequenceId(tableName);

			String sql = "INSERT INTO " + tableName;
			
			sql += " (";
			sql += "ID";
			sql += ", ";
			sql += "election_details_id";
			sql += ", ";
			sql += "election_constituency_id";
			sql += ", ";
			sql += "employee_records_id";
			sql += ", ";
			sql += "political_party_id";
			sql += ", ";
			sql += "start_date";
			sql += ", ";
			sql += "end_date";
			sql += ", ";
			sql += "insertion_date";
			sql += ", ";
			sql += "inserted_by";
			sql += ", ";
			sql += "modified_by";
			sql += ", ";
			sql += "search_column";
			sql += ", ";
			sql += "isDeleted";
			sql += ", ";
			sql += "lastModificationTime";
		
			sql += ")";
			
			
            sql += " VALUES(";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			
			sql += ")";
				
			ps = connection.prepareStatement(sql);
			set(ps, mps_attendanceDTO, true);
			ps.execute();
			
			
			recordUpdateTime(connection, lastModificationTime);

		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if (ps != null) {
					ps.close();
				}
			} catch(Exception e){}
			try{
				if(connection != null)
				{
					DBMW.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		return mps_attendanceDTO.iD;
	}
		
	

	//need another getter for repository
	public Mps_attendanceDTO getDTOByID (long ID)
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Mps_attendanceDTO mps_attendanceDTO = null;
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
			
            sql += " WHERE id=" + ID;
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			if(rs.next()){
				mps_attendanceDTO = new Mps_attendanceDTO();

				get(mps_attendanceDTO, rs);

			}			
			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return mps_attendanceDTO;
	}
	
	public long update(CommonDTO commonDTO) throws Exception
	{		
		Mps_attendanceDTO mps_attendanceDTO = (Mps_attendanceDTO)commonDTO;
		
		Connection connection = null;
		PreparedStatement ps = null;

		long lastModificationTime = System.currentTimeMillis();	
		try{
			connection = DBMW.getInstance().getConnection();

			String sql = "UPDATE " + tableName;
			
			sql += " SET ";
			sql += "ID=?";
			sql += ", ";
			sql += "election_details_id=?";
			sql += ", ";
			sql += "election_constituency_id=?";
			sql += ", ";
			sql += "employee_records_id=?";
			sql += ", ";
			sql += "political_party_id=?";
			sql += ", ";
			sql += "start_date=?";
			sql += ", ";
			sql += "end_date=?";
			sql += ", ";
			sql += "insertion_date=?";
			sql += ", ";
			sql += "inserted_by=?";
			sql += ", ";
			sql += "modified_by=?";
			sql += ", ";
			sql += "search_column=?";
			sql += ", lastModificationTime = "	+ lastModificationTime + "";
            sql += " WHERE id = " + mps_attendanceDTO.iD;
				

			ps = connection.prepareStatement(sql);
			set(ps, mps_attendanceDTO, false);
			ps.executeUpdate();
			

			recordUpdateTime(connection, lastModificationTime);

						
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if (ps != null) {
					ps.close();
				}
			} catch(Exception e){}
			try{
				if(connection != null)
				{
					DBMW.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		return mps_attendanceDTO.iD;
	}
	
	
	public List<Mps_attendanceDTO> getDTOs(Collection recordIDs){
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Mps_attendanceDTO mps_attendanceDTO = null;
		List<Mps_attendanceDTO> mps_attendanceDTOList = new ArrayList<>();
		if(recordIDs.isEmpty()){
			return mps_attendanceDTOList;
		}
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
            
            sql += " WHERE id IN ( ";

			for(int i = 0;i<recordIDs.size();i++){
				if(i!=0){
					sql+=",";
				}
				sql+=((ArrayList)recordIDs).get(i);
			}
			sql+=")  order by lastModificationTime desc";
			
			printSql(sql);
			
			logger.debug("sql " + sql);
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				mps_attendanceDTO = new Mps_attendanceDTO();
				get(mps_attendanceDTO, rs);
				System.out.println("got this DTO: " + mps_attendanceDTO);
				
				mps_attendanceDTOList.add(mps_attendanceDTO);

			}			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return mps_attendanceDTOList;
	
	}
	
	

	
	
	
	//add repository
	public List<Mps_attendanceDTO> getAllMps_attendance (boolean isFirstReload)
    {
		List<Mps_attendanceDTO> mps_attendanceDTOList = new ArrayList<>();

		String sql = "SELECT * FROM mps_attendance";
		sql += " WHERE ";
	

		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by mps_attendance.lastModificationTime desc";
		printSql(sql);
		
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;

		
		try{
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();
			rs = stmt.executeQuery(sql);
			

			while(rs.next()){
				Mps_attendanceDTO mps_attendanceDTO = new Mps_attendanceDTO();
				get(mps_attendanceDTO, rs);
				
				mps_attendanceDTOList.add(mps_attendanceDTO);
			}			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}

		return mps_attendanceDTOList;
    }

	
	public List<Mps_attendanceDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO)
	{
		return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
	}

	
	public List<Mps_attendanceDTO> getDTOs(Hashtable  p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
			String filter, boolean tableHasJobCat)
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		List<Mps_attendanceDTO> mps_attendanceDTOList = new ArrayList<>();

		try{
			
			String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat);
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				Mps_attendanceDTO mps_attendanceDTO = new Mps_attendanceDTO();
				get(mps_attendanceDTO, rs);
				
				mps_attendanceDTOList.add(mps_attendanceDTO);
			}					
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return mps_attendanceDTOList;
	
	}
	public String getSqlWithSearchCriteriaForNormalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
			UserDTO userDTO, String filter, boolean tableHasJobCat)
    {
		boolean viewAll = false;
		
		if(p_searchCriteria != null && p_searchCriteria.get("ViewAll") != null)
		{
			System.out.println("ViewAll = " + p_searchCriteria.get("ViewAll"));
			viewAll = true;
		}
		else
		{
			System.out.println("ViewAll is null ");
		}
		
		String sql = "SELECT ";
		if(category == GETIDS)
		{
			sql += " " + tableName + ".ID as ID ";
		}
		else if(category == GETCOUNT)
		{
			sql += " count( " + tableName + ".ID) as countID ";
		}
		else if(category == GETDTOS)
		{
			sql += " distinct " + tableName + ".* ";
		}
		sql += "FROM " + tableName + " ";
		sql += joinSQL;
		

		
		String AnyfieldSql = "";
		String AllFieldSql = "";
		
		if(p_searchCriteria != null)
		{
						
			
			Enumeration names = p_searchCriteria.keys();
			String str, value;
			
			AnyfieldSql = "(";
			
			if(p_searchCriteria.get("AnyField")!= null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase(""))
			{
				AnyfieldSql+= tableName + ".search_column like '%" + p_searchCriteria.get("AnyField").toString() + "%'";			
			}
			AnyfieldSql += ")";
			System.out.println("AnyfieldSql = " + AnyfieldSql);
			
			AllFieldSql = "(";
			int i = 0;
			while(names.hasMoreElements())
			{				
				str = (String) names.nextElement();
				value = (String)p_searchCriteria.get(str);
		        System.out.println(str + ": " + value);
				if(value != null && !value.equalsIgnoreCase("") && (
						 str.equals("start_date_start")
						|| str.equals("start_date_end")
						|| str.equals("end_date_start")
						|| str.equals("end_date_end")
						|| str.equals("insertion_date_start")
						|| str.equals("insertion_date_end")
				)
						
				)
				{
					if(p_searchCriteria.get(str).equals("any"))
					{
						continue;
					}

					if( i > 0)
					{
						AllFieldSql+= " AND  ";
					}
					
					 if(str.equals("start_date_start"))
					{
						AllFieldSql += "" + tableName + ".start_date >= " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("start_date_end"))
					{
						AllFieldSql += "" + tableName + ".start_date <= " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("end_date_start"))
					{
						AllFieldSql += "" + tableName + ".end_date >= " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("end_date_end"))
					{
						AllFieldSql += "" + tableName + ".end_date <= " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("insertion_date_start"))
					{
						AllFieldSql += "" + tableName + ".insertion_date >= " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("insertion_date_end"))
					{
						AllFieldSql += "" + tableName + ".insertion_date <= " + p_searchCriteria.get(str);
						i ++;
					}
					
					
				}
			}
			
			AllFieldSql += ")";
			System.out.println("AllFieldSql = " + AllFieldSql);
			
			
		}
		
		
		sql += " WHERE ";
		
		sql += " (" + tableName + ".isDeleted = 0 ";			
		sql += ")";
		
		
		if(!filter.equalsIgnoreCase(""))
		{
			sql += " and " + filter + " ";
		}
		
		if(!AnyfieldSql.equals("()") && !AnyfieldSql.equals(""))
		{
			sql += " AND " + AnyfieldSql;
			
		}
		if(!AllFieldSql.equals("()") && !AllFieldSql.equals(""))
		{			
			sql += " AND " + AllFieldSql;
		}
		
	
			
		sql += " order by " + tableName + ".lastModificationTime desc ";

		printSql(sql);
		
		if(limit >= 0)
		{
			sql += " limit " + limit;
		}
		if(offset >= 0)
		{
			sql += " offset " + offset;
		}
		
		System.out.println("-------------- sql = " + sql);
		
		return sql;
    }
	
	public String getSqlWithSearchCriteria(Hashtable p_searchCriteria, int limit, int offset, int category,
			boolean isPermanentTable, UserDTO userDTO, String filter, boolean tableHasJobCat)
    {		
		return getSqlWithSearchCriteriaForNormalSearch(p_searchCriteria, limit, offset, category, userDTO, filter, tableHasJobCat);	
    }

    public String buildOptionMonth(String language) {
		List<OptionDTO> optionDTOList = new ArrayList<>();
		List<String> monthsEng = Arrays.asList("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
		List<String> monthsBng = Arrays.asList("জানুয়ারী", "ফেব্রুয়ারী", "মার্চ", "এপ্রিল", "মে", "জুন", "জুলাই", "আগস্ট", "সেপ্টেম্বর", "অক্টোবর", "নভেম্বর", "ডিসেম্বর");
		for(int i=0;i<monthsEng.size();i++) {
			optionDTOList.add(new OptionDTO(monthsEng.get(i), monthsBng.get(i), String.valueOf(i+1)));
		}
		return Utils.buildOptions(optionDTOList,language,null);
	}
	public String buildOptionYear(String language) {
		List<OptionDTO> optionDTOList = new ArrayList<>();
		for(int i=2021;i>=1970;i--) {
			optionDTOList.add(new OptionDTO(String.valueOf(i), StringUtils.convertToBanNumber(String.valueOf(i)), String.valueOf(i)));
		}
		return Utils.buildOptions(optionDTOList,language,null);
	}
	public String buildOptionReportType(String language) {
		List<OptionDTO> optionDTOList = new ArrayList<>();
		optionDTOList.add(new OptionDTO("Summary", "হাজিরা সামারি","1"));
		return Utils.buildOptions(optionDTOList,language,null);
	}
				
}
	