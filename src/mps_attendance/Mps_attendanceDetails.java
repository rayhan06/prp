package mps_attendance;

public class Mps_attendanceDetails
{
	public long electionConstituencyId;
	public long employeeRecordsId;
	public long electionDetailsId;
	public String inTime;
	public String outTime;
	public long date;
	public int sessionDays;
	public int presentDays;
	public int absentDays;

	public long getDate() {
		return date;
	}
}