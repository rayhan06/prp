package mps_attendance;
import java.util.*; 
import util.*; 


public class Mps_attendanceDTO extends CommonDTO
{

	public long electionDetailsId = 0;
	public long electionConstituencyId = 0;
	public long employeeRecordsId = 0;
	public long politicalPartyId = 0;
	public long startDate = 0;
	public long endDate = 0;
	public long insertionDate = 0;
    public String insertedBy = "";
    public String modifiedBy = "";
	
	
    @Override
	public String toString() {
            return "$Mps_attendanceDTO[" +
            " iD = " + iD +
            " electionDetailsId = " + electionDetailsId +
            " electionConstituencyId = " + electionConstituencyId +
            " employeeRecordsId = " + employeeRecordsId +
            " politicalPartyId = " + politicalPartyId +
            " startDate = " + startDate +
            " endDate = " + endDate +
            " insertionDate = " + insertionDate +
            " insertedBy = " + insertedBy +
            " modifiedBy = " + modifiedBy +
            " searchColumn = " + searchColumn +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}