//package mps_attendance;
//
//import java.util.ArrayList;
//import java.util.HashSet;
//import java.util.List;
//import java.util.Map;
//import java.util.Set;
//import java.util.concurrent.ConcurrentHashMap;
//
//import org.apache.log4j.Logger;
//
//import repository.Repository;
//import repository.RepositoryManager;
//
//
//public class Mps_attendanceRepository implements Repository {
//	Mps_attendanceDAO mps_attendanceDAO = null;
//
//	public void setDAO(Mps_attendanceDAO mps_attendanceDAO)
//	{
//		this.mps_attendanceDAO = mps_attendanceDAO;
//	}
//
//
//	static Logger logger = Logger.getLogger(Mps_attendanceRepository.class);
//	Map<Long, Set<Mps_attendanceDTO> >mapOfMps_attendanceDTOToiD;
//
//
//	static Mps_attendanceRepository instance = null;
//	private Mps_attendanceRepository(){
//		mapOfMps_attendanceDTOToiD = new ConcurrentHashMap<>();
//
//		RepositoryManager.getInstance().addRepository(this);
//	}
//
//	public synchronized static Mps_attendanceRepository getInstance(){
//		if (instance == null){
//			instance = new Mps_attendanceRepository();
//		}
//		return instance;
//	}
//
//	public void reload(boolean reloadAll){
//		if(mps_attendanceDAO == null)
//		{
//			return;
//		}
//		try {
//			List<Mps_attendanceDTO> mps_attendanceDTOs = mps_attendanceDAO.getAllMps_attendance(reloadAll);
//			for(Mps_attendanceDTO mps_attendanceDTO : mps_attendanceDTOs) {
//				Mps_attendanceDTO oldMps_attendanceDTO = mapOfMps_attendanceDTOToiD(mps_attendanceDTO.iD);
//				if( oldMps_attendanceDTO != null ) {
//					if(mapOfMps_attendanceDTOToiD.containsKey(oldMps_attendanceDTO.iD)) {
//						mapOfMps_attendanceDTOToiD.get(oldMps_attendanceDTO.iD).remove(oldMps_attendanceDTO);
//					}
//					if(mapOfMps_attendanceDTOToiD.get(oldMps_attendanceDTO.iD).isEmpty()) {
//						mapOfMps_attendanceDTOToiD.remove(oldMps_attendanceDTO.iD);
//					}
//
//
//				}
//				if(mps_attendanceDTO.isDeleted == 0)
//				{
//
//					if( ! mapOfMps_attendanceDTOToiD.containsKey(mps_attendanceDTO.iD)) {
//						mapOfMps_attendanceDTOToiD.put(mps_attendanceDTO.iD, new HashSet<>());
//					}
//					mapOfMps_attendanceDTOToiD.get(mps_attendanceDTO.iD).add(mps_attendanceDTO);
//
//				}
//			}
//
//		} catch (Exception e) {
//			logger.debug("FATAL", e);
//		}
//	}
//
//	public List<Mps_attendanceDTO> getMps_attendanceList() {
//		List <Mps_attendanceDTO> mps_attendances = new ArrayList<Mps_attendanceDTO>(this.mapOfMps_attendanceDTOToiD.values());
//		return mps_attendances;
//	}
//
//
//
//	public List<Mps_attendanceDTO> getMps_attendanceDTOByID(long ID) {
//		return new ArrayList<>( mapOfMps_attendanceDTOToiD.getOrDefault(ID,new HashSet<>()));
//	}
//
//
//	@Override
//	public String getTableName() {
//		String tableName = "";
//		try{
//			tableName = "mps_attendance";
//		}catch(Exception ex){
//			logger.debug("FATAL",ex);
//		}
//		return tableName;
//	}
//}
//
//
