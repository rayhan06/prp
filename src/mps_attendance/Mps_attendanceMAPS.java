package mps_attendance;
import java.util.*; 
import util.*;


public class Mps_attendanceMAPS extends CommonMaps
{	
	public Mps_attendanceMAPS(String tableName)
	{
		

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("electionDetailsId".toLowerCase(), "electionDetailsId".toLowerCase());
		java_DTO_map.put("electionConstituencyId".toLowerCase(), "electionConstituencyId".toLowerCase());
		java_DTO_map.put("employeeRecordsId".toLowerCase(), "employeeRecordsId".toLowerCase());
		java_DTO_map.put("politicalPartyId".toLowerCase(), "politicalPartyId".toLowerCase());
		java_DTO_map.put("startDate".toLowerCase(), "startDate".toLowerCase());
		java_DTO_map.put("endDate".toLowerCase(), "endDate".toLowerCase());
		java_DTO_map.put("insertionDate".toLowerCase(), "insertionDate".toLowerCase());
		java_DTO_map.put("insertedBy".toLowerCase(), "insertedBy".toLowerCase());
		java_DTO_map.put("modifiedBy".toLowerCase(), "modifiedBy".toLowerCase());
		java_DTO_map.put("searchColumn".toLowerCase(), "searchColumn".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("election_details_id".toLowerCase(), "electionDetailsId".toLowerCase());
		java_SQL_map.put("election_constituency_id".toLowerCase(), "electionConstituencyId".toLowerCase());
		java_SQL_map.put("employee_records_id".toLowerCase(), "employeeRecordsId".toLowerCase());
		java_SQL_map.put("political_party_id".toLowerCase(), "politicalPartyId".toLowerCase());
		java_SQL_map.put("start_date".toLowerCase(), "startDate".toLowerCase());
		java_SQL_map.put("end_date".toLowerCase(), "endDate".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Election Details Id".toLowerCase(), "electionDetailsId".toLowerCase());
		java_Text_map.put("Election Constituency Id".toLowerCase(), "electionConstituencyId".toLowerCase());
		java_Text_map.put("Employee Records Id".toLowerCase(), "employeeRecordsId".toLowerCase());
		java_Text_map.put("Political Party Id".toLowerCase(), "politicalPartyId".toLowerCase());
		java_Text_map.put("Start Date".toLowerCase(), "startDate".toLowerCase());
		java_Text_map.put("End Date".toLowerCase(), "endDate".toLowerCase());
		java_Text_map.put("Insertion Date".toLowerCase(), "insertionDate".toLowerCase());
		java_Text_map.put("Inserted By".toLowerCase(), "insertedBy".toLowerCase());
		java_Text_map.put("Modified By".toLowerCase(), "modifiedBy".toLowerCase());
		java_Text_map.put("Search Column".toLowerCase(), "searchColumn".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}