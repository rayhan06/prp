package mps_attendance;

import java.io.IOException;
import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import election_constituency.Election_constituencyRepository;
import election_wise_mp.Election_wise_mpDTO;
import election_wise_mp.Election_wise_mpRepository;
import employee_attendance.Employee_attendanceDAO;
import employee_attendance.Employee_attendanceDTO;
import employee_records.Employee_recordsRepository;
import language.LC;
import language.LM;
import org.apache.log4j.Logger;

import login.LoginDTO;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import parliament_session.ParliamentSessionDateDAO;
import parliament_session.ParliamentSessionDateDTO;
import parliament_session.Parliament_sessionDTO;
import parliament_session.Parliament_sessionRepository;
import permission.MenuConstants;
import role.PermissionRepository;


import sessionmanager.SessionConstants;

import test_lib.util.Pair;
import user.UserDTO;
import user.UserRepository;
import util.*;

import java.util.*;
import javax.servlet.http.*;


import java.util.stream.Collectors;

import com.google.gson.Gson;

import pb.*;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import util.DateUtils;


/**
 * Servlet implementation class Mps_attendanceServlet
 */
@WebServlet("/Mps_attendanceServlet")
@MultipartConfig
public class Mps_attendanceServlet extends HttpServlet 
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Mps_attendanceServlet.class);

    String tableName = "mps_attendance";

	Mps_attendanceDAO mps_attendanceDAO;
	CommonRequestHandler commonRequestHandler;
    private final Gson gson = new Gson();
    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Mps_attendanceServlet() 
	{
        super();
    	try
    	{
			mps_attendanceDAO = new Mps_attendanceDAO(tableName);
			commonRequestHandler = new CommonRequestHandler(mps_attendanceDAO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }   

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		System.out.println("In doget request = " + request);
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		try
		{
			String actionType = request.getParameter("actionType");
			if(actionType.equals("getAddPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.MPS_ATTENDANCE_ADD))
				{
					commonRequestHandler.getAddPage(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getEditPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.MPS_ATTENDANCE_UPDATE))
				{
					getMps_attendance(request, response);					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}						
			}
			else if(actionType.equals("getURL"))
			{
				String URL = request.getParameter("URL");
				System.out.println("URL = " + URL);
				response.sendRedirect(URL);			
			}			
			else if(actionType.equals("search"))
			{
				System.out.println("search requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.MPS_ATTENDANCE_SEARCH))
				{
					if(isPermanentTable)
					{
						String filter = request.getParameter("filter");
						System.out.println("filter = " + filter);
						if(filter!=null)
						{
							filter = ""; //shouldn't be directly used, rather manipulate it.
							searchMps_attendance(request, response, isPermanentTable, filter);
						}
						else
						{
							searchMps_attendance(request, response, isPermanentTable, "");
						}
					}					
					else
					{
						//searchMps_attendance(request, response, tempTableName, isPermanentTable);
					}
				}			
			}
			else if(actionType.equals("view"))
			{
				System.out.println("view requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.MPS_ATTENDANCE_SEARCH))
				{
					commonRequestHandler.view(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			} else if(actionType.equals("downloadMPAttendanceReport")) {
				downloadMpsAttendanceReport(request,response,loginDTO);
			} else
			{
				request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}
	private void downloadMpsAttendanceReport(HttpServletRequest request, HttpServletResponse response,LoginDTO loginDTO) {
		String electionDetailsId = request.getParameter("electionDetailsId");
		String electionConstituencyId = request.getParameter("electionConstituencyId");
		String employeeRecordsId = request.getParameter("employeeRecordsId");
		String fromDate = request.getParameter("fromDate");
		String toDate = request.getParameter("toDate");
		String date = request.getParameter("date");
		String month = request.getParameter("month");
		String year = request.getParameter("year");
		String reportType = request.getParameter("reportType");
		String parliamentSessionId = request.getParameter("parliamentSessionId");
		String attendanceDeviceId = request.getParameter("attendanceDeviceId");
		List<Mps_attendanceDetails>	mps_attendanceDetails = null;

		Long fromDateLong=null;
		Long toDateLong=null;

		Pair<Long,Long> dateRange = new Employee_attendanceDAO().parseDateRangeFromSearchParam(fromDate,toDate,month,year,date);

		if(dateRange.getKey()!=null)
			fromDateLong = dateRange.getKey();
		if(dateRange.getValue()!=null)
			toDateLong = dateRange.getValue();

		if(parliamentSessionId!=null && !parliamentSessionId.isEmpty()) {

			Parliament_sessionDTO parliament_sessionDTO = Parliament_sessionRepository.getInstance().getParliament_sessionDTOByID(Long.parseLong(parliamentSessionId));

			if(fromDateLong==null) {
				fromDateLong = parliament_sessionDTO.startDate;
			}

			if(toDateLong==null) {
				toDateLong = parliament_sessionDTO.endDate;
			}
		}

		long deviceId = -1;
		if(attendanceDeviceId != null && !attendanceDeviceId.isEmpty()) {

			try {

				deviceId = Long.parseLong(attendanceDeviceId);

			} catch (Exception e) {

			}
		}

		List<Long> employeeRecordsIds = getEmployeeRecordsFromSearchParam(electionDetailsId,electionConstituencyId,employeeRecordsId);

		try {
			if(reportType.equals("1") && parliamentSessionId!=null && !parliamentSessionId.isEmpty()) {
				mps_attendanceDetails = getMpAttendanceSummary(employeeRecordsIds,Long.parseLong(parliamentSessionId),Long.parseLong(electionDetailsId), deviceId);
				downloadExcelForMpAttendanceSummary(mps_attendanceDetails,request,response,loginDTO);
			} else {
				mps_attendanceDetails = getAttendanceForMultipleMPDateRange(fromDateLong,toDateLong,employeeRecordsIds, Long.parseLong(electionDetailsId), deviceId);
				downloadExcelForEmployeeAttendance(mps_attendanceDetails,request,response,loginDTO);
			}


		} catch (Exception e) {
		}


	}
	private void downloadExcelForEmployeeAttendance(List<Mps_attendanceDetails> report,HttpServletRequest request, HttpServletResponse response,LoginDTO loginDTO) {
		try {
			String file_name = "honorable-mps-attendance_report.xlsx";
			response.setContentType("application/vnd.ms-excel");
			response.setHeader("Content-Disposition", "attachment; filename="+file_name);

			XSSFWorkbook workbook    = UtilCharacter.newWorkBook();
			int row_index=0;
			int cell_col_index=0;
			boolean addMerged=true;

			Sheet sheet = UtilCharacter.newSheet(workbook, "attendance_report");
			Row newRow = sheet.createRow(row_index);
			IndexedColors indexedColor = IndexedColors.ROYAL_BLUE;
			boolean isBold=true;
			String language = LM.getLanguageIDByUserDTO(UserRepository.getUserDTOByUserID(loginDTO)) == CommonConstant.Language_ID_English ? "English" : "Bangla";

			UtilCharacter.alreadyCreatedRow(workbook,sheet,LM.getText(LC.ELECTION_CONSTITUENCY_ADD_CONSTITUENCYNUMBER, loginDTO),row_index,row_index,cell_col_index,cell_col_index+1,addMerged,newRow,indexedColor,isBold);
			cell_col_index+=2;
			UtilCharacter.alreadyCreatedRow(workbook,sheet,LM.getText(LC.MPS_ATTENDANCE_SEARCH_ELECTIONCONSTITUENCYID, loginDTO),row_index,row_index,cell_col_index,cell_col_index+1,addMerged,newRow,indexedColor,isBold);
			cell_col_index+=2;
			UtilCharacter.alreadyCreatedRow(workbook,sheet,LM.getText(LC.MPS_ATTENDANCE_SEARCH_EMPLOYEERECORDSID, loginDTO),row_index,row_index,cell_col_index,cell_col_index+2,addMerged,newRow,indexedColor,isBold);
			cell_col_index+=3;
			UtilCharacter.alreadyCreatedRow(workbook,sheet,LM.getText(LC.HM_DATE, loginDTO),row_index,row_index,cell_col_index,cell_col_index+1,addMerged,newRow,indexedColor,isBold);
			cell_col_index+=2;
			UtilCharacter.alreadyCreatedRow(workbook,sheet,LM.getText(LC.OFFICE_SHIFT_DETAILS_ADD_INTIME, loginDTO),row_index,row_index,cell_col_index,cell_col_index++,false,newRow,indexedColor,isBold);
			UtilCharacter.alreadyCreatedRow(workbook,sheet,LM.getText(LC.OFFICE_SHIFT_DETAILS_ADD_OUTTIME, loginDTO),row_index,row_index,cell_col_index,cell_col_index++,false,newRow,indexedColor,isBold);


			for(Mps_attendanceDetails dto: report) {
				row_index++;
				cell_col_index=0;
				newRow = sheet.createRow(row_index);

				String employeeName = Employee_recordsRepository.getInstance().getEmployeeName(dto.employeeRecordsId, language);
				String date = StringUtils.getFormattedDate(language, dto.date);
				String constituencyNumber  = Election_constituencyRepository.getInstance().getConstituencyNumber(dto.electionConstituencyId);
				constituencyNumber = Utils.getDigits(constituencyNumber, language);
				String contituencyName = Election_constituencyRepository.getInstance().getText(dto.electionConstituencyId, language);

				UtilCharacter.alreadyCreatedRow(workbook,sheet,constituencyNumber,row_index,row_index,cell_col_index,cell_col_index+1,addMerged,newRow,indexedColor,isBold);
				cell_col_index+=2;
				UtilCharacter.alreadyCreatedRow(workbook,sheet,contituencyName,row_index,row_index,cell_col_index,cell_col_index+1,addMerged,newRow,indexedColor,isBold);
				cell_col_index+=2;
				UtilCharacter.alreadyCreatedRow(workbook,sheet,employeeName,row_index,row_index,cell_col_index,cell_col_index+2,addMerged,newRow,indexedColor,isBold);
				cell_col_index+=3;
				UtilCharacter.alreadyCreatedRow(workbook,sheet,date,row_index,row_index,cell_col_index,cell_col_index+1,addMerged,newRow,indexedColor,isBold);
				cell_col_index+=2;
				UtilCharacter.alreadyCreatedRow(workbook,sheet,Utils.getDigits(dto.inTime, language),row_index,row_index,cell_col_index,cell_col_index++,false,newRow,indexedColor,isBold);
				UtilCharacter.alreadyCreatedRow(workbook,sheet,Utils.getDigits(dto.outTime, language),row_index,row_index,cell_col_index,cell_col_index++,false,newRow,indexedColor,isBold);
			}
			workbook.write(response.getOutputStream());
			workbook.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	private void downloadExcelForMpAttendanceSummary(List<Mps_attendanceDetails> report,HttpServletRequest request, HttpServletResponse response,LoginDTO loginDTO) {
		try {
			String file_name = "honorable-mps-attendance_summary.xlsx";
			response.setContentType("application/vnd.ms-excel");
			response.setHeader("Content-Disposition", "attachment; filename="+file_name);

			XSSFWorkbook workbook    = UtilCharacter.newWorkBook();
			int row_index=0;
			int cell_col_index=0;
			boolean addMerged=true;

			Sheet sheet = UtilCharacter.newSheet(workbook, "attendance_report");
			Row newRow = sheet.createRow(row_index);
			IndexedColors indexedColor = IndexedColors.ROYAL_BLUE;
			boolean isBold=true;
			String language = LM.getLanguageIDByUserDTO(UserRepository.getUserDTOByUserID(loginDTO)) == CommonConstant.Language_ID_English ? "English" : "Bangla";

			UtilCharacter.alreadyCreatedRow(workbook,sheet,LM.getText(LC.ELECTION_CONSTITUENCY_ADD_CONSTITUENCYNUMBER, loginDTO),row_index,row_index,cell_col_index,cell_col_index+1,addMerged,newRow,indexedColor,isBold);
			cell_col_index+=2;
			UtilCharacter.alreadyCreatedRow(workbook,sheet,LM.getText(LC.MPS_ATTENDANCE_SEARCH_ELECTIONCONSTITUENCYID, loginDTO),row_index,row_index,cell_col_index,cell_col_index+1,addMerged,newRow,indexedColor,isBold);
			cell_col_index+=2;
			UtilCharacter.alreadyCreatedRow(workbook,sheet,LM.getText(LC.MPS_ATTENDANCE_SEARCH_EMPLOYEERECORDSID, loginDTO),row_index,row_index,cell_col_index,cell_col_index+2,addMerged,newRow,indexedColor,isBold);
			cell_col_index+=3;
			UtilCharacter.alreadyCreatedRow(workbook,sheet,LM.getText(LC.ATTENDANCE_TOTAL_SESSION, loginDTO),row_index,row_index,cell_col_index,cell_col_index++,false,newRow,indexedColor,isBold);
			UtilCharacter.alreadyCreatedRow(workbook,sheet,LM.getText(LC.RECRUITMENT_BULK_ATTENDANCE_SEARCH_PRESENT, loginDTO),row_index,row_index,cell_col_index,cell_col_index++,false,newRow,indexedColor,isBold);
			UtilCharacter.alreadyCreatedRow(workbook,sheet,LM.getText(LC.RECRUITMENT_BULK_ATTENDANCE_SEARCH_ABSENT, loginDTO),row_index,row_index,cell_col_index,cell_col_index++,false,newRow,indexedColor,isBold);


			for(Mps_attendanceDetails dto: report) {
				row_index++;
				cell_col_index=0;
				newRow = sheet.createRow(row_index);

				String employeeName = Employee_recordsRepository.getInstance().getEmployeeName(dto.employeeRecordsId, language);
				String totalSession = Utils.getDigits(dto.sessionDays, language);
				String present =  Utils.getDigits(dto.presentDays, language);
				String absent =  Utils.getDigits(dto.absentDays, language);
				String constituencyNumber  = Election_constituencyRepository.getInstance().getConstituencyNumber(dto.electionConstituencyId);
				constituencyNumber = Utils.getDigits(constituencyNumber, language);
				String contituencyName = Election_constituencyRepository.getInstance().getText(dto.electionConstituencyId, language);

				UtilCharacter.alreadyCreatedRow(workbook,sheet,constituencyNumber,row_index,row_index,cell_col_index,cell_col_index+1,addMerged,newRow,indexedColor,isBold);
				cell_col_index+=2;
				UtilCharacter.alreadyCreatedRow(workbook,sheet,contituencyName,row_index,row_index,cell_col_index,cell_col_index+1,addMerged,newRow,indexedColor,isBold);
				cell_col_index+=2;
				UtilCharacter.alreadyCreatedRow(workbook,sheet,employeeName,row_index,row_index,cell_col_index,cell_col_index+2,addMerged,newRow,indexedColor,isBold);
				cell_col_index+=3;
				UtilCharacter.alreadyCreatedRow(workbook,sheet,totalSession,row_index,row_index,cell_col_index,cell_col_index++,false,newRow,indexedColor,isBold);
				UtilCharacter.alreadyCreatedRow(workbook,sheet,present,row_index,row_index,cell_col_index,cell_col_index++,false,newRow,indexedColor,isBold);
				UtilCharacter.alreadyCreatedRow(workbook,sheet,absent,row_index,row_index,cell_col_index,cell_col_index++,false,newRow,indexedColor,isBold);
			}
			workbook.write(response.getOutputStream());
			workbook.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	
	

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		// TODO Auto-generated method stub
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		System.out.println("doPost");
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		
		try
		{
			String actionType = request.getParameter("actionType");
			System.out.println("actionType = " + actionType);
			if(actionType.equals("add"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.MPS_ATTENDANCE_ADD))
				{
					System.out.println("going to  addMps_attendance ");
					addMps_attendance(request, response, true, userDTO, true);
				}
				else
				{
					System.out.println("Not going to  addMps_attendance ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}

			else if(actionType.equals("getDTO"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.MPS_ATTENDANCE_ADD))
				{
					getDTO(request, response);					
				}
				else
				{
					System.out.println("Not going to  addMps_attendance ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else if(actionType.equals("edit"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.MPS_ATTENDANCE_UPDATE))
				{					
					addMps_attendance(request, response, false, userDTO, isPermanentTable);					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("delete"))
			{								
				deleteMps_attendance(request, response, userDTO);				
			}	
			else if(actionType.equals("search"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.MPS_ATTENDANCE_SEARCH))
				{
					searchMps_attendance(request, response, true, "");
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getGeo"))
			{
				System.out.println("going to geoloc ");
				request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
			}
			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}
	
	private void getDTO(HttpServletRequest request, HttpServletResponse response) 
	{
		try 
		{
			System.out.println("In getDTO");
			Mps_attendanceDTO mps_attendanceDTO = mps_attendanceDAO.getDTOByID(Long.parseLong(request.getParameter("ID")));
			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			
			String encoded = this.gson.toJson(mps_attendanceDTO);
			System.out.println("json encoded data = " + encoded);
			out.print(encoded);
			out.flush();
		}
		catch (NumberFormatException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	private void addMps_attendance(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, boolean isPermanentTable) throws IOException 
	{
		// TODO Auto-generated method stub
		try 
		{
			request.setAttribute("failureMessage", "");
			System.out.println("%%%% addMps_attendance");
			String path = getServletContext().getRealPath("/img2/");
			Mps_attendanceDTO mps_attendanceDTO;
			SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
						
			if(addFlag == true)
			{
				mps_attendanceDTO = new Mps_attendanceDTO();
			}
			else
			{
				mps_attendanceDTO = mps_attendanceDAO.getDTOByID(Long.parseLong(request.getParameter("iD")));
			}
			
			String Value = "";

			Value = request.getParameter("iD");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("iD = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				mps_attendanceDTO.iD = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("electionDetailsId");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("electionDetailsId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				mps_attendanceDTO.electionDetailsId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("electionConstituencyId");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("electionConstituencyId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				mps_attendanceDTO.electionConstituencyId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("employeeRecordsId");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("employeeRecordsId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				mps_attendanceDTO.employeeRecordsId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("politicalPartyId");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("politicalPartyId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				mps_attendanceDTO.politicalPartyId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("startDate");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("startDate = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				try 
				{
					Date d = f.parse(Value);
					mps_attendanceDTO.startDate = d.getTime();
				}
				catch (Exception e) 
				{
					e.printStackTrace();
				}
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("endDate");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("endDate = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				try 
				{
					Date d = f.parse(Value);
					mps_attendanceDTO.endDate = d.getTime();
				}
				catch (Exception e) 
				{
					e.printStackTrace();
				}
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			if(addFlag)
			{
				Calendar c = Calendar.getInstance();
				c.set(Calendar.HOUR_OF_DAY, 0);
				c.set(Calendar.MINUTE, 0);
				c.set(Calendar.SECOND, 0);
				c.set(Calendar.MILLISECOND, 0);
				
				mps_attendanceDTO.insertionDate = c.getTimeInMillis();
			}			


			Value = request.getParameter("insertedBy");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("insertedBy = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				mps_attendanceDTO.insertedBy = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("modifiedBy");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("modifiedBy = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				mps_attendanceDTO.modifiedBy = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("searchColumn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("searchColumn = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				mps_attendanceDTO.searchColumn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			
			System.out.println("Done adding  addMps_attendance dto = " + mps_attendanceDTO);
			long returnedID = -1;
			
			if(isPermanentTable == false) //add new row for validation and make the old row outdated
			{
				mps_attendanceDAO.setIsDeleted(mps_attendanceDTO.iD, CommonDTO.OUTDATED);
				returnedID = mps_attendanceDAO.add(mps_attendanceDTO);
				mps_attendanceDAO.setIsDeleted(returnedID, CommonDTO.WAITING_FOR_APPROVAL);
			}
			else if(addFlag == true)
			{
				returnedID = mps_attendanceDAO.manageWriteOperations(mps_attendanceDTO, SessionConstants.INSERT, -1, userDTO);
			}
			else
			{				
				returnedID = mps_attendanceDAO.manageWriteOperations(mps_attendanceDTO, SessionConstants.UPDATE, -1, userDTO);											
			}
			
			
			
			
			
			
			
			
			
			if(isPermanentTable)
			{
				String inPlaceSubmit = request.getParameter("inplacesubmit");
				
				if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
				{
					getMps_attendance(request, response, returnedID);
				}
				else
				{
					response.sendRedirect("Mps_attendanceServlet?actionType=search");
				}
			}
			else
			{
				commonRequestHandler.validate(mps_attendanceDAO.getDTOByID(returnedID), request, response, userDTO);
			}
					
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	



	
	
	

	private void deleteMps_attendance(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO) throws IOException 
	{				
		try 
		{
			String[] IDsToDelete = request.getParameterValues("ID");
			for(int i = 0; i < IDsToDelete.length; i ++)
			{
				long id = Long.parseLong(IDsToDelete[i]);
				System.out.println("------ DELETING " + IDsToDelete[i]);
				
				
				Mps_attendanceDTO mps_attendanceDTO = mps_attendanceDAO.getDTOByID(id);
				mps_attendanceDAO.manageWriteOperations(mps_attendanceDTO, SessionConstants.DELETE, id, userDTO);
				
				
			}
			response.sendRedirect("Mps_attendanceServlet?actionType=search");
		}
		catch (Exception ex) 
		{
			ex.printStackTrace();
		}
		
	}

	private void getMps_attendance(HttpServletRequest request, HttpServletResponse response, long id) throws ServletException, IOException
	{
		System.out.println("in getMps_attendance");
		Mps_attendanceDTO mps_attendanceDTO = null;
		try 
		{
			mps_attendanceDTO = mps_attendanceDAO.getDTOByID(id);
			request.setAttribute("ID", mps_attendanceDTO.iD);
			request.setAttribute("mps_attendanceDTO",mps_attendanceDTO);
			request.setAttribute("mps_attendanceDAO",mps_attendanceDAO);
			
			String URL= "";
			
			String inPlaceEdit = request.getParameter("inplaceedit");
			String inPlaceSubmit = request.getParameter("inplacesubmit");
			String getBodyOnly = request.getParameter("getBodyOnly");
			
			if(inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase(""))
			{
				URL = "mps_attendance/mps_attendanceInPlaceEdit.jsp";	
				request.setAttribute("inplaceedit","");				
			}
			else if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
			{
				URL = "mps_attendance/mps_attendanceSearchRow.jsp";
				request.setAttribute("inplacesubmit","");					
			}
			else
			{
				if(getBodyOnly  != null && !getBodyOnly.equalsIgnoreCase(""))
				{
					URL = "mps_attendance/mps_attendanceEditBody.jsp?actionType=edit";
				}
				else
				{
					URL = "mps_attendance/mps_attendanceEdit.jsp?actionType=edit";
				}				
			}
			
			RequestDispatcher rd = request.getRequestDispatcher(URL);
			rd.forward(request, response);
		}
		catch (NumberFormatException e) 
		{
			e.printStackTrace();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	
	private void getMps_attendance(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		getMps_attendance(request, response, Long.parseLong(request.getParameter("ID")));	
	}
	
	private void searchMps_attendance(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException
	{
		List<Mps_attendanceDetails>	mps_attendanceDetails = new ArrayList<>();
		System.out.println("in  searchMps_attendance 1");
		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String ajax = request.getParameter("ajax");
		boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);


		String electionDetailsId = request.getParameter("election_details_id");
		String electionConstituencyId = request.getParameter("election_constituency_id");
		String employeeRecordsId = request.getParameter("employee_records_id");
		String parliamentSessionId = request.getParameter("parliament_session_id");
		String fromDate = request.getParameter("from_date");
		String toDate = request.getParameter("to_date");
		String date = request.getParameter("date");
		String month = request.getParameter("month");
		String year = request.getParameter("year");
		String reportType = request.getParameter("reportType");
		String attendanceDeviceId = request.getParameter("attendance_device_id");

        Long fromDateLong=null;
        Long toDateLong=null;

		Pair<Long,Long> dateRange = new Employee_attendanceDAO().parseDateRangeFromSearchParam(fromDate,toDate,month,year,date);

		if(dateRange.getKey()!=null)
			fromDateLong = dateRange.getKey();
		if(dateRange.getValue()!=null)
			toDateLong = dateRange.getValue();

		if(parliamentSessionId!=null && !parliamentSessionId.isEmpty()) {

			Parliament_sessionDTO parliament_sessionDTO = Parliament_sessionRepository.getInstance().getParliament_sessionDTOByID(Long.parseLong(parliamentSessionId));

			if(fromDateLong==null) {
				fromDateLong = parliament_sessionDTO.startDate;
			}

			if(toDateLong==null) {
				toDateLong = parliament_sessionDTO.endDate;
			}
		}
		long deviceId = -1;
		if(attendanceDeviceId != null && !attendanceDeviceId.isEmpty()) {

			try {

				deviceId = Long.parseLong(attendanceDeviceId);

			} catch (Exception e) {

			}
		}

		List<Long> employeeRecordsIds = getEmployeeRecordsFromSearchParam(electionDetailsId,electionConstituencyId,employeeRecordsId);

        try {
            if(reportType.equals("1") && parliamentSessionId!=null && !parliamentSessionId.isEmpty()) {
				mps_attendanceDetails = getMpAttendanceSummary(employeeRecordsIds,Long.parseLong(parliamentSessionId),Long.parseLong(electionDetailsId), deviceId );
			} else {
				mps_attendanceDetails = getAttendanceForMultipleMPDateRange(fromDateLong,toDateLong,employeeRecordsIds, Long.parseLong(electionDetailsId), deviceId );
			}


		} catch (Exception e) {
		}


     /*   if(date!=null && !date.isEmpty()) {
			mps_attendanceDetails = getAttendanceForMultipleMpSingleDate(date, electionDetailsId);
		} else if(fromDate!=null && !fromDate.isEmpty() && toDate!=null && !toDate.isEmpty()){
			mps_attendanceDetails = getAttendanceForSingleMPDateRange(fromDate, toDate, electionDetailsId, electionConstituencyId, employeeRecordsId);
		} else if(month!=null && !month.isEmpty() && year!=null && !year.isEmpty()) {
			mps_attendanceDetails = getAttendanceForSingleMPDateRange(constructDateTime(month, year, true), constructDateTime(month, year, false), electionDetailsId, electionConstituencyId, employeeRecordsId);
		}*/


        request.setAttribute("mps_attendanceDetails", mps_attendanceDetails);
		request.setAttribute("mps_attendanceDAO",mps_attendanceDAO);
		request.setAttribute("electionDetailsId",electionDetailsId);
		request.setAttribute("electionConstituencyId",electionConstituencyId);
		request.setAttribute("employeeRecordsId",employeeRecordsId);
		request.setAttribute("fromDate",fromDate);
		request.setAttribute("toDate",toDate);
		request.setAttribute("date",date);
		request.setAttribute("month",month);
		request.setAttribute("year",year);
		request.setAttribute("parliamentSessionId",parliamentSessionId);
		request.setAttribute("reportType",reportType);
		request.setAttribute("attendanceDeviceId", attendanceDeviceId);
        RequestDispatcher rd;

        if(!isPermanent)
        {
        	if(hasAjax == false)
	        {
	        	System.out.println("Going to mps_attendance/mps_attendanceApproval.jsp");
	        	rd = request.getRequestDispatcher("mps_attendance/mps_attendanceApproval.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to mps_attendance/mps_attendanceApprovalForm.jsp");
	        	rd = request.getRequestDispatcher("mps_attendance/mps_attendanceApprovalForm.jsp");
	        }
        }
        else
        {
	        if(hasAjax == false)
	        {
	        	System.out.println("Going to mps_attendance/mps_attendanceSearch.jsp");
	        	rd = request.getRequestDispatcher("mps_attendance/mps_attendanceSearch.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to mps_attendance/mps_attendanceSearchForm.jsp");
	        	rd = request.getRequestDispatcher("mps_attendance/mps_attendanceSearchForm.jsp");
	        }
        }
		rd.forward(request, response);
	}

	private String constructDateTime(String month,String year,boolean isFirstDate) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        String day="1";
		if(!isFirstDate) {
			day = String.valueOf(DateUtils.getDaysInMonth(Integer.parseInt(month)-1, Integer.parseInt(year)));
		}
		Date d = null;
		try {
			d = dateFormat.parse(day.concat("/").concat(month).concat("/").concat(year));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return String.valueOf(d.getTime());
	}

	private List<Mps_attendanceDetails> getAttendanceForSingleMPDateRange(String fromDate, String toDate, String electionDetailsId, String electionConstituencyId, String employeeRecordsId) {

		List<Employee_attendanceDTO> allAttendance = new ArrayList<>();

		long from = Long.parseLong(fromDate); long to = Long.parseLong(toDate);

		List<Long> employeeRecordsIds = getEmployeeRecordsFromSearchParam(electionDetailsId,electionConstituencyId,employeeRecordsId);

		if(employeeRecordsIds==null || employeeRecordsIds.isEmpty()) return new ArrayList<>();

		allAttendance = new Employee_attendanceDAO().getEmployeeAttendanceInRange(from,to,employeeRecordsIds.get(0));
		Map<Long, Pair<String,String>> dateToAttendanceTimeMap = new HashMap<>();
		for(Employee_attendanceDTO dto: allAttendance) {

				if(dateToAttendanceTimeMap.containsKey(dto.inTimestamp)) {
					dateToAttendanceTimeMap.put(dto.inTimestamp, new Pair<>(getMinTime(dto.inTime, dateToAttendanceTimeMap.get(dto.inTimestamp).getKey()), getMaxTime(dto.inTime, dateToAttendanceTimeMap.get(dto.inTimestamp).getValue())));
				} else {
					dateToAttendanceTimeMap.put(dto.inTimestamp, new Pair<>(dto.inTime, dto.inTime));
				}

		}
		long detailsId = 0 ;

		if(electionDetailsId!=null && !electionDetailsId.isEmpty()) {
			detailsId = Long.parseLong(electionDetailsId);
		}
		List<Mps_attendanceDetails> mps_attendanceDetails = new ArrayList<>();
		long empId = employeeRecordsIds.get(0);
		long electionId = Long.parseLong(electionDetailsId);

		Election_wise_mpDTO mpDTO = Election_wise_mpRepository.getInstance().getByElectionAndRecordsId(electionId, empId);



		for(Map.Entry<Long, Pair<String,String>> entry: dateToAttendanceTimeMap.entrySet()) {

			Mps_attendanceDetails details = new Mps_attendanceDetails();

			details.employeeRecordsId = empId;
			details.electionConstituencyId = mpDTO.electionConstituencyId;
			details.electionDetailsId = electionId;
			details.date = entry.getKey();
			details.inTime = entry.getValue().getKey();
			details.outTime = entry.getValue().getValue();

			mps_attendanceDetails.add(details);
		}
		return mps_attendanceDetails;

	}
	private List<Mps_attendanceDetails> getMpAttendanceSummary(List<Long> employeeRecordsIds, long parliamentSessionId, long electionDetailsId, long deviceId ) throws Exception {

		Parliament_sessionDTO parliament_sessionDTO = Parliament_sessionRepository.getInstance().getParliament_sessionDTOByID(parliamentSessionId);
		List<ParliamentSessionDateDTO> parliamentSessionDateDTOList = new ParliamentSessionDateDAO().getParliamentSessionDateDTOListByParliamentSessionID(parliamentSessionId);

		Set<Long> sessionDates = new HashSet<>();
		for(ParliamentSessionDateDTO sessionDateDTO: parliamentSessionDateDTOList) {
			sessionDates.add(sessionDateDTO.sessionDate);
		}

		int totalSessionDays = sessionDates.size();
		long fromDate = parliament_sessionDTO.startDate;
		long toDate = parliament_sessionDTO.endDate;

		List<Employee_attendanceDTO> employeeAttendanceDTOS = null;

		if(deviceId ==-1 ) {

			employeeAttendanceDTOS = new Employee_attendanceDAO().getEmployeeAttendanceInRange( fromDate,toDate );
		} else {

			employeeAttendanceDTOS = new Employee_attendanceDAO().getEmployeeAttendanceInRangeWithDeviceId( fromDate, toDate, deviceId );
		}

		HashSet<Long> attendEmployees = new HashSet<>();
		for(Employee_attendanceDTO dto: employeeAttendanceDTOS) {
			attendEmployees.add(dto.employeeRecordsType);
		}

		List<Mps_attendanceDetails> mps_attendanceDetails = new ArrayList<>();

		for(Long employeeRecordsId: employeeRecordsIds) {

			int present=0;

			if(attendEmployees.contains(employeeRecordsId)) {

				List<Employee_attendanceDTO> attendanceDTOS = new Employee_attendanceDAO().getEmployeeAttendanceInRangeUniqueDate(fromDate,toDate,employeeRecordsId);

				for(Employee_attendanceDTO attendanceDTO: attendanceDTOS) {
					if(sessionDates.contains(attendanceDTO.inTimestamp)) {
						present++;
					}
				}
			}

			Mps_attendanceDetails details = new Mps_attendanceDetails();
			Election_wise_mpDTO mpDTO = Election_wise_mpRepository.getInstance().getByElectionAndRecordsId(electionDetailsId, employeeRecordsId);
			details.employeeRecordsId = employeeRecordsId;
			details.electionConstituencyId = mpDTO.electionConstituencyId;
			details.electionDetailsId = electionDetailsId;
			details.sessionDays = totalSessionDays;
			details.presentDays = present;
			details.absentDays = totalSessionDays - present;

			mps_attendanceDetails.add(details);
		}
		return mps_attendanceDetails;
	}
	public List<Mps_attendanceDetails> getAttendanceForMultipleMPDateRange(long fromDate, long toDate, List<Long> employeeRecordsIds, long electionDetailsId, long deviceId) {

		List<Employee_attendanceDTO> employeeAttendanceDTOS = null;

		if(deviceId == -1) {

			employeeAttendanceDTOS = new Employee_attendanceDAO().getEmployeeAttendanceInRange(fromDate,toDate);
		} else {

			employeeAttendanceDTOS = new Employee_attendanceDAO().getEmployeeAttendanceInRangeWithDeviceId( fromDate, toDate, deviceId );
		}

		HashSet<Long> attendEmployees = new HashSet<>();

		for(Employee_attendanceDTO dto: employeeAttendanceDTOS) {
			attendEmployees.add(dto.employeeRecordsType);
		}

		List<Mps_attendanceDetails> allMps_attendanceDetails = new ArrayList<>();

		for(Long employeeRecordsId: employeeRecordsIds) {

			if(attendEmployees.contains(employeeRecordsId)) {
				allMps_attendanceDetails.addAll(getAttendanceForSingleMPDateRange(fromDate,toDate,employeeRecordsId,electionDetailsId));
			}
		}

		return allMps_attendanceDetails;
	}
	private List<Mps_attendanceDetails> getAttendanceForSingleMPDateRange(long fromDate, long toDate,long employeeRecordsId, long electionDetailsId) {

		List<Employee_attendanceDTO> allAttendance = new ArrayList<>();

		allAttendance = new Employee_attendanceDAO().getEmployeeAttendanceInRange(fromDate,toDate,employeeRecordsId);
		Map<Long, Pair<String,String>> dateToAttendanceTimeMap = new HashMap<>();
		for(Employee_attendanceDTO dto: allAttendance) {

			if(dateToAttendanceTimeMap.containsKey(dto.inTimestamp)) {
				dateToAttendanceTimeMap.put(dto.inTimestamp, new Pair<>(getMinTime(dto.inTime, dateToAttendanceTimeMap.get(dto.inTimestamp).getKey()), getMaxTime(dto.inTime, dateToAttendanceTimeMap.get(dto.inTimestamp).getValue())));
			} else {
				dateToAttendanceTimeMap.put(dto.inTimestamp, new Pair<>(dto.inTime, dto.inTime));
			}

		}
		long detailsId = 0 ;

		List<Mps_attendanceDetails> mps_attendanceDetails = new ArrayList<>();

		Election_wise_mpDTO mpDTO = Election_wise_mpRepository.getInstance().getByElectionAndRecordsId(electionDetailsId, employeeRecordsId);



		for(Map.Entry<Long, Pair<String,String>> entry: dateToAttendanceTimeMap.entrySet()) {

			Mps_attendanceDetails details = new Mps_attendanceDetails();

			details.employeeRecordsId = employeeRecordsId;
			details.electionConstituencyId = mpDTO.electionConstituencyId;
			details.electionDetailsId = electionDetailsId;
			details.date = entry.getKey();
			details.inTime = entry.getValue().getKey();
			details.outTime = entry.getValue().getValue();

			mps_attendanceDetails.add(details);
		}
		return mps_attendanceDetails.stream()
				.sorted(Comparator.comparing(dto->dto.getDate()))
				.collect(Collectors.toList());
	}

	private List<Mps_attendanceDetails> getAttendanceForMultipleMpSingleDate(String date, String electionDetailsId) {
		List<Employee_attendanceDTO> allAttendance = new ArrayList<>();

		if(date != null && !date.isEmpty()) {
			allAttendance = new Employee_attendanceDAO().getAttendanceDetailsByDate(Long.parseLong(date));
		} else {
			return new ArrayList<>();
		}

		List<Long> employeeRecordsIds = getEmployeeRecordsFromSearchParam(electionDetailsId,"","");
		Set<Long> employeeRecordsSet = new HashSet<>(employeeRecordsIds);
		Map<Long, Pair<String,String>> employeeToAttendanceTimeMap = new HashMap<>();

		for(Employee_attendanceDTO dto: allAttendance) {
			if(employeeRecordsSet.contains(dto.employeeRecordsType)) {

				if(employeeToAttendanceTimeMap.containsKey(dto.employeeRecordsType)) {
					employeeToAttendanceTimeMap.put(dto.employeeRecordsType, new Pair<>(getMinTime(dto.inTime, employeeToAttendanceTimeMap.get(dto.employeeRecordsType).getKey()), getMaxTime(dto.inTime, employeeToAttendanceTimeMap.get(dto.employeeRecordsType).getValue())));
				} else {
					employeeToAttendanceTimeMap.put(dto.employeeRecordsType, new Pair<>(dto.inTime, dto.inTime));
				}

			}
		}

		List<Mps_attendanceDetails> mps_attendanceDetails = new ArrayList<>();
		long detailsId = 0 ;

		if(electionDetailsId!=null && !electionDetailsId.isEmpty()) {
			detailsId = Long.parseLong(electionDetailsId);
		}

		for(Map.Entry<Long, Pair<String,String>> entry: employeeToAttendanceTimeMap.entrySet()) {

			Mps_attendanceDetails details = getMpsDetailsByRecordsId(entry.getKey(), detailsId);
			details.date = Long.parseLong(date);
			details.inTime = entry.getValue().getKey();
			details.outTime = entry.getValue().getValue();

			mps_attendanceDetails.add(details);
		}
		return mps_attendanceDetails;
	}

	private Mps_attendanceDetails getMpsDetailsByRecordsId(long employeeRecordsId, long electionDetailsId) {
		Mps_attendanceDetails details = new Mps_attendanceDetails();

		Election_wise_mpDTO dto = Election_wise_mpRepository.getInstance().getByElectionAndRecordsId(electionDetailsId, employeeRecordsId);

		details.employeeRecordsId = employeeRecordsId;
		details.electionConstituencyId = dto.electionConstituencyId;
		details.electionDetailsId = electionDetailsId;

		return details;
	}

	private String getMinTime(String time1,String time2) {

		if(time1==null) return time2;
		if(time2==null) return time1;

		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");

		Date d1 = null;
		try {
			d1 = sdf.parse(time1);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		Date d2 = null;

		try {
			d2 = sdf.parse(time2);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		if(d1.getTime() < d2.getTime()) return time1;
		else return time2;
	}

	private String getMaxTime(String time1,String time2) {

		if(time1==null) return time2;
		if(time2==null) return time1;

		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");

		Date d1 = null;
		try {
			d1 = sdf.parse(time1);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		Date d2 = null;

		try {
			d2 = sdf.parse(time2);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		if(d1.getTime() < d2.getTime()) return time2;
		else return time1;
	}

	private List<Long> getEmployeeRecordsFromSearchParam(String electionDetailsIdStr, String electionConstituencyIdStr, String employeeRecordsIdStr) {

        long electionDetailsId = -1;
        long electionConstituencyId = -1;
        long employeeRecordsId = -1;

		if(electionDetailsIdStr!=null && !electionDetailsIdStr.isEmpty()) {
			electionDetailsId = Long.parseLong(electionDetailsIdStr);
		}

		if(electionConstituencyIdStr != null && !electionConstituencyIdStr.isEmpty()) {
			electionConstituencyId = Long.parseLong(electionConstituencyIdStr);
		}

		if(employeeRecordsIdStr != null && !employeeRecordsIdStr.isEmpty()) {
			employeeRecordsId = Long.parseLong(employeeRecordsIdStr);
		}

		if(electionDetailsId != -1 && electionConstituencyId != -1) {
			Election_wise_mpDTO election_wise_mpDTO = Election_wise_mpRepository.getInstance().getByElectionAndConstituency(electionDetailsId,electionConstituencyId);
			if(election_wise_mpDTO != null) return Arrays.asList(election_wise_mpDTO.employeeRecordsId);
		} if(electionDetailsId != -1 && employeeRecordsId != -1) {
			Election_wise_mpDTO election_wise_mpDTO = Election_wise_mpRepository.getInstance().getByElectionAndRecordsId(electionDetailsId, employeeRecordsId);
			if(election_wise_mpDTO != null) return Arrays.asList(election_wise_mpDTO.employeeRecordsId);
		} else if(electionDetailsId != -1) {
			List<Election_wise_mpDTO> dtos = Election_wise_mpRepository.getInstance().getElection_wise_mpDTOByElectionDetailsId(electionDetailsId);
			return dtos.stream().map(dto -> dto.employeeRecordsId).collect(Collectors.toList());
		}

		return new ArrayList<>();
	}
	
}

