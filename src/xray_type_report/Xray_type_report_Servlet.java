package xray_type_report;


import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import language.LC;
import language.LM;

import login.LoginDTO;
import pb.CommonDAO;
import permission.MenuConstants;

import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;

import util.ReportRequestHandler;
import pbReport.*;






@WebServlet("/Xray_type_report_Servlet")
public class Xray_type_report_Servlet  extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	String[][] Criteria =
	{
		{"criteria","","lab_test_type","=","","int","","",SessionConstants.LAB_TEST_XRAY + "","none", LC.XRAY_TYPE_REPORT_WHERE_LABTESTTYPE + ""},		
		{"criteria","","xray_report_id",">","AND","String","","","-1","none", LC.XRAY_TYPE_REPORT_WHERE_XRAYREPORTID + ""},		
		{"criteria","","is_done","=","AND","int","","","1","none", LC.XRAY_TYPE_REPORT_WHERE_ISDONE + ""},		
		{"criteria","","testing_date",">=","AND","long","","",Long.MIN_VALUE + "","startDate", LC.HM_START_DATE + ""},		
		{"criteria","","testing_date","<=","AND","long","","",Long.MAX_VALUE + "","endDate", LC.HM_END_DATE + ""},		
		{"criteria","","xray_report_id","=","AND","int","","","any","xrayReportId_5", LC.XRAY_TYPE_REPORT_WHERE_XRAYREPORTID_5 + ""}		
	};
	
	String[][] Display =
	{
		{"display","","xray_report_id","xray",""},		
		{"display","","COUNT(id)","int",""},		
		{"display","","SUM(plate_count_1)","int",""},		
		{"display","","SUM(plate_count_2)","int",""},		
		{"display","","SUM(plate_count_3)","int",""}		
	};
	
	String GroupBy = "xray_report_id";
	String OrderBY = "";
	
	ReportRequestHandler reportRequestHandler;
	
	public Xray_type_report_Servlet(){

	}
	
	private final ReportService reportService = new ReportService();
	
	private String sql;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String language = "english";
		if(userDTO!= null && userDTO.languageID == SessionConstants.BANGLA)
		{
			language = "bangla";
		}
				
		String actionType = request.getParameter("actionType");

		System.out.println("In ssservlet doget, actiontype = " + actionType);
		
		sql = "prescription_lab_test";

		Display[0][4] = language.equalsIgnoreCase("english")?"X-Ray type":"এক্স রের ধরণ";
		Display[1][4] = language.equalsIgnoreCase("english")?"X-Ray count":"এক্স রের সংখ্যা";
		Display[2][4] = CommonDAO.getName(language, "medical_equipment_name", SessionConstants.PLATE_1);
		Display[3][4] = CommonDAO.getName(language, "medical_equipment_name", SessionConstants.PLATE_2);
		Display[4][4] = CommonDAO.getName(language, "medical_equipment_name", SessionConstants.PLATE_3);

		
		String reportName = LM.getText(LC.XRAY_TYPE_REPORT_OTHER_XRAY_TYPE_REPORT, loginDTO);
		
		reportRequestHandler = new ReportRequestHandler(null,
				Criteria, Display, GroupBy, OrderBY, sql,
				reportService);
		
		reportRequestHandler = new ReportRequestHandler(null,
				Criteria, Display, GroupBy, OrderBY, sql,
				reportService);
		
		reportRequestHandler.clientSideDataFormatting = new ArrayList<Pair<Integer, Integer>>();
		reportRequestHandler.clientSideDataFormatting.add(new ImmutablePair<Integer, Integer>(1, ReportRequestHandler.RIGHT_ALIGN_INT));
		reportRequestHandler.clientSideDataFormatting.add(new ImmutablePair<Integer, Integer>(2, ReportRequestHandler.RIGHT_ALIGN_INT));
		reportRequestHandler.clientSideDataFormatting.add(new ImmutablePair<Integer, Integer>(3, ReportRequestHandler.RIGHT_ALIGN_INT));
		reportRequestHandler.clientSideDataFormatting.add(new ImmutablePair<Integer, Integer>(4, ReportRequestHandler.RIGHT_ALIGN_INT));

		
		reportRequestHandler.handleReportGet(request, response, userDTO, "xray_type_report",
				MenuConstants.XRAY_TYPE_REPORT_DETAILS, language, reportName, "xray_type_report");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		doGet(request, response);
	}
}
