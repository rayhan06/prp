package pi_office_unit_store_keeper_mapping;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import com.google.gson.Gson;

import repository.Repository;
import repository.RepositoryManager;


public class Pi_office_unit_store_keeper_mappingRepository implements Repository {
	Pi_office_unit_store_keeper_mappingDAO pi_office_unit_store_keeper_mappingDAO = null;
	
	static Logger logger = Logger.getLogger(Pi_office_unit_store_keeper_mappingRepository.class);
	Map<Long, Pi_office_unit_store_keeper_mappingDTO>mapOfPi_office_unit_store_keeper_mappingDTOToiD;
	Map<Long, Pi_office_unit_store_keeper_mappingDTO>mapOfPi_office_unit_store_keeper_mappingDTOToOfficeUnitId;
	Gson gson;

  
	private Pi_office_unit_store_keeper_mappingRepository(){
		pi_office_unit_store_keeper_mappingDAO = Pi_office_unit_store_keeper_mappingDAO.getInstance();
		mapOfPi_office_unit_store_keeper_mappingDTOToiD = new ConcurrentHashMap<>();
		mapOfPi_office_unit_store_keeper_mappingDTOToOfficeUnitId = new ConcurrentHashMap<>();
		gson = new Gson();
		RepositoryManager.getInstance().addRepository(this);
	}

	private static class LazyLoader{
        final static Pi_office_unit_store_keeper_mappingRepository INSTANCE = new Pi_office_unit_store_keeper_mappingRepository();
    }

    public static Pi_office_unit_store_keeper_mappingRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

	public void reload(boolean reloadAll){
		try {
			List<Pi_office_unit_store_keeper_mappingDTO> pi_office_unit_store_keeper_mappingDTOs = pi_office_unit_store_keeper_mappingDAO.getAllDTOs(reloadAll);
			for(Pi_office_unit_store_keeper_mappingDTO pi_office_unit_store_keeper_mappingDTO : pi_office_unit_store_keeper_mappingDTOs) {
				Pi_office_unit_store_keeper_mappingDTO oldPi_office_unit_store_keeper_mappingDTO = getPi_office_unit_store_keeper_mappingDTOByiD(pi_office_unit_store_keeper_mappingDTO.iD);
				if( oldPi_office_unit_store_keeper_mappingDTO != null ) {
					mapOfPi_office_unit_store_keeper_mappingDTOToiD.remove(oldPi_office_unit_store_keeper_mappingDTO.iD);
					mapOfPi_office_unit_store_keeper_mappingDTOToOfficeUnitId.remove(oldPi_office_unit_store_keeper_mappingDTO.iD);

					
				}
				if(pi_office_unit_store_keeper_mappingDTO.isDeleted == 0) 
				{
					
					mapOfPi_office_unit_store_keeper_mappingDTOToiD.put(pi_office_unit_store_keeper_mappingDTO.iD, pi_office_unit_store_keeper_mappingDTO);
					mapOfPi_office_unit_store_keeper_mappingDTOToOfficeUnitId.put(pi_office_unit_store_keeper_mappingDTO.officeUnitId, pi_office_unit_store_keeper_mappingDTO);

				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public Pi_office_unit_store_keeper_mappingDTO clone(Pi_office_unit_store_keeper_mappingDTO dto) {
		String raw = gson.toJson(dto);
		return gson.fromJson(raw, Pi_office_unit_store_keeper_mappingDTO.class);
	}
	
	
	public List<Pi_office_unit_store_keeper_mappingDTO> getPi_office_unit_store_keeper_mappingList() {
		List <Pi_office_unit_store_keeper_mappingDTO> pi_office_unit_store_keeper_mappings = new ArrayList<Pi_office_unit_store_keeper_mappingDTO>(this.mapOfPi_office_unit_store_keeper_mappingDTOToiD.values());
		return pi_office_unit_store_keeper_mappings;
	}
	
	public List<Pi_office_unit_store_keeper_mappingDTO> copyPi_office_unit_store_keeper_mappingList() {
		List <Pi_office_unit_store_keeper_mappingDTO> pi_office_unit_store_keeper_mappings = getPi_office_unit_store_keeper_mappingList();
		return pi_office_unit_store_keeper_mappings
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}
	
	
	
	public Pi_office_unit_store_keeper_mappingDTO getPi_office_unit_store_keeper_mappingDTOByiD( long iD){
		return mapOfPi_office_unit_store_keeper_mappingDTOToiD.get(iD);
	}
	
	public Pi_office_unit_store_keeper_mappingDTO copyPi_office_unit_store_keeper_mappingDTOByiD( long iD){
		return clone(mapOfPi_office_unit_store_keeper_mappingDTOToiD.get(iD));
	}

	public Pi_office_unit_store_keeper_mappingDTO copyPi_office_unit_store_keeper_mappingDTOByOfficeUnitId( long iD){
		return clone(mapOfPi_office_unit_store_keeper_mappingDTOToOfficeUnitId.get(iD));
	}

	
	@Override
	public String getTableName() {
		return pi_office_unit_store_keeper_mappingDAO.getTableName();
	}
}


