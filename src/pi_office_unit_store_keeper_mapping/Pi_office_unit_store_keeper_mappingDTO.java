package pi_office_unit_store_keeper_mapping;
import java.util.*; 
import util.*; 


public class Pi_office_unit_store_keeper_mappingDTO extends CommonDTO
{

	public long officeUnitId = -1;
	public long organogramId = -1;
	public long insertedByUserId = -1;
	public long insertedByOrganogramId = -1;
	public long insertionDate = -1;
    public String lastModifierUser = "";
	
	
    @Override
	public String toString() {
            return "$Pi_office_unit_store_keeper_mappingDTO[" +
            " iD = " + iD +
            " officeUnitId = " + officeUnitId +
            " organogramId = " + organogramId +
            " insertedByUserId = " + insertedByUserId +
            " insertedByOrganogramId = " + insertedByOrganogramId +
            " insertionDate = " + insertionDate +
            " lastModifierUser = " + lastModifierUser +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}