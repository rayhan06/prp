package pi_office_unit_store_keeper_mapping;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.sql.SQLException;
import common.CommonDAOService;
import common.ConnectionAndStatementUtil;
import org.apache.log4j.Logger;
import pi_package_vendor.Pi_package_vendorDTO;
import util.*;
import pb.*;

public class Pi_office_unit_store_keeper_mappingDAO  implements CommonDAOService<Pi_office_unit_store_keeper_mappingDTO>
{
	
	Logger logger = Logger.getLogger(getClass());
	
	private static String addQuery = "";
	private static String updateQuery = "";
	private String[] columnNames = null;

	private static final String getExistingOfficeUnitAndOrganogram =
			"SELECT * FROM pi_office_unit_store_keeper_mapping WHERE  organogram_id = %d AND " +
					" office_unit_id = %d  AND isDeleted = 0 ";

	private static final String getExistingOfficeUnit =
			"SELECT * FROM pi_office_unit_store_keeper_mapping WHERE " +
					" office_unit_id = %d  AND isDeleted = 0 ";

	private static final String getExistingOrganogram =
			"SELECT * FROM pi_office_unit_store_keeper_mapping WHERE  organogram_id = %d AND " +
					"  isDeleted = 0 ";

	private Pi_office_unit_store_keeper_mappingDAO()
	{
		columnNames = new String[] 
		{
			"ID",
			"office_unit_id",
			"organogram_id",
			"inserted_by_user_id",
			"inserted_by_organogram_id",
			"insertion_date",
			"last_modifier_user",
			"isDeleted",
			"lastModificationTime"
		};
		updateQuery = getUpdateQuery(columnNames);
        addQuery = getAddQuery(columnNames);
		
		searchMap.put("insertion_date_start"," and (insertion_date >= ?)");
		searchMap.put("insertion_date_end"," and (insertion_date <= ?)");
		searchMap.put("AnyField"," and (search_column like ?)");
		searchMap.put("employeeRecordsId"," and ( organogram_id = ?)");
		searchMap.put("officeUnitId"," and ( office_unit_id = ?)");
	}

	private final Map<String,String> searchMap = new HashMap<>();

    private static class LazyLoader
    {
    	static final Pi_office_unit_store_keeper_mappingDAO INSTANCE = new Pi_office_unit_store_keeper_mappingDAO();
	}

	public static Pi_office_unit_store_keeper_mappingDAO getInstance()
	{
    	return LazyLoader.INSTANCE;
	}
	
	public void setSearchColumn(Pi_office_unit_store_keeper_mappingDTO pi_office_unit_store_keeper_mappingDTO)
	{
		pi_office_unit_store_keeper_mappingDTO.searchColumn = "";
	}
	
	@Override
	public void set(PreparedStatement ps, Pi_office_unit_store_keeper_mappingDTO pi_office_unit_store_keeper_mappingDTO, boolean isInsert) throws SQLException
	{
		int index = 0;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(pi_office_unit_store_keeper_mappingDTO);
		if(isInsert)
		{
			ps.setObject(++index,pi_office_unit_store_keeper_mappingDTO.iD);
		}
		ps.setObject(++index,pi_office_unit_store_keeper_mappingDTO.officeUnitId);
		ps.setObject(++index,pi_office_unit_store_keeper_mappingDTO.organogramId);
		ps.setObject(++index,pi_office_unit_store_keeper_mappingDTO.insertedByUserId);
		ps.setObject(++index,pi_office_unit_store_keeper_mappingDTO.insertedByOrganogramId);
		ps.setObject(++index,pi_office_unit_store_keeper_mappingDTO.insertionDate);
		ps.setObject(++index,pi_office_unit_store_keeper_mappingDTO.lastModifierUser);
		if(isInsert)
		{
			ps.setObject(++index,pi_office_unit_store_keeper_mappingDTO.isDeleted);
		}
		ps.setObject(++index, lastModificationTime);
		if(!isInsert)
		{
			ps.setObject(++index,pi_office_unit_store_keeper_mappingDTO.iD);
		}
	}
	
	@Override
	public Pi_office_unit_store_keeper_mappingDTO buildObjectFromResultSet(ResultSet rs)
	{
		try
		{
			Pi_office_unit_store_keeper_mappingDTO pi_office_unit_store_keeper_mappingDTO = new Pi_office_unit_store_keeper_mappingDTO();
			int i = 0;
			pi_office_unit_store_keeper_mappingDTO.iD = rs.getLong(columnNames[i++]);
			pi_office_unit_store_keeper_mappingDTO.officeUnitId = rs.getLong(columnNames[i++]);
			pi_office_unit_store_keeper_mappingDTO.organogramId = rs.getLong(columnNames[i++]);
			pi_office_unit_store_keeper_mappingDTO.insertedByUserId = rs.getLong(columnNames[i++]);
			pi_office_unit_store_keeper_mappingDTO.insertedByOrganogramId = rs.getLong(columnNames[i++]);
			pi_office_unit_store_keeper_mappingDTO.insertionDate = rs.getLong(columnNames[i++]);
			pi_office_unit_store_keeper_mappingDTO.lastModifierUser = rs.getString(columnNames[i++]);
			pi_office_unit_store_keeper_mappingDTO.isDeleted = rs.getInt(columnNames[i++]);
			pi_office_unit_store_keeper_mappingDTO.lastModificationTime = rs.getLong(columnNames[i++]);
			return pi_office_unit_store_keeper_mappingDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
		
	public Pi_office_unit_store_keeper_mappingDTO getDTOByID (long id)
	{
		return getDTOFromID(id);
	}

	@Override
	public String getTableName() {
		return "pi_office_unit_store_keeper_mapping";
	}

	@Override
	public Map<String, String> getSearchMap() {
		return searchMap;
	}

	public long add(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Pi_office_unit_store_keeper_mappingDTO) commonDTO,addQuery,true);
	}

	public long update(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Pi_office_unit_store_keeper_mappingDTO) commonDTO,updateQuery,false);
	}
	
	@Override
	public String getLastModifierUser(){
		return "last_modifier_user";
    }

	public List<Pi_office_unit_store_keeper_mappingDTO> getExistingOfficeUnitAndOrganogram(long organogramId, long officeUnitId) {
		String sql = String.format(getExistingOfficeUnitAndOrganogram,organogramId,officeUnitId);
		return ConnectionAndStatementUtil.getListOfT(sql, this::buildObjectFromResultSet);
	}
	public List<Pi_office_unit_store_keeper_mappingDTO> getExistingOfficeUnit(long officeUnitId) {
		String sql = String.format(getExistingOfficeUnit,officeUnitId);
		return ConnectionAndStatementUtil.getListOfT(sql, this::buildObjectFromResultSet);
	}
	public List<Pi_office_unit_store_keeper_mappingDTO> getExistingOrganogram(long organogramId) {
		String sql = String.format(getExistingOrganogram,organogramId);
		return ConnectionAndStatementUtil.getListOfT(sql, this::buildObjectFromResultSet);
	}


				
}
	