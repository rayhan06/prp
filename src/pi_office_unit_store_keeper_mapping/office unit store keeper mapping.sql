select ouo.office_unit_id , ouo.id from prp.office_unit_organograms ouo 
inner join
(select ouoinner.office_unit_id , max(ouoinner.designation_sequence) as designation_sequence FROM prp.office_unit_organograms ouoinner
inner join prp.employee_offices eo
on eo.office_unit_organogram_id  = ouoinner.id
and eo.status = 1
group by ouoinner.office_unit_id) as maxDesig
on (
ouo.office_unit_id = maxDesig.office_unit_id
and ouo.designation_sequence = maxDesig.designation_sequence
)
inner join
(
select ouoinner.office_unit_id , ouoinner.designation_sequence , max(ouoinner.id) as id FROM prp.office_unit_organograms ouoinner
inner join prp.employee_offices eo
on eo.office_unit_organogram_id  = ouoinner.id
and eo.status = 1
group by ouoinner.office_unit_id, ouoinner.designation_sequence) as maxId
on (
ouo.office_unit_id = maxId.office_unit_id
and ouo.designation_sequence = maxId.designation_sequence
and ouo.id = maxId.id
);
