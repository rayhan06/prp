package pi_office_unit_store_keeper_mapping;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;

import am_minister_hostel_unit.Am_minister_hostel_unitDAO;
import office_unit_organograms.OfficeUnitOrganograms;
import office_unit_organograms.OfficeUnitOrganogramsRepository;
import org.apache.log4j.Logger;
import pb.Utils;
import permission.MenuConstants;
import user.UserDTO;
import util.*;
import javax.servlet.http.*;
import common.BaseServlet;
import com.google.gson.Gson;

import java.util.List;

@WebServlet("/Pi_office_unit_store_keeper_mappingServlet")
@MultipartConfig
public class Pi_office_unit_store_keeper_mappingServlet extends BaseServlet
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Pi_office_unit_store_keeper_mappingServlet.class);

    @Override
    public String getTableName() {
        return Pi_office_unit_store_keeper_mappingDAO.getInstance().getTableName();
    }

    @Override
    public String getServletName() {
        return "Pi_office_unit_store_keeper_mappingServlet";
    }

    @Override
    public Pi_office_unit_store_keeper_mappingDAO getCommonDAOService() {
        return Pi_office_unit_store_keeper_mappingDAO.getInstance();
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[] {MenuConstants.PI_OFFICE_UNIT_STORE_KEEPER_MAPPING_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[] {MenuConstants.PI_OFFICE_UNIT_STORE_KEEPER_MAPPING_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[] {MenuConstants.PI_OFFICE_UNIT_STORE_KEEPER_MAPPING_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Pi_office_unit_store_keeper_mappingServlet.class;
    }
    private final Gson gson = new Gson();


	@Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception
	{
		String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
		Pi_office_unit_store_keeper_mappingDTO pi_office_unit_store_keeper_mappingDTO;
		if(addFlag == true)
		{
			pi_office_unit_store_keeper_mappingDTO = new Pi_office_unit_store_keeper_mappingDTO();

			pi_office_unit_store_keeper_mappingDTO.insertionDate = pi_office_unit_store_keeper_mappingDTO.lastModificationTime = System.currentTimeMillis();
			pi_office_unit_store_keeper_mappingDTO.insertedByUserId  = userDTO.ID;
			pi_office_unit_store_keeper_mappingDTO.insertedByOrganogramId  = userDTO.organogramID;
		}
		else
		{
			pi_office_unit_store_keeper_mappingDTO = Pi_office_unit_store_keeper_mappingDAO.getInstance().getDTOFromID(Long.parseLong(request.getParameter("iD")));

			if (pi_office_unit_store_keeper_mappingDTO == null) {
				throw new Exception("Store keeper information is not found");
			}
			pi_office_unit_store_keeper_mappingDTO.lastModificationTime = System.currentTimeMillis();
		}
		
		String Value = "";

		Value = request.getParameter("officeUnitId");
		if (Value == null ||Value.equals("") || Value.equals("-1")) {
			throw new Exception("Please provide office unit");
		}
		pi_office_unit_store_keeper_mappingDTO.officeUnitId = Long.parseLong((Value));


		Value = request.getParameter("organogramId");
		if (Value == null ||Value.equals("") || Value.equals("-1")) {
			throw new Exception("Please provide organogram");
		}
		pi_office_unit_store_keeper_mappingDTO.organogramId = Long.parseLong((Value));

		/*Validation Start*/

		//Own office unit validation
		OfficeUnitOrganograms organograms = OfficeUnitOrganogramsRepository.getInstance().getById(pi_office_unit_store_keeper_mappingDTO.organogramId);
		if(organograms.office_unit_id != pi_office_unit_store_keeper_mappingDTO.officeUnitId){
			throw new Exception(UtilCharacter.getDataByLanguage(Language,
					"নির্বাচিত এমপ্লয়ী এই অফিসের নন",
					"Selected employee is not the employee of this office"));
		}

		//check if the new added office and selected employee is already exist (One to one validation)
		if(addFlag){
			List<Pi_office_unit_store_keeper_mappingDTO> existingOffice = Pi_office_unit_store_keeper_mappingDAO
					.getInstance()
					.getExistingOfficeUnit(pi_office_unit_store_keeper_mappingDTO.officeUnitId );

			if(existingOffice !=null && existingOffice.size()>0){
				throw new Exception(UtilCharacter.getDataByLanguage(Language,
						"নির্বাচিত অফিস ইতিমধ্যে বরাদ্দ করা হয়েছে",
						"Selected office is already assigned"));
			}
		}
		
		List<Pi_office_unit_store_keeper_mappingDTO> existingOrganogram = Pi_office_unit_store_keeper_mappingDAO
				.getInstance()
				.getExistingOrganogram(pi_office_unit_store_keeper_mappingDTO.organogramId);

		if(existingOrganogram !=null && existingOrganogram.size()>0){
			throw new Exception("Selected employee is already assigned");
		}


		/*Validation End*/

		pi_office_unit_store_keeper_mappingDTO.lastModifierUser = userDTO.userName;

		logger.debug("Done adding  addPi_office_unit_store_keeper_mapping dto = " + pi_office_unit_store_keeper_mappingDTO);

		Utils.handleTransaction(()->{
			if(addFlag == true)
			{
				Pi_office_unit_store_keeper_mappingDAO.getInstance().add(pi_office_unit_store_keeper_mappingDTO);
			}
			else
			{
				Pi_office_unit_store_keeper_mappingDAO.getInstance().update(pi_office_unit_store_keeper_mappingDTO);
			}
		});
		
		return pi_office_unit_store_keeper_mappingDTO;
	}
}

