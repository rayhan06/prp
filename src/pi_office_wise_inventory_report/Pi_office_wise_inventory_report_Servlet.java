package pi_office_wise_inventory_report;


import java.io.IOException;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import language.LC;
import language.LM;

import login.LoginDTO;
import permission.MenuConstants;

import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;

import util.ReportRequestHandler;
import pbReport.*;






@WebServlet("/Pi_office_wise_inventory_report_Servlet")
public class Pi_office_wise_inventory_report_Servlet  extends HttpServlet {
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	String[][] Criteria =
	{
		{"criteria","puia","office_unit_id","=","","String","","","any","officeUnitId", LC.PI_OFFICE_WISE_INVENTORY_REPORT_WHERE_OFFICEUNITID + ""},
		{"criteria","puia","isDeleted","=","AND","String","","","0","isDeleted", LC.PI_OFFICE_WISE_INVENTORY_REPORT_WHERE_ISDELETED + ""},
		{"criteria","puia","action_type","=","AND","int","","","1","actionTypePi", LC.PI_OFFICE_WISE_INVENTORY_REPORT_WHERE_ISDELETED + ""}
	};

	String[][] Display =
	{
		{"display","puia","office_unit_id","office_unit",""},
		{"display","puia","item_id","productIdConverter",""},
		{"display","","count(*) ","text",""},
		{"display","","SUM(CASE WHEN stage = 1 THEN 1 ELSE 0 END) ","text",""},
		{"display","","SUM(CASE WHEN stage = 2 THEN 1 ELSE 0 END) ","text",""},
		{"display","","SUM(CASE WHEN stage = 3 THEN 1 ELSE 0 END) ","text",""}
	};

	String GroupBy = "puia.item_id";
	String OrderBY = "";

	ReportRequestHandler reportRequestHandler;

	public Pi_office_wise_inventory_report_Servlet(){

	}

	private final ReportService reportService = new ReportService();

	private String sql;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{

		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String language = "english";
		if(userDTO!= null && userDTO.languageID == SessionConstants.BANGLA)
		{
			language = "bangla";
		}

		String actionType = request.getParameter("actionType");

		sql = "pi_unique_item_assignment puia";

		Display[0][4] = LM.getText(LC.PI_OFFICE_WISE_INVENTORY_REPORT_SELECT_OFFICEUNITID, loginDTO);
		Display[1][4] = LM.getText(LC.PI_OFFICE_WISE_INVENTORY_REPORT_SELECT_ITEMID, loginDTO);
		Display[2][4] = LM.getText(LC.PI_OFFICE_WISE_INVENTORY_REPORT_SELECT_TOTAL, loginDTO);
		Display[3][4] = LM.getText(LC.PI_OFFICE_WISE_INVENTORY_REPORT_SELECT_STOCK, loginDTO);
		Display[4][4] = LM.getText(LC.PI_OFFICE_WISE_INVENTORY_REPORT_SELECT_GIVEN, loginDTO);
		Display[5][4] = LM.getText(LC.PI_OFFICE_WISE_INVENTORY_REPORT_SELECT_AUCTIONED, loginDTO);


		String reportName = LM.getText(LC.PI_OFFICE_WISE_INVENTORY_REPORT_OTHER_PI_OFFICE_WISE_INVENTORY_REPORT, loginDTO);

		reportRequestHandler = new ReportRequestHandler(null,
				Criteria, Display, GroupBy, OrderBY, sql,
				reportService);


		reportRequestHandler.handleReportGet(request, response, userDTO, "pi_office_wise_inventory_report",
				MenuConstants.PI_OFFICE_WISE_INVENTORY_REPORT_DETAILS, language, reportName, "pi_office_wise_inventory_report");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		doGet(request, response);
	}
}
