package allowance_configure;

import common.CommonDAOService;
import common.ConnectionAndStatementUtil;
import org.apache.log4j.Logger;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings({"Duplicates"})
public class Allowance_configureDAO implements CommonDAOService<Allowance_configureDTO> {

    private static final Logger logger = Logger.getLogger(Allowance_configureDAO.class);

    private static final String addQuery =
            "INSERT INTO {tableName} (allowance_cat,amount_type,allowance_amount,tax_deduction_type,tax_deduction,"
                    .concat("ordinance_text,modified_by,lastModificationTime,inserted_by,insertion_time,isDeleted,ID) ")
                    .concat("VALUES (?,?,?,?,?,?,?,?,?,?,?,?)");

    private static final String updateQuery =
            "UPDATE {tableName} SET allowance_cat=?,amount_type=?,allowance_amount=?,tax_deduction_type=?,tax_deduction=?,"
                    .concat("ordinance_text=?,modified_by=?,lastModificationTime=? WHERE ID=?");

    private static final String getByAllowanceCat = "SELECT * FROM allowance_configure WHERE allowance_cat=%d AND isDeleted=0";

    private static final Map<String, String> searchMap = new HashMap<>();

    private Allowance_configureDAO() {
        searchMap.put("allowance_cat", " and (allowance_cat = ?)");
        searchMap.put("amount_type", " and (amount_type = ?)");
    }

    private static class LazyLoader {
        static final Allowance_configureDAO INSTANCE = new Allowance_configureDAO();
    }

    public static Allowance_configureDAO getInstance() {
        return Allowance_configureDAO.LazyLoader.INSTANCE;
    }

    @Override
    public void set(PreparedStatement ps, Allowance_configureDTO allowance_configureDTO, boolean isInsert) throws SQLException {
        int index = 0;
        ps.setInt(++index, allowance_configureDTO.allowanceCat);
        ps.setInt(++index, allowance_configureDTO.amountType);
        ps.setInt(++index, allowance_configureDTO.allowanceAmount);
        ps.setInt(++index, allowance_configureDTO.taxDeductionType);
        ps.setInt(++index, allowance_configureDTO.taxDeduction);
        ps.setString(++index, allowance_configureDTO.ordinanceText);
        ps.setLong(++index, allowance_configureDTO.modifiedBy);
        ps.setLong(++index, allowance_configureDTO.lastModificationTime);
        if (isInsert) {
            ps.setLong(++index, allowance_configureDTO.insertedBy);
            ps.setLong(++index, allowance_configureDTO.insertionTime);
            ps.setInt(++index, 0);
        }
        ps.setLong(++index, allowance_configureDTO.iD);
    }

    @Override
    public Allowance_configureDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Allowance_configureDTO allowance_configureDTO = new Allowance_configureDTO();
            allowance_configureDTO.iD = rs.getLong("ID");
            allowance_configureDTO.allowanceCat = rs.getInt("allowance_cat");
            allowance_configureDTO.amountType = rs.getInt("amount_type");
            allowance_configureDTO.allowanceAmount = rs.getInt("allowance_amount");
            allowance_configureDTO.taxDeductionType = rs.getInt("tax_deduction_type");
            allowance_configureDTO.taxDeduction = rs.getInt("tax_deduction");
            allowance_configureDTO.insertedBy = rs.getLong("inserted_by");
            allowance_configureDTO.insertionTime = rs.getLong("insertion_time");
            allowance_configureDTO.isDeleted = rs.getInt("isDeleted");
            allowance_configureDTO.ordinanceText = rs.getString("ordinance_text");
            allowance_configureDTO.modifiedBy = rs.getLong("modified_by");
            allowance_configureDTO.lastModificationTime = rs.getLong("lastModificationTime");
            return allowance_configureDTO;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    @Override
    public String getTableName() {
        return "allowance_configure";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    @Override
    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Allowance_configureDTO) commonDTO, addQuery, true);
    }

    @Override
    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Allowance_configureDTO) commonDTO, updateQuery, false);
    }

    public Allowance_configureDTO getByAllowanceCat(int allowanceCat) {
        return ConnectionAndStatementUtil.getT(
                String.format(getByAllowanceCat, allowanceCat),
                this::buildObjectFromResultSet
        );
    }

    public Allowance_configureDTO getByAllowanceCatEnum(AllowanceCatEnum allowanceCatEnum) {
        return getByAllowanceCat(allowanceCatEnum.getValue());
    }
}
	