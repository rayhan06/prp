package allowance_configure;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

public enum AllowanceCatEnum {
    EID_UL_FITR(1),
    EID_UL_AZHA(2),
    HINDU_FESTIVAL(3),
    BUDDHISM_ALLOWANCE(4),
    CHRISTIANITY_ALLOWANCE(5),
    OVERTIME_ALLOWANCE(6),
    OVERTIME_A_ALLOWANCE(7),
    OVERTIME_B_ALLOWANCE(8),
    BANGLA_NEW_YEAR_ALLOWANCE(10),
    FOOD_ALLOWANCE(11),
    ;
    private final int value;

    private static final Map<Integer, AllowanceCatEnum> mapByValue;

    static {
        mapByValue = Arrays.stream(values())
                           .collect(Collectors.toMap(
                                   allowanceCatEnum -> allowanceCatEnum.value,
                                   allowanceCatEnum -> allowanceCatEnum,
                                   (a, b) -> a
                           ));
    }

    public static AllowanceCatEnum getByValue(Integer value) {
        AllowanceCatEnum allowanceCatEnum = null;
        if (value != null) {
            allowanceCatEnum = mapByValue.getOrDefault(value, null);
        }
        return allowanceCatEnum;
    }

    AllowanceCatEnum(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    //get corresponding primary key of religion table for religion allowances
    public static int getReligion(int value) {
        if (value == 1 || value == 2) {
            return 2;
        } else
            return value;
    }
}
