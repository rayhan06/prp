package allowance_configure;

import common.BaseServlet;
import login.LoginDTO;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import pb.Utils;
import permission.MenuConstants;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@WebServlet("/Allowance_configureServlet")
@MultipartConfig
@SuppressWarnings({"Duplicates"})
public class Allowance_configureServlet extends BaseServlet {
    private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Allowance_configureServlet.class);
    private final Allowance_configureDAO allowance_configureDAO = Allowance_configureDAO.getInstance();

    @Override
    public String getTableName() {
        return "allowance_configure";
    }

    @Override
    public String getServletName() {
        return "Allowance_configureServlet";
    }

    @Override
    public Allowance_configureDAO getCommonDAOService() {
        return allowance_configureDAO;
    }

    public void handleConfigureAllowance(HttpServletResponse response, Integer allowanceCat) throws IOException {
        AllowanceCatEnum allowanceCatEnum = AllowanceCatEnum.getByValue(allowanceCat);
        if (allowanceCatEnum == null) {
            String errorMessage = "No allowanceCat found with allowanceCat=" + allowanceCat;
            logger.error(errorMessage);
            throw new IllegalArgumentException(errorMessage);
        }
        Allowance_configureDTO allowanceConfigureDTO =
                Allowance_configureDAO.getInstance().getByAllowanceCatEnum(allowanceCatEnum);
        String redirectUrl;
        if (allowanceConfigureDTO == null) {
            redirectUrl = "Allowance_configureServlet?actionType=getAddPage&allowanceCat=" + allowanceCat;
        } else {
            redirectUrl = "Allowance_configureServlet?actionType=getEditPage&ID=" + allowanceConfigureDTO.iD;
        }
        response.sendRedirect(redirectUrl);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }

        String actionType = request.getParameter("actionType");
        try {
            if ("configureAllowance".equals(actionType)) {
                if (Utils.checkPermission(userDTO, MenuConstants.ALLOWANCE_CONFIGURE_ADD)) {
                    Integer allowanceCat = Utils.parseOptionalInt(
                            request.getParameter("allowanceCat"),
                            null,
                            null
                    );
                    handleConfigureAllowance(response, allowanceCat);
                    return;
                }
            } else {
                super.doGet(request, response);
                return;
            }
        } catch (Exception ex) {
            logger.error(ex);
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }

    public static Allowance_configureDTO addOrUpdate(Allowance_configureDTO allowanceConfigureDTO) throws Exception {
        boolean addFlag = true;
        Allowance_configureDAO allowanceConfigureDAO = Allowance_configureDAO.getInstance();
        Allowance_configureDTO alreadyAddedDTO = allowanceConfigureDAO.getByAllowanceCat(allowanceConfigureDTO.allowanceCat);
        if (alreadyAddedDTO != null) {
            addFlag = false;
            allowanceConfigureDTO.iD = alreadyAddedDTO.iD;
        }
        if (addFlag) {
            allowanceConfigureDAO.add(allowanceConfigureDTO);
        } else {
            allowanceConfigureDAO.update(allowanceConfigureDTO);
        }
        return allowanceConfigureDTO;
    }

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        long curTime = System.currentTimeMillis();
        Allowance_configureDTO allowance_configureDTO;
        if (addFlag) {
            allowance_configureDTO = new Allowance_configureDTO();
            allowance_configureDTO.insertedBy = userDTO.ID;
            allowance_configureDTO.insertionTime = curTime;
        } else {
            allowance_configureDTO = allowance_configureDAO.getDTOFromID(Long.parseLong(request.getParameter("iD")));
        }
        allowance_configureDTO.allowanceCat = Integer.parseInt(Jsoup.clean(request.getParameter("allowanceCat"), Whitelist.simpleText()));
        allowance_configureDTO.amountType = Integer.parseInt(Jsoup.clean(request.getParameter("amountType"), Whitelist.simpleText()));
        allowance_configureDTO.allowanceAmount = Integer.parseInt(Jsoup.clean(request.getParameter("allowanceAmount"), Whitelist.simpleText()));
        allowance_configureDTO.taxDeductionType = Integer.parseInt(Jsoup.clean(request.getParameter("taxDeductionType"), Whitelist.simpleText()));
        allowance_configureDTO.taxDeduction = Integer.parseInt(Jsoup.clean(request.getParameter("taxDeduction"), Whitelist.simpleText()));
        allowance_configureDTO.ordinanceText = Jsoup.clean(request.getParameter("ordinanceText"), Whitelist.simpleText());
        allowance_configureDTO.modifiedBy = userDTO.ID;
        allowance_configureDTO.lastModificationTime = curTime;

        Allowance_configureDTO alreadyAddedDTO =
                Allowance_configureDAO.getInstance().getByAllowanceCat(allowance_configureDTO.allowanceCat);
        if (alreadyAddedDTO != null) {
            allowance_configureDTO.iD = alreadyAddedDTO.iD;
            addFlag = false;
        }
        if (addFlag) {
            allowance_configureDAO.add(allowance_configureDTO);
        } else {
            allowance_configureDAO.update(allowance_configureDTO);
        }
        return allowance_configureDTO;
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[]{MenuConstants.ALLOWANCE_CONFIGURE_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[]{MenuConstants.ALLOWANCE_CONFIGURE_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[]{MenuConstants.ALLOWANCE_CONFIGURE_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Allowance_configureServlet.class;
    }
}

