package allowance_configure;

import fund_management.FundApplicationStatus;

import java.util.Arrays;

public enum AllowanceTypeEnum {
    FIXED(1, "/-"),
    PERCENTAGE(2, "%");
    private final int value;
    private final String symbol;

    AllowanceTypeEnum(int value, String symbol) {
        this.value = value;
        this.symbol = symbol;
    }

    public int getValue() {
        return value;
    }

    public String getSymbol() {
        return symbol;
    }

    public static String getSymbol(int value) {
        return Arrays.stream(values())
                     .filter(allowanceTypeEnum -> allowanceTypeEnum.value == value)
                     .map(AllowanceTypeEnum::getSymbol)
                     .findFirst()
                     .orElse("");
    }
}
