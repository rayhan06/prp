package allowance_configure;

import org.apache.log4j.Logger;
import repository.Repository;
import repository.RepositoryManager;
import util.LockManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


public class Allowance_configureRepository implements Repository {
    private static final Logger logger = Logger.getLogger(Allowance_configureRepository.class);
    private final Allowance_configureDAO allowance_configureDAO;
    private final Map<Long, Allowance_configureDTO> mapById;
    private final Map<Integer, Allowance_configureDTO> mapByAllowanceCat;


    private Allowance_configureRepository() {
        allowance_configureDAO = Allowance_configureDAO.getInstance();
        mapById = new ConcurrentHashMap<>();
        mapByAllowanceCat = new ConcurrentHashMap<>();
        RepositoryManager.getInstance().addRepository(this);
    }

    private static class LazyLoader {
        final static Allowance_configureRepository INSTANCE = new Allowance_configureRepository();
    }

    public static Allowance_configureRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

    public void reload(boolean reloadAll) {
        List<Allowance_configureDTO> list = allowance_configureDAO.getAllDTOs(true);
        if (list != null && list.size() > 0) {
            list.forEach(dto -> {
                mapById.put(dto.iD, dto);
                mapByAllowanceCat.put(dto.allowanceCat, dto);
            });
        }
    }

    public List<Allowance_configureDTO> getAllowance_configureList() {
        List<Allowance_configureDTO> allowance_configures = new ArrayList<Allowance_configureDTO>(this.mapById.values());
        return allowance_configures;
    }

    public Allowance_configureDTO getById(long id) {
        if (mapById.get(id) == null) {
            synchronized (LockManager.getLock(id + "ACR")) {
                if (mapById.get(id) == null) {
                    Allowance_configureDTO dto = allowance_configureDAO.getDTOFromIdDeletedOrNot(id);
                    if (dto != null) {
                        mapById.put(dto.iD, dto);
                        if (dto.isDeleted == 0) {
                            mapByAllowanceCat.put(dto.allowanceCat, dto);
                        }
                    }
                }
            }

        }
        return mapById.get(id);
    }

    public Allowance_configureDTO getByAllowanceCat(int allowanceCat) {
        synchronized (LockManager.getLock(allowanceCat + "ACR")) {
            if (mapByAllowanceCat.get(allowanceCat) == null) {
                Allowance_configureDTO dto = allowance_configureDAO.getByAllowanceCat(allowanceCat);
                if (dto != null) {
                    mapByAllowanceCat.put(dto.allowanceCat, dto);
                    mapById.put(dto.iD, dto);
                }
            }
        }
        return mapByAllowanceCat.get(allowanceCat);
    }

    public int getDailyRate(AllowanceCatEnum allowanceCatEnum) {
        Allowance_configureDTO configureDTO =
                Allowance_configureRepository.getInstance()
                                             .getByAllowanceCat(allowanceCatEnum.getValue());
        return configureDTO == null ? 0 : configureDTO.allowanceAmount;
    }

    @Override
    public String getTableName() {
        return "allowance_configure";
    }
}


