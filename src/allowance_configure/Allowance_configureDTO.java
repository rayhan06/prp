package allowance_configure;

import java.util.*;

import sessionmanager.SessionConstants;
import util.*;


public class Allowance_configureDTO extends CommonDTO {

    public int allowanceCat = 0;
    public int amountType = 0;
    public int allowanceAmount = 0;
    public int taxDeductionType = 0;
    public int taxDeduction = 0;
    public String ordinanceText = "";
    public long insertedBy = -1;
    public long insertionTime = SessionConstants.MIN_DATE;
    public long modifiedBy = -1;

    public int getTaxDeductionAmount(long amount) {
        if (amount < 0) return 0;
        return (int) (
                taxDeductionType == AllowanceTypeEnum.PERCENTAGE.getValue()
                ? Math.ceil((amount / 100.0) * taxDeduction)
                : taxDeduction
        );
    }

    @Override
    public String toString() {
        return "$Allowance_configureDTO[" +
               " iD = " + iD +
               " allowanceCat = " + allowanceCat +
               " amountType = " + amountType +
               " allowanceAmount = " + allowanceAmount +
               " insertedBy = " + insertedBy +
               " insertionTime = " + insertionTime +
               " isDeleted = " + isDeleted +
               " modifiedBy = " + modifiedBy +
               " lastModificationTime = " + lastModificationTime +
               "]";
    }

}