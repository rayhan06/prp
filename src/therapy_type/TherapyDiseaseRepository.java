package therapy_type;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import com.google.gson.Gson;


import repository.Repository;
import repository.RepositoryManager;


public class TherapyDiseaseRepository implements Repository {
	TherapyDiseaseDAO therapydiseaseDAO = null;
	
	static Logger logger = Logger.getLogger(TherapyDiseaseRepository.class);
	Map<Long, TherapyDiseaseDTO>mapOfTherapyDiseaseDTOToiD;
	Map<Long, Set<TherapyDiseaseDTO> >mapOfTherapyDiseaseDTOTotherapyTypeId;
	Gson gson;

  
	private TherapyDiseaseRepository(){
		therapydiseaseDAO = TherapyDiseaseDAO.getInstance();
		mapOfTherapyDiseaseDTOToiD = new ConcurrentHashMap<>();
		mapOfTherapyDiseaseDTOTotherapyTypeId = new ConcurrentHashMap<>();
		gson = new Gson();
		RepositoryManager.getInstance().addRepository(this);
	}

	private static class LazyLoader{
        final static TherapyDiseaseRepository INSTANCE = new TherapyDiseaseRepository();
    }

    public static TherapyDiseaseRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

	public void reload(boolean reloadAll){
		try {
			List<TherapyDiseaseDTO> therapydiseaseDTOs = therapydiseaseDAO.getAllDTOs(reloadAll);
			for(TherapyDiseaseDTO therapydiseaseDTO : therapydiseaseDTOs) {
				TherapyDiseaseDTO oldTherapyDiseaseDTO = getTherapyDiseaseDTOByiD(therapydiseaseDTO.iD);
				if( oldTherapyDiseaseDTO != null ) {
					mapOfTherapyDiseaseDTOToiD.remove(oldTherapyDiseaseDTO.iD);
				
					if(mapOfTherapyDiseaseDTOTotherapyTypeId.containsKey(oldTherapyDiseaseDTO.therapyTypeId)) {
						mapOfTherapyDiseaseDTOTotherapyTypeId.get(oldTherapyDiseaseDTO.therapyTypeId).remove(oldTherapyDiseaseDTO);
					}
					if(mapOfTherapyDiseaseDTOTotherapyTypeId.get(oldTherapyDiseaseDTO.therapyTypeId).isEmpty()) {
						mapOfTherapyDiseaseDTOTotherapyTypeId.remove(oldTherapyDiseaseDTO.therapyTypeId);
					}
					
					
				}
				if(therapydiseaseDTO.isDeleted == 0) 
				{
					
					mapOfTherapyDiseaseDTOToiD.put(therapydiseaseDTO.iD, therapydiseaseDTO);
				
					if( ! mapOfTherapyDiseaseDTOTotherapyTypeId.containsKey(therapydiseaseDTO.therapyTypeId)) {
						mapOfTherapyDiseaseDTOTotherapyTypeId.put(therapydiseaseDTO.therapyTypeId, new HashSet<>());
					}
					mapOfTherapyDiseaseDTOTotherapyTypeId.get(therapydiseaseDTO.therapyTypeId).add(therapydiseaseDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public TherapyDiseaseDTO clone(TherapyDiseaseDTO dto) {
		String raw = gson.toJson(dto);
		return gson.fromJson(raw, TherapyDiseaseDTO.class);
	}
	
	
	public List<TherapyDiseaseDTO> getTherapyDiseaseList() {
		List <TherapyDiseaseDTO> therapydiseases = new ArrayList<TherapyDiseaseDTO>(this.mapOfTherapyDiseaseDTOToiD.values());
		return therapydiseases;
	}
	
	public List<TherapyDiseaseDTO> copyTherapyDiseaseList() {
		List <TherapyDiseaseDTO> therapydiseases = getTherapyDiseaseList();
		return therapydiseases
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}
	
	
	
	public TherapyDiseaseDTO getTherapyDiseaseDTOByiD( long iD){
		return mapOfTherapyDiseaseDTOToiD.get(iD);
	}
	
	public TherapyDiseaseDTO copyTherapyDiseaseDTOByiD( long iD){
		return clone(mapOfTherapyDiseaseDTOToiD.get(iD));
	}
	
	
	public List<TherapyDiseaseDTO> getTherapyDiseaseDTOBytherapyTypeId(long therapyTypeId) {
		List <TherapyDiseaseDTO> therapydiseases =  new ArrayList<>( mapOfTherapyDiseaseDTOTotherapyTypeId.getOrDefault(therapyTypeId,new HashSet<>()));

		therapydiseases = therapydiseases.stream()
			.sorted(Comparator.comparing(TherapyDiseaseDTO::getNameEn))
			.collect(Collectors.toList());
		return therapydiseases;
	}
	
	public List<TherapyDiseaseDTO> copyTherapyDiseaseDTOBytherapyTypeId(long therapyTypeId)
	{
		List <TherapyDiseaseDTO> therapydiseases = getTherapyDiseaseDTOBytherapyTypeId(therapyTypeId);
		return therapydiseases
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}

	
	@Override
	public String getTableName() {
		return therapydiseaseDAO.getTableName();
	}
}


