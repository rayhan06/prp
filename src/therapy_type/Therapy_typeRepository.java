package therapy_type;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import com.google.gson.Gson;

import common.ConnectionAndStatementUtil;
import repository.Repository;
import repository.RepositoryManager;


public class Therapy_typeRepository implements Repository {
	Therapy_typeDAO therapy_typeDAO = null;
	
	String getAllDTOsForTrue = "SELECT * FROM %s order by ordering asc";

    String getAllDTOsForFalse = "SELECT * FROM %s WHERE lastModificationTime >= %d order by ordering asc";
	
	static Logger logger = Logger.getLogger(Therapy_typeRepository.class);
	Map<Long, Therapy_typeDTO>mapOfTherapy_typeDTOToiD;
	Gson gson;

  
	private Therapy_typeRepository(){
		therapy_typeDAO = Therapy_typeDAO.getInstance();
		mapOfTherapy_typeDTOToiD = new ConcurrentHashMap<>();
		gson = new Gson();
		RepositoryManager.getInstance().addRepository(this);
	}

	private static class LazyLoader{
        final static Therapy_typeRepository INSTANCE = new Therapy_typeRepository();
    }

    public static Therapy_typeRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

	public void reload(boolean reloadAll){
		try {
			List<Therapy_typeDTO> therapy_typeDTOs = therapy_typeDAO.getAllDTOs(reloadAll);
			for(Therapy_typeDTO therapy_typeDTO : therapy_typeDTOs) {
				Therapy_typeDTO oldTherapy_typeDTO = getTherapy_typeDTOByiD(therapy_typeDTO.iD);
				if( oldTherapy_typeDTO != null ) {
					mapOfTherapy_typeDTOToiD.remove(oldTherapy_typeDTO.iD);
				
					
				}
				if(therapy_typeDTO.isDeleted == 0) 
				{
					
					mapOfTherapy_typeDTOToiD.put(therapy_typeDTO.iD, therapy_typeDTO);
				
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	
	
	public Therapy_typeDTO clone(Therapy_typeDTO dto) {
		String raw = gson.toJson(dto);
		return gson.fromJson(raw, Therapy_typeDTO.class);
	}
	
	
	public List<Therapy_typeDTO> getTherapy_typeList() {
		List <Therapy_typeDTO> therapy_types = new ArrayList<Therapy_typeDTO>(this.mapOfTherapy_typeDTOToiD.values());
		return therapy_types;
	}
	
	public String getOptions(String language, long defaultVal)
	{
		String options = "<option value = '-1'>" + (language.equalsIgnoreCase("english")?"Select":"বাছাই করুন" )
			+ "</option>";
		
		List<Therapy_typeDTO> therapy_typeDTOs = getTherapy_typeList() ;
		for(Therapy_typeDTO therapy_typeDTO: therapy_typeDTOs)
		{
			options += "<option value = '" + therapy_typeDTO.iD + "'" 
					+ (defaultVal == therapy_typeDTO.iD? "selected":"")
					+ ">" + therapy_typeDTO.nameEn + "</option>";
		}
		return options;
		
	}
	
	public List<Therapy_typeDTO> copyTherapy_typeList() {
		List <Therapy_typeDTO> therapy_types = getTherapy_typeList();
		return therapy_types
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}
	
	
	
	public Therapy_typeDTO getTherapy_typeDTOByiD( long iD){
		return mapOfTherapy_typeDTOToiD.get(iD);
	}
	
	public Therapy_typeDTO copyTherapy_typeDTOByiD( long iD){
		return clone(mapOfTherapy_typeDTOToiD.get(iD));
	}

	
	@Override
	public String getTableName() {
		return therapy_typeDAO.getTableName();
	}
}


