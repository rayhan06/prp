package therapy_type;
import java.util.*; 
import util.*; 


public class TherapyDiseaseDTO extends CommonDTO
{

	public long therapyTypeId = -1;
    public String nameEn = "";
    public String nameBn = "";
    
    public String getNameEn()
    {
    	return nameEn;
    }
    
    public static int OTHER = -3;
	
	public List<TherapyDiseaseDTO> therapyDiseaseDTOList = new ArrayList<>();
	
    @Override
	public String toString() {
            return "$TherapyDiseaseDTO[" +
            " iD = " + iD +
            " therapyTypeId = " + therapyTypeId +
            " nameEn = " + nameEn +
            " nameBn = " + nameBn +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}