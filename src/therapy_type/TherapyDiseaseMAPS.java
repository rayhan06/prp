package therapy_type;
import java.util.*; 
import util.*;


public class TherapyDiseaseMAPS extends CommonMaps
{	
	public TherapyDiseaseMAPS(String tableName)
	{
		


		java_SQL_map.put("therapy_type_id".toLowerCase(), "therapyTypeId".toLowerCase());
		java_SQL_map.put("name_en".toLowerCase(), "nameEn".toLowerCase());
		java_SQL_map.put("name_bn".toLowerCase(), "nameBn".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Therapy Type Id".toLowerCase(), "therapyTypeId".toLowerCase());
		java_Text_map.put("English Name".toLowerCase(), "nameEn".toLowerCase());
		java_Text_map.put("Bangla Name".toLowerCase(), "nameBn".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}