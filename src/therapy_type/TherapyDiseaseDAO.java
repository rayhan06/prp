package therapy_type;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.sql.SQLException;
import common.CommonDAOService;
import common.ConnectionAndStatementUtil;

import org.apache.log4j.Logger;
import util.*;
import pb.*;
import repository.RepositoryManager;

public class TherapyDiseaseDAO  implements CommonDAOService<TherapyDiseaseDTO>
{
	
	Logger logger = Logger.getLogger(getClass());
	
	private static String addQuery = "";
	private static String updateQuery = "";
	private String[] columnNames = null;

	private TherapyDiseaseDAO()
	{
		columnNames = new String[] 
		{
			"ID",
			"therapy_type_id",
			"name_en",
			"name_bn",
			"isDeleted",
			"lastModificationTime"
		};
		updateQuery = getUpdateQuery(columnNames);
        addQuery = getAddQuery(columnNames);
		
		searchMap.put("name_en"," and (name_en like ?)");
		searchMap.put("name_bn"," and (name_bn like ?)");
		searchMap.put("AnyField"," and (search_column like ?)");
	}
	
	String getAllDTOsForTrue = "SELECT * FROM %s order by name_en asc";

    String getAllDTOsForFalse = "SELECT * FROM %s WHERE lastModificationTime >= %d order by name_en asc";

	private final Map<String,String> searchMap = new HashMap<>();

    private static class LazyLoader
    {
    	static final TherapyDiseaseDAO INSTANCE = new TherapyDiseaseDAO();
	}

	public static TherapyDiseaseDAO getInstance()
	{
    	return LazyLoader.INSTANCE;
	}
	
	 public List<TherapyDiseaseDTO> getAllDTOs(boolean isFirstReload) {
	        String sql;
	        if (isFirstReload) {
	            sql = String.format(getAllDTOsForTrue, getTableName());
	        } else {
	            sql = String.format(getAllDTOsForFalse, getTableName(), RepositoryManager.lastModifyTime);
	        }
	        return ConnectionAndStatementUtil.getListOfT(sql, this::buildObjectFromResultSet);
	}
	
	public void setSearchColumn(TherapyDiseaseDTO therapydiseaseDTO)
	{
		therapydiseaseDTO.searchColumn = "";
		therapydiseaseDTO.searchColumn += therapydiseaseDTO.nameEn + " ";
		therapydiseaseDTO.searchColumn += therapydiseaseDTO.nameBn + " ";
	}
	
	@Override
	public void set(PreparedStatement ps, TherapyDiseaseDTO therapydiseaseDTO, boolean isInsert) throws SQLException
	{
		int index = 0;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(therapydiseaseDTO);
		if(isInsert)
		{
			ps.setObject(++index,therapydiseaseDTO.iD);
		}
		ps.setObject(++index,therapydiseaseDTO.therapyTypeId);
		ps.setObject(++index,therapydiseaseDTO.nameEn);
		ps.setObject(++index,therapydiseaseDTO.nameBn);
		if(isInsert)
		{
			ps.setObject(++index,therapydiseaseDTO.isDeleted);
		}
		ps.setObject(++index, lastModificationTime);
		if(!isInsert)
		{
			ps.setObject(++index,therapydiseaseDTO.iD);
		}
	}
	
	@Override
	public TherapyDiseaseDTO buildObjectFromResultSet(ResultSet rs)
	{
		try
		{
			TherapyDiseaseDTO therapydiseaseDTO = new TherapyDiseaseDTO();
			int i = 0;
			therapydiseaseDTO.iD = rs.getLong(columnNames[i++]);
			therapydiseaseDTO.therapyTypeId = rs.getLong(columnNames[i++]);
			therapydiseaseDTO.nameEn = rs.getString(columnNames[i++]);
			therapydiseaseDTO.nameBn = rs.getString(columnNames[i++]);
			therapydiseaseDTO.isDeleted = rs.getInt(columnNames[i++]);
			therapydiseaseDTO.lastModificationTime = rs.getLong(columnNames[i++]);
			return therapydiseaseDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
		
	public TherapyDiseaseDTO getDTOByID (long id)
	{
		return getDTOFromID(id);
	}

	@Override
	public String getTableName() {
		return "therapy_disease";
	}

	@Override
	public Map<String, String> getSearchMap() {
		return searchMap;
	}

	public long add(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((TherapyDiseaseDTO) commonDTO,addQuery,true);
	}

	public long update(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((TherapyDiseaseDTO) commonDTO,updateQuery,false);
	}
	
	@Override
	public String getLastModifierUser(){
		return "";
    }				
}
	