package therapy_type;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.sql.SQLException;
import common.CommonDAOService;
import common.ConnectionAndStatementUtil;

import org.apache.log4j.Logger;
import util.*;
import pb.*;
import repository.RepositoryManager;

public class Therapy_typeDAO  implements CommonDAOService<Therapy_typeDTO>
{
	
	Logger logger = Logger.getLogger(getClass());
	
	private static String addQuery = "";
	private static String updateQuery = "";
	private String[] columnNames = null;
	
	String getAllDTOsForTrue = "SELECT * FROM %s order by ordering asc";

    String getAllDTOsForFalse = "SELECT * FROM %s WHERE lastModificationTime >= %d order by ordering asc";

	private Therapy_typeDAO()
	{
		columnNames = new String[] 
		{
			"ID",
			"name_en",
			"name_bn",
			"inserted_by_user_id",
			"inserted_by_organogram_id",
			"insertion_date",
			"search_column",
			"ordering",
			"isDeleted",
			"lastModificationTime"
		};
		updateQuery = getUpdateQuery(columnNames);
        addQuery = getAddQuery(columnNames);
		
		searchMap.put("name_en"," and (name_en like ?)");
		searchMap.put("name_bn"," and (name_bn like ?)");
		searchMap.put("insertion_date_start"," and (insertion_date >= ?)");
		searchMap.put("insertion_date_end"," and (insertion_date <= ?)");
		searchMap.put("AnyField"," and (search_column like ?)");
	}

	private final Map<String,String> searchMap = new HashMap<>();

    private static class LazyLoader
    {
    	static final Therapy_typeDAO INSTANCE = new Therapy_typeDAO();
	}

	public static Therapy_typeDAO getInstance()
	{
    	return LazyLoader.INSTANCE;
	}
	
	public void setSearchColumn(Therapy_typeDTO therapy_typeDTO)
	{
		therapy_typeDTO.searchColumn = "";
		therapy_typeDTO.searchColumn += therapy_typeDTO.nameEn + " ";
		therapy_typeDTO.searchColumn += therapy_typeDTO.nameBn + " ";
	}
	
	@Override
	public void set(PreparedStatement ps, Therapy_typeDTO therapy_typeDTO, boolean isInsert) throws SQLException
	{
		int index = 0;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(therapy_typeDTO);
		if(isInsert)
		{
			ps.setObject(++index,therapy_typeDTO.iD);
		}
		ps.setObject(++index,therapy_typeDTO.nameEn);
		ps.setObject(++index,therapy_typeDTO.nameBn);
		ps.setObject(++index,therapy_typeDTO.insertedByUserId);
		ps.setObject(++index,therapy_typeDTO.insertedByOrganogramId);
		ps.setObject(++index,therapy_typeDTO.insertionDate);
		ps.setObject(++index,therapy_typeDTO.searchColumn);
		ps.setObject(++index,therapy_typeDTO.ordering);
		if(isInsert)
		{
			ps.setObject(++index,therapy_typeDTO.isDeleted);
		}
		ps.setObject(++index, lastModificationTime);
		if(!isInsert)
		{
			ps.setObject(++index,therapy_typeDTO.iD);
		}
	}
	
	@Override
	public Therapy_typeDTO buildObjectFromResultSet(ResultSet rs)
	{
		try
		{
			Therapy_typeDTO therapy_typeDTO = new Therapy_typeDTO();
			int i = 0;
			therapy_typeDTO.iD = rs.getLong(columnNames[i++]);
			therapy_typeDTO.nameEn = rs.getString(columnNames[i++]);
			therapy_typeDTO.nameBn = rs.getString(columnNames[i++]);
			therapy_typeDTO.insertedByUserId = rs.getLong(columnNames[i++]);
			therapy_typeDTO.insertedByOrganogramId = rs.getLong(columnNames[i++]);
			therapy_typeDTO.insertionDate = rs.getLong(columnNames[i++]);
			therapy_typeDTO.searchColumn = rs.getString(columnNames[i++]);
			therapy_typeDTO.ordering = rs.getInt(columnNames[i++]);
			therapy_typeDTO.isDeleted = rs.getInt(columnNames[i++]);
			therapy_typeDTO.lastModificationTime = rs.getLong(columnNames[i++]);
			return therapy_typeDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
		
	public Therapy_typeDTO getDTOByID (long id)
	{
		Therapy_typeDTO therapy_typeDTO = null;
		try 
		{
			therapy_typeDTO = getDTOFromID(id);
			if(therapy_typeDTO != null)
			{
				TherapyDiseaseDAO therapyDiseaseDAO = TherapyDiseaseDAO.getInstance();				
				List<TherapyDiseaseDTO> therapyDiseaseDTOList = (List<TherapyDiseaseDTO>)therapyDiseaseDAO.getDTOsByParent("therapy_type_id", therapy_typeDTO.iD);
				therapy_typeDTO.therapyDiseaseDTOList = therapyDiseaseDTOList;
			}
		}
		catch (Exception e) 
		{
				// TODO Auto-generated catch block
				e.printStackTrace();
		}
		return therapy_typeDTO;
	}
	
	 public List<Therapy_typeDTO> getAllDTOs(boolean isFirstReload) {
	        String sql;
	        if (isFirstReload) {
	            sql = String.format(getAllDTOsForTrue, getTableName());
	        } else {
	            sql = String.format(getAllDTOsForFalse, getTableName(), RepositoryManager.lastModifyTime);
	        }
	        return ConnectionAndStatementUtil.getListOfT(sql, this::buildObjectFromResultSet);
	}

	@Override
	public String getTableName() {
		return "therapy_type";
	}

	@Override
	public Map<String, String> getSearchMap() {
		return searchMap;
	}

	public long add(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Therapy_typeDTO) commonDTO,addQuery,true);
	}

	public long update(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Therapy_typeDTO) commonDTO,updateQuery,false);
	}
	
	@Override
	public String getLastModifierUser(){
		return "";
    }
				
}
	