package therapy_type;

import java.io.IOException;
import java.text.SimpleDateFormat;


import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import org.apache.log4j.Logger;

import permission.MenuConstants;

import sessionmanager.SessionConstants;
import user.UserDTO;
import util.*;
import javax.servlet.http.*;
import java.util.*;


import language.LC;
import language.LM;
import common.ApiResponse;
import common.BaseServlet;
import com.google.gson.Gson;
import pb.*;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;




/**
 * Servlet implementation class Therapy_typeServlet
 */
@WebServlet("/Therapy_typeServlet")
@MultipartConfig
public class Therapy_typeServlet extends BaseServlet 
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Therapy_typeServlet.class);

    @Override
    public String getTableName() {
        return Therapy_typeDAO.getInstance().getTableName();
    }

    @Override
    public String getServletName() {
        return "Therapy_typeServlet";
    }

    @Override
    public Therapy_typeDAO getCommonDAOService() {
        return Therapy_typeDAO.getInstance();
    }
    
    @Override
    public int[] getAddPageMenuConstants() {
        return new int[] {MenuConstants.THERAPY_TYPE_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[] {MenuConstants.THERAPY_TYPE_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[] {MenuConstants.THERAPY_TYPE_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Therapy_typeServlet.class;
    }
	TherapyDiseaseDAO therapyDiseaseDAO = TherapyDiseaseDAO.getInstance();
    private final Gson gson = new Gson();
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        CommonLoginData commonLoginData = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get();
        init(request);
        String actionType = request.getParameter("actionType");
		if ("getDisease".equals(actionType)) 
		{
			try {
				long type = Long.parseLong(request.getParameter("type"));
				long defaultVal = Long.parseLong(request.getParameter("defaultVal"));
				List<TherapyDiseaseDTO> therapyDiseaseDTOs = TherapyDiseaseRepository.getInstance().getTherapyDiseaseDTOBytherapyTypeId(type);
				String options = "";
				if(defaultVal == -1)
				{
					options += "<option value= '-1' selected>" + LM.getText(LC.HM_SELECT, commonLoginData.loginDTO) + "</option>";
				}
				else
				{
					options += "<option value= '-1'>" + LM.getText(LC.HM_SELECT, commonLoginData.loginDTO) + "</option>";
				}
				
				for(TherapyDiseaseDTO therapyDiseaseDTO: therapyDiseaseDTOs)
				{
					options += "<option value= '" + therapyDiseaseDTO.iD + "'";
					if(therapyDiseaseDTO.iD == defaultVal)
					{
						options += " selected ";
					}
					options += ">" + therapyDiseaseDTO.nameEn + "</option>";
				}
				response.getWriter().write(options);
				
			} catch (Exception ex) {
				ex.printStackTrace();
				ApiResponse.sendErrorResponse(response, ex.getMessage());
			}
			return;
		}
		else
		{
			super.doGet(request,response);
		}
		

    }
 	

	@Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception 
	{
		// TODO Auto-generated method stub
		try 
		{
			request.setAttribute("failureMessage", "");
			System.out.println("%%%% addTherapy_type");
			String language = userDTO.languageID == SessionConstants.ENGLISH? "English":"Bangla";
			Therapy_typeDTO therapy_typeDTO;
			SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
						
			if(addFlag == true)
			{
				therapy_typeDTO = new Therapy_typeDTO();
			}
			else
			{
				therapy_typeDTO = Therapy_typeDAO.getInstance().getDTOFromID(Long.parseLong(request.getParameter("iD")));
			}
			
			String Value = "";

			Value = request.getParameter("nameEn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("nameEn = " + Value);
			if(Value != null)
			{
				therapy_typeDTO.nameEn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("nameBn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("nameBn = " + Value);
			if(Value != null)
			{
				therapy_typeDTO.nameBn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			
			Value = request.getParameter("ordering");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("ordering = " + Value);
			if(Value != null)
			{
				therapy_typeDTO.ordering = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			if(addFlag)
			{
				therapy_typeDTO.insertedByUserId = userDTO.ID;
			}


			if(addFlag)
			{
				therapy_typeDTO.insertedByOrganogramId = userDTO.organogramID;
			}


			if(addFlag)
			{				
				therapy_typeDTO.insertionDate = TimeConverter.getToday();
			}			


		
			
			System.out.println("Done adding  addTherapy_type dto = " + therapy_typeDTO);
			long returnedID = -1;
			
			List<TherapyDiseaseDTO> therapyDiseaseDTOList = createTherapyDiseaseDTOListByRequest(request, language);
			
			if(addFlag == true)
			{
				returnedID = Therapy_typeDAO.getInstance().add(therapy_typeDTO);
			}
			else
			{				
				returnedID = Therapy_typeDAO.getInstance().update(therapy_typeDTO);										
			}
			
		
			if(addFlag == true) //add or validate
			{
				if(therapyDiseaseDTOList != null)
				{				
					for(TherapyDiseaseDTO therapyDiseaseDTO: therapyDiseaseDTOList)
					{
						therapyDiseaseDTO.therapyTypeId = therapy_typeDTO.iD; 
						therapyDiseaseDAO.add(therapyDiseaseDTO);
					}
				}
			
			}			
			else
			{
				List<Long> childIdsFromRequest = therapyDiseaseDAO.getChildIdsFromRequest(request, "therapyDisease");
				//delete the removed children
				therapyDiseaseDAO.deleteChildrenNotInList("therapy_type", "therapy_disease", therapy_typeDTO.iD, childIdsFromRequest);
				List<Long> childIDsInDatabase = therapyDiseaseDAO.getChilIds("therapy_type", "therapy_disease", therapy_typeDTO.iD);
				
				
				if(childIdsFromRequest != null)
				{
					for(int i = 0; i < childIdsFromRequest.size(); i ++)
					{
						Long childIDFromRequest = childIdsFromRequest.get(i);
						if(childIDsInDatabase.contains(childIDFromRequest))
						{
							TherapyDiseaseDTO therapyDiseaseDTO =  createTherapyDiseaseDTOByRequestAndIndex(request, false, i, language);
							therapyDiseaseDTO.therapyTypeId = therapy_typeDTO.iD; 
							therapyDiseaseDAO.update(therapyDiseaseDTO);
						}
						else
						{
							TherapyDiseaseDTO therapyDiseaseDTO =  createTherapyDiseaseDTOByRequestAndIndex(request, true, i, language);
							therapyDiseaseDTO.therapyTypeId = therapy_typeDTO.iD; 
							therapyDiseaseDAO.add(therapyDiseaseDTO);
						}
					}
				}
				else
				{
					therapyDiseaseDAO.deleteChildrenByParent(therapy_typeDTO.iD, "therapy_type_id");
				}
				
			}					
			return therapy_typeDTO;
					
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			return null;
		}
	}
	private List<TherapyDiseaseDTO> createTherapyDiseaseDTOListByRequest(HttpServletRequest request, String language) throws Exception{ 
		List<TherapyDiseaseDTO> therapyDiseaseDTOList = new ArrayList<TherapyDiseaseDTO>();
		if(request.getParameterValues("therapyDisease.iD") != null) 
		{
			int therapyDiseaseItemNo = request.getParameterValues("therapyDisease.iD").length;
			
			
			for(int index=0;index<therapyDiseaseItemNo;index++){
				TherapyDiseaseDTO therapyDiseaseDTO = createTherapyDiseaseDTOByRequestAndIndex(request,true,index, language);
				therapyDiseaseDTOList.add(therapyDiseaseDTO);
			}
			
			return therapyDiseaseDTOList;
		}
		return null;
	}
	
	private TherapyDiseaseDTO createTherapyDiseaseDTOByRequestAndIndex(HttpServletRequest request, boolean addFlag, int index, String language) throws Exception{
	
		TherapyDiseaseDTO therapyDiseaseDTO;
		if(addFlag == true )
		{
			therapyDiseaseDTO = new TherapyDiseaseDTO();
		}
		else
		{
			therapyDiseaseDTO = therapyDiseaseDAO.getDTOByID(Long.parseLong(request.getParameterValues("therapyDisease.iD")[index]));
		}
		String path = getServletContext().getRealPath("/img2/");
		SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
		
		
				
		String Value = "";
		Value = request.getParameterValues("therapyDisease.therapyTypeId")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		else
		{
			throw new Exception(LM.getText(LC.THERAPY_TYPE_ADD_THERAPY_DISEASE_THERAPYTYPEID, language) + " " + ErrorMessage.getInvalidMessage(language));
		}

		therapyDiseaseDTO.therapyTypeId = Long.parseLong(Value);
		Value = request.getParameterValues("therapyDisease.nameEn")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		else
		{
			throw new Exception(LM.getText(LC.THERAPY_TYPE_ADD_THERAPY_DISEASE_NAMEEN, language) + " " + ErrorMessage.getInvalidMessage(language));
		}

		therapyDiseaseDTO.nameEn = (Value);
		Value = request.getParameterValues("therapyDisease.nameBn")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		else
		{
			throw new Exception(LM.getText(LC.THERAPY_TYPE_ADD_THERAPY_DISEASE_NAMEBN, language) + " " + ErrorMessage.getInvalidMessage(language));
		}

		therapyDiseaseDTO.nameBn = (Value);
		return therapyDiseaseDTO;
	
	}	
}

