package therapy_type;
import java.util.*; 
import util.*; 


public class Therapy_typeDTO extends CommonDTO
{

    public String nameEn = "";
    public String nameBn = "";
	public long insertedByUserId = -1;
	public long insertedByOrganogramId = -1;
	public long insertionDate = -1;
	public int ordering = 0;
	
	public List<TherapyDiseaseDTO> therapyDiseaseDTOList = new ArrayList<>();
	
    @Override
	public String toString() {
            return "$Therapy_typeDTO[" +
            " iD = " + iD +
            " nameEn = " + nameEn +
            " nameBn = " + nameBn +
            " insertedByUserId = " + insertedByUserId +
            " insertedByOrganogramId = " + insertedByOrganogramId +
            " insertionDate = " + insertionDate +
            " searchColumn = " + searchColumn +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}