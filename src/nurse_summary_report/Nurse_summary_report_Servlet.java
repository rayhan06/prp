package nurse_summary_report;


import java.io.IOException;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import language.LC;
import language.LM;

import login.LoginDTO;
import permission.MenuConstants;

import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;

import util.ReportRequestHandler;
import pbReport.*;






@WebServlet("/Nurse_summary_report_Servlet")
public class Nurse_summary_report_Servlet  extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	String[][] Criteria =
	{
		{"criteria","","action_date",">=","","long","","",Long.MIN_VALUE + "","startDate", LC.HM_START_DATE + ""},		
		{"criteria","","action_date","<=","AND","long","","",Long.MAX_VALUE + "","endDate", LC.HM_END_DATE + ""},		
		{"criteria","nurse_action_details","nurse_employee_record_id","=","AND","String","","","any","nurse_user_name",LC.HM_NURSE + "", "userNameToEmployeeRecordId"},	
		{"criteria","","nurse_action_cat","=","AND","int","","","any","nurseActionCat", LC.HM_ACTION + ""}		
	};
	
	String[][] Display =
	{
		{"display","","nurse_action_details.nurse_employee_record_id","erIdToName",""},			
		{"display","","nurse_action_cat","cat",""},		
		{"display","","COUNT(id)","text",""}		
	};
	
	String GroupBy = "nurse_employee_record_id, nurse_action_cat";
	String OrderBY = "";
	
	ReportRequestHandler reportRequestHandler;
	
	public Nurse_summary_report_Servlet(){

	}
	
	private final ReportService reportService = new ReportService();
	
	private String sql;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String language = "english";
		if(userDTO!= null && userDTO.languageID == SessionConstants.BANGLA)
		{
			language = "bangla";
		}
				
		String actionType = request.getParameter("actionType");

		System.out.println("In ssservlet doget, actiontype = " + actionType);
		
		sql = "nurse_action_details";

		Display[0][4] = LM.getText(LC.HM_NURSE, loginDTO);
		Display[1][4] = LM.getText(LC.HM_ACTION, loginDTO);
		Display[2][4] = LM.getText(LC.HM_COUNT, loginDTO);

		
		String reportName = LM.getText(LC.NURSE_SUMMARY_REPORT_OTHER_NURSE_SUMMARY_REPORT, loginDTO);
		
		reportRequestHandler = new ReportRequestHandler(null,
				Criteria, Display, GroupBy, OrderBY, sql,
				reportService);

		
		reportRequestHandler.handleReportGet(request, response, userDTO, "nurse_summary_report",
				MenuConstants.NURSE_SUMMARY_REPORT_DETAILS, language, reportName, "nurse_summary_report");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		doGet(request, response);
	}
}
