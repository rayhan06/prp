package data_center;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.sql.SQLException;
import common.CommonDAOService;
import org.apache.log4j.Logger;
import util.*;
import pb.*;

public class DataCenterHardDiskDAO  implements CommonDAOService<DataCenterHardDiskDTO>
{
	
	Logger logger = Logger.getLogger(getClass());
	
	private static String addQuery = "";
	private static String updateQuery = "";
	private String[] columnNames = null;

	private DataCenterHardDiskDAO()
	{
		columnNames = new String[] 
		{
			"ID",
			"data_center_id",
			"hd_cat",
			"hd_size_cat",
			"isDeleted",
			"lastModificationTime"
		};
		updateQuery = getUpdateQuery(columnNames);
        addQuery = getAddQuery(columnNames);
		
		searchMap.put("hd_cat"," and (hd_cat = ?)");
		searchMap.put("hd_size_cat"," and (hd_size_cat = ?)");
		searchMap.put("AnyField"," and (search_column like ?)");
	}

	private final Map<String,String> searchMap = new HashMap<>();

    private static class LazyLoader
    {
    	static final DataCenterHardDiskDAO INSTANCE = new DataCenterHardDiskDAO();
	}

	public static DataCenterHardDiskDAO getInstance()
	{
    	return LazyLoader.INSTANCE;
	}
	
	public void setSearchColumn(DataCenterHardDiskDTO datacenterharddiskDTO)
	{
		datacenterharddiskDTO.searchColumn = "";
		datacenterharddiskDTO.searchColumn += CatDAO.getName("English", "hd", datacenterharddiskDTO.hdCat) + " " + CatDAO.getName("Bangla", "hd", datacenterharddiskDTO.hdCat) + " ";
		datacenterharddiskDTO.searchColumn += CatDAO.getName("English", "hd_size", datacenterharddiskDTO.hdSizeCat) + " " + CatDAO.getName("Bangla", "hd_size", datacenterharddiskDTO.hdSizeCat) + " ";
	}
	
	@Override
	public void set(PreparedStatement ps, DataCenterHardDiskDTO datacenterharddiskDTO, boolean isInsert) throws SQLException
	{
		int index = 0;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(datacenterharddiskDTO);
		if(isInsert)
		{
			ps.setObject(++index,datacenterharddiskDTO.iD);
		}
		ps.setObject(++index,datacenterharddiskDTO.dataCenterId);
		ps.setObject(++index,datacenterharddiskDTO.hdCat);
		ps.setObject(++index,datacenterharddiskDTO.hdSizeCat);
		if(isInsert)
		{
			ps.setObject(++index,datacenterharddiskDTO.isDeleted);
		}
		ps.setObject(++index, lastModificationTime);
		if(!isInsert)
		{
			ps.setObject(++index,datacenterharddiskDTO.iD);
		}
	}
	
	@Override
	public DataCenterHardDiskDTO buildObjectFromResultSet(ResultSet rs)
	{
		try
		{
			DataCenterHardDiskDTO datacenterharddiskDTO = new DataCenterHardDiskDTO();
			int i = 0;
			datacenterharddiskDTO.iD = rs.getLong(columnNames[i++]);
			datacenterharddiskDTO.dataCenterId = rs.getLong(columnNames[i++]);
			datacenterharddiskDTO.hdCat = rs.getInt(columnNames[i++]);
			datacenterharddiskDTO.hdSizeCat = rs.getInt(columnNames[i++]);
			datacenterharddiskDTO.isDeleted = rs.getInt(columnNames[i++]);
			datacenterharddiskDTO.lastModificationTime = rs.getLong(columnNames[i++]);
			return datacenterharddiskDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
		
	public DataCenterHardDiskDTO getDTOByID (long id)
	{
		return getDTOFromID(id);
	}

	@Override
	public String getTableName() {
		return "data_center_hard_disk";
	}

	@Override
	public Map<String, String> getSearchMap() {
		return searchMap;
	}

	public long add(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((DataCenterHardDiskDTO) commonDTO,addQuery,true);
	}

	public long update(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((DataCenterHardDiskDTO) commonDTO,updateQuery,false);
	}
	
	@Override
	public String getLastModifierUser(){
		return "";
    }
				
}
	