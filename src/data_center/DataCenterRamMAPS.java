package data_center;
import java.util.*; 
import util.*;


public class DataCenterRamMAPS extends CommonMaps
{	
	public DataCenterRamMAPS(String tableName)
	{
		


		java_SQL_map.put("data_center_id".toLowerCase(), "dataCenterId".toLowerCase());
		java_SQL_map.put("ram_cat".toLowerCase(), "ramCat".toLowerCase());
		java_SQL_map.put("ram_size_cat".toLowerCase(), "ramSizeCat".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Data Center Id".toLowerCase(), "dataCenterId".toLowerCase());
		java_Text_map.put("Ram".toLowerCase(), "ramCat".toLowerCase());
		java_Text_map.put("Ram Size".toLowerCase(), "ramSizeCat".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}