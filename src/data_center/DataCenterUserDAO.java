package data_center;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.sql.SQLException;
import common.CommonDAOService;
import org.apache.log4j.Logger;
import util.*;
import pb.*;

public class DataCenterUserDAO  implements CommonDAOService<DataCenterUserDTO>
{
	
	Logger logger = Logger.getLogger(getClass());
	
	private static String addQuery = "";
	private static String updateQuery = "";
	private String[] columnNames = null;

	private DataCenterUserDAO()
	{
		columnNames = new String[] 
		{
			"ID",
			"data_center_id",
			"user_type_cat",
			"user_name",
			"password",
			"isDeleted",
			"lastModificationTime"
		};
		updateQuery = getUpdateQuery(columnNames);
        addQuery = getAddQuery(columnNames);
		
		searchMap.put("user_type_cat"," and (user_type_cat = ?)");
		searchMap.put("user_name"," and (user_name like ?)");
		searchMap.put("AnyField"," and (search_column like ?)");
	}

	private final Map<String,String> searchMap = new HashMap<>();

    private static class LazyLoader
    {
    	static final DataCenterUserDAO INSTANCE = new DataCenterUserDAO();
	}

	public static DataCenterUserDAO getInstance()
	{
    	return LazyLoader.INSTANCE;
	}
	
	public void setSearchColumn(DataCenterUserDTO datacenteruserDTO)
	{
		datacenteruserDTO.searchColumn = "";
		datacenteruserDTO.searchColumn += CatDAO.getName("English", "user_type", datacenteruserDTO.userTypeCat) + " " + CatDAO.getName("Bangla", "user_type", datacenteruserDTO.userTypeCat) + " ";
		datacenteruserDTO.searchColumn += datacenteruserDTO.userName + " ";
	}
	
	@Override
	public void set(PreparedStatement ps, DataCenterUserDTO datacenteruserDTO, boolean isInsert) throws SQLException
	{
		int index = 0;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(datacenteruserDTO);
		if(isInsert)
		{
			ps.setObject(++index,datacenteruserDTO.iD);
		}
		ps.setObject(++index,datacenteruserDTO.dataCenterId);
		ps.setObject(++index,datacenteruserDTO.userTypeCat);
		ps.setObject(++index,datacenteruserDTO.userName);
		ps.setObject(++index,datacenteruserDTO.password);
		if(isInsert)
		{
			ps.setObject(++index,datacenteruserDTO.isDeleted);
		}
		ps.setObject(++index, lastModificationTime);
		if(!isInsert)
		{
			ps.setObject(++index,datacenteruserDTO.iD);
		}
	}
	
	@Override
	public DataCenterUserDTO buildObjectFromResultSet(ResultSet rs)
	{
		try
		{
			DataCenterUserDTO datacenteruserDTO = new DataCenterUserDTO();
			int i = 0;
			datacenteruserDTO.iD = rs.getLong(columnNames[i++]);
			datacenteruserDTO.dataCenterId = rs.getLong(columnNames[i++]);
			datacenteruserDTO.userTypeCat = rs.getInt(columnNames[i++]);
			datacenteruserDTO.userName = rs.getString(columnNames[i++]);
			datacenteruserDTO.password = rs.getString(columnNames[i++]);
			datacenteruserDTO.isDeleted = rs.getInt(columnNames[i++]);
			datacenteruserDTO.lastModificationTime = rs.getLong(columnNames[i++]);
			return datacenteruserDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
		
	public DataCenterUserDTO getDTOByID (long id)
	{
		return getDTOFromID(id);
	}

	@Override
	public String getTableName() {
		return "data_center_user";
	}

	@Override
	public Map<String, String> getSearchMap() {
		return searchMap;
	}

	public long add(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((DataCenterUserDTO) commonDTO,addQuery,true);
	}

	public long update(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((DataCenterUserDTO) commonDTO,updateQuery,false);
	}
	
	@Override
	public String getLastModifierUser(){
		return "";
    }
				
}
	