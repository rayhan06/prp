package data_center;
import java.util.*; 
import util.*; 


public class DataCenterUserDTO extends CommonDTO
{

	public long dataCenterId = -1;
	public int userTypeCat = -1;
    public String userName = "";
    public String password = "";
	
	public List<DataCenterRamDTO> dataCenterRamDTOList = new ArrayList<>();
	public List<DataCenterHardDiskDTO> dataCenterHardDiskDTOList = new ArrayList<>();
	public List<DataCenterUserDTO> dataCenterUserDTOList = new ArrayList<>();
	
    @Override
	public String toString() {
            return "$DataCenterUserDTO[" +
            " iD = " + iD +
            " dataCenterId = " + dataCenterId +
            " userTypeCat = " + userTypeCat +
            " userName = " + userName +
            " password = " + password +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}