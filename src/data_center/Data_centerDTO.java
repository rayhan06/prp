package data_center;
import java.util.*; 
import util.*; 


public class Data_centerDTO extends CommonDTO
{

	public int dataCenterTypeCat = -1;
	public int clusterNameCat = -1;
    public String serverName = "";
    public String serverIp = "";
    public String vmName = "";
    public String serverDetails = "";
    public String hostName = "";
	public int osVersionCat = -1;
	public int cpu = 1;
	public int processorGenCat = -1;
	public int processorCat = -1;
	public int totalRam = -1;
	public int totalHd = -1;
	public int dataStoreCat = -1;
	public int networkAdapterCat = -1;
    public String natUserIp = "";
    public String realIp = "";
    public String activity = "";
	public long assetModelId = -1;
	public long assetAssigneeId = -1;
	public long insertedByUserId = -1;
	public long insertedByOrganogramId = -1;
	public long insertionDate = -1;
    public String lastModifierUser = "";
	
	public List<DataCenterRamDTO> dataCenterRamDTOList = new ArrayList<>();
	public List<DataCenterHardDiskDTO> dataCenterHardDiskDTOList = new ArrayList<>();
	public List<DataCenterUserDTO> dataCenterUserDTOList = new ArrayList<>();
	
    @Override
	public String toString() {
            return "$Data_centerDTO[" +
            " iD = " + iD +
            " dataCenterTypeCat = " + dataCenterTypeCat +
            " clusterNameCat = " + clusterNameCat +
            " serverName = " + serverName +
            " serverIp = " + serverIp +
            " vmName = " + vmName +
            " serverDetails = " + serverDetails +
            " hostName = " + hostName +
            " osVersionCat = " + osVersionCat +
            " cpu = " + cpu +
            " processorGenCat = " + processorGenCat +
            " processorCat = " + processorCat +
            " totalRam = " + totalRam +
            " totalHd = " + totalHd +
            " dataStoreCat = " + dataStoreCat +
            " networkAdapterCat = " + networkAdapterCat +
            " natUserIp = " + natUserIp +
            " realIp = " + realIp +
            " activity = " + activity +
            " assetModelId = " + assetModelId +
            " assetAssigneeId = " + assetAssigneeId +
            " searchColumn = " + searchColumn +
            " insertedByUserId = " + insertedByUserId +
            " insertedByOrganogramId = " + insertedByOrganogramId +
            " insertionDate = " + insertionDate +
            " lastModifierUser = " + lastModifierUser +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}