package data_center;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import com.google.gson.Gson;

import repository.Repository;
import repository.RepositoryManager;


public class DataCenterRamRepository implements Repository {
	DataCenterRamDAO datacenterramDAO = null;
	
	static Logger logger = Logger.getLogger(DataCenterRamRepository.class);
	Map<Long, DataCenterRamDTO>mapOfDataCenterRamDTOToiD;
	Map<Long, Set<DataCenterRamDTO> >mapOfDataCenterRamDTOTodataCenterId;
	Gson gson;

  
	private DataCenterRamRepository(){
		datacenterramDAO = DataCenterRamDAO.getInstance();
		mapOfDataCenterRamDTOToiD = new ConcurrentHashMap<>();
		mapOfDataCenterRamDTOTodataCenterId = new ConcurrentHashMap<>();
		gson = new Gson();
		RepositoryManager.getInstance().addRepository(this);
	}

	private static class LazyLoader{
        final static DataCenterRamRepository INSTANCE = new DataCenterRamRepository();
    }

    public static DataCenterRamRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

	public void reload(boolean reloadAll){
		try {
			List<DataCenterRamDTO> datacenterramDTOs = datacenterramDAO.getAllDTOs(reloadAll);
			for(DataCenterRamDTO datacenterramDTO : datacenterramDTOs) {
				DataCenterRamDTO oldDataCenterRamDTO = getDataCenterRamDTOByiD(datacenterramDTO.iD);
				if( oldDataCenterRamDTO != null ) {
					mapOfDataCenterRamDTOToiD.remove(oldDataCenterRamDTO.iD);
				
					if(mapOfDataCenterRamDTOTodataCenterId.containsKey(oldDataCenterRamDTO.dataCenterId)) {
						mapOfDataCenterRamDTOTodataCenterId.get(oldDataCenterRamDTO.dataCenterId).remove(oldDataCenterRamDTO);
					}
					if(mapOfDataCenterRamDTOTodataCenterId.get(oldDataCenterRamDTO.dataCenterId).isEmpty()) {
						mapOfDataCenterRamDTOTodataCenterId.remove(oldDataCenterRamDTO.dataCenterId);
					}
					
					
				}
				if(datacenterramDTO.isDeleted == 0) 
				{
					
					mapOfDataCenterRamDTOToiD.put(datacenterramDTO.iD, datacenterramDTO);
				
					if( ! mapOfDataCenterRamDTOTodataCenterId.containsKey(datacenterramDTO.dataCenterId)) {
						mapOfDataCenterRamDTOTodataCenterId.put(datacenterramDTO.dataCenterId, new HashSet<>());
					}
					mapOfDataCenterRamDTOTodataCenterId.get(datacenterramDTO.dataCenterId).add(datacenterramDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public DataCenterRamDTO clone(DataCenterRamDTO dto) {
		String raw = gson.toJson(dto);
		return gson.fromJson(raw, DataCenterRamDTO.class);
	}
	
	
	public List<DataCenterRamDTO> getDataCenterRamList() {
		List <DataCenterRamDTO> datacenterrams = new ArrayList<DataCenterRamDTO>(this.mapOfDataCenterRamDTOToiD.values());
		return datacenterrams;
	}
	
	public List<DataCenterRamDTO> copyDataCenterRamList() {
		List <DataCenterRamDTO> datacenterrams = getDataCenterRamList();
		return datacenterrams
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}
	
	
	
	public DataCenterRamDTO getDataCenterRamDTOByiD( long iD){
		return mapOfDataCenterRamDTOToiD.get(iD);
	}
	
	public DataCenterRamDTO copyDataCenterRamDTOByiD( long iD){
		return clone(mapOfDataCenterRamDTOToiD.get(iD));
	}
	
	
	public List<DataCenterRamDTO> getDataCenterRamDTOBydataCenterId(long dataCenterId) {
		return new ArrayList<>( mapOfDataCenterRamDTOTodataCenterId.getOrDefault(dataCenterId,new HashSet<>()));
	}
	
	public List<DataCenterRamDTO> copyDataCenterRamDTOBydataCenterId(long dataCenterId)
	{
		List <DataCenterRamDTO> datacenterrams = getDataCenterRamDTOBydataCenterId(dataCenterId);
		return datacenterrams
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}

	
	@Override
	public String getTableName() {
		return datacenterramDAO.getTableName();
	}
}


