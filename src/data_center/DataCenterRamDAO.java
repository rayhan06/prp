package data_center;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.sql.SQLException;
import common.CommonDAOService;
import org.apache.log4j.Logger;
import util.*;
import pb.*;

public class DataCenterRamDAO  implements CommonDAOService<DataCenterRamDTO>
{
	
	Logger logger = Logger.getLogger(getClass());
	
	private static String addQuery = "";
	private static String updateQuery = "";
	private String[] columnNames = null;

	private DataCenterRamDAO()
	{
		columnNames = new String[] 
		{
			"ID",
			"data_center_id",
			"ram_cat",
			"ram_size_cat",
			"isDeleted",
			"lastModificationTime"
		};
		updateQuery = getUpdateQuery(columnNames);
        addQuery = getAddQuery(columnNames);
		
		searchMap.put("ram_cat"," and (ram_cat = ?)");
		searchMap.put("ram_size_cat"," and (ram_size_cat = ?)");
		searchMap.put("AnyField"," and (search_column like ?)");
	}

	private final Map<String,String> searchMap = new HashMap<>();

    private static class LazyLoader
    {
    	static final DataCenterRamDAO INSTANCE = new DataCenterRamDAO();
	}

	public static DataCenterRamDAO getInstance()
	{
    	return LazyLoader.INSTANCE;
	}
	
	public void setSearchColumn(DataCenterRamDTO datacenterramDTO)
	{
		datacenterramDTO.searchColumn = "";
		datacenterramDTO.searchColumn += CatDAO.getName("English", "ram", datacenterramDTO.ramCat) + " " + CatDAO.getName("Bangla", "ram", datacenterramDTO.ramCat) + " ";
		datacenterramDTO.searchColumn += CatDAO.getName("English", "ram_size", datacenterramDTO.ramSizeCat) + " " + CatDAO.getName("Bangla", "ram_size", datacenterramDTO.ramSizeCat) + " ";
	}
	
	@Override
	public void set(PreparedStatement ps, DataCenterRamDTO datacenterramDTO, boolean isInsert) throws SQLException
	{
		int index = 0;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(datacenterramDTO);
		if(isInsert)
		{
			ps.setObject(++index,datacenterramDTO.iD);
		}
		ps.setObject(++index,datacenterramDTO.dataCenterId);
		ps.setObject(++index,datacenterramDTO.ramCat);
		ps.setObject(++index,datacenterramDTO.ramSizeCat);
		if(isInsert)
		{
			ps.setObject(++index,datacenterramDTO.isDeleted);
		}
		ps.setObject(++index, lastModificationTime);
		if(!isInsert)
		{
			ps.setObject(++index,datacenterramDTO.iD);
		}
	}
	
	@Override
	public DataCenterRamDTO buildObjectFromResultSet(ResultSet rs)
	{
		try
		{
			DataCenterRamDTO datacenterramDTO = new DataCenterRamDTO();
			int i = 0;
			datacenterramDTO.iD = rs.getLong(columnNames[i++]);
			datacenterramDTO.dataCenterId = rs.getLong(columnNames[i++]);
			datacenterramDTO.ramCat = rs.getInt(columnNames[i++]);
			datacenterramDTO.ramSizeCat = rs.getInt(columnNames[i++]);
			datacenterramDTO.isDeleted = rs.getInt(columnNames[i++]);
			datacenterramDTO.lastModificationTime = rs.getLong(columnNames[i++]);
			return datacenterramDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
		
	public DataCenterRamDTO getDTOByID (long id)
	{
		return getDTOFromID(id);
	}

	@Override
	public String getTableName() {
		return "data_center_ram";
	}

	@Override
	public Map<String, String> getSearchMap() {
		return searchMap;
	}

	public long add(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((DataCenterRamDTO) commonDTO,addQuery,true);
	}

	public long update(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((DataCenterRamDTO) commonDTO,updateQuery,false);
	}
	
	@Override
	public String getLastModifierUser(){
		return "";
    }
				
}
	