package data_center;
import java.util.*; 
import util.*;


public class DataCenterUserMAPS extends CommonMaps
{	
	public DataCenterUserMAPS(String tableName)
	{
		


		java_SQL_map.put("data_center_id".toLowerCase(), "dataCenterId".toLowerCase());
		java_SQL_map.put("user_type_cat".toLowerCase(), "userTypeCat".toLowerCase());
		java_SQL_map.put("user_name".toLowerCase(), "userName".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Data Center Id".toLowerCase(), "dataCenterId".toLowerCase());
		java_Text_map.put("User Type".toLowerCase(), "userTypeCat".toLowerCase());
		java_Text_map.put("User Name".toLowerCase(), "userName".toLowerCase());
		java_Text_map.put("Password".toLowerCase(), "password".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}