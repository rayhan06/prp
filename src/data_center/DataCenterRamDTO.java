package data_center;
import java.util.*; 
import util.*; 


public class DataCenterRamDTO extends CommonDTO
{

	public long dataCenterId = -1;
	public int ramCat = -1;
	public int ramSizeCat = -1;
	
	public List<DataCenterRamDTO> dataCenterRamDTOList = new ArrayList<>();
	public List<DataCenterHardDiskDTO> dataCenterHardDiskDTOList = new ArrayList<>();
	public List<DataCenterUserDTO> dataCenterUserDTOList = new ArrayList<>();
	
    @Override
	public String toString() {
            return "$DataCenterRamDTO[" +
            " iD = " + iD +
            " dataCenterId = " + dataCenterId +
            " ramCat = " + ramCat +
            " ramSizeCat = " + ramSizeCat +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}