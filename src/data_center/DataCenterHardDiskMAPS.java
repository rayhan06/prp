package data_center;
import java.util.*; 
import util.*;


public class DataCenterHardDiskMAPS extends CommonMaps
{	
	public DataCenterHardDiskMAPS(String tableName)
	{
		


		java_SQL_map.put("data_center_id".toLowerCase(), "dataCenterId".toLowerCase());
		java_SQL_map.put("hd_cat".toLowerCase(), "hdCat".toLowerCase());
		java_SQL_map.put("hd_size_cat".toLowerCase(), "hdSizeCat".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Data Center Id".toLowerCase(), "dataCenterId".toLowerCase());
		java_Text_map.put("Hd".toLowerCase(), "hdCat".toLowerCase());
		java_Text_map.put("Hd Size".toLowerCase(), "hdSizeCat".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}