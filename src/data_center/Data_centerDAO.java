package data_center;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.sql.SQLException;
import common.CommonDAOService;
import org.apache.log4j.Logger;
import util.*;
import pb.*;

public class Data_centerDAO  implements CommonDAOService<Data_centerDTO>
{
	
	Logger logger = Logger.getLogger(getClass());
	
	private static String addQuery = "";
	private static String updateQuery = "";
	private String[] columnNames = null;

	private Data_centerDAO()
	{
		columnNames = new String[] 
		{
			"ID",
			"data_center_type_cat",
			"cluster_name_cat",
			"server_name",
			"server_ip",
			"vm_name",
			"server_details",
			"host_name",
			"os_version_cat",
			"cpu",
			"processor_gen_cat",
			"processor_cat",
			"total_ram",
			"total_hd",
			"data_store_cat",
			"network_adapter_cat",
			"nat_user_ip",
			"real_ip",
			"activity",
			"asset_model_id",
			"asset_assignee_id",
			"search_column",
			"inserted_by_user_id",
			"inserted_by_organogram_id",
			"insertion_date",
			"last_modifier_user",
			"isDeleted",
			"lastModificationTime"
		};
		updateQuery = getUpdateQuery(columnNames);
        addQuery = getAddQuery(columnNames);
		
		searchMap.put("data_center_type_cat"," and (data_center_type_cat = ?)");
		searchMap.put("cluster_name_cat"," and (cluster_name_cat = ?)");
		searchMap.put("server_name"," and (server_name like ?)");
		searchMap.put("server_ip"," and (server_ip like ?)");
		searchMap.put("vm_name"," and (vm_name like ?)");
		searchMap.put("server_details"," and (server_details like ?)");
		searchMap.put("host_name"," and (host_name like ?)");
		searchMap.put("os_version_cat"," and (os_version_cat = ?)");
		searchMap.put("processor_gen_cat"," and (processor_gen_cat = ?)");
		searchMap.put("processor_cat"," and (processor_cat = ?)");
		searchMap.put("data_store_cat"," and (data_store_cat = ?)");
		searchMap.put("network_adapter_cat"," and (network_adapter_cat = ?)");
		searchMap.put("nat_user_ip"," and (nat_user_ip like ?)");
		searchMap.put("real_ip"," and (real_ip like ?)");
		searchMap.put("activity"," and (activity like ?)");
		searchMap.put("insertion_date_start"," and (insertion_date >= ?)");
		searchMap.put("insertion_date_end"," and (insertion_date <= ?)");
		searchMap.put("AnyField"," and (search_column like ?)");
	}

	private final Map<String,String> searchMap = new HashMap<>();

    private static class LazyLoader
    {
    	static final Data_centerDAO INSTANCE = new Data_centerDAO();
	}

	public static Data_centerDAO getInstance()
	{
    	return LazyLoader.INSTANCE;
	}
	
	public void setSearchColumn(Data_centerDTO data_centerDTO)
	{
		data_centerDTO.searchColumn = "";
		data_centerDTO.searchColumn += CatDAO.getName("English", "data_center_type", data_centerDTO.dataCenterTypeCat) + " " + CatDAO.getName("Bangla", "data_center_type", data_centerDTO.dataCenterTypeCat) + " ";
		data_centerDTO.searchColumn += CatDAO.getName("English", "cluster_name", data_centerDTO.clusterNameCat) + " " + CatDAO.getName("Bangla", "cluster_name", data_centerDTO.clusterNameCat) + " ";
		data_centerDTO.searchColumn += data_centerDTO.serverName + " ";
		data_centerDTO.searchColumn += data_centerDTO.serverIp + " ";
		data_centerDTO.searchColumn += data_centerDTO.vmName + " ";
		data_centerDTO.searchColumn += data_centerDTO.serverDetails + " ";
		data_centerDTO.searchColumn += data_centerDTO.hostName + " ";
		data_centerDTO.searchColumn += CatDAO.getName("English", "os_version", data_centerDTO.osVersionCat) + " " + CatDAO.getName("Bangla", "os_version", data_centerDTO.osVersionCat) + " ";
		data_centerDTO.searchColumn += CatDAO.getName("English", "processor_gen", data_centerDTO.processorGenCat) + " " + CatDAO.getName("Bangla", "processor_gen", data_centerDTO.processorGenCat) + " ";
		data_centerDTO.searchColumn += CatDAO.getName("English", "processor", data_centerDTO.processorCat) + " " + CatDAO.getName("Bangla", "processor", data_centerDTO.processorCat) + " ";
		data_centerDTO.searchColumn += CatDAO.getName("English", "data_store", data_centerDTO.dataStoreCat) + " " + CatDAO.getName("Bangla", "data_store", data_centerDTO.dataStoreCat) + " ";
		data_centerDTO.searchColumn += CatDAO.getName("English", "network_adapter", data_centerDTO.networkAdapterCat) + " " + CatDAO.getName("Bangla", "network_adapter", data_centerDTO.networkAdapterCat) + " ";
		data_centerDTO.searchColumn += data_centerDTO.natUserIp + " ";
		data_centerDTO.searchColumn += data_centerDTO.realIp + " ";
		data_centerDTO.searchColumn += data_centerDTO.activity + " ";
	}
	
	@Override
	public void set(PreparedStatement ps, Data_centerDTO data_centerDTO, boolean isInsert) throws SQLException
	{
		int index = 0;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(data_centerDTO);
		if(isInsert)
		{
			ps.setObject(++index,data_centerDTO.iD);
		}
		ps.setObject(++index,data_centerDTO.dataCenterTypeCat);
		ps.setObject(++index,data_centerDTO.clusterNameCat);
		ps.setObject(++index,data_centerDTO.serverName);
		ps.setObject(++index,data_centerDTO.serverIp);
		ps.setObject(++index,data_centerDTO.vmName);
		ps.setObject(++index,data_centerDTO.serverDetails);
		ps.setObject(++index,data_centerDTO.hostName);
		ps.setObject(++index,data_centerDTO.osVersionCat);
		ps.setObject(++index,data_centerDTO.cpu);
		ps.setObject(++index,data_centerDTO.processorGenCat);
		ps.setObject(++index,data_centerDTO.processorCat);
		ps.setObject(++index,data_centerDTO.totalRam);
		ps.setObject(++index,data_centerDTO.totalHd);
		ps.setObject(++index,data_centerDTO.dataStoreCat);
		ps.setObject(++index,data_centerDTO.networkAdapterCat);
		ps.setObject(++index,data_centerDTO.natUserIp);
		ps.setObject(++index,data_centerDTO.realIp);
		ps.setObject(++index,data_centerDTO.activity);
		ps.setObject(++index,data_centerDTO.assetModelId);
		ps.setObject(++index,data_centerDTO.assetAssigneeId);
		ps.setObject(++index,data_centerDTO.searchColumn);
		ps.setObject(++index,data_centerDTO.insertedByUserId);
		ps.setObject(++index,data_centerDTO.insertedByOrganogramId);
		ps.setObject(++index,data_centerDTO.insertionDate);
		ps.setObject(++index,data_centerDTO.lastModifierUser);
		if(isInsert)
		{
			ps.setObject(++index,data_centerDTO.isDeleted);
		}
		ps.setObject(++index, lastModificationTime);
		if(!isInsert)
		{
			ps.setObject(++index,data_centerDTO.iD);
		}
	}
	
	@Override
	public Data_centerDTO buildObjectFromResultSet(ResultSet rs)
	{
		try
		{
			Data_centerDTO data_centerDTO = new Data_centerDTO();
			int i = 0;
			data_centerDTO.iD = rs.getLong(columnNames[i++]);
			data_centerDTO.dataCenterTypeCat = rs.getInt(columnNames[i++]);
			data_centerDTO.clusterNameCat = rs.getInt(columnNames[i++]);
			data_centerDTO.serverName = rs.getString(columnNames[i++]);
			data_centerDTO.serverIp = rs.getString(columnNames[i++]);
			data_centerDTO.vmName = rs.getString(columnNames[i++]);
			data_centerDTO.serverDetails = rs.getString(columnNames[i++]);
			data_centerDTO.hostName = rs.getString(columnNames[i++]);
			data_centerDTO.osVersionCat = rs.getInt(columnNames[i++]);
			data_centerDTO.cpu = rs.getInt(columnNames[i++]);
			data_centerDTO.processorGenCat = rs.getInt(columnNames[i++]);
			data_centerDTO.processorCat = rs.getInt(columnNames[i++]);
			data_centerDTO.totalRam = rs.getInt(columnNames[i++]);
			data_centerDTO.totalHd = rs.getInt(columnNames[i++]);
			data_centerDTO.dataStoreCat = rs.getInt(columnNames[i++]);
			data_centerDTO.networkAdapterCat = rs.getInt(columnNames[i++]);
			data_centerDTO.natUserIp = rs.getString(columnNames[i++]);
			data_centerDTO.realIp = rs.getString(columnNames[i++]);
			data_centerDTO.activity = rs.getString(columnNames[i++]);
			data_centerDTO.assetModelId = rs.getLong(columnNames[i++]);
			data_centerDTO.assetAssigneeId = rs.getLong(columnNames[i++]);
			data_centerDTO.searchColumn = rs.getString(columnNames[i++]);
			data_centerDTO.insertedByUserId = rs.getLong(columnNames[i++]);
			data_centerDTO.insertedByOrganogramId = rs.getLong(columnNames[i++]);
			data_centerDTO.insertionDate = rs.getLong(columnNames[i++]);
			data_centerDTO.lastModifierUser = rs.getString(columnNames[i++]);
			data_centerDTO.isDeleted = rs.getInt(columnNames[i++]);
			data_centerDTO.lastModificationTime = rs.getLong(columnNames[i++]);
			return data_centerDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
		
	public Data_centerDTO getDTOByID (long id)
	{
		Data_centerDTO data_centerDTO = null;
		try 
		{
			data_centerDTO = getDTOFromID(id);
			if(data_centerDTO != null)
			{
				DataCenterRamDAO dataCenterRamDAO = DataCenterRamDAO.getInstance();				
				List<DataCenterRamDTO> dataCenterRamDTOList = (List<DataCenterRamDTO>)dataCenterRamDAO.getDTOsByParent("data_center_id", data_centerDTO.iD);
				data_centerDTO.dataCenterRamDTOList = dataCenterRamDTOList;
				DataCenterHardDiskDAO dataCenterHardDiskDAO = DataCenterHardDiskDAO.getInstance();				
				List<DataCenterHardDiskDTO> dataCenterHardDiskDTOList = (List<DataCenterHardDiskDTO>)dataCenterHardDiskDAO.getDTOsByParent("data_center_id", data_centerDTO.iD);
				data_centerDTO.dataCenterHardDiskDTOList = dataCenterHardDiskDTOList;
				DataCenterUserDAO dataCenterUserDAO = DataCenterUserDAO.getInstance();				
				List<DataCenterUserDTO> dataCenterUserDTOList = (List<DataCenterUserDTO>)dataCenterUserDAO.getDTOsByParent("data_center_id", data_centerDTO.iD);
				data_centerDTO.dataCenterUserDTOList = dataCenterUserDTOList;
			}
		}
		catch (Exception e) 
		{
				// TODO Auto-generated catch block
				e.printStackTrace();
		}
		return data_centerDTO;
	}

	@Override
	public String getTableName() {
		return "data_center";
	}

	@Override
	public Map<String, String> getSearchMap() {
		return searchMap;
	}

	public long add(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Data_centerDTO) commonDTO,addQuery,true);
	}

	public long update(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Data_centerDTO) commonDTO,updateQuery,false);
	}
	
	@Override
	public String getLastModifierUser(){
		return "last_modifier_user";
    }
				
}
	