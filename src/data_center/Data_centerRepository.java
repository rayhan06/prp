package data_center;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import com.google.gson.Gson;

import repository.Repository;
import repository.RepositoryManager;


public class Data_centerRepository implements Repository {
	Data_centerDAO data_centerDAO = null;
	
	static Logger logger = Logger.getLogger(Data_centerRepository.class);
	Map<Long, Data_centerDTO>mapOfData_centerDTOToiD;
	Gson gson;

  
	private Data_centerRepository(){
		data_centerDAO = Data_centerDAO.getInstance();
		mapOfData_centerDTOToiD = new ConcurrentHashMap<>();
		gson = new Gson();
		RepositoryManager.getInstance().addRepository(this);
	}

	private static class LazyLoader{
        final static Data_centerRepository INSTANCE = new Data_centerRepository();
    }

    public static Data_centerRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

	public void reload(boolean reloadAll){
		try {
			List<Data_centerDTO> data_centerDTOs = data_centerDAO.getAllDTOs(reloadAll);
			for(Data_centerDTO data_centerDTO : data_centerDTOs) {
				Data_centerDTO oldData_centerDTO = getData_centerDTOByiD(data_centerDTO.iD);
				if( oldData_centerDTO != null ) {
					mapOfData_centerDTOToiD.remove(oldData_centerDTO.iD);
				
					
				}
				if(data_centerDTO.isDeleted == 0) 
				{
					
					mapOfData_centerDTOToiD.put(data_centerDTO.iD, data_centerDTO);
				
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public Data_centerDTO clone(Data_centerDTO dto) {
		String raw = gson.toJson(dto);
		return gson.fromJson(raw, Data_centerDTO.class);
	}
	
	
	public List<Data_centerDTO> getData_centerList() {
		List <Data_centerDTO> data_centers = new ArrayList<Data_centerDTO>(this.mapOfData_centerDTOToiD.values());
		return data_centers;
	}
	
	public List<Data_centerDTO> copyData_centerList() {
		List <Data_centerDTO> data_centers = getData_centerList();
		return data_centers
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}
	
	
	
	public Data_centerDTO getData_centerDTOByiD( long iD){
		return mapOfData_centerDTOToiD.get(iD);
	}
	
	public Data_centerDTO copyData_centerDTOByiD( long iD){
		return clone(mapOfData_centerDTOToiD.get(iD));
	}

	
	@Override
	public String getTableName() {
		return data_centerDAO.getTableName();
	}
}


