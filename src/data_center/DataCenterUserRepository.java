package data_center;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import com.google.gson.Gson;

import repository.Repository;
import repository.RepositoryManager;


public class DataCenterUserRepository implements Repository {
	DataCenterUserDAO datacenteruserDAO = null;
	
	static Logger logger = Logger.getLogger(DataCenterUserRepository.class);
	Map<Long, DataCenterUserDTO>mapOfDataCenterUserDTOToiD;
	Map<Long, Set<DataCenterUserDTO> >mapOfDataCenterUserDTOTodataCenterId;
	Gson gson;

  
	private DataCenterUserRepository(){
		datacenteruserDAO = DataCenterUserDAO.getInstance();
		mapOfDataCenterUserDTOToiD = new ConcurrentHashMap<>();
		mapOfDataCenterUserDTOTodataCenterId = new ConcurrentHashMap<>();
		gson = new Gson();
		RepositoryManager.getInstance().addRepository(this);
	}

	private static class LazyLoader{
        final static DataCenterUserRepository INSTANCE = new DataCenterUserRepository();
    }

    public static DataCenterUserRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

	public void reload(boolean reloadAll){
		try {
			List<DataCenterUserDTO> datacenteruserDTOs = datacenteruserDAO.getAllDTOs(reloadAll);
			for(DataCenterUserDTO datacenteruserDTO : datacenteruserDTOs) {
				DataCenterUserDTO oldDataCenterUserDTO = getDataCenterUserDTOByiD(datacenteruserDTO.iD);
				if( oldDataCenterUserDTO != null ) {
					mapOfDataCenterUserDTOToiD.remove(oldDataCenterUserDTO.iD);
				
					if(mapOfDataCenterUserDTOTodataCenterId.containsKey(oldDataCenterUserDTO.dataCenterId)) {
						mapOfDataCenterUserDTOTodataCenterId.get(oldDataCenterUserDTO.dataCenterId).remove(oldDataCenterUserDTO);
					}
					if(mapOfDataCenterUserDTOTodataCenterId.get(oldDataCenterUserDTO.dataCenterId).isEmpty()) {
						mapOfDataCenterUserDTOTodataCenterId.remove(oldDataCenterUserDTO.dataCenterId);
					}
					
					
				}
				if(datacenteruserDTO.isDeleted == 0) 
				{
					
					mapOfDataCenterUserDTOToiD.put(datacenteruserDTO.iD, datacenteruserDTO);
				
					if( ! mapOfDataCenterUserDTOTodataCenterId.containsKey(datacenteruserDTO.dataCenterId)) {
						mapOfDataCenterUserDTOTodataCenterId.put(datacenteruserDTO.dataCenterId, new HashSet<>());
					}
					mapOfDataCenterUserDTOTodataCenterId.get(datacenteruserDTO.dataCenterId).add(datacenteruserDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public DataCenterUserDTO clone(DataCenterUserDTO dto) {
		String raw = gson.toJson(dto);
		return gson.fromJson(raw, DataCenterUserDTO.class);
	}
	
	
	public List<DataCenterUserDTO> getDataCenterUserList() {
		List <DataCenterUserDTO> datacenterusers = new ArrayList<DataCenterUserDTO>(this.mapOfDataCenterUserDTOToiD.values());
		return datacenterusers;
	}
	
	public List<DataCenterUserDTO> copyDataCenterUserList() {
		List <DataCenterUserDTO> datacenterusers = getDataCenterUserList();
		return datacenterusers
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}
	
	
	
	public DataCenterUserDTO getDataCenterUserDTOByiD( long iD){
		return mapOfDataCenterUserDTOToiD.get(iD);
	}
	
	public DataCenterUserDTO copyDataCenterUserDTOByiD( long iD){
		return clone(mapOfDataCenterUserDTOToiD.get(iD));
	}
	
	
	public List<DataCenterUserDTO> getDataCenterUserDTOBydataCenterId(long dataCenterId) {
		return new ArrayList<>( mapOfDataCenterUserDTOTodataCenterId.getOrDefault(dataCenterId,new HashSet<>()));
	}
	
	public List<DataCenterUserDTO> copyDataCenterUserDTOBydataCenterId(long dataCenterId)
	{
		List <DataCenterUserDTO> datacenterusers = getDataCenterUserDTOBydataCenterId(dataCenterId);
		return datacenterusers
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}

	
	@Override
	public String getTableName() {
		return datacenteruserDAO.getTableName();
	}
}


