package data_center;

import java.io.IOException;
import java.io.*;
import java.text.SimpleDateFormat;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import org.apache.log4j.Logger;

import login.LoginDTO;
import permission.MenuConstants;
import role.PermissionRepository;

import sessionmanager.SessionConstants;
import user.UserDTO;
import util.*;
import javax.servlet.http.*;
import java.util.*;


import geolocation.GeoLocationDAO2;
import language.LC;
import language.LM;
import common.ApiResponse;
import common.BaseServlet;
import com.google.gson.Gson;
import pb.*;
import pbReport.*;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;




/**
 * Servlet implementation class Data_centerServlet
 */
@WebServlet("/Data_centerServlet")
@MultipartConfig
public class Data_centerServlet extends BaseServlet 
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Data_centerServlet.class);

    @Override
    public String getTableName() {
        return Data_centerDAO.getInstance().getTableName();
    }

    @Override
    public String getServletName() {
        return "Data_centerServlet";
    }

    @Override
    public Data_centerDAO getCommonDAOService() {
        return Data_centerDAO.getInstance();
    }
    
    @Override
    public int[] getAddPageMenuConstants() {
        return new int[] {MenuConstants.DATA_CENTER_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[] {MenuConstants.DATA_CENTER_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[] {MenuConstants.DATA_CENTER_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Data_centerServlet.class;
    }
	DataCenterRamDAO dataCenterRamDAO = DataCenterRamDAO.getInstance();
	DataCenterHardDiskDAO dataCenterHardDiskDAO = DataCenterHardDiskDAO.getInstance();
	DataCenterUserDAO dataCenterUserDAO = DataCenterUserDAO.getInstance();
    private final Gson gson = new Gson();
 	

	@Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception 
	{
		
		request.setAttribute("failureMessage", "");
		System.out.println("%%%% addData_center");
		String language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
		Data_centerDTO data_centerDTO;
		SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
					
		if(addFlag)
		{
			data_centerDTO = new Data_centerDTO();
		}
		else
		{
			data_centerDTO = Data_centerDAO.getInstance().getDTOFromID(Long.parseLong(request.getParameter("iD")));
		}
		
		String Value = "";

		Value = request.getParameter("dataCenterTypeCat");

		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("dataCenterTypeCat = " + Value);
		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			data_centerDTO.dataCenterTypeCat = Integer.parseInt(Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}

		Value = request.getParameter("clusterNameCat");

		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("clusterNameCat = " + Value);
		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			data_centerDTO.clusterNameCat = Integer.parseInt(Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}

		Value = request.getParameter("serverName");

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("serverName = " + Value);
		if(Value != null)
		{
			data_centerDTO.serverName = (Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}

		Value = request.getParameter("serverIp");

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("serverIp = " + Value);
		if(Value != null)
		{
			data_centerDTO.serverIp = (Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}

		Value = request.getParameter("vmName");

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("vmName = " + Value);
		if(Value != null)
		{
			data_centerDTO.vmName = (Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}

		Value = request.getParameter("serverDetails");

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("serverDetails = " + Value);
		if(Value != null)
		{
			data_centerDTO.serverDetails = (Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}

		Value = request.getParameter("hostName");

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("hostName = " + Value);
		if(Value != null)
		{
			data_centerDTO.hostName = (Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}

		Value = request.getParameter("osVersionCat");

		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("osVersionCat = " + Value);
		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			data_centerDTO.osVersionCat = Integer.parseInt(Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}

		Value = request.getParameter("cpu");

		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("cpu = " + Value);
		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			data_centerDTO.cpu = Integer.parseInt(Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}

		Value = request.getParameter("processorGenCat");

		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("processorGenCat = " + Value);
		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			data_centerDTO.processorGenCat = Integer.parseInt(Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}

		Value = request.getParameter("processorCat");

		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("processorCat = " + Value);
		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			data_centerDTO.processorCat = Integer.parseInt(Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}

		

		Value = request.getParameter("dataStoreCat");

		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("dataStoreCat = " + Value);
		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			data_centerDTO.dataStoreCat = Integer.parseInt(Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}

		Value = request.getParameter("networkAdapterCat");

		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("networkAdapterCat = " + Value);
		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			data_centerDTO.networkAdapterCat = Integer.parseInt(Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}

		Value = request.getParameter("natUserIp");

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("natUserIp = " + Value);
		if(Value != null)
		{
			data_centerDTO.natUserIp = (Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}

		Value = request.getParameter("realIp");

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("realIp = " + Value);
		if(Value != null)
		{
			data_centerDTO.realIp = (Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}

		Value = request.getParameter("activity");

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("activity = " + Value);
		if(Value != null)
		{
			data_centerDTO.activity = (Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}

		

		if(addFlag)
		{
			data_centerDTO.insertedByUserId = userDTO.ID;
		}


		if(addFlag)
		{
			data_centerDTO.insertedByOrganogramId = userDTO.organogramID;
		}


		if(addFlag)
		{				
			data_centerDTO.insertionDate = TimeConverter.getToday();
		}			


		data_centerDTO.lastModifierUser = userDTO.userName;

		System.out.println("Done adding  addData_center dto = " + data_centerDTO);

		if(addFlag == true)
		{
			Data_centerDAO.getInstance().add(data_centerDTO);
		}
		else
		{				
			Data_centerDAO.getInstance().update(data_centerDTO);										
		}
		
		List<DataCenterRamDTO> dataCenterRamDTOList = createDataCenterRamDTOListByRequest(request, language, data_centerDTO, userDTO);
		List<DataCenterHardDiskDTO> dataCenterHardDiskDTOList = createDataCenterHardDiskDTOListByRequest(request, language, data_centerDTO, userDTO);
		List<DataCenterUserDTO> dataCenterUserDTOList = createDataCenterUserDTOListByRequest(request, language, data_centerDTO, userDTO);
		
		
		
		

		if(addFlag == true) //add or validate
		{
			if(dataCenterRamDTOList != null)
			{				
				for(DataCenterRamDTO dataCenterRamDTO: dataCenterRamDTOList)
				{
					dataCenterRamDAO.add(dataCenterRamDTO);
				}
			}
		
		}			
		else
		{
			List<Long> childIdsFromRequest = dataCenterRamDAO.getChildIdsFromRequest(request, "dataCenterRam");
			//delete the removed children
			dataCenterRamDAO.deleteChildrenNotInList("data_center", "data_center_ram", data_centerDTO.iD, childIdsFromRequest);
			List<Long> childIDsInDatabase = dataCenterRamDAO.getChilIds("data_center", "data_center_ram", data_centerDTO.iD);
			
			
			if(childIdsFromRequest != null)
			{
				for(int i = 0; i < childIdsFromRequest.size(); i ++)
				{
					Long childIDFromRequest = childIdsFromRequest.get(i);
					if(childIDsInDatabase.contains(childIDFromRequest))
					{
						DataCenterRamDTO dataCenterRamDTO =  createDataCenterRamDTOByRequestAndIndex(request, false, i, language, data_centerDTO, userDTO);
						dataCenterRamDAO.update(dataCenterRamDTO);
					}
					else
					{
						DataCenterRamDTO dataCenterRamDTO =  createDataCenterRamDTOByRequestAndIndex(request, true, i, language, data_centerDTO, userDTO);
						dataCenterRamDAO.add(dataCenterRamDTO);
					}
				}
			}
			else
			{
				dataCenterRamDAO.deleteChildrenByParent(data_centerDTO.iD, "data_center_id");
			}
			
		}					
		if(addFlag == true) //add or validate
		{
			if(dataCenterHardDiskDTOList != null)
			{				
				for(DataCenterHardDiskDTO dataCenterHardDiskDTO: dataCenterHardDiskDTOList)
				{
					dataCenterHardDiskDAO.add(dataCenterHardDiskDTO);
				}
			}
		
		}			
		else
		{
			List<Long> childIdsFromRequest = dataCenterHardDiskDAO.getChildIdsFromRequest(request, "dataCenterHardDisk");
			//delete the removed children
			dataCenterHardDiskDAO.deleteChildrenNotInList("data_center", "data_center_hard_disk", data_centerDTO.iD, childIdsFromRequest);
			List<Long> childIDsInDatabase = dataCenterHardDiskDAO.getChilIds("data_center", "data_center_hard_disk", data_centerDTO.iD);
			
			
			if(childIdsFromRequest != null)
			{
				for(int i = 0; i < childIdsFromRequest.size(); i ++)
				{
					Long childIDFromRequest = childIdsFromRequest.get(i);
					if(childIDsInDatabase.contains(childIDFromRequest))
					{
						DataCenterHardDiskDTO dataCenterHardDiskDTO =  createDataCenterHardDiskDTOByRequestAndIndex(request, false, i, language, data_centerDTO, userDTO);
						dataCenterHardDiskDAO.update(dataCenterHardDiskDTO);
					}
					else
					{
						DataCenterHardDiskDTO dataCenterHardDiskDTO =  createDataCenterHardDiskDTOByRequestAndIndex(request, true, i, language, data_centerDTO, userDTO);
						dataCenterHardDiskDAO.add(dataCenterHardDiskDTO);
					}
				}
			}
			else
			{
				dataCenterHardDiskDAO.deleteChildrenByParent(data_centerDTO.iD, "data_center_id");
			}
			
		}					
		if(addFlag == true) //add or validate
		{
			if(dataCenterUserDTOList != null)
			{				
				for(DataCenterUserDTO dataCenterUserDTO: dataCenterUserDTOList)
				{
					dataCenterUserDAO.add(dataCenterUserDTO);
				}
			}
		
		}			
		else
		{
			List<Long> childIdsFromRequest = dataCenterUserDAO.getChildIdsFromRequest(request, "dataCenterUser");
			//delete the removed children
			dataCenterUserDAO.deleteChildrenNotInList("data_center", "data_center_user", data_centerDTO.iD, childIdsFromRequest);
			List<Long> childIDsInDatabase = dataCenterUserDAO.getChilIds("data_center", "data_center_user", data_centerDTO.iD);
			
			
			if(childIdsFromRequest != null)
			{
				for(int i = 0; i < childIdsFromRequest.size(); i ++)
				{
					Long childIDFromRequest = childIdsFromRequest.get(i);
					if(childIDsInDatabase.contains(childIDFromRequest))
					{
						DataCenterUserDTO dataCenterUserDTO =  createDataCenterUserDTOByRequestAndIndex(request, false, i, language, data_centerDTO, userDTO);
						dataCenterUserDAO.update(dataCenterUserDTO);
					}
					else
					{
						DataCenterUserDTO dataCenterUserDTO =  createDataCenterUserDTOByRequestAndIndex(request, true, i, language, data_centerDTO, userDTO);
						dataCenterUserDAO.add(dataCenterUserDTO);
					}
				}
			}
			else
			{
				dataCenterUserDAO.deleteChildrenByParent(data_centerDTO.iD, "data_center_id");
			}
			
		}					
		return data_centerDTO;

	}
	private List<DataCenterRamDTO> createDataCenterRamDTOListByRequest(HttpServletRequest request, String language, Data_centerDTO data_centerDTO, UserDTO userDTO) throws Exception{ 
		List<DataCenterRamDTO> dataCenterRamDTOList = new ArrayList<DataCenterRamDTO>();
		if(request.getParameterValues("dataCenterRam.iD") != null) 
		{
			int dataCenterRamItemNo = request.getParameterValues("dataCenterRam.iD").length;
			
			
			for(int index=0;index<dataCenterRamItemNo;index++){
				DataCenterRamDTO dataCenterRamDTO = createDataCenterRamDTOByRequestAndIndex(request,true,index, language, data_centerDTO, userDTO);
				dataCenterRamDTOList.add(dataCenterRamDTO);
			}
			
			return dataCenterRamDTOList;
		}
		return null;
	}
	
	private DataCenterRamDTO createDataCenterRamDTOByRequestAndIndex(HttpServletRequest request, boolean addFlag, int index, String language, Data_centerDTO data_centerDTO, UserDTO userDTO) throws Exception{
	
		DataCenterRamDTO dataCenterRamDTO;
		if(addFlag == true )
		{
			dataCenterRamDTO = new DataCenterRamDTO();
		}
		else
		{
			dataCenterRamDTO = dataCenterRamDAO.getDTOByID(Long.parseLong(request.getParameterValues("dataCenterRam.iD")[index]));
		}
		String path = getServletContext().getRealPath("/img2/");
		SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
		
		dataCenterRamDTO.dataCenterId = data_centerDTO.iD; 

		String Value = "";

		Value = request.getParameterValues("dataCenterRam.ramCat")[index];

		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("ramCat = " + Value);
		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			dataCenterRamDTO.ramCat = Integer.parseInt(Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}

		Value = request.getParameterValues("dataCenterRam.ramSizeCat")[index];

		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("ramSizeCat = " + Value);
		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			dataCenterRamDTO.ramSizeCat = Integer.parseInt(Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}
		return dataCenterRamDTO;
	
	}	
	private List<DataCenterHardDiskDTO> createDataCenterHardDiskDTOListByRequest(HttpServletRequest request, String language, Data_centerDTO data_centerDTO, UserDTO userDTO) throws Exception{ 
		List<DataCenterHardDiskDTO> dataCenterHardDiskDTOList = new ArrayList<DataCenterHardDiskDTO>();
		if(request.getParameterValues("dataCenterHardDisk.iD") != null) 
		{
			int dataCenterHardDiskItemNo = request.getParameterValues("dataCenterHardDisk.iD").length;
			
			
			for(int index=0;index<dataCenterHardDiskItemNo;index++){
				DataCenterHardDiskDTO dataCenterHardDiskDTO = createDataCenterHardDiskDTOByRequestAndIndex(request,true,index, language, data_centerDTO, userDTO);
				dataCenterHardDiskDTOList.add(dataCenterHardDiskDTO);
			}
			
			return dataCenterHardDiskDTOList;
		}
		return null;
	}
	
	private DataCenterHardDiskDTO createDataCenterHardDiskDTOByRequestAndIndex(HttpServletRequest request, boolean addFlag, int index, String language, Data_centerDTO data_centerDTO, UserDTO userDTO) throws Exception{
	
		DataCenterHardDiskDTO dataCenterHardDiskDTO;
		if(addFlag == true )
		{
			dataCenterHardDiskDTO = new DataCenterHardDiskDTO();
		}
		else
		{
			dataCenterHardDiskDTO = dataCenterHardDiskDAO.getDTOByID(Long.parseLong(request.getParameterValues("dataCenterHardDisk.iD")[index]));
		}
		String path = getServletContext().getRealPath("/img2/");
		SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
		
		dataCenterHardDiskDTO.dataCenterId = data_centerDTO.iD; 

		String Value = "";

		Value = request.getParameterValues("dataCenterHardDisk.hdCat")[index];

		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("hdCat = " + Value);
		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			dataCenterHardDiskDTO.hdCat = Integer.parseInt(Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}

		Value = request.getParameterValues("dataCenterHardDisk.hdSizeCat")[index];

		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("hdSizeCat = " + Value);
		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			dataCenterHardDiskDTO.hdSizeCat = Integer.parseInt(Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}
		return dataCenterHardDiskDTO;
	
	}	
	private List<DataCenterUserDTO> createDataCenterUserDTOListByRequest(HttpServletRequest request, String language, Data_centerDTO data_centerDTO, UserDTO userDTO) throws Exception{ 
		List<DataCenterUserDTO> dataCenterUserDTOList = new ArrayList<DataCenterUserDTO>();
		if(request.getParameterValues("dataCenterUser.iD") != null) 
		{
			int dataCenterUserItemNo = request.getParameterValues("dataCenterUser.iD").length;
			
			
			for(int index=0;index<dataCenterUserItemNo;index++){
				DataCenterUserDTO dataCenterUserDTO = createDataCenterUserDTOByRequestAndIndex(request,true,index, language, data_centerDTO, userDTO);
				dataCenterUserDTOList.add(dataCenterUserDTO);
			}
			
			return dataCenterUserDTOList;
		}
		return null;
	}
	
	private DataCenterUserDTO createDataCenterUserDTOByRequestAndIndex(HttpServletRequest request, boolean addFlag, int index, String language, Data_centerDTO data_centerDTO, UserDTO userDTO) throws Exception{
	
		DataCenterUserDTO dataCenterUserDTO;
		if(addFlag == true )
		{
			dataCenterUserDTO = new DataCenterUserDTO();
		}
		else
		{
			dataCenterUserDTO = dataCenterUserDAO.getDTOByID(Long.parseLong(request.getParameterValues("dataCenterUser.iD")[index]));
		}
		String path = getServletContext().getRealPath("/img2/");
		SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
		
		dataCenterUserDTO.dataCenterId = data_centerDTO.iD; 

		String Value = "";

		Value = request.getParameterValues("dataCenterUser.userTypeCat")[index];

		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("userTypeCat = " + Value);
		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			dataCenterUserDTO.userTypeCat = Integer.parseInt(Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}

		Value = request.getParameterValues("dataCenterUser.userName")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("userName = " + Value);
		if(Value != null)
		{
			dataCenterUserDTO.userName = (Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}

		Value = request.getParameterValues("dataCenterUser.password")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("password = " + Value);
		if(Value != null)
		{
			dataCenterUserDTO.password = (Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}
		return dataCenterUserDTO;
	
	}	
}

