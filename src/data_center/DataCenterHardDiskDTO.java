package data_center;
import java.util.*; 
import util.*; 


public class DataCenterHardDiskDTO extends CommonDTO
{

	public long dataCenterId = -1;
	public int hdCat = -1;
	public int hdSizeCat = -1;
	
	public List<DataCenterRamDTO> dataCenterRamDTOList = new ArrayList<>();
	public List<DataCenterHardDiskDTO> dataCenterHardDiskDTOList = new ArrayList<>();
	public List<DataCenterUserDTO> dataCenterUserDTOList = new ArrayList<>();
	
    @Override
	public String toString() {
            return "$DataCenterHardDiskDTO[" +
            " iD = " + iD +
            " dataCenterId = " + dataCenterId +
            " hdCat = " + hdCat +
            " hdSizeCat = " + hdSizeCat +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}