package data_center;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import com.google.gson.Gson;

import repository.Repository;
import repository.RepositoryManager;


public class DataCenterHardDiskRepository implements Repository {
	DataCenterHardDiskDAO datacenterharddiskDAO = null;
	
	static Logger logger = Logger.getLogger(DataCenterHardDiskRepository.class);
	Map<Long, DataCenterHardDiskDTO>mapOfDataCenterHardDiskDTOToiD;
	Map<Long, Set<DataCenterHardDiskDTO> >mapOfDataCenterHardDiskDTOTodataCenterId;
	Gson gson;

  
	private DataCenterHardDiskRepository(){
		datacenterharddiskDAO = DataCenterHardDiskDAO.getInstance();
		mapOfDataCenterHardDiskDTOToiD = new ConcurrentHashMap<>();
		mapOfDataCenterHardDiskDTOTodataCenterId = new ConcurrentHashMap<>();
		gson = new Gson();
		RepositoryManager.getInstance().addRepository(this);
	}

	private static class LazyLoader{
        final static DataCenterHardDiskRepository INSTANCE = new DataCenterHardDiskRepository();
    }

    public static DataCenterHardDiskRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

	public void reload(boolean reloadAll){
		try {
			List<DataCenterHardDiskDTO> datacenterharddiskDTOs = datacenterharddiskDAO.getAllDTOs(reloadAll);
			for(DataCenterHardDiskDTO datacenterharddiskDTO : datacenterharddiskDTOs) {
				DataCenterHardDiskDTO oldDataCenterHardDiskDTO = getDataCenterHardDiskDTOByiD(datacenterharddiskDTO.iD);
				if( oldDataCenterHardDiskDTO != null ) {
					mapOfDataCenterHardDiskDTOToiD.remove(oldDataCenterHardDiskDTO.iD);
				
					if(mapOfDataCenterHardDiskDTOTodataCenterId.containsKey(oldDataCenterHardDiskDTO.dataCenterId)) {
						mapOfDataCenterHardDiskDTOTodataCenterId.get(oldDataCenterHardDiskDTO.dataCenterId).remove(oldDataCenterHardDiskDTO);
					}
					if(mapOfDataCenterHardDiskDTOTodataCenterId.get(oldDataCenterHardDiskDTO.dataCenterId).isEmpty()) {
						mapOfDataCenterHardDiskDTOTodataCenterId.remove(oldDataCenterHardDiskDTO.dataCenterId);
					}
					
					
				}
				if(datacenterharddiskDTO.isDeleted == 0) 
				{
					
					mapOfDataCenterHardDiskDTOToiD.put(datacenterharddiskDTO.iD, datacenterharddiskDTO);
				
					if( ! mapOfDataCenterHardDiskDTOTodataCenterId.containsKey(datacenterharddiskDTO.dataCenterId)) {
						mapOfDataCenterHardDiskDTOTodataCenterId.put(datacenterharddiskDTO.dataCenterId, new HashSet<>());
					}
					mapOfDataCenterHardDiskDTOTodataCenterId.get(datacenterharddiskDTO.dataCenterId).add(datacenterharddiskDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public DataCenterHardDiskDTO clone(DataCenterHardDiskDTO dto) {
		String raw = gson.toJson(dto);
		return gson.fromJson(raw, DataCenterHardDiskDTO.class);
	}
	
	
	public List<DataCenterHardDiskDTO> getDataCenterHardDiskList() {
		List <DataCenterHardDiskDTO> datacenterharddisks = new ArrayList<DataCenterHardDiskDTO>(this.mapOfDataCenterHardDiskDTOToiD.values());
		return datacenterharddisks;
	}
	
	public List<DataCenterHardDiskDTO> copyDataCenterHardDiskList() {
		List <DataCenterHardDiskDTO> datacenterharddisks = getDataCenterHardDiskList();
		return datacenterharddisks
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}
	
	
	
	public DataCenterHardDiskDTO getDataCenterHardDiskDTOByiD( long iD){
		return mapOfDataCenterHardDiskDTOToiD.get(iD);
	}
	
	public DataCenterHardDiskDTO copyDataCenterHardDiskDTOByiD( long iD){
		return clone(mapOfDataCenterHardDiskDTOToiD.get(iD));
	}
	
	
	public List<DataCenterHardDiskDTO> getDataCenterHardDiskDTOBydataCenterId(long dataCenterId) {
		return new ArrayList<>( mapOfDataCenterHardDiskDTOTodataCenterId.getOrDefault(dataCenterId,new HashSet<>()));
	}
	
	public List<DataCenterHardDiskDTO> copyDataCenterHardDiskDTOBydataCenterId(long dataCenterId)
	{
		List <DataCenterHardDiskDTO> datacenterharddisks = getDataCenterHardDiskDTOBydataCenterId(dataCenterId);
		return datacenterharddisks
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}

	
	@Override
	public String getTableName() {
		return datacenterharddiskDAO.getTableName();
	}
}


