package prescription_report;


import java.io.IOException;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import language.LC;
import language.LM;

import login.LoginDTO;
import permission.MenuConstants;

import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;

import util.ReportRequestHandler;
import pbReport.*;






@WebServlet("/Prescription_report_Servlet")
public class Prescription_report_Servlet  extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger(getClass());

	String[][] Criteria =
	{
		{"criteria","","dr_employee_record_id","=","","String","","","any","dr_user_name", LC.PRESCRIPTION_REPORT_WHERE_DOCTORID + "", "userNameToEmployeeRecordId"},		
		{"criteria","appointment","employee_record_id","=","AND","int","","","any","employee_user_name", LC.PRESCRIPTION_REPORT_WHERE_EMPLOYEEID + "", "userNameToEmployeeRecordId"},		
		{"criteria","","name","like","AND","String","","","%","name", LC.PRESCRIPTION_REPORT_WHERE_NAME + ""},		
		{"criteria","","phone","like","AND","String","","","%","phone", LC.PRESCRIPTION_REPORT_WHERE_PHONE + ""},		
		{"criteria","appointment","visit_date",">=","AND","long","","",Long.MIN_VALUE + "","startDate", LC.HM_START_DATE + ""},		
		{"criteria","appointment","visit_date","<=","AND","long","","",Long.MAX_VALUE + "","endDate", LC.HM_END_DATE + ""}		
	};
	
	String[][] Display =
	{
		{"display","","appointment.visit_date","date",""},		
		{"display","","visit_time","time",""},		
		{"display","","dr_employee_record_id","erIdToName",""},		
		{"display","","appointment.employee_record_id","erIdToName",""},		
		{"display","","name","text",""},		
		{"display","","phone","text",""},		
		{"display","","prescription_details.id","invisible",""}		
	};
	
	String GroupBy = "";
	String OrderBY = "visit_time desc";
	
	ReportRequestHandler reportRequestHandler;
	
	public Prescription_report_Servlet(){

	}
	
	private final ReportService reportService = new ReportService();
	
	private String sql;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String language = "english";
		if(userDTO!= null && userDTO.languageID == SessionConstants.BANGLA)
		{
			language = "bangla";
		}
				
		String actionType = request.getParameter("actionType");

		System.out.println("In ssservlet doget println, employee_user_name inplemented, actiontype = " + actionType);
		logger.debug("In ssservlet doget logger, employee_user_name inplemented, actiontype = " + actionType);
		
		sql = "prescription_details join appointment on appointment.id = prescription_details.appointment_id";

		Display[0][4] = LM.getText(LC.HM_DATE, loginDTO);
		Display[1][4] = LM.getText(LC.HM_TIME, loginDTO);
		Display[2][4] = LM.getText(LC.HM_DOCTOR, loginDTO);
		Display[3][4] = LM.getText(LC.HM_EMPLOYEE_ID, loginDTO);
		Display[4][4] = LM.getText(LC.HM_NAME, loginDTO);
		Display[5][4] = LM.getText(LC.HM_PHONE, loginDTO);
		Display[6][4] = LM.getText(LC.PRESCRIPTION_REPORT_SELECT_ID, loginDTO);

		
		String reportName = LM.getText(LC.PRESCRIPTION_REPORT_OTHER_PRESCRIPTION_REPORT, loginDTO);
		
		reportRequestHandler = new ReportRequestHandler(null,
				Criteria, Display, GroupBy, OrderBY, sql,
				reportService);

		
		reportRequestHandler.handleReportGet(request, response, userDTO, "prescription_report",
				MenuConstants.PRESCRIPTION_REPORT_DETAILS, language, reportName, "prescription_report");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		doGet(request, response);
	}
}
