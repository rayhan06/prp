/*
package office_transfer_logs;

import java.io.IOException;
import java.io.*;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import org.apache.log4j.Logger;

import login.LoginDTO;
import permission.MenuConstants;
import role.PermissionRepository;


import sessionmanager.SessionConstants;

import user.UserDTO;
import user.UserRepository;

import util.RecordNavigationManager3;

import javax.servlet.http.*;
import java.util.UUID;

import approval_module_map.*;


import com.google.gson.Gson;

import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;

*/
/**
 * Servlet implementation class Office_transfer_logsServlet
 *//*

@WebServlet("/Office_transfer_logsServlet")
@MultipartConfig
public class Office_transfer_logsServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Office_transfer_logsServlet.class);
    Approval_module_mapDAO approval_module_mapDAO = new Approval_module_mapDAO();
    Approval_module_mapDTO approval_module_mapDTO;
    String tableName = "office_transfer_logs";
    String tempTableName = "office_transfer_logs_temp";
    Office_transfer_logsDAO office_transfer_logsDAO;
    private Gson gson = new Gson();

    */
/**
     * @see HttpServlet#HttpServlet()
     *//*

    public Office_transfer_logsServlet() {
        super();
        try {
            approval_module_mapDTO = approval_module_mapDAO.getApproval_module_mapDTOByTableName("office_transfer_logs");
            office_transfer_logsDAO = new Office_transfer_logsDAO(tableName, tempTableName, approval_module_mapDTO);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    */
/**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     *//*

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("In doget request = " + request);
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getInstance().getUserDtoByUserId(loginDTO.userID);
        boolean isPermanentTable = true;
        if (request.getParameter("isPermanentTable") != null) {
            isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
        }
        System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
        try {
            String actionType = request.getParameter("actionType");
            if (actionType.equals("getAddPage")) {
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.OFFICE_TRANSFER_LOGS_ADD)) {
                    getAddPage(request, response);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else if (actionType.equals("getEditPage")) {
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.OFFICE_TRANSFER_LOGS_UPDATE)) {
                    if (isPermanentTable) {
                        getOffice_transfer_logs(request, response, tableName);
                    } else {
                        getOffice_transfer_logs(request, response, tempTableName);
                    }
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else if (actionType.equals("getURL")) {
                String URL = request.getParameter("URL");
                System.out.println("URL = " + URL);
                response.sendRedirect(URL);
            } else if (actionType.equals("search")) {
                System.out.println("search requested");
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.OFFICE_TRANSFER_LOGS_SEARCH)) {

                    if (isPermanentTable) {
                        searchOffice_transfer_logs(request, response, tableName, isPermanentTable);
                    } else {
                        searchOffice_transfer_logs(request, response, tempTableName, isPermanentTable);
                    }
                }
            } else if (actionType.equals("getApprovalPage")) {
                System.out.println("getApprovalPage requested");
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.OFFICE_TRANSFER_LOGS_SEARCH)) {
                    searchOffice_transfer_logs(request, response, tempTableName, false);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else {
                request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.debug(ex);
        }
    }

    private void getAddPage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("ID", -1L);
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("office_transfer_logs/office_transfer_logsEdit.jsp");
        requestDispatcher.forward(request, response);
    }

    private String getFileName(final Part part) {
        final String partHeader = part.getHeader("content-disposition");
        System.out.println("Part Header = {0}" + partHeader);
        for (String content : part.getHeader("content-disposition").split(";")) {
            if (content.trim().startsWith("filename")) {
                return content.substring(
                        content.indexOf('=') + 1).trim().replace("\"", "");
            }
        }
        return null;
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getInstance().getUserDtoByUserId(loginDTO.userID);
        System.out.println("doPost");
        boolean isPermanentTable = true;
        if (request.getParameter("isPermanentTable") != null) {
            isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
        }
        System.out.println("In servlet, isPermanentTable = " + isPermanentTable);

        try {
            String actionType = request.getParameter("actionType");
            System.out.println("actionType = " + actionType);
            if (actionType.equals("add")) {

                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.OFFICE_TRANSFER_LOGS_ADD)) {
                    System.out.println("going to  addOffice_transfer_logs ");
                    addOffice_transfer_logs(request, response, true, userDTO, tableName, true);
                } else {
                    System.out.println("Not going to  addOffice_transfer_logs ");
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }

            }
            if (actionType.equals("approve")) {

                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.OFFICE_TRANSFER_LOGS_ADD)) {
                    approveOffice_transfer_logs(request, response, true, userDTO);
                } else {
                    System.out.println("Not going to  addOffice_transfer_logs ");
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }

            }
            if (actionType.equals("getDTO")) {

                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.OFFICE_TRANSFER_LOGS_ADD)) {
                    if (isPermanentTable) {
                        getDTO(request, response, tableName);
                    } else {
                        getDTO(request, response, tempTableName);
                    }
                } else {
                    System.out.println("Not going to  addOffice_transfer_logs ");
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }

            } else if (actionType.equals("edit")) {

                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.OFFICE_TRANSFER_LOGS_UPDATE)) {
                    if (isPermanentTable) {
                        addOffice_transfer_logs(request, response, false, userDTO, tableName, isPermanentTable);
                    } else {
                        addOffice_transfer_logs(request, response, false, userDTO, tempTableName, isPermanentTable);
                    }
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else if (actionType.equals("delete")) {
                deleteOffice_transfer_logs(request, response, userDTO, isPermanentTable);
            } else if (actionType.equals("search")) {
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.OFFICE_TRANSFER_LOGS_SEARCH)) {
                    searchOffice_transfer_logs(request, response, tableName, true);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else if (actionType.equals("getGeo")) {
                System.out.println("going to geoloc ");
                request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            logger.debug(ex);
        }
    }

    private void getDTO(HttpServletRequest request, HttpServletResponse response, String tableName) {
        try {
            System.out.println("In getDTO");
            Office_transfer_logsDTO office_transfer_logsDTO = (Office_transfer_logsDTO) office_transfer_logsDAO.getDTOByID(Long.parseLong(request.getParameter("ID")), tableName);
            PrintWriter out = response.getWriter();
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");

            String encoded = this.gson.toJson(office_transfer_logsDTO);
            System.out.println("json encoded data = " + encoded);
            out.print(encoded);
            out.flush();
        } catch (NumberFormatException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    private void approveOffice_transfer_logs(HttpServletRequest request, HttpServletResponse response, boolean b, UserDTO userDTO) {
        try {
            long id = Long.parseLong(request.getParameter("idToApprove"));
            Office_transfer_logsDTO office_transfer_logsDTO = (Office_transfer_logsDTO) office_transfer_logsDAO.getDTOByID(id, tempTableName);
            office_transfer_logsDAO.manageWriteOperations(office_transfer_logsDTO, SessionConstants.APPROVE, id, userDTO);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void addOffice_transfer_logs(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, String tableName, boolean isPermanentTable) throws IOException {
        // TODO Auto-generated method stub
        try {
            request.setAttribute("failureMessage", "");
            System.out.println("%%%% addOffice_transfer_logs");
            String path = getServletContext().getRealPath("/img2/");
            Office_transfer_logsDTO office_transfer_logsDTO;
            String FileNamePrefix;
            if (addFlag == true) {
                office_transfer_logsDTO = new Office_transfer_logsDTO();
                FileNamePrefix = UUID.randomUUID().toString().substring(0, 10);
            } else {
                office_transfer_logsDTO = (Office_transfer_logsDTO) office_transfer_logsDAO.getDTOByID(Long.parseLong(request.getParameter("identity")), tableName);
                FileNamePrefix = request.getParameter("identity");
            }

            String Value = "";
            Value = request.getParameter("toOfficeId");
            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("toOfficeId = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                office_transfer_logsDTO.toOfficeId = Long.parseLong(Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }
            Value = request.getParameter("toOfficeNameEn");
            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("toOfficeNameEn = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                office_transfer_logsDTO.toOfficeNameEn = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }
            Value = request.getParameter("toOfficeNameBn");
            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("toOfficeNameBn = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                office_transfer_logsDTO.toOfficeNameBn = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }
            Value = request.getParameter("toUnitId");
            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("toUnitId = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                office_transfer_logsDTO.toUnitId = Long.parseLong(Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }
            Value = request.getParameter("toUnitNameEn");
            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("toUnitNameEn = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                office_transfer_logsDTO.toUnitNameEn = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }
            Value = request.getParameter("toUnitNameBn");
            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("toUnitNameBn = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                office_transfer_logsDTO.toUnitNameBn = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }
            Value = request.getParameter("toOrganogramId");
            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("toOrganogramId = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                office_transfer_logsDTO.toOrganogramId = Long.parseLong(Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }
            Value = request.getParameter("toOrganogramNameEn");
            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("toOrganogramNameEn = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                office_transfer_logsDTO.toOrganogramNameEn = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }
            Value = request.getParameter("toOrganogramNameBn");
            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("toOrganogramNameBn = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                office_transfer_logsDTO.toOrganogramNameBn = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }
            Value = request.getParameter("fromOfficeId");
            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("fromOfficeId = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                office_transfer_logsDTO.fromOfficeId = Long.parseLong(Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }
            Value = request.getParameter("fromOfficeNameEn");
            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("fromOfficeNameEn = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                office_transfer_logsDTO.fromOfficeNameEn = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }
            Value = request.getParameter("fromOfficeNameBn");
            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("fromOfficeNameBn = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                office_transfer_logsDTO.fromOfficeNameBn = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }
            Value = request.getParameter("fromUnitId");
            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("fromUnitId = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                office_transfer_logsDTO.fromUnitId = Long.parseLong(Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }
            Value = request.getParameter("fromUnitNameEn");
            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("fromUnitNameEn = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                office_transfer_logsDTO.fromUnitNameEn = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }
            Value = request.getParameter("fromUnitNameBn");
            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("fromUnitNameBn = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                office_transfer_logsDTO.fromUnitNameBn = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }
            Value = request.getParameter("fromOrganogramId");
            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("fromOrganogramId = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                office_transfer_logsDTO.fromOrganogramId = Long.parseLong(Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }
            Value = request.getParameter("fromOrganogramNameEn");
            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("fromOrganogramNameEn = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                office_transfer_logsDTO.fromOrganogramNameEn = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }
            Value = request.getParameter("fromOrganogramNameBn");
            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("fromOrganogramNameBn = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                office_transfer_logsDTO.fromOrganogramNameBn = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }
            Value = request.getParameter("employeeRecordId");
            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("employeeRecordId = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                office_transfer_logsDTO.employeeRecordId = Long.parseLong(Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }
            Value = request.getParameter("employeeNameEn");
            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("employeeNameEn = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                office_transfer_logsDTO.employeeNameEn = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }
            Value = request.getParameter("employeeNameBn");
            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("employeeNameBn = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                office_transfer_logsDTO.employeeNameBn = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            System.out.println("Done adding  addOffice_transfer_logs dto = " + office_transfer_logsDTO);

            if (addFlag == true) {
                office_transfer_logsDAO.manageWriteOperations(office_transfer_logsDTO, SessionConstants.INSERT, -1, userDTO);
            } else {
                if (isPermanentTable) {
                    office_transfer_logsDAO.manageWriteOperations(office_transfer_logsDTO, SessionConstants.UPDATE, -1, userDTO);
                } else {
                    office_transfer_logsDAO.manageWriteOperations(office_transfer_logsDTO, SessionConstants.VALIDATE, -1, userDTO);
                }

            }


            String inPlaceSubmit = (String) request.getParameter("inplacesubmit");

            if (inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase("")) {
                getOffice_transfer_logs(request, response, tableName);
            } else {
                response.sendRedirect("Office_transfer_logsServlet?actionType=search");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void deleteOffice_transfer_logs(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO, boolean deleteOrReject) throws IOException {
        try {
            String[] IDsToDelete = request.getParameterValues("ID");
            for (int i = 0; i < IDsToDelete.length; i++) {
                long id = Long.parseLong(IDsToDelete[i]);
                System.out.println("------ DELETING " + IDsToDelete[i]);

                if (deleteOrReject) {
                    Office_transfer_logsDTO office_transfer_logsDTO = (Office_transfer_logsDTO) office_transfer_logsDAO.getDTOByID(id);
                    office_transfer_logsDAO.manageWriteOperations(office_transfer_logsDTO, SessionConstants.DELETE, id, userDTO);
                    response.sendRedirect("Office_transfer_logsServlet?actionType=search");
                } else {
                    Office_transfer_logsDTO office_transfer_logsDTO = (Office_transfer_logsDTO) office_transfer_logsDAO.getDTOByID(id, tempTableName);
                    office_transfer_logsDAO.manageWriteOperations(office_transfer_logsDTO, SessionConstants.REJECT, id, userDTO);
                    response.sendRedirect("Office_transfer_logsServlet?actionType=getApprovalPage");
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    private void getOffice_transfer_logs(HttpServletRequest request, HttpServletResponse response, String tableName) throws ServletException, IOException {
        System.out.println("in getOffice_transfer_logs");
        Office_transfer_logsDTO office_transfer_logsDTO = null;
        try {
            office_transfer_logsDTO = (Office_transfer_logsDTO) office_transfer_logsDAO.getDTOByID(Long.parseLong(request.getParameter("ID")), tableName);
            request.setAttribute("ID", office_transfer_logsDTO.iD);
            request.setAttribute("office_transfer_logsDTO", office_transfer_logsDTO);
            request.setAttribute("office_transfer_logsDAO", office_transfer_logsDAO);

            String URL = "";

            String inPlaceEdit = (String) request.getParameter("inplaceedit");
            String inPlaceSubmit = (String) request.getParameter("inplacesubmit");
            String getBodyOnly = (String) request.getParameter("getBodyOnly");

            if (inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase("")) {
                URL = "office_transfer_logs/office_transfer_logsInPlaceEdit.jsp";
                request.setAttribute("inplaceedit", "");
            } else if (inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase("")) {
                URL = "office_transfer_logs/office_transfer_logsSearchRow.jsp";
                request.setAttribute("inplacesubmit", "");
            } else {
                if (getBodyOnly != null && !getBodyOnly.equalsIgnoreCase("")) {
                    URL = "office_transfer_logs/office_transfer_logsEditBody.jsp?actionType=edit";
                } else {
                    URL = "office_transfer_logs/office_transfer_logsEdit.jsp?actionType=edit";
                }
            }

            RequestDispatcher rd = request.getRequestDispatcher(URL);
            rd.forward(request, response);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void searchOffice_transfer_logs(HttpServletRequest request, HttpServletResponse response, String tableName, boolean isPermanent) throws ServletException, IOException {
        System.out.println("in  searchOffice_transfer_logs 1");
        LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getInstance().getUserDtoByUserId(loginDTO.userID);
        String ajax = (String) request.getParameter("ajax");
        boolean hasAjax = false;
        if (ajax != null && !ajax.equalsIgnoreCase("")) {
            hasAjax = true;
        }
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);

        RecordNavigationManager3 rnManager = new RecordNavigationManager3(
                SessionConstants.NAV_OFFICE_TRANSFER_LOGS,
                request,
                office_transfer_logsDAO,
                SessionConstants.VIEW_OFFICE_TRANSFER_LOGS,
                SessionConstants.SEARCH_OFFICE_TRANSFER_LOGS,
                tableName,
                isPermanent,
                userDTO.approvalPathID);
        try {
            System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        } catch (Exception e) {
            System.out.println("failed to dojob" + e);
        }

        request.setAttribute("office_transfer_logsDAO", office_transfer_logsDAO);
        RequestDispatcher rd;
        if (hasAjax == false) {
            System.out.println("Going to office_transfer_logs/office_transfer_logsSearch.jsp");
            rd = request.getRequestDispatcher("office_transfer_logs/office_transfer_logsSearch.jsp");
        } else {
            System.out.println("Going to office_transfer_logs/office_transfer_logsSearchForm.jsp");
            rd = request.getRequestDispatcher("office_transfer_logs/office_transfer_logsSearchForm.jsp");
        }
        rd.forward(request, response);
    }
}

*/
