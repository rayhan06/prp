package office_transfer_logs;
import java.util.*; 
import util.*; 


public class Office_transfer_logsDTO extends CommonDTO
{

	public long id = 0;

	public long toOfficeId = 0;
    public String toOfficeNameEn = "";
    public String toOfficeNameBn = "";
	public long toUnitId = 0;
    public String toUnitNameEn = "";
    public String toUnitNameBn = "";
	public long toOrganogramId = 0;
    public String toOrganogramNameEn = "";
    public String toOrganogramNameBn = "";

	public long fromOfficeId = 0;
    public String fromOfficeNameEn = "";
    public String fromOfficeNameBn = "";
	public long fromUnitId = 0;
    public String fromUnitNameEn = "";
    public String fromUnitNameBn = "";
	public long fromOrganogramId = 0;
    public String fromOrganogramNameEn = "";
    public String fromOrganogramNameBn = "";
	public long employeeRecordId = 0;
    public String employeeNameEn = "";
    public String employeeNameBn = "";
	
	
    @Override
	public String toString() {
            return "$Office_transfer_logsDTO[" +
            " id = " + id +
            " toOfficeId = " + toOfficeId +
            " toOfficeNameEn = " + toOfficeNameEn +
            " toOfficeNameBn = " + toOfficeNameBn +
            " toUnitId = " + toUnitId +
            " toUnitNameEn = " + toUnitNameEn +
            " toUnitNameBn = " + toUnitNameBn +
            " toOrganogramId = " + toOrganogramId +
            " toOrganogramNameEn = " + toOrganogramNameEn +
            " toOrganogramNameBn = " + toOrganogramNameBn +
            " fromOfficeId = " + fromOfficeId +
            " fromOfficeNameEn = " + fromOfficeNameEn +
            " fromOfficeNameBn = " + fromOfficeNameBn +
            " fromUnitId = " + fromUnitId +
            " fromUnitNameEn = " + fromUnitNameEn +
            " fromUnitNameBn = " + fromUnitNameBn +
            " fromOrganogramId = " + fromOrganogramId +
            " fromOrganogramNameEn = " + fromOrganogramNameEn +
            " fromOrganogramNameBn = " + fromOrganogramNameBn +
            " employeeRecordId = " + employeeRecordId +
            " employeeNameEn = " + employeeNameEn +
            " employeeNameBn = " + employeeNameBn +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}