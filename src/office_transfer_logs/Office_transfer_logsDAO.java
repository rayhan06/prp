package office_transfer_logs;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;


import org.apache.log4j.Logger;

import dbm.*;

import repository.RepositoryManager;

import util.*;
import pb.*;
import approval_module_map.*;

public class Office_transfer_logsDAO extends NavigationService3 {

    Logger logger = Logger.getLogger(getClass());

    public Office_transfer_logsDAO(String tableName, String tempTableName, Approval_module_mapDTO approval_module_mapDTO) {
        super(tableName, tempTableName, approval_module_mapDTO);
    }

    public long add(Office_transfer_logsDTO office_transfer_logsDTO, String tableName, TempTableDTO tempTableDTO) throws Exception {

        Connection connection = null;
        PreparedStatement ps = null;

        long lastModificationTime = System.currentTimeMillis();

        try {
            connection = DBMW.getInstance().getConnection();

            if (connection == null) {
                System.out.println("nullconn");
            }

            office_transfer_logsDTO.iD = DBMW.getInstance().getNextSequenceId(tableName);

            String sql = "INSERT INTO " + tableName;

            sql += " (";
            sql += "id";
            sql += ", ";
            sql += "to_office_id";
            sql += ", ";
            sql += "to_office_name_en";
            sql += ", ";
            sql += "to_office_name_bn";
            sql += ", ";
            sql += "to_unit_id";
            sql += ", ";
            sql += "to_unit_name_en";
            sql += ", ";
            sql += "to_unit_name_bn";
            sql += ", ";
            sql += "to_organogram_id";
            sql += ", ";
            sql += "to_organogram_name_en";
            sql += ", ";
            sql += "to_organogram_name_bn";
            sql += ", ";
            sql += "from_office_id";
            sql += ", ";
            sql += "from_office_name_en";
            sql += ", ";
            sql += "from_office_name_bn";
            sql += ", ";
            sql += "from_unit_id";
            sql += ", ";
            sql += "from_unit_name_en";
            sql += ", ";
            sql += "from_unit_name_bn";
            sql += ", ";
            sql += "from_organogram_id";
            sql += ", ";
            sql += "from_organogram_name_en";
            sql += ", ";
            sql += "from_organogram_name_bn";
            sql += ", ";
            sql += "employee_record_id";
            sql += ", ";
            sql += "employee_name_en";
            sql += ", ";
            sql += "employee_name_bn";
            sql += ", ";
            sql += "isDeleted";
            sql += ", ";
            sql += "lastModificationTime";
            if (tempTableDTO != null) {
                sql += ", permanent_table_id, operation_type, approval_path_type, approval_order";
            }
            sql += ")";


            sql += " VALUES(";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            if (tempTableDTO != null) {
                sql += ", " + tempTableDTO.permanent_table_id + ", " + tempTableDTO.operation_type + ", " + tempTableDTO.approval_path_type + ", " + tempTableDTO.approval_order;
            }
            sql += ")";


            ps = connection.prepareStatement(sql);


            int index = 1;

            ps.setObject(index++, office_transfer_logsDTO.iD);
            ps.setObject(index++, office_transfer_logsDTO.toOfficeId);
            ps.setObject(index++, office_transfer_logsDTO.toOfficeNameEn);
            ps.setObject(index++, office_transfer_logsDTO.toOfficeNameBn);
            ps.setObject(index++, office_transfer_logsDTO.toUnitId);
            ps.setObject(index++, office_transfer_logsDTO.toUnitNameEn);
            ps.setObject(index++, office_transfer_logsDTO.toUnitNameBn);
            ps.setObject(index++, office_transfer_logsDTO.toOrganogramId);
            ps.setObject(index++, office_transfer_logsDTO.toOrganogramNameEn);
            ps.setObject(index++, office_transfer_logsDTO.toOrganogramNameBn);
            ps.setObject(index++, office_transfer_logsDTO.fromOfficeId);
            ps.setObject(index++, office_transfer_logsDTO.fromOfficeNameEn);
            ps.setObject(index++, office_transfer_logsDTO.fromOfficeNameBn);
            ps.setObject(index++, office_transfer_logsDTO.fromUnitId);
            ps.setObject(index++, office_transfer_logsDTO.fromUnitNameEn);
            ps.setObject(index++, office_transfer_logsDTO.fromUnitNameBn);
            ps.setObject(index++, office_transfer_logsDTO.fromOrganogramId);
            ps.setObject(index++, office_transfer_logsDTO.fromOrganogramNameEn);
            ps.setObject(index++, office_transfer_logsDTO.fromOrganogramNameBn);
            ps.setObject(index++, office_transfer_logsDTO.employeeRecordId);
            ps.setObject(index++, office_transfer_logsDTO.employeeNameEn);
            ps.setObject(index++, office_transfer_logsDTO.employeeNameBn);
            ps.setObject(index++, office_transfer_logsDTO.isDeleted);
            ps.setObject(index++, lastModificationTime);

            System.out.println(ps);
            ps.execute();


            recordUpdateTime(connection, lastModificationTime, tableName);

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (connection != null) {
                    DBMW.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
        return office_transfer_logsDTO.id;
    }

    //need another getter for repository
    public CommonDTO getDTOByID(long ID, String tableName) throws Exception {
        Connection connection = null;
        ResultSet rs = null;
        Statement stmt = null;
        Office_transfer_logsDTO office_transfer_logsDTO = null;
        try {

            String sql = "SELECT * ";

            sql += " FROM " + tableName;

            sql += " WHERE id=" + ID;

            printSql(sql);

            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();


            rs = stmt.executeQuery(sql);

            if (rs.next()) {
                office_transfer_logsDTO = new Office_transfer_logsDTO();

                office_transfer_logsDTO.id = rs.getLong("id");
                office_transfer_logsDTO.toOfficeId = rs.getLong("to_office_id");
                office_transfer_logsDTO.toOfficeNameEn = rs.getString("to_office_name_en");
                office_transfer_logsDTO.toOfficeNameBn = rs.getString("to_office_name_bn");
                office_transfer_logsDTO.toUnitId = rs.getLong("to_unit_id");
                office_transfer_logsDTO.toUnitNameEn = rs.getString("to_unit_name_en");
                office_transfer_logsDTO.toUnitNameBn = rs.getString("to_unit_name_bn");
                office_transfer_logsDTO.toOrganogramId = rs.getLong("to_organogram_id");
                office_transfer_logsDTO.toOrganogramNameEn = rs.getString("to_organogram_name_en");
                office_transfer_logsDTO.toOrganogramNameBn = rs.getString("to_organogram_name_bn");
                office_transfer_logsDTO.fromOfficeId = rs.getLong("from_office_id");
                office_transfer_logsDTO.fromOfficeNameEn = rs.getString("from_office_name_en");
                office_transfer_logsDTO.fromOfficeNameBn = rs.getString("from_office_name_bn");
                office_transfer_logsDTO.fromUnitId = rs.getLong("from_unit_id");
                office_transfer_logsDTO.fromUnitNameEn = rs.getString("from_unit_name_en");
                office_transfer_logsDTO.fromUnitNameBn = rs.getString("from_unit_name_bn");
                office_transfer_logsDTO.fromOrganogramId = rs.getLong("from_organogram_id");
                office_transfer_logsDTO.fromOrganogramNameEn = rs.getString("from_organogram_name_en");
                office_transfer_logsDTO.fromOrganogramNameBn = rs.getString("from_organogram_name_bn");
                office_transfer_logsDTO.employeeRecordId = rs.getLong("employee_record_id");
                office_transfer_logsDTO.employeeNameEn = rs.getString("employee_name_en");
                office_transfer_logsDTO.employeeNameBn = rs.getString("employee_name_bn");
                office_transfer_logsDTO.isDeleted = rs.getInt("isDeleted");
                office_transfer_logsDTO.lastModificationTime = rs.getLong("lastModificationTime");

            }


        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }

            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
        return office_transfer_logsDTO;
    }

    public long update(CommonDTO commonDTO, String tableName) throws Exception {
        Office_transfer_logsDTO office_transfer_logsDTO = (Office_transfer_logsDTO) commonDTO;

        Connection connection = null;
        PreparedStatement ps = null;

        long lastModificationTime = System.currentTimeMillis();
        try {
            connection = DBMW.getInstance().getConnection();

            String sql = "UPDATE " + tableName;

            sql += " SET ";
            sql += "to_office_id=?";
            sql += ", ";
            sql += "to_office_name_en=?";
            sql += ", ";
            sql += "to_office_name_bn=?";
            sql += ", ";
            sql += "to_unit_id=?";
            sql += ", ";
            sql += "to_unit_name_en=?";
            sql += ", ";
            sql += "to_unit_name_bn=?";
            sql += ", ";
            sql += "to_organogram_id=?";
            sql += ", ";
            sql += "to_organogram_name_en=?";
            sql += ", ";
            sql += "to_organogram_name_bn=?";
            sql += ", ";
            sql += "from_office_id=?";
            sql += ", ";
            sql += "from_office_name_en=?";
            sql += ", ";
            sql += "from_office_name_bn=?";
            sql += ", ";
            sql += "from_unit_id=?";
            sql += ", ";
            sql += "from_unit_name_en=?";
            sql += ", ";
            sql += "from_unit_name_bn=?";
            sql += ", ";
            sql += "from_organogram_id=?";
            sql += ", ";
            sql += "from_organogram_name_en=?";
            sql += ", ";
            sql += "from_organogram_name_bn=?";
            sql += ", ";
            sql += "employee_record_id=?";
            sql += ", ";
            sql += "employee_name_en=?";
            sql += ", ";
            sql += "employee_name_bn=?";
            sql += ", lastModificationTime = " + lastModificationTime + "";
            sql += " WHERE id = " + office_transfer_logsDTO.id;


            ps = connection.prepareStatement(sql);


            int index = 1;

            ps.setObject(index++, office_transfer_logsDTO.toOfficeId);
            ps.setObject(index++, office_transfer_logsDTO.toOfficeNameEn);
            ps.setObject(index++, office_transfer_logsDTO.toOfficeNameBn);
            ps.setObject(index++, office_transfer_logsDTO.toUnitId);
            ps.setObject(index++, office_transfer_logsDTO.toUnitNameEn);
            ps.setObject(index++, office_transfer_logsDTO.toUnitNameBn);
            ps.setObject(index++, office_transfer_logsDTO.toOrganogramId);
            ps.setObject(index++, office_transfer_logsDTO.toOrganogramNameEn);
            ps.setObject(index++, office_transfer_logsDTO.toOrganogramNameBn);
            ps.setObject(index++, office_transfer_logsDTO.fromOfficeId);
            ps.setObject(index++, office_transfer_logsDTO.fromOfficeNameEn);
            ps.setObject(index++, office_transfer_logsDTO.fromOfficeNameBn);
            ps.setObject(index++, office_transfer_logsDTO.fromUnitId);
            ps.setObject(index++, office_transfer_logsDTO.fromUnitNameEn);
            ps.setObject(index++, office_transfer_logsDTO.fromUnitNameBn);
            ps.setObject(index++, office_transfer_logsDTO.fromOrganogramId);
            ps.setObject(index++, office_transfer_logsDTO.fromOrganogramNameEn);
            ps.setObject(index++, office_transfer_logsDTO.fromOrganogramNameBn);
            ps.setObject(index++, office_transfer_logsDTO.employeeRecordId);
            ps.setObject(index++, office_transfer_logsDTO.employeeNameEn);
            ps.setObject(index++, office_transfer_logsDTO.employeeNameBn);
            System.out.println(ps);
            ps.executeUpdate();


            recordUpdateTime(connection, lastModificationTime, tableName);


        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (connection != null) {
                    DBMW.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
        return office_transfer_logsDTO.id;
    }

    public List<Office_transfer_logsDTO> getDTOs(Collection recordIDs) {
        return getDTOs(recordIDs, tableName);
    }

    public List<Office_transfer_logsDTO> getDTOs(Collection recordIDs, String tableName) {
        Connection connection = null;
        ResultSet rs = null;
        Statement stmt = null;
        Office_transfer_logsDTO office_transfer_logsDTO = null;
        List<Office_transfer_logsDTO> office_transfer_logsDTOList = new ArrayList<>();
        if (recordIDs.isEmpty()) {
            return office_transfer_logsDTOList;
        }
        try {

            String sql = "SELECT * ";

            sql += " FROM " + tableName;

            sql += " WHERE id IN ( ";

            for (int i = 0; i < recordIDs.size(); i++) {
                if (i != 0) {
                    sql += ",";
                }
                sql += ((ArrayList) recordIDs).get(i);
            }
            sql += ")  order by lastModificationTime desc";

            printSql(sql);

            logger.debug("sql " + sql);
            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();


            rs = stmt.executeQuery(sql);

            while (rs.next()) {
                office_transfer_logsDTO = new Office_transfer_logsDTO();
                office_transfer_logsDTO.id = rs.getLong("id");
                office_transfer_logsDTO.toOfficeId = rs.getLong("to_office_id");
                office_transfer_logsDTO.toOfficeNameEn = rs.getString("to_office_name_en");
                office_transfer_logsDTO.toOfficeNameBn = rs.getString("to_office_name_bn");
                office_transfer_logsDTO.toUnitId = rs.getLong("to_unit_id");
                office_transfer_logsDTO.toUnitNameEn = rs.getString("to_unit_name_en");
                office_transfer_logsDTO.toUnitNameBn = rs.getString("to_unit_name_bn");
                office_transfer_logsDTO.toOrganogramId = rs.getLong("to_organogram_id");
                office_transfer_logsDTO.toOrganogramNameEn = rs.getString("to_organogram_name_en");
                office_transfer_logsDTO.toOrganogramNameBn = rs.getString("to_organogram_name_bn");
                office_transfer_logsDTO.fromOfficeId = rs.getLong("from_office_id");
                office_transfer_logsDTO.fromOfficeNameEn = rs.getString("from_office_name_en");
                office_transfer_logsDTO.fromOfficeNameBn = rs.getString("from_office_name_bn");
                office_transfer_logsDTO.fromUnitId = rs.getLong("from_unit_id");
                office_transfer_logsDTO.fromUnitNameEn = rs.getString("from_unit_name_en");
                office_transfer_logsDTO.fromUnitNameBn = rs.getString("from_unit_name_bn");
                office_transfer_logsDTO.fromOrganogramId = rs.getLong("from_organogram_id");
                office_transfer_logsDTO.fromOrganogramNameEn = rs.getString("from_organogram_name_en");
                office_transfer_logsDTO.fromOrganogramNameBn = rs.getString("from_organogram_name_bn");
                office_transfer_logsDTO.employeeRecordId = rs.getLong("employee_record_id");
                office_transfer_logsDTO.employeeNameEn = rs.getString("employee_name_en");
                office_transfer_logsDTO.employeeNameBn = rs.getString("employee_name_bn");
                office_transfer_logsDTO.isDeleted = rs.getInt("isDeleted");
                office_transfer_logsDTO.lastModificationTime = rs.getLong("lastModificationTime");
                System.out.println("got this DTO: " + office_transfer_logsDTO);

                office_transfer_logsDTOList.add(office_transfer_logsDTO);

            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }

            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
        return office_transfer_logsDTOList;

    }

    //add repository
    public List<Office_transfer_logsDTO> getAllOffice_transfer_logs(boolean isFirstReload) {
        List<Office_transfer_logsDTO> office_transfer_logsDTOList = new ArrayList<>();

        String sql = "SELECT * FROM office_transfer_logs";
        sql += " WHERE ";


        if (isFirstReload) {
            sql += " isDeleted =  0";
        }
        if (!isFirstReload) {
            sql += " lastModificationTime >= " + RepositoryManager.lastModifyTime;
        }
        sql += " order by office_transfer_logs.lastModificationTime desc";
        printSql(sql);

        Connection connection = null;
        Statement stmt = null;
        ResultSet rs = null;


        try {
            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();
            rs = stmt.executeQuery(sql);


            while (rs.next()) {
                Office_transfer_logsDTO office_transfer_logsDTO = new Office_transfer_logsDTO();
                office_transfer_logsDTO.id = rs.getLong("id");
                office_transfer_logsDTO.toOfficeId = rs.getLong("to_office_id");
                office_transfer_logsDTO.toOfficeNameEn = rs.getString("to_office_name_en");
                office_transfer_logsDTO.toOfficeNameBn = rs.getString("to_office_name_bn");
                office_transfer_logsDTO.toUnitId = rs.getLong("to_unit_id");
                office_transfer_logsDTO.toUnitNameEn = rs.getString("to_unit_name_en");
                office_transfer_logsDTO.toUnitNameBn = rs.getString("to_unit_name_bn");
                office_transfer_logsDTO.toOrganogramId = rs.getLong("to_organogram_id");
                office_transfer_logsDTO.toOrganogramNameEn = rs.getString("to_organogram_name_en");
                office_transfer_logsDTO.toOrganogramNameBn = rs.getString("to_organogram_name_bn");
                office_transfer_logsDTO.fromOfficeId = rs.getLong("from_office_id");
                office_transfer_logsDTO.fromOfficeNameEn = rs.getString("from_office_name_en");
                office_transfer_logsDTO.fromOfficeNameBn = rs.getString("from_office_name_bn");
                office_transfer_logsDTO.fromUnitId = rs.getLong("from_unit_id");
                office_transfer_logsDTO.fromUnitNameEn = rs.getString("from_unit_name_en");
                office_transfer_logsDTO.fromUnitNameBn = rs.getString("from_unit_name_bn");
                office_transfer_logsDTO.fromOrganogramId = rs.getLong("from_organogram_id");
                office_transfer_logsDTO.fromOrganogramNameEn = rs.getString("from_organogram_name_en");
                office_transfer_logsDTO.fromOrganogramNameBn = rs.getString("from_organogram_name_bn");
                office_transfer_logsDTO.employeeRecordId = rs.getLong("employee_record_id");
                office_transfer_logsDTO.employeeNameEn = rs.getString("employee_name_en");
                office_transfer_logsDTO.employeeNameBn = rs.getString("employee_name_bn");
                office_transfer_logsDTO.isDeleted = rs.getInt("isDeleted");
                office_transfer_logsDTO.lastModificationTime = rs.getLong("lastModificationTime");

                office_transfer_logsDTOList.add(office_transfer_logsDTO);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }

        return office_transfer_logsDTOList;
    }

    public List<Office_transfer_logsDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset) {
        return getDTOs(p_searchCriteria, limit, offset, tableName, true, 0);
    }

    public List<Office_transfer_logsDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, String tableName, boolean isPermanentTable, long userApprovalPathType) {
        Connection connection = null;
        ResultSet rs = null;
        Statement stmt = null;
        List<Office_transfer_logsDTO> office_transfer_logsDTOList = new ArrayList<>();

        try {

            String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, tableName, isPermanentTable, userApprovalPathType);

            printSql(sql);

            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();


            rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Office_transfer_logsDTO office_transfer_logsDTO = new Office_transfer_logsDTO();
                office_transfer_logsDTO.id = rs.getLong("id");
                office_transfer_logsDTO.toOfficeId = rs.getLong("to_office_id");
                office_transfer_logsDTO.toOfficeNameEn = rs.getString("to_office_name_en");
                office_transfer_logsDTO.toOfficeNameBn = rs.getString("to_office_name_bn");
                office_transfer_logsDTO.toUnitId = rs.getLong("to_unit_id");
                office_transfer_logsDTO.toUnitNameEn = rs.getString("to_unit_name_en");
                office_transfer_logsDTO.toUnitNameBn = rs.getString("to_unit_name_bn");
                office_transfer_logsDTO.toOrganogramId = rs.getLong("to_organogram_id");
                office_transfer_logsDTO.toOrganogramNameEn = rs.getString("to_organogram_name_en");
                office_transfer_logsDTO.toOrganogramNameBn = rs.getString("to_organogram_name_bn");
                office_transfer_logsDTO.fromOfficeId = rs.getLong("from_office_id");
                office_transfer_logsDTO.fromOfficeNameEn = rs.getString("from_office_name_en");
                office_transfer_logsDTO.fromOfficeNameBn = rs.getString("from_office_name_bn");
                office_transfer_logsDTO.fromUnitId = rs.getLong("from_unit_id");
                office_transfer_logsDTO.fromUnitNameEn = rs.getString("from_unit_name_en");
                office_transfer_logsDTO.fromUnitNameBn = rs.getString("from_unit_name_bn");
                office_transfer_logsDTO.fromOrganogramId = rs.getLong("from_organogram_id");
                office_transfer_logsDTO.fromOrganogramNameEn = rs.getString("from_organogram_name_en");
                office_transfer_logsDTO.fromOrganogramNameBn = rs.getString("from_organogram_name_bn");
                office_transfer_logsDTO.employeeRecordId = rs.getLong("employee_record_id");
                office_transfer_logsDTO.employeeNameEn = rs.getString("employee_name_en");
                office_transfer_logsDTO.employeeNameBn = rs.getString("employee_name_bn");
                office_transfer_logsDTO.isDeleted = rs.getInt("isDeleted");
                office_transfer_logsDTO.lastModificationTime = rs.getLong("lastModificationTime");

                office_transfer_logsDTOList.add(office_transfer_logsDTO);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }

            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
        return office_transfer_logsDTOList;

    }

    public String getSqlWithSearchCriteria(Hashtable p_searchCriteria, int limit, int offset, int category, String tableName, boolean isPermanentTable, long userApprovalPathType) {
        Office_transfer_logsMAPS maps = new Office_transfer_logsMAPS(tableName);
        String joinSQL = "";
        return getSqlWithSearchCriteria(p_searchCriteria, limit, offset, category, tableName, isPermanentTable, userApprovalPathType, maps, joinSQL);
    }

    @Override
    public long add(CommonDTO commonDTO, String tableName, TempTableDTO tempTableDTO) throws Exception {
        return 0;
    }
}
	