package office_transfer_logs;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class Office_transfer_logsRepository implements Repository {
	Office_transfer_logsDAO office_transfer_logsDAO = null;
	
	public void setDAO(Office_transfer_logsDAO office_transfer_logsDAO)
	{
		this.office_transfer_logsDAO = office_transfer_logsDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Office_transfer_logsRepository.class);
	Map<Long, Office_transfer_logsDTO>mapOfOffice_transfer_logsDTOToid;
	Map<Long, Set<Office_transfer_logsDTO> >mapOfOffice_transfer_logsDTOTotoOfficeId;
	Map<String, Set<Office_transfer_logsDTO> >mapOfOffice_transfer_logsDTOTotoOfficeNameEn;
	Map<String, Set<Office_transfer_logsDTO> >mapOfOffice_transfer_logsDTOTotoOfficeNameBn;
	Map<Long, Set<Office_transfer_logsDTO> >mapOfOffice_transfer_logsDTOTotoUnitId;
	Map<String, Set<Office_transfer_logsDTO> >mapOfOffice_transfer_logsDTOTotoUnitNameEn;
	Map<String, Set<Office_transfer_logsDTO> >mapOfOffice_transfer_logsDTOTotoUnitNameBn;
	Map<Long, Set<Office_transfer_logsDTO> >mapOfOffice_transfer_logsDTOTotoOrganogramId;
	Map<String, Set<Office_transfer_logsDTO> >mapOfOffice_transfer_logsDTOTotoOrganogramNameEn;
	Map<String, Set<Office_transfer_logsDTO> >mapOfOffice_transfer_logsDTOTotoOrganogramNameBn;
	Map<Long, Set<Office_transfer_logsDTO> >mapOfOffice_transfer_logsDTOTofromOfficeId;
	Map<String, Set<Office_transfer_logsDTO> >mapOfOffice_transfer_logsDTOTofromOfficeNameEn;
	Map<String, Set<Office_transfer_logsDTO> >mapOfOffice_transfer_logsDTOTofromOfficeNameBn;
	Map<Long, Set<Office_transfer_logsDTO> >mapOfOffice_transfer_logsDTOTofromUnitId;
	Map<String, Set<Office_transfer_logsDTO> >mapOfOffice_transfer_logsDTOTofromUnitNameEn;
	Map<String, Set<Office_transfer_logsDTO> >mapOfOffice_transfer_logsDTOTofromUnitNameBn;
	Map<Long, Set<Office_transfer_logsDTO> >mapOfOffice_transfer_logsDTOTofromOrganogramId;
	Map<String, Set<Office_transfer_logsDTO> >mapOfOffice_transfer_logsDTOTofromOrganogramNameEn;
	Map<String, Set<Office_transfer_logsDTO> >mapOfOffice_transfer_logsDTOTofromOrganogramNameBn;
	Map<Long, Set<Office_transfer_logsDTO> >mapOfOffice_transfer_logsDTOToemployeeRecordId;
	Map<String, Set<Office_transfer_logsDTO> >mapOfOffice_transfer_logsDTOToemployeeNameEn;
	Map<String, Set<Office_transfer_logsDTO> >mapOfOffice_transfer_logsDTOToemployeeNameBn;
	Map<Long, Set<Office_transfer_logsDTO> >mapOfOffice_transfer_logsDTOTolastModificationTime;


	static Office_transfer_logsRepository instance = null;  
	private Office_transfer_logsRepository(){
		mapOfOffice_transfer_logsDTOToid = new ConcurrentHashMap<>();
		mapOfOffice_transfer_logsDTOTotoOfficeId = new ConcurrentHashMap<>();
		mapOfOffice_transfer_logsDTOTotoOfficeNameEn = new ConcurrentHashMap<>();
		mapOfOffice_transfer_logsDTOTotoOfficeNameBn = new ConcurrentHashMap<>();
		mapOfOffice_transfer_logsDTOTotoUnitId = new ConcurrentHashMap<>();
		mapOfOffice_transfer_logsDTOTotoUnitNameEn = new ConcurrentHashMap<>();
		mapOfOffice_transfer_logsDTOTotoUnitNameBn = new ConcurrentHashMap<>();
		mapOfOffice_transfer_logsDTOTotoOrganogramId = new ConcurrentHashMap<>();
		mapOfOffice_transfer_logsDTOTotoOrganogramNameEn = new ConcurrentHashMap<>();
		mapOfOffice_transfer_logsDTOTotoOrganogramNameBn = new ConcurrentHashMap<>();
		mapOfOffice_transfer_logsDTOTofromOfficeId = new ConcurrentHashMap<>();
		mapOfOffice_transfer_logsDTOTofromOfficeNameEn = new ConcurrentHashMap<>();
		mapOfOffice_transfer_logsDTOTofromOfficeNameBn = new ConcurrentHashMap<>();
		mapOfOffice_transfer_logsDTOTofromUnitId = new ConcurrentHashMap<>();
		mapOfOffice_transfer_logsDTOTofromUnitNameEn = new ConcurrentHashMap<>();
		mapOfOffice_transfer_logsDTOTofromUnitNameBn = new ConcurrentHashMap<>();
		mapOfOffice_transfer_logsDTOTofromOrganogramId = new ConcurrentHashMap<>();
		mapOfOffice_transfer_logsDTOTofromOrganogramNameEn = new ConcurrentHashMap<>();
		mapOfOffice_transfer_logsDTOTofromOrganogramNameBn = new ConcurrentHashMap<>();
		mapOfOffice_transfer_logsDTOToemployeeRecordId = new ConcurrentHashMap<>();
		mapOfOffice_transfer_logsDTOToemployeeNameEn = new ConcurrentHashMap<>();
		mapOfOffice_transfer_logsDTOToemployeeNameBn = new ConcurrentHashMap<>();
		mapOfOffice_transfer_logsDTOTolastModificationTime = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Office_transfer_logsRepository getInstance(){
		if (instance == null){
			instance = new Office_transfer_logsRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(office_transfer_logsDAO == null)
		{
			return;
		}
		try {
			List<Office_transfer_logsDTO> office_transfer_logsDTOs = office_transfer_logsDAO.getAllOffice_transfer_logs(reloadAll);
			for(Office_transfer_logsDTO office_transfer_logsDTO : office_transfer_logsDTOs) {
				Office_transfer_logsDTO oldOffice_transfer_logsDTO = getOffice_transfer_logsDTOByid(office_transfer_logsDTO.id);
				if( oldOffice_transfer_logsDTO != null ) {
					mapOfOffice_transfer_logsDTOToid.remove(oldOffice_transfer_logsDTO.id);
				
					if(mapOfOffice_transfer_logsDTOTotoOfficeId.containsKey(oldOffice_transfer_logsDTO.toOfficeId)) {
						mapOfOffice_transfer_logsDTOTotoOfficeId.get(oldOffice_transfer_logsDTO.toOfficeId).remove(oldOffice_transfer_logsDTO);
					}
					if(mapOfOffice_transfer_logsDTOTotoOfficeId.get(oldOffice_transfer_logsDTO.toOfficeId).isEmpty()) {
						mapOfOffice_transfer_logsDTOTotoOfficeId.remove(oldOffice_transfer_logsDTO.toOfficeId);
					}
					
					if(mapOfOffice_transfer_logsDTOTotoOfficeNameEn.containsKey(oldOffice_transfer_logsDTO.toOfficeNameEn)) {
						mapOfOffice_transfer_logsDTOTotoOfficeNameEn.get(oldOffice_transfer_logsDTO.toOfficeNameEn).remove(oldOffice_transfer_logsDTO);
					}
					if(mapOfOffice_transfer_logsDTOTotoOfficeNameEn.get(oldOffice_transfer_logsDTO.toOfficeNameEn).isEmpty()) {
						mapOfOffice_transfer_logsDTOTotoOfficeNameEn.remove(oldOffice_transfer_logsDTO.toOfficeNameEn);
					}
					
					if(mapOfOffice_transfer_logsDTOTotoOfficeNameBn.containsKey(oldOffice_transfer_logsDTO.toOfficeNameBn)) {
						mapOfOffice_transfer_logsDTOTotoOfficeNameBn.get(oldOffice_transfer_logsDTO.toOfficeNameBn).remove(oldOffice_transfer_logsDTO);
					}
					if(mapOfOffice_transfer_logsDTOTotoOfficeNameBn.get(oldOffice_transfer_logsDTO.toOfficeNameBn).isEmpty()) {
						mapOfOffice_transfer_logsDTOTotoOfficeNameBn.remove(oldOffice_transfer_logsDTO.toOfficeNameBn);
					}
					
					if(mapOfOffice_transfer_logsDTOTotoUnitId.containsKey(oldOffice_transfer_logsDTO.toUnitId)) {
						mapOfOffice_transfer_logsDTOTotoUnitId.get(oldOffice_transfer_logsDTO.toUnitId).remove(oldOffice_transfer_logsDTO);
					}
					if(mapOfOffice_transfer_logsDTOTotoUnitId.get(oldOffice_transfer_logsDTO.toUnitId).isEmpty()) {
						mapOfOffice_transfer_logsDTOTotoUnitId.remove(oldOffice_transfer_logsDTO.toUnitId);
					}
					
					if(mapOfOffice_transfer_logsDTOTotoUnitNameEn.containsKey(oldOffice_transfer_logsDTO.toUnitNameEn)) {
						mapOfOffice_transfer_logsDTOTotoUnitNameEn.get(oldOffice_transfer_logsDTO.toUnitNameEn).remove(oldOffice_transfer_logsDTO);
					}
					if(mapOfOffice_transfer_logsDTOTotoUnitNameEn.get(oldOffice_transfer_logsDTO.toUnitNameEn).isEmpty()) {
						mapOfOffice_transfer_logsDTOTotoUnitNameEn.remove(oldOffice_transfer_logsDTO.toUnitNameEn);
					}
					
					if(mapOfOffice_transfer_logsDTOTotoUnitNameBn.containsKey(oldOffice_transfer_logsDTO.toUnitNameBn)) {
						mapOfOffice_transfer_logsDTOTotoUnitNameBn.get(oldOffice_transfer_logsDTO.toUnitNameBn).remove(oldOffice_transfer_logsDTO);
					}
					if(mapOfOffice_transfer_logsDTOTotoUnitNameBn.get(oldOffice_transfer_logsDTO.toUnitNameBn).isEmpty()) {
						mapOfOffice_transfer_logsDTOTotoUnitNameBn.remove(oldOffice_transfer_logsDTO.toUnitNameBn);
					}
					
					if(mapOfOffice_transfer_logsDTOTotoOrganogramId.containsKey(oldOffice_transfer_logsDTO.toOrganogramId)) {
						mapOfOffice_transfer_logsDTOTotoOrganogramId.get(oldOffice_transfer_logsDTO.toOrganogramId).remove(oldOffice_transfer_logsDTO);
					}
					if(mapOfOffice_transfer_logsDTOTotoOrganogramId.get(oldOffice_transfer_logsDTO.toOrganogramId).isEmpty()) {
						mapOfOffice_transfer_logsDTOTotoOrganogramId.remove(oldOffice_transfer_logsDTO.toOrganogramId);
					}
					
					if(mapOfOffice_transfer_logsDTOTotoOrganogramNameEn.containsKey(oldOffice_transfer_logsDTO.toOrganogramNameEn)) {
						mapOfOffice_transfer_logsDTOTotoOrganogramNameEn.get(oldOffice_transfer_logsDTO.toOrganogramNameEn).remove(oldOffice_transfer_logsDTO);
					}
					if(mapOfOffice_transfer_logsDTOTotoOrganogramNameEn.get(oldOffice_transfer_logsDTO.toOrganogramNameEn).isEmpty()) {
						mapOfOffice_transfer_logsDTOTotoOrganogramNameEn.remove(oldOffice_transfer_logsDTO.toOrganogramNameEn);
					}
					
					if(mapOfOffice_transfer_logsDTOTotoOrganogramNameBn.containsKey(oldOffice_transfer_logsDTO.toOrganogramNameBn)) {
						mapOfOffice_transfer_logsDTOTotoOrganogramNameBn.get(oldOffice_transfer_logsDTO.toOrganogramNameBn).remove(oldOffice_transfer_logsDTO);
					}
					if(mapOfOffice_transfer_logsDTOTotoOrganogramNameBn.get(oldOffice_transfer_logsDTO.toOrganogramNameBn).isEmpty()) {
						mapOfOffice_transfer_logsDTOTotoOrganogramNameBn.remove(oldOffice_transfer_logsDTO.toOrganogramNameBn);
					}
					
					if(mapOfOffice_transfer_logsDTOTofromOfficeId.containsKey(oldOffice_transfer_logsDTO.fromOfficeId)) {
						mapOfOffice_transfer_logsDTOTofromOfficeId.get(oldOffice_transfer_logsDTO.fromOfficeId).remove(oldOffice_transfer_logsDTO);
					}
					if(mapOfOffice_transfer_logsDTOTofromOfficeId.get(oldOffice_transfer_logsDTO.fromOfficeId).isEmpty()) {
						mapOfOffice_transfer_logsDTOTofromOfficeId.remove(oldOffice_transfer_logsDTO.fromOfficeId);
					}
					
					if(mapOfOffice_transfer_logsDTOTofromOfficeNameEn.containsKey(oldOffice_transfer_logsDTO.fromOfficeNameEn)) {
						mapOfOffice_transfer_logsDTOTofromOfficeNameEn.get(oldOffice_transfer_logsDTO.fromOfficeNameEn).remove(oldOffice_transfer_logsDTO);
					}
					if(mapOfOffice_transfer_logsDTOTofromOfficeNameEn.get(oldOffice_transfer_logsDTO.fromOfficeNameEn).isEmpty()) {
						mapOfOffice_transfer_logsDTOTofromOfficeNameEn.remove(oldOffice_transfer_logsDTO.fromOfficeNameEn);
					}
					
					if(mapOfOffice_transfer_logsDTOTofromOfficeNameBn.containsKey(oldOffice_transfer_logsDTO.fromOfficeNameBn)) {
						mapOfOffice_transfer_logsDTOTofromOfficeNameBn.get(oldOffice_transfer_logsDTO.fromOfficeNameBn).remove(oldOffice_transfer_logsDTO);
					}
					if(mapOfOffice_transfer_logsDTOTofromOfficeNameBn.get(oldOffice_transfer_logsDTO.fromOfficeNameBn).isEmpty()) {
						mapOfOffice_transfer_logsDTOTofromOfficeNameBn.remove(oldOffice_transfer_logsDTO.fromOfficeNameBn);
					}
					
					if(mapOfOffice_transfer_logsDTOTofromUnitId.containsKey(oldOffice_transfer_logsDTO.fromUnitId)) {
						mapOfOffice_transfer_logsDTOTofromUnitId.get(oldOffice_transfer_logsDTO.fromUnitId).remove(oldOffice_transfer_logsDTO);
					}
					if(mapOfOffice_transfer_logsDTOTofromUnitId.get(oldOffice_transfer_logsDTO.fromUnitId).isEmpty()) {
						mapOfOffice_transfer_logsDTOTofromUnitId.remove(oldOffice_transfer_logsDTO.fromUnitId);
					}
					
					if(mapOfOffice_transfer_logsDTOTofromUnitNameEn.containsKey(oldOffice_transfer_logsDTO.fromUnitNameEn)) {
						mapOfOffice_transfer_logsDTOTofromUnitNameEn.get(oldOffice_transfer_logsDTO.fromUnitNameEn).remove(oldOffice_transfer_logsDTO);
					}
					if(mapOfOffice_transfer_logsDTOTofromUnitNameEn.get(oldOffice_transfer_logsDTO.fromUnitNameEn).isEmpty()) {
						mapOfOffice_transfer_logsDTOTofromUnitNameEn.remove(oldOffice_transfer_logsDTO.fromUnitNameEn);
					}
					
					if(mapOfOffice_transfer_logsDTOTofromUnitNameBn.containsKey(oldOffice_transfer_logsDTO.fromUnitNameBn)) {
						mapOfOffice_transfer_logsDTOTofromUnitNameBn.get(oldOffice_transfer_logsDTO.fromUnitNameBn).remove(oldOffice_transfer_logsDTO);
					}
					if(mapOfOffice_transfer_logsDTOTofromUnitNameBn.get(oldOffice_transfer_logsDTO.fromUnitNameBn).isEmpty()) {
						mapOfOffice_transfer_logsDTOTofromUnitNameBn.remove(oldOffice_transfer_logsDTO.fromUnitNameBn);
					}
					
					if(mapOfOffice_transfer_logsDTOTofromOrganogramId.containsKey(oldOffice_transfer_logsDTO.fromOrganogramId)) {
						mapOfOffice_transfer_logsDTOTofromOrganogramId.get(oldOffice_transfer_logsDTO.fromOrganogramId).remove(oldOffice_transfer_logsDTO);
					}
					if(mapOfOffice_transfer_logsDTOTofromOrganogramId.get(oldOffice_transfer_logsDTO.fromOrganogramId).isEmpty()) {
						mapOfOffice_transfer_logsDTOTofromOrganogramId.remove(oldOffice_transfer_logsDTO.fromOrganogramId);
					}
					
					if(mapOfOffice_transfer_logsDTOTofromOrganogramNameEn.containsKey(oldOffice_transfer_logsDTO.fromOrganogramNameEn)) {
						mapOfOffice_transfer_logsDTOTofromOrganogramNameEn.get(oldOffice_transfer_logsDTO.fromOrganogramNameEn).remove(oldOffice_transfer_logsDTO);
					}
					if(mapOfOffice_transfer_logsDTOTofromOrganogramNameEn.get(oldOffice_transfer_logsDTO.fromOrganogramNameEn).isEmpty()) {
						mapOfOffice_transfer_logsDTOTofromOrganogramNameEn.remove(oldOffice_transfer_logsDTO.fromOrganogramNameEn);
					}
					
					if(mapOfOffice_transfer_logsDTOTofromOrganogramNameBn.containsKey(oldOffice_transfer_logsDTO.fromOrganogramNameBn)) {
						mapOfOffice_transfer_logsDTOTofromOrganogramNameBn.get(oldOffice_transfer_logsDTO.fromOrganogramNameBn).remove(oldOffice_transfer_logsDTO);
					}
					if(mapOfOffice_transfer_logsDTOTofromOrganogramNameBn.get(oldOffice_transfer_logsDTO.fromOrganogramNameBn).isEmpty()) {
						mapOfOffice_transfer_logsDTOTofromOrganogramNameBn.remove(oldOffice_transfer_logsDTO.fromOrganogramNameBn);
					}
					
					if(mapOfOffice_transfer_logsDTOToemployeeRecordId.containsKey(oldOffice_transfer_logsDTO.employeeRecordId)) {
						mapOfOffice_transfer_logsDTOToemployeeRecordId.get(oldOffice_transfer_logsDTO.employeeRecordId).remove(oldOffice_transfer_logsDTO);
					}
					if(mapOfOffice_transfer_logsDTOToemployeeRecordId.get(oldOffice_transfer_logsDTO.employeeRecordId).isEmpty()) {
						mapOfOffice_transfer_logsDTOToemployeeRecordId.remove(oldOffice_transfer_logsDTO.employeeRecordId);
					}
					
					if(mapOfOffice_transfer_logsDTOToemployeeNameEn.containsKey(oldOffice_transfer_logsDTO.employeeNameEn)) {
						mapOfOffice_transfer_logsDTOToemployeeNameEn.get(oldOffice_transfer_logsDTO.employeeNameEn).remove(oldOffice_transfer_logsDTO);
					}
					if(mapOfOffice_transfer_logsDTOToemployeeNameEn.get(oldOffice_transfer_logsDTO.employeeNameEn).isEmpty()) {
						mapOfOffice_transfer_logsDTOToemployeeNameEn.remove(oldOffice_transfer_logsDTO.employeeNameEn);
					}
					
					if(mapOfOffice_transfer_logsDTOToemployeeNameBn.containsKey(oldOffice_transfer_logsDTO.employeeNameBn)) {
						mapOfOffice_transfer_logsDTOToemployeeNameBn.get(oldOffice_transfer_logsDTO.employeeNameBn).remove(oldOffice_transfer_logsDTO);
					}
					if(mapOfOffice_transfer_logsDTOToemployeeNameBn.get(oldOffice_transfer_logsDTO.employeeNameBn).isEmpty()) {
						mapOfOffice_transfer_logsDTOToemployeeNameBn.remove(oldOffice_transfer_logsDTO.employeeNameBn);
					}
					
					if(mapOfOffice_transfer_logsDTOTolastModificationTime.containsKey(oldOffice_transfer_logsDTO.lastModificationTime)) {
						mapOfOffice_transfer_logsDTOTolastModificationTime.get(oldOffice_transfer_logsDTO.lastModificationTime).remove(oldOffice_transfer_logsDTO);
					}
					if(mapOfOffice_transfer_logsDTOTolastModificationTime.get(oldOffice_transfer_logsDTO.lastModificationTime).isEmpty()) {
						mapOfOffice_transfer_logsDTOTolastModificationTime.remove(oldOffice_transfer_logsDTO.lastModificationTime);
					}
					
					
				}
				if(office_transfer_logsDTO.isDeleted == 0)
				{
					
					mapOfOffice_transfer_logsDTOToid.put(office_transfer_logsDTO.id, office_transfer_logsDTO);
				
					if( ! mapOfOffice_transfer_logsDTOTotoOfficeId.containsKey(office_transfer_logsDTO.toOfficeId)) {
						mapOfOffice_transfer_logsDTOTotoOfficeId.put(office_transfer_logsDTO.toOfficeId, new HashSet<>());
					}
					mapOfOffice_transfer_logsDTOTotoOfficeId.get(office_transfer_logsDTO.toOfficeId).add(office_transfer_logsDTO);
					
					if( ! mapOfOffice_transfer_logsDTOTotoOfficeNameEn.containsKey(office_transfer_logsDTO.toOfficeNameEn)) {
						mapOfOffice_transfer_logsDTOTotoOfficeNameEn.put(office_transfer_logsDTO.toOfficeNameEn, new HashSet<>());
					}
					mapOfOffice_transfer_logsDTOTotoOfficeNameEn.get(office_transfer_logsDTO.toOfficeNameEn).add(office_transfer_logsDTO);
					
					if( ! mapOfOffice_transfer_logsDTOTotoOfficeNameBn.containsKey(office_transfer_logsDTO.toOfficeNameBn)) {
						mapOfOffice_transfer_logsDTOTotoOfficeNameBn.put(office_transfer_logsDTO.toOfficeNameBn, new HashSet<>());
					}
					mapOfOffice_transfer_logsDTOTotoOfficeNameBn.get(office_transfer_logsDTO.toOfficeNameBn).add(office_transfer_logsDTO);
					
					if( ! mapOfOffice_transfer_logsDTOTotoUnitId.containsKey(office_transfer_logsDTO.toUnitId)) {
						mapOfOffice_transfer_logsDTOTotoUnitId.put(office_transfer_logsDTO.toUnitId, new HashSet<>());
					}
					mapOfOffice_transfer_logsDTOTotoUnitId.get(office_transfer_logsDTO.toUnitId).add(office_transfer_logsDTO);
					
					if( ! mapOfOffice_transfer_logsDTOTotoUnitNameEn.containsKey(office_transfer_logsDTO.toUnitNameEn)) {
						mapOfOffice_transfer_logsDTOTotoUnitNameEn.put(office_transfer_logsDTO.toUnitNameEn, new HashSet<>());
					}
					mapOfOffice_transfer_logsDTOTotoUnitNameEn.get(office_transfer_logsDTO.toUnitNameEn).add(office_transfer_logsDTO);
					
					if( ! mapOfOffice_transfer_logsDTOTotoUnitNameBn.containsKey(office_transfer_logsDTO.toUnitNameBn)) {
						mapOfOffice_transfer_logsDTOTotoUnitNameBn.put(office_transfer_logsDTO.toUnitNameBn, new HashSet<>());
					}
					mapOfOffice_transfer_logsDTOTotoUnitNameBn.get(office_transfer_logsDTO.toUnitNameBn).add(office_transfer_logsDTO);
					
					if( ! mapOfOffice_transfer_logsDTOTotoOrganogramId.containsKey(office_transfer_logsDTO.toOrganogramId)) {
						mapOfOffice_transfer_logsDTOTotoOrganogramId.put(office_transfer_logsDTO.toOrganogramId, new HashSet<>());
					}
					mapOfOffice_transfer_logsDTOTotoOrganogramId.get(office_transfer_logsDTO.toOrganogramId).add(office_transfer_logsDTO);
					
					if( ! mapOfOffice_transfer_logsDTOTotoOrganogramNameEn.containsKey(office_transfer_logsDTO.toOrganogramNameEn)) {
						mapOfOffice_transfer_logsDTOTotoOrganogramNameEn.put(office_transfer_logsDTO.toOrganogramNameEn, new HashSet<>());
					}
					mapOfOffice_transfer_logsDTOTotoOrganogramNameEn.get(office_transfer_logsDTO.toOrganogramNameEn).add(office_transfer_logsDTO);
					
					if( ! mapOfOffice_transfer_logsDTOTotoOrganogramNameBn.containsKey(office_transfer_logsDTO.toOrganogramNameBn)) {
						mapOfOffice_transfer_logsDTOTotoOrganogramNameBn.put(office_transfer_logsDTO.toOrganogramNameBn, new HashSet<>());
					}
					mapOfOffice_transfer_logsDTOTotoOrganogramNameBn.get(office_transfer_logsDTO.toOrganogramNameBn).add(office_transfer_logsDTO);
					
					if( ! mapOfOffice_transfer_logsDTOTofromOfficeId.containsKey(office_transfer_logsDTO.fromOfficeId)) {
						mapOfOffice_transfer_logsDTOTofromOfficeId.put(office_transfer_logsDTO.fromOfficeId, new HashSet<>());
					}
					mapOfOffice_transfer_logsDTOTofromOfficeId.get(office_transfer_logsDTO.fromOfficeId).add(office_transfer_logsDTO);
					
					if( ! mapOfOffice_transfer_logsDTOTofromOfficeNameEn.containsKey(office_transfer_logsDTO.fromOfficeNameEn)) {
						mapOfOffice_transfer_logsDTOTofromOfficeNameEn.put(office_transfer_logsDTO.fromOfficeNameEn, new HashSet<>());
					}
					mapOfOffice_transfer_logsDTOTofromOfficeNameEn.get(office_transfer_logsDTO.fromOfficeNameEn).add(office_transfer_logsDTO);
					
					if( ! mapOfOffice_transfer_logsDTOTofromOfficeNameBn.containsKey(office_transfer_logsDTO.fromOfficeNameBn)) {
						mapOfOffice_transfer_logsDTOTofromOfficeNameBn.put(office_transfer_logsDTO.fromOfficeNameBn, new HashSet<>());
					}
					mapOfOffice_transfer_logsDTOTofromOfficeNameBn.get(office_transfer_logsDTO.fromOfficeNameBn).add(office_transfer_logsDTO);
					
					if( ! mapOfOffice_transfer_logsDTOTofromUnitId.containsKey(office_transfer_logsDTO.fromUnitId)) {
						mapOfOffice_transfer_logsDTOTofromUnitId.put(office_transfer_logsDTO.fromUnitId, new HashSet<>());
					}
					mapOfOffice_transfer_logsDTOTofromUnitId.get(office_transfer_logsDTO.fromUnitId).add(office_transfer_logsDTO);
					
					if( ! mapOfOffice_transfer_logsDTOTofromUnitNameEn.containsKey(office_transfer_logsDTO.fromUnitNameEn)) {
						mapOfOffice_transfer_logsDTOTofromUnitNameEn.put(office_transfer_logsDTO.fromUnitNameEn, new HashSet<>());
					}
					mapOfOffice_transfer_logsDTOTofromUnitNameEn.get(office_transfer_logsDTO.fromUnitNameEn).add(office_transfer_logsDTO);
					
					if( ! mapOfOffice_transfer_logsDTOTofromUnitNameBn.containsKey(office_transfer_logsDTO.fromUnitNameBn)) {
						mapOfOffice_transfer_logsDTOTofromUnitNameBn.put(office_transfer_logsDTO.fromUnitNameBn, new HashSet<>());
					}
					mapOfOffice_transfer_logsDTOTofromUnitNameBn.get(office_transfer_logsDTO.fromUnitNameBn).add(office_transfer_logsDTO);
					
					if( ! mapOfOffice_transfer_logsDTOTofromOrganogramId.containsKey(office_transfer_logsDTO.fromOrganogramId)) {
						mapOfOffice_transfer_logsDTOTofromOrganogramId.put(office_transfer_logsDTO.fromOrganogramId, new HashSet<>());
					}
					mapOfOffice_transfer_logsDTOTofromOrganogramId.get(office_transfer_logsDTO.fromOrganogramId).add(office_transfer_logsDTO);
					
					if( ! mapOfOffice_transfer_logsDTOTofromOrganogramNameEn.containsKey(office_transfer_logsDTO.fromOrganogramNameEn)) {
						mapOfOffice_transfer_logsDTOTofromOrganogramNameEn.put(office_transfer_logsDTO.fromOrganogramNameEn, new HashSet<>());
					}
					mapOfOffice_transfer_logsDTOTofromOrganogramNameEn.get(office_transfer_logsDTO.fromOrganogramNameEn).add(office_transfer_logsDTO);
					
					if( ! mapOfOffice_transfer_logsDTOTofromOrganogramNameBn.containsKey(office_transfer_logsDTO.fromOrganogramNameBn)) {
						mapOfOffice_transfer_logsDTOTofromOrganogramNameBn.put(office_transfer_logsDTO.fromOrganogramNameBn, new HashSet<>());
					}
					mapOfOffice_transfer_logsDTOTofromOrganogramNameBn.get(office_transfer_logsDTO.fromOrganogramNameBn).add(office_transfer_logsDTO);
					
					if( ! mapOfOffice_transfer_logsDTOToemployeeRecordId.containsKey(office_transfer_logsDTO.employeeRecordId)) {
						mapOfOffice_transfer_logsDTOToemployeeRecordId.put(office_transfer_logsDTO.employeeRecordId, new HashSet<>());
					}
					mapOfOffice_transfer_logsDTOToemployeeRecordId.get(office_transfer_logsDTO.employeeRecordId).add(office_transfer_logsDTO);
					
					if( ! mapOfOffice_transfer_logsDTOToemployeeNameEn.containsKey(office_transfer_logsDTO.employeeNameEn)) {
						mapOfOffice_transfer_logsDTOToemployeeNameEn.put(office_transfer_logsDTO.employeeNameEn, new HashSet<>());
					}
					mapOfOffice_transfer_logsDTOToemployeeNameEn.get(office_transfer_logsDTO.employeeNameEn).add(office_transfer_logsDTO);
					
					if( ! mapOfOffice_transfer_logsDTOToemployeeNameBn.containsKey(office_transfer_logsDTO.employeeNameBn)) {
						mapOfOffice_transfer_logsDTOToemployeeNameBn.put(office_transfer_logsDTO.employeeNameBn, new HashSet<>());
					}
					mapOfOffice_transfer_logsDTOToemployeeNameBn.get(office_transfer_logsDTO.employeeNameBn).add(office_transfer_logsDTO);
					
					if( ! mapOfOffice_transfer_logsDTOTolastModificationTime.containsKey(office_transfer_logsDTO.lastModificationTime)) {
						mapOfOffice_transfer_logsDTOTolastModificationTime.put(office_transfer_logsDTO.lastModificationTime, new HashSet<>());
					}
					mapOfOffice_transfer_logsDTOTolastModificationTime.get(office_transfer_logsDTO.lastModificationTime).add(office_transfer_logsDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Office_transfer_logsDTO> getOffice_transfer_logsList() {
		List <Office_transfer_logsDTO> office_transfer_logss = new ArrayList<Office_transfer_logsDTO>(this.mapOfOffice_transfer_logsDTOToid.values());
		return office_transfer_logss;
	}
	
	
	public Office_transfer_logsDTO getOffice_transfer_logsDTOByid( long id){
		return mapOfOffice_transfer_logsDTOToid.get(id);
	}
	
	
	public List<Office_transfer_logsDTO> getOffice_transfer_logsDTOByto_office_id(long to_office_id) {
		return new ArrayList<>( mapOfOffice_transfer_logsDTOTotoOfficeId.getOrDefault(to_office_id,new HashSet<>()));
	}
	
	
	public List<Office_transfer_logsDTO> getOffice_transfer_logsDTOByto_office_name_en(String to_office_name_en) {
		return new ArrayList<>( mapOfOffice_transfer_logsDTOTotoOfficeNameEn.getOrDefault(to_office_name_en,new HashSet<>()));
	}
	
	
	public List<Office_transfer_logsDTO> getOffice_transfer_logsDTOByto_office_name_bn(String to_office_name_bn) {
		return new ArrayList<>( mapOfOffice_transfer_logsDTOTotoOfficeNameBn.getOrDefault(to_office_name_bn,new HashSet<>()));
	}
	
	
	public List<Office_transfer_logsDTO> getOffice_transfer_logsDTOByto_unit_id(long to_unit_id) {
		return new ArrayList<>( mapOfOffice_transfer_logsDTOTotoUnitId.getOrDefault(to_unit_id,new HashSet<>()));
	}
	
	
	public List<Office_transfer_logsDTO> getOffice_transfer_logsDTOByto_unit_name_en(String to_unit_name_en) {
		return new ArrayList<>( mapOfOffice_transfer_logsDTOTotoUnitNameEn.getOrDefault(to_unit_name_en,new HashSet<>()));
	}
	
	
	public List<Office_transfer_logsDTO> getOffice_transfer_logsDTOByto_unit_name_bn(String to_unit_name_bn) {
		return new ArrayList<>( mapOfOffice_transfer_logsDTOTotoUnitNameBn.getOrDefault(to_unit_name_bn,new HashSet<>()));
	}
	
	
	public List<Office_transfer_logsDTO> getOffice_transfer_logsDTOByto_organogram_id(long to_organogram_id) {
		return new ArrayList<>( mapOfOffice_transfer_logsDTOTotoOrganogramId.getOrDefault(to_organogram_id,new HashSet<>()));
	}
	
	
	public List<Office_transfer_logsDTO> getOffice_transfer_logsDTOByto_organogram_name_en(String to_organogram_name_en) {
		return new ArrayList<>( mapOfOffice_transfer_logsDTOTotoOrganogramNameEn.getOrDefault(to_organogram_name_en,new HashSet<>()));
	}
	
	
	public List<Office_transfer_logsDTO> getOffice_transfer_logsDTOByto_organogram_name_bn(String to_organogram_name_bn) {
		return new ArrayList<>( mapOfOffice_transfer_logsDTOTotoOrganogramNameBn.getOrDefault(to_organogram_name_bn,new HashSet<>()));
	}
	
	
	public List<Office_transfer_logsDTO> getOffice_transfer_logsDTOByfrom_office_id(long from_office_id) {
		return new ArrayList<>( mapOfOffice_transfer_logsDTOTofromOfficeId.getOrDefault(from_office_id,new HashSet<>()));
	}
	
	
	public List<Office_transfer_logsDTO> getOffice_transfer_logsDTOByfrom_office_name_en(String from_office_name_en) {
		return new ArrayList<>( mapOfOffice_transfer_logsDTOTofromOfficeNameEn.getOrDefault(from_office_name_en,new HashSet<>()));
	}
	
	
	public List<Office_transfer_logsDTO> getOffice_transfer_logsDTOByfrom_office_name_bn(String from_office_name_bn) {
		return new ArrayList<>( mapOfOffice_transfer_logsDTOTofromOfficeNameBn.getOrDefault(from_office_name_bn,new HashSet<>()));
	}
	
	
	public List<Office_transfer_logsDTO> getOffice_transfer_logsDTOByfrom_unit_id(long from_unit_id) {
		return new ArrayList<>( mapOfOffice_transfer_logsDTOTofromUnitId.getOrDefault(from_unit_id,new HashSet<>()));
	}
	
	
	public List<Office_transfer_logsDTO> getOffice_transfer_logsDTOByfrom_unit_name_en(String from_unit_name_en) {
		return new ArrayList<>( mapOfOffice_transfer_logsDTOTofromUnitNameEn.getOrDefault(from_unit_name_en,new HashSet<>()));
	}
	
	
	public List<Office_transfer_logsDTO> getOffice_transfer_logsDTOByfrom_unit_name_bn(String from_unit_name_bn) {
		return new ArrayList<>( mapOfOffice_transfer_logsDTOTofromUnitNameBn.getOrDefault(from_unit_name_bn,new HashSet<>()));
	}
	
	
	public List<Office_transfer_logsDTO> getOffice_transfer_logsDTOByfrom_organogram_id(long from_organogram_id) {
		return new ArrayList<>( mapOfOffice_transfer_logsDTOTofromOrganogramId.getOrDefault(from_organogram_id,new HashSet<>()));
	}
	
	
	public List<Office_transfer_logsDTO> getOffice_transfer_logsDTOByfrom_organogram_name_en(String from_organogram_name_en) {
		return new ArrayList<>( mapOfOffice_transfer_logsDTOTofromOrganogramNameEn.getOrDefault(from_organogram_name_en,new HashSet<>()));
	}
	
	
	public List<Office_transfer_logsDTO> getOffice_transfer_logsDTOByfrom_organogram_name_bn(String from_organogram_name_bn) {
		return new ArrayList<>( mapOfOffice_transfer_logsDTOTofromOrganogramNameBn.getOrDefault(from_organogram_name_bn,new HashSet<>()));
	}
	
	
	public List<Office_transfer_logsDTO> getOffice_transfer_logsDTOByemployee_record_id(long employee_record_id) {
		return new ArrayList<>( mapOfOffice_transfer_logsDTOToemployeeRecordId.getOrDefault(employee_record_id,new HashSet<>()));
	}
	
	
	public List<Office_transfer_logsDTO> getOffice_transfer_logsDTOByemployee_name_en(String employee_name_en) {
		return new ArrayList<>( mapOfOffice_transfer_logsDTOToemployeeNameEn.getOrDefault(employee_name_en,new HashSet<>()));
	}
	
	
	public List<Office_transfer_logsDTO> getOffice_transfer_logsDTOByemployee_name_bn(String employee_name_bn) {
		return new ArrayList<>( mapOfOffice_transfer_logsDTOToemployeeNameBn.getOrDefault(employee_name_bn,new HashSet<>()));
	}
	
	
	public List<Office_transfer_logsDTO> getOffice_transfer_logsDTOBylastModificationTime(long lastModificationTime) {
		return new ArrayList<>( mapOfOffice_transfer_logsDTOTolastModificationTime.getOrDefault(lastModificationTime,new HashSet<>()));
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "office_transfer_logs";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


