package office_transfer_logs;
import java.util.*; 
import util.*;


public class Office_transfer_logsMAPS extends CommonMaps
{	
	public Office_transfer_logsMAPS(String tableName)
	{
		
		java_allfield_type_map.put("to_office_name_en".toLowerCase(), "String");
		java_allfield_type_map.put("to_office_name_bn".toLowerCase(), "String");
		java_allfield_type_map.put("to_unit_name_en".toLowerCase(), "String");
		java_allfield_type_map.put("to_unit_name_bn".toLowerCase(), "String");
		java_allfield_type_map.put("to_organogram_name_en".toLowerCase(), "String");
		java_allfield_type_map.put("to_organogram_name_bn".toLowerCase(), "String");
		java_allfield_type_map.put("from_office_name_en".toLowerCase(), "String");
		java_allfield_type_map.put("from_office_name_bn".toLowerCase(), "String");
		java_allfield_type_map.put("from_unit_name_en".toLowerCase(), "String");
		java_allfield_type_map.put("from_unit_name_bn".toLowerCase(), "String");
		java_allfield_type_map.put("from_organogram_name_en".toLowerCase(), "String");
		java_allfield_type_map.put("from_organogram_name_bn".toLowerCase(), "String");
		java_allfield_type_map.put("employee_name_en".toLowerCase(), "String");
		java_allfield_type_map.put("employee_name_bn".toLowerCase(), "String");

		java_anyfield_search_map.put(tableName + ".to_office_id".toLowerCase(), "Long");
		java_anyfield_search_map.put(tableName + ".to_office_name_en".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".to_office_name_bn".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".to_unit_id".toLowerCase(), "Long");
		java_anyfield_search_map.put(tableName + ".to_unit_name_en".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".to_unit_name_bn".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".to_organogram_id".toLowerCase(), "Long");
		java_anyfield_search_map.put(tableName + ".to_organogram_name_en".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".to_organogram_name_bn".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".from_office_id".toLowerCase(), "Long");
		java_anyfield_search_map.put(tableName + ".from_office_name_en".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".from_office_name_bn".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".from_unit_id".toLowerCase(), "Long");
		java_anyfield_search_map.put(tableName + ".from_unit_name_en".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".from_unit_name_bn".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".from_organogram_id".toLowerCase(), "Long");
		java_anyfield_search_map.put(tableName + ".from_organogram_name_en".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".from_organogram_name_bn".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".employee_record_id".toLowerCase(), "Long");
		java_anyfield_search_map.put(tableName + ".employee_name_en".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".employee_name_bn".toLowerCase(), "String");

		java_DTO_map.put("id".toLowerCase(), "id".toLowerCase());
		java_DTO_map.put("toOfficeId".toLowerCase(), "toOfficeId".toLowerCase());
		java_DTO_map.put("toOfficeNameEn".toLowerCase(), "toOfficeNameEn".toLowerCase());
		java_DTO_map.put("toOfficeNameBn".toLowerCase(), "toOfficeNameBn".toLowerCase());
		java_DTO_map.put("toUnitId".toLowerCase(), "toUnitId".toLowerCase());
		java_DTO_map.put("toUnitNameEn".toLowerCase(), "toUnitNameEn".toLowerCase());
		java_DTO_map.put("toUnitNameBn".toLowerCase(), "toUnitNameBn".toLowerCase());
		java_DTO_map.put("toOrganogramId".toLowerCase(), "toOrganogramId".toLowerCase());
		java_DTO_map.put("toOrganogramNameEn".toLowerCase(), "toOrganogramNameEn".toLowerCase());
		java_DTO_map.put("toOrganogramNameBn".toLowerCase(), "toOrganogramNameBn".toLowerCase());
		java_DTO_map.put("fromOfficeId".toLowerCase(), "fromOfficeId".toLowerCase());
		java_DTO_map.put("fromOfficeNameEn".toLowerCase(), "fromOfficeNameEn".toLowerCase());
		java_DTO_map.put("fromOfficeNameBn".toLowerCase(), "fromOfficeNameBn".toLowerCase());
		java_DTO_map.put("fromUnitId".toLowerCase(), "fromUnitId".toLowerCase());
		java_DTO_map.put("fromUnitNameEn".toLowerCase(), "fromUnitNameEn".toLowerCase());
		java_DTO_map.put("fromUnitNameBn".toLowerCase(), "fromUnitNameBn".toLowerCase());
		java_DTO_map.put("fromOrganogramId".toLowerCase(), "fromOrganogramId".toLowerCase());
		java_DTO_map.put("fromOrganogramNameEn".toLowerCase(), "fromOrganogramNameEn".toLowerCase());
		java_DTO_map.put("fromOrganogramNameBn".toLowerCase(), "fromOrganogramNameBn".toLowerCase());
		java_DTO_map.put("employeeRecordId".toLowerCase(), "employeeRecordId".toLowerCase());
		java_DTO_map.put("employeeNameEn".toLowerCase(), "employeeNameEn".toLowerCase());
		java_DTO_map.put("employeeNameBn".toLowerCase(), "employeeNameBn".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("to_office_name_en".toLowerCase(), "toOfficeNameEn".toLowerCase());
		java_SQL_map.put("to_office_name_bn".toLowerCase(), "toOfficeNameBn".toLowerCase());
		java_SQL_map.put("to_unit_name_en".toLowerCase(), "toUnitNameEn".toLowerCase());
		java_SQL_map.put("to_unit_name_bn".toLowerCase(), "toUnitNameBn".toLowerCase());
		java_SQL_map.put("to_organogram_name_en".toLowerCase(), "toOrganogramNameEn".toLowerCase());
		java_SQL_map.put("to_organogram_name_bn".toLowerCase(), "toOrganogramNameBn".toLowerCase());
		java_SQL_map.put("from_office_name_en".toLowerCase(), "fromOfficeNameEn".toLowerCase());
		java_SQL_map.put("from_office_name_bn".toLowerCase(), "fromOfficeNameBn".toLowerCase());
		java_SQL_map.put("from_unit_name_en".toLowerCase(), "fromUnitNameEn".toLowerCase());
		java_SQL_map.put("from_unit_name_bn".toLowerCase(), "fromUnitNameBn".toLowerCase());
		java_SQL_map.put("from_organogram_name_en".toLowerCase(), "fromOrganogramNameEn".toLowerCase());
		java_SQL_map.put("from_organogram_name_bn".toLowerCase(), "fromOrganogramNameBn".toLowerCase());
		java_SQL_map.put("employee_name_en".toLowerCase(), "employeeNameEn".toLowerCase());
		java_SQL_map.put("employee_name_bn".toLowerCase(), "employeeNameBn".toLowerCase());

		java_Text_map.put("Id".toLowerCase(), "id".toLowerCase());
		java_Text_map.put("To Office Id".toLowerCase(), "toOfficeId".toLowerCase());
		java_Text_map.put("To Office Name En".toLowerCase(), "toOfficeNameEn".toLowerCase());
		java_Text_map.put("To Office Name Bn".toLowerCase(), "toOfficeNameBn".toLowerCase());
		java_Text_map.put("To Unit Id".toLowerCase(), "toUnitId".toLowerCase());
		java_Text_map.put("To Unit Name En".toLowerCase(), "toUnitNameEn".toLowerCase());
		java_Text_map.put("To Unit Name Bn".toLowerCase(), "toUnitNameBn".toLowerCase());
		java_Text_map.put("To Organogram Id".toLowerCase(), "toOrganogramId".toLowerCase());
		java_Text_map.put("To Organogram Name En".toLowerCase(), "toOrganogramNameEn".toLowerCase());
		java_Text_map.put("To Organogram Name Bn".toLowerCase(), "toOrganogramNameBn".toLowerCase());
		java_Text_map.put("From Office Id".toLowerCase(), "fromOfficeId".toLowerCase());
		java_Text_map.put("From Office Name En".toLowerCase(), "fromOfficeNameEn".toLowerCase());
		java_Text_map.put("From Office Name Bn".toLowerCase(), "fromOfficeNameBn".toLowerCase());
		java_Text_map.put("From Unit Id".toLowerCase(), "fromUnitId".toLowerCase());
		java_Text_map.put("From Unit Name En".toLowerCase(), "fromUnitNameEn".toLowerCase());
		java_Text_map.put("From Unit Name Bn".toLowerCase(), "fromUnitNameBn".toLowerCase());
		java_Text_map.put("From Organogram Id".toLowerCase(), "fromOrganogramId".toLowerCase());
		java_Text_map.put("From Organogram Name En".toLowerCase(), "fromOrganogramNameEn".toLowerCase());
		java_Text_map.put("From Organogram Name Bn".toLowerCase(), "fromOrganogramNameBn".toLowerCase());
		java_Text_map.put("Employee Record Id".toLowerCase(), "employeeRecordId".toLowerCase());
		java_Text_map.put("Employee Name En".toLowerCase(), "employeeNameEn".toLowerCase());
		java_Text_map.put("Employee Name Bn".toLowerCase(), "employeeNameBn".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}