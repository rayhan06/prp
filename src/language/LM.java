package language;

import config.GlobalConfigConstants;
import config.GlobalConfigurationRepository;
import login.LoginDTO;
import org.apache.log4j.Logger;
import pb.Utils;
import user.UserDTO;
import user.UserRepository;
import util.CommonConstant;
import util.LockManager;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;


public class LM {
    private final static Logger logger = Logger.getLogger(LM.class);
    private Map<Long, LanguageTextDTO> mapById;
    private Map<String, LanguageTextDTO> mapByConstant;
    private final LanguageDAO languageDAO = new LanguageDAO();

    private LM() {
        reload(true);
    }

    private static class LazyLoader {
        final static LM INSTANCE = new LM();
    }

    public static LM getInstance() {
        return LazyLoader.INSTANCE;
    }

    public static String getText(long languageTextID, LoginDTO loginDTO) {
        return getText(languageTextID, UserRepository.getUserDTOByUserID(loginDTO));
    }

    public static String getText(String prefix, String constant, LoginDTO loginDTO) {
        return getText(prefix, constant, UserRepository.getUserDTOByUserID(loginDTO));
    }

    public static String getText(String prefix, String constant, UserDTO userDTO) {
        return userDTO == null ? "No constant found" :
                getInstance().getText(getLanguageIDByUserDTO(userDTO) == CommonConstant.Language_ID_English ? "English" : "Bangla", prefix, constant);
    }

    public static String getText(long languageTextID, UserDTO userDTO) {
        return userDTO == null ? "No constant found" :
                getInstance().getText(getLanguageIDByUserDTO(userDTO) == CommonConstant.Language_ID_English ? "English" : "Bangla", languageTextID);
    }

    public static String getText(long languageTextId, String language) {
        return getInstance().getText(language, languageTextId);
    }

    public static int getLanguageIDByUserDTO(UserDTO userDTO) {
        boolean isGlobalLoginEnabled = (Integer.parseInt(GlobalConfigurationRepository.getGlobalConfigDTOByID(GlobalConfigConstants.ENABLE_DEFAULT_LOGIN).value) == 1);
        long defaultUserID = Long.parseLong(GlobalConfigurationRepository.getGlobalConfigDTOByID(GlobalConfigConstants.DEFAULT_USER_ID).value);
        boolean isDefaultUser = (userDTO.ID == defaultUserID);
        if (isGlobalLoginEnabled && isDefaultUser) {
            Integer preferredLanguage = LanguagePreference.getPreferredLanguage();
            return (preferredLanguage != null && preferredLanguage == CommonConstant.Language_ID_English) ? CommonConstant.Language_ID_English
                    : CommonConstant.Language_ID_Bangla;
        } else {
            return userDTO.languageID == CommonConstant.Language_ID_English ? CommonConstant.Language_ID_English : CommonConstant.Language_ID_Bangla;
        }
    }

    public static String getLanguage(UserDTO userDTO) {
        int language = getLanguageIDByUserDTO(userDTO);
        return language == CommonConstant.Language_ID_English ? "English" : "Bangla";
    }
    
    public static String getLanguage(LoginDTO loginDTO) {
    	UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        return getLanguage(userDTO);
    }


    public static String getText(long languageTextID) {
        LanguageTextDTO languageTextDTO = getInstance().getById(languageTextID);
        if (languageTextDTO == null) {
            return "No constant found";
        }
        int defaultLanguage = Integer.parseInt(GlobalConfigurationRepository.getGlobalConfigDTOByID(GlobalConfigConstants.DEFAULT_LANGUAGE).value);
        return defaultLanguage == CommonConstant.Language_ID_English ? languageTextDTO.languageTextEnglish : languageTextDTO.languageTextBangla;
    }

    public void reload(boolean reloadAll) {
        logger.debug("Reload start for LM, reloadAll : " + reloadAll);
        List<LanguageTextDTO> dtoList = languageDAO.languageTextDTOS(reloadAll);
        mapById = dtoList.parallelStream()
                .collect(Collectors.toMap(e -> e.ID, e -> e, (e1, e2) -> e1));
        mapByConstant = dtoList.parallelStream()
                .collect(Collectors.toMap(e -> e.languageConstantPrefix + "_" + e.languageConstant, e -> e, (e1, e2) -> e1));
        logger.debug("Reload end for LM, reloadAll : " + reloadAll);
    }

    public LanguageTextDTO getById(long id) {
        if (mapById.get(id) == null) {
            synchronized (LockManager.getLock("LM" + id)) {
                if (mapById.get(id) == null) {
                    LanguageTextDTO dto = languageDAO.getLanguageTextDTO(id);
                    if (dto != null) {
                        mapById.put(dto.ID, dto);
                    }
                }
            }
        }
        return mapById.get(id);
    }

    public LanguageTextDTO getByConstant(String prefix, String constant) {
        String totalConstant = prefix + "_" + constant;
        if (mapByConstant.get(totalConstant) == null) {
            synchronized (this) {
                if (mapByConstant.get(totalConstant) == null) {
                    LanguageTextDTO dto = languageDAO.getLanguageTextDTO(prefix, constant);
                    if (dto != null) {
                        mapByConstant.put(totalConstant, dto);
                    }
                }
            }
        }
        return mapByConstant.get(totalConstant);
    }

    public String getText(String language, long id) {
        LanguageTextDTO dto = getById(id);
        return dto == null ? "No constant found" : ("English".equalsIgnoreCase(language) ? dto.languageTextEnglish : dto.languageTextBangla);
    }
    
    public static String getText(long id, boolean isLangEng) {
        LanguageTextDTO dto = getInstance().getById(id);
        return dto == null ? "No constant found" : (isLangEng ? dto.languageTextEnglish : dto.languageTextBangla);
    }

    public String getText(String language, String prefix, String constant) {
        LanguageTextDTO dto = getByConstant(prefix, constant);
        return dto == null ? "No constant found" : ("English".equalsIgnoreCase(language) ? dto.languageTextEnglish : dto.languageTextBangla);
    }

    public void add(LanguageTextDTO dto) {
        mapById.put(dto.ID, dto);
        mapByConstant.put(dto.languageConstantPrefix + "_" + dto.languageConstant, dto);
    }

    public void update(List<LanguageTextDTO> dtoList) {
        if (dtoList != null && dtoList.size() > 0) {
            dtoList.forEach(this::add);
        }
    }

    public void remove(List<String> idList) {
        if (idList != null && idList.size() > 0) {
            idList.stream()
                    .filter(Objects::nonNull)
                    .map(Utils::convertToLong)
                    .filter(Objects::nonNull)
                    .forEach(id -> {
                        if (mapById.get(id) != null) {
                            LanguageTextDTO dto = mapById.get(id);
                            mapById.remove(id);
                            mapByConstant.remove(dto.languageConstantPrefix + "_" + dto.languageConstant, dto);
                        }
                    });
        }
    }
}