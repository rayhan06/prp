package language;

/**
 * @author Kayesh Parvez
 */
public class LanguageTextDTO {
    public long ID;
    public long menuID;
    public String languageTextEnglish = "";
    public String languageTextBangla = "";
    public String languageConstantPrefix = "";
    public String languageConstant = "";
    public int isDeleted;

    @Override
    public String toString() {
        return "LanguageTextDTO{" +
                "ID=" + ID +
                ", menuID=" + menuID +
                ", languageTextEnglish='" + languageTextEnglish + '\'' +
                ", languageTextBangla='" + languageTextBangla + '\'' +
                ", languageConstantPrefix='" + languageConstantPrefix + '\'' +
                ", languageConstant='" + languageConstant + '\'' +
                ", isDeleted=" + isDeleted +
                '}';
    }
}
