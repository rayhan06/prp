package language;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class LanguageConstantGenerator {
    public static void main(String[] args) throws Exception {
        String PACKAGE_NAME = "language";
        String FILE_NAME = "LC";
        List<LanguageTextDTO> languageDTOList = new LanguageDAO().getAllDTOs();
        String fileAbsolutePath = "src/" + PACKAGE_NAME.replaceAll("\\.", "/") + "/" + FILE_NAME + ".java";
        StringBuilder stringBuilder = new StringBuilder("package " + PACKAGE_NAME + ";\n\n@SuppressWarnings(\"ALL\")\npublic interface " + FILE_NAME + "{\n");
        ArrayList<String> addedElement = new ArrayList<>();
        for (LanguageTextDTO languageDTO : languageDTOList) {
            String Constant = languageDTO.languageConstantPrefix.replaceAll(
                    "[^a-zA-Z0-9_]", "") + "_" + languageDTO.languageConstant.replaceAll(
                    "[^a-zA-Z0-9_]", "");
            Constant = Constant.replaceAll(" ", "");
            if (Constant.contains(" ") || addedElement.contains(Constant)) {
                continue;
            }
            stringBuilder.append("\tint ").append(Constant).append("=").append(languageDTO.ID).append(";\n");
            addedElement.add(Constant);
        }
        stringBuilder.append("}");
        System.out.println("File path " + fileAbsolutePath);
        PrintWriter writer = new PrintWriter(fileAbsolutePath, "UTF-8");
        writer.print(stringBuilder);
        writer.close();
    }
}


