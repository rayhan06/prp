package language;

import common.ConnectionAndStatementUtil;
import dbm.DBMW;
import login.LoginDTO;
import org.apache.log4j.Logger;
import repository.RepositoryManager;
import util.NavigationService;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

@SuppressWarnings({"rawtypes", "unchecked"})
public class LanguageDAO implements NavigationService {
    private static final Logger logger = Logger.getLogger(LanguageDAO.class.getName());

    private static final String getByIds = "SELECT * FROM language_text where id in (%s)";
    private static final String getById = "SELECT * FROM language_text where id = %d";
    private static final String getByConstant = "SELECT * FROM language_text where languageConstantPrefix = '%s' and languageConstant = '%s'";
    private static final String getAllDTOsForTrue = "SELECT * FROM language_text";
    private static final String getAllDTOsForFalse = "SELECT * FROM language_text WHERE lastModificationTime >= %d";
    private static final String addQuery = "insert into language_text(menuID,languageTextEnglish,languageTextBangla,languageConstantPrefix,languageConstant,lastModificationTime,ID) VALUES(?,?,?,?,?,?,?)";
    private static final String updateQuery = "update language_text set menuID = ?, languageTextEnglish = ?, languageTextBangla = ?, languageConstantPrefix = ?, languageConstant = ?, lastModificationTime = ? where ID = ?";
    private static final String deleteQuery = "delete from language_text where ID in (%s)";
    private static final String getAllIdSQL = "SELECT ID FROM language_text";
    private static final String getAllSQL = "SELECT * FROM language_text order by id";

    public List<LanguageTextDTO> getByIds(List<Long> idList) {
        if (idList == null || idList.size() == 0) {
            return new ArrayList<>();
        }
        return ConnectionAndStatementUtil.getDTOListByNumbers(getByIds, idList, this::buildLanguageTextDTO);
    }

    public List<LanguageTextDTO> getAllDTOs() {
        return ConnectionAndStatementUtil.getListOfT(getAllSQL, this::buildLanguageTextDTO);
    }

    private LanguageTextDTO buildLanguageTextDTO(ResultSet rs) {
        try {
            LanguageTextDTO languageTextDTO = new LanguageTextDTO();
            languageTextDTO.ID = rs.getLong("ID");
            languageTextDTO.menuID = rs.getLong("menuID");
            languageTextDTO.languageTextEnglish = rs.getString("languageTextEnglish");
            languageTextDTO.languageTextBangla = rs.getString("languageTextBangla");
            languageTextDTO.languageConstantPrefix = rs.getString("languageConstantPrefix");
            languageTextDTO.languageConstant = rs.getString("languageConstant");
            languageTextDTO.isDeleted = rs.getInt("isDeleted");
            return languageTextDTO;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    public Collection getDTOs(Collection recordIDs, LoginDTO loginDTO) {
        return getByIds(recordIDs == null ? new ArrayList<>() : (List<Long>) recordIDs);
    }

    public LanguageTextDTO getLanguageTextDTO(long id) {
        return ConnectionAndStatementUtil.getT(String.format(getById, id), this::buildLanguageTextDTO);
    }
    
    public LanguageTextDTO getLanguageTextDTO(String prefix, String constant) {
        return ConnectionAndStatementUtil.getT(String.format(getByConstant, prefix, constant), this::buildLanguageTextDTO);
    }

    public Collection getIDs(LoginDTO loginDTO) throws Exception {
        return ConnectionAndStatementUtil.getListOfT(getAllIdSQL, rs -> {
            try {
                return rs.getLong(1);
            } catch (SQLException ex) {
                ex.printStackTrace();
                return null;
            }
        });
    }

    public Collection getIDsWithSearchCriteria(Hashtable p_searchCriteria, login.LoginDTO loginDTO) throws Exception {
        boolean conditionAdded = false;
        List<Object> objectList = new ArrayList<>();
        String sql = "SELECT ID FROM language_text";
        Hashtable<String, String> searchCriteria = (Hashtable<String, String>) p_searchCriteria;

        String IDInputFromUI = searchCriteria.get("ID");
        if (IDInputFromUI != null && !IDInputFromUI.trim().equals("")) {
            sql += " WHERE  ";
            conditionAdded = true;
            sql += "ID = ? ";
            objectList.add(IDInputFromUI);
        }


        String menuIDInputFromUI = searchCriteria.get("menuID");
        if (menuIDInputFromUI != null && !menuIDInputFromUI.trim().equals("")) {
            if (conditionAdded) {
                sql += " AND  ";
            } else {
                sql += " WHERE  ";
            }
            conditionAdded = true;
            sql += "menuID = ? ";
            objectList.add(menuIDInputFromUI);
        }

        String languageTextEnglishInputFromUI = searchCriteria.get("languageTextEnglish");
        if (languageTextEnglishInputFromUI != null && !languageTextEnglishInputFromUI.trim().equals("")) {
            if (conditionAdded) {
                sql += " AND  ";
            } else {
                sql += " WHERE  ";
            }
            conditionAdded = true;
            sql += "languageTextEnglish LIKE ? ";
            objectList.add("%" + languageTextEnglishInputFromUI + "%");
        }

        String languageTextBanglaInputFromUI = searchCriteria.get("languageTextBangla");
        if (languageTextBanglaInputFromUI != null && !languageTextBanglaInputFromUI.trim().equals("")) {
            if (conditionAdded) {
                sql += " AND  ";
            } else {
                sql += " WHERE  ";
            }
            conditionAdded = true;
            sql += "languageTextBangla LIKE ? ";
            objectList.add("%" + languageTextBanglaInputFromUI + "%");
        }

        String languageConstantPrefixInputFromUI = searchCriteria.get("languageConstantPrefix");
        if (languageConstantPrefixInputFromUI != null && !languageConstantPrefixInputFromUI.trim().equals("")) {
            if (conditionAdded) {
                sql += " AND  ";
            } else {
                sql += " WHERE  ";
            }
            conditionAdded = true;
            sql += "languageConstantPrefix LIKE ? ";
            objectList.add("%" + languageConstantPrefixInputFromUI + "%");
        }

        String languageConstantInputFromUI = searchCriteria.get("languageConstant");
        if (languageConstantInputFromUI != null && !languageConstantInputFromUI.trim().equals("")) {
            if (conditionAdded) {
                sql += " AND  ";
            } else {
                sql += " WHERE  ";
            }
            sql += "languageConstant LIKE ? ";
            objectList.add("%" + languageConstantInputFromUI + "%");
        }

        return (List<Long>) ConnectionAndStatementUtil.getWritePrepareStatement(ps -> {
            List<Long> idList = new ArrayList<>();
            try {
                for (int i = 0; i < objectList.size(); i++) {
                    ps.setObject(i + 1, objectList.get(i));
                }
                ResultSet rs = ps.executeQuery();
                while (rs.next()) {
                    idList.add(rs.getLong(1));
                }
            } catch (SQLException ex) {
                logger.error(ex);
                ex.printStackTrace();
            }
            return idList;
        }, sql);
    }

    public void updateLanguage(ArrayList<LanguageTextDTO> languageTextDTOList) {
        if (languageTextDTOList == null || languageTextDTOList.size() == 0) {
            return;
        }
        ConnectionAndStatementUtil.getWritePrepareStatement(ps -> {
            try {
                for (LanguageTextDTO dto : languageTextDTOList) {
                    setPrepareStatementData(ps, dto);
                    ps.addBatch();
                }
                ps.executeBatch();
                LM.getInstance().update(languageTextDTOList);
            } catch (SQLException ex) {
                logger.error(ex);
            }
        }, updateQuery);
    }

    public void dropLanguages(String[] ids) {
        if (ids == null || ids.length == 0) {
            return;
        }
        List<String> idList = Arrays.asList(ids);
        ConnectionAndStatementUtil.executeByList(deleteQuery,idList);
        LM.getInstance().remove(idList);
    }

    public long addLanguageText(LanguageTextDTO languageTextDTO) {
        return (Long) ConnectionAndStatementUtil.getWritePrepareStatement(ps -> {
            try {
                languageTextDTO.ID = DBMW.getInstance().getNextSequenceId("language_text");
                setPrepareStatementData(ps, languageTextDTO);
                ps.execute();
                LM.getInstance().add(languageTextDTO);
                return languageTextDTO.ID;
            } catch (Exception ex) {
                logger.error(ex);
                return -1;
            }
        }, addQuery);
    }

    public void setPrepareStatementData(PreparedStatement ps, LanguageTextDTO languageTextDTO) throws SQLException {
        int index = 0;
        ps.setObject(++index, languageTextDTO.menuID);
        ps.setObject(++index, languageTextDTO.languageTextEnglish);
        ps.setObject(++index, languageTextDTO.languageTextBangla);
        ps.setObject(++index, languageTextDTO.languageConstantPrefix);
        ps.setObject(++index, languageTextDTO.languageConstant);
        ps.setObject(++index, System.currentTimeMillis());
        ps.setObject(++index, languageTextDTO.ID);
    }

    public List<LanguageTextDTO> languageTextDTOS(boolean reloadAll) {
        String sql;
        if (reloadAll) {
            sql = getAllDTOsForTrue;
        } else {
            sql = String.format(getAllDTOsForFalse, RepositoryManager.lastModifyTime);
        }
        return ConnectionAndStatementUtil.getListOfT(sql, this::buildLanguageTextDTO);
    }
}