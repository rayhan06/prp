package asset_clearance_letter;


import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.sql.SQLException;
import common.ConnectionAndStatementUtil;
import org.apache.log4j.Logger;
import repository.RepositoryManager;
import util.*;
import pb.*;
import pb_notifications.Pb_notificationsDAO;
import user.UserDTO;


public class Asset_clearance_letterDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());

	public Asset_clearance_letterDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";
		commonMaps = new Asset_clearance_letterMAPS(tableName);
		columnNames = new String[] 
		{
			"ID",
			"organogram_id",
			"signing_organogram_id",
			"user_name",
			"request_date",
			"noc_date",
			"letter_number",
			"code_number",
			"u_o_note_number",
			"response_required_date",
			"clearance_status_cat",
			"email_declaration_status_cat",
			"directory_login_status_cat",
			"parliament_software_login_status_cat",
			"report_server_status_cat",
			"it_asset_handover_status_cat",
			"support_ticket_id",
			"employee_record_id",
			"remarks",
			"inserted_by_user_id",
			"inserted_by_organogram_id",
			"insertion_date",
			"search_column",
			"isDeleted",
			"lastModificationTime"
		};
	}
	
	public Asset_clearance_letterDAO()
	{
		this("asset_clearance_letter");		
	}
	
	public void setSearchColumn(Asset_clearance_letterDTO asset_clearance_letterDTO)
	{
		asset_clearance_letterDTO.searchColumn = "";
		asset_clearance_letterDTO.searchColumn += asset_clearance_letterDTO.userName + " ";
		asset_clearance_letterDTO.searchColumn += CatDAO.getName("English", "clearance_status", asset_clearance_letterDTO.clearanceStatusCat) + " " + CatDAO.getName("Bangla", "clearance_status", asset_clearance_letterDTO.clearanceStatusCat) + " ";
		asset_clearance_letterDTO.searchColumn += CatDAO.getName("English", "email_declaration_status", asset_clearance_letterDTO.emailDeclarationStatusCat) + " " + CatDAO.getName("Bangla", "email_declaration_status", asset_clearance_letterDTO.emailDeclarationStatusCat) + " ";
		asset_clearance_letterDTO.searchColumn += CatDAO.getName("English", "directory_login_status", asset_clearance_letterDTO.directoryLoginStatusCat) + " " + CatDAO.getName("Bangla", "directory_login_status", asset_clearance_letterDTO.directoryLoginStatusCat) + " ";
		asset_clearance_letterDTO.searchColumn += CatDAO.getName("English", "parliament_software_login_status", asset_clearance_letterDTO.parliamentSoftwareLoginStatusCat) + " " + CatDAO.getName("Bangla", "parliament_software_login_status", asset_clearance_letterDTO.parliamentSoftwareLoginStatusCat) + " ";
		asset_clearance_letterDTO.searchColumn += CatDAO.getName("English", "report_server_status", asset_clearance_letterDTO.reportServerStatusCat) + " " + CatDAO.getName("Bangla", "report_server_status", asset_clearance_letterDTO.reportServerStatusCat) + " ";
		asset_clearance_letterDTO.searchColumn += CatDAO.getName("English", "it_asset_handover_status", asset_clearance_letterDTO.itAssetHandoverStatusCat) + " " + CatDAO.getName("Bangla", "it_asset_handover_status", asset_clearance_letterDTO.itAssetHandoverStatusCat) + " ";
		asset_clearance_letterDTO.searchColumn += asset_clearance_letterDTO.remarks + " ";
	}
	
	public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException
	{
		Asset_clearance_letterDTO asset_clearance_letterDTO = (Asset_clearance_letterDTO)commonDTO;
		int index = 1;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(asset_clearance_letterDTO);
		if(isInsert)
		{
			ps.setObject(index++,asset_clearance_letterDTO.iD);
		}
		ps.setObject(index++,asset_clearance_letterDTO.organogramId);
		ps.setObject(index++,asset_clearance_letterDTO.signingOrganogramId);
		ps.setObject(index++,asset_clearance_letterDTO.userName);
		
		ps.setObject(index++,asset_clearance_letterDTO.requestDate);
		ps.setObject(index++,asset_clearance_letterDTO.nocDate);
		
		ps.setObject(index++,asset_clearance_letterDTO.letterNumber);
		ps.setObject(index++,asset_clearance_letterDTO.codeNumber);
		ps.setObject(index++,asset_clearance_letterDTO.uONoteNumber);
		
		ps.setObject(index++,asset_clearance_letterDTO.responseDate);
		ps.setObject(index++,asset_clearance_letterDTO.clearanceStatusCat);
		ps.setObject(index++,asset_clearance_letterDTO.emailDeclarationStatusCat);
		ps.setObject(index++,asset_clearance_letterDTO.directoryLoginStatusCat);
		ps.setObject(index++,asset_clearance_letterDTO.parliamentSoftwareLoginStatusCat);
		ps.setObject(index++,asset_clearance_letterDTO.reportServerStatusCat);
		ps.setObject(index++,asset_clearance_letterDTO.itAssetHandoverStatusCat);
		ps.setObject(index++,asset_clearance_letterDTO.supportTicketId);
		ps.setObject(index++,asset_clearance_letterDTO.employeeRecordId);
		ps.setObject(index++,asset_clearance_letterDTO.remarks);
		ps.setObject(index++,asset_clearance_letterDTO.insertedByUserId);
		ps.setObject(index++,asset_clearance_letterDTO.insertedByOrganogramId);
		ps.setObject(index++,asset_clearance_letterDTO.insertionDate);
		ps.setObject(index++,asset_clearance_letterDTO.searchColumn);
		if(isInsert)
		{
			ps.setObject(index++, 0);
			ps.setObject(index++, lastModificationTime);
		}
	}
	
	public Asset_clearance_letterDTO build(ResultSet rs)
	{
		try
		{
			Asset_clearance_letterDTO asset_clearance_letterDTO = new Asset_clearance_letterDTO();
			asset_clearance_letterDTO.iD = rs.getLong("ID");
			asset_clearance_letterDTO.organogramId = rs.getLong("organogram_id");
			asset_clearance_letterDTO.signingOrganogramId = rs.getLong("signing_organogram_id");
			
			asset_clearance_letterDTO.userName = rs.getString("user_name");
			asset_clearance_letterDTO.requestDate = rs.getLong("request_date");
			asset_clearance_letterDTO.nocDate = rs.getLong("noc_date");
			
			asset_clearance_letterDTO.letterNumber = rs.getString("letter_number");
			asset_clearance_letterDTO.codeNumber = rs.getString("code_number");
			asset_clearance_letterDTO.uONoteNumber = rs.getString("u_o_note_number");
			
			asset_clearance_letterDTO.responseDate = rs.getLong("response_required_date");
			asset_clearance_letterDTO.clearanceStatusCat = rs.getInt("clearance_status_cat");
			asset_clearance_letterDTO.emailDeclarationStatusCat = rs.getInt("email_declaration_status_cat");
			asset_clearance_letterDTO.directoryLoginStatusCat = rs.getInt("directory_login_status_cat");
			asset_clearance_letterDTO.parliamentSoftwareLoginStatusCat = rs.getInt("parliament_software_login_status_cat");
			asset_clearance_letterDTO.reportServerStatusCat = rs.getInt("report_server_status_cat");
			asset_clearance_letterDTO.itAssetHandoverStatusCat = rs.getInt("it_asset_handover_status_cat");
			asset_clearance_letterDTO.supportTicketId = rs.getLong("support_ticket_id");
			asset_clearance_letterDTO.employeeRecordId = rs.getLong("employee_record_id");
			asset_clearance_letterDTO.remarks = rs.getString("remarks");
			asset_clearance_letterDTO.insertedByUserId = rs.getLong("inserted_by_user_id");
			asset_clearance_letterDTO.insertedByOrganogramId = rs.getLong("inserted_by_organogram_id");
			asset_clearance_letterDTO.insertionDate = rs.getLong("insertion_date");
			asset_clearance_letterDTO.searchColumn = rs.getString("search_column");
			asset_clearance_letterDTO.isDeleted = rs.getInt("isDeleted");
			asset_clearance_letterDTO.lastModificationTime = rs.getLong("lastModificationTime");
			return asset_clearance_letterDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
	
	public void sendNoti(Asset_clearance_letterDTO asset_clearance_letterDTO)
	{

		Pb_notificationsDAO pb_notificationsDAO = Pb_notificationsDAO.getInstance();

		String englishText = "Employee #" + asset_clearance_letterDTO.userName
				+ " received asset clearance";
		String banglaText = "কর্মকর্তা #" 
				+ asset_clearance_letterDTO.userName
				+ " সম্পদের ক্লিয়ারেন্স পেয়েছেন";


        String URL =  "Asset_clearance_letterServlet?actionType=view&ID=" + asset_clearance_letterDTO.iD;

		pb_notificationsDAO.addPb_notificationsAndSendMailSMS(asset_clearance_letterDTO.insertedByOrganogramId, System.currentTimeMillis(), URL, 
				englishText, banglaText, "NOC");
	}
		
		
	
	
	

	public Asset_clearance_letterDTO getDTOByID (long id)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID = " + id;
		Asset_clearance_letterDTO asset_clearance_letterDTO = ConnectionAndStatementUtil.getT(sql,this::build);
		return asset_clearance_letterDTO;
	}
	
	public Asset_clearance_letterDTO getBySupportTicketId (long support_ticket_id)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE isDeleted = 0 and support_ticket_id = " + support_ticket_id;
		Asset_clearance_letterDTO asset_clearance_letterDTO = ConnectionAndStatementUtil.getT(sql,this::build);
		return asset_clearance_letterDTO;
	}

	
	public List<Asset_clearance_letterDTO> getDTOs(Collection recordIDs)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID IN ( ";
		for(int i = 0;i<recordIDs.size();i++){
			if(i!=0){
				sql+=",";
			}
			sql+=((ArrayList)recordIDs).get(i);
		}
		sql+=")  order by lastModificationTime desc";
		printSql(sql);
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);		
	}
	
	public List<Asset_clearance_letterDTO> getAllAsset_clearance_letter (boolean isFirstReload)
    {
		String sql = "SELECT * FROM " + tableName + " WHERE ";
		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by " + tableName + ".lastModificationTime desc";
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);	
    }

	
	public List<Asset_clearance_letterDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO)
	{
		return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
	}

	
	public List<Asset_clearance_letterDTO> getDTOs(Hashtable  p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
			String filter, boolean tableHasJobCat)
	{
		String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat);
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);			
	}
	public String getSqlWithSearchCriteriaForNormalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
			UserDTO userDTO, String filter, boolean tableHasJobCat)
    {
		boolean viewAll = false;
		
		if(p_searchCriteria != null && p_searchCriteria.get("ViewAll") != null)
		{
			System.out.println("ViewAll = " + p_searchCriteria.get("ViewAll"));
			viewAll = true;
		}
		else
		{
			System.out.println("ViewAll is null ");
		}
		
		String sql = "SELECT ";
		if(category == GETIDS)
		{
			sql += " " + tableName + ".ID as ID ";
		}
		else if(category == GETCOUNT)
		{
			sql += " count( " + tableName + ".ID) as countID ";
		}
		else if(category == GETDTOS)
		{
			sql += " distinct " + tableName + ".* ";
		}
		sql += "FROM " + tableName + " ";
		sql += joinSQL;
		

		
		String AnyfieldSql = "";
		String AllFieldSql = "";
		
		if(p_searchCriteria != null)
		{
						
			
			Enumeration names = p_searchCriteria.keys();
			String str, value;
			
			AnyfieldSql = "(";
			
			if(p_searchCriteria.get("AnyField")!= null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase(""))
			{
				AnyfieldSql+= tableName + ".search_column like '%" + p_searchCriteria.get("AnyField").toString() + "%'";			
			}
			AnyfieldSql += ")";
			System.out.println("AnyfieldSql = " + AnyfieldSql);
			
			AllFieldSql = "(";
			int i = 0;
			while(names.hasMoreElements())
			{				
				str = (String) names.nextElement();
				value = (String)p_searchCriteria.get(str);
		        System.out.println(str + ": " + value);
				if(value != null && !value.equalsIgnoreCase("") && (
						 str.equals("user_name")
						|| str.equals("request_date_start")
						|| str.equals("request_date_end")
						|| str.equals("response_required_date_start")
						|| str.equals("response_required_date_end")
						|| str.equals("clearance_status_cat")
						|| str.equals("email_declaration_status_cat")
						|| str.equals("directory_login_status_cat")
						|| str.equals("parliament_software_login_status_cat")
						|| str.equals("report_server_status_cat")
						|| str.equals("it_asset_handover_status_cat")
						|| str.equals("remarks")
						|| str.equals("insertion_date_start")
						|| str.equals("insertion_date_end")
				)
						
				)
				{
					if(p_searchCriteria.get(str).equals("any"))
					{
						continue;
					}

					if( i > 0)
					{
						AllFieldSql+= " AND  ";
					}
					
					 if(str.equals("user_name"))
					{
						AllFieldSql += "" + tableName + ".user_name like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("request_date_start"))
					{
						AllFieldSql += "" + tableName + ".request_date >= " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("request_date_end"))
					{
						AllFieldSql += "" + tableName + ".request_date <= " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("response_required_date_start"))
					{
						AllFieldSql += "" + tableName + ".response_required_date >= " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("response_required_date_end"))
					{
						AllFieldSql += "" + tableName + ".response_required_date <= " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("clearance_status_cat"))
					{
						AllFieldSql += "" + tableName + ".clearance_status_cat = " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("email_declaration_status_cat"))
					{
						AllFieldSql += "" + tableName + ".email_declaration_status_cat = " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("directory_login_status_cat"))
					{
						AllFieldSql += "" + tableName + ".directory_login_status_cat = " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("parliament_software_login_status_cat"))
					{
						AllFieldSql += "" + tableName + ".parliament_software_login_status_cat = " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("report_server_status_cat"))
					{
						AllFieldSql += "" + tableName + ".report_server_status_cat = " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("it_asset_handover_status_cat"))
					{
						AllFieldSql += "" + tableName + ".it_asset_handover_status_cat = " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("remarks"))
					{
						AllFieldSql += "" + tableName + ".remarks like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("insertion_date_start"))
					{
						AllFieldSql += "" + tableName + ".insertion_date >= " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("insertion_date_end"))
					{
						AllFieldSql += "" + tableName + ".insertion_date <= " + p_searchCriteria.get(str);
						i ++;
					}
					
					
				}
			}
			
			AllFieldSql += ")";
			System.out.println("AllFieldSql = " + AllFieldSql);
			
			
		}
		
		
		sql += " WHERE ";
		
		sql += " (" + tableName + ".isDeleted = 0 ";			
		sql += ")";
		
		
		if(!filter.equalsIgnoreCase(""))
		{
			sql += " and " + filter + " ";
		}
		
		if(!AnyfieldSql.equals("()") && !AnyfieldSql.equals(""))
		{
			sql += " AND " + AnyfieldSql;
			
		}
		if(!AllFieldSql.equals("()") && !AllFieldSql.equals(""))
		{			
			sql += " AND " + AllFieldSql;
		}
		
	
			
		sql += " order by " + tableName + ".lastModificationTime desc ";

		printSql(sql);
		
		if(limit >= 0)
		{
			sql += " limit " + limit;
		}
		if(offset >= 0)
		{
			sql += " offset " + offset;
		}
		
		System.out.println("-------------- sql = " + sql);
		
		return sql;
    }
	
	public String getSqlWithSearchCriteria(Hashtable p_searchCriteria, int limit, int offset, int category,
			boolean isPermanentTable, UserDTO userDTO, String filter, boolean tableHasJobCat)
    {		
		return getSqlWithSearchCriteriaForNormalSearch(p_searchCriteria, limit, offset, category, userDTO, filter, tableHasJobCat);	
    }
				
}
	