package asset_clearance_letter;
import java.util.*; 
import util.*;


public class Asset_clearance_letterMAPS extends CommonMaps
{	
	public Asset_clearance_letterMAPS(String tableName)
	{
		

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("organogramId".toLowerCase(), "organogramId".toLowerCase());
		java_DTO_map.put("userName".toLowerCase(), "userName".toLowerCase());
		java_DTO_map.put("requestDate".toLowerCase(), "requestDate".toLowerCase());
		java_DTO_map.put("responseRequiredDate".toLowerCase(), "responseRequiredDate".toLowerCase());
		java_DTO_map.put("clearanceStatusCat".toLowerCase(), "clearanceStatusCat".toLowerCase());
		java_DTO_map.put("emailDeclarationStatusCat".toLowerCase(), "emailDeclarationStatusCat".toLowerCase());
		java_DTO_map.put("directoryLoginStatusCat".toLowerCase(), "directoryLoginStatusCat".toLowerCase());
		java_DTO_map.put("parliamentSoftwareLoginStatusCat".toLowerCase(), "parliamentSoftwareLoginStatusCat".toLowerCase());
		java_DTO_map.put("reportServerStatusCat".toLowerCase(), "reportServerStatusCat".toLowerCase());
		java_DTO_map.put("itAssetHandoverStatusCat".toLowerCase(), "itAssetHandoverStatusCat".toLowerCase());
		java_DTO_map.put("remarks".toLowerCase(), "remarks".toLowerCase());
		java_DTO_map.put("insertedByUserId".toLowerCase(), "insertedByUserId".toLowerCase());
		java_DTO_map.put("insertedByOrganogramId".toLowerCase(), "insertedByOrganogramId".toLowerCase());
		java_DTO_map.put("insertionDate".toLowerCase(), "insertionDate".toLowerCase());
		java_DTO_map.put("searchColumn".toLowerCase(), "searchColumn".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("organogram_id".toLowerCase(), "organogramId".toLowerCase());
		java_SQL_map.put("user_name".toLowerCase(), "userName".toLowerCase());
		java_SQL_map.put("request_date".toLowerCase(), "requestDate".toLowerCase());
		java_SQL_map.put("response_required_date".toLowerCase(), "responseRequiredDate".toLowerCase());
		java_SQL_map.put("clearance_status_cat".toLowerCase(), "clearanceStatusCat".toLowerCase());
		java_SQL_map.put("email_declaration_status_cat".toLowerCase(), "emailDeclarationStatusCat".toLowerCase());
		java_SQL_map.put("directory_login_status_cat".toLowerCase(), "directoryLoginStatusCat".toLowerCase());
		java_SQL_map.put("parliament_software_login_status_cat".toLowerCase(), "parliamentSoftwareLoginStatusCat".toLowerCase());
		java_SQL_map.put("report_server_status_cat".toLowerCase(), "reportServerStatusCat".toLowerCase());
		java_SQL_map.put("it_asset_handover_status_cat".toLowerCase(), "itAssetHandoverStatusCat".toLowerCase());
		java_SQL_map.put("remarks".toLowerCase(), "remarks".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Organogram Id".toLowerCase(), "organogramId".toLowerCase());
		java_Text_map.put("User Name".toLowerCase(), "userName".toLowerCase());
		java_Text_map.put("Request Date".toLowerCase(), "requestDate".toLowerCase());
		java_Text_map.put("Response Required Date".toLowerCase(), "responseRequiredDate".toLowerCase());
		java_Text_map.put("Clearance Status".toLowerCase(), "clearanceStatusCat".toLowerCase());
		java_Text_map.put("Email Declaration Status".toLowerCase(), "emailDeclarationStatusCat".toLowerCase());
		java_Text_map.put("Directory Login Status".toLowerCase(), "directoryLoginStatusCat".toLowerCase());
		java_Text_map.put("Parliament Software Login Status".toLowerCase(), "parliamentSoftwareLoginStatusCat".toLowerCase());
		java_Text_map.put("Report Server Status".toLowerCase(), "reportServerStatusCat".toLowerCase());
		java_Text_map.put("It Asset Handover Status".toLowerCase(), "itAssetHandoverStatusCat".toLowerCase());
		java_Text_map.put("Remarks".toLowerCase(), "remarks".toLowerCase());
		java_Text_map.put("Inserted By User Id".toLowerCase(), "insertedByUserId".toLowerCase());
		java_Text_map.put("Inserted By Organogram Id".toLowerCase(), "insertedByOrganogramId".toLowerCase());
		java_Text_map.put("Insertion Date".toLowerCase(), "insertionDate".toLowerCase());
		java_Text_map.put("Search Column".toLowerCase(), "searchColumn".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}