package asset_clearance_letter;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class Asset_clearance_letterRepository implements Repository {
	Asset_clearance_letterDAO asset_clearance_letterDAO = null;
	
	public void setDAO(Asset_clearance_letterDAO asset_clearance_letterDAO)
	{
		this.asset_clearance_letterDAO = asset_clearance_letterDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Asset_clearance_letterRepository.class);
	Map<Long, Asset_clearance_letterDTO>mapOfAsset_clearance_letterDTOToiD;
	Map<Long, Set<Asset_clearance_letterDTO> >mapOfAsset_clearance_letterDTOToorganogramId;
	Map<String, Set<Asset_clearance_letterDTO> >mapOfAsset_clearance_letterDTOTouserName;
	Map<Long, Set<Asset_clearance_letterDTO> >mapOfAsset_clearance_letterDTOTorequestDate;
	Map<Long, Set<Asset_clearance_letterDTO> >mapOfAsset_clearance_letterDTOToresponseRequiredDate;
	Map<Integer, Set<Asset_clearance_letterDTO> >mapOfAsset_clearance_letterDTOToclearanceStatusCat;
	Map<Integer, Set<Asset_clearance_letterDTO> >mapOfAsset_clearance_letterDTOToemailDeclarationStatusCat;
	Map<Integer, Set<Asset_clearance_letterDTO> >mapOfAsset_clearance_letterDTOTodirectoryLoginStatusCat;
	Map<Integer, Set<Asset_clearance_letterDTO> >mapOfAsset_clearance_letterDTOToparliamentSoftwareLoginStatusCat;
	Map<Integer, Set<Asset_clearance_letterDTO> >mapOfAsset_clearance_letterDTOToreportServerStatusCat;
	Map<Integer, Set<Asset_clearance_letterDTO> >mapOfAsset_clearance_letterDTOToitAssetHandoverStatusCat;
	Map<String, Set<Asset_clearance_letterDTO> >mapOfAsset_clearance_letterDTOToremarks;
	Map<Long, Set<Asset_clearance_letterDTO> >mapOfAsset_clearance_letterDTOToinsertedByUserId;
	Map<Long, Set<Asset_clearance_letterDTO> >mapOfAsset_clearance_letterDTOToinsertedByOrganogramId;
	Map<Long, Set<Asset_clearance_letterDTO> >mapOfAsset_clearance_letterDTOToinsertionDate;
	Map<String, Set<Asset_clearance_letterDTO> >mapOfAsset_clearance_letterDTOTosearchColumn;
	Map<Long, Set<Asset_clearance_letterDTO> >mapOfAsset_clearance_letterDTOTolastModificationTime;


	static Asset_clearance_letterRepository instance = null;  
	private Asset_clearance_letterRepository(){
		mapOfAsset_clearance_letterDTOToiD = new ConcurrentHashMap<>();
		mapOfAsset_clearance_letterDTOToorganogramId = new ConcurrentHashMap<>();
		mapOfAsset_clearance_letterDTOTouserName = new ConcurrentHashMap<>();
		mapOfAsset_clearance_letterDTOTorequestDate = new ConcurrentHashMap<>();
		mapOfAsset_clearance_letterDTOToresponseRequiredDate = new ConcurrentHashMap<>();
		mapOfAsset_clearance_letterDTOToclearanceStatusCat = new ConcurrentHashMap<>();
		mapOfAsset_clearance_letterDTOToemailDeclarationStatusCat = new ConcurrentHashMap<>();
		mapOfAsset_clearance_letterDTOTodirectoryLoginStatusCat = new ConcurrentHashMap<>();
		mapOfAsset_clearance_letterDTOToparliamentSoftwareLoginStatusCat = new ConcurrentHashMap<>();
		mapOfAsset_clearance_letterDTOToreportServerStatusCat = new ConcurrentHashMap<>();
		mapOfAsset_clearance_letterDTOToitAssetHandoverStatusCat = new ConcurrentHashMap<>();
		mapOfAsset_clearance_letterDTOToremarks = new ConcurrentHashMap<>();
		mapOfAsset_clearance_letterDTOToinsertedByUserId = new ConcurrentHashMap<>();
		mapOfAsset_clearance_letterDTOToinsertedByOrganogramId = new ConcurrentHashMap<>();
		mapOfAsset_clearance_letterDTOToinsertionDate = new ConcurrentHashMap<>();
		mapOfAsset_clearance_letterDTOTosearchColumn = new ConcurrentHashMap<>();
		mapOfAsset_clearance_letterDTOTolastModificationTime = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Asset_clearance_letterRepository getInstance(){
		if (instance == null){
			instance = new Asset_clearance_letterRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(asset_clearance_letterDAO == null)
		{
			return;
		}
		try {
			List<Asset_clearance_letterDTO> asset_clearance_letterDTOs = asset_clearance_letterDAO.getAllAsset_clearance_letter(reloadAll);
			for(Asset_clearance_letterDTO asset_clearance_letterDTO : asset_clearance_letterDTOs) {
				Asset_clearance_letterDTO oldAsset_clearance_letterDTO = getAsset_clearance_letterDTOByID(asset_clearance_letterDTO.iD);
				if( oldAsset_clearance_letterDTO != null ) {
					mapOfAsset_clearance_letterDTOToiD.remove(oldAsset_clearance_letterDTO.iD);
				
					if(mapOfAsset_clearance_letterDTOToorganogramId.containsKey(oldAsset_clearance_letterDTO.organogramId)) {
						mapOfAsset_clearance_letterDTOToorganogramId.get(oldAsset_clearance_letterDTO.organogramId).remove(oldAsset_clearance_letterDTO);
					}
					if(mapOfAsset_clearance_letterDTOToorganogramId.get(oldAsset_clearance_letterDTO.organogramId).isEmpty()) {
						mapOfAsset_clearance_letterDTOToorganogramId.remove(oldAsset_clearance_letterDTO.organogramId);
					}
					
					if(mapOfAsset_clearance_letterDTOTouserName.containsKey(oldAsset_clearance_letterDTO.userName)) {
						mapOfAsset_clearance_letterDTOTouserName.get(oldAsset_clearance_letterDTO.userName).remove(oldAsset_clearance_letterDTO);
					}
					if(mapOfAsset_clearance_letterDTOTouserName.get(oldAsset_clearance_letterDTO.userName).isEmpty()) {
						mapOfAsset_clearance_letterDTOTouserName.remove(oldAsset_clearance_letterDTO.userName);
					}
					
					if(mapOfAsset_clearance_letterDTOTorequestDate.containsKey(oldAsset_clearance_letterDTO.requestDate)) {
						mapOfAsset_clearance_letterDTOTorequestDate.get(oldAsset_clearance_letterDTO.requestDate).remove(oldAsset_clearance_letterDTO);
					}
					if(mapOfAsset_clearance_letterDTOTorequestDate.get(oldAsset_clearance_letterDTO.requestDate).isEmpty()) {
						mapOfAsset_clearance_letterDTOTorequestDate.remove(oldAsset_clearance_letterDTO.requestDate);
					}
					
					if(mapOfAsset_clearance_letterDTOToresponseRequiredDate.containsKey(oldAsset_clearance_letterDTO.responseDate)) {
						mapOfAsset_clearance_letterDTOToresponseRequiredDate.get(oldAsset_clearance_letterDTO.responseDate).remove(oldAsset_clearance_letterDTO);
					}
					if(mapOfAsset_clearance_letterDTOToresponseRequiredDate.get(oldAsset_clearance_letterDTO.responseDate).isEmpty()) {
						mapOfAsset_clearance_letterDTOToresponseRequiredDate.remove(oldAsset_clearance_letterDTO.responseDate);
					}
					
					if(mapOfAsset_clearance_letterDTOToclearanceStatusCat.containsKey(oldAsset_clearance_letterDTO.clearanceStatusCat)) {
						mapOfAsset_clearance_letterDTOToclearanceStatusCat.get(oldAsset_clearance_letterDTO.clearanceStatusCat).remove(oldAsset_clearance_letterDTO);
					}
					if(mapOfAsset_clearance_letterDTOToclearanceStatusCat.get(oldAsset_clearance_letterDTO.clearanceStatusCat).isEmpty()) {
						mapOfAsset_clearance_letterDTOToclearanceStatusCat.remove(oldAsset_clearance_letterDTO.clearanceStatusCat);
					}
					
					if(mapOfAsset_clearance_letterDTOToemailDeclarationStatusCat.containsKey(oldAsset_clearance_letterDTO.emailDeclarationStatusCat)) {
						mapOfAsset_clearance_letterDTOToemailDeclarationStatusCat.get(oldAsset_clearance_letterDTO.emailDeclarationStatusCat).remove(oldAsset_clearance_letterDTO);
					}
					if(mapOfAsset_clearance_letterDTOToemailDeclarationStatusCat.get(oldAsset_clearance_letterDTO.emailDeclarationStatusCat).isEmpty()) {
						mapOfAsset_clearance_letterDTOToemailDeclarationStatusCat.remove(oldAsset_clearance_letterDTO.emailDeclarationStatusCat);
					}
					
					if(mapOfAsset_clearance_letterDTOTodirectoryLoginStatusCat.containsKey(oldAsset_clearance_letterDTO.directoryLoginStatusCat)) {
						mapOfAsset_clearance_letterDTOTodirectoryLoginStatusCat.get(oldAsset_clearance_letterDTO.directoryLoginStatusCat).remove(oldAsset_clearance_letterDTO);
					}
					if(mapOfAsset_clearance_letterDTOTodirectoryLoginStatusCat.get(oldAsset_clearance_letterDTO.directoryLoginStatusCat).isEmpty()) {
						mapOfAsset_clearance_letterDTOTodirectoryLoginStatusCat.remove(oldAsset_clearance_letterDTO.directoryLoginStatusCat);
					}
					
					if(mapOfAsset_clearance_letterDTOToparliamentSoftwareLoginStatusCat.containsKey(oldAsset_clearance_letterDTO.parliamentSoftwareLoginStatusCat)) {
						mapOfAsset_clearance_letterDTOToparliamentSoftwareLoginStatusCat.get(oldAsset_clearance_letterDTO.parliamentSoftwareLoginStatusCat).remove(oldAsset_clearance_letterDTO);
					}
					if(mapOfAsset_clearance_letterDTOToparliamentSoftwareLoginStatusCat.get(oldAsset_clearance_letterDTO.parliamentSoftwareLoginStatusCat).isEmpty()) {
						mapOfAsset_clearance_letterDTOToparliamentSoftwareLoginStatusCat.remove(oldAsset_clearance_letterDTO.parliamentSoftwareLoginStatusCat);
					}
					
					if(mapOfAsset_clearance_letterDTOToreportServerStatusCat.containsKey(oldAsset_clearance_letterDTO.reportServerStatusCat)) {
						mapOfAsset_clearance_letterDTOToreportServerStatusCat.get(oldAsset_clearance_letterDTO.reportServerStatusCat).remove(oldAsset_clearance_letterDTO);
					}
					if(mapOfAsset_clearance_letterDTOToreportServerStatusCat.get(oldAsset_clearance_letterDTO.reportServerStatusCat).isEmpty()) {
						mapOfAsset_clearance_letterDTOToreportServerStatusCat.remove(oldAsset_clearance_letterDTO.reportServerStatusCat);
					}
					
					if(mapOfAsset_clearance_letterDTOToitAssetHandoverStatusCat.containsKey(oldAsset_clearance_letterDTO.itAssetHandoverStatusCat)) {
						mapOfAsset_clearance_letterDTOToitAssetHandoverStatusCat.get(oldAsset_clearance_letterDTO.itAssetHandoverStatusCat).remove(oldAsset_clearance_letterDTO);
					}
					if(mapOfAsset_clearance_letterDTOToitAssetHandoverStatusCat.get(oldAsset_clearance_letterDTO.itAssetHandoverStatusCat).isEmpty()) {
						mapOfAsset_clearance_letterDTOToitAssetHandoverStatusCat.remove(oldAsset_clearance_letterDTO.itAssetHandoverStatusCat);
					}
					
					if(mapOfAsset_clearance_letterDTOToremarks.containsKey(oldAsset_clearance_letterDTO.remarks)) {
						mapOfAsset_clearance_letterDTOToremarks.get(oldAsset_clearance_letterDTO.remarks).remove(oldAsset_clearance_letterDTO);
					}
					if(mapOfAsset_clearance_letterDTOToremarks.get(oldAsset_clearance_letterDTO.remarks).isEmpty()) {
						mapOfAsset_clearance_letterDTOToremarks.remove(oldAsset_clearance_letterDTO.remarks);
					}
					
					if(mapOfAsset_clearance_letterDTOToinsertedByUserId.containsKey(oldAsset_clearance_letterDTO.insertedByUserId)) {
						mapOfAsset_clearance_letterDTOToinsertedByUserId.get(oldAsset_clearance_letterDTO.insertedByUserId).remove(oldAsset_clearance_letterDTO);
					}
					if(mapOfAsset_clearance_letterDTOToinsertedByUserId.get(oldAsset_clearance_letterDTO.insertedByUserId).isEmpty()) {
						mapOfAsset_clearance_letterDTOToinsertedByUserId.remove(oldAsset_clearance_letterDTO.insertedByUserId);
					}
					
					if(mapOfAsset_clearance_letterDTOToinsertedByOrganogramId.containsKey(oldAsset_clearance_letterDTO.insertedByOrganogramId)) {
						mapOfAsset_clearance_letterDTOToinsertedByOrganogramId.get(oldAsset_clearance_letterDTO.insertedByOrganogramId).remove(oldAsset_clearance_letterDTO);
					}
					if(mapOfAsset_clearance_letterDTOToinsertedByOrganogramId.get(oldAsset_clearance_letterDTO.insertedByOrganogramId).isEmpty()) {
						mapOfAsset_clearance_letterDTOToinsertedByOrganogramId.remove(oldAsset_clearance_letterDTO.insertedByOrganogramId);
					}
					
					if(mapOfAsset_clearance_letterDTOToinsertionDate.containsKey(oldAsset_clearance_letterDTO.insertionDate)) {
						mapOfAsset_clearance_letterDTOToinsertionDate.get(oldAsset_clearance_letterDTO.insertionDate).remove(oldAsset_clearance_letterDTO);
					}
					if(mapOfAsset_clearance_letterDTOToinsertionDate.get(oldAsset_clearance_letterDTO.insertionDate).isEmpty()) {
						mapOfAsset_clearance_letterDTOToinsertionDate.remove(oldAsset_clearance_letterDTO.insertionDate);
					}
					
					if(mapOfAsset_clearance_letterDTOTosearchColumn.containsKey(oldAsset_clearance_letterDTO.searchColumn)) {
						mapOfAsset_clearance_letterDTOTosearchColumn.get(oldAsset_clearance_letterDTO.searchColumn).remove(oldAsset_clearance_letterDTO);
					}
					if(mapOfAsset_clearance_letterDTOTosearchColumn.get(oldAsset_clearance_letterDTO.searchColumn).isEmpty()) {
						mapOfAsset_clearance_letterDTOTosearchColumn.remove(oldAsset_clearance_letterDTO.searchColumn);
					}
					
					if(mapOfAsset_clearance_letterDTOTolastModificationTime.containsKey(oldAsset_clearance_letterDTO.lastModificationTime)) {
						mapOfAsset_clearance_letterDTOTolastModificationTime.get(oldAsset_clearance_letterDTO.lastModificationTime).remove(oldAsset_clearance_letterDTO);
					}
					if(mapOfAsset_clearance_letterDTOTolastModificationTime.get(oldAsset_clearance_letterDTO.lastModificationTime).isEmpty()) {
						mapOfAsset_clearance_letterDTOTolastModificationTime.remove(oldAsset_clearance_letterDTO.lastModificationTime);
					}
					
					
				}
				if(asset_clearance_letterDTO.isDeleted == 0) 
				{
					
					mapOfAsset_clearance_letterDTOToiD.put(asset_clearance_letterDTO.iD, asset_clearance_letterDTO);
				
					if( ! mapOfAsset_clearance_letterDTOToorganogramId.containsKey(asset_clearance_letterDTO.organogramId)) {
						mapOfAsset_clearance_letterDTOToorganogramId.put(asset_clearance_letterDTO.organogramId, new HashSet<>());
					}
					mapOfAsset_clearance_letterDTOToorganogramId.get(asset_clearance_letterDTO.organogramId).add(asset_clearance_letterDTO);
					
					if( ! mapOfAsset_clearance_letterDTOTouserName.containsKey(asset_clearance_letterDTO.userName)) {
						mapOfAsset_clearance_letterDTOTouserName.put(asset_clearance_letterDTO.userName, new HashSet<>());
					}
					mapOfAsset_clearance_letterDTOTouserName.get(asset_clearance_letterDTO.userName).add(asset_clearance_letterDTO);
					
					if( ! mapOfAsset_clearance_letterDTOTorequestDate.containsKey(asset_clearance_letterDTO.requestDate)) {
						mapOfAsset_clearance_letterDTOTorequestDate.put(asset_clearance_letterDTO.requestDate, new HashSet<>());
					}
					mapOfAsset_clearance_letterDTOTorequestDate.get(asset_clearance_letterDTO.requestDate).add(asset_clearance_letterDTO);
					
					if( ! mapOfAsset_clearance_letterDTOToresponseRequiredDate.containsKey(asset_clearance_letterDTO.responseDate)) {
						mapOfAsset_clearance_letterDTOToresponseRequiredDate.put(asset_clearance_letterDTO.responseDate, new HashSet<>());
					}
					mapOfAsset_clearance_letterDTOToresponseRequiredDate.get(asset_clearance_letterDTO.responseDate).add(asset_clearance_letterDTO);
					
					if( ! mapOfAsset_clearance_letterDTOToclearanceStatusCat.containsKey(asset_clearance_letterDTO.clearanceStatusCat)) {
						mapOfAsset_clearance_letterDTOToclearanceStatusCat.put(asset_clearance_letterDTO.clearanceStatusCat, new HashSet<>());
					}
					mapOfAsset_clearance_letterDTOToclearanceStatusCat.get(asset_clearance_letterDTO.clearanceStatusCat).add(asset_clearance_letterDTO);
					
					if( ! mapOfAsset_clearance_letterDTOToemailDeclarationStatusCat.containsKey(asset_clearance_letterDTO.emailDeclarationStatusCat)) {
						mapOfAsset_clearance_letterDTOToemailDeclarationStatusCat.put(asset_clearance_letterDTO.emailDeclarationStatusCat, new HashSet<>());
					}
					mapOfAsset_clearance_letterDTOToemailDeclarationStatusCat.get(asset_clearance_letterDTO.emailDeclarationStatusCat).add(asset_clearance_letterDTO);
					
					if( ! mapOfAsset_clearance_letterDTOTodirectoryLoginStatusCat.containsKey(asset_clearance_letterDTO.directoryLoginStatusCat)) {
						mapOfAsset_clearance_letterDTOTodirectoryLoginStatusCat.put(asset_clearance_letterDTO.directoryLoginStatusCat, new HashSet<>());
					}
					mapOfAsset_clearance_letterDTOTodirectoryLoginStatusCat.get(asset_clearance_letterDTO.directoryLoginStatusCat).add(asset_clearance_letterDTO);
					
					if( ! mapOfAsset_clearance_letterDTOToparliamentSoftwareLoginStatusCat.containsKey(asset_clearance_letterDTO.parliamentSoftwareLoginStatusCat)) {
						mapOfAsset_clearance_letterDTOToparliamentSoftwareLoginStatusCat.put(asset_clearance_letterDTO.parliamentSoftwareLoginStatusCat, new HashSet<>());
					}
					mapOfAsset_clearance_letterDTOToparliamentSoftwareLoginStatusCat.get(asset_clearance_letterDTO.parliamentSoftwareLoginStatusCat).add(asset_clearance_letterDTO);
					
					if( ! mapOfAsset_clearance_letterDTOToreportServerStatusCat.containsKey(asset_clearance_letterDTO.reportServerStatusCat)) {
						mapOfAsset_clearance_letterDTOToreportServerStatusCat.put(asset_clearance_letterDTO.reportServerStatusCat, new HashSet<>());
					}
					mapOfAsset_clearance_letterDTOToreportServerStatusCat.get(asset_clearance_letterDTO.reportServerStatusCat).add(asset_clearance_letterDTO);
					
					if( ! mapOfAsset_clearance_letterDTOToitAssetHandoverStatusCat.containsKey(asset_clearance_letterDTO.itAssetHandoverStatusCat)) {
						mapOfAsset_clearance_letterDTOToitAssetHandoverStatusCat.put(asset_clearance_letterDTO.itAssetHandoverStatusCat, new HashSet<>());
					}
					mapOfAsset_clearance_letterDTOToitAssetHandoverStatusCat.get(asset_clearance_letterDTO.itAssetHandoverStatusCat).add(asset_clearance_letterDTO);
					
					if( ! mapOfAsset_clearance_letterDTOToremarks.containsKey(asset_clearance_letterDTO.remarks)) {
						mapOfAsset_clearance_letterDTOToremarks.put(asset_clearance_letterDTO.remarks, new HashSet<>());
					}
					mapOfAsset_clearance_letterDTOToremarks.get(asset_clearance_letterDTO.remarks).add(asset_clearance_letterDTO);
					
					if( ! mapOfAsset_clearance_letterDTOToinsertedByUserId.containsKey(asset_clearance_letterDTO.insertedByUserId)) {
						mapOfAsset_clearance_letterDTOToinsertedByUserId.put(asset_clearance_letterDTO.insertedByUserId, new HashSet<>());
					}
					mapOfAsset_clearance_letterDTOToinsertedByUserId.get(asset_clearance_letterDTO.insertedByUserId).add(asset_clearance_letterDTO);
					
					if( ! mapOfAsset_clearance_letterDTOToinsertedByOrganogramId.containsKey(asset_clearance_letterDTO.insertedByOrganogramId)) {
						mapOfAsset_clearance_letterDTOToinsertedByOrganogramId.put(asset_clearance_letterDTO.insertedByOrganogramId, new HashSet<>());
					}
					mapOfAsset_clearance_letterDTOToinsertedByOrganogramId.get(asset_clearance_letterDTO.insertedByOrganogramId).add(asset_clearance_letterDTO);
					
					if( ! mapOfAsset_clearance_letterDTOToinsertionDate.containsKey(asset_clearance_letterDTO.insertionDate)) {
						mapOfAsset_clearance_letterDTOToinsertionDate.put(asset_clearance_letterDTO.insertionDate, new HashSet<>());
					}
					mapOfAsset_clearance_letterDTOToinsertionDate.get(asset_clearance_letterDTO.insertionDate).add(asset_clearance_letterDTO);
					
					if( ! mapOfAsset_clearance_letterDTOTosearchColumn.containsKey(asset_clearance_letterDTO.searchColumn)) {
						mapOfAsset_clearance_letterDTOTosearchColumn.put(asset_clearance_letterDTO.searchColumn, new HashSet<>());
					}
					mapOfAsset_clearance_letterDTOTosearchColumn.get(asset_clearance_letterDTO.searchColumn).add(asset_clearance_letterDTO);
					
					if( ! mapOfAsset_clearance_letterDTOTolastModificationTime.containsKey(asset_clearance_letterDTO.lastModificationTime)) {
						mapOfAsset_clearance_letterDTOTolastModificationTime.put(asset_clearance_letterDTO.lastModificationTime, new HashSet<>());
					}
					mapOfAsset_clearance_letterDTOTolastModificationTime.get(asset_clearance_letterDTO.lastModificationTime).add(asset_clearance_letterDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Asset_clearance_letterDTO> getAsset_clearance_letterList() {
		List <Asset_clearance_letterDTO> asset_clearance_letters = new ArrayList<Asset_clearance_letterDTO>(this.mapOfAsset_clearance_letterDTOToiD.values());
		return asset_clearance_letters;
	}
	
	
	public Asset_clearance_letterDTO getAsset_clearance_letterDTOByID( long ID){
		return mapOfAsset_clearance_letterDTOToiD.get(ID);
	}
	
	
	public List<Asset_clearance_letterDTO> getAsset_clearance_letterDTOByorganogram_id(long organogram_id) {
		return new ArrayList<>( mapOfAsset_clearance_letterDTOToorganogramId.getOrDefault(organogram_id,new HashSet<>()));
	}
	
	
	public List<Asset_clearance_letterDTO> getAsset_clearance_letterDTOByuser_name(String user_name) {
		return new ArrayList<>( mapOfAsset_clearance_letterDTOTouserName.getOrDefault(user_name,new HashSet<>()));
	}
	
	
	public List<Asset_clearance_letterDTO> getAsset_clearance_letterDTOByrequest_date(long request_date) {
		return new ArrayList<>( mapOfAsset_clearance_letterDTOTorequestDate.getOrDefault(request_date,new HashSet<>()));
	}
	
	
	public List<Asset_clearance_letterDTO> getAsset_clearance_letterDTOByresponse_required_date(long response_required_date) {
		return new ArrayList<>( mapOfAsset_clearance_letterDTOToresponseRequiredDate.getOrDefault(response_required_date,new HashSet<>()));
	}
	
	
	public List<Asset_clearance_letterDTO> getAsset_clearance_letterDTOByclearance_status_cat(int clearance_status_cat) {
		return new ArrayList<>( mapOfAsset_clearance_letterDTOToclearanceStatusCat.getOrDefault(clearance_status_cat,new HashSet<>()));
	}
	
	
	public List<Asset_clearance_letterDTO> getAsset_clearance_letterDTOByemail_declaration_status_cat(int email_declaration_status_cat) {
		return new ArrayList<>( mapOfAsset_clearance_letterDTOToemailDeclarationStatusCat.getOrDefault(email_declaration_status_cat,new HashSet<>()));
	}
	
	
	public List<Asset_clearance_letterDTO> getAsset_clearance_letterDTOBydirectory_login_status_cat(int directory_login_status_cat) {
		return new ArrayList<>( mapOfAsset_clearance_letterDTOTodirectoryLoginStatusCat.getOrDefault(directory_login_status_cat,new HashSet<>()));
	}
	
	
	public List<Asset_clearance_letterDTO> getAsset_clearance_letterDTOByparliament_software_login_status_cat(int parliament_software_login_status_cat) {
		return new ArrayList<>( mapOfAsset_clearance_letterDTOToparliamentSoftwareLoginStatusCat.getOrDefault(parliament_software_login_status_cat,new HashSet<>()));
	}
	
	
	public List<Asset_clearance_letterDTO> getAsset_clearance_letterDTOByreport_server_status_cat(int report_server_status_cat) {
		return new ArrayList<>( mapOfAsset_clearance_letterDTOToreportServerStatusCat.getOrDefault(report_server_status_cat,new HashSet<>()));
	}
	
	
	public List<Asset_clearance_letterDTO> getAsset_clearance_letterDTOByit_asset_handover_status_cat(int it_asset_handover_status_cat) {
		return new ArrayList<>( mapOfAsset_clearance_letterDTOToitAssetHandoverStatusCat.getOrDefault(it_asset_handover_status_cat,new HashSet<>()));
	}
	
	
	public List<Asset_clearance_letterDTO> getAsset_clearance_letterDTOByremarks(String remarks) {
		return new ArrayList<>( mapOfAsset_clearance_letterDTOToremarks.getOrDefault(remarks,new HashSet<>()));
	}
	
	
	public List<Asset_clearance_letterDTO> getAsset_clearance_letterDTOByinserted_by_user_id(long inserted_by_user_id) {
		return new ArrayList<>( mapOfAsset_clearance_letterDTOToinsertedByUserId.getOrDefault(inserted_by_user_id,new HashSet<>()));
	}
	
	
	public List<Asset_clearance_letterDTO> getAsset_clearance_letterDTOByinserted_by_organogram_id(long inserted_by_organogram_id) {
		return new ArrayList<>( mapOfAsset_clearance_letterDTOToinsertedByOrganogramId.getOrDefault(inserted_by_organogram_id,new HashSet<>()));
	}
	
	
	public List<Asset_clearance_letterDTO> getAsset_clearance_letterDTOByinsertion_date(long insertion_date) {
		return new ArrayList<>( mapOfAsset_clearance_letterDTOToinsertionDate.getOrDefault(insertion_date,new HashSet<>()));
	}
	
	
	public List<Asset_clearance_letterDTO> getAsset_clearance_letterDTOBysearch_column(String search_column) {
		return new ArrayList<>( mapOfAsset_clearance_letterDTOTosearchColumn.getOrDefault(search_column,new HashSet<>()));
	}
	
	
	public List<Asset_clearance_letterDTO> getAsset_clearance_letterDTOBylastModificationTime(long lastModificationTime) {
		return new ArrayList<>( mapOfAsset_clearance_letterDTOTolastModificationTime.getOrDefault(lastModificationTime,new HashSet<>()));
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "asset_clearance_letter";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


