package asset_clearance_letter;

import java.io.IOException;
import java.io.*;
import java.text.SimpleDateFormat;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import org.apache.log4j.Logger;

import login.LoginDTO;
import permission.MenuConstants;
import role.PermissionRepository;


import sessionmanager.SessionConstants;
import support_ticket.Support_ticketDAO;
import support_ticket.Support_ticketDTO;
import ticket_forward_history.Ticket_forward_historyDAO;
import ticket_forward_history.Ticket_forward_historyDTO;
import user.UserDTO;
import user.UserRepository;
import util.CommonRequestHandler;
import util.RecordNavigationManager4;
import util.TimeConverter;

import java.util.*;
import javax.servlet.http.*;



import com.google.gson.Gson;

import asset_clearance_status.Asset_clearance_statusDAO;
import asset_clearance_status.Asset_clearance_statusDTO;
import asset_model.AssetAssigneeDAO;
import asset_model.AssetAssigneeDTO;

import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;




/**
 * Servlet implementation class Asset_clearance_letterServlet
 */
@WebServlet("/Asset_clearance_letterServlet")
@MultipartConfig
public class Asset_clearance_letterServlet extends HttpServlet 
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Asset_clearance_letterServlet.class);

    String tableName = "asset_clearance_letter";

	Asset_clearance_letterDAO asset_clearance_letterDAO;
	CommonRequestHandler commonRequestHandler;
    private final Gson gson = new Gson();
    Ticket_forward_historyDAO ticket_forward_historyDAO = new Ticket_forward_historyDAO();
    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Asset_clearance_letterServlet() 
	{
        super();
    	try
    	{
			asset_clearance_letterDAO = new Asset_clearance_letterDAO(tableName);
			commonRequestHandler = new CommonRequestHandler(asset_clearance_letterDAO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }   

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		System.out.println("In doget request = " + request);
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		try
		{
			String actionType = request.getParameter("actionType");
			if(actionType.equals("getAddPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ASSET_CLEARANCE_LETTER_ADD))
				{
					commonRequestHandler.getAddPage(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getEditPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ASSET_CLEARANCE_LETTER_UPDATE))
				{
					getAsset_clearance_letter(request, response);					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}						
			}
			else if(actionType.equals("getURL"))
			{
				String URL = request.getParameter("URL");
				System.out.println("URL = " + URL);
				response.sendRedirect(URL);			
			}			
			else if(actionType.equals("search"))
			{
				System.out.println("search requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ASSET_CLEARANCE_LETTER_SEARCH))
				{
					if(isPermanentTable)
					{
						String filter = request.getParameter("filter");
						System.out.println("filter = " + filter);
						if(filter!=null)
						{
							filter = ""; //shouldn't be directly used, rather manipulate it.
							searchAsset_clearance_letter(request, response, isPermanentTable, filter);
						}
						else
						{
							searchAsset_clearance_letter(request, response, isPermanentTable, "");
						}
					}					
					else
					{
						//searchAsset_clearance_letter(request, response, tempTableName, isPermanentTable);
					}
				}			
			}
			else if(actionType.equals("view"))
			{
				System.out.println("view requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ASSET_CLEARANCE_LETTER_SEARCH))
				{
					commonRequestHandler.view(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else if(actionType.equals("certificate"))
			{
				System.out.println("certificate requested");
				String ID = request.getParameter("ID");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ASSET_CLEARANCE_LETTER_SEARCH))
				{
					request.getRequestDispatcher( "asset_clearance_letter/certificate.jsp?ID=" + ID).forward(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else
			{
				request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}


	
	

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		// TODO Auto-generated method stub
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		System.out.println("doPost");
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		
		try
		{
			String actionType = request.getParameter("actionType");
			System.out.println("actionType = " + actionType);
			if(actionType.equals("add"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ASSET_CLEARANCE_LETTER_ADD))
				{
					System.out.println("going to  addAsset_clearance_letter ");
					addAsset_clearance_letter(request, response, true, userDTO, true);
				}
				else
				{
					System.out.println("Not going to  addAsset_clearance_letter ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}

			else if(actionType.equals("getDTO"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ASSET_CLEARANCE_LETTER_ADD))
				{
					getDTO(request, response);					
				}
				else
				{
					System.out.println("Not going to  addAsset_clearance_letter ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else if(actionType.equals("clear"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ASSET_CLEARANCE_LETTER_UPDATE))
				{
					long id = Long.parseLong(request.getParameter("id"));
					Asset_clearance_letterDTO clrDTO = asset_clearance_letterDAO.getDTOByID(id);
					if(clrDTO != null)
					{
						String Value = request.getParameter("itAssetHandoverStatusCat");

						if(Value != null)
						{
							Value = Jsoup.clean(Value,Whitelist.simpleText());
						}
						System.out.println("itAssetHandoverStatusCat = " + Value);
						if(Value != null && !Value.equalsIgnoreCase(""))
						{
							
							clrDTO.itAssetHandoverStatusCat = Integer.parseInt(Value);
						}
						else
						{
							System.out.println("FieldName has a null Value, not updating" + " = " + Value);
						}
						
						Value = request.getParameter("reportServerStatusCat");

						if(Value != null)
						{
							Value = Jsoup.clean(Value,Whitelist.simpleText());
						}
						System.out.println("reportServerStatusCat = " + Value);
						if(Value != null && !Value.equalsIgnoreCase(""))
						{
							
							clrDTO.reportServerStatusCat = Integer.parseInt(Value);
						}
						else
						{
							System.out.println("FieldName has a null Value, not updating" + " = " + Value);
						}
						
						Value = request.getParameter("parliamentSoftwareLoginStatusCat");

						if(Value != null)
						{
							Value = Jsoup.clean(Value,Whitelist.simpleText());
						}
						System.out.println("parliamentSoftwareLoginStatusCat = " + Value);
						if(Value != null && !Value.equalsIgnoreCase(""))
						{
							
							clrDTO.parliamentSoftwareLoginStatusCat = Integer.parseInt(Value);
						}
						else
						{
							System.out.println("FieldName has a null Value, not updating" + " = " + Value);
						}
						
						Value = request.getParameter("directoryLoginStatusCat");

						if(Value != null)
						{
							Value = Jsoup.clean(Value,Whitelist.simpleText());
						}
						System.out.println("directoryLoginStatusCat = " + Value);
						if(Value != null && !Value.equalsIgnoreCase(""))
						{
							
							clrDTO.directoryLoginStatusCat = Integer.parseInt(Value);
						}
						else
						{
							System.out.println("FieldName has a null Value, not updating" + " = " + Value);
						}
						
						Value = request.getParameter("emailDeclarationStatusCat");

						if(Value != null)
						{
							Value = Jsoup.clean(Value,Whitelist.simpleText());
						}
						System.out.println("emailDeclarationStatusCat = " + Value);
						if(Value != null && !Value.equalsIgnoreCase(""))
						{
							
							clrDTO.emailDeclarationStatusCat = Integer.parseInt(Value);
						}
						else
						{
							System.out.println("FieldName has a null Value, not updating" + " = " + Value);
						}
						
						Value = request.getParameter("remarks");

						if(Value != null)
						{
							Value = Jsoup.clean(Value,Whitelist.simpleText());
						}
						System.out.println("remarks = " + Value);
						if(Value != null && !Value.equalsIgnoreCase(""))
						{
							
							clrDTO.remarks = Value;
						}
						else
						{
							System.out.println("FieldName has a null Value, not updating" + " = " + Value);
						}
						
						Value = request.getParameter("clearanceStatusCat");

						if(Value != null)
						{
							Value = Jsoup.clean(Value,Whitelist.simpleText());
						}
						System.out.println("clearanceStatusCat = " + Value);
						if(Value != null && !Value.equalsIgnoreCase(""))
						{
							
							clrDTO.clearanceStatusCat = Integer.parseInt(Value);
						}
						else
						{
							System.out.println("FieldName has a null Value, not updating" + " = " + Value);
						}
						
						asset_clearance_letterDAO.update(clrDTO);
						if(clrDTO.clearanceStatusCat == Asset_clearance_letterDTO.YES)
						{
							asset_clearance_letterDAO.sendNoti(clrDTO);
						}
						response.sendRedirect("Asset_clearance_letterServlet?actionType=search");
					}
				}
				else
				{
					System.out.println("Not going to  addAsset_clearance_letter ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else if(actionType.equals("edit"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ASSET_CLEARANCE_LETTER_UPDATE))
				{					
					addAsset_clearance_letter(request, response, false, userDTO, isPermanentTable);					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("delete"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ASSET_CLEARANCE_LETTER_UPDATE))
				{
					commonRequestHandler.delete(request, response, userDTO);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}	
			else if(actionType.equals("search"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.ASSET_CLEARANCE_LETTER_SEARCH))
				{
					searchAsset_clearance_letter(request, response, true, "");
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getGeo"))
			{
				System.out.println("going to geoloc ");
				request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
			}
			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}
	
	private void getDTO(HttpServletRequest request, HttpServletResponse response) 
	{
		try 
		{
			System.out.println("In getDTO");
			Asset_clearance_letterDTO asset_clearance_letterDTO = asset_clearance_letterDAO.getDTOByID(Long.parseLong(request.getParameter("ID")));
			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			
			String encoded = this.gson.toJson(asset_clearance_letterDTO);
			System.out.println("json encoded data = " + encoded);
			out.print(encoded);
			out.flush();
		}
		catch (NumberFormatException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	private void addAsset_clearance_letter(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, boolean isPermanentTable) throws IOException 
	{
		// TODO Auto-generated method stub
		try 
		{
			request.setAttribute("failureMessage", "");
			System.out.println("%%%% addAsset_clearance_letter");

			Asset_clearance_letterDTO asset_clearance_letterDTO;
			SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
						
			if(addFlag == true)
			{
				asset_clearance_letterDTO = new Asset_clearance_letterDTO();
			}
			else
			{
				asset_clearance_letterDTO = asset_clearance_letterDAO.getDTOByID(Long.parseLong(request.getParameter("iD")));
			}
			
			String Value = "";

			Value = request.getParameter("organogramId");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("organogramId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				asset_clearance_letterDTO.organogramId = Long.parseLong(Value);
				UserDTO clearanceUserDTO = UserRepository.getUserDTOByOrganogramID(asset_clearance_letterDTO.organogramId);
				if(clearanceUserDTO == null)
				{
					response.sendRedirect("Asset_clearance_letterServlet?actionType=search");
					return;
				}
				else
				{
					asset_clearance_letterDTO.userName = clearanceUserDTO.userName;
				}
				
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			
			
			
			if(addFlag)
			{
				asset_clearance_letterDTO.requestDate = TimeConverter.getToday();
			}

			
			Value = request.getParameter("nocDate");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("nocDate = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				try 
				{
					Date d = f.parse(Value);
					asset_clearance_letterDTO.nocDate = d.getTime();
				}
				catch (Exception e) 
				{
					e.printStackTrace();
				}
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			
			Value = request.getParameter("letterNumber");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("letterNumber = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				asset_clearance_letterDTO.letterNumber = Value;
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			
			Value = request.getParameter("codeNumber");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("codeNumber = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				asset_clearance_letterDTO.codeNumber = Value;
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			
			Value = request.getParameter("uONoteNumber");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("uONoteNumber = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				asset_clearance_letterDTO.uONoteNumber = Value;
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}


			if(addFlag)
			{
				asset_clearance_letterDTO.insertedByUserId = userDTO.ID;
			}


			if(addFlag)
			{
				asset_clearance_letterDTO.insertedByOrganogramId = userDTO.organogramID;
			}


			if(addFlag)
			{				
				asset_clearance_letterDTO.insertionDate = TimeConverter.getToday();
			}			

			
			
			System.out.println("Done adding  addAsset_clearance_letter dto = " + asset_clearance_letterDTO);
			
			Support_ticketDTO support_ticketDTO = new Support_ticketDTO(asset_clearance_letterDTO, userDTO);
			Support_ticketDAO support_ticketDAO = new Support_ticketDAO();
			
			AssetAssigneeDAO assetAssigneeDAO = new AssetAssigneeDAO();
			Asset_clearance_statusDAO asset_clearance_statusDAO = Asset_clearance_statusDAO.getInstance();
			
			asset_clearance_letterDTO.supportTicketId = support_ticketDAO.add(support_ticketDTO);
			
			addToTicketHistory(support_ticketDTO, UserRepository.getUserDTOByOrganogramID(support_ticketDTO.issueRaiserOrganoramId),
                    UserRepository.getUserDTOByOrganogramID(support_ticketDTO.currentAssignedOrganoramId),
                    support_ticketDTO.description, support_ticketDTO.filesDropzone, userDTO, Ticket_forward_historyDTO.TICKET_ASSIGN);
			
			support_ticketDAO.addTicketNoti(support_ticketDTO);
			
			
			long returnedID = -1;
			
			if(addFlag == true)
			{
				returnedID = asset_clearance_letterDAO.add(asset_clearance_letterDTO);
			}
			else
			{				
				returnedID = asset_clearance_letterDAO.update(asset_clearance_letterDTO);											
			}
			
			List<AssetAssigneeDTO> assetAssigneeDTOs = assetAssigneeDAO.getByOrganogramId(asset_clearance_letterDTO.organogramId);
			if(assetAssigneeDTOs != null)
			{
				for(AssetAssigneeDTO assetAssigneeDTO: assetAssigneeDTOs)
				{
					Asset_clearance_statusDTO asset_clearance_statusDTO = new Asset_clearance_statusDTO(asset_clearance_letterDTO, support_ticketDTO, userDTO, assetAssigneeDTO);
					asset_clearance_statusDAO.add(asset_clearance_statusDTO);
				}
			}

			
			
			response.sendRedirect("Asset_clearance_letterServlet?actionType=search");
					
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	

	private void addToTicketHistory(Support_ticketDTO support_ticketDTO, UserDTO fromUserDTO, UserDTO toUserDTO,
			String forwardComments, long filesDropzone, UserDTO userDTO, int forwardActionCat) throws IOException {
		try {

			Ticket_forward_historyDTO ticket_forward_historyDTO = new Ticket_forward_historyDTO(support_ticketDTO,
					fromUserDTO, toUserDTO, forwardComments, filesDropzone, userDTO, forwardActionCat, -1);

			ticket_forward_historyDAO.add(ticket_forward_historyDTO);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	
	

	private void getAsset_clearance_letter(HttpServletRequest request, HttpServletResponse response, long id) throws ServletException, IOException
	{
		System.out.println("in getAsset_clearance_letter");
		Asset_clearance_letterDTO asset_clearance_letterDTO = null;
		try 
		{
			asset_clearance_letterDTO = asset_clearance_letterDAO.getDTOByID(id);
			request.setAttribute("ID", asset_clearance_letterDTO.iD);
			request.setAttribute("asset_clearance_letterDTO",asset_clearance_letterDTO);
			request.setAttribute("asset_clearance_letterDAO",asset_clearance_letterDAO);
			
			String URL= "";
			
			String inPlaceEdit = request.getParameter("inplaceedit");
			String inPlaceSubmit = request.getParameter("inplacesubmit");
			String getBodyOnly = request.getParameter("getBodyOnly");
			
			if(inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase(""))
			{
				URL = "asset_clearance_letter/asset_clearance_letterInPlaceEdit.jsp";	
				request.setAttribute("inplaceedit","");				
			}
			else if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
			{
				URL = "asset_clearance_letter/asset_clearance_letterSearchRow.jsp";
				request.setAttribute("inplacesubmit","");					
			}
			else
			{
				if(getBodyOnly  != null && !getBodyOnly.equalsIgnoreCase(""))
				{
					URL = "asset_clearance_letter/asset_clearance_letterEditBody.jsp?actionType=edit";
				}
				else
				{
					URL = "asset_clearance_letter/asset_clearance_letterEdit.jsp?actionType=edit";
				}				
			}
			
			RequestDispatcher rd = request.getRequestDispatcher(URL);
			rd.forward(request, response);
		}
		catch (NumberFormatException e) 
		{
			e.printStackTrace();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	
	private void getAsset_clearance_letter(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		getAsset_clearance_letter(request, response, Long.parseLong(request.getParameter("ID")));	
	}
	
	private void searchAsset_clearance_letter(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException
	{
		System.out.println("in  searchAsset_clearance_letter 1");
		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String ajax = request.getParameter("ajax");
		boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);
		
        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
			SessionConstants.NAV_ASSET_CLEARANCE_LETTER,
			request,
			asset_clearance_letterDAO,
			SessionConstants.VIEW_ASSET_CLEARANCE_LETTER,
			SessionConstants.SEARCH_ASSET_CLEARANCE_LETTER,
			tableName,
			isPermanent,
			userDTO,
			filter,
			true);
        try
        {
			System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        }
        catch(Exception e)
        {
			System.out.println("failed to dojob" + e);
        }

		request.setAttribute("asset_clearance_letterDAO",asset_clearance_letterDAO);
        RequestDispatcher rd;
        if(!isPermanent)
        {
        	if(hasAjax == false)
	        {
	        	System.out.println("Going to asset_clearance_letter/asset_clearance_letterApproval.jsp");
	        	rd = request.getRequestDispatcher("asset_clearance_letter/asset_clearance_letterApproval.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to asset_clearance_letter/asset_clearance_letterApprovalForm.jsp");
	        	rd = request.getRequestDispatcher("asset_clearance_letter/asset_clearance_letterApprovalForm.jsp");
	        }
        }
        else
        {
	        if(hasAjax == false)
	        {
	        	System.out.println("Going to asset_clearance_letter/asset_clearance_letterSearch.jsp");
	        	rd = request.getRequestDispatcher("asset_clearance_letter/asset_clearance_letterSearch.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to asset_clearance_letter/asset_clearance_letterSearchForm.jsp");
	        	rd = request.getRequestDispatcher("asset_clearance_letter/asset_clearance_letterSearchForm.jsp");
	        }
        }
		rd.forward(request, response);
	}
	
}

