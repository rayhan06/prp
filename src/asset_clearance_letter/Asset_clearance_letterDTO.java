package asset_clearance_letter;

import util.*; 


public class Asset_clearance_letterDTO extends CommonDTO
{

	public long organogramId = -1;
	public long employeeRecordId = -1;
    public String userName = "";
	public long requestDate = System.currentTimeMillis();
	public long responseDate = 0;
	
	
	
	
	
	
    public String remarks = "";
	public long insertedByUserId = -1;
	public long insertedByOrganogramId = -1;
	public long insertionDate = -1;
	
	public long supportTicketId = -1;
	
    public static int NA = 0;
    public static int YES = 1;
    public static int NO = 2;
    
    public static final int PENDING = 0;
    public static final int NOC_VERIFIED = 1;
    public static final int NOC_APPROVED = 2;
    public static final int NOC_REJECTED = 3;
    public static final int NOC_VERIFICATION_STARTED = 4;
    
    public int emailDeclarationStatusCat = 0;
	public int directoryLoginStatusCat = 0;
	public int parliamentSoftwareLoginStatusCat = 0;
	public int reportServerStatusCat = 0;
	public int itAssetHandoverStatusCat = 0;
	public int clearanceStatusCat = PENDING;
    
    
    public long nocDate = System.currentTimeMillis();
    public String letterNumber = "";
    public String codeNumber = "";
    public String uONoteNumber = "";
    public long signingOrganogramId = -1;
	
	
    @Override
	public String toString() {
            return "$Asset_clearance_letterDTO[" +
            " iD = " + iD +
            " organogramId = " + organogramId +
            " userName = " + userName +
            " requestDate = " + requestDate +
            " responseRequiredDate = " + responseDate +
            " clearanceStatusCat = " + clearanceStatusCat +
            " emailDeclarationStatusCat = " + emailDeclarationStatusCat +
            " directoryLoginStatusCat = " + directoryLoginStatusCat +
            " parliamentSoftwareLoginStatusCat = " + parliamentSoftwareLoginStatusCat +
            " reportServerStatusCat = " + reportServerStatusCat +
            " itAssetHandoverStatusCat = " + itAssetHandoverStatusCat +
            " remarks = " + remarks +
            " insertedByUserId = " + insertedByUserId +
            " insertedByOrganogramId = " + insertedByOrganogramId +
            " insertionDate = " + insertionDate +
            " searchColumn = " + searchColumn +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}