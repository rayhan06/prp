package mp_travel_allowance;

import sessionmanager.SessionConstants;
import util.CommonDTO;

import java.util.ArrayList;
import java.util.List;


public class MpTravelMeetingDTO extends CommonDTO {

    public long mpTravelAllowanceID = -1;
    public long meetingDate = System.currentTimeMillis();
    public String purpose = "";
    public long insertedBy = 0;
    public long insertionTime = SessionConstants.MIN_DATE;
    public long modifiedBy = 0;

    public List<MpTravelDestinationDTO> mpTravelDestinationDTOList = new ArrayList<>();

    @Override
    public String toString() {
        return "$MpTravelMeetingDTO[" +
                " iD = " + iD +
                " mpTravelAllowanceID = " + mpTravelAllowanceID +
                " meetingDate = " + meetingDate +
                " purpose = " + purpose +
                " insertedBy = " + insertedBy +
                " insertionTime = " + insertionTime +
                " isDeleted = " + isDeleted +
                " modifiedBy = " + modifiedBy +
                " lastModificationTime = " + lastModificationTime +
                "]";
    }

}