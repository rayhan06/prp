package mp_travel_allowance;

import sessionmanager.SessionConstants;
import util.CommonDTO;

import java.util.ArrayList;
import java.util.List;


public class MpTravelDestinationDTO extends CommonDTO {

    public long mpTravelAllowanceID = -1;
    public String destinationAddress = "";
    public int timeCat = 0;
    public int travelMediumCat = 0;
    public int travelTypeCat = 0;
    public double distanceOrSubCost = 0.0;
    public double cost = 0.0;
    public long reachDate = SessionConstants.MIN_DATE;
    public long insertedBy = 0;
    public long insertionTime = SessionConstants.MIN_DATE;
    public long modifiedBy = 0;

    public List<MpTravelDestinationDTO> mpTravelDestinationDTOList = new ArrayList<>();

    @Override
    public String toString() {
        return "$MpTravelDestinationDTO[" +
                " iD = " + iD +
                " mpTravelAllowanceID = " + mpTravelAllowanceID +
                " destinationAddress = " + destinationAddress +
                " travelMediumCat = " + travelMediumCat +
                " travelTypeCat = " + travelTypeCat +
                " cost = " + cost +
                " reachDate = " + reachDate +
                " distanceOrSubCost = " + distanceOrSubCost +
                " timeCat = " + timeCat +
                " insertedBy = " + insertedBy +
                " insertionTime = " + insertionTime +
                " isDeleted = " + isDeleted +
                " modifiedBy = " + modifiedBy +
                " lastModificationTime = " + lastModificationTime +
                "]";
    }

}