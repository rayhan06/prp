package mp_travel_allowance;

import com.google.gson.Gson;
import org.apache.log4j.Logger;
import repository.Repository;
import repository.RepositoryManager;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

@SuppressWarnings({"unused", "Duplicates"})
public class MpTravelMeetingRepository implements Repository {
    MpTravelMeetingDAO mptravelmeetingDAO;

    static Logger logger = Logger.getLogger(MpTravelMeetingRepository.class);
    Map<Long, MpTravelMeetingDTO> mapOfMpTravelMeetingDTOToiD;
    Map<Long, Set<MpTravelMeetingDTO>> mapOfMpTravelMeetingDTOTompTravelAllowanceID;
    Gson gson;


    private MpTravelMeetingRepository() {
        mptravelmeetingDAO = MpTravelMeetingDAO.getInstance();
        mapOfMpTravelMeetingDTOToiD = new ConcurrentHashMap<>();
        mapOfMpTravelMeetingDTOTompTravelAllowanceID = new ConcurrentHashMap<>();
        gson = new Gson();
        RepositoryManager.getInstance().addRepository(this);
    }

    private static class LazyLoader {
        final static MpTravelMeetingRepository INSTANCE = new MpTravelMeetingRepository();
    }

    public static MpTravelMeetingRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

    public void reload(boolean reloadAll) {
        try {
            List<MpTravelMeetingDTO> mptravelmeetingDTOs = mptravelmeetingDAO.getAllDTOs(reloadAll);
            for (MpTravelMeetingDTO mptravelmeetingDTO : mptravelmeetingDTOs) {
                MpTravelMeetingDTO oldMpTravelMeetingDTO = getMpTravelMeetingDTOByiD(mptravelmeetingDTO.iD);
                if (oldMpTravelMeetingDTO != null) {
                    mapOfMpTravelMeetingDTOToiD.remove(oldMpTravelMeetingDTO.iD);

                    if (mapOfMpTravelMeetingDTOTompTravelAllowanceID.containsKey(oldMpTravelMeetingDTO.mpTravelAllowanceID)) {
                        mapOfMpTravelMeetingDTOTompTravelAllowanceID.get(oldMpTravelMeetingDTO.mpTravelAllowanceID).remove(oldMpTravelMeetingDTO);
                    }
                    if (mapOfMpTravelMeetingDTOTompTravelAllowanceID.get(oldMpTravelMeetingDTO.mpTravelAllowanceID).isEmpty()) {
                        mapOfMpTravelMeetingDTOTompTravelAllowanceID.remove(oldMpTravelMeetingDTO.mpTravelAllowanceID);
                    }


                }
                if (mptravelmeetingDTO.isDeleted == 0) {

                    mapOfMpTravelMeetingDTOToiD.put(mptravelmeetingDTO.iD, mptravelmeetingDTO);

                    if (!mapOfMpTravelMeetingDTOTompTravelAllowanceID.containsKey(mptravelmeetingDTO.mpTravelAllowanceID)) {
                        mapOfMpTravelMeetingDTOTompTravelAllowanceID.put(mptravelmeetingDTO.mpTravelAllowanceID, new HashSet<>());
                    }
                    mapOfMpTravelMeetingDTOTompTravelAllowanceID.get(mptravelmeetingDTO.mpTravelAllowanceID).add(mptravelmeetingDTO);

                }
            }

        } catch (Exception e) {
            logger.debug("FATAL", e);
        }
    }

    public MpTravelMeetingDTO clone(MpTravelMeetingDTO dto) {
        String raw = gson.toJson(dto);
        return gson.fromJson(raw, MpTravelMeetingDTO.class);
    }


    public List<MpTravelMeetingDTO> getMpTravelMeetingList() {
        return new ArrayList<>(this.mapOfMpTravelMeetingDTOToiD.values());
    }

    public List<MpTravelMeetingDTO> copyMpTravelMeetingList() {
        List<MpTravelMeetingDTO> mptravelmeetings = getMpTravelMeetingList();
        return mptravelmeetings
                .stream()
                .map(this::clone)
                .collect(Collectors.toList());
    }


    public MpTravelMeetingDTO getMpTravelMeetingDTOByiD(long iD) {
        return mapOfMpTravelMeetingDTOToiD.get(iD);
    }

    public MpTravelMeetingDTO copyMpTravelMeetingDTOByiD(long iD) {
        return clone(mapOfMpTravelMeetingDTOToiD.get(iD));
    }


    @Override
    public String getTableName() {
        return mptravelmeetingDAO.getTableName();
    }
}


