package mp_travel_allowance;

import util.CommonMaps;


public class MpTravelDestinationMAPS extends CommonMaps {
    public MpTravelDestinationMAPS(String tableName) {


        java_SQL_map.put("mp_travel_allowance_ID".toLowerCase(), "mpTravelAllowanceID".toLowerCase());
        java_SQL_map.put("destination_address".toLowerCase(), "destinationAddress".toLowerCase());
        java_SQL_map.put("travel_medium_cat".toLowerCase(), "travelMediumCat".toLowerCase());
        java_SQL_map.put("travel_type_cat".toLowerCase(), "travelTypeCat".toLowerCase());
        java_SQL_map.put("cost".toLowerCase(), "cost".toLowerCase());
        java_SQL_map.put("reach_date".toLowerCase(), "reachDate".toLowerCase());
        java_SQL_map.put("distance_or_sub_cost".toLowerCase(), "distanceOrSubCost".toLowerCase());
        java_SQL_map.put("time_cat".toLowerCase(), "timeCat".toLowerCase());
        java_SQL_map.put("inserted_by".toLowerCase(), "insertedBy".toLowerCase());
        java_SQL_map.put("insertion_time".toLowerCase(), "insertionTime".toLowerCase());
        java_SQL_map.put("modified_by".toLowerCase(), "modifiedBy".toLowerCase());

        java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
        java_Text_map.put("Mp Travel Allowance ID".toLowerCase(), "mpTravelAllowanceID".toLowerCase());
        java_Text_map.put("Destination Address".toLowerCase(), "destinationAddress".toLowerCase());
        java_Text_map.put("Travel Medium".toLowerCase(), "travelMediumCat".toLowerCase());
        java_Text_map.put("Travel Type".toLowerCase(), "travelTypeCat".toLowerCase());
        java_Text_map.put("Cost".toLowerCase(), "cost".toLowerCase());
        java_Text_map.put("Reach Date".toLowerCase(), "reachDate".toLowerCase());
        java_Text_map.put("Distance Or Sub Cost".toLowerCase(), "distanceOrSubCost".toLowerCase());
        java_Text_map.put("Time".toLowerCase(), "timeCat".toLowerCase());
        java_Text_map.put("Inserted By".toLowerCase(), "insertedBy".toLowerCase());
        java_Text_map.put("Insertion Time".toLowerCase(), "insertionTime".toLowerCase());
        java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
        java_Text_map.put("Modified By".toLowerCase(), "modifiedBy".toLowerCase());
        java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

    }

}