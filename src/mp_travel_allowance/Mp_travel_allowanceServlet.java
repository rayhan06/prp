package mp_travel_allowance;

import com.google.gson.Gson;
import common.BaseServlet;
import election_constituency.Election_constituencyDTO;
import election_constituency.Election_constituencyRepository;
import election_wise_mp.Election_wise_mpDTO;
import election_wise_mp.Election_wise_mpRepository;
import employee_attendance.Employee_attendanceDAO;
import employee_attendance.Employee_attendanceDTO;
import employee_records.Employee_recordsDTO;
import employee_records.Employee_recordsRepository;
import login.LoginDTO;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import parliament_session.Parliament_sessionRepository;
import pb.Utils;
import permission.MenuConstants;
import sessionmanager.SessionConstants;
import travel_allowance_configuration.AllowanceEnum;
import travel_allowance_configuration.AttendanceEnum;
import travel_allowance_configuration.Travel_allowance_configurationDAO;
import user.UserDTO;
import user.UserRepository;
import util.CommonConstant;
import util.CommonDTO;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.TimeUnit;

@SuppressWarnings({"Duplicates"})
@WebServlet("/Mp_travel_allowanceServlet")
@MultipartConfig
public class Mp_travel_allowanceServlet extends BaseServlet {
    private static final long serialVersionUID = 1L;
    private static final Logger logger = Logger.getLogger(Mp_travel_allowanceServlet.class);
    private final MpTravelMeetingDAO mpTravelMeetingDAO = MpTravelMeetingDAO.getInstance();
    private final MpTravelDestinationDAO mpTravelDestinationDAO = MpTravelDestinationDAO.getInstance();
    private final Gson gson = new Gson();

    @Override
    public String getTableName() {
        return Mp_travel_allowanceDAO.getInstance().getTableName();
    }

    @Override
    public String getServletName() {
        return "Mp_travel_allowanceServlet";
    }

    @Override
    public Mp_travel_allowanceDAO getCommonDAOService() {
        return Mp_travel_allowanceDAO.getInstance();
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[]{MenuConstants.MP_TRAVEL_ALLOWANCE_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[]{MenuConstants.MP_TRAVEL_ALLOWANCE_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[]{MenuConstants.MP_TRAVEL_ALLOWANCE_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Mp_travel_allowanceServlet.class;
    }

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        long currentTime = System.currentTimeMillis();
        boolean isLanguageEnglish = userDTO.languageID == SessionConstants.ENGLISH;
        Mp_travel_allowanceDTO mp_travel_allowanceDTO;
        if (addFlag) {
            mp_travel_allowanceDTO = new Mp_travel_allowanceDTO();
            mp_travel_allowanceDTO.insertedBy = userDTO.employee_record_id;
            mp_travel_allowanceDTO.insertionTime = currentTime;
        } else {
            mp_travel_allowanceDTO = Mp_travel_allowanceDAO.getInstance().getDTOFromID(Long.parseLong(request.getParameter("iD")));
        }
        mp_travel_allowanceDTO.modifiedBy = userDTO.employee_record_id;
        mp_travel_allowanceDTO.lastModificationTime = currentTime;

        String Value;
        Value = request.getParameter("isSession");
        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
            mp_travel_allowanceDTO.isSession = Value != null && !Value.equalsIgnoreCase("");
            mp_travel_allowanceDTO.parliamentSessionId = Long.parseLong(Jsoup.clean(request.getParameter("parliament_session"), Whitelist.simpleText()));
        } else {
            mp_travel_allowanceDTO.isSession = false;
        }

        Value = request.getParameter("isMeeting");
        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
            mp_travel_allowanceDTO.isMeeting = Value != null && !Value.equalsIgnoreCase("");
        } else {
            mp_travel_allowanceDTO.isMeeting = false;
        }
        String dateString = Jsoup.clean(request.getParameter("startDate"), Whitelist.simpleText());
        if (dateString.equals("")) {

            throw new Exception(isLanguageEnglish ? "Enter a valid start date." : "সঠিক ভ্রমণ শুরুর তারিখ প্রদান করুন");
        }
        mp_travel_allowanceDTO.startDate = Long.parseLong(dateString);
        mp_travel_allowanceDTO.startTimeCat = Integer.parseInt(Jsoup.clean(request.getParameter("startTimeCat"), Whitelist.simpleText()));

        dateString = Jsoup.clean(request.getParameter("endDate"), Whitelist.simpleText());
        if (dateString.equals("")) {

            throw new Exception(isLanguageEnglish ? "Enter a valid end date." : "সঠিক ভ্রমণ শেষ তারিখ প্রদান করুন");
        }
        mp_travel_allowanceDTO.endDate = Long.parseLong(Jsoup.clean(request.getParameter("endDate"), Whitelist.simpleText()));
        mp_travel_allowanceDTO.endTimeCat = Integer.parseInt(Jsoup.clean(request.getParameter("endTimeCat"), Whitelist.simpleText()));


        mp_travel_allowanceDTO.startAddress = Jsoup.clean(request.getParameter("startAddress"), Whitelist.simpleText());
        mp_travel_allowanceDTO.electionDetailsId = Long.parseLong(request.getParameter("election_details_id"));
        mp_travel_allowanceDTO.electionConstituencyId = Long.parseLong(request.getParameter("election_constituency_id"));

        Election_constituencyDTO election_constituencyDTO = Election_constituencyRepository.getInstance().getElection_constituencyDTOByID(mp_travel_allowanceDTO.electionConstituencyId);
        mp_travel_allowanceDTO.electionConstituencyEn = election_constituencyDTO.constituencyNameEn;
        mp_travel_allowanceDTO.electionConstituencyBn = election_constituencyDTO.constituencyNameBn;

        Election_wise_mpDTO election_wise_mpDTO = Election_wise_mpRepository.getInstance().getByElectionAndConstituency(mp_travel_allowanceDTO.electionDetailsId, mp_travel_allowanceDTO.electionConstituencyId);
        if (election_wise_mpDTO == null) {
            throw new Exception("No MP Found!");
        }
        mp_travel_allowanceDTO.electionWiseMpId = election_wise_mpDTO.iD;
        mp_travel_allowanceDTO.employeeRecordsId = election_wise_mpDTO.employeeRecordsId;

        Employee_recordsDTO employee_recordsDTO = Employee_recordsRepository.getInstance().getById(mp_travel_allowanceDTO.employeeRecordsId);
        if (employee_recordsDTO == null) {
            throw new Exception("No Record Found!");
        }
        mp_travel_allowanceDTO.mpNameEn = employee_recordsDTO.nameEng;
        mp_travel_allowanceDTO.mpNameBn = employee_recordsDTO.nameBng;
        mp_travel_allowanceDTO.mpUserName = election_wise_mpDTO.userName;


        List<MpTravelMeetingDTO> mpTravelMeetingDTOList = createMpTravelMeetingDTOListByRequest(request, userDTO, isLanguageEnglish);
        double totalAmount = 0;
        List<MpTravelDestinationDTO> mpTravelDestinationDTOList = createMpTravelDestinationDTOListByRequest(request, userDTO, isLanguageEnglish);
        if (mpTravelDestinationDTOList != null) {
            for (MpTravelDestinationDTO mpTravelDestinationDTO : mpTravelDestinationDTOList) {
                totalAmount += mpTravelDestinationDTO.cost;
            }
        }
        totalAmount *= 2.0;
        totalAmount = (int) totalAmount;
        int totalStay = 0;

        long diff = mp_travel_allowanceDTO.endDate - mp_travel_allowanceDTO.startDate;
        int totalDays = (int) TimeUnit.MILLISECONDS.toDays(diff) + 1;

        if (mp_travel_allowanceDTO.isMeeting && mpTravelMeetingDTOList != null) {
            totalStay += mpTravelMeetingDTOList.stream()
                    .map(dto -> dto.meetingDate)
                    .distinct()
                    .count();
        }
        if (mp_travel_allowanceDTO.isSession) {
            List<Employee_attendanceDTO> employee_attendanceDTOList = new Employee_attendanceDAO().getEmployeeAttendanceInRange(mp_travel_allowanceDTO.startDate, mp_travel_allowanceDTO.endDate, mp_travel_allowanceDTO.employeeRecordsId);
            totalStay += employee_attendanceDTOList.size();
        }

        int rate = Travel_allowance_configurationDAO.getInstance().getByCategories(AllowanceEnum.DAILY.getValue(), AttendanceEnum.ATTENDANT.getValue()).amount;
        mp_travel_allowanceDTO.dailyAttendant = rate;
        mp_travel_allowanceDTO.dailyAttendantAmount = totalStay * rate;
        rate = Travel_allowance_configurationDAO.getInstance().getByCategories(AllowanceEnum.DAILY.getValue(), AttendanceEnum.STAY.getValue()).amount;
        mp_travel_allowanceDTO.dailyStay = rate;
        mp_travel_allowanceDTO.dailyStayAmount = (totalDays - totalStay) * rate;

        rate = Travel_allowance_configurationDAO.getInstance().getByCategories(AllowanceEnum.TRAVEL.getValue(), AttendanceEnum.ATTENDANT.getValue()).amount;
        mp_travel_allowanceDTO.travelAttendant = rate;
        mp_travel_allowanceDTO.travelAttendantAmount = totalStay * rate;
        rate = Travel_allowance_configurationDAO.getInstance().getByCategories(AllowanceEnum.TRAVEL.getValue(), AttendanceEnum.STAY.getValue()).amount;
        mp_travel_allowanceDTO.travelStay = rate;
        mp_travel_allowanceDTO.travelStayAmount = (totalDays - totalStay) * rate;

        mp_travel_allowanceDTO.netAmount = (int) (totalAmount + mp_travel_allowanceDTO.dailyAttendantAmount + mp_travel_allowanceDTO.dailyStayAmount + mp_travel_allowanceDTO.travelAttendantAmount + mp_travel_allowanceDTO.travelStayAmount);
        System.out.println("Done adding  addMp_travel_allowance dto = " + mp_travel_allowanceDTO);

        if (addFlag) {
            Mp_travel_allowanceDAO.getInstance().add(mp_travel_allowanceDTO);
        } else {
            Mp_travel_allowanceDAO.getInstance().update(mp_travel_allowanceDTO);
        }

        if (mp_travel_allowanceDTO.isMeeting) {
            if (addFlag) //add or validate
            {
                if (mpTravelMeetingDTOList != null) {
                    for (MpTravelMeetingDTO mpTravelMeetingDTO : mpTravelMeetingDTOList) {
                        mpTravelMeetingDTO.mpTravelAllowanceID = mp_travel_allowanceDTO.iD;
                        mpTravelMeetingDAO.add(mpTravelMeetingDTO);
                    }
                }

            } else {
                List<Long> childIdsFromRequest = mpTravelMeetingDAO.getChildIdsFromRequest(request, "mpTravelMeeting");
                //delete the removed children
                mpTravelMeetingDAO.deleteChildrenNotInList("mp_travel_allowance", "mp_travel_meeting", mp_travel_allowanceDTO.iD, childIdsFromRequest);
                List<Long> childIDsInDatabase = mpTravelMeetingDAO.getChilIds("mp_travel_allowance", "mp_travel_meeting", mp_travel_allowanceDTO.iD);


                if (childIdsFromRequest != null) {
                    for (int i = 0; i < childIdsFromRequest.size(); i++) {
                        Long childIDFromRequest = childIdsFromRequest.get(i);
                        if (childIDsInDatabase.contains(childIDFromRequest)) {
                            MpTravelMeetingDTO mpTravelMeetingDTO = createMpTravelMeetingDTOByRequestAndIndex(request, false, i, userDTO, isLanguageEnglish);
                            mpTravelMeetingDTO.mpTravelAllowanceID = mp_travel_allowanceDTO.iD;
                            mpTravelMeetingDAO.update(mpTravelMeetingDTO);
                        } else {
                            MpTravelMeetingDTO mpTravelMeetingDTO = createMpTravelMeetingDTOByRequestAndIndex(request, true, i, userDTO, isLanguageEnglish);
                            mpTravelMeetingDTO.mpTravelAllowanceID = mp_travel_allowanceDTO.iD;
                            mpTravelMeetingDAO.add(mpTravelMeetingDTO);
                        }
                    }
                } else {
                    mpTravelMeetingDAO.deleteChildrenByParent(mp_travel_allowanceDTO.iD, "mp_travel_allowance_id");
                }

            }
        }
        if (addFlag) //add or validate
        {
            if (mpTravelDestinationDTOList != null) {
                for (MpTravelDestinationDTO mpTravelDestinationDTO : mpTravelDestinationDTOList) {
                    mpTravelDestinationDTO.mpTravelAllowanceID = mp_travel_allowanceDTO.iD;
                    mpTravelDestinationDAO.add(mpTravelDestinationDTO);
                }
            }

        } else {
            List<Long> childIdsFromRequest = mpTravelDestinationDAO.getChildIdsFromRequest(request, "mpTravelDestination");
            mpTravelDestinationDAO.deleteChildrenNotInList("mp_travel_allowance", "mp_travel_destination", mp_travel_allowanceDTO.iD, childIdsFromRequest);
            List<Long> childIDsInDatabase = mpTravelDestinationDAO.getChilIds("mp_travel_allowance", "mp_travel_destination", mp_travel_allowanceDTO.iD);
            if (childIdsFromRequest != null) {
                for (int i = 0; i < childIdsFromRequest.size(); i++) {
                    Long childIDFromRequest = childIdsFromRequest.get(i);
                    if (childIDsInDatabase.contains(childIDFromRequest)) {
                        MpTravelDestinationDTO mpTravelDestinationDTO = createMpTravelDestinationDTOByRequestAndIndex(request, i, userDTO, isLanguageEnglish);
                        mpTravelDestinationDTO.mpTravelAllowanceID = mp_travel_allowanceDTO.iD;
                        mpTravelDestinationDAO.update(mpTravelDestinationDTO);
                    } else {
                        MpTravelDestinationDTO mpTravelDestinationDTO = createMpTravelDestinationDTOByRequestAndIndex(request, i, userDTO, isLanguageEnglish);
                        mpTravelDestinationDTO.mpTravelAllowanceID = mp_travel_allowanceDTO.iD;
                        mpTravelDestinationDAO.add(mpTravelDestinationDTO);
                    }
                }
            } else {
                mpTravelDestinationDAO.deleteChildrenByParent(mp_travel_allowanceDTO.iD, "mp_travel_allowance_id");
            }

        }
        return mp_travel_allowanceDTO;
    }

    private List<MpTravelMeetingDTO> createMpTravelMeetingDTOListByRequest(HttpServletRequest request, UserDTO userDTO, boolean isLanguageEnglish) throws Exception {
        List<MpTravelMeetingDTO> mpTravelMeetingDTOList = new ArrayList<>();
        if (request.getParameterValues("mpTravelMeeting.iD") != null) {
            int mpTravelMeetingItemNo = request.getParameterValues("mpTravelMeeting.iD").length;
            for (int index = 0; index < mpTravelMeetingItemNo; index++) {
                MpTravelMeetingDTO mpTravelMeetingDTO = createMpTravelMeetingDTOByRequestAndIndex(request, true, index, userDTO, isLanguageEnglish);
                mpTravelMeetingDTOList.add(mpTravelMeetingDTO);
            }
            return mpTravelMeetingDTOList;
        }
        return null;
    }

    private MpTravelMeetingDTO createMpTravelMeetingDTOByRequestAndIndex(HttpServletRequest request, boolean addFlag, int index, UserDTO userDTO, boolean isLanguageEnglish) throws Exception {
        MpTravelMeetingDTO mpTravelMeetingDTO;
        if (addFlag) {
            mpTravelMeetingDTO = new MpTravelMeetingDTO();
            mpTravelMeetingDTO.insertedBy = userDTO.employee_record_id;
            mpTravelMeetingDTO.insertionTime = new Date().getTime();
        } else {
            mpTravelMeetingDTO = mpTravelMeetingDAO.getDTOByID(Long.parseLong(request.getParameterValues("mpTravelMeeting.iD")[index]));
        }
        String dateString = Jsoup.clean(request.getParameterValues("mpTravelMeeting.meetingDate")[index], Whitelist.simpleText());
        if (dateString.equals("")) {

            throw new Exception(isLanguageEnglish ? "Enter a valid date for reaching destination." : "ভ্রমণ গন্তব্যের সঠিক তারিখ প্রদান করুন");
        }
        mpTravelMeetingDTO.meetingDate = Long.parseLong(dateString);
        mpTravelMeetingDTO.purpose = (Jsoup.clean(request.getParameterValues("mpTravelMeeting.purpose")[index], Whitelist.simpleText()));
        mpTravelMeetingDTO.modifiedBy = userDTO.employee_record_id;
        mpTravelMeetingDTO.lastModificationTime = new Date().getTime();
        return mpTravelMeetingDTO;

    }

    private List<MpTravelDestinationDTO> createMpTravelDestinationDTOListByRequest(HttpServletRequest request, UserDTO userDTO, boolean isLanguageEnglish) throws Exception {
        List<MpTravelDestinationDTO> mpTravelDestinationDTOList = new ArrayList<>();
        if (request.getParameterValues("mpTravelDestination.iD") != null) {
            int mpTravelDestinationItemNo = request.getParameterValues("mpTravelDestination.iD").length;
            for (int index = 0; index < mpTravelDestinationItemNo; index++) {
                MpTravelDestinationDTO mpTravelDestinationDTO = createMpTravelDestinationDTOByRequestAndIndex(request, index, userDTO, isLanguageEnglish);
                mpTravelDestinationDTOList.add(mpTravelDestinationDTO);
            }
            return mpTravelDestinationDTOList;
        }
        return null;
    }

    private MpTravelDestinationDTO createMpTravelDestinationDTOByRequestAndIndex(HttpServletRequest request, int index, UserDTO userDTO, boolean isLanguageEnglish) throws Exception {
        MpTravelDestinationDTO mpTravelDestinationDTO;
        long id = Long.parseLong(request.getParameterValues("mpTravelDestination.iD")[index]);
        if (id == -1) {
            mpTravelDestinationDTO = new MpTravelDestinationDTO();
            mpTravelDestinationDTO.insertedBy = userDTO.employee_record_id;
            mpTravelDestinationDTO.insertionTime = new Date().getTime();
        } else {
            mpTravelDestinationDTO = mpTravelDestinationDAO.getDTOByID(id);
        }
        mpTravelDestinationDTO.destinationAddress = Jsoup.clean(request.getParameterValues("mpTravelDestination.destinationAddress")[index], Whitelist.simpleText());
        mpTravelDestinationDTO.timeCat = Integer.parseInt(Jsoup.clean(request.getParameterValues("mpTravelDestination.timeCat")[index], Whitelist.simpleText()));
        mpTravelDestinationDTO.travelMediumCat = Integer.parseInt(Jsoup.clean(request.getParameterValues("mpTravelDestination.travelMediumCat")[index], Whitelist.simpleText()));
        mpTravelDestinationDTO.travelTypeCat = Integer.parseInt(Jsoup.clean(request.getParameterValues("mpTravelDestination.travelTypeCat")[index], Whitelist.simpleText()));
        mpTravelDestinationDTO.distanceOrSubCost = Double.parseDouble(Jsoup.clean(request.getParameterValues("mpTravelDestination.distanceOrSubCost")[index], Whitelist.simpleText()));

        if (mpTravelDestinationDTO.travelMediumCat == 1) {
            mpTravelDestinationDTO.cost = mpTravelDestinationDTO.distanceOrSubCost * 10.0;
        } else {
            mpTravelDestinationDTO.cost = mpTravelDestinationDTO.distanceOrSubCost * 1.5;
        }
        String dateString = Jsoup.clean(request.getParameterValues("mpTravelDestination.reachDate")[index], Whitelist.simpleText());
        if (dateString.equals("")) {

            throw new Exception(isLanguageEnglish ? "Enter a valid date for reaching destination." : "ভ্রমণ গন্তব্যের সঠিক তারিখ প্রদান করুন");
        }
        mpTravelDestinationDTO.reachDate = Long.parseLong(dateString);
        mpTravelDestinationDTO.modifiedBy = userDTO.employee_record_id;
        mpTravelDestinationDTO.lastModificationTime = new Date().getTime();
        return mpTravelDestinationDTO;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }
        try {
            String actionType = request.getParameter("actionType");
            long electionDetailsId;
            long electionConstituencyId;
            String options;
            String value;
            Map<String, Object> res;
            String employeeName;
            Election_wise_mpDTO election_wise_mpDTO;
            String language = userDTO.languageID == CommonConstant.Language_ID_English ? "English" : "Bangla";
            switch (actionType) {
                case "getMPInfo":
                    electionDetailsId = Long.parseLong(request.getParameter("election_details_id"));
                    electionConstituencyId = Long.parseLong(request.getParameter("election_constituency_id"));

                    res = new HashMap<>();
                    election_wise_mpDTO = Election_wise_mpRepository.getInstance().getByElectionAndConstituency(electionDetailsId, electionConstituencyId);
                    employeeName = Employee_recordsRepository.getInstance().getEmployeeName(election_wise_mpDTO.employeeRecordsId, language);
                    res.put("employeeName", employeeName);
                    response.setContentType("application/json");
                    response.setCharacterEncoding("UTF-8");
                    response.getWriter().println(gson.toJson(res));
                    return;
                case "getElectionConstituency":
                    electionDetailsId = Long.parseLong(request.getParameter("election_details_id"));
                    value = request.getParameter("election_constituency_id");
                    if (value != null && !value.equals("")) {
                        electionConstituencyId = Long.parseLong(value);
                    } else {
                        electionConstituencyId = 0L;
                    }
                    options = Election_constituencyRepository.getInstance().buildOptions(language, electionConstituencyId, electionDetailsId);
                    response.setContentType("text/html; charset=UTF-8");
                    response.getWriter().println(options);
                    return;
                case "getParliamentSession":
                    electionDetailsId = Long.parseLong(request.getParameter("election_details_id"));
                    value = request.getParameter("parliamentSessionId");
                    Long selectedId;
                    if (value != null && !value.equals("")) {
                        selectedId = Long.parseLong(value);
                    } else {
                        selectedId = null;
                    }
                    options = Parliament_sessionRepository.getInstance().buildOptionsWithDate(language, electionDetailsId, selectedId);
                    response.setContentType("text/html; charset=UTF-8");
                    response.getWriter().println(options);
                    return;
                case "getEditPageRecord":
                    electionDetailsId = Long.parseLong(request.getParameter("election_details_id"));
                    value = request.getParameter("election_constituency_id");
                    if (value != null && !value.equals("")) {
                        electionConstituencyId = Long.parseLong(value);
                    } else {
                        electionConstituencyId = 0L;
                    }
                    res = new HashMap<>();
                    options = Election_constituencyRepository.getInstance().buildOptions(language, electionConstituencyId, electionDetailsId);
                    res.put("getElectionConstituency", options);
                    election_wise_mpDTO = Election_wise_mpRepository.getInstance().getByElectionAndConstituency(electionDetailsId, electionConstituencyId);
                    employeeName = Employee_recordsRepository.getInstance().getEmployeeName(election_wise_mpDTO.employeeRecordsId, language);
                    res.put("employeeName", employeeName);

                    value = request.getParameter("parliamentSessionId");
                    Long parliamentSessionId;
                    if (value != null && !value.equals("")) {
                        parliamentSessionId = Long.parseLong(value);
                    } else {
                        parliamentSessionId = null;
                    }
                    if (parliamentSessionId != null && parliamentSessionId == -1) {
                        options = "";
                    } else {
                        options = Parliament_sessionRepository.getInstance().buildOptionsWithDate(language, electionDetailsId, parliamentSessionId);
                    }
                    res.put("getParliamentSession", options);
                    response.setContentType("application/json");
                    response.setCharacterEncoding("UTF-8");
                    response.getWriter().println(gson.toJson(res));
                    return;
                case "form":
                    logger.debug("form requested");
                    if (Utils.checkPermission(userDTO, getViewMenuConstants()) && getViewPagePermission(request)) {
                        Mp_travel_allowanceDTO mp_travel_allowanceDTO = Mp_travel_allowanceDAO.getInstance().getDTOByID(getId(request));
                        request.setAttribute(BaseServlet.DTO_FOR_JSP, mp_travel_allowanceDTO);
                        finalize(request);
                        request.getRequestDispatcher(commonPartOfDispatchURL() + "Form.jsp?ID=" + getId(request)).forward(request, response);
                        return;
                    }
                    break;
                default:
                    super.doGet(request, response);
                    return;
            }
        } catch (Exception e) {
            logger.error(e);
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }
}