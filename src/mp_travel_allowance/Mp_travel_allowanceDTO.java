package mp_travel_allowance;

import sessionmanager.SessionConstants;
import util.CommonDTO;

import java.util.ArrayList;
import java.util.List;


public class Mp_travel_allowanceDTO extends CommonDTO {

    public boolean isSession = false;
    public boolean isMeeting = false;
    public long parliamentSessionId = -1;
    public long startDate = System.currentTimeMillis();
    public int startTimeCat = -1;
    public long endDate = System.currentTimeMillis();
    public int endTimeCat = -1;
    public String startAddress = "";
    public long employeeRecordsId = -1;
    public int dailyAttendant = 0;
    public int dailyAttendantAmount = 0;
    public int dailyStay = 0;
    public int dailyStayAmount = 0;
    public int travelAttendant = 0;
    public int travelAttendantAmount = 0;
    public int travelStay = 0;
    public int travelStayAmount = 0;
    public int netAmount = 0;
    public String mpNameEn = "";
    public String mpNameBn = "";
    public String mpUserName = "";
    public long electionWiseMpId = -1;
    public long electionDetailsId = -1;
    public long electionConstituencyId = -1;
    public String electionConstituencyBn = "";
    public String electionConstituencyEn = "";
    public long insertedBy = -1;
    public long insertionTime = SessionConstants.MIN_DATE;
    public long modifiedBy = -1;

    public List<MpTravelMeetingDTO> mpTravelMeetingDTOList = new ArrayList<>();
    public List<MpTravelDestinationDTO> mpTravelDestinationDTOList = new ArrayList<>();

    @Override
    public String toString() {
        return "Mp_travel_allowanceDTO{" +
                "isSession=" + isSession +
                ", isMeeting=" + isMeeting +
                ", parliamentSessionId=" + parliamentSessionId +
                ", startDate=" + startDate +
                ", startTimeCat=" + startTimeCat +
                ", endDate=" + endDate +
                ", endTimeCat=" + endTimeCat +
                ", startAddress='" + startAddress + '\'' +
                ", employeeRecordsId=" + employeeRecordsId +
                ", dailyAttendant=" + dailyAttendant +
                ", dailyAttendantAmount=" + dailyAttendantAmount +
                ", dailyStay=" + dailyStay +
                ", dailyStayAmount=" + dailyStayAmount +
                ", travelAttendant=" + travelAttendant +
                ", travelAttendantAmount=" + travelAttendantAmount +
                ", travelStay=" + travelStay +
                ", travelStayAmount=" + travelStayAmount +
                ", net_amount=" + netAmount +
                ", mpNameEn='" + mpNameEn + '\'' +
                ", mpNameBn='" + mpNameBn + '\'' +
                ", mpUserName='" + mpUserName + '\'' +
                ", electionWiseMpId=" + electionWiseMpId +
                ", electionDetailsId=" + electionDetailsId +
                ", electionConstituencyId=" + electionConstituencyId +
                ", electionConstituencyBn='" + electionConstituencyBn + '\'' +
                ", electionConstituencyEn='" + electionConstituencyEn + '\'' +
                ", insertedBy=" + insertedBy +
                ", insertionTime=" + insertionTime +
                ", modifiedBy=" + modifiedBy +
                ", mpTravelMeetingDTOList=" + mpTravelMeetingDTOList +
                ", mpTravelDestinationDTOList=" + mpTravelDestinationDTOList +
                '}';
    }

}