package mp_travel_allowance;

import common.CommonDAOService;
import org.apache.log4j.Logger;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings({"unused", "Duplicates"})
public class MpTravelDestinationDAO implements CommonDAOService<MpTravelDestinationDTO> {
    private static final Logger logger = Logger.getLogger(MpTravelDestinationDAO.class);

    private static final String addSqlQuery = "INSERT INTO {tableName} (mp_travel_allowance_ID,time_cat,destination_address,travel_medium_cat,travel_type_cat,distance_or_sub_cost,cost,reach_date,"
            .concat("modified_by,lastModificationTime,inserted_by,insertion_time,isDeleted,ID)")
            .concat(" VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

    private static final String updateSqlQuery = "UPDATE {tableName} SET mp_travel_allowance_ID=?,time_cat=?,destination_address=?,travel_medium_cat=?,travel_type_cat=?,distance_or_sub_cost=?,cost=?,reach_date=?,"
            .concat("modified_by=?,lastModificationTime = ?, isDeleted=? WHERE ID = ?");

    private static final String getByMPTravelAllowanceIdSqlQuery = "SELECT * FROM %s WHERE mp_travel_allowance_ID = %d AND isDeleted = 0 ORDER BY ID";

    private final Map<String, String> searchMap = new HashMap<>();

    private MpTravelDestinationDAO() {
        searchMap.put("mp_travel_allowance_ID", " and (mp_travel_allowance_ID like ?)");
        searchMap.put("travel_medium_cat", " and (travel_medium_cat = ?)");
        searchMap.put("travel_type_cat", " and (travel_type_cat = ?)");
        searchMap.put("reach_date_start", " and (reach_date >= ?)");
        searchMap.put("reach_date_end", " and (reach_date <= ?)");
        searchMap.put("time_cat", " and (time_cat = ?)");
        searchMap.put("inserted_by", " and (inserted_by like ?)");
        searchMap.put("modified_by", " and (modified_by like ?)");
    }

    private static class LazyLoader {
        static final MpTravelDestinationDAO INSTANCE = new MpTravelDestinationDAO();
    }

    public static MpTravelDestinationDAO getInstance() {
        return LazyLoader.INSTANCE;
    }

    @Override
    public void set(PreparedStatement ps, MpTravelDestinationDTO dto, boolean isInsert) throws SQLException {
        int index = 0;
        ps.setLong(++index, dto.mpTravelAllowanceID);
        ps.setInt(++index, dto.timeCat);
        ps.setString(++index, dto.destinationAddress);
        ps.setInt(++index, dto.travelMediumCat);
        ps.setInt(++index, dto.travelTypeCat);
        ps.setDouble(++index, dto.distanceOrSubCost);
        ps.setDouble(++index, dto.cost);
        ps.setLong(++index, dto.reachDate);
        ps.setLong(++index, dto.modifiedBy);
        ps.setLong(++index, dto.lastModificationTime);
        if (isInsert) {
            ps.setLong(++index, dto.insertedBy);
            ps.setLong(++index, dto.insertionTime);
        }
        ps.setInt(++index, dto.isDeleted);
        ps.setLong(++index, dto.iD);
    }

    @Override
    public MpTravelDestinationDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            MpTravelDestinationDTO dto = new MpTravelDestinationDTO();
            dto.iD = rs.getLong("ID");
            dto.mpTravelAllowanceID = rs.getLong("mp_travel_allowance_ID");
            dto.destinationAddress = rs.getString("destination_address");
            dto.timeCat = rs.getInt("time_cat");
            dto.travelMediumCat = rs.getInt("travel_medium_cat");
            dto.travelTypeCat = rs.getInt("travel_type_cat");
            dto.distanceOrSubCost = rs.getDouble("distance_or_sub_cost");
            dto.cost = rs.getDouble("cost");
            dto.reachDate = rs.getLong("reach_date");
            dto.insertionTime = rs.getLong("insertion_time");
            dto.insertedBy = rs.getLong("inserted_by");
            dto.modifiedBy = rs.getLong("modified_by");
            dto.isDeleted = rs.getInt("isDeleted");
            dto.lastModificationTime = rs.getLong("lastModificationTime");
            return dto;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    public MpTravelDestinationDTO getDTOByID(long id) {
        return getDTOFromID(id);
    }

    @Override
    public String getTableName() {
        return "mp_travel_destination";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    @Override
    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((MpTravelDestinationDTO) commonDTO, addSqlQuery, true);
    }

    @Override
    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((MpTravelDestinationDTO) commonDTO, updateSqlQuery, false);
    }

    public List<MpTravelDestinationDTO> getByMPTravelAllowanceId(long MPtravelAllowanceId) {
        return getDTOs(String.format(getByMPTravelAllowanceIdSqlQuery, getTableName(), MPtravelAllowanceId));
    }
}
	