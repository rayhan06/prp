package mp_travel_allowance;

import common.CommonDAOService;
import org.apache.log4j.Logger;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings({"unused", "Duplicates"})
public class MpTravelMeetingDAO implements CommonDAOService<MpTravelMeetingDTO> {
    private static final Logger logger = Logger.getLogger(MpTravelMeetingDAO.class);

    private static final String addSqlQuery = "INSERT INTO {tableName} (mp_travel_allowance_ID,meeting_date,purpose,"
            .concat("modified_by,lastModificationTime,inserted_by,insertion_time,isDeleted,ID)")
            .concat(" VALUES(?,?,?,?,?,?,?,?,?)");

    private static final String updateSqlQuery = "UPDATE {tableName} SET mp_travel_allowance_ID=?,meeting_date=?,purpose=?,"
            .concat("modified_by=?,lastModificationTime = ?, isDeleted=? WHERE ID = ?");

    private static final String getByMPTravelAllowanceIdSqlQuery = "SELECT * FROM %s WHERE mp_travel_allowance_ID = %d AND isDeleted = 0 ORDER BY ID";

    private final Map<String, String> searchMap = new HashMap<>();

    private MpTravelMeetingDAO() {
        searchMap.put("mp_travel_allowance_ID", " and (mp_travel_allowance_ID like ?)");
        searchMap.put("meeting_date_start", " and (meeting_date >= ?)");
        searchMap.put("meeting_date_end", " and (meeting_date <= ?)");
        searchMap.put("purpose", " and (purpose like ?)");
        searchMap.put("inserted_by", " and (inserted_by like ?)");
        searchMap.put("modified_by", " and (modified_by like ?)");
    }

    private static class LazyLoader {
        static final MpTravelMeetingDAO INSTANCE = new MpTravelMeetingDAO();
    }

    public static MpTravelMeetingDAO getInstance() {
        return LazyLoader.INSTANCE;
    }

    @Override
    public void set(PreparedStatement ps, MpTravelMeetingDTO dto, boolean isInsert) throws SQLException {
        int index = 0;
        ps.setLong(++index, dto.mpTravelAllowanceID);
        ps.setLong(++index, dto.meetingDate);
        ps.setString(++index, dto.purpose);
        ps.setLong(++index, dto.modifiedBy);
        ps.setLong(++index, dto.lastModificationTime);
        if (isInsert) {
            ps.setLong(++index, dto.insertedBy);
            ps.setLong(++index, dto.insertionTime);
        }
        ps.setInt(++index, dto.isDeleted);
        ps.setLong(++index, dto.iD);
    }

    @Override
    public MpTravelMeetingDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            MpTravelMeetingDTO dto = new MpTravelMeetingDTO();
            dto.iD = rs.getLong("ID");
            dto.mpTravelAllowanceID = rs.getLong("mp_travel_allowance_ID");
            dto.meetingDate = rs.getLong("meeting_date");
            dto.purpose = rs.getString("purpose");
            dto.insertionTime = rs.getLong("insertion_time");
            dto.insertedBy = rs.getLong("inserted_by");
            dto.modifiedBy = rs.getLong("modified_by");
            dto.isDeleted = rs.getInt("isDeleted");
            dto.lastModificationTime = rs.getLong("lastModificationTime");
            return dto;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    public MpTravelMeetingDTO getDTOByID(long id) {
        return getDTOFromID(id);
    }

    @Override
    public String getTableName() {
        return "mp_travel_meeting";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    @Override
    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((MpTravelMeetingDTO) commonDTO, addSqlQuery, true);
    }

    @Override
    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((MpTravelMeetingDTO) commonDTO, updateSqlQuery, false);
    }

    public List<MpTravelMeetingDTO> getByMPTravelAllowanceId(long MPtravelAllowanceId) {
        return getDTOs(String.format(getByMPTravelAllowanceIdSqlQuery, getTableName(), MPtravelAllowanceId));
    }
}