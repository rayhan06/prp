package mp_travel_allowance;

import util.CommonMaps;


public class MpTravelMeetingMAPS extends CommonMaps {
    public MpTravelMeetingMAPS(String tableName) {


        java_SQL_map.put("mp_travel_allowance_ID".toLowerCase(), "mpTravelAllowanceID".toLowerCase());
        java_SQL_map.put("meeting_date".toLowerCase(), "meetingDate".toLowerCase());
        java_SQL_map.put("purpose".toLowerCase(), "purpose".toLowerCase());
        java_SQL_map.put("inserted_by".toLowerCase(), "insertedBy".toLowerCase());
        java_SQL_map.put("insertion_time".toLowerCase(), "insertionTime".toLowerCase());
        java_SQL_map.put("modified_by".toLowerCase(), "modifiedBy".toLowerCase());

        java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
        java_Text_map.put("Mp Travel Allowance ID".toLowerCase(), "mpTravelAllowanceID".toLowerCase());
        java_Text_map.put("Meeting Date".toLowerCase(), "meetingDate".toLowerCase());
        java_Text_map.put("Purpose".toLowerCase(), "purpose".toLowerCase());
        java_Text_map.put("Inserted By".toLowerCase(), "insertedBy".toLowerCase());
        java_Text_map.put("Insertion Time".toLowerCase(), "insertionTime".toLowerCase());
        java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
        java_Text_map.put("Modified By".toLowerCase(), "modifiedBy".toLowerCase());
        java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

    }

}