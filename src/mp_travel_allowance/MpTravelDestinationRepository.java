package mp_travel_allowance;

import com.google.gson.Gson;
import org.apache.log4j.Logger;
import repository.Repository;
import repository.RepositoryManager;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

@SuppressWarnings({"Duplicates"})
public class MpTravelDestinationRepository implements Repository {
    MpTravelDestinationDAO mptraveldestinationDAO;

    static Logger logger = Logger.getLogger(MpTravelDestinationRepository.class);
    Map<Long, MpTravelDestinationDTO> mapOfMpTravelDestinationDTOToiD;
    Map<Long, Set<MpTravelDestinationDTO>> mapOfMpTravelDestinationDTOTompTravelAllowanceID;
    Gson gson;


    private MpTravelDestinationRepository() {
        mptraveldestinationDAO = MpTravelDestinationDAO.getInstance();
        mapOfMpTravelDestinationDTOToiD = new ConcurrentHashMap<>();
        mapOfMpTravelDestinationDTOTompTravelAllowanceID = new ConcurrentHashMap<>();
        gson = new Gson();
        RepositoryManager.getInstance().addRepository(this);
    }

    private static class LazyLoader {
        final static MpTravelDestinationRepository INSTANCE = new MpTravelDestinationRepository();
    }

    public static MpTravelDestinationRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

    public void reload(boolean reloadAll) {
        try {
            List<MpTravelDestinationDTO> mptraveldestinationDTOs = mptraveldestinationDAO.getAllDTOs(reloadAll);
            for (MpTravelDestinationDTO mptraveldestinationDTO : mptraveldestinationDTOs) {
                MpTravelDestinationDTO oldMpTravelDestinationDTO = getMpTravelDestinationDTOByiD(mptraveldestinationDTO.iD);
                if (oldMpTravelDestinationDTO != null) {
                    mapOfMpTravelDestinationDTOToiD.remove(oldMpTravelDestinationDTO.iD);

                    if (mapOfMpTravelDestinationDTOTompTravelAllowanceID.containsKey(oldMpTravelDestinationDTO.mpTravelAllowanceID)) {
                        mapOfMpTravelDestinationDTOTompTravelAllowanceID.get(oldMpTravelDestinationDTO.mpTravelAllowanceID).remove(oldMpTravelDestinationDTO);
                    }
                    if (mapOfMpTravelDestinationDTOTompTravelAllowanceID.get(oldMpTravelDestinationDTO.mpTravelAllowanceID).isEmpty()) {
                        mapOfMpTravelDestinationDTOTompTravelAllowanceID.remove(oldMpTravelDestinationDTO.mpTravelAllowanceID);
                    }


                }
                if (mptraveldestinationDTO.isDeleted == 0) {

                    mapOfMpTravelDestinationDTOToiD.put(mptraveldestinationDTO.iD, mptraveldestinationDTO);

                    if (!mapOfMpTravelDestinationDTOTompTravelAllowanceID.containsKey(mptraveldestinationDTO.mpTravelAllowanceID)) {
                        mapOfMpTravelDestinationDTOTompTravelAllowanceID.put(mptraveldestinationDTO.mpTravelAllowanceID, new HashSet<>());
                    }
                    mapOfMpTravelDestinationDTOTompTravelAllowanceID.get(mptraveldestinationDTO.mpTravelAllowanceID).add(mptraveldestinationDTO);

                }
            }

        } catch (Exception e) {
            logger.debug("FATAL", e);
        }
    }

    public MpTravelDestinationDTO clone(MpTravelDestinationDTO dto) {
        String raw = gson.toJson(dto);
        return gson.fromJson(raw, MpTravelDestinationDTO.class);
    }


    public List<MpTravelDestinationDTO> getMpTravelDestinationList() {
        return new ArrayList<>(this.mapOfMpTravelDestinationDTOToiD.values());
    }

    public List<MpTravelDestinationDTO> copyMpTravelDestinationList() {
        List<MpTravelDestinationDTO> mptraveldestinations = getMpTravelDestinationList();
        return mptraveldestinations
                .stream()
                .map(this::clone)
                .collect(Collectors.toList());
    }


    public MpTravelDestinationDTO getMpTravelDestinationDTOByiD(long iD) {
        return mapOfMpTravelDestinationDTOToiD.get(iD);
    }

    public MpTravelDestinationDTO copyMpTravelDestinationDTOByiD(long iD) {
        return clone(mapOfMpTravelDestinationDTOToiD.get(iD));
    }


    public List<MpTravelDestinationDTO> getMpTravelDestinationDTOBympTravelAllowanceID(long mpTravelAllowanceID) {
        return new ArrayList<>(mapOfMpTravelDestinationDTOTompTravelAllowanceID.getOrDefault(mpTravelAllowanceID, new HashSet<>()));
    }

    public List<MpTravelDestinationDTO> copyMpTravelDestinationDTOBympTravelAllowanceID(long mpTravelAllowanceID) {
        List<MpTravelDestinationDTO> mptraveldestinations = getMpTravelDestinationDTOBympTravelAllowanceID(mpTravelAllowanceID);
        return mptraveldestinations
                .stream()
                .map(this::clone)
                .collect(Collectors.toList());
    }


    @Override
    public String getTableName() {
        return mptraveldestinationDAO.getTableName();
    }
}


