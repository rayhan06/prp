package mp_travel_allowance;

import com.google.gson.Gson;
import fiscal_year.Fiscal_yearDTO;
import org.apache.log4j.Logger;
import repository.Repository;
import repository.RepositoryManager;
import util.LockManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


public class Mp_travel_allowanceRepository implements Repository {
    Mp_travel_allowanceDAO mp_travel_allowanceDAO;

    static Logger logger = Logger.getLogger(Mp_travel_allowanceRepository.class);
    Map<Long, Mp_travel_allowanceDTO> mapOfMp_travel_allowanceDTOToiD;
    Gson gson;


    private Mp_travel_allowanceRepository() {
        mp_travel_allowanceDAO = Mp_travel_allowanceDAO.getInstance();
        mapOfMp_travel_allowanceDTOToiD = new ConcurrentHashMap<>();
        gson = new Gson();
        RepositoryManager.getInstance().addRepository(this);
    }

    private static class LazyLoader {
        final static Mp_travel_allowanceRepository INSTANCE = new Mp_travel_allowanceRepository();
    }

    public static Mp_travel_allowanceRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

    public void reload(boolean reloadAll) {
        try {
            List<Mp_travel_allowanceDTO> mp_travel_allowanceDTOs = mp_travel_allowanceDAO.getAllDTOs(reloadAll);
            for (Mp_travel_allowanceDTO mp_travel_allowanceDTO : mp_travel_allowanceDTOs) {
                Mp_travel_allowanceDTO oldMp_travel_allowanceDTO = getMp_travel_allowanceDTOByiD(mp_travel_allowanceDTO.iD);
                if (oldMp_travel_allowanceDTO != null) {
                    mapOfMp_travel_allowanceDTOToiD.remove(oldMp_travel_allowanceDTO.iD);
                }
                if (mp_travel_allowanceDTO.isDeleted == 0) {
                    mapOfMp_travel_allowanceDTOToiD.put(mp_travel_allowanceDTO.iD, mp_travel_allowanceDTO);

                }
            }

        } catch (Exception e) {
            logger.debug("FATAL", e);
        }
    }

    public Mp_travel_allowanceDTO clone(Mp_travel_allowanceDTO dto) {
        String raw = gson.toJson(dto);
        return gson.fromJson(raw, Mp_travel_allowanceDTO.class);
    }


    public List<Mp_travel_allowanceDTO> getMp_travel_allowanceList() {
        return new ArrayList<>(this.mapOfMp_travel_allowanceDTOToiD.values());
    }


    public Mp_travel_allowanceDTO getMp_travel_allowanceDTOByiD(long iD) {
        if (mapOfMp_travel_allowanceDTOToiD.get(iD) == null) {
            synchronized (LockManager.getLock(iD + "MPTAR")) {
                if (mapOfMp_travel_allowanceDTOToiD.get(iD) == null) {
                    Mp_travel_allowanceDTO dto = mp_travel_allowanceDAO.getDTOByID(iD);
                    if (dto != null) {
                        mapOfMp_travel_allowanceDTOToiD.put(dto.iD, dto);
                    }
                }
            }
        }
        return mapOfMp_travel_allowanceDTOToiD.get(iD);
    }


    @Override
    public String getTableName() {
        return mp_travel_allowanceDAO.getTableName();
    }
}


