package mp_travel_allowance;

import common.CommonDAOService;
import org.apache.log4j.Logger;
import pb.CatRepository;
import pb.CategoryLanguageModel;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings({"unused", "Duplicates", "unchecked"})
public class Mp_travel_allowanceDAO implements CommonDAOService<Mp_travel_allowanceDTO> {
    private static final Logger logger = Logger.getLogger(Mp_travel_allowanceDAO.class);

    private static final String addSqlQuery = "INSERT INTO {tableName} (employee_records_id,is_session,parliament_session_id,is_meeting,start_date,start_time_cat,end_date,end_time_cat,"
            .concat("daily_attendant,daily_attendant_amount,daily_stay,daily_stay_amount,travel_attendant,travel_attendant_amount,travel_stay,travel_stay_amount,net_amount,")
            .concat("start_address,mp_name_en, mp_name_bn,mp_user_name, election_wise_mp_id, election_details_id, election_constituency_id, election_constituency_en, election_constituency_bn,")
            .concat("search_column,modified_by,lastModificationTime,inserted_by,insertion_time,isDeleted,ID) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

    private static final String updateSqlQuery = "UPDATE {tableName} SET employee_records_id=?,is_session=?,parliament_session_id=?,is_meeting=?,start_date=?,start_time_cat=?,end_date=?,end_time_cat=?,"
            .concat("daily_attendant=?,daily_attendant_amount=?,daily_stay=?,daily_stay_amount=?,travel_attendant=?,travel_attendant_amount=?,travel_stay=?,travel_stay_amount=?,net_amount=?,")
            .concat("start_address=?,mp_name_en=?, mp_name_bn=?,mp_user_name=?, election_wise_mp_id=?, election_details_id=?, election_constituency_id=?, election_constituency_en=?, election_constituency_bn=?,")
            .concat("search_column=?,modified_by=?,lastModificationTime=? WHERE ID=?");

    private static final String getTotalAllowanceSqlQuery =
            "SELECT * FROM mp_travel_allowance WHERE election_details_id=%d AND election_constituency_id=%d AND insertion_time>=%d AND insertion_time<=%d AND isDeleted=0";

    private static final Map<String, String> searchMap = new HashMap<>();

    private Mp_travel_allowanceDAO() {
        searchMap.put("election_details_id", " and (election_details_id = ?)");
        searchMap.put("election_constituency_id", " and (election_constituency_id = ?)");
        searchMap.put("AnyField", " and (search_column like ?)");
    }

    private static class LazyLoader {
        static final Mp_travel_allowanceDAO INSTANCE = new Mp_travel_allowanceDAO();
    }

    public static Mp_travel_allowanceDAO getInstance() {
        return LazyLoader.INSTANCE;
    }

    public void setSearchColumn(Mp_travel_allowanceDTO dto) {
        CategoryLanguageModel startTimeModel = CatRepository.getInstance().getCategoryLanguageModel("travel_time", dto.startTimeCat);
        CategoryLanguageModel endTimeModel = CatRepository.getInstance().getCategoryLanguageModel("travel_time", dto.endTimeCat);

        dto.searchColumn = startTimeModel.englishText + " " + startTimeModel.banglaText + " "
                + endTimeModel.englishText + " " + endTimeModel.banglaText + " "
                + dto.mpNameEn + " " + dto.mpNameBn + " "
                + dto.mpUserName + " " + dto.electionConstituencyBn + " "+ dto.electionConstituencyEn + " ";
    }

    @Override
    public void set(PreparedStatement ps, Mp_travel_allowanceDTO dto, boolean isInsert) throws SQLException {
        setSearchColumn(dto);
        int index = 0;
        ps.setLong(++index, dto.employeeRecordsId);
        ps.setBoolean(++index, dto.isSession);
        ps.setLong(++index, dto.parliamentSessionId);
        ps.setBoolean(++index, dto.isMeeting);
        ps.setLong(++index, dto.startDate);
        ps.setInt(++index, dto.startTimeCat);
        ps.setLong(++index, dto.endDate);
        ps.setInt(++index, dto.endTimeCat);
        ps.setInt(++index, dto.dailyAttendant);
        ps.setInt(++index, dto.dailyAttendantAmount);
        ps.setInt(++index, dto.dailyStay);
        ps.setInt(++index, dto.dailyStayAmount);
        ps.setInt(++index, dto.travelAttendant);
        ps.setInt(++index, dto.travelAttendantAmount);
        ps.setInt(++index, dto.travelStay);
        ps.setInt(++index, dto.travelStayAmount);
        ps.setInt(++index, dto.netAmount);
        ps.setString(++index, dto.startAddress);
        ps.setString(++index, dto.mpNameEn);
        ps.setString(++index, dto.mpNameBn);
        ps.setString(++index, dto.mpUserName);
        ps.setLong(++index, dto.electionWiseMpId);
        ps.setLong(++index, dto.electionDetailsId);
        ps.setLong(++index, dto.electionConstituencyId);
        ps.setString(++index, dto.electionConstituencyEn);
        ps.setString(++index, dto.electionConstituencyBn);
        ps.setString(++index, dto.searchColumn);
        ps.setLong(++index, dto.modifiedBy);
        ps.setLong(++index, dto.lastModificationTime);
        if (isInsert) {
            ps.setLong(++index, dto.insertedBy);
            ps.setLong(++index, dto.insertionTime);
            ps.setInt(++index, 0);
        }
        ps.setLong(++index, dto.iD);
    }

    @Override
    public Mp_travel_allowanceDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Mp_travel_allowanceDTO dto = new Mp_travel_allowanceDTO();
            dto.iD = rs.getLong("ID");
            dto.isSession = rs.getBoolean("is_session");
            dto.isMeeting = rs.getBoolean("is_meeting");
            dto.parliamentSessionId = rs.getLong("parliament_session_id");
            dto.startDate = rs.getLong("start_date");
            dto.startTimeCat = rs.getInt("start_time_cat");
            dto.endDate = rs.getLong("end_date");
            dto.endTimeCat = rs.getInt("end_time_cat");
            dto.dailyAttendant = rs.getInt("daily_attendant");
            dto.dailyAttendantAmount = rs.getInt("daily_attendant_amount");
            dto.dailyStay = rs.getInt("daily_stay");
            dto.dailyStayAmount = rs.getInt("daily_stay_amount");
            dto.travelAttendant = rs.getInt("travel_attendant");
            dto.travelAttendantAmount = rs.getInt("travel_attendant_amount");
            dto.travelStay = rs.getInt("travel_stay");
            dto.travelStayAmount = rs.getInt("travel_stay_amount");
            dto.netAmount = rs.getInt("net_amount");
            dto.searchColumn = rs.getString("search_column");
            dto.startAddress = rs.getString("start_address");
            dto.employeeRecordsId = rs.getLong("employee_records_id");
            dto.mpNameEn = rs.getString("mp_name_en");
            dto.mpNameBn = rs.getString("mp_name_bn");
            dto.mpUserName = rs.getString("mp_user_name");
            dto.electionWiseMpId = rs.getLong("election_wise_mp_id");
            dto.electionDetailsId = rs.getLong("election_details_id");
            dto.electionConstituencyId = rs.getLong("election_constituency_id");
            dto.electionConstituencyBn = rs.getString("election_constituency_bn");
            dto.electionConstituencyEn = rs.getString("election_constituency_en");
            dto.insertedBy = rs.getLong("inserted_by");
            dto.insertionTime = rs.getLong("insertion_time");
            dto.isDeleted = rs.getInt("isDeleted");
            dto.modifiedBy = rs.getLong("modified_by");
            dto.lastModificationTime = rs.getLong("lastModificationTime");
            return dto;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    public Mp_travel_allowanceDTO getDTOByID(long id) {
        Mp_travel_allowanceDTO mp_travel_allowanceDTO = getDTOFromID(id);
        if (mp_travel_allowanceDTO != null) {
            mp_travel_allowanceDTO.mpTravelMeetingDTOList =
                    (List<MpTravelMeetingDTO>) MpTravelMeetingDAO.getInstance()
                                                                 .getDTOsByParent("mp_travel_allowance_ID", mp_travel_allowanceDTO.iD);
            mp_travel_allowanceDTO.mpTravelDestinationDTOList =
                    (List<MpTravelDestinationDTO>) MpTravelDestinationDAO.getInstance()
                                                                         .getDTOsByParent("mp_travel_allowance_ID", mp_travel_allowanceDTO.iD);
        }
        return mp_travel_allowanceDTO;
    }

    @Override
    public String getTableName() {
        return "mp_travel_allowance";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    @Override
    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Mp_travel_allowanceDTO) commonDTO, addSqlQuery, true);
    }

    @Override
    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Mp_travel_allowanceDTO) commonDTO, updateSqlQuery, false);
    }

    public long getTotalAllowance(long electionDetailsId, long electionConstituencyId, long startDate, long endDate) {
        List<Mp_travel_allowanceDTO> travelAllowanceDTOs = getDTOs(String.format(
                getTotalAllowanceSqlQuery, electionDetailsId, electionConstituencyId, startDate, endDate
        ));
        return travelAllowanceDTOs.stream()
                                  .mapToLong(travelAllowanceDTO -> travelAllowanceDTO.netAmount)
                                  .sum();
    }
}