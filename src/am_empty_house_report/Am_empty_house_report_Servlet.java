package am_empty_house_report;

import language.LC;
import language.LM;
import login.LoginDTO;
import pbReport.ReportService;
import permission.MenuConstants;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.ReportRequestHandler;
import util.UtilCharacter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/Am_empty_house_report_Servlet")
public class Am_empty_house_report_Servlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    String[][] Criteria =
            {
                    {"criteria", "tr", "am_house_old_new_cat", "=", "", "int", "", "", "any", "amHouseOldNewCat", LC.AM_HOUSE_ALLOCATION_HISTORY_REPORT_WHERE_AMHOUSEOLDNEWCAT + ""},
                    {"criteria", "tr", "am_house_location_cat", "=", "AND", "int", "", "", "any", "amHouseLocationCat", LC.AM_HOUSE_ALLOCATION_HISTORY_REPORT_WHERE_AMHOUSELOCATIONCAT + ""},
                    {"criteria", "tr", "am_house_class_cat", "=", "AND", "int", "", "", "any", "amHouseClassCat", LC.AM_HOUSE_ALLOCATION_HISTORY_REPORT_WHERE_AMHOUSECLASSCAT + ""},
                    {"criteria", "tr", "house_number", "=", "AND", "String", "", "", "any", "houseNumber", LC.AM_HOUSE_ALLOCATION_HISTORY_REPORT_WHERE_HOUSEID + ""},
                    {"criteria", "tr", "status", "=", "AND", "String", "", "", "18", "isDeleted", LC.AM_HOUSE_ALLOCATION_HISTORY_REPORT_WHERE_ISDELETED + ""},
                    {"criteria", "tr", "isDeleted", "=", "AND", "String", "", "", "0", "isDeleted", LC.AM_HOUSE_ALLOCATION_HISTORY_REPORT_WHERE_ISDELETED + ""}
            };

    String[][] Display =
            {
                    {"display", "tr", "am_house_old_new_cat", "cat", ""},
                    {"display", "tr", "am_house_location_cat", "cat", ""},
                    {"display", "tr", "am_house_class_cat", "cat", ""},
                    {"display", "tr", "house_number", "text", ""},
                    {"display", "tr", "status", "am_office_status_check", ""}
            };

    String GroupBy = "";
    String OrderBY = "";

    ReportRequestHandler reportRequestHandler;

    public Am_empty_house_report_Servlet() {

    }

    private final ReportService reportService = new ReportService();

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        String language = "english";
        if (userDTO != null && userDTO.languageID == SessionConstants.BANGLA) {
            language = "bangla";
        }

        String actionType = request.getParameter("actionType");

        System.out.println("In ssservlet doget, actiontype = " + actionType);

        String sql = "am_house tr";

        Display[0][4] = LM.getText(LC.AM_HOUSE_ALLOCATION_HISTORY_REPORT_SELECT_AMHOUSEOLDNEWCAT, loginDTO);
        Display[1][4] = LM.getText(LC.AM_HOUSE_ALLOCATION_HISTORY_REPORT_SELECT_AMHOUSELOCATIONCAT, loginDTO);
        Display[2][4] = LM.getText(LC.AM_HOUSE_ALLOCATION_HISTORY_REPORT_SELECT_AMHOUSECLASSCAT, loginDTO);
        Display[3][4] = LM.getText(LC.AM_HOUSE_ALLOCATION_HISTORY_REPORT_SELECT_HOUSEID, loginDTO);
        Display[4][4] = LM.getText(LC.AM_HOUSE_ALLOCATION_HISTORY_REPORT_SELECT_STATUS, loginDTO);


        String reportName = UtilCharacter.getDataByLanguage(language, "খালি বাসার রিপোর্ট", "Empty House Report");

        reportRequestHandler = new ReportRequestHandler(null, Criteria, Display, GroupBy, OrderBY, sql, reportService);

        reportRequestHandler.handleReportGet(request, response, userDTO, "am_empty_house_report",
                MenuConstants.AM_HOUSE_ALLOCATION_HISTORY_REPORT_DETAILS, language, reportName, "am_empty_house_report");
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }
}