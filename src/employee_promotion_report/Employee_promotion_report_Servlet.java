package employee_promotion_report;


import education_level.DegreeExamRepository;
import education_level.Education_levelRepository;
import employee_education_info.Employee_education_infoDAO;
import employee_education_info.Employee_education_infoDTO;
import employee_office_report.Employee_office_report_Servlet;
import employee_offices.EmployeeOfficeDTO;
import employee_offices.EmployeeOfficesDAO;
import employee_service_history.Employee_service_historyDAO;
import employee_service_history.Employee_service_historyDTO;
import language.LC;
import language.LM;
import login.LoginDTO;
import office_unit_organograms.OfficeUnitOrganogramsRepository;
import office_units.Office_unitsRepository;
import org.apache.log4j.Logger;
import pb.Utils;
import pbReport.PBReportUtils;
import pbReport.ReportCommonService;
import permission.MenuConstants;
import role.PermissionRepository;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.StringUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;

@SuppressWarnings({"unused", "Duplicates"})
@WebServlet("/Employee_promotion_report_Servlet")
public class Employee_promotion_report_Servlet extends HttpServlet implements ReportCommonService {
    private static final Logger logger = Logger.getLogger(Employee_office_report_Servlet.class);

    private static final long serialVersionUID = 1L;

    private static final Set<String> searchParam =
            new HashSet<>(Arrays.asList("startDate", "endDate", "joinStartDate", "joinEndDate", "promotionNatureCat", "lprDateStart", "lprDateEnd", "dobStartDate", "dobEndDate", "promotion_nature_cat", "officeUnitIds", "officeUnitOrganogramId", "employeeRecordId",
                    "age_end", "age_start", "home_district","jobGradeTypeCat","nameEng","nameBng"));

    private final Map<String, String[]> stringMap = new HashMap<>();
    private static final String sql = " employee_offices eo INNER JOIN employee_records er ON eo.employee_record_id = er.id INNER JOIN promotion_history ph ON eo.employee_record_id = ph.employee_records_id";

    private String[][] Criteria;

    private String[][] Display;

    public Employee_promotion_report_Servlet() {
        stringMap.put("officeUnitIds", new String[]{"criteria", "eo", "office_unit_id", "IN", "AND", "String", "", "", ""
                , "officeUnitIdList", "officeUnitIds", String.valueOf(LC.EMPLOYEE_INFO_REPORT_WHERE_OFFICEUNITID), "office_units", null, "true", "1"});
        stringMap.put("officeUnitOrganogramId", new String[]{"criteria", "eo", "office_unit_organogram_id", "=", "AND", "long", "", "", "any"
                , "officeUnitOrganogramId", "officeUnitOrganogramId", String.valueOf(LC.EMPLOYEE_OFFICE_REPORT_WHERE_OFFICEUNITORGANOGRAMID), "office_unit_organogram", null, "true", "2"});
        stringMap.put("employeeRecordId", new String[]{"criteria", "eo", "employee_record_id", "=", "AND", "long", "", "", "any"
                , "employeeRecordId", "employeeRecordId", String.valueOf(LC.EMPLOYEE_OFFICE_REPORT_SELECT_EMPLOYEERECORDID), "employee_records_id", null, "true", "3"});
        stringMap.put("dobStartDate", new String[]{"criteria", "er", "date_of_birth", ">=", "AND", "long", "", "", Long.MIN_VALUE + ""
                , "dobStartDate", "dobStartDate", String.valueOf(LC.EMPLOYEE_PROMOTION_REPORT_WHERE_DATEOFBIRTH), "date", null, "true", "4"});
        stringMap.put("dobEndDate", new String[]{"criteria", "er", "date_of_birth", "<=", "AND", "long", "", "", Long.MAX_VALUE + ""
                , "dobEndDate", "dobEndDate", String.valueOf(LC.EMPLOYEE_PROMOTION_REPORT_WHERE_DATEOFBIRTH_10), "date", null, "true", "5"});
        stringMap.put("promotionNatureCat", new String[]{"criteria", "ph", "promotion_nature_cat", "=", "AND", "int", "", "", "any"
                , "promotionNatureCat", "promotionNatureCat", String.valueOf(LC.EMPLOYEE_PROMOTION_REPORT_WHERE_PROMOTIONNATURECAT), "category", "promotion_nature", "true", "6"});
        stringMap.put("startDate", new String[]{"criteria", "eo", "joining_date", ">=", "AND", "long", "", "", Long.MIN_VALUE + ""
                , "startDate", "startDate", String.valueOf(LC.EMPLOYEE_PROMOTION_REPORT_WHERE_JOININGDATE), "date", null, "true", "7"});
        stringMap.put("endDate", new String[]{"criteria", "eo", "joining_date", "<=", "AND", "long", "", "", Long.MAX_VALUE + ""
                , "endDate", "endDate", String.valueOf(LC.EMPLOYEE_PROMOTION_REPORT_WHERE_JOININGDATE_3), "date", null, "true", "8"});
        stringMap.put("lprDateStart", new String[]{"criteria", "er", "lpr_date", ">=", "AND", "long", "", "", Long.MIN_VALUE + ""
                , "lprDateStart", "lprDateStart", String.valueOf(LC.EMPLOYEE_PROMOTION_REPORT_WHERE_LPRDATE), "date", null, "true", "9"});
        stringMap.put("lprDateEnd", new String[]{"criteria", "er", "lpr_date", "<=", "AND", "long", "", "", Long.MAX_VALUE + ""
                , "lprDateEnd", "lprDateEnd", String.valueOf(LC.EMPLOYEE_PROMOTION_REPORT_WHERE_LPRDATE_6), "date", null, "true", "10"});
        stringMap.put("joinStartDate", new String[]{"criteria", "ph", "joining_date", ">=", "AND", "long", "", "", Long.MIN_VALUE + ""
                , "joinStartDate", "joinStartDate", String.valueOf(LC.EMPLOYEE_PROMOTION_REPORT_WHERE_JOININGDATE_13), "date", null, "true", "11"});
        stringMap.put("joinEndDate", new String[]{"criteria", "ph", "joining_date", "<=", "AND", "long", "", "", Long.MAX_VALUE + ""
                , "joinEndDate", "joinEndDate", String.valueOf(LC.EMPLOYEE_PROMOTION_REPORT_WHERE_JOININGDATE_14), "date", null, "true", "12"});
        stringMap.put("age_start", new String[]{"criteria", "er", "date_of_birth", "<=", "AND", "long", "", "", ""
                , "ageStart", "age_start", String.valueOf(LC.HR_REPORT_AGE_RANGE_START), "number", null, "true", "13"});
        stringMap.put("age_end", new String[]{"criteria", "er", "date_of_birth", ">=", "AND", "long", "", "", ""
                , "ageEnd", "age_end", String.valueOf(LC.HR_REPORT_AGE_RANGE_TO), "number", null, "true", "14"});
        stringMap.put("home_district", new String[]{"criteria", "er", "home_district", "=", "AND", "long", "", "", "", "home_district"
                , "home_district", String.valueOf(LC.EMPLOYEE_RECORDS_ADD_HOME_DISTRICT), "location", null, "true", "15"});
        stringMap.put("jobGradeTypeCat", new String[]{"criteria", "eo", "job_grade_type_cat", "=", "AND", "int", "", "", "any", "jobGradeTypeCat",
                "jobGradeTypeCat", String.valueOf(LC.DESIGNATION_STATUS_REPORT_WHERE_JOBGRADETYPECAT), "category", "job_grade_type", "true", "16"});
        stringMap.put("nameEng", new String[]{"criteria", "er", "name_eng", "Like", "AND", "String", "", "", "any", "nameEng",
                "nameEng", String.valueOf(LC.EMPLOYEE_RECORDS_ADD_NAMEENG), "text", null, "true", "17"});
        stringMap.put("nameBng", new String[]{"criteria", "er", "name_bng", "Like", "AND", "String", "", "", "any", "nameBng",
                "nameBng", String.valueOf(LC.EMPLOYEE_RECORDS_ADD_NAMEBNG), "text", null, "true", "18"});
        stringMap.put("office_id", new String[]{"criteria", "eo", "office_id", "=", "AND", "long", "", "", "2294", "office_id", null, null, null, null, null, null});
        stringMap.put("joinEndDateDefault", new String[]{"criteria", "ph", "joining_date", ">", "AND", "long", "", "", SessionConstants.MIN_DATE + "", "joinEndDateDefault", null, null, null, null, null, null});
        stringMap.put("dobEndDateDefault", new String[]{"criteria", "er", "date_of_birth", ">", "AND", "long", "", "", SessionConstants.MIN_DATE + "", "dobEndDateDefault", null, null, null, null, null, null});
        stringMap.put("lprDateEndDefault", new String[]{"criteria", "er", "lpr_date", ">", "AND", "long", "", "", SessionConstants.MIN_DATE + "", "lprEndDateDefault", null, null, null, null, null, null});
        stringMap.put("endDateDefault", new String[]{"criteria", "eo", "joining_date", ">", "AND", "long", "", "", SessionConstants.MIN_DATE + "", "endDateDefault", null, null, null, null, null, null});
        stringMap.put("isDeleted_1", new String[]{"criteria", "er", "isDeleted", "=", "AND", "long", "", "", "0", "isDeleted", null, null, null, null, null, null});
        stringMap.put("status", new String[]{"criteria", "eo", "status", "=", "AND", "long", "", "", "1", "status", null, null, null, null, null, null});
        stringMap.put("isDeleted", new String[]{"criteria", "eo", "isDeleted", "=", "AND", "long", "", "", "0", "isDeleted", null, null, null, null, null, null});
    }


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);

        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }

        Set<String> inputs = PBReportUtils.prepareInputSets(searchParam, request);
        if (inputs.contains("endDate")) {
            inputs.add("endDateDefault");
        }
        if (inputs.contains("lprDateEnd")) {
            inputs.add("lprDateEndDefault");
        }
        if (inputs.contains("dobEndDate")) {
            inputs.add("dobEndDateDefault");
        }
        if (inputs.contains("joinEndDate")) {
            inputs.add("joinEndDateDefault");
        }
        inputs.add("office_id");
        inputs.add("isDeleted_1");
        inputs.add("isDeleted");
        inputs.add("status");
        Display = new String[][]{
                {"display", "eo", "employee_record_id", "employee_records_id", LM.getText(LC.EMPLOYEE_PROMOTION_REPORT_SELECT_EMPLOYEERECORDID, loginDTO)},
                {"display", "eo", "employee_record_id", "user_name", LM.getText(LC.EMPLOYEE_INFO_REPORT_SELECT_USER_ID, loginDTO)},
                {"display", "er", "personal_mobile", "number", LM.getText(LC.EMPLOYEE_INFO_REPORT_SELECT_MOBILE_NUMBER, loginDTO)},
                {"display", "er", "personal_email", "plain",  LM.getText(LC.EMPLOYEE_INFO_REPORT_SELECT_EMAIL, loginDTO)},
                {"display", "er", "home_district", "location", LM.getText(LC.EMPLOYEE_RECORDS_ADD_HOME_DISTRICT, loginDTO)},
                {"display", "eo", "office_unit_id", "office_unit", LM.getText(LC.EMPLOYEE_PROMOTION_REPORT_SELECT_OFFICEUNITID, loginDTO)},
                {"display", "eo", "id", "designation", LM.getText(LC.EMPLOYEE_PROMOTION_REPORT_SELECT_OFFICEUNITORGANOGRAMID, loginDTO)},
                {"display", "eo", "joining_date", "date", LM.getText(LC.EMPLOYEE_PROMOTION_REPORT_SELECT_JOININGDATE, loginDTO)},
                {"display", "er", "date_of_birth", "date", LM.getText(LC.EMPLOYEE_PROMOTION_REPORT_SELECT_DATEOFBIRTH, loginDTO)},
                {"display", "eo", "job_grade_type_cat", "category", LM.getText(LC.DESIGNATION_STATUS_REPORT_SELECT_JOBGRADETYPECAT, loginDTO)},
                {"display", "eo", "employee_record_id", "text", LM.getText(LC.EMPLOYEE_PROMOTION_REPORT_SELECT_EDUCATIONLEVELTYPE, loginDTO)},

                {"display", "eo", "employee_record_id", "text", LM.getText(LC.EMPLOYEE_PROMOTION_REPORT_SELECT_GOVTSERVICESTARTDATE, loginDTO)},
                {"display", "eo", "employee_record_id", "text", LM.getText(LC.EMPLOYEE_PROMOTION_REPORT_SELECT_OFFICE, loginDTO)},
                {"display", "eo", "employee_record_id", "text", LM.getText(LC.EMPLOYEE_PROMOTION_REPORT_SELECT_DESIGNATION, loginDTO)},

                {"display", "eo", "employee_record_id", "text", LM.getText(LC.EMPLOYEE_PROMOTION_REPORT_SELECT_JOINING_DATE_IN_PARLIAMENT, loginDTO)},
                {"display", "eo", "employee_record_id", "text", LM.getText(LC.EMPLOYEE_PROMOTION_REPORT_SELECT_JOINING_OFFICE_IN_PARLIAMENT, loginDTO)},
                {"display", "eo", "employee_record_id", "text", LM.getText(LC.EMPLOYEE_PROMOTION_REPORT_SELECT_JOINING_DESIGNATION_IN_PARLIAMENT, loginDTO)},

                {"display", "er", "lpr_date", "date", LM.getText(LC.EMPLOYEE_PROMOTION_REPORT_SELECT_LPRDATE, loginDTO)},
                {"display", "ph", "joining_date", "date", LM.getText(LC.EMPLOYEE_PROMOTION_REPORT_SELECT_JOININGDATE_11, loginDTO)},
                {"display", "ph", "promotion_nature_cat", "category", LM.getText(LC.EMPLOYEE_PROMOTION_REPORT_SELECT_PROMOTIONNATURECAT, loginDTO)},
                {"display", "er", "date_of_birth", "complete_age", LM.getText(LC.PHYSIOTHERAPY_PLAN_ADD_AGE, loginDTO)},
                {"display", "eo", "employee_record_id", "id_to_present_address", LM.getText(LC.EMPLOYEE_RECORDS_ADD_PRESENTADDRESS, loginDTO)},
                {"display", "eo", "employee_record_id", "id_to_permanent_address", LM.getText(LC.EMPLOYEE_RECORDS_ADD_PERMANENTADDRESS, loginDTO)}
        };
        Display[1][4] = LM.getText(LC.USER_ADD_USER_NAME, loginDTO);
        Display[2][4] = LM.getText(LC.EMPLOYEE_PERSONAL_MOBILE_NUMBER, loginDTO);

        Criteria = PBReportUtils.prepareCriteria(inputs, stringMap);

        if (inputs.contains("officeUnitIds") || inputs.contains("age_start") || inputs.contains("age_end") || inputs.contains("jobGradeTypeCat")) {
            for (String[] arr : Criteria) {
                switch (arr[9]) {
                    case "officeUnitIdList":
                        arr[8] = getOfficeIdsFromOfficeUnitIds(request).stream()
                                                                       .map(String::valueOf)
                                                                       .collect(Collectors.joining(","));
                        break;
                    case "ageStart":
                        long ageStart = Long.parseLong(request.getParameter("age_start"));
                        arr[8] = String.valueOf(calculateLongDateValue(ageStart));
                        break;
                    case "ageEnd":
                        long ageEnd = Long.parseLong(request.getParameter("age_end"));
                        arr[8] = String.valueOf(calculateLongDateValue(ageEnd));
                        break;
                    case "jobGradeTypeCat":
                        int jobGradeCat=Integer.parseInt(request.getParameter("jobGradeTypeCat"));
                        String sign="=";
                        int jobGradeEqual=Integer.parseInt(request.getParameter("jobGradeEqual"));
                        int jobGradeLess=Integer.parseInt(request.getParameter("jobGradeLess"));
                        int jobGradeMore=Integer.parseInt(request.getParameter("jobGradeMore"));
                        if(jobGradeEqual==0 && jobGradeLess==0 && jobGradeMore==0)
                            break;
                        if(jobGradeEqual==0)
                            sign="";
                        if(jobGradeLess==1){
                            if(jobGradeCat<=20){
                                sign=">"+sign;
                            }
                        }
                        if(jobGradeMore==1){
                            if(jobGradeCat<=20){
                                sign="<"+sign;
                            }
                        }
                        arr[3]=sign;
                        break;
                }
            }
        }

        request.setAttribute("dontUseOutDatedModal", true);
        if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.EMPLOYEE_OFFICE_REPORT_DETAILS)) {
            reportGenerate(request, response);
        }
    }

    private long calculateLongDateValue(long ageYears) {
        LocalDate ld = LocalDate.now();
        ld = ld.minusYears(ageYears);
        return Date.from(ld.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant()).getTime();
    }

    @Override
    public String[][] getCriteria() {
        return Criteria;
    }

    @Override
    public String[][] getDisplay() {
        return Display;
    }

    @Override
    public String getSQL() {
        return sql;
    }

    @Override
    public int getLCForReportName() {
        return LC.EMPLOYEE_PROMOTION_REPORT_OTHER_EMPLOYEE_PROMOTION_REPORT;
    }

    @Override
    public String getFileName() {
        return "employee_promotion_report";
    }

    @Override
    public String getTableName() {
        return "employee_promotion_report";
    }

    @Override
    public List<List<Object>> doModificationOnResultList(List<List<Object>> list, String language) {
        if (list != null && list.size() > 1) {
            List<Long> empIds = list.parallelStream()
                    .map(ls -> Utils.convertToLong((String) ls.get(7)))
                    .filter(Objects::nonNull)
                    .distinct()
                    .collect(Collectors.toList());
            List<Employee_service_historyDTO> dtoList = Employee_service_historyDAO.getInstance().getDTOSGovJobByEmployeeIds(empIds);
            Map<Long, Employee_service_historyDTO> mapForEmployee_service_historyDTO = dtoList.stream()
                    .collect(Collectors.toMap(e -> e.employeeRecordsId, e -> e, (e1, e2) -> e1));
            List<EmployeeOfficeDTO> officeDTOList = EmployeeOfficesDAO.getInstance().getByEmployeeRecordIdsWithMinJoiningDate(empIds);
            Map<Long, EmployeeOfficeDTO> mapForEmployeeOfficeDTO = officeDTOList.stream().collect(Collectors.toMap(e -> e.employeeRecordId, e -> e, (e1, e2) -> e1));
            List<Employee_education_infoDTO> educationInfoDTOList = Employee_education_infoDAO.getInstance().getByEmployeeRecordIdWithMaxPassingYear(empIds);
            Map<Long, Employee_education_infoDTO> mapForEmployeeEducationInfoDTO = educationInfoDTOList.stream()
                    .collect(Collectors.toMap(e -> e.employeeRecordsId, e -> e, (e1, e2) -> {
                        if (e1.educationLevelType == 100 && e2.educationLevelType == 100) {
                            if (e1.degreeExamType >= e2.degreeExamType) {
                                return e1;
                            } else {
                                return e2;
                            }
                        } else if (e1.educationLevelType == 100) {
                            return e2;
                        } else if (e2.educationLevelType == 100) {
                            return e1;
                        } else if (e1.degreeExamType > e2.degreeExamType) {
                            return e1;
                        } else {
                            return e2;
                        }
                    }));
            return list.stream()
                    .peek(ls -> {
                        Long val = Utils.convertToLong((String) ls.get(7));
                        if (val == null) {
                            return;
                        }
                        if (ls.get(7) != null) {
                            if (mapForEmployee_service_historyDTO.get(Long.parseLong((String) ls.get(7))) != null) {
                                ls.set(7, StringUtils.getFormattedDate(language, mapForEmployee_service_historyDTO.get(Long.parseLong((String) ls.get(7))).servingFrom));
                                ls.set(8, mapForEmployee_service_historyDTO.get(Long.parseLong((String) ls.get(8))).department);
                                ls.set(9, mapForEmployee_service_historyDTO.get(Long.parseLong((String) ls.get(9))).designation);
                            } else {
                                ls.set(7, "");
                                ls.set(8, "");
                                ls.set(9, "");
                            }
                            if (mapForEmployeeOfficeDTO.get(Long.parseLong((String) ls.get(10))) != null) {
                                ls.set(10, StringUtils.getFormattedDate(language, mapForEmployeeOfficeDTO.get(Long.parseLong((String) ls.get(10))).joiningDate));
                                ls.set(11, Office_unitsRepository.getInstance().geText(language, mapForEmployeeOfficeDTO.get(Long.parseLong((String) ls.get(11))).officeUnitId));
                                ls.set(12, OfficeUnitOrganogramsRepository.getInstance().getDesignation(language, mapForEmployeeOfficeDTO.get(Long.parseLong((String) ls.get(12))).officeUnitOrganogramId));
                            } else {
                                ls.set(10, "");
                                ls.set(11, "");
                                ls.set(12, "");
                            }
                            if (mapForEmployeeEducationInfoDTO.get(Long.parseLong((String) ls.get(6))) != null) {
                                Employee_education_infoDTO dto = mapForEmployeeEducationInfoDTO.get(Long.parseLong((String) ls.get(6)));
                                long eduLevel = dto.educationLevelType;
                                String text;
                                if (eduLevel == 100) {
                                    text = DegreeExamRepository.getInstance().getText(language, dto.degreeExamType, 100);
                                } else {
                                    text = Education_levelRepository.getInstance().getText(language, eduLevel);
                                }
                                ls.set(6, text);
                            } else {
                                ls.set(6, "");
                            }
                        }
                    }).collect(Collectors.toList());
        }
        return list;
    }
}