package pi_office_wise_requisition_report;


import java.io.IOException;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import language.LC;
import language.LM;

import login.LoginDTO;
import permission.MenuConstants;

import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;

import util.ReportRequestHandler;
import pbReport.*;






@WebServlet("/Pi_office_wise_requisition_report_Servlet")
public class Pi_office_wise_requisition_report_Servlet  extends HttpServlet {
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	String[][] Criteria =
	{
		{"criteria","esh","office_unit_id","=","","String","","","any","officeUnitId", LC.PI_EMPLOYEE_WISE_REQUISITION_REPORT_WHERE_OFFICEUNITID + ""},
		{"criteria","esh","fiscal_year","=","AND","String","","","any","fiscalYear", LC.PI_OFFICE_WISE_REQUISITION_REPORT_WHERE_FISCALYEAR + ""},
		{"criteria","esh","isDeleted","=","AND","String","","","0","isDeleted", LC.PI_OFFICE_WISE_REQUISITION_REPORT_WHERE_ISDELETED + ""},
		{"criteria","esh","status","=","AND","String","","","36","status", LC.PI_OFFICE_WISE_REQUISITION_REPORT_WHERE_STATUS + ""}
	};

	String[][] Display =
	{
		{"display","esh","office_unit_id","office_unit",""},
		{"display","esh","fiscal_year","fiscal_year_id_check",""},
		{"display","ph","item_type_id","procurement_goods_type_id_check",""},
		{"display","ph","item_id","productIdConverter",""},
		{"display","","SUM(ph.approval_two_quantity) ","text",""},
		{"display","","SUM((SELECT unit_price FROM pi_purchase WHERE office_unit_id = esh.office_unit_id AND fiscal_year_id = esh.fiscal_year AND item_type_id = ph.item_type_id AND item_id = ph.item_id LIMIT 1) * ph.approval_two_quantity)","text",""}
	};

	String GroupBy = "ph.item_id";
	String OrderBY = "";

	ReportRequestHandler reportRequestHandler;

	public Pi_office_wise_requisition_report_Servlet(){

	}

	private final ReportService reportService = new ReportService();

	private String sql;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{

		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String language = "english";
		if(userDTO!= null && userDTO.languageID == SessionConstants.BANGLA)
		{
			language = "bangla";
		}

		String actionType = request.getParameter("actionType");

		System.out.println("In ssservlet doget, actiontype = " + actionType);

		sql = "pi_requisition esh INNER JOIN pi_requisition_item ph ON esh.id = ph.pi_requisition_id";

		Display[0][4] = LM.getText(LC.PI_OFFICE_WISE_REQUISITION_REPORT_SELECT_OFFICEUNITID, loginDTO);
		Display[1][4] = LM.getText(LC.PI_OFFICE_WISE_REQUISITION_REPORT_SELECT_FISCALYEAR, loginDTO);
		Display[2][4] = LM.getText(LC.PI_OFFICE_WISE_REQUISITION_REPORT_SELECT_ITEMTYPEID, loginDTO);
		Display[3][4] = LM.getText(LC.PI_OFFICE_WISE_REQUISITION_REPORT_SELECT_ITEMID, loginDTO);
		Display[4][4] = LM.getText(LC.PI_AUCTION_SEARCH_ITEMCOUNT, loginDTO);
		Display[5][4] = LM.getText(LC.PI_PURCHASE_EDIT_TOTALVALUE, loginDTO);


		String reportName = LM.getText(LC.PI_OFFICE_WISE_REQUISITION_REPORT_OTHER_PI_OFFICE_WISE_REQUISITION_REPORT, loginDTO);

		reportRequestHandler = new ReportRequestHandler(null,
				Criteria, Display, GroupBy, OrderBY, sql,
				reportService);


		reportRequestHandler.handleReportGet(request, response, userDTO, "pi_office_wise_requisition_report",
				MenuConstants.PI_OFFICE_WISE_REQUISITION_REPORT_DETAILS, language, reportName, "pi_office_wise_requisition_report");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		doGet(request, response);
	}
}
