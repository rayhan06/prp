package budget_preparation;

import budget.*;
import budget_selection_info.BudgetSelectionInfoDAO;
import budget_selection_info.BudgetSelectionInfoDTO;
import budget_selection_info.BudgetSelectionInfoRepository;
import budget_submission_info.Budget_submission_infoDAO;
import budget_submission_info.Budget_submission_infoDTO;
import com.google.gson.Gson;
import common.BaseServlet;
import common.CommonDAOService;
import login.LoginDTO;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import permission.MenuConstants;
import role.PermissionRepository;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.CommonConstant;
import util.CommonDTO;
import util.StringUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.groupingBy;

@WebServlet("/Budget_preparationServlet")
@MultipartConfig
public class Budget_preparationServlet extends BaseServlet {
    private static final long serialVersionUID = 1L;
    public static final Logger logger = Logger.getLogger(Budget_preparationServlet.class);
    private final BudgetDAO budgetDAO = BudgetDAO.getInstance();
    private static final String tableName = "budget";
    private final Gson gson = new Gson();


    @Override
    public String getTableName() {
        return tableName;
    }

    @Override
    public String getServletName() {
        return "Budget_preparationServlet";
    }

    @Override
    public CommonDAOService<? extends CommonDTO> getCommonDAOService() {
        return null;
    }

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        return null;
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[0];
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[0];
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[0];
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Budget_preparationServlet.class;
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);

        if (hasPermission(userDTO)) {
            try {
                String language = userDTO.languageID == CommonConstant.Language_ID_English ? "English" : "Bangla";
                String actionType = request.getParameter("actionType");
                switch (actionType) {
                    case "getAddPage":
                        request.getRequestDispatcher("budget_preparation/budget_preparation.jsp").forward(request, response);
                        return;
                    case "getPreviewPage":
                        setPreviewPageBudgetData(request, userDTO);
                        request.getRequestDispatcher("budget_preparation/budget_preparation_preview.jsp").forward(request, response);
                        return;
                    case "getBudgetModels":
                        sendBudgetModels(request, response, language);
                        return;
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                logger.debug(ex);
            }
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);

        if (hasPermission(userDTO)) {
            try {
                String actionType = request.getParameter("actionType");
                switch (actionType) {
                    case "insertBudgetForFinance": {
                        boolean saveSuccess = savePreparationForFinance(userDTO, request);
                        String resJson = "{\"success\": true}";
                        if (!saveSuccess)
                            resJson = "{\"success\": false}";
                        PrintWriter out = response.getWriter();
                        response.setContentType("application/json");
                        response.setCharacterEncoding("UTF-8");
                        out.print(resJson);
                        out.flush();
                        return;
                    }
                    case "submitBudgetForFinance": {
                        submitPreparationForFinance(userDTO);
                        String resJson = "{\"success\": true}";
                        PrintWriter out = response.getWriter();
                        response.setContentType("application/json");
                        response.setCharacterEncoding("UTF-8");
                        out.print(resJson);
                        out.flush();
                        return;
                    }
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                logger.debug(ex);
            }
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }

    private void sendBudgetModels(HttpServletRequest request, HttpServletResponse response, String language) throws Exception {
        BudgetInfo budgetInfo = BudgetUtils.getBudgetInfoForBudgetPreparation(System.currentTimeMillis());
        BudgetSelectionInfoDTO budgetSelectionInfoDTO = BudgetSelectionInfoRepository.getInstance().getDTOByEconomicYear(budgetInfo.economicYear);
        boolean isCodeSelectionDone = budgetSelectionInfoDTO != null && budgetSelectionInfoDTO.isSubmitted;
        if (!isCodeSelectionDone) throw new Exception("Budget Code Selection Not Done Yet!");

        // mandatory param
        long budgetSelectionInfoId = budgetSelectionInfoDTO.iD;
        long budgetOfficeId = Long.parseLong(request.getParameter("budgetOfficeId").trim());
        long budgetOperationId = Long.parseLong(request.getParameter("budgetOperationId").trim());

        // optional param
        Long economicGroupId = StringUtils.parseNullableLong(request.getParameter("economicGroupId"));
        Long economicCodeId = StringUtils.parseNullableLong(request.getParameter("economicCodeId"));

        List<BudgetDTO> budgetDTOList =
                budgetDAO.getByBudgetOfficeAndOperationAndCodeAndSelectionIds(
                        budgetOfficeId,
                        budgetOperationId,
                        economicGroupId,
                        economicCodeId,
                        BudgetUtils.getSelectionIds(budgetInfo.beginYear, 3)
                );

        Map<Long, List<BudgetDTO>> mapBySelectionInfoId = budgetDTOList.stream()
                                                                       .collect(groupingBy(e -> e.budgetSelectionInfoId));

        List<Map<Long, AmountModel>> listOfPreviousMaps =
                BudgetUtils.getPreviousAmountsGroupedBySubCode(budgetInfo.beginYear, 3, mapBySelectionInfoId);

        List<BudgetModel> subCodeModelList =
                mapBySelectionInfoId.get(budgetSelectionInfoId).stream()
                                    .map(dto -> new BudgetModel(dto, language))
                                    .sorted(Comparator.comparing(a -> a.economicSubCode))
                                    .peek(dto -> dto.setPreviousAmounts(listOfPreviousMaps))
                                    .collect(Collectors.toList());

        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().println(new Gson().toJson(subCodeModelList));
    }

    private boolean hasPermission(UserDTO userDTO) {
        return userDTO != null && PermissionRepository.checkPermissionByRoleIDAndMenuID(
                userDTO.roleID, MenuConstants.BUDGET_PREPARATION
        );
    }

    private void submitPreparationForFinance(UserDTO userDTO) throws Exception {
        BudgetInfo budgetInfo = BudgetUtils.getBudgetInfoForBudgetPreparation(System.currentTimeMillis());
        BudgetSelectionInfoDTO budgetSelectionInfoDTO = BudgetSelectionInfoDAO.getInstance().getDTOByEconomicYear(budgetInfo.economicYear);

        boolean isCodeSelectionDone = budgetSelectionInfoDTO != null && budgetSelectionInfoDTO.isSubmitted;
        if (!isCodeSelectionDone)
            throw new Exception("Budget Code Selection has not yet been completed for economic year ".concat(budgetInfo.economicYear));

        Budget_submission_infoDAO submissionInfoDAO = Budget_submission_infoDAO.getInstance();
        Budget_submission_infoDTO submissionInfoDTO = submissionInfoDAO.getByBudgetTypeAndBudgetOffice(
                budgetSelectionInfoDTO.iD,
                budgetInfo.budgetTypeEnum.getValue(),
                BudgetUtils.FINANCE_SECTION_OFFICE_ID
        );

        if(!Budget_preparationServlet.hasFinanceSubmittedBudget(budgetInfo.budgetTypeEnum.getValue(), submissionInfoDTO))
            throw new Exception("Finance has not submitted Budget for this economic year ");

        boolean isPreparationSubmitted = submissionInfoDTO.isPreparationSubmit == SubmissionStatusEnum.SUBMITTED.getValue();
        if (isPreparationSubmitted)
            throw new Exception("Budget Preparation already submitted");

        submissionInfoDTO.isPreparationSubmit = SubmissionStatusEnum.SUBMITTED.getValue();
        submissionInfoDTO.modifiedBy = userDTO.ID;
        submissionInfoDTO.lastModificationTime = System.currentTimeMillis();
        submissionInfoDAO.update(submissionInfoDTO);
    }

    private boolean savePreparationForFinance(UserDTO userDTO, HttpServletRequest request) {
        BudgetInfo budgetInfo = BudgetUtils.getBudgetInfoForBudgetPreparation(System.currentTimeMillis());
        BudgetSelectionInfoDTO budgetSelectionInfoDTO = BudgetSelectionInfoDAO.getInstance().getDTOByEconomicYear(budgetInfo.economicYear);
        boolean isCodeSelectionDone = budgetSelectionInfoDTO != null && budgetSelectionInfoDTO.isSubmitted;
        if (!isCodeSelectionDone) {
            logger.error("Budget Code Selection has not yet been completed for economic year ".concat(budgetInfo.economicYear));
            return false;
        }

        int budgetTypeCat = BudgetTypeEnum.BUDGET.getValue();
        Budget_submission_infoDTO budget_submission_infoDTO = Budget_submission_infoDAO.getInstance().getByBudgetTypeAndBudgetOffice(
                budgetSelectionInfoDTO.iD, budgetTypeCat, BudgetUtils.FINANCE_SECTION_OFFICE_ID
        );
        if(!hasFinanceSubmittedBudget(budgetInfo.budgetTypeEnum.getValue(), budget_submission_infoDTO)){
            logger.error("Finance has not submitted Budget for economic  year ".concat(budgetInfo.economicYear));
            return false;
        }

        boolean isPreparationSubmitted = budget_submission_infoDTO.isPreparationSubmit == SubmissionStatusEnum.SUBMITTED.getValue();
        if (isPreparationSubmitted) {
            logger.error("Budget Preparation already submitted");
            return false;
        }

        String Value = request.getParameter("budgetModels");
        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }
        BudgetModel[] budgetModels = gson.fromJson(Value, BudgetModel[].class);
        for (BudgetModel budgetModel : budgetModels) {
            budgetDAO.updateProjectionById(budgetModel, userDTO.ID, System.currentTimeMillis());
        }
        return true;
    }

    private void setPreviewPageBudgetData(HttpServletRequest request, UserDTO userDTO) {
        boolean isLangEng = userDTO.languageID == CommonConstant.Language_ID_English;
        String errorMessage;
        BudgetSelectionInfoDAO budgetSelectionInfoDAO = BudgetSelectionInfoDAO.getInstance();

        BudgetInfo thisYearBudgetInfo = BudgetUtils.getBudgetInfoForBudgetPreparation(System.currentTimeMillis());
        BudgetSelectionInfoDTO thisYearSelectionDTO = budgetSelectionInfoDAO.getDTOByEconomicYear(thisYearBudgetInfo.economicYear);
        boolean isCodeSelectionDone = thisYearSelectionDTO != null && thisYearSelectionDTO.isSubmitted;
        if (!isCodeSelectionDone) {
            errorMessage = isLangEng
                           ? "Budget code selection process has not yet been completed for this Economic Year "
                           : "এই অর্থ বছরের বাজেটের কোড বাছাইকরণ প্রক্রিয়া এখনো সম্পন্ন হয়নি";
            request.setAttribute("errorMessage", errorMessage);
            return;
        }

        Budget_submission_infoDTO budgetSubmissionInfoDTO = Budget_submission_infoDAO.getInstance().getByBudgetTypeAndBudgetOffice(
                thisYearSelectionDTO.iD, thisYearBudgetInfo.budgetTypeEnum.getValue(), BudgetUtils.FINANCE_SECTION_OFFICE_ID
        );
        if (!hasFinanceSubmittedBudget(thisYearBudgetInfo.budgetTypeEnum.getValue(), budgetSubmissionInfoDTO)) {
            errorMessage = isLangEng
                           ? "Finance has not yet submitted Budget for this Economic Year!"
                           : "অর্থ শাখা এখনো এই অর্থবছরের বাজেট জমা দেয়নি!";
            request.setAttribute("errorMessage", errorMessage);
            return;
        }

        request.setAttribute("thisYearBudgetInfo", thisYearBudgetInfo);
        request.setAttribute("thisYearSelectionDTO", thisYearSelectionDTO);
        request.setAttribute("budgetSubmissionInfoDTO", budgetSubmissionInfoDTO);

        BudgetSelectionInfoDTO prevYearSelectionDTO = budgetSelectionInfoDAO.getDTOByEconomicYear(
                BudgetInfo.getEconomicYear(thisYearBudgetInfo.beginYear - 2)
        );
        BudgetSelectionInfoDTO prevPrevYearSelectionDTO = BudgetSelectionInfoRepository.getInstance().getDTOByEconomicYear(
                BudgetInfo.getEconomicYear(thisYearBudgetInfo.beginYear - 3)
        );

        long thisYearId = thisYearSelectionDTO.iD;
        Long prevYearId = prevYearSelectionDTO == null ? null : prevYearSelectionDTO.iD;
        Long prevPrevYearId = prevPrevYearSelectionDTO == null ? null : prevPrevYearSelectionDTO.iD;
        request.setAttribute("thisYearId", thisYearId);
        request.setAttribute("prevYearId", prevYearId);
        request.setAttribute("prevPrevYearId", prevPrevYearId);

        // Group BudgetDTOs by budgetSelectionInfoId (year)         Map<Long,List<BudgetDTO>>
        //      For each Group                                      List<BudgetDTO>
        //          Group By budgetMappingId                        Map<Long,Map<Long,List<BudgetDTO>>>
        Map<Long, Map<Long, List<BudgetDTO>>> yearsBudgetDTOsByMappingId =
                budgetDAO.getDTOsBySelectionInfoIds(Arrays.asList(thisYearId, prevYearId, prevPrevYearId))
                         .stream()
                         .collect(groupingBy(
                                 dto -> dto.budgetSelectionInfoId,
                                 Collectors.collectingAndThen(
                                         Collectors.toList(),
                                         budgetDTOs -> budgetDTOs.stream()
                                                                 .collect(groupingBy(budgetDTO -> budgetDTO.budgetMappingId))
                                 )
                         ));
        request.setAttribute("yearsBudgetDTOsByMappingId", yearsBudgetDTOsByMappingId);
    }

    public static boolean hasFinanceSubmittedBudget(int budgetTypeValue, Budget_submission_infoDTO budgetSubmissionInfoDTO) {
        if (budgetSubmissionInfoDTO == null) return false;

        if (budgetTypeValue == BudgetTypeEnum.REVISED_BUDGET.getValue())
            return true;

        return budgetSubmissionInfoDTO.isSubmit == SubmissionStatusEnum.SUBMITTED.getValue();
    }
}

