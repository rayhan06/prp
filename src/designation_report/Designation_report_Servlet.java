package designation_report;


import common.StringUtils;
import language.LC;
import language.LM;
import login.LoginDTO;
import pbReport.CriteriaColumn;
import pbReport.PBReportUtils;
import pbReport.ReportCommonService;
import permission.MenuConstants;
import role.PermissionRepository;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.*;


@WebServlet("/Designation_report_Servlet")
public class Designation_report_Servlet extends HttpServlet implements ReportCommonService {
    private static final long serialVersionUID = 1L;
    private static final Set<String> searchParam = new HashSet<>(Arrays.asList(
            "officeUnitIds", "jobGradeTypeCat", "isVacant"
    ));
    private final Map<String, String[]> stringMap = new HashMap<>();

    private static String sql;


    private String[][] Criteria;
    private String[][] XLCriteria;
    private final String[][] Display =
            {
                    {"display", "ouo", "id", "office_unit_organogram", ""},
                    {"display", "ouo", "office_unit_id", "office_unit", ""},
                    {"display", "ouo", "job_grade_type_cat", "category", ""},
                    {"display", "ouo", "assignedEmployeeRecordId", "employee_records_id_details", LM.getText(LC.EMPLOYEE_INFO_REPORT_SELECT_EMPLOYEERECORDID, "")},


            };

    public Designation_report_Servlet() {
        stringMap.put("officeUnitIds", new String[]{"criteria", "ouo", "office_unit_id", "IN", "AND", "String", "", "", "any",
                "officeUnitIdList", "officeUnitIds", String.valueOf(LC.DESIGNATION_STATUS_REPORT_WHERE_OFFICEUNITID), "office_units", null, "true", "1"});

        stringMap.put("jobGradeTypeCat", new String[]{"criteria", "ouo", "job_grade_type_cat", "=", "AND", "int", "", "", "any", "jobGradeTypeCat",
                "jobGradeTypeCat", String.valueOf(LC.DESIGNATION_STATUS_REPORT_WHERE_JOBGRADETYPECAT), "category", "job_grade_type", "true", "2"});
        stringMap.put("isVacant", new String[]{"criteria", "ouo", "isVacant", "=", "AND", "int", "", "", "any", "isVacant", null, null, null, null, null, null});
//        stringMap.put("status", new String[]{"criteria", "eo", "status", "=", "AND", "int", "", "", "1", "status", null, null, null, null, null, null});
        stringMap.put("ouo_isDeleted", new String[]{"criteria", "ouo", "isDeleted", "=", "AND", "int", "", "", "0", "ouo_isDeleted", null, null, null, null, null, null});
        stringMap.put("eo_isDeleted", new String[]{"criteria", "eo", "isDeleted", "=", "AND", "int", "", "", "0", "eo_isDeleted", null, null, null, null, null, null});
    }

    private String getLeftJoinSqlQuery(HttpServletRequest request) {

        Map<String, String> whereClauseByTableName = Arrays.stream(Criteria)
                .map(criterion -> new CriteriaColumn(criterion, request.getParameter(criterion[9])))
                .collect(groupingBy(criteriaColumn -> criteriaColumn.tableName,
                        collectingAndThen(
                                toList(),
                                list -> list.stream()
                                        .map(CriteriaColumn::getCriterionSql)
                                        .collect(joining())
                                        .trim()
                        )));


        StringBuilder leftJoinSqlBuilder;
        leftJoinSqlBuilder = new StringBuilder("office_unit_organograms ouo ");

        String orgnanogramTableName = "ouo";
        String clauseOfOrgTable = whereClauseByTableName.get(orgnanogramTableName);
        if (StringUtils.isNotBlank(clauseOfOrgTable))
            leftJoinSqlBuilder.append(" WHERE ").append(
                    clauseOfOrgTable.startsWith("AND") ? clauseOfOrgTable.substring(3) : clauseOfOrgTable
            );

        return leftJoinSqlBuilder.toString();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }

        Display[0][4] = LM.getText(LC.DESIGNATION_STATUS_REPORT_SELECT_ID, loginDTO);
        Display[1][4] = LM.getText(LC.DESIGNATION_STATUS_REPORT_SELECT_OFFICEUNITID, loginDTO);
        Display[2][4] = LM.getText(LC.DESIGNATION_STATUS_REPORT_SELECT_JOBGRADETYPECAT, loginDTO);

        Set<String> inputs = PBReportUtils.prepareInputSets(searchParam, request);
        inputs.add("ouo_isDeleted");
//        inputs.add("status");
        inputs.add("eo_isDeleted");

        Criteria = PBReportUtils.prepareCriteria(inputs, stringMap);
        XLCriteria = PBReportUtils.prepareCriteria(inputs, stringMap);
        if (inputs.contains("officeUnitIds")) {
            for (String[] arr : Criteria) {
                if ("officeUnitIdList".equals(arr[9])) {
                    arr[8] = getOfficeIdsFromOfficeUnitIds(request).stream()
                            .map(String::valueOf)
                            .collect(Collectors.joining(","));
                }
            }
        }
        sql = getLeftJoinSqlQuery(request);
        Criteria = new String[][]{}; // generating own sql, don't need to pass criteria
        if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.DESIGNATION_STATUS_REPORT_DETAILS)) {
            reportGenerate(request, response);
        }
    }

    @Override
    public String[][] getCriteria() {
        return Criteria;
    }

    @Override
    public String[][] getXLCriteria() {
        return XLCriteria;
    }

    @Override
    public String[][] getDisplay() {
        return Display;
    }

    @Override
    public String getSQL() {
        return sql;
    }

    @Override
    public int getLCForReportName() {
        return LC.DESIGNATION_REPORT_OTHER_DESIGNATION_REPORT;
    }

    @Override
    public String getFileName() {
        return "designation_report";
    }

    @Override
    public String getTableName() {
        return "designation_report";
    }
}