package designation_report;


import pb.OptionDTO;
import pb.Utils;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@SuppressWarnings("unused")
public enum AssignEnum {
    ASSIGNED("Assigned", "নিযুক্ত", 0),
    UNASSIGNED("Unassigned", "পদশূন্য", 1);

    private final String engText;
    private final String bngText;
    private final int value;

    AssignEnum(String engText, String bngText, int value) {
        this.engText = engText;
        this.bngText = bngText;
        this.value = value;
    }

    public String getEngText() {
        return engText;
    }

    public String getBngText() {
        return bngText;
    }

    public int getValue() {
        return value;
    }

    private static String buildOptionsForEnglish;
    private static String buildOptionsForBangla;
    private static volatile Map<Integer, AssignEnum> mapEnumByValue = null;

    private static void loadMap() {
        if (mapEnumByValue == null) {
            synchronized (AssignEnum.class) {
                if (mapEnumByValue == null) {
                    mapEnumByValue = Stream.of(AssignEnum.values())
                            .collect(Collectors.toMap(e -> e.value, e -> e));
                }
            }
        }
    }

    public static String getBuildOptions(String language) {
        if (buildOptionsForEnglish == null || buildOptionsForBangla == null) {
            synchronized (AssignEnum.class) {
                if (buildOptionsForEnglish == null || buildOptionsForBangla == null) {
                    List<OptionDTO> list = Arrays.stream(AssignEnum.values())
                            .map(statusEnum -> new OptionDTO(statusEnum.engText, statusEnum.bngText, String.valueOf(statusEnum.value)))
                            .collect(Collectors.toList());
                    buildOptionsForEnglish = Utils.buildOptions(list, "English", null);
                    buildOptionsForBangla = Utils.buildOptions(list, "Bangla", null);
                }
            }
        }
        return "English".equalsIgnoreCase(language) ? buildOptionsForEnglish : buildOptionsForBangla;
    }

    public static String getTextByValue(String language, int value) {
        if (mapEnumByValue == null) {
            loadMap();
        }
        return mapEnumByValue.get(value) == null ? "" : "English".equalsIgnoreCase(language) ? mapEnumByValue.get(value).engText : mapEnumByValue.get(value).bngText;
    }

    public static AssignEnum getByValue(int value) {
        if (mapEnumByValue == null) {
            loadMap();
        }
        return mapEnumByValue.get(value);
    }
}