package recruitment_job_description;

import com.google.gson.Gson;
import org.apache.log4j.Logger;
import repository.Repository;
import repository.RepositoryManager;
import vm_route.VmRouteStoppageDTO;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;


public class RecruitmentJobSpecificCertsRepository implements Repository {
	RecruitmentJobSpecificCertsDAO recruitmentJobSpecificCertsDAO = null;
	Gson gson = new Gson();

	public void setDAO(RecruitmentJobSpecificCertsDAO recruitmentJobSpecificCertsDAO)
	{
		this.recruitmentJobSpecificCertsDAO = recruitmentJobSpecificCertsDAO;
	}


	static Logger logger = Logger.getLogger(RecruitmentJobSpecificCertsRepository.class);

	Map<Long, RecruitmentJobSpecificCertsDTO>mapOfRecruitmentJobSpecificCertDTOToiD;
	Map<Long, Set<RecruitmentJobSpecificCertsDTO>>mapOfRecruitmentJobSpecificCertToJobDescriptionID;


	static RecruitmentJobSpecificCertsRepository instance = null;
	private RecruitmentJobSpecificCertsRepository(){
		mapOfRecruitmentJobSpecificCertDTOToiD = new ConcurrentHashMap<>();
		mapOfRecruitmentJobSpecificCertToJobDescriptionID =  new ConcurrentHashMap<>();

		recruitmentJobSpecificCertsDAO = new RecruitmentJobSpecificCertsDAO();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static RecruitmentJobSpecificCertsRepository getInstance(){
		if (instance == null){
			instance = new RecruitmentJobSpecificCertsRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(recruitmentJobSpecificCertsDAO == null)
		{
			return;
		}
		try {
			List<RecruitmentJobSpecificCertsDTO> recruitmentJobSpecificCertsDTOS = recruitmentJobSpecificCertsDAO.getAllRecruitmentJobSpecificCerts(reloadAll);
			for(RecruitmentJobSpecificCertsDTO recruitmentJobSpecificCertsDTO : recruitmentJobSpecificCertsDTOS) {
				RecruitmentJobSpecificCertsDTO oldrecruitmentJobSpecificCertsDTO =
						getRecruitmentJobSpecificCertsDTOByIDWithoutClone(recruitmentJobSpecificCertsDTO.iD);
				if( oldrecruitmentJobSpecificCertsDTO != null ) {
					mapOfRecruitmentJobSpecificCertDTOToiD.remove(oldrecruitmentJobSpecificCertsDTO.iD);

					if(mapOfRecruitmentJobSpecificCertToJobDescriptionID.containsKey(oldrecruitmentJobSpecificCertsDTO.recruitmentJobDescriptionId)) {
						mapOfRecruitmentJobSpecificCertToJobDescriptionID.get(oldrecruitmentJobSpecificCertsDTO.recruitmentJobDescriptionId).remove(oldrecruitmentJobSpecificCertsDTO);
					}
					if(mapOfRecruitmentJobSpecificCertToJobDescriptionID.get(oldrecruitmentJobSpecificCertsDTO.recruitmentJobDescriptionId).isEmpty()) {
						mapOfRecruitmentJobSpecificCertToJobDescriptionID.remove(oldrecruitmentJobSpecificCertsDTO.recruitmentJobDescriptionId);
					}




				}
				if(recruitmentJobSpecificCertsDTO.isDeleted == 0)
				{

					mapOfRecruitmentJobSpecificCertDTOToiD.put(recruitmentJobSpecificCertsDTO.iD, recruitmentJobSpecificCertsDTO);

					if( ! mapOfRecruitmentJobSpecificCertToJobDescriptionID.containsKey(recruitmentJobSpecificCertsDTO.recruitmentJobDescriptionId)) {
						mapOfRecruitmentJobSpecificCertToJobDescriptionID.put(recruitmentJobSpecificCertsDTO.recruitmentJobDescriptionId, new HashSet<>());
					}
					mapOfRecruitmentJobSpecificCertToJobDescriptionID.get(recruitmentJobSpecificCertsDTO.recruitmentJobDescriptionId).add(recruitmentJobSpecificCertsDTO);



				}
			}

		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}

	public List<RecruitmentJobSpecificCertsDTO> getRecruitmentJobSpecificCertsDTOByJobDescriptionId(long recruitmentJobDescriptionId) {

		return new ArrayList<>( mapOfRecruitmentJobSpecificCertToJobDescriptionID.getOrDefault(recruitmentJobDescriptionId,new HashSet<>()));
	}

	public List<RecruitmentJobSpecificCertsDTO> getRecruitmentJobSpecificCertsDTOs() {
		List <RecruitmentJobSpecificCertsDTO> recruitmentJobSpecificCertsDTOS = new ArrayList<RecruitmentJobSpecificCertsDTO>(this.mapOfRecruitmentJobSpecificCertDTOToiD.values());
		return clone(recruitmentJobSpecificCertsDTOS);
	}


	public RecruitmentJobSpecificCertsDTO getRecruitmentJobSpecificCertsDTOByIDWithoutClone( long ID){
		return mapOfRecruitmentJobSpecificCertDTOToiD.get(ID);
	}

	public RecruitmentJobSpecificCertsDTO getRecruitmentJobSpecificCertsDTOByID( long ID){
		return clone(mapOfRecruitmentJobSpecificCertDTOToiD.get(ID));
	}

	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "recruitment_job_specific_certs";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}

	public RecruitmentJobSpecificCertsDTO clone(RecruitmentJobSpecificCertsDTO dto) {
		String raw = gson.toJson(dto);
		return gson.fromJson(raw, RecruitmentJobSpecificCertsDTO.class);
	}

	public List<RecruitmentJobSpecificCertsDTO> clone(List<RecruitmentJobSpecificCertsDTO> dtoList) {
		return dtoList
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}


}


