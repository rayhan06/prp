package recruitment_job_description;

import com.google.gson.Gson;
import committees_mapping.Committees_mappingDAO;
import common.ApiResponse;
import files.FilesDAO;
import files.FilesDTO;
import job_applicant_application.Job_applicant_applicationDTO;
import job_applicant_application.Job_applicant_applicationRepository;
import language.LanguageDTO;
import login.LoginDTO;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import pb.*;
import permission.MenuConstants;
import recruitment_exam_venue.RecruitmentExamVenueItemModel;
import recruitment_job_req_trains_and_certs.Recruitment_job_req_trains_and_certsRepository;
import recruitment_job_specific_exam_type.JobSpecificExamTypeDao;
import recruitment_job_specific_exam_type.JobSpecificExamTypeRepository;
import recruitment_job_specific_exam_type.RecruitmentJobSpecificExamTypeDTO;
import recruitment_marks_committee.CommitteeMemberWithStatusDTO;
import recruitment_marks_committee_members.Recruitment_marks_committee_membersDAO;
import recruitment_marks_committee_members.Recruitment_marks_committee_membersRepository;
import recruitment_seat_plan.RecruitmentSeatPlanChildDTO;
import recruitment_seat_plan.RecruitmentSeatPlanChildRepository;
import role.PermissionRepository;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.*;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;


/**
 * Servlet implementation class Recruitment_job_descriptionServlet
 */
@WebServlet("/Recruitment_job_descriptionServlet")
@MultipartConfig
public class Recruitment_job_descriptionServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Recruitment_job_descriptionServlet.class);

    String tableName = "recruitment_job_description";

    Recruitment_job_descriptionDAO recruitment_job_descriptionDAO;
    CommonRequestHandler commonRequestHandler;
    FilesDAO filesDAO = new FilesDAO();
    RecruitmentJobSpecificFilesDAO recruitmentJobSpecificFilesDAO;
    RecruitmentJobSpecificCertsDAO recruitmentJobSpecificCertsDAO;
    private final Gson gson = new Gson();

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Recruitment_job_descriptionServlet() {
        super();
        try {
            recruitment_job_descriptionDAO = new Recruitment_job_descriptionDAO(tableName);
            recruitmentJobSpecificFilesDAO = new RecruitmentJobSpecificFilesDAO("recruitment_job_specific_files");
            recruitmentJobSpecificCertsDAO = new RecruitmentJobSpecificCertsDAO("recruitment_job_specific_certs");
            commonRequestHandler = new CommonRequestHandler(recruitment_job_descriptionDAO);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("In doget request = " + request);
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        boolean isPermanentTable = true;
        if (request.getParameter("isPermanentTable") != null) {
            isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
        }
        System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
        try {
            String actionType = request.getParameter("actionType");
            if (actionType.equals("getAddPage")) {
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.RECRUITMENT_JOB_DESCRIPTION_ADD)) {
                    commonRequestHandler.getAddPage(request, response);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else if (actionType.equals("getEditPage")) {
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.RECRUITMENT_JOB_DESCRIPTION_UPDATE)) {
                    getRecruitment_job_description(request, response);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else if (actionType.equals("downloadDropzoneFile")) {
                long id = Long.parseLong(request.getParameter("id"));

                FilesDTO filesDTO = (FilesDTO) filesDAO.getDTOByID(id);

                Utils.ProcessFile(request, response, filesDTO.fileTitle, filesDTO.inputStream);

            } else if (actionType.equals("getExamTypesByJobId")) {
                long jobId = Long.parseLong(request.getParameter("jobId"));
                String language = request.getParameter("language");
                //String options = getExamTypesByJob(jobId, language, true);
                String options = getExamTypesByJob(jobId, language, true);
                PrintWriter out = response.getWriter();
                out.println(options);
                out.close();
            } else if (actionType.equals("getExamTypesByJobIdForMarksEntry")) {
                long jobId = Long.parseLong(request.getParameter("jobId"));
                String language = request.getParameter("language");
                //String options = getExamTypesByJob(jobId, language, true);
                String options = getExamTypesByJob(jobId, language, false);
                PrintWriter out = response.getWriter();
                out.println(options);
                out.close();
            } else if (actionType.equals("getJobListByStatus")) {
                String status = request.getParameter("status");
                String language = request.getParameter("language");
                String options = recruitment_job_descriptionDAO.getJobList(status, language);
                PrintWriter out = response.getWriter();
                out.println(options);
                out.close();
            } else if (actionType.equals("getMarksCommittee")) {
                Long jobId = Long.parseLong(request.getParameter("jobId"));
                Recruitment_job_descriptionDTO jobDescriptionDTO = Recruitment_job_descriptionRepository.getInstance().
                        getRecruitment_job_descriptionDTOByID(jobId);

                CommitteeMemberWithStatusDTO statusDTO = new CommitteeMemberWithStatusDTO();
                statusDTO.status = jobDescriptionDTO.marks_committee_id != 0;
                if (statusDTO.status) {
                    statusDTO.committeeId = jobDescriptionDTO.marks_committee_id;
                    statusDTO.marks_committee_membersDTOS = Recruitment_marks_committee_membersRepository.getInstance()
                            .getRecruitment_marks_committee_membersDTOByrecruitment_marks_committee_id(jobDescriptionDTO.marks_committee_id);
//							new Recruitment_marks_committee_membersDAO().
//							getAllRecruitment_marks_committee_membersByCommitteeId(jobDescriptionDTO.marks_committee_id);
                }

                PrintWriter out = response.getWriter();
                response.setContentType("application/json");
                response.setCharacterEncoding("UTF-8");

                String encoded = this.gson.toJson(statusDTO);
                System.out.println("json encoded data = " + encoded);
                out.print(encoded);
                out.flush();

            }

//			else if(actionType.equals("getJobListForMarksCommittee"))
//			{
//				List<Recruitment_job_descriptionDTO> jobDescriptionDTOS = recruitment_job_descriptionDAO.
//						getAllRecruitment_job_description(true);
////						.stream()
////						.filter(i -> i.marks_committee_id == 0).collect(Collectors.toList());
//
//
////				System.out.println("In getDTO");
////				Recruitment_job_descriptionDTO recruitment_job_descriptionDTO = (Recruitment_job_descriptionDTO)recruitment_job_descriptionDAO.getDTOByID(Long.parseLong(request.getParameter("ID")));
//
//				PrintWriter out = response.getWriter();
//				response.setContentType("application/json");
//				response.setCharacterEncoding("UTF-8");
//
//				String encoded = this.gson.toJson(jobDescriptionDTOS);
//				System.out.println("json encoded data = " + encoded);
//				out.print(encoded);
//				out.flush();
//			}

            else if (actionType.equals("getJobListByTestNameAndStatusWithoutInternship")) {
                String status = request.getParameter("status");
                Long testNameId = Long.parseLong(request.getParameter("recruitmentTestNameId"));
                String language = request.getParameter("language");
                String options = recruitment_job_descriptionDAO.getJobListByTestNameAndWithoutInternship(testNameId,status, language);
                PrintWriter out = response.getWriter();
                out.println(options);
                out.close();
            }

            else if (actionType.equals("getJobListByStatusWithoutInternship")) {
                String status = request.getParameter("status");
                String language = request.getParameter("language");
                String options = recruitment_job_descriptionDAO.getJobListWithoutInternship(status, language);
                PrintWriter out = response.getWriter();
                out.println(options);
                out.close();
            } else if (actionType.equals("getJobListForSummaryByStatus")) {
                String status = request.getParameter("status");
                String language = request.getParameter("language");
                String options = recruitment_job_descriptionDAO.getJobListForSummary(status, language);
                PrintWriter out = response.getWriter();
                out.println(options);
                out.close();
            } else if (actionType.equals("previewDropzoneFile")) {
                long id = Long.parseLong(request.getParameter("id"));

                FilesDTO filesDTO = (FilesDTO) filesDAO.getDTOByID(id);

                Utils.ProcessFile(request, response, filesDTO.fileTitle, filesDTO);

            } else if (actionType.equals("DeleteFileFromDropZone")) {
                long id = Long.parseLong(request.getParameter("id"));


                System.out.println("In delete file");
                filesDAO.hardDeleteByID(id);
                response.getWriter().write("Deleted");

            } else if (actionType.equals("getURL")) {
                String URL = request.getParameter("URL");
                System.out.println("URL = " + URL);
                response.sendRedirect(URL);
            } else if (actionType.equals("search")) {
                System.out.println("search requested");
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.RECRUITMENT_JOB_DESCRIPTION_SEARCH)) {
                    if (isPermanentTable) {
                        String filter = request.getParameter("filter");
                        System.out.println("filter = " + filter);
                        if (filter != null) {
                            filter = ""; //shouldn't be directly used, rather manipulate it.
                            searchRecruitment_job_description(request, response, isPermanentTable, filter);
                        } else {
                            searchRecruitment_job_description(request, response, isPermanentTable, "");
                        }
                    } else {
                        //searchRecruitment_job_description(request, response, tempTableName, isPermanentTable);
                    }
                }
            } else if (actionType.equals("public_search")) {
                System.out.println("search requested");
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.RECRUITMENT_JOB_DESCRIPTION_SEARCH)) {
                    if (isPermanentTable) {
                        System.out.println("###################################");
                        String filter = ""; //"";
//						System.out.println("filter = " + filter);
                        long currentDate = System.currentTimeMillis();
                        filter = currentDate + " > first_application_date  AND ";
                        filter += currentDate + " < last_application_date ";
                        searchPublicRecruitment_job_description(request, response, isPermanentTable, filter);
//						if(filter!=null)
//						{
//							System.out.println("1st block");
//							long currentDate = System.currentTimeMillis();
//							filter = currentDate + " > last_application_date "; //shouldn't be directly used, rather manipulate it.
//
//							searchPublicRecruitment_job_description(request, response, isPermanentTable, filter);
//						}
//						else
//						{
//							System.out.println("2nd block");
//							searchPublicRecruitment_job_description(request, response, isPermanentTable, "");
//						}
                        System.out.println("###################################");
                    } else {
                        //searchRecruitment_job_description(request, response, tempTableName, isPermanentTable);
                    }
                }
            } else if (actionType.equals("getApprovalPage")) {
                System.out.println("Recruitment_job_description getApprovalPage requested");
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.RECRUITMENT_JOB_DESCRIPTION_APPROVE)) {
                    searchRecruitment_job_description(request, response, false, "");
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else if (actionType.equals("viewApprovalNotification")) {
                System.out.println("view requested");
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.APPROVAL_TEST_SEARCH)) {
                    commonRequestHandler.viewApprovalNotification(request, response);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else if (actionType.equals("view")) {
                System.out.println("view requested");
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.RECRUITMENT_JOB_DESCRIPTION_SEARCH)) {
                    commonRequestHandler.view(request, response);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }

            } else if (actionType.equals("public_view")) {
                System.out.println(" public view requested");
                String ID = request.getParameter("ID");
                RequestDispatcher rd;
                rd = request.getRequestDispatcher("recruitment_job_description/recruitment_job_descriptionPublicView.jsp?ID=" + ID);

                rd.forward(request, response);


            } else if (actionType.equals("getJobDescriptionByRecTestName")) {
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.RECRUITMENT_JOB_DESCRIPTION_UPDATE)) {
                    String options = "";
                    String language = userDTO.languageID == CommonConstant.Language_ID_English ? "English" : "Bangla";
                    Long recruitmentTestNameId = Long.parseLong(request.getParameter("recruitmentTestNameId"));
                    String selectedId = (request.getParameter("selectedId"));

                    if (selectedId==null || selectedId.equalsIgnoreCase("")) {
                        options =  Recruitment_job_descriptionRepository.getInstance().buildOptions(language, null, recruitmentTestNameId);
                    } else {
                        Long recJobDescId = Long.parseLong(selectedId);
                        options =  Recruitment_job_descriptionRepository.getInstance().buildOptions(language, recJobDescId, recruitmentTestNameId);
                    }

                    response.setContentType("text/html; charset=UTF-8");
                    response.getWriter().println(options);
                    return;
                }
                else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            }

            else if (actionType.equals("getJobDescriptionForAdmitCardByRecTestName")) {
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.RECRUITMENT_JOB_DESCRIPTION_UPDATE)) {
                    String options = "";
                    String language = userDTO.languageID == CommonConstant.Language_ID_English ? "English" : "Bangla";
                    Long recruitmentTestNameId = Long.parseLong(request.getParameter("recruitmentTestNameId"));
                    String selectedId = (request.getParameter("selectedId"));

                    if (selectedId==null || selectedId.equalsIgnoreCase("")) {
                        options =  Recruitment_job_descriptionRepository.getInstance().buildOptionsForAdmitCard(language, null, recruitmentTestNameId);
                    } else {
                        Long recJobDescId = Long.parseLong(selectedId);
                        options =  Recruitment_job_descriptionRepository.getInstance().buildOptionsForAdmitCard(language, recJobDescId, recruitmentTestNameId);
                    }

                    response.setContentType("text/html; charset=UTF-8");
                    response.getWriter().println(options);
                    return;
                }
                else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            }

            else if (actionType.equals("getExamTypesByTestNameAndJobId")) {
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.RECRUITMENT_JOB_DESCRIPTION_UPDATE)) {
                    String options = "";
                    String language = userDTO.languageID == CommonConstant.Language_ID_English ? "English" : "Bangla";
                    Long recruitmentTestNameId = Long.parseLong(request.getParameter("recruitmentTestNameId"));
                    Long jobId = Long.parseLong(request.getParameter("jobId"));
                    String selectedId = (request.getParameter("selectedId"));

                    if (selectedId==null || selectedId.equalsIgnoreCase("")) {
                        options =  JobSpecificExamTypeRepository.getInstance().buildOptions(language, null, jobId);
                    } else {
                        Long recSpecificExamId = Long.parseLong(selectedId);
                        options =  JobSpecificExamTypeRepository.getInstance().buildOptions(language, recSpecificExamId, jobId);
                    }

                    response.setContentType("text/html; charset=UTF-8");
                    response.getWriter().println(options);
                    return;
                }
                else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.debug(ex);
        }
    }


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        System.out.println("doPost");
        boolean isPermanentTable = true;
        if (request.getParameter("isPermanentTable") != null) {
            isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
        }
        System.out.println("In servlet, isPermanentTable = " + isPermanentTable);

        try {
            String actionType = request.getParameter("actionType");
            System.out.println("actionType = " + actionType);
            if (actionType.equals("add")) {

                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.RECRUITMENT_JOB_DESCRIPTION_ADD)) {
                    System.out.println("going to  addRecruitment_job_description ");
                    addRecruitment_job_description(request, response, true, userDTO, true);
                } else {
                    System.out.println("Not going to  addRecruitment_job_description ");
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }

            } else if (actionType.equals("UploadFilesFromDropZone")) {
                String Column = request.getParameter("columnName");
                long ColumnID = Long.parseLong(request.getParameter("ColumnID"));
                String pageType = request.getParameter("pageType");

                System.out.println("In " + pageType);
                Utils.UploadFilesFromDropZone(request, response, userDTO.ID, ColumnID, "Recruitment_job_descriptionServlet");
            } else if (actionType.equals("SendToApprovalPath")) {

                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.RECRUITMENT_JOB_DESCRIPTION_APPROVE)) {
                    commonRequestHandler.sendToApprovalPath(request, response, userDTO);
                } else {
                    System.out.println("Not going to SendToApprovalPath ");
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }

            } else if (actionType.equals("approve")) {

                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.RECRUITMENT_JOB_DESCRIPTION_ADD)) {
                    commonRequestHandler.approve(request, response, true, userDTO, true);
                } else {
                    System.out.println("Not going to  addRecruitment_job_description ");
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }

            } else if (actionType.equals("reject")) {
                System.out.println("trying to approve");

                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.RECRUITMENT_JOB_DESCRIPTION_APPROVE)) {
                    commonRequestHandler.approve(request, response, true, userDTO, false);
                } else {
                    System.out.println("Not going to  addRecruitment_job_description ");
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else if (actionType.equals("terminate")) {

                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.RECRUITMENT_JOB_DESCRIPTION_ADD)) {
                    commonRequestHandler.terminate(request, response, userDTO);
                } else {
                    System.out.println("Not going to  addRecruitment_job_description ");
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }

            } else if (actionType.equals("skipStep")) {

                System.out.println("skipStep");
                commonRequestHandler.skipStep(request, response, userDTO);
            } else if (actionType.equals("getDTO")) {

                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.RECRUITMENT_JOB_DESCRIPTION_ADD)) {
                    getDTO(request, response);
                } else {
                    System.out.println("Not going to  addRecruitment_job_description ");
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }

            } else if (actionType.equals("edit")) {

                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.RECRUITMENT_JOB_DESCRIPTION_UPDATE)) {
                    addRecruitment_job_description(request, response, false, userDTO, isPermanentTable);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else if (actionType.equals("delete")) {
                deleteRecruitment_job_description(request, response, userDTO);
            } else if (actionType.equals("search")) {
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.RECRUITMENT_JOB_DESCRIPTION_SEARCH)) {
                    searchRecruitment_job_description(request, response, true, "");
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else if (actionType.equals("getGeo")) {
                System.out.println("going to geoloc ");
                request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            logger.debug(ex);
        }
    }

    private void getDTO(HttpServletRequest request, HttpServletResponse response) {
        try {
            System.out.println("In getDTO");
            Recruitment_job_descriptionDTO recruitment_job_descriptionDTO = Recruitment_job_descriptionRepository.getInstance().
                    getRecruitment_job_descriptionDTOByID(Long.parseLong(request.getParameter("ID")));
//					(Recruitment_job_descriptionDTO)recruitment_job_descriptionDAO.getDTOByID(Long.parseLong(request.getParameter("ID")));
            PrintWriter out = response.getWriter();
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");

            String encoded = this.gson.toJson(recruitment_job_descriptionDTO);
            System.out.println("json encoded data = " + encoded);
            out.print(encoded);
            out.flush();
        } catch (NumberFormatException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    private void addRecruitment_job_description(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, boolean isPermanentTable) throws IOException {
        // TODO Auto-generated method stub
        ApiResponse apiResponse;
        try {
            request.setAttribute("failureMessage", "");
            System.out.println("%%%% addRecruitment_job_description");
            String path = getServletContext().getRealPath("/img2/");
            Recruitment_job_descriptionDTO recruitment_job_descriptionDTO;
            SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");

            if (addFlag == true) {
                recruitment_job_descriptionDTO = new Recruitment_job_descriptionDTO();
            } else {
                recruitment_job_descriptionDTO = Recruitment_job_descriptionRepository.getInstance().
                        getRecruitment_job_descriptionDTOByID(Long.parseLong(request.getParameter("iD")));
//						(Recruitment_job_descriptionDTO)recruitment_job_descriptionDAO.getDTOByID
//								(Long.parseLong(request.getParameter("iD")));
            }

            String Value = "";

            Value = request.getParameter("recruitmentTestName");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("recruitmentTestName = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                recruitment_job_descriptionDTO.recruitmentTestName = Long.parseLong((Value));
            } else {
                throw new Exception(" Invalid recruitment test name");
            }

            Value = request.getParameter("jobTitleEn");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("jobTitleEn = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                recruitment_job_descriptionDTO.jobTitleEn = (Value);
            } else {
                throw new Exception(" Invalid jobTitle En");
            }

            Value = request.getParameter("jobTitleBn");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("jobTitleBn = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                recruitment_job_descriptionDTO.jobTitleBn = (Value);
            } else {
                throw new Exception(" Invalid jobTitle Bn");
            }

            Value = request.getParameter("jobGradeCat");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("jobGradeCat = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                recruitment_job_descriptionDTO.jobGradeCat = Integer.parseInt(Value);
            } else {
                throw new Exception(" Invalid job grade");
            }

            Value = request.getParameter("employmentStatusCat");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("employmentStatusCat = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                recruitment_job_descriptionDTO.employmentStatusCat = Integer.parseInt(Value);
            } else {
                throw new Exception(" Invalid employment Status");
            }

            Value = request.getParameter("employeePayScaleType");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("employeePayScaleType = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                recruitment_job_descriptionDTO.employeePayScaleType = Long.parseLong(Value);
            } else {
                throw new Exception(" Invalid employee PayScale");
            }

            Value = request.getParameter("jobPurpose");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("jobPurpose = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                recruitment_job_descriptionDTO.jobPurpose = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("jobResponsibilities");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("jobResponsibilities = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                recruitment_job_descriptionDTO.jobResponsibilities = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("minimumAcademicQualification");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("minimumAcademicQualification = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                recruitment_job_descriptionDTO.minimumAcademicQualification = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("minimumExperienceRequired");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("minimumExperienceRequired = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                recruitment_job_descriptionDTO.minimumExperienceRequired = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("requiredCertificationAndTraning");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("requiredCertificationAndTraning = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                recruitment_job_descriptionDTO.requiredCertificationAndTraning = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("personalCharacteristics");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("personalCharacteristics = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                recruitment_job_descriptionDTO.personalCharacteristics = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("requiredCompetence");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("requiredCompetence = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                recruitment_job_descriptionDTO.requiredCompetence = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("educationLevelType");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("educationLevelType = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                recruitment_job_descriptionDTO.educationLevelType = Long.parseLong(Value);
            } else {
                throw new Exception(" Invalid education level");
            }


            Value = request.getParameter("yearlyEducationLevelType");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("yearlyEducationLevelType = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                recruitment_job_descriptionDTO.yearly_education_level_type = Long.parseLong(Value);
            } else {
                throw new Exception(" Invalid yearly education level");
            }

            Value = request.getParameter("minExperience");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("minExperience = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                recruitment_job_descriptionDTO.minExperience = Double.parseDouble(Value);
            } else {
                throw new Exception(" Invalid minExperience");
            }

            Value = request.getParameter("maxAgeOnBoundaryDateRegular");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("maxAgeOnBoundaryDateRegular = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                recruitment_job_descriptionDTO.maxAgeOnBoundaryDateRegular = Integer.parseInt(Value);
            } else {
                throw new Exception(" Invalid max age for regular");
            }

            Value = request.getParameter("maxAgeOnBoundaryDateFf");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("maxAgeOnBoundaryDateFf = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                recruitment_job_descriptionDTO.maxAgeOnBoundaryDateFf = Integer.parseInt(Value);
            } else {
                throw new Exception(" Invalid max age for freedom fighter children");
            }

            Value = request.getParameter("maxAgeOnBoundaryDateGrandChildrenFf");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("maxAgeOnBoundaryDateGrandChildrenFf = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                recruitment_job_descriptionDTO.max_age_for_grandchildren_ff = Integer.parseInt(Value);
            } else {
                throw new Exception(" Invalid max age for freedom fighter grandchildren");
            }


            Value = request.getParameter("minAge");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("minAge = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                recruitment_job_descriptionDTO.min_age = Integer.parseInt(Value);
            } else {
                throw new Exception(" Invalid min age");
            }

            Value = request.getParameter("maxAgeDisable");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("maxAgeDisable = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                recruitment_job_descriptionDTO.max_age_disable = Integer.parseInt(Value);
            } else {
                throw new Exception(" Invalid max age for disable");
            }


            Value = request.getParameter("jobLocation");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("jobLocation = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                recruitment_job_descriptionDTO.jobLocation = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("numberOfVacancy");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("numberOfVacancy = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                recruitment_job_descriptionDTO.numberOfVacancy = Integer.parseInt(Value);
            } else {
                throw new Exception(" Invalid number of vacancy");
            }

            Value = request.getParameter("lastApplicationDate");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("lastApplicationDate = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                Date d = f.parse(Value);
                recruitment_job_descriptionDTO.lastApplicationDate = d.getTime() + 86399999;

            } else {
                throw new Exception(" Invalid last Application Date");
            }


            Value = request.getParameter("startApplicationDate");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("startApplicationDate = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                Date d = f.parse(Value);
                recruitment_job_descriptionDTO.firstApplicationDate = d.getTime();

            } else {
                throw new Exception(" Invalid first Application Date");
            }

            Value = request.getParameter("lastAgeCalculationDate");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("lastAgeCalculationDate = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                Date d = f.parse(Value);
                recruitment_job_descriptionDTO.lastAgeCalculationDate = d.getTime();

            } else {
                throw new Exception(" Invalid last Age CalculationDate");
            }


            Value = request.getParameter("onlineJobPostingDate");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("onlineJobPostingDate = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                try {
                    Date d = f.parse(Value);
                    recruitment_job_descriptionDTO.onlineJobPostingDate = d.getTime();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("district");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            logger.debug("district = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                recruitment_job_descriptionDTO.districts = Value;
            } else {
                logger.debug("FieldName has a null Value, not updating" + " = " + Value);
            }


            Value = request.getParameter("admitCardPrintStartDate");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("admitCardPrintStartDate = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                try {
                    Date d = f.parse(Value);
                    recruitment_job_descriptionDTO.admitCardPrintStartDate = d.getTime();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }


            Value = request.getParameter("admitCardPrintEndDate");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("admitCardPrintEndDate = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                try {
                    Date d = f.parse(Value);
                    recruitment_job_descriptionDTO.admitCardPrintEndDate = d.getTime();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }


            Value = request.getParameter("filesDropzone");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("filesDropzone = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {


                System.out.println("filesDropzone = " + Value);
                if (Value != null && !Value.equalsIgnoreCase("")) {
                    recruitment_job_descriptionDTO.filesDropzone = Long.parseLong(Value);
                } else {
                    System.out.println("FieldName has a null Value, not updating" + " = " + Value);
                }

                if (addFlag == false) {
                    String filesDropzoneFilesToDelete = request.getParameter("filesDropzoneFilesToDelete");
                    String[] deleteArray = filesDropzoneFilesToDelete.split(",");
                    for (int i = 0; i < deleteArray.length; i++) {
                        System.out.println("going to delete " + deleteArray[i]);
                        if (i > 0) {
                            filesDAO.delete(Long.parseLong(deleteArray[i]));
                        }
                    }
                }
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("jobCat");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("jobCat = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                recruitment_job_descriptionDTO.jobCat = Integer.parseInt(Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            if (addFlag) {
                recruitment_job_descriptionDTO.insertedByUserId = userDTO.ID;
            }


            if (addFlag) {
                Calendar c = Calendar.getInstance();
                c.set(Calendar.HOUR_OF_DAY, 0);
                c.set(Calendar.MINUTE, 0);
                c.set(Calendar.SECOND, 0);
                c.set(Calendar.MILLISECOND, 0);

                recruitment_job_descriptionDTO.insertionDate = c.getTimeInMillis();

                recruitment_job_descriptionDTO.status = "ONGOING";


            }


            Value = request.getParameter("modifiedBy");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("modifiedBy = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                recruitment_job_descriptionDTO.modifiedBy = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            System.out.println("Done adding  addRecruitment_job_description dto = " + recruitment_job_descriptionDTO);


            childValidation(request);


            long returnedID = -1;

            if (isPermanentTable == false) //add new row for validation and make the old row outdated
            {
                recruitment_job_descriptionDAO.setIsDeleted(recruitment_job_descriptionDTO.iD, CommonDTO.OUTDATED);
                returnedID = recruitment_job_descriptionDAO.add(recruitment_job_descriptionDTO);
                recruitment_job_descriptionDAO.setIsDeleted(returnedID, CommonDTO.WAITING_FOR_APPROVAL);
            } else if (addFlag == true) {
                returnedID = recruitment_job_descriptionDAO.manageWriteOperations(recruitment_job_descriptionDTO, SessionConstants.INSERT, -1, userDTO);
            } else {
                returnedID = recruitment_job_descriptionDAO.manageWriteOperations(recruitment_job_descriptionDTO, SessionConstants.UPDATE, -1, userDTO);
            }


            List<RecruitmentJobSpecificFilesDTO> recruitmentJobSpecificFilesDTOList = createRecruitmentJobSpecificFilesDTOListByRequest(request);
            if (addFlag == true || isPermanentTable == false) //add or validate
            {
                if (recruitmentJobSpecificFilesDTOList != null) {
                    for (RecruitmentJobSpecificFilesDTO recruitmentJobSpecificFilesDTO : recruitmentJobSpecificFilesDTOList) {
                        recruitmentJobSpecificFilesDTO.recruitmentJobDescriptionId = recruitment_job_descriptionDTO.iD;
                        recruitmentJobSpecificFilesDAO.add(recruitmentJobSpecificFilesDTO);
                    }
                }

            } else {
                List<Long> childIdsFromRequest = recruitmentJobSpecificFilesDAO.getChildIdsFromRequest(request, "recruitmentJobSpecificFiles");
                //delete the removed children
                recruitmentJobSpecificFilesDAO.deleteChildrenNotInList("recruitment_job_description", "recruitment_job_specific_files", recruitment_job_descriptionDTO.iD, childIdsFromRequest);
                List<Long> childIDsInDatabase = recruitmentJobSpecificFilesDAO.getChilIds("recruitment_job_description", "recruitment_job_specific_files", recruitment_job_descriptionDTO.iD);


                if (childIdsFromRequest != null) {
                    for (int i = 0; i < childIdsFromRequest.size(); i++) {
                        Long childIDFromRequest = childIdsFromRequest.get(i);
                        if (childIDsInDatabase.contains(childIDFromRequest)) {
                            RecruitmentJobSpecificFilesDTO recruitmentJobSpecificFilesDTO = createRecruitmentJobSpecificFilesDTOByRequestAndIndex(request, false, i);
                            recruitmentJobSpecificFilesDTO.recruitmentJobDescriptionId = recruitment_job_descriptionDTO.iD;
                            recruitmentJobSpecificFilesDAO.update(recruitmentJobSpecificFilesDTO);
                        } else {
                            RecruitmentJobSpecificFilesDTO recruitmentJobSpecificFilesDTO = createRecruitmentJobSpecificFilesDTOByRequestAndIndex(request, true, i);
                            recruitmentJobSpecificFilesDTO.recruitmentJobDescriptionId = recruitment_job_descriptionDTO.iD;
                            recruitmentJobSpecificFilesDAO.add(recruitmentJobSpecificFilesDTO);
                        }
                    }
                } else {
                    recruitmentJobSpecificFilesDAO.deleteRecruitmentJobSpecificFilesByRecruitmentJobDescriptionID(recruitment_job_descriptionDTO.iD);
                }

            }

            List<RecruitmentJobSpecificCertsDTO> recruitmentJobSpecificCertsDTOList = createRecruitmentJobSpecificCertsDTOListByRequest(request);
            if (addFlag == true || isPermanentTable == false) //add or validate
            {
                if (recruitmentJobSpecificCertsDTOList != null) {
                    for (RecruitmentJobSpecificCertsDTO recruitmentJobSpecificCertsDTO : recruitmentJobSpecificCertsDTOList) {
                        recruitmentJobSpecificCertsDTO.recruitmentJobDescriptionId = recruitment_job_descriptionDTO.iD;
                        recruitmentJobSpecificCertsDAO.add(recruitmentJobSpecificCertsDTO);
                    }
                }

            } else {
                List<Long> childIdsFromRequest = recruitmentJobSpecificCertsDAO.getChildIdsFromRequest(request, "recruitmentJobSpecificCerts");
                //delete the removed children
                recruitmentJobSpecificCertsDAO.deleteChildrenNotInList("recruitment_job_description", "recruitment_job_specific_certs", recruitment_job_descriptionDTO.iD, childIdsFromRequest);
                List<Long> childIDsInDatabase = recruitmentJobSpecificCertsDAO.getChilIds("recruitment_job_description", "recruitment_job_specific_certs", recruitment_job_descriptionDTO.iD);


                if (childIdsFromRequest != null) {
                    for (int i = 0; i < childIdsFromRequest.size(); i++) {
                        Long childIDFromRequest = childIdsFromRequest.get(i);
                        if (childIDsInDatabase.contains(childIDFromRequest)) {
                            RecruitmentJobSpecificCertsDTO recruitmentJobSpecificCertsDTO = createRecruitmentJobSpecificCertsDTOByRequestAndIndex(request, false, i);
                            recruitmentJobSpecificCertsDTO.recruitmentJobDescriptionId = recruitment_job_descriptionDTO.iD;
                            recruitmentJobSpecificCertsDAO.update(recruitmentJobSpecificCertsDTO);
                        } else {
                            RecruitmentJobSpecificCertsDTO recruitmentJobSpecificCertsDTO = createRecruitmentJobSpecificCertsDTOByRequestAndIndex(request, true, i);
                            recruitmentJobSpecificCertsDTO.recruitmentJobDescriptionId = recruitment_job_descriptionDTO.iD;
                            recruitmentJobSpecificCertsDAO.add(recruitmentJobSpecificCertsDTO);
                        }
                    }
                } else {
                    recruitmentJobSpecificCertsDAO.deleteRecruitmentJobSpecificCertsByRecruitmentJobDescriptionID(recruitment_job_descriptionDTO.iD);
                }

            }

            // adding exam type

            if (addFlag) {
                List<CatDTO> examTypes = CatRepository.getDTOs("job_exam_type").
                        stream().filter(i -> i.value != 0).
                        collect(Collectors.toList());

                JobSpecificExamTypeDao jobSpecificExamTypeDao = new JobSpecificExamTypeDao();

                int initOrder = 1;
                for (CatDTO examTypeDTO : examTypes) {
                    RecruitmentJobSpecificExamTypeDTO recruitmentJobSpecificExamTypeDTO = new RecruitmentJobSpecificExamTypeDTO();
                    recruitmentJobSpecificExamTypeDTO.jobExamTypeCat = examTypeDTO.value;
                    recruitmentJobSpecificExamTypeDTO.recruitmentJobDescriptionId = recruitment_job_descriptionDTO.iD;
                    recruitmentJobSpecificExamTypeDTO.insertionDate = System.currentTimeMillis();
                    recruitmentJobSpecificExamTypeDTO.order = initOrder;
                    recruitmentJobSpecificExamTypeDTO.examStartDate = System.currentTimeMillis();
                    initOrder++;

                    try {
                        jobSpecificExamTypeDao.add(recruitmentJobSpecificExamTypeDTO);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }


            }


            apiResponse = ApiResponse.makeSuccessResponse("Recruitment_job_descriptionServlet?actionType=search");

        } catch (Exception e) {
            e.printStackTrace();
            apiResponse = ApiResponse.makeErrorResponse(e.getMessage());
        }

        PrintWriter pw = response.getWriter();
        pw.write(apiResponse.getJSONString());
        pw.flush();
        pw.close();
    }


    private void childValidation(HttpServletRequest request) throws Exception {
        String Value = "";
        if (request.getParameterValues("recruitmentJobSpecificFiles.iD") != null) {
            int recruitmentJobSpecificFilesItemNo = request.getParameterValues("recruitmentJobSpecificFiles.iD").length;
            for (int index = 0; index < recruitmentJobSpecificFilesItemNo; index++) {
                Value = request.getParameterValues("recruitmentJobSpecificFiles.recruitmentJobRequiredFilesType")[index];
                if (Value != null) {
                    Value = Jsoup.clean(Value, Whitelist.simpleText());
                    Long.parseLong(Value);
                } else {
                    throw new Exception(" Invalid files type");
                }
            }
        }


        if (request.getParameterValues("recruitmentJobSpecificCerts.iD") != null) {
            int recruitmentJobSpecificCertsItemNo = request.getParameterValues("recruitmentJobSpecificCerts.iD").length;
            for (int index = 0; index < recruitmentJobSpecificCertsItemNo; index++) {
                Value = request.getParameterValues("recruitmentJobSpecificCerts.recruitmentJobReqTrainsAndCertsType")[index];

                if (Value != null) {
                    Value = Jsoup.clean(Value, Whitelist.simpleText());
                    Long.parseLong(Value);
                } else {
                    throw new Exception(" Invalid TrainsAndCertsType");
                }

            }
        }


    }


    private List<RecruitmentJobSpecificFilesDTO> createRecruitmentJobSpecificFilesDTOListByRequest(HttpServletRequest request) throws Exception {
        List<RecruitmentJobSpecificFilesDTO> recruitmentJobSpecificFilesDTOList = new ArrayList<RecruitmentJobSpecificFilesDTO>();
        if (request.getParameterValues("recruitmentJobSpecificFiles.iD") != null) {
            int recruitmentJobSpecificFilesItemNo = request.getParameterValues("recruitmentJobSpecificFiles.iD").length;


            for (int index = 0; index < recruitmentJobSpecificFilesItemNo; index++) {
                RecruitmentJobSpecificFilesDTO recruitmentJobSpecificFilesDTO = createRecruitmentJobSpecificFilesDTOByRequestAndIndex(request, true, index);
                recruitmentJobSpecificFilesDTOList.add(recruitmentJobSpecificFilesDTO);
            }

            return recruitmentJobSpecificFilesDTOList;
        }
        return null;
    }

    private List<RecruitmentJobSpecificCertsDTO> createRecruitmentJobSpecificCertsDTOListByRequest(HttpServletRequest request) throws Exception {
        List<RecruitmentJobSpecificCertsDTO> recruitmentJobSpecificCertsDTOList = new ArrayList<RecruitmentJobSpecificCertsDTO>();
        if (request.getParameterValues("recruitmentJobSpecificCerts.iD") != null) {
            int recruitmentJobSpecificCertsItemNo = request.getParameterValues("recruitmentJobSpecificCerts.iD").length;


            for (int index = 0; index < recruitmentJobSpecificCertsItemNo; index++) {
                RecruitmentJobSpecificCertsDTO recruitmentJobSpecificCertsDTO = createRecruitmentJobSpecificCertsDTOByRequestAndIndex(request, true, index);
                recruitmentJobSpecificCertsDTOList.add(recruitmentJobSpecificCertsDTO);
            }

            return recruitmentJobSpecificCertsDTOList;
        }
        return null;
    }


    private RecruitmentJobSpecificFilesDTO createRecruitmentJobSpecificFilesDTOByRequestAndIndex(HttpServletRequest request, boolean addFlag, int index) throws Exception {

        RecruitmentJobSpecificFilesDTO recruitmentJobSpecificFilesDTO;
        if (addFlag == true) {
            recruitmentJobSpecificFilesDTO = new RecruitmentJobSpecificFilesDTO();
        } else {
            recruitmentJobSpecificFilesDTO = RecruitmentJobSpecificFilesRepository.getInstance().
                    getRecruitmentJobSpecificFilesDTOByID(Long.parseLong
                            (request.getParameterValues("recruitmentJobSpecificFiles.iD")[index]));
//					(RecruitmentJobSpecificFilesDTO)recruitmentJobSpecificFilesDAO.getDTOByID
//							(Long.parseLong(request.getParameterValues("recruitmentJobSpecificFiles.iD")[index]));
        }
        String path = getServletContext().getRealPath("/img2/");
        SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");


        String Value = "";
        Value = request.getParameterValues("recruitmentJobSpecificFiles.recruitmentJobDescriptionId")[index];

        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }

        recruitmentJobSpecificFilesDTO.recruitmentJobDescriptionId = Long.parseLong(Value);
        Value = request.getParameterValues("recruitmentJobSpecificFiles.recruitmentJobRequiredFilesType")[index];

        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }

        recruitmentJobSpecificFilesDTO.recruitmentJobRequiredFilesType = Long.parseLong(Value);
        Value = request.getParameterValues("recruitmentJobSpecificFiles.isMandatoryFiles")[index];

        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }

        recruitmentJobSpecificFilesDTO.isMandatoryFiles = Boolean.parseBoolean(Value);
        Value = request.getParameterValues("recruitmentJobSpecificFiles.insertionDate")[index];

        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }

        recruitmentJobSpecificFilesDTO.insertionDate = Long.parseLong(Value);
        Value = request.getParameterValues("recruitmentJobSpecificFiles.insertedByUserId")[index];

        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }

        recruitmentJobSpecificFilesDTO.insertedByUserId = Long.parseLong(Value);
//		Value = request.getParameterValues("recruitmentJobSpecificFiles.modifiedBy")[index];
//
//		if(Value != null)
//		{
//			Value = Jsoup.clean(Value,Whitelist.simpleText());
//		}
//
//		recruitmentJobSpecificFilesDTO.modifiedBy = (Value);
        return recruitmentJobSpecificFilesDTO;

    }


    private RecruitmentJobSpecificCertsDTO createRecruitmentJobSpecificCertsDTOByRequestAndIndex(HttpServletRequest request, boolean addFlag, int index) throws Exception {

        RecruitmentJobSpecificCertsDTO recruitmentJobSpecificCertsDTO;
        if (addFlag == true) {
            recruitmentJobSpecificCertsDTO = new RecruitmentJobSpecificCertsDTO();
        } else {
            recruitmentJobSpecificCertsDTO = RecruitmentJobSpecificCertsRepository.getInstance().
                    getRecruitmentJobSpecificCertsDTOByID(Long.parseLong
                            (request.getParameterValues("recruitmentJobSpecificCerts.iD")[index]));
//					(RecruitmentJobSpecificCertsDTO)recruitmentJobSpecificCertsDAO.getDTOByID
//							(Long.parseLong(request.getParameterValues("recruitmentJobSpecificCerts.iD")[index]));
        }
        String path = getServletContext().getRealPath("/img2/");
        SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");


        String Value = "";
        Value = request.getParameterValues("recruitmentJobSpecificCerts.recruitmentJobDescriptionId")[index];

        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }

        recruitmentJobSpecificCertsDTO.recruitmentJobDescriptionId = Long.parseLong(Value);
        Value = request.getParameterValues("recruitmentJobSpecificCerts.recruitmentJobReqTrainsAndCertsType")[index];

        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }

        recruitmentJobSpecificCertsDTO.recruitmentJobReqTrainsAndCertsType = Long.parseLong(Value);
        Value = request.getParameterValues("recruitmentJobSpecificCerts.isMandatoryCerts")[index];

        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }

        recruitmentJobSpecificCertsDTO.isMandatoryCerts = Boolean.parseBoolean(Value);

        Value = request.getParameterValues("recruitmentJobSpecificCerts.file_required")[index];

        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }

        recruitmentJobSpecificCertsDTO.file_required = Boolean.parseBoolean(Value);


        Value = request.getParameterValues("recruitmentJobSpecificCerts.insertionDate")[index];

        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }

        recruitmentJobSpecificCertsDTO.insertionDate = Long.parseLong(Value);
        Value = request.getParameterValues("recruitmentJobSpecificCerts.insertedByUserId")[index];

        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }

        recruitmentJobSpecificCertsDTO.insertedByUserId = Long.parseLong(Value);
//		Value = request.getParameterValues("recruitmentJobSpecificCerts.modifiedBy")[index];
//
//		if(Value != null)
//		{
//			Value = Jsoup.clean(Value,Whitelist.simpleText());
//		}
//
//		recruitmentJobSpecificCertsDTO.modifiedBy = (Value);
        return recruitmentJobSpecificCertsDTO;

    }


    private void deleteRecruitment_job_description(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO) throws IOException {
        try {
            String[] IDsToDelete = request.getParameterValues("ID");
            for (int i = 0; i < IDsToDelete.length; i++) {
                long id = Long.parseLong(IDsToDelete[i]);
                System.out.println("------ DELETING " + IDsToDelete[i]);


                Recruitment_job_descriptionDTO recruitment_job_descriptionDTO = Recruitment_job_descriptionRepository.getInstance().
                        getRecruitment_job_descriptionDTOByID(id);
//						(Recruitment_job_descriptionDTO)recruitment_job_descriptionDAO.getDTOByID(id);
                recruitment_job_descriptionDAO.manageWriteOperations(recruitment_job_descriptionDTO, SessionConstants.DELETE, id, userDTO);
                recruitmentJobSpecificFilesDAO.deleteRecruitmentJobSpecificFilesByRecruitmentJobDescriptionID(id);
                recruitmentJobSpecificCertsDAO.deleteRecruitmentJobSpecificCertsByRecruitmentJobDescriptionID(id);
                response.sendRedirect("Recruitment_job_descriptionServlet?actionType=search");

            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    private void getRecruitment_job_description(HttpServletRequest request, HttpServletResponse response, long id) throws ServletException, IOException {
        System.out.println("in getRecruitment_job_description");
        Recruitment_job_descriptionDTO recruitment_job_descriptionDTO = null;
        try {
            recruitment_job_descriptionDTO = Recruitment_job_descriptionRepository.getInstance().
                    getRecruitment_job_descriptionDTOByID(id);
//					(Recruitment_job_descriptionDTO)recruitment_job_descriptionDAO.getDTOByID(id);
            boolean isPermanentTable = true;
            if (request.getParameter("isPermanentTable") != null) {
                isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
            }
            request.setAttribute("ID", recruitment_job_descriptionDTO.iD);
            request.setAttribute("recruitment_job_descriptionDTO", recruitment_job_descriptionDTO);
            request.setAttribute("recruitment_job_descriptionDAO", recruitment_job_descriptionDAO);

            String URL = "";

            String inPlaceEdit = request.getParameter("inplaceedit");
            String inPlaceSubmit = request.getParameter("inplacesubmit");
            String getBodyOnly = request.getParameter("getBodyOnly");

            if (inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase("")) {
                URL = "recruitment_job_description/recruitment_job_descriptionInPlaceEdit.jsp";
                request.setAttribute("inplaceedit", "");
            } else if (inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase("")) {
                URL = "recruitment_job_description/recruitment_job_descriptionSearchRow.jsp";
                request.setAttribute("inplacesubmit", "");
            } else {
                if (getBodyOnly != null && !getBodyOnly.equalsIgnoreCase("")) {
                    URL = "recruitment_job_description/recruitment_job_descriptionEditBody.jsp?actionType=edit";
                } else {
                    URL = "recruitment_job_description/recruitment_job_descriptionEdit.jsp?actionType=edit";
                }
            }

            RequestDispatcher rd = request.getRequestDispatcher(URL);
            rd.forward(request, response);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void getRecruitment_job_description(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        getRecruitment_job_description(request, response, Long.parseLong(request.getParameter("ID")));
    }

    private void searchRecruitment_job_description(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException {
        System.out.println("in  searchRecruitment_job_description 1");
        LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        String ajax = request.getParameter("ajax");
        boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);

        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
                SessionConstants.NAV_RECRUITMENT_JOB_DESCRIPTION,
                request,
                recruitment_job_descriptionDAO,
                SessionConstants.VIEW_RECRUITMENT_JOB_DESCRIPTION,
                SessionConstants.SEARCH_RECRUITMENT_JOB_DESCRIPTION,
                tableName,
                isPermanent,
                userDTO,
                filter,
                true);
        try {
            System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        } catch (Exception e) {
            System.out.println("failed to dojob" + e);
        }

        request.setAttribute("recruitment_job_descriptionDAO", recruitment_job_descriptionDAO);
        RequestDispatcher rd;
        if (!isPermanent) {
            if (hasAjax == false) {
                System.out.println("Going to recruitment_job_description/recruitment_job_descriptionApproval.jsp");
                rd = request.getRequestDispatcher("recruitment_job_description/recruitment_job_descriptionApproval.jsp");
            } else {
                System.out.println("Going to recruitment_job_description/recruitment_job_descriptionApprovalForm.jsp");
                rd = request.getRequestDispatcher("recruitment_job_description/recruitment_job_descriptionApprovalForm.jsp");
            }
        } else {
            if (hasAjax == false) {
                System.out.println("Going to recruitment_job_description/recruitment_job_descriptionSearch.jsp");
                rd = request.getRequestDispatcher("recruitment_job_description/recruitment_job_descriptionSearch.jsp");
            } else {
                System.out.println("Going to recruitment_job_description/recruitment_job_descriptionSearchForm.jsp");
                rd = request.getRequestDispatcher("recruitment_job_description/recruitment_job_descriptionSearchForm.jsp");
            }
        }
        rd.forward(request, response);
    }

    private void searchPublicRecruitment_job_description(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException {
        System.out.println("in  searchRecruitment_job_description 1");
        LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
//		String ajax = (String)request.getParameter("ajax");
//		boolean hasAjax = false;
//		if(ajax != null && !ajax.equalsIgnoreCase(""))
//		{
//			hasAjax = true;
//		}
//		System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);

        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
                SessionConstants.NAV_RECRUITMENT_JOB_DESCRIPTION,
                request,
                recruitment_job_descriptionDAO,
                SessionConstants.VIEW_RECRUITMENT_JOB_DESCRIPTION,
                SessionConstants.SEARCH_RECRUITMENT_JOB_DESCRIPTION,
                tableName,
                isPermanent,
                userDTO,
                filter,
                true);
        try {
            System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        } catch (Exception e) {
            System.out.println("failed to dojob" + e);
        }

        request.setAttribute("recruitment_job_descriptionDAO", recruitment_job_descriptionDAO);
        RequestDispatcher rd;
        {

            System.out.println("Going to recruitment_job_description/recruitment_job_descriptionPublicSearch.jsp");
            rd = request.getRequestDispatcher("recruitment_job_description/recruitment_job_descriptionPublicSearch.jsp");
        }
        rd.forward(request, response);
    }

    String getExamTypesByJob(long jobId, String Language, boolean withDefault) {
        String sOptions = "";

        if (withDefault) {
            if (Language.equals("English")) {
                sOptions = "<option value = '0'>Primary</option>";
            } else {
                sOptions = "<option value = '0'>প্রাথমিক</option>";
            }
        }


        List<RecruitmentJobSpecificExamTypeDTO> jobSpecificExamTypeDTOS =
                JobSpecificExamTypeRepository.getInstance().
                        getRecruitmentJobSpecificExamTypeDTOByjob_id(jobId);


        for (RecruitmentJobSpecificExamTypeDTO dto : jobSpecificExamTypeDTOS) {
            if (dto.isSelected) {
                sOptions += "<option value = '" + dto.order + "'>" +
                        CatRepository.getName(Language, "job_exam_type", dto.jobExamTypeCat) + "</option>";
            }
        }

        return sOptions;
    }

}

