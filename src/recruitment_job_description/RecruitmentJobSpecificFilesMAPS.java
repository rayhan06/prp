package recruitment_job_description;
import java.util.*; 
import util.*;


public class RecruitmentJobSpecificFilesMAPS extends CommonMaps
{	
	public RecruitmentJobSpecificFilesMAPS(String tableName)
	{
		

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("recruitmentJobDescriptionId".toLowerCase(), "recruitmentJobDescriptionId".toLowerCase());
		java_DTO_map.put("recruitmentJobRequiredFilesType".toLowerCase(), "recruitmentJobRequiredFilesType".toLowerCase());
		java_DTO_map.put("isMandatoryFiles".toLowerCase(), "isMandatoryFiles".toLowerCase());
		java_DTO_map.put("insertionDate".toLowerCase(), "insertionDate".toLowerCase());
		java_DTO_map.put("insertedByUserId".toLowerCase(), "insertedByUserId".toLowerCase());
		java_DTO_map.put("modifiedBy".toLowerCase(), "modifiedBy".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("recruitment_job_description_id".toLowerCase(), "recruitmentJobDescriptionId".toLowerCase());
		java_SQL_map.put("recruitment_job_required_files_type".toLowerCase(), "recruitmentJobRequiredFilesType".toLowerCase());
		java_SQL_map.put("is_mandatory_files".toLowerCase(), "isMandatoryFiles".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Recruitment Job Description Id".toLowerCase(), "recruitmentJobDescriptionId".toLowerCase());
		java_Text_map.put("Recruitment Job Required Files".toLowerCase(), "recruitmentJobRequiredFilesType".toLowerCase());
		java_Text_map.put("Is Mandatory Files".toLowerCase(), "isMandatoryFiles".toLowerCase());
		java_Text_map.put("Insertion Date".toLowerCase(), "insertionDate".toLowerCase());
		java_Text_map.put("Inserted By User Id".toLowerCase(), "insertedByUserId".toLowerCase());
		java_Text_map.put("Modified By".toLowerCase(), "modifiedBy".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}