package recruitment_job_description;
import java.util.*; 
import util.*; 


public class RecruitmentJobSpecificFilesDTO extends CommonDTO
{

	public long recruitmentJobDescriptionId = 0;
	public long recruitmentJobRequiredFilesType = 0;
	public boolean isMandatoryFiles = false;
	public long insertionDate = 0;
	public long insertedByUserId = 0;
    public String modifiedBy = "";
    public String nameBn = "";
	public String nameEn = "";

	public List<RecruitmentJobSpecificFilesDTO> recruitmentJobSpecificFilesDTOList = new ArrayList<>();
	public List<RecruitmentJobSpecificCertsDTO> recruitmentJobSpecificCertsDTOList = new ArrayList<>();
	
    @Override
	public String toString() {
            return "$RecruitmentJobSpecificFilesDTO[" +
            " iD = " + iD +
            " recruitmentJobDescriptionId = " + recruitmentJobDescriptionId +
            " recruitmentJobRequiredFilesType = " + recruitmentJobRequiredFilesType +
            " isMandatoryFiles = " + isMandatoryFiles +
            " insertionDate = " + insertionDate +
            " insertedByUserId = " + insertedByUserId +
            " modifiedBy = " + modifiedBy +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
			" nameBn = " + nameBn +
			" nameEn = " + nameEn +
            "]";
    }

}