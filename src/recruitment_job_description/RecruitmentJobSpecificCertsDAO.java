package recruitment_job_description;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;

import common.ConnectionAndStatementUtil;
import sessionmanager.SessionConstants;
import java.sql.SQLException;


import org.apache.log4j.Logger;

import dbm.*;

import repository.RepositoryManager;

import util.*;
import pb.*;
import user.UserDTO;
import vm_vehicle_office_assignment.Vm_vehicle_office_assignmentDTO;

public class RecruitmentJobSpecificCertsDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());
	
	
	public RecruitmentJobSpecificCertsDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";
		commonMaps = new RecruitmentJobSpecificCertsMAPS(tableName);
		columnNames = new String[]
		{
			"ID",
			"recruitment_job_description_id",
			"recruitment_job_req_trains_and_certs_type",
			"is_mandatory_certs",
			"file_required",
			"insertion_date",
			"inserted_by_user_id",
			"modified_by",
			"isDeleted",
			"lastModificationTime"
		};
	}




	
	public RecruitmentJobSpecificCertsDAO()
	{
		this("recruitment_job_specific_certs");		
	}
	
	public void setSearchColumn(RecruitmentJobSpecificCertsDTO recruitmentjobspecificcertsDTO)
	{
		recruitmentjobspecificcertsDTO.searchColumn = "";
		recruitmentjobspecificcertsDTO.searchColumn += CommonDAO.getName("English", "recruitment_job_req_trains_and_certs", recruitmentjobspecificcertsDTO.recruitmentJobReqTrainsAndCertsType) + " " + CommonDAO.getName("Bangla", "recruitment_job_req_trains_and_certs", recruitmentjobspecificcertsDTO.recruitmentJobReqTrainsAndCertsType) + " ";
		recruitmentjobspecificcertsDTO.searchColumn += recruitmentjobspecificcertsDTO.modifiedBy + " ";
		
	}
	
	public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException
	{
		RecruitmentJobSpecificCertsDTO recruitmentjobspecificcertsDTO = (RecruitmentJobSpecificCertsDTO)commonDTO;
		int index = 1;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(recruitmentjobspecificcertsDTO);
		if(isInsert)
		{
			ps.setObject(index++,recruitmentjobspecificcertsDTO.iD);
		}
		ps.setObject(index++,recruitmentjobspecificcertsDTO.recruitmentJobDescriptionId);
		ps.setObject(index++,recruitmentjobspecificcertsDTO.recruitmentJobReqTrainsAndCertsType);
		ps.setObject(index++,recruitmentjobspecificcertsDTO.isMandatoryCerts);
		ps.setObject(index++,recruitmentjobspecificcertsDTO.file_required);
		ps.setObject(index++,recruitmentjobspecificcertsDTO.insertionDate);
		ps.setObject(index++,recruitmentjobspecificcertsDTO.insertedByUserId);
		ps.setObject(index++,recruitmentjobspecificcertsDTO.modifiedBy);
		if(isInsert)
		{
			ps.setObject(index++, 0);
			ps.setObject(index++, lastModificationTime);
		}
	}

	public RecruitmentJobSpecificCertsDTO build(ResultSet rs)
	{
		try
		{
			RecruitmentJobSpecificCertsDTO recruitmentjobspecificcertsDTO = new RecruitmentJobSpecificCertsDTO();
			recruitmentjobspecificcertsDTO.iD = rs.getLong("ID");
			recruitmentjobspecificcertsDTO.recruitmentJobDescriptionId = rs.getLong("recruitment_job_description_id");
			recruitmentjobspecificcertsDTO.recruitmentJobReqTrainsAndCertsType = rs.getLong("recruitment_job_req_trains_and_certs_type");
			recruitmentjobspecificcertsDTO.isMandatoryCerts = rs.getBoolean("is_mandatory_certs");
			recruitmentjobspecificcertsDTO.insertionDate = rs.getLong("insertion_date");
			recruitmentjobspecificcertsDTO.insertedByUserId = rs.getLong("inserted_by_user_id");
			recruitmentjobspecificcertsDTO.modifiedBy = rs.getString("modified_by");
			recruitmentjobspecificcertsDTO.isDeleted = rs.getInt("isDeleted");
			recruitmentjobspecificcertsDTO.lastModificationTime = rs.getLong("lastModificationTime");
			recruitmentjobspecificcertsDTO.file_required = rs.getBoolean("file_required");
			return recruitmentjobspecificcertsDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
	
	

		
	
	public void deleteRecruitmentJobSpecificCertsByRecruitmentJobDescriptionID(long recruitmentJobDescriptionID) throws Exception{

			
		//String sql = "UPDATE recruitment_job_specific_certs SET isDeleted=0 WHERE recruitment_job_description_id="+recruitmentJobDescriptionID;
		String sql = "delete from recruitment_job_specific_certs WHERE recruitment_job_description_id=" + recruitmentJobDescriptionID;
		logger.debug("sql " + sql);

		ConnectionAndStatementUtil.getWriteStatement(st -> {
			try {
				st.execute(sql);
			} catch (SQLException ex) {
				logger.error(ex);
			}
		});
	}		
   
	public List<RecruitmentJobSpecificCertsDTO> getRecruitmentJobSpecificCertsDTOListByRecruitmentJobDescriptionID(long recruitmentJobDescriptionID) throws Exception{
		String sql = "SELECT * FROM recruitment_job_specific_certs where isDeleted=0 and recruitment_job_description_id= ? order by recruitment_job_specific_certs.lastModificationTime";
		logger.debug("sql " + sql);
		return ConnectionAndStatementUtil.getListOfT(sql, Arrays.asList(recruitmentJobDescriptionID),this::build);
	}

	//need another getter for repository
	public RecruitmentJobSpecificCertsDTO getDTOByID (long ID)
	{
			
			String sql = "SELECT * ";
			sql += " FROM " + tableName;
            sql += " WHERE ID= ? ";
			printSql(sql);

			RecruitmentJobSpecificCertsDTO recruitmentJobSpecificCertsDTO =
					ConnectionAndStatementUtil.getT(sql, Arrays.asList(ID), this::build);
			return recruitmentJobSpecificCertsDTO;
			



//		RecruitmentJobSpecificFilesDAO recruitmentJobSpecificFilesDAO = new RecruitmentJobSpecificFilesDAO("recruitment_job_specific_files");
//		List<RecruitmentJobSpecificFilesDTO> recruitmentJobSpecificFilesDTOList = recruitmentJobSpecificFilesDAO.getRecruitmentJobSpecificFilesDTOListByRecruitmentJobDescriptionID(recruitmentjobspecificcertsDTO.iD);
//		recruitmentjobspecificcertsDTO.recruitmentJobSpecificFilesDTOList = recruitmentJobSpecificFilesDTOList;
//
//		RecruitmentJobSpecificCertsDAO recruitmentJobSpecificCertsDAO = new RecruitmentJobSpecificCertsDAO("recruitment_job_specific_certs");
//		List<RecruitmentJobSpecificCertsDTO> recruitmentJobSpecificCertsDTOList = recruitmentJobSpecificCertsDAO.getRecruitmentJobSpecificCertsDTOListByRecruitmentJobDescriptionID(recruitmentjobspecificcertsDTO.iD);
//		recruitmentjobspecificcertsDTO.recruitmentJobSpecificCertsDTOList = recruitmentJobSpecificCertsDTOList;
	}
	
	
	public List<RecruitmentJobSpecificCertsDTO> getDTOs(Collection recordIDs){
		if(recordIDs.isEmpty()){
			return new ArrayList<>();
		}
		String sql = "SELECT * FROM " + tableName + " WHERE ID IN ( ";
		for(int i = 0;i<recordIDs.size();i++){
			if(i!=0){
				sql+=",";
			}
			sql+= " ? ";
		}
		sql+=")  order by lastModificationTime desc";
		printSql(sql);
		return ConnectionAndStatementUtil.getListOfT(sql, Arrays.asList(recordIDs.toArray()),this::build);
	
	}

	//add repository
	public List<RecruitmentJobSpecificCertsDTO> getAllRecruitmentJobSpecificCerts (boolean isFirstReload)
    {
		List<RecruitmentJobSpecificCertsDTO> recruitmentjobspecificcertsDTOList = new ArrayList<>();

		String sql = "SELECT * FROM recruitment_job_specific_certs";
		sql += " WHERE ";
	

		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by lastModificationTime desc";
		printSql(sql);
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);
    }

	
	public List<RecruitmentJobSpecificCertsDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO)
	{
		return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
	}

	
	public List<RecruitmentJobSpecificCertsDTO> getDTOs(Hashtable  p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
			String filter, boolean tableHasJobCat)
	{
		String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat);
		printSql(sql);
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);
	}
				
}
	