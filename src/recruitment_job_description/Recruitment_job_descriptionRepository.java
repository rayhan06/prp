package recruitment_job_description;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.stream.Collectors;

import com.google.gson.Gson;
import election_constituency.Election_constituencyDTO;
import org.apache.log4j.Logger;

import pb.OptionDTO;
import pb.Utils;
import recruitment_seat_plan.Recruitment_seat_planDTO;
import recruitment_test_name.Recruitment_test_nameDTO;
import repository.Repository;
import repository.RepositoryManager;
import vm_route.VmRouteStoppageDTO;


public class Recruitment_job_descriptionRepository implements Repository {
	Recruitment_job_descriptionDAO recruitment_job_descriptionDAO = new Recruitment_job_descriptionDAO();
	Gson gson = new Gson();
	private static final ReentrantReadWriteLock lock = new ReentrantReadWriteLock();
	private static final ReentrantReadWriteLock.ReadLock readLock = lock.readLock();
	
	public void setDAO(Recruitment_job_descriptionDAO recruitment_job_descriptionDAO)
	{
		this.recruitment_job_descriptionDAO = recruitment_job_descriptionDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Recruitment_job_descriptionRepository.class);
	Map<Long, Recruitment_job_descriptionDTO>mapOfRecruitment_job_descriptionDTOToiD;
	private final Map<Long, Set<Recruitment_job_descriptionDTO>> mapByRecTestNameId;


	static Recruitment_job_descriptionRepository instance = null;  
	private Recruitment_job_descriptionRepository(){
		mapOfRecruitment_job_descriptionDTOToiD = new ConcurrentHashMap<>();
		mapByRecTestNameId = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Recruitment_job_descriptionRepository getInstance(){
		if (instance == null){
			instance = new Recruitment_job_descriptionRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(recruitment_job_descriptionDAO == null)
		{
			return;
		}
		try {
			List<Recruitment_job_descriptionDTO> recruitment_job_descriptionDTOs = recruitment_job_descriptionDAO.getAllRecruitment_job_description(reloadAll);
			for(Recruitment_job_descriptionDTO recruitment_job_descriptionDTO : recruitment_job_descriptionDTOs) {
				Recruitment_job_descriptionDTO oldRecruitment_job_descriptionDTO =
						getRecruitment_job_descriptionDTOByIDWithoutClone(recruitment_job_descriptionDTO.iD);
				if( oldRecruitment_job_descriptionDTO != null ) {
					mapOfRecruitment_job_descriptionDTOToiD.remove(oldRecruitment_job_descriptionDTO.iD);

					if(mapByRecTestNameId.containsKey(oldRecruitment_job_descriptionDTO.recruitmentTestName)) {
						mapByRecTestNameId.get(oldRecruitment_job_descriptionDTO.recruitmentTestName).remove(oldRecruitment_job_descriptionDTO);
					}
					if(mapByRecTestNameId.get(oldRecruitment_job_descriptionDTO.recruitmentTestName).isEmpty()) {
						mapByRecTestNameId.remove(oldRecruitment_job_descriptionDTO.recruitmentTestName);
					}

					
				}
				if(recruitment_job_descriptionDTO.isDeleted == 0) 
				{
					
					mapOfRecruitment_job_descriptionDTOToiD.put(recruitment_job_descriptionDTO.iD, recruitment_job_descriptionDTO);

					if( ! mapByRecTestNameId.containsKey(recruitment_job_descriptionDTO.recruitmentTestName)) {
						mapByRecTestNameId.put(recruitment_job_descriptionDTO.recruitmentTestName, new HashSet<>());
					}
					mapByRecTestNameId.get(recruitment_job_descriptionDTO.recruitmentTestName).add(recruitment_job_descriptionDTO);
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Recruitment_job_descriptionDTO> getRecruitment_job_descriptionList() {
		List <Recruitment_job_descriptionDTO> recruitment_job_descriptions = new ArrayList<Recruitment_job_descriptionDTO>(this.mapOfRecruitment_job_descriptionDTOToiD.values());
		return clone(recruitment_job_descriptions);
	}
	
	
	public Recruitment_job_descriptionDTO getRecruitment_job_descriptionDTOByIDWithoutClone( long ID){
		return mapOfRecruitment_job_descriptionDTOToiD.get(ID);
	}

	public Recruitment_job_descriptionDTO getRecruitment_job_descriptionDTOByID( long ID){
		return clone(mapOfRecruitment_job_descriptionDTOToiD.get(ID));
	}

	public Recruitment_job_descriptionDTO clone(Recruitment_job_descriptionDTO dto) {
		String raw = gson.toJson(dto);
		return gson.fromJson(raw, Recruitment_job_descriptionDTO.class);
	}

	public List<Recruitment_job_descriptionDTO> clone(List<Recruitment_job_descriptionDTO> dtoList) {
		return dtoList
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}

	public List<Recruitment_job_descriptionDTO> getRecruitment_job_descriptionListByRecTestNameId(long recruitmentTestNameId) {
		return new ArrayList<>( mapByRecTestNameId.getOrDefault(recruitmentTestNameId,new HashSet<>()));
	}

	public List<Recruitment_job_descriptionDTO> copyRecruitment_job_descriptionListByRecTestNameId(long recruitmentTestNameId)
	{
		List <Recruitment_job_descriptionDTO> recruitmentJobDescriptionDTOS = getRecruitment_job_descriptionListByRecTestNameId(recruitmentTestNameId);
		return recruitmentJobDescriptionDTOS
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "recruitment_job_description";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}

	public String buildOptions(String language,Long selectedId,long recruitmentTestNameId){

		List<Recruitment_job_descriptionDTO> recruitmentJobDescriptionDTOS = getRecruitment_job_descriptionListByRecTestNameId(recruitmentTestNameId);

		return buildOptions(language,selectedId,recruitmentJobDescriptionDTOS);
	}

	public String buildOptionsForAdmitCard(String language,Long selectedId,long recruitmentTestNameId){

		List<Recruitment_job_descriptionDTO> recruitmentJobDescriptionDTOS = getRecruitment_job_descriptionListByRecTestNameId(recruitmentTestNameId);

		long currentTime = System.currentTimeMillis();
		recruitmentJobDescriptionDTOS
				.stream()
				.filter(i -> i.lastApplicationDate < currentTime)
				.sorted(Comparator.comparingLong(i -> i.lastModificationTime)) // sort in descending order of economic year
				.collect(Collectors.toList());

		return buildOptions(language,selectedId,recruitmentJobDescriptionDTOS);
	}

	public static String buildOptions(String language,Long selectedId,List<Recruitment_job_descriptionDTO> list){
		List<OptionDTO> optionDTOList = null;
		if (list != null && list.size() > 0) {
			optionDTOList = list.stream()
					.map(Recruitment_job_descriptionDTO::getOptionDTO)
					.collect(Collectors.toList());
		}
		return Utils.buildOptions(optionDTOList, language, selectedId == null ? null : String.valueOf(selectedId));
	}
}


