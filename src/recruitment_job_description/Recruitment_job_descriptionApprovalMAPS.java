package recruitment_job_description;
import util.*;

public class Recruitment_job_descriptionApprovalMAPS extends CommonMaps
{	
	public Recruitment_job_descriptionApprovalMAPS(String tableName)
	{
		
		java_allfield_type_map.put("job_title_en".toLowerCase(), "String");
		java_allfield_type_map.put("job_title_bn".toLowerCase(), "String");
		java_allfield_type_map.put("job_grade_cat".toLowerCase(), "Integer");
		java_allfield_type_map.put("employment_status_cat".toLowerCase(), "Integer");
		java_allfield_type_map.put("employee_pay_scale_type".toLowerCase(), "Long");
		java_allfield_type_map.put("job_purpose".toLowerCase(), "String");
		java_allfield_type_map.put("job_responsibilities".toLowerCase(), "String");
		java_allfield_type_map.put("minimum_academic_qualification".toLowerCase(), "String");
		java_allfield_type_map.put("minimum_experience_required".toLowerCase(), "String");
		java_allfield_type_map.put("required_certification_and_traning".toLowerCase(), "String");
		java_allfield_type_map.put("personal_characteristics".toLowerCase(), "String");
		java_allfield_type_map.put("required_competence".toLowerCase(), "String");
		java_allfield_type_map.put("education_level_type".toLowerCase(), "Long");
		java_allfield_type_map.put("min_experience".toLowerCase(), "Double");
		java_allfield_type_map.put("max_age_on_boundary_date_regular".toLowerCase(), "Integer");
		java_allfield_type_map.put("max_age_on_boundary_date_ff".toLowerCase(), "Integer");
		java_allfield_type_map.put("job_location".toLowerCase(), "String");
		java_allfield_type_map.put("number_of_vacancy".toLowerCase(), "Integer");
		java_allfield_type_map.put("last_application_date".toLowerCase(), "Long");
		java_allfield_type_map.put("last_age_calculation_date".toLowerCase(), "Long");
		java_allfield_type_map.put("online_job_posting_date".toLowerCase(), "Long");

		java_allfield_type_map.put("job_cat", "Integer");
		java_allfield_type_map.put("approval_status_cat", "Integer");
		java_allfield_type_map.put("initiator", "Long");
		java_allfield_type_map.put("assigned_to", "Long");
		java_allfield_type_map.put("starting_date", "Long");
		java_allfield_type_map.put("ending_date", "Long");
		
		java_table_map.put("approval_status_cat", "approval_summary");
		java_table_map.put("initiator", "approval_summary");
		java_table_map.put("assigned_to", "approval_summary");
	}

}