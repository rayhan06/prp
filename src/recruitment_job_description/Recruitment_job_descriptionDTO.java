package recruitment_job_description;
import java.util.*;

import pb.OptionDTO;
import util.*;


public class Recruitment_job_descriptionDTO extends CommonDTO
{

    public String jobTitleEn = "";
    public String jobTitleBn = "";
	public int jobGradeCat = 0;
	public int employmentStatusCat = 0;
	public long employeePayScaleType = 0;
    public String jobPurpose = "";
    public String jobResponsibilities = "";
    public String minimumAcademicQualification = "";
    public String minimumExperienceRequired = "";
    public String requiredCertificationAndTraning = "";
    public String personalCharacteristics = "";
    public String requiredCompetence = "";
	public long educationLevelType = 0;
	public double minExperience = 0;
	public int maxAgeOnBoundaryDateRegular = 0;
	public int maxAgeOnBoundaryDateFf = 0;
    public String jobLocation = "";
	public int numberOfVacancy = 0;
	public long lastApplicationDate = 0;
	public long lastAgeCalculationDate = 0;
	public long onlineJobPostingDate = 0;
	public long admitCardPrintStartDate = 0;
	public long admitCardPrintEndDate = 0;
	public long filesDropzone = 0;
	public long insertedByUserId = 0;
	public long insertionDate = 0;
    public String modifiedBy = "";
	public String status = "";
	public String closureRemarks = "";
	public long firstApplicationDate = 0;
	public String districts = "";
	public boolean roll_created = false;
	public long yearly_education_level_type = 0;
	public int max_age_for_grandchildren_ff = 0;
	public int min_age = 0;
	public long marks_committee_id = 0;
	public int max_age_disable = 0;
	public int count = 0;
	public long recruitmentTestName;
	
	public List<RecruitmentJobSpecificFilesDTO> recruitmentJobSpecificFilesDTOList = new ArrayList<>();
	public List<RecruitmentJobSpecificCertsDTO> recruitmentJobSpecificCertsDTOList = new ArrayList<>();

	public OptionDTO getOptionDTO() {

		return new OptionDTO(
				jobTitleEn,
				jobTitleBn,
				String.format("%d", iD)
		);
	}
	
    @Override
	public String toString() {
            return "$Recruitment_job_descriptionDTO[" +

            " iD = " + iD +
            " jobTitleEn = " + jobTitleEn +
            " jobTitleBn = " + jobTitleBn +
            " jobGradeCat = " + jobGradeCat +
            " employmentStatusCat = " + employmentStatusCat +
            " employeePayScaleType = " + employeePayScaleType +
            " jobPurpose = " + jobPurpose +
            " jobResponsibilities = " + jobResponsibilities +
            " minimumAcademicQualification = " + minimumAcademicQualification +
            " minimumExperienceRequired = " + minimumExperienceRequired +
            " requiredCertificationAndTraning = " + requiredCertificationAndTraning +
            " personalCharacteristics = " + personalCharacteristics +
            " requiredCompetence = " + requiredCompetence +
            " educationLevelType = " + educationLevelType +
            " minExperience = " + minExperience +
            " maxAgeOnBoundaryDateRegular = " + maxAgeOnBoundaryDateRegular +
            " maxAgeOnBoundaryDateFf = " + maxAgeOnBoundaryDateFf +
            " jobLocation = " + jobLocation +
            " numberOfVacancy = " + numberOfVacancy +
            " lastApplicationDate = " + lastApplicationDate +
            " lastAgeCalculationDate = " + lastAgeCalculationDate +
            " onlineJobPostingDate = " + onlineJobPostingDate +
            " filesDropzone = " + filesDropzone +
            " jobCat = " + jobCat +
            " insertedByUserId = " + insertedByUserId +
            " insertionDate = " + insertionDate +
            " modifiedBy = " + modifiedBy +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
			" roll_created = " + roll_created +
			" yearly_education_level_type = " + yearly_education_level_type +
			" max_age_for_grandchildren_ff = " + max_age_for_grandchildren_ff +
			" min_age = " + min_age +
			" marks_committee_id = " + marks_committee_id  +
            "]";
    }

}