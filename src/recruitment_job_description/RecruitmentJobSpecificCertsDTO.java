package recruitment_job_description;
import java.util.*; 
import util.*; 


public class RecruitmentJobSpecificCertsDTO extends CommonDTO
{
	public String nameBn = "";
	public String nameEn = "";

	public long recruitmentJobDescriptionId = 0;
	public long recruitmentJobReqTrainsAndCertsType = 0;
	public boolean isMandatoryCerts = false;
	public boolean file_required = false;
	public long insertionDate = 0;
	public long insertedByUserId = 0;
    public String modifiedBy = "";
	
	public List<RecruitmentJobSpecificFilesDTO> recruitmentJobSpecificFilesDTOList = new ArrayList<>();
	public List<RecruitmentJobSpecificCertsDTO> recruitmentJobSpecificCertsDTOList = new ArrayList<>();
	
    @Override
	public String toString() {
            return "$RecruitmentJobSpecificCertsDTO[" +
            " iD = " + iD +
            " recruitmentJobDescriptionId = " + recruitmentJobDescriptionId +
            " recruitmentJobReqTrainsAndCertsType = " + recruitmentJobReqTrainsAndCertsType +
            " isMandatoryCerts = " + isMandatoryCerts +
			" file_required = " + file_required +
            " insertionDate = " + insertionDate +
            " insertedByUserId = " + insertedByUserId +
            " modifiedBy = " + modifiedBy +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}