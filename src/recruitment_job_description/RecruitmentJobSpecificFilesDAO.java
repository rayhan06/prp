package recruitment_job_description;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;

import common.ConnectionAndStatementUtil;
import sessionmanager.SessionConstants;
import java.sql.SQLException;


import org.apache.log4j.Logger;

import dbm.*;

import repository.RepositoryManager;

import util.*;
import pb.*;
import user.UserDTO;
import vm_vehicle_office_assignment.Vm_vehicle_office_assignmentDTO;

public class RecruitmentJobSpecificFilesDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());
	
	
	public RecruitmentJobSpecificFilesDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";
		commonMaps = new RecruitmentJobSpecificFilesMAPS(tableName);
		columnNames = new String[]
		{
			"ID",
			"recruitment_job_description_id",
			"recruitment_job_required_files_type",
			"is_mandatory_files",
			"insertion_date",
			"inserted_by_user_id",
			"modified_by",
			"isDeleted",
			"lastModificationTime"
		};
	}


	
	public RecruitmentJobSpecificFilesDAO()
	{
		this("recruitment_job_specific_files");		
	}
	
	public void setSearchColumn(RecruitmentJobSpecificFilesDTO recruitmentjobspecificfilesDTO)
	{
		recruitmentjobspecificfilesDTO.searchColumn = "";
		recruitmentjobspecificfilesDTO.searchColumn += CommonDAO.getName("English", "recruitment_job_required_files", recruitmentjobspecificfilesDTO.recruitmentJobRequiredFilesType) + " " + CommonDAO.getName("Bangla", "recruitment_job_required_files", recruitmentjobspecificfilesDTO.recruitmentJobRequiredFilesType) + " ";
		recruitmentjobspecificfilesDTO.searchColumn += recruitmentjobspecificfilesDTO.modifiedBy + " ";
		
	}
	
	public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException
	{
		RecruitmentJobSpecificFilesDTO recruitmentjobspecificfilesDTO = (RecruitmentJobSpecificFilesDTO)commonDTO;

		int index = 1;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(recruitmentjobspecificfilesDTO);
		if(isInsert)
		{
			ps.setObject(index++,recruitmentjobspecificfilesDTO.iD);
		}
		ps.setObject(index++,recruitmentjobspecificfilesDTO.recruitmentJobDescriptionId);
		ps.setObject(index++,recruitmentjobspecificfilesDTO.recruitmentJobRequiredFilesType);
		ps.setObject(index++,recruitmentjobspecificfilesDTO.isMandatoryFiles);
		ps.setObject(index++,recruitmentjobspecificfilesDTO.insertionDate);
		ps.setObject(index++,recruitmentjobspecificfilesDTO.insertedByUserId);
		ps.setObject(index++,recruitmentjobspecificfilesDTO.modifiedBy);
		if(isInsert)
		{
			ps.setObject(index++, 0);
			ps.setObject(index++, lastModificationTime);
		}
	}
	
	public void get(RecruitmentJobSpecificFilesDTO recruitmentjobspecificfilesDTO, ResultSet rs) throws SQLException
	{
		recruitmentjobspecificfilesDTO.iD = rs.getLong("ID");
		recruitmentjobspecificfilesDTO.recruitmentJobDescriptionId = rs.getLong("recruitment_job_description_id");
		recruitmentjobspecificfilesDTO.recruitmentJobRequiredFilesType = rs.getLong("recruitment_job_required_files_type");
		recruitmentjobspecificfilesDTO.isMandatoryFiles = rs.getBoolean("is_mandatory_files");
		recruitmentjobspecificfilesDTO.insertionDate = rs.getLong("insertion_date");
		recruitmentjobspecificfilesDTO.insertedByUserId = rs.getLong("inserted_by_user_id");
		recruitmentjobspecificfilesDTO.modifiedBy = rs.getString("modified_by");
		recruitmentjobspecificfilesDTO.isDeleted = rs.getInt("isDeleted");
		recruitmentjobspecificfilesDTO.lastModificationTime = rs.getLong("lastModificationTime");
	}

	public RecruitmentJobSpecificFilesDTO build(ResultSet rs)
	{
		try
		{
			RecruitmentJobSpecificFilesDTO recruitmentjobspecificfilesDTO = new RecruitmentJobSpecificFilesDTO();
			recruitmentjobspecificfilesDTO.iD = rs.getLong("ID");
			recruitmentjobspecificfilesDTO.recruitmentJobDescriptionId = rs.getLong("recruitment_job_description_id");
			recruitmentjobspecificfilesDTO.recruitmentJobRequiredFilesType = rs.getLong("recruitment_job_required_files_type");
			recruitmentjobspecificfilesDTO.isMandatoryFiles = rs.getBoolean("is_mandatory_files");
			recruitmentjobspecificfilesDTO.insertionDate = rs.getLong("insertion_date");
			recruitmentjobspecificfilesDTO.insertedByUserId = rs.getLong("inserted_by_user_id");
			recruitmentjobspecificfilesDTO.modifiedBy = rs.getString("modified_by");
			recruitmentjobspecificfilesDTO.isDeleted = rs.getInt("isDeleted");
			recruitmentjobspecificfilesDTO.lastModificationTime = rs.getLong("lastModificationTime");
			return recruitmentjobspecificfilesDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}

		
	
	public void deleteRecruitmentJobSpecificFilesByRecruitmentJobDescriptionID(long recruitmentJobDescriptionID) throws Exception{

		String sql = "delete from recruitment_job_specific_files WHERE recruitment_job_description_id=" + recruitmentJobDescriptionID;
		logger.debug("sql " + sql);

		ConnectionAndStatementUtil.getWriteStatement(st -> {
			try {
				st.execute(sql);
			} catch (SQLException ex) {
				logger.error(ex);
			}
		});

	}		
   
	public List<RecruitmentJobSpecificFilesDTO> getRecruitmentJobSpecificFilesDTOListByRecruitmentJobDescriptionID(long recruitmentJobDescriptionID) throws Exception{

		String sql = "SELECT * FROM recruitment_job_specific_files where isDeleted=0 and " +
				" recruitment_job_description_id= ? order by recruitment_job_specific_files.lastModificationTime";

		logger.debug("sql " + sql);
		return ConnectionAndStatementUtil.getListOfT(sql, Arrays.asList(recruitmentJobDescriptionID),this::build);
	}

	//need another getter for repository
	public RecruitmentJobSpecificFilesDTO getDTOByID (long ID)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID = ? ";
		RecruitmentJobSpecificFilesDTO recruitmentJobSpecificFilesDTO =
				ConnectionAndStatementUtil.getT(sql, Arrays.asList(ID), this::build);
		return recruitmentJobSpecificFilesDTO;




//		RecruitmentJobSpecificFilesDAO recruitmentJobSpecificFilesDAO = new RecruitmentJobSpecificFilesDAO("recruitment_job_specific_files");
//		List<RecruitmentJobSpecificFilesDTO> recruitmentJobSpecificFilesDTOList = recruitmentJobSpecificFilesDAO.getRecruitmentJobSpecificFilesDTOListByRecruitmentJobDescriptionID(recruitmentjobspecificfilesDTO.iD);
//		recruitmentjobspecificfilesDTO.recruitmentJobSpecificFilesDTOList = recruitmentJobSpecificFilesDTOList;
//
//		RecruitmentJobSpecificCertsDAO recruitmentJobSpecificCertsDAO = new RecruitmentJobSpecificCertsDAO("recruitment_job_specific_certs");
//		List<RecruitmentJobSpecificCertsDTO> recruitmentJobSpecificCertsDTOList = recruitmentJobSpecificCertsDAO.getRecruitmentJobSpecificCertsDTOListByRecruitmentJobDescriptionID(recruitmentjobspecificfilesDTO.iD);
//		recruitmentjobspecificfilesDTO.recruitmentJobSpecificCertsDTOList = recruitmentJobSpecificCertsDTOList;


	}

	
	
	public List<RecruitmentJobSpecificFilesDTO> getDTOs(Collection recordIDs){
		if(recordIDs.isEmpty()){
			return new ArrayList<>();
		}
		String sql = "SELECT * FROM " + tableName + " WHERE ID IN ( ";
		for(int i = 0;i<recordIDs.size();i++){
			if(i!=0){
				sql+=",";
			}
			sql+= " ? ";
		}
		sql+=")  order by lastModificationTime desc";
		printSql(sql);
		return ConnectionAndStatementUtil.getListOfT(sql, Arrays.asList(recordIDs.toArray()),this::build);
	
	}
	
	

	
	
	
	//add repository
	public List<RecruitmentJobSpecificFilesDTO> getAllRecruitmentJobSpecificFiles (boolean isFirstReload)
    {
		List<RecruitmentJobSpecificFilesDTO> recruitmentjobspecificfilesDTOList = new ArrayList<>();

		String sql = "SELECT * FROM recruitment_job_specific_files";
		sql += " WHERE ";
	

		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by lastModificationTime desc";
		printSql(sql);
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);
    }

	
	public List<RecruitmentJobSpecificFilesDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO)
	{
		return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
	}

	
	public List<RecruitmentJobSpecificFilesDTO> getDTOs(Hashtable  p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
			String filter, boolean tableHasJobCat)
	{
		String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat);

		printSql(sql);

		return ConnectionAndStatementUtil.getListOfT(sql,this::build);
	
	}
				
}
	