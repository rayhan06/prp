package recruitment_job_description;
import java.util.*; 
import util.*;


public class Recruitment_job_descriptionMAPS extends CommonMaps
{	
	public Recruitment_job_descriptionMAPS(String tableName)
	{
		

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("jobTitleEn".toLowerCase(), "jobTitleEn".toLowerCase());
		java_DTO_map.put("jobTitleBn".toLowerCase(), "jobTitleBn".toLowerCase());
		java_DTO_map.put("jobGradeCat".toLowerCase(), "jobGradeCat".toLowerCase());
		java_DTO_map.put("employmentStatusCat".toLowerCase(), "employmentStatusCat".toLowerCase());
		java_DTO_map.put("employeePayScaleType".toLowerCase(), "employeePayScaleType".toLowerCase());
		java_DTO_map.put("jobPurpose".toLowerCase(), "jobPurpose".toLowerCase());
		java_DTO_map.put("jobResponsibilities".toLowerCase(), "jobResponsibilities".toLowerCase());
		java_DTO_map.put("minimumAcademicQualification".toLowerCase(), "minimumAcademicQualification".toLowerCase());
		java_DTO_map.put("minimumExperienceRequired".toLowerCase(), "minimumExperienceRequired".toLowerCase());
		java_DTO_map.put("requiredCertificationAndTraning".toLowerCase(), "requiredCertificationAndTraning".toLowerCase());
		java_DTO_map.put("personalCharacteristics".toLowerCase(), "personalCharacteristics".toLowerCase());
		java_DTO_map.put("requiredCompetence".toLowerCase(), "requiredCompetence".toLowerCase());
		java_DTO_map.put("educationLevelType".toLowerCase(), "educationLevelType".toLowerCase());
		java_DTO_map.put("minExperience".toLowerCase(), "minExperience".toLowerCase());
		java_DTO_map.put("maxAgeOnBoundaryDateRegular".toLowerCase(), "maxAgeOnBoundaryDateRegular".toLowerCase());
		java_DTO_map.put("maxAgeOnBoundaryDateFf".toLowerCase(), "maxAgeOnBoundaryDateFf".toLowerCase());
		java_DTO_map.put("jobLocation".toLowerCase(), "jobLocation".toLowerCase());
		java_DTO_map.put("numberOfVacancy".toLowerCase(), "numberOfVacancy".toLowerCase());
		java_DTO_map.put("lastApplicationDate".toLowerCase(), "lastApplicationDate".toLowerCase());
		java_DTO_map.put("lastAgeCalculationDate".toLowerCase(), "lastAgeCalculationDate".toLowerCase());
		java_DTO_map.put("onlineJobPostingDate".toLowerCase(), "onlineJobPostingDate".toLowerCase());
		java_DTO_map.put("filesDropzone".toLowerCase(), "filesDropzone".toLowerCase());
		java_DTO_map.put("jobCat".toLowerCase(), "jobCat".toLowerCase());
		java_DTO_map.put("insertedByUserId".toLowerCase(), "insertedByUserId".toLowerCase());
		java_DTO_map.put("insertionDate".toLowerCase(), "insertionDate".toLowerCase());
		java_DTO_map.put("modifiedBy".toLowerCase(), "modifiedBy".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("job_title_en".toLowerCase(), "jobTitleEn".toLowerCase());
		java_SQL_map.put("job_title_bn".toLowerCase(), "jobTitleBn".toLowerCase());
		java_SQL_map.put("job_grade_cat".toLowerCase(), "jobGradeCat".toLowerCase());
		java_SQL_map.put("employment_status_cat".toLowerCase(), "employmentStatusCat".toLowerCase());
		java_SQL_map.put("employee_pay_scale_type".toLowerCase(), "employeePayScaleType".toLowerCase());
		java_SQL_map.put("job_purpose".toLowerCase(), "jobPurpose".toLowerCase());
		java_SQL_map.put("job_responsibilities".toLowerCase(), "jobResponsibilities".toLowerCase());
		java_SQL_map.put("minimum_academic_qualification".toLowerCase(), "minimumAcademicQualification".toLowerCase());
		java_SQL_map.put("minimum_experience_required".toLowerCase(), "minimumExperienceRequired".toLowerCase());
		java_SQL_map.put("required_certification_and_traning".toLowerCase(), "requiredCertificationAndTraning".toLowerCase());
		java_SQL_map.put("personal_characteristics".toLowerCase(), "personalCharacteristics".toLowerCase());
		java_SQL_map.put("required_competence".toLowerCase(), "requiredCompetence".toLowerCase());
		java_SQL_map.put("education_level_type".toLowerCase(), "educationLevelType".toLowerCase());
		java_SQL_map.put("min_experience".toLowerCase(), "minExperience".toLowerCase());
		java_SQL_map.put("max_age_on_boundary_date_regular".toLowerCase(), "maxAgeOnBoundaryDateRegular".toLowerCase());
		java_SQL_map.put("max_age_on_boundary_date_ff".toLowerCase(), "maxAgeOnBoundaryDateFf".toLowerCase());
		java_SQL_map.put("job_location".toLowerCase(), "jobLocation".toLowerCase());
		java_SQL_map.put("number_of_vacancy".toLowerCase(), "numberOfVacancy".toLowerCase());
		java_SQL_map.put("last_application_date".toLowerCase(), "lastApplicationDate".toLowerCase());
		java_SQL_map.put("last_age_calculation_date".toLowerCase(), "lastAgeCalculationDate".toLowerCase());
		java_SQL_map.put("online_job_posting_date".toLowerCase(), "onlineJobPostingDate".toLowerCase());
		java_SQL_map.put("files_dropzone".toLowerCase(), "filesDropzone".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Job Title En".toLowerCase(), "jobTitleEn".toLowerCase());
		java_Text_map.put("Job Title Bn".toLowerCase(), "jobTitleBn".toLowerCase());
		java_Text_map.put("Job Grade".toLowerCase(), "jobGradeCat".toLowerCase());
		java_Text_map.put("Employment Status".toLowerCase(), "employmentStatusCat".toLowerCase());
		java_Text_map.put("Employee Pay Scale".toLowerCase(), "employeePayScaleType".toLowerCase());
		java_Text_map.put("Job Purpose".toLowerCase(), "jobPurpose".toLowerCase());
		java_Text_map.put("Job Responsibilities".toLowerCase(), "jobResponsibilities".toLowerCase());
		java_Text_map.put("Minimum Academic Qualification".toLowerCase(), "minimumAcademicQualification".toLowerCase());
		java_Text_map.put("Minimum Experience Required".toLowerCase(), "minimumExperienceRequired".toLowerCase());
		java_Text_map.put("Required Certification And Traning".toLowerCase(), "requiredCertificationAndTraning".toLowerCase());
		java_Text_map.put("Personal Characteristics".toLowerCase(), "personalCharacteristics".toLowerCase());
		java_Text_map.put("Required Competence".toLowerCase(), "requiredCompetence".toLowerCase());
		java_Text_map.put("Education Level".toLowerCase(), "educationLevelType".toLowerCase());
		java_Text_map.put("Min Experience".toLowerCase(), "minExperience".toLowerCase());
		java_Text_map.put("Max Age On Boundary Date Regular".toLowerCase(), "maxAgeOnBoundaryDateRegular".toLowerCase());
		java_Text_map.put("Max Age On Boundary Date Ff".toLowerCase(), "maxAgeOnBoundaryDateFf".toLowerCase());
		java_Text_map.put("Job Location".toLowerCase(), "jobLocation".toLowerCase());
		java_Text_map.put("Number Of Vacancy".toLowerCase(), "numberOfVacancy".toLowerCase());
		java_Text_map.put("Last Application Date".toLowerCase(), "lastApplicationDate".toLowerCase());
		java_Text_map.put("Last Age Calculation Date".toLowerCase(), "lastAgeCalculationDate".toLowerCase());
		java_Text_map.put("Online Job Posting Date".toLowerCase(), "onlineJobPostingDate".toLowerCase());
		java_Text_map.put("Files Dropzone".toLowerCase(), "filesDropzone".toLowerCase());
		java_Text_map.put("Job".toLowerCase(), "jobCat".toLowerCase());
		java_Text_map.put("Inserted By User Id".toLowerCase(), "insertedByUserId".toLowerCase());
		java_Text_map.put("Insertion Date".toLowerCase(), "insertionDate".toLowerCase());
		java_Text_map.put("Modified By".toLowerCase(), "modifiedBy".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}