package recruitment_job_description;

import com.google.gson.Gson;
import org.apache.log4j.Logger;
import repository.Repository;
import repository.RepositoryManager;
import vm_route.VmRouteStoppageDTO;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;


public class RecruitmentJobSpecificFilesRepository implements Repository {
	RecruitmentJobSpecificFilesDAO recruitmentJobSpecificFilesDAO = null;
	Gson gson = new Gson();

	public void setDAO(RecruitmentJobSpecificFilesDAO recruitmentJobSpecificFilesDAO)
	{
		this.recruitmentJobSpecificFilesDAO = recruitmentJobSpecificFilesDAO;
	}


	static Logger logger = Logger.getLogger(RecruitmentJobSpecificFilesRepository.class);

	Map<Long, RecruitmentJobSpecificFilesDTO>mapOfRecruitmentJobSpecificFileDTOToiD;
	Map<Long, Set<RecruitmentJobSpecificFilesDTO>>mapOfRecruitmentJobSpecificFileToJobDescriptionID;



	static RecruitmentJobSpecificFilesRepository instance = null;
	private RecruitmentJobSpecificFilesRepository(){
		mapOfRecruitmentJobSpecificFileDTOToiD = new ConcurrentHashMap<>();
		mapOfRecruitmentJobSpecificFileToJobDescriptionID =  new ConcurrentHashMap<>();

		recruitmentJobSpecificFilesDAO = new RecruitmentJobSpecificFilesDAO();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static RecruitmentJobSpecificFilesRepository getInstance(){
		if (instance == null){
			instance = new RecruitmentJobSpecificFilesRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(recruitmentJobSpecificFilesDAO == null)
		{
			return;
		}
		try {
			List<RecruitmentJobSpecificFilesDTO> recruitmentJobSpecificFilesDTOS = recruitmentJobSpecificFilesDAO.getAllRecruitmentJobSpecificFiles(reloadAll);
			for(RecruitmentJobSpecificFilesDTO recruitmentJobSpecificFilesDTO : recruitmentJobSpecificFilesDTOS) {
				RecruitmentJobSpecificFilesDTO oldrecruitmentJobSpecificFilesDTO =
						getRecruitmentJobSpecificFilesDTOByIDWithoutClone(recruitmentJobSpecificFilesDTO.iD);
				if( oldrecruitmentJobSpecificFilesDTO != null ) {
					mapOfRecruitmentJobSpecificFileDTOToiD.remove(oldrecruitmentJobSpecificFilesDTO.iD);

					if(mapOfRecruitmentJobSpecificFileToJobDescriptionID.containsKey(oldrecruitmentJobSpecificFilesDTO.recruitmentJobDescriptionId)) {
						mapOfRecruitmentJobSpecificFileToJobDescriptionID.get(oldrecruitmentJobSpecificFilesDTO.recruitmentJobDescriptionId).remove(oldrecruitmentJobSpecificFilesDTO);
					}
					if(mapOfRecruitmentJobSpecificFileToJobDescriptionID.get(oldrecruitmentJobSpecificFilesDTO.recruitmentJobDescriptionId).isEmpty()) {
						mapOfRecruitmentJobSpecificFileToJobDescriptionID.remove(oldrecruitmentJobSpecificFilesDTO.recruitmentJobDescriptionId);
					}




				}
				if(recruitmentJobSpecificFilesDTO.isDeleted == 0)
				{

					mapOfRecruitmentJobSpecificFileDTOToiD.put(recruitmentJobSpecificFilesDTO.iD, recruitmentJobSpecificFilesDTO);

					if( ! mapOfRecruitmentJobSpecificFileToJobDescriptionID.containsKey(recruitmentJobSpecificFilesDTO.recruitmentJobDescriptionId)) {
						mapOfRecruitmentJobSpecificFileToJobDescriptionID.put(recruitmentJobSpecificFilesDTO.recruitmentJobDescriptionId, new HashSet<>());
					}
					mapOfRecruitmentJobSpecificFileToJobDescriptionID.get(recruitmentJobSpecificFilesDTO.recruitmentJobDescriptionId).add(recruitmentJobSpecificFilesDTO);



				}
			}

		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}

	public List<RecruitmentJobSpecificFilesDTO> getRecruitmentJobSpecificFilesDTOByJobDescriptionId(long recruitmentJobDescriptionId) {

		return new ArrayList<>( mapOfRecruitmentJobSpecificFileToJobDescriptionID.getOrDefault(recruitmentJobDescriptionId,new HashSet<>()));
	}

	public List<RecruitmentJobSpecificFilesDTO> getRecruitmentJobSpecificFilesDTOs() {
		List <RecruitmentJobSpecificFilesDTO> recruitmentJobSpecificFilesDTOS = new ArrayList<RecruitmentJobSpecificFilesDTO>(this.mapOfRecruitmentJobSpecificFileDTOToiD.values());
		return clone(recruitmentJobSpecificFilesDTOS);
	}


	public RecruitmentJobSpecificFilesDTO getRecruitmentJobSpecificFilesDTOByID( long ID){
		return clone(mapOfRecruitmentJobSpecificFileDTOToiD.get(ID));
	}

	public RecruitmentJobSpecificFilesDTO getRecruitmentJobSpecificFilesDTOByIDWithoutClone( long ID){
		return mapOfRecruitmentJobSpecificFileDTOToiD.get(ID);
	}

	public RecruitmentJobSpecificFilesDTO clone(RecruitmentJobSpecificFilesDTO dto) {
		String raw = gson.toJson(dto);
		return gson.fromJson(raw, RecruitmentJobSpecificFilesDTO.class);
	}

	public List<RecruitmentJobSpecificFilesDTO> clone(List<RecruitmentJobSpecificFilesDTO> dtoList) {
		return dtoList
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}

	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "recruitment_job_specific_files";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}


}


