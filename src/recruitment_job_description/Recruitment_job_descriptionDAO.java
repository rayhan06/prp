package recruitment_job_description;

import common.ConnectionAndStatementUtil;
import config.GlobalConfigConstants;
import config.GlobalConfigurationRepository;
import dbm.DBMR;
import dbm.DBMW;
import election_constituency.Election_constituencyDTO;
import election_constituency.Election_constituencyRepository;
import org.apache.log4j.Logger;
import parliament_job_applicant.Parliament_job_applicantDTO;
import pb.CatDAO;
import pb.CommonDAO;
import pb.OptionDTO;
import pb.Utils;
import recruitment_test_name.Recruitment_test_nameDTO;
import repository.RepositoryManager;
import user.UserDTO;
import util.CommonDTO;
import util.CommonMaps;
import util.NavigationService4;
import vm_vehicle_office_assignment.Vm_vehicle_office_assignmentDTO;

import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Recruitment_job_descriptionDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());
	public static HashMap<Integer, String> statusMap = new HashMap() {{
		put("CREATED", "তৈরী হয়েছে");
		put("ONGOING", "চলমান");
	}};
	public List<String> statusList = Arrays.asList("CREATED", "ONGOING");
	
	public CommonMaps approvalMaps = new Recruitment_job_descriptionApprovalMAPS("recruitment_job_description");
	
	public Recruitment_job_descriptionDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";
		commonMaps = new Recruitment_job_descriptionMAPS(tableName);
		columnNames = new String[]
		{
			"ID",
			"recruitment_test_name",
			"job_title_en",
			"job_title_bn",
			"job_grade_cat",
			"employment_status_cat",
			"employee_pay_scale_type",
			"job_purpose",
			"job_responsibilities",
			"minimum_academic_qualification",
			"minimum_experience_required",
			"required_certification_and_traning",
			"personal_characteristics",
			"required_competence",
			"education_level_type",
			"min_experience",
			"max_age_on_boundary_date_regular",
			"max_age_on_boundary_date_ff",
			"job_location",
			"number_of_vacancy",
			"last_application_date",
			"last_age_calculation_date",
			"online_job_posting_date",
			"admit_card_print_start_date",
			"admit_card_print_end_date",
			"files_dropzone",
			"job_cat",
			"inserted_by_user_id",
			"insertion_date",
			"modified_by",
			"status",
			"closure_remarks",
			"first_application_date",
			"districts",
			"roll_created",
			"yearly_education_level_type",
			"max_age_for_grandchildren_ff",
			"min_age",
			"max_age_disable",
			"marks_committee_id",
			"search_column",
			"isDeleted",
			"lastModificationTime"
		};






	}
	
	public Recruitment_job_descriptionDAO()
	{
		this("recruitment_job_description");
	}
	
	public void setSearchColumn(Recruitment_job_descriptionDTO recruitment_job_descriptionDTO)
	{
		recruitment_job_descriptionDTO.searchColumn = "";
		recruitment_job_descriptionDTO.searchColumn += recruitment_job_descriptionDTO.jobTitleEn + " ";
		recruitment_job_descriptionDTO.searchColumn += recruitment_job_descriptionDTO.jobTitleBn + " ";
		
	}
	public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException
	{
		Recruitment_job_descriptionDTO recruitment_job_descriptionDTO = (Recruitment_job_descriptionDTO)commonDTO;
		int index = 1;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(recruitment_job_descriptionDTO);
		if(isInsert)
		{
			ps.setObject(index++,recruitment_job_descriptionDTO.iD);
		}
		ps.setObject(index++,recruitment_job_descriptionDTO.recruitmentTestName);
		ps.setObject(index++,recruitment_job_descriptionDTO.jobTitleEn);
		ps.setObject(index++,recruitment_job_descriptionDTO.jobTitleBn);
		ps.setObject(index++,recruitment_job_descriptionDTO.jobGradeCat);
		ps.setObject(index++,recruitment_job_descriptionDTO.employmentStatusCat);
		ps.setObject(index++,recruitment_job_descriptionDTO.employeePayScaleType);
		ps.setObject(index++,recruitment_job_descriptionDTO.jobPurpose);
		ps.setObject(index++,recruitment_job_descriptionDTO.jobResponsibilities);
		ps.setObject(index++,recruitment_job_descriptionDTO.minimumAcademicQualification);
		ps.setObject(index++,recruitment_job_descriptionDTO.minimumExperienceRequired);
		ps.setObject(index++,recruitment_job_descriptionDTO.requiredCertificationAndTraning);
		ps.setObject(index++,recruitment_job_descriptionDTO.personalCharacteristics);
		ps.setObject(index++,recruitment_job_descriptionDTO.requiredCompetence);
		ps.setObject(index++,recruitment_job_descriptionDTO.educationLevelType);
		ps.setObject(index++,recruitment_job_descriptionDTO.minExperience);
		ps.setObject(index++,recruitment_job_descriptionDTO.maxAgeOnBoundaryDateRegular);
		ps.setObject(index++,recruitment_job_descriptionDTO.maxAgeOnBoundaryDateFf);
		ps.setObject(index++,recruitment_job_descriptionDTO.jobLocation);
		ps.setObject(index++,recruitment_job_descriptionDTO.numberOfVacancy);
		ps.setObject(index++,recruitment_job_descriptionDTO.lastApplicationDate);
		ps.setObject(index++,recruitment_job_descriptionDTO.lastAgeCalculationDate);
		ps.setObject(index++,recruitment_job_descriptionDTO.onlineJobPostingDate);
		ps.setObject(index++,recruitment_job_descriptionDTO.admitCardPrintStartDate);
		ps.setObject(index++,recruitment_job_descriptionDTO.admitCardPrintEndDate);
		ps.setObject(index++,recruitment_job_descriptionDTO.filesDropzone);
		ps.setObject(index++,recruitment_job_descriptionDTO.jobCat);
		ps.setObject(index++,recruitment_job_descriptionDTO.insertedByUserId);
		ps.setObject(index++,recruitment_job_descriptionDTO.insertionDate);
		ps.setObject(index++,recruitment_job_descriptionDTO.modifiedBy);
		ps.setObject(index++,recruitment_job_descriptionDTO.status);
		ps.setObject(index++,recruitment_job_descriptionDTO.closureRemarks);
		ps.setObject(index++,recruitment_job_descriptionDTO.firstApplicationDate);
		ps.setObject(index++,recruitment_job_descriptionDTO.districts);
		ps.setObject(index++,recruitment_job_descriptionDTO.roll_created);
		ps.setObject(index++,recruitment_job_descriptionDTO.yearly_education_level_type);
		ps.setObject(index++,recruitment_job_descriptionDTO.max_age_for_grandchildren_ff);
		ps.setObject(index++,recruitment_job_descriptionDTO.min_age);
		ps.setObject(index++,recruitment_job_descriptionDTO.max_age_disable);
		ps.setObject(index++,recruitment_job_descriptionDTO.marks_committee_id);
		ps.setObject(index++, recruitment_job_descriptionDTO.searchColumn);
		if(isInsert)
		{
			ps.setObject(index++, 0);
			ps.setObject(index++, lastModificationTime);
		}
	}

	public Recruitment_job_descriptionDTO build(ResultSet rs)
	{
		try
		{
			Recruitment_job_descriptionDTO recruitment_job_descriptionDTO = new Recruitment_job_descriptionDTO();
			recruitment_job_descriptionDTO.iD = rs.getLong("ID");
			recruitment_job_descriptionDTO.recruitmentTestName = rs.getLong("recruitment_test_name");
			recruitment_job_descriptionDTO.jobTitleEn = rs.getString("job_title_en");
			recruitment_job_descriptionDTO.jobTitleBn = rs.getString("job_title_bn");
			recruitment_job_descriptionDTO.jobGradeCat = rs.getInt("job_grade_cat");
			recruitment_job_descriptionDTO.employmentStatusCat = rs.getInt("employment_status_cat");
			recruitment_job_descriptionDTO.employeePayScaleType = rs.getLong("employee_pay_scale_type");
			recruitment_job_descriptionDTO.jobPurpose = rs.getString("job_purpose");
			recruitment_job_descriptionDTO.jobResponsibilities = rs.getString("job_responsibilities");
			recruitment_job_descriptionDTO.minimumAcademicQualification = rs.getString("minimum_academic_qualification");
			recruitment_job_descriptionDTO.minimumExperienceRequired = rs.getString("minimum_experience_required");
			recruitment_job_descriptionDTO.requiredCertificationAndTraning = rs.getString("required_certification_and_traning");
			recruitment_job_descriptionDTO.personalCharacteristics = rs.getString("personal_characteristics");
			recruitment_job_descriptionDTO.requiredCompetence = rs.getString("required_competence");
			recruitment_job_descriptionDTO.educationLevelType = rs.getLong("education_level_type");
			recruitment_job_descriptionDTO.minExperience = rs.getDouble("min_experience");
			recruitment_job_descriptionDTO.maxAgeOnBoundaryDateRegular = rs.getInt("max_age_on_boundary_date_regular");
			recruitment_job_descriptionDTO.maxAgeOnBoundaryDateFf = rs.getInt("max_age_on_boundary_date_ff");
			recruitment_job_descriptionDTO.jobLocation = rs.getString("job_location");
			recruitment_job_descriptionDTO.numberOfVacancy = rs.getInt("number_of_vacancy");
			recruitment_job_descriptionDTO.lastApplicationDate = rs.getLong("last_application_date");
			recruitment_job_descriptionDTO.lastAgeCalculationDate = rs.getLong("last_age_calculation_date");
			recruitment_job_descriptionDTO.onlineJobPostingDate = rs.getLong("online_job_posting_date");
			recruitment_job_descriptionDTO.admitCardPrintStartDate = rs.getLong("admit_card_print_start_date");
			recruitment_job_descriptionDTO.admitCardPrintEndDate = rs.getLong("admit_card_print_end_date");
			recruitment_job_descriptionDTO.filesDropzone = rs.getLong("files_dropzone");
			recruitment_job_descriptionDTO.jobCat = rs.getInt("job_cat");
			recruitment_job_descriptionDTO.insertedByUserId = rs.getLong("inserted_by_user_id");
			recruitment_job_descriptionDTO.insertionDate = rs.getLong("insertion_date");
			recruitment_job_descriptionDTO.modifiedBy = rs.getString("modified_by");
			recruitment_job_descriptionDTO.status = rs.getString("status");
			recruitment_job_descriptionDTO.districts = rs.getString("districts");
			recruitment_job_descriptionDTO.closureRemarks = rs.getString("closure_remarks");
			recruitment_job_descriptionDTO.firstApplicationDate = rs.getLong("first_application_date");
			recruitment_job_descriptionDTO.isDeleted = rs.getInt("isDeleted");
			recruitment_job_descriptionDTO.lastModificationTime = rs.getLong("lastModificationTime");
			recruitment_job_descriptionDTO.marks_committee_id = rs.getLong("marks_committee_id");

			recruitment_job_descriptionDTO.roll_created = rs.getBoolean("roll_created");
			recruitment_job_descriptionDTO.yearly_education_level_type = rs.getLong("yearly_education_level_type");
			recruitment_job_descriptionDTO.max_age_for_grandchildren_ff = rs.getInt("max_age_for_grandchildren_ff");
			recruitment_job_descriptionDTO.min_age = rs.getInt("min_age");
			recruitment_job_descriptionDTO.max_age_disable = rs.getInt("max_age_disable");
			return recruitment_job_descriptionDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}

	public Recruitment_job_descriptionDTO buildForCount(ResultSet rs)
	{
		try
		{
			Recruitment_job_descriptionDTO recruitment_job_descriptionDTO = new Recruitment_job_descriptionDTO();
			recruitment_job_descriptionDTO.count = rs.getInt("count");
			return recruitment_job_descriptionDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
	
	
	

		
	

	//need another getter for repository
	public Recruitment_job_descriptionDTO getDTOByID (long ID) {
		String sql = "SELECT * FROM " + tableName + " WHERE ID = ? ";
		Recruitment_job_descriptionDTO recruitment_job_descriptionDTO =
				ConnectionAndStatementUtil.getT(sql, Arrays.asList(ID), this::build);

		try {
			RecruitmentJobSpecificFilesDAO recruitmentJobSpecificFilesDAO = new RecruitmentJobSpecificFilesDAO("recruitment_job_specific_files");
			List<RecruitmentJobSpecificFilesDTO> recruitmentJobSpecificFilesDTOList = recruitmentJobSpecificFilesDAO.getRecruitmentJobSpecificFilesDTOListByRecruitmentJobDescriptionID(recruitment_job_descriptionDTO.iD);
			recruitment_job_descriptionDTO.recruitmentJobSpecificFilesDTOList = recruitmentJobSpecificFilesDTOList;

			RecruitmentJobSpecificCertsDAO recruitmentJobSpecificCertsDAO = new RecruitmentJobSpecificCertsDAO("recruitment_job_specific_certs");
			List<RecruitmentJobSpecificCertsDTO> recruitmentJobSpecificCertsDTOList = recruitmentJobSpecificCertsDAO.getRecruitmentJobSpecificCertsDTOListByRecruitmentJobDescriptionID(recruitment_job_descriptionDTO.iD);
			recruitment_job_descriptionDTO.recruitmentJobSpecificCertsDTOList = recruitmentJobSpecificCertsDTOList;
		} catch (Exception e){
			e.printStackTrace();
		}


		return recruitment_job_descriptionDTO;
	}
	

	
	
	public List<Recruitment_job_descriptionDTO> getDTOs(Collection recordIDs){
		if(recordIDs.isEmpty()){
			return new ArrayList<>();
		}
		String sql = "SELECT * FROM " + tableName + " WHERE ID IN ( ";
		for(int i = 0;i<recordIDs.size();i++){
			if(i!=0){
				sql+=",";
			}
			sql+= " ? ";
		}
		sql+=")  order by lastModificationTime desc";
		printSql(sql);
		return ConnectionAndStatementUtil.getListOfT(sql, Arrays.asList(recordIDs.toArray()),this::build);
	
	}
	
	

	
	
	
	//add repository
	public List<Recruitment_job_descriptionDTO> getAllRecruitment_job_description (boolean isFirstReload)
    {

		String sql = "SELECT * FROM recruitment_job_description";
		sql += " WHERE ";
	

		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by recruitment_job_description.lastModificationTime desc";
		printSql(sql);
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);

    }

	public List<Recruitment_job_descriptionDTO> getAllRecruitment_job_descriptionByStatus (String status)
	{

		String sql = "SELECT * FROM recruitment_job_description";
		sql += " WHERE ";
		sql+=" status like  ? ";
		sql+=" and isDeleted =  0";

		sql += " order by recruitment_job_description.lastModificationTime desc";
		printSql(sql);

		return ConnectionAndStatementUtil.getListOfT(sql, Arrays.asList(status), this::build);
	}

	public List<Recruitment_job_descriptionDTO> getByFilter (String filter)
	{

		String sql = "SELECT * FROM recruitment_job_description";
		sql += " WHERE " + filter;

		sql += " order by recruitment_job_description.lastModificationTime desc";
		printSql(sql);
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);
	}


	public List<Recruitment_job_descriptionDTO> getAllRecruitment_job_descriptionForCreatingAdmitCard ()
	{

		long currentTime = System.currentTimeMillis();
		return Recruitment_job_descriptionRepository.getInstance().getRecruitment_job_descriptionList()
				.stream()
				.filter(i -> i.lastApplicationDate < currentTime)
				.sorted(Comparator.comparingLong(i -> i.lastModificationTime)) // sort in descending order of economic year
				.collect(Collectors.toList());



//		String sql = "SELECT * FROM recruitment_job_description";
//		sql += " WHERE isDeleted =  0";
//		sql+=" and last_application_date < " + currentTime;
//
//		sql += " order by recruitment_job_description.lastModificationTime desc";
//		printSql(sql);
//
//		return ConnectionAndStatementUtil.getListOfT(sql,this::build);
	}


	public String getSqlWithSearchCriteriaForNormalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
			UserDTO userDTO, String filter, boolean tableHasJobCat, List<Object> objectList)
    {
		boolean viewAll = false;
		
		if(p_searchCriteria.get("ViewAll") != null)
		{
			System.out.println("ViewAll = " + p_searchCriteria.get("ViewAll"));
			viewAll = true;
		}
		else
		{
			System.out.println("ViewAll is null ");
		}
		
		String sql = "SELECT ";
		if(category == GETIDS)
		{
			sql += " " + tableName + ".ID as ID ";
		}
		else if(category == GETCOUNT)
		{
			sql += " count( " + tableName + ".ID) as countID ";
		}
		else if(category == GETDTOS)
		{
			sql += " distinct " + tableName + ".* ";
		}
		sql += "FROM " + tableName + " ";
		sql += joinSQL;
		

		
		String AnyfieldSql = "";
		String AllFieldSql = "";
		
		if(p_searchCriteria != null)
		{
						
			
			Enumeration names = p_searchCriteria.keys();
			String str, value;
			
			AnyfieldSql = "(";
			
			if(p_searchCriteria.get("AnyField")!= null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase(""))
			{
				AnyfieldSql+= tableName + ".search_column like ? ";
				objectList.add("%" + p_searchCriteria.get("AnyField").toString() + "%");
			}
			AnyfieldSql += ")";
			System.out.println("AnyfieldSql = " + AnyfieldSql);
			
			AllFieldSql = "(";
			int i = 0;
			while(names.hasMoreElements())
			{				
				str = (String) names.nextElement();
				value = (String)p_searchCriteria.get(str);
		        System.out.println(str + ": " + value);
				if(value != null && !value.equalsIgnoreCase("") && (
						 str.equals("job_title_en")
						|| str.equals("job_title_bn")
						|| str.equals("job_grade_cat")
						|| str.equals("employment_status_cat")
//						|| str.equals("employee_pay_scale_type")
//						|| str.equals("job_purpose")
//						|| str.equals("job_responsibilities")
//						|| str.equals("minimum_academic_qualification")
//						|| str.equals("minimum_experience_required")
//						|| str.equals("required_certification_and_traning")
//						|| str.equals("personal_characteristics")
//						|| str.equals("required_competence")
//						|| str.equals("education_level_type")
//						|| str.equals("min_experience")
//						|| str.equals("max_age_on_boundary_date_regular")
//						|| str.equals("max_age_on_boundary_date_ff")
//						|| str.equals("job_location")
//						|| str.equals("last_application_date_start")
//						|| str.equals("last_application_date_end")
//						|| str.equals("last_age_calculation_date_start")
//						|| str.equals("last_age_calculation_date_end")
//						|| str.equals("online_job_posting_date_start")
//						|| str.equals("online_job_posting_date_end")
//						|| str.equals("insertion_date_start")
//						|| str.equals("insertion_date_end")
				)
						
				)
				{
					if(p_searchCriteria.get(str).equals("any"))
					{
						continue;
					}

					if( i > 0)
					{
						AllFieldSql+= " AND  ";
					}
					
					 if(str.equals("job_title_en"))
					{
						AllFieldSql += "" + tableName + ".job_title_en like ? ";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("job_title_bn"))
					{
						AllFieldSql += "" + tableName + ".job_title_bn like ? ";
						objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("job_grade_cat"))
					{
						AllFieldSql += "" + tableName + ".job_grade_cat = ? " ;
						objectList.add(p_searchCriteria.get(str));
						i ++;
					}
					else if(str.equals("employment_status_cat"))
					{
						AllFieldSql += "" + tableName + ".employment_status_cat = ? " ;
						objectList.add(p_searchCriteria.get(str));
						i ++;
					}
//					else if(str.equals("employee_pay_scale_type"))
//					{
//						AllFieldSql += "" + tableName + ".employee_pay_scale_type = " + p_searchCriteria.get(str);
//						i ++;
//					}
//					else if(str.equals("job_purpose"))
//					{
//						AllFieldSql += "" + tableName + ".job_purpose like '%" + p_searchCriteria.get(str) + "%'";
//						i ++;
//					}
//					else if(str.equals("job_responsibilities"))
//					{
//						AllFieldSql += "" + tableName + ".job_responsibilities like '%" + p_searchCriteria.get(str) + "%'";
//						i ++;
//					}
//					else if(str.equals("minimum_academic_qualification"))
//					{
//						AllFieldSql += "" + tableName + ".minimum_academic_qualification like '%" + p_searchCriteria.get(str) + "%'";
//						i ++;
//					}
//					else if(str.equals("minimum_experience_required"))
//					{
//						AllFieldSql += "" + tableName + ".minimum_experience_required like '%" + p_searchCriteria.get(str) + "%'";
//						i ++;
//					}
//					else if(str.equals("required_certification_and_traning"))
//					{
//						AllFieldSql += "" + tableName + ".required_certification_and_traning like '%" + p_searchCriteria.get(str) + "%'";
//						i ++;
//					}
//					else if(str.equals("personal_characteristics"))
//					{
//						AllFieldSql += "" + tableName + ".personal_characteristics like '%" + p_searchCriteria.get(str) + "%'";
//						i ++;
//					}
//					else if(str.equals("required_competence"))
//					{
//						AllFieldSql += "" + tableName + ".required_competence like '%" + p_searchCriteria.get(str) + "%'";
//						i ++;
//					}
//					else if(str.equals("education_level_type"))
//					{
//						AllFieldSql += "" + tableName + ".education_level_type = " + p_searchCriteria.get(str);
//						i ++;
//					}
//					else if(str.equals("min_experience"))
//					{
//						AllFieldSql += "" + tableName + ".min_experience like '%" + p_searchCriteria.get(str) + "%'";
//						i ++;
//					}
//					else if(str.equals("max_age_on_boundary_date_regular"))
//					{
//						AllFieldSql += "" + tableName + ".max_age_on_boundary_date_regular like '%" + p_searchCriteria.get(str) + "%'";
//						i ++;
//					}
//					else if(str.equals("max_age_on_boundary_date_ff"))
//					{
//						AllFieldSql += "" + tableName + ".max_age_on_boundary_date_ff like '%" + p_searchCriteria.get(str) + "%'";
//						i ++;
//					}
//					else if(str.equals("job_location"))
//					{
//						AllFieldSql += "" + tableName + ".job_location like '%" + p_searchCriteria.get(str) + "%'";
//						i ++;
//					}
//					else if(str.equals("last_application_date_start"))
//					{
//						AllFieldSql += "" + tableName + ".last_application_date >= " + p_searchCriteria.get(str);
//						i ++;
//					}
//					else if(str.equals("last_application_date_end"))
//					{
//						AllFieldSql += "" + tableName + ".last_application_date <= " + p_searchCriteria.get(str);
//						i ++;
//					}
//					else if(str.equals("last_age_calculation_date_start"))
//					{
//						AllFieldSql += "" + tableName + ".last_age_calculation_date >= " + p_searchCriteria.get(str);
//						i ++;
//					}
//					else if(str.equals("last_age_calculation_date_end"))
//					{
//						AllFieldSql += "" + tableName + ".last_age_calculation_date <= " + p_searchCriteria.get(str);
//						i ++;
//					}
//					else if(str.equals("online_job_posting_date_start"))
//					{
//						AllFieldSql += "" + tableName + ".online_job_posting_date >= " + p_searchCriteria.get(str);
//						i ++;
//					}
//					else if(str.equals("online_job_posting_date_end"))
//					{
//						AllFieldSql += "" + tableName + ".online_job_posting_date <= " + p_searchCriteria.get(str);
//						i ++;
//					}
//					else if(str.equals("insertion_date_start"))
//					{
//						AllFieldSql += "" + tableName + ".insertion_date >= " + p_searchCriteria.get(str);
//						i ++;
//					}
//					else if(str.equals("insertion_date_end"))
//					{
//						AllFieldSql += "" + tableName + ".insertion_date <= " + p_searchCriteria.get(str);
//						i ++;
//					}
					
					
				}
			}
			
			AllFieldSql += ")";
			System.out.println("AllFieldSql = " + AllFieldSql);
			
			
		}
		
		
		sql += " WHERE ";
		
		sql += " (" + tableName + ".isDeleted = 0 ";			
		sql += ")";
		
		
		if(!filter.equalsIgnoreCase(""))
		{
			sql += " and " + filter + " ";
		}
		
		if(!AnyfieldSql.equals("()") && !AnyfieldSql.equals(""))
		{
			sql += " AND " + AnyfieldSql;
			
		}
		if(!AllFieldSql.equals("()") && !AllFieldSql.equals(""))
		{			
			sql += " AND " + AllFieldSql;
		}
		
	
			
		sql += " order by " + tableName + ".lastModificationTime desc ";

		printSql(sql);
		
		if(limit >= 0)
		{
			sql += " limit " + limit;
		}
		if(offset >= 0)
		{
			sql += " offset " + offset;
		}
		
		System.out.println("-------------- sql = " + sql);
		
		return sql;
    }


    public String getJobList(String status, String Language){
		String sOptions = "";
		if (Language.equals("English")) {
			sOptions = "<option value = ''>Select</option>";
		} else {
			sOptions = "<option value = ''>অনুগ্রহ করে নির্বাচন করুন</option>";
		}

		List<Recruitment_job_descriptionDTO> dtos = Recruitment_job_descriptionRepository.getInstance().
				getRecruitment_job_descriptionList();

		if(!status.equalsIgnoreCase("ALL")){
			dtos = dtos.stream().
					filter(i -> i.status.equalsIgnoreCase(status)).collect(Collectors.toList());
		}

		for(Recruitment_job_descriptionDTO dto: dtos){
			if (Language.equals("English")) {
				sOptions += "<option value = '" + dto.iD + "'>" + dto.jobTitleEn + "</option>";
			} else {
				sOptions += "<option value = '" + dto.iD + "'>" + dto.jobTitleBn + "</option>";
			}
		}

		return sOptions;
	}

	public String getJobListWithoutInternship(String status, String Language){
		String sOptions = "";
		if (Language.equals("English")) {
			sOptions = "<option value = ''>Select</option>";
		} else {
			sOptions = "<option value = ''>অনুগ্রহ করে নির্বাচন করুন</option>";
		}



		List<Recruitment_job_descriptionDTO> dtos = Recruitment_job_descriptionRepository.getInstance().
				getRecruitment_job_descriptionList();

		if(!status.equalsIgnoreCase("ALL")){
			dtos = dtos.stream().
					filter(i -> i.status.equalsIgnoreCase(status)).collect(Collectors.toList());
		}

		long internShipId = Long.parseLong(GlobalConfigurationRepository.
				getGlobalConfigDTOByID(GlobalConfigConstants.INTERNSHIP_ID).value);
		dtos = dtos.stream().filter(i -> i.iD != internShipId).collect(Collectors.toList());

		for(Recruitment_job_descriptionDTO dto: dtos){
			if (Language.equals("English")) {
				sOptions += "<option value = '" + dto.iD + "'>" + dto.jobTitleEn + "</option>";
			} else {
				sOptions += "<option value = '" + dto.iD + "'>" + dto.jobTitleBn + "</option>";
			}
		}

		return sOptions;
	}

	public String getJobListByTestNameAndWithoutInternship(Long testNameId, String status, String Language){
		String sOptions = "";
		if (Language.equals("English")) {
			sOptions = "<option value = ''>Select</option>";
		} else {
			sOptions = "<option value = ''>অনুগ্রহ করে নির্বাচন করুন</option>";
		}



		List<Recruitment_job_descriptionDTO> dtos = Recruitment_job_descriptionRepository.getInstance().
				getRecruitment_job_descriptionList();

		if(!status.equalsIgnoreCase("ALL")){
			dtos = dtos.stream().
					filter(i -> i.status.equalsIgnoreCase(status)).collect(Collectors.toList());
		}

		long internShipId = Long.parseLong(GlobalConfigurationRepository.
				getGlobalConfigDTOByID(GlobalConfigConstants.INTERNSHIP_ID).value);
		dtos = dtos.stream().filter(i -> i.iD != internShipId && i.recruitmentTestName == testNameId).collect(Collectors.toList());

		for(Recruitment_job_descriptionDTO dto: dtos){
			if (Language.equals("English")) {
				sOptions += "<option value = '" + dto.iD + "'>" + dto.jobTitleEn + "</option>";
			} else {
				sOptions += "<option value = '" + dto.iD + "'>" + dto.jobTitleBn + "</option>";
			}
		}

		return sOptions;
	}


	public String getStatusList(String Language){

		String selected = "";
		String sOptions = "";
		if (Language.equals("English")) {
			sOptions = "<option value = '' "+selected+">Select</option>";
			sOptions += "<option value = 'ALL' "+selected+">All</option>";
		} else {
			sOptions = "<option value = '' "+selected+">বাছাই করুন</option>";
			sOptions += "<option value = 'ALL' "+selected+">সকল</option>";
		}

		for(int i = 0; i < statusList.size(); i++){

			//selected = statusList.get(i).equalsIgnoreCase("ongoing")?"selected":"";
			selected = "";


			if (Language.equals("English")) {
				sOptions += "<option value = '" + statusList.get(i) + "' "+selected+" >" + statusList.get(i) + "</option>";
			} else {
				sOptions += "<option value = '" + statusList.get(i) + "' "+ selected +">" + statusMap.get(statusList.get(i)) + "</option>";
			}
		}

		return sOptions;
	}


	public String getJobList( String Language){
		String sOptions = "";
		List<Recruitment_job_descriptionDTO> dtos =
				Recruitment_job_descriptionRepository.getInstance().
						getRecruitment_job_descriptionList();

		for(Recruitment_job_descriptionDTO dto: dtos){
			if (Language.equals("English")) {
				sOptions += "<option value = '" + dto.iD + "'>" + dto.jobTitleEn + "</option>";
			} else {
				sOptions += "<option value = '" + dto.iD + "'>" + dto.jobTitleBn + "</option>";
			}
		}

		return sOptions;
	}



	public String getJobName(List<Recruitment_job_descriptionDTO> recruitmentJobDescriptionDTOS,Long ID,String Language){
		 String jobName="No job found";
		 //return  jobName;
		 String languagePrefix = Language.equalsIgnoreCase("english")?"_en":"_bn";
		 for(Recruitment_job_descriptionDTO dto:recruitmentJobDescriptionDTOS ){
		 	if(dto.iD==ID){
		 		jobName=Language.equalsIgnoreCase("english")?dto.jobTitleEn:dto.jobTitleBn;
		 		//jobName=CommonDAO.getName(dto.iD,tableName,"applicant"+languagePrefix,"id");
		 		break;
			}
		 }
		 return  jobName;
	}
	public String getJobListForSummary(String status, String Language){
		String sOptions = "";
		if (Language.equals("English")) {
			sOptions = "<option value = '-1'>All</option>";
		} else {
			sOptions = "<option value = '-1'>সকল</option>";
		}

		List<Recruitment_job_descriptionDTO> dtos = Recruitment_job_descriptionRepository.getInstance().
				getRecruitment_job_descriptionList();

		if(!status.equalsIgnoreCase("ALL")){
			dtos = dtos.stream().
					filter(i -> i.status.equalsIgnoreCase(status)).collect(Collectors.toList());
		}

		for(Recruitment_job_descriptionDTO dto: dtos){
			if (Language.equals("English")) {
				sOptions += "<option value = '" + dto.iD + "'>" + dto.jobTitleEn + "</option>";
			} else {
				sOptions += "<option value = '" + dto.iD + "'>" + dto.jobTitleBn + "</option>";
			}
		}

		return sOptions;
	}

	//public


	public long getRunningJobCount ()
	{
//		long count = 0;
		long currentDate = System.currentTimeMillis();

		return Recruitment_job_descriptionRepository.getInstance().getRecruitment_job_descriptionList()
				.stream()
				.filter(i -> currentDate > i.firstApplicationDate && currentDate < i.lastApplicationDate)
				.count();

//		String filter =  currentDate + " > first_application_date ";
//		filter +=  " AND " + currentDate + " < last_application_date ";
//
//		String sql = "SELECT count(*) as count FROM recruitment_job_description";
//		sql += " WHERE isDeleted =  0 and " + filter;
//
//		sql += " order by recruitment_job_description.lastModificationTime desc";
//		printSql(sql);
//
//		Recruitment_job_descriptionDTO dto = ConnectionAndStatementUtil.
//				getT(sql ,this::buildForCount);
//		if(dto != null){
//			count = dto.count;
//		}
//		return count;
	}

	public String getBuildOptions(String language,String selectedValues){
		List<Recruitment_job_descriptionDTO> dtoList = Recruitment_job_descriptionRepository.getInstance().
				getRecruitment_job_descriptionList();
		Set<Long> selectedValue = null;
		if(selectedValues!=null && selectedValues.trim().length()>0){
			String [] tokens = selectedValues.trim().split(",");
			if(tokens.length>0){
				selectedValue = Stream.of(tokens)
						.map(String::trim)
						.map(Long::parseLong)
						.collect(Collectors.toSet());
			}
		}
		Set<Long> finalValue = selectedValue == null ? new HashSet<>():selectedValue;
		List<OptionDTO> optionDTOList = dtoList.stream()
				.filter(e->finalValue.contains(e.iD) || e.marks_committee_id == 0)
				.map(e->new OptionDTO(e.jobTitleEn,e.jobTitleBn,String.valueOf(e.iD)))
				.collect(Collectors.toList());
		return Utils.buildOptionsMultipleSelection(optionDTOList,language,selectedValues);
	}




				
}
	