CREATE TABLE food_bill_type_config
(
    ID                   BIGINT PRIMARY KEY,

    budget_office_id     BIGINT,
    office_unit_id_type  INT,
    office_unit_id       BIGINT,
    task_type_id         BIGINT,

    modified_by          BIGINT,
    lastModificationTime BIGINT DEFAULT -62135791200000,
    inserted_by          BIGINT,
    insertion_time       BIGINT DEFAULT -62135791200000,
    isDeleted            INT    DEFAULT 0
) ENGINE = MyISAM
  DEFAULT CHARSET = utf8
  COLLATE = utf8_unicode_ci;

INSERT INTO vb_sequencer (table_name, next_id, table_LastModificationTime)
VALUES ('food_bill_type_config', 1, 0);


INSERT INTO food_bill_type_config (ID, budget_office_id, office_unit_id_type, office_unit_id, task_type_id,
                                   modified_by, lastModificationTime, inserted_by, insertion_time, isDeleted)
VALUES -- Parliament Secretariat
       (1, 1, 1, -1, 6, -1, 0, -1, 0, 0),
       (2, 1, 2, 1103, 6, -1, 0, -1, 0, 0), -- audit unit
       -- Medical
       (3, 2, 1, -1, 8, -1, 0, -1, 0, 0),
       -- Speaker & Deputy Speaker
       (4, 3, 1, -1, 12, -1, 0, -1, 0, 0),
       -- Leader & Deputy Leader
       (5, 4, 1, -1, 9, -1, 0, -1, 0, 0),
       -- Leader of the Opposition
       (6, 5, 1, -1, 9, -1, 0, -1, 0, 0),
       -- Standing Committee
       (7, 6, 1, -1, 10, -1, 0, -1, 0, 0),
       -- Chief Whip & Whip
       (8, 7, 1, -1, 13, -1, 0, -1, 0, 0),
       -- Member of the Parliament
       (9, 8, 1, -1, 11, -1, 0, -1, 0, 0);