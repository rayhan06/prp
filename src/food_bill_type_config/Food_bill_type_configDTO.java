package food_bill_type_config;

import sessionmanager.SessionConstants;
import util.CommonDTO;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

public class Food_bill_type_configDTO extends CommonDTO {
    public long budgetOfficeId;
    public OfficeUnitIdType officeUnitIdType;
    public long officeUnitId;
    public long taskTypeId;

    public long modifiedBy = -1;
    public long insertedBy = -1;
    public long insertionTime = SessionConstants.MIN_DATE;

    public boolean matchesFixedOfficeId(long officeUnitId) {
        return this.officeUnitIdType == OfficeUnitIdType.FIXED
               && this.officeUnitId == officeUnitId;
    }

    public boolean matchesAnyOfficeId() {
        return officeUnitIdType == OfficeUnitIdType.ANY;
    }

    public enum OfficeUnitIdType {
        ANY(1),
        FIXED(2);

        private final int value;

        private static final Map<Integer, OfficeUnitIdType> mapByValue;

        static {
            mapByValue = Arrays.stream(values()).collect(Collectors.toMap(e -> e.value, e -> e));
        }

        public static OfficeUnitIdType getByValue(int value) {
            return mapByValue.getOrDefault(value, null);
        }

        OfficeUnitIdType(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }
}

