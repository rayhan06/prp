package food_bill_type_config;

import common.CommonDAOService;
import org.apache.log4j.Logger;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

@SuppressWarnings({"Duplicates"})
public class Food_bill_type_configDAO implements CommonDAOService<Food_bill_type_configDTO> {
    private static final Logger logger = Logger.getLogger(Food_bill_type_configDAO.class);

    private static final String addSqlQuery =
            "INSERT INTO {tableName} (budget_office_id,office_unit_id_type,office_unit_id,task_type_id,"
                    .concat("modified_by,lastModificationTime,inserted_by,insertion_time,isDeleted,ID) ")
                    .concat("VALUES (?,?,?,?,?,?,?,?,?,?)");

    private static final String updateSqlQuery =
            "UPDATE {tableName} SET budget_office_id=?,office_unit_id_type=?,office_unit_id=?,task_type_id=?,"
                    .concat("modified_by=?,lastModificationTime=? WHERE ID=?");

    private static final Map<String, String> searchMap = new HashMap<>();

    private Food_bill_type_configDAO() {

    }

    private static class LazyLoader {
        static final Food_bill_type_configDAO INSTANCE = new Food_bill_type_configDAO();
    }

    public static Food_bill_type_configDAO getInstance() {
        return LazyLoader.INSTANCE;
    }

    @Override
    public String getTableName() {
        return "food_bill_type_config";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    @Override
    public void set(PreparedStatement ps, Food_bill_type_configDTO dto, boolean isInsert) throws SQLException {
        int index = 0;
        ps.setLong(++index, dto.budgetOfficeId);
        ps.setInt(++index, dto.officeUnitIdType.getValue());
        ps.setLong(++index, dto.officeUnitId);
        ps.setLong(++index, dto.taskTypeId);
        ps.setLong(++index, dto.modifiedBy);
        ps.setLong(++index, dto.lastModificationTime);
        if (isInsert) {
            ps.setLong(++index, dto.insertedBy);
            ps.setLong(++index, dto.insertionTime);
            ps.setInt(++index, 0 /* isDeleted */);
        }
        ps.setLong(++index, dto.iD);
    }

    @Override
    public Food_bill_type_configDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Food_bill_type_configDTO dto = new Food_bill_type_configDTO();
            dto.iD = rs.getLong("ID");
            dto.budgetOfficeId = rs.getLong("budget_office_id");
            dto.officeUnitIdType = Food_bill_type_configDTO.OfficeUnitIdType.getByValue(rs.getInt("office_unit_id_type"));
            dto.officeUnitId = rs.getLong("office_unit_id");
            dto.taskTypeId = rs.getLong("task_type_id");
            dto.modifiedBy = rs.getLong("modified_by");
            dto.lastModificationTime = rs.getLong("lastModificationTime");
            dto.insertedBy = rs.getLong("inserted_by");
            dto.insertionTime = rs.getLong("insertion_time");
            dto.isDeleted = rs.getInt("isDeleted");
            return dto;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    @Override
    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Food_bill_type_configDTO) commonDTO, addSqlQuery, true);
    }

    @Override
    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Food_bill_type_configDTO) commonDTO, updateSqlQuery, false);
    }
}
