package food_bill_type_config;

import budget_mapping.Budget_mappingRepository;
import economic_sub_code.Economic_sub_codeDTO;
import economic_sub_code.Economic_sub_codeRepository;
import org.apache.log4j.Logger;
import repository.Repository;
import repository.RepositoryManager;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Food_bill_type_configRepository implements Repository {
    private static final Logger logger = Logger.getLogger(Food_bill_type_configRepository.class);
    public static final long FOOD_BILL_BUDGET_MAPPING_ID = 1L;
    public static final long FOOD_BILL_SUB_CODE_ID = 303L;
    private final Map<Long, Food_bill_type_configDTO> mapById;

    private Food_bill_type_configRepository() {
        mapById = new ConcurrentHashMap<>();
        RepositoryManager.getInstance().addRepository(this);
    }

    private static class LazyLoader {
        final static Food_bill_type_configRepository INSTANCE = new Food_bill_type_configRepository();
    }

    public static Food_bill_type_configRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

    @Override
    public synchronized void reload(boolean reloadAll) {
        logger.debug("Food_bill_type_configRepository loading start for reloadAll: " + reloadAll);
        List<Food_bill_type_configDTO> dtoList = Food_bill_type_configDAO.getInstance().getAllDTOs(reloadAll);
        if (dtoList != null && dtoList.size() > 0) {
            dtoList.stream()
                   .peek(this::removeIfPresent)
                   .filter(dto -> dto.isDeleted == 0)
                   .forEach(dto -> mapById.put(dto.iD, dto));
        }
        logger.debug("Food_bill_type_configRepository loading end for reloadAll: " + reloadAll);
    }

    private void removeIfPresent(Food_bill_type_configDTO dto) {
        if (mapById.get(dto.iD) != null) {
            mapById.remove(dto.iD);
        }
    }

    public Food_bill_type_configDTO getById(long id) {
        return mapById.getOrDefault(id, null);
    }

    private List<Food_bill_type_configDTO> findListByBudgetOffice(long budgetOfficeId) {
        return mapById.values()
                      .stream()
                      .filter(dto -> dto.budgetOfficeId == budgetOfficeId)
                      .collect(Collectors.toList());
    }

    public Food_bill_type_configDTO findByBudgetOfficeOfficeUnit(long budgetOfficeId, long officeUnitId) {
        Food_bill_type_configDTO selectedConfigDTO = null;
        for (Food_bill_type_configDTO configDTO : findListByBudgetOffice(budgetOfficeId)) {
            if (configDTO.matchesFixedOfficeId(officeUnitId)) {
                return configDTO;
            } else if (configDTO.matchesAnyOfficeId()) {
                selectedConfigDTO = configDTO;
            }
        }
        return selectedConfigDTO;
    }

    public String buildBudgetMappingDropDown(String language, Long selectedBudgetMappingId, boolean withCode) {
        return Budget_mappingRepository.getInstance().buildDropDown(
                language,
                selectedBudgetMappingId,
                withCode,
                budget_mappingDTO -> Objects.equals(budget_mappingDTO.iD, FOOD_BILL_BUDGET_MAPPING_ID)
        );
    }

    public String buildEconomicSubCodeDropDown(String language, Long selectedEconomicSubCodeId) {
        List<Economic_sub_codeDTO> subCodeDTOs =
                Stream.of(FOOD_BILL_SUB_CODE_ID)
                      .map(economicSubCodeId -> Economic_sub_codeRepository.getInstance().getDTOByID(economicSubCodeId))
                      .collect(Collectors.toList());
        return Economic_sub_codeRepository.getInstance().buildOptionFromDTOs(
                subCodeDTOs,
                language,
                selectedEconomicSubCodeId
        );
    }

    @Override
    public String getTableName() {
        return "food_bill_type_config";
    }
}
