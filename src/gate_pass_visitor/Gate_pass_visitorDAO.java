package gate_pass_visitor;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;
import java.sql.SQLException;
import org.apache.log4j.Logger;
import dbm.*;
import repository.RepositoryManager;
import util.*;

import user.UserDTO;

public class Gate_pass_visitorDAO  extends NavigationService4
{
	
	private final static Logger logger = Logger.getLogger(Gate_pass_visitorDAO.class);
	

	
	public Gate_pass_visitorDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";
		commonMaps = new Gate_pass_visitorMAPS(tableName);
		columnNames = new String[] 
		{
			"id",
			"name",
			"father_name",
			"mother_name",
			"permanent_address",
			"current_address",
			"credential_type",
			"credential_no",
			"mobile_number",
			"email",
			"gender_type",
			"officer_type",
			"search_column",
			"insertion_date",
			"office_name",
			"designation",
			
			"vehicle_number",
			"vehicle_description",
			"has_vehicle",
			
			"inserted_by",
			"modified_by",
			"isDeleted",
			"lastModificationTime"
		};
	}
	
	public Gate_pass_visitorDAO()
	{
		this("gate_pass_visitor");		
	}
	
	public void setSearchColumn(Gate_pass_visitorDTO gate_pass_visitorDTO)
	{
		gate_pass_visitorDTO.search_column = "";
		gate_pass_visitorDTO.search_column += gate_pass_visitorDTO.name + " ";
		gate_pass_visitorDTO.search_column += gate_pass_visitorDTO.credentialNo + " ";
		gate_pass_visitorDTO.search_column += gate_pass_visitorDTO.mobileNumber + " ";
		gate_pass_visitorDTO.search_column += gate_pass_visitorDTO.email + " " +  gate_pass_visitorDTO.affiliatedInfo;
		
	}
	
	public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException
	{
		Gate_pass_visitorDTO gate_pass_visitorDTO = (Gate_pass_visitorDTO)commonDTO;
		int index = 1;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(gate_pass_visitorDTO);
		if(isInsert)
		{
			ps.setObject(index++,gate_pass_visitorDTO.iD);
		}
		ps.setObject(index++,gate_pass_visitorDTO.name);
		ps.setObject(index++,gate_pass_visitorDTO.fatherName);
		ps.setObject(index++,gate_pass_visitorDTO.motherName);
		ps.setObject(index++,gate_pass_visitorDTO.permanentAddress);
		ps.setObject(index++,gate_pass_visitorDTO.currentAddress);
		ps.setObject(index++,gate_pass_visitorDTO.credentialType);
		ps.setObject(index++,gate_pass_visitorDTO.credentialNo);
		ps.setObject(index++,gate_pass_visitorDTO.mobileNumber);
		ps.setObject(index++,gate_pass_visitorDTO.email);
		ps.setObject(index++,gate_pass_visitorDTO.genderType);
		ps.setObject(index++,gate_pass_visitorDTO.officerType);
		ps.setObject(index++,gate_pass_visitorDTO.search_column);
		ps.setObject(index++,gate_pass_visitorDTO.insertionDate);
		ps.setObject(index++,gate_pass_visitorDTO.officeName);
		ps.setObject(index++,gate_pass_visitorDTO.designation);
		
		ps.setObject(index++,gate_pass_visitorDTO.vehicleNumber);
		ps.setObject(index++,gate_pass_visitorDTO.vehicleDescription);
		ps.setObject(index++,gate_pass_visitorDTO.hasVehicle);
		
		ps.setObject(index++,gate_pass_visitorDTO.insertedBy);
		ps.setObject(index++,gate_pass_visitorDTO.modifiedBy);
		if(isInsert)
		{
			ps.setObject(index++, 0);
			ps.setObject(index++, lastModificationTime);
		}
	}
	
	public void get(Gate_pass_visitorDTO gate_pass_visitorDTO, ResultSet rs) throws SQLException
	{
		gate_pass_visitorDTO.iD = rs.getLong("id");
		gate_pass_visitorDTO.id = rs.getLong("id");
		gate_pass_visitorDTO.name = rs.getString("name");
		gate_pass_visitorDTO.fatherName = rs.getString("father_name");
		gate_pass_visitorDTO.motherName = rs.getString("mother_name");
		gate_pass_visitorDTO.permanentAddress = rs.getString("permanent_address");
		gate_pass_visitorDTO.currentAddress = rs.getString("current_address");
		gate_pass_visitorDTO.credentialType = rs.getInt("credential_type");
		gate_pass_visitorDTO.credentialNo = rs.getString("credential_no");
		gate_pass_visitorDTO.mobileNumber = rs.getString("mobile_number");
		gate_pass_visitorDTO.email = rs.getString("email");
		gate_pass_visitorDTO.genderType = rs.getInt("gender_type");
		gate_pass_visitorDTO.officerType = rs.getInt("officer_type");
		gate_pass_visitorDTO.search_column = rs.getString("search_column");
		gate_pass_visitorDTO.insertionDate = rs.getLong("insertion_date");
		gate_pass_visitorDTO.officeName = rs.getString("office_name");
		gate_pass_visitorDTO.designation = rs.getString("designation");
		
		gate_pass_visitorDTO.vehicleNumber = rs.getString("vehicle_number");
		gate_pass_visitorDTO.vehicleDescription = rs.getString("vehicle_description");
		gate_pass_visitorDTO.hasVehicle = rs.getBoolean("has_vehicle");
		
		gate_pass_visitorDTO.insertedBy = rs.getString("inserted_by");
		gate_pass_visitorDTO.modifiedBy = rs.getString("modified_by");
		gate_pass_visitorDTO.isDeleted = rs.getInt("isDeleted");
		gate_pass_visitorDTO.lastModificationTime = rs.getLong("lastModificationTime");
	}
	


	//need another getter for repository
	public Gate_pass_visitorDTO getDTOByID (long ID)
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Gate_pass_visitorDTO gate_pass_visitorDTO = null;
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
			
            sql += " WHERE id=" + ID;
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			if(rs.next()){
				gate_pass_visitorDTO = new Gate_pass_visitorDTO();

				get(gate_pass_visitorDTO, rs);

			}			
			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return gate_pass_visitorDTO;
	}
	
	
	
	
	public List<Gate_pass_visitorDTO> getDTOs(Collection recordIDs){
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Gate_pass_visitorDTO gate_pass_visitorDTO = null;
		List<Gate_pass_visitorDTO> gate_pass_visitorDTOList = new ArrayList<>();
		if(recordIDs.isEmpty()){
			return gate_pass_visitorDTOList;
		}
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
            
            sql += " WHERE id IN ( ";

			for(int i = 0;i<recordIDs.size();i++){
				if(i!=0){
					sql+=",";
				}
				sql+=((ArrayList)recordIDs).get(i);
			}
			sql+=")  order by lastModificationTime desc";
			
			printSql(sql);
			
			logger.debug("sql " + sql);
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				gate_pass_visitorDTO = new Gate_pass_visitorDTO();
				get(gate_pass_visitorDTO, rs);
				System.out.println("got this DTO: " + gate_pass_visitorDTO);
				
				gate_pass_visitorDTOList.add(gate_pass_visitorDTO);

			}			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return gate_pass_visitorDTOList;
	
	}
	

	//add repository
	public List<Gate_pass_visitorDTO> getAllGate_pass_visitor (boolean isFirstReload)
    {
		List<Gate_pass_visitorDTO> gate_pass_visitorDTOList = new ArrayList<>();

		String sql = "SELECT * FROM gate_pass_visitor";
		sql += " WHERE ";
	

		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by gate_pass_visitor.lastModificationTime desc";
		printSql(sql);
		
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;

		
		try{
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();
			rs = stmt.executeQuery(sql);
			

			while(rs.next()){
				Gate_pass_visitorDTO gate_pass_visitorDTO = new Gate_pass_visitorDTO();
				get(gate_pass_visitorDTO, rs);
				
				gate_pass_visitorDTOList.add(gate_pass_visitorDTO);
			}			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}

		return gate_pass_visitorDTOList;
    }

	
	public List<Gate_pass_visitorDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO)
	{
		return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
	}

	
	public List<Gate_pass_visitorDTO> getDTOs(Hashtable  p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
			String filter, boolean tableHasJobCat)
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		List<Gate_pass_visitorDTO> gate_pass_visitorDTOList = new ArrayList<>();

		try{
			
			String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat);
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				Gate_pass_visitorDTO gate_pass_visitorDTO = new Gate_pass_visitorDTO();
				get(gate_pass_visitorDTO, rs);
				
				gate_pass_visitorDTOList.add(gate_pass_visitorDTO);
			}					
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return gate_pass_visitorDTOList;
	
	}
	public String getSqlWithSearchCriteriaForNormalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
			UserDTO userDTO, String filter, boolean tableHasJobCat)
    {
		boolean viewAll = false;
		
		if(p_searchCriteria != null && p_searchCriteria.get("ViewAll") != null)
		{
			System.out.println("ViewAll = " + p_searchCriteria.get("ViewAll"));
			viewAll = true;
		}
		else
		{
			System.out.println("ViewAll is null ");
		}
		
		String sql = "SELECT ";
		if(category == GETIDS)
		{
			sql += " " + tableName + ".ID as ID ";
		}
		else if(category == GETCOUNT)
		{
			sql += " count( " + tableName + ".ID) as countID ";
		}
		else if(category == GETDTOS)
		{
			sql += " distinct " + tableName + ".* ";
		}
		sql += "FROM " + tableName + " ";
		sql += joinSQL;
		

		
		String AnyfieldSql = "";
		String AllFieldSql = "";
		
		if(p_searchCriteria != null)
		{
						
			
			Enumeration names = p_searchCriteria.keys();
			String str, value;
			
			AnyfieldSql = "(";
			
			if(p_searchCriteria.get("AnyField")!= null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase(""))
			{
				AnyfieldSql+= tableName + ".search_column like '%" + p_searchCriteria.get("AnyField").toString() + "%'";			
			}
			AnyfieldSql += ")";
			System.out.println("AnyfieldSql = " + AnyfieldSql);
			
			AllFieldSql = "(";
			int i = 0;
			while(names.hasMoreElements())
			{				
				str = (String) names.nextElement();
				value = (String)p_searchCriteria.get(str);
		        System.out.println(str + ": " + value);
				if(value != null && !value.equalsIgnoreCase("") && (
						 str.equals("name")
						|| str.equals("credential_type")
						|| str.equals("credential_no")
						|| str.equals("mobile_number")
						|| str.equals("email")
						|| str.equals("gender_type")
						|| str.equals("officer_type")
						|| str.equals("insertion_date_start")
						|| str.equals("insertion_date_end")
				)
						
				)
				{
					if(p_searchCriteria.get(str).equals("any"))
					{
						continue;
					}

					if( i > 0)
					{
						AllFieldSql+= " AND  ";
					}
					
					 if(str.equals("name"))
					{
						AllFieldSql += "" + tableName + ".name like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("credential_type"))
					{
						AllFieldSql += "" + tableName + ".credential_type = " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("credential_no"))
					{
						AllFieldSql += "" + tableName + ".credential_no like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("mobile_number"))
					{
						AllFieldSql += "" + tableName + ".mobile_number like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("email"))
					{
						AllFieldSql += "" + tableName + ".email like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("gender_type"))
					{
						AllFieldSql += "" + tableName + ".gender_type = " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("officer_type"))
					{
						AllFieldSql += "" + tableName + ".officer_type = " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("insertion_date_start"))
					{
						AllFieldSql += "" + tableName + ".insertion_date >= " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("insertion_date_end"))
					{
						AllFieldSql += "" + tableName + ".insertion_date <= " + p_searchCriteria.get(str);
						i ++;
					}
					
					
				}
			}
			
			AllFieldSql += ")";
			System.out.println("AllFieldSql = " + AllFieldSql);
			
			
		}
		
		
		sql += " WHERE ";
		
		sql += " (" + tableName + ".isDeleted = 0 ";			
		sql += ")";
		
		
		if(!filter.equalsIgnoreCase(""))
		{
			sql += " and " + filter + " ";
		}
		
		if(!AnyfieldSql.equals("()") && !AnyfieldSql.equals(""))
		{
			sql += " AND " + AnyfieldSql;
			
		}
		if(!AllFieldSql.equals("()") && !AllFieldSql.equals(""))
		{			
			sql += " AND " + AllFieldSql;
		}
		
	
			
		sql += " order by " + tableName + ".lastModificationTime desc ";

		printSql(sql);
		
		if(limit >= 0)
		{
			sql += " limit " + limit;
		}
		if(offset >= 0)
		{
			sql += " offset " + offset;
		}
		
		System.out.println("-------------- sql = " + sql);
		
		return sql;
    }
	
	public String getSqlWithSearchCriteria(Hashtable p_searchCriteria, int limit, int offset, int category,
			boolean isPermanentTable, UserDTO userDTO, String filter, boolean tableHasJobCat)
    {		
		return getSqlWithSearchCriteriaForNormalSearch(p_searchCriteria, limit, offset, category, userDTO, filter, tableHasJobCat);	
    }



    public Gate_pass_visitorDTO getDTOByMobileNumber(String mobileNumber, String credentialNo)
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Gate_pass_visitorDTO gate_pass_visitorDTO = null;
		try{

			String sql = "SELECT * ";

			sql += " FROM " + tableName;

			sql += " WHERE mobile_number=" + mobileNumber;

			sql += " AND credential_no='" + credentialNo;

			sql += "' AND isDeleted=0";

			printSql(sql);

			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			if(rs.next()){
				gate_pass_visitorDTO = new Gate_pass_visitorDTO();

				get(gate_pass_visitorDTO, rs);

			}


		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}

			try{
				if (connection != null)
				{
					DBMR.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		return gate_pass_visitorDTO;
	}

	public void updateDTOById(Long id, Gate_pass_visitorDTO dto)
	{
		Connection connection = null;
		Statement stmt = null;

		try{

			String sql = "UPDATE " + tableName + " SET";

			sql += " name='" + dto.name;
			sql += "', father_name='" + dto.fatherName;
			sql += "', mother_name='" + dto.motherName;
			sql += "', permanent_address='" + dto.permanentAddress;
			sql += "', current_address='" + dto.currentAddress;
			sql += "', credential_type=" + dto.credentialType;
			sql += ", credential_no='" + dto.credentialNo;
			sql += "', mobile_number='" + dto.mobileNumber;
			sql += "', email='" + dto.email;
			sql += "', gender_type=" + dto.genderType;
			sql += ", officer_type=" + dto.officerType;
			sql += ", lastModificationTime=" + (new Date()).getTime();
			sql += ", search_column='" + dto.name + " " + dto.credentialNo + " " + dto.mobileNumber
					+ dto.affiliatedInfo;

			sql += "' WHERE id=" + id;

			sql += " AND isDeleted=0";

			printSql(sql);

			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			stmt.executeUpdate(sql);


		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}

			try{
				if (connection != null)
				{
					DBMR.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
	}

	public int countSameDayGatePass(String mobileNumber, String credentialNo, long visitingDate, long gatePassType, long gatePassSubType) {
		int count = 0;
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;

		try {

			String query = "select count(gp.id) as pass_count from gate_pass gp inner join gate_pass_visitor " +
					"gpv on gp.gate_pass_visitor_id = gpv.id where gpv.mobile_number="+mobileNumber+" and gpv.credential_no='" +
					credentialNo+"' and gp.visiting_date="+visitingDate+" and gp.gate_pass_type_id="+gatePassType +
					" and gp.gate_pass_sub_type_id="+gatePassSubType;

			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();
			logger.debug(query);

			rs = stmt.executeQuery(query);
			if (rs.next()) {
				count = rs.getInt("pass_count");
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e) {
			}

			try {
				if (connection != null) {
					DBMR.getInstance().freeConnection(connection);
				}
			} catch (Exception ex2) {
			}
		}

		return count;
	}
				
}
	