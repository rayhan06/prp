package gate_pass_visitor;
import java.util.*; 
import util.*; 


public class Gate_pass_visitorDTO extends CommonDTO
{

	public long id = 0;
    public String name = "";
    public String fatherName = "";
    public String motherName = "";
    public String permanentAddress = "";
    public String currentAddress = "";
	public int credentialType = 0;
    public String credentialNo = "";
    public String mobileNumber = "";
    public String email = "";
	public int genderType = 0;
	public int officerType = 0;
	public String search_column = "";
	public long insertionDate = 0;
    public String insertedBy = "";
    public String modifiedBy = "";
    public String officeName = "";
    public String designation = "";
    
    public String vehicleDescription = "";
    public String vehicleNumber = "";
    public boolean hasVehicle = false;
    
    public String affiliatedInfo = "";
	
	
    @Override
	public String toString() {
            return "$Gate_pass_visitorDTO[" +
            " id = " + id +
            " name = " + name +
            " fatherName = " + fatherName +
            " motherName = " + motherName +
            " permanentAddress = " + permanentAddress +
            " currentAddress = " + currentAddress +
            " credentialType = " + credentialType +
            " credentialNo = " + credentialNo +
            " mobileNumber = " + mobileNumber +
            " email = " + email +
            " genderType = " + genderType +
            " officerType = " + officerType +
            " search_column = " + search_column +
            " insertionDate = " + insertionDate +
            " insertedBy = " + insertedBy +
            " modifiedBy = " + modifiedBy +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}