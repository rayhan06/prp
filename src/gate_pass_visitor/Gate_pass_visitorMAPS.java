package gate_pass_visitor;
import java.util.*; 
import util.*;


public class Gate_pass_visitorMAPS extends CommonMaps
{	
	public Gate_pass_visitorMAPS(String tableName)
	{
		

		java_DTO_map.put("id".toLowerCase(), "id".toLowerCase());
		java_DTO_map.put("name".toLowerCase(), "name".toLowerCase());
		java_DTO_map.put("fatherName".toLowerCase(), "fatherName".toLowerCase());
		java_DTO_map.put("motherName".toLowerCase(), "motherName".toLowerCase());
		java_DTO_map.put("permanentAddress".toLowerCase(), "permanentAddress".toLowerCase());
		java_DTO_map.put("currentAddress".toLowerCase(), "currentAddress".toLowerCase());
		java_DTO_map.put("credentialType".toLowerCase(), "credentialType".toLowerCase());
		java_DTO_map.put("credentialNo".toLowerCase(), "credentialNo".toLowerCase());
		java_DTO_map.put("mobileNumber".toLowerCase(), "mobileNumber".toLowerCase());
		java_DTO_map.put("email".toLowerCase(), "email".toLowerCase());
		java_DTO_map.put("genderType".toLowerCase(), "genderType".toLowerCase());
		java_DTO_map.put("officerType".toLowerCase(), "officerType".toLowerCase());
		java_DTO_map.put("insertionDate".toLowerCase(), "insertionDate".toLowerCase());
		java_DTO_map.put("insertedBy".toLowerCase(), "insertedBy".toLowerCase());
		java_DTO_map.put("modifiedBy".toLowerCase(), "modifiedBy".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("name".toLowerCase(), "name".toLowerCase());
		java_SQL_map.put("credential_no".toLowerCase(), "credentialNo".toLowerCase());
		java_SQL_map.put("mobile_number".toLowerCase(), "mobileNumber".toLowerCase());
		java_SQL_map.put("email".toLowerCase(), "email".toLowerCase());

		java_Text_map.put("Id".toLowerCase(), "id".toLowerCase());
		java_Text_map.put("Name".toLowerCase(), "name".toLowerCase());
		java_Text_map.put("Father Name".toLowerCase(), "fatherName".toLowerCase());
		java_Text_map.put("Mother Name".toLowerCase(), "motherName".toLowerCase());
		java_Text_map.put("Permanent Address".toLowerCase(), "permanentAddress".toLowerCase());
		java_Text_map.put("Current Address".toLowerCase(), "currentAddress".toLowerCase());
		java_Text_map.put("Credential".toLowerCase(), "credentialType".toLowerCase());
		java_Text_map.put("Credential No".toLowerCase(), "credentialNo".toLowerCase());
		java_Text_map.put("Mobile Number".toLowerCase(), "mobileNumber".toLowerCase());
		java_Text_map.put("Email".toLowerCase(), "email".toLowerCase());
		java_Text_map.put("Gender".toLowerCase(), "genderType".toLowerCase());
		java_Text_map.put("Officer".toLowerCase(), "officerType".toLowerCase());
		java_Text_map.put("Insertion Date".toLowerCase(), "insertionDate".toLowerCase());
		java_Text_map.put("Inserted By".toLowerCase(), "insertedBy".toLowerCase());
		java_Text_map.put("Modified By".toLowerCase(), "modifiedBy".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}