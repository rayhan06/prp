package gate_pass_visitor;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class Gate_pass_visitorRepository implements Repository {
	Gate_pass_visitorDAO gate_pass_visitorDAO = new Gate_pass_visitorDAO();
	
	public void setDAO(Gate_pass_visitorDAO gate_pass_visitorDAO)
	{
		this.gate_pass_visitorDAO = gate_pass_visitorDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Gate_pass_visitorRepository.class);
	Map<Long, Gate_pass_visitorDTO>mapOfGate_pass_visitorDTOToid;
	Map<String, Set<Gate_pass_visitorDTO> >mapOfGate_pass_visitorDTOToname;
	Map<String, Set<Gate_pass_visitorDTO> >mapOfGate_pass_visitorDTOTofatherName;
	Map<String, Set<Gate_pass_visitorDTO> >mapOfGate_pass_visitorDTOTomotherName;
	Map<String, Set<Gate_pass_visitorDTO> >mapOfGate_pass_visitorDTOTopermanentAddress;
	Map<String, Set<Gate_pass_visitorDTO> >mapOfGate_pass_visitorDTOTocurrentAddress;
	Map<Integer, Set<Gate_pass_visitorDTO> >mapOfGate_pass_visitorDTOTocredentialType;
	Map<String, Set<Gate_pass_visitorDTO> >mapOfGate_pass_visitorDTOTocredentialNo;
	Map<String, Set<Gate_pass_visitorDTO> >mapOfGate_pass_visitorDTOTomobileNumber;
	Map<String, Set<Gate_pass_visitorDTO> >mapOfGate_pass_visitorDTOToemail;
	Map<Integer, Set<Gate_pass_visitorDTO> >mapOfGate_pass_visitorDTOTogenderType;
	Map<Integer, Set<Gate_pass_visitorDTO> >mapOfGate_pass_visitorDTOToofficerType;
	Map<Long, Set<Gate_pass_visitorDTO> >mapOfGate_pass_visitorDTOToinsertionDate;
	Map<String, Set<Gate_pass_visitorDTO> >mapOfGate_pass_visitorDTOToinsertedBy;
	Map<String, Set<Gate_pass_visitorDTO> >mapOfGate_pass_visitorDTOTomodifiedBy;
	Map<Long, Set<Gate_pass_visitorDTO> >mapOfGate_pass_visitorDTOTolastModificationTime;


	static Gate_pass_visitorRepository instance = null;  
	private Gate_pass_visitorRepository(){
		mapOfGate_pass_visitorDTOToid = new ConcurrentHashMap<>();
		mapOfGate_pass_visitorDTOToname = new ConcurrentHashMap<>();
		mapOfGate_pass_visitorDTOTofatherName = new ConcurrentHashMap<>();
		mapOfGate_pass_visitorDTOTomotherName = new ConcurrentHashMap<>();
		mapOfGate_pass_visitorDTOTopermanentAddress = new ConcurrentHashMap<>();
		mapOfGate_pass_visitorDTOTocurrentAddress = new ConcurrentHashMap<>();
		mapOfGate_pass_visitorDTOTocredentialType = new ConcurrentHashMap<>();
		mapOfGate_pass_visitorDTOTocredentialNo = new ConcurrentHashMap<>();
		mapOfGate_pass_visitorDTOTomobileNumber = new ConcurrentHashMap<>();
		mapOfGate_pass_visitorDTOToemail = new ConcurrentHashMap<>();
		mapOfGate_pass_visitorDTOTogenderType = new ConcurrentHashMap<>();
		mapOfGate_pass_visitorDTOToofficerType = new ConcurrentHashMap<>();
		mapOfGate_pass_visitorDTOToinsertionDate = new ConcurrentHashMap<>();
		mapOfGate_pass_visitorDTOToinsertedBy = new ConcurrentHashMap<>();
		mapOfGate_pass_visitorDTOTomodifiedBy = new ConcurrentHashMap<>();
		mapOfGate_pass_visitorDTOTolastModificationTime = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Gate_pass_visitorRepository getInstance(){
		if (instance == null){
			instance = new Gate_pass_visitorRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(gate_pass_visitorDAO == null)
		{
			return;
		}
		try {
			List<Gate_pass_visitorDTO> gate_pass_visitorDTOs = gate_pass_visitorDAO.getAllGate_pass_visitor(reloadAll);
			for(Gate_pass_visitorDTO gate_pass_visitorDTO : gate_pass_visitorDTOs) {
				Gate_pass_visitorDTO oldGate_pass_visitorDTO = getGate_pass_visitorDTOByid(gate_pass_visitorDTO.iD);
				if( oldGate_pass_visitorDTO != null ) {
					mapOfGate_pass_visitorDTOToid.remove(oldGate_pass_visitorDTO.iD);
				
					if(mapOfGate_pass_visitorDTOToname.containsKey(oldGate_pass_visitorDTO.name)) {
						mapOfGate_pass_visitorDTOToname.get(oldGate_pass_visitorDTO.name).remove(oldGate_pass_visitorDTO);
					}
					if(mapOfGate_pass_visitorDTOToname.get(oldGate_pass_visitorDTO.name).isEmpty()) {
						mapOfGate_pass_visitorDTOToname.remove(oldGate_pass_visitorDTO.name);
					}
					
					if(mapOfGate_pass_visitorDTOTofatherName.containsKey(oldGate_pass_visitorDTO.fatherName)) {
						mapOfGate_pass_visitorDTOTofatherName.get(oldGate_pass_visitorDTO.fatherName).remove(oldGate_pass_visitorDTO);
					}
					if(mapOfGate_pass_visitorDTOTofatherName.get(oldGate_pass_visitorDTO.fatherName).isEmpty()) {
						mapOfGate_pass_visitorDTOTofatherName.remove(oldGate_pass_visitorDTO.fatherName);
					}
					
					if(mapOfGate_pass_visitorDTOTomotherName.containsKey(oldGate_pass_visitorDTO.motherName)) {
						mapOfGate_pass_visitorDTOTomotherName.get(oldGate_pass_visitorDTO.motherName).remove(oldGate_pass_visitorDTO);
					}
					if(mapOfGate_pass_visitorDTOTomotherName.get(oldGate_pass_visitorDTO.motherName).isEmpty()) {
						mapOfGate_pass_visitorDTOTomotherName.remove(oldGate_pass_visitorDTO.motherName);
					}
					
					if(mapOfGate_pass_visitorDTOTopermanentAddress.containsKey(oldGate_pass_visitorDTO.permanentAddress)) {
						mapOfGate_pass_visitorDTOTopermanentAddress.get(oldGate_pass_visitorDTO.permanentAddress).remove(oldGate_pass_visitorDTO);
					}
					if(mapOfGate_pass_visitorDTOTopermanentAddress.get(oldGate_pass_visitorDTO.permanentAddress).isEmpty()) {
						mapOfGate_pass_visitorDTOTopermanentAddress.remove(oldGate_pass_visitorDTO.permanentAddress);
					}
					
					if(mapOfGate_pass_visitorDTOTocurrentAddress.containsKey(oldGate_pass_visitorDTO.currentAddress)) {
						mapOfGate_pass_visitorDTOTocurrentAddress.get(oldGate_pass_visitorDTO.currentAddress).remove(oldGate_pass_visitorDTO);
					}
					if(mapOfGate_pass_visitorDTOTocurrentAddress.get(oldGate_pass_visitorDTO.currentAddress).isEmpty()) {
						mapOfGate_pass_visitorDTOTocurrentAddress.remove(oldGate_pass_visitorDTO.currentAddress);
					}
					
					if(mapOfGate_pass_visitorDTOTocredentialType.containsKey(oldGate_pass_visitorDTO.credentialType)) {
						mapOfGate_pass_visitorDTOTocredentialType.get(oldGate_pass_visitorDTO.credentialType).remove(oldGate_pass_visitorDTO);
					}
					if(mapOfGate_pass_visitorDTOTocredentialType.get(oldGate_pass_visitorDTO.credentialType).isEmpty()) {
						mapOfGate_pass_visitorDTOTocredentialType.remove(oldGate_pass_visitorDTO.credentialType);
					}
					
					if(mapOfGate_pass_visitorDTOTocredentialNo.containsKey(oldGate_pass_visitorDTO.credentialNo)) {
						mapOfGate_pass_visitorDTOTocredentialNo.get(oldGate_pass_visitorDTO.credentialNo).remove(oldGate_pass_visitorDTO);
					}
					if(mapOfGate_pass_visitorDTOTocredentialNo.get(oldGate_pass_visitorDTO.credentialNo).isEmpty()) {
						mapOfGate_pass_visitorDTOTocredentialNo.remove(oldGate_pass_visitorDTO.credentialNo);
					}
					
					if(mapOfGate_pass_visitorDTOTomobileNumber.containsKey(oldGate_pass_visitorDTO.mobileNumber)) {
						mapOfGate_pass_visitorDTOTomobileNumber.get(oldGate_pass_visitorDTO.mobileNumber).remove(oldGate_pass_visitorDTO);
					}
					if(mapOfGate_pass_visitorDTOTomobileNumber.get(oldGate_pass_visitorDTO.mobileNumber).isEmpty()) {
						mapOfGate_pass_visitorDTOTomobileNumber.remove(oldGate_pass_visitorDTO.mobileNumber);
					}
					
					if(mapOfGate_pass_visitorDTOToemail.containsKey(oldGate_pass_visitorDTO.email)) {
						mapOfGate_pass_visitorDTOToemail.get(oldGate_pass_visitorDTO.email).remove(oldGate_pass_visitorDTO);
					}
					if(mapOfGate_pass_visitorDTOToemail.get(oldGate_pass_visitorDTO.email).isEmpty()) {
						mapOfGate_pass_visitorDTOToemail.remove(oldGate_pass_visitorDTO.email);
					}
					
					if(mapOfGate_pass_visitorDTOTogenderType.containsKey(oldGate_pass_visitorDTO.genderType)) {
						mapOfGate_pass_visitorDTOTogenderType.get(oldGate_pass_visitorDTO.genderType).remove(oldGate_pass_visitorDTO);
					}
					if(mapOfGate_pass_visitorDTOTogenderType.get(oldGate_pass_visitorDTO.genderType).isEmpty()) {
						mapOfGate_pass_visitorDTOTogenderType.remove(oldGate_pass_visitorDTO.genderType);
					}
					
					if(mapOfGate_pass_visitorDTOToofficerType.containsKey(oldGate_pass_visitorDTO.officerType)) {
						mapOfGate_pass_visitorDTOToofficerType.get(oldGate_pass_visitorDTO.officerType).remove(oldGate_pass_visitorDTO);
					}
					if(mapOfGate_pass_visitorDTOToofficerType.get(oldGate_pass_visitorDTO.officerType).isEmpty()) {
						mapOfGate_pass_visitorDTOToofficerType.remove(oldGate_pass_visitorDTO.officerType);
					}
					
					if(mapOfGate_pass_visitorDTOToinsertionDate.containsKey(oldGate_pass_visitorDTO.insertionDate)) {
						mapOfGate_pass_visitorDTOToinsertionDate.get(oldGate_pass_visitorDTO.insertionDate).remove(oldGate_pass_visitorDTO);
					}
					if(mapOfGate_pass_visitorDTOToinsertionDate.get(oldGate_pass_visitorDTO.insertionDate).isEmpty()) {
						mapOfGate_pass_visitorDTOToinsertionDate.remove(oldGate_pass_visitorDTO.insertionDate);
					}
					
					if(mapOfGate_pass_visitorDTOToinsertedBy.containsKey(oldGate_pass_visitorDTO.insertedBy)) {
						mapOfGate_pass_visitorDTOToinsertedBy.get(oldGate_pass_visitorDTO.insertedBy).remove(oldGate_pass_visitorDTO);
					}
					if(mapOfGate_pass_visitorDTOToinsertedBy.get(oldGate_pass_visitorDTO.insertedBy).isEmpty()) {
						mapOfGate_pass_visitorDTOToinsertedBy.remove(oldGate_pass_visitorDTO.insertedBy);
					}
					
					if(mapOfGate_pass_visitorDTOTomodifiedBy.containsKey(oldGate_pass_visitorDTO.modifiedBy)) {
						mapOfGate_pass_visitorDTOTomodifiedBy.get(oldGate_pass_visitorDTO.modifiedBy).remove(oldGate_pass_visitorDTO);
					}
					if(mapOfGate_pass_visitorDTOTomodifiedBy.get(oldGate_pass_visitorDTO.modifiedBy).isEmpty()) {
						mapOfGate_pass_visitorDTOTomodifiedBy.remove(oldGate_pass_visitorDTO.modifiedBy);
					}
					
					if(mapOfGate_pass_visitorDTOTolastModificationTime.containsKey(oldGate_pass_visitorDTO.lastModificationTime)) {
						mapOfGate_pass_visitorDTOTolastModificationTime.get(oldGate_pass_visitorDTO.lastModificationTime).remove(oldGate_pass_visitorDTO);
					}
					if(mapOfGate_pass_visitorDTOTolastModificationTime.get(oldGate_pass_visitorDTO.lastModificationTime).isEmpty()) {
						mapOfGate_pass_visitorDTOTolastModificationTime.remove(oldGate_pass_visitorDTO.lastModificationTime);
					}
					
					
				}
				if(gate_pass_visitorDTO.isDeleted == 0) 
				{
					
					mapOfGate_pass_visitorDTOToid.put(gate_pass_visitorDTO.iD, gate_pass_visitorDTO);
				
					if( ! mapOfGate_pass_visitorDTOToname.containsKey(gate_pass_visitorDTO.name)) {
						mapOfGate_pass_visitorDTOToname.put(gate_pass_visitorDTO.name, new HashSet<>());
					}
					mapOfGate_pass_visitorDTOToname.get(gate_pass_visitorDTO.name).add(gate_pass_visitorDTO);
					
					if( ! mapOfGate_pass_visitorDTOTofatherName.containsKey(gate_pass_visitorDTO.fatherName)) {
						mapOfGate_pass_visitorDTOTofatherName.put(gate_pass_visitorDTO.fatherName, new HashSet<>());
					}
					mapOfGate_pass_visitorDTOTofatherName.get(gate_pass_visitorDTO.fatherName).add(gate_pass_visitorDTO);
					
					if( ! mapOfGate_pass_visitorDTOTomotherName.containsKey(gate_pass_visitorDTO.motherName)) {
						mapOfGate_pass_visitorDTOTomotherName.put(gate_pass_visitorDTO.motherName, new HashSet<>());
					}
					mapOfGate_pass_visitorDTOTomotherName.get(gate_pass_visitorDTO.motherName).add(gate_pass_visitorDTO);
					
					if( ! mapOfGate_pass_visitorDTOTopermanentAddress.containsKey(gate_pass_visitorDTO.permanentAddress)) {
						mapOfGate_pass_visitorDTOTopermanentAddress.put(gate_pass_visitorDTO.permanentAddress, new HashSet<>());
					}
					mapOfGate_pass_visitorDTOTopermanentAddress.get(gate_pass_visitorDTO.permanentAddress).add(gate_pass_visitorDTO);
					
					if( ! mapOfGate_pass_visitorDTOTocurrentAddress.containsKey(gate_pass_visitorDTO.currentAddress)) {
						mapOfGate_pass_visitorDTOTocurrentAddress.put(gate_pass_visitorDTO.currentAddress, new HashSet<>());
					}
					mapOfGate_pass_visitorDTOTocurrentAddress.get(gate_pass_visitorDTO.currentAddress).add(gate_pass_visitorDTO);
					
					if( ! mapOfGate_pass_visitorDTOTocredentialType.containsKey(gate_pass_visitorDTO.credentialType)) {
						mapOfGate_pass_visitorDTOTocredentialType.put(gate_pass_visitorDTO.credentialType, new HashSet<>());
					}
					mapOfGate_pass_visitorDTOTocredentialType.get(gate_pass_visitorDTO.credentialType).add(gate_pass_visitorDTO);
					
					if( ! mapOfGate_pass_visitorDTOTocredentialNo.containsKey(gate_pass_visitorDTO.credentialNo)) {
						mapOfGate_pass_visitorDTOTocredentialNo.put(gate_pass_visitorDTO.credentialNo, new HashSet<>());
					}
					mapOfGate_pass_visitorDTOTocredentialNo.get(gate_pass_visitorDTO.credentialNo).add(gate_pass_visitorDTO);
					
					if( ! mapOfGate_pass_visitorDTOTomobileNumber.containsKey(gate_pass_visitorDTO.mobileNumber)) {
						mapOfGate_pass_visitorDTOTomobileNumber.put(gate_pass_visitorDTO.mobileNumber, new HashSet<>());
					}
					mapOfGate_pass_visitorDTOTomobileNumber.get(gate_pass_visitorDTO.mobileNumber).add(gate_pass_visitorDTO);
					
					if( ! mapOfGate_pass_visitorDTOToemail.containsKey(gate_pass_visitorDTO.email)) {
						mapOfGate_pass_visitorDTOToemail.put(gate_pass_visitorDTO.email, new HashSet<>());
					}
					mapOfGate_pass_visitorDTOToemail.get(gate_pass_visitorDTO.email).add(gate_pass_visitorDTO);
					
					if( ! mapOfGate_pass_visitorDTOTogenderType.containsKey(gate_pass_visitorDTO.genderType)) {
						mapOfGate_pass_visitorDTOTogenderType.put(gate_pass_visitorDTO.genderType, new HashSet<>());
					}
					mapOfGate_pass_visitorDTOTogenderType.get(gate_pass_visitorDTO.genderType).add(gate_pass_visitorDTO);
					
					if( ! mapOfGate_pass_visitorDTOToofficerType.containsKey(gate_pass_visitorDTO.officerType)) {
						mapOfGate_pass_visitorDTOToofficerType.put(gate_pass_visitorDTO.officerType, new HashSet<>());
					}
					mapOfGate_pass_visitorDTOToofficerType.get(gate_pass_visitorDTO.officerType).add(gate_pass_visitorDTO);
					
					if( ! mapOfGate_pass_visitorDTOToinsertionDate.containsKey(gate_pass_visitorDTO.insertionDate)) {
						mapOfGate_pass_visitorDTOToinsertionDate.put(gate_pass_visitorDTO.insertionDate, new HashSet<>());
					}
					mapOfGate_pass_visitorDTOToinsertionDate.get(gate_pass_visitorDTO.insertionDate).add(gate_pass_visitorDTO);
					
					if( ! mapOfGate_pass_visitorDTOToinsertedBy.containsKey(gate_pass_visitorDTO.insertedBy)) {
						mapOfGate_pass_visitorDTOToinsertedBy.put(gate_pass_visitorDTO.insertedBy, new HashSet<>());
					}
					mapOfGate_pass_visitorDTOToinsertedBy.get(gate_pass_visitorDTO.insertedBy).add(gate_pass_visitorDTO);
					
					if( ! mapOfGate_pass_visitorDTOTomodifiedBy.containsKey(gate_pass_visitorDTO.modifiedBy)) {
						mapOfGate_pass_visitorDTOTomodifiedBy.put(gate_pass_visitorDTO.modifiedBy, new HashSet<>());
					}
					mapOfGate_pass_visitorDTOTomodifiedBy.get(gate_pass_visitorDTO.modifiedBy).add(gate_pass_visitorDTO);
					
					if( ! mapOfGate_pass_visitorDTOTolastModificationTime.containsKey(gate_pass_visitorDTO.lastModificationTime)) {
						mapOfGate_pass_visitorDTOTolastModificationTime.put(gate_pass_visitorDTO.lastModificationTime, new HashSet<>());
					}
					mapOfGate_pass_visitorDTOTolastModificationTime.get(gate_pass_visitorDTO.lastModificationTime).add(gate_pass_visitorDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Gate_pass_visitorDTO> getGate_pass_visitorList() {
		List <Gate_pass_visitorDTO> gate_pass_visitors = new ArrayList<Gate_pass_visitorDTO>(this.mapOfGate_pass_visitorDTOToid.values());
		return gate_pass_visitors;
	}
	
	
	public Gate_pass_visitorDTO getGate_pass_visitorDTOByid( long id){
		return mapOfGate_pass_visitorDTOToid.get(id);
	}
	
	
	public List<Gate_pass_visitorDTO> getGate_pass_visitorDTOByname(String name) {
		return new ArrayList<>( mapOfGate_pass_visitorDTOToname.getOrDefault(name,new HashSet<>()));
	}
	
	
	public List<Gate_pass_visitorDTO> getGate_pass_visitorDTOByfather_name(String father_name) {
		return new ArrayList<>( mapOfGate_pass_visitorDTOTofatherName.getOrDefault(father_name,new HashSet<>()));
	}
	
	
	public List<Gate_pass_visitorDTO> getGate_pass_visitorDTOBymother_name(String mother_name) {
		return new ArrayList<>( mapOfGate_pass_visitorDTOTomotherName.getOrDefault(mother_name,new HashSet<>()));
	}
	
	
	public List<Gate_pass_visitorDTO> getGate_pass_visitorDTOBypermanent_address(String permanent_address) {
		return new ArrayList<>( mapOfGate_pass_visitorDTOTopermanentAddress.getOrDefault(permanent_address,new HashSet<>()));
	}
	
	
	public List<Gate_pass_visitorDTO> getGate_pass_visitorDTOBycurrent_address(String current_address) {
		return new ArrayList<>( mapOfGate_pass_visitorDTOTocurrentAddress.getOrDefault(current_address,new HashSet<>()));
	}
	
	
	public List<Gate_pass_visitorDTO> getGate_pass_visitorDTOBycredential_type(int credential_type) {
		return new ArrayList<>( mapOfGate_pass_visitorDTOTocredentialType.getOrDefault(credential_type,new HashSet<>()));
	}
	
	
	public List<Gate_pass_visitorDTO> getGate_pass_visitorDTOBycredential_no(String credential_no) {
		return new ArrayList<>( mapOfGate_pass_visitorDTOTocredentialNo.getOrDefault(credential_no,new HashSet<>()));
	}
	
	
	public List<Gate_pass_visitorDTO> getGate_pass_visitorDTOBymobile_number(String mobile_number) {
		return new ArrayList<>( mapOfGate_pass_visitorDTOTomobileNumber.getOrDefault(mobile_number,new HashSet<>()));
	}
	
	
	public List<Gate_pass_visitorDTO> getGate_pass_visitorDTOByemail(String email) {
		return new ArrayList<>( mapOfGate_pass_visitorDTOToemail.getOrDefault(email,new HashSet<>()));
	}
	
	
	public List<Gate_pass_visitorDTO> getGate_pass_visitorDTOBygender_type(int gender_type) {
		return new ArrayList<>( mapOfGate_pass_visitorDTOTogenderType.getOrDefault(gender_type,new HashSet<>()));
	}
	
	
	public List<Gate_pass_visitorDTO> getGate_pass_visitorDTOByofficer_type(int officer_type) {
		return new ArrayList<>( mapOfGate_pass_visitorDTOToofficerType.getOrDefault(officer_type,new HashSet<>()));
	}
	
	
	public List<Gate_pass_visitorDTO> getGate_pass_visitorDTOByinsertion_date(long insertion_date) {
		return new ArrayList<>( mapOfGate_pass_visitorDTOToinsertionDate.getOrDefault(insertion_date,new HashSet<>()));
	}
	
	
	public List<Gate_pass_visitorDTO> getGate_pass_visitorDTOByinserted_by(String inserted_by) {
		return new ArrayList<>( mapOfGate_pass_visitorDTOToinsertedBy.getOrDefault(inserted_by,new HashSet<>()));
	}
	
	
	public List<Gate_pass_visitorDTO> getGate_pass_visitorDTOBymodified_by(String modified_by) {
		return new ArrayList<>( mapOfGate_pass_visitorDTOTomodifiedBy.getOrDefault(modified_by,new HashSet<>()));
	}
	
	
	public List<Gate_pass_visitorDTO> getGate_pass_visitorDTOBylastModificationTime(long lastModificationTime) {
		return new ArrayList<>( mapOfGate_pass_visitorDTOTolastModificationTime.getOrDefault(lastModificationTime,new HashSet<>()));
	}



	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "gate_pass_visitor";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


