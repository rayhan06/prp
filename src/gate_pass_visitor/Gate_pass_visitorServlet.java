package gate_pass_visitor;

import com.google.gson.Gson;
import employee_records.Employee_recordsDTO;
import employee_records.Employee_recordsRepository;
import gate_pass.Gate_passDAO;
import gate_pass.Gate_passDTO;
import gate_pass_affiliated_person.Gate_pass_affiliated_personDAO;
import gate_pass_affiliated_person.Gate_pass_affiliated_personDTO;
import gate_pass_item.Gate_pass_itemDAO;
import gate_pass_item.Gate_pass_itemDTO;
import gate_pass_type.Gate_pass_typeDTO;
import geolocation.GeoLocationDAO2;
import login.LoginDTO;
import pb.Utils;

import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import pb_notifications.Pb_notificationsDAO;
import permission.MenuConstants;
import role.PermissionRepository;
import sessionmanager.SessionConstants;
import task_type.TaskTypeEnum;
import task_type_approval_path.TaskTypeApprovalPathRepository;
import user.UserDTO;
import user.UserRepository;
import util.CommonRequestHandler;
import util.HttpRequestUtils;
import util.RecordNavigationManager4;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


/**
 * Servlet implementation class Gate_pass_visitorServlet
 */
@WebServlet("/Gate_pass_visitorServlet")
@MultipartConfig
public class Gate_pass_visitorServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Gate_pass_visitorServlet.class);

    String tableName = "gate_pass_visitor";

    Pb_notificationsDAO pb_notificationsDAO = Pb_notificationsDAO.getInstance();
    Gate_pass_visitorDAO gate_pass_visitorDAO;
    CommonRequestHandler commonRequestHandler;
    private final Gson gson = new Gson();

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Gate_pass_visitorServlet() {
        super();
        try {
            gate_pass_visitorDAO = new Gate_pass_visitorDAO(tableName);
            commonRequestHandler = new CommonRequestHandler(gate_pass_visitorDAO);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("In doget request = " + request);
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        boolean isPermanentTable = true;
        if (request.getParameter("isPermanentTable") != null) {
            isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
        }
        System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
        try {
            String actionType = request.getParameter("actionType");
            if (actionType.equals("getAddPage")) {
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.GATE_PASS_VISITOR_ADD))
				{
					commonRequestHandler.getAddPage(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
            } else if (actionType.equals("getEditPage")) {
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.GATE_PASS_VISITOR_UPDATE)) {
                    getGate_pass_visitor(request, response);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else if (actionType.equals("getURL")) {
                String URL = request.getParameter("URL");
                System.out.println("URL = " + URL);
                response.sendRedirect(URL);
            } else if (actionType.equals("search")) {
                System.out.println("search requested");
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.GATE_PASS_VISITOR_SEARCH)) {
                    if (isPermanentTable) {
                        String filter = request.getParameter("filter");
                        System.out.println("filter = " + filter);
                        if (filter != null) {
                            filter = ""; //shouldn't be directly used, rather manipulate it.
                            searchGate_pass_visitor(request, response, isPermanentTable, filter);
                        } else {
                            searchGate_pass_visitor(request, response, isPermanentTable, "");
                        }
                    } else {
                        //searchGate_pass_visitor(request, response, tempTableName, isPermanentTable);
                    }
                }
            } else if (actionType.equals("view")) {
                System.out.println("view requested");
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.GATE_PASS_VISITOR_SEARCH)) {
                    commonRequestHandler.view(request, response);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }

            } else if (actionType.equals("getStoredDataByMobileNumber")) {
                String mobileNumber = request.getParameter("mobileNumber");
                String credentialNo = request.getParameter("credentialNo");

                Gate_pass_visitorDTO dto = gate_pass_visitorDAO.getDTOByMobileNumber(mobileNumber, credentialNo);

                if (dto == null) {
                    PrintWriter out = response.getWriter();
                    out.println("noString");
                    out.close();
                } else {
                    String jsonInString = new Gson().toJson(dto);
                    JSONObject mJSONObject = new JSONObject(jsonInString);

                    PrintWriter out = response.getWriter();
                    out.println(mJSONObject);
                    out.close();
                }
            } else if (actionType.equals("samePassCheck")) {
                String mobileNumber = request.getParameter("mobileNumber");
                String credentialNo = request.getParameter("credentialNo");
                Long visitingDate = Long.parseLong(request.getParameter("visitingDate"));
                Long gatePassType = Long.parseLong(request.getParameter("gatePassType"));
                Long gatePassSubType = Long.parseLong(request.getParameter("gatePassSubType"));

                int count = gate_pass_visitorDAO.countSameDayGatePass(mobileNumber, credentialNo, visitingDate, gatePassType, gatePassSubType);

                PrintWriter out = response.getWriter();
                if (count == 0)
                    out.print("noSamePass");
                else
                    out.print("foundSamePass");
                out.close();
            } else {
                request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.debug(ex);
        }
    }


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        System.out.println("doPost");
        boolean isPermanentTable = true;
        if (request.getParameter("isPermanentTable") != null) {
            isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
        }
        System.out.println("In servlet, isPermanentTable = " + isPermanentTable);

        try {
            String actionType = request.getParameter("actionType");
            System.out.println("actionType = " + actionType);
            if (actionType.equals("add")) {

                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.GATE_PASS_VISITOR_ADD)) {
                    System.out.println("going to  addGate_pass_visitor ");
                    addGate_pass_visitor(request, response, true, userDTO, true);
                } else {
                    System.out.println("Not going to  addGate_pass_visitor ");
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }

            } else if (actionType.equals("getDTO")) {

                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.GATE_PASS_VISITOR_ADD)) {
                    getDTO(request, response);
                } else {
                    System.out.println("Not going to  addGate_pass_visitor ");
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }

            } else if (actionType.equals("edit")) {

                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.GATE_PASS_VISITOR_UPDATE)) {
                    addGate_pass_visitor(request, response, false, userDTO, isPermanentTable);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else if (actionType.equals("delete")) {
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.FAST_SEARCH_TEST_UPDATE)) {
                    commonRequestHandler.delete(request, response, userDTO);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else if (actionType.equals("search")) {
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.GATE_PASS_VISITOR_SEARCH)) {
                    searchGate_pass_visitor(request, response, true, "");
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else if (actionType.equals("getGeo")) {
                System.out.println("going to geoloc ");
                request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            logger.debug(ex);
        }
    }

    private void getDTO(HttpServletRequest request, HttpServletResponse response) {
        try {
            System.out.println("In getDTO");
            Gate_pass_visitorDTO gate_pass_visitorDTO = gate_pass_visitorDAO.getDTOByID(Long.parseLong(request.getParameter("ID")));
            PrintWriter out = response.getWriter();
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");

            String encoded = this.gson.toJson(gate_pass_visitorDTO);
            System.out.println("json encoded data = " + encoded);
            out.print(encoded);
            out.flush();
        } catch (NumberFormatException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    private void addGate_pass_visitor(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, boolean isPermanentTable) throws IOException {
        // TODO Auto-generated method stub
        try {
            request.setAttribute("failureMessage", "");
            System.out.println("%%%% addGate_pass_visitor");
            String path = getServletContext().getRealPath("/img2/");
            Gate_pass_visitorDTO gate_pass_visitorDTO;
            SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
            
            String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
            boolean isLangEng = Language.equalsIgnoreCase("English");

            if (addFlag == true) {
                gate_pass_visitorDTO = new Gate_pass_visitorDTO();
            } else {
                gate_pass_visitorDTO = gate_pass_visitorDAO.getDTOByID(Long.parseLong(request.getParameter("iD")));
            }

            String Value = "";

            Value = request.getParameter("name");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText()).trim();
            }
            System.out.println("name = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                gate_pass_visitorDTO.name = (Value);
            } 
            else 
            {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
                String message = isLangEng ? "Wrong Visitor info, pass rejected!" : "প্রবেশকারীর তথ্য ভুল থাকায় পাসটি বাতিল হয়েছে!";
                response.sendRedirect("Gate_passServlet?actionType=getAddPage&error=1&message=" + message);
                return;
            }

            Value = request.getParameter("fatherName");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("fatherName = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                gate_pass_visitorDTO.fatherName = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("motherName");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("motherName = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                gate_pass_visitorDTO.motherName = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("permanentAddress");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("permanentAddress = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {


                {

                    String[] addresses = Value.split("\\$", 2);
                    String addressDetails = "", address_id = "";

                    try {
                        address_id = addresses[0];
                        addressDetails = addresses[1];
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }


                    try {
                        if (address_id.matches("-?\\d+")) {
                            gate_pass_visitorDTO.permanentAddress
                                    = address_id + ":"
                                    + GeoLocationDAO2.getLocationText(Integer.parseInt(address_id), "English") + ":"
                                    + addressDetails + ":"
                                    + GeoLocationDAO2.getLocationText(Integer.parseInt(address_id), "Bangla");
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }

            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("currentAddress");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("currentAddress = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {


                {
                    String[] addresses = Value.split("\\$", 2);
                    String addressDetails = "", address_id = "";

                    try {
                        address_id = addresses[0];
                        addressDetails = addresses[1];
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }

                    try {
                        if (address_id.matches("-?\\d+")) {
                            gate_pass_visitorDTO.currentAddress
                                    = address_id + ":"
                                    + GeoLocationDAO2.getLocationText(Integer.parseInt(address_id), "English") + ":"
                                    + addressDetails + ":"
                                    + GeoLocationDAO2.getLocationText(Integer.parseInt(address_id), "Bangla");
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }

            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("credentialType");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("credentialType = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                gate_pass_visitorDTO.credentialType = Integer.parseInt(Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("credentialNo");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText()).trim();
            }
            System.out.println("credentialNo = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                gate_pass_visitorDTO.credentialNo = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
                response.sendRedirect("Gate_passServlet?actionType=getAddPage&error=1&message=" + Gate_passDTO.ERROR_VISITOR);
                return;
            }

            Value = request.getParameter("mobileNumber");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText()).trim();
            }
            System.out.println("mobileNumber = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {           	
                gate_pass_visitorDTO.mobileNumber = "88" + Value;
                if(!Utils.isMobileNumberValid(gate_pass_visitorDTO.mobileNumber))
                {
                    response.sendRedirect("Gate_passServlet?actionType=getAddPage&error=1&message=" + Gate_passDTO.ERROR_VISITOR);
                    return;
                }
            } else {
                response.sendRedirect("Gate_passServlet?actionType=getAddPage&error=1&message=" + Gate_passDTO.ERROR_VISITOR);
                return;
            }

            Value = request.getParameter("email");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("email = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                gate_pass_visitorDTO.email = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }
            
            Value = request.getParameter("officeName");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("officeName = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                gate_pass_visitorDTO.officeName = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }
            
            Value = request.getParameter("designation");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("designation = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                gate_pass_visitorDTO.designation = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("genderType");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("genderType = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                gate_pass_visitorDTO.genderType = Integer.parseInt(Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("officerType");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("officerType = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                gate_pass_visitorDTO.officerType = Integer.parseInt(Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }
            
            Value = request.getParameter("vehicleDescription");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("vehicleDescription = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                gate_pass_visitorDTO.vehicleDescription = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }
            
            Value = request.getParameter("vehicleNumber");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("vehicleNumber = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                gate_pass_visitorDTO.vehicleNumber = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }
            
            Value = request.getParameter("hasVehicle");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("hasVehicle = " + Value);
            gate_pass_visitorDTO.hasVehicle = Value != null && !Value.equalsIgnoreCase("");
            
            
            

            if (addFlag) {
                Calendar c = Calendar.getInstance();
                c.set(Calendar.HOUR_OF_DAY, 0);
                c.set(Calendar.MINUTE, 0);
                c.set(Calendar.SECOND, 0);
                c.set(Calendar.MILLISECOND, 0);

                gate_pass_visitorDTO.insertionDate = c.getTimeInMillis();
            }


            Value = request.getParameter("insertedBy");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("insertedBy = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                gate_pass_visitorDTO.insertedBy = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("modifiedBy");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("modifiedBy = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                gate_pass_visitorDTO.modifiedBy = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            System.out.println("Done adding  addGate_pass_visitor dto = " + gate_pass_visitorDTO);


            HttpSession session = request.getSession(false);
            Gate_passDTO gate_passDTO = (Gate_passDTO) session.getAttribute("gate_passDTO");


            // Now it's time to remove attribute
            session.removeAttribute("gate_passDTO");
            session.setAttribute("isToast", "yes");
            gate_pass_visitorDTO.affiliatedInfo = "";
            
            List<Gate_pass_affiliated_personDTO> gate_pass_affiliated_personDTOs = new ArrayList<Gate_pass_affiliated_personDTO>();
            
            if (gate_passDTO.gatePassTypeId == Gate_pass_typeDTO.AREA_PASS || gate_passDTO.gatePassTypeId == Gate_pass_typeDTO.MEETING_PASS 
            		|| gate_passDTO.gatePassTypeId == Gate_pass_typeDTO.BUILDING_VISITOR_PASS) {
                // Insert List of Affiliated persons
                int personCount = Integer.parseInt(request.getParameter("additionalPersonCount"));
                for (int i = 0; i < personCount; i++) {
                    Gate_pass_affiliated_personDTO dto = new Gate_pass_affiliated_personDTO();
                   
                    dto.credentialType = Integer.parseInt(Jsoup.clean(request.getParameter("credentialType_" + i), Whitelist.simpleText()));
                    dto.credentialNo = Jsoup.clean(request.getParameter("credentialNo_" + i), Whitelist.simpleText());
                    dto.mobileNumber = Jsoup.clean(request.getParameter("mobileNumber_" + i), Whitelist.simpleText());
                    dto.name = Jsoup.clean(request.getParameter("name_" + i), Whitelist.simpleText());
                    dto.officeName = Jsoup.clean(request.getParameter("officeName_" + i), Whitelist.simpleText());
                    dto.designation = Jsoup.clean(request.getParameter("designation_" + i), Whitelist.simpleText());
                    
                    System.out.println("dto.officeName = " + dto.officeName + " dto.designation = " + dto.designation);
                    
                    if(dto.name != null && !dto.name.equalsIgnoreCase("")
                    && dto.mobileNumber != null && !dto.mobileNumber.equalsIgnoreCase("")
                   
                    )
                    {
                    	gate_pass_visitorDTO.affiliatedInfo += " " + dto.name + " " 
                                + Utils.getDigits(dto.mobileNumber, "english") + " "
                                + Utils.getDigits(dto.mobileNumber, "bangla") + " ";
                    	gate_pass_affiliated_personDTOs.add(dto);
                    }
                    else
                    {
                    	request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                        return;
                    }
                    
                    

                }
            }

            // Insert Gate_passDAO, Gate_pass_visitorDAO, affiliatedPersonDTOList into database
            // Insert Gate_pass_visitorDAO
            Gate_pass_visitorDTO storedDTO = gate_pass_visitorDAO.getDTOByMobileNumber(gate_pass_visitorDTO.mobileNumber, gate_pass_visitorDTO.credentialNo);
            if (storedDTO == null) {
                gate_passDTO.gatePassVisitorId = gate_pass_visitorDAO.add(gate_pass_visitorDTO);
            } else {
//				gate_pass_visitorDAO.deleteDTOByMobileNumber(gate_pass_visitorDTO.mobileNumber);
                gate_pass_visitorDAO.updateDTOById(storedDTO.id, gate_pass_visitorDTO);
//				gate_pass_visitorDAO.add(gate_pass_visitorDTO);
                gate_passDTO.gatePassVisitorId = storedDTO.id;
            }


            // Insert Gate_passDAO

            Gate_passDAO gate_passDAO = new Gate_passDAO();

            if (gate_passDTO.referrerId == userDTO.employee_record_id) {
                gate_passDTO.firstLayerApprovalStatus = Gate_passDAO.approvalStatusApproved;
                gate_passDTO.firstLayerApprovalTime = System.currentTimeMillis();
                gate_passDTO.firstLayerApprovedBy = userDTO.userName;
            }

            long gatePassId = gate_passDAO.add(gate_passDTO);

           

            if (gate_passDTO.gatePassTypeId == Gate_pass_typeDTO.AREA_PASS 
            		|| gate_passDTO.gatePassTypeId == Gate_pass_typeDTO.MEETING_PASS 
            		|| gate_passDTO.gatePassTypeId == Gate_pass_typeDTO.BUILDING_VISITOR_PASS) {
                // Insert List of Affiliated persons
                Gate_pass_affiliated_personDAO gate_pass_affiliated_personDAO = new Gate_pass_affiliated_personDAO();
                for (Gate_pass_affiliated_personDTO dto: gate_pass_affiliated_personDTOs) {                 
                    dto.gatePassId = gatePassId;
                    gate_pass_affiliated_personDAO.add(dto);
                }
            }


            if (gate_passDTO.gatePassTypeId == Gate_pass_typeDTO.BUILDING_VISITOR_PASS
            		|| gate_passDTO.gatePassTypeId == Gate_pass_typeDTO.MEETING_PASS) {
                // Insert List of Gate Pass Items
                int itemCount = Integer.parseInt(request.getParameter("totalItemCount"));
                Gate_pass_itemDAO gate_pass_itemDAO = new Gate_pass_itemDAO();
                for (int i = 0; i < itemCount; i++) {

                    Gate_pass_itemDTO dto = new Gate_pass_itemDTO();
                    try {
                        dto.parliamentItemId = Long.parseLong(request.getParameter("parliamentItemId_" + (i + 1)));
                    } catch (Exception e) {
                        continue;
                    }
                    dto.gatePassId = gatePassId;
                    try {
                        dto.amount = Integer.parseInt(request.getParameter("amount_" + (i + 1)));
                        dto.approvedAmount = dto.amount;
                    } catch (Exception e) {
                        continue;
                    }
                    dto.description = request.getParameter("description_" + (i + 1));

                    if (dto.amount > 0) gate_pass_itemDAO.add(dto);
                }
            }


            if (gate_passDTO.referrerId != userDTO.employee_record_id) {
                // generate notification and sent it to referrer
                String URL = "Gate_passServlet?notification=true&actionType=view&ID=" + gatePassId;
                String notificationMessage = gate_pass_visitorDTO.name + " has submitted a Gate Pass waiting for your approval.$" + gate_pass_visitorDTO.name + " প্রবেশ পাসের জন্য আবেদন করেছেন, যা আপনার সুপারিশের অপেক্ষায় আছে।";
                long organogramId = UserRepository.getInstance().getUserDtoByEmployeeRecordId(gate_passDTO.referrerId).organogramID;
                pb_notificationsDAO.addPb_notifications(organogramId, System.currentTimeMillis(), URL, notificationMessage);
            } else {
                // self referrer cas: send notification to sergeant-at-arms
                Employee_recordsDTO employeeRecordsDTO = Employee_recordsRepository.getInstance().getById(gate_passDTO.referrerId);
                String URL = "Gate_passServlet?notification=true&actionType=armsOfficeView&ID=" + gatePassId;
                String notificationMessage = "A gate pass referred by " + employeeRecordsDTO.nameEng +
                        " has been waiting for your approval$" + employeeRecordsDTO.nameBng +
                        " একটি প্রবেশ পাস সুপারিশ করেছেন, যা আপনার অনুমোদনের অপেক্ষায় আছে।";


                TaskTypeApprovalPathRepository.getInstance().getByTaskTypeIdAndLevel(TaskTypeEnum.SERGEANT_AT_ARMS_APPROVAL.getValue(), 1).stream()
                        .forEach(taskTypeApprovalPathDTO -> pb_notificationsDAO.addPb_notifications(
                                taskTypeApprovalPathDTO.officeUnitOrganogramId, System.currentTimeMillis(), URL, notificationMessage));
            }
            
                       //  then Redirect to the gate pass page
            response.sendRedirect("Gate_passServlet?actionType=getAddPage&error=0&message=" + Gate_passDTO.SUCCESS_MSG);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void getGate_pass_visitor(HttpServletRequest request, HttpServletResponse response, long id) throws ServletException, IOException {
        System.out.println("in getGate_pass_visitor");
        Gate_pass_visitorDTO gate_pass_visitorDTO = null;
        try {
            gate_pass_visitorDTO = gate_pass_visitorDAO.getDTOByID(id);
            request.setAttribute("ID", gate_pass_visitorDTO.iD);
            request.setAttribute("gate_pass_visitorDTO", gate_pass_visitorDTO);
            request.setAttribute("gate_pass_visitorDAO", gate_pass_visitorDAO);

            String URL = "";

            String inPlaceEdit = request.getParameter("inplaceedit");
            String inPlaceSubmit = request.getParameter("inplacesubmit");
            String getBodyOnly = request.getParameter("getBodyOnly");

            if (inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase("")) {
                URL = "gate_pass_visitor/gate_pass_visitorInPlaceEdit.jsp";
                request.setAttribute("inplaceedit", "");
            } else if (inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase("")) {
                URL = "gate_pass_visitor/gate_pass_visitorSearchRow.jsp";
                request.setAttribute("inplacesubmit", "");
            } else {
                if (getBodyOnly != null && !getBodyOnly.equalsIgnoreCase("")) {
                    URL = "gate_pass_visitor/gate_pass_visitorEditBody.jsp?actionType=edit";
                } else {
                    URL = "gate_pass_visitor/gate_pass_visitorEdit.jsp?actionType=edit";
                }
            }

            RequestDispatcher rd = request.getRequestDispatcher(URL);
            rd.forward(request, response);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void getGate_pass_visitor(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        getGate_pass_visitor(request, response, Long.parseLong(request.getParameter("ID")));
    }

    private void searchGate_pass_visitor(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException {
        System.out.println("in  searchGate_pass_visitor 1");
        LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        String ajax = request.getParameter("ajax");
        boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);

        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
                SessionConstants.NAV_GATE_PASS_VISITOR,
                request,
                gate_pass_visitorDAO,
                SessionConstants.VIEW_GATE_PASS_VISITOR,
                SessionConstants.SEARCH_GATE_PASS_VISITOR,
                tableName,
                isPermanent,
                userDTO,
                filter,
                true);
        try {
            System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        } catch (Exception e) {
            System.out.println("failed to dojob" + e);
        }

        request.setAttribute("gate_pass_visitorDAO", gate_pass_visitorDAO);
        RequestDispatcher rd;
        if (!isPermanent) {
            if (hasAjax == false) {
                System.out.println("Going to gate_pass_visitor/gate_pass_visitorApproval.jsp");
                rd = request.getRequestDispatcher("gate_pass_visitor/gate_pass_visitorApproval.jsp");
            } else {
                System.out.println("Going to gate_pass_visitor/gate_pass_visitorApprovalForm.jsp");
                rd = request.getRequestDispatcher("gate_pass_visitor/gate_pass_visitorApprovalForm.jsp");
            }
        } else {
            if (hasAjax == false) {
                System.out.println("Going to gate_pass_visitor/gate_pass_visitorSearch.jsp");
                rd = request.getRequestDispatcher("gate_pass_visitor/gate_pass_visitorSearch.jsp");
            } else {
                System.out.println("Going to gate_pass_visitor/gate_pass_visitorSearchForm.jsp");
                rd = request.getRequestDispatcher("gate_pass_visitor/gate_pass_visitorSearchForm.jsp");
            }
        }
        rd.forward(request, response);
    }

}

