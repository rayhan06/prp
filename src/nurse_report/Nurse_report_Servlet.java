package nurse_report;


import java.io.IOException;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import language.LC;
import language.LM;

import login.LoginDTO;
import permission.MenuConstants;

import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;

import util.ReportRequestHandler;
import pbReport.*;






@WebServlet("/Nurse_report_Servlet")
public class Nurse_report_Servlet  extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	String[][] Criteria =
	{	
		{"criteria","nurse_action_details","nurse_employee_record_id","=","","String","","","any","nurse_user_name",LC.HM_NURSE + "", "userNameToEmployeeRecordId"},
		{"criteria","users","employee_record_id","=","AND","String","","","any","userName",LC.HM_EMPLOYEE_ID + "", "userNameToEmployeeRecordId"},
		{"criteria","","nurse_action_cat","=","AND","int","","","any","nurseActionCat",LC.HM_ACTION + ""},
		{"criteria","","action_date",">=","AND","long","","",1 + "","startDate", LC.HM_START_DATE + ""},		
		{"criteria","","action_date","<=","AND","long","","",Long.MAX_VALUE + "","endDate", LC.HM_END_DATE + ""},
	};
	
	String[][] Display =
	{
		{"display","","nurse_action_details.nurse_employee_record_id","erIdToName",""},	
		{"display","","users.employee_record_id","erIdToName",""},				
		{"display","","patient_name","text",""},		
		{"display","","nurse_action_cat","cat",""},		
		{"display","","action_date","date",""},
		{"display","","strip_count","int",""}
	};
	
	String GroupBy = "";
	String OrderBY = " nurse_action_details.id desc ";
	
	ReportRequestHandler reportRequestHandler;
	
	public Nurse_report_Servlet(){

	}
	
	private final ReportService reportService = new ReportService();
	
	private String sql;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String language = "english";
		if(userDTO!= null && userDTO.languageID == SessionConstants.BANGLA)
		{
			language = "bangla";
		}
				
		String actionType = request.getParameter("actionType");

		System.out.println("In ssservlet doget, actiontype = " + actionType);
		
		sql = " nurse_action_details join users on users.username = patient_user_name ";

		Display[0][4] = LM.getText(LC.HM_NURSE, loginDTO);
		Display[1][4] = LM.getText(LC.HM_REFERENCE_EMPLOYEE, loginDTO);
		Display[2][4] = LM.getText(LC.HM_PATIENT_NAME, loginDTO);
		Display[3][4] = LM.getText(LC.HM_ACTION, loginDTO);
		Display[4][4] = LM.getText(LC.HM_DATE, loginDTO);
		Display[5][4] = language.equalsIgnoreCase("english")?"Strip Count":"স্ট্রিপের সংখ্যা";
		
		String reportName = LM.getText(LC.NURSE_REPORT_OTHER_NURSE_REPORT, loginDTO);
		
		reportRequestHandler = new ReportRequestHandler(null,
				Criteria, Display, GroupBy, OrderBY, sql,
				reportService);

		
		reportRequestHandler.handleReportGet(request, response, userDTO, "nurse_report",
				MenuConstants.NURSE_REPORT_DETAILS, language, reportName, "nurse_report");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		doGet(request, response);
	}
}
