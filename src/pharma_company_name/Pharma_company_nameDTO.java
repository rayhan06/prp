package pharma_company_name;
import java.util.*; 
import util.*; 


public class Pharma_company_nameDTO extends CommonDTO
{

    public String nameEn = "";
    public String nameBn = "";
    public String address = "";
    public String contact = "";
    public String email = "";
	
	
    @Override
	public String toString() {
            return "$Pharma_company_nameDTO[" +
            " iD = " + iD +
            " nameEn = " + nameEn +
            " nameBn = " + nameBn +
            " address = " + address +
            " contact = " + contact +
            " email = " + email +
            " searchColumn = " + searchColumn +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}