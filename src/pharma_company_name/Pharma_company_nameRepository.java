package pharma_company_name;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class Pharma_company_nameRepository implements Repository {
	Pharma_company_nameDAO pharma_company_nameDAO = null;
	
	public void setDAO(Pharma_company_nameDAO pharma_company_nameDAO)
	{
		this.pharma_company_nameDAO = pharma_company_nameDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Pharma_company_nameRepository.class);
	Map<Long, Pharma_company_nameDTO>mapOfPharma_company_nameDTOToiD;
	Map<String, Set<Pharma_company_nameDTO> >mapOfPharma_company_nameDTOTonameEn;
	Map<String, Set<Pharma_company_nameDTO> >mapOfPharma_company_nameDTOTonameBn;
	Map<String, Set<Pharma_company_nameDTO> >mapOfPharma_company_nameDTOToaddress;
	Map<String, Set<Pharma_company_nameDTO> >mapOfPharma_company_nameDTOTocontact;
	Map<String, Set<Pharma_company_nameDTO> >mapOfPharma_company_nameDTOToemail;
	Map<String, Set<Pharma_company_nameDTO> >mapOfPharma_company_nameDTOTosearchColumn;
	Map<Long, Set<Pharma_company_nameDTO> >mapOfPharma_company_nameDTOTolastModificationTime;


	static Pharma_company_nameRepository instance = null;  
	private Pharma_company_nameRepository(){
		mapOfPharma_company_nameDTOToiD = new ConcurrentHashMap<>();
		mapOfPharma_company_nameDTOTonameEn = new ConcurrentHashMap<>();
		mapOfPharma_company_nameDTOTonameBn = new ConcurrentHashMap<>();
		mapOfPharma_company_nameDTOToaddress = new ConcurrentHashMap<>();
		mapOfPharma_company_nameDTOTocontact = new ConcurrentHashMap<>();
		mapOfPharma_company_nameDTOToemail = new ConcurrentHashMap<>();
		mapOfPharma_company_nameDTOTosearchColumn = new ConcurrentHashMap<>();
		mapOfPharma_company_nameDTOTolastModificationTime = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Pharma_company_nameRepository getInstance(){
		if (instance == null){
			instance = new Pharma_company_nameRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(pharma_company_nameDAO == null)
		{
			return;
		}
		try {
			List<Pharma_company_nameDTO> pharma_company_nameDTOs = pharma_company_nameDAO.getAllPharma_company_name(reloadAll);
			for(Pharma_company_nameDTO pharma_company_nameDTO : pharma_company_nameDTOs) {
				Pharma_company_nameDTO oldPharma_company_nameDTO = getPharma_company_nameDTOByID(pharma_company_nameDTO.iD);
				if( oldPharma_company_nameDTO != null ) {
					mapOfPharma_company_nameDTOToiD.remove(oldPharma_company_nameDTO.iD);
				
					if(mapOfPharma_company_nameDTOTonameEn.containsKey(oldPharma_company_nameDTO.nameEn)) {
						mapOfPharma_company_nameDTOTonameEn.get(oldPharma_company_nameDTO.nameEn).remove(oldPharma_company_nameDTO);
					}
					if(mapOfPharma_company_nameDTOTonameEn.get(oldPharma_company_nameDTO.nameEn).isEmpty()) {
						mapOfPharma_company_nameDTOTonameEn.remove(oldPharma_company_nameDTO.nameEn);
					}
					
					if(mapOfPharma_company_nameDTOTonameBn.containsKey(oldPharma_company_nameDTO.nameBn)) {
						mapOfPharma_company_nameDTOTonameBn.get(oldPharma_company_nameDTO.nameBn).remove(oldPharma_company_nameDTO);
					}
					if(mapOfPharma_company_nameDTOTonameBn.get(oldPharma_company_nameDTO.nameBn).isEmpty()) {
						mapOfPharma_company_nameDTOTonameBn.remove(oldPharma_company_nameDTO.nameBn);
					}
					
					if(mapOfPharma_company_nameDTOToaddress.containsKey(oldPharma_company_nameDTO.address)) {
						mapOfPharma_company_nameDTOToaddress.get(oldPharma_company_nameDTO.address).remove(oldPharma_company_nameDTO);
					}
					if(mapOfPharma_company_nameDTOToaddress.get(oldPharma_company_nameDTO.address).isEmpty()) {
						mapOfPharma_company_nameDTOToaddress.remove(oldPharma_company_nameDTO.address);
					}
					
					if(mapOfPharma_company_nameDTOTocontact.containsKey(oldPharma_company_nameDTO.contact)) {
						mapOfPharma_company_nameDTOTocontact.get(oldPharma_company_nameDTO.contact).remove(oldPharma_company_nameDTO);
					}
					if(mapOfPharma_company_nameDTOTocontact.get(oldPharma_company_nameDTO.contact).isEmpty()) {
						mapOfPharma_company_nameDTOTocontact.remove(oldPharma_company_nameDTO.contact);
					}
					
					if(mapOfPharma_company_nameDTOToemail.containsKey(oldPharma_company_nameDTO.email)) {
						mapOfPharma_company_nameDTOToemail.get(oldPharma_company_nameDTO.email).remove(oldPharma_company_nameDTO);
					}
					if(mapOfPharma_company_nameDTOToemail.get(oldPharma_company_nameDTO.email).isEmpty()) {
						mapOfPharma_company_nameDTOToemail.remove(oldPharma_company_nameDTO.email);
					}
					
					if(mapOfPharma_company_nameDTOTosearchColumn.containsKey(oldPharma_company_nameDTO.searchColumn)) {
						mapOfPharma_company_nameDTOTosearchColumn.get(oldPharma_company_nameDTO.searchColumn).remove(oldPharma_company_nameDTO);
					}
					if(mapOfPharma_company_nameDTOTosearchColumn.get(oldPharma_company_nameDTO.searchColumn).isEmpty()) {
						mapOfPharma_company_nameDTOTosearchColumn.remove(oldPharma_company_nameDTO.searchColumn);
					}
					
					if(mapOfPharma_company_nameDTOTolastModificationTime.containsKey(oldPharma_company_nameDTO.lastModificationTime)) {
						mapOfPharma_company_nameDTOTolastModificationTime.get(oldPharma_company_nameDTO.lastModificationTime).remove(oldPharma_company_nameDTO);
					}
					if(mapOfPharma_company_nameDTOTolastModificationTime.get(oldPharma_company_nameDTO.lastModificationTime).isEmpty()) {
						mapOfPharma_company_nameDTOTolastModificationTime.remove(oldPharma_company_nameDTO.lastModificationTime);
					}
					
					
				}
				if(pharma_company_nameDTO.isDeleted == 0) 
				{
					
					mapOfPharma_company_nameDTOToiD.put(pharma_company_nameDTO.iD, pharma_company_nameDTO);
				
					if( ! mapOfPharma_company_nameDTOTonameEn.containsKey(pharma_company_nameDTO.nameEn)) {
						mapOfPharma_company_nameDTOTonameEn.put(pharma_company_nameDTO.nameEn, new HashSet<>());
					}
					mapOfPharma_company_nameDTOTonameEn.get(pharma_company_nameDTO.nameEn).add(pharma_company_nameDTO);
					
					if( ! mapOfPharma_company_nameDTOTonameBn.containsKey(pharma_company_nameDTO.nameBn)) {
						mapOfPharma_company_nameDTOTonameBn.put(pharma_company_nameDTO.nameBn, new HashSet<>());
					}
					mapOfPharma_company_nameDTOTonameBn.get(pharma_company_nameDTO.nameBn).add(pharma_company_nameDTO);
					
					if( ! mapOfPharma_company_nameDTOToaddress.containsKey(pharma_company_nameDTO.address)) {
						mapOfPharma_company_nameDTOToaddress.put(pharma_company_nameDTO.address, new HashSet<>());
					}
					mapOfPharma_company_nameDTOToaddress.get(pharma_company_nameDTO.address).add(pharma_company_nameDTO);
					
					if( ! mapOfPharma_company_nameDTOTocontact.containsKey(pharma_company_nameDTO.contact)) {
						mapOfPharma_company_nameDTOTocontact.put(pharma_company_nameDTO.contact, new HashSet<>());
					}
					mapOfPharma_company_nameDTOTocontact.get(pharma_company_nameDTO.contact).add(pharma_company_nameDTO);
					
					if( ! mapOfPharma_company_nameDTOToemail.containsKey(pharma_company_nameDTO.email)) {
						mapOfPharma_company_nameDTOToemail.put(pharma_company_nameDTO.email, new HashSet<>());
					}
					mapOfPharma_company_nameDTOToemail.get(pharma_company_nameDTO.email).add(pharma_company_nameDTO);
					
					if( ! mapOfPharma_company_nameDTOTosearchColumn.containsKey(pharma_company_nameDTO.searchColumn)) {
						mapOfPharma_company_nameDTOTosearchColumn.put(pharma_company_nameDTO.searchColumn, new HashSet<>());
					}
					mapOfPharma_company_nameDTOTosearchColumn.get(pharma_company_nameDTO.searchColumn).add(pharma_company_nameDTO);
					
					if( ! mapOfPharma_company_nameDTOTolastModificationTime.containsKey(pharma_company_nameDTO.lastModificationTime)) {
						mapOfPharma_company_nameDTOTolastModificationTime.put(pharma_company_nameDTO.lastModificationTime, new HashSet<>());
					}
					mapOfPharma_company_nameDTOTolastModificationTime.get(pharma_company_nameDTO.lastModificationTime).add(pharma_company_nameDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Pharma_company_nameDTO> getPharma_company_nameList() {
		List <Pharma_company_nameDTO> pharma_company_names = new ArrayList<Pharma_company_nameDTO>(this.mapOfPharma_company_nameDTOToiD.values());
		return pharma_company_names;
	}
	
	
	public Pharma_company_nameDTO getPharma_company_nameDTOByID( long ID){
		return mapOfPharma_company_nameDTOToiD.get(ID);
	}
	
	
	public List<Pharma_company_nameDTO> getPharma_company_nameDTOByname_en(String name_en) {
		return new ArrayList<>( mapOfPharma_company_nameDTOTonameEn.getOrDefault(name_en,new HashSet<>()));
	}
	
	
	public List<Pharma_company_nameDTO> getPharma_company_nameDTOByname_bn(String name_bn) {
		return new ArrayList<>( mapOfPharma_company_nameDTOTonameBn.getOrDefault(name_bn,new HashSet<>()));
	}
	
	
	public List<Pharma_company_nameDTO> getPharma_company_nameDTOByaddress(String address) {
		return new ArrayList<>( mapOfPharma_company_nameDTOToaddress.getOrDefault(address,new HashSet<>()));
	}
	
	
	public List<Pharma_company_nameDTO> getPharma_company_nameDTOBycontact(String contact) {
		return new ArrayList<>( mapOfPharma_company_nameDTOTocontact.getOrDefault(contact,new HashSet<>()));
	}
	
	
	public List<Pharma_company_nameDTO> getPharma_company_nameDTOByemail(String email) {
		return new ArrayList<>( mapOfPharma_company_nameDTOToemail.getOrDefault(email,new HashSet<>()));
	}
	
	
	public List<Pharma_company_nameDTO> getPharma_company_nameDTOBysearch_column(String search_column) {
		return new ArrayList<>( mapOfPharma_company_nameDTOTosearchColumn.getOrDefault(search_column,new HashSet<>()));
	}
	
	
	public List<Pharma_company_nameDTO> getPharma_company_nameDTOBylastModificationTime(long lastModificationTime) {
		return new ArrayList<>( mapOfPharma_company_nameDTOTolastModificationTime.getOrDefault(lastModificationTime,new HashSet<>()));
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "pharma_company_name";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


