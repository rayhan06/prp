package disciplinary_action;

import common.ConnectionAndStatementUtil;
import common.EmployeeCommonDAOService;
import org.apache.log4j.Logger;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings({"rawtypes", "unused"})
public class Disciplinary_actionDAO implements EmployeeCommonDAOService<Disciplinary_actionDTO> {

    private static final Logger logger = Logger.getLogger(Disciplinary_actionDAO.class);

    private static final String addSqlQuery = "INSERT INTO {tableName} (disciplinary_log_incident_number,works_done,findings,prevention_advice,"
            .concat(" files_dropzone,insertion_date,inserted_by,modified_by,lastModificationTime,isDeleted,ID)")
            .concat(" VALUES(?,?,?,?,?,?,?,?,?,?,?)");

    private static final String updateSqlQuery = "UPDATE {tableName} SET disciplinary_log_incident_number=?,works_done=?,findings=?, "
            .concat(" prevention_advice=?,files_dropzone=?,insertion_date=?,inserted_by=?,modified_by=?,lastModificationTime =? WHERE ID = ?");

    private static final String getDTOByIncidentNumber = "SELECT * FROM %s WHERE disciplinary_log_incident_number= ? AND isDeleted = 0";

    private final Map<String, String> searchMap = new HashMap<>();

    private Disciplinary_actionDAO() {
    }

    private static class LazyLoader {
        static final Disciplinary_actionDAO INSTANCE = new Disciplinary_actionDAO();
    }

    public static Disciplinary_actionDAO getInstance() {
        return Disciplinary_actionDAO.LazyLoader.INSTANCE;
    }

    public void set(PreparedStatement ps, Disciplinary_actionDTO disciplinary_actionDTO, boolean isInsert) throws SQLException {
        int index = 0;
        ps.setObject(++index, disciplinary_actionDTO.disciplinaryLogIncidentNumber);
        ps.setObject(++index, disciplinary_actionDTO.worksDone);
        ps.setObject(++index, disciplinary_actionDTO.findings);
        ps.setObject(++index, disciplinary_actionDTO.preventionAdvice);
        ps.setObject(++index, disciplinary_actionDTO.filesDropzone);
        ps.setObject(++index, disciplinary_actionDTO.insertionDate);
        ps.setObject(++index, disciplinary_actionDTO.insertedBy);
        ps.setObject(++index, disciplinary_actionDTO.modifiedBy);
        ps.setObject(++index, System.currentTimeMillis());
        if (isInsert) {
            ps.setObject(++index, 0);
        }
        ps.setObject(++index, disciplinary_actionDTO.iD);
    }

    @Override
    public Disciplinary_actionDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Disciplinary_actionDTO disciplinary_actionDTO = new Disciplinary_actionDTO();
            disciplinary_actionDTO.iD = rs.getLong("ID");
            disciplinary_actionDTO.disciplinaryLogIncidentNumber = rs.getString("disciplinary_log_incident_number");
            disciplinary_actionDTO.worksDone = rs.getString("works_done");
            disciplinary_actionDTO.findings = rs.getString("findings");
            disciplinary_actionDTO.preventionAdvice = rs.getString("prevention_advice");
            disciplinary_actionDTO.filesDropzone = rs.getLong("files_dropzone");
            disciplinary_actionDTO.insertionDate = rs.getLong("insertion_date");
            disciplinary_actionDTO.insertedBy = rs.getString("inserted_by");
            disciplinary_actionDTO.modifiedBy = rs.getString("modified_by");
            disciplinary_actionDTO.isDeleted = rs.getInt("isDeleted");
            disciplinary_actionDTO.lastModificationTime = rs.getLong("lastModificationTime");
            return disciplinary_actionDTO;
        } catch (Exception ex) {
            logger.error(ex);
            return null;
        }
    }

    @Override
    public String getTableName() {
        return "disciplinary_action";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }


    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Disciplinary_actionDTO) commonDTO, addSqlQuery, true);
    }

    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Disciplinary_actionDTO) commonDTO, updateSqlQuery, false);
    }

    public CommonDTO getDTOByID(long ID) throws Exception {
        return getDTOFromID(ID);
    }

    public List<Disciplinary_actionDTO> getDTOsByIncidentNumber(String logId) {
        String sql = String.format(getDTOByIncidentNumber, getTableName());
        return ConnectionAndStatementUtil.getListOfT(sql, Collections.singletonList(logId), this::buildObjectFromResultSet);
    }

    public Disciplinary_actionDTO getDTObyIncidentNumber(String logId) {
        List<Disciplinary_actionDTO> tmplist = getDTOsByIncidentNumber(logId);
        if (tmplist == null || tmplist.size() == 0)
            return null;
        else
            return tmplist.get(0);
    }

    public long getActionIdbyIncidentNumber(String logId) {
        if (getDTObyIncidentNumber(logId) == null)
            return -1;//Invalid Value
        else
            return getDTObyIncidentNumber(logId).iD;
    }
}
	