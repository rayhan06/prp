package disciplinary_action;

import disciplinary_details.Disciplinary_detailsDTO;
import employee_offices.EmployeeModel;

public class EmployeeDisciplinaryModel {
    public EmployeeModel empOfficeDesignationModel;
    public Disciplinary_detailsDTO disciplinary_detailsDTO;

    @Override
    public String toString() {
        return "EmployeeDisciplinaryModel{" +
                "empOfficeDesignationModel=" + empOfficeDesignationModel +
                ", disciplinary_detailsDTO=" + disciplinary_detailsDTO +
                '}';
    }
}
