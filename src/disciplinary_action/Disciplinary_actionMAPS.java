package disciplinary_action;
import java.util.*; 
import util.*;


public class Disciplinary_actionMAPS extends CommonMaps
{	
	public Disciplinary_actionMAPS(String tableName)
	{
		

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("disciplinaryLogIncidentNumber".toLowerCase(), "disciplinaryLogIncidentNumber".toLowerCase());
		java_DTO_map.put("worksDone".toLowerCase(), "worksDone".toLowerCase());
		java_DTO_map.put("findings".toLowerCase(), "findings".toLowerCase());
		java_DTO_map.put("preventionAdvice".toLowerCase(), "preventionAdvice".toLowerCase());
		java_DTO_map.put("filesDropzone".toLowerCase(), "filesDropzone".toLowerCase());
		java_DTO_map.put("insertionDate".toLowerCase(), "insertionDate".toLowerCase());
		java_DTO_map.put("insertedBy".toLowerCase(), "insertedBy".toLowerCase());
		java_DTO_map.put("modifiedBy".toLowerCase(), "modifiedBy".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("disciplinary_log_incident_number".toLowerCase(), "disciplinaryLogIncidentNumber".toLowerCase());
		java_SQL_map.put("works_done".toLowerCase(), "worksDone".toLowerCase());
		java_SQL_map.put("findings".toLowerCase(), "findings".toLowerCase());
		java_SQL_map.put("prevention_advice".toLowerCase(), "preventionAdvice".toLowerCase());
		java_SQL_map.put("files_dropzone".toLowerCase(), "filesDropzone".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Disciplinary Log Incident Number".toLowerCase(), "disciplinaryLogIncidentNumber".toLowerCase());
		java_Text_map.put("Works Done".toLowerCase(), "worksDone".toLowerCase());
		java_Text_map.put("Findings".toLowerCase(), "findings".toLowerCase());
		java_Text_map.put("Prevention Advice".toLowerCase(), "preventionAdvice".toLowerCase());
		java_Text_map.put("Files Dropzone".toLowerCase(), "filesDropzone".toLowerCase());
		java_Text_map.put("Insertion Date".toLowerCase(), "insertionDate".toLowerCase());
		java_Text_map.put("Inserted By".toLowerCase(), "insertedBy".toLowerCase());
		java_Text_map.put("Modified By".toLowerCase(), "modifiedBy".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}