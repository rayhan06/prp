package disciplinary_action;

import util.CommonDTO;


public class Disciplinary_actionDTO extends CommonDTO {

    public String disciplinaryLogIncidentNumber = "";
    public String worksDone = "";
    public String findings = "";
    public String preventionAdvice = "";
    public long filesDropzone = 0;
    public long insertionDate = 0;
    public String insertedBy = "";
    public String modifiedBy = "";


    @Override
    public String toString() {
        return "$Disciplinary_actionDTO[" +
                " iD = " + iD +
                " disciplinaryLogIncidentNumber = " + disciplinaryLogIncidentNumber +
                " worksDone = " + worksDone +
                " findings = " + findings +
                " preventionAdvice = " + preventionAdvice +
                " filesDropzone = " + filesDropzone +
                " insertionDate = " + insertionDate +
                " insertedBy = " + insertedBy +
                " modifiedBy = " + modifiedBy +
                " isDeleted = " + isDeleted +
                " lastModificationTime = " + lastModificationTime +
                "]";
    }

}