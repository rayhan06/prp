package disciplinary_action;

import common.BaseServlet;
import disciplinary_details.DetailsDTOModel;
import disciplinary_details.Disciplinary_detailsDAO;
import disciplinary_details.Disciplinary_detailsDTO;
import disciplinary_log.Disciplinary_logDAO;
import disciplinary_log.Disciplinary_logDTO;
import login.LoginDTO;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import permission.MenuConstants;
import role.PermissionRepository;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.CommonConstant;
import util.CommonDTO;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@WebServlet("/Disciplinary_actionServlet")
@MultipartConfig
public class Disciplinary_actionServlet extends BaseServlet {
    private static final long serialVersionUID = 1L;
    private static final Logger logger = Logger.getLogger(Disciplinary_actionServlet.class);
    private static final String tableName = "disciplinary_action";
    private final Disciplinary_actionDAO disciplinary_actionDAO = Disciplinary_actionDAO.getInstance();

    @Override
    public String getTableName() {
        return tableName;
    }

    @Override
    public String getServletName() {
        return "Disciplinary_actionServlet";
    }

    @Override
    public Disciplinary_actionDAO getCommonDAOService() {
        return disciplinary_actionDAO;
    }

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        boolean isLangEng = userDTO.languageID == CommonConstant.Language_ID_English;
        Disciplinary_actionDTO disciplinary_actionDTO;
        String Value;
        String incidentNumber = "";
        Value = request.getParameter("disciplinaryLogIncidentNumber");
        long curTime = System.currentTimeMillis();
        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
            incidentNumber = Value;
        }
        if (addFlag) {
            disciplinary_actionDTO = new Disciplinary_actionDTO();
            disciplinary_actionDTO.insertionDate = curTime;
            disciplinary_actionDTO.insertedBy = String.valueOf(userDTO.ID);
        } else {
            disciplinary_actionDTO = disciplinary_actionDAO.getDTObyIncidentNumber(incidentNumber);
            if (disciplinary_actionDTO == null)
                throw new Exception(
                        isLangEng ? "No Record with this Incident Number"
                                : "এ ঘটনা সংখার জন্য কোন তথ্য পাওয়া যায়নি"
                );
        }
        disciplinary_actionDTO.modifiedBy = String.valueOf(userDTO.ID);
        disciplinary_actionDTO.lastModificationTime = curTime;
        disciplinary_actionDTO.disciplinaryLogIncidentNumber = incidentNumber;
        disciplinary_actionDTO.worksDone = Jsoup.clean(request.getParameter("worksDone"), Whitelist.simpleText());
        disciplinary_actionDTO.findings = Jsoup.clean(request.getParameter("findings"), Whitelist.simpleText());
        disciplinary_actionDTO.preventionAdvice = Jsoup.clean(request.getParameter("preventionAdvice"), Whitelist.simpleText());
        Value = request.getParameter("filesDropzone");
        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }
        if (Value != null && !Value.equalsIgnoreCase("")) {
            if (!Value.equalsIgnoreCase("")) {
                disciplinary_actionDTO.filesDropzone = Long.parseLong(Value);
            }
            if (!addFlag) {
                String filesDropzoneFilesToDelete = request.getParameter("filesDropzoneFilesToDelete");
                String[] deleteArray = filesDropzoneFilesToDelete.split(",");
                for (int i = 0; i < deleteArray.length; i++) {
                    if (i > 0) {
                        filesDAO.delete(Long.parseLong(deleteArray[i]));
                    }
                }
            }
        }
        int complainStatus = 0;
        Value = request.getParameter("complaintStatusCat");
        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
            complainStatus = Integer.parseInt(Value);
        }

        Value = request.getParameter("disciplinaryDetails");
        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }

        List<DetailsDTOModel> detailsDTOModelList = Arrays.asList(gson.fromJson(Value, DetailsDTOModel[].class));
        List<Disciplinary_detailsDTO> detailsDTOList = detailsDTOModelList.stream()
                .map(e -> {
                    try {
                        return e.buildJson();
                    } catch (ParseException parseException) {
                        parseException.printStackTrace();
                        return null;
                    }
                })
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
        if (addFlag)
            disciplinary_actionDAO.add(disciplinary_actionDTO);
        else
            disciplinary_actionDAO.update(disciplinary_actionDTO);
        new Disciplinary_detailsDAO().updateDisciplinaryTableAfterActionEdit(detailsDTOList, userDTO.userName, curTime);
        Disciplinary_logDAO.getInstance().updateComplainStatus(incidentNumber, complainStatus, userDTO.userName, curTime);
        return disciplinary_actionDTO;
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[]{MenuConstants.DISCIPLINARY_ACTION_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[]{MenuConstants.DISCIPLINARY_ACTION_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[]{MenuConstants.DISCIPLINARY_ACTION_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Disciplinary_actionServlet.class;
    }

    @Override
    public String getAjaxAddRedirectURL(HttpServletRequest request, CommonDTO commonDTO) {
        return "Disciplinary_logServlet?actionType=search";
    }

    @Override
    public String getAjaxEditRedirectURL(HttpServletRequest request, CommonDTO commonDTO) {
        return "Disciplinary_logServlet?actionType=search";
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }
        try {
            String actionType = request.getParameter("actionType");
            switch (actionType) {
                case "getAddPage":
                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.DISCIPLINARY_ACTION_ADD)) {
                        List<Disciplinary_detailsDTO> disciplinary_detailsDTOList = loadAccusedListFromDetailsTable(request);
                        request.setAttribute("disciplinary_detailsDTOList", disciplinary_detailsDTOList);
                        request.getRequestDispatcher("disciplinary_action/disciplinary_actionEdit.jsp?actionType=add").forward(request, response);
                        return;
                    }
                    break;
                case "getEditPage":
                    if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.DISCIPLINARY_ACTION_UPDATE)) {
                        List<Disciplinary_detailsDTO> disciplinary_detailsDTOList = loadAccusedListFromDetailsTable(request);
                        request.setAttribute("disciplinary_detailsDTOList", disciplinary_detailsDTOList);
                        getDisciplinary_action(request, response);
                        return;
                    }
                    break;
                default:
                    super.doGet(request, response);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error(ex);
        }
    }

    private void getDisciplinary_action(HttpServletRequest request, HttpServletResponse response, String incidentNumber) throws ServletException, IOException {
        Disciplinary_actionDTO disciplinary_actionDTO = null;
        Disciplinary_logDTO disciplinary_logDTO = null;
        if (incidentNumber != null) {
            disciplinary_actionDTO = disciplinary_actionDAO.getDTObyIncidentNumber(incidentNumber);
            disciplinary_logDTO = Disciplinary_logDAO.getInstance().getDTOByIncidentNumber(incidentNumber);
        }
        if (disciplinary_actionDTO != null)
            request.setAttribute("ID", disciplinary_actionDTO.iD);
        request.setAttribute("complaintStatusCat", disciplinary_logDTO.complaintStatusCat);
        request.setAttribute("disciplinary_actionDTO", disciplinary_actionDTO);
        request.setAttribute("disciplinary_actionDAO", disciplinary_actionDAO);
        request.getRequestDispatcher("disciplinary_action/disciplinary_actionEdit.jsp?actionType=edit").forward(request, response);
    }

    private void getDisciplinary_action(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String incidentNumber = request.getParameter("incidentNumber");
        if (incidentNumber == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
        }

        getDisciplinary_action(request, response, incidentNumber);
    }

    private List<Disciplinary_detailsDTO> loadAccusedListFromDetailsTable(HttpServletRequest request) {
        String incidentNumber = request.getParameter("incidentNumber");
        if (incidentNumber == null) return null;
        return new Disciplinary_detailsDAO().getByForeignKey(incidentNumber);
    }
}

