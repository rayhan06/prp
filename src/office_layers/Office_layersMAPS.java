package office_layers;
import java.util.*; 


public class Office_layersMAPS 
{

	public HashMap<String, String> java_allfield_type_map = new HashMap<String, String>();
	public HashMap<String, String> java_anyfield_search_map = new HashMap<String, String>();
	public HashMap<String, String> java_DTO_map = new HashMap<String, String>();
	public HashMap<String, String> java_SQL_map = new HashMap<String, String>();
	public HashMap<String, String> java_Text_map = new HashMap<String, String>();
	
	private static Office_layersMAPS self = null;
	
	private Office_layersMAPS()
	{
		
		java_allfield_type_map.put("office_ministry_id".toLowerCase(), "Integer");
		java_allfield_type_map.put("parent_layer_id".toLowerCase(), "Integer");
		java_allfield_type_map.put("custom_layer_id".toLowerCase(), "Integer");
		java_allfield_type_map.put("layer_name_eng".toLowerCase(), "String");
		java_allfield_type_map.put("layer_name_bng".toLowerCase(), "String");
		java_allfield_type_map.put("layer_level".toLowerCase(), "Integer");
		java_allfield_type_map.put("layer_sequence".toLowerCase(), "Integer");
		java_allfield_type_map.put("status".toLowerCase(), "Boolean");

		java_anyfield_search_map.put("office_custom_layers.name".toLowerCase(), "String");
		java_anyfield_search_map.put("office_custom_layers.name".toLowerCase(), "String");
		java_anyfield_search_map.put("office_layers.layer_name_eng".toLowerCase(), "String");
		java_anyfield_search_map.put("office_layers.layer_name_bng".toLowerCase(), "String");
		java_anyfield_search_map.put("office_layers.layer_level".toLowerCase(), "Integer");
		java_anyfield_search_map.put("office_layers.layer_sequence".toLowerCase(), "Integer");
		java_anyfield_search_map.put("office_layers.status".toLowerCase(), "Boolean");

		java_DTO_map.put("id".toLowerCase(), "id".toLowerCase());
		java_DTO_map.put("officeMinistryId".toLowerCase(), "officeMinistryId".toLowerCase());
		java_DTO_map.put("parentLayerId".toLowerCase(), "parentLayerId".toLowerCase());
		java_DTO_map.put("customLayerId".toLowerCase(), "customLayerId".toLowerCase());
		java_DTO_map.put("layerNameEng".toLowerCase(), "layerNameEng".toLowerCase());
		java_DTO_map.put("layerNameBng".toLowerCase(), "layerNameBng".toLowerCase());
		java_DTO_map.put("layerLevel".toLowerCase(), "layerLevel".toLowerCase());
		java_DTO_map.put("layerSequence".toLowerCase(), "layerSequence".toLowerCase());
		java_DTO_map.put("status".toLowerCase(), "status".toLowerCase());
		java_DTO_map.put("createdBy".toLowerCase(), "createdBy".toLowerCase());
		java_DTO_map.put("modifiedBy".toLowerCase(), "modifiedBy".toLowerCase());
		java_DTO_map.put("created".toLowerCase(), "created".toLowerCase());
		java_DTO_map.put("modified".toLowerCase(), "modified".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());

		java_SQL_map.put("office_ministry_id".toLowerCase(), "officeMinistryId".toLowerCase());
		java_SQL_map.put("parent_layer_id".toLowerCase(), "parentLayerId".toLowerCase());
		java_SQL_map.put("custom_layer_id".toLowerCase(), "customLayerId".toLowerCase());
		java_SQL_map.put("layer_name_eng".toLowerCase(), "layerNameEng".toLowerCase());
		java_SQL_map.put("layer_name_bng".toLowerCase(), "layerNameBng".toLowerCase());
		java_SQL_map.put("layer_level".toLowerCase(), "layerLevel".toLowerCase());
		java_SQL_map.put("layer_sequence".toLowerCase(), "layerSequence".toLowerCase());
		java_SQL_map.put("status".toLowerCase(), "status".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "id".toLowerCase());
		java_Text_map.put("Office Ministry Id".toLowerCase(), "officeMinistryId".toLowerCase());
		java_Text_map.put("Parent Layer Id".toLowerCase(), "parentLayerId".toLowerCase());
		java_Text_map.put("Custom Layer Id".toLowerCase(), "customLayerId".toLowerCase());
		java_Text_map.put("Layer Name Eng".toLowerCase(), "layerNameEng".toLowerCase());
		java_Text_map.put("Layer Name Bng".toLowerCase(), "layerNameBng".toLowerCase());
		java_Text_map.put("Layer Level".toLowerCase(), "layerLevel".toLowerCase());
		java_Text_map.put("Layer Sequence".toLowerCase(), "layerSequence".toLowerCase());
		java_Text_map.put("Status".toLowerCase(), "status".toLowerCase());
		java_Text_map.put("Created By".toLowerCase(), "createdBy".toLowerCase());
		java_Text_map.put("Modified By".toLowerCase(), "modifiedBy".toLowerCase());
		java_Text_map.put("Created".toLowerCase(), "created".toLowerCase());
		java_Text_map.put("Modified".toLowerCase(), "modified".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
			
	}
	
	public static Office_layersMAPS GetInstance()
	{
		if(self == null)
		{
			self = new Office_layersMAPS ();
		}
		return self;
	}
	

}