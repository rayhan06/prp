/*
package office_layers;

import java.io.IOException;
import java.io.*;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import org.apache.log4j.Logger;

import login.LoginDTO;
import org.json.JSONObject;
import permission.MenuConstants;
import role.PermissionRepository;


import sessionmanager.SessionConstants;

import user.UserDTO;
import user.UserRepository;

import util.RecordNavigationManager;

import java.util.*;
import javax.servlet.http.*;
import java.util.UUID;

*/
/**
 * Servlet implementation class Office_layersServlet
 *//*

@WebServlet("/Office_layersServlet")
@MultipartConfig
public class Office_layersServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Office_layersServlet.class);

    */
/**
     * @see HttpServlet#HttpServlet()
     *//*

    public Office_layersServlet() {
        super();
    }

    */
/**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     *//*

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("In doget request = " + request);
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getInstance().getUserDtoByUserId(loginDTO.userID);
        try {
            String actionType = request.getParameter("actionType");
            if (actionType.equals("getAddPage")) {
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.OFFICE_LAYERS_ADD)) {
                    getAddPage(request, response);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else if (actionType.equals("getEditPage")) {
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.OFFICE_LAYERS_UPDATE)) {
                    getOffice_layers(request, response);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else if (actionType.equals("getURL")) {
                String URL = request.getParameter("URL");
                System.out.println("URL = " + URL);
                response.sendRedirect(URL);
            } else if (actionType.equals("search")) {
                System.out.println("search requested");
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.OFFICE_LAYERS_SEARCH)) {
                    searchOffice_layers(request, response);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else if (actionType.equals("getCustomLayerDetails")) {
                response.setContentType("application/json");

                String customLayerId = (String) request.getParameter("customLayerId");
                int layerId = Integer.valueOf(customLayerId);

                //String res = _gson.toJson(obj);
                Map<String, String> res = new Office_layersDAO().getCustomLayerDetails(layerId);

                PrintWriter out = response.getWriter();

                out.print(new JSONObject(res));
                out.flush();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.debug(ex);
        }
    }

    private void getAddPage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("ID", -1L);
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("office_layers/office_layersEdit.jsp");
        requestDispatcher.forward(request, response);
    }

    private String getFileName(final Part part) {
        final String partHeader = part.getHeader("content-disposition");
        System.out.println("Part Header = {0}" + partHeader);
        for (String content : part.getHeader("content-disposition").split(";")) {
            if (content.trim().startsWith("filename")) {
                return content.substring(
                        content.indexOf('=') + 1).trim().replace("\"", "");
            }
        }
        return null;
    }


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getInstance().getUserDtoByUserId(loginDTO.userID);
        System.out.println("doPost");

        try {
            String actionType = request.getParameter("actionType");
            System.out.println("actionType = " + actionType);
            if (actionType.equals("add")) {

                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.OFFICE_LAYERS_ADD)) {
                    System.out.println("going to  addOffice_layers ");
                    addOffice_layers(request, response, true);
                } else {
                    System.out.println("Not going to  addOffice_layers ");
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }

            } else if (actionType.equals("edit")) {

                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.OFFICE_LAYERS_UPDATE)) {
                    addOffice_layers(request, response, false);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else if (actionType.equals("delete")) {
                deleteOffice_layers(request, response);
            } else if (actionType.equals("search")) {
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.OFFICE_LAYERS_SEARCH)) {
                    searchOffice_layers(request, response);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else if (actionType.equals("getGeo")) {
                System.out.println("going to geoloc ");
                request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            logger.debug(ex);
        }
    }

    private void addOffice_layers(HttpServletRequest request, HttpServletResponse response, Boolean addFlag) throws IOException {
        // TODO Auto-generated method stub
        try {
            request.setAttribute("failureMessage", "");
            System.out.println("%%%% addOffice_layers");
            String path = getServletContext().getRealPath("/img2/");
            Office_layersDAO office_layersDAO = new Office_layersDAO();
            Office_layersDTO office_layersDTO;
            String FileNamePrefix;
            if (addFlag == true) {
                office_layersDTO = new Office_layersDTO();
                FileNamePrefix = UUID.randomUUID().toString().substring(0, 10);
            } else {
                office_layersDTO = office_layersDAO.getOffice_layersDTOByID(Long.parseLong(request.getParameter("identity")));
                FileNamePrefix = request.getParameter("identity");
            }

            String Value = "";
            Value = request.getParameter("officeMinistryId");
            System.out.println("officeMinistryId = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                office_layersDTO.officeMinistryId = Integer.parseInt(Value);
            } else {
                System.out.println("FieldName has a null value, not updating" + " = " + Value);
            }
            Value = request.getParameter("parentLayerId");
            System.out.println("parentLayerId = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                office_layersDTO.parentLayerId = Integer.parseInt(Value);
                if (office_layersDTO.parentLayerId < 0) office_layersDTO.parentLayerId = 0;
            } else {
                System.out.println("FieldName has a null value, not updating" + " = " + Value);
            }
            Value = request.getParameter("customLayerId");
            System.out.println("customLayerId = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                office_layersDTO.customLayerId = Integer.parseInt(Value);
            } else {
                System.out.println("FieldName has a null value, not updating" + " = " + Value);
            }
            Value = request.getParameter("layerNameEng");
            System.out.println("layerNameEng = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                office_layersDTO.layerNameEng = (Value);
            } else {
                System.out.println("FieldName has a null value, not updating" + " = " + Value);
            }
            Value = request.getParameter("layerNameBng");
            System.out.println("layerNameBng = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                office_layersDTO.layerNameBng = (Value);
            } else {
                System.out.println("FieldName has a null value, not updating" + " = " + Value);
            }
            Value = request.getParameter("layerLevel");
            System.out.println("layerLevel = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                office_layersDTO.layerLevel = Integer.parseInt(Value);
            } else {
                System.out.println("FieldName has a null value, not updating" + " = " + Value);
            }
            Value = request.getParameter("layerSequence");
            System.out.println("layerSequence = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                office_layersDTO.layerSequence = Integer.parseInt(Value);
            } else {
                System.out.println("FieldName has a null value, not updating" + " = " + Value);
            }
            Value = request.getParameter("status");
            System.out.println("status = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                office_layersDTO.status = Boolean.parseBoolean(Value);
            } else {
                office_layersDTO.status = false;
                System.out.println("FieldName has a null value, not updating" + " = " + Value);
            }
            Value = request.getParameter("createdBy");
            System.out.println("createdBy = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                office_layersDTO.createdBy = Integer.parseInt(Value);
            } else {
                System.out.println("FieldName has a null value, not updating" + " = " + Value);
            }
            Value = request.getParameter("modifiedBy");
            System.out.println("modifiedBy = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                office_layersDTO.modifiedBy = Integer.parseInt(Value);
            } else {
                System.out.println("FieldName has a null value, not updating" + " = " + Value);
            }
            Value = request.getParameter("created");
            System.out.println("created = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                office_layersDTO.created = Long.parseLong(Value);
            } else {
                System.out.println("FieldName has a null value, not updating" + " = " + Value);
            }
            Value = request.getParameter("modified");
            System.out.println("modified = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                office_layersDTO.modified = Long.parseLong(Value);
            } else {
                System.out.println("FieldName has a null value, not updating" + " = " + Value);
            }
            Value = request.getParameter("isDeleted");
            System.out.println("isDeleted = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                office_layersDTO.isDeleted = Boolean.parseBoolean(Value);
            } else {
                System.out.println("FieldName has a null value, not updating" + " = " + Value);
            }

            System.out.println("Done adding  addOffice_layers dto = " + office_layersDTO);

            if (addFlag == true) {
                office_layersDAO.addOffice_layers(office_layersDTO);
            } else {
                office_layersDAO.updateOffice_layers(office_layersDTO);

            }


            String inPlaceSubmit = (String) request.getParameter("inplacesubmit");

            if (inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase("")) {
                getOffice_layers(request, response);
            } else {
                response.sendRedirect("Office_layersServlet?actionType=search&success=true");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void deleteOffice_layers(HttpServletRequest request, HttpServletResponse response) throws IOException {
        try {
            String[] IDsToDelete = request.getParameterValues("ID");
            for (int i = 0; i < IDsToDelete.length; i++) {
                long id = Long.parseLong(IDsToDelete[i]);
                System.out.println("###DELETING " + IDsToDelete[i]);
                new Office_layersDAO().deleteOffice_layersByID(id);
            }
        } catch (Exception ex) {
            logger.debug(ex);
        }
        response.sendRedirect("Office_layersServlet?actionType=search");
    }

    private void getOffice_layers(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("in getOffice_layers");
        Office_layersDTO office_layersDTO = null;
        try {
            office_layersDTO = new Office_layersDAO().getOffice_layersDTOByID(Long.parseLong(request.getParameter("ID")));
            request.setAttribute("ID", office_layersDTO.iD);
            request.setAttribute("office_layersDTO", office_layersDTO);

            String URL = "";

            String inPlaceEdit = (String) request.getParameter("inplaceedit");
            String inPlaceSubmit = (String) request.getParameter("inplacesubmit");
            String getBodyOnly = (String) request.getParameter("getBodyOnly");

            if (inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase("")) {
                URL = "office_layers/office_layersInPlaceEdit.jsp";
                request.setAttribute("inplaceedit", "");
            } else if (inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase("")) {
                URL = "office_layers/office_layersSearchRow.jsp";
                request.setAttribute("inplacesubmit", "");
            } else {
                if (getBodyOnly != null && !getBodyOnly.equalsIgnoreCase("")) {
                    URL = "office_layers/office_layersEditBody.jsp?actionType=edit";
                } else {
                    URL = "office_layers/office_layersEdit.jsp?actionType=edit";
                }
            }

            RequestDispatcher rd = request.getRequestDispatcher(URL);
            rd.forward(request, response);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void searchOffice_layers(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("in  searchOffice_layers 1");
        Office_layersDAO office_layersDAO = new Office_layersDAO();
        LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
        String ajax = (String) request.getParameter("ajax");
        boolean hasAjax = false;
        if (ajax != null && !ajax.equalsIgnoreCase("")) {
            hasAjax = true;
        }
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);

        RecordNavigationManager rnManager = new RecordNavigationManager(SessionConstants.NAV_OFFICE_LAYERS, request, office_layersDAO, SessionConstants.VIEW_OFFICE_LAYERS, SessionConstants.SEARCH_OFFICE_LAYERS);
        try {
            System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        } catch (Exception e) {
            System.out.println("failed to dojob" + e);
        }

        RequestDispatcher rd;
        if (hasAjax == false) {
            System.out.println("Going to office_layers/office_layersSearch.jsp");
            rd = request.getRequestDispatcher("office_layers/office_layersSearch.jsp");
        } else {
            System.out.println("Going to office_layers/office_layersSearchForm.jsp");
            rd = request.getRequestDispatcher("office_layers/office_layersSearchForm.jsp");
        }
        rd.forward(request, response);
    }

}

*/
