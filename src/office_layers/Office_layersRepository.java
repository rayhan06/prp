package office_layers;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;

public class Office_layersRepository implements Repository {
    Office_layersDAO office_layersDAO = new Office_layersDAO();

    static Logger logger = Logger.getLogger(Office_layersRepository.class);
    Map<Long, Office_layersDTO> mapOfOffice_layersDTOToiD;
    Map<Integer, Set<Office_layersDTO>> mapOfOffice_layersDTOToofficeMinistryId;
    Map<Integer, Set<Office_layersDTO>> mapOfOffice_layersDTOToparentLayerId;
    Map<Integer, Set<Office_layersDTO>> mapOfOffice_layersDTOTocustomLayerId;
    Map<String, Set<Office_layersDTO>> mapOfOffice_layersDTOTolayerNameEng;
    Map<String, Set<Office_layersDTO>> mapOfOffice_layersDTOTolayerNameBng;
    Map<Integer, Set<Office_layersDTO>> mapOfOffice_layersDTOTolayerLevel;
    Map<Integer, Set<Office_layersDTO>> mapOfOffice_layersDTOTolayerSequence;
    Map<Boolean, Set<Office_layersDTO>> mapOfOffice_layersDTOTostatus;
    Map<Integer, Set<Office_layersDTO>> mapOfOffice_layersDTOTocreatedBy;
    Map<Integer, Set<Office_layersDTO>> mapOfOffice_layersDTOTomodifiedBy;
    Map<Long, Set<Office_layersDTO>> mapOfOffice_layersDTOTocreated;
    Map<Long, Set<Office_layersDTO>> mapOfOffice_layersDTOTomodified;


    static Office_layersRepository instance = null;

    private Office_layersRepository() {
        mapOfOffice_layersDTOToiD = new ConcurrentHashMap<>();
        mapOfOffice_layersDTOToofficeMinistryId = new ConcurrentHashMap<>();
        mapOfOffice_layersDTOToparentLayerId = new ConcurrentHashMap<>();
        mapOfOffice_layersDTOTocustomLayerId = new ConcurrentHashMap<>();
        mapOfOffice_layersDTOTolayerNameEng = new ConcurrentHashMap<>();
        mapOfOffice_layersDTOTolayerNameBng = new ConcurrentHashMap<>();
        mapOfOffice_layersDTOTolayerLevel = new ConcurrentHashMap<>();
        mapOfOffice_layersDTOTolayerSequence = new ConcurrentHashMap<>();
        mapOfOffice_layersDTOTostatus = new ConcurrentHashMap<>();
        mapOfOffice_layersDTOTocreatedBy = new ConcurrentHashMap<>();
        mapOfOffice_layersDTOTomodifiedBy = new ConcurrentHashMap<>();
        mapOfOffice_layersDTOTocreated = new ConcurrentHashMap<>();
        mapOfOffice_layersDTOTomodified = new ConcurrentHashMap<>();

        RepositoryManager.getInstance().addRepository(this);
    }

    public synchronized static Office_layersRepository getInstance() {
        if (instance == null) {
            instance = new Office_layersRepository();
        }
        return instance;
    }

    public void reload(boolean reloadAll) {
        try {
            List<Office_layersDTO> office_layersDTOs = office_layersDAO.getAllOffice_layers(reloadAll);
            for (Office_layersDTO office_layersDTO : office_layersDTOs) {
                Office_layersDTO oldOffice_layersDTO = getOffice_layersDTOByID(office_layersDTO.iD);
                if (oldOffice_layersDTO != null) {
                    mapOfOffice_layersDTOToiD.remove(oldOffice_layersDTO.iD);

                    if (mapOfOffice_layersDTOToofficeMinistryId.containsKey(oldOffice_layersDTO.officeMinistryId)) {
                        mapOfOffice_layersDTOToofficeMinistryId.get(oldOffice_layersDTO.officeMinistryId).remove(oldOffice_layersDTO);
                    }
                    if (mapOfOffice_layersDTOToofficeMinistryId.get(oldOffice_layersDTO.officeMinistryId).isEmpty()) {
                        mapOfOffice_layersDTOToofficeMinistryId.remove(oldOffice_layersDTO.officeMinistryId);
                    }

                    if (mapOfOffice_layersDTOToparentLayerId.containsKey(oldOffice_layersDTO.parentLayerId)) {
                        mapOfOffice_layersDTOToparentLayerId.get(oldOffice_layersDTO.parentLayerId).remove(oldOffice_layersDTO);
                    }
                    if (mapOfOffice_layersDTOToparentLayerId.get(oldOffice_layersDTO.parentLayerId).isEmpty()) {
                        mapOfOffice_layersDTOToparentLayerId.remove(oldOffice_layersDTO.parentLayerId);
                    }

                    if (mapOfOffice_layersDTOTocustomLayerId.containsKey(oldOffice_layersDTO.customLayerId)) {
                        mapOfOffice_layersDTOTocustomLayerId.get(oldOffice_layersDTO.customLayerId).remove(oldOffice_layersDTO);
                    }
                    if (mapOfOffice_layersDTOTocustomLayerId.get(oldOffice_layersDTO.customLayerId).isEmpty()) {
                        mapOfOffice_layersDTOTocustomLayerId.remove(oldOffice_layersDTO.customLayerId);
                    }

                    if (mapOfOffice_layersDTOTolayerNameEng.containsKey(oldOffice_layersDTO.layerNameEng)) {
                        mapOfOffice_layersDTOTolayerNameEng.get(oldOffice_layersDTO.layerNameEng).remove(oldOffice_layersDTO);
                    }
                    if (mapOfOffice_layersDTOTolayerNameEng.get(oldOffice_layersDTO.layerNameEng).isEmpty()) {
                        mapOfOffice_layersDTOTolayerNameEng.remove(oldOffice_layersDTO.layerNameEng);
                    }

                    if (mapOfOffice_layersDTOTolayerNameBng.containsKey(oldOffice_layersDTO.layerNameBng)) {
                        mapOfOffice_layersDTOTolayerNameBng.get(oldOffice_layersDTO.layerNameBng).remove(oldOffice_layersDTO);
                    }
                    if (mapOfOffice_layersDTOTolayerNameBng.get(oldOffice_layersDTO.layerNameBng).isEmpty()) {
                        mapOfOffice_layersDTOTolayerNameBng.remove(oldOffice_layersDTO.layerNameBng);
                    }

                    if (mapOfOffice_layersDTOTolayerLevel.containsKey(oldOffice_layersDTO.layerLevel)) {
                        mapOfOffice_layersDTOTolayerLevel.get(oldOffice_layersDTO.layerLevel).remove(oldOffice_layersDTO);
                    }
                    if (mapOfOffice_layersDTOTolayerLevel.get(oldOffice_layersDTO.layerLevel).isEmpty()) {
                        mapOfOffice_layersDTOTolayerLevel.remove(oldOffice_layersDTO.layerLevel);
                    }

                    if (mapOfOffice_layersDTOTolayerSequence.containsKey(oldOffice_layersDTO.layerSequence)) {
                        mapOfOffice_layersDTOTolayerSequence.get(oldOffice_layersDTO.layerSequence).remove(oldOffice_layersDTO);
                    }
                    if (mapOfOffice_layersDTOTolayerSequence.get(oldOffice_layersDTO.layerSequence).isEmpty()) {
                        mapOfOffice_layersDTOTolayerSequence.remove(oldOffice_layersDTO.layerSequence);
                    }

                    if (mapOfOffice_layersDTOTostatus.containsKey(oldOffice_layersDTO.status)) {
                        mapOfOffice_layersDTOTostatus.get(oldOffice_layersDTO.status).remove(oldOffice_layersDTO);
                    }
                    if (mapOfOffice_layersDTOTostatus.get(oldOffice_layersDTO.status).isEmpty()) {
                        mapOfOffice_layersDTOTostatus.remove(oldOffice_layersDTO.status);
                    }

                    if (mapOfOffice_layersDTOTocreatedBy.containsKey(oldOffice_layersDTO.createdBy)) {
                        mapOfOffice_layersDTOTocreatedBy.get(oldOffice_layersDTO.createdBy).remove(oldOffice_layersDTO);
                    }
                    if (mapOfOffice_layersDTOTocreatedBy.get(oldOffice_layersDTO.createdBy).isEmpty()) {
                        mapOfOffice_layersDTOTocreatedBy.remove(oldOffice_layersDTO.createdBy);
                    }

                    if (mapOfOffice_layersDTOTomodifiedBy.containsKey(oldOffice_layersDTO.modifiedBy)) {
                        mapOfOffice_layersDTOTomodifiedBy.get(oldOffice_layersDTO.modifiedBy).remove(oldOffice_layersDTO);
                    }
                    if (mapOfOffice_layersDTOTomodifiedBy.get(oldOffice_layersDTO.modifiedBy).isEmpty()) {
                        mapOfOffice_layersDTOTomodifiedBy.remove(oldOffice_layersDTO.modifiedBy);
                    }

                    if (mapOfOffice_layersDTOTocreated.containsKey(oldOffice_layersDTO.created)) {
                        mapOfOffice_layersDTOTocreated.get(oldOffice_layersDTO.created).remove(oldOffice_layersDTO);
                    }
                    if (mapOfOffice_layersDTOTocreated.get(oldOffice_layersDTO.created).isEmpty()) {
                        mapOfOffice_layersDTOTocreated.remove(oldOffice_layersDTO.created);
                    }

                    if (mapOfOffice_layersDTOTomodified.containsKey(oldOffice_layersDTO.modified)) {
                        mapOfOffice_layersDTOTomodified.get(oldOffice_layersDTO.modified).remove(oldOffice_layersDTO);
                    }
                    if (mapOfOffice_layersDTOTomodified.get(oldOffice_layersDTO.modified).isEmpty()) {
                        mapOfOffice_layersDTOTomodified.remove(oldOffice_layersDTO.modified);
                    }


                }
                if (office_layersDTO.isDeleted == false) {

                    mapOfOffice_layersDTOToiD.put(office_layersDTO.iD, office_layersDTO);

                    if (!mapOfOffice_layersDTOToofficeMinistryId.containsKey(office_layersDTO.officeMinistryId)) {
                        mapOfOffice_layersDTOToofficeMinistryId.put(office_layersDTO.officeMinistryId, new HashSet<>());
                    }
                    mapOfOffice_layersDTOToofficeMinistryId.get(office_layersDTO.officeMinistryId).add(office_layersDTO);

                    if (!mapOfOffice_layersDTOToparentLayerId.containsKey(office_layersDTO.parentLayerId)) {
                        mapOfOffice_layersDTOToparentLayerId.put(office_layersDTO.parentLayerId, new HashSet<>());
                    }
                    mapOfOffice_layersDTOToparentLayerId.get(office_layersDTO.parentLayerId).add(office_layersDTO);

                    if (!mapOfOffice_layersDTOTocustomLayerId.containsKey(office_layersDTO.customLayerId)) {
                        mapOfOffice_layersDTOTocustomLayerId.put(office_layersDTO.customLayerId, new HashSet<>());
                    }
                    mapOfOffice_layersDTOTocustomLayerId.get(office_layersDTO.customLayerId).add(office_layersDTO);

                    if (!mapOfOffice_layersDTOTolayerNameEng.containsKey(office_layersDTO.layerNameEng)) {
                        mapOfOffice_layersDTOTolayerNameEng.put(office_layersDTO.layerNameEng, new HashSet<>());
                    }
                    mapOfOffice_layersDTOTolayerNameEng.get(office_layersDTO.layerNameEng).add(office_layersDTO);

                    if (!mapOfOffice_layersDTOTolayerNameBng.containsKey(office_layersDTO.layerNameBng)) {
                        mapOfOffice_layersDTOTolayerNameBng.put(office_layersDTO.layerNameBng, new HashSet<>());
                    }
                    mapOfOffice_layersDTOTolayerNameBng.get(office_layersDTO.layerNameBng).add(office_layersDTO);

                    if (!mapOfOffice_layersDTOTolayerLevel.containsKey(office_layersDTO.layerLevel)) {
                        mapOfOffice_layersDTOTolayerLevel.put(office_layersDTO.layerLevel, new HashSet<>());
                    }
                    mapOfOffice_layersDTOTolayerLevel.get(office_layersDTO.layerLevel).add(office_layersDTO);

                    if (!mapOfOffice_layersDTOTolayerSequence.containsKey(office_layersDTO.layerSequence)) {
                        mapOfOffice_layersDTOTolayerSequence.put(office_layersDTO.layerSequence, new HashSet<>());
                    }
                    mapOfOffice_layersDTOTolayerSequence.get(office_layersDTO.layerSequence).add(office_layersDTO);

                    if (!mapOfOffice_layersDTOTostatus.containsKey(office_layersDTO.status)) {
                        mapOfOffice_layersDTOTostatus.put(office_layersDTO.status, new HashSet<>());
                    }
                    mapOfOffice_layersDTOTostatus.get(office_layersDTO.status).add(office_layersDTO);

                    if (!mapOfOffice_layersDTOTocreatedBy.containsKey(office_layersDTO.createdBy)) {
                        mapOfOffice_layersDTOTocreatedBy.put(office_layersDTO.createdBy, new HashSet<>());
                    }
                    mapOfOffice_layersDTOTocreatedBy.get(office_layersDTO.createdBy).add(office_layersDTO);

                    if (!mapOfOffice_layersDTOTomodifiedBy.containsKey(office_layersDTO.modifiedBy)) {
                        mapOfOffice_layersDTOTomodifiedBy.put(office_layersDTO.modifiedBy, new HashSet<>());
                    }
                    mapOfOffice_layersDTOTomodifiedBy.get(office_layersDTO.modifiedBy).add(office_layersDTO);

                    if (!mapOfOffice_layersDTOTocreated.containsKey(office_layersDTO.created)) {
                        mapOfOffice_layersDTOTocreated.put(office_layersDTO.created, new HashSet<>());
                    }
                    mapOfOffice_layersDTOTocreated.get(office_layersDTO.created).add(office_layersDTO);

                    if (!mapOfOffice_layersDTOTomodified.containsKey(office_layersDTO.modified)) {
                        mapOfOffice_layersDTOTomodified.put(office_layersDTO.modified, new HashSet<>());
                    }
                    mapOfOffice_layersDTOTomodified.get(office_layersDTO.modified).add(office_layersDTO);

                }
            }

        } catch (Exception e) {
            logger.debug("FATAL", e);
        }
    }

    public List<Office_layersDTO> getOffice_layersList() {
        List<Office_layersDTO> office_layerss = new ArrayList<Office_layersDTO>(this.mapOfOffice_layersDTOToiD.values());
        return office_layerss;
    }


    public Office_layersDTO getOffice_layersDTOByID(long ID) {
        return mapOfOffice_layersDTOToiD.get(ID);
    }


    public List<Office_layersDTO> getOffice_layersDTOByoffice_ministry_id(int office_ministry_id) {
        return new ArrayList<>(mapOfOffice_layersDTOToofficeMinistryId.getOrDefault(office_ministry_id, new HashSet<>()));
    }


    public List<Office_layersDTO> getOffice_layersDTOByparent_layer_id(int parent_layer_id) {
        return new ArrayList<>(mapOfOffice_layersDTOToparentLayerId.getOrDefault(parent_layer_id, new HashSet<>()));
    }


    public List<Office_layersDTO> getOffice_layersDTOBycustom_layer_id(int custom_layer_id) {
        return new ArrayList<>(mapOfOffice_layersDTOTocustomLayerId.getOrDefault(custom_layer_id, new HashSet<>()));
    }


    public List<Office_layersDTO> getOffice_layersDTOBylayer_name_eng(String layer_name_eng) {
        return new ArrayList<>(mapOfOffice_layersDTOTolayerNameEng.getOrDefault(layer_name_eng, new HashSet<>()));
    }


    public List<Office_layersDTO> getOffice_layersDTOBylayer_name_bng(String layer_name_bng) {
        return new ArrayList<>(mapOfOffice_layersDTOTolayerNameBng.getOrDefault(layer_name_bng, new HashSet<>()));
    }


    public List<Office_layersDTO> getOffice_layersDTOBylayer_level(int layer_level) {
        return new ArrayList<>(mapOfOffice_layersDTOTolayerLevel.getOrDefault(layer_level, new HashSet<>()));
    }


    public List<Office_layersDTO> getOffice_layersDTOBylayer_sequence(int layer_sequence) {
        return new ArrayList<>(mapOfOffice_layersDTOTolayerSequence.getOrDefault(layer_sequence, new HashSet<>()));
    }


    public List<Office_layersDTO> getOffice_layersDTOBystatus(boolean status) {
        return new ArrayList<>(mapOfOffice_layersDTOTostatus.getOrDefault(status, new HashSet<>()));
    }


    public List<Office_layersDTO> getOffice_layersDTOBycreated_by(int created_by) {
        return new ArrayList<>(mapOfOffice_layersDTOTocreatedBy.getOrDefault(created_by, new HashSet<>()));
    }


    public List<Office_layersDTO> getOffice_layersDTOBymodified_by(int modified_by) {
        return new ArrayList<>(mapOfOffice_layersDTOTomodifiedBy.getOrDefault(modified_by, new HashSet<>()));
    }


    public List<Office_layersDTO> getOffice_layersDTOBycreated(long created) {
        return new ArrayList<>(mapOfOffice_layersDTOTocreated.getOrDefault(created, new HashSet<>()));
    }


    public List<Office_layersDTO> getOffice_layersDTOBymodified(long modified) {
        return new ArrayList<>(mapOfOffice_layersDTOTomodified.getOrDefault(modified, new HashSet<>()));
    }


    @Override
    public String getTableName() {
        String tableName = "";
        try {
            tableName = "office_layers";
        } catch (Exception ex) {
            logger.debug("FATAL", ex);
        }
        return tableName;
    }
}


