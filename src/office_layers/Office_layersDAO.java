package office_layers;

import dbm.DBMR;
import dbm.DBMW;
import login.LoginDTO;
import org.apache.log4j.Logger;
import repository.RepositoryManager;
import util.NavigationService;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;


public class Office_layersDAO implements NavigationService {

    Logger logger = Logger.getLogger(getClass());

    private void printSql(String sql) {
        System.out.println("sql: " + sql);
    }

    private void recordUpdateTime(Connection connection, long lastModificationTime) {
        recordUpdateTime(connection,"office_layers",lastModificationTime);
    }

    public long addOffice_layers(Office_layersDTO office_layersDTO) throws Exception {

        Connection connection = null;
        PreparedStatement ps = null;

        long lastModificationTime = System.currentTimeMillis();

        try {
            connection = DBMW.getInstance().getConnection();

            if (connection == null) {
                System.out.println("nullconn");
            }

            office_layersDTO.iD = DBMW.getInstance().getNextSequenceId("Office_layers");

            String sql = "INSERT INTO office_layers";

            sql += " (";
            sql += "ID";
            sql += ", ";
            sql += "office_ministry_id";
            sql += ", ";
            sql += "parent_layer_id";
            sql += ", ";
            sql += "custom_layer_id";
            sql += ", ";
            sql += "layer_name_eng";
            sql += ", ";
            sql += "layer_name_bng";
            sql += ", ";
            sql += "layer_level";
            sql += ", ";
            sql += "layer_sequence";
            sql += ", ";
            sql += "status";
            sql += ", ";
            sql += "created_by";
            sql += ", ";
            sql += "modified_by";
            sql += ", ";
            sql += "created";
            sql += ", ";
            sql += "modified";
            sql += ", ";
            sql += "isDeleted";
            sql += ", lastModificationTime)";


            sql += " VALUES(";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ?)";

            printSql(sql);

            ps = connection.prepareStatement(sql);


            int index = 1;

            ps.setObject(index++, office_layersDTO.iD);
            ps.setObject(index++, office_layersDTO.officeMinistryId);
            ps.setObject(index++, office_layersDTO.parentLayerId > 0 ? office_layersDTO.parentLayerId : 0);
            ps.setObject(index++, office_layersDTO.customLayerId);
            ps.setObject(index++, office_layersDTO.layerNameEng);
            ps.setObject(index++, office_layersDTO.layerNameBng);
            ps.setObject(index++, office_layersDTO.layerLevel);
            ps.setObject(index++, office_layersDTO.layerSequence);
            ps.setObject(index++, office_layersDTO.status);
            ps.setObject(index++, office_layersDTO.createdBy);
            ps.setObject(index++, office_layersDTO.modifiedBy);
            ps.setObject(index++, office_layersDTO.created);
            ps.setObject(index++, office_layersDTO.modified);
            ps.setObject(index++, office_layersDTO.isDeleted);
            ps.setObject(index++, lastModificationTime);

            System.out.println(ps);
            ps.execute();


            recordUpdateTime(connection, lastModificationTime);

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (connection != null) {
                    DBMW.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
        return office_layersDTO.iD;
    }

    //need another getter for repository
    public Office_layersDTO getOffice_layersDTOByID(long ID) throws Exception {
        Connection connection = null;
        ResultSet rs = null;
        Statement stmt = null;
        Office_layersDTO office_layersDTO = null;
        try {

            String sql = "SELECT * ";

            sql += " FROM office_layers";

            sql += " WHERE ID=" + ID;

            printSql(sql);

            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();


            rs = stmt.executeQuery(sql);

            if (rs.next()) {
                office_layersDTO = new Office_layersDTO();

                office_layersDTO.iD = rs.getLong("ID");
                office_layersDTO.officeMinistryId = rs.getInt("office_ministry_id");
                office_layersDTO.parentLayerId = rs.getInt("parent_layer_id");
                office_layersDTO.customLayerId = rs.getInt("custom_layer_id");
                office_layersDTO.layerNameEng = rs.getString("layer_name_eng");
                office_layersDTO.layerNameBng = rs.getString("layer_name_bng");
                office_layersDTO.layerLevel = rs.getInt("layer_level");
                office_layersDTO.layerSequence = rs.getInt("layer_sequence");
                office_layersDTO.status = rs.getBoolean("status");
                office_layersDTO.createdBy = rs.getInt("created_by");
                office_layersDTO.modifiedBy = rs.getInt("modified_by");
                office_layersDTO.created = rs.getLong("created");
                office_layersDTO.modified = rs.getLong("modified");
                office_layersDTO.isDeleted = rs.getBoolean("isDeleted");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }

            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
        return office_layersDTO;
    }

    public long updateOffice_layers(Office_layersDTO office_layersDTO) throws Exception {

        Connection connection = null;
        PreparedStatement ps = null;

        long lastModificationTime = System.currentTimeMillis();
        try {
            connection = DBMW.getInstance().getConnection();

            String sql = "UPDATE office_layers";

            sql += " SET ";
            sql += "office_ministry_id=?";
            sql += ", ";
            sql += "parent_layer_id=?";
            sql += ", ";
            sql += "custom_layer_id=?";
            sql += ", ";
            sql += "layer_name_eng=?";
            sql += ", ";
            sql += "layer_name_bng=?";
            sql += ", ";
            sql += "layer_level=?";
            sql += ", ";
            sql += "layer_sequence=?";
            sql += ", ";
            sql += "status=?";
            sql += ", ";
            sql += "created_by=?";
            sql += ", ";
            sql += "modified_by=?";
            sql += ", ";
            sql += "created=?";
            sql += ", ";
            sql += "modified=?";
            sql += ", ";
            sql += "isDeleted=?";
            sql += ", lastModificationTime = " + lastModificationTime + "";
            sql += " WHERE ID = " + office_layersDTO.iD;

            printSql(sql);

            ps = connection.prepareStatement(sql);

            int index = 1;
            ps.setObject(index++, office_layersDTO.officeMinistryId);
            ps.setObject(index++, office_layersDTO.parentLayerId);
            ps.setObject(index++, office_layersDTO.customLayerId);
            ps.setObject(index++, office_layersDTO.layerNameEng);
            ps.setObject(index++, office_layersDTO.layerNameBng);
            ps.setObject(index++, office_layersDTO.layerLevel);
            ps.setObject(index++, office_layersDTO.layerSequence);
            ps.setObject(index++, office_layersDTO.status);
            ps.setObject(index++, office_layersDTO.createdBy);
            ps.setObject(index++, office_layersDTO.modifiedBy);
            ps.setObject(index++, office_layersDTO.created);
            ps.setObject(index++, office_layersDTO.modified);
            ps.setObject(index++, office_layersDTO.isDeleted);
            System.out.println(ps);
            ps.executeUpdate();

            recordUpdateTime(connection, lastModificationTime);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (connection != null) {
                    DBMW.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
        return office_layersDTO.iD;
    }

    public void deleteOffice_layersByID(long ID) throws Exception {
        Connection connection = null;
        Statement stmt = null;
        PreparedStatement ps = null;

        long lastModificationTime = System.currentTimeMillis();
        try {
            String sql = "UPDATE office_layers";
            sql += " SET isDeleted=1,lastModificationTime=" + lastModificationTime + " WHERE ID = " + ID;
            printSql(sql);

            connection = DBMW.getInstance().getConnection();
            stmt = connection.createStatement();
            stmt.execute(sql);

            recordUpdateTime(connection, lastModificationTime);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }

            try {
                if (connection != null) {
                    DBMW.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
    }

    public List<Office_layersDTO> getDTOs(Collection recordIDs, LoginDTO loginDTO) {
        Connection connection = null;
        ResultSet rs = null;
        Statement stmt = null;
        Office_layersDTO office_layersDTO = null;
        List<Office_layersDTO> office_layersDTOList = new ArrayList<>();
        if (recordIDs.isEmpty()) {
            return office_layersDTOList;
        }
        try {

            String sql = "SELECT * ";

            sql += " FROM office_layers";

            sql += " WHERE ID IN ( ";

            for (int i = 0; i < recordIDs.size(); i++) {
                if (i != 0) {
                    sql += ",";
                }
                sql += ((ArrayList) recordIDs).get(i);
            }
            sql += ")  order by ID desc";

            printSql(sql);

            logger.debug("sql " + sql);
            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();

            rs = stmt.executeQuery(sql);

            while (rs.next()) {
                office_layersDTO = new Office_layersDTO();
                office_layersDTO.iD = rs.getLong("ID");
                office_layersDTO.officeMinistryId = rs.getInt("office_ministry_id");
                office_layersDTO.parentLayerId = rs.getInt("parent_layer_id");
                office_layersDTO.customLayerId = rs.getInt("custom_layer_id");
                office_layersDTO.layerNameEng = rs.getString("layer_name_eng");
                office_layersDTO.layerNameBng = rs.getString("layer_name_bng");
                office_layersDTO.layerLevel = rs.getInt("layer_level");
                office_layersDTO.layerSequence = rs.getInt("layer_sequence");
                office_layersDTO.status = rs.getBoolean("status");
                office_layersDTO.createdBy = rs.getInt("created_by");
                office_layersDTO.modifiedBy = rs.getInt("modified_by");
                office_layersDTO.created = rs.getLong("created");
                office_layersDTO.modified = rs.getLong("modified");
                office_layersDTO.isDeleted = rs.getBoolean("isDeleted");
                System.out.println("got this DTO: " + office_layersDTO);

                office_layersDTOList.add(office_layersDTO);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }

            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
        return office_layersDTOList;

    }

    public Collection getIDs(LoginDTO loginDTO) {
        Collection data = new ArrayList();
        Connection connection = null;
        Statement stmt = null;
        ResultSet resultSet = null;

        String sql = "SELECT ID FROM office_layers";

        sql += " WHERE isDeleted = 0  order by ID desc ";

        printSql(sql);

        try {
            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();

            for (resultSet = stmt.executeQuery(sql); resultSet.next(); data.add(resultSet.getString("ID"))) ;

            resultSet.close();
        } catch (Exception e) {
            logger.fatal("DAO " + e, e);
        } finally {
            try {
                if (resultSet != null && !resultSet.isClosed()) {
                    resultSet.close();
                }
            } catch (Exception ex) {

            }
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception e) {
                logger.fatal("DAO finally :" + e);
            }
        }
        return data;
    }

    //add repository
    public List<Office_layersDTO> getAllOffice_layers(boolean isFirstReload) {
        List<Office_layersDTO> office_layersDTOList = new ArrayList<>();

        String sql = "SELECT * FROM office_layers";
        sql += " WHERE ";

        if (isFirstReload) {
            sql += " isDeleted =  0";
        }
        if (!isFirstReload) {
            sql += " lastModificationTime >= " + RepositoryManager.lastModifyTime;
        }
        printSql(sql);
        Connection connection = null;
        Statement stmt = null;
        ResultSet rs = null;

        try {
            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();
            rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Office_layersDTO office_layersDTO = new Office_layersDTO();
                office_layersDTO.iD = rs.getLong("ID");
                office_layersDTO.officeMinistryId = rs.getInt("office_ministry_id");
                office_layersDTO.parentLayerId = rs.getInt("parent_layer_id");
                office_layersDTO.customLayerId = rs.getInt("custom_layer_id");
                office_layersDTO.layerNameEng = rs.getString("layer_name_eng");
                office_layersDTO.layerNameBng = rs.getString("layer_name_bng");
                office_layersDTO.layerLevel = rs.getInt("layer_level");
                office_layersDTO.layerSequence = rs.getInt("layer_sequence");
                office_layersDTO.status = rs.getBoolean("status");
                office_layersDTO.createdBy = rs.getInt("created_by");
                office_layersDTO.modifiedBy = rs.getInt("modified_by");
                office_layersDTO.created = rs.getLong("created");
                office_layersDTO.modified = rs.getLong("modified");
                office_layersDTO.isDeleted = rs.getBoolean("isDeleted");
                int i = 0;
                long primaryKey = office_layersDTO.iD;
                while (i < office_layersDTOList.size() && office_layersDTOList.get(i).iD < primaryKey) {
                    i++;
                }
                office_layersDTOList.add(i, office_layersDTO);
                //office_layersDTOList.add(office_layersDTO);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
        return office_layersDTOList;
    }


    public Collection getIDsWithSearchCriteria(Hashtable p_searchCriteria, LoginDTO loginDTO) throws Exception {
        System.out.println("table: " + p_searchCriteria);
        List<Long> idList = new ArrayList<>();
        Connection connection = null;
        PreparedStatement ps = null;

        try {

            String sql = "SELECT distinct office_layers.ID as ID FROM office_layers ";
            sql += " join office_custom_layers on office_layers.custom_layer_id = office_custom_layers.ID ";

            Enumeration names = p_searchCriteria.keys();
            String str, value;

            String AnyfieldSql = "(";

            if (p_searchCriteria.get("AnyField") != null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase("")) {
                int i = 0;
                Iterator it = Office_layersMAPS.GetInstance().java_anyfield_search_map.entrySet().iterator();
                while (it.hasNext()) {
                    if (i > 0) {
                        AnyfieldSql += " OR  ";
                    }
                    Map.Entry pair = (Map.Entry) it.next();
                    AnyfieldSql += pair.getKey() + " like '%" + p_searchCriteria.get("AnyField").toString() + "%'";
                    i++;
                }
            }
            AnyfieldSql += ")";
            System.out.println("AnyfieldSql = " + AnyfieldSql);

            String AllFieldSql = "(";
            int i = 0;
            while (names.hasMoreElements()) {
                str = (String) names.nextElement();
                value = (String) p_searchCriteria.get(str);
                System.out.println(str + ": " + value);
                if (Office_layersMAPS.GetInstance().java_allfield_type_map.get(str.toLowerCase()) != null && !Office_layersMAPS.GetInstance().java_allfield_type_map.get(str.toLowerCase()).equalsIgnoreCase("")
                        && !str.equalsIgnoreCase("AnyField")
                        && value != null && !value.equalsIgnoreCase("")) {
                    if (p_searchCriteria.get(str).equals("any")) {
                        continue;
                    }

                    if (i > 0) {
                        AllFieldSql += " AND  ";
                    }
                    if (Office_layersMAPS.GetInstance().java_allfield_type_map.get(str.toLowerCase()).equals("String")) {
                        AllFieldSql += "office_layers." + str.toLowerCase() + " like '%" + p_searchCriteria.get(str) + "%'";
                    } else {
                        AllFieldSql += "office_layers." + str.toLowerCase() + " = '" + p_searchCriteria.get(str) + "'";
                    }
                    i++;
                }
            }

            AllFieldSql += ")";

            System.out.println("AllFieldSql = " + AllFieldSql);


            sql += " WHERE ";
            sql += " office_layers.isDeleted = false";


            if (!AnyfieldSql.equals("()")) {
                sql += " AND " + AnyfieldSql;

            }
            if (!AllFieldSql.equals("()")) {
                sql += " AND " + AllFieldSql;
            }

            sql += " order by office_layers.ID desc ";

            printSql(sql);

            connection = DBMR.getInstance().getConnection();
            ps = connection.prepareStatement(sql);

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                idList.add(rs.getLong("ID"));
            }


        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
        return idList;
    }

    public Map<String, String> getCustomLayerDetails(int customLayerId) {
        Connection connection = null;
        ResultSet rs = null;
        Statement stmt = null;
        Map<String, String> data = new HashMap<>();

        try {

            String sql = "SELECT * ";

            sql += " FROM office_custom_layers";
            sql += " WHERE ID = " + customLayerId;
            printSql(sql);

            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();

            rs = stmt.executeQuery(sql);

            while (rs.next()) {
                data.put("name", rs.getString("name"));
                data.put("layer_level", rs.getString("layer_level"));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }

            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
        return data;
    }
}
	