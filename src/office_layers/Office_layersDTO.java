package office_layers;
import java.util.*; 


public class Office_layersDTO {

	public long iD = 0;
	public int officeMinistryId = 0;
	public int parentLayerId = 0;
	public int customLayerId = 0;
    public String layerNameEng = "";
    public String layerNameBng = "";
	public int layerLevel = 0;
	public int layerSequence = 0;
	public boolean status = false;
	public int createdBy = 0;
	public int modifiedBy = 0;
	public long created = 0;
	public long modified = 0;
	public boolean isDeleted = false;
	
	
    @Override
	public String toString() {
            return "$Office_layersDTO[" +
            " id = " + iD +
            " officeMinistryId = " + officeMinistryId +
            " parentLayerId = " + parentLayerId +
            " customLayerId = " + customLayerId +
            " layerNameEng = " + layerNameEng +
            " layerNameBng = " + layerNameBng +
            " layerLevel = " + layerLevel +
            " layerSequence = " + layerSequence +
            " status = " + status +
            " createdBy = " + createdBy +
            " modifiedBy = " + modifiedBy +
            " created = " + created +
            " modified = " + modified +
            " isDeleted = " + isDeleted +
            "]";
    }

}