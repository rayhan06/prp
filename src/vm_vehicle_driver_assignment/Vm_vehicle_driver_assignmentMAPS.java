package vm_vehicle_driver_assignment;
import java.util.*; 
import util.*;


public class Vm_vehicle_driver_assignmentMAPS extends CommonMaps
{	
	public Vm_vehicle_driver_assignmentMAPS(String tableName)
	{
		

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("vehicleTypeCat".toLowerCase(), "vehicleTypeCat".toLowerCase());
		java_DTO_map.put("vehicleId".toLowerCase(), "vehicleId".toLowerCase());
		java_DTO_map.put("driverId".toLowerCase(), "driverId".toLowerCase());
		java_DTO_map.put("employeeRecordName".toLowerCase(), "employeeRecordName".toLowerCase());
		java_DTO_map.put("employeeRecordNameBn".toLowerCase(), "employeeRecordNameBn".toLowerCase());
		java_DTO_map.put("unitName".toLowerCase(), "unitName".toLowerCase());
		java_DTO_map.put("unitNameBn".toLowerCase(), "unitNameBn".toLowerCase());
		java_DTO_map.put("postName".toLowerCase(), "postName".toLowerCase());
		java_DTO_map.put("postNameBn".toLowerCase(), "postNameBn".toLowerCase());
		java_DTO_map.put("remarks".toLowerCase(), "remarks".toLowerCase());
		java_DTO_map.put("insertedByUserId".toLowerCase(), "insertedByUserId".toLowerCase());
		java_DTO_map.put("insertedByOrganogramId".toLowerCase(), "insertedByOrganogramId".toLowerCase());
		java_DTO_map.put("insertionDate".toLowerCase(), "insertionDate".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
		java_DTO_map.put("searchColumn".toLowerCase(), "searchColumn".toLowerCase());

		java_SQL_map.put("vehicle_type_cat".toLowerCase(), "vehicleTypeCat".toLowerCase());
		java_SQL_map.put("vehicle_id".toLowerCase(), "vehicleId".toLowerCase());
		java_SQL_map.put("driver_id".toLowerCase(), "driverId".toLowerCase());
		java_SQL_map.put("employee_record_name".toLowerCase(), "employeeRecordName".toLowerCase());
		java_SQL_map.put("employee_record_name_bn".toLowerCase(), "employeeRecordNameBn".toLowerCase());
		java_SQL_map.put("unit_name".toLowerCase(), "unitName".toLowerCase());
		java_SQL_map.put("unit_name_bn".toLowerCase(), "unitNameBn".toLowerCase());
		java_SQL_map.put("post_name".toLowerCase(), "postName".toLowerCase());
		java_SQL_map.put("post_name_bn".toLowerCase(), "postNameBn".toLowerCase());
		java_SQL_map.put("remarks".toLowerCase(), "remarks".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Vehicle Type".toLowerCase(), "vehicleTypeCat".toLowerCase());
		java_Text_map.put("Vehicle Id".toLowerCase(), "vehicleId".toLowerCase());
		java_Text_map.put("Driver Id".toLowerCase(), "driverId".toLowerCase());
		java_Text_map.put("Employee Record Name".toLowerCase(), "employeeRecordName".toLowerCase());
		java_Text_map.put("Employee Record Name Bn".toLowerCase(), "employeeRecordNameBn".toLowerCase());
		java_Text_map.put("Unit Name".toLowerCase(), "unitName".toLowerCase());
		java_Text_map.put("Unit Name Bn".toLowerCase(), "unitNameBn".toLowerCase());
		java_Text_map.put("Post Name".toLowerCase(), "postName".toLowerCase());
		java_Text_map.put("Post Name Bn".toLowerCase(), "postNameBn".toLowerCase());
		java_Text_map.put("Remarks".toLowerCase(), "remarks".toLowerCase());
		java_Text_map.put("Inserted By User Id".toLowerCase(), "insertedByUserId".toLowerCase());
		java_Text_map.put("Inserted By Organogram Id".toLowerCase(), "insertedByOrganogramId".toLowerCase());
		java_Text_map.put("Insertion Date".toLowerCase(), "insertionDate".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
		java_Text_map.put("Search Column".toLowerCase(), "searchColumn".toLowerCase());
			
	}

}