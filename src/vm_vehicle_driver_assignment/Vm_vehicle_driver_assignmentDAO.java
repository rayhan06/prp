package vm_vehicle_driver_assignment;

import common.ConnectionAndStatementUtil;
import org.apache.log4j.Logger;
import pb.CatRepository;
import user.UserDTO;
import util.CommonDTO;
import util.NavigationService4;
import vm_vehicle.Vm_vehicleDTO;
import vm_vehicle.Vm_vehicleRepository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class Vm_vehicle_driver_assignmentDAO extends NavigationService4 {

    Logger logger = Logger.getLogger(getClass());


    public Vm_vehicle_driver_assignmentDAO(String tableName) {
        super(tableName);
        joinSQL = "";
        commonMaps = new Vm_vehicle_driver_assignmentMAPS(tableName);
        columnNames = new String[]
                {
                        "ID",
                        "vehicle_type_cat",
                        "vehicle_type_cat_multiple",
                        "vehicle_id",
                        "vehicle_id_multiple",
                        "driver_id",
                        "unit_id",
                        "post_id",
                        "start_time",
                        "end_time",
                        "employee_record_name",
                        "employee_record_name_bn",
                        "unit_name",
                        "unit_name_bn",
                        "post_name",
                        "post_name_bn",
                        "remarks",
                        "inserted_by_user_id",
                        "inserted_by_organogram_id",
                        "insertion_date",
                        "search_column",
                        "isDeleted",
                        "lastModificationTime"
                };
    }

    public Vm_vehicle_driver_assignmentDAO() {
        this("vm_vehicle_driver_assignment");
    }

    static class LazyLoader {
        static final Vm_vehicle_driver_assignmentDAO INSTANCE = new Vm_vehicle_driver_assignmentDAO();
    }

    // the whole project has DAOs as singleton! I am too tired to refactor the other peoples whole class!
    // doing only this make DAO Instance getting consistent!
    public static Vm_vehicle_driver_assignmentDAO getInstance() {
        return LazyLoader.INSTANCE;
    }

    public void setSearchColumn(Vm_vehicle_driver_assignmentDTO vm_vehicle_driver_assignmentDTO) {
        vm_vehicle_driver_assignmentDTO.searchColumn = "";
        vm_vehicle_driver_assignmentDTO.searchColumn += CatRepository.getName("English", "vehicle_type", vm_vehicle_driver_assignmentDTO.vehicleTypeCat) + " " + CatRepository.getName("Bangla", "vehicle_type", vm_vehicle_driver_assignmentDTO.vehicleTypeCat) + " ";
        vm_vehicle_driver_assignmentDTO.searchColumn += vm_vehicle_driver_assignmentDTO.employeeRecordName + " ";
        vm_vehicle_driver_assignmentDTO.searchColumn += vm_vehicle_driver_assignmentDTO.employeeRecordNameBn + " ";
        vm_vehicle_driver_assignmentDTO.searchColumn += vm_vehicle_driver_assignmentDTO.unitName + " ";
        vm_vehicle_driver_assignmentDTO.searchColumn += vm_vehicle_driver_assignmentDTO.unitNameBn + " ";
        vm_vehicle_driver_assignmentDTO.searchColumn += vm_vehicle_driver_assignmentDTO.postName + " ";
        vm_vehicle_driver_assignmentDTO.searchColumn += vm_vehicle_driver_assignmentDTO.postNameBn + " ";
        vm_vehicle_driver_assignmentDTO.searchColumn += vm_vehicle_driver_assignmentDTO.remarks + " ";

        Vm_vehicleDTO vm_vehicleDTO = Vm_vehicleRepository.getInstance().getVm_vehicleDTOByID(vm_vehicle_driver_assignmentDTO.vehicleId);
        if (vm_vehicleDTO != null) {
            vm_vehicle_driver_assignmentDTO.searchColumn += vm_vehicleDTO.regNo + " ";
        }

    }

    public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException {
        Vm_vehicle_driver_assignmentDTO vm_vehicle_driver_assignmentDTO = (Vm_vehicle_driver_assignmentDTO) commonDTO;
        int index = 1;
        long lastModificationTime = System.currentTimeMillis();
        setSearchColumn(vm_vehicle_driver_assignmentDTO);
        if (isInsert) {
            ps.setObject(index++, vm_vehicle_driver_assignmentDTO.iD);
        }
        ps.setObject(index++, vm_vehicle_driver_assignmentDTO.vehicleTypeCat);
        ps.setObject(index++, vm_vehicle_driver_assignmentDTO.vehicleTypeCatMultiple);
        ps.setObject(index++, vm_vehicle_driver_assignmentDTO.vehicleId);
        ps.setObject(index++, vm_vehicle_driver_assignmentDTO.vehicleIdMultiple);
        ps.setObject(index++, vm_vehicle_driver_assignmentDTO.driverId);
        ps.setObject(index++, vm_vehicle_driver_assignmentDTO.unit_id);
        ps.setObject(index++, vm_vehicle_driver_assignmentDTO.post_id);
        ps.setObject(index++, vm_vehicle_driver_assignmentDTO.start_time);
        ps.setObject(index++, vm_vehicle_driver_assignmentDTO.end_time);
        ps.setObject(index++, vm_vehicle_driver_assignmentDTO.employeeRecordName);
        ps.setObject(index++, vm_vehicle_driver_assignmentDTO.employeeRecordNameBn);
        ps.setObject(index++, vm_vehicle_driver_assignmentDTO.unitName);
        ps.setObject(index++, vm_vehicle_driver_assignmentDTO.unitNameBn);
        ps.setObject(index++, vm_vehicle_driver_assignmentDTO.postName);
        ps.setObject(index++, vm_vehicle_driver_assignmentDTO.postNameBn);
        ps.setObject(index++, vm_vehicle_driver_assignmentDTO.remarks);
        ps.setObject(index++, vm_vehicle_driver_assignmentDTO.insertedByUserId);
        ps.setObject(index++, vm_vehicle_driver_assignmentDTO.insertedByOrganogramId);
        ps.setObject(index++, vm_vehicle_driver_assignmentDTO.insertionDate);
        ps.setObject(index++, vm_vehicle_driver_assignmentDTO.searchColumn);
        if (isInsert) {
            ps.setObject(index++, 0);
            ps.setObject(index++, lastModificationTime);
        }
    }

    public Vm_vehicle_driver_assignmentDTO build(ResultSet rs) {
        try {
            Vm_vehicle_driver_assignmentDTO vm_vehicle_driver_assignmentDTO = new Vm_vehicle_driver_assignmentDTO();
            vm_vehicle_driver_assignmentDTO.iD = rs.getLong("ID");
            vm_vehicle_driver_assignmentDTO.vehicleTypeCat = rs.getInt("vehicle_type_cat");
            vm_vehicle_driver_assignmentDTO.vehicleTypeCatMultiple = rs.getString("vehicle_type_cat_multiple");
            vm_vehicle_driver_assignmentDTO.vehicleId = rs.getLong("vehicle_id");
            vm_vehicle_driver_assignmentDTO.vehicleIdMultiple = rs.getString("vehicle_id_multiple");
            vm_vehicle_driver_assignmentDTO.driverId = rs.getLong("driver_id");
            vm_vehicle_driver_assignmentDTO.unit_id = rs.getLong("unit_id");
            vm_vehicle_driver_assignmentDTO.post_id = rs.getLong("post_id");
            vm_vehicle_driver_assignmentDTO.start_time = rs.getLong("start_time");
            vm_vehicle_driver_assignmentDTO.end_time = rs.getLong("end_time");
            vm_vehicle_driver_assignmentDTO.employeeRecordName = rs.getString("employee_record_name");
            vm_vehicle_driver_assignmentDTO.employeeRecordNameBn = rs.getString("employee_record_name_bn");
            vm_vehicle_driver_assignmentDTO.unitName = rs.getString("unit_name");
            vm_vehicle_driver_assignmentDTO.unitNameBn = rs.getString("unit_name_bn");
            vm_vehicle_driver_assignmentDTO.postName = rs.getString("post_name");
            vm_vehicle_driver_assignmentDTO.postNameBn = rs.getString("post_name_bn");
            vm_vehicle_driver_assignmentDTO.remarks = rs.getString("remarks");
            vm_vehicle_driver_assignmentDTO.insertedByUserId = rs.getLong("inserted_by_user_id");
            vm_vehicle_driver_assignmentDTO.insertedByOrganogramId = rs.getLong("inserted_by_organogram_id");
            vm_vehicle_driver_assignmentDTO.insertionDate = rs.getLong("insertion_date");
            vm_vehicle_driver_assignmentDTO.isDeleted = rs.getInt("isDeleted");
            vm_vehicle_driver_assignmentDTO.lastModificationTime = rs.getLong("lastModificationTime");
            vm_vehicle_driver_assignmentDTO.searchColumn = rs.getString("search_column");
            return vm_vehicle_driver_assignmentDTO;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    public Vm_vehicle_driver_assignmentDTO buildForCount(ResultSet rs) {
        try {
            Vm_vehicle_driver_assignmentDTO vm_vehicle_driver_assignmentDTO = new Vm_vehicle_driver_assignmentDTO();
            vm_vehicle_driver_assignmentDTO.count = rs.getInt("count");
            return vm_vehicle_driver_assignmentDTO;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }


    //need another getter for repository


    public Vm_vehicle_driver_assignmentDTO getAllVm_vehicle_driver_assignmentByVehicleId(long vehicleId) {

        List<Vm_vehicle_driver_assignmentDTO> dtos = Vm_vehicle_driver_assignmentRepository.getInstance().
                                                                                           getVm_vehicle_driver_assignmentDTOByvehicle_id(vehicleId);

        if (dtos.size() > 0) {
            return dtos.get(0);
        } else {
            return null;
        }

//		String sql = "SELECT * FROM vm_vehicle_driver_assignment";
//		sql += " WHERE ";
//		sql+=" isDeleted =  0 and vehicle_id = ? ";
//		sql += " order by vm_vehicle_driver_assignment.lastModificationTime desc";
//		printSql(sql);
//		return ConnectionAndStatementUtil.getT(sql, Arrays.asList(vehicleId),this::build);


    }


    public String getSqlWithSearchCriteriaForNormalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
                                                          UserDTO userDTO, String filter, boolean tableHasJobCat, List<Object> objectList) {
        boolean viewAll = false;

        if (p_searchCriteria != null && p_searchCriteria.get("ViewAll") != null) {
            System.out.println("ViewAll = " + p_searchCriteria.get("ViewAll"));
            viewAll = true;
        } else {
            System.out.println("ViewAll is null ");
        }

        String sql = "SELECT ";
        if (category == GETIDS) {
            sql += " " + tableName + ".ID as ID ";
        } else if (category == GETCOUNT) {
            sql += " count( " + tableName + ".ID) as countID ";
        } else if (category == GETDTOS) {
            sql += " distinct " + tableName + ".* ";
        }
        sql += "FROM " + tableName + " ";
        sql += joinSQL;


        String AnyfieldSql = "";
        String AllFieldSql = "";

        if (p_searchCriteria != null) {


            Enumeration names = p_searchCriteria.keys();
            String str, value;

            AnyfieldSql = "(";

            if (p_searchCriteria.get("AnyField") != null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase("")) {
                AnyfieldSql += tableName + ".search_column like ? ";
                objectList.add("%" + p_searchCriteria.get("AnyField").toString() + "%");
            }
            AnyfieldSql += ")";
            System.out.println("AnyfieldSql = " + AnyfieldSql);

            AllFieldSql = "(";
            int i = 0;
            while (names.hasMoreElements()) {
                str = (String) names.nextElement();
                value = (String) p_searchCriteria.get(str);
                System.out.println(str + ": " + value);
                if (value != null && !value.equalsIgnoreCase("") && (
                        str.equals("vehicle_type_cat")
                        || str.equals("employee_record_name")
                        || str.equals("employee_record_name_bn")
                        || str.equals("unit_name")
                        || str.equals("unit_name_bn")
                        || str.equals("post_name")
                        || str.equals("post_name_bn")
                        || str.equals("remarks")
                        || str.equals("insertion_date_start")
                        || str.equals("insertion_date_end")
                )

                ) {
                    if (p_searchCriteria.get(str).equals("any")) {
                        continue;
                    }

                    if (i > 0) {
                        AllFieldSql += " AND  ";
                    }

                    if (str.equals("vehicle_type_cat")) {
                        String columnName = tableName + ".vehicle_type_cat_multiple ";
                        AllFieldSql += "" + columnName + " LIKE '%," + p_searchCriteria.get(str) + ",%' OR " + columnName + " LIKE '%," +
                                       p_searchCriteria.get(str) + "' OR " + columnName + " LIKE '" + p_searchCriteria.get(str) + ",%' OR "
                                       + columnName + "='" + p_searchCriteria.get(str) + "'  ";
                        //objectList.add(p_searchCriteria.get(str).toString());
                        i++;
                    }
//					else if(str.equals("employee_record_name"))
//					{
//						AllFieldSql += "" + tableName + ".employee_record_name like '%" + p_searchCriteria.get(str) + "%'";
//						i ++;
//					}
//					else if(str.equals("employee_record_name_bn"))
//					{
//						AllFieldSql += "" + tableName + ".employee_record_name_bn like '%" + p_searchCriteria.get(str) + "%'";
//						i ++;
//					}
//					else if(str.equals("unit_name"))
//					{
//						AllFieldSql += "" + tableName + ".unit_name like '%" + p_searchCriteria.get(str) + "%'";
//						i ++;
//					}
//					else if(str.equals("unit_name_bn"))
//					{
//						AllFieldSql += "" + tableName + ".unit_name_bn like '%" + p_searchCriteria.get(str) + "%'";
//						i ++;
//					}
//					else if(str.equals("post_name"))
//					{
//						AllFieldSql += "" + tableName + ".post_name like '%" + p_searchCriteria.get(str) + "%'";
//						i ++;
//					}
//					else if(str.equals("post_name_bn"))
//					{
//						AllFieldSql += "" + tableName + ".post_name_bn like '%" + p_searchCriteria.get(str) + "%'";
//						i ++;
//					}
//					else if(str.equals("remarks"))
//					{
//						AllFieldSql += "" + tableName + ".remarks like '%" + p_searchCriteria.get(str) + "%'";
//						i ++;
//					}
//					else if(str.equals("insertion_date_start"))
//					{
//						AllFieldSql += "" + tableName + ".insertion_date >= " + p_searchCriteria.get(str);
//						i ++;
//					}
//					else if(str.equals("insertion_date_end"))
//					{
//						AllFieldSql += "" + tableName + ".insertion_date <= " + p_searchCriteria.get(str);
//						i ++;
//					}


                }
            }

            AllFieldSql += ")";
            System.out.println("AllFieldSql = " + AllFieldSql);


        }


        sql += " WHERE ";

        sql += " (" + tableName + ".isDeleted = 0 ";
        sql += ")";


        if (!filter.equalsIgnoreCase("")) {
            sql += " and " + filter + " ";
        }

        if (!AnyfieldSql.equals("()") && !AnyfieldSql.equals("")) {
            sql += " AND " + AnyfieldSql;

        }
        if (!AllFieldSql.equals("()") && !AllFieldSql.equals("")) {
            sql += " AND " + AllFieldSql;
        }


        sql += " order by " + tableName + ".lastModificationTime desc ";

        printSql(sql);

        if (limit >= 0) {
            sql += " limit " + limit;
        }
        if (offset >= 0) {
            sql += " offset " + offset;
        }

        System.out.println("-------------- sql = " + sql);

        return sql;
    }


    public int getCountByVehicleId(long vehicleId) {


        String sql = "SELECT COUNT(*) AS count FROM vm_vehicle_driver_assignment ";
        sql += " WHERE isDeleted = 0 AND vehicle_id = ? ";
        printSql(sql);

        int count = 0;

        Vm_vehicle_driver_assignmentDTO dto = ConnectionAndStatementUtil.
                getT(sql, Collections.singletonList(vehicleId), this::buildForCount);
        if (dto != null) {
            count = dto.count;
        }
        return count;

    }

    private static final String getByEmployeeAndTimeRangeSql = "select * from vm_vehicle_driver_assignment where isDeleted = 0 and driver_id = %d";

    @SuppressWarnings("unused")
    // I don't get why start_time, end_time is not shown even in search page! so ignoring start, end for now
    public List<Vm_vehicle_driver_assignmentDTO> getByEmployeeAndTimeRange(long employeeRecordId, long startInc, long endInc) {
        List<Vm_vehicle_driver_assignmentDTO> dtoListByDriverId = ConnectionAndStatementUtil.getListOfT(
                String.format(getByEmployeeAndTimeRangeSql, employeeRecordId),
                this::build
        );
        // if start end is final use isActiveInTimeRange(startInc, endInc) method on dto to filter
        return new ArrayList<>(dtoListByDriverId);
    }
}
	