package vm_vehicle_driver_assignment;

public class DriverDTO
{
    public long assignmentId = -1;
	public boolean flag = false;
	public String driverNameEn = "";
    public String driverNameBn = "";
	
	
    @Override
	public String toString() {
            return "DriverDTO[" +
            " flag = " + flag +
            " driverNameEn = " + driverNameEn +
            " driverNameBn = " + driverNameBn +
            "]";
    }

}