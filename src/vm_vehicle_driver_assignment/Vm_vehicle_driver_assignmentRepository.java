package vm_vehicle_driver_assignment;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import com.google.gson.Gson;
import org.apache.log4j.Logger;

import pb.Utils;
import repository.Repository;
import repository.RepositoryManager;
import vm_vehicle.Vm_vehicleDTO;


public class Vm_vehicle_driver_assignmentRepository implements Repository {
	Vm_vehicle_driver_assignmentDAO vm_vehicle_driver_assignmentDAO = new Vm_vehicle_driver_assignmentDAO();
	Gson gson = new Gson();
	
	public void setDAO(Vm_vehicle_driver_assignmentDAO vm_vehicle_driver_assignmentDAO)
	{
		this.vm_vehicle_driver_assignmentDAO = vm_vehicle_driver_assignmentDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Vm_vehicle_driver_assignmentRepository.class);
	Map<Long, Vm_vehicle_driver_assignmentDTO>mapOfVm_vehicle_driver_assignmentDTOToiD;
//	Map<Integer, Set<Vm_vehicle_driver_assignmentDTO> >mapOfVm_vehicle_driver_assignmentDTOTovehicleTypeCat;
	Map<Long, Set<Vm_vehicle_driver_assignmentDTO> >mapOfVm_vehicle_driver_assignmentDTOTovehicleId;
//	Map<Long, Set<Vm_vehicle_driver_assignmentDTO> >mapOfVm_vehicle_driver_assignmentDTOTodriverId;
//	Map<String, Set<Vm_vehicle_driver_assignmentDTO> >mapOfVm_vehicle_driver_assignmentDTOToemployeeRecordName;
//	Map<String, Set<Vm_vehicle_driver_assignmentDTO> >mapOfVm_vehicle_driver_assignmentDTOToemployeeRecordNameBn;
//	Map<String, Set<Vm_vehicle_driver_assignmentDTO> >mapOfVm_vehicle_driver_assignmentDTOTounitName;
//	Map<String, Set<Vm_vehicle_driver_assignmentDTO> >mapOfVm_vehicle_driver_assignmentDTOTounitNameBn;
//	Map<String, Set<Vm_vehicle_driver_assignmentDTO> >mapOfVm_vehicle_driver_assignmentDTOTopostName;
//	Map<String, Set<Vm_vehicle_driver_assignmentDTO> >mapOfVm_vehicle_driver_assignmentDTOTopostNameBn;
//	Map<String, Set<Vm_vehicle_driver_assignmentDTO> >mapOfVm_vehicle_driver_assignmentDTOToremarks;
//	Map<Long, Set<Vm_vehicle_driver_assignmentDTO> >mapOfVm_vehicle_driver_assignmentDTOToinsertedByUserId;
//	Map<Long, Set<Vm_vehicle_driver_assignmentDTO> >mapOfVm_vehicle_driver_assignmentDTOToinsertedByOrganogramId;
//	Map<Long, Set<Vm_vehicle_driver_assignmentDTO> >mapOfVm_vehicle_driver_assignmentDTOToinsertionDate;
//	Map<Long, Set<Vm_vehicle_driver_assignmentDTO> >mapOfVm_vehicle_driver_assignmentDTOTolastModificationTime;
//	Map<String, Set<Vm_vehicle_driver_assignmentDTO> >mapOfVm_vehicle_driver_assignmentDTOTosearchColumn;


	static Vm_vehicle_driver_assignmentRepository instance = null;  
	private Vm_vehicle_driver_assignmentRepository(){
		mapOfVm_vehicle_driver_assignmentDTOToiD = new ConcurrentHashMap<>();
//		mapOfVm_vehicle_driver_assignmentDTOTovehicleTypeCat = new ConcurrentHashMap<>();
		mapOfVm_vehicle_driver_assignmentDTOTovehicleId = new ConcurrentHashMap<>();
//		mapOfVm_vehicle_driver_assignmentDTOTodriverId = new ConcurrentHashMap<>();
//		mapOfVm_vehicle_driver_assignmentDTOToemployeeRecordName = new ConcurrentHashMap<>();
//		mapOfVm_vehicle_driver_assignmentDTOToemployeeRecordNameBn = new ConcurrentHashMap<>();
//		mapOfVm_vehicle_driver_assignmentDTOTounitName = new ConcurrentHashMap<>();
//		mapOfVm_vehicle_driver_assignmentDTOTounitNameBn = new ConcurrentHashMap<>();
//		mapOfVm_vehicle_driver_assignmentDTOTopostName = new ConcurrentHashMap<>();
//		mapOfVm_vehicle_driver_assignmentDTOTopostNameBn = new ConcurrentHashMap<>();
//		mapOfVm_vehicle_driver_assignmentDTOToremarks = new ConcurrentHashMap<>();
//		mapOfVm_vehicle_driver_assignmentDTOToinsertedByUserId = new ConcurrentHashMap<>();
//		mapOfVm_vehicle_driver_assignmentDTOToinsertedByOrganogramId = new ConcurrentHashMap<>();
//		mapOfVm_vehicle_driver_assignmentDTOToinsertionDate = new ConcurrentHashMap<>();
//		mapOfVm_vehicle_driver_assignmentDTOTolastModificationTime = new ConcurrentHashMap<>();
//		mapOfVm_vehicle_driver_assignmentDTOTosearchColumn = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Vm_vehicle_driver_assignmentRepository getInstance(){
		if (instance == null){
			instance = new Vm_vehicle_driver_assignmentRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(vm_vehicle_driver_assignmentDAO == null)
		{
			return;
		}
		try {
			List<Vm_vehicle_driver_assignmentDTO> vm_vehicle_driver_assignmentDTOs = (List<Vm_vehicle_driver_assignmentDTO>) vm_vehicle_driver_assignmentDAO.getAll(reloadAll);
			for(Vm_vehicle_driver_assignmentDTO vm_vehicle_driver_assignmentDTO : vm_vehicle_driver_assignmentDTOs) {
				Vm_vehicle_driver_assignmentDTO oldVm_vehicle_driver_assignmentDTO =
						getVm_vehicle_driver_assignmentDTOByIDWithoutClone(vm_vehicle_driver_assignmentDTO.iD);
				if( oldVm_vehicle_driver_assignmentDTO != null ) {
					mapOfVm_vehicle_driver_assignmentDTOToiD.remove(oldVm_vehicle_driver_assignmentDTO.iD);
				
//					if(mapOfVm_vehicle_driver_assignmentDTOTovehicleTypeCat.containsKey(oldVm_vehicle_driver_assignmentDTO.vehicleTypeCat)) {
//						mapOfVm_vehicle_driver_assignmentDTOTovehicleTypeCat.get(oldVm_vehicle_driver_assignmentDTO.vehicleTypeCat).remove(oldVm_vehicle_driver_assignmentDTO);
//					}
//					if(mapOfVm_vehicle_driver_assignmentDTOTovehicleTypeCat.get(oldVm_vehicle_driver_assignmentDTO.vehicleTypeCat).isEmpty()) {
//						mapOfVm_vehicle_driver_assignmentDTOTovehicleTypeCat.remove(oldVm_vehicle_driver_assignmentDTO.vehicleTypeCat);
//					}
//
					if(mapOfVm_vehicle_driver_assignmentDTOTovehicleId.containsKey(oldVm_vehicle_driver_assignmentDTO.vehicleId)) {
						mapOfVm_vehicle_driver_assignmentDTOTovehicleId.get(oldVm_vehicle_driver_assignmentDTO.vehicleId).remove(oldVm_vehicle_driver_assignmentDTO);
					}
					if(mapOfVm_vehicle_driver_assignmentDTOTovehicleId.get(oldVm_vehicle_driver_assignmentDTO.vehicleId).isEmpty()) {
						mapOfVm_vehicle_driver_assignmentDTOTovehicleId.remove(oldVm_vehicle_driver_assignmentDTO.vehicleId);
					}
//
//					if(mapOfVm_vehicle_driver_assignmentDTOTodriverId.containsKey(oldVm_vehicle_driver_assignmentDTO.driverId)) {
//						mapOfVm_vehicle_driver_assignmentDTOTodriverId.get(oldVm_vehicle_driver_assignmentDTO.driverId).remove(oldVm_vehicle_driver_assignmentDTO);
//					}
//					if(mapOfVm_vehicle_driver_assignmentDTOTodriverId.get(oldVm_vehicle_driver_assignmentDTO.driverId).isEmpty()) {
//						mapOfVm_vehicle_driver_assignmentDTOTodriverId.remove(oldVm_vehicle_driver_assignmentDTO.driverId);
//					}
//
//					if(mapOfVm_vehicle_driver_assignmentDTOToemployeeRecordName.containsKey(oldVm_vehicle_driver_assignmentDTO.employeeRecordName)) {
//						mapOfVm_vehicle_driver_assignmentDTOToemployeeRecordName.get(oldVm_vehicle_driver_assignmentDTO.employeeRecordName).remove(oldVm_vehicle_driver_assignmentDTO);
//					}
//					if(mapOfVm_vehicle_driver_assignmentDTOToemployeeRecordName.get(oldVm_vehicle_driver_assignmentDTO.employeeRecordName).isEmpty()) {
//						mapOfVm_vehicle_driver_assignmentDTOToemployeeRecordName.remove(oldVm_vehicle_driver_assignmentDTO.employeeRecordName);
//					}
//
//					if(mapOfVm_vehicle_driver_assignmentDTOToemployeeRecordNameBn.containsKey(oldVm_vehicle_driver_assignmentDTO.employeeRecordNameBn)) {
//						mapOfVm_vehicle_driver_assignmentDTOToemployeeRecordNameBn.get(oldVm_vehicle_driver_assignmentDTO.employeeRecordNameBn).remove(oldVm_vehicle_driver_assignmentDTO);
//					}
//					if(mapOfVm_vehicle_driver_assignmentDTOToemployeeRecordNameBn.get(oldVm_vehicle_driver_assignmentDTO.employeeRecordNameBn).isEmpty()) {
//						mapOfVm_vehicle_driver_assignmentDTOToemployeeRecordNameBn.remove(oldVm_vehicle_driver_assignmentDTO.employeeRecordNameBn);
//					}
//
//					if(mapOfVm_vehicle_driver_assignmentDTOTounitName.containsKey(oldVm_vehicle_driver_assignmentDTO.unitName)) {
//						mapOfVm_vehicle_driver_assignmentDTOTounitName.get(oldVm_vehicle_driver_assignmentDTO.unitName).remove(oldVm_vehicle_driver_assignmentDTO);
//					}
//					if(mapOfVm_vehicle_driver_assignmentDTOTounitName.get(oldVm_vehicle_driver_assignmentDTO.unitName).isEmpty()) {
//						mapOfVm_vehicle_driver_assignmentDTOTounitName.remove(oldVm_vehicle_driver_assignmentDTO.unitName);
//					}
//
//					if(mapOfVm_vehicle_driver_assignmentDTOTounitNameBn.containsKey(oldVm_vehicle_driver_assignmentDTO.unitNameBn)) {
//						mapOfVm_vehicle_driver_assignmentDTOTounitNameBn.get(oldVm_vehicle_driver_assignmentDTO.unitNameBn).remove(oldVm_vehicle_driver_assignmentDTO);
//					}
//					if(mapOfVm_vehicle_driver_assignmentDTOTounitNameBn.get(oldVm_vehicle_driver_assignmentDTO.unitNameBn).isEmpty()) {
//						mapOfVm_vehicle_driver_assignmentDTOTounitNameBn.remove(oldVm_vehicle_driver_assignmentDTO.unitNameBn);
//					}
//
//					if(mapOfVm_vehicle_driver_assignmentDTOTopostName.containsKey(oldVm_vehicle_driver_assignmentDTO.postName)) {
//						mapOfVm_vehicle_driver_assignmentDTOTopostName.get(oldVm_vehicle_driver_assignmentDTO.postName).remove(oldVm_vehicle_driver_assignmentDTO);
//					}
//					if(mapOfVm_vehicle_driver_assignmentDTOTopostName.get(oldVm_vehicle_driver_assignmentDTO.postName).isEmpty()) {
//						mapOfVm_vehicle_driver_assignmentDTOTopostName.remove(oldVm_vehicle_driver_assignmentDTO.postName);
//					}
//
//					if(mapOfVm_vehicle_driver_assignmentDTOTopostNameBn.containsKey(oldVm_vehicle_driver_assignmentDTO.postNameBn)) {
//						mapOfVm_vehicle_driver_assignmentDTOTopostNameBn.get(oldVm_vehicle_driver_assignmentDTO.postNameBn).remove(oldVm_vehicle_driver_assignmentDTO);
//					}
//					if(mapOfVm_vehicle_driver_assignmentDTOTopostNameBn.get(oldVm_vehicle_driver_assignmentDTO.postNameBn).isEmpty()) {
//						mapOfVm_vehicle_driver_assignmentDTOTopostNameBn.remove(oldVm_vehicle_driver_assignmentDTO.postNameBn);
//					}
//
//					if(mapOfVm_vehicle_driver_assignmentDTOToremarks.containsKey(oldVm_vehicle_driver_assignmentDTO.remarks)) {
//						mapOfVm_vehicle_driver_assignmentDTOToremarks.get(oldVm_vehicle_driver_assignmentDTO.remarks).remove(oldVm_vehicle_driver_assignmentDTO);
//					}
//					if(mapOfVm_vehicle_driver_assignmentDTOToremarks.get(oldVm_vehicle_driver_assignmentDTO.remarks).isEmpty()) {
//						mapOfVm_vehicle_driver_assignmentDTOToremarks.remove(oldVm_vehicle_driver_assignmentDTO.remarks);
//					}
//
//					if(mapOfVm_vehicle_driver_assignmentDTOToinsertedByUserId.containsKey(oldVm_vehicle_driver_assignmentDTO.insertedByUserId)) {
//						mapOfVm_vehicle_driver_assignmentDTOToinsertedByUserId.get(oldVm_vehicle_driver_assignmentDTO.insertedByUserId).remove(oldVm_vehicle_driver_assignmentDTO);
//					}
//					if(mapOfVm_vehicle_driver_assignmentDTOToinsertedByUserId.get(oldVm_vehicle_driver_assignmentDTO.insertedByUserId).isEmpty()) {
//						mapOfVm_vehicle_driver_assignmentDTOToinsertedByUserId.remove(oldVm_vehicle_driver_assignmentDTO.insertedByUserId);
//					}
//
//					if(mapOfVm_vehicle_driver_assignmentDTOToinsertedByOrganogramId.containsKey(oldVm_vehicle_driver_assignmentDTO.insertedByOrganogramId)) {
//						mapOfVm_vehicle_driver_assignmentDTOToinsertedByOrganogramId.get(oldVm_vehicle_driver_assignmentDTO.insertedByOrganogramId).remove(oldVm_vehicle_driver_assignmentDTO);
//					}
//					if(mapOfVm_vehicle_driver_assignmentDTOToinsertedByOrganogramId.get(oldVm_vehicle_driver_assignmentDTO.insertedByOrganogramId).isEmpty()) {
//						mapOfVm_vehicle_driver_assignmentDTOToinsertedByOrganogramId.remove(oldVm_vehicle_driver_assignmentDTO.insertedByOrganogramId);
//					}
//
//					if(mapOfVm_vehicle_driver_assignmentDTOToinsertionDate.containsKey(oldVm_vehicle_driver_assignmentDTO.insertionDate)) {
//						mapOfVm_vehicle_driver_assignmentDTOToinsertionDate.get(oldVm_vehicle_driver_assignmentDTO.insertionDate).remove(oldVm_vehicle_driver_assignmentDTO);
//					}
//					if(mapOfVm_vehicle_driver_assignmentDTOToinsertionDate.get(oldVm_vehicle_driver_assignmentDTO.insertionDate).isEmpty()) {
//						mapOfVm_vehicle_driver_assignmentDTOToinsertionDate.remove(oldVm_vehicle_driver_assignmentDTO.insertionDate);
//					}
//
//					if(mapOfVm_vehicle_driver_assignmentDTOTolastModificationTime.containsKey(oldVm_vehicle_driver_assignmentDTO.lastModificationTime)) {
//						mapOfVm_vehicle_driver_assignmentDTOTolastModificationTime.get(oldVm_vehicle_driver_assignmentDTO.lastModificationTime).remove(oldVm_vehicle_driver_assignmentDTO);
//					}
//					if(mapOfVm_vehicle_driver_assignmentDTOTolastModificationTime.get(oldVm_vehicle_driver_assignmentDTO.lastModificationTime).isEmpty()) {
//						mapOfVm_vehicle_driver_assignmentDTOTolastModificationTime.remove(oldVm_vehicle_driver_assignmentDTO.lastModificationTime);
//					}
//
//					if(mapOfVm_vehicle_driver_assignmentDTOTosearchColumn.containsKey(oldVm_vehicle_driver_assignmentDTO.searchColumn)) {
//						mapOfVm_vehicle_driver_assignmentDTOTosearchColumn.get(oldVm_vehicle_driver_assignmentDTO.searchColumn).remove(oldVm_vehicle_driver_assignmentDTO);
//					}
//					if(mapOfVm_vehicle_driver_assignmentDTOTosearchColumn.get(oldVm_vehicle_driver_assignmentDTO.searchColumn).isEmpty()) {
//						mapOfVm_vehicle_driver_assignmentDTOTosearchColumn.remove(oldVm_vehicle_driver_assignmentDTO.searchColumn);
//					}
					
					
				}
				if(vm_vehicle_driver_assignmentDTO.isDeleted == 0) 
				{
					
					mapOfVm_vehicle_driver_assignmentDTOToiD.put(vm_vehicle_driver_assignmentDTO.iD, vm_vehicle_driver_assignmentDTO);
				
//					if( ! mapOfVm_vehicle_driver_assignmentDTOTovehicleTypeCat.containsKey(vm_vehicle_driver_assignmentDTO.vehicleTypeCat)) {
//						mapOfVm_vehicle_driver_assignmentDTOTovehicleTypeCat.put(vm_vehicle_driver_assignmentDTO.vehicleTypeCat, new HashSet<>());
//					}
//					mapOfVm_vehicle_driver_assignmentDTOTovehicleTypeCat.get(vm_vehicle_driver_assignmentDTO.vehicleTypeCat).add(vm_vehicle_driver_assignmentDTO);
//
					if( ! mapOfVm_vehicle_driver_assignmentDTOTovehicleId.containsKey(vm_vehicle_driver_assignmentDTO.vehicleId)) {
						mapOfVm_vehicle_driver_assignmentDTOTovehicleId.put(vm_vehicle_driver_assignmentDTO.vehicleId, new HashSet<>());
					}
					mapOfVm_vehicle_driver_assignmentDTOTovehicleId.get(vm_vehicle_driver_assignmentDTO.vehicleId).add(vm_vehicle_driver_assignmentDTO);
//
//					if( ! mapOfVm_vehicle_driver_assignmentDTOTodriverId.containsKey(vm_vehicle_driver_assignmentDTO.driverId)) {
//						mapOfVm_vehicle_driver_assignmentDTOTodriverId.put(vm_vehicle_driver_assignmentDTO.driverId, new HashSet<>());
//					}
//					mapOfVm_vehicle_driver_assignmentDTOTodriverId.get(vm_vehicle_driver_assignmentDTO.driverId).add(vm_vehicle_driver_assignmentDTO);
//
//					if( ! mapOfVm_vehicle_driver_assignmentDTOToemployeeRecordName.containsKey(vm_vehicle_driver_assignmentDTO.employeeRecordName)) {
//						mapOfVm_vehicle_driver_assignmentDTOToemployeeRecordName.put(vm_vehicle_driver_assignmentDTO.employeeRecordName, new HashSet<>());
//					}
//					mapOfVm_vehicle_driver_assignmentDTOToemployeeRecordName.get(vm_vehicle_driver_assignmentDTO.employeeRecordName).add(vm_vehicle_driver_assignmentDTO);
//
//					if( ! mapOfVm_vehicle_driver_assignmentDTOToemployeeRecordNameBn.containsKey(vm_vehicle_driver_assignmentDTO.employeeRecordNameBn)) {
//						mapOfVm_vehicle_driver_assignmentDTOToemployeeRecordNameBn.put(vm_vehicle_driver_assignmentDTO.employeeRecordNameBn, new HashSet<>());
//					}
//					mapOfVm_vehicle_driver_assignmentDTOToemployeeRecordNameBn.get(vm_vehicle_driver_assignmentDTO.employeeRecordNameBn).add(vm_vehicle_driver_assignmentDTO);
//
//					if( ! mapOfVm_vehicle_driver_assignmentDTOTounitName.containsKey(vm_vehicle_driver_assignmentDTO.unitName)) {
//						mapOfVm_vehicle_driver_assignmentDTOTounitName.put(vm_vehicle_driver_assignmentDTO.unitName, new HashSet<>());
//					}
//					mapOfVm_vehicle_driver_assignmentDTOTounitName.get(vm_vehicle_driver_assignmentDTO.unitName).add(vm_vehicle_driver_assignmentDTO);
//
//					if( ! mapOfVm_vehicle_driver_assignmentDTOTounitNameBn.containsKey(vm_vehicle_driver_assignmentDTO.unitNameBn)) {
//						mapOfVm_vehicle_driver_assignmentDTOTounitNameBn.put(vm_vehicle_driver_assignmentDTO.unitNameBn, new HashSet<>());
//					}
//					mapOfVm_vehicle_driver_assignmentDTOTounitNameBn.get(vm_vehicle_driver_assignmentDTO.unitNameBn).add(vm_vehicle_driver_assignmentDTO);
//
//					if( ! mapOfVm_vehicle_driver_assignmentDTOTopostName.containsKey(vm_vehicle_driver_assignmentDTO.postName)) {
//						mapOfVm_vehicle_driver_assignmentDTOTopostName.put(vm_vehicle_driver_assignmentDTO.postName, new HashSet<>());
//					}
//					mapOfVm_vehicle_driver_assignmentDTOTopostName.get(vm_vehicle_driver_assignmentDTO.postName).add(vm_vehicle_driver_assignmentDTO);
//
//					if( ! mapOfVm_vehicle_driver_assignmentDTOTopostNameBn.containsKey(vm_vehicle_driver_assignmentDTO.postNameBn)) {
//						mapOfVm_vehicle_driver_assignmentDTOTopostNameBn.put(vm_vehicle_driver_assignmentDTO.postNameBn, new HashSet<>());
//					}
//					mapOfVm_vehicle_driver_assignmentDTOTopostNameBn.get(vm_vehicle_driver_assignmentDTO.postNameBn).add(vm_vehicle_driver_assignmentDTO);
//
//					if( ! mapOfVm_vehicle_driver_assignmentDTOToremarks.containsKey(vm_vehicle_driver_assignmentDTO.remarks)) {
//						mapOfVm_vehicle_driver_assignmentDTOToremarks.put(vm_vehicle_driver_assignmentDTO.remarks, new HashSet<>());
//					}
//					mapOfVm_vehicle_driver_assignmentDTOToremarks.get(vm_vehicle_driver_assignmentDTO.remarks).add(vm_vehicle_driver_assignmentDTO);
//
//					if( ! mapOfVm_vehicle_driver_assignmentDTOToinsertedByUserId.containsKey(vm_vehicle_driver_assignmentDTO.insertedByUserId)) {
//						mapOfVm_vehicle_driver_assignmentDTOToinsertedByUserId.put(vm_vehicle_driver_assignmentDTO.insertedByUserId, new HashSet<>());
//					}
//					mapOfVm_vehicle_driver_assignmentDTOToinsertedByUserId.get(vm_vehicle_driver_assignmentDTO.insertedByUserId).add(vm_vehicle_driver_assignmentDTO);
//
//					if( ! mapOfVm_vehicle_driver_assignmentDTOToinsertedByOrganogramId.containsKey(vm_vehicle_driver_assignmentDTO.insertedByOrganogramId)) {
//						mapOfVm_vehicle_driver_assignmentDTOToinsertedByOrganogramId.put(vm_vehicle_driver_assignmentDTO.insertedByOrganogramId, new HashSet<>());
//					}
//					mapOfVm_vehicle_driver_assignmentDTOToinsertedByOrganogramId.get(vm_vehicle_driver_assignmentDTO.insertedByOrganogramId).add(vm_vehicle_driver_assignmentDTO);
//
//					if( ! mapOfVm_vehicle_driver_assignmentDTOToinsertionDate.containsKey(vm_vehicle_driver_assignmentDTO.insertionDate)) {
//						mapOfVm_vehicle_driver_assignmentDTOToinsertionDate.put(vm_vehicle_driver_assignmentDTO.insertionDate, new HashSet<>());
//					}
//					mapOfVm_vehicle_driver_assignmentDTOToinsertionDate.get(vm_vehicle_driver_assignmentDTO.insertionDate).add(vm_vehicle_driver_assignmentDTO);
//
//					if( ! mapOfVm_vehicle_driver_assignmentDTOTolastModificationTime.containsKey(vm_vehicle_driver_assignmentDTO.lastModificationTime)) {
//						mapOfVm_vehicle_driver_assignmentDTOTolastModificationTime.put(vm_vehicle_driver_assignmentDTO.lastModificationTime, new HashSet<>());
//					}
//					mapOfVm_vehicle_driver_assignmentDTOTolastModificationTime.get(vm_vehicle_driver_assignmentDTO.lastModificationTime).add(vm_vehicle_driver_assignmentDTO);
//
//					if( ! mapOfVm_vehicle_driver_assignmentDTOTosearchColumn.containsKey(vm_vehicle_driver_assignmentDTO.searchColumn)) {
//						mapOfVm_vehicle_driver_assignmentDTOTosearchColumn.put(vm_vehicle_driver_assignmentDTO.searchColumn, new HashSet<>());
//					}
//					mapOfVm_vehicle_driver_assignmentDTOTosearchColumn.get(vm_vehicle_driver_assignmentDTO.searchColumn).add(vm_vehicle_driver_assignmentDTO);
//
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Vm_vehicle_driver_assignmentDTO> getVm_vehicle_driver_assignmentList() {
		List <Vm_vehicle_driver_assignmentDTO> vm_vehicle_driver_assignments = new ArrayList<Vm_vehicle_driver_assignmentDTO>(this.mapOfVm_vehicle_driver_assignmentDTOToiD.values());
		return clone(vm_vehicle_driver_assignments);
	}
	
	
	public Vm_vehicle_driver_assignmentDTO getVm_vehicle_driver_assignmentDTOByIDWithoutClone( long ID){
		return mapOfVm_vehicle_driver_assignmentDTOToiD.get(ID);
	}

	public Vm_vehicle_driver_assignmentDTO getVm_vehicle_driver_assignmentDTOByID( long ID){
		return clone(mapOfVm_vehicle_driver_assignmentDTOToiD.get(ID));
	}
	
	
//	public List<Vm_vehicle_driver_assignmentDTO> getVm_vehicle_driver_assignmentDTOByvehicle_type_cat(int vehicle_type_cat) {
//		return new ArrayList<>( mapOfVm_vehicle_driver_assignmentDTOTovehicleTypeCat.getOrDefault(vehicle_type_cat,new HashSet<>()));
//	}
//
//
	public List<Vm_vehicle_driver_assignmentDTO> getVm_vehicle_driver_assignmentDTOByvehicle_id(long vehicle_id) {
		return clone(new ArrayList<>( mapOfVm_vehicle_driver_assignmentDTOTovehicleId.getOrDefault(vehicle_id,new HashSet<>())));
	}
//
//
//	public List<Vm_vehicle_driver_assignmentDTO> getVm_vehicle_driver_assignmentDTOBydriver_id(long driver_id) {
//		return new ArrayList<>( mapOfVm_vehicle_driver_assignmentDTOTodriverId.getOrDefault(driver_id,new HashSet<>()));
//	}
//
//
//	public List<Vm_vehicle_driver_assignmentDTO> getVm_vehicle_driver_assignmentDTOByemployee_record_name(String employee_record_name) {
//		return new ArrayList<>( mapOfVm_vehicle_driver_assignmentDTOToemployeeRecordName.getOrDefault(employee_record_name,new HashSet<>()));
//	}
//
//
//	public List<Vm_vehicle_driver_assignmentDTO> getVm_vehicle_driver_assignmentDTOByemployee_record_name_bn(String employee_record_name_bn) {
//		return new ArrayList<>( mapOfVm_vehicle_driver_assignmentDTOToemployeeRecordNameBn.getOrDefault(employee_record_name_bn,new HashSet<>()));
//	}
//
//
//	public List<Vm_vehicle_driver_assignmentDTO> getVm_vehicle_driver_assignmentDTOByunit_name(String unit_name) {
//		return new ArrayList<>( mapOfVm_vehicle_driver_assignmentDTOTounitName.getOrDefault(unit_name,new HashSet<>()));
//	}
//
//
//	public List<Vm_vehicle_driver_assignmentDTO> getVm_vehicle_driver_assignmentDTOByunit_name_bn(String unit_name_bn) {
//		return new ArrayList<>( mapOfVm_vehicle_driver_assignmentDTOTounitNameBn.getOrDefault(unit_name_bn,new HashSet<>()));
//	}
//
//
//	public List<Vm_vehicle_driver_assignmentDTO> getVm_vehicle_driver_assignmentDTOBypost_name(String post_name) {
//		return new ArrayList<>( mapOfVm_vehicle_driver_assignmentDTOTopostName.getOrDefault(post_name,new HashSet<>()));
//	}
//
//
//	public List<Vm_vehicle_driver_assignmentDTO> getVm_vehicle_driver_assignmentDTOBypost_name_bn(String post_name_bn) {
//		return new ArrayList<>( mapOfVm_vehicle_driver_assignmentDTOTopostNameBn.getOrDefault(post_name_bn,new HashSet<>()));
//	}
//
//
//	public List<Vm_vehicle_driver_assignmentDTO> getVm_vehicle_driver_assignmentDTOByremarks(String remarks) {
//		return new ArrayList<>( mapOfVm_vehicle_driver_assignmentDTOToremarks.getOrDefault(remarks,new HashSet<>()));
//	}
//
//
//	public List<Vm_vehicle_driver_assignmentDTO> getVm_vehicle_driver_assignmentDTOByinserted_by_user_id(long inserted_by_user_id) {
//		return new ArrayList<>( mapOfVm_vehicle_driver_assignmentDTOToinsertedByUserId.getOrDefault(inserted_by_user_id,new HashSet<>()));
//	}
//
//
//	public List<Vm_vehicle_driver_assignmentDTO> getVm_vehicle_driver_assignmentDTOByinserted_by_organogram_id(long inserted_by_organogram_id) {
//		return new ArrayList<>( mapOfVm_vehicle_driver_assignmentDTOToinsertedByOrganogramId.getOrDefault(inserted_by_organogram_id,new HashSet<>()));
//	}
//
//
//	public List<Vm_vehicle_driver_assignmentDTO> getVm_vehicle_driver_assignmentDTOByinsertion_date(long insertion_date) {
//		return new ArrayList<>( mapOfVm_vehicle_driver_assignmentDTOToinsertionDate.getOrDefault(insertion_date,new HashSet<>()));
//	}
//
//
//	public List<Vm_vehicle_driver_assignmentDTO> getVm_vehicle_driver_assignmentDTOBylastModificationTime(long lastModificationTime) {
//		return new ArrayList<>( mapOfVm_vehicle_driver_assignmentDTOTolastModificationTime.getOrDefault(lastModificationTime,new HashSet<>()));
//	}
//
//
//	public List<Vm_vehicle_driver_assignmentDTO> getVm_vehicle_driver_assignmentDTOBysearch_column(String search_column) {
//		return new ArrayList<>( mapOfVm_vehicle_driver_assignmentDTOTosearchColumn.getOrDefault(search_column,new HashSet<>()));
//	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "vm_vehicle_driver_assignment";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}

	public String getText(long id,String language){
		Vm_vehicle_driver_assignmentDTO dto = getVm_vehicle_driver_assignmentList().
													 stream().
													 filter(f -> f.driverId==id).findAny().get();
		return dto==null?"":("Bangla".equalsIgnoreCase(language)?dto.employeeRecordNameBn: dto.employeeRecordName);
	}

	public Vm_vehicle_driver_assignmentDTO clone(Vm_vehicle_driver_assignmentDTO dto) {
		String raw = gson.toJson(dto);
		return gson.fromJson(raw, Vm_vehicle_driver_assignmentDTO.class);
	}

	public List<Vm_vehicle_driver_assignmentDTO> clone(List<Vm_vehicle_driver_assignmentDTO> dtoList) {
		return dtoList
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}
}


