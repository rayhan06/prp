package vm_vehicle_driver_assignment;

import util.CommonDTO;

import static pbReport.DateUtils.ONE_DAY_IN_MILLIS;

public class Vm_vehicle_driver_assignmentDTO extends CommonDTO {
    public int vehicleTypeCat = -1;
    public String vehicleTypeCatMultiple = "";
    public long vehicleId = -1;
    public String vehicleIdMultiple = "";
    public long driverId = -1;
    public long unit_id = -1;
    public long post_id = -1;
    public String employeeRecordName = "";
    public String employeeRecordNameBn = "";
    public String unitName = "";
    public String unitNameBn = "";
    public String postName = "";
    public String postNameBn = "";
    public long insertedByUserId = -1;
    public long insertedByOrganogramId = -1;
    public long insertionDate = -1;

    public long start_time = 0;
    public long end_time = 0;
    public int count = 0;

    public boolean isActiveInTimeRange(long startInc, long endInc) {
        if (start_time > end_time) {
            return false;
        }
        if (end_time == 0) {
            return start_time <= endInc;
        }
        long lastEndTime = end_time + ONE_DAY_IN_MILLIS - 1;
        // end_time is 12AM of the date. date is valid upto 11:59PM that date.
        return !(endInc < start_time || lastEndTime < startInc);
    }

    @Override
    public String toString() {
        return "$Vm_vehicle_driver_assignmentDTO[" +
               " iD = " + iD +
               " vehicleTypeCat = " + vehicleTypeCat +
               " vehicleId = " + vehicleId +
               " driverId = " + driverId +
               " employeeRecordName = " + employeeRecordName +
               " employeeRecordNameBn = " + employeeRecordNameBn +
               " unitName = " + unitName +
               " unitNameBn = " + unitNameBn +
               " postName = " + postName +
               " postNameBn = " + postNameBn +
               " remarks = " + remarks +
               " insertedByUserId = " + insertedByUserId +
               " insertedByOrganogramId = " + insertedByOrganogramId +
               " insertionDate = " + insertionDate +
               " isDeleted = " + isDeleted +
               " lastModificationTime = " + lastModificationTime +
               " searchColumn = " + searchColumn +
               "]";
    }

}