package emp_travel_details;

import common.EmployeeCommonDAOService;
import common.NameDTO;
import employee_records.Employee_recordsDAO;
import employee_records.Employee_recordsDTO;
import geolocation.GeoCountryRepository;
import geolocation.GeoDistrictDTO;
import geolocation.GeoDistrictRepository;
import org.apache.log4j.Logger;
import pb.CatRepository;
import pb.CategoryLanguageModel;
import pb.Utils;
import util.CommonDTO;
import util.StringUtils;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@SuppressWarnings({"unused"})
public class Emp_travel_detailsDAO implements EmployeeCommonDAOService<Emp_travel_detailsDTO> {

    private static final Logger logger = Logger.getLogger(Emp_travel_detailsDAO.class);

    private static final String addSqlQuery = "INSERT INTO {tableName} (employee_records_id,travel_plan_number,travel_type_cat,model_of_travel_cat,travel_purpose_cat,travel_purpose_other,arranged_by,"
            + "training_calender_type,travel_destination,planned_cities,transport_facility_cat,is_accomodation_required,accomodation_address,"
            + "departure_date,return_date,detailed_purpose,number_of_working_days,planned_cost_bdt,is_advance_required,requested_advance_amount,"
            + "number_of_passengers,details_of_other_passengers,files_dropzone,insertion_date,inserted_by_user_id,modified_by,search_column,"
            + "job_cat,lastModificationTime,isDeleted,ID) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

    private static final String updateSqlQuery = "UPDATE {tableName} SET employee_records_id=?,travel_plan_number=?,travel_type_cat=?,model_of_travel_cat=?,travel_purpose_cat=?,travel_purpose_other=?,arranged_by=?,"
            + "training_calender_type=?,travel_destination=?,planned_cities=?,transport_facility_cat=?,is_accomodation_required=?,accomodation_address=?,"
            + "departure_date=?,return_date=?,detailed_purpose=?,number_of_working_days=?,planned_cost_bdt=?,is_advance_required=?,requested_advance_amount=?,"
            + "number_of_passengers=?,details_of_other_passengers=?,files_dropzone=?,insertion_date=?,inserted_by_user_id=?,modified_by=?,search_column=?,"
            + "job_cat=?,lastModificationTime = ? WHERE ID = ?";

    private final Map<String, String> searchMap = new HashMap<>();

    private final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

    private static final class InstanceHolder {
        static final Emp_travel_detailsDAO INSTANCE = new Emp_travel_detailsDAO();
    }

    public static Emp_travel_detailsDAO getInstance() {
        return InstanceHolder.INSTANCE;
    }

    private Emp_travel_detailsDAO() {
        searchMap.put("travel_type_cat", " and (travel_type_cat = ?)");
        searchMap.put("model_of_travel_cat", " and (model_of_travel_cat = ?)");
        searchMap.put("travel_purpose_cat", " and (travel_purpose_cat = ?)");
        searchMap.put("transport_facility_cat", " and (transport_facility_cat = ?)");
        searchMap.put("travel_destination", " and (travel_destination = ?)");
        searchMap.put("departure_date", " and (departure_date = ?)");
        searchMap.put("return_date", " and (return_date = ?)");
        searchMap.put("AnyField", " and (search_column like ?)");
    }

    public void set(PreparedStatement ps, Emp_travel_detailsDTO emp_travel_detailsDTO, boolean isInsert) throws SQLException {
        int index = 0;
        setSearchColumn(emp_travel_detailsDTO);
        long lastModificationTime = System.currentTimeMillis();
        System.out.println(emp_travel_detailsDTO.searchColumn);
        ps.setObject(++index, emp_travel_detailsDTO.employeeRecordsId);
        ps.setObject(++index, emp_travel_detailsDTO.travelPlanNumber);
        ps.setObject(++index, emp_travel_detailsDTO.travelTypeCat);
        ps.setObject(++index, emp_travel_detailsDTO.modelOfTravelCat);
        ps.setObject(++index, emp_travel_detailsDTO.travelPurposeCat);
        ps.setObject(++index, emp_travel_detailsDTO.travelPurposeOther);
        ps.setObject(++index, emp_travel_detailsDTO.arrangedBy);
        ps.setObject(++index, emp_travel_detailsDTO.trainingCalendarType);
        ps.setObject(++index, emp_travel_detailsDTO.travelDestination);
        ps.setObject(++index, emp_travel_detailsDTO.plannedCities);
        ps.setObject(++index, emp_travel_detailsDTO.transportFacilityCat);
        ps.setObject(++index, emp_travel_detailsDTO.isAccomodationRequired);
        ps.setObject(++index, emp_travel_detailsDTO.accomodationAddress);
        ps.setObject(++index, emp_travel_detailsDTO.departureDate);
        ps.setObject(++index, emp_travel_detailsDTO.returnDate);
        ps.setObject(++index, emp_travel_detailsDTO.detailedPurpose);
        ps.setObject(++index, emp_travel_detailsDTO.numberOfWorkingDays);
        ps.setObject(++index, emp_travel_detailsDTO.plannedCostBdt);
        ps.setObject(++index, emp_travel_detailsDTO.isAdvanceRequired);
        ps.setObject(++index, emp_travel_detailsDTO.requestedAdvanceAmount);
        ps.setObject(++index, emp_travel_detailsDTO.numberOfPassengers);
        ps.setObject(++index, emp_travel_detailsDTO.detailsOfOtherPassengers);
        ps.setObject(++index, emp_travel_detailsDTO.filesDropzone);
        ps.setObject(++index, emp_travel_detailsDTO.insertionDate);
        ps.setObject(++index, emp_travel_detailsDTO.insertedByUserId);
        ps.setObject(++index, emp_travel_detailsDTO.modifiedById);
        ps.setString(++index, emp_travel_detailsDTO.searchColumn);
        ps.setObject(++index, emp_travel_detailsDTO.jobCat);
        ps.setObject(++index, lastModificationTime);
        if (isInsert) {
            ps.setObject(++index, 0);
        }
        ps.setObject(++index, emp_travel_detailsDTO.iD);
    }

    public void setSearchColumn(Emp_travel_detailsDTO emp_travel_detailsDTO) {
        emp_travel_detailsDTO.searchColumn = "";
        Employee_recordsDTO employeeRecordsDTO = new Employee_recordsDAO().getEmployee_recordsDTOByID(emp_travel_detailsDTO.employeeRecordsId);
        if (employeeRecordsDTO.nameEng != null && employeeRecordsDTO.nameEng.trim().length() > 0) {
            emp_travel_detailsDTO.searchColumn += employeeRecordsDTO.nameEng.trim() + " ";
        }
        if (employeeRecordsDTO.nameBng != null && employeeRecordsDTO.nameBng.trim().length() > 0) {
            if (!emp_travel_detailsDTO.searchColumn.contains(employeeRecordsDTO.nameBng.trim())) {
                emp_travel_detailsDTO.searchColumn += employeeRecordsDTO.nameBng.trim() + " ";
            }
        }
        CategoryLanguageModel categoryLanguageModel = CatRepository.getInstance().getCategoryLanguageModel("travel_type", emp_travel_detailsDTO.travelTypeCat);
        setModelValueToSearchColumn(categoryLanguageModel, emp_travel_detailsDTO);

        Set<NameDTO> travelDestinationCountries = convertToSetOfT(emp_travel_detailsDTO.travelDestination,
                val -> GeoCountryRepository.getInstance().getDTOByID(val));
        emp_travel_detailsDTO.searchColumn += travelDestinationCountries.stream()
                .filter(e->e!=null && e.nameEn!=null && e.nameEn.length()>0)
                .map(e->e.nameEn.trim())
                .distinct()
                .collect(Collectors.joining(" "));
        emp_travel_detailsDTO.searchColumn += travelDestinationCountries.stream()
                .filter(e->e!=null && e.nameBn!=null && e.nameBn.length()>0)
                .map(e->e.nameBn.trim())
                .distinct()
                .collect(Collectors.joining(" "));

        categoryLanguageModel = CatRepository.getInstance().getCategoryLanguageModel("travel_purpose", emp_travel_detailsDTO.travelPurposeCat);
        setModelValueToSearchColumn(categoryLanguageModel, emp_travel_detailsDTO);
    }

    private void setModelValueToSearchColumn(CategoryLanguageModel categoryLanguageModel, Emp_travel_detailsDTO emp_travel_detailsDTO) {
        if (categoryLanguageModel != null) {
            if (categoryLanguageModel.englishText != null && categoryLanguageModel.englishText.trim().length() > 0
                    && !emp_travel_detailsDTO.searchColumn.contains(categoryLanguageModel.englishText.trim())) {
                emp_travel_detailsDTO.searchColumn += categoryLanguageModel.englishText.trim() + "";
            }
            if (categoryLanguageModel.banglaText != null && categoryLanguageModel.banglaText.trim().length() > 0
                    && !emp_travel_detailsDTO.searchColumn.contains(categoryLanguageModel.banglaText.trim())) {
                emp_travel_detailsDTO.searchColumn += categoryLanguageModel.banglaText.trim() + "";
            }
        }
    }

    public Emp_travel_detailsDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Emp_travel_detailsDTO emp_travel_detailsDTO = new Emp_travel_detailsDTO();
            emp_travel_detailsDTO.iD = rs.getLong("ID");
            emp_travel_detailsDTO.employeeRecordsId = rs.getLong("employee_records_id");
            emp_travel_detailsDTO.travelPlanNumber = rs.getString("travel_plan_number");
            emp_travel_detailsDTO.travelTypeCat = rs.getInt("travel_type_cat");
            emp_travel_detailsDTO.modelOfTravelCat = rs.getInt("model_of_travel_cat");
            emp_travel_detailsDTO.travelPurposeCat = rs.getInt("travel_purpose_cat");
            emp_travel_detailsDTO.travelPurposeOther = rs.getString("travel_purpose_other");
            emp_travel_detailsDTO.arrangedBy = rs.getString("arranged_by");
            emp_travel_detailsDTO.trainingCalendarType = rs.getLong("training_calender_type");
            emp_travel_detailsDTO.travelDestination = rs.getString("travel_destination");
            emp_travel_detailsDTO.plannedCities = rs.getString("planned_cities");
            emp_travel_detailsDTO.transportFacilityCat = rs.getInt("transport_facility_cat");
            emp_travel_detailsDTO.isAccomodationRequired = rs.getBoolean("is_accomodation_required");
            emp_travel_detailsDTO.accomodationAddress = rs.getString("accomodation_address");
            emp_travel_detailsDTO.departureDate = rs.getLong("departure_date");
            emp_travel_detailsDTO.returnDate = rs.getLong("return_date");
            emp_travel_detailsDTO.detailedPurpose = rs.getString("detailed_purpose");
            emp_travel_detailsDTO.numberOfWorkingDays = rs.getInt("number_of_working_days");
            emp_travel_detailsDTO.plannedCostBdt = rs.getInt("planned_cost_bdt");
            emp_travel_detailsDTO.isAdvanceRequired = rs.getBoolean("is_advance_required");
            emp_travel_detailsDTO.requestedAdvanceAmount = rs.getInt("requested_advance_amount");
            emp_travel_detailsDTO.numberOfPassengers = rs.getInt("number_of_passengers");
            emp_travel_detailsDTO.detailsOfOtherPassengers = rs.getString("details_of_other_passengers");
            emp_travel_detailsDTO.filesDropzone = rs.getLong("files_dropzone");
            emp_travel_detailsDTO.insertionDate = rs.getLong("insertion_date");
            emp_travel_detailsDTO.insertedByUserId = rs.getLong("inserted_by_user_id");
            emp_travel_detailsDTO.modifiedById = rs.getString("modified_by");
            emp_travel_detailsDTO.searchColumn = rs.getString("search_column");
            emp_travel_detailsDTO.jobCat = rs.getInt("job_cat");
            emp_travel_detailsDTO.isDeleted = rs.getInt("isDeleted");
            emp_travel_detailsDTO.lastModificationTime = rs.getLong("lastModificationTime");
            return emp_travel_detailsDTO;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    @Override
    public String getTableName() {
        return "emp_travel_details";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    @Override
    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Emp_travel_detailsDTO) commonDTO, addSqlQuery, true);
    }

    @Override
    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Emp_travel_detailsDTO) commonDTO, updateSqlQuery, false);
    }

    public List<Emp_travel_detailsDtoWithValue> getAllEmp_travel_detailsByEmployeeRecordsId(long employeeRecordsId) {
        return getByEmployeeId(employeeRecordsId)
                .stream()
                .sorted(Comparator.comparingLong(e -> e.departureDate))
                .map(this::buildEmp_travel_detailsDtoWithValueFromDTO)
                .collect(Collectors.toList());
    }
    private <T> String join(Set<T> list, Predicate<T> filter, Function<T, String> transform) {
        return list.stream()
                .filter(filter)
                .map(transform)
                .filter(e -> e != null && e.trim().length() > 0)
                .collect(Collectors.joining(","));
    }

    private <T> Set<T> convertToSetOfT(String s,Function<Long,T> transform){
        return Stream.of(s.split(","))
                .filter(option -> option.length() > 0)
                .map(String::trim)
                .map(Utils::convertToLong)
                .filter(Objects::nonNull)
                .map(transform)
                .collect(Collectors.toSet());
    }

    private Emp_travel_detailsDtoWithValue buildEmp_travel_detailsDtoWithValueFromDTO(Emp_travel_detailsDTO dto) {
        Emp_travel_detailsDtoWithValue model = new Emp_travel_detailsDtoWithValue();
        model.dto = dto;

        CategoryLanguageModel categoryLanguageModel = CatRepository.getInstance().getCategoryLanguageModel("travel_type_cat", dto.travelTypeCat);
        if (categoryLanguageModel != null) {
            model.travelTypeEng = categoryLanguageModel.englishText;
            model.travelTypeBng = categoryLanguageModel.banglaText;
        }

        if (dto.travelTypeCat == 1) {
            Set<GeoDistrictDTO> travelDestinationDistricts = convertToSetOfT(dto.travelDestination,
                    val -> GeoDistrictRepository.getInstance().getById(val));
            model.travelDestinationEng = join(travelDestinationDistricts,Objects::nonNull, e -> e.nameEng);
            model.travelDestinationBng = join(travelDestinationDistricts,Objects::nonNull, e -> e.nameBan);
        } else if (dto.travelTypeCat == 2) {
            Set<NameDTO> travelDestinationCountries = convertToSetOfT(dto.travelDestination,
                    val -> GeoCountryRepository.getInstance().getDTOByID(val));
            model.travelDestinationEng = join(travelDestinationCountries,Objects::nonNull, e -> e.nameEn);
            model.travelDestinationBng = join(travelDestinationCountries,Objects::nonNull, e -> e.nameBn);
        }

        if (dto.travelPurposeCat == Integer.MAX_VALUE) {
            model.travelPurposeEng = model.travelPurposeBng = dto.travelPurposeOther;
        } else {
            categoryLanguageModel = CatRepository.getInstance().getCategoryLanguageModel("travel_purpose", dto.travelPurposeCat);
            if (categoryLanguageModel != null) {
                model.travelPurposeEng = categoryLanguageModel.englishText;
                model.travelPurposeBng = categoryLanguageModel.banglaText;
            }
        }
        model.departureDateEng = StringUtils.getFormattedDate(true,dto.departureDate);
        model.departureDateBng = StringUtils.convertToBanNumber(model.departureDateEng);
        model.returnDateEng = StringUtils.getFormattedDate(true,dto.returnDate);
        model.returnDateBng =  StringUtils.convertToBanNumber(model.returnDateEng);

        return model;
    }
}