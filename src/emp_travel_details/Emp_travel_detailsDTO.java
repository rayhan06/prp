package emp_travel_details;

import sessionmanager.SessionConstants;
import util.CommonEmployeeDTO;


public class Emp_travel_detailsDTO extends CommonEmployeeDTO {
    public String travelPlanNumber = "";
    public int travelTypeCat = 0;
    public int modelOfTravelCat = 0;
    public int travelPurposeCat = 0;
    public String travelPurposeOther = "";
    public long trainingCalendarType = 0;
    public String travelDestination = "";
    public String plannedCities = "";
    public int transportFacilityCat = 0;
    public boolean isAccomodationRequired = false;
    public String accomodationAddress = "";
    public long departureDate = SessionConstants.MIN_DATE;
    public long returnDate = SessionConstants.MIN_DATE;
    public String detailedPurpose = "";
    public String arrangedBy = "";
    public int numberOfWorkingDays = 0;
    public int plannedCostBdt = 0;
    public boolean isAdvanceRequired = false;
    public int requestedAdvanceAmount = 0;
    public int numberOfPassengers = 0;
    public String detailsOfOtherPassengers = "";
    public long filesDropzone = 0;
    public long insertionDate = 0;
    public long insertedByUserId = 0;
    public String modifiedById = "";


    @Override
    public String toString() {
        return "$Emp_travel_detailsDTO[" +
                " iD = " + iD +
                " employeeRecordsId = " + employeeRecordsId +
                " travelPlanNumber = " + travelPlanNumber +
                " travelTypeCat = " + travelTypeCat +
                " modelOfTravelCat = " + modelOfTravelCat +
                " travelPurposeCat = " + travelPurposeCat +
                " trainingCalendarType = " + trainingCalendarType +
                " travelDestination = " + travelDestination +
                " plannedCities = " + plannedCities +
                " transportFacilityCat = " + transportFacilityCat +
                " isAccomodationRequired = " + isAccomodationRequired +
                " accomodationAddress = " + accomodationAddress +
                " departureDate = " + departureDate +
                " returnDate = " + returnDate +
                " detailedPurpose = " + detailedPurpose +
                " numberOfWorkingDays = " + numberOfWorkingDays +
                " plannedCostBdt = " + plannedCostBdt +
                " isAdvanceRequired = " + isAdvanceRequired +
                " requestedAdvanceAmount = " + requestedAdvanceAmount +
                " numberOfPassengers = " + numberOfPassengers +
                " detailsOfOtherPassengers = " + detailsOfOtherPassengers +
                " filesDropzone = " + filesDropzone +
                " insertionDate = " + insertionDate +
                " insertedByUserId = " + insertedByUserId +
                " modifiedById = " + modifiedById +
                " jobCat = " + jobCat +
                " isDeleted = " + isDeleted +
                " lastModificationTime = " + lastModificationTime +
                "]";
    }

}