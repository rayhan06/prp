package emp_travel_details;


public class Emp_travel_detailsDtoWithValue {

   public Emp_travel_detailsDTO dto;
   public String travelTypeEng = "";
   public String travelTypeBng = "";
   public String travelPurposeEng = "";
   public String travelPurposeBng = "";
   public String travelDestinationEng = "";
   public String travelDestinationBng = "";
   public String departureDateEng = "";
   public String departureDateBng = "";
   public String returnDateEng = "";
   public String returnDateBng = "";

}