package emp_travel_details;

import common.BaseServlet;
import common.EmployeeServletService;
import files.FilesDAO;
import language.LC;
import language.LM;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import sessionmanager.SessionConstants;
import user.UserDTO;
import util.CommonDTO;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

@SuppressWarnings("Duplicates")
@WebServlet("/Emp_travel_detailsServlet")
@MultipartConfig
public class Emp_travel_detailsServlet extends BaseServlet implements EmployeeServletService {
    private static final long serialVersionUID = 1L;

    public static Logger logger = Logger.getLogger(Emp_travel_detailsServlet.class);

    private final Emp_travel_detailsDAO emp_travel_detailsDAO = Emp_travel_detailsDAO.getInstance();

    private final FilesDAO filesDAO = new FilesDAO();

    public static final int travelPurposeOtherCategory = Integer.MAX_VALUE;

    @Override
    public String getTableName() {
        return emp_travel_detailsDAO.getTableName();
    }

    @Override
    public String getServletName() {
        return "Emp_travel_detailsServlet";
    }

    @Override
    public Emp_travel_detailsDAO getCommonDAOService() {
        return emp_travel_detailsDAO;
    }

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        Emp_travel_detailsDTO emp_travel_detailsDTO;
        SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
        long currentTime = System.currentTimeMillis();
        long employeeRecordsId = 0;
        String Value;
        if (addFlag) {
            emp_travel_detailsDTO = new Emp_travel_detailsDTO();
            emp_travel_detailsDTO.insertedByUserId = userDTO.ID;
            emp_travel_detailsDTO.insertionDate = currentTime;
            Value = request.getParameter("empId");
            if (Value != null && !Value.equalsIgnoreCase("")) {
                employeeRecordsId = Long.parseLong(Value);
            }
        } else {
            emp_travel_detailsDTO = emp_travel_detailsDAO.getDTOFromID(Long.parseLong(request.getParameter("iD")));
            employeeRecordsId = emp_travel_detailsDTO.employeeRecordsId;
        }
        emp_travel_detailsDTO.modifiedById = String.valueOf(userDTO.ID);
        emp_travel_detailsDTO.lastModificationTime = currentTime;


        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");

        emp_travel_detailsDTO.travelPlanNumber = "TP-EMP-" + formatter.format(date).trim().replaceAll("-", "") + "-" + employeeRecordsId; // TP-EMP-DDMMYYYY-EMPID

        emp_travel_detailsDTO.employeeRecordsId = employeeRecordsId;
        String value = Jsoup.clean(request.getParameter("travelTypeCat"), Whitelist.simpleText()).trim();
        if (!value.isEmpty()) {
            emp_travel_detailsDTO.travelTypeCat = Integer.parseInt(value);
        }

        value = Jsoup.clean(request.getParameter("modelOfTravelCat"), Whitelist.simpleText()).trim();
        if (!value.isEmpty()) {
            emp_travel_detailsDTO.modelOfTravelCat = Integer.parseInt(value);
        }


        value = Jsoup.clean(request.getParameter("travelPurposeCat"), Whitelist.simpleText()).trim();
        if (!value.isEmpty()) {
            emp_travel_detailsDTO.travelPurposeCat = Integer.parseInt(value);
        }
        value = Jsoup.clean(request.getParameter("transportFacilityCat"), Whitelist.simpleText()).trim();
        if (!value.isEmpty()) {
            emp_travel_detailsDTO.transportFacilityCat = Integer.parseInt(value);
        }
        if (emp_travel_detailsDTO.travelTypeCat == 1) {

            emp_travel_detailsDTO.travelDestination = Jsoup.clean(request.getParameter("district"), Whitelist.simpleText()).trim();
        } else {
            emp_travel_detailsDTO.travelDestination = Jsoup.clean(request.getParameter("country"), Whitelist.simpleText()).trim();
        }
        if (emp_travel_detailsDTO.travelDestination.trim().length() == 0) {
            throw new Exception(LM.getText(LC.EMP_TRAVEL_DETAILS_ADD_TRAVEL_DESTINATION, userDTO));
        }
        if (emp_travel_detailsDTO.travelPurposeCat == travelPurposeOtherCategory) {
            emp_travel_detailsDTO.travelPurposeOther = Jsoup.clean(request.getParameter("travelPurposeOther"), Whitelist.simpleText());
        } else {
            emp_travel_detailsDTO.travelPurposeOther = "";
        }
        emp_travel_detailsDTO.arrangedBy = Jsoup.clean(request.getParameter("arrangedBy"), Whitelist.simpleText());

        Value = request.getParameter("plannedCities");

        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }
        if (Value != null && Value.trim().length() > 0) {
            emp_travel_detailsDTO.plannedCities = Value.trim();
        } else {
            emp_travel_detailsDTO.plannedCities = "";
        }

        Value = request.getParameter("isAccomodationRequired");
        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }
        emp_travel_detailsDTO.isAccomodationRequired = Value != null && !Value.equalsIgnoreCase("");

        Value = request.getParameter("accomodationAddress");
        logger.debug("accomodationAddress = " + Value);
        if (Value != null && !Value.equalsIgnoreCase("")) {
            emp_travel_detailsDTO.accomodationAddress = Value;
        } else {
            emp_travel_detailsDTO.accomodationAddress = "";
            logger.debug("FieldName has a null Value, not updating" + " = " + Value);
        }
        Value = request.getParameter("departureDate");

        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }
        logger.debug("departureDate = " + Value);
        if (Value != null && !Value.equalsIgnoreCase("")) {
            try {
                Date d = f.parse(Value);
                emp_travel_detailsDTO.departureDate = d.getTime();
            } catch (Exception e) {
                emp_travel_detailsDTO.departureDate = SessionConstants.MIN_DATE;
                e.printStackTrace();
            }
        } else {
            emp_travel_detailsDTO.departureDate = SessionConstants.MIN_DATE;
            logger.debug("FieldName has a null Value, not updating" + " = " + Value);
        }

        Value = request.getParameter("returnDate");
        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }
        logger.debug("returnDate = " + Value);
        if (Value != null && !Value.equalsIgnoreCase("")) {
            try {
                Date d = f.parse(Value);
                emp_travel_detailsDTO.returnDate = d.getTime();
            } catch (Exception e) {
                emp_travel_detailsDTO.returnDate = SessionConstants.MIN_DATE;
                e.printStackTrace();
            }
        } else {
            emp_travel_detailsDTO.returnDate = SessionConstants.MIN_DATE;
            logger.debug("FieldName has a null Value, not updating" + " = " + Value);
        }

        Value = request.getParameter("detailedPurpose");

        if (Value != null) {
            Value = Value
                    .replaceAll("<strong>", "<b>")
                    .replaceAll("</strong>", "</b>")
                    .replaceAll("<em>", "<i>")
                    .replaceAll("</em>", "</i>")
                    .replaceAll("<s>", "<strike>")
                    .replaceAll("</s>", "</strike>");
        }
        logger.debug("detailedPurpose = " + Value);
        if (Value != null && Value.trim().length() > 0) {
            emp_travel_detailsDTO.detailedPurpose = Value.trim();
        } else {
            emp_travel_detailsDTO.detailedPurpose = "";
        }

        Value = request.getParameter("plannedCostBdt");
        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }
        logger.debug("plannedCostBdt = " + Value);
        if (Value != null && !Value.equalsIgnoreCase("")) {
            emp_travel_detailsDTO.plannedCostBdt = Integer.parseInt(Value);
        } else {
            emp_travel_detailsDTO.plannedCostBdt = 0;
        }

        Value = request.getParameter("isAdvanceRequired");
        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }
        logger.debug("isAdvanceRequired = " + Value);
        emp_travel_detailsDTO.isAdvanceRequired = Value != null && !Value.equalsIgnoreCase("");

        if (emp_travel_detailsDTO.isAdvanceRequired) {
            Value = request.getParameter("requestedAdvanceAmount");
            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            logger.debug("requestedAdvanceAmount = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                emp_travel_detailsDTO.requestedAdvanceAmount = Integer.parseInt(Value);
            } else {
                logger.debug("FieldName has a null Value, not updating" + " = " + Value);
            }
        } else {
            emp_travel_detailsDTO.requestedAdvanceAmount = 0;
        }


        Value = request.getParameter("numberOfPassengers");
        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }
        logger.debug("numberOfPassengers = " + Value);
        if (Value != null && !Value.equalsIgnoreCase("")) {
            emp_travel_detailsDTO.numberOfPassengers = Integer.parseInt(Value);
        } else {
            emp_travel_detailsDTO.numberOfPassengers = 0;
        }

        Value = request.getParameter("detailsOfOtherPassengers");

        if (Value != null) {
            Value = Value
                    .replaceAll("<strong>", "<b>")
                    .replaceAll("</strong>", "</b>")
                    .replaceAll("<em>", "<i>")
                    .replaceAll("</em>", "</i>")
                    .replaceAll("<s>", "<strike>")
                    .replaceAll("</s>", "</strike>");
        }
        logger.debug("detailsOfOtherPassengers = " + Value);
        if (Value != null && Value.trim().length() > 0) {
            emp_travel_detailsDTO.detailsOfOtherPassengers = Value.trim();
        } else {
            emp_travel_detailsDTO.detailsOfOtherPassengers = "";
        }

        Value = request.getParameter("filesDropzone");

        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }
        logger.debug("filesDropzone = " + Value);
        if (Value != null && !Value.equalsIgnoreCase("")) {
            logger.debug("filesDropzone = " + Value);
            if (!Value.equalsIgnoreCase("")) {
                emp_travel_detailsDTO.filesDropzone = Long.parseLong(Value);
            } else {
                logger.debug("FieldName has a null Value, not updating" + " = " + Value);
            }
            if (!addFlag) {
                String filesDropzoneFilesToDelete = request.getParameter("filesDropzoneFilesToDelete");
                String[] deleteArray = filesDropzoneFilesToDelete.split(",");
                for (int i = 0; i < deleteArray.length; i++) {
                    logger.debug("going to delete " + deleteArray[i]);
                    if (i > 0) {
                        filesDAO.delete(Long.parseLong(deleteArray[i]));
                    }
                }
            }
        } else {
            logger.debug("FieldName has a null Value, not updating" + " = " + Value);
        }

        Value = request.getParameter("jobCat");

        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }
        logger.debug("jobCat = " + Value);
        if (Value != null && !Value.equalsIgnoreCase("")) {

            emp_travel_detailsDTO.jobCat = Integer.parseInt(Value);
        } else {
            logger.debug("FieldName has a null Value, not updating" + " = " + Value);
        }

        if (addFlag) {
            emp_travel_detailsDAO.add(emp_travel_detailsDTO);
        } else {
            emp_travel_detailsDAO.update(emp_travel_detailsDTO);
        }
        return emp_travel_detailsDTO;
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[0];
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[0];
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[0];
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Emp_travel_detailsServlet.class;
    }

    @Override
    public String getAddRedirectURL(HttpServletRequest request, CommonDTO commonDTO) {
        return getAddOrEditOrDeleteRedirectURL(request, 5, "travel");
    }

    @Override
    public String getAjaxAddRedirectURL(HttpServletRequest request, CommonDTO commonDTO) {
        return getAddOrEditOrDeleteRedirectURL(request, 5, "travel");
    }

    @Override
    public String getEditRedirectURL(HttpServletRequest request, CommonDTO commonDTO) {
        return getAddOrEditOrDeleteRedirectURL(request, 5, "travel");
    }

    @Override
    public String getAjaxEditRedirectURL(HttpServletRequest request, CommonDTO commonDTO) {
        return getAddOrEditOrDeleteRedirectURL(request, 5, "travel");
    }

    @Override
    public String getDeleteRedirectURL(HttpServletRequest request) {
        return getAddOrEditOrDeleteRedirectURL(request, 5, "travel");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        search(request);
        super.doGet(request, response);
    }
}