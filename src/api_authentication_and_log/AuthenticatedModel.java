package api_authentication_and_log;

public class AuthenticatedModel {
    String agentAlias;
    String token;
    long expiry;

    public AuthenticatedModel(String agentAlias, String token, long expiry) {
        this.agentAlias = agentAlias;
        this.token = token;
        this.expiry = expiry;
    }
}
