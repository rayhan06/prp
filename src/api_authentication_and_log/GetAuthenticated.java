package api_authentication_and_log;

import common.ApiResponse;
import employee_records.EmployeeRecordsApiPerformer;
import org.apache.log4j.Logger;
import javax.servlet.http.HttpServletRequest;

public class GetAuthenticated implements EmployeeRecordsApiPerformer {
    public final Logger logger = Logger.getLogger(GetAuthenticated.class);

    @Override
    public ApiResponse perform(HttpServletRequest request) {
        try {
            String alias = request.getHeader("alias");
            String secret = request.getHeader("secret");

            AuthenticatedModel model =  APIAgentDAO.getInstance().getAuthenticatedModelByAlias(alias, secret);

            return model==null ? ApiResponse.makeResponseToResourceNotFound("Kindly check the alias and secret, else, contact the API provider!") :
                    ApiResponse.makeSuccessResponse(model, "This token will expires after One Hour!");
        } catch (Exception e) {
            e.printStackTrace();
            return ApiResponse.makeResponseToInternalServerError("Internal Server Error!");
        }
    }
}
