package api_authentication_and_log;

import common.ConnectionAndStatementUtil;
import org.apache.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public class APILogDAO {
    private static final Logger logger = Logger.getLogger(APILogDAO.class);

    private static final String insertLogData = "insert into api_log (api_agent_id, uri, api_method, request_time, ip_address, status) values (?,?,?,?,?,?)";

    private static APILogDAO INSTANCE = null;

    public static APILogDAO getInstance() {
        if (INSTANCE == null) {
            synchronized (APILogDAO.class) {
                if (INSTANCE == null) {
                    INSTANCE = new APILogDAO();
                }
            }
        }
        return INSTANCE;
    }

    public void insertApiLog(APILogDTO apiLogDTO) {
        ConnectionAndStatementUtil.getWriteConnectionAndPrepareStatement(model -> {
            PreparedStatement ps = (PreparedStatement) model.getStatement();
            try {
                ps.setObject(1, apiLogDTO.apiAgentId);
                ps.setObject(2, apiLogDTO.uri);
                ps.setObject(3, apiLogDTO.apiMethod);
                ps.setObject(4, apiLogDTO.requestTime);
                ps.setObject(5, apiLogDTO.ipAddress);
                ps.setObject(6, apiLogDTO.status ? 1 : 0);
                logger.debug(ps);
                ps.executeUpdate();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }, insertLogData);
    }

}
