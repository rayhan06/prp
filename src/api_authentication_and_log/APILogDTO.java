package api_authentication_and_log;

import sessionmanager.SessionConstants;

public class APILogDTO {
    long id = -1;
    long apiAgentId = -1;
    String uri = "";
    String apiMethod = "";
    long requestTime = SessionConstants.MIN_DATE;
    String ipAddress = "";
    boolean status = false;

    public APILogDTO(String alias, String uri, String apiMethod, long requestTime, String ipAddress, boolean status) {
        if (alias != null)
            this.apiAgentId = APIAgentDAO.getInstance().getAliasToIdMap().get(alias);
        this.uri = uri;
        this.apiMethod = apiMethod;
        this.requestTime = requestTime;
        this.ipAddress = ipAddress;
        this.status = status;
    }


    public String toString() {
        return "APILogDTO[" +
                " id = " + id +
                " apiAgentId = " + apiAgentId +
                " uri = " + uri +
                " apiMethod = " + apiMethod +
                " requestTime = " + requestTime +
                " ipAddress = " + ipAddress +
                " status = " + status +
                "]";
    }
}
