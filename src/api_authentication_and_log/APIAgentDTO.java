package api_authentication_and_log;

public class APIAgentDTO {
    long id = -1;
    String name_eng = "";
    String name_bng = "";
    String alias = "";
    String secret = "";
    String mobile = "";
    String email = "";
    boolean status = false;

    public static final int STATUS_ACTIVE = 1;

    public static final int AUTHENTICATED = 1;
    public static final int TOKEN_EXPIRED = 2;
    public static final int ALIAS_TOKEN_NULL_VALUE = 3;
    public static final int NO_TOKEN_GENERATED_SO_FAR = 4;
    public static final int TOKEN_MISMATCHED = 5;

    public String toString() {
        return "APIAgentDTO[" +
                " id = " + id +
                " name_eng = " + name_eng +
                " name_bng = " + name_bng +
                " alias = " + alias +
                " secret = " + secret +
                " mobile = " + mobile +
                " email = " + email +
                " status = " + status +
                "]";
    }
}
