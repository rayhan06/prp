package api_authentication_and_log;

import common.ConnectionAndStatementUtil;
import org.apache.log4j.Logger;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;

public class APIAgentDAO {
    private static final Logger logger = Logger.getLogger(APIAgentDAO.class);

    private static final String getByAlias = "select * from api_agent where alias = ? and status = 1";

    private static final String getAllActiveAgents = "select * from api_agent where status = 1";

    private static final String SALT_CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";

    private static APIAgentDAO INSTANCE = null;

    private static Map<String, TokenAndExpiry> aliasTokenMap = null;

    private static Map<String, Long> aliasToIdMap = null;

    private static List<APIAgentDTO> apiAgentDTOList = null;

    public static final long TOKEN_EXPIRE_TIME_IN_MILLISECOND = 3600000L;  // One hour, then token will be expired!

    public static APIAgentDAO getInstance() {
        if (INSTANCE == null) {
            synchronized (APIAgentDAO.class) {
                if (INSTANCE == null) {
                    INSTANCE = new APIAgentDAO();
                    aliasTokenMap = new HashMap<>();
                    apiAgentDTOList = INSTANCE.reload();
                    aliasToIdMap = getAliasToIdMap(apiAgentDTOList);
                }
            }
        }
        return INSTANCE;
    }

    public static Map<String, Long> getAliasToIdMap(List<APIAgentDTO> apiAgentDTOs) {
        return apiAgentDTOs.stream().collect(Collectors.toMap(dto -> dto.alias, dto -> dto.id));
    }

    public APIAgentDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            APIAgentDTO apiAgentDTO = new APIAgentDTO();
            apiAgentDTO.id = rs.getLong("ID");
            apiAgentDTO.name_eng = rs.getString("name_eng");
            apiAgentDTO.name_bng = rs.getString("name_bng");
            apiAgentDTO.alias = rs.getString("alias");
            apiAgentDTO.secret = rs.getString("secret");
            apiAgentDTO.mobile = rs.getString("mobile");
            apiAgentDTO.email = rs.getString("email");
            apiAgentDTO.status = rs.getInt("status") == APIAgentDTO.STATUS_ACTIVE;

            return apiAgentDTO;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    public APIAgentDTO getDTOByAlias(String alias) {
        return ConnectionAndStatementUtil.getT(getByAlias, ps->{
            try {
                ps.setObject(1,alias);
            } catch (SQLException ex) {
                logger.error(ex);
            }
        },this::buildObjectFromResultSet);
    }

    public List<APIAgentDTO> reload() {
        return ConnectionAndStatementUtil.getListOfT(getAllActiveAgents, ps -> {}, this::buildObjectFromResultSet);
    }

    public AuthenticatedModel getAuthenticatedModelByAlias(String alias, String secret) {

        if (alias==null || alias.equals(""))
            return null;

        APIAgentDTO apiAgentDTO = getDTOByAlias(alias);

        if (apiAgentDTO==null || !apiAgentDTO.secret.equals(secret))
            return null;

        TokenAndExpiry tokenAndExpiry = new TokenAndExpiry(getSaltString(), System.currentTimeMillis());

        aliasTokenMap.put(alias, tokenAndExpiry);

        return new AuthenticatedModel(alias, tokenAndExpiry.token, tokenAndExpiry.expiry);
    }

    private String getSaltString() {
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 32) {                // it is actually the length of the random salt string
            int index = (int) (rnd.nextFloat() * SALT_CHARACTERS.length());
            salt.append(SALT_CHARACTERS.charAt(index));
        }
        return salt.toString();
    }


    public int getAuthenticated(String alias, String token) {

        if (alias==null || token==null)
            return APIAgentDTO.ALIAS_TOKEN_NULL_VALUE;

        TokenAndExpiry tokenAndExpiry = aliasTokenMap.get(alias);

        if (tokenAndExpiry == null)
            return APIAgentDTO.NO_TOKEN_GENERATED_SO_FAR;

        if (!tokenAndExpiry.token.equals(token))
            return APIAgentDTO.TOKEN_MISMATCHED;

        if (tokenAndExpiry.expiry < System.currentTimeMillis())
            return APIAgentDTO.TOKEN_EXPIRED;

        return APIAgentDTO.AUTHENTICATED;
    }

    public List<APIAgentDTO> getAllActiveAgents() {
        return apiAgentDTOList;
    }

    public Map<String, Long> getAliasToIdMap() {
        return aliasToIdMap;
    }

}

class TokenAndExpiry {
    public String token;
    public long expiry;

    public TokenAndExpiry(String token, long currentTime) {
        this.token = token;
        this.expiry = currentTime + APIAgentDAO.TOKEN_EXPIRE_TIME_IN_MILLISECOND;
    }
}
