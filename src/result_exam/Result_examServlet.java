package result_exam;

import common.BaseServlet;
import common.NameDao;
import common.NameRepository;
import common.NameInterface;
import language.LC;
import permission.MenuConstants;
import sessionmanager.SessionConstants;
import user.UserDTO;
import util.CommonDTO;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;

@WebServlet("/Result_examServlet")
@MultipartConfig
public class Result_examServlet extends BaseServlet implements NameInterface {

    public String commonPartOfDispatchURL(){
        return  "common/name";
    }

    @Override
    public String getTableName() {
        return Result_examDAO.getInstance().getTableName();
    }

    @Override
    public String getServletName() {
        return "Result_examServlet";
    }

    @Override
    public NameDao getCommonDAOService() {
        return Result_examDAO.getInstance();
    }

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        return addT(request,addFlag,userDTO,Result_examDAO.getInstance());
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[] {MenuConstants.RESULT_EXAM_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[] {MenuConstants.RESULT_EXAM_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[] {MenuConstants.RESULT_EXAM_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Result_examServlet.class;
    }

    @Override
    public int getSearchTitleValue() {
        return LC.RESULT_EXAM_SEARCH_RESULT_EXAM_SEARCH_FORMNAME;
    }

    @Override
    public String get_p_navigatorName() {
        return SessionConstants.NAV_RESULT_EXAM;
    }

    @Override
    public String get_p_dtoCollectionName() {
        return SessionConstants.VIEW_RESULT_EXAM;
    }

    @Override
    public NameRepository getNameRepository() {
        return Result_examRepository.getInstance();
    }

    @Override
    public int getAddTitleValue() {
        return LC.RESULT_EXAM_ADD_RESULT_EXAM_ADD_FORMNAME;
    }

    @Override
    public int getEditTitleValue() {
        return LC.RESULT_EXAM_EDIT_RESULT_EXAM_EDIT_FORMNAME;
    }

    @Override
    public void init(HttpServletRequest request) throws ServletException {
        setCommonAttributes(request,getServletName(),getCommonDAOService());
    }
}