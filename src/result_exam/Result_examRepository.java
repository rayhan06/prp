package result_exam;

import common.NameRepository;

public class Result_examRepository extends NameRepository {

	private Result_examRepository() {
		super(Result_examDAO.getInstance(),Result_examRepository.class);
	}
	
	private static class LazyHolderResult_examRepository {
        static final Result_examRepository INSTANCE = new Result_examRepository();
    }

	public static Result_examRepository getInstance() {
		return LazyHolderResult_examRepository.INSTANCE;
	}
}
