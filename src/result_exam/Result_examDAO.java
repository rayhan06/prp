package result_exam;

import common.NameDao;

public class Result_examDAO extends NameDao {

	private Result_examDAO() {
		super("result_exam");
	}

	private static class LazyLoader{
		static final Result_examDAO INSTANCE = new Result_examDAO();
	}

	public static Result_examDAO getInstance(){
		return LazyLoader.INSTANCE;
	}
}
