package geolocation;

/*
 * @author Md. Erfan Hossain
 * @created 09/08/2021 - 11:48 AM
 * @project parliament
 */

import common.ConnectionAndStatementUtil;
import org.apache.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

public class GeoMigrate2 {
    private static final Logger logger = Logger.getLogger(GeoMigrate2.class);
    private static final String sql ="select id,present_address,present_division,present_district, permanent_address,permanent_division,permanent_district from employee_records where present_address like '%@@%' or permanent_address like '%@@%' order by id";
    private static final String updateSQL = "update employee_records SET present_division = ?,present_district=?,permanent_division=?,permanent_district=? WHERE id = ?";
    private static class Location2{
        long id;
        String presentAddress;
        long presentDivision;
        long presentDistrict;
        String permanentAddress;
        long permanentDivision;
        long permanentDistrict;

        @Override
        public String toString() {
            return "Location2{" +
                    "id=" + id +
                    ", presentAddress='" + presentAddress + '\'' +
                    ", permanentAddress='" + permanentAddress + '\'' +
                    '}';
        }
    }

    private static class Location3{
        long id;
        long presentDivision;
        long presentDistrict;
        long permanentDivision;
        long permanentDistrict;

    }
    public static void main(String[] args) {
        List<Location2> list = ConnectionAndStatementUtil.getListOfT(sql,rs->{
            Location2 location2 = new Location2();
            try {
                location2.id = rs.getLong("id");
                location2.presentAddress = rs.getString("present_address");
                location2.presentDivision = rs.getLong("present_division");
                location2.presentDistrict = rs.getLong("present_district");
                location2.permanentAddress = rs.getString("permanent_address");
                location2.permanentDivision = rs.getLong("permanent_division");
                location2.permanentDistrict = rs.getLong("permanent_district");
                return location2;
            } catch (SQLException ex) {
                ex.printStackTrace();
                return null;
            }
        });

        logger.debug("Size : "+list.size());

        Map<Long,Location2> map = list.parallelStream()
                .collect(Collectors.toMap(e->e.id,e->e));

        List<Location3> location3List = list.stream()
                .peek(System.out::println)
                .map(GeoMigrate2::buildLocation3)
                .filter(Objects::nonNull)
                .filter(e->!match(e,map.get(e.id)))
                .collect(Collectors.toList());

        logger.debug("Size2 : "+location3List.size());

        ConnectionAndStatementUtil.getWriteConnectionAndPrepareStatement(model->{
            PreparedStatement ps = (PreparedStatement) model.getStatement();
            location3List.forEach(e->{
                try {
                    ps.setLong(1,e.presentDivision);
                    ps.setLong(2,e.presentDistrict);
                    ps.setLong(3,e.permanentDivision);
                    ps.setLong(4,e.permanentDistrict);
                    ps.setLong(5,e.id);
                    logger.debug(ps);
                    ps.executeUpdate();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            });
        },updateSQL);

    }

    private static Location3 buildLocation3(Location2 e) {
        String[] present = e.presentAddress.split("@@");
        String[] permanent = e.permanentAddress.split("@@");
        if(present.length == 5 && permanent.length == 5){
            try{
                Location3 location3 = new Location3();
                location3.presentDivision = Long.parseLong(present[0]);
                location3.presentDistrict = Long.parseLong(present[1]);
                location3.permanentDivision = Long.parseLong(permanent[0]);
                location3.permanentDistrict = Long.parseLong(permanent[1]);
                location3.id = e.id;
                return location3;
            }catch (Exception ex){
                ex.printStackTrace();
                return null;
            }
        }
        return null;
    }

    private static boolean match(Location3 location3,Location2 location2){
        return location2.presentDivision == location3.presentDivision && location2.presentDistrict == location3.presentDistrict &&
                location2.permanentDivision == location3.permanentDivision && location2.permanentDistrict == location3.permanentDistrict;
    }
}
