package geolocation;

import common.ConnectionAndStatementUtil;
import dbm.DBMR;
import jdk.nashorn.internal.ir.WhileNode;
import pb.CategoryLanguageModel;

import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class GeoUpozilaDAO{
    private static final Logger logger = Logger.getLogger(GeoDistrictDAO.class);

    private static final String  getAllSqlQuery= "SELECT * FROM geo_upazilas";

    private static final String getSqlById = "SELECT * FROM geo_upazilas WHERE ID = ?";

    private static final String getSQLByDistrictId = "SELECT * FROM geo_upazilas WHERE geo_district_id = ?";

    private static final String getSQLByUpozilasName = "SELECT * FROM geo_upazilas WHERE ";

    private GeoUpozilaDTO buildGeoUpozilaDTOFromResultSet(ResultSet rs){
        try {
            GeoUpozilaDTO dto = new GeoUpozilaDTO();
            dto.id = rs.getLong("id");
            dto.geoDistrictId = rs.getLong("geo_district_id");
            dto.bbsCode = rs.getString("bbs_code");
            dto.nameEng = rs.getString("upazila_name_eng");
            dto.nameBan = rs.getString("upazila_name_bng");
            return dto;
        } catch (SQLException ex) {
            logger.error(ex);
        }
        return null;
    }
    public List<GeoUpozilaDTO> getAllUpozilas(){
        return ConnectionAndStatementUtil.getListOfT(getAllSqlQuery,this::buildGeoUpozilaDTOFromResultSet);
    }

    public GeoUpozilaDTO getById(long id){
        return ConnectionAndStatementUtil.getT(getSqlById,ps->{
            try {
                ps.setObject(1,id);
            } catch (SQLException ex) {
                logger.error(ex);
            }
        },this::buildGeoUpozilaDTOFromResultSet);
    }

    public List<GeoUpozilaDTO> getByDistrictId(long id){
        return ConnectionAndStatementUtil.getListOfT(getSQLByDistrictId,ps->{
            try {
                ps.setObject(1,id);
            } catch (SQLException ex) {
                logger.error(ex);
            }
        },this::buildGeoUpozilaDTOFromResultSet);
    }
//    public static String getUpozilaOption(String Language, String savedDistrict) {
//        String languagetextColumn, options;
//        Connection connection = null;
//        ResultSet rs = null;
//        Statement stmt = null;
//
//
//        if (Language.equalsIgnoreCase("English")) {
//            languagetextColumn = "district_name_eng";
//            options = "<option value=\"select\">Select</option>";
//        } else {
//            languagetextColumn = "district_name_bng";
//            options = "<option value=\"select\">বাছাই করুন</option>";
//        }
//
//
//        if (!savedDistrict.equals("")) {
//            options = "<option value=\""+savedDistrict+"\">"+savedDistrict+"</option>";
//        }
//
//
//        try{
//
//            final String getOptionSQL = "SELECT " + languagetextColumn + " FROM geo_districts";
//            String district;
//
//            connection = DBMR.getInstance().getConnection();
//            stmt = connection.createStatement();
//            logger.debug(getOptionSQL);
//
//            rs = stmt.executeQuery(getOptionSQL);
//            while (rs.next()) {
//                district = rs.getString(languagetextColumn);
//                options += "<option value=\""+district+"\">"+district+"</option>";
//            }
//
//        }catch(Exception ex){
//            ex.printStackTrace();
//        }finally{
//            try{
//                if (stmt != null) {
//                    stmt.close();
//                }
//            } catch (Exception e){}
//
//            try{
//                if (connection != null){
//                    DBMR.getInstance().freeConnection(connection);
//                }
//            }catch(Exception ex2){}
//        }
//
//        return options;
//    }

    public List<GeoUpozilaDTO> getByUpazilaName(String Language, String name){

        String matchColumn = "";
        matchColumn = Language.equals("Bangla")?"upazila_name_bng":"upazila_name_eng";

        return ConnectionAndStatementUtil.getListOfT(getSQLByUpozilasName + matchColumn + " = '" + name + "'", ps->{
        },this::buildGeoUpozilaDTOFromResultSet);
    }
}
