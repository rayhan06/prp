package geolocation;

public class DivisionModel {
    public long id;
    public String nameEng;
    public String nameBng;
    public String bbsCode;

    public DivisionModel(GeoDivisionDTO dto) {
        id = dto.id;
        nameEng = dto.nameEng;
        nameBng = dto.nameBan;
        bbsCode = dto.bbsCode;
    }
}
