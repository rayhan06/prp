package geolocation;

public class GeoDistrictDTO {
    public long id;
    public long geoDivisionId;
    public String nameEng;
    public String nameBan;
    public String bbsCode;
}
