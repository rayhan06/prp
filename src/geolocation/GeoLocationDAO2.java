package geolocation;

import org.apache.log4j.Logger;

import java.util.*;
import java.util.stream.Collectors;

public class GeoLocationDAO2 {
    private static final Logger logger = Logger.getLogger(GeoLocationDAO2.class);

    private static final GeoLocationRepository geoLocationRepository = GeoLocationRepository.getInstance();

    private static final GeoLocationTypeRepository geoLocationTypeRepository = GeoLocationTypeRepository.getInstance();

    public static List<GeoLocationDTO> getLocation(int parentID, int geoLevelID) throws Exception {
        return geoLocationRepository.getByParentIdAndLevelId(parentID,geoLevelID);
    }

    public static String getLevelName(int geo_level_id, String language) throws Exception {
    	String smLanguage = language.toLowerCase();
    	if(!smLanguage.equalsIgnoreCase("bangla") && !smLanguage.equalsIgnoreCase("english"))
    	{
    		language = "bangla";
    	}
        GeoLocationTypeDTO locationTypeDTO = geoLocationTypeRepository.getById(geo_level_id);
        return locationTypeDTO == null ? "Unknown" : "Bangla".equalsIgnoreCase(language) ? locationTypeDTO.nameBn : locationTypeDTO.nameEn;
    }

    public static int getLevel(int id) throws Exception {
        GeoLocationDTO locationDTO = geoLocationRepository.getById(id);
        return locationDTO == null ? -1 : locationDTO.geoLevelID;
    }

    public static String getLocationText(int id) {
        try {
            return getLocationText(id, "name_en");
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return "";
        }
    }
    
    public static String getAddressToShow(String value, String Language)
    {
    	String smLanguage = Language.toLowerCase();
    	if(!smLanguage.equalsIgnoreCase("bangla") && !smLanguage.equalsIgnoreCase("english"))
    	{
    		Language = "bangla";
    	}
    	String address = "";
    
    	String addressdetails = GeoLocationDAO2.parseDetails(value);
		if(!addressdetails.equals(""))
		{
			address += addressdetails + ", ";
		}
		address += parseText(value, Language);
		return address;
    }
    
    public static String getAddressToSave(String value)
    {
    	String address = "";
//		StringTokenizer tok3=new StringTokenizer(value, "$");
//		int i = 0;
//		while(tok3.hasMoreElements())
//		{
//			if(i == 0)
//			{
//				address_id = tok3.nextElement() + "";
//			}
//			else if(i == 1)
//			{
//				addressDetails = tok3.nextElement() + "";
//			}
////			else {
////                String temp = (String) tok3.nextElement();
////                System.out.println(temp);
////            }
//			i ++;
//		}

        String[] addresses = value.split("\\$", 2);
        String addressDetails = "", address_id = "";

        try {
            address_id = addresses[0];
            addressDetails = addresses[1];
        } catch (Exception ex) {
            ex.printStackTrace();
        }
		try
		{
			if(address_id.matches("-?\\d+"))
			{
				address
				= address_id + "$" 
				+ GeoLocationDAO2.getLocationText(Integer.parseInt(address_id), "English") + "$" 
				+ addressDetails + "$"
				+ GeoLocationDAO2.getLocationText(Integer.parseInt(address_id), "Bangla");
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return address;
    }

    public static String getLocationText(int id, String language) throws Exception {
        List<String> list = new ArrayList<>();
        boolean isLanguageBangla = "Bangla".equalsIgnoreCase(language);
        while (id!=1){
            GeoLocationDTO dto = geoLocationRepository.getById(id);
            if(dto == null){
                logger.debug("GeoLocationDTO is not found for id : "+id);
                break;
            }
            list.add(isLanguageBangla?dto.name_bn: dto.name_en);
            id = dto.parentID;
        }
        return String.join(", ", list);
    }

    /* *
     * index 0 has id value
     * index 1 has id EnglishText
     * index 2 has id Details
     * index 3 has id BanglaText
     * */
    public static String parse(String Datatype, String Value, String language) {
        if(Value == null){
            return "";
        }
        String[] tokens = Value.split("\\$");
        switch (Datatype){
            case "id":
                return tokens.length>0 ? tokens[0] : "";
            case "enText":
                return tokens.length>1 ? tokens[1] : "";
            case "details":
                return tokens.length>2 ? tokens[2] : "";
            case "bnText":
                return tokens.length>3 ? tokens[3] : "";
            case "text":
                return "bangla".equalsIgnoreCase(language) ? (tokens.length>3 ? tokens[3] : "") : tokens.length>1 ? tokens[1] : "";
            default:
                return "";
        }
    }

    public static String parseID(String Value) {
        return parse("id", Value, "english");
    }

    public static String parseText(String Value, String language) {
    	String smLanguage = language.toLowerCase();
    	if(!smLanguage.equalsIgnoreCase("bangla") && !smLanguage.equalsIgnoreCase("english"))
    	{
    		language = "bangla";
    	}
        return parse("text", Value, language);
    }

    public static String parseEnText(String Value) {
        return parse("text", Value, "english");
    }

    public static String parseBnText(String Value) {
        return parse("bnText", Value, "Bangla");
    }

    public static String parseDetails(String Value) {
        return parse("details", Value, "english");
    }

    public static String parseTextAndDetails(String Value) {
        if (Value == null || Value.isEmpty()) {
            return "";
        }
        String Str = GeoLocationDAO2.parseEnText(Value);
        String Details = GeoLocationDAO2.parseDetails(Value);
        if (!Details.equals("")) {
            Str += ", " + Details;
        }
        return Str;
    }


    public static String parseAddress(String address) {
        try{
            String presentableAddress = "";

            if (address == null || address.isEmpty() || address.equals("$$")) {
                return "";
            }

            String[] splittedAddress = address.split(":");
            if (splittedAddress.length == 0) {
                return "";
            }
            if (splittedAddress[1].isEmpty()) {
                return splittedAddress[2];
            }

            if (splittedAddress.length >= 3) {
                presentableAddress = splittedAddress[2];
            }

            List<String> geoGraphicalAddress = Arrays.asList(splittedAddress[1].split(","));

            Collections.reverse(geoGraphicalAddress);

            return geoGraphicalAddress.stream().collect(Collectors.joining(",",presentableAddress,""));
        }catch (Exception ex){
            logger.error(ex);
            return "";
        }
    }
}
