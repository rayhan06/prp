package geolocation;

import common.ApiResponse;
import javax.servlet.http.HttpServletRequest;

public interface GeoApiPerformer {
    ApiResponse perform(HttpServletRequest request);
}
