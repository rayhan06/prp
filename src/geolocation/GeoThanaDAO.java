package geolocation;

import common.ConnectionAndStatementUtil;
import org.apache.log4j.Logger;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

class GeoThanaDAO {
    private static final Logger logger = Logger.getLogger(GeoThanaDAO.class);

    private static final String  getAllSqlQuery= "SELECT * FROM geo_thanas";

    private static final String getSqlById = "SELECT * FROM geo_thanas WHERE ID = ?";

    private static final String getSQLByDistrictId = "SELECT * FROM geo_thanas WHERE geo_district_id = ?";

    private GeoThanaDTO buildGeoThanaDTOFromResultSet(ResultSet rs){
        try {
            GeoThanaDTO dto = new GeoThanaDTO();
            dto.id = rs.getLong("id");
            dto.geoDivisionId = rs.getLong("geo_division_id");
            dto.geoDistrictId = rs.getLong("geo_district_id");
            dto.bbsCode = rs.getString("bbs_code");
            dto.nameEng = rs.getString("thana_name_eng");
            dto.nameBan = rs.getString("thana_name_bng");
            return dto;
        } catch (SQLException ex) {
            logger.error(ex);
        }
        return null;
    }
    public List<GeoThanaDTO> getAllThanas(){
        return ConnectionAndStatementUtil.getListOfT(getAllSqlQuery,this::buildGeoThanaDTOFromResultSet);
    }

    public GeoThanaDTO getById(long id){
        return ConnectionAndStatementUtil.getT(getSqlById,ps->{
            try {
                ps.setObject(1,id);
            } catch (SQLException ex) {
                logger.error(ex);
            }
        },this::buildGeoThanaDTOFromResultSet);
    }

    public  List<GeoThanaDTO> getByDistrictId(long id){
        return ConnectionAndStatementUtil.getListOfT(getSQLByDistrictId,ps->{
            try {
                ps.setObject(1,id);
            } catch (SQLException ex) {
                logger.error(ex);
            }
        },this::buildGeoThanaDTOFromResultSet);
    }
}
