package geolocation;

import org.apache.log4j.Logger;
import pb.OptionDTO;
import pb.OrderByEnum;
import pb.Utils;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;


@SuppressWarnings({"unused", "Duplicates"})
public class GeoDistrictRepository {
    private static final Logger logger = Logger.getLogger(GeoDistrictRepository.class);
    private Map<Long, List<GeoDistrictDTO>> map;
    private Map<Long, GeoDistrictDTO> mapById;
    private Map<String, GeoDistrictDTO> mapByEngText;
    private Map<String, GeoDistrictDTO> mapByBngText;
    private List<GeoDistrictDTO> allDistrictDTO;

    private GeoDistrictRepository() {
        reload();
    }

    private static class LazyLoader {
        static GeoDistrictRepository INSTANCE = new GeoDistrictRepository();
    }

    public static GeoDistrictRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

    private void reload() {
        logger.debug("GeoDistrictRepository reload start");
        allDistrictDTO = new GeoDistrictDAO().getAllDistricts();
        map = allDistrictDTO
                .parallelStream()
                .collect(Collectors.groupingBy(dto -> dto.geoDivisionId,
                        Collectors.collectingAndThen(
                                Collectors.toList(),
                                l -> {
                                    l.sort(Comparator.comparing(o -> o.nameEng));
                                    return l;
                                })));
        mapById = allDistrictDTO.parallelStream()
                .collect(Collectors.toMap(dto -> dto.id, dto -> dto));
        allDistrictDTO.sort(Comparator.comparing(o -> o.nameEng));
        mapByEngText = allDistrictDTO.parallelStream()
                .collect(Collectors.toMap(e -> e.nameEng.toUpperCase(), e -> e));
        mapByBngText = allDistrictDTO.parallelStream()
                .collect(Collectors.toMap(e -> e.nameBan, e -> e));
        logger.debug("GeoDistrictRepository reload end");
    }

    public List<GeoDistrictDTO> getGeoDistrictDTOListByDivisionId(long divisionId) {
        return map.get(divisionId);
    }

    public GeoDistrictDTO getById(long id) {
        return mapById.get(id);
    }

    public List<GeoDistrictDTO> getAllDistrict() {
        return allDistrictDTO;
    }

    public String getDistrictOptionUpdated(String language, Long selectedId) {
        return buildOptionsByDivision(language, null, selectedId);
    }

    public String buildOptionsByDivision(String language, Long divisionId, Long selectedId) {
        List<GeoDistrictDTO> list;
        if (divisionId == null || map.get(divisionId) == null) {
            list = allDistrictDTO;
        } else {
            list = map.get(divisionId);
        }
        List<OptionDTO> optionDTOList = null;
        if (list != null && list.size() > 0) {
            optionDTOList = list.stream()
                    .map(dto -> new OptionDTO(dto.nameEng, dto.nameBan, String.valueOf(dto.id)))
                    .collect(Collectors.toList());
        }
        return Utils.buildOptions(optionDTOList, language, selectedId == null ? null : String.valueOf(selectedId));
    }

    public String getText(String language, long id) {

        GeoDistrictDTO districtDTO = getById(id);
        return districtDTO == null ? ""
                : (language.equalsIgnoreCase("English")) ? (districtDTO.nameEng == null ? "" : districtDTO.nameEng)
                : (districtDTO.nameBan == null ? "" : districtDTO.nameBan);
    }


    public String getTexts(String language, String ids) {

        Set<GeoDistrictDTO> dtos = Stream.of(ids.split(","))
                .filter(option -> option.length() > 0)
                .map(option -> option.trim())
                .map(Utils::convertToLong)
                .filter(Objects::nonNull)
                .map(val -> GeoDistrictRepository.getInstance().getById(val))
                .collect(Collectors.toSet());
        String banglaText = "", englishText = "";
        for (GeoDistrictDTO dto : dtos) {
            if (dto != null) {
                if (dto.nameEng != null && dto.nameEng.trim().length() > 0) {
                    englishText += dto.nameEng.trim() + ",";
                }
                if (dto.nameBan != null && dto.nameBan.length() > 0) {
                    banglaText += dto.nameBan.trim() + ",";
                }
            }
        }
        if (englishText.length() > 0) {
            englishText = englishText.substring(0, englishText.length() - 1);
        }

        if (banglaText.length() > 0) {
            banglaText = banglaText.substring(0, banglaText.length() - 1);
        }

        return (language.equalsIgnoreCase("English")) ? englishText
                : banglaText;
    }


    public GeoDistrictDTO getByEngText(String engText) {
        return engText == null ? null : mapByEngText.get(engText.toUpperCase());
    }

    public GeoDistrictDTO getByBngText(String bngText) {
        return bngText == null ? null : mapByBngText.get(bngText);
    }


    public String buildOptions(String language, String selectedId, OrderByEnum orderByEnum, boolean withSelectOption, boolean isMultiSelect) {

        List<GeoDistrictDTO> list;

        list = allDistrictDTO;

        List<OptionDTO> optionDTOList = null;
        if (list != null && list.size() > 0) {
            optionDTOList = list.stream()
                    .map(dto -> new OptionDTO(dto.nameEng, dto.nameBan, String.valueOf(dto.id)))
                    .collect(Collectors.toList());
        }
        if (optionDTOList != null && orderByEnum == OrderByEnum.DSC) {
            Collections.reverse(optionDTOList);
        }
        if (isMultiSelect) {
            return Utils.buildOptionsMultipleSelection(optionDTOList, language, selectedId);
        } else {
            if (withSelectOption) {
                return Utils.buildOptions(optionDTOList, language, selectedId);
            } else {
                return Utils.buildOptionsWithoutSelectWithSelectId(optionDTOList, language, selectedId);
            }
        }
    }


}