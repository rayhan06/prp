package geolocation;

import common.ConnectionAndStatementUtil;
import org.apache.log4j.Logger;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

class GeoDivisionDAO {
    private static final Logger logger = Logger.getLogger(GeoDivisionDAO.class);

    private static final String  getAllSqlQuery= "SELECT * FROM geo_divisions";

    private static final String getSqlById = "SELECT * FROM geo_divisions WHERE ID = ?";

    private GeoDivisionDTO buildGeoDivisionDTOFromResultSet(ResultSet rs){
        try {
            GeoDivisionDTO dto = new GeoDivisionDTO();
            dto.id = rs.getLong("id");
            dto.bbsCode = rs.getString("bbs_code");
            dto.nameEng = rs.getString("division_name_eng");
            dto.nameBan = rs.getString("division_name_bng");
            return dto;
        } catch (SQLException ex) {
            logger.error(ex);
        }
        return null;
    }
    public List<GeoDivisionDTO> getAllDivisions(){
        return ConnectionAndStatementUtil.getListOfT(getAllSqlQuery,this::buildGeoDivisionDTOFromResultSet);
    }

    public GeoDivisionDTO getById(long id){
        return ConnectionAndStatementUtil.getT(getSqlById,ps->{
            try {
                ps.setObject(1,id);
            } catch (SQLException ex) {
                logger.error(ex);
            }
        },this::buildGeoDivisionDTOFromResultSet);
    }
}
