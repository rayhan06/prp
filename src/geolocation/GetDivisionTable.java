package geolocation;

import common.ApiResponse;
import common.NameDTO;
import employee_records.EmployeeRecordsApiPerformer;
import employee_records.ReligionModel;
import religion.ReligionRepository;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class GetDivisionTable implements EmployeeRecordsApiPerformer {
    @Override
    public ApiResponse perform(HttpServletRequest request) {
        try {

            List<GeoDivisionDTO> geoDivisionDTOS;
            List<DivisionModel> divisionTable;

            geoDivisionDTOS = GeoDivisionRepository.getInstance().getDivisionDTOList();
            divisionTable = geoDivisionDTOS.stream().map(DivisionModel::new).collect(Collectors.toList());

            divisionTable.sort(Comparator.comparingInt(model -> (int) model.id));

            return divisionTable.size()==0 ? ApiResponse.makeResponseToResourceNotFound("No more entry is found!") :
                    ApiResponse.makeSuccessResponse(divisionTable, "Successfully found the data.");
        } catch (Exception e) {
            e.printStackTrace();
            return ApiResponse.makeResponseToInternalServerError("Internal Server Error!");
        }

    }
}
