package geolocation;

public class GeoThanaDTO {
    public long id;
    public long geoDivisionId;
    public long geoDistrictId;
    public String nameEng;
    public String nameBan;
    public String bbsCode;
}
