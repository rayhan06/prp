package geolocation;

import common.ApiResponse;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public class GetAllDistrictPerformer implements GeoApiPerformer{
    @Override
    public ApiResponse perform(HttpServletRequest request) {
        List<GeoDistrictDTO> districtDTOList = GeoDistrictRepository.getInstance().getAllDistrict();
        return ApiResponse.makeSuccessResponse(districtDTOList, "List is loaded successfully");
    }
}
