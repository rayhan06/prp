package geolocation;

public class GeoMunicipalityDTO {
    public long id;
    public long geoUpozilaId;
    public String nameEng;
    public String nameBan;
    public String bbsCode;
}
