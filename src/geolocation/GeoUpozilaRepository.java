package geolocation;

import org.apache.log4j.Logger;
import pb.OptionDTO;
import pb.Utils;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@SuppressWarnings({"unused","Duplicates"})
public class GeoUpozilaRepository {
    private static final Logger logger = Logger.getLogger(GeoUpozilaRepository.class);
    private Map<Long, List<GeoUpozilaDTO>> map;
    private Map<Long, GeoUpozilaDTO> mapById;
    private Map<String,GeoUpozilaDTO> mapByEngText;
    private Map<String,GeoUpozilaDTO> mapByBngText;
    private List<GeoUpozilaDTO> allUpozilaDTO;

    private GeoUpozilaRepository() {
        reload();
    }

    private static class LazyLoader {
        static GeoUpozilaRepository INSTANCE = new GeoUpozilaRepository();
    }

    public static GeoUpozilaRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

    private void reload() {
        logger.debug("GeoUpozilaRepository reload start");
        allUpozilaDTO = new GeoUpozilaDAO().getAllUpozilas();
        map = allUpozilaDTO
                .parallelStream()
                .collect(Collectors.groupingBy(dto -> dto.geoDistrictId,
                        Collectors.collectingAndThen(
                                Collectors.toList(),
                                l -> l)));
        mapById = allUpozilaDTO.parallelStream()
                .collect(Collectors.toMap(dto -> dto.id, dto -> dto));
        allUpozilaDTO.sort(Comparator.comparing(o -> o.nameEng));
//        mapByEngText = allUpozilaDTO.parallelStream()
//                .collect(Collectors.toMap(e->e.nameEng.toUpperCase(),e->e));
//        mapByBngText = allUpozilaDTO.parallelStream()
//                .collect(Collectors.toMap(e->e.nameBan,e->e));
        logger.debug("GeoUpozilaRepository reload end");
    }

    public List<GeoUpozilaDTO> getGeoUpozilaDTOListByDistrictId(long districtId) {
        return map.get(districtId);
    }

    public GeoUpozilaDTO getById(long id) {
        return mapById.get(id);
    }

    public List<GeoUpozilaDTO> getAllUpozilaDTO() {
        return allUpozilaDTO;
    }

    public String getUpozilaOptionUpdated(String language, Long selectedId) {
        return buildOptionsByDistrict(language,null,selectedId);
    }

    public String buildOptionsByDistrict(String language,Long districtId,Long selectedId){
        List<GeoUpozilaDTO> list;
        if(districtId == null || map.get(districtId) == null){
            list = allUpozilaDTO;
        }else{
            list = map.get(districtId);
        }
        List<OptionDTO> optionDTOList = null;
        if (list != null && list.size() > 0) {
            optionDTOList = list.stream()
                    .map(dto -> new OptionDTO(dto.nameEng, dto.nameBan, String.valueOf(dto.id)))
                    .collect(Collectors.toList());
        }
        return Utils.buildOptions(optionDTOList, language, selectedId == null ? null : String.valueOf(selectedId));
    }

    public String getText(String language, long id) {
        GeoUpozilaDTO upozilaDTO = getById(id);
        return upozilaDTO == null ? ""
                : (language.equalsIgnoreCase("English")) ? (upozilaDTO.nameEng == null ? "" : upozilaDTO.nameEng)
                : (upozilaDTO.nameBan == null ? "" : upozilaDTO.nameBan);
    }

//    public GeoUpozilaDTO getByEngText(String engText){
//        return engText == null ? null : mapByEngText.get(engText.toUpperCase());
//    }

    public GeoUpozilaDTO getByBngText(String bngText){
        return bngText == null ? null : mapByBngText.get(bngText);
    }
}