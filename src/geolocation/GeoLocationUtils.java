package geolocation;

import pb.Utils;

public class GeoLocationUtils {

    public static GeoLocationModel getGeoLocationModel(String location){
        GeoLocationModel geoLocationModel = new GeoLocationModel();
        if(location == null){
            return geoLocationModel;
        }
        String[] locationAra = location.split("@@");

        if (locationAra.length < 5)
            return geoLocationModel;

        int divisionId = Integer.parseInt(locationAra[0]);
        int districtId = Integer.parseInt(locationAra[1]);
        int upozilaId = Integer.parseInt(locationAra[2]);
        int municipalityId = Integer.parseInt(locationAra[3]);
        String address = locationAra[4];

        if (divisionId == 0 || districtId == 0 || upozilaId == 0)
            return geoLocationModel;

        /*geoLocationModel.divisionEng = GeoDivisionRepository.getInstance().getText("English", divisionId);
        geoLocationModel.divisionBng = GeoDivisionRepository.getInstance().getText("Bangla", divisionId);
        geoLocationModel.districtEng = GeoDistrictRepository.getInstance().getText("English", districtId);
        geoLocationModel.districtBng = GeoDistrictRepository.getInstance().getText("Bangla", districtId);
        geoLocationModel.upozilaEng = GeoUpozilaRepository.getInstance().getText("English", upozilaId);
        geoLocationModel.upozilaBng = GeoUpozilaRepository.getInstance().getText("Bangla", upozilaId);
        geoLocationModel.divisionEng = GeoDivisionRepository.getInstance().getText("English", divisionId);*/
        geoLocationModel.divisionEng = GeoLocationRepository.getInstance().getText("English", divisionId);
        geoLocationModel.divisionBng = GeoLocationRepository.getInstance().getText("Bangla", divisionId);
        geoLocationModel.districtEng = GeoLocationRepository.getInstance().getText("English", districtId);
        geoLocationModel.districtBng = GeoLocationRepository.getInstance().getText("Bangla", districtId);
        geoLocationModel.upozilaEng = GeoLocationRepository.getInstance().getText("English", upozilaId);
        geoLocationModel.upozilaBng = GeoLocationRepository.getInstance().getText("Bangla", upozilaId);
        if(municipalityId != 0) {
            geoLocationModel.municipalityEng = GeoLocationRepository.getInstance().getText("English", municipalityId);
            geoLocationModel.municipalityBng = GeoLocationRepository.getInstance().getText("Bangla", municipalityId);
        }
//        else {
//            geoLocationModel.municipalityEng = "null";
//            geoLocationModel.municipalityBng = "null";
//        }
        geoLocationModel.address = address;

        geoLocationModel.geoLocationStringEng = geoLocationModel.geoLocationStringBng = geoLocationModel.address + ", ";

        if(!geoLocationModel.municipalityEng.equals("")){
            geoLocationModel.geoLocationStringEng += geoLocationModel.municipalityEng + ", ";
            geoLocationModel.geoLocationStringBng += geoLocationModel.municipalityBng + ", ";
        }

        geoLocationModel.geoLocationStringEng += geoLocationModel.upozilaEng + ", " + geoLocationModel.districtEng
                                                    + ", " + geoLocationModel.divisionEng;
        geoLocationModel.geoLocationStringBng += geoLocationModel.upozilaBng + ", " + geoLocationModel.districtBng
                                                    + ", " + geoLocationModel.divisionBng;

        return geoLocationModel;
    }

    public static String getGeoLocationString(String location, String language) {
        GeoLocationModel model = getGeoLocationModel(location);
        return language.equalsIgnoreCase("English") ? model.geoLocationStringEng : model.geoLocationStringBng;
    }
}
