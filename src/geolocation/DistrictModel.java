package geolocation;

public class DistrictModel {
    public long id;
    public String nameEng;
    public String nameBng;
    public String bbsCode;

    public DistrictModel(GeoDistrictDTO dto) {
        id = dto.id;
        nameEng = dto.nameEng;
        nameBng = dto.nameBan;
        bbsCode = dto.bbsCode;
    }
}
