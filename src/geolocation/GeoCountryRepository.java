package geolocation;

import common.NameRepository;

public class GeoCountryRepository extends NameRepository {

    private GeoCountryRepository() {
        super(GeoCountryDao.getInstance(), GeoCountryRepository.class);
    }

    private static class LazyLoader {
        static GeoCountryRepository INSTANCE = new GeoCountryRepository();
    }

    public static GeoCountryRepository getInstance() {
        return GeoCountryRepository.LazyLoader.INSTANCE;
    }
}
