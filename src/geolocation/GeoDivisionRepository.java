package geolocation;

import org.apache.log4j.Logger;
import pb.OptionDTO;
import pb.Utils;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@SuppressWarnings({"unused","Duplicates"})
public class GeoDivisionRepository {
    public static final Logger logger = Logger.getLogger(GeoDivisionRepository.class);
    private Map<Long,GeoDivisionDTO> map;
    private List<GeoDivisionDTO> divisionDTOList;
    private Map<String,GeoDivisionDTO> mapByEngText;
    private Map<String,GeoDivisionDTO> mapByBngText;


    private static class LazyLoader{
        static GeoDivisionRepository INSTANCE = new GeoDivisionRepository();
    }

    public static GeoDivisionRepository getInstance(){
        return LazyLoader.INSTANCE;
    }

    private GeoDivisionRepository() {
        reload();
    }

    private void reload(){
        logger.debug("GeoDivisionRepository reload start");
        divisionDTOList = new GeoDivisionDAO().getAllDivisions();
        map = divisionDTOList.stream()
                .collect(Collectors.toMap(dto->dto.id,dto->dto));
        divisionDTOList.sort(Comparator.comparing(o -> o.nameEng));
        mapByEngText = divisionDTOList.stream()
                .collect(Collectors.toMap(e->e.nameEng.toUpperCase(),e->e));
        mapByBngText = divisionDTOList.stream()
                .collect(Collectors.toMap(e->e.nameBan,e->e));
        logger.debug("GeoDivisionRepository reload end");
    }

    public GeoDivisionDTO getById(long id){
        return map.get(id);
    }

    public Map<Long,GeoDivisionDTO> getDivisionMap(){
        return map;
    }

    public String getOptions(String language, Long selectedId){
        List<OptionDTO> optionDTOList = null;
        if(divisionDTOList!=null && divisionDTOList.size()>0){
            optionDTOList = divisionDTOList.stream()
                    .map(dto->new OptionDTO(dto.nameEng,dto.nameBan,String.valueOf(dto.id)))
                    .collect(Collectors.toList());
        }
        return Utils.buildOptions(optionDTOList,language,selectedId == null ? null : String.valueOf(selectedId));
    }

    public List<GeoDivisionDTO> getDivisionDTOList(){
        return divisionDTOList;
    }

    public String getText(String language, long id) {
        return getText(language.equalsIgnoreCase("English"),id);
    }

    public String getText(boolean isLangEng, long id) {
        GeoDivisionDTO dto = getById(id);
        return dto == null ? ""
                : isLangEng ? (dto.nameEng == null ? "" : dto.nameEng)
                : (dto.nameBan == null ? "" : dto.nameBan);
    }

    public GeoDivisionDTO getByEngText(String engText){
        return engText == null ? null : mapByEngText.get(engText.toUpperCase());
    }

    public GeoDivisionDTO getByBngText(String bngText){
        return bngText == null ? null : mapByBngText.get(bngText);
    }
}
