package geolocation;

import common.ConnectionAndStatementUtil;
import dbm.DBMR;
import jdk.nashorn.internal.ir.WhileNode;
import pb.CategoryLanguageModel;

import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class GeoMunicipalityDAO{
    private static final Logger logger = Logger.getLogger(GeoDistrictDAO.class);

    private static final String  getAllSqlQuery= "SELECT * FROM geo_municipalities";

    private static final String getSqlById = "SELECT * FROM geo_municipalities WHERE ID = ?";

    private static final String getSQLByDistrictId = "SELECT * FROM geo_municipalities WHERE geo_district_id = ?";

    private static final String getSQLByMunicipalitysName = "SELECT * FROM geo_municipalities WHERE ";

    private GeoMunicipalityDTO buildGeoMunicipalityDTOFromResultSet(ResultSet rs){
        try {
            GeoMunicipalityDTO dto = new GeoMunicipalityDTO();
            dto.id = rs.getLong("id");
            dto.geoUpozilaId = rs.getLong("geo_upazila_id");
            dto.bbsCode = rs.getString("bbs_code");
            dto.nameEng = rs.getString("municipality_name_eng");
            dto.nameBan = rs.getString("municipality_name_bng");
            return dto;
        } catch (SQLException ex) {
            logger.error(ex);
        }
        return null;
    }
    public List<GeoMunicipalityDTO> getAllMunicipalitys(){
        return ConnectionAndStatementUtil.getListOfT(getAllSqlQuery,this::buildGeoMunicipalityDTOFromResultSet);
    }

    public GeoMunicipalityDTO getById(long id){
        return ConnectionAndStatementUtil.getT(getSqlById,ps->{
            try {
                ps.setObject(1,id);
            } catch (SQLException ex) {
                logger.error(ex);
            }
        },this::buildGeoMunicipalityDTOFromResultSet);
    }
}
