package geolocation;

import common.ConnectionAndStatementUtil;
import org.apache.log4j.Logger;
import pb.OptionDTO;
import pb.Utils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class GeoLocationRepository {
    private static final Logger logger = Logger.getLogger(GeoLocationRepository.class);
    private static final String getAllSQL = "SELECT * FROM geo_location";
    private Map<Integer, GeoLocationDTO> mapById;
    List<GeoLocationDTO> geoLocationDTOList;
    Map<Integer, List<GeoLocationDTO>> mapByParentId;

    private GeoLocationRepository() {
        reload();
    }

    private static class LazyLoader {
        static final GeoLocationRepository INSTANCE = new GeoLocationRepository();
    }

    public static GeoLocationRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

    private GeoLocationDTO buildGeoLocationDTO(ResultSet rs) {
        try {
            GeoLocationDTO dto = new GeoLocationDTO();
            dto.id = rs.getInt("id");
            dto.parentID = rs.getInt("parent_geo_id");
            dto.geoLevelID = rs.getInt("geo_level_id");
            dto.name_bn = rs.getString("name_bn");
            dto.name_en = rs.getString("name_en");
            return dto;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    private void reload() {
        logger.debug("GeoLocationRepository loading is started");
        geoLocationDTOList = ConnectionAndStatementUtil.getListOfT(getAllSQL, this::buildGeoLocationDTO);
        if (geoLocationDTOList.size() > 0) {
            mapById = geoLocationDTOList.parallelStream()
                    .collect(Collectors.toMap(dto -> dto.id, dto -> dto));
            mapByParentId = geoLocationDTOList.parallelStream().collect(Collectors.groupingBy(dto -> dto.parentID));
        }
        logger.debug("GeoLocationRepository loading is completed");
    }

    public GeoLocationDTO getById(int id) {
        return mapById == null ? null : mapById.get(id);
    }

    public List<GeoLocationDTO> getByParentGeoIdAndGeoLevelId(int parentID, int geoLevelID) {
        if (geoLocationDTOList != null && geoLocationDTOList.size() > 0) {
            return geoLocationDTOList.parallelStream()
                    .filter(dto -> dto.geoLevelID == geoLevelID && dto.parentID == parentID)
                    .collect(Collectors.toList());
        }
        return new ArrayList<>();
    }

    public List<GeoLocationDTO> getByParentId(int parentId) {
        List<GeoLocationDTO> dtoList = mapByParentId.get(parentId);
        return dtoList == null ? new ArrayList<>() : new ArrayList<>(dtoList);
    }

    public List<GeoLocationDTO> getByParentIdAndLevelId(int parentId, int geoLevelID) {
        return getByParentId(parentId)
                .stream()
                .filter(dto -> dto.geoLevelID == geoLevelID)
                .collect(Collectors.toList());
    }

    public String getText(String language, int id) {
        return getText(language.equalsIgnoreCase("English"), id);
    }

    public String getText(boolean isLangEn, int id) {
        GeoLocationDTO dto = getById(id);
        return dto == null ? ""
                : isLangEn ? (dto.name_en == null ? "" : dto.name_en)
                : (dto.name_bn == null ? "" : dto.name_bn);
    }

    public boolean isDivision(int id){
        GeoLocationDTO dto = getById(id);
        return dto != null && dto.geoLevelID == 1;
    }

    public String buildOption(String language, int geoParentId, Integer selectedId) {
        List<GeoLocationDTO> list = mapByParentId.get(geoParentId);
        List<OptionDTO> optionDTOList = new ArrayList<>();
        if (list != null && list.size() > 0) {
            optionDTOList = list.stream()
                    .map(dto -> new OptionDTO(dto.name_en, dto.name_bn, String.valueOf(dto.id)))
                    .collect(Collectors.toList());
        }

        return Utils.buildOptionsWithoutSelectWithSelectId(optionDTOList, language, selectedId == null ? null : String.valueOf(selectedId));
    }

    public String buildOptionByLevel(String language, int levelId, Integer selectedId) {
        List<OptionDTO> optionDTOList = mapById.values().stream()
                .filter(dto -> dto.geoLevelID == levelId)
                .map(dto -> new OptionDTO(dto.name_en, dto.name_bn, String.valueOf(dto.id)))
                .collect(Collectors.toList());

        return Utils.buildOptions(optionDTOList, language, selectedId == null ? null : String.valueOf(selectedId));
    }
}
