package geolocation;

import common.ConnectionAndStatementUtil;
import dbm.DBMR;
import jdk.nashorn.internal.ir.WhileNode;
import pb.CategoryLanguageModel;

import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class GeoDistrictDAO{
    private static final Logger logger = Logger.getLogger(GeoDistrictDAO.class);

    private static final String  getAllSqlQuery= "SELECT * FROM geo_districts";

    private static final String getSqlById = "SELECT * FROM geo_districts WHERE ID = ?";

    private static final String getSQLByDivisionId = "SELECT * FROM geo_districts WHERE geo_district_id = ?";

    private static final String getSQLByDistrictName = "SELECT * FROM geo_districts WHERE ";

    private GeoDistrictDTO buildGeoDistrictDTOFromResultSet(ResultSet rs){
        try {
            GeoDistrictDTO dto = new GeoDistrictDTO();
            dto.id = rs.getLong("id");
            dto.geoDivisionId = rs.getLong("geo_division_id");
            dto.bbsCode = rs.getString("bbs_code");
            dto.nameEng = rs.getString("district_name_eng");
            dto.nameBan = rs.getString("district_name_bng");
            return dto;
        } catch (SQLException ex) {
            logger.error(ex);
        }
        return null;
    }
    public List<GeoDistrictDTO> getAllDistricts(){
        return ConnectionAndStatementUtil.getListOfT(getAllSqlQuery,this::buildGeoDistrictDTOFromResultSet);
    }

    public GeoDistrictDTO getById(long id){
        return ConnectionAndStatementUtil.getT(getSqlById,ps->{
            try {
                ps.setObject(1,id);
            } catch (SQLException ex) {
                logger.error(ex);
            }
        },this::buildGeoDistrictDTOFromResultSet);
    }

    public List<GeoDistrictDTO> getByDivisionId(long id){
        return ConnectionAndStatementUtil.getListOfT(getSQLByDivisionId,ps->{
            try {
                ps.setObject(1,id);
            } catch (SQLException ex) {
                logger.error(ex);
            }
        },this::buildGeoDistrictDTOFromResultSet);
    }
    public static String getDistrictOption(String Language, String savedDistrict) {
        String languagetextColumn, options;
        Connection connection = null;
        ResultSet rs = null;
        Statement stmt = null;


        if (Language.equalsIgnoreCase("English")) {
            languagetextColumn = "district_name_eng";
            options = "<option value=\"select\">Select</option>";
        } else {
            languagetextColumn = "district_name_bng";
            options = "<option value=\"select\">বাছাই করুন</option>";
        }
        
        
        if (!savedDistrict.equals("")) {
            options = "<option value=\""+savedDistrict+"\">"+savedDistrict+"</option>";
        }


        try{

            final String getOptionSQL = "SELECT " + languagetextColumn + " FROM geo_districts";
            String district;

            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();
            logger.debug(getOptionSQL);

            rs = stmt.executeQuery(getOptionSQL);
            while (rs.next()) {
                district = rs.getString(languagetextColumn);
                options += "<option value=\""+district+"\">"+district+"</option>";
            }

        }catch(Exception ex){
            ex.printStackTrace();
        }finally{
            try{
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e){}

            try{
                if (connection != null){
                    DBMR.getInstance().freeConnection(connection);
                }
            }catch(Exception ex2){}
        }

        return options;
    }

    public List<GeoDistrictDTO> getByDistrictName(String Language, String name){

        String matchColumn = "";
        matchColumn = Language.equals("Bangla")?"district_name_bng":"district_name_eng";

        return ConnectionAndStatementUtil.getListOfT(getSQLByDistrictName + matchColumn + " = '" + name + "'", ps->{
        },this::buildGeoDistrictDTOFromResultSet);
    }
}
