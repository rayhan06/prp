package geolocation;

import common.NameDao;

public class GeoCountryDao extends NameDao {

    private GeoCountryDao() {
        super("geo_countries");
    }

    private static class LazyLoader{
        static final GeoCountryDao INSTANCE = new GeoCountryDao();
    }

    public static GeoCountryDao getInstance(){
        return LazyLoader.INSTANCE;
    }
}
