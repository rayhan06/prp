package geolocation;

import common.ApiResponse;
import common.NameDTO;
import employee_records.EmployeeRecordsApiPerformer;
import employee_records.ReligionModel;
import religion.ReligionRepository;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class GetDistrictTable implements EmployeeRecordsApiPerformer {
    @Override
    public ApiResponse perform(HttpServletRequest request) {
        try {

            List<GeoDistrictDTO> geoDistrictDTOS;
            List<DistrictModel> districtTable;

            geoDistrictDTOS = GeoDistrictRepository.getInstance().getAllDistrict();
            districtTable = geoDistrictDTOS.stream().map(DistrictModel::new).collect(Collectors.toList());

            districtTable.sort(Comparator.comparingInt(model -> (int) model.id));

            return districtTable.size()==0 ? ApiResponse.makeResponseToResourceNotFound("No more entry is found!") :
                    ApiResponse.makeSuccessResponse(districtTable, "Successfully found the data.");
        } catch (Exception e) {
            e.printStackTrace();
            return ApiResponse.makeResponseToInternalServerError("Internal Server Error!");
        }

    }
}
