package geolocation;

public class GeoPerformerFactory {
    private static final String GET_DISTRICT_REQUEST = "getDistricts";
    private static final String GET_THANA_REQUEST = "getThanas";
    private static final String GET_ALL_DISTRICT_REQUEST = "getAllDistricts";

    public static GeoApiPerformer getGeoApiPerformer(String actionType){
        if(actionType!=null){
            switch (actionType){
                case GET_DISTRICT_REQUEST:
                    return new GetDistrictPerformer();
                case GET_THANA_REQUEST:
                    return new GetThanaPerformer();
                case GET_ALL_DISTRICT_REQUEST:
                    return new GetAllDistrictPerformer();
            }
        }
        return null;
    }
}
