package geolocation;

import org.apache.log4j.Logger;
import pb.OptionDTO;
import pb.Utils;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@SuppressWarnings({"unused","Duplicates"})
public class GeoMunicipalityRepository {
    private static final Logger logger = Logger.getLogger(GeoMunicipalityRepository.class);
    private Map<Long, List<GeoMunicipalityDTO>> map;
    private Map<Long, GeoMunicipalityDTO> mapById;
    private Map<String,GeoMunicipalityDTO> mapByEngText;
    private Map<String,GeoMunicipalityDTO> mapByBngText;
    private List<GeoMunicipalityDTO> allMunicipalityDTO;

    private GeoMunicipalityRepository() {
        reload();
    }

    private static class LazyLoader {
        static GeoMunicipalityRepository INSTANCE = new GeoMunicipalityRepository();
    }

    public static GeoMunicipalityRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

    private void reload() {
        logger.debug("GeoMunicipalityRepository reload start");
        allMunicipalityDTO = new GeoMunicipalityDAO().getAllMunicipalitys();
        map = allMunicipalityDTO
                .parallelStream()
                .collect(Collectors.groupingBy(dto -> dto.geoUpozilaId,
                        Collectors.collectingAndThen(
                                Collectors.toList(),
                                l -> l)));
        mapById = allMunicipalityDTO.parallelStream()
                .collect(Collectors.toMap(dto -> dto.id, dto -> dto));
        logger.debug("GeoMunicipalityRepository reload end");
    }

    public List<GeoMunicipalityDTO> getGeoMunicipalityDTOListByUpozilaId(long upozilaId) {
        return map.get(upozilaId);
    }

    public GeoMunicipalityDTO getById(long id) {
        return mapById.get(id);
    }

    public List<GeoMunicipalityDTO> getAllMunicipalityDTO() {
        return allMunicipalityDTO;
    }

    public String getMunicipalityOptionUpdated(String language, Long selectedId) {
        return buildOptionsByUpozila(language,null,selectedId);
    }

    public String buildOptionsByUpozila(String language,Long upozilaId,Long selectedId){
        List<GeoMunicipalityDTO> list;
        if(upozilaId == null || map.get(upozilaId) == null){
            list = allMunicipalityDTO;
        }else{
            list = map.get(upozilaId);
        }
        List<OptionDTO> optionDTOList = null;
        if (list != null && list.size() > 0) {
            optionDTOList = list.stream()
                    .map(dto -> new OptionDTO(dto.nameEng, dto.nameBan, String.valueOf(dto.id)))
                    .collect(Collectors.toList());
        }
        return Utils.buildOptions(optionDTOList, language, selectedId == null ? null : String.valueOf(selectedId));
    }

    public String getText(String language, long id) {
        GeoMunicipalityDTO MunicipalityDTO = getById(id);
        return MunicipalityDTO == null ? ""
                : (language.equalsIgnoreCase("English")) ? (MunicipalityDTO.nameEng == null ? "" : MunicipalityDTO.nameEng)
                : (MunicipalityDTO.nameBan == null ? "" : MunicipalityDTO.nameBan);
    }
}