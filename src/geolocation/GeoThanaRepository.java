package geolocation;

import org.apache.log4j.Logger;
import pb.OptionDTO;
import pb.Utils;

import java.util.*;
import java.util.stream.Collectors;

public class GeoThanaRepository {
    private static final Logger logger = Logger.getLogger(GeoThanaRepository.class);
    private Map<Long, List<GeoThanaDTO>> map;
    private Map<Long, GeoThanaDTO> mapById;
    private List<GeoThanaDTO> allThanaDTO;

    private GeoThanaRepository(){
        reload();
    }
    private static class LazyLoader{
        static GeoThanaRepository INSTANCE = new GeoThanaRepository();
    }

    public static GeoThanaRepository getInstance(){
        return LazyLoader.INSTANCE;
    }

    private void reload(){
        logger.debug("GeoThanaRepository reload start");
        List<GeoThanaDTO> thanaDTOList = new GeoThanaDAO().getAllThanas();
        map = thanaDTOList
                .parallelStream()
                .collect(Collectors.groupingBy(dto->dto.geoDistrictId,
                        Collectors.collectingAndThen(
                                Collectors.toCollection(ArrayList::new),
                                l->{ l.sort(Comparator.comparing(o->o.nameEng)); return l; } )));
        mapById = thanaDTOList.parallelStream()
                .collect(Collectors.toMap(dto->dto.id,dto->dto));
        logger.debug("GeoThanaRepository reload end");
    }

    public List<GeoThanaDTO> geoThanaDTOListByDistrictId(long districtId){
        return map.get(districtId);
    }

    public GeoThanaDTO getById(long id){
        return mapById.get(id);
    }

    public String getThanaByIds(String ids,String language) {
    	if(ids==null || language==null) return "";
    	return Arrays.stream(ids.split(","))
                .map(this::parseToLong)
                .filter(Objects::nonNull)
                .map(this::getById)
                .filter(Objects::nonNull)
                .map(dto->language.equalsIgnoreCase("English") ? dto.nameEng : dto.nameBan)
                .collect(Collectors.joining(","));
    }

    private Long parseToLong(String str) {
        try {
            return Long.parseLong(str.trim());
        }catch (NumberFormatException ex){
            logger.error(ex);
            return null;
        }
    }

    public String buildOptionsByDistrict(String language,Long districtId,Long selectedId){
        List<GeoThanaDTO> list;
        if(districtId == null || map.get(districtId) == null){
            list = allThanaDTO;
        }else{
            list = map.get(districtId);
        }
        List<OptionDTO> optionDTOList = null;
        if (list != null && list.size() > 0) {
            optionDTOList = list.stream()
                    .map(dto -> new OptionDTO(dto.nameEng, dto.nameBan, String.valueOf(dto.id)))
                    .collect(Collectors.toList());
        }
        return Utils.buildOptions(optionDTOList, language, selectedId == null ? null : String.valueOf(selectedId));
    }

    public String buildOptionsByDistrict(String language,Long districtId,String selectedIds){
        List<GeoThanaDTO> list;
        if(districtId == null || map.get(districtId) == null){
            list = allThanaDTO;
        }else{
            list = map.get(districtId);
        }
        List<OptionDTO> optionDTOList = null;
        if (list != null && list.size() > 0) {
            optionDTOList = list.stream()
                    .map(dto -> new OptionDTO(dto.nameEng, dto.nameBan, String.valueOf(dto.id)))
                    .collect(Collectors.toList());
        }
        return Utils.buildOptionsMultipleSelection(optionDTOList, language, selectedIds);
    }
}
