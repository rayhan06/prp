package geolocation;

import common.ApiResponse;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public class GetDistrictPerformer implements GeoApiPerformer{
    @Override
    public ApiResponse perform(HttpServletRequest request) {
        long divisionId = Long.parseLong(request.getParameter("divisionId"));
        List<GeoDistrictDTO> districtDTOList = GeoDistrictRepository.getInstance().getGeoDistrictDTOListByDivisionId(divisionId);
        return ApiResponse.makeSuccessResponse(districtDTOList, "List is loaded successfully");
    }
}
