package geolocation;

/*
 * @author Md. Erfan Hossain
 * @created 27/07/2021 - 3:40 AM
 * @project parliament
 */

import common.ConnectionAndStatementUtil;
import employee_records.Employee_recordsDTO;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class GeoLocationMigrate {
    private static final String sql = "select * from employee_records where present_address like '1%' or present_address like '2%' or present_address like '3%' or present_address like '4%'" +
            " or present_address like '5%' or present_address like '6%' or present_address like '7%' or present_address like '8%' or present_address like '9%'" +
            " or permanent_address like '1%' or permanent_address like '2%' or permanent_address like '3%' or permanent_address like '4%'" +
            " or permanent_address like '5%' or permanent_address like '6%' or permanent_address like '7%' or permanent_address like '8%' or permanent_address like '9%' order by id";
    private static final String updateSql = "UPDATE employee_records SET present_address = ?, present_address_bng=?,permanent_address=?," +
            "permanent_address_bng=? WHERE id = ?";
    static GeoLocationDTO locationDTO = new GeoLocationDTO();
    public static void main(String[] args) {

        locationDTO.id = 0;
        List<Employee_recordsDTO> list = ConnectionAndStatementUtil.getListOfT(sql,GeoLocationMigrate::buildEmployeeRecordsDTO);
        for(Employee_recordsDTO e : list){
            String presentAddress = buildAddress(e.presentAddress,true);
            String presentAddressBng = buildAddress(e.presentAddressBng,false);
            String permanentAddress = buildAddress(e.permanentAddress,true);
            String permanentAddressBng = buildAddress(e.permanentAddressBng,false);
            ConnectionAndStatementUtil.getWritePrepareStatement(ps->{
                try {
                    ps.setString(1,presentAddress);
                    ps.setString(2,presentAddressBng);
                    ps.setString(3,permanentAddress);
                    ps.setString(4,permanentAddressBng);
                    ps.setLong(5,e.iD);
                    System.out.println(ps);
                    ps.executeUpdate();
                } catch (SQLException ex) {
                    System.out.println(ex.getMessage());
                }
            },updateSql);
        }
    }

    private static String buildAddress(String addr,boolean isEnglish){
        String [] present = addr.split(":");
        List<GeoLocationDTO> dtoList = new ArrayList<>();
        GeoLocationDTO a = GeoLocationRepository.getInstance().getById(Integer.parseInt(present[0]));
        dtoList.add(a);
        GeoLocationDTO b = GeoLocationRepository.getInstance().getById(a.parentID);
        dtoList.add(0,b);
        GeoLocationDTO c = GeoLocationRepository.getInstance().getById(b.parentID);
        dtoList.add(0,c);
        if(c.geoLevelID>1){
            dtoList.add(0,GeoLocationRepository.getInstance().getById(c.parentID));
        }else{
            dtoList.add(locationDTO);
        }
        String address = dtoList.stream()
                .map(e1->e1.id)
                .map(String::valueOf)
                .collect(Collectors.joining("@@"));
        String add = addr.split(isEnglish?" Division:":" বিভাগ:")[1];
        return address+"@@"+add;
    }

    private static Employee_recordsDTO buildEmployeeRecordsDTO(ResultSet rs) {
        try {
            Employee_recordsDTO employee_recordsDTO = new Employee_recordsDTO();
            employee_recordsDTO.iD = rs.getLong("ID");
            employee_recordsDTO.nameEng = rs.getString("name_eng");
            employee_recordsDTO.nameBng = rs.getString("name_bng");
            employee_recordsDTO.fatherNameEng = rs.getString("father_name_eng");
            employee_recordsDTO.fatherNameBng = rs.getString("father_name_bng");
            employee_recordsDTO.motherNameEng = rs.getString("mother_name_eng");
            employee_recordsDTO.motherNameBng = rs.getString("mother_name_bng");
            employee_recordsDTO.dateOfBirth = rs.getLong("date_of_birth");
            employee_recordsDTO.presentAddress = rs.getString("present_address");
            employee_recordsDTO.permanentAddress = rs.getString("permanent_address");
            employee_recordsDTO.presentAddressBng = rs.getString("present_address_bng");
            employee_recordsDTO.permanentAddressBng = rs.getString("permanent_address_bng");
            employee_recordsDTO.nid = rs.getString("nid");
            employee_recordsDTO.bcn = rs.getString("bcn");
            employee_recordsDTO.gender = rs.getInt("gender_cat");
            employee_recordsDTO.religion = rs.getLong("religion");
            employee_recordsDTO.bloodGroup = rs.getInt("blood_group");
            employee_recordsDTO.maritalStatus = rs.getInt("marital_status");
            employee_recordsDTO.personalEml = rs.getString("personal_email");
            employee_recordsDTO.personalMobile = rs.getString("personal_mobile");
            employee_recordsDTO.alternativeMobile = rs.getString("alternative_mobile");
            employee_recordsDTO.officePhoneNumber = rs.getString("office_phone_number");
            employee_recordsDTO.officePhoneExtension = rs.getString("office_phone_extension");
            employee_recordsDTO.faxNumber = rs.getString("fax_number");
            employee_recordsDTO.birthDistrict = rs.getLong("birth_district");
            employee_recordsDTO.homeDistrict = rs.getInt("home_district");
            employee_recordsDTO.passportNo = rs.getString("passport_no");
            employee_recordsDTO.passportIssueDate = rs.getLong("passport_issue_date");
            employee_recordsDTO.passportExpiryDate = rs.getLong("passport_expiry_date");
            employee_recordsDTO.marriageDate = rs.getLong("marriage_date");
            employee_recordsDTO.nationalityType = rs.getLong("nationality_type");
            employee_recordsDTO.identificationMark = rs.getString("identification_mark");
            employee_recordsDTO.height = rs.getDouble("height");
            employee_recordsDTO.joiningDate = rs.getLong("joining_date");
            employee_recordsDTO.status = rs.getBoolean("status");
            employee_recordsDTO.createdBy = rs.getInt("created_by");
            employee_recordsDTO.modifiedBy = rs.getInt("modified_by");
            employee_recordsDTO.created = rs.getLong("created");
            employee_recordsDTO.filesDropzone = rs.getLong("filesDropzone");
            employee_recordsDTO.isDeleted = rs.getBoolean("isDeleted");
            employee_recordsDTO.lastModificationTime = rs.getLong("lastModificationTime");
            employee_recordsDTO.isMP = rs.getInt("is_mp");
            employee_recordsDTO.professionOfMP = rs.getString("mp_profession");
            employee_recordsDTO.addressOfMP = rs.getString("mp_parliament_address");
            employee_recordsDTO.employeeNumber = rs.getString("employee_number");
            employee_recordsDTO.freedomFighterInfo = rs.getString("freedom_fighter_info");
            employee_recordsDTO.employmentType = rs.getInt("employment_cat");
            employee_recordsDTO.employeeClass = rs.getInt("employee_class_cat");
            employee_recordsDTO.officerTypeCat = rs.getInt("emp_officer_cat");
            employee_recordsDTO.lprDate=rs.getLong("lpr_date");
            employee_recordsDTO.govtJobJoiningDate=rs.getLong("govt_job_joining_date");
            employee_recordsDTO.retirementDate=rs.getLong("retirement_date");
            employee_recordsDTO.presentDivisionId=rs.getLong("present_division");
            employee_recordsDTO.presentDistrictId=rs.getLong("present_district");
            employee_recordsDTO.permanentDivisionId=rs.getLong("permanent_division");
            employee_recordsDTO.permanentDistrictId=rs.getLong("permanent_district");
            employee_recordsDTO.provisionPeriod=rs.getLong("provision_period");
            employee_recordsDTO.provisionEndDate=rs.getLong("provision_end_date");
            employee_recordsDTO.pensionRemarks=rs.getString("pension_remarks");
            employee_recordsDTO.mpStatus = rs.getInt("mp_status_cat");
            employee_recordsDTO.mpStatusChangeDate = rs.getLong("change_mp_status_date");
            employee_recordsDTO.mpRemarks = rs.getString("mp_remarks");
            employee_recordsDTO.mpElectedCount = rs.getInt("mp_elected_count");

            return employee_recordsDTO;
        } catch (SQLException ex) {
            return null;
        }
    }
}
