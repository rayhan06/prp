package geolocation;

public class GeoUpozilaDTO {
    public long id;
    public long geoDistrictId;
    public String nameEng;
    public String nameBan;
    public String bbsCode;
}
