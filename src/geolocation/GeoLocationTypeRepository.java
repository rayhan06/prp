package geolocation;

import common.ConnectionAndStatementUtil;
import org.apache.log4j.Logger;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class GeoLocationTypeRepository {
    private static final Logger logger = Logger.getLogger(GeoLocationTypeRepository.class);
    private static final String getAllSQL = "SELECT * FROM geo_location_type";
    private Map<Integer,GeoLocationTypeDTO> mapById;
    private static class LazyLoader{
        static final GeoLocationTypeRepository INSTANCE = new GeoLocationTypeRepository();
    }

    public static GeoLocationTypeRepository getInstance(){
        return LazyLoader.INSTANCE;
    }
    private GeoLocationTypeRepository(){
        reload();
    }

    private GeoLocationTypeDTO buildGeoLocationTypeDTO(ResultSet rs){
        try{
            GeoLocationTypeDTO dto = new GeoLocationTypeDTO();
            dto.id = rs.getInt("id");
            dto.nameEn = rs.getString("name");
            dto.nameBn = rs.getString("name_bn");
            return dto;
        }catch (SQLException ex){
            logger.error(ex);
            return null;
        }
    }

    private void reload(){
        logger.debug("GeoLocationTypeRepository loading is started");
        List<GeoLocationTypeDTO> list = ConnectionAndStatementUtil.getListOfT(getAllSQL,this::buildGeoLocationTypeDTO);
        if(list.size()>0){
            mapById = list.stream()
                    .collect(Collectors.toMap(dto->dto.id,dto->dto));
        }
        logger.debug("GeoLocationTypeRepository loading is completed");
    }

    public GeoLocationTypeDTO getById(int id){
        return mapById == null ? null : mapById.get(id);
    }
}
