package geolocation;

import common.ApiResponse;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public class GetThanaPerformer implements GeoApiPerformer{
    @Override
    public ApiResponse perform(HttpServletRequest request) {
        long districtId = Long.parseLong(request.getParameter("districtId"));
        List<GeoThanaDTO> thanaDTOList = GeoThanaRepository.getInstance().geoThanaDTOListByDistrictId(districtId);
        return ApiResponse.makeSuccessResponse(thanaDTOList, "List is loaded successfully");
    }
}
