package geolocation;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import org.apache.log4j.Logger;

import common.ApiResponse;
import pb.OptionDTO;

@WebServlet("/geo_Servlet")
public class GeoLocationServlet extends HttpServlet {
    private static final Logger logger = Logger.getLogger(GeoLocationServlet.class);
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ApiResponse api_response;
        OptionDTO optionDTO;
        Gson gson = new Gson();
        String actionType = request.getParameter("actionType");
        logger.debug("actionType = " + actionType);
        switch (actionType) {
            case "getDistricts":
                long divisionId = Long.parseLong(request.getParameter("divisionId"));
                List<OptionDTO> districtOptionDTOList = new ArrayList<>();
                optionDTO = new OptionDTO("Select District", "জেলা নির্বাচন করুন", "0");
                districtOptionDTOList.add(optionDTO);
                List<GeoDistrictDTO> geoDistrictDTOList = GeoDistrictRepository.getInstance().getGeoDistrictDTOListByDivisionId(divisionId);

                for (GeoDistrictDTO dto : geoDistrictDTOList) {
                    optionDTO = new OptionDTO(dto.nameEng, dto.nameBan, dto.id + "");
                    districtOptionDTOList.add(optionDTO);
                }
                response.getWriter().write(gson.toJson(districtOptionDTOList));
                response.getWriter().flush();
                response.getWriter().close();
                break;
            case "getUpozilas":
                long districtId = Long.parseLong(request.getParameter("districtId"));
                List<OptionDTO> upozilaOptionDTOList = new ArrayList<>();
                optionDTO = new OptionDTO("Select Upozila", "উপজেলা নির্বাচন করুন", "0");
                upozilaOptionDTOList.add(optionDTO);
                List<GeoUpozilaDTO> geoUpozilaDTOList = GeoUpozilaRepository.getInstance().getGeoUpozilaDTOListByDistrictId(districtId);

                for (GeoUpozilaDTO dto : geoUpozilaDTOList) {
                    optionDTO = new OptionDTO(dto.nameEng, dto.nameBan, dto.id + "");
                    upozilaOptionDTOList.add(optionDTO);
                }

                response.getWriter().write(gson.toJson(upozilaOptionDTOList));
                response.getWriter().flush();
                response.getWriter().close();
                break;
            case "getMunicipalities":
                long upozilaId = Long.parseLong(request.getParameter("upozilaId"));
                List<OptionDTO> municipalityOptionDTOList = new ArrayList<>();
                optionDTO = new OptionDTO("Select municipality", "পৌরসভা নির্বাচন করুন", "0");
                municipalityOptionDTOList.add(optionDTO);
                List<GeoMunicipalityDTO> geoMunicipalityDTOList = GeoMunicipalityRepository.getInstance().getGeoMunicipalityDTOListByUpozilaId(upozilaId);

                if (geoMunicipalityDTOList != null) {
                    for (GeoMunicipalityDTO dto : geoMunicipalityDTOList) {
                        optionDTO = new OptionDTO(dto.nameEng, dto.nameBan, dto.id + "");
                        municipalityOptionDTOList.add(optionDTO);
                    }
                }
                response.getWriter().write(gson.toJson(municipalityOptionDTOList));
                response.getWriter().flush();
                response.getWriter().close();
                break;
            case "geo_location":
                int parentId = Integer.parseInt(request.getParameter("parent_id"));
                int type = Integer.parseInt(request.getParameter("type"));
                List<OptionDTO> list = GeoLocationRepository.getInstance()
                        .getByParentId(parentId)
                        .stream()
                        .map(e -> new OptionDTO(e.name_en, e.name_bn, String.valueOf(e.id)))
                        .collect(Collectors.toList());
                switch (type) {
                    case 2:
                        list.add(0, new OptionDTO("Select District", "জেলা নির্বাচন করুন", "0"));
                        break;
                    case 3:
                        list.add(0, new OptionDTO("Select Upozila", "উপজেলা নির্বাচন করুন", "0"));
                        break;
                    case 4:
                        list.add(0, new OptionDTO("Select municipality", "পৌরসভা নির্বাচন করুন", "0"));
                        break;
                }

                response.getWriter().write(gson.toJson(list));
                response.getWriter().flush();
                response.getWriter().close();

                break;
        /*else if(actionType.equals("getGeoLocationObject")) {
            long divisionId = Long.parseLong(request.getParameter("divisionId"));
            long districtId = Long.parseLong(request.getParameter("districtId"));
            long upozilaId = Long.parseLong(request.getParameter("upozilaId"));
            String munStr = request.getParameter("municipalityId");
            long municipalityId = 0;
            if(!munStr.equals("null"))
                municipalityId = Long.parseLong(munStr);
            String address = request.getParameter("address");

            HashMap<String, String> map = new HashMap<>();
            map.put("DivisionEng", GeoDivisionRepository.getInstance().getText("English", divisionId));
            map.put("DivisionBng", GeoDivisionRepository.getInstance().getText("Bangla", divisionId));
            map.put("DistrictEng", GeoDistrictRepository.getInstance().getText("English", districtId));
            map.put("DistrictBng", GeoDistrictRepository.getInstance().getText("Bangla", districtId));
            map.put("UpozilaEng", GeoUpozilaRepository.getInstance().getText("English", upozilaId));
            map.put("UpozilaBng", GeoUpozilaRepository.getInstance().getText("Bangla", upozilaId));
            if(!munStr.equals("null")) {
                map.put("MunicipalityEng", GeoMunicipalityRepository.getInstance().getText("English", municipalityId));
                map.put("MunicipalityBng", GeoMunicipalityRepository.getInstance().getText("Bangla", municipalityId));
            }
            else {
                map.put("MunicipalityEng", "null");
                map.put("MunicipalityBng", "null");
            }
            map.put("Address", address);

            response.getWriter().write(gson.toJson(map));
            response.getWriter().flush();
            response.getWriter().close();
            System.out.println(address);
        }*/
            default:
                GeoApiPerformer performer = GeoPerformerFactory.getGeoApiPerformer(actionType);
                if (performer == null) {
                    api_response = ApiResponse.makeErrorResponse("Action type is invalid");
                } else {
                    api_response = performer.perform(request);
                }
                response.getWriter().write(api_response.getJSONString());
                response.getWriter().flush();
                response.getWriter().close();
                break;
        }
    }
}
