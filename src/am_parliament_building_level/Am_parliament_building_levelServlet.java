package am_parliament_building_level;

import common.BaseServlet;
import common.CommonDAOService;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import permission.MenuConstants;
import user.UserDTO;
import util.CommonDTO;
import util.UtilCharacter;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;


@WebServlet("/Am_parliament_building_levelServlet")
@MultipartConfig
public class Am_parliament_building_levelServlet extends BaseServlet
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Am_parliament_building_levelServlet.class);



	@Override
	public String getTableName() {
		return Am_parliament_building_levelDAO.getInstance().getTableName();
	}

	@Override
	public String getServletName() {
		return "Am_parliament_building_levelServlet";
	}

	@Override
	public CommonDAOService<? extends CommonDTO> getCommonDAOService() {
		return Am_parliament_building_levelDAO.getInstance();
	}

	@Override
	public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
		Am_parliament_building_levelDTO am_parliament_building_levelDTO;

		if(Boolean.TRUE.equals(addFlag))
		{
			am_parliament_building_levelDTO = new Am_parliament_building_levelDTO();
			am_parliament_building_levelDTO.insertionDate = am_parliament_building_levelDTO.lastModificationTime
					= System.currentTimeMillis();
			am_parliament_building_levelDTO.insertedByUserId = userDTO.ID;
			am_parliament_building_levelDTO.insertedByOrganogramId = userDTO.organogramID;
		}
		else
		{
			am_parliament_building_levelDTO = Am_parliament_building_levelRepository.getInstance().
					getAm_parliament_building_levelDTOByID(Long.parseLong(request.getParameter("iD")));
			if (am_parliament_building_levelDTO == null) {
				UtilCharacter.throwException("লেভেল খুঁজে পাওয়া যায় নি", "Level not found");
			}

			if(!Am_parliament_building_levelDAO.getInstance().isNotUsed((am_parliament_building_levelDTO.iD))){
				UtilCharacter.throwException("ইতোমধ্যে ব্যবহৃত, পরিবর্তন সম্ভব নয় ", "Can't Update, Already Used.");
			}

			am_parliament_building_levelDTO.lastModificationTime = System.currentTimeMillis();
			am_parliament_building_levelDTO.modifiedBy = userDTO.ID;
		}

		am_parliament_building_levelDTO.levelNo = Jsoup.clean(request.getParameter("levelNo"),
				Whitelist.simpleText());
		if (am_parliament_building_levelDTO.levelNo.isEmpty()) {
			UtilCharacter.throwException("লেভেল নম্বর প্রয়োজনীয়", "Level Number required");
		}

		if(!uniquenessCheck(am_parliament_building_levelDTO.levelNo, addFlag, am_parliament_building_levelDTO.iD)){
			UtilCharacter.throwException("ইউনিক লেভেল নম্বর দিন ", "Level Number not unique");
		}

		if (Boolean.TRUE.equals(addFlag)) {
			getCommonDAOService().add(am_parliament_building_levelDTO);
		} else {
			getCommonDAOService().update(am_parliament_building_levelDTO);
		}
		return am_parliament_building_levelDTO;
	}

	@Override
	public int[] getAddPageMenuConstants() {
		return new int[] {MenuConstants.AM_PARLIAMENT_BUILDING_LEVEL_ADD};
	}

	@Override
	public int[] getEditPageMenuConstants() {
		return new int[] {MenuConstants.AM_PARLIAMENT_BUILDING_LEVEL_UPDATE};
	}

	@Override
	public int[] getSearchMenuConstants() {
		return new int[] {MenuConstants.AM_PARLIAMENT_BUILDING_LEVEL_SEARCH};
	}

	@Override
	public Class<? extends HttpServlet> getClazz() {
		return Am_parliament_building_levelServlet.class;
	}

	@Override
	protected void deleteT(HttpServletRequest request, UserDTO userDTO) {
		String[] IDsToDelete = request.getParameterValues("ID");
		if(IDsToDelete.length>0){
			List<Long> ids = Stream.of(IDsToDelete)
					.map(Long::parseLong)
					.filter(i -> Am_parliament_building_levelDAO.getInstance().isNotUsed(i))
					.collect(Collectors.toList());

			if(!ids.isEmpty()){
				getCommonDAOService().deletePb(userDTO.employee_record_id, ids);
			}

		}
	}

	private boolean uniquenessCheck(String levelNo, boolean addFlag, Long ID){
		boolean flag = false;
		int count = Am_parliament_building_levelDAO.getInstance().getCountOfLevelNo(levelNo);
		if (addFlag) {
			if (count == 0) {
				flag = true;
			}
		} else {
			Am_parliament_building_levelDTO levelDTO = Am_parliament_building_levelRepository.getInstance().
					getAm_parliament_building_levelDTOByID(ID);
			if(levelDTO != null){
				Boolean sameValueSaveLogic = levelDTO.levelNo.equals(levelNo) && count == 1;
				Boolean differentValueSaveLogic = !levelDTO.levelNo.equals(levelNo) && count == 0;
				if (sameValueSaveLogic || differentValueSaveLogic) {
					flag = true;
				}
			}
		}

		return flag;
	}


}

