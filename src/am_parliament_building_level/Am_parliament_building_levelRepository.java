package am_parliament_building_level;

import com.google.gson.Gson;
import org.apache.log4j.Logger;
import pb.CatDTO;
import repository.Repository;
import repository.RepositoryManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

@SuppressWarnings("unchecked")
public class Am_parliament_building_levelRepository implements Repository {
	Am_parliament_building_levelDAO am_parliament_building_levelDAO;
	Gson gson = new Gson();
	
	public void setDAO(Am_parliament_building_levelDAO am_parliament_building_levelDAO)
	{
		this.am_parliament_building_levelDAO = am_parliament_building_levelDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Am_parliament_building_levelRepository.class);
	Map<Long, Am_parliament_building_levelDTO>mapOfAm_parliament_building_levelDTOToiD;

  
	private Am_parliament_building_levelRepository(){
		am_parliament_building_levelDAO = Am_parliament_building_levelDAO.getInstance();
		mapOfAm_parliament_building_levelDTOToiD = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	private static class LazyLoader{
         static final Am_parliament_building_levelRepository INSTANCE = new Am_parliament_building_levelRepository();
    }

    public static Am_parliament_building_levelRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

	public void reload(boolean reloadAll){
		try {
			List<Am_parliament_building_levelDTO> am_parliament_building_levelDTOs =
					am_parliament_building_levelDAO.getAllDTOs(reloadAll);
			for(Am_parliament_building_levelDTO am_parliament_building_levelDTO : am_parliament_building_levelDTOs) {
				Am_parliament_building_levelDTO oldAm_parliament_building_levelDTO = getAm_parliament_building_levelDTOByIDWithoutClone(am_parliament_building_levelDTO.iD);
				if( oldAm_parliament_building_levelDTO != null ) {
					mapOfAm_parliament_building_levelDTOToiD.remove(oldAm_parliament_building_levelDTO.iD);
				}
				if(am_parliament_building_levelDTO.isDeleted == 0) 
				{
					mapOfAm_parliament_building_levelDTOToiD.put(am_parliament_building_levelDTO.iD, am_parliament_building_levelDTO);
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Am_parliament_building_levelDTO> getAm_parliament_building_levelList() {
		return clone(new ArrayList<>(this.mapOfAm_parliament_building_levelDTOToiD.values()));
	}
	
	
	public Am_parliament_building_levelDTO getAm_parliament_building_levelDTOByID( long ID){
		return clone(mapOfAm_parliament_building_levelDTOToiD.get(ID));
	}

	public Am_parliament_building_levelDTO getAm_parliament_building_levelDTOByIDWithoutClone( long ID){
		return mapOfAm_parliament_building_levelDTOToiD.get(ID);
	}

	public String getOptions(String language, long defaultValue){
		StringBuilder sOptions =new StringBuilder();
		if (defaultValue == -1)
		{
			if (language.equalsIgnoreCase("English"))
			{
				sOptions.append("<option value = ''>Select</option>");
			}
			else
			{
				sOptions.append("<option value = ''>বাছাই করুন</option>");
			}
		}

		List<Am_parliament_building_levelDTO> dtos = getAm_parliament_building_levelList();

		for(Am_parliament_building_levelDTO dto: dtos){
			StringBuilder sOption =new StringBuilder();
			sOption.append("<option value = '").append( dto.iD).append("'");
			if(defaultValue != CatDTO.CATDEFAULTNOTREQUIRED && dto.iD == defaultValue)
			{
				sOption.append(" selected ");
			}
			sOption.append(">").append(dto.levelNo).append("</option>");
			sOptions.append(sOption);
		}


		return sOptions.toString();

	}

	public String getText(long id) {
		Am_parliament_building_levelDTO dto = getAm_parliament_building_levelDTOByID(id);
		return dto == null ? "": dto.levelNo;
	}

	
	@Override
	public String getTableName() {
		return "am_parliament_building_level";
	}

	public Am_parliament_building_levelDTO clone(Am_parliament_building_levelDTO dto) {
		String raw = gson.toJson(dto);
		return gson.fromJson(raw, Am_parliament_building_levelDTO.class);
	}

	public List<Am_parliament_building_levelDTO> clone(List<Am_parliament_building_levelDTO> dtoList) {
		return dtoList
				.stream()
				.map(this::clone)
				.collect(Collectors.toList());
	}

	@Override
	public String getDtoJsonById(long ID) {
		return gson.toJson(getAm_parliament_building_levelDTOByID(ID));
	}
}


