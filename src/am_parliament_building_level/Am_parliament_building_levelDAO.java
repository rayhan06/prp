package am_parliament_building_level;

import am_house.Am_houseRepository;
import am_parliament_building_block.Am_parliament_building_blockDAO;
import am_parliament_building_block.Am_parliament_building_blockRepository;
import common.CommonDAOService;
import org.apache.log4j.Logger;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class Am_parliament_building_levelDAO  implements CommonDAOService<Am_parliament_building_levelDTO> {


	private static final Logger logger = Logger.getLogger(Am_parliament_building_levelDAO.class);
	private final Map<String,String> searchMap = new HashMap<>();


	private static final String addQuery = "INSERT INTO {tableName} (inserted_by_user_id,inserted_by_organogram_id," +
			"insertion_date, modified_by, search_column,level_no, lastModificationTime," +
			"isDeleted,ID) VALUES (?,?,?,?,?,?,?,?, ?)";

	private static final String updateQuery = "UPDATE {tableName} SET inserted_by_user_id = ?,inserted_by_organogram_id=?," +
			" insertion_date=?, modified_by = ?, search_column=?,level_no=?,lastModificationTime=? WHERE ID = ?";


	private static class LazyLoader{
		static final Am_parliament_building_levelDAO INSTANCE = new Am_parliament_building_levelDAO();
	}

	public static Am_parliament_building_levelDAO getInstance(){
		return Am_parliament_building_levelDAO.LazyLoader.INSTANCE;
	}

	@Override
	public void set(PreparedStatement ps, Am_parliament_building_levelDTO am_parliament_building_levelDTO, boolean isInsert) throws SQLException {
		setSearchColumn(am_parliament_building_levelDTO);
		int index = 0;
		ps.setObject(++index, am_parliament_building_levelDTO.insertedByUserId);
		ps.setObject(++index, am_parliament_building_levelDTO.insertedByOrganogramId);
		ps.setObject(++index, am_parliament_building_levelDTO.insertionDate);
		ps.setObject(++index, am_parliament_building_levelDTO.modifiedBy);
		ps.setObject(++index, am_parliament_building_levelDTO.searchColumn);
		ps.setObject(++index, am_parliament_building_levelDTO.levelNo);
		ps.setObject(++index, am_parliament_building_levelDTO.lastModificationTime);
		if (isInsert) {
			ps.setObject(++index, 0);
		}
		ps.setObject(++index, am_parliament_building_levelDTO.iD);
	}

	public void setSearchColumn(Am_parliament_building_levelDTO am_parliament_building_levelDTO)
	{
		am_parliament_building_levelDTO.searchColumn = am_parliament_building_levelDTO.levelNo;
	}


	private Am_parliament_building_levelDAO() {
		searchMap.put("level_no"," and (level_no = ?)");
		searchMap.put("insertion_date_start"," and (insertion_date>= ?)");
		searchMap.put("insertion_date_end"," and (insertion_date <= ?)");
		searchMap.put("AnyField"," and (search_column like ?)");
	}

	@Override
	public Am_parliament_building_levelDTO buildObjectFromResultSet(ResultSet rs) {
		try
		{
			Am_parliament_building_levelDTO am_parliament_building_levelDTO = new Am_parliament_building_levelDTO();
			am_parliament_building_levelDTO.iD = rs.getLong("ID");
			am_parliament_building_levelDTO.levelNo = rs.getString("level_no");
			am_parliament_building_levelDTO.insertedByUserId = rs.getLong("inserted_by_user_id");
			am_parliament_building_levelDTO.modifiedBy = rs.getLong("modified_by");
			am_parliament_building_levelDTO.insertedByOrganogramId = rs.getLong("inserted_by_organogram_id");
			am_parliament_building_levelDTO.insertionDate = rs.getLong("insertion_date");
			am_parliament_building_levelDTO.searchColumn = rs.getString("search_column");
			am_parliament_building_levelDTO.isDeleted = rs.getInt("isDeleted");
			am_parliament_building_levelDTO.lastModificationTime = rs.getLong("lastModificationTime");
			return am_parliament_building_levelDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}

	@Override
	public String getTableName()
	{
		return "am_parliament_building_level";
	}


	@Override
	public Map<String, String> getSearchMap() {
		return searchMap;
	}

	@Override
	public long add(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Am_parliament_building_levelDTO) commonDTO,addQuery,true);
	}

	@Override
	public long update(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Am_parliament_building_levelDTO) commonDTO,updateQuery,false);
	}




	public boolean isNotUsed(Long iD){
        return Am_parliament_building_blockDAO.getInstance().getCountByLevelId(iD) <= 0;
    }

	public int getCountOfLevelNo (String levelNo) {
		return (int) Am_parliament_building_levelRepository.getInstance().getAm_parliament_building_levelList().
				stream().filter(i -> i.levelNo.equals(levelNo)).count();
	}

	

	
				
}
	