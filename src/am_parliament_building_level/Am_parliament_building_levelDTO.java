package am_parliament_building_level;
import java.util.*; 
import util.*; 


public class Am_parliament_building_levelDTO extends CommonDTO
{

    public String levelNo = "";
	public long insertedByUserId = -1;
	public long insertedByOrganogramId = -1;
	public long insertionDate = -1;
    public long modifiedBy = 0;
	
	
    @Override
	public String toString() {
            return "$Am_parliament_building_levelDTO[" +
            " iD = " + iD +
            " levelNo = " + levelNo +
            " insertedByUserId = " + insertedByUserId +
            " insertedByOrganogramId = " + insertedByOrganogramId +
            " insertionDate = " + insertionDate +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            " searchColumn = " + searchColumn +
            "]";
    }

}