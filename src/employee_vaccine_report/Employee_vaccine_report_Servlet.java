package employee_vaccine_report;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import language.LC;
import language.LM;
import login.LoginDTO;
import office_unit_organograms.OfficeUnitOrganogramsRepository;
import pbReport.ReportService;
import permission.MenuConstants;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.ReportRequestHandler;


@WebServlet("/Employee_vaccine_report_Servlet")
public class Employee_vaccine_report_Servlet extends HttpServlet{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	String[][] Criteria;
	
	String[][] Display;
	
	
	String GroupBy = "";
	String OrderBY = " employee_records_id asc ";
	
	ReportRequestHandler reportRequestHandler;
	
	public Employee_vaccine_report_Servlet(){

	}
	
	private final ReportService reportService = new ReportService();
	
	private String sql;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String language = "english";
		if(userDTO!= null && userDTO.languageID == SessionConstants.BANGLA)
		{
			language = "bangla";
		}
				
		String actionType = request.getParameter("actionType");

		System.out.println("In ssservlet doget, actiontype = " + actionType);
		
		sql = "  employee_vaccination_info join employee_records on employee_records.id = employee_vaccination_info.employee_records_id ";
		
		
		ArrayList<String[]> criteriaList = new ArrayList<String[]>();
		criteriaList.add(new String[] {"criteria","employee_vaccination_info","isDeleted","=","","int","","","0","none",""});
		

		String userName = request.getParameter("userName");
		if(userName != null && !userName.equalsIgnoreCase(""))
		{
			criteriaList.add(new String[] {"criteria","","employee_number","=","AND","String","","","any","userName",LC.PHARMACY_REPORT_WHERE_EMPLOYEEID + ""});
		}
		
		String officeUnitId = request.getParameter("officeUnitId");
		if(officeUnitId != null && !officeUnitId.equalsIgnoreCase(""))
		{
			List<String> userNames = OfficeUnitOrganogramsRepository.getInstance().getAllDescentsUserNamesInclusive(Long.parseLong(officeUnitId));
			String sNames = "";
			int i = 0;
			for(String eUserName: userNames)
			{
				if(i > 0)
				{
					sNames += ", ";
				}
				sNames += eUserName;
				i++;
			}
			if(!sNames.equalsIgnoreCase(""))
			{
				criteriaList.add(new String[] {"criteria","","employee_number","in","AND","String","","",sNames,"userName", ""});
			}
		}
		
		String mp = request.getParameter("mp");
		if(mp != null && !mp.equalsIgnoreCase(""))
		{
			criteriaList.add(new String[] {"criteria","","employee_number","like","AND","exact","","","0%","none",""});
		}
		
		
		
		Criteria = new String[criteriaList.size()][];
		int i = 0;
		for(String [] sCrtiFromList: criteriaList)
		{
			Criteria[i++] = sCrtiFromList;
		}
		
		
		Display= new String[12][4];
		Display[0] = new String[]{"display","",
				"employee_records_id",
				"erIdToOffice",
				LM.getText(LC.HM_OFFICE, loginDTO)};
		Display[1] = new String[]{"display","",
				"employee_number",
				"text",
				language.equalsIgnoreCase("english")?"UserID":"ইউজার আইডি"};
		Display[2] = new String[]{"display","",
				"employee_records_id",
				"erIdToName",
				LM.getText(LC.HM_NAME, loginDTO)};
		Display[3] = new String[]{"display","",
				"employee_records_id",
				"erIdToOrganogram",
				LM.getText(LC.HM_DESIGNATION, loginDTO)};
	
		Display[4] = new String[]{"display","",
				"is_fully_vaccinated",
				"boolean",
				language.equalsIgnoreCase("english")?"Fully Vaccinated?":"পুরোপুরি টীকা নিয়েছেন?"};

		Display[5] = new String[]{"display","",
				"total_doses",
				"int",
				language.equalsIgnoreCase("english")?"Total Doses":"ডোজ সংখ্যা"};
		
		Display[6] = new String[]{"display","",
				"dose1_time",
				"date",
				language.equalsIgnoreCase("english")?"Dose One Time":"প্রথম ডোজের তারিখ"};

		Display[7] = new String[]{"display","",
				"dose2_time",
				"date",
				language.equalsIgnoreCase("english")?"Dose Two Time":"দ্বিতীয় ডোজের তারিখ"};

		Display[8] = new String[]{"display","",
				"booster_time",
				"date",
				language.equalsIgnoreCase("english")?"Booster Time":"বুস্টার ডোজের তারিখ"};
		
		Display[9] = new String[]{"display","",
				"certificate_number",
				"basic",
				language.equalsIgnoreCase("english")?"Certificate Number":"সার্টিফিকেট নাম্বার"};

		Display[10] = new String[]{"display","",
				"vaccine_name",
				"vaccineName",
				language.equalsIgnoreCase("english")?"Vaccine Name":"ভ্যাক্সিনের নাম"};
		
		Display[11] = new String[]{"display","",
				"vaccine_center",
				"text",
				language.equalsIgnoreCase("english")?"Vaccine Center":"ভ্যাক্সিন সেন্টার"};


		
		String reportName = language.equalsIgnoreCase("english")?
				"Employee Vaccination Report"
				:"কর্মচারীর টীকাদান  রিপোর্ট";
		
		reportRequestHandler = new ReportRequestHandler(null,
				Criteria, Display, GroupBy, OrderBY, sql,
				reportService);
		

		
		reportRequestHandler.handleReportGet(request, response, userDTO, "employee_vaccine_report",
				MenuConstants.EV_REPORT, language, reportName, "employee_vaccine_report");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		doGet(request, response);
	}

}
