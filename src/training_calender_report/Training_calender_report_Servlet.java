package training_calender_report;


import language.LC;
import language.LM;
import login.LoginDTO;
import org.apache.log4j.Logger;
import pbReport.PBReportUtils;
import pbReport.ReportCommonService;
import permission.MenuConstants;
import role.PermissionRepository;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.HttpRequestUtils;
import util.StringUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@SuppressWarnings({"unused", "Duplicates"})
@WebServlet("/Training_calender_report_Servlet")
public class Training_calender_report_Servlet extends HttpServlet implements ReportCommonService {
    private static final Logger logger = Logger.getLogger(Training_calender_report_Servlet.class);
    private static final long serialVersionUID = 1L;

    private static final Set<String> searchParam =
            new HashSet<>(Arrays.asList("start_date_from", "start_date_to", "end_date_from", "end_date_to",
                    "trainingTypeCat", "trainingModeCat", "trainerCat", "nameEng", "nameBng", "username",
                    "officeUnitIds", "employeeNameEn", "employeeNameBn", "isPrivate", "arrangedBy", "country",
                    "age_end", "age_start", "empOfficerCat", "employmentCat", "employeeClassCat", "genderCat"));

    private final Map<String, String[]> stringMap = new HashMap<>();
    private static final String sql = " training_calender tc left join training_calendar_details tcd on tc.ID = tcd.training_calendar_id left join employee_records er on tcd.employee_records_id = er.id ";

    private String[][] Criteria;

    private String[][] Display;

    private static int officeUnitNameIndex = -1;

    public Training_calender_report_Servlet() {
        stringMap.put("start_date_from", new String[]{"criteria", "tc", "start_date", ">=", "AND", "long", "", "", Long.MIN_VALUE + ""
                , "start_date_from", "start_date_from", String.valueOf(LC.TRAINING_CALENDER_REPORT_WHERE_STARTDATE_1), "date", null, "true", "1"});
        stringMap.put("start_date_to", new String[]{"criteria", "tc", "start_date", "<=", "AND", "long", "", "", Long.MAX_VALUE + ""
                , "start_date_to", "start_date_to", String.valueOf(LC.TRAINING_CALENDER_REPORT_WHERE_STARTDATE), "date", null, "true", "2"});
        stringMap.put("end_date_from", new String[]{"criteria", "tc", "end_date", ">=", "AND", "long", "", "", Long.MIN_VALUE + ""
                , "end_date_from", "end_date_from", String.valueOf(LC.TRAINING_CALENDER_REPORT_WHERE_ENDDATE_3), "date", null, "true", "3"});
        stringMap.put("end_date_to", new String[]{"criteria", "tc", "end_date", "<=", "AND", "long", "", "", Long.MAX_VALUE + ""
                , "end_date_to", "end_date_to", String.valueOf(LC.TRAINING_CALENDER_REPORT_WHERE_ENDDATE), "date", null, "true", "4"});
        stringMap.put("trainingTypeCat", new String[]{"criteria", "tc", "training_type_cat", "=", "AND", "int", "", "", "any"
                , "trainingTypeCat", "trainingTypeCat", String.valueOf(LC.TRAINING_CALENDER_REPORT_WHERE_TRAININGTYPECAT), "number", null, "true", "5"});
        stringMap.put("trainingModeCat", new String[]{"criteria", "tc", "training_mode_cat", "=", "AND", "int", "", "", "any"
                , "trainingModeCat", "trainingModeCat", String.valueOf(LC.TRAINING_CALENDER_REPORT_WHERE_TRAININGMODECAT), "number", null, "true", "6"});
        stringMap.put("trainerCat", new String[]{"criteria", "tc", "trainer_cat", "=", "AND", "int", "", "", "any"
                , "trainerCat", "trainerCat", String.valueOf(LC.TRAINING_CALENDER_REPORT_WHERE_TRAINERCAT), "number", null, "true", "7"});
        stringMap.put("nameEng", new String[]{"criteria", "tc", "name_en", "Like", "AND", "String", "", "", "any", "nameEng",
                "nameEng", String.valueOf(LC.EMPLOYEE_RECORDS_ADD_NAMEENG), "text", null, "true", "8"});
        stringMap.put("nameBng", new String[]{"criteria", "tc", "name_bn", "Like", "AND", "String", "", "", "any", "nameBng",
                "nameBng", String.valueOf(LC.EMPLOYEE_RECORDS_ADD_NAMEBNG), "text", null, "true", "9"});
        stringMap.put("username", new String[]{"criteria", "er", "employee_number", "Like", "AND", "String", "", "", "any", "username",
                "username", String.valueOf(LC.EMPLOYEE_INFO_REPORT_SELECT_USER_ID), "text", null, "true", "10"});
        stringMap.put("employeeNameEn", new String[]{"criteria", "tcd", "employee_name_en", "Like", "AND", "String", "", "", "any", "employeeNameEn",
                "employeeNameEn", String.valueOf(LC.TRAINING_CALENDER_REPORT_SELECT_NAME_EMP_MP_ENG), "text", null, "true", "11"});
        stringMap.put("employeeNameBn", new String[]{"criteria", "tcd", "employee_name_bn", "Like", "AND", "String", "", "", "any", "employeeNameBn",
                "employeeNameBn", String.valueOf(LC.TRAINING_CALENDER_REPORT_SELECT_NAME_EMP_MP_BNG), "text", null, "true", "12"});
        stringMap.put("officeUnitIds", new String[]{"criteria", "tcd", "office_unit_id", "IN", "AND", "String", "", "", ""
                , "officeUnitIdList", "officeUnitIds", String.valueOf(LC.EMPLOYEE_INFO_REPORT_WHERE_OFFICEUNITID), "office_units", null, "true", "13"});
        stringMap.put("isPrivate", new String[]{"criteria", "tc", "is_private", "=", "AND", "int", "", "", "any"
                , "isPrivate", "isPrivate", String.valueOf(LC.TRAINING_CALENDER_REPORT_SELECT_PRIVATE_TRAINING), "number", null, "true", "14"});
        stringMap.put("country", new String[]{"criteria", "tc", "country", "=", "AND", "long", "", "", "any"
                , "country", "country", String.valueOf(LC.EMPLOYEE_POSTING_ADD_COUNTRY), "number", null, "true", "15"});
        stringMap.put("arrangedBy", new String[]{"criteria", "tc", "training_arrange_by_cat", "=", "AND", "int", "", "", "any"
                , "arrangedBy", "arrangedBy", String.valueOf(LC.TRAINING_CALENDER_ADD_ARRANGEDBY), "number", null, "true", "16"});

        stringMap.put("isDeleted_tc", new String[]{"criteria", "tc", "isDeleted", "=", "AND", "long", "", "", "0", "isDeleted", null, null, null, null, null, null});
        stringMap.put("isDeleted_tcd", new String[]{"criteria", "tcd", "isDeleted", "=", "AND", "long", "", "", "0", "isDeleted", null, null, null, null, null, null});
        stringMap.put("genderCat", new String[]{"criteria", "er", "gender_cat", "=", "AND", "long", "", "", "any"
                , "genderCat", "genderCat", String.valueOf(LC.EMPLOYEE_INFO_REPORT_WHERE_GENDERCAT), "category", "gender", "true", "17"});
        stringMap.put("employeeClassCat", new String[]{"criteria", "er", "employee_class_cat", "=", "AND", "long", "", "", "any",
                "employeeClassCat", "employeeClassCat", String.valueOf(LC.EMPLOYEE_EMPLOYEE_CLASS), "category", "employee_class", "true", "18"});
        stringMap.put("employmentCat", new String[]{"criteria", "er", "employment_cat", "=", "AND", "long", "", "", "any"
                , "employmentCat", "employmentCat", String.valueOf(LC.EMPLOYMENT_CATEGORY_EMPLOYMENTCATEGORY), "category", "employment", "true", "19"});
        stringMap.put("empOfficerCat", new String[]{"criteria", "er", "emp_officer_cat", "=", "AND", "long", "", "", "any",
                "empOfficerCat", "empOfficerCat", String.valueOf(LC.EMPLOYEE_OFFICER_TYPE), "category", "emp_officer", "true", "20"});
        stringMap.put("age_start", new String[]{"criteria", "er", "date_of_birth", "<=", "AND", "long", "", "", ""
                , "ageStart", "age_start", String.valueOf(LC.HR_REPORT_AGE_RANGE_START), "number", null, "true", "21"});
        stringMap.put("age_end", new String[]{"criteria", "er", "date_of_birth", ">=", "AND", "long", "", "", ""
                , "ageEnd", "age_end", String.valueOf(LC.HR_REPORT_AGE_RANGE_TO), "number", null, "true", "22"});
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);

        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }
        Set<String> inputs = PBReportUtils.prepareInputSets(searchParam, request);
        inputs.add("isDeleted_tc");
        inputs.add("isDeleted_tcd");

        Display = new String[][]{
                {"display", "tcd", "employee_name_en", "text", ""},//0
                {"display", "tcd", "employee_name_bn", "text", ""},//1
                {"display", "tcd", "office_name_en", "text", ""},//2
                {"display", "tcd", "organogram_name_en", "text", ""},//3
                {"display", "er", "employee_number", "text", ""},//4
                {"display", "tc", "name_en", "plain", ""},//5
                {"display", "tc", "name_bn", "plain", ""},//6
                {"display", "tc", "start_date", "date", ""},//7
                {"display", "tc", "end_date", "date", ""},//8
                {"display", "tc", "training_arrange_by_cat", "cat", ""},//9
                {"display", "tc", "country", "country", ""},//10
                {"display", "tc", "institute_name", "plain", ""},//11
                {"display", "tc", "training_type_cat", "cat", ""},//12
                {"display", "tc", "training_mode_cat", "cat", ""},//13
                {"display", "tc", "trainer_cat", "cat", ""},//14
                {"display", "tc", "is_private", "boolean", ""},//15
                {"display", "er", "date_of_birth", "complete_age", ""},//16
                {"display", "er", "employee_class_cat", "category", ""},//17
                {"display", "er", "emp_officer_cat", "category", ""},//18
                {"display", "er", "employment_cat", "category", ""},//19
                {"display", "er", "gender_cat", "category", ""},//20
        };
        Display[0][4] = LM.getText(LC.TRAINING_CALENDER_REPORT_SELECT_NAME_EMP_MP_ENG, loginDTO);
        Display[1][4] = LM.getText(LC.TRAINING_CALENDER_REPORT_SELECT_NAME_EMP_MP_BNG, loginDTO);
        Display[2][4] = LM.getText(LC.HM_OFFICE, loginDTO);
        Display[3][4] = LM.getText(LC.HM_DESIGNATION, loginDTO);
        Display[4][4] = LM.getText(LC.EMPLOYEE_INFO_REPORT_SELECT_USER_ID, loginDTO);
        Display[5][4] = LM.getText(LC.TRAINING_CALENDER_REPORT_SELECT_NAMEEN, loginDTO);
        Display[6][4] = LM.getText(LC.TRAINING_CALENDER_REPORT_SELECT_NAMEBN, loginDTO);
        Display[7][4] = LM.getText(LC.TRAINING_CALENDER_REPORT_SELECT_STARTDATE, loginDTO);
        Display[8][4] = LM.getText(LC.TRAINING_CALENDER_REPORT_SELECT_ENDDATE, loginDTO);
        Display[9][4] = LM.getText(LC.TRAINING_CALENDER_REPORT_SELECT_ARRANGEDBY, loginDTO);
        Display[10][4] = LM.getText(LC.EMPLOYEE_POSTING_ADD_COUNTRY, loginDTO);
        Display[11][4] = LM.getText(LC.TRAINING_CALENDER_ADD_INSTITUTENAME, loginDTO);
        Display[12][4] = LM.getText(LC.TRAINING_CALENDER_REPORT_SELECT_TRAININGTYPECAT, loginDTO);
        Display[13][4] = LM.getText(LC.TRAINING_CALENDER_REPORT_SELECT_TRAININGMODECAT, loginDTO);
        Display[14][4] = LM.getText(LC.TRAINING_CALENDER_REPORT_SELECT_TRAINERCAT, loginDTO);
        Display[15][4] = LM.getText(LC.TRAINING_CALENDER_REPORT_SELECT_PRIVATE_TRAINING, loginDTO);
        Display[16][4] = LM.getText(LC.PHYSIOTHERAPY_PLAN_ADD_AGE, loginDTO);
        Display[17][4] = LM.getText(LC.EMPLOYEE_INFO_REPORT_SELECT_EMPLOYEECLASSCAT, loginDTO);
        Display[18][4] = LM.getText(LC.EMPLOYEE_INFO_REPORT_SELECT_EMPOFFICERCAT, loginDTO);
        Display[19][4] = LM.getText(LC.EMPLOYEE_INFO_REPORT_SELECT_EMPLOYMENTCAT, loginDTO);
        Display[20][4] = LM.getText(LC.EMPLOYEE_INFO_REPORT_SELECT_GENDERCAT, loginDTO);
        officeUnitNameIndex = 2;
        if (!HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng) {
            Display[2][2] = "office_name_bn";
            Display[3][2] = "organogram_name_bn";
        }
        Criteria = PBReportUtils.prepareCriteria(inputs, stringMap);
        if (inputs.contains("officeUnitIds") || inputs.contains("age_start") || inputs.contains("age_end")) {
            for (String[] arr : Criteria) {
                switch (arr[9]) {
                    case "ageStart":
                        long ageStart = Long.parseLong(request.getParameter("age_start"));
                        arr[8] = String.valueOf(calculateLongDateValue(ageStart));
                        break;
                    case "ageEnd":
                        long ageEnd = Long.parseLong(request.getParameter("age_end"));
                        arr[8] = String.valueOf(calculateLongDateValue(ageEnd));
                        break;
                    case "officeUnitIdList":
                        arr[8] = getOfficeIdsFromOfficeUnitIds(request)
                                .stream()
                                .map(String::valueOf)
                                .collect(Collectors.joining(","));
                        break;
                }


            }
        }
        request.setAttribute("dontUseOutDatedModal", true);
        if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.TRAINING_CALENDER_REPORT_DETAILS)) {
            reportGenerate(request, response);
        }
    }

    private long calculateLongDateValue(long ageYears) {
        LocalDate ld = LocalDate.now();
        ld = ld.minusYears(ageYears);
        return Date.from(ld.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant()).getTime();
    }

    private int[] extractIntegers(String officeName) {
        if (officeName == null) {
            return new int[0];
        }
        officeName = StringUtils.convertToEngNumber(officeName)
                                .replaceAll("[^0-9]+", " ")
                                .trim();
        return Arrays.stream(officeName.split(" "))
                     .filter(s -> s.length() > 0)
                     .mapToInt(Integer::parseInt)
                     .toArray();
    }

    private int compareInts(int[] a, int[] b) {
        for (int i = 0, j = 0; i < a.length && j < b.length; ++i, ++j) {
            if (a[i] != b[j]) {
                return Integer.compare(a[i], b[j]);
            }
        }
        return Integer.compare(a.length, b.length);
    }

    private int compareReportRowByOfficeUnitName(List<Object> reportRow1, List<Object> reportRow2) {
        boolean isOfficeUnitNameIndexOutOfBound =
                officeUnitNameIndex < 0
                || officeUnitNameIndex >= reportRow1.size()
                || officeUnitNameIndex >= reportRow2.size();
        if (isOfficeUnitNameIndexOutOfBound) {
            logger.error("officeUnitNameIndex is out of bound. NOT comparing");
            return 0;
        }
        String officeName1 = (String) reportRow1.get(officeUnitNameIndex);
        String officeName2 = (String) reportRow2.get(officeUnitNameIndex);
        return compareInts(extractIntegers(officeName1), extractIntegers(officeName2));
    }

    @Override
    public List<List<Object>> doModificationOnResultList(List<List<Object>> list, String language) {
        if (list == null || list.isEmpty()) {
            return list;
        }
        return Stream.concat(Stream.of(list.get(0)), list.stream().skip(1).sorted(this::compareReportRowByOfficeUnitName))
                     .sequential()
                     .collect(Collectors.toList());
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }

    @Override
    public String[][] getCriteria() {
        return Criteria;
    }

    @Override
    public String[][] getDisplay() {
        return Display;
    }

    @Override
    public String getSQL() {
        return sql;
    }

    @Override
    public int getLCForReportName() {
        return LC.TRAINING_CALENDER_REPORT_OTHER_TRAINING_CALENDER_REPORT;
    }

    @Override
    public String getFileName() {
        return "training_calender_report";
    }

    @Override
    public String getTableName() {
        return "training_calender_report";
    }
}
