package restriction_summary;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.sql.SQLException;
import common.CommonDAOService;
import common.ConnectionAndStatementUtil;

import org.apache.log4j.Logger;

import appointment.AppointmentDTO;
import util.*;
import pb.*;

public class Restriction_summaryDAO  implements CommonDAOService<Restriction_summaryDTO>
{
	
	Logger logger = Logger.getLogger(getClass());
	
	private static String addQuery = "";
	private static String updateQuery = "";
	private String[] columnNames = null;

	private Restriction_summaryDAO()
	{
		columnNames = new String[] 
		{
			"ID",
			"restriction_config_id",
			"restriction_config_details_id",
			"restriction_cat",
			"employee_record_id",
			"restriction_reason",
			"restriction_added",
			"inserted_by",
			"insertion_date",
			"search_column",
			"isDeleted",
			"lastModificationTime"
			
		};
		updateQuery = getUpdateQuery(columnNames);
        addQuery = getAddQuery(columnNames);
		
		searchMap.put("restriction_cat"," and (restriction_cat = ?)");
		searchMap.put("restriction_reason"," and (restriction_reason like ?)");
		searchMap.put("inserted_by"," and (inserted_by like ?)");
		searchMap.put("inserttion_date_start"," and (inserttion_date >= ?)");
		searchMap.put("inserttion_date_end"," and (inserttion_date <= ?)");
		searchMap.put("AnyField"," and (search_column like ?)");
	}

	private final Map<String,String> searchMap = new HashMap<>();

    private static class LazyLoader
    {
    	static final Restriction_summaryDAO INSTANCE = new Restriction_summaryDAO();
	}

	public static Restriction_summaryDAO getInstance()
	{
    	return LazyLoader.INSTANCE;
	}
	
	public void setSearchColumn(Restriction_summaryDTO restriction_summaryDTO)
	{
		restriction_summaryDTO.searchColumn = "";
		restriction_summaryDTO.searchColumn += CatDAO.getName("English", "restriction", restriction_summaryDTO.restrictionCat) + " " + CatDAO.getName("Bangla", "restriction", restriction_summaryDTO.restrictionCat) + " ";
		restriction_summaryDTO.searchColumn += restriction_summaryDTO.restrictionReason + " ";
		restriction_summaryDTO.searchColumn += restriction_summaryDTO.insertedBy + " ";
	}
	
	@Override
	public void set(PreparedStatement ps, Restriction_summaryDTO restriction_summaryDTO, boolean isInsert) throws SQLException
	{
		int index = 0;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(restriction_summaryDTO);
		if(isInsert)
		{
			ps.setObject(++index,restriction_summaryDTO.iD);
		}
		ps.setObject(++index,restriction_summaryDTO.restrictionConfigId);
		ps.setObject(++index,restriction_summaryDTO.restrictionConfigDetailsId);
		ps.setObject(++index,restriction_summaryDTO.restrictionCat);
		ps.setObject(++index,restriction_summaryDTO.employeeRecordId);
		ps.setObject(++index,restriction_summaryDTO.restrictionReason);
		ps.setObject(++index,restriction_summaryDTO.restrictionAdded);
		ps.setObject(++index,restriction_summaryDTO.insertedBy);
		ps.setObject(++index,restriction_summaryDTO.insertionDate);
		ps.setObject(++index,restriction_summaryDTO.searchColumn);
		if(isInsert)
		{
			ps.setObject(++index,restriction_summaryDTO.isDeleted);
		}
		ps.setObject(++index, lastModificationTime);
		if(!isInsert)
		{
			ps.setObject(++index,restriction_summaryDTO.iD);
		}
	}
	
	@Override
	public Restriction_summaryDTO buildObjectFromResultSet(ResultSet rs)
	{
		try
		{
			Restriction_summaryDTO restriction_summaryDTO = new Restriction_summaryDTO();
			int i = 0;
			restriction_summaryDTO.iD = rs.getLong(columnNames[i++]);
			restriction_summaryDTO.restrictionConfigId = rs.getLong(columnNames[i++]);
			restriction_summaryDTO.restrictionConfigDetailsId = rs.getLong(columnNames[i++]);
			restriction_summaryDTO.restrictionCat = rs.getInt(columnNames[i++]);
			restriction_summaryDTO.employeeRecordId = rs.getLong(columnNames[i++]);
			restriction_summaryDTO.restrictionReason = rs.getString(columnNames[i++]);
			restriction_summaryDTO.restrictionAdded = rs.getBoolean(columnNames[i++]);
			restriction_summaryDTO.insertedBy = rs.getLong(columnNames[i++]);
			restriction_summaryDTO.insertionDate = rs.getLong(columnNames[i++]);
			restriction_summaryDTO.searchColumn = rs.getString(columnNames[i++]);
			restriction_summaryDTO.isDeleted = rs.getInt(columnNames[i++]);
			restriction_summaryDTO.lastModificationTime = rs.getLong(columnNames[i++]);
			return restriction_summaryDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
		
	public Restriction_summaryDTO getDTOByID (long id)
	{
		return getDTOFromID(id);
	}

	@Override
	public String getTableName() {
		return "restriction_summary";
	}

	@Override
	public Map<String, String> getSearchMap() {
		return searchMap;
	}

	public long add(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Restriction_summaryDTO) commonDTO,addQuery,true);
	}

	public long update(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Restriction_summaryDTO) commonDTO,updateQuery,false);
	}
	
	@Override
	public String getLastModifierUser(){
		return "";
    }
	
	public Restriction_summaryDTO getLast (long erId, int restrictionCat)
    {
		String sql = "SELECT * FROM " + getTableName() + " WHERE isDeleted = 0 and employee_record_id = " + erId;
		
		sql += " and restriction_cat =  " + restrictionCat + " order by lastModificationTime desc limit 1 ";
		return ConnectionAndStatementUtil.getT(sql,this::buildObjectFromResultSet);	
    }
	
	public List<Long> getAllEmployees ()
    {
		String sql = "SELECT distinct employee_record_id FROM " + getTableName() + " WHERE isDeleted = 0  " ;
		return ConnectionAndStatementUtil.getListOfT(sql,this::getEmp);	
    }
	
	public long getEmp(ResultSet rs)
	{
		long erId = -1;
		try
		{
			erId = rs.getLong("employee_record_id");
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return erId;
		}
		return erId;
	}
				
}
	