package restriction_summary;


import restriction_config.Restriction_configDTO;
import util.*; 


public class Restriction_summaryDTO extends CommonDTO
{

	public long restrictionConfigId = -1;
	public long restrictionConfigDetailsId = -1;
	public int restrictionCat = -1;
	public long employeeRecordId = -1;
    public String restrictionReason = "";
	public long insertedBy = -1;
	public long insertionDate = System.currentTimeMillis();
	public boolean restrictionAdded = false;
	
	public Restriction_summaryDTO()
	{
		
	}
	
	
	public void set(Restriction_configDTO restriction_configDTO, long restrictionConfigDetailsId, int cat, boolean added)
	{
		restrictionConfigId = restriction_configDTO.employeeRecordId;
		this.restrictionConfigDetailsId = restrictionConfigDetailsId;
		restrictionCat = cat;
		restrictionReason = restriction_configDTO.restrictionReason;
		employeeRecordId = restriction_configDTO.employeeRecordId;
		insertedBy = restriction_configDTO.insertedBy;
		insertionDate = restriction_configDTO.insertionDate;
		restrictionAdded = added;
	}
	
    @Override
	public String toString() {
            return "$Restriction_summaryDTO[" +
            " iD = " + iD +
            " restrictionConfigId = " + restrictionConfigId +
            " restrictionConfigDetailsId = " + restrictionConfigDetailsId +
            " restrictionCat = " + restrictionCat +
            " employeeRecordId = " + employeeRecordId +
            " restrictionReason = " + restrictionReason +
            " insertedBy = " + insertedBy +
            " inserttionDate = " + insertionDate +
            " searchColumn = " + searchColumn +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}