package restriction_summary;

import java.io.IOException;
import java.io.*;
import java.text.SimpleDateFormat;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import org.apache.log4j.Logger;

import login.LoginDTO;
import permission.MenuConstants;
import role.PermissionRepository;

import sessionmanager.SessionConstants;
import user.UserDTO;
import util.*;
import javax.servlet.http.*;
import java.util.*;


import geolocation.GeoLocationDAO2;
import language.LC;
import language.LM;
import common.ApiResponse;
import common.BaseServlet;
import com.google.gson.Gson;
import pb.*;
import pbReport.*;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;




/**
 * Servlet implementation class Restriction_summaryServlet
 */
@WebServlet("/Restriction_summaryServlet")
@MultipartConfig
public class Restriction_summaryServlet extends BaseServlet 
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Restriction_summaryServlet.class);

    @Override
    public String getTableName() {
        return Restriction_summaryDAO.getInstance().getTableName();
    }

    @Override
    public String getServletName() {
        return "Restriction_summaryServlet";
    }

    @Override
    public Restriction_summaryDAO getCommonDAOService() {
        return Restriction_summaryDAO.getInstance();
    }
    
    @Override
    public int[] getAddPageMenuConstants() {
        return new int[] {MenuConstants.RESTRICTION_SUMMARY_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[] {MenuConstants.RESTRICTION_SUMMARY_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[] {MenuConstants.RESTRICTION_SUMMARY_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Restriction_summaryServlet.class;
    }
    private final Gson gson = new Gson();
 	

	@Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception 
	{
		
		request.setAttribute("failureMessage", "");
		System.out.println("%%%% addRestriction_summary");
		String language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
		Restriction_summaryDTO restriction_summaryDTO;
		SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
					
		if(addFlag)
		{
			restriction_summaryDTO = new Restriction_summaryDTO();
		}
		else
		{
			restriction_summaryDTO = Restriction_summaryDAO.getInstance().getDTOFromID(Long.parseLong(request.getParameter("iD")));
		}
		
		String Value = "";

		Value = request.getParameter("restrictionConfigId");

		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("restrictionConfigId = " + Value);
		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			restriction_summaryDTO.restrictionConfigId = Long.parseLong(Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}

		Value = request.getParameter("restrictionConfigDetailsId");

		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("restrictionConfigDetailsId = " + Value);
		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			restriction_summaryDTO.restrictionConfigDetailsId = Long.parseLong(Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}

		Value = request.getParameter("restrictionCat");

		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("restrictionCat = " + Value);
		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			restriction_summaryDTO.restrictionCat = Integer.parseInt(Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}

		Value = request.getParameter("employeeRecordId");

		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("employeeRecordId = " + Value);
		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			restriction_summaryDTO.employeeRecordId = Long.parseLong(Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}

		Value = request.getParameter("restrictionReason");

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("restrictionReason = " + Value);
		if(Value != null)
		{
			restriction_summaryDTO.restrictionReason = (Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}

		Value = request.getParameter("insertedBy");

		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("insertedBy = " + Value);
		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			restriction_summaryDTO.insertedBy = Long.parseLong(Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}

		Value = request.getParameter("inserttionDate");

		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("inserttionDate = " + Value);
		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			try 
			{
				Date d = f.parse(Value);
				restriction_summaryDTO.insertionDate = d.getTime();
			}
			catch (Exception e) 
			{
				e.printStackTrace();
				throw new Exception(LM.getText(LC.RESTRICTION_SUMMARY_ADD_INSERTTIONDATE, language) + " " + ErrorMessage.getInvalidMessage(language));
			}
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}
		System.out.println("Done adding  addRestriction_summary dto = " + restriction_summaryDTO);

		if(addFlag == true)
		{
			Restriction_summaryDAO.getInstance().add(restriction_summaryDTO);
		}
		else
		{				
			Restriction_summaryDAO.getInstance().update(restriction_summaryDTO);										
		}
		
		

		return restriction_summaryDTO;

	}
}

