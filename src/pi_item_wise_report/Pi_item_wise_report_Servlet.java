package pi_item_wise_report;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import language.LC;
import language.LM;
import login.LoginDTO;
import org.intellij.lang.annotations.Language;
import permission.MenuConstants;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.ReportRequestHandler;
import pbReport.*;
import util.UtilCharacter;


@WebServlet("/Pi_item_wise_report_Servlet")
public class Pi_item_wise_report_Servlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    String[][] Criteria =
            {
                    {"criteria", "procurement_goods", "procurement_package_type", "=", "", "int", "", "", "any", "procurementPackageType", LC.PI_ITEM_WISE_REPORT_WHERE_PROCUREMENTPACKAGETYPE + ""},
                    {"criteria", "procurement_goods", "procurement_goods_type_type", "=", "AND", "int", "", "", "any", "procurementGoodsTypeType", LC.PI_ITEM_WISE_REPORT_WHERE_PROCUREMENTGOODSTYPETYPE + ""},
                    {"criteria", "procurement_goods", "name_bn", "like", "AND", "String", "", "", "%", "nameBn", LC.PI_ITEM_WISE_REPORT_WHERE_NAMEBN + ""},
                    {"criteria","procurement_goods","is_actual_item","=","AND","String","","","1","isDeleted", LC.PI_OFFICE_WISE_INVENTORY_REPORT_WHERE_ISDELETED + ""},
                    {"criteria","procurement_goods","isDeleted","=","AND","String","","","0","isDeleted", LC.PI_OFFICE_WISE_INVENTORY_REPORT_WHERE_ISDELETED + ""}

            };

    String[][] Display =
            {
                    {"display", "procurement_goods", "procurement_package_type", "type", ""},
                    {"display", "procurement_goods", "procurement_goods_type_type", "type", ""},
                    {"display", "procurement_goods", "name_en", "text", ""},
                    {"display", "procurement_goods", "description", "text", ""},
//                    {"display", "", "CASE WHEN procurement_goods.description != '' THEN CONCAT(procurement_goods.name_bn, ',', procurement_goods.description) ELSE procurement_goods.name_bn END AS ", "text", ""}
            };

    String GroupBy = "";
    String OrderBY = "";

    ReportRequestHandler reportRequestHandler;

    public Pi_item_wise_report_Servlet() {

    }

    private final ReportService reportService = new ReportService();

    private String sql;

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        String language = "english";
        if (userDTO != null && userDTO.languageID == SessionConstants.BANGLA) {
            language = "bangla";
            Display[2][2] = "name_bn";

        }

        String actionType = request.getParameter("actionType");

        System.out.println("In ssservlet doget, actiontype = " + actionType);

        sql = "procurement_goods";

        Display[0][4] = LM.getText(LC.PI_ITEM_WISE_REPORT_SELECT_PROCUREMENTPACKAGETYPE, loginDTO);
        Display[1][4] = LM.getText(LC.PI_ITEM_WISE_REPORT_SELECT_PROCUREMENTGOODSTYPETYPE, loginDTO);
        Display[2][4] = LM.getText(LC.PI_ITEM_WISE_REPORT_SELECT_PROCUREMENTGOODSNAME, loginDTO);
        Display[3][4] = UtilCharacter.getDataByLanguage(language, "আইটেম বিবরণ", "Item Description");


        String reportName = LM.getText(LC.PI_ITEM_WISE_REPORT_OTHER_PI_ITEM_WISE_REPORT, loginDTO);

        reportRequestHandler = new ReportRequestHandler(null,
                Criteria, Display, GroupBy, OrderBY, sql,
                reportService);


        reportRequestHandler.handleReportGet(request, response, userDTO, "pi_item_wise_report",
                MenuConstants.PI_ITEM_WISE_REPORT, language, reportName, "pi_item_wise_report");
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }
}