package date;


import login.LoginDTO;
import org.apache.log4j.Logger;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.CommonRequestHandler;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/DateServlet")
@MultipartConfig
public class DateServlet extends HttpServlet
{
    private static final Logger logger = Logger.getLogger(CommonRequestHandler.class);
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("In doget request = " + request);
        try{
            RequestDispatcher requestDispatcher = request.getRequestDispatcher("date/date.jsp");
            requestDispatcher.forward(request, response);
        }
        catch(Exception ex)
        {
            logger.error("",ex);
        }
    }
}
