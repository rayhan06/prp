package date;

public enum JSDateConfig {
    LANGUAGE,
    DATE_ID,
    START_YEAR,
    END_YEAR,
    HIDE_DAY,
    IS_DISABLED
//    HIDE_MONTH,
//    HIDE_YEAR,
//    MIN_DATE,   //  'DD/MM/YYYY'
//    MAX_DATE   //  'DD/MM/YYYY'
//    DEFAULT_DATE
}
