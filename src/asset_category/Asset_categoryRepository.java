package asset_category;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import brand.BrandDTO;
import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class Asset_categoryRepository implements Repository {
	Asset_categoryDAO asset_categoryDAO = null;
	
	static Logger logger = Logger.getLogger(Asset_categoryRepository.class);
	Map<Long, Asset_categoryDTO>mapOfAsset_categoryDTOToiD;

  
	private Asset_categoryRepository(){
		asset_categoryDAO = Asset_categoryDAO.getInstance();
		mapOfAsset_categoryDTOToiD = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	private static class LazyLoader{
        final static Asset_categoryRepository INSTANCE = new Asset_categoryRepository();
    }

    public static Asset_categoryRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

	public void reload(boolean reloadAll){
		try {
			List<Asset_categoryDTO> asset_categoryDTOs = asset_categoryDAO.getAllDTOs(reloadAll);
			for(Asset_categoryDTO asset_categoryDTO : asset_categoryDTOs) {
				Asset_categoryDTO oldAsset_categoryDTO = getAsset_categoryDTOByID(asset_categoryDTO.iD);
				if( oldAsset_categoryDTO != null ) {
					mapOfAsset_categoryDTOToiD.remove(oldAsset_categoryDTO.iD);
				
					
				}
				if(asset_categoryDTO.isDeleted == 0) 
				{
					
					mapOfAsset_categoryDTOToiD.put(asset_categoryDTO.iD, asset_categoryDTO);
				
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Asset_categoryDTO> getAsset_categoryList() {
		List <Asset_categoryDTO> asset_categorys = new ArrayList<Asset_categoryDTO>(this.mapOfAsset_categoryDTOToiD.values());
		return asset_categorys;
	}
	
	
	public Asset_categoryDTO getAsset_categoryDTOByID( long ID){
		return mapOfAsset_categoryDTOToiD.get(ID);
	}

	
	@Override
	public String getTableName() {
		return "asset_category";
	}

	public String getCategoryText(long ID, boolean isLanguageEnglish) {
		Asset_categoryDTO dto = mapOfAsset_categoryDTOToiD.get(ID);

		return dto==null ? "" : isLanguageEnglish ? dto.nameEn : dto.nameBn;
	}
}


