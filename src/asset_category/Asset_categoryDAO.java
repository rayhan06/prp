package asset_category;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import sessionmanager.SessionConstants;
import java.sql.SQLException;
import common.CommonDAOService;
import common.ConnectionAndStatementUtil;
import org.apache.log4j.Logger;
import util.*;
import pb.*;


public class Asset_categoryDAO  implements CommonDAOService<Asset_categoryDTO>
{
	
	Logger logger = Logger.getLogger(getClass());
	
	private static String addQuery = "";
	private static String updateQuery = "";
	private String[] columnNames = null;

	public Asset_categoryDAO()
	{
		columnNames = new String[] 
		{
			"ID",
			"name_en",
			"name_bn",

			"asset_type_type",

			"parent_list",
			
			"search_column",
			"inserted_by_user_id",
			"inserted_by_organogram_id",
			"insertion_date",
			"last_modifier_user",
			"is_assignable",
			"isDeleted",
			"lastModificationTime"
		};
		updateQuery = getUpdateQuery(columnNames);
        addQuery = getAddQuery(columnNames);
		
		searchMap.put("name_en"," and (name_en like ?)");
		searchMap.put("name_bn"," and (name_bn like ?)");

		searchMap.put("asset_type_type"," and (asset_type_type = ?)");
		searchMap.put("insertion_date_start"," and (insertion_date >= ?)");
		searchMap.put("insertion_date_end"," and (insertion_date <= ?)");
		searchMap.put("AnyField"," and (search_column like ?)");
	}

	private final Map<String,String> searchMap = new HashMap<>();

    private static class LazyLoader
    {
    	static final Asset_categoryDAO INSTANCE = new Asset_categoryDAO();
	}

	public static Asset_categoryDAO getInstance()
	{
    	return LazyLoader.INSTANCE;
	}
	
	public void setSearchColumn(Asset_categoryDTO asset_categoryDTO)
	{
		asset_categoryDTO.searchColumn = "";
		asset_categoryDTO.searchColumn += asset_categoryDTO.nameEn + " ";
		asset_categoryDTO.searchColumn += asset_categoryDTO.nameBn + " ";
		asset_categoryDTO.searchColumn += CommonDAO.getName("English", "asset_type", asset_categoryDTO.assetTypeType) + " " + CommonDAO.getName("Bangla", "asset_type", asset_categoryDTO.assetTypeType) + " ";
	}
	
	@Override
	public void set(PreparedStatement ps, Asset_categoryDTO asset_categoryDTO, boolean isInsert) throws SQLException
	{
		int index = 0;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(asset_categoryDTO);
		if(isInsert)
		{
			ps.setObject(++index,asset_categoryDTO.iD);
		}
		ps.setObject(++index,asset_categoryDTO.nameEn);
		ps.setObject(++index,asset_categoryDTO.nameBn);


		ps.setObject(++index,asset_categoryDTO.assetTypeType);

		ps.setObject(++index,asset_categoryDTO.parentList);
		
		ps.setObject(++index,asset_categoryDTO.searchColumn);
		ps.setObject(++index,asset_categoryDTO.insertedByUserId);
		ps.setObject(++index,asset_categoryDTO.insertedByOrganogramId);
		ps.setObject(++index,asset_categoryDTO.insertionDate);
		ps.setObject(++index,asset_categoryDTO.lastModifierUser);
		ps.setObject(++index,asset_categoryDTO.isAssignable);
		if(isInsert)
		{
			ps.setObject(++index,asset_categoryDTO.isDeleted);
		}
		ps.setObject(++index, lastModificationTime);
		if(!isInsert)
		{
			ps.setObject(++index,asset_categoryDTO.iD);
		}
	}
	
	@Override
	public Asset_categoryDTO buildObjectFromResultSet(ResultSet rs)
	{
		try
		{
			Asset_categoryDTO asset_categoryDTO = new Asset_categoryDTO();
			int i = 0;
			asset_categoryDTO.iD = rs.getLong(columnNames[i++]);
			asset_categoryDTO.nameEn = rs.getString(columnNames[i++]);
			asset_categoryDTO.nameBn = rs.getString(columnNames[i++]);

			
			asset_categoryDTO.assetTypeType = rs.getLong(columnNames[i++]);

			asset_categoryDTO.parentList = rs.getString(columnNames[i++]);
			if(asset_categoryDTO.parentList == null)
			{
				asset_categoryDTO.parentList = "";
			}
			
			asset_categoryDTO.searchColumn = rs.getString(columnNames[i++]);
			asset_categoryDTO.insertedByUserId = rs.getLong(columnNames[i++]);
			asset_categoryDTO.insertedByOrganogramId = rs.getLong(columnNames[i++]);
			asset_categoryDTO.insertionDate = rs.getLong(columnNames[i++]);
			asset_categoryDTO.lastModifierUser = rs.getString(columnNames[i++]);
			asset_categoryDTO.isAssignable = rs.getBoolean(columnNames[i++]);
			asset_categoryDTO.isDeleted = rs.getInt(columnNames[i++]);
			asset_categoryDTO.lastModificationTime = rs.getLong(columnNames[i++]);
			return asset_categoryDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
	public List<Asset_categoryDTO> getNonLicenceAssets()
    {

		String sql = "SELECT * FROM asset_category";
		sql += " WHERE isDeleted =  0 and asset_type_type != " + SessionConstants.ASSET_TYPE_LICENCE;
	

		return ConnectionAndStatementUtil.getListOfT(sql,this::buildObjectFromResultSet);		

    }
	
	public List<Asset_categoryDTO> getAssetsByType(long type)
    {

		String sql = "SELECT * FROM asset_category";
		sql += " WHERE isDeleted =  0 and asset_type_type = " + type + " order by id asc";
	
		return ConnectionAndStatementUtil.getListOfT(sql,this::buildObjectFromResultSet);
    }
	
	public List<Asset_categoryDTO> getAssignableAssets()
    {

		String sql = "SELECT * FROM asset_category";
		sql += " WHERE isDeleted =  0 and is_assignable = 1 order by id asc";
	
		return ConnectionAndStatementUtil.getListOfT(sql,this::buildObjectFromResultSet);
    }
		
	public Asset_categoryDTO getDTOByID (long id)
	{
		return getDTOFromID(id);
	}

	@Override
	public String getTableName() {
		return "asset_category";
	}

	@Override
	public Map<String, String> getSearchMap() {
		return searchMap;
	}

	public long add(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Asset_categoryDTO) commonDTO,addQuery,true);
	}

	public long update(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Asset_categoryDTO) commonDTO,updateQuery,false);
	}
	
	@Override
	public String getLastModifierUser(){
		return "last_modifier_user";
    }
				
}
	