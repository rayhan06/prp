package asset_category;



import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import org.apache.log4j.Logger;
import permission.MenuConstants;
import sessionmanager.SessionConstants;
import user.UserDTO;
import util.*;
import javax.servlet.http.*;
import language.LC;
import language.LM;
import common.BaseServlet;


import asset_type.Asset_typeDTO;
import pb.*;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;

import files.*;



/**
 * Servlet implementation class Asset_categoryServlet
 */
@WebServlet("/Asset_categoryServlet")
@MultipartConfig
public class Asset_categoryServlet extends BaseServlet 
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Asset_categoryServlet.class);

    @Override
    public String getTableName() {
        return Asset_categoryDAO.getInstance().getTableName();
    }

    @Override
    public String getServletName() {
        return "Asset_categoryServlet";
    }

    @Override
    public Asset_categoryDAO getCommonDAOService() {
        return Asset_categoryDAO.getInstance();
    }
    
    @Override
    public int[] getAddPageMenuConstants() {
        return new int[] {MenuConstants.ASSET_CATEGORY_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[] {MenuConstants.ASSET_CATEGORY_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[] {MenuConstants.ASSET_CATEGORY_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Asset_categoryServlet.class;
    }
	FilesDAO filesDAO = new FilesDAO();

 	

	@Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception 
	{
		// TODO Auto-generated method stub
		
		request.setAttribute("failureMessage", "");
		System.out.println("%%%% addAsset_category");
		String language = userDTO.languageID == SessionConstants.ENGLISH? "English":"Bangla";
		Asset_categoryDTO asset_categoryDTO;

					
		if(addFlag == true)
		{
			asset_categoryDTO = new Asset_categoryDTO();
		}
		else
		{
			asset_categoryDTO = Asset_categoryDAO.getInstance().getDTOFromID(Long.parseLong(request.getParameter("iD")));
		}
		
		String Value = "";

		Value = request.getParameter("nameEn");

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("nameEn = " + Value);
		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			asset_categoryDTO.nameEn = (Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			throw new Exception(LM.getText(LC.ASSET_CATEGORY_ADD_NAMEEN, language) + " " + ErrorMessage.getEmptyMessage(language));
		}

		Value = request.getParameter("nameBn");

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("nameBn = " + Value);
		if(Value != null)
		{
			asset_categoryDTO.nameBn = (Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}

	

		Value = request.getParameter("assetTypeType");

		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("assetTypeType = " + Value);
		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			asset_categoryDTO.assetTypeType = Long.parseLong(Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}

		
		
		String[] parents = request.getParameterValues("parent");
		if(parents != null)
		{
			int i = 0;
			asset_categoryDTO.parentList = "";
			for(String parent: parents)
			{
				asset_categoryDTO.parentList += Long.parseLong(parent);
				if(i < parents.length - 1)
				{
					asset_categoryDTO.parentList += ", ";
				}
			}
		}
		
		
	

		if(addFlag)
		{
			asset_categoryDTO.insertedByUserId = userDTO.ID;
		}


		if(addFlag)
		{
			asset_categoryDTO.insertedByOrganogramId = userDTO.organogramID;
		}


		if(addFlag)
		{				
			asset_categoryDTO.insertionDate = TimeConverter.getToday();
		}
		
		Value = request.getParameter("isAssignable");

		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("isAssignable = " + Value);
        asset_categoryDTO.isAssignable = Value != null && !Value.equalsIgnoreCase("");


		asset_categoryDTO.lastModifierUser = userDTO.userName;
		if(asset_categoryDTO.assetTypeType == Asset_typeDTO.HARDWARE 
				|| asset_categoryDTO.assetTypeType == Asset_typeDTO.ACCESSORY
				|| asset_categoryDTO.assetTypeType == Asset_typeDTO.SOFTWARE)
		{
			asset_categoryDTO.isAssignable = true;
		}


		
		
		System.out.println("Done adding  addAsset_category dto 2 = " + asset_categoryDTO);

		
		
		if(addFlag == true)
		{
			Asset_categoryDAO.getInstance().add(asset_categoryDTO);
		}
		else
		{				
			Asset_categoryDAO.getInstance().update(asset_categoryDTO);										
		}
		
	
		return asset_categoryDTO;
					
		
	}
}

