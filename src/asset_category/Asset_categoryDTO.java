package asset_category;
import java.util.*; 
import util.*; 


public class Asset_categoryDTO extends CommonDTO
{

    public String nameEn = "";
    public String nameBn = "";

	public long assetTypeType = -1;

	public long insertedByUserId = -1;
	public long insertedByOrganogramId = -1;
	public long insertionDate = -1;
    public String lastModifierUser = "";
	public boolean isAssignable = true;
	
	public String parentList = "";
	
	public static final long MONITOR = 1400;
	public static final long LAPTOP = 1401;
	public static final long UPS = 1402;
	public static final long PRINTER = 1403;
	public static final long CPU = 1404;
	public static final long SCANNER = 1405;
	public static final long ACCESS_POINT = 1406;
	public static final long DATA_CENTRE = 1407;
	public static final long SWITCH = 1408;
	public static final long NETWORK_ACCESSORIES = 1409;
	public static final long CAMERA_AND_MONITORING = 1410;
	
	public static final long RAM = 1600;
	public static final long HARD_DISK = 1700;
	public static final long PRINTER_ACCESSORY = 1800;
	public static final long PROCESSOR = 1801;
	public static final long MOTHER_BOARD = 1802;
	public static final long SOFTWARE = 1803;
	
		
    @Override
	public String toString() {
            return "$Asset_categoryDTO[" +
            " iD = " + iD +
            " nameEn = " + nameEn +
            " nameBn = " + nameBn +

            " assetTypeType = " + assetTypeType +

            " searchColumn = " + searchColumn +
            " insertedByUserId = " + insertedByUserId +
            " insertedByOrganogramId = " + insertedByOrganogramId +
            " insertionDate = " + insertionDate +
            " lastModifierUser = " + lastModifierUser +
            " isWholesale = " + isAssignable +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}