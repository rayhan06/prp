package model;

public class EmployeeWorkHistory {
    private long employee_record_id;
    private String office_name_eng;
    private String office_name_bng;
    private String designation_eng;
    private String designation_bng;
    private String unit_name_bng;
    private String unit_name_eng;
    private long joining_date;
    private long last_office_date;

    public long getEmployee_record_id() {
        return employee_record_id;
    }

    public void setEmployee_record_id(long employee_record_id) {
        this.employee_record_id = employee_record_id;
    }

    public String getOffice_name_eng() {
        return office_name_eng;
    }

    public void setOffice_name_eng(String office_name_eng) {
        this.office_name_eng = office_name_eng;
    }

    public String getOffice_name_bng() {
        return office_name_bng;
    }

    public void setOffice_name_bng(String office_name_bng) {
        this.office_name_bng = office_name_bng;
    }

    public String getDesignation_eng() {
        return designation_eng;
    }

    public void setDesignation_eng(String designation_eng) {
        this.designation_eng = designation_eng;
    }

    public String getDesignation_bng() {
        return designation_bng;
    }

    public void setDesignation_bng(String designation_bng) {
        this.designation_bng = designation_bng;
    }

    public String getUnit_name_bng() {
        return unit_name_bng;
    }

    public void setUnit_name_bng(String unit_name_bng) {
        this.unit_name_bng = unit_name_bng;
    }

    public String getUnit_name_eng() {
        return unit_name_eng;
    }

    public void setUnit_name_eng(String unit_name_eng) {
        this.unit_name_eng = unit_name_eng;
    }

    public long getJoining_date() {
        return joining_date;
    }

    public void setJoining_date(long joining_date) {
        this.joining_date = joining_date;
    }

    public long getLast_office_date() {
        return last_office_date;
    }

    public void setLast_office_date(long last_office_date) {
        this.last_office_date = last_office_date;
    }
}
