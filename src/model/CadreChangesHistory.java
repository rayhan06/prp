package model;

import db.tables.cadre_changes_history;

public class CadreChangesHistory {

    private long id;
    private long employee_record_id;
    private String employee_name;
    private String prev_username;
    private long prev_is_cadre;
    private long prev_employee_cadre_id;
    private String cadre_name;
    private long prev_employee_batch_id;
    private String batch_name;
    private String prev_identity_no;
    private String prev_appointment_memo_no;
    private long modified_by;
    private String modified_by_name;
    private long prev_joining_date;
    private boolean isDeleted;
    private long lastModificationTime;

    public CadreChangesHistory() {
    }

    public CadreChangesHistory(cadre_changes_history table) {
        id = table.getId();
        employee_record_id = table.getEmployee_record_id();
        prev_username = table.getPrev_username();
        prev_is_cadre = table.getPrev_is_cadre();
        prev_employee_cadre_id = table.getPrev_employee_cadre_id();
        prev_employee_batch_id = table.getPrev_employee_batch_id();
        prev_identity_no = table.getPrev_identity_no();
        prev_appointment_memo_no = table.getPrev_appointment_memo_no();
        modified_by = table.getModified_by();
        prev_joining_date = table.getPrev_joining_date();
        isDeleted = table.getIsDeleted();
        lastModificationTime = table.getLastModificationTime();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getEmployee_record_id() {
        return employee_record_id;
    }

    public void setEmployee_record_id(long employee_record_id) {
        this.employee_record_id = employee_record_id;
    }

    public String getEmployee_name() {
        return employee_name;
    }

    public void setEmployee_name(String employee_name) {
        this.employee_name = employee_name;
    }

    public String getPrev_username() {
        return prev_username;
    }

    public void setPrev_username(String prev_username) {
        this.prev_username = prev_username;
    }

    public long getPrev_is_cadre() {
        return prev_is_cadre;
    }

    public void setPrev_is_cadre(long prev_is_cadre) {
        this.prev_is_cadre = prev_is_cadre;
    }

    public long getPrev_employee_cadre_id() {
        return prev_employee_cadre_id;
    }

    public void setPrev_employee_cadre_id(long prev_employee_cadre_id) {
        this.prev_employee_cadre_id = prev_employee_cadre_id;
    }

    public String getCadre_name() {
        return cadre_name;
    }

    public void setCadre_name(String cadre_name) {
        this.cadre_name = cadre_name;
    }

    public long getPrev_employee_batch_id() {
        return prev_employee_batch_id;
    }

    public void setPrev_employee_batch_id(long prev_employee_batch_id) {
        this.prev_employee_batch_id = prev_employee_batch_id;
    }

    public String getBatch_name() {
        return batch_name;
    }

    public void setBatch_name(String batch_name) {
        this.batch_name = batch_name;
    }

    public String getPrev_identity_no() {
        return prev_identity_no;
    }

    public void setPrev_identity_no(String prev_identity_no) {
        this.prev_identity_no = prev_identity_no;
    }

    public String getPrev_appointment_memo_no() {
        return prev_appointment_memo_no;
    }

    public void setPrev_appointment_memo_no(String prev_appointment_memo_no) {
        this.prev_appointment_memo_no = prev_appointment_memo_no;
    }

    public long getModified_by() {
        return modified_by;
    }

    public void setModified_by(long modified_by) {
        this.modified_by = modified_by;
    }

    public String getModified_by_name() {
        return modified_by_name;
    }

    public void setModified_by_name(String modified_by_name) {
        this.modified_by_name = modified_by_name;
    }

    public long getPrev_joining_date() {
        return prev_joining_date;
    }

    public void setPrev_joining_date(long prev_joining_date) {
        this.prev_joining_date = prev_joining_date;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }

    public long getLastModificationTime() {
        return lastModificationTime;
    }

    public void setLastModificationTime(long lastModificationTime) {
        this.lastModificationTime = lastModificationTime;
    }
}
