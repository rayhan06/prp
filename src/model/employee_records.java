package model;

public class employee_records {
    private long id;
    private String name_eng;
    private String name_bng;
    private String father_name_eng;
    private String father_name_bng;
    private String mother_name_eng;
    private String mother_name_bng;
    private long date_of_birth;
    private String place_of_birth;
    private String nationality;
    private String present_address;
    private String permanent_address;
    private String occupation;
    private String nid;
    private boolean nid_valid;
    private String bcn;
    private String ppn;
    private String gender;
    private String religion;
    private String blood_group;
    private String marital_status;
    private String spouse_name_eng;
    private String spouse_name_bng;
    private String personal_email;
    private String personal_mobile;
    private String alternative_mobile;
    private boolean is_cadre;
    private String employee_cadre_id;
    private String employee_batch_id;
    private String identity_no;
    private String appointment_memo_no;
    private long joining_date;
    private int service_rank_id;
    private int service_grade_id;
    private int service_ministry_id;
    private int service_office_id;
    private int current_office_ministry_id;
    private int current_office_layer_id;
    private int current_office_id;
    private int current_office_unit_id;
    private long current_office_joining_date;
    private int current_office_designation_id;
    private String current_office_address;
    private String e_sign;
    private String d_sign;
    private String image_file_name;
    private boolean status;
    private int created_by;
    private int modified_by;
    private long created;
    private long modified;
    private boolean isDeleted;
    private long lastModificationTime;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName_eng() {
        return name_eng;
    }

    public void setName_eng(String name_eng) {
        this.name_eng = name_eng;
    }

    public String getName_bng() {
        return name_bng;
    }

    public void setName_bng(String name_bng) {
        this.name_bng = name_bng;
    }

    public String getFather_name_eng() {
        return father_name_eng;
    }

    public void setFather_name_eng(String father_name_eng) {
        this.father_name_eng = father_name_eng;
    }

    public String getFather_name_bng() {
        return father_name_bng;
    }

    public void setFather_name_bng(String father_name_bng) {
        this.father_name_bng = father_name_bng;
    }

    public String getMother_name_eng() {
        return mother_name_eng;
    }

    public void setMother_name_eng(String mother_name_eng) {
        this.mother_name_eng = mother_name_eng;
    }

    public String getMother_name_bng() {
        return mother_name_bng;
    }

    public void setMother_name_bng(String mother_name_bng) {
        this.mother_name_bng = mother_name_bng;
    }

    public long getDate_of_birth() {
        return date_of_birth;
    }

    public void setDate_of_birth(long date_of_birth) {
        this.date_of_birth = date_of_birth;
    }

    public String getPlace_of_birth() {
        return place_of_birth;
    }

    public void setPlace_of_birth(String place_of_birth) {
        this.place_of_birth = place_of_birth;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getPresent_address() {
        return present_address;
    }

    public void setPresent_address(String present_address) {
        this.present_address = present_address;
    }

    public String getPermanent_address() {
        return permanent_address;
    }

    public void setPermanent_address(String permanent_address) {
        this.permanent_address = permanent_address;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getNid() {
        return nid;
    }

    public void setNid(String nid) {
        this.nid = nid;
    }

    public boolean getNid_valid() {
        return nid_valid;
    }

    public void setNid_valid(boolean nid_valid) {
        this.nid_valid = nid_valid;
    }

    public String getBcn() {
        return bcn;
    }

    public void setBcn(String bcn) {
        this.bcn = bcn;
    }

    public String getPpn() {
        return ppn;
    }

    public void setPpn(String ppn) {
        this.ppn = ppn;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getReligion() {
        return religion;
    }

    public void setReligion(String religion) {
        this.religion = religion;
    }

    public String getBlood_group() {
        return blood_group;
    }

    public void setBlood_group(String blood_group) {
        this.blood_group = blood_group;
    }

    public String getMarital_status() {
        return marital_status;
    }

    public void setMarital_status(String marital_status) {
        this.marital_status = marital_status;
    }

    public String getSpouse_name_eng() {
        return spouse_name_eng;
    }

    public void setSpouse_name_eng(String spouse_name_eng) {
        this.spouse_name_eng = spouse_name_eng;
    }

    public String getSpouse_name_bng() {
        return spouse_name_bng;
    }

    public void setSpouse_name_bng(String spouse_name_bng) {
        this.spouse_name_bng = spouse_name_bng;
    }

    public String getPersonal_email() {
        return personal_email;
    }

    public void setPersonal_email(String personal_email) {
        this.personal_email = personal_email;
    }

    public String getPersonal_mobile() {
        return personal_mobile;
    }

    public void setPersonal_mobile(String personal_mobile) {
        this.personal_mobile = personal_mobile;
    }

    public String getAlternative_mobile() {
        return alternative_mobile;
    }

    public void setAlternative_mobile(String alternative_mobile) {
        this.alternative_mobile = alternative_mobile;
    }

    public boolean getIs_cadre() {
        return is_cadre;
    }

    public void setIs_cadre(boolean is_cadre) {
        this.is_cadre = is_cadre;
    }

    public String getEmployee_cadre_id() {
        return employee_cadre_id;
    }

    public void setEmployee_cadre_id(String employee_cadre_id) {
        this.employee_cadre_id = employee_cadre_id;
    }

    public String getEmployee_batch_id() {
        return employee_batch_id;
    }

    public void setEmployee_batch_id(String employee_batch_id) {
        this.employee_batch_id = employee_batch_id;
    }

    public String getIdentity_no() {
        return identity_no;
    }

    public void setIdentity_no(String identity_no) {
        this.identity_no = identity_no;
    }

    public String getAppointment_memo_no() {
        return appointment_memo_no;
    }

    public void setAppointment_memo_no(String appointment_memo_no) {
        this.appointment_memo_no = appointment_memo_no;
    }

    public long getJoining_date() {
        return joining_date;
    }

    public void setJoining_date(long joining_date) {
        this.joining_date = joining_date;
    }

    public int getService_rank_id() {
        return service_rank_id;
    }

    public void setService_rank_id(int service_rank_id) {
        this.service_rank_id = service_rank_id;
    }

    public int getService_grade_id() {
        return service_grade_id;
    }

    public void setService_grade_id(int service_grade_id) {
        this.service_grade_id = service_grade_id;
    }

    public int getService_ministry_id() {
        return service_ministry_id;
    }

    public void setService_ministry_id(int service_ministry_id) {
        this.service_ministry_id = service_ministry_id;
    }

    public int getService_office_id() {
        return service_office_id;
    }

    public void setService_office_id(int service_office_id) {
        this.service_office_id = service_office_id;
    }

    public int getCurrent_office_ministry_id() {
        return current_office_ministry_id;
    }

    public void setCurrent_office_ministry_id(int current_office_ministry_id) {
        this.current_office_ministry_id = current_office_ministry_id;
    }

    public int getCurrent_office_layer_id() {
        return current_office_layer_id;
    }

    public void setCurrent_office_layer_id(int current_office_layer_id) {
        this.current_office_layer_id = current_office_layer_id;
    }

    public int getCurrent_office_id() {
        return current_office_id;
    }

    public void setCurrent_office_id(int current_office_id) {
        this.current_office_id = current_office_id;
    }

    public int getCurrent_office_unit_id() {
        return current_office_unit_id;
    }

    public void setCurrent_office_unit_id(int current_office_unit_id) {
        this.current_office_unit_id = current_office_unit_id;
    }

    public long getCurrent_office_joining_date() {
        return current_office_joining_date;
    }

    public void setCurrent_office_joining_date(long current_office_joining_date) {
        this.current_office_joining_date = current_office_joining_date;
    }

    public int getCurrent_office_designation_id() {
        return current_office_designation_id;
    }

    public void setCurrent_office_designation_id(int current_office_designation_id) {
        this.current_office_designation_id = current_office_designation_id;
    }

    public String getCurrent_office_address() {
        return current_office_address;
    }

    public void setCurrent_office_address(String current_office_address) {
        this.current_office_address = current_office_address;
    }

    public String getE_sign() {
        return e_sign;
    }

    public void setE_sign(String e_sign) {
        this.e_sign = e_sign;
    }

    public String getD_sign() {
        return d_sign;
    }

    public void setD_sign(String d_sign) {
        this.d_sign = d_sign;
    }

    public String getImage_file_name() {
        return image_file_name;
    }

    public void setImage_file_name(String image_file_name) {
        this.image_file_name = image_file_name;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public int getCreated_by() {
        return created_by;
    }

    public void setCreated_by(int created_by) {
        this.created_by = created_by;
    }

    public int getModified_by() {
        return modified_by;
    }

    public void setModified_by(int modified_by) {
        this.modified_by = modified_by;
    }

    public long getCreated() {
        return created;
    }

    public void setCreated(long created) {
        this.created = created;
    }

    public long getModified() {
        return modified;
    }

    public void setModified(long modified) {
        this.modified = modified;
    }

    public boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(boolean deleted) {
        isDeleted = deleted;
    }

    public long getLastModificationTime() {
        return lastModificationTime;
    }

    public void setLastModificationTime(long lastModificationTime) {
        this.lastModificationTime = lastModificationTime;
    }
}
