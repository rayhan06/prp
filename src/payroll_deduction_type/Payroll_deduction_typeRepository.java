package payroll_deduction_type;

import repository.Repository;
import repository.RepositoryManager;
import util.LockManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;


public class Payroll_deduction_typeRepository implements Repository {
    private final Payroll_deduction_typeDAO payroll_deduction_typeDAO;
    private final Map<Long, Payroll_deduction_typeDTO> mapById;

    private Payroll_deduction_typeRepository() {
        payroll_deduction_typeDAO = Payroll_deduction_typeDAO.getInstance();
        mapById = new ConcurrentHashMap<>();
        RepositoryManager.getInstance().addRepository(this);
    }

    private static class LazyLoader {
        final static Payroll_deduction_typeRepository INSTANCE = new Payroll_deduction_typeRepository();
    }

    public static Payroll_deduction_typeRepository getInstance() {
        return Payroll_deduction_typeRepository.LazyLoader.INSTANCE;
    }

    public void reload(boolean reloadAll) {
        List<Payroll_deduction_typeDTO> list = payroll_deduction_typeDAO.getAllDTOs(true);
        if (list != null && list.size() > 0) {
            list.forEach(dto -> mapById.put(dto.iD, dto));
        }
    }

    public List<Payroll_deduction_typeDTO> getPayroll_deduction_typeList() {
        return new ArrayList<>(mapById.values());
    }

    public Payroll_deduction_typeDTO getById(long id) {
        if (mapById.get(id) == null) {
            synchronized (LockManager.getLock(id + "PDTR")) {
                if (mapById.get(id) == null) {
                    Payroll_deduction_typeDTO dto = payroll_deduction_typeDAO.getDTOFromIdDeletedOrNot(id);
                    if (dto != null) {
                        mapById.put(dto.iD, dto);
                    }
                }
            }

        }
        return mapById.get(id);
    }

    public List<Payroll_deduction_typeDTO> getByCode(String code) {
        return getPayroll_deduction_typeList().stream().filter(dto -> dto.code.equalsIgnoreCase(code)).collect(Collectors.toList());
    }

    @Override
    public String getTableName() {
        return "payroll_deduction_type";
    }

}