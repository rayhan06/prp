package payroll_deduction_type;

import util.CommonDTO;


public class Payroll_deduction_typeDTO extends CommonDTO {
    public String code = "";
    public String descriptionEn = "";
    public String descriptionBn = "";
    public long insertedBy = -1;
    public long insertionTime = -1;
    public long modifiedBy = -1;

    @Override
    public String toString() {
        return "$Payroll_deduction_typeDTO[" +
                " iD = " + iD +
                " code = " + code +
                " descriptionEn = " + descriptionEn +
                " descriptionBn = " + descriptionBn +
                " insertedBy = " + insertedBy +
                " insertionTime = " + insertionTime +
                " isDeleted = " + isDeleted +
                " modifiedBy = " + modifiedBy +
                " lastModificationTime = " + lastModificationTime +
                " searchColumn = " + searchColumn +
                "]";
    }
}