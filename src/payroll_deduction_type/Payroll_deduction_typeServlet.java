package payroll_deduction_type;

import common.BaseServlet;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import payroll_allowance_configuration.Payroll_allowance_configurationRepository;
import pb.Utils;
import permission.MenuConstants;
import user.UserDTO;
import util.CommonDTO;
import util.HttpRequestUtils;

import javax.rmi.CORBA.Util;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;


@WebServlet("/Payroll_deduction_typeServlet")
@MultipartConfig
public class Payroll_deduction_typeServlet extends BaseServlet {
    private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Payroll_deduction_typeServlet.class);
    private final Payroll_deduction_typeDAO payroll_deduction_typeDAO = Payroll_deduction_typeDAO.getInstance();

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        Payroll_deduction_typeDTO deductionTypeDTO;
        boolean isLangEng= HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
        long curTime = System.currentTimeMillis();
        if (addFlag) {
            deductionTypeDTO = new Payroll_deduction_typeDTO();
            deductionTypeDTO.insertedBy = userDTO.ID;
            deductionTypeDTO.insertionTime = curTime;
        } else {
            deductionTypeDTO = payroll_deduction_typeDAO.getDTOFromID(Long.parseLong(request.getParameter("iD")));
        }
        deductionTypeDTO.code = Utils.getDigits(Jsoup.clean(request.getParameter("code"), Whitelist.simpleText()),"English");
        if(Payroll_deduction_typeRepository.getInstance().getByCode(deductionTypeDTO.code).size()>0){
            throw new Exception(isLangEng?"Same Code No already exists,Give different code":"একই কোড নং বর্তমানে আছে,ভিন্ন কোড দিন");
        }
        deductionTypeDTO.descriptionEn = Jsoup.clean(request.getParameter("descriptionEn"), Whitelist.simpleText());
        deductionTypeDTO.descriptionBn = Jsoup.clean(request.getParameter("descriptionBn"), Whitelist.simpleText());
        deductionTypeDTO.modifiedBy = userDTO.ID;
        deductionTypeDTO.lastModificationTime = curTime;
        if (addFlag) {
            payroll_deduction_typeDAO.add(deductionTypeDTO);
        } else {
            payroll_deduction_typeDAO.update(deductionTypeDTO);
        }
        return deductionTypeDTO;
    }

    @Override
    public String getTableName() {
        return "payroll_deduction_type";
    }

    @Override
    public String getServletName() {
        return "Payroll_deduction_typeServlet";
    }

    @Override
    public Payroll_deduction_typeDAO getCommonDAOService() {
        return payroll_deduction_typeDAO;
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[]{MenuConstants.PAYROLL_DEDUCTION_TYPE_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[]{MenuConstants.PAYROLL_DEDUCTION_TYPE_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[]{MenuConstants.PAYROLL_DEDUCTION_TYPE_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Payroll_deduction_typeServlet.class;
    }
}

