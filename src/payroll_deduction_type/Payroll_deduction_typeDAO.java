package payroll_deduction_type;

import allowance_configure.Allowance_configureDAO;
import common.CommonDAOService;
import org.apache.log4j.Logger;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

@SuppressWarnings({"Duplicates"})
public class Payroll_deduction_typeDAO implements CommonDAOService<Payroll_deduction_typeDTO> {
    private static final Logger logger = Logger.getLogger(Allowance_configureDAO.class);

    private static final String addQuery =
            "INSERT INTO {tableName} (code,description_en,description_bn,modified_by,lastModificationTime,search_column,"
                    .concat("inserted_by,insertion_time,isDeleted,ID) VALUES (?,?,?,?,?,?,?,?,?,?)");

    private static final String updateQuery =
            "UPDATE {tableName} SET code=?,description_en=?,description_bn=?,"
                    .concat("modified_by=?,lastModificationTime=?,search_column=? WHERE ID=?");

    private static final Map<String, String> searchMap = new HashMap<>();

    private Payroll_deduction_typeDAO() {
        searchMap.put("AnyField", " AND (search_column LIKE ?) ");
    }


    private static class LazyLoader {
        static final Payroll_deduction_typeDAO INSTANCE = new Payroll_deduction_typeDAO();
    }

    public static Payroll_deduction_typeDAO getInstance() {
        return Payroll_deduction_typeDAO.LazyLoader.INSTANCE;
    }

    @Override
    public void set(PreparedStatement ps, Payroll_deduction_typeDTO dto, boolean isInsert) throws SQLException {
        int index = 0;
        setSearchColumn(dto);
        ps.setString(++index, dto.code);
        ps.setString(++index, dto.descriptionEn);
        ps.setString(++index, dto.descriptionBn);
        ps.setLong(++index, dto.modifiedBy);
        ps.setLong(++index, dto.lastModificationTime);
        ps.setString(++index, dto.searchColumn);
        if (isInsert) {
            ps.setLong(++index, dto.insertedBy);
            ps.setLong(++index, dto.insertionTime);
            ps.setInt(++index, 0);
        }
        ps.setLong(++index, dto.iD);
    }

    @Override
    public Payroll_deduction_typeDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Payroll_deduction_typeDTO dto = new Payroll_deduction_typeDTO();
            dto.iD = rs.getLong("ID");
            dto.code = rs.getString("code");
            dto.descriptionEn = rs.getString("description_en");
            dto.descriptionBn = rs.getString("description_bn");
            dto.insertedBy = rs.getLong("inserted_by");
            dto.insertionTime = rs.getLong("insertion_time");
            dto.isDeleted = rs.getInt("isDeleted");
            dto.modifiedBy = rs.getLong("modified_by");
            dto.lastModificationTime = rs.getLong("lastModificationTime");
            dto.searchColumn = rs.getString("search_column");
            return dto;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    @Override
    public String getTableName() {
        return "payroll_deduction_type";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    @Override
    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Payroll_deduction_typeDTO) commonDTO, addQuery, true);
    }

    @Override
    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Payroll_deduction_typeDTO) commonDTO, updateQuery, false);
    }

    public void setSearchColumn(Payroll_deduction_typeDTO payroll_deduction_typeDTO) {
        payroll_deduction_typeDTO.searchColumn = payroll_deduction_typeDTO.code + " " + payroll_deduction_typeDTO.descriptionEn + " " + payroll_deduction_typeDTO.descriptionBn;
    }
}