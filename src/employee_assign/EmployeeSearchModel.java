package employee_assign;

import com.google.gson.Gson;

public class EmployeeSearchModel {
    public long employeeRecordId = 0L;
    public String userName = "";
    public String employeeNameEn = "";
    public String employeeNameBn = "";
    public String email = "";
    public String phoneNumber = "";

    public long officeUnitId = 0L;
    public String officeUnitNameEn = "";
    public String officeUnitNameBn = "";

    public long organogramId = 0L;
    public String organogramNameEn = "";
    public String organogramNameBn = "";

    public String getJSON(){
        return new Gson().toJson(this);
    }
}
