package employee_assign;

import common.ConnectionAndStatementUtil;
import org.apache.log4j.Logger;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;

public class EmployeeAssignRepository {
    private static final Logger logger = Logger.getLogger(EmployeeAssignRepository.class);
    private static final String getAllOfficeUint = "select ou.id as office_unit_id, ou.unit_name_bng, ou.unit_name_eng,ou.office_id, "
            .concat(" ouo.id as organogram_id,ouo.designation_bng, ouo.designation_eng, ouo.designation_level,")
            .concat(" ouo.designation_sequence from office_units ou inner join office_unit_organograms ouo " )
            .concat(" on ou.id = ouo.office_unit_id where ou.isDeleted = 0");

    private Map<Long,List<OfficeUnitAndOrganogramModel>> mapByOfficeUnitId;
    private List<OfficeUnitAndOrganogramModel> officeUnitList;

    private EmployeeAssignRepository(){
        reload();
    }

    private static class LazyLoader{
        static EmployeeAssignRepository INSTANCE = new EmployeeAssignRepository();
    }

    public static EmployeeAssignRepository getInstance(){
        return LazyLoader.INSTANCE;
    }

    private OfficeUnitAndOrganogramModel buildOfficeUnitAndOrganogramModel(ResultSet rs){
        OfficeUnitAndOrganogramModel model = new OfficeUnitAndOrganogramModel();
        try{
            model.officeUnitId = rs.getLong("office_unit_id");
            model.unitNameBng = rs.getString("unit_name_bng");
            model.unitNameEng = rs.getString("unit_name_eng");
            model.officeId = rs.getLong("office_id");
            model.organogramId = rs.getInt("organogram_id");
            model.designationBng = rs.getString("designation_bng");
            model.designationEng = rs.getString("designation_eng");
            model.designationLevel = rs.getInt("designation_level");
            model.designationSequence = rs.getInt("designation_sequence");
            return model;
        }catch (SQLException ex){
            logger.error(ex);
            return null;
        }
    }

    private void reload(){
       List<OfficeUnitAndOrganogramModel> modelList = ConnectionAndStatementUtil.getListOfT(getAllOfficeUint,this::buildOfficeUnitAndOrganogramModel);
       if(modelList!=null && modelList.size()>0){
           mapByOfficeUnitId = modelList.stream()
                   .collect(Collectors.groupingBy(model->model.officeUnitId));
           officeUnitList = modelList;
           officeUnitList.sort(Comparator.comparingLong(o -> o.officeUnitId));
       }else{
           mapByOfficeUnitId = new HashMap<>();
           officeUnitList = new ArrayList<>();
       }
    }

    public List<OfficeUnitAndOrganogramModel> getAllOfficeUnits(){
        return officeUnitList;
    }

    public List<OfficeUnitAndOrganogramModel> getOfficeUnitAndOrganogramModelByOfficeUnitId(long officeUnitId){
        return mapByOfficeUnitId.get(officeUnitId);
    }
}
