package employee_assign;

import com.google.gson.Gson;
import common.CommonDAOService;
import common.ConnectionAndStatementUtil;
import common.VbSequencerService;
import dbm.DBMR;
import dbm.DBMW;
import employee_office_report.InChargeLevelEnum;
import employee_offices.EmployeeOfficeDTO;
import employee_offices.EmployeeOfficesDAO;
import employee_records.EmpOfficeModel;
import employee_records.Employee_recordsDTO;
import employee_records.Employee_recordsRepository;
import grade_wise_pay_scale.Grade_wise_pay_scaleRepository;
import office_unit_organograms.OfficeUnitOrganograms;
import office_unit_organograms.OfficeUnitOrganogramsRepository;
import office_units.Office_unitsDTO;
import office_units.Office_unitsRepository;
import org.apache.log4j.Logger;
import pb.CatRepository;
import pb.Utils;
import pi_unique_item.PiUniqueItemAssignmentDAO;
import sessionmanager.SessionConstants;
import test_lib.util.Pair;
import user.UserDAO;
import util.CommonConstant;
import util.StringUtils;

import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.*;
import java.util.stream.Collectors;

@SuppressWarnings({"rawtypes", "unused", "unchecked"})
public class EmployeeAssignDAO implements VbSequencerService {

    private static final Logger logger = Logger.getLogger(EmployeeAssignDAO.class);

    private static final String getOfcUnitAndOfcUnitOrgmByOfcId = "select ou.id as office_unit_id, ou.unit_name_bng, ou.unit_name_eng,ou.office_id,"
            .concat(" ouo.id as organogram_id,ouo.designation_bng, ouo.designation_eng, ouo.designation_level,")
            .concat(" ouo.designation_sequence, ouo.brief_description_eng, ouo.brief_description_bng")
            .concat(" from (select * from office_units where id = %d and office_id = %d and isDeleted = 0) ou")
            .concat(" inner join(select * from office_unit_organograms where office_unit_id = %d and isDeleted = 0) ouo on ou.id = ouo.office_unit_id");

    private static final String getEmployeeOffices = "select * from employee_offices where office_id = %d and office_unit_id = %d " +
            " and office_unit_organogram_id in  (%s) and (last_office_date = -62135791200000 OR last_office_date> %d) and isDeleted = 0 and status = 1";

    private static final String getEmpRecordsAndUsers = "select * from( select * from employee_records where id in ( %s) and isDeleted = 0) er"
            .concat(" inner join (select * from users where employee_record_id in ( %s) and isDeleted = 0) u")
            .concat(" on er.id = u.employee_record_id and u.username = er.employee_number");

    private static final String addEmployeeOfficeQuery = "INSERT INTO employee_offices (ID,employee_record_id,identification_number,office_id,office_unit_id,office_unit_organogram_id,designation," +
            "designation_level,designation_sequence,is_default_role,office_head,summary_nothi_post_type,incharge_label,main_role_id,joining_date,last_office_date," +
            "status,status_change_date,show_unit,job_grade_type_cat,grade_type_level, layer_1,layer_2,created,modified,created_by,modified_by,role_type,lastModificationTime,isDeleted,isDefault,designation_en,designation_bn," +
            "unit_name_bn,unit_name_en,office_name_bn,office_name_en, salary_grade_type)" +
            "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
    private static final String updateEmployeeOfficeStatus = "UPDATE employee_offices SET status = 0 where id = %d";

    private static final String updateLastOfficeDateQuery = "UPDATE employee_offices SET last_office_date = %d,status = %d,lastModificationTime=%d WHERE id = %d";

    private static final String updateLastOfficeDateByOrganogramQuery = "UPDATE employee_offices SET last_office_date = %d,status = %d,lastModificationTime=%d WHERE office_unit_organogram_id = %d and status = 1";

    private static final String updateLastOfficeDateByOrgIdList = "UPDATE employee_offices SET last_office_date = %d,status = %d,lastModificationTime=%d WHERE office_unit_organogram_id in (%s) and status = 1 and isDeleted = 0";

    public static final Map<String, String> employeeModalSearchMap = new HashMap<>();

    static {
        employeeModalSearchMap.put("employee_number", "AND employee_number ={}");
        employeeModalSearchMap.put("name_eng", "AND name_eng like '%{}%'");
        employeeModalSearchMap.put("name_bng", "AND name_bng like '%{}%'");
        employeeModalSearchMap.put("personal_mobile", "AND personal_mobile = '{}'");
    }

    private static class OfcAndOfcOrgm {
        long office_unit_id;
        String unit_name_bng;
        String unit_name_eng;
        long organogram_id;
        String designation_bng;
        String designation_eng;
        int designation_level;
        int designation_sequence;
        String brief_description_bng;
        String brief_description_eng;
    }

    private OfcAndOfcOrgm buildOfcAndOfcOrgm(ResultSet rs) {
        try {
            OfcAndOfcOrgm obj = new OfcAndOfcOrgm();
            obj.office_unit_id = rs.getLong("office_unit_id");
            obj.unit_name_bng = rs.getString("unit_name_bng");
            obj.unit_name_eng = rs.getString("unit_name_eng");
            obj.organogram_id = rs.getLong("organogram_id");
            obj.designation_bng = rs.getString("designation_bng");
            obj.designation_eng = rs.getString("designation_eng");
            obj.designation_level = rs.getInt("designation_level");
            obj.designation_sequence = rs.getInt("designation_sequence");
            obj.brief_description_eng = rs.getString("brief_description_eng");
            obj.brief_description_bng = rs.getString("brief_description_bng");
            return obj;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    private static class EmployeeOffice {
        long employee_office_id;
        long employee_record_id;
        long joining_date;
        long last_office_date;
        long office_unit_organogram_id;
        boolean status;
        String inChargeLabel;
        long gradeTypeLevel;
        Integer organogramRole;
        long salaryGradeType;
    }

    private EmployeeOffice buildEmployeeOffice(ResultSet rs) {
        try {
            EmployeeOffice employeeOffice = new EmployeeOffice();
            employeeOffice.employee_office_id = rs.getLong("id");
            employeeOffice.employee_record_id = rs.getLong("employee_record_id");
            employeeOffice.joining_date = rs.getLong("joining_date");
            employeeOffice.last_office_date = rs.getLong("last_office_date");
            employeeOffice.office_unit_organogram_id = rs.getLong("office_unit_organogram_id");
            employeeOffice.status = rs.getBoolean("status");
            employeeOffice.inChargeLabel = rs.getString("incharge_label");
            employeeOffice.gradeTypeLevel = rs.getLong("grade_type_level");
            employeeOffice.organogramRole = rs.getInt("organogram_role");
            employeeOffice.salaryGradeType = rs.getLong("salary_grade_type");
            return employeeOffice;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    private static class EmpRecordAndUser {
        long employee_id;
        String employee_name_eng;
        String employee_name_bng;
        String username;
    }

    private EmpRecordAndUser buildEmpRecordAndUser(ResultSet rs) {
        try {
            EmpRecordAndUser obj = new EmpRecordAndUser();
            obj.employee_id = rs.getLong("id");
            obj.employee_name_eng = rs.getString("name_eng");
            obj.employee_name_bng = rs.getString("name_bng");
            obj.username = rs.getString("username");
            return obj;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    private EmpOfficeModel buildOfcAndOfcOrgmForSearch(ResultSet rs) {
        try {
            EmpOfficeModel model = new EmpOfficeModel();
            model.employeeRecordId = rs.getLong("employee_record_id");
            model.officeUnitId = rs.getLong("office_unit_id");
            model.organogramId = model.officeUnitOrganogramId = rs.getLong("office_unit_organogram_id");
            return model;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    private EmployeeAssignDTO buildFlatDTO(EmpOfficeModel model) {
        EmployeeAssignDTO flatInfoDTO = new EmployeeAssignDTO();
        Employee_recordsRepository recordsRepository = Employee_recordsRepository.getInstance();
        Office_unitsRepository officeUnitsRepository = Office_unitsRepository.getInstance();
        OfficeUnitOrganogramsRepository organogramsRepository = OfficeUnitOrganogramsRepository.getInstance();
        flatInfoDTO.employee_id = model.employeeRecordId;
        flatInfoDTO.employee_name_eng = recordsRepository.getEmployeeName(flatInfoDTO.employee_id, "EMGLISH");
        flatInfoDTO.employee_name_bng = recordsRepository.getEmployeeName(flatInfoDTO.employee_id, "BANGLA");
        flatInfoDTO.office_unit_id = model.officeUnitId;
        flatInfoDTO.unit_name_eng = officeUnitsRepository.geText("ENGLISH", flatInfoDTO.office_unit_id);
        flatInfoDTO.unit_name_bng = officeUnitsRepository.geText("BANGLA", flatInfoDTO.office_unit_id);
        flatInfoDTO.organogram_id = model.organogramId;
        flatInfoDTO.organogram_name_eng = organogramsRepository.getDesignation("ENGLISH", flatInfoDTO.organogram_id);
        flatInfoDTO.organogram_name_bng = organogramsRepository.getDesignation("BANGLA", flatInfoDTO.organogram_id);
        flatInfoDTO.username = recordsRepository.getById(flatInfoDTO.employee_id).employeeNumber;
        return flatInfoDTO;
    }

    public List<EmployeeAssignDTO> getEmployeeAssignDTOsBySearch(String sqlQuery, Long curTime) {
        Collection<EmpOfficeModel> ofcAndOfcOrgCollection =
                ConnectionAndStatementUtil
                        .getListOfT(sqlQuery, this::buildOfcAndOfcOrgmForSearch)
                        .stream()
                        .collect(Collectors.toMap(
                                empOfficeModel -> empOfficeModel.officeUnitOrganogramId,
                                e -> e,
                                (e1, e2) -> e1
                        )).values();
        return ofcAndOfcOrgCollection
                .stream()
                .map(this::buildFlatDTO)
                .collect(Collectors.toList());
    }

    public List<EmployeeAssignDTO> getEmployeeAssignDTOs(int officeId) {
        String sql = String.format(getOfcUnitAndOfcUnitOrgmByOfcId, officeId, CommonConstant.DEFAULT_OFFICE_ID, officeId);
        List<OfcAndOfcOrgm> ofcAndOfcOrgList = ConnectionAndStatementUtil.getListOfT(sql, this::buildOfcAndOfcOrgm);


        Map<Long, OfcAndOfcOrgm> mapByOrganogramId = ofcAndOfcOrgList.stream()
                .collect(Collectors.toMap(e -> e.organogram_id, e -> e));

        String organogramIds = ofcAndOfcOrgList
                .stream()
                .map(e -> String.valueOf(e.organogram_id))
                .distinct()
                .collect(Collectors.joining(","));
        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        String empSQL = String.format(getEmployeeOffices, CommonConstant.DEFAULT_OFFICE_ID, officeId, organogramIds, c.getTimeInMillis());
        List<EmployeeOffice> officeList = ConnectionAndStatementUtil.getListOfT(empSQL, this::buildEmployeeOffice);
        return officeList.stream()
                .map(employeeOffice -> buildEmployeeAssignDTO(officeId, employeeOffice))
                .collect(Collectors.toList());
    }

    private EmployeeAssignDTO buildEmployeeAssignDTO(int officeId, EmployeeOffice employeeOffice) {
        EmployeeAssignDTO dto = new EmployeeAssignDTO();
        Employee_recordsDTO employeeRecordsDTO = Employee_recordsRepository.getInstance().getById(employeeOffice.employee_record_id);
        Office_unitsDTO officeUnitsDTO = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(officeId);
        OfficeUnitOrganograms officeUnitOrganograms = OfficeUnitOrganogramsRepository.getInstance().getById(employeeOffice.office_unit_organogram_id);
        dto.office_unit_id = officeId;
        dto.unit_name_eng = officeUnitsDTO.unitNameEng;
        dto.unit_name_bng = officeUnitsDTO.unitNameBng;
        dto.username = employeeRecordsDTO.employeeNumber;
        dto.organogram_id = officeUnitOrganograms.id;
        dto.organogram_name_eng = officeUnitOrganograms.designation_eng;
        dto.organogram_name_bng = officeUnitOrganograms.designation_bng;
        dto.employee_id = employeeRecordsDTO.iD;
        dto.employee_name_eng = employeeRecordsDTO.nameEng;
        dto.employee_name_bng = employeeRecordsDTO.nameBng;
        dto.salaryGradeType = employeeOffice.salaryGradeType;
        return dto;
    }

    public ArrayList<Pair<Map<Object, Object>, ArrayList<EmployeeAssignDTO>>> getEmployeeRecordDTOs2(int officeId) {
        String sql = String.format(getOfcUnitAndOfcUnitOrgmByOfcId, officeId, CommonConstant.DEFAULT_OFFICE_ID, officeId);
        List<OfcAndOfcOrgm> ofcAndOfcOrgList = ConnectionAndStatementUtil.getListOfT(sql, this::buildOfcAndOfcOrgm);


        Map<Long, OfcAndOfcOrgm> mapByOrganogramId = ofcAndOfcOrgList.stream()
                .collect(Collectors.toMap(e -> e.organogram_id, e -> e));

        if (mapByOrganogramId.isEmpty()) {
            return new ArrayList<>();
        }

        String organogramIds = mapByOrganogramId.keySet()
                .stream()
                .map(String::valueOf)
                .collect(Collectors.joining(","));
        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        String empSQL = String.format(getEmployeeOffices, CommonConstant.DEFAULT_OFFICE_ID, officeId, organogramIds, c.getTimeInMillis());
        List<EmployeeOffice> officeList = ConnectionAndStatementUtil.getListOfT(empSQL, this::buildEmployeeOffice);
        Map<Long, EmployeeOffice> mapEmployeeOfficeByOrgId = officeList.stream()
                .collect(Collectors.groupingBy(e -> e.office_unit_organogram_id))
                .values()
                .stream()
                .map(employeeOffices -> employeeOffices.get(0))
                .collect(Collectors.toMap(e -> e.office_unit_organogram_id, e -> e));

        Map<Long, EmpRecordAndUser> mapEmpRecordAndUserByEmpRecordId;
        if (officeList.size() == 0) {
            mapEmpRecordAndUserByEmpRecordId = new HashMap<>();
        } else {
            String empRecordIds = officeList.stream()
                    .map(e -> String.valueOf(e.employee_record_id))
                    .distinct()
                    .collect(Collectors.joining(","));
            String empRecordAndUserSQL = String.format(getEmpRecordsAndUsers, empRecordIds, empRecordIds);
            List<EmpRecordAndUser> empRecordAndUserList = ConnectionAndStatementUtil.getListOfT(empRecordAndUserSQL, this::buildEmpRecordAndUser);

            mapEmpRecordAndUserByEmpRecordId = empRecordAndUserList.stream()
                    .collect(Collectors.toMap(e -> e.employee_id, e -> e, (e1, e2) -> e1));
        }
        ArrayList<Pair<Map<Object, Object>, ArrayList<EmployeeAssignDTO>>> modifiedData = new ArrayList<>();
        for (OfcAndOfcOrgm ofcAndOfcOrg : ofcAndOfcOrgList) {
            EmployeeOffice assignToEmpOffice = mapEmployeeOfficeByOrgId.get(ofcAndOfcOrg.organogram_id);
            if (assignToEmpOffice == null) {
                addForDefaultToModifiedData(ofcAndOfcOrg, modifiedData);
            } else {
                EmployeeOffice employeeOffice = null;
                if (mapEmpRecordAndUserByEmpRecordId.containsKey(assignToEmpOffice.employee_record_id)) {
                    employeeOffice = assignToEmpOffice;
                }
                if (employeeOffice == null) {
                    addForDefaultToModifiedData(ofcAndOfcOrg, modifiedData);
                } else {
                    addToModifiedData(ofcAndOfcOrg, employeeOffice, mapEmpRecordAndUserByEmpRecordId.get(employeeOffice.employee_record_id), modifiedData);
                }
            }
        }
        return modifiedData;
    }

    private void addForDefaultToModifiedData(OfcAndOfcOrgm ofcAndOfcOrgm, ArrayList<Pair<Map<Object, Object>, ArrayList<EmployeeAssignDTO>>> modifiedData) {
        EmployeeAssignDTO employeeAssignDTO = new EmployeeAssignDTO();
        Map<Object, Object> unit = new HashMap<>();
        setCommonData(ofcAndOfcOrgm, employeeAssignDTO, unit);
        employeeAssignDTO.last_office_date = 111;

        int indx = hasUnit(unit, modifiedData);
        if (indx >= 0) {
            modifiedData.get(indx).getValue().add(employeeAssignDTO);
        } else {
            ArrayList<Object> objects = new ArrayList<>();
            objects.add(employeeAssignDTO);
            Pair pair = new Pair(unit, objects);
            modifiedData.add(pair);
        }
    }

    private void setCommonData(OfcAndOfcOrgm ofcAndOfcOrgm, EmployeeAssignDTO employeeAssignDTO, Map<Object, Object> unit) {
        employeeAssignDTO.office_unit_id = ofcAndOfcOrgm.office_unit_id;
        employeeAssignDTO.unit_name_eng = ofcAndOfcOrgm.unit_name_eng;
        employeeAssignDTO.unit_name_bng = ofcAndOfcOrgm.unit_name_bng;
        unit.put("office_unit_id", employeeAssignDTO.office_unit_id);
        unit.put("unit_name_eng", employeeAssignDTO.unit_name_eng);
        unit.put("unit_name_bng", employeeAssignDTO.unit_name_bng);
        employeeAssignDTO.organogram_id = ofcAndOfcOrgm.organogram_id;
        employeeAssignDTO.organogram_name_eng = ofcAndOfcOrgm.designation_eng;
        employeeAssignDTO.organogram_name_bng = ofcAndOfcOrgm.designation_bng;
        employeeAssignDTO.designation_level = ofcAndOfcOrgm.designation_level;
        employeeAssignDTO.designation_sequence = ofcAndOfcOrgm.designation_sequence;
        employeeAssignDTO.officeId = ofcAndOfcOrgm.office_unit_id;
        employeeAssignDTO.briefDescriptionEng = ofcAndOfcOrgm.brief_description_eng;
        employeeAssignDTO.briefDescriptionBng = ofcAndOfcOrgm.brief_description_bng;
    }

    private void addToModifiedData(OfcAndOfcOrgm ofcAndOfcOrgm, EmployeeOffice employeeOffice, EmpRecordAndUser empRecordAndUser
            , ArrayList<Pair<Map<Object, Object>, ArrayList<EmployeeAssignDTO>>> modifiedData) {

        EmployeeAssignDTO employeeAssignDTO = new EmployeeAssignDTO();
        Map<Object, Object> unit = new HashMap<>();
        setCommonData(ofcAndOfcOrgm, employeeAssignDTO, unit);

        employeeAssignDTO.employee_id = empRecordAndUser.employee_id;
        employeeAssignDTO.employee_name_eng = empRecordAndUser.employee_name_eng;
        employeeAssignDTO.employee_name_bng = empRecordAndUser.employee_name_bng;
        employeeAssignDTO.username = empRecordAndUser.username;

        employeeAssignDTO.employee_office_id = employeeOffice.employee_office_id;
        employeeAssignDTO.joining_date = employeeOffice.joining_date;
        employeeAssignDTO.last_office_date = employeeOffice.last_office_date;
        employeeAssignDTO.inChargeLabel = employeeOffice.inChargeLabel;
        employeeAssignDTO.gradeTypeLevel = employeeOffice.gradeTypeLevel;
        employeeAssignDTO.salaryGradeType = employeeOffice.salaryGradeType;

        logger.debug("OfficeId : " + employeeAssignDTO.officeId + ", Last Office Date: " + employeeAssignDTO.last_office_date);

        int indx = hasUnit(unit, modifiedData);
        if (indx >= 0) {
            modifiedData.get(indx).getValue().add(employeeAssignDTO);
        } else {
            ArrayList<Object> objects = new ArrayList<>();
            objects.add(employeeAssignDTO);
            Pair pair = new Pair(unit, objects);
            modifiedData.add(pair);
        }
    }

    public ArrayList<Pair<Map<Object, Object>, ArrayList<EmployeeAssignDTO>>> getEmployeeRecordDTOs(int officeId) {

        Connection connection = null;
        ResultSet rs;
        Statement stmt = null;

        String unitOrganogramSql = "select \n" +
                "\toffice_units.id as office_unit_id,\n" +
                "\toffice_units.unit_name_bng,\n" +
                "\toffice_units.unit_name_eng,\n" +
                "\toffice_unit_organograms.id as organogram_id,\n" +
                "\toffice_unit_organograms.designation_bng,\n" +
                "\toffice_unit_organograms.designation_eng,\n" +
                "\toffice_unit_organograms.designation_level,\n" +
                "\toffice_unit_organograms.designation_sequence\n" +
                "\n" +
                "from office_units, office_unit_organograms \n" +
                "\n" +
                "where office_units.id = office_unit_organograms.office_unit_id \n" +
                "\tand office_units.id = %d \n" +
                "\tand office_units.office_id = %d \n" +
                "\tand office_units.isDeleted = 0;";
        unitOrganogramSql = String.format(unitOrganogramSql, officeId, CommonConstant.DEFAULT_OFFICE_ID);

        ArrayList<Object[]> unitAndOrganogram = new ArrayList<>();
        try {
            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();
            rs = stmt.executeQuery(unitOrganogramSql);

            while (rs.next()) {
                unitAndOrganogram.add(new Object[]{
                        rs.getInt("office_unit_id"),
                        rs.getString("unit_name_bng"),
                        rs.getString("unit_name_eng"),
                        rs.getInt("organogram_id"),
                        rs.getString("designation_bng"),
                        rs.getString("designation_eng"),
                        rs.getInt("designation_level"),
                        rs.getInt("designation_sequence")
                });
            }
        } catch (SQLException | IllegalAccessException | ClassNotFoundException | InstantiationException e) {
            e.printStackTrace();
        }

        // endregion Organogram and Unit

        // region Employee Data
        ArrayList<Pair<Map<Object, Object>, ArrayList<EmployeeAssignDTO>>> modifiedData = new ArrayList<>();
        try {
            for (int i = 0; i < unitAndOrganogram.size(); i++) {
                int office_unit_id = (int) unitAndOrganogram.get(i)[0];
                String unit_name_bng = (String) unitAndOrganogram.get(i)[1];
                String unit_name_eng = (String) unitAndOrganogram.get(i)[2];
                int organogram_id = (int) unitAndOrganogram.get(i)[3];
                String designation_bng = (String) unitAndOrganogram.get(i)[4];
                String designation_eng = (String) unitAndOrganogram.get(i)[5];
                int designation_level = (int) unitAndOrganogram.get(i)[6];
                int designation_sequence = (int) unitAndOrganogram.get(i)[7];

                String employeeSql = "select\n" +
                        "\n" +
                        "employee_records.id as employee_id,\n" +
                        "employee_records.name_eng as employee_name_eng,\n" +
                        "employee_records.name_bng as employee_name_bng,\n" +
                        "\n" +
                        "users.username," +
                        "\n" +
                        "employee_offices.id as employee_office_id,\n" +
                        "employee_offices.joining_date as joining_date,\n" +
                        "employee_offices.last_office_date as last_office_date,\n" +
                        "employee_offices.status as status\n" +
                        "\n" +
                        "from employee_offices, employee_records, users \n" +
                        "where employee_records.id = employee_offices.employee_record_id\n" +
                        "and employee_records.id = users.employee_record_id\n" +
                        "and employee_offices.office_id = %d \n" +
                        "and employee_offices.office_unit_id = %d \n" +
                        "and employee_offices.office_unit_organogram_id = %d\n" +
                        "and (employee_offices.last_office_date is null or employee_offices.last_office_date = 0)\n" +
                        "and employee_offices.isDeleted = 0 \n" +
                        "and employee_offices.status = 1 \n" +
                        "and employee_records.isDeleted = 0;";

                employeeSql = String.format(employeeSql, CommonConstant.DEFAULT_OFFICE_ID, office_unit_id, organogram_id);

                try {
                    rs = stmt.executeQuery(employeeSql);
                    if (!rs.next()) {
                        EmployeeAssignDTO employeeAssignDTO = new EmployeeAssignDTO();

                        Map<Object, Object> unit = new HashMap<>();

                        employeeAssignDTO.office_unit_id = office_unit_id;
                        employeeAssignDTO.unit_name_eng = unit_name_bng;
                        employeeAssignDTO.unit_name_bng = unit_name_eng;
                        unit.put("office_unit_id", employeeAssignDTO.office_unit_id);
                        unit.put("unit_name_eng", employeeAssignDTO.unit_name_eng);
                        unit.put("unit_name_bng", employeeAssignDTO.unit_name_bng);

                        employeeAssignDTO.organogram_id = organogram_id;
                        employeeAssignDTO.organogram_name_eng = designation_eng;
                        employeeAssignDTO.organogram_name_bng = designation_bng;
                        employeeAssignDTO.designation_level = designation_level;
                        employeeAssignDTO.designation_sequence = designation_sequence;
                        employeeAssignDTO.officeId = officeId;
                        employeeAssignDTO.last_office_date = 111;

                        int indx = hasUnit(unit, modifiedData);
                        if (indx >= 0) {
                            modifiedData.get(indx).getValue().add(employeeAssignDTO);
                        } else {
                            ArrayList<Object> objects = new ArrayList<>();
                            objects.add(employeeAssignDTO);
                            Pair pair = new Pair(unit, objects);
                            modifiedData.add(pair);
                        }
                    } else
                        do {
                            EmployeeAssignDTO employeeAssignDTO = new EmployeeAssignDTO();

                            Map<Object, Object> unit = new HashMap<>();

                            employeeAssignDTO.office_unit_id = office_unit_id;
                            employeeAssignDTO.unit_name_eng = unit_name_bng;
                            employeeAssignDTO.unit_name_bng = unit_name_eng;
                            unit.put("office_unit_id", employeeAssignDTO.office_unit_id);
                            unit.put("unit_name_eng", employeeAssignDTO.unit_name_eng);
                            unit.put("unit_name_bng", employeeAssignDTO.unit_name_bng);

                            employeeAssignDTO.organogram_id = organogram_id;
                            employeeAssignDTO.organogram_name_eng = designation_eng;
                            employeeAssignDTO.organogram_name_bng = designation_bng;
                            employeeAssignDTO.designation_level = designation_level;
                            employeeAssignDTO.designation_sequence = designation_sequence;

                            employeeAssignDTO.employee_id = rs.getLong("employee_id");
                            employeeAssignDTO.employee_name_eng = rs.getString("employee_name_eng");
                            employeeAssignDTO.employee_name_bng = rs.getString("employee_name_bng");
                            employeeAssignDTO.username = rs.getString("username");

                            employeeAssignDTO.employee_office_id = rs.getLong("employee_office_id");
                            employeeAssignDTO.joining_date = rs.getLong("joining_date");
                            employeeAssignDTO.last_office_date = rs.getLong("last_office_date");

                            employeeAssignDTO.officeId = officeId;

                            logger.debug("Last Office Date: " + employeeAssignDTO.officeId + " " + employeeAssignDTO.last_office_date);

                            int indx = hasUnit(unit, modifiedData);
                            if (indx >= 0) {
                                modifiedData.get(indx).getValue().add(employeeAssignDTO);
                            } else {
                                ArrayList<Object> objects = new ArrayList<>();
                                objects.add(employeeAssignDTO);
                                Pair pair = new Pair(unit, objects);
                                modifiedData.add(pair);
                            }
                        }
                        while (rs.next());
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception ignored) {

        } finally {
            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (SQLException | IllegalAccessException | ClassNotFoundException | InstantiationException e) {
                e.printStackTrace();
            }
        }

        // endregion Employee Data

        return modifiedData;
    }

    private String getDataSqlWithOffice(String userName, String phone, String office) {
        String where = " where employee_records.isDeleted = 0 ";
        where += " and employee_offices.isDeleted = 0 and employee_offices.status = 1 and employee_offices.is_default_role = 1 ";
        if (office != null && !office.equalsIgnoreCase("")) {

            where += " and employee_offices.office_unit_id in ( " + office + ")";
        }
        if (userName != null && userName.length() > 0) {
            where += " and employee_records.employee_number = " + userName + " ";
        }
        if (checkPhone(phone)) {
            where += " and (employee_records.personal_mobile = " + phone + " OR employee_records.alternative_mobile = " + phone + ") ";
        }

        String dataSql = "select \n" +
                "employee_records.id,\n" +
                "employee_records.employee_number,\n" +
                "employee_records.name_eng,\n" +
                "employee_records.name_bng,\n" +
                "employee_offices.office_unit_id,\r\n" +
                "    employee_offices.designation_en,\r\n" +
                "    employee_offices.designation_bn,\r\n" +
                "    employee_offices.unit_name_en,\r\n" +
                "    employee_offices.unit_name_bn,\r\n" +
                "    employee_offices.grade_type_level,\r\n" +
                "    employee_offices.job_grade_type_cat,\n" +
                "    employee_offices.salary_grade_type,\n" +
                "    employee_offices.id\n" +
                "from employee_records \n";

        dataSql += " left join employee_offices on employee_offices.employee_record_id = employee_records.id ";

        dataSql += where;
        logger.debug("dataSql = " + dataSql);
        return dataSql;
    }


    public String getEmployeeDetailsByUserNameOrOffice(String userName, String phone, Set<Long> lOfficeUnitIds) throws ClassNotFoundException {
        if (userName != null) {
            userName = StringUtils.convertToEngNumber(userName);
        }

        if (phone != null) {
            phone = "88" + StringUtils.convertToEngNumber(phone);
            if (phone.equalsIgnoreCase("88")) {
                phone = "";
            }
        }

        String offices = null;
        if (lOfficeUnitIds != null && !lOfficeUnitIds.isEmpty()) {
            offices = lOfficeUnitIds.stream().map(String::valueOf)
                    .collect(Collectors.joining(","));
        }

        Connection connection = null;
        ResultSet rs;
        PreparedStatement ps;
        ArrayList<Map<Object, Object>> result = new ArrayList<>();

        String sql = getDataSqlWithOffice(userName, phone, offices);

        logger.debug("getEmployeeDetailsByUserNameOrNid Sql - " + sql);

        try {
            connection = DBMR.getInstance().getConnection();
            ps = connection.prepareStatement(sql);

            logger.debug(ps);
            rs = ps.executeQuery();
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            while (rs.next()) {
                Map<Object, Object> data = new HashMap<>();
                data.put("employee_office_id", rs.getLong("employee_offices.id"));
                data.put("employee_number", rs.getString("employee_records.employee_number"));
                data.put("name_eng", rs.getString("name_eng"));
                data.put("name_bng", rs.getString("name_bng"));
                data.put("office_unit_id", rs.getString("employee_offices.office_unit_id"));
                data.put("unitNameEng", rs.getString("employee_offices.unit_name_en"));
                data.put("unitNameBng", rs.getString("employee_offices.unit_name_bn"));
                data.put("designationEng", rs.getString("employee_offices.designation_en"));
                data.put("designationBng", rs.getString("employee_offices.designation_bn"));
                data.put("gradeTypeLevel", rs.getString("employee_offices.grade_type_level"));
                data.put("job_grade_type_cat", rs.getString("employee_offices.job_grade_type_cat"));
                data.put("salary_grade_type", rs.getString("employee_offices.salary_grade_type"));
                int grade = rs.getInt("employee_offices.job_grade_type_cat");
                data.put("gradeEng", CatRepository.getInstance().getText("English", "job_grade", grade));
                data.put("gradeBng", CatRepository.getInstance().getText("Bangla", "job_grade", grade));


                result.add(data);

            }
        } catch (SQLException | IllegalAccessException | InstantiationException e) {
            e.printStackTrace();
        } finally {
            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (SQLException | IllegalAccessException | ClassNotFoundException | InstantiationException e) {
                e.printStackTrace();
            }
        }


        return new Gson().toJson(result);
    }

    public String getEmployeeDetailsByUserNameOrNid(String userName, String nid, String phone) throws ClassNotFoundException {
        if (userName != null) {
            userName = StringUtils.convertToEngNumber(userName);
        }
        if (nid != null) {
            nid = StringUtils.convertToEngNumber(nid);
        }
        if (phone != null) {
            phone = "88" + StringUtils.convertToEngNumber(phone);
            if (phone.equalsIgnoreCase("88")) {
                phone = "";
            }
        }
        Connection connection = null;
        ResultSet rs;
        PreparedStatement ps;
        ArrayList<Map<Object, Object>> result = new ArrayList<>();

        String sql = getDataSql(userName, nid, phone);

        logger.debug("getEmployeeDetailsByUserNameOrNid Sql - " + sql);

        try {
            connection = DBMR.getInstance().getConnection();
            ps = connection.prepareStatement(sql);
            if (userName != null && userName.length() > 0 && checkNid(nid) && checkPhone(phone)) {
                ps.setString(1, userName);
                ps.setString(2, nid);
                ps.setString(3, phone);
                ps.setString(4, phone);
            } else if (userName != null && userName.length() > 0 && checkPhone(phone)) {
                ps.setString(1, userName);
                ps.setString(2, phone);
                ps.setString(3, phone);
            } else if (userName != null && userName.length() > 0) {
                ps.setString(1, userName);
            } else if (checkNid(nid)) {
                ps.setString(1, nid);
            } else if (checkPhone(phone)) {
                ps.setString(1, phone);
                ps.setString(2, phone);
            }
            logger.debug(ps);
            rs = ps.executeQuery();
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            while (rs.next()) {
                Map<Object, Object> data = new HashMap<>();
                data.put("id", rs.getLong("ID"));
                data.put("username", rs.getString("username"));
                data.put("nameEng", rs.getString("name_eng"));
                data.put("nameBng", rs.getString("name_bng"));
                data.put("personalEmail", rs.getString("personal_email"));
                data.put("personalMobile", rs.getString("personal_mobile"));
                result.add(data);
            }
        } catch (SQLException | IllegalAccessException | InstantiationException e) {
            e.printStackTrace();
        } finally {
            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (SQLException | IllegalAccessException | ClassNotFoundException | InstantiationException e) {
                e.printStackTrace();
            }
        }
        Employee_recordsDTO employeeRecordsDTO = Employee_recordsRepository.getInstance().getByUserNameOrPhone(userName, phone);
        if (employeeRecordsDTO != null) {
            long employeeId = employeeRecordsDTO.iD;
            List<EmployeeOfficeDTO> employee_officesDTOList = EmployeeOfficesDAO.getInstance().getActiveOfficesByEmployeeRecordId(employeeId);
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

            for (EmployeeOfficeDTO employeeOfficeDTO : employee_officesDTOList) {
                OfficeUnitOrganograms ouo = OfficeUnitOrganogramsRepository.getInstance().getById(employeeOfficeDTO.officeUnitOrganogramId);
                if(ouo.isVirtual){
                    continue;
                }
                Map<Object, Object> data = new HashMap<>();
                data.put("id", employeeRecordsDTO.iD);
                data.put("employee_office_id", employeeOfficeDTO.iD);
                data.put("username", employeeRecordsDTO.employeeNumber);
                data.put("nameEng", employeeRecordsDTO.nameEng);
                data.put("nameBng", employeeRecordsDTO.nameBng);
                data.put("personalEmail", employeeRecordsDTO.personalEml);
                data.put("personalMobile", employeeRecordsDTO.personalMobile);
                data.put("designationEng", employeeOfficeDTO.designationEn);
                data.put("designationBng", employeeOfficeDTO.designationBn);

                data.put("designation_id", employeeOfficeDTO.officeUnitOrganogramId);
                data.put("designation", employeeOfficeDTO.designation);
                data.put("officeNameEng", employeeOfficeDTO.unitNameEn);
                data.put("officeNameBng", employeeOfficeDTO.unitNameBn);
                
                data.put("salaryGradeTypeEng", CatRepository.getInstance().getText("English", "job_grade", employeeOfficeDTO.salaryGradeType));
                data.put("salaryGradeTypeBng", CatRepository.getInstance().getText("Bangla", "job_grade", employeeOfficeDTO.salaryGradeType));
                Office_unitsDTO officeUnitsDTO = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(employeeOfficeDTO.officeUnitId);
                if (officeUnitsDTO != null) {
                    data.put("unitNameEng", officeUnitsDTO.unitNameEng);
                    data.put("unitNameBng", officeUnitsDTO.unitNameBng);
                } else {
                    data.put("unitNameEng", "");
                    data.put("unitNameBng", "");
                }

                data.put("inChargeLabelEng", InChargeLevelEnum.getByValue(employeeOfficeDTO.inchargeLabel).getEngText());
                data.put("inChargeLabelBng", InChargeLevelEnum.getByValue(employeeOfficeDTO.inchargeLabel).getBngText());

                OfficeUnitOrganograms officeUnitOrganograms = OfficeUnitOrganogramsRepository.getInstance().getById(employeeOfficeDTO.officeUnitOrganogramId);
                data.put("gradeEng", CatRepository.getInstance().getText("English", "job_grade", officeUnitOrganograms.job_grade_type_cat));
                data.put("gradeBng", CatRepository.getInstance().getText("Bangla", "job_grade", officeUnitOrganograms.job_grade_type_cat));


                data.put("gradeTypeLevel", employeeOfficeDTO.gradeTypeLevel);
                data.put("gradeLevelEng", Grade_wise_pay_scaleRepository.getInstance().getText("English", employeeOfficeDTO.gradeTypeLevel));
                data.put("gradeLevelBng", Grade_wise_pay_scaleRepository.getInstance().getText("Bangla", employeeOfficeDTO.gradeTypeLevel));

                String joiningDate = sdf.format(new Date(employeeOfficeDTO.joiningDate));
                data.put("joiningDateLong", employeeOfficeDTO.joiningDate);
                data.put("joiningDateEng", joiningDate);
                data.put("joiningDateBng", StringUtils.convertToBanNumber(joiningDate));
                data.put("lastOfficeDate", employeeOfficeDTO.lastOfficeDate);
                data.put("attachmentFlag", false);
                data.put("attachmentId", -1);
                result.add(data);
            }
        }
        return new Gson().toJson(result);
    }

    public boolean addLastOfficeDate(int employee_office_id, long last_office_date) {
        return (Boolean) ConnectionAndStatementUtil.getWriteStatement(st -> {
            long currentTime = System.currentTimeMillis();
            String sql = String.format(updateLastOfficeDateQuery, last_office_date, 0, currentTime, employee_office_id);
            logger.debug(sql);
            try {
                st.executeUpdate(sql);
                recordUpdateTime("employee_offices", currentTime);
            } catch (SQLException exception) {
                exception.printStackTrace();
                return false;
            }
            return true;
        });
    }

    public void addLastOfficeDateByOrganogramId(long organogramId, long last_office_date,
                                                long lastModificationTime) throws Exception {
        String sql = String.format(updateLastOfficeDateByOrganogramQuery, last_office_date, 0,
                lastModificationTime, organogramId);
        checkConnectionAndExecute(sql, lastModificationTime);
    }

    public void addLastOfficeDateByOrganogramIdList(List<Long> list, long last_office_date,
                                                    long lastModificationTime) throws Exception {
        Optional<String> ids = StringUtils.getCommaSeparatedStringFromLongList(list);
        if (ids.isPresent()) {
            String sql = String.format(updateLastOfficeDateByOrgIdList, last_office_date, 0, lastModificationTime, ids.get());
            checkConnectionAndExecute(sql, lastModificationTime);
        }
    }

    private void checkConnectionAndExecute(String sql, long lastModificationTime) throws Exception {
        if (CommonDAOService.CONNECTION_THREAD_LOCAL.get() != null) {
            executeSQL(sql, lastModificationTime);
            Utils.addToRepoList("employee_offices");
        } else {
            Utils.handleTransaction(() -> {
                executeSQL(sql, lastModificationTime);
                Utils.runExactTimeRepoMethod("employee_offices", lastModificationTime);
            });
        }
    }

    private void executeSQL(String sql, long lastModificationTime) throws Exception {
        Utils.wrapWithAtomicReference(ar ->
                ConnectionAndStatementUtil.getWriteStatement(st -> {
                    logger.debug(sql);
                    try {
                        st.executeUpdate(sql);
                        recordUpdateTime(CommonDAOService.CONNECTION_THREAD_LOCAL.get(), "employee_offices", lastModificationTime);
                    } catch (SQLException exception) {
                        exception.printStackTrace();
                        ar.set(exception);
                    }
                }, CommonDAOService.CONNECTION_THREAD_LOCAL.get())
        );
    }

    public void addEmployeeIntoNewOffice(long employee_record_id, long office_unit_id, long office_unit_organogram_id,
                                         String designation, int designation_level, int designation_sequence, String incharge_label,
                                         int isDefault, long assignDate, long gradeTypLevel,
                                         long lastModificationTime, long userId, long salaryGradeType) throws Exception {

        Utils.wrapWithAtomicReference(ar ->
                ConnectionAndStatementUtil.getWritePrepareStatement(ps -> {
                    try {
                        long id = DBMW.getInstance().getNextSequenceId("employee_offices");
                        int index = 0;
                        ps.setObject(++index, id);
                        ps.setObject(++index, employee_record_id);
                        ps.setObject(++index, 0);
                        ps.setObject(++index, 2294);
                        ps.setObject(++index, office_unit_id);
                        ps.setObject(++index, office_unit_organogram_id);
                        ps.setObject(++index, designation);
                        ps.setObject(++index, designation_level);
                        ps.setObject(++index, designation_sequence);
                        ps.setObject(++index, true);
                        ps.setObject(++index, false);
                        ps.setObject(++index, 0);
                        ps.setObject(++index, incharge_label);
                        ps.setObject(++index, 0);
                        ps.setObject(++index, assignDate);
                        ps.setObject(++index, SessionConstants.MIN_DATE);
                        ps.setObject(++index, true);
                        ps.setObject(++index, assignDate);
                        OfficeUnitOrganograms officeUnitOrganograms = OfficeUnitOrganogramsRepository.getInstance().getById(office_unit_organogram_id);
                        ps.setObject(++index, 0);
                        ps.setObject(++index, officeUnitOrganograms.job_grade_type_cat);
                        ps.setObject(++index, gradeTypLevel);
                        ps.setObject(++index, officeUnitOrganograms.layer1);
                        ps.setObject(++index, officeUnitOrganograms.layer2);
                        ps.setObject(++index, lastModificationTime);
                        ps.setObject(++index, lastModificationTime);
                        ps.setObject(++index, userId);
                        ps.setObject(++index, userId);
                        ps.setObject(++index, 0);
                        ps.setObject(++index, lastModificationTime);
                        ps.setObject(++index, false);
                        ps.setObject(++index, isDefault);
                        ps.setObject(++index, officeUnitOrganograms.designation_eng);
                        ps.setObject(++index, officeUnitOrganograms.designation_bng);
                        Office_unitsDTO officeUnitsDTO = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(office_unit_id);
                        ps.setObject(++index, officeUnitsDTO.unitNameBng);
                        ps.setObject(++index, officeUnitsDTO.unitNameEng);
                        ps.setObject(++index, officeUnitsDTO.unitNameBng);
                        ps.setObject(++index, officeUnitsDTO.unitNameEng);
                        ps.setObject(++index, salaryGradeType);
                        logger.debug(ps);
                        ps.execute();
                        recordUpdateTime(CommonDAOService.CONNECTION_THREAD_LOCAL.get(), "employee_offices", lastModificationTime);
                        Utils.addToRepoList("employee_offices");

                        new UserDAO().updateLastModificationTimeByEmpId(employee_record_id, lastModificationTime, CommonDAOService.CONNECTION_THREAD_LOCAL.get());
                        recordUpdateTime(CommonDAOService.CONNECTION_THREAD_LOCAL.get(), "users", lastModificationTime);
                        Utils.addToRepoList("users");

                        //This is for inventory. This needs to be called whenever an employee is assigned to a designation. Please shift this portion of code properly if assignment code is shifted
                        PiUniqueItemAssignmentDAO.getInstance().handleInventoryAfterEmployeeAssign(employee_record_id, office_unit_organogram_id, assignDate);

                    } catch (Exception ex) {
                        logger.error(ex);
                        ex.printStackTrace();
                        ar.set(ex);
                    }
                }, CommonDAOService.CONNECTION_THREAD_LOCAL.get(), addEmployeeOfficeQuery));
    }

    private int hasUnit(Map<Object, Object> unit, ArrayList<Pair<Map<Object, Object>, ArrayList<EmployeeAssignDTO>>> modifiedData) {
        for (int i = 0; i < modifiedData.size(); i++) {
            Map<Object, Object> key = modifiedData.get(i).getKey();
            if (key.equals(unit)) {
                return i;
            }
        }
        return -1;
    }


    private String getDataSql(String userName, String nid, String phone) {
        String where = " where users.isDeleted = 0 and employee_records.isDeleted = 0 ";
        if (userName != null && userName.length() > 0 && checkNid(nid) && checkPhone(phone))
            where = "where users.isDeleted = 0 and employee_records.isDeleted = 0 and users.username = ? OR employee_records.nid = ? OR (employee_records.personal_mobile = ? OR employee_records.alternative_mobile = ?)\n";
        else if (userName != null && userName.length() > 0 && checkPhone(phone)) {
            where = "where users.isDeleted = 0 and employee_records.isDeleted = 0 and users.username = ? OR (employee_records.personal_mobile = ? OR employee_records.alternative_mobile = ?)\n";
        } else if (userName != null && userName.length() > 0)
            where = "where users.isDeleted = 0 and employee_records.isDeleted = 0 and users.username = ?\n";
        else if (checkNid(nid))
            where = "where users.isDeleted = 0 and employee_records.isDeleted = 0 and employee_records.nid = ?\n";
        else if (checkPhone(phone))
            where = "where users.isDeleted = 0 and employee_records.isDeleted = 0 and (employee_records.personal_mobile = ? OR employee_records.alternative_mobile = ?)\n";

        String dataSql = "select \n" +
                "employee_records.id,\n" +
                "users.username,\n" +
                "employee_records.name_eng,\n" +
                "employee_records.name_bng,\n" +
                "employee_records.personal_email,\n" +
                "employee_records.personal_mobile\n" +
                "from employee_records left join users on employee_records.id = users.employee_record_id\n" + where;
        logger.debug("dataSql = " + dataSql);
        return dataSql;
    }


    private boolean checkNid(String nid) {
        if (nid == null || nid.trim().length() == 0) return false;
        for (char c : nid.toCharArray()) {
            if (!(c >= '0' && c <= '9')) return false;
        }
        return true;
    }

    private boolean checkPhone(String phone) {
        if (phone == null || phone.trim().length() == 0)
            return false;
        for (char c : phone.toCharArray()) {
            if (!(c >= '0' && c <= '9')) return false;
        }
        return true;
    }

    public ArrayList<EmployeeOfficePermissionDTO> getEmployeePermittedAppByDesignationId(int designationId) throws SQLException {

        ArrayList<EmployeeOfficePermissionDTO> employeeOfficePermissionDTOS = new ArrayList<>();
        Connection cn = null;
        PreparedStatement ps;
        PreparedStatement par = null;

        String sql = "SELECT\n" +
                "designation_menu_roles.menu_id AS menu_id,\n" +
                "application_registration.id AS app_id,\n" +
                "application_registration.application_name_bng AS app_name_bng,\n" +
                "application_registration.application_name AS app_name_eng\n" +
                "\n" +
                "FROM designation_menu_roles\n" +
                "JOIN application_registration ON designation_menu_roles.menu_id = application_registration.id\n" +
                "\n" +
                "WHERE application_registration.is_approved = 1 \n" +
                "AND application_registration.is_published = 1 \n" +
                "AND designation_menu_roles.designation_id = ?";

        ResultSet rs;
        try {
            cn = DBMR.getInstance().getConnection();
            ps = cn.prepareStatement(sql);
            ps.setLong(1, designationId);

            rs = ps.executeQuery();
            while (rs.next()) {
                EmployeeOfficePermissionDTO employeeOfficePermissionDTO = getData(rs);
                employeeOfficePermissionDTOS.add(employeeOfficePermissionDTO);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cn != null) {
                try {
                    DBMR.getInstance().freeConnection(cn);
                } catch (ClassNotFoundException | IllegalAccessException | InstantiationException e) {
                    e.printStackTrace();
                }
            }
        }

        //executeQuery(designationId, employeeOfficePermissionDTOS, cn, ps, par, sql);
        return employeeOfficePermissionDTOS;
    }

    public ArrayList<EmployeeOfficePermissionDTO> getEmployeeUnpermittedAppByDesignationId(int designationId) throws SQLException {

        ArrayList<EmployeeOfficePermissionDTO> employeeOfficePermissionDTOS = new ArrayList<>();
        Connection cn = null;
        PreparedStatement ps = null;
        PreparedStatement par = null;
        ResultSet rs;

        String sql = "SELECT\n" +
                "id AS menu_id,\n" +
                "application_name_bng AS app_name_bng,\n" +
                "application_name AS app_name_eng\n" +
                "FROM application_registration ar\n" +
                "WHERE ar.is_approved = 1 AND ar.is_published = 1 AND id NOT IN(\n" +
                "SELECT d.menu_id FROM designation_menu_roles d WHERE d.menu_id = ar.id AND d.designation_id =?\n" +
                ")";

        //executeQuery(designationId, employeeOfficePermissionDTOS, cn, ps, par, sql);

        //ResultSet rs;
        try {
            cn = DBMR.getInstance().getConnection();
            ps = cn.prepareStatement(sql);
            ps.setLong(1, designationId);

            rs = ps.executeQuery();
            while (rs.next()) {
                EmployeeOfficePermissionDTO employeeOfficePermissionDTO = getData(rs);
                employeeOfficePermissionDTOS.add(employeeOfficePermissionDTO);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cn != null) {
                try {
                    DBMR.getInstance().freeConnection(cn);
                } catch (ClassNotFoundException | IllegalAccessException | InstantiationException e) {
                    e.printStackTrace();
                }
            }
        }
        return employeeOfficePermissionDTOS;
    }

    public boolean employeeAppPermission(int designationId, int[] apps) throws SQLException {

        String sql = "INSERT INTO designation_menu_roles (menu_id, designation_id, type, origin_org_id, has_exceptions) " + "VALUES(?, ?, ?, ?, ?)";

        Connection connection = null;
        PreparedStatement preparedStatement = null;

        for (int i = 0; i < apps.length; i++) {
            int menuId = apps[i];
            try {
                connection = DBMW.getInstance().getConnection();
                preparedStatement = connection.prepareStatement(sql);
                preparedStatement.setInt(1, menuId);
                preparedStatement.setInt(2, designationId);
                preparedStatement.setInt(3, 0);
                preparedStatement.setInt(4, 0);
                preparedStatement.setInt(5, 1);

                preparedStatement.execute();

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    try {
                        DBMW.getInstance().freeConnection(connection);
                    } catch (ClassNotFoundException | IllegalAccessException | InstantiationException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        return true;
    }

    private EmployeeOfficePermissionDTO getData(ResultSet resultSet) throws SQLException {
        int menuId = resultSet.getInt("menu_id");
        String appNameBng = resultSet.getString("app_name_bng");
        String appNameEng = resultSet.getString("app_name_eng");

        EmployeeOfficePermissionDTO employeeOfficePermissionDTO = new EmployeeOfficePermissionDTO();
        employeeOfficePermissionDTO.menuId = (menuId);
        employeeOfficePermissionDTO.applicationNameBng = (appNameBng);
        employeeOfficePermissionDTO.applicationNameEng = (appNameEng);
        return employeeOfficePermissionDTO;
    }
}
