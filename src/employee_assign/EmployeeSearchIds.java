package employee_assign;

public class EmployeeSearchIds {
    public long employeeRecordId;
    public long officeUnitId;
    public long organogramId;

    public EmployeeSearchIds(long employeeRecordId, long officeUnitId, long organogramId) {
        this.employeeRecordId = employeeRecordId;
        this.officeUnitId = officeUnitId;
        this.organogramId = organogramId;
    }
}
