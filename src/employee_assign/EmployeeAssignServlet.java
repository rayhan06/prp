package employee_assign;

import asset_model.AssetAssigneeDAO;
import com.google.gson.Gson;
import common.ApiResponse;
import common.CacheUpdateModel;
import common.CommonDAOService;
import common.NameDTO;
import employee_attachment.Employee_attachmentDAO;
import employee_attachment.Employee_attachmentDTO;
import employee_management.EmployeeOfficerCat;
import employee_office_report.InChargeLevelEnum;
import employee_offices.EmployeeOfficeDTO;
import employee_offices.EmployeeOfficeRepository;
import employee_offices.EmployeeOfficesDAO;
import employee_records.Employee_recordsDAO;
import employee_records.Employee_recordsDTO;
import employee_records.Employee_recordsRepository;
import employee_seniority.Employee_seniorityDAO;
import leave_reliever_mapping.Leave_reliever_mappingDTO;
import leave_reliever_mapping.Leave_reliever_mappingRepository;
import office_unit_organogram.Office_unit_organogramDAO;
import office_unit_organograms.Committee_post_mapDTO;
import office_unit_organograms.Committee_post_mapRepository;
import office_unit_organograms.OfficeUnitOrganograms;
import office_unit_organograms.OfficeUnitOrganogramsRepository;
import org.apache.log4j.Logger;
import other_office.Other_officeRepository;
import pb.Utils;
import pi_unique_item.PiActionTypeEnum;
import pi_unique_item.PiStageEnum;
import pi_unique_item.PiUniqueItemAssignmentDAO;
import pi_unique_item.PiUniqueItemAssignmentDTO;
import promotion_history.Promotion_historyDAO;
import promotion_history.Promotion_historyDTO;
import sessionmanager.SessionConstants;
import test_lib.util.Pair;
import user.UserDAO;
import user.UserDTO;
import util.BooleanWrapper;
import util.HttpRequestUtils;
import util.StringUtils;
import vm_requisition.Vm_requisitionDAO;
import vm_requisition.Vm_requisitionDTO;
import workflow.WorkflowController;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

@SuppressWarnings({"unchecked", "Duplicates", "rawtypes"})
@WebServlet("/EmployeeAssignServlet")
@MultipartConfig
public class EmployeeAssignServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;
    private static final Logger logger = Logger.getLogger(EmployeeAssignServlet.class);
    private static final AssetAssigneeDAO assetAssigneeDAO = new AssetAssigneeDAO();

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            String actionType = request.getParameter("actionType");
            String pb = request.getParameter("pb");
            String employeeAssignFolder = "employee_assign";
            if (pb != null) {
                employeeAssignFolder = "pb_employee_assign";
            }
            switch (actionType) {
                case "getEmployeeRecord": {
                    int officeId = Integer.parseInt(request.getParameter("officeId"));
                    ArrayList<Pair<Map<Object, Object>, ArrayList<EmployeeAssignDTO>>> data = new EmployeeAssignDAO().getEmployeeRecordDTOs2(officeId);
                    request.getSession().setAttribute("data", data);
                    RequestDispatcher rd;
                    rd = request.getRequestDispatcher(employeeAssignFolder + "/employeeAssignSearch.jsp");
                    rd.forward(request, response);
                    break;
                }
                case "getDisciplinaryEmployeeList": {
                    long officeId = Long.parseLong(request.getParameter("officeId"));
                    List<EmployeeAssignDTO> data = Employee_recordsRepository.getInstance().getActiveEmployeeAssignDTOByOfficeUnitId(officeId);
                    request.getSession().setAttribute("data", data);
                    RequestDispatcher rd;
                    rd = request.getRequestDispatcher(employeeAssignFolder + "/employeeSearchModalTable.jsp");
                    rd.forward(request, response);
                    break;
                }
                case "getDriverEmployeeList": {
                    String all = request.getParameter("all");
                    boolean allDriver = all != null && all.equals("1");
                    String vmRequisitionId = request.getParameter("ID");
                    HashSet<Long> assignedDriverIds = new HashSet<>();
                    if (!allDriver) {
                        Vm_requisitionDAO vm_requisitionDAO = new Vm_requisitionDAO();
                        Vm_requisitionDTO vm_requisitionDTO = vm_requisitionDAO.getDTOByID(Long.parseLong(vmRequisitionId));
                        assignedDriverIds = new HashSet<>(
                                (List<Long>) vm_requisitionDAO.getDriverIDs(vm_requisitionDTO)
                        );
                    }

                    List<EmployeeAssignDTO> dataUnProcessed = Employee_recordsRepository.getInstance().getDriverEmployeeAssignDTO();
                    List<EmployeeAssignDTO> data = new ArrayList<>();
                    HashSet<Long> finalAssignedDriverIds = assignedDriverIds;
                    dataUnProcessed
                            .stream().filter(employeeAssignDTO -> !finalAssignedDriverIds.contains(employeeAssignDTO.employee_id))
                            .forEach(data::add);
                    request.getSession().setAttribute("data", data);
                    RequestDispatcher rd;
                    rd = request.getRequestDispatcher(employeeAssignFolder + "/employeeSearchModalTable.jsp");
                    rd.forward(request, response);
                    break;
                }
                case "getEmployeeListFilteredByLeaveReliever": {
                    long officeId = Long.parseLong(request.getParameter("officeId"));
                    List<EmployeeAssignDTO> data = Employee_recordsRepository.getInstance().getEmployeeAssignDTOByOfficeUnitId(officeId);

                    List<Leave_reliever_mappingDTO> leaveRelieverMappingList = Leave_reliever_mappingRepository.getInstance().getLeave_reliever_mappingList();

                    HashSet<Long> allOrganogramIds = (HashSet<Long>) leaveRelieverMappingList.stream()
                            .map(dto -> dto.organogramId)
                            .collect(Collectors.toSet());
                    data = data.stream()
                            .filter(dto -> !allOrganogramIds.contains(dto.organogram_id))
                            .collect(Collectors.toList());

                    request.getSession().setAttribute("data", data);
                    request.getRequestDispatcher(employeeAssignFolder + "/employeeSearchModalTable.jsp").forward(request, response);
                    break;
                }
                case "getAllDescentEmployeeList": {
                    UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;
                    long empId = userDTO.employee_record_id;
                    List<EmployeeAssignDTO> data = EmployeeOfficesDAO.getInstance().getAllDescentEmployeeAssignDTOsForSameOfficeId(empId);
                    request.getSession().setAttribute("data", data);
                    RequestDispatcher rd;
                    rd = request.getRequestDispatcher(employeeAssignFolder + "/employeeSearchModalTable.jsp");
                    rd.forward(request, response);
                    break;
                }
                case "searchEmployeeOfficeInfo": {
                    String userName = request.getParameter("userName");
                    String nameEn = request.getParameter("nameEn");
                    String nameBn = request.getParameter("nameBn");
                    String phoneNumber = request.getParameter("phoneNumber");
                    logger.debug(userName);
                    logger.debug(nameEn);
                    logger.debug(nameBn);
                    Long curTime = Calendar.getInstance().getTimeInMillis();
                    Hashtable<String, String> searchTable = new Hashtable<>();
                    if (userName != null && userName.length() > 0)
                        searchTable.put("employee_number", userName);
                    if (nameEn != null && nameEn.length() > 0)
                        searchTable.put("name_eng", nameEn);
                    if (nameBn != null && nameBn.length() > 0)
                        searchTable.put("name_bng", nameBn);
                    if (phoneNumber != null && phoneNumber.length() > 0)
                        searchTable.put("personal_mobile", phoneNumber);
                    String seqrchSql = getSearchQueryForEmployeeModal(searchTable, String.valueOf(curTime));
                    List<EmployeeAssignDTO> searchedList = new EmployeeAssignDAO().getEmployeeAssignDTOsBySearch(seqrchSql, curTime);
                    searchedList.addAll(getActiveWithoutOfficeEmployees(searchTable));
                    request.getSession().setAttribute("data", searchedList);
                    RequestDispatcher rd = request.getRequestDispatcher(employeeAssignFolder + "/employeeSearchModalTable.jsp");
                    rd.forward(request, response);
                    break;
                }
                case "searchEmployeeOfficeInfoByOfficeProperty": {
                    String all = request.getParameter("all");
                    boolean allDriver = all != null && all.equals("1");
                    String vmRequisitionId = request.getParameter("ID");
                    HashSet<Long> assignedDriverIds = new HashSet<>();
                    if (!allDriver) {
                        Vm_requisitionDAO vm_requisitionDAO = new Vm_requisitionDAO();
                        Vm_requisitionDTO vm_requisitionDTO = vm_requisitionDAO.getDTOByID(Long.parseLong(vmRequisitionId));
                        assignedDriverIds = new HashSet<>(
                                (List<Long>) vm_requisitionDAO.getDriverIDs(vm_requisitionDTO)
                        );
                    }
                    String propertyName = request.getParameter("propertyName");
                    String propertyValue = request.getParameter("propertyValue");
                    String userName = request.getParameter("userName");
                    String nameEn = request.getParameter("nameEn");
                    String nameBn = request.getParameter("nameBn");
                    logger.debug(userName);
                    logger.debug(nameEn);
                    logger.debug(nameBn);
                    Long curTime = Calendar.getInstance().getTimeInMillis();
                    Hashtable<String, String> searchTable = new Hashtable<>();
                    Hashtable<String, String> searchOuterTable = new Hashtable<>();
                    searchOuterTable.put(propertyName, propertyValue);
                    if (userName != null && userName.length() > 0)
                        searchTable.put("employee_number", userName);
                    if (nameEn != null && nameEn.length() > 0)
                        searchTable.put("name_eng", nameEn);
                    if (nameBn != null && nameBn.length() > 0)
                        searchTable.put("name_bng", nameBn);
                    String seqrchSql = getSearchQueryForEmployeeModalByProperty(searchTable, searchOuterTable, String.valueOf(curTime));
                    List<EmployeeAssignDTO> searchedListUnProcessed = new EmployeeAssignDAO().getEmployeeAssignDTOsBySearch(seqrchSql, curTime);
                    if (StringUtils.isValidString(propertyName) && StringUtils.isValidString(propertyValue)) {
                        if (propertyName.equals("office_unit_id") && propertyValue.equals("45")) {
                            searchOuterTable.put("office_unit_id", "46");
                            seqrchSql = getSearchQueryForEmployeeModalByProperty(searchTable, searchOuterTable, String.valueOf(curTime))
                                    + " AND designation_en like '%despatch rider%'";
                            searchedListUnProcessed.addAll(new EmployeeAssignDAO().getEmployeeAssignDTOsBySearch(seqrchSql, curTime));
                        }
                    }
                    List<EmployeeAssignDTO> searchedList = new ArrayList<>();
                    HashSet<Long> finalAssignedDriverIds = assignedDriverIds;
                    searchedListUnProcessed
                            .stream().filter(employeeAssignDTO -> !finalAssignedDriverIds.contains(employeeAssignDTO.employee_id))
                            .forEach(searchedList::add);
//                    logger.debug("search sql : "+seqrchSql);
//                    searchedList.forEach(item->{
//                        logger.debug(item);
//                    });
                    request.getSession().setAttribute("data", searchedList);
                    RequestDispatcher rd = request.getRequestDispatcher(employeeAssignFolder + "/employeeSearchModalTable.jsp");
                    rd.forward(request, response);
                    break;
                }
                case "getEmployeeDetailsByUserName": {
                    String userName = request.getParameter("userName");
                    String nid = request.getParameter("nid");
                    String phone = request.getParameter("phone");

                    String data = new EmployeeAssignDAO().getEmployeeDetailsByUserNameOrNid(userName, nid, phone);
                    response.setContentType("application/json");
                    PrintWriter out = response.getWriter();

                    out.print(data);
                    out.flush();
                    break;
                }
                case "getEmployeeDetailsByUserNameOrNidOrOffice": {
                    String userName = request.getParameter("userName");

                    String phone = request.getParameter("phone");
                    Set<Long> lOfficeUnitIds = WorkflowController.getOfficeIdsFromOfficeUnitIdsParsed(request);

                    String data = new EmployeeAssignDAO().getEmployeeDetailsByUserNameOrOffice(userName, phone, lOfficeUnitIds);
                    response.setContentType("application/json");
                    PrintWriter out = response.getWriter();

                    out.print(data);
                    out.flush();
                    break;
                }
                case "getEmployeePermittedApp": {

                    int designationId = Integer.parseInt(request.getParameter("designation_id"));
                    ArrayList<EmployeeOfficePermissionDTO> data = new EmployeeAssignDAO().getEmployeePermittedAppByDesignationId(designationId);

                    String result = new Gson().toJson(data);

                    response.setContentType("application/json");
                    PrintWriter out = response.getWriter();

                    out.print(result);
                    out.flush();
                    break;
                }
                case "getEmployeeUnPermittedApp": {

                    int designationId = Integer.parseInt(request.getParameter("designation_id"));
                    ArrayList<EmployeeOfficePermissionDTO> data = new EmployeeAssignDAO().getEmployeeUnpermittedAppByDesignationId(designationId);

                    String result = new Gson().toJson(data);

                    response.setContentType("application/json");
                    PrintWriter out = response.getWriter();

                    out.print(result);
                    out.flush();
                    break;
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.debug(ex);
        }
    }


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;
        try {
            String actionType = request.getParameter("actionType");
            logger.debug("actionType = " + actionType);

            switch (actionType) {
                case "addLastOfficeDate": {
                    int employee_office_id = Integer.parseInt(request.getParameter("employee_office_id"));
                    String date = request.getParameter("last_office_date");
                    long millis = new SimpleDateFormat("yyyy-MM-dd").parse(date).getTime();
                    BooleanWrapper bw = BooleanWrapper.of();
                    EmployeeOfficeDTO employeeOfficeDTO = EmployeeOfficeRepository.getInstance().getById(employee_office_id);
                    OfficeUnitOrganograms ouo = OfficeUnitOrganogramsRepository.getInstance().getById(employeeOfficeDTO.officeUnitOrganogramId);
                    AtomicReference<Exception> ar = new AtomicReference();
                    if (!ouo.isVirtual) {
                        long lastModificationTime = System.currentTimeMillis() + 180000;//3mins delay
                        CommonDAOService.CACHE_UPDATE_MODEL_THREAD_LOCAL.set(new CacheUpdateModel(lastModificationTime));

                        try {
                            Utils.handleTransaction(() -> {
                                assetAssigneeDAO.unassignEmployeeFromPost(employeeOfficeDTO.officeUnitOrganogramId);
                                EmployeeAssignDAO employeeAssignDAO = new EmployeeAssignDAO();
                                employeeAssignDAO.addLastOfficeDateByOrganogramId(employeeOfficeDTO.officeUnitOrganogramId, millis, lastModificationTime);
                                Office_unit_organogramDAO.getInstance().updateVacancy(
                                        employeeOfficeDTO.officeUnitOrganogramId, 0, true, userDTO.ID, lastModificationTime
                                );

                                List<Committee_post_mapDTO> committee_post_mapDTOs = Committee_post_mapRepository.getInstance().getByReplacementPost(employeeOfficeDTO.officeUnitOrganogramId);
                                logger.debug("getting Committee count by post = " + employeeOfficeDTO.officeUnitOrganogramId);
                                if (committee_post_mapDTOs != null && committee_post_mapDTOs.size() > 0) {
                                    logger.debug("count by committee post = " + committee_post_mapDTOs.size());
                                    List<Long> idList = committee_post_mapDTOs.stream()
                                            .map(e -> e.postId)
                                            .collect(Collectors.toList());
                                    employeeAssignDAO.addLastOfficeDateByOrganogramIdList(idList, millis, lastModificationTime);
                                    Office_unit_organogramDAO.getInstance().updateVacancyList(
                                            idList, 0, true, userDTO.ID, lastModificationTime);
                                }

                                new UserDAO().updateLastModificationTimeByEmpId(employeeOfficeDTO.employeeRecordId,
                                        lastModificationTime, CommonDAOService.CONNECTION_THREAD_LOCAL.get());
                                Utils.addToRepoList("users");

                                String toDateString = employeeOfficeDTO.lastOfficeDate != SessionConstants.MIN_DATE ? " AND to_date <= " + employeeOfficeDTO.lastOfficeDate + " " : "";

                                List<PiUniqueItemAssignmentDTO> piUniqueItemAssignmentDTOS2 = PiUniqueItemAssignmentDAO.getInstance().
                                        getMovableItem(employeeOfficeDTO.officeUnitOrganogramId, PiStageEnum.OUT_STOCK.getValue(), PiActionTypeEnum.DELIVERY.getValue(),
                                                1, employeeOfficeDTO.joiningDate, toDateString);


                                if (piUniqueItemAssignmentDTOS2 != null && piUniqueItemAssignmentDTOS2.size() > 0) {
                                    bw.setValue(true);
                                }
                            });
                        } catch (Exception ex) {
                            ex.printStackTrace();
                            ar.set(ex);
                        }
                    }
                    if(ar.get() == null){
                        Utils.executeCache();
                    }
                    response.setContentType("application/json");
                    response.setCharacterEncoding("UTF-8");
                    PrintWriter out = response.getWriter();

                    out.print(new Pair<>("success", bw.getValue()));
                    out.flush();
                    break;
                }

                case "setPayScale": {
                    int employee_office_id = Integer.parseInt(request.getParameter("employee_office_id"));
                    int payScale = Integer.parseInt(request.getParameter("payScale"));
                    int salaryGrade = Integer.parseInt(request.getParameter("salaryGrade"));


                    EmployeeOfficeDTO employeeOfficeDTO = EmployeeOfficeRepository.getInstance().getById(employee_office_id);
                    employeeOfficeDTO.gradeTypeLevel = payScale;
                    if(salaryGrade >= 0)
                    {
                    	employeeOfficeDTO.salaryGradeType = salaryGrade;
                    }

                    EmployeeOfficesDAO.getInstance().update(employeeOfficeDTO);
                    EmployeeOfficeRepository.getInstance().reloadWithExactModificationTime(employeeOfficeDTO.lastModificationTime);

                    Promotion_historyDTO promotion_historyDTO = Promotion_historyDAO.getInstance().getDTOByID(employeeOfficeDTO.promotionHistoryId);
                    if (promotion_historyDTO != null) {
                        promotion_historyDTO.gradeTypeLevel = employeeOfficeDTO.gradeTypeLevel;
                        Promotion_historyDAO.getInstance().update(promotion_historyDTO);
                    }
                    // if is routine -> reload all serial of employees
                    boolean isRoutineResponsibility = InChargeLevelEnum.ROUTINE_RESPONSIBILITY.getValue().equalsIgnoreCase(employeeOfficeDTO.inchargeLabel);
                    if(isRoutineResponsibility) {
                        Employee_seniorityDAO.getInstance().reloadAllSenioritySerial(
                                Collections.singletonList(employeeOfficeDTO.employeeRecordId),
                                userDTO.ID
                        );
                    }

                    Pair<String, Boolean> result = new Pair<>("success", true);
                    response.setContentType("application/json");
                    PrintWriter out = response.getWriter();

                    out.print(result);
                    out.flush();
                    break;
                }
                case "assignEmployee":
                    AtomicReference<ApiResponse> apiResponse = new AtomicReference<>();

                    long employee_record_id = Long.parseLong(request.getParameter("employee_record_id"));

                    long office_unit_id = Long.parseLong(request.getParameter("office_unit_id"));

                    int office_unit_organogram_id = Integer.parseInt(request.getParameter("office_unit_organogram_id"));

                    String designation = request.getParameter("designation");

                    long assignDate = Long.parseLong(request.getParameter("assign_date"));

                    String inChargeLabel = request.getParameter("incharge_label");

                    int isDefault = inChargeLabel.equalsIgnoreCase(InChargeLevelEnum.ROUTINE_RESPONSIBILITY.getValue()) ? 1 : 0;

                    OfficeUnitOrganograms officeUnitOrganograms = OfficeUnitOrganogramsRepository.getInstance().getById(office_unit_organogram_id);

                    if (officeUnitOrganograms.isVirtual) {
                        apiResponse.set(ApiResponse.makeErrorResponse("Illegal operation : Assigning employee virtual post"));
                    } else {
                        long gradeTypLevel;

                        if (request.getParameter("gradeTypeLevel") != null) {
                            gradeTypLevel = Long.parseLong(request.getParameter("gradeTypeLevel"));
                        } else {
                            gradeTypLevel = officeUnitOrganograms.job_grade_type_cat;
                        }
                        
                       

                        EmployeeOfficeDTO employeeOfficesDTO = EmployeeOfficeRepository.getInstance().getByEmployeeRecordIdIsDefault(employee_record_id);
                        
                       

                        EmployeeOfficeDTO assignedOfficesDTO = EmployeeOfficeRepository.getInstance().getActiveByOfficeUnitOrganogramId(office_unit_organogram_id);

                        if (employeeOfficesDTO != null && isDefault == 1 && employeeOfficesDTO.status != 0) {
                            apiResponse.set(ApiResponse.makeErrorResponse("Employee is already assigned to Routine responsibility for another post"));

                        } else if (assignedOfficesDTO != null) {
                            apiResponse.set(ApiResponse.makeErrorResponse("Another employee is already assigned to this post"));

                        } else {
                            int designation_level = Integer.parseInt(request.getParameter("designation_level") != null && request.getParameter("designation_level").length() > 0 ? request.getParameter("designation_level") : "0");

                            int designation_sequence = Integer.parseInt(request.getParameter("designation_sequence") != null && request.getParameter("designation_sequence").length() > 0 ? request.getParameter("designation_sequence") : "0");

                            try {
                                long lastModificationTime = System.currentTimeMillis() + 180000;//3mins delay
                                CommonDAOService.CACHE_UPDATE_MODEL_THREAD_LOCAL.set(new CacheUpdateModel(lastModificationTime));
                                Utils.handleTransaction(() -> {
                                	 long salaryGradeType = 0;
                                     
                                     if (request.getParameter("salaryGradeType") != null) {
                                     	salaryGradeType = Long.parseLong(request.getParameter("salaryGradeType"));
                                     }
                                    new EmployeeAssignDAO().addEmployeeIntoNewOffice(employee_record_id,
                                            office_unit_id, office_unit_organogram_id, designation, designation_level,
                                            designation_sequence, inChargeLabel, isDefault, assignDate, gradeTypLevel, lastModificationTime, userDTO.ID, salaryGradeType);

                                    Office_unit_organogramDAO.getInstance().updateVacancy(
                                            office_unit_organogram_id, employee_record_id, false, userDTO.ID, lastModificationTime
                                    );

                                    assetAssigneeDAO.assignEmployeeToPost(office_unit_organogram_id, employee_record_id);

                                    List<Committee_post_mapDTO> committee_post_mapDTOs = Committee_post_mapRepository.getInstance().getByReplacementPost(office_unit_organogram_id);
                                    logger.debug("getByReplacementPost id = " + office_unit_organogram_id + " isnull = " + (committee_post_mapDTOs == null));
                                    if (committee_post_mapDTOs != null && !committee_post_mapDTOs.isEmpty()) {
                                        logger.debug("getByReplacementPost id = " + office_unit_organogram_id + " size = " + committee_post_mapDTOs.size());
                                        for (Committee_post_mapDTO committee_post_mapDTO : committee_post_mapDTOs) {
                                            Office_unit_organogramDAO.getInstance().assignHead(committee_post_mapDTO.postId, office_unit_organogram_id, userDTO, committee_post_mapDTO.committeeId, lastModificationTime, employee_record_id);
                                        }
                                    }
                                    // if is routine -> reload all serial of employees
                                    if(isDefault == 1) {
                                        Employee_seniorityDAO.getInstance().reloadAllSenioritySerial(
                                                Collections.singletonList(employee_record_id),
                                                userDTO.ID
                                        );
                                    }

                                    apiResponse.set(ApiResponse.makeSuccessResponse("Successfully assigned"));
                                });
                            } catch (Exception ex) {
                                ex.printStackTrace();
                                logger.error("-------------" + ex.getMessage());
                                apiResponse.set(ApiResponse.makeErrorResponse(ex.getMessage()));
                            }
                        }
                        if (apiResponse.get().getResponseCode() == ApiResponse.API_RESPONSE_OK) {
                            Utils.executeCache();
                        }
                    }

                    response.getWriter().write(apiResponse.get().getJSONString());
                    response.getWriter().flush();
                    response.getWriter().close();
                    break;
                case "assignAppPermission": {
                    int designationId = Integer.parseInt(request.getParameter("designation_id"));
                    String[] appIds = request.getParameterValues("app_id");
                    int[] apps = new int[appIds.length];

                    for (int i = 0; i < appIds.length; i++) {
                        apps[i] = Integer.parseInt(appIds[i]);
                    }
                    new EmployeeAssignDAO().employeeAppPermission(designationId, apps);

                    Map<String, Boolean> result = new HashMap<>();
                    result.put("success", true);
                    response.setContentType("application/json");
                    PrintWriter out = response.getWriter();

                    out.print(new Gson().toJson(result));
                    out.flush();
                    break;
                }
                case "setLastAttachmentDate": {

                    long attachmentId = Long.parseLong(request.getParameter("attachmentId"));
                    long lastDate = Long.parseLong(request.getParameter("lastDate"));
                    Employee_attachmentDTO attachmentDTO = Employee_attachmentDAO.getInstance().getDTOFromID(attachmentId);
                    attachmentDTO.endDate = lastDate;
                    attachmentDTO.status = 2;
                    attachmentDTO.lastModificationTime = System.currentTimeMillis();
                    attachmentDTO.modifiedBy = userDTO.ID;
                    Employee_attachmentDAO.getInstance().update(attachmentDTO);
                    Pair<String, Boolean> result = new Pair<>("success", true);
                    response.setContentType("application/json");
                    PrintWriter out = response.getWriter();
                    out.print(result);
                    out.flush();
                    break;
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.debug(ex);
        }

    }

    private String getSearchQueryForEmployeeModal(Hashtable p_searchCriteria, String curTime) {
        StringBuilder sqlBuilder = new StringBuilder();
        sqlBuilder.append("SELECT employee_record_id,office_unit_id,office_unit_organogram_id\n" +
                "from employee_offices\n" +
                "where\n" +
                "employee_record_id IN(SELECT employee_records.id from employee_records\n" +
                "    where employee_records.isDeleted=0 "
                + " and employee_records.status = 1 ");
        if (p_searchCriteria != null) {
            Map<String, String> map = new HashMap<>(p_searchCriteria);
            String searchString = map.entrySet()
                    .stream()
                    .filter(entry -> entry.getValue() != null && entry.getValue().trim().length() > 0 && EmployeeAssignDAO.employeeModalSearchMap.get(entry.getKey()) != null)
                    .map(entry -> EmployeeAssignDAO.employeeModalSearchMap.get(entry.getKey()).replace("{}", entry.getValue()))
                    .collect(Collectors.joining(" "));
            if (searchString.trim().length() > 0) {
                sqlBuilder.append(searchString);
            }
        }

        sqlBuilder.append(")\n" + "AND\n" + "isDeleted=0 AND is_default_role = 1 and status = 1 and " + "(last_office_date=" + SessionConstants.MIN_DATE + " OR " + "last_office_date>=").append(curTime).append(")");
        logger.debug(sqlBuilder.toString());
        return sqlBuilder.toString();
    }

    private String getSearchQueryForEmployeeModalByProperty(Hashtable p_searchCriteria, Hashtable p_searchCriteriaOuter, String curTime) {
        StringBuilder sqlBuilder = new StringBuilder();
        sqlBuilder.append("SELECT employee_record_id,office_unit_id,office_unit_organogram_id\n" +
                "from employee_offices\n" +
                "where\n" +
                "employee_record_id IN(SELECT employee_records.id from employee_records\n" +
                "    where employee_records.isDeleted=0 "
                + " and (select active from users where users.employee_record_id = employee_records.id and users.isDeleted = 0 limit 1) = 1 ");
        if (p_searchCriteria != null) {
            Map<String, String> map = new HashMap<>(p_searchCriteria);
            String searchString = map.entrySet()
                    .stream()
                    .filter(entry -> entry.getValue() != null && entry.getValue().trim().length() > 0 && EmployeeAssignDAO.employeeModalSearchMap.get(entry.getKey()) != null)
                    .map(entry -> EmployeeAssignDAO.employeeModalSearchMap.get(entry.getKey()).replace("{}", entry.getValue()))
                    .collect(Collectors.joining(" "));
            if (searchString.trim().length() > 0) {
                sqlBuilder.append(searchString);
            }
        }
        sqlBuilder.append(")\n" + "AND\n" + "isDeleted=0 AND " + "(last_office_date=" + SessionConstants.MIN_DATE + " OR " + "last_office_date>=").append(curTime).append(")");
        Map<String, String> map = new HashMap<>(p_searchCriteriaOuter);
        String searchString = map.entrySet()
                .stream()
                .filter(entry -> entry.getValue() != null && entry.getValue().trim().length() > 0)
                .map(entry -> " AND " + (entry.getKey() + " = " + entry.getValue()))
                .collect(Collectors.joining(" "));
        if (searchString.trim().length() > 0) {
            sqlBuilder.append(searchString);
        }
        logger.debug(sqlBuilder.toString());
        return sqlBuilder.toString();
    }

    private EmployeeAssignDTO getWithoutOfficeEmployeesAssignDTO(Employee_recordsDTO employeeRecordsDTO) {
        EmployeeAssignDTO employeeAssignDTO = new EmployeeAssignDTO();
        employeeAssignDTO.employee_id = employeeRecordsDTO.iD;
        employeeAssignDTO.employee_name_eng = employeeRecordsDTO.nameEng;
        employeeAssignDTO.employee_name_bng = employeeRecordsDTO.nameBng;

        employeeAssignDTO.office_unit_id = -1;
        NameDTO otherOfficeDTO = Other_officeRepository.getInstance().getDTOByID(employeeRecordsDTO.currentOffice);
        if (otherOfficeDTO == null) otherOfficeDTO = new NameDTO();
        employeeAssignDTO.unit_name_eng = otherOfficeDTO.nameEn;
        employeeAssignDTO.unit_name_bng = otherOfficeDTO.nameBn;

        employeeAssignDTO.organogram_id = -1;
        employeeAssignDTO.organogram_name_eng = employeeRecordsDTO.currentDesignation;
        employeeAssignDTO.organogram_name_bng = employeeRecordsDTO.currentDesignation;

        employeeAssignDTO.username = employeeRecordsDTO.employeeNumber;
        return employeeAssignDTO;
    }

    private boolean isWithoutOfficeEmployee(Employee_recordsDTO employeeRecordsDTO) {
        return employeeRecordsDTO.officerTypeCat == EmployeeOfficerCat.OTHERS.getValue();
    }

    private List<EmployeeAssignDTO> getActiveWithoutOfficeEmployees(Map<String, String> searchCriteria) {
        return new Employee_recordsDAO()
                .searchActiveEmployees(searchCriteria)
                .stream()
                .filter(this::isWithoutOfficeEmployee)
                .map(this::getWithoutOfficeEmployeesAssignDTO)
                .collect(Collectors.toList());
    }
}
