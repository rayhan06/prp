package employee_assign;

import common.NameDTO;
import employee_offices.EmployeeOfficeDTO;
import employee_offices.EmployeeOfficeRepository;
import employee_offices.Employee_officesDTO;
import employee_records.Employee_recordsDTO;
import employee_records.Employee_recordsRepository;
import office_unit_organograms.OfficeUnitOrganograms;
import office_unit_organograms.OfficeUnitOrganogramsRepository;
import office_units.Office_unitsDTO;
import office_units.Office_unitsRepository;
import other_office.Other_officeRepository;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@SuppressWarnings({"Duplicates"})
public class EmployeeSearchModalUtil {
    // ---------------public methods-----------------------
    public static String initJsMapSingleEmployee(long employeeRecordId, long officeUnitId, long organogramId) {
        return initJsMap(
                Collections.singletonList(
                        new EmployeeSearchIds(employeeRecordId, officeUnitId, organogramId)
                )
        );
    }

    public static String initJsMap(List<EmployeeSearchIds> idsList) {
        if (idsList == null || idsList.isEmpty()) {
            return "";
        }

        List<EmployeeSearchModel> modelList = idsList.stream()
                                                     .map(EmployeeSearchModalUtil::getEmployeeSearchModel)
                                                     .collect(Collectors.toList());

        return initJsMapFromModelList(modelList);
    }

    public static String initJsMapWithDefaultOffice(long employeeRecordId) {
        return initJsMap(
                Collections.singletonList(getDefaultOfficeIds(employeeRecordId))
        );
    }

    public static String getEmployeeSearchModelJson(long employeeRecordId, long officeUnitId, long organogramId) {
        return getEmployeeSearchModel(
                new EmployeeSearchIds(employeeRecordId, officeUnitId, organogramId)
        ).getJSON();
    }

    public static EmployeeSearchModel getEmployeeSearchModelByOrganogramId(long organogramId) {
        if (organogramId < 0) return null;

        EmployeeOfficeDTO employeeOffices = EmployeeOfficeRepository.getInstance().getActiveByOfficeUnitOrganogramId(organogramId);
        if (employeeOffices == null) return null;

        return getEmployeeSearchModel(
                new EmployeeSearchIds(employeeOffices.employeeRecordId, employeeOffices.officeUnitId, organogramId)
        );
    }

    public static String getEmployeeDefaultSearchModelJson(long employeeRecordId) {
        return getEmployeeSearchModel(getDefaultOfficeIds(employeeRecordId)).getJSON();
    }

    public static EmployeeSearchIds getDefaultOfficeIds(long employeeRecordId) {
        EmployeeOfficeDTO employeeOfficeDTO = EmployeeOfficeRepository.getInstance().getByEmployeeRecordIdIsDefault(employeeRecordId);

        System.out.println(employeeOfficeDTO);
        return new EmployeeSearchIds(
                employeeOfficeDTO.employeeRecordId,
                employeeOfficeDTO.officeUnitId,
                employeeOfficeDTO.officeUnitOrganogramId
        );
    }

    public static EmployeeSearchModel getModelWithOnlyEmployeeInfo(long employeeRecordId) {
        Employee_recordsDTO employee_recordsDTO = null;
        try {
            employee_recordsDTO = Employee_recordsRepository.getInstance().getById(employeeRecordId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (employee_recordsDTO == null)
            employee_recordsDTO = new Employee_recordsDTO();
        EmployeeSearchModel employeeSearchModel = new EmployeeSearchModel();
        employeeSearchModel.employeeRecordId = employee_recordsDTO.iD;
        employeeSearchModel.userName = employee_recordsDTO.employeeNumber;
        employeeSearchModel.employeeNameEn = employee_recordsDTO.nameEng;
        employeeSearchModel.employeeNameBn = employee_recordsDTO.nameBng;
        employeeSearchModel.email = employee_recordsDTO.personalEml;
        employeeSearchModel.phoneNumber = employee_recordsDTO.personalMobile;
        return employeeSearchModel;
    }

    public static EmployeeSearchModel getEmployeeSearchModel(EmployeeSearchIds ids) {
        Employee_recordsDTO employee_recordsDTO = null;
        Office_unitsDTO officeUnitsDTO = null;
        OfficeUnitOrganograms officeUnitOrganograms = null;
        try {
            employee_recordsDTO = Employee_recordsRepository.getInstance().getById(ids.employeeRecordId);

            officeUnitsDTO = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(ids.officeUnitId);

            officeUnitOrganograms = OfficeUnitOrganogramsRepository.getInstance().getById(ids.organogramId);

        } catch (Exception e) {
            e.printStackTrace();
        }

        EmployeeSearchModel employeeSearchModel = new EmployeeSearchModel();

        if (employee_recordsDTO != null) {
            employeeSearchModel.employeeRecordId = employee_recordsDTO.iD;
            employeeSearchModel.userName = employee_recordsDTO.employeeNumber;
            employeeSearchModel.employeeNameEn = employee_recordsDTO.nameEng;
            employeeSearchModel.employeeNameBn = employee_recordsDTO.nameBng;
            employeeSearchModel.email = employee_recordsDTO.personalEml;
            employeeSearchModel.phoneNumber = employee_recordsDTO.personalMobile;

            if (officeUnitsDTO != null) {
                employeeSearchModel.officeUnitId = officeUnitsDTO.iD;
                employeeSearchModel.officeUnitNameEn = officeUnitsDTO.unitNameEng;
                employeeSearchModel.officeUnitNameBn = officeUnitsDTO.unitNameBng;
            } else {
                employeeSearchModel.officeUnitId = -1;
                NameDTO otherOfficeDTO = Other_officeRepository.getInstance().getDTOByID(employee_recordsDTO.currentOffice);
                if (otherOfficeDTO != null) {
                    employeeSearchModel.officeUnitNameEn = otherOfficeDTO.nameEn;
                    employeeSearchModel.officeUnitNameBn = otherOfficeDTO.nameBn;
                }
            }

            if (officeUnitOrganograms != null) {
                employeeSearchModel.organogramId = officeUnitOrganograms.id;
                employeeSearchModel.organogramNameEn = officeUnitOrganograms.designation_eng;
                employeeSearchModel.organogramNameBn = officeUnitOrganograms.designation_bng;
            } else {
                employeeSearchModel.organogramId = -1;
                employeeSearchModel.organogramNameEn = employee_recordsDTO.currentDesignation;
                employeeSearchModel.organogramNameBn = employee_recordsDTO.currentDesignation;
            }
        }
        return employeeSearchModel;
    }

    // ---------------private methods-----------------------

    private static String getMapKey(EmployeeSearchModel model) {
        return "'" + model.employeeRecordId + "'";
    }

    private static String getJsKeyValPair(EmployeeSearchModel model) {
        String kvPair = "";
        try {
            kvPair = "[" + getMapKey(model) + "," + model.getJSON() + "]";
        } catch (Exception e) {
            e.printStackTrace();
        }
        return kvPair;
    }

    private static String initJsMapFromModelList(List<EmployeeSearchModel> modelList) {
        if (modelList == null || modelList.isEmpty()) {
            return "";
        }

        return modelList.stream()
                        .map(EmployeeSearchModalUtil::getJsKeyValPair)
                        .collect(Collectors.joining(",", "[", "]"));
    }


    // ---------------testing code-----------------------
    public static void main(String[] args) throws Exception {
        // getEmployeeInfoDefaultOfficeInfo(3202900L);


        System.out.println(getTestModel(3202900L).getJSON());

        // initJSMap Test
        List<EmployeeSearchModel> modelList = Arrays.asList(
                getTestModel(3202900L),
                getTestModel(3202901L),
                getTestModel(3202902L)
        );
        System.out.println(initJsMapFromModelList(modelList));
    }

    public static EmployeeSearchModel getTestModel(long id) {
        EmployeeSearchModel employeeSearchModel = new EmployeeSearchModel();
        employeeSearchModel.employeeRecordId = id;
        employeeSearchModel.employeeNameEn = "Erfan Hossain";
        employeeSearchModel.employeeNameBn = "ইরফান";
        employeeSearchModel.officeUnitId = 1;
        employeeSearchModel.officeUnitNameEn = "SPEAKER";
        employeeSearchModel.officeUnitNameBn = "মাননীয় স্পীকারের কার্যালয় ";
        employeeSearchModel.organogramId = 261503;
        employeeSearchModel.organogramNameEn = "PS";
        employeeSearchModel.organogramNameBn = "পিএস";
        return employeeSearchModel;
    }
}
