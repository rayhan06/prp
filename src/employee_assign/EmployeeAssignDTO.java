package employee_assign;

public class EmployeeAssignDTO {

    public long employee_id;
    public String employee_name_eng;
    public String employee_name_bng;

    public long officeId;

    public long office_unit_id;
    public String unit_name_eng;
    public String unit_name_bng;

    public String username;

    public long organogram_id;
    public String organogram_name_eng;
    public String organogram_name_bng;

    public long designation_id;
    public int designation_level;

    public int designation_sequence;


    public long gradeTypeLevel;
    public long employee_office_id;
    public long joining_date;
    public long last_office_date;
    public String inChargeLabel;
    public String briefDescriptionEng = "";
    public String briefDescriptionBng = "";
    public long salaryGradeType = 0;

    public String toUserJSON() {
        String jsonStr = "{\"employeeRecordId\": %d, \"employeeNameEn\": \"%s\", \"employeeNameBn\": \"%s\", "
                + " \"officeUnitId\": %d, \"officeUnitNameEn\": \"%s\", \"officeUnitNameBn\": \"%s\", "
                + " \"organogramId\": %d, \"organogramNameEn\": \"%s\", \"organogramNameBn\": \"%s\" }";

        return String.format(
                jsonStr, employee_id, employee_name_eng, employee_name_bng,
                office_unit_id, unit_name_eng, unit_name_bng,
                organogram_id, organogram_name_eng, organogram_name_bng
        );
    }

    @Override
    public String toString() {
        return "EmployeeAssignDTO{" +
                "officeId=" + officeId +
                ", office_unit_id=" + office_unit_id +
                ", unit_name_eng='" + unit_name_eng + '\'' +
                ", unit_name_bng='" + unit_name_bng + '\'' +
                ", username='" + username + '\'' +
                ", organogram_id=" + organogram_id +
                ", designation_id=" + designation_id +
                ", designation_level=" + designation_level +
                ", designation_sequence=" + designation_sequence +
                ", organogram_name_eng='" + organogram_name_eng + '\'' +
                ", organogram_name_bng='" + organogram_name_bng + '\'' +
                ", employee_id=" + employee_id +
                ", employee_name_eng='" + employee_name_eng + '\'' +
                ", employee_name_bng='" + employee_name_bng + '\'' +
                ", employee_office_id=" + employee_office_id +
                ", joining_date=" + joining_date +
                ", last_office_date=" + last_office_date +
                '}';
    }


}
