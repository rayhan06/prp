package employee_assign;

public class OfficeUnitAndOrganogramModel {
    public long officeUnitId;
    public String unitNameBng;
    public String unitNameEng;
    public long officeId;
    public long organogramId;
    public String designationBng;
    public String designationEng;
    public int designationLevel;
    public int designationSequence;
}
