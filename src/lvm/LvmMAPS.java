package lvm;
import java.util.*; 
import util.*;


public class LvmMAPS extends CommonMaps
{	
	public LvmMAPS(String tableName)
	{
		
		java_allfield_type_map.put("employee_in_leave_type".toLowerCase(), "Long");
		java_allfield_type_map.put("employee_in_leave_organogram_id".toLowerCase(), "Long");
		java_allfield_type_map.put("employee_in_leave_name".toLowerCase(), "String");
		java_allfield_type_map.put("employee_in_leave_designation".toLowerCase(), "String");
		java_allfield_type_map.put("employee_in_leave_office_unit_id".toLowerCase(), "Long");
		java_allfield_type_map.put("employee_in_leave_office_unit_name".toLowerCase(), "String");
		java_allfield_type_map.put("employee_in_charge_type".toLowerCase(), "Long");
		java_allfield_type_map.put("employee_in_charge_organogram_id".toLowerCase(), "Long");
		java_allfield_type_map.put("employee_in_charge_name".toLowerCase(), "String");
		java_allfield_type_map.put("employee_in_charge_designation".toLowerCase(), "String");
		java_allfield_type_map.put("employee_in_charge_office_unit_id".toLowerCase(), "Long");
		java_allfield_type_map.put("employee_in_charge_office_unit_name".toLowerCase(), "String");
		java_allfield_type_map.put("employee_in_charge_policy_roles_type".toLowerCase(), "Long");
		java_allfield_type_map.put("leave_start_date".toLowerCase(), "Long");
		java_allfield_type_map.put("leave_end_date".toLowerCase(), "Long");
		java_allfield_type_map.put("leave_entry_date".toLowerCase(), "Long");
		java_allfield_type_map.put("leave_status_cat".toLowerCase(), "Integer");

		java_anyfield_search_map.put("language_text.languageTextEnglish", "String");
		java_anyfield_search_map.put("language_text.languageTextBangla", "String");

		java_anyfield_search_map.put("employee_records.name_en".toLowerCase(), "String");
		java_anyfield_search_map.put("employee_records.name_bn".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".employee_in_leave_organogram_id".toLowerCase(), "Long");
		java_anyfield_search_map.put(tableName + ".employee_in_leave_name".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".employee_in_leave_designation".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".employee_in_leave_office_unit_id".toLowerCase(), "Long");
		java_anyfield_search_map.put(tableName + ".employee_in_leave_office_unit_name".toLowerCase(), "String");
		java_anyfield_search_map.put("employee_records.name_en".toLowerCase(), "String");
		java_anyfield_search_map.put("employee_records.name_bn".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".employee_in_charge_organogram_id".toLowerCase(), "Long");
		java_anyfield_search_map.put(tableName + ".employee_in_charge_name".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".employee_in_charge_designation".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".employee_in_charge_office_unit_id".toLowerCase(), "Long");
		java_anyfield_search_map.put(tableName + ".employee_in_charge_office_unit_name".toLowerCase(), "String");
		java_anyfield_search_map.put("employee_policy_roles.name_en".toLowerCase(), "String");
		java_anyfield_search_map.put("employee_policy_roles.name_bn".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".leave_start_date".toLowerCase(), "Long");
		java_anyfield_search_map.put(tableName + ".leave_end_date".toLowerCase(), "Long");
		java_anyfield_search_map.put(tableName + ".leave_entry_date".toLowerCase(), "Long");

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("employeeInLeaveType".toLowerCase(), "employeeInLeaveType".toLowerCase());
		java_DTO_map.put("employeeInLeaveOrganogramId".toLowerCase(), "employeeInLeaveOrganogramId".toLowerCase());
		java_DTO_map.put("employeeInLeaveName".toLowerCase(), "employeeInLeaveName".toLowerCase());
		java_DTO_map.put("employeeInLeaveDesignation".toLowerCase(), "employeeInLeaveDesignation".toLowerCase());
		java_DTO_map.put("employeeInLeaveOfficeUnitId".toLowerCase(), "employeeInLeaveOfficeUnitId".toLowerCase());
		java_DTO_map.put("employeeInLeaveOfficeUnitName".toLowerCase(), "employeeInLeaveOfficeUnitName".toLowerCase());
		java_DTO_map.put("employeeInChargeType".toLowerCase(), "employeeInChargeType".toLowerCase());
		java_DTO_map.put("employeeInChargeOrganogramId".toLowerCase(), "employeeInChargeOrganogramId".toLowerCase());
		java_DTO_map.put("employeeInChargeName".toLowerCase(), "employeeInChargeName".toLowerCase());
		java_DTO_map.put("employeeInChargeDesignation".toLowerCase(), "employeeInChargeDesignation".toLowerCase());
		java_DTO_map.put("employeeInChargeOfficeUnitId".toLowerCase(), "employeeInChargeOfficeUnitId".toLowerCase());
		java_DTO_map.put("employeeInChargeOfficeUnitName".toLowerCase(), "employeeInChargeOfficeUnitName".toLowerCase());
		java_DTO_map.put("employeeInChargePolicyRolesType".toLowerCase(), "employeeInChargePolicyRolesType".toLowerCase());
		java_DTO_map.put("leaveStartDate".toLowerCase(), "leaveStartDate".toLowerCase());
		java_DTO_map.put("leaveEndDate".toLowerCase(), "leaveEndDate".toLowerCase());
		java_DTO_map.put("leaveEntryDate".toLowerCase(), "leaveEntryDate".toLowerCase());
		java_DTO_map.put("leaveStatusCat".toLowerCase(), "leaveStatusCat".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("employee_in_leave_type".toLowerCase(), "employeeInLeaveType".toLowerCase());
		java_SQL_map.put("employee_in_leave_organogram_id".toLowerCase(), "employeeInLeaveOrganogramId".toLowerCase());
		java_SQL_map.put("employee_in_leave_name".toLowerCase(), "employeeInLeaveName".toLowerCase());
		java_SQL_map.put("employee_in_leave_designation".toLowerCase(), "employeeInLeaveDesignation".toLowerCase());
		java_SQL_map.put("employee_in_leave_office_unit_id".toLowerCase(), "employeeInLeaveOfficeUnitId".toLowerCase());
		java_SQL_map.put("employee_in_leave_office_unit_name".toLowerCase(), "employeeInLeaveOfficeUnitName".toLowerCase());
		java_SQL_map.put("employee_in_charge_type".toLowerCase(), "employeeInChargeType".toLowerCase());
		java_SQL_map.put("employee_in_charge_organogram_id".toLowerCase(), "employeeInChargeOrganogramId".toLowerCase());
		java_SQL_map.put("employee_in_charge_name".toLowerCase(), "employeeInChargeName".toLowerCase());
		java_SQL_map.put("employee_in_charge_designation".toLowerCase(), "employeeInChargeDesignation".toLowerCase());
		java_SQL_map.put("employee_in_charge_office_unit_id".toLowerCase(), "employeeInChargeOfficeUnitId".toLowerCase());
		java_SQL_map.put("employee_in_charge_office_unit_name".toLowerCase(), "employeeInChargeOfficeUnitName".toLowerCase());
		java_SQL_map.put("employee_in_charge_policy_roles_type".toLowerCase(), "employeeInChargePolicyRolesType".toLowerCase());
		java_SQL_map.put("leave_start_date".toLowerCase(), "leaveStartDate".toLowerCase());
		java_SQL_map.put("leave_end_date".toLowerCase(), "leaveEndDate".toLowerCase());
		java_SQL_map.put("leave_entry_date".toLowerCase(), "leaveEntryDate".toLowerCase());
		java_SQL_map.put("leave_status_cat".toLowerCase(), "leaveStatusCat".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Employee In Leave".toLowerCase(), "employeeInLeaveType".toLowerCase());
		java_Text_map.put("Employee In Leave Organogram Id".toLowerCase(), "employeeInLeaveOrganogramId".toLowerCase());
		java_Text_map.put("Employee In Leave Name".toLowerCase(), "employeeInLeaveName".toLowerCase());
		java_Text_map.put("Employee In Leave Designation".toLowerCase(), "employeeInLeaveDesignation".toLowerCase());
		java_Text_map.put("Employee In Leave Office Unit Id".toLowerCase(), "employeeInLeaveOfficeUnitId".toLowerCase());
		java_Text_map.put("Employee In Leave Office Unit Name".toLowerCase(), "employeeInLeaveOfficeUnitName".toLowerCase());
		java_Text_map.put("Employee In Charge".toLowerCase(), "employeeInChargeType".toLowerCase());
		java_Text_map.put("Employee In Charge Organogram Id".toLowerCase(), "employeeInChargeOrganogramId".toLowerCase());
		java_Text_map.put("Employee In Charge Name".toLowerCase(), "employeeInChargeName".toLowerCase());
		java_Text_map.put("Employee In Charge Designation".toLowerCase(), "employeeInChargeDesignation".toLowerCase());
		java_Text_map.put("Employee In Charge Office Unit Id".toLowerCase(), "employeeInChargeOfficeUnitId".toLowerCase());
		java_Text_map.put("Employee In Charge Office Unit Name".toLowerCase(), "employeeInChargeOfficeUnitName".toLowerCase());
		java_Text_map.put("Employee In Charge Policy Roles".toLowerCase(), "employeeInChargePolicyRolesType".toLowerCase());
		java_Text_map.put("Leave Start Date".toLowerCase(), "leaveStartDate".toLowerCase());
		java_Text_map.put("Leave End Date".toLowerCase(), "leaveEndDate".toLowerCase());
		java_Text_map.put("Leave Entry Date".toLowerCase(), "leaveEntryDate".toLowerCase());
		java_Text_map.put("Leave Status Cat".toLowerCase(), "leaveStatusCat".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}