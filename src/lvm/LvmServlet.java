package lvm;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.io.*;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import employee_offices.Employee_officesDTO;
import employee_records.Employee_recordsDAO;
import employee_records.Employee_recordsDTO;
import org.apache.log4j.Logger;

import login.LoginDTO;


import sessionmanager.SessionConstants;

import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;
import util.CommonRequestHandler;
import util.RecordNavigationManager4;

import java.util.*;
import javax.servlet.http.*;


import com.google.gson.Gson;

import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import workflow.WorkflowController;


/**
 * Servlet implementation class LvmServlet
 */
@WebServlet("/LvmServlet")
@MultipartConfig
public class LvmServlet extends HttpServlet 
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(LvmServlet.class);

    String tableName = "lvm";

	LvmDAO lvmDAO;
	CommonRequestHandler commonRequestHandler;
    private final Gson gson = new Gson();
    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LvmServlet() 
	{
        super();
    	try
    	{
			lvmDAO = new LvmDAO(tableName);
			commonRequestHandler = new CommonRequestHandler(lvmDAO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error("",e);
		}
    }   

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		System.out.println("In doget request = " + request);
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		try
		{
			String actionType = request.getParameter("actionType");
			if(actionType.equals("getAddPage"))
			{
				//if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.LVM_ADD))
				{
					commonRequestHandler.getAddPage(request, response);
				}
				
			}
			else if(actionType.equals("getEditPage"))
			{
				//if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.LVM_UPDATE))
				{
					getLvm(request, response);					
				}
									
			}
			else if(actionType.equals("getURL"))
			{
				String URL = request.getParameter("URL");
				System.out.println("URL = " + URL);
				response.sendRedirect(URL);			
			}			
			else if(actionType.equals("search"))
			{
				System.out.println("search requested");
				//if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.LVM_SEARCH))
				{
					
					if(isPermanentTable)
					{
						searchLvm(request, response, isPermanentTable);
					}
					else
					{
						//searchLvm(request, response, tempTableName, isPermanentTable);
					}
				}			
			}
			else if(actionType.equals("view"))
			{
				System.out.println("view requested");
				//if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.LVM_SEARCH))
				{
					commonRequestHandler.view(request, response);
				}
				
				
			}
			else
			{
				request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			}
		}
		catch(Exception ex)
		{
			logger.error("",ex);
		}
	}


	
	

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		// TODO Auto-generated method stub
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		System.out.println("doPost");
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		
		try
		{
			String actionType = request.getParameter("actionType");
			System.out.println("actionType = " + actionType);
			if(actionType.equals("add"))
			{
				
				//if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.LVM_ADD))
				{
					System.out.println("going to  addLvm ");
					addLvm(request, response, true, userDTO, true);
				}
				
				
			}

			else if(actionType.equals("getDTO"))
			{
				
				//if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.LVM_ADD))
				{
					getDTO(request, response);					
				}
				
				
			}
			else if(actionType.equals("edit"))
			{
				
				//if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.LVM_UPDATE))
				{					
					addLvm(request, response, false, userDTO, isPermanentTable);					
				}
				
			}
			else if(actionType.equals("delete"))
			{								
				deleteLvm(request, response, userDTO);				
			}	
			else if(actionType.equals("search"))
			{
				//if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.LVM_SEARCH))
				{
					searchLvm(request, response, true);
				}
				
			}
			else if(actionType.equals("getGeo"))
			{
				System.out.println("going to geoloc ");
				request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
			}
			
		}
		catch(Exception ex)
		{
			logger.error("",ex);
		}
	}
	
	private void getDTO(HttpServletRequest request, HttpServletResponse response) 
	{
		try 
		{
			System.out.println("In getDTO");
			LvmDTO lvmDTO = (LvmDTO)lvmDAO.getDTOByID(Long.parseLong(request.getParameter("ID")));
			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			
			String encoded = this.gson.toJson(lvmDTO);
			System.out.println("json encoded data = " + encoded);
			out.print(encoded);
			out.flush();
		}
		catch (NumberFormatException e) 
		{
			// TODO Auto-generated catch block
			logger.error("",e);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error("",e);
		}
		
	}

	private void addLvm(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, boolean isPermanentTable) throws IOException 
	{
		// TODO Auto-generated method stub
		try 
		{
			request.setAttribute("failureMessage", "");
			System.out.println("%%%% addLvm");
			String path = getServletContext().getRealPath("/img2/");
			LvmDTO lvmDTO;
						
			if(addFlag == true)
			{
				lvmDTO = new LvmDTO();
			}
			else
			{
				lvmDTO = (LvmDTO)lvmDAO.getDTOByID(Long.parseLong(request.getParameter("iD")));
			}


			Employee_recordsDAO employee_recordsDAO = new Employee_recordsDAO();

			String Value = "";

			Value = request.getParameter("employeeInLeaveType");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("employeeInLeaveType = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				lvmDTO.employeeInLeaveType = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Employee_recordsDTO employee_in_leave_recordsDTO = new Employee_recordsDTO();
			employee_in_leave_recordsDTO = employee_recordsDAO.getEmployee_recordsDTOByID( lvmDTO.employeeInLeaveType );

			Employee_officesDTO employee_in_leave_officesDTO = WorkflowController.getEmployee_officesDTOByEmployeeRecordsId( employee_in_leave_recordsDTO.iD );

			/*Value = request.getParameter("employeeInLeaveOrganogramId");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("employeeInLeaveOrganogramId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				lvmDTO.employeeInLeaveOrganogramId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}*/
			lvmDTO.employeeInLeaveOrganogramId = employee_in_leave_officesDTO.officeUnitOrganogramId;


			/*Value = request.getParameter("employeeInLeaveName");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("employeeInLeaveName = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				lvmDTO.employeeInLeaveName = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}*/
			lvmDTO.employeeInLeaveName = employee_in_leave_recordsDTO.nameEng;


			/*Value = request.getParameter("employeeInLvmDesignation");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("employeeInLvmDesignation = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				lvmDTO.employeeInLvmDesignation = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}*/
			lvmDTO.employeeInLeaveDesignation = employee_in_leave_officesDTO.designation;


			/*Value = request.getParameter("employeeInLeaveOfficeUnitId");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("employeeInLeaveOfficeUnitId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				lvmDTO.employeeInLeaveOfficeUnitId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}*/
			lvmDTO.employeeInLeaveOfficeUnitId = employee_in_leave_officesDTO.officeUnitId;


			/*Value = request.getParameter("employeeInLeaveOfficeUnitName");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("employeeInLeaveOfficeUnitName = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				lvmDTO.employeeInLeaveOfficeUnitName = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}*/
			lvmDTO.employeeInLeaveOfficeUnitName = employee_in_leave_officesDTO.officeUnitName;


			Value = request.getParameter("employeeInChargeType");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("employeeInChargeType = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				lvmDTO.employeeInChargeType = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Employee_recordsDTO employee_in_charge_recordsDTO = new Employee_recordsDTO();
			employee_in_charge_recordsDTO = employee_recordsDAO.getEmployee_recordsDTOByID( lvmDTO.employeeInChargeType );

			Employee_officesDTO employee_in_charge_officesDTO = WorkflowController.getEmployee_officesDTOByEmployeeRecordsId( employee_in_charge_recordsDTO.iD );

			/*Value = request.getParameter("employeeInChargeOrganogramId");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("employeeInChargeOrganogramId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				lvmDTO.employeeInChargeOrganogramId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}*/
			lvmDTO.employeeInChargeOrganogramId = employee_in_charge_officesDTO.officeUnitOrganogramId;


			/*Value = request.getParameter("employeeInChargeName");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("employeeInChargeName = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				lvmDTO.employeeInChargeName = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}*/
			lvmDTO.employeeInChargeName = employee_in_charge_recordsDTO.nameEng;


			/*Value = request.getParameter("employeeInChargeDesignation");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("employeeInChargeDesignation = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				lvmDTO.employeeInChargeDesignation = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}*/
			lvmDTO.employeeInChargeDesignation = employee_in_charge_officesDTO.designation;


			/*Value = request.getParameter("employeeInChargeOfficeUnitId");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("employeeInChargeOfficeUnitId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				lvmDTO.employeeInChargeOfficeUnitId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}*/
			lvmDTO.employeeInChargeOfficeUnitId = employee_in_charge_officesDTO.officeUnitId;


			/*Value = request.getParameter("employeeInChargeOfficeUnitName");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("employeeInChargeOfficeUnitName = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				lvmDTO.employeeInChargeOfficeUnitName = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}*/
			lvmDTO.employeeInChargeOfficeUnitName = employee_in_charge_officesDTO.officeUnitName;


			Value = request.getParameter("employeeInChargePolicyRolesType");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("employeeInChargePolicyRolesType = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				lvmDTO.employeeInChargePolicyRolesType = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("leaveStartDate");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("leaveStartDate = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				lvmDTO.leaveStartDate = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("leaveEndDate");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("leaveEndDate = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				lvmDTO.leaveEndDate = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			/*Value = request.getParameter("leaveEntryDate");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("leaveEntryDate = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				lvmDTO.leaveEntryDate = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}*/
			lvmDTO.leaveEntryDate = System.currentTimeMillis();



			Value = request.getParameter("leaveStatusCat");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("leaveStatusCat = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				lvmDTO.leaveStatusCat = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			
			System.out.println("Done adding  addLvm dto = " + lvmDTO);
			long returnedID = -1;
			
			if(isPermanentTable == false) //add new row for validation and make the old row outdated
			{
				lvmDAO.setIsDeleted(lvmDTO.iD, CommonDTO.OUTDATED);
				returnedID = lvmDAO.add(lvmDTO);
				lvmDAO.setIsDeleted(returnedID, CommonDTO.WAITING_FOR_APPROVAL);
			}
			else if(addFlag == true)
			{
				returnedID = lvmDAO.manageWriteOperations(lvmDTO, SessionConstants.INSERT, -1, userDTO);
			}
			else
			{				
				returnedID = lvmDAO.manageWriteOperations(lvmDTO, SessionConstants.UPDATE, -1, userDTO);											
			}
			
			Date startDate = new Date(lvmDTO.leaveStartDate);
			Date endDate = new Date(lvmDTO.leaveEndDate);
			
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			System.out.println("Start Date " + dateFormat.format(startDate));
			System.out.println("End Date " + dateFormat.format(endDate));
			
			Timer timer = new Timer(true);
	        timer.schedule(new LvmApprovalUpdater(LvmApprovalUpdater.LEAVE_STARTED, lvmDTO.employeeInLeaveOrganogramId, lvmDTO.employeeInChargeOrganogramId), startDate);
	        timer.schedule(new LvmApprovalUpdater(LvmApprovalUpdater.LEAVE_ENDED, lvmDTO.employeeInLeaveOrganogramId, 0), endDate);
			
			
			
			
			
			
			
			if(isPermanentTable)
			{
				String inPlaceSubmit = request.getParameter("inplacesubmit");
				
				if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
				{
					getLvm(request, response, returnedID);
				}
				else
				{
					response.sendRedirect("LvmServlet?actionType=search");
				}
			}
			else
			{
				commonRequestHandler.validate(lvmDAO.getDTOByID(returnedID), request, response, userDTO);
			}
					
		}
		catch (Exception e) 
		{
			logger.error("",e);
		}
	}
	
	



	
	
	

	private void deleteLvm(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO) throws IOException 
	{				
		try 
		{
			String[] IDsToDelete = request.getParameterValues("ID");
			for(int i = 0; i < IDsToDelete.length; i ++)
			{
				long id = Long.parseLong(IDsToDelete[i]);
				System.out.println("------ DELETING " + IDsToDelete[i]);
				
				
				LvmDTO lvmDTO = (LvmDTO)lvmDAO.getDTOByID(id);
				lvmDAO.manageWriteOperations(lvmDTO, SessionConstants.DELETE, id, userDTO);
				response.sendRedirect("LvmServlet?actionType=search");
				
			}			
		}
		catch (Exception ex) 
		{
			logger.error("",ex);
		}
		
	}

	private void getLvm(HttpServletRequest request, HttpServletResponse response, long id) throws ServletException, IOException
	{
		System.out.println("in getLvm");
		LvmDTO lvmDTO = null;
		try 
		{
			lvmDTO = (LvmDTO)lvmDAO.getDTOByID(id);
			request.setAttribute("ID", lvmDTO.iD);
			request.setAttribute("lvmDTO",lvmDTO);
			request.setAttribute("lvmDAO",lvmDAO);
			
			String URL= "";
			
			String inPlaceEdit = request.getParameter("inplaceedit");
			String inPlaceSubmit = request.getParameter("inplacesubmit");
			String getBodyOnly = request.getParameter("getBodyOnly");
			
			if(inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase(""))
			{
				URL = "lvm/lvmInPlaceEdit.jsp";	
				request.setAttribute("inplaceedit","");				
			}
			else if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
			{
				URL = "lvm/lvmSearchRow.jsp";
				request.setAttribute("inplacesubmit","");					
			}
			else
			{
				if(getBodyOnly  != null && !getBodyOnly.equalsIgnoreCase(""))
				{
					URL = "lvm/lvmEditBody.jsp?actionType=edit";
				}
				else
				{
					URL = "lvm/lvmEdit.jsp?actionType=edit";
				}				
			}
			
			RequestDispatcher rd = request.getRequestDispatcher(URL);
			rd.forward(request, response);
		}
		catch (NumberFormatException e) 
		{
			logger.error("",e);
		}
		catch (Exception e) 
		{
			logger.error("",e);
		}
	}
	
	
	private void getLvm(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		getLvm(request, response, Long.parseLong(request.getParameter("ID")));	
	}
	
	private void searchLvm(HttpServletRequest request, HttpServletResponse response, boolean isPermanent) throws ServletException, IOException
	{
		System.out.println("in  searchLvm 1");
		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String ajax = request.getParameter("ajax");
		boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);
		
        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
			SessionConstants.NAV_LVM,
			request,
			lvmDAO,
			SessionConstants.VIEW_LVM,
			SessionConstants.SEARCH_LVM,
			tableName,
			isPermanent,
			userDTO,
			"",
			true);
        try
        {
			System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        }
        catch(Exception e)
        {
			System.out.println("failed to dojob" + e);
        }

		request.setAttribute("lvmDAO",lvmDAO);
        RequestDispatcher rd;
        if(!isPermanent)
        {
        	if(hasAjax == false)
	        {
	        	System.out.println("Going to lvm/lvmApproval.jsp");
	        	rd = request.getRequestDispatcher("lvm/lvmApproval.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to lvm/lvmApprovalForm.jsp");
	        	rd = request.getRequestDispatcher("lvm/lvmApprovalForm.jsp");
	        }
        }
        else
        {
	        if(hasAjax == false)
	        {
	        	System.out.println("Going to lvm/lvmSearch.jsp");
	        	rd = request.getRequestDispatcher("lvm/lvmSearch.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to lvm/lvmSearchForm.jsp");
	        	rd = request.getRequestDispatcher("lvm/lvmSearchForm.jsp");
	        }
        }
		rd.forward(request, response);
	}
	
}

