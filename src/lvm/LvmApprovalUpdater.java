package lvm;

import java.util.Date;
import java.util.TimerTask;

import approval_path.ApprovalPathDetailsDAO;
import approval_summary.Approval_summaryDAO;

public class LvmApprovalUpdater extends TimerTask{
	
	public static int LEAVE_STARTED = 0;
	public static int LEAVE_ENDED = 1;
	
	private final int taskType;
	private final long organogramToReplace;
	private final long replaceWith;
	ApprovalPathDetailsDAO approvalPathDetailsDAO = new ApprovalPathDetailsDAO();
	Approval_summaryDAO approval_summaryDAO = new Approval_summaryDAO();
	LvmApprovalUpdater(int taskType, long organogramToReplace, long replaceWith)
	{
		this.taskType = taskType;
		this.organogramToReplace = organogramToReplace;
		this.replaceWith = replaceWith;
	}
	
	 @Override
	    public void run() {
	        System.out.println("Timer task started at:"+new Date());
	        System.out.println("taskType = " + taskType);
	        
	        if(taskType == LEAVE_STARTED)
	        {
	        	approvalPathDetailsDAO.replaceForLeaveStarting(organogramToReplace, replaceWith);
	        	approval_summaryDAO.replaceForLeaveStarting(organogramToReplace, replaceWith);
	        }
	        if(taskType == LEAVE_ENDED)
	        {
	        	approvalPathDetailsDAO.replaceForLeaveEnding(organogramToReplace);
	        	approval_summaryDAO.replaceForLeaveEnding(organogramToReplace);
	        }
	        System.out.println("Timer task finished at:"+new Date());
	    }

}
