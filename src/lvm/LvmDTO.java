package lvm;
import java.util.*; 
import util.*; 


public class LvmDTO extends CommonDTO
{

	public long employeeInLeaveType = 0;
	public long employeeInLeaveOrganogramId = 0;
    public String employeeInLeaveName = "";
    public String employeeInLeaveDesignation = "";
	public long employeeInLeaveOfficeUnitId = 0;
    public String employeeInLeaveOfficeUnitName = "";
	public long employeeInChargeType = 0;
	public long employeeInChargeOrganogramId = 0;
    public String employeeInChargeName = "";
    public String employeeInChargeDesignation = "";
	public long employeeInChargeOfficeUnitId = 0;
    public String employeeInChargeOfficeUnitName = "";
	public long employeeInChargePolicyRolesType = 0;
	public long leaveStartDate = 0;
	public long leaveEndDate = 0;
	public long leaveEntryDate = 0;
	public int leaveStatusCat = 0;
	
	
    @Override
	public String toString() {
            return "$LvmDTO[" +
            " iD = " + iD +
            " employeeInLeaveType = " + employeeInLeaveType +
            " employeeInLeaveOrganogramId = " + employeeInLeaveOrganogramId +
            " employeeInLeaveName = " + employeeInLeaveName +
            " employeeInLeaveDesignation = " + employeeInLeaveDesignation +
            " employeeInLeaveOfficeUnitId = " + employeeInLeaveOfficeUnitId +
            " employeeInLeaveOfficeUnitName = " + employeeInLeaveOfficeUnitName +
            " employeeInChargeType = " + employeeInChargeType +
            " employeeInChargeOrganogramId = " + employeeInChargeOrganogramId +
            " employeeInChargeName = " + employeeInChargeName +
            " employeeInChargeDesignation = " + employeeInChargeDesignation +
            " employeeInChargeOfficeUnitId = " + employeeInChargeOfficeUnitId +
            " employeeInChargeOfficeUnitName = " + employeeInChargeOfficeUnitName +
            " employeeInChargePolicyRolesType = " + employeeInChargePolicyRolesType +
            " leaveStartDate = " + leaveStartDate +
            " leaveEndDate = " + leaveEndDate +
            " leaveEntryDate = " + leaveEntryDate +
            " leaveStatusCat = " + leaveStatusCat +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}