package lvm;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class LvmRepository implements Repository {
	LvmDAO lvmDAO = null;
	
	public void setDAO(LvmDAO lvmDAO)
	{
		this.lvmDAO = lvmDAO;
	}
	
	
	static Logger logger = Logger.getLogger(LvmRepository.class);
	Map<Long, LvmDTO>mapOfLvmDTOToiD;
	Map<Long, Set<LvmDTO> >mapOfLvmDTOToemployeeInLeaveType;
	Map<Long, Set<LvmDTO> >mapOfLvmDTOToemployeeInLeaveOrganogramId;
	Map<String, Set<LvmDTO> >mapOfLvmDTOToemployeeInLeaveName;
	Map<String, Set<LvmDTO> >mapOfLvmDTOToemployeeInLeaveDesignation;
	Map<Long, Set<LvmDTO> >mapOfLvmDTOToemployeeInLeaveOfficeUnitId;
	Map<String, Set<LvmDTO> >mapOfLvmDTOToemployeeInLeaveOfficeUnitName;
	Map<Long, Set<LvmDTO> >mapOfLvmDTOToemployeeInChargeType;
	Map<Long, Set<LvmDTO> >mapOfLvmDTOToemployeeInChargeOrganogramId;
	Map<String, Set<LvmDTO> >mapOfLvmDTOToemployeeInChargeName;
	Map<String, Set<LvmDTO> >mapOfLvmDTOToemployeeInChargeDesignation;
	Map<Long, Set<LvmDTO> >mapOfLvmDTOToemployeeInChargeOfficeUnitId;
	Map<String, Set<LvmDTO> >mapOfLvmDTOToemployeeInChargeOfficeUnitName;
	Map<Long, Set<LvmDTO> >mapOfLvmDTOToemployeeInChargePolicyRolesType;
	Map<Long, Set<LvmDTO> >mapOfLvmDTOToleaveStartDate;
	Map<Long, Set<LvmDTO> >mapOfLvmDTOToleaveEndDate;
	Map<Long, Set<LvmDTO> >mapOfLvmDTOToleaveEntryDate;
	Map<Integer, Set<LvmDTO> >mapOfLvmDTOToleaveStatusCat;
	Map<Long, Set<LvmDTO> >mapOfLvmDTOTolastModificationTime;


	static LvmRepository instance = null;  
	private LvmRepository(){
		mapOfLvmDTOToiD = new ConcurrentHashMap<>();
		mapOfLvmDTOToemployeeInLeaveType = new ConcurrentHashMap<>();
		mapOfLvmDTOToemployeeInLeaveOrganogramId = new ConcurrentHashMap<>();
		mapOfLvmDTOToemployeeInLeaveName = new ConcurrentHashMap<>();
		mapOfLvmDTOToemployeeInLeaveDesignation = new ConcurrentHashMap<>();
		mapOfLvmDTOToemployeeInLeaveOfficeUnitId = new ConcurrentHashMap<>();
		mapOfLvmDTOToemployeeInLeaveOfficeUnitName = new ConcurrentHashMap<>();
		mapOfLvmDTOToemployeeInChargeType = new ConcurrentHashMap<>();
		mapOfLvmDTOToemployeeInChargeOrganogramId = new ConcurrentHashMap<>();
		mapOfLvmDTOToemployeeInChargeName = new ConcurrentHashMap<>();
		mapOfLvmDTOToemployeeInChargeDesignation = new ConcurrentHashMap<>();
		mapOfLvmDTOToemployeeInChargeOfficeUnitId = new ConcurrentHashMap<>();
		mapOfLvmDTOToemployeeInChargeOfficeUnitName = new ConcurrentHashMap<>();
		mapOfLvmDTOToemployeeInChargePolicyRolesType = new ConcurrentHashMap<>();
		mapOfLvmDTOToleaveStartDate = new ConcurrentHashMap<>();
		mapOfLvmDTOToleaveEndDate = new ConcurrentHashMap<>();
		mapOfLvmDTOToleaveEntryDate = new ConcurrentHashMap<>();
		mapOfLvmDTOToleaveStatusCat = new ConcurrentHashMap<>();
		mapOfLvmDTOTolastModificationTime = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static LvmRepository getInstance(){
		if (instance == null){
			instance = new LvmRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(lvmDAO == null)
		{
			return;
		}
		try {
			List<LvmDTO> lvmDTOs = lvmDAO.getAllLvm(reloadAll);
			for(LvmDTO lvmDTO : lvmDTOs) {
				LvmDTO oldLvmDTO = getLvmDTOByID(lvmDTO.iD);
				if( oldLvmDTO != null ) {
					mapOfLvmDTOToiD.remove(oldLvmDTO.iD);
				
					if(mapOfLvmDTOToemployeeInLeaveType.containsKey(oldLvmDTO.employeeInLeaveType)) {
						mapOfLvmDTOToemployeeInLeaveType.get(oldLvmDTO.employeeInLeaveType).remove(oldLvmDTO);
					}
					if(mapOfLvmDTOToemployeeInLeaveType.get(oldLvmDTO.employeeInLeaveType).isEmpty()) {
						mapOfLvmDTOToemployeeInLeaveType.remove(oldLvmDTO.employeeInLeaveType);
					}
					
					if(mapOfLvmDTOToemployeeInLeaveOrganogramId.containsKey(oldLvmDTO.employeeInLeaveOrganogramId)) {
						mapOfLvmDTOToemployeeInLeaveOrganogramId.get(oldLvmDTO.employeeInLeaveOrganogramId).remove(oldLvmDTO);
					}
					if(mapOfLvmDTOToemployeeInLeaveOrganogramId.get(oldLvmDTO.employeeInLeaveOrganogramId).isEmpty()) {
						mapOfLvmDTOToemployeeInLeaveOrganogramId.remove(oldLvmDTO.employeeInLeaveOrganogramId);
					}
					
					if(mapOfLvmDTOToemployeeInLeaveName.containsKey(oldLvmDTO.employeeInLeaveName)) {
						mapOfLvmDTOToemployeeInLeaveName.get(oldLvmDTO.employeeInLeaveName).remove(oldLvmDTO);
					}
					if(mapOfLvmDTOToemployeeInLeaveName.get(oldLvmDTO.employeeInLeaveName).isEmpty()) {
						mapOfLvmDTOToemployeeInLeaveName.remove(oldLvmDTO.employeeInLeaveName);
					}
					
					if(mapOfLvmDTOToemployeeInLeaveDesignation.containsKey(oldLvmDTO.employeeInLeaveDesignation)) {
						mapOfLvmDTOToemployeeInLeaveDesignation.get(oldLvmDTO.employeeInLeaveDesignation).remove(oldLvmDTO);
					}
					if(mapOfLvmDTOToemployeeInLeaveDesignation.get(oldLvmDTO.employeeInLeaveDesignation).isEmpty()) {
						mapOfLvmDTOToemployeeInLeaveDesignation.remove(oldLvmDTO.employeeInLeaveDesignation);
					}
					
					if(mapOfLvmDTOToemployeeInLeaveOfficeUnitId.containsKey(oldLvmDTO.employeeInLeaveOfficeUnitId)) {
						mapOfLvmDTOToemployeeInLeaveOfficeUnitId.get(oldLvmDTO.employeeInLeaveOfficeUnitId).remove(oldLvmDTO);
					}
					if(mapOfLvmDTOToemployeeInLeaveOfficeUnitId.get(oldLvmDTO.employeeInLeaveOfficeUnitId).isEmpty()) {
						mapOfLvmDTOToemployeeInLeaveOfficeUnitId.remove(oldLvmDTO.employeeInLeaveOfficeUnitId);
					}
					
					if(mapOfLvmDTOToemployeeInLeaveOfficeUnitName.containsKey(oldLvmDTO.employeeInLeaveOfficeUnitName)) {
						mapOfLvmDTOToemployeeInLeaveOfficeUnitName.get(oldLvmDTO.employeeInLeaveOfficeUnitName).remove(oldLvmDTO);
					}
					if(mapOfLvmDTOToemployeeInLeaveOfficeUnitName.get(oldLvmDTO.employeeInLeaveOfficeUnitName).isEmpty()) {
						mapOfLvmDTOToemployeeInLeaveOfficeUnitName.remove(oldLvmDTO.employeeInLeaveOfficeUnitName);
					}
					
					if(mapOfLvmDTOToemployeeInChargeType.containsKey(oldLvmDTO.employeeInChargeType)) {
						mapOfLvmDTOToemployeeInChargeType.get(oldLvmDTO.employeeInChargeType).remove(oldLvmDTO);
					}
					if(mapOfLvmDTOToemployeeInChargeType.get(oldLvmDTO.employeeInChargeType).isEmpty()) {
						mapOfLvmDTOToemployeeInChargeType.remove(oldLvmDTO.employeeInChargeType);
					}
					
					if(mapOfLvmDTOToemployeeInChargeOrganogramId.containsKey(oldLvmDTO.employeeInChargeOrganogramId)) {
						mapOfLvmDTOToemployeeInChargeOrganogramId.get(oldLvmDTO.employeeInChargeOrganogramId).remove(oldLvmDTO);
					}
					if(mapOfLvmDTOToemployeeInChargeOrganogramId.get(oldLvmDTO.employeeInChargeOrganogramId).isEmpty()) {
						mapOfLvmDTOToemployeeInChargeOrganogramId.remove(oldLvmDTO.employeeInChargeOrganogramId);
					}
					
					if(mapOfLvmDTOToemployeeInChargeName.containsKey(oldLvmDTO.employeeInChargeName)) {
						mapOfLvmDTOToemployeeInChargeName.get(oldLvmDTO.employeeInChargeName).remove(oldLvmDTO);
					}
					if(mapOfLvmDTOToemployeeInChargeName.get(oldLvmDTO.employeeInChargeName).isEmpty()) {
						mapOfLvmDTOToemployeeInChargeName.remove(oldLvmDTO.employeeInChargeName);
					}
					
					if(mapOfLvmDTOToemployeeInChargeDesignation.containsKey(oldLvmDTO.employeeInChargeDesignation)) {
						mapOfLvmDTOToemployeeInChargeDesignation.get(oldLvmDTO.employeeInChargeDesignation).remove(oldLvmDTO);
					}
					if(mapOfLvmDTOToemployeeInChargeDesignation.get(oldLvmDTO.employeeInChargeDesignation).isEmpty()) {
						mapOfLvmDTOToemployeeInChargeDesignation.remove(oldLvmDTO.employeeInChargeDesignation);
					}
					
					if(mapOfLvmDTOToemployeeInChargeOfficeUnitId.containsKey(oldLvmDTO.employeeInChargeOfficeUnitId)) {
						mapOfLvmDTOToemployeeInChargeOfficeUnitId.get(oldLvmDTO.employeeInChargeOfficeUnitId).remove(oldLvmDTO);
					}
					if(mapOfLvmDTOToemployeeInChargeOfficeUnitId.get(oldLvmDTO.employeeInChargeOfficeUnitId).isEmpty()) {
						mapOfLvmDTOToemployeeInChargeOfficeUnitId.remove(oldLvmDTO.employeeInChargeOfficeUnitId);
					}
					
					if(mapOfLvmDTOToemployeeInChargeOfficeUnitName.containsKey(oldLvmDTO.employeeInChargeOfficeUnitName)) {
						mapOfLvmDTOToemployeeInChargeOfficeUnitName.get(oldLvmDTO.employeeInChargeOfficeUnitName).remove(oldLvmDTO);
					}
					if(mapOfLvmDTOToemployeeInChargeOfficeUnitName.get(oldLvmDTO.employeeInChargeOfficeUnitName).isEmpty()) {
						mapOfLvmDTOToemployeeInChargeOfficeUnitName.remove(oldLvmDTO.employeeInChargeOfficeUnitName);
					}
					
					if(mapOfLvmDTOToemployeeInChargePolicyRolesType.containsKey(oldLvmDTO.employeeInChargePolicyRolesType)) {
						mapOfLvmDTOToemployeeInChargePolicyRolesType.get(oldLvmDTO.employeeInChargePolicyRolesType).remove(oldLvmDTO);
					}
					if(mapOfLvmDTOToemployeeInChargePolicyRolesType.get(oldLvmDTO.employeeInChargePolicyRolesType).isEmpty()) {
						mapOfLvmDTOToemployeeInChargePolicyRolesType.remove(oldLvmDTO.employeeInChargePolicyRolesType);
					}
					
					if(mapOfLvmDTOToleaveStartDate.containsKey(oldLvmDTO.leaveStartDate)) {
						mapOfLvmDTOToleaveStartDate.get(oldLvmDTO.leaveStartDate).remove(oldLvmDTO);
					}
					if(mapOfLvmDTOToleaveStartDate.get(oldLvmDTO.leaveStartDate).isEmpty()) {
						mapOfLvmDTOToleaveStartDate.remove(oldLvmDTO.leaveStartDate);
					}
					
					if(mapOfLvmDTOToleaveEndDate.containsKey(oldLvmDTO.leaveEndDate)) {
						mapOfLvmDTOToleaveEndDate.get(oldLvmDTO.leaveEndDate).remove(oldLvmDTO);
					}
					if(mapOfLvmDTOToleaveEndDate.get(oldLvmDTO.leaveEndDate).isEmpty()) {
						mapOfLvmDTOToleaveEndDate.remove(oldLvmDTO.leaveEndDate);
					}
					
					if(mapOfLvmDTOToleaveEntryDate.containsKey(oldLvmDTO.leaveEntryDate)) {
						mapOfLvmDTOToleaveEntryDate.get(oldLvmDTO.leaveEntryDate).remove(oldLvmDTO);
					}
					if(mapOfLvmDTOToleaveEntryDate.get(oldLvmDTO.leaveEntryDate).isEmpty()) {
						mapOfLvmDTOToleaveEntryDate.remove(oldLvmDTO.leaveEntryDate);
					}
					
					if(mapOfLvmDTOToleaveStatusCat.containsKey(oldLvmDTO.leaveStatusCat)) {
						mapOfLvmDTOToleaveStatusCat.get(oldLvmDTO.leaveStatusCat).remove(oldLvmDTO);
					}
					if(mapOfLvmDTOToleaveStatusCat.get(oldLvmDTO.leaveStatusCat).isEmpty()) {
						mapOfLvmDTOToleaveStatusCat.remove(oldLvmDTO.leaveStatusCat);
					}
					
					if(mapOfLvmDTOTolastModificationTime.containsKey(oldLvmDTO.lastModificationTime)) {
						mapOfLvmDTOTolastModificationTime.get(oldLvmDTO.lastModificationTime).remove(oldLvmDTO);
					}
					if(mapOfLvmDTOTolastModificationTime.get(oldLvmDTO.lastModificationTime).isEmpty()) {
						mapOfLvmDTOTolastModificationTime.remove(oldLvmDTO.lastModificationTime);
					}
					
					
				}
				if(lvmDTO.isDeleted == 0) 
				{
					
					mapOfLvmDTOToiD.put(lvmDTO.iD, lvmDTO);
				
					if( ! mapOfLvmDTOToemployeeInLeaveType.containsKey(lvmDTO.employeeInLeaveType)) {
						mapOfLvmDTOToemployeeInLeaveType.put(lvmDTO.employeeInLeaveType, new HashSet<>());
					}
					mapOfLvmDTOToemployeeInLeaveType.get(lvmDTO.employeeInLeaveType).add(lvmDTO);
					
					if( ! mapOfLvmDTOToemployeeInLeaveOrganogramId.containsKey(lvmDTO.employeeInLeaveOrganogramId)) {
						mapOfLvmDTOToemployeeInLeaveOrganogramId.put(lvmDTO.employeeInLeaveOrganogramId, new HashSet<>());
					}
					mapOfLvmDTOToemployeeInLeaveOrganogramId.get(lvmDTO.employeeInLeaveOrganogramId).add(lvmDTO);
					
					if( ! mapOfLvmDTOToemployeeInLeaveName.containsKey(lvmDTO.employeeInLeaveName)) {
						mapOfLvmDTOToemployeeInLeaveName.put(lvmDTO.employeeInLeaveName, new HashSet<>());
					}
					mapOfLvmDTOToemployeeInLeaveName.get(lvmDTO.employeeInLeaveName).add(lvmDTO);
					
					if( ! mapOfLvmDTOToemployeeInLeaveDesignation.containsKey(lvmDTO.employeeInLeaveDesignation)) {
						mapOfLvmDTOToemployeeInLeaveDesignation.put(lvmDTO.employeeInLeaveDesignation, new HashSet<>());
					}
					mapOfLvmDTOToemployeeInLeaveDesignation.get(lvmDTO.employeeInLeaveDesignation).add(lvmDTO);
					
					if( ! mapOfLvmDTOToemployeeInLeaveOfficeUnitId.containsKey(lvmDTO.employeeInLeaveOfficeUnitId)) {
						mapOfLvmDTOToemployeeInLeaveOfficeUnitId.put(lvmDTO.employeeInLeaveOfficeUnitId, new HashSet<>());
					}
					mapOfLvmDTOToemployeeInLeaveOfficeUnitId.get(lvmDTO.employeeInLeaveOfficeUnitId).add(lvmDTO);
					
					if( ! mapOfLvmDTOToemployeeInLeaveOfficeUnitName.containsKey(lvmDTO.employeeInLeaveOfficeUnitName)) {
						mapOfLvmDTOToemployeeInLeaveOfficeUnitName.put(lvmDTO.employeeInLeaveOfficeUnitName, new HashSet<>());
					}
					mapOfLvmDTOToemployeeInLeaveOfficeUnitName.get(lvmDTO.employeeInLeaveOfficeUnitName).add(lvmDTO);
					
					if( ! mapOfLvmDTOToemployeeInChargeType.containsKey(lvmDTO.employeeInChargeType)) {
						mapOfLvmDTOToemployeeInChargeType.put(lvmDTO.employeeInChargeType, new HashSet<>());
					}
					mapOfLvmDTOToemployeeInChargeType.get(lvmDTO.employeeInChargeType).add(lvmDTO);
					
					if( ! mapOfLvmDTOToemployeeInChargeOrganogramId.containsKey(lvmDTO.employeeInChargeOrganogramId)) {
						mapOfLvmDTOToemployeeInChargeOrganogramId.put(lvmDTO.employeeInChargeOrganogramId, new HashSet<>());
					}
					mapOfLvmDTOToemployeeInChargeOrganogramId.get(lvmDTO.employeeInChargeOrganogramId).add(lvmDTO);
					
					if( ! mapOfLvmDTOToemployeeInChargeName.containsKey(lvmDTO.employeeInChargeName)) {
						mapOfLvmDTOToemployeeInChargeName.put(lvmDTO.employeeInChargeName, new HashSet<>());
					}
					mapOfLvmDTOToemployeeInChargeName.get(lvmDTO.employeeInChargeName).add(lvmDTO);
					
					if( ! mapOfLvmDTOToemployeeInChargeDesignation.containsKey(lvmDTO.employeeInChargeDesignation)) {
						mapOfLvmDTOToemployeeInChargeDesignation.put(lvmDTO.employeeInChargeDesignation, new HashSet<>());
					}
					mapOfLvmDTOToemployeeInChargeDesignation.get(lvmDTO.employeeInChargeDesignation).add(lvmDTO);
					
					if( ! mapOfLvmDTOToemployeeInChargeOfficeUnitId.containsKey(lvmDTO.employeeInChargeOfficeUnitId)) {
						mapOfLvmDTOToemployeeInChargeOfficeUnitId.put(lvmDTO.employeeInChargeOfficeUnitId, new HashSet<>());
					}
					mapOfLvmDTOToemployeeInChargeOfficeUnitId.get(lvmDTO.employeeInChargeOfficeUnitId).add(lvmDTO);
					
					if( ! mapOfLvmDTOToemployeeInChargeOfficeUnitName.containsKey(lvmDTO.employeeInChargeOfficeUnitName)) {
						mapOfLvmDTOToemployeeInChargeOfficeUnitName.put(lvmDTO.employeeInChargeOfficeUnitName, new HashSet<>());
					}
					mapOfLvmDTOToemployeeInChargeOfficeUnitName.get(lvmDTO.employeeInChargeOfficeUnitName).add(lvmDTO);
					
					if( ! mapOfLvmDTOToemployeeInChargePolicyRolesType.containsKey(lvmDTO.employeeInChargePolicyRolesType)) {
						mapOfLvmDTOToemployeeInChargePolicyRolesType.put(lvmDTO.employeeInChargePolicyRolesType, new HashSet<>());
					}
					mapOfLvmDTOToemployeeInChargePolicyRolesType.get(lvmDTO.employeeInChargePolicyRolesType).add(lvmDTO);
					
					if( ! mapOfLvmDTOToleaveStartDate.containsKey(lvmDTO.leaveStartDate)) {
						mapOfLvmDTOToleaveStartDate.put(lvmDTO.leaveStartDate, new HashSet<>());
					}
					mapOfLvmDTOToleaveStartDate.get(lvmDTO.leaveStartDate).add(lvmDTO);
					
					if( ! mapOfLvmDTOToleaveEndDate.containsKey(lvmDTO.leaveEndDate)) {
						mapOfLvmDTOToleaveEndDate.put(lvmDTO.leaveEndDate, new HashSet<>());
					}
					mapOfLvmDTOToleaveEndDate.get(lvmDTO.leaveEndDate).add(lvmDTO);
					
					if( ! mapOfLvmDTOToleaveEntryDate.containsKey(lvmDTO.leaveEntryDate)) {
						mapOfLvmDTOToleaveEntryDate.put(lvmDTO.leaveEntryDate, new HashSet<>());
					}
					mapOfLvmDTOToleaveEntryDate.get(lvmDTO.leaveEntryDate).add(lvmDTO);
					
					if( ! mapOfLvmDTOToleaveStatusCat.containsKey(lvmDTO.leaveStatusCat)) {
						mapOfLvmDTOToleaveStatusCat.put(lvmDTO.leaveStatusCat, new HashSet<>());
					}
					mapOfLvmDTOToleaveStatusCat.get(lvmDTO.leaveStatusCat).add(lvmDTO);
					
					if( ! mapOfLvmDTOTolastModificationTime.containsKey(lvmDTO.lastModificationTime)) {
						mapOfLvmDTOTolastModificationTime.put(lvmDTO.lastModificationTime, new HashSet<>());
					}
					mapOfLvmDTOTolastModificationTime.get(lvmDTO.lastModificationTime).add(lvmDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<LvmDTO> getLvmList() {
		List <LvmDTO> lvms = new ArrayList<LvmDTO>(this.mapOfLvmDTOToiD.values());
		return lvms;
	}
	
	
	public LvmDTO getLvmDTOByID( long ID){
		return mapOfLvmDTOToiD.get(ID);
	}
	
	
	public List<LvmDTO> getLvmDTOByemployee_in_leave_type(long employee_in_leave_type) {
		return new ArrayList<>( mapOfLvmDTOToemployeeInLeaveType.getOrDefault(employee_in_leave_type,new HashSet<>()));
	}
	
	
	public List<LvmDTO> getLvmDTOByemployee_in_leave_organogram_id(long employee_in_leave_organogram_id) {
		return new ArrayList<>( mapOfLvmDTOToemployeeInLeaveOrganogramId.getOrDefault(employee_in_leave_organogram_id,new HashSet<>()));
	}
	
	
	public List<LvmDTO> getLvmDTOByemployee_in_leave_name(String employee_in_leave_name) {
		return new ArrayList<>( mapOfLvmDTOToemployeeInLeaveName.getOrDefault(employee_in_leave_name,new HashSet<>()));
	}
	
	
	public List<LvmDTO> getLvmDTOByemployee_in_leave_designation(String employee_in_leave_designation) {
		return new ArrayList<>( mapOfLvmDTOToemployeeInLeaveDesignation.getOrDefault(employee_in_leave_designation,new HashSet<>()));
	}
	
	
	public List<LvmDTO> getLvmDTOByemployee_in_leave_office_unit_id(long employee_in_leave_office_unit_id) {
		return new ArrayList<>( mapOfLvmDTOToemployeeInLeaveOfficeUnitId.getOrDefault(employee_in_leave_office_unit_id,new HashSet<>()));
	}
	
	
	public List<LvmDTO> getLvmDTOByemployee_in_leave_office_unit_name(String employee_in_leave_office_unit_name) {
		return new ArrayList<>( mapOfLvmDTOToemployeeInLeaveOfficeUnitName.getOrDefault(employee_in_leave_office_unit_name,new HashSet<>()));
	}
	
	
	public List<LvmDTO> getLvmDTOByemployee_in_charge_type(long employee_in_charge_type) {
		return new ArrayList<>( mapOfLvmDTOToemployeeInChargeType.getOrDefault(employee_in_charge_type,new HashSet<>()));
	}
	
	
	public List<LvmDTO> getLvmDTOByemployee_in_charge_organogram_id(long employee_in_charge_organogram_id) {
		return new ArrayList<>( mapOfLvmDTOToemployeeInChargeOrganogramId.getOrDefault(employee_in_charge_organogram_id,new HashSet<>()));
	}
	
	
	public List<LvmDTO> getLvmDTOByemployee_in_charge_name(String employee_in_charge_name) {
		return new ArrayList<>( mapOfLvmDTOToemployeeInChargeName.getOrDefault(employee_in_charge_name,new HashSet<>()));
	}
	
	
	public List<LvmDTO> getLvmDTOByemployee_in_charge_designation(String employee_in_charge_designation) {
		return new ArrayList<>( mapOfLvmDTOToemployeeInChargeDesignation.getOrDefault(employee_in_charge_designation,new HashSet<>()));
	}
	
	
	public List<LvmDTO> getLvmDTOByemployee_in_charge_office_unit_id(long employee_in_charge_office_unit_id) {
		return new ArrayList<>( mapOfLvmDTOToemployeeInChargeOfficeUnitId.getOrDefault(employee_in_charge_office_unit_id,new HashSet<>()));
	}
	
	
	public List<LvmDTO> getLvmDTOByemployee_in_charge_office_unit_name(String employee_in_charge_office_unit_name) {
		return new ArrayList<>( mapOfLvmDTOToemployeeInChargeOfficeUnitName.getOrDefault(employee_in_charge_office_unit_name,new HashSet<>()));
	}
	
	
	public List<LvmDTO> getLvmDTOByemployee_in_charge_policy_roles_type(long employee_in_charge_policy_roles_type) {
		return new ArrayList<>( mapOfLvmDTOToemployeeInChargePolicyRolesType.getOrDefault(employee_in_charge_policy_roles_type,new HashSet<>()));
	}
	
	
	public List<LvmDTO> getLvmDTOByleave_start_date(long leave_start_date) {
		return new ArrayList<>( mapOfLvmDTOToleaveStartDate.getOrDefault(leave_start_date,new HashSet<>()));
	}
	
	
	public List<LvmDTO> getLvmDTOByleave_end_date(long leave_end_date) {
		return new ArrayList<>( mapOfLvmDTOToleaveEndDate.getOrDefault(leave_end_date,new HashSet<>()));
	}
	
	
	public List<LvmDTO> getLvmDTOByleave_entry_date(long leave_entry_date) {
		return new ArrayList<>( mapOfLvmDTOToleaveEntryDate.getOrDefault(leave_entry_date,new HashSet<>()));
	}
	
	
	public List<LvmDTO> getLvmDTOByleave_status_cat(int leave_status_cat) {
		return new ArrayList<>( mapOfLvmDTOToleaveStatusCat.getOrDefault(leave_status_cat,new HashSet<>()));
	}
	
	
	public List<LvmDTO> getLvmDTOBylastModificationTime(long lastModificationTime) {
		return new ArrayList<>( mapOfLvmDTOTolastModificationTime.getOrDefault(lastModificationTime,new HashSet<>()));
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "lvm";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


