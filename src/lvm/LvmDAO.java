package lvm;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;
import sessionmanager.SessionConstants;


import org.apache.log4j.Logger;

import dbm.*;

import repository.RepositoryManager;

import util.*;
import pb.*;
import user.UserDTO;

public class LvmDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());
	
	
	public LvmDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";
		joinSQL += " join employee_records on (" + tableName + ".employee_in_leave_type = employee_records.ID ";
		joinSQL += " or " + tableName + ".employee_in_charge_type = employee_records.ID )";
		joinSQL += " join employee_policy_roles on " + tableName + ".employee_in_charge_policy_roles_type = employee_policy_roles.ID ";

		joinSQL += " join category on (";
		joinSQL += " (" + tableName + ".leave_status_cat = category.value  and category.domain_name = 'leave_status')";
		joinSQL += " )";
		joinSQL += " join language_text on category.language_id = language_text.id";		
		commonMaps = new LvmMAPS(tableName);
	}
	
	public LvmDAO()
	{
		this("lvm");		
	}
	
	
	
	public long add(CommonDTO commonDTO) throws Exception
	{
		
		LvmDTO lvmDTO = (LvmDTO)commonDTO;
		
		Connection connection = null;
		PreparedStatement ps = null;

		long lastModificationTime = System.currentTimeMillis();	

		try{
			connection = DBMW.getInstance().getConnection();
			
			if(connection == null)
			{
				System.out.println("nullconn");
			}

			lvmDTO.iD = DBMW.getInstance().getNextSequenceId(tableName);

			String sql = "INSERT INTO " + tableName;
			
			sql += " (";
			sql += "ID";
			sql += ", ";
			sql += "employee_in_leave_type";
			sql += ", ";
			sql += "employee_in_leave_organogram_id";
			sql += ", ";
			sql += "employee_in_leave_name";
			sql += ", ";
			sql += "employee_in_leave_designation";
			sql += ", ";
			sql += "employee_in_leave_office_unit_id";
			sql += ", ";
			sql += "employee_in_leave_office_unit_name";
			sql += ", ";
			sql += "employee_in_charge_type";
			sql += ", ";
			sql += "employee_in_charge_organogram_id";
			sql += ", ";
			sql += "employee_in_charge_name";
			sql += ", ";
			sql += "employee_in_charge_designation";
			sql += ", ";
			sql += "employee_in_charge_office_unit_id";
			sql += ", ";
			sql += "employee_in_charge_office_unit_name";
			sql += ", ";
			sql += "employee_in_charge_policy_roles_type";
			sql += ", ";
			sql += "leave_start_date";
			sql += ", ";
			sql += "leave_end_date";
			sql += ", ";
			sql += "leave_entry_date";
			sql += ", ";
			sql += "leave_status_cat";
			sql += ", ";
			sql += "isDeleted";
			sql += ", ";
			sql += "lastModificationTime";
		
			sql += ")";
			
			
            sql += " VALUES(";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			
			sql += ")";
				

			ps = connection.prepareStatement(sql);
			
			
			

			int index = 1;

			ps.setObject(index++,lvmDTO.iD);
			ps.setObject(index++,lvmDTO.employeeInLeaveType);
			ps.setObject(index++,lvmDTO.employeeInLeaveOrganogramId);
			ps.setObject(index++,lvmDTO.employeeInLeaveName);
			ps.setObject(index++,lvmDTO.employeeInLeaveDesignation);
			ps.setObject(index++,lvmDTO.employeeInLeaveOfficeUnitId);
			ps.setObject(index++,lvmDTO.employeeInLeaveOfficeUnitName);
			ps.setObject(index++,lvmDTO.employeeInChargeType);
			ps.setObject(index++,lvmDTO.employeeInChargeOrganogramId);
			ps.setObject(index++,lvmDTO.employeeInChargeName);
			ps.setObject(index++,lvmDTO.employeeInChargeDesignation);
			ps.setObject(index++,lvmDTO.employeeInChargeOfficeUnitId);
			ps.setObject(index++,lvmDTO.employeeInChargeOfficeUnitName);
			ps.setObject(index++,lvmDTO.employeeInChargePolicyRolesType);
			ps.setObject(index++,lvmDTO.leaveStartDate);
			ps.setObject(index++,lvmDTO.leaveEndDate);
			ps.setObject(index++,lvmDTO.leaveEntryDate);
			ps.setObject(index++,lvmDTO.leaveStatusCat);
			ps.setObject(index++,lvmDTO.isDeleted);
			ps.setObject(index++, lastModificationTime);
			
			System.out.println(ps);
			ps.execute();
			
			
			recordUpdateTime(connection, lastModificationTime);

		}catch(Exception ex){
			logger.error("",ex);
		}finally{
			try{
				if (ps != null) {
					ps.close();
				}
			} catch(Exception e){}
			try{
				if(connection != null)
				{
					DBMW.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		return lvmDTO.iD;		
	}
		
	

	//need another getter for repository
	public CommonDTO getDTOByID (long ID) throws Exception
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		LvmDTO lvmDTO = null;
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
			
            sql += " WHERE ID=" + ID;
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			if(rs.next()){
				lvmDTO = new LvmDTO();

				lvmDTO.iD = rs.getLong("ID");
				lvmDTO.employeeInLeaveType = rs.getLong("employee_in_leave_type");
				lvmDTO.employeeInLeaveOrganogramId = rs.getLong("employee_in_leave_organogram_id");
				lvmDTO.employeeInLeaveName = rs.getString("employee_in_leave_name");
				lvmDTO.employeeInLeaveDesignation = rs.getString("employee_in_leave_designation");
				lvmDTO.employeeInLeaveOfficeUnitId = rs.getLong("employee_in_leave_office_unit_id");
				lvmDTO.employeeInLeaveOfficeUnitName = rs.getString("employee_in_leave_office_unit_name");
				lvmDTO.employeeInChargeType = rs.getLong("employee_in_charge_type");
				lvmDTO.employeeInChargeOrganogramId = rs.getLong("employee_in_charge_organogram_id");
				lvmDTO.employeeInChargeName = rs.getString("employee_in_charge_name");
				lvmDTO.employeeInChargeDesignation = rs.getString("employee_in_charge_designation");
				lvmDTO.employeeInChargeOfficeUnitId = rs.getLong("employee_in_charge_office_unit_id");
				lvmDTO.employeeInChargeOfficeUnitName = rs.getString("employee_in_charge_office_unit_name");
				lvmDTO.employeeInChargePolicyRolesType = rs.getLong("employee_in_charge_policy_roles_type");
				lvmDTO.leaveStartDate = rs.getLong("leave_start_date");
				lvmDTO.leaveEndDate = rs.getLong("leave_end_date");
				lvmDTO.leaveEntryDate = rs.getLong("leave_entry_date");
				lvmDTO.leaveStatusCat = rs.getInt("leave_status_cat");
				lvmDTO.isDeleted = rs.getInt("isDeleted");
				lvmDTO.lastModificationTime = rs.getLong("lastModificationTime");

			}			
			
			
		}catch(Exception ex){
			logger.error("",ex);
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return lvmDTO;
	}
	
	public CommonDTO getCurrentlyInLeaveDTOByOrganogramId (long organogramID) throws Exception
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		LvmDTO lvmDTO = null;
		try{
			
			String sql = "SELECT * FROM lvm where isDeleted = 0 and employee_in_leave_organogram_id = " + organogramID 
					+ " and leave_start_date <= " + System.currentTimeMillis() 
					+ " and leave_end_date >= " + + System.currentTimeMillis()
					+ " order by leave_start_date desc limit 1";
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			if(rs.next()){
				lvmDTO = new LvmDTO();

				lvmDTO.iD = rs.getLong("ID");
				lvmDTO.employeeInLeaveType = rs.getLong("employee_in_leave_type");
				lvmDTO.employeeInLeaveOrganogramId = rs.getLong("employee_in_leave_organogram_id");
				lvmDTO.employeeInLeaveName = rs.getString("employee_in_leave_name");
				lvmDTO.employeeInLeaveDesignation = rs.getString("employee_in_leave_designation");
				lvmDTO.employeeInLeaveOfficeUnitId = rs.getLong("employee_in_leave_office_unit_id");
				lvmDTO.employeeInLeaveOfficeUnitName = rs.getString("employee_in_leave_office_unit_name");
				lvmDTO.employeeInChargeType = rs.getLong("employee_in_charge_type");
				lvmDTO.employeeInChargeOrganogramId = rs.getLong("employee_in_charge_organogram_id");
				lvmDTO.employeeInChargeName = rs.getString("employee_in_charge_name");
				lvmDTO.employeeInChargeDesignation = rs.getString("employee_in_charge_designation");
				lvmDTO.employeeInChargeOfficeUnitId = rs.getLong("employee_in_charge_office_unit_id");
				lvmDTO.employeeInChargeOfficeUnitName = rs.getString("employee_in_charge_office_unit_name");
				lvmDTO.employeeInChargePolicyRolesType = rs.getLong("employee_in_charge_policy_roles_type");
				lvmDTO.leaveStartDate = rs.getLong("leave_start_date");
				lvmDTO.leaveEndDate = rs.getLong("leave_end_date");
				lvmDTO.leaveEntryDate = rs.getLong("leave_entry_date");
				lvmDTO.leaveStatusCat = rs.getInt("leave_status_cat");
				lvmDTO.isDeleted = rs.getInt("isDeleted");
				lvmDTO.lastModificationTime = rs.getLong("lastModificationTime");

			}			
			
			
		}catch(Exception ex){
			logger.error("",ex);
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return lvmDTO;
	}
	
	public long update(CommonDTO commonDTO) throws Exception
	{		
		LvmDTO lvmDTO = (LvmDTO)commonDTO;
		
		Connection connection = null;
		PreparedStatement ps = null;

		long lastModificationTime = System.currentTimeMillis();	
		try{
			connection = DBMW.getInstance().getConnection();

			String sql = "UPDATE " + tableName;
			
			sql += " SET ";
			sql += "employee_in_leave_type=?";
			sql += ", ";
			sql += "employee_in_leave_organogram_id=?";
			sql += ", ";
			sql += "employee_in_leave_name=?";
			sql += ", ";
			sql += "employee_in_leave_designation=?";
			sql += ", ";
			sql += "employee_in_leave_office_unit_id=?";
			sql += ", ";
			sql += "employee_in_leave_office_unit_name=?";
			sql += ", ";
			sql += "employee_in_charge_type=?";
			sql += ", ";
			sql += "employee_in_charge_organogram_id=?";
			sql += ", ";
			sql += "employee_in_charge_name=?";
			sql += ", ";
			sql += "employee_in_charge_designation=?";
			sql += ", ";
			sql += "employee_in_charge_office_unit_id=?";
			sql += ", ";
			sql += "employee_in_charge_office_unit_name=?";
			sql += ", ";
			sql += "employee_in_charge_policy_roles_type=?";
			sql += ", ";
			sql += "leave_start_date=?";
			sql += ", ";
			sql += "leave_end_date=?";
			sql += ", ";
			sql += "leave_entry_date=?";
			sql += ", ";
			sql += "leave_status_cat=?";
			sql += ", lastModificationTime = "	+ lastModificationTime + "";
            sql += " WHERE ID = " + lvmDTO.iD;
				

			ps = connection.prepareStatement(sql);
			
			

			int index = 1;
			
			ps.setObject(index++,lvmDTO.employeeInLeaveType);
			ps.setObject(index++,lvmDTO.employeeInLeaveOrganogramId);
			ps.setObject(index++,lvmDTO.employeeInLeaveName);
			ps.setObject(index++,lvmDTO.employeeInLeaveDesignation);
			ps.setObject(index++,lvmDTO.employeeInLeaveOfficeUnitId);
			ps.setObject(index++,lvmDTO.employeeInLeaveOfficeUnitName);
			ps.setObject(index++,lvmDTO.employeeInChargeType);
			ps.setObject(index++,lvmDTO.employeeInChargeOrganogramId);
			ps.setObject(index++,lvmDTO.employeeInChargeName);
			ps.setObject(index++,lvmDTO.employeeInChargeDesignation);
			ps.setObject(index++,lvmDTO.employeeInChargeOfficeUnitId);
			ps.setObject(index++,lvmDTO.employeeInChargeOfficeUnitName);
			ps.setObject(index++,lvmDTO.employeeInChargePolicyRolesType);
			ps.setObject(index++,lvmDTO.leaveStartDate);
			ps.setObject(index++,lvmDTO.leaveEndDate);
			ps.setObject(index++,lvmDTO.leaveEntryDate);
			ps.setObject(index++,lvmDTO.leaveStatusCat);
			System.out.println(ps);
			ps.executeUpdate();
			


			
			recordUpdateTime(connection, lastModificationTime);

						
			
		}catch(Exception ex){
			logger.error("",ex);
		}finally{
			try{
				if (ps != null) {
					ps.close();
				}
			} catch(Exception e){}
			try{
				if(connection != null)
				{
					DBMW.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		return lvmDTO.iD;
	}
	
	
	public List<LvmDTO> getDTOs(Collection recordIDs){
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		LvmDTO lvmDTO = null;
		List<LvmDTO> lvmDTOList = new ArrayList<>();
		if(recordIDs.isEmpty()){
			return lvmDTOList;
		}
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
            
            sql += " WHERE ID IN ( ";

			for(int i = 0;i<recordIDs.size();i++){
				if(i!=0){
					sql+=",";
				}
				sql+=((ArrayList)recordIDs).get(i);
			}
			sql+=")  order by lastModificationTime desc";
			
			printSql(sql);
			
			logger.debug("sql " + sql);
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				lvmDTO = new LvmDTO();
				lvmDTO.iD = rs.getLong("ID");
				lvmDTO.employeeInLeaveType = rs.getLong("employee_in_leave_type");
				lvmDTO.employeeInLeaveOrganogramId = rs.getLong("employee_in_leave_organogram_id");
				lvmDTO.employeeInLeaveName = rs.getString("employee_in_leave_name");
				lvmDTO.employeeInLeaveDesignation = rs.getString("employee_in_leave_designation");
				lvmDTO.employeeInLeaveOfficeUnitId = rs.getLong("employee_in_leave_office_unit_id");
				lvmDTO.employeeInLeaveOfficeUnitName = rs.getString("employee_in_leave_office_unit_name");
				lvmDTO.employeeInChargeType = rs.getLong("employee_in_charge_type");
				lvmDTO.employeeInChargeOrganogramId = rs.getLong("employee_in_charge_organogram_id");
				lvmDTO.employeeInChargeName = rs.getString("employee_in_charge_name");
				lvmDTO.employeeInChargeDesignation = rs.getString("employee_in_charge_designation");
				lvmDTO.employeeInChargeOfficeUnitId = rs.getLong("employee_in_charge_office_unit_id");
				lvmDTO.employeeInChargeOfficeUnitName = rs.getString("employee_in_charge_office_unit_name");
				lvmDTO.employeeInChargePolicyRolesType = rs.getLong("employee_in_charge_policy_roles_type");
				lvmDTO.leaveStartDate = rs.getLong("leave_start_date");
				lvmDTO.leaveEndDate = rs.getLong("leave_end_date");
				lvmDTO.leaveEntryDate = rs.getLong("leave_entry_date");
				lvmDTO.leaveStatusCat = rs.getInt("leave_status_cat");
				lvmDTO.isDeleted = rs.getInt("isDeleted");
				lvmDTO.lastModificationTime = rs.getLong("lastModificationTime");
				System.out.println("got this DTO: " + lvmDTO);
				
				lvmDTOList.add(lvmDTO);

			}			
			
		}catch(Exception ex){
			logger.error("",ex);
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return lvmDTOList;
	
	}
	
	

	
	
	
	//add repository
	public List<LvmDTO> getAllLvm (boolean isFirstReload)
    {
		List<LvmDTO> lvmDTOList = new ArrayList<>();

		String sql = "SELECT * FROM lvm";
		sql += " WHERE ";
	

		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by lvm.lastModificationTime desc";
		printSql(sql);
		
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;

		
		try{
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();
			rs = stmt.executeQuery(sql);
			

			while(rs.next()){
				LvmDTO lvmDTO = new LvmDTO();
				lvmDTO.iD = rs.getLong("ID");
				lvmDTO.employeeInLeaveType = rs.getLong("employee_in_leave_type");
				lvmDTO.employeeInLeaveOrganogramId = rs.getLong("employee_in_leave_organogram_id");
				lvmDTO.employeeInLeaveName = rs.getString("employee_in_leave_name");
				lvmDTO.employeeInLeaveDesignation = rs.getString("employee_in_leave_designation");
				lvmDTO.employeeInLeaveOfficeUnitId = rs.getLong("employee_in_leave_office_unit_id");
				lvmDTO.employeeInLeaveOfficeUnitName = rs.getString("employee_in_leave_office_unit_name");
				lvmDTO.employeeInChargeType = rs.getLong("employee_in_charge_type");
				lvmDTO.employeeInChargeOrganogramId = rs.getLong("employee_in_charge_organogram_id");
				lvmDTO.employeeInChargeName = rs.getString("employee_in_charge_name");
				lvmDTO.employeeInChargeDesignation = rs.getString("employee_in_charge_designation");
				lvmDTO.employeeInChargeOfficeUnitId = rs.getLong("employee_in_charge_office_unit_id");
				lvmDTO.employeeInChargeOfficeUnitName = rs.getString("employee_in_charge_office_unit_name");
				lvmDTO.employeeInChargePolicyRolesType = rs.getLong("employee_in_charge_policy_roles_type");
				lvmDTO.leaveStartDate = rs.getLong("leave_start_date");
				lvmDTO.leaveEndDate = rs.getLong("leave_end_date");
				lvmDTO.leaveEntryDate = rs.getLong("leave_entry_date");
				lvmDTO.leaveStatusCat = rs.getInt("leave_status_cat");
				lvmDTO.isDeleted = rs.getInt("isDeleted");
				lvmDTO.lastModificationTime = rs.getLong("lastModificationTime");
				
				lvmDTOList.add(lvmDTO);
			}			
		}catch(Exception ex){
			logger.error("",ex);
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}

		return lvmDTOList;
    }

	
	public List<LvmDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO)
	{
		return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
	}

	
	public List<LvmDTO> getDTOs(Hashtable  p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
			String filter, boolean tableHasJobCat)
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		List<LvmDTO> lvmDTOList = new ArrayList<>();

		try{
			
			String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat);
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				LvmDTO lvmDTO = new LvmDTO();
				lvmDTO.iD = rs.getLong("ID");
				lvmDTO.employeeInLeaveType = rs.getLong("employee_in_leave_type");
				lvmDTO.employeeInLeaveOrganogramId = rs.getLong("employee_in_leave_organogram_id");
				lvmDTO.employeeInLeaveName = rs.getString("employee_in_leave_name");
				lvmDTO.employeeInLeaveDesignation = rs.getString("employee_in_leave_designation");
				lvmDTO.employeeInLeaveOfficeUnitId = rs.getLong("employee_in_leave_office_unit_id");
				lvmDTO.employeeInLeaveOfficeUnitName = rs.getString("employee_in_leave_office_unit_name");
				lvmDTO.employeeInChargeType = rs.getLong("employee_in_charge_type");
				lvmDTO.employeeInChargeOrganogramId = rs.getLong("employee_in_charge_organogram_id");
				lvmDTO.employeeInChargeName = rs.getString("employee_in_charge_name");
				lvmDTO.employeeInChargeDesignation = rs.getString("employee_in_charge_designation");
				lvmDTO.employeeInChargeOfficeUnitId = rs.getLong("employee_in_charge_office_unit_id");
				lvmDTO.employeeInChargeOfficeUnitName = rs.getString("employee_in_charge_office_unit_name");
				lvmDTO.employeeInChargePolicyRolesType = rs.getLong("employee_in_charge_policy_roles_type");
				lvmDTO.leaveStartDate = rs.getLong("leave_start_date");
				lvmDTO.leaveEndDate = rs.getLong("leave_end_date");
				lvmDTO.leaveEntryDate = rs.getLong("leave_entry_date");
				lvmDTO.leaveStatusCat = rs.getInt("leave_status_cat");
				lvmDTO.isDeleted = rs.getInt("isDeleted");
				lvmDTO.lastModificationTime = rs.getLong("lastModificationTime");
				
				lvmDTOList.add(lvmDTO);
			}					
			
		}catch(Exception ex){
			logger.error("",ex);
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null)
				{ 
					DBMR.getInstance().freeConnection(connection);
				} 
			}catch(Exception ex2){}
		}
		return lvmDTOList;
	
	}
				
}
	