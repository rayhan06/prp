package am_parliament_building_block;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import am_minister_hostel_block.Am_minister_hostel_blockDTO;
import am_parliament_building_level.Am_parliament_building_levelDTO;
import com.google.gson.Gson;
import economic_code.Economic_codeDTO;
import org.apache.log4j.Logger;

import pb.*;
import repository.Repository;
import repository.RepositoryManager;
import vm_vehicle_driver_assignment.Vm_vehicle_driver_assignmentDTO;


public class Am_parliament_building_blockRepository implements Repository {
	Am_parliament_building_blockDAO am_parliament_building_blockDAO;
	Gson gson = new Gson();
	
	public void setDAO(Am_parliament_building_blockDAO am_parliament_building_blockDAO)
	{
		this.am_parliament_building_blockDAO = am_parliament_building_blockDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Am_parliament_building_blockRepository.class);
	Map<Long, Am_parliament_building_blockDTO>mapOfAm_parliament_building_blockDTOToiD;
	Map<Long, Set<Am_parliament_building_blockDTO> >mapOfAm_parliament_building_blockDTOToLevelId;

  
	private Am_parliament_building_blockRepository(){
		am_parliament_building_blockDAO = Am_parliament_building_blockDAO.getInstance();
		mapOfAm_parliament_building_blockDTOToiD = new ConcurrentHashMap<>();
		mapOfAm_parliament_building_blockDTOToLevelId = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	private static class LazyLoader{
        static final Am_parliament_building_blockRepository INSTANCE = new Am_parliament_building_blockRepository();
    }

    public static Am_parliament_building_blockRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

	public void reload(boolean reloadAll){
		try {
			List<Am_parliament_building_blockDTO> am_parliament_building_blockDTOs = am_parliament_building_blockDAO.getAllDTOs(reloadAll);
			for(Am_parliament_building_blockDTO am_parliament_building_blockDTO : am_parliament_building_blockDTOs) {
				Am_parliament_building_blockDTO oldAm_parliament_building_blockDTO = getAm_parliament_building_blockDTOByIDWithOutClone(am_parliament_building_blockDTO.iD);
				if( oldAm_parliament_building_blockDTO != null ) {
					mapOfAm_parliament_building_blockDTOToiD.remove(oldAm_parliament_building_blockDTO.iD);

					if(mapOfAm_parliament_building_blockDTOToLevelId.containsKey(oldAm_parliament_building_blockDTO.amParliamentBuildingLevelType)) {
						mapOfAm_parliament_building_blockDTOToLevelId.get(oldAm_parliament_building_blockDTO.amParliamentBuildingLevelType).
								remove(oldAm_parliament_building_blockDTO);
					}
					if(mapOfAm_parliament_building_blockDTOToLevelId.get(oldAm_parliament_building_blockDTO.amParliamentBuildingLevelType).isEmpty()) {
						mapOfAm_parliament_building_blockDTOToLevelId.remove(oldAm_parliament_building_blockDTO.amParliamentBuildingLevelType);
					}
				
					
				}
				if(am_parliament_building_blockDTO.isDeleted == 0) 
				{
					
					mapOfAm_parliament_building_blockDTOToiD.put(am_parliament_building_blockDTO.iD, am_parliament_building_blockDTO);

					if( ! mapOfAm_parliament_building_blockDTOToLevelId.containsKey(am_parliament_building_blockDTO.amParliamentBuildingLevelType)) {
						mapOfAm_parliament_building_blockDTOToLevelId.put(am_parliament_building_blockDTO.amParliamentBuildingLevelType, new HashSet<>());
					}
					mapOfAm_parliament_building_blockDTOToLevelId.get(am_parliament_building_blockDTO.amParliamentBuildingLevelType).add(am_parliament_building_blockDTO);
				
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Am_parliament_building_blockDTO> getAm_parliament_building_blockList() {
		return clone(new ArrayList<>(this.mapOfAm_parliament_building_blockDTOToiD.values()));

	}
	
	
	public Am_parliament_building_blockDTO getAm_parliament_building_blockDTOByID( long ID){
		return clone(mapOfAm_parliament_building_blockDTOToiD.get(ID));
	}

	public Am_parliament_building_blockDTO getAm_parliament_building_blockDTOByIDWithOutClone( long ID){
		return mapOfAm_parliament_building_blockDTOToiD.get(ID);
	}

	public List<Am_parliament_building_blockDTO> getAm_parliament_building_blockDTOByLevel_id(long levelId) {
		return clone(new ArrayList<>( mapOfAm_parliament_building_blockDTOToLevelId.getOrDefault(levelId,new HashSet<>())));
	}

	
	@Override
	public String getTableName() {
		return "am_parliament_building_block";
	}

	public Am_parliament_building_blockDTO clone(Am_parliament_building_blockDTO dto) {
		String raw = gson.toJson(dto);
		return gson.fromJson(raw, Am_parliament_building_blockDTO.class);
	}

	public List<Am_parliament_building_blockDTO> clone(List<Am_parliament_building_blockDTO> dtoList) {
		return dtoList
				.stream()
				.map(this::clone)
				.collect(Collectors.toList());
	}

	public String getText(long id, String language) {
		String text = "";
		Am_parliament_building_blockDTO dto = getAm_parliament_building_blockDTOByID(id);
		if(dto != null){
			CategoryLanguageModel model = CatRepository.getInstance().getCategoryLanguageModel
					("parliament_block",dto.parliamentBlockCat);
			if(model != null){
				text = language.equalsIgnoreCase("english") ? model.englishText : model.banglaText;
			}
		}
		return text;
	}

	public String getOptionsByLevelId(String language, long levelId, long defaultValue){
		StringBuilder sOptions =new StringBuilder();
		if (defaultValue == -1)
		{
			if (language.equalsIgnoreCase("English"))
			{
				sOptions.append("<option value = ''>Select</option>");
			}
			else
			{
				sOptions.append("<option value = ''>বাছাই করুন</option>");
			}
		}

		List<Am_parliament_building_blockDTO> dtos = getAm_parliament_building_blockDTOByLevel_id(levelId);

		for(Am_parliament_building_blockDTO dto: dtos){
			CategoryLanguageModel model = CatRepository.getInstance().getCategoryLanguageModel
					("parliament_block",dto.parliamentBlockCat);
			if(model != null){
				StringBuilder sOption =new StringBuilder();
				sOption.append("<option value = '").append( dto.iD).append("'");
				if(defaultValue != CatDTO.CATDEFAULTNOTREQUIRED && dto.iD == defaultValue)
				{
					sOption.append(" selected ");
				}
				sOption.append(">").append(language.equalsIgnoreCase
						("english") ? model.englishText : model.banglaText)
						.append("</option>");
				sOptions.append(sOption);
			}

		}


		return sOptions.toString();

	}

	@Override
	public String getDtoJsonById(long ID) {
		return gson.toJson(getAm_parliament_building_blockDTOByID(ID));
	}
}


