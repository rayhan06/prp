package am_parliament_building_block;

import util.CommonDTO;


public class Am_parliament_building_blockDTO extends CommonDTO
{

	public int parliamentBlockCat = -1;
	public long amParliamentBuildingLevelType = -1;
	public long insertedByUserId = -1;
	public long insertedByOrganogramId = -1;
	public long modifiedByOrganogramId = -1;
	public long insertionDate = -1;
	
	
    @Override
	public String toString() {
            return "$Am_parliament_building_blockDTO[" +
            " iD = " + iD +
            " parliamentBlockCat = " + parliamentBlockCat +
            " amParliamentBuildingLevelType = " + amParliamentBuildingLevelType +
            " insertedByUserId = " + insertedByUserId +
            " insertedByOrganogramId = " + insertedByOrganogramId +
            " modifiedByOrganogramId = " + modifiedByOrganogramId +
            " insertionDate = " + insertionDate +
            " searchColumn = " + searchColumn +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}