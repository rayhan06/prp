package am_parliament_building_block;

import am_house.Am_houseRepository;
import am_parliament_building_level.Am_parliament_building_levelDTO;
import am_parliament_building_level.Am_parliament_building_levelRepository;
import am_parliament_building_room.Am_parliament_building_roomDAO;
import common.CommonDAOService;
import common.ConnectionAndStatementUtil;
import org.apache.log4j.Logger;
import pb.CatRepository;
import pb.CategoryLanguageModel;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class Am_parliament_building_blockDAO  implements CommonDAOService<Am_parliament_building_blockDTO>
{

	private static final Logger logger = Logger.getLogger(Am_parliament_building_blockDAO.class);
	private final Map<String,String> searchMap = new HashMap<>();

	private static final String addQuery = "INSERT INTO {tableName} (inserted_by_user_id,inserted_by_organogram_id," +
			"insertion_date, modified_by, search_column, parliament_block_cat, am_parliament_building_level_type, lastModificationTime," +
			"isDeleted,ID) VALUES (?,?,?,?,?,?,?,?, ?, ?)";

	private static final String updateQuery = "UPDATE {tableName} SET inserted_by_user_id = ?,inserted_by_organogram_id=?," +
			" insertion_date=?, modified_by = ?, search_column=?,parliament_block_cat=?,am_parliament_building_level_type=?," +
			"lastModificationTime=? WHERE ID = ?";

	private static class LazyLoader{
		static final Am_parliament_building_blockDAO INSTANCE = new Am_parliament_building_blockDAO();
	}

	public static Am_parliament_building_blockDAO getInstance(){
		return Am_parliament_building_blockDAO.LazyLoader.INSTANCE;
	}

	private Am_parliament_building_blockDAO() {
		searchMap.put("parliament_block_cat"," and (parliament_block_cat = ?)");
		searchMap.put("am_parliament_building_level_type"," and (am_parliament_building_level_type = ?)");
		searchMap.put("insertion_date_start"," and (insertion_date>= ?)");
		searchMap.put("insertion_date_end"," and (insertion_date <= ?)");
		searchMap.put("AnyField"," and (search_column like ?)");
	}



	@Override
	public void set(PreparedStatement ps, Am_parliament_building_blockDTO am_parliament_building_blockDTO, boolean isInsert) throws SQLException {
		setSearchColumn(am_parliament_building_blockDTO);
		int index = 0;
		ps.setObject(++index, am_parliament_building_blockDTO.insertedByUserId);
		ps.setObject(++index, am_parliament_building_blockDTO.insertedByOrganogramId);
		ps.setObject(++index, am_parliament_building_blockDTO.insertionDate);
		ps.setObject(++index, am_parliament_building_blockDTO.modifiedByOrganogramId);
		ps.setObject(++index, am_parliament_building_blockDTO.searchColumn);
		ps.setObject(++index, am_parliament_building_blockDTO.parliamentBlockCat);
		ps.setObject(++index, am_parliament_building_blockDTO.amParliamentBuildingLevelType);
		ps.setObject(++index, am_parliament_building_blockDTO.lastModificationTime);
		if (isInsert) {
			ps.setObject(++index, 0);
		}
		ps.setObject(++index, am_parliament_building_blockDTO.iD);
	}

	@Override
	public Am_parliament_building_blockDTO buildObjectFromResultSet(ResultSet rs) {
		try
		{
			Am_parliament_building_blockDTO am_parliament_building_blockDTO = new Am_parliament_building_blockDTO();
			am_parliament_building_blockDTO.iD = rs.getLong("ID");
			am_parliament_building_blockDTO.parliamentBlockCat = rs.getInt("parliament_block_cat");
			am_parliament_building_blockDTO.amParliamentBuildingLevelType = rs.getLong("am_parliament_building_level_type");
			am_parliament_building_blockDTO.insertedByUserId = rs.getLong("inserted_by_user_id");
			am_parliament_building_blockDTO.insertedByOrganogramId = rs.getLong("inserted_by_organogram_id");
			am_parliament_building_blockDTO.modifiedByOrganogramId = rs.getLong("modified_by");
			am_parliament_building_blockDTO.insertionDate = rs.getLong("insertion_date");
			am_parliament_building_blockDTO.searchColumn = rs.getString("search_column");
			am_parliament_building_blockDTO.isDeleted = rs.getInt("isDeleted");
			am_parliament_building_blockDTO.lastModificationTime = rs.getLong("lastModificationTime");
			return am_parliament_building_blockDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}

	@Override
	public String getTableName() {
		return "am_parliament_building_block";
	}

	@Override
	public Map<String, String> getSearchMap() {
		return searchMap;
	}

	@Override
	public long add(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Am_parliament_building_blockDTO) commonDTO,addQuery,true);
	}

	@Override
	public long update(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Am_parliament_building_blockDTO) commonDTO,updateQuery,false);
	}


	public void setSearchColumn(Am_parliament_building_blockDTO am_parliament_building_blockDTO)
	{
		am_parliament_building_blockDTO.searchColumn = "";
		CategoryLanguageModel model = CatRepository.getInstance().getCategoryLanguageModel
				("parliament_block",am_parliament_building_blockDTO.parliamentBlockCat);
		if(model != null){
			am_parliament_building_blockDTO.searchColumn += model.englishText+" "+model.banglaText+" ";
		}

		Am_parliament_building_levelDTO levelDTO =  Am_parliament_building_levelRepository.getInstance().getAm_parliament_building_levelDTOByID
				(am_parliament_building_blockDTO.amParliamentBuildingLevelType);
		if(levelDTO != null){
			am_parliament_building_blockDTO.searchColumn += levelDTO.levelNo;
		}
	}

	public int getCountByLevelId(long levelId){
		String countQuery = "SELECT count(*) as countID FROM "
				+ getTableName() + " where isDeleted = 0 and am_parliament_building_level_type = ? ";
		return ConnectionAndStatementUtil.getT(countQuery, Arrays.asList(levelId),rs -> {
			try {
				return rs.getInt("countID");
			} catch (SQLException ex) {
				ex.printStackTrace();
				return 0;
			}
		}, 0);
	}

	public boolean isNotUsed(Long iD){
        return Am_parliament_building_roomDAO.getInstance().getCountByBlockId(iD) <= 0;
    }

	public int getCountOfLevelIdAndBlockCat (long levelId, int blockCat) {
		return (int) Am_parliament_building_blockRepository.getInstance().
				getAm_parliament_building_blockDTOByLevel_id(levelId).
				stream().filter(i -> i.parliamentBlockCat == blockCat).count();
	}
}
	