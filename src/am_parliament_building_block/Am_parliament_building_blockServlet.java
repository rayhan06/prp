package am_parliament_building_block;

import am_parliament_building_level.Am_parliament_building_levelDTO;
import am_parliament_building_level.Am_parliament_building_levelRepository;
import common.ApiResponse;
import common.BaseServlet;
import common.CommonDAOService;
import org.apache.log4j.Logger;
import pb.CatDAO;
import pb.CatRepository;
import pb.CategoryLanguageModel;
import pb.Utils;
import permission.MenuConstants;
import user.UserDTO;
import util.CommonDTO;
import util.HttpRequestUtils;
import util.UtilCharacter;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;


/**
 * Servlet implementation class Am_parliament_building_blockServlet
 */
@WebServlet("/Am_parliament_building_blockServlet")
@MultipartConfig
public class Am_parliament_building_blockServlet extends BaseServlet {
    private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Am_parliament_building_blockServlet.class);
    String domainName = "parliament_block";
    CatDAO catDAO = new CatDAO();


    @Override
    public String getTableName() {
        return Am_parliament_building_blockDAO.getInstance().getTableName();
    }

    @Override
    public String getServletName() {
        return "Am_parliament_building_blockServlet";
    }

    @Override
    public CommonDAOService<? extends CommonDTO> getCommonDAOService() {
        return Am_parliament_building_blockDAO.getInstance();
    }

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {

        String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
        boolean isLanEng = Language.equalsIgnoreCase("English");

        Am_parliament_building_blockDTO am_parliament_building_blockDTO;

        if (Boolean.TRUE.equals(addFlag)) {
            am_parliament_building_blockDTO = new Am_parliament_building_blockDTO();
            am_parliament_building_blockDTO.insertionDate = am_parliament_building_blockDTO.lastModificationTime
                    = System.currentTimeMillis();
            am_parliament_building_blockDTO.insertedByUserId = userDTO.ID;
            am_parliament_building_blockDTO.insertedByOrganogramId = userDTO.organogramID;
        } else {
            am_parliament_building_blockDTO = Am_parliament_building_blockRepository.getInstance().
                    getAm_parliament_building_blockDTOByID(Long.parseLong(request.getParameter("iD")));
            if (am_parliament_building_blockDTO == null) {
                UtilCharacter.throwException("ব্লক খুঁজে পাওয়া যায় নি", "Block not found");
            }

            if (!Am_parliament_building_blockDAO.getInstance().isNotUsed(am_parliament_building_blockDTO.iD)) {
                UtilCharacter.throwException("ইতোমধ্যে ব্যবহৃত, পরিবর্তন সম্ভব নয় ", "Can't Update, Already Used.");
            }


            am_parliament_building_blockDTO.lastModificationTime = System.currentTimeMillis();
            am_parliament_building_blockDTO.modifiedByOrganogramId = userDTO.ID;
        }

        int parliamentBlockCat = Integer.parseInt(request.getParameter
                ("parliamentBlockCat"));

        if (parliamentBlockCat == -2) {

            String otherBlockNameEn = "";
            String otherBlockNameBn = "";

            String Value = request.getParameter("otherBlockEn");
            if (Value == null || Value.equals("") || Value.equals("-1")) {
                throw new Exception(isLanEng ? "Please provide block english name" : "অনুগ্রহপূর্বক ব্লকের ইংরেজি নাম দিন");
            }
            boolean hasNonEnglish =
                    Value.chars()
                            .anyMatch(ch -> !(ch >= ' ' && ch <= '~'));
            if (!hasNonEnglish) {
                otherBlockNameEn = (Value.trim());
            } else {
                throw new Exception(isLanEng ? "Please provide block english name" : "অনুগ্রহপূর্বক ব্লকের ইংরেজি নাম দিন");
            }


            Value = request.getParameter("otherBlockBn");

            if (Value == null || Value.equals("") || Value.equals("-1")) {
                throw new Exception(isLanEng ? "Please provide block bangla name" : "অনুগ্রহপূর্বক ব্লকের বাংলা নাম দিন");
            }

            boolean hasAllBangla = Value.chars().allMatch(Utils::isValidBanglaCharacter);
            if (!hasAllBangla) {
                throw new Exception(isLanEng ? "Please provide block bangla name" : "অনুগ্রহপূর্বক ব্লকের বাংলা নাম দিন");
            }

            otherBlockNameBn = (Value.trim());

            List<CategoryLanguageModel> categoryLanguageModelList = catDAO.getCategoryLanguageModelListByDomainName(domainName);

            String finalOtherBlockNameEn = otherBlockNameEn;
            String finalOtherBlockNameBn = otherBlockNameBn;
            boolean isBlockAlreadyExist = categoryLanguageModelList.stream().
                    anyMatch(e -> e.englishText.equals(finalOtherBlockNameEn) || e.banglaText.equals(finalOtherBlockNameBn));

            if(isBlockAlreadyExist) throw new Exception(isLanEng ? "Other block name already exist" :
                    "অন্যান্য ব্লকের নাম ইতোমধ্যে যোগ করা হয়েছে");

            am_parliament_building_blockDTO.parliamentBlockCat = CatDAO.addFromOthers(domainName, otherBlockNameEn, otherBlockNameBn);
        } else {
            am_parliament_building_blockDTO.parliamentBlockCat = parliamentBlockCat;
            CategoryLanguageModel categoryLanguageModel = CatRepository.getInstance().getCategoryLanguageModel
                    (domainName, am_parliament_building_blockDTO.parliamentBlockCat);
            if (categoryLanguageModel == null) {
                UtilCharacter.throwException("ব্লক খুঁজে পাওয়া যায় নি", "Block not found");
            }
        }


        am_parliament_building_blockDTO.amParliamentBuildingLevelType = Long.parseLong(request.getParameter
                ("amParliamentBuildingLevelType"));

        Am_parliament_building_levelDTO levelDTO = Am_parliament_building_levelRepository.getInstance().
                getAm_parliament_building_levelDTOByID(am_parliament_building_blockDTO.amParliamentBuildingLevelType);
        if (levelDTO == null) {
            UtilCharacter.throwException("লেভেল খুঁজে পাওয়া যায় নি", "Level not found");
        }

        if (!uniquenessCheck(am_parliament_building_blockDTO.amParliamentBuildingLevelType, am_parliament_building_blockDTO.parliamentBlockCat,
                addFlag, am_parliament_building_blockDTO.iD)) {
            UtilCharacter.throwException("লেভেল এবং ব্লক কম্বিনেশন ইউনিক নয় ",
                    "Level and Block combination not unique");
        }

        if (Boolean.TRUE.equals(addFlag)) {
            getCommonDAOService().add(am_parliament_building_blockDTO);
        } else {
            getCommonDAOService().update(am_parliament_building_blockDTO);
        }
        return am_parliament_building_blockDTO;
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[]{MenuConstants.AM_PARLIAMENT_BUILDING_BLOCK_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[]{MenuConstants.AM_PARLIAMENT_BUILDING_BLOCK_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[]{MenuConstants.AM_PARLIAMENT_BUILDING_BLOCK_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Am_parliament_building_blockServlet.class;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String actionType = request.getParameter("actionType");
        if ("getBlockByLevelId".equals(actionType)) {
            try {
                long levelId = Long.parseLong(request.getParameter("levelId"));
                long defaultValue = Long.parseLong(request.getParameter("defaultValue"));
                String language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
                String responseText = Am_parliament_building_blockRepository.getInstance().
                        getOptionsByLevelId(language, levelId, defaultValue);
                ApiResponse.sendSuccessResponse(response, responseText);
            } catch (Exception ex) {
                logger.error(ex);
                ApiResponse.sendErrorResponse(response, ex.getMessage());
            }
            return;
        }
        super.doGet(request, response);
    }

    @Override
    protected void deleteT(HttpServletRequest request, UserDTO userDTO) {
        String[] IDsToDelete = request.getParameterValues("ID");
        if (IDsToDelete.length > 0) {
            List<Long> ids = Stream.of(IDsToDelete)
                    .map(Long::parseLong)
                    .filter(i -> Am_parliament_building_blockDAO.getInstance().isNotUsed(i))
                    .collect(Collectors.toList());
            if (!ids.isEmpty()) {
                getCommonDAOService().deletePb(userDTO.employee_record_id, ids);
            }
        }
    }

    private boolean uniquenessCheck(long levelId, int blockCat, boolean addFlag, Long ID) {
        boolean flag = false;
        int count = Am_parliament_building_blockDAO.getInstance().getCountOfLevelIdAndBlockCat(levelId, blockCat);
        if (addFlag) {
            if (count == 0) {
                flag = true;
            }
        } else {
            Am_parliament_building_blockDTO blockDTO = Am_parliament_building_blockRepository.getInstance()
                    .getAm_parliament_building_blockDTOByID(ID);
            if (blockDTO != null) {
                Boolean sameValueSaveLogic = blockDTO.amParliamentBuildingLevelType == levelId &&
                        blockDTO.parliamentBlockCat == blockCat && count == 1;
                Boolean differentValueSaveLogic = !(blockDTO.amParliamentBuildingLevelType == levelId &&
                        blockDTO.parliamentBlockCat == blockCat) && count == 0;
                if (sameValueSaveLogic || differentValueSaveLogic) {
                    flag = true;
                }
            }
        }

        return flag;
    }
}

