package ticket_type_status_report;


import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import language.LC;
import language.LM;

import login.LoginDTO;
import permission.MenuConstants;

import sessionmanager.SessionConstants;
import support_ticket.Support_ticketDTO;
import user.UserDTO;
import user.UserRepository;

import util.ReportRequestHandler;
import pbReport.*;






@WebServlet("/Ticket_type_status_report_Servlet")
public class Ticket_type_status_report_Servlet  extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	String[][] Criteria =
	{	
		{"criteria","","ticket_issues_type","=","","int","","","any","ticketIssuesType", LC.TICKET_TYPE_STATUS_REPORT_WHERE_TICKETISSUESTYPE + ""},
		{"criteria","","assignment_time",">=","AND","long","","",Long.MIN_VALUE + "","startDate", LC.HM_START_DATE + ""},		
		{"criteria","","assignment_time","<=","AND","long","","",Long.MAX_VALUE + "","endDate", LC.HM_END_DATE + ""}	
	};
	

	
	String[][] Display =
		{
				{"display","","support_engineer_org","organogram",""},	
				{"display","","ticket_issues_type","ticketIssueType",""},	
				{"display","","SUM(CASE \r\n" + 
						"    WHEN done_time <= 0 \r\n" + 
						"    THEN 1 \r\n" + 
						"    ELSE 0 \r\n" + 
						"END)","int",""},		
				{"display","","SUM(CASE \r\n" + 
						"    WHEN done_time > 0 \r\n" + 
						"    THEN 1 \r\n" + 
						"    ELSE 0 \r\n" + 
						"END)","int",""},		
				
		};
	String GroupBy = " ticket_issues_type, support_engineer_org";
	String OrderBY = "";
	
	ReportRequestHandler reportRequestHandler;
	
	public Ticket_type_status_report_Servlet(){

	}
	
	private final ReportService reportService = new ReportService();
	
	private String sql;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String language = "english";
		if(userDTO!= null && userDTO.languageID == SessionConstants.BANGLA)
		{
			language = "bangla";
		}
				
		String actionType = request.getParameter("actionType");

		System.out.println("In ssservlet doget, actiontype = " + actionType);
		
		sql = " support_engineers_diary join support_ticket on  support_engineers_diary.support_ticket_id = support_ticket.id ";
		
		Display[0][4] = LM.getText(LC.HM_NAME, loginDTO);
		Display[1][4] = LM.getText(LC.HM_TYPE, loginDTO);
		Display[2][4] = language.equalsIgnoreCase("english")?"Assigned":"অর্পিত";
		Display[3][4] = language.equalsIgnoreCase("english")?"Solved":"সমাধানকৃত";
		


		
		String reportName = LM.getText(LC.TICKET_TYPE_STATUS_REPORT_OTHER_TICKET_TYPE_STATUS_REPORT, loginDTO);
		
		reportRequestHandler = new ReportRequestHandler(null,
				Criteria, Display, GroupBy, OrderBY, sql,
				reportService);
		
		reportRequestHandler.clientSideDataFormatting = new ArrayList<Pair<Integer, Integer>>();
		reportRequestHandler.clientSideDataFormatting.add(new ImmutablePair<Integer, Integer>(2, ReportRequestHandler.RIGHT_ALIGN_INT));
		reportRequestHandler.clientSideDataFormatting.add(new ImmutablePair<Integer, Integer>(3, ReportRequestHandler.RIGHT_ALIGN_INT));

		
		reportRequestHandler.handleReportGet(request, response, userDTO, "ticket_type_status_report",
				MenuConstants.TICKET_TYPE_STATUS_REPORT_DETAILS, language, reportName, "ticket_type_status_report");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		doGet(request, response);
	}
}
