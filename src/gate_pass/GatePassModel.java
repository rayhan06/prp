package gate_pass;


import sessionmanager.SessionConstants;

import java.util.List;

public class GatePassModel {
    public long gatePassId = 0;
    public long gatePassType = 0;
    public String gatePassTypeEng = "";
    public String gatePassSubTypeEng = "";
    public String gatePassTypeBng = "";
    public String gatePassSubTypeBng = "";
    public String referrerEng = "";
    public String referrerBng = "";
    public String referrerOfficeEng = "";
    public String referrerOfficeBng = "";
    public String visitingDateEng = "";
    public String visitingDateBng = "";
    public long visitingDateLongFormat = 0;
    public String parliamentGateEng = "";
    public String parliamentGateBng = "";
    public String validityFromEng = "";
    public String validityFromBng = "";
    public String validityToEng = "";
    public String validityToBng = "";
    public String visitorName = "";
    public String visitorFatherName = "";
    public String visitorMotherName = "";
    public String visitorCredentialTypeEng = "";
    public String visitorCredentialTypeBng = "";
    public String visitorCredentialNo = "";
    public String visitorMobileNumberEng = "";
    public String visitorMobileNumberBng = "";
    public String visitorEmail = "";
    public String genderTypeEng = "";
    public String officerTypeEng = "";
    public String genderTypeBng = "";
    public String officerTypeBng = "";
    public GatePassVisitorAddress permanentAddress;
    public GatePassVisitorAddress currentAddress;
    public List<GatePassPersonModel> persons = null;
    public List<GatePassItemModel> items = null;
    public int firstLayerApprovalStatus = Gate_passDAO.approvalStatusPending;
    public String firstLayerApprovedBy = "";
    public long firstLayerApprovalTime = SessionConstants.MIN_DATE;
    public int secondLayerApprovalStatus = Gate_passDAO.approvalStatusPending;
    public String secondLayerApprovedBy = "";
    public long secondLayerApprovalTime = SessionConstants.MIN_DATE;
    public byte[] referrerSignature = null;
    public String remarks = "";
    public String referrerOfficeOrgBng = "";
    public String sergeantNameBng = "";
    public String sergeantOfficeBng = "";
    public String sergeantOfficeOrgBng = "";
    public byte[] sergeantSignature = null;
    public long creatorId = 0;
    public String reasonForVisit = "";
    public String cardNumber = "";
    public int isIssued = 0;
    public boolean partiallyIssued = false;
    public String officeName = "";
    public String designation = "";
    
    public String vehicleDescription = "";
    public String vehicleNumber = "";
    public boolean hasVehicle = false;



    @Override
    public String toString() {
        return "$GatePassModel[" +
                " gatePassTypeEng = " + gatePassTypeEng +
                " gatePassTypeBng = " + gatePassTypeBng +
                " gatePassSubTypeEng = " + gatePassSubTypeEng +
                " gatePassSubTypeBng = " + gatePassSubTypeBng +
                " referrerEng = " + referrerEng +
                " referrerBng = " + referrerBng +
                " visitingDateEng = " + visitingDateEng +
                " visitingDateBng = " + visitingDateBng +
                " parliamentGateEng = " + parliamentGateEng +
                " parliamentGateBng = " + parliamentGateBng +
                " validityFromEng = " + validityFromEng +
                " validityFromBng = " + validityFromBng +
                " validityToEng = " + validityToEng +
                " validityToBng = " + validityToBng +
                " visitorName = " + visitorName +
                " visitorFatherName = " + visitorFatherName +
                " visitorMotherName = " + visitorMotherName +
                " visitorMobileNumber = " + visitorMobileNumberEng +
                " visitorCredentialTypeEng = " + visitorCredentialTypeEng +
                " visitorCredentialTypeBng = " + visitorCredentialTypeBng +
                " visitorCredentialNo = " + visitorCredentialNo +
                " visitorMobileNumberBng = " + visitorMobileNumberBng +
                " genderTypeEng = " + genderTypeEng +
                " genderTypeBng = " + genderTypeBng +
                " officerTypeEng = " + officerTypeEng +
                " officerTypeBng = " + officerTypeBng +
                "]";
    }
}
