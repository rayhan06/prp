package gate_pass;

import com.google.gson.Gson;

import employee_offices.EmployeeOfficeDTO;
import employee_offices.EmployeeOfficeRepository;
import employee_records.Employee_recordsDTO;
import employee_records.Employee_recordsRepository;
import gate_pass_item.Gate_pass_itemDAO;
import gate_pass_item.ReceivedItemModel;
import gate_pass_type.Gate_pass_typeDTO;
import language.LC;
import language.LM;
import login.LoginDTO;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import parliament_gallery.Parliament_galleryDAO;
import pb_notifications.Pb_notificationsDAO;
import permission.MenuConstants;
import role.PermissionRepository;
import sessionmanager.SessionConstants;
import task_type.TaskTypeEnum;
import task_type_approval_path.TaskTypeApprovalPathRepository;
import user.UserDTO;
import user.UserRepository;
import util.CommonRequestHandler;
import util.RecordNavigationManager4;
import util.TimeConverter;
import workflow.WorkflowController;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


/**
 * Servlet implementation class Gate_passServlet
 */
@WebServlet("/Gate_passServlet")
@MultipartConfig
public class Gate_passServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Gate_passServlet.class);

    String tableName = "gate_pass";

    Gate_passDAO gate_passDAO;
    Gate_pass_searchModel gate_pass_searchModel;
    CommonRequestHandler commonRequestHandler;
    private final Gson gson = new Gson();
    Pb_notificationsDAO pb_notificationsDAO = Pb_notificationsDAO.getInstance();

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Gate_passServlet() {
        super();
        try {
            gate_passDAO = new Gate_passDAO(tableName);
            commonRequestHandler = new CommonRequestHandler(gate_passDAO);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("In doget request = " + request);
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        boolean isPermanentTable = true;
        if (request.getParameter("isPermanentTable") != null) {
            isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
        }
        System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
        try {
            String actionType = request.getParameter("actionType");
            if (actionType.equals("getAddPage")) {
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.GATE_PASS_ADD)) {

                    HttpSession session = request.getSession(false);
                    session.setAttribute("galleryPassDates", gate_passDAO.buildParliamentSessionDatesForGalleryPass(LM.getText(LC.GATE_PASS_EDIT_LANGUAGE, loginDTO)));

                    commonRequestHandler.getAddPage(request, response);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } 
            
            else if (actionType.equals("getGrade")) {
                long referrerId = Long.parseLong(request.getParameter("referrerId"));
                EmployeeOfficeDTO employeeOfficeDTO = EmployeeOfficeRepository.getInstance().getByEmployeeRecordIdIsDefault(referrerId);
                if(employeeOfficeDTO !=null)
                {
                	logger.debug("employeeOfficeDTO.gradeTypeLevel = " + employeeOfficeDTO.jobGradeTypeCat);
                	response.getWriter().write(employeeOfficeDTO.jobGradeTypeCat + "");
                }
                else
                {
                	logger.debug("employeeOfficeDTO.jobGradeTypeCat = " + null);
                	response.getWriter().write(-1 + "");
                }
            }
            
            else if (actionType.equals("deletePass")) {
                long id = Long.parseLong(request.getParameter("gate_pass_id"));
                gate_passDAO.deleteGate_pass(id);
            } else if (actionType.equals("getEditPage")) {
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.GATE_PASS_UPDATE)) {
                    getGate_pass(request, response);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else if (actionType.equals("getURL")) {
                String URL = request.getParameter("URL");
                System.out.println("URL = " + URL);
                response.sendRedirect(URL);
            } else if (actionType.equals("search")) {
                boolean notification;
                try {
                    notification = Boolean.parseBoolean(request.getParameter("notification"));
                } catch (Exception e) {
                    notification = false;
                    System.out.println("view requested without notification!");
                }
                String filterToPass = "";
                
                if(userDTO.roleID != SessionConstants.ADMIN_ROLE && userDTO.roleID != SessionConstants.GATE_MANAGEMENT_ADMIN_ROLE 
                		&& userDTO.roleID != SessionConstants.PBS_ROLE && userDTO.roleID != SessionConstants.SERJEANT_AT_ARMS_ROLE)
                {
                	filterToPass = " AND (" + tableName + ".referrer_id = " + userDTO.employee_record_id + " OR " + tableName + ".inserted_by = " + userDTO.employee_record_id + ") ";
                }
                
                if(userDTO.roleID == SessionConstants.SERJEANT_AT_ARMS_ROLE)
                {
                	filterToPass = " and first_layer_approval_status = 1 ";
                }
                else if(userDTO.roleID == SessionConstants.PBS_ROLE)
                {
                	filterToPass = " and second_layer_approval_status = 1 and gate_pass_type_id not in ("
                			+ Gate_pass_typeDTO.AREA_PASS + ", " + Gate_pass_typeDTO.MEETING_PASS + ")";
                }

                
                String filter = request.getParameter("filter");
                String day = request.getParameter("day");
                if(filter == null)
                {
                	filter = "";
                }
                if(day == null)
                {
                	day = "";
                }
                if(filter.equals("issued")) {
                	filterToPass += " and is_issued = 1 ";
                	if(day.equals("today"))
                	{
                		filterToPass += " and issue_date >= " + TimeConverter.getToday();
                	}
                	else if(day.equals("month"))
                	{
                		filterToPass += " and issue_date >= " + TimeConverter.get1stDayOfMonth();
                	}
                } else if(filter.equals("approved")) {
                	filterToPass += " and second_layer_approval_status = 1 and is_issued != 1 ";
                	if(day.equals("today"))
                	{
                		filterToPass += " and second_layer_approval_time >= " + TimeConverter.getToday();
                	}
                	else if(day.equals("month"))
                	{
                		filterToPass += " and second_layer_approval_time >= " + TimeConverter.get1stDayOfMonth();
                	}
                } else if(filter.equals("requested")) {
                	if(day.equals("today"))
                	{
                		filterToPass += " and " + tableName + ".insertion_date >= " + TimeConverter.getToday();
                	}
                	else if(day.equals("month"))
                	{
                		filterToPass += " and " + tableName + ".insertion_date >= " + TimeConverter.get1stDayOfMonth();
                	}
                } else if(filter.equals("rejected")) {
                    if(day.equals("today"))
                    {
                        filterToPass += " and " + tableName + ".insertion_date >= " + TimeConverter.getToday();
                    }
                    else if(day.equals("month"))
                    {
                        filterToPass += " and " + tableName + ".insertion_date >= " + TimeConverter.get1stDayOfMonth();
                    }
                    else
                    {
                        filterToPass += " and ( second_layer_approval_status = 2 or first_layer_approval_status = 2 ) " ;
                    }
                }
              

                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.GATE_PASS_SEARCH) || notification) {
                    if (isPermanentTable) {
                        searchGate_pass(request, response, isPermanentTable, filterToPass);
                    } else {
                        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                    }
                }
            } 
            else if (actionType.equals("armsOfficeSearch")) {
                System.out.println("arms office search requested");
                if (gate_passDAO.isOriginalSergeantAtArmsApprover(userDTO)) 
                {
                    String filterToPass  =  " and " + tableName + ".gate_pass_type_id != 1 and " + tableName +
                            ".first_layer_approval_status = " + Gate_passDAO.approvalStatusApproved;

                    String filter = request.getParameter("filter");
                    String day = request.getParameter("day");
                    if(filter == null)
                    {
                        filter = "";
                    }
                    if(day == null)
                    {
                        day = "";
                    }
                    if(filter.equals("issued")) {
                        filterToPass += " and is_issued = 1 ";
                        if(day.equals("today"))
                        {
                            filterToPass += " and issue_date >= " + TimeConverter.getToday();
                        }
                        else if(day.equals("month"))
                        {
                            filterToPass += " and issue_date >= " + TimeConverter.get1stDayOfMonth();
                        }
                    } else if(filter.equals("approved")) {
                        filterToPass += " and second_layer_approval_status = 1 and is_issued != 1 ";
                        if(day.equals("today"))
                        {
                            filterToPass += " and second_layer_approval_time >= " + TimeConverter.getToday() 
                            + " and " + tableName + ".insertion_date >= " + TimeConverter.getToday();
                        }
                        else if(day.equals("month"))
                        {
                            filterToPass += " and second_layer_approval_time >= " + TimeConverter.get1stDayOfMonth() 
                            + " and " + tableName + ".insertion_date >= " + TimeConverter.get1stDayOfMonth();
                        }
                    } else if(filter.equals("requested")) {
                        if(day.equals("today"))
                        {
                            filterToPass += " and " + tableName + ".insertion_date >= " + TimeConverter.getToday();
                        }
                        else if(day.equals("month"))
                        {
                            filterToPass += " and " + tableName + ".insertion_date >= " + TimeConverter.get1stDayOfMonth();
                        }
                    } else if(filter.equals("rejected")) {
                        if(day.equals("today"))
                        {
                            filterToPass += " and " + tableName + ".insertion_date >= " + TimeConverter.getToday();
                        }
                        else if(day.equals("month"))
                        {
                            filterToPass += " and " + tableName + ".insertion_date >= " + TimeConverter.get1stDayOfMonth();
                        }
                        else
                        {
                            filterToPass += " and ( second_layer_approval_status = 2 or first_layer_approval_status = 2 ) " ;
                        }
                    }
                    else if(filter.equals("unapproved")) {
                        if(day.equals("today"))
                        {
                            filterToPass += " and " + tableName + ".insertion_date >= " + TimeConverter.getToday() + " and second_layer_approval_status != 1 ";
                        }
                        else if(day.equals("month"))
                        {
                            filterToPass += " and " + tableName + ".insertion_date >= " + TimeConverter.get1stDayOfMonth() + " and second_layer_approval_status != 1 ";
                        }
                        else
                        {
                            filterToPass += " and second_layer_approval_status != 1 " ;
                        }
                    }


                    armsOfficeSearchGate_pass(request, response, isPermanentTable, filterToPass);
                }
                else 
                {
                	logger.debug("Permission Issue for sea");
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else if (actionType.equals("getApprovalPage")) {
                System.out.println("Gate_pass getApprovalPage requested");
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.GATE_PASS_APPROVE)) {
                    searchGate_pass(request, response, false, "");
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else if (actionType.equals("viewApprovalNotification")) {
                System.out.println("view requested");
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.GATE_PASS_APPROVE)) {
                    commonRequestHandler.viewApprovalNotification(request, response);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else if (actionType.equals("view")) {
                boolean notification;
                try {
                    notification = Boolean.parseBoolean(request.getParameter("notification"));
                } catch (Exception e) {
                    notification = false;
                    System.out.println("view requested without notification!");
                }

                System.out.println("Notification View: " + notification);
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.GATE_PASS_SEARCH) || notification) {
                    commonRequestHandler.view(request, response);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }

            } else if (actionType.equals("visitorPassLookup")) {
                PrintWriter out = response.getWriter();
                out.println(gate_passDAO.getCountByMobileNumber(request.getParameter("mobileNumber")));
                out.close();
            } else if (actionType.equals("parliamentAreaSecurity")) {
                request.getRequestDispatcher("parliament_area_security/parliament_area_securityPage.jsp").forward(request, response);
            } else if (actionType.equals("parliamentBuildingReceiveCard")) {
                request.getRequestDispatcher("parliament_building_security/parliament_building_receive_card.jsp").forward(request, response);
            } else if (actionType.equals("getAddPageAtGate")) {
                request.getRequestDispatcher("parliament_area_security/parliament_area_securityAddGatePass.jsp").forward(request, response);
            } else if (actionType.equals("getAddPageAtReception")) {
                request.getRequestDispatcher("parliament_building_security/parliament_building_securityAddGatePass.jsp").forward(request, response);
            } 
            else if (actionType.equals("parliamentAreaSecuritySearch")) {
//				request.getRequestDispatcher("parliament_area_security/parliament_area_securityEdit.jsp").forward(request, response);
                System.out.println("area security search requested");
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.GATE_PASS_SEARCH)) {
                    String filter = request.getParameter("filter");
                    System.out.println("filter = " + filter);

                    filter = ""; //shouldn't be directly used, rather manipulate it.
                    areaSearchGate_pass(request, response, isPermanentTable, filter);

                }
            }
            else if (actionType.equals("parliamentBuildingSecuritySearch")) {
//				request.getRequestDispatcher("parliament_area_security/parliament_area_securityEdit.jsp").forward(request, response);
                System.out.println("building security search requested");
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.GATE_PASS_SEARCH)) {
                    String filter = request.getParameter("filter");
                    System.out.println("filter = " + filter);

//                    filter =  " and " + tableName + ".gate_pass_type_id != 1 and " + tableName + ".first_layer_approval_status = " + Gate_passDAO.approvalStatusApproved;
                    filter = "";
                    searchGate_pass(request, response, true, "");

                }
            } else if (actionType.equals("parliamentBuildingSecurity")) {
                request.getRequestDispatcher("parliament_building_security/parliament_building_securityPage.jsp").forward(request, response);
            } else if (actionType.equals("armsOfficeView")) {
                request.getRequestDispatcher("gate_pass/gate_pass_arms_officeView.jsp").forward(request, response);
            } else if (actionType.equals("gatePassFirstLayerApprovalFromView")) {
                String approvalStatus = request.getParameter("approvalstatus");
                String notification = request.getParameter("notification");
                String affiliatedPersonsIDs = request.getParameter("personIds");
                long gatePassId = Long.parseLong(request.getParameter("gatePassId"));
                HttpSession session = request.getSession(false);
                session.setAttribute("notificationAfterApproval", approvalStatus);

                Gate_passDTO dto = gate_passDAO.getDTOByID(gatePassId);
                if (approvalStatus.equalsIgnoreCase("approved")) {
                    dto.firstLayerApprovalStatus = Gate_passDAO.approvalStatusApproved;
                    dto.firstLayerApprovalTime = System.currentTimeMillis();
                    assert userDTO != null;
                    dto.firstLayerApprovedBy = userDTO.userName;

                    // Send approval SMS
                    if (dto.gatePassTypeId == 1) {

                        try {
                            gate_passDAO.sendApprovalSMS(dto);
                            gate_passDAO.sentMail(dto);
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }

                    } else {
                        // Send notification to sergeant-at-arms A gate pass referred by
                        Employee_recordsDTO employeeRecordsDTO = Employee_recordsRepository.getInstance().getById(dto.referrerId);
                        String URL = "Gate_passServlet?notification=true&actionType=armsOfficeView&ID=" + gatePassId;
                        String notificationMessage = "A gate pass referred by " + employeeRecordsDTO.nameEng +
                                " has been waiting for your approval$" + employeeRecordsDTO.nameBng +
                                " একটি প্রবেশ পাস সুপারিশ করেছেন, যা আপনার অনুমোদনের অপেক্ষায় আছে।";


                        TaskTypeApprovalPathRepository.getInstance().getByTaskTypeIdAndLevel(TaskTypeEnum.SERGEANT_AT_ARMS_APPROVAL.getValue(), 1)
                                .forEach(taskTypeApprovalPathDTO -> pb_notificationsDAO.addPb_notifications(
                                        taskTypeApprovalPathDTO.officeUnitOrganogramId, System.currentTimeMillis(), URL, notificationMessage));

                    }
                } else if (approvalStatus.equalsIgnoreCase("dismiss")) {
                    dto.firstLayerApprovalStatus = Gate_passDAO.approvalStatusCancelled;
                    dto.firstLayerApprovalTime = System.currentTimeMillis();
                    assert userDTO != null;
                    dto.firstLayerApprovedBy = userDTO.userName;
                }
                gate_passDAO.update(dto);


                gate_passDAO.deleteAffiliatedPerson(affiliatedPersonsIDs);


                response.sendRedirect(getServletContext().getContextPath() + "/Gate_passServlet?actionType=search&notification=" + notification);
            } else if (actionType.equals("gatePassSecondLayerApprovalFromView")) {
                String approvalStatus = request.getParameter("approvalstatus");
                long gatePassId = Long.parseLong(request.getParameter("gatePassId"));
                HttpSession session = request.getSession(false);
                session.setAttribute("notificationAfterApproval", approvalStatus);

                String gallery;
                long gatePassGalleryId;
                try {
                    gallery = request.getParameter("gallery");
                    if (gallery == null || gallery.equals("")) {
                        gallery = "";
                    }
                } catch (Exception ex) {
                    gallery = "";
                }


                Gate_passDTO dto = gate_passDAO.getDTOByID(gatePassId);
                if (approvalStatus.equalsIgnoreCase("approved")) {
                    String items = request.getParameter("items");
                    parseAndUpdateItems(items);
                    dto.secondLayerApprovalStatus = Gate_passDAO.approvalStatusApproved;
                    dto.secondLayerApprovalTime = System.currentTimeMillis();
                    assert userDTO != null;
                    dto.secondLayerApprovedBy = userDTO.userName;
                    if (gallery.equalsIgnoreCase("gallery")) {
                        gatePassGalleryId = Long.parseLong(request.getParameter("gallery_id"));
                        Parliament_galleryDAO dao = new Parliament_galleryDAO();
                        dao.calculateAvailableSeatByGatePassGalleryId(gatePassGalleryId, "subtract");
                        dto.gatePassGalleryId = gatePassGalleryId;
                    }
                    try {
                        gate_passDAO.sendApprovalSMS(dto);
                        gate_passDAO.sentMail(dto);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                } else if (approvalStatus.equalsIgnoreCase("dismiss")) {
                    String remarks = request.getParameter("remarks");
                    dto.remarks = remarks;
                    dto.secondLayerApprovalStatus = Gate_passDAO.approvalStatusCancelled;
                    dto.secondLayerApprovalTime = System.currentTimeMillis();
                    assert userDTO != null;
                    dto.secondLayerApprovedBy = userDTO.userName;
                    if (gallery.equalsIgnoreCase("gallery")) {
                        gatePassGalleryId = gate_passDAO.getGatePassGalleryId(gatePassId);
                        Parliament_galleryDAO dao = new Parliament_galleryDAO();
                        dao.calculateAvailableSeatByGatePassGalleryId(gatePassGalleryId, "add");
                        dto.gatePassGalleryId = 0;
                    }
                } else if (approvalStatus.equalsIgnoreCase("process")) {
                    dto.secondLayerApprovalStatus = Gate_passDAO.approvalStatusProcessing;
                    dto.secondLayerApprovalTime = System.currentTimeMillis();
                    assert userDTO != null;
                    dto.secondLayerApprovedBy = userDTO.userName;
                    if (gallery.equalsIgnoreCase("gallery")) {
                        gatePassGalleryId = gate_passDAO.getGatePassGalleryId(gatePassId);
                        Parliament_galleryDAO dao = new Parliament_galleryDAO();
                        dao.calculateAvailableSeatByGatePassGalleryId(gatePassGalleryId, "add");
                        dto.gatePassGalleryId = 0;
                    }
                }

                gate_passDAO.update(dto);

                response.sendRedirect(getServletContext().getContextPath() + "/Gate_passServlet?actionType=armsOfficeSearch");
            } else if (actionType.equals("parliamentAreaSecurityView")) {
                String gatePassId = request.getParameter("ID");
                Gate_passDAO gate_passDAO = new Gate_passDAO();
                GatePassModel gatePassModel = gate_passDAO.getGatePassModelById(Long.parseLong(gatePassId));
                request.setAttribute("GatePassModel", gatePassModel);
                request.getRequestDispatcher("parliament_area_security/parliament_area_securityView.jsp").forward(request, response);
            } else if (actionType.equals("parliamentBuildingSecurityView")) {
                String gatePassId = request.getParameter("ID");
                Gate_passDAO gate_passDAO = new Gate_passDAO();
                GatePassModel gatePassModel = gate_passDAO.getGatePassModelById(Long.parseLong(gatePassId));
                request.setAttribute("GatePassModel", gatePassModel);
                request.getRequestDispatcher("parliament_building_security/parliament_building_securityView.jsp").forward(request, response);
            }else if (actionType.equals("printPreview")) {
                String gatePassId = request.getParameter("ID");
                String holdingItemIdAmountString = request.getParameter("holdingItemIdAmountString");
                Gate_passDAO gate_passDAO = new Gate_passDAO();
                GatePassModel gatePassModel = gate_passDAO.getGatePassModelByIdWithCardNumber(Long.parseLong(gatePassId), userDTO, holdingItemIdAmountString);
                request.setAttribute("GatePassModel", gatePassModel);

                HttpSession session = request.getSession(false);
                session.setAttribute("cardIssueNotification", "yes");

                request.getRequestDispatcher("parliament_building_security/printPreview.jsp").forward(request, response);
            }else if (actionType.equals("atReceptionReceiveItems")) {
                Gate_pass_itemDAO gate_pass_itemDAO = new Gate_pass_itemDAO();
                String gatePassId = request.getParameter("ID");
                List<GatePassCardIssueModalItem> gatePassCardIssueModalItems = gate_pass_itemDAO.getAdditionalItems(Long.parseLong(gatePassId));
                request.setAttribute("gatePassId", gatePassId);
                request.setAttribute("gatePassCardIssueModalItems", gatePassCardIssueModalItems);
                request.getRequestDispatcher("parliament_building_security/receiveItem.jsp").forward(request, response);
            } else if (actionType.equals("receiveCardData")) {
                String cardNumber = request.getParameter("cardNumber");

                Gate_pass_itemDAO gate_pass_itemDAO = new Gate_pass_itemDAO();
                List<ReceivedItemModel> model = gate_pass_itemDAO.getReceivedItemModelByCardNumber(Integer.parseInt(cardNumber));

                if (model.size() == 0) {
                    PrintWriter out = response.getWriter();
                    out.println("noString");
                    out.close();
                } else {
                    String jsonInString = new Gson().toJson(model);
                    JSONArray mJSONArray = new JSONArray(jsonInString);

                    PrintWriter out = response.getWriter();
                    out.println(mJSONArray);
                    out.close();
                }
            } else if (actionType.equals("receiveCard")) {
                String cardNumber = request.getParameter("cardNumber");
                Gate_pass_itemDAO gate_pass_itemDAO = new Gate_pass_itemDAO();
                gate_pass_itemDAO.deleteAdditionItemDTO(Integer.parseInt(cardNumber));
            } else {
                request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.debug(ex);
        }
    }


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        System.out.println("doPost");
        boolean isPermanentTable = true;
        if (request.getParameter("isPermanentTable") != null) {
            isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
        }
        System.out.println("In servlet, isPermanentTable = " + isPermanentTable);

        try {
            String actionType = request.getParameter("actionType");
            System.out.println("actionType = " + actionType);
            if (actionType.equals("add")) {

                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.GATE_PASS_ADD)) {
                    System.out.println("going to  addGate_pass ");
                    addGate_pass(request, response, true, userDTO, true);
                } else {
                    System.out.println("Not going to  addGate_pass ");
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }

            } else if (actionType.equals("SendToApprovalPath")) {

                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.GATE_PASS_APPROVE)) {
                    commonRequestHandler.sendToApprovalPath(request, response, userDTO);
                } else {
                    System.out.println("Not going to SendToApprovalPath ");
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }

            } else if (actionType.equals("approve")) {

                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.GATE_PASS_ADD)) {
                    commonRequestHandler.approve(request, response, true, userDTO, true);
                } else {
                    System.out.println("Not going to  addGate_pass ");
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }

            } else if (actionType.equals("reject")) {
                System.out.println("trying to approve");

                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.GATE_PASS_APPROVE)) {
                    commonRequestHandler.approve(request, response, true, userDTO, false);
                } else {
                    System.out.println("Not going to  addGate_pass ");
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else if (actionType.equals("terminate")) {

                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.GATE_PASS_ADD)) {
                    commonRequestHandler.terminate(request, response, userDTO);
                } else {
                    System.out.println("Not going to  addGate_pass ");
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }

            } else if (actionType.equals("skipStep")) {

                System.out.println("skipStep");
                commonRequestHandler.skipStep(request, response, userDTO);
            } else if (actionType.equals("getDTO")) {

                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.GATE_PASS_ADD)) {
                    getDTO(request, response);
                } else {
                    System.out.println("Not going to  addGate_pass ");
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }

            } else if (actionType.equals("edit")) {

                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.GATE_PASS_UPDATE)) {
                    addGate_pass(request, response, false, userDTO, isPermanentTable);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else if (actionType.equals("delete")) {
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.FAST_SEARCH_TEST_UPDATE)) {
                    commonRequestHandler.delete(request, response, userDTO);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else if (actionType.equals("search")) {
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.GATE_PASS_SEARCH)) {
                    searchGate_pass(request, response, true, "");
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else if (actionType.equals("getGeo")) {
                System.out.println("going to geoloc ");
                request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
            } else if (actionType.equals("insertGatePassValidity")) {
                long validityFrom = Long.parseLong(request.getParameter("validityFrom"));
                long validityTo = Long.parseLong(request.getParameter("validityTo"));
                long gatePassId = Long.parseLong(request.getParameter("gatePassId"));
                System.out.println("Xahid:" + validityFrom + " vai " + validityTo + " gustav " + gatePassId);

                Gate_passDTO dto = gate_passDAO.getDTOByID(gatePassId);
                dto.validityFrom = validityFrom;
                dto.validityTo = validityTo;
                gate_passDAO.update(dto);
            } else if (actionType.equals("gatePassFirstLayerApproval")) {
                String approvalStatus = request.getParameter("approvalstatus");
                long gatePassId = Long.parseLong(request.getParameter("gatePassId"));

                Gate_passDTO dto = gate_passDAO.getDTOByID(gatePassId);
                if (approvalStatus.equalsIgnoreCase("approved")) {
                    dto.firstLayerApprovalStatus = Gate_passDAO.approvalStatusApproved;
                    dto.firstLayerApprovalTime = System.currentTimeMillis();
                    assert userDTO != null;
                    dto.firstLayerApprovedBy = userDTO.userName;
                    // Send SMS to Clients
                    if (dto.gatePassTypeId == 1) {
                        try {
                            gate_passDAO.sendApprovalSMS(dto);
                            gate_passDAO.sentMail(dto);
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
                } else if (approvalStatus.equalsIgnoreCase("dismiss")) {
                    dto.firstLayerApprovalStatus = Gate_passDAO.approvalStatusCancelled;
                    dto.firstLayerApprovalTime = System.currentTimeMillis();
                    assert userDTO != null;
                    dto.firstLayerApprovedBy = userDTO.userName;
                }
                gate_passDAO.update(dto);

            } else if (actionType.equals("gatePassSecondLayerApproval")) {
                String approvalStatus = request.getParameter("approvalstatus");
                long gatePassId = Long.parseLong(request.getParameter("gatePassId"));

                Gate_passDTO dto = gate_passDAO.getDTOByID(gatePassId);
                if (approvalStatus.equalsIgnoreCase("approved")) {
                    dto.secondLayerApprovalStatus = Gate_passDAO.approvalStatusApproved;
                    dto.secondLayerApprovalTime = System.currentTimeMillis();
                    assert userDTO != null;
                    dto.secondLayerApprovedBy = userDTO.userName;

                    try {
                        gate_passDAO.sendApprovalSMS(dto);
                        gate_passDAO.sentMail(dto);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                } else if (approvalStatus.equalsIgnoreCase("dismiss")) {
                    dto.remarks = request.getParameter("remarks");
                    dto.secondLayerApprovalStatus = Gate_passDAO.approvalStatusCancelled;
                    dto.secondLayerApprovalTime = System.currentTimeMillis();
                    assert userDTO != null;
                    dto.secondLayerApprovedBy = userDTO.userName;
                } else if (approvalStatus.equalsIgnoreCase("process")) {
                    dto.secondLayerApprovalStatus = Gate_passDAO.approvalStatusProcessing;
                    dto.secondLayerApprovalTime = System.currentTimeMillis();
                    assert userDTO != null;
                    dto.secondLayerApprovedBy = userDTO.userName;
                }

                long gatePassGalleryId;
                String method;

                try {
                    String gallery = request.getParameter("gallery");
                    if (gallery != null) {
                        gatePassGalleryId = gate_passDAO.getGatePassGalleryId(gatePassId);
                        Parliament_galleryDAO dao = new Parliament_galleryDAO();
                        dao.calculateAvailableSeatByGatePassGalleryId(gatePassGalleryId, "add");
                    }
                } catch (Exception ex) {
                    logger.debug(ex);
                }


                try {
                    gatePassGalleryId = Long.parseLong(request.getParameter("galleryId"));
                    method = request.getParameter("method");
                    Parliament_galleryDAO dao = new Parliament_galleryDAO();
                    dao.calculateAvailableSeatByGatePassGalleryId(gatePassGalleryId, method);
                } catch (Exception ex) {
                    gatePassGalleryId = 0;
                }

                dto.gatePassGalleryId = gatePassGalleryId;

                gate_passDAO.update(dto);
            } else if (actionType.equals("issueCard")) {

//				Add item before issuing card
                response.sendRedirect(getServletContext().getContextPath() + "/Gate_passServlet?actionType=parliamentBuildingSecuritySearch");
            } else if (actionType.equals("addGatePassAtGate")) {
                response.sendRedirect(getServletContext().getContextPath() + "/Gate_passServlet?actionType=parliamentAreaSecurity");
            } else if (actionType.equals("addGatePassAtReception")) {
                response.sendRedirect(getServletContext().getContextPath() + "/Gate_passServlet?actionType=parliamentBuildingSecurity");
            } else {
                System.out.println("Not going to  addGate_pass ");
                request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            logger.debug(ex);
        }
    }

    private void getDTO(HttpServletRequest request, HttpServletResponse response) {
        try {
            System.out.println("In getDTO");
            Gate_passDTO gate_passDTO = gate_passDAO.getDTOByID(Long.parseLong(request.getParameter("ID")));
            PrintWriter out = response.getWriter();
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");

            String encoded = this.gson.toJson(gate_passDTO);
            System.out.println("json encoded data = " + encoded);
            out.print(encoded);
            out.flush();
        } catch (NumberFormatException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    private void addGate_pass(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, boolean isPermanentTable) throws IOException {
        // TODO Auto-generated method stub
        try {
            request.setAttribute("failureMessage", "");
            System.out.println("%%%% addGate_pass");
            String path = getServletContext().getRealPath("/img2/");
            Gate_passDTO gate_passDTO;
            SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");

//			if(addFlag == true)
//			{
//				gate_passDTO = new Gate_passDTO();
//			}
//			else
//			{
//				gate_passDTO = (Gate_passDTO)gate_passDAO.getDTOByID(Long.parseLong(request.getParameter("iD")));
//			}

            gate_passDTO = new Gate_passDTO();

            String Value = "";

            Value = request.getParameter("gatePassVisitorId");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("gatePassVisitorId = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                gate_passDTO.gatePassVisitorId = Long.parseLong(Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("gatePassTypeId");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("gatePassTypeId = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                gate_passDTO.gatePassTypeId = Long.parseLong(Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("gatePassSubTypeId");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("gatePassSubTypeId = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                gate_passDTO.gatePassSubTypeId = Long.parseLong(Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("visitingDate");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("visitingDate = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                if (gate_passDTO.gatePassTypeId == Gate_pass_typeDTO.GALLERY_PASS) {
                    gate_passDTO.visitingDate = Long.parseLong(Value);
                } else {
                    try {
                        Date d = f.parse(Value);
                        gate_passDTO.visitingDate = d.getTime();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("referrerId");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("referrerId = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                gate_passDTO.referrerId = Long.parseLong(Value);
                boolean validReferrer = false;
                String refName = WorkflowController.getUserNameFromErId(gate_passDTO.referrerId, "english");
                if(refName != null && refName.startsWith("0"))
                {
                	validReferrer = true;
                }
                else
                {
                	int maxLevel = Gate_passDTO.MAX_NORMAL_LEVEL;
                	if(gate_passDTO.gatePassTypeId == Gate_pass_typeDTO.GALLERY_PASS)
                	{
                		maxLevel = Gate_passDTO.MAX_GALLERY_LEVEL;
                	}
                	EmployeeOfficeDTO employeeOfficeDTO = EmployeeOfficeRepository.getInstance().getByEmployeeRecordIdIsDefault(gate_passDTO.referrerId);
                	if(employeeOfficeDTO != null && employeeOfficeDTO.jobGradeTypeCat >= 0 && employeeOfficeDTO.jobGradeTypeCat <= maxLevel)
                	{
                		validReferrer = true;
                	}
                }
                if(!validReferrer)
                {
                	request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                    return;
                }
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("parliamentGateId");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("parliamentGateId = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                gate_passDTO.parliamentGateId = Long.parseLong(Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("validityFrom");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("validityFrom = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                gate_passDTO.validityFrom = Long.parseLong(Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("validityTo");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("validityTo = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                gate_passDTO.validityTo = Long.parseLong(Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("jobCat");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("jobCat = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                gate_passDTO.jobCat = Integer.parseInt(Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            if (addFlag) {
                Calendar c = Calendar.getInstance();
                c.set(Calendar.HOUR_OF_DAY, 0);
                c.set(Calendar.MINUTE, 0);
                c.set(Calendar.SECOND, 0);
                c.set(Calendar.MILLISECOND, 0);

                gate_passDTO.insertionDate = c.getTimeInMillis();
                gate_passDTO.insertedBy = String.valueOf(userDTO.employee_record_id);
            }

            Value = request.getParameter("modifiedBy");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("modifiedBy = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                gate_passDTO.modifiedBy = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("reasonForVisit");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("reasonForVisit = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                gate_passDTO.reasonForVisit = Value;
            } else {
                System.out.println("FieldName has a null Value or empty string, not updating" + " = " + Value);
            }

            System.out.println("Done adding  addGate_pass dto = " + gate_passDTO);

            // redirecting to Gate_pass_visitorServlet
            HttpSession session = request.getSession(false);
            session.setAttribute("gate_passDTO", gate_passDTO);

            response.sendRedirect(request.getContextPath() + "/Gate_pass_visitorServlet?actionType=getAddPage");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void getGate_pass(HttpServletRequest request, HttpServletResponse response, long id) throws ServletException, IOException {
        System.out.println("in getGate_pass");
        Gate_passDTO gate_passDTO = null;
        try {
            gate_passDTO = gate_passDAO.getDTOByID(id);
            boolean isPermanentTable = true;
            if (request.getParameter("isPermanentTable") != null) {
                isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
            }
            request.setAttribute("ID", gate_passDTO.iD);
            request.setAttribute("gate_passDTO", gate_passDTO);
            request.setAttribute("gate_passDAO", gate_passDAO);

            String URL = "";

            String inPlaceEdit = request.getParameter("inplaceedit");
            String inPlaceSubmit = request.getParameter("inplacesubmit");
            String getBodyOnly = request.getParameter("getBodyOnly");

            if (inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase("")) {
                URL = "gate_pass/gate_passInPlaceEdit.jsp";
                request.setAttribute("inplaceedit", "");
            } else if (inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase("")) {
                URL = "gate_pass/gate_passSearchRow.jsp";
                request.setAttribute("inplacesubmit", "");
            } else {
                if (getBodyOnly != null && !getBodyOnly.equalsIgnoreCase("")) {
                    URL = "gate_pass/gate_passEditBody.jsp?actionType=edit";
                } else {
                    URL = "gate_pass/gate_passEdit.jsp?actionType=edit";
                }
            }

            RequestDispatcher rd = request.getRequestDispatcher(URL);
            rd.forward(request, response);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void getGate_pass(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        getGate_pass(request, response, Long.parseLong(request.getParameter("ID")));
    }

    private void searchGate_pass(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException {
        System.out.println("in  searchGate_pass 1");
        LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        String ajax = request.getParameter("ajax");
        boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);

        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
                SessionConstants.NAV_GATE_PASS,
                request,
                gate_passDAO,
                SessionConstants.VIEW_GATE_PASS,
                SessionConstants.SEARCH_GATE_PASS,
                tableName,
                isPermanent,
                userDTO,
                filter,
                true);
        try {
            System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        } catch (Exception e) {
            System.out.println("failed to dojob" + e);
        }

        request.setAttribute("gate_passDAO", gate_passDAO);
        RequestDispatcher rd;
        if (!isPermanent) {
            if (hasAjax == false) {
                System.out.println("Going to gate_pass/gate_passApproval.jsp");
                rd = request.getRequestDispatcher("gate_pass/gate_passApproval.jsp");
            } else {
                System.out.println("Going to gate_pass/gate_passApprovalForm.jsp");
                rd = request.getRequestDispatcher("gate_pass/gate_passApprovalForm.jsp");
            }
        } else {
            if (hasAjax == false) {
                System.out.println("Going to gate_pass/gate_passSearch.jsp");
                rd = request.getRequestDispatcher("gate_pass/gate_passSearch.jsp");
            } else {
                System.out.println("Going to gate_pass/gate_passSearchForm.jsp");
                rd = request.getRequestDispatcher("gate_pass/gate_passSearchForm.jsp");
            }
        }
        rd.forward(request, response);
    }

    private void armsOfficeSearchGate_pass(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException {
        System.out.println("in arms office search Gate_pass 1");
        LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        String ajax = request.getParameter("ajax");
        boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);

        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
                SessionConstants.NAV_GATE_PASS,
                request,
                gate_passDAO,
                SessionConstants.VIEW_GATE_PASS,
                SessionConstants.SEARCH_GATE_PASS,
                tableName,
                isPermanent,
                userDTO,
                filter,
                true);
        try {
            System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        } catch (Exception e) {
            System.out.println("failed to dojob" + e);
        }

        request.setAttribute("gate_passDAO", gate_passDAO);
        RequestDispatcher rd;


        if (hasAjax == false) {
            System.out.println("Going to gate_pass/gate_passSearch.jsp");
            rd = request.getRequestDispatcher("gate_pass/gate_passSearch.jsp");
        } else {
            System.out.println("Going to gate_pass/gate_passSearchForm.jsp");
            rd = request.getRequestDispatcher("gate_pass/gate_passSearchForm.jsp");
        }

        rd.forward(request, response);
    }


    private void parseAndUpdateItems(String itemString) {
        if (itemString == null || itemString.equals("")) {
            return;
        }

        String[] items = itemString.split(";");
        String[] individualItem;
        Gate_pass_itemDAO gate_pass_itemDAO = new Gate_pass_itemDAO();

        for (String item : items) {
            individualItem = item.split(",");
            gate_pass_itemDAO.updateItemAmountById(Long.parseLong(individualItem[0]), Integer.parseInt(individualItem[1]));
        }
    }


    private void areaSearchGate_pass(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException {
        System.out.println("in area search Gate_pass");
        LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        String ajax = request.getParameter("ajax");
        boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);

        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
                SessionConstants.NAV_GATE_PASS,
                request,
                gate_passDAO,
                SessionConstants.VIEW_GATE_PASS,
                SessionConstants.SEARCH_GATE_PASS,
                tableName,
                isPermanent,
                userDTO,
                filter,
                true);
        try {
            System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        } catch (Exception e) {
            System.out.println("failed to dojob" + e);
        }

        request.setAttribute("gate_passDAO", gate_passDAO);
        RequestDispatcher rd;


        if (hasAjax == false) {
            System.out.println("Going to parliament_area_security/parliament_area_securitySearch.jsp");
            rd = request.getRequestDispatcher("parliament_area_security/parliament_area_securitySearch.jsp");
        } else {
            System.out.println("Going to parliament_area_security/parliament_area_securitySearchForm.jsp");
            rd = request.getRequestDispatcher("parliament_area_security/parliament_area_securitySearchForm.jsp");
        }

        rd.forward(request, response);
    }

    private void buildingSearchGate_pass(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException {
        System.out.println("in building search Gate_pass");
        LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        String ajax = request.getParameter("ajax");
        boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);

        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
                SessionConstants.NAV_GATE_PASS,
                request,
                gate_passDAO,
                SessionConstants.VIEW_GATE_PASS,
                SessionConstants.SEARCH_GATE_PASS,
                tableName,
                isPermanent,
                userDTO,
                filter,
                true);
        try {
            System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        } catch (Exception e) {
            System.out.println("failed to dojob" + e);
        }

        request.setAttribute("gate_passDAO", gate_passDAO);
        RequestDispatcher rd;


        if (hasAjax == false) {
            System.out.println("Going to parliament_building_security/parliament_building_securitySearch.jsp");
            rd = request.getRequestDispatcher("parliament_building_security/parliament_building_securitySearch.jsp");
        } else {
            System.out.println("Going to parliament_building_security/parliament_building_securitySearchForm.jsp");
            rd = request.getRequestDispatcher("parliament_building_security/parliament_building_securitySearchForm.jsp");
        }

        rd.forward(request, response);
    }
}

