package gate_pass;

import java.util.ArrayList;
import java.util.List;

public class GatePassDashboardDataModel {
    public long totalGatePassAppliedInThisMonth = 0;
    public long approvedPassAppliedInThisMonth = 0;
    public long dismissedGatePassAppliedInThisMonth = 0;
    public long pendingGatePassAppliedInThisMonth = 0;
    public long processedPassAppliedInThisMonth = 0;

    public long totalVisitorPassAppliedInThisMonth = 0;
    public long approvedVisitorPassAppliedInThisMonth = 0;
    public long dismissedVisitorPassAppliedInThisMonth = 0;
    public long pendingVisitorPassAppliedInThisMonth = 0;
    public long receivedVisitorPassAppliedInThisMonth = 0;

    public long areaPassVisitor = 0;
    public long areaPassOfficial = 0;
    public long visitorPass = 0;
    public long galleryPass = 0;
    public long meetingBriefingPass = 0;

    public long visitorDomestic = 0;
    public long visitorForeigner = 0;

    public List<Long> totalGatePassAppliedMonthWise = new ArrayList<>();
    public List<Long> approvedPassAppliedMonthWise = new ArrayList<>();
    public List<Long> dismissedGatePassAppliedMonthWise = new ArrayList<>();
    public List<Long> pendingGatePassAppliedMonthWise = new ArrayList<>();
    public List<Long> processedPassAppliedMonthWise = new ArrayList<>();

    public List<Long> totalVisitorPassAppliedMonthWise = new ArrayList<>();
    public List<Long> approvedVisitorPassAppliedMonthWise = new ArrayList<>();
    public List<Long> dismissedVisitorPassAppliedMonthWise = new ArrayList<>();
    public List<Long> pendingVisitorPassAppliedMonthWise = new ArrayList<>();
    public List<Long> receivedVisitorPassAppliedMonthWise = new ArrayList<>();

}
