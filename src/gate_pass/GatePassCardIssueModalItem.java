package gate_pass;

public class GatePassCardIssueModalItem {
    public long id = 0;
    public String visitorName = "";
    public String itemIdList = "";
    public int amount = 0;
    public int cardNumber = 0;
    public String description = "";
    public long returnTime = -1;
    public int galleryCat = -1;
}
