package gate_pass;
import java.util.*; 
import util.*;


public class Gate_passMAPS extends CommonMaps
{	
	public Gate_passMAPS(String tableName)
	{
		

		java_DTO_map.put("id".toLowerCase(), "id".toLowerCase());
		java_DTO_map.put("gatePassVisitorId".toLowerCase(), "gatePassVisitorId".toLowerCase());
		java_DTO_map.put("visitingDate".toLowerCase(), "visitingDate".toLowerCase());
		java_DTO_map.put("gatePassTypeId".toLowerCase(), "gatePassTypeId".toLowerCase());
		java_DTO_map.put("gatePassSubTypeId".toLowerCase(), "gatePassSubTypeId".toLowerCase());
		java_DTO_map.put("referrerId".toLowerCase(), "referrerId".toLowerCase());
		java_DTO_map.put("parliamentGateId".toLowerCase(), "parliamentGateId".toLowerCase());
		java_DTO_map.put("validityFrom".toLowerCase(), "validityFrom".toLowerCase());
		java_DTO_map.put("validityTo".toLowerCase(), "validityTo".toLowerCase());
		java_DTO_map.put("jobCat".toLowerCase(), "jobCat".toLowerCase());
		java_DTO_map.put("insertionDate".toLowerCase(), "insertionDate".toLowerCase());
		java_DTO_map.put("insertedBy".toLowerCase(), "insertedBy".toLowerCase());
		java_DTO_map.put("modifiedBy".toLowerCase(), "modifiedBy".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("gate_pass_visitor_id".toLowerCase(), "gatePassVisitorId".toLowerCase());
		java_SQL_map.put("visiting_date".toLowerCase(), "visitingDate".toLowerCase());

		java_Text_map.put("Id".toLowerCase(), "id".toLowerCase());
		java_Text_map.put("Gate Pass Visitor Id".toLowerCase(), "gatePassVisitorId".toLowerCase());
		java_Text_map.put("Visiting Date".toLowerCase(), "visitingDate".toLowerCase());
		java_Text_map.put("Gate Pass Type Id".toLowerCase(), "gatePassTypeId".toLowerCase());
		java_Text_map.put("Gate Pass Sub Type Id".toLowerCase(), "gatePassSubTypeId".toLowerCase());
		java_Text_map.put("Referrer Id".toLowerCase(), "referrerId".toLowerCase());
		java_Text_map.put("Parliament Gate Id".toLowerCase(), "parliamentGateId".toLowerCase());
		java_Text_map.put("Validity From".toLowerCase(), "validityFrom".toLowerCase());
		java_Text_map.put("Validity To".toLowerCase(), "validityTo".toLowerCase());
		java_Text_map.put("Job".toLowerCase(), "jobCat".toLowerCase());
		java_Text_map.put("Insertion Date".toLowerCase(), "insertionDate".toLowerCase());
		java_Text_map.put("Inserted By".toLowerCase(), "insertedBy".toLowerCase());
		java_Text_map.put("Modified By".toLowerCase(), "modifiedBy".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}