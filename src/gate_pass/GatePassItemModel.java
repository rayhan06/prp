package gate_pass;

public class GatePassItemModel {
    public long id = 0;
    public String nameEng = "";
    public String nameBng = "";
    public int amount = 0;
    public int approvedAmount = 0;
    public String description = "";


    @Override
    public String toString() {
        return "GatePassItemModel[" +
                " nameEng = " + nameEng +
                " amount = " + amount +
                "]";
    }
}
