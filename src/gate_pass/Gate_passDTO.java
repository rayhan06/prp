package gate_pass;
import java.util.*;

import sessionmanager.SessionConstants;
import util.*;


public class Gate_passDTO extends CommonDTO
{

	public long id = 0;
	public long gatePassVisitorId = 0;
	public long visitingDate = 0;
	public long gatePassTypeId = 0;
	public long gatePassSubTypeId = 0;
	public long referrerId = 0;
	public long parliamentGateId = 0;
	public long validityFrom = 0;
	public long validityTo = 0;
	public long insertionDate = 0;
    public String insertedBy = "";
    public String modifiedBy = "";
    public int firstLayerApprovalStatus = Gate_passDAO.approvalStatusPending;
    public String firstLayerApprovedBy = "";
    public long firstLayerApprovalTime = SessionConstants.MIN_DATE;
	public int secondLayerApprovalStatus = Gate_passDAO.approvalStatusPending;
	public String secondLayerApprovedBy = "";
	public long secondLayerApprovalTime = SessionConstants.MIN_DATE;
	public String remarks = "";
	public long gatePassGalleryId = 0;
	public String reasonForVisit = "";
	public int isIssued = 0;
	public boolean partiallyIssued = false;
	public String issuedBy = "";
	public long issueDate = SessionConstants.MIN_DATE;
	
	public static final int MAX_NORMAL_LEVEL = 9;
	public static final int MAX_GALLERY_LEVEL = 5;
	
	public static final String[] errorsEn = new String[] {
			"Your Gate Pass request is submitted for approval!",
			"Wrong Visitor info, pass rejected!"
			};
	
	public static final String[] errorsBn = new String[] {
			"আপনার প্রবেশ পাসটি অনুমোদনের জন্য কতৃপক্ষের নিকট পাঠানো হয়েছে!",
			"প্রবেশকারীর তথ্য ভুল থাকায় পাসটি বাতিল হয়েছে!"
			};

	public static final int SUCCESS_MSG = 0;
	public static final int ERROR_VISITOR = 1;
	
	
    @Override
	public String toString() {
            return "$Gate_passDTO[" +
            " id = " + id +
            " gatePassVisitorId = " + gatePassVisitorId +
            " visitingDate = " + visitingDate +
            " gatePassTypeId = " + gatePassTypeId +
            " gatePassSubTypeId = " + gatePassSubTypeId +
            " referrerId = " + referrerId +
            " parliamentGateId = " + parliamentGateId +
            " validityFrom = " + validityFrom +
			" validityTo = " + validityTo +
            " gatePassGalleryId = " + gatePassGalleryId +
            " jobCat = " + jobCat +
            " insertionDate = " + insertionDate +
            " insertedBy = " + insertedBy +
            " modifiedBy = " + modifiedBy +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}