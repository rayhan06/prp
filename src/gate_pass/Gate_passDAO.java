package gate_pass;

import appointment.KeyCountDTO;
import appointment.YearMonthCount;
import common.ConnectionAndStatementUtil;
import dbm.DBMR;
import employee_records.Employee_recordsDAO;
import employee_records.Employee_recordsDTO;
import employee_records.Employee_recordsRepository;
import gate_pass_affiliated_person.Gate_pass_affiliated_personDAO;
import gate_pass_item.Gate_pass_itemDAO;
import gate_pass_sub_type.Gate_pass_sub_typeRepository;
import gate_pass_type.Gate_pass_typeDTO;
import gate_pass_type.Gate_pass_typeRepository;
import gate_pass_visitor.Gate_pass_visitorDAO;
import gate_pass_visitor.Gate_pass_visitorDTO;
import office_unit_organograms.OfficeUnitOrganograms;
import office_unit_organograms.OfficeUnitOrganogramsRepository;
import office_units.Office_unitsDTO;
import office_units.Office_unitsRepository;
import org.apache.log4j.Logger;
import parliament_gate.Parliament_gateDTO;
import parliament_gate.Parliament_gateRepository;
import parliament_session.Parliament_sessionDTO;
import parliament_session.Parliament_sessionRepository;
import pb.CatRepository;
import pb.OptionDTO;
import pb.Utils;
import pb_notifications.Pb_notificationsDAO;
import repository.RepositoryManager;
import sessionmanager.SessionConstants;
import sms.SmsService;
import task_type.TaskTypeEnum;
import task_type_approval_path.TaskTypeApprovalPathDTO;
import task_type_approval_path.TaskTypeApprovalPathRepository;
import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;
import util.NavigationService4;
import util.StringUtils;
import util.TimeConverter;

import java.io.IOException;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.*;

/*
I know, I should be and must be vilified for this garbage!
But believe me, I tried my best. It's a fucking ERP system with no framework at all (Project Builder is my ass!!!).
I wasn't given time even enough to think about a functionality.
Please, don't be frustrated and start the fucking change of this rubbish!
contact me if anything intrigue you: ahzahid@iastate.edu
 */
public class Gate_passDAO extends NavigationService4 {

    private static final Logger logger = Logger.getLogger(Gate_passDAO.class);
    public static final int approvalStatusPending = 0;
    public static final int approvalStatusApproved = 1;
    public static final int approvalStatusCancelled = 2;
    public static final int approvalStatusProcessing = 4;
    public static final int approvalStatusIssued = 5;


    public static final long SAS_APPROVAL_LEVEL_ID = 601;

    // Writing Query


    public Gate_passDAO(String tableName) {
        super(tableName);
        joinSQL = "";
        commonMaps = new Gate_passMAPS(tableName);
        approvalMaps = new Gate_passApprovalMAPS("gate_pass");
        columnNames = new String[]
                {
                        "ID",
                        "gate_pass_visitor_id",
                        "visiting_date",
                        "gate_pass_type_id",
                        "gate_pass_sub_type_id",
                        "referrer_id",
                        "parliament_gate_id",
                        "validity_from",
                        "validity_to",
                        "job_cat",
                        "first_layer_approval_status",
                        "first_layer_approved_by",
                        "first_layer_approval_time",
                        "second_layer_approval_status",
                        "second_layer_approved_by",
                        "second_layer_approval_time",
                        "remarks",
                        "gate_pass_gallery_id",
                        "insertion_date",
                        "inserted_by",
                        "modified_by",
                        "reason_for_visit",
                        "is_issued",
                        "partially_issued",
                        "issued_by",
                        "issue_date",
                        "isDeleted",
                        "lastModificationTime"
                };
    }

    public Gate_passDAO() {
        this("gate_pass");
    }

    public void setSearchColumn(Gate_passDTO gate_passDTO) {
        gate_passDTO.searchColumn = "";

    }

    public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException {
        Gate_passDTO gate_passDTO = (Gate_passDTO) commonDTO;
        int index = 1;
        long lastModificationTime = System.currentTimeMillis();
        setSearchColumn(gate_passDTO);
        if (isInsert) {
            ps.setObject(index++, gate_passDTO.iD);
        }
        ps.setObject(index++, gate_passDTO.gatePassVisitorId);
        ps.setObject(index++, gate_passDTO.visitingDate);
        ps.setObject(index++, gate_passDTO.gatePassTypeId);
        ps.setObject(index++, gate_passDTO.gatePassSubTypeId);
        ps.setObject(index++, gate_passDTO.referrerId);
        ps.setObject(index++, gate_passDTO.parliamentGateId);
        ps.setObject(index++, gate_passDTO.validityFrom);
        ps.setObject(index++, gate_passDTO.validityTo);
        ps.setObject(index++, gate_passDTO.jobCat);
        ps.setObject(index++, gate_passDTO.firstLayerApprovalStatus);
        ps.setObject(index++, gate_passDTO.firstLayerApprovedBy);
        ps.setObject(index++, gate_passDTO.firstLayerApprovalTime);
        ps.setObject(index++, gate_passDTO.secondLayerApprovalStatus);
        ps.setObject(index++, gate_passDTO.secondLayerApprovedBy);
        ps.setObject(index++, gate_passDTO.secondLayerApprovalTime);
        ps.setObject(index++, gate_passDTO.remarks);
        ps.setObject(index++, gate_passDTO.gatePassGalleryId);
        ps.setObject(index++, gate_passDTO.insertionDate);
        ps.setObject(index++, gate_passDTO.insertedBy);
        ps.setObject(index++, gate_passDTO.modifiedBy);
        ps.setObject(index++, gate_passDTO.reasonForVisit);
        ps.setObject(index++, gate_passDTO.isIssued);
        ps.setObject(index++, gate_passDTO.partiallyIssued);
        ps.setObject(index++, gate_passDTO.issuedBy);
        ps.setObject(index++, gate_passDTO.issueDate);
        if (isInsert) {
            ps.setObject(index++, 0);
            ps.setObject(index++, lastModificationTime);
        }
    }

    public void get(Gate_passDTO gate_passDTO, ResultSet rs) throws SQLException {
        gate_passDTO.id = rs.getLong("id");
        gate_passDTO.iD = gate_passDTO.id;
        gate_passDTO.gatePassVisitorId = rs.getLong("gate_pass_visitor_id");
        gate_passDTO.visitingDate = rs.getLong("visiting_date");
        gate_passDTO.gatePassTypeId = rs.getLong("gate_pass_type_id");
        gate_passDTO.gatePassSubTypeId = rs.getLong("gate_pass_sub_type_id");
        gate_passDTO.referrerId = rs.getLong("referrer_id");
        gate_passDTO.parliamentGateId = rs.getLong("parliament_gate_id");
        gate_passDTO.validityFrom = rs.getLong("validity_from");
        gate_passDTO.validityTo = rs.getLong("validity_to");
        gate_passDTO.jobCat = rs.getInt("job_cat");
        gate_passDTO.firstLayerApprovalStatus = rs.getInt("first_layer_approval_status");
        gate_passDTO.firstLayerApprovedBy = rs.getString("first_layer_approved_by");
        gate_passDTO.firstLayerApprovalTime = rs.getLong("first_layer_approval_time");
        gate_passDTO.secondLayerApprovalStatus = rs.getInt("second_layer_approval_status");
        gate_passDTO.secondLayerApprovedBy = rs.getString("second_layer_approved_by");
        gate_passDTO.secondLayerApprovalTime = rs.getLong("second_layer_approval_time");
        gate_passDTO.remarks = rs.getString("remarks");
        gate_passDTO.gatePassGalleryId = rs.getLong("gate_pass_gallery_id");
        gate_passDTO.insertionDate = rs.getLong("insertion_date");
        gate_passDTO.insertedBy = rs.getString("inserted_by");
        gate_passDTO.modifiedBy = rs.getString("modified_by");
        gate_passDTO.isDeleted = rs.getInt("isDeleted");
        gate_passDTO.lastModificationTime = rs.getLong("lastModificationTime");
        gate_passDTO.reasonForVisit = rs.getString("reason_for_visit");
        gate_passDTO.isIssued = rs.getInt("is_issued");
        gate_passDTO.partiallyIssued = rs.getBoolean("partially_issued");
        gate_passDTO.issuedBy = rs.getString("issued_by");
        gate_passDTO.issueDate = rs.getLong("issue_date");
    }

    public void getSearchModel(Gate_pass_searchModel gate_pass_searchModel, ResultSet rs) throws Exception {
        int dataIntId;
        long dataLongId;
        gate_pass_searchModel.gate_pass_id = rs.getLong("id");
        gate_pass_searchModel.gatePassVisitorId = rs.getLong("gate_pass_visitor_id");
        gate_pass_searchModel.visitingDate = rs.getLong("visiting_date");

        dataLongId = rs.getLong("gate_pass_type_id");
        gate_pass_searchModel.gatePassTypeId = dataLongId;
        gate_pass_searchModel.gatePassTypeEng = Gate_pass_typeRepository.getInstance().getGate_pass_typeDTOByid(dataLongId).nameEng;
        gate_pass_searchModel.gatePassTypeBng = Gate_pass_typeRepository.getInstance().getGate_pass_typeDTOByid(dataLongId).nameBng;

        if (dataLongId == 1) {
            dataLongId = rs.getLong("gate_pass_sub_type_id");
            gate_pass_searchModel.gatePassSubTypeEng = Gate_pass_sub_typeRepository.getInstance().getGate_pass_sub_typeDTOByid(dataLongId).nameEng;
            gate_pass_searchModel.gatePassSubTypeBng = Gate_pass_sub_typeRepository.getInstance().getGate_pass_sub_typeDTOByid(dataLongId).nameBng;
        }
        gate_pass_searchModel.referrerId = rs.getLong("referrer_id");
        Employee_recordsDAO employee_recordsDAO = new Employee_recordsDAO();
        Employee_recordsDTO employee_recordsDTO = employee_recordsDAO.getEmployee_recordsDTOByID(gate_pass_searchModel.referrerId);
        if (employee_recordsDTO != null) {
            gate_pass_searchModel.referrerNameEng = employee_recordsDTO.nameEng;
            gate_pass_searchModel.referrerNameBng = employee_recordsDTO.nameBng;

            long organogram = UserRepository.getInstance().getUserDtoByEmployeeRecordId(gate_pass_searchModel.referrerId).organogramID;
            try {
                gate_pass_searchModel.referrerOfficeUnitEng = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(OfficeUnitOrganogramsRepository.getInstance().getById(organogram).office_unit_id).unitNameEng;
            } catch (Exception ex) {
                gate_pass_searchModel.referrerOfficeUnitEng = "";
            }
            try {
                gate_pass_searchModel.referrerOfficeUnitBng = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(OfficeUnitOrganogramsRepository.getInstance().getById(organogram).office_unit_id).unitNameBng;
            } catch (Exception ex) {
                gate_pass_searchModel.referrerOfficeUnitBng = "";
            }
        }
        gate_pass_searchModel.parliamentGateId = rs.getLong("parliament_gate_id");
        gate_pass_searchModel.validityFrom = rs.getLong("validity_from");
        gate_pass_searchModel.validityTo = rs.getLong("validity_to");
        gate_pass_searchModel.insertionDate = rs.getLong("insertion_date");
        gate_pass_searchModel.name = rs.getString("name");
        gate_pass_searchModel.fatherName = rs.getString("father_name");
        gate_pass_searchModel.motherName = rs.getString("mother_name");

        gate_pass_searchModel.permanentAddress = getGatePassVisitorAddressModel(rs.getString("permanent_address"));
        gate_pass_searchModel.currentAddress = getGatePassVisitorAddressModel(rs.getString("current_address"));

        dataIntId = rs.getInt("credential_type");
        gate_pass_searchModel.credentialTypeEng = CatRepository.getInstance().getText("English", "credential", dataIntId);
        gate_pass_searchModel.credentialTypeBng = CatRepository.getInstance().getText("Bangla", "credential", dataIntId);

        gate_pass_searchModel.credentialNo = rs.getString("credential_no");
        gate_pass_searchModel.mobileNumber = rs.getString("mobile_number").substring(2);
        gate_pass_searchModel.email = rs.getString("email");

        dataIntId = rs.getInt("gender_type");
        gate_pass_searchModel.genderTypeEng = CatRepository.getInstance().getText("English", "gender", dataIntId);
        gate_pass_searchModel.genderTypeBng = CatRepository.getInstance().getText("Bangla", "gender", dataIntId);

        dataIntId = rs.getInt("officer_type");
        gate_pass_searchModel.officerTypeEng = CatRepository.getInstance().getText("English", "officer", dataIntId);
        gate_pass_searchModel.officerTypeBng = CatRepository.getInstance().getText("Bangla", "officer", dataIntId);
        gate_pass_searchModel.firstLayerApprovalStatus = rs.getInt("first_layer_approval_status");
        gate_pass_searchModel.firstLayerApprovedBy = rs.getString("first_layer_approved_by");
        gate_pass_searchModel.firstLayerApprovalTime = rs.getLong("first_layer_approval_time");
        gate_pass_searchModel.secondLayerApprovalStatus = rs.getInt("second_layer_approval_status");
        gate_pass_searchModel.secondLayerApprovedBy = rs.getString("second_layer_approved_by");
        gate_pass_searchModel.secondLayerApprovalTime = rs.getLong("second_layer_approval_time");

        try {
            gate_pass_searchModel.creatorId = rs.getLong("inserted_by");
        } catch (Exception ex) {
            gate_pass_searchModel.creatorId = 0;
        }

        gate_pass_searchModel.isIssued = rs.getInt("is_issued");
        gate_pass_searchModel.partiallyIssued = rs.getBoolean("partially_issued");
    }

    //need another getter for repository
    public Gate_passDTO getDTOByID(long ID) {
        Connection connection = null;
        ResultSet rs = null;
        Statement stmt = null;
        Gate_passDTO gate_passDTO = null;
        try {

            String sql = "SELECT * ";

            sql += " FROM " + tableName;

            sql += " WHERE id=" + ID;

            printSql(sql);

            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();


            rs = stmt.executeQuery(sql);

            if (rs.next()) {
                gate_passDTO = new Gate_passDTO();

                get(gate_passDTO, rs);

            }


        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }

            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
        return gate_passDTO;
    }

    public List<Gate_passDTO> getDTOs(Collection recordIDs) {
        Connection connection = null;
        ResultSet rs = null;
        Statement stmt = null;
        Gate_passDTO gate_passDTO = null;
        List<Gate_passDTO> gate_passDTOList = new ArrayList<>();
        if (recordIDs.isEmpty()) {
            return gate_passDTOList;
        }
        try {

            String sql = "SELECT * ";

            sql += " FROM " + tableName;

            sql += " WHERE id IN ( ";

            for (int i = 0; i < recordIDs.size(); i++) {
                if (i != 0) {
                    sql += ",";
                }
                sql += ((ArrayList) recordIDs).get(i);
            }
            sql += ")  order by lastModificationTime desc";

            printSql(sql);

            logger.debug("sql " + sql);
            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();


            rs = stmt.executeQuery(sql);

            while (rs.next()) {
                gate_passDTO = new Gate_passDTO();
                get(gate_passDTO, rs);
                System.out.println("got this DTO: " + gate_passDTO);

                gate_passDTOList.add(gate_passDTO);

            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }

            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
        return gate_passDTOList;

    }

    //add repository
    public List<Gate_passDTO> getAllGate_pass(boolean isFirstReload) {
        List<Gate_passDTO> gate_passDTOList = new ArrayList<>();

        String sql = "SELECT * FROM gate_pass";
        sql += " WHERE ";


        if (isFirstReload) {
            sql += " isDeleted =  0";
        }
        if (!isFirstReload) {
            sql += " lastModificationTime >= " + RepositoryManager.lastModifyTime;
        }
        sql += " order by gate_pass.lastModificationTime desc";
        printSql(sql);

        Connection connection = null;
        Statement stmt = null;
        ResultSet rs = null;


        try {
            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();
            rs = stmt.executeQuery(sql);


            while (rs.next()) {
                Gate_passDTO gate_passDTO = new Gate_passDTO();
                get(gate_passDTO, rs);

                gate_passDTOList.add(gate_passDTO);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }

        return gate_passDTOList;
    }

    public List<Gate_pass_searchModel> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO) {
        return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
    }

    public List<Gate_pass_searchModel> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
                                               String filter, boolean tableHasJobCat) {
        Connection connection = null;
        ResultSet rs = null;
        Statement stmt = null;
        List<Gate_pass_searchModel> gate_pass_searchModelList = new ArrayList<>();

        try {

            String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat);

            printSql(sql);

            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();


            rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Gate_pass_searchModel gate_pass_searchModel = new Gate_pass_searchModel();
                getSearchModel(gate_pass_searchModel, rs);

                gate_pass_searchModelList.add(gate_pass_searchModel);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }

            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
        return gate_pass_searchModelList;

    }

    public String getSqlWithSearchCriteriaForNormalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
                                                          UserDTO userDTO, String filter, boolean tableHasJobCat) {
        String jointable = "gate_pass_visitor";

        String sql = "SELECT ";
        if (category == GETIDS) {
            sql += " " + tableName + ".ID as ID ";
        } else if (category == GETCOUNT) {
            sql += " count( " + tableName + ".ID) as countID ";
        } else if (category == GETDTOS) {
            sql += " distinct " + tableName + ".* , " + jointable + ".* ";
        }
        
        
        sql += " FROM " + tableName + " INNER JOIN " + jointable;
        sql += " ON " + tableName + ".gate_pass_visitor_id = " + jointable + ".id ";
        /*sql += " LEFT JOIN gate_pass_affiliated_person ON gate_pass.id = gate_pass_affiliated_person.gate_pass_id ";*/


     


        String AnyfieldSql = "";
        String AllFieldSql = "";

        if (p_searchCriteria != null) {

            Enumeration names = p_searchCriteria.keys();
            String str, value;

            AnyfieldSql = "(";

            if (p_searchCriteria.get("AnyField") != null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase("")) {
                AnyfieldSql += jointable + ".search_column like '%" + p_searchCriteria.get("AnyField").toString() + "%'";
            }
            AnyfieldSql += ")";
            System.out.println("AnyfieldSql = " + AnyfieldSql);

            AllFieldSql = "(";
            int i = 0;
            while (names.hasMoreElements()) {
                str = (String) names.nextElement();
                value = (String) p_searchCriteria.get(str);
                System.out.println(str + ": " + value);
                if (value != null && !value.equalsIgnoreCase("") && (str.equals("credential_no")
                        || (str.equals("mobile_number") && !(p_searchCriteria.get(str).equals("88"))) ||
                        (str.equals("mobile_number_affiliated_persons") && !(p_searchCriteria.get(str).equals("88"))) || str.equals("email")
                        || str.equals("visiting_date") || str.equals("name") || str.equals("gatePassType") || str.equals("gatePassStatus")
                        || str.equals("cardNumber"))) {
                    if (p_searchCriteria.get(str).equals("any")) {
                        continue;
                    }

                    if (i > 0) {
                        AllFieldSql += " AND  ";
                    }

                    if (str.equals("visiting_date")) {
                        AllFieldSql += " " + tableName + ".visiting_date = " + p_searchCriteria.get(str);
                        i++;
                    } else if (str.equals("name")) {
                        AllFieldSql += " " + jointable + ".name like \"%" + p_searchCriteria.get(str) + "%\"";
                        i++;
                    } else if (str.equals("email")) {
                        AllFieldSql += " " + jointable + ".email = \"" + p_searchCriteria.get(str) + "\"";
                        i++;
                    } else if (str.equals("credential_no")) {
                        AllFieldSql += " " + jointable + ".credential_no = \"" + p_searchCriteria.get(str) + "\"";
                        i++;
                    } else if (str.equals("mobile_number")) {
                        AllFieldSql += " ( " + jointable + ".mobile_number = \"" + p_searchCriteria.get(str) + "\""
                                + " or " + p_searchCriteria.get(str) + " in "
                                + " (select mobile_number from gate_pass_affiliated_person where gate_pass.id = gate_pass_affiliated_person.gate_pass_id)) ";
                        i++;
                    }
                   /* else if (str.equals("mobile_number_affiliated_persons")) {
                        AllFieldSql += " " + tableName + ".gate_pass_type_id != 1 and " + tableName + ".first_layer_approval_status = "
                                + Gate_passDAO.approvalStatusApproved + " and ( " + jointable + ".mobile_number = \"" + p_searchCriteria.get(str) + "\""
                                + " or gate_pass_affiliated_person.mobile_number = \"" + p_searchCriteria.get(str) + "\") ";
                        i++;
                    }*/
                    else if (str.equals("gatePassType")) {
                        AllFieldSql += " " + tableName + ".gate_pass_type_id = " + p_searchCriteria.get(str);
                        i++;
                    }
                    else if (str.equals("cardNumber")) {
                        AllFieldSql += " " + Utils.getDigitEnglishFromBangla((String)p_searchCriteria.get(str)) 
                        + " in (select card_number from gate_pass_additional_item where gate_pass_id = " + tableName + ".id and isDeleted = 0)";
                        i++;
                    }
                    else if (str.equals("gatePassStatus")) {
                        if (Integer.parseInt((String) p_searchCriteria.get(str)) == Gate_passDAO.approvalStatusIssued) {
                            AllFieldSql += " " + tableName + ".is_issued = 1 ";
                        } else if (Integer.parseInt((String) p_searchCriteria.get(str)) == Gate_passDAO.approvalStatusApproved) {
                            AllFieldSql += " " + tableName + ".second_layer_approval_status = " + p_searchCriteria.get(str) +
                                    " AND " + tableName + ".is_issued = 0 ";
                        } else {
                            AllFieldSql += " " + tableName + ".second_layer_approval_status = " + p_searchCriteria.get(str);
                        }

                        i++;
                    }

                }
            }

            AllFieldSql += ")";
            System.out.println("AllFieldSql = " + AllFieldSql);
        }


        sql += " WHERE " + tableName + ".isDeleted = 0 ";

        if (!filter.equals("")) {
            sql += filter;
        }
        
        if(userDTO.roleID == SessionConstants.SERJEANT_AT_ARMS_ROLE)
        {
        	sql += " and first_layer_approval_status = " + Gate_passDAO.approvalStatusApproved + " ";
        }
        
        else if(userDTO.roleID == SessionConstants.PBS_ROLE)
        {
        	sql += " and first_layer_approval_status = " + Gate_passDAO.approvalStatusApproved 
        			+ " and second_layer_approval_status = " + Gate_passDAO.approvalStatusApproved ;
        }


        if (!AnyfieldSql.equals("()") && !AnyfieldSql.equals("")) {
            sql += " AND " + AnyfieldSql;

        }
        if (!AllFieldSql.equals("()") && !AllFieldSql.equals("")) {
            sql += " AND " + AllFieldSql;
        }

        sql += " order by " + tableName + ".id desc ";

        if (limit >= 0)
            sql += " limit " + limit;

        if (offset >= 0) {
            sql += " offset " + offset;
        }

        System.out.println("-------------- sql = " + sql);

        return sql;
    }

    public String getSqlWithSearchCriteria(Hashtable p_searchCriteria, int limit, int offset, int category,
                                           boolean isPermanentTable, UserDTO userDTO, String filter, boolean tableHasJobCat) {
        if (isPermanentTable) {
            return getSqlWithSearchCriteriaForNormalSearch(p_searchCriteria, limit, offset, category, userDTO, filter, tableHasJobCat);
        } else {
            return getSqlWithSearchCriteriaForApprovalSearch(p_searchCriteria, limit, offset, category, userDTO, filter, tableHasJobCat);
        }

    }

    public static String getFormTitle(Gate_passDTO gate_passDTO, String language) {
        String formTitle = "";
        Connection connection = null;
        ResultSet rs = null;
        Statement stmt = null;

        try {

            String query;
            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();


            query = "select gpt.name_eng as gpt_name_eng, gpt.name_bng as gpt_name_bng, gpst.name_eng as gpst_name_eng, gpst.name_bng as gpst_name_bng " +
                    "from gate_pass_type gpt join gate_pass_sub_type gpst on gpt.id = gpst.gate_pass_type_id where gpt.id = " + gate_passDTO.gatePassTypeId + " and gpst.id = " + gate_passDTO.gatePassSubTypeId;
            logger.debug(query);

            rs = stmt.executeQuery(query);
            if (rs.next()) {
                if (language.equalsIgnoreCase("English")) {
                    formTitle = rs.getString("gpt_name_eng") + " (" + rs.getString("gpst_name_eng") + ")";
                } else {
                    formTitle = rs.getString("gpt_name_bng") + " (" + rs.getString("gpst_name_bng") + ")";
                }
            }

            // if null then
            if (formTitle.equals("")) {
                query = "select name_eng, name_bng from gate_pass_type where id = " + gate_passDTO.gatePassTypeId;
                logger.debug("No sub type, so another query, must replace this inefficient way of getting data by Repository: " + query);

                rs = stmt.executeQuery(query);
                if (rs.next()) {
                    if (language.equalsIgnoreCase("English")) {
                        formTitle = rs.getString("name_eng");
                    } else {
                        formTitle = rs.getString("name_bng");
                    }
                }
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }

            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }

        return formTitle;
    }

    public long getId(long visitingDate, long gatePassTypeId, long gatePassSubTtypeId, long visitorId) {
        long gatePassId = 0;
        Connection connection = null;
        ResultSet rs = null;
        Statement stmt = null;

        try {

            String query = "select id from gate_pass where visiting_date=%d and gate_pass_type_id=%d and gate_pass_sub_type_id=%d and gate_pass_visitor_id=%d order by insertion_date";
            query = String.format(query, visitingDate, gatePassTypeId, gatePassSubTtypeId, visitorId);

            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();
            logger.debug(query);

            rs = stmt.executeQuery(query);
            if (rs.next()) {
                gatePassId = rs.getLong("id");
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }

            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }

        return gatePassId;
    }

    public GatePassModel getGatePassModelById(long id) {
        GatePassModel gatePassModel = null;
        long dataLongId;
        int dataIntId;
        String dataNameEng, dataNameBng;
        Connection connection = null;
        ResultSet rs = null;
        Statement stmt = null;

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

        try {

            String query = "select * from gate_pass as gp inner join gate_pass_visitor as gpv on gp.gate_pass_visitor_id = gpv.id where gp.id = %d";
            query = String.format(query, id);

            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();
            logger.debug(query);

            rs = stmt.executeQuery(query);


            gatePassModel = new GatePassModel();
            if (rs.next()) {

                gatePassModel.gatePassId = rs.getLong("gp.id");
                dataLongId = rs.getLong("gate_pass_type_id");
                gatePassModel.gatePassType = dataLongId;
                List<Gate_pass_typeDTO> gate_pass_typeDTOs = Gate_pass_typeRepository.getInstance().getGate_pass_typeList();
                gatePassModel.gatePassTypeEng = Gate_pass_typeRepository.getInstance().getGate_pass_typeDTOByid(dataLongId).nameEng;
                gatePassModel.gatePassTypeBng = Gate_pass_typeRepository.getInstance().getGate_pass_typeDTOByid(dataLongId).nameBng;
                gatePassModel.officeName = rs.getString("gpv.office_name");
                gatePassModel.designation = rs.getString("gpv.designation");
                gatePassModel.vehicleDescription = rs.getString("gpv.vehicle_description");
                gatePassModel.vehicleNumber = rs.getString("gpv.vehicle_number");
                gatePassModel.hasVehicle = rs.getBoolean("gpv.has_vehicle");

                if (dataLongId == 1) {
                    dataLongId = rs.getLong("gate_pass_sub_type_id");
                    gatePassModel.gatePassSubTypeEng = Gate_pass_sub_typeRepository.getInstance().getGate_pass_sub_typeDTOByid(dataLongId).nameEng;
                    gatePassModel.gatePassSubTypeBng = Gate_pass_sub_typeRepository.getInstance().getGate_pass_sub_typeDTOByid(dataLongId).nameBng;
                    gatePassModel.gatePassSubTypeBng = Gate_pass_sub_typeRepository.getInstance().getGate_pass_sub_typeDTOByid(dataLongId).nameBng;
                }

                dataLongId = rs.getLong("referrer_id");
                Employee_recordsDTO employee_recordsDTO = Employee_recordsRepository.getInstance().getById(dataLongId);
                if (employee_recordsDTO != null) {
                    gatePassModel.referrerEng = employee_recordsDTO.nameEng;
                    gatePassModel.referrerBng = employee_recordsDTO.nameBng;
                    gatePassModel.referrerSignature = employee_recordsDTO.signature;

                    long organogram = UserRepository.getInstance().getUserDtoByEmployeeRecordId(dataLongId).organogramID;
                    try {
                        gatePassModel.referrerOfficeEng = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(OfficeUnitOrganogramsRepository.getInstance().getById(organogram).office_unit_id).unitNameEng;
                        gatePassModel.referrerOfficeBng = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(OfficeUnitOrganogramsRepository.getInstance().getById(organogram).office_unit_id).unitNameBng;
                        gatePassModel.referrerOfficeOrgBng = OfficeUnitOrganogramsRepository.getInstance().getById(organogram).designation_bng;
                    } catch (Exception ex) {
                        gatePassModel.referrerOfficeEng = " ";
                        gatePassModel.referrerOfficeBng = " ";
                        gatePassModel.referrerOfficeOrgBng = " ";
                    }
                }

                dataLongId = rs.getLong("visiting_date");
                gatePassModel.visitingDateLongFormat = dataLongId;
                String formatted_visitingDate = simpleDateFormat.format(new Date(dataLongId));
                gatePassModel.visitingDateEng = Utils.getDigits(formatted_visitingDate, "English");
                gatePassModel.visitingDateBng = Utils.getDigits(formatted_visitingDate, "Bangla");

                dataLongId = rs.getLong("validity_from");
                formatted_visitingDate = simpleDateFormat.format(new Date(dataLongId));
                gatePassModel.validityFromEng = Utils.getDigits(formatted_visitingDate, "English");
                gatePassModel.validityFromBng = Utils.getDigits(formatted_visitingDate, "Bangla");

                dataLongId = rs.getLong("validity_to");
                formatted_visitingDate = simpleDateFormat.format(new Date(dataLongId));
                gatePassModel.validityToEng = Utils.getDigits(formatted_visitingDate, "English");
                gatePassModel.validityToBng = Utils.getDigits(formatted_visitingDate, "Bangla");

                dataLongId = rs.getLong("parliament_gate_id");
                Parliament_gateDTO parliament_gateDTO = Parliament_gateRepository.getInstance().getParliament_gateDTOByid(dataLongId);
                try {
                    gatePassModel.parliamentGateEng = parliament_gateDTO.nameEng + " (" + parliament_gateDTO.gateNumber + ")";
                    gatePassModel.parliamentGateBng = parliament_gateDTO.nameBng + " (" + englishToBanglaNumber(parliament_gateDTO.gateNumber) + ")";

                } catch (Exception ex) {
                    gatePassModel.parliamentGateEng = "";
                    gatePassModel.parliamentGateBng = "";
                }

                gatePassModel.visitorName = rs.getString("name");
                gatePassModel.visitorFatherName = rs.getString("father_name");
                gatePassModel.visitorMotherName = rs.getString("mother_name");
                gatePassModel.visitorMobileNumberEng = rs.getString("mobile_number");
                gatePassModel.visitorMobileNumberEng = gatePassModel.visitorMobileNumberEng.substring(2);
                gatePassModel.visitorMobileNumberBng = StringUtils.convertToBanNumber(gatePassModel.visitorMobileNumberEng);
                gatePassModel.visitorEmail = rs.getString("email");

                dataIntId = rs.getInt("credential_type");
                gatePassModel.visitorCredentialTypeEng = CatRepository.getInstance().getText("English", "credential", dataIntId);
                gatePassModel.visitorCredentialTypeBng = CatRepository.getInstance().getText("Bangla", "credential", dataIntId);
                gatePassModel.visitorCredentialNo = rs.getString("credential_no");

                dataIntId = rs.getInt("gender_type");
                gatePassModel.genderTypeEng = CatRepository.getInstance().getText("English", "gender", dataIntId);
                gatePassModel.genderTypeBng = CatRepository.getInstance().getText("Bangla", "gender", dataIntId);

                dataIntId = rs.getInt("officer_type");
                gatePassModel.officerTypeEng = CatRepository.getInstance().getText("English", "officer", dataIntId);
                gatePassModel.officerTypeBng = CatRepository.getInstance().getText("Bangla", "officer", dataIntId);

                Gate_pass_affiliated_personDAO gate_pass_affiliated_personDAO = new Gate_pass_affiliated_personDAO();
                Gate_pass_itemDAO gate_pass_itemDAO = new Gate_pass_itemDAO();
                gatePassModel.persons = gate_pass_affiliated_personDAO.getGatePassPersonModelsByGatePassId(id);
                gatePassModel.items = gate_pass_itemDAO.getGatePassItemModelByGatePassId(id);

                gatePassModel.permanentAddress = getGatePassVisitorAddressModel(rs.getString("permanent_address"));
                gatePassModel.currentAddress = getGatePassVisitorAddressModel(rs.getString("current_address"));

                gatePassModel.firstLayerApprovalStatus = rs.getInt("first_layer_approval_status");
                gatePassModel.firstLayerApprovedBy = rs.getString("first_layer_approved_by");
                gatePassModel.firstLayerApprovalTime = rs.getLong("first_layer_approval_time");

                gatePassModel.secondLayerApprovalStatus = rs.getInt("second_layer_approval_status");
                gatePassModel.secondLayerApprovedBy = rs.getString("second_layer_approved_by");
                gatePassModel.secondLayerApprovalTime = rs.getLong("second_layer_approval_time");

                try {
                    UserDTO sergeantUserDTO = UserRepository.getUserDTOByUserName(gatePassModel.secondLayerApprovedBy);
                    if(sergeantUserDTO != null)
                    {
                	   employee_recordsDTO = Employee_recordsRepository.getInstance().getById(sergeantUserDTO.employee_record_id);
                       if (employee_recordsDTO != null) {
                           gatePassModel.sergeantNameBng = employee_recordsDTO.nameBng;
                           gatePassModel.sergeantSignature = employee_recordsDTO.signature;

                           long organogram = sergeantUserDTO.organogramID;
                           
                           OfficeUnitOrganograms officeUnitOrganograms = OfficeUnitOrganogramsRepository.getInstance().getById(organogram);
                          
                           
                          
                           if(officeUnitOrganograms != null)
                           {
                        	   Office_unitsDTO office_unitsDTO = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(officeUnitOrganograms.office_unit_id);
                               if(office_unitsDTO != null)
                               {
                            	   gatePassModel.sergeantOfficeBng = office_unitsDTO.unitNameBng;
                               }
                        	   gatePassModel.sergeantOfficeOrgBng = officeUnitOrganograms.designation_bng;
                           }
                           
                           
                           
                       }
                    }
                 
                    
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                gatePassModel.remarks = rs.getString("remarks");

                try {
                    gatePassModel.creatorId = rs.getLong("gp.inserted_by");
                } catch (Exception ex) {
                    gatePassModel.creatorId = 0;
                }

                gatePassModel.reasonForVisit = rs.getString("gp.reason_for_visit");
                gatePassModel.isIssued = rs.getInt("gp.is_issued");
                gatePassModel.partiallyIssued = rs.getBoolean("partially_issued");

                System.out.println("gatePassModel:" + gatePassModel);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }

            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }

        return gatePassModel;
    }

    public GatePassModel getGatePassModelByIdWithCardNumber(long id, UserDTO receptionUserDTO, String holdingItemIdAmountString) {
        GatePassModel gatePassModel = null;
        long dataLongId;
        int dataIntId;
        String dataNameEng, dataNameBng;
        Connection connection = null;
        ResultSet rs = null;
        Statement stmt = null;

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

        try {

            String query = "select * from gate_pass as gp inner join gate_pass_visitor as gpv on gp.gate_pass_visitor_id = gpv.id where gp.id = %d";
            query = String.format(query, id);

            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();
            logger.debug(query);

            rs = stmt.executeQuery(query);


            gatePassModel = new GatePassModel();
            if (rs.next()) {

                gatePassModel.gatePassId = rs.getLong("gp.id");
                dataLongId = rs.getLong("gate_pass_type_id");
                gatePassModel.gatePassType = dataLongId;
                List<Gate_pass_typeDTO> gate_pass_typeDTOs = Gate_pass_typeRepository.getInstance().getGate_pass_typeList();
                gatePassModel.gatePassTypeEng = Gate_pass_typeRepository.getInstance().getGate_pass_typeDTOByid(dataLongId).nameEng;
                gatePassModel.gatePassTypeBng = Gate_pass_typeRepository.getInstance().getGate_pass_typeDTOByid(dataLongId).nameBng;

                if (dataLongId == 1) {
                    dataLongId = rs.getLong("gate_pass_sub_type_id");
                    gatePassModel.gatePassSubTypeEng = Gate_pass_sub_typeRepository.getInstance().getGate_pass_sub_typeDTOByid(dataLongId).nameEng;
                    gatePassModel.gatePassSubTypeBng = Gate_pass_sub_typeRepository.getInstance().getGate_pass_sub_typeDTOByid(dataLongId).nameBng;
                    gatePassModel.gatePassSubTypeBng = Gate_pass_sub_typeRepository.getInstance().getGate_pass_sub_typeDTOByid(dataLongId).nameBng;
                }

                dataLongId = rs.getLong("referrer_id");
                Employee_recordsDTO employee_recordsDTO = Employee_recordsRepository.getInstance().getById(dataLongId);
                if (employee_recordsDTO != null) {
                    gatePassModel.referrerEng = employee_recordsDTO.nameEng;
                    gatePassModel.referrerBng = employee_recordsDTO.nameBng;
                    gatePassModel.referrerSignature = employee_recordsDTO.signature;

                    long organogram = UserRepository.getInstance().getUserDtoByEmployeeRecordId(dataLongId).organogramID;
                    try {
                        gatePassModel.referrerOfficeEng = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(OfficeUnitOrganogramsRepository.getInstance().getById(organogram).office_unit_id).unitNameEng;
                    } catch (Exception ex) {
                        gatePassModel.referrerOfficeEng = "";
                    }

                    try {
                        gatePassModel.referrerOfficeBng = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(OfficeUnitOrganogramsRepository.getInstance().getById(organogram).office_unit_id).unitNameBng;
                    } catch (Exception ex) {
                        gatePassModel.referrerOfficeBng = "";
                    }

                    try {
                        gatePassModel.referrerOfficeOrgBng = OfficeUnitOrganogramsRepository.getInstance().getById(organogram).designation_bng;
                    } catch (Exception ex) {
                        gatePassModel.referrerOfficeOrgBng = "";
                    }
                }

                dataLongId = rs.getLong("visiting_date");
                gatePassModel.visitingDateLongFormat = dataLongId;
                String formatted_visitingDate = simpleDateFormat.format(new Date(dataLongId));
                gatePassModel.visitingDateEng = Utils.getDigits(formatted_visitingDate, "English");
                gatePassModel.visitingDateBng = Utils.getDigits(formatted_visitingDate, "Bangla");

                dataLongId = rs.getLong("validity_from");
                formatted_visitingDate = simpleDateFormat.format(new Date(dataLongId));
                gatePassModel.validityFromEng = Utils.getDigits(formatted_visitingDate, "English");
                gatePassModel.validityFromBng = Utils.getDigits(formatted_visitingDate, "Bangla");

                dataLongId = rs.getLong("validity_to");
                formatted_visitingDate = simpleDateFormat.format(new Date(dataLongId));
                gatePassModel.validityToEng = Utils.getDigits(formatted_visitingDate, "English");
                gatePassModel.validityToBng = Utils.getDigits(formatted_visitingDate, "Bangla");

                dataLongId = rs.getLong("parliament_gate_id");
                Parliament_gateDTO parliament_gateDTO = Parliament_gateRepository.getInstance().getParliament_gateDTOByid(dataLongId);
                try {
                    gatePassModel.parliamentGateEng = parliament_gateDTO.nameEng + " (" + parliament_gateDTO.gateNumber + ")";
                    gatePassModel.parliamentGateBng = parliament_gateDTO.nameBng + " (" + englishToBanglaNumber(parliament_gateDTO.gateNumber) + ")";

                } catch (Exception ex) {
                    gatePassModel.parliamentGateEng = "";
                    gatePassModel.parliamentGateBng = "";
                }

                gatePassModel.visitorName = rs.getString("name");
                gatePassModel.visitorFatherName = rs.getString("father_name");
                gatePassModel.visitorMotherName = rs.getString("mother_name");
                gatePassModel.visitorMobileNumberEng = rs.getString("mobile_number");
                gatePassModel.visitorMobileNumberEng = gatePassModel.visitorMobileNumberEng.substring(2);
                gatePassModel.visitorMobileNumberBng = StringUtils.convertToBanNumber(gatePassModel.visitorMobileNumberEng);
                gatePassModel.visitorEmail = rs.getString("email");

                dataIntId = rs.getInt("credential_type");
                gatePassModel.visitorCredentialTypeEng = CatRepository.getInstance().getText("English", "credential", dataIntId);
                gatePassModel.visitorCredentialTypeBng = CatRepository.getInstance().getText("Bangla", "credential", dataIntId);
                gatePassModel.visitorCredentialNo = rs.getString("credential_no");

                dataIntId = rs.getInt("gender_type");
                gatePassModel.genderTypeEng = CatRepository.getInstance().getText("English", "gender", dataIntId);
                gatePassModel.genderTypeBng = CatRepository.getInstance().getText("Bangla", "gender", dataIntId);

                dataIntId = rs.getInt("officer_type");
                gatePassModel.officerTypeEng = CatRepository.getInstance().getText("English", "officer", dataIntId);
                gatePassModel.officerTypeBng = CatRepository.getInstance().getText("Bangla", "officer", dataIntId);

                Gate_pass_affiliated_personDAO gate_pass_affiliated_personDAO = new Gate_pass_affiliated_personDAO();
                Gate_pass_itemDAO gate_pass_itemDAO = new Gate_pass_itemDAO();
                gatePassModel.persons = gate_pass_affiliated_personDAO.getGatePassPersonModelsByGatePassIdForCardNumber(id);
                gatePassModel.items = gate_pass_itemDAO.getHoldingItemsFromEncodedString(holdingItemIdAmountString);

                gatePassModel.permanentAddress = getGatePassVisitorAddressModel(rs.getString("permanent_address"));
                gatePassModel.currentAddress = getGatePassVisitorAddressModel(rs.getString("current_address"));

                gatePassModel.firstLayerApprovalStatus = rs.getInt("first_layer_approval_status");
                gatePassModel.firstLayerApprovedBy = rs.getString("first_layer_approved_by");
                gatePassModel.firstLayerApprovalTime = rs.getLong("first_layer_approval_time");

                gatePassModel.secondLayerApprovalStatus = rs.getInt("second_layer_approval_status");
                gatePassModel.secondLayerApprovedBy = rs.getString("second_layer_approved_by");
                gatePassModel.secondLayerApprovalTime = rs.getLong("second_layer_approval_time");

                try {
                    UserDTO sergeantUserDTO = receptionUserDTO;
                    employee_recordsDTO = Employee_recordsRepository.getInstance().getById(sergeantUserDTO.employee_record_id);
                    if (employee_recordsDTO != null) {
                        gatePassModel.sergeantNameBng = employee_recordsDTO.nameBng;
                        gatePassModel.sergeantSignature = employee_recordsDTO.signature;

                        long organogram = sergeantUserDTO.organogramID;

                        try {
                            gatePassModel.sergeantOfficeBng = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(OfficeUnitOrganogramsRepository.getInstance().getById(organogram).office_unit_id).unitNameBng;
                        } catch (Exception ex) {
                            gatePassModel.sergeantOfficeBng = "";
                        }

                        try {
                            gatePassModel.sergeantOfficeOrgBng = OfficeUnitOrganogramsRepository.getInstance().getById(organogram).designation_bng;
                        } catch (Exception ex) {
                            gatePassModel.sergeantOfficeOrgBng = "";
                        }


                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                gatePassModel.remarks = rs.getString("remarks");

                try {
                    gatePassModel.creatorId = rs.getLong("gp.inserted_by");
                } catch (Exception ex) {
                    gatePassModel.creatorId = 0;
                }

                gatePassModel.reasonForVisit = rs.getString("gp.reason_for_visit");
                gatePassModel.partiallyIssued = rs.getBoolean("partially_issued");
                gatePassModel.cardNumber = gate_pass_itemDAO.getCardNumberByVisitorName(gatePassModel.visitorName);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }

            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }

        return gatePassModel;
    }

    public int getCountByMobileNumber(String mobileNumber) {
        int count = 0;

        String sql = "SELECT count(distinct gate_pass.id) as count FROM gate_pass_visitor inner join gate_pass";
        sql += " on gate_pass.gate_pass_visitor_id = gate_pass_visitor.id ";
        sql += " WHERE gate_pass_visitor.mobile_number=" + mobileNumber + " AND gate_pass.visiting_date >= ";
        Date today = new Date();
        sql += (new Date(today.getYear(), today.getMonth(), today.getDate())).getTime();
//        sql += (new Date(2021-1900,3,14)).getTime();

        printSql(sql);

        Connection connection = null;
        Statement stmt = null;
        ResultSet rs = null;


        try {
            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();
            rs = stmt.executeQuery(sql);

            if (rs.next())
                count = rs.getInt("count");

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }

        return count;
    }

    public GatePassModel getGatePassModelByMobileNumber(String mobileNumber) {
        GatePassModel gatePassModel = null;
        long dataLongId;
        int dataIntId;
        String dataNameEng, dataNameBng;
        Connection connection = null;
        ResultSet rs = null;
        PreparedStatement ps = null;

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

        try {

            String query = "select * from gate_pass as gp inner join gate_pass_visitor as gpv on gp.gate_pass_visitor_id = gpv.id where gpv.mobile_number = ? order by gp.lastModificationTime";
            //query = String.format(query, mobileNumber);

            connection = DBMR.getInstance().getConnection();
            ps = connection.prepareStatement(query);
            ps.setString(1, mobileNumber);
            logger.debug(ps);

            rs = ps.executeQuery();


            gatePassModel = new GatePassModel();
            if (rs.next()) {

                gatePassModel.gatePassId = rs.getLong("gp.id");

                dataLongId = rs.getLong("gate_pass_type_id");
                List<Gate_pass_typeDTO> gate_pass_typeDTOs = Gate_pass_typeRepository.getInstance().getGate_pass_typeList();
                gatePassModel.gatePassTypeEng = Gate_pass_typeRepository.getInstance().getGate_pass_typeDTOByid(dataLongId).nameEng;
                gatePassModel.gatePassTypeBng = Gate_pass_typeRepository.getInstance().getGate_pass_typeDTOByid(dataLongId).nameBng;

                if (dataLongId == 1) {
                    dataLongId = rs.getLong("gate_pass_sub_type_id");
                    gatePassModel.gatePassSubTypeEng = Gate_pass_sub_typeRepository.getInstance().getGate_pass_sub_typeDTOByid(dataLongId).nameEng;
                    gatePassModel.gatePassSubTypeBng = Gate_pass_sub_typeRepository.getInstance().getGate_pass_sub_typeDTOByid(dataLongId).nameBng;
                }

                dataLongId = rs.getLong("referrer_id");
                Employee_recordsDAO employee_recordsDAO = new Employee_recordsDAO();
                Employee_recordsDTO employee_recordsDTO = employee_recordsDAO.getEmployee_recordsDTOByID(dataLongId);
                if (employee_recordsDTO != null) {
                    gatePassModel.referrerEng = employee_recordsDTO.nameEng;
                    gatePassModel.referrerBng = employee_recordsDTO.nameBng;
                    gatePassModel.referrerSignature = employee_recordsDTO.signature;

                    long organogram = UserRepository.getInstance().getUserDtoByEmployeeRecordId(dataLongId).organogramID;
                    gatePassModel.referrerOfficeEng = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(OfficeUnitOrganogramsRepository.getInstance().getById(organogram).office_unit_id).unitNameEng;
                    gatePassModel.referrerOfficeBng = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(OfficeUnitOrganogramsRepository.getInstance().getById(organogram).office_unit_id).unitNameBng;
                }

                dataLongId = rs.getLong("visiting_date");
                gatePassModel.visitingDateLongFormat = dataLongId;
                String formatted_visitingDate = simpleDateFormat.format(new Date(dataLongId));
                gatePassModel.visitingDateEng = Utils.getDigits(formatted_visitingDate, "English");
                gatePassModel.visitingDateBng = Utils.getDigits(formatted_visitingDate, "Bangla");

                dataLongId = rs.getLong("parliament_gate_id");
                Parliament_gateDTO parliament_gateDTO = Parliament_gateRepository.getInstance().getParliament_gateDTOByid(dataLongId);
                gatePassModel.parliamentGateEng = parliament_gateDTO.nameEng + " (" + parliament_gateDTO.gateNumber + ")";
                gatePassModel.parliamentGateBng = parliament_gateDTO.nameBng + " (" + englishToBanglaNumber(parliament_gateDTO.gateNumber) + ")";

                gatePassModel.visitorName = rs.getString("name");
                gatePassModel.visitorFatherName = rs.getString("father_name");
                gatePassModel.visitorMotherName = rs.getString("mother_name");
                gatePassModel.visitorMobileNumberEng = rs.getString("mobile_number");
                gatePassModel.visitorMobileNumberEng = gatePassModel.visitorMobileNumberEng.substring(2);
                gatePassModel.visitorMobileNumberBng = StringUtils.convertToBanNumber(gatePassModel.visitorMobileNumberEng);
                gatePassModel.visitorEmail = rs.getString("email");

                dataIntId = rs.getInt("credential_type");
                gatePassModel.visitorCredentialTypeEng = CatRepository.getInstance().getText("English", "credential", dataIntId);
                gatePassModel.visitorCredentialTypeBng = CatRepository.getInstance().getText("Bangla", "credential", dataIntId);
                gatePassModel.visitorCredentialNo = rs.getString("credential_no");

                dataIntId = rs.getInt("gender_type");
                gatePassModel.genderTypeEng = CatRepository.getInstance().getText("English", "gender", dataIntId);
                gatePassModel.genderTypeBng = CatRepository.getInstance().getText("Bangla", "gender", dataIntId);

                dataIntId = rs.getInt("officer_type");
                gatePassModel.officerTypeEng = CatRepository.getInstance().getText("English", "officer", dataIntId);
                gatePassModel.officerTypeBng = CatRepository.getInstance().getText("Bangla", "officer", dataIntId);

                Gate_pass_affiliated_personDAO gate_pass_affiliated_personDAO = new Gate_pass_affiliated_personDAO();
                Gate_pass_itemDAO gate_pass_itemDAO = new Gate_pass_itemDAO();
                dataLongId = rs.getLong("gp.id");
                gatePassModel.persons = gate_pass_affiliated_personDAO.getGatePassPersonModelsByGatePassId(dataLongId);
                gatePassModel.items = gate_pass_itemDAO.getGatePassItemModelByGatePassId(dataLongId);

                gatePassModel.permanentAddress = getGatePassVisitorAddressModel(rs.getString("permanent_address"));
                gatePassModel.currentAddress = getGatePassVisitorAddressModel(rs.getString("current_address"));

                gatePassModel.firstLayerApprovalStatus = rs.getInt("first_layer_approval_status");
                gatePassModel.firstLayerApprovedBy = rs.getString("first_layer_approved_by");
                gatePassModel.firstLayerApprovalTime = rs.getInt("first_layer_approval_time");
                gatePassModel.secondLayerApprovalStatus = rs.getInt("second_layer_approval_status");
                gatePassModel.secondLayerApprovedBy = rs.getString("second_layer_approved_by");
                gatePassModel.secondLayerApprovalTime = rs.getInt("second_layer_approval_time");
                gatePassModel.partiallyIssued = rs.getBoolean("partially_issued");

                System.out.println("gatePassModel:" + gatePassModel);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }

            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }

        return gatePassModel;
    }

    private String englishToBanglaNumber(String englishNumber) {
        char[] en = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
        char[] bn = {'০', '১', '২', '৩', '৪', '৫', '৬', '৭', '৮', '৯'};
        if (englishNumber == null || englishNumber.isEmpty()) {
            return "";
        }
        String banglaNumber = "";
        for (int i = 0; i < englishNumber.length(); i++) {
            for (int j = 0; j < en.length; j++) {
                if (englishNumber.charAt(i) == en[j]) {
                    banglaNumber += bn[j];
                    break;
                }
            }
        }
        if (banglaNumber.isEmpty()) banglaNumber = "0";
        return banglaNumber;
    }

    private GatePassVisitorAddress getGatePassVisitorAddressModel(String address) {
        if (address == null) {
            return new GatePassVisitorAddress();
        }
        String englishAddressString, banglaAddressString, addressText;
        String[] addresses, englishAddressStrings, banglaAddressStrings;
        int length;
        GatePassVisitorAddress gatePassVisitorAddress = new GatePassVisitorAddress();

        addresses = address.split(":");

        if(addresses == null || addresses.length < 4)
    	{
    		return new GatePassVisitorAddress();
    	}

        try {
            addressText = addresses[2];
            gatePassVisitorAddress.addressText = addressText;
        } catch (Exception e) {
            e.printStackTrace();
        }


        try {
            englishAddressString = addresses[1];
            englishAddressStrings = englishAddressString.split(",");
            reverseArray(englishAddressStrings);

            for (int i = 0; i < englishAddressStrings.length; ++i) {
                if (i == 0) {
                    gatePassVisitorAddress.divisionEng = englishAddressStrings[i];
                } else if (i == 1) {
                    gatePassVisitorAddress.districtEng = englishAddressStrings[i];
                } else if (i == 2) {
                    gatePassVisitorAddress.thanaEng = englishAddressStrings[i];
                } else {
                    gatePassVisitorAddress.unionEng = englishAddressStrings[i];
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            banglaAddressString = addresses[3];
            banglaAddressStrings = banglaAddressString.split(",");
            reverseArray(banglaAddressStrings);

            for (int i = 0; i < banglaAddressStrings.length; ++i) {
                if (i == 0) {
                    gatePassVisitorAddress.divisionBng = banglaAddressStrings[i];
                } else if (i == 1) {
                    gatePassVisitorAddress.districtBng = banglaAddressStrings[i];
                } else if (i == 2) {
                    gatePassVisitorAddress.thanaBng = banglaAddressStrings[i];
                } else {
                    gatePassVisitorAddress.unionBng = banglaAddressStrings[i];
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return gatePassVisitorAddress;
    }

    public void deleteGate_pass(long gate_pass_id) {

        String sql = "UPDATE gate_pass SET isDeleted = 1";
        sql += " WHERE id = " + gate_pass_id + " AND isDeleted = 0";

        printSql(sql);

        Connection connection = null;
        Statement stmt = null;

        try {
            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();
            stmt.executeUpdate(sql);

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
    }

    private void reverseArray(String[] arr) {
        List<String> list = Arrays.asList(arr);
        Collections.reverse(list);
        list.toArray(arr);
    }

    public String buildParliamentSessionDatesForGalleryPass(String language) {
        String options, dateText;
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        long today = c.getTimeInMillis();
        Date startDate, endDate;
        Calendar start, end;

        List<Parliament_sessionDTO> parliamentSessionDTOList = Parliament_sessionRepository.getInstance().getParliament_sessionList();
        List<OptionDTO> optionDTOList = new ArrayList<>();
        for (Parliament_sessionDTO parliamentSessionDTO : parliamentSessionDTOList) {
            if (parliamentSessionDTO.endDate > today) {
                if (parliamentSessionDTO.startDate > today) {
                    startDate = new Date(parliamentSessionDTO.startDate);
                    endDate = new Date(parliamentSessionDTO.endDate);
                    start = Calendar.getInstance();
                    end = Calendar.getInstance();
                    start.setTime(startDate);
                    end.setTime(endDate);
                } else {
                    startDate = new Date(today);
                    endDate = new Date(parliamentSessionDTO.endDate);
                    start = Calendar.getInstance();
                    end = Calendar.getInstance();
                    start.setTime(startDate);
                    end.setTime(endDate);
                }

                for (Date date = start.getTime(); !start.after(end); start.add(Calendar.DATE, 1), date = start.getTime()) {
                    dateText = sdf.format(date);
                    optionDTOList.add(new OptionDTO(dateText, StringUtils.convertToBanNumber(dateText), String.valueOf(date.getTime())));
                }
            }
        }


        return Utils.buildOptions(optionDTOList, language, null);
    }

    public long getGatePassGalleryId(long gatePassId) {
        Gate_passDAO gate_passDAO = new Gate_passDAO();
        Gate_passDTO gate_passDTO = gate_passDAO.getDTOByID(gatePassId);

        return gate_passDTO.gatePassGalleryId;
    }


    public GatePassDashboardDataModel getGatePassDashboardDataModel() {

        GatePassDashboardDataModel model = new GatePassDashboardDataModel();
        long currentTime, firstDayOfMonth, previousSixMonth;
        List<String> sqlApprovalMonthWise = new ArrayList<>();
        List<String> sqlApprovalVisitorMonthWise = new ArrayList<>();

        currentTime = System.currentTimeMillis();

        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 0); // ! clear would not reset the hour of day !
        cal.clear(Calendar.MINUTE);
        cal.clear(Calendar.SECOND);
        cal.clear(Calendar.MILLISECOND);
        // get start of the month
        cal.set(Calendar.DAY_OF_MONTH, 1);

        firstDayOfMonth = cal.getTimeInMillis();

        String rawsql = "select count(*) 'totalPass',\n" +
                "       COUNT(IF(first_layer_approval_status=1 and second_layer_approval_status=1, 1, null)) 'approved',\n" +
                "       COUNT(IF(first_layer_approval_status=2 or second_layer_approval_status=2, 1, null)) 'dismissed',\n" +
                "       COUNT(IF(first_layer_approval_status=1 and second_layer_approval_status=4, 1, NULL)) 'processing'\n" +
                "       from gate_pass where insertion_date >= %d and insertion_date <= %d";

        String sql = String.format(rawsql, firstDayOfMonth, currentTime);

        String rawsqlVisitor = "SELECT count(*) 'totalPass',\n" +
                "       COUNT(IF(first_layer_approval_status=1 and second_layer_approval_status=1, 1, null)) 'approved',\n" +
                "       COUNT(IF(first_layer_approval_status=2 or second_layer_approval_status=2, 1, null)) 'dismissed',\n" +
                "       COUNT(IF(first_layer_approval_status=3 and second_layer_approval_status != 2, 1, NULL)) 'received'\n" +
                "       from visitor_pass where insertion_date >= %d and insertion_date <= %d";

        String sqlVisitor = String.format(rawsqlVisitor, firstDayOfMonth, currentTime);

        String sqlPassType = "SELECT count(*) 'totalPass',\n" +
                "       COUNT(IF(gate_pass_type_id=1 and gate_pass_sub_type_id=10, 1, null)) 'areaPassVisitor',\n" +
                "       COUNT(IF(gate_pass_type_id=1 and gate_pass_sub_type_id=100, 1, null)) 'areaPassOfficial',\n" +
                "       COUNT(IF(gate_pass_type_id=10, 1, null)) 'visitorPass',\n" +
                "       COUNT(IF(gate_pass_type_id=11, 1, null)) 'galleryPass',\n" +
                "       COUNT(IF(gate_pass_type_id=110, 1, null)) 'meetingBriefingPass'\n" +
                "       from gate_pass where insertion_date >= %d and insertion_date <= %d";

        String sqlVisitorType = "SELECT count(*) 'totalPass',\n" +
                "       COUNT(IF(type_of_visitor=1, 1, null)) 'domestic',\n" +
                "       COUNT(IF(type_of_visitor=2, 1, null)) 'foreigner'\n" +
                "       from visitor_pass where insertion_date >= %d and insertion_date <= %d";

        Calendar cal2 = Calendar.getInstance();
        cal2.set(Calendar.HOUR_OF_DAY, 0); // ! clear would not reset the hour of day !
        cal2.clear(Calendar.MINUTE);
        cal2.clear(Calendar.SECOND);
        cal2.clear(Calendar.MILLISECOND);
        long time = firstDayOfMonth, timeNext;
        sqlApprovalMonthWise.add(sql);
        for (int i = 1; i <= 5; i++) {
            cal2.set(Calendar.MONTH, cal.get(Calendar.MONTH) - i);
            timeNext = time - 1;
            time = cal2.getTimeInMillis();
            sqlApprovalMonthWise.add(String.format(rawsql, time, timeNext));
        }

        sqlApprovalVisitorMonthWise.add(sqlVisitor);
        cal2 = Calendar.getInstance();
        cal2.set(Calendar.HOUR_OF_DAY, 0); // ! clear would not reset the hour of day !
        cal2.clear(Calendar.MINUTE);
        cal2.clear(Calendar.SECOND);
        cal2.clear(Calendar.MILLISECOND);
        time = firstDayOfMonth;
        for (int i = 1; i <= 5; i++) {
            cal2.set(Calendar.MONTH, cal.get(Calendar.MONTH) - i);
            timeNext = time - 1;
            time = cal2.getTimeInMillis();
            sqlApprovalVisitorMonthWise.add(String.format(rawsqlVisitor, time, timeNext));
        }

        cal.set(Calendar.MONTH, cal.get(Calendar.MONTH) - 5);
        System.out.println(cal.getTime());
        previousSixMonth = cal.getTimeInMillis();

        sqlPassType = String.format(sqlPassType, previousSixMonth, currentTime);
        sqlVisitorType = String.format(sqlVisitorType, previousSixMonth, currentTime);


//        System.out.println("SQLPASSTYPE:   " + sqlPassType);

        Connection connection = null;
        Statement stmt = null;
        ResultSet rs = null;

        try {
            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();
            rs = stmt.executeQuery(sql);

            if (rs.next()) {
                model.totalGatePassAppliedInThisMonth = rs.getInt("totalPass");
                model.approvedPassAppliedInThisMonth = rs.getInt("approved");
                model.dismissedGatePassAppliedInThisMonth = rs.getInt("dismissed");
                model.processedPassAppliedInThisMonth = rs.getInt("processing");
                model.pendingGatePassAppliedInThisMonth = model.totalGatePassAppliedInThisMonth - (model.approvedPassAppliedInThisMonth +
                        model.dismissedGatePassAppliedInThisMonth + model.processedPassAppliedInThisMonth);
            }

            rs = stmt.executeQuery(sqlVisitor);

            if (rs.next()) {
                model.totalVisitorPassAppliedInThisMonth = rs.getInt("totalPass");
                model.approvedVisitorPassAppliedInThisMonth = rs.getInt("approved");
                model.dismissedVisitorPassAppliedInThisMonth = rs.getInt("dismissed");
                model.receivedVisitorPassAppliedInThisMonth = rs.getInt("received");
                model.pendingVisitorPassAppliedInThisMonth = model.totalVisitorPassAppliedInThisMonth - (model.approvedVisitorPassAppliedInThisMonth +
                        model.dismissedVisitorPassAppliedInThisMonth + model.receivedVisitorPassAppliedInThisMonth);
            }

            rs = stmt.executeQuery(sqlPassType);

            if (rs.next()) {
                model.areaPassVisitor = rs.getInt("areaPassVisitor");
                model.areaPassOfficial = rs.getInt("areaPassOfficial");
                model.visitorPass = rs.getInt("visitorPass");
                model.galleryPass = rs.getInt("galleryPass");
                model.meetingBriefingPass = rs.getInt("meetingBriefingPass");
            }

            rs = stmt.executeQuery(sqlVisitorType);

            if (rs.next()) {
                model.visitorDomestic = rs.getInt("domestic");
                model.visitorForeigner = rs.getInt("foreigner");
            }

            for (int i = 0; i < 6; i++) {
                rs = stmt.executeQuery(sqlApprovalMonthWise.get(i));
//                System.out.println(sqlApprovalMonthWise.get(i));

                if (rs.next()) {
                    model.totalGatePassAppliedMonthWise.add((long) rs.getInt("totalPass"));
                    model.approvedPassAppliedMonthWise.add((long) rs.getInt("approved"));
                    model.dismissedGatePassAppliedMonthWise.add((long) rs.getInt("dismissed"));
                    model.processedPassAppliedMonthWise.add((long) rs.getInt("processing"));
                    model.pendingGatePassAppliedMonthWise.add(rs.getInt("totalPass") - ((long) rs.getInt("approved") +
                            rs.getInt("dismissed") + rs.getInt("processing")));
                }
            }

            for (int i = 0; i < 6; i++) {
                rs = stmt.executeQuery(sqlApprovalVisitorMonthWise.get(i));
                System.out.println(sqlApprovalVisitorMonthWise.get(i));

                if (rs.next()) {
                    model.totalVisitorPassAppliedMonthWise.add((long) rs.getInt("totalPass"));
                    model.approvedVisitorPassAppliedMonthWise.add((long) rs.getInt("approved"));
                    model.dismissedVisitorPassAppliedMonthWise.add((long) rs.getInt("dismissed"));
                    model.receivedVisitorPassAppliedMonthWise.add((long) rs.getInt("received"));
                    model.pendingVisitorPassAppliedMonthWise.add(rs.getInt("totalPass") - ((long) rs.getInt("approved") +
                            rs.getInt("dismissed") + rs.getInt("received")));
                }
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }

        return model;
    }

    public KeyCountDTO getKeyCount(ResultSet rs) {
        try {
            KeyCountDTO keyCountDTO = new KeyCountDTO();
            keyCountDTO.key = rs.getLong("key1");
            keyCountDTO.count = rs.getInt("count(id)");

            return keyCountDTO;
        } catch (SQLException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public List<KeyCountDTO> getTypeWiseCount() {
        String sql = "SELECT \r\n" +
                "    gate_pass_type_id AS key1,\r\n" +
                "    COUNT(id)\r\n" +
                "FROM\r\n" +
                "    gate_pass\r\n" +
                "WHERE\r\n" +
                "    isDeleted = 0 and is_issued = 1 ";

        sql += "GROUP BY key1\r\n";

        return ConnectionAndStatementUtil.getListOfT(sql, this::getKeyCount);
    }

	public KeyCountDTO getCounts(ResultSet rs)
	{
		try
		{
			KeyCountDTO keyCountDTO = new KeyCountDTO();
			keyCountDTO.count = rs.getInt("totalCount");
			keyCountDTO.count2 = rs.getInt("todaysCount");
			keyCountDTO.count3 = rs.getInt("currentMonthCount");

			return keyCountDTO;
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
			return null;
		}
	}
	
	public KeyCountDTO getPendingCounts ()
    {
		String sql = "SELECT \r\n" + 
				"    COUNT(gate_pass.id) AS totalCount,\r\n" + 
				"    SUM(CASE\r\n" + 
				"        WHEN gate_pass.insertion_date >= " + TimeConverter.getToday() + " THEN 1\r\n" + 
				"        ELSE 0\r\n" + 
				"    END) AS todaysCount,\r\n" + 
				"    SUM(CASE\r\n" + 
				"        WHEN gate_pass.insertion_date >= " + TimeConverter.get1stDayOfMonth() + " THEN 1\r\n" + 
				"        ELSE 0\r\n" + 
				"    END) AS currentMonthCount\r\n" + 
				"FROM\r\n" + 
				"    gate_pass inner join gate_pass_visitor ON gate_pass.gate_pass_visitor_id = gate_pass_visitor.id \r\n" + 
				"WHERE\r\n" + 
				"    gate_pass.isDeleted = 0 AND gate_pass.gate_pass_type_id != 1\r\n" + 
				"        AND gate_pass.first_layer_approval_status = 1";

        System.out.println("goccha: " + sql);
		return ConnectionAndStatementUtil.getT(sql,this::getCounts);	
    }
	
	public KeyCountDTO getApprovedCounts ()
    {
		String sql = "SELECT \r\n" + 
				"    COUNT(gate_pass.id) as totalCount,\r\n" + 
				
				"    SUM(CASE\r\n" + 
				"        WHEN second_layer_approval_time >= " + TimeConverter.getToday() 
				+ " and gate_pass.insertion_date >= " + TimeConverter.getToday()
				+ " THEN 1\r\n" + 
				"        ELSE 0\r\n" + 
				"    END) AS todaysCount,\r\n" + 
				
				"    SUM(CASE\r\n" + 
				"        WHEN second_layer_approval_time >= " + TimeConverter.get1stDayOfMonth() 
				+ " and gate_pass.insertion_date >= " + TimeConverter.get1stDayOfMonth() 
				+ " THEN 1\r\n" + 
				"        ELSE 0\r\n" + 
				"    END) AS currentMonthCount\r\n" + 
				"FROM\r\n" + 
				"    gate_pass inner join gate_pass_visitor ON gate_pass.gate_pass_visitor_id = gate_pass_visitor.id \r\n" + 
				"WHERE\r\n" + 
				"    gate_pass.isDeleted = 0 and second_layer_approval_status = 1 AND is_issued != 1\r\n" + 
				"        AND gate_pass.first_layer_approval_status = 1"
				+ " and gate_pass.gate_pass_type_id in (" + Gate_pass_typeDTO.BUILDING_VISITOR_PASS + ", " 
				+ Gate_pass_typeDTO.GALLERY_PASS + ") ";

		return ConnectionAndStatementUtil.getT(sql,this::getCounts);	
    }
	
	public KeyCountDTO getIssuedCounts ()
    {
		String sql = "SELECT \r\n" + 
				"    COUNT(gate_pass.id) as totalCount,\r\n" + 
				"    SUM(CASE\r\n" + 
				"        WHEN issue_date >= " + TimeConverter.getToday() + " THEN 1\r\n" + 
				"        ELSE 0\r\n" + 
				"    END) AS todaysCount,\r\n" + 
				"    SUM(CASE\r\n" + 
				"        WHEN issue_date >= " + TimeConverter.get1stDayOfMonth() + " THEN 1\r\n" + 
				"        ELSE 0\r\n" + 
				"    END) AS currentMonthCount\r\n" + 
				"FROM\r\n" + 
				"    gate_pass inner join gate_pass_visitor ON gate_pass.gate_pass_visitor_id = gate_pass_visitor.id \r\n" + 
				"WHERE\r\n" + 
				"    gate_pass.isDeleted = 0 and is_issued = 1";

		return ConnectionAndStatementUtil.getT(sql,this::getCounts);	
    }
	
	public KeyCountDTO get7DayCount(ResultSet rs)
	{
		try
		{
			KeyCountDTO keyCountDTO = new KeyCountDTO();
			keyCountDTO.key = rs.getLong("tdate");
			keyCountDTO.count = rs.getInt("count(id)");

			return keyCountDTO;
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
			return null;
		}
	}
	
	public List<KeyCountDTO> getLast7DayApprovalCount ()
    {
		String sql = "SELECT \r\n" + 
				"    ((second_layer_approval_time DIV 86400000) * 86400000 - 21600000) AS tdate,\r\n" + 
				"    COUNT(id)\r\n" + 
				"FROM\r\n" + 
				"    gate_pass\r\n" + 
				"WHERE\r\n" + 
				"    isDeleted = 0 and second_layer_approval_status = 1 ";
		
		sql += " and second_layer_approval_time >= " + TimeConverter.getNthDay(-7) + "\r\n" + 
				"GROUP BY tdate\r\n" + 
				"ORDER BY tdate ASC;";
		
		return ConnectionAndStatementUtil.getListOfT(sql,this::get7DayCount);	
    }
	
	public List<KeyCountDTO> getLast7DayIssueCount ()
    {
		String sql = "SELECT \r\n" + 
				"    ((issue_date DIV 86400000) * 86400000 - 21600000) AS tdate,\r\n" + 
				"    COUNT(id)\r\n" + 
				"FROM\r\n" + 
				"    gate_pass\r\n" + 
				"WHERE\r\n" + 
				"    isDeleted = 0 and is_issued = 1 ";
		
		sql += " and issue_date >= " + TimeConverter.getNthDay(-7) + "\r\n" + 
				"GROUP BY tdate\r\n" + 
				"ORDER BY tdate ASC;";
		
		return ConnectionAndStatementUtil.getListOfT(sql,this::get7DayCount);
    }
	
	public List<KeyCountDTO> getTypeWiseRequestedCount ()
    {
		String sql = "SELECT \r\n" + 
				"    gate_pass_type_id AS key1,\r\n" + 
				"    COUNT(id)\r\n" + 
				"FROM\r\n" + 
				"    gate_pass\r\n" + 
				"WHERE\r\n" + 
				"    isDeleted = 0 ";
		
		sql += "GROUP BY key1\r\n";
		
		return ConnectionAndStatementUtil.getListOfT(sql,this::getKeyCount);	
    }
	
	public KeyCountDTO getKeyCountStr(ResultSet rs)
	{
		try
		{
			KeyCountDTO keyCountDTO = new KeyCountDTO();
			keyCountDTO.keyStr = rs.getString("key1");
			keyCountDTO.count = rs.getInt("count(id)");

			return keyCountDTO;
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
			return null;
		}
	}
	
	public List<KeyCountDTO> getUserWiseCount ()
    {
		String sql = "SELECT \r\n" + 
				"    issued_by AS key1,\r\n" + 
				"    COUNT(id)\r\n" + 
				"FROM\r\n" + 
				"    gate_pass\r\n" + 
				"WHERE\r\n" + 
				"    isDeleted = 0 and is_issued = 1 ";
		
		sql += "GROUP BY key1\r\n";
		
		return ConnectionAndStatementUtil.getListOfT(sql,this::getKeyCountStr);	
    }
	
	public List<KeyCountDTO> getSergeantUserWiseCount ()
    {
		String sql = "SELECT \r\n" + 
				"    second_layer_approved_by AS key1,\r\n" + 
				"    COUNT(id)\r\n" + 
				"FROM\r\n" + 
				"    gate_pass\r\n" + 
				"WHERE\r\n" + 
				"    isDeleted = 0 and second_layer_approval_status = 1 ";
		
		sql += "GROUP BY key1\r\n";
		
		return ConnectionAndStatementUtil.getListOfT(sql,this::getKeyCountStr);	
    }

	public YearMonthCount getYmCount(ResultSet rs)
	{
		try
		{
			YearMonthCount ymCount = new YearMonthCount();
			String ym = rs.getString("ym");
			ymCount.year = Integer.parseInt(ym.split("-")[0]);
			ymCount.month = Integer.parseInt(ym.split("-")[1]);
			ymCount.count = rs.getInt("count(id)");
			
			//System.out.println("year = " + ymCount.year + " month = " + ymCount.month + " count = " + ymCount.count);

			return ymCount;
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
			return null;
		}
	}

	public List<YearMonthCount> getLast6MonthIssuedCount ()
    {
		String sql = "SELECT \r\n" + 
				"    DATE_FORMAT(FROM_UNIXTIME(`issue_date` / 1000),\r\n" + 
				"            '%Y-%m') AS ym,\r\n" + 
				"    COUNT(id)\r\n" + 
				"FROM\r\n" + 
				"    gate_pass\r\n" + 
				"WHERE\r\n" + 
				"    issue_date >= " + TimeConverter.get1stDayOfNthtMonth(-6) + " AND isDeleted = 0 and is_issued = 1\r\n" + 
				"GROUP BY ym\r\n";

		return ConnectionAndStatementUtil.getListOfT(sql,this::getYmCount);
    }

    public List<YearMonthCount> getLast6MonthApprovedCount() {
        String sql = "SELECT \r\n" +
                "    DATE_FORMAT(FROM_UNIXTIME(`second_layer_approval_time` / 1000),\r\n" +
                "            '%Y-%m') AS ym,\r\n" +
                "    COUNT(id)\r\n" +
                "FROM\r\n" +
                "    gate_pass\r\n" +
                "WHERE\r\n" +
                "    second_layer_approval_time >= " + TimeConverter.get1stDayOfNthtMonth(-6) + " AND isDeleted = 0 and second_layer_approval_status = 1\r\n" +
                "GROUP BY ym\r\n";

        return ConnectionAndStatementUtil.getListOfT(sql, this::getYmCount);
    }
	
	public List<YearMonthCount> getLast6MonthRequestedCount ()
    {
		String sql = "SELECT \r\n" + 
				"    DATE_FORMAT(FROM_UNIXTIME(`insertion_date` / 1000),\r\n" + 
				"            '%Y-%m') AS ym,\r\n" + 
				"    COUNT(id)\r\n" + 
				"FROM\r\n" + 
				"    gate_pass\r\n" + 
				"WHERE\r\n" + 
				"    insertion_date >= " + TimeConverter.get1stDayOfNthtMonth(-6) + " AND isDeleted = 0\r\n" + 
				"GROUP BY ym\r\n";
		
		//System.out.println(sql);
		return ConnectionAndStatementUtil.getListOfT(sql,this::getYmCount);	
    }

  

    public boolean isOriginalSergeantAtArmsApprover(UserDTO userDTO) {

        /*// if he is admin or gate pass admin -> authorized to see the search result at arms office
        if (userDTO.roleID == 1 || userDTO.roleID == SessionConstants.GATE_MANAGEMENT_ADMIN_ROLE) return true;

        // other wise if he is a authorized member of sergeant at arms office

        return TaskTypeApprovalPathRepository.getInstance().getByTaskTypeIdAndLevel(
                TaskTypeEnum.SERGEANT_AT_ARMS_APPROVAL.getValue(), 1).stream().anyMatch(
                taskTypeApprovalPathDTO -> userDTO.organogramID == taskTypeApprovalPathDTO.officeUnitOrganogramId);*/

        return userDTO.roleID == SessionConstants.ADMIN_ROLE
                || userDTO.roleID == SessionConstants.GATE_MANAGEMENT_ADMIN_ROLE
                || userDTO.roleID == SessionConstants.SERJEANT_AT_ARMS_ROLE;

    }

    public void sentMail(Gate_passDTO dto) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

//        Notifier notifier = new Notifier();
        String[] to = new String[1];
        Pb_notificationsDAO pb_notificationsDAO = Pb_notificationsDAO.getInstance();
        String subject = "Bangladesh Parliament GATE PASS approval notification";

        Gate_pass_visitorDAO dao = new Gate_pass_visitorDAO();
        Gate_pass_visitorDTO gate_pass_visitorDTO = dao.getDTOByID(dto.gatePassVisitorId);
        String approvalStatus;
        if (dto.gatePassTypeId == 1) {
            approvalStatus = dto.firstLayerApprovalStatus == approvalStatusApproved ? "approved" : "dismissed";
        } else {
            if (dto.secondLayerApprovalStatus == approvalStatusApproved)
                approvalStatus = "approved";
            else if (dto.secondLayerApprovalStatus == approvalStatusCancelled)
                approvalStatus = "dismissed";
            else
                approvalStatus = "stacked for further processing";
        }

        String messageBody;
        if (approvalStatus.equalsIgnoreCase("approved")) {
            messageBody = "Dear " + gate_pass_visitorDTO.name + ",<br><br>Your applied Gate Pass (" + sdf.format(new Date(dto.visitingDate))
                    + ") has been " + approvalStatus + "! Please bring a valid Photo ID at the time of your Visit." +
                    "<br><br>Sincerely, <br>Bangladesh Parliament";
        } else {
            messageBody = "Dear " + gate_pass_visitorDTO.name + ",<br><br>We are sorry to inform you that your applied Gate Pass (" + sdf.format(new Date(dto.visitingDate))
                    + ") has been " + approvalStatus + "! <br><br>Sincerely, <br>Bangladesh Parliament";
        }

        to[0] = gate_pass_visitorDTO.email;

        try {
            logger.debug("in send mail ..." + messageBody);
            if (to == null) return;
//            notifier.sendEmail(to, EmailConstant.RECEIVER_SYSTEM_USER,"",subject, messageBody);
            pb_notificationsDAO.sendMail(to, subject, messageBody, -1, null);
            logger.debug("mail pushed successfully..." + messageBody);
        } catch (Exception ex) {
            logger.fatal("", ex);
        }
    }

    public void sendApprovalSMS(Gate_passDTO dto) {
        String applierMobileNumber, referrerMobileNumber, applierMsgBody, referrerMsgBody;

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

        Gate_pass_visitorDAO dao = new Gate_pass_visitorDAO();
        Gate_pass_visitorDTO gate_pass_visitorDTO = dao.getDTOByID(dto.gatePassVisitorId);
        Employee_recordsDTO employee_recordsDTO = Employee_recordsRepository.getInstance().getById(dto.referrerId);

        applierMobileNumber = gate_pass_visitorDTO.mobileNumber;
        referrerMobileNumber = employee_recordsDTO.personalMobile;
        applierMsgBody = "Dear " + gate_pass_visitorDTO.name + ",\nYour applied Parliament Gate Pass (" +
                (dto.gatePassTypeId == 1 && dto.gatePassSubTypeId == 100 ? simpleDateFormat.format(new Date(dto.validityFrom)) + " to " +
                        simpleDateFormat.format(new Date(dto.validityTo)) : simpleDateFormat.format(new Date(dto.visitingDate)))
                + ") has been approved!";
        referrerMsgBody = "Dear " + employee_recordsDTO.nameEng + ",\nYour referred Parliament Gate Pass (" +
                (dto.gatePassTypeId == 1 && dto.gatePassSubTypeId == 100 ? simpleDateFormat.format(new Date(dto.validityFrom)) + " to " +
                        simpleDateFormat.format(new Date(dto.validityTo)) : simpleDateFormat.format(new Date(dto.visitingDate)))
                + ") has been approved!";

        try {
            SmsService.send(applierMobileNumber, applierMsgBody);
            SmsService.send(referrerMobileNumber, referrerMsgBody);
        } catch (IOException ioException) {
            logger.debug("Can't send sms on Gate Pass Approval");
        }

    }

    public void deleteAffiliatedPerson(String affiliatedPersonIds) throws Exception {
        Gate_pass_affiliated_personDAO dao = new Gate_pass_affiliated_personDAO();
        String[] IdList = affiliatedPersonIds.split(";");

        for (String id : IdList)
            try {
                dao.delete(Long.parseLong(id));
            } catch (NumberFormatException nfe) {
                logger.debug(nfe);
            }
    }
}
	