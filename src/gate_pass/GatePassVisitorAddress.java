package gate_pass;

public class GatePassVisitorAddress {
    public String addressText = "";
    public String unionEng = "";
    public String unionBng = "";
    public String thanaEng = "";
    public String thanaBng = "";
    public String districtEng = "";
    public String districtBng = "";
    public String divisionEng = "";
    public String divisionBng = "";

    @Override
    public String toString() {
        return "GatePassVisitorAddress[" +
                " addressText = " + addressText +
                " union = " + unionEng +
                " thana = " + thanaEng +
                " district = " + districtEng +
                " division = " + divisionEng +
                "]";
    }
}
