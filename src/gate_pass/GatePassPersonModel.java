package gate_pass;

public class GatePassPersonModel {
    public String mobileNoEng = "";
    public String mobileNoBng = "";
    public String credentialTypeEng = "";
    public String credentialTypeBng = "";
    public String credentialNo = "";
    public String name = "";
    public long id = 0;
    public String cardNumber = "";
    public String officeName = "";
    public String designation = "";


    @Override
    public String toString() {
        return "GatePassPersonModel[" +
                " mobileNoEng = " + mobileNoEng +
                " credentialTypeEng = " + credentialTypeEng +
                "]";
    }

}
