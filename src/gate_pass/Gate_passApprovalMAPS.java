package gate_pass;
import util.*;

public class Gate_passApprovalMAPS extends CommonMaps
{	
	public Gate_passApprovalMAPS(String tableName)
	{
		
		java_allfield_type_map.put("gate_pass_visitor_id".toLowerCase(), "Long");
		java_allfield_type_map.put("visiting_date".toLowerCase(), "Long");

		java_allfield_type_map.put("job_cat", "Integer");
		java_allfield_type_map.put("approval_status_cat", "Integer");
		java_allfield_type_map.put("initiator", "Long");
		java_allfield_type_map.put("assigned_to", "Long");
		java_allfield_type_map.put("starting_date", "Long");
		java_allfield_type_map.put("ending_date", "Long");
		
		java_table_map.put("approval_status_cat", "approval_summary");
		java_table_map.put("initiator", "approval_summary");
		java_table_map.put("assigned_to", "approval_summary");
	}

}