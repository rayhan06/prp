package gate_pass;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class Gate_passRepository implements Repository {
	Gate_passDAO gate_passDAO = new Gate_passDAO();
	
	public void setDAO(Gate_passDAO gate_passDAO)
	{
		this.gate_passDAO = gate_passDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Gate_passRepository.class);
	Map<Long, Gate_passDTO>mapOfGate_passDTOToid;
	Map<Long, Set<Gate_passDTO> >mapOfGate_passDTOTogatePassVisitorId;
	Map<Long, Set<Gate_passDTO> >mapOfGate_passDTOTovisitingDate;
	Map<Long, Set<Gate_passDTO> >mapOfGate_passDTOTogatePassTypeId;
	Map<Long, Set<Gate_passDTO> >mapOfGate_passDTOTogatePassSubTypeId;
	Map<Long, Set<Gate_passDTO> >mapOfGate_passDTOToreferrerId;
	Map<Long, Set<Gate_passDTO> >mapOfGate_passDTOToparliamentGateId;
	Map<Long, Set<Gate_passDTO> >mapOfGate_passDTOTovalidityFrom;
	Map<Long, Set<Gate_passDTO> >mapOfGate_passDTOTovalidityTo;
	Map<Integer, Set<Gate_passDTO> >mapOfGate_passDTOTojobCat;
	Map<Long, Set<Gate_passDTO> >mapOfGate_passDTOToinsertionDate;
	Map<String, Set<Gate_passDTO> >mapOfGate_passDTOToinsertedBy;
	Map<String, Set<Gate_passDTO> >mapOfGate_passDTOTomodifiedBy;
	Map<Long, Set<Gate_passDTO> >mapOfGate_passDTOTolastModificationTime;


	static Gate_passRepository instance = null;  
	private Gate_passRepository(){
		mapOfGate_passDTOToid = new ConcurrentHashMap<>();
		mapOfGate_passDTOTogatePassVisitorId = new ConcurrentHashMap<>();
		mapOfGate_passDTOTovisitingDate = new ConcurrentHashMap<>();
		mapOfGate_passDTOTogatePassTypeId = new ConcurrentHashMap<>();
		mapOfGate_passDTOTogatePassSubTypeId = new ConcurrentHashMap<>();
		mapOfGate_passDTOToreferrerId = new ConcurrentHashMap<>();
		mapOfGate_passDTOToparliamentGateId = new ConcurrentHashMap<>();
		mapOfGate_passDTOTovalidityFrom = new ConcurrentHashMap<>();
		mapOfGate_passDTOTovalidityTo = new ConcurrentHashMap<>();
		mapOfGate_passDTOTojobCat = new ConcurrentHashMap<>();
		mapOfGate_passDTOToinsertionDate = new ConcurrentHashMap<>();
		mapOfGate_passDTOToinsertedBy = new ConcurrentHashMap<>();
		mapOfGate_passDTOTomodifiedBy = new ConcurrentHashMap<>();
		mapOfGate_passDTOTolastModificationTime = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Gate_passRepository getInstance(){
		if (instance == null){
			instance = new Gate_passRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(gate_passDAO == null)
		{
			return;
		}
		try {
			List<Gate_passDTO> gate_passDTOs = gate_passDAO.getAllGate_pass(reloadAll);
			for(Gate_passDTO gate_passDTO : gate_passDTOs) {
				Gate_passDTO oldGate_passDTO = getGate_passDTOByid(gate_passDTO.iD);
				if( oldGate_passDTO != null ) {
					mapOfGate_passDTOToid.remove(oldGate_passDTO.iD);
				
					if(mapOfGate_passDTOTogatePassVisitorId.containsKey(oldGate_passDTO.gatePassVisitorId)) {
						mapOfGate_passDTOTogatePassVisitorId.get(oldGate_passDTO.gatePassVisitorId).remove(oldGate_passDTO);
					}
					if(mapOfGate_passDTOTogatePassVisitorId.get(oldGate_passDTO.gatePassVisitorId).isEmpty()) {
						mapOfGate_passDTOTogatePassVisitorId.remove(oldGate_passDTO.gatePassVisitorId);
					}
					
					if(mapOfGate_passDTOTovisitingDate.containsKey(oldGate_passDTO.visitingDate)) {
						mapOfGate_passDTOTovisitingDate.get(oldGate_passDTO.visitingDate).remove(oldGate_passDTO);
					}
					if(mapOfGate_passDTOTovisitingDate.get(oldGate_passDTO.visitingDate).isEmpty()) {
						mapOfGate_passDTOTovisitingDate.remove(oldGate_passDTO.visitingDate);
					}
					
					if(mapOfGate_passDTOTogatePassTypeId.containsKey(oldGate_passDTO.gatePassTypeId)) {
						mapOfGate_passDTOTogatePassTypeId.get(oldGate_passDTO.gatePassTypeId).remove(oldGate_passDTO);
					}
					if(mapOfGate_passDTOTogatePassTypeId.get(oldGate_passDTO.gatePassTypeId).isEmpty()) {
						mapOfGate_passDTOTogatePassTypeId.remove(oldGate_passDTO.gatePassTypeId);
					}
					
					if(mapOfGate_passDTOTogatePassSubTypeId.containsKey(oldGate_passDTO.gatePassSubTypeId)) {
						mapOfGate_passDTOTogatePassSubTypeId.get(oldGate_passDTO.gatePassSubTypeId).remove(oldGate_passDTO);
					}
					if(mapOfGate_passDTOTogatePassSubTypeId.get(oldGate_passDTO.gatePassSubTypeId).isEmpty()) {
						mapOfGate_passDTOTogatePassSubTypeId.remove(oldGate_passDTO.gatePassSubTypeId);
					}
					
					if(mapOfGate_passDTOToreferrerId.containsKey(oldGate_passDTO.referrerId)) {
						mapOfGate_passDTOToreferrerId.get(oldGate_passDTO.referrerId).remove(oldGate_passDTO);
					}
					if(mapOfGate_passDTOToreferrerId.get(oldGate_passDTO.referrerId).isEmpty()) {
						mapOfGate_passDTOToreferrerId.remove(oldGate_passDTO.referrerId);
					}
					
					if(mapOfGate_passDTOToparliamentGateId.containsKey(oldGate_passDTO.parliamentGateId)) {
						mapOfGate_passDTOToparliamentGateId.get(oldGate_passDTO.parliamentGateId).remove(oldGate_passDTO);
					}
					if(mapOfGate_passDTOToparliamentGateId.get(oldGate_passDTO.parliamentGateId).isEmpty()) {
						mapOfGate_passDTOToparliamentGateId.remove(oldGate_passDTO.parliamentGateId);
					}
					
					if(mapOfGate_passDTOTovalidityFrom.containsKey(oldGate_passDTO.validityFrom)) {
						mapOfGate_passDTOTovalidityFrom.get(oldGate_passDTO.validityFrom).remove(oldGate_passDTO);
					}
					if(mapOfGate_passDTOTovalidityFrom.get(oldGate_passDTO.validityFrom).isEmpty()) {
						mapOfGate_passDTOTovalidityFrom.remove(oldGate_passDTO.validityFrom);
					}
					
					if(mapOfGate_passDTOTovalidityTo.containsKey(oldGate_passDTO.validityTo)) {
						mapOfGate_passDTOTovalidityTo.get(oldGate_passDTO.validityTo).remove(oldGate_passDTO);
					}
					if(mapOfGate_passDTOTovalidityTo.get(oldGate_passDTO.validityTo).isEmpty()) {
						mapOfGate_passDTOTovalidityTo.remove(oldGate_passDTO.validityTo);
					}
					
					if(mapOfGate_passDTOTojobCat.containsKey(oldGate_passDTO.jobCat)) {
						mapOfGate_passDTOTojobCat.get(oldGate_passDTO.jobCat).remove(oldGate_passDTO);
					}
					if(mapOfGate_passDTOTojobCat.get(oldGate_passDTO.jobCat).isEmpty()) {
						mapOfGate_passDTOTojobCat.remove(oldGate_passDTO.jobCat);
					}
					
					if(mapOfGate_passDTOToinsertionDate.containsKey(oldGate_passDTO.insertionDate)) {
						mapOfGate_passDTOToinsertionDate.get(oldGate_passDTO.insertionDate).remove(oldGate_passDTO);
					}
					if(mapOfGate_passDTOToinsertionDate.get(oldGate_passDTO.insertionDate).isEmpty()) {
						mapOfGate_passDTOToinsertionDate.remove(oldGate_passDTO.insertionDate);
					}
					
					if(mapOfGate_passDTOToinsertedBy.containsKey(oldGate_passDTO.insertedBy)) {
						mapOfGate_passDTOToinsertedBy.get(oldGate_passDTO.insertedBy).remove(oldGate_passDTO);
					}
					if(mapOfGate_passDTOToinsertedBy.get(oldGate_passDTO.insertedBy).isEmpty()) {
						mapOfGate_passDTOToinsertedBy.remove(oldGate_passDTO.insertedBy);
					}
					
					if(mapOfGate_passDTOTomodifiedBy.containsKey(oldGate_passDTO.modifiedBy)) {
						mapOfGate_passDTOTomodifiedBy.get(oldGate_passDTO.modifiedBy).remove(oldGate_passDTO);
					}
					if(mapOfGate_passDTOTomodifiedBy.get(oldGate_passDTO.modifiedBy).isEmpty()) {
						mapOfGate_passDTOTomodifiedBy.remove(oldGate_passDTO.modifiedBy);
					}
					
					if(mapOfGate_passDTOTolastModificationTime.containsKey(oldGate_passDTO.lastModificationTime)) {
						mapOfGate_passDTOTolastModificationTime.get(oldGate_passDTO.lastModificationTime).remove(oldGate_passDTO);
					}
					if(mapOfGate_passDTOTolastModificationTime.get(oldGate_passDTO.lastModificationTime).isEmpty()) {
						mapOfGate_passDTOTolastModificationTime.remove(oldGate_passDTO.lastModificationTime);
					}
					
					
				}
				if(gate_passDTO.isDeleted == 0) 
				{
					
					mapOfGate_passDTOToid.put(gate_passDTO.iD, gate_passDTO);
				
					if( ! mapOfGate_passDTOTogatePassVisitorId.containsKey(gate_passDTO.gatePassVisitorId)) {
						mapOfGate_passDTOTogatePassVisitorId.put(gate_passDTO.gatePassVisitorId, new HashSet<>());
					}
					mapOfGate_passDTOTogatePassVisitorId.get(gate_passDTO.gatePassVisitorId).add(gate_passDTO);
					
					if( ! mapOfGate_passDTOTovisitingDate.containsKey(gate_passDTO.visitingDate)) {
						mapOfGate_passDTOTovisitingDate.put(gate_passDTO.visitingDate, new HashSet<>());
					}
					mapOfGate_passDTOTovisitingDate.get(gate_passDTO.visitingDate).add(gate_passDTO);
					
					if( ! mapOfGate_passDTOTogatePassTypeId.containsKey(gate_passDTO.gatePassTypeId)) {
						mapOfGate_passDTOTogatePassTypeId.put(gate_passDTO.gatePassTypeId, new HashSet<>());
					}
					mapOfGate_passDTOTogatePassTypeId.get(gate_passDTO.gatePassTypeId).add(gate_passDTO);
					
					if( ! mapOfGate_passDTOTogatePassSubTypeId.containsKey(gate_passDTO.gatePassSubTypeId)) {
						mapOfGate_passDTOTogatePassSubTypeId.put(gate_passDTO.gatePassSubTypeId, new HashSet<>());
					}
					mapOfGate_passDTOTogatePassSubTypeId.get(gate_passDTO.gatePassSubTypeId).add(gate_passDTO);
					
					if( ! mapOfGate_passDTOToreferrerId.containsKey(gate_passDTO.referrerId)) {
						mapOfGate_passDTOToreferrerId.put(gate_passDTO.referrerId, new HashSet<>());
					}
					mapOfGate_passDTOToreferrerId.get(gate_passDTO.referrerId).add(gate_passDTO);
					
					if( ! mapOfGate_passDTOToparliamentGateId.containsKey(gate_passDTO.parliamentGateId)) {
						mapOfGate_passDTOToparliamentGateId.put(gate_passDTO.parliamentGateId, new HashSet<>());
					}
					mapOfGate_passDTOToparliamentGateId.get(gate_passDTO.parliamentGateId).add(gate_passDTO);
					
					if( ! mapOfGate_passDTOTovalidityFrom.containsKey(gate_passDTO.validityFrom)) {
						mapOfGate_passDTOTovalidityFrom.put(gate_passDTO.validityFrom, new HashSet<>());
					}
					mapOfGate_passDTOTovalidityFrom.get(gate_passDTO.validityFrom).add(gate_passDTO);
					
					if( ! mapOfGate_passDTOTovalidityTo.containsKey(gate_passDTO.validityTo)) {
						mapOfGate_passDTOTovalidityTo.put(gate_passDTO.validityTo, new HashSet<>());
					}
					mapOfGate_passDTOTovalidityTo.get(gate_passDTO.validityTo).add(gate_passDTO);
					
					if( ! mapOfGate_passDTOTojobCat.containsKey(gate_passDTO.jobCat)) {
						mapOfGate_passDTOTojobCat.put(gate_passDTO.jobCat, new HashSet<>());
					}
					mapOfGate_passDTOTojobCat.get(gate_passDTO.jobCat).add(gate_passDTO);
					
					if( ! mapOfGate_passDTOToinsertionDate.containsKey(gate_passDTO.insertionDate)) {
						mapOfGate_passDTOToinsertionDate.put(gate_passDTO.insertionDate, new HashSet<>());
					}
					mapOfGate_passDTOToinsertionDate.get(gate_passDTO.insertionDate).add(gate_passDTO);
					
					if( ! mapOfGate_passDTOToinsertedBy.containsKey(gate_passDTO.insertedBy)) {
						mapOfGate_passDTOToinsertedBy.put(gate_passDTO.insertedBy, new HashSet<>());
					}
					mapOfGate_passDTOToinsertedBy.get(gate_passDTO.insertedBy).add(gate_passDTO);
					
					if( ! mapOfGate_passDTOTomodifiedBy.containsKey(gate_passDTO.modifiedBy)) {
						mapOfGate_passDTOTomodifiedBy.put(gate_passDTO.modifiedBy, new HashSet<>());
					}
					mapOfGate_passDTOTomodifiedBy.get(gate_passDTO.modifiedBy).add(gate_passDTO);
					
					if( ! mapOfGate_passDTOTolastModificationTime.containsKey(gate_passDTO.lastModificationTime)) {
						mapOfGate_passDTOTolastModificationTime.put(gate_passDTO.lastModificationTime, new HashSet<>());
					}
					mapOfGate_passDTOTolastModificationTime.get(gate_passDTO.lastModificationTime).add(gate_passDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Gate_passDTO> getGate_passList() {
		List <Gate_passDTO> gate_passs = new ArrayList<Gate_passDTO>(this.mapOfGate_passDTOToid.values());
		return gate_passs;
	}
	
	
	public Gate_passDTO getGate_passDTOByid( long id){
		return mapOfGate_passDTOToid.get(id);
	}
	
	
	public List<Gate_passDTO> getGate_passDTOBygate_pass_visitor_id(long gate_pass_visitor_id) {
		return new ArrayList<>( mapOfGate_passDTOTogatePassVisitorId.getOrDefault(gate_pass_visitor_id,new HashSet<>()));
	}
	
	
	public List<Gate_passDTO> getGate_passDTOByvisiting_date(long visiting_date) {
		return new ArrayList<>( mapOfGate_passDTOTovisitingDate.getOrDefault(visiting_date,new HashSet<>()));
	}
	
	
	public List<Gate_passDTO> getGate_passDTOBygate_pass_type_id(long gate_pass_type_id) {
		return new ArrayList<>( mapOfGate_passDTOTogatePassTypeId.getOrDefault(gate_pass_type_id,new HashSet<>()));
	}
	
	
	public List<Gate_passDTO> getGate_passDTOBygate_pass_sub_type_id(long gate_pass_sub_type_id) {
		return new ArrayList<>( mapOfGate_passDTOTogatePassSubTypeId.getOrDefault(gate_pass_sub_type_id,new HashSet<>()));
	}
	
	
	public List<Gate_passDTO> getGate_passDTOByreferrer_id(long referrer_id) {
		return new ArrayList<>( mapOfGate_passDTOToreferrerId.getOrDefault(referrer_id,new HashSet<>()));
	}
	
	
	public List<Gate_passDTO> getGate_passDTOByparliament_gate_id(long parliament_gate_id) {
		return new ArrayList<>( mapOfGate_passDTOToparliamentGateId.getOrDefault(parliament_gate_id,new HashSet<>()));
	}
	
	
	public List<Gate_passDTO> getGate_passDTOByvalidity_from(long validity_from) {
		return new ArrayList<>( mapOfGate_passDTOTovalidityFrom.getOrDefault(validity_from,new HashSet<>()));
	}
	
	
	public List<Gate_passDTO> getGate_passDTOByvalidity_to(long validity_to) {
		return new ArrayList<>( mapOfGate_passDTOTovalidityTo.getOrDefault(validity_to,new HashSet<>()));
	}
	
	
	public List<Gate_passDTO> getGate_passDTOByjob_cat(int job_cat) {
		return new ArrayList<>( mapOfGate_passDTOTojobCat.getOrDefault(job_cat,new HashSet<>()));
	}
	
	
	public List<Gate_passDTO> getGate_passDTOByinsertion_date(long insertion_date) {
		return new ArrayList<>( mapOfGate_passDTOToinsertionDate.getOrDefault(insertion_date,new HashSet<>()));
	}
	
	
	public List<Gate_passDTO> getGate_passDTOByinserted_by(String inserted_by) {
		return new ArrayList<>( mapOfGate_passDTOToinsertedBy.getOrDefault(inserted_by,new HashSet<>()));
	}
	
	
	public List<Gate_passDTO> getGate_passDTOBymodified_by(String modified_by) {
		return new ArrayList<>( mapOfGate_passDTOTomodifiedBy.getOrDefault(modified_by,new HashSet<>()));
	}
	
	
	public List<Gate_passDTO> getGate_passDTOBylastModificationTime(long lastModificationTime) {
		return new ArrayList<>( mapOfGate_passDTOTolastModificationTime.getOrDefault(lastModificationTime,new HashSet<>()));
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "gate_pass";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


