package gate_pass;

import com.google.gson.Gson;
import employee_assign.EmployeeAssignDAO;
import employee_assign.EmployeeAssignDTO;
import employee_records.Employee_recordsRepository;
import files.FilesDAO;
import gate_pass_affiliated_person.Gate_pass_affiliated_personDAO;
import gate_pass_affiliated_person.Gate_pass_affiliated_personDTO;
import gate_pass_item.Gate_pass_itemDAO;
import gate_pass_item.Gate_pass_itemDTO;
import gate_pass_visitor.Gate_pass_visitorDAO;
import gate_pass_visitor.Gate_pass_visitorDTO;
import geolocation.GeoLocationDAO2;
import nl.captcha.Captcha;
import office_unit_organograms.OfficeUnitOrganograms;
import office_unit_organograms.OfficeUnitOrganogramsRepository;
import office_units.Office_unitsDTO;
import office_units.Office_unitsRepository;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import pb_notifications.Pb_notificationsDAO;
import sessionmanager.SessionConstants;
import user.UserRepository;
import visitor_pass.Visitor_passDAO;
import visitor_pass.Visitor_passDTO;
import visitor_pass_affiliated_person.Visitor_pass_affiliated_personDAO;
import visitor_pass_affiliated_person.Visitor_pass_affiliated_personDTO;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;


@WebServlet("/Gate_passPublicServlet")
@MultipartConfig
public class Gate_passPublicServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Gate_passPublicServlet.class);
    private final FilesDAO filesDAO = new FilesDAO();

    private static final long AREA_PASS = 1;
    private static final long VISITOR_PASS = 10;
    private static final long GALLERY_PASS = 11;
    private static final long MEETING_PASS = 110;

    String tableName = "gate_pass";

    Gate_passDAO gate_passDAO;

    public Gate_passPublicServlet() {
        super();
        try {
            gate_passDAO = new Gate_passDAO(tableName);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }



    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String language = "Bangla";
        boolean isPermanentTable = true;
        if (request.getParameter("isPermanentTable") != null) {
            isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
        }

        try {
            String actionType = request.getParameter("actionType");
            if (actionType.equals("getAddPage")) {
                HttpSession session = request.getSession(false);
                session.setAttribute("galleryPassDates", gate_passDAO.buildParliamentSessionDatesForGalleryPass(language));

                RequestDispatcher requestDispatcher = request.getRequestDispatcher("gate_pass/gate_passPublicAddPage.jsp");
                requestDispatcher.forward(request, response);
            }
            else if(actionType.equals("getVisitorAddPage")) {
                RequestDispatcher requestDispatcher = request.getRequestDispatcher("gate_pass/gate_passPublicVisitorAddPage.jsp");
                requestDispatcher.forward(request, response);
            }
            else if (actionType.equals("getVisitorPass")) {
                RequestDispatcher requestDispatcher = request.getRequestDispatcher("visitor_pass/visitor_passPublicAddPage.jsp");
                requestDispatcher.forward(request, response);
            }
            else if(actionType.equals("getPaymentPage")) {
                RequestDispatcher requestDispatcher = request.getRequestDispatcher("visitor_pass/visitor_pass_PublicPaymentPage.jsp");
                requestDispatcher.forward(request, response);
            }
            else if(actionType.equals("makePayment")) {
                Visitor_passDAO visitor_passDAO = new Visitor_passDAO();
                HttpSession session = request.getSession(false);
                Visitor_passDTO dto = (Visitor_passDTO) session.getAttribute("visitor_passDTO");
                session.removeAttribute("visitor_passDTO");

                dto.paymentStatus=1;
                visitor_passDAO.add(dto);

                List<Visitor_pass_affiliated_personDTO> visitor_pass_affiliated_personDTOList = (List<Visitor_pass_affiliated_personDTO>) session.getAttribute("visitor_pass_affiliated_personDTOList");
                session.removeAttribute("visitor_pass_affiliated_personDTOList");

                Visitor_pass_affiliated_personDAO visitor_pass_affiliated_personDAO = new Visitor_pass_affiliated_personDAO();

                for(Visitor_pass_affiliated_personDTO afdto : visitor_pass_affiliated_personDTOList) {
                    afdto.visitorPassId = dto.iD;
                    visitor_pass_affiliated_personDAO.add(afdto);
                }

                session.setAttribute("fireSwal", "yes");
                response.sendRedirect("Gate_passPublicServlet?actionType=getVisitorPass");
            }
            else if(actionType.equals("checkCaptcha")) {
                String answerString = request.getParameter("answerString");
                boolean isAuthenticCaptcha;

                Captcha captcha = (Captcha) request.getSession().getAttribute(Captcha.NAME);
                String answer = answerString.trim();

                isAuthenticCaptcha = captcha.isCorrect(answer);

                PrintWriter out = response.getWriter();
                out.print(isAuthenticCaptcha);
                out.flush();
            }
            else if(actionType.equals("searchEmployeeOfficeInfo")) {
                String userName = request.getParameter("userName");
                String nameEn = request.getParameter("nameEn");
                String nameBn = request.getParameter("nameBn");
                String phoneNumber = request.getParameter("phoneNumber");
                logger.debug(userName);
                logger.debug(nameEn);
                logger.debug(nameBn);
                Long curTime = Calendar.getInstance().getTimeInMillis();
                Hashtable<String, String> searchTable = new Hashtable<>();
                if (userName != null && userName.length() > 0)
                    searchTable.put("employee_number", userName);
                if (nameEn != null && nameEn.length() > 0)
                    searchTable.put("name_eng", nameEn);
                if (nameBn != null && nameBn.length() > 0)
                    searchTable.put("name_bng", nameBn);
                if (phoneNumber != null && phoneNumber.length() > 0)
                    searchTable.put("personal_mobile", phoneNumber);
                String seqrchSql = getSearchQueryForEmployeeModal(searchTable, EmployeeAssignDAO.employeeModalSearchMap, String.valueOf(curTime));
                List<EmployeeAssignDTO> searchedList = new EmployeeAssignDAO().getEmployeeAssignDTOsBySearch(seqrchSql, curTime);
                logger.debug("search sql : "+seqrchSql);
                searchedList.forEach(item->{
                    logger.debug(item);
                });
                request.getSession().setAttribute("data", searchedList);
                RequestDispatcher rd = request.getRequestDispatcher("gate_pass/gatePassEmployeeSearchModalTable.jsp");
                rd.forward(request, response);

            }
            else if(actionType.equals("getAffiliatedPersonAddPage")) {
                RequestDispatcher requestDispatcher = request.getRequestDispatcher("gate_pass/gate_passPublicAffiliatedPersonEditBody.jsp");
                requestDispatcher.forward(request, response);
            }
            else if(actionType.equals("getStoredDataByMobileNumber")) {
                String mobileNumber = request.getParameter("mobileNumber");
                String credentialNo = request.getParameter("credentialNo");

                Gate_pass_visitorDAO gate_pass_visitorDAO = new Gate_pass_visitorDAO();
                Gate_pass_visitorDTO dto = gate_pass_visitorDAO.getDTOByMobileNumber(mobileNumber, credentialNo);

                if (dto == null) {
                    PrintWriter out = response.getWriter();
                    out.println("noString");
                    out.close();
                } else {
                    String jsonInString = new Gson().toJson(dto);
                    JSONObject mJSONObject = new JSONObject(jsonInString);

                    PrintWriter out = response.getWriter();
                    out.println(mJSONObject);
                    out.close();
                }
            }
            else if(actionType.equals("getStoredDataByMobileNumberForAffiliatedPerson")) {
                String mobileNumber = request.getParameter("mobileNumber");
                String credentialNo = request.getParameter("credentialNo");

                Gate_pass_affiliated_personDAO gate_pass_affiliated_personDAO = new Gate_pass_affiliated_personDAO();
                Gate_pass_affiliated_personDTO dto = gate_pass_affiliated_personDAO.getDTOByMobileNumberAndCredential(mobileNumber, credentialNo);

                if (dto == null) {
                    PrintWriter out = response.getWriter();
                    out.println("noString");
                    out.close();
                } else {
                    String jsonInString = new Gson().toJson(dto);
                    JSONObject mJSONObject = new JSONObject(jsonInString);

                    PrintWriter out = response.getWriter();
                    out.println(mJSONObject);
                    out.close();
                }
            }
            else if(actionType.equals("getItemAddPage")) {
                RequestDispatcher requestDispatcher = request.getRequestDispatcher("gate_pass/gate_passPublicItemEditBody.jsp");
                requestDispatcher.forward(request, response);
            }
            else if(actionType.equals("samePassCheck")) {
                String mobileNumber = request.getParameter("mobileNumber");
                String credentialNo = request.getParameter("credentialNo");
                Long visitingDate = Long.parseLong(request.getParameter("visitingDate"));
                Long gatePassType = Long.parseLong(request.getParameter("gatePassType"));
                Long gatePassSubType = Long.parseLong(request.getParameter("gatePassSubType"));

                Gate_pass_visitorDAO gate_pass_visitorDAO = new Gate_pass_visitorDAO();
                int count = gate_pass_visitorDAO.countSameDayGatePass(mobileNumber, credentialNo, visitingDate, gatePassType, gatePassSubType);

                PrintWriter out = response.getWriter();
                if(count == 0)
                    out.print("noSamePass");
                else
                    out.print("foundSamePass");
                out.close();
            }
            else if (actionType.equals("office")) {
                String parentIdStr = request.getParameter("parent_id");
                long parentId;
                if (parentIdStr == null) {
                    System.out.println("I don't know this logic yet! So, set default parentId!");
                    parentId = -1;
                } else if (!parentIdStr.equals("")) {
                    parentId = Long.parseLong(parentIdStr);
                } else {
                    parentId = -1;
                }


                String lang = request.getParameter("language");
                List<Office_unitsDTO> list = Office_unitsRepository.getInstance().getChildList(parentId);
                PrintWriter out = response.getWriter();
                out.println(Office_unitsRepository.getInstance().buildOptionsByParentId(lang, parentId, null));
                out.close();
            }
            else if (actionType.equals("getDisciplinaryEmployeeList")) {
                long officeId = Long.parseLong(request.getParameter("officeId"));
                long gatePassTypeId = Long.parseLong(request.getParameter("gatePassTypeId"));

                List<EmployeeAssignDTO> data, tempData;
                OfficeUnitOrganograms officeUnitOrganograms;
//                This action sends employee list in layerd option

                data = new ArrayList<>();
                tempData = Employee_recordsRepository.getInstance().getEmployeeAssignDTOByOfficeUnitId(officeId);
                if (gatePassTypeId==AREA_PASS) {
                    for (EmployeeAssignDTO dto: tempData) {
                        officeUnitOrganograms = OfficeUnitOrganogramsRepository.getInstance().getById(dto.organogram_id);
                        if (officeUnitOrganograms.job_grade_type_cat > 0 && officeUnitOrganograms.job_grade_type_cat < 10) {
                            data.add(dto);
                        }
                    }
                } else if(gatePassTypeId==VISITOR_PASS || gatePassTypeId==MEETING_PASS) {

                    for (EmployeeAssignDTO dto: tempData) {
                        officeUnitOrganograms = OfficeUnitOrganogramsRepository.getInstance().getById(dto.organogram_id);
                        if (Employee_recordsRepository.getInstance().getById(dto.employee_id).isMP==1 ||
                                officeUnitOrganograms.designation_eng.equalsIgnoreCase("PS") ||
                                officeUnitOrganograms.designation_eng.equalsIgnoreCase("APS") ||
                                officeUnitOrganograms.designation_eng.equalsIgnoreCase("DS") ||
                                officeUnitOrganograms.orgTree.endsWith("|")) {
                                    data.add(dto);
                        }
                    }
                } else if(gatePassTypeId==GALLERY_PASS) {

                    for (EmployeeAssignDTO dto: tempData) {
                        officeUnitOrganograms = OfficeUnitOrganogramsRepository.getInstance().getById(dto.organogram_id);
                        if (Employee_recordsRepository.getInstance().getById(dto.employee_id).isMP==1 ||
                                officeUnitOrganograms.designation_eng.equalsIgnoreCase("PS") ||
                                officeUnitOrganograms.designation_eng.equalsIgnoreCase("APS") ||
                                officeUnitOrganograms.designation_eng.equalsIgnoreCase("Private Secretary") ||
                                officeUnitOrganograms.designation_eng.contains("Joint Secretary") ||
                                officeUnitOrganograms.designation_eng.equalsIgnoreCase("Secretary")) {
                            data.add(dto);
                        }
                    }
                } else {
                    data = tempData;
                }


                request.getSession().setAttribute("data", data);
                RequestDispatcher rd;
                rd = request.getRequestDispatcher("gate_pass/gatePassEmployeeSearchModalTable.jsp");
                rd.forward(request, response);
            }
            else {
                request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.debug(ex);
        }
    }


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        boolean isPermanentTable = true;
        if(request.getParameter("isPermanentTable") != null)
        {
            isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
        }
        System.out.println("In servlet, isPermanentTable = " + isPermanentTable);

        try
        {
            String actionType = request.getParameter("actionType");
            System.out.println("actionType = " + actionType);
            if(actionType.equals("add"))
            {
                addGate_pass(request, response, true, true);
            }
            else if(actionType.equals("addVisitorPass"))
            {
                addVisitor_pass(request, response, true, true);
            }
            else if(actionType.equals("insertVisitor"))
            {
                addGate_pass_visitor(request, response, true, true);
            }
            else if(actionType.equals("getGeo"))
            {
                System.out.println("going to public geoloc");
                request.getRequestDispatcher("gate_pass/public_geoloc.jsp").forward(request, response);
            }
            else
            {
                System.out.println("Not going to  addGate_pass ");
                request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            }

        }
        catch(Exception ex)
        {
            ex.printStackTrace();
            logger.debug(ex);
        }
    }


    private void addGate_pass(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, boolean isPermanentTable) throws IOException
    {
        try {
            request.setAttribute("failureMessage", "");
            System.out.println("%%%% addGate_pass");
            String path = getServletContext().getRealPath("/img2/");
            Gate_passDTO gate_passDTO;
            SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");

//			if(addFlag == true)
//			{
//				gate_passDTO = new Gate_passDTO();
//			}
//			else
//			{
//				gate_passDTO = (Gate_passDTO)gate_passDAO.getDTOByID(Long.parseLong(request.getParameter("iD")));
//			}

            gate_passDTO = new Gate_passDTO();

            String Value = "";

            Value = request.getParameter("gatePassVisitorId");

            if(Value != null)
            {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("gatePassVisitorId = " + Value);
            if(Value != null && !Value.equalsIgnoreCase(""))
            {

                gate_passDTO.gatePassVisitorId = Long.parseLong(Value);
            }
            else
            {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("gatePassTypeId");

            if(Value != null)
            {
                Value = Jsoup.clean(Value,Whitelist.simpleText());
            }
            System.out.println("gatePassTypeId = " + Value);
            if(Value != null && !Value.equalsIgnoreCase(""))
            {

                gate_passDTO.gatePassTypeId = Long.parseLong(Value);
            }
            else
            {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("gatePassSubTypeId");

            if(Value != null)
            {
                Value = Jsoup.clean(Value,Whitelist.simpleText());
            }
            System.out.println("gatePassSubTypeId = " + Value);
            if(Value != null && !Value.equalsIgnoreCase(""))
            {

                gate_passDTO.gatePassSubTypeId = Long.parseLong(Value);
            }
            else
            {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("visitingDate");

            if(Value != null)
            {
                Value = Jsoup.clean(Value,Whitelist.simpleText());
            }
            System.out.println("visitingDate = " + Value);
            if(Value != null && !Value.equalsIgnoreCase(""))
            {

                if (gate_passDTO.gatePassTypeId==11) {
                    gate_passDTO.visitingDate = Long.parseLong(Value);
                } else {
                    try
                    {
                        Date d = f.parse(Value);
                        gate_passDTO.visitingDate = d.getTime();
                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                }
            }
            else
            {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("referrerId");

            if(Value != null)
            {
                Value = Jsoup.clean(Value,Whitelist.simpleText());
            }
            System.out.println("referrerId = " + Value);
            if(Value != null && !Value.equalsIgnoreCase(""))
            {

                gate_passDTO.referrerId = Long.parseLong(Value);
            }
            else
            {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("parliamentGateId");

            if(Value != null)
            {
                Value = Jsoup.clean(Value,Whitelist.simpleText());
            }
            System.out.println("parliamentGateId = " + Value);
            if(Value != null && !Value.equalsIgnoreCase(""))
            {

                gate_passDTO.parliamentGateId = Long.parseLong(Value);
            }
            else
            {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("validityFrom");

            if(Value != null)
            {
                Value = Jsoup.clean(Value,Whitelist.simpleText());
            }
            System.out.println("validityFrom = " + Value);
            if(Value != null && !Value.equalsIgnoreCase(""))
            {

                gate_passDTO.validityFrom = Long.parseLong(Value);
            }
            else
            {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("validityTo");

            if(Value != null)
            {
                Value = Jsoup.clean(Value,Whitelist.simpleText());
            }
            System.out.println("validityTo = " + Value);
            if(Value != null && !Value.equalsIgnoreCase(""))
            {

                gate_passDTO.validityTo = Long.parseLong(Value);
            }
            else
            {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("jobCat");

            if(Value != null)
            {
                Value = Jsoup.clean(Value,Whitelist.simpleText());
            }
            System.out.println("jobCat = " + Value);
            if(Value != null && !Value.equalsIgnoreCase(""))
            {

                gate_passDTO.jobCat = Integer.parseInt(Value);
            }
            else
            {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            if(addFlag)
            {
                Calendar c = Calendar.getInstance();
                c.set(Calendar.HOUR_OF_DAY, 0);
                c.set(Calendar.MINUTE, 0);
                c.set(Calendar.SECOND, 0);
                c.set(Calendar.MILLISECOND, 0);

                gate_passDTO.insertionDate = c.getTimeInMillis();
            }


            Value = request.getParameter("insertedBy");

            if(Value != null)
            {
                Value = Jsoup.clean(Value,Whitelist.simpleText());
            }
            System.out.println("insertedBy = " + Value);
            if(Value != null && !Value.equalsIgnoreCase(""))
            {

                gate_passDTO.insertedBy = (Value);
            }
            else
            {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("modifiedBy");

            if(Value != null)
            {
                Value = Jsoup.clean(Value,Whitelist.simpleText());
            }
            System.out.println("modifiedBy = " + Value);
            if(Value != null && !Value.equalsIgnoreCase(""))
            {

                gate_passDTO.modifiedBy = (Value);
            }
            else
            {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            System.out.println("Done adding  addGate_pass dto = " + gate_passDTO);

            // redirecting to Gate_pass_visitorServlet

            HttpSession session = request.getSession(false);
            session.setAttribute("gate_passDTO", gate_passDTO);

//            response.setStatus(HttpServletResponse.SC_MOVED_PERMANENTLY);
//            response.setHeader("Location", request.getContextPath() + "/Gate_passPublicServlet?actionType=getVisitorAddPage");
            response.sendRedirect(request.getContextPath() + "/Gate_passPublicServlet?actionType=getVisitorAddPage");
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private void addVisitor_pass(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, boolean isPermanentTable) throws IOException {
        // TODO Auto-generated method stub
        try {
            request.setAttribute("failureMessage", "");
            System.out.println("%%%% addVisitor_pass");
            String path = getServletContext().getRealPath("/img2/");
            Visitor_passDTO visitor_passDTO;
            SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
            Visitor_passDAO visitor_passDAO = new Visitor_passDAO();

            if (addFlag == true) {
                visitor_passDTO = new Visitor_passDTO();
            } else {
                visitor_passDTO = visitor_passDAO.getDTOByID(Long.parseLong(request.getParameter("iD")));
            }
            String FileNamePrefix;
            if (addFlag == true) {
                FileNamePrefix = UUID.randomUUID().toString().substring(0, 10);
            } else {
                FileNamePrefix = request.getParameter("iD");
            }

            String Value = "";

            Value = request.getParameter("visitingDate");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("visitingDate = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                try {
                    Date d = f.parse(Value);
                    visitor_passDTO.visitingDate = d.getTime();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("visitingTime");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("visitingTime = " + Value);
            if (Value != null) {
                visitor_passDTO.visitingTime = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("name");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("name = " + Value);
            if (Value != null) {
                visitor_passDTO.name = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("address");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("address = " + Value);
            if (Value != null) {
                visitor_passDTO.address = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("mobileNumber");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("mobileNumber = " + Value);
            if (Value != null) {
                visitor_passDTO.mobileNumber = "88"+Value;
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("email");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("email = " + Value);
            if (Value != null) {
                visitor_passDTO.email = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("enjoyAssembly");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("enjoyAssembly = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                visitor_passDTO.enjoyAssembly = Integer.parseInt(Value);
            }

            Value = request.getParameter("localOrForeign");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("typeOfVisitor = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                visitor_passDTO.typeOfVisitor = Integer.parseInt(Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("filesDropzone");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }

            logger.debug("filesDropzone = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                logger.debug("filesDropzone = " + Value);
                if (!Value.equalsIgnoreCase("")) {
                    visitor_passDTO.fileID = Long.parseLong(Value);
                } else {
                    logger.debug("FieldName has a null Value, not updating" + " = " + Value);
                }

                if (!addFlag) {
                    String filesDropzoneFilesToDelete = request.getParameter("filesDropzoneFilesToDelete");
                    String[] deleteArray = filesDropzoneFilesToDelete.split(",");
                    for (int i = 0; i < deleteArray.length; i++) {
                        if (i > 0) {
                            filesDAO.delete(Long.parseLong(deleteArray[i]));
                        }
                    }
                }
            } else {
                logger.debug("FieldName has a null Value, not updating" + " = " + Value);
            }

//			Part filePart_institutionalLetter =  request.getPart("institutionalLetter");
//			Value = commonRequestHandler.getFileName(filePart_institutionalLetter);
//
//			if(Value != null && !Value.equalsIgnoreCase(""))
//			{
//				Value = Jsoup.clean(Value,Whitelist.simpleText());
//			}
//			System.out.println("institutionalLetter = " + Value);
//			if(Value != null && !Value.equalsIgnoreCase(""))
//			{
////                String FileName = FileNamePrefix + "_" + "institutionalLetter" + "_" + Value;
////                visitor_passDTO.institutionalLetter = (FileName);
////                Utils.uploadFile(filePart_institutionalLetter, FileName, path);
//			}
//			else
//			{
//				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//			}

            Value = request.getParameter("nationalityType");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("nationalityType = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                visitor_passDTO.nationalityType = Integer.parseInt(Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("professionType");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("professionType = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                visitor_passDTO.professionType = Integer.parseInt(Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("passportNo");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("passportNo = " + Value);
            if (Value != null) {
                visitor_passDTO.passportNo = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("visaExpiry");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("visaExpiry = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                try {
                    Date d = f.parse(Value);
                    visitor_passDTO.visitingDate = d.getTime();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("paymentStatus");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("paymentStatus = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                visitor_passDTO.paymentStatus = Integer.parseInt(Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("firstLayerApprovalStatus");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("firstLayerApprovalStatus = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                visitor_passDTO.firstLayerApprovalStatus = Integer.parseInt(Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("firstLayerApprovedBy");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("firstLayerApprovedBy = " + Value);
            if (Value != null) {
                visitor_passDTO.firstLayerApprovedBy = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("firstLayerApprovalTime");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("firstLayerApprovalTime = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                visitor_passDTO.firstLayerApprovalTime = Long.parseLong(Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("secondLayerApprovalStatus");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("secondLayerApprovalStatus = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                visitor_passDTO.secondLayerApprovalStatus = Integer.parseInt(Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("secondLayerApprovedBy");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("secondLayerApprovedBy = " + Value);
            if (Value != null) {
                visitor_passDTO.secondLayerApprovedBy = (Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("secondLayerApprovalTime");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("secondLayerApprovalTime = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                visitor_passDTO.secondLayerApprovalTime = Long.parseLong(Value);
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }


            if (addFlag) {
                Calendar c = Calendar.getInstance();
                c.set(Calendar.HOUR_OF_DAY, 0);
                c.set(Calendar.MINUTE, 0);
                c.set(Calendar.SECOND, 0);
                c.set(Calendar.MILLISECOND, 0);

                visitor_passDTO.insertionDate = c.getTimeInMillis();
            }


            System.out.println("Done adding  addVisitor_pass dto = " + visitor_passDTO);

            HttpSession session = request.getSession(false);

            int personCount = Integer.parseInt(request.getParameter("additionalPersonCount"));

            List<Visitor_pass_affiliated_personDTO> visitor_pass_affiliated_personDTOList = new ArrayList<>();
            for(int i = 0; i < personCount; i++){
                if(request.getParameter("name_"+i).equals(""))
                    continue;

                Visitor_pass_affiliated_personDTO dto = new Visitor_pass_affiliated_personDTO();
                dto.visitorPassId = visitor_passDTO.iD;

                if(request.getParameter("credentialType_"+i) != null)
                    dto.credentialCat = Integer.parseInt(request.getParameter("credentialType_"+i));

                dto.credentialNo = request.getParameter("credentialNo_"+i);

                if(request.getParameter("mobileNumber_"+i) != null)
                    dto.mobileNumber = request.getParameter("mobileNumber_"+i);

                if(request.getParameter("nationality_"+i) != null)
                    dto.nationalityType = Integer.parseInt(request.getParameter("nationality_"+i));

                dto.name = request.getParameter("name_"+i);

                System.out.println(dto);
                visitor_pass_affiliated_personDTOList.add(dto);
                //visitor_pass_affiliated_personDAO.add(dto);
            }

            if (visitor_passDTO.typeOfVisitor== Visitor_passDAO.VISITOR_TYPE_LOCAL) {
                long visitorPassId = visitor_passDAO.add(visitor_passDTO);

                Visitor_pass_affiliated_personDAO visitor_pass_affiliated_personDAO = new Visitor_pass_affiliated_personDAO();

                for(Visitor_pass_affiliated_personDTO dto : visitor_pass_affiliated_personDTOList) {
                    dto.visitorPassId = visitorPassId;
                    visitor_pass_affiliated_personDAO.add(dto);
                }

                session.setAttribute("fireSwal", "yes");
                response.sendRedirect("Gate_passPublicServlet?actionType=getVisitorPass");
            } else {
                session.setAttribute("visitor_passDTO", visitor_passDTO);
                session.setAttribute("visitor_pass_affiliated_personDTOList", visitor_pass_affiliated_personDTOList);
                response.sendRedirect("Gate_passPublicServlet?actionType=getPaymentPage");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void addGate_pass_visitor(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, boolean isPermanentTable) throws IOException
    {
        // TODO Auto-generated method stub
        try
        {
            request.setAttribute("failureMessage", "");
            System.out.println("%%%% addGate_pass_visitor");
            String path = getServletContext().getRealPath("/img2/");
            Gate_pass_visitorDTO gate_pass_visitorDTO;
            SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
            Gate_pass_visitorDAO gate_pass_visitorDAO = new Gate_pass_visitorDAO();
            Pb_notificationsDAO pb_notificationsDAO = Pb_notificationsDAO.getInstance();

            if(addFlag == true)
            {
                gate_pass_visitorDTO = new Gate_pass_visitorDTO();
            }
            else
            {
                gate_pass_visitorDTO = gate_pass_visitorDAO.getDTOByID(Long.parseLong(request.getParameter("iD")));
            }

            String Value = "";

            Value = request.getParameter("name");

            if(Value != null)
            {
                Value = Jsoup.clean(Value,Whitelist.simpleText());
            }
            System.out.println("name = " + Value);
            if(Value != null && !Value.equalsIgnoreCase(""))
            {

                gate_pass_visitorDTO.name = (Value);
            }
            else
            {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("fatherName");

            if(Value != null)
            {
                Value = Jsoup.clean(Value,Whitelist.simpleText());
            }
            System.out.println("fatherName = " + Value);
            if(Value != null && !Value.equalsIgnoreCase(""))
            {

                gate_pass_visitorDTO.fatherName = (Value);
            }
            else
            {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("motherName");

            if(Value != null)
            {
                Value = Jsoup.clean(Value,Whitelist.simpleText());
            }
            System.out.println("motherName = " + Value);
            if(Value != null && !Value.equalsIgnoreCase(""))
            {

                gate_pass_visitorDTO.motherName = (Value);
            }
            else
            {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("permanentAddress");

            if(Value != null)
            {
                Value = Jsoup.clean(Value,Whitelist.simpleText());
            }
            System.out.println("permanentAddress = " + Value);
            if(Value != null && !Value.equalsIgnoreCase(""))
            {


                {

                    String[] addresses = Value.split("\\$", 2);
                    String addressDetails = "", address_id = "";

                    try {
                        address_id = addresses[0];
                        addressDetails = addresses[1];
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }




//					StringTokenizer tok3=new StringTokenizer(Value, "$");
//					int i = 0;
//
//					while(tok3.hasMoreElements())
//					{
//						if(i == 0)
//						{
//							address_id = tok3.nextElement() + "";
//						}
//						else if(i == 1)
//						{
//							addressDetails = tok3.nextElement() + "";
//						}
//						else {
//							break;
//						}
//						i ++;
//					}
                    try
                    {
                        if(address_id.matches("-?\\d+"))
                        {
                            gate_pass_visitorDTO.permanentAddress
                                    = address_id + ":"
                                    + GeoLocationDAO2.getLocationText(Integer.parseInt(address_id), "English") + ":"
                                    + addressDetails + ":"
                                    + GeoLocationDAO2.getLocationText(Integer.parseInt(address_id), "Bangla");
                        }
                    }
                    catch(Exception ex)
                    {
                        ex.printStackTrace();
                    }
                }

            }
            else
            {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("currentAddress");

            if(Value != null)
            {
                Value = Jsoup.clean(Value,Whitelist.simpleText());
            }
            System.out.println("currentAddress = " + Value);
            if(Value != null && !Value.equalsIgnoreCase(""))
            {


                {
                    String[] addresses = Value.split("\\$", 2);
                    String addressDetails = "", address_id = "";

                    try {
                        address_id = addresses[0];
                        addressDetails = addresses[1];
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
//					StringTokenizer tok3=new StringTokenizer(Value, "$");
//					int i = 0;
//					String addressDetails = "", address_id = "";
//					while(tok3.hasMoreElements())
//					{
//						if(i == 0)
//						{
//							address_id = tok3.nextElement() + "";
//						}
//						else if(i == 1)
//						{
//							addressDetails = tok3.nextElement() + "";
//						}
//						i ++;
//					}
                    try
                    {
                        if(address_id.matches("-?\\d+"))
                        {
                            gate_pass_visitorDTO.currentAddress
                                    = address_id + ":"
                                    + GeoLocationDAO2.getLocationText(Integer.parseInt(address_id), "English") + ":"
                                    + addressDetails + ":"
                                    + GeoLocationDAO2.getLocationText(Integer.parseInt(address_id), "Bangla");
                        }
                    }
                    catch(Exception ex)
                    {
                        ex.printStackTrace();
                    }
                }

            }
            else
            {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("credentialType");

            if(Value != null)
            {
                Value = Jsoup.clean(Value,Whitelist.simpleText());
            }
            System.out.println("credentialType = " + Value);
            if(Value != null && !Value.equalsIgnoreCase(""))
            {

                gate_pass_visitorDTO.credentialType = Integer.parseInt(Value);
            }
            else
            {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("credentialNo");

            if(Value != null)
            {
                Value = Jsoup.clean(Value,Whitelist.simpleText());
            }
            System.out.println("credentialNo = " + Value);
            if(Value != null && !Value.equalsIgnoreCase(""))
            {

                gate_pass_visitorDTO.credentialNo = (Value);
            }
            else
            {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("mobileNumber");

            if(Value != null)
            {
                Value = Jsoup.clean(Value,Whitelist.simpleText());
            }
            System.out.println("mobileNumber = " + Value);
            if(Value != null && !Value.equalsIgnoreCase(""))
            {

                gate_pass_visitorDTO.mobileNumber = "88" + Value;
            }
            else
            {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("email");

            if(Value != null)
            {
                Value = Jsoup.clean(Value,Whitelist.simpleText());
            }
            System.out.println("email = " + Value);
            if(Value != null && !Value.equalsIgnoreCase(""))
            {

                gate_pass_visitorDTO.email = (Value);
            }
            else
            {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("genderType");

            if(Value != null)
            {
                Value = Jsoup.clean(Value,Whitelist.simpleText());
            }
            System.out.println("genderType = " + Value);
            if(Value != null && !Value.equalsIgnoreCase(""))
            {

                gate_pass_visitorDTO.genderType = Integer.parseInt(Value);
            }
            else
            {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("officerType");

            if(Value != null)
            {
                Value = Jsoup.clean(Value,Whitelist.simpleText());
            }
            System.out.println("officerType = " + Value);
            if(Value != null && !Value.equalsIgnoreCase(""))
            {

                gate_pass_visitorDTO.officerType = Integer.parseInt(Value);
            }
            else
            {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            if(addFlag)
            {
                Calendar c = Calendar.getInstance();
                c.set(Calendar.HOUR_OF_DAY, 0);
                c.set(Calendar.MINUTE, 0);
                c.set(Calendar.SECOND, 0);
                c.set(Calendar.MILLISECOND, 0);

                gate_pass_visitorDTO.insertionDate = c.getTimeInMillis();
            }


            Value = request.getParameter("insertedBy");

            if(Value != null)
            {
                Value = Jsoup.clean(Value,Whitelist.simpleText());
            }
            System.out.println("insertedBy = " + Value);
            if(Value != null && !Value.equalsIgnoreCase(""))
            {

                gate_pass_visitorDTO.insertedBy = (Value);
            }
            else
            {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            Value = request.getParameter("modifiedBy");

            if(Value != null)
            {
                Value = Jsoup.clean(Value,Whitelist.simpleText());
            }
            System.out.println("modifiedBy = " + Value);
            if(Value != null && !Value.equalsIgnoreCase(""))
            {

                gate_pass_visitorDTO.modifiedBy = (Value);
            }
            else
            {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            System.out.println("Done adding  addGate_pass_visitor dto = " + gate_pass_visitorDTO);


            HttpSession session = request.getSession(false);
            Gate_passDTO gate_passDTO = (Gate_passDTO) session.getAttribute("gate_passDTO");


            // Now it's time to remove attribute
            session.removeAttribute("gate_passDTO");
            session.setAttribute("isToast", "yes");

            // Insert Gate_passDAO, Gate_pass_visitorDAO, affiliatedPersonDTOList into database
            // Insert Gate_pass_visitorDAO
            Gate_pass_visitorDTO storedDTO = gate_pass_visitorDAO.getDTOByMobileNumber(gate_pass_visitorDTO.mobileNumber, gate_pass_visitorDTO.credentialNo);
            if (storedDTO == null) {
                gate_passDTO.gatePassVisitorId = gate_pass_visitorDAO.add(gate_pass_visitorDTO);
            }
            else {
//				gate_pass_visitorDAO.deleteDTOByMobileNumber(gate_pass_visitorDTO.mobileNumber);
                gate_pass_visitorDAO.updateDTOById(storedDTO.id, gate_pass_visitorDTO);
//				gate_pass_visitorDAO.add(gate_pass_visitorDTO);
                gate_passDTO.gatePassVisitorId = storedDTO.id;
            }


            // Insert Gate_passDAO

            Gate_passDAO gate_passDAO = new Gate_passDAO();
            long gatePassId = gate_passDAO.add(gate_passDTO);


            if (gate_passDTO.gatePassTypeId== 1 || gate_passDTO.gatePassTypeId==110) {
                // Insert List of Affiliated persons
                int personCount = Integer.parseInt(request.getParameter("additionalPersonCount"));
                Gate_pass_affiliated_personDAO gate_pass_affiliated_personDAO = new Gate_pass_affiliated_personDAO();
                for(int i = 0; i < personCount; i++){
                    Gate_pass_affiliated_personDTO dto = new Gate_pass_affiliated_personDTO();
                    dto.gatePassId = gatePassId;
                    dto.credentialType = Integer.parseInt(request.getParameter("credentialType_"+i));
                    dto.credentialNo = request.getParameter("credentialNo_"+i);
                    dto.mobileNumber = request.getParameter("mobileNumber_"+i);
                    dto.name = request.getParameter("name_"+i);
                    System.out.println(dto);
                    gate_pass_affiliated_personDAO.add(dto);
                }
            }


            if (gate_passDTO.gatePassTypeId== 10 || gate_passDTO.gatePassTypeId==110) {
                // Insert List of Gate Pass Items
                int itemCount = Integer.parseInt(request.getParameter("totalItemCount"));
                Gate_pass_itemDAO gate_pass_itemDAO = new Gate_pass_itemDAO();
                for(int i = 0; i < itemCount; i++){
                    //System.out.println("*****"+request.getParameter("parliamentItemId_" + (i + 1)));
                    Gate_pass_itemDTO dto = new Gate_pass_itemDTO();
                    try {
                        dto.parliamentItemId = Long.parseLong(request.getParameter("parliamentItemId_" + (i + 1)));
                    } catch (Exception e) {
                        continue;
                    }
                    dto.gatePassId = gatePassId;
                    try {
                        dto.amount = Integer.parseInt(request.getParameter("amount_" + (i + 1)));
                        dto.approvedAmount = dto.amount;
                    } catch (Exception e) {
                        continue;
                    }
                    dto.description = request.getParameter("description_" + (i + 1));
                    System.out.println(dto);
                    if(dto.amount > 0) gate_pass_itemDAO.add(dto);
                }
            }



            // generate notification and sent it to referrer
            String URL = "Gate_passServlet?notification=true&actionType=view&ID=" + gatePassId;
            String notificationMessage = gate_pass_visitorDTO.name+" has submitted a Gate Pass waiting for your approval.$"+gate_pass_visitorDTO.name+" প্রবেশ পাসের জন্য আবেদন করেছেন, যা আপনার সুপারিশের অপেক্ষায় আছে।";
            long organogramId = UserRepository.getInstance().getUserDtoByEmployeeRecordId(gate_passDTO.referrerId).organogramID;
            pb_notificationsDAO.addPb_notifications(organogramId, System.currentTimeMillis(), URL, notificationMessage);


            //  then Redirect to the gate pass page
            response.sendRedirect("Gate_passPublicServlet?actionType=getAddPage");

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private String getSearchQueryForEmployeeModal(Hashtable p_searchCriteria, Map<String, String> searchMap, String curTime) {
        StringBuilder sqlBuilder = new StringBuilder();
        sqlBuilder.append("SELECT employee_record_id,office_unit_id,office_unit_organogram_id\n" +
                "from employee_offices\n" +
                "where\n" +
                "employee_record_id IN(SELECT employee_records.id from employee_records\n" +
                "    where employee_records.isDeleted=0 ");
        if (p_searchCriteria != null) {
            Map<String, String> map = new HashMap<>(p_searchCriteria);
            String searchString = map.entrySet()
                    .stream()
                    .filter(entry -> entry.getValue() != null && entry.getValue().trim().length() > 0 && searchMap.get(entry.getKey()) != null)
                    .map(entry -> searchMap.get(entry.getKey()).replace("{}", entry.getValue()))
                    .collect(Collectors.joining(" "));
            if (searchString.trim().length() > 0) {
                sqlBuilder.append(searchString);
            }
        }
        sqlBuilder.append(")\n" +
                "AND\n" +
                "isDeleted=0 AND " +
                "(last_office_date=" + SessionConstants.MIN_DATE + " OR " +
                "last_office_date>=" + curTime + ")");
        logger.debug(sqlBuilder.toString());
        return sqlBuilder.toString();
    }

}
