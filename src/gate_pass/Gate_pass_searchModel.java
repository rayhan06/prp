package gate_pass;

import sessionmanager.SessionConstants;
import util.CommonDTO;

import java.util.ArrayList;
import java.util.List;

public class Gate_pass_searchModel extends CommonDTO {

    public long gate_pass_id = 0;
    public long gatePassVisitorId = 0;
    public long visitingDate = 0;
    public String gatePassTypeEng = "";
    public String gatePassTypeBng = "";
    public String gatePassSubTypeEng = "";
    public String gatePassSubTypeBng = "";
    public long referrerId = 0;
    public String referrerNameEng = "";
    public String referrerNameBng = "";
    public long parliamentGateId = 0;
    public long validityFrom = 0;
    public long validityTo = 0;
    public long insertionDate = 0;
    public String name = "";
    public String fatherName = "";
    public String motherName = "";
    public GatePassVisitorAddress permanentAddress;
    public GatePassVisitorAddress currentAddress;
    public String credentialTypeEng = "";
    public String credentialTypeBng = "";
    public String credentialNo = "";
    public String mobileNumber = "";
    public String email = "";
    public String genderTypeEng = "";
    public String genderTypeBng = "";
    public String officerTypeEng = "";
    public String officerTypeBng = "";
    public int firstLayerApprovalStatus = Gate_passDAO.approvalStatusPending;
    public String firstLayerApprovedBy = "";
    public long firstLayerApprovalTime = SessionConstants.MIN_DATE;
    public int secondLayerApprovalStatus = Gate_passDAO.approvalStatusPending;
    public String secondLayerApprovedBy = "";
    public long secondLayerApprovalTime = SessionConstants.MIN_DATE;
    public String referrerOfficeUnitEng = "";
    public String referrerOfficeUnitBng = "";
    public long creatorId = 0;
    public int isIssued = 0;
    public boolean partiallyIssued = false;
    public long gatePassTypeId = -1;
}
