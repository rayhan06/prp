package recruitment_dashboard;

import util.CommonDTO;

import java.util.ArrayList;
import java.util.List;


public class PositionWiseStatusDTO
{
	
	public List<PositionWiseItemDTO> data = new ArrayList<>();

    @Override
	public String toString() {
            return "PositionWiseStatusDTO[" +
            " data = " + data +
            "]";
    }

}