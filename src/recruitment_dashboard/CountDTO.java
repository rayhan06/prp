package recruitment_dashboard;

public class CountDTO {
    public String jobCount = "";
    public String applicantCount = "";

    @Override
    public String toString() {
        return "CountDTO[" +
                " jobCount = " + jobCount +
                " applicantCount = " + applicantCount +
                "]";
    }
}
