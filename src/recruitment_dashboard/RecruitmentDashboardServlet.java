package recruitment_dashboard;

import com.google.gson.Gson;
import job_applicant_application.Job_applicant_applicationDAO;
import login.LoginDTO;
import org.apache.log4j.Logger;
import parliament_job_applicant.Parliament_job_applicantDAO;
import recruitment_job_description.Recruitment_job_descriptionDAO;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.UtilCharacter;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;
import language.LC;
import language.LM;


/**
 * Servlet implementation class Recruitment_job_descriptionServlet
 */
@WebServlet("/RecruitmentDashboardServlet")
@MultipartConfig
public class RecruitmentDashboardServlet extends HttpServlet
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(RecruitmentDashboardServlet.class);
	private final Gson gson = new Gson();

    /**
     * @see HttpServlet#HttpServlet()
     */
    public RecruitmentDashboardServlet()
	{
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		System.out.println("In doget request = " + request);
		try
		{

			String actionType = request.getParameter("actionType");
			if(actionType.equals("main"))
			{

				request.getRequestDispatcher("recruitment_dashboard/recruitment_dashboard.jsp").forward(request, response);

			} else if(actionType.equals("gender_count")){

				String data = "";
				long jobId = Long.parseLong(request.getParameter("jobId"));
				String language = request.getParameter("language");
				Map<String, Integer> result = new Parliament_job_applicantDAO().getGenderList(jobId, language);
				data = new Gson().toJson(result);
				response.setContentType("application/json");
				PrintWriter out = response.getWriter();
				out.print(data);
				out.flush();
				out.close();

			}

			else if(actionType.equals("job_count")){

				LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
				String language = LM.getText(LC.TRAINING_CALENDER_EDIT_LANGUAGE, loginDTO);

				CountDTO countDTO = new CountDTO();
				countDTO.jobCount = UtilCharacter.convertDataByLanguage(language,
						new Recruitment_job_descriptionDAO().getRunningJobCount() + "");
				countDTO.applicantCount = UtilCharacter.convertDataByLanguage(language,
						new Job_applicant_applicationDAO().getTotalApplicationCount() + "");

				String data = "";
				data = new Gson().toJson(countDTO);
				response.setContentType("application/json");
				PrintWriter out = response.getWriter();
				out.print(data);
				out.flush();
				out.close();

			}

			else if(actionType.equals("highest_edu_level_count")){

				String data = "";
				long jobId = Long.parseLong(request.getParameter("jobId"));
				String language = request.getParameter("language");
				Map<String, Integer> result = new Job_applicant_applicationDAO().getHighestEduLevelList(jobId, language);
				data = new Gson().toJson(result);
				response.setContentType("application/json");
				PrintWriter out = response.getWriter();
				out.print(data);
				out.flush();
				out.close();

			}

			else if(actionType.equals("division_count")){

				String data = "";
				long jobId = Long.parseLong(request.getParameter("jobId"));
				String language = request.getParameter("language");
				Map<String, Integer> result = new Job_applicant_applicationDAO().getDivisionCountList(jobId, language);
				data = new Gson().toJson(result);
				response.setContentType("application/json");
				PrintWriter out = response.getWriter();
				out.print(data);
				out.flush();
				out.close();

			}

			else if(actionType.equals("district_count")){

				String data = "";
				long jobId = Long.parseLong(request.getParameter("jobId"));
				String language = request.getParameter("language");
				Map<String, Integer> result = new Job_applicant_applicationDAO().getDistrictCountList(jobId, language);
				data = new Gson().toJson(result);
				response.setContentType("application/json");
				PrintWriter out = response.getWriter();
				out.print(data);
				out.flush();
				out.close();

			}

			else if(actionType.equals("all_status_count")){

				String data = "";
				long jobId = Long.parseLong(request.getParameter("jobId"));
				LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
				Map<String, Integer> result = new Job_applicant_applicationDAO().getAllStatusCountByJobId(jobId, loginDTO);
				data = new Gson().toJson(result);
				response.setContentType("application/json");
				PrintWriter out = response.getWriter();
				out.print(data);
				out.flush();
				out.close();

			}

			else if(actionType.equals("status_count")){
				String data = "";
				long jobId = Long.parseLong(request.getParameter("jobId"));
				String language = request.getParameter("language");
				PositionWiseStatusDTO dto = new Job_applicant_applicationDAO().getPositionWiseStatus(jobId, language);
				response.setContentType("application/json");
				response.setCharacterEncoding("UTF-8");
				String encoded = this.gson.toJson(dto);
				PrintWriter out = response.getWriter();
				out.print(encoded);
				out.flush();
				out.close();
			}
			else
			{
				request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}





	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{

	}


}

