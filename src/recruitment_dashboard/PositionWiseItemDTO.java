package recruitment_dashboard;

public class PositionWiseItemDTO
{

    public String level = "";
    public int total = 0;
    public int shortListed = 0;
    public int rejected = 0;
	
    @Override
	public String toString() {
            return "PositionWiseItemDTO[" +
            " level = " + level +
            " total = " + total +
            " shortListed = " + shortListed +
            " rejected = " + rejected +
            "]";
    }

}