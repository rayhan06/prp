CREATE TABLE budget_office
(
    ID                   BIGINT(20) UNSIGNED PRIMARY KEY,
    code                 VARCHAR(16)  DEFAULT NULL,
    name_eng             VARCHAR(255) DEFAULT NULL,
    name_bng             VARCHAR(255) DEFAULT NULL,
    isDeleted            INT(11)      DEFAULT 0,
    inserted_by          BIGINT(20)   DEFAULT 0,
    insertion_time       BIGINT(20)   DEFAULT -62135791200000,
    modified_by          BIGINT(20)   DEFAULT 0,
    lastModificationTime BIGINT(20)   DEFAULT -62135791200000
) ENGINE = MyISAM
  DEFAULT CHARSET = utf8
  COLLATE = utf8_unicode_ci;