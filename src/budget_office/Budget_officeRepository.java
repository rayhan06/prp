package budget_office;

import office_units.Office_unitsDTO;
import office_units.Office_unitsRepository;
import org.apache.log4j.Logger;
import pb.OptionDTO;
import pb.Utils;
import util.HttpRequestUtils;
import util.LockManager;
import util.StringUtils;

import java.util.*;
import java.util.stream.Collectors;

import static util.StringUtils.convertBanglaIfLanguageIsBangla;
import static util.UtilCharacter.getDataByLanguage;

@SuppressWarnings({"Duplicates"})
public class Budget_officeRepository {
    private final Logger logger = Logger.getLogger(Budget_officeRepository.class);
    private final Budget_officeDAO budget_officeDAO = Budget_officeDAO.getInstance();
    private Map<Long, Budget_officeDTO> mapById;
    private List<Budget_officeDTO> allBudgetOfficeDTOList;

    private Budget_officeRepository() {
        reload();
    }

    private static class Budget_institutional_groupRepositoryLazyLoader {
        static final Budget_officeRepository INSTANCE = new Budget_officeRepository();
    }

    public static Budget_officeRepository getInstance() {
        return Budget_officeRepository.Budget_institutional_groupRepositoryLazyLoader.INSTANCE;
    }

    private void reload() {
        logger.debug("Budget_officeRepository reload start");
        List<Budget_officeDTO> list = budget_officeDAO.getAllDTOs(true);
        if (list != null && list.size() > 0) {
            allBudgetOfficeDTOList = list.stream()
                                         .filter(e -> e.isDeleted == 0)
                                         .collect(Collectors.toList());
            mapById = allBudgetOfficeDTOList.stream()
                                            .collect(Collectors.toMap(e -> e.iD, e -> e, (e1, e2) -> e1));
        } else {
            allBudgetOfficeDTOList = new ArrayList<>();
            mapById = new HashMap<>();
        }
        logger.debug("Budget_officeRepository reload end");
    }

    public Budget_officeDTO getById(long id) {
        if (mapById.get(id) == null) {
            synchronized (LockManager.getLock(id + "BOR")) {
                if (mapById.get(id) == null) {
                    Budget_officeDTO dto = budget_officeDAO.getDTOFromIdDeletedOrNot(id);
                    if (dto != null) {
                        mapById.put(dto.iD, dto);
                        if (dto.isDeleted == 0) {
                            allBudgetOfficeDTOList.add(dto);
                            allBudgetOfficeDTOList.sort(Comparator.comparingLong(o -> o.iD));
                        }
                    }
                }
            }
        }
        return mapById.get(id);
    }

    public String getText(long id, boolean withCode, String language) {
        Budget_officeDTO dto = getById(id);
        if (dto == null) return "";
        String text = withCode ? convertBanglaIfLanguageIsBangla(language, dto.code) + " - " : "";
        return text.concat(getDataByLanguage(language, dto.nameBn, dto.nameEn));
    }

    public String getText(long id, String language) {
        return getText(id, false, language);
    }

    public String getCode(long id, String language) {
        Budget_officeDTO dto = getById(id);
        if (dto == null) return "";
        return StringUtils.convertBanglaIfLanguageIsBangla(language, dto.code);
    }

    public List<Budget_officeDTO> getAllOfficeList() {
        return allBudgetOfficeDTOList;
    }

    public String buildOptions(String language, Long selectedId) {
        return buildOptions(language, selectedId, false);
    }

    public String buildOptions(String language, Long selectedId, boolean withCode) {
        List<OptionDTO> optionDTOList = null;
        if (allBudgetOfficeDTOList != null && allBudgetOfficeDTOList.size() > 0) {
            optionDTOList = allBudgetOfficeDTOList.stream()
                                                  .map(dto -> dto.getOptionDTO(withCode))
                                                  .collect(Collectors.toList());
        }
        return Utils.buildOptions(optionDTOList, language, selectedId == null ? null : String.valueOf(selectedId));
    }

    public Set<Long> getOfficeUnitIdSet(long budgetOfficeId) {
        return Office_unitsRepository
                .getInstance()
                .getOffice_unitsList()
                .stream()
                .filter(officeUnitsDTO -> officeUnitsDTO.budgetOfficeId > 0)
                .filter(officeUnitsDTO -> officeUnitsDTO.budgetOfficeId == budgetOfficeId)
                .map(officeUnitsDTO -> officeUnitsDTO.iD)
                .collect(Collectors.toSet());
    }

    public long getBudgetOfficeIdByOfficeUnitId(long officeUnitId) {
        Office_unitsDTO officeUnitsDTO = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(officeUnitId);
        if (officeUnitsDTO == null || officeUnitsDTO.budgetOfficeId < 0) {
            boolean isLangEng = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
            throw new IllegalArgumentException(
                    isLangEng ? "Budget Office Not found for this office"
                              : "এই দপ্তরের জন্য কোন বাজেট অফিস পাওয়া যায়নি"
            );
        }
        return officeUnitsDTO.budgetOfficeId;
    }
}