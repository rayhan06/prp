package daily_basis_messenger_bill;

import daily_basis_messenger.Daily_basis_messengerDAO;
import daily_basis_messenger.Daily_basis_messengerDTO;
import pb.Utils;
import util.UtilCharacter;

public class Daily_basis_messenger_billModel {
    public String dailyBasisMessengerId = "-1";
    public String dailyBasisMessengerBillId = "-1";
    public String name = "";
    public String designation = "";
    public String fatherName = "";
    public String mobileNumber = "";
    public String savingAccountNumber = "";
    public String dailyRate;
    public String day = "0";
    public String totalAmount = "0";

    public Daily_basis_messenger_billModel(Daily_basis_messengerDTO messengerDTO, String language) {
        setEmployeeData(messengerDTO, language);
        this.dailyRate = String.valueOf(messengerDTO.dailyRate);
    }

    public Daily_basis_messenger_billModel(Daily_basis_messenger_billDTO billDTO, String language) {
        Daily_basis_messengerDTO messengerDTO = Daily_basis_messengerDAO.getInstance().getDTOFromIdDeletedOrNot(billDTO.dailyBasisMessengerId);
        setEmployeeData(messengerDTO, language);
        this.dailyBasisMessengerId = String.valueOf(billDTO.dailyBasisMessengerId);
        this.dailyRate = String.valueOf(billDTO.dailyRate);
        this.day = String.valueOf(billDTO.day);
        this.totalAmount = String.valueOf(billDTO.dailyRate * billDTO.day);
        this.dailyBasisMessengerBillId = String.valueOf(billDTO.iD);
    }

    private void setEmployeeData(Daily_basis_messengerDTO messengerDTO, String language) {
        dailyBasisMessengerId = String.valueOf(messengerDTO.iD);
        name = messengerDTO.name;
        fatherName = messengerDTO.fatherName;
        mobileNumber = Utils.getDigits(messengerDTO.mobileNo, language);
        savingAccountNumber = Utils.getDigits(messengerDTO.bankAccountNo, language);
        designation = UtilCharacter.getDataByLanguage(
                language,
                Daily_basis_messenger_billServlet.DESIGNATION_BN,
                Daily_basis_messenger_billServlet.DESIGNATION_EN
        );
    }
}
