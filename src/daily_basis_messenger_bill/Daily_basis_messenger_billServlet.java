package daily_basis_messenger_bill;

import bill_register.Bill_registerServlet;
import budget.BudgetCategoryEnum;
import budget.BudgetUtils;
import budget_mapping.Budget_mappingDTO;
import budget_mapping.Budget_mappingRepository;
import budget_register.Budget_registerDAO;
import budget_register.Budget_registerDTO;
import budget_register.Budget_registerModel;
import budget_selection_info.BudgetSelectionInfoRepository;
import com.google.gson.GsonBuilder;
import common.BaseServlet;
import daily_basis_messenger.Daily_basis_messengerDAO;
import daily_basis_messenger.Daily_basis_messengerDTO;
import daily_basis_messenger.Daily_basis_messengerServlet;
import finance.CashTypeEnum;
import finance.FinanceUtil;
import login.LoginDTO;
import org.apache.log4j.Logger;
import pbReport.DateUtils;
import permission.MenuConstants;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;
import util.StringUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@SuppressWarnings({"Duplicates"})
@WebServlet("/Daily_basis_messenger_billServlet")
public class Daily_basis_messenger_billServlet extends BaseServlet {
    private static final long serialVersionUID = 1L;
    private static final Logger logger = Logger.getLogger(Daily_basis_messengerServlet.class);

    public static final long WORKER_WAGE_ECONOMIC_SUB_CODE_ID = 317L;
    public static final long BUDGET_OFFICE_ID = 1L;
    public static final String DESIGNATION_EN = "Daily Basis Messenger";
    public static final String DESIGNATION_BN = "দৈনিক ভিত্তিক বার্তাবাহক";
    public static final CashTypeEnum CASH_TYPE = CashTypeEnum.SALARY;
    public static final String BILL_DESCRIPTION = "বাংলাদেশ জাতীয় সংসদ সচিবায়ের %s জন দৈনিক ভিত্তিক বার্তাবাহক কর্মচারীর %s মাসের মজুরী";

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }

        try {
            String actionType = request.getParameter("actionType");
            switch (actionType) {
                case "ajax_getPayrollData": {
                    long electionDetailsId = Long.parseLong(request.getParameter("electionDetailsId"));
                    long parliamentSessionId = Long.parseLong(request.getParameter("parliamentSessionId"));
                    String language = userDTO.languageID == SessionConstants.ENGLISH ? "English" : "Bangla";
                    Map<String, Object> res = getPayrollData(electionDetailsId, parliamentSessionId, language);
                    response.setContentType("application/json");
                    response.setCharacterEncoding("UTF-8");
                    response.getWriter().println(new GsonBuilder().serializeNulls().create().toJson(res));
                    return;
                }
                case "prepareBill": {
                    setBillDTOs(request);
                    request.getRequestDispatcher("daily_basis_messenger_bill/daily_basis_messenger_billAgOfficeBill.jsp")
                           .forward(request, response);
                    return;
                }
                case "prepareBankStatement": {
                    setBillDTOs(request);
                    request.getRequestDispatcher("daily_basis_messenger_bill/daily_basis_messenger_billBankStatement.jsp")
                           .forward(request, response);
                    return;
                }
                default:
                    super.doGet(request, response);
                    return;
            }
        } catch (Exception ex) {
            logger.error(ex);
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }

    @Override
    public String getAjaxAddRedirectURL(HttpServletRequest request, CommonDTO commonDTO) {
        String url = getServletName() + "?actionType=";
        String source = request.getParameter("source");
        if ("prepareBill".equalsIgnoreCase(source)) {
            url += "prepareBill";
        } else if ("prepareBankStatement".equalsIgnoreCase(source)) {
            url += "prepareBankStatement";
        }
        Daily_basis_messenger_billDTO dailyBillDTO = (Daily_basis_messenger_billDTO) commonDTO;
        url += ("&parliamentSessionId=" + dailyBillDTO.parliamentSessionId);
        return url;
    }

    @Override
    public String getAjaxEditRedirectURL(HttpServletRequest request, CommonDTO commonDTO) {
        return getAjaxAddRedirectURL(request, commonDTO);
    }

    private void setBillDTOs(HttpServletRequest request) throws FileNotFoundException {
        long parliamentSessionId = Long.parseLong(request.getParameter("parliamentSessionId"));
        List<Daily_basis_messenger_billDTO> billDTOs =
                Daily_basis_messenger_billDAO.getInstance()
                                             .getDTOsByParliamentSessionId(parliamentSessionId);
        if (billDTOs == null || billDTOs.isEmpty())
            throw new FileNotFoundException("no DTOs found");
        request.setAttribute("billDTOs", billDTOs);
        request.setAttribute("parliamentSessionId", parliamentSessionId);
    }

    @SuppressWarnings({"unused"})
    private Map<String, Object> getPayrollData(long electionDetailsId, long parliamentSessionId, String language) {
        List<Daily_basis_messenger_billDTO> dtoList = Daily_basis_messenger_billDAO.getInstance().getDTOsByParliamentSessionId(parliamentSessionId);
        List<Daily_basis_messenger_billModel> daily_billModels;

        boolean isAlreadyAdded = dtoList != null && !dtoList.isEmpty();
        Budget_registerModel budgetRegisterModel = null;
        if (isAlreadyAdded) {
            daily_billModels =
                    dtoList.stream()
                           .map(dto -> new Daily_basis_messenger_billModel(dto, language))
                           .collect(Collectors.toList());
            budgetRegisterModel = Budget_registerDAO.getInstance().getModelById(dtoList.get(0).budgetRegisterId);
        } else {
            List<Daily_basis_messengerDTO> messengerInfoDTOs = Daily_basis_messengerDAO.getInstance()
                                                                                       .getAllActiveMessengers();
            daily_billModels =
                    messengerInfoDTOs.stream()
                                     .map(dto -> new Daily_basis_messenger_billModel(dto, language))
                                     .collect(Collectors.toList());
        }
        Map<String, Object> res = new HashMap<>();
        res.put("isAlreadyAdded", isAlreadyAdded);
        res.put("dailyBillModels", daily_billModels);
        res.put("budgetRegisterModel", budgetRegisterModel);
        return res;
    }

    public static String getBillDescription(UserInput userInput) {
        String monthName = DateUtils.getMonthYear(userInput.modificationTime, "Bangla", "/");
        return String.format(
                BILL_DESCRIPTION,
                StringUtils.convertToBanNumber(String.format("%d", userInput.employeeCount)),
                monthName
        );
    }

    private Budget_registerDTO addBudgetRegisterDTO(UserInput userInput) throws Exception {
        Budget_registerDTO budgetRegisterDTO = new Budget_registerDTO();

        budgetRegisterDTO.recipientName = FinanceUtil.getFinance1HeadDesignation("bangla");
        budgetRegisterDTO.issueNumber = "";
        budgetRegisterDTO.issueDate = SessionConstants.MIN_DATE;
        budgetRegisterDTO.description = getBillDescription(userInput);
        budgetRegisterDTO.billAmount = userInput.billAmount;

        Long budgetSelectionInfoId =
                BudgetSelectionInfoRepository.getInstance()
                                             .getId(BudgetUtils.getEconomicYear(userInput.modificationTime));
        if (budgetSelectionInfoId != null)
            budgetRegisterDTO.budgetSelectionInfoId = budgetSelectionInfoId;

        Budget_mappingDTO budgetMappingDTO =
                Budget_mappingRepository.getInstance()
                                        .getDTO(userInput.budgetOfficeId, BudgetCategoryEnum.OPERATIONAL.getValue());
        if (budgetMappingDTO != null) {
            budgetRegisterDTO.budgetMappingId = budgetMappingDTO.iD;
            budgetRegisterDTO.budgetOfficeId = budgetMappingDTO.budgetOfficeId;
        }
        budgetRegisterDTO.economicSubCodeId = WORKER_WAGE_ECONOMIC_SUB_CODE_ID;

        budgetRegisterDTO.insertionTime
                = budgetRegisterDTO.lastModificationTime
                = userInput.modificationTime;
        budgetRegisterDTO.insertedBy
                = budgetRegisterDTO.modifiedBy
                = userInput.modifierId;

        Budget_registerDAO.getInstance().add(budgetRegisterDTO);
        return budgetRegisterDTO;
    }

    public Daily_basis_messenger_billDTO getMessengerBillDTO(UserInput userInput, Daily_basis_messengerDTO messengerDTO) throws Exception {
        Daily_basis_messenger_billDTO messengerBillDTO = new Daily_basis_messenger_billDTO();
        messengerBillDTO.insertedBy = messengerBillDTO.modifiedBy = userInput.modifierId;
        messengerBillDTO.insertionTime = messengerBillDTO.lastModificationTime = userInput.modificationTime;
        messengerBillDTO.electionDetailsId = userInput.electionDetailsId;
        messengerBillDTO.parliamentSessionId = userInput.parliamentSessionId;
        messengerBillDTO.dailyBasisMessengerId = userInput.dailyBasisMessengerId;
        messengerBillDTO.dailyRate = messengerDTO.dailyRate;
        messengerBillDTO.day = userInput.day;
        messengerBillDTO.totalAmount = messengerBillDTO.dailyRate * messengerBillDTO.day;
        return messengerBillDTO;
    }

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        UserInput userInput = new UserInput();
        userInput.electionDetailsId = Long.parseLong(request.getParameter("electionDetailsId"));
        userInput.parliamentSessionId = Long.parseLong(request.getParameter("parliamentSessionId"));
        userInput.modificationTime = System.currentTimeMillis();
        userInput.budgetOfficeId = FinanceUtil.FINANCE_1_BUDGET_OFFICE_ID;
        if (addFlag) {
            List<Daily_basis_messenger_billDTO> dtoList =
                    Daily_basis_messenger_billDAO.getInstance()
                                                 .getDTOsByParliamentSessionId(userInput.parliamentSessionId);
            boolean isAlreadyAdded = dtoList != null && !dtoList.isEmpty();
            if (!isAlreadyAdded) {
                String[] messengerIds = request.getParameterValues("dailyBasisMessengerId");
                String[] billIds = request.getParameterValues("dailyBasisMessengerBillId");
                String[] dayCounts = request.getParameterValues("day");
                userInput.modifierId = userDTO.ID;
                Map<Long, Daily_basis_messengerDTO> messengerDTOsById =
                        Daily_basis_messengerDAO.getInstance()
                                                .getAllActiveMessengers()
                                                .stream()
                                                .collect(Collectors.toMap(
                                                        dto -> dto.iD,
                                                        dto -> dto,
                                                        (dto1, dto2) -> dto1
                                                ));
                List<Daily_basis_messenger_billDTO> messenger_billDTOS = new ArrayList<>();
                for (int i = 0; i < messengerIds.length; ++i) {
                    userInput.dailyBasisMessengerId = Long.parseLong(messengerIds[i]);
                    if (userInput.dailyBasisMessengerId < 0) continue;
                    userInput.dailyBasisMessengerBillId = Long.parseLong(billIds[i]);
                    userInput.day = Long.parseLong(dayCounts[i]);
                    if (messengerDTOsById.containsKey(userInput.dailyBasisMessengerId)) {
                        messenger_billDTOS.add(getMessengerBillDTO(
                                userInput,
                                messengerDTOsById.get(userInput.dailyBasisMessengerId)
                        ));
                    }
                }
                userInput.billAmount = messenger_billDTOS.stream()
                                                         .mapToLong(dto -> dto.totalAmount)
                                                         .sum();
                userInput.employeeCount = messenger_billDTOS.size();
                Budget_registerDTO addedBudget_registerDTO = addBudgetRegisterDTO(userInput);
                userInput.budgetRegisterId = addedBudget_registerDTO.iD;
                userInput.billRegisterId = Bill_registerServlet.addBillRegister(addedBudget_registerDTO, CASH_TYPE, true).iD;
                for (Daily_basis_messenger_billDTO messenger_billDTO : messenger_billDTOS) {
                    messenger_billDTO.budgetRegisterId = userInput.budgetRegisterId;
                    messenger_billDTO.billRegisterId = userInput.billRegisterId;
                    Daily_basis_messenger_billDAO.getInstance().add(messenger_billDTO);
                }
            }
        }
        Daily_basis_messenger_billDTO daily_billDTO = new Daily_basis_messenger_billDTO();
        daily_billDTO.parliamentSessionId = userInput.parliamentSessionId;
        return daily_billDTO;
    }

    @Override
    public String getTableName() {
        return Daily_basis_messenger_billDAO.getInstance().getTableName();
    }

    @Override
    public String getServletName() {
        return "Daily_basis_messenger_billServlet";
    }

    @Override
    public Daily_basis_messenger_billDAO getCommonDAOService() {
        return Daily_basis_messenger_billDAO.getInstance();
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[]{MenuConstants.DAILY_BASIS_MESSENGER_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[]{MenuConstants.DAILY_BASIS_MESSENGER_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[]{MenuConstants.DAILY_BASIS_MESSENGER_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Daily_basis_messenger_billServlet.class;
    }

    static class UserInput {
        long modifierId;
        long modificationTime;
        long electionDetailsId;
        long parliamentSessionId;
        long budgetOfficeId;
        long dailyBasisMessengerId;
        long dailyBasisMessengerBillId;
        long budgetRegisterId;
        long billRegisterId;
        long billAmount;
        long day;
        int employeeCount;
    }
}
