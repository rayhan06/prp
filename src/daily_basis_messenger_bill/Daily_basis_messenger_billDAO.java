package daily_basis_messenger_bill;

import common.CommonDAOService;
import org.apache.log4j.Logger;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings({"Duplicates"})
public class Daily_basis_messenger_billDAO implements CommonDAOService<Daily_basis_messenger_billDTO> {
    private static final Logger logger = Logger.getLogger(Daily_basis_messenger_billDAO.class);

    private static final String addQuery =
            "INSERT INTO {tableName} (budget_register_id,election_details_id,parliament_session_id,bill_register_id,daily_basis_messenger_id,daily_rate,day,"
                    .concat("total_amount,modified_by,lastModificationTime,inserted_by,insertion_time,isDeleted,ID)")
                    .concat("VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

    private static final String updateQuery =
            "UPDATE {tableName} SET budget_register_id=?,election_details_id=?,parliament_session_id=?,bill_register_id=?,daily_basis_messenger_id=?,daily_rate=?,day=?,"
                    .concat("total_amount=?,modified_by=?,lastModificationTime=? WHERE ID=?");

    private final Map<String, String> searchMap = new HashMap<>();

    private Daily_basis_messenger_billDAO() {

    }

    private static class LazyLoader {
        static final Daily_basis_messenger_billDAO INSTANCE = new Daily_basis_messenger_billDAO();
    }

    public static Daily_basis_messenger_billDAO getInstance() {
        return LazyLoader.INSTANCE;
    }

    @Override
    public void set(PreparedStatement ps, Daily_basis_messenger_billDTO dto, boolean isInsert) throws SQLException {
        int index = 0;
        ps.setLong(++index, dto.budgetRegisterId);
        ps.setLong(++index, dto.electionDetailsId);
        ps.setLong(++index, dto.parliamentSessionId);
        ps.setLong(++index, dto.billRegisterId);
        ps.setLong(++index, dto.dailyBasisMessengerId);
        ps.setLong(++index, dto.dailyRate);
        ps.setLong(++index, dto.day);
        ps.setLong(++index, dto.totalAmount);
        ps.setLong(++index, dto.modifiedBy);
        ps.setLong(++index, dto.lastModificationTime);
        if (isInsert) {
            ps.setLong(++index, dto.insertedBy);
            ps.setLong(++index, dto.insertionTime);
            ps.setObject(++index, 0 /* isDeleted */);
        }
        ps.setLong(++index, dto.iD);
    }

    @Override
    public Daily_basis_messenger_billDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Daily_basis_messenger_billDTO dto = new Daily_basis_messenger_billDTO();
            dto.iD = rs.getLong("ID");
            dto.budgetRegisterId = rs.getLong("budget_register_id");
            dto.electionDetailsId = rs.getLong("election_details_id");
            dto.parliamentSessionId = rs.getLong("parliament_session_id");
            dto.billRegisterId = rs.getLong("bill_register_id");
            dto.dailyBasisMessengerId = rs.getLong("daily_basis_messenger_id");
            dto.dailyRate = rs.getLong("daily_rate");
            dto.day = rs.getLong("day");
            dto.totalAmount = rs.getLong("total_amount");
            dto.modifiedBy = rs.getLong("modified_by");
            dto.lastModificationTime = rs.getLong("lastModificationTime");
            dto.insertedBy = rs.getLong("inserted_by");
            dto.insertionTime = rs.getLong("insertion_time");
            dto.isDeleted = rs.getInt("isDeleted");
            return dto;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    @Override
    public String getTableName() {
        return "daily_basis_messenger_bill";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    @Override
    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Daily_basis_messenger_billDTO) commonDTO, addQuery, true);
    }

    @Override
    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Daily_basis_messenger_billDTO) commonDTO, updateQuery, false);
    }

    private static final String getByParliamentSession =
            "SELECT * FROM daily_basis_messenger_bill WHERE parliament_session_id=%d AND isDeleted=0";

    public List<Daily_basis_messenger_billDTO> getDTOsByParliamentSessionId(long parliamentSessionId) {
        return getDTOs(String.format(getByParliamentSession, parliamentSessionId));
    }
}