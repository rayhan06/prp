package daily_basis_messenger_bill;

import sessionmanager.SessionConstants;
import util.CommonDTO;

public class Daily_basis_messenger_billDTO extends CommonDTO {
    public long budgetRegisterId = -1;
    public long electionDetailsId = -1;
    public long parliamentSessionId = -1;
    public long billRegisterId = -1;
    public long dailyBasisMessengerId = -1;
    public long dailyRate = 0;
    public long day = 0;
    public long totalAmount = 0;
    public long modifiedBy = -1;
    public long insertedBy = -1;
    public long insertionTime = SessionConstants.MIN_DATE;

    @Override
    public String toString() {
        return "Daily_basis_messenger_billDTO{" +
                "budgetRegisterId=" + budgetRegisterId +
                ", billRegisterId=" + billRegisterId +
                ", dailyBasisMessengerId=" + dailyBasisMessengerId +
                ", dailyRate=" + dailyRate +
                ", day=" + day +
                ", totalAmount=" + totalAmount +
                ", modifiedBy=" + modifiedBy +
                ", insertedBy=" + insertedBy +
                ", insertionTime=" + insertionTime +
                ", iD=" + iD +
                '}';
    }
}
