package profile;

import dbm.DBMW;
import model.EmployeeWorkHistory;
import model.employee_records;
import model.profile_images;
import model.signatures;
import test_lib.RInsertQueryBuilder;
import test_lib.RQueryBuilder;
import user.UserDTO;
import util.PasswordUtil;

import java.util.ArrayList;
import java.util.HashMap;

public class ProfileService {
    public ProfileService() {
    }

    public employee_records getEmployeeRecord(long id) {
        String sql = "select * from employee_records where id = " + id;
        RQueryBuilder<employee_records> rQueryBuilder = new RQueryBuilder<>();
        employee_records data = rQueryBuilder.setSql(sql).of(employee_records.class).buildRaw().get(0);
        return data;
    }

    public String getEmployeeWorkHistory(long employeeRecordId) {
        String sql = String.format("select\n" +
                "\n" +
                "employee_offices.employee_record_id,\n" +
                "offices.office_name_eng,\n" +
                "offices.office_name_bng,\n" +
                "office_unit_organograms.designation_eng,\n" +
                "office_unit_organograms.designation_bng,\n" +
                "office_units.unit_name_bng,\n" +
                "office_units.unit_name_eng,\n" +
                "employee_offices.joining_date,\n" +
                "employee_offices.last_office_date\n" +
                "\n" +
                "from employee_offices, offices, office_unit_organograms, office_units\n" +
                "where employee_offices.employee_record_id = %d\n" +
                "and offices.id = employee_offices.office_id\n" +
                "and office_unit_organograms.id = employee_offices.office_unit_organogram_id\n" +
                "and employee_offices.office_unit_id = office_units.id", employeeRecordId);

        RQueryBuilder<EmployeeWorkHistory> rQueryBuilder = new RQueryBuilder<>();
        String data = rQueryBuilder.setSql(sql).of(EmployeeWorkHistory.class).buildJson();
        return data;
    }

    public boolean uploadProfileImage(String image, long employeeRecordId, String username) {
        profile_images img = new profile_images();
        img.setId(getProfileImageTableId());
        img.setImage(image);
        img.setEmployee_record_id(employeeRecordId);
        img.setUsername(username);
        img.setIsDeleted(false);
        img.setLastModificationTime(System.currentTimeMillis());

        this.setCurrentProfileImage(employeeRecordId);
        RInsertQueryBuilder<profile_images> builder = new RInsertQueryBuilder<>();
        boolean r = builder.of(profile_images.class).model(img).buildInsert();
        return r;
    }

    public boolean uploadSignatureImage(String image, long employeeRecordId, String username) {
        signatures sig = new signatures();
        sig.setId(getSignatureTableId());
        sig.setSignature(image);
        sig.setEmployee_record_id(employeeRecordId);
        sig.setUsername(username);
        sig.setIsDeleted(false);
        sig.setLastModificationTime(System.currentTimeMillis());

        this.setCurrentSignature(employeeRecordId);
        RInsertQueryBuilder<signatures> builder = new RInsertQueryBuilder<>();
        boolean r = builder.of(signatures.class).model(sig).buildInsert();
        return r;
    }

    private void setCurrentProfileImage(long employeeRecordId) {
        String sql = String.format("update profile_images set isDeleted = 1 where employee_record_id = %d;", employeeRecordId);

        RQueryBuilder<Boolean> rQueryBuilder = new RQueryBuilder<>();
        boolean r = rQueryBuilder.setSql(sql).of(Boolean.class).buildUpdate();
    }

    private void setCurrentSignature(long employeeRecordId) {
        String sql = String.format("update signatures set isDeleted = 1 where employee_record_id = %d;", employeeRecordId);

        RQueryBuilder<Boolean> rQueryBuilder = new RQueryBuilder<>();
        boolean r = rQueryBuilder.setSql(sql).of(Boolean.class).buildUpdate();
    }

    public boolean changePassword(UserDTO userDTO, String passwordOld, String password, long employeeRecordId) {
        try {
            String encOldPassword = PasswordUtil.getInstance().encrypt(passwordOld);
            if (!userDTO.password.equals(encOldPassword)) return false;

            String encPassword = PasswordUtil.getInstance().encrypt(password);
            String sql = String.format("update users set password = '%s' where employee_record_id = %d", encPassword, employeeRecordId);

            RQueryBuilder<Boolean> rQueryBuilder = new RQueryBuilder<>();
            boolean r = rQueryBuilder.setSql(sql).of(Boolean.class).buildUpdate();
            if (r) userDTO.password = encPassword;
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public profile_images getCurrentProfileImage(long employeeRecordId) {
        String sql = String.format("select * from profile_images where employee_record_id = %d and isDeleted = 0", employeeRecordId);

        RQueryBuilder<profile_images> rQueryBuilder = new RQueryBuilder<>();
        ArrayList<profile_images> data = rQueryBuilder.setSql(sql).of(profile_images.class).buildRaw();
        return data != null ? data.get(0) : new profile_images();
    }

    public ArrayList<HashMap> getCurrentOfficeName(long officeId) {
        String sql = String.format("select office_name_eng, office_name_bng from offices where id = %d", officeId);

        RQueryBuilder<HashMap> rQueryBuilder = new RQueryBuilder<>();
        ArrayList<HashMap> data = rQueryBuilder.setSql(sql).of(HashMap.class).buildHashMap();
        return data;
    }

    public signatures getCurrentSignature(long employeeRecordId) {
        String sql = String.format("select * from signatures where employee_record_id = %d and isDeleted = 0", employeeRecordId);

        RQueryBuilder<signatures> rQueryBuilder = new RQueryBuilder<>();
        ArrayList<signatures> data = rQueryBuilder.setSql(sql).of(signatures.class).buildRaw();
        return data != null ? data.get(0) : new signatures();
    }

    private long getProfileImageTableId() {
        try {
            return DBMW.getInstance().getNextSequenceId("profile_images");
        } catch (Exception e) {
            return 0l;
        }
    }

    private long getSignatureTableId() {
        try {
            return DBMW.getInstance().getNextSequenceId("signatures");
        } catch (Exception e) {
            return 0l;
        }
    }
}
