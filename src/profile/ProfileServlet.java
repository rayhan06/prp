/*
package profile;

import com.google.gson.Gson;
import login.LoginDTO;
import model.employee_records;
import model.profile_images;
import model.signatures;
import org.apache.log4j.Logger;
import protikolpo.protikolpo_dashboard.ProtikolpoDashboardDAO;
import protikolpo.protikolpo_dashboard.ProtikolpoDashboardModel;
import sessionmanager.SessionConstants;
import sun.misc.IOUtils;
import user.UserDTO;
import user.UserRepository;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Base64;

@WebServlet("/ProfileServlet")
@MultipartConfig
public class ProfileServlet extends HttpServlet {
    public static Logger logger = Logger.getLogger(ProfileServlet.class);

    public ProfileServlet() {
        super();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);

        try {
            String actionType = request.getParameter("actionType");
            switch (actionType) {
                case "getProfile":
                    this.getProfilePageTemplate(request, response, userDTO);
                    break;
                case "getProfilePage":
                    this.getProfilePage(request, response, userDTO);
                    break;
                case "getPasswordPage":
                    this.getPasswordPage(request, response, userDTO);
                    break;
                case "getPPPage":
                    this.getPPPage(request, response, userDTO);
                    break;
                case "getSignaturePage":
                    this.getSignaturePage(request, response, userDTO);
                    break;
                case "getProtikolpoPage":
                    this.getProtikolpoPage(request, response, userDTO);
                    break;
                case "getWorkHistory":
                    response.setContentType("application/json");

                    PrintWriter out = response.getWriter();
                    out.print(new ProfileService().getEmployeeWorkHistory(userDTO.employee_record_id));
                    out.flush();
                    break;
                case "getProfilePic":
                    response.setContentType("application/json");
                    profile_images b0 = new ProfileService().getCurrentProfileImage(userDTO.employee_record_id);
                    PrintWriter out0 = response.getWriter();
                    out0.print(new Gson().toJson(b0));
                    out0.flush();
                    break;
                case "getSignaturePic":
                    response.setContentType("application/json");
                    signatures b1 = new ProfileService().getCurrentSignature(userDTO.employee_record_id);
                    PrintWriter out1 = response.getWriter();
                    out1.print(new Gson().toJson(b1));
                    out1.flush();
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);

        try {
            String actionType = request.getParameter("actionType");
            switch (actionType) {
                case "profileImage":
                    response.setContentType("application/json");

                    PrintWriter out = response.getWriter();
                    out.print(uploadImage(request, response, userDTO));
                    out.flush();
                    break;
                case "signatureImage":
                    response.setContentType("application/json");

                    PrintWriter out0 = response.getWriter();
                    out0.print(uploadImage(request, response, userDTO));
                    out0.flush();
                    break;
                case "changePassword":
                    response.setContentType("application/json");
                    String passwordOld = request.getParameter("password_old");
                    String password = request.getParameter("password");

                    PrintWriter out1 = response.getWriter();
                    out1.print(new ProfileService().changePassword(userDTO, passwordOld, password, userDTO.employee_record_id));
                    out1.flush();
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean uploadImage(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO) {
        String image_type = request.getParameter("image_type");
        Part image = null;
        try {
            image = request.getPart("image");
        } catch (IOException | ServletException e) {
            return false;
        }

        String value = getFileName(image);
        if (value != null && !value.equalsIgnoreCase("")) {
            if (value.toLowerCase().endsWith(".jpg") || value.toLowerCase().endsWith(".png") || value.toLowerCase().endsWith(".gif") || value.toLowerCase().endsWith(".bmp") || value.toLowerCase().endsWith(".ico")) {
                try {
                    int length = value.length();
                    byte[] bb = IOUtils.readFully(image.getInputStream(), -1, false);

                    String encodedFile = value.toLowerCase().substring(length - 3) + ";" + Base64.getEncoder().encodeToString(bb);
                    System.out.println(encodedFile);
                    if (image_type.equals("profile")) {
                        return new ProfileService().uploadProfileImage(encodedFile, userDTO.employee_record_id, userDTO.userName);
                    } else {
                        new ProfileService().uploadSignatureImage(encodedFile, userDTO.employee_record_id, userDTO.userName);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return true;
    }

    private void getProfilePageTemplate(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO) {
        request.setAttribute("profile_image", new ProfileService().getCurrentProfileImage(userDTO.employee_record_id));
        request.setAttribute("office_name", new ProfileService().getCurrentOfficeName(userDTO.officeID));
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("profile/ProfileDashboard.jsp");
        try {
            requestDispatcher.forward(request, response);
        } catch (ServletException | IOException e) {
            e.printStackTrace();
        }
    }

    private void getProfilePage(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO) {
        ProfileService profileService = new ProfileService();
        employee_records s = profileService.getEmployeeRecord(userDTO.employee_record_id);
        request.setAttribute("employee_records", s);

        RequestDispatcher requestDispatcher = request.getRequestDispatcher("profile/profile_tabs/ProfileTab.jsp");
        try {
            requestDispatcher.forward(request, response);
        } catch (ServletException | IOException e) {
            e.printStackTrace();
        }
    }

    private void getPasswordPage(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO) {
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("profile/profile_tabs/PasswordTab.jsp");
        try {
            requestDispatcher.forward(request, response);
        } catch (ServletException | IOException e) {
            e.printStackTrace();
        }
    }

    private void getPPPage(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO) {
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("profile/profile_tabs/ProfileImageTab.jsp");
        request.setAttribute("profile_image", new ProfileService().getCurrentProfileImage(userDTO.employee_record_id));

        try {
            requestDispatcher.forward(request, response);
        } catch (ServletException | IOException e) {
            e.printStackTrace();
        }
    }

    private void getSignaturePage(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO) {
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("profile/profile_tabs/SignatureTab.jsp");
        signatures signature = new ProfileService().getCurrentSignature(userDTO.employee_record_id);
        request.setAttribute("signature", signature);

        try {
            requestDispatcher.forward(request, response);
        } catch (ServletException | IOException e) {
            e.printStackTrace();
        }
    }

    private void getProtikolpoPage(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO) {
        ProtikolpoDashboardDAO protikolpoDashboardDAO = new ProtikolpoDashboardDAO();
        ArrayList<ProtikolpoDashboardModel.protikolpo_settings> data = protikolpoDashboardDAO.getEmployeeProtikolpo(String.valueOf(userDTO.employee_record_id));
        request.getSession().setAttribute("data", data);

        request.setAttribute("ID", -1L);
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("protikolpo/protikolpo_dashboard/EmployeeProtikolpoDashboardBody.jsp");
        try {
            requestDispatcher.forward(request, response);
        } catch (ServletException | IOException e) {
            e.printStackTrace();
        }
    }

    private String getFileName(final Part part) {
        final String partHeader = part.getHeader("content-disposition");
        System.out.println("Part Header = {0}" + partHeader);
        for (String content : part.getHeader("content-disposition").split(";")) {
            if (content.trim().startsWith("filename")) {
                return content.substring(content.indexOf('=') + 1).trim().replace("\"", "");
            }
        }
        return null;
    }
}
*/
