package disciplinary_details;

public class DisciplinaryDetailsModel {
	public Disciplinary_detailsDTO dto;
	public String suspensionDaysBan = "";
	public String complainEngText = "";
	public String complainBangText = "";

	@Override
	public String toString() {
		return "DisplinaryDetailsModel{" +
				"dto=" + dto +
				", suspensionDaysBan='" + suspensionDaysBan + '\'' +
				", complainEngText='" + complainEngText + '\'' +
				", complainBangText='" + complainBangText + '\'' +
				'}';
	}
}
