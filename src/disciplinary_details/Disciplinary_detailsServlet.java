//package disciplinary_details;
//
//import java.io.IOException;
//import java.io.*;
//import java.text.SimpleDateFormat;
//
//import javax.servlet.RequestDispatcher;
//import javax.servlet.ServletException;
//import javax.servlet.annotation.MultipartConfig;
//import javax.servlet.annotation.WebServlet;
//
//import org.apache.log4j.Logger;
//
//import login.LoginDTO;
//import permission.MenuConstants;
//import role.PermissionRepository;
//
//import sessionmanager.SessionConstants;
//
//import user.UserDTO;
//import user.UserRepository;
//import util.CommonDTO;
//import util.CommonRequestHandler;
//import util.RecordNavigationManager4;
//
//import java.util.*;
//import javax.servlet.http.*;
//import java.util.UUID;
//
//import geolocation.GeoLocationDAO2;
//import java.util.StringTokenizer;
//
//import com.google.gson.Gson;
//
//import pb.*;
//import pbReport.*;
//import org.jsoup.Jsoup;
//import org.jsoup.safety.Whitelist;
//
///**
// * Servlet implementation class Disciplinary_detailsServlet
// */
//@WebServlet("/Disciplinary_detailsServlet")
//@MultipartConfig
//public class Disciplinary_detailsServlet extends HttpServlet {
//	private static final long serialVersionUID = 1L;
//	public static final Logger logger = Logger.getLogger(Disciplinary_detailsServlet.class);
//
//	String tableName = "disciplinary_details";
//
//	Disciplinary_detailsDAO disciplinary_detailsDAO;
//	CommonRequestHandler commonRequestHandler;
//	private Gson gson = new Gson();
//
//	/**
//	 * @see HttpServlet#HttpServlet()
//	 */
//	public Disciplinary_detailsServlet() {
//		super();
//		try {
//			disciplinary_detailsDAO = new Disciplinary_detailsDAO(tableName);
//			commonRequestHandler = new CommonRequestHandler(disciplinary_detailsDAO);
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			logger.error(e);
//		}
//	}
//
//	/**
//	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
//	 *      response)
//	 */
//	protected void doGet(HttpServletRequest request, HttpServletResponse response)
//			throws ServletException, IOException {
//		LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
//		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
//		boolean isPermanentTable = true;
//		if (request.getParameter("isPermanentTable") != null) {
//			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
//		}
//
//		try {
//			String actionType = request.getParameter("actionType");
//			switch (actionType) {
//				case "getAddPage":
//					if (PermissionRepository.getInstance().checkPermissionByRoleIDAndMenuID(userDTO.roleID,
//							MenuConstants.DISCIPLINARY_DETAILS_ADD)) {
//						commonRequestHandler.getAddPage(request, response);
//					} else {
//						request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
//					}
//					break;
//				case "getEditPage":
//					if (PermissionRepository.getInstance().checkPermissionByRoleIDAndMenuID(userDTO.roleID,
//							MenuConstants.DISCIPLINARY_DETAILS_UPDATE)) {
//						getDisciplinary_details(request, response);
//					} else {
//						request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
//					}
//					break;
//				case "getURL":
//					String URL = request.getParameter("URL");
//					System.out.println("URL = " + URL);
//					response.sendRedirect(URL);
//					break;
//				case "search":
//					System.out.println("search requested");
//					if (PermissionRepository.getInstance().checkPermissionByRoleIDAndMenuID(userDTO.roleID,
//							MenuConstants.DISCIPLINARY_DETAILS_SEARCH)) {
//						if (isPermanentTable) {
//							String filter = request.getParameter("filter");
//							System.out.println("filter = " + filter);
//							if (filter != null) {
//								filter = ""; // shouldn't be directly used, rather manipulate it.
//								searchDisciplinary_details(request, response, isPermanentTable, filter);
//							} else {
//								searchDisciplinary_details(request, response, isPermanentTable, "");
//							}
//						} else {
//
//						}
//					}
//					break;
//				case "view":
//					System.out.println("view requested");
//					if (PermissionRepository.getInstance().checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.DISCIPLINARY_DETAILS_SEARCH)) {
//						commonRequestHandler.view(request, response);
//					} else {
//						request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
//					}
//
//					break;
//				default:
//					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
//					break;
//			}
//		} catch (Exception ex) {
//			logger.error(ex);
//		}
//	}
//
//	protected void doPost(HttpServletRequest request, HttpServletResponse response)
//			throws ServletException, IOException {
//		// TODO Auto-generated method stub
//		LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
//		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
//		System.out.println("doPost");
//		boolean isPermanentTable = true;
//		if (request.getParameter("isPermanentTable") != null) {
//			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
//		}
//		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
//
//		try {
//			String actionType = request.getParameter("actionType");
//			System.out.println("actionType = " + actionType);
//			if (actionType.equals("add")) {
//
//				if (PermissionRepository.getInstance().checkPermissionByRoleIDAndMenuID(userDTO.roleID,
//						MenuConstants.DISCIPLINARY_DETAILS_ADD)) {
//					System.out.println("going to  addDisciplinary_details ");
//					addDisciplinary_details(request, response, true, userDTO, true);
//				} else {
//					System.out.println("Not going to  addDisciplinary_details ");
//					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
//				}
//
//			}
//
//			else if (actionType.equals("getDTO")) {
//
//				if (PermissionRepository.getInstance().checkPermissionByRoleIDAndMenuID(userDTO.roleID,
//						MenuConstants.DISCIPLINARY_DETAILS_ADD)) {
//					getDTO(request, response);
//				} else {
//					System.out.println("Not going to  addDisciplinary_details ");
//					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
//				}
//
//			} else if (actionType.equals("edit")) {
//
//				if (PermissionRepository.getInstance().checkPermissionByRoleIDAndMenuID(userDTO.roleID,
//						MenuConstants.DISCIPLINARY_DETAILS_UPDATE)) {
//					addDisciplinary_details(request, response, false, userDTO, isPermanentTable);
//				} else {
//					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
//				}
//			} else if (actionType.equals("delete")) {
//				deleteDisciplinary_details(request, response, userDTO);
//			} else if (actionType.equals("search")) {
//				if (PermissionRepository.getInstance().checkPermissionByRoleIDAndMenuID(userDTO.roleID,
//						MenuConstants.DISCIPLINARY_DETAILS_SEARCH)) {
//					searchDisciplinary_details(request, response, true, "");
//				} else {
//					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
//				}
//			} else if (actionType.equals("getGeo")) {
//				System.out.println("going to geoloc ");
//				request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
//			}
//
//		} catch (Exception ex) {
//			logger.error(ex);
//			logger.debug(ex);
//		}
//	}
//
//	private void getDTO(HttpServletRequest request, HttpServletResponse response) {
//		try {
//			System.out.println("In getDTO");
//			Disciplinary_detailsDTO disciplinary_detailsDTO = (Disciplinary_detailsDTO) disciplinary_detailsDAO
//					.getDTOByID(Long.parseLong(request.getParameter("ID")));
//			PrintWriter out = response.getWriter();
//			response.setContentType("application/json");
//			response.setCharacterEncoding("UTF-8");
//
//			String encoded = this.gson.toJson(disciplinary_detailsDTO);
//			System.out.println("json encoded data = " + encoded);
//			out.print(encoded);
//			out.flush();
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			logger.error(e);
//		}
//
//	}
//
//	private void addDisciplinary_details(HttpServletRequest request, HttpServletResponse response, Boolean addFlag,
//			UserDTO userDTO, boolean isPermanentTable) {
//		// TODO Auto-generated method stub
//		try {
//			request.setAttribute("failureMessage", "");
//			System.out.println("%%%% addDisciplinary_details");
//			String path = getServletContext().getRealPath("/img2/");
//			Disciplinary_detailsDTO disciplinary_detailsDTO;
//			SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
//
//			if (addFlag) {
//				disciplinary_detailsDTO = new Disciplinary_detailsDTO();
//				disciplinary_detailsDTO.employeeRecordsId = Long.parseLong(request.getParameter("employeeRecordsId"));
//			} else {
//				disciplinary_detailsDTO = (Disciplinary_detailsDTO) disciplinary_detailsDAO
//						.getDTOByID(Long.parseLong(request.getParameter("iD")));
//			}
//
//			String Value = "";
//
//			Value = request.getParameter("disciplinaryLogId");
//
//			if (Value != null) {
//				Value = Jsoup.clean(Value, Whitelist.simpleText());
//			}
//			System.out.println("disciplinaryLogId = " + Value);
//			if (Value != null && !Value.equalsIgnoreCase("")) {
//
//				disciplinary_detailsDTO.disciplinaryLogId = (Value);
//			} else {
//				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//			}
//
//			Value = request.getParameter("suspensionDays");
//
//			if (Value != null) {
//				Value = Jsoup.clean(Value, Whitelist.simpleText());
//			}
//			System.out.println("suspensionDays = " + Value);
//			if (Value != null && !Value.equalsIgnoreCase("")) {
//
//				disciplinary_detailsDTO.suspensionDays = Integer.parseInt(Value);
//			} else {
//				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//			}
//
//			Value = request.getParameter("complainActionType");
//
//			if (Value != null) {
//				Value = Jsoup.clean(Value, Whitelist.simpleText());
//			}
//			System.out.println("complainActionType = " + Value);
//			if (Value != null && !Value.equalsIgnoreCase("")) {
//
//				disciplinary_detailsDTO.complainActionType = Long.parseLong(Value);
//			} else {
//				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//			}
//
//			Value = request.getParameter("complianOther");
//
//			if (Value != null) {
//				Value = Jsoup.clean(Value, Whitelist.simpleText());
//			}
//			System.out.println("complianOther = " + Value);
//			if (Value != null && !Value.equalsIgnoreCase("")) {
//
//				disciplinary_detailsDTO.complianOther = (Value);
//			} else {
//				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//			}
//
//			Value = request.getParameter("remarks");
//
//			if (Value != null) {
//				Value = Jsoup.clean(Value, Whitelist.simpleText());
//			}
//			System.out.println("remarks = " + Value);
//			if (Value != null && !Value.equalsIgnoreCase("")) {
//
//				disciplinary_detailsDTO.remarks = (Value);
//			} else {
//				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//			}
//
//			if (addFlag) {
//				Calendar c = Calendar.getInstance();
//				c.set(Calendar.HOUR_OF_DAY, 0);
//				c.set(Calendar.MINUTE, 0);
//				c.set(Calendar.SECOND, 0);
//				c.set(Calendar.MILLISECOND, 0);
//
//				disciplinary_detailsDTO.insertionDate = c.getTimeInMillis();
//			}
//
//			Value = request.getParameter("insertedBy");
//
//			if (Value != null) {
//				Value = Jsoup.clean(Value, Whitelist.simpleText());
//			}
//			System.out.println("insertedBy = " + Value);
//			if (Value != null && !Value.equalsIgnoreCase("")) {
//
//				disciplinary_detailsDTO.insertedBy = (Value);
//			} else {
//				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//			}
//
//			Value = request.getParameter("modifiedBy");
//
//			if (Value != null) {
//				Value = Jsoup.clean(Value, Whitelist.simpleText());
//			}
//			System.out.println("modifiedBy = " + Value);
//			if (Value != null && !Value.equalsIgnoreCase("")) {
//
//				disciplinary_detailsDTO.modifiedBy = (Value);
//			} else {
//				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//			}
//
//			System.out.println("Done adding  addDisciplinary_details dto = " + disciplinary_detailsDTO);
//			long returnedID = -1;
//
//			if (!isPermanentTable) // add new row for validation and make the old row outdated
//			{
//				disciplinary_detailsDAO.setIsDeleted(disciplinary_detailsDTO.iD, CommonDTO.OUTDATED);
//				returnedID = disciplinary_detailsDAO.add(disciplinary_detailsDTO);
//				disciplinary_detailsDAO.setIsDeleted(returnedID, CommonDTO.WAITING_FOR_APPROVAL);
//			} else if (addFlag) {
//				returnedID = disciplinary_detailsDAO.manageWriteOperations(disciplinary_detailsDTO,
//						SessionConstants.INSERT, -1, userDTO);
//			} else {
//				returnedID = disciplinary_detailsDAO.manageWriteOperations(disciplinary_detailsDTO,
//						SessionConstants.UPDATE, -1, userDTO);
//			}
//
//			if (isPermanentTable) {
//				String inPlaceSubmit = request.getParameter("inplacesubmit");
//
//				if (inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase("")) {
//					getDisciplinary_details(request, response, returnedID);
//				} else {
//					//response.sendRedirect("Disciplinary_detailsServlet?actionType=search");
//					response.sendRedirect(request.getContextPath() + "/Employee_recordsServlet?actionType=viewMultiForm&data=disciplinary_details&tab=6&ID=" + disciplinary_detailsDTO.employeeRecordsId);
//				}
//			} else {
//				commonRequestHandler.validate(disciplinary_detailsDAO.getDTOByID(returnedID), request, response,
//						userDTO);
//			}
//
//		} catch (Exception e) {
//			logger.error(e);
//		}
//	}
//
//	private void deleteDisciplinary_details(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO) {
//		long empId = 0;
//		try {
//			String[] IDsToDelete = request.getParameterValues("ID");
//			for (String s : IDsToDelete) {
//				long id = Long.parseLong(s);
//				System.out.println("------ DELETING " + s);
//				Disciplinary_detailsDTO disciplinary_detailsDTO = (Disciplinary_detailsDTO) disciplinary_detailsDAO.getDTOByID(id);
//				empId = disciplinary_detailsDTO.employeeRecordsId;
//				disciplinary_detailsDAO.manageWriteOperations(disciplinary_detailsDTO, SessionConstants.DELETE, id, userDTO);
//			}
//			//response.sendRedirect("Disciplinary_detailsServlet?actionType=search");
//			response.sendRedirect(request.getContextPath() + "/Employee_recordsServlet?actionType=viewMultiForm&data=disciplinary_details&tab=6&ID="+empId);
//		} catch (Exception ex) {
//			logger.error(ex);
//		}
//
//	}
//
//	private void getDisciplinary_details(HttpServletRequest request, HttpServletResponse response, long id) {
//		System.out.println("in getDisciplinary_details");
//		Disciplinary_detailsDTO disciplinary_detailsDTO = null;
//		try {
//			disciplinary_detailsDTO = (Disciplinary_detailsDTO) disciplinary_detailsDAO.getDTOByID(id);
//			request.setAttribute("ID", disciplinary_detailsDTO.iD);
//			request.setAttribute("disciplinary_detailsDTO", disciplinary_detailsDTO);
//			request.setAttribute("disciplinary_detailsDAO", disciplinary_detailsDAO);
//			//Get Employee Record List by Log Id
//			/*String logId=disciplinary_detailsDTO.disciplinaryLogId;
//			List<Disciplinary_detailsDTO>dtoList=disciplinary_detailsDAO.getDTOsByLogId(logId);
//			logger.debug("Testing getDTOsByLogId Query");
//			for(Disciplinary_detailsDTO dto:dtoList){
//				logger.debug(dto.employeeRecordsId);
//			}*/
//			String URL = "";
//
//			String inPlaceEdit = request.getParameter("inplaceedit");
//			String inPlaceSubmit = request.getParameter("inplacesubmit");
//			String getBodyOnly = request.getParameter("getBodyOnly");
//
//			if (inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase("")) {
//				URL = "disciplinary_details/disciplinary_detailsInPlaceEdit.jsp";
//				request.setAttribute("inplaceedit", "");
//			} else if (inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase("")) {
//				URL = "disciplinary_details/disciplinary_detailsSearchRow.jsp";
//				request.setAttribute("inplacesubmit", "");
//			} else {
//				if (getBodyOnly != null && !getBodyOnly.equalsIgnoreCase("")) {
//					URL = "disciplinary_details/disciplinary_detailsEditBody.jsp?actionType=edit";
//				} else {
//					URL = "disciplinary_details/disciplinary_detailsEdit.jsp?actionType=edit";
//				}
//			}
//
//			RequestDispatcher rd = request.getRequestDispatcher(URL);
//			rd.forward(request, response);
//		} catch (Exception e) {
//			logger.error(e);
//		}
//	}
//
//	private void getDisciplinary_details(HttpServletRequest request, HttpServletResponse response) {
//		getDisciplinary_details(request, response, Long.parseLong(request.getParameter("ID")));
//	}
//
//	private void searchDisciplinary_details(HttpServletRequest request, HttpServletResponse response,
//			boolean isPermanent, String filter) throws ServletException, IOException {
//		System.out.println("in  searchDisciplinary_details 1");
//		LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
//		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
//		String ajax = request.getParameter("ajax");
//		boolean hasAjax = false;
//		if (ajax != null && !ajax.equalsIgnoreCase("")) {
//			hasAjax = true;
//		}
//		System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);
//
//		RecordNavigationManager4 rnManager = new RecordNavigationManager4(SessionConstants.NAV_DISCIPLINARY_DETAILS,
//				request, disciplinary_detailsDAO, SessionConstants.VIEW_DISCIPLINARY_DETAILS,
//				SessionConstants.SEARCH_DISCIPLINARY_DETAILS, tableName, isPermanent, userDTO, filter, true);
//		try {
//			System.out.println("trying to dojob");
//			rnManager.doJob(loginDTO);
//		} catch (Exception e) {
//			System.out.println("failed to dojob" + e);
//		}
//
//		request.setAttribute("disciplinary_detailsDAO", disciplinary_detailsDAO);
//		RequestDispatcher rd;
//		if (!isPermanent) {
//			if (!hasAjax) {
//				System.out.println("Going to disciplinary_details/disciplinary_detailsApproval.jsp");
//				rd = request.getRequestDispatcher("disciplinary_details/disciplinary_detailsApproval.jsp");
//			} else {
//				System.out.println("Going to disciplinary_details/disciplinary_detailsApprovalForm.jsp");
//				rd = request.getRequestDispatcher("disciplinary_details/disciplinary_detailsApprovalForm.jsp");
//			}
//		} else {
//			if (!hasAjax) {
//				System.out.println("Going to disciplinary_details/disciplinary_detailsSearch.jsp");
//				rd = request.getRequestDispatcher("disciplinary_details/disciplinary_detailsSearch.jsp");
//			} else {
//				System.out.println("Going to disciplinary_details/disciplinary_detailsSearchForm.jsp");
//				rd = request.getRequestDispatcher("disciplinary_details/disciplinary_detailsSearchForm.jsp");
//			}
//		}
//		rd.forward(request, response);
//	}
//
//}
