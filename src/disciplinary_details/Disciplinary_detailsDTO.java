package disciplinary_details;

import employee_records.EmployeeFlatInfoDTO;
import sessionmanager.SessionConstants;

public class Disciplinary_detailsDTO extends EmployeeFlatInfoDTO {

	public String disciplinaryLogId = "";
	public long fromDate=SessionConstants.MIN_DATE;
	public long toDate=SessionConstants.MIN_DATE;
	public int suspensionDays = 0;
	public long complainActionType = 0;
	public String complianOther = "";
	//public String remarks = "";
	public long insertionDate = 0;
	public String insertedBy = "";
	public String modifiedBy = "";

	public String getIdsStrInJsonString(){
		return String.format("{employeeRecordId: '%s', officeUnitId: '%s', organogramId: '%s'}",
				                employeeRecordsId,officeUnitId,organogramId);
	}


	@Override
	public String toString() {
		return "Disciplinary_detailsDTO{" +
				"disciplinaryLogId='" + disciplinaryLogId + '\'' +
				", suspensionDays=" + suspensionDays +
				", complainActionType=" + complainActionType +
				", complianOther='" + complianOther + '\'' +
				", remarks='" + remarks + '\'' +
				", insertionDate=" + insertionDate +
				", insertedBy='" + insertedBy + '\'' +
				", modifiedBy='" + modifiedBy + '\'' +
				", searchColumn='" + searchColumn + '\'' +
				'}';
	}
}