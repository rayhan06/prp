package disciplinary_details;
import java.util.*; 
import util.*;


public class Disciplinary_detailsMAPS extends CommonMaps
{	
	public Disciplinary_detailsMAPS(String tableName)
	{
		
		java_allfield_type_map.put("disciplinary_log_id".toLowerCase(), "String");
		java_allfield_type_map.put("employee_records_id".toLowerCase(), "Long");
		java_allfield_type_map.put("suspension_days".toLowerCase(), "Integer");
		java_allfield_type_map.put("complain_action_type".toLowerCase(), "Long");
		java_allfield_type_map.put("complian_other".toLowerCase(), "String");


		java_anyfield_search_map.put(tableName + ".disciplinary_log_id".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".employee_records_id".toLowerCase(), "Long");
		java_anyfield_search_map.put(tableName + ".suspension_days".toLowerCase(), "Integer");
		java_anyfield_search_map.put("complain_action.name_en".toLowerCase(), "String");
		java_anyfield_search_map.put("complain_action.name_bn".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".complian_other".toLowerCase(), "String");

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("disciplinaryLogId".toLowerCase(), "disciplinaryLogId".toLowerCase());
		java_DTO_map.put("employeeRecordsId".toLowerCase(), "employeeRecordsId".toLowerCase());
		java_DTO_map.put("employeeNameEng".toLowerCase(), "employeeNameEng".toLowerCase());
		java_DTO_map.put("employeeNameBng".toLowerCase(), "employeeNameBng".toLowerCase());
		java_DTO_map.put("officeNameEng".toLowerCase(), "officeNameEng".toLowerCase());
		java_DTO_map.put("officeNameBng".toLowerCase(), "officeNameBng".toLowerCase());
		java_DTO_map.put("designationNameEng".toLowerCase(), "designationNameEng".toLowerCase());
		java_DTO_map.put("designationNameBng".toLowerCase(), "designationNameBng".toLowerCase());
		java_DTO_map.put("fromDate".toLowerCase(), "fromDate".toLowerCase());
		java_DTO_map.put("toDate".toLowerCase(), "toDate".toLowerCase());
		java_DTO_map.put("suspensionDays".toLowerCase(), "suspensionDays".toLowerCase());
		java_DTO_map.put("complainActionType".toLowerCase(), "complainActionType".toLowerCase());
		java_DTO_map.put("complianOther".toLowerCase(), "complianOther".toLowerCase());
		java_DTO_map.put("remarks".toLowerCase(), "remarks".toLowerCase());
		java_DTO_map.put("insertionDate".toLowerCase(), "insertionDate".toLowerCase());
		java_DTO_map.put("insertedBy".toLowerCase(), "insertedBy".toLowerCase());
		java_DTO_map.put("modifiedBy".toLowerCase(), "modifiedBy".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("disciplinary_log_id".toLowerCase(), "disciplinaryLogId".toLowerCase());
		java_SQL_map.put("employee_records_id".toLowerCase(), "employeeRecordsId".toLowerCase());
		java_SQL_map.put("employee_name_en".toLowerCase(), "employeeNameEng".toLowerCase());
		java_SQL_map.put("employee_name_bn".toLowerCase(), "employeeNameBng".toLowerCase());
		java_SQL_map.put("office_name_en".toLowerCase(), "officeNameEng".toLowerCase());
		java_SQL_map.put("office_name_bn".toLowerCase(), "officeNameBng".toLowerCase());
		java_SQL_map.put("office_name_bn".toLowerCase(), "officeNameBng".toLowerCase());
		java_SQL_map.put("designation_name_en".toLowerCase(), "designationNameEng".toLowerCase());
		java_SQL_map.put("designation_name_bn".toLowerCase(), "designationNameBng".toLowerCase());
		java_SQL_map.put("from_date".toLowerCase(), "fromDate".toLowerCase());
		java_SQL_map.put("to_date".toLowerCase(), "toDate".toLowerCase());
		java_SQL_map.put("suspension_days".toLowerCase(), "suspensionDays".toLowerCase());
		java_SQL_map.put("complain_action_type".toLowerCase(), "complainActionType".toLowerCase());
		java_SQL_map.put("complian_other".toLowerCase(), "complianOther".toLowerCase());
		java_SQL_map.put("remarks".toLowerCase(), "remarks".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Disciplinary Log Id".toLowerCase(), "disciplinaryLogId".toLowerCase());
		java_Text_map.put("Employee Records Id".toLowerCase(), "employeeRecordsId".toLowerCase());
		java_Text_map.put("Suspension Days".toLowerCase(), "suspensionDays".toLowerCase());
		java_Text_map.put("Complain Action".toLowerCase(), "complainActionType".toLowerCase());
		java_Text_map.put("Complian Other".toLowerCase(), "complianOther".toLowerCase());
		java_Text_map.put("Remarks".toLowerCase(), "remarks".toLowerCase());
		java_Text_map.put("Insertion Date".toLowerCase(), "insertionDate".toLowerCase());
		java_Text_map.put("Inserted By".toLowerCase(), "insertedBy".toLowerCase());
		java_Text_map.put("Modified By".toLowerCase(), "modifiedBy".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}