package disciplinary_details;

import common.ConnectionAndStatementUtil;
import complain_action.Complain_actionRepository;
import disciplinary_log.Disciplinary_logDAO;
import disciplinary_log.Disciplinary_logDTO;
import employee_records.EmpOfficeModel;
import employee_records.EmployeeFlatInfoDAO;
import org.apache.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@SuppressWarnings({"unused","Duplicates"})
public class Disciplinary_detailsDAO implements EmployeeFlatInfoDAO<Disciplinary_detailsDTO> {

    private static final Logger logger = Logger.getLogger(Disciplinary_detailsDAO.class);
    private static final String tableName="disciplinary_details";
    private final List<String>extraColumns= Arrays.asList("disciplinary_log_id",
            "complain_action_type","complian_other","from_date","to_date","remarks");
    private static final String getEmployeeDetails="SELECT * FROM disciplinary_details WHERE employee_records_id='%d' AND isDeleted=0";
    @Override
    public String getTableName() {
        return tableName;
    }

    @Override
    public List<String> getExtraColumnListForAddSQLQueryClause() {
        return extraColumns;
    }

    @Override
    public List<String> getExtraColumnListUpdateSQLQueryClause() {
        return extraColumns;
    }

    @Override
    public void setExtraPreparedStatementParams(PreparedStatement ps, Disciplinary_detailsDTO dto, boolean isInsert) throws Exception {
        int index=0;
        ps.setObject(++index,dto.disciplinaryLogId);
        ps.setObject(++index,dto.complainActionType);
        ps.setObject(++index,dto.complianOther);
        ps.setObject(++index,dto.fromDate);
        ps.setObject(++index,dto.toDate);
        ps.setObject(++index,dto.remarks);
    }

    @Override
    public Disciplinary_detailsDTO buildTFromResultSet(ResultSet rs) throws SQLException {
        Disciplinary_detailsDTO dto=new Disciplinary_detailsDTO();
        dto.disciplinaryLogId=rs.getString("disciplinary_log_id");
        dto.complainActionType=rs.getInt("complain_action_type");
        dto.complianOther=rs.getString("complian_other");
        dto.fromDate=rs.getLong("from_date");
        dto.toDate=rs.getLong("to_date");
        dto.remarks=rs.getString("remarks");
        return dto;
    }
    public void insertAfterDisciplinaryLog(String logId, List<EmpOfficeModel> employeeList, String insertedBy, long insertTime) {
        if (employeeList == null || employeeList.size() == 0) {
            return;
        }
        employeeList.forEach(employee -> {
            Disciplinary_detailsDTO dto = new Disciplinary_detailsDTO();
            dto.disciplinaryLogId = logId;
            dto.employeeRecordsId = employee.employeeRecordId;
            dto.officeUnitId=employee.officeUnitId;
            dto.organogramId=employee.organogramId;
            dto.insertionDate = insertTime;
            dto.modifiedBy = insertedBy;
            dto.insertedBy = insertedBy;
            dto.lastModificationTime = insertTime;
            logger.debug(dto);
            try {
                addEmployeeFlatInfoDTO(dto);
            } catch (Exception e) {
                logger.error(e);
                e.printStackTrace();
            }
        });
    }
    public void updateDisciplinaryTableAfterActionEdit(List<Disciplinary_detailsDTO>dtoList,String updatedBy, long updateTime){
        if(dtoList==null || dtoList.size()==0)
            return;
        dtoList.forEach(editedDTO->{
            Disciplinary_detailsDTO oldDTO=getById(editedDTO.iD);
            oldDTO.complainActionType=editedDTO.complainActionType;
            oldDTO.complianOther=editedDTO.complianOther;
            oldDTO.fromDate=editedDTO.fromDate;
            oldDTO.toDate=editedDTO.toDate;
            oldDTO.remarks=editedDTO.remarks;
            oldDTO.modifiedBy=updatedBy;
            oldDTO.lastModificationTime=updateTime;
            try {
                updateEmployeeFlatInfoDTO(oldDTO);
            } catch (Exception e) {
                logger.error(e);
                e.printStackTrace();
            }
        });

    }
    public List<Disciplinary_detailsDTO>getEmployeeDetailsById(long employeeId){
        String searchSql=String.format(getEmployeeDetails,employeeId);
        logger.debug("searchsql: "+searchSql);
        return ConnectionAndStatementUtil.
                getListOfT(searchSql, this::buildTFromRS);
    }
    public DisciplinaryDetailsModel buildDetailsModel(Disciplinary_detailsDTO detailsDTO){
        DisciplinaryDetailsModel model=new DisciplinaryDetailsModel();
        model.complainEngText=Complain_actionRepository.getInstance().getText("ENGLISH",detailsDTO.complainActionType);
        model.complainBangText=Complain_actionRepository.getInstance().getText("BANGLA",detailsDTO.complainActionType);
        model.dto=detailsDTO;
        return model;
    }
    public List<DisciplinaryDetailsModelWithLogModel>getDisciplinaryListModel(long employeeId){
        Disciplinary_logDAO disciplinary_logDAO=Disciplinary_logDAO.getInstance();
        List<Disciplinary_detailsDTO>disciplinaryDetailsDTOS=getEmployeeDetailsById(employeeId);
        List<DisciplinaryDetailsModelWithLogModel>savedDisciplinaryList=new ArrayList<>();
        disciplinaryDetailsDTOS.forEach(detailsDTO -> {
            Disciplinary_logDTO logDTO=disciplinary_logDAO.getDTOByIncidentNumber(detailsDTO.disciplinaryLogId);
            DisciplinaryDetailsModelWithLogModel model=new DisciplinaryDetailsModelWithLogModel();
            model.detailsModel=buildDetailsModel(detailsDTO);
            model.logModel=disciplinary_logDAO.buildDisciplinaryLogModel(logDTO);
            if(logDTO != null)
                savedDisciplinaryList.add(model);
        });
        return savedDisciplinaryList;
    }
}
