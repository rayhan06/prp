package disciplinary_details;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Logger;

public class DetailsDTOModel {
    public String iD="";
    public String employeeRecordsId="";
    public String officeUnitId="";
    public String organogramId="";
    public String complainActionType="";
    public String complianOther="";
    public String fromDate="";
    public String toDate="";
    public String remarks="";

    public Disciplinary_detailsDTO buildJson() throws ParseException {
        SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
        Disciplinary_detailsDTO detailsDTOJson=new Disciplinary_detailsDTO();
        if(!(iD==null || iD.length()==0)){
            detailsDTOJson.iD=Long.parseLong(iD);
        }
        if(!(employeeRecordsId==null || employeeRecordsId.length()==0)){
            detailsDTOJson.employeeRecordsId=Long.parseLong(employeeRecordsId);
        }
        if(!(officeUnitId==null || officeUnitId.length()==0)){
            detailsDTOJson.officeUnitId=Long.parseLong(officeUnitId);
        }
        if(!(organogramId==null || organogramId.length()==0)){
            detailsDTOJson.organogramId=Long.parseLong(organogramId);
        }
        if(!(complainActionType==null || complainActionType.length()==0)){
            detailsDTOJson.complainActionType=Long.parseLong(complainActionType);
        }
        detailsDTOJson.complianOther=complianOther;
        detailsDTOJson.remarks=remarks;
        if(!(fromDate==null || fromDate.length()==0)) {
            Date d = f.parse(fromDate);
            detailsDTOJson.fromDate = d.getTime();
        }
        if(!(toDate==null || toDate.length()==0)) {
            Date d = f.parse(toDate);
            detailsDTOJson.toDate = d.getTime();
        }
        return detailsDTOJson;
    }
}
