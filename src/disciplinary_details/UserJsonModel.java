package disciplinary_details;

import java.util.Objects;

public class UserJsonModel {
    public long employeeRecordId;
    public long officeUnitId;
    public long organogramId;

    public UserJsonModel() {
    }

    @Override
    public String toString() {
        return "UserJsonModel{" +
                "employeeRecordId=" + employeeRecordId +
                ", officeUnitId=" + officeUnitId +
                ", organogramId=" + organogramId +
                '}';
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UserJsonModel)) return false;
        UserJsonModel that = (UserJsonModel) o;
        return employeeRecordId == that.employeeRecordId &&
                officeUnitId == that.officeUnitId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(employeeRecordId, officeUnitId);
    }
}
