package food_bill_submission_config;

import common.BaseServlet;
import employee_records.Employee_recordsDTO;
import employee_records.Employee_recordsRepository;
import food_bill.Food_billDAO;
import login.LoginDTO;
import office_unit_organograms.OfficeUnitOrganograms;
import office_unit_organograms.OfficeUnitOrganogramsRepository;
import org.apache.log4j.Logger;
import overtime_bill.BillStatusResponse;
import pb.Utils;
import pbReport.DateUtils;
import permission.MenuConstants;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;
import util.HttpRequestUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@SuppressWarnings({"Duplicates"})
@WebServlet("/Food_bill_submission_configServlet")
@MultipartConfig
public class Food_bill_submission_configServlet extends BaseServlet {
    private final Logger logger = Logger.getLogger(Food_bill_submission_configServlet.class);

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }

        String actionType = request.getParameter("actionType");
        try {
            switch (actionType) {
                case "getAddPage":
                case "getEditPage":
                case "search":
                    super.doGet(request, response);
                    return;
                case "ajax_getActiveBillStatusForEmployeeDashboard":
                    List<BillStatusResponse> billStatusResponses = Collections.emptyList();
                    try {
                        billStatusResponses = getActiveBillStatusForEmployeeDashboard(userDTO);
                    } catch (Exception ex) {
                        logger.error(ex.getMessage());
                    }
                    request.setAttribute("billStatusResponses", billStatusResponses);
                    request.getRequestDispatcher("food_bill_submission_config/food_bill_submission_configStatusForEmployeeDashboard.jsp")
                           .forward(request, response);
                    return;
            }
        } catch (Exception ex) {
            logger.error(ex);
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }

    private List<BillStatusResponse> getActiveBillStatusForEmployeeDashboard(UserDTO userDTO) {
        OfficeUnitOrganograms organogram = OfficeUnitOrganogramsRepository.getInstance().getById(userDTO.organogramID);
        if (organogram == null) {
            return Collections.emptyList();
        }
        Employee_recordsDTO employeeRecord = Employee_recordsRepository.getInstance().getById(userDTO.employee_record_id);
        if (employeeRecord == null) {
            return Collections.emptyList();
        }
        long employeeRecordId = employeeRecord.iD, officeUnitsId = organogram.office_unit_id;
        boolean isLangEn = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
        List<Food_bill_submission_configDTO> activeDTOs =
                Food_bill_submission_configDAO.getInstance()
                                              .findActiveDTOsByTime(System.currentTimeMillis());
        activeDTOs.sort(Food_bill_submission_configDTO.byParliamentThenSession);

        Food_billDAO overtimeBillDAO = Food_billDAO.getInstance();
        List<BillStatusResponse> response = new ArrayList<>();
        for (Food_bill_submission_configDTO activeDTO : activeDTOs) {
            BillStatusResponse billStatus = overtimeBillDAO.getStatusForEmployeeDashboard(activeDTO.iD, officeUnitsId, employeeRecordId, isLangEn);
            billStatus.billDescription = activeDTO.getText(isLangEn);
            response.add(billStatus);
        }
        return response;
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }

        String actionType = request.getParameter("actionType");
        try {
            switch (actionType) {
                case "ajax_add":
                case "ajax_edit":
                case "delete":
                    super.doPost(request, response);
                    return;
            }
        } catch (Exception ex) {
            logger.error(ex);
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        boolean isLangEn = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
        long currentTime = System.currentTimeMillis();


        Food_bill_submission_configDTO submissionConfigDTO;
        if (addFlag) {
            submissionConfigDTO = new Food_bill_submission_configDTO();
            submissionConfigDTO.insertionTime = currentTime;
            submissionConfigDTO.insertedBy = userDTO.ID;
        } else {
            Long id = Utils.parseMandatoryLong(
                    request.getParameter("iD"),
                    isLangEn ? "No record with found to edit" : "এডিট করার মত তথ্য পাওয়া যায়নি"
            );
            submissionConfigDTO = Food_bill_submission_configDAO.getInstance().getDTOFromID(id);
        }
        submissionConfigDTO.lastModificationTime = currentTime;
        submissionConfigDTO.modifiedBy = userDTO.ID;

        submissionConfigDTO.electionDetailsId = Utils.parseMandatoryLong(
                request.getParameter("electionDetailsId"),
                isLangEn ? "Select Parliament Number" : "সংসদ নম্বর বাছাই করুন"
        );
        submissionConfigDTO.setParliamentNumber(isLangEn);
        submissionConfigDTO.parliamentSessionId = Utils.parseMandatoryLong(
                request.getParameter("parliamentSessionId"),
                isLangEn ? "Select Parliament Session" : "সংসদ অধিবেশন বাছাই করুন"
        );
        submissionConfigDTO.setSessionNumber(isLangEn);

        submissionConfigDTO.startDate = Utils.parseMandatoryDate(
                request.getParameter("startDate"),
                isLangEn ? "Select Valid Start Date" : "সঠিক শুরুর তারিখ বাছাই করুন"
        );

        submissionConfigDTO.endDate = Utils.parseMandatoryDate(
                request.getParameter("endDate"),
                isLangEn ? "Select Valid End Date" : "সঠিক শেষের তারিখ বাছাই করুন"
        );
        final long ONE_DAY_IN_MILLIS = (24L * 60) * 60 * 1000;
        submissionConfigDTO.endDate += (ONE_DAY_IN_MILLIS - 1);
        if (submissionConfigDTO.startDate > submissionConfigDTO.endDate) {
            throw new IllegalArgumentException(
                    isLangEn
                    ? "Submission Start Date Must be before submission end date"
                    : "জমা শুরুর তারিখ জমা শেষের তারিখের আগে হতে হবে"
            );
        }
        submissionConfigDTO.foodBillTypeCat = Utils.parseMandatoryInt(
                request.getParameter("foodBillTypeCat"),
                1,
                isLangEn ? "Select Bill Type" : "বিলের ধরণ বাছাই করুন"
        );
        Food_bill_submission_configDTO byParliamentInfo =
                Food_bill_submission_configDAO.getInstance().findByParliamentInfo(
                        submissionConfigDTO.electionDetailsId,
                        submissionConfigDTO.parliamentSessionId,
                        submissionConfigDTO.foodBillTypeCat
                );
        IllegalArgumentException alreadyExistsException = new IllegalArgumentException(
                isLangEn
                ? "This Parliament Session's submission info is already added"
                : "এই সংসদ অশিবেশনের জমা দেয়ার তারিখ এরই মধ্যে যোগ করা হয়েছে"
        );
        submissionConfigDTO.lastActiveTime = DateUtils.get1stDayOfNextMonth(submissionConfigDTO.endDate) - 1;

        submissionConfigDTO.allowedOfficeIds = null;
        boolean allOfficesNotChecked = request.getParameter("allOfficesCheckbox") == null;
        if (allOfficesNotChecked) {
            submissionConfigDTO.allowedOfficeIds = new HashSet<>();
            String[] allowedOfficeIds = request.getParameterValues("allowedOfficeIds");
            if (allowedOfficeIds != null) {
                submissionConfigDTO.allowedOfficeIds =
                        Arrays.stream(allowedOfficeIds)
                              .map(Long::parseLong)
                              .collect(Collectors.toSet());
            }
        }

        if (addFlag) {
            if (byParliamentInfo != null) {
                throw alreadyExistsException;
            }
            Food_bill_submission_configDAO.getInstance().add(submissionConfigDTO);
        } else {
            if (byParliamentInfo != null && byParliamentInfo.iD != submissionConfigDTO.iD) {
                throw alreadyExistsException;
            }
            Food_bill_submission_configDAO.getInstance().update(submissionConfigDTO);
        }
        return submissionConfigDTO;
    }

    @Override
    public String getServletName() {
        return "Food_bill_submission_configServlet";
    }

    @Override
    public Food_bill_submission_configDAO getCommonDAOService() {
        return Food_bill_submission_configDAO.getInstance();
    }

    @Override
    public String getTableName() {
        return getCommonDAOService().getTableName();
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[]{MenuConstants.FOOD_BILL_SUBMISSION_CONFIG};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[]{MenuConstants.FOOD_BILL_SUBMISSION_CONFIG};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[]{MenuConstants.FOOD_BILL_ADMIN_TASK};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Food_bill_submission_configServlet.class;
    }
}
