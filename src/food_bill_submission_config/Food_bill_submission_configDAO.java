package food_bill_submission_config;

import common.CommonDAOService;
import common.ConnectionAndStatementUtil;
import org.apache.log4j.Logger;
import pb.OptionDTO;
import pb.Utils;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@SuppressWarnings({"Duplicates"})
public class Food_bill_submission_configDAO implements CommonDAOService<Food_bill_submission_configDTO> {
    private static final Logger logger = Logger.getLogger(Food_bill_submission_configDAO.class);

    private static final String addSqlQuery =
            "INSERT INTO {tableName} (election_details_id,parliament_number,parliament_session_id,session_number,start_date,"
                    .concat("end_date,last_active_time,allowed_office_ids,food_bill_type_cat,modified_by,lastModificationTime,inserted_by,insertion_time,isDeleted,ID) ")
                    .concat("VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

    private static final String updateSqlQuery =
            "UPDATE {tableName} SET election_details_id=?,parliament_number=?,parliament_session_id=?,session_number=?,"
                    .concat("start_date=?,end_date=?,last_active_time=?,allowed_office_ids=?,food_bill_type_cat=?,modified_by=?,lastModificationTime=? WHERE ID=?");

    private static final Map<String, String> searchMap = new HashMap<>();

    private Food_bill_submission_configDAO() {
        searchMap.put("electionDetailsId", " AND (election_details_id = ?) ");
        searchMap.put("parliamentSessionId", " AND (parliament_session_id = ?) ");
        searchMap.put("foodBillTypeCat", " AND (food_bill_type_cat = ?) ");
    }

    private static class LazyLoader {
        static final Food_bill_submission_configDAO INSTANCE = new Food_bill_submission_configDAO();
    }

    public static Food_bill_submission_configDAO getInstance() {
        return LazyLoader.INSTANCE;
    }

    @Override
    public String getTableName() {
        return "food_bill_submission_config";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    @Override
    public void set(PreparedStatement ps, Food_bill_submission_configDTO dto, boolean isInsert) throws SQLException {
        int index = 0;
        ps.setLong(++index, dto.electionDetailsId);
        ps.setInt(++index, dto.parliamentNumber);
        ps.setLong(++index, dto.parliamentSessionId);
        ps.setInt(++index, dto.sessionNumber);
        ps.setLong(++index, dto.startDate);
        ps.setLong(++index, dto.endDate);
        ps.setLong(++index, dto.lastActiveTime);
        ps.setString(++index, dto.getAllowedOfficeIdsStr());
        ps.setInt(++index, dto.foodBillTypeCat);
        ps.setLong(++index, dto.modifiedBy);
        ps.setLong(++index, dto.lastModificationTime);
        if (isInsert) {
            ps.setLong(++index, dto.insertedBy);
            ps.setLong(++index, dto.insertionTime);
            ps.setInt(++index, 0 /* isDeleted */);
        }
        ps.setLong(++index, dto.iD);
    }

    @Override
    public Food_bill_submission_configDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Food_bill_submission_configDTO dto = new Food_bill_submission_configDTO();
            dto.iD = rs.getLong("ID");
            dto.electionDetailsId = rs.getLong("election_details_id");
            dto.parliamentNumber = rs.getInt("parliament_number");
            dto.parliamentSessionId = rs.getLong("parliament_session_id");
            dto.sessionNumber = rs.getInt("session_number");
            dto.startDate = rs.getLong("start_date");
            dto.endDate = rs.getLong("end_date");
            dto.lastActiveTime = rs.getLong("last_active_time");
            dto.setAllowedOfficeIds(rs.getString("allowed_office_ids"));
            dto.foodBillTypeCat = rs.getInt("food_bill_type_cat");
            dto.modifiedBy = rs.getLong("modified_by");
            dto.lastModificationTime = rs.getLong("lastModificationTime");
            dto.insertedBy = rs.getLong("inserted_by");
            dto.insertionTime = rs.getLong("insertion_time");
            dto.isDeleted = rs.getInt("isDeleted");
            return dto;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    @Override
    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Food_bill_submission_configDTO) commonDTO, addSqlQuery, true);
    }

    @Override
    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Food_bill_submission_configDTO) commonDTO, updateSqlQuery, false);
    }

    private static final String findByParliamentInfoSql =
            "SELECT * FROM food_bill_submission_config WHERE isDeleted = 0 AND election_details_id = %d "
                    .concat("AND parliament_session_id = %d AND food_bill_type_cat = %d ");

    public Food_bill_submission_configDTO findByParliamentInfo(long electionDetailsId, long parliamentSessionId, int foodBillTypeCat) {
        return ConnectionAndStatementUtil.getT(
                String.format(findByParliamentInfoSql, electionDetailsId, parliamentSessionId, foodBillTypeCat),
                this::buildObjectFromResultSet
        );
    }

    private static final String findSubmittableDTOsByTimeSql =
            "SELECT * FROM food_bill_submission_config WHERE isDeleted = 0 AND (start_date <= %d AND %d <= end_date)";

    public List<Food_bill_submission_configDTO> findSubmittableDTOsByTime(long timeStamp) {
        return ConnectionAndStatementUtil.getListOfT(
                String.format(findSubmittableDTOsByTimeSql, timeStamp, timeStamp),
                this::buildObjectFromResultSet
        );
    }

    private static final String findActiveDTOsByTimeSql =
            "SELECT * FROM food_bill_submission_config WHERE isDeleted = 0 AND (start_date <= %d AND %d <= last_active_time)";

    public List<Food_bill_submission_configDTO> findActiveDTOsByTime(long timeStamp) {
        return ConnectionAndStatementUtil.getListOfT(
                String.format(findActiveDTOsByTimeSql, timeStamp, timeStamp),
                this::buildObjectFromResultSet
        );
    }

    public String buildOptionsOfSubmittableDTOs(Long officeUnitId, long timeStamp, String language) {
        List<OptionDTO> optionDTOList =
                findSubmittableDTOsByTime(timeStamp)
                        .stream()
                        .filter(dto -> dto.isOfficeUnitIdAllowed(officeUnitId))
                        .sorted(Food_bill_submission_configDTO.byParliamentThenSession)
                        .map(Food_bill_submission_configDTO::getOptionDTO)
                        .collect(Collectors.toList());
        return Utils.buildOptions(optionDTOList, language, null);
    }

    public String getTextById(long id, boolean isLangEn) {
        Food_bill_submission_configDTO dtoFromID = getDTOFromID(id);
        return dtoFromID == null ? "NULL" : dtoFromID.getText(isLangEn);
    }

    public String buildOptions(String language, Long selectedId) {
        List<OptionDTO> optionDTOList =
                getAllDTOs().stream()
                            .sorted(Food_bill_submission_configDTO.byParliamentInfoThenDates.reversed())
                            .map(Food_bill_submission_configDTO::getOptionDTO)
                            .collect(Collectors.toList());
        return Utils.buildOptions(optionDTOList, language, selectedId == null ? null : String.format("%d", selectedId));
    }
}
