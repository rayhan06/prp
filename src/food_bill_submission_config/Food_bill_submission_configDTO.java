package food_bill_submission_config;

import election_details.Election_detailsDTO;
import election_details.Election_detailsRepository;
import org.apache.log4j.Logger;
import parliament_session.Parliament_sessionDTO;
import parliament_session.Parliament_sessionRepository;
import pb.CatRepository;
import pb.OptionDTO;
import sessionmanager.SessionConstants;
import util.CommonDTO;
import util.StringUtils;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@SuppressWarnings({"Duplicates"})
public class Food_bill_submission_configDTO extends CommonDTO {
    private static final Logger logger = Logger.getLogger(Food_bill_submission_configDTO.class);

    public long electionDetailsId = -1L;
    public int parliamentNumber = -1;
    public long parliamentSessionId = -1L;
    public int sessionNumber = -1;
    public long startDate = SessionConstants.MIN_DATE;
    public long endDate = SessionConstants.MIN_DATE;
    public long lastActiveTime = SessionConstants.MIN_DATE;
    // food bill active from [startDate, lastActiveTime]
    // this is used to get which bill is active at a timeframe
    public Set<Long> allowedOfficeIds;

    public int foodBillTypeCat = 1;

    public long modifiedBy = -1;
    public long insertedBy = -1;
    public long insertionTime = SessionConstants.MIN_DATE;

    public static Comparator<Food_bill_submission_configDTO>
            byParliamentThenSession =
            Comparator.comparingInt((Food_bill_submission_configDTO dto) -> dto.parliamentNumber)
                      .thenComparingInt(dto -> dto.sessionNumber);

    public static Comparator<Food_bill_submission_configDTO> byParliamentInfoThenDates =
            byParliamentThenSession.thenComparingLong(dto -> dto.startDate)
                                   .thenComparingLong(dto -> dto.endDate);

    public String getText(boolean isLangEn) {
        if (!isLangEn) {
            return String.format(
                    "%s-তম সংসদ | %s-তম অধিবেশন (%s)",
                    StringUtils.convertToBanNumber(String.format("%d", parliamentNumber)),
                    StringUtils.convertToBanNumber(String.format("%d", sessionNumber)),
                    CatRepository.getInstance().getText(false, SessionConstants.FOOD_BILL_TYPE_CAT_DOMAIN_NAME, foodBillTypeCat)
            );
        }
        return String.format(
                "%d-th Parliament | %d-th Session (%s)",
                parliamentNumber,
                sessionNumber,
                CatRepository.getInstance().getText(true, SessionConstants.FOOD_BILL_TYPE_CAT_DOMAIN_NAME, foodBillTypeCat)
        );
    }

    public OptionDTO getOptionDTO() {
        return new OptionDTO(getText(true), getText(false), String.format("%d", iD));
    }


    public void setParliamentNumber(boolean isLangEn) {
        Election_detailsDTO electionDetailsDTO = Election_detailsRepository.getInstance().getElectionDetailsDTOByID(electionDetailsId);
        if (electionDetailsDTO == null) {
            throw new IllegalArgumentException(
                    isLangEn ? "Parliament Number Not Found"
                             : "সংসদ নম্বর পাওয়া যায়নি"
            );
        }
        parliamentNumber = electionDetailsDTO.parliamentNumber;
    }

    public void setSessionNumber(boolean isLangEn) {
        Parliament_sessionDTO parliamentSessionDTO = Parliament_sessionRepository.getInstance().getParliament_sessionDTOByID(parliamentSessionId);
        if (parliamentSessionDTO == null) {
            throw new IllegalArgumentException(
                    isLangEn ? "Parliament Session Number Not Found"
                             : "সংসদ অদিবেশন নম্বর পাওয়া যায়নি"
            );
        }
        sessionNumber = parliamentSessionDTO.sessionNumber;
    }

    public boolean isAllowedToSubmitByTime(long time) {
        boolean isStartEndDateInvalid = (startDate == SessionConstants.MIN_DATE)
                                        || (endDate == SessionConstants.MIN_DATE)
                                        || (startDate > endDate);
        if (isStartEndDateInvalid) {
            logger.error("INVALID startDate or endDate for " + this);
            return false;
        }
        return startDate <= time && time <= endDate;
    }

    public String getAllowedOfficeIdsStr() {
        if (allowedOfficeIds == null) {
            return null;
        }
        return allowedOfficeIds.stream()
                               .map(Objects::toString)
                               .collect(Collectors.joining(","));
    }

    public void setAllowedOfficeIds(String allowedOfficeIdsStr) {
        if (allowedOfficeIdsStr == null) {
            allowedOfficeIds = null;
            return;
        }
        allowedOfficeIds = Arrays.stream(allowedOfficeIdsStr.split(","))
                                 .filter(s -> !s.isEmpty())
                                 .map(Long::parseLong)
                                 .collect(Collectors.toSet());
    }

    public boolean isOfficeUnitIdAllowed(Long officeUnitId) {
        return officeUnitId == null || allowedOfficeIds == null || allowedOfficeIds.contains(officeUnitId);
    }
}