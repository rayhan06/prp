create table food_bill_submission_config
(
    ID                    bigint primary key,
    election_details_id   bigint,
    parliament_number     int,
    parliament_session_id bigint,
    session_number        int,
    start_date            bigint,
    end_date              bigint,
    modified_by           bigint,
    lastModificationTime  bigint default -62135791200000,
    inserted_by           bigint,
    insertion_time        bigint default -62135791200000,
    isDeleted             int    default 0
) engine = MyISAM;

insert into vb_sequencer (table_name, next_id, table_LastModificationTime) value ('food_bill_submission_config', 1, 0);


