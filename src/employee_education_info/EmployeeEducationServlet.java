package employee_education_info;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import common.ApiResponse;

@WebServlet("/employee_education_Servlet")
public class EmployeeEducationServlet extends HttpServlet{
	private static final long serialVersionUID = -6541291713378918868L;

	private static final Logger logger = Logger.getLogger(EmployeeEducationServlet.class);

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ApiResponse api_response;
		String actionType = request.getParameter("a");
		logger.debug("actionType = " + actionType);
		EmployeeEducationApiPerformer performer = EmployeeEducationApiPerformerFactory.getPerformer(actionType);
		if(performer == null) {
			api_response = ApiResponse.makeErrorResponse("Action type is invalid");
		}else {
			api_response = performer.perform(request);
		}
		response.getWriter().write(api_response.getJSONString());
		response.getWriter().flush();
		response.getWriter().close();
	}

}
