package employee_education_info;

public class EmployeeEducationApiPerformerFactory {
	private static final String GET_DEGREE_REQUEST = "getDegrees";

	public static EmployeeEducationApiPerformer getPerformer(String actionType) {
		if (actionType != null) {
			switch (actionType) {
			case GET_DEGREE_REQUEST:
				return new GetDegreePerformer();
			}
		}
		return null;
	}
}
