package employee_education_info;

import javax.servlet.http.HttpServletRequest;

import common.ApiResponse;

public interface EmployeeEducationApiPerformer {
	ApiResponse perform(HttpServletRequest request);
}
