package employee_education_info;

import common.ApiResponse;
import education_level.DegreeExamDTO;
import education_level.DegreeExamRepository;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class GetDegreePerformer implements EmployeeEducationApiPerformer{

	Logger logger = Logger.getLogger(GetDegreePerformer.class);
	@Override
	public ApiResponse perform(HttpServletRequest request) {
		long educationLevelId = Long.parseLong(request.getParameter("education_level_id"));
		ApiResponse apiResponse;
		try {
			List<DegreeExamDTO> list = DegreeExamRepository.getInstance().getByEducationLevelId(educationLevelId);
			list.sort(Comparator.comparingLong(f -> f.iD));
			DegreeExamDTO selectDegreeExamDTO = new DegreeExamDTO();
			selectDegreeExamDTO.nameEn = "Select";
			selectDegreeExamDTO.nameBn = "বাছাই করুন";
			list.add(0,selectDegreeExamDTO);
			logger.debug(list);
			apiResponse = ApiResponse.makeSuccessResponse(list, "List is loaded successfully");
		} catch (Exception e) {
			logger.error(e);
			apiResponse = ApiResponse.makeErrorResponse("Exception is occurred");
		}
		return apiResponse;
	}
}
