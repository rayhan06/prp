package employee_education_info;

public class EmployeeEducationInfoModel {
	public String educationLevelNameEn;
	public String educationLevelNameBn;
	public String degreeExamNameEn;
	public String degreeExamNameBn;
	public String otherDegreeExam;
	public String resultExamNameEn;
	public String resultExamNameBn;
	public String marksPercentage;
	public String major;
	public Integer gradePointCat;
	public Double gradePoint;
	public Integer yearOfPassingNumber;
	public String institutionName;
	public String boardNameEn;
	public String boardNameBn;
	@Override
	public String toString() {
		return "EmployeeEducationInfoModel [educationLevelNameEn=" + educationLevelNameEn + ", educationLevelNameBn="
				+ educationLevelNameBn + ", degreeExamNameEn=" + degreeExamNameEn + ", degreeExamNameBn="
				+ degreeExamNameBn + ", otherDegreeExam=" + otherDegreeExam + ", resultExamNameEn=" + resultExamNameEn
				+ ", resultExamNameBn=" + resultExamNameBn + ", marksPercentage=" + marksPercentage + ", major=" + major
				+ ", gradePointCat=" + gradePointCat + ", gradePoint=" + gradePoint + ", yearOfPassingNumber="
				+ yearOfPassingNumber + ", institutionName=" + institutionName + ", boardNameEn=" + boardNameEn
				+ ", boardNameBn=" + boardNameBn + "]";
	}
}
