package employee_education_info;

import board.BoardRepository;
import common.ConnectionAndStatementUtil;
import common.EmployeeCommonDAOService;
import common.NameDTO;
import education_level.DegreeExamDTO;
import education_level.DegreeExamRepository;
import education_level.Education_levelRepository;
import employee_records.Employee_recordsDAO;
import employee_records.Employee_recordsDTO;
import org.apache.log4j.Logger;
import pb.CatRepository;
import pb.CategoryLanguageModel;
import result_exam.Result_examRepository;
import util.CommonDTO;
import util.StringUtils;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@SuppressWarnings({"unused","Duplicates"})
public class  Employee_education_infoDAO implements EmployeeCommonDAOService<Employee_education_infoDTO> {

    private static final Logger logger = Logger.getLogger(Employee_education_infoDAO.class);

    private static final String getByEmpIdSortByEduLevel = "SELECT * FROM employee_education_info WHERE employee_records_id = %d AND isDeleted = 0 ORDER BY education_level_type desc";

    private static final String addSqlQuery = "INSERT INTO {tableName} (employee_records_id,education_level_type,degree_exam_type,other_degree_exam,major,"
            .concat("result_exam_type,grade_point_cat,cgpa_number,year_of_passing_number,board_type,institution_name,university_info_type,")
            .concat("is_foreign,duration_years,achievement,files_dropzone,modified_by,search_column,lastModificationTime,insertion_date,inserted_by,isDeleted,ID)")
            .concat(" VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

    private static final String updateSqlQuery = "UPDATE {tableName} SET employee_records_id=?,education_level_type=?,degree_exam_type=?,other_degree_exam=?,major=?,result_exam_type=?,"
            .concat("grade_point_cat=?,cgpa_number=?,year_of_passing_number=?,board_type=?,institution_name=?,university_info_type=?,is_foreign=?,duration_years=?,")
            .concat("achievement=?,files_dropzone=?,modified_by=?,search_column=?,lastModificationTime = ? WHERE ID = ?");

    private static final String educationInfoQslQuery = "SELECT EL.name_bn as education_level_name_bn,EL.name_en as education_level_name_en,"
            .concat(" DE.name_bn as degree_exam_name_bn,DE.name_en as degree_exam_name_en,ERI.other_degree_exam,")
            .concat(" RE.name_en as result_exam_name_en,RE.name_bn as result_exam_name_bn,")
            .concat(" ERI.major,ERI.grade_point_cat,ERI.cgpa_number,ERI.year_of_passing_number,")
            .concat(" ERI.institution_name,BR.name_en as board_name_en,BR.name_bn as board_name_bn").concat(" FROM (")
            .concat(" SELECT * FROM employee_education_info where employee_records_id = ? and isDeleted = 0) ERI")
            .concat(" INNER join education_level EL ON ERI.education_level_type = EL.ID")
            .concat(" INNER JOIN result_exam RE ON RE.ID = ERI.result_exam_type")
            .concat(" INNER JOIN degree_exam DE ON DE.education_level_id = ERI.education_level_type")
            .concat(" INNER JOIN board BR ON BR.ID = ERI.board_type and DE.ID = ERI.degree_exam_type")
            .concat(" order by ERI.education_level_type,ERI.duration_years DESC");

    private static final String getByEmployeeRecordIdAndMaxPassingYear = "select eei.* from employee_education_info eei" +
            " inner join ( select employee_records_id,max(year_of_passing_number) passing_year from" +
            " (select * from employee_education_info where  employee_records_id in (%s))" +
            "  A group by employee_records_id ) B on eei.employee_records_id = B.employee_records_id AND eei.year_of_passing_number = passing_year;";
    private final Map<String, String> searchMap = new HashMap<>();

    private Employee_education_infoDAO() {
        searchMap.put("education_level_type", " and (employee_education_info.education_level_type = ?)");
        searchMap.put("degree_exam_type", " and (employee_education_info.degree_exam_type = ?)");
        searchMap.put("result_exam_type", " and (employee_education_info.result_exam_type = ?)");
        searchMap.put("board_type", " and (employee_education_info.board_type = ?)");
        searchMap.put("employee_records_id_internal", " and (employee_education_info.employee_records_id = ?)");
        searchMap.put("AnyField", " and (employee_education_info.search_column like ?)");
    }

    private static class LazyLoader{
        static final Employee_education_infoDAO INSTANCE = new Employee_education_infoDAO();
    }

    public static Employee_education_infoDAO getInstance(){
        return LazyLoader.INSTANCE;
    }

    @Override
    public void set(PreparedStatement ps, Employee_education_infoDTO employee_education_infoDTO, boolean isInsert)
            throws SQLException {
        int index = 0;
        setSearchColumn(employee_education_infoDTO);
        ps.setObject(++index, employee_education_infoDTO.employeeRecordsId);
        ps.setObject(++index, employee_education_infoDTO.educationLevelType);
        ps.setObject(++index, employee_education_infoDTO.degreeExamType);
        ps.setObject(++index, employee_education_infoDTO.otherDegreeExam);
        ps.setObject(++index, employee_education_infoDTO.major);
        ps.setObject(++index, employee_education_infoDTO.resultExamType);
        ps.setObject(++index, employee_education_infoDTO.gradePointCat);
        ps.setObject(++index, employee_education_infoDTO.cgpaNumber);
        ps.setObject(++index, employee_education_infoDTO.yearOfPassingNumber);
        ps.setObject(++index, employee_education_infoDTO.boardType);
        ps.setObject(++index, employee_education_infoDTO.institutionName);
        ps.setObject(++index, employee_education_infoDTO.universityInfoType);
        ps.setObject(++index, employee_education_infoDTO.isForeign);
        ps.setObject(++index, employee_education_infoDTO.durationYears);
        ps.setObject(++index, employee_education_infoDTO.achievement);
        ps.setObject(++index, employee_education_infoDTO.filesDropzone);
        ps.setObject(++index, employee_education_infoDTO.modifiedBy);
        ps.setObject(++index, employee_education_infoDTO.searchColumn);
        ps.setObject(++index, employee_education_infoDTO.lastModificationTime);
        if (isInsert) {
            ps.setObject(++index, employee_education_infoDTO.insertionDate);
            ps.setObject(++index, employee_education_infoDTO.insertedBy);
            ps.setObject(++index, 0);
        }
        ps.setObject(++index, employee_education_infoDTO.iD);
    }

    public void setSearchColumn(Employee_education_infoDTO employee_education_infoDTO) {
        StringBuilder searchColumnBuilder = new StringBuilder();
        try {
            Employee_recordsDTO employeeRecordsDTO = new Employee_recordsDAO().getEmployee_recordsDTOByID(employee_education_infoDTO.employeeRecordsId);
            if (employeeRecordsDTO != null) {
                if (employeeRecordsDTO.nameEng != null && employeeRecordsDTO.nameEng.trim().length() > 0) {
                    searchColumnBuilder.append(employeeRecordsDTO.nameEng.trim()).append(" ");
                }
                if (employeeRecordsDTO.nameBng != null && employeeRecordsDTO.nameBng.trim().length() > 0) {
                    searchColumnBuilder.append(employeeRecordsDTO.nameBng.trim()).append(" ");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        NameDTO nameDTO = Education_levelRepository.getInstance().getDTOByID(employee_education_infoDTO.educationLevelType);
        if (nameDTO != null) {
            searchColumnBuilder.append(nameDTO.nameEn).append(" ");
            searchColumnBuilder.append(nameDTO.nameBn).append(" ");
        }

        DegreeExamDTO degreeExamDTO = DegreeExamRepository.getInstance().getByIdAndEducationLevelId(employee_education_infoDTO.degreeExamType, employee_education_infoDTO.educationLevelType);
        if (degreeExamDTO != null) {
            searchColumnBuilder.append(degreeExamDTO.nameEn).append(" ");
            searchColumnBuilder.append(degreeExamDTO.nameBn).append(" ");
        }

        NameDTO resultExamDTO = Result_examRepository.getInstance().getDTOByID(employee_education_infoDTO.resultExamType);
        if (resultExamDTO != null) {
            searchColumnBuilder.append(resultExamDTO.nameEn).append(" ");
            searchColumnBuilder.append(resultExamDTO.nameBn).append(" ");
        }

        NameDTO boardDTO = BoardRepository.getInstance().getDTOByID(employee_education_infoDTO.boardType);
        if (boardDTO != null) {
            searchColumnBuilder.append(boardDTO.nameEn).append(" ");
            searchColumnBuilder.append(boardDTO.nameBn).append(" ");
        }

        employee_education_infoDTO.searchColumn = String.join(" ", searchColumnBuilder.toString());
    }

    @Override
    public Employee_education_infoDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Employee_education_infoDTO employee_education_infoDTO = new Employee_education_infoDTO();
            employee_education_infoDTO.iD = rs.getLong("ID");
            employee_education_infoDTO.employeeRecordsId = rs.getLong("employee_records_id");
            employee_education_infoDTO.educationLevelType = rs.getLong("education_level_type");
            employee_education_infoDTO.degreeExamType = rs.getLong("degree_exam_type");
            employee_education_infoDTO.otherDegreeExam = rs.getString("other_degree_exam");
            employee_education_infoDTO.major = rs.getString("major");
            employee_education_infoDTO.resultExamType = rs.getLong("result_exam_type");
            employee_education_infoDTO.gradePointCat = rs.getInt("grade_point_cat");
            employee_education_infoDTO.cgpaNumber = rs.getDouble("cgpa_number");
            employee_education_infoDTO.yearOfPassingNumber = rs.getInt("year_of_passing_number");
            employee_education_infoDTO.boardType = rs.getLong("board_type");
            employee_education_infoDTO.institutionName = rs.getString("institution_name");
            employee_education_infoDTO.universityInfoType = rs.getLong("university_info_type");
            employee_education_infoDTO.isForeign = rs.getBoolean("is_foreign");
            employee_education_infoDTO.durationYears = rs.getInt("duration_years");
            employee_education_infoDTO.achievement = rs.getString("achievement");
            employee_education_infoDTO.filesDropzone = rs.getLong("files_dropzone");
            employee_education_infoDTO.insertionDate = rs.getLong("insertion_date");
            employee_education_infoDTO.insertedBy = rs.getString("inserted_by");
            employee_education_infoDTO.modifiedBy = rs.getString("modified_by");
            employee_education_infoDTO.searchColumn = rs.getString("search_column");
            employee_education_infoDTO.isDeleted = rs.getInt("isDeleted");
            employee_education_infoDTO.lastModificationTime = rs.getLong("lastModificationTime");
            return employee_education_infoDTO;
        } catch (SQLException ex) {
            logger.error(ex);
            ex.printStackTrace();
            return null;
        }
    }

    @Override
    public String getTableName() {
        return "employee_education_info";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    private EmployeeEducationInfoModel buildEmployeeEducationInfoModel(ResultSet rs) {
        try {
            EmployeeEducationInfoModel model = new EmployeeEducationInfoModel();
            model.educationLevelNameEn = rs.getString("education_level_name_en");
            model.educationLevelNameBn = rs.getString("education_level_name_bn");
            model.degreeExamNameEn = rs.getString("degree_exam_name_en");
            model.degreeExamNameBn = rs.getString("degree_exam_name_bn");
            model.otherDegreeExam = rs.getString("other_degree_exam");
            model.resultExamNameEn = rs.getString("result_exam_name_en");
            model.resultExamNameBn = rs.getString("result_exam_name_bn");
            model.major = rs.getString("major");
            model.gradePointCat = rs.getInt("grade_point_cat");
            model.gradePoint = rs.getDouble("cgpa_number");
            model.yearOfPassingNumber = rs.getInt("year_of_passing_number");
            model.institutionName = rs.getString("institution_name");
            model.boardNameEn = rs.getString("board_name_en");
            model.boardNameBn = rs.getString("board_name_bn");
            return model;
        } catch (SQLException ex) {
            return null;
        }
    }

    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Employee_education_infoDTO) commonDTO, addSqlQuery, true);
    }

    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Employee_education_infoDTO) commonDTO, updateSqlQuery, false);
    }

    public List<EmployeeEducationInfoModel> getEducationInfos(long employeeRecordsId) {
        return getDTOs(educationInfoQslQuery, ps -> setValueForPreparedStatement(ps, employeeRecordsId), this::buildEmployeeEducationInfoModel);
    }

    private void setValueForPreparedStatement(PreparedStatement ps, long employeeRecordsId) {
        try {
            ps.setLong(1, employeeRecordsId);
        } catch (SQLException e) {
            logger.error(e);
            e.printStackTrace();
        }
    }

    public List<EmployeeEducationInfoDTOWithValue> getEmployeeEducationInfosByEmployeeId(long employeeId) {
        return getModelList(employeeId, this::buildEmployeeEducationInfoDTOWithValueFromDTO);
    }

    private EmployeeEducationInfoDTOWithValue buildEmployeeEducationInfoDTOWithValueFromDTO(Employee_education_infoDTO dto) {
        EmployeeEducationInfoDTOWithValue model = new EmployeeEducationInfoDTOWithValue();
        model.dto = dto;
        NameDTO nameDTO = Education_levelRepository.getInstance().getDTOByID(dto.educationLevelType);
        if (nameDTO == null) {
            return null;
        }
        model.educationLevelEng = nameDTO.nameEn;
        model.educationLevelBan = nameDTO.nameBn;
        DegreeExamDTO degreeExamDTO = DegreeExamRepository.getInstance().getByIdAndEducationLevelId(dto.degreeExamType, dto.educationLevelType);
        if (degreeExamDTO != null) {
            model.degreeExamEng = degreeExamDTO.nameEn;
            model.degreeExamBan = degreeExamDTO.nameBn;
        }
        
        NameDTO resultExamDTO = Result_examRepository.getInstance().getDTOByID(dto.resultExamType);
        if (resultExamDTO != null) {
            model.resultExamEng = resultExamDTO.nameEn;
            model.resultExamBan = resultExamDTO.nameBn;
        }

        model.gradePointBan = StringUtils.convertToBanNumber(String.valueOf(dto.cgpaNumber));
        if (dto.gradePointCat != null) {
            CategoryLanguageModel categoryLanguageModel = CatRepository.getInstance().getCategoryLanguageModel("grade_point", dto.gradePointCat);
            if (categoryLanguageModel != null) {
                model.gradePointScaleEng = categoryLanguageModel.englishText;
                model.gradePointScaleBan = categoryLanguageModel.banglaText;
            }
        }
        model.passingYearBan = StringUtils.convertToBanNumber(String.valueOf(dto.yearOfPassingNumber));

        NameDTO boardDTO = BoardRepository.getInstance().getDTOByID(dto.boardType);
        if (boardDTO != null) {
            model.boardBan = boardDTO.nameBn;
            model.boardEng = boardDTO.nameEn;
        }

        model.durationOfYearsBan = StringUtils.convertToBanNumber(String.valueOf(dto.durationYears));
        return model;
    }

    public List<Employee_education_infoDTO> getByEmployeeRecordIdWithMaxPassingYear(List<Long> empIds){
        return ConnectionAndStatementUtil.getDTOListByNumbers(getByEmployeeRecordIdAndMaxPassingYear,empIds,this::buildObjectFromResultSet);
    }

    public List<EmployeeEducationInfoDTOWithValue> getByEmpIdSortByEduLevel(long empId){
        String sql = String.format(getByEmpIdSortByEduLevel,empId);
        return ConnectionAndStatementUtil.getListOfT(sql,this::buildObjectFromResultSet)
                .stream()
                .map(this::buildEmployeeEducationInfoDTOWithValueFromDTO)
                .collect(Collectors.toList());
    }
}