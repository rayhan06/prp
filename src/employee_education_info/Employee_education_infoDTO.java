package employee_education_info;

import util.CommonEmployeeDTO;


public class Employee_education_infoDTO extends CommonEmployeeDTO implements Comparable< Employee_education_infoDTO > {
    public long educationLevelType = 0;
    public long degreeExamType = 0;
    public String otherDegreeExam = "";
    public String major = "";
    public long resultExamType = 0;
    public Integer gradePointCat;
    public Double cgpaNumber;
    public int yearOfPassingNumber = 0;
    public long boardType = 0;
    public String institutionName = "";
    public long universityInfoType = 0;
    public boolean isForeign = false;
    public int durationYears = 0;
    public String achievement = "";

    @Override
    public String toString() {
        return "Employee_education_infoDTO{" +
                "employeeRecordsId=" + employeeRecordsId +
                ", educationLevelType=" + educationLevelType +
                ", degreeExamType=" + degreeExamType +
                ", otherDegreeExam='" + otherDegreeExam + '\'' +
                ", major='" + major + '\'' +
                ", resultExamType=" + resultExamType +
                ", gradePointCat=" + gradePointCat +
                ", cgpaNumber=" + cgpaNumber +
                ", yearOfPassingNumber=" + yearOfPassingNumber +
                ", boardType=" + boardType +
                ", institutionName='" + institutionName + '\'' +
                ", universityInfo=" + universityInfoType +
                ", isForeign=" + isForeign +
                ", durationYears=" + durationYears +
                ", achievement='" + achievement + '\'' +
                ", filesDropzone=" + filesDropzone +
                ", insertionDate=" + insertionDate +
                ", insertedBy='" + insertedBy + '\'' +
                ", modifiedBy='" + modifiedBy + '\'' +
                ", searchColumn='" + searchColumn + '\'' +
                '}';
    }

    public long filesDropzone = 0;
    public long insertionDate = 0;
    public String insertedBy = "";
    public String modifiedBy = "";

    public Long getEducationLevelType() {
        return educationLevelType;
    }
    @Override
    public int compareTo(Employee_education_infoDTO o) {
        return this.getEducationLevelType().compareTo(o.getEducationLevelType());
    }
}