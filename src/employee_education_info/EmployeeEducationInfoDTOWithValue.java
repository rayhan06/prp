package employee_education_info;

public class EmployeeEducationInfoDTOWithValue {
    public Employee_education_infoDTO dto;
    public String educationLevelEng = "";
    public String educationLevelBan = "";
    public String degreeExamEng = "";
    public String degreeExamBan = "";
    public String resultExamEng = "";
    public String resultExamBan = "";
    public String gradePointScaleEng = "";
    public String gradePointScaleBan = "";
    public String gradePointBan = "";
    public String markPercentageBan = "";
    public String passingYearBan = "";
    public String boardEng = "";
    public String boardBan = "";
    public String durationOfYearsBan = "";

    @Override
    public String toString() {
        return "EmployeeEducationInfoDTOWithValue{" +
                "dto=" + dto +
                ", educationLevelEng='" + educationLevelEng + '\'' +
                ", educationLevelBan='" + educationLevelBan + '\'' +
                ", degreeExamEng='" + degreeExamEng + '\'' +
                ", degreeExamBan='" + degreeExamBan + '\'' +
                ", resultExamEng='" + resultExamEng + '\'' +
                ", resultExamBan='" + resultExamBan + '\'' +
                ", gradePointScaleEng='" + gradePointScaleEng + '\'' +
                ", gradePointScaleBan='" + gradePointScaleBan + '\'' +
                ", gradePointBan='" + gradePointBan + '\'' +
                ", markPercentageBan='" + markPercentageBan + '\'' +
                ", passingYearBan='" + passingYearBan + '\'' +
                ", boardEng='" + boardEng + '\'' +
                ", boardBan='" + boardBan + '\'' +
                '}';
    }
}
