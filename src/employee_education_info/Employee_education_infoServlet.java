package employee_education_info;

import common.BaseServlet;
import common.EmployeeServletService;
import employee_records.Employee_recordsRepository;
import pb.Utils;
import permission.MenuConstants;
import university_info.University_infoDAO;
import university_info.University_infoDTO;
import user.UserDTO;
import util.CommonDTO;
import util.HttpRequestUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@SuppressWarnings("Duplicates")
@WebServlet("/Employee_education_infoServlet")
@MultipartConfig
public class Employee_education_infoServlet extends BaseServlet implements EmployeeServletService {
    private static final long serialVersionUID = 1L;

    private final Employee_education_infoDAO employeeEducationInfoDAO = Employee_education_infoDAO.getInstance();

    @Override
    public String getTableName() {
        return employeeEducationInfoDAO.getTableName();
    }

    @Override
    public String getServletName() {
        return "Employee_education_infoServlet";
    }

    @Override
    public Employee_education_infoDAO getCommonDAOService() {
        return employeeEducationInfoDAO;
    }

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        Employee_education_infoDTO employee_education_infoDTO;
        boolean isLangEng = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
        long currentTime = System.currentTimeMillis();
        if (addFlag) {
            employee_education_infoDTO = new Employee_education_infoDTO();
            long empId;
            try{
                empId = Long.parseLong(request.getParameter("empId"));
            }catch (Exception ex){
                ex.printStackTrace();
                throw new Exception(isLangEng?"empId is not found":"empId পাওয়া যায়নি");
            }
            if(Employee_recordsRepository.getInstance().getById(empId) == null){
                throw new Exception(isLangEng?"Employee information is not found":"কর্মকর্তার তথ্য পাওয়া যায়নি");
            }
            employee_education_infoDTO.employeeRecordsId = empId;
            employee_education_infoDTO.insertedBy = String.valueOf(userDTO.ID);
            employee_education_infoDTO.insertionDate = currentTime;
        } else {
            employee_education_infoDTO =  employeeEducationInfoDAO.getDTOFromID(Long.parseLong(request.getParameter("iD")));
        }
        employee_education_infoDTO.modifiedBy = String.valueOf(userDTO.ID);
        employee_education_infoDTO.lastModificationTime = currentTime;

        employee_education_infoDTO.educationLevelType = Long.parseLong(request.getParameter("educationLevelType"));
        employee_education_infoDTO.degreeExamType = Long.parseLong(request.getParameter("degreeExamType"));

        employee_education_infoDTO.resultExamType = Utils.parseOptionalLong(
                request.getParameter("resultExamType"),
                0L,
                isLangEng ? "Give Proper Exam Result" : "সঠিক পরীক্ষার ফলাফল দিন"
        );
        employee_education_infoDTO.major = Utils.cleanAndGetOptionalString(request.getParameter("major"));
        employee_education_infoDTO.otherDegreeExam = Utils.cleanAndGetOptionalString(request.getParameter("otherDegreeExam"));

        if(employee_education_infoDTO.resultExamType == 4){
            employee_education_infoDTO.gradePointCat = Utils.parseOptionalInt(
                    request.getParameter("gradePointCat"),
                    0,
                    isLangEng ? "Give Proper Grade Scale" : "সঠিক গ্রেড স্কেল দিন"
            );
            employee_education_infoDTO.cgpaNumber = Utils.parseOptionalDouble(
                    request.getParameter("cgpaNumber"),
                    .0,
                    isLangEng ? "Give Proper GPA/CGPA Value" : "সঠিক দিন জিপিএ/সিজিপিএ"
            );
        }else{
            employee_education_infoDTO.gradePointCat = 0;
            employee_education_infoDTO.cgpaNumber = 0.0;
        }

        if(employee_education_infoDTO.educationLevelType > 4){
            employee_education_infoDTO.durationYears = Utils.parseOptionalInt(
                    request.getParameter("durationYears"),
                    0,
                    isLangEng ? "Give Proper Duration (Years)" : "সঠিক সময়কাল (বছর) দিন"
            );
            employee_education_infoDTO.boardType = 0;
        }else{
            employee_education_infoDTO.durationYears = 0;
            employee_education_infoDTO.boardType = Utils.parseOptionalLong(
                    request.getParameter("boardType"),
                    0L,
                    isLangEng ? "Give Correct Board" : "সঠিক বোর্ড দিন"
            );
        }
        employee_education_infoDTO.yearOfPassingNumber = Utils.parseOptionalInt(
                request.getParameter("yearOfPassingNumber"),
                0,
                isLangEng ? "Give Proper Year Of Passing" : "সঠিক পাশ করার বছর দিন"
        );
        employee_education_infoDTO.institutionName = Utils.cleanAndGetOptionalString(request.getParameter("institutionName"));

        employee_education_infoDTO.universityInfoType = Utils.parseOptionalLong(
                request.getParameter("instituteType"),
                0L,
                isLangEng ? "Give Correct University" : "সঠিক বিশ্ববিদ্যালয় দিন"
        );
        employee_education_infoDTO.isForeign = Boolean.getBoolean(request.getParameter("isForeign"));
        employee_education_infoDTO.achievement = Utils.cleanAndGetOptionalString(request.getParameter("achievement"));

        String value;
        value = request.getParameter("filesDropzone");
        value = value ==null?null:value.trim();
        if (value != null && value.length()>0) {
            employee_education_infoDTO.filesDropzone = Long.parseLong(value);
            if (!addFlag) {
                String filesDropzoneFilesToDelete = request.getParameter("filesDropzoneFilesToDelete");
                String[] deleteArray = filesDropzoneFilesToDelete.split(",");
                for (int i = 0; i < deleteArray.length; i++) {
                    if (i > 0) {
                        filesDAO.delete(Long.parseLong(deleteArray[i]));
                    }
                }
            }
        }

        if (addFlag) {
            employeeEducationInfoDAO.add(employee_education_infoDTO);
        } else {
            employeeEducationInfoDAO.update(employee_education_infoDTO);
        }
        return employee_education_infoDTO;
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[0];
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[0];
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[0];
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Employee_education_infoServlet.class;
    }

    @Override
    public String getAjaxAddRedirectURL(HttpServletRequest request,CommonDTO commonDTO) {
        return getAddOrEditOrDeleteRedirectURL(request,2,"education");
    }

    @Override
    public String getAjaxEditRedirectURL(HttpServletRequest request,CommonDTO commonDTO) {
        return getAddOrEditOrDeleteRedirectURL(request,2,"education");
    }

    @Override
    public String getDeleteRedirectURL(HttpServletRequest request) {
        return getAddOrEditOrDeleteRedirectURL(request,2,"education");
    }

    @Override
    public boolean getAddPagePermission(HttpServletRequest request) {
        return checkAddPermission(request);
    }

    @Override
    public boolean getEditPagePermission(HttpServletRequest request) {
        return checkEditOrViewPagePermission(request);
    }

    @Override
    public boolean getViewPagePermission(HttpServletRequest request) {
        return checkEditOrViewPagePermission(request);
    }

    @Override
    public boolean getEditPermission(HttpServletRequest request) {
        return checkEditOrViewPagePermission(request);
    }

    @Override
    public boolean getAddPermission(HttpServletRequest request) {
        return checkAddPermission(request);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        search(request);
        super.doGet(request, response);
    }
}