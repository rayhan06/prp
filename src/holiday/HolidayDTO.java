package holiday;
import java.util.*; 
import util.*; 


public class HolidayDTO extends CommonDTO
{

	public long currentDay = 0;
	public int isHoliday = 0;
    public String description = "";
	public int holidayCat = 0;
	public long officeStartTime = 0;
	public long officeEndTime = 0;
	
	
    @Override
	public String toString() {
            return "$HolidayDTO[" +
            " iD = " + iD +
            " currentDay = " + currentDay +
            " isHoliday = " + isHoliday +
            " description = " + description +
            " holidayCat = " + holidayCat +
            " officeStartTime = " + officeStartTime +
            " officeEndTime = " + officeEndTime +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}