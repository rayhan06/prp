package holiday;
import java.util.*; 
import util.*;


public class HolidayMAPS extends CommonMaps
{	
	public HolidayMAPS(String tableName)
	{
		
		java_allfield_type_map.put("current_day".toLowerCase(), "Long");
		java_allfield_type_map.put("is_holiday".toLowerCase(), "Integer");
		java_allfield_type_map.put("description".toLowerCase(), "String");
		java_allfield_type_map.put("holiday_cat".toLowerCase(), "Integer");
		java_allfield_type_map.put("office_start_time".toLowerCase(), "Long");
		java_allfield_type_map.put("office_end_time".toLowerCase(), "Long");

		java_anyfield_search_map.put("language_text.languageTextEnglish", "String");
		java_anyfield_search_map.put("language_text.languageTextBangla", "String");

		java_anyfield_search_map.put(tableName + ".current_day".toLowerCase(), "Long");
		java_anyfield_search_map.put(tableName + ".is_holiday".toLowerCase(), "Integer");
		java_anyfield_search_map.put(tableName + ".description".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".office_start_time".toLowerCase(), "Long");
		java_anyfield_search_map.put(tableName + ".office_end_time".toLowerCase(), "Long");

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("currentDay".toLowerCase(), "currentDay".toLowerCase());
		java_DTO_map.put("isHoliday".toLowerCase(), "isHoliday".toLowerCase());
		java_DTO_map.put("description".toLowerCase(), "description".toLowerCase());
		java_DTO_map.put("holidayCat".toLowerCase(), "holidayCat".toLowerCase());
		java_DTO_map.put("officeStartTime".toLowerCase(), "officeStartTime".toLowerCase());
		java_DTO_map.put("officeEndTime".toLowerCase(), "officeEndTime".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("current_day".toLowerCase(), "currentDay".toLowerCase());
		java_SQL_map.put("is_holiday".toLowerCase(), "isHoliday".toLowerCase());
		java_SQL_map.put("description".toLowerCase(), "description".toLowerCase());
		java_SQL_map.put("holiday_cat".toLowerCase(), "holidayCat".toLowerCase());
		java_SQL_map.put("office_start_time".toLowerCase(), "officeStartTime".toLowerCase());
		java_SQL_map.put("office_end_time".toLowerCase(), "officeEndTime".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Current Day".toLowerCase(), "currentDay".toLowerCase());
		java_Text_map.put("Is Holiday".toLowerCase(), "isHoliday".toLowerCase());
		java_Text_map.put("Description".toLowerCase(), "description".toLowerCase());
		java_Text_map.put("Holiday Cat".toLowerCase(), "holidayCat".toLowerCase());
		java_Text_map.put("Office Start Time".toLowerCase(), "officeStartTime".toLowerCase());
		java_Text_map.put("Office End Time".toLowerCase(), "officeEndTime".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}