package holiday;

import java.io.IOException;
import java.io.*;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import org.apache.log4j.Logger;

import login.LoginDTO;
import permission.MenuConstants;
import role.PermissionRepository;


import sessionmanager.SessionConstants;

import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;
import util.CommonRequestHandler;
import util.RecordNavigationManager4;

import java.util.*;
import javax.servlet.http.*;
import java.util.UUID;

import holiday.Constants;




import geolocation.GeoLocationDAO2;
import java.util.StringTokenizer;

import com.google.gson.Gson;

import pb.*;
import pbReport.*;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;




/**
 * Servlet implementation class HolidayServlet
 */
@WebServlet("/HolidayServlet")
@MultipartConfig
public class HolidayServlet extends HttpServlet
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(HolidayServlet.class);

    String tableName = "holiday";

	HolidayDAO holidayDAO;
	CommonRequestHandler commonRequestHandler;
    private final Gson gson = new Gson();

    /**
     * @see HttpServlet#HttpServlet()
     */
    public HolidayServlet()
	{
        super();
    	try
    	{
			holidayDAO = new HolidayDAO(tableName);
			commonRequestHandler = new CommonRequestHandler(holidayDAO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		System.out.println("In doget request = " + request);
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		try
		{
			String actionType = request.getParameter("actionType");
			if(actionType.equals("getAddPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.HOLIDAY_ADD))
				{
					commonRequestHandler.getAddPage(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getEditPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.HOLIDAY_UPDATE))
				{
					getHoliday(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getURL"))
			{
				String URL = request.getParameter("URL");
				System.out.println("URL = " + URL);
				response.sendRedirect(URL);
			}
			else if(actionType.equals("search"))
			{
				System.out.println("search requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.HOLIDAY_SEARCH))
				{

					if(isPermanentTable)
					{
						searchHoliday(request, response, isPermanentTable);
					}
					else
					{
						//searchHoliday(request, response, tempTableName, isPermanentTable);
					}
				}
			}
			else if(actionType.equals("view"))
			{
				System.out.println("view requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.HOLIDAY_SEARCH))
				{
					commonRequestHandler.view(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}
			else
			{
				request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}





	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		// TODO Auto-generated method stub
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		System.out.println("doPost");
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);

		try
		{
			String actionType = request.getParameter("actionType");
			System.out.println("actionType = " + actionType);
			if(actionType.equals("add"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.HOLIDAY_ADD))
				{
					System.out.println("going to  addHoliday ");
					addHoliday(request, response, true, userDTO, true);
				}
				else
				{
					System.out.println("Not going to  addHoliday ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}

			else if(actionType.equals("getDTO"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.HOLIDAY_ADD))
				{
					getDTO(request, response);
				}
				else
				{
					System.out.println("Not going to  addHoliday ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}

			}
			else if(actionType.equals("edit"))
			{

				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.HOLIDAY_UPDATE))
				{
					addHoliday(request, response, false, userDTO, isPermanentTable);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("delete"))
			{
				deleteHoliday(request, response, userDTO);
			}
			else if(actionType.equals("search"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.HOLIDAY_SEARCH))
				{
					searchHoliday(request, response, true);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getGeo"))
			{
				System.out.println("going to geoloc ");
				request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
			}

		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}

	private void getDTO(HttpServletRequest request, HttpServletResponse response)
	{
		try
		{
			System.out.println("In getDTO");
			HolidayDTO holidayDTO = (HolidayDTO)holidayDAO.getDTOByID(Long.parseLong(request.getParameter("ID")));
			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");

			String encoded = this.gson.toJson(holidayDTO);
			System.out.println("json encoded data = " + encoded);
			out.print(encoded);
			out.flush();
		}
		catch (NumberFormatException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void addHoliday(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, boolean isPermanentTable) throws IOException
	{
		// TODO Auto-generated method stub
		try
		{
			request.setAttribute("failureMessage", "");
			System.out.println("%%%% addHoliday");
			String path = getServletContext().getRealPath("/img2/");
			HolidayDTO holidayDTO;

			if(addFlag == true)
			{
				holidayDTO = new HolidayDTO();
			}
			else
			{
				holidayDTO = (HolidayDTO)holidayDAO.getDTOByID(Long.parseLong(request.getParameter("iD")));
			}

			String Value = "";

			Value = request.getParameter("currentDay");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("currentDay = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{

				holidayDTO.currentDay = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("isHoliday");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("isHoliday = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{

				holidayDTO.isHoliday = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("description");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("description = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{

				holidayDTO.description = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("holidayCat");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("holidayCat = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{

				holidayDTO.holidayCat = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("officeStartTime");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("officeStartTime = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{

				holidayDTO.officeStartTime = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("officeEndTime");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("officeEndTime = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{

				holidayDTO.officeEndTime = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			System.out.println("Done adding  addHoliday dto = " + holidayDTO);
			long returnedID = -1;

			if(isPermanentTable == false) //add new row for validation and make the old row outdated
			{
				holidayDAO.setIsDeleted(holidayDTO.iD, CommonDTO.OUTDATED);
				returnedID = holidayDAO.add(holidayDTO);
				holidayDAO.setIsDeleted(returnedID, CommonDTO.WAITING_FOR_APPROVAL);
			}
			else if(addFlag == true)
			{
				returnedID = holidayDAO.manageWriteOperations(holidayDTO, SessionConstants.INSERT, -1, userDTO);
			}
			else
			{
				returnedID = holidayDAO.manageWriteOperations(holidayDTO, SessionConstants.UPDATE, -1, userDTO);
			}









			if(isPermanentTable)
			{
				String inPlaceSubmit = request.getParameter("inplacesubmit");

				if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
				{
					getHoliday(request, response, returnedID);
				}
				else
				{
					response.sendRedirect("HolidayServlet?actionType=search");
				}
			}
			else
			{
				commonRequestHandler.validate(holidayDAO.getDTOByID(returnedID), request, response, userDTO);
			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}









	private void deleteHoliday(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO) throws IOException
	{
		try
		{
			String[] IDsToDelete = request.getParameterValues("ID");
			for(int i = 0; i < IDsToDelete.length; i ++)
			{
				long id = Long.parseLong(IDsToDelete[i]);
				System.out.println("------ DELETING " + IDsToDelete[i]);


				HolidayDTO holidayDTO = (HolidayDTO)holidayDAO.getDTOByID(id);
				holidayDAO.manageWriteOperations(holidayDTO, SessionConstants.DELETE, id, userDTO);
				response.sendRedirect("HolidayServlet?actionType=search");

			}
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}

	}

	private void getHoliday(HttpServletRequest request, HttpServletResponse response, long id) throws ServletException, IOException
	{
		System.out.println("in getHoliday");
		HolidayDTO holidayDTO = null;
		try
		{
			holidayDTO = (HolidayDTO)holidayDAO.getDTOByID(id);
			request.setAttribute("ID", holidayDTO.iD);
			request.setAttribute("holidayDTO",holidayDTO);
			request.setAttribute("holidayDAO",holidayDAO);

			String URL= "";

			String inPlaceEdit = request.getParameter("inplaceedit");
			String inPlaceSubmit = request.getParameter("inplacesubmit");
			String getBodyOnly = request.getParameter("getBodyOnly");

			if(inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase(""))
			{
				URL = "holiday/holidayInPlaceEdit.jsp";
				request.setAttribute("inplaceedit","");
			}
			else if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
			{
				URL = "holiday/holidaySearchRow.jsp";
				request.setAttribute("inplacesubmit","");
			}
			else
			{
				if(getBodyOnly  != null && !getBodyOnly.equalsIgnoreCase(""))
				{
					URL = "holiday/holidayEditBody.jsp?actionType=edit";
				}
				else
				{
					URL = "holiday/holidayEdit.jsp?actionType=edit";
				}
			}

			RequestDispatcher rd = request.getRequestDispatcher(URL);
			rd.forward(request, response);
		}
		catch (NumberFormatException e)
		{
			e.printStackTrace();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}


	private void getHoliday(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		getHoliday(request, response, Long.parseLong(request.getParameter("ID")));
	}

	private void searchHoliday(HttpServletRequest request, HttpServletResponse response, boolean isPermanent) throws ServletException, IOException
	{
		System.out.println("in  searchHoliday 1");
		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String ajax = request.getParameter("ajax");
		boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);

        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
			SessionConstants.NAV_HOLIDAY,
			request,
			holidayDAO,
			SessionConstants.VIEW_HOLIDAY,
			SessionConstants.SEARCH_HOLIDAY,
			tableName,
			isPermanent,
			userDTO,
			"",
			true);
        try
        {
			System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        }
        catch(Exception e)
        {
			System.out.println("failed to dojob" + e);
        }

		request.setAttribute("holidayDAO",holidayDAO);
        RequestDispatcher rd;
        if(!isPermanent)
        {
        	if(hasAjax == false)
	        {
	        	System.out.println("Going to holiday/holidayApproval.jsp");
	        	rd = request.getRequestDispatcher("holiday/holidayApproval.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to holiday/holidayApprovalForm.jsp");
	        	rd = request.getRequestDispatcher("holiday/holidayApprovalForm.jsp");
	        }
        }
        else
        {
	        if(hasAjax == false)
	        {
	        	System.out.println("Going to holiday/holidaySearch.jsp");
	        	rd = request.getRequestDispatcher("holiday/holidaySearch.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to holiday/holidaySearchForm.jsp");
	        	rd = request.getRequestDispatcher("holiday/holidaySearchForm.jsp");
	        }
        }
		rd.forward(request, response);
	}

}

