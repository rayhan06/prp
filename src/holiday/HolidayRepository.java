package holiday;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class HolidayRepository implements Repository {
	HolidayDAO holidayDAO = null;
	
	public void setDAO(HolidayDAO holidayDAO)
	{
		this.holidayDAO = holidayDAO;
	}
	
	
	static Logger logger = Logger.getLogger(HolidayRepository.class);
	Map<Long, HolidayDTO>mapOfHolidayDTOToiD;
	Map<Long, Set<HolidayDTO> >mapOfHolidayDTOTocurrentDay;
	Map<Long,String> holidayToDescriptionMap = new HashMap<>();

	static HolidayRepository instance = null;  
	private HolidayRepository(){
		mapOfHolidayDTOToiD = new ConcurrentHashMap<>();
		mapOfHolidayDTOTocurrentDay = new ConcurrentHashMap<>();
		holidayDAO = new HolidayDAO();
		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static HolidayRepository getInstance(){
		if (instance == null){
			instance = new HolidayRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(holidayDAO == null)
		{
			return;
		}
		try {
			List<HolidayDTO> holidayDTOs = holidayDAO.getAllHoliday(reloadAll);
			for(HolidayDTO holidayDTO : holidayDTOs) {
				holidayToDescriptionMap.put(holidayDTO.currentDay, holidayDTO.description);
				HolidayDTO oldHolidayDTO = getHolidayDTOByID(holidayDTO.iD);
				if( oldHolidayDTO != null ) {
					mapOfHolidayDTOToiD.remove(oldHolidayDTO.iD);
				
					if(mapOfHolidayDTOTocurrentDay.containsKey(oldHolidayDTO.currentDay)) {
						mapOfHolidayDTOTocurrentDay.get(oldHolidayDTO.currentDay).remove(oldHolidayDTO);
					}
					if(mapOfHolidayDTOTocurrentDay.get(oldHolidayDTO.currentDay).isEmpty()) {
						mapOfHolidayDTOTocurrentDay.remove(oldHolidayDTO.currentDay);
					}
					
				}
				if(holidayDTO.isDeleted == 0) 
				{
					
					mapOfHolidayDTOToiD.put(holidayDTO.iD, holidayDTO);
				
					if( ! mapOfHolidayDTOTocurrentDay.containsKey(holidayDTO.currentDay)) {
						mapOfHolidayDTOTocurrentDay.put(holidayDTO.currentDay, new HashSet<>());
					}
					mapOfHolidayDTOTocurrentDay.get(holidayDTO.currentDay).add(holidayDTO);
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<HolidayDTO> getHolidayList() {
		List <HolidayDTO> holidays = new ArrayList<HolidayDTO>(this.mapOfHolidayDTOToiD.values());
		return holidays;
	}

	public Map<Long,String> getHolidayToDescriptionMap() {
		return holidayToDescriptionMap;
	}
	
	public HolidayDTO getHolidayDTOByID( long ID){
		return mapOfHolidayDTOToiD.get(ID);
	}
	
	
	public List<HolidayDTO> getHolidayDTOBycurrent_day(long current_day) {
		return new ArrayList<>( mapOfHolidayDTOTocurrentDay.getOrDefault(current_day,new HashSet<>()));
	}
	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "holiday";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


