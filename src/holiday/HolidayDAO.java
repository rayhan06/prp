package holiday;

import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import common.ConnectionAndStatementUtil;
import ot_type_calendar.OT_type_CalendarDTO;
import sessionmanager.SessionConstants;


import org.apache.log4j.Logger;

import dbm.*;

import repository.RepositoryManager;

import util.*;
import pb.*;
import user.UserDTO;

public class HolidayDAO extends NavigationService4 {

    Logger logger = Logger.getLogger(getClass());


    public HolidayDAO(String tableName) {
        super(tableName);
        joinSQL = "";

        joinSQL += " join category on (";
        joinSQL += " (" + tableName + ".holiday_cat = category.value  and category.domain_name = 'holiday')";
        joinSQL += " )";
        joinSQL += " join language_text on category.language_id = language_text.id";
        commonMaps = new HolidayMAPS(tableName);
    }

    public HolidayDAO() {
        this("holiday");
    }


    public long add(CommonDTO commonDTO) throws Exception {

        HolidayDTO holidayDTO = (HolidayDTO) commonDTO;

        Connection connection = null;
        PreparedStatement ps = null;

        long lastModificationTime = System.currentTimeMillis();

        try {
            connection = DBMW.getInstance().getConnection();

            if (connection == null) {
                System.out.println("nullconn");
            }

            holidayDTO.iD = DBMW.getInstance().getNextSequenceId(tableName);

            String sql = "INSERT INTO " + tableName;

            sql += " (";
            sql += "ID";
            sql += ", ";
            sql += "current_day";
            sql += ", ";
            sql += "is_holiday";
            sql += ", ";
            sql += "description";
            sql += ", ";
            sql += "holiday_cat";
            sql += ", ";
            sql += "office_start_time";
            sql += ", ";
            sql += "office_end_time";
            sql += ", ";
            sql += "isDeleted";
            sql += ", ";
            sql += "lastModificationTime";

            sql += ")";


            sql += " VALUES(";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";

            sql += ")";


            ps = connection.prepareStatement(sql);


            int index = 1;

            ps.setObject(index++, holidayDTO.iD);
            ps.setObject(index++, holidayDTO.currentDay);
            ps.setObject(index++, holidayDTO.isHoliday);
            ps.setObject(index++, holidayDTO.description);
            ps.setObject(index++, holidayDTO.holidayCat);
            ps.setObject(index++, holidayDTO.officeStartTime);
            ps.setObject(index++, holidayDTO.officeEndTime);
            ps.setObject(index++, holidayDTO.isDeleted);
            ps.setObject(index++, lastModificationTime);

            System.out.println(ps);
            ps.execute();


            recordUpdateTime(connection, lastModificationTime);

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (connection != null) {
                    DBMW.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
        return holidayDTO.iD;
    }


    //need another getter for repository
    public CommonDTO getDTOByID(long ID) throws Exception {
        Connection connection = null;
        ResultSet rs = null;
        Statement stmt = null;
        HolidayDTO holidayDTO = null;
        try {

            String sql = "SELECT * ";

            sql += " FROM " + tableName;

            sql += " WHERE ID=" + ID;

            printSql(sql);

            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();


            rs = stmt.executeQuery(sql);

            if (rs.next()) {
                holidayDTO = new HolidayDTO();

                holidayDTO.iD = rs.getLong("ID");
                holidayDTO.currentDay = rs.getLong("current_day");
                holidayDTO.isHoliday = rs.getInt("is_holiday");
                holidayDTO.description = rs.getString("description");
                holidayDTO.holidayCat = rs.getInt("holiday_cat");
                holidayDTO.officeStartTime = rs.getLong("office_start_time");
                holidayDTO.officeEndTime = rs.getLong("office_end_time");
                holidayDTO.isDeleted = rs.getInt("isDeleted");
                holidayDTO.lastModificationTime = rs.getLong("lastModificationTime");

            }


        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }

            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
        return holidayDTO;
    }

    public long update(CommonDTO commonDTO) throws Exception {
        HolidayDTO holidayDTO = (HolidayDTO) commonDTO;

        Connection connection = null;
        PreparedStatement ps = null;

        long lastModificationTime = System.currentTimeMillis();
        try {
            connection = DBMW.getInstance().getConnection();

            String sql = "UPDATE " + tableName;

            sql += " SET ";
            sql += "current_day=?";
            sql += ", ";
            sql += "is_holiday=?";
            sql += ", ";
            sql += "description=?";
            sql += ", ";
            sql += "holiday_cat=?";
            sql += ", ";
            sql += "office_start_time=?";
            sql += ", ";
            sql += "office_end_time=?";
            sql += ", lastModificationTime = " + lastModificationTime + "";
            sql += " WHERE ID = " + holidayDTO.iD;


            ps = connection.prepareStatement(sql);


            int index = 1;

            ps.setObject(index++, holidayDTO.currentDay);
            ps.setObject(index++, holidayDTO.isHoliday);
            ps.setObject(index++, holidayDTO.description);
            ps.setObject(index++, holidayDTO.holidayCat);
            ps.setObject(index++, holidayDTO.officeStartTime);
            ps.setObject(index++, holidayDTO.officeEndTime);
            System.out.println(ps);
            ps.executeUpdate();


            recordUpdateTime(connection, lastModificationTime);


        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (connection != null) {
                    DBMW.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
        return holidayDTO.iD;
    }


    public List<HolidayDTO> getDTOs(Collection recordIDs) {
        Connection connection = null;
        ResultSet rs = null;
        Statement stmt = null;
        HolidayDTO holidayDTO = null;
        List<HolidayDTO> holidayDTOList = new ArrayList<>();
        if (recordIDs.isEmpty()) {
            return holidayDTOList;
        }
        try {

            String sql = "SELECT * ";

            sql += " FROM " + tableName;

            sql += " WHERE ID IN ( ";

            for (int i = 0; i < recordIDs.size(); i++) {
                if (i != 0) {
                    sql += ",";
                }
                sql += ((ArrayList) recordIDs).get(i);
            }
            sql += ")  order by lastModificationTime desc";

            printSql(sql);

            logger.debug("sql " + sql);
            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();


            rs = stmt.executeQuery(sql);

            while (rs.next()) {
                holidayDTO = new HolidayDTO();
                holidayDTO.iD = rs.getLong("ID");
                holidayDTO.currentDay = rs.getLong("current_day");
                holidayDTO.isHoliday = rs.getInt("is_holiday");
                holidayDTO.description = rs.getString("description");
                holidayDTO.holidayCat = rs.getInt("holiday_cat");
                holidayDTO.officeStartTime = rs.getLong("office_start_time");
                holidayDTO.officeEndTime = rs.getLong("office_end_time");
                holidayDTO.isDeleted = rs.getInt("isDeleted");
                holidayDTO.lastModificationTime = rs.getLong("lastModificationTime");
                System.out.println("got this DTO: " + holidayDTO);

                holidayDTOList.add(holidayDTO);

            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }

            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
        return holidayDTOList;

    }

    //add repository
    public List<HolidayDTO> getAllHoliday(boolean isFirstReload) {
        List<HolidayDTO> holidayDTOList = new ArrayList<>();

        String sql = "SELECT * FROM holiday";
        sql += " WHERE ";


        if (isFirstReload) {
            sql += " isDeleted =  0";
        }
        if (!isFirstReload) {
            sql += " lastModificationTime >= " + RepositoryManager.lastModifyTime;
        }
        sql += " order by holiday.lastModificationTime desc";
        printSql(sql);

        Connection connection = null;
        Statement stmt = null;
        ResultSet rs = null;


        try {
            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();
            rs = stmt.executeQuery(sql);


            while (rs.next()) {
                HolidayDTO holidayDTO = new HolidayDTO();
                holidayDTO.iD = rs.getLong("ID");
                holidayDTO.currentDay = rs.getLong("current_day");
                holidayDTO.isHoliday = rs.getInt("is_holiday");
                holidayDTO.description = rs.getString("description");
                holidayDTO.holidayCat = rs.getInt("holiday_cat");
                holidayDTO.officeStartTime = rs.getLong("office_start_time");
                holidayDTO.officeEndTime = rs.getLong("office_end_time");
                holidayDTO.isDeleted = rs.getInt("isDeleted");
                holidayDTO.lastModificationTime = rs.getLong("lastModificationTime");

                holidayDTOList.add(holidayDTO);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }

        return holidayDTOList;
    }


    public List<HolidayDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO) {
        return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
    }


    public List<HolidayDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
                                    String filter, boolean tableHasJobCat) {
        Connection connection = null;
        ResultSet rs = null;
        Statement stmt = null;
        List<HolidayDTO> holidayDTOList = new ArrayList<>();

        try {

            String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat);

            printSql(sql);

            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();


            rs = stmt.executeQuery(sql);

            while (rs.next()) {
                HolidayDTO holidayDTO = new HolidayDTO();
                holidayDTO.iD = rs.getLong("ID");
                holidayDTO.currentDay = rs.getLong("current_day");
                holidayDTO.isHoliday = rs.getInt("is_holiday");
                holidayDTO.description = rs.getString("description");
                holidayDTO.holidayCat = rs.getInt("holiday_cat");
                holidayDTO.officeStartTime = rs.getLong("office_start_time");
                holidayDTO.officeEndTime = rs.getLong("office_end_time");
                holidayDTO.isDeleted = rs.getInt("isDeleted");
                holidayDTO.lastModificationTime = rs.getLong("lastModificationTime");

                holidayDTOList.add(holidayDTO);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }

            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
        return holidayDTOList;

    }

    public HolidayDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            HolidayDTO dto = new HolidayDTO();
            dto.iD = rs.getLong("ID");
            dto.currentDay = rs.getLong("current_day");
            dto.isHoliday = rs.getInt("is_holiday");
            dto.description = rs.getString("description");
            dto.holidayCat = rs.getInt("holiday_cat");
            dto.officeStartTime = rs.getLong("office_start_time");
            dto.officeEndTime = rs.getLong("office_end_time");
            dto.isDeleted = rs.getInt("isDeleted");
            dto.lastModificationTime = rs.getLong("lastModificationTime");
            return dto;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    private static final String findByTimeRangeSql =
            "SELECT * FROM holiday WHERE is_holiday = 1 AND isDeleted = 0 AND current_day >= %d AND current_day < %d";

    public List<HolidayDTO> findByTimeRange(long startDateInclusive, long endDateExclusive) {
        return ConnectionAndStatementUtil.getListOfT(
                String.format(findByTimeRangeSql, startDateInclusive, endDateExclusive),
                this::buildObjectFromResultSet
        );
    }

    public Map<Long, List<HolidayDTO>> findByTimeRangeGroupedByDate(long startDateInclusive, long endDateExclusive) {
        return findByTimeRange(startDateInclusive, endDateExclusive)
                .stream()
                .collect(Collectors.groupingBy(holidayDTO -> holidayDTO.currentDay));
    }
}
	