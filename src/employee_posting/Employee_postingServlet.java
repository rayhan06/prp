package employee_posting;

import common.BaseServlet;
import common.CommonDAOService;
import common.EmployeeServletService;
import common.StringUtils;
import employee_office_report.InChargeLevelEnum;
import employee_offices.EmployeeOfficeDTO;
import employee_offices.EmployeeOfficesDAO;
import employee_records.Employee_recordsRepository;
import geolocation.GeoCountryRepository;
import office_unit_organograms.OfficeUnitOrganogramsRepository;
import office_units.Office_unitsRepository;
import offices.OfficesRepository;
import pb.Utils;
import permission.MenuConstants;
import sessionmanager.SessionConstants;
import user.UserDTO;
import util.CommonConstant;
import util.CommonDTO;
import util.HttpRequestUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

@SuppressWarnings("Duplicates")
@WebServlet("/Employee_postingServlet")
@MultipartConfig
public class Employee_postingServlet extends BaseServlet implements EmployeeServletService {
    private static final long serialVersionUID = 1L;

    private final Employee_postingDAO employee_postingDAO = Employee_postingDAO.getInstance();

    @Override
    public String getTableName() {
        return employee_postingDAO.getTableName();
    }

    @Override
    public String getServletName() {
        return "Employee_postingServlet";
    }

    @Override
    public Employee_postingDAO getCommonDAOService() {
        return employee_postingDAO;
    }

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        boolean isLangEng = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
        Employee_postingDTO employee_postingDTO;
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        long currentTime = System.currentTimeMillis();
        String postingType = Utils.doJsoupCleanOrReturnEmpty(request.getParameter("localOrForeign"));
        if (postingType == null || postingType.length() == 0) {
            throw new Exception(isLangEng ? "Internal mandatory fields is missing" : "অভ্যন্তরীন আবশ্যিক অথ্য পাওয়া যায় নি");
        }
        boolean isLocalPosting = postingType.equalsIgnoreCase("localPosting");
        if (addFlag) {
            employee_postingDTO = new Employee_postingDTO();
            long empId;
            try {
                empId = Long.parseLong(request.getParameter("empId"));
            } catch (Exception ex) {
                ex.printStackTrace();
                throw new Exception(isLangEng ? "empId is not found" : "empId পাওয়া যায়নি");
            }
            if (Employee_recordsRepository.getInstance().getById(empId) == null) {
                throw new Exception(isLangEng ? "Employee information is not found" : "কর্মকর্তার তথ্য পাওয়া যায়নি");
            }
            employee_postingDTO.localOrForeign = isLocalPosting ? PostingTypeEnum.LOCAL.getValue() : PostingTypeEnum.FOREIGN.getValue();
            employee_postingDTO.employeeRecordsId = empId;
            employee_postingDTO.insertedByUserId = userDTO.ID;
            employee_postingDTO.insertionDate = currentTime;
        } else {
            employee_postingDTO = employee_postingDAO.getDTOFromID(Long.parseLong(request.getParameter("iD")));
            if (employee_postingDTO.localOrForeign == PostingTypeEnum.LOCAL.getValue() && !isLocalPosting) {
                throw new Exception(isLangEng ? "Internal mandatory fields is modified" : "অভ্যন্তরীন আবশ্যিক অথ্য পরিবর্তন করা হয়েছে");
            }
        }
        employee_postingDTO.modifiedBy = String.valueOf(userDTO.ID);
        employee_postingDTO.lastModificationTime = currentTime;

        InChargeLevelEnum inChargeLevelEnum = null;
        if (isLocalPosting) {
            try {
                employee_postingDTO.post = Long.parseLong(request.getParameter("post"));
            } catch (Exception ex) {
                throw new Exception(isLangEng ? "Select office" : "অফিস বাছাই করুন");
            }
            if (employee_postingDTO.post != Office_unitsRepository.OTHER_MAXIMUM_VALUE && Office_unitsRepository.getInstance().getOffice_unitsDTOByID(employee_postingDTO.post) == null) {
                throw new Exception(isLangEng ? "Select correct office" : "সঠিক অফিস বাছাই করুন");
            }
            if (employee_postingDTO.post != Office_unitsRepository.OTHER_MAXIMUM_VALUE) {
                try {
                    employee_postingDTO.designation = Long.parseLong(request.getParameter("designation"));
                } catch (Exception ex) {
                    throw new Exception(isLangEng ? "Select designation" : "পদবী বাছাই করুন");
                }
                if (OfficeUnitOrganogramsRepository.getInstance().getById(employee_postingDTO.designation) == null) {
                    throw new Exception(isLangEng ? "Select correct designation" : "সঠিক পদবী বাছাই করুন");
                }
            } else {
                employee_postingDTO.oldPost = request.getParameter("oldOffice");
                employee_postingDTO.oldDesignation = request.getParameter("oldDesignation");
            }


            inChargeLevelEnum = InChargeLevelEnum.getByValue(request.getParameter("inChargeLevel"));
            if (inChargeLevelEnum == null) {
                throw new Exception(isLangEng ? "Provide In charge Level" : "ইনচার্জ লেভেল দিন");
            }
        } else {
            try {
                employee_postingDTO.country = Long.parseLong(request.getParameter("country"));
            } catch (Exception ex) {
                throw new Exception(isLangEng ? "Select country" : "দেশ বাছাই করুন");
            }
            if (GeoCountryRepository.getInstance().getDTOByID(employee_postingDTO.country) == null) {
                throw new Exception(isLangEng ? "Select correct country" : "সঠিক দেশ বাছাই করুন");
            }

            employee_postingDTO.organization = Utils.doJsoupCleanOrReturnEmpty(request.getParameter("organization"));
            if (employee_postingDTO.organization.length() == 0) {
                throw new Exception(isLangEng ? "Enter Organization" : "সংস্থা্র নাম লিখুন");
            }

            employee_postingDTO.designationForeign = Utils.doJsoupCleanOrReturnEmpty(request.getParameter("designation_foreign"));
            if (employee_postingDTO.designationForeign.length() == 0) {
                throw new Exception(isLangEng ? "Please enter designation" : "অনুগ্রহ করে পদবী লিখুন");
            }
        }

        try {
            employee_postingDTO.postingFrom = dateFormat.parse(request.getParameter("postingFrom")).getTime();
        } catch (Exception ex) {
            throw new Exception(isLangEng ? "Enter a valid Posting Start Date" : "সঠিক নিযুক্তি শুরুর তারিখ দিন");
        }

        if (request.getParameter("isCurrentlyWorking") != null && Boolean.parseBoolean(request.getParameter("isCurrentlyWorking"))) {
            employee_postingDTO.postingTo = SessionConstants.MIN_DATE;
        } else {
            try {
                employee_postingDTO.postingTo = dateFormat.parse(request.getParameter("postingTo")).getTime();
            } catch (Exception ex) {
                throw new Exception(isLangEng ? "Enter a valid posting start date" : "সঠিক নিযুক্তি শেষের তারিখ দিন");
            }

            if (employee_postingDTO.postingFrom > employee_postingDTO.postingTo)
                throw new Exception(isLangEng ? "From date can not be greater than to date" : "তারিখ হতে, তারিখ পর্যন্ত থেকে বড় হতে পারে না");
        }

        if (addFlag) {
            if (isLocalPosting) {
                EmployeeOfficesDAO.getInstance().add(getEmployeeOfficeDTO(employee_postingDTO, inChargeLevelEnum));
            } else {
                employee_postingDAO.add(employee_postingDTO);
            }
        } else {
            employee_postingDAO.update(employee_postingDTO);
        }
        return employee_postingDTO;
    }

    private EmployeeOfficeDTO getEmployeeOfficeDTO(Employee_postingDTO employeePostingDTO, InChargeLevelEnum inChargeLevelEnum) {
        EmployeeOfficeDTO employeeOfficeDTO = new EmployeeOfficeDTO();
        employeeOfficeDTO.employeeRecordId = employeePostingDTO.employeeRecordsId;

        employeeOfficeDTO.officeId = (int) CommonConstant.DEFAULT_OFFICE_ID;
        employeeOfficeDTO.officeNameEn = OfficesRepository.getInstance().getOfficesDTOByID(employeeOfficeDTO.officeId).officeNameEng;
        employeeOfficeDTO.officeNameBn = OfficesRepository.getInstance().getOfficesDTOByID(employeeOfficeDTO.officeId).officeNameBng;

        employeeOfficeDTO.officeUnitId = employeePostingDTO.post;
        if (employeeOfficeDTO.officeUnitId != Office_unitsRepository.OTHER_MAXIMUM_VALUE) {
            employeeOfficeDTO.unitNameEn = Office_unitsRepository.getInstance().geText("English", employeeOfficeDTO.officeUnitId);
            employeeOfficeDTO.unitNameBn = Office_unitsRepository.getInstance().geText("BANGLA", employeeOfficeDTO.officeUnitId);

            employeeOfficeDTO.officeUnitOrganogramId = employeePostingDTO.designation;
            employeeOfficeDTO.designationEn = OfficeUnitOrganogramsRepository.getInstance().getDesignation("ENGLISH", employeeOfficeDTO.officeUnitOrganogramId);
            employeeOfficeDTO.designationBn = OfficeUnitOrganogramsRepository.getInstance().getDesignation("BANGLA", employeeOfficeDTO.officeUnitOrganogramId);
            employeeOfficeDTO.designation = employeeOfficeDTO.designationEn;
        } else {
            employeeOfficeDTO.unitNameEn = employeePostingDTO.oldPost;
            employeeOfficeDTO.unitNameBn = employeePostingDTO.oldPost;

            employeeOfficeDTO.officeUnitOrganogramId = -1L;
            employeeOfficeDTO.designationEn = employeePostingDTO.oldDesignation;
            employeeOfficeDTO.designationBn = employeePostingDTO.oldDesignation;
            employeeOfficeDTO.designation = employeePostingDTO.oldDesignation;
        }


        employeeOfficeDTO.inchargeLabel = inChargeLevelEnum.getValue();
        employeeOfficeDTO.isDefault = employeeOfficeDTO.inchargeLabel.equalsIgnoreCase("Routine responsibility") ? 1 : 0;
        employeeOfficeDTO.status = 0;

        employeeOfficeDTO.joiningDate = employeePostingDTO.postingFrom;
        employeeOfficeDTO.lastOfficeDate = employeePostingDTO.postingTo;

        employeeOfficeDTO.created = employeeOfficeDTO.modified = employeeOfficeDTO.lastModificationTime = employeePostingDTO.insertionDate;
        employeeOfficeDTO.createdBy = employeeOfficeDTO.modifiedBy = employeePostingDTO.insertedByUserId;
        return employeeOfficeDTO;
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[]{MenuConstants.EMPLOYEE_POSTING_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[]{MenuConstants.EMPLOYEE_POSTING_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[]{MenuConstants.EMPLOYEE_POSTING_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Employee_postingServlet.class;
    }

    @Override
    public String getAjaxAddRedirectURL(HttpServletRequest request, CommonDTO commonDTO) {
        boolean isLocalPosting = request.getParameter("data").equalsIgnoreCase("localPosting");
        return getAddOrEditOrDeleteRedirectURL(request, isLocalPosting ? 3 : 4, isLocalPosting ? "parliament_service_history" : "foreignPosting");
    }

    @Override
    public String getAjaxEditRedirectURL(HttpServletRequest request, CommonDTO commonDTO) {
        return getAjaxAddRedirectURL(request, commonDTO);
    }

    @Override
    public String getDeleteRedirectURL(HttpServletRequest request) {
        return getAddOrEditOrDeleteRedirectURL(request, 4, request.getParameter("data").equalsIgnoreCase("localPosting") ? "localPosting" : "foreignPosting");
    }

    @Override
    public boolean getEditPagePermission(HttpServletRequest request) {
        return checkEditOrViewPagePermission(request);
    }

    @Override
    public boolean getViewPagePermission(HttpServletRequest request) {
        return checkEditOrViewPagePermission(request);
    }

    @Override
    public boolean getEditPermission(HttpServletRequest request) {
        return checkEditOrViewPagePermission(request);
    }

    @Override
    public boolean getAddPermission(HttpServletRequest request) {
        return checkAddPermission(request);
    }

    @Override
    public boolean getAddPagePermission(HttpServletRequest request) {
        return checkAddPermission(request);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        switch (request.getParameter("actionType")) {
            case "search":
                Map<String, String> map = new HashMap<>();
                map.put("localOrForeign", String.valueOf(PostingTypeEnum.FOREIGN.getValue()));
                request.setAttribute(CommonDAOService.CRITERIA_MAP_KEY, map);
                search(request);
                super.doGet(request, response);
                break;
            case "getDesignationOpt":
                long officeUnitId = Long.parseLong(request.getParameter("office_unit_id"));
                String language = request.getParameter("language");
                Long selectedId = StringUtils.parseIdFromStr(request.getParameter("selectedId"));
                String options = OfficeUnitOrganogramsRepository.getInstance().buildOptionsForDesignation(language, selectedId, officeUnitId);
                PrintWriter out = response.getWriter();
                out.println(options);
                out.close();
                return;
            default:
                super.doGet(request, response);
        }
    }
}