package employee_posting;

import sessionmanager.SessionConstants;
import util.CommonEmployeeDTO;

public class Employee_postingDTO extends CommonEmployeeDTO {
    public int localOrForeign = 0;              // if 0, then local else, foreign posting
    public long post = 0;
    public long designation = 0;
    public String oldPost = "";
    public String oldDesignation = "";
    public long country = 0;
    public long place = 0;
    public String organization = "";
    public long postingFrom = SessionConstants.MIN_DATE;
    public long postingTo = SessionConstants.MIN_DATE;
    public String payScale = "";
    public long insertedByUserId = 0;
    public long insertionDate = 0;
    public String modifiedBy = "";
    public String designationForeign = "";

    @Override
    public String toString() {
        return "Employee_postingDTO{" +
                "employeeRecordsId=" + employeeRecordsId +
                ", localOrForeign=" + localOrForeign +
                ", post=" + post +
                ", designation=" + designation +
                ", country=" + country +
                ", place=" + place +
                ", organization='" + organization + '\'' +
                ", postingFrom=" + postingFrom +
                ", postingTo=" + postingTo +
                ", payScale='" + payScale + '\'' +
                ", insertedByUserId=" + insertedByUserId +
                ", insertionDate=" + insertionDate +
                ", modifiedBy='" + modifiedBy + '\'' +
                ", searchColumn='" + searchColumn + '\'' +
                '}';
    }
}

