package employee_posting;

import util.CommonDTO;

public class Employee_local_posting_model extends CommonDTO
{
    public Employee_postingDTO dto;
    public String postEng="";
    public String postBng="";
    public String designationEng="";
    public String designationBng="";
    public String postingFromEng="";
    public String postingFromBng="";
    public String postingToEng="";
    public String postingToBng="";
    public String payScaleEng = "";
    public String payScaleBng = "";



    @Override
    public String toString() {
        return "Employee_local_posting_model[" +
                " dto = " + dto.toString() +
                " postEng = " + postEng +
                " postBng = " + postBng +
                " designationEng = " + designationEng +
                " designationBng = " + designationBng +
                " postingFromEng = " + postingFromEng +
                " postingFromBng = " + postingFromBng +
                " postingToEng = " + postingToEng +
                " postingToBng = " + postingToBng +
                " payScaleEng = " + payScaleEng +
                " payScaleBng = " + payScaleBng +
                "]";
    }
}
