package employee_posting;

import common.EmployeeCommonDAOService;
import dbm.DBMR;
import employee_records.Employee_recordsDAO;
import employee_records.Employee_recordsDTO;
import office_unit_organograms.OfficeUnitOrganograms;
import office_unit_organograms.OfficeUnitOrganogramsRepository;
import office_units.Office_unitsDAO;
import office_units.Office_unitsDTO;
import org.apache.log4j.Logger;
import pb.CatRepository;
import pb.CategoryLanguageModel;
import util.CommonDTO;
import util.StringUtils;

import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.*;
import java.util.stream.Collectors;

@SuppressWarnings({"Duplicates"})
public class Employee_postingDAO implements EmployeeCommonDAOService<Employee_postingDTO> {

    private static final Logger logger = Logger.getLogger(Employee_postingDAO.class);

    private static final String addQuery =
            "INSERT INTO {tableName} (employee_records_id, local_or_foreign, post, designation, country, place, organization,posting_from, " +
            "posting_to, pay_scale, inserted_by_user_id, insertion_date, modified_by,search_column, lastModificationTime,designation_foreign," +
            "isDeleted, ID) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

    private static final String updateQuery =
            "UPDATE {tableName} SET employee_records_id=?, local_or_foreign=?, post=?, designation=?, country=?, place=?, " +
            "organization=?,posting_from=?, posting_to=?, pay_scale=?, inserted_by_user_id=?, insertion_date=?, modified_by=?," +
            "search_column=?, lastModificationTime=?,designation_foreign=? WHERE ID = ?";

    private final Map<String, String> searchMap = new HashMap<>();

    private Employee_postingDAO() {
        searchMap.put("organization", " and (organization like ?)");
        searchMap.put("posting_from", " and (posting_from >= ?)");
        searchMap.put("posting_to", " and (posting_to <= ?)");
        searchMap.put("pay_scale", " and (pay_scale = ?)");
        searchMap.put("AnyField", " and (search_column like ?)");
        searchMap.put("localOrForeign", " and (local_or_foreign = ?)");
        searchMap.put("employee_records_id_internal", " and (employee_records_id = ?)");
    }

    private static Employee_postingDAO INSTANCE = null;

    public static Employee_postingDAO getInstance() {
        if (INSTANCE == null) {
            synchronized (Employee_postingDAO.class) {
                if (INSTANCE == null) {
                    INSTANCE = new Employee_postingDAO();
                }
            }
        }
        return INSTANCE;
    }

    public void set(PreparedStatement ps, Employee_postingDTO employee_postingDTO, boolean isInsert) throws SQLException {
        int index = 0;
        setSearchColumn(employee_postingDTO);
        ps.setObject(++index, employee_postingDTO.employeeRecordsId);
        ps.setObject(++index, employee_postingDTO.localOrForeign);
        ps.setObject(++index, employee_postingDTO.post);
        ps.setObject(++index, employee_postingDTO.designation);
        ps.setObject(++index, employee_postingDTO.country);
        ps.setObject(++index, employee_postingDTO.place);
        ps.setObject(++index, employee_postingDTO.organization);
        ps.setObject(++index, employee_postingDTO.postingFrom);
        ps.setObject(++index, employee_postingDTO.postingTo);
        ps.setObject(++index, employee_postingDTO.payScale);
        ps.setObject(++index, employee_postingDTO.insertedByUserId);
        ps.setObject(++index, employee_postingDTO.insertionDate);
        ps.setObject(++index, employee_postingDTO.modifiedBy);
        ps.setObject(++index, employee_postingDTO.searchColumn);
        ps.setObject(++index, System.currentTimeMillis());
        ps.setObject(++index, employee_postingDTO.designationForeign);
        if (isInsert) {
            ps.setObject(++index, 0);
        }
        ps.setObject(++index, employee_postingDTO.iD);
    }

    private void setSearchColumn(Employee_postingDTO employee_postingDTO) {
        List<String> list = new ArrayList<>();
        try {
            Employee_recordsDTO employeeRecordsDTO = new Employee_recordsDAO().getEmployee_recordsDTOByID(employee_postingDTO.employeeRecordsId);
            if (employeeRecordsDTO != null) {
                if (employeeRecordsDTO.nameEng != null && employeeRecordsDTO.nameEng.trim().length() > 0) {
                    list.add(employeeRecordsDTO.nameEng.trim());
                }
                if (employeeRecordsDTO.nameBng != null && employeeRecordsDTO.nameBng.trim().length() > 0) {
                    list.add(employeeRecordsDTO.nameBng.trim());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        employee_postingDTO.searchColumn = String.join(" ", list);

    }

    @Override
    public Employee_postingDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Employee_postingDTO employee_postingDTO = new Employee_postingDTO();
            employee_postingDTO.iD = rs.getLong("ID");
            employee_postingDTO.employeeRecordsId = rs.getLong("employee_records_id");
            employee_postingDTO.localOrForeign = rs.getInt("local_or_foreign");
            employee_postingDTO.post = rs.getLong("post");
            employee_postingDTO.designation = rs.getLong("designation");
            employee_postingDTO.country = rs.getLong("country");
            employee_postingDTO.place = rs.getLong("place");
            employee_postingDTO.organization = rs.getString("organization");
            employee_postingDTO.postingFrom = rs.getLong("posting_from");
            employee_postingDTO.postingTo = rs.getLong("posting_to");
            employee_postingDTO.payScale = rs.getString("pay_scale");
            employee_postingDTO.insertedByUserId = rs.getLong("inserted_by_user_id");
            employee_postingDTO.insertionDate = rs.getLong("insertion_date");
            employee_postingDTO.modifiedBy = rs.getString("modified_by");
            employee_postingDTO.searchColumn = rs.getString("search_column");
            employee_postingDTO.designationForeign = rs.getString("designation_foreign");
            employee_postingDTO.isDeleted = rs.getInt("isDeleted");
            employee_postingDTO.lastModificationTime = rs.getLong("lastModificationTime");
            return employee_postingDTO;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    @Override
    public String getTableName() {
        return "employee_posting";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    @Override
    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Employee_postingDTO) commonDTO, addQuery, true);
    }

    @Override
    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Employee_postingDTO) commonDTO, updateQuery, false);
    }

    public List<Employee_postingDTO> getLocalPostingDTOsByEmployeeId(long employeeId) {
        return getByEmployeeId(employeeId).stream().filter(dto -> dto.localOrForeign == PostingTypeEnum.LOCAL.getValue()).collect(Collectors.toList());
    }

    public List<Employee_postingDTO> getForeignPostingDTOsByEmployeeId(long employeeId) {
        return getByEmployeeId(employeeId).stream().filter(dto -> dto.localOrForeign == PostingTypeEnum.FOREIGN.getValue()).collect(Collectors.toList());
    }

    public List<Employee_local_posting_model> getAllLocalPostingModelByEmployeeId(long employeeRecordsId) throws Exception {
        return getLocalPostingDTOsByEmployeeId(employeeRecordsId)
                .stream()
                .map(this::buildLocalPostingModel)
                .collect(Collectors.toList());
    }

    private Employee_local_posting_model buildLocalPostingModel(Employee_postingDTO dto) {
        Employee_local_posting_model model = new Employee_local_posting_model();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        model.dto = dto;

        Office_unitsDTO officeUnit = Office_unitsDAO.getInstance().getDTOFromID(dto.post);
        if (officeUnit != null) {
            model.postEng = officeUnit.unitNameEng;
            model.postBng = officeUnit.unitNameBng;
        }

        OfficeUnitOrganograms org = OfficeUnitOrganogramsRepository.getInstance().getById(dto.designation);
        if (org != null) {
            model.designationEng = org.designation_eng;
            model.designationBng = org.designation_bng;
        }

        model.postingFromEng = dateFormat.format(new Date(dto.postingFrom));
        model.postingFromBng = StringUtils.convertToBanNumber(dateFormat.format(new Date(dto.postingFrom)));
        model.postingToEng = dateFormat.format(new Date(dto.postingTo));
        model.postingToBng = StringUtils.convertToBanNumber(dateFormat.format(new Date(dto.postingTo)));

        return getPayscaleFromDesignation(model);
    }


    private Employee_local_posting_model getPayscaleFromDesignation(Employee_local_posting_model model) {
        Connection connection = null;
        ResultSet rs;
        Statement stmt = null;

        try {

            final String getOptionSQL = "select job_grade_cat, national_pay_scale_cat from employee_pay_scale where ID= (select payscale_id from designation_to_payscale_map where designation_bng='" + model.designationBng + "' or designation_eng='" + model.designationEng + "')";

            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();
            logger.debug(getOptionSQL);

            rs = stmt.executeQuery(getOptionSQL);
            if (rs.next()) {
                int job_grade_cat, national_pay_scale_cat;
                CategoryLanguageModel jobModel, nationalPayModel;
                job_grade_cat = rs.getInt("job_grade_cat");
                national_pay_scale_cat = rs.getInt("national_pay_scale_cat");

                jobModel = CatRepository.getInstance().getCategoryLanguageModel("job_grade_type", job_grade_cat);
                nationalPayModel = CatRepository.getInstance().getCategoryLanguageModel("national_pay_scale_type", national_pay_scale_cat);

                model.payScaleEng = jobModel.englishText + " (" + nationalPayModel.englishText + ")";
                model.payScaleBng = jobModel.banglaText + " (" + nationalPayModel.banglaText + ")";
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception ignored) {
            }

            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ignored) {
            }
        }
        return model;
    }
}
	