package employee_posting;

/*
 * @author Md. Erfan Hossain
 * @created 23/08/2021 - 11:22 AM
 * @project parliament
 */

import pb.OptionDTO;
import pb.Utils;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public enum PostingTypeEnum {
    LOCAL(0,"Local","স্থানীয়"),
    FOREIGN(1,"Foreign","বিদেশী");

    private final int value;
    private final String engText, bngText;
    private static String [] mapByLang = null;

    PostingTypeEnum(int value,String engText,String bngText){
        this.value = value;
        this.engText = engText;
        this.bngText = bngText;
    }

    public int getValue() {
        return value;
    }

    public String getEngText() {
        return engText;
    }

    public String getBngText() {
        return bngText;
    }

    public static PostingTypeEnum getByValue(int value){
        switch (value){
            case 0:
                return LOCAL;
            case 1:
                return FOREIGN;
            default:
                return null;
        }
    }

    public static String getTextByValue(int value,boolean isLangEng){
        switch (value){
            case 0:
                return isLangEng? LOCAL.engText : LOCAL.bngText;
            case 1:
                return isLangEng? FOREIGN.engText : FOREIGN.bngText;
            default:
                return "";
        }
    }

    public static String getTextByValue(int value,String language){
        return getTextByValue(value,"English".equalsIgnoreCase(language));
    }

    public static String buildOptions(boolean isLangEng){
        if(mapByLang == null){
            synchronized (PostingTypeEnum.class){
                if(mapByLang == null){
                    List<OptionDTO> optionDTOList = Stream.of(PostingTypeEnum.values())
                            .map(e->new OptionDTO(e.engText,e.bngText,String.valueOf(e.value)))
                            .collect(Collectors.toList());
                    mapByLang = new String[2];
                    mapByLang[0] =  Utils.buildOptions(optionDTOList,"English",null);
                    mapByLang[1] =  Utils.buildOptions(optionDTOList,"Bangla",null);
                }
            }
        }
        return isLangEng?mapByLang[0] : mapByLang[1];
    }
}