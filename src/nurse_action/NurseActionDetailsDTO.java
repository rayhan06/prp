package nurse_action;
import java.util.*;

import medical_emergency_request.Medical_emergency_requestDTO;
import user.UserDTO;
import user.UserRepository;
import util.*; 


public class NurseActionDetailsDTO extends CommonDTO
{

	public long nurseActionId = -1;
	public int nurseActionCat = -1;
    public String description = "";
    
    public String patientName = "";
    public String nurseUserName = "";
    public String patientUserName = "";
    public long patientOrganogramId = -1;
    public long nurseOrganogramId = -1;
    public long actionDate = -1;
    public long nurseEmployeeRecordId = -1;
    
    public long merId = -1;
    
    public int stripCount = 0;
    public static final int ACTION_BLOOD_SUGAR = 1;
    public static final int CALL_ATTEND = 14;
	
	public List<NurseActionDetailsDTO> nurseActionDetailsDTOList = new ArrayList<>();
	
	public NurseActionDetailsDTO()
	{
		
	}
	
	public NurseActionDetailsDTO(Nurse_actionDTO nurse_actionDTO)
	{
		nurseActionId = nurse_actionDTO.iD;
		nurseActionCat = CALL_ATTEND;
		patientName = nurse_actionDTO.name;
		nurseUserName = nurse_actionDTO.nurseUserId;
		UserDTO nurseDTO = UserRepository.getUserDTOByUserName(nurseUserName);
		if(nurseDTO != null)
		{
			nurseEmployeeRecordId= nurseDTO.employee_record_id;
		}
		patientUserName = nurse_actionDTO.patientUserId;
		patientOrganogramId = nurse_actionDTO.patientOrganogramId;
		nurseOrganogramId = nurse_actionDTO.nurseOrganogramId;
		actionDate = nurse_actionDTO.actionDate;
		merId = nurse_actionDTO.merId;
	}
	
    @Override
	public String toString() {
            return "$NurseActionDetailsDTO[" +
            " iD = " + iD +
            " nurseActionId = " + nurseActionId +
            " nurseActionCat = " + nurseActionCat +
            " description = " + description +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}