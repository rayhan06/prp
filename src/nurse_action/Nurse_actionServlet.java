package nurse_action;


import java.io.IOException;
import java.text.SimpleDateFormat;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import org.apache.log4j.Logger;
import permission.MenuConstants;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.*;
import workflow.WorkflowController;

import javax.servlet.http.*;
import java.util.*;
import language.LC;
import language.LM;
import medical_inventory_out.Medical_inventory_outDAO;

import other_office.Other_officeRepository;
import common.ApiResponse;
import common.BaseServlet;
import common.NameDTO;
import department_requisition_lot.Department_requisition_lotDTO;
import drug_information.Drug_informationDAO;
import borrow_medicine.*;


import drug_information.Drug_informationDTO;
import employee_records.Employee_recordsDTO;
import employee_records.Employee_recordsRepository;
import employee_records.EmploymentStatusEnum;

import com.google.gson.Gson;

import appointment.AppointmentDAO;
import appointment.AppointmentDTO;
import pb.*;


import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;




/**
 * Servlet implementation class Nurse_actionServlet
 */
@WebServlet("/Nurse_actionServlet")
@MultipartConfig
public class Nurse_actionServlet extends BaseServlet 
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Nurse_actionServlet.class);
    Drug_informationDAO drug_informationDAO = new Drug_informationDAO();
    Medical_inventory_outDAO medical_inventory_outDAO = new Medical_inventory_outDAO();
    PersonalStockDAO personalStockDAO = PersonalStockDAO.getInstance();
    @Override
    public String getTableName() {
        return Nurse_actionDAO.getInstance().getTableName();
    }

    @Override
    public String getServletName() {
        return "Nurse_actionServlet";
    }

    @Override
    public Nurse_actionDAO getCommonDAOService() {
        return Nurse_actionDAO.getInstance();
    }
    
    @Override
    public int[] getAddPageMenuConstants() {
        return new int[] {MenuConstants.NURSE_ACTION_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[] {MenuConstants.NURSE_ACTION_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[] {MenuConstants.NURSE_ACTION_SEARCH};
    }
    
    @Override
    public Map<String, String> buildRequestParams(HttpServletRequest request) {
    	 Enumeration<String> paramList = request.getParameterNames();
         Map<String,String> params = new HashMap<>();
         while (paramList.hasMoreElements()){
             String paramName = paramList.nextElement();
             String paramValue = request.getParameter(paramName);
             if(Utils.isValidSearchString(paramValue)){
            	 if(paramName.equalsIgnoreCase("patient_user_id"))
            	 {
            		 params.put(paramName, WorkflowController.getEmployeeRecordsIdFromUserName(paramValue) + "");
            	 }
            	 else
            	 {
            		 params.put(paramName,paramValue);
            	 }
                 
             }
         }
         return params;
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Nurse_actionServlet.class;
    }
	NurseActionDetailsDAO nurseActionDetailsDAO = NurseActionDetailsDAO.getInstance();
    private final Gson gson = new Gson();
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        CommonLoginData commonLoginData = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get();
        init(request);
        String actionType = request.getParameter("actionType");
		if ("viewByAppointmentId".equals(actionType)) {
			try {
				long appointmentId = Long.parseLong(request.getParameter("appointmentId"));
				long nurseDTOId = Nurse_actionDAO.getInstance().getIDByAppointmentId(appointmentId);
				if(nurseDTOId != -1)
				{
					request.getRequestDispatcher(commonPartOfDispatchURL() + "View.jsp?ID=" + nurseDTOId).forward(request,response);
				}
			} catch (Exception ex) {
				logger.error(ex);
				ApiResponse.sendErrorResponse(response, ex.getMessage());
			}
			return;
		}
		else if ("viewTodaysActions".equals(actionType)) {
			try {
				
			} catch (Exception ex) {
				logger.error(ex);
				ApiResponse.sendErrorResponse(response, ex.getMessage());
			}
			return;
		}
		else
		{
			super.doGet(request,response);
		}
    }
    
    @Override
    public String getWhereClause(HttpServletRequest request){
    	CommonLoginData commonLoginData = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get();
    	if(commonLoginData.userDTO.roleID == SessionConstants.DOCTOR_ROLE
    		||commonLoginData.userDTO.roleID == SessionConstants.NURSE_ROLE
    		||commonLoginData.userDTO.roleID == SessionConstants.ADMIN_ROLE
    		||commonLoginData.userDTO.roleID == SessionConstants.MEDICAL_ADMIN_ROLE
    		|| commonLoginData.userDTO.roleID == SessionConstants.MEDICAL_RECEPTIONIST_ROLE)
    	{
    		return null;
    	}
    	else
    	{
    		return " and patient_user_id = '" + commonLoginData.userDTO.userName + "'";
    	}
    }
 	

	@Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception 
	{
		// TODO Auto-generated method stub	
		request.setAttribute("failureMessage", "");
		System.out.println("%%%% addNurse_action");
		String language = userDTO.languageID == SessionConstants.ENGLISH? "English":"Bangla";
		Nurse_actionDTO nurse_actionDTO;
		SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
					
		if(addFlag == true)
		{
			nurse_actionDTO = new Nurse_actionDTO();
		}
		else
		{
			nurse_actionDTO = Nurse_actionDAO.getInstance().getDTOFromID(Long.parseLong(request.getParameter("iD")));
		}
		
		String Value = "";

		Value = request.getParameter("nurseOrganogramId");

		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("nurseOrganogramId = " + Value);
		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			nurse_actionDTO.nurseOrganogramId = Long.parseLong(Value);
			UserDTO nurseDTO = UserRepository.getUserDTOByOrganogramID(nurse_actionDTO.nurseOrganogramId);
			if(nurseDTO == null)
			{
				throw new Exception(ErrorMessage.getInvalidMessage(language));
			}
			nurse_actionDTO.nurseUserId = nurseDTO.userName;
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}

	
		Value = request.getParameter("appointmentId");

		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("appointmentId = " + Value);
		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			nurse_actionDTO.appointmentId = Long.parseLong(Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}
		
		if(nurse_actionDTO.appointmentId != -1)
		{
			AppointmentDAO appointmentDAO = new AppointmentDAO();
			AppointmentDTO appointmentDTO = appointmentDAO.getDTOByID(nurse_actionDTO.appointmentId);
			nurse_actionDTO.patientOrganogramId = appointmentDTO.patientId;
			nurse_actionDTO.patientUserId = appointmentDTO.employeeUserName;
			nurse_actionDTO.name = appointmentDTO.patientName;
			nurse_actionDTO.phone = appointmentDTO.phoneNumber;
			nurse_actionDTO.whoIsThePatientCat = appointmentDTO.whoIsThePatientCat;
			nurse_actionDTO.othersDesignationAndId = appointmentDTO.othersDesignationAndId;
			nurse_actionDTO.othersOfficeId = appointmentDTO.othersOfficeId;
			nurse_actionDTO.othersOfficeEn = appointmentDTO.othersOfficeEn;
			nurse_actionDTO.othersOfficeBn = appointmentDTO.othersOfficeBn;
		}
		else
		{
			Value = request.getParameter("patientUserId");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("patientUserId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				nurse_actionDTO.patientUserId = (Value);
				Employee_recordsDTO erDTO = Employee_recordsRepository.getInstance().getByUserNameOrPhone(nurse_actionDTO.patientUserId, "");
				if(erDTO != null)
				{
					if(erDTO.employmentStatus != EmploymentStatusEnum.ACTIVE.getValue()
							&& (erDTO.employmentStatus != EmploymentStatusEnum.RELEASED.getValue()))
					{
						return null;
					}
				}
				UserDTO patientDTO = UserRepository.getUserDTOByUserName(nurse_actionDTO.patientUserId);
				if(patientDTO != null)
				{
					nurse_actionDTO.patientOrganogramId = patientDTO.organogramID;
				}			
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			
			
			Value = request.getParameter("whoIsThePatientCat");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("whoIsThePatientCat = " + Value);
			if(Value != null)
			{
				nurse_actionDTO.whoIsThePatientCat = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

		
			Value = request.getParameter("name");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("name = " + Value);
			if(Value != null)
			{
				nurse_actionDTO.name = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			
			Value = request.getParameter("othersOfficeId");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("othersOfficeId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				nurse_actionDTO.othersOfficeId = Long.parseLong(Value);
				if(nurse_actionDTO.othersOfficeId != -1)
				{
					NameDTO otherOfficeDTO = Other_officeRepository.getInstance().getDTOByID(nurse_actionDTO.othersOfficeId);
					if(otherOfficeDTO != null)
					{
						nurse_actionDTO.othersOfficeBn = otherOfficeDTO.nameBn;
						nurse_actionDTO.othersOfficeEn = otherOfficeDTO.nameEn;
					}
				}
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			
			Value = request.getParameter("othersDesignationAndId");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("othersDesignationAndId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				
				nurse_actionDTO.othersDesignationAndId = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}


			Value = request.getParameter("phone");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("phone = " + Value);
			if(Value != null)
			{
				nurse_actionDTO.phone = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
		}

		Value = request.getParameter("actionDate");

		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("actionDate = " + Value);
		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			try 
			{
				Date d = f.parse(Value);
				nurse_actionDTO.actionDate = d.getTime();
			}
			catch (Exception e) 
			{
				e.printStackTrace();
			}
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}

		if(addFlag)
		{
			nurse_actionDTO.insertedByUserId = userDTO.ID;
		}


		if(addFlag)
		{
			nurse_actionDTO.insertedByOrganogramId = userDTO.organogramID;
		}


		if(addFlag)
		{				
			nurse_actionDTO.insertionDate = TimeConverter.getToday();
		}			


		nurse_actionDTO.lastModifierUser = userDTO.userName;

		
		System.out.println("Done adding  addNurse_action dto = " + nurse_actionDTO);
		long returnedID = -1;
		
		List<NurseActionDetailsDTO> nurseActionDetailsDTOList = createNurseActionDetailsDTOListByRequest(request, language, nurse_actionDTO, userDTO);
		
		if(addFlag == true)
		{
			returnedID = Nurse_actionDAO.getInstance().add(nurse_actionDTO);
		}
		else
		{				
			returnedID = Nurse_actionDAO.getInstance().update(nurse_actionDTO);										
		}
		
	
		if(addFlag == true) //add or validate
		{
			if(nurseActionDetailsDTOList != null)
			{				
				for(NurseActionDetailsDTO nurseActionDetailsDTO: nurseActionDetailsDTOList)
				{
					nurseActionDetailsDTO.nurseActionId = nurse_actionDTO.iD; 
					nurseActionDetailsDAO.add(nurseActionDetailsDTO);
				}
			}
		
		}			
				
		return nurse_actionDTO;

	}
	private List<NurseActionDetailsDTO> createNurseActionDetailsDTOListByRequest(HttpServletRequest request, String language, Nurse_actionDTO nurse_actionDTO, UserDTO userDTO) throws Exception{ 
		List<NurseActionDetailsDTO> nurseActionDetailsDTOList = new ArrayList<NurseActionDetailsDTO>();
		
		int rowCount = request.getParameterValues("isDone").length;
		for(int index=0;index<rowCount;index++){
			if(request.getParameterValues("isDone")[index].equalsIgnoreCase("true"))
			{
				System.out.println("adding details");
				NurseActionDetailsDTO nurseActionDetailsDTO = createNurseActionDetailsDTOByRequestAndIndex(request,true,index, language, nurse_actionDTO, userDTO);
				nurseActionDetailsDTOList.add(nurseActionDetailsDTO);
			}
		}
		
		return nurseActionDetailsDTOList;

	}
	
	private NurseActionDetailsDTO createNurseActionDetailsDTOByRequestAndIndex(HttpServletRequest request, boolean addFlag, int index, String language,
			Nurse_actionDTO nurse_actionDTO, UserDTO userDTO) throws Exception{
	
		NurseActionDetailsDTO nurseActionDetailsDTO;
		if(addFlag == true )
		{
			nurseActionDetailsDTO = new NurseActionDetailsDTO(nurse_actionDTO);
		}
		else
		{
			nurseActionDetailsDTO = nurseActionDetailsDAO.getDTOByID(Long.parseLong(request.getParameterValues("nurseActionDetails.iD")[index]));
		}

		String Value = request.getParameterValues("nurseActionDetails.nurseActionCat")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
			nurseActionDetailsDTO.nurseActionCat = Integer.parseInt(Value);
		}
		else
		{
			throw new Exception(LM.getText(LC.NURSE_ACTION_ADD_NURSE_ACTION_DETAILS_NURSEACTIONCAT, language) + " " + ErrorMessage.getInvalidMessage(language));
		}

		
		Value = request.getParameterValues("nurseActionDetails.description")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
			nurseActionDetailsDTO.description = (Value);
		}
		
		Value = request.getParameterValues("nurseActionDetails.stripCount")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
			nurseActionDetailsDTO.stripCount = Integer.parseInt(Value);
		}
		

		
		/*nurseActionDetailsDTO.nurseActionId = nurse_actionDTO.iD;
		nurseActionDetailsDTO.actionDate = nurse_actionDTO.actionDate;
		nurseActionDetailsDTO.nurseOrganogramId = userDTO.organogramID;
		nurseActionDetailsDTO.nurseUserName = userDTO.userName;
		nurseActionDetailsDTO.patientOrganogramId = nurse_actionDTO.patientOrganogramId;
		nurseActionDetailsDTO.patientUserName = nurse_actionDTO.patientUserId;
		nurseActionDetailsDTO.patientName = nurse_actionDTO.name;*/
		
		if(nurseActionDetailsDTO.nurseActionCat == NurseActionDetailsDTO.ACTION_BLOOD_SUGAR && nurseActionDetailsDTO.stripCount > 0)
		{
			PersonalStockDTO accuDTO = personalStockDAO.getDTOByDeptAndDrugId((int)Department_requisition_lotDTO.EMERGENCY, Drug_informationDTO.ACCU_CHECK_ID, SessionConstants.MEDICAL_ITEM_DRUG);
			if(accuDTO != null)
			{
				logger.debug("accuDTO.detailedCount = " + accuDTO.detailedCount 
						+ " nurseActionDetailsDTO.stripCount = " + nurseActionDetailsDTO.stripCount);
				if(accuDTO.detailedCount - nurseActionDetailsDTO.stripCount >= 0)
				{
					int boxUsedCount = (accuDTO.detailedCount /Drug_informationDTO.ACCU_SIZE) - ((accuDTO.detailedCount - nurseActionDetailsDTO.stripCount) /Drug_informationDTO.ACCU_SIZE);
					logger.debug("boxUsedCount = " + boxUsedCount);
					accuDTO.detailedCount -= nurseActionDetailsDTO.stripCount;	
					accuDTO.quantity -= boxUsedCount;	
					personalStockDAO.update(accuDTO);
					
				}
				else
				{
					throw new Exception(ErrorMessage.getInvalidMessage(language));
				
				}
				
			}
		}
		return nurseActionDetailsDTO;
	
	}	
}

