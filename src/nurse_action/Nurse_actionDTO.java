package nurse_action;
import java.util.*;

import family.FamilyDTO;
import medical_emergency_request.Medical_emergency_requestDTO;
import user.UserDTO;
import user.UserRepository;
import util.*; 


public class Nurse_actionDTO extends CommonDTO
{

	public long nurseOrganogramId = -1;
    public String nurseUserId = "";
	public long appointmentId = -1;
	public long patientOrganogramId = -1;
    public String patientUserId = "";
    public String name = "";
    public String phone = "";
	public long actionDate = TimeConverter.getToday();
	public long insertedByUserId = -1;
	public long insertedByOrganogramId = -1;
	public long insertionDate = -1;
    public String lastModifierUser = "";
    
    public String othersOfficeBn = "";
	public String othersOfficeEn = "";
	public long othersOfficeId = -1;
	public String othersDesignationAndId = "";
	public int whoIsThePatientCat =FamilyDTO.OWN;
	
	public long merId = -1;
	
	public List<NurseActionDetailsDTO> nurseActionDetailsDTOList = new ArrayList<>();
	
	public Nurse_actionDTO()
	{
		
	}
	
	public Nurse_actionDTO(Medical_emergency_requestDTO medical_emergency_requestDTO)
	{
		merId = medical_emergency_requestDTO.iD;
		nurseOrganogramId = medical_emergency_requestDTO.nurseOrganogramId;
		UserDTO nurseDTO = UserRepository.getUserDTOByUserID(medical_emergency_requestDTO.nurseUserId);
		if(nurseDTO != null)
		{
			nurseUserId = nurseDTO.userName;
			lastModifierUser = nurseDTO.userName;
		}
		insertedByUserId = medical_emergency_requestDTO.insertedByUserId;
		insertedByOrganogramId = medical_emergency_requestDTO.insertedByOrganogramId;
		insertionDate = medical_emergency_requestDTO.serviceDate;
		actionDate = insertionDate;
		
		UserDTO patientDTO = UserRepository.getUserDTOByUserID(medical_emergency_requestDTO.employeeUserId);
		if(patientDTO != null)
		{
			patientUserId = patientDTO.userName;
			patientOrganogramId = patientDTO.organogramID;
		}		
		name = medical_emergency_requestDTO.patientName;
		phone = medical_emergency_requestDTO.phoneNumber;
		
	}
	
    @Override
	public String toString() {
            return "$Nurse_actionDTO[" +
            " iD = " + iD +
            " nurseOrganogramId = " + nurseOrganogramId +
            " nurseUserId = " + nurseUserId +
            " appointmentId = " + appointmentId +
            " patientOrganogramId = " + patientOrganogramId +
            " patientUserId = " + patientUserId +
            " name = " + name +
            " phone = " + phone +
            " actionDate = " + actionDate +
            " searchColumn = " + searchColumn +
            " insertedByUserId = " + insertedByUserId +
            " insertedByOrganogramId = " + insertedByOrganogramId +
            " insertionDate = " + insertionDate +
            " lastModifierUser = " + lastModifierUser +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}