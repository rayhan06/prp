package nurse_action;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.sql.SQLException;
import common.CommonDAOService;
import common.ConnectionAndStatementUtil;

import org.apache.log4j.Logger;

import appointment.KeyCountDTO;
import util.*;
import pb.*;

public class NurseActionDetailsDAO  implements CommonDAOService<NurseActionDetailsDTO>
{
	
	Logger logger = Logger.getLogger(getClass());
	
	private static String addQuery = "";
	private static String updateQuery = "";
	private String[] columnNames = null;

	private NurseActionDetailsDAO()
	{
		columnNames = new String[] 
		{
			"ID",
			"nurse_action_id",
			"nurse_action_cat",
			"description",
			"nurse_user_name",
			"nurse_employee_record_id",
			"patient_user_name",
			"patient_name",
			"nurse_organogram_id",
			"patient_organogram_id",
			"action_date",
			"strip_count",
			"mer_id",
			"isDeleted",
			"lastModificationTime"
		};
		updateQuery = getUpdateQuery(columnNames);
        addQuery = getAddQuery(columnNames);
		
		searchMap.put("nurse_action_cat"," and (nurse_action_cat = ?)");
		searchMap.put("description"," and (description like ?)");
		searchMap.put("AnyField"," and (search_column like ?)");
	}

	private final Map<String,String> searchMap = new HashMap<>();

    private static class LazyLoader
    {
    	static final NurseActionDetailsDAO INSTANCE = new NurseActionDetailsDAO();
	}

	public static NurseActionDetailsDAO getInstance()
	{
    	return LazyLoader.INSTANCE;
	}
	
	public void setSearchColumn(NurseActionDetailsDTO nurseactiondetailsDTO)
	{
		nurseactiondetailsDTO.searchColumn = "";
		nurseactiondetailsDTO.searchColumn += CatDAO.getName("English", "nurse_action", nurseactiondetailsDTO.nurseActionCat) + " " + CatDAO.getName("Bangla", "nurse_action", nurseactiondetailsDTO.nurseActionCat) + " ";
		nurseactiondetailsDTO.searchColumn += nurseactiondetailsDTO.description + " ";
	}
	
	public List<KeyCountDTO> getLast7DayCount (String nurseUserName)
    {
		String sql = "SELECT action_date, count(id) FROM " + getTableName() + " where action_date >=" + TimeConverter.getNthDay(-7) + " and isDeleted = 0";
		if(!nurseUserName.equalsIgnoreCase(""))
		{
			sql+= " and nurse_user_name = '" + nurseUserName + "'";
		}
		sql+= " group by action_date order by action_date asc";
		
		return ConnectionAndStatementUtil.getListOfT(sql,this::getCount);	
    }
	
	public KeyCountDTO getCount(ResultSet rs)
	{
		try
		{
			KeyCountDTO keyCountDTO = new KeyCountDTO();
			keyCountDTO.key = rs.getLong("action_date");
			keyCountDTO.count = rs.getInt("count(id)");

			return keyCountDTO;
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
			return null;
		}
	}
	
	public List<KeyCountDTO> getActionWiseCount (String nurseUserName)
    {
		String sql = "SELECT nurse_action_cat, count(id) FROM " + getTableName() + " where " +  " isDeleted = 0";
		if(!nurseUserName.equalsIgnoreCase(""))
		{
			sql+= " and nurse_user_name = '" + nurseUserName + "'";
		}
		sql+= " group by nurse_action_cat order by nurse_action_cat asc";
		
		return ConnectionAndStatementUtil.getListOfT(sql,this::getActionCount);	
    }
	
	public KeyCountDTO getActionCount(ResultSet rs)
	{
		try
		{
			KeyCountDTO keyCountDTO = new KeyCountDTO();
			keyCountDTO.key = rs.getLong("nurse_action_cat");
			keyCountDTO.count = rs.getInt("count(id)");

			return keyCountDTO;
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
			return null;
		}
	}
	
	@Override
	public void set(PreparedStatement ps, NurseActionDetailsDTO nurseactiondetailsDTO, boolean isInsert) throws SQLException
	{
		int index = 0;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(nurseactiondetailsDTO);
		if(isInsert)
		{
			ps.setObject(++index,nurseactiondetailsDTO.iD);
		}
		ps.setObject(++index,nurseactiondetailsDTO.nurseActionId);
		ps.setObject(++index,nurseactiondetailsDTO.nurseActionCat);
		ps.setObject(++index,nurseactiondetailsDTO.description);
		
		ps.setObject(++index,nurseactiondetailsDTO.nurseUserName);
		ps.setObject(++index,nurseactiondetailsDTO.nurseEmployeeRecordId);
		ps.setObject(++index,nurseactiondetailsDTO.patientUserName);
		ps.setObject(++index,nurseactiondetailsDTO.patientName);
		
		ps.setObject(++index,nurseactiondetailsDTO.nurseOrganogramId);
		ps.setObject(++index,nurseactiondetailsDTO.patientOrganogramId);
		ps.setObject(++index,nurseactiondetailsDTO.actionDate);
		
		ps.setObject(++index,nurseactiondetailsDTO.stripCount);
		ps.setObject(++index,nurseactiondetailsDTO.merId);
		
		
		if(isInsert)
		{
			ps.setObject(++index,nurseactiondetailsDTO.isDeleted);
		}
		ps.setObject(++index, lastModificationTime);
		if(!isInsert)
		{
			ps.setObject(++index,nurseactiondetailsDTO.iD);
		}
	}
	
	@Override
	public NurseActionDetailsDTO buildObjectFromResultSet(ResultSet rs)
	{
		try
		{
			NurseActionDetailsDTO nurseactiondetailsDTO = new NurseActionDetailsDTO();
			int i = 0;
			nurseactiondetailsDTO.iD = rs.getLong(columnNames[i++]);
			nurseactiondetailsDTO.nurseActionId = rs.getLong(columnNames[i++]);
			nurseactiondetailsDTO.nurseActionCat = rs.getInt(columnNames[i++]);
			nurseactiondetailsDTO.description = rs.getString(columnNames[i++]);
			
			nurseactiondetailsDTO.nurseUserName = rs.getString(columnNames[i++]);
			nurseactiondetailsDTO.nurseEmployeeRecordId = rs.getLong(columnNames[i++]);
			nurseactiondetailsDTO.patientUserName = rs.getString(columnNames[i++]);
			nurseactiondetailsDTO.patientName = rs.getString(columnNames[i++]);
			
			nurseactiondetailsDTO.nurseOrganogramId = rs.getLong(columnNames[i++]);
			nurseactiondetailsDTO.patientOrganogramId = rs.getLong(columnNames[i++]);
			nurseactiondetailsDTO.actionDate = rs.getLong(columnNames[i++]);
			
			nurseactiondetailsDTO.stripCount = rs.getInt(columnNames[i++]);
			nurseactiondetailsDTO.merId = rs.getLong(columnNames[i++]);
			
			nurseactiondetailsDTO.isDeleted = rs.getInt(columnNames[i++]);
			nurseactiondetailsDTO.lastModificationTime = rs.getLong(columnNames[i++]);
			return nurseactiondetailsDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
		
	public NurseActionDetailsDTO getDTOByID (long id)
	{
		return getDTOFromID(id);
	}

	@Override
	public String getTableName() {
		return "nurse_action_details";
	}

	@Override
	public Map<String, String> getSearchMap() {
		return searchMap;
	}

	public long add(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((NurseActionDetailsDTO) commonDTO,addQuery,true);
	}

	public long update(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((NurseActionDetailsDTO) commonDTO,updateQuery,false);
	}
	
	@Override
	public String getLastModifierUser(){
		return "";
    }
				
}
	