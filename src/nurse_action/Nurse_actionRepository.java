package nurse_action;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import com.google.gson.Gson;

import repository.Repository;
import repository.RepositoryManager;


public class Nurse_actionRepository implements Repository {
	Nurse_actionDAO nurse_actionDAO = null;
	
	static Logger logger = Logger.getLogger(Nurse_actionRepository.class);
	Map<Long, Nurse_actionDTO>mapOfNurse_actionDTOToiD;
	Gson gson;

  
	private Nurse_actionRepository(){
		nurse_actionDAO = Nurse_actionDAO.getInstance();
		mapOfNurse_actionDTOToiD = new ConcurrentHashMap<>();
		gson = new Gson();
		RepositoryManager.getInstance().addRepository(this);
	}

	private static class LazyLoader{
        final static Nurse_actionRepository INSTANCE = new Nurse_actionRepository();
    }

    public static Nurse_actionRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

	public void reload(boolean reloadAll){
		try {
			List<Nurse_actionDTO> nurse_actionDTOs = nurse_actionDAO.getAllDTOs(reloadAll);
			for(Nurse_actionDTO nurse_actionDTO : nurse_actionDTOs) {
				Nurse_actionDTO oldNurse_actionDTO = getNurse_actionDTOByiD(nurse_actionDTO.iD);
				if( oldNurse_actionDTO != null ) {
					mapOfNurse_actionDTOToiD.remove(oldNurse_actionDTO.iD);
				
					
				}
				if(nurse_actionDTO.isDeleted == 0) 
				{
					
					mapOfNurse_actionDTOToiD.put(nurse_actionDTO.iD, nurse_actionDTO);
				
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public Nurse_actionDTO clone(Nurse_actionDTO dto) {
		String raw = gson.toJson(dto);
		return gson.fromJson(raw, Nurse_actionDTO.class);
	}
	
	
	public List<Nurse_actionDTO> getNurse_actionList() {
		List <Nurse_actionDTO> nurse_actions = new ArrayList<Nurse_actionDTO>(this.mapOfNurse_actionDTOToiD.values());
		return nurse_actions;
	}
	
	public List<Nurse_actionDTO> copyNurse_actionList() {
		List <Nurse_actionDTO> nurse_actions = getNurse_actionList();
		return nurse_actions
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}
	
	
	
	public Nurse_actionDTO getNurse_actionDTOByiD( long iD){
		return mapOfNurse_actionDTOToiD.get(iD);
	}
	
	public Nurse_actionDTO copyNurse_actionDTOByiD( long iD){
		return clone(mapOfNurse_actionDTOToiD.get(iD));
	}

	
	@Override
	public String getTableName() {
		return nurse_actionDAO.getTableName();
	}
}


