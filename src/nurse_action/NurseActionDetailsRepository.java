package nurse_action;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import com.google.gson.Gson;

import repository.Repository;
import repository.RepositoryManager;


public class NurseActionDetailsRepository implements Repository {
	NurseActionDetailsDAO nurseactiondetailsDAO = null;
	
	static Logger logger = Logger.getLogger(NurseActionDetailsRepository.class);
	Map<Long, NurseActionDetailsDTO>mapOfNurseActionDetailsDTOToiD;
	Map<Long, Set<NurseActionDetailsDTO> >mapOfNurseActionDetailsDTOTonurseActionId;
	Gson gson;

  
	private NurseActionDetailsRepository(){
		nurseactiondetailsDAO = NurseActionDetailsDAO.getInstance();
		mapOfNurseActionDetailsDTOToiD = new ConcurrentHashMap<>();
		mapOfNurseActionDetailsDTOTonurseActionId = new ConcurrentHashMap<>();
		gson = new Gson();
		RepositoryManager.getInstance().addRepository(this);
	}

	private static class LazyLoader{
        final static NurseActionDetailsRepository INSTANCE = new NurseActionDetailsRepository();
    }

    public static NurseActionDetailsRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

	public void reload(boolean reloadAll){
		try {
			List<NurseActionDetailsDTO> nurseactiondetailsDTOs = nurseactiondetailsDAO.getAllDTOs(reloadAll);
			for(NurseActionDetailsDTO nurseactiondetailsDTO : nurseactiondetailsDTOs) {
				NurseActionDetailsDTO oldNurseActionDetailsDTO = getNurseActionDetailsDTOByiD(nurseactiondetailsDTO.iD);
				if( oldNurseActionDetailsDTO != null ) {
					mapOfNurseActionDetailsDTOToiD.remove(oldNurseActionDetailsDTO.iD);
				
					if(mapOfNurseActionDetailsDTOTonurseActionId.containsKey(oldNurseActionDetailsDTO.nurseActionId)) {
						mapOfNurseActionDetailsDTOTonurseActionId.get(oldNurseActionDetailsDTO.nurseActionId).remove(oldNurseActionDetailsDTO);
					}
					if(mapOfNurseActionDetailsDTOTonurseActionId.get(oldNurseActionDetailsDTO.nurseActionId).isEmpty()) {
						mapOfNurseActionDetailsDTOTonurseActionId.remove(oldNurseActionDetailsDTO.nurseActionId);
					}
					
					
				}
				if(nurseactiondetailsDTO.isDeleted == 0) 
				{
					
					mapOfNurseActionDetailsDTOToiD.put(nurseactiondetailsDTO.iD, nurseactiondetailsDTO);
				
					if( ! mapOfNurseActionDetailsDTOTonurseActionId.containsKey(nurseactiondetailsDTO.nurseActionId)) {
						mapOfNurseActionDetailsDTOTonurseActionId.put(nurseactiondetailsDTO.nurseActionId, new HashSet<>());
					}
					mapOfNurseActionDetailsDTOTonurseActionId.get(nurseactiondetailsDTO.nurseActionId).add(nurseactiondetailsDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public NurseActionDetailsDTO clone(NurseActionDetailsDTO dto) {
		String raw = gson.toJson(dto);
		return gson.fromJson(raw, NurseActionDetailsDTO.class);
	}
	
	
	public List<NurseActionDetailsDTO> getNurseActionDetailsList() {
		List <NurseActionDetailsDTO> nurseactiondetailss = new ArrayList<NurseActionDetailsDTO>(this.mapOfNurseActionDetailsDTOToiD.values());
		return nurseactiondetailss;
	}
	
	public List<NurseActionDetailsDTO> copyNurseActionDetailsList() {
		List <NurseActionDetailsDTO> nurseactiondetailss = getNurseActionDetailsList();
		return nurseactiondetailss
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}
	
	
	
	public NurseActionDetailsDTO getNurseActionDetailsDTOByiD( long iD){
		return mapOfNurseActionDetailsDTOToiD.get(iD);
	}
	
	public NurseActionDetailsDTO copyNurseActionDetailsDTOByiD( long iD){
		return clone(mapOfNurseActionDetailsDTOToiD.get(iD));
	}
	
	
	public List<NurseActionDetailsDTO> getNurseActionDetailsDTOBynurseActionId(long nurseActionId) {
		return new ArrayList<>( mapOfNurseActionDetailsDTOTonurseActionId.getOrDefault(nurseActionId,new HashSet<>()));
	}
	
	public List<NurseActionDetailsDTO> copyNurseActionDetailsDTOBynurseActionId(long nurseActionId)
	{
		List <NurseActionDetailsDTO> nurseactiondetailss = getNurseActionDetailsDTOBynurseActionId(nurseActionId);
		return nurseactiondetailss
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}

	
	@Override
	public String getTableName() {
		return nurseactiondetailsDAO.getTableName();
	}
}


