package nurse_action;
import java.util.*; 
import util.*;


public class NurseActionDetailsMAPS extends CommonMaps
{	
	public NurseActionDetailsMAPS(String tableName)
	{
		


		java_SQL_map.put("nurse_action_id".toLowerCase(), "nurseActionId".toLowerCase());
		java_SQL_map.put("nurse_action_cat".toLowerCase(), "nurseActionCat".toLowerCase());
		java_SQL_map.put("description".toLowerCase(), "description".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Nurse Action Id".toLowerCase(), "nurseActionId".toLowerCase());
		java_Text_map.put("Nurse Action".toLowerCase(), "nurseActionCat".toLowerCase());
		java_Text_map.put("Description".toLowerCase(), "description".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}