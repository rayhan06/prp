package nurse_action;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.sql.SQLException;
import common.CommonDAOService;
import common.ConnectionAndStatementUtil;

import org.apache.log4j.Logger;
import util.*;



public class Nurse_actionDAO  implements CommonDAOService<Nurse_actionDTO>
{
	
	Logger logger = Logger.getLogger(getClass());
	
	private static String addQuery = "";
	private static String updateQuery = "";
	private String[] columnNames = null;

	private Nurse_actionDAO()
	{
		columnNames = new String[] 
		{
			"ID",
			"nurse_organogram_id",
			"nurse_user_id",
			"appointment_id",
			"patient_organogram_id",
			"patient_user_id",
			"name",
			"phone",
			"action_date",
			"search_column",
			"inserted_by_user_id",
			"inserted_by_organogram_id",
			"insertion_date",
			"last_modifier_user",
			
			"others_office_bn",
			"others_office_en",
			"others_office_id",
			"others_designation_and_id",
			
			"who_is_the_patient_cat",
			"mer_id",
			
			"isDeleted",
			"lastModificationTime"
		};
		updateQuery = getUpdateQuery(columnNames);
        addQuery = getAddQuery(columnNames);
		
        searchMap.put("nurse_organogram_id"," and (nurse_organogram_id = ?)");
		searchMap.put("nurse_user_id"," and (nurse_user_id = ?)");
		searchMap.put("patient_user_id"," and (employee_record_id = ?)");
		searchMap.put("phone"," and (phone = ?)");
		searchMap.put("action_date_start"," and (action_date >= ?)");
		searchMap.put("action_date_end"," and (action_date <= ?)");	
		searchMap.put("AnyField"," and (search_column like ?)");
	}

	private final Map<String,String> searchMap = new HashMap<>();

    private static class LazyLoader
    {
    	static final Nurse_actionDAO INSTANCE = new Nurse_actionDAO();
	}

	public static Nurse_actionDAO getInstance()
	{
    	return LazyLoader.INSTANCE;
	}
	
	public void setSearchColumn(Nurse_actionDTO nurse_actionDTO)
	{
		nurse_actionDTO.searchColumn = "";
		nurse_actionDTO.searchColumn += nurse_actionDTO.name + " ";
		nurse_actionDTO.searchColumn += nurse_actionDTO.phone + " ";
	}
	
	@Override
	public void set(PreparedStatement ps, Nurse_actionDTO nurse_actionDTO, boolean isInsert) throws SQLException
	{
		int index = 0;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(nurse_actionDTO);
		if(isInsert)
		{
			ps.setObject(++index,nurse_actionDTO.iD);
		}
		ps.setObject(++index,nurse_actionDTO.nurseOrganogramId);
		ps.setObject(++index,nurse_actionDTO.nurseUserId);
		ps.setObject(++index,nurse_actionDTO.appointmentId);
		ps.setObject(++index,nurse_actionDTO.patientOrganogramId);
		ps.setObject(++index,nurse_actionDTO.patientUserId);
		ps.setObject(++index,nurse_actionDTO.name);
		ps.setObject(++index,nurse_actionDTO.phone);
		ps.setObject(++index,nurse_actionDTO.actionDate);
		ps.setObject(++index,nurse_actionDTO.searchColumn);
		ps.setObject(++index,nurse_actionDTO.insertedByUserId);
		ps.setObject(++index,nurse_actionDTO.insertedByOrganogramId);
		ps.setObject(++index,nurse_actionDTO.insertionDate);
		ps.setObject(++index,nurse_actionDTO.lastModifierUser);
		
		ps.setObject(++index,nurse_actionDTO.othersOfficeBn);
		ps.setObject(++index,nurse_actionDTO.othersOfficeEn);
		ps.setObject(++index,nurse_actionDTO.othersOfficeId);
		ps.setObject(++index,nurse_actionDTO.othersDesignationAndId);
		
		ps.setObject(++index,nurse_actionDTO.whoIsThePatientCat);
		ps.setObject(++index,nurse_actionDTO.merId);
		if(isInsert)
		{
			ps.setObject(++index,nurse_actionDTO.isDeleted);
		}
		ps.setObject(++index, lastModificationTime);
		if(!isInsert)
		{
			ps.setObject(++index,nurse_actionDTO.iD);
		}
	}
	
	@Override
	public Nurse_actionDTO buildObjectFromResultSet(ResultSet rs)
	{
		try
		{
			Nurse_actionDTO nurse_actionDTO = new Nurse_actionDTO();
			int i = 0;
			nurse_actionDTO.iD = rs.getLong(columnNames[i++]);
			nurse_actionDTO.nurseOrganogramId = rs.getLong(columnNames[i++]);
			nurse_actionDTO.nurseUserId = rs.getString(columnNames[i++]);
			nurse_actionDTO.appointmentId = rs.getLong(columnNames[i++]);
			nurse_actionDTO.patientOrganogramId = rs.getLong(columnNames[i++]);
			nurse_actionDTO.patientUserId = rs.getString(columnNames[i++]);
			nurse_actionDTO.name = rs.getString(columnNames[i++]);
			nurse_actionDTO.phone = rs.getString(columnNames[i++]);
			nurse_actionDTO.actionDate = rs.getLong(columnNames[i++]);
			nurse_actionDTO.searchColumn = rs.getString(columnNames[i++]);
			nurse_actionDTO.insertedByUserId = rs.getLong(columnNames[i++]);
			nurse_actionDTO.insertedByOrganogramId = rs.getLong(columnNames[i++]);
			nurse_actionDTO.insertionDate = rs.getLong(columnNames[i++]);
			nurse_actionDTO.lastModifierUser = rs.getString(columnNames[i++]);
			
			nurse_actionDTO.othersOfficeBn = rs.getString(columnNames[i++]);
			nurse_actionDTO.othersOfficeEn = rs.getString(columnNames[i++]);
			nurse_actionDTO.othersOfficeId = rs.getLong(columnNames[i++]);
			nurse_actionDTO.othersDesignationAndId = rs.getString(columnNames[i++]);
			
			nurse_actionDTO.whoIsThePatientCat = rs.getInt(columnNames[i++]);
			nurse_actionDTO.merId = rs.getInt(columnNames[i++]);
			
			nurse_actionDTO.isDeleted = rs.getInt(columnNames[i++]);
			nurse_actionDTO.lastModificationTime = rs.getLong(columnNames[i++]);
			return nurse_actionDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
		
	public Nurse_actionDTO getDTOByID (long id)
	{
		Nurse_actionDTO nurse_actionDTO = null;
		try 
		{
			nurse_actionDTO = getDTOFromID(id);
			if(nurse_actionDTO != null)
			{
				NurseActionDetailsDAO nurseActionDetailsDAO = NurseActionDetailsDAO.getInstance();				
				List<NurseActionDetailsDTO> nurseActionDetailsDTOList = (List<NurseActionDetailsDTO>)nurseActionDetailsDAO.getDTOsByParent("nurse_action_id", nurse_actionDTO.iD);
				nurse_actionDTO.nurseActionDetailsDTOList = nurseActionDetailsDTOList;
			}
		}
		catch (Exception e) 
		{
				// TODO Auto-generated catch block
				e.printStackTrace();
		}
		return nurse_actionDTO;
	}
	
	public Nurse_actionDTO getDTOByMerId (long merId)
	{
		String sql = "select * from " + getTableName() + " where isDeleted = 0 and mer_id = " + merId;
		Nurse_actionDTO nurse_actionDTO = ConnectionAndStatementUtil.getT(sql,this::buildObjectFromResultSet);	
		try 
		{
			if(nurse_actionDTO != null)
			{
				NurseActionDetailsDAO nurseActionDetailsDAO = NurseActionDetailsDAO.getInstance();				
				List<NurseActionDetailsDTO> nurseActionDetailsDTOList = (List<NurseActionDetailsDTO>)nurseActionDetailsDAO.getDTOsByParent("nurse_action_id", nurse_actionDTO.iD);
				nurse_actionDTO.nurseActionDetailsDTOList = nurseActionDetailsDTOList;
			}
		}
		catch (Exception e) 
		{
				// TODO Auto-generated catch block
				e.printStackTrace();
		}
		return nurse_actionDTO;
	}
	
	 public RecordNavigator getRecordNavigator(Map<String, String> params, String tableJoinClause, String whereClause, String sortingClause) {
        if (params.get("todaysActions") != null) {
            if (whereClause == null) {
                whereClause = "";
            }
            whereClause += " and action_date >= " + TimeConverter.getToday();
        }
        tableJoinClause = " join users on users.username = nurse_action.patient_user_id ";
        return CommonDAOService.super.getRecordNavigator(params, tableJoinClause, whereClause, sortingClause);
	}

	
	public long getIDByAppointmentId(long appointmentId)
    {

		String sql = "SELECT id FROM nurse_action where isDeleted = 0 and appointment_id = " + appointmentId + " order by id desc limit 1";

		Long result = ConnectionAndStatementUtil.getT(sql, rs -> {
            try {
                return rs.getLong("id");
            } catch (SQLException ex) {
                ex.printStackTrace();
                logger.error(ex);
                return null;
            }
        });
        return result == null ? -1 : result;		

    }

	@Override
	public String getTableName() {
		return "nurse_action";
	}

	@Override
	public Map<String, String> getSearchMap() {
		return searchMap;
	}

	public long add(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Nurse_actionDTO) commonDTO,addQuery,true);
	}

	public long update(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Nurse_actionDTO) commonDTO,updateQuery,false);
	}
	
	@Override
	public String getLastModifierUser(){
		return "last_modifier_user";
    }
				
}
	