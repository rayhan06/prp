package MSR;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.google.gson.Gson;

import appointment.KeyCountDTO;

import common_lab_report.Common_lab_reportDAO;
import login.LoginDTO;
import medical_emergency_request.Medical_emergency_requestDAO;
import medical_transaction.Medical_transactionDAO;
import nurse_action.NurseActionDetailsDAO;
import nurse_action.Nurse_actionDAO;
import pb.CommonDAO;
import pb.Utils;
import physiotherapy_plan.PhysiotherapyScheduleDAO;
import prescription_details.Prescription_detailsDAO;
import sessionmanager.SessionConstants;
import user.*;
import vaccine_entry.Vaccine_entryDAO;

@WebServlet("/MSRServlet")
@MultipartConfig

public class MSRServlet 
extends HttpServlet{
	
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(MSRServlet.class);
    private final Gson gson = new Gson();
    Prescription_detailsDAO prescription_detailsDAO = new Prescription_detailsDAO();
    NurseActionDetailsDAO nurseActionDetailsDAO = NurseActionDetailsDAO.getInstance();
    Medical_emergency_requestDAO medical_emergency_requestDAO = new Medical_emergency_requestDAO();
    PhysiotherapyScheduleDAO physiotherapyScheduleDAO = new PhysiotherapyScheduleDAO();
    Common_lab_reportDAO common_lab_reportDAO = new Common_lab_reportDAO();
    Medical_transactionDAO medical_transactionDAO = new Medical_transactionDAO();
    Vaccine_entryDAO vaccine_entryDAO = Vaccine_entryDAO.getInstance();
    
    public static final int DENTISTRY = 16;
    public static final int EMERGENCY = 17;
    public static final int RBS = 1;
    public static final int ECG = 10;
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		System.out.println("In doget request = " + request);
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		try
		{
			String actionType = request.getParameter("actionType");
			if(actionType.equals("getReport"))
			{
				request.getRequestDispatcher("msr/msrView.jsp").forward(request, response);
			}
			else if(actionType.equals("getMSR"))
			{
				long startDate = Long.parseLong(request.getParameter("startDate"));
				long endDate = Long.parseLong(request.getParameter("endDate"));
				String Language = request.getParameter("Language");
				
				MSRDTO mSRDTO = new MSRDTO();
				long outdoor = prescription_detailsDAO.getCount(" doctor_id not in "
						+ "(select office_unit_organogram_id from employee_offices "
						+ "where status = 1 "
						+ " and isDeleted = 0 and medical_dept_cat = " + DENTISTRY + ")"
						+ " and visit_date >= " + startDate
						+ " and visit_date <= " + endDate
						, userDTO);
				mSRDTO.outdoor = Utils.getDigits(outdoor, Language);
				
				Nurse_actionDAO nurse_actionDAO = Nurse_actionDAO.getInstance();
				
				long emergencyPatients = nurse_actionDAO.getCount(
						" insertion_date >= " + startDate
						+ " and insertion_date <= " + endDate
						);
				mSRDTO.emergencyPatients = Utils.getDigits(emergencyPatients, Language);
				
				long dentistries = prescription_detailsDAO.getCount(" doctor_id in "
						+ "(select office_unit_organogram_id from employee_offices "
						+ "where status = 1 "
						+ " and isDeleted = 0 and medical_dept_cat = " + DENTISTRY + ")"
						+ " and visit_date >= " + startDate
						+ " and visit_date <= " + endDate
						, userDTO);
				mSRDTO.dentistries = Utils.getDigits(dentistries, Language);
				
				long prescriptions = prescription_detailsDAO.getCount(
						" visit_date >= " + startDate
						+ " and visit_date <= " + endDate
						, userDTO);
				mSRDTO.prescriptions = Utils.getDigits(prescriptions, Language);
				
				long ecg = nurseActionDetailsDAO.getCount(" nurse_action_cat = " + ECG
						+ " and action_date >= " + startDate
						+ " and action_date <= " + endDate);
				
				mSRDTO.ecg = Utils.getDigits(ecg, Language);
				
				long rbs = nurseActionDetailsDAO.getCount(" nurse_action_cat = " + RBS
						+ " and action_date >= " + startDate
						+ " and action_date <= " + endDate);
				
				mSRDTO.rbs = Utils.getDigits(rbs, Language);
				
				long emergencyCalls = medical_emergency_requestDAO.getCount(" service_cat = " + SessionConstants.HOME_SERVICE
						+ " and (nurse_organogram_id != -1 or service_person_organogram_id != -1)"
						+ " and service_date >= " + startDate
						+ " and service_date <= " + endDate, userDTO);
				
				mSRDTO.emergencyCalls = Utils.getDigits(emergencyCalls, Language);
				
				long ambulances = medical_emergency_requestDAO.getCount(" service_cat = " + SessionConstants.AMBULANCE_SERVICE
						+ " and (driver_organogram_id != -1)"
						+ " and service_date >= " + startDate
						+ " and service_date <= " + endDate, userDTO);
				
				mSRDTO.ambulances = Utils.getDigits(ambulances, Language);
				
				long physiotherapies = physiotherapyScheduleDAO.getCount(" is_done = 1"
						+ " and therapy_date >= " + startDate
						+ " and therapy_date <= " + endDate, userDTO);
				
				mSRDTO.physiotherapies = Utils.getDigits(physiotherapies, Language);
				
				long xrays = common_lab_reportDAO.getXRayCount(startDate, endDate);
				
				mSRDTO.xrays = Utils.getDigits(xrays, Language);
				
				List<KeyCountDTO> pathologies = common_lab_reportDAO.getPathologyCount(startDate, endDate);
				for(KeyCountDTO path: pathologies)
				{
					mSRDTO.pathologies += CommonDAO.getName(Language, "lab_test", path.key) + ": " + Utils.getDigits(path.count, Language) + "<br>";
				}
				
				KeyCountDTO quantityCost = medical_transactionDAO.getDeliveredPrescriptions(startDate, endDate);
				mSRDTO.pharmacyPrescriptions = Utils.getDigits(quantityCost.count, Language);
				mSRDTO.medicines = Utils.getDigits(quantityCost.count2, Language);
				mSRDTO.drugCosts = Utils.getDigits(quantityCost.cost, Language);
				
				mSRDTO.covids = Utils.getDigits(vaccine_entryDAO.getCount(startDate, endDate), Language);
				
				
				
				PrintWriter out = response.getWriter();
				response.setContentType("application/json");
				response.setCharacterEncoding("UTF-8");
				String encoded = this.gson.toJson(mSRDTO);
				System.out.println("json encoded data = " + encoded);
				out.print(encoded);
				out.flush();
			}
		}
		catch(Exception ex)
		{
			logger.debug("",ex);
		}
	}
	


}
