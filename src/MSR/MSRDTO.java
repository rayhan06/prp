package MSR;

public class MSRDTO {
	
	public String outdoor = "";	
	public String emergencyPatients = "";
	public String ecg = "";	
	public String rbs = "";
	public String emergencyCalls = "";	
	public String physiotherapies = "";
	public String xrays = "";	
	public String pathologies = "";
	public String dentistries = "";	
	public String covids = "";
	public String ambulances = "";
	public String medicines = "";	
	public String prescriptions = "";
	public String pharmacyPrescriptions = "";
	public String drugCosts = "";

}
