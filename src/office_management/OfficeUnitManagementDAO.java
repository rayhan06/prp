package office_management;

import common.VbSequencerService;
import dbm.DBMR;
import dbm.DBMW;
import office_units.Office_unitsRepository;
import test_lib.RQueryBuilder;

import java.sql.*;
import java.util.List;

public class OfficeUnitManagementDAO implements VbSequencerService {

    public void recordUpdateTime2(Connection connection,  long lastModificationTime) throws SQLException {
        recordUpdateTime(connection,"office_units",lastModificationTime);
    }

    public long addOffice(List<List<Integer>> listOfList, int office_id) {
        for (List<Integer> ids : listOfList) {
            for (int i = 0; i < ids.size(); i++) {

                if (!alreadyInOfficeList(ids.get(i), office_id)) {
                    OfficeUnitManagementDTO officeUnitManagementDTO = getOfficeOriginUnit(ids.get(i));
                    officeUnitManagementDTO.office_id = office_id;
                    if (office_id < 0) {
                        System.out.println("OFFICE ID IS NEGATIVE @@@@@@@@@@@@@@@@@@@@@ " + office_id);
                        return 0l;
                    }
                    officeUnitManagementDTO.parent_unit_id_for_office_unit = getParentUnitId(officeUnitManagementDTO.id, office_id);
                    insertOffice(officeUnitManagementDTO);
                }
            }
        }
        return 0l;
    }

    public int deleteOffice(List<List<Integer>> listOfList, int office_id) {
        int count = 0;
        for (List<Integer> ids : listOfList) {
            for (Integer id : ids) {
                if (!hasEmployee(id) && !hasSubUnit(id, office_id)) {
                    delete(id);
                    break;
                } else {
                    count++;
                }
            }
        }
        return count;
    }

    private void delete(int unit_id) {

        Connection connection = null;
        PreparedStatement ps = null;
        try {
            connection = DBMW.getInstance().getConnection();
            String sql = "update office_units set isDeleted = 1 where id = ?";
            ps = connection.prepareStatement(sql);
            ps.setInt(1, unit_id);
            ps.execute();
            ps.close();
            Office_unitsRepository.getInstance().deleteDtoById(unit_id);

        } catch (SQLException | ClassNotFoundException | InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    DBMW.getInstance().freeConnection(connection);
                } catch (SQLException | InstantiationException | IllegalAccessException | ClassNotFoundException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private long insertOffice(OfficeUnitManagementDTO officeUnitManagementDTO) {

        Connection connection = null;
        PreparedStatement ps = null;

        try {
            connection = DBMW.getInstance().getConnection();

            long newId = DBMW.getInstance().getNextSequenceId("office_units");
            long lastModificationTime = System.currentTimeMillis();

            String sql = "INSERT INTO office_units";

            // region SQL

            sql += "(";
            sql += "ID";
            sql += ", ";
            sql += "office_ministry_id";
            sql += ", ";
            sql += "office_layer_id";
            sql += ", ";
            sql += "office_id";
            sql += ", ";
            sql += "office_origin_unit_id";
            sql += ", ";
            sql += "unit_name_bng";
            sql += ", ";
            sql += "unit_name_eng";
            sql += ", ";
            sql += "office_unit_category";
            sql += ", ";
            sql += "parent_unit_id";
            sql += ", ";
            sql += "parent_origin_unit_id";
            sql += ", ";
            sql += "unit_nothi_code";
            sql += ", ";
            sql += "unit_level";
            sql += ", ";
            sql += "sarok_no_start";
            sql += ", ";
            sql += "email";
            sql += ", ";
            sql += "phone";
            sql += ", ";
            sql += "fax";
            sql += ", ";
            sql += "active_status";
            sql += ", ";
            sql += "created_by";
            sql += ", ";
            sql += "modified_by";
            sql += ", ";
            sql += "created";
            sql += ", ";
            sql += "modified";
            sql += ", ";
            sql += "status";
            sql += ", ";
            sql += "isDeleted";
            sql += ", ";
            sql += "lastModificationTime)";

            sql += " VALUES(";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ?)";

            // endregion SQL

            System.out.println(sql);
            ps = connection.prepareStatement(sql);
            int index = 1;

            ps.setObject(index++, newId);
            ps.setObject(index++, officeUnitManagementDTO.office_ministry_id);
            ps.setObject(index++, officeUnitManagementDTO.office_layer_id);
            ps.setObject(index++, officeUnitManagementDTO.office_id);
            ps.setObject(index++, officeUnitManagementDTO.id); // office_origin_unit_id
            ps.setObject(index++, officeUnitManagementDTO.unit_name_bng);
            ps.setObject(index++, officeUnitManagementDTO.unit_name_eng);
            ps.setObject(index++, officeUnitManagementDTO.office_unit_category);
            ps.setObject(index++, officeUnitManagementDTO.parent_unit_id_for_office_unit); // parent_unit_id
            ps.setObject(index++, officeUnitManagementDTO.parent_unit_id);
            ps.setObject(index++, officeUnitManagementDTO.unit_nothi_code);
            ps.setObject(index++, officeUnitManagementDTO.unit_level);
            ps.setObject(index++, officeUnitManagementDTO.sarok_no_start);
            ps.setObject(index++, officeUnitManagementDTO.email);
            ps.setObject(index++, officeUnitManagementDTO.phone);
            ps.setObject(index++, officeUnitManagementDTO.fax);
            ps.setObject(index++, true); // active_status
            ps.setObject(index++, officeUnitManagementDTO.created_by);
            ps.setObject(index++, officeUnitManagementDTO.modified_by);
            ps.setObject(index++, officeUnitManagementDTO.created);
            ps.setObject(index++, officeUnitManagementDTO.modified);
            ps.setObject(index++, officeUnitManagementDTO.status);
            ps.setObject(index++, officeUnitManagementDTO.isDeleted);
            ps.setObject(index++, lastModificationTime);

            System.out.println(ps);
            ps.execute();

            recordUpdateTime2(connection, lastModificationTime);

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (connection != null) {
                    DBMW.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
        return officeUnitManagementDTO.id;
    }

    public OfficeUnitManagementDTO getOfficeOriginUnit(int id) {
        Connection connection = null;
        Statement stmt = null;
        ResultSet rs = null;
        OfficeUnitManagementDTO officeUnitManagementDTO = new OfficeUnitManagementDTO();

        String sql = "select * from office_origin_units where id = " + id + " and isDeleted = 0";
        System.out.println(sql);

        try {
            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();
            rs = stmt.executeQuery(sql);

            while (rs.next()) {
                officeUnitManagementDTO.id = rs.getLong("id");
                officeUnitManagementDTO.office_ministry_id = rs.getInt("office_ministry_id");
                officeUnitManagementDTO.office_layer_id = rs.getInt("office_layer_id");
                officeUnitManagementDTO.office_origin_id = rs.getInt("office_origin_id");
                officeUnitManagementDTO.unit_name_bng = rs.getString("unit_name_bng");
                officeUnitManagementDTO.unit_name_eng = rs.getString("unit_name_eng");
                officeUnitManagementDTO.office_unit_category = rs.getString("office_unit_category");
                officeUnitManagementDTO.parent_unit_id = rs.getInt("parent_unit_id");
                officeUnitManagementDTO.unit_level = rs.getInt("unit_level");
                officeUnitManagementDTO.status = rs.getBoolean("status");
                officeUnitManagementDTO.created_by = rs.getLong("created_by");
                officeUnitManagementDTO.modified_by = rs.getLong("modified_by");
                officeUnitManagementDTO.created = rs.getLong("created");
                officeUnitManagementDTO.modified = rs.getLong("modified");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    DBMR.getInstance().freeConnection(connection);
                } catch (SQLException | ClassNotFoundException | InstantiationException | IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }

        return officeUnitManagementDTO;
    }

    private int getParentUnitId(long office_origin_unit_id, int office_id) {
        Connection connection = null;
        Statement stmt = null;
        ResultSet rs = null;

        String sql = "select id from office_units where office_origin_unit_id in " +
                "(SELECT parent_unit_id FROM office_origin_units where id = %d and isDeleted = 0) and office_id = %d and isDeleted = 0";
        sql = String.format(sql, office_origin_unit_id, office_id);

        try {
            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();
            rs = stmt.executeQuery(sql);
            int x = 0;
            while (rs.next()) {
                x = rs.getInt("id");
            }
            return x;

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    DBMR.getInstance().freeConnection(connection);
                } catch (SQLException | ClassNotFoundException | InstantiationException | IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }
        return 0;
    }

    private boolean hasEmployee(int unit_id) {
        Connection connection = null;
        Statement stmt = null;
        ResultSet rs = null;

        String sql = "select id from employee_offices where office_unit_id = " + unit_id;
        try {
            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();
            rs = stmt.executeQuery(sql);

            while (rs.next()) {
                return rs.getInt("id") > 0;
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    DBMR.getInstance().freeConnection(connection);
                } catch (SQLException | ClassNotFoundException | InstantiationException | IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }
        return false;
    }

    private boolean hasSubUnit(int officeUnitIdAsParentUnitId, int officeId) {
        Connection connection = null;
        Statement stmt = null;
        ResultSet rs = null;

        String sql = "SELECT id FROM office_units where parent_unit_id = %d and office_id = %d and isDeleted = 0;";
        sql = String.format(sql, officeUnitIdAsParentUnitId, officeId);
        try {
            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();
            rs = stmt.executeQuery(sql);

            while (rs.next()) {
                return rs.getInt("id") > 0;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    DBMR.getInstance().freeConnection(connection);
                } catch (SQLException | ClassNotFoundException | InstantiationException | IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }
        return false;
    }

    private boolean alreadyInOfficeList(int officeOriginUnitId, int officeId) {
        Connection connection = null;
        Statement stmt = null;
        ResultSet rs = null;

        String sql = "SELECT id FROM office_units where office_origin_unit_id = %d and office_id = %d and isDeleted = 0";
        sql = String.format(sql, officeOriginUnitId, officeId);

        try {
            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();
            rs = stmt.executeQuery(sql);

            while (rs.next()) {
                return rs.getInt("id") > 0;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    DBMR.getInstance().freeConnection(connection);
                } catch (SQLException | ClassNotFoundException | InstantiationException | IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }
        return false;
    }

    public long getOfficeOriginUnitAndOfficeUnitCount(int officeOriginUnitId) {
        Connection connection = null;
        Statement stmt = null;
        ResultSet rs = null;

        String sql = "select count(id) as count from office_origin_units where isDeleted = 0 and office_origin_id = %d";
        sql = String.format(sql, officeOriginUnitId);

        try {
            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();
            rs = stmt.executeQuery(sql);

            while (rs.next()) {
                return rs.getInt("count");
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    DBMR.getInstance().freeConnection(connection);
                } catch (SQLException | ClassNotFoundException | InstantiationException | IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }
        return -1;
    }

    public boolean swapOfficeUnit(String officeUnitId0, String officeUnitId1) {
        String sql0 = "select id, unit_level from office_units where id  = " + officeUnitId0;
        RQueryBuilder<OfficeManagementModel.OfficeUnitShortModel> rQueryBuilder0 = new RQueryBuilder<>();
        OfficeManagementModel.OfficeUnitShortModel a = rQueryBuilder0.setSql(sql0).of(OfficeManagementModel.OfficeUnitShortModel.class).buildRaw().get(0);

        String sql1 = "select id, unit_level from office_units where id  = " + officeUnitId1;
        RQueryBuilder<OfficeManagementModel.OfficeUnitShortModel> rQueryBuilder1 = new RQueryBuilder<>();
        OfficeManagementModel.OfficeUnitShortModel b = rQueryBuilder1.setSql(sql1).of(OfficeManagementModel.OfficeUnitShortModel.class).buildRaw().get(0);

        String updateSql0 = String.format("update office_units set unit_level = %d where id = %s", b.getUnit_level(), officeUnitId0);
        String updateSql1 = String.format("update office_units set unit_level = %d where id = %s", a.getUnit_level(), officeUnitId1);

        RQueryBuilder<Boolean> u = new RQueryBuilder<>();
        Boolean x = u.setSql(updateSql0).of(Boolean.class).buildUpdate();

        RQueryBuilder<Boolean> v = new RQueryBuilder<>();
        Boolean y = v.setSql(updateSql1).of(Boolean.class).buildUpdate();

        return x & y;
    }
}
