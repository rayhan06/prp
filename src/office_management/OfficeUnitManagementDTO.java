package office_management;

public class OfficeUnitManagementDTO {
    public long id;
    public int office_ministry_id;
    public int office_layer_id;
    public int office_id;
    public int office_origin_id;
    public int office_origin_unit_id;
    public String unit_name_bng;
    public String unit_name_eng;
    public String office_unit_category;
    public int parent_unit_id;
    public long parent_unit_id_for_office_unit;
    public int parent_origin_unit_id;
    public String unit_nothi_code;
    public int unit_level;
    public int sarok_no_start;
    public String email;
    public String phone;
    public String fax;
    public boolean active_status;
    public long created_by;
    public long modified_by;
    public long created;
    public long modified;
    public boolean status;
    public boolean isDeleted;
    public long lastModificationTime;
}
