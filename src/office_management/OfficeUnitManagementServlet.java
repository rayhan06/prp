/*
package office_management;

import com.google.gson.Gson;
import office_units.OfficeUnitService;
import test_lib.util.Pair;
import login.LoginDTO;
import office_layers.Office_layersServlet;
import org.apache.log4j.Logger;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

@WebServlet("/OfficeUnitManagementServlet")
public class OfficeUnitManagementServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Office_layersServlet.class);

    public OfficeUnitManagementServlet() {
        super();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getInstance().getUserDtoByUserId(loginDTO.userID);

        try {
            String actionType = request.getParameter("actionType");
            if (actionType.equals("addOffices")) {
                String listAsString = request.getParameter("ids");
                String[] data = listAsString.split("__");
                List<List<Integer>> listOfList = new ArrayList<>();

                for (String s : data) {
                    String[] ids = s.split("_0_");

                    List<Integer> arr = new ArrayList<>();
                    for (String d : ids) {
                        try {
                            arr.add(Integer.parseInt(d));
                        } catch (Exception ignore) {
                        }
                    }
                    if (arr.size() > 0) listOfList.add(arr);
                }

                int office_id = Integer.parseInt(request.getParameter("office_id"));
                if (listOfList.size() > 0) {
                    // Working good
                    //new OfficeUnitManagementDAO().addOffice(listOfList, office_id);
                    OfficeUnitService officeUnitService = new OfficeUnitService();
                    officeUnitService.addNewOffice(listOfList, office_id);
                }

                response.setContentType("application/json");
                String res = new Gson().toJson(new Pair<String, Boolean>("success", true));
                PrintWriter out = response.getWriter();

                out.print(res);
                out.flush();
            } else if (actionType.equals("deleteOffices")) {
                String listAsString = request.getParameter("ids");
                String[] data = listAsString.split("__");
                List<List<Integer>> listOfList = new ArrayList<>();

                for (String s : data) {
                    String[] ids = s.split("_0_");

                    List<Integer> arr = new ArrayList<>();
                    for (String d : ids) {
                        try {
                            arr.add(Integer.parseInt(d));
                        } catch (Exception ignore) {
                        }
                    }
                    if (arr.size() > 0) {
                        Collections.reverse(arr);
                        listOfList.add(arr);
                    }
                }

                int deleteCount = 0;
                int office_id = Integer.parseInt(request.getParameter("office_id"));
                if (listOfList.size() > 0) {
                    listOfList.sort(Comparator.comparingInt(List::size));
                    Collections.reverse(listOfList);
                    deleteCount = new OfficeUnitManagementDAO().deleteOffice(listOfList, office_id);
                }

                response.setContentType("application/json");
                String res = new Gson().toJson(new Pair<String, Integer>("success", deleteCount));
                PrintWriter out = response.getWriter();

                out.print(res);
                out.flush();
            } else if (actionType.equals("updateLevel")) {
                String officeUnitId0 = request.getParameter("id0");
                String officeUnitId1 = request.getParameter("id1");

                response.setContentType("application/json");
                PrintWriter out = response.getWriter();

                out.print(new OfficeUnitManagementDAO().swapOfficeUnit(officeUnitId0, officeUnitId1));
                out.flush();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.debug(ex);
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getInstance().getUserDtoByUserId(loginDTO.userID);

        try {
            String actionType = request.getParameter("actionType");
            if (actionType.equals("addOriginUnitAndUnitCount")) {
                int officeOriginUnitId = Integer.parseInt(request.getParameter("origin_id"));
                long count = new OfficeUnitManagementDAO().getOfficeOriginUnitAndOfficeUnitCount(officeOriginUnitId);
                response.setContentType("application/json");
                Map<String, Long> map = new HashMap<>();
                map.put("count", count);
                String res = new Gson().toJson(map);
                PrintWriter out = response.getWriter();

                out.print(res);
                out.flush();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
*/
