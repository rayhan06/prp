package office_management;

public class OfficeManagementModel {
    public static class OfficeUnitShortModel {
        private int id;
        private int unit_level;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getUnit_level() {
            return unit_level;
        }

        public void setUnit_level(int unit_level) {
            this.unit_level = unit_level;
        }
    }
}
