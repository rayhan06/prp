package vm_vehicle;
import java.util.*; 
import util.*;


public class Vm_vehicleMAPS extends CommonMaps
{	
	public Vm_vehicleMAPS(String tableName)
	{
		

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("purchaseDate".toLowerCase(), "purchaseDate".toLowerCase());
		java_DTO_map.put("vehicleBrandCat".toLowerCase(), "vehicleBrandCat".toLowerCase());
		java_DTO_map.put("vehicleColorCat".toLowerCase(), "vehicleColorCat".toLowerCase());
		java_DTO_map.put("vehicleTypeCat".toLowerCase(), "vehicleTypeCat".toLowerCase());
		java_DTO_map.put("regNo".toLowerCase(), "regNo".toLowerCase());
		java_DTO_map.put("chasisNo".toLowerCase(), "chasisNo".toLowerCase());
		java_DTO_map.put("engineNo".toLowerCase(), "engineNo".toLowerCase());
		java_DTO_map.put("numberOfSeats".toLowerCase(), "numberOfSeats".toLowerCase());
		java_DTO_map.put("vehicleFuelCat".toLowerCase(), "vehicleFuelCat".toLowerCase());
		java_DTO_map.put("supplierType".toLowerCase(), "supplierType".toLowerCase());
		java_DTO_map.put("modelNo".toLowerCase(), "modelNo".toLowerCase());
		java_DTO_map.put("purchaseAmount".toLowerCase(), "purchaseAmount".toLowerCase());
		java_DTO_map.put("manufactureYear".toLowerCase(), "manufactureYear".toLowerCase());
		java_DTO_map.put("fitnessExpiryDate".toLowerCase(), "fitnessExpiryDate".toLowerCase());
		java_DTO_map.put("taxTokenExpiryDate".toLowerCase(), "taxTokenExpiryDate".toLowerCase());
		java_DTO_map.put("digitalPlate".toLowerCase(), "digitalPlate".toLowerCase());
		java_DTO_map.put("status".toLowerCase(), "status".toLowerCase());
		java_DTO_map.put("filesDropzone".toLowerCase(), "filesDropzone".toLowerCase());
		java_DTO_map.put("insertedByUserId".toLowerCase(), "insertedByUserId".toLowerCase());
		java_DTO_map.put("insertedByOrganogramId".toLowerCase(), "insertedByOrganogramId".toLowerCase());
		java_DTO_map.put("insertionDate".toLowerCase(), "insertionDate".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
		java_DTO_map.put("searchColumn".toLowerCase(), "searchColumn".toLowerCase());

		java_SQL_map.put("purchase_date".toLowerCase(), "purchaseDate".toLowerCase());
		java_SQL_map.put("vehicle_brand_cat".toLowerCase(), "vehicleBrandCat".toLowerCase());
		java_SQL_map.put("vehicle_color_cat".toLowerCase(), "vehicleColorCat".toLowerCase());
		java_SQL_map.put("vehicle_type_cat".toLowerCase(), "vehicleTypeCat".toLowerCase());
		java_SQL_map.put("reg_no".toLowerCase(), "regNo".toLowerCase());
		java_SQL_map.put("chasis_no".toLowerCase(), "chasisNo".toLowerCase());
		java_SQL_map.put("engine_no".toLowerCase(), "engineNo".toLowerCase());
		java_SQL_map.put("number_of_seats".toLowerCase(), "numberOfSeats".toLowerCase());
		java_SQL_map.put("vehicle_fuel_cat".toLowerCase(), "vehicleFuelCat".toLowerCase());
		java_SQL_map.put("supplier_type".toLowerCase(), "supplierType".toLowerCase());
		java_SQL_map.put("model_no".toLowerCase(), "modelNo".toLowerCase());
		java_SQL_map.put("purchase_amount".toLowerCase(), "purchaseAmount".toLowerCase());
		java_SQL_map.put("manufacture_year".toLowerCase(), "manufactureYear".toLowerCase());
		java_SQL_map.put("fitness_expiry_date".toLowerCase(), "fitnessExpiryDate".toLowerCase());
		java_SQL_map.put("tax_token_expiry_date".toLowerCase(), "taxTokenExpiryDate".toLowerCase());
		java_SQL_map.put("digital_plate".toLowerCase(), "digitalPlate".toLowerCase());
		java_SQL_map.put("status".toLowerCase(), "status".toLowerCase());
		java_SQL_map.put("files_dropzone".toLowerCase(), "filesDropzone".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Purchase Date".toLowerCase(), "purchaseDate".toLowerCase());
		java_Text_map.put("Vehicle Brand".toLowerCase(), "vehicleBrandCat".toLowerCase());
		java_Text_map.put("Vehicle Color".toLowerCase(), "vehicleColorCat".toLowerCase());
		java_Text_map.put("Vehicle Type".toLowerCase(), "vehicleTypeCat".toLowerCase());
		java_Text_map.put("Reg No".toLowerCase(), "regNo".toLowerCase());
		java_Text_map.put("Chasis No".toLowerCase(), "chasisNo".toLowerCase());
		java_Text_map.put("Engine No".toLowerCase(), "engineNo".toLowerCase());
		java_Text_map.put("Number Of Seats".toLowerCase(), "numberOfSeats".toLowerCase());
		java_Text_map.put("Vehicle Fuel".toLowerCase(), "vehicleFuelCat".toLowerCase());
		java_Text_map.put("Supplier".toLowerCase(), "supplierType".toLowerCase());
		java_Text_map.put("Model No".toLowerCase(), "modelNo".toLowerCase());
		java_Text_map.put("Purchase Amount".toLowerCase(), "purchaseAmount".toLowerCase());
		java_Text_map.put("Manufacture Year".toLowerCase(), "manufactureYear".toLowerCase());
		java_Text_map.put("Fitness Expiry Date".toLowerCase(), "fitnessExpiryDate".toLowerCase());
		java_Text_map.put("Tax Token Expiry Date".toLowerCase(), "taxTokenExpiryDate".toLowerCase());
		java_Text_map.put("Digital Plate".toLowerCase(), "digitalPlate".toLowerCase());
		java_Text_map.put("Status".toLowerCase(), "status".toLowerCase());
		java_Text_map.put("Files Dropzone".toLowerCase(), "filesDropzone".toLowerCase());
		java_Text_map.put("Inserted By User Id".toLowerCase(), "insertedByUserId".toLowerCase());
		java_Text_map.put("Inserted By Organogram Id".toLowerCase(), "insertedByOrganogramId".toLowerCase());
		java_Text_map.put("Insertion Date".toLowerCase(), "insertionDate".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
		java_Text_map.put("Search Column".toLowerCase(), "searchColumn".toLowerCase());
			
	}

}