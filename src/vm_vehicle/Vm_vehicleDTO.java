package vm_vehicle;
import java.util.*; 
import util.*; 


public class Vm_vehicleDTO extends CommonDTO
{

	public long purchaseDate = System.currentTimeMillis();
	public String purchaseDateInString = "";
	public int vehicleBrandCat = -1;
	public int vehicleColorCat = -1;
	public int vehicleTypeCat = -1;
    public String regNo = "";
    public String chasisNo = "";
    public String engineNo = "";
	public int numberOfSeats = -1;
	public String vehicleFuelCat = "";
	public long supplierType = -1;
	public String supplierName = "";
    public String modelNo = "";
	public double purchaseAmount = -1;
	public long manufactureYear = -1;
	public long last_maintenance_date = -1;
	public long last_maintenance_id = -1;
	public long fitnessExpiryDate = System.currentTimeMillis();
	public long taxTokenExpiryDate = System.currentTimeMillis();
	public boolean digitalPlate = false;
	public int status = -1;
	public long filesDropzone = -1;
	public long insertedByUserId = -1;
	public long insertedByOrganogramId = -1;
	public long insertionDate = -1;
	public long count = 0;
	public String typeName = "";
	
	
    @Override
	public String toString() {
            return "$Vm_vehicleDTO[" +
            " iD = " + iD +
            " purchaseDate = " + purchaseDate +
            " vehicleBrandCat = " + vehicleBrandCat +
            " vehicleColorCat = " + vehicleColorCat +
            " vehicleTypeCat = " + vehicleTypeCat +
            " regNo = " + regNo +
            " chasisNo = " + chasisNo +
            " engineNo = " + engineNo +
            " numberOfSeats = " + numberOfSeats +
            " vehicleFuelCat = " + vehicleFuelCat +
            " supplierType = " + supplierType +
            " modelNo = " + modelNo +
            " purchaseAmount = " + purchaseAmount +
            " manufactureYear = " + manufactureYear +
            " fitnessExpiryDate = " + fitnessExpiryDate +
            " taxTokenExpiryDate = " + taxTokenExpiryDate +
            " digitalPlate = " + digitalPlate +
            " status = " + status +
            " filesDropzone = " + filesDropzone +
            " insertedByUserId = " + insertedByUserId +
            " insertedByOrganogramId = " + insertedByOrganogramId +
            " insertionDate = " + insertionDate +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            " searchColumn = " + searchColumn +
            "]";
    }

}