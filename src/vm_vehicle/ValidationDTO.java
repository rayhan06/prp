package vm_vehicle;

public class ValidationDTO
{

	public boolean valid = false;
	public String errMsg = "";
//	public String errMsgBn = "";

    @Override
	public String toString() {
            return "ValidationDTO[" +
            " valid = " + valid +
            " errMsg = " + errMsg +
//            " errMsgBn = " + errMsgBn +
            "]";
    }

}