package vm_vehicle;

import asset_supplier.Asset_supplierRepository;
import category.CategoryDTO;
import com.google.gson.Gson;
import common.ApiResponse;
import files.FilesDAO;
import fiscal_year.Fiscal_yearDAO;
import fiscal_year.Fiscal_yearDTO;
import fiscal_year.Fiscal_yearRepository;
import language.LC;
import language.LM;
import login.LoginDTO;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import pb.*;
import permission.MenuConstants;
import role.PermissionRepository;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.*;
import vm_maintenance.*;
import vm_requisition.Vm_requisitionDAO;
import vm_requisition.Vm_requisitionDTO;
import vm_tax_token.VmTaxTokenItemDAO;
import vm_tax_token.VmTaxTokenItemDTO;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;


/**
 * Servlet implementation class Vm_vehicleServlet
 */
@WebServlet("/Vm_vehicleServlet")
@MultipartConfig
public class Vm_vehicleServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Vm_vehicleServlet.class);

    String tableName = "vm_vehicle";

    Vm_vehicleDAO vm_vehicleDAO;
    CommonRequestHandler commonRequestHandler;
    FilesDAO filesDAO = new FilesDAO();
    private final Gson gson = new Gson();

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Vm_vehicleServlet() {
        super();
        try {
            vm_vehicleDAO = new Vm_vehicleDAO(tableName);
            commonRequestHandler = new CommonRequestHandler(vm_vehicleDAO);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("In doget request = " + request);
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        boolean isPermanentTable = true;
        if (request.getParameter("isPermanentTable") != null) {
            isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
        }
        System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
        try {
            String actionType = request.getParameter("actionType");
            if (actionType.equals("getAddPage")) {
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_VEHICLE_ADD)) {
                    commonRequestHandler.getAddPage(request, response);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else if (actionType.equals("getEditPage")) {
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_VEHICLE_UPDATE)) {
                    getVm_vehicle(request, response);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else if (actionType.equals("getReportPage")) {

                request.getRequestDispatcher("vm_vehicle_report/vm_vehicleEdit.jsp")
                        .forward(request, response);
//				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_VEHICLE_UPDATE))
//				{
//					getVm_vehicle(request, response);
//				}
//				else
//				{
//					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
//				}
            } else if (actionType.equals("getReportData")) {
                getReportData(request, response);
            } else if (actionType.equals("getByVehicleType")) {
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_REQUISITION_UPDATE)) {
                    getByVehicleType(request, response);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else if (actionType.equals("getAllByVehicleType")) {
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_REQUISITION_UPDATE)) {
                    getAllByVehicleType(request, response);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else if (actionType.equals("getVehicleFuelType")) {
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_REQUISITION_UPDATE)) {
                    getVehicleFuelType(request, response);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else if (actionType.equals("getByVehicleTypeForAssignment")) {

                getByVehicleTypeForAssignment(request, response);
            } else if (actionType.equals("downloadDropzoneFile")) {
                commonRequestHandler.downloadDropzoneFile(request, response, filesDAO);
            } else if (actionType.equals("DeleteFileFromDropZone")) {
                commonRequestHandler.deleteFileFromDropZone(request, response, filesDAO);
            } else if (actionType.equals("getURL")) {
                String URL = request.getParameter("URL");
                System.out.println("URL = " + URL);
                response.sendRedirect(URL);
            } else if (actionType.equals("search")) {
                System.out.println("search requested");
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_VEHICLE_SEARCH)) {
                    if (isPermanentTable) {
                        String filter = request.getParameter("filter");
                        System.out.println("filter = " + filter);
                        if (filter != null) {
                            filter = ""; //shouldn't be directly used, rather manipulate it.
                            searchVm_vehicle(request, response, isPermanentTable, filter);
                        } else {
                            searchVm_vehicle(request, response, isPermanentTable, "");
                        }
                    } else {
                        //searchVm_vehicle(request, response, tempTableName, isPermanentTable);
                    }
                }
            } else if (actionType.equals("view")) {
                System.out.println("view requested");
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_VEHICLE_SEARCH)) {
                    commonRequestHandler.view(request, response);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }

            } else if (actionType.equals("checkRegistrationAndChasisValidation")) {
                checkRegistrationAndChasisValidation(request, response);
            }
            else if (actionType.equals("getAllVehicleFuelCat")) {
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_VEHICLE_ADD)) {

                    List<OptionDTO> OptionDTOList = new ArrayList<>();
                    OptionDTO optionDTO;
                    List<CategoryDTO> otherOfficeDTOList = CatRepository.getInstance().getCategoryDTOList("vehicle_fuel");

                    for (CategoryDTO dto : otherOfficeDTOList) {

                        optionDTO = new OptionDTO(dto.nameEn, dto.nameBn, dto.value + "");
                        OptionDTOList.add(optionDTO);
                    }

                    response.getWriter().write(gson.toJson(OptionDTOList));
                    response.getWriter().flush();
                    response.getWriter().close();

                    return;
                }
            }
            else {
                request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.debug(ex);
        }
    }


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        boolean isLanEng = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language.equalsIgnoreCase("English");
        System.out.println("doPost");
        boolean isPermanentTable = true;
        if (request.getParameter("isPermanentTable") != null) {
            isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
        }
        System.out.println("In servlet, isPermanentTable = " + isPermanentTable);

        try {
            String actionType = request.getParameter("actionType");
            System.out.println("actionType = " + actionType);
            if (actionType.equals("add")) {

                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_VEHICLE_ADD)) {
                    System.out.println("going to  addVm_vehicle ");
                    addVm_vehicle(request, response, true, userDTO, true);
                } else {
                    System.out.println("Not going to  addVm_vehicle ");
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }

            } else if (actionType.equals("UploadFilesFromDropZone")) {
                commonRequestHandler.UploadFilesFromDropZone(request, response, userDTO);
            } else if (actionType.equals("getDTO")) {

                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_VEHICLE_ADD)) {
                    getDTO(request, response);
                } else {
                    System.out.println("Not going to  addVm_vehicle ");
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }

            } else if (actionType.equals("edit")) {

                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_VEHICLE_UPDATE)) {
                    addVm_vehicle(request, response, false, userDTO, isPermanentTable);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else if (actionType.equals("delete")) {
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_VEHICLE_UPDATE)) {
                    if(!vm_vehicleDAO.vehicleAlreadyInUse(request)){
                        commonRequestHandler.delete(request, response, userDTO);
                        Vm_vehicleRepository.getInstance().reload(true);
                    }
                    else{
                        response.sendRedirect("Vm_vehicleServlet?actionType=search&deleteFailed=true");
                    }

                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else if (actionType.equals("search")) {
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.VM_VEHICLE_SEARCH)) {
                    searchVm_vehicle(request, response, true, "");
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else if (actionType.equals("getGeo")) {
                System.out.println("going to geoloc ");
                request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            logger.debug(ex);
        }
    }

    private void getDTO(HttpServletRequest request, HttpServletResponse response) {
        try {
            System.out.println("In getDTO");
            Vm_vehicleDTO vm_vehicleDTO = Vm_vehicleRepository.getInstance()
                    .getVm_vehicleDTOByID(Long.parseLong(request.getParameter("ID")));
//					(Vm_vehicleDTO)vm_vehicleDAO.getDTOByID(Long.parseLong(request.getParameter("ID")));
            PrintWriter out = response.getWriter();
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");

            String encoded = this.gson.toJson(vm_vehicleDTO);
            System.out.println("json encoded data = " + encoded);
            out.print(encoded);
            out.flush();
        } catch (NumberFormatException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    private ValidationDTO checkRegistrationAndChasisValidation(String regNo, String chasisNo, String language,
                                                               String actionType, Long ID) {

        ValidationDTO validationDTO = new ValidationDTO();
        int regCount = vm_vehicleDAO.getCountOfRegNo(regNo);
        int chasisCount = vm_vehicleDAO.getCountOfChasisNo(chasisNo);

        boolean regValid = false;
        boolean chasisValid = false;

        if (actionType.equalsIgnoreCase("add")) {
            if (regCount == 0) {
                regValid = true;
            }

            if (chasisCount == 0) {
                chasisValid = true;
            }


        } else {
            Vm_vehicleDTO dto = Vm_vehicleRepository.getInstance().
                    getVm_vehicleDTOByID(ID);
            if (dto.regNo.equals(regNo) && regCount == 1) {
                regValid = true;
            } else if (!dto.regNo.equals(regNo) && regCount == 0) {
                regValid = true;
            }

            if (dto.chasisNo.equals(chasisNo) && chasisCount == 1) {
                chasisValid = true;
            } else if (!dto.chasisNo.equals(chasisNo) && chasisCount == 0) {
                chasisValid = true;
            }
        }

        String errMsg = "";
        if (language.equalsIgnoreCase("english")) {
            if (!regValid) {
                errMsg += " Duplicate Registration Number";
            }

            if (!chasisValid) {
                errMsg += " Duplicate Chasis Number";
            }
        } else {
            if (!regValid) {
                errMsg += " ডুপ্লিকেট রেজিস্ট্রেশন নম্বর ";
            }

            if (!chasisValid) {
                errMsg += " ডুপ্লিকেট চেসিস নম্বর ";
            }
        }


        validationDTO.valid = regValid && chasisValid;
        validationDTO.errMsg = errMsg;

        return validationDTO;

    }


    private void checkRegistrationAndChasisValidation(HttpServletRequest request, HttpServletResponse response) {
        try {
            System.out.println("In checkRegistrationAndChesisValidation");

            String regNo = request.getParameter("regNo");
            String chasisNo = request.getParameter("chasisNo");
            String language = request.getParameter("language");
            String type = request.getParameter("type");

            ValidationDTO validationDTO = checkRegistrationAndChasisValidation(regNo, chasisNo, language, type,
                    Long.parseLong(request.getParameter("iD")));


            PrintWriter out = response.getWriter();
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");

            String encoded = this.gson.toJson(validationDTO);
            System.out.println("json encoded data = " + encoded);
            out.print(encoded);
            out.flush();
        } catch (NumberFormatException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    private void addVm_vehicle(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, boolean isPermanentTable) throws IOException {
        // TODO Auto-generated method stub

        ApiResponse apiResponse;

        try {
            request.setAttribute("failureMessage", "");
            System.out.println("%%%% addVm_vehicle");
            String path = getServletContext().getRealPath("/img2/");
            Vm_vehicleDTO vm_vehicleDTO;
            SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");

            if (addFlag == true) {
                vm_vehicleDTO = new Vm_vehicleDTO();
            } else {
                vm_vehicleDTO = Vm_vehicleRepository.getInstance().
                        getVm_vehicleDTOByID(Long.parseLong(request.getParameter("iD")));
//						(Vm_vehicleDTO)vm_vehicleDAO.getDTOByID(Long.parseLong(request.getParameter("iD")));
            }

            String Value = "";

            Value = request.getParameter("purchaseDate");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("purchaseDate = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                Date d = f.parse(Value);
                vm_vehicleDTO.purchaseDate = d.getTime();

            } else {
//                throw new Exception(" Invalid purchase date");
            }

            Value = request.getParameter("vehicleBrandCat");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("vehicleBrandCat = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                vm_vehicleDTO.vehicleBrandCat = Integer.parseInt(Value);
            } else {
//                throw new Exception(" Invalid brand name");
            }

            Value = request.getParameter("vehicleColorCat");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("vehicleColorCat = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                vm_vehicleDTO.vehicleColorCat = Integer.parseInt(Value);
            } else {
//                throw new Exception(" Invalid color");
            }

            Value = request.getParameter("vehicleTypeCat");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("vehicleTypeCat = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                vm_vehicleDTO.vehicleTypeCat = Integer.parseInt(Value);
            } else {
//                throw new Exception(" Invalid vehicle type");
            }

            Value = request.getParameter("regNo");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("regNo = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                vm_vehicleDTO.regNo = (Value);
            } else {
//                throw new Exception(" Invalid reg No");
            }

            Value = request.getParameter("chasisNo");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("chasisNo = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                vm_vehicleDTO.chasisNo = (Value);
            } else {
//                throw new Exception(" Invalid chasis no");
            }


            // uniqueness check

            ValidationDTO validationDTO = checkRegistrationAndChasisValidation(
                    vm_vehicleDTO.regNo,
                    vm_vehicleDTO.chasisNo,
                    "english",
                    addFlag ? "add" : "edit",
                    vm_vehicleDTO.iD);

            if (!validationDTO.valid) {
//                throw new Exception(validationDTO.errMsg);
            }


            Value = request.getParameter("engineNo");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("engineNo = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                vm_vehicleDTO.engineNo = (Value);
            } else {
//                throw new Exception(" Invalid engine no");
            }

            Value = request.getParameter("numberOfSeats");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("numberOfSeats = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                vm_vehicleDTO.numberOfSeats = Integer.parseInt(Value);
            } else {
//                throw new Exception(" Invalid number of seats");
            }

            Value = request.getParameter("vehicleFuelCat");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("vehicleFuelCat = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                vm_vehicleDTO.vehicleFuelCat = Value;
            } else {
//                throw new Exception(" Invalid fuel type");
            }

            Value = request.getParameter("supplierType");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("supplierType = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                vm_vehicleDTO.supplierType = Long.parseLong(Value);
            } else {
//                throw new Exception(" Invalid supplier");
            }

            Value = request.getParameter("modelNo");

            if (Value != null) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("modelNo = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                vm_vehicleDTO.modelNo = (Value);
            } else {
//                throw new Exception(" Invalid model no");
            }

            Value = request.getParameter("purchaseAmount");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("purchaseAmount = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                vm_vehicleDTO.purchaseAmount = Double.parseDouble(Value);
            } else {
//                throw new Exception(" Invalid purchase amount");
            }

            Value = request.getParameter("manufactureYear");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("manufactureYear = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                vm_vehicleDTO.manufactureYear = Long.parseLong(Value);
            } else {
//                throw new Exception(" Invalid manufacturing year");
            }

            Value = request.getParameter("fitnessExpiryDate");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("fitnessExpiryDate = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                Date d = f.parse(Value);
                vm_vehicleDTO.fitnessExpiryDate = d.getTime();

            } else {
//                throw new Exception(" Invalid fitness expiry date");
            }

            Value = request.getParameter("taxTokenExpiryDate");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("taxTokenExpiryDate = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                Date d = f.parse(Value);
                vm_vehicleDTO.taxTokenExpiryDate = d.getTime();

            } else {
//                throw new Exception(" Invalid token expiry date");
            }

            Value = request.getParameter("digitalPlate");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("digitalPlate = " + Value);
            vm_vehicleDTO.digitalPlate = Value != null && !Value.equalsIgnoreCase("");

            Value = request.getParameter("status");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("status = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                vm_vehicleDTO.status = Integer.parseInt(Value);
            } else {
//                throw new Exception(" Invalid status");
            }

            Value = request.getParameter("filesDropzone");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            System.out.println("filesDropzone = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {

                System.out.println("filesDropzone = " + Value);

                vm_vehicleDTO.filesDropzone = Long.parseLong(Value);


                if (addFlag == false) {
                    String filesDropzoneFilesToDelete = request.getParameter("filesDropzoneFilesToDelete");
                    String[] deleteArray = filesDropzoneFilesToDelete.split(",");
                    for (int i = 0; i < deleteArray.length; i++) {
                        System.out.println("going to delete " + deleteArray[i]);
                        if (i > 0) {
                            filesDAO.delete(Long.parseLong(deleteArray[i]));
                        }
                    }
                }
            } else {
                System.out.println("FieldName has a null Value, not updating" + " = " + Value);
            }

            if (addFlag) {
                vm_vehicleDTO.insertedByUserId = userDTO.ID;
                vm_vehicleDTO.insertedByOrganogramId = userDTO.organogramID;
                Calendar c = Calendar.getInstance();
                c.set(Calendar.HOUR_OF_DAY, 0);
                c.set(Calendar.MINUTE, 0);
                c.set(Calendar.SECOND, 0);
                c.set(Calendar.MILLISECOND, 0);

                vm_vehicleDTO.insertionDate = c.getTimeInMillis();
            }


            System.out.println("Done adding  addVm_vehicle dto = " + vm_vehicleDTO);
            long returnedID = -1;

            if (isPermanentTable == false) //add new row for validation and make the old row outdated
            {
                vm_vehicleDAO.setIsDeleted(vm_vehicleDTO.iD, CommonDTO.OUTDATED);
                returnedID = vm_vehicleDAO.add(vm_vehicleDTO);
                vm_vehicleDAO.setIsDeleted(returnedID, CommonDTO.WAITING_FOR_APPROVAL);
            } else if (addFlag == true) {
                returnedID = vm_vehicleDAO.manageWriteOperations(vm_vehicleDTO, SessionConstants.INSERT, -1, userDTO);
            } else {
                returnedID = vm_vehicleDAO.manageWriteOperations(vm_vehicleDTO, SessionConstants.UPDATE, -1, userDTO);
            }


            apiResponse = ApiResponse.makeSuccessResponse("Vm_vehicleServlet?actionType=search");

        } catch (Exception e) {
            e.printStackTrace();
            apiResponse = ApiResponse.makeErrorResponse(e.getMessage());
        }

        PrintWriter pw = response.getWriter();
        pw.write(apiResponse.getJSONString());
        pw.flush();
        pw.close();
    }


    private void getVm_vehicle(HttpServletRequest request, HttpServletResponse response, long id) throws ServletException, IOException {
        System.out.println("in getVm_vehicle");
        Vm_vehicleDTO vm_vehicleDTO = null;
        try {
            vm_vehicleDTO = Vm_vehicleRepository.getInstance().getVm_vehicleDTOByID(id);
//					(Vm_vehicleDTO)vm_vehicleDAO.getDTOByID(id);
            request.setAttribute("ID", vm_vehicleDTO.iD);
            request.setAttribute("vm_vehicleDTO", vm_vehicleDTO);
            request.setAttribute("vm_vehicleDAO", vm_vehicleDAO);

            String URL = "";

            String inPlaceEdit = request.getParameter("inplaceedit");
            String inPlaceSubmit = request.getParameter("inplacesubmit");
            String getBodyOnly = request.getParameter("getBodyOnly");

            if (inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase("")) {
                URL = "vm_vehicle/vm_vehicleInPlaceEdit.jsp";
                request.setAttribute("inplaceedit", "");
            } else if (inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase("")) {
                URL = "vm_vehicle/vm_vehicleSearchRow.jsp";
                request.setAttribute("inplacesubmit", "");
            } else {
                if (getBodyOnly != null && !getBodyOnly.equalsIgnoreCase("")) {
                    URL = "vm_vehicle/vm_vehicleEditBody.jsp?actionType=edit";
                } else {
                    URL = "vm_vehicle/vm_vehicleEdit.jsp?actionType=edit";
                }
            }

            RequestDispatcher rd = request.getRequestDispatcher(URL);
            rd.forward(request, response);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void getVm_vehicle(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        getVm_vehicle(request, response, Long.parseLong(request.getParameter("ID")));
    }


    private void getByVehicleType(HttpServletRequest request, HttpServletResponse response, int id) throws ServletException, IOException {
        Vm_requisitionDAO vm_requisitionDAO = new Vm_requisitionDAO();
        Vm_requisitionDTO vm_requisitionDTO = vm_requisitionDAO.getDTOByID(Long.parseLong(request.getParameter("vmRequisitionId")));
        HashSet<Long> assignedVehicleIds = new HashSet<>(
                (List<Long>) vm_requisitionDAO.getVehicleIDs(vm_requisitionDTO)
        );

        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        String Language = LM.getText(LC.VM_REQUISITION_EDIT_LANGUAGE, loginDTO);

        Vm_vehicleDAO vm_vehicleDAO = new Vm_vehicleDAO();
        List<Vm_vehicleDTO> vm_vehicleDTOListUnprocessed = vm_vehicleDAO.getDTOsByVehicleType(id);
        List<Vm_vehicleDTO> vm_vehicleDTOList = new ArrayList<>();
        vm_vehicleDTOListUnprocessed
                .stream().filter(vehicleDTO -> !assignedVehicleIds.contains(vehicleDTO.iD))
                .forEach(vm_vehicleDTOList::add);

        String tempString = "";

        for (Vm_vehicleDTO vm_vehicleDTO : vm_vehicleDTOList) {

            String brand = CatRepository.getName(Language, "vehicle_brand", vm_vehicleDTO.vehicleBrandCat);
            String seats = Language.equals("English") ? "Number of seats: " : "আসন সংখ্যা: ";
            String option = vm_vehicleDTO.regNo + " " + brand + " " + vm_vehicleDTO.modelNo;
            tempString += "<option value='" + vm_vehicleDTO.iD + "'>" + Utils.getDigits(option + "(" + seats + vm_vehicleDTO.numberOfSeats + ")", Language) + "</option>";
        }

        PrintWriter out = response.getWriter();
        out.println(tempString);
        out.close();
    }

    private void getAllByVehicleType(HttpServletRequest request, HttpServletResponse response, int id) throws ServletException, IOException {
        long vehicleId = Long.parseLong(request.getParameter("vehicleId"));

        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        String Language = LM.getText(LC.VM_REQUISITION_EDIT_LANGUAGE, loginDTO);

        Vm_vehicleDAO vm_vehicleDAO = new Vm_vehicleDAO();
        List<Vm_vehicleDTO> vm_vehicleDTOList = vm_vehicleDAO.getDTOsByVehicleType(id);

        List<OptionDTO> list = vm_vehicleDTOList.stream()
                .map(e -> {
                    CategoryLanguageModel model = CatRepository.getInstance().getCategoryLanguageModel("vehicle_brand", e.vehicleBrandCat);
                    String brandName = model == null ? "" : model.englishText;
                    return new OptionDTO(e.regNo + " " + brandName + " " + e.modelNo + "(Number of seats: " + e.numberOfSeats + ")",
                            StringUtils.convertToBanNumber(e.regNo + " " + brandName + " " + e.modelNo + "(Number of seats: " + e.numberOfSeats + ")"), String.valueOf(e.iD));
                })
                .collect(Collectors.toList());
        PrintWriter out = response.getWriter();
        out.println(Utils.buildOptions(list, Language, String.valueOf(vehicleId)));
        out.close();
    }
    private void getVehicleFuelType(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        long vehicleId = Long.parseLong(request.getParameter("vehicleId"));

        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        String Language = LM.getText(LC.VM_REQUISITION_EDIT_LANGUAGE, loginDTO);

        Vm_vehicleDTO vehicleDTO = Vm_vehicleRepository.getInstance().getVm_vehicleDTOByID(vehicleId);
        String fuelName = CatRepository.getName(Language, "vehicle_fuel", Integer.parseInt(vehicleDTO.vehicleFuelCat));

        PrintWriter out = response.getWriter();
        out.println(gson.toJson(new FuelModel(vehicleDTO.vehicleTypeCat, fuelName)));
        out.close();
    }

    private void getByVehicleTypeForAssignment(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        int type = Integer.parseInt(request.getParameter("type"));
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        String Language = LM.getText(LC.VM_REQUISITION_EDIT_LANGUAGE, loginDTO);

        List<Vm_vehicleDTO> vm_vehicleDTOList = vm_vehicleDAO.getDTOsByVehicleType(type);

        String tempString = "";

        for (Vm_vehicleDTO vm_vehicleDTO : vm_vehicleDTOList) {

            String brand = CatRepository.getName(Language, "vehicle_brand", vm_vehicleDTO.vehicleBrandCat);
            String seats = Language.equals("English") ? "Number of seats: " : "আসন সংখ্যা: ";
            String option = vm_vehicleDTO.regNo + " " + brand + " " + vm_vehicleDTO.modelNo;
            tempString += "<option value='" + vm_vehicleDTO.iD + "'>" + Utils.getDigits(option + "(" + seats + vm_vehicleDTO.numberOfSeats + ")", Language) + "</option>";
        }

        PrintWriter out = response.getWriter();
        out.println(tempString);
        out.close();
    }


    private void getByVehicleType(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        getByVehicleType(request, response, Integer.parseInt(request.getParameter("ID")));
    }

    private void getAllByVehicleType(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        getAllByVehicleType(request, response, Integer.parseInt(request.getParameter("ID")));
    }


    private void searchVm_vehicle(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException {
        System.out.println("in  searchVm_vehicle 1");
        LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        String ajax = request.getParameter("ajax");
        boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);

        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
                SessionConstants.NAV_VM_VEHICLE,
                request,
                vm_vehicleDAO,
                SessionConstants.VIEW_VM_VEHICLE,
                SessionConstants.SEARCH_VM_VEHICLE,
                tableName,
                isPermanent,
                userDTO,
                filter,
                true);
        try {
            System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        } catch (Exception e) {
            System.out.println("failed to dojob" + e);
        }

        request.setAttribute("vm_vehicleDAO", vm_vehicleDAO);
        RequestDispatcher rd;
        if (!isPermanent) {
            if (hasAjax == false) {
                System.out.println("Going to vm_vehicle/vm_vehicleApproval.jsp");
                rd = request.getRequestDispatcher("vm_vehicle/vm_vehicleApproval.jsp");
            } else {
                System.out.println("Going to vm_vehicle/vm_vehicleApprovalForm.jsp");
                rd = request.getRequestDispatcher("vm_vehicle/vm_vehicleApprovalForm.jsp");
            }
        } else {
            if (hasAjax == false) {
                System.out.println("Going to vm_vehicle/vm_vehicleSearch.jsp");
                rd = request.getRequestDispatcher("vm_vehicle/vm_vehicleSearch.jsp");
            } else {
                System.out.println("Going to vm_vehicle/vm_vehicleSearchForm.jsp");
                rd = request.getRequestDispatcher("vm_vehicle/vm_vehicleSearchForm.jsp");
            }
        }
        rd.forward(request, response);
    }


    private void getReportData(HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        System.out.println("In getReportData");

        String Language = request.getParameter("language");
        long vehicleId = Long.parseLong(request.getParameter("vehicleId"));
        String fiscalYearIdsInString = request.getParameter("fiscalYearIds");
        //System.out.println("fiscalYearIdsInString: "+fiscalYearIdsInString);
        List<String> fiscalYearIds = Arrays.asList(fiscalYearIdsInString.split(","));


        Boolean allSelected = false;
        if (fiscalYearIds.contains("-100")) {
            allSelected = true;
        }

        List<Long> fiscalYearIdsInLong = fiscalYearIds.stream()
                .map(s -> Long.parseLong(s))
                .collect(Collectors.toList());

        Vm_vehicleReportDTO reportDTO = new Vm_vehicleReportDTO();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
        reportDTO.vm_vehicleDTO = Vm_vehicleRepository.getInstance().getVm_vehicleDTOByID(vehicleId);
        if (reportDTO.vm_vehicleDTO != null) {

            String formatted_purchaseDate = simpleDateFormat.format(new Date(Long.parseLong
                    (reportDTO.vm_vehicleDTO.purchaseDate + "")));
            reportDTO.vm_vehicleDTO.purchaseDateInString =
                    Utils.getDigits(formatted_purchaseDate, Language);


            String value = Asset_supplierRepository.getInstance().
                    getText(Language, reportDTO.vm_vehicleDTO.supplierType);
            reportDTO.vm_vehicleDTO.supplierName = Utils.getDigits(value, Language);
        }

        // maintenance Data
        Vm_maintenanceDAO vm_maintenanceDAO = new Vm_maintenanceDAO();
        VmMaintenanceItemDAO itemDAO = new VmMaintenanceItemDAO();
        List<Vm_maintenanceDTO> maintenanceDTOS = vm_maintenanceDAO.
                getAllApprovedVm_maintenanceByVehicleIdAndFiscalYear(vehicleId, fiscalYearIdsInLong, !allSelected);

        VmTaxTokenItemDAO vmTaxTokenItemDAO = new VmTaxTokenItemDAO("vm_tax_token_item");
        List<VmTaxTokenItemDTO> vmTaxTokenItemDTOS = vmTaxTokenItemDAO.
                getAllApprovedVm_taxTokenFeesByVehicleIdAndFiscalYear(vehicleId, fiscalYearIdsInLong, allSelected);

        reportDTO.maintenanceDTOListForMaintenance = new ArrayList<>();
        reportDTO.maintenanceDTOListForRepair = new ArrayList<>();

        reportDTO.VmTaxTokenItemListForTax = new ArrayList<>();

        Fiscal_yearDAO fiscal_yearDAO = new Fiscal_yearDAO();
        List<Fiscal_yearDTO> fiscal_yearDTOS = Fiscal_yearRepository.getInstance().getFiscal_yearList();
//				fiscal_yearDAO.getAllFiscal_year(true);

        for (VmTaxTokenItemDTO vmTaxTokenItemDTO : vmTaxTokenItemDTOS) {

            Fiscal_yearDTO fiscal_yearDTO = fiscal_yearDAO.getFiscalYearFromList(fiscal_yearDTOS, vmTaxTokenItemDTO.fiscalYearId);

            double fiscalYearCost = 0;

            if (vmTaxTokenItemDTO.taxTokenFees > 0 && vmTaxTokenItemDTO.fitnessFees > 0) {
                fiscalYearCost = (vmTaxTokenItemDTO.taxTokenFees + vmTaxTokenItemDTO.fitnessFees);
            }

            if (fiscalYearCost > 0) {
                reportDTO.totalTaxTokenAndFitnessCost += fiscalYearCost;
            }


            ReportItemDTO reportItemDTO = new ReportItemDTO();

            reportItemDTO.taxTokenFiscalYearID = fiscal_yearDTO.id;
            reportItemDTO.fiscalYearBn = fiscal_yearDTO.nameBn;
            reportItemDTO.fiscalYearEn = fiscal_yearDTO.nameEn;

            reportItemDTO.taxTokenFiscalYearCost = fiscalYearCost > 0 ? fiscalYearCost : 0;
            reportDTO.VmTaxTokenItemListForTax.add(reportItemDTO);

        }

        for (Vm_maintenanceDTO maintenanceDTO : maintenanceDTOS) {
            reportDTO.totalMaintenanceCost += maintenanceDTO.totalWithVat;

            String approvedDate = simpleDateFormat.format(new Date(Long.parseLong
                    (maintenanceDTO.aoApproveDate + "")));
            double maintenanceValue = 0.0;
            double repairValue = 0.0;

//			System.out.println(maintenanceDTO.vmMaintenanceItemDTOList);itemDAO.getVmMaintenanceItemDTOListByVmMaintenanceID
            List<VmMaintenanceItemDTO> itemDTOS = VmMaintenanceItemRepository.getInstance().getVmMaintenanceItemDTOByMaintenanceId
                    (maintenanceDTO.iD).stream().filter(i -> i.selected).collect(Collectors.toList());

//			System.out.println(itemDTOS);

            for (VmMaintenanceItemDTO itemDTO : itemDTOS) {
                if (itemDTO.vehicleMaintenanceCat == 2) {
                    double v = itemDTO.ao_price + ((itemDTO.ao_price * maintenanceDTO.vatPercentage) / 100);
//					System.out.println(v);
                    maintenanceValue += v;
                } else {
                    double v = itemDTO.ao_price + ((itemDTO.ao_price * maintenanceDTO.vatPercentage) / 100);
//					System.out.println(v);
                    repairValue += v;
                }

            }

//			System.out.println(maintenanceValue);
//			System.out.println(repairValue);

            if (maintenanceValue > 0.0) {
                ReportItemDTO reportItemDTO = new ReportItemDTO();
                reportItemDTO.amount = (int) (Math.round(maintenanceValue * 100)) / 100.0;
                reportItemDTO.date = Utils.getDigits(approvedDate, Language);
                reportDTO.maintenanceDTOListForMaintenance.add(reportItemDTO);
            }

            if (repairValue > 0.0) {
                ReportItemDTO reportItemDTO = new ReportItemDTO();
                reportItemDTO.amount = (int) (Math.round(repairValue * 100)) / 100.0;
                reportItemDTO.date = Utils.getDigits(approvedDate, Language);
                reportDTO.maintenanceDTOListForRepair.add(reportItemDTO);
            }
        }

        reportDTO.totalMaintenanceCost = (int) (Math.round(reportDTO.totalMaintenanceCost * 100)) / 100.0;

//		System.out.println(maintenanceDTOS);

        // maintenance end


        PrintWriter out = response.getWriter();
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");

        String encoded = this.gson.toJson(reportDTO);
        System.out.println("json encoded data = " + encoded);
        out.print(encoded);
        out.flush();

    }


}

