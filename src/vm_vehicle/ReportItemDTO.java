package vm_vehicle;

public class ReportItemDTO
{

	public String date = "";
	public Double amount = 0.0;

	public long taxTokenFiscalYearID =-1;
	public double taxTokenFiscalYearCost =0.0;

	public String fiscalYearBn = "";
	public String fiscalYearEn = "";



    @Override
	public String toString() {
            return "ReportItemDTO[" +
            " date = " + date +
            " amount = " + amount +
			" taxTokenFiscalYearID = " + taxTokenFiscalYearID +
			" taxTokenFiscalYearCost = " + taxTokenFiscalYearCost +
			" fiscalYearBn = " + fiscalYearBn +
			" fiscalYearEn = " + fiscalYearEn +

            "]";
    }

}