package vm_vehicle;

import org.apache.log4j.Logger;
import pb.CatRepository;
import user.UserDTO;
import util.CommonDTO;
import util.NavigationService4;
import util.UtilCharacter;
import vm_requisition.Vm_requisitionDTO;
import vm_requisition.Vm_requisitionRepository;
import vm_vehicle_driver_assignment.Vm_vehicle_driver_assignmentDTO;
import vm_vehicle_driver_assignment.Vm_vehicle_driver_assignmentRepository;
import vm_vehicle_office_assignment.Vm_vehicle_office_assignmentDTO;
import vm_vehicle_office_assignment.Vm_vehicle_office_assignmentRepository;

import javax.servlet.http.HttpServletRequest;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;

public class Vm_vehicleDAO extends NavigationService4 {

    Logger logger = Logger.getLogger(getClass());
    public static HashMap<Integer, String[]> statusMap = new HashMap() {{
        put(1, new String[]{"Active", "সক্রিয়"});
        put(2, new String[]{"In Active", "নিষ্ক্রিয় "});
    }};


    public static String getStatus(int value, String language) {
        String[] s = statusMap.get(value);
        if (s != null) {
            return UtilCharacter.getDataByLanguage(language, s[1], s[0]);

        } else {
            return UtilCharacter.getDataByLanguage(language, "পাওয়া যায় নি ", "Not Available");
        }

    }

    public static String getStatusList(String Language, int value) {

        String selected = "";
        String sOptions = "";
        if (Language.equals("English")) {
            sOptions = "<option value = '-1' " + selected + ">Select</option>";
        } else {
            sOptions = "<option value = '-1' " + selected + ">বাছাই করুন</option>";
        }

        for (int i = 1; i <= statusMap.size(); i++) {
            selected = i == value ? "selected" : "";
            if (Language.equals("English")) {
                sOptions += "<option value = '" + i + "' " + selected + " >" + statusMap.get(i)[0] + "</option>";
            } else {
                sOptions += "<option value = '" + i + "' " + selected + ">" + statusMap.get(i)[1] + "</option>";
            }
        }

        return sOptions;
    }

    public static String getYears(String Language, int value) {
        String sOptions = "";
        String selected = "";
        if (Language.equals("English")) {
            sOptions = "<option value = '-1' " + selected + ">Select</option>";
        } else {
            sOptions = "<option value = '-1' " + selected + ">বাছাই করুন</option>";
        }

        int curYear = Calendar.getInstance().get(Calendar.YEAR);

        for (int i = 1950; i <= curYear; i++) {
            selected = i == value ? "selected" : "";
            sOptions += Language.equalsIgnoreCase("English") ? "<option value = '" + i + "' " + selected + " >"
                    + i + "</option>" : "<option value = '" + i + "' " + selected + " >"
                    + UtilCharacter.convertNumberEnToBn(String.valueOf(i)) + "</option>";
        }
        return sOptions;
    }


    public Vm_vehicleDAO(String tableName) {
        super(tableName);
        joinSQL = "";
        commonMaps = new Vm_vehicleMAPS(tableName);
        columnNames = new String[]
                {
                        "ID",
                        "purchase_date",
                        "vehicle_brand_cat",
                        "vehicle_color_cat",
                        "vehicle_type_cat",
                        "reg_no",
                        "chasis_no",
                        "engine_no",
                        "number_of_seats",
                        "vehicle_fuel_cat",
                        "supplier_type",
                        "model_no",
                        "purchase_amount",
                        "manufacture_year",
                        "last_maintenance_date",
                        "last_maintenance_id",
                        "fitness_expiry_date",
                        "tax_token_expiry_date",
                        "digital_plate",
                        "status",
                        "files_dropzone",
                        "inserted_by_user_id",
                        "inserted_by_organogram_id",
                        "insertion_date",
                        "search_column",
                        "isDeleted",
                        "lastModificationTime"
                };
    }

    public Vm_vehicleDAO() {
        this("vm_vehicle");
    }

    public void setSearchColumn(Vm_vehicleDTO vm_vehicleDTO) {
        vm_vehicleDTO.searchColumn = "";
        vm_vehicleDTO.searchColumn += CatRepository.getName("English", "vehicle_brand", vm_vehicleDTO.vehicleBrandCat) + " " + CatRepository.getName("Bangla", "vehicle_brand", vm_vehicleDTO.vehicleBrandCat) + " ";
        vm_vehicleDTO.searchColumn += CatRepository.getName("English", "vehicle_color", vm_vehicleDTO.vehicleColorCat) + " " + CatRepository.getName("Bangla", "vehicle_color", vm_vehicleDTO.vehicleColorCat) + " ";
        vm_vehicleDTO.searchColumn += CatRepository.getName("English", "vehicle_type", vm_vehicleDTO.vehicleTypeCat) + " " + CatRepository.getName("Bangla", "vehicle_type", vm_vehicleDTO.vehicleTypeCat) + " ";
        vm_vehicleDTO.searchColumn += vm_vehicleDTO.regNo + " ";
        vm_vehicleDTO.searchColumn += vm_vehicleDTO.chasisNo + " ";
        vm_vehicleDTO.searchColumn += vm_vehicleDTO.engineNo + " ";
        String[] arr = vm_vehicleDTO.vehicleFuelCat.split(",");
        if (arr.length > 1) {
            vm_vehicleDTO.searchColumn += CatRepository.getInstance().getText("English", "vehicle_fuel", Long.valueOf(arr[0].trim())) + " " + CatRepository.getInstance().getText("English", "vehicle_fuel", Long.valueOf(arr[1].trim())) + " ";
            vm_vehicleDTO.searchColumn += CatRepository.getInstance().getText("Bangla", "vehicle_fuel", Long.valueOf(arr[0].trim())) + " " + CatRepository.getInstance().getText("Bangla", "vehicle_fuel", Long.valueOf(arr[1].trim())) + " ";
        } else {
            vm_vehicleDTO.searchColumn += CatRepository.getInstance().getText("English", "vehicle_fuel", Long.valueOf(vm_vehicleDTO.vehicleFuelCat)) + " " + CatRepository.getInstance().getText("Bangla", "vehicle_fuel", Long.valueOf(vm_vehicleDTO.vehicleFuelCat)) + " ";
        }
        vm_vehicleDTO.searchColumn += vm_vehicleDTO.modelNo + " ";

    }

    public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException {
        Vm_vehicleDTO vm_vehicleDTO = (Vm_vehicleDTO) commonDTO;
        int index = 1;
        long lastModificationTime = System.currentTimeMillis();
        setSearchColumn(vm_vehicleDTO);
        if (isInsert) {
            ps.setObject(index++, vm_vehicleDTO.iD);
        }
        ps.setObject(index++, vm_vehicleDTO.purchaseDate);
        ps.setObject(index++, vm_vehicleDTO.vehicleBrandCat);
        ps.setObject(index++, vm_vehicleDTO.vehicleColorCat);
        ps.setObject(index++, vm_vehicleDTO.vehicleTypeCat);
        ps.setObject(index++, vm_vehicleDTO.regNo);
        ps.setObject(index++, vm_vehicleDTO.chasisNo);
        ps.setObject(index++, vm_vehicleDTO.engineNo);
        ps.setObject(index++, vm_vehicleDTO.numberOfSeats);
        ps.setObject(index++, vm_vehicleDTO.vehicleFuelCat);
        ps.setObject(index++, vm_vehicleDTO.supplierType);
        ps.setObject(index++, vm_vehicleDTO.modelNo);
        ps.setObject(index++, vm_vehicleDTO.purchaseAmount);
        ps.setObject(index++, vm_vehicleDTO.manufactureYear);
        ps.setObject(index++, vm_vehicleDTO.last_maintenance_date);
        ps.setObject(index++, vm_vehicleDTO.last_maintenance_id);
        ps.setObject(index++, vm_vehicleDTO.fitnessExpiryDate);
        ps.setObject(index++, vm_vehicleDTO.taxTokenExpiryDate);
        ps.setObject(index++, vm_vehicleDTO.digitalPlate);
        ps.setObject(index++, vm_vehicleDTO.status);
        ps.setObject(index++, vm_vehicleDTO.filesDropzone);
        ps.setObject(index++, vm_vehicleDTO.insertedByUserId);
        ps.setObject(index++, vm_vehicleDTO.insertedByOrganogramId);
        ps.setObject(index++, vm_vehicleDTO.insertionDate);
        ps.setObject(index++, vm_vehicleDTO.searchColumn);
        if (isInsert) {
            ps.setObject(index++, 0);
            ps.setObject(index++, lastModificationTime);
        }
    }


    public Vm_vehicleDTO build(ResultSet rs) {
        try {
            Vm_vehicleDTO vm_vehicleDTO = new Vm_vehicleDTO();
            vm_vehicleDTO.iD = rs.getLong("ID");
            vm_vehicleDTO.purchaseDate = rs.getLong("purchase_date");
            vm_vehicleDTO.vehicleBrandCat = rs.getInt("vehicle_brand_cat");
            vm_vehicleDTO.vehicleColorCat = rs.getInt("vehicle_color_cat");
            vm_vehicleDTO.vehicleTypeCat = rs.getInt("vehicle_type_cat");
            vm_vehicleDTO.regNo = rs.getString("reg_no");
            vm_vehicleDTO.chasisNo = rs.getString("chasis_no");
            vm_vehicleDTO.engineNo = rs.getString("engine_no");
            vm_vehicleDTO.numberOfSeats = rs.getInt("number_of_seats");
            vm_vehicleDTO.vehicleFuelCat = rs.getString("vehicle_fuel_cat");
            vm_vehicleDTO.supplierType = rs.getLong("supplier_type");
            vm_vehicleDTO.modelNo = rs.getString("model_no");
            vm_vehicleDTO.purchaseAmount = rs.getDouble("purchase_amount");
            vm_vehicleDTO.manufactureYear = rs.getLong("manufacture_year");
            vm_vehicleDTO.fitnessExpiryDate = rs.getLong("fitness_expiry_date");
            vm_vehicleDTO.taxTokenExpiryDate = rs.getLong("tax_token_expiry_date");
            vm_vehicleDTO.last_maintenance_date = rs.getLong("last_maintenance_date");
            vm_vehicleDTO.last_maintenance_id = rs.getLong("last_maintenance_id");
            vm_vehicleDTO.digitalPlate = rs.getBoolean("digital_plate");
            vm_vehicleDTO.status = rs.getInt("status");
            vm_vehicleDTO.filesDropzone = rs.getLong("files_dropzone");
            vm_vehicleDTO.insertedByUserId = rs.getLong("inserted_by_user_id");
            vm_vehicleDTO.insertedByOrganogramId = rs.getLong("inserted_by_organogram_id");
            vm_vehicleDTO.insertionDate = rs.getLong("insertion_date");
            vm_vehicleDTO.isDeleted = rs.getInt("isDeleted");
            vm_vehicleDTO.lastModificationTime = rs.getLong("lastModificationTime");
            vm_vehicleDTO.searchColumn = rs.getString("search_column");
            return vm_vehicleDTO;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    public Vm_vehicleDTO buildForCount(ResultSet rs) {
        try {
            Vm_vehicleDTO vm_vehicleDTO = new Vm_vehicleDTO();
            vm_vehicleDTO.count = rs.getInt("count");
            return vm_vehicleDTO;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }


    public List<Vm_vehicleDTO> getDTOsByVehicleType(int vehicleTypeCat) {

        return Vm_vehicleRepository.getInstance().getVm_vehicleDTOByvehicle_type_cat(vehicleTypeCat)
                .stream()
                .filter(i -> i.status == 1)
                .sorted((f1, f2) -> Long.compare(f2.lastModificationTime, f1.lastModificationTime))
                .collect(Collectors.toList());

    }


    public String getSqlWithSearchCriteriaForNormalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
                                                          UserDTO userDTO, String filter, boolean tableHasJobCat, List<Object> objectList) {
        boolean viewAll = false;

        if (p_searchCriteria != null && p_searchCriteria.get("ViewAll") != null) {
            System.out.println("ViewAll = " + p_searchCriteria.get("ViewAll"));
            viewAll = true;
        } else {
            System.out.println("ViewAll is null ");
        }

        String sql = "SELECT ";
        if (category == GETIDS) {
            sql += " " + tableName + ".ID as ID ";
        } else if (category == GETCOUNT) {
            sql += " count( " + tableName + ".ID) as countID ";
        } else if (category == GETDTOS) {
            sql += " distinct " + tableName + ".* ";
        }
        sql += "FROM " + tableName + " ";
        sql += joinSQL;


        String AnyfieldSql = "";
        String AllFieldSql = "";

        if (p_searchCriteria != null) {


            Enumeration names = p_searchCriteria.keys();
            String str, value;

            AnyfieldSql = "(";

            if (p_searchCriteria.get("AnyField") != null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase("")) {
                AnyfieldSql += tableName + ".search_column like ? ";
                objectList.add("%" + p_searchCriteria.get("AnyField").toString() + "%");
            }
            AnyfieldSql += ")";
            System.out.println("AnyfieldSql = " + AnyfieldSql);

            AllFieldSql = "(";
            int i = 0;
            while (names.hasMoreElements()) {
                str = (String) names.nextElement();
                value = (String) p_searchCriteria.get(str);
                System.out.println(str + ": " + value);
                if (value != null && !value.equalsIgnoreCase("") && (
                        str.equals("vehicle_type_cat")
                                || str.equals("reg_no")
                                || str.equals("model_no")
                                || str.equals("fitness_expiry_date_start")
                                || str.equals("fitness_expiry_date_end")
                                || str.equals("tax_token_expiry_date_start")
                                || str.equals("tax_token_expiry_date_end")
                )

                ) {
                    if (p_searchCriteria.get(str).equals("any")) {
                        continue;
                    }

                    if (i > 0) {
                        AllFieldSql += " AND  ";
                    }
                    if (str.equals("vehicle_type_cat")) {
                        AllFieldSql += "" + tableName + ".vehicle_type_cat = ? ";
                        objectList.add(p_searchCriteria.get(str));
                        i++;
                    } else if (str.equals("reg_no")) {
                        AllFieldSql += "" + tableName + ".reg_no like ? ";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
                        i++;
                    }
                    else if (str.equals("model_no")) {
                        AllFieldSql += "" + tableName + ".model_no like '%" + p_searchCriteria.get(str) + "%'";
                        i++;
                    } else if (str.equals("fitness_expiry_date_start")) {
                        AllFieldSql += "" + tableName + ".fitness_expiry_date >= " + p_searchCriteria.get(str);
                        i++;
                    } else if (str.equals("fitness_expiry_date_end")) {
                        AllFieldSql += "" + tableName + ".fitness_expiry_date <= " + p_searchCriteria.get(str);
                        i++;
                    } else if (str.equals("tax_token_expiry_date_start")) {
                        AllFieldSql += "" + tableName + ".tax_token_expiry_date >= " + p_searchCriteria.get(str);
                        i++;
                    } else if (str.equals("tax_token_expiry_date_end")) {
                        AllFieldSql += "" + tableName + ".tax_token_expiry_date <= " + p_searchCriteria.get(str);
                        i++;
                    }

                }
            }

            AllFieldSql += ")";
            System.out.println("AllFieldSql = " + AllFieldSql);


        }


        sql += " WHERE ";

        sql += " (" + tableName + ".isDeleted = 0 ";
        sql += ")";


        if (!filter.equalsIgnoreCase("")) {
            sql += " and " + filter + " ";
        }

        if (!AnyfieldSql.equals("()") && !AnyfieldSql.equals("")) {
            sql += " AND " + AnyfieldSql;

        }
        if (!AllFieldSql.equals("()") && !AllFieldSql.equals("")) {
            sql += " AND " + AllFieldSql;
        }


        sql += " order by " + tableName + ".lastModificationTime desc ";

        printSql(sql);

        if (limit >= 0) {
            sql += " limit " + limit;
        }
        if (offset >= 0) {
            sql += " offset " + offset;
        }

        System.out.println("-------------- sql = " + sql);

        return sql;
    }


    public int getCountOfRegNo(String regNo) {
        return (int) Vm_vehicleRepository.getInstance().getVm_vehicleList().
                stream().filter(i -> i.regNo.equals(regNo)).count();
    }

    public int getCountOfChasisNo(String chasisNo) {

        return (int) Vm_vehicleRepository.getInstance().getVm_vehicleList().
                stream().filter(i -> i.chasisNo.equals(chasisNo)).count();

    }

    public List<Vm_vehicleDTO> getAllVm_vehicleCountByType() {
        List<Vm_vehicleDTO> vehicleDTOS = Vm_vehicleRepository.getInstance().getVm_vehicleList();
        List<Vm_vehicleDTO> dtos = new ArrayList<>();

        Set<Integer> vehicleTypes = vehicleDTOS.stream().map(i -> i.vehicleTypeCat).collect(Collectors.toSet());
        vehicleTypes.forEach(i -> {
            Vm_vehicleDTO vm_vehicleDTO = new Vm_vehicleDTO();
            vm_vehicleDTO.vehicleTypeCat = i;
            vm_vehicleDTO.count = vehicleDTOS.stream().filter(j -> j.vehicleTypeCat == i).count();
            dtos.add(vm_vehicleDTO);
        });

        return dtos;

    }

    public boolean vehicleAlreadyInUse(HttpServletRequest request) {
        List<Long> ids = Arrays.stream(request.getParameterValues("ID")).map(Long::parseLong).collect(Collectors.toList());
        List<Vm_requisitionDTO> vm_requisitionDTOS = Vm_requisitionRepository.getInstance().getVm_requisitionList();
        List<Vm_vehicle_office_assignmentDTO> vm_vehicle_office_assignmentDTOS = Vm_vehicle_office_assignmentRepository.getInstance().getVm_vehicle_office_assignmentList();
        List<Vm_vehicle_driver_assignmentDTO> vm_vehicle_driver_assignmentDTOS = Vm_vehicle_driver_assignmentRepository.getInstance().getVm_vehicle_driver_assignmentList();
        return vm_requisitionDTOS.stream().anyMatch(e -> ids.contains(e.givenVehicleId)) ||
                vm_vehicle_office_assignmentDTOS.stream().anyMatch(e -> ids.contains(e.vehicleId)) ||
                vm_vehicle_driver_assignmentDTOS.stream().anyMatch(e -> Arrays.stream(e.vehicleIdMultiple.split(",")).anyMatch(f -> ids.contains(Long.parseLong(f))));

    }


}
	