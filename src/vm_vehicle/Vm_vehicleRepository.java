package vm_vehicle;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import com.google.gson.Gson;
import election_constituency.Election_constituencyDTO;
import org.apache.log4j.Logger;

import pb.Utils;
import repository.Repository;
import repository.RepositoryManager;
import vm_route.VmRouteStoppageDTO;


public class Vm_vehicleRepository implements Repository {
	Vm_vehicleDAO vm_vehicleDAO = new Vm_vehicleDAO();
	Gson gson = new Gson();
	
	public void setDAO(Vm_vehicleDAO vm_vehicleDAO)
	{
		this.vm_vehicleDAO = vm_vehicleDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Vm_vehicleRepository.class);
	Map<Long, Vm_vehicleDTO>mapOfVm_vehicleDTOToiD;
	Map<Integer, Set<Vm_vehicleDTO> > mapOfVm_vehicleDTOTovehicleTypeCat;

//	Map<Long, Set<Vm_vehicleDTO> >mapOfVm_vehicleDTOTopurchaseDate;
//	Map<Integer, Set<Vm_vehicleDTO> >mapOfVm_vehicleDTOTovehicleBrandCat;
//	Map<Integer, Set<Vm_vehicleDTO> >mapOfVm_vehicleDTOTovehicleColorCat;

//	Map<String, Set<Vm_vehicleDTO> >mapOfVm_vehicleDTOToregNo;
//	Map<String, Set<Vm_vehicleDTO> >mapOfVm_vehicleDTOTochasisNo;
//	Map<String, Set<Vm_vehicleDTO> >mapOfVm_vehicleDTOToengineNo;
//	Map<Integer, Set<Vm_vehicleDTO> >mapOfVm_vehicleDTOTonumberOfSeats;
//	Map<Integer, Set<Vm_vehicleDTO> >mapOfVm_vehicleDTOTovehicleFuelCat;
//	Map<Long, Set<Vm_vehicleDTO> >mapOfVm_vehicleDTOTosupplierType;
//	Map<String, Set<Vm_vehicleDTO> >mapOfVm_vehicleDTOTomodelNo;
//	Map<Double, Set<Vm_vehicleDTO> >mapOfVm_vehicleDTOTopurchaseAmount;
//	Map<Long, Set<Vm_vehicleDTO> >mapOfVm_vehicleDTOTomanufactureYear;
//	Map<Long, Set<Vm_vehicleDTO> >mapOfVm_vehicleDTOTofitnessExpiryDate;
//	Map<Long, Set<Vm_vehicleDTO> >mapOfVm_vehicleDTOTotaxTokenExpiryDate;
//	Map<Boolean, Set<Vm_vehicleDTO> >mapOfVm_vehicleDTOTodigitalPlate;
//	Map<Integer, Set<Vm_vehicleDTO> >mapOfVm_vehicleDTOTostatus;
//	Map<Long, Set<Vm_vehicleDTO> >mapOfVm_vehicleDTOTofilesDropzone;
//	Map<Long, Set<Vm_vehicleDTO> >mapOfVm_vehicleDTOToinsertedByUserId;
//	Map<Long, Set<Vm_vehicleDTO> >mapOfVm_vehicleDTOToinsertedByOrganogramId;
//	Map<Long, Set<Vm_vehicleDTO> >mapOfVm_vehicleDTOToinsertionDate;
//	Map<Long, Set<Vm_vehicleDTO> >mapOfVm_vehicleDTOTolastModificationTime;
//	Map<String, Set<Vm_vehicleDTO> >mapOfVm_vehicleDTOTosearchColumn;


	static Vm_vehicleRepository instance = null;  
	private Vm_vehicleRepository(){
		mapOfVm_vehicleDTOToiD = new ConcurrentHashMap<>();
//		mapOfVm_vehicleDTOTopurchaseDate = new ConcurrentHashMap<>();
		mapOfVm_vehicleDTOTovehicleTypeCat = new ConcurrentHashMap<>();
//		mapOfVm_vehicleDTOTovehicleBrandCat = new ConcurrentHashMap<>();
//		mapOfVm_vehicleDTOTovehicleColorCat = new ConcurrentHashMap<>();

//		mapOfVm_vehicleDTOToregNo = new ConcurrentHashMap<>();
//		mapOfVm_vehicleDTOTochasisNo = new ConcurrentHashMap<>();
//		mapOfVm_vehicleDTOToengineNo = new ConcurrentHashMap<>();
//		mapOfVm_vehicleDTOTonumberOfSeats = new ConcurrentHashMap<>();
//		mapOfVm_vehicleDTOTovehicleFuelCat = new ConcurrentHashMap<>();
//		mapOfVm_vehicleDTOTosupplierType = new ConcurrentHashMap<>();
//		mapOfVm_vehicleDTOTomodelNo = new ConcurrentHashMap<>();
//		mapOfVm_vehicleDTOTopurchaseAmount = new ConcurrentHashMap<>();
//		mapOfVm_vehicleDTOTomanufactureYear = new ConcurrentHashMap<>();
//		mapOfVm_vehicleDTOTofitnessExpiryDate = new ConcurrentHashMap<>();
//		mapOfVm_vehicleDTOTotaxTokenExpiryDate = new ConcurrentHashMap<>();
//		mapOfVm_vehicleDTOTodigitalPlate = new ConcurrentHashMap<>();
//		mapOfVm_vehicleDTOTostatus = new ConcurrentHashMap<>();
//		mapOfVm_vehicleDTOTofilesDropzone = new ConcurrentHashMap<>();
//		mapOfVm_vehicleDTOToinsertedByUserId = new ConcurrentHashMap<>();
//		mapOfVm_vehicleDTOToinsertedByOrganogramId = new ConcurrentHashMap<>();
//		mapOfVm_vehicleDTOToinsertionDate = new ConcurrentHashMap<>();
//		mapOfVm_vehicleDTOTolastModificationTime = new ConcurrentHashMap<>();
//		mapOfVm_vehicleDTOTosearchColumn = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Vm_vehicleRepository getInstance(){
		if (instance == null){
			instance = new Vm_vehicleRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(vm_vehicleDAO == null)
		{
			return;
		}
		try {
			List<Vm_vehicleDTO> vm_vehicleDTOs = (List<Vm_vehicleDTO>) vm_vehicleDAO.getAll(reloadAll);
			for(Vm_vehicleDTO vm_vehicleDTO : vm_vehicleDTOs) {
				Vm_vehicleDTO oldVm_vehicleDTO = getVm_vehicleDTOByIDWithoutClone(vm_vehicleDTO.iD);
				if( oldVm_vehicleDTO != null ) {
					mapOfVm_vehicleDTOToiD.remove(oldVm_vehicleDTO.iD);

					if(mapOfVm_vehicleDTOTovehicleTypeCat.containsKey(oldVm_vehicleDTO.vehicleTypeCat)) {
						mapOfVm_vehicleDTOTovehicleTypeCat.get(oldVm_vehicleDTO.vehicleTypeCat).remove(oldVm_vehicleDTO);
					}
					if(mapOfVm_vehicleDTOTovehicleTypeCat.get(oldVm_vehicleDTO.vehicleTypeCat).isEmpty()) {
						mapOfVm_vehicleDTOTovehicleTypeCat.remove(oldVm_vehicleDTO.vehicleTypeCat);
					}

					
					
				}
				if(vm_vehicleDTO.isDeleted == 0) 
				{
					
					mapOfVm_vehicleDTOToiD.put(vm_vehicleDTO.iD, vm_vehicleDTO);

					if( ! mapOfVm_vehicleDTOTovehicleTypeCat.containsKey(vm_vehicleDTO.vehicleTypeCat)) {
						mapOfVm_vehicleDTOTovehicleTypeCat.put(vm_vehicleDTO.vehicleTypeCat, new HashSet<>());
					}
					mapOfVm_vehicleDTOTovehicleTypeCat.get(vm_vehicleDTO.vehicleTypeCat).add(vm_vehicleDTO);


				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Vm_vehicleDTO> getVm_vehicleList() {
		List <Vm_vehicleDTO> vm_vehicles = new ArrayList<Vm_vehicleDTO>(this.mapOfVm_vehicleDTOToiD.values());
		return clone(vm_vehicles);
	}
	
	
	public Vm_vehicleDTO getVm_vehicleDTOByIDWithoutClone( long ID){
		return mapOfVm_vehicleDTOToiD.get(ID);
	}

	public Vm_vehicleDTO getVm_vehicleDTOByID( long ID){
		return clone(mapOfVm_vehicleDTOToiD.get(ID));
	}

	public List<Vm_vehicleDTO> getVm_vehicleDTOByvehicle_type_cat(int vehicle_type_cat) {
		return clone(new ArrayList<>( mapOfVm_vehicleDTOTovehicleTypeCat.
				getOrDefault(vehicle_type_cat,new HashSet<>())));
	}

	public Vm_vehicleDTO clone(Vm_vehicleDTO dto) {
		String raw = gson.toJson(dto);
		return gson.fromJson(raw, Vm_vehicleDTO.class);
	}

	public List<Vm_vehicleDTO> clone(List<Vm_vehicleDTO> dtoList) {
		return dtoList
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "vm_vehicle";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}

	public String getText(long id,String language){
		Vm_vehicleDTO dto = getVm_vehicleDTOByID(id);
		return dto==null?"": Utils.getDigits(dto.regNo, language);//("Bangla".equalsIgnoreCase(language)?dto.regNo: dto.regNo);
	}

	public String getModelOptions(String Language) {
		String sOptionInit;
		if (Language.equals("English")) {
			sOptionInit = "<option value = ''>Select</option>";
		} else {
			sOptionInit = "<option value = ''>বাছাই করুন</option>";
		}
		return sOptionInit + getVm_vehicleList()
				.stream()
				.map(vm_vehicleDTO -> {
					String sOption;
					sOption = "<option value = '" + vm_vehicleDTO.modelNo + "'>";
					sOption += vm_vehicleDTO.modelNo;
					sOption += "</option>";
					return sOption;
				})
				.collect(Collectors.joining(""));
	}


}


