package vm_vehicle;

import java.util.List;

public class Vm_vehicleReportDTO
{
	public Vm_vehicleDTO vm_vehicleDTO;
	public double totalMaintenanceCost = 0.0;
	public List<ReportItemDTO> maintenanceDTOListForMaintenance;
	public List<ReportItemDTO> maintenanceDTOListForRepair;

	public double totalTaxTokenAndFitnessCost = 0.0;
	public List<ReportItemDTO> VmTaxTokenItemListForTax;

}