package task_type;

/*
 * @author Md. Erfan Hossain
 * @created 05/05/2021 - 10:24 PM
 * @project parliament
 */

public enum TaskTypeEnum {
    PERMANENT_CARD_FOR_1ST_OR_2ND_CLASS_EMPLOYEE(1),
    PERMANENT_CARD_FOR_3RD_OR_4TH_CLASS_EMPLOYEE(2),
    TEMPORARY_CARD_FOR_EMPLOYEE(3),
    ROUTE_WITHDRAW(4),
    BUSINESS_CARD(5),
    VM_FUEL_REQUEST(204),
    VM_MAINTENANCE_REQUEST(304),
    FUND_MANAGEMENT(704),
    SERGEANT_AT_ARMS_APPROVAL(705),
    AM_OFFICE_ASSIGNMENT(904),
    AM_HOUSE_ASSIGNMENT(1004);

    private final long value;

    TaskTypeEnum(long value) {
        this.value = value;
    }

    public long getValue() {
        return value;
    }
}
