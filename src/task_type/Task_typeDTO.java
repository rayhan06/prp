package task_type;

import sessionmanager.SessionConstants;
import util.*;

public class Task_typeDTO extends CommonDTO {
    public String nameEn = "";
    public String nameBn = "";
    public long insertedBy = 0;
    public long insertionTime = SessionConstants.MIN_DATE;
    public long modifiedBy = 0;



    public String getFormattedName(String language) {
        return HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng ? nameEn : nameBn;
    }

    @Override
    public String toString() {
        return "$Task_typeDTO[" +
                " iD = " + iD +
                " nameEn = " + nameEn +
                " nameBn = " + nameBn +
                " isDeleted = " + isDeleted +
                " insertedBy = " + insertedBy +
                " insertionTime = " + insertionTime +
                " modifiedBy = " + modifiedBy +
                " lastModificationTime = " + lastModificationTime +
                "]";
    }
}