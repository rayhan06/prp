package task_type;

import common.BaseServlet;
import common.CommonDAOService;
import office_units.Office_unitsRepository;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import pb.OptionDTO;
import pb.Utils;
import permission.MenuConstants;
import task_type_approval_path.TaskTypeApprovalPathDAO;
import task_type_assign.Task_type_assignDAO;
import task_type_assign.Task_type_assignDTO;
import task_type_assign.Task_type_assignRepository;
import task_type_level.TaskTypeLevelDAO;
import task_type_level.TaskTypeLevelDTO;
import task_type_level.TaskTypeLevelRepository;
import user.UserDTO;
import util.CommonDTO;
import util.HttpRequestUtils;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@SuppressWarnings("Duplicates")
@WebServlet("/Task_typeServlet")
@MultipartConfig
public class Task_typeServlet extends BaseServlet {
    private static final long serialVersionUID = 1L;

    private static final Logger logger = Logger.getLogger(Task_typeServlet.class);

    private final Task_typeDAO task_typeDAO = Task_typeDAO.getInstance();

    @Override
    public String getTableName() {
        return task_typeDAO.getTableName();
    }

    @Override
    public String getServletName() {
        return "Task_typeServlet";
    }

    @Override
    public CommonDAOService<? extends CommonDTO> getCommonDAOService() {
        return task_typeDAO;
    }

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        boolean isLangEng = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
        Task_typeDTO task_typeDTO;
        long currentTime = System.currentTimeMillis();

        if (addFlag) {
            task_typeDTO = new Task_typeDTO();
            task_typeDTO.insertedBy = userDTO.ID;
            task_typeDTO.insertionTime = currentTime;
        } else {
            task_typeDTO = Task_typeRepository.getInstance().getDTOById(Long.parseLong(request.getParameter("iD")));
        }
        task_typeDTO.modifiedBy = userDTO.ID;
        task_typeDTO.lastModificationTime = currentTime;

        task_typeDTO.nameEn = Jsoup.clean(request.getParameter("nameEn"), Whitelist.simpleText());
        if (task_typeDTO.nameEn == null || task_typeDTO.nameEn.trim().length() == 0) {
            throw new Exception(isLangEng ? "Please enter english name for task type" : "কাজের ধরনের জন্য ইংরেজি নাম লিখুন");
        }

        task_typeDTO.nameBn = Jsoup.clean(request.getParameter("nameBn"), Whitelist.simpleText());
        if (task_typeDTO.nameBn == null || task_typeDTO.nameBn.trim().length() == 0) {
            throw new Exception(isLangEng ? "Please enter bangla name for task type" : "কাজের ধরনের জন্য বাংলা নাম লিখুন");
        }

        if (request.getParameter("levelData") == null) {
            throw new Exception(isLangEng ? "Please enter level information" : "অনুগ্রহ করে স্তরের তথ্য দিন");
        }
        TaskTypeLevelDTO[] taskTypeLevelDTOS = gson.fromJson(request.getParameter("levelData"), TaskTypeLevelDTO[].class);
        if (taskTypeLevelDTOS == null || taskTypeLevelDTOS.length == 0) {
            throw new Exception(isLangEng ? "Please enter level information" : "অনুগ্রহ করে স্তরের তথ্য দিন");
        }
        for (TaskTypeLevelDTO taskTypeLevelDTO : taskTypeLevelDTOS) {
            if (taskTypeLevelDTO.level <= 0) {
                throw new Exception(isLangEng ? "Level value should be positive" : "স্তরের মান অব্যশই ধনাত্নক হবে");
            }
            if (taskTypeLevelDTO.officeUnitIds == null || taskTypeLevelDTO.officeUnitIds.trim().length() == 0) {
                throw new Exception(isLangEng ? "Please select correct office" : "অনুগ্রহ করে সঠিক দপ্তর বাছাই করুন");
            }
            boolean res = Stream.of(taskTypeLevelDTO.officeUnitIds.split(","))
                    .map(String::trim)
                    .map(Long::parseLong)
                    .distinct()
                    .map(id -> Office_unitsRepository.getInstance().getOffice_unitsDTOByID(id))
                    .anyMatch(Objects::isNull);

            if (res) {
                throw new Exception(isLangEng ? "Please select correct office" : "অনুগ্রহ করে সঠিক দপ্তর বাছাই করুন");
            }
        }
        if (addFlag) {
            task_typeDAO.add(task_typeDTO);
        } else {
            task_typeDAO.update(task_typeDTO);
        }
        addTaskTypeLevels(task_typeDTO.iD, taskTypeLevelDTOS);
        return task_typeDTO;
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[]{MenuConstants.TASK_TYPE_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[]{MenuConstants.TASK_TYPE_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[]{MenuConstants.TASK_TYPE_SEARCH};
    }

    @Override
    public int[] getViewMenuConstants() {
        return new int[]{MenuConstants.APPROVAL_PATH_ASSIGN,MenuConstants.TASK_TYPE_SEARCH};
    }

    @Override
    public boolean getViewPagePermission(HttpServletRequest request) {
        if(Utils.checkPermission(MenuConstants.TASK_TYPE_SEARCH)){
            return true;
        }
        if(Utils.checkPermission(MenuConstants.APPROVAL_PATH_ASSIGN)){
            UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;
            Task_type_assignDTO taskTypeAssignDTO = Task_type_assignRepository.getInstance().getByTaskTypeId(Long.parseLong(request.getParameter("ID")));
            if(taskTypeAssignDTO == null){
                return false;
            }
            return Stream.of(taskTypeAssignDTO.roleID.split(","))
                    .map(String::trim)
                    .map(Long::parseLong)
                    .filter(role -> role == userDTO.roleID)
                    .findAny()
                    .orElse(null)!=null;
        }
        return false;
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Task_typeServlet.class;
    }

    void addTaskTypeLevels(long taskTypeId, TaskTypeLevelDTO[] taskTypeLevelDTOS) {
        UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;
        TaskTypeLevelDAO taskTypeLevelDAO = TaskTypeLevelDAO.getInstance();
        List<TaskTypeLevelDTO> oldList = TaskTypeLevelRepository.getInstance().getByTaskTypeId(taskTypeId);
        List<TaskTypeLevelDTO> mergedByLevel = Stream.of(taskTypeLevelDTOS)
                .collect(Collectors.groupingBy(e -> e.level))
                .values()
                .stream()
                .map(this::mergeDTOs)
                .collect(Collectors.toList());
        Map<Boolean, List<TaskTypeLevelDTO>> map = mergedByLevel.stream().collect(Collectors.partitioningBy(e -> e.iD == -1));
        map.get(true)
                .forEach(dto -> {
                    dto.taskTypeId = taskTypeId;
                    dto.insertedBy = dto.modifiedBy = userDTO.ID;
                    dto.insertionDate = dto.lastModificationTime = System.currentTimeMillis();
                    try {
                        taskTypeLevelDAO.add(dto);
                    } catch (Exception e) {
                        logger.error(e);
                        e.printStackTrace();
                    }
                });
        map.get(false).forEach(dto -> {
            dto.insertedBy = dto.modifiedBy = userDTO.ID;
            dto.insertionDate = dto.lastModificationTime = System.currentTimeMillis();
            try {
                taskTypeLevelDAO.update(dto);
            } catch (Exception e) {
                logger.error(e);
                e.printStackTrace();
            }
        });

        Set<Long> newListIds = mergedByLevel.stream()
                .map(dto -> dto.iD)
                .collect(Collectors.toSet());
        oldList.forEach(oldDTO -> {
            if (!newListIds.contains(oldDTO.iD)) {
                try {
                    taskTypeLevelDAO.delete(userDTO.ID, oldDTO.iD);
                } catch (Exception ex) {
                    logger.error(ex);
                    ex.printStackTrace();
                }
            }
        });
    }

    private TaskTypeLevelDTO mergeDTOs(List<TaskTypeLevelDTO> ls) {
        if (ls.size() == 1) {
            return ls.get(0);
        }
        TaskTypeLevelDTO dto = ls.get(0);
        dto.officeUnitIds = ls.stream().map(e -> e.officeUnitIds).collect(Collectors.joining(","));
        return dto;
    }

    public static String buildAllOfficeUnitMultiSelectOption(String language, String selectedIds) {
        List<OptionDTO> optionDTOList = Office_unitsRepository.getInstance()
                .getOffice_unitsList()
                .stream()
                .map(dto -> new OptionDTO(dto.unitNameEng, dto.unitNameBng, String.valueOf(dto.iD)))
                .collect(Collectors.toList());
        return Utils.buildOptionsMultipleSelection(optionDTOList, language, selectedIds);
    }

    @Override
    public void finalize(HttpServletRequest request) {
        if ("delete".equals(request.getParameter("actionType"))) {
            String[] IDsToDelete = request.getParameterValues("ID");
            if (IDsToDelete.length > 0) {
                List<Long> ids = Stream.of(IDsToDelete)
                        .map(Long::parseLong)
                        .collect(Collectors.toList());
                List<Long> taskLevelId = ids.stream()
                        .map(id->TaskTypeLevelRepository.getInstance().getByTaskTypeId(id))
                        .flatMap(Collection::stream)
                        .map(e->e.iD)
                        .collect(Collectors.toList());
                long modifiedBy = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO.ID;
                Utils.runIOTaskInASeparateThread(()->()-> TaskTypeLevelDAO.getInstance().deleteByTaskTypeId(ids,modifiedBy));
                Utils.runIOTaskInASeparateThread(()->()-> Task_type_assignDAO.getInstance().deleteByTaskTypeId(ids,modifiedBy));
                Utils.runIOTaskInASeparateThread(()->()-> TaskTypeApprovalPathDAO.getInstance().deleteByTaskTypeLevelId(taskLevelId,modifiedBy));
            }
        }
    }
}