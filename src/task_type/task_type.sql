CREATE TABLE task_type
(
    ID                   BIGINT(20) UNSIGNED PRIMARY KEY,
    name_en              VARCHAR(255) DEFAULT NULL,
    name_bn              VARCHAR(255) DEFAULT NULL,
    office_unit_id       BIGINT(20)   DEFAULT 0,
    level                INT(11)      DEFAULT 0,
    isDeleted            INT(11)      DEFAULT 0,
    inserted_by          BIGINT(20)   DEFAULT 0,
    insertion_time       BIGINT(20)   DEFAULT -62135791200000,
    modified_by          BIGINT(20)   DEFAULT 0,
    lastModificationTime BIGINT(20)   DEFAULT -62135791200000
) ENGINE = MyISAM
  DEFAULT CHARSET = utf8
  COLLATE = utf8_unicode_ci;