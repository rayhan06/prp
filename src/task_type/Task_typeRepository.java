package task_type;

import org.apache.log4j.Logger;
import pb.OptionDTO;
import pb.Utils;
import repository.Repository;
import repository.RepositoryManager;
import util.LockManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

@SuppressWarnings("unused")
public class Task_typeRepository implements Repository {
    private final Task_typeDAO task_typeDAO;

    private static final Logger logger = Logger.getLogger(Task_typeRepository.class);
    private final Map<Long, Task_typeDTO> mapById;

    private Task_typeRepository() {
        task_typeDAO = Task_typeDAO.getInstance();
        mapById = new ConcurrentHashMap<>();
        RepositoryManager.getInstance().addRepository(this);
    }

    private static class LazyLoader {
        final static Task_typeRepository INSTANCE = new Task_typeRepository();
    }

    public static Task_typeRepository getInstance() {
        return Task_typeRepository.LazyLoader.INSTANCE;
    }

    @Override
    public void reload(boolean reloadAll) {
        logger.debug("Task_typeRepository reload start, reloadAll = " + reloadAll);
        List<Task_typeDTO> dtoList = task_typeDAO.getAllDTOs(reloadAll);
        logger.debug("Data loaded from DB, loaded size : " + dtoList.size());
        if (dtoList.size() > 0) {
            dtoList.stream()
                    .peek(this::removeIfPresent)
                    .filter(dot -> dot.isDeleted == 0)
                    .forEach(dto -> mapById.put(dto.iD, dto));
        }
        logger.debug("Task_typeRepository reload end, reloadAll = " + reloadAll);
    }

    private void removeIfPresent(Task_typeDTO newDTO) {
        if (newDTO == null) {
            return;
        }
        if (mapById.get(newDTO.iD) != null) {
            mapById.remove(newDTO.iD);
        }
    }

    @Override
    public String getTableName() {
        return "task_type";
    }

    public Task_typeDTO getDTOById(long id) {
        if(mapById.get(id) == null){
            synchronized (LockManager.getLock("TT"+id)){
                if(mapById.get(id) == null){
                    Task_typeDTO taskTypeDTO = task_typeDAO.getDTOFromID(id);
                    if(taskTypeDTO!=null){
                        mapById.put(taskTypeDTO.iD,taskTypeDTO);
                    }
                }
            }
        }
        return mapById.get(id);
    }

    public Map<Long,Task_typeDTO> getByIdList(List<Long> ids){
       return ids.stream()
               .map(this::getDTOById)
               .filter(Objects::nonNull)
               .collect(Collectors.toMap(e->e.iD,e->e,(e1,e2)->e1));
    }

    public List<Task_typeDTO> getDTOSByIdList(List<Long> ids){
        return ids.stream()
                .map(this::getDTOById)
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    public String getNameById(String language, long id){
        Task_typeDTO dto = getDTOById(id);
        if(dto == null) return "";
        return dto.getFormattedName(language);
    }

    public List<Task_typeDTO> getAllDTOList() {
        return new ArrayList<>(mapById.values());
    }

    public String buildOptionOfAllTaskType(String language, long selectedId) {
        List<OptionDTO> optionDTOList = getAllDTOList().stream()
                .map(dto -> makeOptionDTOFrom(dto, language))
                .collect(Collectors.toList());
        return Utils.buildOptions(optionDTOList, language, String.valueOf(selectedId));
    }

    private OptionDTO makeOptionDTOFrom(Task_typeDTO dto, String language) {
        return new OptionDTO(
                dto.nameEn,
                dto.nameBn,
                String.valueOf(dto.iD)
        );
    }
}


