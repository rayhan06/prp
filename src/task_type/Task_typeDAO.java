package task_type;

import common.CommonDAOService;
import org.apache.log4j.Logger;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

@SuppressWarnings({"unused","Duplicates"})
public class Task_typeDAO implements CommonDAOService<Task_typeDTO> {
    private static final Logger logger = Logger.getLogger(Task_typeDAO.class);

    private static final String addQuery = "INSERT INTO {tableName} (name_en,name_bn,"
            .concat("modified_by,lastModificationTime,inserted_by,insertion_time,isDeleted,ID) ")
            .concat("VALUES(?,?,?,?,?,?,?,?)");

    private static final String updateQuery = "UPDATE {tableName} SET name_en=?,name_bn=?,"
            .concat("modified_by=?,lastModificationTime=? WHERE ID=?");

    private final Map<String,String > searchMap = new HashMap<>();

    private static Task_typeDAO INSTANCE = null;

    private Task_typeDAO() {
        searchMap.put("name_en"," and (name_en like ?)");
        searchMap.put("name_bn"," and (name_bn like ?)");
    }

    public static Task_typeDAO getInstance(){
        if(INSTANCE == null){
            synchronized (Task_typeDAO.class){
                if(INSTANCE == null){
                    INSTANCE = new Task_typeDAO();
                }
            }
        }
        return INSTANCE;
    }

    @Override
    public void set(PreparedStatement ps, Task_typeDTO task_typeDTO, boolean isInsert) throws SQLException {
        int index = 0;

        ps.setString(++index, task_typeDTO.nameEn);
        ps.setString(++index, task_typeDTO.nameBn);
        ps.setLong(++index, task_typeDTO.modifiedBy);
        ps.setLong(++index, task_typeDTO.lastModificationTime);

        if (isInsert) {
            ps.setLong(++index, task_typeDTO.insertedBy);
            ps.setLong(++index, task_typeDTO.insertionTime);
            ps.setInt(++index, 0 /* isDeleted */);
        }

        ps.setLong(++index, task_typeDTO.iD);
    }

    @Override
    public Task_typeDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Task_typeDTO task_typeDTO = new Task_typeDTO();

            task_typeDTO.iD = rs.getLong("ID");
            task_typeDTO.nameEn = rs.getString("name_en");
            task_typeDTO.nameBn = rs.getString("name_bn");
            task_typeDTO.isDeleted = rs.getInt("isDeleted");
            task_typeDTO.insertedBy = rs.getLong("inserted_by");
            task_typeDTO.insertionTime = rs.getLong("insertion_time");
            task_typeDTO.modifiedBy = rs.getLong("modified_by");
            task_typeDTO.lastModificationTime = rs.getLong("lastModificationTime");

            return task_typeDTO;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    @Override
    public String getTableName() {
        return "task_type";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    @Override
    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Task_typeDTO) commonDTO,addQuery,true);
    }

    @Override
    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Task_typeDTO) commonDTO,updateQuery,false);
    }
}