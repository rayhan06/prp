/*
package office_types;

import java.io.IOException;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import org.apache.log4j.Logger;

import login.LoginDTO;
import permission.MenuConstants;
import role.PermissionRepository;


import sessionmanager.SessionConstants;

import user.UserDTO;
import user.UserRepository;

import util.RecordNavigationManager;

import javax.servlet.http.*;
import java.util.UUID;

*/
/**
 * Servlet implementation class Office_typesServlet
 *//*

@WebServlet("/Office_typesServlet")
@MultipartConfig
public class Office_typesServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Office_typesServlet.class);

    */
/**
     * @see HttpServlet#HttpServlet()
     *//*

    public Office_typesServlet() {
        super();
    }

    */
/**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     *//*

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("In doget request = " + request);
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getInstance().getUserDtoByUserId(loginDTO.userID);
        try {
            String actionType = request.getParameter("actionType");
            if (actionType.equals("getAddPage")) {
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.OFFICE_TYPES_ADD)) {
                    getAddPage(request, response);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else if (actionType.equals("getEditPage")) {
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.OFFICE_TYPES_UPDATE)) {
                    getOffice_types(request, response);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else if (actionType.equals("getURL")) {
                String URL = request.getParameter("URL");
                System.out.println("URL = " + URL);
                response.sendRedirect(URL);
            } else if (actionType.equals("search")) {
                System.out.println("search requested");
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.OFFICE_TYPES_SEARCH)) {
                    searchOffice_types(request, response);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.debug(ex);
        }
    }

    private void getAddPage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("ID", -1L);
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("office_types/office_typesEdit.jsp");
        requestDispatcher.forward(request, response);
    }

    private String getFileName(final Part part) {
        final String partHeader = part.getHeader("content-disposition");
        System.out.println("Part Header = {0}" + partHeader);
        for (String content : part.getHeader("content-disposition").split(";")) {
            if (content.trim().startsWith("filename")) {
                return content.substring(
                        content.indexOf('=') + 1).trim().replace("\"", "");
            }
        }
        return null;
    }


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getInstance().getUserDtoByUserId(loginDTO.userID);
        System.out.println("doPost");

        try {
            String actionType = request.getParameter("actionType");
            System.out.println("actionType = " + actionType);
            if (actionType.equals("add")) {

                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.OFFICE_TYPES_ADD)) {
                    System.out.println("going to  addOffice_types ");
                    addOffice_types(request, response, true);
                } else {
                    System.out.println("Not going to  addOffice_types ");
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }

            } else if (actionType.equals("edit")) {

                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.OFFICE_TYPES_UPDATE)) {
                    addOffice_types(request, response, false);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else if (actionType.equals("delete")) {
                deleteOffice_types(request, response);
            } else if (actionType.equals("search")) {
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.OFFICE_TYPES_SEARCH)) {
                    searchOffice_types(request, response);
                } else {
                    request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
                }
            } else if (actionType.equals("getGeo")) {
                System.out.println("going to geoloc ");
                request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            logger.debug(ex);
        }
    }

    private void addOffice_types(HttpServletRequest request, HttpServletResponse response, Boolean addFlag) throws IOException {
        // TODO Auto-generated method stub
        try {
            request.setAttribute("failureMessage", "");
            System.out.println("%%%% addOffice_types");
            String path = getServletContext().getRealPath("/img2/");
            Office_typesDAO office_typesDAO = new Office_typesDAO();
            Office_typesDTO office_typesDTO;
            String FileNamePrefix;
            if (addFlag == true) {
                office_typesDTO = new Office_typesDTO();
                FileNamePrefix = UUID.randomUUID().toString().substring(0, 10);
            } else {
                office_typesDTO = office_typesDAO.getOffice_typesDTOByID(Long.parseLong(request.getParameter("identity")));
                FileNamePrefix = request.getParameter("identity");
            }

            String Value = "";
            Value = request.getParameter("nameBn");
            System.out.println("nameBn = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                office_typesDTO.nameBn = (Value);
            } else {
                System.out.println("FieldName has a null value, not updating" + " = " + Value);
            }
            Value = request.getParameter("nameEn");
            System.out.println("nameEn = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                office_typesDTO.nameEn = (Value);
            } else {
                System.out.println("FieldName has a null value, not updating" + " = " + Value);
            }
            Value = request.getParameter("isDeleted");
            System.out.println("isDeleted = " + Value);
            if (Value != null && !Value.equalsIgnoreCase("")) {
                office_typesDTO.isDeleted = Boolean.parseBoolean(Value);
            } else {
                System.out.println("FieldName has a null value, not updating" + " = " + Value);
            }

            System.out.println("Done adding  addOffice_types dto = " + office_typesDTO);

            if (addFlag == true) {
                office_typesDAO.addOffice_types(office_typesDTO);
            } else {
                office_typesDAO.updateOffice_types(office_typesDTO);

            }


            String inPlaceSubmit = (String) request.getParameter("inplacesubmit");

            if (inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase("")) {
                getOffice_types(request, response);
            } else {
                response.sendRedirect("Office_typesServlet?actionType=search&success=true");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void deleteOffice_types(HttpServletRequest request, HttpServletResponse response) throws IOException {
        try {
            String[] IDsToDelete = request.getParameterValues("ID");
            for (int i = 0; i < IDsToDelete.length; i++) {
                long id = Long.parseLong(IDsToDelete[i]);
                System.out.println("###DELETING " + IDsToDelete[i]);
                new Office_typesDAO().deleteOffice_typesByID(id);
            }
        } catch (Exception ex) {
            logger.debug(ex);
        }
        response.sendRedirect("Office_typesServlet?actionType=search");
    }

    private void getOffice_types(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("in getOffice_types");
        Office_typesDTO office_typesDTO = null;
        try {
            office_typesDTO = new Office_typesDAO().getOffice_typesDTOByID(Long.parseLong(request.getParameter("ID")));
            request.setAttribute("ID", office_typesDTO.iD);
            request.setAttribute("office_typesDTO", office_typesDTO);

            String URL = "";

            String inPlaceEdit = (String) request.getParameter("inplaceedit");
            String inPlaceSubmit = (String) request.getParameter("inplacesubmit");
            String getBodyOnly = (String) request.getParameter("getBodyOnly");

            if (inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase("")) {
                URL = "office_types/office_typesInPlaceEdit.jsp";
                request.setAttribute("inplaceedit", "");
            } else if (inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase("")) {
                URL = "office_types/office_typesSearchRow.jsp";
                request.setAttribute("inplacesubmit", "");
            } else {
                if (getBodyOnly != null && !getBodyOnly.equalsIgnoreCase("")) {
                    URL = "office_types/office_typesEditBody.jsp?actionType=edit";
                } else {
                    URL = "office_types/office_typesEdit.jsp?actionType=edit";
                }
            }

            RequestDispatcher rd = request.getRequestDispatcher(URL);
            rd.forward(request, response);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void searchOffice_types(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("in  searchOffice_types 1");
        Office_typesDAO office_typesDAO = new Office_typesDAO();
        LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
        String ajax = (String) request.getParameter("ajax");
        boolean hasAjax = false;
        if (ajax != null && !ajax.equalsIgnoreCase("")) {
            hasAjax = true;
        }
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);

        RecordNavigationManager rnManager = new RecordNavigationManager(SessionConstants.NAV_OFFICE_TYPES, request, office_typesDAO, SessionConstants.VIEW_OFFICE_TYPES, SessionConstants.SEARCH_OFFICE_TYPES);
        try {
            System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        } catch (Exception e) {
            System.out.println("failed to dojob" + e);
        }

        RequestDispatcher rd;
        if (hasAjax == false) {
            System.out.println("Going to office_types/office_typesSearch.jsp");
            rd = request.getRequestDispatcher("office_types/office_typesSearch.jsp");
        } else {
            System.out.println("Going to office_types/office_typesSearchForm.jsp");
            rd = request.getRequestDispatcher("office_types/office_typesSearchForm.jsp");
        }
        rd.forward(request, response);
    }

}

*/
