DELETE FROM language_text WHERE menuID = 736001;
DELETE FROM language_text WHERE menuID = 736002;
DELETE FROM language_text WHERE menuID = 736003;
DELETE FROM language_text WHERE languageConstantPrefix = 'OFFICE_TYPES_ADD';
DELETE FROM language_text WHERE languageConstantPrefix = 'OFFICE_TYPES_EDIT';
DELETE FROM language_text WHERE languageConstantPrefix = 'OFFICE_TYPES_SEARCH';
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('800744','736001','ID','????','OFFICE_TYPES_ADD','ID');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('800745','736001','Bangla Name','????? ???','OFFICE_TYPES_ADD','NAMEBN');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('800746','736001','English Name','?????? ???','OFFICE_TYPES_ADD','NAMEEN');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('800747','736001','IsDeleted','????? ??? ?????','OFFICE_TYPES_ADD','ISDELETED');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('800748','736002','ID','????','OFFICE_TYPES_EDIT','ID');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('800749','736002','Bangla Name','????? ???','OFFICE_TYPES_EDIT','NAMEBN');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('800750','736002','English Name','?????? ???','OFFICE_TYPES_EDIT','NAMEEN');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('800751','736002','IsDeleted','????? ??? ?????','OFFICE_TYPES_EDIT','ISDELETED');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('800752','736003','ID','????','OFFICE_TYPES_SEARCH','ID');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('800753','736003','Bangla Name','????? ???','OFFICE_TYPES_SEARCH','NAMEBN');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('800754','736003','English Name','?????? ???','OFFICE_TYPES_SEARCH','NAMEEN');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('800755','736003','IsDeleted','????? ??? ?????','OFFICE_TYPES_SEARCH','ISDELETED');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('800756','736001','OFFICE TYPES ADD','????? ??? ??? ????','OFFICE_TYPES_ADD','OFFICE_TYPES_ADD_FORMNAME');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('800757','736001','ADD','??? ????','OFFICE_TYPES_ADD','OFFICE_TYPES_ADD_BUTTON');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('800758','736001','SUBMIT','?????? ????','OFFICE_TYPES_ADD','OFFICE_TYPES_SUBMIT_BUTTON');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('800759','736001','CANCEL','????? ????','OFFICE_TYPES_ADD','OFFICE_TYPES_CANCEL_BUTTON');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('800760','736002','OFFICE TYPES EDIT','????? ??? ???????? ????','OFFICE_TYPES_EDIT','OFFICE_TYPES_EDIT_FORMNAME');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('800761','736002','SUBMIT','?????? ????','OFFICE_TYPES_EDIT','OFFICE_TYPES_SUBMIT_BUTTON');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('800762','736002','CANCEL','????? ????','OFFICE_TYPES_EDIT','OFFICE_TYPES_CANCEL_BUTTON');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('800763','736002','English','Bangla','OFFICE_TYPES_EDIT','LANGUAGE');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('800764','736003','OFFICE TYPES SEARCH','????? ??? ??????','OFFICE_TYPES_SEARCH','OFFICE_TYPES_SEARCH_FORMNAME');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('800765','736003','SEARCH','??????','OFFICE_TYPES_SEARCH','OFFICE_TYPES_SEARCH_BUTTON');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('800766','736003','DELETE','????? ????','OFFICE_TYPES_SEARCH','OFFICE_TYPES_DELETE_BUTTON');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('800767','736003','EDIT','???????? ????','OFFICE_TYPES_SEARCH','OFFICE_TYPES_EDIT_BUTTON');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('800768','736003','CANCEL','????? ????','OFFICE_TYPES_SEARCH','OFFICE_TYPES_CANCEL_BUTTON');
INSERT INTO `language_text` (`ID`, `menuID`, `languageTextEnglish`, `languageTextBangla`, `languageConstantPrefix`, `languageConstant`) VALUES ('800769','736003','OFFICE TYPES','????? ???','OFFICE_TYPES_SEARCH','ANYFIELD');
