package office_types;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class Office_typesRepository implements Repository {
    Office_typesDAO office_typesDAO = new Office_typesDAO();

    static Logger logger = Logger.getLogger(Office_typesRepository.class);
    Map<Long, Office_typesDTO> mapOfOffice_typesDTOToiD;
    Map<String, Set<Office_typesDTO>> mapOfOffice_typesDTOTonameBn;
    Map<String, Set<Office_typesDTO>> mapOfOffice_typesDTOTonameEn;


    static Office_typesRepository instance = null;

    private Office_typesRepository() {
        mapOfOffice_typesDTOToiD = new ConcurrentHashMap<>();
        mapOfOffice_typesDTOTonameBn = new ConcurrentHashMap<>();
        mapOfOffice_typesDTOTonameEn = new ConcurrentHashMap<>();

        RepositoryManager.getInstance().addRepository(this);
    }

    public synchronized static Office_typesRepository getInstance() {
        if (instance == null) {
            instance = new Office_typesRepository();
        }
        return instance;
    }

    public void reload(boolean reloadAll) {
        try {
            List<Office_typesDTO> office_typesDTOs = office_typesDAO.getAllOffice_types(reloadAll);
            for (Office_typesDTO office_typesDTO : office_typesDTOs) {
                Office_typesDTO oldOffice_typesDTO = getOffice_typesDTOByID(office_typesDTO.iD);
                if (oldOffice_typesDTO != null) {
                    mapOfOffice_typesDTOToiD.remove(oldOffice_typesDTO.iD);

                    if (mapOfOffice_typesDTOTonameBn.containsKey(oldOffice_typesDTO.nameBn)) {
                        mapOfOffice_typesDTOTonameBn.get(oldOffice_typesDTO.nameBn).remove(oldOffice_typesDTO);
                    }
                    if (mapOfOffice_typesDTOTonameBn.get(oldOffice_typesDTO.nameBn).isEmpty()) {
                        mapOfOffice_typesDTOTonameBn.remove(oldOffice_typesDTO.nameBn);
                    }

                    if (mapOfOffice_typesDTOTonameEn.containsKey(oldOffice_typesDTO.nameEn)) {
                        mapOfOffice_typesDTOTonameEn.get(oldOffice_typesDTO.nameEn).remove(oldOffice_typesDTO);
                    }
                    if (mapOfOffice_typesDTOTonameEn.get(oldOffice_typesDTO.nameEn).isEmpty()) {
                        mapOfOffice_typesDTOTonameEn.remove(oldOffice_typesDTO.nameEn);
                    }


                }
                if (office_typesDTO.isDeleted == false) {

                    mapOfOffice_typesDTOToiD.put(office_typesDTO.iD, office_typesDTO);

                    if (!mapOfOffice_typesDTOTonameBn.containsKey(office_typesDTO.nameBn)) {
                        mapOfOffice_typesDTOTonameBn.put(office_typesDTO.nameBn, new HashSet<>());
                    }
                    mapOfOffice_typesDTOTonameBn.get(office_typesDTO.nameBn).add(office_typesDTO);

                    if (!mapOfOffice_typesDTOTonameEn.containsKey(office_typesDTO.nameEn)) {
                        mapOfOffice_typesDTOTonameEn.put(office_typesDTO.nameEn, new HashSet<>());
                    }
                    mapOfOffice_typesDTOTonameEn.get(office_typesDTO.nameEn).add(office_typesDTO);

                }
            }

        } catch (Exception e) {
            logger.debug("FATAL", e);
        }
    }

    public List<Office_typesDTO> getOffice_typesList() {
        List<Office_typesDTO> office_typess = new ArrayList<Office_typesDTO>(this.mapOfOffice_typesDTOToiD.values());
        return office_typess;
    }


    public Office_typesDTO getOffice_typesDTOByID(long ID) {
        return mapOfOffice_typesDTOToiD.get(ID);
    }


    public List<Office_typesDTO> getOffice_typesDTOByname_bn(String name_bn) {
        return new ArrayList<>(mapOfOffice_typesDTOTonameBn.getOrDefault(name_bn, new HashSet<>()));
    }


    public List<Office_typesDTO> getOffice_typesDTOByname_en(String name_en) {
        return new ArrayList<>(mapOfOffice_typesDTOTonameEn.getOrDefault(name_en, new HashSet<>()));
    }


    @Override
    public String getTableName() {
        String tableName = "";
        try {
            tableName = "office_types";
        } catch (Exception ex) {
            logger.debug("FATAL", ex);
        }
        return tableName;
    }
}


