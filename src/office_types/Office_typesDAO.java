package office_types;

import dbm.DBMR;
import dbm.DBMW;
import login.LoginDTO;
import org.apache.log4j.Logger;
import repository.RepositoryManager;
import util.NavigationService;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;


public class Office_typesDAO implements NavigationService {

    Logger logger = Logger.getLogger(getClass());


    private void printSql(String sql) {
        System.out.println("sql: " + sql);
    }


    private void recordUpdateTime(Connection connection, long lastModificationTime) {
        recordUpdateTime(connection,"office_types",lastModificationTime);
    }


    public long addOffice_types(Office_typesDTO office_typesDTO) throws Exception {

        Connection connection = null;
        PreparedStatement ps = null;

        long lastModificationTime = System.currentTimeMillis();

        try {
            connection = DBMW.getInstance().getConnection();

            if (connection == null) {
                System.out.println("nullconn");
            }

            office_typesDTO.iD = DBMW.getInstance().getNextSequenceId("Office_types");

            String sql = "INSERT INTO office_types";

            sql += " (";
            sql += "ID";
            sql += ", ";
            sql += "name_bn";
            sql += ", ";
            sql += "name_en";
            sql += ", ";
            sql += "isDeleted";
            sql += ", lastModificationTime)";


            sql += " VALUES(";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ";
            sql += "?";
            sql += ", ?)";

            printSql(sql);

            ps = connection.prepareStatement(sql);


            int index = 1;

            ps.setObject(index++, office_typesDTO.iD);
            ps.setObject(index++, office_typesDTO.nameBn);
            ps.setObject(index++, office_typesDTO.nameEn);
            ps.setObject(index++, office_typesDTO.isDeleted);
            ps.setObject(index++, lastModificationTime);

            System.out.println(ps);
            ps.execute();


            recordUpdateTime(connection,lastModificationTime);

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (connection != null) {
                    DBMW.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
        return office_typesDTO.iD;
    }


    //need another getter for repository
    public Office_typesDTO getOffice_typesDTOByID(long ID) throws Exception {
        Connection connection = null;
        ResultSet rs = null;
        Statement stmt = null;
        Office_typesDTO office_typesDTO = null;
        try {

            String sql = "SELECT * ";

            sql += " FROM office_types";

            sql += " WHERE ID=" + ID;

            printSql(sql);

            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();


            rs = stmt.executeQuery(sql);

            if (rs.next()) {
                office_typesDTO = new Office_typesDTO();

                office_typesDTO.iD = rs.getLong("ID");
                office_typesDTO.nameBn = rs.getString("name_bn");
                office_typesDTO.nameEn = rs.getString("name_en");
                office_typesDTO.isDeleted = rs.getBoolean("isDeleted");

            }


        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }

            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
        return office_typesDTO;
    }

    public long updateOffice_types(Office_typesDTO office_typesDTO) throws Exception {

        Connection connection = null;
        PreparedStatement ps = null;

        long lastModificationTime = System.currentTimeMillis();
        try {
            connection = DBMW.getInstance().getConnection();

            String sql = "UPDATE office_types";

            sql += " SET ";
            sql += "name_bn=?";
            sql += ", ";
            sql += "name_en=?";
            sql += ", ";
            sql += "isDeleted=?";
            sql += ", lastModificationTime = " + lastModificationTime + "";
            sql += " WHERE ID = " + office_typesDTO.iD;

            printSql(sql);

            ps = connection.prepareStatement(sql);


            int index = 1;

            ps.setObject(index++, office_typesDTO.nameBn);
            ps.setObject(index++, office_typesDTO.nameEn);
            ps.setObject(index++, office_typesDTO.isDeleted);
            System.out.println(ps);
            ps.executeUpdate();


            recordUpdateTime(connection, lastModificationTime);


        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (connection != null) {
                    DBMW.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
        return office_typesDTO.iD;
    }

    public void deleteOffice_typesByID(long ID) throws Exception {
        Connection connection = null;
        Statement stmt = null;
        PreparedStatement ps = null;

        long lastModificationTime = System.currentTimeMillis();
        try {
            String sql = "UPDATE office_types";

            sql += " SET isDeleted=1,lastModificationTime=" + lastModificationTime + " WHERE ID = " + ID;

            printSql(sql);

            connection = DBMW.getInstance().getConnection();
            stmt = connection.createStatement();
            stmt.execute(sql);


            recordUpdateTime(connection, lastModificationTime);


        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }

            try {
                if (connection != null) {
                    DBMW.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
    }


    public List<Office_typesDTO> getDTOs(Collection recordIDs, LoginDTO loginDTO) {
        Connection connection = null;
        ResultSet rs = null;
        Statement stmt = null;
        Office_typesDTO office_typesDTO = null;
        List<Office_typesDTO> office_typesDTOList = new ArrayList<>();
        if (recordIDs.isEmpty()) {
            return office_typesDTOList;
        }
        try {

            String sql = "SELECT * ";

            sql += " FROM office_types";

            sql += " WHERE ID IN ( ";

            for (int i = 0; i < recordIDs.size(); i++) {
                if (i != 0) {
                    sql += ",";
                }
                sql += ((ArrayList) recordIDs).get(i);
            }
            sql += ")  order by ID desc";

            printSql(sql);

            logger.debug("sql " + sql);
            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();


            rs = stmt.executeQuery(sql);

            while (rs.next()) {
                office_typesDTO = new Office_typesDTO();
                office_typesDTO.iD = rs.getLong("ID");
                office_typesDTO.nameBn = rs.getString("name_bn");
                office_typesDTO.nameEn = rs.getString("name_en");
                office_typesDTO.isDeleted = rs.getBoolean("isDeleted");
                System.out.println("got this DTO: " + office_typesDTO);

                office_typesDTOList.add(office_typesDTO);

            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }

            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
        return office_typesDTOList;

    }


    public Collection getIDs(LoginDTO loginDTO) {
        Collection data = new ArrayList();
        Connection connection = null;
        Statement stmt = null;
        ResultSet resultSet = null;

        String sql = "SELECT ID FROM office_types";

        sql += " WHERE isDeleted = 0  order by ID desc ";

        printSql(sql);

        try {
            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();

            for (resultSet = stmt.executeQuery(sql); resultSet.next(); data.add(resultSet.getString("ID"))) ;

            resultSet.close();
        } catch (Exception e) {
            logger.fatal("DAO " + e, e);
        } finally {
            try {
                if (resultSet != null && !resultSet.isClosed()) {
                    resultSet.close();
                }
            } catch (Exception ex) {

            }
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception e) {
                logger.fatal("DAO finally :" + e);
            }
        }
        return data;
    }

    //add repository
    public List<Office_typesDTO> getAllOffice_types(boolean isFirstReload) {
        List<Office_typesDTO> office_typesDTOList = new ArrayList<>();

        String sql = "SELECT * FROM office_types";
        sql += " WHERE ";


        if (isFirstReload) {
            sql += " isDeleted =  0";
        }
        if (!isFirstReload) {
            sql += " lastModificationTime >= " + RepositoryManager.lastModifyTime;
        }
        printSql(sql);

        Connection connection = null;
        Statement stmt = null;
        ResultSet rs = null;


        try {
            connection = DBMR.getInstance().getConnection();
            stmt = connection.createStatement();
            rs = stmt.executeQuery(sql);


            while (rs.next()) {
                Office_typesDTO office_typesDTO = new Office_typesDTO();
                office_typesDTO.iD = rs.getLong("ID");
                office_typesDTO.nameBn = rs.getString("name_bn");
                office_typesDTO.nameEn = rs.getString("name_en");
                office_typesDTO.isDeleted = rs.getBoolean("isDeleted");
                int i = 0;
                long primaryKey = office_typesDTO.iD;
                while (i < office_typesDTOList.size() && office_typesDTOList.get(i).iD < primaryKey) {
                    i++;
                }
                office_typesDTOList.add(i, office_typesDTO);
                //office_typesDTOList.add(office_typesDTO);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }

        return office_typesDTOList;
    }


    public Collection getIDsWithSearchCriteria(Hashtable p_searchCriteria, LoginDTO loginDTO) throws Exception {
        System.out.println("table: " + p_searchCriteria);
        List<Long> idList = new ArrayList<>();
        Connection connection = null;
        PreparedStatement ps = null;

        try {

            String sql = "SELECT distinct office_types.ID as ID FROM office_types ";


            Enumeration names = p_searchCriteria.keys();
            String str, value;

            String AnyfieldSql = "(";

            if (p_searchCriteria.get("AnyField") != null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase("")) {
                int i = 0;
                Iterator it = Office_typesMAPS.GetInstance().java_anyfield_search_map.entrySet().iterator();
                while (it.hasNext()) {
                    if (i > 0) {
                        AnyfieldSql += " OR  ";
                    }
                    Map.Entry pair = (Map.Entry) it.next();
                    AnyfieldSql += pair.getKey() + " like '%" + p_searchCriteria.get("AnyField").toString() + "%'";
                    i++;
                }
            }
            AnyfieldSql += ")";
            System.out.println("AnyfieldSql = " + AnyfieldSql);

            String AllFieldSql = "(";
            int i = 0;
            while (names.hasMoreElements()) {
                str = (String) names.nextElement();
                value = (String) p_searchCriteria.get(str);
                System.out.println(str + ": " + value);
                if (Office_typesMAPS.GetInstance().java_allfield_type_map.get(str.toLowerCase()) != null && !Office_typesMAPS.GetInstance().java_allfield_type_map.get(str.toLowerCase()).equalsIgnoreCase("")
                        && !str.equalsIgnoreCase("AnyField")
                        && value != null && !value.equalsIgnoreCase("")) {
                    if (p_searchCriteria.get(str).equals("any")) {
                        continue;
                    }

                    if (i > 0) {
                        AllFieldSql += " AND  ";
                    }
                    if (Office_typesMAPS.GetInstance().java_allfield_type_map.get(str.toLowerCase()).equals("String")) {
                        AllFieldSql += "office_types." + str.toLowerCase() + " like '%" + p_searchCriteria.get(str) + "%'";
                    } else {
                        AllFieldSql += "office_types." + str.toLowerCase() + " = '" + p_searchCriteria.get(str) + "'";
                    }
                    i++;
                }
            }

            AllFieldSql += ")";

            System.out.println("AllFieldSql = " + AllFieldSql);


            sql += " WHERE ";
            sql += " office_types.isDeleted = false";


            if (!AnyfieldSql.equals("()")) {
                sql += " AND " + AnyfieldSql;

            }
            if (!AllFieldSql.equals("()")) {
                sql += " AND " + AllFieldSql;
            }

            sql += " order by office_types.ID desc ";

            printSql(sql);

            connection = DBMR.getInstance().getConnection();
            ps = connection.prepareStatement(sql);

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                idList.add(rs.getLong("ID"));
            }


        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (connection != null) {
                    DBMR.getInstance().freeConnection(connection);
                }
            } catch (Exception ex2) {
            }
        }
        return idList;
    }


}
	