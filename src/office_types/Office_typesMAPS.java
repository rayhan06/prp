package office_types;
import java.util.*; 


public class Office_typesMAPS 
{

	public HashMap<String, String> java_allfield_type_map = new HashMap<String, String>();
	public HashMap<String, String> java_anyfield_search_map = new HashMap<String, String>();
	public HashMap<String, String> java_DTO_map = new HashMap<String, String>();
	public HashMap<String, String> java_SQL_map = new HashMap<String, String>();
	public HashMap<String, String> java_Text_map = new HashMap<String, String>();
	
	private static Office_typesMAPS self = null;
	
	private Office_typesMAPS()
	{
		
		java_allfield_type_map.put("name_bn".toLowerCase(), "String");
		java_allfield_type_map.put("name_en".toLowerCase(), "String");

		java_anyfield_search_map.put("office_types.name_bn".toLowerCase(), "String");
		java_anyfield_search_map.put("office_types.name_en".toLowerCase(), "String");

		java_DTO_map.put("id".toLowerCase(), "id".toLowerCase());
		java_DTO_map.put("nameBn".toLowerCase(), "nameBn".toLowerCase());
		java_DTO_map.put("nameEn".toLowerCase(), "nameEn".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());

		java_SQL_map.put("name_bn".toLowerCase(), "nameBn".toLowerCase());
		java_SQL_map.put("name_en".toLowerCase(), "nameEn".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "id".toLowerCase());
		java_Text_map.put("Bangla Name".toLowerCase(), "nameBn".toLowerCase());
		java_Text_map.put("English Name".toLowerCase(), "nameEn".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
			
	}
	
	public static Office_typesMAPS GetInstance()
	{
		if(self == null)
		{
			self = new Office_typesMAPS ();
		}
		return self;
	}
	

}