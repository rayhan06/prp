package office_types;
import java.util.*; 


public class Office_typesDTO {

	public long iD = 0;
    public String nameBn = "";
    public String nameEn = "";
	public boolean isDeleted = false;
	
	
    @Override
	public String toString() {
            return "$Office_typesDTO[" +
            " id = " + iD +
            " nameBn = " + nameBn +
            " nameEn = " + nameEn +
            " isDeleted = " + isDeleted +
            "]";
    }

}