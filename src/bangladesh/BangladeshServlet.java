//package bangladesh;
//
//import java.io.IOException;
//import java.io.*;
//import java.text.SimpleDateFormat;
//
//
//import javax.servlet.RequestDispatcher;
//import javax.servlet.ServletException;
//import javax.servlet.annotation.MultipartConfig;
//import javax.servlet.annotation.WebServlet;
//
//
//import org.apache.log4j.Logger;
//
//import login.LoginDTO;
//import permission.MenuConstants;
//import role.PermissionRepository;
//
//import sessionmanager.SessionConstants;
//import user.UserDTO;
//import util.*;
//import javax.servlet.http.*;
//import java.util.*;
//
//
//import geolocation.GeoLocationDAO2;
//import language.LC;
//import language.LM;
//import common.ApiResponse;
//import common.BaseServlet;
//import com.google.gson.Gson;
//import pb.*;
//import pbReport.*;
//import org.jsoup.Jsoup;
//import org.jsoup.safety.Whitelist;
//
//
//
//
///**
// * Servlet implementation class BangladeshServlet
// */
//@WebServlet("/BangladeshServlet")
//@MultipartConfig
//public class BangladeshServlet extends BaseServlet
//{
//	private static final long serialVersionUID = 1L;
//    public static Logger logger = Logger.getLogger(BangladeshServlet.class);
//
//    @Override
//    public String getTableName() {
//        return BangladeshDAO.getInstance().getTableName();
//    }
//
//    @Override
//    public String getServletName() {
//        return "BangladeshServlet";
//    }
//
//    @Override
//    public BangladeshDAO getCommonDAOService() {
//        return BangladeshDAO.getInstance();
//    }
//
//    @Override
//    public int[] getAddPageMenuConstants() {
//        return new int[] {MenuConstants.BANGLADESH_ADD};
//    }
//
//    @Override
//    public int[] getEditPageMenuConstants() {
//        return new int[] {MenuConstants.BANGLADESH_UPDATE};
//    }
//
//    @Override
//    public int[] getSearchMenuConstants() {
//        return new int[] {MenuConstants.BANGLADESH_SEARCH};
//    }
//
//    @Override
//    public Class<? extends HttpServlet> getClazz() {
//        return BangladeshServlet.class;
//    }
//    private final Gson gson = new Gson();
//
//
//	@Override
//    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception
//	{
//		// TODO Auto-generated method stub
//		try
//		{
//			request.setAttribute("failureMessage", "");
//			System.out.println("%%%% addBangladesh");
//			String language = userDTO.languageID == SessionConstants.ENGLISH? "English":"Bangla";
//			BangladeshDTO bangladeshDTO;
//			SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
//
//			if(addFlag == true)
//			{
//				bangladeshDTO = new BangladeshDTO();
//			}
//			else
//			{
//				bangladeshDTO = BangladeshDAO.getInstance().getDTOFromID(Long.parseLong(request.getParameter("iD")));
//			}
//
//			String Value = "";
//
//			Value = request.getParameter("nameEn");
//
//			if(Value != null)
//			{
//				Value = Jsoup.clean(Value,Whitelist.simpleText());
//			}
//			System.out.println("nameEn = " + Value);
//			if(Value != null)
//			{
//				bangladeshDTO.nameEn = (Value);
//			}
//			else
//			{
//				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//			}
//
//			Value = request.getParameter("nameBn");
//
//			if(Value != null)
//			{
//				Value = Jsoup.clean(Value,Whitelist.simpleText());
//			}
//			System.out.println("nameBn = " + Value);
//			if(Value != null)
//			{
//				bangladeshDTO.nameBn = (Value);
//			}
//			else
//			{
//				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//			}
//
//			Value = request.getParameter("searchColumn");
//
//			if(Value != null)
//			{
//				Value = Jsoup.clean(Value,Whitelist.simpleText());
//			}
//			System.out.println("searchColumn = " + Value);
//			if(Value != null)
//			{
//				bangladeshDTO.searchColumn = (Value);
//			}
//			else
//			{
//				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
//			}
//
//			System.out.println("Done adding  addBangladesh dto = " + bangladeshDTO);
//			long returnedID = -1;
//
//
//			if(addFlag == true)
//			{
//				returnedID = BangladeshDAO.getInstance().add(bangladeshDTO);
//			}
//			else
//			{
//				returnedID = BangladeshDAO.getInstance().update(bangladeshDTO);
//			}
//
//
//			return bangladeshDTO;
//
//		}
//		catch (Exception e)
//		{
//			e.printStackTrace();
//			return null;
//		}
//	}
//}
//
