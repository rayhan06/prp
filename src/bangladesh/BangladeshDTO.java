package bangladesh;
import java.util.*; 
import util.*; 


public class BangladeshDTO extends CommonDTO
{

    public String nameEn = "";
    public String nameBn = "";
	
	
    @Override
	public String toString() {
            return "$BangladeshDTO[" +
            " iD = " + iD +
            " nameEn = " + nameEn +
            " nameBn = " + nameBn +
            " searchColumn = " + searchColumn +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}