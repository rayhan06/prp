package bangladesh;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.sql.SQLException;
import common.CommonDAOService;
import org.apache.log4j.Logger;
import util.*;
import pb.*;

public class BangladeshDAO  implements CommonDAOService<BangladeshDTO>
{
	
	Logger logger = Logger.getLogger(getClass());
	
	private static String addQuery = "";
	private static String updateQuery = "";
	private String[] columnNames = null;

	private BangladeshDAO()
	{
		columnNames = new String[] 
		{
			"ID",
			"name_en",
			"name_bn",
			"search_column",
			"isDeleted",
			"lastModificationTime"
		};
		updateQuery = getUpdateQuery(columnNames);
        addQuery = getAddQuery(columnNames);
		
		searchMap.put("name_en"," and (name_en like ?)");
		searchMap.put("name_bn"," and (name_bn like ?)");
		searchMap.put("AnyField"," and (search_column like ?)");
	}

	private final Map<String,String> searchMap = new HashMap<>();

    private static class LazyLoader
    {
    	static final BangladeshDAO INSTANCE = new BangladeshDAO();
	}

	public static BangladeshDAO getInstance()
	{
    	return LazyLoader.INSTANCE;
	}
	
	public void setSearchColumn(BangladeshDTO bangladeshDTO)
	{
		bangladeshDTO.searchColumn = "";
		bangladeshDTO.searchColumn += bangladeshDTO.nameEn + " ";
		bangladeshDTO.searchColumn += bangladeshDTO.nameBn + " ";
	}
	
	@Override
	public void set(PreparedStatement ps, BangladeshDTO bangladeshDTO, boolean isInsert) throws SQLException
	{
		int index = 0;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(bangladeshDTO);
		if(isInsert)
		{
			ps.setObject(++index,bangladeshDTO.iD);
		}
		ps.setObject(++index,bangladeshDTO.nameEn);
		ps.setObject(++index,bangladeshDTO.nameBn);
		ps.setObject(++index,bangladeshDTO.searchColumn);
		if(isInsert)
		{
			ps.setObject(++index,bangladeshDTO.isDeleted);
		}
		ps.setObject(++index, lastModificationTime);
		if(!isInsert)
		{
			ps.setObject(++index,bangladeshDTO.iD);
		}
	}
	
	@Override
	public BangladeshDTO buildObjectFromResultSet(ResultSet rs)
	{
		try
		{
			BangladeshDTO bangladeshDTO = new BangladeshDTO();
			int i = 0;
			bangladeshDTO.iD = rs.getLong(columnNames[i++]);
			bangladeshDTO.nameEn = rs.getString(columnNames[i++]);
			bangladeshDTO.nameBn = rs.getString(columnNames[i++]);
			bangladeshDTO.searchColumn = rs.getString(columnNames[i++]);
			bangladeshDTO.isDeleted = rs.getInt(columnNames[i++]);
			bangladeshDTO.lastModificationTime = rs.getLong(columnNames[i++]);
			return bangladeshDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
		
	public BangladeshDTO getDTOByID (long id)
	{
		return getDTOFromID(id);
	}

	@Override
	public String getTableName() {
		return "bangladesh";
	}

	@Override
	public Map<String, String> getSearchMap() {
		return searchMap;
	}

	public long add(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((BangladeshDTO) commonDTO,addQuery,true);
	}

	public long update(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((BangladeshDTO) commonDTO,updateQuery,false);
	}
	
	@Override
	public String getLastModifierUser(){
		return "";
    }
				
}
	