package parliament_house_summary_report;


import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import am_minister_hostel_block.AmMinisterHostelSideRepository;
import am_minister_hostel_block.Am_minister_hostel_blockRepository;
import am_minister_hostel_level.Am_minister_hostel_levelRepository;
import am_minister_hostel_unit.Am_minister_hostel_unitRepository;
import am_parliament_building_block.Am_parliament_building_blockRepository;
import am_parliament_building_level.Am_parliament_building_levelRepository;
import language.LC;
import language.LM;

import login.LoginDTO;
import permission.MenuConstants;

import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;

import util.ReportRequestHandler;
import pbReport.*;
import util.UtilCharacter;


@WebServlet("/Parliament_house_summary_report_Servlet")
public class Parliament_house_summary_report_Servlet  extends HttpServlet {
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	String[][] Criteria =
	{
		{"criteria","tr","level_type","=","","long","","","any","levelType", LC.AM_PARLIAMENT_BUILDING_ROOM_ADD_LEVELTYPE + ""},
		{"criteria","tr","block_type","=","AND","long","","","any","blockType", LC.AM_PARLIAMENT_BUILDING_ROOM_ADD_BLOCKTYPE + ""},
		{"criteria","tr","room_no","=","AND","String","","","any","roomNo", LC.AM_PARLIAMENT_BUILDING_ROOM_ADD_ROOMNO + ""},
		{"criteria","tr","status","=","AND","int","","","any","status", LC.PI_REQUISITION_ADD_STATUS + ""},
		{"criteria","tr","isDeleted","=","AND","String","","","0","isDeleted", LC.AM_OFFICE_ASSIGNMENT_EMPLOYEE_WISE_REPORT_WHERE_ISDELETED + ""}
	};

	String[][] Display =
	{
		{"display","tr","level_type","cacheDataByDtoAttribute","","am_parliament_building_level,levelNo"},
		{"display","tr","block_type","catDataByDtoAttribute","","am_parliament_building_block,parliament_block,parliamentBlockCat"},
		{"display","tr","room_no","","",""},
		{"display","tr","status","commonApprovalStatus",""}
	};

	String GroupBy = "";
	String OrderBY = "";

	ReportRequestHandler reportRequestHandler;

	public Parliament_house_summary_report_Servlet(){

	}

	private final ReportService reportService = new ReportService();

	private String sql;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{

		Am_parliament_building_blockRepository.getInstance();
		Am_parliament_building_levelRepository.getInstance();

		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String language = "english";
		if(userDTO!= null && userDTO.languageID == SessionConstants.BANGLA)
		{
			language = "bangla";
		}



		String actionType = request.getParameter("actionType");

		System.out.println("In ssservlet doget, actiontype = " + actionType);

		sql = "am_parliament_building_room tr ";


		List<String[]> l = new ArrayList<String[]>(Arrays.asList(Display));
		Display = l.toArray(new String[][]{});

		Display[0][4] = LM.getText(LC.AM_PARLIAMENT_BUILDING_ROOM_ADD_LEVELTYPE, loginDTO);
		Display[1][4] = LM.getText(LC.AM_PARLIAMENT_BUILDING_ROOM_ADD_BLOCKTYPE, loginDTO);
		Display[2][4] = LM.getText(LC.AM_PARLIAMENT_BUILDING_ROOM_ADD_ROOMNO, loginDTO);
		Display[3][4] = UtilCharacter.getDataByLanguage(language, "অবস্থা", "Status");

		String reportName = UtilCharacter.getDataByLanguage(language, "সংসদ ভবন বরাদ্দের রিপোর্ট", "Parliament House Allocation Report");

		reportRequestHandler = new ReportRequestHandler(null,
				Criteria, Display, GroupBy, OrderBY, sql,
				reportService);


		reportRequestHandler.handleReportGet(request, response, userDTO, "parliament_house_summary_report",
				MenuConstants.AM_OFFICE_ASSIGNMENT_EMPLOYEE_WISE_REPORT_DETAILS, language, reportName, "parliament_house_summary_report");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		doGet(request, response);
	}
}
