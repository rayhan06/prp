package solved_ticket_report;


import java.io.IOException;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import language.LC;
import language.LM;

import login.LoginDTO;
import permission.MenuConstants;

import sessionmanager.SessionConstants;
import support_ticket.Support_ticketDTO;
import ticket_forward_history.Ticket_forward_historyDTO;
import user.UserDTO;
import user.UserRepository;

import util.ReportRequestHandler;
import pbReport.*;






@WebServlet("/Solved_ticket_report_Servlet")
public class Solved_ticket_report_Servlet  extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	String[][] Criteria =
	{
		{"criteria","ticket_forward_history","ticket_status_cat","=","","int","","",Support_ticketDTO.SOLVED + "","ticketStatusCat", LC.SOLVED_TICKET_REPORT_WHERE_TICKETSTATUSCAT + ""},		
		{"criteria","ticket_forward_history","forward_action_cat","=","AND","int","","",Ticket_forward_historyDTO.FORWARD  + "","forwardActionCat", LC.SOLVED_TICKET_REPORT_WHERE_FORWARDACTIONCAT + ""},		
		{"criteria","ticket_forward_history","ticket_issues_type","=","AND","int","","","any","ticketIssuesType", LC.SOLVED_TICKET_REPORT_WHERE_TICKETISSUESTYPE + ""},		
		{"criteria","","forward_from_organogram_id","=","AND","int","","","any","forwardFromOrganogramId", LC.SOLVED_TICKET_REPORT_WHERE_FORWARDFROMORGANOGRAMID + ""},		
		{"criteria","","issue_raiser_organogram_id","=","AND","int","","","any","issueRaiserOrganogramId", LC.SOLVED_TICKET_REPORT_WHERE_ISSUERAISERORGANOGRAMID + ""},		
		{"criteria","","forward_date",">=","AND","long","","",Long.MIN_VALUE + "","startDate", LC.HM_START_DATE + ""},		
		{"criteria","","forward_date","<=","AND","long","","",Long.MAX_VALUE + "","endDate", LC.HM_END_DATE + ""}		
	};
	
	String[][] Display =
	{
		{"display","support_ticket","id","text",""},		
		{"display","support_ticket","issue_raiser_organogram_id","organogram",""},		
		{"display","support_ticket","description","text",""},		
		{"display","support_ticket","ticket_issues_type","type",""},		
		{"display","support_ticket","issue_raiser_organogram_id","organogram_to_wing",""},		
		{"display","support_ticket","priority_cat","cat",""},		
		{"display","ticket_forward_history","forward_from_organogram_id","organogram",""}		
	};
	
	String GroupBy = "";
	String OrderBY = " forward_date desc ";
	
	ReportRequestHandler reportRequestHandler;
	
	public Solved_ticket_report_Servlet(){

	}
	
	private final ReportService reportService = new ReportService();
	
	private String sql;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String language = "english";
		if(userDTO!= null && userDTO.languageID == SessionConstants.BANGLA)
		{
			language = "bangla";
		}
				
		String actionType = request.getParameter("actionType");

		System.out.println("In ssservlet doget, actiontype = " + actionType);
		
		sql = "ticket_forward_history JOIN support_ticket ON ticket_forward_history.support_ticket_id = support_ticket.id";

		Display[0][4] = LM.getText(LC.HM_ID, loginDTO);
		Display[1][4] = LM.getText(LC.HM_ISSUE_RAISER, loginDTO);
		Display[2][4] = LM.getText(LC.SOLVED_TICKET_REPORT_SELECT_DESCRIPTION, loginDTO);
		Display[3][4] = LM.getText(LC.HM_TYPE, loginDTO);
		Display[4][4] = LM.getText(LC.HM_WING, loginDTO);
		Display[5][4] = language.equalsIgnoreCase("english")?"Priority":"গুরুত্ব";

		Display[6][4] = language.equalsIgnoreCase("english")?"Support Engineer":"সাপোর্ট ইঞ্জিনিয়ার";

		
		String reportName = LM.getText(LC.SOLVED_TICKET_REPORT_OTHER_SOLVED_TICKET_REPORT, loginDTO);
		
		reportRequestHandler = new ReportRequestHandler(null,
				Criteria, Display, GroupBy, OrderBY, sql,
				reportService);

		
		reportRequestHandler.handleReportGet(request, response, userDTO, "solved_ticket_report",
				MenuConstants.SOLVED_TICKET_REPORT_DETAILS, language, reportName, "solved_ticket_report");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		doGet(request, response);
	}
}
