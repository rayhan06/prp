package user_expertise_map;
import java.util.*; 
import util.*; 


public class User_expertise_mapDTO extends CommonDTO
{

	public long userId = -1;
    public String userName = "";
	public int userExpertiseCat = -1;
    public String remarks = "";
	
	
    @Override
	public String toString() {
            return "$User_expertise_mapDTO[" +
            " iD = " + iD +
            " userId = " + userId +
            " userName = " + userName +
            " userExpertiseCat = " + userExpertiseCat +
            " remarks = " + remarks +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}