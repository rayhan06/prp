package user_expertise_map;
import java.util.*; 
import util.*;


public class User_expertise_mapMAPS extends CommonMaps
{	
	public User_expertise_mapMAPS(String tableName)
	{
		

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("userId".toLowerCase(), "userId".toLowerCase());
		java_DTO_map.put("userName".toLowerCase(), "userName".toLowerCase());
		java_DTO_map.put("userExpertiseCat".toLowerCase(), "userExpertiseCat".toLowerCase());
		java_DTO_map.put("remarks".toLowerCase(), "remarks".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("user_id".toLowerCase(), "userId".toLowerCase());
		java_SQL_map.put("user_name".toLowerCase(), "userName".toLowerCase());
		java_SQL_map.put("user_expertise_cat".toLowerCase(), "userExpertiseCat".toLowerCase());
		java_SQL_map.put("remarks".toLowerCase(), "remarks".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("User Id".toLowerCase(), "userId".toLowerCase());
		java_Text_map.put("User Name".toLowerCase(), "userName".toLowerCase());
		java_Text_map.put("User Expertise".toLowerCase(), "userExpertiseCat".toLowerCase());
		java_Text_map.put("Remarks".toLowerCase(), "remarks".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}