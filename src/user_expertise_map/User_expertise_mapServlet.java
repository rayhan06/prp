package user_expertise_map;

import java.io.IOException;
import java.io.*;
import java.text.SimpleDateFormat;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import org.apache.log4j.Logger;

import login.LoginDTO;
import permission.MenuConstants;
import role.PermissionRepository;


import sessionmanager.SessionConstants;

import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;
import util.CommonRequestHandler;
import util.RecordNavigationManager4;

import java.util.*;
import javax.servlet.http.*;
import java.util.UUID;





import geolocation.GeoLocationDAO2;
import java.util.StringTokenizer;

import com.google.gson.Gson;

import pb.*;
import pbReport.*;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;




/**
 * Servlet implementation class User_expertise_mapServlet
 */
@WebServlet("/User_expertise_mapServlet")
@MultipartConfig
public class User_expertise_mapServlet extends HttpServlet 
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(User_expertise_mapServlet.class);

    String tableName = "user_expertise_map";

	User_expertise_mapDAO user_expertise_mapDAO;
	CommonRequestHandler commonRequestHandler;
    private final Gson gson = new Gson();
    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public User_expertise_mapServlet() 
	{
        super();
    	try
    	{
			user_expertise_mapDAO = new User_expertise_mapDAO(tableName);
			commonRequestHandler = new CommonRequestHandler(user_expertise_mapDAO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }   

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		System.out.println("In doget request = " + request);
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		try
		{
			String actionType = request.getParameter("actionType");
			if(actionType.equals("getAddPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.SUPPORT_TICKET_UPDATE))
				{
					commonRequestHandler.getAddPage(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getEditPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.SUPPORT_TICKET_UPDATE))
				{
					getUser_expertise_map(request, response);					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}						
			}
			else if(actionType.equals("getURL"))
			{
				String URL = request.getParameter("URL");
				System.out.println("URL = " + URL);
				response.sendRedirect(URL);			
			}			
			else if(actionType.equals("search"))
			{
				System.out.println("search requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.SUPPORT_TICKET_UPDATE))
				{
					if(isPermanentTable)
					{
						String filter = request.getParameter("filter");
						System.out.println("filter = " + filter);
						if(filter!=null)
						{
							filter = ""; //shouldn't be directly used, rather manipulate it.
							searchUser_expertise_map(request, response, isPermanentTable, filter);
						}
						else
						{
							searchUser_expertise_map(request, response, isPermanentTable, "");
						}
					}					
					else
					{
						//searchUser_expertise_map(request, response, tempTableName, isPermanentTable);
					}
				}			
			}
			else if(actionType.equals("view"))
			{
				System.out.println("view requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.SUPPORT_TICKET_UPDATE))
				{
					commonRequestHandler.view(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else
			{
				request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}


	
	

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		// TODO Auto-generated method stub
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		System.out.println("doPost");
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		
		try
		{
			String actionType = request.getParameter("actionType");
			System.out.println("actionType = " + actionType);
			if(actionType.equals("add"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.SUPPORT_TICKET_UPDATE))
				{
					System.out.println("going to  addUser_expertise_map ");
					addUser_expertise_map(request, response, true, userDTO, true);
				}
				else
				{
					System.out.println("Not going to  addUser_expertise_map ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}

			else if(actionType.equals("getDTO"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.SUPPORT_TICKET_UPDATE))
				{
					getDTO(request, response);					
				}
				else
				{
					System.out.println("Not going to  addUser_expertise_map ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else if(actionType.equals("edit"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.SUPPORT_TICKET_UPDATE))
				{					
					addUser_expertise_map(request, response, false, userDTO, isPermanentTable);					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("delete"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.SUPPORT_TICKET_UPDATE))
				{
					commonRequestHandler.delete(request, response, userDTO);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}	
			else if(actionType.equals("search"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.SUPPORT_TICKET_UPDATE))
				{
					searchUser_expertise_map(request, response, true, "");
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getGeo"))
			{
				System.out.println("going to geoloc ");
				request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
			}
			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}
	
	private void getDTO(HttpServletRequest request, HttpServletResponse response) 
	{
		try 
		{
			System.out.println("In getDTO");
			User_expertise_mapDTO user_expertise_mapDTO = user_expertise_mapDAO.getDTOByID(Long.parseLong(request.getParameter("ID")));
			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			
			String encoded = this.gson.toJson(user_expertise_mapDTO);
			System.out.println("json encoded data = " + encoded);
			out.print(encoded);
			out.flush();
		}
		catch (NumberFormatException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	private void addUser_expertise_map(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, boolean isPermanentTable) throws IOException 
	{
		// TODO Auto-generated method stub
		try 
		{
			request.setAttribute("failureMessage", "");
			System.out.println("%%%% addUser_expertise_map");
			String path = getServletContext().getRealPath("/img2/");
			User_expertise_mapDTO user_expertise_mapDTO;
			SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
						
			if(addFlag == true)
			{
				user_expertise_mapDTO = new User_expertise_mapDTO();
			}
			else
			{
				user_expertise_mapDTO = user_expertise_mapDAO.getDTOByID(Long.parseLong(request.getParameter("iD")));
			}
			
			String Value = "";

			Value = request.getParameter("userId");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("userId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				user_expertise_mapDTO.userId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("userName");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("userName = " + Value);
			if(Value != null)
			{
				user_expertise_mapDTO.userName = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("userExpertiseCat");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("userExpertiseCat = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				user_expertise_mapDTO.userExpertiseCat = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("remarks");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("remarks = " + Value);
			if(Value != null)
			{
				user_expertise_mapDTO.remarks = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			
			System.out.println("Done adding  addUser_expertise_map dto = " + user_expertise_mapDTO);
			long returnedID = -1;
			
			if(isPermanentTable == false) //add new row for validation and make the old row outdated
			{
				user_expertise_mapDAO.setIsDeleted(user_expertise_mapDTO.iD, CommonDTO.OUTDATED);
				returnedID = user_expertise_mapDAO.add(user_expertise_mapDTO);
				user_expertise_mapDAO.setIsDeleted(returnedID, CommonDTO.WAITING_FOR_APPROVAL);
			}
			else if(addFlag == true)
			{
				returnedID = user_expertise_mapDAO.manageWriteOperations(user_expertise_mapDTO, SessionConstants.INSERT, -1, userDTO);
			}
			else
			{				
				returnedID = user_expertise_mapDAO.manageWriteOperations(user_expertise_mapDTO, SessionConstants.UPDATE, -1, userDTO);											
			}
			
			
			
			
			
			
			
			
			
			if(isPermanentTable)
			{
				String inPlaceSubmit = request.getParameter("inplacesubmit");
				
				if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
				{
					getUser_expertise_map(request, response, returnedID);
				}
				else
				{
					response.sendRedirect("User_expertise_mapServlet?actionType=search");
				}
			}
			else
			{
				commonRequestHandler.validate(user_expertise_mapDAO.getDTOByID(returnedID), request, response, userDTO);
			}
					
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	



	
	
	

	private void getUser_expertise_map(HttpServletRequest request, HttpServletResponse response, long id) throws ServletException, IOException
	{
		System.out.println("in getUser_expertise_map");
		User_expertise_mapDTO user_expertise_mapDTO = null;
		try 
		{
			user_expertise_mapDTO = user_expertise_mapDAO.getDTOByID(id);
			request.setAttribute("ID", user_expertise_mapDTO.iD);
			request.setAttribute("user_expertise_mapDTO",user_expertise_mapDTO);
			request.setAttribute("user_expertise_mapDAO",user_expertise_mapDAO);
			
			String URL= "";
			
			String inPlaceEdit = request.getParameter("inplaceedit");
			String inPlaceSubmit = request.getParameter("inplacesubmit");
			String getBodyOnly = request.getParameter("getBodyOnly");
			
			if(inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase(""))
			{
				URL = "user_expertise_map/user_expertise_mapInPlaceEdit.jsp";	
				request.setAttribute("inplaceedit","");				
			}
			else if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
			{
				URL = "user_expertise_map/user_expertise_mapSearchRow.jsp";
				request.setAttribute("inplacesubmit","");					
			}
			else
			{
				if(getBodyOnly  != null && !getBodyOnly.equalsIgnoreCase(""))
				{
					URL = "user_expertise_map/user_expertise_mapEditBody.jsp?actionType=edit";
				}
				else
				{
					URL = "user_expertise_map/user_expertise_mapEdit.jsp?actionType=edit";
				}				
			}
			
			RequestDispatcher rd = request.getRequestDispatcher(URL);
			rd.forward(request, response);
		}
		catch (NumberFormatException e) 
		{
			e.printStackTrace();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	
	private void getUser_expertise_map(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		getUser_expertise_map(request, response, Long.parseLong(request.getParameter("ID")));	
	}
	
	private void searchUser_expertise_map(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException
	{
		System.out.println("in  searchUser_expertise_map 1");
		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String ajax = request.getParameter("ajax");
		boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);
		
        /*RecordNavigationManager4 rnManager = new RecordNavigationManager4(
			SessionConstants.NAV_USER_EXPERTISE_MAP,
			request,
			user_expertise_mapDAO,
			SessionConstants.VIEW_USER_EXPERTISE_MAP,
			SessionConstants.SEARCH_USER_EXPERTISE_MAP,
			tableName,
			isPermanent,
			userDTO,
			filter,
			true);
        try
        {
			System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        }
        catch(Exception e)
        {
			System.out.println("failed to dojob" + e);
        }

		request.setAttribute("user_expertise_mapDAO",user_expertise_mapDAO);
        RequestDispatcher rd;
        if(!isPermanent)
        {
        	if(hasAjax == false)
	        {
	        	System.out.println("Going to user_expertise_map/user_expertise_mapApproval.jsp");
	        	rd = request.getRequestDispatcher("user_expertise_map/user_expertise_mapApproval.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to user_expertise_map/user_expertise_mapApprovalForm.jsp");
	        	rd = request.getRequestDispatcher("user_expertise_map/user_expertise_mapApprovalForm.jsp");
	        }
        }
        else
        {
	        if(hasAjax == false)
	        {
	        	System.out.println("Going to user_expertise_map/user_expertise_mapSearch.jsp");
	        	rd = request.getRequestDispatcher("user_expertise_map/user_expertise_mapSearch.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to user_expertise_map/user_expertise_mapSearchForm.jsp");
	        	rd = request.getRequestDispatcher("user_expertise_map/user_expertise_mapSearchForm.jsp");
	        }
        }
		rd.forward(request, response);*/
	}
	
}

