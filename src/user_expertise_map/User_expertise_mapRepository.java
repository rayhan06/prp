package user_expertise_map;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class User_expertise_mapRepository implements Repository {
	User_expertise_mapDAO user_expertise_mapDAO = null;
	
	public void setDAO(User_expertise_mapDAO user_expertise_mapDAO)
	{
		this.user_expertise_mapDAO = user_expertise_mapDAO;
	}
	
	
	static Logger logger = Logger.getLogger(User_expertise_mapRepository.class);
	Map<Long, User_expertise_mapDTO>mapOfUser_expertise_mapDTOToiD;
	Map<Long, Set<User_expertise_mapDTO> >mapOfUser_expertise_mapDTOTouserId;
	Map<String, Set<User_expertise_mapDTO> >mapOfUser_expertise_mapDTOTouserName;
	Map<Integer, Set<User_expertise_mapDTO> >mapOfUser_expertise_mapDTOTouserExpertiseCat;
	Map<String, Set<User_expertise_mapDTO> >mapOfUser_expertise_mapDTOToremarks;
	Map<Long, Set<User_expertise_mapDTO> >mapOfUser_expertise_mapDTOTolastModificationTime;


	static User_expertise_mapRepository instance = null;  
	private User_expertise_mapRepository(){
		mapOfUser_expertise_mapDTOToiD = new ConcurrentHashMap<>();
		mapOfUser_expertise_mapDTOTouserId = new ConcurrentHashMap<>();
		mapOfUser_expertise_mapDTOTouserName = new ConcurrentHashMap<>();
		mapOfUser_expertise_mapDTOTouserExpertiseCat = new ConcurrentHashMap<>();
		mapOfUser_expertise_mapDTOToremarks = new ConcurrentHashMap<>();
		mapOfUser_expertise_mapDTOTolastModificationTime = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static User_expertise_mapRepository getInstance(){
		if (instance == null){
			instance = new User_expertise_mapRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(user_expertise_mapDAO == null)
		{
			return;
		}
		try {
			List<User_expertise_mapDTO> user_expertise_mapDTOs = user_expertise_mapDAO.getAllUser_expertise_map(reloadAll);
			for(User_expertise_mapDTO user_expertise_mapDTO : user_expertise_mapDTOs) {
				User_expertise_mapDTO oldUser_expertise_mapDTO = getUser_expertise_mapDTOByID(user_expertise_mapDTO.iD);
				if( oldUser_expertise_mapDTO != null ) {
					mapOfUser_expertise_mapDTOToiD.remove(oldUser_expertise_mapDTO.iD);
				
					if(mapOfUser_expertise_mapDTOTouserId.containsKey(oldUser_expertise_mapDTO.userId)) {
						mapOfUser_expertise_mapDTOTouserId.get(oldUser_expertise_mapDTO.userId).remove(oldUser_expertise_mapDTO);
					}
					if(mapOfUser_expertise_mapDTOTouserId.get(oldUser_expertise_mapDTO.userId).isEmpty()) {
						mapOfUser_expertise_mapDTOTouserId.remove(oldUser_expertise_mapDTO.userId);
					}
					
					if(mapOfUser_expertise_mapDTOTouserName.containsKey(oldUser_expertise_mapDTO.userName)) {
						mapOfUser_expertise_mapDTOTouserName.get(oldUser_expertise_mapDTO.userName).remove(oldUser_expertise_mapDTO);
					}
					if(mapOfUser_expertise_mapDTOTouserName.get(oldUser_expertise_mapDTO.userName).isEmpty()) {
						mapOfUser_expertise_mapDTOTouserName.remove(oldUser_expertise_mapDTO.userName);
					}
					
					if(mapOfUser_expertise_mapDTOTouserExpertiseCat.containsKey(oldUser_expertise_mapDTO.userExpertiseCat)) {
						mapOfUser_expertise_mapDTOTouserExpertiseCat.get(oldUser_expertise_mapDTO.userExpertiseCat).remove(oldUser_expertise_mapDTO);
					}
					if(mapOfUser_expertise_mapDTOTouserExpertiseCat.get(oldUser_expertise_mapDTO.userExpertiseCat).isEmpty()) {
						mapOfUser_expertise_mapDTOTouserExpertiseCat.remove(oldUser_expertise_mapDTO.userExpertiseCat);
					}
					
					if(mapOfUser_expertise_mapDTOToremarks.containsKey(oldUser_expertise_mapDTO.remarks)) {
						mapOfUser_expertise_mapDTOToremarks.get(oldUser_expertise_mapDTO.remarks).remove(oldUser_expertise_mapDTO);
					}
					if(mapOfUser_expertise_mapDTOToremarks.get(oldUser_expertise_mapDTO.remarks).isEmpty()) {
						mapOfUser_expertise_mapDTOToremarks.remove(oldUser_expertise_mapDTO.remarks);
					}
					
					if(mapOfUser_expertise_mapDTOTolastModificationTime.containsKey(oldUser_expertise_mapDTO.lastModificationTime)) {
						mapOfUser_expertise_mapDTOTolastModificationTime.get(oldUser_expertise_mapDTO.lastModificationTime).remove(oldUser_expertise_mapDTO);
					}
					if(mapOfUser_expertise_mapDTOTolastModificationTime.get(oldUser_expertise_mapDTO.lastModificationTime).isEmpty()) {
						mapOfUser_expertise_mapDTOTolastModificationTime.remove(oldUser_expertise_mapDTO.lastModificationTime);
					}
					
					
				}
				if(user_expertise_mapDTO.isDeleted == 0) 
				{
					
					mapOfUser_expertise_mapDTOToiD.put(user_expertise_mapDTO.iD, user_expertise_mapDTO);
				
					if( ! mapOfUser_expertise_mapDTOTouserId.containsKey(user_expertise_mapDTO.userId)) {
						mapOfUser_expertise_mapDTOTouserId.put(user_expertise_mapDTO.userId, new HashSet<>());
					}
					mapOfUser_expertise_mapDTOTouserId.get(user_expertise_mapDTO.userId).add(user_expertise_mapDTO);
					
					if( ! mapOfUser_expertise_mapDTOTouserName.containsKey(user_expertise_mapDTO.userName)) {
						mapOfUser_expertise_mapDTOTouserName.put(user_expertise_mapDTO.userName, new HashSet<>());
					}
					mapOfUser_expertise_mapDTOTouserName.get(user_expertise_mapDTO.userName).add(user_expertise_mapDTO);
					
					if( ! mapOfUser_expertise_mapDTOTouserExpertiseCat.containsKey(user_expertise_mapDTO.userExpertiseCat)) {
						mapOfUser_expertise_mapDTOTouserExpertiseCat.put(user_expertise_mapDTO.userExpertiseCat, new HashSet<>());
					}
					mapOfUser_expertise_mapDTOTouserExpertiseCat.get(user_expertise_mapDTO.userExpertiseCat).add(user_expertise_mapDTO);
					
					if( ! mapOfUser_expertise_mapDTOToremarks.containsKey(user_expertise_mapDTO.remarks)) {
						mapOfUser_expertise_mapDTOToremarks.put(user_expertise_mapDTO.remarks, new HashSet<>());
					}
					mapOfUser_expertise_mapDTOToremarks.get(user_expertise_mapDTO.remarks).add(user_expertise_mapDTO);
					
					if( ! mapOfUser_expertise_mapDTOTolastModificationTime.containsKey(user_expertise_mapDTO.lastModificationTime)) {
						mapOfUser_expertise_mapDTOTolastModificationTime.put(user_expertise_mapDTO.lastModificationTime, new HashSet<>());
					}
					mapOfUser_expertise_mapDTOTolastModificationTime.get(user_expertise_mapDTO.lastModificationTime).add(user_expertise_mapDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<User_expertise_mapDTO> getUser_expertise_mapList() {
		List <User_expertise_mapDTO> user_expertise_maps = new ArrayList<User_expertise_mapDTO>(this.mapOfUser_expertise_mapDTOToiD.values());
		return user_expertise_maps;
	}
	
	
	public User_expertise_mapDTO getUser_expertise_mapDTOByID( long ID){
		return mapOfUser_expertise_mapDTOToiD.get(ID);
	}
	
	
	public List<User_expertise_mapDTO> getUser_expertise_mapDTOByuser_id(long user_id) {
		return new ArrayList<>( mapOfUser_expertise_mapDTOTouserId.getOrDefault(user_id,new HashSet<>()));
	}
	
	
	public List<User_expertise_mapDTO> getUser_expertise_mapDTOByuser_name(String user_name) {
		return new ArrayList<>( mapOfUser_expertise_mapDTOTouserName.getOrDefault(user_name,new HashSet<>()));
	}
	
	
	public List<User_expertise_mapDTO> getUser_expertise_mapDTOByuser_expertise_cat(int user_expertise_cat) {
		return new ArrayList<>( mapOfUser_expertise_mapDTOTouserExpertiseCat.getOrDefault(user_expertise_cat,new HashSet<>()));
	}
	
	
	public List<User_expertise_mapDTO> getUser_expertise_mapDTOByremarks(String remarks) {
		return new ArrayList<>( mapOfUser_expertise_mapDTOToremarks.getOrDefault(remarks,new HashSet<>()));
	}
	
	
	public List<User_expertise_mapDTO> getUser_expertise_mapDTOBylastModificationTime(long lastModificationTime) {
		return new ArrayList<>( mapOfUser_expertise_mapDTOTolastModificationTime.getOrDefault(lastModificationTime,new HashSet<>()));
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "user_expertise_map";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


