package user_expertise_map;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;
import sessionmanager.SessionConstants;
import java.sql.SQLException;
import common.ConnectionAndStatementUtil;


import org.apache.log4j.Logger;

import dbm.*;

import repository.RepositoryManager;

import util.*;
import pb.*;
import user.UserDTO;

public class User_expertise_mapDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());

	public User_expertise_mapDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";
		commonMaps = new User_expertise_mapMAPS(tableName);
		columnNames = new String[] 
		{
			"ID",
			"user_id",
			"user_name",
			"user_expertise_cat",
			"remarks",
			"isDeleted",
			"lastModificationTime"
		};
	}
	
	public User_expertise_mapDAO()
	{
		this("user_expertise_map");		
	}
	
	public void setSearchColumn(User_expertise_mapDTO user_expertise_mapDTO)
	{
		user_expertise_mapDTO.searchColumn = "";
		user_expertise_mapDTO.searchColumn += user_expertise_mapDTO.userName + " ";
		user_expertise_mapDTO.searchColumn += CatDAO.getName("English", "user_expertise", user_expertise_mapDTO.userExpertiseCat) + " " + CatDAO.getName("Bangla", "user_expertise", user_expertise_mapDTO.userExpertiseCat) + " ";
		user_expertise_mapDTO.searchColumn += user_expertise_mapDTO.remarks + " ";
	}
	
	public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException
	{
		User_expertise_mapDTO user_expertise_mapDTO = (User_expertise_mapDTO)commonDTO;
		int index = 1;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(user_expertise_mapDTO);
		if(isInsert)
		{
			ps.setObject(index++,user_expertise_mapDTO.iD);
		}
		ps.setObject(index++,user_expertise_mapDTO.userId);
		ps.setObject(index++,user_expertise_mapDTO.userName);
		ps.setObject(index++,user_expertise_mapDTO.userExpertiseCat);
		ps.setObject(index++,user_expertise_mapDTO.remarks);
		if(isInsert)
		{
			ps.setObject(index++, 0);
			ps.setObject(index++, lastModificationTime);
		}
	}
	
	public User_expertise_mapDTO build(ResultSet rs)
	{
		try
		{
			User_expertise_mapDTO user_expertise_mapDTO = new User_expertise_mapDTO();
			user_expertise_mapDTO.iD = rs.getLong("ID");
			user_expertise_mapDTO.userId = rs.getLong("user_id");
			user_expertise_mapDTO.userName = rs.getString("user_name");
			user_expertise_mapDTO.userExpertiseCat = rs.getInt("user_expertise_cat");
			user_expertise_mapDTO.remarks = rs.getString("remarks");
			if(user_expertise_mapDTO.remarks == null)
			{
				user_expertise_mapDTO.remarks = "";
			}
			user_expertise_mapDTO.isDeleted = rs.getInt("isDeleted");
			user_expertise_mapDTO.lastModificationTime = rs.getLong("lastModificationTime");
			return user_expertise_mapDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
	
	

	public User_expertise_mapDTO getDTOByUserId (long userId)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE user_id = " + userId;
		User_expertise_mapDTO user_expertise_mapDTO = ConnectionAndStatementUtil.getT(sql,this::build);
		return user_expertise_mapDTO;
	}
	
	public User_expertise_mapDTO getDTOByID (long id)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID = " + id;
		User_expertise_mapDTO user_expertise_mapDTO = ConnectionAndStatementUtil.getT(sql,this::build);
		return user_expertise_mapDTO;
	}

	
	public List<User_expertise_mapDTO> getDTOs(Collection recordIDs)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID IN ( ";
		for(int i = 0;i<recordIDs.size();i++){
			if(i!=0){
				sql+=",";
			}
			sql+=((ArrayList)recordIDs).get(i);
		}
		sql+=")  order by lastModificationTime desc";
		printSql(sql);
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);		
	}
	
	public List<User_expertise_mapDTO> getAllUser_expertise_map (boolean isFirstReload)
    {
		String sql = "SELECT * FROM " + tableName + " WHERE ";
		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by " + tableName + ".lastModificationTime desc";
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);	
    }

	
	public List<User_expertise_mapDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO)
	{
		return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
	}

	
	public List<User_expertise_mapDTO> getDTOs(Hashtable  p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
			String filter, boolean tableHasJobCat)
	{
		String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat);
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);			
	}
	public String getSqlWithSearchCriteriaForNormalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
			UserDTO userDTO, String filter, boolean tableHasJobCat)
    {
		boolean viewAll = false;
		
		if(p_searchCriteria != null && p_searchCriteria.get("ViewAll") != null)
		{
			System.out.println("ViewAll = " + p_searchCriteria.get("ViewAll"));
			viewAll = true;
		}
		else
		{
			System.out.println("ViewAll is null ");
		}
		
		String sql = "SELECT ";
		if(category == GETIDS)
		{
			sql += " " + tableName + ".ID as ID ";
		}
		else if(category == GETCOUNT)
		{
			sql += " count( " + tableName + ".ID) as countID ";
		}
		else if(category == GETDTOS)
		{
			sql += " distinct " + tableName + ".* ";
		}
		sql += "FROM " + tableName + " ";
		sql += joinSQL;
		

		
		String AnyfieldSql = "";
		String AllFieldSql = "";
		
		if(p_searchCriteria != null)
		{
						
			
			Enumeration names = p_searchCriteria.keys();
			String str, value;
			
			AnyfieldSql = "(";
			
			if(p_searchCriteria.get("AnyField")!= null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase(""))
			{
				AnyfieldSql+= tableName + ".search_column like '%" + p_searchCriteria.get("AnyField").toString() + "%'";			
			}
			AnyfieldSql += ")";
			System.out.println("AnyfieldSql = " + AnyfieldSql);
			
			AllFieldSql = "(";
			int i = 0;
			while(names.hasMoreElements())
			{				
				str = (String) names.nextElement();
				value = (String)p_searchCriteria.get(str);
		        System.out.println(str + ": " + value);
				if(value != null && !value.equalsIgnoreCase("") && (
						 str.equals("user_name")
						|| str.equals("user_expertise_cat")
						|| str.equals("remarks")
				)
						
				)
				{
					if(p_searchCriteria.get(str).equals("any"))
					{
						continue;
					}

					if( i > 0)
					{
						AllFieldSql+= " AND  ";
					}
					
					 if(str.equals("user_name"))
					{
						AllFieldSql += "" + tableName + ".user_name like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("user_expertise_cat"))
					{
						AllFieldSql += "" + tableName + ".user_expertise_cat = " + p_searchCriteria.get(str);
						i ++;
					}
					else if(str.equals("remarks"))
					{
						AllFieldSql += "" + tableName + ".remarks like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					
					
				}
			}
			
			AllFieldSql += ")";
			System.out.println("AllFieldSql = " + AllFieldSql);
			
			
		}
		
		
		sql += " WHERE ";
		
		sql += " (" + tableName + ".isDeleted = 0 ";			
		sql += ")";
		
		
		if(!filter.equalsIgnoreCase(""))
		{
			sql += " and " + filter + " ";
		}
		
		if(!AnyfieldSql.equals("()") && !AnyfieldSql.equals(""))
		{
			sql += " AND " + AnyfieldSql;
			
		}
		if(!AllFieldSql.equals("()") && !AllFieldSql.equals(""))
		{			
			sql += " AND " + AllFieldSql;
		}
		
	
			
		sql += " order by " + tableName + ".lastModificationTime desc ";

		printSql(sql);
		
		if(limit >= 0)
		{
			sql += " limit " + limit;
		}
		if(offset >= 0)
		{
			sql += " offset " + offset;
		}
		
		System.out.println("-------------- sql = " + sql);
		
		return sql;
    }
	
	public String getSqlWithSearchCriteria(Hashtable p_searchCriteria, int limit, int offset, int category,
			boolean isPermanentTable, UserDTO userDTO, String filter, boolean tableHasJobCat)
    {		
		return getSqlWithSearchCriteriaForNormalSearch(p_searchCriteria, limit, offset, category, userDTO, filter, tableHasJobCat);	
    }
				
}
	