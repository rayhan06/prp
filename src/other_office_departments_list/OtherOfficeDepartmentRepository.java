package other_office_departments_list;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import com.google.gson.Gson;

import repository.Repository;
import repository.RepositoryManager;


public class OtherOfficeDepartmentRepository implements Repository {
	OtherOfficeDepartmentDAO otherofficedepartmentDAO = null;
	
	static Logger logger = Logger.getLogger(OtherOfficeDepartmentRepository.class);
	Map<Long, OtherOfficeDepartmentDTO>mapOfOtherOfficeDepartmentDTOToiD;
	Map<Long, Set<OtherOfficeDepartmentDTO> >mapOfOtherOfficeDepartmentDTOTootherOfficeDepartmentsListId;
	Map<Long, Set<OtherOfficeDepartmentDTO> >mapOfOtherOfficeDepartmentDTOTootherOfficeId;
	Gson gson;

  
	private OtherOfficeDepartmentRepository(){
		otherofficedepartmentDAO = OtherOfficeDepartmentDAO.getInstance();
		mapOfOtherOfficeDepartmentDTOToiD = new ConcurrentHashMap<>();
		mapOfOtherOfficeDepartmentDTOTootherOfficeDepartmentsListId = new ConcurrentHashMap<>();
		mapOfOtherOfficeDepartmentDTOTootherOfficeId = new ConcurrentHashMap<>();
		gson = new Gson();
		RepositoryManager.getInstance().addRepository(this);
	}

	private static class LazyLoader{
        final static OtherOfficeDepartmentRepository INSTANCE = new OtherOfficeDepartmentRepository();
    }

    public static OtherOfficeDepartmentRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

	public void reload(boolean reloadAll){
		try {
			List<OtherOfficeDepartmentDTO> otherofficedepartmentDTOs = otherofficedepartmentDAO.getAllDTOs(reloadAll);
			for(OtherOfficeDepartmentDTO otherofficedepartmentDTO : otherofficedepartmentDTOs) {
				OtherOfficeDepartmentDTO oldOtherOfficeDepartmentDTO = getOtherOfficeDepartmentDTOByiD(otherofficedepartmentDTO.iD);
				if( oldOtherOfficeDepartmentDTO != null ) {
					mapOfOtherOfficeDepartmentDTOToiD.remove(oldOtherOfficeDepartmentDTO.iD);
				
					if(mapOfOtherOfficeDepartmentDTOTootherOfficeDepartmentsListId.containsKey(oldOtherOfficeDepartmentDTO.otherOfficeDepartmentsListId)) {
						mapOfOtherOfficeDepartmentDTOTootherOfficeDepartmentsListId.get(oldOtherOfficeDepartmentDTO.otherOfficeDepartmentsListId).remove(oldOtherOfficeDepartmentDTO);
					}
					if(mapOfOtherOfficeDepartmentDTOTootherOfficeDepartmentsListId.get(oldOtherOfficeDepartmentDTO.otherOfficeDepartmentsListId).isEmpty()) {
						mapOfOtherOfficeDepartmentDTOTootherOfficeDepartmentsListId.remove(oldOtherOfficeDepartmentDTO.otherOfficeDepartmentsListId);
					}
					
					if(mapOfOtherOfficeDepartmentDTOTootherOfficeId.containsKey(oldOtherOfficeDepartmentDTO.otherOfficeId)) {
						mapOfOtherOfficeDepartmentDTOTootherOfficeId.get(oldOtherOfficeDepartmentDTO.otherOfficeId).remove(oldOtherOfficeDepartmentDTO);
					}
					if(mapOfOtherOfficeDepartmentDTOTootherOfficeId.get(oldOtherOfficeDepartmentDTO.otherOfficeId).isEmpty()) {
						mapOfOtherOfficeDepartmentDTOTootherOfficeId.remove(oldOtherOfficeDepartmentDTO.otherOfficeId);
					}
					
					
				}
				if(otherofficedepartmentDTO.isDeleted == 0) 
				{
					
					mapOfOtherOfficeDepartmentDTOToiD.put(otherofficedepartmentDTO.iD, otherofficedepartmentDTO);
				
					if( ! mapOfOtherOfficeDepartmentDTOTootherOfficeDepartmentsListId.containsKey(otherofficedepartmentDTO.otherOfficeDepartmentsListId)) {
						mapOfOtherOfficeDepartmentDTOTootherOfficeDepartmentsListId.put(otherofficedepartmentDTO.otherOfficeDepartmentsListId, new HashSet<>());
					}
					mapOfOtherOfficeDepartmentDTOTootherOfficeDepartmentsListId.get(otherofficedepartmentDTO.otherOfficeDepartmentsListId).add(otherofficedepartmentDTO);
					
					if( ! mapOfOtherOfficeDepartmentDTOTootherOfficeId.containsKey(otherofficedepartmentDTO.otherOfficeId)) {
						mapOfOtherOfficeDepartmentDTOTootherOfficeId.put(otherofficedepartmentDTO.otherOfficeId, new HashSet<>());
					}
					mapOfOtherOfficeDepartmentDTOTootherOfficeId.get(otherofficedepartmentDTO.otherOfficeId).add(otherofficedepartmentDTO);
		
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public OtherOfficeDepartmentDTO clone(OtherOfficeDepartmentDTO dto) {
		String raw = gson.toJson(dto);
		return gson.fromJson(raw, OtherOfficeDepartmentDTO.class);
	}
	
	
	public List<OtherOfficeDepartmentDTO> getOtherOfficeDepartmentList() {
		List <OtherOfficeDepartmentDTO> otherofficedepartments = new ArrayList<OtherOfficeDepartmentDTO>(this.mapOfOtherOfficeDepartmentDTOToiD.values());
		return otherofficedepartments;
	}
	
	public List<OtherOfficeDepartmentDTO> copyOtherOfficeDepartmentList() {
		List <OtherOfficeDepartmentDTO> otherofficedepartments = getOtherOfficeDepartmentList();
		return otherofficedepartments
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}
	
	
	
	public OtherOfficeDepartmentDTO getOtherOfficeDepartmentDTOByiD( long iD){
		return mapOfOtherOfficeDepartmentDTOToiD.get(iD);
	}
	
	public OtherOfficeDepartmentDTO copyOtherOfficeDepartmentDTOByiD( long iD){
		return clone(mapOfOtherOfficeDepartmentDTOToiD.get(iD));
	}
	
	
	public List<OtherOfficeDepartmentDTO> getOtherOfficeDepartmentDTOByotherOfficeDepartmentsListId(long otherOfficeDepartmentsListId) {
		return new ArrayList<>( mapOfOtherOfficeDepartmentDTOTootherOfficeDepartmentsListId.getOrDefault(otherOfficeDepartmentsListId,new HashSet<>()));
	}
	
	public List<OtherOfficeDepartmentDTO> copyOtherOfficeDepartmentDTOByotherOfficeDepartmentsListId(long otherOfficeDepartmentsListId)
	{
		List <OtherOfficeDepartmentDTO> otherofficedepartments = getOtherOfficeDepartmentDTOByotherOfficeDepartmentsListId(otherOfficeDepartmentsListId);
		return otherofficedepartments
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}
	
	public List<OtherOfficeDepartmentDTO> getOtherOfficeDepartmentDTOByotherOfficeId(long otherOfficeId) {
		return new ArrayList<>( mapOfOtherOfficeDepartmentDTOTootherOfficeId.getOrDefault(otherOfficeId,new HashSet<>()));
	}
	
	public List<OtherOfficeDepartmentDTO> copyOtherOfficeDepartmentDTOByotherOfficeId(long otherOfficeId)
	{
		List <OtherOfficeDepartmentDTO> otherofficedepartments = getOtherOfficeDepartmentDTOByotherOfficeId(otherOfficeId);
		return otherofficedepartments
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}

	
	@Override
	public String getTableName() {
		return otherofficedepartmentDAO.getTableName();
	}
}


