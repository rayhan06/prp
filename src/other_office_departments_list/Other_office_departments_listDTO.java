package other_office_departments_list;
import java.util.*; 
import util.*; 


public class Other_office_departments_listDTO extends CommonDTO
{

	public long otherOfficeType = -1;
	public long modifiedBy = -1;
	public long insertedBy = -1;
	public long insertionDate = -1;
	
	public List<OtherOfficeDepartmentDTO> otherOfficeDepartmentDTOList = new ArrayList<>();
	
    @Override
	public String toString() {
            return "$Other_office_departments_listDTO[" +
            " iD = " + iD +
            " otherOfficeType = " + otherOfficeType +
            " searchColumn = " + searchColumn +
            " modifiedBy = " + modifiedBy +
            " insertedBy = " + insertedBy +
            " insertionDate = " + insertionDate +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}