package other_office_departments_list;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import com.google.gson.Gson;

import repository.Repository;
import repository.RepositoryManager;


public class Other_office_departments_listRepository implements Repository {
	Other_office_departments_listDAO other_office_departments_listDAO = null;
	
	static Logger logger = Logger.getLogger(Other_office_departments_listRepository.class);
	Map<Long, Other_office_departments_listDTO>mapOfOther_office_departments_listDTOToiD;
	Gson gson;

  
	private Other_office_departments_listRepository(){
		other_office_departments_listDAO = Other_office_departments_listDAO.getInstance();
		mapOfOther_office_departments_listDTOToiD = new ConcurrentHashMap<>();
		gson = new Gson();
		RepositoryManager.getInstance().addRepository(this);
	}

	private static class LazyLoader{
        final static Other_office_departments_listRepository INSTANCE = new Other_office_departments_listRepository();
    }

    public static Other_office_departments_listRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

	public void reload(boolean reloadAll){
		try {
			List<Other_office_departments_listDTO> other_office_departments_listDTOs = other_office_departments_listDAO.getAllDTOs(reloadAll);
			for(Other_office_departments_listDTO other_office_departments_listDTO : other_office_departments_listDTOs) {
				Other_office_departments_listDTO oldOther_office_departments_listDTO = getOther_office_departments_listDTOByiD(other_office_departments_listDTO.iD);
				if( oldOther_office_departments_listDTO != null ) {
					mapOfOther_office_departments_listDTOToiD.remove(oldOther_office_departments_listDTO.iD);
				
					
				}
				if(other_office_departments_listDTO.isDeleted == 0) 
				{
					
					mapOfOther_office_departments_listDTOToiD.put(other_office_departments_listDTO.iD, other_office_departments_listDTO);
				
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public Other_office_departments_listDTO clone(Other_office_departments_listDTO dto) {
		String raw = gson.toJson(dto);
		return gson.fromJson(raw, Other_office_departments_listDTO.class);
	}
	
	
	public List<Other_office_departments_listDTO> getOther_office_departments_listList() {
		List <Other_office_departments_listDTO> other_office_departments_lists = new ArrayList<Other_office_departments_listDTO>(this.mapOfOther_office_departments_listDTOToiD.values());
		return other_office_departments_lists;
	}
	
	public List<Other_office_departments_listDTO> copyOther_office_departments_listList() {
		List <Other_office_departments_listDTO> other_office_departments_lists = getOther_office_departments_listList();
		return other_office_departments_lists
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}
	
	
	
	public Other_office_departments_listDTO getOther_office_departments_listDTOByiD( long iD){
		return mapOfOther_office_departments_listDTOToiD.get(iD);
	}
	
	public Other_office_departments_listDTO copyOther_office_departments_listDTOByiD( long iD){
		return clone(mapOfOther_office_departments_listDTOToiD.get(iD));
	}

	
	@Override
	public String getTableName() {
		return other_office_departments_listDAO.getTableName();
	}
}


