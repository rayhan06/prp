package other_office_departments_list;


import java.io.IOException;
import java.text.SimpleDateFormat;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import org.apache.log4j.Logger;


import permission.MenuConstants;


import user.UserDTO;
import util.*;
import javax.servlet.http.*;
import java.util.*;

import common.ApiResponse;
import common.BaseServlet;
import language.LC;
import language.LM;

import com.google.gson.Gson;

import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;




/**
 * Servlet implementation class Other_office_departments_listServlet
 */
@WebServlet("/Other_office_departments_listServlet")
@MultipartConfig
public class Other_office_departments_listServlet extends BaseServlet 
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Other_office_departments_listServlet.class);

    @Override
    public String getTableName() {
        return Other_office_departments_listDAO.getInstance().getTableName();
    }

    @Override
    public String getServletName() {
        return "Other_office_departments_listServlet";
    }

    @Override
    public Other_office_departments_listDAO getCommonDAOService() {
        return Other_office_departments_listDAO.getInstance();
    }
    
    @Override
    public int[] getAddPageMenuConstants() {
        return new int[] {MenuConstants.OTHER_OFFICE_DEPARTMENTS_LIST_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[] {MenuConstants.OTHER_OFFICE_DEPARTMENTS_LIST_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[] {MenuConstants.OTHER_OFFICE_DEPARTMENTS_LIST_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Other_office_departments_listServlet.class;
    }
	OtherOfficeDepartmentDAO otherOfficeDepartmentDAO = OtherOfficeDepartmentDAO.getInstance();
    private final Gson gson = new Gson();
 	
    public void getOfficeDepts(long otherOfficeId, String options, CommonLoginData commonLoginData, HttpServletResponse response, long dept) throws IOException
    {
    	List<OtherOfficeDepartmentDTO> otherOfficeDepartmentDTOs = OtherOfficeDepartmentRepository.getInstance().getOtherOfficeDepartmentDTOByotherOfficeId(otherOfficeId);
		if(otherOfficeDepartmentDTOs != null)
		{
			for(OtherOfficeDepartmentDTO otherOfficeDepartmentDTO: otherOfficeDepartmentDTOs)
			{
				options += "<option value = '" + otherOfficeDepartmentDTO.iD + "'";
				if(dept == otherOfficeDepartmentDTO.iD)
				{
					options += " selected ";
				}
				options +=  ">";
				if(commonLoginData.isLangEng)
				{
					options += otherOfficeDepartmentDTO.nameEn;
				}
				else
				{
					options += otherOfficeDepartmentDTO.nameBn;
				}
				options += "</option>";
			}
		}
		response.getWriter().write(options);
    }
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        CommonLoginData commonLoginData = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get();
        init(request);
        String actionType = request.getParameter("actionType");
		if ("getDept".equals(actionType)) {
			try {
				long otherOfficeId = Long.parseLong(request.getParameter("otherOfficeId"));
				long dept = -1;
				if(request.getParameter("dept") != null)
				{
					dept = Long.parseLong(request.getParameter("dept"));
				}
				if(otherOfficeId >= 0)
				{
					String options = "<option value='-1'";
					if(dept == -1)
					{
						options += " selected ";
					}
					options += "> " + LM.getText(LC.HM_SELECT, commonLoginData.loginDTO) + "</option>";
					getOfficeDepts(otherOfficeId, options, commonLoginData, response, dept);
				}
			} catch (Exception ex) {
				logger.error(ex);
				ApiResponse.sendErrorResponse(response, ex.getMessage());
			}
			return;
		}
		if ("getDeptReport".equals(actionType)) {
			try {
				long otherOfficeId = Long.parseLong(request.getParameter("otherOfficeId"));
				if(otherOfficeId >= 0)
				{
					String options = "<option value=''> " + LM.getText(LC.HM_SELECT, commonLoginData.loginDTO) + "</option>";
					getOfficeDepts(otherOfficeId, options, commonLoginData, response, -1);
				}
			} catch (Exception ex) {
				logger.error(ex);
				ApiResponse.sendErrorResponse(response, ex.getMessage());
			}
			return;
		}	
		else
		{
			super.doGet(request,response);
		}
    }

	@Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception 
	{
		
		request.setAttribute("failureMessage", "");
		System.out.println("%%%% addOther_office_departments_list");
		String language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
		Other_office_departments_listDTO other_office_departments_listDTO;
		SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
					
		if(addFlag)
		{
			other_office_departments_listDTO = new Other_office_departments_listDTO();
		}
		else
		{
			other_office_departments_listDTO = Other_office_departments_listDAO.getInstance().getDTOFromID(Long.parseLong(request.getParameter("iD")));
		}
		
		String Value = "";

		Value = request.getParameter("otherOfficeType");

		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("otherOfficeType = " + Value);
		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			other_office_departments_listDTO.otherOfficeType = Long.parseLong(Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}
		
		Other_office_departments_listDTO oldDTO = Other_office_departments_listDAO.getInstance().getByOffice(other_office_departments_listDTO.otherOfficeType);
		if(addFlag && oldDTO != null)
		{
			throw new Exception("Data exists for this office. Delete or update it.");
		}
		
		if(addFlag)
		{				
			other_office_departments_listDTO.insertionDate = TimeConverter.getToday();
			other_office_departments_listDTO.insertedBy = userDTO.ID;
		}
		other_office_departments_listDTO.modifiedBy = userDTO.ID;

		System.out.println("Done adding  addOther_office_departments_list dto = " + other_office_departments_listDTO);

		if(addFlag == true)
		{
			Other_office_departments_listDAO.getInstance().add(other_office_departments_listDTO);
		}
		else
		{				
			Other_office_departments_listDAO.getInstance().update(other_office_departments_listDTO);										
		}
		
		List<OtherOfficeDepartmentDTO> otherOfficeDepartmentDTOList = createOtherOfficeDepartmentDTOListByRequest(request, language, other_office_departments_listDTO, userDTO);
		

		if(addFlag == true) //add or validate
		{
			if(otherOfficeDepartmentDTOList != null)
			{				
				for(OtherOfficeDepartmentDTO otherOfficeDepartmentDTO: otherOfficeDepartmentDTOList)
				{
					otherOfficeDepartmentDAO.add(otherOfficeDepartmentDTO);
				}
			}
		
		}			
		else
		{
			List<Long> childIdsFromRequest = otherOfficeDepartmentDAO.getChildIdsFromRequest(request, "otherOfficeDepartment");
			//delete the removed children
			otherOfficeDepartmentDAO.deleteChildrenNotInList("other_office_departments_list", "other_office_department", other_office_departments_listDTO.iD, childIdsFromRequest);
			List<Long> childIDsInDatabase = otherOfficeDepartmentDAO.getChilIds("other_office_departments_list", "other_office_department", other_office_departments_listDTO.iD);
			
			
			if(childIdsFromRequest != null)
			{
				for(int i = 0; i < childIdsFromRequest.size(); i ++)
				{
					Long childIDFromRequest = childIdsFromRequest.get(i);
					if(childIDsInDatabase.contains(childIDFromRequest))
					{
						OtherOfficeDepartmentDTO otherOfficeDepartmentDTO =  createOtherOfficeDepartmentDTOByRequestAndIndex(request, false, i, language, other_office_departments_listDTO, userDTO);
						otherOfficeDepartmentDAO.update(otherOfficeDepartmentDTO);
					}
					else
					{
						OtherOfficeDepartmentDTO otherOfficeDepartmentDTO =  createOtherOfficeDepartmentDTOByRequestAndIndex(request, true, i, language, other_office_departments_listDTO, userDTO);
						otherOfficeDepartmentDAO.add(otherOfficeDepartmentDTO);
					}
				}
			}
			else
			{
				otherOfficeDepartmentDAO.deleteChildrenByParent(other_office_departments_listDTO.iD, "other_office_departments_list_id");
			}
			
		}
		Other_office_departments_listRepository.getInstance().reload(false);
		OtherOfficeDepartmentRepository.getInstance().reload(false);
		return other_office_departments_listDTO;

	}
	private List<OtherOfficeDepartmentDTO> createOtherOfficeDepartmentDTOListByRequest(HttpServletRequest request, String language, Other_office_departments_listDTO other_office_departments_listDTO, UserDTO userDTO) throws Exception{ 
		List<OtherOfficeDepartmentDTO> otherOfficeDepartmentDTOList = new ArrayList<OtherOfficeDepartmentDTO>();
		if(request.getParameterValues("otherOfficeDepartment.iD") != null) 
		{
			int otherOfficeDepartmentItemNo = request.getParameterValues("otherOfficeDepartment.iD").length;
			
			
			for(int index=0;index<otherOfficeDepartmentItemNo;index++){
				OtherOfficeDepartmentDTO otherOfficeDepartmentDTO = createOtherOfficeDepartmentDTOByRequestAndIndex(request,true,index, language, other_office_departments_listDTO, userDTO);
				otherOfficeDepartmentDTOList.add(otherOfficeDepartmentDTO);
			}
			
			return otherOfficeDepartmentDTOList;
		}
		return null;
	}
	
	private OtherOfficeDepartmentDTO createOtherOfficeDepartmentDTOByRequestAndIndex(HttpServletRequest request, boolean addFlag, int index, String language, Other_office_departments_listDTO other_office_departments_listDTO, UserDTO userDTO) throws Exception{
	
		OtherOfficeDepartmentDTO otherOfficeDepartmentDTO;
		if(addFlag == true )
		{
			otherOfficeDepartmentDTO = new OtherOfficeDepartmentDTO();
		}
		else
		{
			otherOfficeDepartmentDTO = otherOfficeDepartmentDAO.getDTOByID(Long.parseLong(request.getParameterValues("otherOfficeDepartment.iD")[index]));
		}
		
		
		
		otherOfficeDepartmentDTO.otherOfficeDepartmentsListId = other_office_departments_listDTO.iD; 

		String Value = "";

		otherOfficeDepartmentDTO.otherOfficeId = other_office_departments_listDTO.otherOfficeType;

		Value = request.getParameterValues("otherOfficeDepartment.nameEn")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("nameEn = " + Value);
		if(Value != null)
		{
			otherOfficeDepartmentDTO.nameEn = (Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}

		Value = request.getParameterValues("otherOfficeDepartment.nameBn")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("nameBn = " + Value);
		if(Value != null)
		{
			otherOfficeDepartmentDTO.nameBn = (Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}

		if(addFlag)
		{				
			otherOfficeDepartmentDTO.insertionDate = TimeConverter.getToday();
			otherOfficeDepartmentDTO.insertedBy = userDTO.ID;
		}
		otherOfficeDepartmentDTO.modifiedBy = userDTO.ID;

		return otherOfficeDepartmentDTO;
	
	}	
}

