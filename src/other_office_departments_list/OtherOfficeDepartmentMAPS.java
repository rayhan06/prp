package other_office_departments_list;
import java.util.*; 
import util.*;


public class OtherOfficeDepartmentMAPS extends CommonMaps
{	
	public OtherOfficeDepartmentMAPS(String tableName)
	{
		


		java_SQL_map.put("other_office_departments_list_id".toLowerCase(), "otherOfficeDepartmentsListId".toLowerCase());
		java_SQL_map.put("other_office_id".toLowerCase(), "otherOfficeId".toLowerCase());
		java_SQL_map.put("name_en".toLowerCase(), "nameEn".toLowerCase());
		java_SQL_map.put("name_bn".toLowerCase(), "nameBn".toLowerCase());
		java_SQL_map.put("modified_by".toLowerCase(), "modifiedBy".toLowerCase());
		java_SQL_map.put("inserted_by".toLowerCase(), "insertedBy".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Other Office Departments List Id".toLowerCase(), "otherOfficeDepartmentsListId".toLowerCase());
		java_Text_map.put("Other Office Id".toLowerCase(), "otherOfficeId".toLowerCase());
		java_Text_map.put("English Name".toLowerCase(), "nameEn".toLowerCase());
		java_Text_map.put("Bangla Name".toLowerCase(), "nameBn".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
		java_Text_map.put("Search Column".toLowerCase(), "searchColumn".toLowerCase());
		java_Text_map.put("Modified By".toLowerCase(), "modifiedBy".toLowerCase());
		java_Text_map.put("Inserted By".toLowerCase(), "insertedBy".toLowerCase());
		java_Text_map.put("Insertion Date".toLowerCase(), "insertionDate".toLowerCase());
			
	}

}