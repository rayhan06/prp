package other_office_departments_list;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.sql.SQLException;
import common.CommonDAOService;
import common.ConnectionAndStatementUtil;

import org.apache.log4j.Logger;
import util.*;
import pb.*;

public class Other_office_departments_listDAO  implements CommonDAOService<Other_office_departments_listDTO>
{
	
	Logger logger = Logger.getLogger(getClass());
	
	private static String addQuery = "";
	private static String updateQuery = "";
	private String[] columnNames = null;

	private Other_office_departments_listDAO()
	{
		columnNames = new String[] 
		{
			"ID",
			"other_office_type",
			"search_column",
			"modified_by",
			"inserted_by",
			"insertion_date",
			"isDeleted",
			"lastModificationTime"
		};
		updateQuery = getUpdateQuery(columnNames);
        addQuery = getAddQuery(columnNames);
		
		searchMap.put("other_office_type"," and (other_office_type = ?)");
		searchMap.put("modified_by"," and (modified_by like ?)");
		searchMap.put("inserted_by"," and (inserted_by like ?)");
		searchMap.put("insertion_date_start"," and (insertion_date >= ?)");
		searchMap.put("insertion_date_end"," and (insertion_date <= ?)");
		searchMap.put("AnyField"," and (search_column like ?)");
	}

	private final Map<String,String> searchMap = new HashMap<>();

    private static class LazyLoader
    {
    	static final Other_office_departments_listDAO INSTANCE = new Other_office_departments_listDAO();
	}

	public static Other_office_departments_listDAO getInstance()
	{
    	return LazyLoader.INSTANCE;
	}
	
	public void setSearchColumn(Other_office_departments_listDTO other_office_departments_listDTO)
	{
		other_office_departments_listDTO.searchColumn = "";
		other_office_departments_listDTO.searchColumn += CommonDAO.getName("English", "other_office", other_office_departments_listDTO.otherOfficeType) + " " + CommonDAO.getName("Bangla", "other_office", other_office_departments_listDTO.otherOfficeType) + " ";
		other_office_departments_listDTO.searchColumn += other_office_departments_listDTO.modifiedBy + " ";
		other_office_departments_listDTO.searchColumn += other_office_departments_listDTO.insertedBy + " ";
	}
	
	@Override
	public void set(PreparedStatement ps, Other_office_departments_listDTO other_office_departments_listDTO, boolean isInsert) throws SQLException
	{
		int index = 0;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(other_office_departments_listDTO);
		if(isInsert)
		{
			ps.setObject(++index,other_office_departments_listDTO.iD);
		}
		ps.setObject(++index,other_office_departments_listDTO.otherOfficeType);
		ps.setObject(++index,other_office_departments_listDTO.searchColumn);
		ps.setObject(++index,other_office_departments_listDTO.modifiedBy);
		ps.setObject(++index,other_office_departments_listDTO.insertedBy);
		ps.setObject(++index,other_office_departments_listDTO.insertionDate);
		if(isInsert)
		{
			ps.setObject(++index,other_office_departments_listDTO.isDeleted);
		}
		ps.setObject(++index, lastModificationTime);
		if(!isInsert)
		{
			ps.setObject(++index,other_office_departments_listDTO.iD);
		}
	}
	
	@Override
	public Other_office_departments_listDTO buildObjectFromResultSet(ResultSet rs)
	{
		try
		{
			Other_office_departments_listDTO other_office_departments_listDTO = new Other_office_departments_listDTO();
			int i = 0;
			other_office_departments_listDTO.iD = rs.getLong(columnNames[i++]);
			other_office_departments_listDTO.otherOfficeType = rs.getLong(columnNames[i++]);
			other_office_departments_listDTO.searchColumn = rs.getString(columnNames[i++]);
			other_office_departments_listDTO.modifiedBy = rs.getLong(columnNames[i++]);
			other_office_departments_listDTO.insertedBy = rs.getLong(columnNames[i++]);
			other_office_departments_listDTO.insertionDate = rs.getLong(columnNames[i++]);
			other_office_departments_listDTO.isDeleted = rs.getInt(columnNames[i++]);
			other_office_departments_listDTO.lastModificationTime = rs.getLong(columnNames[i++]);
			return other_office_departments_listDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
	
	public Other_office_departments_listDTO getByOffice(long otherOfficeId)
    {
		String sql = "select * from " + getTableName() + " where isDeleted = 0 and other_office_type = " + otherOfficeId;

		return ConnectionAndStatementUtil.getT(sql,this::buildObjectFromResultSet);	
    }
		
	public Other_office_departments_listDTO getDTOByID (long id)
	{
		Other_office_departments_listDTO other_office_departments_listDTO = null;
		try 
		{
			other_office_departments_listDTO = getDTOFromID(id);
			if(other_office_departments_listDTO != null)
			{
				OtherOfficeDepartmentDAO otherOfficeDepartmentDAO = OtherOfficeDepartmentDAO.getInstance();				
				List<OtherOfficeDepartmentDTO> otherOfficeDepartmentDTOList = (List<OtherOfficeDepartmentDTO>)otherOfficeDepartmentDAO.getDTOsByParent("other_office_departments_list_id", other_office_departments_listDTO.iD);
				other_office_departments_listDTO.otherOfficeDepartmentDTOList = otherOfficeDepartmentDTOList;
			}
		}
		catch (Exception e) 
		{
				// TODO Auto-generated catch block
				e.printStackTrace();
		}
		return other_office_departments_listDTO;
	}

	@Override
	public String getTableName() {
		return "other_office_departments_list";
	}

	@Override
	public Map<String, String> getSearchMap() {
		return searchMap;
	}

	public long add(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Other_office_departments_listDTO) commonDTO,addQuery,true);
	}

	public long update(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Other_office_departments_listDTO) commonDTO,updateQuery,false);
	}
	
	@Override
	public String getLastModifierUser(){
		return "modified_by";
    }
				
}
	