package other_office_departments_list;
import java.util.*; 
import util.*; 


public class OtherOfficeDepartmentDTO extends CommonDTO
{

	public long otherOfficeDepartmentsListId = -1;
	public long otherOfficeId = -1;
    public String nameEn = "";
    public String nameBn = "";
	public long modifiedBy = -1;
	public long insertedBy = -1;
	public long insertionDate = -1;
	
	public List<OtherOfficeDepartmentDTO> otherOfficeDepartmentDTOList = new ArrayList<>();
	
	
    @Override
	public String toString() {
            return "$OtherOfficeDepartmentDTO[" +
            " iD = " + iD +
            " otherOfficeDepartmentsListId = " + otherOfficeDepartmentsListId +
            " otherOfficeId = " + otherOfficeId +
            " nameEn = " + nameEn +
            " nameBn = " + nameBn +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            " searchColumn = " + searchColumn +
            " modifiedBy = " + modifiedBy +
            " insertedBy = " + insertedBy +
            " insertionDate = " + insertionDate +
            "]";
    }

}