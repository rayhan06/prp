package other_office_departments_list;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.sql.SQLException;
import common.CommonDAOService;
import common.ConnectionAndStatementUtil;

import org.apache.log4j.Logger;

import appointment.KeyCountDTO;
import util.*;
import pb.*;

public class OtherOfficeDepartmentDAO  implements CommonDAOService<OtherOfficeDepartmentDTO>
{
	
	Logger logger = Logger.getLogger(getClass());
	
	private static String addQuery = "";
	private static String updateQuery = "";
	private String[] columnNames = null;

	private OtherOfficeDepartmentDAO()
	{
		columnNames = new String[] 
		{
			"ID",
			"other_office_departments_list_id",
			"other_office_id",
			"name_en",
			"name_bn",			
			"search_column",
			"modified_by",
			"inserted_by",
			"insertion_date",
			"isDeleted",
			"lastModificationTime"
		};
		updateQuery = getUpdateQuery(columnNames);
        addQuery = getAddQuery(columnNames);
		
		searchMap.put("name_en"," and (name_en like ?)");
		searchMap.put("name_bn"," and (name_bn like ?)");
		searchMap.put("modified_by"," and (modified_by like ?)");
		searchMap.put("inserted_by"," and (inserted_by like ?)");
		searchMap.put("insertion_date_start"," and (insertion_date >= ?)");
		searchMap.put("insertion_date_end"," and (insertion_date <= ?)");
		searchMap.put("AnyField"," and (search_column like ?)");
	}

	private final Map<String,String> searchMap = new HashMap<>();

    private static class LazyLoader
    {
    	static final OtherOfficeDepartmentDAO INSTANCE = new OtherOfficeDepartmentDAO();
	}

	public static OtherOfficeDepartmentDAO getInstance()
	{
    	return LazyLoader.INSTANCE;
	}
	
	public void setSearchColumn(OtherOfficeDepartmentDTO otherofficedepartmentDTO)
	{
		otherofficedepartmentDTO.searchColumn = "";
		otherofficedepartmentDTO.searchColumn += otherofficedepartmentDTO.nameEn + " ";
		otherofficedepartmentDTO.searchColumn += otherofficedepartmentDTO.nameBn + " ";
		otherofficedepartmentDTO.searchColumn += otherofficedepartmentDTO.modifiedBy + " ";
		otherofficedepartmentDTO.searchColumn += otherofficedepartmentDTO.insertedBy + " ";
	}
	
	@Override
	public void set(PreparedStatement ps, OtherOfficeDepartmentDTO otherofficedepartmentDTO, boolean isInsert) throws SQLException
	{
		int index = 0;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(otherofficedepartmentDTO);
		if(isInsert)
		{
			ps.setObject(++index,otherofficedepartmentDTO.iD);
		}
		ps.setObject(++index,otherofficedepartmentDTO.otherOfficeDepartmentsListId);
		ps.setObject(++index,otherofficedepartmentDTO.otherOfficeId);
		ps.setObject(++index,otherofficedepartmentDTO.nameEn);
		ps.setObject(++index,otherofficedepartmentDTO.nameBn);
		ps.setObject(++index,otherofficedepartmentDTO.searchColumn);
		ps.setObject(++index,otherofficedepartmentDTO.modifiedBy);
		ps.setObject(++index,otherofficedepartmentDTO.insertedBy);
		ps.setObject(++index,otherofficedepartmentDTO.insertionDate);
		if(isInsert)
		{
			ps.setObject(++index,otherofficedepartmentDTO.isDeleted);
		}
		ps.setObject(++index, lastModificationTime);
		if(!isInsert)
		{
			ps.setObject(++index,otherofficedepartmentDTO.iD);
		}
	}
	
	@Override
	public OtherOfficeDepartmentDTO buildObjectFromResultSet(ResultSet rs)
	{
		try
		{
			OtherOfficeDepartmentDTO otherofficedepartmentDTO = new OtherOfficeDepartmentDTO();
			int i = 0;
			otherofficedepartmentDTO.iD = rs.getLong(columnNames[i++]);
			otherofficedepartmentDTO.otherOfficeDepartmentsListId = rs.getLong(columnNames[i++]);
			otherofficedepartmentDTO.otherOfficeId = rs.getLong(columnNames[i++]);
			otherofficedepartmentDTO.nameEn = rs.getString(columnNames[i++]);
			otherofficedepartmentDTO.nameBn = rs.getString(columnNames[i++]);
			
			otherofficedepartmentDTO.searchColumn = rs.getString(columnNames[i++]);
			otherofficedepartmentDTO.modifiedBy = rs.getLong(columnNames[i++]);
			otherofficedepartmentDTO.insertedBy = rs.getLong(columnNames[i++]);
			otherofficedepartmentDTO.insertionDate = rs.getLong(columnNames[i++]);
			otherofficedepartmentDTO.isDeleted = rs.getInt(columnNames[i++]);
			otherofficedepartmentDTO.lastModificationTime = rs.getLong(columnNames[i++]);
			return otherofficedepartmentDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
		
	public OtherOfficeDepartmentDTO getDTOByID (long id)
	{
		return getDTOFromID(id);
	}
	
	public List<OtherOfficeDepartmentDTO> getByOffice(long otherOfficeId)
    {
		String sql = "select * from " + getTableName() + " where isDeleted = 0 and other_office_type = " + otherOfficeId;

		return ConnectionAndStatementUtil.getListOfT(sql,this::buildObjectFromResultSet);	
    }

	@Override
	public String getTableName() {
		return "other_office_department";
	}

	@Override
	public Map<String, String> getSearchMap() {
		return searchMap;
	}

	public long add(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((OtherOfficeDepartmentDTO) commonDTO,addQuery,true);
	}

	public long update(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((OtherOfficeDepartmentDTO) commonDTO,updateQuery,false);
	}
	
	@Override
	public String getLastModifierUser(){
		return "modified_by";
    }
				
}
	