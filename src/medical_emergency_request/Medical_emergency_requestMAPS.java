package medical_emergency_request;
import java.util.*; 
import util.*;


public class Medical_emergency_requestMAPS extends CommonMaps
{	
	public Medical_emergency_requestMAPS(String tableName)
	{
		

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("employeeId".toLowerCase(), "employeeId".toLowerCase());
		java_DTO_map.put("employeeUserId".toLowerCase(), "employeeUserId".toLowerCase());
		java_DTO_map.put("patientName".toLowerCase(), "patientName".toLowerCase());
		java_DTO_map.put("phoneNumber".toLowerCase(), "phoneNumber".toLowerCase());
		java_DTO_map.put("dateOfBirth".toLowerCase(), "dateOfBirth".toLowerCase());
		java_DTO_map.put("genderCat".toLowerCase(), "genderCat".toLowerCase());
		java_DTO_map.put("whoIsThePatientCat".toLowerCase(), "whoIsThePatientCat".toLowerCase());
		java_DTO_map.put("serviceCat".toLowerCase(), "serviceCat".toLowerCase());
		java_DTO_map.put("fromAddress".toLowerCase(), "fromAddress".toLowerCase());
		java_DTO_map.put("toAddress".toLowerCase(), "toAddress".toLowerCase());
		java_DTO_map.put("serviceDate".toLowerCase(), "serviceDate".toLowerCase());
		java_DTO_map.put("fromTime".toLowerCase(), "fromTime".toLowerCase());
		java_DTO_map.put("toTime".toLowerCase(), "toTime".toLowerCase());
		java_DTO_map.put("servicePersonCat".toLowerCase(), "servicePersonCat".toLowerCase());
		java_DTO_map.put("servicePersonOrganogramId".toLowerCase(), "servicePersonOrganogramId".toLowerCase());
		java_DTO_map.put("servicePersonUserId".toLowerCase(), "servicePersonUserId".toLowerCase());
		java_DTO_map.put("instructions".toLowerCase(), "instructions".toLowerCase());
		java_DTO_map.put("insertedByUserId".toLowerCase(), "insertedByUserId".toLowerCase());
		java_DTO_map.put("insertedByOrganogramId".toLowerCase(), "insertedByOrganogramId".toLowerCase());
		java_DTO_map.put("insertionDate".toLowerCase(), "insertionDate".toLowerCase());
		java_DTO_map.put("lastModifierUser".toLowerCase(), "lastModifierUser".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
		java_DTO_map.put("nurseUserId".toLowerCase(), "nurseUserId".toLowerCase());
		java_DTO_map.put("nurseOrganogramId".toLowerCase(), "nurseOrganogramId".toLowerCase());

		java_SQL_map.put("employee_id".toLowerCase(), "employeeId".toLowerCase());
		java_SQL_map.put("employee_user_id".toLowerCase(), "employeeUserId".toLowerCase());
		java_SQL_map.put("patient_name".toLowerCase(), "patientName".toLowerCase());
		java_SQL_map.put("phone_number".toLowerCase(), "phoneNumber".toLowerCase());
		java_SQL_map.put("date_of_birth".toLowerCase(), "dateOfBirth".toLowerCase());
		java_SQL_map.put("gender_cat".toLowerCase(), "genderCat".toLowerCase());
		java_SQL_map.put("who_is_the_patient_cat".toLowerCase(), "whoIsThePatientCat".toLowerCase());
		java_SQL_map.put("service_cat".toLowerCase(), "serviceCat".toLowerCase());
		java_SQL_map.put("from_address".toLowerCase(), "fromAddress".toLowerCase());
		java_SQL_map.put("to_address".toLowerCase(), "toAddress".toLowerCase());
		java_SQL_map.put("service_date".toLowerCase(), "serviceDate".toLowerCase());
		java_SQL_map.put("from_time".toLowerCase(), "fromTime".toLowerCase());
		java_SQL_map.put("to_time".toLowerCase(), "toTime".toLowerCase());
		java_SQL_map.put("service_person_cat".toLowerCase(), "servicePersonCat".toLowerCase());
		java_SQL_map.put("service_person_organogram_id".toLowerCase(), "servicePersonOrganogramId".toLowerCase());
		java_SQL_map.put("service_person_user_id".toLowerCase(), "servicePersonUserId".toLowerCase());
		java_SQL_map.put("instructions".toLowerCase(), "instructions".toLowerCase());
		java_SQL_map.put("nurse_user_id".toLowerCase(), "nurseUserId".toLowerCase());
		java_SQL_map.put("nurse_organogram_id".toLowerCase(), "nurseOrganogramId".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Employee Id".toLowerCase(), "employeeId".toLowerCase());
		java_Text_map.put("Employee User Id".toLowerCase(), "employeeUserId".toLowerCase());
		java_Text_map.put("Patient Name".toLowerCase(), "patientName".toLowerCase());
		java_Text_map.put("Phone Number".toLowerCase(), "phoneNumber".toLowerCase());
		java_Text_map.put("Date Of Birth".toLowerCase(), "dateOfBirth".toLowerCase());
		java_Text_map.put("Gender".toLowerCase(), "genderCat".toLowerCase());
		java_Text_map.put("Who Is The Patient".toLowerCase(), "whoIsThePatientCat".toLowerCase());
		java_Text_map.put("Service".toLowerCase(), "serviceCat".toLowerCase());
		java_Text_map.put("From Address".toLowerCase(), "fromAddress".toLowerCase());
		java_Text_map.put("To Address".toLowerCase(), "toAddress".toLowerCase());
		java_Text_map.put("Service Date".toLowerCase(), "serviceDate".toLowerCase());
		java_Text_map.put("From Time".toLowerCase(), "fromTime".toLowerCase());
		java_Text_map.put("To Time".toLowerCase(), "toTime".toLowerCase());
		java_Text_map.put("Service Person".toLowerCase(), "servicePersonCat".toLowerCase());
		java_Text_map.put("Service Person Organogram Id".toLowerCase(), "servicePersonOrganogramId".toLowerCase());
		java_Text_map.put("Service Person User Id".toLowerCase(), "servicePersonUserId".toLowerCase());
		java_Text_map.put("Instructions".toLowerCase(), "instructions".toLowerCase());
		java_Text_map.put("Inserted By User Id".toLowerCase(), "insertedByUserId".toLowerCase());
		java_Text_map.put("Inserted By Organogram Id".toLowerCase(), "insertedByOrganogramId".toLowerCase());
		java_Text_map.put("Insertion Date".toLowerCase(), "insertionDate".toLowerCase());
		java_Text_map.put("Last Modifier User".toLowerCase(), "lastModifierUser".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
		java_Text_map.put("Nurse User Id".toLowerCase(), "nurseUserId".toLowerCase());
		java_Text_map.put("Nurse Organogram Id".toLowerCase(), "nurseOrganogramId".toLowerCase());
			
	}

}