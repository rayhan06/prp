package medical_emergency_request;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class Medical_emergency_requestRepository implements Repository {
	Medical_emergency_requestDAO medical_emergency_requestDAO = null;
	
	public void setDAO(Medical_emergency_requestDAO medical_emergency_requestDAO)
	{
		this.medical_emergency_requestDAO = medical_emergency_requestDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Medical_emergency_requestRepository.class);
	Map<Long, Medical_emergency_requestDTO>mapOfMedical_emergency_requestDTOToiD;
	Map<Long, Set<Medical_emergency_requestDTO> >mapOfMedical_emergency_requestDTOToemployeeId;
	Map<Long, Set<Medical_emergency_requestDTO> >mapOfMedical_emergency_requestDTOToemployeeUserId;
	Map<String, Set<Medical_emergency_requestDTO> >mapOfMedical_emergency_requestDTOTopatientName;
	Map<String, Set<Medical_emergency_requestDTO> >mapOfMedical_emergency_requestDTOTophoneNumber;
	Map<Long, Set<Medical_emergency_requestDTO> >mapOfMedical_emergency_requestDTOTodateOfBirth;
	Map<Integer, Set<Medical_emergency_requestDTO> >mapOfMedical_emergency_requestDTOTogenderCat;
	Map<Integer, Set<Medical_emergency_requestDTO> >mapOfMedical_emergency_requestDTOTowhoIsThePatientCat;
	Map<Integer, Set<Medical_emergency_requestDTO> >mapOfMedical_emergency_requestDTOToserviceCat;
	Map<String, Set<Medical_emergency_requestDTO> >mapOfMedical_emergency_requestDTOTofromAddress;
	Map<String, Set<Medical_emergency_requestDTO> >mapOfMedical_emergency_requestDTOTotoAddress;
	Map<Long, Set<Medical_emergency_requestDTO> >mapOfMedical_emergency_requestDTOToserviceDate;
	Map<String, Set<Medical_emergency_requestDTO> >mapOfMedical_emergency_requestDTOTofromTime;
	Map<String, Set<Medical_emergency_requestDTO> >mapOfMedical_emergency_requestDTOTotoTime;
	Map<Integer, Set<Medical_emergency_requestDTO> >mapOfMedical_emergency_requestDTOToservicePersonCat;
	Map<Long, Set<Medical_emergency_requestDTO> >mapOfMedical_emergency_requestDTOToservicePersonOrganogramId;
	Map<Long, Set<Medical_emergency_requestDTO> >mapOfMedical_emergency_requestDTOToservicePersonUserId;
	Map<String, Set<Medical_emergency_requestDTO> >mapOfMedical_emergency_requestDTOToinstructions;
	Map<Long, Set<Medical_emergency_requestDTO> >mapOfMedical_emergency_requestDTOToinsertedByUserId;
	Map<Long, Set<Medical_emergency_requestDTO> >mapOfMedical_emergency_requestDTOToinsertedByOrganogramId;
	Map<Long, Set<Medical_emergency_requestDTO> >mapOfMedical_emergency_requestDTOToinsertionDate;
	Map<String, Set<Medical_emergency_requestDTO> >mapOfMedical_emergency_requestDTOTolastModifierUser;
	Map<Long, Set<Medical_emergency_requestDTO> >mapOfMedical_emergency_requestDTOTolastModificationTime;
	Map<Long, Set<Medical_emergency_requestDTO> >mapOfMedical_emergency_requestDTOTonurseUserId;
	Map<Long, Set<Medical_emergency_requestDTO> >mapOfMedical_emergency_requestDTOTonurseOrganogramId;


	static Medical_emergency_requestRepository instance = null;  
	private Medical_emergency_requestRepository(){
		mapOfMedical_emergency_requestDTOToiD = new ConcurrentHashMap<>();
		mapOfMedical_emergency_requestDTOToemployeeId = new ConcurrentHashMap<>();
		mapOfMedical_emergency_requestDTOToemployeeUserId = new ConcurrentHashMap<>();
		mapOfMedical_emergency_requestDTOTopatientName = new ConcurrentHashMap<>();
		mapOfMedical_emergency_requestDTOTophoneNumber = new ConcurrentHashMap<>();
		mapOfMedical_emergency_requestDTOTodateOfBirth = new ConcurrentHashMap<>();
		mapOfMedical_emergency_requestDTOTogenderCat = new ConcurrentHashMap<>();
		mapOfMedical_emergency_requestDTOTowhoIsThePatientCat = new ConcurrentHashMap<>();
		mapOfMedical_emergency_requestDTOToserviceCat = new ConcurrentHashMap<>();
		mapOfMedical_emergency_requestDTOTofromAddress = new ConcurrentHashMap<>();
		mapOfMedical_emergency_requestDTOTotoAddress = new ConcurrentHashMap<>();
		mapOfMedical_emergency_requestDTOToserviceDate = new ConcurrentHashMap<>();
		mapOfMedical_emergency_requestDTOTofromTime = new ConcurrentHashMap<>();
		mapOfMedical_emergency_requestDTOTotoTime = new ConcurrentHashMap<>();
		mapOfMedical_emergency_requestDTOToservicePersonCat = new ConcurrentHashMap<>();
		mapOfMedical_emergency_requestDTOToservicePersonOrganogramId = new ConcurrentHashMap<>();
		mapOfMedical_emergency_requestDTOToservicePersonUserId = new ConcurrentHashMap<>();
		mapOfMedical_emergency_requestDTOToinstructions = new ConcurrentHashMap<>();
		mapOfMedical_emergency_requestDTOToinsertedByUserId = new ConcurrentHashMap<>();
		mapOfMedical_emergency_requestDTOToinsertedByOrganogramId = new ConcurrentHashMap<>();
		mapOfMedical_emergency_requestDTOToinsertionDate = new ConcurrentHashMap<>();
		mapOfMedical_emergency_requestDTOTolastModifierUser = new ConcurrentHashMap<>();
		mapOfMedical_emergency_requestDTOTolastModificationTime = new ConcurrentHashMap<>();
		mapOfMedical_emergency_requestDTOTonurseUserId = new ConcurrentHashMap<>();
		mapOfMedical_emergency_requestDTOTonurseOrganogramId = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Medical_emergency_requestRepository getInstance(){
		if (instance == null){
			instance = new Medical_emergency_requestRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(medical_emergency_requestDAO == null)
		{
			return;
		}
		try {
			List<Medical_emergency_requestDTO> medical_emergency_requestDTOs = medical_emergency_requestDAO.getAllMedical_emergency_request(reloadAll);
			for(Medical_emergency_requestDTO medical_emergency_requestDTO : medical_emergency_requestDTOs) {
				Medical_emergency_requestDTO oldMedical_emergency_requestDTO = getMedical_emergency_requestDTOByID(medical_emergency_requestDTO.iD);
				if( oldMedical_emergency_requestDTO != null ) {
					mapOfMedical_emergency_requestDTOToiD.remove(oldMedical_emergency_requestDTO.iD);
				
					if(mapOfMedical_emergency_requestDTOToemployeeId.containsKey(oldMedical_emergency_requestDTO.employeeId)) {
						mapOfMedical_emergency_requestDTOToemployeeId.get(oldMedical_emergency_requestDTO.employeeId).remove(oldMedical_emergency_requestDTO);
					}
					if(mapOfMedical_emergency_requestDTOToemployeeId.get(oldMedical_emergency_requestDTO.employeeId).isEmpty()) {
						mapOfMedical_emergency_requestDTOToemployeeId.remove(oldMedical_emergency_requestDTO.employeeId);
					}
					
					if(mapOfMedical_emergency_requestDTOToemployeeUserId.containsKey(oldMedical_emergency_requestDTO.employeeUserId)) {
						mapOfMedical_emergency_requestDTOToemployeeUserId.get(oldMedical_emergency_requestDTO.employeeUserId).remove(oldMedical_emergency_requestDTO);
					}
					if(mapOfMedical_emergency_requestDTOToemployeeUserId.get(oldMedical_emergency_requestDTO.employeeUserId).isEmpty()) {
						mapOfMedical_emergency_requestDTOToemployeeUserId.remove(oldMedical_emergency_requestDTO.employeeUserId);
					}
					
					if(mapOfMedical_emergency_requestDTOTopatientName.containsKey(oldMedical_emergency_requestDTO.patientName)) {
						mapOfMedical_emergency_requestDTOTopatientName.get(oldMedical_emergency_requestDTO.patientName).remove(oldMedical_emergency_requestDTO);
					}
					if(mapOfMedical_emergency_requestDTOTopatientName.get(oldMedical_emergency_requestDTO.patientName).isEmpty()) {
						mapOfMedical_emergency_requestDTOTopatientName.remove(oldMedical_emergency_requestDTO.patientName);
					}
					
					if(mapOfMedical_emergency_requestDTOTophoneNumber.containsKey(oldMedical_emergency_requestDTO.phoneNumber)) {
						mapOfMedical_emergency_requestDTOTophoneNumber.get(oldMedical_emergency_requestDTO.phoneNumber).remove(oldMedical_emergency_requestDTO);
					}
					if(mapOfMedical_emergency_requestDTOTophoneNumber.get(oldMedical_emergency_requestDTO.phoneNumber).isEmpty()) {
						mapOfMedical_emergency_requestDTOTophoneNumber.remove(oldMedical_emergency_requestDTO.phoneNumber);
					}
					
					if(mapOfMedical_emergency_requestDTOTodateOfBirth.containsKey(oldMedical_emergency_requestDTO.dateOfBirth)) {
						mapOfMedical_emergency_requestDTOTodateOfBirth.get(oldMedical_emergency_requestDTO.dateOfBirth).remove(oldMedical_emergency_requestDTO);
					}
					if(mapOfMedical_emergency_requestDTOTodateOfBirth.get(oldMedical_emergency_requestDTO.dateOfBirth).isEmpty()) {
						mapOfMedical_emergency_requestDTOTodateOfBirth.remove(oldMedical_emergency_requestDTO.dateOfBirth);
					}
					
					if(mapOfMedical_emergency_requestDTOTogenderCat.containsKey(oldMedical_emergency_requestDTO.genderCat)) {
						mapOfMedical_emergency_requestDTOTogenderCat.get(oldMedical_emergency_requestDTO.genderCat).remove(oldMedical_emergency_requestDTO);
					}
					if(mapOfMedical_emergency_requestDTOTogenderCat.get(oldMedical_emergency_requestDTO.genderCat).isEmpty()) {
						mapOfMedical_emergency_requestDTOTogenderCat.remove(oldMedical_emergency_requestDTO.genderCat);
					}
					
					if(mapOfMedical_emergency_requestDTOTowhoIsThePatientCat.containsKey(oldMedical_emergency_requestDTO.whoIsThePatientCat)) {
						mapOfMedical_emergency_requestDTOTowhoIsThePatientCat.get(oldMedical_emergency_requestDTO.whoIsThePatientCat).remove(oldMedical_emergency_requestDTO);
					}
					if(mapOfMedical_emergency_requestDTOTowhoIsThePatientCat.get(oldMedical_emergency_requestDTO.whoIsThePatientCat).isEmpty()) {
						mapOfMedical_emergency_requestDTOTowhoIsThePatientCat.remove(oldMedical_emergency_requestDTO.whoIsThePatientCat);
					}
					
					if(mapOfMedical_emergency_requestDTOToserviceCat.containsKey(oldMedical_emergency_requestDTO.serviceCat)) {
						mapOfMedical_emergency_requestDTOToserviceCat.get(oldMedical_emergency_requestDTO.serviceCat).remove(oldMedical_emergency_requestDTO);
					}
					if(mapOfMedical_emergency_requestDTOToserviceCat.get(oldMedical_emergency_requestDTO.serviceCat).isEmpty()) {
						mapOfMedical_emergency_requestDTOToserviceCat.remove(oldMedical_emergency_requestDTO.serviceCat);
					}
					
					if(mapOfMedical_emergency_requestDTOTofromAddress.containsKey(oldMedical_emergency_requestDTO.fromAddress)) {
						mapOfMedical_emergency_requestDTOTofromAddress.get(oldMedical_emergency_requestDTO.fromAddress).remove(oldMedical_emergency_requestDTO);
					}
					if(mapOfMedical_emergency_requestDTOTofromAddress.get(oldMedical_emergency_requestDTO.fromAddress).isEmpty()) {
						mapOfMedical_emergency_requestDTOTofromAddress.remove(oldMedical_emergency_requestDTO.fromAddress);
					}
					
					if(mapOfMedical_emergency_requestDTOTotoAddress.containsKey(oldMedical_emergency_requestDTO.toAddress)) {
						mapOfMedical_emergency_requestDTOTotoAddress.get(oldMedical_emergency_requestDTO.toAddress).remove(oldMedical_emergency_requestDTO);
					}
					if(mapOfMedical_emergency_requestDTOTotoAddress.get(oldMedical_emergency_requestDTO.toAddress).isEmpty()) {
						mapOfMedical_emergency_requestDTOTotoAddress.remove(oldMedical_emergency_requestDTO.toAddress);
					}
					
					if(mapOfMedical_emergency_requestDTOToserviceDate.containsKey(oldMedical_emergency_requestDTO.serviceDate)) {
						mapOfMedical_emergency_requestDTOToserviceDate.get(oldMedical_emergency_requestDTO.serviceDate).remove(oldMedical_emergency_requestDTO);
					}
					if(mapOfMedical_emergency_requestDTOToserviceDate.get(oldMedical_emergency_requestDTO.serviceDate).isEmpty()) {
						mapOfMedical_emergency_requestDTOToserviceDate.remove(oldMedical_emergency_requestDTO.serviceDate);
					}
					
					if(mapOfMedical_emergency_requestDTOTofromTime.containsKey(oldMedical_emergency_requestDTO.fromTime)) {
						mapOfMedical_emergency_requestDTOTofromTime.get(oldMedical_emergency_requestDTO.fromTime).remove(oldMedical_emergency_requestDTO);
					}
					if(mapOfMedical_emergency_requestDTOTofromTime.get(oldMedical_emergency_requestDTO.fromTime).isEmpty()) {
						mapOfMedical_emergency_requestDTOTofromTime.remove(oldMedical_emergency_requestDTO.fromTime);
					}
					
					if(mapOfMedical_emergency_requestDTOTotoTime.containsKey(oldMedical_emergency_requestDTO.toTime)) {
						mapOfMedical_emergency_requestDTOTotoTime.get(oldMedical_emergency_requestDTO.toTime).remove(oldMedical_emergency_requestDTO);
					}
					if(mapOfMedical_emergency_requestDTOTotoTime.get(oldMedical_emergency_requestDTO.toTime).isEmpty()) {
						mapOfMedical_emergency_requestDTOTotoTime.remove(oldMedical_emergency_requestDTO.toTime);
					}
					
					if(mapOfMedical_emergency_requestDTOToservicePersonCat.containsKey(oldMedical_emergency_requestDTO.servicePersonCat)) {
						mapOfMedical_emergency_requestDTOToservicePersonCat.get(oldMedical_emergency_requestDTO.servicePersonCat).remove(oldMedical_emergency_requestDTO);
					}
					if(mapOfMedical_emergency_requestDTOToservicePersonCat.get(oldMedical_emergency_requestDTO.servicePersonCat).isEmpty()) {
						mapOfMedical_emergency_requestDTOToservicePersonCat.remove(oldMedical_emergency_requestDTO.servicePersonCat);
					}
					
					if(mapOfMedical_emergency_requestDTOToservicePersonOrganogramId.containsKey(oldMedical_emergency_requestDTO.servicePersonOrganogramId)) {
						mapOfMedical_emergency_requestDTOToservicePersonOrganogramId.get(oldMedical_emergency_requestDTO.servicePersonOrganogramId).remove(oldMedical_emergency_requestDTO);
					}
					if(mapOfMedical_emergency_requestDTOToservicePersonOrganogramId.get(oldMedical_emergency_requestDTO.servicePersonOrganogramId).isEmpty()) {
						mapOfMedical_emergency_requestDTOToservicePersonOrganogramId.remove(oldMedical_emergency_requestDTO.servicePersonOrganogramId);
					}
					
					if(mapOfMedical_emergency_requestDTOToservicePersonUserId.containsKey(oldMedical_emergency_requestDTO.servicePersonUserId)) {
						mapOfMedical_emergency_requestDTOToservicePersonUserId.get(oldMedical_emergency_requestDTO.servicePersonUserId).remove(oldMedical_emergency_requestDTO);
					}
					if(mapOfMedical_emergency_requestDTOToservicePersonUserId.get(oldMedical_emergency_requestDTO.servicePersonUserId).isEmpty()) {
						mapOfMedical_emergency_requestDTOToservicePersonUserId.remove(oldMedical_emergency_requestDTO.servicePersonUserId);
					}
					
					if(mapOfMedical_emergency_requestDTOToinstructions.containsKey(oldMedical_emergency_requestDTO.instructions)) {
						mapOfMedical_emergency_requestDTOToinstructions.get(oldMedical_emergency_requestDTO.instructions).remove(oldMedical_emergency_requestDTO);
					}
					if(mapOfMedical_emergency_requestDTOToinstructions.get(oldMedical_emergency_requestDTO.instructions).isEmpty()) {
						mapOfMedical_emergency_requestDTOToinstructions.remove(oldMedical_emergency_requestDTO.instructions);
					}
					
					if(mapOfMedical_emergency_requestDTOToinsertedByUserId.containsKey(oldMedical_emergency_requestDTO.insertedByUserId)) {
						mapOfMedical_emergency_requestDTOToinsertedByUserId.get(oldMedical_emergency_requestDTO.insertedByUserId).remove(oldMedical_emergency_requestDTO);
					}
					if(mapOfMedical_emergency_requestDTOToinsertedByUserId.get(oldMedical_emergency_requestDTO.insertedByUserId).isEmpty()) {
						mapOfMedical_emergency_requestDTOToinsertedByUserId.remove(oldMedical_emergency_requestDTO.insertedByUserId);
					}
					
					if(mapOfMedical_emergency_requestDTOToinsertedByOrganogramId.containsKey(oldMedical_emergency_requestDTO.insertedByOrganogramId)) {
						mapOfMedical_emergency_requestDTOToinsertedByOrganogramId.get(oldMedical_emergency_requestDTO.insertedByOrganogramId).remove(oldMedical_emergency_requestDTO);
					}
					if(mapOfMedical_emergency_requestDTOToinsertedByOrganogramId.get(oldMedical_emergency_requestDTO.insertedByOrganogramId).isEmpty()) {
						mapOfMedical_emergency_requestDTOToinsertedByOrganogramId.remove(oldMedical_emergency_requestDTO.insertedByOrganogramId);
					}
					
					if(mapOfMedical_emergency_requestDTOToinsertionDate.containsKey(oldMedical_emergency_requestDTO.insertionDate)) {
						mapOfMedical_emergency_requestDTOToinsertionDate.get(oldMedical_emergency_requestDTO.insertionDate).remove(oldMedical_emergency_requestDTO);
					}
					if(mapOfMedical_emergency_requestDTOToinsertionDate.get(oldMedical_emergency_requestDTO.insertionDate).isEmpty()) {
						mapOfMedical_emergency_requestDTOToinsertionDate.remove(oldMedical_emergency_requestDTO.insertionDate);
					}
					
					if(mapOfMedical_emergency_requestDTOTolastModifierUser.containsKey(oldMedical_emergency_requestDTO.lastModifierUser)) {
						mapOfMedical_emergency_requestDTOTolastModifierUser.get(oldMedical_emergency_requestDTO.lastModifierUser).remove(oldMedical_emergency_requestDTO);
					}
					if(mapOfMedical_emergency_requestDTOTolastModifierUser.get(oldMedical_emergency_requestDTO.lastModifierUser).isEmpty()) {
						mapOfMedical_emergency_requestDTOTolastModifierUser.remove(oldMedical_emergency_requestDTO.lastModifierUser);
					}
					
					if(mapOfMedical_emergency_requestDTOTolastModificationTime.containsKey(oldMedical_emergency_requestDTO.lastModificationTime)) {
						mapOfMedical_emergency_requestDTOTolastModificationTime.get(oldMedical_emergency_requestDTO.lastModificationTime).remove(oldMedical_emergency_requestDTO);
					}
					if(mapOfMedical_emergency_requestDTOTolastModificationTime.get(oldMedical_emergency_requestDTO.lastModificationTime).isEmpty()) {
						mapOfMedical_emergency_requestDTOTolastModificationTime.remove(oldMedical_emergency_requestDTO.lastModificationTime);
					}
					
					if(mapOfMedical_emergency_requestDTOTonurseUserId.containsKey(oldMedical_emergency_requestDTO.nurseUserId)) {
						mapOfMedical_emergency_requestDTOTonurseUserId.get(oldMedical_emergency_requestDTO.nurseUserId).remove(oldMedical_emergency_requestDTO);
					}
					if(mapOfMedical_emergency_requestDTOTonurseUserId.get(oldMedical_emergency_requestDTO.nurseUserId).isEmpty()) {
						mapOfMedical_emergency_requestDTOTonurseUserId.remove(oldMedical_emergency_requestDTO.nurseUserId);
					}
					
					if(mapOfMedical_emergency_requestDTOTonurseOrganogramId.containsKey(oldMedical_emergency_requestDTO.nurseOrganogramId)) {
						mapOfMedical_emergency_requestDTOTonurseOrganogramId.get(oldMedical_emergency_requestDTO.nurseOrganogramId).remove(oldMedical_emergency_requestDTO);
					}
					if(mapOfMedical_emergency_requestDTOTonurseOrganogramId.get(oldMedical_emergency_requestDTO.nurseOrganogramId).isEmpty()) {
						mapOfMedical_emergency_requestDTOTonurseOrganogramId.remove(oldMedical_emergency_requestDTO.nurseOrganogramId);
					}
					
					
				}
				if(medical_emergency_requestDTO.isDeleted == 0) 
				{
					
					mapOfMedical_emergency_requestDTOToiD.put(medical_emergency_requestDTO.iD, medical_emergency_requestDTO);
				
					if( ! mapOfMedical_emergency_requestDTOToemployeeId.containsKey(medical_emergency_requestDTO.employeeId)) {
						mapOfMedical_emergency_requestDTOToemployeeId.put(medical_emergency_requestDTO.employeeId, new HashSet<>());
					}
					mapOfMedical_emergency_requestDTOToemployeeId.get(medical_emergency_requestDTO.employeeId).add(medical_emergency_requestDTO);
					
					if( ! mapOfMedical_emergency_requestDTOToemployeeUserId.containsKey(medical_emergency_requestDTO.employeeUserId)) {
						mapOfMedical_emergency_requestDTOToemployeeUserId.put(medical_emergency_requestDTO.employeeUserId, new HashSet<>());
					}
					mapOfMedical_emergency_requestDTOToemployeeUserId.get(medical_emergency_requestDTO.employeeUserId).add(medical_emergency_requestDTO);
					
					if( ! mapOfMedical_emergency_requestDTOTopatientName.containsKey(medical_emergency_requestDTO.patientName)) {
						mapOfMedical_emergency_requestDTOTopatientName.put(medical_emergency_requestDTO.patientName, new HashSet<>());
					}
					mapOfMedical_emergency_requestDTOTopatientName.get(medical_emergency_requestDTO.patientName).add(medical_emergency_requestDTO);
					
					if( ! mapOfMedical_emergency_requestDTOTophoneNumber.containsKey(medical_emergency_requestDTO.phoneNumber)) {
						mapOfMedical_emergency_requestDTOTophoneNumber.put(medical_emergency_requestDTO.phoneNumber, new HashSet<>());
					}
					mapOfMedical_emergency_requestDTOTophoneNumber.get(medical_emergency_requestDTO.phoneNumber).add(medical_emergency_requestDTO);
					
					if( ! mapOfMedical_emergency_requestDTOTodateOfBirth.containsKey(medical_emergency_requestDTO.dateOfBirth)) {
						mapOfMedical_emergency_requestDTOTodateOfBirth.put(medical_emergency_requestDTO.dateOfBirth, new HashSet<>());
					}
					mapOfMedical_emergency_requestDTOTodateOfBirth.get(medical_emergency_requestDTO.dateOfBirth).add(medical_emergency_requestDTO);
					
					if( ! mapOfMedical_emergency_requestDTOTogenderCat.containsKey(medical_emergency_requestDTO.genderCat)) {
						mapOfMedical_emergency_requestDTOTogenderCat.put(medical_emergency_requestDTO.genderCat, new HashSet<>());
					}
					mapOfMedical_emergency_requestDTOTogenderCat.get(medical_emergency_requestDTO.genderCat).add(medical_emergency_requestDTO);
					
					if( ! mapOfMedical_emergency_requestDTOTowhoIsThePatientCat.containsKey(medical_emergency_requestDTO.whoIsThePatientCat)) {
						mapOfMedical_emergency_requestDTOTowhoIsThePatientCat.put(medical_emergency_requestDTO.whoIsThePatientCat, new HashSet<>());
					}
					mapOfMedical_emergency_requestDTOTowhoIsThePatientCat.get(medical_emergency_requestDTO.whoIsThePatientCat).add(medical_emergency_requestDTO);
					
					if( ! mapOfMedical_emergency_requestDTOToserviceCat.containsKey(medical_emergency_requestDTO.serviceCat)) {
						mapOfMedical_emergency_requestDTOToserviceCat.put(medical_emergency_requestDTO.serviceCat, new HashSet<>());
					}
					mapOfMedical_emergency_requestDTOToserviceCat.get(medical_emergency_requestDTO.serviceCat).add(medical_emergency_requestDTO);
					
					if( ! mapOfMedical_emergency_requestDTOTofromAddress.containsKey(medical_emergency_requestDTO.fromAddress)) {
						mapOfMedical_emergency_requestDTOTofromAddress.put(medical_emergency_requestDTO.fromAddress, new HashSet<>());
					}
					mapOfMedical_emergency_requestDTOTofromAddress.get(medical_emergency_requestDTO.fromAddress).add(medical_emergency_requestDTO);
					
					if( ! mapOfMedical_emergency_requestDTOTotoAddress.containsKey(medical_emergency_requestDTO.toAddress)) {
						mapOfMedical_emergency_requestDTOTotoAddress.put(medical_emergency_requestDTO.toAddress, new HashSet<>());
					}
					mapOfMedical_emergency_requestDTOTotoAddress.get(medical_emergency_requestDTO.toAddress).add(medical_emergency_requestDTO);
					
					if( ! mapOfMedical_emergency_requestDTOToserviceDate.containsKey(medical_emergency_requestDTO.serviceDate)) {
						mapOfMedical_emergency_requestDTOToserviceDate.put(medical_emergency_requestDTO.serviceDate, new HashSet<>());
					}
					mapOfMedical_emergency_requestDTOToserviceDate.get(medical_emergency_requestDTO.serviceDate).add(medical_emergency_requestDTO);
					
					if( ! mapOfMedical_emergency_requestDTOTofromTime.containsKey(medical_emergency_requestDTO.fromTime)) {
						mapOfMedical_emergency_requestDTOTofromTime.put(medical_emergency_requestDTO.fromTime, new HashSet<>());
					}
					mapOfMedical_emergency_requestDTOTofromTime.get(medical_emergency_requestDTO.fromTime).add(medical_emergency_requestDTO);
					
					if( ! mapOfMedical_emergency_requestDTOTotoTime.containsKey(medical_emergency_requestDTO.toTime)) {
						mapOfMedical_emergency_requestDTOTotoTime.put(medical_emergency_requestDTO.toTime, new HashSet<>());
					}
					mapOfMedical_emergency_requestDTOTotoTime.get(medical_emergency_requestDTO.toTime).add(medical_emergency_requestDTO);
					
					if( ! mapOfMedical_emergency_requestDTOToservicePersonCat.containsKey(medical_emergency_requestDTO.servicePersonCat)) {
						mapOfMedical_emergency_requestDTOToservicePersonCat.put(medical_emergency_requestDTO.servicePersonCat, new HashSet<>());
					}
					mapOfMedical_emergency_requestDTOToservicePersonCat.get(medical_emergency_requestDTO.servicePersonCat).add(medical_emergency_requestDTO);
					
					if( ! mapOfMedical_emergency_requestDTOToservicePersonOrganogramId.containsKey(medical_emergency_requestDTO.servicePersonOrganogramId)) {
						mapOfMedical_emergency_requestDTOToservicePersonOrganogramId.put(medical_emergency_requestDTO.servicePersonOrganogramId, new HashSet<>());
					}
					mapOfMedical_emergency_requestDTOToservicePersonOrganogramId.get(medical_emergency_requestDTO.servicePersonOrganogramId).add(medical_emergency_requestDTO);
					
					if( ! mapOfMedical_emergency_requestDTOToservicePersonUserId.containsKey(medical_emergency_requestDTO.servicePersonUserId)) {
						mapOfMedical_emergency_requestDTOToservicePersonUserId.put(medical_emergency_requestDTO.servicePersonUserId, new HashSet<>());
					}
					mapOfMedical_emergency_requestDTOToservicePersonUserId.get(medical_emergency_requestDTO.servicePersonUserId).add(medical_emergency_requestDTO);
					
					if( ! mapOfMedical_emergency_requestDTOToinstructions.containsKey(medical_emergency_requestDTO.instructions)) {
						mapOfMedical_emergency_requestDTOToinstructions.put(medical_emergency_requestDTO.instructions, new HashSet<>());
					}
					mapOfMedical_emergency_requestDTOToinstructions.get(medical_emergency_requestDTO.instructions).add(medical_emergency_requestDTO);
					
					if( ! mapOfMedical_emergency_requestDTOToinsertedByUserId.containsKey(medical_emergency_requestDTO.insertedByUserId)) {
						mapOfMedical_emergency_requestDTOToinsertedByUserId.put(medical_emergency_requestDTO.insertedByUserId, new HashSet<>());
					}
					mapOfMedical_emergency_requestDTOToinsertedByUserId.get(medical_emergency_requestDTO.insertedByUserId).add(medical_emergency_requestDTO);
					
					if( ! mapOfMedical_emergency_requestDTOToinsertedByOrganogramId.containsKey(medical_emergency_requestDTO.insertedByOrganogramId)) {
						mapOfMedical_emergency_requestDTOToinsertedByOrganogramId.put(medical_emergency_requestDTO.insertedByOrganogramId, new HashSet<>());
					}
					mapOfMedical_emergency_requestDTOToinsertedByOrganogramId.get(medical_emergency_requestDTO.insertedByOrganogramId).add(medical_emergency_requestDTO);
					
					if( ! mapOfMedical_emergency_requestDTOToinsertionDate.containsKey(medical_emergency_requestDTO.insertionDate)) {
						mapOfMedical_emergency_requestDTOToinsertionDate.put(medical_emergency_requestDTO.insertionDate, new HashSet<>());
					}
					mapOfMedical_emergency_requestDTOToinsertionDate.get(medical_emergency_requestDTO.insertionDate).add(medical_emergency_requestDTO);
					
					if( ! mapOfMedical_emergency_requestDTOTolastModifierUser.containsKey(medical_emergency_requestDTO.lastModifierUser)) {
						mapOfMedical_emergency_requestDTOTolastModifierUser.put(medical_emergency_requestDTO.lastModifierUser, new HashSet<>());
					}
					mapOfMedical_emergency_requestDTOTolastModifierUser.get(medical_emergency_requestDTO.lastModifierUser).add(medical_emergency_requestDTO);
					
					if( ! mapOfMedical_emergency_requestDTOTolastModificationTime.containsKey(medical_emergency_requestDTO.lastModificationTime)) {
						mapOfMedical_emergency_requestDTOTolastModificationTime.put(medical_emergency_requestDTO.lastModificationTime, new HashSet<>());
					}
					mapOfMedical_emergency_requestDTOTolastModificationTime.get(medical_emergency_requestDTO.lastModificationTime).add(medical_emergency_requestDTO);
					
					if( ! mapOfMedical_emergency_requestDTOTonurseUserId.containsKey(medical_emergency_requestDTO.nurseUserId)) {
						mapOfMedical_emergency_requestDTOTonurseUserId.put(medical_emergency_requestDTO.nurseUserId, new HashSet<>());
					}
					mapOfMedical_emergency_requestDTOTonurseUserId.get(medical_emergency_requestDTO.nurseUserId).add(medical_emergency_requestDTO);
					
					if( ! mapOfMedical_emergency_requestDTOTonurseOrganogramId.containsKey(medical_emergency_requestDTO.nurseOrganogramId)) {
						mapOfMedical_emergency_requestDTOTonurseOrganogramId.put(medical_emergency_requestDTO.nurseOrganogramId, new HashSet<>());
					}
					mapOfMedical_emergency_requestDTOTonurseOrganogramId.get(medical_emergency_requestDTO.nurseOrganogramId).add(medical_emergency_requestDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Medical_emergency_requestDTO> getMedical_emergency_requestList() {
		List <Medical_emergency_requestDTO> medical_emergency_requests = new ArrayList<Medical_emergency_requestDTO>(this.mapOfMedical_emergency_requestDTOToiD.values());
		return medical_emergency_requests;
	}
	
	
	public Medical_emergency_requestDTO getMedical_emergency_requestDTOByID( long ID){
		return mapOfMedical_emergency_requestDTOToiD.get(ID);
	}
	
	
	public List<Medical_emergency_requestDTO> getMedical_emergency_requestDTOByemployee_id(long employee_id) {
		return new ArrayList<>( mapOfMedical_emergency_requestDTOToemployeeId.getOrDefault(employee_id,new HashSet<>()));
	}
	
	
	public List<Medical_emergency_requestDTO> getMedical_emergency_requestDTOByemployee_user_id(long employee_user_id) {
		return new ArrayList<>( mapOfMedical_emergency_requestDTOToemployeeUserId.getOrDefault(employee_user_id,new HashSet<>()));
	}
	
	
	public List<Medical_emergency_requestDTO> getMedical_emergency_requestDTOBypatient_name(String patient_name) {
		return new ArrayList<>( mapOfMedical_emergency_requestDTOTopatientName.getOrDefault(patient_name,new HashSet<>()));
	}
	
	
	public List<Medical_emergency_requestDTO> getMedical_emergency_requestDTOByphone_number(String phone_number) {
		return new ArrayList<>( mapOfMedical_emergency_requestDTOTophoneNumber.getOrDefault(phone_number,new HashSet<>()));
	}
	
	
	public List<Medical_emergency_requestDTO> getMedical_emergency_requestDTOBydate_of_birth(long date_of_birth) {
		return new ArrayList<>( mapOfMedical_emergency_requestDTOTodateOfBirth.getOrDefault(date_of_birth,new HashSet<>()));
	}
	
	
	public List<Medical_emergency_requestDTO> getMedical_emergency_requestDTOBygender_cat(int gender_cat) {
		return new ArrayList<>( mapOfMedical_emergency_requestDTOTogenderCat.getOrDefault(gender_cat,new HashSet<>()));
	}
	
	
	public List<Medical_emergency_requestDTO> getMedical_emergency_requestDTOBywho_is_the_patient_cat(int who_is_the_patient_cat) {
		return new ArrayList<>( mapOfMedical_emergency_requestDTOTowhoIsThePatientCat.getOrDefault(who_is_the_patient_cat,new HashSet<>()));
	}
	
	
	public List<Medical_emergency_requestDTO> getMedical_emergency_requestDTOByservice_cat(int service_cat) {
		return new ArrayList<>( mapOfMedical_emergency_requestDTOToserviceCat.getOrDefault(service_cat,new HashSet<>()));
	}
	
	
	public List<Medical_emergency_requestDTO> getMedical_emergency_requestDTOByfrom_address(String from_address) {
		return new ArrayList<>( mapOfMedical_emergency_requestDTOTofromAddress.getOrDefault(from_address,new HashSet<>()));
	}
	
	
	public List<Medical_emergency_requestDTO> getMedical_emergency_requestDTOByto_address(String to_address) {
		return new ArrayList<>( mapOfMedical_emergency_requestDTOTotoAddress.getOrDefault(to_address,new HashSet<>()));
	}
	
	
	public List<Medical_emergency_requestDTO> getMedical_emergency_requestDTOByservice_date(long service_date) {
		return new ArrayList<>( mapOfMedical_emergency_requestDTOToserviceDate.getOrDefault(service_date,new HashSet<>()));
	}
	
	
	public List<Medical_emergency_requestDTO> getMedical_emergency_requestDTOByfrom_time(String from_time) {
		return new ArrayList<>( mapOfMedical_emergency_requestDTOTofromTime.getOrDefault(from_time,new HashSet<>()));
	}
	
	
	public List<Medical_emergency_requestDTO> getMedical_emergency_requestDTOByto_time(String to_time) {
		return new ArrayList<>( mapOfMedical_emergency_requestDTOTotoTime.getOrDefault(to_time,new HashSet<>()));
	}
	
	
	public List<Medical_emergency_requestDTO> getMedical_emergency_requestDTOByservice_person_cat(int service_person_cat) {
		return new ArrayList<>( mapOfMedical_emergency_requestDTOToservicePersonCat.getOrDefault(service_person_cat,new HashSet<>()));
	}
	
	
	public List<Medical_emergency_requestDTO> getMedical_emergency_requestDTOByservice_person_organogram_id(long service_person_organogram_id) {
		return new ArrayList<>( mapOfMedical_emergency_requestDTOToservicePersonOrganogramId.getOrDefault(service_person_organogram_id,new HashSet<>()));
	}
	
	
	public List<Medical_emergency_requestDTO> getMedical_emergency_requestDTOByservice_person_user_id(long service_person_user_id) {
		return new ArrayList<>( mapOfMedical_emergency_requestDTOToservicePersonUserId.getOrDefault(service_person_user_id,new HashSet<>()));
	}
	
	
	public List<Medical_emergency_requestDTO> getMedical_emergency_requestDTOByinstructions(String instructions) {
		return new ArrayList<>( mapOfMedical_emergency_requestDTOToinstructions.getOrDefault(instructions,new HashSet<>()));
	}
	
	
	public List<Medical_emergency_requestDTO> getMedical_emergency_requestDTOByinserted_by_user_id(long inserted_by_user_id) {
		return new ArrayList<>( mapOfMedical_emergency_requestDTOToinsertedByUserId.getOrDefault(inserted_by_user_id,new HashSet<>()));
	}
	
	
	public List<Medical_emergency_requestDTO> getMedical_emergency_requestDTOByinserted_by_organogram_id(long inserted_by_organogram_id) {
		return new ArrayList<>( mapOfMedical_emergency_requestDTOToinsertedByOrganogramId.getOrDefault(inserted_by_organogram_id,new HashSet<>()));
	}
	
	
	public List<Medical_emergency_requestDTO> getMedical_emergency_requestDTOByinsertion_date(long insertion_date) {
		return new ArrayList<>( mapOfMedical_emergency_requestDTOToinsertionDate.getOrDefault(insertion_date,new HashSet<>()));
	}
	
	
	public List<Medical_emergency_requestDTO> getMedical_emergency_requestDTOBylast_modifier_user(String last_modifier_user) {
		return new ArrayList<>( mapOfMedical_emergency_requestDTOTolastModifierUser.getOrDefault(last_modifier_user,new HashSet<>()));
	}
	
	
	public List<Medical_emergency_requestDTO> getMedical_emergency_requestDTOBylastModificationTime(long lastModificationTime) {
		return new ArrayList<>( mapOfMedical_emergency_requestDTOTolastModificationTime.getOrDefault(lastModificationTime,new HashSet<>()));
	}
	
	
	public List<Medical_emergency_requestDTO> getMedical_emergency_requestDTOBynurse_user_id(long nurse_user_id) {
		return new ArrayList<>( mapOfMedical_emergency_requestDTOTonurseUserId.getOrDefault(nurse_user_id,new HashSet<>()));
	}
	
	
	public List<Medical_emergency_requestDTO> getMedical_emergency_requestDTOBynurse_organogram_id(long nurse_organogram_id) {
		return new ArrayList<>( mapOfMedical_emergency_requestDTOTonurseOrganogramId.getOrDefault(nurse_organogram_id,new HashSet<>()));
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "medical_emergency_request";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


