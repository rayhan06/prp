package medical_emergency_request;


import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.sql.SQLException;
import common.ConnectionAndStatementUtil;
import org.apache.log4j.Logger;
import repository.RepositoryManager;
import sessionmanager.SessionConstants;
import util.*;
import pb.*;
import user.UserDTO;
import user.UserRepository;

public class Medical_emergency_requestDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());

	public Medical_emergency_requestDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";
		commonMaps = new Medical_emergency_requestMAPS(tableName);
		columnNames = new String[] 
		{
			"ID",
			"employee_id",
			"employee_user_id",
			"patient_name",
			"phone_number",
			"date_of_birth",
			"gender_cat",
			"who_is_the_patient_cat",
			"service_cat",
			"from_address",
			"to_address",
			"service_date",
			"from_time",
			"to_time",
			"service_person_cat",
			"service_person_organogram_id",
			"service_person_user_id",
			"instructions",
			"inserted_by_user_id",
			"inserted_by_organogram_id",
			"insertion_date",
			"last_modifier_user",
			"nurse_user_id",
			"nurse_organogram_id",
			"driver_user_name",
			"driver_organogram_id",
			"isDeleted",
			"lastModificationTime"
		};
	}
	
	public Medical_emergency_requestDAO()
	{
		this("medical_emergency_request");		
	}
	
	public void setSearchColumn(Medical_emergency_requestDTO medical_emergency_requestDTO)
	{
		medical_emergency_requestDTO.searchColumn = "";
		medical_emergency_requestDTO.searchColumn += medical_emergency_requestDTO.patientName + " ";
		medical_emergency_requestDTO.searchColumn += medical_emergency_requestDTO.phoneNumber + " ";
		medical_emergency_requestDTO.searchColumn += Utils.getDigitBanglaFromEnglish(medical_emergency_requestDTO.phoneNumber) + " ";
		
		UserDTO employeeUser = UserRepository.getUserDTOByUserID(medical_emergency_requestDTO.employeeUserId);
		if(employeeUser != null)
		{
			medical_emergency_requestDTO.searchColumn += UserRepository.getUserDTOByUserID(medical_emergency_requestDTO.employeeUserId).userName  + " ";
			medical_emergency_requestDTO.searchColumn += Utils.getDigitBanglaFromEnglish(UserRepository.getUserDTOByUserID(medical_emergency_requestDTO.employeeUserId).userName) + " ";

		}
		
		medical_emergency_requestDTO.searchColumn += CatDAO.getName("English", "service", medical_emergency_requestDTO.serviceCat) + " " + CatDAO.getName("Bangla", "service", medical_emergency_requestDTO.serviceCat) + " ";
		medical_emergency_requestDTO.searchColumn += CatDAO.getName("English", "service_person", medical_emergency_requestDTO.servicePersonCat) + " " + CatDAO.getName("Bangla", "service_person", medical_emergency_requestDTO.servicePersonCat) + " ";
		medical_emergency_requestDTO.searchColumn += medical_emergency_requestDTO.instructions + " ";
	}
	
	public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException
	{
		Medical_emergency_requestDTO medical_emergency_requestDTO = (Medical_emergency_requestDTO)commonDTO;
		int index = 1;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(medical_emergency_requestDTO);
		if(isInsert)
		{
			ps.setObject(index++,medical_emergency_requestDTO.iD);
		}
		ps.setObject(index++,medical_emergency_requestDTO.employeeId);
		ps.setObject(index++,medical_emergency_requestDTO.employeeUserId);
		ps.setObject(index++,medical_emergency_requestDTO.patientName);
		ps.setObject(index++,medical_emergency_requestDTO.phoneNumber);
		ps.setObject(index++,medical_emergency_requestDTO.dateOfBirth);
		ps.setObject(index++,medical_emergency_requestDTO.genderCat);
		ps.setObject(index++,medical_emergency_requestDTO.whoIsThePatientCat);
		ps.setObject(index++,medical_emergency_requestDTO.serviceCat);
		ps.setObject(index++,medical_emergency_requestDTO.fromAddress);
		ps.setObject(index++,medical_emergency_requestDTO.toAddress);
		ps.setObject(index++,medical_emergency_requestDTO.serviceDate);
		ps.setObject(index++,medical_emergency_requestDTO.fromTime);
		ps.setObject(index++,medical_emergency_requestDTO.toTime);
		ps.setObject(index++,medical_emergency_requestDTO.servicePersonCat);
		ps.setObject(index++,medical_emergency_requestDTO.servicePersonOrganogramId);
		ps.setObject(index++,medical_emergency_requestDTO.servicePersonUserId);
		ps.setObject(index++,medical_emergency_requestDTO.instructions);
		ps.setObject(index++,medical_emergency_requestDTO.insertedByUserId);
		ps.setObject(index++,medical_emergency_requestDTO.insertedByOrganogramId);
		ps.setObject(index++,medical_emergency_requestDTO.insertionDate);
		ps.setObject(index++,medical_emergency_requestDTO.lastModifierUser);
		ps.setObject(index++,medical_emergency_requestDTO.nurseUserId);
		ps.setObject(index++,medical_emergency_requestDTO.nurseOrganogramId);
		ps.setObject(index++,medical_emergency_requestDTO.driverUserName);
		ps.setObject(index++,medical_emergency_requestDTO.driverOrganogramId);
		if(isInsert)
		{
			ps.setObject(index++, 0);
			ps.setObject(index++, lastModificationTime);
		}
	}
	
	public Medical_emergency_requestDTO build(ResultSet rs)
	{
		try
		{
			Medical_emergency_requestDTO medical_emergency_requestDTO = new Medical_emergency_requestDTO();
			medical_emergency_requestDTO.iD = rs.getLong("ID");
			medical_emergency_requestDTO.employeeId = rs.getLong("employee_id");
			medical_emergency_requestDTO.employeeUserId = rs.getLong("employee_user_id");
			medical_emergency_requestDTO.patientName = rs.getString("patient_name");
			medical_emergency_requestDTO.phoneNumber = rs.getString("phone_number");
			medical_emergency_requestDTO.dateOfBirth = rs.getLong("date_of_birth");
			medical_emergency_requestDTO.genderCat = rs.getInt("gender_cat");
			medical_emergency_requestDTO.whoIsThePatientCat = rs.getInt("who_is_the_patient_cat");
			medical_emergency_requestDTO.serviceCat = rs.getInt("service_cat");
			medical_emergency_requestDTO.fromAddress = rs.getString("from_address");
			medical_emergency_requestDTO.toAddress = rs.getString("to_address");
			medical_emergency_requestDTO.serviceDate = rs.getLong("service_date");
			medical_emergency_requestDTO.fromTime = rs.getString("from_time");
			medical_emergency_requestDTO.toTime = rs.getString("to_time");
			medical_emergency_requestDTO.servicePersonCat = rs.getInt("service_person_cat");
			medical_emergency_requestDTO.servicePersonOrganogramId = rs.getLong("service_person_organogram_id");
			medical_emergency_requestDTO.servicePersonUserId = rs.getLong("service_person_user_id");
			medical_emergency_requestDTO.instructions = rs.getString("instructions");
			medical_emergency_requestDTO.insertedByUserId = rs.getLong("inserted_by_user_id");
			medical_emergency_requestDTO.insertedByOrganogramId = rs.getLong("inserted_by_organogram_id");
			medical_emergency_requestDTO.insertionDate = rs.getLong("insertion_date");
			medical_emergency_requestDTO.lastModifierUser = rs.getString("last_modifier_user");
			medical_emergency_requestDTO.nurseUserId = rs.getLong("nurse_user_id");
			medical_emergency_requestDTO.nurseOrganogramId = rs.getLong("nurse_organogram_id");
			medical_emergency_requestDTO.driverUserName = rs.getString("driver_user_name");
			medical_emergency_requestDTO.driverOrganogramId = rs.getLong("driver_organogram_id");
			medical_emergency_requestDTO.isDeleted = rs.getInt("isDeleted");
			medical_emergency_requestDTO.lastModificationTime = rs.getLong("lastModificationTime");
			return medical_emergency_requestDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
	

	
	

	public Medical_emergency_requestDTO getDTOByID (long id)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID = " + id;
		Medical_emergency_requestDTO medical_emergency_requestDTO = ConnectionAndStatementUtil.getT(sql,this::build);
		return medical_emergency_requestDTO;
	}
	
	public List<Medical_emergency_requestDTO> getDTOsWithoutNurseAction()
	{
		String sql = "SELECT \r\n" + 
				"    *\r\n" + 
				"FROM\r\n" + 
				"    medical_emergency_request\r\n" + 
				"WHERE\r\n" + 
				"    nurse_organogram_id >= 0\r\n" + 
				"        AND isdeleted = 0\r\n" + 
				"        AND id NOT IN (SELECT \r\n" + 
				"            mer_id\r\n" + 
				"        FROM\r\n" + 
				"            nurse_action\r\n" + 
				"        WHERE\r\n" + 
				"            isdeleted = 0)";
		printSql(sql);
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);		
	}
	
	public List<Medical_emergency_requestDTO> getDTOs(Collection recordIDs)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID IN ( ";
		for(int i = 0;i<recordIDs.size();i++){
			if(i!=0){
				sql+=",";
			}
			sql+=((ArrayList)recordIDs).get(i);
		}
		sql+=")  order by lastModificationTime desc";
		printSql(sql);
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);		
	}

	public long getDriver(ResultSet rs)
	{
		long driver = -1;
		try
		{		
			driver = rs.getLong("id");
			return driver;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return -1;
		}
	}
	public List<Long> getAmbulanceDrivers()
	{
		String sql = "SELECT * FROM office_unit_organograms where office_unit_id =  " + SessionConstants.MEDICAL_UNIT
				+ " and designation_eng like '%driver%' and isDeleted = 0 and status = 1";
		return ConnectionAndStatementUtil.getListOfT(sql,this::getDriver);		
	}
	
	public List<Medical_emergency_requestDTO> getAllMedical_emergency_request (boolean isFirstReload)
    {
		String sql = "SELECT * FROM " + tableName + " WHERE ";
		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by " + tableName + ".lastModificationTime desc";
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);	
    }

	
	public List<Medical_emergency_requestDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO)
	{
		return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
	}

	
	public List<Medical_emergency_requestDTO> getDTOs(Hashtable  p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
			String filter, boolean tableHasJobCat)
	{
		String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat);
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);			
	}
	public String getSqlWithSearchCriteriaForNormalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
			UserDTO userDTO, String filter, boolean tableHasJobCat)
    {
		boolean viewAll = false;
		
		if(p_searchCriteria != null && p_searchCriteria.get("ViewAll") != null)
		{
			System.out.println("ViewAll = " + p_searchCriteria.get("ViewAll"));
			viewAll = true;
		}
		else
		{
			System.out.println("ViewAll is null ");
		}
		
		String sql = "SELECT ";
		if(category == GETIDS)
		{
			sql += " " + tableName + ".ID as ID ";
		}
		else if(category == GETCOUNT)
		{
			sql += " count( " + tableName + ".ID) as countID ";
		}
		else if(category == GETDTOS)
		{
			sql += " distinct " + tableName + ".* ";
		}
		sql += "FROM " + tableName + " ";
		sql += joinSQL;
		

		
		String AnyfieldSql = "";
		String AllFieldSql = "";
		
		if(p_searchCriteria != null)
		{
						
			
			Enumeration names = p_searchCriteria.keys();
			String str, value;
			
			AnyfieldSql = "(";
			
			if(p_searchCriteria.get("AnyField")!= null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase(""))
			{
				AnyfieldSql+= tableName + ".search_column like '%" + p_searchCriteria.get("AnyField").toString() + "%'";			
			}
			AnyfieldSql += ")";
			System.out.println("AnyfieldSql = " + AnyfieldSql);
			
			AllFieldSql = "(";
			int i = 0;
			while(names.hasMoreElements())
			{				
				str = (String) names.nextElement();
				value = (String)p_searchCriteria.get(str);
		        System.out.println(str + ": " + value);
				if(value != null && !value.equalsIgnoreCase("") && (
						 str.equals("patient_name")
						|| str.equals("phone_number")
						|| str.equals("service_cat")
						|| str.equals("service_date_start")
						|| str.equals("service_date_end")
						|| str.equals("service_person_organogram_id")
						|| str.equals("nurse_organogram_id")
						|| str.equals("sl")
					
				)
						
				)
				{
					if(p_searchCriteria.get(str).equals("any"))
					{
						continue;
					}

					if( i > 0)
					{
						AllFieldSql+= " AND  ";
					}
					
					 if(str.equals("patient_name"))
					{
						AllFieldSql += "" + tableName + ".patient_name like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					else if(str.equals("phone_number"))
					{
						AllFieldSql += "" + tableName + ".phone_number like '%" + p_searchCriteria.get(str) + "%'";
						i ++;
					}
					
					else if(str.equals("service_cat"))
					{
						AllFieldSql += "" + tableName + ".service_cat = " + Long.parseLong((String) p_searchCriteria.get(str));
						i ++;
					}
					else if(str.equals("service_date_start"))
					{
						AllFieldSql += "" + tableName + ".service_date >= " + Long.parseLong((String)p_searchCriteria.get(str));
						i ++;
					}
					else if(str.equals("service_date_end"))
					{
						AllFieldSql += "" + tableName + ".service_date <= " + Long.parseLong((String)p_searchCriteria.get(str));
						i ++;
					}
					else if(str.equals("service_person_organogram_id"))
					{
						AllFieldSql += "" + tableName + ".service_person_organogram_id = " + Long.parseLong((String)p_searchCriteria.get(str));
						i ++;
					}
					else if(str.equals("nurse_organogram_id"))
					{
						AllFieldSql += "" + tableName + ".nurse_organogram_id = " + Long.parseLong((String)p_searchCriteria.get(str));
						i ++;
					}
					else if(str.equals("sl"))
					{
						AllFieldSql += "" + tableName + ".id = " + Long.parseLong((String)p_searchCriteria.get(str));
						i ++;
					}
					
					
					
				}
			}
			
			AllFieldSql += ")";
			System.out.println("AllFieldSql = " + AllFieldSql);
			
			
		}
		
		
		sql += " WHERE ";
		
		sql += " (" + tableName + ".isDeleted = 0 ";
		if(userDTO.roleID != SessionConstants.ADMIN_ROLE && userDTO.roleID != SessionConstants.MEDICAL_ADMIN_ROLE
				&& userDTO.roleID != SessionConstants.NURSE_ROLE && userDTO.roleID != SessionConstants.MEDICAL_RECEPTIONIST_ROLE)
		{
			sql += " and (employee_id = " + userDTO.organogramID + " or ";
			sql += " service_person_organogram_id = " + userDTO.organogramID + " or ";
			sql += " nurse_organogram_id = " + userDTO.organogramID + " ) ";
		}
		sql += ")";
		
		
		if(!filter.equalsIgnoreCase(""))
		{
			sql += " and " + filter + " ";
		}
		
		if(!AnyfieldSql.equals("()") && !AnyfieldSql.equals(""))
		{
			sql += " AND " + AnyfieldSql;
			
		}
		if(!AllFieldSql.equals("()") && !AllFieldSql.equals(""))
		{			
			sql += " AND " + AllFieldSql;
		}
		
	
			
		sql += " order by " + tableName + ".lastModificationTime desc ";

		printSql(sql);
		
		if(limit >= 0)
		{
			sql += " limit " + limit;
		}
		if(offset >= 0)
		{
			sql += " offset " + offset;
		}
		
		System.out.println("-------------- sql = " + sql);
		
		return sql;
    }
	
	public String getSqlWithSearchCriteria(Hashtable p_searchCriteria, int limit, int offset, int category,
			boolean isPermanentTable, UserDTO userDTO, String filter, boolean tableHasJobCat)
    {		
		return getSqlWithSearchCriteriaForNormalSearch(p_searchCriteria, limit, offset, category, userDTO, filter, tableHasJobCat);	
    }
				
}
	