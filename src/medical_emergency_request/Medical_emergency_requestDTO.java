package medical_emergency_request;
import java.util.*;

import family.FamilyDTO;
import util.*; 


public class Medical_emergency_requestDTO extends CommonDTO
{

	public long employeeId = -1;
	public long employeeUserId = -1;
    public String patientName = "";
    public String phoneNumber = "";
	public long dateOfBirth = System.currentTimeMillis();
	public int genderCat = -1;
	public int whoIsThePatientCat = FamilyDTO.UNSELECTED;
	public int serviceCat = -1;
    public String fromAddress = "";
    public String toAddress = "";
	public long serviceDate = System.currentTimeMillis();
    public String fromTime = "";
    public String toTime = "";
	public int servicePersonCat = -1;
	public long servicePersonOrganogramId = -1;
	public long servicePersonUserId = -1;
    public String instructions = "";
	public long insertedByUserId = -1;
	public long insertedByOrganogramId = -1;
	public long insertionDate = -1;
    public String lastModifierUser = "";
	public long nurseUserId = -1;
	public long nurseOrganogramId = -1;
	public long driverOrganogramId = -1;
	public String driverUserName = "";
	
	
    @Override
	public String toString() {
            return "$Medical_emergency_requestDTO[" +
            " iD = " + iD +
            " employeeId = " + employeeId +
            " employeeUserId = " + employeeUserId +
            " patientName = " + patientName +
            " phoneNumber = " + phoneNumber +
            " dateOfBirth = " + dateOfBirth +
            " genderCat = " + genderCat +
            " whoIsThePatientCat = " + whoIsThePatientCat +
            " serviceCat = " + serviceCat +
            " fromAddress = " + fromAddress +
            " toAddress = " + toAddress +
            " serviceDate = " + serviceDate +
            " fromTime = " + fromTime +
            " toTime = " + toTime +
            " servicePersonCat = " + servicePersonCat +
            " servicePersonOrganogramId = " + servicePersonOrganogramId +
            " servicePersonUserId = " + servicePersonUserId +
            " instructions = " + instructions +
            " insertedByUserId = " + insertedByUserId +
            " insertedByOrganogramId = " + insertedByOrganogramId +
            " insertionDate = " + insertionDate +
            " lastModifierUser = " + lastModifierUser +
            " nurseUserId = " + nurseUserId +
            " nurseOrganogramId = " + nurseOrganogramId +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}