package medical_emergency_request;

import com.google.gson.Gson;
import employee_offices.EmployeeOfficeRepository;
import language.LC;
import language.LM;
import login.LoginDTO;
import nurse_action.NurseActionDetailsDAO;
import nurse_action.NurseActionDetailsDTO;
import nurse_action.Nurse_actionDAO;
import nurse_action.Nurse_actionDTO;

import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;

import pb.CatDAO;
import pb.CommonDAO;
import pb.Utils;
import pb_notifications.Pb_notificationsDAO;
import permission.MenuConstants;
import role.PermissionRepository;
import sessionmanager.SessionConstants;

import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;
import util.CommonRequestHandler;
import util.RecordNavigationManager4;
import workflow.WorkflowController;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Set;




/**
 * Servlet implementation class Medical_emergency_requestServlet
 */
@WebServlet("/Medical_emergency_requestServlet")
@MultipartConfig
public class Medical_emergency_requestServlet extends HttpServlet 
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Medical_emergency_requestServlet.class);

    String tableName = "medical_emergency_request";

	Medical_emergency_requestDAO medical_emergency_requestDAO;
	Nurse_actionDAO nurse_actionDAO = Nurse_actionDAO.getInstance();
	NurseActionDetailsDAO nurseActionDetailsDAO = NurseActionDetailsDAO.getInstance();
	CommonRequestHandler commonRequestHandler;
    private final Gson gson = new Gson();
    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Medical_emergency_requestServlet() 
	{
        super();
    	try
    	{
			medical_emergency_requestDAO = new Medical_emergency_requestDAO(tableName);
			commonRequestHandler = new CommonRequestHandler(medical_emergency_requestDAO);
			
			List<Medical_emergency_requestDTO> merDTOs = medical_emergency_requestDAO.getDTOsWithoutNurseAction();
			
			if(merDTOs != null)
			{
				logger.debug("^^^^^^^^^^^^^^^^^^^ bad merDTOscount = " + merDTOs.size());
				for(Medical_emergency_requestDTO medical_emergency_requestDTO: merDTOs)
				{
					Nurse_actionDTO nurse_actionDTO = new Nurse_actionDTO(medical_emergency_requestDTO);
					nurse_actionDAO.add(nurse_actionDTO);
					NurseActionDetailsDTO nurseActionDetailsDTO = new NurseActionDetailsDTO(nurse_actionDTO);
					nurseActionDetailsDAO.add(nurseActionDetailsDTO);
				}
			}
			else
			{
				logger.debug("^^^^^^^^^^^^^^^^^^^ bad merDTOscount = 0");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }   

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		System.out.println("In doget request = " + request);
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		try
		{
			String actionType = request.getParameter("actionType");
			if(actionType.equals("getAddPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.MEDICAL_EMERGENCY_REQUEST_ADD))
				{
					commonRequestHandler.getAddPage(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getEditPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.MEDICAL_EMERGENCY_REQUEST_UPDATE))
				{
					getMedical_emergency_request(request, response);					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}						
			}
			else if(actionType.equals("xl"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.MEDICAL_EMERGENCY_REQUEST_SEARCH))
				{
					getXl(request, response, userDTO, loginDTO);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}		
			}
			else if(actionType.equals("getURL"))
			{
				String URL = request.getParameter("URL");
				System.out.println("URL = " + URL);
				response.sendRedirect(URL);			
			}			
			else if(actionType.equals("search"))
			{
				System.out.println("search requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.MEDICAL_EMERGENCY_REQUEST_SEARCH))
				{
					if(isPermanentTable)
					{
						String filter = request.getParameter("filter");
						System.out.println("filter = " + filter);
						if(filter!=null)
						{
							filter = ""; //shouldn't be directly used, rather manipulate it.
							searchMedical_emergency_request(request, response, isPermanentTable, filter);
						}
						else
						{
							searchMedical_emergency_request(request, response, isPermanentTable, "");
						}
					}					
					else
					{
						//searchMedical_emergency_request(request, response, tempTableName, isPermanentTable);
					}
				}			
			}
			else if(actionType.equals("view"))
			{
				System.out.println("view requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.MEDICAL_EMERGENCY_REQUEST_SEARCH))
				{
					commonRequestHandler.view(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else
			{
				request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}


	
	

	private void getXl(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO, LoginDTO loginDTO) {
		Hashtable hashTable = null;
		hashTable  = commonRequestHandler.fillHashTable(hashTable, request);
		
		List <Medical_emergency_requestDTO> medical_emergency_requestDTOs = medical_emergency_requestDAO.getDTOs(hashTable, -1, -1, true, userDTO);
		
		String Language = LM.getLanguage(userDTO);
		
		try {
			getXl(medical_emergency_requestDTOs, response, Language, loginDTO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void getXl(List <Medical_emergency_requestDTO> medical_emergency_requestDTOs, HttpServletResponse response, String Language, LoginDTO loginDTO) throws Exception
    {
		XSSFWorkbook wb = new XSSFWorkbook();
    	boolean isLangEng = Language.equalsIgnoreCase("english");

        List<String> headers = new ArrayList<String>();
        
    	headers.add(LM.getText(LC.HM_SL, loginDTO));
        headers.add(LM.getText(LC.HM_EMPLOYEE_ID, loginDTO));
        headers.add(LM.getText(LC.MEDICAL_EMERGENCY_REQUEST_ADD_SERVICECAT, loginDTO));
        headers.add(LM.getText(LC.MEDICAL_EMERGENCY_REQUEST_ADD_FROMADDRESS, loginDTO));
        headers.add(LM.getText(LC.MEDICAL_EMERGENCY_REQUEST_ADD_TOADDRESS, loginDTO));
        headers.add(LM.getText(LC.MEDICAL_EMERGENCY_REQUEST_ADD_SERVICEDATE, loginDTO));
        headers.add(isLangEng ? "Service Person(s)" : "সেবাদানকারী");
        
        Sheet sheet = null;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm aa");

        String tabName = "Medical Emergeny Requests";
        
        sheet = wb.createSheet(tabName);
		Row headerRow = sheet.createRow(0);
		try {
			int cellIndex = 0;
			for(String header: headers)
			{
				sheet.setColumnWidth(cellIndex, 25 * 256);
				Cell cell = headerRow.createCell(cellIndex);
				cell.setCellValue(header);
				cell.setCellStyle(commonRequestHandler.getHeaderStyle(wb));
				
				cellIndex++;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		int i = 1;
		CellStyle wrapStyle = wb.createCellStyle();
		wrapStyle.setWrapText(true);
		wrapStyle.setVerticalAlignment(VerticalAlignment.TOP);
		
		for(Medical_emergency_requestDTO medical_emergency_requestDTO: medical_emergency_requestDTOs)
		{
			Row row = sheet.createRow(i);
			
			int cellIndex = 0;
			
			Cell cell = row.createCell(cellIndex);
			cell.setCellStyle(wrapStyle);
			String value = Utils.getDigits(medical_emergency_requestDTO.iD, isLangEng);
			cell.setCellValue(value);
			value = "";
			cellIndex ++;
			
			
			cell = row.createCell(cellIndex);
			cell.setCellStyle(wrapStyle);
			value = WorkflowController.getUserNameFromUserId(medical_emergency_requestDTO.employeeUserId);
	        if (medical_emergency_requestDTO.employeeUserId == -1) {
	            value = "";
	        }
	        value += "\n" +  medical_emergency_requestDTO.patientName;
	        value += "\n" +  Utils.getDigits(medical_emergency_requestDTO.phoneNumber, Language);
			cell.setCellValue(value);
			value = "";
			cellIndex ++;
			
			
			cell = row.createCell(cellIndex);
			cell.setCellStyle(wrapStyle);
			value = CatDAO.getName(Language, "service", medical_emergency_requestDTO.serviceCat);	       
			cell.setCellValue(value);
			value = "";
			cellIndex ++;
			
			
			cell = row.createCell(cellIndex);
			cell.setCellStyle(wrapStyle);
			value = medical_emergency_requestDTO.fromAddress;	       
			cell.setCellValue(value);
			value = "";
			cellIndex ++;
			
			
			cell = row.createCell(cellIndex);
			cell.setCellStyle(wrapStyle);
			value = medical_emergency_requestDTO.toAddress;	       
			cell.setCellValue(value);
			value = "";
			cellIndex ++;
			
			
			cell = row.createCell(cellIndex);
			cell.setCellStyle(wrapStyle);
			value = simpleDateFormat.format(new Date(medical_emergency_requestDTO.serviceDate));
			String fromTime = "";
			if(medical_emergency_requestDTO.fromTime != null && !medical_emergency_requestDTO.fromTime.equalsIgnoreCase(""))
			{
				fromTime = Utils.getDigits(fromTime, Language);
				value += fromTime;
			}
			if(!fromTime.equalsIgnoreCase(""))
			{
				value += "-";
			}
			if(medical_emergency_requestDTO.toTime != null && !medical_emergency_requestDTO.toTime.equalsIgnoreCase(""))
			{
				value += Utils.getDigits(medical_emergency_requestDTO.toTime, Language);
			}
			cell.setCellValue(value);
			value = "";
			cellIndex ++;
			
			
			cell = row.createCell(cellIndex);
			cell.setCellStyle(wrapStyle);
			if(medical_emergency_requestDTO.servicePersonUserId != -1)
	    	{
	    		value += LM.getText(LC.HM_DOCTOR, loginDTO) + ": " 
	    				+ WorkflowController.getNameFromUserId(medical_emergency_requestDTO.servicePersonUserId, Language) + "\n";
	    		
	    	} 
			if(medical_emergency_requestDTO.nurseUserId != -1)
	    	{
	    		value += LM.getText(LC.HM_NURSE, loginDTO) + ": " 
	    				+ WorkflowController.getNameFromUserId(medical_emergency_requestDTO.nurseUserId, Language) + "\n";
	    		
	    	}
			if(medical_emergency_requestDTO.driverUserName != null && !medical_emergency_requestDTO.driverUserName.equalsIgnoreCase(""))	    	
	    	{
	    		value += (isLangEng ? "Driver" : "ড্রাইভার") + ": " 
	    				+ WorkflowController.getNameFromUserName(medical_emergency_requestDTO.driverUserName, Language) + "\n";
	    		
	    	}
			cell.setCellValue(value);
			value = "";
			cellIndex ++;
			
			
			
			i++;						
		}
		
		try {
        	String fileName =  "EmergencyRequests.xlsx";
            response.setContentType("application/vnd.ms-excel");
            response.setHeader("Content-Disposition", "attachment; filename=" + fileName);
            System.out.println("Writing xl");
			wb.write(response.getOutputStream());
			wb.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} // Write workbook to
       
    }

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		// TODO Auto-generated method stub
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		System.out.println("doPost");
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		
		try
		{
			String actionType = request.getParameter("actionType");
			System.out.println("actionType = " + actionType);
			if(actionType.equals("add"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.MEDICAL_EMERGENCY_REQUEST_ADD))
				{
					System.out.println("going to  addMedical_emergency_request ");
					addMedical_emergency_request(request, response, true, userDTO, true);
				}
				else
				{
					System.out.println("Not going to  addMedical_emergency_request ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else if(actionType.equals("getServicePersons"))
			{
				long id = Long.parseLong(request.getParameter("id"));
				int role = Integer.parseInt(request.getParameter("role"));
				String options = CommonDAO.getEmployeesByOrganogramIDAndRole(id,  role);
				response.getWriter().write(options);
			}

			else if(actionType.equals("getDTO"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.MEDICAL_EMERGENCY_REQUEST_ADD))
				{
					getDTO(request, response);					
				}
				else
				{
					System.out.println("Not going to  addMedical_emergency_request ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else if(actionType.equals("edit"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.MEDICAL_EMERGENCY_REQUEST_UPDATE))
				{					
					addMedical_emergency_request(request, response, false, userDTO, isPermanentTable);					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("addService"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.MEDICAL_EMERGENCY_REQUEST_UPDATE))
				{					
					addService(request, response, userDTO);					
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("delete"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.MEDICAL_EMERGENCY_REQUEST_UPDATE))
				{
					commonRequestHandler.delete(request, response, userDTO);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}	
			else if(actionType.equals("search"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.MEDICAL_EMERGENCY_REQUEST_SEARCH))
				{
					searchMedical_emergency_request(request, response, true, "");
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getGeo"))
			{
				System.out.println("going to geoloc ");
				request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
			}
			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}
	
	private void getDTO(HttpServletRequest request, HttpServletResponse response) 
	{
		try 
		{
			System.out.println("In getDTO");
			Medical_emergency_requestDTO medical_emergency_requestDTO = medical_emergency_requestDAO.getDTOByID(Long.parseLong(request.getParameter("ID")));
			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			
			String encoded = this.gson.toJson(medical_emergency_requestDTO);
			System.out.println("json encoded data = " + encoded);
			out.print(encoded);
			out.flush();
		}
		catch (NumberFormatException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	private void sendEmergencyRequest(Medical_emergency_requestDTO merDTO)
	{
		String URL = "";
		String englishText = "";
		String banglaText = "";
		if(merDTO.serviceCat == SessionConstants.HOME_SERVICE)
		{
			URL = "Medical_emergency_requestServlet?actionType=view&ID=" + merDTO.iD;
			englishText = "An emergeny patient needs home service.";
			banglaText = "একজন জরুরী রোগী গৃহসেবার অপেক্ষায় আছেন।";
		}
		else
		{
			URL = "Medical_emergency_requestServlet?actionType=view&ID=" + merDTO.iD;
			englishText = "Please send an ambulance.";
			banglaText = "জরুরি ভিত্তিতে অ্যাম্বুলেন্স পাঠান।";
        }
		Pb_notificationsDAO pb_notificationsDAO = Pb_notificationsDAO.getInstance();
		Set<Long> medicalAdmins = EmployeeOfficeRepository.getInstance().getByRole(SessionConstants.MEDICAL_ADMIN_ROLE);
		for(Long medicalAdmin: medicalAdmins)
		{
			pb_notificationsDAO.addPb_notificationsAndSendMailSMS(medicalAdmin, System.currentTimeMillis(), URL, englishText, banglaText, "Home Service Request", SessionConstants.MEDICAL_ADMIN_ROLE);
		}
	}
	
	private void addService(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO) throws IOException 
	{
		if(userDTO.roleID != SessionConstants.ADMIN_ROLE && userDTO.roleID != SessionConstants.MEDICAL_ADMIN_ROLE
				&& userDTO.roleID != SessionConstants.NURSE_ROLE && userDTO.roleID != SessionConstants.MEDICAL_RECEPTIONIST_ROLE)
		{
			try {
				request.getRequestDispatcher("medical_emergency_request/medical_emergency_requestSearch.jsp").forward(request, response);
			} catch (ServletException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		Medical_emergency_requestDTO medical_emergency_requestDTO = medical_emergency_requestDAO.getDTOByID(Long.parseLong(request.getParameter("ID")));
		
		String Value;
		
		
		Value= request.getParameter("servicePersonOrganogramId");

		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("servicePersonOrganogramId = " + Value);
		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			medical_emergency_requestDTO.servicePersonOrganogramId = Long.parseLong(Value);
			UserDTO serviceUserDTO = UserRepository.getUserDTOByOrganogramID(medical_emergency_requestDTO.servicePersonOrganogramId);
			if(serviceUserDTO != null)
			{
				medical_emergency_requestDTO.servicePersonUserId = serviceUserDTO.ID;
			}
			
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}
		
		UserDTO nurseDTO = null;
		
		Value = request.getParameter("nurseOrganogramId");

		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("nurseOrganogramId = " + Value);
		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			medical_emergency_requestDTO.nurseOrganogramId = Long.parseLong(Value);
			nurseDTO = UserRepository.getUserDTOByOrganogramID(medical_emergency_requestDTO.nurseOrganogramId);
			if(nurseDTO != null)
			{
				medical_emergency_requestDTO.nurseUserId = nurseDTO.ID;
			}
			
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}
		
		Value = request.getParameter("driverOrganogramId");
		
		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("driverOrganogramId = " + Value);
		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			medical_emergency_requestDTO.driverOrganogramId = Long.parseLong(Value);
			UserDTO driverDTO = UserRepository.getUserDTOByOrganogramID(medical_emergency_requestDTO.driverOrganogramId);
			if(driverDTO != null)
			{
				medical_emergency_requestDTO.driverUserName = driverDTO.userName;
			}
			
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}
       		
		medical_emergency_requestDTO.lastModifierUser = userDTO.userName;
		try {
			medical_emergency_requestDAO.update(medical_emergency_requestDTO);
			if(nurseDTO != null)
			{
				Nurse_actionDTO nurse_actionDTO = nurse_actionDAO.getDTOByMerId(medical_emergency_requestDTO.iD);
				if(nurse_actionDTO != null)
				{
					nurseActionDetailsDAO.hardDeleteChildrenByParent(medical_emergency_requestDTO.iD, "mer_id");
					List<Long> ids = new ArrayList<Long>();
					ids.add(nurse_actionDTO.iD);
					nurse_actionDAO.hardDeleteChildrenByParent(medical_emergency_requestDTO.iD, "mer_id");					
				}
				
				nurse_actionDTO = new Nurse_actionDTO(medical_emergency_requestDTO);
				nurse_actionDAO.add(nurse_actionDTO);
				
				NurseActionDetailsDTO nurseActionDetailsDTO = new NurseActionDetailsDTO(nurse_actionDTO);
				nurseActionDetailsDAO.add(nurseActionDetailsDTO);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			request.getRequestDispatcher("medical_emergency_request/medical_emergency_requestSearch.jsp").forward(request, response);
		} catch (ServletException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void addMedical_emergency_request(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, boolean isPermanentTable) throws IOException 
	{
		// TODO Auto-generated method stub
		try 
		{
			request.setAttribute("failureMessage", "");
			System.out.println("%%%% addMedical_emergency_request");
			String path = getServletContext().getRealPath("/img2/");
			Medical_emergency_requestDTO medical_emergency_requestDTO;
			SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
						
			if(addFlag == true)
			{
				medical_emergency_requestDTO = new Medical_emergency_requestDTO();
			}
			else
			{
				medical_emergency_requestDTO = medical_emergency_requestDAO.getDTOByID(Long.parseLong(request.getParameter("iD")));
			}
			
			String Value = "";

			Value = request.getParameter("orgId");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("employeeId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				medical_emergency_requestDTO.employeeId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			medical_emergency_requestDTO.employeeUserId = UserRepository.getUserDTOByOrganogramID(medical_emergency_requestDTO.employeeId).ID;

			Value = request.getParameter("patientName");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("patientName = " + Value);
			if(Value != null)
			{
				medical_emergency_requestDTO.patientName = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("phoneNumber");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("phoneNumber = " + Value);
			if(Value != null)
			{
				medical_emergency_requestDTO.phoneNumber = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("dateOfBirth");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("dateOfBirth = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				try 
				{
					Date d = f.parse(Value);
					medical_emergency_requestDTO.dateOfBirth = d.getTime();
				}
				catch (Exception e) 
				{
					e.printStackTrace();
				}
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("genderCat");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("genderCat = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				medical_emergency_requestDTO.genderCat = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("whoIsThePatientCat");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("whoIsThePatientCat = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				medical_emergency_requestDTO.whoIsThePatientCat = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("serviceCat");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("serviceCat = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				medical_emergency_requestDTO.serviceCat = Integer.parseInt(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("fromAddress");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("fromAddress = " + Value);
			if(Value != null)
			{
				medical_emergency_requestDTO.fromAddress = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("toAddress");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("toAddress = " + Value);
			if(Value != null)
			{
				medical_emergency_requestDTO.toAddress = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("serviceDate");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("serviceDate = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				try 
				{
					Date d = f.parse(Value);
					medical_emergency_requestDTO.serviceDate = d.getTime();
				}
				catch (Exception e) 
				{
					e.printStackTrace();
				}
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("fromTime");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("fromTime = " + Value);
			if(Value != null)
			{
				medical_emergency_requestDTO.fromTime = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("toTime");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("toTime = " + Value);
			if(Value != null)
			{
				medical_emergency_requestDTO.toTime = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}


			Value = request.getParameter("instructions");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("instructions = " + Value);
			if(Value != null)
			{
				medical_emergency_requestDTO.instructions = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			if(addFlag)
			{
				medical_emergency_requestDTO.insertedByUserId = userDTO.ID;
			}


			if(addFlag)
			{
				medical_emergency_requestDTO.insertedByOrganogramId = userDTO.organogramID;
			}


			if(addFlag)
			{
				Calendar c = Calendar.getInstance();
				c.set(Calendar.HOUR_OF_DAY, 0);
				c.set(Calendar.MINUTE, 0);
				c.set(Calendar.SECOND, 0);
				c.set(Calendar.MILLISECOND, 0);
				
				medical_emergency_requestDTO.insertionDate = c.getTimeInMillis();
			}			


			medical_emergency_requestDTO.lastModifierUser = userDTO.userName;

			
			System.out.println("Done adding  addMedical_emergency_request dto = " + medical_emergency_requestDTO);
			long returnedID = -1;
			
			if(isPermanentTable == false) //add new row for validation and make the old row outdated
			{
				medical_emergency_requestDAO.setIsDeleted(medical_emergency_requestDTO.iD, CommonDTO.OUTDATED);
				returnedID = medical_emergency_requestDAO.add(medical_emergency_requestDTO);
				medical_emergency_requestDAO.setIsDeleted(returnedID, CommonDTO.WAITING_FOR_APPROVAL);
			}
			else if(addFlag == true)
			{
				returnedID = medical_emergency_requestDAO.manageWriteOperations(medical_emergency_requestDTO, SessionConstants.INSERT, -1, userDTO);
			}
			else
			{				
				returnedID = medical_emergency_requestDAO.manageWriteOperations(medical_emergency_requestDTO, SessionConstants.UPDATE, -1, userDTO);											
			}
			
			
			sendEmergencyRequest(medical_emergency_requestDTO);
			
			
			
			
			
			
			
			if(isPermanentTable)
			{
				String inPlaceSubmit = request.getParameter("inplacesubmit");
				
				if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
				{
					getMedical_emergency_request(request, response, returnedID);
				}
				else
				{
					response.sendRedirect("Medical_emergency_requestServlet?actionType=search");
				}
			}
			else
			{
				commonRequestHandler.validate(medical_emergency_requestDAO.getDTOByID(returnedID), request, response, userDTO);
			}
					
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	



	
	
	

	private void getMedical_emergency_request(HttpServletRequest request, HttpServletResponse response, long id) throws ServletException, IOException
	{
		System.out.println("in getMedical_emergency_request");
		Medical_emergency_requestDTO medical_emergency_requestDTO = null;
		try 
		{
			medical_emergency_requestDTO = medical_emergency_requestDAO.getDTOByID(id);
			request.setAttribute("ID", medical_emergency_requestDTO.iD);
			request.setAttribute("medical_emergency_requestDTO",medical_emergency_requestDTO);
			request.setAttribute("medical_emergency_requestDAO",medical_emergency_requestDAO);
			
			String URL= "";
			
			String inPlaceEdit = request.getParameter("inplaceedit");
			String inPlaceSubmit = request.getParameter("inplacesubmit");
			String getBodyOnly = request.getParameter("getBodyOnly");
			
			if(inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase(""))
			{
				URL = "medical_emergency_request/medical_emergency_requestInPlaceEdit.jsp";	
				request.setAttribute("inplaceedit","");				
			}
			else if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
			{
				URL = "medical_emergency_request/medical_emergency_requestSearchRow.jsp";
				request.setAttribute("inplacesubmit","");					
			}
			else
			{
				if(getBodyOnly  != null && !getBodyOnly.equalsIgnoreCase(""))
				{
					URL = "medical_emergency_request/medical_emergency_requestEditBody.jsp?actionType=edit";
				}
				else
				{
					URL = "medical_emergency_request/medical_emergency_requestEdit.jsp?actionType=edit";
				}				
			}
			
			RequestDispatcher rd = request.getRequestDispatcher(URL);
			rd.forward(request, response);
		}
		catch (NumberFormatException e) 
		{
			e.printStackTrace();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	
	private void getMedical_emergency_request(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		getMedical_emergency_request(request, response, Long.parseLong(request.getParameter("ID")));	
	}
	
	private void searchMedical_emergency_request(HttpServletRequest request, HttpServletResponse response, boolean isPermanent, String filter) throws ServletException, IOException
	{
		System.out.println("in  searchMedical_emergency_request 1");
		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String ajax = request.getParameter("ajax");
		boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);
		
        RecordNavigationManager4 rnManager = new RecordNavigationManager4(
			SessionConstants.NAV_MEDICAL_EMERGENCY_REQUEST,
			request,
			medical_emergency_requestDAO,
			SessionConstants.VIEW_MEDICAL_EMERGENCY_REQUEST,
			SessionConstants.SEARCH_MEDICAL_EMERGENCY_REQUEST,
			tableName,
			isPermanent,
			userDTO,
			filter,
			true);
        try
        {
			System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        }
        catch(Exception e)
        {
			System.out.println("failed to dojob" + e);
        }

		request.setAttribute("medical_emergency_requestDAO",medical_emergency_requestDAO);
        RequestDispatcher rd;
        if(!isPermanent)
        {
        	if(hasAjax == false)
	        {
	        	System.out.println("Going to medical_emergency_request/medical_emergency_requestApproval.jsp");
	        	rd = request.getRequestDispatcher("medical_emergency_request/medical_emergency_requestApproval.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to medical_emergency_request/medical_emergency_requestApprovalForm.jsp");
	        	rd = request.getRequestDispatcher("medical_emergency_request/medical_emergency_requestApprovalForm.jsp");
	        }
        }
        else
        {
	        if(hasAjax == false)
	        {
	        	System.out.println("Going to medical_emergency_request/medical_emergency_requestSearch.jsp");
	        	rd = request.getRequestDispatcher("medical_emergency_request/medical_emergency_requestSearch.jsp");
	        }
	        else
	        {
	        	System.out.println("Going to medical_emergency_request/medical_emergency_requestSearchForm.jsp");
	        	rd = request.getRequestDispatcher("medical_emergency_request/medical_emergency_requestSearchForm.jsp");
	        }
        }
		rd.forward(request, response);
	}
	
}

