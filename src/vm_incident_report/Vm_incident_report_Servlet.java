package vm_incident_report;


import java.io.IOException;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import language.LC;
import language.LM;

import login.LoginDTO;
import permission.MenuConstants;

import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;

import util.ReportRequestHandler;
import pbReport.*;






@WebServlet("/Vm_incident_report_Servlet")
public class Vm_incident_report_Servlet  extends HttpServlet {
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	String[][] Criteria =
	{
		{"criteria","tr","incident_date",">=","","long","","",Long.MIN_VALUE + "","startDate", LC.HM_START_DATE + ""},
		{"criteria","tr","incident_date","<=","AND","long","","",Long.MAX_VALUE + "","endDate", LC.HM_END_DATE + ""},
		{"criteria","tr","incident_cat","=","AND","int","","","any","incidentCat", LC.VM_INCIDENT_REPORT_WHERE_INCIDENTCAT + ""},
		{"criteria","tr","vehicle_type_cat","=","AND","int","","","any","vehicleTypeCat", LC.VM_INCIDENT_REPORT_WHERE_VEHICLETYPECAT + ""},
		{"criteria","tr","vehicle_id","=","AND","String","","","any","vehicleId", LC.VM_INCIDENT_REPORT_WHERE_VEHICLEID + ""},
		{"criteria","tr","reg_no","=","AND","String","","","any","regNo", LC.VM_INCIDENT_REPORT_WHERE_REGNO + ""},
		{"criteria","tr","driver_phone_num","=","AND","String","","","any","driverPhoneNum", LC.VM_INCIDENT_REPORT_WHERE_DRIVERPHONENUM + ""},
		{"criteria","tr","driver_name_en","=","AND","String","","","any","driverNameEn", LC.VM_INCIDENT_REPORT_WHERE_DRIVERNAMEEN + ""},
		{"criteria","tr","driver_name_bn","=","AND","String","","","any","driverNameBn", LC.VM_INCIDENT_REPORT_WHERE_DRIVERNAMEBN + ""},
		{"criteria","tr","isDeleted","=","AND","String","","","0","isDeleted", LC.VM_INCIDENT_REPORT_WHERE_ISDELETED + ""}
	};

	String[][] Display =
	{
		{"display","tr","incident_cat","cat",""},
		{"display","tr","vehicle_type_cat","cat",""},
		//{"display","tr","vehicle_id","text",""},
		{"display","tr","reg_no","text",""},
		{"display","tr","incident_date","date",""},
		{"display","tr","incident_time","time",""},
		{"display","tr","driver_phone_num","text",""},
		{"display","tr","driver_name_en","text",""},
		{"display","tr","driver_name_bn","text",""},
		{"display","tr","incident_place","text",""}
	};

	String GroupBy = "";
	String OrderBY = "";

	ReportRequestHandler reportRequestHandler;

	public Vm_incident_report_Servlet(){

	}

	private final ReportService reportService = new ReportService();

	private String sql;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{

		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String language = "english";
		if(userDTO!= null && userDTO.languageID == SessionConstants.BANGLA)
		{
			language = "bangla";
		}

		String actionType = request.getParameter("actionType");

		System.out.println("In ssservlet doget, actiontype = " + actionType);

		sql = "vm_incident tr";
		int i=0;

		Display[i++][4] = LM.getText(LC.VM_INCIDENT_REPORT_SELECT_INCIDENTCAT, loginDTO);
		Display[i++][4] = LM.getText(LC.VM_INCIDENT_REPORT_SELECT_VEHICLETYPECAT, loginDTO);
		//Display[i++][4] = LM.getText(LC.VM_INCIDENT_REPORT_SELECT_VEHICLEID, loginDTO);
		Display[i++][4] = LM.getText(LC.VM_INCIDENT_REPORT_SELECT_REGNO, loginDTO);
		Display[i++][4] = LM.getText(LC.VM_INCIDENT_REPORT_SELECT_INCIDENTDATE, loginDTO);
		Display[i++][4] = LM.getText(LC.VM_INCIDENT_REPORT_SELECT_INCIDENTTIME, loginDTO);
		Display[i++][4] = LM.getText(LC.VM_INCIDENT_REPORT_SELECT_DRIVERPHONENUM, loginDTO);
		Display[i++][4] = LM.getText(LC.VM_INCIDENT_REPORT_SELECT_DRIVERNAMEEN, loginDTO);
		Display[i++][4] = LM.getText(LC.VM_INCIDENT_REPORT_SELECT_DRIVERNAMEBN, loginDTO);
		Display[i++][4] = LM.getText(LC.VM_INCIDENT_REPORT_SELECT_INCIDENTPLACE, loginDTO);


		String reportName = LM.getText(LC.VM_INCIDENT_REPORT_OTHER_VM_INCIDENT_REPORT, loginDTO);

		reportRequestHandler = new ReportRequestHandler(null,
				Criteria, Display, GroupBy, OrderBY, sql,
				reportService);


		reportRequestHandler.handleReportGet(request, response, userDTO, "vm_incident_report",
				MenuConstants.VM_INCIDENT_REPORT_DETAILS, language, reportName, "vm_incident_report");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		doGet(request, response);
	}
}
