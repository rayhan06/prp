package cheque_register;

import bill_register.Bill_registerServlet;
import cash_management.Cash_managementServlet;
import common.BaseServlet;
import finance.CashStatusEnum;
import login.LoginDTO;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import permission.MenuConstants;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

@SuppressWarnings({"Duplicates"})
@WebServlet("/Cheque_registerServlet")
@MultipartConfig
public class Cheque_registerServlet extends BaseServlet {
    private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Cheque_registerServlet.class);

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }
        try {
            String actionType = request.getParameter("actionType");
            if ("addToRegister".equals(actionType)) {
                long billRegisterId = Long.parseLong(request.getParameter("billRegisterId"));
                Cheque_registerDTO chequeRegisterDTO = Cheque_registerDAO.getInstance()
                                                                         .getDTOByBillRegisterId(billRegisterId);
                String pageName = chequeRegisterDTO == null
                                  ? "getAddPage"
                                  : "getEditPage&ID=" + chequeRegisterDTO.iD;
                response.sendRedirect("Cheque_registerServlet?actionType=" + pageName + "&billRegisterId=" + billRegisterId);
            } else {
                super.doGet(request, response);
            }
            return;
        } catch (Exception ex) {
            logger.error(ex);
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }

    @Override
    public String getTableName() {
        return Cheque_registerDAO.getInstance().getTableName();
    }

    @Override
    public String getServletName() {
        return "Cheque_registerServlet";
    }

    @Override
    public Cheque_registerDAO getCommonDAOService() {
        return Cheque_registerDAO.getInstance();
    }

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        Cheque_registerDTO cheque_registerDTO;
        SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");

        if (addFlag) {
            cheque_registerDTO = new Cheque_registerDTO();
            cheque_registerDTO.insertedBy = userDTO.employee_record_id;
            cheque_registerDTO.insertionTime = new Date().getTime();
        } else {
            cheque_registerDTO = Cheque_registerDAO.getInstance().getDTOByID(Long.parseLong(request.getParameter("iD")));
        }
        cheque_registerDTO.modifiedBy = userDTO.employee_record_id;
        cheque_registerDTO.lastModificationTime = new Date().getTime();
        cheque_registerDTO.chequeNumber = (Jsoup.clean(request.getParameter("chequeNumber"), Whitelist.simpleText()));
        Date d = f.parse(Jsoup.clean(request.getParameter("chequeDate"), Whitelist.simpleText()));
        cheque_registerDTO.chequeDate = d.getTime();
        cheque_registerDTO.tokenNumber = (Jsoup.clean(request.getParameter("tokenNumber"), Whitelist.simpleText()));
        d = f.parse(Jsoup.clean(request.getParameter("tokenDate"), Whitelist.simpleText()));
        cheque_registerDTO.tokenDate = d.getTime();
        cheque_registerDTO.billRegisterId = Long.parseLong(Jsoup.clean(request.getParameter("billRegisterId"), Whitelist.simpleText()));
        cheque_registerDTO.billDescription = (Jsoup.clean(request.getParameter("billDescription"), Whitelist.simpleText()));
        cheque_registerDTO.billAmount = Long.parseLong(Jsoup.clean(request.getParameter("billAmount"), Whitelist.simpleText()));
        cheque_registerDTO.cashType = Integer.parseInt(Jsoup.clean(request.getParameter("cashType"), Whitelist.simpleText()));
        cheque_registerDTO.chequeAmount = Long.parseLong(Jsoup.clean(request.getParameter("chequeAmount"), Whitelist.simpleText()));
        cheque_registerDTO.supplierName = (Jsoup.clean(request.getParameter("supplierName"), Whitelist.simpleText()));
        cheque_registerDTO.chequeStatusCat = 1;
        cheque_registerDTO.fileDropzone = Long.parseLong(Jsoup.clean(request.getParameter("fileDropzone"), Whitelist.simpleText()));
        if (!addFlag) {
            String fileDropzoneFilesToDelete = request.getParameter("fileDropzoneFilesToDelete");
            String[] deleteArray = fileDropzoneFilesToDelete.split(",");
            for (int i = 0; i < deleteArray.length; i++) {
                System.out.println("going to delete " + deleteArray[i]);
                if (i > 0) {
                    filesDAO.delete(Long.parseLong(deleteArray[i]));
                }
            }
        }


        if (addFlag) {
            getCommonDAOService().add(cheque_registerDTO);
            Cash_managementServlet.addCashRegister(cheque_registerDTO, CashStatusEnum.ACQUISITION, true);
            Cash_managementServlet.addCashRegister(cheque_registerDTO, CashStatusEnum.PAYMENT, true);
        } else {
            getCommonDAOService().update(cheque_registerDTO);
            Cash_managementServlet.addCashRegister(cheque_registerDTO, CashStatusEnum.ACQUISITION, false);
            Cash_managementServlet.addCashRegister(cheque_registerDTO, CashStatusEnum.PAYMENT, false);
        }
        Bill_registerServlet.updateBillRegister(cheque_registerDTO);
        return cheque_registerDTO;
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[]{MenuConstants.CHEQUE_REGISTER_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[]{MenuConstants.CHEQUE_REGISTER_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[]{MenuConstants.CHEQUE_REGISTER_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Cheque_registerServlet.class;
    }
}