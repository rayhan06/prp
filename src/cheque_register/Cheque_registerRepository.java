package cheque_register;

import org.apache.log4j.Logger;
import repository.Repository;
import repository.RepositoryManager;
import util.LockManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


public class Cheque_registerRepository implements Repository {
    Cheque_registerDAO cheque_registerDAO;

    public void setDAO(Cheque_registerDAO cheque_registerDAO) {
        this.cheque_registerDAO = cheque_registerDAO;
    }


    static Logger logger = Logger.getLogger(Cheque_registerRepository.class);
    Map<Long, Cheque_registerDTO> mapOfCheque_registerDTOToiD;


    private Cheque_registerRepository() {
        cheque_registerDAO = Cheque_registerDAO.getInstance();
        mapOfCheque_registerDTOToiD = new ConcurrentHashMap<>();

        RepositoryManager.getInstance().addRepository(this);
    }

    private static class LazyLoader {
        final static Cheque_registerRepository INSTANCE = new Cheque_registerRepository();
    }

    public static Cheque_registerRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

    public void reload(boolean reloadAll) {
        try {
            List<Cheque_registerDTO> cheque_registerDTOs = Cheque_registerDAO.getInstance().getAllCheque_registerDTO_details(reloadAll);
            for (Cheque_registerDTO cheque_registerDTO : cheque_registerDTOs) {
                Cheque_registerDTO oldCheque_registerDTO = getCheque_registerDTOByID(cheque_registerDTO.iD);
                if (oldCheque_registerDTO != null) {
                    mapOfCheque_registerDTOToiD.remove(oldCheque_registerDTO.iD);
                }
                if (cheque_registerDTO.isDeleted == 0) {

                    mapOfCheque_registerDTOToiD.put(cheque_registerDTO.iD, cheque_registerDTO);

                }
            }

        } catch (Exception e) {
            logger.debug("FATAL", e);
        }
    }

    public List<Cheque_registerDTO> getCheque_registerList() {
        return new ArrayList<>(this.mapOfCheque_registerDTOToiD.values());
    }


    public Cheque_registerDTO getCheque_registerDTOByID(long ID) {
        if (mapOfCheque_registerDTOToiD.get(ID) == null) {
            synchronized (LockManager.getLock(ID + "CRR")) {
                Cheque_registerDTO dto = cheque_registerDAO.getDTOByID(ID);
                if (dto != null)
                    mapOfCheque_registerDTOToiD.put(dto.iD, dto);
            }
        }
        return mapOfCheque_registerDTOToiD.get(ID);
    }


    @Override
    public String getTableName() {
        return "cheque_register";
    }
}


