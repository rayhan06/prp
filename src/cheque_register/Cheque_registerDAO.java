package cheque_register;

import common.CommonDAOService;
import common.ConnectionAndStatementUtil;
import org.apache.log4j.Logger;
import travel_allowance.Travel_allowanceDAO;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings({"Duplicates"})
public class Cheque_registerDAO implements CommonDAOService<Cheque_registerDTO> {

    private static final Logger logger = Logger.getLogger(Travel_allowanceDAO.class);


    private static final String addSqlQuery = "INSERT INTO {tableName} (cheque_number,cheque_date,token_number,token_date,bill_register_id,bill_description,bill_amount,cheque_amount,supplier_name,cheque_status_cat,cash_type,file_dropzone,"
            .concat("modified_by,search_column,lastModificationTime,inserted_by,insertion_time,isDeleted,ID)")
            .concat(" VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
    private static final String updateSqlQuery = "UPDATE {tableName} SET cheque_number=?,cheque_date=?,token_number=?,token_date=?,bill_register_id=?,bill_description=?,bill_amount=?,cheque_amount=?,supplier_name=?,cheque_status_cat=?,cash_type=?, file_dropzone=?,"
            .concat("modified_by=?,search_column=?,lastModificationTime = ? WHERE ID = ?");
    private static final Map<String, String> searchMap = new HashMap<>();

    private Cheque_registerDAO() {
        searchMap.put("cheque_number", " AND (cheque_number like ?)");
        searchMap.put("bill_number", " AND (bill_register_id like ?)");
        searchMap.put("supplier_name", " AND (supplier_name like ?)");
        searchMap.put("AnyField", " AND (search_column LIKE ?)");
    }

    private static class LazyLoader {
        static final Cheque_registerDAO INSTANCE = new Cheque_registerDAO();
    }

    public static Cheque_registerDAO getInstance() {
        return Cheque_registerDAO.LazyLoader.INSTANCE;
    }


    public void setSearchColumn(Cheque_registerDTO cheque_registerDTO) {
        cheque_registerDTO.searchColumn = "";
        cheque_registerDTO.searchColumn += cheque_registerDTO.chequeNumber + " ";
        cheque_registerDTO.searchColumn += cheque_registerDTO.tokenNumber + " ";
        cheque_registerDTO.searchColumn += cheque_registerDTO.billRegisterId + " ";
        cheque_registerDTO.searchColumn += cheque_registerDTO.supplierName + " ";
    }


    @Override
    public void set(PreparedStatement ps, Cheque_registerDTO cheque_registerDTO, boolean isInsert) throws SQLException {
        int index = 0;

        ps.setObject(++index, cheque_registerDTO.chequeNumber);
        ps.setObject(++index, cheque_registerDTO.chequeDate);
        ps.setObject(++index, cheque_registerDTO.tokenNumber);
        ps.setObject(++index, cheque_registerDTO.tokenDate);
        ps.setObject(++index, cheque_registerDTO.billRegisterId);
        ps.setObject(++index, cheque_registerDTO.billDescription);
        ps.setObject(++index, cheque_registerDTO.billAmount);
        ps.setObject(++index, cheque_registerDTO.chequeAmount);
        ps.setObject(++index, cheque_registerDTO.supplierName);
        ps.setObject(++index, cheque_registerDTO.chequeStatusCat);
        ps.setObject(++index, cheque_registerDTO.cashType);
        ps.setObject(++index, cheque_registerDTO.fileDropzone);

        ps.setObject(++index, cheque_registerDTO.modifiedBy);

        setSearchColumn(cheque_registerDTO);
        ps.setObject(++index, cheque_registerDTO.searchColumn);
        ps.setObject(++index, cheque_registerDTO.lastModificationTime);
        if (isInsert) {
            ps.setObject(++index, cheque_registerDTO.insertedBy);
            ps.setObject(++index, cheque_registerDTO.insertionTime);
            ps.setObject(++index, 0);
        }
        ps.setObject(++index, cheque_registerDTO.iD);
    }

    @Override
    public Cheque_registerDTO buildObjectFromResultSet(ResultSet rs) {

        try {
            Cheque_registerDTO cheque_registerDTO = new Cheque_registerDTO();
            cheque_registerDTO.iD = rs.getLong("ID");
            cheque_registerDTO.chequeNumber = rs.getString("cheque_number");
            cheque_registerDTO.chequeDate = rs.getLong("cheque_date");
            cheque_registerDTO.tokenNumber = rs.getString("token_number");
            cheque_registerDTO.tokenDate = rs.getLong("token_date");
            cheque_registerDTO.billRegisterId = rs.getLong("bill_register_Id");
            cheque_registerDTO.billDescription = rs.getString("bill_description");
            cheque_registerDTO.billAmount = rs.getLong("bill_amount");
            cheque_registerDTO.chequeAmount = rs.getLong("cheque_amount");
            cheque_registerDTO.supplierName = rs.getString("supplier_name");
            cheque_registerDTO.chequeStatusCat = rs.getInt("cheque_status_cat");
            cheque_registerDTO.cashType = rs.getInt("cash_type");
            cheque_registerDTO.fileDropzone = rs.getLong("file_dropzone");
            cheque_registerDTO.insertionTime = rs.getLong("insertion_time");
            cheque_registerDTO.insertedBy = rs.getLong("inserted_by");
            cheque_registerDTO.modifiedBy = rs.getLong("modified_by");
            cheque_registerDTO.searchColumn = rs.getString("search_column");
            cheque_registerDTO.isDeleted = rs.getInt("isDeleted");
            cheque_registerDTO.lastModificationTime = rs.getLong("lastModificationTime");
            return cheque_registerDTO;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    @Override
    public String getTableName() {
        return "cheque_register";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    @Override
    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Cheque_registerDTO) commonDTO, addSqlQuery, true);
    }

    @Override
    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Cheque_registerDTO) commonDTO, updateSqlQuery, false);
    }

    public Cheque_registerDTO getDTOByID(long ID) {
        return getDTOFromID(ID);
    }

    public List<Cheque_registerDTO> getAllCheque_registerDTO_details(boolean isFirstReload) {
        return getAllDTOs(isFirstReload);
    }

    private static final String getByBillRegisterId = "SELECT * FROM cheque_register WHERE bill_register_id=%d AND isDeleted=0";

    public Cheque_registerDTO getDTOByBillRegisterId(long billRegisterId) {
        return ConnectionAndStatementUtil.getT(
                String.format(getByBillRegisterId, billRegisterId),
                this::buildObjectFromResultSet
        );
    }
}
	