package cheque_register;

import util.CommonDTO;


public class Cheque_registerDTO extends CommonDTO {

    public String chequeNumber = "";
    public long chequeDate = System.currentTimeMillis();
    public String tokenNumber = "";
    public long tokenDate = System.currentTimeMillis();
    public long billRegisterId = -1;
    public String billDescription = "";
    public long billAmount = 0;
    public long chequeAmount = 0;
    public int cashType=0;
    public String supplierName = "";
    public int chequeStatusCat = 0;
    public long fileDropzone = 0;
    public long insertedBy = 0;
    public long modifiedBy = 0;
    public long insertionTime = 0;


    @Override
    public String toString() {
        return "$Cheque_registerDTO[" +
                " iD = " + iD +
                " chequeNumber = " + chequeNumber +
                " chequeDate = " + chequeDate +
                " tokenNumber = " + tokenNumber +
                " tokenDate = " + tokenDate +
                " billNumber = " + billRegisterId +
                " billDescription = " + billDescription +
                " billAmount = " + billAmount +
                " supplierName = " + supplierName +
                " chequeStatusCat = " + chequeStatusCat +
                " fileDropzone = " + fileDropzone +
                " searchColumn = " + searchColumn +
                " insertedBy = " + insertedBy +
                " lastModificationTime = " + lastModificationTime +
                " isDeleted = " + isDeleted +
                " modifiedBy = " + modifiedBy +
                "]";
    }

}