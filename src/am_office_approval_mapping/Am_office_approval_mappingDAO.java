package am_office_approval_mapping;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;

import am_office_assignment_request.AmOfficeAssignmentRequestStatus;
import card_info.*;
import common.CommonDAOService;

import employee_offices.EmployeeOfficeDTO;
import employee_offices.EmployeeOfficeRepository;

import java.sql.SQLException;
import java.util.stream.Collectors;

import common.ConnectionAndStatementUtil;
import fund_management.FundApplicationStatus;
import org.apache.log4j.Logger;

import task_type_approval_path.TaskTypeApprovalPathDTO;
import task_type_approval_path.TaskTypeApprovalPathRepository;
import util.*;
import pb.*;

import static java.util.stream.Collectors.toList;


@SuppressWarnings({"Duplicates", "SameParameterValue"})

public class Am_office_approval_mappingDAO implements CommonDAOService<Am_office_approval_mappingDTO>
{

	private static final Logger logger = Logger.getLogger(Am_office_approval_mappingDAO.class);

	private static final String addSqlQuery = "INSERT INTO {tableName} (am_office_assignment_request_id, card_approval_id, sequence, am_office_assignment_status_cat,"
			.concat("task_type_id, employee_records_id, approver_employee_records_id,search_column,")
			.concat("modified_by, lastModificationTime, comment, inserted_by, insertion_date, isDeleted, ID)")
			.concat("VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

	private static final String updateSqlQuery = "UPDATE {tableName} SET am_office_assignment_request_id = ?, card_approval_id = ?, sequence = ?,"
			.concat("am_office_assignment_status_cat = ?, task_type_id = ?, employee_records_id = ?, approver_employee_records_id = ?,")
			.concat("search_column = ?,modified_by = ?, lastModificationTime = ?, comment = ? WHERE ID = ?");

	private static final String getByAmOfficeAssignmentRequestId =
			"SELECT * FROM am_office_approval_mapping WHERE am_office_assignment_request_id = %d AND isDeleted = 0 ORDER BY sequence DESC";

	private static final String getByAmOfficeAssignmentRequestAndApproverRecordsId =
			"SELECT * FROM am_office_approval_mapping WHERE am_office_assignment_request_id = %d AND approver_employee_records_id = %d";

	private static final String getByAmOfficeAssignmentRequestIdAndStatus =
			"SELECT * FROM am_office_approval_mapping WHERE am_office_assignment_request_id = %d AND am_office_assignment_status_cat = %d AND isDeleted = 0";

	private static final String updateStatus =
			"UPDATE am_office_approval_mapping SET am_office_assignment_status_cat = %d,isDeleted = %d,modified_by = %d,lastModificationTime = %d WHERE ID IN (%s)";

	private static final String updateStatusByAmOfficeRequestId =
			"UPDATE am_office_approval_mapping SET am_office_assignment_status_cat = ?, comment = ?,isDeleted =?," +
					"modified_by = ?,lastModificationTime = ? WHERE am_office_assignment_status_cat = ? AND am_office_assignment_request_id = ?";   // TODO AM OFFICE COMMENT

	private static final Map<String, String> searchMap = new HashMap<>();

	private Am_office_approval_mappingDAO() {
		searchMap.put("employeeRecordsId", " AND (employee_records_id = ?)");
		searchMap.put("approverEmployeeRecordsId", " AND (approver_employee_records_id = ?)");
		searchMap.put("amOfficeAssignmentStatusCat", " AND (am_office_assignment_status_cat = ?)");
	}

	private static class LazyLoader {
		static final Am_office_approval_mappingDAO INSTANCE = new Am_office_approval_mappingDAO();
	}

	public static Am_office_approval_mappingDAO getInstance() {
		return Am_office_approval_mappingDAO.LazyLoader.INSTANCE;
	}

	public void setSearchColumn(Am_office_approval_mappingDTO am_office_approval_mappingDTO)
	{
		am_office_approval_mappingDTO.searchColumn = "";
		am_office_approval_mappingDTO.searchColumn += CatDAO.getName("English", "am_office_assignment_status", am_office_approval_mappingDTO.amOfficeAssignmentStatusCat) + " " + CatDAO.getName("Bangla", "am_office_assignment_status", am_office_approval_mappingDTO.amOfficeAssignmentStatusCat) + " ";
		am_office_approval_mappingDTO.searchColumn += am_office_approval_mappingDTO.insertedBy + " ";
		am_office_approval_mappingDTO.searchColumn += am_office_approval_mappingDTO.modifiedBy + " ";
	}

	@Override
	public void set(PreparedStatement ps, Am_office_approval_mappingDTO am_office_approval_mappingDTO, boolean isInsert) throws SQLException {
		setSearchColumn(am_office_approval_mappingDTO);
		int index = 0;

		ps.setObject(++index,am_office_approval_mappingDTO.amOfficeAssignmentRequestId);
		ps.setObject(++index,am_office_approval_mappingDTO.cardApprovalId);
		ps.setObject(++index,am_office_approval_mappingDTO.sequence);
		ps.setObject(++index,am_office_approval_mappingDTO.amOfficeAssignmentStatusCat);
		ps.setObject(++index,am_office_approval_mappingDTO.taskTypeId);
		ps.setObject(++index,am_office_approval_mappingDTO.employeeRecordsId);
		ps.setObject(++index,am_office_approval_mappingDTO.approverEmployeeRecordsId);
		ps.setObject(++index,am_office_approval_mappingDTO.searchColumn);
		ps.setObject(++index,am_office_approval_mappingDTO.modifiedBy);
		ps.setLong(++index, am_office_approval_mappingDTO.lastModificationTime);
		ps.setObject(++index, am_office_approval_mappingDTO.comment);

		if(isInsert)
		{
			ps.setObject(++index,am_office_approval_mappingDTO.insertedBy);
			ps.setObject(++index,am_office_approval_mappingDTO.insertionDate);
			ps.setObject(++index, 0);
		}

		ps.setObject(++index, am_office_approval_mappingDTO.iD);
	}

	@Override
	public Am_office_approval_mappingDTO buildObjectFromResultSet(ResultSet rs) {
		try
		{
			Am_office_approval_mappingDTO am_office_approval_mappingDTO = new Am_office_approval_mappingDTO();

			am_office_approval_mappingDTO.iD = rs.getLong("ID");
			am_office_approval_mappingDTO.amOfficeAssignmentRequestId = rs.getLong("am_office_assignment_request_id");
			am_office_approval_mappingDTO.cardApprovalId = rs.getLong("card_approval_id");
			am_office_approval_mappingDTO.sequence = rs.getInt("sequence");
			am_office_approval_mappingDTO.amOfficeAssignmentStatusCat = rs.getInt("am_office_assignment_status_cat");
			am_office_approval_mappingDTO.taskTypeId = rs.getLong("task_type_id");
			am_office_approval_mappingDTO.employeeRecordsId = rs.getLong("employee_records_id");
			am_office_approval_mappingDTO.approverEmployeeRecordsId = rs.getLong("approver_employee_records_id");
			am_office_approval_mappingDTO.insertionDate = rs.getLong("insertion_date");
			am_office_approval_mappingDTO.insertedBy = rs.getString("inserted_by");
			am_office_approval_mappingDTO.modifiedBy = rs.getString("modified_by");
			am_office_approval_mappingDTO.isDeleted = rs.getInt("isDeleted");
			am_office_approval_mappingDTO.lastModificationTime = rs.getLong("lastModificationTime");
			am_office_approval_mappingDTO.searchColumn = rs.getString("search_column");
			am_office_approval_mappingDTO.comment = rs.getString("comment");
			return am_office_approval_mappingDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}

	private Am_office_approval_mappingDTO buildDTO(long approverEmployeeRecordId, AmOfficeApprovalModel model, int level,
											  int isDeleted, AmOfficeAssignmentRequestStatus amOfficeAssignmentRequestStatus) {
		Am_office_approval_mappingDTO dto = new Am_office_approval_mappingDTO();
		CardApprovalDTO cardApprovalDTO = CardApprovalRepository.getInstance()
				.getByEmployeeRecordId(approverEmployeeRecordId, model.getRequesterEmployeeRecordId());
		if (cardApprovalDTO == null) {
			return null;
		}

		dto.amOfficeAssignmentRequestId = model.getAmOfficeAssignmentRequestDTO().iD;

		dto.amOfficeAssignmentStatusCat = amOfficeAssignmentRequestStatus.getValue();
		dto.cardApprovalId = cardApprovalDTO.iD;
		dto.sequence = level;
		dto.taskTypeId = model.getTaskTypeId();
		dto.insertedBy = dto.modifiedBy = String.valueOf(model.getRequesterEmployeeRecordId());
		dto.insertionDate = dto.lastModificationTime = System.currentTimeMillis();
		dto.isDeleted = isDeleted;
		dto.employeeRecordsId = model.getAmOfficeAssignmentRequestDTO().requesterEmpId;
		dto.approverEmployeeRecordsId = cardApprovalDTO.employeeRecordId;

		return dto;
	}



	@Override
	public String getTableName() {
		return "am_office_approval_mapping";
	}

	@Override
	public Map<String, String> getSearchMap() {
		return searchMap;
	}

	@Override
	public long add(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Am_office_approval_mappingDTO) commonDTO, addSqlQuery, true);
	}

	@Override
	public long update(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Am_office_approval_mappingDTO) commonDTO, updateSqlQuery, false);
	}

	public CardApprovalResponse createAmOfficeApproval(AmOfficeApprovalModel model) throws Exception {
		String sql = String.format(getByAmOfficeAssignmentRequestId, model.getAmOfficeAssignmentRequestDTO().iD);
		Am_office_approval_mappingDTO approvalMappingDTO = ConnectionAndStatementUtil.getT(sql, this::buildObjectFromResultSet);
		if (approvalMappingDTO != null) {
			throw new DuplicateCardInfoException("Approval is already created for " + model.getAmOfficeAssignmentRequestDTO().iD);
		}

//		CardApprovalResponse cardApprovalResponse = new CardApprovalResponse();
//		EmployeeOfficeDTO employeeOfficeDTO = EmployeeOfficeRepository.getInstance()
//				.getByEmployeeRecordIdIsDefault(model.getRequesterEmployeeRecordId());
//		OfficeUnitOrganograms officeUnitOrganograms = OfficeUnitOrganogramsRepository.getInstance()
//				.getById(employeeOfficeDTO.officeUnitOrganogramId);
//
//		TaskTypeApprovalPathDTO taskTypeApprovalPathDTO = new TaskTypeApprovalPathDTO();
//		taskTypeApprovalPathDTO.officeUnitOrganogramId = officeUnitOrganograms.superior_designation_id;
//		if (taskTypeApprovalPathDTO.officeUnitOrganogramId == 0) {
//			logger.debug("taskTypeApprovalPathDTO.officeUnitOrganogramId = 0 (No Superior); For that card approval direct moved to next level");
//			return movedToNextLevel(model, 2);
//		} else {
//			cardApprovalResponse.hasNextApproval = true;
//			cardApprovalResponse.organogramIds = addToApprovalAndGetAddedOrganogramIds(Collections.singletonList(taskTypeApprovalPathDTO), model, 1);
//		}

		return movedToNextLevel(model, 1);

		//return cardApprovalResponse;
	}

	private CardApprovalResponse movedToNextLevel(AmOfficeApprovalModel model, int nextLevel) {
		CardApprovalResponse response = new CardApprovalResponse();
		List<TaskTypeApprovalPathDTO> nextApprovalPath = TaskTypeApprovalPathRepository.getInstance()
				.getByTaskTypeIdAndLevel(model.getTaskTypeId(), nextLevel);

		if (nextApprovalPath == null || nextApprovalPath.size() == 0) {
			response.hasNextApproval = false;
			response.organogramIds = null;
		} else {
			List<Long> organogramIds = addToApprovalAndGetAddedOrganogramIds(nextApprovalPath, model, nextLevel);
			response.hasNextApproval = true;
			response.organogramIds = organogramIds;
		}
		return response;
	}

	private List<Long> addToApprovalAndGetAddedOrganogramIds(List<TaskTypeApprovalPathDTO> nextApprovalPath, AmOfficeApprovalModel model, int level) {
		List<Long> addedToApproval = new ArrayList<>();
		nextApprovalPath.forEach(approver -> {
			EmployeeOfficeDTO approverOfficeDTO = EmployeeOfficeRepository.getInstance().getByOfficeUnitOrganogramId(approver.officeUnitOrganogramId);
			if (approverOfficeDTO != null) {
				Am_office_approval_mappingDTO dto = buildDTO(
						approverOfficeDTO.employeeRecordId, model, level,
						0, AmOfficeAssignmentRequestStatus.WAITING_FOR_APPROVAL
				);
				if (dto != null) {
					try {
						add(dto);
						addedToApproval.add(approverOfficeDTO.officeUnitOrganogramId);
					} catch (Exception e) {
						logger.error(e);
					}
				}
			}
		});
		return addedToApproval;
	}

	public Am_office_approval_mappingDTO getByAmOfficeAssignmentRequestAndApproverRecordsId(long amOfficeAssignmentRequestId, long approverEmployeeId) {
		String sql = String.format(getByAmOfficeAssignmentRequestAndApproverRecordsId, amOfficeAssignmentRequestId, approverEmployeeId);
		return ConnectionAndStatementUtil.getT(sql, this::buildObjectFromResultSet);
	}

	private static class ValidApprovalInfo {
		List<Am_office_approval_mappingDTO> dtoList;
		int currentLevel;
		Am_office_approval_mappingDTO approvalMappingDTO;

		public ValidApprovalInfo(List<Am_office_approval_mappingDTO> dtoList, int currentLevel, Am_office_approval_mappingDTO approvalMappingDTO) {
			this.dtoList = dtoList;
			this.currentLevel = currentLevel;
			this.approvalMappingDTO = approvalMappingDTO;
		}
	}

	public ValidApprovalInfo validateInput(AmOfficeApprovalModel model,int currentStatus) throws InvalidDataException, AlreadyApprovedException {
		String sql = String.format(
				getByAmOfficeAssignmentRequestIdAndStatus,
				model.getAmOfficeAssignmentRequestDTO().iD,
				currentStatus
		);

		List<Am_office_approval_mappingDTO> dtoList = ConnectionAndStatementUtil.getListOfT(sql, this::buildObjectFromResultSet);
		if (dtoList == null || dtoList.size() == 0) {
			throw new InvalidDataException("No Pending approval is found for requestId : " + model.getAmOfficeAssignmentRequestDTO().iD);
		}

		Am_office_approval_mappingDTO requesterApprovalDTO =
				dtoList.stream()
						.filter(dto -> dto.approverEmployeeRecordsId == model.getRequesterEmployeeRecordId())
						.findAny()
						.orElse(null);

		if (requesterApprovalDTO == null) {
			sql = String.format(getByAmOfficeAssignmentRequestAndApproverRecordsId, model.getAmOfficeAssignmentRequestDTO().iD, model.getRequesterEmployeeRecordId());
			requesterApprovalDTO = ConnectionAndStatementUtil.getT(sql, this::buildObjectFromResultSet);
			if (requesterApprovalDTO != null) {
				throw new AlreadyApprovedException();
			}
			throw new InvalidDataException("Invalid employee request to approve");
		}

		Set<Integer> sequenceValueSet = dtoList.stream()
				.map(e -> e.sequence)
				.collect(Collectors.toSet());
		if (sequenceValueSet.size() > 1) {
			throw new InvalidDataException(
					"Multiple sequence value is found for Pending approval of requestID : "
							+ model.getAmOfficeAssignmentRequestDTO().iD
			);
		}
		return new ValidApprovalInfo(dtoList, (Integer) sequenceValueSet.toArray()[0], requesterApprovalDTO);

	}

	public CardApprovalResponse movedToNextLevelOfApproval(AmOfficeApprovalModel model) throws InvalidDataException, AlreadyApprovedException {
		synchronized (LockManager.getLock(model.getAmOfficeAssignmentRequestDTO().iD + "AMMTNLA")) {
			ValidApprovalInfo validApprovalInfo = validateInput(model,AmOfficeAssignmentRequestStatus.WAITING_FOR_APPROVAL.getValue());

			String idsToUpdate =
					validApprovalInfo.dtoList.stream()
							.map(e -> String.valueOf(e.iD))
							.collect(Collectors.joining(","));

			String sql = String.format(
					updateStatus, AmOfficeAssignmentRequestStatus.APPROVED.getValue(),
					0, model.getRequesterEmployeeRecordId(), System.currentTimeMillis(), idsToUpdate
			);

			boolean res = (Boolean) ConnectionAndStatementUtil.getWriteStatement(st -> {
				try {
					st.executeUpdate(sql);
					return true;
				} catch (SQLException ex) {
					logger.error(ex);
					return false;
				}
			});
			if (!res) {
				throw new InvalidDataException("Exception occurred during updating approval status");
			}
			return movedToNextLevel(model, validApprovalInfo.currentLevel + 1);
		}
	}

	public boolean rejectPendingApproval(AmOfficeApprovalModel model, String rejectReason) throws InvalidDataException, AlreadyApprovedException {
		synchronized (LockManager.getLock(model.getAmOfficeAssignmentRequestDTO().iD + "AMDRejectPA")) {
			validateInput(model,AmOfficeAssignmentRequestStatus.WAITING_FOR_APPROVAL.getValue());
			return (Boolean) ConnectionAndStatementUtil.getWritePrepareStatement(ps -> {
				try {
					int index = 0;

					ps.setInt(++index, AmOfficeAssignmentRequestStatus.REJECTED.getValue());
					ps.setString(++index, rejectReason);
					ps.setInt(++index, 0);
					ps.setLong(++index, model.getRequesterEmployeeRecordId());
					ps.setLong(++index, System.currentTimeMillis());
					ps.setInt(++index, AmOfficeAssignmentRequestStatus.WAITING_FOR_APPROVAL.getValue());
					ps.setObject(++index, model.getAmOfficeAssignmentRequestDTO().iD);

					logger.info(ps);
					ps.executeUpdate();
					return true;
				} catch (SQLException ex) {
					logger.error(ex);
					return false;
				}
			}, updateStatusByAmOfficeRequestId);
		}
	}

	public boolean withdrawApprovedApproval(AmOfficeApprovalModel model, String withdrawReason) throws InvalidDataException, AlreadyApprovedException {
		synchronized (LockManager.getLock(model.getAmOfficeAssignmentRequestDTO().iD + "AMDWithdrawPA")) {
			validateInput(model,AmOfficeAssignmentRequestStatus.APPROVED.getValue());
			return (Boolean) ConnectionAndStatementUtil.getWritePrepareStatement(ps -> {
				try {
					int index = 0;

					ps.setInt(++index, AmOfficeAssignmentRequestStatus.WITHDRAWN.getValue());
					ps.setString(++index, withdrawReason);
					ps.setInt(++index, 0);
					ps.setLong(++index, model.getRequesterEmployeeRecordId());
					ps.setLong(++index, System.currentTimeMillis());
					ps.setInt(++index, AmOfficeAssignmentRequestStatus.APPROVED.getValue());
					ps.setObject(++index, model.getAmOfficeAssignmentRequestDTO().iD);

					logger.info(ps);
					ps.executeUpdate();
					return true;
				} catch (SQLException ex) {
					logger.error(ex);
					return false;
				}
			}, updateStatusByAmOfficeRequestId);
		}
	}

	public List<Am_office_approval_mappingDTO> getAllApprovalDTOByAmOfficeRequestId(long amOfficeRequestId) {
		String sql = String.format(getByAmOfficeAssignmentRequestId, amOfficeRequestId);
		return ConnectionAndStatementUtil.getListOfT(sql, this::buildObjectFromResultSet)
				.stream()
				.filter(dto -> dto.amOfficeAssignmentStatusCat == AmOfficeAssignmentRequestStatus.WAITING_FOR_APPROVAL.getValue()
						|| String.valueOf(dto.approverEmployeeRecordsId).equals(dto.modifiedBy))
				.collect(toList());
	}



}
	