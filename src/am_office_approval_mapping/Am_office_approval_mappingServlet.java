package am_office_approval_mapping;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;

import am_minister_hostel_level.Am_minister_hostel_levelDAO;
import am_minister_hostel_level.Am_minister_hostel_levelDTO;
import am_minister_hostel_level.Am_minister_hostel_levelRepository;
import am_office_assignment.Am_office_assignmentDAO;
import am_office_assignment.Am_office_assignmentDTO;
import am_office_assignment_request.AmOfficeAssignmentRequestStatus;
import am_office_assignment_request.Am_office_assignment_requestDAO;
import am_office_assignment_request.Am_office_assignment_requestDTO;
import am_parliament_building_room.Am_parliament_building_roomDAO;
import am_parliament_building_room.Am_parliament_building_roomDTO;
import am_parliament_building_room.Am_parliament_building_roomRepository;
import card_info.CardApprovalResponse;
import common.BaseServlet;
import common.CommonDAOService;
import employee_assign.EmployeeSearchModalUtil;
import employee_assign.EmployeeSearchModel;
import office_units.Office_unitsRepository;
import offices.OfficesDTO;
import offices.OfficesRepository;
import org.apache.log4j.Logger;
import login.LoginDTO;
import permission.MenuConstants;
import sessionmanager.SessionConstants;
import task_type.TaskTypeEnum;
import user.UserDTO;
import user.UserRepository;
import util.*;

import java.util.*;
import javax.servlet.http.*;

import common.ApiResponse;
import pb.*;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import vm_requisition.CommonApprovalStatus;


/**
 * Servlet implementation class Am_office_approval_mappingServlet
 */
@WebServlet("/Am_office_approval_mappingServlet")
@MultipartConfig
public class Am_office_approval_mappingServlet extends BaseServlet {
    private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Am_office_approval_mappingServlet.class);

    @Override
    public String getTableName() {
        return Am_office_approval_mappingDAO.getInstance().getTableName();
    }

    @Override
    public String getServletName() {
        return "am_office_approval_mapping";
    }

    @Override
    public CommonDAOService<? extends CommonDTO> getCommonDAOService() {
        return Am_office_approval_mappingDAO.getInstance();
    }

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        return null;
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[]{MenuConstants.AM_OFFICE_APPROVAL_MAPPING_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[]{MenuConstants.AM_OFFICE_APPROVAL_MAPPING_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[]{MenuConstants.AM_OFFICE_APPROVAL_MAPPING_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Am_office_approval_mappingServlet.class;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }

        try {
            String actionType = request.getParameter("actionType");
            switch (actionType) {
                case "search":
                    if (Utils.checkPermission(String.valueOf(userDTO.employee_record_id), userDTO, MenuConstants.AM_OFFICE_APPROVAL_MAPPING_SEARCH)) {
                        Map<String, String> extraCriteriaMap = new HashMap<>();
                        extraCriteriaMap.put("approverEmployeeRecordsId", String.valueOf(userDTO.employee_record_id));
                        //extraCriteriaMap.put("amOfficeAssignmentStatusCat", String.valueOf(CommonApprovalStatus.PENDING.getValue()));
                        request.setAttribute(CommonDAOService.CRITERIA_MAP_KEY, extraCriteriaMap);
                        search(request, response);
                        return;
                    }
                    break;
                case "getApprovalPage":
                    long amOfficeAssignmentRequestId = Long.parseLong(request.getParameter("amOfficeAssignmentRequestId"));
                    setApprovalPageData(amOfficeAssignmentRequestId, request, userDTO);
                    request.getRequestDispatcher("am_office_assignment_request/am_office_assignment_requestViewAdmin.jsp").forward(request, response);
                    return;
                default:
                    super.doGet(request, response);
                    return;
            }
        } catch (Exception ex) {
            logger.error(ex);
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }

        try {
            String actionType = request.getParameter("actionType");
            switch (actionType) {

                case "withdrawAllocation":
                    if (Utils.checkPermission(String.valueOf(userDTO.employee_record_id), userDTO, MenuConstants.AM_OFFICE_APPROVAL_MAPPING_SEARCH)) {
                        withdraw(request, userDTO);
                        response.sendRedirect("Am_office_approval_mappingServlet?actionType=search");
                        return;
                    }
                    break;

                case "rejectApplication":
                    if (Utils.checkPermission(String.valueOf(userDTO.employee_record_id), userDTO, MenuConstants.AM_OFFICE_APPROVAL_MAPPING_SEARCH)) {
                        approve(request, userDTO, false);
                        response.sendRedirect("Am_office_approval_mappingServlet?actionType=search");
                        return;
                    }
                    break;
                case "approveApplication":
                    if (Utils.checkPermission(String.valueOf(userDTO.employee_record_id), userDTO, MenuConstants.AM_OFFICE_APPROVAL_MAPPING_SEARCH)) {
                        approve(request, userDTO, true);
                        try {
                            //ApiResponse.sendSuccessResponse(response, "Success");
                            ApiResponse.sendSuccessResponse(response, "Am_office_assignmentServlet?actionType=search");
                        } catch (Exception ex) {
                            logger.error(ex);
                            ApiResponse.sendErrorResponse(response, ex.getMessage());
                        }
                        //response.sendRedirect("Am_office_approval_mappingServlet?actionType=search");
                        return;
                    }
            }
        } catch (Exception ex) {
            logger.error(ex);
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }

    private void setApprovalPageData(long amOfficeAssignmentRequestId, HttpServletRequest request, UserDTO userDTO) throws Exception {
        Am_office_assignment_requestDTO am_office_assignment_requestDTO = Am_office_assignment_requestDAO.getInstance().getDTOByID(amOfficeAssignmentRequestId);
        if (am_office_assignment_requestDTO == null)
            throw new Exception("No application for office assignment is not found with id = " + amOfficeAssignmentRequestId);
        request.setAttribute("am_office_assignment_requestDTO", am_office_assignment_requestDTO);

        Am_office_approval_mappingDTO approverAmOfficeAssignmentRequestDTO =
                Am_office_approval_mappingDAO.getInstance().getByAmOfficeAssignmentRequestAndApproverRecordsId(amOfficeAssignmentRequestId, userDTO.employee_record_id);

        if (approverAmOfficeAssignmentRequestDTO == null)
            throw new Exception("User Not allowed to approve  for office with  id = " + amOfficeAssignmentRequestId);
        request.setAttribute("approverAmOfficeAssignmentRequestDTO", approverAmOfficeAssignmentRequestDTO);
    }

    private void approve(HttpServletRequest request, UserDTO userDTO, boolean isAccepted) throws Exception {
        long requesterEmployeeRecordId = userDTO.employee_record_id;
        long amOfficeAssignmentRequestId = Long.parseLong(request.getParameter("amOfficeAssignmentRequestId"));
        Am_office_assignment_requestDTO amOfficeAssignmentRequestDTO = Am_office_assignment_requestDAO.getInstance().getDTOByID(amOfficeAssignmentRequestId);

        AmOfficeApprovalModel model =
                new AmOfficeApprovalModel.AmOfficeApprovalModelBuilder()
                        .setAmOfficeAssignmentRequestDTO(amOfficeAssignmentRequestDTO)
                        .setRequesterEmployeeRecordId(userDTO.employee_record_id)
                        .setTaskTypeId(TaskTypeEnum.AM_OFFICE_ASSIGNMENT.getValue())
                        .build();

        EmployeeSearchModel employeeSearchModel = (gson.fromJson
                (EmployeeSearchModalUtil.getEmployeeSearchModelJson
                                (userDTO.employee_record_id,
                                        userDTO.unitID,
                                        userDTO.organogramID),
                        EmployeeSearchModel.class)
        );

        boolean sendNotificationToUser = false;
        if (isAccepted) {
            CardApprovalResponse approvalResponse = Am_office_approval_mappingDAO.getInstance().movedToNextLevelOfApproval(model);
            if (!approvalResponse.hasNextApproval) {

                amOfficeAssignmentRequestDTO.approverEmpId = employeeSearchModel.employeeRecordId;
                amOfficeAssignmentRequestDTO.approverOfficeUnitId = employeeSearchModel.officeUnitId;
                amOfficeAssignmentRequestDTO.approverOrgId = employeeSearchModel.organogramId;

                amOfficeAssignmentRequestDTO.approverNameEn = employeeSearchModel.employeeNameEn;
                amOfficeAssignmentRequestDTO.approverNameBn = employeeSearchModel.employeeNameBn;
                amOfficeAssignmentRequestDTO.approverOfficeUnitNameEn = employeeSearchModel.officeUnitNameEn;
                amOfficeAssignmentRequestDTO.approverOfficeUnitNameBn = employeeSearchModel.officeUnitNameBn;
                amOfficeAssignmentRequestDTO.approverOfficeUnitOrgNameEn = employeeSearchModel.organogramNameEn;
                amOfficeAssignmentRequestDTO.approverOfficeUnitOrgNameBn = employeeSearchModel.organogramNameBn;

                amOfficeAssignmentRequestDTO.approverPhoneNum = employeeSearchModel.phoneNumber;

                OfficesDTO approverOffice = OfficesRepository.getInstance().getOfficesDTOByID(amOfficeAssignmentRequestDTO.approverOfficeId);

                if (approverOffice != null) {
                    amOfficeAssignmentRequestDTO.approverOfficeId = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(employeeSearchModel.officeUnitId).officeId;

                    amOfficeAssignmentRequestDTO.approverOfficeNameEn = approverOffice.officeNameEng;
                    amOfficeAssignmentRequestDTO.approverOfficeNameBn = approverOffice.officeNameBng;
                }

                amOfficeAssignmentRequestDTO.status = AmOfficeAssignmentRequestStatus.APPROVED.getValue();
                amOfficeAssignmentRequestDTO.lastModificationTime = System.currentTimeMillis();
                amOfficeAssignmentRequestDTO.modifiedBy = String.valueOf(requesterEmployeeRecordId);
                Am_office_assignment_requestDAO.getInstance().update(amOfficeAssignmentRequestDTO);
                sendNotificationToUser = true;
            } else {
                AmOfficeApprovalNotification.getInstance().sendWaitingForApprovalNotification(
                        approvalResponse.organogramIds, amOfficeAssignmentRequestDTO
                );
            }
        } else {
            String rejectReason = request.getParameter("reject_reason");
            if (rejectReason != null) {
                rejectReason = Jsoup.clean(rejectReason, Whitelist.simpleText());
                rejectReason = rejectReason.replace("'", " ");
                rejectReason = rejectReason.replace("\"", " ");
            } else {
                rejectReason = "";
            }
            Am_office_approval_mappingDAO.getInstance().rejectPendingApproval(model, rejectReason);

            amOfficeAssignmentRequestDTO.approverEmpId = employeeSearchModel.employeeRecordId;
            amOfficeAssignmentRequestDTO.approverOfficeUnitId = employeeSearchModel.officeUnitId;
            amOfficeAssignmentRequestDTO.approverOrgId = employeeSearchModel.organogramId;

            amOfficeAssignmentRequestDTO.approverNameEn = employeeSearchModel.employeeNameEn;
            amOfficeAssignmentRequestDTO.approverNameBn = employeeSearchModel.employeeNameBn;
            amOfficeAssignmentRequestDTO.approverOfficeUnitNameEn = employeeSearchModel.officeUnitNameEn;
            amOfficeAssignmentRequestDTO.approverOfficeUnitNameBn = employeeSearchModel.officeUnitNameBn;
            amOfficeAssignmentRequestDTO.approverOfficeUnitOrgNameEn = employeeSearchModel.organogramNameEn;
            amOfficeAssignmentRequestDTO.approverOfficeUnitOrgNameBn = employeeSearchModel.organogramNameBn;

            amOfficeAssignmentRequestDTO.approverPhoneNum = employeeSearchModel.phoneNumber;

            OfficesDTO approverOffice = OfficesRepository.getInstance().getOfficesDTOByID(amOfficeAssignmentRequestDTO.approverOfficeId);

            if (approverOffice != null) {
                amOfficeAssignmentRequestDTO.approverOfficeId = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(employeeSearchModel.officeUnitId).officeId;

                amOfficeAssignmentRequestDTO.approverOfficeNameEn = approverOffice.officeNameEng;
                amOfficeAssignmentRequestDTO.approverOfficeNameBn = approverOffice.officeNameBng;
            }
            amOfficeAssignmentRequestDTO.status = AmOfficeAssignmentRequestStatus.REJECTED.getValue();
            amOfficeAssignmentRequestDTO.comment = rejectReason;
            Am_office_assignment_requestDAO.getInstance().update(amOfficeAssignmentRequestDTO);
            sendNotificationToUser = true;
        }

        if (sendNotificationToUser) {
            AmOfficeApprovalNotification.getInstance().sendApproveOrRejectNotification(
                    Collections.singletonList(amOfficeAssignmentRequestDTO.requesterOrgId), amOfficeAssignmentRequestDTO, isAccepted
            );
        }
    }

    private void withdraw(HttpServletRequest request, UserDTO userDTO) throws Exception {

        long amOfficeAssignmentRequestId = Long.parseLong(request.getParameter("amOfficeAssignmentRequestId"));
        Am_office_assignment_requestDTO amOfficeAssignmentRequestDTO = Am_office_assignment_requestDAO.getInstance().getDTOByID(amOfficeAssignmentRequestId);
        long lastModificationTime = System.currentTimeMillis();

        AmOfficeApprovalModel model =
                new AmOfficeApprovalModel.AmOfficeApprovalModelBuilder()
                        .setAmOfficeAssignmentRequestDTO(amOfficeAssignmentRequestDTO)
                        .setRequesterEmployeeRecordId(userDTO.employee_record_id)
                        .setTaskTypeId(TaskTypeEnum.AM_OFFICE_ASSIGNMENT.getValue())
                        .build();

        EmployeeSearchModel employeeSearchModel = (gson.fromJson
                (EmployeeSearchModalUtil.getEmployeeSearchModelJson
                                (userDTO.employee_record_id,
                                        userDTO.unitID,
                                        userDTO.organogramID),
                        EmployeeSearchModel.class)
        );

        boolean sendNotificationToUser = false;

        String withdrawalReason = request.getParameter("withdrawal_reason");
        if (withdrawalReason != null) {
            withdrawalReason = Jsoup.clean(withdrawalReason, Whitelist.simpleText());
            withdrawalReason = withdrawalReason.replace("'", " ");
            withdrawalReason = withdrawalReason.replace("\"", " ");
        } else {
            withdrawalReason = "";
        }
        Am_office_approval_mappingDAO.getInstance().withdrawApprovedApproval(model, withdrawalReason);

        Am_office_assignmentDTO am_office_assignmentDTO = Am_office_assignmentDAO.getInstance().getByAmOfficeAssignmentRequestId(amOfficeAssignmentRequestDTO.iD);

        if (am_office_assignmentDTO.buildingTypeCat == 1) {
            Am_parliament_building_roomDTO amParliamentBuildingRoomDTO = Am_parliament_building_roomRepository.getInstance().
                    getAm_parliament_building_roomDTOByID(am_office_assignmentDTO.roomNo);
            amParliamentBuildingRoomDTO.status = CommonApprovalStatus.AVAILABLE.getValue();
            amParliamentBuildingRoomDTO.lastModificationTime = lastModificationTime;
            Am_parliament_building_roomDAO.getInstance().update(amParliamentBuildingRoomDTO);
        } else if (am_office_assignmentDTO.buildingTypeCat == 2) {
            Am_minister_hostel_levelDTO amMinisterHostelLevelDTO = Am_minister_hostel_levelRepository.getInstance().getAm_minister_hostel_levelDTOByID(am_office_assignmentDTO.level);
            amMinisterHostelLevelDTO.status = CommonApprovalStatus.AVAILABLE.getValue();
            amMinisterHostelLevelDTO.lastModificationTime = lastModificationTime;
            Am_minister_hostel_levelDAO.getInstance().update(amMinisterHostelLevelDTO);
        }

        amOfficeAssignmentRequestDTO.withdrawerEmpId = employeeSearchModel.employeeRecordId;
        amOfficeAssignmentRequestDTO.withdrawerOfficeUnitId = employeeSearchModel.officeUnitId;
        amOfficeAssignmentRequestDTO.withdrawerOrgId = employeeSearchModel.organogramId;

        amOfficeAssignmentRequestDTO.withdrawerNameEn = employeeSearchModel.employeeNameEn;
        amOfficeAssignmentRequestDTO.withdrawerNameBn = employeeSearchModel.employeeNameBn;
        amOfficeAssignmentRequestDTO.withdrawerOfficeUnitNameEn = employeeSearchModel.officeUnitNameEn;
        amOfficeAssignmentRequestDTO.withdrawerOfficeUnitNameBn = employeeSearchModel.officeUnitNameBn;
        amOfficeAssignmentRequestDTO.withdrawerOfficeUnitOrgNameEn = employeeSearchModel.organogramNameEn;
        amOfficeAssignmentRequestDTO.withdrawerOfficeUnitOrgNameBn = employeeSearchModel.organogramNameBn;

        amOfficeAssignmentRequestDTO.withdrawerPhoneNum = employeeSearchModel.phoneNumber;

        OfficesDTO withdrawerOffice = OfficesRepository.getInstance().getOfficesDTOByID(amOfficeAssignmentRequestDTO.withdrawerOfficeId);

        if (withdrawerOffice != null) {
            amOfficeAssignmentRequestDTO.withdrawerOfficeId = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(employeeSearchModel.officeUnitId).officeId;
            amOfficeAssignmentRequestDTO.withdrawerOfficeNameEn = withdrawerOffice.officeNameEng;
            amOfficeAssignmentRequestDTO.withdrawerOfficeNameBn = withdrawerOffice.officeNameBng;
        }


        amOfficeAssignmentRequestDTO.status = AmOfficeAssignmentRequestStatus.WITHDRAWN.getValue();
        amOfficeAssignmentRequestDTO.withdrawalReason = withdrawalReason;
        amOfficeAssignmentRequestDTO.withdrawalDate = lastModificationTime;
        Am_office_assignment_requestDAO.getInstance().update(amOfficeAssignmentRequestDTO);

        am_office_assignmentDTO.withdrawalDate = amOfficeAssignmentRequestDTO.withdrawalDate;
        am_office_assignmentDTO.status = CommonApprovalStatus.WITHDRAWN.getValue();
        am_office_assignmentDTO.lastModificationTime = lastModificationTime;
        Am_office_assignmentDAO.getInstance().update(am_office_assignmentDTO);

        sendNotificationToUser = true;


        if (sendNotificationToUser) {
            AmOfficeApprovalNotification.getInstance().sendWithdrawalNotification(
                    Collections.singletonList(amOfficeAssignmentRequestDTO.requesterOrgId), amOfficeAssignmentRequestDTO
            );
        }
    }


}

