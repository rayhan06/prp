package am_office_approval_mapping;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class Am_office_approval_mappingRepository implements Repository {
	Am_office_approval_mappingDAO am_office_approval_mappingDAO = null;
	
	public void setDAO(Am_office_approval_mappingDAO am_office_approval_mappingDAO)
	{
		this.am_office_approval_mappingDAO = am_office_approval_mappingDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Am_office_approval_mappingRepository.class);
	Map<Long, Am_office_approval_mappingDTO>mapOfAm_office_approval_mappingDTOToiD;

  
	private Am_office_approval_mappingRepository(){
		am_office_approval_mappingDAO =  Am_office_approval_mappingDAO.getInstance();
		mapOfAm_office_approval_mappingDTOToiD = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	private static class LazyLoader{
        final static Am_office_approval_mappingRepository INSTANCE = new Am_office_approval_mappingRepository();
    }

    public static Am_office_approval_mappingRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

	public void reload(boolean reloadAll){
		try {
			//List<Am_office_approval_mappingDTO> am_office_approval_mappingDTOs = (List<Am_office_approval_mappingDTO>)am_office_approval_mappingDAO.getAll(reloadAll);
			List<Am_office_approval_mappingDTO> am_office_approval_mappingDTOs = null;
			for(Am_office_approval_mappingDTO am_office_approval_mappingDTO : am_office_approval_mappingDTOs) {
				Am_office_approval_mappingDTO oldAm_office_approval_mappingDTO = getAm_office_approval_mappingDTOByID(am_office_approval_mappingDTO.iD);
				if( oldAm_office_approval_mappingDTO != null ) {
					mapOfAm_office_approval_mappingDTOToiD.remove(oldAm_office_approval_mappingDTO.iD);
				
					
				}
				if(am_office_approval_mappingDTO.isDeleted == 0) 
				{
					
					mapOfAm_office_approval_mappingDTOToiD.put(am_office_approval_mappingDTO.iD, am_office_approval_mappingDTO);
				
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Am_office_approval_mappingDTO> getAm_office_approval_mappingList() {
		List <Am_office_approval_mappingDTO> am_office_approval_mappings = new ArrayList<Am_office_approval_mappingDTO>(this.mapOfAm_office_approval_mappingDTOToiD.values());
		return am_office_approval_mappings;
	}
	
	
	public Am_office_approval_mappingDTO getAm_office_approval_mappingDTOByID( long ID){
		return mapOfAm_office_approval_mappingDTOToiD.get(ID);
	}

	
	@Override
	public String getTableName() {
		return "am_office_approval_mapping";
	}
}


