package am_office_approval_mapping;
import java.util.*; 
import util.*; 


public class Am_office_approval_mappingDTO extends CommonDTO
{

	public long amOfficeAssignmentRequestId = -1;
	public long cardApprovalId = -1;
	public int sequence = -1;
	public int amOfficeAssignmentStatusCat = -1;
	public long taskTypeId = -1;
	public long employeeRecordsId = -1;
	public long approverEmployeeRecordsId = -1;
	public long insertionDate = -1;
    public String insertedBy = "";
    public String modifiedBy = "";
    //public String searchColumn = "";
	public String comment  = "";
	
	
    @Override
	public String toString() {
            return "$Am_office_approval_mappingDTO[" +
            " iD = " + iD +
            " amOfficeAssignmentRequestId = " + amOfficeAssignmentRequestId +
            " cardApprovalId = " + cardApprovalId +
            " sequence = " + sequence +
            " amOfficeAssignmentStatusCat = " + amOfficeAssignmentStatusCat +
            " taskTypeId = " + taskTypeId +
            " employeeRecordsId = " + employeeRecordsId +
            " approverEmployeeRecordsId = " + approverEmployeeRecordsId +
            " insertionDate = " + insertionDate +
            " insertedBy = " + insertedBy +
            " modifiedBy = " + modifiedBy +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            " searchColumn = " + searchColumn +
			" comment = " + comment +
            "]";
    }

}