package am_office_approval_mapping;

import am_office_assignment_request.Am_office_assignment_requestDTO;

import pb_notifications.Pb_notificationsDAO;

import java.util.List;

public class AmOfficeApprovalNotification {
    private final Pb_notificationsDAO pb_notificationsDAO;

    private AmOfficeApprovalNotification(){
        pb_notificationsDAO = Pb_notificationsDAO.getInstance();
    }

    private static class LazyLoader {
        static final AmOfficeApprovalNotification INSTANCE = new AmOfficeApprovalNotification();
    }

    public static AmOfficeApprovalNotification getInstance(){
        return LazyLoader.INSTANCE;
    }

    public void sendWaitingForApprovalNotification(List<Long> organogramIds, Am_office_assignment_requestDTO amOfficeAssignmentRequestDTO){
        String textEn = amOfficeAssignmentRequestDTO.requesterNameEn + "(" + amOfficeAssignmentRequestDTO.requesterOfficeUnitOrgNameEn + ","
                + amOfficeAssignmentRequestDTO.requesterOfficeNameEn + ")'s office allocation's application is waiting for your approval";

        String textBn = amOfficeAssignmentRequestDTO.requesterNameBn + "(" + amOfficeAssignmentRequestDTO.requesterOfficeUnitOrgNameBn + ","
                + amOfficeAssignmentRequestDTO.requesterOfficeNameBn + ") এর অফিস বরাদ্দের আবেদন আপনার আনুমোদনের জন্য অপেক্ষমান";

        String notificationMessage = textEn + "$" + textBn;

        String url = "Am_office_approval_mappingServlet?actionType=getApprovalPage&amOfficeAssignmentRequestId=" + amOfficeAssignmentRequestDTO.iD;
        sendNotification(organogramIds, notificationMessage, url);
    }

    public void sendApproveOrRejectNotification(List<Long> organogramIds, Am_office_assignment_requestDTO am_office_assignment_requestDTO, boolean isApproved){
        String textEn = "Your application for office allocation has been " + (isApproved ? "accepted" : "rejected");

        String textBn = "আপনার অফিস বরাদ্দের আবেদন " + (isApproved ? "গৃহীত " : " প্রত্যাখ্যাত ") + " হয়েছে";

        String notificationMessage = textEn + "$" + textBn;

        String url = "Am_office_assignment_requestServlet?actionType=view&ID=" + am_office_assignment_requestDTO.iD;

        sendNotification(organogramIds,notificationMessage,url);
    }

    public void sendWithdrawalNotification(List<Long> organogramIds, Am_office_assignment_requestDTO am_office_assignment_requestDTO){
        String textEn = "The office assigned to you has been canceled";

        String textBn = "আপনার জন্য বরাদ্দকৃত অফিস বাতিল করা হয়েছে";

        String notificationMessage = textEn + "$" + textBn;

        String url = "Am_office_assignment_requestServlet?actionType=view&ID=" + am_office_assignment_requestDTO.iD;

        sendNotification(organogramIds,notificationMessage,url);
    }

    private void sendNotification(List<Long> organogramIds,String notificationMessage,String url){
        if(organogramIds == null || organogramIds.size() == 0) return;

        Thread thread = new Thread(()->{
            long currentTime = System.currentTimeMillis();
            organogramIds.forEach(organogramId -> pb_notificationsDAO.addPb_notifications(organogramId, currentTime, url, notificationMessage));
        });
        thread.setDaemon(true);
        thread.start();
    }
}
