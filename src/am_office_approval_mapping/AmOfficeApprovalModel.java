package am_office_approval_mapping;
import am_office_assignment_request.Am_office_assignment_requestDTO;


public class AmOfficeApprovalModel {
    private final long taskTypeId;
    private final long requesterEmployeeRecordId;
    private final Am_office_assignment_requestDTO amOfficeAssignmentRequestDTO;

    private AmOfficeApprovalModel(AmOfficeApprovalModelBuilder builder) {
        taskTypeId = builder.taskTypeId;
        requesterEmployeeRecordId = builder.requesterEmployeeRecordId;
        amOfficeAssignmentRequestDTO = builder.amOfficeAssignmentRequestDTO;
    }

    public long getTaskTypeId() {
        return taskTypeId;
    }

    public long getRequesterEmployeeRecordId() {
        return requesterEmployeeRecordId;
    }

    public Am_office_assignment_requestDTO getAmOfficeAssignmentRequestDTO() {
        return amOfficeAssignmentRequestDTO;
    }

    public static class AmOfficeApprovalModelBuilder {
        private long taskTypeId;
        private long requesterEmployeeRecordId;
        private Am_office_assignment_requestDTO amOfficeAssignmentRequestDTO;

        public AmOfficeApprovalModel build() {
            return new AmOfficeApprovalModel(this);
        }

        public AmOfficeApprovalModelBuilder setTaskTypeId(long taskTypeId) {
            this.taskTypeId = taskTypeId;
            return this;
        }

        public AmOfficeApprovalModelBuilder setRequesterEmployeeRecordId(long requesterEmployeeRecordId) {
            this.requesterEmployeeRecordId = requesterEmployeeRecordId;
            return this;
        }

        public AmOfficeApprovalModelBuilder setAmOfficeAssignmentRequestDTO(Am_office_assignment_requestDTO amOfficeAssignmentRequestDTO) {
            this.amOfficeAssignmentRequestDTO = amOfficeAssignmentRequestDTO;
            return this;
        }
    }
}
