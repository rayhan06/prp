package restriction_config;
import java.util.*; 
import util.*; 


public class RestrictionConfigDetailsDTO extends CommonDTO
{

	public long restrictionConfigId = -1;
	public long employeeRecordId = -1;
	public int restrictionCat = -1;
	public long insertedBy = -1;
	public long inserttionDate = System.currentTimeMillis();
	
	public static final int MEDICAL = 1;
	public static final int OTHERS = 2;
	
	public RestrictionConfigDetailsDTO()
	{
		
	}
	
	public RestrictionConfigDetailsDTO(Restriction_configDTO restriction_configDTO)
	{
		employeeRecordId = restriction_configDTO.employeeRecordId;
		insertedBy = restriction_configDTO.insertedBy;
		inserttionDate = restriction_configDTO.insertionDate;
		restrictionConfigId = restriction_configDTO.iD;
		
	}
	
	
	public List<RestrictionConfigDetailsDTO> restrictionConfigDetailsDTOList = new ArrayList<>();
	
    @Override
	public String toString() {
            return "$RestrictionConfigDetailsDTO[" +
            " iD = " + iD +
            " restrictionConfigId = " + restrictionConfigId +
            " employeeRecordId = " + employeeRecordId +
            " restrictionCat = " + restrictionCat +
            " insertedBy = " + insertedBy +
            " inserttionDate = " + inserttionDate +
            " searchColumn = " + searchColumn +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}