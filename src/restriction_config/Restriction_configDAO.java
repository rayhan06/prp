package restriction_config;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.sql.SQLException;
import common.CommonDAOService;
import org.apache.log4j.Logger;
import util.*;
import workflow.WorkflowController;

public class Restriction_configDAO  implements CommonDAOService<Restriction_configDTO>
{
	
	Logger logger = Logger.getLogger(getClass());
	
	private static String addQuery = "";
	private static String updateQuery = "";
	private String[] columnNames = null;

	private Restriction_configDAO()
	{
		columnNames = new String[] 
		{
			"ID",
			"employee_record_id",
			"restriction_reason",
			"inserted_by",
			"insertion_date",
			"search_column",
			"isDeleted",
			"lastModificationTime"
		};
		updateQuery = getUpdateQuery(columnNames);
        addQuery = getAddQuery(columnNames);
		
		searchMap.put("employee_record_id"," and (employee_record_id = ?)");
		searchMap.put("inserted_by"," and (inserted_by like ?)");
		searchMap.put("inserttion_date_start"," and (inserttion_date >= ?)");
		searchMap.put("userName"," and (employee_record_id = ?)");
		searchMap.put("inserttion_date_end"," and (inserttion_date <= ?)");
		searchMap.put("AnyField"," and (search_column like ?)");
	}

	private final Map<String,String> searchMap = new HashMap<>();

    private static class LazyLoader
    {
    	static final Restriction_configDAO INSTANCE = new Restriction_configDAO();
	}

	public static Restriction_configDAO getInstance()
	{
    	return LazyLoader.INSTANCE;
	}
	
	public void setSearchColumn(Restriction_configDTO restriction_configDTO) throws Exception
	{
		restriction_configDTO.searchColumn = "";
		restriction_configDTO.searchColumn += WorkflowController.getNameFromEmployeeRecordID(restriction_configDTO.employeeRecordId, "english") + " ";
		restriction_configDTO.searchColumn += WorkflowController.getNameFromEmployeeRecordID(restriction_configDTO.employeeRecordId, "bangla") + " ";
	}
	
	@Override
	public void set(PreparedStatement ps, Restriction_configDTO restriction_configDTO, boolean isInsert) throws SQLException
	{
		int index = 0;
		long lastModificationTime = System.currentTimeMillis();	
		try {
			setSearchColumn(restriction_configDTO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(isInsert)
		{
			ps.setObject(++index,restriction_configDTO.iD);
		}
		ps.setObject(++index,restriction_configDTO.employeeRecordId);
		ps.setObject(++index,restriction_configDTO.restrictionReason);
		ps.setObject(++index,restriction_configDTO.insertedBy);
		ps.setObject(++index,restriction_configDTO.insertionDate);
		ps.setObject(++index,restriction_configDTO.searchColumn);
		if(isInsert)
		{
			ps.setObject(++index,restriction_configDTO.isDeleted);
		}
		ps.setObject(++index, lastModificationTime);
		if(!isInsert)
		{
			ps.setObject(++index,restriction_configDTO.iD);
		}
	}
	
	@Override
	public Restriction_configDTO buildObjectFromResultSet(ResultSet rs)
	{
		try
		{
			Restriction_configDTO restriction_configDTO = new Restriction_configDTO();
			int i = 0;
			restriction_configDTO.iD = rs.getLong(columnNames[i++]);
			restriction_configDTO.employeeRecordId = rs.getLong(columnNames[i++]);
			restriction_configDTO.restrictionReason = rs.getString(columnNames[i++]);
			restriction_configDTO.insertedBy = rs.getLong(columnNames[i++]);
			restriction_configDTO.insertionDate = rs.getLong(columnNames[i++]);
			restriction_configDTO.searchColumn = rs.getString(columnNames[i++]);
			restriction_configDTO.isDeleted = rs.getInt(columnNames[i++]);
			restriction_configDTO.lastModificationTime = rs.getLong(columnNames[i++]);
			return restriction_configDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
		
	public Restriction_configDTO getDTOByID (long id)
	{
		Restriction_configDTO restriction_configDTO = null;
		try 
		{
			restriction_configDTO = getDTOFromID(id);
			if(restriction_configDTO != null)
			{
				RestrictionConfigDetailsDAO restrictionConfigDetailsDAO = RestrictionConfigDetailsDAO.getInstance();				
				List<RestrictionConfigDetailsDTO> restrictionConfigDetailsDTOList = (List<RestrictionConfigDetailsDTO>)restrictionConfigDetailsDAO.getDTOsByParent("restriction_config_id", restriction_configDTO.iD);
				restriction_configDTO.restrictionConfigDetailsDTOList = restrictionConfigDetailsDTOList;
			}
		}
		catch (Exception e) 
		{
				// TODO Auto-generated catch block
				e.printStackTrace();
		}
		return restriction_configDTO;
	}

	@Override
	public String getTableName() {
		return "restriction_config";
	}

	@Override
	public Map<String, String> getSearchMap() {
		return searchMap;
	}

	public long add(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Restriction_configDTO) commonDTO,addQuery,true);
	}

	public long update(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Restriction_configDTO) commonDTO,updateQuery,false);
	}
	
	@Override
	public String getLastModifierUser(){
		return "";
    }
				
}
	