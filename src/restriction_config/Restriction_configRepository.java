package restriction_config;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import com.google.gson.Gson;

import repository.Repository;
import repository.RepositoryManager;


public class Restriction_configRepository implements Repository {
	Restriction_configDAO restriction_configDAO = null;
	
	static Logger logger = Logger.getLogger(Restriction_configRepository.class);
	Map<Long, Restriction_configDTO>mapOfRestriction_configDTOToiD;
	private List<Restriction_configDTO> restrictionConfigList;
	Gson gson;

  
	private Restriction_configRepository(){
		restriction_configDAO = Restriction_configDAO.getInstance();
		mapOfRestriction_configDTOToiD = new ConcurrentHashMap<>();
		restrictionConfigList = new ArrayList<Restriction_configDTO> ();
		gson = new Gson();
		RepositoryManager.getInstance().addRepository(this);
	}

	private static class LazyLoader{
        final static Restriction_configRepository INSTANCE = new Restriction_configRepository();
    }

    public static Restriction_configRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

	public void reload(boolean reloadAll){
		try {
			List<Restriction_configDTO> restriction_configDTOs = restriction_configDAO.getAllDTOs(reloadAll);
			for(Restriction_configDTO restriction_configDTO : restriction_configDTOs) {
				Restriction_configDTO oldRestriction_configDTO = getRestriction_configDTOByiD(restriction_configDTO.iD);
				if( oldRestriction_configDTO != null ) {
					mapOfRestriction_configDTOToiD.remove(oldRestriction_configDTO.iD);
				
					
				}
				if(restriction_configDTO.isDeleted == 0) 
				{
					
					mapOfRestriction_configDTOToiD.put(restriction_configDTO.iD, restriction_configDTO);
				
				}
			}
			restrictionConfigList = new ArrayList<>(mapOfRestriction_configDTOToiD.values());
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public Restriction_configDTO clone(Restriction_configDTO dto) {
		String raw = gson.toJson(dto);
		return gson.fromJson(raw, Restriction_configDTO.class);
	}
	
	
	public List<Restriction_configDTO> getRestriction_configList() {
		List <Restriction_configDTO> restriction_configs = new ArrayList<Restriction_configDTO>(this.mapOfRestriction_configDTOToiD.values());
		return restriction_configs;
	}
	
	public List<Restriction_configDTO> copyRestriction_configList() {
		List <Restriction_configDTO> restriction_configs = getRestriction_configList();
		return restriction_configs
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}
	
	public Restriction_configDTO getLastByErId(long erId)
	{		
		Restriction_configDTO rDTO =  restrictionConfigList
		.stream()
		.filter(e -> e.employeeRecordId == erId)
		.sorted(Comparator.comparing(Restriction_configDTO::getLastModificationTime).reversed())
		.findFirst()
        .orElse(null);
		
		if(rDTO != null)
		{
			rDTO.restrictionConfigDetailsDTOList = RestrictionConfigDetailsRepository.getInstance().getRestrictionConfigDetailsDTOByrestrictionConfigId(rDTO.iD);
			logger.debug("restrictionConfigDetailsDTOList size = " + rDTO.restrictionConfigDetailsDTOList.size());
		}
		else
		{
			logger.debug("rDTO is null");
		}
		
		return rDTO;
	}
	
	public boolean restrictionInList(List<RestrictionConfigDetailsDTO> restrictionConfigDetailsDTOList, int restriction)
	{
		if(restrictionConfigDetailsDTOList == null || restrictionConfigDetailsDTOList.isEmpty())
		{
			return false;
		}
		
		return (restrictionConfigDetailsDTOList
				.stream()
				.filter(e -> e.restrictionCat == restriction)
				.findAny()
				.orElse(null) != null);
	}
	
	public boolean restrictionInList(long erId, int restriction)
	{
		Restriction_configDTO rtDTO = getLastByErId(erId);
		if(rtDTO == null)
		{
			return false;
		}
		return restrictionInList(rtDTO.restrictionConfigDetailsDTOList, restriction);
	}
	
	
	
	public Restriction_configDTO getRestriction_configDTOByiD( long iD){
		return mapOfRestriction_configDTOToiD.get(iD);
	}
	
	
	public Restriction_configDTO copyRestriction_configDTOByiD( long iD){
		return clone(mapOfRestriction_configDTOToiD.get(iD));
	}

	
	@Override
	public String getTableName() {
		return restriction_configDAO.getTableName();
	}
}


