package restriction_config;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.sql.SQLException;
import common.CommonDAOService;
import org.apache.log4j.Logger;
import util.*;
import pb.*;

public class RestrictionConfigDetailsDAO  implements CommonDAOService<RestrictionConfigDetailsDTO>
{
	
	Logger logger = Logger.getLogger(getClass());
	
	private static String addQuery = "";
	private static String updateQuery = "";
	private String[] columnNames = null;

	private RestrictionConfigDetailsDAO()
	{
		columnNames = new String[] 
		{
			"ID",
			"restriction_config_id",
			"employee_record_id",
			"restriction_cat",
			"inserted_by",
			"inserttion_date",
			"search_column",
			"isDeleted",
			"lastModificationTime"
		};
		updateQuery = getUpdateQuery(columnNames);
        addQuery = getAddQuery(columnNames);
		
		searchMap.put("restriction_cat"," and (restriction_cat = ?)");
		searchMap.put("inserted_by"," and (inserted_by like ?)");
		searchMap.put("inserttion_date_start"," and (inserttion_date >= ?)");
		searchMap.put("inserttion_date_end"," and (inserttion_date <= ?)");
		searchMap.put("AnyField"," and (search_column like ?)");
	}

	private final Map<String,String> searchMap = new HashMap<>();

    private static class LazyLoader
    {
    	static final RestrictionConfigDetailsDAO INSTANCE = new RestrictionConfigDetailsDAO();
	}

	public static RestrictionConfigDetailsDAO getInstance()
	{
    	return LazyLoader.INSTANCE;
	}
	
	public void setSearchColumn(RestrictionConfigDetailsDTO restrictionconfigdetailsDTO)
	{
		restrictionconfigdetailsDTO.searchColumn = "";
		restrictionconfigdetailsDTO.searchColumn += CatDAO.getName("English", "restriction", restrictionconfigdetailsDTO.restrictionCat) + " " + CatDAO.getName("Bangla", "restriction", restrictionconfigdetailsDTO.restrictionCat) + " ";
		restrictionconfigdetailsDTO.searchColumn += restrictionconfigdetailsDTO.insertedBy + " ";
	}
	
	@Override
	public void set(PreparedStatement ps, RestrictionConfigDetailsDTO restrictionconfigdetailsDTO, boolean isInsert) throws SQLException
	{
		int index = 0;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(restrictionconfigdetailsDTO);
		if(isInsert)
		{
			ps.setObject(++index,restrictionconfigdetailsDTO.iD);
		}
		ps.setObject(++index,restrictionconfigdetailsDTO.restrictionConfigId);
		ps.setObject(++index,restrictionconfigdetailsDTO.employeeRecordId);
		ps.setObject(++index,restrictionconfigdetailsDTO.restrictionCat);
		ps.setObject(++index,restrictionconfigdetailsDTO.insertedBy);
		ps.setObject(++index,restrictionconfigdetailsDTO.inserttionDate);
		ps.setObject(++index,restrictionconfigdetailsDTO.searchColumn);
		if(isInsert)
		{
			ps.setObject(++index,restrictionconfigdetailsDTO.isDeleted);
		}
		ps.setObject(++index, lastModificationTime);
		if(!isInsert)
		{
			ps.setObject(++index,restrictionconfigdetailsDTO.iD);
		}
	}
	
	@Override
	public RestrictionConfigDetailsDTO buildObjectFromResultSet(ResultSet rs)
	{
		try
		{
			RestrictionConfigDetailsDTO restrictionconfigdetailsDTO = new RestrictionConfigDetailsDTO();
			int i = 0;
			restrictionconfigdetailsDTO.iD = rs.getLong(columnNames[i++]);
			restrictionconfigdetailsDTO.restrictionConfigId = rs.getLong(columnNames[i++]);
			restrictionconfigdetailsDTO.employeeRecordId = rs.getLong(columnNames[i++]);
			restrictionconfigdetailsDTO.restrictionCat = rs.getInt(columnNames[i++]);
			restrictionconfigdetailsDTO.insertedBy = rs.getLong(columnNames[i++]);
			restrictionconfigdetailsDTO.inserttionDate = rs.getLong(columnNames[i++]);
			restrictionconfigdetailsDTO.searchColumn = rs.getString(columnNames[i++]);
			restrictionconfigdetailsDTO.isDeleted = rs.getInt(columnNames[i++]);
			restrictionconfigdetailsDTO.lastModificationTime = rs.getLong(columnNames[i++]);
			return restrictionconfigdetailsDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
		
	public RestrictionConfigDetailsDTO getDTOByID (long id)
	{
		return getDTOFromID(id);
	}

	@Override
	public String getTableName() {
		return "restriction_config_details";
	}

	@Override
	public Map<String, String> getSearchMap() {
		return searchMap;
	}

	public long add(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((RestrictionConfigDetailsDTO) commonDTO,addQuery,true);
	}

	public long update(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((RestrictionConfigDetailsDTO) commonDTO,updateQuery,false);
	}
	
	@Override
	public String getLastModifierUser(){
		return "";
    }
				
}
	