package restriction_config;


import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import org.apache.log4j.Logger;


import permission.MenuConstants;
import restriction_summary.Restriction_summaryDAO;
import restriction_summary.Restriction_summaryDTO;
import role.PermissionRepository;
import user.UserDTO;
import util.*;
import workflow.WorkflowController;

import javax.servlet.http.*;
import java.util.*;


import common.ApiResponse;
import common.BaseServlet;
import com.google.gson.Gson;
import pb.*;

import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;




/**
 * Servlet implementation class Restriction_configServlet
 */
@WebServlet("/Restriction_configServlet")
@MultipartConfig
public class Restriction_configServlet extends BaseServlet 
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Restriction_configServlet.class);

    @Override
    public String getTableName() {
        return Restriction_configDAO.getInstance().getTableName();
    }

    @Override
    public String getServletName() {
        return "Restriction_configServlet";
    }

    @Override
    public Restriction_configDAO getCommonDAOService() {
        return Restriction_configDAO.getInstance();
    }
    
    @Override
    public int[] getAddPageMenuConstants() {
        return new int[] {MenuConstants.RESTRICTION_CONFIG_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[] {MenuConstants.RESTRICTION_CONFIG_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[] {MenuConstants.RESTRICTION_CONFIG_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Restriction_configServlet.class;
    }
	RestrictionConfigDetailsDAO restrictionConfigDetailsDAO = RestrictionConfigDetailsDAO.getInstance();
    private final Gson gson = new Gson();
    
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        CommonLoginData commonLoginData = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get();
        init(request);
        String actionType = request.getParameter("actionType");
		if ("getLastByErId".equals(actionType)) {
			try {
				String userName = request.getParameter("userName");
				long erId = WorkflowController.getEmployeeRecordsIdFromUserName(userName);
				logger.debug("erId = " + erId);
				if(erId != -1)
				{
					Restriction_configDTO rcDTO = Restriction_configRepository.getInstance().getLastByErId(erId);
					PrintWriter out = response.getWriter();
					response.setContentType("application/json");
					response.setCharacterEncoding("UTF-8");
					
					String encoded = this.gson.toJson(rcDTO);
					out.print(encoded);
					out.flush();
				}
			} catch (Exception ex) {
				logger.error(ex);
				ApiResponse.sendErrorResponse(response, ex.getMessage());
			}
			return;
		}
		else if ("history".equals(actionType)) {
			try {
				long erId = Long.parseLong(request.getParameter("erId"));
				logger.debug("erId = " + erId);
				if(erId != -1)
				{
					if(PermissionRepository.checkPermissionByRoleIDAndMenuID(commonLoginData.userDTO.roleID, MenuConstants.RESTRICTION_CONFIG_SEARCH))
					{
						request.setAttribute("erId", erId);
						request.getRequestDispatcher("restriction_config/history.jsp").forward(request, response);
						
					}
					else
					{
						request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
					}
				}
			} catch (Exception ex) {
				logger.error(ex);
				ApiResponse.sendErrorResponse(response, ex.getMessage());
			}
			return;
		}
		else
		{
			super.doGet(request,response);
		}
    }
    
    @Override
    public Map<String, String> buildRequestParams(HttpServletRequest request) {
    	 Enumeration<String> paramList = request.getParameterNames();
         Map<String,String> params = new HashMap<>();
         while (paramList.hasMoreElements()){
             String paramName = paramList.nextElement();
             String paramValue = request.getParameter(paramName);
             if(Utils.isValidSearchString(paramValue)){
            	 if(paramName.equalsIgnoreCase("userName"))
            	 {
            		 params.put(paramName, WorkflowController.getEmployeeRecordsIdFromUserName(paramValue) + "");
            	 }
            	 else if(paramName.equalsIgnoreCase("AnyField") && Utils.isValidUserName(paramValue))
            	 {
            		 params.put("userName", WorkflowController.getEmployeeRecordsIdFromUserName(paramValue) + "");
            	 }
            	 else
            	 {
            		 params.put(paramName,paramValue);
            	 }
                 
             }
         }
         return params;
    }
 	

	@Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception 
	{
		
		request.setAttribute("failureMessage", "");
		System.out.println("%%%% addRestriction_config");
		String language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
		Restriction_configDTO restriction_configDTO;

					
		if(addFlag)
		{
			restriction_configDTO = new Restriction_configDTO();
		}
		else
		{
			restriction_configDTO = Restriction_configDAO.getInstance().getDTOFromID(Long.parseLong(request.getParameter("iD")));
		}
		
		String Value = "";

		Value = request.getParameter("userName");

		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("userName = " + Value);
		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			Value = Utils.getDigitEnglishFromBangla(Value);
			restriction_configDTO.employeeRecordId = WorkflowController.getEmployeeRecordsIdFromUserName(Value);
			if(restriction_configDTO.employeeRecordId == -1)
			{
				return null;
			}
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}

		Value = request.getParameter("restrictionReason");

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("restrictionReason = " + Value);
		if(Value != null)
		{
			restriction_configDTO.restrictionReason = (Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}

		restriction_configDTO.insertedBy = userDTO.employee_record_id;
		restriction_configDTO.insertionDate = System.currentTimeMillis();

		
		if(addFlag == true)
		{
			Restriction_configDAO.getInstance().add(restriction_configDTO);
		}
		else
		{				
			Restriction_configDAO.getInstance().update(restriction_configDTO);										
		}
		
		restrictionConfigDetailsDAO.hardDeleteChildrenByParent(restriction_configDTO.iD, "restriction_config_id");
		String []sCats = request.getParameterValues("sCat");
		if(sCats != null)
		{
			int i = 0;
			for(String sCat: sCats)
			{
				int cat = Integer.parseInt(sCat);
				long detailsId = -1;
				boolean added = request.getParameterValues("sCatVal")[i].equals("1");
				if(added)
				{
					RestrictionConfigDetailsDTO restrictionConfigDetailsDTO = new RestrictionConfigDetailsDTO(restriction_configDTO);
					restrictionConfigDetailsDTO.restrictionCat = cat;
					detailsId = restrictionConfigDetailsDAO.add(restrictionConfigDetailsDTO);
					
				}
				Restriction_summaryDTO restriction_summaryDTO = Restriction_summaryDAO.getInstance().getLast(restriction_configDTO.employeeRecordId, cat);
				boolean found = true;
				if(restriction_summaryDTO == null)
				{
					found = false;
					restriction_summaryDTO= new Restriction_summaryDTO();
				}
				restriction_summaryDTO.set(restriction_configDTO, detailsId, cat, added);
				if(found)
				{
					Restriction_summaryDAO.getInstance().update(restriction_summaryDTO);
				}
				else
				{
					Restriction_summaryDAO.getInstance().add(restriction_summaryDTO);
				}
				i++;
				
			}
		}
		
		Restriction_configRepository.getInstance().reload(false);
		RestrictionConfigDetailsRepository.getInstance().reload(false);
					
		return restriction_configDTO;

	}

	
}

