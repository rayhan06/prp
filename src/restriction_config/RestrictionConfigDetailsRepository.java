package restriction_config;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import com.google.gson.Gson;

import repository.Repository;
import repository.RepositoryManager;


public class RestrictionConfigDetailsRepository implements Repository {
	RestrictionConfigDetailsDAO restrictionconfigdetailsDAO = null;
	
	static Logger logger = Logger.getLogger(RestrictionConfigDetailsRepository.class);
	Map<Long, RestrictionConfigDetailsDTO>mapOfRestrictionConfigDetailsDTOToiD;
	Map<Long, Set<RestrictionConfigDetailsDTO> >mapOfRestrictionConfigDetailsDTOTorestrictionConfigId;
	Gson gson;

  
	private RestrictionConfigDetailsRepository(){
		restrictionconfigdetailsDAO = RestrictionConfigDetailsDAO.getInstance();
		mapOfRestrictionConfigDetailsDTOToiD = new ConcurrentHashMap<>();
		mapOfRestrictionConfigDetailsDTOTorestrictionConfigId = new ConcurrentHashMap<>();
		gson = new Gson();
		RepositoryManager.getInstance().addRepository(this);
	}

	private static class LazyLoader{
        final static RestrictionConfigDetailsRepository INSTANCE = new RestrictionConfigDetailsRepository();
    }

    public static RestrictionConfigDetailsRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

	public void reload(boolean reloadAll){
		try {
			List<RestrictionConfigDetailsDTO> restrictionconfigdetailsDTOs = restrictionconfigdetailsDAO.getAllDTOs(reloadAll);
			logger.debug("restrictionconfigdetailsDTOs size = " + restrictionconfigdetailsDTOs.size() + " reloadAll = " + reloadAll);
			for(RestrictionConfigDetailsDTO restrictionconfigdetailsDTO : restrictionconfigdetailsDTOs) {
				RestrictionConfigDetailsDTO oldRestrictionConfigDetailsDTO = getRestrictionConfigDetailsDTOByiD(restrictionconfigdetailsDTO.iD);
				if( oldRestrictionConfigDetailsDTO != null ) {
					mapOfRestrictionConfigDetailsDTOToiD.remove(oldRestrictionConfigDetailsDTO.iD);
				
					if(mapOfRestrictionConfigDetailsDTOTorestrictionConfigId.containsKey(oldRestrictionConfigDetailsDTO.restrictionConfigId)) {
						mapOfRestrictionConfigDetailsDTOTorestrictionConfigId.get(oldRestrictionConfigDetailsDTO.restrictionConfigId).remove(oldRestrictionConfigDetailsDTO);
					}
					if(mapOfRestrictionConfigDetailsDTOTorestrictionConfigId.get(oldRestrictionConfigDetailsDTO.restrictionConfigId).isEmpty()) {
						mapOfRestrictionConfigDetailsDTOTorestrictionConfigId.remove(oldRestrictionConfigDetailsDTO.restrictionConfigId);
					}
					
					
				}
				if(restrictionconfigdetailsDTO.isDeleted == 0) 
				{
					
					mapOfRestrictionConfigDetailsDTOToiD.put(restrictionconfigdetailsDTO.iD, restrictionconfigdetailsDTO);
				
					if( ! mapOfRestrictionConfigDetailsDTOTorestrictionConfigId.containsKey(restrictionconfigdetailsDTO.restrictionConfigId)) {
						logger.debug("does not contain key for " + restrictionconfigdetailsDTO.restrictionConfigId );
						mapOfRestrictionConfigDetailsDTOTorestrictionConfigId.put(restrictionconfigdetailsDTO.restrictionConfigId, new HashSet<>());
					}
					
					mapOfRestrictionConfigDetailsDTOTorestrictionConfigId.get(restrictionconfigdetailsDTO.restrictionConfigId).add(restrictionconfigdetailsDTO);
					
					
				}
			}
			
			
			logger.debug("mapOfRestrictionConfigDetailsDTOTorestrictionConfigId size = " + mapOfRestrictionConfigDetailsDTOTorestrictionConfigId.size() );
			
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public RestrictionConfigDetailsDTO clone(RestrictionConfigDetailsDTO dto) {
		String raw = gson.toJson(dto);
		return gson.fromJson(raw, RestrictionConfigDetailsDTO.class);
	}
	
	
	public List<RestrictionConfigDetailsDTO> getRestrictionConfigDetailsList() {
		List <RestrictionConfigDetailsDTO> restrictionconfigdetailss = new ArrayList<RestrictionConfigDetailsDTO>(this.mapOfRestrictionConfigDetailsDTOToiD.values());
		return restrictionconfigdetailss;
	}
	
	public List<RestrictionConfigDetailsDTO> copyRestrictionConfigDetailsList() {
		List <RestrictionConfigDetailsDTO> restrictionconfigdetailss = getRestrictionConfigDetailsList();
		return restrictionconfigdetailss
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}
	
	
	
	public RestrictionConfigDetailsDTO getRestrictionConfigDetailsDTOByiD( long iD){
		return mapOfRestrictionConfigDetailsDTOToiD.get(iD);
	}
	
	public RestrictionConfigDetailsDTO copyRestrictionConfigDetailsDTOByiD( long iD){
		return clone(mapOfRestrictionConfigDetailsDTOToiD.get(iD));
	}
	
	
	public List<RestrictionConfigDetailsDTO> getRestrictionConfigDetailsDTOByrestrictionConfigId(long restrictionConfigId) {
		Set<RestrictionConfigDetailsDTO>  testSet = mapOfRestrictionConfigDetailsDTOTorestrictionConfigId.getOrDefault(restrictionConfigId,new HashSet<>());
		logger.debug("set size for " + restrictionConfigId + " = " 
				+ testSet.size());
		List<RestrictionConfigDetailsDTO> list =  new ArrayList<>(testSet);
		logger.debug("list size for " + restrictionConfigId + " = " 
				+ list.size());
		return list;
	}
	
	public List<RestrictionConfigDetailsDTO> copyRestrictionConfigDetailsDTOByrestrictionConfigId(long restrictionConfigId)
	{
		List <RestrictionConfigDetailsDTO> restrictionconfigdetailss = getRestrictionConfigDetailsDTOByrestrictionConfigId(restrictionConfigId);
		return restrictionconfigdetailss
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}

	
	@Override
	public String getTableName() {
		return restrictionconfigdetailsDAO.getTableName();
	}
}


