package restriction_config;
import java.util.*; 
import util.*; 


public class Restriction_configDTO extends CommonDTO
{

	public long employeeRecordId = -1;
    public String restrictionReason = "";
	public long insertedBy = -1;
	public long insertionDate = System.currentTimeMillis();
	
	public long getId()
	{
		return iD;
	}
	
	public long getLastModificationTime()
	{
		return lastModificationTime;
	}
	
	public Restriction_configDTO() {}
	public Restriction_configDTO(long erId)
	{
		employeeRecordId = erId;
	}
	
	public List<RestrictionConfigDetailsDTO> restrictionConfigDetailsDTOList = new ArrayList<>();
	
    @Override
	public String toString() {
            return "$Restriction_configDTO[" +
            " iD = " + iD +
            " employeeRecordId = " + employeeRecordId +
            " restrictionReason = " + restrictionReason +
            " insertedBy = " + insertedBy +
            " inserttionDate = " + insertionDate +
            " searchColumn = " + searchColumn +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}