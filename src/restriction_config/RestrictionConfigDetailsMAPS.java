package restriction_config;
import java.util.*; 
import util.*;


public class RestrictionConfigDetailsMAPS extends CommonMaps
{	
	public RestrictionConfigDetailsMAPS(String tableName)
	{
		


		java_SQL_map.put("restriction_config_id".toLowerCase(), "restrictionConfigId".toLowerCase());
		java_SQL_map.put("employee_record_id".toLowerCase(), "employeeRecordId".toLowerCase());
		java_SQL_map.put("restriction_cat".toLowerCase(), "restrictionCat".toLowerCase());
		java_SQL_map.put("inserted_by".toLowerCase(), "insertedBy".toLowerCase());
		java_SQL_map.put("inserttion_date".toLowerCase(), "inserttionDate".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Restriction Config Id".toLowerCase(), "restrictionConfigId".toLowerCase());
		java_Text_map.put("Employee Record Id".toLowerCase(), "employeeRecordId".toLowerCase());
		java_Text_map.put("Restriction".toLowerCase(), "restrictionCat".toLowerCase());
		java_Text_map.put("Inserted By".toLowerCase(), "insertedBy".toLowerCase());
		java_Text_map.put("Inserttion Date".toLowerCase(), "inserttionDate".toLowerCase());
		java_Text_map.put("Search Column".toLowerCase(), "searchColumn".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}