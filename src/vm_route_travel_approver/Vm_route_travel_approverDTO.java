package vm_route_travel_approver;
import java.util.*; 
import util.*; 


public class Vm_route_travel_approverDTO extends CommonDTO
{

	public long insertedByUserId = -1;
	public long insertedByOrganogramId = -1;
	public long insertionDate = -1;
	public long approverOrgId = -1;
	public long approverOfficeId = -1;
	public long approverOfficeUnitId = -1;
	public long approverEmpId = -1;
    public String approverPhoneNum = "";
    public String approverNameEn = "";
    public String approverNameBn = "";
    public String approverOfficeNameEn = "";
    public String approverOfficeNameBn = "";
    public String approverOfficeUnitNameEn = "";
    public String approverOfficeUnitNameBn = "";
    public String approverOfficeUnitOrgNameEn = "";
    public String approverOfficeUnitOrgNameBn = "";
	
	
    @Override
	public String toString() {
            return "$Vm_route_travel_approverDTO[" +
            " iD = " + iD +
            " insertedByUserId = " + insertedByUserId +
            " insertedByOrganogramId = " + insertedByOrganogramId +
            " insertionDate = " + insertionDate +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            " searchColumn = " + searchColumn +
            " approverOrgId = " + approverOrgId +
            " approverOfficeId = " + approverOfficeId +
            " approverOfficeUnitId = " + approverOfficeUnitId +
            " approverEmpId = " + approverEmpId +
            " approverPhoneNum = " + approverPhoneNum +
            " approverNameEn = " + approverNameEn +
            " approverNameBn = " + approverNameBn +
            " approverOfficeNameEn = " + approverOfficeNameEn +
            " approverOfficeNameBn = " + approverOfficeNameBn +
            " approverOfficeUnitNameEn = " + approverOfficeUnitNameEn +
            " approverOfficeUnitNameBn = " + approverOfficeUnitNameBn +
            " approverOfficeUnitOrgNameEn = " + approverOfficeUnitOrgNameEn +
            " approverOfficeUnitOrgNameBn = " + approverOfficeUnitOrgNameBn +
            "]";
    }

}