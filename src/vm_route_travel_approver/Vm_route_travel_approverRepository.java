package vm_route_travel_approver;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import com.google.gson.Gson;
import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;
import vm_requisition_approver.Vm_requisition_approverDAO;


public class Vm_route_travel_approverRepository implements Repository {
	Vm_route_travel_approverDAO vm_route_travel_approverDAO = null;
	Gson gson;

	public void setDAO(Vm_route_travel_approverDAO vm_route_travel_approverDAO)
	{
		this.vm_route_travel_approverDAO = vm_route_travel_approverDAO;
		gson = new Gson();

	}
	
	
	static Logger logger = Logger.getLogger(Vm_route_travel_approverRepository.class);
	Map<Long, Vm_route_travel_approverDTO>mapOfVm_route_travel_approverDTOToiD;
//	Map<Long, Set<Vm_route_travel_approverDTO> >mapOfVm_route_travel_approverDTOToinsertedByUserId;
//	Map<Long, Set<Vm_route_travel_approverDTO> >mapOfVm_route_travel_approverDTOToinsertedByOrganogramId;
//	Map<Long, Set<Vm_route_travel_approverDTO> >mapOfVm_route_travel_approverDTOToinsertionDate;
//	Map<Long, Set<Vm_route_travel_approverDTO> >mapOfVm_route_travel_approverDTOTolastModificationTime;
//	Map<String, Set<Vm_route_travel_approverDTO> >mapOfVm_route_travel_approverDTOTosearchColumn;
	Map<Long, Set<Vm_route_travel_approverDTO> >mapOfVm_route_travel_approverDTOToapproverOrgId;
//	Map<Long, Set<Vm_route_travel_approverDTO> >mapOfVm_route_travel_approverDTOToapproverOfficeId;
//	Map<Long, Set<Vm_route_travel_approverDTO> >mapOfVm_route_travel_approverDTOToapproverOfficeUnitId;
//	Map<Long, Set<Vm_route_travel_approverDTO> >mapOfVm_route_travel_approverDTOToapproverEmpId;
//	Map<String, Set<Vm_route_travel_approverDTO> >mapOfVm_route_travel_approverDTOToapproverPhoneNum;
//	Map<String, Set<Vm_route_travel_approverDTO> >mapOfVm_route_travel_approverDTOToapproverNameEn;
//	Map<String, Set<Vm_route_travel_approverDTO> >mapOfVm_route_travel_approverDTOToapproverNameBn;
//	Map<String, Set<Vm_route_travel_approverDTO> >mapOfVm_route_travel_approverDTOToapproverOfficeNameEn;
//	Map<String, Set<Vm_route_travel_approverDTO> >mapOfVm_route_travel_approverDTOToapproverOfficeNameBn;
//	Map<String, Set<Vm_route_travel_approverDTO> >mapOfVm_route_travel_approverDTOToapproverOfficeUnitNameEn;
//	Map<String, Set<Vm_route_travel_approverDTO> >mapOfVm_route_travel_approverDTOToapproverOfficeUnitNameBn;
//	Map<String, Set<Vm_route_travel_approverDTO> >mapOfVm_route_travel_approverDTOToapproverOfficeUnitOrgNameEn;
//	Map<String, Set<Vm_route_travel_approverDTO> >mapOfVm_route_travel_approverDTOToapproverOfficeUnitOrgNameBn;


	static Vm_route_travel_approverRepository instance = null;
	private Vm_route_travel_approverRepository(){
		mapOfVm_route_travel_approverDTOToiD = new ConcurrentHashMap<>();
//		mapOfVm_route_travel_approverDTOToinsertedByUserId = new ConcurrentHashMap<>();
//		mapOfVm_route_travel_approverDTOToinsertedByOrganogramId = new ConcurrentHashMap<>();
//		mapOfVm_route_travel_approverDTOToinsertionDate = new ConcurrentHashMap<>();
//		mapOfVm_route_travel_approverDTOTolastModificationTime = new ConcurrentHashMap<>();
//		mapOfVm_route_travel_approverDTOTosearchColumn = new ConcurrentHashMap<>();
		mapOfVm_route_travel_approverDTOToapproverOrgId = new ConcurrentHashMap<>();
//		mapOfVm_route_travel_approverDTOToapproverOfficeId = new ConcurrentHashMap<>();
//		mapOfVm_route_travel_approverDTOToapproverOfficeUnitId = new ConcurrentHashMap<>();
//		mapOfVm_route_travel_approverDTOToapproverEmpId = new ConcurrentHashMap<>();
//		mapOfVm_route_travel_approverDTOToapproverPhoneNum = new ConcurrentHashMap<>();
//		mapOfVm_route_travel_approverDTOToapproverNameEn = new ConcurrentHashMap<>();
//		mapOfVm_route_travel_approverDTOToapproverNameBn = new ConcurrentHashMap<>();
//		mapOfVm_route_travel_approverDTOToapproverOfficeNameEn = new ConcurrentHashMap<>();
//		mapOfVm_route_travel_approverDTOToapproverOfficeNameBn = new ConcurrentHashMap<>();
//		mapOfVm_route_travel_approverDTOToapproverOfficeUnitNameEn = new ConcurrentHashMap<>();
//		mapOfVm_route_travel_approverDTOToapproverOfficeUnitNameBn = new ConcurrentHashMap<>();
//		mapOfVm_route_travel_approverDTOToapproverOfficeUnitOrgNameEn = new ConcurrentHashMap<>();
//		mapOfVm_route_travel_approverDTOToapproverOfficeUnitOrgNameBn = new ConcurrentHashMap<>();

		setDAO(new Vm_route_travel_approverDAO());

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Vm_route_travel_approverRepository getInstance(){
		if (instance == null){
			instance = new Vm_route_travel_approverRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(vm_route_travel_approverDAO == null)
		{
			return;
		}
		try {
			List<Vm_route_travel_approverDTO> vm_route_travel_approverDTOs = vm_route_travel_approverDAO.getAllVm_route_travel_approver(reloadAll);
			for(Vm_route_travel_approverDTO vm_route_travel_approverDTO : vm_route_travel_approverDTOs) {
				Vm_route_travel_approverDTO oldVm_route_travel_approverDTO = getVm_route_travel_approverDTOByIDWithoutClone(vm_route_travel_approverDTO.iD);
				if( oldVm_route_travel_approverDTO != null ) {
					mapOfVm_route_travel_approverDTOToiD.remove(oldVm_route_travel_approverDTO.iD);
				
//					if(mapOfVm_route_travel_approverDTOToinsertedByUserId.containsKey(oldVm_route_travel_approverDTO.insertedByUserId)) {
//						mapOfVm_route_travel_approverDTOToinsertedByUserId.get(oldVm_route_travel_approverDTO.insertedByUserId).remove(oldVm_route_travel_approverDTO);
//					}
//					if(mapOfVm_route_travel_approverDTOToinsertedByUserId.get(oldVm_route_travel_approverDTO.insertedByUserId).isEmpty()) {
//						mapOfVm_route_travel_approverDTOToinsertedByUserId.remove(oldVm_route_travel_approverDTO.insertedByUserId);
//					}
//
//					if(mapOfVm_route_travel_approverDTOToinsertedByOrganogramId.containsKey(oldVm_route_travel_approverDTO.insertedByOrganogramId)) {
//						mapOfVm_route_travel_approverDTOToinsertedByOrganogramId.get(oldVm_route_travel_approverDTO.insertedByOrganogramId).remove(oldVm_route_travel_approverDTO);
//					}
//					if(mapOfVm_route_travel_approverDTOToinsertedByOrganogramId.get(oldVm_route_travel_approverDTO.insertedByOrganogramId).isEmpty()) {
//						mapOfVm_route_travel_approverDTOToinsertedByOrganogramId.remove(oldVm_route_travel_approverDTO.insertedByOrganogramId);
//					}
//
//					if(mapOfVm_route_travel_approverDTOToinsertionDate.containsKey(oldVm_route_travel_approverDTO.insertionDate)) {
//						mapOfVm_route_travel_approverDTOToinsertionDate.get(oldVm_route_travel_approverDTO.insertionDate).remove(oldVm_route_travel_approverDTO);
//					}
//					if(mapOfVm_route_travel_approverDTOToinsertionDate.get(oldVm_route_travel_approverDTO.insertionDate).isEmpty()) {
//						mapOfVm_route_travel_approverDTOToinsertionDate.remove(oldVm_route_travel_approverDTO.insertionDate);
//					}
//
//					if(mapOfVm_route_travel_approverDTOTolastModificationTime.containsKey(oldVm_route_travel_approverDTO.lastModificationTime)) {
//						mapOfVm_route_travel_approverDTOTolastModificationTime.get(oldVm_route_travel_approverDTO.lastModificationTime).remove(oldVm_route_travel_approverDTO);
//					}
//					if(mapOfVm_route_travel_approverDTOTolastModificationTime.get(oldVm_route_travel_approverDTO.lastModificationTime).isEmpty()) {
//						mapOfVm_route_travel_approverDTOTolastModificationTime.remove(oldVm_route_travel_approverDTO.lastModificationTime);
//					}
//
//					if(mapOfVm_route_travel_approverDTOTosearchColumn.containsKey(oldVm_route_travel_approverDTO.searchColumn)) {
//						mapOfVm_route_travel_approverDTOTosearchColumn.get(oldVm_route_travel_approverDTO.searchColumn).remove(oldVm_route_travel_approverDTO);
//					}
//					if(mapOfVm_route_travel_approverDTOTosearchColumn.get(oldVm_route_travel_approverDTO.searchColumn).isEmpty()) {
//						mapOfVm_route_travel_approverDTOTosearchColumn.remove(oldVm_route_travel_approverDTO.searchColumn);
//					}
					
					if(mapOfVm_route_travel_approverDTOToapproverOrgId.containsKey(oldVm_route_travel_approverDTO.approverOrgId)) {
						mapOfVm_route_travel_approverDTOToapproverOrgId.get(oldVm_route_travel_approverDTO.approverOrgId).remove(oldVm_route_travel_approverDTO);
					}
					if(mapOfVm_route_travel_approverDTOToapproverOrgId.get(oldVm_route_travel_approverDTO.approverOrgId).isEmpty()) {
						mapOfVm_route_travel_approverDTOToapproverOrgId.remove(oldVm_route_travel_approverDTO.approverOrgId);
					}
					
//					if(mapOfVm_route_travel_approverDTOToapproverOfficeId.containsKey(oldVm_route_travel_approverDTO.approverOfficeId)) {
//						mapOfVm_route_travel_approverDTOToapproverOfficeId.get(oldVm_route_travel_approverDTO.approverOfficeId).remove(oldVm_route_travel_approverDTO);
//					}
//					if(mapOfVm_route_travel_approverDTOToapproverOfficeId.get(oldVm_route_travel_approverDTO.approverOfficeId).isEmpty()) {
//						mapOfVm_route_travel_approverDTOToapproverOfficeId.remove(oldVm_route_travel_approverDTO.approverOfficeId);
//					}
//
//					if(mapOfVm_route_travel_approverDTOToapproverOfficeUnitId.containsKey(oldVm_route_travel_approverDTO.approverOfficeUnitId)) {
//						mapOfVm_route_travel_approverDTOToapproverOfficeUnitId.get(oldVm_route_travel_approverDTO.approverOfficeUnitId).remove(oldVm_route_travel_approverDTO);
//					}
//					if(mapOfVm_route_travel_approverDTOToapproverOfficeUnitId.get(oldVm_route_travel_approverDTO.approverOfficeUnitId).isEmpty()) {
//						mapOfVm_route_travel_approverDTOToapproverOfficeUnitId.remove(oldVm_route_travel_approverDTO.approverOfficeUnitId);
//					}
//
//					if(mapOfVm_route_travel_approverDTOToapproverEmpId.containsKey(oldVm_route_travel_approverDTO.approverEmpId)) {
//						mapOfVm_route_travel_approverDTOToapproverEmpId.get(oldVm_route_travel_approverDTO.approverEmpId).remove(oldVm_route_travel_approverDTO);
//					}
//					if(mapOfVm_route_travel_approverDTOToapproverEmpId.get(oldVm_route_travel_approverDTO.approverEmpId).isEmpty()) {
//						mapOfVm_route_travel_approverDTOToapproverEmpId.remove(oldVm_route_travel_approverDTO.approverEmpId);
//					}
//
//					if(mapOfVm_route_travel_approverDTOToapproverPhoneNum.containsKey(oldVm_route_travel_approverDTO.approverPhoneNum)) {
//						mapOfVm_route_travel_approverDTOToapproverPhoneNum.get(oldVm_route_travel_approverDTO.approverPhoneNum).remove(oldVm_route_travel_approverDTO);
//					}
//					if(mapOfVm_route_travel_approverDTOToapproverPhoneNum.get(oldVm_route_travel_approverDTO.approverPhoneNum).isEmpty()) {
//						mapOfVm_route_travel_approverDTOToapproverPhoneNum.remove(oldVm_route_travel_approverDTO.approverPhoneNum);
//					}
//
//					if(mapOfVm_route_travel_approverDTOToapproverNameEn.containsKey(oldVm_route_travel_approverDTO.approverNameEn)) {
//						mapOfVm_route_travel_approverDTOToapproverNameEn.get(oldVm_route_travel_approverDTO.approverNameEn).remove(oldVm_route_travel_approverDTO);
//					}
//					if(mapOfVm_route_travel_approverDTOToapproverNameEn.get(oldVm_route_travel_approverDTO.approverNameEn).isEmpty()) {
//						mapOfVm_route_travel_approverDTOToapproverNameEn.remove(oldVm_route_travel_approverDTO.approverNameEn);
//					}
//
//					if(mapOfVm_route_travel_approverDTOToapproverNameBn.containsKey(oldVm_route_travel_approverDTO.approverNameBn)) {
//						mapOfVm_route_travel_approverDTOToapproverNameBn.get(oldVm_route_travel_approverDTO.approverNameBn).remove(oldVm_route_travel_approverDTO);
//					}
//					if(mapOfVm_route_travel_approverDTOToapproverNameBn.get(oldVm_route_travel_approverDTO.approverNameBn).isEmpty()) {
//						mapOfVm_route_travel_approverDTOToapproverNameBn.remove(oldVm_route_travel_approverDTO.approverNameBn);
//					}
//
//					if(mapOfVm_route_travel_approverDTOToapproverOfficeNameEn.containsKey(oldVm_route_travel_approverDTO.approverOfficeNameEn)) {
//						mapOfVm_route_travel_approverDTOToapproverOfficeNameEn.get(oldVm_route_travel_approverDTO.approverOfficeNameEn).remove(oldVm_route_travel_approverDTO);
//					}
//					if(mapOfVm_route_travel_approverDTOToapproverOfficeNameEn.get(oldVm_route_travel_approverDTO.approverOfficeNameEn).isEmpty()) {
//						mapOfVm_route_travel_approverDTOToapproverOfficeNameEn.remove(oldVm_route_travel_approverDTO.approverOfficeNameEn);
//					}
//
//					if(mapOfVm_route_travel_approverDTOToapproverOfficeNameBn.containsKey(oldVm_route_travel_approverDTO.approverOfficeNameBn)) {
//						mapOfVm_route_travel_approverDTOToapproverOfficeNameBn.get(oldVm_route_travel_approverDTO.approverOfficeNameBn).remove(oldVm_route_travel_approverDTO);
//					}
//					if(mapOfVm_route_travel_approverDTOToapproverOfficeNameBn.get(oldVm_route_travel_approverDTO.approverOfficeNameBn).isEmpty()) {
//						mapOfVm_route_travel_approverDTOToapproverOfficeNameBn.remove(oldVm_route_travel_approverDTO.approverOfficeNameBn);
//					}
//
//					if(mapOfVm_route_travel_approverDTOToapproverOfficeUnitNameEn.containsKey(oldVm_route_travel_approverDTO.approverOfficeUnitNameEn)) {
//						mapOfVm_route_travel_approverDTOToapproverOfficeUnitNameEn.get(oldVm_route_travel_approverDTO.approverOfficeUnitNameEn).remove(oldVm_route_travel_approverDTO);
//					}
//					if(mapOfVm_route_travel_approverDTOToapproverOfficeUnitNameEn.get(oldVm_route_travel_approverDTO.approverOfficeUnitNameEn).isEmpty()) {
//						mapOfVm_route_travel_approverDTOToapproverOfficeUnitNameEn.remove(oldVm_route_travel_approverDTO.approverOfficeUnitNameEn);
//					}
//
//					if(mapOfVm_route_travel_approverDTOToapproverOfficeUnitNameBn.containsKey(oldVm_route_travel_approverDTO.approverOfficeUnitNameBn)) {
//						mapOfVm_route_travel_approverDTOToapproverOfficeUnitNameBn.get(oldVm_route_travel_approverDTO.approverOfficeUnitNameBn).remove(oldVm_route_travel_approverDTO);
//					}
//					if(mapOfVm_route_travel_approverDTOToapproverOfficeUnitNameBn.get(oldVm_route_travel_approverDTO.approverOfficeUnitNameBn).isEmpty()) {
//						mapOfVm_route_travel_approverDTOToapproverOfficeUnitNameBn.remove(oldVm_route_travel_approverDTO.approverOfficeUnitNameBn);
//					}
//
//					if(mapOfVm_route_travel_approverDTOToapproverOfficeUnitOrgNameEn.containsKey(oldVm_route_travel_approverDTO.approverOfficeUnitOrgNameEn)) {
//						mapOfVm_route_travel_approverDTOToapproverOfficeUnitOrgNameEn.get(oldVm_route_travel_approverDTO.approverOfficeUnitOrgNameEn).remove(oldVm_route_travel_approverDTO);
//					}
//					if(mapOfVm_route_travel_approverDTOToapproverOfficeUnitOrgNameEn.get(oldVm_route_travel_approverDTO.approverOfficeUnitOrgNameEn).isEmpty()) {
//						mapOfVm_route_travel_approverDTOToapproverOfficeUnitOrgNameEn.remove(oldVm_route_travel_approverDTO.approverOfficeUnitOrgNameEn);
//					}
//
//					if(mapOfVm_route_travel_approverDTOToapproverOfficeUnitOrgNameBn.containsKey(oldVm_route_travel_approverDTO.approverOfficeUnitOrgNameBn)) {
//						mapOfVm_route_travel_approverDTOToapproverOfficeUnitOrgNameBn.get(oldVm_route_travel_approverDTO.approverOfficeUnitOrgNameBn).remove(oldVm_route_travel_approverDTO);
//					}
//					if(mapOfVm_route_travel_approverDTOToapproverOfficeUnitOrgNameBn.get(oldVm_route_travel_approverDTO.approverOfficeUnitOrgNameBn).isEmpty()) {
//						mapOfVm_route_travel_approverDTOToapproverOfficeUnitOrgNameBn.remove(oldVm_route_travel_approverDTO.approverOfficeUnitOrgNameBn);
//					}
					
					
				}
				if(vm_route_travel_approverDTO.isDeleted == 0)
				{
					
					mapOfVm_route_travel_approverDTOToiD.put(vm_route_travel_approverDTO.iD, vm_route_travel_approverDTO);
				
//					if( ! mapOfVm_route_travel_approverDTOToinsertedByUserId.containsKey(vm_route_travel_approverDTO.insertedByUserId)) {
//						mapOfVm_route_travel_approverDTOToinsertedByUserId.put(vm_route_travel_approverDTO.insertedByUserId, new HashSet<>());
//					}
//					mapOfVm_route_travel_approverDTOToinsertedByUserId.get(vm_route_travel_approverDTO.insertedByUserId).add(vm_route_travel_approverDTO);
//
//					if( ! mapOfVm_route_travel_approverDTOToinsertedByOrganogramId.containsKey(vm_route_travel_approverDTO.insertedByOrganogramId)) {
//						mapOfVm_route_travel_approverDTOToinsertedByOrganogramId.put(vm_route_travel_approverDTO.insertedByOrganogramId, new HashSet<>());
//					}
//					mapOfVm_route_travel_approverDTOToinsertedByOrganogramId.get(vm_route_travel_approverDTO.insertedByOrganogramId).add(vm_route_travel_approverDTO);
//
//					if( ! mapOfVm_route_travel_approverDTOToinsertionDate.containsKey(vm_route_travel_approverDTO.insertionDate)) {
//						mapOfVm_route_travel_approverDTOToinsertionDate.put(vm_route_travel_approverDTO.insertionDate, new HashSet<>());
//					}
//					mapOfVm_route_travel_approverDTOToinsertionDate.get(vm_route_travel_approverDTO.insertionDate).add(vm_route_travel_approverDTO);
//
//					if( ! mapOfVm_route_travel_approverDTOTolastModificationTime.containsKey(vm_route_travel_approverDTO.lastModificationTime)) {
//						mapOfVm_route_travel_approverDTOTolastModificationTime.put(vm_route_travel_approverDTO.lastModificationTime, new HashSet<>());
//					}
//					mapOfVm_route_travel_approverDTOTolastModificationTime.get(vm_route_travel_approverDTO.lastModificationTime).add(vm_route_travel_approverDTO);
//
//					if( ! mapOfVm_route_travel_approverDTOTosearchColumn.containsKey(vm_route_travel_approverDTO.searchColumn)) {
//						mapOfVm_route_travel_approverDTOTosearchColumn.put(vm_route_travel_approverDTO.searchColumn, new HashSet<>());
//					}
//					mapOfVm_route_travel_approverDTOTosearchColumn.get(vm_route_travel_approverDTO.searchColumn).add(vm_route_travel_approverDTO);
//
					if( ! mapOfVm_route_travel_approverDTOToapproverOrgId.containsKey(vm_route_travel_approverDTO.approverOrgId)) {
						mapOfVm_route_travel_approverDTOToapproverOrgId.put(vm_route_travel_approverDTO.approverOrgId, new HashSet<>());
					}
					mapOfVm_route_travel_approverDTOToapproverOrgId.get(vm_route_travel_approverDTO.approverOrgId).add(vm_route_travel_approverDTO);
					
//					if( ! mapOfVm_route_travel_approverDTOToapproverOfficeId.containsKey(vm_route_travel_approverDTO.approverOfficeId)) {
//						mapOfVm_route_travel_approverDTOToapproverOfficeId.put(vm_route_travel_approverDTO.approverOfficeId, new HashSet<>());
//					}
//					mapOfVm_route_travel_approverDTOToapproverOfficeId.get(vm_route_travel_approverDTO.approverOfficeId).add(vm_route_travel_approverDTO);
//
//					if( ! mapOfVm_route_travel_approverDTOToapproverOfficeUnitId.containsKey(vm_route_travel_approverDTO.approverOfficeUnitId)) {
//						mapOfVm_route_travel_approverDTOToapproverOfficeUnitId.put(vm_route_travel_approverDTO.approverOfficeUnitId, new HashSet<>());
//					}
//					mapOfVm_route_travel_approverDTOToapproverOfficeUnitId.get(vm_route_travel_approverDTO.approverOfficeUnitId).add(vm_route_travel_approverDTO);
//
//					if( ! mapOfVm_route_travel_approverDTOToapproverEmpId.containsKey(vm_route_travel_approverDTO.approverEmpId)) {
//						mapOfVm_route_travel_approverDTOToapproverEmpId.put(vm_route_travel_approverDTO.approverEmpId, new HashSet<>());
//					}
//					mapOfVm_route_travel_approverDTOToapproverEmpId.get(vm_route_travel_approverDTO.approverEmpId).add(vm_route_travel_approverDTO);
//
//					if( ! mapOfVm_route_travel_approverDTOToapproverPhoneNum.containsKey(vm_route_travel_approverDTO.approverPhoneNum)) {
//						mapOfVm_route_travel_approverDTOToapproverPhoneNum.put(vm_route_travel_approverDTO.approverPhoneNum, new HashSet<>());
//					}
//					mapOfVm_route_travel_approverDTOToapproverPhoneNum.get(vm_route_travel_approverDTO.approverPhoneNum).add(vm_route_travel_approverDTO);
//
//					if( ! mapOfVm_route_travel_approverDTOToapproverNameEn.containsKey(vm_route_travel_approverDTO.approverNameEn)) {
//						mapOfVm_route_travel_approverDTOToapproverNameEn.put(vm_route_travel_approverDTO.approverNameEn, new HashSet<>());
//					}
//					mapOfVm_route_travel_approverDTOToapproverNameEn.get(vm_route_travel_approverDTO.approverNameEn).add(vm_route_travel_approverDTO);
//
//					if( ! mapOfVm_route_travel_approverDTOToapproverNameBn.containsKey(vm_route_travel_approverDTO.approverNameBn)) {
//						mapOfVm_route_travel_approverDTOToapproverNameBn.put(vm_route_travel_approverDTO.approverNameBn, new HashSet<>());
//					}
//					mapOfVm_route_travel_approverDTOToapproverNameBn.get(vm_route_travel_approverDTO.approverNameBn).add(vm_route_travel_approverDTO);
//
//					if( ! mapOfVm_route_travel_approverDTOToapproverOfficeNameEn.containsKey(vm_route_travel_approverDTO.approverOfficeNameEn)) {
//						mapOfVm_route_travel_approverDTOToapproverOfficeNameEn.put(vm_route_travel_approverDTO.approverOfficeNameEn, new HashSet<>());
//					}
//					mapOfVm_route_travel_approverDTOToapproverOfficeNameEn.get(vm_route_travel_approverDTO.approverOfficeNameEn).add(vm_route_travel_approverDTO);
//
//					if( ! mapOfVm_route_travel_approverDTOToapproverOfficeNameBn.containsKey(vm_route_travel_approverDTO.approverOfficeNameBn)) {
//						mapOfVm_route_travel_approverDTOToapproverOfficeNameBn.put(vm_route_travel_approverDTO.approverOfficeNameBn, new HashSet<>());
//					}
//					mapOfVm_route_travel_approverDTOToapproverOfficeNameBn.get(vm_route_travel_approverDTO.approverOfficeNameBn).add(vm_route_travel_approverDTO);
//
//					if( ! mapOfVm_route_travel_approverDTOToapproverOfficeUnitNameEn.containsKey(vm_route_travel_approverDTO.approverOfficeUnitNameEn)) {
//						mapOfVm_route_travel_approverDTOToapproverOfficeUnitNameEn.put(vm_route_travel_approverDTO.approverOfficeUnitNameEn, new HashSet<>());
//					}
//					mapOfVm_route_travel_approverDTOToapproverOfficeUnitNameEn.get(vm_route_travel_approverDTO.approverOfficeUnitNameEn).add(vm_route_travel_approverDTO);
//
//					if( ! mapOfVm_route_travel_approverDTOToapproverOfficeUnitNameBn.containsKey(vm_route_travel_approverDTO.approverOfficeUnitNameBn)) {
//						mapOfVm_route_travel_approverDTOToapproverOfficeUnitNameBn.put(vm_route_travel_approverDTO.approverOfficeUnitNameBn, new HashSet<>());
//					}
//					mapOfVm_route_travel_approverDTOToapproverOfficeUnitNameBn.get(vm_route_travel_approverDTO.approverOfficeUnitNameBn).add(vm_route_travel_approverDTO);
//
//					if( ! mapOfVm_route_travel_approverDTOToapproverOfficeUnitOrgNameEn.containsKey(vm_route_travel_approverDTO.approverOfficeUnitOrgNameEn)) {
//						mapOfVm_route_travel_approverDTOToapproverOfficeUnitOrgNameEn.put(vm_route_travel_approverDTO.approverOfficeUnitOrgNameEn, new HashSet<>());
//					}
//					mapOfVm_route_travel_approverDTOToapproverOfficeUnitOrgNameEn.get(vm_route_travel_approverDTO.approverOfficeUnitOrgNameEn).add(vm_route_travel_approverDTO);
//
//					if( ! mapOfVm_route_travel_approverDTOToapproverOfficeUnitOrgNameBn.containsKey(vm_route_travel_approverDTO.approverOfficeUnitOrgNameBn)) {
//						mapOfVm_route_travel_approverDTOToapproverOfficeUnitOrgNameBn.put(vm_route_travel_approverDTO.approverOfficeUnitOrgNameBn, new HashSet<>());
//					}
//					mapOfVm_route_travel_approverDTOToapproverOfficeUnitOrgNameBn.get(vm_route_travel_approverDTO.approverOfficeUnitOrgNameBn).add(vm_route_travel_approverDTO);
//
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}

	public Vm_route_travel_approverDTO clone(Vm_route_travel_approverDTO dto) {
		String raw = gson.toJson(dto);
		return gson.fromJson(raw, Vm_route_travel_approverDTO.class);
	}

	public List<Vm_route_travel_approverDTO> clone(List<Vm_route_travel_approverDTO> dtoList) {
		return dtoList
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}

	public Vm_route_travel_approverDTO getVm_route_travel_approverDTOByIDWithoutClone( long ID){
		return mapOfVm_route_travel_approverDTOToiD.get(ID);
	}
	
	public List<Vm_route_travel_approverDTO> getVm_route_travel_approverList() {
		List <Vm_route_travel_approverDTO> vm_route_travel_approvers = new ArrayList<Vm_route_travel_approverDTO>(this.mapOfVm_route_travel_approverDTOToiD.values());
		return vm_route_travel_approvers;
	}
	
	
	public Vm_route_travel_approverDTO getVm_route_travel_approverDTOByID( long ID){
		return clone(mapOfVm_route_travel_approverDTOToiD.get(ID));
	}
	
	
//	public List<Vm_route_travel_approverDTO> getVm_route_travel_approverDTOByinserted_by_user_id(long inserted_by_user_id) {
//		return new ArrayList<>( mapOfVm_route_travel_approverDTOToinsertedByUserId.getOrDefault(inserted_by_user_id,new HashSet<>()));
//	}
//
//
//	public List<Vm_route_travel_approverDTO> getVm_route_travel_approverDTOByinserted_by_organogram_id(long inserted_by_organogram_id) {
//		return new ArrayList<>( mapOfVm_route_travel_approverDTOToinsertedByOrganogramId.getOrDefault(inserted_by_organogram_id,new HashSet<>()));
//	}
//
//
//	public List<Vm_route_travel_approverDTO> getVm_route_travel_approverDTOByinsertion_date(long insertion_date) {
//		return new ArrayList<>( mapOfVm_route_travel_approverDTOToinsertionDate.getOrDefault(insertion_date,new HashSet<>()));
//	}
//
//
//	public List<Vm_route_travel_approverDTO> getVm_route_travel_approverDTOBylastModificationTime(long lastModificationTime) {
//		return new ArrayList<>( mapOfVm_route_travel_approverDTOTolastModificationTime.getOrDefault(lastModificationTime,new HashSet<>()));
//	}
//
//
//	public List<Vm_route_travel_approverDTO> getVm_route_travel_approverDTOBysearch_column(String search_column) {
//		return new ArrayList<>( mapOfVm_route_travel_approverDTOTosearchColumn.getOrDefault(search_column,new HashSet<>()));
//	}
	
	
	public List<Vm_route_travel_approverDTO> getVm_route_travel_approverDTOByapprover_org_id(long approver_org_id) {
		return clone(new ArrayList<>( mapOfVm_route_travel_approverDTOToapproverOrgId.getOrDefault(approver_org_id,new HashSet<>())));
	}
	
	
//	public List<Vm_route_travel_approverDTO> getVm_route_travel_approverDTOByapprover_office_id(long approver_office_id) {
//		return new ArrayList<>( mapOfVm_route_travel_approverDTOToapproverOfficeId.getOrDefault(approver_office_id,new HashSet<>()));
//	}
//
//
//	public List<Vm_route_travel_approverDTO> getVm_route_travel_approverDTOByapprover_office_unit_id(long approver_office_unit_id) {
//		return new ArrayList<>( mapOfVm_route_travel_approverDTOToapproverOfficeUnitId.getOrDefault(approver_office_unit_id,new HashSet<>()));
//	}
//
//
//	public List<Vm_route_travel_approverDTO> getVm_route_travel_approverDTOByapprover_emp_id(long approver_emp_id) {
//		return new ArrayList<>( mapOfVm_route_travel_approverDTOToapproverEmpId.getOrDefault(approver_emp_id,new HashSet<>()));
//	}
//
//
//	public List<Vm_route_travel_approverDTO> getVm_route_travel_approverDTOByapprover_phone_num(String approver_phone_num) {
//		return new ArrayList<>( mapOfVm_route_travel_approverDTOToapproverPhoneNum.getOrDefault(approver_phone_num,new HashSet<>()));
//	}
//
//
//	public List<Vm_route_travel_approverDTO> getVm_route_travel_approverDTOByapprover_name_en(String approver_name_en) {
//		return new ArrayList<>( mapOfVm_route_travel_approverDTOToapproverNameEn.getOrDefault(approver_name_en,new HashSet<>()));
//	}
//
//
//	public List<Vm_route_travel_approverDTO> getVm_route_travel_approverDTOByapprover_name_bn(String approver_name_bn) {
//		return new ArrayList<>( mapOfVm_route_travel_approverDTOToapproverNameBn.getOrDefault(approver_name_bn,new HashSet<>()));
//	}
//
//
//	public List<Vm_route_travel_approverDTO> getVm_route_travel_approverDTOByapprover_office_name_en(String approver_office_name_en) {
//		return new ArrayList<>( mapOfVm_route_travel_approverDTOToapproverOfficeNameEn.getOrDefault(approver_office_name_en,new HashSet<>()));
//	}
//
//
//	public List<Vm_route_travel_approverDTO> getVm_route_travel_approverDTOByapprover_office_name_bn(String approver_office_name_bn) {
//		return new ArrayList<>( mapOfVm_route_travel_approverDTOToapproverOfficeNameBn.getOrDefault(approver_office_name_bn,new HashSet<>()));
//	}
//
//
//	public List<Vm_route_travel_approverDTO> getVm_route_travel_approverDTOByapprover_office_unit_name_en(String approver_office_unit_name_en) {
//		return new ArrayList<>( mapOfVm_route_travel_approverDTOToapproverOfficeUnitNameEn.getOrDefault(approver_office_unit_name_en,new HashSet<>()));
//	}
//
//
//	public List<Vm_route_travel_approverDTO> getVm_route_travel_approverDTOByapprover_office_unit_name_bn(String approver_office_unit_name_bn) {
//		return new ArrayList<>( mapOfVm_route_travel_approverDTOToapproverOfficeUnitNameBn.getOrDefault(approver_office_unit_name_bn,new HashSet<>()));
//	}
//
//
//	public List<Vm_route_travel_approverDTO> getVm_route_travel_approverDTOByapprover_office_unit_org_name_en(String approver_office_unit_org_name_en) {
//		return new ArrayList<>( mapOfVm_route_travel_approverDTOToapproverOfficeUnitOrgNameEn.getOrDefault(approver_office_unit_org_name_en,new HashSet<>()));
//	}
//
//
//	public List<Vm_route_travel_approverDTO> getVm_route_travel_approverDTOByapprover_office_unit_org_name_bn(String approver_office_unit_org_name_bn) {
//		return new ArrayList<>( mapOfVm_route_travel_approverDTOToapproverOfficeUnitOrgNameBn.getOrDefault(approver_office_unit_org_name_bn,new HashSet<>()));
//	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "vm_route_travel_approver";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


