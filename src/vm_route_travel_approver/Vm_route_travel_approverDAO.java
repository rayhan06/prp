package vm_route_travel_approver;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;

import common.ConnectionAndStatementUtil;
import sessionmanager.SessionConstants;
import java.sql.SQLException;


import org.apache.log4j.Logger;

import dbm.*;

import repository.RepositoryManager;

import util.*;
import pb.*;
import user.UserDTO;
import vm_route_travel_approver.Vm_route_travel_approverDTO;

public class Vm_route_travel_approverDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());
	

	
	public Vm_route_travel_approverDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";
		commonMaps = new Vm_route_travel_approverMAPS(tableName);
		columnNames = new String[] 
		{
			"ID",
			"inserted_by_user_id",
			"inserted_by_organogram_id",
			"insertion_date",
			"search_column",
			"approver_org_id",
			"approver_office_id",
			"approver_office_unit_id",
			"approver_emp_id",
			"approver_phone_num",
			"approver_name_en",
			"approver_name_bn",
			"approver_office_name_en",
			"approver_office_name_bn",
			"approver_office_unit_name_en",
			"approver_office_unit_name_bn",
			"approver_office_unit_org_name_en",
			"approver_office_unit_org_name_bn",
				"isDeleted",
				"lastModificationTime"
		};
	}
	
	public Vm_route_travel_approverDAO()
	{
		this("vm_route_travel_approver");
	}
	
	public void setSearchColumn(Vm_route_travel_approverDTO vm_route_travel_approverDTO)
	{
		vm_route_travel_approverDTO.searchColumn = "";
		vm_route_travel_approverDTO.searchColumn += vm_route_travel_approverDTO.approverPhoneNum + " ";
		vm_route_travel_approverDTO.searchColumn += vm_route_travel_approverDTO.approverNameEn + " ";
		vm_route_travel_approverDTO.searchColumn += vm_route_travel_approverDTO.approverNameBn + " ";
		vm_route_travel_approverDTO.searchColumn += vm_route_travel_approverDTO.approverOfficeNameEn + " ";
		vm_route_travel_approverDTO.searchColumn += vm_route_travel_approverDTO.approverOfficeNameBn + " ";
		vm_route_travel_approverDTO.searchColumn += vm_route_travel_approverDTO.approverOfficeUnitNameEn + " ";
		vm_route_travel_approverDTO.searchColumn += vm_route_travel_approverDTO.approverOfficeUnitNameBn + " ";
		vm_route_travel_approverDTO.searchColumn += vm_route_travel_approverDTO.approverOfficeUnitOrgNameEn + " ";
		vm_route_travel_approverDTO.searchColumn += vm_route_travel_approverDTO.approverOfficeUnitOrgNameBn + " ";
		
	}
	
	public void doSet(PreparedStatement ps, CommonDTO commonDTO, boolean isInsert) throws SQLException
	{
		Vm_route_travel_approverDTO vm_route_travel_approverDTO = (Vm_route_travel_approverDTO)commonDTO;
		int index = 1;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(vm_route_travel_approverDTO);
		if(isInsert)
		{
			ps.setObject(index++,vm_route_travel_approverDTO.iD);
		}
		ps.setObject(index++,vm_route_travel_approverDTO.insertedByUserId);
		ps.setObject(index++,vm_route_travel_approverDTO.insertedByOrganogramId);
		ps.setObject(index++,vm_route_travel_approverDTO.insertionDate);
		ps.setObject(index++,vm_route_travel_approverDTO.searchColumn);
		ps.setObject(index++,vm_route_travel_approverDTO.approverOrgId);
		ps.setObject(index++,vm_route_travel_approverDTO.approverOfficeId);
		ps.setObject(index++,vm_route_travel_approverDTO.approverOfficeUnitId);
		ps.setObject(index++,vm_route_travel_approverDTO.approverEmpId);
		ps.setObject(index++,vm_route_travel_approverDTO.approverPhoneNum);
		ps.setObject(index++,vm_route_travel_approverDTO.approverNameEn);
		ps.setObject(index++,vm_route_travel_approverDTO.approverNameBn);
		ps.setObject(index++,vm_route_travel_approverDTO.approverOfficeNameEn);
		ps.setObject(index++,vm_route_travel_approverDTO.approverOfficeNameBn);
		ps.setObject(index++,vm_route_travel_approverDTO.approverOfficeUnitNameEn);
		ps.setObject(index++,vm_route_travel_approverDTO.approverOfficeUnitNameBn);
		ps.setObject(index++,vm_route_travel_approverDTO.approverOfficeUnitOrgNameEn);
		ps.setObject(index++,vm_route_travel_approverDTO.approverOfficeUnitOrgNameBn);
		if(isInsert)
		{
			ps.setObject(index++, 0);
			ps.setObject(index++, lastModificationTime);
		}
	}
	
	public void get(Vm_route_travel_approverDTO vm_route_travel_approverDTO, ResultSet rs) throws SQLException
	{
		vm_route_travel_approverDTO.iD = rs.getLong("ID");
		vm_route_travel_approverDTO.insertedByUserId = rs.getLong("inserted_by_user_id");
		vm_route_travel_approverDTO.insertedByOrganogramId = rs.getLong("inserted_by_organogram_id");
		vm_route_travel_approverDTO.insertionDate = rs.getLong("insertion_date");
		vm_route_travel_approverDTO.isDeleted = rs.getInt("isDeleted");
		vm_route_travel_approverDTO.lastModificationTime = rs.getLong("lastModificationTime");
		vm_route_travel_approverDTO.searchColumn = rs.getString("search_column");
		vm_route_travel_approverDTO.approverOrgId = rs.getLong("approver_org_id");
		vm_route_travel_approverDTO.approverOfficeId = rs.getLong("approver_office_id");
		vm_route_travel_approverDTO.approverOfficeUnitId = rs.getLong("approver_office_unit_id");
		vm_route_travel_approverDTO.approverEmpId = rs.getLong("approver_emp_id");
		vm_route_travel_approverDTO.approverPhoneNum = rs.getString("approver_phone_num");
		vm_route_travel_approverDTO.approverNameEn = rs.getString("approver_name_en");
		vm_route_travel_approverDTO.approverNameBn = rs.getString("approver_name_bn");
		vm_route_travel_approverDTO.approverOfficeNameEn = rs.getString("approver_office_name_en");
		vm_route_travel_approverDTO.approverOfficeNameBn = rs.getString("approver_office_name_bn");
		vm_route_travel_approverDTO.approverOfficeUnitNameEn = rs.getString("approver_office_unit_name_en");
		vm_route_travel_approverDTO.approverOfficeUnitNameBn = rs.getString("approver_office_unit_name_bn");
		vm_route_travel_approverDTO.approverOfficeUnitOrgNameEn = rs.getString("approver_office_unit_org_name_en");
		vm_route_travel_approverDTO.approverOfficeUnitOrgNameBn = rs.getString("approver_office_unit_org_name_bn");
	}


	public Vm_route_travel_approverDTO build(ResultSet rs)
	{
		try
		{
			Vm_route_travel_approverDTO vm_route_travel_approverDTO =  new Vm_route_travel_approverDTO();
			vm_route_travel_approverDTO.iD = rs.getLong("ID");
			vm_route_travel_approverDTO.insertedByUserId = rs.getLong("inserted_by_user_id");
			vm_route_travel_approverDTO.insertedByOrganogramId = rs.getLong("inserted_by_organogram_id");
			vm_route_travel_approverDTO.insertionDate = rs.getLong("insertion_date");
			vm_route_travel_approverDTO.isDeleted = rs.getInt("isDeleted");
			vm_route_travel_approverDTO.lastModificationTime = rs.getLong("lastModificationTime");
			vm_route_travel_approverDTO.searchColumn = rs.getString("search_column");
			vm_route_travel_approverDTO.approverOrgId = rs.getLong("approver_org_id");
			vm_route_travel_approverDTO.approverOfficeId = rs.getLong("approver_office_id");
			vm_route_travel_approverDTO.approverOfficeUnitId = rs.getLong("approver_office_unit_id");
			vm_route_travel_approverDTO.approverEmpId = rs.getLong("approver_emp_id");
			vm_route_travel_approverDTO.approverPhoneNum = rs.getString("approver_phone_num");
			vm_route_travel_approverDTO.approverNameEn = rs.getString("approver_name_en");
			vm_route_travel_approverDTO.approverNameBn = rs.getString("approver_name_bn");
			vm_route_travel_approverDTO.approverOfficeNameEn = rs.getString("approver_office_name_en");
			vm_route_travel_approverDTO.approverOfficeNameBn = rs.getString("approver_office_name_bn");
			vm_route_travel_approverDTO.approverOfficeUnitNameEn = rs.getString("approver_office_unit_name_en");
			vm_route_travel_approverDTO.approverOfficeUnitNameBn = rs.getString("approver_office_unit_name_bn");
			vm_route_travel_approverDTO.approverOfficeUnitOrgNameEn = rs.getString("approver_office_unit_org_name_en");
			vm_route_travel_approverDTO.approverOfficeUnitOrgNameBn = rs.getString("approver_office_unit_org_name_bn");
			return vm_route_travel_approverDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
	
		
	

	//need another getter for repository
	public Vm_route_travel_approverDTO getDTOByID (long ID)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID = ? ";
		Vm_route_travel_approverDTO vm_route_travel_approverDTO =
				ConnectionAndStatementUtil.getT(sql, Arrays.asList(ID), this::build);
		return vm_route_travel_approverDTO;
	}




	public List<Vm_route_travel_approverDTO> getDTOs(Collection recordIDs)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID IN ( ";
		for(int i = 0;i<recordIDs.size();i++){
			if(i!=0){
				sql+=",";
			}
			sql+=((ArrayList)recordIDs).get(i);
		}
		sql+=")  order by lastModificationTime desc";
		printSql(sql);
		return ConnectionAndStatementUtil.getListOfT(sql, Arrays.asList(recordIDs.toArray()),this::build);
	}
	
	

	
	
	
	//add repository
	public List<Vm_route_travel_approverDTO> getAllVm_route_travel_approver (boolean isFirstReload)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ";
		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;
		}
		sql += " order by " + tableName + ".lastModificationTime desc";
		return ConnectionAndStatementUtil.getListOfT(sql,this::build);
	}

	
	public List<Vm_route_travel_approverDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO)
	{
		return getDTOs(p_searchCriteria, limit, offset, isPermanentTable, userDTO, "", false);
	}


	public List<Vm_route_travel_approverDTO> getDTOs(Hashtable  p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO,
			String filter, boolean tableHasJobCat)
	{
		List<Object> objectList = new ArrayList<Object>();

		String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, isPermanentTable, userDTO, filter, tableHasJobCat, objectList);
		return ConnectionAndStatementUtil.getListOfT(sql,objectList,this::build);
	}
	public String getSqlWithSearchCriteriaForNormalSearch(Hashtable p_searchCriteria, int limit, int offset, int category,
			UserDTO userDTO, String filter, boolean tableHasJobCat, List<Object> objectList)
    {
		boolean viewAll = false;
		
		if(p_searchCriteria != null && p_searchCriteria.get("ViewAll") != null)
		{
			System.out.println("ViewAll = " + p_searchCriteria.get("ViewAll"));
			viewAll = true;
		}
		else
		{
			System.out.println("ViewAll is null ");
		}
		
		String sql = "SELECT ";
		if(category == GETIDS)
		{
			sql += " " + tableName + ".ID as ID ";
		}
		else if(category == GETCOUNT)
		{
			sql += " count( " + tableName + ".ID) as countID ";
		}
		else if(category == GETDTOS)
		{
			sql += " distinct " + tableName + ".* ";
		}
		sql += "FROM " + tableName + " ";
		sql += joinSQL;
		

		
		String AnyfieldSql = "";
		String AllFieldSql = "";
		
		if(p_searchCriteria != null)
		{
						
			
			Enumeration names = p_searchCriteria.keys();
			String str, value;
			
			AnyfieldSql = "(";
			
			if(p_searchCriteria.get("AnyField")!= null && !p_searchCriteria.get("AnyField").toString().equalsIgnoreCase(""))
			{
				AnyfieldSql+= tableName + ".search_column like ?";
				objectList.add("%" + p_searchCriteria.get("AnyField").toString() + "%");
			}
			AnyfieldSql += ")";
			System.out.println("AnyfieldSql = " + AnyfieldSql);
			
			AllFieldSql = "(";
			int i = 0;
			while(names.hasMoreElements())
			{				
				str = (String) names.nextElement();
				value = (String)p_searchCriteria.get(str);
		        System.out.println(str + ": " + value);
				if(value != null && !value.equalsIgnoreCase("") && (
						 str.equals("insertion_date_start")
						|| str.equals("insertion_date_end")
						|| str.equals("approver_phone_num")
						|| str.equals("approver_name_en")
						|| str.equals("approver_name_bn")
						|| str.equals("approver_office_name_en")
						|| str.equals("approver_office_name_bn")
						|| str.equals("approver_office_unit_name_en")
						|| str.equals("approver_office_unit_name_bn")
						|| str.equals("approver_office_unit_org_name_en")
						|| str.equals("approver_office_unit_org_name_bn")
				)
						
				)
				{
					if(p_searchCriteria.get(str).equals("any"))
					{
						continue;
					}

					if( i > 0)
					{
						AllFieldSql+= " AND  ";
					}
					
					 if(str.equals("insertion_date_start"))
					{
						AllFieldSql += "" + tableName + ".insertion_date >= " + "?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
					else if(str.equals("insertion_date_end"))
					{
						AllFieldSql += "" + tableName + ".insertion_date <= " + "?";
						objectList.add(Long.parseLong((String)p_searchCriteria.get(str)));
						i ++;
					}
					else if(str.equals("approver_phone_num"))
					{
						AllFieldSql += "" + tableName + ".approver_phone_num like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("approver_name_en"))
					{
						AllFieldSql += "" + tableName + ".approver_name_en like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("approver_name_bn"))
					{
						AllFieldSql += "" + tableName + ".approver_name_bn like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("approver_office_name_en"))
					{
						AllFieldSql += "" + tableName + ".approver_office_name_en like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("approver_office_name_bn"))
					{
						AllFieldSql += "" + tableName + ".approver_office_name_bn like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("approver_office_unit_name_en"))
					{
						AllFieldSql += "" + tableName + ".approver_office_unit_name_en like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("approver_office_unit_name_bn"))
					{
						AllFieldSql += "" + tableName + ".approver_office_unit_name_bn like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("approver_office_unit_org_name_en"))
					{
						AllFieldSql += "" + tableName + ".approver_office_unit_org_name_en like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					else if(str.equals("approver_office_unit_org_name_bn"))
					{
						AllFieldSql += "" + tableName + ".approver_office_unit_org_name_bn like " + "?";
                        objectList.add("%" + p_searchCriteria.get(str) + "%");
						i ++;
					}
					
					
				}
			}
			
			AllFieldSql += ")";
			System.out.println("AllFieldSql = " + AllFieldSql);
			
			
		}
		
		
		sql += " WHERE ";
		
		sql += " (" + tableName + ".isDeleted = 0 ";			
		sql += ")";
		
		
		if(!filter.equalsIgnoreCase(""))
		{
			sql += " and " + filter + " ";
		}
		
		if(!AnyfieldSql.equals("()") && !AnyfieldSql.equals(""))
		{
			sql += " AND " + AnyfieldSql;
			
		}
		if(!AllFieldSql.equals("()") && !AllFieldSql.equals(""))
		{			
			sql += " AND " + AllFieldSql;
		}
		
	
			
		sql += " order by " + tableName + ".lastModificationTime desc ";

		printSql(sql);
		
		if(limit >= 0)
		{
			sql += " limit " + limit;
		}
		if(offset >= 0)
		{
			sql += " offset " + offset;
		}
		
		System.out.println("-------------- sql = " + sql);
		
		return sql;
    }

	public void deleteAll() throws Exception {
		long lastModificationTime = System.currentTimeMillis();
		StringBuilder sqlBuilder = new StringBuilder("UPDATE ")
				.append(tableName)
				.append(" SET isDeleted=1,lastModificationTime=")
				.append(lastModificationTime)
				.append(" WHERE TRUE ") ;
		ConnectionAndStatementUtil.getWriteConnectionAndStatement(model -> {
			String sql = sqlBuilder.toString();
			Connection connection = model.getConnection();
			Statement stmt = model.getStatement();
			try {
				logger.debug(sql);
				stmt.execute(sql);
				recordUpdateTime(connection, lastModificationTime);
			} catch (SQLException ex) {
				logger.error(ex);
			}
		});
	}
				
}
	