package job_applicant_photo_signature;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class Job_applicant_photo_signatureRepository implements Repository {
	Job_applicant_photo_signatureDAO job_applicant_photo_signatureDAO = null;
	
	public void setDAO(Job_applicant_photo_signatureDAO job_applicant_photo_signatureDAO)
	{
		this.job_applicant_photo_signatureDAO = job_applicant_photo_signatureDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Job_applicant_photo_signatureRepository.class);
	Map<Long, Job_applicant_photo_signatureDTO>mapOfJob_applicant_photo_signatureDTOToiD;
	Map<Long, Set<Job_applicant_photo_signatureDTO> >mapOfJob_applicant_photo_signatureDTOTojobApplicantId;
	Map<Long, Set<Job_applicant_photo_signatureDTO> >mapOfJob_applicant_photo_signatureDTOTophotoOrSignature;
	Map<Long, Set<Job_applicant_photo_signatureDTO> >mapOfJob_applicant_photo_signatureDTOTofilesDropzone;
	Map<Long, Set<Job_applicant_photo_signatureDTO> >mapOfJob_applicant_photo_signatureDTOToinsertionDate;
	Map<Long, Set<Job_applicant_photo_signatureDTO> >mapOfJob_applicant_photo_signatureDTOToinsertedByUserId;
	Map<String, Set<Job_applicant_photo_signatureDTO> >mapOfJob_applicant_photo_signatureDTOTomodifiedBy;
	Map<Long, Set<Job_applicant_photo_signatureDTO> >mapOfJob_applicant_photo_signatureDTOTolastModificationTime;


	static Job_applicant_photo_signatureRepository instance = null;  
	private Job_applicant_photo_signatureRepository(){
		mapOfJob_applicant_photo_signatureDTOToiD = new ConcurrentHashMap<>();
		mapOfJob_applicant_photo_signatureDTOTojobApplicantId = new ConcurrentHashMap<>();
		mapOfJob_applicant_photo_signatureDTOTophotoOrSignature = new ConcurrentHashMap<>();
		mapOfJob_applicant_photo_signatureDTOTofilesDropzone = new ConcurrentHashMap<>();
		mapOfJob_applicant_photo_signatureDTOToinsertionDate = new ConcurrentHashMap<>();
		mapOfJob_applicant_photo_signatureDTOToinsertedByUserId = new ConcurrentHashMap<>();
		mapOfJob_applicant_photo_signatureDTOTomodifiedBy = new ConcurrentHashMap<>();
		mapOfJob_applicant_photo_signatureDTOTolastModificationTime = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Job_applicant_photo_signatureRepository getInstance(){
		if (instance == null){
			instance = new Job_applicant_photo_signatureRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(job_applicant_photo_signatureDAO == null)
		{
			return;
		}
		try {
			List<Job_applicant_photo_signatureDTO> job_applicant_photo_signatureDTOs = job_applicant_photo_signatureDAO.getAllJob_applicant_photo_signature(reloadAll);
			for(Job_applicant_photo_signatureDTO job_applicant_photo_signatureDTO : job_applicant_photo_signatureDTOs) {
				Job_applicant_photo_signatureDTO oldJob_applicant_photo_signatureDTO = getJob_applicant_photo_signatureDTOByID(job_applicant_photo_signatureDTO.iD);
				if( oldJob_applicant_photo_signatureDTO != null ) {
					mapOfJob_applicant_photo_signatureDTOToiD.remove(oldJob_applicant_photo_signatureDTO.iD);
				
					if(mapOfJob_applicant_photo_signatureDTOTojobApplicantId.containsKey(oldJob_applicant_photo_signatureDTO.jobApplicantId)) {
						mapOfJob_applicant_photo_signatureDTOTojobApplicantId.get(oldJob_applicant_photo_signatureDTO.jobApplicantId).remove(oldJob_applicant_photo_signatureDTO);
					}
					if(mapOfJob_applicant_photo_signatureDTOTojobApplicantId.get(oldJob_applicant_photo_signatureDTO.jobApplicantId).isEmpty()) {
						mapOfJob_applicant_photo_signatureDTOTojobApplicantId.remove(oldJob_applicant_photo_signatureDTO.jobApplicantId);
					}
					
					if(mapOfJob_applicant_photo_signatureDTOTophotoOrSignature.containsKey(oldJob_applicant_photo_signatureDTO.photoOrSignature)) {
						mapOfJob_applicant_photo_signatureDTOTophotoOrSignature.get(oldJob_applicant_photo_signatureDTO.photoOrSignature).remove(oldJob_applicant_photo_signatureDTO);
					}
					if(mapOfJob_applicant_photo_signatureDTOTophotoOrSignature.get(oldJob_applicant_photo_signatureDTO.photoOrSignature).isEmpty()) {
						mapOfJob_applicant_photo_signatureDTOTophotoOrSignature.remove(oldJob_applicant_photo_signatureDTO.photoOrSignature);
					}
					
					if(mapOfJob_applicant_photo_signatureDTOTofilesDropzone.containsKey(oldJob_applicant_photo_signatureDTO.filesDropzone)) {
						mapOfJob_applicant_photo_signatureDTOTofilesDropzone.get(oldJob_applicant_photo_signatureDTO.filesDropzone).remove(oldJob_applicant_photo_signatureDTO);
					}
					if(mapOfJob_applicant_photo_signatureDTOTofilesDropzone.get(oldJob_applicant_photo_signatureDTO.filesDropzone).isEmpty()) {
						mapOfJob_applicant_photo_signatureDTOTofilesDropzone.remove(oldJob_applicant_photo_signatureDTO.filesDropzone);
					}
					
					if(mapOfJob_applicant_photo_signatureDTOToinsertionDate.containsKey(oldJob_applicant_photo_signatureDTO.insertionDate)) {
						mapOfJob_applicant_photo_signatureDTOToinsertionDate.get(oldJob_applicant_photo_signatureDTO.insertionDate).remove(oldJob_applicant_photo_signatureDTO);
					}
					if(mapOfJob_applicant_photo_signatureDTOToinsertionDate.get(oldJob_applicant_photo_signatureDTO.insertionDate).isEmpty()) {
						mapOfJob_applicant_photo_signatureDTOToinsertionDate.remove(oldJob_applicant_photo_signatureDTO.insertionDate);
					}
					
					if(mapOfJob_applicant_photo_signatureDTOToinsertedByUserId.containsKey(oldJob_applicant_photo_signatureDTO.insertedByUserId)) {
						mapOfJob_applicant_photo_signatureDTOToinsertedByUserId.get(oldJob_applicant_photo_signatureDTO.insertedByUserId).remove(oldJob_applicant_photo_signatureDTO);
					}
					if(mapOfJob_applicant_photo_signatureDTOToinsertedByUserId.get(oldJob_applicant_photo_signatureDTO.insertedByUserId).isEmpty()) {
						mapOfJob_applicant_photo_signatureDTOToinsertedByUserId.remove(oldJob_applicant_photo_signatureDTO.insertedByUserId);
					}
					
					if(mapOfJob_applicant_photo_signatureDTOTomodifiedBy.containsKey(oldJob_applicant_photo_signatureDTO.modifiedBy)) {
						mapOfJob_applicant_photo_signatureDTOTomodifiedBy.get(oldJob_applicant_photo_signatureDTO.modifiedBy).remove(oldJob_applicant_photo_signatureDTO);
					}
					if(mapOfJob_applicant_photo_signatureDTOTomodifiedBy.get(oldJob_applicant_photo_signatureDTO.modifiedBy).isEmpty()) {
						mapOfJob_applicant_photo_signatureDTOTomodifiedBy.remove(oldJob_applicant_photo_signatureDTO.modifiedBy);
					}
					
					if(mapOfJob_applicant_photo_signatureDTOTolastModificationTime.containsKey(oldJob_applicant_photo_signatureDTO.lastModificationTime)) {
						mapOfJob_applicant_photo_signatureDTOTolastModificationTime.get(oldJob_applicant_photo_signatureDTO.lastModificationTime).remove(oldJob_applicant_photo_signatureDTO);
					}
					if(mapOfJob_applicant_photo_signatureDTOTolastModificationTime.get(oldJob_applicant_photo_signatureDTO.lastModificationTime).isEmpty()) {
						mapOfJob_applicant_photo_signatureDTOTolastModificationTime.remove(oldJob_applicant_photo_signatureDTO.lastModificationTime);
					}
					
					
				}
				if(job_applicant_photo_signatureDTO.isDeleted == 0) 
				{
					
					mapOfJob_applicant_photo_signatureDTOToiD.put(job_applicant_photo_signatureDTO.iD, job_applicant_photo_signatureDTO);
				
					if( ! mapOfJob_applicant_photo_signatureDTOTojobApplicantId.containsKey(job_applicant_photo_signatureDTO.jobApplicantId)) {
						mapOfJob_applicant_photo_signatureDTOTojobApplicantId.put(job_applicant_photo_signatureDTO.jobApplicantId, new HashSet<>());
					}
					mapOfJob_applicant_photo_signatureDTOTojobApplicantId.get(job_applicant_photo_signatureDTO.jobApplicantId).add(job_applicant_photo_signatureDTO);
					
					if( ! mapOfJob_applicant_photo_signatureDTOTophotoOrSignature.containsKey(job_applicant_photo_signatureDTO.photoOrSignature)) {
						mapOfJob_applicant_photo_signatureDTOTophotoOrSignature.put(job_applicant_photo_signatureDTO.photoOrSignature, new HashSet<>());
					}
					mapOfJob_applicant_photo_signatureDTOTophotoOrSignature.get(job_applicant_photo_signatureDTO.photoOrSignature).add(job_applicant_photo_signatureDTO);
					
					if( ! mapOfJob_applicant_photo_signatureDTOTofilesDropzone.containsKey(job_applicant_photo_signatureDTO.filesDropzone)) {
						mapOfJob_applicant_photo_signatureDTOTofilesDropzone.put(job_applicant_photo_signatureDTO.filesDropzone, new HashSet<>());
					}
					mapOfJob_applicant_photo_signatureDTOTofilesDropzone.get(job_applicant_photo_signatureDTO.filesDropzone).add(job_applicant_photo_signatureDTO);
					
					if( ! mapOfJob_applicant_photo_signatureDTOToinsertionDate.containsKey(job_applicant_photo_signatureDTO.insertionDate)) {
						mapOfJob_applicant_photo_signatureDTOToinsertionDate.put(job_applicant_photo_signatureDTO.insertionDate, new HashSet<>());
					}
					mapOfJob_applicant_photo_signatureDTOToinsertionDate.get(job_applicant_photo_signatureDTO.insertionDate).add(job_applicant_photo_signatureDTO);
					
					if( ! mapOfJob_applicant_photo_signatureDTOToinsertedByUserId.containsKey(job_applicant_photo_signatureDTO.insertedByUserId)) {
						mapOfJob_applicant_photo_signatureDTOToinsertedByUserId.put(job_applicant_photo_signatureDTO.insertedByUserId, new HashSet<>());
					}
					mapOfJob_applicant_photo_signatureDTOToinsertedByUserId.get(job_applicant_photo_signatureDTO.insertedByUserId).add(job_applicant_photo_signatureDTO);
					
					if( ! mapOfJob_applicant_photo_signatureDTOTomodifiedBy.containsKey(job_applicant_photo_signatureDTO.modifiedBy)) {
						mapOfJob_applicant_photo_signatureDTOTomodifiedBy.put(job_applicant_photo_signatureDTO.modifiedBy, new HashSet<>());
					}
					mapOfJob_applicant_photo_signatureDTOTomodifiedBy.get(job_applicant_photo_signatureDTO.modifiedBy).add(job_applicant_photo_signatureDTO);
					
					if( ! mapOfJob_applicant_photo_signatureDTOTolastModificationTime.containsKey(job_applicant_photo_signatureDTO.lastModificationTime)) {
						mapOfJob_applicant_photo_signatureDTOTolastModificationTime.put(job_applicant_photo_signatureDTO.lastModificationTime, new HashSet<>());
					}
					mapOfJob_applicant_photo_signatureDTOTolastModificationTime.get(job_applicant_photo_signatureDTO.lastModificationTime).add(job_applicant_photo_signatureDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Job_applicant_photo_signatureDTO> getJob_applicant_photo_signatureList() {
		List <Job_applicant_photo_signatureDTO> job_applicant_photo_signatures = new ArrayList<Job_applicant_photo_signatureDTO>(this.mapOfJob_applicant_photo_signatureDTOToiD.values());
		return job_applicant_photo_signatures;
	}
	
	
	public Job_applicant_photo_signatureDTO getJob_applicant_photo_signatureDTOByID( long ID){
		return mapOfJob_applicant_photo_signatureDTOToiD.get(ID);
	}
	
	
	public List<Job_applicant_photo_signatureDTO> getJob_applicant_photo_signatureDTOByjob_applicant_id(long job_applicant_id) {
		return new ArrayList<>( mapOfJob_applicant_photo_signatureDTOTojobApplicantId.getOrDefault(job_applicant_id,new HashSet<>()));
	}
	
	
	public List<Job_applicant_photo_signatureDTO> getJob_applicant_photo_signatureDTOByphoto_or_signature(long photo_or_signature) {
		return new ArrayList<>( mapOfJob_applicant_photo_signatureDTOTophotoOrSignature.getOrDefault(photo_or_signature,new HashSet<>()));
	}
	
	
	public List<Job_applicant_photo_signatureDTO> getJob_applicant_photo_signatureDTOByfilesDropzone(long filesDropzone) {
		return new ArrayList<>( mapOfJob_applicant_photo_signatureDTOTofilesDropzone.getOrDefault(filesDropzone,new HashSet<>()));
	}
	
	
	public List<Job_applicant_photo_signatureDTO> getJob_applicant_photo_signatureDTOByinsertion_date(long insertion_date) {
		return new ArrayList<>( mapOfJob_applicant_photo_signatureDTOToinsertionDate.getOrDefault(insertion_date,new HashSet<>()));
	}
	
	
	public List<Job_applicant_photo_signatureDTO> getJob_applicant_photo_signatureDTOByinserted_by_user_id(long inserted_by_user_id) {
		return new ArrayList<>( mapOfJob_applicant_photo_signatureDTOToinsertedByUserId.getOrDefault(inserted_by_user_id,new HashSet<>()));
	}
	
	
	public List<Job_applicant_photo_signatureDTO> getJob_applicant_photo_signatureDTOBymodified_by(String modified_by) {
		return new ArrayList<>( mapOfJob_applicant_photo_signatureDTOTomodifiedBy.getOrDefault(modified_by,new HashSet<>()));
	}
	
	
	public List<Job_applicant_photo_signatureDTO> getJob_applicant_photo_signatureDTOBylastModificationTime(long lastModificationTime) {
		return new ArrayList<>( mapOfJob_applicant_photo_signatureDTOTolastModificationTime.getOrDefault(lastModificationTime,new HashSet<>()));
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "job_applicant_photo_signature";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


