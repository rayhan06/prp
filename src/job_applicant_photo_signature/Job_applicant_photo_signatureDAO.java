package job_applicant_photo_signature;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;
import java.util.Map.Entry;

import common.ConnectionAndStatementUtil;
import files.FilesDAO;
import files.FilesDTO;
import org.apache.commons.io.IOUtils;
import sessionmanager.SessionConstants;
import java.sql.SQLException;


import org.apache.log4j.Logger;

import dbm.*;

import repository.RepositoryManager;

import util.*;
import pb.*;
import user.UserDTO;
import vm_vehicle_office_assignment.Vm_vehicle_office_assignmentDTO;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;

public class Job_applicant_photo_signatureDAO  extends NavigationService4
{
	
	Logger logger = Logger.getLogger(getClass());
	
	
	public Job_applicant_photo_signatureDAO(String tableName)
	{
		super(tableName);
		joinSQL = "";
	}

	@Override
	public Collection getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO) {
		return null;
	}

	public Job_applicant_photo_signatureDAO()
	{
		this("job_applicant_photo_signature");		
	}
	
	public void get(Job_applicant_photo_signatureDTO job_applicant_photo_signatureDTO, ResultSet rs) throws SQLException
	{
		job_applicant_photo_signatureDTO.iD = rs.getLong("ID");
		job_applicant_photo_signatureDTO.jobApplicantId = rs.getLong("job_applicant_id");
		job_applicant_photo_signatureDTO.photoOrSignature = rs.getLong("photo_or_signature");
		job_applicant_photo_signatureDTO.filesDropzone = rs.getLong("filesDropzone");
		job_applicant_photo_signatureDTO.insertionDate = rs.getLong("insertion_date");
		job_applicant_photo_signatureDTO.insertedByUserId = rs.getLong("inserted_by_user_id");
		job_applicant_photo_signatureDTO.modifiedBy = rs.getString("modified_by");
		job_applicant_photo_signatureDTO.isDeleted = rs.getInt("isDeleted");
		job_applicant_photo_signatureDTO.lastModificationTime = rs.getLong("lastModificationTime");
	}

	public Job_applicant_photo_signatureDTO build(ResultSet rs)
	{
		try
		{
			Job_applicant_photo_signatureDTO job_applicant_photo_signatureDTO = new Job_applicant_photo_signatureDTO();
			job_applicant_photo_signatureDTO.iD = rs.getLong("ID");
			job_applicant_photo_signatureDTO.jobApplicantId = rs.getLong("job_applicant_id");
			job_applicant_photo_signatureDTO.photoOrSignature = rs.getLong("photo_or_signature");
			job_applicant_photo_signatureDTO.filesDropzone = rs.getLong("filesDropzone");
			job_applicant_photo_signatureDTO.insertionDate = rs.getLong("insertion_date");
			job_applicant_photo_signatureDTO.insertedByUserId = rs.getLong("inserted_by_user_id");
			job_applicant_photo_signatureDTO.modifiedBy = rs.getString("modified_by");
			job_applicant_photo_signatureDTO.isDeleted = rs.getInt("isDeleted");
			job_applicant_photo_signatureDTO.lastModificationTime = rs.getLong("lastModificationTime");
			return job_applicant_photo_signatureDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}


	public String validate(Job_applicant_photo_signatureDTO commonDTO) throws Exception
	{

		int validated = 1;

		FilesDAO filesDAO = new FilesDAO();
		List<FilesDTO> fileDropzoneDTOList = filesDAO.getDTOsByFileID(commonDTO.filesDropzone);

		String responseMessage = "";
		String[] fileTypes = {"Photo", "Signature"};
		String fileType = fileTypes[(int) (commonDTO.photoOrSignature - 1)];

		if (fileDropzoneDTOList.isEmpty())return fileType + " File Size/Format is Invalid!";

		FilesDTO file = fileDropzoneDTOList.get(fileDropzoneDTOList.size()-1);

		String fileSizeString = String.valueOf((double) (file.fileBlob.length)/1024.0);
		InputStream in = new ByteArrayInputStream(file.fileBlob);

		BufferedImage buf = ImageIO.read(in);
		int height = buf.getHeight();
		int width = buf.getWidth();

		String heightString = String.valueOf(height);
		String widthString = String.valueOf(width);

		long fileSize = file.fileBlob.length;

		if (commonDTO.photoOrSignature == 1) {
			if (fileSize> (1024 * 100)) validated = -1;
			if (height != 300 || width != 300) validated = -1;
		} else if (commonDTO.photoOrSignature == 2) {
			if (fileSize > (1024 * 60)) {
				validated = -1;
			}
			if (height != 80 || width != 300) {
				validated = -1;
			}
		}

		if (validated == 1) {
			for (int i=0; i< fileDropzoneDTOList.size()-1; i++) {
				filesDAO.delete(fileDropzoneDTOList.get(i).iD);
			}
			responseMessage = fileType + " is Valid!";
		}
		else if (validated == -1) {

			String invalidMessage = " File Size/Format is Invalid (width = " + widthString + ", height = " + heightString + " and size = " + fileSizeString + " KB)!";

			responseMessage = fileType + invalidMessage;
		}

		return responseMessage;
	}

	//need another getter for repository
	public Job_applicant_photo_signatureDTO getDTOByID (long ID)
	{
		String sql = "SELECT * FROM " + tableName + " WHERE ID = ? ";
		Job_applicant_photo_signatureDTO job_applicant_photo_signatureDTO =
				ConnectionAndStatementUtil.getT(sql, Arrays.asList(ID), this::build);
		return job_applicant_photo_signatureDTO;
	}
	

	
	
	public List<Job_applicant_photo_signatureDTO> getDTOs(Collection recordIDs){
		if(recordIDs.isEmpty()){
			return new ArrayList<>();
		}
		String sql = "SELECT * FROM " + tableName + " WHERE ID IN ( ";
		for(int i = 0;i<recordIDs.size();i++){
			if(i!=0){
				sql+=",";
			}
			sql+= " ? ";
		}
		sql+=")  order by lastModificationTime desc";
		printSql(sql);
		return ConnectionAndStatementUtil.getListOfT(sql, Arrays.asList(recordIDs.toArray()),this::build);
	
	}
	
	

	
	
	
	//add repository
	public List<Job_applicant_photo_signatureDTO> getAllJob_applicant_photo_signature (boolean isFirstReload)
    {

		String sql = "SELECT * FROM job_applicant_photo_signature";
		sql += " WHERE ";
	

		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by job_applicant_photo_signature.lastModificationTime desc";
		printSql(sql);

		return ConnectionAndStatementUtil.getListOfT(sql, this::build);
    }

	public List<Job_applicant_photo_signatureDTO> getDTOByJobApplicantID (long ID)
	{

		String sql = "SELECT * FROM job_applicant_photo_signature";

		sql += " WHERE job_applicant_id= ? ";

		sql+=" AND isDeleted =  0";

		sql += " order by job_applicant_photo_signature.lastModificationTime desc";
		printSql(sql);
		return ConnectionAndStatementUtil.getListOfT(sql, Arrays.asList(ID), this::build);
	}

	public List<Job_applicant_photo_signatureDTO> getDTOByJobApplicantIDAndFileType (long ID, int photoOrSignature)
	{

		String sql = "SELECT * FROM job_applicant_photo_signature";

		sql += " WHERE job_applicant_id = ? " ;

		sql+=" AND photo_or_signature =  ? ";

		sql+=" AND isDeleted =  0";

		sql += " order by job_applicant_photo_signature.lastModificationTime desc";
		printSql(sql);

		return ConnectionAndStatementUtil.getListOfT(sql, Arrays.asList(ID, photoOrSignature), this::build);
	}
				
}
	