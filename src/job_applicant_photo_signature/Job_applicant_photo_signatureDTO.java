package job_applicant_photo_signature;
import java.util.*; 
import util.*; 


public class Job_applicant_photo_signatureDTO extends CommonDTO
{

	public long jobApplicantId = 0;
	public long photoOrSignature = 0;
	public long filesDropzone = -1;
	public long insertionDate = 0;
	public long insertedByUserId = 0;
    public String modifiedBy = "";
	
	
    @Override
	public String toString() {
            return "$Job_applicant_photo_signatureDTO[" +
            " iD = " + iD +
            " jobApplicantId = " + jobApplicantId +
            " photoOrSignature = " + photoOrSignature +
            " filesDropzone = " + filesDropzone +
            " insertionDate = " + insertionDate +
            " insertedByUserId = " + insertedByUserId +
            " modifiedBy = " + modifiedBy +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}