package job_applicant_photo_signature;

import files.FilesDTO;
import util.CommonDTO;

import java.util.ArrayList;
import java.util.List;


public class Job_applicant_photo_signatureSummaryDTO extends CommonDTO
{

	public long jobApplicantId = 0;
	public long photoOrSignature = 0;
	public long filesDropzone = 0;
	public long insertionDate = 0;
	public long insertedByUserId = 0;
    public String modifiedBy = "";

	public List<FilesDTO> filesDropzoneFiles = new ArrayList();

	@Override
	public String toString() {
            return "$Job_applicant_photo_signatureDTO[" +
            " iD = " + iD +
            " jobApplicantId = " + jobApplicantId +
            " photoOrSignature = " + photoOrSignature +
            " filesDropzone = " + filesDropzone +
            " insertionDate = " + insertionDate +
            " insertedByUserId = " + insertedByUserId +
            " modifiedBy = " + modifiedBy +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}