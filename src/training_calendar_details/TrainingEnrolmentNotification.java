package training_calendar_details;

import card_info.CardApprovalNotification;
import pb_notifications.Pb_notificationsDAO;
import training_calender.Training_calenderDTO;
import util.StringUtils;

import java.util.List;

public class TrainingEnrolmentNotification {
    private final Pb_notificationsDAO pb_notificationsDAO;

    private TrainingEnrolmentNotification(){
        pb_notificationsDAO = Pb_notificationsDAO.getInstance();
    }

    private static class TrainingEnrolmentNotificationLazyLoader{
        static final TrainingEnrolmentNotification INSTANCE = new TrainingEnrolmentNotification();
    }
    public static TrainingEnrolmentNotification getInstance(){
        return TrainingEnrolmentNotification.TrainingEnrolmentNotificationLazyLoader.INSTANCE;
    }
    public void sendEnrolmentNotification(List<Long> organogramIds, Training_calenderDTO training_calenderDTO){
        String notificationEngText="You Have been enrolled in Training: "+training_calenderDTO.nameEn+ " Starting from "+
                StringUtils.getFormattedDate("ENGLISH",training_calenderDTO.startDate);
        String notificationBngText="আপনি নির্বাচিত হয়েছেন প্রশিক্ষণঃ "+training_calenderDTO.nameBn+ " শুরু হবে "+
                StringUtils.getFormattedDate("BANGLA",training_calenderDTO.startDate)+ " তারিখে";
        String notificationMessage = notificationEngText +"$"+notificationBngText;
        String url = "Training_calenderServlet?actionType=view&ID="+training_calenderDTO.iD;
        sendNotification(organogramIds,notificationMessage,url);
    }
    public void sendEnrolmentCancelNotification(List<Long> organogramIds, Training_calenderDTO training_calenderDTO){
        String notificationEngText="You Have been Removed from Training: "+training_calenderDTO.nameEn+ " Starting from "+
                StringUtils.getFormattedDate("ENGLISH",training_calenderDTO.startDate);
        String notificationBngText="আপনি বাদ পড়েছেণ প্রশিক্ষণঃ "+training_calenderDTO.nameBn+ " থেকে যা শুরু হবে "+
                StringUtils.getFormattedDate("BANGLA",training_calenderDTO.startDate)+ " তারিখে";
        String notificationMessage = notificationEngText +"$"+notificationBngText;
        String url = "Training_calenderServlet?actionType=view&ID="+training_calenderDTO.iD;
        sendNotification(organogramIds,notificationMessage,url);
    }
    private void sendNotification(List<Long> organogramIds, String notificationMessage, String url){
        if(organogramIds == null || organogramIds.size() == 0){
            return;
        }
        Thread thread = new Thread(()->{
            long currentTime = System.currentTimeMillis();
            organogramIds.forEach(id-> pb_notificationsDAO.addPb_notifications(id, currentTime, url, notificationMessage));
        });
        thread.setDaemon(true);
        thread.start();
    }
}
