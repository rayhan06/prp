package training_calendar_details;

import employee_records.EmployeeFlatInfoDTO;


public class Training_calendar_detailsDTO extends EmployeeFlatInfoDTO {

    public long trainingCalendarId = 0;
    public String metaData1 = "";
    public String metaData2 = "";
    public boolean isFeedBackDone = false;

    @Override
    public String toString() {
        return "Training_calendar_detailsDTO{" +
                "trainingCalendarId=" + trainingCalendarId +
                ", metaData1='" + metaData1 + '\'' +
                ", metaData2='" + metaData2 + '\'' +
                ", isFeedBackDone=" + isFeedBackDone +
                '}';
    }


}