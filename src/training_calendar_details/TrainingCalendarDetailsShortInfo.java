package training_calendar_details;

/*
 * @author Md. Erfan Hossain
 * @created 27/03/2021 - 4:06 PM
 * @project parliament
 */

import training_calender.Training_calenderDTO;

public class TrainingCalendarDetailsShortInfo {
    public long id;
    public long trainingCalendarId;
    public String trainingCalendarName;
    public String startDateAndTime;
    public String endDateAndTime;
    public String trainingType;
    public String trainingMode;
    public Training_calenderDTO trainingCalenderDTO;
}
