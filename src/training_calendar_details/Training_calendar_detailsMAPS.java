package training_calendar_details;

import util.CommonMaps;


public class Training_calendar_detailsMAPS extends CommonMaps
{	
	public Training_calendar_detailsMAPS(String tableName)
	{
		

		java_DTO_map.put("id".toLowerCase(), "id".toLowerCase());
		java_DTO_map.put("trainingCalendarId".toLowerCase(), "trainingCalendarId".toLowerCase());
		java_DTO_map.put("employeeRecordId".toLowerCase(), "employeeRecordId".toLowerCase());
		java_DTO_map.put("officeUnitId".toLowerCase(), "officeUnitId".toLowerCase());
		java_DTO_map.put("officeUnitNameEng".toLowerCase(), "officeUnitNameEng".toLowerCase());
		java_DTO_map.put("officeUnitNameBng".toLowerCase(), "officeUnitNameBng".toLowerCase());
		java_DTO_map.put("designationEng".toLowerCase(), "designationEng".toLowerCase());
		java_DTO_map.put("designationBng".toLowerCase(), "designationBng".toLowerCase());
		java_DTO_map.put("qsAns1".toLowerCase(), "qsAns1".toLowerCase());
		java_DTO_map.put("qsAns2".toLowerCase(), "qsAns2".toLowerCase());
		java_DTO_map.put("insertionDate".toLowerCase(), "insertionDate".toLowerCase());
		java_DTO_map.put("lastmodificationtime".toLowerCase(), "lastmodificationtime".toLowerCase());
		java_DTO_map.put("insertBy".toLowerCase(), "insertBy".toLowerCase());
		java_DTO_map.put("modifiedBy".toLowerCase(), "modeifiedBy".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());

		java_SQL_map.put("training_calendar_id".toLowerCase(), "trainingCalendarId".toLowerCase());
		java_SQL_map.put("employee_record_id".toLowerCase(), "employeeRecordId".toLowerCase());
		java_SQL_map.put("office_unit_id".toLowerCase(), "officeUnitId".toLowerCase());
		java_SQL_map.put("office_unit_name_eng".toLowerCase(), "officeUnitNameEng".toLowerCase());
		java_SQL_map.put("office_unit_name_bng".toLowerCase(), "officeUnitNameBng".toLowerCase());
		java_SQL_map.put("designation_eng".toLowerCase(), "designationEng".toLowerCase());
		java_SQL_map.put("designation_bng".toLowerCase(), "designationBng".toLowerCase());

		java_Text_map.put("Id".toLowerCase(), "id".toLowerCase());
		java_Text_map.put("Training Calendar Id".toLowerCase(), "trainingCalendarId".toLowerCase());
		java_Text_map.put("Employee Record Id".toLowerCase(), "employeeRecordId".toLowerCase());
		java_Text_map.put("Office Unit Id".toLowerCase(), "officeUnitId".toLowerCase());
		java_Text_map.put("Office Unit Name Eng".toLowerCase(), "officeUnitNameEng".toLowerCase());
		java_Text_map.put("Office Unit Name Bng".toLowerCase(), "officeUnitNameBng".toLowerCase());
		java_Text_map.put("Designation Eng".toLowerCase(), "designationEng".toLowerCase());
		java_Text_map.put("Designation Bng".toLowerCase(), "designationBng".toLowerCase());
		java_Text_map.put("Qs Ans 1".toLowerCase(), "qsAns1".toLowerCase());
		java_Text_map.put("Qs Ans 2".toLowerCase(), "qsAns2".toLowerCase());
		java_Text_map.put("Insertion Date".toLowerCase(), "insertionDate".toLowerCase());
		java_Text_map.put("Lastmodificationtime".toLowerCase(), "lastmodificationtime".toLowerCase());
		java_Text_map.put("Insert By".toLowerCase(), "insertBy".toLowerCase());
		java_Text_map.put("Modified By".toLowerCase(), "modeifiedBy".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
			
	}

}