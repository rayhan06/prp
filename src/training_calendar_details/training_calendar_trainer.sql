CREATE TABLE training_calendar_trainer
(
    id                   bigint(20) PRIMARY KEY,
    training_calendar_id bigint(20),
    employee_records_id  bigint(20),
    employee_name_en     varchar(255),
    employee_name_bn     varchar(255),
    employee_user_name   varchar(255),

    office_unit_id       bigint(20),
    office_name_en       varchar(127),
    office_name_bn       varchar(127),

    organogram_id        bigint(20),
    organogram_name_en   varchar(255),
    organogram_name_bn   varchar(255),

    insertion_date       BIGINT,
    lastmodificationtime BIGINT,
    insert_by            VARCHAR(255),
    modified_by          VARCHAR(255),
    isDeleted            INT(4)
);