package training_calendar_details;

import common.ConnectionAndStatementUtil;
import employee_records.EmpOfficeModel;
import employee_records.EmployeeFlatInfoDAO;
import org.apache.log4j.Logger;
import pb.CatRepository;
import training_calender.TrainingCalendarStatusEnum;
import training_calender.Training_calenderDAO;
import training_calender.Training_calenderDTO;
import training_calender.Training_calenderMAPS;
import user.UserDTO;
import util.CommonDTO;
import util.NavigationService4;
import util.StringUtils;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;

@SuppressWarnings({"unused,rawtypes"})
public class Training_calendar_detailsDAO extends NavigationService4 implements EmployeeFlatInfoDAO<Training_calendar_detailsDTO> {

    private static final Logger logger = Logger.getLogger(Training_calendar_detailsDAO.class);
    private static final String getDTOByTrainingCalendarIdAndEmployeeRecordIdQuery = "select * from training_calendar_details where training_calendar_id = %d and employee_records_id = %d and isDeleted = 0";

    private static final String getDTOByEmployeeRecordIdQuery = "SELECT * FROM training_calendar_details WHERE employee_records_id = %d AND isDeleted = 0 ORDER BY lastmodificationtime";

    private static final List<String> columnList;

    private static final String deleteByCalenderAndRecordId = "UPDATE training_calendar_details SET "
            .concat(" isDeleted=1 where training_calendar_id='%d' AND employee_records_id='%d'");

    private static final String updateMetadataQuery = "update training_calendar_details set metadata_1 = '%s', metadata_2 = '%s', isFeedbackDone = %d where employee_records_id = %d and training_calendar_id = %d";

    private static final String getByTrainingCalendarIdAndEmpId = "SELECT * FROM training_calendar_details WHERE training_calendar_id = %d AND employee_records_id = %d";

    static {
        columnList = Collections.singletonList("training_calendar_id");
    }

    public Training_calendar_detailsDTO getByTrainingCalendarIdAndEmpId(long trainingCalendarId, long empId) {
        String sql = String.format(getByTrainingCalendarIdAndEmpId, trainingCalendarId, empId);
        return ConnectionAndStatementUtil.getT(sql, this::buildDTOForFeedback);
    }

    public Training_calendar_detailsDAO(String tableName) {
        super(tableName);
        joinSQL = "";
        commonMaps = new Training_calenderMAPS(tableName);
    }

    public Training_calendar_detailsDAO() {
        this("training_calendar_details");
    }


    public void setSearchColumn(Training_calendar_detailsDTO training_calendar_detailsDTO) {
        training_calendar_detailsDTO.searchColumn = "";

    }

    @Override
    public String getTableName() {
        return "training_calendar_details";
    }

    @Override
    public List<String> getExtraColumnListForAddSQLQueryClause() {
        return columnList;
    }

    @Override
    public List<String> getExtraColumnListUpdateSQLQueryClause() {
        return columnList;
    }

    @Override
    public void setExtraPreparedStatementParams(PreparedStatement ps, Training_calendar_detailsDTO dto, boolean isInsert) throws Exception {
        int index = 0;
        if (isInsert) {
            ps.setObject(++index, dto.trainingCalendarId);
        }
    }

    @Override
    public Training_calendar_detailsDTO buildTFromResultSet(ResultSet rs) throws SQLException {
        Training_calendar_detailsDTO training_calendar_detailsDTO = new Training_calendar_detailsDTO();
        training_calendar_detailsDTO.trainingCalendarId = rs.getLong("training_calendar_id");
        return training_calendar_detailsDTO;
    }

    public void add(List<EmpOfficeModel> list, long trainingCalendarId, String requestBy) throws Exception {
        add(list, trainingCalendarId, requestBy, System.currentTimeMillis());
    }

    private void add(List<EmpOfficeModel> list, long trainingCalendarId, String requestBy, long requestTime) throws Exception {
        for (EmpOfficeModel model : list) {
            addEmployeeFlatInfoDTO(buildTrainerDTO(model, trainingCalendarId, requestBy, requestTime));
        }
    }

    private Training_calendar_detailsDTO buildTrainerDTO(EmpOfficeModel model, long trainingCalendarId, String requestBy, long requestTime) {
        Training_calendar_detailsDTO dto = new Training_calendar_detailsDTO();
        dto.trainingCalendarId = trainingCalendarId;
        dto.employeeRecordsId = model.employeeRecordId;
        dto.officeUnitId = model.officeUnitId;
        dto.organogramId = model.organogramId;
        dto.insertBy = requestBy;
        dto.insertionDate = requestTime;
        dto.modified_by = requestBy;
        dto.lastModificationTime = requestTime;
        return dto;
    }

    public void update(List<EmpOfficeModel> list, long trainingCalendarId, String requestBy) throws Exception {
        long requestTime = System.currentTimeMillis();
        Map<Boolean, List<EmpOfficeModel>> booleanListMap = deleteDeletedInfoAndReturnAMap(list, String.valueOf(trainingCalendarId), requestBy, requestTime, true);
        List<EmpOfficeModel> newlyAddingList = booleanListMap.get(false);
        List<Long> addedOrganograms = newlyAddingList
                .stream()
                .map(dto -> dto.organogramId)
                .collect(Collectors.toList());
        if (newlyAddingList.size() > 0) {
            add(newlyAddingList, trainingCalendarId, requestBy, requestTime);
        }
        List<Long> deletedOrganograms = getDeletedOrganograms();
        Training_calenderDTO training_calenderDTO = Training_calenderDAO.getInstance().getDTOFromID(trainingCalendarId);
        TrainingEnrolmentNotification.getInstance().sendEnrolmentNotification(addedOrganograms, training_calenderDTO);
        TrainingEnrolmentNotification.getInstance().sendEnrolmentCancelNotification(deletedOrganograms, training_calenderDTO);
    }


    @Override
    public Collection getDTOs(Hashtable p_searchCriteria, int limit, int offset, boolean isPermanentTable, UserDTO userDTO) {
        return null;
    }

    @Override
    public CommonDTO getDTOByID(long id) throws Exception {
        return getById(id);
    }

    public void deletebyCalendarAndEmployee(long calendarId, long employeeId) {
        String deleteSql = String.format(deleteByCalenderAndRecordId, calendarId, employeeId);
        ConnectionAndStatementUtil.getWriteStatement(st -> {
            try {
                st.executeUpdate(deleteSql);
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        });

    }

    private Training_calendar_detailsDTO buildDTOForFeedback(ResultSet rs) {
        try {
            Training_calendar_detailsDTO dto = new Training_calendar_detailsDTO();
            dto.iD = rs.getInt("id");
            dto.employeeNameEn = rs.getString("employee_name_en");
            dto.employeeNameBn = rs.getString("employee_name_bn");
            dto.officeNameEn = rs.getString("office_name_en");
            dto.officeNameBn = rs.getString("office_name_bn");
            dto.organogramNameEn = rs.getString("organogram_name_en");
            dto.organogramNameBn = rs.getString("organogram_name_bn");
            dto.metaData1 = rs.getString("metadata_1");
            dto.metaData2 = rs.getString("metadata_2");
            dto.isFeedBackDone = rs.getBoolean("isFeedbackDone");
            dto.trainingCalendarId = rs.getLong("training_calendar_id");
            return dto;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    public Training_calendar_detailsDTO getDTOByCalendarIdAndEmployeeRecordId(long trainingCalendarId, long employeeRecordId) {
        String queryStatement = String.format(getDTOByTrainingCalendarIdAndEmployeeRecordIdQuery, trainingCalendarId, employeeRecordId);
        return ConnectionAndStatementUtil.getT(queryStatement, this::buildDTOForFeedback);
    }


    public int updateMetadata(long empId, long trainingCalendarId, String metadataOne, String metadataTwo, int isFeedbackDone) {
        return (Integer) ConnectionAndStatementUtil.getWriteStatement(st -> {
            String sql = String.format(updateMetadataQuery, metadataOne, metadataTwo, isFeedbackDone, empId, trainingCalendarId);
            try {
                return st.executeUpdate(sql);
            } catch (SQLException ex) {
                logger.error(ex);
                return 0;
            }
        });
    }

    public List<Long> getTrainingCalendarIdsList(long employeeId, String Language) {
        String sql = String.format(getDTOByEmployeeRecordIdQuery, employeeId);
        List<Training_calendar_detailsDTO> dtoList = ConnectionAndStatementUtil.getListOfT(sql, this::buildDTOForFeedback);
        if (dtoList.size() == 0) {
            return new ArrayList<>();
        }
        return dtoList.stream().map(dto -> dto.trainingCalendarId).collect(Collectors.toList());
    }

    public List<TrainingCalendarDetailsShortInfo> getTrainingCalendarDetailsShortInfoList(long employeeId, String Language) {
        String sql = String.format(getDTOByEmployeeRecordIdQuery, employeeId);
        List<Training_calendar_detailsDTO> dtoList = ConnectionAndStatementUtil.getListOfT(sql, this::buildDTOForFeedback);
        if (dtoList.size() == 0) {
            return new ArrayList<>();
        }
        List<Long> calendarIds = dtoList.stream().map(dto -> dto.trainingCalendarId).collect(Collectors.toList());
        List<Training_calenderDTO> calenderDTOList = Training_calenderDAO.getInstance().getDTOs(calendarIds);
        Map<Long, Training_calenderDTO> mapById =
                calenderDTOList.stream()
                               .filter(dto -> dto.isDeleted == 0)
                               .filter(dto -> dto.isPrivate || dto.status == TrainingCalendarStatusEnum.COMPLETED.getValue())
                               .collect(Collectors.toMap(dto -> dto.iD, dto -> dto));
        return dtoList.stream()
                      .filter(dto -> mapById.get(dto.trainingCalendarId) != null)
                      .map(dto -> convertToTrainingCalendarDetailsShortInfo(dto, mapById.get(dto.trainingCalendarId), Language))
                      .collect(Collectors.toList());
    }

    public TrainingCalendarDetailsShortInfo convertToTrainingCalendarDetailsShortInfo(Training_calendar_detailsDTO detailsDTO, Training_calenderDTO calenderDTO, String Language) {
        TrainingCalendarDetailsShortInfo info = new TrainingCalendarDetailsShortInfo();
        boolean languageIsBangla = Language.equalsIgnoreCase("Bangla");
        info.id = detailsDTO.iD;
        info.trainingCalendarId = calenderDTO.iD;
        info.trainingCalendarName = languageIsBangla ? calenderDTO.nameBn : calenderDTO.nameEn;
        info.trainingType = CatRepository.getInstance().getText(Language, "training_type", calenderDTO.trainingTypeCat);
        info.trainingMode = CatRepository.getInstance().getText(Language, "training_mode", calenderDTO.trainingModeCat);
        info.startDateAndTime = StringUtils.getFormattedDate(Language, calenderDTO.startDate);
        if (calenderDTO.startTime != null) {
            info.startDateAndTime += " " + StringUtils.convertBanglaIfLanguageIsBangla(Language, calenderDTO.startTime);
        }
        info.endDateAndTime = StringUtils.getFormattedDate(Language, calenderDTO.endDate);
        if (calenderDTO.endTime != null) {
            info.endDateAndTime += " " + StringUtils.convertBanglaIfLanguageIsBangla(Language, calenderDTO.endTime);
        }

        info.trainingCalenderDTO = calenderDTO;
        return info;
    }
}