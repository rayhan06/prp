package training_calendar_details;

import com.google.gson.Gson;
import employee_records.EmpOfficeModel;
import login.LoginDTO;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import permission.MenuConstants;
import role.PermissionRepository;
import sessionmanager.SessionConstants;
import training_calender.Training_calenderDAO;
import training_calender.Training_calenderDTO;
import user.UserDTO;
import user.UserRepository;
import util.CommonRequestHandler;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;


@WebServlet("/Training_calendar_detailsServlet")
@MultipartConfig
public class Training_calendar_detailsServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Training_calendar_detailsServlet.class);

    private final Training_calendar_detailsDAO training_calendar_detailsDAO;
    private final CommonRequestHandler commonRequestHandler;
    private final Gson gson = new Gson();

    public Training_calendar_detailsServlet() {
        super();

        training_calendar_detailsDAO = new Training_calendar_detailsDAO();
        commonRequestHandler = new CommonRequestHandler(training_calendar_detailsDAO);
    }


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO != null) {
            try {
                if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.TRAINING_CALENDAR_DETAILS_ADD)) {
                    commonRequestHandler.getAddPage(request, response);
                    return;
                }
            } catch (Exception e) {
                logger.error(e);
            }
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }
        long trainingCalendarId = Long.parseLong(request.getParameter("trainingCalendarId"));
        List<Training_calendar_detailsDTO> oldDTOList = training_calendar_detailsDAO.getByForeignKey(String.valueOf(trainingCalendarId));

        String employeeIds = request.getParameter("addedEmployees");
        if (employeeIds != null) {
            employeeIds = Jsoup.clean(employeeIds, Whitelist.simpleText());
        }

        List<EmpOfficeModel> employeeRecords = Arrays.asList(gson.fromJson(employeeIds, EmpOfficeModel[].class));


        if (employeeIds == null && (oldDTOList == null || oldDTOList.size() == 0)) {
            response.sendRedirect("Training_calenderServlet?actionType=search");
            return;
        }
        try {
            if (oldDTOList == null || oldDTOList.size() == 0) {
                training_calendar_detailsDAO.add(employeeRecords, trainingCalendarId, userDTO.userName);
                List<Long> addedOrganograms = employeeRecords.stream()
                        .map(dto -> dto.organogramId)
                        .collect(Collectors.toList());
                Training_calenderDTO training_calenderDTO = Training_calenderDAO.getInstance().getDTOFromID(trainingCalendarId);
                TrainingEnrolmentNotification.getInstance().sendEnrolmentNotification(addedOrganograms, training_calenderDTO);
            } else {
                training_calendar_detailsDAO.update(employeeRecords, trainingCalendarId, userDTO.userName);
            }
            response.sendRedirect("Training_calenderServlet?actionType=search");
        } catch (Exception e) {
            logger.error(e);
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
        }


    }


}

