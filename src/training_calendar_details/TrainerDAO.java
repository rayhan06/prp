package training_calendar_details;

/*
 * @author Md. Erfan Hossain
 * @created 16/03/2021 - 9:55 AM
 * @project parliament
 */

import employee_records.EmpOfficeModel;
import employee_records.EmployeeFlatInfoDAO;
import training_calender.TrainerNotification;
import training_calender.Training_calenderDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class TrainerDAO implements EmployeeFlatInfoDAO<TrainerDTO> {

    private static final List<String> columnList;

    static {
        columnList = Collections.singletonList("training_calendar_id");
    }

    @Override
    public String getTableName() {
        return "training_calendar_trainer";
    }

    @Override
    public List<String> getExtraColumnListForAddSQLQueryClause() {
        return columnList;
    }

    @Override
    public List<String> getExtraColumnListUpdateSQLQueryClause() {
        return columnList;
    }

    @Override
    public void setExtraPreparedStatementParams(PreparedStatement ps, TrainerDTO dto, boolean isInsert) throws Exception {
        ps.setObject(1,dto.training_calender_id);
    }

    @Override
    public TrainerDTO buildTFromResultSet(ResultSet rs) throws SQLException {
        TrainerDTO dto = new TrainerDTO();
        dto.training_calender_id = rs.getLong("training_calendar_id");
        return dto;
    }

    public void add(List<EmpOfficeModel> list, long trainingCalendarId, String requestBy) throws Exception {
        add(list,trainingCalendarId,requestBy,System.currentTimeMillis());
    }

    private void add(List<EmpOfficeModel> list, long trainingCalendarId, String requestBy,long requestTime) throws Exception {
        for (EmpOfficeModel model : list) {
            addEmployeeFlatInfoDTO(buildTrainerDTO(model, trainingCalendarId, requestBy, requestTime));
        }
    }

    private TrainerDTO buildTrainerDTO(EmpOfficeModel model, long trainingCalendarId, String requestBy, long requestTime) {
        TrainerDTO dto = new TrainerDTO();
        dto.training_calender_id = trainingCalendarId;
        dto.employeeRecordsId = model.employeeRecordId;
        dto.officeUnitId = model.officeUnitId;
        dto.organogramId = model.organogramId;
        dto.insertBy = requestBy;
        dto.insertionDate = requestTime;
        dto.modified_by = requestBy;
        dto.lastModificationTime = requestTime;
        return dto;
    }

    public void update(List<EmpOfficeModel> list, long trainingCalendarId, String requestBy, Training_calenderDTO training_calenderDTO) throws Exception {
        long requestTime = System.currentTimeMillis();
        Map<Boolean, List<EmpOfficeModel>> booleanListMap = deleteDeletedInfoAndReturnAMap(list, String.valueOf(trainingCalendarId), requestBy, requestTime,true);
        List<EmpOfficeModel> newlyAddingList = booleanListMap.get(false);
        List<Long> addedOrganograms = newlyAddingList
                .stream()
                .map(dto -> dto.organogramId)
                .collect(Collectors.toList());
        if(newlyAddingList != null && newlyAddingList.size()>0){
            add(newlyAddingList,trainingCalendarId,requestBy,requestTime);
        }
        List<Long>deletedOrganograms=getDeletedOrganograms();
        TrainerNotification.getInstance().sendTrainerSelectionNotification(addedOrganograms,training_calenderDTO);
        TrainerNotification.getInstance().sendTrainerEjectNotification(deletedOrganograms,training_calenderDTO);
        // No updating required here.
    }

}
