package pbReport;

import org.json.JSONException;
import org.json.JSONObject;
import pb.CatRepository;
import repository.RepositoryManager;

public class CacheDataByDtoAttributeConverter extends ColumnConvertor{

    @Override
    public Object convert(Object columnValue, String language, String tableName) {
        if (columnValue == null) return "";

        String tableNameExtracted = tableName.split(",")[0];
        String attributeName = tableName.split(",")[1];
        long id = Long.parseLong(columnValue.toString());
        String dtoJson = RepositoryManager.getInstance().getRepository(tableNameExtracted).getDtoJsonById(id);
        if (dtoJson != null) {
            JSONObject jsonObject = null;
            try {
                jsonObject = new JSONObject(dtoJson);
                return jsonObject.get(attributeName);
            } catch (JSONException e) {
                e.printStackTrace();
                return null;
            }

        }
        return null;
    }
}
