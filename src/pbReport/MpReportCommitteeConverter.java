package pbReport;

import committees.CommitteesRepository;
import committees_mapping.Committees_mappingDAO;
import committees_mapping.Committees_mappingDTO;
import committees_mapping.Committees_mappingRepository;
import pb.CatRepository;
import pb.Utils;

import java.util.List;
import java.util.stream.Collectors;

public class MpReportCommitteeConverter extends ColumnConvertor {
    @Override
    public Object convert(Object columnValue, String language, String tableName) {
        if (columnValue == null) {
            return "";
        }

        StringBuilder stringBuilder = new StringBuilder();
        long employeeRecordId = Long.parseLong(columnValue.toString());
        int committeeCount = 1;

        List<Committees_mappingDTO> committees_mappingDTOS = Committees_mappingRepository.getInstance().getCommittees_mappingDTOsByEmployeeRecordId(employeeRecordId);

        for (Committees_mappingDTO dto: committees_mappingDTOS) {
            stringBuilder.append(Utils.getDigits(committeeCount++, language)).append(". ").append(CommitteesRepository.getInstance().getText(language, dto.committeesId)).
                    append(" (").append(CatRepository.getInstance().getText(language, "committees_role", dto.committeesRoleCat)).append(")    ");
        }

        return stringBuilder.toString();
    }
}

