package pbReport;

import employee_records.Employee_recordsRepository;
import geolocation.GeoLocationDTO;
import geolocation.GeoLocationRepository;

public class RecordIdtoPermanentDistrictConverter extends ColumnConvertor {

    @Override
    public Object convert(Object columnValue, String language, String tableName) {
        if (columnValue == null) {
            return "";
        }
        long val = Long.parseLong(columnValue.toString());
        long district = Employee_recordsRepository.getInstance().getById(val).permanentDistrictId;
        GeoLocationDTO dto = GeoLocationRepository.getInstance().getById((int) district);
        if (dto != null) {
            return language.equalsIgnoreCase("ENGLISH") ? dto.name_en : dto.name_bn;
        }
        return "";
    }
}
