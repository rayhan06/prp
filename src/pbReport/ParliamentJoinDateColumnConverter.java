package pbReport;

import employee_offices.EmployeeOfficeDTO;
import employee_offices.EmployeeOfficeRepository;
import pb.Utils;
import util.TimeConverter;

import java.util.Collections;
import java.util.List;

public class ParliamentJoinDateColumnConverter extends ColumnConvertor {
    public String convert(Object columnValue, String language, String tableName) {
        if (columnValue == null) {
            return "";
        }
        List<EmployeeOfficeDTO> employeeOfficeDTOList = EmployeeOfficeRepository.getInstance().getByEmployeeRecordId(Long.parseLong(columnValue.toString()));
        if (employeeOfficeDTOList != null && employeeOfficeDTOList.size() >= 1) {
            Collections.sort(employeeOfficeDTOList);

            String sDate = TimeConverter.getTimeStringFromLong(employeeOfficeDTOList.get(0).getJoiningDate());
            sDate = Utils.getDigits(sDate, language);
            return sDate;

        } else {
            return "";
        }


    }
}
