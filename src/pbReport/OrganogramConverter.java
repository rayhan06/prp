package pbReport;

//import com.sun.deploy.uitoolkit.impl.fx.Utils;

import office_unit_organograms.*;
import workflow.WorkflowController;

public class OrganogramConverter extends ColumnConvertor{
	
	public String convert(Object value, String language, String tableName) {
		try {
			
				String name = WorkflowController.getNameFromOrganogramId((long)value, language);
				String designation = OfficeUnitOrganogramsRepository.getInstance().getDesignation(language,Long.parseLong(value.toString()));
				String userName = "";
				if(value != null)
				{
					userName = pb.Utils.getDigits( WorkflowController.getUserNameFromOrganogramId((long)value), language);
				}
				
				if(!name.equalsIgnoreCase("") && !designation.equalsIgnoreCase(""))
				{
					return userName + ": " + name + ", " + designation;
				}
				else if(!name.equalsIgnoreCase(""))
				{
					return userName + ": " + name;
				}
				else
				{
					if(language.equalsIgnoreCase("english"))
					{
						return "N/A";
					}
					else
					{
						return "প্রযোজ্য নয়";
					}
				}
				
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "";
	}

}
