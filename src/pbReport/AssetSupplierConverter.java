package pbReport;


import asset_supplier.Asset_supplierRepository;

public class AssetSupplierConverter extends ColumnConvertor{

    @Override
    public Object convert(Object columnValue, String language, String tableName) {
        if (columnValue == null) return "";

        long id = Long.parseLong(columnValue.toString());
        return Asset_supplierRepository.getInstance().getText(language,id);
    }
}
