package pbReport;

/*
 * @author Md. Erfan Hossain
 * @created 18/05/2021 - 11:37 PM
 * @project parliament
 */

import office_unit_organograms.OfficeUnitOrganogramsRepository;

public class OfficeUnitOrganogramConverter extends ColumnConvertor {

    public String convert(Object value, String language, String tableName) {
        if (value == null) {
            return "";
        }
        return OfficeUnitOrganogramsRepository.getInstance().getDesignation(language, Long.parseLong(value.toString()));
    }
}