package pbReport;

/*
 * @author R. R. Jahin
 * @created 06/07/2021 - 2:13 AM
 * @project parliament
 */

import gate_pass_sub_type.Gate_pass_sub_typeDTO;
import gate_pass_sub_type.Gate_pass_sub_typeRepository;
import gate_pass_type.Gate_pass_typeDTO;
import gate_pass_type.Gate_pass_typeRepository;
import gate_pass_visitor.Gate_pass_visitorDTO;
import gate_pass_visitor.Gate_pass_visitorRepository;
import pb.Utils;

public class GatePassDataConverter extends ColumnConvertor {
    private final String column;

    public GatePassDataConverter(String column) {
        this.column = column;
    }

    @Override
    public Object convert(Object columnValue, String language, String tableName) {
        if(column!=null && columnValue != null){
            long id;
            if(column.equals("gate_pass_first_layer_approval") || column.equals("gate_pass_second_layer_approval") || column.equals("gate_pass_is_issued"))
                id = (int)columnValue;
            else
                id = (long) columnValue;
            Gate_pass_visitorDTO gate_pass_visitorDTO = null;
            Gate_pass_typeDTO gate_pass_typeDTO = null;
            Gate_pass_sub_typeDTO gate_pass_sub_typeDTO = null;
            String mobile = "";
            if(column.equals("gate_pass_visitor_name")) {
                gate_pass_visitorDTO = Gate_pass_visitorRepository.getInstance().getGate_pass_visitorDTOByid(id);
                return gate_pass_visitorDTO.name;
            }
            else if(column.equals("gate_pass_visitor_phone")){
                gate_pass_visitorDTO = Gate_pass_visitorRepository.getInstance().getGate_pass_visitorDTOByid(id);
                mobile = gate_pass_visitorDTO.mobileNumber.substring(2);
                return language.equals("English") ? mobile : Utils.getDigitBanglaFromEnglish(mobile);
            }
            else if(column.equals("gate_pass_type")){
                gate_pass_typeDTO = Gate_pass_typeRepository.getInstance().getGate_pass_typeDTOByid(id);
                return language.equals("English") ? gate_pass_typeDTO.nameEng : gate_pass_typeDTO.nameBng;
            }
            else if(column.equals("gate_pass_sub_type")){
                if(id == 0) return "";
                gate_pass_sub_typeDTO = Gate_pass_sub_typeRepository.getInstance().getGate_pass_sub_typeDTOByid(id);
                return language.equals("English") ? gate_pass_sub_typeDTO.nameEng : gate_pass_sub_typeDTO.nameBng;
            }
            else if(column.equals("gate_pass_first_layer_approval")){
                if(id == 0) return language.equals("English") ? "Pending" : "অনিষ্পন্ন";
                if(id == 1) return language.equals("English") ? "Approved" : "অনুমোদিত";
                if(id == 2) return language.equals("English") ? "Dismissed " : "অননুমোদিত";
            }
            else if(column.equals("gate_pass_second_layer_approval")){
                if(id == 0) return language.equals("English") ? "Pending" : "অনিষ্পন্ন";
                if(id == 1) return language.equals("English") ? "Approved" : "অনুমোদিত";
                if(id == 2) return language.equals("English") ? "Dismissed" : "অননুমোদিত";
                if(id == 4) return language.equals("English") ? "Processing" : "প্রক্রিয়াধীন";
            } else if(column.equals("gate_pass_is_issued")) {
                if(id == 1) return language.equals("English") ? "Issued" : "ইস্যুকৃত";
                if(id == 0) return language.equals("English") ? "Not Issued" : "ইস্যুকৃত নয়";
            }
        }
        return "";
    }
}
