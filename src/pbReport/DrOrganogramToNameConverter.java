package pbReport;

import doctor_time_slot.Doctor_time_slotRepository;
import employee_offices.EmployeeOfficeDTO;
import employee_offices.EmployeeOfficeRepository;
import pb.CatDAO;
import sessionmanager.SessionConstants;
import workflow.WorkflowController;

public class DrOrganogramToNameConverter extends ColumnConvertor {

	@Override
	public Object convert(Object value, String language, String tableName) {
		Long organogramId = -1L;
		if (value instanceof Long)
		{
			organogramId = (Long)value;
		}
		else
		{
			organogramId = Long.parseLong(value.toString());
		}
		// TODO Auto-generated method stub
		String name = WorkflowController.getNameFromOrganogramId(organogramId, language);
		EmployeeOfficeDTO employeeOfficeDTO = EmployeeOfficeRepository.getInstance().getByOfficeUnitOrganogramId(organogramId);
		if(employeeOfficeDTO != null && organogramId != SessionConstants.MEDICAL_DIRECTOR_ORGANOGRAM_ID && employeeOfficeDTO.medicalDeptCat >= 0)
		{
			name +=", " + CatDAO.getName(language, "medical_dept", employeeOfficeDTO.medicalDeptCat);
		}
		return name;
	}

}
