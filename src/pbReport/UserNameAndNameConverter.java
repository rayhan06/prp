package pbReport;

import employee_records.Employee_recordsRepository;
import pb.Utils;


public class UserNameAndNameConverter extends ColumnConvertor {

    @Override
    public Object convert(Object columnValue, String language, String tableName) {
        if (columnValue == null || columnValue.toString().isEmpty()) {
            return "";
        }
        String userName = columnValue.toString();
        String name = Employee_recordsRepository.getInstance().getByUserName(userName, language);
        if (language.equalsIgnoreCase("english")) {
            return userName + "<br>" + name;
        } else {
            return Utils.getDigitBanglaFromEnglish(userName) + "<br>" + name;
        }
    }

}
