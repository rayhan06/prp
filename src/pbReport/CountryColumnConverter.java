package pbReport;

/*
 * @author Md. Erfan Hossain
 * @created 07/03/2022 - 6:48 AM
 * @project parliament
 */

import geolocation.GeoCountryRepository;

public class CountryColumnConverter extends ColumnConvertor {

    @Override
    public Object convert(Object columnValue, String language, String tableName) {
        boolean isLangEng = "English".equalsIgnoreCase(language);
        String countryName = isLangEng ? "Bangladesh" : "বাংলাদেশ";
        if (columnValue == null) {
            return countryName;
        }
        try {
            long id = (Long) columnValue;
            if (id == 0) {
                return countryName;
            }
            return GeoCountryRepository.getInstance().getText(isLangEng, id);
        } catch (Exception ex) {
            ex.printStackTrace();
            return countryName;
        }
    }
}
