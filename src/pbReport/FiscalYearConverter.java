package pbReport;

import fiscal_year.Fiscal_yearRepository;
import vm_vehicle_driver_assignment.Vm_vehicle_driver_assignmentRepository;

public class FiscalYearConverter extends ColumnConvertor{

    @Override
    public Object convert(Object columnValue, String language, String tableName) {
        if (columnValue == null) return "";

        long id = Long.parseLong(columnValue.toString());
        return Fiscal_yearRepository.getInstance().getText(id, language);
    }
}
