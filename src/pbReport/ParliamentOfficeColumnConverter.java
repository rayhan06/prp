package pbReport;

import employee_offices.EmployeeOfficeDTO;
import employee_offices.EmployeeOfficeRepository;
import office_units.Office_unitsRepository;

import java.util.Collections;
import java.util.List;

public class ParliamentOfficeColumnConverter extends ColumnConvertor {
    public String convert(Object columnValue, String language, String tableName) {
        if (columnValue == null) {
            return "";
        }
        List<EmployeeOfficeDTO> employeeOfficeDTOList = EmployeeOfficeRepository.getInstance().getByEmployeeRecordId(Long.parseLong(columnValue.toString()));
        if (employeeOfficeDTOList != null && employeeOfficeDTOList.size() >= 1) {
            Collections.sort(employeeOfficeDTOList);

            return Office_unitsRepository.getInstance().geText(language, employeeOfficeDTOList.get(0).officeUnitId);
        } else {
            return "";
        }


    }
}
