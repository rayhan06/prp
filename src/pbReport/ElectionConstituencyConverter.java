package pbReport;

import election_constituency.Election_constituencyRepository;

public class ElectionConstituencyConverter extends ColumnConvertor {
    @Override
    public Object convert(Object columnValue, String language, String tableName) {
        if (columnValue == null) return "";

        long id = Long.parseLong(columnValue.toString());
        return Election_constituencyRepository.getInstance().getText(id, language);
    }
}
