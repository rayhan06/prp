package pbReport;

/*
 * @author Md. Erfan Hossain
 * @created 21/04/2021 - 7:33 PM
 * @project parliament
 */

import pb.CatRepository;
import pb.Utils;

public class CategoryConverter extends ColumnConvertor {

	@Override
    public Object convert(Object value, String language, String tableName) {
        if (value == null) {
            return "";
        }else if(value.toString().length() == 0){
            return "";
        }
        return CatRepository.getInstance().getText(language, tableName, Integer.parseInt(value.toString()));
    }
}
