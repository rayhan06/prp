package pbReport;

import com.google.gson.Gson;
import office_units.Office_unitsRepository;


import java.util.Arrays;
import java.util.stream.Collectors;

public class OfficeUnitConverter extends ColumnConvertor {
    private final String singleOrList;

    public OfficeUnitConverter() {
        singleOrList = "single";
    }

    public OfficeUnitConverter(String singleOrList) {
        this.singleOrList = singleOrList;
    }

    public String convert(Object value, String language, String tableName) {
        try {
            Long[] officeUnitIds = null;
            switch (singleOrList) {
                case "single":
                    officeUnitIds = new Long[]{Long.parseLong(value.toString())};
                    break;
                case "list":
                    officeUnitIds = new Gson().fromJson(value.toString(), Long[].class);
                    break;
            }
            if (officeUnitIds == null) {
                return getEmptyText(language);
            }

            String text = Arrays.stream(officeUnitIds)
                    .map(id -> Office_unitsRepository.getInstance().geText(language, id))
                    .map(String::valueOf)
                    .collect(Collectors.joining(","));
            if (text.equalsIgnoreCase("")) {
                return getEmptyText(language);
            }
            return text;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return getEmptyText(language);
    }


    private static String getEmptyText(String language) {
        if (language.equalsIgnoreCase("english")) {
            return "N/A";
        } else {
            return "প্রযোজ্য নয়";
        }
    }
}
