package pbReport;

import org.json.JSONException;
import org.json.JSONObject;
import pb.CatRepository;
import repository.RepositoryManager;

public class CatDataByDtoAttributeConverter extends ColumnConvertor{

    @Override
    public Object convert(Object columnValue, String language, String tableName) {
        if (columnValue == null) return "";

        String tableNameExtracted = tableName.split(",")[0];
        String domainName = tableName.split(",")[1];
        String attributeName = tableName.split(",")[2];
        long id = Long.parseLong(columnValue.toString());
        String dtoJson = RepositoryManager.getInstance().getRepository(tableNameExtracted).getDtoJsonById(id);
        if (dtoJson != null) {
            JSONObject jsonObject = null;
            try {
                jsonObject = new JSONObject(dtoJson);
                int catValue = Integer.parseInt(String.valueOf(jsonObject.get(attributeName)));
                return CatRepository.getInstance().getText(language, domainName, catValue);
            } catch (JSONException e) {
                e.printStackTrace();
                return null;
            }
        }
        return null;
    }
}
