package pbReport;

import pb.Utils;
import sessionmanager.SessionConstants;
import util.TimeConverter;

public class DateExceptZeroConvertor extends ColumnConvertor {

	@Override
	public Object convert(Object timeInMills, String language, String tableName) {
		if(timeInMills == null){
			return "";
		}
		long ms = Long.parseLong(timeInMills.toString());
		if(ms <= 0){
			return "";
		}
		String sDate = TimeConverter.getTimeStringFromLong(ms);
		sDate= Utils.getDigits(sDate, language);
		return sDate;
	}

}
