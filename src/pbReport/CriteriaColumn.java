package pbReport;

import common.StringUtils;
import workflow.WorkflowController;

import org.apache.log4j.Logger;

public class CriteriaColumn {
    public String databaseColumnName;
    public String criteriaValue;
    public String operator;
    public String tableName;
    Integer moduleID;
    public String delimeter;
    public String dataType;
    public String ParenthesisRight;
    public String ParenthesisLeft;
    public String placeHolderValue;
    Logger logger = Logger.getLogger(CriteriaColumn.class);

    @Override
    public String toString() {
        return "CriteriaColumn{" +
                "databaseColumnName='" + databaseColumnName + '\'' +
                ", criteriaValue='" + criteriaValue + '\'' +
                ", operator='" + operator + '\'' +
                ", moduleID=" + moduleID +
                ", delimeter='" + delimeter + '\'' +
                ", dataType='" + dataType + '\'' +
                ", ParenthesisRight='" + ParenthesisRight + '\'' +
                ", ParenthesisLeft='" + ParenthesisLeft + '\'' +
                ", placeHolderValue='" + placeHolderValue + '\'' +
                '}';
    }
    
    public String getFormattedValue(String[] criterion, String value)
    {
    	logger.debug("criterion.length = " + criterion.length);
    	if(criterion.length >= 12)
    	{
    		logger.debug("formatter = " + criterion[11]);
            if(criterion[11] == null){
                return value;
            }
    		switch (criterion[11]) {
            case "userNameToEmployeeRecordId":
            	return  WorkflowController.getEmployeeRecordsIdFromUserName(value) + "";
            default:
            	return  value;
    		}
    	}
    	else
    	{
    		return value;
    	}
    }

    public CriteriaColumn(String[] criterion, String value) {

        // 
        tableName = criterion[1];
        String fieldName = criterion[2];
        String operator = criterion[3];
        this.delimeter = criterion[4];
        this.dataType = criterion[5];
        this.ParenthesisLeft = criterion[6];
        this.ParenthesisRight = criterion[7];
        this.placeHolderValue = criterion[8];
        try {
            if (tableName.equals("")) {
                this.databaseColumnName = fieldName;
            } else {
                this.databaseColumnName = tableName + "." + fieldName;
            }

            // logger.debug("columnname = " + this.databaseColumnName + " searching value = " + value);
            if (StringUtils.isBlank(value) || value.toLowerCase().contains("select") || value.equals("any")) {
                // logger.debug("default will be applied");
                value = this.placeHolderValue;
                if (this.placeHolderValue.equals("any")) {
                    value = "%";
                    operator = "like";
                    dataType = "String";
                }
            }
            else
            {
            	value = getFormattedValue(criterion, value);
            }
            
            this.criteriaValue = value;
            
            logger.debug("for value = " + value + " we got formatted value = " + this.criteriaValue);


            if (operator.equalsIgnoreCase("like")) {
            	if(!dataType.equalsIgnoreCase("exact"))
            	{
            		this.criteriaValue = "%" + this.criteriaValue + "%";
            	}
                
            }
          


            this.operator = operator;
        } catch (SecurityException e) {
            throw new RuntimeException(e);
        }
    }
    
    public String getCriterionSql(){
        StringBuilder sqlBuilder = new StringBuilder();

        sqlBuilder.append(" ").append(delimeter).append(" ").append(ParenthesisLeft).append(" ");

        if (operator.equalsIgnoreCase("null")) {
            sqlBuilder.append(" ").append(databaseColumnName).append(" IS ");
            if (criteriaValue.trim().equals("1"))
                sqlBuilder.append(" NOT ");
            sqlBuilder.append(" NULL ");
        } else {
            sqlBuilder.append(" ").append(databaseColumnName).append(" ").append(operator).append(" ");

            if (operator.equalsIgnoreCase("in"))
                sqlBuilder.append(" (").append(criteriaValue).append(") ");
            else if(dataType.equalsIgnoreCase("string"))
                sqlBuilder.append(" '").append(criteriaValue).append("' ");
            else
                sqlBuilder.append(" ").append(criteriaValue).append(" ");
        }
        sqlBuilder.append(" ").append(ParenthesisRight).append(" ");

        return sqlBuilder.toString();
    }
}