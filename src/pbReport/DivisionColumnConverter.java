package pbReport;

import geolocation.GeoDivisionRepository;

public class DivisionColumnConverter extends ColumnConvertor {
    @Override
    public Object convert(Object columnValue, String language, String tableName) {
        if (columnValue == null) {
            return "";
        }
        return GeoDivisionRepository.getInstance().getText(language, Long.parseLong(columnValue.toString()));
    }
}