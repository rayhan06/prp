package pbReport;

import com.google.gson.Gson;
import org.apache.log4j.Logger;
import util.ReportRequestHandler;

import javax.servlet.http.HttpServletRequest;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@SuppressWarnings("ALL")
public class ReportService {
    private final static Logger logger = Logger.getLogger(ReportService.class);
    private final static Gson GSON = new Gson();

    public List<List<Object>> getResultSet(String tableJoinSql, HttpServletRequest request,
                                           int limit, int offset, String[][] criteria, String[][] display, String GroupBy, String OrderBY, String language) throws Exception {
        // 22 Feb, 2022 nayeem bhai told to add overload to fix build error with XLState ReportRequestHandler.BOTH
        return getResultSet2(tableJoinSql, request, limit, offset, criteria, display, GroupBy, OrderBY, language, ReportRequestHandler.BOTH);
    }

    public List<List<Object>> getResultSet(String tableJoinSql, HttpServletRequest request,
                                           int limit, int offset, String[][] criteria, String[][] display, String GroupBy, String OrderBY, String language, int XLState) throws Exception {
        return getResultSet2(tableJoinSql, request, limit, offset, criteria, display, GroupBy, OrderBY, language, XLState);
    }

    public Integer getTotalCount(String tableJoinSql, HttpServletRequest request,
                                 String[][] criteria, String[][] display, String GroupBy, String OrderBY, String language)
            throws Exception {
        return getTotalCount2(tableJoinSql, request, criteria, GroupBy);
    }

    public static List<List<Object>> getResultSet2(String tableJoinSql, HttpServletRequest request,
                                                   int limit, int offset, String[][] criteria, String[][] display, String GroupBy, String OrderBY, String language, int XLState) throws Exception {
        return ReportProcessor.getResult(new ReportMetadata(request, criteria, display, GroupBy, OrderBY, language, XLState), tableJoinSql, limit, offset);
    }

    public static List<List<Object>> getResultSet2(String tableJoinSql, HttpServletRequest request,
                                                   int limit, int offset, String[][] criteria, String[][] display, String GroupBy, String OrderBY, String language) throws Exception {
        return ReportProcessor.getResult(new ReportMetadata(request, criteria, display, GroupBy, OrderBY, language, ReportRequestHandler.BOTH), tableJoinSql, limit, offset);
    }

    public static Integer getTotalCount2(String tableJoinSql, HttpServletRequest request, String[][] criteria, String GroupBy)
            throws Exception {
        return ReportProcessor.getTotalResultCount(new ReportMetadata(request, criteria, GroupBy), tableJoinSql);
    }

    private static Map<Integer, ReportColumn> getReportColumnsByDisplayIndex(HttpServletRequest request) {
        String reportColumnDisplayIndexValues = request.getParameter("reportColumnDisplayIndexValues");
        String reportColumnShowValues = request.getParameter("reportColumnShowValues");
        if (reportColumnDisplayIndexValues == null || reportColumnShowValues == null) {
            return new HashMap<>();
        }
        try {
            int[] displayIndexValues = GSON.fromJson(
                    String.format("[%s]", reportColumnDisplayIndexValues),
                    int[].class
            );
            boolean[] showValues = GSON.fromJson(
                    String.format("[%s]", reportColumnShowValues),
                    boolean[].class
            );
            return IntStream.range(0, displayIndexValues.length)
                            .mapToObj(viewIndex -> ReportColumn.from(displayIndexValues[viewIndex], viewIndex, showValues[viewIndex]))
                            .collect(Collectors.toMap(
                                    reportColumn -> reportColumn.reportDisplayIndex,
                                    Function.identity(),
                                    (e1, e2) -> e1
                            ));
        } catch (Exception e) {
            logger.error(e);
            return new HashMap<>();
        }
    }

    public static List<ReportColumn> getReportColumns(List<Object> columnTitles, String[][] display, HttpServletRequest request) {
        Map<Integer, ReportColumn> inputByDisplayIndex = getReportColumnsByDisplayIndex(request);
        return IntStream.range(0, columnTitles.size())
                        .mapToObj(index -> ReportColumn.from(index, (String) columnTitles.get(index)))
                        .map(reportColumn -> reportColumn.getReportColumnModifiedByUserInput(inputByDisplayIndex.getOrDefault(reportColumn.reportDisplayIndex, null)))
                        .sorted(Comparator.comparingInt(reportColumn -> reportColumn.viewIndex))
                        .collect(Collectors.toList());
    }
}