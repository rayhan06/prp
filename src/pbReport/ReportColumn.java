package pbReport;

public class ReportColumn {
    public int reportDisplayIndex;
    public int viewIndex; // ignored updating in front end. Index on ReportColumn[] in request conveys viewIndex
    public boolean isVisible;
    public boolean show;
    public String title;

    public ReportColumn getReportColumnModifiedByUserInput(ReportColumn reportColumn) {
        if (reportColumn != null) {
            this.viewIndex = reportColumn.viewIndex;
            this.show = reportColumn.show;
        }
        return this;
    }

    public boolean toShowInExcel() {
        return isVisible && show;
    }

    public static ReportColumn from(int index, String title) {
        ReportColumn reportColumn = new ReportColumn();
        reportColumn.reportDisplayIndex = index;
        reportColumn.viewIndex = index;
        reportColumn.isVisible = true;
        reportColumn.show = true;
        reportColumn.title = title;
        return reportColumn;
    }

    public static ReportColumn from(int reportDisplayIndex, int viewIndex, boolean show) {
        ReportColumn reportColumn = new ReportColumn();
        reportColumn.reportDisplayIndex = reportDisplayIndex;
        reportColumn.viewIndex = viewIndex;
        reportColumn.isVisible = true;
        reportColumn.show = show;
        reportColumn.title = null;
        return reportColumn;
    }
}
