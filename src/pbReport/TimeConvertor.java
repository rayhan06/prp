package pbReport;

import java.util.Date;

import pb.Utils;
import sessionmanager.SessionConstants;
import util.HttpRequestUtils;
import util.TimeConverter;

public class TimeConvertor extends ColumnConvertor {

    @Override
    public Object convert(Object timeInMills, String language, String tableName) {
        if (timeInMills == null) {
            return "";
        }
        if(timeInMills.toString().contains("PM") || timeInMills.toString().contains("AM")){

        	return HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language.equalsIgnoreCase("Bangla")?
                    (Utils.getDigitBanglaFromEnglish(timeInMills.toString())):(timeInMills.toString());
        	
		}
        long ms = Long.parseLong(timeInMills.toString());
        if (ms == SessionConstants.MIN_DATE) {
            return "";
        }
        String sDate = TimeConverter.hourMinuteFormat.format(new Date(ms));
        sDate = Utils.getDigits(sDate, language);
        return sDate;
    }

}
