package pbReport;

/*
 * @author Md. Erfan Hossain
 * @created 20/04/2021 - 4:13 PM
 * @project parliament
 */

import util.StringUtils;

public class NumberConverter extends ColumnConvertor{

    @Override
    public Object convert(Object columnValue, String language, String tableName) {
        if(columnValue == null){
            return "";
        }
        if(columnValue instanceof Float){
            float val = (Float)columnValue;
            if(val == 0){
                return "";
            }
            String str = convertToTwoDecimal(String.valueOf(val));
            return StringUtils.convertBanglaIfLanguageIsBangla(language,str);
        }
        if(columnValue instanceof Double){
            double val = (Double)columnValue;
            if(val == 0){
                return "";
            }
            String str = convertToTwoDecimal(String.valueOf(val));
            return StringUtils.convertBanglaIfLanguageIsBangla(language,str);
        }
        if(columnValue instanceof Integer){
            int val = (Integer)columnValue;
            return val == 0? "" : StringUtils.convertBanglaIfLanguageIsBangla(language,String.valueOf(val));
        }
        if(columnValue instanceof Long){
            long val = (Long)columnValue;
            return val == 0? "" : StringUtils.convertBanglaIfLanguageIsBangla(language,String.valueOf(val));
        }
        if(columnValue instanceof String){
            String val = columnValue.toString();
            return val.equals("0") ? "" : StringUtils.convertBanglaIfLanguageIsBangla(language,val);
        }
        return columnValue;
    }

    private String convertToTwoDecimal(String str){
        String[]tokens = str.split("\\.");
        if(tokens.length == 1){
            str =tokens[0]+".00";
        }else{
            if(tokens[1].length() == 1){
                str+="0";
            }else if (tokens[1].length() >2){
                str = tokens[0]+"."+tokens[1].substring(0,2);
            }
        }
        return str;
    }
}
