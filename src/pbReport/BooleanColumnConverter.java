package pbReport;

/*
 * @author Md. Erfan Hossain
 * @created 07/03/2022 - 7:06 AM
 * @project parliament
 */

import util.StringUtils;

public class BooleanColumnConverter extends ColumnConvertor{
    @Override
    public Object convert(Object columnValue, String language, String tableName) {
        boolean isLangEng = "English".equalsIgnoreCase(language);
        String falseValue = StringUtils.getYesNo(isLangEng,false);
        if(columnValue == null){
        	//System.out.println("columnValue == null");
            return falseValue;
        }
        //System.out.println("columnValue = " + columnValue.toString());
        if (columnValue instanceof String){
            return StringUtils.getYesNo(isLangEng,((String) columnValue).equalsIgnoreCase("true"));
        }else if(columnValue instanceof Boolean){
            return StringUtils.getYesNo(isLangEng,columnValue.equals(true));
        }else if(columnValue instanceof Number){
            return StringUtils.getYesNo(isLangEng,((Number) columnValue).longValue() == 1);
        }
        return falseValue;
    }
}