package pbReport;

import employee_records.Employee_recordsDTO;
import employee_records.Employee_recordsRepository;
import pb.Utils;

public class MobileNumberConverter extends ColumnConvertor {

    @Override
    public Object convert(Object columnValue, String language, String tableName) {
        if (columnValue == null) {
            return "";
        }
        long val = Long.parseLong(columnValue.toString());
        Employee_recordsDTO employeeRecordsDTO = Employee_recordsRepository.getInstance().getById(val);
        if (employeeRecordsDTO == null) {
            return "";
        }
        String mobileNumber = employeeRecordsDTO.personalMobile;
        return Utils.getDigits(mobileNumber, language);
    }
}
