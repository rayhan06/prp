package pbReport;

import ministry_office.Ministry_officeRepository;
import ministry_office_mapping.Ministry_office_mappingDAO;
import ministry_office_mapping.Ministry_office_mappingDTO;
import ministry_office_mapping.Ministry_office_mappingRepository;
import pb.CatRepository;
import pb.Utils;

import java.util.List;
import java.util.stream.Collectors;

public class MpReportMinistryConverter extends ColumnConvertor {
    @Override
    public Object convert(Object columnValue, String language, String tableName) {
        if (columnValue == null) {
            return "";
        }

        StringBuilder stringBuilder = new StringBuilder();
        long employeeRecordId = Long.parseLong(columnValue.toString());
        int ministryCount = 1;

        List<Ministry_office_mappingDTO> ministry_office_mappingDTOS = Ministry_office_mappingRepository.getInstance().getMinistry_office_mappingDTOsByEmployeeRecordId(employeeRecordId);

        for (Ministry_office_mappingDTO dto: ministry_office_mappingDTOS) {
            stringBuilder.append(Utils.getDigits(ministryCount++, language)).append(". ").append(Ministry_officeRepository.getInstance().getText(language, dto.ministryOfficeId)).
                    append(" (").append(CatRepository.getInstance().getText(language, "ministers", dto.ministersCat)).append(")    ");
        }

        return stringBuilder.toString();
    }
}


