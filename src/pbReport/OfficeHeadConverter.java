package pbReport;

import employee_offices.EmployeeOfficeDTO;
import employee_offices.EmployeeOfficeRepository;
import employee_records.Employee_recordsDTO;
import employee_records.Employee_recordsRepository;
import office_unit_organograms.OfficeUnitOrganograms;
import office_unit_organograms.OfficeUnitOrganogramsRepository;
import org.apache.commons.lang3.StringUtils;

import java.math.BigInteger;

public class OfficeHeadConverter extends ColumnConvertor {

    @Override
    public Object convert(Object value, String language, String tableName) {
        if (value == null) {
            return "";
        }
        if (value instanceof String) {
            if (StringUtils.isNumeric((String) value)) {
                value = Long.parseLong((String) value);
            } else {
                return value;
            }
        }

        if (value instanceof Integer || value instanceof BigInteger) {
            value = Long.parseLong(String.valueOf(value));
        }

        String head = "";
        OfficeUnitOrganograms org = OfficeUnitOrganogramsRepository.getInstance().getOfficeUnitHeadWithActiveEmpStatus((long) value, true);
        if (org != null) {
            boolean isLangEng = language.equalsIgnoreCase("English");
            EmployeeOfficeDTO employeeOfficeDTO = EmployeeOfficeRepository.getInstance().getByOfficeUnitOrganogramId(org.id);
            if (employeeOfficeDTO != null) {
                Employee_recordsDTO employeeRecordsDTO = Employee_recordsRepository.getInstance().getById(employeeOfficeDTO.employeeRecordId);
                if (employeeRecordsDTO != null) {
                    head = isLangEng ? employeeRecordsDTO.nameEng : employeeRecordsDTO.nameBng;
                }

            }
            if (!head.isEmpty()) {
                head += ", ";
            }
            head += isLangEng ? org.designation_eng : org.designation_bng;
        }
        return head;
    }

}
