package pbReport;

import util.ReportRequestHandler;

public abstract class ColumnConvertor {
	public int XLState = ReportRequestHandler.BOTH;
	public abstract Object convert(Object columnValue, String language, String tableName);
	
}
