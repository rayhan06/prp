package pbReport;

import java.math.BigInteger;

import org.apache.commons.lang3.StringUtils;

import xray_report.Xray_reportDTO;
import xray_report.Xray_reportRepository;

public class XRayConvertor extends ColumnConvertor {

	@Override
	public Object convert(Object value, String language, String tableName) {
		// TODO Auto-generated method stub
		long id = -1;
		if(value == null)
    	{
    		return "";
    	}
        if(value instanceof String){
        	if(StringUtils.isNumeric((String) value))
        	{
        		id = Long.parseLong((String) value);
        	}
        	else return "";       	
            
        }
        else if(value instanceof Integer || value instanceof BigInteger || value instanceof Long){
			id = (Long)value;
		}
		Xray_reportDTO xray_reportDTO = Xray_reportRepository.getInstance().getXray_reportDTOByID(id);
		if(xray_reportDTO != null)
		{
			return xray_reportDTO.comment;
		}
		return "";
	}

}
