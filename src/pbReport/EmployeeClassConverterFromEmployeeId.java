package pbReport;

/*
 * @author Md. Erfan Hossain
 * @created 21/04/2021 - 7:56 PM
 * @project parliament
 */

import employee_records.Employee_recordsDTO;
import employee_records.Employee_recordsRepository;
import pb.CatRepository;

public class EmployeeClassConverterFromEmployeeId  extends ColumnConvertor {
    @Override
    public Object convert(Object columnValue, String language, String tableName) {
        if (columnValue == null) {
            return "";
        }
        Employee_recordsDTO employeeRecordsDTO = Employee_recordsRepository.getInstance().getById(Long.parseLong(columnValue.toString()));
        if (employeeRecordsDTO == null) {
            return "";
        }
        return CatRepository.getInstance().getText(language,"employee_class",employeeRecordsDTO.employeeClass);
    }
}
