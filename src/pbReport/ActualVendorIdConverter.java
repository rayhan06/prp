package pbReport;

import pi_vendor_auctioneer_details.Pi_vendor_auctioneer_detailsRepository;

public class ActualVendorIdConverter extends ColumnConvertor{

    @Override
    public Object convert(Object columnValue, String language, String tableName) {
        if (columnValue == null) return "";

        long id = Long.parseLong(columnValue.toString());
        return Pi_vendor_auctioneer_detailsRepository.getInstance().getText(id, language);
    }
}
