package pbReport;

import geolocation.GeoLocationRepository;

public class HomeDistrictConverter extends ColumnConvertor {
    @Override
    public Object convert(Object columnValue, String language, String tableName) {
        if (columnValue == null) {
            return "";
        }

        return GeoLocationRepository.getInstance().getText(language, (int) columnValue);
    }
}