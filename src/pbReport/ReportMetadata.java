package pbReport;



import org.apache.log4j.Logger;

import util.ReportRequestHandler;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;


class DisplayColumn {


    String databaseColumnName;
    String caption;
    String language = "english";
    String tableName = "";
    public String converterType = "";
    int XLState = ReportRequestHandler.BOTH;
    ColumnConvertor convertor = new DefaultConvertor();

    @Override
    public String toString() {
        return "DisplayColumn{" +
                "databaseColumnName='" + databaseColumnName + '\'' +
                ", caption='" + caption + '\'' +
                '}';
    }

    public DisplayColumn(String[] tokens, String value, String language, int XLState) {


        try {


            this.caption = value;
            if (tokens[1] != null && tokens[1].trim().length() > 0) {
                this.databaseColumnName = tokens[1] + ".";
            } else {
                this.databaseColumnName = "";
            }
            this.databaseColumnName += tokens[2];
            this.language = language;
            this.XLState = XLState;
            converterType = tokens[3];


            switch (tokens[3]) {
                case "date":
                    this.convertor = new DateConvertor();
                    break;
                case "dateExceptZero":
                    this.convertor = new DateExceptZeroConvertor();
                    break;
                case "time":
                    this.convertor = new TimeConvertor();
                    break;
                case "xray":
                    this.convertor = new XRayConvertor();
                    break;
                case "geolocation":
                    this.convertor = new GeoConvertor();
                    break;
                /*case "employee_office":
                    this.convertor = new EmployeeOfficeConverter();
                    break;*/
                case "office_unit":
                    this.convertor = new OfficeUnitConverter();
                    break;
                case "office_head":
                    this.convertor = new OfficeHeadConverter();
                    break;
                case "none":
                    this.convertor = new NoneConverter();
                    break;
                case "office_unit_organogram":
                    this.convertor = new OfficeUnitOrganogramConverter();
                    break;
                case "organogram":
                    this.convertor = new OrganogramConverter();
                    break;
                case "organograms":
                    this.convertor = new OrganogramListConverter();
                    break;
                case "organogram_to_name":
                    this.convertor = new OrganogramToNameConverter();
                    break;
                case "dr_organogram":
                    this.convertor = new DrOrganogramToNameConverter();
                    break;
                case "dr_dept":
                    this.convertor = new DrOrganogramToDeptConverter();
                    break;
                case "dr_user_name":
                    this.convertor = new DrUserNameToNameConverter();
                    break;
                case "organogram_and_wing":
                    this.convertor = new OrganogramAndWingConverter();
                    break;
                case "organogram_to_office_type":
                    this.convertor = new OrganogramToOfficeTypeConverter();
                    break;
                case "organogram_to_wing":
                    this.convertor = new OrganogramToWingConverter();
                    break;
                case "organogram_to_office_unit":
                    this.convertor = new OrganogramToOfficeUnitConverter();
                    break;
                case "username_and_name":
                    this.convertor = new UserNameAndNameConverter();
                    break;
                case "userIdToName":
                    this.convertor = new UserIdToNameConverter();
                    break;
                case "username_to_phone":
                    this.convertor = new usernameToPhoneConverter();
                    break;
                case "assignment_status":
                    this.convertor = new CatConverter();
                    tableName = "assignment_status";
                    break;
                case "software_sub_cat":
                    this.convertor = new SoftwareSubCatConverter();
                    break;
                case "vehicle_status":
                    this.convertor = new CatConverter();
                    tableName = "pi_vendor_status";
                    break;
                case "ticket_action":
                    this.convertor = new TicketActionConverter();
                    break;
                case "user_id":
                    this.convertor = new UserIdConverter();
                    break;
                case "cat":
                    this.convertor = new CatConverter();
                    tableName = tokens[2].substring(0, tokens[2].length() - "_cat".length());
                    break;
                case "cat_multiple":
                    this.convertor = new VehicleTypeCatMultipleConverter();
                    break;
                case "medical_transaction":
                    this.convertor = new CatConverter();
                    tableName = "medical_transaction";
                    break;
                case "type":
                    this.convertor = new TypeConverter();
                    tableName = tokens[2].substring(0, tokens[2].length() - "_type".length());
                    break;
                case "id":
                    this.convertor = new TypeConverter();
                    tableName = tokens[2].substring(0, tokens[2].length() - "_id".length());
                    break;
                case "name_type":
                    this.convertor = new NameTypeConverter();
                    tableName = tokens[2].substring(0, tokens[2].length() - "_type".length());
                    break;
                case "file":
                    this.convertor = new FileConverter();
                    this.convertor.XLState = this.XLState;
                    break;
                case "double":
                    this.convertor = new DoubleConverter();
                    break;
                case "int":
                    this.convertor = new IntConverter();
                    break;
                case "text_type":
                    this.convertor = new TextInterfaceConverter();
                    tableName = tokens[2].substring(0, tokens[2].length() - "_type".length());
                    break;
                case "office_type":
                    this.convertor = new CatConverter();
                    tableName = "office_type";
                    break;
                case "other_office":
                    this.convertor = new OtherOfficeConverter();
                    tableName = "other_office";
                    break;
                case "employee_records_id":
                    this.convertor = new EmployeeRecordIdToDataConverter("employee_records_id");
                    break;
                case "employee_records_id_bn":
                    this.convertor = new EmployeeRecordIdToDataConverter("employee_records_id_bn");
                    break;
                case "employee_records_id_en":
                    this.convertor = new EmployeeRecordIdToDataConverter("employee_records_id_en");
                    break;
                case "employee_records_id_details":
                    this.convertor = new EmployeeRecordIdToDataConverter("employee_records_id_details");
                    break;
                case "incharge_label":
                    this.convertor = new InChargeLevelConverter();
                    break; case "grade":
                    this.convertor = new GradeConverter();
                    break;
                case "boolean":
                    this.convertor = new BooleanColumnConverter();
                    break;
                case "status":
                    this.convertor = new StatusConverter();
                    break;
                case "number":
                    this.convertor = new NumberConverter();
                    break;
                case "age":
                    this.convertor = new AgeConverter();
                    break;
                case "complete_age":
                    this.convertor = new CompleteAgeConverter();
                    break;
                case "category":
                    this.convertor = new CategoryConverter();
                    tableName = tokens[2].substring(0, tokens[2].length() - "_cat".length());
                    break;
                case "categoryWithTableName":
                	this.convertor = new CategoryConverter();
                	tableName = tokens[1];
                    break;
                case "vaccineName":
                	this.convertor = new CategoryConverter();
                	tableName = "covid_vaccine";
                    break;
                case "ticketIssueType":
                	this.convertor = new TypeConverter();
                	tableName = "ticket_issues";
                    break;
                case "usernameToName":
                    this.convertor = new UserNameToNameConverter();
                    break;
                case "erIdToName":
                    this.convertor = new ERIdToNameConverter();
                    break;
                case "erIdToPhone":
                    this.convertor = new ERIdToPhoneConverter();
                    break;
                case "erIdToUserName":
                    this.convertor = new ERIdToUserNameConverter();
                    break;
                case "erIdToOrganogram":
                    this.convertor = new ErIdToOrganogramConverter();
                    break;
                case "erIdToOffice":
                    this.convertor = new ErIdToOfficeConverter();
                    break;
                case "categorywithoutCat":
                    this.convertor = new CategoryConverter();
                    tableName = tokens[2];
                    break;
                case "attachment_status":
                    this.convertor = new CategoryConverter();
                    tableName = "attachment_status";
                    break;
                case "complete_age_employee":
                    this.convertor = new EmployeeRecordIdToDataConverter("complete_age_employee");
                    break;
                case "gender_employee":
                    this.convertor = new EmployeeRecordIdToDataConverter("gender_employee");
                    break;
                case "emp_class_employee":
                    this.convertor = new EmployeeClassConverterFromEmployeeId();
                    break;
                case "proc_report_type":
                    this.convertor = new TypeConverter();
                    TypeConverter.tableName = tokens[2].substring(0, tokens[2].length() - "_id".length());
                    break;
                case "religion":
                    this.convertor = new ReligionConverter();
                    tableName = tokens[2];
                    break;
                case "district":
                    this.convertor = new DistrictColumnConverter();
                    break;
                case "role":
                    this.convertor = new RoleColumnConverter();
                    break;
                case "parliamentJoinDate":
                    this.convertor = new ParliamentJoinDateColumnConverter();
                    break;
                case "parliamentOfficeUnit":
                    this.convertor = new ParliamentOfficeColumnConverter();
                    break;
                case "parliamentOrganogram":
                    this.convertor = new ParliamentOrganogramColumnConverter();
                    break;
                case "govtJobJoinDate":
                    this.convertor = new GovtJobJoinDateColumnConverter();
                    break;
                case "govtJobOfficeUnit":
                    this.convertor = new GovtJobOfficeColumnConverter();
                    break;
                case "govtJobOrganogram":
                    this.convertor = new GovtJobOrganogramColumnConverter();
                    break;
                case "employee_records_id_class":
                    this.convertor = new EmployeeRecordIdToDataConverter("employee_records_id_class");
                    break;
                case "gate_pass_visitor_name":
                    this.convertor = new GatePassDataConverter("gate_pass_visitor_name");
                    break;
                case "gate_pass_visitor_phone":
                    this.convertor = new GatePassDataConverter("gate_pass_visitor_phone");
                    break;
                case "gate_pass_type":
                    this.convertor = new GatePassDataConverter("gate_pass_type");
                    break;
                case "gate_pass_sub_type":
                    this.convertor = new GatePassDataConverter("gate_pass_sub_type");
                    break;
                case "gate_pass_first_layer_approval":
                    this.convertor = new GatePassDataConverter("gate_pass_first_layer_approval");
                    break;
                case "gate_pass_second_layer_approval":
                    this.convertor = new GatePassDataConverter("gate_pass_second_layer_approval");
                    break;
                case "gate_pass_is_issued":
                    this.convertor = new GatePassDataConverter("gate_pass_is_issued");
                    break;
                case "division":
                    this.convertor = new DivisionColumnConverter();
                    break;
                case "parliament_number":
                    this.convertor = new ParliamentNumberConverter();
                    break;
                case "political_party":
                    this.convertor = new PoliticalPartyConverter();
                    break;
                case "election_constituency":
                    this.convertor = new ElectionConstituencyConverter();
                    break;
                case "election_constituency_with_number":
                    this.convertor = new ElectionConstituencyWithNumberConverter();
                    break;
                case "reg_no":
                    this.convertor = new RegistrationNumberConverter();
                    break;

                case "vm_requisition_status_check":
                    this.convertor = new VmRequisitionStatusConverter();
                    break;

                case "given_vehicle_type_check":
                    this.convertor = new GivenVehicleTypeConverter();
                    break;

                case "vehicle_driver_assignment_id_check":
                    this.convertor = new VehicleDriverAssignmentIdConverter();
                    break;

                case "vehicle_supplier_converter":
                    this.convertor = new AssetSupplierConverter();
                    break;

                case "vm_fuel_request_approval_status_check":
                    this.convertor = new VehicleFuelStatusConverter();
                    break;

                case "fiscal_year_id_check":
                    this.convertor = new FiscalYearConverter();
                    break;

                case "vendor_id_check":
                    this.convertor = new VendorIdConverter();
                    break;
                case "actual_vendor_id":
                    this.convertor = new ActualVendorIdConverter();
                    break;
                case "procurement_package_id_check":
                    this.convertor = new ProcurementGoodsTypeConverter();
                    break;
                case "procurement_goods_type_id_check":
                    this.convertor = new ProcurementGoodsTypeConverter();
                    break;
                case "user_name":
                    this.convertor = new UserNameConverter();
                    break;
                case "mobile_number":
                    this.convertor = new MobileNumberConverter();
                    break;
                case "email":
                    this.convertor = new EmailConverter();
                    break;
                case "id_to_present_district":
                    this.convertor = new RecordIdtoPresentDistrictConverter();
                    break;
                case "id_to_permanent_district":
                    this.convertor = new RecordIdtoPermanentDistrictConverter();
                    break;
                case "id_to_home_district":
                    this.convertor = new RecordIdtoHomeDistrictConverter();
                    break;
                case "location":
                    this.convertor = new LocationConverter();
                    break;
                case "id_to_present_address":
                    this.convertor = new RecordIdtoPresentAddressConverter();
                    break;
                case "id_to_permanent_address":
                    this.convertor = new RecordIdtoPermanentAddressConverter();
                    break;
                case "concated_to_office":
                    this.convertor = new IdToOfficeConverter();
                    break;
                case "am_office_status_check":
                    this.convertor = new AmOfficeStatusConverter();
                    break;
                case "am_house_sub_class":
                    this.convertor = new AmHouseSubClassConverter();
                    break;
                case "am_house_status_check":
                    this.convertor = new AmHouseStatusConverter();
                    break;
                case "am_house_allocation_emp_name":
                    this.convertor = new AmHouseEmpNameConverter();
                    break;
                case "am_house_allocation_emp_org":
                    this.convertor = new AmHouseEmpOrgNameConverter();
                    break;
                case "am_house_allocation_emp_office":
                    this.convertor = new AmHouseEmpOfficeNameConverter();
                    break;
                case "dateConverterForMinusOne":
                    this.convertor = new DateConvertorForMinusOne();
                    break;
                case "amHouseIdConverter":
                    this.convertor = new AmHouseIdConverter();
                    break;
                case "cacheDataByDtoAttribute":
                    this.convertor = new CacheDataByDtoAttributeConverter();
                    tableName = tokens[5];
                    break;
                case "catDataByDtoAttribute":
                    this.convertor = new CatDataByDtoAttributeConverter();
                    tableName = tokens[5];
                    break;
                case "commonApprovalStatus":
                    this.convertor = new CommonApprovalStatusConverter();
                    break;

                case "productIdConverter":
                    this.convertor = new ProductIdConverter();
                    break;

                case "plain":
                    this.convertor = new PlainConverter();
                    break;

                case "organogram_key":
                    this.convertor = new OrganogramKeyConverter();
                    break;
                case "designation":
                    this.convertor = new DesignationConverter();
                    break;
                case "parliamentary_committee":
                    this.convertor = new ParliamentaryCommitteeConverter();
                    break;
                case "mp_report_committee":
                    this.convertor = new MpReportCommitteeConverter();
                    break;
                case "mp_report_ministry":
                    this.convertor = new MpReportMinistryConverter();
                    break;
                case "home_district":
                    this.convertor = new HomeDistrictConverter();
                    break;
                case "basic":
                    this.convertor = new BasicConverter();
                    break;
                case "country":
                    this.convertor = new CountryColumnConverter();
                    break;
                default:
                    this.convertor = new DefaultConvertor();
                    break;
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }


}

public class ReportMetadata {
    private static final Logger logger = Logger.getLogger(ReportMetadata.class);
    private List<DisplayColumn> outputColumnNames = new ArrayList<>();
    private List<CriteriaColumn> inputColumnNames;
    public String GroupBy = "";
    public String OrderBy = "";
    public int XLState = ReportRequestHandler.BOTH;
    public List<Object> captionList = new ArrayList<>();


    public List<DisplayColumn> getOutputColumnNames() {
        return outputColumnNames;
    }


    public void setOutputColumnNames(List<DisplayColumn> outputColumnNames) {
        this.outputColumnNames = outputColumnNames;
    }


    public List<CriteriaColumn> getInputColumnNames() {
        return inputColumnNames;
    }


    public void setInputColumnNames(List<CriteriaColumn> inputColumnNames) {
        this.inputColumnNames = inputColumnNames;
    }


    public ReportMetadata(HttpServletRequest request, String[][] criteria, String[][] display, String GroupBy, String OrderBY, String language, int XLState) {
        inputColumnNames = Stream.of(criteria)
                .map(criterion -> new CriteriaColumn(criterion, request.getParameter(criterion[9])))
                .collect(Collectors.toList());
        
        

        Stream.of(display).forEach(strings -> {
            outputColumnNames.add(new DisplayColumn(strings, strings[4], language, XLState));
            captionList.add(strings[4]);
        });
        this.GroupBy = GroupBy;
        this.OrderBy = OrderBY;
        this.XLState = XLState;
        logger.debug("inputColumnNames " + inputColumnNames);
        logger.debug("outputColumnNames " + outputColumnNames);

    }

    public ReportMetadata(HttpServletRequest request, String[][] criteria, String GroupBy) {
        inputColumnNames = Stream.of(criteria)
                .map(criterion -> new CriteriaColumn(criterion, request.getParameter(criterion[9])))
                .collect(Collectors.toList());
        this.GroupBy = GroupBy;
        logger.debug("inputColumnNames " + inputColumnNames);
    }
}
