package pbReport;

import workflow.WorkflowController;

public class UserIdToNameConverter extends ColumnConvertor {

	@Override
	public Object convert(Object value, String language, String tableName) {
		Long userId = -1L;
		if (value instanceof Long)
		{
			userId = (Long)value;
		}
		else
		{
			userId = Long.parseLong(value.toString());
		}
		// TODO Auto-generated method stub
		try {
			return WorkflowController.getNameFromUserId(userId, language);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "N/A";
	}

}
