package pbReport;

import procurement_package.ProcurementGoodsTypeRepository;
import procurement_package.Procurement_packageRepository;

public class ProcurementPackageIdConverter extends ColumnConvertor{

    @Override
    public Object convert(Object columnValue, String language, String tableName) {
        if (columnValue == null) return "";

        long id = Long.parseLong(columnValue.toString());
        return Procurement_packageRepository.getInstance().getText(id, language);
    }
}
