package pbReport;

import workflow.WorkflowController;

public class OrganogramAndWingConverter extends ColumnConvertor {

	@Override
	public Object convert(Object value, String language, String tableName) {
		Long organogramId = -1L;
		if (value instanceof Long)
		{
			organogramId = (Long)value;
		}
		else
		{
			organogramId = Long.parseLong(value.toString());
		}
		// TODO Auto-generated method stub
		String name =  WorkflowController.getNameFromOrganogramId(organogramId, language);
		String wingName = WorkflowController.getWingNameFromOrganogramId(organogramId, language);
		if(wingName != null && !wingName.equalsIgnoreCase(""))
		{
			name += ", " + wingName;
		}
		return name;
	}

}
