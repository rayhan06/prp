package pbReport;

public class UserIdConverter extends ColumnConvertor {

	@Override
	public Object convert(Object value, String language, String tableName) {
		String userId = value.toString();
		if(userId.equalsIgnoreCase("-1"))
		{
			return "";
		}
		return userId;
	}

}
