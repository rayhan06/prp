package pbReport;

import employee_service_history.Employee_service_historyDAO;
import employee_service_history.Employee_service_historyDTO;

import java.util.Collections;
import java.util.List;

public class GovtJobOrganogramColumnConverter extends ColumnConvertor {
    public String convert(Object columnValue, String language, String tableName) {
        if (columnValue == null) {
            return "";
        }
        List<Employee_service_historyDTO> employee_service_historyDTOList = Employee_service_historyDAO.getInstance().getDTOSgovJobbyEmployeeId(Long.parseLong(columnValue.toString()));
        if (employee_service_historyDTOList != null && employee_service_historyDTOList.size() >= 1) {
            Collections.sort(employee_service_historyDTOList);
            return employee_service_historyDTOList.get(0).designation;
        } else {
            return "";
        }
    }
}