package pbReport;

/*
 * @author Md. Erfan Hossain
 * @created 26/04/2021 - 11:57 AM
 * @project parliament
 */

import com.google.gson.Gson;
import common.ApiResponse;
import common.RequestFailureException;
import language.LC;
import language.LM;
import login.LoginDTO;
import office_units.Office_unitsRepository;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import user.UserDTO;
import util.ActionTypeConstant;
import util.HttpRequestUtils;
import util.ReportRequestHandler;
import util.StringUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.Collections.singletonList;

@SuppressWarnings({"Duplicates", "unused"})
public interface ReportCommonService extends GenerateExcelReportService {
    Logger logger = Logger.getLogger(ReportCommonService.class);

    /*
     * Index-0 -> default String criteria
     * Index-1 -> table's anonymous
     * Index-2 -> column name
     * Index-3 -> operator (=,IN,LIKE)
     * Index-4 -> Conjunction (OR,AND)
     * Index-5 -> input data type
     * Index-6 -> Left Parenthesis
     * Index-7 -> Right Parenthesis
     * Index-8 -> default Input value (This value is applied if input is not found)
     * Index-9 -> input param's name
     * Index-10 -> input value
     * Index-11 -> input label
     * Index-12 -> input criteria converter name
     * Index-13 -> table name
     * Index-14 -> display input's in Excel file or not(true->display, orElse->Not display)
     * Index-15 -> displaying input order in Excel
     * */
    String[][] getCriteria();

    default String[][] getXLCriteria() {
        return getCriteria();
    }

    /*
     * Index-0 -> default String display
     * Index-1 -> table's anonymous
     * Index-2 -> column name
     * Index-3 -> column converter key
     * Index-4 -> display column name in report
     * */
    String[][] getDisplay();

    default String getGroupBy() {
        return "";
    }

    default boolean excelHasFooter() {
        return false;
    }

    default String getOrderBy(HttpServletRequest request) {
        String orderBy = "";
        int orderByCol = -1;
        boolean isAscending = true;
        if (request.getParameter("orderByCol") != null && !request.getParameter("orderByCol").equalsIgnoreCase("")) {
            orderByCol = Integer.parseInt(request.getParameter("orderByCol"));
        }
        if (request.getParameter("isAscending") != null && !request.getParameter("isAscending").equalsIgnoreCase("")) {
            isAscending = Boolean.parseBoolean(request.getParameter("isAscending"));
        }

        if (orderByCol > 0 && orderByCol <= getDisplay().length) {
            int actualColIndex = orderByCol - 1;
            if (getDisplay()[actualColIndex][1].length() > 0) {
                orderBy = " " + getDisplay()[actualColIndex][1] + ".";
            } else {
                orderBy = " ";
            }
            orderBy += getDisplay()[actualColIndex][2] + " " + (isAscending ? "asc" : "desc") + " ";
        }
        return orderBy;
    }

    String getSQL();

    int getLCForReportName();

    String getFileName();

    String getTableName();

    default String getReportName(LoginDTO loginDTO) {
        return LM.getText(getLCForReportName(), loginDTO);
    }

    default List<List<Object>> doModificationOnResultList(List<List<Object>> list, String language) {
        return list;
    }

    default List<List<Object>> reorderColumnsForReportResult(List<List<Object>> columnByDisplayIndexList, List<ReportColumn> reportColumns) {
        Function<List<Object>, List<Object>> reorderColumnByReportColumns =
                columnByDisplayIndex -> reportColumns.stream()
                                                     .map(reportColumn -> columnByDisplayIndex.get(reportColumn.reportDisplayIndex))
                                                     .collect(Collectors.toList());
        return columnByDisplayIndexList
                .stream()
                .map(reorderColumnByReportColumns)
                .collect(Collectors.toList());
    }

    default List<List<Object>> reorderColumnsForExcelReport(List<List<Object>> columnByDisplayIndexList, List<ReportColumn> reportColumns) {
        Function<List<Object>, List<Object>> reorderColumnByReportColumns =
                columnByDisplayIndex -> reportColumns.stream()
                                                     .filter(ReportColumn::toShowInExcel)
                                                     .map(reportColumn -> columnByDisplayIndex.get(reportColumn.reportDisplayIndex))
                                                     .collect(Collectors.toList());
        return columnByDisplayIndexList
                .stream()
                .map(reorderColumnByReportColumns)
                .collect(Collectors.toList());
    }

    default Integer getTotalEntryCount(String tableJoinSql, HttpServletRequest request, String[][] criteria, String GroupBy) throws Exception {
        return ReportService.getTotalCount2(getSQL(), request, getCriteria(), getGroupBy());
    }

    default ReportTemplate getReportTemplate() {
        return new ReportTemplate();
    }

    default void reportGenerate(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;
        UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;
        String language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
        ApiResponse apiResponse = null;
        String actionType = request.getParameter("actionType");
        if (ActionTypeConstant.REPORT_PAGE.equals(actionType)) {
            request.setAttribute("tableName", getTableName());
            request.setAttribute("reportName", getReportName(loginDTO));
            request.setAttribute("bodyName", getTableName() + "/" + getTableName() + "_Body.jsp");
            request.getRequestDispatcher("pbreport/report.jsp").forward(request, response);
            return;
        } else if (ActionTypeConstant.REPORT_COUNT.equals(actionType)) {
            try {
                int count = ReportService.getTotalCount2(getSQL(), request, getCriteria(), getGroupBy());
                apiResponse = ApiResponse.makeSuccessResponse(count, "Success");
            } catch (Exception ex) {
                apiResponse = ApiResponse.makeErrorResponse(ex.getMessage());
                if (ex instanceof RequestFailureException) {
                    throw (RequestFailureException) ex;
                }
            }
        } else if (ActionTypeConstant.REPORT_RESULT.equals(actionType)) {
            try {
                int recordPerPage = Integer.parseInt(request.getParameter("RECORDS_PER_PAGE"));
                int pageNo = Integer.parseInt(request.getParameter("pageno"));
                logger.debug("recordPerPage = " + recordPerPage + " pageNo = " + pageNo);
                int offset = (pageNo - 1) * recordPerPage;
                List<List<Object>> list = ReportService.getResultSet2(getSQL(), request, recordPerPage, offset, getCriteria(), getDisplay(), getGroupBy(), getOrderBy(request), language);
                list = doModificationOnResultList(list, language);
                addSerialNoColumn(language, list, userDTO, offset);
                request.setAttribute("TotalListSize", list.size() - 1);
                // int count = ReportService.getTotalCount2(getSQL(), request, getCriteria(), getGroupBy());
                int count = getTotalEntryCount(getSQL(), request, getCriteria(), getGroupBy());

                List<ReportColumn> reportColumns = ReportService.getReportColumns(list.get(0), getDisplay(), request);
                List<Object> combinedResponseList = Arrays.asList(
                        reorderColumnsForReportResult(list, reportColumns),
                        count,
                        reportColumns
                );
                apiResponse = ApiResponse.makeSuccessResponse(combinedResponseList, "Success");
            } catch (Exception ex) {
                ex.printStackTrace();
                apiResponse = ApiResponse.makeErrorResponse(ex.getMessage());
                if (ex instanceof RequestFailureException) {
                    throw (RequestFailureException) ex;
                }
            }
        } else if (ActionTypeConstant.REPORT_TEMPLATE.equals(actionType)) {
            apiResponse = ApiResponse.makeSuccessResponse(getReportTemplate(), "Success");
        } else if (ActionTypeConstant.EXCEL_REPORT.equals(actionType)) {
            try {
                List<List<Object>> reportColumnsList = ReportService.getResultSet2(getSQL(), request, -1, -1, getCriteria(), getDisplay(), getGroupBy(), getOrderBy(request), language, ReportRequestHandler.XL);
                reportColumnsList = doModificationOnResultList(reportColumnsList, language);
                addSerialNoColumn(language, reportColumnsList, userDTO, 0);
                reportColumnsList = reorderColumnsForExcelReport(
                        reportColumnsList,
                        ReportService.getReportColumns(reportColumnsList.get(0), getDisplay(), request)
                );

                List<List<String>> inputCriteriaList =
                        Stream.of(getXLCriteria())
                              .filter(arr -> Boolean.parseBoolean(arr[14]))
                              .map(Arrays::asList)
                              .sorted(Comparator.comparingInt(criteriaList -> Integer.parseInt(criteriaList.get(15))))
                              .map(criteriaList -> Arrays.asList(LM.getText(Long.parseLong(criteriaList.get(11)), loginDTO), CriteriaConverterMap.getValue(criteriaList.get(12), request.getParameter(criteriaList.get(10)), language, criteriaList.get(13))))
                              .collect(Collectors.toList());
                logger.debug("inputCriteriaList : " + inputCriteriaList);

                XSSFWorkbook workbook = createXl(reportColumnsList, inputCriteriaList, getReportName(loginDTO), language, userDTO);
                response.setContentType("application/vnd.ms-excel");
                response.setHeader("Content-Disposition", "attachment; filename=" + getFileName() + ".xlsx");
                workbook.write(response.getOutputStream());
                workbook.close();
                return;
            } catch (Exception ex) {
                ex.printStackTrace();
                return;
            }
        }
        PrintWriter writer = response.getWriter();
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        writer.write(new Gson().toJson(apiResponse));
        writer.flush();
        writer.close();
    }

    default XSSFWorkbook createXl(List<List<Object>> data) {
        XSSFWorkbook wb = new XSSFWorkbook();
        Sheet sheet = wb.createSheet();
        logger.debug("Data size = " + data.size());
        int i = 0;
        for (List<Object> rowData : data) {
            logger.debug("rowData size = " + rowData.size());
            Row row = sheet.createRow(i);

            int j = 0;
            for (Object cellData : rowData) {
                Cell cell = row.createCell(j);
                cell.setCellValue(cellData.toString());

                if (i == 0 || i == data.size() - 1) {
                    XSSFCellStyle cellStyle = wb.createCellStyle();
                    XSSFFont font = wb.createFont();
                    font.setBold(true);
                    cellStyle.setFont(font);
                    cell.setCellStyle(cellStyle);
                }
                j++;
            }
            i++;
        }
        return wb;
    }

    default void addSerialNoColumn(String language, List<List<Object>> list, UserDTO userDTO, int offset) {
        list.get(0).add(0, LM.getText(LC.HM_SL, userDTO));
        for (int i = 1; i < list.size(); i++) {
            list.get(i).add(0, "Bangla".equalsIgnoreCase(language) ? StringUtils.convertToBanNumber(String.valueOf(i + offset)) : (i + offset));
        }
    }

    default Set<Long> getOfficeIds(HttpServletRequest request) {
        boolean onlySelectedOffice = Boolean.parseBoolean(request.getParameter("onlySelectedOffice"));
        long officeUnitId = Long.parseLong(request.getParameter("officeUnitId"));
        return getOfficeIdsFromOfficeUnitIds(singletonList(officeUnitId), onlySelectedOffice);
    }

    default Set<Long> getOfficeIdsFromOfficeUnitIds(HttpServletRequest request) {
        List<Long> officeUnitIds =
                Arrays.asList(new Gson().fromJson(
                        request.getParameter("officeUnitIds"),
                        Long[].class
                ));
        boolean onlySelectedOffice = Boolean.parseBoolean(request.getParameter("onlySelectedOffice"));
        return getOfficeIdsFromOfficeUnitIds(officeUnitIds, onlySelectedOffice);
    }

    default Set<Long> getOfficeIdsFromOfficeUnitIds(List<Long> officeUnitIds, boolean onlySelectedOffice) {
        if (onlySelectedOffice) {
            return new HashSet<>(officeUnitIds);
        }
        return officeUnitIds
                .stream()
                .flatMap(officeUnitId -> Office_unitsRepository.getInstance().getAllChildOfficeDTOWithParent(officeUnitId).stream())
                .map(e -> e.iD)
                .collect(Collectors.toSet());
    }
}