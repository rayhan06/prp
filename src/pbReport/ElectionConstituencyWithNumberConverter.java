package pbReport;

import election_constituency.Election_constituencyRepository;

public class ElectionConstituencyWithNumberConverter extends ColumnConvertor {
    @Override
    public Object convert(Object columnValue, String language, String tableName) {
        if (columnValue == null) return "";

        long id = Long.parseLong(columnValue.toString());
        return Election_constituencyRepository.getInstance().getTextWithConstituencyNumber(id, language);
    }
}

