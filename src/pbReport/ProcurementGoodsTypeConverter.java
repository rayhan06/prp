package pbReport;

import procurement_package.ProcurementGoodsTypeRepository;

public class ProcurementGoodsTypeConverter extends ColumnConvertor{

    @Override
    public Object convert(Object columnValue, String language, String tableName) {
        if (columnValue == null) return "";

        long id = Long.parseLong(columnValue.toString());
        return ProcurementGoodsTypeRepository.getInstance().getText(id, language);
    }
}
