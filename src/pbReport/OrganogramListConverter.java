package pbReport;

import java.util.List;

import pb.Utils;
import workflow.WorkflowController;

public class OrganogramListConverter extends ColumnConvertor {

	@Override
	public Object convert(Object value, String language, String tableName) {
		
		List<Long> organograms = Utils.StringToArrayList((String)value);
		String names = "";
		int i = 0;
		for(Long organogram: organograms)
		{
			if(i > 0)
			{
				names += ", ";
			}
			names += WorkflowController.getNameFromOrganogramId(organogram, language);
			i ++;
		}
		return names;
	}

}
