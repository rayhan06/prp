package pbReport;

import pb.Utils;

public class IntConverter extends ColumnConvertor {

	@Override
	public String convert(Object value, String language, String tableName) {
		if(value == null)
		{
			return "";
		}
		Integer iVal;
		if(value instanceof Integer)
		{
			iVal = (int)value;
			return Utils.getDigits(iVal, language, true);
		}
		else if(!value.toString().equalsIgnoreCase(""))
		{
			iVal = Integer.parseInt(value.toString());
			return Utils.getDigits(iVal, language, true);
		}
        return "";
    }

}
