package pbReport;

import employee_offices.EmployeeOfficeDTO;
import employee_offices.EmployeeOfficeRepository;
import employee_service_history.Employee_service_historyDAO;
import employee_service_history.Employee_service_historyDTO;
import pb.Utils;
import util.TimeConverter;

import java.util.Collections;
import java.util.List;

public class GovtJobJoinDateColumnConverter extends ColumnConvertor {
    public String convert(Object columnValue, String language, String tableName) {
        if (columnValue == null) {
            return "";
        }
        List<Employee_service_historyDTO> employee_service_historyDTOList = Employee_service_historyDAO.getInstance().getDTOSgovJobbyEmployeeId(Long.parseLong(columnValue.toString()));
        if (employee_service_historyDTOList != null && employee_service_historyDTOList.size() >= 1) {
            Collections.sort(employee_service_historyDTOList);

            String sDate = TimeConverter.getTimeStringFromLong(employee_service_historyDTOList.get(0).servingFrom);
            sDate = Utils.getDigits(sDate, language);
            return sDate;

        } else {
            return "";
        }


    }
}