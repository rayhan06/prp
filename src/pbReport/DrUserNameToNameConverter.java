package pbReport;

import pb.Utils;
import workflow.WorkflowController;

public class DrUserNameToNameConverter extends ColumnConvertor {

	@Override
	public Object convert(Object value, String language, String tableName) {
		String userName = (String)value;
		String name = WorkflowController.getNameFromUserName(userName, language);
		/*String org = WorkflowController.getOrganogramName(userName, language);
		if(!org.equalsIgnoreCase(""))
		{
			name += ", " + org;
		}*/
		if(name.equalsIgnoreCase("") || name.equalsIgnoreCase(", "))
		{
			if(language.equalsIgnoreCase("english"))
			{
				return "N/A";
			}
			else
			{
				return "প্রযোজ্য নয়";
			}
		}
		return name;
	}

}
