package pbReport;

import office_unit_organograms.OfficeUnitOrganogramsRepository;

import java.util.Arrays;
import java.util.Objects;
import java.util.stream.Collectors;

public class OrganogramKeyConverter extends ColumnConvertor {
    public String convert(Object value, String language, String tableName) {
        return Arrays.stream(String.valueOf(value).split(","))
                .filter(Objects::nonNull)
                .filter(e -> e.contains(":"))
                .map(e -> e.split(":")[0])
                .map(String::trim)
                .map(e-> OfficeUnitOrganogramsRepository.getInstance().getDesignationNameById(Long.valueOf(e),language))
                .filter(e -> e.length() > 0)
                .collect(Collectors.joining(","));
    }
}
