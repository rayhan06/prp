package pbReport;

/*
 * @author Md. Erfan Hossain
 * @created 21/04/2021 - 7:04 PM
 * @project parliament
 */

import pb.Utils;
import sessionmanager.SessionConstants;

public class CompleteAgeConverter extends ColumnConvertor {

    @Override
    public Object convert(Object columnValue, String language, String tableName) {
        if (columnValue == null) {
            return "";
        }
        long value = Long.parseLong(columnValue.toString());
        if(value == SessionConstants.MIN_DATE){
            return "";
        }
        return Utils.calculateCompleteAge(value,language);
    }
}
