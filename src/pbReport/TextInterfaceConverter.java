package pbReport;

/*
 * @author Md. Erfan Hossain
 * @created 10/04/2021 - 3:40 PM
 * @project parliament
 */

import common.TextInterface;
import common.TextInterfaceMap;
import org.apache.log4j.Logger;

public class TextInterfaceConverter extends ColumnConvertor{
    private static final Logger logger = Logger.getLogger(TextInterfaceConverter.class);
    

    @Override
    public String convert(Object value, String language, String tableName) {
        if(value == null){
            return "";
        }
        TextInterface textInterface =  TextInterfaceMap.getByTableName(tableName);
        return textInterface.getText(language,Long.parseLong(value.toString()));
    }
}
