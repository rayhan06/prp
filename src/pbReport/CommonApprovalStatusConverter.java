package pbReport;

import org.json.JSONObject;
import pb.CatRepository;
import repository.RepositoryManager;
import vm_requisition.CommonApprovalStatus;

public class CommonApprovalStatusConverter extends ColumnConvertor{

    @Override
    public Object convert(Object columnValue, String language, String tableName) {
        if (columnValue == null) return "";

        int id = Integer.parseInt(columnValue.toString());
        return CommonApprovalStatus.getText
                (id, language);
    }
}
