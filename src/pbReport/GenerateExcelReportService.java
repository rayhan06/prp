package pbReport;

/*
 * @author Md. Erfan Hossain
 * @created 10/06/2021 - 12:09 AM
 * @project parliament
 */

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.*;
import user.UserDTO;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.List;

@SuppressWarnings("ALL")
public interface GenerateExcelReportService {
    default XSSFCellStyle getBold(XSSFWorkbook wb) {
        XSSFCellStyle cellStyle = wb.createCellStyle();
        XSSFFont font = wb.createFont();
        font.setBold(true);
        cellStyle.setFont(font);
        return cellStyle;
    }

    default XSSFCellStyle getHeaderStyle(XSSFWorkbook wb) {
        XSSFCellStyle cellStyle = wb.createCellStyle();
        cellStyle.setAlignment(HorizontalAlignment.CENTER);
        cellStyle.setFillForegroundColor(IndexedColors.CORNFLOWER_BLUE.getIndex());
        cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        XSSFFont font = wb.createFont();
        font.setFontHeightInPoints((short) 16);
        font.setBold(true);
        cellStyle.setFont(font);
        return cellStyle;
    }

    default XSSFCellStyle getFooterStyle(XSSFWorkbook wb) {
        XSSFCellStyle cellStyle = wb.createCellStyle();
        cellStyle.setAlignment(HorizontalAlignment.LEFT);
        cellStyle.setFillForegroundColor(IndexedColors.AQUA.getIndex());
        cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        XSSFFont font = wb.createFont();
        font.setFontHeightInPoints((short) 16);
        font.setBold(true);
        cellStyle.setFont(font);
        return cellStyle;
    }

    default XSSFCellStyle get1stRowStyle(XSSFWorkbook wb) {
        XSSFCellStyle cellStyle = wb.createCellStyle();
        cellStyle.setFillForegroundColor(IndexedColors.LIGHT_GREEN.getIndex());
        cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        XSSFFont font = wb.createFont();
        font.setBold(true);
        cellStyle.setFont(font);
        return cellStyle;
    }

    default XSSFCellStyle getParamStyle(XSSFWorkbook wb) {
        XSSFCellStyle cellStyle = wb.createCellStyle();
        cellStyle.setFillForegroundColor(IndexedColors.LIGHT_CORNFLOWER_BLUE.getIndex());
        cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        XSSFFont font = wb.createFont();
        font.setBold(true);
        cellStyle.setFont(font);
        return cellStyle;
    }

    default XSSFCellStyle getParamValueStyle(XSSFWorkbook wb) {
        XSSFCellStyle cellStyle = wb.createCellStyle();
        cellStyle.setFillForegroundColor(IndexedColors.LIGHT_CORNFLOWER_BLUE.getIndex());
        cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        XSSFFont font = wb.createFont();
        font.setBold(true);
        cellStyle.setFont(font);
        return cellStyle;
    }

    default void autoSizeColumns(XSSFWorkbook workbook) {
        int numberOfSheets = workbook.getNumberOfSheets();
        for (int i = 0; i < numberOfSheets; i++) {
            Sheet sheet = workbook.getSheetAt(i);
            if (sheet.getPhysicalNumberOfRows() > 0) {
                Row row = sheet.getRow(sheet.getFirstRowNum());
                Iterator<Cell> cellIterator = row.cellIterator();
                while (cellIterator.hasNext()) {
                    Cell cell = cellIterator.next();
                    int columnIndex = cell.getColumnIndex();
                    sheet.autoSizeColumn(columnIndex);
                }
            }
        }
    }

    default double setImageInCell(XSSFWorkbook wb, Sheet sheet, int rowIdx, int colIdx,
                                  byte[] bytes) {
        int pictureIdx = wb.addPicture(bytes, Workbook.PICTURE_TYPE_PNG);

        //Creates the top-level drawing patriarch.
        XSSFDrawing drawing = (XSSFDrawing) sheet.createDrawingPatriarch();
        //Create an anchor that is attached to the worksheet

        XSSFClientAnchor anchor = new XSSFClientAnchor();

        //create an anchor with upper left cell _and_ bottom right cell
        anchor.setCol1(colIdx); //Column B
        anchor.setRow1(rowIdx); //Row 3
        anchor.setCol2(colIdx + 1); //Column C
        anchor.setRow2(rowIdx + 1); //Row 4


        //Creates a picture
        Picture pict = drawing.createPicture(anchor, pictureIdx);

        //Reset the image to the original size
        //pict.resize(); //don't do that. Let the anchor resize the image!
        //pict.resize(0.2); //don't do that. Let the anchor resize the image!
        double height = pict.getImageDimension().getHeight();
        return height;
    }

    default int getImageHeight(byte[] picture) {
        InputStream in = new ByteArrayInputStream(picture);
        BufferedImage buf;
        try {
            buf = ImageIO.read(in);
            int height = buf.getHeight();
            return height;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return 0;
    }

    default XSSFWorkbook createXl(List<List<Object>> data, List<List<String>> inputList, String headerStr, String language, UserDTO userDTO) {
        XSSFWorkbook wb = new XSSFWorkbook();
        Sheet sheet = wb.createSheet();
        Header header = sheet.getHeader();
        header.setCenter(headerStr);
        Row row;
        Cell cell;
        int rowNo = 0;
        if (inputList != null && inputList.size() > 0) {
            for (List<String> list : inputList) {
                rowNo++;
                row = sheet.createRow(rowNo);
                cell = row.createCell(0);
                cell.setCellValue(list.get(0) + " : ");
                cell.setCellStyle(getParamStyle(wb));
                cell = row.createCell(1);
                cell.setCellValue(list.get(1));
                cell.setCellStyle(getParamValueStyle(wb));
                for (int i = 2; i < data.get(0).size(); i++) {
                    cell = row.createCell(i);
                    cell.setCellValue("");
                    cell.setCellStyle(getParamValueStyle(wb));
                }
            }
        }
        boolean titleRow = true;
        for (List<Object> rowData : data) {
            rowNo++;
            row = sheet.createRow(rowNo);
            int colNo = 0;
            for (Object cellData : rowData) {
                cell = row.createCell(colNo);
                if (titleRow) {
                    cell.setCellStyle(get1stRowStyle(wb));
                    cell.setCellValue(cellData.toString());
                } else {
                    if (cellData instanceof byte[]) {
                        short height = (short) setImageInCell(wb, sheet, rowNo, colNo, (byte[]) cellData);
                        row.setHeight((short) 1000);
                    } if (cellData instanceof Number) {
                        Number number = (Number) cellData;
                        cell.setCellValue(number.doubleValue());
                    } else if (cellData != null) {
                        cell.setCellValue(cellData.toString());
                    }
                }
                colNo++;
            }
            titleRow = false;
        }
        autoSizeColumns(wb);
        row = sheet.createRow(0);
        cell = row.createCell(0);
        cell.setCellValue(headerStr);
        cell.setCellStyle(getHeaderStyle(wb));
        sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, data.get(0).size() - 1));
        return wb;
    }
}
