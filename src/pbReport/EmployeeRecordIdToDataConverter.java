package pbReport;

/*
 * @author Md. Erfan Hossain
 * @created 13/05/2021 - 12:45 AM
 * @project parliament
 */

import employee_records.Employee_recordsDTO;
import employee_records.Employee_recordsRepository;
import pb.CatRepository;
import pb.Utils;
import sessionmanager.SessionConstants;
import util.StringUtils;

public class EmployeeRecordIdToDataConverter extends ColumnConvertor {
    private final String column;

    public EmployeeRecordIdToDataConverter(String column) {
        this.column = column;
    }

    @Override
    public Object convert(Object columnValue, String language, String tableName) {
        if (column != null && columnValue != null) {
            long id = Long.parseLong(columnValue.toString());
            Employee_recordsDTO employeeRecordsDTO = Employee_recordsRepository.getInstance().getById(id);
            if (employeeRecordsDTO == null) {
                return "";
            }
            switch (column) {
                case "employee_records_id_class":
                    return CatRepository.getInstance().getText(language, "employee_class", employeeRecordsDTO.employeeClass);
                case "complete_age_employee":
                    if (employeeRecordsDTO.dateOfBirth == SessionConstants.MIN_DATE) {
                        return "";
                    }
                    return Utils.calculateCompleteAge(employeeRecordsDTO.dateOfBirth, language);
                case "gender_employee":
                    return CatRepository.getInstance().getText(language, "gender", employeeRecordsDTO.gender);
                case "employee_records_id":
                    return language.equalsIgnoreCase("English") ? employeeRecordsDTO.nameEng : employeeRecordsDTO.nameBng;
                case "employee_records_id_en":
                    return employeeRecordsDTO.nameEng;
                case "employee_records_id_bn":
                    return employeeRecordsDTO.nameBng;
                case "employee_records_id_details": {
                    return language.equalsIgnoreCase("English") ? employeeRecordsDTO.nameEng+"<br>"+employeeRecordsDTO.employeeNumber :
                            employeeRecordsDTO.nameBng+"<br>"+StringUtils.convertToBanNumber(employeeRecordsDTO.employeeNumber);
                }
            }
        }
        return "";
    }
}
