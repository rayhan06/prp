package pbReport;

import am_office_assignment.Am_office_assignmentRepository;


public class IdToOfficeConverter extends ColumnConvertor{

    @Override
    public Object convert(Object columnValue, String language, String tableName) {

        if (columnValue == null) return "";
        if(!columnValue.toString().contains(",")) return "";

        String[] values = (columnValue.toString().split(","));
        long officeType  = Long.parseLong(values[0]);
        long officeUnit  = Long.parseLong(values[1]);
        return Am_office_assignmentRepository.getInstance().getText(officeType,officeUnit, language);
    }
}
