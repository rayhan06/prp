package pbReport;

import pb.CatRepository;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class VehicleTypeCatMultipleConverter extends ColumnConvertor{
    private String getTypeName(Integer id, String language){
        return CatRepository.getName(language, "vehicle_type", id);
    }
    @Override
    public Object convert(Object columnValue, String language, String tableName) {
        if(columnValue == null) return "";
        List<Integer> vehicleTypeIds = Arrays.stream(columnValue.toString().split(","))
                .map(Integer::parseInt).collect(Collectors.toList());
        String vehicleTypeNames = vehicleTypeIds.stream()
                .map(id -> this.getTypeName(id, language))
                .collect(Collectors.joining(", "));
        return vehicleTypeNames;
    }
}
