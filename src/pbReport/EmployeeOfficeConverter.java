package pbReport;

import employee_offices.EmployeeOfficeDTO;
import employee_offices.EmployeeOfficeRepository;
import office_unit_organograms.OfficeUnitOrganogramsRepository;

public class EmployeeOfficeConverter extends ColumnConvertor {
    public String convert(Object value, String language, String tableName) {
        if (value == null) {
            return "";
        }
        EmployeeOfficeDTO employeeOfficeDTO = EmployeeOfficeRepository.getInstance().getById(Long.parseLong(value.toString()));
        if (employeeOfficeDTO == null) {
            return "";
        }
        return language.equalsIgnoreCase("English") ?
                (employeeOfficeDTO.unitNameEn != null ? employeeOfficeDTO.unitNameEn + "<br>" : "") + (employeeOfficeDTO.designationEn != null ? employeeOfficeDTO.designationEn : "")
                : (employeeOfficeDTO.unitNameBn != null ? employeeOfficeDTO.unitNameBn + "<br>" : "") + (employeeOfficeDTO.designationBn != null ? employeeOfficeDTO.designationBn : "");
    }
}
