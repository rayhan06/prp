package pbReport;


public class BasicConverter extends ColumnConvertor {

	@Override
	public Object convert(Object columnValue, String language, String tableName) {
		if(columnValue == null){
			return "";
		}
		return columnValue.toString();
	}

}
