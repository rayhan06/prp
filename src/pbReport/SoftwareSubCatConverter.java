package pbReport;

import software_type.Software_typeDAO;

public class SoftwareSubCatConverter extends ColumnConvertor {
	Software_typeDAO software_typeDAO = Software_typeDAO.getInstance();

	@Override
	public Object convert(Object value, String language, String tableName) {
		// TODO Auto-generated method stub
		if(value == null)
		{
			return "";
		}
		String cat_subcat = value.toString();
		if(cat_subcat.equalsIgnoreCase(""))
		{
			return "";
		}
		else
		{
			String [] splitted = cat_subcat.split("_");
			return software_typeDAO.getName(Integer.parseInt(splitted[0]), Integer.parseInt(splitted[1]));
			
		}
	}

}
