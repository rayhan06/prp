package pbReport;
import pb.CatRepository;
import religion.ReligionRepository;

public class ReligionConverter extends ColumnConvertor{
    @Override
    public Object convert(Object value, String language, String tableName) {
        if (value == null) {
            return "";
        }
        int val=Integer.parseInt(value.toString());
        return ReligionRepository.getInstance().getText(language,val);
    }
}
