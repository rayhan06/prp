package pbReport;

import geolocation.GeoDistrictRepository;

public class DistrictColumnConverter extends ColumnConvertor {
    @Override
    public Object convert(Object columnValue, String language, String tableName) {
        if (columnValue == null) {
            return "";
        }
        return GeoDistrictRepository.getInstance().getText(language, Long.parseLong(columnValue.toString()));
    }
}
