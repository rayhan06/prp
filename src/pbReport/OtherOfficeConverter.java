package pbReport;

import java.math.BigInteger;

import org.apache.commons.lang3.StringUtils;

import pb.CommonDAO;

public class OtherOfficeConverter extends ColumnConvertor {

	public static String tableName = "other_office";
    public static String idColumn = "id";

    public String convert(Object value, String language, String tableName) {
    	if(value == null)
    	{
    		return "N/A";
    	}
        if(value instanceof String){
        	if(StringUtils.isNumeric((String) value))
        	{
        		value = Long.parseLong((String) value);
        	}
        	else
        	{
        		return "N/A";
        	}
            
        }

		if(value instanceof Integer || value instanceof BigInteger){
			value = Long.parseLong(String.valueOf(value));
		}

        String name = CommonDAO.getName(language, tableName, (long) value);
        if(name.equalsIgnoreCase(""))
        {
        	name = "N/A";
        }
        return name;
    }
}
