package pbReport;

/*
 * @author Md. Erfan Hossain
 * @created 20/04/2021 - 8:28 PM
 * @project parliament
 */

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public final class PBReportUtils {
    public static Set<String> prepareInputSets(Set<String> searchParam, HttpServletRequest request) {
        Set<String> inputs = new HashSet<>();
        Enumeration<String> parameterNames = request.getParameterNames();

        while (parameterNames.hasMoreElements()) {
            String paramName = parameterNames.nextElement();
            if (searchParam.contains(paramName)) {
                String val = request.getParameter(paramName);
                if (val != null && val.trim().length() > 0) {
                    inputs.add(paramName);
                }
            }
        }
        return inputs;
    }

    public static String[][] prepareCriteria(Set<String> inputs, Map<String, String[]> stringMap) {
        String[][] criteria = new String[inputs.size()][];
        int i = 0;
        for (String item : inputs) {
            String[] oldArr = stringMap.get(item);
            String[] newArr = new String[oldArr.length];
            System.arraycopy(oldArr,0,newArr,0,oldArr.length);
            criteria[i] = newArr;
            if(i == 0){
                criteria[i][4] = "";
            }
            i++;
        }
        return criteria;
    }
}
