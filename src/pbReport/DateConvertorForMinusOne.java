package pbReport;

import pb.Utils;
import sessionmanager.SessionConstants;
import util.TimeConverter;

public class DateConvertorForMinusOne extends ColumnConvertor{

	public String convert(Object timeInMills, String language, String tableName) {
		if(timeInMills == null){
			return "";
		}
		long ms = Long.parseLong(timeInMills.toString());
		if( ms == SessionConstants.MIN_DATE || ms == -1){
			return "";
		}
		String sDate = TimeConverter.getTimeStringFromLong(ms);
		sDate= Utils.getDigits(sDate, language);
		return sDate;
	}
}