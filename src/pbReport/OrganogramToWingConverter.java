package pbReport;

import workflow.WorkflowController;

public class OrganogramToWingConverter extends ColumnConvertor {

	@Override
	public Object convert(Object value, String language, String tableName) {
		Long organogramId = -1L;
		if (value instanceof Long)
		{
			organogramId = (Long)value;
		}
		else
		{
			organogramId = Long.parseLong(value.toString());
		}
		// TODO Auto-generated method stub
		return WorkflowController.getWingNameFromOrganogramId(organogramId, language);
	}

}
