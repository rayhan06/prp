package pbReport;

import employee_offices.EmployeeOfficeDTO;
import employee_offices.EmployeeOfficeRepository;

public class DesignationConverter extends ColumnConvertor {
    @Override
    public Object convert(Object columnValue, String language, String tableName) {
        if (columnValue == null) {
            return "";
        }
        long employeeOfficeId = Long.parseLong(columnValue.toString());
        EmployeeOfficeDTO dto = EmployeeOfficeRepository.getInstance().getById(employeeOfficeId);
        if (dto != null) {
            String designationName =  language.equalsIgnoreCase("ENGLISH") ? dto.designationEn : dto.designationBn;
            return designationName != null ? designationName : "";
        }
        return "";
    }
}
