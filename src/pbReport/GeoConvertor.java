package pbReport;

import geolocation.GeoLocationDAO2;

public class GeoConvertor extends ColumnConvertor{

	@Override
	public String convert(Object address, String language, String tableName) {
		if(address == null){
			return "";
		}
		return GeoLocationDAO2.parseTextAndDetails(address.toString());
	}

}
