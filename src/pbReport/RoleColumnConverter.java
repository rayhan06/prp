package pbReport;

import role.PermissionRepository;
import role.RoleDTO;

public class RoleColumnConverter extends ColumnConvertor {
    @Override
    public Object convert(Object columnValue, String language, String tableName) {
        if (columnValue == null) {
            return "";
        }
        RoleDTO roleDTO = PermissionRepository.getRoleDTOByRoleID(Long.parseLong(columnValue.toString()));
        return roleDTO != null ? roleDTO.roleName : "";
    }
}
