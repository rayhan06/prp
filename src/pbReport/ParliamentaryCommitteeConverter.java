package pbReport;

import committees.CommitteesRepository;

public class ParliamentaryCommitteeConverter extends ColumnConvertor {
    @Override
    public Object convert(Object columnValue, String language, String tableName) {
        if (columnValue == null) {
            return "";
        }
        long committeeId = Long.parseLong(columnValue.toString());
        return CommitteesRepository.getInstance().getText(language, committeeId);
    }
}
