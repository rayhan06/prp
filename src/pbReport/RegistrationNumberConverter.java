package pbReport;

import election_constituency.Election_constituencyRepository;
import vm_vehicle.Vm_vehicleRepository;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class RegistrationNumberConverter extends ColumnConvertor{
    private String getRegNum(Long id, String language){
        return Vm_vehicleRepository.getInstance().getText(id, language);
    }
    @Override
    public Object convert(Object columnValue, String language, String tableName) {
        if (columnValue == null) return "";

        List<Long> vehicleIds = Arrays.stream(columnValue.toString().split(","))
                .map(Long::parseLong).collect(Collectors.toList());
        String vehicleRegNums = vehicleIds.stream()
                .map(id -> this.getRegNum(id, language))
                .collect(Collectors.joining(", "));
        return vehicleRegNums;
    }
}
