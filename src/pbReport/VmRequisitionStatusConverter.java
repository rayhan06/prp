package pbReport;

import pb.CatRepository;
import pb.CategoryLanguageModel;
import vm_fuel_vendor.VmFuelVendorItemDAO;
import vm_vehicle.Vm_vehicleRepository;

import java.util.List;

public class VmRequisitionStatusConverter extends ColumnConvertor{

    @Override
    public Object convert(Object columnValue, String language, String tableName) {
        if (columnValue == null) return "";

        long id = Long.parseLong(columnValue.toString());
        return CatRepository.getInstance().getText(language,"vm_requisition_status",id);

    }
}
