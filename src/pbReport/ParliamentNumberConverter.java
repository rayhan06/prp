package pbReport;

import election_details.Election_detailsRepository;

public class ParliamentNumberConverter extends ColumnConvertor {
    @Override
    public Object convert(Object columnValue, String language, String tableName) {
        if (columnValue == null) return "";

        long id = Long.parseLong(columnValue.toString());
        return Election_detailsRepository.getInstance().getText(id, language);
    }
}
