package pbReport;

import common.ConnectionAndStatementUtil;
import pb.Utils;

import org.apache.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings({"unchecked", "Duplicates"})
public class ReportProcessor {

    private static final Logger logger = Logger.getLogger(ReportProcessor.class);

    public static int getTotalResultCount(ReportMetadata reportMetadata, String tableName) throws Exception {
        SqlPair sqlPair = createReportCountSQL(reportMetadata, tableName);
        return executeCountSql(sqlPair);
    	//return 1;
    }

    public static int executeCountSql(SqlPair sqlPair) {
        return (Integer) ConnectionAndStatementUtil.getReadPrepareStatement(ps -> {
            try {
                set(ps, sqlPair);
                logger.debug(ps);
                ResultSet rs = ps.executeQuery();
                if(rs.next())
                {
                	return rs.getInt(1);
                }
                else
                {
                	return 0;
                }
                
            } catch (SQLException ex) {
                ex.printStackTrace();
                return 0;
            }
        }, sqlPair.sql);
    }

    public static List<List<Object>> getResult(ReportMetadata reportMetadata, String tableName, int limit, int offset) throws Exception {
        SqlPair sqlPair = createReportSQL(reportMetadata, tableName);
        if (limit != -1 && offset != -1) {
            sqlPair.sql += (" limit " + limit + " offset " + offset);
        }

        return (List<List<Object>>) ConnectionAndStatementUtil.getReadPrepareStatement(ps -> {
            List<List<Object>> result = new ArrayList<>();
            try {
                set(ps, sqlPair);
                logger.debug(ps);
                ResultSet rs = ps.executeQuery();
                ResultSetMetaData resultSetMetaData = rs.getMetaData();
                result.add(reportMetadata.captionList);
                while (rs.next()) {
                    result.add(buildObjectFromResultSet(rs, reportMetadata.getOutputColumnNames(), resultSetMetaData));
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
            return result;
        }, sqlPair.sql);
    }

    private static void set(PreparedStatement ps, SqlPair sqlPair) throws SQLException {
        for (int i = 0; i < sqlPair.objectList.size(); i++) {
            logger.debug("found datatype = " + sqlPair.dataTypes.get(i) + " value = " + sqlPair.objectList.get(i));
            if (!sqlPair.dataTypes.get(i).equals("constant")) {
                switch (sqlPair.dataTypes.get(i)) {
                    case "String":
                        ps.setString(i + 1, (String) sqlPair.objectList.get(i));
                        break;
                    case "long":
                        ps.setLong(i + 1, Long.parseLong((String) sqlPair.objectList.get(i)));
                        break;
                    case "int":
                        ps.setInt(i + 1, Integer.parseInt((String) sqlPair.objectList.get(i)));
                        break;
                    case "double":
                        ps.setDouble(i + 1, Double.parseDouble((String) sqlPair.objectList.get(i)));
                        break;
                    case "boolean":
                        ps.setBoolean(i + 1, Boolean.parseBoolean((String) sqlPair.objectList.get(i)));
                        break;
                    default:
                        ps.setObject(i + 1, sqlPair.objectList.get(i));
                        break;
                }
            }
        }
    }

    public static SqlPair createReportSQL(ReportMetadata reportMetadata
            , String tableName) throws Exception {

        StringBuilder sqlBuilder = new StringBuilder();
        sqlBuilder.append("SELECT ");
        if (reportMetadata.getOutputColumnNames().isEmpty()) {
            sqlBuilder.append(" * ");
        } else {
            for (int i = 0; i < reportMetadata.getOutputColumnNames().size(); i++) {
                if (i != 0) {
                    sqlBuilder.append(",");
                }
                DisplayColumn outputColumn = reportMetadata.getOutputColumnNames().get(i);
                sqlBuilder.append(outputColumn.databaseColumnName)
                        .append(" '")
                        .append("col_")
                        .append(i)
                        .append("' ");
            }
        }

        List<Object> values = new ArrayList<>();
        List<String> dataTypes = new ArrayList<>();
        sqlBuilder.append(" FROM ")
                .append(tableName)
                .append(" ");
        for (int i = 0; i < reportMetadata.getInputColumnNames().size(); i++) {
            CriteriaColumn inputColumn = reportMetadata.getInputColumnNames().get(i);
            if (i == 0) {
                sqlBuilder.append(" WHERE ");
            }

            sqlBuilder.append(" ").append(inputColumn.delimeter).append(" ");
            sqlBuilder.append(" ").append(inputColumn.ParenthesisLeft).append(" ");

            if (inputColumn.operator.equalsIgnoreCase("null")) {
                logger.debug("null operator");
                sqlBuilder.append(" ")
                        .append(inputColumn.databaseColumnName)
                        .append(" IS ");
                if (inputColumn.criteriaValue.trim().equals("1")) {
                    sqlBuilder.append(" NOT ");
                }
                sqlBuilder.append(" NULL ");
            } else {
                sqlBuilder.append(" ")
                        .append(inputColumn.databaseColumnName)
                        .append(" ")
                        .append(inputColumn.operator)
                        .append(" ");

                if (inputColumn.operator.equalsIgnoreCase("in")) {
                    sqlBuilder.append("(").append(inputColumn.criteriaValue).append(")");
                } else if (inputColumn.dataType.equalsIgnoreCase("constant")) {
                    sqlBuilder.append(inputColumn.criteriaValue);
                } else {
                    sqlBuilder.append(" ? ");

                    logger.debug("adding " + inputColumn.criteriaValue + " as " + inputColumn.dataType);
                    values.add(inputColumn.criteriaValue);
                    dataTypes.add(inputColumn.dataType);
                }
            }
            sqlBuilder.append(" ").append(inputColumn.ParenthesisRight).append(" ");
        }

        if (reportMetadata.GroupBy.matches(".*[a-zA-Z]+.*")) {
        	if(!reportMetadata.GroupBy.trim().startsWith("having"))
        	{
        		sqlBuilder.append(" group by ");
                sqlBuilder.append(reportMetadata.GroupBy);
        	}
        	else
        	{
        		sqlBuilder.append(reportMetadata.GroupBy);
        	}
            
        }

        if (reportMetadata.OrderBy.matches(".*[a-zA-Z]+.*")) {
            sqlBuilder.append(" order by ");
            sqlBuilder.append(reportMetadata.OrderBy);
        }

        SqlPair sqlPair = new SqlPair();
        sqlPair.sql = sqlBuilder.toString();
        sqlPair.objectList = values;
        sqlPair.dataTypes = dataTypes;

        logger.debug(sqlPair);
        return sqlPair;
    }

    public static SqlPair createReportCountSQL(ReportMetadata reportMetadata, String tableName) throws Exception {

        StringBuilder sqlBuilder = new StringBuilder();
        sqlBuilder.append("SELECT COUNT(*) ");
        return createCommonReportCountSQL(reportMetadata, tableName, sqlBuilder);
    }

    public static SqlPair createCommonReportCountSQL(ReportMetadata reportMetadata, String tableName, StringBuilder sqlBuilder) throws Exception {
        List<Object> values = new ArrayList<>();
        List<String> dataTypes = new ArrayList<>();
        sqlBuilder.append(" FROM ")
                .append(tableName)
                .append(" ");
        for (int i = 0; i < reportMetadata.getInputColumnNames().size(); i++) {
            CriteriaColumn inputColumn = reportMetadata.getInputColumnNames().get(i);
            if (i == 0) {
                sqlBuilder.append(" WHERE ");
            }

            sqlBuilder.append(" ").append(inputColumn.delimeter).append(" ");
            sqlBuilder.append(" ").append(inputColumn.ParenthesisLeft).append(" ");

            if (inputColumn.operator.equalsIgnoreCase("null")) {
                logger.debug("null operator");
                sqlBuilder.append(" ")
                        .append(inputColumn.databaseColumnName)
                        .append(" IS ");
                if (inputColumn.criteriaValue.trim().equals("1")) {
                    sqlBuilder.append(" NOT ");
                }
                sqlBuilder.append(" NULL ");
            } else {
                sqlBuilder.append(" ")
                        .append(inputColumn.databaseColumnName)
                        .append(" ")
                        .append(inputColumn.operator)
                        .append(" ");

                if (inputColumn.operator.equalsIgnoreCase("in")) {
                    sqlBuilder.append("(").append(inputColumn.criteriaValue).append(")");
                } else if (inputColumn.dataType.equalsIgnoreCase("constant")) {
                    sqlBuilder.append(inputColumn.criteriaValue);
                } else {
                    sqlBuilder.append(" ? ");

                    logger.debug("adding " + inputColumn.criteriaValue + " as " + inputColumn.dataType);
                    values.add(inputColumn.criteriaValue);
                    dataTypes.add(inputColumn.dataType);
                }
            }
            sqlBuilder.append(" ").append(inputColumn.ParenthesisRight).append(" ");
        }

        /*if (reportMetadata.GroupBy.matches(".*[a-zA-Z]+.*")) {
        	if(!reportMetadata.GroupBy.trim().startsWith("having"))
        	{
        		sqlBuilder.append(" group by ");
                sqlBuilder.append(reportMetadata.GroupBy);
        	}
        	else
        	{
        		sqlBuilder.append(reportMetadata.GroupBy);
        	}
            
        }*/

        if (reportMetadata.OrderBy.matches(".*[a-zA-Z]+.*")) {
            sqlBuilder.append(" order by ");
            sqlBuilder.append(reportMetadata.OrderBy);
        }

        SqlPair sqlPair = new SqlPair();
        sqlPair.sql = sqlBuilder.toString();
        sqlPair.objectList = values;
        sqlPair.dataTypes = dataTypes;

        logger.debug(sqlPair);
        return sqlPair;
    }

    private static List<Object> buildObjectFromResultSet(ResultSet rs, List<DisplayColumn> outputColumns, ResultSetMetaData resultSetMetaData) {
        try {
            List<Object> row = new ArrayList<>();
            for (int i = 0; i < resultSetMetaData.getColumnCount(); i++) {
            	DisplayColumn outputColumn = outputColumns.get(i);
                Object object;
                if(outputColumn.converterType.equalsIgnoreCase("file"))
                {
                	object = Utils.getByteArrayFromInputStream(rs.getBinaryStream(i + 1)); //object contains a byte array now
                }
                else
                {
                	object = rs.getObject(i + 1);
                }
                
                
                object = outputColumn.convertor.convert(object, outputColumn.language, outputColumn.tableName);
                row.add(object);
            }
            return row;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }
}