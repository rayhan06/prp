package pbReport;

import pb.Utils;
import user.UserDTO;
import user.UserRepository;

public class UserNameConverter extends ColumnConvertor {

    @Override
    public Object convert(Object columnValue, String language, String tableName) {
        if (columnValue == null) {
            return "";
        }
        long val = Long.parseLong(columnValue.toString());
        UserDTO userDTO = UserRepository.getInstance().getUserDtoByEmployeeRecordId(val);

        String userName = userDTO != null ? userDTO.userName : "";
        return Utils.getDigits(userName, language);
    }
}
