package pbReport;

import pb.CatRepository;

public class GradeConverter extends ColumnConvertor {
    @Override
    public Object convert(Object columnValue, String language, String tableName) {
        if (columnValue == null) {
            return "";
        }
        if (columnValue.toString().equalsIgnoreCase("0")) {
            return language.equalsIgnoreCase("English") ? "Not within grade" : "গ্রেডভুক্ত নয়";
        }
        return CatRepository.getInstance().getText(language,"job_grade",Integer.parseInt(columnValue.toString()));
    }
}
