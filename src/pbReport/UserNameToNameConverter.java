package pbReport;

import employee_records.Employee_recordsRepository;
import pb.Utils;
import workflow.WorkflowController;

public class UserNameToNameConverter extends ColumnConvertor {

	@Override
	public Object convert(Object columnValue, String language, String tableName) {
		// TODO Auto-generated method stub
		String userName = (String)columnValue;
		String name = Utils.getDigits(userName, language) + ", " +
				WorkflowController.getNameFromUserName(userName, language);
		/*String org = WorkflowController.getOrganogramName(userName, language);
		if(!org.equalsIgnoreCase(""))
		{
			name += ", " + org;
		}*/
		if(name.equalsIgnoreCase("") || name.equalsIgnoreCase(", "))
		{
			if(language.equalsIgnoreCase("english"))
			{
				return "N/A";
			}
			else
			{
				return "প্রযোজ্য নয়";
			}
		}
		return name;
	}

}
