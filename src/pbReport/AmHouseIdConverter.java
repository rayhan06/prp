package pbReport;

import am_house.Am_houseRepository;
import vm_fuel_vendor.Vm_fuel_vendorRepository;

public class AmHouseIdConverter extends ColumnConvertor{

    @Override
    public Object convert(Object columnValue, String language, String tableName) {
        if (columnValue == null) return "";

        long id = Long.parseLong(columnValue.toString());
        return Am_houseRepository.getInstance().getText(id, language);
    }
}
