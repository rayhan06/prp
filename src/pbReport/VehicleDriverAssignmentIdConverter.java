package pbReport;

import vm_vehicle.Vm_vehicleRepository;
import vm_vehicle_driver_assignment.Vm_vehicle_driver_assignmentRepository;

public class VehicleDriverAssignmentIdConverter extends ColumnConvertor{

    @Override
    public Object convert(Object columnValue, String language, String tableName) {
        if (columnValue == null) return "";

        long id = Long.parseLong(columnValue.toString());
        return Vm_vehicle_driver_assignmentRepository.getInstance().getText(id, language);
    }
}
