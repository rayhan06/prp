package pbReport;

import ticket_forward_history.Ticket_forward_historyDTO;

public class TicketActionConverter extends ColumnConvertor {

	@Override
	public Object convert(Object value, String language, String tableName) {
		// TODO Auto-generated method stub
		if(value == null)
		{
			return "";
		}
		if(value.toString().equals(Ticket_forward_historyDTO.FORWARD + ""))
		{
			if(language.equalsIgnoreCase("bangla"))
			{
				return "সমাধান করা";
			}
			else
			{
				return "Solve";
			}
		}
		else if(value.toString().equals(Ticket_forward_historyDTO.CLOSE + ""))
		{
			if(language.equalsIgnoreCase("bangla"))
			{
				return "নিষ্পন্ন করা";
			}
			else
			{
				return "Close";
			}
		}
		return "";
	}

}
