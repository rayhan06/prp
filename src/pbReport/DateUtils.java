package pbReport;

import budget.BudgetUtils;
import pb.Utils;
import sessionmanager.SessionConstants;
import util.StringUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

@SuppressWarnings({"Duplicates", "UnnecessaryLocalVariable", "unused"})
public class DateUtils {

    public static String[] monthEn = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
    public static String[] monthBn = {"জানুয়ারী", "ফেরুয়ারী", "মার্চ", "এপ্রিল", "মে", "জুন", "জুলাই", "আগস্ট", "সেপ্টেম্বর", "অক্টোবর", "নভেম্বর", "ডিসেম্বর"};

    public static String getMonthName(int i, String language) {
        if (i >= monthEn.length) {
            return "";
        }
        if (language.equalsIgnoreCase("english")) {
            return monthEn[i];
        } else {
            return monthBn[i];
        }
    }

    public static long get1stDayOfMonth() {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        c.set(Calendar.DAY_OF_MONTH, 1);
        long l1stDayOfMonth = c.getTimeInMillis() - 6 * 60 * 60 * 1000;

        return l1stDayOfMonth;
    }

    public static long getToday12AM() {
        Calendar c = Calendar.getInstance();
        c.setTimeZone(TimeZone.getTimeZone("GMT"));
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        System.out.println("Todays time is " + c.getTime() + " in millis  " + c.getTimeInMillis());
        long lToday12AM = c.getTimeInMillis();

        return lToday12AM;
    }

    public static long getYesterday12AM() {

        long lYesterday12AM = getToday12AM() - (24 * 60 * 60 * 1000);

        return lYesterday12AM;
    }

    public static long get1stDayOfJuly() {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        c.set(Calendar.DAY_OF_MONTH, 1);

        if (c.get(Calendar.MONTH) >= 6) {
            c.set(Calendar.MONTH, 6);
        } else {
            c.set(Calendar.YEAR, c.get(Calendar.YEAR) - 1);
            c.set(Calendar.MONTH, 6);
        }

        long l1stDayOfJuly = c.getTimeInMillis() - 6 * 60 * 60 * 1000;


        return l1stDayOfJuly;
    }

    public static long addMonth(long millis, int count) {
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(millis);
        c.set(Calendar.MONTH, c.get(Calendar.MONTH) + count);

        long time = c.getTimeInMillis();
        return time;
    }

    public static long get1stDayOfNextMonth() {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        c.set(Calendar.DAY_OF_MONTH, 1);

        if (c.get(Calendar.MONTH) >= 11) {
            c.set(Calendar.MONTH, 1);
            c.set(Calendar.YEAR, c.get(Calendar.YEAR) + 1);
        } else {
            c.set(Calendar.MONTH, c.get(Calendar.MONTH) + 1);
        }

        long l1stDayOfNextMonth = c.getTimeInMillis() - 6 * 60 * 60 * 1000;

        return l1stDayOfNextMonth;
    }

    public static long stringToDate(String string_date) {
        long milliseconds = 0;
        SimpleDateFormat f = new SimpleDateFormat("dd-MMM-yyyy");
        try {
            Date d = f.parse(string_date);
            milliseconds = d.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return milliseconds;
    }

    // fromCalendarMonth <= timeMillis <= toCalendarMonth
    // example monthIsBetween(timeMillis, Calendar.JANUARY, Calendar.JUNE)
    public static boolean isMonthBetween(long timeMillis, int fromCalendarMonth, int toCalendarMonth) {
        int timeMillisMonth = getMonth(timeMillis);
        return isMonthBetween(timeMillisMonth, fromCalendarMonth, toCalendarMonth);
    }

    public static boolean isMonthBetween(int calendarMonth, int fromCalendarMonth, int toCalendarMonth) {
        return fromCalendarMonth <= calendarMonth
               && calendarMonth <= toCalendarMonth;
    }

    public static int getYear(long timeMillis) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timeMillis);
        return calendar.get(Calendar.YEAR);
    }

    public static int getMonth(long timeMillis) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timeMillis);
        return calendar.get(Calendar.MONTH);
    }

    public static int getDayOfTheMonth(long timeMillis) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timeMillis);
        return calendar.get(Calendar.DAY_OF_MONTH);
    }

    public static String getMonthYear(long monthYear, String language) {
        return getMonthYear(monthYear, language, ",");
    }

    public static String getMonthYear(long monthYear, String language, String separator) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(monthYear);
        int month = calendar.get(Calendar.MONTH);
        int year = calendar.get(Calendar.YEAR);

        String output = "english".equalsIgnoreCase(language) ? monthEn[month] : monthBn[month];
        return output.concat(separator).concat(StringUtils.convertBanglaIfLanguageIsBangla(language, String.valueOf(year)));
    }

    public static String getCurrentMonthYear(String language, String separator) {
        Calendar calendar = Calendar.getInstance();
        int month = calendar.get(Calendar.MONTH);
        int year = calendar.get(Calendar.YEAR);
        String output = "english".equalsIgnoreCase(language) ? monthEn[month] : monthBn[month];
        return output.concat(separator).concat(StringUtils.convertBanglaIfLanguageIsBangla(language, String.valueOf(year)));
    }

    public static long get1stDayOfNextMonth(long timeMillis) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timeMillis);

        int month = calendar.get(Calendar.MONTH);
        month = (month + 1) % 12;

        int year = calendar.get(Calendar.YEAR);
        if (month == 0) year++;

        calendar.set(Calendar.MILLISECOND, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.YEAR, year);
        return calendar.getTimeInMillis();
    }

    public static String getDateInWord(String language, long date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(date);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int month = calendar.get(Calendar.MONTH);
        int year = calendar.get(Calendar.YEAR);

        String formattedDate = day + " " + ("english".equalsIgnoreCase(language) ? monthEn[month] : monthBn[month])
                               + ", " + year;
        return StringUtils.convertBanglaIfLanguageIsBangla(language, formattedDate);
    }

    public static String getMonthNameFromIndex(int monthIndex, boolean isLangEn) {
        return isLangEn ? monthEn[monthIndex] : monthBn[monthIndex];
    }

    public static String getMonthName(long timeInMills, boolean isLangEn) {
        return getMonthNameFromIndex(getMonth(timeInMills), isLangEn);
    }

    public static String getMonthName(long timeInMills, String language) {
        return getMonthName(timeInMills, language.equalsIgnoreCase("English"));
    }

    public static long getMonthYearTimestamp(Integer month, Integer year) {
        try {
            String dateStr = String.format("%02d/%02d/%04d", 1, month, year);
            return new SimpleDateFormat("dd/MM/yyyy").parse(dateStr).getTime();
        } catch (Exception ex) {
            return SessionConstants.MIN_DATE;
        }
    }

    public static boolean isLeapYear(int year) {
        return (year % 4 == 0 && year % 100 != 0) || year % 400 == 0;
    }

    public static long getLastTimeStampOfMonth(int month, int year) {
        if (month == 12) {
            month = 1;
            ++year;
        } else {
            ++month;
        }
        long monthYearTimestamp = getMonthYearTimestamp(month, year);
        return monthYearTimestamp == SessionConstants.MIN_DATE
               ? SessionConstants.MIN_DATE : monthYearTimestamp - 1;
    }

    public static String getYearName(long timeInMills, String language) {
        return Utils.getDigits(getYear(timeInMills), language);
    }

    public static long get1stDayOfYear(long time) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.MILLISECOND, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        calendar.set(Calendar.YEAR, getYear(time));
        calendar.set(Calendar.MONTH, 0);
        return calendar.getTimeInMillis();
    }

    public static long get1stDayOfNextYear(long time) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.MILLISECOND, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        calendar.set(Calendar.YEAR, getYear(time) + 1);
        calendar.set(Calendar.MONTH, 0);
        return calendar.getTimeInMillis();
    }

    public static long get1stDayOfMonth(long timeMillis) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timeMillis);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTimeInMillis();
    }

    public static final long ONE_DAY_IN_MILLIS = (24L * 60) * 60 * 1000;

    public static long get12amTime(long timeStampInMillis) {
        long days = timeStampInMillis / ONE_DAY_IN_MILLIS;
        return ONE_DAY_IN_MILLIS * days;
    }

    public static int getDayOfWeek(long timeMillis) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timeMillis);
        return calendar.get(Calendar.DAY_OF_WEEK);
    }

    public static String getDateRangeString(long timeFrom, long timeTo, boolean isLangEn) {
        Calendar calFrom = Calendar.getInstance();
        calFrom.setTimeInMillis(timeFrom);

        Calendar calTo = Calendar.getInstance();
        calTo.setTimeInMillis(timeTo);

        String dateRange;
        if (calFrom.get(Calendar.YEAR) == calTo.get(Calendar.YEAR)) {
            if (calFrom.get(Calendar.MONTH) == calTo.get(Calendar.MONTH)) {
                dateRange = String.format(
                        "%02d-%02d %s,%d",
                        calFrom.get(Calendar.DAY_OF_MONTH),
                        calTo.get(Calendar.DAY_OF_MONTH),
                        getMonthNameFromIndex(calTo.get(Calendar.MONTH), isLangEn),
                        calTo.get(Calendar.YEAR)
                );
            } else {
                dateRange = String.format(
                        "%02d %s-%02d %s,%d",
                        calFrom.get(Calendar.DAY_OF_MONTH),
                        getMonthNameFromIndex(calFrom.get(Calendar.MONTH), isLangEn),
                        calTo.get(Calendar.DAY_OF_MONTH),
                        getMonthNameFromIndex(calTo.get(Calendar.MONTH), isLangEn),
                        calTo.get(Calendar.YEAR)
                );
            }
        } else {
            dateRange = String.format(
                    "%02d %s,%d - %02d %s,%d",
                    calFrom.get(Calendar.DAY_OF_MONTH),
                    getMonthNameFromIndex(calFrom.get(Calendar.MONTH), isLangEn),
                    calFrom.get(Calendar.YEAR),
                    calTo.get(Calendar.DAY_OF_MONTH),
                    getMonthNameFromIndex(calTo.get(Calendar.MONTH), isLangEn),
                    calTo.get(Calendar.YEAR)
            );
        }
        return isLangEn ? dateRange : StringUtils.convertToBanNumber(dateRange);
    }

    public static void main(String[] args) {
        long timeFrom = BudgetUtils.parseMonthYearString("2/2/2021");
        long timeTo = BudgetUtils.parseMonthYearString("25/2/2021");

        System.out.println(getDateRangeString(timeFrom, timeTo, true));
        System.out.println(getDateRangeString(timeFrom, timeTo, false));

        long sunday = BudgetUtils.parseMonthYearString("23/10/2022");
        System.out.println(Calendar.SUNDAY == getDayOfWeek(sunday));
    }
}
