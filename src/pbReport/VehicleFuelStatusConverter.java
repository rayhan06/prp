package pbReport;

import pb.CatRepository;
import vm_vehicle_driver_assignment.Vm_vehicle_driver_assignmentRepository;

public class VehicleFuelStatusConverter extends ColumnConvertor{

    @Override
    public Object convert(Object columnValue, String language, String tableName) {
        if (columnValue == null) return "";

        long id = Long.parseLong(columnValue.toString());
        return CatRepository.getInstance().getText(language,"vm_fuel_request_approval_status",id);
    }
}
