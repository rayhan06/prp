package pbReport;

import org.apache.commons.lang3.StringUtils;

import language.LC;
import language.LM;

public class AssignmentStatusConverter extends ColumnConvertor {

	@Override
	public Object convert(Object value, String language, String tableName) {
		if(value instanceof String){
        	if(StringUtils.isNumeric((String) value))
        	{
        		value = Integer.parseInt((String) value);
        	}
        	else
        	{
        		return value;
        	}
            
        }
		int status = (int)value;

		if(status == 1)
		{
			return LM.getText(LC.HM_ASSIGNED, language);
		}
		else
		{
			return LM.getText(LC.HM_FREE, language);
		}
	}

}
