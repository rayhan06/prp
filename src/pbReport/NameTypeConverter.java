package pbReport;

/*
 * @author Md. Erfan Hossain
 * @created 10/04/2021 - 2:23 PM
 * @project parliament
 */

import common.NameRepository;
import common.NameRepositoryMap;

public class NameTypeConverter extends ColumnConvertor{
  

    @Override
    public String convert(Object value, String language, String tableName) {
        if(value == null){
            return "";
        }
        NameRepository nameRepository =  NameRepositoryMap.getByTableName(tableName);
        return nameRepository.getText(language,Long.parseLong(value.toString()));
    }
}