package pbReport;

import employee_records.Employee_recordsDTO;
import employee_records.Employee_recordsRepository;

public class EmailConverter extends ColumnConvertor {

    @Override
    public Object convert(Object columnValue, String language, String tableName) {
        if (columnValue == null) {
            return "";
        }
        long val = Long.parseLong(columnValue.toString());
        Employee_recordsDTO employeeRecordsDTO = Employee_recordsRepository.getInstance().getById(val);
        if (employeeRecordsDTO == null) {
            return "";
        }
        return employeeRecordsDTO.personalEml;
    }
}
