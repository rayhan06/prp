package pbReport;

import java.math.BigInteger;

import workflow.WorkflowController;

public class ERIdToUserNameConverter extends ColumnConvertor {

	@Override
	public Object convert(Object columnValue, String language, String tableName) {
		boolean isLangEng = language.equalsIgnoreCase("english");
		if(columnValue== null)
		{
			if(isLangEng)
			{
				return "Employee is probably deleted" ;
			}
			else
			{
				return "কর্মকর্তার রেকর্ড পাওয়া যায় নি" ;
			}
		}
		long employeeRecordID = -1;
		if(columnValue instanceof Long)
		{
			employeeRecordID = (long)columnValue;
		}
		else if(columnValue instanceof BigInteger)
		{
			BigInteger bErId = (BigInteger)columnValue;
			employeeRecordID= bErId.longValue();
		}
		String userName = "";
		try {
			userName = WorkflowController.getUserNameFromErId(employeeRecordID, language);
			if(userName.equalsIgnoreCase(""))
			{
				if(isLangEng)
				{
					return "Username not found" ;
				}
				else
				{
					return "ইউজারনেম পাওয়া যায় নি" ;
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return userName;
	}

}
