package pbReport;

import employee_records.Employee_recordsDTO;
import employee_records.Employee_recordsRepository;
import geolocation.GeoLocationUtils;

public class RecordIdtoPresentAddressConverter extends ColumnConvertor {

    @Override
    public Object convert(Object columnValue, String language, String tableName) {
        if (columnValue == null) {
            return "";
        }
        long val = Long.parseLong(columnValue.toString());
        Employee_recordsDTO recordsDTO = Employee_recordsRepository.getInstance().getById(val);
        boolean isLanguageEnglish = language.equalsIgnoreCase("ENGLISH");
        if(recordsDTO!=null) {
            if (isLanguageEnglish)
                return GeoLocationUtils.getGeoLocationString(recordsDTO.presentAddress, language);
            else
                return GeoLocationUtils.getGeoLocationString(recordsDTO.presentAddressBng, language);
        }
        return "";
    }
}
