package pbReport;

import pb.CatRepository;


public class GivenVehicleTypeConverter extends ColumnConvertor{

    @Override
    public Object convert(Object columnValue, String language, String tableName) {
        if (columnValue == null) return "";

        long id = Long.parseLong(columnValue.toString());
        return CatRepository.getInstance().getText(language,"vehicle_type",id);
    }
}
