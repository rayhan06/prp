package pbReport;

import fiscal_year.Fiscal_yearRepository;
import vm_fuel_vendor.Vm_fuel_vendorRepository;

public class VendorIdConverter extends ColumnConvertor{

    @Override
    public Object convert(Object columnValue, String language, String tableName) {
        if (columnValue == null) return "";

        long id = Long.parseLong(columnValue.toString());
        return Vm_fuel_vendorRepository.getInstance().getText(id, language);
    }
}
