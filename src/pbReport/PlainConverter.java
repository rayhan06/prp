package pbReport;

/*
 * @author Md. Erfan Hossain
 * @created 07/09/2021 - 11:10 AM
 * @project parliament
 */

public class PlainConverter extends ColumnConvertor{

    @Override
    public Object convert(Object columnValue, String language, String tableName) {
        return columnValue == null ? "" : columnValue;
    }

}
