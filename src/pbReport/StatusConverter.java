package pbReport;

public class StatusConverter extends ColumnConvertor {

	@Override
	public Object convert(Object columnValue, String language, String tableName) {
		if (columnValue == null) {
            return "";
        }
        String value = columnValue.toString();
        if (value.equalsIgnoreCase("0") || value.equalsIgnoreCase("false")) {
            return language.equalsIgnoreCase("English") ? "Inactive" : "নিষ্ক্রিয়";
        } else {
            return language.equalsIgnoreCase("English") ? "Active" : "সক্রিয়";
        }
	}

}
