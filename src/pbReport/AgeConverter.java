package pbReport;

/*
 * @author Md. Erfan Hossain
 * @created 20/04/2021 - 4:13 PM
 * @project parliament
 */

import sessionmanager.SessionConstants;
import util.StringUtils;

import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.Date;

public class AgeConverter extends ColumnConvertor{

    @Override
    public Object convert(Object columnValue, String language, String tableName) {
        if(columnValue == null){
            return "";
        }
        long val = Long.parseLong(columnValue.toString());
        if(val == SessionConstants.MIN_DATE){
            return "";
        }
        LocalDate birthday = new Date(val).toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDate();
        Period p = Period.between(birthday, LocalDate.now());
        return "English".equalsIgnoreCase(language)?p.getYears() :StringUtils.convertToBanNumber(String.valueOf(p.getYears()));
    }
}
