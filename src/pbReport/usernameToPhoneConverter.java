package pbReport;

import employee_records.Employee_recordsRepository;
import pb.Utils;


public class usernameToPhoneConverter extends ColumnConvertor {

	@Override
	public Object convert(Object columnValue, String language, String tableName) {
		// TODO Auto-generated method stub
		if (columnValue == null || columnValue.toString().isEmpty()) {
            return "";
        }
        String userName = columnValue.toString();
        return Utils.getDigits(Employee_recordsRepository.getInstance().getPhoneByUserName(userName), language);
        
	}

}
