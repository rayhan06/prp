package pbReport;

import office_units.Office_unitsRepository;
import procurement_goods.Procurement_goodsRepository;

public class ProductIdConverter extends ColumnConvertor{
	
	public String convert(Object value, String language, String tableName) {
		try {
			return Procurement_goodsRepository.getInstance().geText(language,Long.parseLong(value.toString()));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}

}
