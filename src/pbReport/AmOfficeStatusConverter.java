package pbReport;

import pb.CatRepository;

public class AmOfficeStatusConverter extends ColumnConvertor{

    @Override
    public Object convert(Object columnValue, String language, String tableName) {
        if (columnValue == null) return "";

        long id = Long.parseLong(columnValue.toString());
        return CatRepository.getInstance().getText(language,"am_office_assignment_request_status",id);

    }
}
