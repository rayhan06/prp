package pbReport;

import org.apache.commons.codec.binary.Base64;

import util.ReportRequestHandler;

public class FileConverter extends ColumnConvertor {

    @Override
    public Object convert(Object value, String language, String tableName) {
        if (value == null) {
            return "";
        }
        if (XLState == ReportRequestHandler.XL) {
            return value;
        } else {
            byte[] file = (byte[]) value;
            String base64 = Base64.encodeBase64String(file);
            return "<img  height=\"75px\" src='data:image/jpg;base64," + base64 + "' >";
        }
    }
}
