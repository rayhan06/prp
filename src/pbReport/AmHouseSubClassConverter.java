package pbReport;

import am_house_type_sub_category.Am_house_type_sub_categoryDTO;
import am_house_type_sub_category.Am_house_type_sub_categoryRepository;

public class AmHouseSubClassConverter extends ColumnConvertor {
    @Override
    public Object convert(Object columnValue, String language, String tableName) {
        if (columnValue == null) return "";

        long id = Long.parseLong(columnValue.toString());
        Am_house_type_sub_categoryDTO dto = Am_house_type_sub_categoryRepository.getInstance().getAm_house_type_sub_categoryDTOByiD(id);
        return dto == null ? "" :
                language.equalsIgnoreCase("English") ? dto.amHouseSubCatNameEn : dto.amHouseSubCatNameBn;
    }
}