package pbReport;

import workflow.WorkflowController;

public class OrganogramToOfficeTypeConverter extends ColumnConvertor {

	@Override
	public Object convert(Object value, String language, String tableName) {
		// TODO Auto-generated method stub
		Long organogramId = -1L;
		if (value instanceof Long)
		{
			organogramId = (Long)value;
		}
		else
		{
			organogramId = Long.parseLong(value.toString());
		}
		// TODO Auto-generated method stub
		return WorkflowController.getOfficeTypeNameFromOrganogramId(organogramId, language);
	}

}
