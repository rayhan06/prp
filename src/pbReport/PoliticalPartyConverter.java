package pbReport;

import political_party.Political_partyRepository;

public class PoliticalPartyConverter extends ColumnConvertor {
    @Override
    public Object convert(Object columnValue, String language, String tableName) {
        if (columnValue == null) return "";

        long id = Long.parseLong(columnValue.toString());
        return Political_partyRepository.getInstance().getText(id, language);
    }
}
