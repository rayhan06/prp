package pbReport;

/*
 * @author Md. Erfan Hossain
 * @created 12/04/2021 - 12:50 AM
 * @project parliament
 */

import employee_office_report.InChargeLevelEnum;

public class InChargeLevelConverter extends ColumnConvertor{

    @Override
    public Object convert(Object columnValue, String language, String tableName) {
        return InChargeLevelEnum.getTextByValue(language,columnValue.toString());
    }
}
