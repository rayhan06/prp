package pbReport;

/*
 * @author Md. Erfan Hossain
 * @created 09/08/2021 - 11:08 AM
 * @project parliament
 */

import geolocation.GeoLocationRepository;

public class GeoLocationConverter extends ColumnConvertor {
    @Override
    public Object convert(Object columnValue, String language, String tableName) {
        if (columnValue == null) {
            return "";
        }
        return GeoLocationRepository.getInstance().getText(language, Integer.parseInt(columnValue.toString()));
    }
}

