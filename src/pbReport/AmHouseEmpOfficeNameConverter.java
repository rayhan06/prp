package pbReport;

import am_house_allocation_request.Am_house_allocation_requestDTO;
import am_house_allocation_request.Am_house_allocation_requestRepository;

public class AmHouseEmpOfficeNameConverter extends ColumnConvertor {
    @Override
    public Object convert(Object columnValue, String language, String tableName) {
        if (columnValue == null) return "";

        long id = Long.parseLong(columnValue.toString());
        Am_house_allocation_requestDTO dto = Am_house_allocation_requestRepository.getInstance().getAm_house_allocation_requestDTOByID(id);
        return dto == null ? "" :
                language.equalsIgnoreCase("English") ? dto.requesterOfficeUnitNameEn : dto.requesterOfficeUnitNameBn;
    }
}
