package pbReport;

/*
 * @author Md. Erfan Hossain
 * @created 10/06/2021 - 5:01 AM
 * @project parliament
 */

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class CriteriaConverterMap {
    private static final Map<String,ColumnConvertor> convertorMap = new ConcurrentHashMap<>();
    private static final DefaultConvertor defaultConvertor = new DefaultConvertor();
    static {
        convertorMap.put("date",new DateConvertor());
        convertorMap.put("geolocation",new GeoConvertor());
        convertorMap.put("office_unit",new OfficeUnitConverter());
        convertorMap.put("office_units",new OfficeUnitConverter("list"));
        convertorMap.put("office_unit_organogram",new OfficeUnitOrganogramConverter());
        convertorMap.put("organogram",new OrganogramConverter());
        convertorMap.put("cat",new CatConverter());
        convertorMap.put("type",new TypeConverter());
        convertorMap.put("name_type",new NameTypeConverter());
        convertorMap.put("text_type",new TextInterfaceConverter());
        convertorMap.put("employee_records_id",new EmployeeRecordIdToDataConverter("employee_records_id"));
        convertorMap.put("incharge_label",new InChargeLevelConverter());
        convertorMap.put("number",new NumberConverter());
        convertorMap.put("age",new AgeConverter());
        convertorMap.put("complete_age",new CompleteAgeConverter());
        convertorMap.put("category",new CategoryConverter());
        convertorMap.put("complete_age_employee",new EmployeeRecordIdToDataConverter("complete_age_employee"));
        convertorMap.put("gender_employee",new EmployeeRecordIdToDataConverter("gender_employee"));
        convertorMap.put("emp_class_employee",new EmployeeClassConverterFromEmployeeId());
        convertorMap.put("proc_report_type",new TypeConverter());
        convertorMap.put("religion",new ReligionConverter());
        convertorMap.put("district",new DistrictColumnConverter());
        convertorMap.put("parliamentJoinDate",new ParliamentJoinDateColumnConverter());
        convertorMap.put("parliamentOfficeUnit",new ParliamentOfficeColumnConverter());
        convertorMap.put("parliamentOrganogram",new ParliamentOrganogramColumnConverter());
        convertorMap.put("govtJobJoinDate",new GovtJobJoinDateColumnConverter());
        convertorMap.put("govtJobOfficeUnit",new GovtJobOfficeColumnConverter());
        convertorMap.put("govtJobOrganogram",new GovtJobOrganogramColumnConverter());
        convertorMap.put("employee_records_id_class",new EmployeeRecordIdToDataConverter("employee_records_id_class"));
        convertorMap.put("division",new DivisionColumnConverter());
        convertorMap.put("parliament_number",new ParliamentNumberConverter());
        convertorMap.put("political_party", new PoliticalPartyConverter());
        convertorMap.put("election_constituency",new ElectionConstituencyConverter());
        convertorMap.put("geo_location",new GeoLocationConverter());
        convertorMap.put("location",new LocationConverter());
        convertorMap.put("organogram_key",new OrganogramKeyConverter());
    }

    public static ColumnConvertor getCriteriaConverter(String name){
        return convertorMap.get(name) == null ? defaultConvertor : convertorMap.get(name);
    }

    public static String getValue(String name,String inputValue,String language,String tableName){
        return getCriteriaConverter(name).convert(inputValue,language,tableName).toString();
    }
}
