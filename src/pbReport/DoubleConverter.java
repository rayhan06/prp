package pbReport;

import pb.Utils;

public class DoubleConverter extends ColumnConvertor{
	
	@Override
    public String convert(Object value, String language, String tableName) {
		if(value == null)
		{
			return "";
		}
		double dVal;
		if(value instanceof Double)
		{
			dVal = (double)value;
			return Utils.getDigits(dVal, language);
		}
		else if(!value.toString().equalsIgnoreCase(""))
		{
			dVal = Double.parseDouble(value.toString());
			return Utils.getDigits(dVal, language);
		}
        return "";
    }

}
