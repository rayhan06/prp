package pbReport;

import pb.CatRepository;

public class AmHouseStatusConverter extends ColumnConvertor {
    @Override
    public Object convert(Object columnValue, String language, String tableName) {
        if (columnValue == null) return "";

        long id = Long.parseLong(columnValue.toString());
        return CatRepository.getInstance().getText(language,"am_house_allocation_status",id);

    }
}
