package pbReport;

import pb.Utils;

public class DefaultConvertor extends ColumnConvertor{

	@Override
	public Object convert(Object columnValue, String language, String tableName) {
		if(columnValue == null){
			return "";
		}
		return Utils.getDigits(columnValue.toString(), language);
	}

}