package pbReport;

import org.apache.commons.lang3.StringUtils;

import pb.CatDAO;
import pb.CatRepository;


public class CatConverter extends ColumnConvertor{

	public String convert(Object value, String language, String tableName) {
		if(value == null)
		{
			return "";
		}
		if(value instanceof String){
			if(StringUtils.isNumeric((String) value))
        	{
				value = Integer.parseInt((String) value);
				return CatRepository.getName(language, tableName, (int) value);
        	}
        	else
        	{
        		return (String)value;
        	}
        }
		else if (value instanceof Long)
		{
			return CatRepository.getName(language, tableName, (long) value);
		}
		else if (value instanceof Integer)
		{
			return CatRepository.getName(language, tableName, (int) value);
		}
		else
		{
			return value.toString();
		}
		
	}
}
