package medical_stock_approval_history;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.sql.SQLException;
import common.CommonDAOService;
import common.ConnectionAndStatementUtil;

import org.apache.log4j.Logger;
import util.*;
import pb.*;
import prescription_details.Prescription_detailsDTO;

public class Medical_stock_approval_historyDAO  implements CommonDAOService<Medical_stock_approval_historyDTO>
{
	
	Logger logger = Logger.getLogger(getClass());
	
	private static String addQuery = "";
	private static String updateQuery = "";
	private String[] columnNames = null;

	private Medical_stock_approval_historyDAO()
	{
		columnNames = new String[] 
		{
			"ID",
			"drug_information_id",
			"existing_quantity",
			"approved_quantity",
			"approval_date",
			"approver_organogram_id",
			"approver_employee_id",
			"approver_user_name",
			"search_column",
			"isDeleted",
			"lastModificationTime"
		};
		updateQuery = getUpdateQuery(columnNames);
        addQuery = getAddQuery(columnNames);
		
		searchMap.put("approval_date_start"," and (approval_date >= ?)");
		searchMap.put("approval_date_end"," and (approval_date <= ?)");
		searchMap.put("approver_user_name"," and (approver_user_name like ?)");
		searchMap.put("AnyField"," and (search_column like ?)");
	}

	private final Map<String,String> searchMap = new HashMap<>();

    private static class LazyLoader
    {
    	static final Medical_stock_approval_historyDAO INSTANCE = new Medical_stock_approval_historyDAO();
	}

	public static Medical_stock_approval_historyDAO getInstance()
	{
    	return LazyLoader.INSTANCE;
	}
	
	public void setSearchColumn(Medical_stock_approval_historyDTO medical_stock_approval_historyDTO)
	{
		medical_stock_approval_historyDTO.searchColumn = "";
		medical_stock_approval_historyDTO.searchColumn += medical_stock_approval_historyDTO.approverUserName + " ";
	}
	
	@Override
	public void set(PreparedStatement ps, Medical_stock_approval_historyDTO medical_stock_approval_historyDTO, boolean isInsert) throws SQLException
	{
		int index = 0;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(medical_stock_approval_historyDTO);
		if(isInsert)
		{
			ps.setObject(++index,medical_stock_approval_historyDTO.iD);
		}
		ps.setObject(++index,medical_stock_approval_historyDTO.drugInformationId);
		ps.setObject(++index,medical_stock_approval_historyDTO.existingQuantity);
		ps.setObject(++index,medical_stock_approval_historyDTO.approvedQuantity);
		ps.setObject(++index,medical_stock_approval_historyDTO.approvalDate);
		ps.setObject(++index,medical_stock_approval_historyDTO.approverOrganogramId);
		ps.setObject(++index,medical_stock_approval_historyDTO.approverEmployeeId);
		ps.setObject(++index,medical_stock_approval_historyDTO.approverUserName);
		ps.setObject(++index,medical_stock_approval_historyDTO.searchColumn);
		if(isInsert)
		{
			ps.setObject(++index,medical_stock_approval_historyDTO.isDeleted);
		}
		ps.setObject(++index, lastModificationTime);
		if(!isInsert)
		{
			ps.setObject(++index,medical_stock_approval_historyDTO.iD);
		}
	}
	
	@Override
	public Medical_stock_approval_historyDTO buildObjectFromResultSet(ResultSet rs)
	{
		try
		{
			Medical_stock_approval_historyDTO medical_stock_approval_historyDTO = new Medical_stock_approval_historyDTO();
			int i = 0;
			medical_stock_approval_historyDTO.iD = rs.getLong(columnNames[i++]);
			medical_stock_approval_historyDTO.drugInformationId = rs.getLong(columnNames[i++]);
			medical_stock_approval_historyDTO.existingQuantity = rs.getInt(columnNames[i++]);
			medical_stock_approval_historyDTO.approvedQuantity = rs.getInt(columnNames[i++]);
			medical_stock_approval_historyDTO.approvalDate = rs.getLong(columnNames[i++]);
			medical_stock_approval_historyDTO.approverOrganogramId = rs.getLong(columnNames[i++]);
			medical_stock_approval_historyDTO.approverEmployeeId = rs.getLong(columnNames[i++]);
			medical_stock_approval_historyDTO.approverUserName = rs.getString(columnNames[i++]);
			medical_stock_approval_historyDTO.searchColumn = rs.getString(columnNames[i++]);
			medical_stock_approval_historyDTO.isDeleted = rs.getInt(columnNames[i++]);
			medical_stock_approval_historyDTO.lastModificationTime = rs.getLong(columnNames[i++]);
			return medical_stock_approval_historyDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
		
	public Medical_stock_approval_historyDTO getDTOByID (long id)
	{
		return getDTOFromID(id);
	}

	@Override
	public String getTableName() {
		return "medical_stock_approval_history";
	}

	@Override
	public Map<String, String> getSearchMap() {
		return searchMap;
	}

	public long add(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Medical_stock_approval_historyDTO) commonDTO,addQuery,true);
	}

	public long update(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Medical_stock_approval_historyDTO) commonDTO,updateQuery,false);
	}
	
	public Medical_stock_approval_historyDTO getLastByDrugInformationId (long drug_information_id)
	{
		String sql = "SELECT * FROM " + getTableName() + " WHERE isDeleted = 0 and drug_information_id = " + drug_information_id
				+ " order by id desc limit 1";
		return ConnectionAndStatementUtil.getT(sql,this::buildObjectFromResultSet);
	}
	
	@Override
	public String getLastModifierUser(){
		return "";
    }
				
}
	