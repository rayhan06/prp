package medical_stock_approval_history;

import java.io.IOException;
import java.io.*;
import java.text.SimpleDateFormat;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import org.apache.log4j.Logger;

import login.LoginDTO;
import permission.MenuConstants;
import role.PermissionRepository;

import sessionmanager.SessionConstants;
import user.UserDTO;
import util.*;
import javax.servlet.http.*;
import java.util.*;


import geolocation.GeoLocationDAO2;
import language.LC;
import language.LM;
import common.ApiResponse;
import common.BaseServlet;
import com.google.gson.Gson;
import pb.*;
import pbReport.*;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;




/**
 * Servlet implementation class Medical_stock_approval_historyServlet
 */
@WebServlet("/Medical_stock_approval_historyServlet")
@MultipartConfig
public class Medical_stock_approval_historyServlet extends BaseServlet 
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Medical_stock_approval_historyServlet.class);

    @Override
    public String getTableName() {
        return Medical_stock_approval_historyDAO.getInstance().getTableName();
    }

    @Override
    public String getServletName() {
        return "Medical_stock_approval_historyServlet";
    }

    @Override
    public Medical_stock_approval_historyDAO getCommonDAOService() {
        return Medical_stock_approval_historyDAO.getInstance();
    }
    
    @Override
    public int[] getAddPageMenuConstants() {
        return new int[] {MenuConstants.MEDICAL_STOCK_APPROVAL_HISTORY_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[] {MenuConstants.MEDICAL_STOCK_APPROVAL_HISTORY_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[] {MenuConstants.MEDICAL_STOCK_APPROVAL_HISTORY_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Medical_stock_approval_historyServlet.class;
    }
    private final Gson gson = new Gson();
 	

	@Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception 
	{
		
		request.setAttribute("failureMessage", "");
		System.out.println("%%%% addMedical_stock_approval_history");
		String language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
		Medical_stock_approval_historyDTO medical_stock_approval_historyDTO;
		SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
					
		if(addFlag)
		{
			medical_stock_approval_historyDTO = new Medical_stock_approval_historyDTO();
		}
		else
		{
			medical_stock_approval_historyDTO = Medical_stock_approval_historyDAO.getInstance().getDTOFromID(Long.parseLong(request.getParameter("iD")));
		}
		
		String Value = "";

		Value = request.getParameter("drugInformationId");

		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("drugInformationId = " + Value);
		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			medical_stock_approval_historyDTO.drugInformationId = Long.parseLong(Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}

		Value = request.getParameter("existingQuantity");

		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("existingQuantity = " + Value);
		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			medical_stock_approval_historyDTO.existingQuantity = Integer.parseInt(Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}

		Value = request.getParameter("approvedQuantity");

		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("approvedQuantity = " + Value);
		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			medical_stock_approval_historyDTO.approvedQuantity = Integer.parseInt(Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}

		Value = request.getParameter("approvalDate");

		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("approvalDate = " + Value);
		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			try 
			{
				Date d = f.parse(Value);
				medical_stock_approval_historyDTO.approvalDate = d.getTime();
			}
			catch (Exception e) 
			{
				e.printStackTrace();
				throw new Exception(LM.getText(LC.MEDICAL_STOCK_APPROVAL_HISTORY_ADD_APPROVALDATE, language) + " " + ErrorMessage.getInvalidMessage(language));
			}
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}

		Value = request.getParameter("approverOrganogramId");

		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("approverOrganogramId = " + Value);
		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			medical_stock_approval_historyDTO.approverOrganogramId = Long.parseLong(Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}

		Value = request.getParameter("approverEmployeeId");

		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("approverEmployeeId = " + Value);
		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			medical_stock_approval_historyDTO.approverEmployeeId = Long.parseLong(Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}

		Value = request.getParameter("approverUserName");

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("approverUserName = " + Value);
		if(Value != null)
		{
			medical_stock_approval_historyDTO.approverUserName = (Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}
		System.out.println("Done adding  addMedical_stock_approval_history dto = " + medical_stock_approval_historyDTO);

		if(addFlag == true)
		{
			Medical_stock_approval_historyDAO.getInstance().add(medical_stock_approval_historyDTO);
		}
		else
		{				
			Medical_stock_approval_historyDAO.getInstance().update(medical_stock_approval_historyDTO);										
		}
		
		

		return medical_stock_approval_historyDTO;

	}
}

