package medical_stock_approval_history;


import drug_information.Drug_informationDTO;
import user.UserDTO;
import util.*; 


public class Medical_stock_approval_historyDTO extends CommonDTO
{

	public long drugInformationId = -1;
	public int existingQuantity = 0;
	public int approvedQuantity = 0;
	public long approvalDate = System.currentTimeMillis();
	public long approverOrganogramId = -1;
	public long approverEmployeeId = -1;
    public String approverUserName = "";
    
    public Medical_stock_approval_historyDTO()
    {
    	
    }
    
    public Medical_stock_approval_historyDTO(Drug_informationDTO drug_informationDTO, UserDTO userDTO)
    {
    	drugInformationId = drug_informationDTO.iD;
    	existingQuantity = drug_informationDTO.currentStock;
    	approvedQuantity = drug_informationDTO.availableStock;
    	approverOrganogramId = userDTO.organogramID;
    	approverEmployeeId = userDTO.employee_record_id;
    	approverUserName = userDTO.userName;
    }
	
	
    @Override
	public String toString() {
            return "$Medical_stock_approval_historyDTO[" +
            " iD = " + iD +
            " drugInformationId = " + drugInformationId +
            " existingQuantity = " + existingQuantity +
            " approvedQuantity = " + approvedQuantity +
            " approvalDate = " + approvalDate +
            " approverOrganogramId = " + approverOrganogramId +
            " approverEmployeeId = " + approverEmployeeId +
            " approverUserName = " + approverUserName +
            " searchColumn = " + searchColumn +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}