package payroll_daily_bill;

import util.CommonDTO;


public class Payroll_daily_billDTO extends CommonDTO {
    public long monthYear = System.currentTimeMillis();
    public long budgetOfficeId = -1;
    public long budgetRegisterId = -1;
    public long billRegisterId = -1;
    public long allowanceEmployeeInfoId = -1;
    public long employeeRecordsId = -1;
    public long officeUnitId = -1;
    public String organogramKey = "";
    public int dailyRate = 0;
    public int day = 0;
    public long totalAmount = 0;
    public int revenueStampDeduction = 0;
    public long netAmount = 0;
    public long modifiedBy = -1;
    public long insertedBy = -1;
    public long insertionDate = -1;

    @Override
    public String toString() {
        return "$Payroll_daily_billDTO[" +
                " iD = " + iD +
                " monthYear = " + monthYear +
                " allowanceEmployeeInfoId = " + allowanceEmployeeInfoId +
                " employeeRecordsId = " + employeeRecordsId +
                " officeUnitId = " + officeUnitId +
                " organogramKey = " + organogramKey +
                " dailyRate = " + dailyRate +
                " day = " + day +
                " revenueStampDeduction = " + revenueStampDeduction +
                " searchColumn = " + searchColumn +
                " modifiedBy = " + modifiedBy +
                " lastModificationTime = " + lastModificationTime +
                " insertedBy = " + insertedBy +
                " insertionDate = " + insertionDate +
                " isDeleted = " + isDeleted +
                " budgetOfficeId = " + budgetOfficeId +
                "]";
    }
}