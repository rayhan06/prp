package payroll_daily_bill;

import allowance_employee_info.AllowanceEmployeeInfoDTO;
import allowance_employee_info.AllowanceEmployeeInfoRepository;
import util.StringUtils;

import static util.UtilCharacter.getDataByLanguage;

@SuppressWarnings({"Duplicates"})
public class Payroll_daily_billModel {
    public String employeeRecordsId = "-1";
    public String payrollDailyBillId = "-1";
    public String name = "";
    public String officeName = "";
    public String organogramName = "";
    public String mobileNumber = "";
    public String savingAccountNumber = "";
    public String dailyRate = "";
    public String day = "";
    public String totalAmount = "";
    public String revenueStampDeduction = "";
    public String netAmount = "";
    public String organogramKey = "";

    public Payroll_daily_billModel(AllowanceEmployeeInfoDTO employeeInfoDTO, long monthYear, String language) {
        setEmployeeInfo(this, employeeInfoDTO, language);

        int dailyRateInt = Payroll_daily_billServlet.getDailyRate(organogramKey);
        dailyRate = String.valueOf(dailyRateInt);

        int dayInt = Payroll_daily_billServlet.getSalaryDaysCount(employeeInfoDTO.employeeRecordId, monthYear);
        day = String.valueOf(dayInt);

        long totalAmountLong = (long) dailyRateInt * dayInt;
        totalAmount = String.valueOf(totalAmountLong);

        int deduct = Payroll_daily_billServlet.getTaxDeduction(organogramKey, totalAmountLong);
        revenueStampDeduction = String.valueOf(deduct);

        netAmount = String.valueOf(totalAmountLong - deduct);
    }

    public Payroll_daily_billModel(Payroll_daily_billDTO billDTO, String language) {
        AllowanceEmployeeInfoDTO allowanceEmployeeInfoDTO = AllowanceEmployeeInfoRepository.getInstance().getById(billDTO.allowanceEmployeeInfoId);
        setEmployeeInfo(this, allowanceEmployeeInfoDTO, language);
        setAllowanceInfo(this, billDTO);
    }

    private static void setEmployeeInfo(Payroll_daily_billModel model, AllowanceEmployeeInfoDTO employeeInfoDTO, String language) {
        model.employeeRecordsId = String.valueOf(employeeInfoDTO.employeeRecordId);
        model.name = getDataByLanguage(language, employeeInfoDTO.nameBn, employeeInfoDTO.nameEn);
        model.officeName = getDataByLanguage(language, employeeInfoDTO.officeNameBn, employeeInfoDTO.officeNameEn);
        model.organogramName = getDataByLanguage(language, employeeInfoDTO.organogramNameBn, employeeInfoDTO.organogramNameEn);
        model.mobileNumber = StringUtils.convertBanglaIfLanguageIsBangla(language, employeeInfoDTO.mobileNumber);
        model.savingAccountNumber = StringUtils.convertBanglaIfLanguageIsBangla(language, employeeInfoDTO.savingAccountNumber);
        model.organogramKey = employeeInfoDTO.organogramNameEn.toLowerCase();
    }

    private static void setAllowanceInfo(Payroll_daily_billModel model, Payroll_daily_billDTO billDTO) {
        model.payrollDailyBillId = String.valueOf(billDTO.iD);
        model.dailyRate = billDTO.dailyRate < 0 ? "" : String.valueOf(billDTO.dailyRate);
        model.day = billDTO.day < 0 ? "" : String.valueOf(billDTO.day);
        model.totalAmount = billDTO.totalAmount < 0 ? "" : String.valueOf(billDTO.totalAmount);
        model.netAmount = billDTO.netAmount < 0 ? "" : String.valueOf(billDTO.netAmount);
        model.revenueStampDeduction = billDTO.revenueStampDeduction < 0 ? "" : String.valueOf(billDTO.revenueStampDeduction);
    }
}
