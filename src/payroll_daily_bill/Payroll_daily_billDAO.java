package payroll_daily_bill;

import allowance_employee_info.AllowanceEmployeeInfoDTO;
import allowance_employee_info.AllowanceEmployeeInfoRepository;
import common.CommonDAOService;
import org.apache.log4j.Logger;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static util.StringUtils.convertToBanNumber;

@SuppressWarnings({"Duplicates"})
public class Payroll_daily_billDAO implements CommonDAOService<Payroll_daily_billDTO> {
    private static final Logger logger = Logger.getLogger(Payroll_daily_billDAO.class);

    private static final String addQuery =
            "INSERT INTO {tableName} (budget_office_id,month_year,budget_register_id,bill_register_id,allowance_employee_info_id,employee_records_id,office_unit_id,"
                    .concat("organogram_key,daily_rate,day,revenue_stamp_deduction,search_column,modified_by,lastModificationTime,")
                    .concat("inserted_by,insertion_date,isDeleted,ID) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

    private static final String updateQuery =
            "UPDATE {tableName} SET budget_office_id=?,month_year=?,budget_register_id=?,bill_register_id,allowance_employee_info_id=?,"
                    .concat("employee_records_id=?,office_unit_id=?,organogram_key=?,daily_rate=?,day=?,revenue_stamp_deduction=?,")
                    .concat("search_column=?,modified_by=?,lastModificationTime=? WHERE ID=?");

    private static final String getByBudgetOfficeAndMonth =
            "SELECT * FROM payroll_daily_bill WHERE budget_office_id=%d AND month_year=%d AND isDeleted=0";

    private final Map<String, String> searchMap = new HashMap<>();

    public Payroll_daily_billDAO() {
        searchMap.put("organogram_key", " and (organogram_key = ?)");
        searchMap.put("budgetOfficeId", " AND (budget_office_id = ?) ");
        searchMap.put("monthYear", " AND (month_year = ?) ");
        searchMap.put("AnyField", " and (search_column like ?)");
    }

    private static class LazyLoader {
        static final Payroll_daily_billDAO INSTANCE = new Payroll_daily_billDAO();
    }

    public static Payroll_daily_billDAO getInstance() {
        return LazyLoader.INSTANCE;
    }

    public void setSearchColumn(Payroll_daily_billDTO payroll_daily_billDTO) {
        AllowanceEmployeeInfoDTO allowanceEmployeeInfoDTO =
                AllowanceEmployeeInfoRepository.getInstance().getById(payroll_daily_billDTO.allowanceEmployeeInfoId);
        payroll_daily_billDTO.searchColumn = "";
        payroll_daily_billDTO.searchColumn =
                allowanceEmployeeInfoDTO.nameEn + " " + allowanceEmployeeInfoDTO.nameBn + " "
                + allowanceEmployeeInfoDTO.officeNameEn + " "
                + allowanceEmployeeInfoDTO.officeNameBn + " "
                + allowanceEmployeeInfoDTO.organogramNameEn + " "
                + allowanceEmployeeInfoDTO.organogramNameBn + " "
                + allowanceEmployeeInfoDTO.mobileNumber + " "
                + convertToBanNumber(allowanceEmployeeInfoDTO.mobileNumber) + " "
                + allowanceEmployeeInfoDTO.savingAccountNumber + " "
                + convertToBanNumber(allowanceEmployeeInfoDTO.savingAccountNumber);
    }

    @Override
    public void set(PreparedStatement ps, Payroll_daily_billDTO dto, boolean isInsert) throws SQLException {
        setSearchColumn(dto);
        int index = 0;
        ps.setLong(++index, dto.budgetOfficeId);
        ps.setLong(++index, dto.monthYear);
        ps.setLong(++index, dto.budgetRegisterId);
        ps.setLong(++index, dto.billRegisterId);
        ps.setLong(++index, dto.allowanceEmployeeInfoId);
        ps.setLong(++index, dto.employeeRecordsId);
        ps.setLong(++index, dto.officeUnitId);
        ps.setString(++index, dto.organogramKey);
        ps.setInt(++index, dto.dailyRate);
        ps.setInt(++index, dto.day);
        ps.setInt(++index, dto.revenueStampDeduction);
        ps.setString(++index, dto.searchColumn);
        ps.setLong(++index, dto.modifiedBy);
        ps.setLong(++index, dto.lastModificationTime);
        if (isInsert) {
            ps.setLong(++index, dto.insertedBy);
            ps.setLong(++index, dto.insertionDate);
            ps.setInt(++index, 0 /* isDeleted */);
        }
        ps.setLong(++index, dto.iD);
    }

    @Override
    public Payroll_daily_billDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Payroll_daily_billDTO dto = new Payroll_daily_billDTO();
            dto.iD = rs.getLong("ID");
            dto.budgetOfficeId = rs.getLong("budget_office_id");
            dto.monthYear = rs.getLong("month_year");
            dto.budgetRegisterId = rs.getLong("budget_register_id");
            dto.billRegisterId = rs.getLong("bill_register_id");
            dto.allowanceEmployeeInfoId = rs.getLong("allowance_employee_info_id");
            dto.employeeRecordsId = rs.getLong("employee_records_id");
            dto.officeUnitId = rs.getLong("office_unit_id");
            dto.organogramKey = rs.getString("organogram_key");
            dto.dailyRate = rs.getInt("daily_rate");
            dto.day = rs.getInt("day");
            dto.totalAmount = ((long) dto.dailyRate * dto.day);
            dto.revenueStampDeduction = rs.getInt("revenue_stamp_deduction");
            dto.netAmount = dto.totalAmount - dto.revenueStampDeduction;
            dto.searchColumn = rs.getString("search_column");
            dto.modifiedBy = rs.getLong("modified_by");
            dto.lastModificationTime = rs.getLong("lastModificationTime");
            dto.insertedBy = rs.getLong("inserted_by");
            dto.insertionDate = rs.getLong("insertion_date");
            dto.isDeleted = rs.getInt("isDeleted");
            return dto;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    public Payroll_daily_billDTO getDTOByID(long id) {
        return getDTOFromID(id);
    }

    @Override
    public String getTableName() {
        return "payroll_daily_bill";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Payroll_daily_billDTO) commonDTO, addQuery, true);
    }

    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Payroll_daily_billDTO) commonDTO, updateQuery, false);
    }

    public List<Payroll_daily_billDTO> getByBudgetOfficeAndMonth(long budgetOfficeId, long monthYear) {
        return getDTOs(String.format(getByBudgetOfficeAndMonth, budgetOfficeId, monthYear));
    }
}