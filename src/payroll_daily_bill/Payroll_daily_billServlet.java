package payroll_daily_bill;

import allowance_configure.AllowanceTypeEnum;
import allowance_employee_info.AllowanceEmployeeInfoDTO;
import allowance_employee_info.AllowanceEmployeeInfoRepository;
import bill_register.Bill_registerServlet;
import budget.BudgetCategoryEnum;
import budget.BudgetUtils;
import budget_mapping.Budget_mappingDTO;
import budget_mapping.Budget_mappingRepository;
import budget_office.Budget_officeRepository;
import budget_register.Budget_registerDAO;
import budget_register.Budget_registerDTO;
import budget_register.Budget_registerModel;
import budget_selection_info.BudgetSelectionInfoRepository;
import com.google.gson.GsonBuilder;
import common.BaseServlet;
import employee_attendance.Employee_attendanceDAO;
import employee_records.EmploymentEnum;
import finance.CashTypeEnum;
import finance.FinanceUtil;
import login.LoginDTO;
import office_unit_organograms.SameDesignationGroup;
import org.apache.log4j.Logger;
import payroll_daily_configuration.Payroll_daily_configurationDTO;
import payroll_daily_configuration.Payroll_daily_configurationRepository;
import pb.Utils;
import pbReport.DateUtils;
import permission.MenuConstants;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.CommonDTO;
import util.HttpRequestUtils;
import util.StringUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

import static pbReport.DateUtils.get1stDayOfNextMonth;

@WebServlet("/Payroll_daily_billServlet")
@MultipartConfig
@SuppressWarnings({"Duplicates"})
public class Payroll_daily_billServlet extends BaseServlet {
    public static final long DAILY_BILL_ECONOMIC_SUB_CODE_ID = 301L;
    public static final String DAILY_BILL_DESCRIPTION = "%s-এ কর্মরত %s জন সাং-বাৎসরিক কর্মচারীর %s মাসের মজুরী";
    public static final CashTypeEnum CASH_TYPE = CashTypeEnum.SALARY;
    private static final long serialVersionUID = 1L;
    private static final Logger logger = Logger.getLogger(Payroll_daily_billServlet.class);

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }

        try {
            String actionType = request.getParameter("actionType");
            switch (actionType) {
                case "ajax_getDailyPayrollData": {
                    long monthYear = BudgetUtils.parseMonthYearString(request.getParameter("monthYear"));
                    long budgetOfficeId = Long.parseLong(request.getParameter("budgetOfficeId"));
                    String language = userDTO.languageID == SessionConstants.ENGLISH ? "English" : "Bangla";
                    Map<String, Object> res = getDailyPayrollData(budgetOfficeId, monthYear, language);
                    response.setContentType("application/json");
                    response.setCharacterEncoding("UTF-8");
                    response.getWriter().println(new GsonBuilder().serializeNulls().create().toJson(res));
                    return;
                }
                case "prepareBill": {
                    setBillDTOs(request);
                    request.getRequestDispatcher("payroll_daily_bill/payroll_daily_billAgOfficeBill.jsp").forward(request, response);
                    return;
                }
                case "prepareSummary": {
                    setBillDTOs(request);
                    request.getRequestDispatcher("payroll_daily_bill/payroll_daily_billSummary.jsp").forward(request, response);
                    return;
                }
                default:
                    super.doGet(request, response);
                    return;
            }
        } catch (Exception ex) {
            logger.error(ex);
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }

    private void setBillDTOs(HttpServletRequest request) throws FileNotFoundException {
        long monthYear = Long.parseLong(request.getParameter("monthYear"));
        long budgetOfficeId = Long.parseLong(request.getParameter("budgetOfficeId"));

        List<Payroll_daily_billDTO> billDTOs = Payroll_daily_billDAO.getInstance().getByBudgetOfficeAndMonth(budgetOfficeId, monthYear);
        if (billDTOs == null || billDTOs.isEmpty())
            throw new FileNotFoundException("no DTOs found");

        request.setAttribute("billDTOs", billDTOs);
        request.setAttribute("monthYear", monthYear);
        request.setAttribute("budgetOfficeId", budgetOfficeId);
    }

    private Map<String, Object> getDailyPayrollData(long budgetOfficeId, long monthYear, String language) {
        List<Payroll_daily_billDTO> dtoList = Payroll_daily_billDAO.getInstance().getByBudgetOfficeAndMonth(budgetOfficeId, monthYear);
        List<Payroll_daily_billModel> daily_billModels;

        boolean isAlreadyAdded = dtoList != null && !dtoList.isEmpty();
        Budget_registerModel budgetRegisterModel = null;
        if (isAlreadyAdded) {
            daily_billModels =
                    dtoList.stream()
                            .map(dto -> new Payroll_daily_billModel(dto, language))
                            .collect(Collectors.toList());
            budgetRegisterModel = Budget_registerDAO.getInstance().getModelById(dtoList.get(0).budgetRegisterId);
        } else {
            Set<Long> budgetOfficeIds = Budget_officeRepository.getInstance().getOfficeUnitIdSet(budgetOfficeId);
            List<AllowanceEmployeeInfoDTO> employeeInfoDTOs =
                    AllowanceEmployeeInfoRepository.getInstance().buildDTOsFromCacheForEmploymentType(budgetOfficeIds, EmploymentEnum.DAILY_BASIS.getValue());

            daily_billModels =
                    employeeInfoDTOs.stream()
                            .map(employeeInfoDTO -> new Payroll_daily_billModel(employeeInfoDTO, monthYear, language))
                            .collect(Collectors.toList());
        }
        Map<String, Object> res = new HashMap<>();
        res.put("isAlreadyAdded", isAlreadyAdded);
        res.put("dailyBillModels", daily_billModels);
        res.put("budgetRegisterModel", budgetRegisterModel);
        return res;
    }

    @SuppressWarnings({"unused"})
    public static int getSalaryDaysCount(long employeeRecordId, long monthYear) {
        long endDate = get1stDayOfNextMonth(monthYear) - 1;
        return new Employee_attendanceDAO().numberOfWorkingDays(monthYear, endDate);
    }

    public static int getDailyRate(String organogramKey) {
        Payroll_daily_configurationDTO configureDTO =
                Payroll_daily_configurationRepository.getInstance()
                        .getByOrganogramKey(organogramKey.trim());
        return configureDTO != null ? configureDTO.dailyAmount : 0;
    }

    public static int getTaxDeduction(String organogramKey, long totalAmountLong) {
        Payroll_daily_configurationDTO configureDTO =
                Payroll_daily_configurationRepository.getInstance()
                        .getByOrganogramKey(organogramKey.trim());
        if (configureDTO == null) return 0;
        return (int) (
                configureDTO.taxDeductionAmountType == AllowanceTypeEnum.FIXED.getValue()
                        ? configureDTO.taxDeductionAmount
                        : totalAmountLong * configureDTO.taxDeductionAmount / 100.0D
        );
    }

    public Payroll_daily_billDTO getPayrollDailyBillDTO(UserInput userInput) throws Exception {
        Payroll_daily_billDTO daily_billDTO = new Payroll_daily_billDTO();

        daily_billDTO.insertedBy = daily_billDTO.modifiedBy = userInput.modifierId;
        daily_billDTO.insertionDate = daily_billDTO.lastModificationTime = userInput.modificationTime;
        daily_billDTO.budgetOfficeId = userInput.budgetOfficeId;
        daily_billDTO.monthYear = userInput.monthYear;
        daily_billDTO.budgetRegisterId = userInput.budgetRegisterId;

        AllowanceEmployeeInfoDTO employeeInfoDTO =
                AllowanceEmployeeInfoRepository.getInstance()
                        .getByEmployeeRecordId(userInput.employeeRecordsId);
        if (employeeInfoDTO == null) return null;

        daily_billDTO.allowanceEmployeeInfoId = employeeInfoDTO.iD;
        daily_billDTO.officeUnitId = employeeInfoDTO.officeUnitId;
        long organogramId = employeeInfoDTO.organogramId;
        daily_billDTO.organogramKey = new SameDesignationGroup(organogramId).organogramKey;
        daily_billDTO.employeeRecordsId = employeeInfoDTO.employeeRecordId;
        daily_billDTO.dailyRate = getDailyRate(daily_billDTO.organogramKey);
        daily_billDTO.day = getSalaryDaysCount(userInput.employeeRecordsId, userInput.monthYear);
        daily_billDTO.totalAmount = (long) daily_billDTO.day * daily_billDTO.dailyRate;
        daily_billDTO.revenueStampDeduction = getTaxDeduction(daily_billDTO.organogramKey, daily_billDTO.totalAmount);

        return daily_billDTO;
    }

    @Override
    public String getAjaxAddRedirectURL(HttpServletRequest request, CommonDTO commonDTO) {
        String url = getServletName() + "?actionType=";
        String source = request.getParameter("source");
        if ("prepareBill".equalsIgnoreCase(source)) {
            url += "prepareBill";
        } else if ("prepareSummary".equalsIgnoreCase(source)) {
            url += "prepareSummary";
        }
        Payroll_daily_billDTO dailyBillDTO = (Payroll_daily_billDTO) commonDTO;
        url += ("&monthYear=" + dailyBillDTO.monthYear + "&budgetOfficeId=" + dailyBillDTO.budgetOfficeId);
        return url;
    }

    @Override
    public String getAjaxEditRedirectURL(HttpServletRequest request, CommonDTO commonDTO) {
        return getAjaxAddRedirectURL(request, commonDTO);
    }

    public static String getBillDescription(UserInput userInput) {
        String budgetOfficeNameBn = Budget_officeRepository.getInstance().getText(userInput.budgetOfficeId, "Bangla");
        String monthName = DateUtils.getMonthYear(userInput.monthYear, "Bangla", "/");
        return String.format(
                DAILY_BILL_DESCRIPTION,
                budgetOfficeNameBn,
                StringUtils.convertToBanNumber(String.valueOf(userInput.employeeCount)),
                monthName
        );
    }

    private Budget_registerDTO addBudgetRegisterDTO(UserInput userInput) throws Exception {
        Budget_registerDTO budgetRegisterDTO = new Budget_registerDTO();

        budgetRegisterDTO.recipientName = FinanceUtil.getFinance1HeadDesignation("bangla");
        budgetRegisterDTO.issueNumber = userInput.issueNumber;
        budgetRegisterDTO.issueDate = userInput.issueDate;
        budgetRegisterDTO.description = getBillDescription(userInput);
        budgetRegisterDTO.billAmount = userInput.billAmount;

        Long budgetSelectionInfoId =
                BudgetSelectionInfoRepository.getInstance()
                        .getId(BudgetUtils.getEconomicYear(userInput.monthYear));
        if (budgetSelectionInfoId != null)
            budgetRegisterDTO.budgetSelectionInfoId = budgetSelectionInfoId;

        Budget_mappingDTO budgetMappingDTO =
                Budget_mappingRepository.getInstance()
                        .getDTO(userInput.budgetOfficeId, BudgetCategoryEnum.OPERATIONAL.getValue());
        if (budgetMappingDTO != null) {
            budgetRegisterDTO.budgetMappingId = budgetMappingDTO.iD;
            budgetRegisterDTO.budgetOfficeId = budgetMappingDTO.budgetOfficeId;
        }
        budgetRegisterDTO.economicSubCodeId = DAILY_BILL_ECONOMIC_SUB_CODE_ID;

        budgetRegisterDTO.insertionTime
                = budgetRegisterDTO.lastModificationTime
                = userInput.modificationTime;
        budgetRegisterDTO.insertedBy
                = budgetRegisterDTO.modifiedBy
                = userInput.modifierId;

        Budget_registerDAO.getInstance().add(budgetRegisterDTO);
        return budgetRegisterDTO;
    }

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        boolean isLanguageEnglish = "english".equalsIgnoreCase(HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language);

        UserInput userInput = new UserInput();
        userInput.budgetOfficeId = Long.parseLong(request.getParameter("budgetOfficeId"));
        userInput.monthYear = BudgetUtils.parseMonthYearString(request.getParameter("monthYear"));
        userInput.modificationTime = System.currentTimeMillis();
        if (addFlag) {
            List<Payroll_daily_billDTO> dtoList = Payroll_daily_billDAO.getInstance().getByBudgetOfficeAndMonth(
                    userInput.budgetOfficeId, userInput.monthYear
            );
            boolean isAlreadyAdded = dtoList != null && !dtoList.isEmpty();
            if (!isAlreadyAdded) {
                userInput.issueDate = Utils.parseMandatoryDate(
                        request.getParameter("issueDate"),
                        isLanguageEnglish ? "Input Issue Date" : "ইশ্যু তারিখ বাছাই করুন"
                );
                userInput.issueNumber = Utils.cleanAndGetMandatoryString(
                        request.getParameter("issueNumber"),
                        isLanguageEnglish ? "Input Issue Number" : "ইশ্যু নম্বর লিখুন"
                );

                String[] employeeRecordsIds = request.getParameterValues("employeeRecordsId");
                if (employeeRecordsIds.length <= 1 && Long.parseLong(employeeRecordsIds[0]) < 0) {
                    throw new Exception(isLanguageEnglish ? "No Employee Found" : "কোন কর্মকর্তা পাওয়া যায়নি");
                }
                String[] dailyBillIds = request.getParameterValues("payrollDailyBillId");
                userInput.modifierId = userDTO.ID;
                List<Payroll_daily_billDTO> payrollDailyBillDTOs = new ArrayList<>();
                for (int i = 0; i < employeeRecordsIds.length; i++) {
                    userInput.employeeRecordsId = Long.parseLong(employeeRecordsIds[i]);
                    if (userInput.employeeRecordsId < 0) continue;
                    userInput.payrollDailyBillId = Long.parseLong(dailyBillIds[i]);
                    Payroll_daily_billDTO payrollDailyBillDTO = getPayrollDailyBillDTO(userInput);
                    if (payrollDailyBillDTO != null)
                        payrollDailyBillDTOs.add(payrollDailyBillDTO);
                }

                userInput.billAmount = payrollDailyBillDTOs.stream()
                        .mapToLong(dto -> dto.totalAmount)
                        .sum();
                userInput.employeeCount = payrollDailyBillDTOs.size();
                Budget_registerDTO addedBudget_registerDTO = addBudgetRegisterDTO(userInput);
                userInput.budgetRegisterId = addedBudget_registerDTO.iD;
                userInput.billRegisterId = Bill_registerServlet.addBillRegister(addedBudget_registerDTO, CASH_TYPE, true).iD;
                for (Payroll_daily_billDTO payrollDailyBillDTO : payrollDailyBillDTOs) {
                    payrollDailyBillDTO.budgetRegisterId = userInput.budgetRegisterId;
                    payrollDailyBillDTO.billRegisterId = userInput.billRegisterId;
                    Payroll_daily_billDAO.getInstance().add(payrollDailyBillDTO);
                }
            }
        }
        Payroll_daily_billDTO daily_billDTO = new Payroll_daily_billDTO();
        daily_billDTO.monthYear = userInput.monthYear;
        daily_billDTO.budgetOfficeId = userInput.budgetOfficeId;
        return daily_billDTO;
    }

    static class UserInput {
        long modifierId;
        long modificationTime;
        long budgetOfficeId;
        long monthYear;
        long employeeRecordsId;
        long payrollDailyBillId;
        long issueDate;
        long budgetRegisterId;
        long billRegisterId;
        String issueNumber;
        int employeeCount;
        long billAmount;
    }

    @Override
    public String getTableName() {
        return Payroll_daily_billDAO.getInstance().getTableName();
    }

    @Override
    public String getServletName() {
        return "Payroll_daily_billServlet";
    }

    @Override
    public Payroll_daily_billDAO getCommonDAOService() {
        return Payroll_daily_billDAO.getInstance();
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[]{MenuConstants.PAYROLL_DAILY_BILL_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[]{MenuConstants.PAYROLL_DAILY_BILL_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[]{MenuConstants.PAYROLL_DAILY_BILL_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Payroll_daily_billServlet.class;
    }
}