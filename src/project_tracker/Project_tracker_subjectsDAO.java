package project_tracker;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;


import org.apache.log4j.Logger;

import dbm.*;

import repository.RepositoryManager;

import util.*;
import pb.*;
import approval_module_map.*;

public class Project_tracker_subjectsDAO  extends NavigationService3
{
	
	Logger logger = Logger.getLogger(getClass());

	
	public Project_tracker_subjectsDAO(String tableName, String tempTableName, Approval_module_mapDTO approval_module_mapDTO)
	{
		super(tableName, tempTableName, approval_module_mapDTO);		
	}
	
	
	
	public long add(CommonDTO commonDTO, String tableName, TempTableDTO tempTableDTO) throws Exception
	{
		
		Project_tracker_subjectsDTO project_tracker_subjectsDTO = (Project_tracker_subjectsDTO)commonDTO;
		
		Connection connection = null;
		PreparedStatement ps = null;

		long lastModificationTime = System.currentTimeMillis();	

		try{
			connection = DBMW.getInstance().getConnection();
			
			if(connection == null)
			{
				System.out.println("nullconn");
			}

			project_tracker_subjectsDTO.iD = DBMW.getInstance().getNextSequenceId(tableName);

			String sql = "INSERT INTO " + tableName;
			
			sql += " (";
			sql += "ID";
			sql += ", ";
			sql += "name_en";
			sql += ", ";
			sql += "name_bn";
			sql += ", ";
			sql += "isDeleted";
			sql += ", ";
			sql += "lastModificationTime";
			if(tempTableDTO!=null)
			{
				sql += ", permanent_table_id, operation_type, approval_path_type, approval_order";
			}
			sql += ")";
			
			
            sql += " VALUES(";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			if(tempTableDTO!=null)
			{
				sql += ", " + tempTableDTO.permanent_table_id + ", " + tempTableDTO.operation_type + ", " + tempTableDTO.approval_path_type + ", " + tempTableDTO.approval_order;
			}
			sql += ")";
				
			printSql(sql);

			ps = connection.prepareStatement(sql);
			
			
			

			int index = 1;

			ps.setObject(index++,project_tracker_subjectsDTO.iD);
			ps.setObject(index++,project_tracker_subjectsDTO.nameEn);
			ps.setObject(index++,project_tracker_subjectsDTO.nameBn);
			ps.setObject(index++,project_tracker_subjectsDTO.isDeleted);
			ps.setObject(index++, lastModificationTime);
			
			System.out.println(ps);
			ps.execute();
			
			
			recordUpdateTime(connection, lastModificationTime, tableName);

		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if (ps != null) {
					ps.close();
				}
			} catch(Exception e){}
			try{
				if(connection != null){
					DBMW.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		return project_tracker_subjectsDTO.iD;		
	}
		
	

	//need another getter for repository
	public CommonDTO getDTOByID (long ID, String tableName) throws Exception
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Project_tracker_subjectsDTO project_tracker_subjectsDTO = null;
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
			
            sql += " WHERE ID=" + ID;
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			if(rs.next()){
				project_tracker_subjectsDTO = new Project_tracker_subjectsDTO();

				project_tracker_subjectsDTO.iD = rs.getLong("ID");
				project_tracker_subjectsDTO.nameEn = rs.getString("name_en");
				project_tracker_subjectsDTO.nameBn = rs.getString("name_bn");
				project_tracker_subjectsDTO.isDeleted = rs.getInt("isDeleted");
				project_tracker_subjectsDTO.lastModificationTime = rs.getLong("lastModificationTime");

			}			
			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DBMR.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
		return project_tracker_subjectsDTO;
	}
	
	public long update(CommonDTO commonDTO, String tableName) throws Exception
	{		
		Project_tracker_subjectsDTO project_tracker_subjectsDTO = (Project_tracker_subjectsDTO)commonDTO;
		
		Connection connection = null;
		PreparedStatement ps = null;

		long lastModificationTime = System.currentTimeMillis();	
		try{
			connection = DBMW.getInstance().getConnection();

			String sql = "UPDATE " + tableName;
			
			sql += " SET ";
			sql += "name_en=?";
			sql += ", ";
			sql += "name_bn=?";
			sql += ", lastModificationTime = "	+ lastModificationTime + "";
            sql += " WHERE ID = " + project_tracker_subjectsDTO.iD;
				
			printSql(sql);

			ps = connection.prepareStatement(sql);
			
			

			int index = 1;
			
			ps.setObject(index++,project_tracker_subjectsDTO.nameEn);
			ps.setObject(index++,project_tracker_subjectsDTO.nameBn);
			System.out.println(ps);
			ps.executeUpdate();
			


			
			recordUpdateTime(connection,  lastModificationTime, tableName);

						
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if (ps != null) {
					ps.close();
				}
			} catch(Exception e){}
			try{
				if(connection != null){
					DBMW.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		return project_tracker_subjectsDTO.iD;
	}
	
	public List<Project_tracker_subjectsDTO> getDTOs(Collection recordIDs)
	{
		return getDTOs(recordIDs, tableName);
	}
	
	public List<Project_tracker_subjectsDTO> getDTOs(Collection recordIDs, String tableName){
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Project_tracker_subjectsDTO project_tracker_subjectsDTO = null;
		List<Project_tracker_subjectsDTO> project_tracker_subjectsDTOList = new ArrayList<>();
		if(recordIDs.isEmpty()){
			return project_tracker_subjectsDTOList;
		}
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
            
            sql += " WHERE ID IN ( ";

			for(int i = 0;i<recordIDs.size();i++){
				if(i!=0){
					sql+=",";
				}
				sql+=((ArrayList)recordIDs).get(i);
			}
			sql+=")  order by lastModificationTime desc";
			
			printSql(sql);
			
			logger.debug("sql " + sql);
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				project_tracker_subjectsDTO = new Project_tracker_subjectsDTO();
				project_tracker_subjectsDTO.iD = rs.getLong("ID");
				project_tracker_subjectsDTO.nameEn = rs.getString("name_en");
				project_tracker_subjectsDTO.nameBn = rs.getString("name_bn");
				project_tracker_subjectsDTO.isDeleted = rs.getInt("isDeleted");
				project_tracker_subjectsDTO.lastModificationTime = rs.getLong("lastModificationTime");
				System.out.println("got this DTO: " + project_tracker_subjectsDTO);
				
				project_tracker_subjectsDTOList.add(project_tracker_subjectsDTO);

			}			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DBMR.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
		return project_tracker_subjectsDTOList;
	
	}
	
	

	
	
	
	//add repository
	public List<Project_tracker_subjectsDTO> getAllProject_tracker_subjects (boolean isFirstReload)
    {
		List<Project_tracker_subjectsDTO> project_tracker_subjectsDTOList = new ArrayList<>();

		String sql = "SELECT * FROM project_tracker_subjects";
		sql += " WHERE ";
	

		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by project_tracker_subjects.lastModificationTime desc";
		printSql(sql);
		
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;

		
		try{
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();
			rs = stmt.executeQuery(sql);
			

			while(rs.next()){
				Project_tracker_subjectsDTO project_tracker_subjectsDTO = new Project_tracker_subjectsDTO();
				project_tracker_subjectsDTO.iD = rs.getLong("ID");
				project_tracker_subjectsDTO.nameEn = rs.getString("name_en");
				project_tracker_subjectsDTO.nameBn = rs.getString("name_bn");
				project_tracker_subjectsDTO.isDeleted = rs.getInt("isDeleted");
				project_tracker_subjectsDTO.lastModificationTime = rs.getLong("lastModificationTime");
				
				project_tracker_subjectsDTOList.add(project_tracker_subjectsDTO);
			}			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			try{ 
				if (connection != null){ 
					DBMR.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}

		return project_tracker_subjectsDTOList;
    }
	
	public List<Project_tracker_subjectsDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset)
	{
		return getDTOs(p_searchCriteria, limit, offset, tableName, true, 0);
	}
	
	public List<Project_tracker_subjectsDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, String tableName, boolean isPermanentTable, long userApprovalPathType)
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		List<Project_tracker_subjectsDTO> project_tracker_subjectsDTOList = new ArrayList<>();

		try{
			
			String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, tableName, isPermanentTable, userApprovalPathType);
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				Project_tracker_subjectsDTO project_tracker_subjectsDTO = new Project_tracker_subjectsDTO();
				project_tracker_subjectsDTO.iD = rs.getLong("ID");
				project_tracker_subjectsDTO.nameEn = rs.getString("name_en");
				project_tracker_subjectsDTO.nameBn = rs.getString("name_bn");
				project_tracker_subjectsDTO.isDeleted = rs.getInt("isDeleted");
				project_tracker_subjectsDTO.lastModificationTime = rs.getLong("lastModificationTime");
				
				project_tracker_subjectsDTOList.add(project_tracker_subjectsDTO);
			}					
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DBMR.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
		return project_tracker_subjectsDTOList;
	
	}

		
	
	public String getSqlWithSearchCriteria(Hashtable p_searchCriteria, int limit, int offset, int category, String tableName, boolean isPermanentTable, long userApprovalPathType)
    {
		Project_tracker_subjectsMAPS maps = new Project_tracker_subjectsMAPS(tableName);
		String joinSQL = "";
		return getSqlWithSearchCriteria(p_searchCriteria, limit, offset, category, tableName, isPermanentTable, userApprovalPathType, maps, joinSQL);
    }			
}
	