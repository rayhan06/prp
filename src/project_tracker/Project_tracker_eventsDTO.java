package project_tracker;
import java.util.*; 
import util.*; 


public class Project_tracker_eventsDTO extends CommonDTO
{

    public String nameEn = "";
    public String nameBn = "";
	
	
    @Override
	public String toString() {
            return "$Project_tracker_eventsDTO[" +
            " iD = " + iD +
            " nameEn = " + nameEn +
            " nameBn = " + nameBn +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}