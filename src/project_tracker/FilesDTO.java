package project_tracker;

public class FilesDTO {

	
	public long iD;
	public long rowId;
	public String fileType = "";
	public byte[] fileData;
	public String fileTag;
	public String fileTitle;
	public long createdAt;
	public int isDeleted;
}
