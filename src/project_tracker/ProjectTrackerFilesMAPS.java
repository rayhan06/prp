package project_tracker;
import java.util.*; 
import util.*;


public class ProjectTrackerFilesMAPS extends CommonMaps
{	
	public ProjectTrackerFilesMAPS(String tableName)
	{
		
		java_allfield_type_map.put("project_tracker_id".toLowerCase(), "Long");
		java_allfield_type_map.put("data_file".toLowerCase(), "String");

		java_anyfield_search_map.put(tableName + ".project_tracker_id".toLowerCase(), "Long");
		java_anyfield_search_map.put(tableName + ".data_file".toLowerCase(), "String");

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("projectTrackerId".toLowerCase(), "projectTrackerId".toLowerCase());
		java_DTO_map.put("dataFile".toLowerCase(), "dataFile".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("project_tracker_id".toLowerCase(), "projectTrackerId".toLowerCase());
		java_SQL_map.put("data_file".toLowerCase(), "dataFile".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Project Tracker Id".toLowerCase(), "projectTrackerId".toLowerCase());
		java_Text_map.put("Data File".toLowerCase(), "dataFile".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}