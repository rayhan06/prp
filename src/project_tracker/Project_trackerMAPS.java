package project_tracker;
import java.util.*; 
import util.*;


public class Project_trackerMAPS extends CommonMaps
{	
	public Project_trackerMAPS(String tableName)
	{
		
		java_allfield_type_map.put("project_tracker_events_type".toLowerCase(), "Long");
		java_allfield_type_map.put("project_tracker_events_text".toLowerCase(), "String");
		java_allfield_type_map.put("project_tracker_subjects_type".toLowerCase(), "Long");
		java_allfield_type_map.put("project_tracker_subjects_text".toLowerCase(), "String");
		java_allfield_type_map.put("event_location".toLowerCase(), "String");
		java_allfield_type_map.put("summary".toLowerCase(), "String");

		java_anyfield_search_map.put("project_tracker_events.name_en".toLowerCase(), "String");
		java_anyfield_search_map.put("project_tracker_events.name_bn".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".project_tracker_events_text".toLowerCase(), "String");
		java_anyfield_search_map.put("project_tracker_subjects.name_en".toLowerCase(), "String");
		java_anyfield_search_map.put("project_tracker_subjects.name_bn".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".project_tracker_subjects_text".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".event_location".toLowerCase(), "String");
		java_anyfield_search_map.put(tableName + ".summary".toLowerCase(), "String");

		java_DTO_map.put("iD".toLowerCase(), "iD".toLowerCase());
		java_DTO_map.put("projectTrackerEventsType".toLowerCase(), "projectTrackerEventsType".toLowerCase());
		java_DTO_map.put("projectTrackerEventsText".toLowerCase(), "projectTrackerEventsText".toLowerCase());
		java_DTO_map.put("projectTrackerSubjectsType".toLowerCase(), "projectTrackerSubjectsType".toLowerCase());
		java_DTO_map.put("projectTrackerSubjectsText".toLowerCase(), "projectTrackerSubjectsText".toLowerCase());
		java_DTO_map.put("eventLocation".toLowerCase(), "eventLocation".toLowerCase());
		java_DTO_map.put("summary".toLowerCase(), "summary".toLowerCase());
		java_DTO_map.put("isDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_DTO_map.put("lastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());

		java_SQL_map.put("project_tracker_events_type".toLowerCase(), "projectTrackerEventsType".toLowerCase());
		java_SQL_map.put("project_tracker_events_text".toLowerCase(), "projectTrackerEventsText".toLowerCase());
		java_SQL_map.put("project_tracker_subjects_type".toLowerCase(), "projectTrackerSubjectsType".toLowerCase());
		java_SQL_map.put("project_tracker_subjects_text".toLowerCase(), "projectTrackerSubjectsText".toLowerCase());
		java_SQL_map.put("event_location".toLowerCase(), "eventLocation".toLowerCase());
		java_SQL_map.put("summary".toLowerCase(), "summary".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Project Tracker Events".toLowerCase(), "projectTrackerEventsType".toLowerCase());
		java_Text_map.put("Project Tracker Events Text".toLowerCase(), "projectTrackerEventsText".toLowerCase());
		java_Text_map.put("Project Tracker Subjects".toLowerCase(), "projectTrackerSubjectsType".toLowerCase());
		java_Text_map.put("Project Tracker Subjects Text".toLowerCase(), "projectTrackerSubjectsText".toLowerCase());
		java_Text_map.put("Event Location".toLowerCase(), "eventLocation".toLowerCase());
		java_Text_map.put("Summary".toLowerCase(), "summary".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}