package project_tracker;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;


import org.apache.log4j.Logger;

import dbm.*;

import repository.RepositoryManager;

import util.*;
import pb.*;
import approval_module_map.*;

public class Project_trackerDAO  extends NavigationService3
{
	
	Logger logger = Logger.getLogger(getClass());

	
	public Project_trackerDAO(String tableName, String tempTableName, Approval_module_mapDTO approval_module_mapDTO)
	{
		super(tableName, tempTableName, approval_module_mapDTO);		
	}
	
//	public long add(CommonDTO commonDTO,  String tableName, TempTableDTO tempTableDTO) throws Exception
//	{
//		return 0;
//	}
	
	
	
	public long add(CommonDTO commonDTO, String tableName, TempTableDTO tempTableDTO) throws Exception
	{
		
		Project_trackerDTO project_trackerDTO = (Project_trackerDTO)commonDTO;
		
		Connection connection = null;
		PreparedStatement ps = null;

		long lastModificationTime = System.currentTimeMillis();	

		try{
			connection = DBMW.getInstance().getConnection();
			
			if(connection == null)
			{
				System.out.println("nullconn");
			}

			project_trackerDTO.iD = DBMW.getInstance().getNextSequenceId(tableName);

			String sql = "INSERT INTO " + tableName;
			
			sql += " (";
			sql += "ID";
			sql += ", ";
			sql += "project_tracker_events_type";
			sql += ", ";
			sql += "project_tracker_events_text";
			sql += ", ";
			sql += "project_tracker_subjects_type";
			sql += ", ";
			sql += "project_tracker_subjects_text";
			sql += ", ";
			sql += "event_location";
			sql += ", ";
			sql += "summary";
			sql += ", ";
			sql += "isDeleted";
			sql += ", ";
			sql += "lastModificationTime";
			if(tempTableDTO!=null)
			{
				sql += ", permanent_table_id, operation_type, approval_path_type, approval_order";
			}
			sql += ")";
			
			
            sql += " VALUES(";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			if(tempTableDTO!=null)
			{
				sql += ", " + tempTableDTO.permanent_table_id + ", " + tempTableDTO.operation_type + ", " + tempTableDTO.approval_path_type + ", " + tempTableDTO.approval_order;
			}
			sql += ")";
				
			//printSql(sql);

			ps = connection.prepareStatement(sql);
			
			
			

			int index = 1;

			ps.setObject(index++,project_trackerDTO.iD);
			ps.setObject(index++,project_trackerDTO.projectTrackerEventsType);
			ps.setObject(index++,project_trackerDTO.projectTrackerEventsText);
			ps.setObject(index++,project_trackerDTO.projectTrackerSubjectsType);
			ps.setObject(index++,project_trackerDTO.projectTrackerSubjectsText);
			ps.setObject(index++,project_trackerDTO.eventLocation);
			ps.setObject(index++,project_trackerDTO.summary);
			ps.setObject(index++,project_trackerDTO.isDeleted);
			ps.setObject(index++, lastModificationTime);
			
			//System.out.println(ps);
			ps.execute();
			
			
			
			
			recordUpdateTime(connection, lastModificationTime, tableName);
			
			ps.close();
			
		 FilesDAO tempFileDao = new FilesDAO();
		 tempFileDao.insertFiles(project_trackerDTO.files, "project_tracker",project_trackerDTO.iD,connection);

		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if (ps != null) {
					ps.close();
				}
			} catch(Exception e){}
			try{
				if(connection != null){
					DBMW.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		return project_trackerDTO.iD;		
	}
		
	

	//need another getter for repository
	public Project_trackerDTO getDTOByID (long ID, String tableName) throws Exception
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Project_trackerDTO project_trackerDTO = null;
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
			
            sql += " WHERE ID=" + ID;
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			if(rs.next()){
				project_trackerDTO = new Project_trackerDTO();

				project_trackerDTO.iD = rs.getLong("ID");
				project_trackerDTO.projectTrackerEventsType = rs.getLong("project_tracker_events_type");
				project_trackerDTO.projectTrackerEventsText = rs.getString("project_tracker_events_text");
				project_trackerDTO.projectTrackerSubjectsType = rs.getLong("project_tracker_subjects_type");
				project_trackerDTO.projectTrackerSubjectsText = rs.getString("project_tracker_subjects_text");
				project_trackerDTO.eventLocation = rs.getString("event_location");
				project_trackerDTO.summary = rs.getString("summary");
				project_trackerDTO.isDeleted = rs.getInt("isDeleted");
				project_trackerDTO.lastModificationTime = rs.getLong("lastModificationTime");

			}			
			
			
			
			//ProjectTrackerFilesDAO projectTrackerFilesDAO = new ProjectTrackerFilesDAO("project_tracker_files",  "", null);			
		//	List<ProjectTrackerFilesDTO> projectTrackerFilesDTOList = projectTrackerFilesDAO.getProjectTrackerFilesDTOListByProjectTrackerID(project_trackerDTO.iD);
			//project_trackerDTO.projectTrackerFilesDTOList = projectTrackerFilesDTOList;
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DBMR.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
		return project_trackerDTO;
	}
	
	
	
	
	public long update(CommonDTO commonDTO, String tableName) throws Exception
	{		
		Project_trackerDTO project_trackerDTO = (Project_trackerDTO)commonDTO;
		
		Connection connection = null;
		PreparedStatement ps = null;

		long lastModificationTime = System.currentTimeMillis();	
		try{
			connection = DBMW.getInstance().getConnection();

			String sql = "UPDATE " + tableName;
			
			sql += " SET ";
			sql += "project_tracker_events_type=?";
			sql += ", ";
			sql += "project_tracker_events_text=?";
			sql += ", ";
			sql += "project_tracker_subjects_type=?";
			sql += ", ";
			sql += "project_tracker_subjects_text=?";
			sql += ", ";
			sql += "event_location=?";
			sql += ", ";
			sql += "summary=?";
			sql += ", lastModificationTime = "	+ lastModificationTime + "";
            sql += " WHERE ID = " + project_trackerDTO.iD;
				
			printSql(sql);

			ps = connection.prepareStatement(sql);
			
			

			int index = 1;
			
			ps.setObject(index++,project_trackerDTO.projectTrackerEventsType);
			ps.setObject(index++,project_trackerDTO.projectTrackerEventsText);
			ps.setObject(index++,project_trackerDTO.projectTrackerSubjectsType);
			ps.setObject(index++,project_trackerDTO.projectTrackerSubjectsText);
			ps.setObject(index++,project_trackerDTO.eventLocation);
			ps.setObject(index++,project_trackerDTO.summary);
			System.out.println(ps);
			ps.executeUpdate();
			


			
			recordUpdateTime(connection,  lastModificationTime, tableName);
			
			ps.close();
			
			 FilesDAO tempFileDao = new FilesDAO();
			 tempFileDao.insertFiles(project_trackerDTO.files,"project_tracker", project_trackerDTO.iD, connection);

						
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if (ps != null) {
					ps.close();
				}
			} catch(Exception e){}
			try{
				if(connection != null){
					DBMW.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		return project_trackerDTO.iD;
	}
	
	public List<Project_trackerDTO> getDTOs(Collection recordIDs)
	{
		return getDTOs(recordIDs, tableName);
	}
	
	public List<Project_trackerDTO> getDTOs(Collection recordIDs, String tableName){
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		Project_trackerDTO project_trackerDTO = null;
		List<Project_trackerDTO> project_trackerDTOList = new ArrayList<>();
		if(recordIDs.isEmpty()){
			return project_trackerDTOList;
		}
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
            
            sql += " WHERE ID IN ( ";

			for(int i = 0;i<recordIDs.size();i++){
				if(i!=0){
					sql+=",";
				}
				sql+=((ArrayList)recordIDs).get(i);
			}
			sql+=")  order by lastModificationTime desc";
			
			printSql(sql);
			
			logger.debug("sql " + sql);
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				project_trackerDTO = new Project_trackerDTO();
				project_trackerDTO.iD = rs.getLong("ID");
				project_trackerDTO.projectTrackerEventsType = rs.getLong("project_tracker_events_type");
				project_trackerDTO.projectTrackerEventsText = rs.getString("project_tracker_events_text");
				project_trackerDTO.projectTrackerSubjectsType = rs.getLong("project_tracker_subjects_type");
				project_trackerDTO.projectTrackerSubjectsText = rs.getString("project_tracker_subjects_text");
				project_trackerDTO.eventLocation = rs.getString("event_location");
				project_trackerDTO.summary = rs.getString("summary");
				project_trackerDTO.isDeleted = rs.getInt("isDeleted");
				project_trackerDTO.lastModificationTime = rs.getLong("lastModificationTime");
				System.out.println("got this DTO: " + project_trackerDTO);
				
				project_trackerDTOList.add(project_trackerDTO);

			}			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DBMR.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
		return project_trackerDTOList;
	
	}
	
	

	
	
	
	//add repository
	public List<Project_trackerDTO> getAllProject_tracker (boolean isFirstReload)
    {
		List<Project_trackerDTO> project_trackerDTOList = new ArrayList<>();

		String sql = "SELECT * FROM project_tracker";
		sql += " WHERE ";
	

		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by project_tracker.lastModificationTime desc";
		printSql(sql);
		
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;

		
		try{
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();
			rs = stmt.executeQuery(sql);
			

			while(rs.next()){
				Project_trackerDTO project_trackerDTO = new Project_trackerDTO();
				project_trackerDTO.iD = rs.getLong("ID");
				project_trackerDTO.projectTrackerEventsType = rs.getLong("project_tracker_events_type");
				project_trackerDTO.projectTrackerEventsText = rs.getString("project_tracker_events_text");
				project_trackerDTO.projectTrackerSubjectsType = rs.getLong("project_tracker_subjects_type");
				project_trackerDTO.projectTrackerSubjectsText = rs.getString("project_tracker_subjects_text");
				project_trackerDTO.eventLocation = rs.getString("event_location");
				project_trackerDTO.summary = rs.getString("summary");
				project_trackerDTO.isDeleted = rs.getInt("isDeleted");
				project_trackerDTO.lastModificationTime = rs.getLong("lastModificationTime");
				
				project_trackerDTOList.add(project_trackerDTO);
			}			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			try{ 
				if (connection != null){ 
					DBMR.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}

		return project_trackerDTOList;
    }
	
	public List<Project_trackerDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset)
	{
		return getDTOs(p_searchCriteria, limit, offset, tableName, true, 0);
	}
	
	public List<Project_trackerDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, String tableName, boolean isPermanentTable, long userApprovalPathType)
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		List<Project_trackerDTO> project_trackerDTOList = new ArrayList<>();

		try{
			
			String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, tableName, isPermanentTable, userApprovalPathType);
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				Project_trackerDTO project_trackerDTO = new Project_trackerDTO();
				project_trackerDTO.iD = rs.getLong("ID");
				project_trackerDTO.projectTrackerEventsType = rs.getLong("project_tracker_events_type");
				project_trackerDTO.projectTrackerEventsText = rs.getString("project_tracker_events_text");
				project_trackerDTO.projectTrackerSubjectsType = rs.getLong("project_tracker_subjects_type");
				project_trackerDTO.projectTrackerSubjectsText = rs.getString("project_tracker_subjects_text");
				project_trackerDTO.eventLocation = rs.getString("event_location");
				project_trackerDTO.summary = rs.getString("summary");
				project_trackerDTO.isDeleted = rs.getInt("isDeleted");
				project_trackerDTO.lastModificationTime = rs.getLong("lastModificationTime");
				
				project_trackerDTOList.add(project_trackerDTO);
			}					
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DBMR.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
		return project_trackerDTOList;
	
	}

		
	
	public String getSqlWithSearchCriteria(Hashtable p_searchCriteria, int limit, int offset, int category, String tableName, boolean isPermanentTable, long userApprovalPathType)
    {
		Project_trackerMAPS maps = new Project_trackerMAPS(tableName);
		String joinSQL = "";
		joinSQL += " join project_tracker_events on " + tableName + ".project_tracker_events_type = project_tracker_events.ID ";
		joinSQL += " join project_tracker_subjects on " + tableName + ".project_tracker_subjects_type = project_tracker_subjects.ID ";
		return getSqlWithSearchCriteria(p_searchCriteria, limit, offset, category, tableName, isPermanentTable, userApprovalPathType, maps, joinSQL);
    }			
}
	