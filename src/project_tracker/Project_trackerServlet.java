package project_tracker;

import java.io.IOException;
import java.io.*;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import org.apache.log4j.Logger;

import login.LoginDTO;
import permission.MenuConstants;
import role.PermissionRepository;


import sessionmanager.SessionConstants;

import user.UserDTO;
import user.UserRepository;

import util.RecordNavigationManager3;
import util.RootstockUtils;

import java.util.*;
import javax.servlet.http.*;

import project_tracker.*;
import approval_module_map.*;


import com.google.gson.Gson;

import pb.*;
import pbReport.*;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import language.LM;
import util.CommonConstant;



/**
 * Servlet implementation class Project_trackerServlet
 */
@WebServlet("/Project_trackerServlet")
@MultipartConfig
public class Project_trackerServlet extends HttpServlet 
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Project_trackerServlet.class);
	Approval_module_mapDAO approval_module_mapDAO = new Approval_module_mapDAO();
    Approval_module_mapDTO approval_module_mapDTO;
    String tableName = "project_tracker";
    String tempTableName = "project_tracker_temp";
	Project_trackerDAO project_trackerDAO;
	FilesDAO projectTrackerFilesDAO;
	
	Project_tracker_eventsDTO project_tracker_eventsDTO = null;
	
	Project_tracker_subjectsDTO project_tracker_subjectsDTO = null;
	
	long insertId = 0;
	
Project_tracker_eventsDAO project_tracker_eventsDAO;
	
	Project_tracker_subjectsDAO project_tracker_subjectsDAO;
    private final Gson gson = new Gson();
    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Project_trackerServlet() 
	{
        super();
    	try
    	{
			approval_module_mapDTO = approval_module_mapDAO.getApproval_module_mapDTOByTableName("project_tracker");
			project_trackerDAO = new Project_trackerDAO(tableName, tempTableName, approval_module_mapDTO);
			projectTrackerFilesDAO = new FilesDAO();
			
             project_tracker_eventsDAO = new Project_tracker_eventsDAO("project_tracker_events","",null);
			
			project_tracker_subjectsDAO = new Project_tracker_subjectsDAO("project_tracker_subjects","",null);
			
			project_tracker_eventsDTO = new Project_tracker_eventsDTO();
			
			project_tracker_subjectsDTO = new Project_tracker_subjectsDTO();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }   

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		System.out.println("In doget request = " + request);
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		try
		{
			String actionType = request.getParameter("actionType");
			if(actionType.equals("getAddPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PROJECT_TRACKER_ADD))
				{
					getAddPage(request, response);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getEditPage"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PROJECT_TRACKER_UPDATE))
				{
					if(isPermanentTable)
					{
						getProject_tracker(request, response, tableName);
					}
					else
					{
						getProject_tracker(request, response, tempTableName);
					}
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}						
			}
			else if(actionType.equals("getURL"))
			{
				String URL = request.getParameter("URL");
				System.out.println("URL = " + URL);
				response.sendRedirect(URL);			
			}
			
			else if(actionType.equals("getFile"))
			{
				String tempId = request.getParameter("id");
				//System.out.println("URL = " + URL);
				//response.sendRedirect(URL);
				ProcessFile(tempId, request, response);
			}
			else if(actionType.equals("search"))
			{
				System.out.println("search requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PROJECT_TRACKER_SEARCH))
				{
					
					if(isPermanentTable)
					{
						searchProject_tracker(request, response, tableName, isPermanentTable);
					}
					else
					{
						searchProject_tracker(request, response, tempTableName, isPermanentTable);
					}
				}			
			}
			else if(actionType.equals("getApprovalPage"))
			{
				System.out.println("getApprovalPage requested");
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PROJECT_TRACKER_SEARCH))
				{
					searchProject_tracker(request, response, tempTableName, false);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}				
			}
			else
			{
				request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}
	
	private void ProcessFile(String fileId,HttpServletRequest request, HttpServletResponse response)
	{
		
		
		long tempId = Long.parseLong(fileId);
		FilesDAO tempDao = new FilesDAO();
		
		FilesDTO tempObj = tempDao.getFileByID(tempId);
				
		
				String mimeType = "application/octet-stream";
               // if (mimeType == null) {        
              //      mimeType = "application/octet-stream";
              //  }              
                 
                // set content properties and header attributes for the response
                response.setContentType(mimeType);
                response.setContentLength(tempObj.fileData.length);
                String headerKey = "Content-Disposition";
                String headerValue = String.format("attachment; filename=\"%s\"", tempObj.fileTitle);
                response.setHeader(headerKey, headerValue);
 
                try
                {
                // writes the file to the client
                OutputStream outStream = response.getOutputStream();
                
                outStream.write(tempObj.fileData);
                 
//                byte[] buffer = new byte[BUFFER_SIZE];
//                int bytesRead = -1;
//                 
//                while ((bytesRead = inputStream.read(buffer)) != -1) {
//                    outStream.write(buffer, 0, bytesRead);
//                }
                 
               // inputStream.close();
                outStream.close();    
                
                }
                catch(Exception ex)
                {
                	
                }
			
	}

	private void getAddPage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		request.setAttribute("ID", -1L);
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("project_tracker/project_trackerEdit.jsp");
		requestDispatcher.forward(request, response);
	}
	private String getFileName(final Part part) 
	{
	    final String partHeader = part.getHeader("content-disposition");
	    System.out.println("Part Header = {0}" +  partHeader);
	    for (String content : part.getHeader("content-disposition").split(";")) {
	        if (content.trim().startsWith("filename")) {
	            return content.substring(
	                    content.indexOf('=') + 1).trim().replace("\"", "");
	        }
	    }
	    return null;
	}

	
	

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		// TODO Auto-generated method stub
		LoginDTO loginDTO = (LoginDTO)request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		System.out.println("doPost");
		boolean isPermanentTable = true;
		if(request.getParameter("isPermanentTable") != null)
		{
			isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
		}
		System.out.println("In servlet, isPermanentTable = " + isPermanentTable);
		
		try
		{
			String actionType = request.getParameter("actionType");
			System.out.println("actionType = " + actionType);
			if(actionType.equals("add"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PROJECT_TRACKER_ADD))
				{
					System.out.println("going to  addProject_tracker ");
					addProject_tracker(request, response, true, userDTO, tableName, true);
				}
				else
				{
					System.out.println("Not going to  addProject_tracker ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			if(actionType.equals("approve"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PROJECT_TRACKER_ADD))
				{					
					approveProject_tracker(request, response, true, userDTO);
				}
				else
				{
					System.out.println("Not going to  addProject_tracker ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			if(actionType.equals("getDTO"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PROJECT_TRACKER_ADD))
				{
					if(isPermanentTable)
					{
						getDTO(request, response, tableName);
					}
					else
					{
						getDTO(request, response, tempTableName);
					}
				}
				else
				{
					System.out.println("Not going to  addProject_tracker ");
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
				
			}
			else if(actionType.equals("edit"))
			{
				
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PROJECT_TRACKER_UPDATE))
				{
					if(isPermanentTable)
					{
						addProject_tracker(request, response, false, userDTO, tableName, isPermanentTable);
					}
					else
					{
						addProject_tracker(request, response, false, userDTO, tempTableName, isPermanentTable);
					}
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("delete"))
			{								
				deleteProject_tracker(request, response, userDTO, isPermanentTable);				
			}	
			else if(actionType.equals("search"))
			{
				if(PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.PROJECT_TRACKER_SEARCH))
				{
					searchProject_tracker(request, response, tableName, true);
				}
				else
				{
					request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
				}
			}
			else if(actionType.equals("getGeo"))
			{
				System.out.println("going to geoloc ");
				request.getRequestDispatcher("geolocation/geoloc.jsp").forward(request, response);
			}
			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.debug(ex);
		}
	}
	
	private void getDTO(HttpServletRequest request, HttpServletResponse response, String tableName) 
	{
		try 
		{
			System.out.println("In getDTO");
			Project_trackerDTO project_trackerDTO = project_trackerDAO.getDTOByID(Long.parseLong(request.getParameter("ID")), tableName);
			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			
			String encoded = this.gson.toJson(project_trackerDTO);
			System.out.println("json encoded data = " + encoded);
			out.print(encoded);
			out.flush();
		}
		catch (NumberFormatException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	private void approveProject_tracker(HttpServletRequest request, HttpServletResponse response, boolean b, UserDTO userDTO) 
	{
		try
		{
			long id = Long.parseLong(request.getParameter("idToApprove"));
			Project_trackerDTO project_trackerDTO = project_trackerDAO.getDTOByID(id, tempTableName);
			project_trackerDAO.manageWriteOperations(project_trackerDTO, SessionConstants.APPROVE, id, userDTO);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
	}
	private void addProject_tracker(HttpServletRequest request, HttpServletResponse response, Boolean addFlag, UserDTO userDTO, String tableName, boolean isPermanentTable) throws IOException 
	{
		// TODO Auto-generated method stub
		try 
		{
			int tempLanguage = LM.getLanguageIDByUserDTO(userDTO);
			request.setAttribute("failureMessage", "");
			System.out.println("%%%% addProject_tracker");
			String path = getServletContext().getRealPath("/img2/");
			Project_trackerDTO project_trackerDTO;
			Project_tracker_eventsDTO project_tracker_eventsDTO;
			Project_tracker_subjectsDTO project_tracker_subjectsDTO;
			String FileNamePrefix;
			
project_tracker_eventsDTO = new Project_tracker_eventsDTO();
			
			project_tracker_subjectsDTO = new Project_tracker_subjectsDTO();
			if(addFlag == true)
			{
				project_trackerDTO = new Project_trackerDTO();
				FileNamePrefix = UUID.randomUUID().toString().substring(0, 10);
			}
			else
			{
				project_trackerDTO = project_trackerDAO.getDTOByID(Long.parseLong(request.getParameter("identity")), tableName);
				FileNamePrefix = request.getParameter("identity");
			}
			
			String Value = "";
			Value = request.getParameter("projectTrackerEventsType");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			//System.out.println("projectTrackerEventsType = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				project_trackerDTO.projectTrackerEventsType = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			Value = request.getParameter("projectTrackerEventsText");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			//System.out.println("projectTrackerEventsText = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				project_trackerDTO.projectTrackerEventsText = (Value);
				
				if(tempLanguage == CommonConstant.Language_ID_English)
				{
					project_tracker_eventsDTO.nameEn = (Value);
				}
				else
				{
					project_tracker_eventsDTO.nameBn = (Value);
				}
				
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			Value = request.getParameter("projectTrackerSubjectsType");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			//System.out.println("projectTrackerSubjectsType = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				project_trackerDTO.projectTrackerSubjectsType = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			Value = request.getParameter("projectTrackerSubjectsText");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("projectTrackerSubjectsText = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				project_trackerDTO.projectTrackerSubjectsText = (Value);
				
				if(tempLanguage == CommonConstant.Language_ID_English)
				{
					project_tracker_subjectsDTO.nameEn = (Value);
				}
				else
				{
					project_tracker_subjectsDTO.nameBn = (Value);
				}
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			Value = request.getParameter("eventLocation");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			//System.out.println("eventLocation = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				project_trackerDTO.eventLocation = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			Value = request.getParameter("summary");
			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			//System.out.println("summary = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				project_trackerDTO.summary = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}
			
			System.out.println("Done adding  addProject_tracker dto = " + project_trackerDTO);
			
			if(addFlag == true)
			{								
				List<FilesDTO> FilesDTOList = createProjectTrackerFilesDTOListByRequest(request,addFlag);	
				
				project_trackerDTO.files = FilesDTOList;
				
				project_trackerDAO.manageWriteOperations(project_trackerDTO, SessionConstants.INSERT, -1, userDTO);				
				
			}
			else
			{		
				
				List<FilesDTO> FilesDTOList = createProjectTrackerFilesDTOListByRequest(request,addFlag);	
				
				project_trackerDTO.files = FilesDTOList;
					project_trackerDAO.manageWriteOperations(project_trackerDTO, SessionConstants.UPDATE, -1, userDTO);
							
			}
			
			
			
//			if(addFlag==true)
//			{
//			
//			}
			
			
			
			if(project_tracker_eventsDTO.nameBn.isEmpty() && project_tracker_eventsDTO.nameEn.isEmpty())
			{
				
			}
			else
			{
				project_tracker_eventsDAO.add(project_tracker_eventsDTO);
			}
			
			if(project_tracker_subjectsDTO.nameBn.isEmpty() && project_tracker_subjectsDTO.nameEn.isEmpty())
			{
				
			}
			else
			{
				project_tracker_subjectsDAO.add(project_tracker_subjectsDTO);
			}
			
			
			
			
			
			String inPlaceSubmit = request.getParameter("inplacesubmit");
			
			if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
			{
				getProject_tracker(request, response, tableName);
			}
			else
			{
				response.sendRedirect("Project_trackerServlet?actionType=search");
			}
					
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	



	private List<FilesDTO> createProjectTrackerFilesDTOListByRequest(HttpServletRequest request,boolean addFlag) throws Exception{ 
		List<FilesDTO> projectTrackerFilesDTOList = new ArrayList<FilesDTO>();
		if(request.getParameterValues("projectTrackerFiles.iD") != null)
		{
			int projectTrackerFilesItemNo = request.getParameterValues("projectTrackerFiles.iD").length;
			
			
			for(int index=0;index<projectTrackerFilesItemNo;index++){
				FilesDTO projectTrackerFilesDTO = createProjectTrackerFilesDTOByRequestAndIndex(request,addFlag,index);
				
				projectTrackerFilesDTO.rowId = insertId;
				
				projectTrackerFilesDTOList.add(projectTrackerFilesDTO);
			}
		}	
		
		
		if(request.getParameterValues("projectTrackerFiles.titleedit.id") != null)
		{
			int projectTrackerFilesItemNo = request.getParameterValues("projectTrackerFiles.titleedit.id").length;
			
			
			for(int index=0;index<projectTrackerFilesItemNo;index++){
				FilesDTO projectTrackerFilesDTO2 = createProjectTrackerFilesDTOByRequestAndIndex2(request,addFlag,index);
				
				
			}
		}
		
		return projectTrackerFilesDTOList;
	}
	
	
	private FilesDTO createProjectTrackerFilesDTOByRequestAndIndex(HttpServletRequest request, boolean addFlag,int index) throws Exception{
	
		
		FilesDTO projectTrackerFilesDTO = new FilesDTO();
		String path = getServletContext().getRealPath("/img2/");		
				
		String Value = "";
		Value = request.getParameterValues("projectTrackerFiles.projectTrackerId")[index];

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		
		String ValueTitle = "";
		ValueTitle = request.getParameterValues("projectTrackerFiles.title")[index];

		if(ValueTitle != null)
		{
			ValueTitle = Jsoup.clean(ValueTitle,Whitelist.simpleText());
		}
		
		String ValueTag = "";
		ValueTag = request.getParameterValues("projectTrackerFiles.tag")[index];

		if(ValueTag != null)
		{
			ValueTag = Jsoup.clean(ValueTag,Whitelist.simpleText());
		}

		//projectTrackerFilesDTO.rowId = insertId;//Long.parseLong(request.getParameterValues("projectTrackerFiles.projectTrackerId")[index]);
		
		Part part = RootstockUtils.uploadFileByIndex2("projectTrackerFiles.dataFile", path, index, request);	
		
		String name = part.getName();
		
		String tempExtention = RootstockUtils.getFileExtension(name);
		//String name = part.getName();
		
		String tempName = "";
		
		if(ValueTitle.trim().length() > 0)
		{
			tempName = ValueTitle + "." + tempExtention;
		}
		else
		{
			tempName = name;
		}
		
		projectTrackerFilesDTO.fileTag = ValueTag;
		
		projectTrackerFilesDTO.fileTitle = tempName;
		
		projectTrackerFilesDTO.fileType = part.getContentType();

		projectTrackerFilesDTO.fileData = Utils.uploadFileToByteAray(part);
		
		// update tag, title
		
		
		
		String ValueTitleEdit = "";
		
		if(request.getParameterValues("projectTrackerFiles.titleedit") != null)
		{
			ValueTitleEdit = request.getParameterValues("projectTrackerFiles.titleedit")[index];
	
			if(ValueTitleEdit != null)
			{
				ValueTitleEdit = Jsoup.clean(ValueTitleEdit,Whitelist.simpleText());
			}
		
		}
		
		String ValueTagEdit = "";
		
		if(request.getParameterValues("projectTrackerFiles.tagedit") != null)
		{
			ValueTagEdit = request.getParameterValues("projectTrackerFiles.tagedit")[index];
	
			if(ValueTagEdit != null)
			{
				ValueTagEdit = Jsoup.clean(ValueTagEdit,Whitelist.simpleText());
			}
		}
		
		String ValueIDEdit = "";
		
		if(request.getParameterValues("projectTrackerFiles.titleedit.id") != null)
		{
			ValueIDEdit = request.getParameterValues("projectTrackerFiles.titleedit.id")[index];
	
			if(ValueIDEdit != null)
			{
				ValueIDEdit = Jsoup.clean(ValueIDEdit,Whitelist.simpleText());
				
				FilesDTO tempDTO = new FilesDTO();
				
				tempDTO.iD = Long.parseLong(ValueIDEdit);
				
				tempDTO.fileTag = ValueTagEdit;
				
				tempDTO.fileTitle = ValueTitleEdit;
				
				FilesDAO tempDao = new FilesDAO();
				
				tempDao.UpdateFileTileTag(tempDTO);
				
				
			}
		
		}
		
		
		
		
		
		
		
		return projectTrackerFilesDTO;
	
	}
	
private FilesDTO createProjectTrackerFilesDTOByRequestAndIndex2(HttpServletRequest request, boolean addFlag,int index) throws Exception{
	
				
		String ValueTitleEdit = "";
		ValueTitleEdit = request.getParameterValues("projectTrackerFiles.titleedit")[index];

		if(ValueTitleEdit != null)
		{
			ValueTitleEdit = Jsoup.clean(ValueTitleEdit,Whitelist.simpleText());
		}
		
		String ValueTagEdit = "";
		ValueTagEdit = request.getParameterValues("projectTrackerFiles.tagedit")[index];

		if(ValueTagEdit != null)
		{
			ValueTagEdit = Jsoup.clean(ValueTagEdit,Whitelist.simpleText());
		}
		
		String ValueIDEdit = "";
		ValueIDEdit = request.getParameterValues("projectTrackerFiles.titleedit.id")[index];

		if(ValueIDEdit != null)
		{
			ValueIDEdit = Jsoup.clean(ValueIDEdit,Whitelist.simpleText());
			
			FilesDTO tempDTO = new FilesDTO();
			
			tempDTO.iD = Long.parseLong(ValueIDEdit);
			
			tempDTO.fileTag = ValueTagEdit;
			
			tempDTO.fileTitle = ValueTitleEdit;
			
			FilesDAO tempDao = new FilesDAO();
			
			tempDao.UpdateFileTileTag(tempDTO);
			
			
		}	
		
		return null;
	
	}
	
	
	
	
	

	private void deleteProject_tracker(HttpServletRequest request, HttpServletResponse response, UserDTO userDTO, boolean deleteOrReject) throws IOException 
	{				
		try 
		{
			String[] IDsToDelete = request.getParameterValues("ID");
			for(int i = 0; i < IDsToDelete.length; i ++)
			{
				long id = Long.parseLong(IDsToDelete[i]);
				System.out.println("------ DELETING " + IDsToDelete[i]);
				
				if(deleteOrReject)
				{
					Project_trackerDTO project_trackerDTO = (Project_trackerDTO)project_trackerDAO.getDTOByID(id);
					project_trackerDAO.manageWriteOperations(project_trackerDTO, SessionConstants.DELETE, id, userDTO);
					FilesDAO tempFileDao = new FilesDAO();
					
					List<FilesDTO> tempList5 = tempFileDao.getAllFilesIDs("project_tracker",id);
					
					if(tempList5 != null)
					{
					
						for(int k=0; k < tempList5.size(); k++)
						{
							tempFileDao.deleteFile(tempList5.get(k).iD);
						}
					}
					
					
					//tempFileDao.deleteFile(id);
					
					response.sendRedirect("Project_trackerServlet?actionType=search");
				}
				else
				{
					Project_trackerDTO project_trackerDTO = project_trackerDAO.getDTOByID(id, tempTableName);
					project_trackerDAO.manageWriteOperations(project_trackerDTO, SessionConstants.REJECT, id, userDTO);
					response.sendRedirect("Project_trackerServlet?actionType=getApprovalPage");
				}
			}			
		}
		catch (Exception ex) 
		{
			ex.printStackTrace();
		}
		
	}

	private void getProject_tracker(HttpServletRequest request, HttpServletResponse response, String tableName) throws ServletException, IOException
	{
		System.out.println("in getProject_tracker");
		Project_trackerDTO project_trackerDTO = null;
		try 
		{
			project_trackerDTO = project_trackerDAO.getDTOByID(Long.parseLong(request.getParameter("ID")), tableName);
			
			FilesDAO tempDao = new FilesDAO();
			
			 List<FilesDTO> tempList = tempDao.getAllFiles("project_tracker", project_trackerDTO.iD);
			 
			 logger.debug("sql 11 : " + tempList.size());
			 
			 project_trackerDTO.files = tempList;
			 
			 request.getSession().removeAttribute("fileObj");
			 
			 request.getSession().setAttribute("fileObj",project_trackerDTO );
			
			request.setAttribute("ID", project_trackerDTO.iD);
			request.setAttribute("project_trackerDTO",project_trackerDTO);
			request.setAttribute("project_trackerDAO",project_trackerDAO);
			request.setAttribute("projectTrackerFilesDAO",projectTrackerFilesDAO);
			
			String URL= "";
			
			//String inPlaceEdit = (String)request.getParameter("inplaceedit");
		//	String inPlaceSubmit = (String)request.getParameter("inplacesubmit");
			String getBodyOnly = request.getParameter("getBodyOnly");
			
//			if(inPlaceEdit != null && !inPlaceEdit.equalsIgnoreCase(""))
//			{
//				URL = "project_tracker/project_trackerInPlaceEdit.jsp";	
//				request.setAttribute("inplaceedit","");				
//			}
//			else if(inPlaceSubmit != null && !inPlaceSubmit.equalsIgnoreCase(""))
//			{
//				URL = "project_tracker/project_trackerSearchRow.jsp";
//				request.setAttribute("inplacesubmit","");					
//			}
//			else
//			{
//				if(getBodyOnly  != null && !getBodyOnly.equalsIgnoreCase(""))
//				{
//					URL = "project_tracker/project_trackerEditBody.jsp?actionType=edit";
//				}
//				else
//				{
//					URL = "project_tracker/project_trackerEdit.jsp?actionType=edit";
//				}				
			//}
			
			URL = "project_tracker/project_trackerEdit.jsp?actionType=edit";
			
			RequestDispatcher rd = request.getRequestDispatcher(URL);
			rd.forward(request, response);
		}
		catch (NumberFormatException e) 
		{
			e.printStackTrace();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	private void searchProject_tracker(HttpServletRequest request, HttpServletResponse response, String tableName, boolean isPermanent) throws ServletException, IOException
	{
		System.out.println("in  searchProject_tracker 1");
		LoginDTO loginDTO = (LoginDTO)request.getSession(true).getAttribute( SessionConstants.USER_LOGIN );
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		String ajax = request.getParameter("ajax");
		boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
        System.out.println("ajax = " + ajax + " hasajax = " + hasAjax);
		
        RecordNavigationManager3 rnManager = new RecordNavigationManager3(
			SessionConstants.NAV_PROJECT_TRACKER,
			request,
			project_trackerDAO,
			SessionConstants.VIEW_PROJECT_TRACKER,
			SessionConstants.SEARCH_PROJECT_TRACKER,
			tableName,
			isPermanent,
			userDTO.approvalPathID);
        try
        {
			System.out.println("trying to dojob");
            rnManager.doJob(loginDTO);
        }
        catch(Exception e)
        {
			System.out.println("failed to dojob" + e);
        }

		request.setAttribute("project_trackerDAO",project_trackerDAO);
		request.setAttribute("projectTrackerFilesDAO",projectTrackerFilesDAO);
		//request.setAttribute("project_tracker_subjectsDAO",project_tracker_subjectsDAO);
		// project_tracker_eventsDAO = new Project_tracker_eventsDAO("project_tracker_events","",null);
			
		//	project_tracker_subjectsDAO = new Project_tracker_subjectsDAO("project_tracker_subjects","",null);
        RequestDispatcher rd;
        if(hasAjax == false)
        {
        	System.out.println("Going to project_tracker/project_trackerSearch.jsp");
        	rd = request.getRequestDispatcher("project_tracker/project_trackerSearch.jsp");
        }
        else
        {
        	System.out.println("Going to project_tracker/project_trackerSearchForm.jsp");
        	rd = request.getRequestDispatcher("project_tracker/project_trackerSearchForm.jsp");
        }
		rd.forward(request, response);
	}
	
}

