package project_tracker;

import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import dbm.DBMR;
import dbm.DBMW;

public class FilesDAO {
	
	Logger logger = Logger.getLogger(getClass());
	
	public void insertFiles(List<FilesDTO> filesList, String tableName, long inID, Connection connection)
	{
		
		//List<FilesDTO> filesList = project_trackerDTO.files;
		String sql = "";
		
		//Connection connection = null;
		PreparedStatement ps = null;
		
		int index =1;

		long lastModificationTime = System.currentTimeMillis();	
		
		try
		{
			
			//connection = DBMR.getInstance().getConnection();
			
			if(filesList != null)
			{
		
		for(int i=0; i < filesList.size(); i++)
		{
			filesList.get(i).iD = DBMW.getInstance().getNextSequenceId("files");
			 sql = "INSERT INTO files " ;
			
			sql += " (";
			sql += "id";
			sql += ", ";
			sql += "file_type";
			sql += ", ";
			sql += "file_data";
			sql += ", ";
			sql += "file_tag";
			sql += ", ";
			sql += "file_title";
			sql += ", ";
			sql += "created_at";
			sql += ", ";
			sql += "isDeleted";
			

			sql += ")";
			
			
            sql += " VALUES(";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			

			sql += ")";
				
			//printSql(sql);

			ps = connection.prepareStatement(sql);
			
			
			

			 index = 1;

			ps.setObject(index++,filesList.get(i).iD);
			ps.setObject(index++,filesList.get(i).fileType);
			ps.setObject(index++,filesList.get(i).fileData);
			ps.setObject(index++,filesList.get(i).fileTag);
			ps.setObject(index++,filesList.get(i).fileTitle);
			ps.setObject(index++,lastModificationTime);
			ps.setObject(index++,filesList.get(i).isDeleted);
			
			
			System.out.println(ps);
			ps.execute();
			
			FileMappingDTO tempDto = new FileMappingDTO();
			
			tempDto.id = DBMW.getInstance().getNextSequenceId("files_mapping");
			 sql = "INSERT INTO files_mapping " ;
			
			sql += " (";
			sql += "id";
			sql += ", ";
			sql += "file_id";
			sql += ", ";
			sql += "table_name";
			sql += ", ";
			sql += "table_id";
						

			sql += ")";
			
			
            sql += " VALUES(";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			
			sql += ")";
				
			//printSql(sql);

			ps = connection.prepareStatement(sql);
			
			
			

			 index = 1;

			ps.setObject(index++,tempDto.id);
			ps.setObject(index++,filesList.get(i).iD);
			ps.setObject(index++,tableName);
			ps.setObject(index++,inID);
						
			
			System.out.println(ps);
			ps.execute();
			
			
		}
			}
		}
		catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if (ps != null) {
					ps.close();
				}
			} catch(Exception e){}
			try{
				if(connection != null){
					//DBMW.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		
	}
	
	public void insertFiles(List<FilesDTO> filesList, String inTable)
	{
		String sql = "";
		
		Connection connection = null;
		PreparedStatement ps = null;
		
		int index =1;

		long lastModificationTime = System.currentTimeMillis();	
		
		try
		{
			
			connection = DBMW.getInstance().getConnection();
		
		for(int i=0; i < filesList.size(); i++)
		{
			filesList.get(i).iD = DBMW.getInstance().getNextSequenceId("files");
			 sql = "INSERT INTO files " ;
			
			sql += " (";
			sql += "id";
			sql += ", ";
			sql += "file_type";
			sql += ", ";
			sql += "file_data";
			sql += ", ";
			sql += "file_tag";
			sql += ", ";
			sql += "file_title";
			sql += ", ";
			sql += "created_at";
			sql += ", ";
			sql += "isDeleted";
			

			sql += ")";
			
			
            sql += " VALUES(";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			

			sql += ")";
				
			//printSql(sql);

			ps = connection.prepareStatement(sql);
			
			
			

			 index = 1;

			ps.setObject(index++,filesList.get(i).iD);
			ps.setObject(index++,filesList.get(i).fileType);
			ps.setObject(index++,filesList.get(i).fileData);
			ps.setObject(index++,filesList.get(i).fileTag);
			ps.setObject(index++,filesList.get(i).fileTitle);
			ps.setObject(index++,lastModificationTime);
			ps.setObject(index++,filesList.get(i).isDeleted);
			
			
			System.out.println(ps);
			ps.execute();
			
			FileMappingDTO tempDto = new FileMappingDTO();
			
			tempDto.id = DBMW.getInstance().getNextSequenceId("files_mapping");
			 sql = "INSERT INTO files_mapping " ;
			
			sql += " (";
			sql += "id";
			sql += ", ";
			sql += "file_id";
			sql += ", ";
			sql += "table_name";
			sql += ", ";
			sql += "table_id";
						

			sql += ")";
			
			
            sql += " VALUES(";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			sql += ", ";
			sql += "?";
			
			sql += ")";
				
			//printSql(sql);

			ps = connection.prepareStatement(sql);
			
			
			

			 index = 1;

			ps.setObject(index++,tempDto.id);
			ps.setObject(index++,filesList.get(i).iD);
			ps.setObject(index++,inTable);
			ps.setObject(index++,filesList.get(i).rowId);
						
			
			System.out.println(ps);
			ps.execute();
			
			
		}
		}
		catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if (ps != null) {
					ps.close();
				}
			} catch(Exception e){}
			try{
				if(connection != null){
					DBMW.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		
	}
	
	public List<FilesDTO> getAllFiles(String tableName, long tableId,Connection connection)
	{
		List<FilesDTO> tempList = null;
		
	
		//Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		
			try{
				tempList = new ArrayList<FilesDTO>();
						String sql = "SELECT a.id, a.file_type, a.file_data, a.file_tag, a.file_title, a.created_at, a.isDeleted ";
			
						sql += " FROM files a, files_mapping b ";
						
			            sql += " WHERE a.id= b.file_id  and b.table_id = " + tableId + " and b.table_name LIKE '" + tableName.trim() + "'";			
									
						//connection = DBMR.getInstance().getConnection();
						stmt = connection.createStatement();
			
			
						rs = stmt.executeQuery(sql);
						
						//logger.debug("sql 10 : " + sql);
			
						while(rs.next()){
							FilesDTO tempObj = new FilesDTO();			
							
							tempObj.iD = rs.getLong("id");
							tempObj.fileType = rs.getString("file_type");
							
							 Blob blob = rs.getBlob("file_data");
					         //byte [] bytes = blob.getBytes(1l, (int)blob.length());
							tempObj.fileData = blob.getBytes(1, (int)blob.length());//(byte[]) rs.getBytes("file_data");
							//logger.debug("sql 12 : " + blob.length());
							blob.free();
							tempObj.fileTag = rs.getString("file_tag");
							tempObj.fileTitle = rs.getString("file_title");
							tempObj.createdAt = rs.getLong("created_at");
							tempObj.isDeleted = rs.getInt("isDeleted");
							
							tempList.add(tempObj);
							
			
						}			
						
						
			}catch(Exception ex){
				ex.printStackTrace();
				
				//logger.debug("sql 12 : " + ex.toString());
			}finally{
				try{ 
					if (stmt != null) {
						stmt.close();
					}
				} catch (Exception e){}
				
//				try{ 
//					if (connection != null){ 
//						DBMR.getInstance().freeConnection(connection); 
//					} 
//				}catch(Exception ex2){}
			}
			
		return tempList;
	}
	
	public List<FilesDTO> getAllFiles(String tableName, long tableId)
	{
		List<FilesDTO> tempList = null;
		
	
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		
			try{
				tempList = new ArrayList<FilesDTO>();
						String sql = "SELECT a.id, a.file_types, a.file_blob, a.file_tag, a.file_title, a.created_at, a.isDeleted ";
			
						sql += " FROM files a, files_mapping b ";
						
			            sql += " WHERE a.id= b.file_id  and b.table_id = " + tableId + " and b.table_name LIKE '" + tableName.trim() + "'";			
									
						connection = DBMR.getInstance().getConnection();
						stmt = connection.createStatement();
			
			
						rs = stmt.executeQuery(sql);
						
						//logger.debug("sql 10 : " + sql);
			
						while(rs.next()){
							FilesDTO tempObj = new FilesDTO();			
							
							tempObj.iD = rs.getLong("id");
							tempObj.fileType = rs.getString("file_type");
							
							 Blob blob = rs.getBlob("file_data");
					         //byte [] bytes = blob.getBytes(1l, (int)blob.length());
							tempObj.fileData = blob.getBytes(1, (int)blob.length());//(byte[]) rs.getBytes("file_data");
							//logger.debug("sql 12 : " + blob.length());
							blob.free();
							tempObj.fileTag = rs.getString("file_tag");
							tempObj.fileTitle = rs.getString("file_title");
							tempObj.createdAt = rs.getLong("created_at");
							tempObj.isDeleted = rs.getInt("isDeleted");
							
							tempList.add(tempObj);
							
			
						}			
						
						
			}catch(Exception ex){
				ex.printStackTrace();
				
				//logger.debug("sql 12 : " + ex.toString());
			}finally{
				try{ 
					if (stmt != null) {
						stmt.close();
					}
				} catch (Exception e){}
				
				try{ 
					if (connection != null){ 
						DBMR.getInstance().freeConnection(connection); 
					} 
				}catch(Exception ex2){}
			}
			
		return tempList;
	}
	
	public FilesDTO getFileByID( long id)
	{
		//List<FilesDTO> tempList = null;
		
		FilesDTO tempObj = null; 
		
	
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		
			try{
				//tempList = new ArrayList<FilesDTO>();
						String sql = "SELECT a.id, a.file_type, a.file_data, a.file_tag, a.file_title, a.created_at, a.isDeleted ";
			
						sql += " FROM files a WHERE a.id= " + id;
						
			           // sql += " WHERE a.id= b.file_id  and b.table_id = " + tableId + " and b.table_name LIKE '" + tableName.trim() + "'";			
									
						connection = DBMR.getInstance().getConnection();
						stmt = connection.createStatement();
			
			
						rs = stmt.executeQuery(sql);
						
						//logger.debug("sql 10 : " + sql);
			
						if(rs.next()){
							tempObj= new FilesDTO();			
							
							tempObj.iD = rs.getLong("id");
							tempObj.fileType = rs.getString("file_type");
							
							 Blob blob = rs.getBlob("file_data");
					         //byte [] bytes = blob.getBytes(1l, (int)blob.length());
							tempObj.fileData = blob.getBytes(1, (int)blob.length());//(byte[]) rs.getBytes("file_data");
							//logger.debug("sql 12 : " + blob.length());
							blob.free();
							tempObj.fileTag = rs.getString("file_tag");
							tempObj.fileTitle = rs.getString("file_title");
							tempObj.createdAt = rs.getLong("created_at");
							tempObj.isDeleted = rs.getInt("isDeleted");
							
							//tempList.add(tempObj);
							
			
						}			
						
						
			}catch(Exception ex){
				ex.printStackTrace();
				
				//logger.debug("sql 12 : " + ex.toString());
			}finally{
				try{ 
					if (stmt != null) {
						stmt.close();
					}
				} catch (Exception e){}
				
				try{ 
					if (connection != null){ 
						DBMR.getInstance().freeConnection(connection); 
					} 
				}catch(Exception ex2){}
			}
			
		return tempObj;
	}
	
	public List<FilesDTO> getAllFilesIDs(String tableName, long tableId)
	{
		List<FilesDTO> tempList = null;
		
	
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		
			try{
				tempList = new ArrayList<FilesDTO>();
						String sql = "SELECT a.id  ";
			
						sql += " FROM files a, files_mapping b ";
						
			            sql += " WHERE a.id= b.file_id  and b.table_id = " + tableId + " and b.table_name LIKE '" + tableName.trim() + "'";			
									
						connection = DBMR.getInstance().getConnection();
						stmt = connection.createStatement();
			
			
						rs = stmt.executeQuery(sql);
						
						//logger.debug("sql 10 : " + sql);
			
						while(rs.next()){
							FilesDTO tempObj = new FilesDTO();			
							
							tempObj.iD = rs.getLong("id");
							
							
							tempList.add(tempObj);
							
			
						}			
						
						
			}catch(Exception ex){
				ex.printStackTrace();
				
				//logger.debug("sql 12 : " + ex.toString());
			}finally{
				try{ 
					if (stmt != null) {
						stmt.close();
					}
				} catch (Exception e){}
				
				try{ 
					if (connection != null){ 
						DBMR.getInstance().freeConnection(connection); 
					} 
				}catch(Exception ex2){}
			}
			
		return tempList;
	}
	
	
	
	public void deleteFile(long fileID)
	{

		
		Connection connection = null;
		PreparedStatement ps = null;

		long lastModificationTime = System.currentTimeMillis();	
		try{
			connection = DBMW.getInstance().getConnection();

			String sql = "UPDATE files ";
			
			sql += " SET ";
			sql += "isDeleted = 1";
			
            sql += " WHERE ID = " + fileID;			
		

			ps = connection.prepareStatement(sql);		
			

			int index = 1;
			
		
			ps.executeUpdate();
			
			
			sql = "DELETE from  files_mapping WHERE file_id = " + fileID;

			ps = connection.prepareStatement(sql);

			ps.executeUpdate();	
			

			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if (ps != null) {
					ps.close();
				}
			} catch(Exception e){}
			try{
				if(connection != null){
					DBMW.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		
	}
	
	
	public void UpdateFileTileTag(FilesDTO fileDTO)
	{
		
		Connection connection = null;
		PreparedStatement ps = null;

		long lastModificationTime = System.currentTimeMillis();	
		try{
			connection = DBMW.getInstance().getConnection();

			String sql = "UPDATE files";
			
			sql += " SET ";
			sql += "file_tag=?";
			sql += ", ";
			sql += "file_title=?";			
            sql += " WHERE ID = " + fileDTO.iD;
				
			//printSql(sql);

			ps = connection.prepareStatement(sql);
			
			

			int index = 1;
			
			ps.setObject(index++,fileDTO.fileTag);
			ps.setObject(index++,fileDTO.fileTitle);
		
			System.out.println(ps);
			ps.executeUpdate();
			


			
			//recordUpdateTime(connection, ps, lastModificationTime, "files");
			
			ps.close();
			
			// FilesDAO tempFileDao = new FilesDAO();
			// tempFileDao.insertFiles(project_trackerDTO, connection);

						
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if (ps != null) {
					ps.close();
				}
			} catch(Exception e){}
			try{
				if(connection != null){
					DBMW.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		//return project_trackerDTO.iD;
	}
	

}
