package project_tracker;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import repository.Repository;
import repository.RepositoryManager;


public class Project_trackerRepository implements Repository {
	Project_trackerDAO project_trackerDAO = null;
	
	public void setDAO(Project_trackerDAO project_trackerDAO)
	{
		this.project_trackerDAO = project_trackerDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Project_trackerRepository.class);
	Map<Long, Project_trackerDTO>mapOfProject_trackerDTOToiD;
	Map<Long, Set<Project_trackerDTO> >mapOfProject_trackerDTOToprojectTrackerEventsType;
	Map<String, Set<Project_trackerDTO> >mapOfProject_trackerDTOToprojectTrackerEventsText;
	Map<Long, Set<Project_trackerDTO> >mapOfProject_trackerDTOToprojectTrackerSubjectsType;
	Map<String, Set<Project_trackerDTO> >mapOfProject_trackerDTOToprojectTrackerSubjectsText;
	Map<String, Set<Project_trackerDTO> >mapOfProject_trackerDTOToeventLocation;
	Map<String, Set<Project_trackerDTO> >mapOfProject_trackerDTOTosummary;
	Map<Long, Set<Project_trackerDTO> >mapOfProject_trackerDTOTolastModificationTime;


	static Project_trackerRepository instance = null;  
	private Project_trackerRepository(){
		mapOfProject_trackerDTOToiD = new ConcurrentHashMap<>();
		mapOfProject_trackerDTOToprojectTrackerEventsType = new ConcurrentHashMap<>();
		mapOfProject_trackerDTOToprojectTrackerEventsText = new ConcurrentHashMap<>();
		mapOfProject_trackerDTOToprojectTrackerSubjectsType = new ConcurrentHashMap<>();
		mapOfProject_trackerDTOToprojectTrackerSubjectsText = new ConcurrentHashMap<>();
		mapOfProject_trackerDTOToeventLocation = new ConcurrentHashMap<>();
		mapOfProject_trackerDTOTosummary = new ConcurrentHashMap<>();
		mapOfProject_trackerDTOTolastModificationTime = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	public synchronized static Project_trackerRepository getInstance(){
		if (instance == null){
			instance = new Project_trackerRepository();
		}
		return instance;
	}

	public void reload(boolean reloadAll){
		if(project_trackerDAO == null)
		{
			return;
		}
		try {
			List<Project_trackerDTO> project_trackerDTOs = project_trackerDAO.getAllProject_tracker(reloadAll);
			for(Project_trackerDTO project_trackerDTO : project_trackerDTOs) {
				Project_trackerDTO oldProject_trackerDTO = getProject_trackerDTOByID(project_trackerDTO.iD);
				if( oldProject_trackerDTO != null ) {
					mapOfProject_trackerDTOToiD.remove(oldProject_trackerDTO.iD);
				
					if(mapOfProject_trackerDTOToprojectTrackerEventsType.containsKey(oldProject_trackerDTO.projectTrackerEventsType)) {
						mapOfProject_trackerDTOToprojectTrackerEventsType.get(oldProject_trackerDTO.projectTrackerEventsType).remove(oldProject_trackerDTO);
					}
					if(mapOfProject_trackerDTOToprojectTrackerEventsType.get(oldProject_trackerDTO.projectTrackerEventsType).isEmpty()) {
						mapOfProject_trackerDTOToprojectTrackerEventsType.remove(oldProject_trackerDTO.projectTrackerEventsType);
					}
					
					if(mapOfProject_trackerDTOToprojectTrackerEventsText.containsKey(oldProject_trackerDTO.projectTrackerEventsText)) {
						mapOfProject_trackerDTOToprojectTrackerEventsText.get(oldProject_trackerDTO.projectTrackerEventsText).remove(oldProject_trackerDTO);
					}
					if(mapOfProject_trackerDTOToprojectTrackerEventsText.get(oldProject_trackerDTO.projectTrackerEventsText).isEmpty()) {
						mapOfProject_trackerDTOToprojectTrackerEventsText.remove(oldProject_trackerDTO.projectTrackerEventsText);
					}
					
					if(mapOfProject_trackerDTOToprojectTrackerSubjectsType.containsKey(oldProject_trackerDTO.projectTrackerSubjectsType)) {
						mapOfProject_trackerDTOToprojectTrackerSubjectsType.get(oldProject_trackerDTO.projectTrackerSubjectsType).remove(oldProject_trackerDTO);
					}
					if(mapOfProject_trackerDTOToprojectTrackerSubjectsType.get(oldProject_trackerDTO.projectTrackerSubjectsType).isEmpty()) {
						mapOfProject_trackerDTOToprojectTrackerSubjectsType.remove(oldProject_trackerDTO.projectTrackerSubjectsType);
					}
					
					if(mapOfProject_trackerDTOToprojectTrackerSubjectsText.containsKey(oldProject_trackerDTO.projectTrackerSubjectsText)) {
						mapOfProject_trackerDTOToprojectTrackerSubjectsText.get(oldProject_trackerDTO.projectTrackerSubjectsText).remove(oldProject_trackerDTO);
					}
					if(mapOfProject_trackerDTOToprojectTrackerSubjectsText.get(oldProject_trackerDTO.projectTrackerSubjectsText).isEmpty()) {
						mapOfProject_trackerDTOToprojectTrackerSubjectsText.remove(oldProject_trackerDTO.projectTrackerSubjectsText);
					}
					
					if(mapOfProject_trackerDTOToeventLocation.containsKey(oldProject_trackerDTO.eventLocation)) {
						mapOfProject_trackerDTOToeventLocation.get(oldProject_trackerDTO.eventLocation).remove(oldProject_trackerDTO);
					}
					if(mapOfProject_trackerDTOToeventLocation.get(oldProject_trackerDTO.eventLocation).isEmpty()) {
						mapOfProject_trackerDTOToeventLocation.remove(oldProject_trackerDTO.eventLocation);
					}
					
					if(mapOfProject_trackerDTOTosummary.containsKey(oldProject_trackerDTO.summary)) {
						mapOfProject_trackerDTOTosummary.get(oldProject_trackerDTO.summary).remove(oldProject_trackerDTO);
					}
					if(mapOfProject_trackerDTOTosummary.get(oldProject_trackerDTO.summary).isEmpty()) {
						mapOfProject_trackerDTOTosummary.remove(oldProject_trackerDTO.summary);
					}
					
					if(mapOfProject_trackerDTOTolastModificationTime.containsKey(oldProject_trackerDTO.lastModificationTime)) {
						mapOfProject_trackerDTOTolastModificationTime.get(oldProject_trackerDTO.lastModificationTime).remove(oldProject_trackerDTO);
					}
					if(mapOfProject_trackerDTOTolastModificationTime.get(oldProject_trackerDTO.lastModificationTime).isEmpty()) {
						mapOfProject_trackerDTOTolastModificationTime.remove(oldProject_trackerDTO.lastModificationTime);
					}
					
					
				}
				if(project_trackerDTO.isDeleted == 0) 
				{
					
					mapOfProject_trackerDTOToiD.put(project_trackerDTO.iD, project_trackerDTO);
				
					if( ! mapOfProject_trackerDTOToprojectTrackerEventsType.containsKey(project_trackerDTO.projectTrackerEventsType)) {
						mapOfProject_trackerDTOToprojectTrackerEventsType.put(project_trackerDTO.projectTrackerEventsType, new HashSet<>());
					}
					mapOfProject_trackerDTOToprojectTrackerEventsType.get(project_trackerDTO.projectTrackerEventsType).add(project_trackerDTO);
					
					if( ! mapOfProject_trackerDTOToprojectTrackerEventsText.containsKey(project_trackerDTO.projectTrackerEventsText)) {
						mapOfProject_trackerDTOToprojectTrackerEventsText.put(project_trackerDTO.projectTrackerEventsText, new HashSet<>());
					}
					mapOfProject_trackerDTOToprojectTrackerEventsText.get(project_trackerDTO.projectTrackerEventsText).add(project_trackerDTO);
					
					if( ! mapOfProject_trackerDTOToprojectTrackerSubjectsType.containsKey(project_trackerDTO.projectTrackerSubjectsType)) {
						mapOfProject_trackerDTOToprojectTrackerSubjectsType.put(project_trackerDTO.projectTrackerSubjectsType, new HashSet<>());
					}
					mapOfProject_trackerDTOToprojectTrackerSubjectsType.get(project_trackerDTO.projectTrackerSubjectsType).add(project_trackerDTO);
					
					if( ! mapOfProject_trackerDTOToprojectTrackerSubjectsText.containsKey(project_trackerDTO.projectTrackerSubjectsText)) {
						mapOfProject_trackerDTOToprojectTrackerSubjectsText.put(project_trackerDTO.projectTrackerSubjectsText, new HashSet<>());
					}
					mapOfProject_trackerDTOToprojectTrackerSubjectsText.get(project_trackerDTO.projectTrackerSubjectsText).add(project_trackerDTO);
					
					if( ! mapOfProject_trackerDTOToeventLocation.containsKey(project_trackerDTO.eventLocation)) {
						mapOfProject_trackerDTOToeventLocation.put(project_trackerDTO.eventLocation, new HashSet<>());
					}
					mapOfProject_trackerDTOToeventLocation.get(project_trackerDTO.eventLocation).add(project_trackerDTO);
					
					if( ! mapOfProject_trackerDTOTosummary.containsKey(project_trackerDTO.summary)) {
						mapOfProject_trackerDTOTosummary.put(project_trackerDTO.summary, new HashSet<>());
					}
					mapOfProject_trackerDTOTosummary.get(project_trackerDTO.summary).add(project_trackerDTO);
					
					if( ! mapOfProject_trackerDTOTolastModificationTime.containsKey(project_trackerDTO.lastModificationTime)) {
						mapOfProject_trackerDTOTolastModificationTime.put(project_trackerDTO.lastModificationTime, new HashSet<>());
					}
					mapOfProject_trackerDTOTolastModificationTime.get(project_trackerDTO.lastModificationTime).add(project_trackerDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Project_trackerDTO> getProject_trackerList() {
		List <Project_trackerDTO> project_trackers = new ArrayList<Project_trackerDTO>(this.mapOfProject_trackerDTOToiD.values());
		return project_trackers;
	}
	
	
	public Project_trackerDTO getProject_trackerDTOByID( long ID){
		return mapOfProject_trackerDTOToiD.get(ID);
	}
	
	
	public List<Project_trackerDTO> getProject_trackerDTOByproject_tracker_events_type(long project_tracker_events_type) {
		return new ArrayList<>( mapOfProject_trackerDTOToprojectTrackerEventsType.getOrDefault(project_tracker_events_type,new HashSet<>()));
	}
	
	
	public List<Project_trackerDTO> getProject_trackerDTOByproject_tracker_events_text(String project_tracker_events_text) {
		return new ArrayList<>( mapOfProject_trackerDTOToprojectTrackerEventsText.getOrDefault(project_tracker_events_text,new HashSet<>()));
	}
	
	
	public List<Project_trackerDTO> getProject_trackerDTOByproject_tracker_subjects_type(long project_tracker_subjects_type) {
		return new ArrayList<>( mapOfProject_trackerDTOToprojectTrackerSubjectsType.getOrDefault(project_tracker_subjects_type,new HashSet<>()));
	}
	
	
	public List<Project_trackerDTO> getProject_trackerDTOByproject_tracker_subjects_text(String project_tracker_subjects_text) {
		return new ArrayList<>( mapOfProject_trackerDTOToprojectTrackerSubjectsText.getOrDefault(project_tracker_subjects_text,new HashSet<>()));
	}
	
	
	public List<Project_trackerDTO> getProject_trackerDTOByevent_location(String event_location) {
		return new ArrayList<>( mapOfProject_trackerDTOToeventLocation.getOrDefault(event_location,new HashSet<>()));
	}
	
	
	public List<Project_trackerDTO> getProject_trackerDTOBysummary(String summary) {
		return new ArrayList<>( mapOfProject_trackerDTOTosummary.getOrDefault(summary,new HashSet<>()));
	}
	
	
	public List<Project_trackerDTO> getProject_trackerDTOBylastModificationTime(long lastModificationTime) {
		return new ArrayList<>( mapOfProject_trackerDTOTolastModificationTime.getOrDefault(lastModificationTime,new HashSet<>()));
	}

	
	@Override
	public String getTableName() {
		String tableName = "";
		try{
			tableName = "project_tracker";
		}catch(Exception ex){
			logger.debug("FATAL",ex);
		}
		return tableName;
	}
}


