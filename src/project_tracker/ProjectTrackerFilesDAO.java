package project_tracker;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;


import org.apache.log4j.Logger;

import dbm.*;

import repository.RepositoryManager;

import util.*;
import pb.*;
import approval_module_map.*;

public class ProjectTrackerFilesDAO  extends NavigationService3
{
	
	Logger logger = Logger.getLogger(getClass());

	
	public ProjectTrackerFilesDAO(String tableName, String tempTableName, Approval_module_mapDTO approval_module_mapDTO)
	{
		super(tableName, tempTableName, approval_module_mapDTO);		
	}
	
	public long add(CommonDTO commonDTO, String tableName, TempTableDTO tempTableDTO) throws Exception
	{
		return 0;
	}
	
	
	
//	public long add(CommonDTO commonDTO, String tableName, TempTableDTO tempTableDTO) throws Exception
//	{
//		
//		ProjectTrackerFilesDTO projecttrackerfilesDTO = (ProjectTrackerFilesDTO)commonDTO;
//		
//		Connection connection = null;
//		PreparedStatement ps = null;
//
//		long lastModificationTime = System.currentTimeMillis();	
//
//		try{
//			connection = DBMW.getInstance().getConnection();
//			
//			if(connection == null)
//			{
//				System.out.println("nullconn");
//			}
//
//			projecttrackerfilesDTO.iD = DBMW.getInstance().getNextSequenceId(tableName);
//
//			String sql = "INSERT INTO " + tableName;
//			
//			sql += " (";
//			sql += "ID";
//			sql += ", ";
//			sql += "project_tracker_id";
//			sql += ", ";
//			sql += "data_file";
//			sql += ", ";
//			sql += "isDeleted";
//			sql += ", ";
//			sql += "lastModificationTime";
//			if(tempTableDTO!=null)
//			{
//				sql += ", permanent_table_id, operation_type, approval_path_type, approval_order";
//			}
//			sql += ")";
//			
//			
//            sql += " VALUES(";
//			sql += "?";
//			sql += ", ";
//			sql += "?";
//			sql += ", ";
//			sql += "?";
//			sql += ", ";
//			sql += "?";
//			sql += ", ";
//			sql += "?";
//			if(tempTableDTO!=null)
//			{
//				sql += ", " + tempTableDTO.permanent_table_id + ", " + tempTableDTO.operation_type + ", " + tempTableDTO.approval_path_type + ", " + tempTableDTO.approval_order;
//			}
//			sql += ")";
//				
//			printSql(sql);
//
//			ps = connection.prepareStatement(sql);
//			
//			
//			
//
//			int index = 1;
//
//			ps.setObject(index++,projecttrackerfilesDTO.iD);
//			ps.setObject(index++,projecttrackerfilesDTO.projectTrackerId);
//			ps.setObject(index++,projecttrackerfilesDTO.dataFile);
//			ps.setObject(index++,projecttrackerfilesDTO.isDeleted);
//			ps.setObject(index++, lastModificationTime);
//			
//			System.out.println(ps);
//			ps.execute();
//			
//			
//			recordUpdateTime(connection, ps, lastModificationTime, tableName);
//
//		}catch(Exception ex){
//			ex.printStackTrace();
//		}finally{
//			try{
//				if (ps != null) {
//					ps.close();
//				}
//			} catch(Exception e){}
//			try{
//				if(connection != null){
//					DBMW.getInstance().freeConnection(connection);
//				}
//			}catch(Exception ex2){}
//		}
//		return projecttrackerfilesDTO.iD;		
//	}
//		
	
	public void deleteProjectTrackerFilesByProjectTrackerID(long projectTrackerID) throws Exception{
		
		
		Connection connection = null;
		Statement stmt = null;
		try{
			
			//String sql = "UPDATE project_tracker_files SET isDeleted=0 WHERE project_tracker_id="+projectTrackerID;
			String sql = "delete from project_tracker_files WHERE project_tracker_id=" + projectTrackerID;
			logger.debug("sql " + sql);
			connection = DBMW.getInstance().getConnection();
			stmt = connection.createStatement();
			stmt.execute(sql);
		}catch(Exception ex){
			logger.debug("fatal",ex);
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DBMW.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
	}		
   
	public List<ProjectTrackerFilesDTO> getProjectTrackerFilesDTOListByProjectTrackerID(long projectTrackerID) throws Exception{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		ProjectTrackerFilesDTO projecttrackerfilesDTO = null;
		List<ProjectTrackerFilesDTO> projecttrackerfilesDTOList = new ArrayList<>();
		
		try{
			
			String sql = "SELECT * FROM project_tracker_files where isDeleted=0 and project_tracker_id="+projectTrackerID+" order by project_tracker_files.lastModificationTime";
			
			logger.debug("sql " + sql);
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();
			rs = stmt.executeQuery(sql);

			while(rs.next()){
				projecttrackerfilesDTO = new ProjectTrackerFilesDTO();
				projecttrackerfilesDTO.iD = rs.getLong("ID");
				//projecttrackerfilesDTO.projectTrackerId = rs.getLong("project_tracker_id");
				//projecttrackerfilesDTO.dataFile = rs.getString("data_file");
				//projecttrackerfilesDTO.isDeleted = rs.getInt("isDeleted");
				projecttrackerfilesDTO.lastModificationTime = rs.getLong("lastModificationTime");
				projecttrackerfilesDTOList.add(projecttrackerfilesDTO);

			}			
			
		}catch(Exception ex){
			logger.debug("fatal",ex);
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DBMR.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
		return projecttrackerfilesDTOList;
	}

	//need another getter for repository
	public CommonDTO getDTOByID (long ID, String tableName) throws Exception
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		ProjectTrackerFilesDTO projecttrackerfilesDTO = null;
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
			
            sql += " WHERE ID=" + ID;
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			if(rs.next()){
				projecttrackerfilesDTO = new ProjectTrackerFilesDTO();

				projecttrackerfilesDTO.iD = rs.getLong("ID");
				//projecttrackerfilesDTO.projectTrackerId = rs.getLong("project_tracker_id");
				//projecttrackerfilesDTO.dataFile = rs.getString("data_file");
				//projecttrackerfilesDTO.isDeleted = rs.getInt("isDeleted");
				projecttrackerfilesDTO.lastModificationTime = rs.getLong("lastModificationTime");

			}			
			
			
			
			ProjectTrackerFilesDAO projectTrackerFilesDAO = new ProjectTrackerFilesDAO("project_tracker_files",  "", null);			
			List<ProjectTrackerFilesDTO> projectTrackerFilesDTOList = projectTrackerFilesDAO.getProjectTrackerFilesDTOListByProjectTrackerID(projecttrackerfilesDTO.iD);
			projecttrackerfilesDTO.projectTrackerFilesDTOList = projectTrackerFilesDTOList;
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DBMR.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
		return projecttrackerfilesDTO;
	}
	
	public long update(CommonDTO commonDTO, String tableName) throws Exception
	{		
		ProjectTrackerFilesDTO projecttrackerfilesDTO = (ProjectTrackerFilesDTO)commonDTO;
		
		Connection connection = null;
		PreparedStatement ps = null;

		long lastModificationTime = System.currentTimeMillis();	
		try{
			connection = DBMW.getInstance().getConnection();

			String sql = "UPDATE " + tableName;
			
			sql += " SET ";
			sql += "project_tracker_id=?";
			sql += ", ";
			sql += "data_file=?";
			sql += ", lastModificationTime = "	+ lastModificationTime + "";
            sql += " WHERE ID = " + projecttrackerfilesDTO.iD;
				
			printSql(sql);

			ps = connection.prepareStatement(sql);
			
			

			int index = 1;
			
		//	ps.setObject(index++,projecttrackerfilesDTO.projectTrackerId);
		//	ps.setObject(index++,projecttrackerfilesDTO.dataFile);
			System.out.println(ps);
			ps.executeUpdate();
			


			
			recordUpdateTime(connection, lastModificationTime, tableName);

						
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if (ps != null) {
					ps.close();
				}
			} catch(Exception e){}
			try{
				if(connection != null){
					DBMW.getInstance().freeConnection(connection);
				}
			}catch(Exception ex2){}
		}
		return projecttrackerfilesDTO.iD;
	}
	
	public List<ProjectTrackerFilesDTO> getDTOs(Collection recordIDs)
	{
		return getDTOs(recordIDs, tableName);
	}
	
	public List<ProjectTrackerFilesDTO> getDTOs(Collection recordIDs, String tableName){
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		ProjectTrackerFilesDTO projecttrackerfilesDTO = null;
		List<ProjectTrackerFilesDTO> projecttrackerfilesDTOList = new ArrayList<>();
		if(recordIDs.isEmpty()){
			return projecttrackerfilesDTOList;
		}
		try{
			
			String sql = "SELECT * ";

			sql += " FROM " + tableName;
            
            sql += " WHERE ID IN ( ";

			for(int i = 0;i<recordIDs.size();i++){
				if(i!=0){
					sql+=",";
				}
				sql+=((ArrayList)recordIDs).get(i);
			}
			sql+=")  order by lastModificationTime desc";
			
			printSql(sql);
			
			logger.debug("sql " + sql);
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				projecttrackerfilesDTO = new ProjectTrackerFilesDTO();
				projecttrackerfilesDTO.iD = rs.getLong("ID");
				//projecttrackerfilesDTO.projectTrackerId = rs.getLong("project_tracker_id");
			//	projecttrackerfilesDTO.dataFile = rs.getString("data_file");
			//	projecttrackerfilesDTO.isDeleted = rs.getInt("isDeleted");
				projecttrackerfilesDTO.lastModificationTime = rs.getLong("lastModificationTime");
				System.out.println("got this DTO: " + projecttrackerfilesDTO);
				
				projecttrackerfilesDTOList.add(projecttrackerfilesDTO);

			}			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DBMR.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
		return projecttrackerfilesDTOList;
	
	}
	
	

	
	
	
	//add repository
	public List<ProjectTrackerFilesDTO> getAllProjectTrackerFiles (boolean isFirstReload)
    {
		List<ProjectTrackerFilesDTO> projecttrackerfilesDTOList = new ArrayList<>();

		String sql = "SELECT * FROM project_tracker_files";
		sql += " WHERE ";
	

		if(isFirstReload){
			sql+=" isDeleted =  0";
		}
		if(!isFirstReload){
			sql+=" lastModificationTime >= " + RepositoryManager.lastModifyTime;		
		}
		sql += " order by projecttrackerfiles.lastModificationTime desc";
		printSql(sql);
		
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;

		
		try{
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();
			rs = stmt.executeQuery(sql);
			

			while(rs.next()){
				ProjectTrackerFilesDTO projecttrackerfilesDTO = new ProjectTrackerFilesDTO();
				projecttrackerfilesDTO.iD = rs.getLong("ID");
			//	projecttrackerfilesDTO.projectTrackerId = rs.getLong("project_tracker_id");
				//projecttrackerfilesDTO.dataFile = rs.getString("data_file");
				//projecttrackerfilesDTO.isDeleted = rs.getInt("isDeleted");
				projecttrackerfilesDTO.lastModificationTime = rs.getLong("lastModificationTime");
				
				projecttrackerfilesDTOList.add(projecttrackerfilesDTO);
			}			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			try{ 
				if (connection != null){ 
					DBMR.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}

		return projecttrackerfilesDTOList;
    }
	
	public List<ProjectTrackerFilesDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset)
	{
		return getDTOs(p_searchCriteria, limit, offset, tableName, true, 0);
	}
	
	public List<ProjectTrackerFilesDTO> getDTOs(Hashtable p_searchCriteria, int limit, int offset, String tableName, boolean isPermanentTable, long userApprovalPathType)
	{
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		List<ProjectTrackerFilesDTO> projecttrackerfilesDTOList = new ArrayList<>();

		try{
			
			String sql = getSqlWithSearchCriteria(p_searchCriteria, limit, offset, GETDTOS, tableName, isPermanentTable, userApprovalPathType);
			
			printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			while(rs.next()){
				ProjectTrackerFilesDTO projecttrackerfilesDTO = new ProjectTrackerFilesDTO();
				projecttrackerfilesDTO.iD = rs.getLong("ID");
				///projecttrackerfilesDTO.projectTrackerId = rs.getLong("project_tracker_id");
				//projecttrackerfilesDTO.dataFile = rs.getString("data_file");
				//projecttrackerfilesDTO.isDeleted = rs.getInt("isDeleted");
				projecttrackerfilesDTO.lastModificationTime = rs.getLong("lastModificationTime");
				
				projecttrackerfilesDTOList.add(projecttrackerfilesDTO);
			}					
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DBMR.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
		return projecttrackerfilesDTOList;
	
	}

		
	
	public String getSqlWithSearchCriteria(Hashtable p_searchCriteria, int limit, int offset, int category, String tableName, boolean isPermanentTable, long userApprovalPathType)
    {
		ProjectTrackerFilesMAPS maps = new ProjectTrackerFilesMAPS(tableName);
		String joinSQL = "";
		return getSqlWithSearchCriteria(p_searchCriteria, limit, offset, category, tableName, isPermanentTable, userApprovalPathType, maps, joinSQL);
    }			
}
	