package project_tracker;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;

import dbm.DBMR;

public class FileMappingDAO {
	
	
	public List<FilesDTO> getAllFiles(String tableName, long tableId)
	{
		List<FilesDTO> tempList = null;
		
//		try
//		{
//			tempList = new ArrayList<FileMappingDTO>();
//		}
//		catch(Exception ex)
//		{
//			
//		}
		
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		
try{
	tempList = new ArrayList<FilesDTO>();
			String sql = "SELECT a.id, a.file_type, a.file_data, a.file_tag, a.file_title, a.created_at, a.isDeleted ";

			sql += " FROM files a, " + tableName + " b ";
			
            sql += " WHERE a.id= b.file_id  and b.table_id = " + tableId + " and b.table_name LIKE '" + tableName.trim() + "'";
			
			//printSql(sql);
			
			connection = DBMR.getInstance().getConnection();
			stmt = connection.createStatement();


			rs = stmt.executeQuery(sql);

			if(rs.next()){
				FilesDTO tempObj = new FilesDTO();
				
				//project_tracker_eventsDTO = new Project_tracker_eventsDTO();

				tempObj.iD = rs.getLong("id");
				tempObj.fileType = rs.getString("file_type");
				tempObj.fileData = rs.getBytes("file_data");
				tempObj.fileTag = rs.getString("file_tag");
				tempObj.fileTitle = rs.getString("file_title");
				tempObj.createdAt = rs.getLong("created_at");
				tempObj.isDeleted = rs.getInt("isDeleted");
				
				tempList.add(tempObj);
				

			}			
			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{ 
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e){}
			
			try{ 
				if (connection != null){ 
					DBMR.getInstance().freeConnection(connection); 
				} 
			}catch(Exception ex2){}
		}
		
		return tempList;
	}

}
