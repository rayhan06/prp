package project_tracker;
import java.util.*; 
import util.*; 


public class ProjectTrackerFilesDTO extends CommonDTO
{

	public long id;
	public String fileType = "";
	public byte[] fileData;
	public String fileTag;
	public String fileTitle;
	public long createdAt;
	public int isDeleted;
	//public long projectTrackerId = 0;
   // public String dataFile = "";
	
	public List<ProjectTrackerFilesDTO> projectTrackerFilesDTOList = new ArrayList<>();
	
    @Override
	public String toString() {
            return "$ProjectTrackerFilesDTO[" +
            " iD = " + iD +
           // " projectTrackerId = " + projectTrackerId +
           // " dataFile = " + dataFile +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}