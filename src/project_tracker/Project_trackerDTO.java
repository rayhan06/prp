package project_tracker;
import java.util.*; 
import util.*; 


public class Project_trackerDTO extends CommonDTO
{

	public long projectTrackerEventsType = 0;
    public String projectTrackerEventsText = "";
	public long projectTrackerSubjectsType = 0;
    public String projectTrackerSubjectsText = "";
    public String eventLocation = "";
    public String summary = "";
    public List<FilesDTO> files;
	
	public List<ProjectTrackerFilesDTO> projectTrackerFilesDTOList = new ArrayList<>();
	
    @Override
	public String toString() {
            return "$Project_trackerDTO[" +
            " iD = " + iD +
            " projectTrackerEventsType = " + projectTrackerEventsType +
            " projectTrackerEventsText = " + projectTrackerEventsText +
            " projectTrackerSubjectsType = " + projectTrackerSubjectsType +
            " projectTrackerSubjectsText = " + projectTrackerSubjectsText +
            " eventLocation = " + eventLocation +
            " summary = " + summary +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}