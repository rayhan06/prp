package mp_remuneration_allowance;

import com.google.gson.Gson;
import login.LoginDTO;
import org.apache.log4j.Logger;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/Mp_remuneration_allowanceServlet")
@MultipartConfig
public class Mp_remuneration_allowanceServlet extends HttpServlet {
    private static final Logger logger = Logger.getLogger(Mp_remuneration_allowanceServlet.class);

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }

        try {
            String actionType = request.getParameter("actionType");
            switch (actionType) {
                case "ajax_getCertificateModel": {
                    long electionDetailsId = Long.parseLong(request.getParameter("election_details_id"));
                    long electionConstituencyId = Long.parseLong(request.getParameter("election_constituency_id"));
                    long startDate = Long.parseLong(request.getParameter("startDate"));
                    long endDate = Long.parseLong(request.getParameter("endDate"));
                    Mp_remuneration_allowanceModel model = new Mp_remuneration_allowanceModel(
                            electionDetailsId, electionConstituencyId, startDate, endDate, "BANGLA"
                    );
                    response.setContentType("application/json");
                    response.setCharacterEncoding("UTF-8");
                    response.getWriter().println(new Gson().toJson(model));
                    return;
                }
                case "downloadCertificate": {
                    request.getRequestDispatcher("mp_remuneration_allowance/mp_remuneration_allowanceCertificate.jsp")
                           .forward(request, response);
                    return;
                }
            }
        } catch (Exception ex) {
            logger.error(ex);
        }
        request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
    }
}