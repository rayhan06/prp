package mp_remuneration_allowance;

import allowance_employee_info.AllowanceEmployeeInfoDTO;
import allowance_employee_info.AllowanceEmployeeInfoRepository;
import bangladehi_number_format_util.BangladeshiNumberFormatter;
import bangladehi_number_format_util.BangladeshiNumberInWord;
import budget.BudgetInfo;
import election_constituency.Election_constituencyRepository;
import election_details.Election_detailsRepository;
import employee_records.Employee_recordsRepository;
import mp_payroll_bill.Mp_payroll_billDAO;
import mp_payroll_bill.Mp_payroll_billDTO;
import mp_travel_allowance.Mp_travel_allowanceDAO;
import pb.Utils;
import pbReport.DateUtils;
import util.StringUtils;

import static budget.BudgetUtils.getEconomicYearBeginYear;

public class Mp_remuneration_allowanceModel {
    public String reportDate;
    public String parliamentNumber;
    public String electionConstituency;
    public String mpName = "";
    public String economicYear;
    public String startDate;
    public String endDate;
    public String gratuity = "0";
    public String totalAllowance = "0";
    public String totalAmount = "0";
    public String totalInWord;

    public Mp_remuneration_allowanceModel(long electionDetailsId, long electionConstituencyId, long startDate, long endDate, String language) throws Exception {
        reportDate = DateUtils.getDateInWord(language, System.currentTimeMillis());
        parliamentNumber = Election_detailsRepository.getInstance().getText(electionDetailsId, language);
        electionConstituency = Election_constituencyRepository.getInstance().getText(electionConstituencyId, language);
        this.startDate = DateUtils.getDateInWord(language, startDate);
        this.endDate = DateUtils.getDateInWord(language, endDate);
        if (language.equalsIgnoreCase("ENGLISH")) {
            this.economicYear = BudgetInfo.getEconomicYear(getEconomicYearBeginYear(startDate));
        } else {
            this.economicYear = StringUtils.convertToBanNumber(BudgetInfo.getEconomicYear(getEconomicYearBeginYear(startDate)));
        }
        Mp_payroll_billDTO summaryDTO = Mp_payroll_billDAO.getInstance().getSummaryDTO(electionDetailsId, electionConstituencyId, startDate, endDate);
        if (summaryDTO != null) {
            AllowanceEmployeeInfoDTO employeeInfoDTO = AllowanceEmployeeInfoRepository.getInstance().getById(summaryDTO.allowanceEmployeeInfoId);
            int genderCat = Employee_recordsRepository.getInstance().getById(employeeInfoDTO.employeeRecordId).gender;
            mpName = getNameWithPrefix(genderCat, employeeInfoDTO.nameEn, employeeInfoDTO.nameBn, language);
            gratuity = BangladeshiNumberFormatter.getFormattedNumber(Utils.getDigits(summaryDTO.gratuity, language)) + "/-";
            int travelAllowanceSum = (int) Mp_travel_allowanceDAO.getInstance().getTotalAllowance(electionDetailsId, electionConstituencyId, startDate, endDate);
            totalAllowance = BangladeshiNumberFormatter.getFormattedNumber(Utils.getDigits(summaryDTO.totalAllowance + travelAllowanceSum, language)) + "/-";
            totalAmount = Utils.getDigits(summaryDTO.totalAllowance + travelAllowanceSum + summaryDTO.gratuity, language);
        }
        totalInWord = BangladeshiNumberInWord.convertToWord(Utils.getDigits(totalAmount, language));
        totalAmount = BangladeshiNumberFormatter.getFormattedNumber(totalAmount) + "/-";
    }

    private String getNameWithPrefix(int genderCat, String nameEn, String nameBn, String language) {
        if (genderCat == 1) {
            if (language.equalsIgnoreCase("ENGLISH"))
                return "Mr. " + nameEn;
            else
                return "জনাব " + nameBn;
        } else if (genderCat == 2) {
            if (language.equalsIgnoreCase("ENGLISH"))
                return "Ms. " + nameEn;
            else
                return "জনাবা " + nameBn;
        }
        return language.equalsIgnoreCase("ENGLISH") ? nameEn : nameBn;
    }

}
