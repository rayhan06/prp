package other_office_configuration;

import java.io.IOException;
import java.text.SimpleDateFormat;


import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import org.apache.log4j.Logger;

import permission.MenuConstants;

import user.UserDTO;
import util.*;
import javax.servlet.http.*;


import common.ApiResponse;
import common.BaseServlet;
import com.google.gson.Gson;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;




/**
 * Servlet implementation class Other_office_configurationServlet
 */
@WebServlet("/Other_office_configurationServlet")
@MultipartConfig
public class Other_office_configurationServlet extends BaseServlet 
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Other_office_configurationServlet.class);

    @Override
    public String getTableName() {
        return Other_office_configurationDAO.getInstance().getTableName();
    }

    @Override
    public String getServletName() {
        return "Other_office_configurationServlet";
    }

    @Override
    public Other_office_configurationDAO getCommonDAOService() {
        return Other_office_configurationDAO.getInstance();
    }
    
    @Override
    public int[] getAddPageMenuConstants() {
        return new int[] {MenuConstants.OTHER_OFFICE_CONFIGURATION_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[] {MenuConstants.OTHER_OFFICE_CONFIGURATION_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[] {MenuConstants.OTHER_OFFICE_CONFIGURATION_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Other_office_configurationServlet.class;
    }
    private final Gson gson = new Gson();
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        CommonLoginData commonLoginData = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get();
        init(request);
        String actionType = request.getParameter("actionType");
		if ("getByOfficeId".equals(actionType)) {
			try {
				long otherOfficeType = Long.parseLong(request.getParameter("otherOfficeType"));
				Other_office_configurationDTO other_office_configurationDTO = Other_office_configurationDAO.getInstance().getByOfficeId(otherOfficeType);
				if(other_office_configurationDTO != null)
				{
					response.getWriter().write(other_office_configurationDTO.iD + "");
				}
				else
				{
					response.getWriter().write("-1");
				}
			} catch (Exception ex) {
				logger.error(ex);
				ApiResponse.sendErrorResponse(response, ex.getMessage());
			}
			return;
		}
		else
		{
			super.doGet(request,response);
		}
    }
 	

	@Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception 
	{
		
		request.setAttribute("failureMessage", "");
		System.out.println("%%%% addOther_office_configuration");
		String language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
		Other_office_configurationDTO other_office_configurationDTO;
		SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
					
		if(addFlag)
		{
			other_office_configurationDTO = new Other_office_configurationDTO();
		}
		else
		{
			other_office_configurationDTO = Other_office_configurationDAO.getInstance().getDTOFromID(Long.parseLong(request.getParameter("iD")));
		}
		
		String Value = "";

		Value = request.getParameter("otherOfficeType");

		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("otherOfficeType = " + Value);
		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			other_office_configurationDTO.otherOfficeType = Integer.parseInt(Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}
		
		String [] configurations = request.getParameterValues("configurtionList");
		other_office_configurationDTO.configurtionList = "";
		if(configurations != null)
		{
			for(String configuration: configurations)
			{
				other_office_configurationDTO.configurtionList += Jsoup.clean(configuration,Whitelist.simpleText()) + ", ";
			}
		}
		System.out.println("configurtionList set = " + other_office_configurationDTO.configurtionList);

		
		if(addFlag)
		{
			other_office_configurationDTO.insertedByUserId = userDTO.ID;
		}


		if(addFlag)
		{
			other_office_configurationDTO.insertedByOrganogramId = userDTO.organogramID;
		}


		if(addFlag)
		{				
			other_office_configurationDTO.insertionDate = TimeConverter.getToday();
		}			


		other_office_configurationDTO.lastModifierUser = userDTO.userName;

		System.out.println("Done adding  addOther_office_configuration dto = " + other_office_configurationDTO);

		if(addFlag == true)
		{
			Other_office_configurationDAO.getInstance().add(other_office_configurationDTO);
		}
		else
		{				
			Other_office_configurationDAO.getInstance().update(other_office_configurationDTO);										
		}
		
		

		return other_office_configurationDTO;

	}
}

