package other_office_configuration;
 


import java.util.ArrayList;
import java.util.List;

import util.*; 


public class Other_office_configurationDTO extends CommonDTO
{

	public int otherOfficeType = -1;
    public String configurtionList = "";
	public long insertedByUserId = -1;
	public long insertedByOrganogramId = -1;
	public long insertionDate = -1;
    public String lastModifierUser = "";
    
    public static final int DOCTOR_VISIT = 0;
    public static final int MEDICINE = 1;
    public static final int LAB_TEST = 2;
    public static final int PHYSIOTHERAPY = 3;

    public List<String> existingPermissions = new ArrayList<String>();
    public String permissionEng = "";
    public String permissionBng = "";
	
	
    @Override
	public String toString() {
            return "$Other_office_configurationDTO[" +
            " iD = " + iD +
            " otherOfficeType = " + otherOfficeType +
            " configurtionList = " + configurtionList +
            " insertedByUserId = " + insertedByUserId +
            " insertedByOrganogramId = " + insertedByOrganogramId +
            " insertionDate = " + insertionDate +
            " lastModifierUser = " + lastModifierUser +
            " searchColumn = " + searchColumn +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}