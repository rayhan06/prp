package other_office_configuration;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.sql.SQLException;
import common.CommonDAOService;
import common.ConnectionAndStatementUtil;

import org.apache.log4j.Logger;

import appointment.KeyCountDTO;
import util.*;
import pb.*;
import sessionmanager.SessionConstants;

public class Other_office_configurationDAO  implements CommonDAOService<Other_office_configurationDTO>
{
	
	Logger logger = Logger.getLogger(getClass());
	
	private static String addQuery = "";
	private static String updateQuery = "";
	private String[] columnNames = null;

	private Other_office_configurationDAO()
	{
		columnNames = new String[] 
		{
			"ID",
			"other_office_type",
			"configurtion_list",
			"inserted_by_user_id",
			"inserted_by_organogram_id",
			"insertion_date",
			"last_modifier_user",
			"search_column",
			"isDeleted",
			"lastModificationTime"
		};
		updateQuery = getUpdateQuery(columnNames);
        addQuery = getAddQuery(columnNames);
		
		searchMap.put("other_office_type"," and (other_office_type = ?)");
		searchMap.put("configurtion_list"," and (configurtion_list like ?)");
		searchMap.put("insertion_date_start"," and (insertion_date >= ?)");
		searchMap.put("insertion_date_end"," and (insertion_date <= ?)");
		searchMap.put("AnyField"," and (search_column like ?)");
	}

	private final Map<String,String> searchMap = new HashMap<>();

    private static class LazyLoader
    {
    	static final Other_office_configurationDAO INSTANCE = new Other_office_configurationDAO();
	}

	public static Other_office_configurationDAO getInstance()
	{
    	return LazyLoader.INSTANCE;
	}
	
	public void setSearchColumn(Other_office_configurationDTO other_office_configurationDTO)
	{
		other_office_configurationDTO.searchColumn = "";
		other_office_configurationDTO.searchColumn += CommonDAO.getName("English", "other_office", other_office_configurationDTO.otherOfficeType) + " " + CommonDAO.getName("Bangla", "other_office", other_office_configurationDTO.otherOfficeType) + " ";
		other_office_configurationDTO.searchColumn += other_office_configurationDTO.configurtionList + " ";
	}
	
	@Override
	public void set(PreparedStatement ps, Other_office_configurationDTO other_office_configurationDTO, boolean isInsert) throws SQLException
	{
		int index = 0;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(other_office_configurationDTO);
		if(isInsert)
		{
			ps.setObject(++index,other_office_configurationDTO.iD);
		}
		ps.setObject(++index,other_office_configurationDTO.otherOfficeType);
		ps.setObject(++index,other_office_configurationDTO.configurtionList);
		ps.setObject(++index,other_office_configurationDTO.insertedByUserId);
		ps.setObject(++index,other_office_configurationDTO.insertedByOrganogramId);
		ps.setObject(++index,other_office_configurationDTO.insertionDate);
		ps.setObject(++index,other_office_configurationDTO.lastModifierUser);
		ps.setObject(++index,other_office_configurationDTO.searchColumn);
		if(isInsert)
		{
			ps.setObject(++index,other_office_configurationDTO.isDeleted);
		}
		ps.setObject(++index, lastModificationTime);
		if(!isInsert)
		{
			ps.setObject(++index,other_office_configurationDTO.iD);
		}
	}
	
	@Override
	public Other_office_configurationDTO buildObjectFromResultSet(ResultSet rs)
	{
		try
		{
			Other_office_configurationDTO other_office_configurationDTO = new Other_office_configurationDTO();
			int i = 0;
			other_office_configurationDTO.iD = rs.getLong(columnNames[i++]);
			other_office_configurationDTO.otherOfficeType = rs.getInt(columnNames[i++]);
			other_office_configurationDTO.configurtionList = rs.getString(columnNames[i++]);
			String []existingPermissions = other_office_configurationDTO.configurtionList.split(", ");
			other_office_configurationDTO.existingPermissions = Arrays.asList(existingPermissions);
			if(other_office_configurationDTO.existingPermissions != null)
			{
				int j = 0;
				for(String permissionValStr: other_office_configurationDTO.existingPermissions)
				{
					if(j > 0)
					{
						other_office_configurationDTO.permissionEng += ", ";
						other_office_configurationDTO.permissionBng += ", ";
					}
					int permissionVal = Integer.parseInt(permissionValStr);
					other_office_configurationDTO.permissionEng += CatDAO.getName("english", "other_office_permissions", permissionVal);
					other_office_configurationDTO.permissionBng += CatDAO.getName("bangla", "other_office_permissions", permissionVal);
					
					j++;
				}
			}
			other_office_configurationDTO.insertedByUserId = rs.getLong(columnNames[i++]);
			other_office_configurationDTO.insertedByOrganogramId = rs.getLong(columnNames[i++]);
			other_office_configurationDTO.insertionDate = rs.getLong(columnNames[i++]);
			other_office_configurationDTO.lastModifierUser = rs.getString(columnNames[i++]);
			other_office_configurationDTO.searchColumn = rs.getString(columnNames[i++]);
			other_office_configurationDTO.isDeleted = rs.getInt(columnNames[i++]);
			other_office_configurationDTO.lastModificationTime = rs.getLong(columnNames[i++]);
			return other_office_configurationDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
		
	public Other_office_configurationDTO getDTOByID (long id)
	{
		return getDTOFromID(id);
	}
	
	
	public Other_office_configurationDTO getByOfficeId(long otherOfficeId)
    {
		String sql = "select * from " + getTableName() + " where isDeleted = 0 and other_office_type = " + otherOfficeId;

		return ConnectionAndStatementUtil.getT(sql,this::buildObjectFromResultSet);	
    }

	@Override
	public String getTableName() {
		return "other_office_configuration";
	}

	@Override
	public Map<String, String> getSearchMap() {
		return searchMap;
	}

	public long add(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Other_office_configurationDTO) commonDTO,addQuery,true);
	}

	public long update(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Other_office_configurationDTO) commonDTO,updateQuery,false);
	}
	
	@Override
	public String getLastModifierUser(){
		return "last_modifier_user";
    }
				
}
	