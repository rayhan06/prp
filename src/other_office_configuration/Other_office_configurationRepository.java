package other_office_configuration;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import com.google.gson.Gson;

import repository.Repository;
import repository.RepositoryManager;


public class Other_office_configurationRepository implements Repository {
	Other_office_configurationDAO other_office_configurationDAO = null;
	
	static Logger logger = Logger.getLogger(Other_office_configurationRepository.class);
	Map<Long, Other_office_configurationDTO>mapOfOther_office_configurationDTOToiD;
	Gson gson;

  
	private Other_office_configurationRepository(){
		other_office_configurationDAO = Other_office_configurationDAO.getInstance();
		mapOfOther_office_configurationDTOToiD = new ConcurrentHashMap<>();
		gson = new Gson();
		RepositoryManager.getInstance().addRepository(this);
	}

	private static class LazyLoader{
        final static Other_office_configurationRepository INSTANCE = new Other_office_configurationRepository();
    }

    public static Other_office_configurationRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

	public void reload(boolean reloadAll){
		try {
			List<Other_office_configurationDTO> other_office_configurationDTOs = other_office_configurationDAO.getAllDTOs(reloadAll);
			for(Other_office_configurationDTO other_office_configurationDTO : other_office_configurationDTOs) {
				Other_office_configurationDTO oldOther_office_configurationDTO = getOther_office_configurationDTOByiD(other_office_configurationDTO.iD);
				if( oldOther_office_configurationDTO != null ) {
					mapOfOther_office_configurationDTOToiD.remove(oldOther_office_configurationDTO.iD);
				
					
				}
				if(other_office_configurationDTO.isDeleted == 0) 
				{
					
					mapOfOther_office_configurationDTOToiD.put(other_office_configurationDTO.iD, other_office_configurationDTO);
				
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public Other_office_configurationDTO clone(Other_office_configurationDTO dto) {
		String raw = gson.toJson(dto);
		return gson.fromJson(raw, Other_office_configurationDTO.class);
	}
	
	
	public List<Other_office_configurationDTO> getOther_office_configurationList() {
		List <Other_office_configurationDTO> other_office_configurations = new ArrayList<Other_office_configurationDTO>(this.mapOfOther_office_configurationDTOToiD.values());
		return other_office_configurations;
	}
	
	public List<Other_office_configurationDTO> copyOther_office_configurationList() {
		List <Other_office_configurationDTO> other_office_configurations = getOther_office_configurationList();
		return other_office_configurations
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}
	
	
	
	public Other_office_configurationDTO getOther_office_configurationDTOByiD( long iD){
		return mapOfOther_office_configurationDTOToiD.get(iD);
	}
	
	public Other_office_configurationDTO copyOther_office_configurationDTOByiD( long iD){
		return clone(mapOfOther_office_configurationDTOToiD.get(iD));
	}
	
	public ArrayList<Integer> getOfficesWithPermission(int permission)
	{
		List<Other_office_configurationDTO> other_office_configurationDTOs= getOther_office_configurationList();
		ArrayList<Integer> permissibleOffices = new ArrayList<Integer>();
		permissibleOffices.add(-1);
		for(Other_office_configurationDTO other_office_configurationDTO: other_office_configurationDTOs)
		{
			if(other_office_configurationDTO.existingPermissions.contains(permission + ""))
			{
				permissibleOffices.add(other_office_configurationDTO.otherOfficeType);
			}
		}
		return permissibleOffices;
		
	}

	
	@Override
	public String getTableName() {
		return other_office_configurationDAO.getTableName();
	}
}


