package tax_deduction_configuration;

import org.apache.log4j.Logger;
import pb.OptionDTO;
import pb.Utils;
import repository.Repository;
import repository.RepositoryManager;
import util.LockManager;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import static util.StringUtils.convertToBanNumber;


public class Tax_deduction_configurationRepository implements Repository {
    private static final Logger logger = Logger.getLogger(Tax_deduction_configurationRepository.class);
    private final Tax_deduction_configurationDAO dao;
    private final Map<Long, Tax_deduction_configurationDTO> mapById;

    private Tax_deduction_configurationRepository() {
        dao = Tax_deduction_configurationDAO.getInstance();
        mapById = new ConcurrentHashMap<>();
        RepositoryManager.getInstance().addRepository(this);
    }

    private static class LazyLoader {
        final static Tax_deduction_configurationRepository INSTANCE = new Tax_deduction_configurationRepository();
    }

    public static Tax_deduction_configurationRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

    @Override
    public void reload(boolean reloadAll) {
        logger.debug("Tax_deduction_configurationRepository reload start for, reloadAll : " + reloadAll);
        List<Tax_deduction_configurationDTO> dtoList = dao.getAllDTOs(reloadAll);
        if (dtoList != null && dtoList.size() > 0) {
            dtoList.stream()
                    .peek(this::removeIfPresent)
                    .filter(dto -> dto.isDeleted == 0)
                    .forEach(dto -> mapById.put(dto.iD, dto));
        }
        logger.debug("Tax_deduction_configurationRepository reload end for, reloadAll : " + reloadAll);
    }

    private void removeIfPresent(Tax_deduction_configurationDTO newDTO) {
        if (newDTO == null) return;
        if (mapById.get(newDTO.iD) != null) {
            mapById.remove(newDTO.iD);
        }
    }

    public List<Tax_deduction_configurationDTO> getAllDTOs() {
        return new ArrayList<>(this.mapById.values());
    }

    public List<Tax_deduction_configurationDTO> getAllActiveDTOs() {
        return mapById.values()
                .stream()
                .filter(dto -> dto.isActive)
                .collect(Collectors.toList());
    }

    public Tax_deduction_configurationDTO getDTOById(long id) {
        if (mapById.get(id) == null) {
            synchronized (LockManager.getLock(id + "TDCGDTOID")) {
                if (mapById.get(id) == null) {
                    Tax_deduction_configurationDTO dto = dao.getDTOFromID(id);
                    if (dto != null) {
                        mapById.put(dto.iD, dto);
                    }
                }
            }
        }
        return mapById.get(id);
    }

    public Tax_deduction_configurationDTO getDTODeletedOrNot(long id) {
        if (mapById.get(id) == null) {
            return dao.getDTOFromIdDeletedOrNot(id);
        }
        return mapById.get(id);
    }

    @Override
    public String getTableName() {
        return "tax_deduction_configuration";
    }

    public String getDeductionText(long id, String language) {
        Tax_deduction_configurationDTO dto = getDTOById(id);
        if (dto == null) return "";
        return "english".equalsIgnoreCase(language)
                ? dto.code.concat(" - ").concat(dto.nameEn)
                : convertToBanNumber(dto.code).concat(" - ").concat(dto.nameBn);
    }

    public String buildOptions(int employmentCat, Long selectedId, String language) {
        List<OptionDTO> optionDTOList =
                getActiveDTOs(employmentCat).stream()
                        .map(Tax_deduction_configurationDTO::getOptionDTO)
                        .collect(Collectors.toList());
        return Utils.buildOptions(optionDTOList, language, selectedId == null ? null : String.valueOf(selectedId));
    }

    public List<Tax_deduction_configurationModel> getActiveModels(int employmentCat, String language) {
        return getActiveDTOs(employmentCat).stream()
                .map(dto -> new Tax_deduction_configurationModel(dto, language))
                .sorted(Comparator.comparing(dto -> dto.configurationId))
                .collect(Collectors.toList());
    }

    public List<Tax_deduction_configurationDTO> getActiveDTOs(int employmentCat) {
        return getAllDTOs().stream()
                .filter(deductionConfig -> deductionConfig.employmentType == employmentCat && deductionConfig.isActive)
                .collect(Collectors.toList());
    }

    public long getHouseRentDeductionId() {
        return getAllDTOs().stream()
                .filter(dto -> dto.code.equals("1-3237-0000-2111"))
                .map(dto -> dto.iD)
                .findFirst()
                .orElse(-1L);
    }
}