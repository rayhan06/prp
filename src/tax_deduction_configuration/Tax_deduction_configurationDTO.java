package tax_deduction_configuration;

import pb.OptionDTO;
import util.CommonDTO;

import static util.StringUtils.convertToBanNumber;

public class Tax_deduction_configurationDTO extends CommonDTO {

    public long taxDeductionID = -1;
    public int employmentType = -1;
    public String code = "";
    public String nameEn = "";
    public String nameBn = "";
    public boolean isActive = false;
    public long modifiedBy = -1;
    public long insertedBy = -1;
    public long insertionTime = -1;

    public OptionDTO getOptionDTO() {
        return new OptionDTO(
                code.concat(" - ").concat(nameEn),
                convertToBanNumber(code).concat(" - ").concat(nameBn),
                String.valueOf(iD)
        );
    }

    @Override
    public String toString() {
        return "Tax_deduction_configurationDTO{" +
                "taxDeductionID=" + taxDeductionID +
                ", employmentType=" + employmentType +
                ", code='" + code + '\'' +
                ", nameEn='" + nameEn + '\'' +
                ", nameBn='" + nameBn + '\'' +
                ", isActive=" + isActive +
                ", modifiedBy=" + modifiedBy +
                ", insertedBy=" + insertedBy +
                ", insertionTime=" + insertionTime +
                '}';
    }

}