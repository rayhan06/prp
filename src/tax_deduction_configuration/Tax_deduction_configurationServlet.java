package tax_deduction_configuration;

import common.BaseServlet;
import employee_records.Employee_recordsRepository;
import employee_records.EmploymentEnum;
import office_unit_organograms.SameDesignationGroup;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import payroll_deduction_lookup.PayrollDeductionLookupModel;
import payroll_deduction_lookup.Payroll_deduction_lookupServlet;
import payroll_deduction_type.Payroll_deduction_typeDTO;
import payroll_deduction_type.Payroll_deduction_typeRepository;
import permission.MenuConstants;
import user.UserDTO;
import util.CommonDTO;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;


@WebServlet("/Tax_deduction_configurationServlet")
@MultipartConfig
public class Tax_deduction_configurationServlet extends BaseServlet {
    private static final long serialVersionUID = 1L;

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        int employmentType = Integer.parseInt(Jsoup.clean(request.getParameter("employmentType"), Whitelist.simpleText()));
        List<Payroll_deduction_typeDTO> payroll_deduction_typeDTOList = Payroll_deduction_typeRepository.getInstance().getPayroll_deduction_typeList();
        int idx = 0;
        long currentTime = System.currentTimeMillis();
        for (Payroll_deduction_typeDTO payroll_deduction_typeDTO : payroll_deduction_typeDTOList) {
            Tax_deduction_configurationDTO taxDeductionConfigurationDTO = Tax_deduction_configurationDAO.getInstance().getDTOByPayrollId(payroll_deduction_typeDTO.iD, employmentType);
            boolean isActive = Boolean.parseBoolean(request.getParameter("isActive_" + idx));

            if (taxDeductionConfigurationDTO == null) {
                taxDeductionConfigurationDTO = new Tax_deduction_configurationDTO();
            }

            taxDeductionConfigurationDTO.taxDeductionID = payroll_deduction_typeDTO.iD;
            taxDeductionConfigurationDTO.code = payroll_deduction_typeDTO.code;
            taxDeductionConfigurationDTO.nameEn = payroll_deduction_typeDTO.descriptionEn;
            taxDeductionConfigurationDTO.nameBn = payroll_deduction_typeDTO.descriptionBn;
            taxDeductionConfigurationDTO.employmentType = employmentType;
            taxDeductionConfigurationDTO.isActive = isActive;
            taxDeductionConfigurationDTO.modifiedBy = userDTO.employee_record_id;
            taxDeductionConfigurationDTO.lastModificationTime = currentTime;
            if (taxDeductionConfigurationDTO.iD == -1) {
                taxDeductionConfigurationDTO.insertedBy = userDTO.employee_record_id;
                taxDeductionConfigurationDTO.insertionTime = currentTime;
                Tax_deduction_configurationDAO.getInstance().add(taxDeductionConfigurationDTO);
            } else {

                Tax_deduction_configurationDAO.getInstance().update(taxDeductionConfigurationDTO);
            }
            idx++;

        }
        Tax_deduction_configurationRepository.getInstance().reload(false);
        syncWithLookUp(userDTO);
        return null;
    }

    void syncWithLookUp(UserDTO userDTO) throws Exception {
        int PRIVILEGED_EMPLOYEE_CAT = EmploymentEnum.PRIVILEGED.getValue();
        List<SameDesignationGroup> designationGroups = new ArrayList<>(Employee_recordsRepository.getInstance().getSameDesignationGroups(PRIVILEGED_EMPLOYEE_CAT));
        Payroll_deduction_lookupServlet.UserInput userInput = new Payroll_deduction_lookupServlet.UserInput();
        userInput.modifierId = userDTO.ID;
        userInput.lastModificationTime = System.currentTimeMillis();

        userInput.employmentCat = PRIVILEGED_EMPLOYEE_CAT;

        for (SameDesignationGroup sameDesignationGroup : designationGroups) {
            userInput.organogramId = sameDesignationGroup.organogramId;
            List<PayrollDeductionLookupModel> payrollDeductionLookupModels = new Payroll_deduction_lookupServlet().getLookupModels(PRIVILEGED_EMPLOYEE_CAT, sameDesignationGroup.organogramKey, userDTO);
            for (PayrollDeductionLookupModel payrollDeductionLookupModel : payrollDeductionLookupModels) {
                userInput.configId = payrollDeductionLookupModel.payrollDeductionConfigId;
                if (userInput.configId < 0) continue;
                userInput.lookupDTOId = payrollDeductionLookupModel.lookupDTOId;
                boolean addFlag = (userInput.lookupDTOId < 0);
                userInput.amount = payrollDeductionLookupModel.amount;
                new Payroll_deduction_lookupServlet().addLookupDTO(userInput, addFlag);
            }
        }

    }

    @Override
    public String getTableName() {
        return Tax_deduction_configurationDAO.getInstance().getTableName();
    }

    @Override
    public String getServletName() {
        return "Tax_deduction_configurationServlet";
    }

    @Override
    public Tax_deduction_configurationDAO getCommonDAOService() {
        return Tax_deduction_configurationDAO.getInstance();
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[]{MenuConstants.TAX_DEDUCTION_CONFIGURATION_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[]{MenuConstants.TAX_DEDUCTION_CONFIGURATION_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[]{MenuConstants.TAX_DEDUCTION_CONFIGURATION_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Tax_deduction_configurationServlet.class;
    }
}