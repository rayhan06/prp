package tax_deduction_configuration;

import common.CommonDAOService;
import org.apache.log4j.Logger;
import pb.CommonDAO;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings({"Duplicates"})
public class Tax_deduction_configurationDAO implements CommonDAOService<Tax_deduction_configurationDTO> {
    private static final Logger logger = Logger.getLogger(Tax_deduction_configurationDAO.class);

    private static final String addQuery =
            "INSERT INTO {tableName} (tax_deduction_ID,employment_type,code,name_en,name_bn,isActive,modified_by,"
                    .concat("lastModificationTime,inserted_by,insertion_time,isDeleted,ID) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)");

    private static final String updateQuery =
            "UPDATE {tableName} SET tax_deduction_ID=?,employment_type=?,code=?,name_en=?,name_bn=?,isActive=?,"
                    .concat("modified_by=?,lastModificationTime=? WHERE ID=?");

    private static final String getByPayrollIDQuery =
            "SELECT * FROM tax_deduction_configuration WHERE tax_deduction_ID=%d AND employment_type=%d AND isDeleted=0";

    private final Map<String, String> searchMap = new HashMap<>();

    public Tax_deduction_configurationDAO() {
        searchMap.put("employment_type", " and (employment_type = ?)");
        searchMap.put("name_en", " and (name_en like ?)");
        searchMap.put("name_bn", " and (binary name_bn like ?)");
        searchMap.put("AnyField", " and (search_column like ?)");
    }

    private static class LazyLoader {
        static final Tax_deduction_configurationDAO INSTANCE = new Tax_deduction_configurationDAO();
    }

    public static Tax_deduction_configurationDAO getInstance() {
        return LazyLoader.INSTANCE;
    }

    public void setSearchColumn(Tax_deduction_configurationDTO tax_deduction_configurationDTO) {
        tax_deduction_configurationDTO.searchColumn = "";
        tax_deduction_configurationDTO.searchColumn += tax_deduction_configurationDTO.taxDeductionID + " ";
        tax_deduction_configurationDTO.searchColumn += CommonDAO.getName("English", "employement", tax_deduction_configurationDTO.employmentType) + " " + CommonDAO.getName("Bangla", "employement", tax_deduction_configurationDTO.employmentType) + " ";
        tax_deduction_configurationDTO.searchColumn += tax_deduction_configurationDTO.nameEn + " ";
        tax_deduction_configurationDTO.searchColumn += tax_deduction_configurationDTO.nameBn + " ";
    }

    @Override
    public void set(PreparedStatement ps, Tax_deduction_configurationDTO dto, boolean isInsert) throws SQLException {
        int index = 0;
        ps.setLong(++index, dto.taxDeductionID);
        ps.setInt(++index, dto.employmentType);
        ps.setString(++index, dto.code);
        ps.setString(++index, dto.nameEn);
        ps.setString(++index, dto.nameBn);
        ps.setBoolean(++index, dto.isActive);
        ps.setLong(++index, dto.modifiedBy);
        ps.setLong(++index, dto.lastModificationTime);
        if (isInsert) {
            ps.setLong(++index, dto.insertedBy);
            ps.setLong(++index, dto.insertionTime);
            ps.setInt(++index, 0 /* isDeleted */);
        }
        ps.setLong(++index, dto.iD);
    }

    @Override
    public Tax_deduction_configurationDTO buildObjectFromResultSet(ResultSet rs) {
        try {
            Tax_deduction_configurationDTO dto = new Tax_deduction_configurationDTO();
            dto.iD = rs.getLong("ID");
            dto.taxDeductionID = rs.getLong("tax_deduction_ID");
            dto.employmentType = rs.getInt("employment_type");
            dto.code = rs.getString("code");
            dto.nameEn = rs.getString("name_en");
            dto.nameBn = rs.getString("name_bn");
            dto.isActive = rs.getBoolean("isActive");
            dto.insertedBy = rs.getLong("inserted_by");
            dto.insertionTime = rs.getLong("insertion_time");
            dto.isDeleted = rs.getInt("isDeleted");
            dto.modifiedBy = rs.getLong("modified_by");
            dto.lastModificationTime = rs.getLong("lastModificationTime");
            return dto;
        } catch (SQLException ex) {
            logger.error(ex);
            return null;
        }
    }

    public Tax_deduction_configurationDTO getDTOByID(long id) {
        return getDTOFromID(id);
    }

    @Override
    public String getTableName() {
        return "tax_deduction_configuration";
    }

    @Override
    public Map<String, String> getSearchMap() {
        return searchMap;
    }

    public long add(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Tax_deduction_configurationDTO) commonDTO, addQuery, true);
    }

    public long update(CommonDTO commonDTO) throws Exception {
        return executeAddOrUpdateQuery((Tax_deduction_configurationDTO) commonDTO, updateQuery, false);
    }

    public Tax_deduction_configurationDTO getDTOByPayrollId(long payrollId, int employmentType) {
        String sql = String.format(getByPayrollIDQuery, payrollId, employmentType);
        List<Tax_deduction_configurationDTO> list = getDTOs(sql);
        return list.size() == 0 ? null : list.get(0);
    }
}