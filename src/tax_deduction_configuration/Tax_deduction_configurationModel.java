package tax_deduction_configuration;

import payroll_deduction_type.Payroll_deduction_typeDTO;
import payroll_deduction_type.Payroll_deduction_typeRepository;
import payroll_month_bill_deduction.Payroll_month_bill_deductionDTO;
import pb.Utils;

public class Tax_deduction_configurationModel {
    public String configurationId;
    public String name;
    public String code;

    public Tax_deduction_configurationModel(Tax_deduction_configurationDTO dto, String language) {
        configurationId = String.valueOf(dto.iD);
        Payroll_deduction_typeDTO typeDTO = Payroll_deduction_typeRepository.getInstance().getById(dto.taxDeductionID);
        code = Utils.getDigits(typeDTO.code, language);
        name = language.equalsIgnoreCase("ENGLISH") ? typeDTO.descriptionEn : typeDTO.descriptionBn;
    }

    public Tax_deduction_configurationModel(Payroll_month_bill_deductionDTO dto, String language) {
        configurationId = String.valueOf(dto.taxDeductionConfigurationId);
        Tax_deduction_configurationDTO taxDeductionConfigurationDTO = Tax_deduction_configurationRepository.getInstance().getDTODeletedOrNot(dto.taxDeductionConfigurationId);
        Payroll_deduction_typeDTO typeDTO = Payroll_deduction_typeRepository.getInstance().getById(taxDeductionConfigurationDTO.taxDeductionID);
        code = Utils.getDigits(typeDTO.code, language);
        name = language.equalsIgnoreCase("ENGLISH") ? typeDTO.descriptionEn : typeDTO.descriptionBn;
    }
}
