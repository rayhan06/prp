package employee_education_report;


import language.LC;
import language.LM;
import login.LoginDTO;
import org.apache.log4j.Logger;
import pbReport.PBReportUtils;
import pbReport.ReportCommonService;
import permission.MenuConstants;
import role.PermissionRepository;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;

@SuppressWarnings({"unused", "Duplicates"})
@WebServlet("/Employee_education_report_Servlet")
public class Employee_education_report_Servlet extends HttpServlet implements ReportCommonService {

    private static final long serialVersionUID = 1L;
    private static final Logger logger = Logger.getLogger(Employee_education_report_Servlet.class);
    private static final Set<String> searchParam =
            new HashSet<>(Arrays.asList("officeUnitIds", "officeUnitOrganogramId", "employeeRecordId",
                    "educationLevelType", "degreeExamType", "major", "gradePointCat", "cgpaNumber",
                    "institutionName", "universityInfoType", "resultExamType", "nameEng", "nameBng",
                    "empOfficerCat", "employmentCat", "employeeClassCat", "genderCat", "age_end", "age_start"));
    private final Map<String, String[]> stringMap = new HashMap<>();
    private static final String sql = " employee_education_info edi inner join employee_offices eo on edi.employee_records_id = eo.employee_record_id  left join employee_records er on edi.employee_records_id = er.id ";

    public Employee_education_report_Servlet() {
        stringMap.put("officeUnitIds", new String[]{"criteria", "eo", "office_unit_id", "IN", "AND", "String", "", "", ""
                , "officeUnitIdList", "officeUnitIds", String.valueOf(LC.EMPLOYEE_OFFICE_REPORT_WHERE_OFFICEUNITID), "office_units", null, "true", "1"});
        stringMap.put("officeUnitOrganogramId", new String[]{"criteria", "eo", "office_unit_organogram_id", "=", "AND", "long", "", "", ""
                , "officeUnitOrganogramId", "officeUnitOrganogramId", String.valueOf(LC.EMPLOYEE_OFFICE_REPORT_WHERE_OFFICEUNITORGANOGRAMID), "office_unit_organogram", null, "true", "2"});
        stringMap.put("office_id", new String[]{"criteria", "eo", "office_id", "=", "AND", "long", "", "", "2294", "office_id", null, null, null, null, null, null});
        stringMap.put("isDeleted", new String[]{"criteria", "eo", "isDeleted", "=", "AND", "long", "", "", "0", "isDeleted", null, null, null, null, null, null});
        stringMap.put("status", new String[]{"criteria", "eo", "status", "=", "AND", "long", "", "", "1", "status", null, null, null, null, null, null});
        stringMap.put("isDeleted_1", new String[]{"criteria", "edi", "isDeleted", "=", "AND", "long", "", "", "0", "isDeleted_1", null, null, null, null, null, null});
        stringMap.put("employeeRecordId", new String[]{"criteria", "eo", "employee_record_id", "=", "AND", "long", "", "", "any"
                , "employeeRecordId", "employeeRecordId", String.valueOf(LC.EMPLOYEE_OFFICE_REPORT_SELECT_EMPLOYEERECORDID), "employee_records_id", null, "true", "3"});
        stringMap.put("educationLevelType", new String[]{"criteria", "edi", "education_level_type", "=", "AND", "int", "", "", "any"
                , "educationLevelType", "educationLevelType", String.valueOf(LC.EMPLOYEE_EDUCATION_INFO_EDIT_EDUCATIONLEVELTYPE), "name_type", "education_level", "true", "4"});
        stringMap.put("degreeExamType", new String[]{"criteria", "edi", "degree_exam_type", "=", "AND", "int", "", "", "any"
                , "degreeExamType", "degreeExamType", String.valueOf(LC.EMPLOYEE_EDUCATION_INFO_EDIT_DEGREEEXAMTYPE), "text_type", "degree_exam", "true", "5"});
        stringMap.put("major", new String[]{"criteria", "edi", "major", "=", "AND", "String", "", "", "any"
                , "major", "major", String.valueOf(LC.EMPLOYEE_EDUCATION_INFO_EDIT_MAJOR), "text", null, "true", "6"});
        stringMap.put("resultExamType", new String[]{"criteria", "edi", "result_exam_type", "=", "AND", "int", "", "", "any"
                , "resultExamType", "resultExamType", String.valueOf(LC.EMPLOYEE_EDUCATION_INFO_ADD_RESULTEXAMTYPE), "name_type", "result_exam", "true", "7"});
        stringMap.put("gradePointCat", new String[]{"criteria", "edi", "grade_point_cat", "=", "AND", "int", "", "", "any"
                , "gradePointCat", "gradePointCat", String.valueOf(LC.EMPLOYEE_EDUCATION_INFO_EDIT_GRADEPOINTCAT), "category", "grade_point", "true", "8"});
        stringMap.put("cgpaNumber", new String[]{"criteria", "edi", "cgpa_number", "=", "AND", "String", "", "", "any"
                , "cgpaNumber", "cgpaNumber", String.valueOf(LC.EMPLOYEE_EDUCATION_INFO_EDIT_CGPANUMBER), "number", null, "true", "9"});
        stringMap.put("institutionName", new String[]{"criteria", "edi", "institution_name", "=", "AND", "String", "", "", "any"
                , "institutionName", "institutionName", String.valueOf(LC.EMPLOYEE_EDUCATION_INFO_EDIT_INSTITUTIONNAME), "text", null, "true", "10"});
        stringMap.put("nameEng", new String[]{"criteria", "er", "name_eng", "Like", "AND", "String", "", "", "any", "nameEng",
                "nameEng", String.valueOf(LC.EMPLOYEE_RECORDS_ADD_NAMEENG), "text", null, "true", "11"});
        stringMap.put("nameBng", new String[]{"criteria", "er", "name_bng", "Like", "AND", "String", "", "", "any", "nameBng",
                "nameBng", String.valueOf(LC.EMPLOYEE_RECORDS_ADD_NAMEBNG), "text", null, "true", "12"});
        stringMap.put("genderCat", new String[]{"criteria", "er", "gender_cat", "=", "AND", "long", "", "", "any"
                , "genderCat", "genderCat", String.valueOf(LC.EMPLOYEE_INFO_REPORT_WHERE_GENDERCAT), "category", "gender", "true", "13"});
        stringMap.put("employeeClassCat", new String[]{"criteria", "er", "employee_class_cat", "=", "AND", "long", "", "", "any",
                "employeeClassCat", "employeeClassCat", String.valueOf(LC.EMPLOYEE_EMPLOYEE_CLASS), "category", "employee_class", "true", "14"});
        stringMap.put("employmentCat", new String[]{"criteria", "er", "employment_cat", "=", "AND", "long", "", "", "any"
                , "employmentCat", "employmentCat", String.valueOf(LC.EMPLOYMENT_CATEGORY_EMPLOYMENTCATEGORY), "category", "employment", "true", "15"});
        stringMap.put("empOfficerCat", new String[]{"criteria", "er", "emp_officer_cat", "=", "AND", "long", "", "", "any",
                "empOfficerCat", "empOfficerCat", String.valueOf(LC.EMPLOYEE_OFFICER_TYPE), "category", "emp_officer", "true", "16"});
        stringMap.put("age_start", new String[]{"criteria", "er", "date_of_birth", "<=", "AND", "long", "", "", ""
                , "ageStart", "age_start", String.valueOf(LC.HR_REPORT_AGE_RANGE_START), "number", null, "true", "17"});
        stringMap.put("age_end", new String[]{"criteria", "er", "date_of_birth", ">=", "AND", "long", "", "", ""
                , "ageEnd", "age_end", String.valueOf(LC.HR_REPORT_AGE_RANGE_TO), "number", null, "true", "18"});
    }

    private String[][] Criteria;

    private final String[][] Display =
            {
                    {"display", "eo", "employee_record_id", "employee_records_id", ""},
                    {"display", "eo", "employee_record_id", "user_name", ""},
                    {"display", "eo", "employee_record_id", "mobile_number", ""},
                    {"display", "eo", "employee_record_id", "email", ""},
                    {"display", "eo", "employee_record_id", "id_to_present_address", ""},
                    {"display", "eo", "employee_record_id", "id_to_permanent_address", ""},
                    {"display", "eo", "employee_record_id", "id_to_home_district", ""},
                    {"display", "eo", "office_unit_id", "office_unit", ""},
                    {"display", "eo", "id", "designation", ""},
                    {"display", "eo", "joining_date", "date", ""},
                    {"display", "edi", "education_level_type", "name_type", ""},
                    {"display", "edi", "degree_exam_type", "text_type", ""},
                    {"display", "edi", "other_degree_exam", "text", ""},
                    {"display", "edi", "major", "text", ""},
                    {"display", "edi", "result_exam_type", "name_type", ""},
                    {"display", "edi", "grade_point_cat", "category", ""},
                    {"display", "edi", "cgpa_number", "number", ""},
                    {"display", "edi", "year_of_passing_number", "number", ""},
                    {"display", "edi", "board_type", "name_type", ""},
                    {"display", "edi", "institution_name", "text", ""},
                    {"display", "edi", "duration_years", "number", ""},
                    {"display", "er", "date_of_birth", "complete_age", ""},
                    {"display", "er", "employee_class_cat", "category", ""},
                    {"display", "er", "emp_officer_cat", "category", ""},
                    {"display", "er", "employment_cat", "category", ""},
                    {"display", "er", "gender_cat", "category", ""},
            };


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        if (userDTO == null) {
            request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
            return;
        }

        Display[0][4] = LM.getText(LC.EMPLOYEE_OFFICE_REPORT_SELECT_EMPLOYEERECORDID, loginDTO);
        Display[1][4] = LM.getText(LC.EMPLOYEE_INFO_REPORT_SELECT_USER_ID, loginDTO);
        Display[2][4] = LM.getText(LC.EMPLOYEE_INFO_REPORT_SELECT_MOBILE_NUMBER, loginDTO);
        Display[3][4] = LM.getText(LC.EMPLOYEE_INFO_REPORT_SELECT_EMAIL, loginDTO);
        Display[4][4] = LM.getText(LC.EMPLOYEE_RECORDS_ADD_PRESENTADDRESS, loginDTO);
        Display[5][4] = LM.getText(LC.EMPLOYEE_RECORDS_ADD_PERMANENTADDRESS, loginDTO);
        Display[6][4] = LM.getText(LC.EMPLOYEE_RECORDS_ADD_HOME_DISTRICT, loginDTO);
        Display[7][4] = LM.getText(LC.EMPLOYEE_OFFICE_REPORT_SELECT_OFFICEUNITID, loginDTO);
        Display[8][4] = LM.getText(LC.EMPLOYEE_OFFICE_REPORT_SELECT_OFFICEUNITORGANOGRAMID, loginDTO);
        Display[9][4] = LM.getText(LC.EMPLOYEE_INFO_REPORT_SELECT_JOININGDATE, loginDTO);
        Display[10][4] = LM.getText(LC.EMPLOYEE_EDUCATION_INFO_EDIT_EDUCATIONLEVELTYPE, loginDTO);
        Display[11][4] = LM.getText(LC.EMPLOYEE_EDUCATION_INFO_EDIT_DEGREEEXAMTYPE, loginDTO);
        Display[12][4] = LM.getText(LC.EMPLOYEE_EDUCATION_INFO_ADD_OTHERDEGREEEXAM, loginDTO);
        Display[13][4] = LM.getText(LC.EMPLOYEE_EDUCATION_INFO_EDIT_MAJOR, loginDTO);
        Display[14][4] = LM.getText(LC.EMPLOYEE_EDUCATION_INFO_ADD_RESULTEXAMTYPE, loginDTO);
        Display[15][4] = LM.getText(LC.EMPLOYEE_EDUCATION_INFO_EDIT_GRADEPOINTCAT, loginDTO);
        Display[16][4] = LM.getText(LC.EMPLOYEE_EDUCATION_INFO_ADD_CGPANUMBER, loginDTO);
        Display[17][4] = LM.getText(LC.EMPLOYEE_EDUCATION_INFO_ADD_YEAROFPASSINGNUMBER, loginDTO);
        Display[18][4] = LM.getText(LC.EMPLOYEE_EDUCATION_INFO_ADD_BOARDTYPE, loginDTO);
        Display[19][4] = LM.getText(LC.EMPLOYEE_EDUCATION_INFO_ADD_INSTITUTIONNAME, loginDTO);
        Display[20][4] = LM.getText(LC.EMPLOYEE_EDUCATION_INFO_ADD_DURATIONYEARS, loginDTO);
        Display[21][4] = LM.getText(LC.PHYSIOTHERAPY_PLAN_ADD_AGE, loginDTO);
        Display[22][4] = LM.getText(LC.EMPLOYEE_INFO_REPORT_SELECT_EMPLOYEECLASSCAT, loginDTO);
        Display[23][4] = LM.getText(LC.EMPLOYEE_INFO_REPORT_SELECT_EMPOFFICERCAT, loginDTO);
        Display[24][4] = LM.getText(LC.EMPLOYEE_INFO_REPORT_SELECT_EMPLOYMENTCAT, loginDTO);
        Display[25][4] = LM.getText(LC.EMPLOYEE_INFO_REPORT_SELECT_GENDERCAT, loginDTO);

        Set<String> inputs = PBReportUtils.prepareInputSets(searchParam, request);
        inputs.add("office_id");
        inputs.add("isDeleted");
        inputs.add("status");
        inputs.add("isDeleted_1");
        Criteria = PBReportUtils.prepareCriteria(inputs, stringMap);


        for (String[] arr : Criteria) {
            switch (arr[9]) {
                case "officeUnitIdList":
                    arr[8] = getOfficeIdsFromOfficeUnitIds(request).stream()
                            .map(String::valueOf)
                            .collect(Collectors.joining(","));
                    break;
                case "ageStart":
                    long ageStart = Long.parseLong(request.getParameter("age_start"));
                    arr[8] = String.valueOf(calculateLongDateValue(ageStart));
                    break;
                case "ageEnd":
                    long ageEnd = Long.parseLong(request.getParameter("age_end"));
                    arr[8] = String.valueOf(calculateLongDateValue(ageEnd));
                    break;
            }
        }
        request.setAttribute("dontUseOutDatedModal", true);
        if (PermissionRepository.checkPermissionByRoleIDAndMenuID(userDTO.roleID, MenuConstants.EMPLOYEE_EDUCATION_REPORT)) {
            reportGenerate(request, response);
        }
    }

    @Override
    public String[][] getCriteria() {
        return Criteria;
    }

    @Override
    public String[][] getDisplay() {
        return Display;
    }

    @Override
    public String getSQL() {
        return sql;
    }

    @Override
    public int getLCForReportName() {
        return LC.EMPLOYEE_EDUCATION_REPORT_OTHER_EMPLOYEE_EDUCATION_REPORT;
    }

    @Override
    public String getFileName() {
        return "employee_education_report";
    }

    @Override
    public String getTableName() {
        return "employee_education_report";
    }

    private long calculateLongDateValue(long ageYears) {
        LocalDate ld = LocalDate.now();
        ld = ld.minusYears(ageYears);
        return Date.from(ld.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant()).getTime();
    }
}