package dpm;
import java.util.*; 
import util.*;


public class DpmDetailsMAPS extends CommonMaps
{	
	public DpmDetailsMAPS(String tableName)
	{
		


		java_SQL_map.put("dpm_id".toLowerCase(), "dpmId".toLowerCase());
		java_SQL_map.put("drug_information_id".toLowerCase(), "drugInformationId".toLowerCase());
		java_SQL_map.put("quantity".toLowerCase(), "quantity".toLowerCase());
		java_SQL_map.put("inserted_by_er_id".toLowerCase(), "insertedByErId".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Dpm Id".toLowerCase(), "dpmId".toLowerCase());
		java_Text_map.put("Drug Information Id".toLowerCase(), "drugInformationId".toLowerCase());
		java_Text_map.put("Quantity".toLowerCase(), "quantity".toLowerCase());
		java_Text_map.put("Inserted By Er Id".toLowerCase(), "insertedByErId".toLowerCase());
		java_Text_map.put("Inserted By User Id".toLowerCase(), "insertedByUserId".toLowerCase());
		java_Text_map.put("Inserted By Organogram Id".toLowerCase(), "insertedByOrganogramId".toLowerCase());
		java_Text_map.put("Insertion Date".toLowerCase(), "insertionDate".toLowerCase());
		java_Text_map.put("Last Modifier User".toLowerCase(), "lastModifierUser".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}