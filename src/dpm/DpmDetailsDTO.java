package dpm;
import java.util.*; 
import util.*; 


public class DpmDetailsDTO extends CommonDTO
{

	public long dpmId = -1;
	public long patientErId = -1;
	public long drugInformationId = -1;
	public int quantity = -1;
	public long insertedByErId = -1;
	public long insertedByUserId = -1;
	public long insertedByOrganogramId = -1;
	public long insertionDate = -1;
    public String lastModifierUser = "";
    public double totalPrice = 0;
	
	public List<DpmDetailsDTO> dpmDetailsDTOList = new ArrayList<>();
	
	public DpmDetailsDTO()
	{
		
	}
	
	public DpmDetailsDTO(DpmDTO dpmDTO)
	{
		dpmId = dpmDTO.iD;
		patientErId = dpmDTO.patientErid;
		insertedByErId = dpmDTO.insertedByErId;
		insertedByUserId = dpmDTO.insertedByUserId;
		insertedByOrganogramId = dpmDTO.insertedByOrganogramId;
		lastModifierUser = dpmDTO.lastModifierUser;
	}
	
	public void set(DpmDTO dpmDTO)
	{
		dpmId = dpmDTO.iD;
		patientErId = dpmDTO.patientErid;
		lastModifierUser = dpmDTO.lastModifierUser;
	}
	
    @Override
	public String toString() {
            return "$DpmDetailsDTO[" +
            " iD = " + iD +
            " dpmId = " + dpmId +
            " drugInformationId = " + drugInformationId +
            " quantity = " + quantity +
            " insertedByErId = " + insertedByErId +
            " insertedByUserId = " + insertedByUserId +
            " insertedByOrganogramId = " + insertedByOrganogramId +
            " insertionDate = " + insertionDate +
            " lastModifierUser = " + lastModifierUser +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}