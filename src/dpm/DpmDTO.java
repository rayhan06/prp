package dpm;
import java.util.*; 
import util.*; 


public class DpmDTO extends CommonDTO
{

	public long patientErid = -1;
	public long approverErid = -1;
	public int purchaseMethodCat = -1;
    public String storeName = "";
    public String storeAddress = "";
	public long insertedByErId = -1;
	public long insertedByUserId = -1;
	public long insertedByOrganogramId = -1;
	public long insertionDate = -1;
	public long entryDate = TimeConverter.getToday();
    public String lastModifierUser = "";
	
	public List<DpmDetailsDTO> dpmDetailsDTOList = new ArrayList<>();
	
    @Override
	public String toString() {
            return "$DpmDTO[" +
            " iD = " + iD +
            " patientErid = " + patientErid +
            " approverErid = " + approverErid +
            " purchaseMethodCat = " + purchaseMethodCat +
            " storeName = " + storeName +
            " storeAddress = " + storeAddress +
            " insertedByErId = " + insertedByErId +
            " insertedByUserId = " + insertedByUserId +
            " insertedByOrganogramId = " + insertedByOrganogramId +
            " insertionDate = " + insertionDate +
            " lastModifierUser = " + lastModifierUser +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}