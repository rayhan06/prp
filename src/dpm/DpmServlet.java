package dpm;


import java.text.SimpleDateFormat;



import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;

import org.apache.log4j.Logger;

import permission.MenuConstants;

import user.UserDTO;
import util.*;
import workflow.WorkflowController;

import javax.servlet.http.*;
import java.util.*;


import common.BaseServlet;
import pb.Utils;

import com.google.gson.Gson;

import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;




/**
 * Servlet implementation class DpmServlet
 */
@WebServlet("/DpmServlet")
@MultipartConfig
public class DpmServlet extends BaseServlet 
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(DpmServlet.class);

    @Override
    public String getTableName() {
        return DpmDAO.getInstance().getTableName();
    }

    @Override
    public String getServletName() {
        return "DpmServlet";
    }

    @Override
    public DpmDAO getCommonDAOService() {
        return DpmDAO.getInstance();
    }
    
    @Override
    public int[] getAddPageMenuConstants() {
        return new int[] {MenuConstants.DPM_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[] {MenuConstants.DPM_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[] {MenuConstants.DPM_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return DpmServlet.class;
    }
	DpmDetailsDAO dpmDetailsDAO = DpmDetailsDAO.getInstance();
    private final Gson gson = new Gson();
    
    @Override
    public Map<String, String> buildRequestParams(HttpServletRequest request) {
    	 Enumeration<String> paramList = request.getParameterNames();
         Map<String,String> params = new HashMap<>();
         while (paramList.hasMoreElements()){
             String paramName = paramList.nextElement();
             String paramValue = request.getParameter(paramName);
             if(Utils.isValidSearchString(paramValue)){
            	 if(paramName.equalsIgnoreCase("patient_erid"))
            	 {
            		 long erId = WorkflowController.getEmployeeRecordsIdFromUserName(paramValue);
            		 if(erId != -1)
            		 {
            			 params.put(paramName, erId + "");
            		 }
            		 
            	 }
            	 else
            	 {
            		 params.put(paramName,paramValue);
            	 }
                 
             }
         }
         return params;
    }
 	

	@Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception 
	{
		
		request.setAttribute("failureMessage", "");
		System.out.println("%%%% addDpm");
		String language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
		DpmDTO dpmDTO;
		SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
					
		if(addFlag)
		{
			dpmDTO = new DpmDTO();
		}
		else
		{
			dpmDTO = DpmDAO.getInstance().getDTOFromID(Long.parseLong(request.getParameter("iD")));
		}
		
		String Value = "";

		Value = request.getParameter("patientErid");

		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("patientErid = " + Value);
		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			dpmDTO.patientErid = Long.parseLong(Value);
			String userName = WorkflowController.getUserNameFromErId(dpmDTO.patientErid, "english");
			if(userName.equalsIgnoreCase(""))
			{
				return null;
			}
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}

		Value = request.getParameter("approverOrgId");

		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("approverOrgId = " + Value);
		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			dpmDTO.approverErid = WorkflowController.getEmployeeRecordIDFromOrganogramID(Long.parseLong(Value)) ;
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}
		
		Value = request.getParameter("entryDate");

		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("entryDate = " + Value);
		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			try 
			{
				Date d = f.parse(Value);
				dpmDTO.entryDate = d.getTime();
			}
			catch (Exception e) 
			{
				logger.error("",e);
			}
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}

		Value = request.getParameter("purchaseMethodCat");

		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("purchaseMethodCat = " + Value);
		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			dpmDTO.purchaseMethodCat = Integer.parseInt(Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}

		Value = request.getParameter("storeName");

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("storeName = " + Value);
		if(Value != null)
		{
			dpmDTO.storeName = (Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}

		Value = request.getParameter("storeAddress");

		if(Value != null)
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("storeAddress = " + Value);
		if(Value != null)
		{
			dpmDTO.storeAddress = (Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}

	

		if(addFlag)
		{
			dpmDTO.insertedByUserId = userDTO.ID;
			dpmDTO.insertedByErId = userDTO.employee_record_id;
		}


		if(addFlag)
		{
			dpmDTO.insertedByOrganogramId = userDTO.organogramID;
		}


		if(addFlag)
		{				
			dpmDTO.insertionDate = TimeConverter.getToday();
		}			


		dpmDTO.lastModifierUser = userDTO.userName;

		System.out.println("Done adding  addDpm dto = " + dpmDTO);

		if(addFlag == true)
		{
			DpmDAO.getInstance().add(dpmDTO);
		}
		else
		{				
			DpmDAO.getInstance().update(dpmDTO);										
		}
		
		List<DpmDetailsDTO> dpmDetailsDTOList = createDpmDetailsDTOListByRequest(request, language, dpmDTO, userDTO);
		

		if(addFlag == true) //add or validate
		{
			if(dpmDetailsDTOList != null)
			{				
				for(DpmDetailsDTO dpmDetailsDTO: dpmDetailsDTOList)
				{
					dpmDetailsDAO.add(dpmDetailsDTO);
				}
			}
		
		}			
		else
		{
			List<Long> childIdsFromRequest = dpmDetailsDAO.getChildIdsFromRequest(request, "dpmDetails");
			//delete the removed children
			dpmDetailsDAO.deleteChildrenNotInList("dpm", "dpm_details", dpmDTO.iD, childIdsFromRequest);
			List<Long> childIDsInDatabase = dpmDetailsDAO.getChilIds("dpm", "dpm_details", dpmDTO.iD);
			
			
			if(childIdsFromRequest != null)
			{
				for(int i = 0; i < childIdsFromRequest.size(); i ++)
				{
					Long childIDFromRequest = childIdsFromRequest.get(i);
					if(childIDsInDatabase.contains(childIDFromRequest))
					{
						DpmDetailsDTO dpmDetailsDTO =  createDpmDetailsDTOByRequestAndIndex(request, false, i, language, dpmDTO, userDTO);
						dpmDetailsDAO.update(dpmDetailsDTO);
					}
					else
					{
						DpmDetailsDTO dpmDetailsDTO =  createDpmDetailsDTOByRequestAndIndex(request, true, i, language, dpmDTO, userDTO);
						dpmDetailsDAO.add(dpmDetailsDTO);
					}
				}
			}
			else
			{
				dpmDetailsDAO.deleteChildrenByParent(dpmDTO.iD, "dpm_id");
			}
			
		}					
		return dpmDTO;

	}
	private List<DpmDetailsDTO> createDpmDetailsDTOListByRequest(HttpServletRequest request, String language, DpmDTO dpmDTO, UserDTO userDTO) throws Exception{ 
		List<DpmDetailsDTO> dpmDetailsDTOList = new ArrayList<DpmDetailsDTO>();
		if(request.getParameterValues("dpmDetails.iD") != null) 
		{
			int dpmDetailsItemNo = request.getParameterValues("dpmDetails.iD").length;
			
			
			for(int index=0;index<dpmDetailsItemNo;index++){
				DpmDetailsDTO dpmDetailsDTO = createDpmDetailsDTOByRequestAndIndex(request,true,index, language, dpmDTO, userDTO);
				if(dpmDetailsDTO != null)
				{
					dpmDetailsDTOList.add(dpmDetailsDTO);
				}
				
			}
			
			return dpmDetailsDTOList;
		}
		return null;
	}
	
	private DpmDetailsDTO createDpmDetailsDTOByRequestAndIndex(HttpServletRequest request, boolean addFlag, int index, String language, DpmDTO dpmDTO, UserDTO userDTO) throws Exception{
	
		DpmDetailsDTO dpmDetailsDTO;
		if(addFlag == true )
		{
			dpmDetailsDTO = new DpmDetailsDTO(dpmDTO);
		}
		else
		{
			dpmDetailsDTO = dpmDetailsDAO.getDTOByID(Long.parseLong(request.getParameterValues("dpmDetails.iD")[index]));
			dpmDetailsDTO.set(dpmDTO);
		}


		String Value = "";

		Value = request.getParameterValues("dpmDetails.drugInformationId")[index];

		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("drugInformationId = " + Value);
		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			dpmDetailsDTO.drugInformationId = Long.parseLong(Value);
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}

		Value = request.getParameterValues("dpmDetails.quantity")[index];

		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("quantity = " + Value);
		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			dpmDetailsDTO.quantity = Integer.parseInt(Value);
			if(dpmDetailsDTO.quantity <= 0)
			{
				return null;
			}
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}
		
		Value = request.getParameterValues("dpmDetails.totalPrice")[index];

		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			Value = Jsoup.clean(Value,Whitelist.simpleText());
		}
		System.out.println("totalPrice = " + Value);
		if(Value != null && !Value.equalsIgnoreCase(""))
		{
			dpmDetailsDTO.totalPrice = Double.parseDouble(Value);
			if(dpmDetailsDTO.totalPrice < 0)
			{
				return null;
			}
		}
		else
		{
			System.out.println("FieldName has a null Value, not updating" + " = " + Value);
		}

		return dpmDetailsDTO;
	
	}	
}

