package dpm;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.sql.SQLException;
import common.CommonDAOService;
import org.apache.log4j.Logger;
import util.*;
import workflow.WorkflowController;
import pb.*;

public class DpmDAO  implements CommonDAOService<DpmDTO>
{
	
	Logger logger = Logger.getLogger(getClass());
	
	private static String addQuery = "";
	private static String updateQuery = "";
	private String[] columnNames = null;

	private DpmDAO()
	{
		columnNames = new String[] 
		{
			"ID",
			"patient_erid",
			"approver_erid",
			"purchase_method_cat",
			"store_name",
			"store_address",
			"inserted_by_er_id",
			"inserted_by_user_id",
			"inserted_by_organogram_id",
			"insertion_date",
			"entry_date",
			"last_modifier_user",
			"isDeleted",
			"lastModificationTime"
		};
		updateQuery = getUpdateQuery(columnNames);
        addQuery = getAddQuery(columnNames);
		
		searchMap.put("patient_erid"," and (patient_erid like ?)");
		searchMap.put("approver_erid"," and (approver_erid like ?)");
		searchMap.put("purchase_method_cat"," and (purchase_method_cat = ?)");
		searchMap.put("store_name"," and (store_name like ?)");
		searchMap.put("entry_date_start"," and (entry_date >= ?)");
		searchMap.put("entry_date_end"," and (entry_date <= ?)");
		searchMap.put("AnyField"," and (search_column like ?)");
	}

	private final Map<String,String> searchMap = new HashMap<>();

    private static class LazyLoader
    {
    	static final DpmDAO INSTANCE = new DpmDAO();
	}

	public static DpmDAO getInstance()
	{
    	return LazyLoader.INSTANCE;
	}
	
	public void setSearchColumn(DpmDTO dpmDTO)
	{
		dpmDTO.searchColumn = "";
		dpmDTO.searchColumn += WorkflowController.getUserNameFromErId(dpmDTO.patientErid, "english") + " ";
		dpmDTO.searchColumn += WorkflowController.getUserNameFromErId(dpmDTO.patientErid, "bangla") + " ";
		
		dpmDTO.searchColumn += dpmDTO.storeName + " ";
	}
	
	@Override
	public void set(PreparedStatement ps, DpmDTO dpmDTO, boolean isInsert) throws SQLException
	{
		int index = 0;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(dpmDTO);
		if(isInsert)
		{
			ps.setObject(++index,dpmDTO.iD);
		}
		ps.setObject(++index,dpmDTO.patientErid);
		ps.setObject(++index,dpmDTO.approverErid);
		ps.setObject(++index,dpmDTO.purchaseMethodCat);
		ps.setObject(++index,dpmDTO.storeName);
		ps.setObject(++index,dpmDTO.storeAddress);
		ps.setObject(++index,dpmDTO.insertedByErId);
		ps.setObject(++index,dpmDTO.insertedByUserId);
		ps.setObject(++index,dpmDTO.insertedByOrganogramId);
		ps.setObject(++index,dpmDTO.insertionDate);
		ps.setObject(++index,dpmDTO.entryDate);
		ps.setObject(++index,dpmDTO.lastModifierUser);
		if(isInsert)
		{
			ps.setObject(++index,dpmDTO.isDeleted);
		}
		ps.setObject(++index, lastModificationTime);
		if(!isInsert)
		{
			ps.setObject(++index,dpmDTO.iD);
		}
	}
	
	@Override
	public DpmDTO buildObjectFromResultSet(ResultSet rs)
	{
		try
		{
			DpmDTO dpmDTO = new DpmDTO();
			int i = 0;
			dpmDTO.iD = rs.getLong(columnNames[i++]);
			dpmDTO.patientErid = rs.getLong(columnNames[i++]);
			dpmDTO.approverErid = rs.getLong(columnNames[i++]);
			dpmDTO.purchaseMethodCat = rs.getInt(columnNames[i++]);
			dpmDTO.storeName = rs.getString(columnNames[i++]);
			dpmDTO.storeAddress = rs.getString(columnNames[i++]);
			dpmDTO.insertedByErId = rs.getLong(columnNames[i++]);
			dpmDTO.insertedByUserId = rs.getLong(columnNames[i++]);
			dpmDTO.insertedByOrganogramId = rs.getLong(columnNames[i++]);
			dpmDTO.insertionDate = rs.getLong(columnNames[i++]);
			dpmDTO.entryDate = rs.getLong(columnNames[i++]);
			dpmDTO.lastModifierUser = rs.getString(columnNames[i++]);
			dpmDTO.isDeleted = rs.getInt(columnNames[i++]);
			dpmDTO.lastModificationTime = rs.getLong(columnNames[i++]);
			return dpmDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
		
	public DpmDTO getDTOByID (long id)
	{
		DpmDTO dpmDTO = null;
		try 
		{
			dpmDTO = getDTOFromID(id);
			if(dpmDTO != null)
			{
				DpmDetailsDAO dpmDetailsDAO = DpmDetailsDAO.getInstance();				
				List<DpmDetailsDTO> dpmDetailsDTOList = (List<DpmDetailsDTO>)dpmDetailsDAO.getDTOsByParent("dpm_id", dpmDTO.iD);
				dpmDTO.dpmDetailsDTOList = dpmDetailsDTOList;
			}
		}
		catch (Exception e) 
		{
				// TODO Auto-generated catch block
			loggerCommonDAOService.error("",e);
		}
		return dpmDTO;
	}

	@Override
	public String getTableName() {
		return "dpm";
	}

	@Override
	public Map<String, String> getSearchMap() {
		return searchMap;
	}

	public long add(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((DpmDTO) commonDTO,addQuery,true);
	}

	public long update(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((DpmDTO) commonDTO,updateQuery,false);
	}
	
	@Override
	public String getLastModifierUser(){
		return "last_modifier_user";
    }
				
}
	