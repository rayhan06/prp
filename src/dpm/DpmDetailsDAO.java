package dpm;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.sql.SQLException;
import common.CommonDAOService;
import org.apache.log4j.Logger;
import util.*;
import pb.*;

public class DpmDetailsDAO  implements CommonDAOService<DpmDetailsDTO>
{
	
	Logger logger = Logger.getLogger(getClass());
	
	private static String addQuery = "";
	private static String updateQuery = "";
	private String[] columnNames = null;

	private DpmDetailsDAO()
	{
		columnNames = new String[] 
		{
			"ID",
			"dpm_id",
			"drug_information_id",
			"quantity",
			"total_price",
			"inserted_by_er_id",
			"inserted_by_user_id",
			"inserted_by_organogram_id",
			"insertion_date",
			"last_modifier_user",
			"isDeleted",
			"lastModificationTime"
		};
		updateQuery = getUpdateQuery(columnNames);
        addQuery = getAddQuery(columnNames);
		
		searchMap.put("insertion_date_start"," and (insertion_date >= ?)");
		searchMap.put("insertion_date_end"," and (insertion_date <= ?)");
		searchMap.put("AnyField"," and (search_column like ?)");
	}

	private final Map<String,String> searchMap = new HashMap<>();

    private static class LazyLoader
    {
    	static final DpmDetailsDAO INSTANCE = new DpmDetailsDAO();
	}

	public static DpmDetailsDAO getInstance()
	{
    	return LazyLoader.INSTANCE;
	}
	
	public void setSearchColumn(DpmDetailsDTO dpmdetailsDTO)
	{
		dpmdetailsDTO.searchColumn = "";
	}
	
	@Override
	public void set(PreparedStatement ps, DpmDetailsDTO dpmdetailsDTO, boolean isInsert) throws SQLException
	{
		int index = 0;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(dpmdetailsDTO);
		if(isInsert)
		{
			ps.setObject(++index,dpmdetailsDTO.iD);
		}
		ps.setObject(++index,dpmdetailsDTO.dpmId);
		ps.setObject(++index,dpmdetailsDTO.drugInformationId);
		ps.setObject(++index,dpmdetailsDTO.quantity);
		ps.setObject(++index,dpmdetailsDTO.totalPrice);
		ps.setObject(++index,dpmdetailsDTO.insertedByErId);
		ps.setObject(++index,dpmdetailsDTO.insertedByUserId);
		ps.setObject(++index,dpmdetailsDTO.insertedByOrganogramId);
		ps.setObject(++index,dpmdetailsDTO.insertionDate);
		ps.setObject(++index,dpmdetailsDTO.lastModifierUser);
		if(isInsert)
		{
			ps.setObject(++index,dpmdetailsDTO.isDeleted);
		}
		ps.setObject(++index, lastModificationTime);
		if(!isInsert)
		{
			ps.setObject(++index,dpmdetailsDTO.iD);
		}
	}
	
	@Override
	public DpmDetailsDTO buildObjectFromResultSet(ResultSet rs)
	{
		try
		{
			DpmDetailsDTO dpmdetailsDTO = new DpmDetailsDTO();
			int i = 0;
			dpmdetailsDTO.iD = rs.getLong(columnNames[i++]);
			dpmdetailsDTO.dpmId = rs.getLong(columnNames[i++]);
			dpmdetailsDTO.drugInformationId = rs.getLong(columnNames[i++]);
			dpmdetailsDTO.quantity = rs.getInt(columnNames[i++]);
			dpmdetailsDTO.totalPrice = rs.getDouble(columnNames[i++]);
			dpmdetailsDTO.insertedByErId = rs.getLong(columnNames[i++]);
			dpmdetailsDTO.insertedByUserId = rs.getLong(columnNames[i++]);
			dpmdetailsDTO.insertedByOrganogramId = rs.getLong(columnNames[i++]);
			dpmdetailsDTO.insertionDate = rs.getLong(columnNames[i++]);
			dpmdetailsDTO.lastModifierUser = rs.getString(columnNames[i++]);
			dpmdetailsDTO.isDeleted = rs.getInt(columnNames[i++]);
			dpmdetailsDTO.lastModificationTime = rs.getLong(columnNames[i++]);
			return dpmdetailsDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
		
	public DpmDetailsDTO getDTOByID (long id)
	{
		return getDTOFromID(id);
	}

	@Override
	public String getTableName() {
		return "dpm_details";
	}

	@Override
	public Map<String, String> getSearchMap() {
		return searchMap;
	}

	public long add(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((DpmDetailsDTO) commonDTO,addQuery,true);
	}

	public long update(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((DpmDetailsDTO) commonDTO,updateQuery,false);
	}
	
	@Override
	public String getLastModifierUser(){
		return "last_modifier_user";
    }
				
}
	